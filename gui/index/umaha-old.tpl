<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Sistem Langitan - {$nama_pt}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="shortcut icon" href="icon-langitan-57.png" />
	<link rel="apple-touch-icon" href="icon-langitan-57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="icon-langitan-72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="icon-langitan-114.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="icon-langitan-144.png" />

	<link href="css/bootstrap-3.2.min.css" rel="stylesheet" id="bootstrap-css">

	<style type="text/css">
	body {
		background: url('langitan/Bg-Umaha.jpg');
		background-color: #444;
	}
	.panel-default > .panel-heading {
	    color: #333;
	    background-color: #094231;
	    border-color: #DDD;
	}
	.form-signin input[type="text"] {
		margin-bottom: 5px;
		border-bottom-left-radius: 0;
		border-bottom-right-radius: 0;
	}
	.form-signin input[type="password"] {
		margin-bottom: 10px;
		border-top-left-radius: 0;
		border-top-right-radius: 0;
	}
	.form-signin .form-control {
		position: relative;
		font-size: 16px;
		font-family: 'Open Sans', Arial, Helvetica, sans-serif;
		height: auto;
		padding: 10px;
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
	}
	.vertical-offset-100 {
		padding-top: 100px;
	}
	.img-responsive {
	display: block;
	max-width: 100%;
	height: auto;
	margin: auto;
	}
	.panel {
	margin-bottom: 20px;
	background-color: rgba(255, 255, 255, 0.75);
	border: 1px solid transparent;
	border-radius: 4px;
	-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
	box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
	}
	</style>
	
</head>
<body>	

	<div class="container">
		<div class="row vertical-offset-100">
			<img src="langitan/logo-langitan-{$nama_singkat}.PNG" class="img-responsive" alt="Sistem Langitan Nahdlatul Ulama"/>

			<!-- <h3><center>TES hahahha hihihih huhuhuhu hahaha</center></h3> -->
			<!-- <br/>
			<br/> -->
			<div class="col-md-4 col-md-offset-4">


				<div class="panel panel-default">
					<!--<div class="panel-heading">                                
						<div class="row-fluid user-row">
							<img src="langitan/logo-langitan-{$nama_singkat}.PNG" class="img-responsive" alt="Sistem Langitan Nahdlatul Ulama"/>
						</div>
					</div>
					-->
					<div class="panel-body">
						<form accept-charset="UTF-8" role="form" class="form-signin" method="post" action="login.php">
							<input name="mode" type="hidden" value="login">
							<fieldset>
								<label class="panel-login">
									<div class="login_result"></div>
								</label>
								<input class="form-control" placeholder="NIP/NIK/NIM" id="username" type="text" name="username" />
								<input class="form-control" placeholder="Password" id="password" type="password" name="password"/>
								<br />
								<input class="btn btn-lg btn-success btn-block" type="submit" id="login" value="Login" />
								<br/>
								<hr/>
                                <p>Untuk membantu pencegahan penyebaran COVID-19 di UMAHA, semua pertanyaan / komplain terkait Sistem Langitan / DSI
                                    dilakukan secara online dengan menghubungi nomer kontak berikut:</p>
                                <label>Helpdesk 1 : <img src="img/login/wa-small.png" /> <a href="https://wa.me/6281233825113">0812-3382-5113</a> (09.00 - 15.00)</label>
                                <label>Helpdesk 2 : <img src="img/login/wa-small.png" /> <a href="https://wa.me/6289678642147">0896-7864-2147</a> (15.00 - 21.00)</label>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- </body></html> Prevent Telkom Injection Script -->
</body>
</html>