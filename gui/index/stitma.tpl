<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Sistem Langitan - {$nama_pt}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" href="../../css/login-umaha.css" id="bootstrap-css" />

	<style type="text/css">
	body {
		background-color: #444;
	}
	.panel-default > .panel-heading {
	    color: #333;
	    background-color: #375b01;
	    border-color: #DDD;
	}
	.form-signin input[type="text"] {
		margin-bottom: 5px;
		border-bottom-left-radius: 0;
		border-bottom-right-radius: 0;
	}
	.form-signin input[type="password"] {
		margin-bottom: 10px;
		border-top-left-radius: 0;
		border-top-right-radius: 0;
	}
	.form-signin .form-control {
		position: relative;
		font-size: 16px;
		font-family: 'Open Sans', Arial, Helvetica, sans-serif;
		height: auto;
		padding: 10px;
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
	}
	.vertical-offset-100 {
		padding-top: 100px;
	}
	.img-responsive {
	display: block;
	max-width: 100%;
	height: auto;
	margin: auto;
	}
	.panel {
	margin-bottom: 20px;
	background-color: rgba(255, 255, 255, 0.75);
	border: 1px solid transparent;
	border-radius: 4px;
	-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
	box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
	}
	</style>
	
</head>
<body>
	<div class="container">
		<div class="row vertical-offset-100">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">                                
						<div class="row-fluid user-row">
							<img src="langitan/logo-langitan-{$nama_singkat}.PNG" class="img-responsive" alt="Sistem Langitan Nahdlatul Ulama"/>
						</div>
					</div>
					<div class="panel-body">
						<form accept-charset="UTF-8" role="form" class="form-signin" method="post" action="login.php">
							<input name="mode" type="hidden" value="login">
							<fieldset>
								<label class="panel-login">
									<div class="login_result"></div>
								</label>
								<input class="form-control" placeholder="Username" id="username" type="text" name="username" />
								<input class="form-control" placeholder="Password" id="password" type="password" name="password"/>
								<br />
								<input class="btn btn-lg btn-success btn-block" type="submit" id="login" value="Login" />
								<br/>
								<!--
								<label><a href="langitan/Sistem_Langitan_UNU.pptx">Download Konsep Langitan (PowerPoint 2007)</a></label>
								<br/>
								<label><a href="langitan/Sistem_Langitan_UNU.ppt">Download Konsep Langitan (PowerPoint 2003)</a></label>
								-->
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script src="//mymaplist.com/js/vendor/TweenLite.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(document).mousemove(function(event) {
				TweenLite.to($("body"), 
				.5, {
					css: {
						backgroundPosition: "" + parseInt(event.pageX / 8) + "px " + parseInt(event.pageY / '12') + "px, " + parseInt(event.pageX / '15') + "px " + parseInt(event.pageY / '15') + "px, " + parseInt(event.pageX / '30') + "px " + parseInt(event.pageY / '30') + "px",
						"background-position": parseInt(event.pageX / 8) + "px " + parseInt(event.pageY / 12) + "px, " + parseInt(event.pageX / 15) + "px " + parseInt(event.pageY / 15) + "px, " + parseInt(event.pageX / 30) + "px " + parseInt(event.pageY / 30) + "px"
					}
				})
			})
		});
	</script>

<!-- </body></html> Prevent Telkom Injection Script -->
</body>
</html>