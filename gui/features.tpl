<div id="main">
	<div class="shell">
		<div id="content" class="left">
            <h2>Our Offer</h2>
            <p><strong>Gallery Campus</strong> berdiri pada tahun 2010, sebagai sebuah bentuk usaha yang bergerak di bidang Clothing Line. Pada awalnya kami hanya melayani pesanan atribut-atribut kampus, namun kini bidang usaha kami juga merambah pada Web Development dan Web Hosting.</p>
            <p><strong>Gallery Campus Clothing Line</strong> Clothing Line Gallery Campus merupakan bidang usaha yang pertama kami dirikan. Kami memberikan jasa pemesanan Kaos dan Jaket kampus, sering kami menyebutnya atribut kampus.</p>
            <p><strong>Gallery Campus Web Hosting</strong> GalleryCampus Web Hosting adalah produk baru kami. Sebuah produk yang menawarkan hosting website secara gratis maupun bayar. Hal ini merupakan tantangan tersendiri bagi kami dan kami beranikan diri untuk tampil dengan sebuah inovasi yang tidak pernah terlintas di benak  sebelumnya. Inovasi yang mungkin dirasa berat oleh mahasiswa, namun bagi kami, itu merupakan hal yang menyenangkan bila kami memiliki produk yang dapat kami bagikan. Dengan pelajar dan mahasiswa sebagai pengguna fasilitas ini, kami harapkan kebutuhan mereka akan sarana pendidikan dapat terpenuhi baik melalui atribut-atribut  maupun website hosting yang nantinya akan mereka perlukan dalam tugas yang diberikan. Akan tetapi, kami juga tidak menutup kemungkinan bagi pengguna lain untuk memanfaatkan fasilitas kami, baik itu orang awam, dosen, dan sebagainya.</p>
            <p><strong>Gallery Campus Web Development</strong> Tak diragukan lagi, masa sekarang adalah eranya informasi mobile. Informasi begitu kian cepat dan mudah diakses diamanapun dan kapanpun. Permintaan akan pembuatan website, baik untuk kalangan pribadi, organisasi, maupun perusahaan naik karena demikian mudahnya informasi itu diakses. Kami, Tim Gallery Campus Web Development,  menerima pesanan website baik untuk skala kecil maupun sedang.</p>
 ( Author : Sani Iman Pribadi )
        </div>
        <div class="cl">&nbsp;</div>
    </div>
</div>