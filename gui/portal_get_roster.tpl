	<div class="portlet-roster">
		<div class="portlet-header">
		<span style="font-size:12px;font-style:italic; font-weight:bold; color:blue;"> 
		<!--{$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun}-->
		</span>
		</div>
		<div class="portlet-content">
		<table class="ui-widget" id="hor-zebra">
			<thead class="ui-widget-header">
			<tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;">
				
					<td style="padding:3px;">
						MULAI
					</td>
					
					<td style="padding:3px;">
						SELESAI
					</td>
					
					<td style="padding:3px;">
						KODE
					</td>
					
					<td style="padding:3px;">
						MATA AJAR
					</td>
					
					<td style="padding:3px;">
						KELAS
					</td>

					<td style="padding:3px;">
						RUANGAN
					</td>

					<td style="padding:3px;">
						JML
					</td>
				</tr>
			</thead>
			
			<tbody class="ui-widget-content" style="font-size:12px;">
			{for $foo=0 to $jml_data}
				<tr>
					<td>
						{$mulai[$foo]}
					</td>
					<td>
						{$selesai[$foo]}
					</td>
					<td>
						{$kd_mata_kuliah[$foo]}
					</td>
					<td>
						{$nm_mata_kuliah[$foo]}
					</td>
					<td>
						{$nama_kelas[$foo]}
					</td>
					<td>
						{$nm_ruangan[$foo]}
					</td>
					<td>
						{$kls_terisi[$foo]}
					</td>
				</tr>
			{/for}
			</tbody>
		</table>
		</div>
	</div>