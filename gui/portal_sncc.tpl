<style>
	#toolbar {
		padding: 10px 4px;
	}
	.btn-like{
		background:url('/img/social-network/thumbs_up.png');

		background-repeat:no-repeat;
		width:18px;
		height:18px;
		float:left;
	}
	.btn-dislike{
		background:url('/img/social-network/thumbs_down.png');
		background-repeat:no-repeat;
		width:18px;
		height:18px;
		float:left;
	}
	.btn-comment{
		background:url('/img/social-network/comment.png');
		background-repeat:no-repeat;
		width:18px;
		height:18px;
		float:left;
	}
	.btn-like:hover{
		cursor:pointer;
	}
	
	{for $i=0 to $jml_status-1}
		#btn_like{$id_status[$i]}:hover{
			cursor:pointer;
			text-decoration:underline;
		}
		#btn_dislike{$id_status[$i]}:hover{
			cursor:pointer;
			text-decoration:underline;
		}
		#btn_comment{$id_status[$i]}:hover{
			cursor:pointer;
			text-decoration:underline;
		}
	{/for}
</style>
<script>
	function ImgError(source){
		source.src = "/images/noimages.jpg";
		source.onerror = "";
		return true;
	}
	
	function ImgErrorSmall(source){
		source.src = "/images/noimage-small.gif";
		source.onerror = "";
		return true;
	}

	$(function() {
				$("#btnSubmit").button({
					icons: {
						primary: "ui-icon-check",
					},
					text: true
				});
				
				$("#btnLike").button({
					icons: {
						primary: "ui-icon-heart",
					},
					text: true
				});
				$("#btnComment").button({
					icons: {
						primary: "ui-icon-comment",
					},
					text: true
				});
		
				{for $i=0 to $jml_status-1}
						$("#btnDelete{$i}").button({
							icons: {
								primary: "ui-icon-trash",
							},
							text: true
						});
						
						$("#btnLike{$i}").button({
							icons: {
								primary: "ui-icon-heart",
							},
							text: true
						});
						
						$("#btnComment{$i}").button({
							icons: {
								primary: "ui-icon-comment",
							},
							text: true
						});			

				{/for}
				
	});
</script>
<script>
var source = new EventSource('status_checker.php');
var curData = parseInt({$max_id_status});
source.onmessage = function(e1) {
  var newData = parseInt(e1.data);
  if(newData > curData){
	$('#status_content').load('portal_sncc.php');
  }
};
</script>


<div class="column-news" id="status_content">
	
	<div class="portlet-news">
		<section>
		<div class="ui-widget-header" style="padding:3px;">Social Network Cyber Campus Unair</div>
		<div  class="portlet-content-news ui-widget-content" >
			{for $i=0 to $jml_status-1}
				<div style="float:left; width:60px; padding-left:5px;">
					{$username_fb[$i]}<img src="{$foto_pengguna_status[$i]}/{$username_status[$i]}.JPG" onerror="ImgError(this);" style="height:70px; width:60px; padding-top:5px;" />{$username_fbx[$i]}
				</div>
				<div style="float:left; width:510px; padding-left:5px;" >
								<table width="100%" class="ui-widget" border="0">
									<tr>
										<td valign="top">
												<strong>{$nm_pengguna_status[$i]}-{$id_pengguna_status[$i]}</strong>
												<br />
												{$status[$i]}
												<br />
												<span style="color:blue; font-size:10px;">{$tgl_status[$i]}</span>
												<span style="color:blue; font-size:10px;">{$id_status[$i]}</span>
										</td>
									</tr>
							<tr>
								<td class="ui-widget-content">
									<div class="btn-like">
										<form style="display:none;" action="is-like.php" id="form-like-status{$id_status[$i]}" name="form-like-status{$id_status[$i]}" method="post">
											<input type="text" name="id_status" value="{$id_status[$i]}" style="display:none"/>
											<input type="text" name="id_pengguna" value="{$id_pengguna}" style="display:none;" />
										</form>
									</div>
									
									<span id="space-likes{$id_status[$i]}">
										{for $suka = 0 to $jml_data_sn_status_suka}
											{if $id_sn_status_suka[$suka] eq $id_status[$i]}
												<span id="space-like{$id_status[$i]}" style="display:none;">
													{if $nm_pengguna_suka_1stpos[$suka]==$nm_pengguna}
													Anda
													{/if}
												</span>
												{$nm_pengguna_suka[$suka]}
												&nbsp; Menyukai ini
											{/if}

										{/for}
									</span>
								</td>
							</tr>
							<tr>
								<td class="ui-widget-content">
									<div class="btn-dislike">
										&nbsp;
										<form style="display:none;" action="sn_is_dislike.php" id="form-dislike-status{$id_status[$i]}" name="form-dislike-status{$id_status[$i]}" method="post">
											<input type="text" name="id_status" value="{$id_status[$i]}" style="display:none"/>
											<input type="text" name="id_pengguna" value="{$id_pengguna}" style="display:none;" />
										</form>
									</div>
									<span id="space-dislike{$id_status[$i]}">&nbsp;
										{for $tdksuka = 0 to $jml_data_sn_status_tdksuka}
											{if $id_sn_status_tdksuka[$tdksuka] eq $id_status[$i]}
												<span id="space-dislike{$id_status[$i]}">
													{if $nm_pengguna_tdksuka_1stpos[$tdksuka]==$nm_pengguna}
													Anda
													{/if}
												</span>
												{$nm_pengguna_tdksuka[$tdksuka]}
												&nbsp; Tidak menyukai ini
											{/if}

										{/for}
									</span>
								</td>
							</tr>
							<tr>
								<td class="ui-widget-content">
									<div class="btn-comment">
										&nbsp;
									</div>
									<span id="space-comment-count{$id_status[$i]}">&nbsp;</span>
								</td>
							</tr>


							{for $j=0 to $jml_status_balas-1}
								{if $id_status_balas[$j]==$id_status[$i]}
								<div id="comment-box{$id_status[$i]}">
								<tr id="space-comment{$id_status_balas[$j]}">
									<td class="ui-widget-content">
										<div style="float:left; width:35px; height:100%;">
											<img src="{$foto_pengguna_status[$i]}/{$username_status_balas[$j]}.JPG" onerror="ImgErrorSmall(this);" style="width:35px; height:35px;"/>
										</div>
										<div style="float:left; padding-left:10px; width:300px; text-align:justify;">
											<strong>{$nm_pengguna_status_balas[$j]}</strong> &nbsp; {$status_balas[$j]}
										</div>
									</td>
								</tr>
								</div>
								{/if}						
							{/for}
								</table>
						</div>
						{/for}	
			</div>
				
		</div>
		</section>
	</div>

