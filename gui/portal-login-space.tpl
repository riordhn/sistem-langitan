{if $id_pengguna==""}

<form class="disable-ajax" action="login.php" method="post" accept-charset="utf-8">
	
	<input type="hidden" name="mode" value="login" />
	
	<div class="ui-icon ui-icon-person" style="float:left;"></div>
	<input style="float:left;color:#000;font-weight:bold; width: 150px" type="text" name="username" id="username" autocomplete="off">
	
	<div class="ui-icon ui-icon-locked" style="float:left;"></div>
	<input style="float:left;color:#000; width: 150px" type="password" name="password" id="password" autocomplete="off">
	
	<input style="height:25px;font-size:11px;margin-left:5px;" id="btnLogin" style="float:left;" type="submit" value="Login">
	<input style="height:25px;font-size:11px;" id="btnLupaPwd" style="float:right;" type="button" value="Lupa password?" />
	
	{literal}
	<script type="text/javascript">
		jQuery(document).ready(function(){
			
			$('#btnLupaPwd').click(function(){

				// create dialog element
				var lupaPasswordDialog = document.createElement('div');

				$(lupaPasswordDialog).html('<center><iframe width="535" height="330" frameborder="0" scrolling="no" src="smsgw/"></iframe></center>');

				// Display dialog
				$(lupaPasswordDialog).dialog({
					title: 'Reset Password Cyber Campus',
					width: 575,
					height: 370,
					resizable: false,
					close: function() {
						lupaPasswordDialog.parentNode.removeChild(lupaPasswordDialog);
					}
				});
			});
			
		});
	</script>
	{/literal}
</form>
{else}
<div style="color:#FFFFFF;">
	<span>Selamat datang {$nm_pengguna}&nbsp; | </span>&nbsp;
	<span><a class="disable-ajax" style="color:#FFF; text-decoration:none;" href="{$account_dir}">My Account</a></span> |
	<span><a class="disable-ajax" style="color:#FFF; text-decoration:none;" href="#" target="_blank">My Email</a></span> |
	<span><span onclick="window.location='logout.php';" style="cursor: pointer; ">Log Out</span></span>
</div>
{/if}
