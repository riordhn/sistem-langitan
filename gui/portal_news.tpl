		<script src="js/jquery.paginate.js" type="text/javascript"></script>
		<script type="text/javascript">
		$("a").has("img").addClass("disable-ajax");
		$(function() {
		/*
			$('#article0').addClass('_current').fadeIn('slow');
			$('#article1').addClass('_current').fadeIn('slow');
			$('#article2').addClass('_current').fadeIn('slow');
			$('#article3').addClass('_current').fadeIn('slow');
			$('#article4').addClass('_current').fadeIn('slow');
			$('#article5').addClass('_current').fadeIn('slow');
			$('#article6').addClass('_current').fadeIn('slow');
			$('#article7').addClass('_current').fadeIn('slow');
			$('#article8').addClass('_current').fadeIn('slow');
			$('#article9').addClass('_current').fadeIn('slow');
			$('#article10').addClass('_current').fadeIn('slow');
		*/
			$("#paging-news").paginate({
				count 		: {$count_news},
				start 		: 1,
				display     : 5,
				border					: true,
				border_color			: '#fff',
				text_color  			: '#fff',
				background_color    	: 'black',	
				border_hover_color		: '#ccc',
				text_hover_color  		: '#000',
				background_hover_color	: '#fff', 
				images					: false,
				mouse					: 'press',
				onChange     			: function(page){
											$('._current','#news-content').removeClass('_current').fadeOut('slow');
											$('#article'+page).addClass('_current').fadeIn('slow');
										  }
			});
		});
		</script>
		{literal}
		<script type="text/javascript">var switchTo5x=true;</script>
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		{/literal}



<div class="column-news">
	
	<div class="portlet-news">
		<section>
			<div class="ui-widget-header" style="padding:3px;">News{$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun}</div>
		<div id="news-content" class="portlet-content-news ui-widget-content" style="background:#fff; padding:10px;">
			{if $get_id_news==0}
					{for $foo=0 to $jml_data_news-1}
						<article id="article{$foo}">
						<div id="title" class="title">
							<a class="disable-ajax" id="title" onclick="scrollToTop()" href="#!portal_news.php?category=news&id={$id_news[$foo]}" >{$judul_news[$foo]}</a>
						</div>
						<div class="date">
							Date : &nbsp; {$tgl_news[$foo]}
						</div>
						<div class="date">
							Oleh : &nbsp; {$nm_pengguna_link[$foo]}
						</div>
						<div class="content_news">
							{$resume_news[$foo]}
							<a class="disable-ajax" style="color:blue;" id="title" onclick="scrollToTop()" href="#!portal_news.php?category=news&id={$id_news[$foo]}" >Selanjutnya</a>
						</div>
						</article>
					{/for}
				{else}
						<article id="article{$get_id_news-1}">
						<div class="social-network">
							<span  class='st_facebook_button' displayText='Facebook'></span>
							<span  class='st_twitter_button' displayText='Tweet'></span>
							<span  class='st_email_button' displayText='Email'></span>
							<span  class='st_sharethis_button' displayText='ShareThis'></span>
							<span  class='st_plusone_button' ></span>
						</div>

						<div>
							&nbsp;
						</div>
						<div class="title">
							{$judul_news_link}
							<!--horizontal count-->

						</div>
						<div class="date">
							Date : &nbsp; {$tgl_news_link}
						</div>
						<div class="date">
							Oleh : &nbsp; {$nm_pengguna_link}
						</div>
						<div class="content_news">
							{$isi_news_link}
						</div>
						</article>
			{/if}
		</div>
		</section>
	</div>

