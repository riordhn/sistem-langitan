<!doctype html>
<html class="fixed">
	<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<meta name="keywords" content="UMAHA Management System, UMAHA Langitan, Langitan System," />
		<meta name="description" content="Langitan - UMAHA">
		<meta name="author" content="patihnambi.com">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,800,900|Shadows+Into+Light" rel="stylesheet" type="text/css">
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/vendor/animate/animate.css">
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/all.min.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/css/theme.css" />
		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/css/theme-skin.css" />
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/css/custom.css">
		<!-- Head Libs -->
		<script src="assets/vendor/modernizr/modernizr.js"></script>
        <!-- Icon -->
        <link rel="shortcut icon" href="icon-langitan-57.png" />
        <link rel="apple-touch-icon" href="icon-langitan-57.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="icon-langitan-72.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="icon-langitan-114.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="icon-langitan-144.png" />
	    <title>Sistem Langitan - {$nama_pt}</title>
	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo float-left d-none d-sm-block" style="width:65%">
					<img src="langitan/logo-langitan-{$nama_singkat}.PNG" width="100%" alt="Langitan" />
				</a>

				<div class="panel card-sign">
					<div class="card-title-sign mt-3 text-right">
						<h2 class="title text-uppercase font-weight-bold m-0"><i class="fas fa-user mr-1"></i> Forgot Password</h2>
					</div>
					<div class="card-body">
                        {if $submited}
                            {if !$status}                            
                                <div class="alert alert-danger">
                                    <p class="m-0">{$message}</p>
                                </div>
                            {else}
                                
                                <div class="alert alert-success">
                                    <p class="m-0">{$message}</p>
                                </div>
                            {/if}
                        {else}                            
						<div class="alert alert-info">
							<p class="m-0">Masukkan Username/NIM/NIK dan tanggal lahir anda, kemudian link penggantian password akan dikirim ke email<br/>
                                <b>Jika email anda di sistem tidak sesuai / email tidak terkirim silahkan hubungi DSI</b>
                            </p>
						</div>
                        {/if}
                        {if !$submited||!$status}
                            <form accept-charset="UTF-8" role="form" method="post" action="forgot-password.php">
                                <input name="mode" type="hidden" value="forgot-password">
                                <div class="form-group mb-3">
                                    <label>Username</label>
                                    <input name="username" type="text" class="form-control form-control-lg" placeholder="Username/NIM/NIK" required=""/>
                                </div>
                                <div class="form-group mb-3">
                                    <label>Tanggal Lahir</label>
                                    <input name="tgl_lahir" type="date" class="form-control form-control-lg" required="" />
                                </div>
                                <div class="row">
                                    <div class="col-sm-8">
                                        
                                    </div>
                                    <div class="col-sm-4 text-right">
                                        <button type="submit" class="btn btn-primary mt-2">Reset</button>
                                    </div>
                                </div>

                            </form>
                        {/if}                        
                        <p class="text-center mt-3"><a href="/">Kembali ke Login!</a></p>
					</div>
				</div>

				<p class="text-center text-muted mt-3 mb-3">&copy; Copyright DSI UMAHA. All Rights Reserved.</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="assets/vendor/jquery/jquery.js"></script>
		<script src="assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="assets/vendor/popper/umd/popper.min.js"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/vendor/common/common.js"></script>
		<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="assets/js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="assets/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="assets/js/theme.init.js"></script>

	</body>
</html>