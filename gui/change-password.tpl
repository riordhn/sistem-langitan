<!doctype html>
<html class="fixed">
	<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<meta name="keywords" content="UMAHA Management System, UMAHA Langitan, Langitan System," />
		<meta name="description" content="Langitan - UMAHA">
		<meta name="author" content="patihnambi.com">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,800,900|Shadows+Into+Light" rel="stylesheet" type="text/css">
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/vendor/animate/animate.css">
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/all.min.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/css/theme.css" />
		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/css/theme-skin.css" />
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/css/custom.css">
		<!-- Head Libs -->
		<script src="assets/vendor/modernizr/modernizr.js"></script>
        <!-- Icon -->
        <link rel="shortcut icon" href="icon-langitan-57.png" />
        <link rel="apple-touch-icon" href="icon-langitan-57.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="icon-langitan-72.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="icon-langitan-114.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="icon-langitan-144.png" />
	    <title>Sistem Langitan - {$nama_pt}</title>
	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo float-left d-none d-sm-block" style="width:60%">
					<img src="langitan/logo-langitan-{$nama_singkat}.PNG" width="100%" alt="Langitan" />
				</a>

				<div class="panel card-sign">
					<div class="card-title-sign mt-3 text-right">
						<h2 class="title text-uppercase font-weight-bold m-0"><i class="fas fa-user mr-1"></i> Change Password</h2>
					</div>
					<div class="card-body">
						{if !$success}
							<label class="alert alert-info mb-3">
								Demi keamanan account, Anda diharuskan mengganti password terlebih dahulu dengan ketentuan sebagai berkut : <br/>
										<b>- Syarat password baru minimal 8 karakter yang terdiri dari kombinasi huruf dan angka.</b> <br /> Contoh : l4ng1tan
							</label>
						{/if}
						{if !empty($error)}
							<label class="alert alert-danger">{$error_message}</label>
						{/if}
						{if !empty($success)}
							<label class="alert alert-success">Selamat, password anda berhasi diganti. Silahkan login</label>
							<p class="text-center mt-3"><a href="/">Kembali ke Login!</a></p>
						{/if}
						{if !$success}
							<form accept-charset="UTF-8" role="form" class="form-signin" method="post" action="auth.php?{$smarty.server.QUERY_STRING}">
								<input name="mode" type="hidden" value="change-passwd">							
								<input type="hidden" name="password_request" value="{$password_request}" />
								<input type="hidden" name="id" value="{$user->ID_PENGGUNA}" />
								<input type="hidden" name="username" value="{$user->USERNAME}" />
								<div class="form-group mb-3">
									<label>Name</label>
									<input name="name" type="text" class="form-control form-control-lg" readonly="true" value="{$user->NAMA_PENGGUNA}" />
								</div>
								<div class="form-group mb-3">
									<label>Username</label>
									<input name="name" type="text" class="form-control form-control-lg" readonly="true" value="{$user->USERNAME}"/>
								</div>
								<div class="form-group mb-3">
									<label>Email</label>
									<input name="name" type="text" class="form-control form-control-lg" readonly="true" value="{$user->EMAIL_PENGGUNA}"/>
								</div>
								<div class="form-group mb-3">
									<label>Terakhir Ganti</label>
									<input name="name" type="text" class="form-control form-control-lg" readonly="true" value="{($user->LAST_TIME_PASSWORD != '') ? strftime('%d/%m/%Y', strtotime($user->LAST_TIME_PASSWORD)) : ''}"/>
								</div>

								<div class="form-group mb-0">
									<div class="row">
										<div class="col-sm-6 mb-3">
											<label>Password</label>
											<input type="password" name="password_new" class="form-control form-control-lg" required=""/>
										</div>
										<div class="col-sm-6 mb-3">
											<label>Password Confirmation</label>
											<input type="password" name="password_new2" class="form-control form-control-lg"  required=""/>
											<input type="hidden" name="password_old" value="{$user->PASSWORD_HASH}" /></td>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-8">
										{if $password_request=='0'}																	
											<a href="logout.php" class="btn btn-warning mt-2">Logout</a>
										{/if}
									</div>
									<div class="col-sm-4 text-right">
										<button type="submit" class="btn btn-primary mt-2">Simpan</button>
									</div>
								</div>

							</form>
						{/if}

						
					</div>
				</div>

				<p class="text-center text-muted mt-3 mb-3">&copy; Copyright DSI UMAHA. All Rights Reserved.</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="assets/vendor/jquery/jquery.js"></script>
		<script src="assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="assets/vendor/popper/umd/popper.min.js"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/vendor/common/common.js"></script>
		<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="assets/js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="assets/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="assets/js/theme.init.js"></script>

	</body>
</html>