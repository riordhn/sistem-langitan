
<script>
    $(function() {
    $(".column-roster").sortable({
    connectWith: ".column"
    });
            $(".portlet-roster").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
            .find(".portlet-header-roster")
            .addClass("ui-widget-header ui-corner-all")
            .prepend("

                    < span class = 'ui-icon ui-icon-minusthick' >
                    < /span>")
                    .end()
                    .find(".portlet-content-roster");
                    $(".portlet-header-roster .ui-icon").click(function() {
            $(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
                    $(this).parents(".portlet-roster:first").find(".portlet-content-roster").toggle();
            });
            });</script> 

<script>
            $(function(){
            /*
             $('#slides').slides({
             preload: true,
             preloadImage: './api/images/portal/loading.gif',
             play: 3000,
             pause: 2000,
             hoverPause: true
             });
             */
            });

</script> 

<div class="ui-widget"> 

    <div class="ui-widget-header" style="padding:3px;"> {if $lang=='en-flg.gif'}Produk Cyber Campus Unair{else}Cyber Campus Unair Products{/if} 
    </div> 

    <div id="slider" class="ui-widget-content" style="padding-top:0px; padding-bottom:0px;"> <iframe id="sslider" src="api/slider/slider.php?id={$time}" width="575" height="420" scrolling="no"> </iframe> 
    </div> 

    <div style="clear:both;"> &nbsp; 
    </div> 

    <div class="column-right"> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : KEDOKTERAN<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="ui-widget" id="hor-zebra"> 
                    <thead class="ui-widget-header"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 1} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> <br/> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : KEDOKTERAN GIGI<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="ui-widget" id="hor-zebra"> 
                    <thead class="ui-widget-header"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 2} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> <br/> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : HUKUM<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="table-to-grid" id="hor-zebra"> 
                    <thead class="ui-widget-header"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 3} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> <br/> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : EKONOMI DAN BISNIS<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="ui-widget" id="hor-zebra"> 
                    <thead class="ui-widget-header"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 4} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> <br/> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : FARMASI<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="ui-widget" id="hor-zebra"> 
                    <thead class="ui-widget-header" id="hor-zebra"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 5} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> <br/> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : KEDOKTERAN HEWAN<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="ui-widget" id="hor-zebra"> 
                    <thead class="ui-widget-header"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 6} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> <br/> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : ILMU SOSIAL ILMU POLITIK<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="ui-widget" id="hor-zebra"> 
                    <thead class="ui-widget-header"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 7} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> <br/> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : SAINS & TEKNOLOGI<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="ui-widget" id="hor-zebra"> 
                    <thead class="ui-widget-header"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 8} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> <br/> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : PASCASARJANA<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="ui-widget" id="hor-zebra"> 
                    <thead class="ui-widget-header"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 9} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> <br/> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : KESEHATAN MASYARAKAT<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="ui-widget" id="hor-zebra"> 
                    <thead class="ui-widget-header"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 10} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> <br/> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : PSIKOLOGI<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="ui-widget" id="hor-zebra"> 
                    <thead class="ui-widget-header"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 11} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> <br/> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : ILMU BUDAYA<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="ui-widget" id="hor-zebra"> 
                    <thead class="ui-widget-header"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 12} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> <br/> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : KEPERAWATAN<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="ui-widget" id="hor-zebra"> 
                    <thead class="ui-widget-header"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 13} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> <br/> 

        <div class="portlet-roster"> 

            <div class="portlet-header"> FAKULTAS : PERIKANAN DAN KELAUTAN<br/>

                <span style="font-size:9px;font-style:italic; color:#CCC;"> {$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun} 
                </span> 
            </div> 

            <div class="portlet-content"> 
                <table class="ui-widget" id="hor-zebra"> 
                    <thead class="ui-widget-header"> 
                        <tr style="font-weight:bold; font-size:12px; height:25px; margin:10px;"> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MULAI{else}START{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}SELESAI{else}END{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KODE{else}CODE{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}MATA AJAR{else}SUBJECT{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}KELAS{else}CLASS{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}RUANGAN{else}CLASSROOM{/if} 
                            </td> 
                            <td style="padding:3px;"> {if $lang=='en-flg.gif'}JML{else}TOT{/if} 
                            </td> 
                        </tr> </thead> <tbody class="ui-widget-content" style="font-size:12px;"> {for $foo=0 to $jml_data} {if $id_fakultas[$foo] == 14} 
                        <tr> 
                            <td> {$mulai[$foo]} 
                            </td> 
                            <td> {$selesai[$foo]} 
                            </td> 
                            <td> {$kd_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nm_mata_kuliah[$foo]} 
                            </td> 
                            <td> {$nama_kelas[$foo]} 
                            </td> 
                            <td> {$nm_ruangan[$foo]} 
                            </td> 
                            <td> {$kls_terisi[$foo]} 
                            </td> 
                        </tr> {/if} {/for} </tbody> 
                </table> 
            </div> 
        </div> 
    </div> 
</div>