<div id="head">
	<div class="shell">
		<div class="slider-holder">
			<a href="#" class="btn prev"></a>
			<a href="#" class="btn next"></a>
			<div class="slider">
				<ul>
				    <li>
				    	<div class="item">
				    		<div class="info left">
				    			<h2>Gallery Campus</h2>
				    			<p>Merangkul semua pembelajar dimanapun dan kapanpun untuk berkarya di internet. </p>
				    			<p>Tunggu apa lagi, ayo berkarya bersama GalleryCampus.</p>
				    			<a href="#" class="btn-more">Read More</a>
				    		</div>
				    		<div class="image right">
				    			<img src="css/images/slide-img.jpg" alt="" />
				    		</div>
				    		<div class="cl">&nbsp;</div>
				    	</div>
				    </li>
				    <li>
				    	<div class="item">
				    		<div class="info left">
				    			<h2>Gallery Campus Web Hosting</h2>
				    			<p>Adalah layanan unggulan kami. Kami memberikan fasilitas hosting kepada mahasiswa secara cuma-cuma dengan fasilitas lengkap.</p>
				    			<p></p>
				    			<a href="#" class="btn-more">Read More</a>
				    		</div>
				    		<div class="image right">
				    			<img src="css/images/slide-img.jpg" alt="" />
				    		</div>
				    		<div class="cl">&nbsp;</div>
				    	</div>
				    </li>
				    <!--<li>
				    	<div class="item">
				    		<div class="info left">
				    			<h2>Gallery Campus E-Commerce</h2>
				    			<p>Adalah salah satu fitur unggulan yang kami tawarkan, Gallery Campus E-Commerce menawarkan jasa E-Commerce bagi pelaku bisnis dunia maya</p>
				    			<p></p>
				    			<a href="#" class="btn-more">Read More</a>
				    		</div>
				    		<div class="image right">
				    			<img src="css/images/slide-img.jpg" alt="" />
				    		</div>
				    		<div class="cl">&nbsp;</div>
				    	</div>
				    </li>-->
				</ul>
			</div>
		</div>
	</div> 
</div>

<div id="main">
	<div class="shell">
    <p><h1>Our Webs</h1></p>
		<div class="boxes">
			<div class="box-white left">
				<h2>ILMMIPA</h2>
				<img src="images/ilmmipa.png" alt="" class="left" />
				<p></p>
				<div class="cl">&nbsp;</div>
				<a href="http://ilmmipa.gallerycampus.com" class="btn-more">See more</a>
			</div>
			<div class="box-white left">
				<h2>Decision Support System Web</h2>
				<img src="images/dss.png" alt="" class="left" />
				<a href="http://ferdywd.gallerycampus.com" class="btn-more">See more</a>
			</div>
			<div class="box-white box-white-last left">
				<h2>Other Web</h2>
                <img src="images/nefi.png" alt="" class="left" />
				<!--<div class="item">
					<span>September 12th, 2010</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor conceset ornare adip. </p>
				</div>
				<div class="item item-last">
					<span>September 12th, 2010</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor conceset ornare adip. </p>
				</div>-->
				<a href="http://nefi.gallerycampus.com" class="btn-more">See more</a>
			</div>
			<div class="cl">&nbsp;</div>
		</div>
		
		<div id="content" class="left">
			<h2>A bit about the company</h2>
			<p>Gallery Campus didirikan pada tahun 2010, pertama kali bergerak dalam bidang usaha clothing line kemudian mulai merambah dunia developing web, yang pada saat ini mulai mengembangkan diri dengan memulai salah satu divisi perusahaannya yaitu Web Hosting</p>
			<p class="quote">"Berkaryalah, karena dengan berkarya dan menciptakan kreasi, kita hidup. Sebaik-baiknya usaha adalah usaha yang berguna bagi orang lain."</p>
			<p>Berkreasilah teman-teman, mari memulai karya anda bersama Gallery Campus.</p>
			<a href="#" class="btn-more">Get Started!</a>
		</div>
		<div id="sidebar" class="right">
			<h2>Testimonials</h2>
			<div class="box-quote">
				<div class="box-c">
					<p><em><strong>I'm CEO, Bitch!</strong></em></p>
				</div>
				<p class="by"><strong>Sani Iman Pribadi</strong><span><em>CEO and Founder</em></span></p>
			</div>
		</div>
		<div class="cl">&nbsp;</div>
	</div>
</div>
<div id="footer-push">&nbsp;</div>
