
		<script src="js/jquery.paginate.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(function() {
		/*
			$('#article0').addClass('_current').fadeIn('slow');
			$('#article1').addClass('_current').fadeIn('slow');
			$('#article2').addClass('_current').fadeIn('slow');
			$('#article3').addClass('_current').fadeIn('slow');
			$('#article4').addClass('_current').fadeIn('slow');
			$('#article5').addClass('_current').fadeIn('slow');
			$('#article6').addClass('_current').fadeIn('slow');
			$('#article7').addClass('_current').fadeIn('slow');
			$('#article8').addClass('_current').fadeIn('slow');
			$('#article9').addClass('_current').fadeIn('slow');
			$('#article10').addClass('_current').fadeIn('slow');
		*/
			$("#paging-article").paginate({
				count 		: {$count_article},
				start 		: 1,
				display     : 5,
				border					: true,
				border_color			: '#fff',
				text_color  			: '#fff',
				background_color    	: 'black',	
				border_hover_color		: '#ccc',
				text_hover_color  		: '#000',
				background_hover_color	: '#fff', 
				images					: false,
				mouse					: 'press',
				onChange     			: function(page){
											$('._current','#article-content').removeClass('_current').fadeOut('slow');
											$('#article'+page).addClass('_current').fadeIn('slow');
										  }
			});
		});
		</script>
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript">
				stLight.options({
						publisher:"12345"
				});
		</script>
		<script type="text/javascript">
			jQuery(document).ready(function () {
				//var href = jQuery(location).attr('href');
				//var url = jQuery(this).attr('title');
				//jQuery('#site-map-divider').html('<span class="ui-icon ui-icon-carat-1-e"></span>');
				//jQuery('#site-map-url').html('article');
			});
		</script>

<div class="column-article">
	
	<div class="portlet-article">
		<section>
			<div class="ui-widget-header" style="padding:3px;">article{$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun}</div>
			<!--
			<span><strong>Pages &nbsp; : &nbsp;</strong></span>
			<div id="paging-article">
			</div>
			-->
		<div id="article-content" class="portlet-content-article ui-widget-content" style="background:#fff; padding:10px;">
			{if $get_id_article==0}
					{for $foo=0 to $jml_data_article-1}
						<article id="article{$foo}">
						<div class="social-network">
							<span  class='st_facebook_button' displayText='Facebook'></span>
							<span  class='st_twitter_button' displayText='Tweet'></span>
							<span  class='st_email_button' displayText='Email'></span>
							<span  class='st_sharethis_button' displayText='ShareThis'></span>
							<span  class='st_plusone_button' ></span>
						</div>

						<div>
							&nbsp;
						</div>
						<div class="title">
							<a class="disable-ajax" id="title" onclick="scrollToTop()" href="index.html?category=article&id={$id_article[$foo]}#!portal_article.html?category=article&id={$id_article[$foo]}" >{$judul_article[$foo]}</a>
						</div>
						<div class="date">
							Date : &nbsp; {$tgl_article[$foo]}
						</div>
						<div class="date">
							Oleh : &nbsp; {$pengguna[$foo]}
						</div>
						<div class="content_article">
							<!--
							<img src="{$img_link[$foo]}" />
							-->
							{$resume_article[$foo]}<span><a class="disable-ajax" id="title" onclick="scrollToTop()" href="index.html?category=article&id={$id_article[$foo]}#!portal_article.html?category=article&id={$id_article[$foo]}" >Selanjutnya</a></span>
						</div>
						</article>
					{/for}
				{else}
						<article id="{$id_article_link}">
						<div class="social-network">
							<span  class='st_facebook_button' displayText='Facebook'></span>
							<span  class='st_twitter_button' displayText='Tweet'></span>
							<span  class='st_email_button' displayText='Email'></span>
							<span  class='st_sharethis_button' displayText='ShareThis'></span>
							<span  class='st_plusone_button' ></span>
						</div>

						<div>
							&nbsp;
						</div>
						<div class="title">
							{$judul_article_link}
							<!--horizontal count-->

						</div>
						<div class="date">
							Date : &nbsp; {$tgl_article_link}
						</div>
						<div class="date">
							Oleh : &nbsp; {$pengguna_link}
						</div>
						<div class="content_article">
							<!--
							<img src="{$img_link_link}" />
							-->
							{$isi_article_link}
						</div>
						</article>
			{/if}
		</div>
		</section>
	</div>
