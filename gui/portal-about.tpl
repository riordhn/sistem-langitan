<script>
	$(function() {
	
	/*
		$( ".column-about" ).sortable({
			connectWith: ".column-about"
		});
	*/

		$( ".portlet-about" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header-about" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
				.end()
			.find( ".portlet-content-about" );

		$( ".portlet-header-about .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet-about:first" ).find( ".portlet-content-about" ).toggle();
		});

		//$( ".column-about" ).enableSelection();
		
				
	});

</script>
<div class="ui-widget">
	<div class="column-about">
	<div class="portlet-about">
		<div class="ui-widget-header " style="padding:3px; padding-left:10px;">About Cyber Campus<span id="arrowPanel" style="float:right;" class="ui-icon ui-icon-arrow-4-diag"></span></div> 
		
			<div class="portlet-content-about" style="padding:10px;">
				{$about}
			</div>
			<span class="ui-icon ui-icon-grip-diagonal-se" style="float:right;"></span>
	</div>
	</div>
</div>