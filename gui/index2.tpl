<html>
    <head>
        <title>Universitas Airlangga Cyber Campus</title>
        <meta name="description" content="Cyber Campus Universitas Airlangga, Fasilitas Layanan Terpadu mengenai Informasi Akademik Universitas Airlangga Surabaya. Informasi lebih lanjut silakan kontak helpdesk di:031-60277704, 031-60277703, 031-60276766. YM!: cybercampus_unair ..." />
        <meta name="keywords" content="Cyber Campus,Cyber,Campus,Universitas Airlangga,Airlangga,Airlangga University,University,Universitas,Unair,AUCC,CCUA,Surabaya,KRS,KHS,Pendaftaran,Penerimaan,Mahasiswa,Dosen,Pegawai" />
        <meta name="author" content="Cyber Campus Universitas Airlangga" />
        <meta name="generator" content="CCUA 0.1 Final" />
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="/css/index.css" />
        <link type="text/css" rel="stylesheet" href="css/custom-theme/jquery-ui-fix-portal.css" />
        <script language="JavaScript1.2" type="text/javascript">
            <!--
        x = 20;
            y = 20;
            function setVisible(obj)
            {
                obj = document.getElementById(obj);
                obj.style.visibility = (obj.style.visibility == 'visible') ? 'hidden' : 'visible';
            }
            function placeIt(obj)
            {
                obj = document.getElementById(obj);
                if (document.documentElement)
                {
                    theLeft = document.documentElement.scrollLeft;
                    theTop = document.documentElement.scrollTop;
                }
                else if (document.body)
                {
                    theLeft = document.body.scrollLeft;
                    theTop = document.body.scrollTop;
                }
                theLeft += x;
                theTop += y;
                obj.style.left = theLeft + 'px';
                obj.style.top = theTop + 'px';
                setTimeout("placeIt('helpdesk')", 500);
            }
            window.onscroll = setTimeout("placeIt('helpdesk')", 500);
            //-->
        </script>
        <style type="text/css">
            #helpdesk {
                position: absolute;
                visibility: hidden;
                width: 512px;
                /*height: 120px;*/
                left: 20px;
                top: 20px;
                background-color: #ccccff;
                border-radius: 5px 10px 5px 10px / 10px 5px 10px 5px;
                border-radius: 5px;
                border-radius: 5px 10px / 10px; 
                font-family: "Trebuchet MS";
                padding: 10px;
            }
            #close {
                float: right;
            }
        </style>
        <!--
        <script src="/js/jquery-1.5.1.min.js" type="text/javascript"></script>
        -->
        <script src="js/?f=jquery-1.6.2.min.js"></script> 
        <script type="text/javascript" src="js/jquery-ui-fix-portal.min.js"></script> 
        {literal}<script type="text/javascript">
            $(document).ready(function() {

                $('.show-captcha').click(function() {
                    $(this).css('display', 'none');
                    $('.username-box').css('display', 'none');
                    $('.password-box').css('display', 'none');
                    $('input[type="submit"]').css('display', 'block');
                    $('.recaptcha-box').show();
                    return false;
                });
            });
            </script>{/literal}
        </head>

        <body>
            <!-- {$false_login} -->
            <form action="login.php" method="post">
                <input type="hidden" name="mode" value="login" />
                <div class="wrapper">
                    <div class="login-box">
                        {if $false_login > 3}
                            <div class="recaptcha-box" style="display: none;">    
                                {$recaptcha}
                            </div>
                        {/if}
                        <div class="username-box">
                            <label>USERNAME</label>
                            <input type="text" name="username" />
                        </div>
                        <div class="password-box">
                            <label>PASSWORD</label>
                            <input type="password" name="password" />
                        </div>
                        <div class="submit-box">
                            {if $false_login <= 3}
                                <input type="button" value="Helpdesk!" onclick="setVisible('helpdesk')" />
                                <input style="height:25px;font-size:11px;" id="btnLupaPwd" style="float:right;" type="button" value="Lupa password?" />
                                <input type="submit" value="Login" />
                                
                            {else}
                                <button class="show-captcha">Login</button>
                                
                                <input type="submit" value="Login" style="display: none;" />
                            {/if}
                        </div>  
                    </div>
                </div>
            </form>
            <div id="helpdesk">
                <span id="close"><a href="javascript:setVisible('helpdesk')"><img src="/img/login/red_close.png"></a></span>
                <p><u>Helpdesk Cyber Campus Universitas Airlangga:</u><br/>
                Telepon: 031-71996177, 031-60511576, 088801414214<br/>
                - Facebook	: Dsi Helpdesk I<br />
                - YM	 : dsi.helpdesk1@yahoo.com ; dsi.helpdesk2@yahoo.com<br />
                - Phone	 : 031-91449298<br />
                - Facebook	: Dsi Helpdesk II<br />
                - YM	 : dsi.helpdesk05@yahoo.com<br />
                - Phone	 : 031- 60511576<br /><br />

                - Facebook	: Dsi Helpdesk III<br />
                - YM : dsi.helpdesk03<br />
                - Phone : 031-71996177<br /><br />

                - Facebook : Dsi Helpdesk IV<br />
                - YM : dsi.helpdesk04<br />
                - Phone : 088801414214<br /><br />

                Official Email: helpdesk@ditsi.unair.ac.id<br /> 
                Official Twitter: ITHelpdeskUNAIR<br />
            </div>
            <div id="pengumuman"><!--Server akan maintenance tgl 9/9/2011 jam 12:30 s.d. 12:45 WIB ...--></div>
            <!-- google analitycs menggunakan fathoni@staf.unair.ac.id -->
            {literal}
                
                <script>
                    /*
                    (function(i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function() {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                    ga('create', 'UA-41992721-1', 'unair.ac.id');
                    ga('send', 'pageview');
                    */
                </script>
            {/literal}
            {literal}
                <script type="text/javascript">
                    jQuery(document).ready(function() {

                        $('#btnLupaPwd').click(function() {
                            
                            console.log("Hello");

                            // create dialog element
                            var lupaPasswordDialog = document.createElement('div');

                            $(lupaPasswordDialog).html('<iframe height="300" frameborder="0" scrolling="no" src="emailpass/"></iframe>');

                            // Display dialog
                            $(lupaPasswordDialog).dialog({
                                title: 'Reset Password Account Cybercampus',
                                width: 500,
                                height: 360,
                                resizable: false,
                                close: function() {
                                    lupaPasswordDialog.parentNode.removeChild(lupaPasswordDialog);
                                }
                            });
                        });

                    });
                </script>
            {/literal}
        </body>
    </html>