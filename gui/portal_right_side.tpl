<script>
	$(function() {
		$( ".column-right" ).sortable({
			connectWith: ".column-right"
		});

		$( ".portlet-right" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header-right" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
				.end()
			.find( ".portlet-content-right" );

		$( ".portlet-header-right .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet-right:first" ).find( ".portlet-content-right" ).toggle();
		});

		$( ".column" ).disableSelection();
		
	});

</script>
<div class="column-right">
	<!--
	<div class="portlet-right">
		<div class="ui-widget-header" style="padding:3px;">Pengunjung Portal</div>
		<div style="margin:10px;" >
			<div class="title" style="padding-left:10px; padding-right:10px; font-size:18px;">Total : {$counter}</div> 
		</div>
	</div>
	<br />
	-->
        
	<div class="portlet-right">
		<div class="ui-widget-header" style="padding:3px;">Sertifikasi dan Prestasi</div>
		<div style="margin:3px;" >
			<div class="title" style="padding-left:4px; padding-right:5px; font-size:18px;"><img src="/images/iso27001.jpg"></div> 
		</div>
	</div>
	<br />
	<div class="portlet-right">
            <div class="ui-widget" style="padding:3px;">
                <a href="http://ua.unair.ac.id/radio-universitas-airlangga.html" target="blank" >
                    <img width="175px;" src="http://ua.unair.ac.id/images/radiologo.jpg" />
                </a>
            </div>
	</div>
        <br />
	
	<div class="portlet-right">
	
	<div class="ui-widget-header" style="padding:3px;">Testimoni Cyber Campus</div>
	<div class="portlet-content-right"> {if $jml_data_blog < 5} {for $foo=0 to $jml_data_blog-1} 
	<div class="title" style="padding-left:10px; padding-right:10px;">
	<a href="{$link_blog[$foo]}" target="_blank" class="disable-ajax" >{$judul_blog[$foo]}</a>
	</div>
	<div class="date" style="padding-left:10px; padding-right:10px;"> Date : {$tgl_blog[$foo]} </div>
	<div class="date" style="padding-left:10px; padding-right:10px;"> Oleh : {$nm_pengguna_blog[$foo]} </div>
	<div class="resume">
		<table class="ui-widget"><tr><td valign="top" style="width:50px;"><img style="width:50px; height:50px;" src="{$img_link_blog[$foo]}"/></td><td style="padding-right:10px;" valign="top">{$resume_blog[$foo]}</td></tr>
		</table>
	</div> {/for} {else} {for $foo=0 to 2} 
	<div class="title" style="padding-left:10px; padding-right:10px;">
	<a href="{$link_blog[$foo]}" target="_blank" class="disable-ajax" >{$judul_blog[$foo]}</a>
	</div>
	
	<div class="date" style="padding-left:10px; padding-right:10px;"> Date : {$tgl_blog[$foo]} </div><div class="date" style="padding-left:10px; padding-right:10px;"> Oleh : {$nm_pengguna_blog[$foo]} </div><div class="resume"><table class="ui-widget"><tr><td valign="top" style="width:50px;"><img style="width:50px; height:50px;" src="{$img_link_blog[$foo]}"/></td><td style="padding-right:10px;" valign="top">{$resume_blog[$foo]}</td></tr></table></div> {/for} {/if} </div></div><br /><div class="portlet-right"><div class="ui-widget-header" style="padding:3px;">Latest News</div><div style="margin:10px;" ><div class="portlet-content-right"> {if $jml_data_news<5} {for $foo=0 to $jml_data_news-1} <div id="title" class="title"><a class="disable-ajax" id="title" onclick="scrollToTop()" href="index.php?category=news&id={$id_news[$foo]}#!portal_news.php?category=news&id={$id_news[$foo]}" >{$judul_news[$foo]}</a></div><div class="date"> Date : &nbsp; {$tgl_news[$foo]} </div><div class="date"> Oleh : &nbsp; {$nm_pengguna_news[$foo]} </div><div class="resume"> {$resume_news[$foo]} </div> {/for} {else} {for $foo=0 to 4} <div id="title" class="title"><a id="title" onclick="scrollToTop()" href="#!portal_news.php?category=news&id={$id_news[$foo]}" >{$judul_news[$foo]}</a></div><div class="date"> Date : &nbsp; {$tgl_news[$foo]} </div><div class="date"> Oleh : &nbsp; {$nm_pengguna_news[$foo]} </div><div class="resume"> {$resume_news[$foo]} </div> {/for} {/if} </div></div></div> {literal} <script>
		function scrollToTop()
		{
			$('body').animate({scrollTop: $("body").offset().top},'medium');
		}
	</script> {/literal} <br /><div class="portlet-right"><div class="ui-widget-header" style="padding:3px;">Latest Article</div><div style="margin:10px;" ><div class="portlet-content-right"> {for $foo=0 to $jml_data_article-1} <div id="title" class="title"><a class="disable-ajax" id="title" onclick="scrollToTop()" href="index.php?category=article&id={$id_article[$foo]}#!portal_article.php?category=article&id={$id_article[$foo]}" >{$judul_article[$foo]}</a></div><div class="date"> Date : &nbsp; {$tgl_article[$foo]} </div><div class="date"> Oleh : &nbsp; {$nm_pengguna_article[$foo]} </div><div class="resume"> {$resume_article[$foo]} </div> {/for} </div></div></div><br /></div>