<script>
	$(function() {
		/*
		$( ".column-left" ).sortable({
			//connectWith: ".column-left"
		});
		*/

		$( ".portlet-left" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header-left" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
				.end()
			.find( ".portlet-content-left" );

		$( ".portlet-header-left .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet-left:first" ).find( ".portlet-content-left" ).toggle();
		});

		//$( ".column" ).disableSelection();
		
	});

</script><div class="column-left"><div class="portlet-left"><div class="ui-widget-header" style="padding:3px;" >{if $lang=='en-flg.gif'}Distribusi Nilai{else}Distribution Of Values{/if}</div><div class="portlet-content"> {for $foo=0 to 13} {if $nm_fakultas[$foo]!=""} <div class="title"> {if $lang=='en-flg.gif'}FAKULTAS{else}FACULTY{/if} : {$nm_fakultas[$foo]} </div><div class="persentase"><table style="width:100%; border:none; font-weight:bold;" border="0" id="hor-zebra"><tr><td style="width:20px;">A</td><td>&nbsp;:&nbsp; {$nilai_A[$foo]}%</td></tr><tr><td>AB</td><td>&nbsp;:&nbsp;{$nilai_AB[$foo]}%</td><tr><td>B</td><td>&nbsp;:&nbsp;{$nilai_B[$foo]}%</td></tr><tr><td>BC</td><td>&nbsp;:&nbsp;{$nilai_BC[$foo]}%</td></tr><tr><td>C</td><td>&nbsp;:&nbsp;{$nilai_C[$foo]}%</td></tr><tr><td>D</td><td>&nbsp;:&nbsp;{$nilai_D[$foo]}%</td></tr><tr><td>E</td><td>&nbsp;:&nbsp;{$nilai_E[$foo]}%</td></tr></table></div> {/if} {/for} </div></div></div> 