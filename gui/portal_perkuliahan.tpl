<script type="text/javascript">
		$(document).ready(function() {

			$().ajaxStart(function() {
				$('#loading').show();
				$('#result').hide();
			}).ajaxStop(function() {
				$('#loading').hide();
				$('#result').fadeIn('slow');
			});
			
			
			//event submit function
			$('#myForm').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {
						$('#result').html(data);
						$('#result').fadeIn('slow');
					}
				})
				return false;
			});
			
			//event click function
			
			$('#radio1').click(function() {
				$('#hari').hide();
			});
			
			$('#radio2').click(function() {
				$('#hari').show();
			});
			
		})
</script>

<script>
	$(function() {
		$( "#radio" ).buttonset();
	});
</script>


<script>
function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
	}
	
	
	
	function getProgramStudi(strURL) {		
		
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('program_studi_div').innerHTML=req.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
				
	}

</script>
{literal}
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
{/literal}
<div class="column-news">
	
	<div class="portlet-news">
		<section>
			<div class="ui-widget-header" style="padding:3px;">JADWAL PERKULIAHAN UNIVERSITAS AIRLANGGA</div>

		<div id="news-content" class="portlet-content-news ui-widget-content" style="background:#fff; padding:10px;">
			<div class="social-network">
				<span  class='st_facebook_button' displayText='Facebook'></span>
				<span  class='st_twitter_button' displayText='Tweet'></span>
				<span  class='st_email_button' displayText='Email'></span>
				<span  class='st_sharethis_button' displayText='ShareThis'></span>
				<span  class='st_plusone_button' ></span>
			</div>
			<br />
			
			<form method="post" id="myForm" name="myForm" action="portal_get_roster.php" style="border-width:1px;border-color:#000;border-style:solid;">
			<table width="100%" cellpadding="1" cellspacing="1" class="ui-widget">
				<tbody class="ui-widget">
				<tr>
					<td id="hari">
						<select name="hari" onChange="javascript:$('#myForm').submit()">
						{for $i=0 to $jml_jadwal_hari-1}
						<option value="{$id_jadwal_hari[$i]}">{$nm_jadwal_hari[$i]}</option>
						{/for}
						<option selected value="">Pilih Hari</option>
						</select>
					</td>
					<td>
						<select name="fakultas" onChange="getProgramStudi('portal_get_prodi.php?id_fakultas='+this.value)" >

						{for $i=0 to $jml_fakultas-1}
						<option value="{$id_fakultas[$i]}">{$nm_fakultas[$i]}</option>
						{/for}
						<option selected value="">Pilih Fakultas</option>
						</select>
					</td>

					<td>
						<div id="program_studi_div">
						</div>
					</td>
				</tr>
				</tbody>
			</table>

			</form>
			
			<br />
			<div id="result">
			</div>
		</div>

		</section>
	</div>