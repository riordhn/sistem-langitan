<script>
	$(function() {
		$( ".column-blog" ).sortable({
			connectWith: ".column-blog"
		});

		$( ".portlet-blog" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header-blog" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
				.end()
			.find( ".portlet-content-blog" );

		$( ".portlet-header-blog .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet-blog:first" ).find( ".portlet-content-blog" ).toggle();
		});

		//$( ".column" ).disableSelection();
		
	});

</script>
<div>
	<div class="portlet-blog">
		<div class="ui-widget-header" style="padding:3px;">Blog Mahasiswa</div>
		<div>
			{for $foo=0 to $jml_data_blog-1}
			<div class="title disable-ajax">
				<a  href="{$link_blog[$foo]}" target="_blank" class="disable-ajax" >{$judul_blog[$foo]}</a>
			</div>
			<div class="date">
				Date : {$tgl_blog[$foo]}
			</div>
			<div class="date">
				Oleh : {$nm_pengguna}
			</div>
			<div class="resume">
				<table>
					<tr>
						<td style="width:100px;"><img style="width:100px; height:100px;" src="{$img_link_blog[$foo]}"/></td>
						<td valign="top">{$resume_blog[$foo]}</td>
					</tr>
				</table>
			</div>
			{/for}
		</div>
	</div>
</div>