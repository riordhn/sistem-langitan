<?php
require '../../config.php';

	// Panggil Class Perguruan Tinggi
	$PT = new PerguruanTinggi($db);

	$id_pt = $PT->id_perguruan_tinggi;
	$nama_singkat = $PT->nama_singkat;
	$nama_pt = $PT->nama_pt;
	$alamat = $PT->alamat;
	$kota_pt = $PT->kota_pt;
	$telp_pt = $PT->telp_pt;
	$fax_pt = $PT->fax_pt;
	$web_email_pt = $PT->web_email;
	$web_elearning = $PT->web_elearning;
	$web_pmb = $PT->web_pmb;



	$smarty->assign("id_pt",$PT->id_perguruan_tinggi);
	$smarty->assign("nama_pt",$PT->nama_pt);
	$smarty->assign("base_url",$PT->base_url);
	$smarty->assign("nama_singkat",$PT->nama_singkat);
	$smarty->assign("alamat",$PT->alamat);
	$smarty->assign("kota_pt",$PT->kota_pt);
	$smarty->assign("web_email_pt",$PT->web_email);
	$smarty->assign("web_elearning",$PT->web_elearning);
	$smarty->assign("web_pmb",$PT->web_pmb);


	function get($name, $default_value = '')
	{
	    return isset($_GET[$name]) ? $_GET[$name] : $default_value;
	}


	$mode = get('mode','index');
	$jenis = get('jenis');

	if($mode == 'index'){
		$coba = 1;
		$smarty->assign('coba', $coba);

		$fakultas_set = $db->QueryToArray("
            select id_fakultas, nm_fakultas, singkatan_fakultas 
            from fakultas
            where id_perguruan_tinggi = {$id_pt}
            order by nm_fakultas");
        $smarty->assign('fakultas_set', $fakultas_set);

        $fakultas_set_js = $db->QueryToArray("
            select nm_fakultas as name, 250 as y
            from fakultas
            where id_perguruan_tinggi = {$id_pt}
            order by nm_fakultas");
        $smarty->assign('fakultas_set_js', json_encode($fakultas_set_js));
	}

$smarty->display("{$mode}.tpl");