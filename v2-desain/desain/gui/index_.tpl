<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>{$nama_pt}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
{literal}
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
{/literal}
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
{literal}
<!--js-->
<script src="js/jquery-2.1.1.min.js"></script> 
{/literal}
<!--icons-css-->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
{literal}
<!--static chart-->
<script src="js/Chart.min.js"></script>
<!--//charts-->
<!-- geo chart -->
    <script src="//cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
    <script>window.modernizr || document.write('<script src="lib/modernizr/modernizr-custom.js"><\/script>')</script>
    <!--<script src="lib/html5shiv/html5shiv.js"></script>-->
     <!-- Chartinator  -->
    <script src="js/chartinator.js" ></script>
    <script type="text/javascript">
        jQuery(function ($) {

            var chart3 = $('#geoChart').chartinator({
                tableSel: '.geoChart',

                columns: [{role: 'tooltip', type: 'string'}],
         
                colIndexes: [2],
             
                rows: [
                    ['China - 2015'],
                    ['Colombia - 2015'],
                    ['France - 2015'],
                    ['Italy - 2015'],
                    ['Japan - 2015'],
                    ['Kazakhstan - 2015'],
                    ['Mexico - 2015'],
                    ['Poland - 2015'],
                    ['Russia - 2015'],
                    ['Spain - 2015'],
                    ['Tanzania - 2015'],
                    ['Turkey - 2015']],
              
                ignoreCol: [2],
              
                chartType: 'GeoChart',
              
                chartAspectRatio: 1.5,
             
                chartZoom: 1.75,
             
                chartOffset: [-12,0],
             
                chartOptions: {
                  
                    width: null,
                 
                    backgroundColor: '#fff',
                 
                    datalessRegionColor: '#F5F5F5',
               
                    region: 'world',
                  
                    resolution: 'countries',
                 
                    legend: 'none',

                    colorAxis: {
                       
                        colors: ['#679CCA', '#337AB7']
                    },
                    tooltip: {
                     
                        trigger: 'focus',

                        isHtml: true
                    }
                }

               
            });                       
        });
    </script>
<!--geo chart-->

<!--skycons-icons-->
<script src="js/skycons.js"></script>
<!--//skycons-icons-->
{/literal}
</head>
<body>	

<div class="page-container">

<H1 style="text-align:center; background-color: #15EFA6"><br> DASBOARD SISTEM LANGITAN <br>
 {$nama_pt|upper} </H1><br>
 
</div>
<!--inner block start here-->
<hr>
<div class="clearfix"></div>
<h3 style="text-align:center;"> Status Mahasiswa</h3>
<hr>
<div class="clearfix"></div>
<!--market updates updates-->
	 <div class="market-updates">
			<div class="col-md-3 market-update-gd">
			<a href="index.php?mode=mhs-aktif--Mahasiswa aktif.html" class="hvr-shrink">
				<div class="market-update-block clr-block-1">
					<div class="col-md-20 market-update-left">
						<h3 style="text-align:center;">2503</h3>
						<h4 style="text-align:center;">MAHASISWA AKTIF</h4>
						</a>
					</div>
					
				  <div class="clearfix"> </div>
				</div>
			</div>
			<div class="col-md-3 market-update-gd">
				<div class="market-update-block clr-block-3">
				 <div class="col-md-15 market-update-left">
				 <a href="index.php?mode=mhs-perwalian--mahasiswaperwalian.html" class="hvr-shrink">
					<h3 style="text-align:center;">2300</h3>
					<h4 style="text-align:center;">MAHASISWA PERWALIAN</h4>
				</a>
				  </div>
					
				  <div class="clearfix"> </div>
				</div>
			</div>
			<div class="col-md-2 market-update-gd">
				<div class="market-update-block clr-block-2">
				<a href="index.php?mode=mhs-ipk--.html" class="hvr-shrink">
					<div class="col-md-12 market-update-left">
						<h3 style="text-align:center;">2.32</h3>
						<h4 style="text-align:center;">IPK RATA-RATA</h4>
						
					</div>
					</a>
				  <div class="clearfix"> </div>
				</div>
				</div>
				<div class="col-md-4 market-update">
				<div class="market-update-block clr-block-3">
					<div class="col-md-8 market-update-left">
					<a href="index.php?mode=mhs-sekolah--.html" class="hvr-grow">
						<h4 style="text-align:left;">		
							 1. SMK YPM 1 SEPANJANG </h4>
							 <h4>2. SMK YPM 2</h4>          
							 <h4>3.  SMK YPM 3</h4>          
							 <h4>4.  SMA yang lainnya</h4>
						</a>
						
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		
<div class="clearfix"> </div>
				</div>
		

                       
<hr>
<!--main page chart start here-->
<div class="main-page-charts">
   <div class="main-page-chart-layer1">
   <div class="col-md-6 chart-layer1">
    		<div class="chart-other">
    		
	    		<h3>Mahasiswa</h3> 	
	    		<br>	
	    		
				<canvas id="pie" height="315" width="470" style="width: 470px; height: 315px;"></canvas>
								{literal}
								<script>
									var pieData = [
										
										{
											label :"Teknik",
											value: 30,
											color:"#337AB7",
											src : "datafakteknik.html"
										
										},
										{
											label :"Ekonomi",
											value : 50,
											color : "#FC8213"
										},
										{
											label : "Hukum",
											value : 100,
											color : "#8BC34A"
										},
										{
											label :"FKES",
											value : 80,
											color : "	#00FFFF"

										}
									
									];
									new Chart(document.getElementById("pie").getContext("2d")).Pie(pieData);
								</script>
							    {/literal}
								
								<center><div class="footer">
                                {foreach $fakultas_set as $fak}
								    <a href="index.php?mode=mhs-aktif-prodi&id_fakultas={$fak.ID_FAKULTAS}--datafakteknik.html">
                                    <i class=" fa fa-circle" style="font-size: 18px; color: #337AB7 "> {$fak.NM_FAKULTAS} </i>
                                    </a>
                                {/foreach}
								<!-- <a href="datafakekonomi.html"><i class=" fa fa-circle" style="font-size: 18px; color: #FC8213 "> EKONOMI </i></a>
								<a href="datafakhhukum.html"><i class=" fa fa-circle" style="font-size: 18px; color: #8BC34A "> HUKUM </i></a>
								<a href="datafakkes.html"><i class=" fa fa-circle" style="font-size: 18px; color: #00FFFF "> FKES </i></a> -->
                       </div></center>

                      
            </div>
    	</div>
    	
		<div class="col-md-5 chart-layer1-left"> 
			<div class="glocy-chart">
			<div class="span-2c">  
			
                        <h3 class="tlt">Mahasiswa KRS</h3>
                        <canvas id="bar" height="300" width="400" style="width: 200px; height: 200px;"></canvas>
                        {literal}
                        <script>
                            var barChartData = {
                            labels : ["FKES","Ekonomi","Teknik","Hukum"],
                            datasets : [
                                {
                                    
                                    label :"Sudah KRS",
                                    data : [65,59,90,81],
                                    fillColor : "#FC8213"
   
                                },
                                {
                                	label :"belum KRS",
                                    fillColor : "#337AB7",
                                    data : [28,48,40,19]
                                }
                            ]

                        };
                            new Chart(document.getElementById("bar").getContext("2d")).Bar(barChartData);

                        </script>
                        {/literal}
                        
                        <div class ="footer">
                    	
                    	<i class=" fa fa-square" style="font-size: 14px; color: #FC8213 "> Sudah KRS</i></a>

                    	<i class=" fa fa-square" style="font-size: 14px; color: #337AB7 "> Belum KRS</i>
                    	
                    	</div><center>
                    	|<a href="datakrsfkes.html"> FKES </a>|
                         |<a href="datakrsteknik.html"> TEKNIK </a>|
                          |<a href="datakrsekonomi.html"> EKONOMI </a>|
                           |<a href="datakrshukum.html"> HUKUM </a>|		
                    </div> 			  </center>		   			
			
			</div>

			<div class="clearfix"> </div>
    	</div>
		</div>
		 <div class="clearfix"> </div>
		</div>
		</div>

		<hr>

		

<!--main page chart layer2-->
<h3 style="text-align:center;"> Status Pengajaran Dosen</h3>
<hr>

<div class="chart-layer-2">
	
	<div class="col-md-6 chart-layer1-left"> 
			<div class="glocy-chart">
			<div class="span-2c"> 
			<a href="datamengajardosen.html" class="hvr-shrink"> 
                        <h3 class="tlt">Jumlah SKS mengajar dosen</h3>
                        <canvas id="dosen" height="300" width="400" style="width: 400px; height: 300px;"></canvas>
                        {literal}
                        <script>
                            var barChartData = {
                            labels : ["<8 SKS","8 SKS","8-12 SKS",">12 SKS"],
                            datasets : [
                                                                
                                
                                {
                                    fillColor : "#337AB7",
                                    data : [28,48,40,19]
                                }
                            ]

                        };
                            new Chart(document.getElementById("dosen").getContext("2d")).Bar(barChartData);

                        </script>
                        {/literal}
                    </div> <center>	
                    |</a><a href="data.html"> < 8 SKS </a>|
                    |</a><a href="data.html">  8 SKS </a>|
                    |</a><a href="data.html">  8-12 SKS </a>|
                    |</a><a href="data.html">  >12 SKS </a>|
                    </center>
			</div>
			</div>
	<div class="col-md-6 chart-layer1-left"> 
			<div class="glocy-chart">
			<div class="span-2c">  
			
                        <h3 class="tlt">Jenjang Pendidikan Dosen</h3>
                        <br>
                        <canvas id="Jenjang" height="400" width="300" style="width: 500px; height: 300px;"></canvas>
                        {literal}
                        <script>
                            var jenjangChartData = {
                            labels : ["FKES{/literal}{$coba}{literal}","Ekonomi","Teknik","Hukum"],
                            datasets : [
                                {
                                    label : "S1",
                                    fillColor : "#FF4500",
                                    data : [8,4,4,9]
                                },
                                {
                                    label : "S2",
                                    fillColor : "#0000CD",
                                    data : [8,18,20,12]
                                },                                
                                
                                {
                                    label : "S3",
                                    fillColor : "#00FF00",
                                    data : [2,8,0,1]
                                }
                            ]

                        };
                            new Chart(document.getElementById("Jenjang").getContext("2d")).Bar(jenjangChartData);

                        </script>
                        {/literal}
                        <center>
                    	<div class="footer">
                    	<div class ="legend">
                    	<a href="datadosens1.html"><i class=" fa fa-square" style="font-size: 18px; color: #FF4500 "> S1</i></a>
                    	<a href="datadosens2.html"><i class=" fa fa-square" style="font-size: 18px; color: #0000CD"> S2</i></a>
                    	<a href="datadosens3.html"><i class=" fa fa-square" style="font-size: 18px; color: #00FF00"> S3</i></a>
                    	</div>		  		   			
                    	</div></center>
			</div>
			</div>
	</div>
  <div class="clearfix"> </div>





<!--main page chart layer2-->
<hr>
<hr>

<div class="chart-layer-2">
	
	<div class="col-md-6 chart-layer1-left"> 
			<div class="glocy-chart">
			<div class="span-2c">
			<a href="jabatandosen.html" class="hvr-shrink">  
                        <h3 class="tlt">Jabatan Fungsional Dosen</h3>
                        <canvas id="jf" height="300" width="400" style="width: 400px; height: 300px;"></canvas>
                        {literal}
                        <script>
                            var barChartData = {
                            labels : ["belum ada data","Asisten ahli","Lektor","Guru Besar","Lektor kepala"],
                            datasets : [
                                                                
                                
                                {
                                    fillColor : "#6A5ACD",
                                    data : [28,60,48,40,19]
                                }
                            ]

                        };
                            new Chart(document.getElementById("jf").getContext("2d")).Bar(barChartData);

                        </script>
                        {/literal}
                    </div> 			  		   			
			</a>
			</div>
			</div>
	<div class="col-md-6 chart-layer1-left"> 
			<div class="glocy-chart">
			<div class="span-2c"> 
			<a href="dataakreditasi.html" class="hvr-shrink"> 
                        <h3 class="tlt">Status Program Studi</h3>
                        <canvas id="studi" height="300" width="400" style="width: 400px; height: 300px;"></canvas>
                        {literal}
                        <script>
                            var barChartData = {
                            labels : ["A","B","C","Persiapan Akreditasi","Proses BAN PT"],
                            datasets : [
                                {
                                    fillColor : "#FF6347",
                                    data : [10,8,4,4,9]
                                }
                                
                                
                            ]

                        };
                            new Chart(document.getElementById("studi").getContext("2d")).Bar(barChartData);

                        </script>
                        {/literal}
                    			  		   			
			</div>
			</a>
			</div>
	</div>
  <div class="clearfix"> </div>


<!--climate end here-->
</div>
<!--inner block end here-->
<!--copy rights start here-->
<div class="copyrights">
	 <p>© 2016. All Rights Reserved </p>
</div>	
<!--COPY rights end here-->
</div>
</div>
<!--slider menu-->
{literal}
<!--slide bar menu end here-->
<script>
var toggle = true;
            
$(".sidebar-icon").click(function() {                
  if (toggle)
  {
    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
    $("#menu span").css({"position":"absolute"});
  }
  else
  {
    $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
    setTimeout(function() {
      $("#menu span").css({"position":"relative"});
    }, 400);
  }               
                toggle = !toggle;
            });
</script>
<!--scrolling js-->
		<script src="js/jquery.nicescroll.js"></script>
		<script src="js/scripts.js"></script>
		<!--//scrolling js-->
<script src="js/bootstrap.js"> </script>
<!-- mother grid end here-->
{/literal}
</body>
</html>                     