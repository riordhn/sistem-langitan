<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>{$nama_pt}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
{literal}
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
{/literal}
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
{literal}
<!--js-->
<script src="js/jquery-2.1.1.min.js"></script> 
{/literal}
<!--icons-css-->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
{literal}
<!--static chart-->
<script src="js/Chart.min.js"></script>
<!--//charts-->
<!-- geo chart -->
    <script src="//cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
    <script>window.modernizr || document.write('<script src="lib/modernizr/modernizr-custom.js"><\/script>')</script>
    <!--<script src="lib/html5shiv/html5shiv.js"></script>-->
     <!-- Chartinator  -->
    
<!--geo chart-->

<!--skycons-icons-->
<script src="js/skycons.js"></script>
<!--//skycons-icons-->
{/literal}
</head>
<body>	

<div>

<H1 style="text-align:center; background-color: #15EFA6"><br> DASBOARD SISTEM LANGITAN <br>
 {$nama_pt|upper} </H1><br>
 
</div>
<!--inner block start here-->
<hr>

<h3 style="text-align:center;"> Status Mahasiswa</h3>
<hr>
<div class="clearfix"></div>
<!--market updates updates-->
	 <div class="market-updates">
			<div class="col-md-3 market-update-gd">
			<a href="Mahasiswa/Mahasiswa aktif.html" class="hvr-shrink">
				<div class="market-update-block clr-block-1">
					<div class="col-md-20 market-update-left">
						<h3 style="text-align:center;">2503</h3>
						<h4 style="text-align:center;">MAHASISWA AKTIF</h4>
						</a>
					</div>
					
				  <div class="clearfix"> </div>
				</div>
			</div>
			<div class="col-md-3 market-update-gd">
				<div class="market-update-block clr-block-3">
				 <div class="col-md-15 market-update-left">
				 <a href="Mahasiswa/mahasiswaperwalian.html" class="hvr-shrink">
					<h3 style="text-align:center;">2300</h3>
					<h4 style="text-align:center;">MAHASISWA PERWALIAN</h4>
				</a>
				  </div>
					
				  <div class="clearfix"> </div>
				</div>
			</div>
			<div class="col-md-2 market-update-gd">
				<div class="market-update-block clr-block-2">
				<a href="Mahasiswa/dataipkmhs.html" class="hvr-shrink">
					<div class="col-md-12 market-update-left">
						<h3 style="text-align:center;">2.32</h3>
						<h4 style="text-align:center;">IPK RATA-RATA</h4>
						
					</div>
					</a>
				  <div class="clearfix"> </div>
				</div>
				</div>
				<div class="col-md-4 market-update">
				<div class="market-update-block clr-block-4">
					<div class="col-md-8 market-update-left">
					<a href="Mahasiswa/asalsekolah.html" class="hvr-grow">
						<h4 style="text-align:left;">		
							 1. SMK YPM 1 SEPANJANG </h4>
							 <h4>2. SMK YPM 2</h4>          
							 <h4>3.  SMK YPM 3</h4>          
							 <h4>4.  SMA yang lainnya</h4>
						</a>
						
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		
<div class="clearfix"> </div>
				</div>
		

                       
<hr>
<!--main page chart start here-->
<div class="main-page-charts">
   <div class="main-page-chart-layer1">
   <div class="col-md-6 chart-layer1">
    		<div class="chart-other">
    		
	    		<h3>Mahasiswa</h3> 	
	    		<br>	
	    		{literal}
				<script src="js/chartpie.js"></script>
				<script src="js/exporting.js"></script>
                {/literal}
				<div id="mahasiswa" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto "></div>
				{literal}
				<script>
                //var fakultas = {$fakultas_set_js};

				Highcharts.chart('mahasiswa', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'GRAFIK MAHASISWA PER FAKULTAS'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        
                        name: 'Brands',
                        colorByPoint: true,
                        data: 
                        //{$fakultas_set_js} 
                        [{
                       
                            name: 'FKES',
                            y: 300
                            
                        }, {
                            name: 'TEKNIK',
                            y: 200,
                            sliced: true,
                            selected: true
                        }, {
                            name: 'EKONOMI',
                            y: 400
                        }, {
                            name: 'HUKUM',
                            y: 100
                        },]
                    }]
                });				
                </script>
                {/literal}

                {foreach $fakultas_set as $fak}
                    <a href="index.php?mode=mhs-aktif-prodi&jenis=mhs&id_fakultas={$fak.ID_FAKULTAS}" class= "btn btn-default" role= "button">{$fak.NM_FAKULTAS}</a>
                {/foreach}

<!-- <a href="Mahasiswa/datafakkes.html" class= "btn btn-default" role= "button"> DATA FKES</a>
<a href="Mahasiswa/datafakhhukum.html" class= "btn btn-default" role= "button"> DATA HUKUM</a>
<a href="Mahasiswa/datafakekonomi.html" class= "btn btn-default" role= "button"> DATA EKONOMI</a>
<a href="Mahasiswa/datafakteknik.html" class= "btn btn-default" role= "button"> DATA TEKNIK</a> -->


        </div>
            </div>
    	</div>
    	
		<div class="col-md-6 chart-layer1-left"> 
			<div class="glocy-chart">
			<div class="span-2c">  
			
                        <h3 class="tlt">Mahasiswa KRS</h3>
                        {literal}
                        <script src="js/chartpie.js"></script>
				        <script src="js/exporting.js"></script>
                        {/literal}

<div id="KRS" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
{literal}
<script>
	Highcharts.chart('KRS', {

    chart: {
        type: 'column'
    },

    title: {
        text: 'Statistik Mahasiswa KRS'
    },

    xAxis: {
        categories: ['FKES','TEKNIK','EKONOMI','HUKUM']
    },

    yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
            text: 'Jumlah mahasiswa'
        }
    },

    tooltip: {
        formatter: function () {
            return '<b>' + this.x + '</b><br/>' +
                this.series.name + ': ' + this.y + '<br/>' +
                'Total: ' + this.point.stackTotal;
        }
    },

    plotOptions: {
        column: {
            stacking: 'normal'
        }
    },

    series: [{
        name: 'Sudah KRS',
        data: [5, 3, 4, 7],
        stack: '1'
    }, {
        name: 'Belum KRS',
        data: [4, 4, 2, 5],
        stack: '1'
   
    }]
});
</script>	
{/literal}	
                {foreach $fakultas_set as $fak}
                    <a href="index.php?mode=mhs-krs-prodi&jenis=krs&id_fakultas={$fak.ID_FAKULTAS}" class= "btn btn-default" role= "button">{$fak.NM_FAKULTAS}</a>
                {/foreach}
            <!-- <a href="KRS/datakrsfkes.html" class= "btn btn-default" role= "button">  KRS FKES</a>
			<a href="KRS/datakrshukum.html" class= "btn btn-default" role= "button">  KRS HUKUM</a>
			<a href="KRS/datakrsekonomi.html" class= "btn btn-default" role= "button">  KRS EKONOMI</a>
			<a href="KRS/datakrsteknik.html" class= "btn btn-default" role= "button">  KRS TEKNIK</a> -->

			<div class="clearfix"> </div>
    	</div>
    	</div>
		
</div>

</div>
</div>
		
<hr>
<hr>
<div class="chart-layer-2">
<div class="clearfix"></div>
<hr>
<h3 style="text-align:center;"> STATUS MENGAJAR DOSEN</h3>

<!--main page chart layer2-->


<div class="chart-layer-2">
	
	<div class="col-md-6 chart-layer2-right"> 
			{literal}
			<script src="js/chartpie.js"></script>
			<script src="js/exporting.js"></script>
            {/literal}

<div id="MENGAJAR DOSEN" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
{literal}
<script>
	Highcharts.chart('MENGAJAR DOSEN', {

    chart: {
        type: 'column'
    },

    title: {
        text: 'DATA JAM MENGAJAR DOSEN'
    },

    xAxis: {
        categories: ['<8 SKS','8 SKS','8-12 SKS','>12 SKS']
    },

    yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
            text: 'Jumlah Dosen'
        }
    },

    tooltip: {
        formatter: function () {
            return '<b>' + this.x + '</b><br/>' +
                this.series.name + ': ' + this.y + '<br/>' +
                'Total: ' + this.point.stackTotal;
        }
    },

    plotOptions: {
        column: {
            stacking: 'normal'
        }
    },

    series: [{
        name: 'Dosen',
        data: [5, 3, 4, 7],
        stack: '1'
    
    }]
});
</script>	
{/literal}	
            <center> 
            <a href="dosen/kurang8.html" class= "btn btn-default" role= "button">  < 8sks </a>
		    <a href="dosen/8sks.html" class= "btn btn-default" role= "button">  8 SKS </a>
			<a href="dosen/8-12sks.html" class= "btn btn-default" role= "button">  8 - 12 SKS</a>
			<a href="dosen/lebih12.html" class= "btn btn-default" role= "button">  > 12 SKS </a>
			</div>
			</div>
            </center>
	
	</div>
	</div>
	

	<div class="col-md-6 chart-layer2-left"> 
			
			
                <br>
                {literal}
                <script src="js/chartpie.js"></script>
				<script src="js/exporting.js"></script>
                {/literal}

<div id="pendidikan dosen" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
{literal}
<script>
	Highcharts.chart('pendidikan dosen', {

    chart: {
        type: 'column'
    },

    title: {
        text: 'DATA PENDIDIKAN DOSEN UNIVERSITAS MAARIF HASYIMLATIF'
    },

    xAxis: {
        categories: ['FKES','TEKNIK','EKONOMI','HUKUM']
    },

    yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
            text: 'Jumlah Dosen'
        }
    },

    tooltip: {
        formatter: function () {
            return '<b>' + this.x + '</b><br/>' +
                this.series.name + ': ' + this.y + '<br/>' +
                'Total: ' + this.point.stackTotal;
        }
    },

    plotOptions: {
        column: {
            stacking: 'normal'
        }
    },

    series: [{
        name: 'S1',
        data: [5, 3, 4, 7],
        stack: '1'
    },{
    name: 'S2',
        data: [5, 3, 4, 7],
        stack: '1'
    },
    {name: 'S3',
        data: [5, 3, 4, 7],
        stack: '1'
   }]
});
</script>		
{/literal}
           <center>
            {foreach $fakultas_set as $fak}
                    <a href="index.php?mode=jenjang-dosen-prodi&jenis=jenjang-dosen&id_fakultas={$fak.ID_FAKULTAS}" class= "btn btn-default" role= "button">{$fak.NM_FAKULTAS}</a>
                {/foreach}
           	<!-- <a href="Jenjang dosen/jenjang dosen fkes.html" class= "btn btn-default" role= "button">  FKES </a>
			<a href="Jenjang dosen/jenjang dosen teknik.html" class= "btn btn-default" role= "button">  TEKNIK </a>
			<a href="Jenjang dosen/jenjang dosen hukum.html" class= "btn btn-default" role= "button">  EKONOMI </a>
			<a href="Jenjang dosen/jenjang dosen ekonomi.html" class= "btn btn-default" role= "button">   HUKUM </a> -->
			</div>
			</div></center>
			</div>
			</div>
	</div>

<hr>


	
	<div class="col-md-6 chart-layer1-leftt"> 
	<center>
            {literal}
			<script src="js/chartpie.js"></script>
			<script src="js/exporting.js"></script>
            {/literal}

<div id="jabatan fungsional" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
{literal}
<script>
	Highcharts.chart('jabatan fungsional', {

    chart: {
        type: 'column'
    },

    title: {
        text: 'JABATAN FUNGSIONAL DOSEN UNIVERSITAS MAARIF HASYIMLATIF'
    },

    xAxis: {
        categories: ['BELUM ADA DATA','ASSISTEN AHLI','LEKTOR','GURU BESAR','LEKTOR KEPALA']
    },

    yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
            text: 'Jumlah Dosen'
        }
    },

    tooltip: {
        formatter: function () {
            return '<b>' + this.x + '</b><br/>' +
                this.series.name + ': ' + this.y + '<br/>' +
                'Total: ' + this.point.stackTotal;
        }
    },

    plotOptions: {
        column: {
            stacking: 'normal'
        }
    },

    series: [{
        name: '',
        data: [5, 3, 4, 7, 10],
        stack: '1'
    
   }]
});
</script>	
{/literal}	
            <center> 
            <a href="dosen/belumadadata.html" class= "btn btn-default" role= "button" color="red">  BELUM ADA DATA </a>
			<a href="dosen/asst ahli.html" class= "btn btn-default" role= "button">  ASST AHLI </a>
			<a href="dosen/lektor.html" class= "btn btn-default" role= "button">  LEKTOR </a>
			<a href="dosen/guru besar.html" class= "btn btn-default" role= "button">  GURU BESAR </a>
			</div>
			</div>
            </center>
			</div>

<div class="clearfix"></div>
</center>

<div class="chart-layer-3">
	<center> 
			
			<h3 class="tlt">Status Program Studi</h3>
                {literal}
                <script src="js/chartpie.js"></script>
				<script src="js/exporting.js"></script>
                {/literal}

<div id="akreditasi" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
{literal}
<script>
	Highcharts.chart('akreditasi', {

    chart: {
        type: 'column'
    },

    title: {
        text: 'AKREDITASI FAKULTAS UNIVERSITAS MAARIF HASYIMLATIF'
    },

    xAxis: {
        categories: ['A','B ','C','BAN PT','PERSIAPAN AKREDITASI']
    },

    yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
            text: 'Jumlah Jurusan'
        }
    },

    tooltip: {
        formatter: function () {
            return '<b>' + this.x + '</b><br/>' +
                this.series.name + ': ' + this.y + '<br/>' +
                'Total: ' + this.point.stackTotal;
        }
    },

    plotOptions: {
        column: {
            stacking: 'normal'
        }
    },

    series: [{
        name: 'Akreditasi',
        data: [5, 3, 4, 7, 10],
        stack: '1'
    
   }]
});
</script>		
{/literal}
           
  </div>
</center>
<hr>
<hr>

<!--inner block end here-->
<!--copy rights start here-->
<div class="clearfix"></div>
<div class="copyrights">
	 <p>© 2016. All Rights Reserved </p>
</div>	
<!--COPY rights end here-->
</div>
</div>

<!--slider menu-->
    
{literal}    
<!--slide bar menu end here-->
<script>
var toggle = true;
            
$(".sidebar-icon").click(function() {                
  if (toggle)
  {
    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
    $("#menu span").css({"position":"absolute"});
  }
  else
  {
    $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
    setTimeout(function() {
      $("#menu span").css({"position":"relative"});
    }, 400);
  }               
                toggle = !toggle;
            });
</script>
<!--scrolling js-->
		<script src="js/jquery.nicescroll.js"></script>
		<script src="js/scripts.js"></script>
		<!--//scrolling js-->
<script src="js/bootstrap.js"> </script>
<!-- mother grid end here-->
{/literal}
</body>
</html>                     