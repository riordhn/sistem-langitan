# Sistem Langitan #

Repo Sistem Langitan yang sedang berjalan saat ini, merupkan hasil forking dari sistem cybercampus yang dikembangkan Universitas Airlangga. Sistem yang saat ini tidak ada pengembangan baru dan hanya akan dimaintain sampai pengembangan [Sistem Langitan v2](https://bitbucket.org/umaha/sistem-langitan-v2) rilis.

## Petunjuk Commit Message ##
Commit message hendaknya melibatkan 3 unsur yaitu **What**, **Why**, dan **Who**.

- **What** :  What atau Apa ? Menjelaskan perubahan spesifik apa yang dilakukan. Untuk membedakan sebaiknya digunakan prefix:
  - `feat` untuk fitur baru,
  - `fix` untuk perbaikan error/bug,
  - `docs` apabila menambahkan komentar saja pada kode atau dokumentasi seperti pada README,
  - `style/ui` apabila berkenaan dengan tampilan saja (file .tpl saja dan tidak mengubah alur proses dalam file .php), Misal untuk kasus typo
- **Why** : Why atau Mengapa ? Menjelaskan hal yang mendasari perubahan yang dilakukan.
- **Who** : Who atau Siapa ? Mengacu pada modul yang dilakukan perubahan atau person aktor yang melakukan pelaporan/komplain (Dosen/Mahasiswa/Staf/Alumni/Calon Mahasiswa).

Sebagai contoh kasus: Terdapat error pada halaman `/modul/pendidikan/akademik-transkrip.php` ketika diakses oleh staf pada salah satu klik aksi. Kemudian setelah diperbaiki, maka kira-kira hasil bentuk commitnya adalah : 

> fix: Perbaikan error pada saat klik menu X pada modul pendidikan

Jika dijabarkan: *Perbaikan error pada saat klik menu X* mewakili unsur What, *pada modul pendidikan* mewakili unsur Who, dan unsur Why-nya sudah diwakili oleh prefix `fix` berarti karena error.

Contoh kasus lain : Wakil rektor 1 ingin muncul ipk pada list data mahasiswa. Maka bentuk commitnya kira-kira adalah :

> feat: Penambahan kolom IPK pada list data mahasiswa permintaan wakil rektor 1

Jika dijabarkan: *Penambahan kolom IPK pada list data mahasiswa* => What, *permintaan wakil rektor 1* => Who.

*NB: Petunjuk commit message ini tidak mengikat, tetapi sebagai anjuran agar commit messagenya lebih mudah dipahami dan dimengerti*

### Penting !! ###
- **Jika tidak ada perubahan konseptual yang sangat mendasar, dimohon untuk tidak melakukan push perubahan pada file `config.php` dan `index.php`**

## Development dengan Docker (Linux / Windows) ##

1. Install Docker
2. Build dockerfile dengan perintah:
   ```shell script
   $ docker build -t sistem-langitan .
   ``` 
3. Jalankan image docker hasil build.
   - Untuk PowerShell Windows
   ```shell script
   $ docker run --rm -d -p 80:80 -v ${pwd}:/var/www/html sistem-langitan
   ```
   - Untuk Terminal linux
   ```shell script
   $ docker run --rm -d -p 80:80 -v $(pwd):/var/www/html sistem-langitan
   ```
4. Tambahkan baris vhost `127.0.0.1 langitan-dev.umaha.ac.id` pada file `C:\Windows\system32\drivers\etc\hosts`,
   untuk linux di file `/etc/hosts`
5. Tes di browser dengan mengakses `http://langitan-dev.umaha.ac.id`

## Development Setting (Windows 10) ##

### 1. Konfigurasi XAMPP + OCI ###
- Install [XAMPP](https://sourceforge.net/projects/xampp/files/XAMPP%20Windows/5.6.40/) yang menggunakan PHP 5.6.x (bisa juga menggunakan versi PHP 7.x tetapi masih belum optimal)
- Download [Oracle Instant Client for Microsoft Windows (32-bit)](https://www.oracle.com/database/technologies/instant-client/downloads.html). Pilih versi 12.1 atau 12.2
- Extract Oracle Instant client ke folder `C:\Program Files (x86)\instantclient_12_1` atau ke folder `C:\Program Files (x86)\instantclient_12_2` untuk yang versi 12.2
- Masukkan `C:\Program Files (x86)\instantclient_12_1` ke variabel **PATH** environment windows
- Buka file `php.ini` di folder `\xampp\php\php.ini`
- Hapus `;` pada baris `extension=oci8_12c` untuk mengaktifkan extension oci8
- Buka `php1nf0.php` lewat browser untuk memastikan ekstensi oci8 sudah aktif

### 2. Konfigurasi Host ###
Sistem Langitan menggunakan satu database dengan pembeda nama virtual host, dan untuk development lokal menggunakan format : `langitan-[namapt].localhost`. Maka untuk umaha formatnya `langitan-umaha.localhost`. Ini digunakan untuk mensetting konfigurasi virtual host pada Apache di XAMPP. Penamaan host ini mengambil dari tabel `PERGURUAN_TINGGI`.

- Buka file `\xampp\apache\conf\extra\httpd-vhosts.conf`
- Tambahkan blok konfigurasi virtual host untuk folder langitan hasil clone. Misalnya, jika di clone di folder `C:\xampp\htdocs\sistem-langitan` maka konfigurasinya adalah sebagai berikut:
```Apache
<VirtualHost *:80>
	DocumentRoot "C:/xampp/htdocs/sistem-langitan"
	ServerName langitan-umaha.localhost
	<Directory "C:/xampp/htdocs/sistem-langitan">
		Require all granted
	</Directory>
</VirtualHost>
```
- Kemudian tambahkan baris vhost `127.0.0.1 langitan-umaha.localhost` pada file `C:\Windows\system32\drivers\etc\hosts`

Setelah konfigurasi selesai jalankan (atau restart) XAMPP.

### 3. Konfigurasi Database ###

- Copy file `.env.example` menjadi `.env`
- Entri semua settingan konfigurasi yang ada dalam file `.env` sesuai setting konfigurasi database. Sebagai contoh:
```Ini
DB_HOST=
DB_PORT=
DB_SERVICE_NAME=
DB_SERVER_TYPE=
DB_INSTANCE_NAME=
DB_CHARSET=
DB_USER=
DB_PASS=
```
- Semua nilai konfigurasi hendaknya di isi semua.


### Menggunakan PHP Built-in Server sebagai pengganti XAMPP di Windows (Alternatif) ###

PHP bisa digunakan langsung sebagai webserver development agar tidak perlu menggunakan XAMPP. Langkah-langkah menggunakan PHP sebagai built in server:

- Download [PHP for Windows](https://windows.php.net/download). Disarankan versi 7.x terbaru jika develop menggunakan php built-in server dan pilih versi x86/32bit agar kompatibel dengan Oracle Instant Client. Lalu extract di `C:\php\` (atau boleh ditempat lain)
- Setelah di extract, copy file `php.ini-development` menjadi `php.ini`.
- Masukkan path hasil extract (`C:\php`) ke variabel **PATH** environment windows agar pemanggilan **php** bisa dipakai disegala lokasi. Untuk memastikan, buka Command Prompt. Ketik `php --version`. Maka akan muncul kira-kira seperti berikut ini :
![PHP Version](readme-assets/php-version.png)
- Download [Oracle Instant Client for Microsoft Windows (32-bit)](https://www.oracle.com/database/technologies/instant-client/downloads.html). Pilih versi 12.1 atau 12.2
- Extract Oracle Instant client ke folder `C:\Program Files (x86)\instantclient_12_1` atau ke folder `C:\Program Files (x86)\instantclient_12_2` untuk yang versi 12.2
- Masukkan `C:\Program Files (x86)\instantclient_12_1` ke variabel **PATH** environment windows
- Buka file `C:\php\php.ini`, hapus `;` pada baris `extension=oci8_12c` untuk mengaktifkan extension oci8
- Buka file `C:\Windows\system32\drivers\etc\hosts`, tambahkan baris vhost `127.0.0.1 langitan-umaha.localhost` 
- Lalu untuk menjalankan php built in server, buka command prompt, masuk ke folder tempat sistem langitan di clone, kemudian ketik `php -S langitan-umaha.localhost:80`.
- Pada saat development redirect otomatis http ke https perlu dimatikan. Untuk itu buka file `index.php`, dan beri komentar pada baris berikut :
```php
header("Location: https://".$_SERVER['HTTP_HOST']);
exit();
```
- Selain itu tampilan error php perlu ditampilkan. Buka file `config.php`, dan ubah baris `ini_set('display_errors', 0);` menjadi `ini_set('display_errors', 1);`
- Buka di browser `http://langitan-umaha.localhost/` untuk memastikan setting development sudah aktif

## Error Handling
```text
Perguruan Tinggi belum di set.
```
- Harap menghapus file pada folder `/cache/PERGURUAN_TINGGI`

## Tim Pengembang ##

- Fathoni
- Nambi
- Fikrie
- Amirul Fatah
- Rizaq