<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AcademicController extends Controller
{
    private $offset = 0;
    private $limit = 10;
    private $query = '';
    private $filter = '';
    private $result = [];
    private $id_perguruan_tinggi;

    public function getProgramStudi(Request $request)
    {
        $this->limit = $request->get('limit') ? $request->get('limit') : $this->limit;
        $this->offset = $request->get('offset') ? $request->get('offset') : $this->offset;
        $this->id_perguruan_tinggi = $request->get('id_perguruan_tinggi') ? $request->get('id_perguruan_tinggi') : '';
        $fakultas = $request->get('fakultas') ? $request->get('limit') : '';
        $jenjang = $request->get('jenjang') ? $request->get('jenjang') : '';
        if (!empty($this->id_perguruan_tinggi)) {
            // JIKA FAKULTAS TIDAK KOSONG
            $this->query = "
                SELECT FAKULTAS.NM_FAKULTAS, PROGRAM_STUDI.* 
                FROM PROGRAM_STUDI
                JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS=PROGRAM_STUDI.ID_FAKULTAS
                WHERE FAKULTAS.ID_PERGURUAN_TINGGI='{$this->id_perguruan_tinggi}'
                ";
            if (!empty($fakultas)) {
                $this->filter = "AND ID_FAKULTAS='{$fakultas}'";
                $this->query .= $this->filter;
                $this->result = [
                    'status' => 'success',
                    'message' => 'Data berhasil diambil',
                    'data' =>  DB::select($this->query)
                ];
            } else {
                $this->query .= $this->filter;
                $this->result = [
                    'status' => 'success',
                    'message' => 'Data berhasil diambil',
                    'data' => DB::select($this->query)
                ];
            }
        } else {
            $this->result = [
                'status' => 'error',
                'message' => 'Perguruan tinggi kosong',
                'data' => []
            ];
        }
        return response()->json($this->result);
    }
}
