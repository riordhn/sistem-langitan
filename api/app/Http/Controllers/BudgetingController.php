<?php

namespace App\Http\Controllers;

use Illuminate\Cache\FileStore;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BudgetingController extends Controller
{
    private $offset = 0;
    private $limit = 10;
    private $query = '';
    private $filter = '';
    private $result = [];
    private $id_perguruan_tinggi;

    public function all(Request $request)
    {
        $this->limit = $request->get('limit') ? $request->get('limit') : $this->limit;
        $this->offset = $request->get('offset') ? $request->get('offset') : $this->offset;
        $this->id_perguruan_tinggi = $request->get('pt') ? $request->get('pt') : '1';
        $this->id_perguruan_tinggi = $request->get('pt') ? $request->get('pt') : '1';
        $date = $request->get('date') ? $request->get('date') : '';
        $fakultas = $request->get('fakultas') ? $request->get('limit') : '';
        $jenjang = $request->get('jenjang') ? $request->get('jenjang') : '';
        try {
            if (!empty($date)) {
                $this->query = "
                SELECT P.ID_PEMBAYARAN AS TRANSACTION_ID, 
                    B.NM_BIAYA || ' ' || J.NM_JENJANG || '-' || PS.NM_PROGRAM_STUDI AS SUBCATEGORY,
                    J.NM_JENJANG || '-' || PS.NM_PROGRAM_STUDI AS DIVISION_NAME,
                    P.TGL_BAYAR AS TRANSACTION_DATE,
                    P.BESAR_PEMBAYARAN AS TRANSACTION_NOMINAL,
                    P.ID_BANK,
                    B.NM_BANK AS BANK_NAME
                FROM PEMBAYARAN P
                JOIN TAGIHAN T ON T.ID_TAGIHAN = P.ID_TAGIHAN
                JOIN TAGIHAN_MHS TMHS ON TMHS.ID_TAGIHAN_MHS = T.ID_TAGIHAN_MHS
                JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = T.ID_DETAIL_BIAYA
                JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                JOIN MAHASISWA M ON M.ID_MHS = TMHS.ID_MHS
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
                JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                JOIN BANK B ON B.ID_BANK = P.ID_BANK
                WHERE FAK.ID_PERGURUAN_TINGGI='{$this->id_perguruan_tinggi}' 
                AND TO_CHAR(P.TGL_BAYAR,'YYYY-MM-DD') >= '{$date}'
                AND P.ID_STATUS_PEMBAYARAN = 1 
                AND P.BESAR_PEMBAYARAN>0
                ORDER BY P.ID_PEMBAYARAN ASC, P.TGL_BAYAR ASC
                OFFSET {$this->offset} ROWS 
                FETCH NEXT {$this->limit} ROWS ONLY
                ";
                $this->filter = "";
                $this->query .= $this->filter;
                $this->result = [
                    'status' => 'success',
                    'message' => 'Get data budgeting',
                    'data' =>  DB::select($this->query)
                ];
                return response()->json([$this->result], 200);
            }else{
                $this->result = [
                    'status' => 'failed',
                    'message' => "Date cannot be empty",
                    'data' => []
                ];
                return response()->json([$this->result], 500);
            }
        } catch (\Exception $e) {
            $this->result = [
                'status' => 'failed',
                'message' => $e->getMessage(),
                'data' => []
            ];
            return response()->json([$this->result], 500);
        }
    }
}
