<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

Route::get('/', function () use ($router) {
    return 'Langitan API. By DSI UMAHA.';
});
/*AUTH */
$router->post(
    'auth/login',
    [
        'uses' => 'AuthController@authenticate'
    ]
);
/* ACADEMIC MODULE */
Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'academic'

], function ($router) {

    Route::group(['prefix' => 'program-studi'], function () {
        Route::get('/', 'AcademicController@getProgramStudi');
    });

});

Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'finance'

], function ($router) {

    Route::group(['prefix' => 'budgeting'], function () {
        Route::get('/', 'BudgetingController@all');
    });

});
