FROM php:7.3-apache

# Install Required lib
RUN apt-get update \
    && apt-get install -qqy git unzip libfreetype6-dev libjpeg62-turbo-dev libpng-dev libaio1 wget libzip-dev \
    && apt-get clean autoclean \
    && apt-get autoremove --yes \
    && rm -rf /var/lib/{apt,dpkg,cache,log}/

# ORACLE oci
RUN mkdir /opt/oracle \
    && cd /opt/oracle

ADD instantclient-basic-linux.x64-12.2.0.1.0.zip /opt/oracle
ADD instantclient-sdk-linux.x64-12.2.0.1.0.zip /opt/oracle

# Install Oracle Instantclient
RUN unzip -qq /opt/oracle/instantclient-basic-linux.x64-12.2.0.1.0.zip -d /opt/oracle \
    && unzip -qq /opt/oracle/instantclient-sdk-linux.x64-12.2.0.1.0.zip -d /opt/oracle \
    && ln -s /opt/oracle/instantclient_12_2/libclntsh.so.12.1 /opt/oracle/instantclient_12_2/libclntsh.so \
    && ln -s /opt/oracle/instantclient_12_2/libclntshcore.so.12.1 /opt/oracle/instantclient_12_2/libclntshcore.so \
    && ln -s /opt/oracle/instantclient_12_2/libocci.so.12.1 /opt/oracle/instantclient_12_2/libocci.so \
    && rm -rf /opt/oracle/*.zip

ENV LD_LIBRARY_PATH  /opt/oracle/instantclient_12_2:${LD_LIBRARY_PATH}

# Install Oracle extensions
RUN echo 'instantclient,/opt/oracle/instantclient_12_2/' | pecl install oci8 \
    && docker-php-ext-enable oci8 \
    && docker-php-ext-configure pdo_oci --with-pdo-oci=instantclient,/opt/oracle/instantclient_12_2,12.2 \
    && docker-php-ext-install pdo_oci

# Install GD
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd

# Install ext-zip
RUN docker-php-ext-install zip

WORKDIR /var/www/html
VOLUME /var/www/html