<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use Illuminate\Encryption\Encrypter;

require 'config.php';
require 'includes/Cryptography.class.php';

$message = "";
$status = true;
$submited = false;
$crypto = new Encrypter(getenv('URL_ENCRYPTION_KEY'));

function sendMail($subject, $to, $to2, $content)
{
    try {
        $mail = new PHPMailer(true);
        //Server settings
        $mail->isSMTP();                                            // Send using SMTP
        $mail->SMTPDebug = getenv('APP_ENV') == 'production' ? SMTP::DEBUG_OFF : SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->Host       = gethostbyname(getenv('MAIL_HOST'));     // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->SMTPAutoTLS = false;
        $mail->SMTPSecure = getenv('MAIL_SECURE_MODE');         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Username   = getenv('MAIL_USERNAME');                // SMTP username
        $mail->Password   = getenv('MAIL_PASSWORD');                // SMTP password
        $mail->Port       = getenv('MAIL_PORT');                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        //Recipients
        $mail->setFrom(getenv('MAIL_FROM_ADDRESS'), 'LANGITAN - UMAHA');
        $mail->addAddress($to);
        if (!empty($to2)) {
            $mail->addAddress($to2);
        }

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $content;
        if ($mail->send()) {
            return [
                'status' => true,
                'message' => "Message has been sent."
            ];
        } else {
            return [
                'status' => false,
                'message' => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}"
            ];
        }
    } catch (Exception $e) {
        return [
            'status' => false,
            'message' => "Message could not be sent. Mailer Error: " . $e->getMessage() . ""
        ];
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($_POST['mode'] == 'forgot-password') {
        $submited = true;
        $username = post("username");
        $tgl_lahir = date('d-m-Y', strtotime(post('tgl_lahir')));
        $db->Query("SELECT ID_PENGGUNA,TO_CHAR(TGL_LAHIR_PENGGUNA,'DD-MM-YYYY') TGL_LAHIR_PENGGUNA,EMAIL_ALTERNATE,EMAIL_PENGGUNA FROM PENGGUNA WHERE USERNAME = '{$username}' AND ID_PERGURUAN_TINGGI='{$PT->ID_PERGURUAN_TINGGI}'");
        // Cek hasil username
        $pengguna = $db->FetchAssoc();

        if (!empty($pengguna)) {
            if (empty($pengguna['EMAIL_ALTERNATE']) || strpos($pengguna['EMAIL_ALTERNATE'], ' ') !== false) {
                $status = false;
                $message = "Format email tidak sesuai, silahkan hubungi DSI untuk perubahan email";
            } else if (empty($pengguna['TGL_LAHIR_PENGGUNA'])) {
                $status = false;
                $message = "Format tanggal lahir tidak sesuai, silahkan hubungi DSI untuk perubahan tanggal lahir";
            } else if ($pengguna['TGL_LAHIR_PENGGUNA'] != $tgl_lahir) {
                $status = false;
                $message = "Tanggal lahir salah";
            } else if ($pengguna['TGL_LAHIR_PENGGUNA'] == $tgl_lahir) {
                $message = "Reset password berhasil, link perubahan password di kirim ke email : <b>" . substr($pengguna['EMAIL_ALTERNATE'], 0, 2) . "*****" . substr($pengguna['EMAIL_ALTERNATE'], -14) . '</b>';
                $content = "Silahkan klik link <a href='" . getenv('URL_HOST') . "/auth.php?mode=generate-password&key=" . $crypto->encrypt($pengguna['ID_PENGGUNA']) . "'>Disini</a> untuk melakukan perubahan password. <br/>Terima Kasih";
                $response_email = sendMail("Reset Password", $pengguna['EMAIL_ALTERNATE'], $pengguna['EMAIL_PENGGUNA'], $content);
                if (!$response_email['status']) {
                    $status = false;
                    $message = $response_email['message'];
                }
            }
        } else {
            $status = false;
            $message = "Pengguna tidak ditemukan";
        }
    }
}

$smarty->assign('submited', $submited);
$smarty->assign('status', $status);
$smarty->assign('message', $message);
$smarty->display('gui/forgot-password.tpl');
