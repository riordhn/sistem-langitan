import _ from 'lodash';
import 'jquery';

function component() {
    const element = document.createElement('div');

    // Lodash, currently included via a script, is required for this line to work
    element.innerHTML = _.join(['Hello', 'webpack'], ' ');

    return element;
}

$(document).ready(function() {
    console.log(this);
});

document.body.appendChild(component());