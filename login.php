<?php
require 'config.php';

$html_output = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($_POST['mode'] == 'login') {
        $error_message = "";
        $error = false;
        $html_output = 'login-error';

        if (strlen(trim($_POST['username'])) > 0 && strlen(trim($_POST['password'])) > 0) {
            $login_result = $user->Login($_POST['username'], $_POST['password'], $PT->id_perguruan_tinggi);
        } else {
            if (strlen(trim($_POST['username'])) == 0) {
                $login_result = User::LOGIN_STATUS_WRONG_USERNAME;
            } elseif (strlen(trim($_POST['password'])) == 0) {
                $login_result = User::LOGIN_STATUS_WRONG_PASSWORD;
            }
        }

        if ($login_result == User::LOGIN_STATUS_WRONG_USERNAME) {
            $error_message = "Username tidak ditemukan, silakan mengulangi login.";
            $error = false;
        } elseif ($login_result == User::LOGIN_STATUS_WRONG_PASSWORD) {
            $error_message = "Password tidak sesuai, silakan mengulangi login.";
            $error = false;
        } elseif ($login_result == User::LOGIN_STATUS_WRONG_PT) {
            $error_message = "Login tidak sesuai, silakan mengulangi login.";
            $error = false;
        } elseif ($login_result == User::LOGIN_STATUS_FAILED) {
            $error_message = "Gagal login, silakan mengulangi login.";
            $error = false;
        } elseif ($login_result == User::LOGIN_STATUS_SUCCESS) {
            // khusus auto-login UMAHA
            if (isset($_POST['mode-login'])) {
                if ($_POST['mode-login'] == 'auto-login') {
                    $_SESSION['auto-login'] = 'yes';
                }
            }
            
            $username = $user->USERNAME;
            $name = $user->NAMA_PENGGUNA;
            $email = $user->EMAIL_PENGGUNA;
            $id_role = $user->ID_ROLE;
            $id_pengguna = $user->ID_PENGGUNA;
            
            $_SESSION['standar_iso'] = '';
            
            //get diff month
            $date1 = new DateTime(date('Y-m-d', strtotime($user->LAST_TIME_PASSWORD)));
            $date2 = new DateTime(date('Y-m-d'));
            $diff = $date1->diff($date2);
            
            $diffmonth = ($diff->format('%y') * 12) + $diff->format('%m');
            
            $uppercase = preg_match('@[A-Z]@', $_POST['password']);
            $lowercase = preg_match('@[a-z]@', $_POST['password']);
            $number = preg_match('@[0-9]@', $_POST['password']);

            // jika belum pernah ganti password & bukan password um@ha
            if ($user->LAST_TIME_PASSWORD == '' && $_POST['password'] != 'um@ha') {
                // Redirect ke halaman ganti password terlebih dahulu
                $_SESSION['standar_iso'] = 'no';
                header("Location: login.php?mode=ltp-null");
                //session_destroy();
                exit();
            } elseif ($user->PASSWORD_MUST_CHANGE == 1 && $_POST['password'] != 'um@ha') { // Jika diharuskan ganti password (habis reset)
                // Redirect ke halaman ganti password terlebih dahulu
                $_SESSION['standar_iso'] = 'no';
                header("Location: login.php?mode=ltp-null");
                exit();
            } else {
                // Redirect ke path yg sesuai
                $_SESSION['standar_iso'] = 'yes';
                header("Location: {$user->PATH}");
                exit();
            }
        }
    }

    if ($_POST['mode'] == 'change-passwd') {
        $error = false;
        $error_message = "";
        $success = false;
        $html_output = 'change-password';

        if (strlen(trim($_POST['password_new'])) == 0 || strlen(trim($_POST['password_new2'])) == 0) {
            $error_message = "Isian password baru tidak lengkap.";
            $error = true;
        } elseif ($_POST['password_new'] != $_POST['password_new2']) {
            $error_message = "Isian password baru harus sama.";
            $error = true;
        } else {
            $cp_result = $user->ChangePasswordKhusus($_POST['password_old'], $_POST['password_new']);

            if ($cp_result == User::CP_WRONG_PASSWORD) {
                $error_message = "Password lama salah.";
                $error = true;
            } elseif ($cp_result == User::CP_SAME_USERNAME) {
                $error_message = "Password baru tidak boleh sama dengan username.";
                $error = true;
            } elseif ($cp_result == User::CP_SAME_OLD_PASSWORD) {
                $error_message = "Password baru tidak boleh sama dengan password lama.";
                $error = true;
            } elseif ($cp_result == User::CP_WRONG_RULES) {
                $error_message = "Syarat password baru adalah 8 karakter dan harus terdapat angka, huruf, dan spesial karakter.";
                $error = true;
            } elseif ($cp_result == User::CP_FAILED) {
                $error_message = "Gagal mengganti password.";
                $error = true;
            } elseif ($cp_result == User::CP_SUCCESS) {
                $success = true;
                $_SESSION['standar_iso'] = 'yes';
                
                // Redirect ke path yg sesuai
                header("Location: {$user->PATH}");
                exit();
            }
        }
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['mode'])) {
        // Jika belum pernah set password
        if ($_GET['mode'] == 'ltp-null') {
            $html_output = 'change-password';
        }
    } else {
        header("Location: /");
        exit();
    }
}

// HTML output
if ($html_output == 'login-error') {
    ?>
    <html>
        <head>
            <title>Sistem Langitan NU - <?php echo $PT->nama_pt; ?></title>
        </head>
        <body>
            <div align="center">
                <table style="margin-top: 25px; border: 1px solid #ddd;" cellspacing="5">
                    <tr>
                        <td style="background-color: #EEF5F7; text-align: center;">
                            <img src="langitan/Login_Lock.png" style="border-spacing:0px"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: #EEF5F7; text-align: center;">
    <?php echo $error_message; ?><br/><br/><a href="/">Kembali ke halaman depan</a></td>
                    </tr>
                </table>
            </div>
        </body>
    </html><?php
} elseif ($html_output == 'change-password') {
    ?>
    <html>
        <head>
            <title>Sistem Langitan NU - <?php echo $PT->nama_pt; ?></title>
            <style>
                * { font-family: "Segoe UI","Lucida Sans Unicode", Arial; font-size: 13px; }
                .box { margin-bottom: 20px; border: 1px solid #000; background-color: #eef; }
                .box thead tr th { background-color: navy; color: #fff; padding: 5px 0px; }
            </style>
        </head>
        <body>
            <form action="login.php?mode=ltp-null" method="post">
                <input type="hidden" name="mode" value="change-passwd" />
                <div align="center">
                    <table class="box">
                        <thead>
                            <tr>
                                <td colspan="2"><img src="img/portal/logo-langitan-<?php echo $PT->nama_singkat; ?>.PNG" style="border-spacing:0px"/></td>
                            </tr>
                            <tr>
                                <td colspan="2">                                     
                                    Demi keamanan account, Anda diharuskan mengganti password terlebih <br />dahulu dengan ketentuan sebagai berkut : <br/>
                                    <font color="green">- Syarat password baru minimal 8 karakter yang terdiri dari <br />kombinasi huruf dan angka.</font> Contoh : l4ng1tan
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2">Setting Account</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Nama</td>
                                <td><?php echo $user->NAMA_PENGGUNA; ?></td>
                            </tr>
                            <tr>
                                <td>Username</td>
                                <td><?php echo $user->USERNAME; ?></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><?php echo $user->EMAIL_PENGGUNA; ?></td>
                            </tr>
                            <tr>
                                <td>Terakhir Ganti</td>
                                <td><?php echo ($user->LAST_TIME_PASSWORD != '') ? strftime('%d/%m/%Y', strtotime($user->LAST_TIME_PASSWORD)) : ''; ?></td>
                            </tr>
                            <tr>
                                <td>Password Baru</td>
                                <td><input type="password" name="password_new" value="" /></td>
                            </tr>
                            <tr>
                                <td>Password Baru (Ulangi)</td>
                                <td><input type="password" name="password_new2" value="" />
                                <input type="hidden" name="password_old" value="<?php echo $user->PASSWORD_HASH; ?>" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <a href="logout.php">Logout</a>
                                    <input type="submit" value="Simpan" />
                                </td>
                            </tr>
    <?php if (!empty($error)) { ?>
                                <tr>
                                    <td colspan="2" style="color: red"><?php echo $error_message; ?></td>
                                </tr>
    <?php } ?>
                            <?php if (!empty($success)) { ?>
                                <tr>
                                    <td colspan="2" style="color: green; font-weight: bold">Password berhasil diganti.</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </form>
        </body>
    </html><?php
}
