<?php

class Mahasiswa
{
	private $db;

	public $id_mhs;

	function __construct(&$db, $id_mhs)
	{
		$this->db = $db;
		$this->id_mhs = $id_mhs;
	}

	public function list_krs_by_semester($id_semester)
	{
		$sql = 
			"SELECT 
				pmk.id_pengambilan_mk,
				COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
				COALESCE(mk1.nm_mata_kuliah, mk2.nm_mata_kuliah) AS nm_mata_kuliah,
				COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
				nk.nama_kelas,
				pmk.nilai_angka,
				pmk.nilai_huruf,
				pmk.status_apv_pengambilan_mk,
				to_char(pmk.created_on, 'YYYY-MM-DD HH24:MI:SS') as created_on
			FROM 
				pengambilan_mk pmk
			JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
			-- Via Kelas
			LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
			LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
			LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kls.no_kelas_mk
			-- Via Kurikulum
			LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
			LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
			WHERE mhs.id_mhs = {$this->id_mhs} and pmk.id_semester = {$id_semester}
			ORDER BY 3 ASC";

		return $this->db->QueryToArray($sql);
	}
}