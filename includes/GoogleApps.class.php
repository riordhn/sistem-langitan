<?php
/**
 * Google Apps Provisioning Class
 * @author Fathoni <fathoni@staf.unair.ac.id>
 * @copyright (c) 2013, Fathoni
 * @version 11/02/2013
 */
class GoogleApps
{
	public $AuthToken;
	public static $CustomerId	= "C03a9ttt6"; // konstanta for unair.ac.id
	
	function __construct($auth_token)
	{
		$this->AuthToken = $auth_token;
	}
	
	private function execute_curl($options)
	{
		$ch = curl_init();
		curl_setopt_array($ch, $options);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // force
		curl_setopt($ch, CURLOPT_HEADER, false); // force
		$curl_response = curl_exec($ch);
		curl_close($ch);

		return $curl_response;
	}

	/**
	 * Mendapatkan token, dengan menggunakan email / password admin
	 * @param string $email Email admin
	 * @param string $password Password admin
	 * @return string String token dari google
	 */
	function GetAuthToken($email = "", $password = "")
	{
		if ($email == "") $email = $this->AdminUser;
		if ($password == "") $password = $this->AdminPassword;
		
		$options = array(
			CURLOPT_URL				=> 'https://www.google.com/accounts/ClientLogin',
			CURLOPT_POSTFIELDS		=> "Email={$email}&Passwd={$password}&accountType=HOSTED&service=apps",
			CURLOPT_POST			=> true,
		);

		// Eksekusi url
		$response = $this->execute_curl($options);
		$response = explode("\n", $response);

		$this->AuthToken = substr($response[2], 5, strlen($response[2]) - 5);
		
		return $this->AuthToken;
	}
	
	/**
	 * Membuat UserAccount baru
	 * @param string $domain
	 * @param string $username
	 * @param string $password
	 * @param string $first_name
	 * @param string $last_name
	 * @param string $auth_token
	 * @return atom+xml Atom Entry result dari google
	 */
	function CreateUserAccount($domain, $username, $password, $first_name, $last_name)
	{
		// hash first
		$password = sha1($password);
		
		$xml_feed  = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml_feed .= '<atom:entry xmlns:atom="http://www.w3.org/2005/Atom" xmlns:apps="http://schemas.google.com/apps/2006">';
		$xml_feed .= '<atom:category scheme="http://schemas.google.com/g/2005#kind" term="http://schemas.google.com/apps/2006#user"/>';
		$xml_feed .= '<apps:login userName="'.$username.'" password="'.$password.'" hashFunctionName="SHA-1" suspended="false"/>';
		$xml_feed .= '<apps:quota limit="25600"/>';
		$xml_feed .= '<apps:name givenName="'.$first_name.'" familyName="'.$last_name.'" />';
		$xml_feed .= '</atom:entry>';
		
		$options = array(
			CURLOPT_URL				=> "https://apps-apis.google.com/a/feeds/{$domain}/user/2.0",
			CURLOPT_HTTPHEADER		=> array('Content-type: application/atom+xml', 'Authorization: GoogleLogin auth='.$this->AuthToken),
			CURLOPT_POST			=> true,
			CURLOPT_POSTFIELDS		=> $xml_feed,
		);
		
		$response = $this->execute_curl($options);
		
		return $response;
	}

	/**
	 * Mendapatkan account user
	 * @param string $domain
	 * @param string $username
	 * @param string $auth_token
	 * @return atom+xml String xml jika terdapat account, NULL jika tidak ada
	 */
	function GetUserAccount($username, $domain)
	{
		$options = array(
			CURLOPT_URL				=> "https://apps-apis.google.com/a/feeds/{$domain}/user/2.0/{$username}",
			CURLOPT_HTTPHEADER		=> array('Content-type: application/atom+xml', 'Authorization: GoogleLogin auth='.$this->AuthToken)
		);

		$response = $this->execute_curl($options);

		return $response;
	}
	
	function UpdateUserAccount($atom_xml)
	{
		$xml = new DOMDocument();
		$xml->loadXML($atom_xml);
		
		$url = $xml->getElementsByTagName("link")->item(1)->getAttribute("href");
		
		$options = array(
			CURLOPT_URL				=> $url,
			CURLOPT_HTTPHEADER		=> array('Content-type: application/atom+xml', 'Authorization: GoogleLogin auth='.$this->AuthToken),
			CURLOPT_CUSTOMREQUEST	=> "PUT",
			CURLOPT_POSTFIELDS		=> $xml->saveXML()
		);
		
		$response = $this->execute_curl($options);
		
		return $response;
	}
	
	/**
	 * Mengganti password
	 * @param atom+xml $atom_xml
	 * @param string $password_hash Password harus berupa hash SHA-1
	 * @return atom+xml String atom xml hasil
	 */
	function ChangePassword($atom_xml, $password_hash)
	{
		$xml = new DOMDocument();
		$xml->loadXML($atom_xml);
		
		$apps_login = $xml->getElementsByTagNameNS("http://schemas.google.com/apps/2006", "login")->item(0);
		$apps_login->setAttribute('password', $password_hash);
		$apps_login->setAttribute('hashFunctionName', 'SHA-1');
		
		return $xml->saveXML();
	}

	/**
	 * Mendapatkan semua user di domain
	 * @param type $domain
	 * @param type $auth_token
	 * @return type
	 */
	function GetAllUsersInDomain($domain)
	{
		$options = array(
			CURLOPT_URL				=> "https://apps-apis.google.com/a/feeds/{$domain}/user/2.0",
			CURLOPT_HTTPHEADER		=> array('Content-type: application/atom+xml', 'Authorization: GoogleLogin auth='.$this->AuthToken)
		);

		$response = $this->execute_curl($options);

		return $response;
	}

	function GetCustomerId()
	{
		$options = array(
			CURLOPT_URL				=> "https://apps-apis.google.com/a/feeds/customer/2.0/customerId",
			CURLOPT_HTTPHEADER		=> array('Content-type: application/atom+xml', 'Authorization: GoogleLogin auth='.$this->AuthToken)
		);

		$response = $this->execute_curl($options);

		return $response;
	}

	function GetAllOrganizationUnits($customer_id = "")
	{
		if ($customer_id == "") $customer_id = GoogleApps::$CustomerId;
		
		$options = array(
			CURLOPT_URL				=> "https://apps-apis.google.com/a/feeds/orgunit/2.0/{$customer_id}?get=all",
			CURLOPT_HTTPHEADER		=> array('Content-type: application/atom+xml', 'Authorization: GoogleLogin auth='.$this->AuthToken)
		);

		$response = $this->execute_curl($options);

		return $response;
	}

	function GetAllOrganizationUsers($customer_id = "")
	{
		if ($customer_id == "") $customer_id = GoogleApps::$CustomerId;
		
		$options = array(
			CURLOPT_URL				=> "https://apps-apis.google.com/a/feeds/orguser/2.0/{$customer_id}?get=all",
			CURLOPT_HTTPHEADER		=> array('Content-type: application/atom+xml', 'Authorization: GoogleLogin auth='.$this->AuthToken)
		);

		$response = $this->execute_curl($options);

		return $response;
	}

	function GetSingleOrganizationUser($user_email, $customer_id = "")
	{
		if ($customer_id == "") $customer_id = GoogleApps::$CustomerId;
		$user_email  = rawurlencode($user_email);

		$options = array(
			CURLOPT_URL				=> "https://apps-apis.google.com/a/feeds/orguser/2.0/{$customer_id}/{$user_email}",
			CURLOPT_HTTPHEADER		=> array('Content-type: application/atom+xml', 'Authorization: GoogleLogin auth='.$this->AuthToken)
		);

		$response = $this->execute_curl($options);

		return $response;
	}
	
	/**
	 * Melakukan login SSO
	 * @param string $email Email yang akan digunakan untuk melakukan SSO
	 */
	public static function StartSSOSession($saml_request, $relay_state, $email, $session_time = 3600)
	{
		include 'xmlseclibs.php';
		
		// Inisialisasi request dan konversi ke xml
		// $saml_request = $_POST['SAMLRequest'];
		$dec_saml_req = base64_decode($saml_request);
		$xml_saml_req = gzinflate($dec_saml_req);

		//read XML
		$xml_dom = new DOMDocument();
		$xml_dom->loadXML($xml_saml_req);

		// definisi SAML Request ID dan SAML Request ACS
		$saml_req_id = '';
		$saml_req_acs = '';

		//parse saml req ID and ACS
		foreach ($xml_dom->getElementsByTagNameNS('urn:oasis:names:tc:SAML:2.0:protocol', 'AuthnRequest') as $element)
		{
			$saml_req_id = $element->getAttribute('ID');
			$saml_req_acs = $element->getAttribute('AssertionConsumerServiceURL');

			break;
		}

		// menentukan waktu start saml
		$time = time() - 300;   // waktu sekarang minus 5 menit buat jaga

		//generate saml resp
		$xml_dom = new DOMDocument('1.0', 'utf-8');
		$response = $xml_dom->createElementNS('urn:oasis:names:tc:SAML:2.0:protocol', 'samlp:Response');

		//samlp:Response
		$response->setAttribute('ID', GoogleApps::generateUniqueID(40));
		$response->setAttribute('InResponseTo', $saml_req_id);
		$response->setAttribute('Version', '2.0');
		$response->setAttribute('IssueInstant', substr(gmdate("c", $time), 0, 19) . 'Z');
		$response->setAttribute('Destination', $saml_req_acs);
		$xml_dom->appendChild($response);

		//samlp:Status
		$status = $xml_dom->createElementNS('urn:oasis:names:tc:SAML:2.0:protocol', 'samlp:Status');
		$response->appendChild($status);
		$status_code = $xml_dom->createElementNS('urn:oasis:names:tc:SAML:2.0:protocol', 'samlp:StatusCode');
		$status_code->setAttribute('Value', 'urn:oasis:names:tc:SAML:2.0:status:Success');
		$status->appendChild($status_code);

		//samlp:Assertion
		$assertion = $xml_dom->createElementNS('urn:oasis:names:tc:SAML:2.0:assertion', 'Assertion');
		$assertion->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns', 'urn:oasis:names:tc:SAML:2.0:assertion');
		$assertion->setAttribute('ID', GoogleApps::generateUniqueID(40));
		$assertion->setAttribute('IssueInstant', substr(gmdate("c", $time), 0, 19) . 'Z');
		$assertion->setAttribute('Version', '2.0');
		$response->appendChild($assertion);
		//Issuer
		$assertion->appendChild($xml_dom->createElement('Issuer', "unair.ac.id"));   // Isian issuer
		//Subject
		$subject = $xml_dom->createElement('Subject');
		$assertion->appendChild($subject);

		//NameID
		$nameid = $xml_dom->createElement('NameID', $email);   // username yang dibuat login
		$nameid->setAttribute('Format', 'urn:oasis:names:tc:SAML:2.0:nameid-format:email');
		$nameid->setAttribute('SPNameQualifier', 'google.com');
		$subject->appendChild($nameid);

		//SubjectConfirmation
		$confirmation = $xml_dom->createElement('SubjectConfirmation');
		$confirmation->setAttribute('Method', 'urn:oasis:names:tc:SAML:2.0:cm:bearer');
		$subject->appendChild($confirmation);

		//SubjectConfirmationData                
		$confirmationdata = $xml_dom->createElement('SubjectConfirmationData');
		$confirmationdata->setAttribute('InResponseTo', $saml_req_id);
		$confirmationdata->setAttribute('NotOnOrAfter', substr(gmdate("c", $time + 300), 0, 19) . 'Z');
		$confirmationdata->setAttribute('Recipient', $saml_req_acs);
		$confirmation->appendChild($confirmationdata);

		//Conditions
		$condition = $xml_dom->createElement('Conditions');
		$condition->setAttribute('NotBefore', substr(gmdate("c", $time - 30), 0, 19) . 'Z');
		$condition->setAttribute('NotOnOrAfter', substr(gmdate("c", $time + 300), 0, 19) . 'Z');
		$assertion->appendChild($condition);
		$audience_rest = $xml_dom->createElement('AudienceRestriction');
		$audience_rest->appendChild($xml_dom->createElement('Audience', 'google.com'));
		$condition->appendChild($audience_rest);

		//AuthnStatement
		$authnstat = $xml_dom->createElement('AuthnStatement');
		$authnstat->setAttribute('AuthnInstant', substr(gmdate("c", $time), 0, 19) . 'Z');
		$authnstat->setAttribute('SessionIndex', "_" . GoogleApps::generateUniqueID(30)); // bisa di isi session
		$authnstat->setAttribute('SessionNotOnOrAfter', substr(gmdate("c", $time + 300), 0, 19) . 'Z');  // session ke google (dibuat satu 5 menit buat jaga2)
		$assertion->appendChild($authnstat);
		$authncontext = $xml_dom->createElement('AuthnContext');
		$authncontext->appendChild($xml_dom->createElement('AuthnContextClassRef', 'urn:oasis:names:tc:SAML:2.0:ac:classes:Password'));
		$authnstat->appendChild($authncontext);

		//Private KEY        
		$objKey = new XMLSecurityKey(XMLSecurityKey::RSA_SHA1, array('type' => 'private'));
		$objKey->loadKey('private.pem', true);

		//Sign the Assertion
		$objXMLSecDSig = new XMLSecurityDSig();
		$objXMLSecDSig->setCanonicalMethod(XMLSecurityDSig::EXC_C14N);
		$objXMLSecDSig->addReferenceList(array($response), XMLSecurityDSig::SHA1, array('http://www.w3.org/2000/09/xmldsig#enveloped-signature', XMLSecurityDSig::EXC_C14N), array('id_name' => 'ID', 'overwrite' => false));
		$objXMLSecDSig->sign($objKey);
		$objXMLSecDSig->insertSignature($response, $status);

		//Plain XML SAML Response
		$xml_saml_resp = $xml_dom->saveXML();
		// header("Content-type: text/xml");
		// echo $xml_saml_resp; exit();

		//Encoded SAML Response
		$xml_saml_enc = base64_encode(stripslashes(substr($xml_saml_resp, strpos($xml_saml_resp, '<saml'))));

		echo '<!DOCTYPE html><html><body onload="javascript: document.forms[0].submit();">Loading, please wait...<form action="'.$saml_req_acs.'" method="post">';
		echo '<input type="hidden" name="SAMLResponse" value="'.$xml_saml_enc.'" />';
		echo '<input type="hidden" name="RelayState" value="'.$relay_state.'" />';
		echo '</form></body></html>';
		exit();
	}
	
	public static function generateUniqueID($length)
	{
		$chars = "abcdef0123456789"; $uniqueID = "";
		for ($i = 0; $i < $length; $i++) { $uniqueID .= substr($chars, rand(0, 15), 1); }
		return 'a' . $uniqueID;
	}
}
?>