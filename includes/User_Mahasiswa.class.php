<?php

/**
 * Class User_Mahasiswa
 */
class User_Mahasiswa
{
    public $ID_MHS;
    public $NIM;
    public $NAMA;
    public $NO_UJIAN;
    public $ID_PROGRAM_STUDI;
    public $ID_FAKULTAS;
    public $ID_JENJANG;
    public $TAHUN_ANGKATAN;
    public $IS_SEMESTER_LALU_CUTI; // boolean
    public $IPK;
    public $IPS;
    public $SKS_TOTAL;
    public $SKS_SEMESTER;
    public $STATUS;
}