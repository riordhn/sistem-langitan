<?php

/** 
 * MyOracle Class
 * @author Sugeng Kurniawan <sugeng.kurniawan@yahoo.com.sg>
 * @author Fathoni <m.fathoni@mail.com>
 * @start Wednesday, March 09, 2011
 * @copyright 2011 Universitas Airlangga
 */

class MyOracle
{
	public $db_host;
	public $db_port;
	public $db_service_name;
	public $db_server_type;
	public $db_instance_name;
	public $db_charset;
	public $db_user;
	public $db_pass;
	
	/**
	 * 
	 * @var string TNS Names
	 */
	public $tns;

	const SQLVALUE_CHAR = "CHAR";
	const SQLVALUE_VARCHAR2 = "VARCHAR2";
	const SQLVALUE_NUMBER = "NUMBER";
	const SQLVALUE_DATE = "DATE";
	const SQLVALUE_TIMESTAMP= "TIMESTAMP";
	const SQLVALUE_INTERVAL = "INTERVAL";
	const SQLVALUE_BLOB = "BLOB";
	const SQLVALUE_CLOB = "CLOB";
	const SQLVALUE_BFILE = "BFILE";
	const SQLVALUE_XMLType = "XMLType";
	
	/**
	 * Oracle connection identifier
	 * @var Resource 
	 */
	public $conn;
	
	/**
	 * A valid OCI8 statement identifier created by oci_parse() and executed by oci_execute(), or a REF CURSOR statement identifier.
	 * @var Resource 
	 */
	private $stid;

	// class-internal variables - DO NOT CHANGE
	private $active_row = -1;			// current row
	private $error_desc = "";			// last error string
	private $error_number = 0;			// last error number
	private $in_transaction = false;	// used for transactions
	private $last_insert_id;			// last id of record inserted
	private $last_result;				// last query result
	public	$last_sql = "";				// last query (dipublic-kan oleh Fathoni untuk keperluan debugging)
	private $link_ress = 0;				// link resource
	private $time_diff = 0;				// holds the difference in time
	private $time_start = 0;			// start time for the timer
	private $last_execute;				// status eksekusi query terakhir

	public $num_rows = 0;				// mengembalikan nilai num_rows

	/**
	 * Determines if an error throws an exception
	 *
	 * @var boolean Set to true to throw error exceptions
	 */
	public $ThrowExceptions = false;

	/**
	 * Constructor: Opens the connection to the database
	 *
	 * @param boolean $auto_connect (Optional) Auto-connect when object is created
	 */
	function __construct($auto_connect = true) 
	{
		if ($auto_connect)
		{
			$this->Open();
		}
	}

	function _convert($row) {
		foreach ((array) $row as $key => $val) {
			if (is_a($val, 'OCI-Lob')) { // jika value = Lob
				$row[$key] = $val->load(); // muat value...
			}
		}
		return $row;
	}

	/**
	 * Connect to specified Oracle server
	 *
	 * @param string $type_of_connection <p>null | Using oci_connect()</p><p>new | Using oci_new_connect()</p><p>persistent | Using oci_pconnect()</p>
	 * @return resource <p>Returns a connection identifier or <b><code>FALSE</code></b> on error.</p>
	 */
	function Open($type_of_connection = 'normal')
	{
		$this->db_host			= getenv('DB_HOST');
		$this->db_port			= getenv('DB_PORT');
		$this->db_service_name	= getenv('DB_SERVICE_NAME');
		$this->db_server_type	= getenv('DB_SERVER_TYPE');
		$this->db_instance_name	= getenv('DB_INSTANCE_NAME');
		$this->db_charset		= getenv('DB_CHARSET');
		$this->db_user			= getenv('DB_USER');
		$this->db_pass			= getenv('DB_PASS');
		
		// Setting TNS
		// Syntax : [//]host_name[:port][/service_name][:server_type][/instance_name]
		$this->tns = $this->db_host .
			($this->db_port ? ':' . $this->db_port : '') .
			($this->db_service_name ? '/' . $this->db_service_name : '') .
			($this->db_server_type ? ':' . $this->db_server_type : '') .
			($this->db_instance_name ? '/' . $this->db_instance_name : '');

		if ($type_of_connection == 'new')
		{
			$this->conn = oci_new_connect($this->db_user, $this->db_pass, $this->tns, $this->db_charset);
		} 
		else if ($type_of_connection == 'normal')
		{
			$this->conn = oci_connect($this->db_user, $this->db_pass, $this->tns, $this->db_charset);
		} 
		else if ($type_of_connection == 'persistent')
		{
			$this->conn = oci_pconnect($this->db_user, $this->db_pass, $this->tns, $this->db_charset);
		}
		
		return $this->conn;
	}

	/**
	 * Determines if a valid connection to the database exists
	 * @return boolean TRUE idf connectect or FALSE if not connected
	 */
	function Connected() 
	{
		//if (gettype($this->conn) == "resource") {
		if ($this->conn) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	function Quote($string) {
		return "'" . str_replace("'", "''", $string) . "'";
	}
	
	function Query($query)
	{
		// Jika resource koneksi tidak ada, kembalikan nilai false
		if ( ! is_resource($this->conn)) {
			return false;
		}

		// Simpan last query dan stid
		$this->last_sql = $query;
		$this->stid = oci_parse($this->conn, $query);
		
		// Jika oci_parse gagal
		if ( ! $this->stid)
		{
			$this->Error = oci_error($this->conn);
			
			// Log Parse Fail : 30/08/2015 - fathoni
			//$fd = fopen("/var/log/php-fpm/query-parse-fail.log", "a");
			//fwrite($fd, date('Y-m-d H:i:s') . " " . $_SERVER['HTTP_HOST'] . " " . $_SERVER['SCRIPT_FILENAME'] . "\r\n" . str_replace(array("\r\n", "\n", "\r"), " ", $query) . "\r\n" . $error["message"] . "\r\n");
			//fclose($fd);

			// dikembalikan
			return $this->stid;
		}

		// Waktu mulai eksekusi script
		$time_start = microtime(true);
		
		// Jika masih dalam transaksi
		if ($this->in_transaction)
			$this->last_execute = @oci_execute($this->stid, OCI_NO_AUTO_COMMIT);
		else
			$this->last_execute = @oci_execute($this->stid);
		
		// Jika eksekusi berhasil
		if ($this->last_execute)
		{			
			$this->affected_rows = oci_num_rows($this->stid);
		}
		else
		{
			$this->Error = oci_error($this->stid);
		}

		return $this->last_execute;
	}

	/** Parse and Execute query
	 * returns boolean, TRUE if success
	 */
	function ParseExecute($query) {
		$this->last_sql = $query;
		$this->stid = oci_parse($this->conn, $query);
		if (!$this->stid) {
			$error = oci_error($this->conn);
			$this->Error = "";// $error["message"]; alasan keamanan @ Wednesday, April 04, 2012
			return false;
		}
		//set_error_handler(array($this, '_error'));
		// must be fix, for fetch method
		if ($this->in_transaction)
			$return = @oci_execute($this->stid, OCI_NO_AUTO_COMMIT);
		else
			$return = @oci_execute($this->stid);
		//restore_error_handler();
		if ($return) {
			if (oci_num_fields($this->stid)) {
				//return new MyResult($this->stid);
			}
			$this->affected_rows = oci_num_rows($this->stid);
		}
		return $return;
	}
	
	/**
	 * Prepares an Oracle statement for execution
	 * @param string $query
	 * @return resource Returns a statement handle on success, or FALSE on error
	 */
	function Parse($query)
	{
		$this->last_sql = $query;

		// Pastikan resource connection ada
		if ( ! $this->conn) {
			return false;
		}

		$this->stid = oci_parse($this->conn, $query);
		
		if ( ! $this->stid)
		{
			$this->Error = oci_error($this->conn);
			return $this->stid;
		}
		else
		{
			return $this->stid;
		}
	}
	
	/**
	 * Melakukan binding data pada query yang telah di Parse()
	 * @param string $bind_name
	 * @param string $bind_value
	 * @param object $stid Statement resource
	 */
	function BindByName($bind_name, $bind_value, $stid = null)
	{
		$stid = ($stid == null) ? $this->stid : $stid;

		if ( ! $stid) {
			return FALSE;
		}

		return oci_bind_by_name($stid, $bind_name, $bind_value);
	}

	function Execute($stid = null)
	{
		$stid = ($stid == null) ? $this->stid : $stid;
		
		// Jika masih dalam transaksi
		if ($this->in_transaction)
			$this->last_execute = oci_execute($stid, OCI_NO_AUTO_COMMIT);
		else
			$this->last_execute = oci_execute($stid);
		
		// Jika eksekusi berhasil
		if ($this->last_execute)
		{
			$this->affected_rows = oci_num_rows($stid);
		}
		else
		{
			$this->Error = oci_error($stid);
			return false;
		}
		
		return $this->last_execute;
	}
	
	/**
	 * Melakukan select query yg hasilnya langsung berupa array
	 * @param string $query 
	 */
	function QueryToArray($query)
	{	
		if ($this->Query($query))
		{
			$this->num_rows = oci_fetch_all($this->stid, $result, null, null, OCI_FETCHSTATEMENT_BY_ROW);

			return $result;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Mendapatkan bentuk XML dari query
	 * @param string $query 
	 */
	function QueryToXML($query)
	{
		$rows = $this->QueryToArray($query);
	
		$xml = new SimpleXMLElement("<root></root>");

		foreach ($rows as $row)
		{
			$child = $xml->addChild('row');

			foreach (array_keys($row) as $col)
			{
				$child->addChild($col, $row[$col]);
			}
		}
		
		return $xml->asXML();
	}
	
	/**
	 * Melakukan query yg hasilnya mengambil 1 kolom dan 1 row
	 * @param string $query 
	 */
	function QuerySingle($query)
	{
		$result = $this->Query($query);
		if ($result)
		{
			$row = $this->FetchRow();
			return $row[0];
		}
		else
		{
			return null;
		}
	}

	function MultiQuery($query) {
		$this->last_sql = $query;
		return $result = $this->Query($query);
	}

	function NextResult() {
		return false;
	}

	function StoreResult($query, $field = 1) {
		$this->result = $this->Query($query);
		if (!is_object($this->result) || !oci_fetch($this->result)) {
			return false;
		}
		return oci_result($this->result, $field);
	}

	/** fetch all 
	 * fetches multiple rows from a query into a two-dimensional array
	 * This function can be called only once for each query executed with oci_execute()
	 */
	function FetchAll($result) {
		return $this->_convert(oci_fetch_all($result));
	}

	/** fetch array 
	 * @flags = OCI_BOTH, OCI_ASSOC, OCI_NUM, OCI_RETURN_NULLS, OCI_RETURN_LOBS
	 * @flags default = OCI_BOTH => OCI_ASSOC + OCI_NUM 
	 * returns an array containing the next result-set row of a query
	 */
	function FetchArray($stid = null, $flags = null)
	{
		$stid = ($stid == null) ? $this->stid : $stid;
		
		// Jika stid dan eksekusi terakhir tidak bermasalah
		if (is_resource($stid) && $this->last_execute == true)
		{
			if ($flags == null)
			{
				return $this->_convert(oci_fetch_array($stid));
			}
			else
			{
				return $this->_convert(oci_fetch_array($stid, $flags));
			}
		}
		else
		{
			return false;
		}
	}

	/** fetch assoc
	 * this is identical to fetch array with flags = OCI_ASSOC + OCI_RETURN_NULLS
	 * returns the next row from a query as an associative array
	 */
	function FetchAssoc($stid = null)
	{
		$stid = ($stid == null) ? $this->stid : $stid;
		
		// jika stid tidak bermasalah
		if (is_resource($stid) && $this->last_execute == true)
		{
			return $this->_convert(oci_fetch_assoc($stid));
		}
		else
		{
			return false;
		}
	}

	/** fetch object
	 * returns the next row from a query as an object
	 */
	function FetchObject($stid = null) {
		$stid = ($stid == null) ? $this->stid : $stid;
		return $this->_convert(oci_fetch_object($stid));
	}

	/** fetch row
	 * this is identical to fetch array with flags = OCI_NUM + OCI_RETURN_NULLS
	 * returns the next row from a query as a numeric array
	 */
	function FetchRow($stid = null)
	{
		$stid = ($stid == null) ? $this->stid : $stid;
		
		if (is_resource($stid) && $this->last_execute == true)
		{
			return $this->_convert(oci_fetch_row($stid));
		}
		else
		{
			return false;
		}
	}

	/** define by name
	 * associates a PHP variable with a column for query fetches using oci_fetch()
	 * this call must occur before executing query()
	 */
	function DefineByName($stid, $column_name, $variable, $type = null) {
		if ($type == null) {
			return oci_define_by_name($stid, $column_name, $variable);
		} else {
			return oci_define_by_name($stid, $column_name, $variable, $type);
		}
	}

	function Fetch($stid = null) {
		$stid = ($stid == null) ? $this->stid : $stid;
		return $this->_convert(oci_fetch($stid));
	}

	function FetchField($result) {
		$column = $this->_offset++;
		$return = new stdClass;
		$return->name = oci_field_name($result, $column);
		$return->orgname = $return->name;
		$return->type = oci_field_type($result, $column);
		$return->charsetnr = (preg_match("raw|blob|bfile", $return->type) ? 63 : 0); // 63 - binary
		return $return;
	}

	/**
	 * mendapatkan nama tablespace_name (database)
	 * @return string name of tablespace
	 */
	// 
	function GetDatabases() {
		return get_vals("SELECT tablespace_name FROM user_tablespaces");
	}

	/**
	 * Clears the internal variables from any error information
	 *
	 */
	function ResetError() {
		$this->error_desc = '';
		$this->error_number = 0;
	}

	/**
	 * frees memory used by the query results and returns the function result
	 * @return boolean TRUE on success or FALSE on failure
	 */
	function Release() {
		$this->ResetError();
		$success = @oci_free_statement($this->stid);
		if (! $success) $this->SetError();
		return $success;
	}

	/**
	 * Returns the last Oracle error as text
	 * @return string Error text from last known error
	 */
	function _error($errno, $error) {
		if (ini_bool("html_errors")) {
			$error = html_entity_decode(strip_tags($error));
		}
		$error = preg_replace('^\[^:]*: ', '', $error);
		$this->error = $error;
	}

	function Error() {
		$error = $this->error_desc;
		if (empty($error)) {
			if ($this->error_number <> 0) {
				$error = "Unknown Error (#" . $this->error_number . ")";
			} else {
				$error = false;
			}
		} else {
			if ($this->error_number > 0) {
				$error .= " (#" . $this->error_number . ")";
			}
		}
		return $error;
	}

	/**
	 * Sets the local variables with the last error information
	 * @param string $errorMessage The error description
	 * @param integer $errorNumber The error number
	 */
	function SetError($errorMessage = "", $errorNumber = 0) {
		try {
			if (strlen($errorMessage) > 0) {
				$this->error_desc = $errorMessage;
			} else {
				if ($this->Connected()) {
					$this->error_desc = oci_error($this->conn)['message'];
				} else {
					$this->error_desc = oci_error()['message'];
				}
			}
			if ($errorNumber <> 0) {
				$this->error_number = $errorNumber;
			} else {
				if ($this->Connected()) {
					$this->error_number = oci_error($this->conn)['code'];
				} else {
					$this->error_number = oci_error()['code'];
				}
			}
		} catch (Exception $e) {
			$this->error_desc = $e->getMessage();
			$this->error_number = -999;
		}
		if ($this->ThrowExceptions) {
			if (isset($this->error_desc) && $this->error_desc != NULL) {
				throw new Exception($this->error_desc . ' (' . __LINE__ . ')');
			}
		}
	}

	/** xxx
	 * Returns the last query as a JSON document
	 * @return string JSON containing all records listed
	 */
	function GetJSON() {
		if ($this->last_result) {
			if ($this->RowCount() > 0) {
				for ($i = 0, $il = oci_num_fields($this->last_result); $i < $il; $i++) {
					$types[$i] = oci_field_type($this->last_result, $i);
				}
				$json = '[';
				$this->MoveFirst();
				while ($member = oci_fetch_object($this->last_result)) {
					$json .= json_encode($member) . ",";
				}
				$json .= ']';
				$json = str_replace("},]", "}]", $json);
			} else {
				$json = 'null';
			}
		} else {
			$this->active_row = -1;
			$json = 'null';
		}
		return $json;
	}

	/**
	 * Returns the last query as an XML Document
	 * @return string XML containing all records listed
	 */
	function GetXML() {
		// Create a new XML document
		$doc = new DomDocument('1.0'); // ,'UTF-8');
		// Create the root node
		$root = $doc->createElement('root');
		$root = $doc->appendChild($root);

		// If there was a result set
		if (is_resource($this->last_result)) {

			// Show the row count and query
			$root->setAttribute('rows', ($this->RowCount() ? $this->RowCount() : 0));
			$root->setAttribute('query', $this->last_sql);
			$root->setAttribute('error', "");

			// process one row at a time
			$rowCount = 0;
			while ($row = oci_fetch_assoc($this->last_result)) {

				// Keep the row count
				$rowCount = $rowCount + 1;

				// Add node for each row
				$element = $doc->createElement('row');
				$element = $root->appendChild($element);
				$element->setAttribute('index', $rowCount);

				// Add a child node for each field
				foreach ($row as $fieldname => $fieldvalue) {
					$child = $doc->createElement($fieldname);
					$child = $element->appendChild($child);

					// $fieldvalue = iconv("ISO-8859-1", "UTF-8", $fieldvalue);
					$fieldvalue = htmlspecialchars($fieldvalue);
					$value = $doc->createTextNode($fieldvalue);
					$value = $child->appendChild($value);
				} // foreach
			} // while
		} else {
			// Process any errors
			$root->setAttribute('rows', 0);
			$root->setAttribute('query', $this->last_sql);
			if ($this->Error()) {
				$root->setAttribute('error', $this->error());
			} else {
				$root->setAttribute('error', "No query has been executed.");
			}
		}

		// Show the XML document
		return $doc->saveXML();
	}
	
	/**
	 * Mendefinisikan bahwa query akan diberlakukan sebagai transaksi
	 */
	function BeginTransaction()
	{
		$this->in_transaction = true;
	}
	
	/**
	 * Mengcommit transaksi
	 * @return bool 
	 */
	function Commit()
	{
		$this->in_transaction = false;
		return oci_commit($this->conn);
	}
	
	/**
	 * Rollback transaksi
	 * @return bool 
	 */
	function Rollback()
	{
		$this->in_transaction = false;
		return oci_rollback($this->conn);
	}
	
	/**
	 * Mendapatkan jumlah baris yg tereksekusi
	 * @param object $stid
	 * @return int 
	 */
	function NumRows($stid = null)
	{
		$stid = ($stid == null) ? $this->stid : $stid;
		
		// jika resource stid masih ada
		if (is_resource($stid))			
			return $this->_convert(oci_num_rows($stid));
		else
			return false;
	}
	
	/**
	 * Mendapatkan jumlah field dari query terakhir
	 * @return int Jumlah field
	 */
	function FieldCount()
	{
		return oci_num_fields($this->stid);
	}
	
	/**
	 * Mendapatkan nama field
	 * @param int $field
	 * @return string Nama field 
	 */
	function FieldName($field)
	{
		return oci_field_name($this->stid, $field);
	}
	
	/**
	 * Mendapatkan tipe field
	 * @param int $field
	 * @return string Tipe field
	 */
	function FieldType($field)
	{
		return oci_field_type($this->stid, $field);
	}

	/**
	 * Close current Oracle connection
	 * @return object Returns TRUE on success or FALSE on error
	 */
	function Close() {
		$this->ResetError();
		$this->active_row = -1;
		$success = $this->Release();
		if ($success) {
			$success = @oci_close($this->conn);
			if (!$success) {
				$this->SetError();
			} else {
				unset($this->last_sql);
				unset($this->last_result);
				//unset($this->conn);
			}
		}
		$this->db_pass = md5(date("Y-m-d H:i:s"));
		return $success;
	}

	/**
	 * Destructor: closes the connection to the database
	 *
	 */
	function __destruct() {
		//$this->Close();
	}

}