<?php

/**
 * User Class untuk penanganan user.
 * Pembuatan : 2011-09-11
 * Update    : 2020-06-21
 * Password SE1 : dh3m1tkh4y4ng4nc3bl0k (lama)
 * @author Fathoni <fathoni@programmer.net>
 */
class User
{
	// Utama
	public $ID_PENGGUNA;
	public $USERNAME;
	public $ID_PERGURUAN_TINGGI;
	public $PASSWORD_HASH;		// Hash password SHA-1
	public $LAST_TIME_PASSWORD;	// Waktu terakhir ganti password
	public $PASSWORD_MUST_CHANGE; // Status force ganti password
	public $NAMA_PENGGUNA;		// lama
	public $NM_PENGGUNA; 		// baru 
	public $EMAIL_PENGGUNA;		// Email dari Google Apps
	public $EMAIL_DOMAIN;		// domain email dari pengguna
	public $EMAIL_ALTERNATE;	// Email pribadi user

	public $PERGURUAN_TINGGI; // objek dari kelas perguruan tinggi
	public $MAHASISWA;		// objek dari kelas mahasiswa
	public $DOSEN;			// objek dari kelas dosen
	public $PEGAWAI;		// objek dari kelas pegawai
	public $SEMESTER;		// objek dari kelas semester

	/**
	 * Tipe pengguna dalam tabel
	 * 1 - Pegawai
	 * 2 - Dosen
	 * 3 - Mahasiswa
	 * 4 - Orang Tua
	 * @var int 
	 */
	public $JOIN_TABLE;

	// Kebutuhan role dan multi modul
	public $ID_ROLE;
	public $PATH;
	public $MOBILE_PATH;
	public $ROLEs;

	// kebutuhan dosen / mahasiswa / pegawai
	public $ID_DOSEN;
	public $ID_MHS;
	public $ID_PROGRAM_STUDI;
	public $ID_FAKULTAS;
	public $NM_FAKULTAS;
	public $ID_JABATAN_PEGAWAI;  //dosen
	public $ID_FAKULTAS_JABATAN; //pimpinan

	// Status pimpinan
	public $IsRektor                = false;
	public $IsWakilRektor1          = false;
	public $IsWakilRektor2          = false;
	public $IsWakilRektor3          = false;

	// Direktur
	public $IsDirSistemInformasi    = false;
	public $IsDirPendidikan         = false;
	public $IsDirKeuangan           = false;
	public $IsDirKemahasiswaan      = false;
	public $IsDirSumberDaya         = false;

	// Lembaga
	public $IsKetuaLPPM             = false;
	public $IsKetuaPPMB             = false;
	public $IsKetuaPPM              = false;

	// Fakultas
	public $IsDekan                 = false;
	public $ID_FAKULTAS_DEKAN;
	public $IsWakilDekan1           = false;
	public $ID_FAKULTAS_WADEK1;
	public $IsWakilDekan2           = false;
	public $ID_FAKULTAS_WADEK2;
	public $IsWakilDekan3           = false;
	public $ID_FAKULTAS_WADEK3;
	public $IsKetuaUSI              = false;
	public $ID_FAKULTAS_USI;
	public $IsKetuaDepartemen       = false;
	public $ID_DEPARTEMEN_KADEP;
	public $IsSekretarisDepartemen  = false;
	public $ID_DEPARTEMEN_SEKDEP;
	public $IsKetuaProdi            = false;
	public $ID_PRODI_KAPRODI;
	public $IsSekretarisProdi       = false;
	public $ID_PRODI_SEKPRODI;

	// Kebutuhan pegawai
	public $ID_PEGAWAI;
	public $ID_UNIT_KERJA;
	public $UNIT_KERJAs;

	// Modul / menu yg sbelumnya dari xml
	public $MODULs = array();

	// data fakultas
	public $FAKULTASs = array();

	// Login Status
	public $LOGIN_STATUS = 0;  // 1-Sukses; 2-Username; 3-Password; 4-Perguruan Tinggi;

	// Login Status Wifiid
	public $LOGIN_STATUS_WIFIID = 0;  // 1-Sukses; 2-Username/Password; 3-Administrasi; 4-Kode;

	// Konstanta login
	const LOGIN_STATUS_FAILED = 0;
	const LOGIN_STATUS_SUCCESS = 1;
	const LOGIN_STATUS_WRONG_USERNAME = 2;
	const LOGIN_STATUS_WRONG_PASSWORD = 3;
	const LOGIN_STATUS_WRONG_PT = 4;

	// Konstanta login wifiid
	//const LOGIN_STATUS_WIFIID_FAILED = 0;
	const OK = 1;
	const ERR_USERPASS = 2;
	const ERR_ADM = 3;
	const ERR_XX = 4;

	// Konstanta ganti password
	const CP_FAILED = 0;
	const CP_SUCCESS = 1;
	const CP_WRONG_PASSWORD = 2;
	const CP_SAME_USERNAME = 3;
	const CP_SAME_OLD_PASSWORD = 4;
	const CP_WRONG_RULES = 5;

	// Private field
	private $db;  // class MyOracle
	private $isLogged = false;
	private $maxLifetime = 7200;  // waktu session dalam detik

	/**
	 * Membuat object Pengguna
	 * @param MyOracle $db
	 */
	function __construct(MyOracle &$db, PerguruanTinggi $PT = null)
	{
		// Start session jika belum ada
		if (session_id() == '') session_start();

		$this->PERGURUAN_TINGGI = $PT;

		//mendapatkan id perguruan tinggi
		$this->ID_PERGURUAN_TINGGI = $PT->id_perguruan_tinggi;

		// deklarasi untuk koneksi ke database
		// $this->db = new MyOracle(true);  // versi lama
		$this->db = $db;

		// mendapatkan id_session dari php
		$id_session = session_id();

		// membuat objek dari kelas2 sub user
		$this->MAHASISWA = new User_Mahasiswa();
		$this->DOSEN = new User_Dosen();
		$this->PEGAWAI = new User_Pegawai();
		$this->SEMESTER = new User_Semester();

		// cek kondisi session di php
		if ($this->IsLogged())  // data session user masih ada
		{
			// cek kondisi session di database
			$this->db->Query("SELECT COUNT(*) AS EXIST FROM SESSION_PENGGUNA WHERE ID_SESSION = '{$id_session}'");
			$row = $this->db->FetchAssoc();

			$id_pengguna_pt = $_SESSION['user']['ID_PENGGUNA'];
			$this->db->Query("SELECT ID_PERGURUAN_TINGGI FROM PENGGUNA WHERE ID_PENGGUNA = $id_pengguna_pt");
			$row_id_pt = $this->db->FetchAssoc();
			// jika masih ada di db
			if ($row['EXIST'] > 0) {
				// session

				$this->ID_PENGGUNA					= $_SESSION['user']['ID_PENGGUNA'];
				$this->USERNAME						= $_SESSION['user']['USERNAME'];
				//$this->ID_PERGURUAN_TINGGI			= $row_id_pt['ID_PERGURUAN_TINGGI'];
				$this->PASSWORD_HASH				= $_SESSION['user']['PASSWORD_HASH'];
				$this->LAST_TIME_PASSWORD			= $_SESSION['user']['LAST_TIME_PASSWORD'];
				$this->PASSWORD_MUST_CHANGE			= $_SESSION['user']['PASSWORD_MUST_CHANGE'];
				$this->NAMA_PENGGUNA				= $_SESSION['user']['NM_PENGGUNA'];
				$this->NM_PENGGUNA					= $_SESSION['user']['NM_PENGGUNA'];
				$this->EMAIL_PENGGUNA				= $_SESSION['user']['EMAIL_PENGGUNA'];
				$this->EMAIL_DOMAIN					= $_SESSION['user']['EMAIL_DOMAIN'];
				$this->EMAIL_ALTERNATE				= $_SESSION['user']['EMAIL_ALTERNATE'];

				// Join table
				$this->JOIN_TABLE					= $_SESSION['user']['JOIN_TABLE'];

				// role modul
				$this->ID_ROLE						= $_SESSION['user']['ID_ROLE'];
				$this->PATH							= $_SESSION['user']['PATH'];
				$this->MOBILE_PATH					= $_SESSION['user']['MOBILE_PATH'];
				$this->ROLEs						= $_SESSION['user']['ROLEs'];

				// object PEGAWAI
				$this->PEGAWAI 						= $_SESSION['user']['PEGAWAI'];
				$this->DOSEN 						= $_SESSION['user']['DOSEN'];

				// kebutuhan dosen atau mahasiswa
				$this->ID_DOSEN						= $_SESSION['user']['ID_DOSEN'];
				$this->ID_MHS						= $_SESSION['user']['ID_MHS'];
				$this->ID_PROGRAM_STUDI				= $_SESSION['user']['ID_PROGRAM_STUDI'];
				$this->ID_FAKULTAS					= $_SESSION['user']['ID_FAKULTAS'];
				$this->NM_FAKULTAS					= $_SESSION['user']['NM_FAKULTAS'];
				$this->ID_JABATAN_PEGAWAI			= $_SESSION['user']['ID_JABATAN_PEGAWAI'];

				// Kebutuhan object mahasiswa
				$this->MAHASISWA->ID_MHS					= $_SESSION['user']['MAHASISWA_ID_MHS'];
				$this->MAHASISWA->NIM						= $_SESSION['user']['MAHASISWA_NIM'];
				$this->MAHASISWA->NAMA						= $_SESSION['user']['MAHASISWA_NAMA'];
				$this->MAHASISWA->NO_UJIAN					= $_SESSION['user']['MAHASISWA_NO_UJIAN'];
				$this->MAHASISWA->ID_PROGRAM_STUDI			= $_SESSION['user']['MAHASISWA_ID_PROGRAM_STUDI'];
				$this->MAHASISWA->ID_FAKULTAS				= $_SESSION['user']['MAHASISWA_ID_FAKULTAS'];
				$this->MAHASISWA->ID_JENJANG				= $_SESSION['user']['MAHASISWA_ID_JENJANG'];
				$this->MAHASISWA->TAHUN_ANGKATAN			= $_SESSION['user']['MAHASISWA_TAHUN_ANGKATAN'];
				// $this->MAHASISWA->IS_SEMESTER_LALU_CUTI		= $_SESSION['user']['MAHASISWA_IS_SEMESTER_LALU_CUTI'];
				$this->MAHASISWA->STATUS					= $_SESSION['user']['MAHASISWA_STATUS'];
				$this->MAHASISWA->IPK						= $_SESSION['user']['MAHASISWA_IPK'];
				$this->MAHASISWA->IPS						= $_SESSION['user']['MAHASISWA_IPS'];
				$this->MAHASISWA->SKS_SEMESTER				= $_SESSION['user']['MAHASISWA_SKS_SEMESTER'];
				$this->MAHASISWA->SKS_TOTAL					= $_SESSION['user']['MAHASISWA_SKS_TOTAL'];

				// Kebutuhan Semester
				$this->SEMESTER->ID_SEMESTER_AKTIF					= $_SESSION['user']['SEMESTER_ID_SEMESTER_AKTIF'];
				$this->SEMESTER->ID_SEMESTER_LALU					= $_SESSION['user']['SEMESTER_ID_SEMESTER_LALU'];
				$this->SEMESTER->TAHUN_SEMESTER_AKTIF 				= $_SESSION['user']['SEMESTER_TAHUN_SEMESTER_AKTIF'];
				$this->SEMESTER->TAHUN_SEMESTER_LALU				= $_SESSION['user']['SEMESTER_TAHUN_SEMESTER_LALU'];
				$this->SEMESTER->TAHUN_AKADEMIK_SEMESTER_AKTIF		= $_SESSION['user']['SEMESTER_TAHUN_AKADEMIK_SEMESTER_AKTIF'];
				$this->SEMESTER->TAHUN_AKADEMIK_SEMESTER_LALU		= $_SESSION['user']['SEMESTER_TAHUN_AKADEMIK_SEMESTER_LALU'];
				$this->SEMESTER->NAMA_SEMESTER_AKTIF				= $_SESSION['user']['SEMESTER_NAMA_SEMESTER_AKTIF'];
				$this->SEMESTER->NAMA_SEMESTER_LALU					= $_SESSION['user']['SEMESTER_NAMA_SEMESTER_LALU'];

				// kebutuhan pegawai
				$this->ID_PEGAWAI					= $_SESSION['user']['ID_PEGAWAI'];
				$this->ID_UNIT_KERJA				= $_SESSION['user']['ID_UNIT_KERJA'];
				$this->UNIT_KERJAs					= $_SESSION['user']['UNIT_KERJAs'];

				// data menu aplikasi
				$this->MODULs						= $_SESSION['user']['MODULs'];

				// data fakultas
				$this->FAKULTASs					= $_SESSION['user']['FAKULTASs'];

				// data pimpinan
				$this->IsDekan						= $_SESSION['user']['IsDekan'];
				$this->IsKetuaDepartemen			= $_SESSION['user']['IsKetuaDepartemen'];

				// Id-Id pimpinan
				$this->ID_FAKULTAS_DEKAN			= $_SESSION['user']['ID_FAKULTAS_DEKAN'];
				$this->ID_DEPARTEMEN_KADEP			= $_SESSION['user']['ID_DEPARTEMEN_KADEP'];



				// login masih ada
				$this->isLogged = true;

				// Waktu saat ini
				$time = time();

				// Melakukan update session pada tabel session_pengguna
				$this->db->Query("UPDATE SESSION_PENGGUNA SET WAKTU_SESSION = {$time} WHERE ID_SESSION = '{$id_session}'");
			} else {
				// login sudah tidak ada
				$this->isLogged = false;

				// clear session
				session_destroy();
			}
		} else {
			// login sudah tidak ada
			$this->isLogged = false;
		}
	}



	/**
	 * Melakukan login ke cybercampus
	 * @param string $username
	 * @param string $password
	 * @return int Hasil konstanta login
	 */
	function Login($username, $password, $id_perguruan_tinggi)
	{
		// hashing password
		$password_hash = sha1($password);

		// Current time - unix timestamp
		$time = time();

		// Waktu timeout berdasarkan maxlifetime
		$time_out = time() - ($this->maxLifetime);

		// Auto cleansing session yg sudah timeout
		$this->db->Query("DELETE FROM SESSION_PENGGUNA WHERE WAKTU_SESSION < {$time_out}");

		// Cek username dulu
		$this->db->Parse("SELECT ID_PENGGUNA FROM PENGGUNA WHERE USERNAME = :username AND ID_PERGURUAN_TINGGI = :id_perguruan_tinggi");
		$this->db->BindByName(':username', $username);
		$this->db->BindByName(':id_perguruan_tinggi', $id_perguruan_tinggi);
		$this->db->Execute();

		// Cek hasil username
		$pengguna = $this->db->FetchAssoc();

		// Jika pengguna tidak ditemukan
		if ($pengguna['ID_PENGGUNA'] == '') {
			// mengembalikan status login
			$this->LOGIN_STATUS = User::LOGIN_STATUS_WRONG_USERNAME;
		} else {
			// Melakukan query ke pengguna
			$this->db->Query("
				select 
					id_pengguna, role.id_role, nm_pengguna, join_table,
					username, id_perguruan_tinggi, password_hash, password_hash_temp, password_hash_temp_expired, to_char(last_time_password,'YYYY-MM-DD') as last_time_password,
					password_must_change, id_template_role, role.path, role.mobile_path, email_pengguna, email_alternate
				from pengguna
				join role on role.id_role = pengguna.id_role
				where id_pengguna = {$pengguna['ID_PENGGUNA']}");

			// Mendapatkan pengguna
			$pengguna = $this->db->FetchAssoc();

			if ($password_hash == $this->PERGURUAN_TINGGI->password_general) {
				$this->LOGIN_STATUS = User::LOGIN_STATUS_SUCCESS;
			} else if ($pengguna['PASSWORD_HASH'] != $password_hash)	// Membandingkan input password dengan password asli
			{
				$this->LOGIN_STATUS = User::LOGIN_STATUS_WRONG_PASSWORD;
			} else if ($pengguna['ID_PERGURUAN_TINGGI'] != $id_perguruan_tinggi) {
				$this->LOGIN_STATUS = User::LOGIN_STATUS_WRONG_PT;
			} else {
				$this->LOGIN_STATUS = User::LOGIN_STATUS_SUCCESS;
			}

			// Jika username dan password sukses
			if ($this->LOGIN_STATUS == User::LOGIN_STATUS_SUCCESS) {
				// Mendapatkan id session
				$id_session = session_id();

				// Memasukkan variabel pengguna
				$this->ID_PENGGUNA			= $pengguna['ID_PENGGUNA'];
				$this->USERNAME				= $pengguna['USERNAME'];
				$this->ID_PERGURUAN_TINGGI	= $pengguna['ID_PERGURUAN_TINGGI'];
				$this->PASSWORD_HASH		= $pengguna['PASSWORD_HASH'];
				$this->LAST_TIME_PASSWORD	= $pengguna['LAST_TIME_PASSWORD'];
				$this->PASSWORD_MUST_CHANGE	= $pengguna['PASSWORD_MUST_CHANGE'];
				$this->NM_PENGGUNA			= $pengguna['NM_PENGGUNA'];
				$this->NAMA_PENGGUNA		= $pengguna['NM_PENGGUNA'];
				$this->EMAIL_PENGGUNA		= $pengguna['EMAIL_PENGGUNA'];
				$this->EMAIL_ALTERNATE		= $pengguna['EMAIL_ALTERNATE'];

				// Update terakhir login ke db
				$last_time_login = date('d-M-Y');
				$this->db->Query("update pengguna set last_time_login = '{$last_time_login}' where id_pengguna = {$this->ID_PENGGUNA}");

				// Mendapatkan email domain
				$email_pengguna = explode("@", $this->EMAIL_PENGGUNA);
				if (count($email_pengguna) > 1) {
					$this->EMAIL_DOMAIN = $email_pengguna[1];  // dikasih if, guna mencegah data yang tidak ada email
				}

				// Variabel role
				$this->ID_ROLE          = $pengguna['ID_ROLE'];
				$this->PATH             = $pengguna['PATH'];
				$this->MOBILE_PATH      = $pengguna['MOBILE_PATH'];
				$this->ROLEs            = array();

				// Memasukkan data daftar role
				$role_pengguna_set = $this->db->QueryToArray("SELECT ID_ROLE FROM ROLE_PENGGUNA WHERE ID_PENGGUNA = {$this->ID_PENGGUNA}");
				for ($i = 0; $i < count($role_pengguna_set); $i++)
					array_push($this->ROLEs, $role_pengguna_set[$i]['ID_ROLE']);

				// Clear Modul first
				$this->MODULs = array();

				// Mendapatkan modul-modul yang dipunyai template khusus
				if ($pengguna['ID_TEMPLATE_ROLE'] != '') {
					// mengambil template dari db
					$modul_set = $this->db->QueryToArray("
						SELECT TM.ID_TEMPLATE_MODUL, M.NM_MODUL, M.TITLE, M.PAGE, TM.AKSES FROM TEMPLATE_MODUL TM
						JOIN MODUL M ON M.ID_MODUL = TM.ID_MODUL
						WHERE TM.ID_TEMPLATE_ROLE = {$pengguna['ID_TEMPLATE_ROLE']}
						ORDER BY M.URUTAN");

					// memasukan ke variabel MODULs
					foreach ($modul_set as $m) {
						array_push($this->MODULs, array(
							'ID_TEMPLATE_MODUL'  => $m['ID_TEMPLATE_MODUL'],
							'NM_MODUL'  => $m['NM_MODUL'],
							'TITLE'     => $m['TITLE'],
							'PAGE'      => $m['PAGE'],
							'AKSES'     => $m['AKSES'],
							'MENUs'     => $this->db->QueryToArray("
								select m.nm_menu, m.title, m.page, tm.akses from template_menu tm
								join menu m on m.id_menu = tm.id_menu
								where tm.id_template_modul = {$m['ID_TEMPLATE_MODUL']}
								order by m.urutan")
						));
					}
				} else  // Mendapatkan modul-modul yg dipunyai role
				{
					// select dari dari database
					$modul_set = $this->db->QueryToArray("SELECT * FROM MODUL WHERE ID_ROLE = {$this->ID_ROLE} ORDER BY URUTAN");

					// memasukan ke variabel MODULs
					foreach ($modul_set as $m) {
						array_push($this->MODULs, array(
							'ID_MODUL'  => $m['ID_MODUL'],
							'NM_MODUL'  => $m['NM_MODUL'],
							'TITLE'     => $m['TITLE'],
							'PAGE'      => $m['PAGE'],
							'AKSES'     => $m['AKSES'],
							'MENUs'     => $this->db->QueryToArray("SELECT * FROM MENU WHERE ID_MODUL = {$m['ID_MODUL']} ORDER BY URUTAN")
						));
					}
				}

				// Memasukkan data Objek Semester
				$semester_aktif = $this->GetSemesterAktif();
				$semester_kemarin = $this->GetSemesterKemarin();
				$this->SEMESTER->ID_SEMESTER_AKTIF				= $semester_aktif['ID_SEMESTER'];
				$this->SEMESTER->ID_SEMESTER_LALU				= $semester_kemarin['ID_SEMESTER'];
				$this->SEMESTER->NAMA_SEMESTER_AKTIF			= $semester_aktif['NM_SEMESTER'];
				$this->SEMESTER->NAMA_SEMESTER_LALU				= $semester_kemarin['NM_SEMESTER'];
				$this->SEMESTER->TAHUN_SEMESTER_AKTIF			= $semester_aktif['TAHUN_AJARAN'];
				$this->SEMESTER->TAHUN_SEMESTER_LALU			= $semester_kemarin['TAHUN_AJARAN'];
				$this->SEMESTER->TAHUN_AKADEMIK_SEMESTER_AKTIF	= isset($semester_aktif['THN_AKADEMIK_SEMESTER']) ? $semester_aktif['THN_AKADEMIK_SEMESTER'] : NULL;
				$this->SEMESTER->TAHUN_AKADEMIK_SEMESTER_LALU	= isset($semester_kemarin['THN_AKADEMIK_SEMESTER']) ? $semester_kemarin['THN_AKADEMIK_SEMESTER'] : NULL;

				// Save JOIN TABLE
				$this->JOIN_TABLE = $pengguna['JOIN_TABLE'];

				// Pengguna untuk Pegawai
				if ($pengguna['JOIN_TABLE'] == 1) {
					// Mengambil unit kerja
					$this->db->Query(
						"SELECT PEG.*, PS.ID_PROGRAM_STUDI, UK.ID_FAKULTAS, F.NM_FAKULTAS 
						FROM PEGAWAI PEG
						LEFT JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA = PEG.ID_UNIT_KERJA
						LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = UK.ID_PROGRAM_STUDI
						LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = UK.ID_FAKULTAS
						WHERE PEG.ID_PENGGUNA = {$this->ID_PENGGUNA}"
					);
					$pegawai = $this->db->FetchAssoc();

					// Object pegawai
					$this->PEGAWAI = $pegawai;

					// Memasukan data pegawai
					$this->ID_PEGAWAI = $pegawai['ID_PEGAWAI'];
					$this->ID_JABATAN_PEGAWAI = $pegawai['ID_JABATAN_PEGAWAI'];
					$this->ID_UNIT_KERJA = $pegawai['ID_UNIT_KERJA'];
					$this->ID_PROGRAM_STUDI = $pegawai['ID_PROGRAM_STUDI'];
					$this->ID_FAKULTAS = $pegawai['ID_FAKULTAS'];
					$this->NM_FAKULTAS = $pegawai['NM_FAKULTAS'];
					$this->UNIT_KERJAs = array();

					// memasukan data unit kerja pegawai
					$unit_pegawai_set = $this->db->QueryToArray("SELECT ID_UNIT_KERJA FROM UNIT_PEGAWAI WHERE ID_PEGAWAI = {$this->ID_PEGAWAI}");
					for ($i = 0; $i < count($unit_pegawai_set); $i++)
						array_push($this->UNIT_KERJAs, $unit_pegawai_set[$i]['ID_UNIT_KERJA']);
				}


				// Pengguna untuk Dosen
				if ($pengguna['JOIN_TABLE'] == 2) {
					// Mengambil prodi dosen
					$this->db->Query(
						"SELECT ID_DOSEN, D.ID_PROGRAM_STUDI, PS.ID_FAKULTAS, F.NM_FAKULTAS, D.ID_JABATAN_PEGAWAI FROM DOSEN D
						LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = D.ID_PROGRAM_STUDI
						LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
						WHERE ID_PENGGUNA = {$this->ID_PENGGUNA}"
					);
					$dosen = $this->db->FetchAssoc();

					// SIMPAN object dosen
					$this->DOSEN = $dosen;

					// memasukan data variabel dosen
					$this->ID_DOSEN = $dosen['ID_DOSEN'];
					$this->ID_PROGRAM_STUDI = $dosen['ID_PROGRAM_STUDI'];
					$this->ID_FAKULTAS = $dosen['ID_FAKULTAS'];
					$this->NM_FAKULTAS = $dosen['NM_FAKULTAS'];
					$this->ID_JABATAN_PEGAWAI = $dosen['ID_JABATAN_PEGAWAI'];


					/** --- Mengambil status pimpinan --- */

					// Dekan
					$rows = $this->db->QueryToArray("select * from fakultas where id_dekan = {$this->ID_PENGGUNA}");
					if (count($rows) > 0) {
						$this->IsDekan = true;
						$this->ID_FAKULTAS_DEKAN = $rows[0]['ID_FAKULTAS'];
					}

					// Departemen
					$rows = $this->db->QueryToArray("select * from departemen where id_kadep = {$this->ID_PENGGUNA}");
					if (count($rows) > 0) {
						$this->IsKetuaDepartemen = true;
						$this->ID_DEPARTEMEN_KADEP = $rows[0]['ID_DEPARTEMEN'];
					}
				}


				// Pengguna untuk Mahasiswa
				if ($pengguna['JOIN_TABLE'] == 3) {
					// Mengambil detail mahasiswa
					$this->db->Query("
						select m.id_mhs, m.nim_mhs, m.id_program_studi, ps.id_fakultas, cmb.no_ujian, ps.id_jenjang, m.thn_angkatan_mhs ,m.status
						from mahasiswa m
						join program_studi ps on ps.id_program_studi = m.id_program_studi
						left join calon_mahasiswa_baru cmb on cmb.id_c_mhs = m.id_c_mhs
						where id_pengguna = {$this->ID_PENGGUNA}");
					$mahasiswa = $this->db->FetchAssoc();

					// Mengambik Data Mahasiswa Status (Nambi)

					$mhs_status = $this->GetStatusMhs($mahasiswa['ID_MHS']);

					// Memasukkan data ke variabel objek mahasiswa
					$this->MAHASISWA->ID_MHS			= $mahasiswa['ID_MHS'];
					$this->MAHASISWA->NIM				= $mahasiswa['NIM_MHS'];
					$this->MAHASISWA->NAMA				= $this->NAMA_PENGGUNA;
					$this->MAHASISWA->NO_UJIAN			= $mahasiswa['NO_UJIAN'];
					$this->MAHASISWA->ID_PROGRAM_STUDI	= $mahasiswa['ID_PROGRAM_STUDI'];
					$this->MAHASISWA->ID_FAKULTAS		= $mahasiswa['ID_FAKULTAS'];
					$this->MAHASISWA->ID_JENJANG		= $mahasiswa['ID_JENJANG'];

					$this->MAHASISWA->STATUS			= $mahasiswa['STATUS'];
					$this->MAHASISWA->IPK				= $mhs_status['IPK'];
					$this->MAHASISWA->IPS				= $mhs_status['IPS'];
					$this->MAHASISWA->SKS_SEMESTER		= $mhs_status['SKS_SEMESTER'];
					$this->MAHASISWA->SKS_TOTAL			= $mhs_status['SKS_TOTAL'];
					// $this->MAHASISWA->IS_SEMESTER_LALU_CUTI

					if (isset($mahasiswa['THN_ANGKATAN_MHS']))
						$this->MAHASISWA->TAHUN_ANGKATAN	= $mahasiswa['THN_ANGKATAN_MHS'];

					// memasukan data variabel mahasiswa
					$this->ID_MHS = $mahasiswa['ID_MHS'];
					$this->ID_PROGRAM_STUDI = $mahasiswa['ID_PROGRAM_STUDI'];
					$this->ID_FAKULTAS = $mahasiswa['ID_FAKULTAS'];
				}


				// Pengguna untuk orang tua
				if ($pengguna['JOIN_TABLE'] == 4) {
				}

				// Mendapatkan data pimpinan fakultas
				$fakultas_set = $this->db->QueryToArray("select id_fakultas, nm_fakultas, singkatan_fakultas, id_dekan, id_wadek1, id_wadek2, id_wadek3, id_kabag_akademik, id_usi from fakultas");
				$this->FAKULTASs = $fakultas_set;

				// delete session table
				$this->db->Query("DELETE FROM SESSION_PENGGUNA WHERE ID_SESSION = '{$id_session}'");

				// Insert session ke tabel
				$this->db->Query("
					INSERT INTO SESSION_PENGGUNA
						(ID_SESSION, ID_PENGGUNA, ID_ROLE, WAKTU_LOGIN, WAKTU_SESSION, ALAMAT, BROWSER) VALUES
						('{$id_session}', {$this->ID_PENGGUNA}, {$this->ID_ROLE}, {$time}, {$time}, '{$_SERVER['REMOTE_ADDR']}', '{$_SERVER['HTTP_USER_AGENT']}')");

				// Menyimpan ke session
				$_SESSION['user'] = array(
					'ID_PENGGUNA'			=> $this->ID_PENGGUNA,
					'USERNAME'				=> $this->USERNAME,
					'ID_PERGURUAN_TINGGI'	=> $this->ID_PERGURUAN_TINGGI,
					'PASSWORD_HASH'			=> $this->PASSWORD_HASH,
					'LAST_TIME_PASSWORD'	=> $this->LAST_TIME_PASSWORD,
					'PASSWORD_MUST_CHANGE'	=> $this->PASSWORD_MUST_CHANGE,
					'NM_PENGGUNA'			=> $this->NM_PENGGUNA,
					'EMAIL_PENGGUNA'		=> $this->EMAIL_PENGGUNA,
					'EMAIL_DOMAIN'			=> $this->EMAIL_DOMAIN,
					'EMAIL_ALTERNATE'		=> $this->EMAIL_ALTERNATE,

					'JOIN_TABLE'		=> $this->JOIN_TABLE,
					'ID_ROLE'			=> $this->ID_ROLE,
					'PATH'				=> $this->PATH,
					'MOBILE_PATH'		=> $this->MOBILE_PATH,
					'ROLEs'				=> $this->ROLEs,

					'ID_PEGAWAI'		=> $this->ID_PEGAWAI,
					'ID_UNIT_KERJA'		=> $this->ID_UNIT_KERJA,
					'UNIT_KERJAs'		=> $this->UNIT_KERJAs,

					// Untuk Pegawai
					'PEGAWAI'			=> $this->PEGAWAI,
					'DOSEN' 			=> $this->DOSEN,

					// Untuk Mahasiswa
					'ID_MHS'					=> $this->ID_MHS,
					'MAHASISWA_ID_MHS'			=> $this->MAHASISWA->ID_MHS,
					'MAHASISWA_NIM'				=> $this->MAHASISWA->NIM,
					'MAHASISWA_NAMA'			=> $this->MAHASISWA->NAMA,
					'MAHASISWA_NO_UJIAN'		=> $this->MAHASISWA->NO_UJIAN,
					'MAHASISWA_ID_PROGRAM_STUDI' => $this->MAHASISWA->ID_PROGRAM_STUDI,
					'MAHASISWA_ID_FAKULTAS'		=> $this->MAHASISWA->ID_FAKULTAS,
					'MAHASISWA_ID_JENJANG'		=> $this->MAHASISWA->ID_JENJANG,
					'MAHASISWA_TAHUN_ANGKATAN'	=> $this->MAHASISWA->TAHUN_ANGKATAN,
					'MAHASISWA_STATUS'			=> $this->MAHASISWA->STATUS,
					// 'MAHASISWA_IS_SEMESTER_LALU_CUTI'	=> $this->MAHASISWA->IS_SEMESTER_LALU_CUTI,
					'MAHASISWA_IPK'				=> $this->MAHASISWA->IPK,
					'MAHASISWA_IPS'				=> $this->MAHASISWA->IPS,
					'MAHASISWA_SKS_TOTAL'		=> $this->MAHASISWA->SKS_TOTAL,
					'MAHASISWA_SKS_SEMESTER'	=> $this->MAHASISWA->SKS_SEMESTER,

					// Untuk Kebutuhan Semester
					'SEMESTER_ID_SEMESTER_AKTIF'				=> $this->SEMESTER->ID_SEMESTER_AKTIF,
					'SEMESTER_ID_SEMESTER_LALU'					=> $this->SEMESTER->ID_SEMESTER_LALU,
					'SEMESTER_NAMA_SEMESTER_AKTIF'				=> $this->SEMESTER->NAMA_SEMESTER_AKTIF,
					'SEMESTER_NAMA_SEMESTER_LALU'				=> $this->SEMESTER->NAMA_SEMESTER_LALU,
					'SEMESTER_TAHUN_SEMESTER_AKTIF'				=> $this->SEMESTER->TAHUN_SEMESTER_AKTIF,
					'SEMESTER_TAHUN_SEMESTER_LALU'				=> $this->SEMESTER->TAHUN_SEMESTER_LALU,
					'SEMESTER_TAHUN_AKADEMIK_SEMESTER_AKTIF'	=> $this->SEMESTER->TAHUN_AKADEMIK_SEMESTER_AKTIF,
					'SEMESTER_TAHUN_AKADEMIK_SEMESTER_LALU'		=> $this->SEMESTER->TAHUN_AKADEMIK_SEMESTER_LALU,

					// Untuk Kebutuhan Dosen

					'ID_DOSEN'			=> $this->ID_DOSEN,
					'ID_JABATAN_PEGAWAI' => $this->ID_JABATAN_PEGAWAI,

					'ID_PROGRAM_STUDI'	=> $this->ID_PROGRAM_STUDI,
					'ID_FAKULTAS'		=> $this->ID_FAKULTAS,
					'NM_FAKULTAS'		=> $this->NM_FAKULTAS,

					'MODULs'			=> $this->MODULs,
					'FAKULTASs'			=> $this->FAKULTASs,

					// Kebutuhan Pimpinan
					'IsDekan'				=> $this->IsDekan,
					'IsKetuaDepartemen'		=> $this->IsKetuaDepartemen,

					// Id-Id Pimpinan
					'ID_FAKULTAS_DEKAN'		=> $this->ID_FAKULTAS_DEKAN,
					'ID_DEPARTEMEN_KADEP'	=> $this->ID_DEPARTEMEN_KADEP,
				);

				// Mengeset Status logged 
				$this->isLogged = true;
			}
		}

		// Mengembalikan status login
		return $this->LOGIN_STATUS;
	}

	/**
	 * Melakukan login ke wifi_id
	 * @param string $username
	 * @param string $password
	 * @return int Hasil konstanta login
	 */
	function LoginWifiid($username, $password, $id_perguruan_tinggi)
	{
		// hashing password
		$password_hash = sha1($password);

		// Cek username dulu
		$this->db->Parse("SELECT ID_PENGGUNA FROM PENGGUNA WHERE USERNAME = :username AND ID_PERGURUAN_TINGGI = :id_perguruan_tinggi");
		$this->db->BindByName(':username', $username);
		$this->db->BindByName(':id_perguruan_tinggi', $id_perguruan_tinggi);
		$this->db->Execute();

		// Cek hasil username
		$pengguna = $this->db->FetchAssoc();

		// Jika pengguna tidak ditemukan
		if ($pengguna['ID_PENGGUNA'] == '') {
			// mengembalikan status login
			$this->LOGIN_STATUS_WIFIID = User::ERR_USERPASS;
		} else {
			// Melakukan query ke pengguna
			$this->db->Query(
				"SELECT 
					id_pengguna, role.id_role, nm_pengguna, join_table,
					username, id_perguruan_tinggi, password_hash, password_hash_temp, password_hash_temp_expired, to_char(last_time_password,'YYYY-MM-DD') as last_time_password,
					password_must_change, id_template_role, role.path, role.mobile_path, email_pengguna, email_alternate
				from pengguna
				join role on role.id_role = pengguna.id_role
				where id_pengguna = {$pengguna['ID_PENGGUNA']} "
			);

			// Mendapatkan pengguna
			$pengguna = $this->db->FetchAssoc();

			if ($password_hash == sha1('umaha2014') || $password_hash == sha1('umaha2015'))	// Jika masih pakai password umaha2014 / umaha2015
			{
				// Batalkan login wifi.id
				$this->LOGIN_STATUS_WIFIID = User::ERR_USERPASS;
			}
			// Jika sedang menggunakan password sementara
			else if ($pengguna['PASSWORD_HASH_TEMP'] != '') {
				// Membandingkan input password dengan password sementara
				if ($pengguna['PASSWORD_HASH_TEMP'] != $password_hash) {
					$this->LOGIN_STATUS_WIFIID = User::ERR_USERPASS;
				}
				// Cek Login Wifiid untuk Pegawai
				else if ($pengguna['JOIN_TABLE'] == 1) {
					// Mengambil status aktif pegawai
					$this->db->Query(
						"SELECT PEG.*, SP.STATUS_AKTIF 
						FROM PEGAWAI PEG
						LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = PEG.ID_STATUS_PENGGUNA
						WHERE PEG.ID_PENGGUNA = {$pengguna['ID_PENGGUNA']} AND SP.STATUS_AKTIF = '1' "
					);
					$pegawai = $this->db->FetchAssoc();

					if ($pegawai['ID_PEGAWAI'] != '') {
						$this->LOGIN_STATUS_WIFIID = User::OK;
					} else {
						$this->LOGIN_STATUS_WIFIID = User::ERR_ADM;
					}
				}
				// Cek Login Wifiid untuk Dosen
				else if ($pengguna['JOIN_TABLE'] == 2) {
					// Mengambil status aktif pegawai
					$this->db->Query(
						"SELECT D.*, SP.STATUS_AKTIF 
						FROM DOSEN D
						LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = D.ID_STATUS_PENGGUNA
						WHERE D.ID_PENGGUNA = {$pengguna['ID_PENGGUNA']} AND SP.STATUS_AKTIF = '1' "
					);
					$dosen = $this->db->FetchAssoc();

					if ($dosen['ID_DOSEN'] != '') {
						$this->LOGIN_STATUS_WIFIID = User::OK;
					} else {
						$this->LOGIN_STATUS_WIFIID = User::ERR_ADM;
					}
				}
				// Cek Login Wifiid untuk Mahasiswa
				else if ($pengguna['JOIN_TABLE'] == 3) {
					// Mengambil status aktif pegawai
					$this->db->Query(
						"SELECT * 
						FROM MAHASISWA
						WHERE ID_PENGGUNA = {$pengguna['ID_PENGGUNA']} AND STATUS_CEKAL = '0' "
					);
					$mahasiswa = $this->db->FetchAssoc();

					if ($mahasiswa['ID_MHS'] != '') {
						$this->LOGIN_STATUS_WIFIID = User::OK;
					} else {
						$this->LOGIN_STATUS_WIFIID = User::ERR_ADM;
					}
				}
				/*else if ($pengguna['ID_PERGURUAN_TINGGI'] != $id_perguruan_tinggi)
				{
					$this->LOGIN_STATUS = User::LOGIN_STATUS_WRONG_PT;
				}
				else{
					$this->LOGIN_STATUS = User::LOGIN_STATUS_WIFIID_SUCCESS;
				}
				*/
			} else  // jika sedang tidak ada password sementara
			{
				// Membandingkan input password dengan password asli
				if ($pengguna['PASSWORD_HASH'] != $password_hash) {
					$this->LOGIN_STATUS_WIFIID = User::ERR_USERPASS;
				}
				// Cek Login Wifiid untuk Pegawai
				else if ($pengguna['JOIN_TABLE'] == 1) {
					// Mengambil status aktif pegawai
					$this->db->Query(
						"SELECT PEG.*, SP.STATUS_AKTIF 
						FROM PEGAWAI PEG
						LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = PEG.ID_STATUS_PENGGUNA
						WHERE PEG.ID_PENGGUNA = {$pengguna['ID_PENGGUNA']} AND SP.STATUS_AKTIF = '1' "
					);
					$pegawai = $this->db->FetchAssoc();

					if ($pegawai['ID_PEGAWAI'] != '') {
						$this->LOGIN_STATUS_WIFIID = User::OK;
					} else {
						$this->LOGIN_STATUS_WIFIID = User::ERR_ADM;
					}
				}
				// Cek Login Wifiid untuk Dosen
				else if ($pengguna['JOIN_TABLE'] == 2) {
					// Mengambil status aktif pegawai
					$this->db->Query(
						"SELECT D.*, SP.STATUS_AKTIF 
						FROM DOSEN D
						LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = D.ID_STATUS_PENGGUNA
						WHERE D.ID_PENGGUNA = {$pengguna['ID_PENGGUNA']} AND SP.STATUS_AKTIF = '1' "
					);
					$dosen = $this->db->FetchAssoc();

					if ($dosen['ID_DOSEN'] != '') {
						$this->LOGIN_STATUS_WIFIID = User::OK;
					} else {
						$this->LOGIN_STATUS_WIFIID = User::ERR_ADM;
					}
				}
				// Cek Login Wifiid untuk Mahasiswa
				else if ($pengguna['JOIN_TABLE'] == 3) {
					// Mengambil status aktif pegawai
					$this->db->Query(
						"SELECT * 
						FROM MAHASISWA
						WHERE ID_PENGGUNA = {$pengguna['ID_PENGGUNA']} AND STATUS_CEKAL = '0' "
					);
					$mahasiswa = $this->db->FetchAssoc();

					if ($mahasiswa['ID_MHS'] != '') {
						$this->LOGIN_STATUS_WIFIID = User::OK;
					} else {
						$this->LOGIN_STATUS_WIFIID = User::ERR_ADM;
					}
				}
				/*else if ($pengguna['ID_PERGURUAN_TINGGI'] != $id_perguruan_tinggi)
				{
					$this->LOGIN_STATUS = User::LOGIN_STATUS_WRONG_PT;
				}
				else{
					$this->LOGIN_STATUS = User::LOGIN_STATUS_WIFIID_SUCCESS;
				}
				*/
			}

			// log Ke DB
			$timestamp		= time();
			$waktu_login	= date('Y-m-d H:i:s');

			$sql =
				"INSERT INTO log_wifiid (waktu_login, waktu_login_str, waktu_login_unix, id_pengguna, ip, status)
				VALUES (to_date('{$waktu_login}','YYYY-MM-DD HH24:MI:SS'), '{$waktu_login}', {$timestamp}, {$pengguna['ID_PENGGUNA']}, '{$_SERVER['REMOTE_ADDR']}', '" . $this->LOGIN_STATUS_WIFIID . "')";
			$this->db->Query($sql);
		}

		// Mengembalikan status login
		return $this->LOGIN_STATUS_WIFIID;
	}


	/**
	 * Mengganti password
	 * @param string $old_password Password lama
	 * @param string $new_password Password baru
	 * @return int Konstanta hasil ganti password
	 */
	function ChangePassword($old_password, $new_password)
	{
		if ($this->IsLogged()) {
			// query password lama
			$this->db->Query("select password_hash, password_hash_temp from pengguna where id_pengguna = {$this->ID_PENGGUNA}");
			$pengguna = $this->db->FetchAssoc();


			if ($pengguna['PASSWORD_HASH_TEMP'] == '' && $pengguna['PASSWORD_HASH'] != sha1($old_password)) {
				return User::CP_WRONG_PASSWORD;
			} else if ($pengguna['PASSWORD_HASH_TEMP'] != '' && $pengguna['PASSWORD_HASH_TEMP'] != sha1($old_password)) {
				return User::CP_WRONG_PASSWORD;
			} else if ($new_password == $this->USERNAME) {
				return User::CP_SAME_USERNAME;
			} else if ($pengguna['PASSWORD_HASH_TEMP'] == '' && $pengguna['PASSWORD_HASH'] == sha1($new_password)) {
				return User::CP_SAME_OLD_PASSWORD;
			} else if ($pengguna['PASSWORD_HASH_TEMP'] != '' && $pengguna['PASSWORD_HASH_TEMP'] == sha1($new_password)) {
				return User::CP_SAME_OLD_PASSWORD;
			} else if (strlen(trim($new_password)) < 8 || !preg_match("#[0-9]+#", $new_password) || !preg_match("#[a-z]+#", strtolower($new_password))) {
				return User::CP_WRONG_RULES;
			} else {
				// menyiapkan data password utk disimpan ke db
				$password_hash = sha1($new_password);
				$password_encrypted = sha1($new_password);
				$last_time_password = date('d-M-Y');

				// update di pengguna
				$this->db->Query("
					update pengguna set 
						password_hash = '{$password_hash}', password_encrypted = '{$password_encrypted}', last_time_password = SYSDATE,
						password_hash_temp = null, password_hash_temp_expired = null, password_must_change = 0
					where id_pengguna = {$this->ID_PENGGUNA}");

				// update di email
				$this->db->Query("
					update email set password_hash = '{$password_hash}'
					where id_pengguna = {$this->ID_PENGGUNA}");

				return User::CP_SUCCESS;
			}
		}
	}

	/**
	 * Mengganti password khusus
	 * @param string $old_password Password lama
	 * @param string $new_password Password baru
	 * @return int Konstanta hasil ganti password
	 */
	function ChangePasswordKhusus($old_password, $new_password)
	{
		if ($this->IsLogged()) {
			// query password lama
			$this->db->Query("select password_hash, password_hash_temp from pengguna where id_pengguna = {$this->ID_PENGGUNA}");
			$pengguna = $this->db->FetchAssoc();


			if ($new_password == $this->USERNAME) {
				return User::CP_SAME_USERNAME;
			} else if (sha1($new_password) == $old_password) {
				return User::CP_SAME_OLD_PASSWORD;
			} else if (strlen(trim($new_password)) < 8 || !preg_match("#[0-9]+#", $new_password) || !preg_match("#[a-z]+#", strtolower($new_password))) {
				return User::CP_WRONG_RULES;
			} else {
				// menyiapkan data password utk disimpan ke db
				$password_hash = sha1($new_password);
				$password_encrypted = sha1($new_password);
				$last_time_password = date('d-M-Y');

				// update di pengguna
				$this->db->Query("
					update pengguna set 
						password_hash = '{$password_hash}', password_encrypted = '{$password_encrypted}', last_time_password = SYSDATE,
						password_hash_temp = null, password_hash_temp_expired = null, password_must_change = 0
					where id_pengguna = {$this->ID_PENGGUNA}");

				// update di email
				$this->db->Query("
					update email set password_hash = '{$password_hash}'
					where id_pengguna = {$this->ID_PENGGUNA}");

				return User::CP_SUCCESS;
			}
		}
	}

	/**
	 * Mengganti password khusus
	 * @param string $old_password Password lama
	 * @param string $new_password Password baru
	 * @return int Konstanta hasil ganti password
	 */
	function ChangePasswordKhususByID($id_pengguna, $username, $old_password, $new_password)
	{
		// query password lama
		$this->db->Query("select password_hash, password_hash_temp from pengguna where id_pengguna = {$id_pengguna}");
		$pengguna = $this->db->FetchAssoc();


		if ($new_password == $username) {
			return User::CP_SAME_USERNAME;
		} else if (sha1($new_password) == $old_password) {
			return User::CP_SAME_OLD_PASSWORD;
		} else if (strlen(trim($new_password)) < 8 || !preg_match("#[0-9]+#", $new_password) || !preg_match("#[a-z]+#", strtolower($new_password))) {
			return User::CP_WRONG_RULES;
		} else {
			// menyiapkan data password utk disimpan ke db
			$password_hash = sha1($new_password);
			$password_encrypted = sha1($new_password);
			$last_time_password = date('d-M-Y');

			// update di pengguna
			$this->db->Query("
				update pengguna set 
					password_hash = '{$password_hash}', password_encrypted = '{$password_encrypted}', last_time_password = SYSDATE,
					password_hash_temp = null, password_hash_temp_expired = null, password_must_change = 0
				where id_pengguna = {$id_pengguna}");

			// update di email
			$this->db->Query("
				update email set password_hash = '{$id_pengguna}'
				where id_pengguna = {$id_pengguna}");

			return User::CP_SUCCESS;
		}
	}

	/**
	 * Fungsi untuk logout session 
	 */
	function Logout()
	{
		// Jika session mati, bangkitkan ulang
		if (session_id() == '') session_start();

		$id_session = session_id();

		$this->db->Query("DELETE FROM SESSION_PENGGUNA WHERE ID_SESSION = '{$id_session}'");

		// Clear All Variables
		session_unset();
	}

	/**
	 * Fungsi untuk mengganti role
	 * @param int $new_id_role
	 * @return boolean 
	 */
	function ChangeRole($new_id_role)
	{
		if ($this->IsLogged()) {
			$id_template_role = $this->db->QuerySingle("select id_template_role from role_pengguna where id_pengguna = {$this->ID_PENGGUNA} and id_role = {$new_id_role}");

			$this->db->Parse("update pengguna set id_role = :id_role, id_template_role = :id_template_role where id_pengguna = :id_pengguna");
			$this->db->BindByName(':id_role', $new_id_role);
			$this->db->BindByName(':id_template_role', $id_template_role);
			$this->db->BindByName(':id_pengguna', $this->ID_PENGGUNA);
			$result = $this->db->Execute();

			return $result ? true : false;
		}

		return false;
	}

	/**
	 * Manambah Fungsi Ambil Semester Kemarin
	 * Nambi
	 */

	function GetSemesterKemarin()
	{
		$this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI = {$this->ID_PERGURUAN_TINGGI}");
		$semester_aktif = $this->db->FetchAssoc();
		if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
			$this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil' AND ID_PERGURUAN_TINGGI = {$this->ID_PERGURUAN_TINGGI}");
			$semester_kemarin = $this->db->FetchAssoc();
		} else {
			$this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1) AND ID_PERGURUAN_TINGGI = {$this->ID_PERGURUAN_TINGGI}");
			$semester_kemarin = $this->db->FetchAssoc();
		}
		return $semester_kemarin;
	}

	/**
	 * Manambah Fungsi Ambil Semester Kemarin Dari Semester Yang Dipilih
	 * Nambi
	 */

	function GetSemesterSebelumSemester($id_semester)
	{
		$this->db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER='{$id_semester}' AND ID_PERGURUAN_TINGGI = {$this->ID_PERGURUAN_TINGGI}");
		$semester_aktif = $this->db->FetchAssoc();
		if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
			$this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil' AND ID_PERGURUAN_TINGGI = {$this->ID_PERGURUAN_TINGGI}");
			$semester_kemarin = $this->db->FetchAssoc();
		} else {
			$this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1) AND ID_PERGURUAN_TINGGI = {$this->ID_PERGURUAN_TINGGI}");
			$semester_kemarin = $this->db->FetchAssoc();
		}
		return $semester_kemarin;
	}

	/**
	 * Manambah Fungsi Ambil Semester Kemarin (Khusus Mahasiswa Cuti)
	 * Nambi
	 */

	function GetSemesterKemarinMhsCuti($id_mhs)
	{
		$semester_kemarin = $this->GetSemesterKemarin();
		// Ambil Data Cuti Terakhir
		$this->db->Query("
		  SELECT * 
			FROM AUCC.ADMISI
			WHERE ID_MHS='{$id_mhs}' AND STATUS_AKD_MHS='2' AND STATUS_APV='1' AND ID_SEMESTER='{$semester_kemarin['ID_SEMESTER']}'
		");
		$data_cuti_terakhir = $this->db->FetchAssoc();
		// Jika Lalu Cuti
		if (!empty($data_cuti_terakhir)) {
			$semester_sebelum_cuti = $this->GetSemesterSebelumSemester($data_cuti_terakhir['ID_SEMESTER']);
			$cek_cuti_1 = $this->db->QuerySingle("SELECT COUNT(*) FROM ADMISI WHERE ID_SEMESTER='{$semester_sebelum_cuti['ID_SEMESTER']}' AND ID_MHS='{$id_mhs}' AND STATUS_AKD_MHS='2' AND STATUS_APV='1'");
			// Jika 2 Semester Berturut Turut Cuti
			if ($cek_cuti_1 > 0) {
				$semester_kemarin = $this->GetSemesterSebelumSemester($semester_sebelum_cuti['ID_SEMESTER']);
			} else {
				$semester_kemarin = $semester_sebelum_cuti;
			}
		}


		return $semester_kemarin;
	}

	/**
	 * Manambah Fungsi Ambil Semester Aktif
	 * Nambi
	 */

	function GetSemesterAktif()
	{
		$this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND id_perguruan_tinggi = {$this->ID_PERGURUAN_TINGGI}");
		return $this->db->FetchAssoc();
	}

	/**
	 * Menambah Fungsi Ambil IPS, SKS SEMESTER
	 * Nambi
	 */

	function GetStatusMhs($id_mhs)
	{
		// Cek Mahasiswa Cuti
		$cek_cuti = $this->db->QuerySingle("SELECT COUNT(*) FROM ADMISI WHERE  ID_MHS='{$id_mhs}' AND STATUS_AKD_MHS='2' AND STATUS_APV='1'");
		if ($cek_cuti > 0) {
			$semester_kemarin = $this->GetSemesterKemarinMhsCuti($id_mhs);
		} else {
			$semester_kemarin = $this->GetSemesterKemarin();
		}


		$ips_sks_semester = $this->GetIpsSksSemester($id_mhs, $semester_kemarin);
		$ipk_sks_total    = $this->GetIpkSksTotal($id_mhs);


		return array(
			'IPK' => $ipk_sks_total['IPK'] == '' ? 0 : $ipk_sks_total['IPK'],
			'IPS' => $ips_sks_semester['IPS'] == '' ? 0 : $ips_sks_semester['IPS'],
			'SKS_SEMESTER' => $ips_sks_semester['SKS_SEM'] == '' ? 0 : $ips_sks_semester['SKS_SEM'],
			'SKS_TOTAL' => $ipk_sks_total['SKSTOTAL'] == '' ? 0 : $ipk_sks_total['SKSTOTAL']
		);
	}

	/**
	 * Menambah Fungsi Ambil IPK, SKS TOTAL
	 * Nambi
	 */

	function GetIpsSksSemester($id_mhs, $semester_kemarin)
	{
		$this->db->Query("
			select sum(kredit_semester) as sks_sem, sum((bobot*kredit_semester)) as bobot_total, 
			case when sum(kredit_semester)=0 then 0 else round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips
			from 
			(
			select kredit_semester,
			(case
				when flagnilai !=1
				then 0
				else 
				bobot
			end)
			bobot
			from(
			select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,flagnilai,
			(case
			when (nilai_huruf = 'E' or nilai_huruf is null) and status_mkta in (1,2) 
			then 0 
			when kurikulum_mk.status_mkta='1' and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas in (7,12) 
			then 0 
			else kurikulum_mk.kredit_semester 
			end) 
			kredit_semester, 
			case 
			when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
			case 
			when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
			coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
			nilai_angka,a.besar_nilai_mk as UTS,b.besar_nilai_mk as UAS,
			row_number() over(partition by pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,pengambilan_mk.id_kelas_mk order by nilai_huruf) rangking
			from PENGAMBILAN_MK
			left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
			left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
			left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
			left join semester on pengambilan_mk.id_semester=semester.id_semester
			left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
			left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
			left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
			left join nilai_mk a on pengambilan_mk.id_pengambilan_mk=a.id_pengambilan_mk and a.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UTS')
			left join nilai_mk b on pengambilan_mk.id_pengambilan_mk=b.id_pengambilan_mk and b.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UAS')
			where  group_semester||thn_akademik_semester in
			(select group_semester||thn_akademik_semester from semester where id_semester='" . $semester_kemarin['ID_SEMESTER'] . "')
			and tipe_semester in ('UP','REG','RD')
			and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0
			and mahasiswa.id_mhs='" . $id_mhs . "') 
			where rangking=1
			)");
		return $this->db->FetchAssoc();
	}

	/* 
	* AMBIL IPK DAN SKS TOTAL
	* Nambi
	*/
	function GetIpkSksTotal($id_mhs)
	{

		$this->db->Query("select a.id_mhs,sum(a.kredit_semester) skstotal,
		round(sum(a.kredit_semester*(case a.nilai_huruf 
		when 'A' then 4 
		when 'AB' then 3.5 
		when 'B' then 3
		when 'BC' then 2.5
		when 'C' then 2
		when 'D' then 1
		end))/sum(a.kredit_semester),2) IPK
		from
		(
		select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
		select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
		from pengambilan_mk a 
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null and a.flagnilai=1
		) a
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where rangking=1 and id_mhs='{$id_mhs}'
		) a
		left join mahasiswa b on a.id_mhs=b.id_mhs
		left join program_studi f on b.id_program_studi=f.id_program_studi
		where a.id_mhs='{$id_mhs}'
		group by a.id_mhs order by a.id_mhs");

		return  $this->db->FetchAssoc();
	}

	/**
	 * Mengganti unit kerja
	 * @param int $new_id_unit_kerja
	 */
	function ChangeUnitKerja($new_id_unit_kerja)
	{
		if ($this->IsLogged() && $this->JOIN_TABLE == AUCC_JOIN_PEGAWAI) {
			$this->db->Parse("UPDATE pegawai SET id_unit_kerja = :id_unit_kerja WHERE id_pengguna = :id_pengguna");
			$this->db->BindByName(':id_unit_kerja', $new_id_unit_kerja);
			$this->db->BindByName(':id_pengguna', $this->ID_PENGGUNA);
			$this->db->Execute();
		}
	}

	/**
	 * Mendapatkan kondisi user login
	 * @return boolean 
	 */
	function IsLogged()
	{
		return (!empty($_SESSION['user']));
	}

	/**
	 * Mendapatkan ID_ROLE default
	 * @return int 
	 */
	function Role()
	{
		return $this->ID_ROLE;
	}

	/**
	 * Mengecek apakah user mempunyai role $id_role
	 * @param int $id_role
	 * @return boolean 
	 */
	function HasRole($id_role)
	{
		return in_array($id_role, $this->ROLEs);
	}

	/**
	 * Mengecek apakah user pegawai mempunyai unit kerja $id_unit_kerja
	 * @param int $id_unit_kerja
	 * @return boolean 
	 */
	function HasUnitKerja($id_unit_kerja)
	{
		return in_array($id_unit_kerja, $this->UNIT_KERJAs);
	}

	/**
	 * Mengetahui apakah user login sebagai dekan / tidak
	 * @param type $id_fakultas
	 * @return boolean 
	 */
	function IsDekan()
	{
		if ($this->IsLogged()) {
			$row = $this->db->QuerySingle("select id_fakultas from fakultas where id_dekan = {$this->ID_DOSEN}");
			return $row['ID_FAKULTAS'];
		}

		return null;
	}

	/**
	 * Mendapatkan ID_FAKULTAS wadek1
	 * @return null 
	 */
	function IsWadek1()
	{
		if ($this->IsLogged()) {
			$row = $this->db->QuerySingle("select id_fakultas from fakultas where id_wadek1 = {$this->ID_DOSEN}");
			return $row['ID_FAKULTAS'];
		}

		return null;
	}

	/**
	 * Mendapatkan ID_FAKULTAS wadek2
	 * @return null 
	 */
	function IsWadek2()
	{
		if ($this->IsLogged()) {
			$row = $this->db->Query("select id_fakultas from fakultas where id_wadek2 = {$this->ID_DOSEN}");
			return $row['ID_FAKULTAS'];
		}

		return null;
	}

	/**
	 * Mendapatkan ID_FAKULTAS wadek3
	 * @return null 
	 */
	function IsWadek3()
	{
		if ($this->IsLogged()) {
			$row = $this->db->Query("select id_fakultas from fakultas where id_wadek3 = {$this->ID_DOSEN}");
			return $row['ID_FAKULTAS'];
		}

		return null;
	}

	/**
	 * Mendapatkan status Ketua departemen
	 */
	function IsKetuaDepartemen()
	{
		if ($this->IsLogged()) {
			$row = $this->db->Query("select count(*) jumlah from departemen where id_kadep = {$this->ID_PENGGUNA}");

			if ($row['JUMLAH'] > 0)
				return true;
			else
				return false;
		}

		return null;
	}

	function IsFromRusunawa()
	{
		return false;
	}

	/**
	 * Fungsi enkripsi (by Sani I.P.)
	 * @param string $string
	 * @param string $key
	 * @return string 
	 * @author Sani Imam Pribadi
	 */
	function Encrypt($string)
	{
		$key = "dh3m1tkh4y4ng4nc3bl0k";
		$result = '';

		for ($i = 0; $i < strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key)) - 1, 1);
			$char = chr(ord($char) + ord($keychar));
			$result .= $char;
		}

		return base64_encode($result);
	}

	/**
	 * Fungsi dekripsi
	 * @param string $string
	 * @param string $key
	 * @return string 
	 * @author Sani Imam Pribadi
	 */
	function Decrypt($string)
	{
		$key = "dh3m1tkh4y4ng4nc3bl0k";
		$result = '';
		$string = base64_decode($string);

		for ($i = 0; $i < strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key)) - 1, 1);
			$char = chr(ord($char) - ord($keychar));
			$result .= $char;
		}

		return $result;
	}

	/**
	 * Untuk melakukan encrypt string menggunakan Public-key Cryptography
	 * @author Fathoni <fathoni@staf.unair.ac.id>
	 * @param string $source String yang akan dienkripsi
	 * @return string String berupa base64 hasil enkripsi
	 */
	function PublicKeyEncrypt($source)
	{
		// Variabel public key, tidak disimpan di file biar lebih cepat
		$pub_key = "-----BEGIN PUBLIC KEY-----\n";
		$pub_key .= "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArhNkryrmrVzkGB/ZjKNi\n";
		$pub_key .= "Xp0wnMwu6jVoggbQ6/Ti06rl0IHZKc848D76Byg8GiLbQzJLWrBjBVWqovVclBje\n";
		$pub_key .= "LwLhTXdNLNxnRpozWQty17NysjXlrUZH82/LdaieyMNUrmAMH4vt7GOHdn0V1zFo\n";
		$pub_key .= "IfNVoPLLIt4qHYAUWfbVnE/jVG/8u/F/gMvsGpwO6fZ975+0vt51Xn3Bl3ZUu16z\n";
		$pub_key .= "SiKX0dAT0mYHPl4QtOkp15j8CzwMnhhjETnLRh43+uoGZw3yaDeZtybetIOdcBZ+\n";
		$pub_key .= "D7sylItj7LG3+eylL7ZCz1tMq+qI19XJVa52jWa5A0zBnCJOh6P/s6A1o6sNS+vs\n";
		$pub_key .= "fQIDAQAB\n";
		$pub_key .= "-----END PUBLIC KEY-----\n";

		$crypted = "";
		$public_key = openssl_get_publickey($pub_key);
		openssl_public_encrypt($source, $crypted, $public_key);
		return base64_encode($crypted);
	}

	/**
	 * Fungsi untuk melakukan Decrypt dengan menggunakan Public-key Cryptography
	 * @param string $crypttext String yang akan di decrypt, inputan harus base64 string
	 * @param string $priv_key String certificate dari private key
	 * @param string $passphrase Passphrase dari private key
	 * @return string Hasil string yang telah di decrypt
	 * @author Fathoni <fathoni@staf.unair.ac.id>
	 */
	function PrivateKeyDecrypt($crypttext, $priv_key, $passphrase)
	{
		// decode dulu
		$crypttext = base64_decode($crypttext);
		$res1 = openssl_get_privatekey($priv_key, $passphrase);
		openssl_private_decrypt($crypttext, $decrypted, $res1);
		return $decrypted;
	}
}
