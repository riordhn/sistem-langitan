<?php

class PerguruanTinggi
{
	private $db;

	public $langitan_set = array();
	public $id_set;
	public $id_perguruan_tinggi;
	public $ID_PERGURUAN_TINGGI;
	public $nama_pt;
	public $base_url;
	public $nama_singkat;
	public $alamat;
	public $telp_pt;
	public $fax_pt;
	public $kota_pt;
	public $web_email;
	public $web_elearning;
	public $web_pmb;
	public $web_pt;
	public $is_exist;

	function __construct(&$db)
	{
		$this->db = $db;

		// Mengambil dari cache
		if (file_exists(__DIR__.'/../cache/PERGURUAN_TINGGI')) {
			$this->langitan_set = json_decode(file_get_contents(__DIR__.'/../cache/PERGURUAN_TINGGI'), true);
		}
		else {
			$this->langitan_set = $db->QueryToArray(
				"SELECT ID_PERGURUAN_TINGGI, NAMA_PERGURUAN_TINGGI, NAMA_SINGKAT, HTTP_HOST, KOTA_PERGURUAN_TINGGI,
				ALAMAT_PERGURUAN_TINGGI, TELP_PERGURUAN_TINGGI, FAX_PERGURUAN_TINGGI, WEB_EMAIL, WEB_ELEARNING, WEB_PMB,
				WEB_PT,KODE_POS,PASSWORD_GENERAL 
				FROM PERGURUAN_TINGGI WHERE is_unu = 1");

			if ( ! file_exists(__DIR__.'/../cache/')) {
				mkdir(__DIR__.'/../cache/');
			}

			file_put_contents(__DIR__.'/../cache/PERGURUAN_TINGGI', json_encode($this->langitan_set));
		}
		
		$key_selected = '0'; // belum ada PT 
		$this->is_exist = false;
		
		foreach ($this->langitan_set as $key => $value) 
		{
			if ($value['HTTP_HOST'] == $_SERVER['HTTP_HOST'])
			{
				$key_selected = $key;

				// Set sudah ada
				$this->is_exist = true;
				
				// Keluar Looping Setelah ketemu
				break;
			}
		}

		//echo $this->langitan_set[$key_selected]['ID_PERGURUAN_TINGGI'];
		$this->id_perguruan_tinggi = $this->langitan_set[$key_selected]['ID_PERGURUAN_TINGGI'];
		$this->ID_PERGURUAN_TINGGI = $this->langitan_set[$key_selected]['ID_PERGURUAN_TINGGI'];
		$this->nama_pt = $this->langitan_set[$key_selected]['NAMA_PERGURUAN_TINGGI'];
		$this->base_url = "http://".$this->langitan_set[$key_selected]['HTTP_HOST']."/";
		$this->nama_singkat = strtolower($this->langitan_set[$key_selected]['NAMA_SINGKAT']);
		$this->alamat = $this->langitan_set[$key_selected]['ALAMAT_PERGURUAN_TINGGI'];
		$this->telp_pt = $this->langitan_set[$key_selected]['TELP_PERGURUAN_TINGGI'];
		$this->fax_pt = $this->langitan_set[$key_selected]['FAX_PERGURUAN_TINGGI'];
		$this->kota_pt = $this->langitan_set[$key_selected]['KOTA_PERGURUAN_TINGGI'];
		$this->web_email = $this->langitan_set[$key_selected]['WEB_EMAIL'];
		$this->web_elearning = $this->langitan_set[$key_selected]['WEB_ELEARNING'];
		$this->web_pmb = $this->langitan_set[$key_selected]['WEB_PMB'];
		$this->web_pt = $this->langitan_set[$key_selected]['WEB_PT'];
		$this->kode_pos = $this->langitan_set[$key_selected]['KODE_POS'];
		$this->password_general = $this->langitan_set[$key_selected]['PASSWORD_GENERAL'];
	}

	function list_semester()
	{
		$sql = 
			"SELECT id_semester, nm_semester, thn_akademik_semester, tahun_ajaran, fd_id_smt, status_aktif_semester, status_aktif_sp
			FROM semester WHERE id_perguruan_tinggi = {$this->id_perguruan_tinggi} AND tipe_semester = 'REG'
			ORDER BY thn_akademik_semester DESC, nm_semester DESC";

		if ( ! isset($_SESSION['semester_set']))
		{
			$_SESSION['semester_set'] = $this->db->QueryToArray($sql);
		}

		return $_SESSION['semester_set'];
	}

	function list_semester_with_sp()
	{
		$sql = 
			"SELECT id_semester, nm_semester, thn_akademik_semester, tahun_ajaran, fd_id_smt, status_aktif_semester, status_aktif_sp
			FROM semester WHERE id_perguruan_tinggi = {$this->id_perguruan_tinggi} AND tipe_semester IN ('REG','SP')
			ORDER BY thn_akademik_semester DESC, nm_semester DESC";

		if ( ! isset($_SESSION['semester_with_sp_set']))
		{
			$_SESSION['semester_with_sp_set'] = $this->db->QueryToArray($sql);
		}

		return $_SESSION['semester_with_sp_set'];
	}

	function get_semester_aktif()
	{
		$sql = 
			"SELECT id_semester, nm_semester, thn_akademik_semester, tahun_ajaran, fd_id_smt, status_aktif_semester, status_aktif_sp
			FROM semester 
			WHERE id_perguruan_tinggi = {$this->id_perguruan_tinggi} AND tipe_semester = 'REG' AND status_aktif_semester = 'True'";

		if ( ! isset($_SESSION['semester_aktif']))
		{
			$this->db->Query($sql);

			$_SESSION['semester_aktif'] = $this->db->FetchAssoc();	
		}

		return $_SESSION['semester_aktif'];
	}

	function list_prodi()
	{
		$sql = 
			"SELECT ps.id_program_studi, j.nm_jenjang || ' - ' || ps.nm_program_studi AS nm_program_studi, f.nm_fakultas
				FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas 
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				WHERE f.id_perguruan_tinggi = {$this->id_perguruan_tinggi} AND status_aktif_prodi = 1
				ORDER BY j.nm_jenjang, ps.nm_program_studi";

		if ( ! isset($_SESSION['prodi_set']))
		{
			$_SESSION['prodi_set'] = $this->db->QueryToArray($sql);
		}

		return $_SESSION['prodi_set'];
	}
}