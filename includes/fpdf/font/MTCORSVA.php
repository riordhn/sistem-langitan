<?php
$type = 'TrueType';
$name = 'MonotypeCorsiva';
$desc = array('Ascent'=>689,'Descent'=>-259,'CapHeight'=>689,'Flags'=>96,'FontBBox'=>'[-240 -307 1159 913]','ItalicAngle'=>-15,'StemV'=>70,'MissingWidth'=>750);
$up = -120;
$ut = 50;
$cw = array(
	chr(0)=>750,chr(1)=>750,chr(2)=>750,chr(3)=>750,chr(4)=>750,chr(5)=>750,chr(6)=>750,chr(7)=>750,chr(8)=>750,chr(9)=>750,chr(10)=>750,chr(11)=>750,chr(12)=>750,chr(13)=>750,chr(14)=>750,chr(15)=>750,chr(16)=>750,chr(17)=>750,chr(18)=>750,chr(19)=>750,chr(20)=>750,chr(21)=>750,
	chr(22)=>750,chr(23)=>750,chr(24)=>750,chr(25)=>750,chr(26)=>750,chr(27)=>750,chr(28)=>750,chr(29)=>750,chr(30)=>750,chr(31)=>750,' '=>220,'!'=>280,'"'=>220,'#'=>680,'$'=>440,'%'=>680,'&'=>780,'\''=>160,'('=>260,')'=>220,'*'=>420,'+'=>520,
	','=>220,'-'=>280,'.'=>220,'/'=>340,'0'=>440,'1'=>440,'2'=>440,'3'=>440,'4'=>440,'5'=>440,'6'=>440,'7'=>440,'8'=>440,'9'=>440,':'=>260,';'=>240,'<'=>520,'='=>520,'>'=>520,'?'=>380,'@'=>700,'A'=>620,
	'B'=>600,'C'=>520,'D'=>700,'E'=>620,'F'=>580,'G'=>620,'H'=>680,'I'=>380,'J'=>400,'K'=>660,'L'=>580,'M'=>840,'N'=>700,'O'=>600,'P'=>540,'Q'=>600,'R'=>600,'S'=>460,'T'=>500,'U'=>740,'V'=>640,'W'=>880,
	'X'=>560,'Y'=>560,'Z'=>620,'['=>240,'\\'=>480,']'=>320,'^'=>520,'_'=>500,'`'=>220,'a'=>420,'b'=>420,'c'=>340,'d'=>440,'e'=>340,'f'=>320,'g'=>400,'h'=>440,'i'=>240,'j'=>220,'k'=>440,'l'=>240,'m'=>620,
	'n'=>460,'o'=>400,'p'=>440,'q'=>400,'r'=>300,'s'=>320,'t'=>320,'u'=>460,'v'=>440,'w'=>680,'x'=>420,'y'=>400,'z'=>440,'{'=>240,'|'=>520,'}'=>240,'~'=>520,chr(127)=>750,chr(128)=>750,chr(129)=>750,chr(130)=>750,chr(131)=>750,
	chr(132)=>750,chr(133)=>750,chr(134)=>750,chr(135)=>750,chr(136)=>750,chr(137)=>750,chr(138)=>750,chr(139)=>750,chr(140)=>750,chr(141)=>750,chr(142)=>750,chr(143)=>750,chr(144)=>750,chr(145)=>750,chr(146)=>750,chr(147)=>750,chr(148)=>750,chr(149)=>750,chr(150)=>750,chr(151)=>750,chr(152)=>750,chr(153)=>750,
	chr(154)=>750,chr(155)=>750,chr(156)=>750,chr(157)=>750,chr(158)=>750,chr(159)=>750,chr(160)=>220,chr(161)=>620,chr(162)=>440,chr(163)=>580,chr(164)=>600,chr(165)=>580,chr(166)=>460,chr(167)=>420,chr(168)=>360,chr(169)=>460,chr(170)=>460,chr(171)=>500,chr(172)=>620,chr(173)=>280,chr(174)=>620,chr(175)=>620,
	chr(176)=>400,chr(177)=>420,chr(178)=>280,chr(179)=>300,chr(180)=>300,chr(181)=>337,chr(182)=>320,chr(183)=>340,chr(184)=>300,chr(185)=>320,chr(186)=>320,chr(187)=>320,chr(188)=>440,chr(189)=>400,chr(190)=>440,chr(191)=>440,chr(192)=>600,chr(193)=>620,chr(194)=>620,chr(195)=>620,chr(196)=>620,chr(197)=>580,
	chr(198)=>520,chr(199)=>520,chr(200)=>520,chr(201)=>620,chr(202)=>620,chr(203)=>620,chr(204)=>620,chr(205)=>380,chr(206)=>380,chr(207)=>700,chr(208)=>700,chr(209)=>700,chr(210)=>700,chr(211)=>600,chr(212)=>600,chr(213)=>600,chr(214)=>600,chr(215)=>520,chr(216)=>600,chr(217)=>740,chr(218)=>740,chr(219)=>740,
	chr(220)=>740,chr(221)=>560,chr(222)=>500,chr(223)=>420,chr(224)=>300,chr(225)=>420,chr(226)=>420,chr(227)=>420,chr(228)=>420,chr(229)=>240,chr(230)=>340,chr(231)=>340,chr(232)=>340,chr(233)=>340,chr(234)=>340,chr(235)=>340,chr(236)=>340,chr(237)=>240,chr(238)=>240,chr(239)=>482,chr(240)=>440,chr(241)=>460,
	chr(242)=>460,chr(243)=>400,chr(244)=>400,chr(245)=>400,chr(246)=>400,chr(247)=>549,chr(248)=>300,chr(249)=>460,chr(250)=>460,chr(251)=>460,chr(252)=>460,chr(253)=>400,chr(254)=>320,chr(255)=>220);
$enc = 'ISO-8859-2';
$diff = '128 /.notdef 130 /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef 142 /.notdef 145 /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef 158 /.notdef /.notdef 161 /Aogonek /breve /Lslash 165 /Lcaron /Sacute 169 /Scaron /Scedilla /Tcaron /Zacute 174 /Zcaron /Zdotaccent 177 /aogonek /ogonek /lslash 181 /lcaron /sacute /caron 185 /scaron /scedilla /tcaron /zacute /hungarumlaut /zcaron /zdotaccent /Racute 195 /Abreve 197 /Lacute /Cacute 200 /Ccaron 202 /Eogonek 204 /Ecaron 207 /Dcaron /Dcroat /Nacute /Ncaron 213 /Ohungarumlaut 216 /Rcaron /Uring 219 /Uhungarumlaut 222 /Tcommaaccent 224 /racute 227 /abreve 229 /lacute /cacute 232 /ccaron 234 /eogonek 236 /ecaron 239 /dcaron /dcroat /nacute /ncaron 245 /ohungarumlaut 248 /rcaron /uring 251 /uhungarumlaut 254 /tcommaaccent /dotaccent';
$file = 'MTCORSVA.z';
$originalsize = 157360;
?>
