<?php

/**
 * Kumpulan konstanta yang digunakan untuk Sistem Langitan
 */

define('AUCC_ROLE_SUPERADMIN', 1);
define('AUCC_ROLE_ADMINISTRATOR', 2);
define('AUCC_ROLE_MAHASISWA', 3);
define('AUCC_ROLE_DOSEN', 4);
define('AUCC_ROLE_PRODI', 5);
define('AUCC_ROLE_ORTU', 6);
define('AUCC_ROLE_AKADEMIK', 7);
define('AUCC_ROLE_KEMAHASISWAAN', 8);
define('AUCC_ROLE_KEPEGAWAIAN', 9);
define('AUCC_ROLE_KEUANGAN', 10);
define('AUCC_ROLE_PENDIDIKAN', 11);
define('AUCC_ROLE_PIMPINAN', 12);
define('AUCC_ROLE_PPM', 13);
define('AUCC_ROLE_SPM', 14);
define('AUCC_ROLE_GPM', 33);
define('AUCC_ROLE_XPM', 15);
define('AUCC_ROLE_PENDAFTARAN', 16);
define('AUCC_ROLE_AUCC', 17);
define('AUCC_ROLE_PPMB', 18);
define('AUCC_ROLE_KEUANGAN_FAKULTAS', 19);
define('AUCC_ROLE_LP3', 20);
define('AUCC_ROLE_PLK', 21);
define('AUCC_ROLE_STAF', 22);
define('AUCC_ROLE_SDM', 23);
define('AUCC_ROLE_KEPEGAWAIAN_FAKULTAS', 24);
define('AUCC_ROLE_LPPM', 25);
define('AUCC_ROLE_KEUANGAN_ANGGARAN', 26);
define('AUCC_ROLE_SARANA_PRASARANA', 27);
define('AUCC_ROLE_PERUSAHAAN', 29);
define('AUCC_ROLE_PROGRAMMER', 36);
define('AUCC_ROLE_KEMAHASISWAAN_FAKULTAS', 37);
define('AUCC_ROLE_SEKRETARIS', 38);
define('AUCC_ROLE_MANAJEMEN_MUTU', 39);
define('AUCC_ROLE_IOP', 41);
define('AUCC_ROLE_SPI', 43);
define('AUCC_ROLE_SEKRETARIAT_ARSIPARIS', 45);
define('AUCC_ROLE_AKREDITASI', 49);

// reCaptcha Key
define('RECAPTCHA_PUBLIC_KEY', '6LfrnMYSAAAAAJJLehiABXE-vatrHhiPMkbuZcal');
define('RECAPTCHA_PRIVATE_KEY', '6LfrnMYSAAAAAHEOXaOQlAtIYLTBA-PKMB4Jr3nd');

// Session
define('AUCC_SESSION_TIME', 1800);  // 30 menit

// Jenis Pengguna
define('AUCC_JOIN_PEGAWAI', 1);
define('AUCC_JOIN_DOSEN', 2);
define('AUCC_JOIN_MAHASISWA', 3);
define('AUCC_JOIN_ORANG_TUA', 4);

// Constant Perguruan Tinggi
define('PT_UMAHA', 1);
define('PT_UNU_LAMPUNG', 5);
define('PT_UNU_KALBAR', 10);
define('PT_UNINUS', 11);

// Keuangan
define('ID_BIAYA_SPP', 185);
