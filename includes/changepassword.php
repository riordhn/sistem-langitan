<?php
require '../config.php';

// hanya bisa diakses ketika user sudah login
if ($user->IsLogged())
{
	$username = $user->USERNAME;
	
	$role_pengguna_set = $db->QueryToArray("
		select role.id_role, role.nm_role from role_pengguna
		join role on role.id_role = role_pengguna.id_role
		where id_pengguna = {$user->ID_PENGGUNA}
		order by nm_role");
		
	
	$pilih_unit_kerja_prodi = FALSE;
		
	if ( ! empty($_GET['id_role']))
	{
		if (in_array($_GET['id_role'], array(AUCC_ROLE_AKADEMIK, AUCC_ROLE_PRODI)))
		{
			$unit_kerja_set = $db->QueryToArray(
				"SELECT uk.id_unit_kerja, j.nm_jenjang||' '||ps.nm_program_studi AS nm_program_studi
				FROM unit_kerja uk
				JOIN program_studi ps ON ps.id_program_studi = uk.id_program_studi
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				WHERE ps.id_fakultas = {$user->ID_FAKULTAS}");
				
			$pilih_unit_kerja_prodi = TRUE;
		}
	}

	$error = false;
	$success = false;
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		if ($_POST['mode'] == 'change-passwd')
		{
			
			$error_msg = "";
			
			if (strlen(trim($_POST['password_old'])) == 0)
			{
				$error_msg = "Tulis password lama terlebih dahulu";
				$error = true;
			}
			else if (strlen(trim($_POST['password_new'])) == 0 || strlen(trim($_POST['password_new2'])) == 0)
			{
				$error_msg = "Isian password baru tidak lengkap";
				$error = true;
			}
			else if ($_POST['password_new'] != $_POST['password_new2'])
			{
				$error_msg = "Isian password baru harus sama";
				$error = true;
			}
			else
			{
				$cp_result = $user->ChangePassword($_POST['password_old'], $_POST['password_new']);
				
				if ($cp_result == User::CP_WRONG_PASSWORD)
				{
					$error_msg = "Password lama salah.";
					$error = true;
				}
				else if ($cp_result == User::CP_SAME_USERNAME)
				{
					$error_msg = "Password baru tidak boleh sama dengan username";
					$error = true;
				}
				else if ($cp_result == User::CP_SAME_OLD_PASSWORD)
				{
					$error_msg = "Password baru tidak boleh sama dengan password lama";
					$error = true;
				}
				else if ($cp_result == User::CP_WRONG_RULES)
				{
					$error_msg = "Syarat password baru adalah 8 karakter dan harus terdapat angka dan huruf";
					$error = true;
				}
				else if ($cp_result == User::CP_FAILED)
				{
					$error_msg = "Gagal mengganti password.";
					$error = true;
				}
				else if ($cp_result == User::CP_SUCCESS)
				{
					$success = true;
				}
			}
		}
		
		if ($_POST['mode'] == 'change-role')
		{
			$id_pengguna	= $user->ID_PENGGUNA;
			$id_role		= $_POST['id_role'];
			$id_unit_kerja	= isset($_POST['id_unit_kerja']) ? $_POST['id_unit_kerja'] : NULL;
			
			// Memastikan role terdapat di pengguna
            $cek_role_pengguna = $db->QuerySingle("SELECT COUNT (*) FROM ROLE_PENGGUNA WHERE ID_PENGGUNA='{$id_pengguna}' AND ID_ROLE='{$id_role}'");

            if ($cek_role_pengguna == 0)
			{
                echo "<html><head><script>alert('Maaf Role yang anda pilih salah.');top.window.location='/logout.php';</script></head></html>";
                exit();
            }
            else
            {
				$user->ChangeRole($id_role);
				
				// Diganti jika ada submit id_unit_kerja
				if ( ! empty($id_unit_kerja))
					$user->ChangeUnitKerja($id_unit_kerja);
				
				// logout setelah ganti
				echo "<html><head><script>alert('Anda telah ganti Role. Silahkan login kembali.');top.window.location='/logout.php';</script></head></html>";
			}
		}
	}
}
else
{
	exit();
}
?>
<html>
	<head>
		<title>Ganti Password</title>
		<style>
			* { font-family: "Segoe UI","Lucida Sans Unicode", Arial; font-size: 13px; }
			.box { margin-bottom: 20px; border: 1px solid #000; background-color: #eef; }
			.box thead tr th { background-color: navy; color: #fff; padding: 5px 0px; }
		</style>
	</head>
	<body>
		<form action="changepassword.php" method="post">
			<input type="hidden" name="mode" value="change-passwd" />
			<table class="box">
				<thead>
					<tr>
						<th colspan="2">Setting Account</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Username</td>
						<td><?php echo $username; ?></td>
					</tr>
					<tr>
						<td>Email</td>
						<td><?php echo $user->EMAIL_PENGGUNA; ?></td>
					</tr>
					<tr>
						<td>Terakhir Ganti</td>
						<td><?php echo ($user->LAST_TIME_PASSWORD != '') ? strftime('%d/%m/%Y', strtotime($user->LAST_TIME_PASSWORD)) : ''; ?></td>
					</tr>
					<tr>
						<td>Password Lama</td>
						<td><input type="password" name="password_old" value="" /></td>
					</tr>
					<tr>
						<td>Password Baru</td>
						<td><input type="password" name="password_new" value="" /></td>
					</tr>
					<tr>
						<td>Password Baru (Ulangi)</td>
						<td><input type="password" name="password_new2" value="" /></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<input type="submit" value="Simpan" />
						</td>
					</tr>
					<?php if ($error) { ?>
					<tr>
						<td colspan="2" style="color: red"><?php echo $error_msg; ?></td>
					</tr>
					<?php } ?>
					<?php if ($success) { ?>
					<tr>
						<td colspan="2" style="color: green; font-weight: bold">Password berhasil diganti.</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</form>
		<?php if (count($role_pengguna_set) > 1) { ?>
		<form action="changepassword.php" method="post">
			<input type="hidden" name="mode" value="change-role" />
			<table class="box">
				<thead>
					<tr>
						<th colspan="2">Setting Role</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Pilih Role</td>
						<td>
							<select name="id_role">
								<?php $selected = ''; ?>
								<?php foreach ($role_pengguna_set as $r) {
									if ($user->ID_ROLE == $r['ID_ROLE']) $selected = 'selected="selected"';
									echo "<option value=\"{$r['ID_ROLE']}\" {$selected}>{$r['NM_ROLE']}</option>";
									$selected = '';
								} ?>
							</select>
							<?php if ($pilih_unit_kerja_prodi): ?>
							<select name="id_unit_kerja">
								<?php foreach ($unit_kerja_set as $uk): ?>
								<option value="<?php echo $uk['ID_UNIT_KERJA']; ?>" <?php if ($uk['ID_UNIT_KERJA'] == $user->ID_UNIT_KERJA) { echo 'selected'; } ?>><?php echo $uk['NM_PROGRAM_STUDI']; ?></option>
								<?php endforeach; ?>
							</select>
							<?php endif; ?>
							<input type="submit" value="Pindah" />
						</td>
					</tr>
				</tbody>
			</table>
		</form>
		<?php } ?>
	</body>
</html>