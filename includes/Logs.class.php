<?php
class Logs
{
    public static function WriteLog($text)
    {
        $filename = "/var/www/html/logs/log" . date('Ym') .".txt";
        $time_log = date('d-m-Y H:i:s');
        $script_filename = str_replace("/var/www/html", "", $_SERVER['SCRIPT_FILENAME']);
        $file = fopen($filename, 'a', 1);
        fwrite($file, "[{$_SERVER['REMOTE_ADDR']}] {$time_log} @{$script_filename} : {$text}\n");
        fclose($file);
    }
}
?>
