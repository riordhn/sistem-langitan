<?php
/**
 * Kelas enkripsi untuk kebutuhan encrypt password dengan public / private key
 * @author Fathoni <fathoni@staf.unair.ac.id>
 * @version 1.0
 */
class Cryptography
{
	static function PublicKeyEncrypt($source)
	{
		// Menyimpan variabel public key
		$pub_key = "-----BEGIN PUBLIC KEY-----\n";
		$pub_key .= "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArhNkryrmrVzkGB/ZjKNi\n";
		$pub_key .= "Xp0wnMwu6jVoggbQ6/Ti06rl0IHZKc848D76Byg8GiLbQzJLWrBjBVWqovVclBje\n";
		$pub_key .= "LwLhTXdNLNxnRpozWQty17NysjXlrUZH82/LdaieyMNUrmAMH4vt7GOHdn0V1zFo\n";
		$pub_key .= "IfNVoPLLIt4qHYAUWfbVnE/jVG/8u/F/gMvsGpwO6fZ975+0vt51Xn3Bl3ZUu16z\n";
		$pub_key .= "SiKX0dAT0mYHPl4QtOkp15j8CzwMnhhjETnLRh43+uoGZw3yaDeZtybetIOdcBZ+\n";
		$pub_key .= "D7sylItj7LG3+eylL7ZCz1tMq+qI19XJVa52jWa5A0zBnCJOh6P/s6A1o6sNS+vs\n";
		$pub_key .= "fQIDAQAB\n";
		$pub_key .= "-----END PUBLIC KEY-----\n";

		$crypted = "";
		$public_key = openssl_get_publickey($pub_key);
		openssl_public_encrypt($source, $crypted, $public_key);
		return base64_encode($crypted);
	}
	
	static function SHA1($source)
	{
		return sha1($source);
	}

	static function PrivateKeyDecrypt($crypttext, $priv_key, $passphrase)
	{
		// decode dulu
		$crypttext = base64_decode($crypttext);
		$res1 = openssl_get_privatekey($priv_key, $passphrase);
		openssl_private_decrypt($crypttext, $decrypted, $res1);
		return $decrypted;
	}

}
?>
