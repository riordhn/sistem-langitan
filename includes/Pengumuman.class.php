<?php
function message_box($message, $href = "/")
{
	echo <<<EOD
	<html>
	<head>
		<title>UACC - Universitas Airlangga Cyber Campus</title>
	</head>
	<body>
		<div align="center">
			<table style="margin-top: 25px; border: 1px solid #ddd;" cellspacing="0">
				<tr>
					<td style="background-color: #EEF5F7; text-align: center;">
						<img src="img/portal/header_error_login.png" style="border-spacing:0px"/>
					</td>
				</tr>
				<tr>
					<td style="background-color: #EEF5F7; text-align: center;">
					<?php echo {$message}; ?><br/><br/><a href="{$href}">Kembali ke halaman depan</a></td>
				</tr>
			</table>
		</div>
	</body>
</html>
EOD;
	exit();
}
?>

