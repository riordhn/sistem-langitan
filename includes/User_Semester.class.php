<?php

/**
 * Class User_Semester
 */
class User_Semester
{
    public $ID_SEMESTER_AKTIF;
    public $ID_SEMESTER_LALU;
    public $ID_SEMESTER_DEPAN;
    public $TAHUN_SEMESTER_AKTIF;
    public $TAHUN_SEMESTER_LALU;
    public $TAHUN_SEMESTER_DEPAN;
    public $TAHUN_AKADEMIK_SEMESTER_AKTIF;
    public $TAHUN_AKADEMIK_SEMESTER_LALU;
    public $TAHUN_AKADEMIK_SEMESTER_DEPAN;
    public $NAMA_SEMESTER_AKTIF;
    public $NAMA_SEMESTER_LALU;
    public $NAMA_SEMESTER_DEPAN;
}