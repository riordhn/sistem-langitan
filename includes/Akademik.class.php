<?php

class Akademik
{
	private $db;

	function __construct(&$db)
	{
		$this->db = $db;
	}

	public function list_mahasiswa_krs_per_prodi ($id_program_studi, $id_semester)
	{
		// Keterangan :
		//   Data mahasiswa yang mengambil pada semester X + 
		//   Data mahasiswa masih aktif, tapi tidak mengambil pada semester X
		$sql = 
			"SELECT 
				mhs.id_mhs, mhs.nim_mhs, P.nm_pengguna, sp.nm_status_pengguna, pd.nm_pengguna AS nm_dosen_wali,
				COUNT(pmk.id_pengambilan_mk) AS jumlah_mk,
				SUM(pmk.status_apv_pengambilan_mk) AS jumlah_mk_approve,
				SUM(COALESCE(mk1.kredit_semester, mk2.kredit_semester)) AS jumlah_sks,
				SUM(CASE pmk.status_apv_pengambilan_mk WHEN 1 THEN COALESCE(mk1.kredit_semester, mk2.kredit_semester) ELSE 0 END) AS jumlah_sks_approve
			FROM mahasiswa mhs
			JOIN pengguna P ON P.id_pengguna = mhs.id_pengguna
			JOIN status_pengguna sp ON sp.id_status_pengguna = mhs.status_akademik_mhs
			JOIN pengambilan_mk pmk ON pmk.id_mhs = mhs.id_mhs
			LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
			LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
			LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
			LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
			LEFT JOIN dosen_wali dw ON dw.id_mhs = mhs.id_mhs AND dw.status_dosen_wali = 1
			LEFT JOIN dosen D ON D.id_dosen = dw.id_dosen
			LEFT JOIN pengguna pd ON pd.id_pengguna = D.id_pengguna
			WHERE
				-- Program studi
				mhs.id_program_studi = {$id_program_studi} AND
				-- Status Aktif
				sp.status_aktif = 1 AND
				-- Semester Terpilih
				pmk.id_semester = {$id_semester}
			GROUP BY mhs.id_mhs, mhs.nim_mhs, P.nm_pengguna, sp.nm_status_pengguna, pd.nm_pengguna

			UNION ALL

			SELECT 
				mhs.id_mhs, mhs.nim_mhs, P.nm_pengguna, sp.nm_status_pengguna, pd.nm_pengguna AS nm_dosen_wali, 0, 0, 0, 0
			FROM mahasiswa mhs
			JOIN pengguna P ON P.id_pengguna = mhs.id_pengguna
			JOIN status_pengguna sp ON sp.id_status_pengguna = mhs.status_akademik_mhs
			LEFT JOIN dosen_wali dw ON dw.id_mhs = mhs.id_mhs AND dw.status_dosen_wali = 1
			LEFT JOIN dosen D ON D.id_dosen = dw.id_dosen
			LEFT JOIN pengguna pd ON pd.id_pengguna = D.id_pengguna
			WHERE
				-- Program studi
				mhs.id_program_studi = {$id_program_studi} AND
				-- Angkatan sudah terdaftar
				mhs.thn_angkatan_mhs <= (SELECT thn_akademik_semester FROM semester WHERE id_semester = {$id_semester}) AND
				-- Belum Pengambilan
				mhs.id_mhs NOT IN (SELECT DISTINCT id_mhs FROM pengambilan_mk WHERE id_semester = {$id_semester}) AND
				-- Status Aktif
				sp.status_aktif = 1
			ORDER BY 1";

		return $this->db->QueryToArray($sql);
	}

	public function list_kelas_per_prodi ($id_program_studi=null, $id_semester)
	{
		if( ! empty($id_program_studi)) {
			$prodi = "AND kls.id_program_studi = {$id_program_studi}";
		}
		else {
			$prodi = "";
		}

		$sql = 
				"SELECT
					kls.id_kelas_mk,
					j.nm_jenjang || ' - ' || ps.nm_program_studi AS nm_program_studi,
					mk.kd_mata_kuliah, mk.nm_mata_kuliah, nk.nama_kelas, mk.kredit_semester AS sks, 
					kmk.tingkat_semester,
					kls.kapasitas_kelas_mk AS kapasitas,
					(SELECT COUNT(*) FROM pengambilan_mk pmk WHERE pmk.id_kelas_mk = kls.id_kelas_mk) AS krs,
					(SELECT COUNT(*) FROM pengambilan_mk pmk WHERE pmk.id_kelas_mk = kls.id_kelas_mk AND pmk.status_apv_pengambilan_mk = 1) AS krs_approve,
					kmk.status_mkta,
					(SELECT COUNT(*) FROM pengampu_mk pmk WHERE pmk.id_kelas_mk = kls.id_kelas_mk AND pjmk_pengampu_mk = 1) AS jml_dosen,
					(SELECT COUNT(*) FROM jadwal_kelas jdw WHERE jdw.id_kelas_mk = kls.id_kelas_mk) AS jml_jadwal,
					(SELECT COUNT(*) FROM jadwal_kelas jdw JOIN ruangan R ON R.id_ruangan = jdw.id_ruangan WHERE jdw.id_kelas_mk = kls.id_kelas_mk) AS jml_ruangan,
					pd.nm_pengguna AS nm_dosen_pjmk,
					(SELECT COUNT(*) FROM komponen_mk komp WHERE komp.id_kelas_mk = kls.id_kelas_mk) AS jml_komponen_nilai,
					(SELECT COUNT(*) FROM pengambilan_mk pmk WHERE pmk.id_kelas_mk = kls.id_kelas_mk AND pmk.status_apv_pengambilan_mk = 1 AND pmk.nilai_huruf IS NOT NULL AND id_pengguna_input IS NULL) AS jml_nilai,
					(SELECT COUNT(*) FROM pengambilan_mk pmk WHERE pmk.id_kelas_mk = kls.id_kelas_mk AND pmk.status_apv_pengambilan_mk = 1 AND pmk.nilai_huruf IS NOT NULL AND id_pengguna_input IS NOT NULL) AS jml_nilai_pegawai,
					(SELECT DISTINCT MAX(p.nm_pengguna) FROM pengambilan_mk pmk JOIN pengguna p ON p.id_pengguna = pmk.id_pengguna_input WHERE pmk.id_kelas_mk = kls.id_kelas_mk AND pmk.status_apv_pengambilan_mk = 1 AND pmk.nilai_huruf IS NOT NULL AND id_pengguna_input IS NOT NULL) AS nm_pegawai,
					kls.status_seri, kls.tingkat_semester_seri
				FROM kelas_mk kls
				JOIN program_studi ps ON ps.id_program_studi = kls.id_program_studi
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = kls.id_kurikulum_mk
				JOIN mata_kuliah mk ON mk.id_mata_kuliah = kls.id_mata_kuliah
				LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kls.no_kelas_mk
				LEFT JOIN pengampu_mk ON pengampu_mk.id_kelas_mk = kls.id_kelas_mk AND pengampu_mk.pjmk_pengampu_mk = 1
				LEFT JOIN dosen D ON D.id_dosen = pengampu_mk.id_dosen
				LEFT JOIN pengguna pd ON pd.id_pengguna = D.id_pengguna
				WHERE 
					kls.id_semester = {$id_semester} 
					{$prodi}					
				ORDER BY j.nm_jenjang, ps.nm_program_studi, mk.nm_mata_kuliah";

		return $this->db->QueryToArray($sql);
	}

	public function list_dosen_kelas_per_prodi($id_program_studi, $id_semester)
	{
		$sql = 
			"SELECT
				kls.id_kelas_mk,
				p.nm_pengguna||nvl2(p.gelar_belakang,', '||p.gelar_belakang,NULL) AS nm_dosen,
				pmk.pjmk_pengampu_mk
			FROM kelas_mk kls
			JOIN pengampu_mk pmk ON pmk.id_kelas_mk = kls.id_kelas_mk
			JOIN dosen d ON d.id_dosen = pmk.id_dosen
			JOIN pengguna p ON p.id_pengguna = d.id_pengguna
			WHERE 
				kls.id_program_studi = {$id_program_studi} AND
				kls.id_semester = {$id_semester}
			ORDER BY pjmk_pengampu_mk DESC";

		return $this->db->QueryToArray($sql);
	}

	public function list_jadwal_kelas_per_prodi($id_program_studi, $id_semester)
	{
		$sql =
			"SELECT
				kls.id_kelas_mk,
				hari.nm_jadwal_hari||', '||jam.jam_mulai||':'||jam.menit_mulai||' - '||jam.jam_selesai||':'||jam.menit_selesai as jadwal, nm_ruangan
			FROM kelas_mk kls
			JOIN jadwal_kelas jdw ON jdw.id_kelas_mk = kls.id_kelas_mk
			LEFT JOIN jadwal_hari hari ON hari.id_jadwal_hari = jdw.id_jadwal_hari
			LEFT JOIN jadwal_jam jam ON jam.id_jadwal_jam = jdw.id_jadwal_jam
			LEFT JOIN ruangan r ON r.id_ruangan = jdw.id_ruangan
			WHERE 
				kls.id_program_studi = {$id_program_studi} AND
				kls.id_semester = '{$id_semester}'
			ORDER BY hari.id_jadwal_hari, jam.jam_mulai";

		return $this->db->QueryToArray($sql);
	}

	public function get_kelas($id_kelas_mk)
	{
		$sql = 
			"SELECT 
				kls.id_kelas_mk,
				mk.kd_mata_kuliah, mk.nm_mata_kuliah, mk.kredit_semester,
				kls.kapasitas_kelas_mk,
				nk.nama_kelas
			FROM 
				kelas_mk kls
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kls.id_mata_kuliah
			LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kls.no_kelas_mk
			WHERE kls.id_kelas_mk = {$id_kelas_mk}";

		$this->db->Query($sql);

		return $this->db->FetchAssoc();
	}

	public function list_mahasiswa_of_kelas($id_kelas_mk)
	{
		$sql = 
			"SELECT
				mhs.id_mhs, mhs.nim_mhs, P.nm_pengguna, pd.nm_pengguna AS nm_dosen_wali, pmk.status_apv_pengambilan_mk
			FROM pengambilan_mk pmk
			JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
			JOIN pengguna P ON P.id_pengguna = mhs.id_pengguna
			LEFT JOIN dosen_wali dw ON dw.id_mhs = mhs.id_mhs AND dw.status_dosen_wali = 1
			LEFT JOIN dosen D ON D.id_dosen = dw.id_dosen
			LEFT JOIN pengguna pd ON pd.id_pengguna = D.id_pengguna
			WHERE pmk.id_kelas_mk = {$id_kelas_mk}
			ORDER BY mhs.nim_mhs";

		return $this->db->QueryToArray($sql);
	}

	public function get_semester_mhs($id_pengguna)
	{
		return $this->db->QueryToArray(
			"select a.id_semester,a.tahun_ajaran,a.nm_semester,a.group_semester
			from semester a, pengambilan_mk b, mahasiswa c
			where a.id_semester=b.id_semester and b.id_mhs=c.id_mhs and c.id_pengguna='" . $id_pengguna . "'
			group by a.id_semester,a.tahun_ajaran,a.nm_semester,a.group_semester
			order by a.tahun_ajaran desc, a.nm_semester desc");
	}

	public function get_angkatan_mhs($id_program_studi)
	{
		return $this->db->QueryToArray(
			"SELECT DISTINCT(THN_ANGKATAN_MHS) 
				FROM MAHASISWA m 
				JOIN STATUS_PENGGUNA sp ON sp.ID_STATUS_PENGGUNA = m.STATUS_AKADEMIK_MHS
				WHERE m.ID_PROGRAM_STUDI = '{$id_program_studi}' AND sp.AKTIF_STATUS_PENGGUNA = 1
				ORDER BY THN_ANGKATAN_MHS");
	}

	public function get_angkatan_mhs_with_lulus($id_program_studi)
	{
		return $this->db->QueryToArray(
			"SELECT DISTINCT(THN_ANGKATAN_MHS) 
				FROM MAHASISWA m 
				JOIN STATUS_PENGGUNA sp ON sp.ID_STATUS_PENGGUNA = m.STATUS_AKADEMIK_MHS
				WHERE m.ID_PROGRAM_STUDI = '{$id_program_studi}' AND (m.STATUS_AKADEMIK_MHS = 4 OR sp.AKTIF_STATUS_PENGGUNA = 1)
				ORDER BY THN_ANGKATAN_MHS");
	}

	public function mhs_nilai_kosong($id_program_studi, $thn_angkatan_mhs = null)
	{
		if(empty($thn_angkatan_mhs)){

			return $this->db->QueryToArray(
				"SELECT COUNT(*) AS JML_NILAI_KOSONG, pmk.ID_MHS, m.NIM_MHS, p.NM_PENGGUNA
					FROM PENGAMBILAN_MK pmk
					JOIN MAHASISWA m ON m.ID_MHS = pmk.ID_MHS
					JOIN PENGGUNA p ON p.ID_PENGGUNA = m.ID_PENGGUNA
					WHERE (pmk.NILAI_HURUF IS NULL OR pmk.NILAI_HURUF = 'E' OR pmk.NILAI_HURUF = '-')
						AND pmk.STATUS_APV_PENGAMBILAN_MK = 1
						AND m.ID_PROGRAM_STUDI = '{$id_program_studi}'
					GROUP BY pmk.ID_MHS, m.NIM_MHS, p.NM_PENGGUNA
					HAVING COUNT(*) > 0
					ORDER BY m.NIM_MHS");
		}
		else{

			return $this->db->QueryToArray(
				"SELECT COUNT(*) AS JML_NILAI_KOSONG, pmk.ID_MHS, m.NIM_MHS, p.NM_PENGGUNA
					FROM PENGAMBILAN_MK pmk
					JOIN MAHASISWA m ON m.ID_MHS = pmk.ID_MHS
					JOIN PENGGUNA p ON p.ID_PENGGUNA = m.ID_PENGGUNA
					WHERE (pmk.NILAI_HURUF IS NULL OR pmk.NILAI_HURUF = 'E' OR pmk.NILAI_HURUF = '-')
						AND pmk.STATUS_APV_PENGAMBILAN_MK = 1
						AND m.THN_ANGKATAN_MHS = '{$thn_angkatan_mhs}' AND m.ID_PROGRAM_STUDI = '{$id_program_studi}'
					GROUP BY pmk.ID_MHS, m.NIM_MHS, p.NM_PENGGUNA
					HAVING COUNT(*) > 0
					ORDER BY m.NIM_MHS");
		}
	}

	public function get_data_mhs($id_mhs)
	{
		$sql = 
			"SELECT 
				m.NIM_MHS,
				p.NM_PENGGUNA,
				m.THN_ANGKATAN_MHS,
				nk.NAMA_KELAS
			FROM 
				MAHASISWA m
			JOIN PENGGUNA p ON p.ID_PENGGUNA = m.ID_PENGGUNA
			LEFT JOIN MAHASISWA_KELAS mk ON mk.ID_MHS = m.ID_MHS
			LEFT JOIN NAMA_KELAS nk ON nk.ID_NAMA_KELAS = mk.ID_NAMA_KELAS
			WHERE m.ID_MHS = '{$id_mhs}'";

		$this->db->Query($sql);

		return $this->db->FetchAssoc();
	}

	public function get_data_nilai_kosong($id_mhs)
	{
		return $this->db->QueryToArray(
			"SELECT
		        pmk.id_mhs,
		        s.tahun_ajaran, s.nm_semester,
		        /* Kode MK */
		        COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
		        /* Nama MK */
        		COALESCE(mk1.nm_mata_kuliah, mk2.nm_mata_kuliah) AS nm_mata_kuliah, 
		        /* SKS */
		        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
		        /* Nilai Angka */
		        pmk.nilai_angka,
		        /* Nilai Huruf */
		        pmk.nilai_huruf,
		        /* Urutan Nilai Terbaik */
		        row_number() OVER (PARTITION BY pmk.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY sn.nilai_standar_nilai DESC) urut
		    FROM pengambilan_mk pmk
		    JOIN semester S ON S.id_semester = pmk.id_semester
		    -- Via Kelas
		    LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
		    LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
		    -- Via Kurikulum
		    LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
		    LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
		    -- Penyetaraan
		    LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
		    LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
		    -- nilai bobot
		    LEFT JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
		    WHERE 
		    	(pmk.nilai_huruf IS NULL OR pmk.nilai_huruf = 'E' OR pmk.nilai_huruf = '-') AND
		        pmk.status_apv_pengambilan_mk = 1 AND 
		        pmk.id_mhs = '{$id_mhs}'
		    ORDER BY s.TAHUN_AJARAN, s.NM_SEMESTER");
	}

	public function monitoring_biodata_mhs($id_program_studi, $angkatan=null) {
		if( ! empty($angkatan)) {
			return $this->db->QueryToArray("SELECT m.ID_MHS, m.NIM_MHS, p.NM_PENGGUNA, m.THN_ANGKATAN_MHS, 
											j.NM_JENJANG || ' - ' || ps.NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI, 
											sp.NM_STATUS_PENGGUNA, p.EMAIL_ALTERNATE, m.MOBILE_MHS,
											(SELECT COUNT(*) AS CEK 
												FROM MAHASISWA m1 
												JOIN PENGGUNA p1 ON p1.ID_PENGGUNA = m1.ID_PENGGUNA
												WHERE m1.ID_MHS = m.ID_MHS AND lahir_prop_mhs IS NOT NULL AND lahir_kota_mhs IS NOT NULL AND alamat_asal_mhs IS NOT NULL 
												AND mobile_mhs IS NOT NULL AND p1.email_alternate IS NOT NULL 
												AND id_sekolah_asal_mhs IS NOT NULL 
												AND nm_ayah_mhs IS NOT NULL AND nm_ibu_mhs IS NOT NULL 
												AND ASAL_KOTA_MHS IS NOT NULL AND ASAL_PROV_MHS IS NOT NULL 
												AND	ALAMAT_ASAL_MHS IS NOT NULL AND ALAMAT_ASAL_MHS_PROV IS NOT NULL 
												AND ALAMAT_ASAL_MHS_KOTA IS NOT NULL) AS CEK_KELENGKAPAN
											FROM MAHASISWA m
											JOIN PENGGUNA p ON p.ID_PENGGUNA = m.ID_PENGGUNA
											JOIN PROGRAM_STUDI ps ON ps.ID_PROGRAM_STUDI = m.ID_PROGRAM_STUDI
											JOIN JENJANG j ON j.ID_JENJANG = ps.ID_JENJANG
											JOIN STATUS_PENGGUNA sp ON sp.ID_STATUS_PENGGUNA = m.STATUS_AKADEMIK_MHS
											WHERE ps.STATUS_AKTIF_PRODI = 1 
											AND m.ID_PROGRAM_STUDI = '{$id_program_studi}'
											AND (m.STATUS_AKADEMIK_MHS = 4 OR sp.AKTIF_STATUS_PENGGUNA = 1)
											AND m.THN_ANGKATAN_MHS = '{$angkatan}'
											ORDER BY m.NIM_MHS");
		}
	}

	public function list_pengambilan_mk_mhs($id_mhs, $id_semester = null)
	{
		if ($id_semester != null) {
			$where_semester = "group_semester||thn_akademik_semester IN (SELECT group_semester||thn_akademik_semester FROM semester WHERE id_semester = {$id_semester}) AND";
		}

		return $this->db->QueryToArray(
			"SELECT 
					nm_semester, tahun_ajaran, thn_akademik_semester,
					kd_mata_kuliah,
					upper(nm_mata_kuliah) AS nm_mata_kuliah,
					nama_kelas,
					tipe_semester,
					kredit_semester,
					/* Status Belum Tampil */
					CASE WHEN flagnilai = 1 THEN nilai_huruf ELSE '*BT' END AS nilai_huruf,
					bobot,
					bobot_total,
					status_transfer,
					status_hapus,
					id_pengambilan_mk
				FROM (
					SELECT 
						pengambilan_mk.id_mhs,
						semester.nm_semester, semester.tahun_ajaran, semester.thn_akademik_semester,
						/* Kode MK */
				        COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
				        /* Nama MK */
				        COALESCE(mk1.nm_mata_kuliah, mk2.nm_mata_kuliah) AS nm_mata_kuliah,
				        /* SKS */
				        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS kredit_semester,
						nama_kelas,
						tipe_semester,

						CASE 
						  WHEN pengambilan_mk.nilai_huruf IS NULL THEN '*K' 
						  ELSE pengambilan_mk.nilai_huruf
						END AS nilai_huruf,

						/* Bobot */
						CASE
						  WHEN standar_nilai.nilai_standar_nilai IS NULL THEN 0
						  ELSE standar_nilai.nilai_standar_nilai
						END AS bobot,

						/* Bobot Total = Standar Nilai x SKS */
						COALESCE(standar_nilai.nilai_standar_nilai * mk1.kredit_semester, standar_nilai.nilai_standar_nilai * mk2.kredit_semester, 0) AS bobot_total,

						/* Ranking berdasar kode */
						row_number() OVER(PARTITION BY pengambilan_mk.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY nilai_huruf) AS rangking,

						pengambilan_mk.id_pengambilan_mk,
						pengambilan_mk.flagnilai,
						pengambilan_mk.status_transfer, pengambilan_mk.status_hapus
					FROM pengambilan_mk
					JOIN mahasiswa			ON pengambilan_mk.id_mhs = mahasiswa.id_mhs
					JOIN semester			ON pengambilan_mk.id_semester = semester.id_semester
					JOIN program_studi		ON mahasiswa.id_program_studi = program_studi.id_program_studi
					-- Via Kelas
					LEFT JOIN kelas_mk		ON pengambilan_mk.id_kelas_mk = kelas_mk.id_kelas_mk
		    		LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kelas_mk.id_mata_kuliah
					LEFT JOIN nama_kelas	ON kelas_mk.no_kelas_mk = nama_kelas.id_nama_kelas
					-- Via Kurikulum
					LEFT JOIN kurikulum_mk	ON pengambilan_mk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk
					LEFT JOIN mata_kuliah mk2 ON kurikulum_mk.id_mata_kuliah = mk2.id_mata_kuliah
					-- Penyetaraan
				    LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
				    LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
					LEFT JOIN standar_nilai	ON pengambilan_mk.nilai_huruf = standar_nilai.nm_standar_nilai
					WHERE
						{$where_semester}
						tipe_semester IN ('SP', 'UP','REG') AND
						status_apv_pengambilan_mk = 1 AND 
						pengambilan_mk.status_hapus = 0 AND 
						pengambilan_mk.status_pengambilan_mk != 0 AND
						mahasiswa.id_mhs = '" . $id_mhs . "'
				)
				WHERE rangking = 1
				ORDER BY 3, 1, 5"
		);

		/*return $this->db->QueryToArray(
			"SELECT
				PMK.ID_PENGAMBILAN_MK,
				S.NM_SEMESTER, S.TAHUN_AJARAN, S.THN_AKADEMIK_SEMESTER,
				COALESCE(MK1.KD_MATA_KULIAH, MK2.KD_MATA_KULIAH) AS KD_MATA_KULIAH,
				COALESCE(MK1.NM_MATA_KULIAH, MK2.NM_MATA_KULIAH) AS NM_MATA_KULIAH,
				COALESCE(MK1.KREDIT_SEMESTER, MK2.KREDIT_SEMESTER) AS KREDIT_SEMESTER,
				NK.NAMA_KELAS,
				PMK.NILAI_HURUF,
				COALESCE(MK1.KREDIT_SEMESTER, MK2.KREDIT_SEMESTER) * SN.NILAI_STANDAR_NILAI AS BOBOT,
				PMK.STATUS_TRANSFER, PMK.STATUS_HAPUS
			FROM PENGAMBILAN_MK PMK
			JOIN SEMESTER S ON S.ID_SEMESTER = PMK.ID_SEMESTER
			/* VIA KELAS *
			LEFT JOIN KELAS_MK KLS ON KLS.ID_KELAS_MK = PMK.ID_KELAS_MK
			LEFT JOIN MATA_KULIAH MK1 ON MK1.ID_MATA_KULIAH = KLS.ID_MATA_KULIAH
			LEFT JOIN NAMA_KELAS NK ON NK.ID_NAMA_KELAS = KLS.NO_KELAS_MK
			/*GANTI NAMBI
			LEFT JOIN KELAS_MK KLS ON KLS.ID_KELAS_MK = PMK.ID_KELAS_MK
			LEFT JOIN KURIKULUM_MK KMK1 ON KMK1.ID_KURIKULUM_MK = KLS.ID_KURIKULUM_MK
			LEFT JOIN MATA_KULIAH MK1 ON MK1.ID_MATA_KULIAH = KMK1.ID_MATA_KULIAH
			LEFT JOIN NAMA_KELAS NK ON NK.ID_NAMA_KELAS = KLS.NO_KELAS_MK
			*/
			/* VIA KURIKULUM *
			LEFT JOIN KURIKULUM_MK KMK2 ON KMK2.ID_KURIKULUM_MK = PMK.ID_KURIKULUM_MK
			LEFT JOIN MATA_KULIAH MK2 ON MK2.ID_MATA_KULIAH = KMK2.ID_MATA_KULIAH
			/* BOBOT *
			LEFT JOIN STANDAR_NILAI SN ON SN.NM_STANDAR_NILAI = PMK.NILAI_HURUF
			WHERE
				PMK.STATUS_APV_PENGAMBILAN_MK = 1 AND
				PMK.ID_MHS = {$id_mhs}
			ORDER BY 4, 2, 6"
		);*/
	}

	/**
	 * Mendapatkan IPK Mahasiswa dan total SKS hingga semester ke $id_semester. Jika $id_semester = null, maka data yang dihasilkan
	 * adalah IPK Kumulatif
	 */
	public function get_ipk_mhs($id_mhs, $id_semester = null)
	{
		$where_semester = "";

		if ($id_semester != null) {
			$where_semester = "AND s.fd_id_smt <= (SELECT fd_id_smt FROM semester WHERE id_semester = {$id_semester})";
		}

		$this->db->Query(
			"SELECT 
				id_mhs,
				SUM(sks) AS total_sks, 
				SUM(bobot) AS total_bobot,
				round(SUM(bobot) / SUM(sks), 2) AS ipk 
			FROM (
				SELECT
					mhs.id_mhs,
					/* Kode MK utk grouping total mutu */
					COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
					/* SKS */
					COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
					/* Nilai Mutu = SKS * Bobot */
					COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS bobot,
					/* Jika seri, perulangan tetap ditotal */
					CASE kls.status_seri
						WHEN 1 THEN 1
						WHEN 0 THEN
							/* Urutan Nilai Terbaik */
							row_number() OVER (PARTITION BY mhs.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY sn.nilai_standar_nilai DESC)
					END AS urut
				FROM pengambilan_mk pmk
				JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
				JOIN semester S ON S.id_semester = pmk.id_semester
				-- Via Kelas
				LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
				LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
				-- Via Kurikulum
				LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
				LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
				-- Penyetaraan
				LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
				LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
				-- nilai bobot
				JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
				WHERE 
					pmk.status_apv_pengambilan_mk = 1 AND 
					mhs.id_mhs = {$id_mhs} {$where_semester}
			)
			WHERE urut = 1
			GROUP BY id_mhs"
		);

		return $this->db->FetchAssoc();
	}
}