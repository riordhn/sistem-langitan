<?php
/*
YAH 06/06/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

//print_r($user);

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');


$kdprodi= $user->ID_PROGRAM_STUDI; 
$id_thkur=getvar("select id_kurikulum_prodi from kurikulum_prodi 
where id_program_studi=$kdprodi and status_aktif=1");
$smarty->assign('IDKURPRODI',$id_thkur['ID_KURIKULUM_PRODI']);

$id_minat=getvar("select id_prodi_minat from prodi_minat where id_program_studi=$kdprodi and rownum <=1");
$smarty->assign('IDPRODIMINAT',$id_minat['ID_PRODI_MINAT']);

$id_status_mk=getvar("select * from status_mk where id_program_studi=$kdprodi order by nm_status_mk and rownum <=1");
$smarty->assign('IDSTATUS_MK',$id_status_mk['ID_STATUS_MK']);

$datakur=getData("select id_kurikulum_prodi,nm_kurikulum||'-'||tahun_kurikulum as nama from kurikulum_prodi
left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
where kurikulum_prodi.id_program_studi=$kdprodi");
$smarty->assign('T_KUR', $datakur);

$dataminat=getData("select * from prodi_minat where id_program_studi=$kdprodi");
$smarty->assign('T_MINAT', $dataminat);

$datastatus_mk=getData("select * from status_mk where id_program_studi=$kdprodi order by nm_status_mk");
$smarty->assign('T_STATUS_MK', $datastatus_mk);

$status = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'view';
//echo $status;

switch($status) {
	 case 'view':
		 //echo $id_thkur['ID_KURIKULUM_PRODI'];
		//$thkur= $_POST['thkur'];
		$thkur = isSet($_POST['thkur']) ? $_POST['thkur'] :$id_thkur['ID_KURIKULUM_PRODI'];
        break;
		
	case 'del':
		 // pilih
		
		$idkm= $_GET['id_km'];
		
		deleteData("delete from kurikulum_minat where id_kurikulum_minat='$idkm'");
        
        break;
	
	case 'update1':
		 // pilih
		$id_km= $_POST['id_km'];		
		$status_mk= $_POST['status_mk'];
		
		UpdateData("update kurikulum_minat set id_status_mk='$status_mk' where id_kurikulum_minat=$id_km");
        break;
   
	case 'viewup':
		//$thkur= $_POST['thkur'];
		$idkm= $_GET['id_km'];
		if (trim($idkm) != '') {
		
		$smarty->assign('IDKM',$idkm);
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block'); 
		$smarty->assign('disp3','none');}

		//$datamp=getData("select * from prodi_minat where id_program_studi=$kdprodi");
		//$smarty->assign('MP_PRODI', $datamp);

		//$datakelmk=getData("select * from kelompok_mk");
		//$smarty->assign('KEL_MK', $datakelmk);

		$datamk=getData("select * from status_mk");
		$smarty->assign('ST_MK', $datamk);
		
		$mk=getData("select kmm.id_kurikulum_minat,kmk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_tatap_muka,
kredit_praktikum,kmk.kredit_semester,kmm.id_status_mk,nm_status_mk,nm_kelompok_mk,kurikulum_prodi.tahun_kurikulum,
tingkat_semester from kurikulum_minat kmm
left join kurikulum_mk kmk on kmm.id_kurikulum_mk=kmk.id_kurikulum_mk
left join prodi_minat pm on kmm.id_prodi_minat=pm.id_prodi_minat
left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join status_mk on kmm.id_status_mk=status_mk.id_status_mk 
left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk 
left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi 
where id_kurikulum_minat=$idkm");
		
		$smarty->assign('T_MK1', $mk);
           
	break;
	
	case 'setting':
		
		if ($_GET['st']==1) {
			$idkurprodi= $_GET['thkur1'];
			$idminat= $_GET['minatprodi'];
			$idstatus_mk= $_GET['status_mk'];
			$id_kurikulum_mk= $_GET['id_kurmk'];
			//insert ke kurikulum_minat
			tambahdata("kurikulum_minat","id_kurikulum_mk,id_prodi_minat,id_status_mk,id_kurikulum_prodi","$id_kurikulum_mk,$idminat,$idstatus_mk,$idkurprodi");

		} else
		{
			$idkurprodi= $_POST['thkur1'];
			$idminat= $_POST['minatprodi'];
			$idstatus_mk= $_POST['status_mk'];
		}
		
		$smarty->assign('IDKURPRODI', $idkurprodi);
		$smarty->assign('IDPRODIMINAT', $idminat);
		$smarty->assign('ID_STATUS_MK', $idstatus_mk);
		
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');


		$mk=getData("select kmk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kmk.kredit_semester,nm_kelompok_mk,
tingkat_semester,kurikulum_prodi.tahun_kurikulum from kurikulum_mk kmk 
left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join status_mk on kmk.id_status_mk=status_mk.id_status_mk 
left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk 
left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi 
where kmk.id_program_studi=$kdprodi and kmk.id_kurikulum_prodi=$idkurprodi
and id_kurikulum_mk not in (select id_kurikulum_mk from kurikulum_minat where id_prodi_minat=$idminat and id_kurikulum_prodi=$idkurprodi)
order by nm_mata_kuliah");
		$smarty->assign('INSERT_MK', $mk);
           
	break;


}

$thkur = isSet($_POST['thkur']) ? $_POST['thkur'] :$id_thkur['ID_KURIKULUM_PRODI'];
$id_minat = isSet($_POST['minat']) ? $_POST['minat'] :$id_minat['ID_PRODI_MINAT'];

$smarty->assign('IDKP', $thkur);
$smarty->assign('IDM', $id_minat);

$mk=getData("select kmm.id_kurikulum_minat,kmk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_tatap_muka,
kredit_praktikum,kmk.kredit_semester,nm_status_mk,nm_kelompok_mk,kurikulum_prodi.tahun_kurikulum,
tingkat_semester from kurikulum_minat kmm
left join kurikulum_mk kmk on kmm.id_kurikulum_mk=kmk.id_kurikulum_mk
left join prodi_minat pm on kmm.id_prodi_minat=pm.id_prodi_minat
left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join status_mk on kmm.id_status_mk=status_mk.id_status_mk 
left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk 
left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi 
where kmk.id_program_studi=$kdprodi and kmk.id_kurikulum_prodi=$thkur and id_prodi_minat=$id_minat
order by nm_mata_kuliah");
$smarty->assign('T_MK', $mk);
$smarty->display('daftarmkminat.tpl');  

?>
