<?php
require('common.php');
require_once ('ociFunction.php');

error_reporting (E_ALL & ~E_NOTICE);
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pengguna= $user->ID_PENGGUNA; 

$fak=getvar("select fakultas.id_fakultas from pegawai left join unit_kerja on pegawai.id_unit_kerja=unit_kerja.id_unit_kerja 
left join program_studi on unit_kerja.id_program_studi=program_studi.id_program_studi 
left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
where pegawai.id_pengguna=$id_pengguna");
$kdfak=$fak['ID_FAKULTAS'];

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester order by thn_akademik_semester desc,nm_semester");
$smarty->assign('T_ST', $smt);

$smt1 = isSet($_POST['smt']) ? $_POST['smt'] : $smtaktif['ID_SEMESTER'];
		
if ($smt1!='') {
		$smarty->assign('SMT',$smt1);
		
		$jaf=getData("select id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,tm, count(nim_mhs) as mhscekal, 
case when id_kelas_mk in (select distinct id_kelas_mk from pengambilan_mk where id_semester=$smt1 and status_cekal=0) then 'SUDAH' else 'BELUM' end as status
from
(select s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester, s1.id_mhs,nim_mhs,nm_pengguna,
nama_kelas,sum(kehadiran) as hadir,count(id_presensi_kelas) as tm, round(sum(kehadiran)/count(id_presensi_kelas)*100,2)as hadir 
from ( select presensi_mkmhs.id_presensi_kelas,id_kelas_mk,presensi_mkmhs.id_mhs,kehadiran 
from presensi_mkmhs 
left join presensi_kelas on presensi_mkmhs.id_presensi_kelas=presensi_kelas.id_presensi_kelas 
left join mahasiswa on presensi_mkmhs.id_mhs=mahasiswa.id_mhs 
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi  
where program_studi.id_fakultas=$kdfak and id_kelas_mk in (select id_kelas_mk from kelas_mk where id_semester=$smt1) 
order by presensi_mkmhs.id_presensi_kelas,presensi_mkmhs.id_mhs) s1 
left join mahasiswa on s1.id_mhs=mahasiswa.id_mhs 
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna 
left join kelas_mk on s1.id_kelas_mk=kelas_mk.id_kelas_mk 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
where kelas_mk.id_semester=$smt1 
group by s1.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, 
s1.id_mhs, nim_mhs, nm_pengguna, nama_kelas 
having sum(kehadiran)/count(id_presensi_kelas)<0.75 
order by kd_mata_kuliah,nim_mhs) 
group by id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, nama_kelas, tm, case when id_kelas_mk in (select distinct id_kelas_mk from pengambilan_mk where id_semester=5 and status_cekal=2) then 'BELUM' else 'SUDAH' end
order by kd_mata_kuliah");



$smarty->assign('T_MK', $jaf);

}

if ($_GET['action']=='cekal') {
	$idkelas= $_GET['id_kelas_mk'];
	$smarty->assign('ID_KELAS',$idkelas);
	$kd_mata_kuliah= $_GET['kd_mk'];
	
	$smarty->assign('disp1','none');
	$smarty->assign('disp2','block');

$datacekal=getData("select s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester, 
s1.id_mhs,nim_mhs,nm_pengguna,nama_kelas,sum(kehadiran) as hadir,
case when s1.id_mhs in (select id_mhs from pengambilan_mk 
where status_cekal=0 and id_semester='$smtaktif[ID_SEMESTER]' and id_kelas_mk=$idkelas) then 'checked' else 'non' end as tanda,
count(id_presensi_kelas) as tm, round(sum(kehadiran)/count(id_presensi_kelas)*100,2)as prosen, 
case when s1.id_mhs  in (select s1.id_mhs  from pengambilan_mk 
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
where (id_semester!='$smtaktif[ID_SEMESTER]' or id_semester is null) and kd_mata_kuliah='$kd_mata_kuliah') then 'Ulang' else 'Baru' end as status 
from ( select presensi_mkmhs.id_presensi_kelas,id_kelas_mk,presensi_mkmhs.id_mhs,kehadiran 
from presensi_mkmhs 
left join presensi_kelas on presensi_mkmhs.id_presensi_kelas=presensi_kelas.id_presensi_kelas 
left join mahasiswa on presensi_mkmhs.id_mhs=mahasiswa.id_mhs 
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
where program_studi.id_fakultas=$kdfak and id_kelas_mk in (select id_kelas_mk from kelas_mk where id_semester='$smtaktif[ID_SEMESTER]') 
order by presensi_mkmhs.id_presensi_kelas,presensi_mkmhs.id_mhs) s1 
left join mahasiswa on s1.id_mhs=mahasiswa.id_mhs 
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna 
left join kelas_mk on s1.id_kelas_mk=kelas_mk.id_kelas_mk 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
where kelas_mk.id_semester='$smtaktif[ID_SEMESTER]' and s1.id_kelas_mk=$idkelas 
group by s1.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, s1.id_mhs, nim_mhs, nm_pengguna, nama_kelas, 
case when s1.id_mhs in (select id_mhs from pengambilan_mk 
where status_cekal=0 and id_semester='$smtaktif[ID_SEMESTER]' and id_kelas_mk=$idkelas) then 'checked' else 'non' end, case when s1.id_mhs  in (select id_mhs from pengambilan_mk 
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
where (id_semester!='$smtaktif[ID_SEMESTER]' or id_semester is null) and kd_mata_kuliah='$kd_mata_kuliah') then 'Ulang' else 'Baru' end 
having sum(kehadiran)/count(id_presensi_kelas)<0.75 
order by nim_mhs");

$smarty->assign('DATA_CEKAL', $datacekal);
}	

if ($_POST['action']=='proses') {
	$idkelas= $_POST['id_kelas_mk'];
	$counter=$_POST['counter'];
	
	$smarty->assign('disp1','block');
	$smarty->assign('disp2','none');
	
	//$mhs=getData("select id_mhs from pengambilan_mk where id_kelas_mk=$idkelas  and id_semester='$smtaktif[ID_SEMESTER]'");
	

for ($i=1; $i<=$counter; $i++)
	{
	//echo $mhs['ID_MHS'];
	if ($_POST['cekalmhs'.$i]<>'' || $_POST['cekalmhs'.$i]<>null) {
			   gantidata("update pengambilan_mk set status_cekal=3 where id_kelas_mk=$idkelas and id_mhs='".$_POST['cekalmhs'.$i]."' and id_semester='$smtaktif[ID_SEMESTER]'");
			   //echo "update pengambilan_mk set status_cekal=0 where id_kelas_mk=$idkelas and id_mhs='".$_POST['mhs'.$i]."' and id_semester='$smtaktif[ID_SEMESTER]'";
			   //gantidata("update pengambilan_mk set id_kelas_mk=$kelas_mk_tujuan where id_pengambilan_mk='".$_POST['pil'.$i]."'");
			} 
	}
	 gantidata("update pengambilan_mk set status_cekal=1 where id_kelas_mk=$idkelas and (status_cekal=0 or status_cekal is null) and id_semester='$smtaktif[ID_SEMESTER]'");
	 gantidata("update pengambilan_mk set status_cekal=1 where id_kelas_mk=$idkelas and (status_cekal=2 or status_cekal is null) and id_semester='$smtaktif[ID_SEMESTER]'");
	 gantidata("update pengambilan_mk set status_cekal=0 where id_kelas_mk=$idkelas and (status_cekal=3) and id_semester='$smtaktif[ID_SEMESTER]'");

	 //echo "update pengambilan_mk set status_cekal=1 where id_kelas_mk=$idkelas and status_cekal=2  and id_semester='$smtaktif[ID_SEMESTER]'";
}
$smarty->display('proses-cekal-otomatis.tpl');
?>
