<?php
error_reporting (E_ALL & ~E_NOTICE);
require_once('config.php');
require_once ('ociFunction.php');

if ($user->ID_FAKULTAS == 8 ) { header('Location: /modul/prodi/fst/'); }

$id_prodi= $user->ID_PROGRAM_STUDI; 

if ($user->IsLogged()&& $user->Role() == AUCC_ROLE_PRODI && $id_prodi<>null && $id_prodi<>"") {
	
    $dataprodi=getData("select nm_jenjang||'-'||nm_program_studi as nm_prodi from program_studi 
    left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
    where id_program_studi=$id_prodi");
    $smarty->assign('dataprodi',$dataprodi);
    
    $smarty->assign('modul_set', $user->MODULs);

    $smarty->display('index.tpl');
    
} else {
    
    echo '<script>alert(" Maaf..., Data Anda Belum Lengkap, Anda Belum BIsa Login..Periksa Kembali Data Anda, Silahkan Hubungi Admin CyberCampus ");window.location="../../logout.php";</script>';
}
?>