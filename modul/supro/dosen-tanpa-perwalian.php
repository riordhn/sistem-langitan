<?php
/** 
* @author Choirul Hidayat, http://www.choirulhidayat.wordpress.com [Freelance Programmer]
* @contributor all AUCC Developers, dibuat berdasarkan manual PHP5 dan OCI8
* @version 0.1-beta // WARNING: Masih belum lengkap dan stabil.
* @start Wednesday, March 09, 2011
* @lastchange Wednesday, March 09, 2011
* @copyright 2011 Universitas Airlangga
* @project Airlangga University - Cyber Campus
* @link http://www.unair.ac.id
*/
/*===========================================================================*/
require('common.php');
$smarty->display('dosen-tanpa-perwalian.tpl');
?>
