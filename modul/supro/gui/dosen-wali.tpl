{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
      3: { sorter: false }
		}
		}
	);
}
);

	var i = $('input type="text"').size() + 2;

	$('#addButton').click(function() {


		 $('#tableText tr:last').after('<tr><td>Mahasiswa ' + i + '</td>' +
		  '<td>:</td>' +
		  '<td>' +
          ' <select name="select"> ' +
                  '<option value"">-- Mahasiswa --</option>' +
				  '    {foreach $dosen_pengguna as $dosen}'+
               	  '			<option value="{$dosen.id_dosen}">{$dosen.nama_dosen}</option>' +
             	  '	   {/foreach}'+
          ' </select>' +
          '</td></tr>').
			animate({ opacity: "show" }, "slow");
		i++;
	});


	function ubah(ID_MHS){
		$.ajax({
		type: "POST",
        url: "dosen-wali.php",
        data: "action=savewali&dosen="+$("#id_dosen"+ID_MHS).val()+"&id_mhs="+$("#id_mhs"+ID_MHS).val(),
        cache: false,
        success: function(data){
			confirm("Yakin Untuk Menyimpan?");
			$('#content').html(data);
        }
        });
	}
	function update(ID_MHS){
		$.ajax({
		type: "POST",
        url: "dosen-wali.php",
        data: "action=update&dosen="+$("#id_dosen").val()+"&id_mhs="+$("#id_mhs"+ID_MHS).val(),
        cache: false,
        success: function(data){
			confirm("Yakin Untuk Menyimpan?");
			$('#content').html(data);
        }
        });
	}
</script>
{/literal}

<div class="center_title_bar">Penetapan Dosen Wali </div>
<form action="dosen-wali.php" method="post">
<input type="hidden" name="action" value="tampil" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		Tahun Ajaran : 
			<select name="thn_ajaran" id="thn_ajaran">
				<option value="">-- Tahun Ajaran --</option>
				{foreach item="smt" from=$T_ST}
				{html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$smtaktif}
				{/foreach}
			</select>
			<input type="submit" name="View" value="View">
		</td>
	</tr>
</table>
</form>

<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" align="center" onclick="javascript: displayPanel('2');">Mhs No Wali</div>
	<div id="tab3" class="tab" align="center" onclick="javascript: displayPanel('3');">Update</div>
	<div id="tab4" class="tab" align="center" onclick="javascript: displayPanel('4');">Dist. Perwalian</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table>
	<tr bgcolor="#efefef">
{foreach item="agk" from=$ANG}
		<td><a href="dosen-wali.php?action=tampil&angk={$agk.ANGKATAN}"><strong>{$agk.ANGKATAN}</strong></a></td>
{/foreach}
	</tr>
</table>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Nim</th>
			<th>Nama Mahasiswa</th>
			<th>Dosen Wali</th>
			<th class="noheader">Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="wali" from=$WALI}
		<tr>
			<td>{$wali.NIM_MHS}</td>
			<td>{$wali.MHS}</td>
			<td>{$wali.DOLI}</td>
			<td><center><a href="dosen-wali.php?action=viewupdate&id_mhs={$wali.ID_MHS}">Update</a></center></td>
		</tr>
	{foreachelse}
		<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable1").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
            2: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Nim</th>
			<th>Nama Mahasiswa</th>
			<th class="noheader">Dosen Wali</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=nowali item="mhs_nowali" from=$NOWALI}
		<input type="hidden" value="{$mhs_nowali.ID_MHS}" name="mhs" id="id_mhs{$mhs_nowali.ID_MHS}" />
		<tr>
			<td>{$mhs_nowali.NIM_MHS}</td>
			<td>{$mhs_nowali.NM_PENGGUNA}</td>
			<td>
				<select name="dosen" id="id_dosen{$mhs_nowali.ID_MHS}" onChange="ubah({$mhs_nowali.ID_MHS})">
					<option value="">-- Pilih Dosen --</option>
					{foreach item="dosen" from=$DOSEN}
					{html_options values=$dosen.ID_DOSEN output=$dosen.NAMA_DOSEN}
					{/foreach}
				</select>
			</td>
		</tr>
		{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
</table>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
</p> </p>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Nim</th>
			<th>Nama Mahasiswa</th>
			<th>Dosen Wali</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=nowali item="mhs_doli" from=$DOLI}
	<input type="hidden" value="{$mhs_doli.ID_MHS}" name="mhs" id="id_mhs{$mhs_doli.ID_MHS}" />
		<tr>
			<td>{$mhs_doli.NIM_MHS}</td>
			<td>{$mhs_doli.MHS}</td>
			<td>
				<select name="dosen" id="id_dosen" onChange="update({$mhs_doli.ID_MHS})">
					<option value="">-- Pilih Dosen --</option>
					{foreach item="dosen" from=$DOSEN}
					{html_options values=$dosen.ID_DOSEN output=$dosen.NAMA_DOSEN}
					{/foreach}
				</select>
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable2").tablesorter(
		{
		sortList: [[2,1]],
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel4" style="display: {$disp4}">
<p> </p>
<table id="myTable2" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Nip</th>
			<th>Nama Dosen</th>
			<th>Total Mahasiswa</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="seb" from=$SEBDOLI}
		<tr>
			<td>{$seb.NIP_DOSEN}</td>
			<td>{$seb.NM_PENGGUNA}</td>
			<td>{$seb.TTL}</td>
		</tr>
	{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

{literal}
 <script>$('form').validate();</script>
{/literal}

