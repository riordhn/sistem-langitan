{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,1]],
		headers: {
            3: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="center_title_bar">Master Kurikulum (Program Studi)</div>
	<div id="tabs">
		<div id="tab1" {if $smarty.get.action == '' || $smarty.get.action == 'tampil'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
		<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
		{*<div id="tab3" {if $smarty.get.action == 'tampil'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>*}
   	</div>
<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Nama Kurikulum</th>
			<th>Thn Dibuat</th>
			<th>Keterangan</th>
			<th class="noheader">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach item="list" from=$T_KUR}
		<tr>
			<td>{$list.NM_KURIKULUM}</td>
			<td><center>{$list.THN_KURIKULUM}</center></td>
			<td>{$list.KETERANGAN_KURIKULUM}</td>
			<td><center><a href="kurikulum.php?action=tampil&id_kurikulum={$list.ID_KURIKULUM}" onclick="displayPanel('3')">Update</a> | <a href="kurikulum.php?action=del&id_kurikulum={$list.ID_KURIKULUM}">Delete</a></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel2" style="display:{$disp2} ">
<p> </p>
<form action="kurikulum.php" method="post" >
<input type="hidden" name="action" value="add" >
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td width="20%">Nama Kurikulum</td>
	  <td width="2%"><center>:</center></td>
	  <td width="78%"><input type="text" name="nama_kurikulum" id="nama_kurikulum" size="80" /></td>
	</tr>
	<tr>
	  <td>Keterangan Kurikulum</td>
	  <td><center>:</center></td>
	  <td><input type="text" name="ket_kurikulum" id="nama_kurikulum2" size="80" /></td>
	</tr>
	<tr>
	  <td>Tahun Dibuat</td>
	  <td><center>:</center></td>
	  <td>
		<select name="thn_kurikulum" id="thn_kurikulum">
			<option value="">-- Tahun Kurikulum --</option>
			{for $i=$thn_skrg; $i>=($thn_skrg-10); $i--}
			<option value="{$i}">{$i}</option>
			{/for}
		</select>
	  </td>
	</tr>
</table>
<p><input type="submit" name="ttambah" value="Simpan" id="ttambah"></p>
</form>
</div>

<div class="panel" id="panel3" style="display: {$disp3} ">
<p> </p>
<form action="kurikulum.php" method="post" >
<input type="hidden" name="action" value="update" >
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
{foreach item="datakur" from=$T_KUR}
{if $datakur.ID_KURIKULUM==$id_kur}
<input type="hidden" name="id_kur" value="{$id_kur}" >
	<tr>
	  <td width="20%">Nama Kurikulum</td>
	  <td width="2%"><center>:</center></td>
	  <td width="78%"><input type="text" name="nama_kurikulum" class="required" value="{$datakur.NM_KURIKULUM}" size="80" /></td>
	</tr>
	<tr>
	  <td>Keterangan Kurikulum</td>
	  <td><center>:</center></td>
	  <td><input type="text" name="keterangan_kurikulum" class="required" value="{$datakur.KETERANGAN_KURIKULUM}" size="80" />
	  </td>
	</tr>
	<tr>
	  <td>Tahun Dibuat</td>
	  <td>:</td>
	  <td>
		 <select name="thn_kurikulum" id="thn_kurikulum">
			<option value="{$datakur.THN_KURIKULUM}">{$datakur.THN_KURIKULUM}</option>
			{for $i=$thn_skrg; $i>=($thn_skrg-15); $i--}
			<option value="{$i}">{$i}</option>
			{/for}
		 </select>
	  </td>
	</tr>
{/if}
{/foreach}
</table>
<p><input type="submit" name="tambah" value="Update"></p>
</form>
</div>

<script>$('form').validate();</script>
