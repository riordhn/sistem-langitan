<script type="text/javascript">
var editState = false;
$("#dosen").change(function(){
	dosen = $("#dosen").val();
	if(dosen == ""){
	  clearForm();
	}else{
		$('#fakultas').attr('disabled','');
	}
})

$("#fakultas").change(function(){
		prodi();
});


$("#tab2").click(function(){
	if(!editState){
	displayPanel('2');
	}else{
		alert('Anda sedang melakukan edit data, klik tombol Batal');
	}});


$("#tab1").click(function(){
	clearForm();
	displayPanel('1');
	});

function clearForm(){
		$('#dosen').val('');
		$('#fakultas').val('');
		$("#prodi").val('');
	 	$('#fakultas').attr('disabled','disabled');
		prodi();
}


function prodi(){
		fakultas = $("#fakultas").val();
        $("#loading").show();
		datanya = "&fakultas="+fakultas
	   $.ajax({
		type: "POST",
        url: "proses/getProdi.php",
        data: "op=getProdi"+datanya,
        cache: false,
        success: function(data){
           	 $('#prodi').html(data);
        }
        });		
		$("#loading").hide();
}


function editProdi(prodi){
		fakultas = $("#editFakultas").val();
        $("#loading").show();
		datanya = "&fakultas="+fakultas+"&prodi="+prodi
	   $.ajax({
		type: "POST",
        url: "proses/getProdi.php",
        data: "op=getProdi"+datanya,
        cache: false,
        success: function(data){
				$('#editProdi').html(data);
			}
        });		
		$("#loading").hide();
}

function editData(id){
	 if(!editState){
		   var element = '#tr'+id; 
		   $($('#tableFoot').html()).insertAfter(element); 
		   $('#tableFoot').empty(); 
		   $('#id').val(id);
		   $('#no').html($(element+' td:nth-child(1)').text());
		   $('#nomer').val($(element+' td:nth-child(1)').text()); 
		   $('select[@name=editDosen] option[value='+$('#dosen'+id).val()+']').attr('selected',true);
		   $('select[@name=editFakultas] option[value='+$('#fakultas'+id).val()+']').attr('selected',true);
		   prod = $('#prodi'+id).val();
		   editProdi(prod);
		   $("#editFakultas").change(function(){
					editProdi('');
			});
		   $(element).hide(); 
		   editState = true;
	   }else{
		   alert('Anda sedang melakukan edit data, klik tombol Batal');
	   }
}


function cancelForm(){
		if(editState){ 
			var element = '#tr'+$('#id').val();
			$(element).show(); 
			var html = '<tr>'+$(element).next().html()+'</tr>';
			$('#tableFoot').html(html); 
			$(element).next().remove();
			editState = false;
		}else{
			$('#tableFoot').hide();
		}
		$('#id').val('');
		$('#editDosen').val('');
		$('#editFakultas').val('');
		$('#editProdi').val('');
		$('#nomer').val('');
	}
	

$("#finput").validate({
  rules: {
	  dosen: "required",
	  fakultas: "required",
	  prodi: "required"
  },
  messages: {
	  dosen: "Harus dipilih",
	  fakultas: "Harus dipilih",
	  prodi: "Harus dipilih"
  },
  submitHandler: function() { 
		$("#loading").show();
		$.ajax({
		type: "POST",
        url: "proses/mkwu-proses.php",
        data: "mode=input&"+$("#finput").serialize(),
        cache: false,
        success: function(data){
				alert("Data sudah dimasukkan");
				clearForm();
				$("#tableBody").html(data);
				$("#loading").hide();
		}
        });
	}
});


$("#fedit").validate({
  rules: {
	  editProdi: "required"
  },
  messages: {
	  editProdi: ""
  },
  submitHandler: function() { 
  		var element = '#tr'+$('#id').val();
		$("#loading").show();
		$.ajax({
		type: "POST",
        url: "proses/mkwu-proses.php",
        data: "mode=update&"+$("#fedit").serialize(),
        cache: false,
        success: function(data){
				alert("Data sudah diupdate");
				clearForm();		
				$(element).html(data);		
				$("#loading").hide();
		}
        });
	}
});

</script>

<div class="center_title_bar">MKWU</div>  
   <img src="../../img/akademik_images/loading.gif" id="loading" style="display:none">
<br><br>
		<div id="tabs">
        <div id="tab1" class="tab_sel" align="center">Rincian</div>
        <div id="tab2" class="tab" style="margin-left:1px;" align="center">Input</div>
    	</div>
        <div class="tab_bdr"></div>
        <div class="panel" id="panel1" style="display: block">  
        <form id="fedit" name="fedit">  
        <table width="70%" border="0" cellspacing="0" cellpadding="0">
           <tr class="left_menu">
             <td width="11%" bgcolor="#333333"><div align="center"><font color="#FFFFFF">No.</font></div></td>
             <td width="31%" bgcolor="#333333"><div align="center"><font color="#FFFFFF">Dosen</font> </div></td>
             <td width="31%" bgcolor="#333333"><div align="center"><font color="#FFFFFF">Fakultas</font></div></td>
             <td width="31%" bgcolor="#333333"><div align="center"><font color="#FFFFFF">Prodi</font></div></td>
             <td width="27%" bgcolor="#333333"><div align="center"><font color="#FFFFFF">Aksi</font></div></td>
           </tr>
           <tbody id="tableBody">
           {foreach $mkwu as $m}
           <tr id="tr{$m.ID_DOSEN_MKWU}">
             <td>{$m.NO}</td>
             <td>{$m.NM_PENGGUNA}</td>
             <td>{$m.NM_FAKULTAS}</td>
             <td>{$m.NM_PROGRAM_STUDI}</td>
             <input type="hidden" name="id_dosen" id="dosen{$m.ID_DOSEN_MKWU}" value="{$m.ID_DOSEN}" />
             <input type="hidden" name="id_fakultas" id="fakultas{$m.ID_DOSEN_MKWU}" value="{$m.ID_FAKULTAS}" />
             <input type="hidden" name="id_prodi" id="prodi{$m.ID_DOSEN_MKWU}" value="{$m.ID_PROGRAM_STUDI}" />
             <td align="center"><a href="javascript:editData('{$m.ID_DOSEN_MKWU}')">Edit</a></td>
           </tr>
           {/foreach}
           </tbody>
           <tfoot id="tableFoot" style="display:none">
          <tr>
            	<td><span id="no"></span></td>
                <td>
                <select name="editDosen" id="editDosen">
                        {foreach $dosen_pengguna as $dosen}
                            <option value="{$dosen.id_dosen}">{$dosen.nama_dosen}</option>
                         {/foreach}
     			</select>
                </td>
                <td>
                <select name="editFakultas" id="editFakultas">
                	<option value="">-- Fakultas --</option>
                        {foreach $fakultas as $fak}
                            <option value="{$fak.id_fakultas}">{$fak.nama_fakultas}</option>
                         {/foreach}
     			</select>
                </td>
                <td>
                <select name="editProdi" id="editProdi">
                       {foreach $prodi as $p}
                            <option value="{$p.ID_PROGRAM_STUDI}">{$p.NM_PROGRAM_STUDI}</option>
                         {/foreach}
     			</select>
                </td>
               <td nowrap>
               	<input type="hidden" name="id" id="id" />
                <input type="hidden" name="nomer" id="nomer" />
                <input type="submit" name="submit" value="Simpan" />
                <input type="reset" id="btnReset" value="Batal" onClick="cancelForm()" />
               </td>
            </tr>
          </tfoot>
    	</table>
        </form>
</div>
		
        <div class="panel" id="panel2" style="display: none">
       	<form id="finput" name="finput">
				  <table class="tb_frame" width="46%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td>Dosen</td>
					  <td>:</td>
					  <td>
                      <select name="dosen" id="dosen">
                            <option value="">-- Dosen --</option>
                        {foreach $dosen_pengguna as $dosen}
                            <option value="{$dosen.id_dosen}">{$dosen.nama_dosen}</option>
                         {/foreach}
                      </select>
                      </td>
					</tr>
					<tr>
					  <td>Fakultas</td>
					  <td>:</td>
					  <td>
                      <select name="fakultas" id="fakultas" disabled="disabled">
               		  <option value="">-- Fakultas --</option>
                        {foreach $fakultas as $fak}
                            <option value="{$fak.id_fakultas}">{$fak.nama_fakultas}</option>
                         {/foreach}
				      </select>
                      </td>
				    </tr>
                    <tr>
					  <td>Prodi</td>
					  <td>:</td>
					  <td>
                      <select name="prodi" id="prodi">
               		  	<option value="">-- Prodi --</option>
				      </select>
                      </td>
				    </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="Submit" value="Simpan">
                      <input type="reset" id="btnReset" value="Batal" onClick="clearForm()" /></td>
					</tr>
				  </table>
          </form>
		</div>