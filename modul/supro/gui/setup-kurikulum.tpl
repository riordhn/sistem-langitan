{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[3,1]],
		headers: {
            5: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">Master Kurikulum (Program Studi)</div>
	<div id="tabs">
		<div id="tab1" {if $smarty.get.action == '' || $smarty.get.action == 'tampil'}class="tab_sel"{else}class="tab"{/if} align="center" onclick="javascript: displayPanel('1');">Rincian</div>
		<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
		{*<div id="tab3" {if $smarty.get.action == 'tampil'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>*}
   	</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Nama Kurikulum</th>
			<th>Status</th>
			<th>No. SK. Kurikulum</th>
			<th>Tahun Kurikulum</th>
			<th>Masa Berlaku</th>
			<th class="noheader">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach item="list" from=$T_KUR}
		<tr>
			<td>{$list.NM_KURIKULUM}</td>
			<td>{$list.STATUS_AKTIF}</td>
			<td>{$list.NOMOR_SK_KURIKULUM}</td>
			<td><center>{$list.TAHUN_KURIKULUM}</center></td>
			<td>{$list.BERLAKU_MULAI} s/d {$list.BERLAKU_SAMPAI}</td>
			<td><center><a href="setup-kurikulum.php?action=tampil&id_kurikulum={$list.ID_KURIKULUM_PRODI}" onclick="displayPanel('3')">Update</a> | <a href="setup-kurikulum.php?action=del&id_kurikulum={$list.ID_KURIKULUM_PRODI}">Delete</a></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel2" style="display:{$disp2} ">
<p> </p>
<form action="setup-kurikulum.php" method="post" >
<input type="hidden" name="action" value="add" >
	<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>Nama Kurikulum</td>
		<td><center>:</center></td>
		<td>
			<select name="namakur">
				<option value=''>-- NAMA KUR --</option>
				{foreach item="kur" from=$NM_KUR}
				{html_options  values=$kur.ID_KURIKULUM output=$kur.NM_KURIKULUM}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Status Kurikulum</td>
		<td><center>:</center></td>
		<td>
			<select name="status_kur" id="status_kur">
				<option value="">-- Status --</option>
				<option value="1">Aktif</option>
				<option value="0">Non Aktif</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Tahun Kurikulum</td>
		<td><center>:</center></td>
		<td>
			<select name="thn_kurikulum" id="thn_kurikulum">
			<option value="">-- Tahun Kurikulum --</option>
			{for $i=$thn_skrg; $i>=($thn_skrg-15); $i--}
			<option value="{$i}">{$i}</option>
			{/for}
			</select>
		</td>
	</tr>
	<tr>
		<td>Berlaku Mulai</td>
		<td><center>:</center></td>
		<td><input type="Text" name="calawal" value="" id="calawal" maxlength="25" size="25"><input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calawal','ddmmmyyyy');" /></td>
	</tr>
	<tr>
		<td>Berlaku Sampai</td>
		<td><center>:</center></td>
		<td><input type="Text" name="calakhir" value="" id="calakhir" maxlength="25" size="25"><input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calakhir','ddmmmyyyy');" /></td>
	</tr>
	<tr>
		<td>No. SK Kurikulum</td>
		<td><center>:</center></td>
		<td><input type="text" name="sk_kur" id="nama_kurikulum3" size="40"/> Tulis SK Rektor-nya</td>
	</tr>
	</table>
<p><input type="submit" name="ttambah" value="Simpan" id="ttambah"></p>
</form>
</div>

<div class="panel" id="panel3" style="display: {$disp3} ">
<p> </p>
<form action="setup-kurikulum.php?action=update" method="post" >
	<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	{foreach item="datakur" from=$T_KUR}
	{if $datakur.ID_KURIKULUM_PRODI==$id_kur}
	<input type="hidden" name="id_kur" value="{$id_kur}" >
		<tr>
			<td>Nama Kurikulum</td>
			<td><center>:</center></td>
			<td>
				<select name="namakur">
				{foreach item="kur" from=$NM_KUR}
				{html_options  values=$kur.ID_KURIKULUM output=$kur.NM_KURIKULUM selected=$datakur.ID_KURIKULUM}
				{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Status Kurikulum</td>
			<td><center>:</center></td>
			<td>
				<select name="status_kur" id="status_kur">
				{html_options options=$myOptions selected=$datakur.STATUS}
				</select>
			</td>
		</tr>
		<tr>
		<td>Tahun Kurikulum</td>
		<td><center>:</center></td>
		<td>
		<select name="thn_kurikulum" id="thn_kurikulum" >
		<option value="{$datakur.TAHUN_KURIKULUM}">{$datakur.TAHUN_KURIKULUM}</option>
		{for $i=$thn_skrg; $i>=($thn_skrg-15); $i--}
		<option value="{$i}">{$i}</option>
		{/for}
		</select>
		</tr>
		<tr>
			<td>Berlaku Mulai</td>
			<td><center>:</center></td>
			<td><input type="Text" name="calawal1" value="{$datakur.BERLAKU_MULAI}" id="calawal1" maxlength="25" size="25"><input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calawal1','ddmmmyyyy');" /></td>
		</tr>
		<tr>
			<td>Berlaku Sampai</td>
			<td><center>:</center></td>
			<td><input type="Text" name="calakhir1" value="{$datakur.BERLAKU_SAMPAI}" id="calakhir1" maxlength="25" size="25"><input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calakhir1','ddmmmyyyy');" /></td>
		</tr>
		<tr>
			<td>No. SK Kurikulum</td>
			<td><center>:</center></td>
			<td><input type="text" name="sk_kur1" value="{$datakur.NOMOR_SK_KURIKULUM}" id="nama_kurikulum3" size="40"/> Tulis SK Rektor-nya</td>
		</tr>
		{/if}
		{/foreach}
	</table>
<p><input type="submit" name="tambah" value="Update"></p>
</form>
</div>
<script>$('form').validate();</script>

