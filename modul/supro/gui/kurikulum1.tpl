   
<div class="center_title_bar">Master Kurikulum</div>  

<div id="tabs">
        <div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
        <div id="tab2" class="tab" style="margin-left:1px;" align="center">Input</div>
    	</div>

<div class="tab_bdr"></div>
<div class="panel" id="panel1" style="display: block">    
   <table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr class="left_menu">
    <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Prodi</font></td>
    <td width="5%" bgcolor="#333333"><font color="#FFFFFF">id</font></td>
    <td width="18%" bgcolor="#333333"><font color="#FFFFFF">Nama </font><font color="#FFFFFF">kurikulum</font></td>
    <td width="14%" bgcolor="#333333"><font color="#FFFFFF">Status</font></td>
    <td width="25%" bgcolor="#333333"><font color="#FFFFFF">Keterangan</font></td>
  </tr>
  {foreach item="list" from=$T_KUR}
  <tr>
    <td >{$list.NM_POGRAM_STUDI}</td>
    <td >{$list.ID_KURIKULUM}</td>
    <td >{$list.NM_KURIKULUM}</td>
    <td >{$list.STATUS_KURIKULUM}</td>
    <td >&nbsp;</td>
  </tr>
  
     {foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
</table>
<br>
</div>

<div class="panel" id="panel2" style="display: none"><br>
				<form name="finput" id="finput" method="post">
		       	  <table class="tb_frame" width="80%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td width="20%">Nama Kurikulum</td>
					  <td width="2%">:</td>
					  <td width="78%"><input type="text" name="nama_kurikulum" id="nama_kurikulum" size="40" /></td>
					</tr>
                    <tr>
					  <td width="20%">Tahun Kurikulum</td>
					  <td width="2%">:</td>
					  <td width="78%">
                        <select name="thn_kurikulum" id="thn_kurikulum">
                             <option value="">-- Tahun Kurikulum --</option>
                        {for $i=$thn_skrg; $i>=($thn_skrg-15); $i--}
                            <option value="{$i}">{$i}</option>
                         {/for}
                         </select>					
                    </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="ttambah" value="Simpan" id="ttambah"></td>
					</tr>
		  		  </table>
		  		</form>
          <br>
        </div>   