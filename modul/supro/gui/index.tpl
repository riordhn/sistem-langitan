<html>
    <head>
        <title>AKADEMIK - Universitas Airlangga</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="../../css/reset.css" />
        <link rel="stylesheet" type="text/css" href="../../css/text.css" />
        <link rel="stylesheet" type="text/css" href="../../css/prodi_edit.css" />
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-1.8.11.custom.css" />
		<link rel="stylesheet" type="text/css" href="includes/sortable/themes/green/style.css" />
        <script type="text/javascript" src="../../js/jquery-1.5.1.js"></script>
        <script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
		<script type="text/javascript">var defaultRel = 'perkuliahan'; var defaultPage = 'master-perkuliahan.php'; var id_fakultas = null;</script>
        <script type="text/javascript" src="../../js/cybercampus.ajax-1.0.js"></script>
        <script type="text/javascript" src="gui/tab.js"></script>
		<script language="javascript" type="text/javascript" src="gui/datetimepicker.js"></script>
	</head>
    <body>
	  </div>
        <table class="clear-margin-bottom">
            <colgroup>
                <col />
                <col class="main-width"/>
                <col />
            </colgroup>
            <thead>
                <tr>
                    <td class="header-left"></td>
                  <td class="header-center"> 
			        <div align="right" >
				      <span style="font-size:32px">
					  {foreach $dataprodi as $prodi}{$prodi.NM_PRODI} {/foreach} </span>
					  <br>
				      <span style="font-size:16px">AKADEMIK - PRODI</span> 
					 </div>
				  </td>
					<td class="header-right"></td>
                </tr>
                <tr>
                    <td class="tab-left"></td>
                    <td class="tab-center">
                        <ul>
                        {foreach $modul_set as $m}
							{if $m.AKSES == 1}
                            <li><a href="#{$m.NM_MODUL}!{$m.PAGE}">{$m.TITLE}</a></li>
                            <li class="divider"></li>
							{/if}
                        {/foreach}
                            <li><a class="disable-ajax" href="../../logout.php">Logout</a></li>
							
                        </ul>
                    </td>
                    <td class="tab-right"></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="body-left">&nbsp;</td>
                    <td class="body-center">
                        <table class="content-table">
                            <colgroup>
                                <col />
                                <col />
                            </colgroup>
                            <tr>
                                <td colspan="2" id="breadcrumbs" class="breadcrumbs">Navigation : <a href="#">Tab</a> / <a href="#">Menu</a></td>
                            </tr>
                            <tr>
                                <td id="menu" class="menu">
                                    
                                </td>
                                <td id="content" class="content">Content</td>
                            </tr>
                        </table>
                    </td>
                    <td class="body-right">&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td class="foot-left">&nbsp;</td>
                    <td class="foot-center">
                        <div class="footer-nav">
                            <a href="#">Home</a> | <a href="#">About</a> | <a href="#">Sitemap</a> | <a href="#">RSS</a> | <a href="#">Contact Us</a>
                        </div>
                        <div class="footer">Copyright &copy; 2011 - Universitas Airlangga <br/>All Rights Reserved</div>
                    </td>
                    <td class="foot-right">&nbsp;</td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>