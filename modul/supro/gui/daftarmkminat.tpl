<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script type="text/javascript" src="jquery-1.2.1.pack.js"></script>
{literal}
<script type="text/javascript">

function lookup(inputString) {
if(inputString.length == 0) {
// Hide the suggestion box.
$('#suggestions').hide();
} else {
$.post("gp.php", {queryString: ""+inputString+""}, function(data){
if(data.length >0) {
$('#suggestions').show();
$('#autoSuggestionsList').html(data);
}
});
}
} // lookup

function fill1(thisValue) {
$('#kode_matkul').val(thisValue);
setTimeout("$('#suggestions').hide();", 200);
}
function fill2(thisValue) {
$('#nama_matkul').val(thisValue);
setTimeout("$('#suggestions').hide();", 200);
}
function fill3(thisValue) {
$('#nama_en_matkul').val(thisValue);
setTimeout("$('#suggestions').hide();", 200);
}
</script>
{/literal}
  
  <div class="center_title_bar">Daftar Mata Ajar Peminatan </div>  

		<div id="tabs">
		<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
		<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Update</div>
		<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Insert MA</div>
   	</div>

		
   <div class="tab_bdr"></div>

   <div class="panel" id="panel1" style="display: {$disp1}">  
   <form action="daftarmkminat.php" method="post">
 <input type="hidden" name="action" value="view" /> 
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
		    <tr>
             <td>Tahun Kurikulum</td>
             <td>:</td>
             <td>			 
                <select name="thkur">
	 		   {foreach item="kur" from=$T_KUR}
    		   {html_options  values=$kur.ID_KURIKULUM_PRODI output=$kur.NAMA selected=$IDKP}
	 		   {/foreach}
			   </select>
			   
		     </td> 
           </tr>
		    <tr>
             <td>Peminatan</td> 
			 <td>:</td>
			 <td>
                <select name="minat">
	 		   {foreach item="minat1" from=$T_MINAT}
    		   {html_options  values=$minat1.ID_PRODI_MINAT output=$minat1.NM_PRODI_MINAT selected=$IDM}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>
</table>
</form>	  
	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr class="left_menu" align="center">
             <td width="2%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
             <td width="6%" bgcolor="#333333"><font color="#FFFFFF">Kode MA</font></td>
             <td width="33%" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Ajar</font></td>
             <td width="6%" bgcolor="#333333"><font color="#FFFFFF">SKS TM</font></td>
			 <td width="6%" bgcolor="#333333"><font color="#FFFFFF">SKS PRAK</font></td>
			 <td width="6%" bgcolor="#333333"><font color="#FFFFFF">SKS TTL</font></td>
			 <td width="6%" bgcolor="#333333"><font color="#FFFFFF">Status</font></td>
			 <td width="6%" bgcolor="#333333"><font color="#FFFFFF">KLP</font></td>
			 <td width="6%" bgcolor="#333333"><font color="#FFFFFF">SMT</font></td>
             <td width="6%" bgcolor="#333333"><font color="#FFFFFF">Thn Kur</font></td>
             <td width="20%" bgcolor="#333333"><font color="#FFFFFF">Aksi</font></td>
           </tr>
           <tbody id="tableBody">
           {foreach name=tmk item="list" from=$T_MK}
             <td>{$smarty.foreach.tmk.iteration}</td>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td>{$list.KREDIT_TATAP_MUKA}</td>
			 <td>{$list.KREDIT_PRAKTIKUM}</td>
			 <td>{$list.KREDIT_SEMESTER}</td>
			 <td>{$list.NM_STATUS_MK}</td>
			 <td>{$list.NM_KELOMPOK_MK}</td>
			 <td>{$list.TINGKAT_SEMESTER}</td>
             <td>{$list.TAHUN_KURIKULUM}</td>
             <td align="center"><a href="daftarmkminat.php?action=viewup&id_km={$list.ID_KURIKULUM_MINAT}" onclick="displayPanel('2')">Update</a> | <a href="daftarmkminat.php?action=del&id_km={$list.ID_KURIKULUM_MINAT}">Delete</a>
             </td>
           </tr>
		   {foreachelse}
        <tr><td colspan="11"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
   	 </table>
   </div>
   
 <div class="panel" id="panel2" style="display: {$disp2}">
 <form action="daftarmkminat.php" method="post" >
 <input type="hidden" name="action" value="update1" />
 <input type="hidden" name="id_km" value="{$IDKM}" /> 
		          
				  <table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td colspan="3"><strong> PERUBAHAN STATUS MATA AJAR </strong></td>
				    </tr>
					{foreach item="ganti" from=$T_MK1}
				  
					<tr>
					  <td width="29%" >Kode Mata Ajar</td>
					  <td width="2%" >:</td>
					  <td width="69%" >{$ganti.KD_MATA_KULIAH}</td>
					</tr>
					<tr>
					  <td>Nama Mata Ajar</td>
					  <td>:</td>
					  <td>{$ganti.NM_MATA_KULIAH}</td>
					</tr>					
					<tr>
                      <td>Nama MA (English) </td>
                      <td>:</td>
                      <td>{$ganti.NM_MATA_KULIAH_EN}</td>
				    </tr>
					
					<tr>
					  <td>SKS TTL</td>
					  <td>:</td>
					  <td>{$ganti.KREDIT_SEMESTER}</td>
				    </tr>
					
					<tr>
					  <td>Tingkat Semester</td>
					  <td>:</td>
					  <td>{$ganti.TINGKAT_SEMESTER}</td>
				    </tr>
                    <tr>
                    	<td>Kelompok Mata Ajar</td>
                        <td>:</td>
                        <td>{$ganti.NM_KELOMPOK_MK}</td>
                    </tr>
                    <tr>
                    	<td>Minat Prodi</td>
                        <td>:</td>
                        <td>{$ganti.NM_PRODI_MINAT}</td>
                    </tr>
					<tr>
                    	<td>Tahun Kurikulum</td>
                        <td>:</td>
                        <td>{$ganti.TAHUN_KURIKULUM}</td>
                    </tr>
					<tr>
					  <td>Status</td>
					  <td>:</td>
					  <td>
                      	<select name="status_mk" id="status_mk">
                            {foreach item="list" from=$ST_MK}
    		   				{html_options  values=$list.ID_STATUS_MK output=$list.NM_STATUS_MK selected=$ganti.ID_STATUS_MK}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					{/foreach}
					
					<tr>
							<td colspan="3"><input type="submit" name="simpan" value="Simpan"></td>		  
					</tr>
					
		  		</table>
   </form>
   </div>
   
   <div class="panel" id="panel3" style="display: {$disp3}">
   <form action="daftarmkminat.php" method="post">
   <input type="hidden" name="action" value="setting" />
   <table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
					  <td>Tahun Kurikulum </td>
					  <td>:</td>
					  <td> 
                <select name="thkur1">
	 		   {foreach item="kur" from=$T_KUR}
    		   {html_options  values=$kur.ID_KURIKULUM_PRODI output=$kur.NAMA selected=$IDKURPRODI}
	 		   {/foreach}
			   </select>
					  </td>
					  
  </tr>
	
   <tr>
					  <td>Peminatan </td>
					  <td>:</td>
					  <td><select name="minatprodi">
	 		   {foreach item="minat1" from=$T_MINAT}
    		   {html_options  values=$minat1.ID_PRODI_MINAT output=$minat1.NM_PRODI_MINAT selected=$IDPRODIMINAT}
	 		   {/foreach}
			   </select></td>
					  
  </tr> 
  <tr>
					  <td>Status MA </td>
					  <td>:</td>
					  <td><select name="status_mk">
	 		   {foreach item="status" from=$T_STATUS_MK}
    		   {html_options  values=$status.ID_STATUS_MK output=$status.NM_STATUS_MK selected=$ID_STATUS_MK}
	 		   {/foreach}
			   </select></td>
					  <td><input type="submit" name="View" value="Setting"> </td>
  </tr> 
  </table> 
   </form>  
	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr class="left_menu" align="center">
             <td width="2%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
             <td width="6%" bgcolor="#333333"><font color="#FFFFFF">Kode MA</font></td>
             <td width="33%" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Ajar</font> </td>
			 <td width="4%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
			 <td width="6%" bgcolor="#333333"><font color="#FFFFFF">KLP</font></td>
			 <td width="4%" bgcolor="#333333"><font color="#FFFFFF">SMT</font></td>
             <td width="4%" bgcolor="#333333"><font color="#FFFFFF">Thn</font></td>
             <td width="20%" bgcolor="#333333"><font color="#FFFFFF">Aksi</font></td>
           </tr>
           <tbody id="tableBody">
           {foreach name="test" item="list" from=$INSERT_MK}
             <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td>{$list.KREDIT_SEMESTER}</td>
			 <td>{$list.NM_KELOMPOK_MK}</td>
			 <td>{$list.TINGKAT_SEMESTER}</td>
			 <td>{$list.TAHUN_KURIKULUM}</td>
             <td><a href="daftarmkminat.php?action=setting&st=1&thkur1={$IDKURPRODI}&minatprodi={$IDPRODIMINAT}&status_mk={$ID_STATUS_MK}&id_kurmk={$list.ID_KURIKULUM_MK}" onclick="displayPanel('3')">Pilih</a></td>
           </tr>
		   {foreachelse}
        <tr><td colspan="11"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
   	 </table>
	 <br>
	 <br>
   </div>
   
 <script>$('form').validate();</script>