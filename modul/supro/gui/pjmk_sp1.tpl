{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0], [2,0]],
		headers: {
            5: { sorter: false }
		}
		}
	);
}
);
	var i = $('input type="text"').size();

	$('#addButton').click(function() {
		 $('#tableText tr:last').after('<tr><td>Anggota </td>' +
		  '<td>:</td>' +
		  '<td>' +
          ' <a href="pjmk_sp1.php?action=searchdosen&pjmk=2&id_klsmk={$IDKEL}">Search</a>' +
          '</td></tr>').
			animate({ opacity: "show" }, "slow");
		i++;
	});

</script>
{/literal}

<div id="pjmk">
<div class="center_title_bar">PJMA Supro I</div>
<div id="tabs">
        <div id="tab1" {if $smarty.get.action == ''}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
        <div id="tab2" {if $smarty.get.action == 'addview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">ISI/EDIT</div>
		<div id="tab3" {if $smarty.get.action == 'searchdosen' && $smarty.get.pjmk !=''}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Search Dosen</div>
		<div id="tab4" {if $smarty.get.action == 'searchdosen' && $smarty.get.id_pengampu !=''}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('4');">Edit Dosen</div>
		<div id="tab5" {if $smarty.get.action == 'copy'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('5');">Copy Dosen</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<form action="pjmk_sp1.php" method="post">
<input type="hidden" name="action" value="tampil" >
<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>Tahun Ajaran : 
			<select name="thn_ajaran" id="thn_ajaran">
			<option value="">-- Tahun Ajaran --</option>
			{foreach item="smt" from=$T_ST}
			{html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$smtaktif}
			{/foreach}
			</select>
			<input type="submit" name="View" value="View">
		</td>
	</tr>
</table>
</form>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="10%">Kode</th>
			<th width="35%">Nama Mata Ajar</th>
			<th width="5%">KLS</th>
			<th width="5%">SKS</th>
			<th width="35%">Tim Pengajar</th>
			<th class="noheader" width="10%">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=test item="list" from=$PJMK}
		{if $list.ID_STATUS == "0"}
		<tr bgcolor="yellow">
		{else}
		<tr>
		{/if}
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.NAMA_KELAS}</center></td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
		{if $list.ID_STATUS == "0"}
			<td></td>
		{else}
			<td><br><ol>{$list.TIM}</li></ol></td>
		{/if}
		{if $list.ID_STATUS == "0"}
			<td><center><a href="pjmk_sp1.php?action=del&id_klsmk={$list.ID_KELAS_MK}">Hapus</a><br/><a href="pjmk_sp1.php?action=addview&id_klsmk={$list.ID_KELAS_MK}&st={$list.ID_STATUS}">Isi</a><br/><a href="pjmk_sp1.php?action=copy&id_klsmk={$list.ID_KELAS_MK}&id_kurmk={$list.ID_KURIKULUM_MK}&st={$list.ID_STATUS}">Copy</a></center></td>
		{else}
			<td><center><a href="pjmk_sp1.php?action=del&id_klsmk={$list.ID_KELAS_MK}">Hapus</a><br/><a href="pjmk_sp1.php?action=addview&id_klsmk={$list.ID_KELAS_MK}&st={$list.ID_STATUS}&pjma={$list.ADA}">Edit</a><center></td>
		{/if}
		</tr>
		{foreachelse}
		<tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>
</div>

        <div class="panel" id="panel2" style="display: {$disp2}">

	      <table width="100%" border="1">
            <tr>
              <td>
			  {foreach item="lihat" from=$T_PJMK}

              <table class="tb_frame" width="100%" border="1" cellspacing="0" cellpadding="0" id="tableText">
                  <tr>
                    <td width="149">Kode Mata Ajar</td>
                    <td width="9">:</td>
                    <td width="754">{$lihat.KD_MATA_KULIAH}</td>
                  </tr>
				  <tr>
                    <td width="149">Nama Mata Ajar</td>
                    <td width="9">:</td>
                    <td>{$lihat.NM_MATA_KULIAH}</td>
                  </tr>
				  <tr>
                    <td width="149">SKS Mata Ajar</td>
                    <td width="9">:</td>
                    <td>{$lihat.KREDIT_SEMESTER}</td>
                  </tr>
				  <tr>
                    <td width="149">Kelas Mata Ajar</td>
                    <td width="9">:</td>
                    <td>{$lihat.NAMA_KELAS}</td>
                  </tr>

				{if $ST==1}
				{foreach item="pgp" from=$PGP}
				{if $pgp.PJMK_PENGAMPU_MK==1}
                  <tr>
                    <td>PJMA </td>
                    <td>:</td>
                    <td>
                    <input name="nip_pjmk" id="nippjmk" type="text" class="required" size="20" maxlength="20" class="required" value="{$pgp.NIP_DOSEN}" /> - <input name="nm_pjmk" id="nmpjmk" type="text" class="required" size="40" maxlength="40" class="required" value="{$pgp.NM_PENGGUNA}" />
					<a href="pjmk_sp1.php?action=searchdosen&id_pengampu={$pgp.ID_PENGAMPU_MK}&status1=ganti&id_klsmk={$IDKEL}">Edit</a> | <a href="pjmk_sp1.php?action=hapusdosen&id_pengampu={$pgp.ID_PENGAMPU_MK}&status2=hapus&id_klsmk={$IDKEL}">Hapus</a>
                    </td>
                  </tr>
				{else}
                  <tr>
                    <td>Anggota </td>
                    <td>:</td>
                    <td>
                    <input name="nip_agt" id="nipagt" type="text" class="required" size="20" maxlength="20" class="required" value="{$pgp.NIP_DOSEN}" /> - <input name="nm_agt" id="nmagt" type="text" class="required" size="40" maxlength="40" class="required" value="{$pgp.NM_PENGGUNA}" />
					<a href="pjmk_sp1.php?action=searchdosen&id_pengampu={$pgp.ID_PENGAMPU_MK}&status1=ganti&id_klsmk={$IDKEL}">Edit</a> | <a href="pjmk_sp1.php?action=hapusdosen&id_pengampu={$pgp.ID_PENGAMPU_MK}&status2=hapus&id_klsmk={$IDKEL}">Hapus</a>
                    </td>
                  </tr>
				{/if}
				 {/foreach}
				 {else}
				  	<tr>
                    <td>PJMA </td>
                    <td>:</td>
                    <td>
                    <input name="nip_pjmk" id="nippjmk" type="text" class="required" size="20" maxlength="20" class="required" value="{$pgp.NIP_DOSEN}" /> - <input name="nm_pjmk" id="nmpjmk" type="text" class="required" size="40" maxlength="40" class="required" value="{$pgp.NM_PENGGUNA}" />
					<a href="pjmk_sp1.php?action=searchdosen&pjmk=1&id_klsmk={$IDKEL}">Search</a>
                    </td>
                  </tr>
				 {/if}
                </table>
				{if $ST==1}
                  <p>
					<input name="Kembali" type="button" value="Kembali" ONCLICK="window.location.href='#supro_1-pjmasp1!pjmk_sp1.php'"/>
					{if $PJ=='kosong'}
					<input name="nip_pjmk" id="nippjmk" type="button" value="Tambah PJMA" ONCLICK="window.location.href='#supro_1-pjmasp1!pjmk_sp1.php?action=searchdosen&pjmk=1&id_klsmk={$IDKEL}'"/>
					{else}
					<input type="button" name="addButton" id="addButton" value="Tambah Anggota" ONCLICK="window.location.href='#supro_1-pjmasp1!pjmk_sp1.php?action=searchdosen&pjmk=2&id_klsmk={$IDKEL}'"/>
					{/if}
					</p>
				{else}
                  <p>
					<input name="Kembali" type="button" value="Kembali" ONCLICK="window.location.href='#supro_1-pjmasp1!pjmk_sp1.php'"/>
					<input type="button" name="addButton" id="addButton" value="Tambah Anggota" ONCLICK="window.location.href='#supro_1-pjmasp1!pjmk_sp1.php?action=searchdosen&pjmk=2&id_klsmk={$IDKEL}'"/>
					</p>
				{/if}
				</td>
            </tr>
		{/foreach}
          </table>
        </div>

<div class="panel" id="panel3" style="display: {$disp3}">
<form action="pjmk_sp1.php?action=searchdosen&id_klsmk={$IDKEL}&pjmk={$ST_PJMK}" method="post" id="cari1">
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="121">Nama Dosen
    <td width="431">: <input name="namadosen" type="text" /> <input type="submit" name="cari" value="Cari Dosen">
	</td>
	</tr>
</table>
</form>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
			   <tr bgcolor="#000000">
			     <td width="2%"><font color="#FFFFFF">No</font></td>
				 <td width="20%"><font color="#FFFFFF">Photo Dosen</font></td>
				 <td width="8%"><font color="#FFFFFF">Kode Dosen</font></td>
				 <td width="45%"><font color="#FFFFFF">Nama Dosen</font></td>
				 <td width="20%"><font color="#FFFFFF">Prodi</font></td>
				 <td width="5%"><font color="#FFFFFF">Aksi</font></td>
			  </tr>
			  <tr>
			 {foreach name=test item="list" from=$DOSEN}
			     <td>{$smarty.foreach.test.iteration}</td>
				 <td><img src="{$list.FOTO_PENGGUNA}/{$list.NIP_DOSEN}.JPG" width="160px"/></td>
				 <td>{$list.NIP_DOSEN}</td>
				 <td>{$list.NM_PENGGUNA}</td>
				 <td>{$list.NM_PROGRAM_STUDI}</td>
				 <td><a href="pjmk_sp1.php?action=addview&id_dosen={$list.ID_DOSEN}&id_klsmk={$IDKEL}&pil={$ST_PJMK}">Pilih</a></td>
			</tr>
        {/foreach}
			</table>
</div>

<div class="panel" id="panel4" style="display: {$disp4}">
<form action="pjmk_sp1.php?action=searchdosen&id_pengampu={$ID_PENGAMPU}&status1={$STATUS1}&id_klsmk={$IDKEL}" method="post" id="cari">

<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="121">Nama Dosen
    <td width="431">: <input name="namadosen1" type="text" /> <input type="submit" name="cari" value="Cari">
	</td>
	</tr>
</table>
</form>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
			   <tr bgcolor="#000000">
			     <td width="2%"><font color="#FFFFFF">No</font></td>
				 <td width="20%"><font color="#FFFFFF">Photo Dosen</font></td>
				 <td width="8%"><font color="#FFFFFF">Kode Dosen</font></td>
				 <td width="45%"><font color="#FFFFFF">Nama Dosen</font></td>
				 <td width="20%"><font color="#FFFFFF">Prodi</font></td>
				 <td width="5%"><font color="#FFFFFF">Aksi (Ganti Dosen)</font></td>
			  </tr>
			  <tr>
			 {foreach name=test item="list" from=$DOSEN}
			     <td>{$smarty.foreach.test.iteration}</td>
				 <td><img src="{$list.FOTO_PENGGUNA}/{$list.NIP_DOSEN}.JPG" width="160px"/></td>
				 <td>{$list.NIP_DOSEN}</td>
				 <td>{$list.NM_PENGGUNA}</td>
				 <td>{$list.NM_PROGRAM_STUDI}</td>
				 <td><a href="pjmk_sp1.php?action=addview&id_dosen={$list.ID_DOSEN}&id_pengampu={$ID_PENGAMPU}&status1={$STATUS1}&id_klsmk={$IDKEL}">Pilih</a></td>
			</tr>
        {/foreach}
			</table>
</div>
<div class="panel" id="panel5" style="display: {$disp5}">
<form action="pjmk_sp1.php" method="post">
<input type="hidden" name="action" value="prosescp" >
<input type="hidden" name="kelas_mk" value="{$id_klsmk1}" >
 <table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="121">Copy Dari </td>
    <td width="431">:
      <select name="cp" id="cp">
        <option value="">-- Pilih Kelas --</option>
       		{foreach item="cp1" from=$T_CP}
    		   {html_options  values=$cp1.ID_KELAS_MK output=$cp1.NAMA}
	 		   {/foreach}
      </select>
    </td>

<td> <input type="submit" name="Proses" value="Proses"> </td>
           </tr>

</table>
</form>
</div>
</div>
{literal}
 <script>$('form').validate();</script>
{/literal}