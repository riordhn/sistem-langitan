<?php
// Author	: Yudi Sulistya
// Created	: 01/05/2011

// Db Configuration
$db_server      = 'localhost';
$db_port        = '1521';
$db_name        = 'XE';
$db_user        = 'fisip';
$db_password    = 'fisip';

// DB connection
$db_tnsc = '
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = '.$db_server.')(PORT = '.$db_port.'))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = '.$db_name.')
    )
  )';
@$conn = oci_connect($db_user, $db_password, $db_tnsc);
if (!$conn){
    $e = oci_error(); // For oci_connect errors do not pass a handle
	die('<pre>Error message:</br><>'.$e['message'].'</pre>');
}
?>