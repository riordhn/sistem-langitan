<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 
$smarty->assign('display','-- Periksa dan Pastikan Anda Yakin Melakukan GANTI Password --');

// Recode by Fathoni : 06-02-2012

if ($request_method == 'POST')
{
    if (post('action') == 'gantipwd')
    {
        $pwdlama=$_POST['pwdlama'];
		$pwdbaru=$_POST['pwdbaru'];
		$pwdbaru2=$_POST['pwdbaru2'];

		if($pwdbaru != $pwdbaru2) {
			$smarty->assign('display','Password Baru tidak sama');
		} else {
			$hasil = $user->ChangePassword($pwdlama, $pwdbaru);
			if($hasil == true) {
			$smarty->assign('display','Sukses Mengganti Password');
			} else {
			$smarty->assign('display','Cek Kembali Isian Anda !!!');
			}
		}
    }
    
    if (post('mode') == 'change-role')
    {
        $id_pengguna = post('id_pengguna');
        $id_role = post('id_role');
        
        $id_template_role = $db->QuerySingle("select id_template_role from role_pengguna where id_pengguna = {$id_pengguna} and id_role = {$id_role}");
        
        $db->Parse("update pengguna set id_role = :id_role, id_template_role = :id_template_role where id_pengguna = :id_pengguna");
        $db->BindByName(':id_role', $id_role);
        $db->BindByName(':id_template_role', $id_template_role);
        $db->BindByName(':id_pengguna', $id_pengguna);
        $result = $db->Execute();
        
        echo ($result) ? "1" : "0";
        
        exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    $smarty->assign('id_pengguna', $user->ID_PENGGUNA);
    $smarty->assign('id_role', $user->ID_ROLE);
    $smarty->assign('role_set', $db->QueryToArray("select * from role where id_role in (select id_role from role_pengguna where id_pengguna = {$user->ID_PENGGUNA})"));
}

$smarty->display('ganti-pwd.tpl');
?>
