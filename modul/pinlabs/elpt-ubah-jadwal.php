<?php

include('config.php');
include 'class/elpt.class.php';

$elpt = new elpt($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        if (isset($_POST)) {
            if (post('mode') == 'ubah') {
                $elpt->update_jadwal_elpt(post('id_c_mhs'), post('jadwal_lama'), post('jadwal_baru'));
            }
        }
        $smarty->assign('data_jadwal_cmhs', $elpt->get_data_jadwal_cmhs(get('no_ujian')));
    }
}
$smarty->assign('data_jadwal', $elpt->load_jadwal_elpt());
$smarty->display("elpt-ubah-jadwal.tpl");
?>
