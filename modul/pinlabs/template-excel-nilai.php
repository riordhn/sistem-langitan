<?php

include 'config.php';
include 'excel/OLEwriter.php';
include 'excel/BIFFwriter.php';
include 'excel/Worksheet.php';
include 'excel/Workbook.php';
include 'class/elpt.class.php';

$elpt = new elpt($db);

$data_cmhs = $elpt->load_data_elpt_penerimaan(get('penerimaan'), get('tanggal'));

//create 'header' function. if called, this will tell the browser that the file returned is an excel document

function HeaderingExcel($filename) {
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$filename");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");
}

// HTTP headers

HeaderingExcel('excel-nilai-elpt.xls'); //call the function above
// Creating a workbook instance
$workbook = new Workbook("-");

// woksheet 1
$worksheet1 = & $workbook->add_worksheet('Nilai');

$worksheet1->set_zoom(100); //75% zoom
$worksheet1->set_portrait();
$worksheet1->set_paper(9); //set A4
$worksheet1->hide_gridlines();  //hide gridlines

$worksheet1->write_string(0, 0, "NO UJIAN");
$worksheet1->write_string(0, 1, "NAMA");
$worksheet1->write_string(0, 2, "LISTENING");
$worksheet1->write_string(0, 3, "STRUCTURE");
$worksheet1->write_string(0, 4, "READING");

$index_baris = 1;
foreach ($data_cmhs as $data) {

    $worksheet1->write_string($index_baris, 0, $data['NO_UJIAN']);
    $worksheet1->write_string($index_baris, 1, $data['NM_C_MHS']);
    $worksheet1->write_string($index_baris, 2, $data['LISTENING']);
    $worksheet1->write_string($index_baris, 3, $data['STRUCTURE']);
    $worksheet1->write_string($index_baris, 4, $data['READING']);

    $index_baris++;
}
$workbook->close();
?>
