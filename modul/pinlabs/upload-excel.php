<?php

include 'config.php';
include 'excel/reader.php';

$data = new Spreadsheet_Excel_Reader();
$status_message = '';
$status_upload = '';

if (isset($_POST)) {
    if (post('mode') == 'read_excel') {
        $status_upload = 'False';
        $filename = get('penerimaan') . '_' . $user->ID_PENGGUNA . '_' . date('dmy_h:i:s') . '_' . $_FILES['file']['name'];
        $filename = str_replace(' ', '', $filename);
        if ($_FILES["file"]["error"] > 0) {
            $status_message .= "Error : " . $_FILES["file"]["error"] . "<br />";
        } else {
            $status_message .= "Nama File : " . $filename . "<br />";
            $status_message .= "Type : " . $_FILES["file"]["type"] . "<br />";
            $status_message .= "Size : " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
            if (file_exists("file/excel_input_nilai/" . $filename)) {
                $status_message .= 'Keterangan : ' . $filename . " Sudah Ada ";
            } else {
                move_uploaded_file($_FILES["file"]["tmp_name"], "file/excel_input_nilai/" . $filename);
                chmod("file/excel_input_nilai/" . $filename, 0777);
                $status_upload = 'True';
            }
        }

        $jumlah_insert = 0;
        if ($status_upload == 'True') {
            $data->setOutputEncoding('CP1251');
            $data->read("file/excel_input_nilai/{$filename}");
            for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
                $no_ujian = $data->sheets[0]['cells'][$i][1];
                $id_c_mhs = $db->QuerySingle("SELECT ID_C_MHS FROM CALON_MAHASISWA_BARU WHERE NO_UJIAN='{$no_ujian}'");
                $listening = str_replace(',', '.', $data->sheets[0]['cells'][$i][3]);
                $structure = str_replace(',', '.', $data->sheets[0]['cells'][$i][4]);
                $reading = str_replace(',', '.', $data->sheets[0]['cells'][$i][5]);
                if ($listening != '' && $structure != '' && $reading != '') {
                    $score = round((intval($listening) + intval($structure) + intval($reading)) * 10 / 3);
                    $db->Query("
                    UPDATE CALON_MAHASISWA_DATA
                        SET
                            ELPT_LISTENING='{$listening}',
                            ELPT_STRUCTURE='{$structure}',
                            ELPT_READING='{$reading}',
                            ELPT_SCORE='{$score}'
                    WHERE ID_C_MHS='{$id_c_mhs}'
                    ");

                    $jumlah_insert++;
                }
            }
        }
    }
}


$smarty->assign('jumlah_insert', $jumlah_insert);
$smarty->assign('status_upload', $status_upload);
$smarty->assign('status_message', $status_message);
$smarty->display('upload-excel.tpl');
?>
