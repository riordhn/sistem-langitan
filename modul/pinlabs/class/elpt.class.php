<?php

class elpt {

    public $db;

    function __construct($database) {
        $this->db = $database;
    }

    //menu jadwal elpt

    function load_jadwal_elpt() {
        return $this->db->QueryToArray("
            SELECT JT.*,RT.*,PEN.*,JAL.* FROM JADWAL_TOEFL JT
            JOIN RUANG_TOEFL RT ON JT.ID_RUANG_TOEFL = RT.ID_RUANG_TOEFL 
            LEFT JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=JT.ID_PENERIMAAN
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR=PEN.ID_JALUR
            WHERE TO_CHAR(JT.TGL_TEST,'YYYY')= TO_CHAR(SYSDATE, 'YYYY')
            ORDER BY TGL_TEST,JAM_MULAI,MENIT_MULAI,RT.NM_RUANG");
    }

    function get_jadwal_elpt($id_jawdal) {
        $this->db->Query("
            SELECT JT.*,RT.*,PEN.*,JAL.* 
            FROM JADWAL_TOEFL JT
            JOIN RUANG_TOEFL RT ON JT.ID_RUANG_TOEFL = RT.ID_RUANG_TOEFL 
            LEFT JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=JT.ID_PENERIMAAN
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR=PEN.ID_JALUR
            WHERE JT.ID_JADWAL_TOEFL='{$id_jawdal}'");
        return $this->db->FetchAssoc();
    }

    function tambah_jadwal_elpt($penerimaan, $ruang, $tgl_test, $jam_mulai, $menit_mulai) {
        $this->db->Query("
            INSERT INTO JADWAL_TOEFL
                (ID_PENERIMAAN,ID_RUANG_TOEFL,TGL_TEST,JAM_MULAI,MENIT_MULAI)
            VALUES
                ('{$penerimaan}','{$ruang}','{$tgl_test}','{$jam_mulai}','{$menit_mulai}')");
    }

    function edit_jadwal_elpt($id_jadwal, $penerimaan, $ruang, $tgl_test, $jam_mulai, $menit_mulai) {
        $this->db->Query("
            UPDATE JADWAL_TOEFL
            SET
                ID_RUANG_TOEFL='{$ruang}',
                ID_PENERIMAAN='{$penerimaan}',
                TGL_TEST='{$tgl_test}',
                JAM_MULAI='{$jam_mulai}',
                MENIT_MULAI='{$menit_mulai}'
            WHERE ID_JADWAL_TOEFL='{$id_jadwal}'
            ");
    }

    function delete_jadwal_elpt($id_jadwal_toefl) {
        $this->db->Query("DELETE JADWAL_TOEFL WHERE ID_JADWAL_TOEFL='$id_jadwal_toefl'");
    }

    function load_data_elpt_cmhs($id_jadwal_elpt) {
        return $this->db->QueryToArray("
            SELECT CMD.ELPT_KEHADIRAN,CM.ALAMAT,CM.TELP,CM.ID_C_MHS,UPPER(CM.NM_C_MHS) AS NM_C_MHS,CM.NO_UJIAN,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,
                UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI,CMD.ELPT_LISTENING,CMD.ELPT_STRUCTURE,CMD.ELPT_READING,CMD.ELPT_SCORE,J.NM_JENJANG
                FROM CALON_MAHASISWA_BARU CM
                JOIN CALON_MAHASISWA_DATA CMD ON CM.ID_C_MHS=CMD.ID_C_MHS
                JOIN PROGRAM_STUDI PR ON CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
                JOIN JENJANG J ON J.ID_JENJANG=PR.ID_JENJANG
                JOIN FAKULTAS F ON PR.ID_FAKULTAS= F.ID_FAKULTAS
            WHERE CMD.ID_JADWAL_TOEFL ='{$id_jadwal_elpt}'");
    }

    function load_data_elpt_penerimaan($penerimaan, $tanggal) {
        return $this->db->QueryToArray("
            SELECT CMD.ELPT_KEHADIRAN,CM.ALAMAT,CM.TELP,CM.ID_C_MHS,UPPER(CM.NM_C_MHS) AS NM_C_MHS,CM.NO_UJIAN,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,
                UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI,CMD.ELPT_LISTENING,CMD.ELPT_STRUCTURE,CMD.ELPT_READING,CMD.ELPT_SCORE,J.NM_JENJANG,
                JT.TGL_TEST,JT.JAM_MULAI,JT.MENIT_MULAI,RT.NM_RUANG
                FROM CALON_MAHASISWA_BARU CM
                JOIN CALON_MAHASISWA_DATA CMD ON CM.ID_C_MHS=CMD.ID_C_MHS
                JOIN JADWAL_TOEFL JT ON JT.ID_JADWAL_TOEFL=CMD.ID_JADWAL_TOEFL
                JOIN RUANG_TOEFL RT ON RT.ID_RUANG_TOEFL=JT.ID_RUANG_TOEFL
                JOIN PROGRAM_STUDI PR ON CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
                JOIN JENJANG J ON J.ID_JENJANG=PR.ID_JENJANG
                JOIN FAKULTAS F ON PR.ID_FAKULTAS= F.ID_FAKULTAS
            WHERE CM.ID_PENERIMAAN ='{$penerimaan}' AND JT.TGL_TEST='{$tanggal}'
            ORDER BY JT.TGL_TEST,JT.JAM_MULAI,RT.NM_RUANG,CM.NO_UJIAN");
    }

    function load_ruang_elpt() {
        return $this->db->QueryToArray("SELECT * FROM RUANG_TOEFL ORDER BY NM_RUANG");
    }

    //menu ruang elpt

    function get_ruang_elpt($id_ruang) {
        $this->db->Query("SELECT * FROM RUANG_TOEFL WHERE ID_RUANG_TOEFL='{$id_ruang}'");
        return $this->db->FetchAssoc();
    }

    function add_ruang_elpt($nama, $kapasitas) {
        $this->db->Query("
            INSERT INTO RUANG_TOEFL
                (NM_RUANG,KAPASITAS)
            VALUES 
                ('{$nama}','{$kapasitas}')");
    }

    function edit_ruang_elpt($id_ruang, $nama, $kapasitas) {
        $this->db->Query("
            UPDATE RUANG_TOEFL
            SET
                NM_RUANG='{$nama}',
                KAPASITAS='{$kapasitas}'
            WHERE ID_RUANG_TOEFL='{$id_ruang}'");
    }

    function delete_ruang_elpt($id_ruang) {
        $this->db->Query("DELETE RUANG_TOEFL WHERE ID_RUANG_TOEFL='{$id_ruang}'");
    }

    function load_tanggal_jadwal_elpt() {
        $data_tanggal_jadwal_elpt = array();
        $this->db->Query("SELECT DISTICT TGL_TEST FROM JADWAL_TOEFL ORDER BY ID_JADWAL_TOEFL");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_tanggal_jadwal_elpt, $temp);
        }
        return $data_tanggal_jadwal_elpt;
    }

    function load_laporan_tes_elpt($fakultas, $penerimaan) {
        if ($fakultas != '') {
            $query = "
                SELECT PS.NM_PROGRAM_STUDI,J.NM_JENJANG,CM.* FROM AUCC.PROGRAM_STUDI PS
                JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                JOIN 
                (
                SELECT PS.ID_PROGRAM_STUDI,
                SUM(
                    CASE WHEN CMD.ID_JADWAL_TOEFL IS NOT NULL
                    THEN 1
                    ELSE 0
                    END
                ) PESERTA,
                SUM(
                    CASE WHEN CMD.ELPT_KEHADIRAN=1
                    THEN 1
                    ELSE 0
                    END
                ) HADIR,
                SUM(
                    CASE WHEN CMD.ELPT_SCORE <= 400 AND CMD.ELPT_SCORE!=0
                    THEN 1
                    ELSE 0
                    END
                ) RANGE_1,
                SUM(
                    CASE WHEN CMD.ELPT_SCORE BETWEEN 401 AND 450
                    THEN 1
                    ELSE 0
                    END
                ) RANGE_2,
                SUM(
                    CASE WHEN CMD.ELPT_SCORE BETWEEN 451 AND 500
                    THEN 1
                    ELSE 0
                    END
                ) RANGE_3,
                SUM(
                    CASE WHEN CMD.ELPT_SCORE BETWEEN 501 AND 550
                    THEN 1
                    ELSE 0
                    END
                ) RANGE_4,
                SUM(
                    CASE WHEN CMD.ELPT_SCORE BETWEEN 551 AND 600
                    THEN 1
                    ELSE 0
                    END
                ) RANGE_5,
                SUM(
                    CASE WHEN CMD.ELPT_SCORE >600
                    THEN 1
                    ELSE 0
                    END
                ) RANGE_6
                FROM AUCC.CALON_MAHASISWA_BARU CMB
                JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS='{$fakultas}'
                GROUP BY PS.ID_PROGRAM_STUDI
                ) CM ON CM.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI";
        } else {
            $query = "
                SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,CM.* FROM AUCC.FAKULTAS F
                LEFT JOIN 
                (
                SELECT PS.ID_FAKULTAS,
                SUM(
                    CASE WHEN CMD.ID_JADWAL_TOEFL IS NOT NULL
                    THEN 1
                    ELSE 0
                    END
                ) PESERTA,
                SUM(
                    CASE WHEN CMD.ELPT_KEHADIRAN=1
                    THEN 1
                    ELSE 0
                    END
                ) HADIR,
                SUM(
                    CASE WHEN CMD.ELPT_SCORE <= 400 AND CMD.ELPT_SCORE!=0
                    THEN 1
                    ELSE 0
                    END
                ) RANGE_1,
                SUM(
                    CASE WHEN CMD.ELPT_SCORE BETWEEN 401 AND 450
                    THEN 1
                    ELSE 0
                    END
                ) RANGE_2,
                SUM(
                    CASE WHEN CMD.ELPT_SCORE BETWEEN 451 AND 500
                    THEN 1
                    ELSE 0
                    END
                ) RANGE_3,
                SUM(
                    CASE WHEN CMD.ELPT_SCORE BETWEEN 501 AND 550
                    THEN 1
                    ELSE 0
                    END
                ) RANGE_4,
                SUM(
                    CASE WHEN CMD.ELPT_SCORE BETWEEN 551 AND 600
                    THEN 1
                    ELSE 0
                    END
                ) RANGE_5,
                SUM(
                    CASE WHEN CMD.ELPT_SCORE >600
                    THEN 1
                    ELSE 0
                    END
                ) RANGE_6
                FROM AUCC.CALON_MAHASISWA_BARU CMB
                JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                WHERE CMB.ID_PENERIMAAN='{$penerimaan}'
                GROUP BY PS.ID_FAKULTAS
                ) CM ON CM.ID_FAKULTAS=F.ID_FAKULTAS
                ORDER BY F.ID_FAKULTAS";
        }
        return $this->db->QueryToArray($query);
    }

    function update_nilai_eltp($id_c_mhs, $list, $struc, $reading, $score) {
        $this->db->Query("
            UPDATE CALON_MAHASISWA_DATA
            SET 
                ELPT_LISTENING='{$list}',
                ELPT_STRUCTURE='{$struc}',
                ELPT_READING='{$reading}',
                ELPT_SCORE='{$score}' 
            WHERE ID_C_MHS='{$id_c_mhs}'");
    }

    function update_presensi_elpt($id_c_mhs, $absen) {
        $this->db->Query("UPDATE CALON_MAHASISWA_DATA SET ELPT_KEHADIRAN='{$absen}' WHERE ID_C_MHS='{$id_c_mhs}'");
    }

    function update_verifikator_nilai_elpt($id_c_mhs, $pengguna) {
        $this->db->Query("UPDATE CALON_MAHASISWA_BARU SET ID_VERIFIKATOR_ELPT='{$pengguna}' WHERE ID_C_MHS='{$id_c_mhs}'");
    }

    function update_tgl_verifikasi_elpt($id_c_mhs) {
        $this->db->Query("UPDATE CALON_MAHASISWA_BARU SET TGL_VERIFIKASI_ELPT=CURRENT_TIMESTAMP WHERE ID_C_MHS='{$id_c_mhs}'");
    }

    function load_data_hasil_elpt($kelompok, $hasil, $tanggal) {

        while ($temp = $this->db->FetchArray()) {
            array_push($data_hasil_tes, $temp);
        }
        return $data_hasil_tes;
    }

    function load_data_view_elpt() {
        $data_view_elpt = array();
        $this->db->Query("SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,COUNT(CM.ID_C_MHS) AS JUMLAH_PESERTA,
        SUM(CASE WHEN CM.ELPT_KEHADIRAN=1 THEN 1 ELSE 0 END) AS PESERTA_HADIR 
        FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR, FAKULTAS F
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL IS NOT NULL
        GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
        ORDER BY F.ID_FAKULTAS
        ");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_view_elpt, $temp);
        }
        return $data_view_elpt;
    }

    function get_elpt_score_more_than($id_fakultas, $range) {
        $this->db->Query("SELECT COUNT(CM.ID_C_MHS) as JUMLAH FROM CALON_MAHASISWA CM, PROGRAM_STUDI PR,FAKULTAS F
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL IS NOT NULL AND CM.ELPT_KEHADIRAN =1 AND CM.ELPT_SCORE > '$range' AND F.ID_FAKULTAS ='$id_fakultas'");
        return $this->db->FetchArray();
    }

    function get_elpt_score_between($id_fakultas, $range1, $range2) {
        $this->db->Query("SELECT COUNT(CM.ID_C_MHS) as JUMLAH FROM CALON_MAHASISWA CM, PROGRAM_STUDI PR,FAKULTAS F
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL IS NOT NULL AND CM.ELPT_KEHADIRAN =1 AND CM.ELPT_SCORE BETWEEN '$range1' AND '$range2' AND F.ID_FAKULTAS ='$id_fakultas'");
        return $this->db->FetchArray();
    }

    function get_elpt_score_less_than($id_fakultas, $range) {
        $this->db->Query("SELECT COUNT(CM.ID_C_MHS) as JUMLAH FROM CALON_MAHASISWA CM, PROGRAM_STUDI PR,FAKULTAS F
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL IS NOT NULL AND CM.ELPT_KEHADIRAN =1 AND CM.ELPT_SCORE < '$range' AND F.ID_FAKULTAS ='$id_fakultas'");
        return $this->db->FetchArray();
    }

    // Menu Laporan Verifikasi 

    function load_verifikasi_data($fakultas, $penerimaan) {
        if ($fakultas != '') {
            return $this->db->QueryToArray("
                SELECT ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                    ) DITERIMA,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMB.TGL_REGMABA IS NOT NULL
                    ) REGMABA,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMD.ID_JADWAL_TOEFL IS NOT NULL
                    ) SUDAH_JADWAL,
                    (
                        SELECT COUNT(*) FROM AUCC.CALON_MAHASISWA_BARU CMB
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI AND CMB.TGL_VERIFIKASI_ELPT IS NOT NULL
                    ) SUDAH_TES
                FROM AUCC.PROGRAM_STUDI PS 
                JOIN AUCC.JENJANG J ON PS.ID_JENJANG=J.ID_JENJANG
                WHERE PS.ID_FAKULTAS='{$fakultas}'
                GROUP BY ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
        } else {
            return $this->db->QueryToArray("
                SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,
                    (
                        SELECT COUNT(*) FROM CALON_MAHASISWA_BARU CMB
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS
                    ) DITERIMA,
                    (
                        SELECT COUNT(*) FROM CALON_MAHASISWA_BARU CMB
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMB.TGL_REGMABA IS NOT NULL
                    ) REGMABA,
                    (
                        SELECT COUNT(*) FROM CALON_MAHASISWA_BARU CMB
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMB.TGL_VERIFIKASI_ELPT IS NOT NULL
                    ) SUDAH_TES,
                    (
                        SELECT COUNT(*) FROM CALON_MAHASISWA_BARU CMB
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                        JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                        WHERE ID_PENERIMAAN='{$penerimaan}' AND PS.ID_FAKULTAS=F.ID_FAKULTAS AND CMD.ID_JADWAL_TOEFL IS NOT NULL
                    ) SUDAH_JADWAL
                FROM FAKULTAS F
                GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS");
        }
    }

    //menu ubah jadwal

    function get_data_jadwal_cmhs($no_ujian) {
        $this->db->Query("
            SELECT CMB.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JT.ID_JADWAL_TOEFL,JT.JAM_MULAI JAM_ELPT,JT.MENIT_MULAI MENIT_ELPT,JT.TGL_TEST TGL_ELPT,JK.*,RT.NM_RUANG
            FROM CALON_MAHASISWA_BARU CMB
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
            JOIN JADWAL_TOEFL JT ON JT.ID_JADWAL_TOEFL=CMD.ID_JADWAL_TOEFL
            JOIN RUANG_TOEFL RT ON RT.ID_RUANG_TOEFL=JT.ID_RUANG_TOEFL
            JOIN JADWAL_KESEHATAN JK ON JK.ID_JADWAL_KESEHATAN=CMD.ID_JADWAL_KESEHATAN
            WHERE CMB.NO_UJIAN='{$no_ujian}'
            ");
        return $this->db->FetchAssoc();
    }

    function update_jadwal_elpt($id_c_mhs, $jadwal_lama, $jadwal_baru) {
        $this->db->Query("
            UPDATE JADWAL_TOEFL
            SET
                TERISI=TERISI-1
            WHERE ID_JADWAL_TOEFL='{$jadwal_lama}'");
        $this->db->Query("
            UPDATE CALON_MAHASISWA_DATA
            SET
                ID_JADWAL_TOEFL='{$jadwal_baru}'
            WHERE ID_C_MHS='{$id_c_mhs}'");
        $this->db->Query("
            UPDATE JADWAL_TOEFL
            SET
                TERISI=TERISI+1
            WHERE ID_JADWAL_TOEFL='{$jadwal_baru}'");
    }

}

?>
