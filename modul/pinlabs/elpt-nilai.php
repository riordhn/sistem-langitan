<?php

include 'config.php';
include '../ppmb/class/Penerimaan.class.php';
include 'class/elpt.class.php';


$elpt = new elpt($db);
$penerimaan = new Penerimaan($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $pen = get('penerimaan');
        if ($pen != '') {
            if (isset($_POST)) {
                if (post('mode') == 'save') {
                    for ($i = 1; $i <= post('jumlah_data'); $i++) {
                        $elpt->update_nilai_eltp(post('id_c_mhs' . $i), post('listening' . $i), post('structure' . $i), post('reading' . $i), round((intval(post('listening' . $i)) + intval(post('structure' . $i)) + intval(post('reading' . $i))) * 10 / 3));
                        $elpt->update_verifikator_nilai_elpt(post('id_c_mhs' . $i), $user->ID_PENGGUNA);
                    }
                }
            }
            $smarty->assign('data_tanggal', $db->QueryToArray("SELECT DISTINCT(TGL_TEST) TGL FROM JADWAL_TOEFL WHERE ID_PENERIMAAN='{$pen}' ORDER BY TGL"));
            $smarty->assign('data_cmhs', $elpt->load_data_elpt_penerimaan(get('penerimaan'), get('tanggal')));
        }
    }
}
$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
$smarty->assign('data_jadwal_elpt', $elpt->load_jadwal_elpt());
$smarty->display("elpt-nilai.tpl");
?>
