<?php

include('config.php');
include('class/elpt.class.php');
include('../ppmb/class/Penerimaan.class.php');


$elpt = new elpt($db);
$pen = new Penerimaan($db);

$mode = get('mode', 'view');


if (isset($_POST)) {
    if (post('mode') == 'add') {
        $elpt->tambah_jadwal_elpt(post('penerimaan'), post('ruang'), date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year'))), post('mulai_Hour'), post('mulai_Minute'));
    } else if (post('mode') == 'edit') {
        $elpt->edit_jadwal_elpt(post('id_jadwal_toefl'), post('penerimaan'), post('ruang'), date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year'))), post('mulai_Hour'), post('mulai_Minute'));
    } else if (post('mode') == 'delete') {
        $elpt->delete_jadwal_elpt(post('id_jadwal_toefl'));
    }
}
if ($mode == 'add') {
    $smarty->assign('data_ruang', $elpt->load_ruang_elpt());
    $smarty->assign('penerimaan_set', $pen->GetAllForView());
} elseif ($mode == 'edit') {
    $smarty->assign('data_ruang', $elpt->load_ruang_elpt());
    $smarty->assign('penerimaan_set', $pen->GetAllForView());
    $smarty->assign('jadwal_elpt', $elpt->get_jadwal_elpt(get('id')));
} else if ($mode == 'delete') {
    $smarty->assign('jadwal_elpt', $elpt->get_jadwal_elpt(get('id')));
} else if ($mode == 'data_cm') {
    $smarty->assign('jadwal_elpt', $elpt->get_jadwal_elpt(get('id')));
    $smarty->assign('data_calon_mahasiswa', $elpt->load_data_elpt_cmhs(get('id')));
} else {
    $smarty->assign('data_jadwal_elpt', $elpt->load_jadwal_elpt());
}
$smarty->display("elpt-jadwal-{$mode}.tpl");
?>
