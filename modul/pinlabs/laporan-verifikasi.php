<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';
include '../ppmb/class/Penerimaan.class.php';
include 'class/elpt.class.php';


$list = new list_data($db);
$penerimaan = new Penerimaan($db);
$elpt = new elpt($db);

if (isset($_GET)) {
    if (get('mode') == 'laporan') {
        $smarty->assign('data_laporan', $elpt->load_verifikasi_data(get('fakultas'), get('penerimaan')));
    }
}

$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
$smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->display('laporan-verifikasi.tpl');
?>
