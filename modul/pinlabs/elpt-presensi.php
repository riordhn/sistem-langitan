<?php

include('config.php');
include 'class/elpt.class.php';

$elpt = new elpt($db);
if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        if (get('jadwal') != '') {
            if (isset($_POST)) {
                if (post('mode') == 'save') {
                    for ($i = 1; $i <= post('jumlah_data'); $i++) {
                        $absen = post('absen' . $i) == '' ? 0 : 1;
                        $elpt->update_presensi_elpt(post('id_c_mhs' . $i), $absen);
                        $elpt->update_tgl_verifikasi_elpt(post('id_c_mhs' . $i));
                    }
                }
            }
            $smarty->assign('data_cmhs', $elpt->load_data_elpt_cmhs(get('jadwal')));
        }
    }
}
$smarty->assign('data_jadwal_elpt', $elpt->load_jadwal_elpt());
$smarty->display("elpt-presensi.tpl");
?>
