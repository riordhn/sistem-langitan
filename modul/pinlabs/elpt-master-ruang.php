<?php

include('config.php');
include('class/elpt.class.php');

$mode = get('mode', 'view');

$elpt = new elpt($db);

if (isset($_POST)) {
    if (post('mode') == 'add') {
        $elpt->add_ruang_elpt(post('nama'), post('kapasitas'));
    } else if (post('mode') == 'edit') {
        $elpt->edit_ruang_elpt(post('id_ruang'), post('nama'), post('kapasitas'));
    } else if (post('mode') == 'delete') {
        $elpt->delete_ruang_elpt(post('id_ruang'));
    }
}

if ($mode == 'view') {
    $smarty->assign('data_ruang_elpt', $elpt->load_ruang_elpt());
} else if ($mode == 'edit' || $mode == 'delete') {
    $smarty->assign('ruang_elpt', $elpt->get_ruang_elpt(get('id_ruang')));
}

$smarty->display("elpt-ruang-{$mode}.tpl");
?>
