<div class="center_title_bar">Jadwal Tes ELPT - Hapus Jadwal</div>

<form action="elpt-jadwal.php" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="id_jadwal_toefl" value="{$jadwal_elpt.ID_JADWAL_TOEFL}" />
    <table>
        <tr>
            <th colspan="2">Hapus Jadwal ELPT</th>
        </tr>
        <tr>
            <td>Penerimaan</td>
            <td>Gelombang {$jadwal_elpt.GELOMBANG} {$jadwal_elpt.NM_PENERIMAAN} <br/>Jalur ({$jadwal_elpt.NM_JALUR})</td>
        </tr>
        <tr>
            <td>Ruang</td>
            <td>{$jadwal_elpt.NM_RUANG}</td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>
                {$jadwal_elpt.TGL_TEST}
            </td>
        </tr>
        <tr>
            <td>Jam</td>
            <td>
                {$jadwal_elpt.JAM_MULAI} : {$jadwal_elpt.MENIT_MULAI}
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <a class="button" href="elpt-jadwal.php">Batal</a>
                <input class="button" type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>