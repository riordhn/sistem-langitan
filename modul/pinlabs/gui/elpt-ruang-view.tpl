<div class="center_title_bar">Master Ruang ELPT</div>

<table>
    <tr>
        <th>NO</th>
        <th>Nama Ruang</th>
        <th>Kapasitas</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_ruang_elpt as $data}
    <tr>
        <td>{$data@index+1}</td>
        <td>{$data.NM_RUANG}</td>
        <td>{$data.KAPASITAS}</td>
        <td>
            <a class="button" href="elpt-master-ruang.php?mode=edit&id_ruang={$data.ID_RUANG_TOEFL}">Edit</a>
            <a class="button" href="elpt-master-ruang.php?mode=delete&id_ruang={$data.ID_RUANG_TOEFL}">Hapus</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="4" style="text-align: center">
            <a class="button" href="elpt-master-ruang.php?mode=add">Tambah</a>
        </td>
    </tr>
</table>