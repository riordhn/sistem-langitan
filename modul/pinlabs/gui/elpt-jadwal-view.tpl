<div class="center_title_bar">Jadwal Tes ELPT</div>

<table style="width: 100%">
    <tr>
        <th class="center" style="width: 5%">No</th>
        <th>Penerimaan</th>
        <th>Nama Ruang</th>
        <th>Kapasitas</th>
        <th>Jadwal</th>
        <th>Terisi</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_jadwal_elpt as $jd}
        <tr>
            <td class="center">{$jd@index+1}</td>
            <td>{if $jd.NM_PENERIMAAN!=''}Gelombang {$jd.GELOMBANG} {$jd.NM_PENERIMAAN} <br/>Jalur ({$jd.NM_JALUR}){/if}</td>
            <td>{$jd.NM_RUANG}</td>
            <td>{$jd.KAPASITAS}</td>
            <td>{$jd.TGL_TEST} [{$jd.JAM_MULAI|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$jd.MENIT_MULAI|str_pad:2:"0"}]</td>
            <td>{$jd.TERISI}</td>
            <td class="center">
                <a class="button" href="elpt-jadwal.php?mode=edit&id={$jd.ID_JADWAL_TOEFL}">Edit</a>
                <a class="button" href="elpt-jadwal.php?mode=delete&id={$jd.ID_JADWAL_TOEFL}">Hapus</a>
                <a class="button" href="elpt-jadwal.php?mode=data_cm&id={$jd.ID_JADWAL_TOEFL}">View Data</a>
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="7" class="data-kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="8" class="center">
            <a class="button" href="elpt-jadwal.php?mode=add">Tambah</a>
        </td>
    </tr>
</table>