<div class="center_title_bar">Input Presensi ELPT </div>
<form method="get" action="elpt-presensi.php">
    <table style="width: 90%">
        <tr>
            <th colspan="2" class="center">Parameter</th>
        </tr>
        <tr>
            <td><label>Kelompok Jadwal ELPT</label></td>
            <td>
                <select name="jadwal">
                    {foreach $data_jadwal_elpt as $jadwal}
                        <option value="{$jadwal.ID_JADWAL_TOEFL}" {if $jadwal.ID_JADWAL_TOEFL==$smarty.get.jadwal}selected="true"{/if}>{$jadwal.NM_PENERIMAAN} {$jadwal.TGL_TEST} {$jadwal.NM_RUANG} ({$jadwal.JAM_MULAI} : {$jadwal.MENIT_MULAI})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="tampil"/>
                <input class="button" type="submit" value="Tampil"/>
            </td>
        </tr>
    </table>
</form>
{if isset($data_cmhs)}
    <form action="elpt-presensi.php?{$smarty.server.QUERY_STRING}" method="post">
        <table style="width: 98%">
            <tr>
                <th>No</th>
                <th>No Ujian</th>
                <th>Nama</th>
                <th>Fakultas</th>
                <th>Program Studi</th>
                <th style="width: 20%">Presensi</th>
            </tr>
            {$index=1}
            {foreach $data_cmhs as $cmhs}
                <tr>
                    <td>{$cmhs@index+1}</td>
                    <td>{$cmhs.NO_UJIAN}</td>
                    <td>{$cmhs.NM_C_MHS}</td>
                    <td>{$cmhs.NM_FAKULTAS}</td>
                    <td>{$cmhs.NM_JENJANG} {$cmhs.NM_PROGRAM_STUDI}</td>
                    <td>
                        <input type="checkbox" name="absen{$cmhs@index+1}" value="1" {if $cmhs.ELPT_KEHADIRAN=='1'}checked="true"{/if}/>
                        <label for="absen{$cmhs@index+1}">
                            {if $cmhs.ELPT_KEHADIRAN=='1'}
                                Hadir
                            {else if $cmhs.ELPT_KEHADIRAN=='0'}
                                Belum/Tidak Hadir
                            {else}
                                Kosong
                            {/if}
                        </label>
                        <input type="hidden" name="id_c_mhs{$cmhs@index+1}" value="{$cmhs.ID_C_MHS}"/>
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="6" class="data-kosong">Data Kosong</td>
                </tr>
            {/foreach}
            {if count($data_cmhs)!=0}
                <tr>
                    <td colspan="6" class="center">
                        <input type="hidden" name="jumlah_data" value="{count($data_cmhs)}"/>
                        <input type="hidden" name="mode" value="save"/>
                        <input class="button" type="submit" value="Simpan" />
                        <a class="button disable-ajax" target="_blank" href="cetak-presensi-elpt.php?jadwal={$smarty.get.jadwal}">Cetak Presensi</a>
                    </td>
                </tr>
            {/if}
        </table>
    </form>
{/if}