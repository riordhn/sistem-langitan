<div class="center_title_bar">Jadwal Tes ELPT - Edit</div>

<form action="elpt-jadwal.php" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_jadwal_toefl" value="{$jadwal_elpt.ID_JADWAL_TOEFL}" />
    <table>
        <tr>
            <th colspan="2">Edit Jadwal ELPT</th>
        </tr>
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="penerimaan">
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                {if ($p2.ID_JENJANG==1||$p2.ID_JENJANG==5)}
                                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $jadwal_elpt.ID_PENERIMAAN}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                                {/if}
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Ruang</td>
            <td><select name="ruang">
                    {foreach $data_ruang as $dr}
                        <option value="{$dr.ID_RUANG_TOEFL}" {if $dr.ID_RUANG_TOEFL==$jadwal_elpt.ID_RUANG_TOEFL}selected="true"{/if}>{$dr.NM_RUANG}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>
                {html_select_date prefix="tgl_test_" field_order="DMY" time=$jadwal_elpt.TGL_TEST}
            </td>
        </tr>
        <tr>
            <td>Jam</td>
            <td>
                {html_select_time prefix="mulai_" display_seconds=false use_24_hours=true minute_interval=5 time=mktime($jadwal_elpt.JAM_MULAI, $jadwal_elpt.MENIT_MULAI)}
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <a class="button" href="elpt-jadwal.php">Batal</a>
                <input class="button" type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>