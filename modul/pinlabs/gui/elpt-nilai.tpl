<div class="center_title_bar">Input Nilai ELPT - Nilai Mahasiswa</div>
<form method="get" action="elpt-nilai.php">
    <table style="width: 90%">
        <tr>
            <th colspan="4" class="center">Parameter</th>
        </tr>
        <tr>
            <td>Penerimaan</td>
            <td>
                <select id="penerimaan" name="penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                {if ($p2.ID_JENJANG==1||$p2.ID_JENJANG==5)}
                                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                                {/if}
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
            <td>Tanggal</td>
            <td>
                <select id="tanggal" name="tanggal">
                    <option value="">Pilih Tanggal</option>
                    {foreach $data_tanggal as $t}
                        <option value="{$t.TGL}" {if $smarty.get.tanggal==$t.TGL}selected="true"{/if}>{$t.TGL}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr id="upload" style="display: none">
            <td colspan="4">
                <iframe src="upload-excel.php?penerimaan={$smarty.get.penerimaan}" width="100%" height="100px"></iframe>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="center">
                <input type="hidden" name="mode" value="tampil"/>
                <input class="button" type="submit" value="Tampil"/>
                {if $smarty.get.mode=='tampil'}
                    <a class="disable-ajax button" style="cursor: pointer" onclick="$('#upload').show()">Upload Excel</a>
                    <a class="disable-ajax button" style="cursor: pointer" href="template-excel-nilai.php?penerimaan={$smarty.get.penerimaan}&tanggal={$smarty.get.tanggal}">Download Template</a>
                    <a class="disable-ajax button" style="cursor: pointer" onclick="window.location.reload()">Reload</a>
                {/if}
            </td>
        </tr>
    </table>
</form>
{if isset($data_cmhs)}
    <form action="elpt-nilai.php?{$smarty.server.QUERY_STRING}" method="post">
        <table>
            <tr>
                <th>No</th>
                <th>No Ujian</th>
                <th>Nama</th>
                <th>Fakultas</th>
                <th>Program Studi</th>
                <th>Jadwal</th>
                <th>Listening</th>
                <th>Structure</th>
                <th>Reading</th>
                <th>Score</th>
            </tr>
            {foreach $data_cmhs as $cmhs}
                <tr>
                    <td>{$cmhs@index+1}</td>
                    <td>{$cmhs.NO_UJIAN}</td>
                    <td>{$cmhs.NM_C_MHS}</td>
                    <td>{$cmhs.NM_FAKULTAS}</td>
                    <td>{$cmhs.NM_PROGRAM_STUDI}</td>
                    <td>{$cmhs.TGL_TEST}<br/> {$cmhs.NM_RUANG}<br/> [{$cmhs.JAM_MULAI|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$cmhs.MENIT_MULAI|str_pad:2:"0"}]</td>
                    <td><input size="5" type="text"  name="listening{$cmhs@index+1}" value="{if $cmhs.ELPT_LISTENING!=0}{$cmhs.ELPT_LISTENING}{else}0{/if}"/></td>
                    <td><input size="5" type="text"  name="structure{$cmhs@index+1}" value="{if $cmhs.ELPT_STRUCTURE!=0}{$cmhs.ELPT_STRUCTURE}{else}0{/if}"/></td>
                    <td><input size="5" type="text"  name="reading{$cmhs@index+1}" value="{if $cmhs.ELPT_READING!=0}{$cmhs.ELPT_READING}{else}0{/if}"/></td>
                    <td>
                        <input size="5" type="text"  readonly="true" value="{if $cmhs.ELPT_SCORE!=0}{$cmhs.ELPT_SCORE}{else}0{/if}"/>
                        <input type="hidden" name="id_c_mhs{$cmhs@index+1}" value="{$cmhs.ID_C_MHS}"/>
                    </td>

                </tr>
            {foreachelse}
                <tr>
                    <td colspan="10" class="data-kosong">Data Kosong</td>
                </tr>
            {/foreach}
            {if count($data_cmhs)!=0}
                <tr>
                    <td colspan="10" class="center">
                        <input type="hidden" name="jumlah_data" value="{count($data_cmhs)}"/>
                        <input type="hidden" name="mode" value="save"/>
                        <input class="button" type="submit" value="Simpan" />
                    </td>
                </tr>
            {/if}
        </table>
    </form>
{/if}
{literal}
    <script type="text/javascript">
        $('#penerimaan').change(function(){
            $.ajax({
                    type:'post',
                    url:'getTanggal.php',
                    data:'penerimaan='+$(this).val(),
                    success:function(data){
                            $('#tanggal').html(data);
                    }                    
                })
        });
    </script>
{/literal}