<div class="center_title_bar">Daftar Calon Mahasiswa {$jadwal_elpt.NM_RUANG} Jadwal {$jadwal_elpt.TGL_TEST} JAM {$jadwal_elpt.JAM_MULAI|str_pad:2:0:$smarty.const.STR_PAD_LEFT}:{$jadwal_elpt.JAM_SELESAI|str_pad:2:0:$smarty.const.STR_PAD_RIGHT}</div>

<table>
    <tr>
        <th>Nomer</th>
        <th>Nama</th>
        <th>Nomer Ujian</th>
        <th>Alamat</th>
        <th>Telp</th>
        <th>Fakultas</th>
        <th>Program Studi</th>
    </tr>
    {foreach $data_calon_mahasiswa as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data.NM_C_MHS}</td>
            <td>{$data.NO_UJIAN}</td>
            <td>{$data.ALAMAT}</td>
            <td>{$data.TELP}</td>
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_JENJANG} {$data.NM_PROGRAM_STUDI}</td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="7" class="data-kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}

</table>

<a class="button" href="elpt-jadwal.php">Kembali</a>