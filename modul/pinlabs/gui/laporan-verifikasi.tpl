<div class="center_title_bar">Laporan Verifikasi Data</div> 
<form method="get" id="report_form" action="laporan-verifikasi.php">
    <table style="width:98%;">
        <tr>
            <th colspan="4" class="center">Parameter</th>
        </tr>
        <tr>
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Penerimaan</td>
            <td>
                <select name="penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                {if ($p2.ID_JENJANG==1||$p2.ID_JENJANG==5)}
                                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                                {/if}
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="6" class="center">
                <input type="hidden" name="mode" value="laporan" />
                <input type="submit" name="submit" class="button" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_laporan)}
    <table style="width: 98%">
        <tr>
            <th colspan="6" class="center">Laporan Lapor Diri Calon Mahasiswa</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>
                {if $smarty.get.fakultas!=''}
                    PROGRAM STUDI
                {else}
                    FAKULTAS
                {/if}
            </th>
            <th>JUMLAH DITERIMA</th>
            <th>JUMLAH ISI REGMABA</th>
            <th>SUDAH PUNYA JADWAL</th>
            <th>SUDAH IKUT TES</th>
        </tr>
        <tr>
            {foreach $data_jadwal as $jadwal}
                <th>{$jadwal.TGL_JADWAL}</th>
            {/foreach}
        </tr>
        {$diterima=0}
        {$regmaba=0}
        {$sudah_jadwal=0}
        {$sudah_tes=0}
        {foreach $data_laporan as $lap}
            <tr style="font-size: 13px">
                <td>{$lap@index+1}</td>
                <td>
                    {if $smarty.get.fakultas!=''}
                        {$lap.NM_JENJANG} {$lap.NM_PROGRAM_STUDI}
                    {else}
                        {$lap.NM_FAKULTAS}
                    {/if}
                </td>
                <td>{$lap.DITERIMA}</td>
                <td>{$lap.REGMABA}</td>
                <td>{$lap.SUDAH_JADWAL}</td>
                <td>{$lap.SUDAH_TES}</td>
            </tr>
            {$diterima=$diterima+$lap.DITERIMA}
            {$regmaba=$regmaba+$lap.REGMABA}
            {$sudah_jadwal=$sudah_jadwal+$lap.SUDAH_JADWAL}
            {$sudah_tes=$sudah_tes+$lap.SUDAH_TES}
        {foreachelse}
            <tr>
                <td colspan="6" class="data-kosong">Data Kosong</td>
            </tr>
        {/foreach}
        <tr class="total">
            <td class="center" colspan="2">TOTAL</td>
            <td>{$diterima}</td>
            <td>{$regmaba}</td>
            <td>{$sudah_jadwal}</td>
            <td>{$sudah_tes}</td>
        </tr>
    </table>
{/if}
{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}