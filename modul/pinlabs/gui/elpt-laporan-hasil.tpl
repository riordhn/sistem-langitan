<div class="center_title_bar">Laporan Hasil Tes ELPT</div> 
<form method="get" id="report_form" action="elpt-laporan-hasil.php">
    <table style="width:98%;">
        <tr>
            <th colspan="4" class="center">Parameter</th>
        </tr>
        <tr>
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Penerimaan</td>
            <td>
                <select name="penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                {if ($p2.ID_JENJANG==1||$p2.ID_JENJANG==5)}
                                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                                {/if}
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="6" class="center">
                <input type="hidden" name="mode" value="laporan" />
                <input type="submit" name="submit" class="button" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_laporan)}
    <div class="center_title_bar">View Data Hasil Tes ELPT</div>
    <table>
        <tr>
            <th>No</th>
            <th>
                {if $smarty.get.fakultas!=''}
                    PROGRAM STUDI
                {else}
                    FAKULTAS
                {/if}
            </th>
            <th>Jumlah Peserta</th>
            <th>Peserta Hadir</th>
            <th>Kurang = 400</th>
            <th>Antara 401 - 450</th>
            <th>Antara 451 - 500</th>
            <th>Antara 501 - 550</th>
            <th>Antara 551 - 600</th>
            <th>Lebih dari 600</th>
        </tr>
        {foreach $data_laporan as $lap}
            <tr>
                <td>{$lap@index+1}</td>
                <td>
                    {if $smarty.get.fakultas!=''}
                        {$lap.NM_JENJANG} {$lap.NM_PROGRAM_STUDI}
                    {else}
                        {$lap.NM_FAKULTAS}
                    {/if}
                </td>
                <td>{number_format($lap.PESERTA)}</td>
                <td>{number_format($lap.HADIR)}</td>
                <td>{number_format($lap.RANGE_1)}</td>
                <td>{number_format($lap.RANGE_2)}</td>
                <td>{number_format($lap.RANGE_3)}</td>
                <td>{number_format($lap.RANGE_4)}</td>
                <td>{number_format($lap.RANGE_5)}</td>
                <td>{number_format($lap.RANGE_6)}</td>
                {$total_peserta=$total_peserta+$lap.PESERTA}
                {$total_hadir=$total_hadir+$lap.HADIR}
                {$total_range_1=$total_range_1+$lap.RANGE_1}
                {$total_range_2=$total_range_2+$lap.RANGE_2}
                {$total_range_3=$total_range_3+$lap.RANGE_3}
                {$total_range_4=$total_range_4+$lap.RANGE_4}
                {$total_range_5=$total_range_5+$lap.RANGE_5}
                {$total_range_6=$total_range_6+$lap.RANGE_6}
            </tr>
        {/foreach}
        <tr class="total">
            <td colspan="2">TOTAL</td>
            <td>{$total_peserta}</td>
            <td>{$total_hadir}</td>
            <td>{$total_range_1}</td>
            <td>{$total_range_2}</td>
            <td>{$total_range_3}</td>
            <td>{$total_range_4}</td>
            <td>{$total_range_5}</td>
            <td>{$total_range_6}</td>
        </tr>
    </table>
{/if}