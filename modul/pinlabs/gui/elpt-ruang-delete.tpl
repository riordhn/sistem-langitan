<div class="center_title_bar">Master Ruang ELPT - Hapus Ruangan</div>
<h2>Apakah data ruangan ini akan dihapus ?</h2>
<form action="elpt-master-ruang.php" method="post">
<input type="hidden" name="mode" value="delete" />
<input type="hidden" name="id_ruang" value="{$ruang_elpt.ID_RUANG_TOEFL}" />
<table>
    <tr>
        <th colspan="2">Hapus Master RUang Elpt</th>
    </tr>
    <tr>
        <td>Nama Ruang</td>
        <td>{$ruang_elpt.NM_RUANG}</td>
    </tr>
    <tr>
        <td>Kapasitas</td>
        <td>{$ruang_elpt.KAPASITAS}</td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <a class="button" href="elpt-master-ruang.php">Batal</a>
            <input class="button" type="submit" value="Hapus" />
        </td>
    </tr>
</table>
</form>