<div class="center_title_bar">Master Ruang ELPT - Edit</div>

<form action="elpt-master-ruang.php" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_ruang" value="{$ruang_elpt.ID_RUANG_TOEFL}" />
<table>
    <tr>
        <th colspan="2">Edit Master RUang Elpt</th>
    </tr>
    <tr>
        <td>Nama Ruang</td>
        <td><input type="text" name="nama" size="30" value="{$ruang_elpt.NM_RUANG}" /></td>
    </tr>
    <tr>
        <td>Kapasitas</td>
        <td><input type="text" name="kapasitas" size="2" maxlength="2" value="{$ruang_elpt.KAPASITAS}" /></td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <a class="button" href="elpt-master-ruang.php">Batal</a>
            <input class="button" type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>