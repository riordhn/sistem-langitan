<?php
include ('common.php');
require_once ('ociFunction.php');

$id_pt = $id_pt_user;

if ($request_method == 'GET' or $request_method == 'POST') {
    $display = get('cari', '');

    $upper = strtoupper($display);
    //$lower = strtolower($display);
    //$proper = ucwords($display);

    $count = getvar("select count(*) as cek from 
		(select pgg.id_pengguna, nip_pegawai, 
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		username||'.JPG' as photo, upper(nm_unit_kerja) as nm_unit_kerja, upper(nm_role) as nm_role
		from pegawai peg
		join pengguna pgg on pgg.id_pengguna=peg.id_pengguna and pgg.id_perguruan_tinggi = {$id_pt}
		left join role rl on rl.id_role=pgg.id_role
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
		where (upper(nm_pengguna) like '%" . $upper . "%' or upper(pgg.username) like '%" . $upper . "%'))");
    $smarty->assign('DATA', $count['CEK']);

    $hasil = getData("select pgg.id_pengguna, nip_pegawai, 
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		username||'.JPG' as photo, upper(nm_unit_kerja) as nm_unit_kerja, upper(nm_role) as nm_role
		from pegawai peg
		join pengguna pgg on pgg.id_pengguna=peg.id_pengguna and pgg.id_perguruan_tinggi = {$id_pt}
		left join role rl on rl.id_role=pgg.id_role
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
		where (upper(nm_pengguna) like '%" . $upper . "%' or upper(pgg.username) like '%" . $upper . "%')");
    $smarty->assign('KARYAWAN', $hasil);
}

$smarty->display('cari_karyawan.tpl');
?>