<?php
include '../../config.php';
include 'includes/upload.class.php';

$id_pt = $id_pt_user;

$upload = new upload($db);

$mode = 'view';

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	// Jika tidak ada masalah upload
	if ($_FILES['file_karyawan']['error'] == UPLOAD_ERR_INI_SIZE || $_FILES['file_karyawan']['error'] == UPLOAD_ERR_FORM_SIZE)
	{
		echo "Ukuran file terlalu besar"; exit();
	}
	else if ($_FILES['file_karyawan']['error'] == UPLOAD_ERR_NO_FILE)
	{
		echo "File tidak ada"; exit();
	}
	else if ($_FILES['file_karyawan']['error'] == UPLOAD_ERR_OK)
	{

		// cara load file excel
		$excel = \PhpOffice\PhpSpreadsheet\IOFactory::load($_FILES['file_karyawan']['tmp_name']);

		// ambil sheet pertama
		$sheet = $excel->getSheet();

		// Perulangan
		for ($pRow = 2; $pRow <= $sheet->getHighestRow(); $pRow++)
		{
			// mengambil cell baris ke pRow kolom ke 2
			//$cell = $sheet->getCellByColumnAndRow(2, $pRow);  // k

			$nik = $sheet->getCellByColumnAndRow(0, $pRow)->getFormattedValue();
			$unit_kerja = $sheet->getCellByColumnAndRow(1, $pRow)->getFormattedValue();
			$nama = $sheet->getCellByColumnAndRow(2, $pRow)->getFormattedValue();
			$status = $sheet->getCellByColumnAndRow(3, $pRow)->getFormattedValue();

			if($status == 'TETAP'){
					$status = 'PNS';
			}

				//cek master pengguna dan kebutuhan input
				$pengguna       = $upload->cek_pengguna($nik,$id_pt);
			    $karyawan  	    = $upload->cek_karyawan_by_perguruan_tinggi($nik,$id_pt);

			    //cek master data sebelum input
			    $unit_kerja		= $upload->cek_unit_kerja($unit_kerja,$id_pt);

			    if(empty($unit_kerja)){
			    	echo "Data Unit Kerja Belum Ada Pada Master, Error Pada Baris Ke-".$pRow."</br>";
			    	exit();
			    }


			    if(!empty($unit_kerja)){
			        if(empty($pengguna)){
			            $nama_pgg =  str_replace("'", "''", $nama);
			            $upload->insert_pengguna_karyawan(stripslashes($nama_pgg), $nik, $id_pt);            
			        }
			        else{
			        	$nama_pgg =  str_replace("'", "''", $nama);
			            $upload->update_pengguna_karyawan(stripslashes($nama_pgg), $nik, $id_pt);
			            //echo "Data Pengguna Sudah Ada";
			        }

			        if(empty($karyawan)){
			            $pengguna1 = $upload->cek_pengguna($nik,$id_pt);
			            //echo "hmm ".$pengguna1['ID_PENGGUNA'];
			            $upload->insert_karyawan($pengguna1['ID_PENGGUNA'], $nik, $unit_kerja['ID_UNIT_KERJA'], $status);
			        }
			        else{
			        	$pengguna1 = $upload->cek_pengguna($nik,$id_pt);
			            $upload->update_karyawan($pengguna1['ID_PENGGUNA'], $nik, $unit_kerja['ID_UNIT_KERJA'], $status);
			            //echo "Data Mahasiswa Sudah Ada";
			        }
			    }


		}
		/*// convert csv menjadi array
		$csv = array_map('str_getcsv', file($_FILES['file_karyawan']['tmp_name']));

		// Proses disini :
		// Edited By : FIKRIE
		// 08-10-2015
		$i=0;
		foreach ($csv as $baris) {
			$i++;
			if($i>1){
				$nik 		= $baris[0];
				$unit_kerja	= $baris[1];
				$nama	 	= $baris[2];
				$status		= $baris[3];

				if($status == 'TETAP'){
					$status = 'PNS';
				}

				//cek master pengguna dan kebutuhan input
				$pengguna       = $upload->cek_pengguna($nik,$id_pt);
			    $karyawan  	    = $upload->cek_karyawan_by_perguruan_tinggi($nik,$id_pt);

			    //cek master data sebelum input
			    $unit_kerja		= $upload->cek_unit_kerja($unit_kerja,$id_pt);

			    if(empty($unit_kerja)){
			    	echo "Data Unit Kerja Belum Ada Pada Master, Error Pada Baris Ke-".$i."</br>";
			    	exit();
			    }


			    if(!empty($unit_kerja)){
			        if(empty($pengguna)){
			            $nama_pgg =  str_replace("'", "''", $nama);
			            $upload->insert_pengguna_karyawan(stripslashes($nama_pgg), $nik, $id_pt);            
			        }
			        else{
			        	$nama_pgg =  str_replace("'", "''", $nama);
			            $upload->update_pengguna_karyawan(stripslashes($nama_pgg), $nik, $id_pt);
			            //echo "Data Pengguna Sudah Ada";
			        }

			        if(empty($karyawan)){
			            $pengguna1 = $upload->cek_pengguna($nik,$id_pt);
			            //echo "hmm ".$pengguna1['ID_PENGGUNA'];
			            $upload->insert_karyawan($pengguna1['ID_PENGGUNA'], $nik, $unit_kerja['ID_UNIT_KERJA'], $status);
			        }
			        else{
			        	$pengguna1 = $upload->cek_pengguna($nik,$id_pt);
			            $upload->update_karyawan($pengguna1['ID_PENGGUNA'], $nik, $unit_kerja['ID_UNIT_KERJA'], $status);
			            //echo "Data Mahasiswa Sudah Ada";
			        }
			    }
			}

		}*/

		echo "Data Tenaga Non Dosen Berhasil Dimasukkan";

		exit();
	}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	$smarty->display('upload/iframe-karyawan.tpl');
}

