<?php
include ('common.php');
require_once ('ociFunction.php');

// sementara, agar tidak edit satu-satu
/* FIKRIE */
if ($user->Role() == AUCC_ROLE_SDM OR $user->Role() == AUCC_ROLE_DOSEN) {
    $id = $_GET['id'];

    if (isset($_POST['submit'])) {
        $id_pengguna = $_POST['id_pengguna'];
        $kode = $_POST['kode'];
        $sk = $_POST['no_sk'];
        $tgl_sk = $_POST['tgl_sk'];
        $tgl_tmt = $_POST['tgl_tmt'];
        $pejabat = $_POST['pejabat'];

        InsertData("insert into sejarah_hukuman (id_pengguna,kode_hukuman, no_sk_hukuman,tgl_sk_hukuman,tmt_sk_hukuman,pejabat_sk_hukuman,status_valid) 
                    values ('{$id_pengguna}','{$kode}','{$sk}',to_date('{$tgl_sk}','DD-MM-YYYY'),to_date('{$tgl_tmt}','DD-MM-YYYY'),'{$pejabat}',1)");

        echo '<script>alert("Data berhasil ditambahkan")</script>';
        echo '<script>window.parent.document.location.reload();</script>';
    }
    $kode_hukuman = getData("SELECT * FROM JENIS_HUKUMAN ORDER BY ID_JENIS_HUKUMAN");
    $smarty->assign('kode_hukuman', $kode_hukuman);
    $smarty->display('insert/insert_hukuman.tpl');
} else {
    header("location: /logout.php");
    exit();
}
?>