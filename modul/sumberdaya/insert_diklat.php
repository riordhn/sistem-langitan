<?php
include ('common.php');
require_once ('ociFunction.php');

// sementara, agar tidak edit satu-satu
/* FIKRIE */
if ($user->Role() == AUCC_ROLE_SDM OR $user->Role() == AUCC_ROLE_DOSEN) {
    $id = $_GET['id'];

    if (isset($_POST['submit'])) {
        $id_pengguna = $_POST['id_pengguna'];
        $nama = $_POST['nama'];
        $status_luar = $_POST['status_luar'];
        $lokasi = $_POST['lokasi'];
        $kota_lokasi = $_POST['kota_lokasi'];
        $tgl_mulai = $_POST['tgl_mulai'];
        $tgl_selesai = $_POST['tgl_selesai'];
        $jumlah_jam = $_POST['jumlah_jam'];
        $penyelenggara = $_POST['penyelenggara'];
        $jenis = $_POST['jenis'];
        $keterangan = $_POST['keterangan'];
        $tahun = $_POST['tahun'];
        $predikat = $_POST['predikat'];
        $tingkat = $_POST['tingkat'];
        
        InsertData("insert into sejarah_diklat (id_pengguna, nama_diklat, status_luar_negeri,lokasi, id_kota_lokasi, tgl_mulai, tgl_selesai,jumlah_jam,penyelenggara,jenis_diklat,keterangan_diklat,tahun_angkatan,predikat,tingkat_diklat,status_valid) 
                    values ('{$id_pengguna}',upper('{$nama}'),'{$status_luar}','{$lokasi}','{$kota_lokasi}',to_date('{$tgl_mulai}','DD-MM-YYYY'),to_date('{$tgl_selesai}','DD-MM-YYYY'),'{$jumlah_jam}','{$penyelenggara}','{$jenis}','{$keterangan}','{$tahun}','{$predikat}','{$tingkat}',1)");

        echo '<script>alert("Data berhasil ditambahkan")</script>';
        echo '<script>window.parent.document.location.reload();</script>';
    }
    $negara = getData("SELECT * FROM NEGARA ORDER BY NM_NEGARA");
    $smarty->assign('negara',$negara);
    $smarty->display('insert/insert_diklat.tpl');
} else {
    header("location: /logout.php");
    exit();
}
?>