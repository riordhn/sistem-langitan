<?php
//Yudi Sulistya, 29/03/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pt = $id_pt_user;

$kd_uk=getData("select id_unit_kerja, upper(nm_unit_kerja) as unitkerja,
case when type_unit_kerja='REKTORAT' then 0 
when type_unit_kerja='LEMBAGA' then 1 
when type_unit_kerja='FAKULTAS' then 2 end as urut0,
case when id_fakultas is null then 0 else id_fakultas end as urut1
from unit_kerja where id_perguruan_tinggi = {$id_pt} and type_unit_kerja in ('REKTORAT', 'LEMBAGA', 'FAKULTAS')
order by urut0, urut1, unitkerja");
$smarty->assign('KD_UK', $kd_uk);

if ($request_method == 'GET' or $request_method == 'POST')
{
		$id_uk = get('id','');
		$id_uk = ($id_uk == '' || !isset($id_uk)) ? 0 : $id_uk; #BIAR GAK NEMU QUERYNYA

		$uk=getData("select upper(uk.nm_unit_kerja)||' ('||count(peg.nip_pegawai)||' karyawan)' as unit_kerja
		from pegawai peg
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
		where (uk.id_unit_kerja=$id_uk or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja=$id_uk) or 
		uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja=$id_uk)) and peg.status_pegawai='KONTRAK'
		and peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=1)
		group by uk.nm_unit_kerja");
		$smarty->assign('UK', $uk);

		$count=getvar("select count(*) as cek from 
		(select peg.id_pengguna, peg.nip_pegawai as username, 
		case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
		else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		upper(nm_unit_kerja) as nm_unit_kerja
		from pegawai peg
		left join pengguna pgg on peg.id_pengguna=pgg.id_pengguna
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
		left join fakultas fak on fak.id_fakultas=uk.id_fakultas
		left join program_studi pst on pst.id_program_studi=uk.id_program_studi
		where (uk.id_unit_kerja=$id_uk or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja=$id_uk) or 
		uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja=$id_uk)) and peg.status_pegawai='KONTRAK'
		and peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=1))");
		$smarty->assign('DATA', $count['CEK']);
		
		$pegawai=getData("select peg.id_pengguna, peg.nip_pegawai as username, 
		case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
		else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		upper(nm_unit_kerja) as nm_unit_kerja
		from pegawai peg
		left join pengguna pgg on peg.id_pengguna=pgg.id_pengguna
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
		left join fakultas fak on fak.id_fakultas=uk.id_fakultas
		left join program_studi pst on pst.id_program_studi=uk.id_program_studi
		where (uk.id_unit_kerja=$id_uk or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja=$id_uk) or 
		uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja=$id_uk)) and peg.status_pegawai='KONTRAK'
		and peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=1)");
		$smarty->assign('PEGAWAI', $pegawai);
}

$smarty->display('sd_karyawan_kontrak.tpl');

?>