<div class="center_title_bar">Upload Data Dosen</div>
<table>
        <tr>
            <th colspan="2">Upload File Excel</th>
        </tr>
        <tr>
            <td>
				<iframe src="upload-dosen-iframe.php"></iframe>
            </td>
            <td>
                <label><a class="disable-ajax" href="includes/Template_Excel_Upload_Dosen.xlsx">Download Template Excel</a></label>
				
				<p>Format susunan file excel :</p>
				<ul>
					<li>NIK : Nomer Induk Kepegawaian</li>
					<li>NIDN : Nomer Induk Dosen Nasional</li>
					<li>PRODI : Program Studi dosennya. Contoh format program studi yang benar : "S1 Sistem Informasi". Pastikan sudah ada di master program studi</li>
					<li>NAMA : Nama lengkap dosen</li>
					<li>STATUS DOSEN : Status dari dosen. Contoh : "TETAP", "KONTRAK", "DPK", atau "HONORER" </li>
				</ul>
            </td>
			<td>
				
			</td>
        </tr>
</table>