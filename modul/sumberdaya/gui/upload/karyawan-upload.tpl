<div class="center_title_bar">Upload Data Tenaga Non Dosen</div>
<table>
        <tr>
            <th colspan="2">Upload File Excel</th>
        </tr>
        <tr>
            <td>
				<iframe src="upload-karyawan-iframe.php"></iframe>
            </td>
            <td>
                <label><a class="disable-ajax" href="includes/Template_Excel_Upload_Karyawan.xlsx">Download Template Excel</a></label>
				
				<p>Format susunan file excel :</p>
				<ul>
					<li>NIK : Nomer Induk Kepegawaian</li>
					<li>UNIT KERJA : Unit Kerja karyawannya. Pastikan sudah ada di master unit kerja</li>
					<li>NAMA : Nama lengkap karyawan</li>
					<li>STATUS KARYAWAN : Status dari karyawan. Contoh : "TETAP", "KONTRAK" , atau "HONORER" </li>
				</ul>
            </td>
			<td>
				
			</td>
        </tr>
</table>