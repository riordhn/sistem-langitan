<div class="center_title_bar">ID Unit Kerja : {$unit_kerja.ID_UNIT_KERJA}</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="master_unit_kerja.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_unit_kerja" value="{$unit_kerja.ID_UNIT_KERJA}" />
<!--<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="2">Detail Unit Kerja</th>
    </tr>
    <tr>
        <td>ID</td>
        <td>{$unit_kerja.ID_UNIT_KERJA}</td>
    </tr>
    <tr>
        <td>Nama Unit Kerja</td>
        <td><input class="form-control" id="nm_unit_kerja" type="text" name="nm_unit_kerja" value="{$unit_kerja.NM_UNIT_KERJA}" size="75" />
        </td>
    </tr>
    <tr>
        <td>Deskripsi</td>
        <td><textarea name="desk_unit_kerja" cols="75" rows="5" class="required">{$unit_kerja.DESKRIPSI_UNIT_KERJA}</textarea>
        </td>
    </tr>
    <!--<tr>
        <td>Type</td>
        <td><input class="form-control" id="type_unit_kerja" type="text" name="type_unit_kerja" value="{$unit_kerja.TYPE_UNIT_KERJA}" />
        </td>
    </tr>-->
    <tr>
        <td>Type Unit Kerja</td>
        <td>
            <select name="type_unit_kerja">
            {if $unit_kerja.TYPE_UNIT_KERJA == 'PERGURUAN TINGGI'}
                {foreach $type_unit_kerja_set_full as $tuk}
                    <option value="{$tuk.TYPE_UNIT_KERJA}" {if $tuk.TYPE_UNIT_KERJA == $unit_kerja.TYPE_UNIT_KERJA}selected="selected"{/if}>{$tuk.TYPE_UNIT_KERJA}</option>
                {/foreach}
            {else}
                {foreach $type_unit_kerja_set as $tuk}
                    <option value="{$tuk.TYPE_UNIT_KERJA}" {if $tuk.TYPE_UNIT_KERJA == $unit_kerja.TYPE_UNIT_KERJA}selected="selected"{/if}>{$tuk.TYPE_UNIT_KERJA}</option>
                {/foreach}
            {/if}
            </select>
        </td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>
            <select name="id_program_studi">
                <option value="0">-- Tidak Ada --</option>
            {foreach $program_studi_set as $prodi}
                <option value="{$prodi.ID_PROGRAM_STUDI}" {if $prodi.ID_PROGRAM_STUDI == $unit_kerja.ID_PROGRAM_STUDI}selected="selected"{/if}>{$prodi.NM_JENJANG} - {$prodi.NM_PROGRAM_STUDI}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>
            <select name="id_fakultas">
                <option value="0">-- Tidak Ada --</option>
            {foreach $fakultas_set as $fak}
                <option value="{$fak.ID_FAKULTAS}" {if $fak.ID_FAKULTAS == $unit_kerja.ID_FAKULTAS}selected="selected"{/if}>{$fak.NM_FAKULTAS}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Unit Kerja Induk</td>
        <td>
            <select name="id_unit_kerja_induk">
                <option value="{$unit_kerja.ID_UNIT_KERJA}">-- Tidak Ada --</option>
            {foreach $unit_kerja_induk_set as $uki}
                <option value="{$uki.ID_UNIT_KERJA}" {if $uki.ID_UNIT_KERJA == $unit_kerja.ID_UNIT_KERJA_INDUK}selected="selected"{/if}>{$uki.NM_JENJANG} {$uki.NM_UNIT_KERJA}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Nama Singkatan Unit</td>
        <td><input class="form-control" id="nm_singkatan_unit" type="text" name="nm_singkatan_unit" value="{$unit_kerja.NM_SINGKATAN_UNIT}" />
        </td>
    </tr>
    <!--<tr>
        <td>Program Studi Dosen</td>
        <td>
            <select name="id_program_studi">
            {foreach $program_studi_set as $prodi}
                <option value="{$prodi.ID_PROGRAM_STUDI}" {if $prodi.ID_PROGRAM_STUDI == $pengguna.ID_PROGRAM_STUDI}selected="selected"{/if}>{$prodi.NM_PROGRAM_STUDI}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Type</td>
        <td><input class="form-control" id="page" type="text" name="page" value="{$modul.PAGE}" />
        </td>
    </tr>-->
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

<!--<form action="account-search.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="ubahPassword" />
<input type="hidden" name="id_pengguna" value="{$pengguna.ID_PENGGUNA}" />
<input type="hidden" name="username" value="{$pengguna.USERNAME}" />
<table>
    <tr>
        <th colspan="2" class="center">Reset Password?</th>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Reset" />
        </td>
    </tr>
</table>
</form>

<script type="text/javascript">
    function deleteRolePengguna(id_role_pengguna)
    {
        if (confirm('Apakah role ini akan di hapus') == true)
        {
            $.ajax({
                type: 'POST',
                url: 'account-search.php',
                data: 'mode=delete-role&id_role_pengguna='+id_role_pengguna,
                success: function(data) {
                    if (data == '1') {
                        $('#r'+id_role_pengguna).remove();
                    }
                    else {
                        alert('Gagal dihapus');
                    }  
                }
            });
        }
    }
</script>
            
<table>
    <tr>
        <th>Default</th>
        <th>Role</th>
        <th>Template</th>
        <th>Action</th>
    </tr>
    {foreach $role_pengguna_set as $rp}
    <tr {if $rp@index is not div by 2}class="row1"{/if} id="r{$rp.ID_ROLE_PENGGUNA}">
        <td class="center"><input type="radio" name="id_role" value="{$rp.ID_ROLE}" {if $rp.ID_ROLE == $pengguna.ID_ROLE}checked="checked"{/if} disabled="disabled"/></td>
        <td>{$rp.NM_ROLE}</td>
        <td>{if $rp.NM_TEMPLATE == ''}Default{else}{$rp.NM_TEMPLATE}{/if}</td>
        <td>
            <a href="account-search.php?mode=edit-role&id_role_pengguna={$rp.ID_ROLE_PENGGUNA}" title="Edit"><img src="{$base_url}img/superadmin/navigation/edit.png" /></a>
            {if $rp.ID_ROLE != $pengguna.ID_ROLE}
            <img src="{$base_url}img/superadmin/navigation/del-red.png" title="Hapus Role" style="cursor: pointer" onclick='deleteRolePengguna({$rp.ID_ROLE_PENGGUNA})' />
            {/if}
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="4" class="center">
            <a href="account-search.php?mode=add-role&id_pengguna={$pengguna.ID_PENGGUNA}&q={$smarty.get.q}">Tambah</a>
        </td>
    </tr>
</table>
-->

<a href="master_unit_kerja.php">Kembali</a>