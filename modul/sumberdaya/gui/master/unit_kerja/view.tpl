<div class="center_title_bar">Unit Kerja</div>

{if isset($unit_kerja_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Unit Kerja</th>
				<th>Deskripsi</th>
				<th>Type</th>
				<th>Program Studi</th>
				<th>Fakultas</th>
				<th>Unit Kerja Induk</th>
				<th>Nama Singkatan</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $unit_kerja_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td><strong>{$uk.NM_JENJANG} {$uk.NM_UNIT_KERJA}</strong></td>
					<td>{$uk.DESKRIPSI_UNIT_KERJA}</td>
					<td>{$uk.TYPE_UNIT_KERJA}</td>
					<td>{$uk.NM_JENJANG} {$uk.NM_PROGRAM_STUDI}</td>
					<td>{$uk.NM_FAKULTAS}</td>
					<!--//memanggil nama unit kerja induk-->
					<td>{$uk.NM_INDUK}</td>
					<td>{$uk.NM_SINGKATAN_UNIT}</td>
					<td class="center">
						<a href="master_unit_kerja.php?mode=edit&id_unit_kerja={$uk.ID_UNIT_KERJA}"><b style="color: red">Edit</b></a>
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="9" class="center">
					<a href="master_unit_kerja.php?mode=add"><b style="color: red">Tambah Unit Kerja</b></a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}