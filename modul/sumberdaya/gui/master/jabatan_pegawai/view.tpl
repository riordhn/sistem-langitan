<div class="center_title_bar">Jabatan Pegawai</div>

{if isset($jabatan_pegawai_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Jabatan Pegawai</th>
				<th>Deskripsi</th>
				<th>Kode Jabatan Pegawai</th>
				<th>Fakultas</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $jabatan_pegawai_set as $jp}
				<tr {if $jp@index is not div by 2}class="odd"{/if}>
					<td class="center">{$jp@index + 1}</td>
					<td><strong>{$jp.NM_JABATAN_PEGAWAI}</strong></td>
					<td>{$jp.DESKRIPSI_JABATAN}</td>
					<td>{$jp.KODE_JABATAN_PEGAWAI}</td>
					<td>{$uk.NM_FAKULTAS}</td>
					<td class="center">
						<a href="master_jabatan_pegawai.php?mode=edit&id_jabatan_pegawai={$jp.ID_JABATAN_PEGAWAI}"><b style="color: red">Edit</b></a>
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="9" class="center">
					<a href="master_jabatan_pegawai.php?mode=add"><b style="color: red">Tambah Jabatan Pegawai</b></a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}