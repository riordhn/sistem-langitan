<div class="center_title_bar">Tambah Jabatan Pegawai</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="master_jabatan_pegawai.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="add" />
<!--<input type="hidden" name="id_role" value="{$smarty.get.id_role}" />
<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="2">Detail Jabatan Pegawai</th>
    </tr>
    <tr>
        <td>Nama Jabatan Pegawai</td>
        <td><input class="form-control" id="nm_jabatan_pegawai" type="text" name="nm_jabatan_pegawai" value="" size="75" />
        </td>
    </tr>
    <tr>
        <td>Deskripsi</td>
        <td><textarea name="desk_jabatan_pegawai" cols="75" rows="5" class="required"></textarea>
        </td>
    </tr>
    <!--<tr>
        <td>Type</td>
        <td><input class="form-control" id="type_unit_kerja" type="text" name="type_unit_kerja" value="{$unit_kerja.TYPE_UNIT_KERJA}" />
        </td>
    </tr>-->
    <tr>
        <td>Kode Jabatan Pegawai</td>
        <td>
            <select name="kode_jabatan_pegawai">
            {foreach $kode_jabatan_pegawai_set as $kjp}
                <option value="{$kjp.KODE_JABATAN_PEGAWAI}">{$kjp.KODE_JABATAN_PEGAWAI}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>
            <select name="id_fakultas">
                <option value="0">-- Tidak Ada --</option>
            {foreach $fakultas_set as $fak}
                <option value="{$fak.ID_FAKULTAS}">{$fak.NM_FAKULTAS}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

<!--<form action="account-search.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="ubahPassword" />
<input type="hidden" name="id_pengguna" value="{$pengguna.ID_PENGGUNA}" />
<input type="hidden" name="username" value="{$pengguna.USERNAME}" />
<table>
    <tr>
        <th colspan="2" class="center">Reset Password?</th>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Reset" />
        </td>
    </tr>
</table>
</form>

<script type="text/javascript">
    function deleteRolePengguna(id_role_pengguna)
    {
        if (confirm('Apakah role ini akan di hapus') == true)
        {
            $.ajax({
                type: 'POST',
                url: 'account-search.php',
                data: 'mode=delete-role&id_role_pengguna='+id_role_pengguna,
                success: function(data) {
                    if (data == '1') {
                        $('#r'+id_role_pengguna).remove();
                    }
                    else {
                        alert('Gagal dihapus');
                    }  
                }
            });
        }
    }
</script>
            
<table>
    <tr>
        <th>Default</th>
        <th>Role</th>
        <th>Template</th>
        <th>Action</th>
    </tr>
    {foreach $role_pengguna_set as $rp}
    <tr {if $rp@index is not div by 2}class="row1"{/if} id="r{$rp.ID_ROLE_PENGGUNA}">
        <td class="center"><input type="radio" name="id_role" value="{$rp.ID_ROLE}" {if $rp.ID_ROLE == $pengguna.ID_ROLE}checked="checked"{/if} disabled="disabled"/></td>
        <td>{$rp.NM_ROLE}</td>
        <td>{if $rp.NM_TEMPLATE == ''}Default{else}{$rp.NM_TEMPLATE}{/if}</td>
        <td>
            <a href="account-search.php?mode=edit-role&id_role_pengguna={$rp.ID_ROLE_PENGGUNA}" title="Edit"><img src="{$base_url}img/superadmin/navigation/edit.png" /></a>
            {if $rp.ID_ROLE != $pengguna.ID_ROLE}
            <img src="{$base_url}img/superadmin/navigation/del-red.png" title="Hapus Role" style="cursor: pointer" onclick='deleteRolePengguna({$rp.ID_ROLE_PENGGUNA})' />
            {/if}
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="4" class="center">
            <a href="account-search.php?mode=add-role&id_pengguna={$pengguna.ID_PENGGUNA}&q={$smarty.get.q}">Tambah</a>
        </td>
    </tr>
</table>
-->

<a href="master_jabatan_pegawai.php">Kembali</a>