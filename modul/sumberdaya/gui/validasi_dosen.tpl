<div class="center_title_bar">Validasi Data Dosen dari Fakultas dan Program Studi</div>
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[3,0]],
		headers: {
            6: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th>NIDN</th>
			<th>Serdos</th>
			<th>NIP</th>
			<th>Nama Dosen</th>
			<th>Status</th>
			<th>Prodi</th>
			<th class="noheader">Aksi</th>
		</tr>
		</thead>
		<tbody>
			{foreach item="dsn" from=$DOSEN}
		<tr class="row">
			{if $dsn.NIDN_DOSEN > 0}
			<td>{$dsn.NIDN_DOSEN}</td>
			{else}
			<td>-</td>
			{/if}
			{if $dsn.SERDOS > 0}
			<td>{$dsn.SERDOS}</td>
			{else}
			<td>-</td>
			{/if}
			<td>{$dsn.USERNAME}</td>
			<td>{$dsn.NM_PENGGUNA}</td>
			<td>{$dsn.NM_STATUS_PENGGUNA}</td>
			<td>{$dsn.NM_PROGRAM_STUDI}</td>
			<td><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/search.png" alt="Detail" title="Detail" onclick="window.location.href='#data_dosen-dosen_lb!dosen_detail.php?action=detail&id={$dsn.ID_PENGGUNA}';"/></span></center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
		</tbody>
	</table>
{if $DATA > 10}
<p align="center">
<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0,0); return false" /></span>
</p>
{else}
{/if}
<p><br/>&nbsp;</p>