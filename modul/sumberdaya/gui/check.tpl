<div class="center_title_bar"> Utility </div>
<table width="850" class="tableabout">
  <tr>
    <th width="38">No</th>
    <th width="178">Sub menu</th>
    <th width="634">Uraian</th>
  </tr>
  <tbody>
  <tr>
    <td><center>1</center></td>
    <td>Tools</td>
    <td>Difungsikan untuk mengganti password akun anda dan untuk berganti role</td>
  </tr>
  <tr class="odd">
    <td><center>2</center></td>
    <td>Dosen No Prodi</td>
    <td>Difungsikan untuk mengecek data Dosen yang belum memiliki homebase Fakultas dan Program Studi</td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Karyawan No Unit Kerja</td>
    <td>Difungsikan untuk mengecek data Karyawan yang belum memiliki homebase Fakultas dan Unit Kerja</td>
  </tr>
  <tr class="odd">
    <td><center>4</center></td>
    <td>Validasi Dosen</td>
    <td>Difungsikan untuk mengecek dan mem-validasi data Dosen dari Fakultas dan Program Studi</td>
  </tr>
  <tr>
    <td><center>5</center></td>
    <td>Validasi Karyawan</td>
    <td>Difungsikan untuk mengecek dan mem-validasi data Karyawan dari Fakultas dan Unit Kerja</td>
  </tr>
  </tbody>
</table>