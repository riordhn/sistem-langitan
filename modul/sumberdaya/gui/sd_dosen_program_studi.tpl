<div class="center_title_bar">Data Dosen Program Studi {foreach item="list" from=$PRODI}<li>{$list.PRODI}</li>{/foreach}</div>
<form action="sd_dosen_program_studi.php" method="get" id="filter">
	<table>
        <tr>
            <th colspan="4">Filter</th>
        </tr>
        <tr>
			<td>Fakultas</td>
            <td>
            <select name="id_fakultas" id="id_fakultas" onchange="javascript: $(this).submit();">
				<option value="">-- Pilih Fakultas --</option>
				{foreach $KD_FAK as $f}
                <option value="{$f.ID_FAKULTAS}" {if $f.ID_FAKULTAS == $smarty.get.id_fakultas}selected="selected"{/if}>{$f.NM_FAKULTAS} <!--({$f.JUMLAH})--></option>
                {/foreach}

		<!--{foreach item="fak" from=$KD_FAK}
		{html_options values=$fak.ID_FAKULTAS output=$fak.NM_FAKULTAS}
		{/foreach}
		-->
			</select>
			</td>

            <td>Program Studi</td>
            <td>
            <select name="id_prodi" id="id_prodi"  onchange="javascript: $(this).submit();">
          		<option value="">-- Pilih Program Studi --</option>
                    <!--{foreach item="prodi" from=$prodi_set}
					{html_options values=$prodi.ID_PROGRAM_STUDI output=$prodi.NM_PROGRAM_STUDI}
					{/foreach}
					-->
                {foreach $prodi_set as $p}
                <option value="{$p.ID_PROGRAM_STUDI}" {if $p.ID_PROGRAM_STUDI == $smarty.get.id_prodi}selected="selected"{/if}>{$p.NM_JENJANG_SNGKAT} - {$p.NM_PROGRAM_STUDI} ({$p.JUMLAH})</option>
                {/foreach}
           	</select>

	<!--<input type="submit" name="View" value="View">
	-->		</td>
        </tr>
    </table>
</form>
<!--{if $smarty.get.id_prodi == ''}
<iframe scrolling="no" src="proses/graph_dosen_tetap.php" width="100%" height="420" frameborder="0"></iframe>
{else}-->
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[3,0]],
		headers: {
            6: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th>NIDN</th>
			<th>Serdos</th>
			<th>NIP</th>
			<th>Nama Dosen</th>
			<th>Status</th>
			<th>Prodi</th>
			<th class="noheader">Aksi</th>
		</tr>
		</thead>
		<tbody>
			{foreach item="dsn" from=$DOSEN}
		<tr class="row">
			<td>{$dsn.NIDN_DOSEN}</td>
			<td>{$dsn.SERDOS}</td>
			<td>{$dsn.USERNAME}</td>
			<td>{$dsn.NM_PENGGUNA}</td>
			<td>{$dsn.NM_STATUS_PENGGUNA}</td>
			<td>{$dsn.NM_PROGRAM_STUDI}</td>
			<td><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/search.png" alt="Detail" title="Detail" onclick="window.location.href='#data_dosen-dosen_tetap!dosen_detail.php?action=detail&id={$dsn.ID_PENGGUNA}';"/></span></center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
		</tbody>
	</table>
<!--{/if}-->
{if $DATA > 10}
<p align="center">
<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0,0); return false" /></span>
</p>
{else}
{/if}
<p><br/>&nbsp;</p>