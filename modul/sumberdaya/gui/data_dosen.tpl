<div class="center_title_bar"> Data Dosen </div>
<table width="850" class="tableabout">
  <tr>
    <th width="38">No</th>
    <th width="178">Sub menu</th>
    <th width="634">Uraian</th>
  </tr>
  <tbody>
  <tr>
    <td><center>1</center></td>
    <td>Dosen Tetap</td>
    <td>Difungsikan untuk menampilkan data Dosen Tetap (PNS)</td>
  </tr>
  <tr class="odd">
    <td><center>2</center></td>
    <td>Dosen CPNS</td>
    <td>Difungsikan untuk menampilkan data Dosen CPNS</td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Dosen GB</td>
    <td>Difungsikan untuk menampilkan data Dosen Guru Besar {$nama_pt}</td>
  </tr>
  <tr class="odd">
    <td><center>4</center></td>
    <td>Dosen GB Emiritus</td>
    <td>Difungsikan untuk menampilkan data Dosen Guru Besar Emiritus</td>
  </tr>
  <tr>
    <td><center>5</center></td>
    <td>Dosen Honorer</td>
    <td>Difungsikan untuk menampilkan data Dosen Honorer</td>
  </tr>
  <tr class="odd">
    <td><center>6</center></td>
    <td>Dosen Kontrak</td>
    <td>Difungsikan untuk menampilkan data Dosen Kontrak</td>
  </tr>
  <tr>
    <td><center>7</center></td>
    <td>Dosen LB</td>
    <td>Difungsikan untuk menampilkan data Dosen Luar Biasa</td>
  </tr>
  <tr class="odd">
    <td><center>8</center></td>
    <td>Dosen No Status</td>
    <td>Difungsikan untuk menampilkan data Dosen yang belum diset status kepegawaiannya</td>
  </tr>
  <tr>
    <td><center>9</center></td>
    <td>Dosen No NIDN</td>
    <td>Difungsikan untuk menampilkan data Dosen yang belum memiliki NIDN</td>
  </tr>
  <tr class="odd">
    <td><center>10</center></td>
    <td>Dosen No Serdos</td>
    <td>Difungsikan untuk menampilkan data Dosen yang belum memiliki Sertifikasi Dosen</td>
  </tr>
  <tr>
    <td><center>11</center></td>
    <td>Dosen Non Aktif</td>
    <td>Difungsikan untuk menampilkan data Dosen non-aktif (Pensiun, Undur Diri, dll)</td>
  </tr>
  <tr class="odd">
    <td><center>12</center></td>
    <td>Cari Dosen</td>
    <td>Difungsikan untuk mencari data Dosen</td>
  </tr>
  <tr>
    <td><center>13</center></td>
    <td>Input Dosen Baru</td>
    <td>Difungsikan untuk menambah data Dosen baru</td>
  </tr>
  </tbody>
</table>