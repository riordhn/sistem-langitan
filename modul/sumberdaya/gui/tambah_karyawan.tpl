{literal}
    <script language="javascript">
        $(document).ready(function() {

            $("#nip").blur(function()
            {
                $("#msgbox1").text("Checking...").fadeIn("slow");
                $.post("cek_nip.php", {nip: $("#nip").val(), id_pt: $("#id_pt").val()}, function(data)
                {
                    if (data == "ada")
                    {
                        $("#msgbox1").fadeTo(200, 0.1, function()
                        {
                            $(this).html("<font color='red'>NIP/NIK tidak valid, sudah digunakan</font>").fadeTo(900, 1);
                        });
                    }
                    else
                    {
                        $("#msgbox1").fadeTo(200, 0.1, function()
                        {
                            $(this).html("").fadeTo(900, 1);
                        });
                    }

                });

            });

        });

        function submitform()
        {
            document.update.submit();
        }
    </script>
{/literal}

<div class="center_title_bar">
    <span class="left">Input Data Karyawan</span>
</div>
{* biodata karyawan *}
<form name="update" id="update" action="tambah_karyawan.php" method="post">
    <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
        <tr><th colspan="4" width="850"><strong>BIODATA</strong></th></tr>
        <input type="hidden" name="action" value="add">
        <input type="hidden" name="id_pt" id="id_pt" value="{$id_pt}">
        <tr>
            <td>Unit Kerja</td>
            <td>:</td>
            <td>
                <select name="unit_kerja" id="unit_kerja">
                    <option value=''></option>
                    {foreach item="unit" from=$T_UNIT}
                        {html_options values=$unit.ID_UNIT_KERJA output=$unit.UNITKERJA}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>NIP/NIK</td>
            <td>:</td>
            <td><input type="text" name="nip" id="nip" style="width:200px;" value=""/>&nbsp;&nbsp;&nbsp;<span id="msgbox1" style="display:none"></span></td>
        </tr>
        <tr>
            <td>Status Kepegawaian</td>
            <td>:</td>
            <td>
                <select name="status_peg" id="status_peg">
                    {foreach item="sts_peg" from=$STS_PEG}
                        {html_options values=$sts_peg.STATUS_PEGAWAI output=$sts_peg.STATUS_PEGAWAI}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Status Aktif</td>
            <td>:</td>
            <td>
                <select name="aktif" id="aktif">
                    {foreach item="aktif" from=$STS_AKTIF}
                        {html_options values=$aktif.ID_STATUS_PENGGUNA output=$aktif.NM_STATUS_PENGGUNA}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Golongan</td>
            <td>:</td>
            <td>
                <select name="gol" id="gol">
                    {foreach item="gol" from=$GOL}
                        {html_options values=$gol.ID_GOLONGAN output=$gol.NM_GOLONGAN}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Gelar Depan</td>
            <td>:</td>
            <td><input type="text" name="gelar_dpn" style="width:200px;" value=""/></td>
        </tr>
        <tr>
            <td>Gelar Belakang</td>
            <td>:</td>
            <td><input type="text" name="gelar_blkg" style="width:200;" value=""/></td>
        </tr>
        <tr>
            <td>Nama Lengkap</td>
            <td>:</td>
            <td><input type="text" name="nm_lengkap" style="width:450px;" value=""/></td>
        </tr>
        <tr>
            <td>Tempat Lahir</td>
            <td>:</td>
            <td>
                <select name="id_kota_lahir">
                    {for $i=0 to $count_provinsi}
                        <optgroup label="{$data_kota[$i].nama}">
                            {foreach $data_kota[$i].kota as $data}
                                {html_options values=$data.ID_KOTA output=$data.KOTA}
                            {/foreach}
                        </optgroup>
                    {/for}
                </select>
                &nbsp;, Tanggal Lahir : <input type="text" name="tgl_lahir" id="tgl_lahir" onclick="javascript:NewCssCal('tgl_lahir', 'ddmmyyyy', '', '', '', '', 'past');" value=""> Format: DD-MM-YYYY
            </td>
        </tr>
        <tr>
            <td>Agama</td>
            <td style="text-align:center"></td>
            <td colspan="2">
                <select name="agama" id="jk">
                    {foreach item="ag" from=$AGAMA}
                        {html_options values=$ag.ID_AGAMA output=$ag.NM_AGAMA}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>:</td>
            <td>
                <select name="jk" id="jk">
                    {foreach item="jk" from=$JK}
                        {html_options values=$jk.KELAMIN_PENGGUNA output=$jk.NM_KELAMIN_PENGGUNA}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><input type="text" name="alamat" style="width:650px;" value="" /></td>
        </tr>
        <tr>
            <td>Telepon/HP</td>
            <td>:</td>
            <td><input type="text" name="tlp" value="" maxlength="15"/></td>
        </tr>
        <tr>
            <td>Email</td>
            <td>:</td>
            <td><input type="text" name="email" style="width:335px;" value="" maxlength="100"/></td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: right;">
                <span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/save.png" alt="Simpan" title="Simpan" onclick="javascript:submitform();" /></span>
                <span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/cancel.png" alt="Batal" title="Batal" onclick="javascript:history.go(-1);" /></span>
            </td>
        </tr>
    </table>
</form>
<p><br/>&nbsp;</p>