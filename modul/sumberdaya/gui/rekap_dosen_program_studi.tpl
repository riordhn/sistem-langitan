<div class="center_title_bar">Data Dosen Berdasarkan Program Studi</div>
<form action="rekap_dosen_program_studi.php" method="get">
    <p>Nama Dosen/Bidang Keahlian :  <input name="cari" type="text" value="{$smarty.get.cari}" style="width:300px;" /> <input type="submit" name="Cari_Dosen" value="Cari Dosen"></p>
</form>
{if $smarty.get.cari == ''}
{else}
    {literal}
        <script type="text/javascript">
            $(document).ready(function()
            {
                $("#myTable").tablesorter(
                        {
                            sortList: [[2, 0]],
                            headers: {
                                0: {sorter: false},
                                6: {sorter: false}
                            }
                        }
                );
            }
            );
        </script>
    {/literal}
    <table id="myTable" style="width: 98%" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
        <thead>
            <tr>
                <th class="noheader">Photo Dosen</th>
                <th>NIP/NIK</th>
                <th>Nama Dosen</th>
                <th>Fakultas (Program Studi)</th>
                <th>Status</th>
                <th>Bidang Keahlian</th>
                <th class="noheader">Aksi</th>
            </tr>
        </thead>
        <tbody>
            {foreach name=test item="list" from=$DOSEN}
                {assign var="photo" value="../../foto_pegawai/{$list.PHOTO}"}
                <tr class="row">
                    {if file_exists($photo)}
                        <td><img src="../../foto_pegawai/{$list.PHOTO}" border="0" width="160" /></td>
                        {else}
                        <td><img src="includes/images/unknown.png" border="0" width="160" /></td>
                        {/if}
                    <td>{$list.NIP_DOSEN}</td>
                    <td>{$list.NM_PENGGUNA}</td>
                    <td>{$list.NM_FAKULTAS}<br>({$list.NM_PROGRAM_STUDI})</td>
                    <td>{$list.NM_STATUS_PENGGUNA}</td>
                    <td>{$list.BIDANG_KEAHLIAN_DOSEN}</td>
                    <td><a class="disable-ajax" onclick="window.location.href='#data_dosen-cari_dosen!dosen_detail.php?action=detail&id={$list.ID_PENGGUNA}'" > <img src="includes/images/search.png" alt="Detail" title="Detail" /></a></td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    {if $DATA > 5}
        <p align="center">
            <span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0, 0);
                return false" /></span>
        </p>
    {else}
    {/if}
{/if}
<p><br/>&nbsp;</p>