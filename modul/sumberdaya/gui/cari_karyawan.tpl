<div class="center_title_bar">Pencarian Data Karyawan</div>
<form action="cari_karyawan.php" method="get">
    <p>Nama/NIK/NIP Tenaga Non Dosen :  <input name="cari" type="text" style="width:300px;" /> <input type="submit" name="Cari_Karyawan" value="Cari Karyawan"></p>
</form>
{if $smarty.get.cari == ''}
{else}
    {literal}
        <script type="text/javascript">
            $(document).ready(function()
            {
                $("#myTable").tablesorter(
                        {
                            sortList: [[2, 0]],
                            headers: {
                                0: {sorter: false},
                                5: {sorter: false}
                            }
                        }
                );
            }
            );
        </script>
    {/literal}
    <table id="myTable" style="width: 98%" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
        <thead>
            <tr>
                <th class="noheader">Photo Karyawan</td>
                <th>NIP/NIK</td>
                <th>Nama Karyawan</td>
                <th>Unit Kerja</td>
                <th>Status</td>
                <th class="noheader">Aksi</td>
            </tr>
        </thead>
        <tbody>
            {foreach name=test item="list" from=$KARYAWAN}
                {assign var="photo" value="../../foto_pegawai/{$nama_singkat}/{$list.PHOTO}"}
                <tr class="row">
                    {if file_exists($photo)}
                        <td style="text-align:center"><img src="../../foto_pegawai/{$nama_singkat}/{$list.PHOTO}" border="0" width="160" /></td>
                        {else}
                        <td style="text-align:center"><img src="includes/images/unknown.png" border="0" width="160" /></td>
                        {/if}
                    <td>{$list.NIP_PEGAWAI}</td>
                    <td>{$list.NM_PENGGUNA}</td>
                    <td>{$list.NM_UNIT_KERJA}</td>
                    <td>{$list.NM_ROLE}</td>
                    <td style="text-align:center"><span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/search.png" alt="Detail" title="Detail" onclick="window.location.href = '#data_karyawan-cari_karyawan!karyawan_detail.php?action=detail&id={$list.ID_PENGGUNA}';"/></span></td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    {if $DATA > 5}
        <p align="center">
            <span onMouseOver="this.style.cursor = 'pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0, 0);
                return false" /></span>
        </p>
    {else}
    {/if}
{/if}
<p><br/>&nbsp;</p>