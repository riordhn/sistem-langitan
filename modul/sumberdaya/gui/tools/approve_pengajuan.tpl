{literal}
    <script language="javascript">



        function submitformDosen()
        {
            var form = $('#pengajuan_dosen');
            form.validate();
            if (form.valid()) {
                $.ajax({
                    url: form.attr('action'),
                    data: form.serialize(),
                    type: 'POST',
                    dataType: 'html',
                    beforeSend: function() {
                        $('#content').html('<div class="loading"><img src="includes/images/loading.gif" border="0" /></div>');
                    },
                    success: function(data) {
                        $('#content').html(data);
                    }
                });
            } else {
                return false;
            }

        }


        function submitformTP()
        {
            var form = $('#pengajuan_tenaga_kependidikan');
            form.validate();
            if (form.valid()) {
                $.ajax({
                    url: form.attr('action'),
                    data: form.serialize(),
                    type: 'POST',
                    dataType: 'html',
                    beforeSend: function() {
                        $('#content').html('<div class="loading"><img src="includes/images/loading.gif" border="0" /></div>');
                    },
                    success: function(data) {
                        $('#content').html(data);
                    }
                });
            } else {
                return false;
            }

        }


        function ambil(KDFAK)
        {
            $.ajax({
                type: "POST",
                url: "getFakultasProgramStudi.php",
                data: "id=" + $("#kdfak").val(),
                cache: false,
                success: function(data) {
                    $('#prodi').html(data);
                }
            });
        }



        function IsNumber(NUMBER)
        {
            if (!document.update.nip.value.match(/^[0-9]+$/)) {
                alert("Format NIP harus angka.");
                return true;
            }
            else if (document.activation.nidn.value.match(/^[0-9]+$/)) {
                alert("Format NIDN harus angka.");
                return true;
            }
            else if (document.activation.serdos.value.match(/^[0-9]+$/)) {
                alert("Format SERDOS harus angka.");
                return true;
            }
            else if (document.activation.tlp.value.match(/^[0-9]+$/)) {
                alert("Format nomor telepon harus angka.");
                return true;
            }
            else if (document.activation.hp.value.match(/^[0-9]+$/)) {
                alert("Format nomor HP harus angka.");
                return true;
            }
            return false;
        }
        $("#myTable").tablesorter(
                {
                    sortList: [[2, 0]]
                }
        );
    </script>
{/literal}

<div class="center_title_bar">
    <span class="left">Pengajuan Data Pegawai</span>
</div>
{if $smarty.get.mode==''}
    <table id="myTable" class="tablesorter" cellspacing="1" style="width: 96%" cellpadding="0" border="0">
        <thead>
            <tr>
                <th>NAMA</td>
                <th>JENIS</td>
                <th>UNIT KERJA/PRODI</td>
                <th>TTL</th>
                <th>STATUS</td>
            </tr>
        </thead>
        <tbody>
            {foreach $pengajuan as $p}
                <tr>
                    <td>{$p.GELAR_DEPAN} {$p.NAMA_LENGKAP} {$p.GELAR_BELAKANG} <br/>({$p.STATUS_KEPEGAWAIAN})</td>
                    <td>{$p.JENIS}</td>
                    <td>{$p.NM_UNIT_KERJA} {$p.NM_JENJANG} {$p.NM_PROGRAM_STUDI}</td>
                    <td>{$p.NM_KOTA} , {$p.TGL_LAHIR}</td>
                    <td style="text-align: center">
                        {if $p.STATUS_PENGAJUAN==0}
                            <label style="color: red">Belum Disetujui</label><br/>
                            <a onclick="window.location.href = '#utility-approve_pengajuan!approve_pengajuan.php?id={$p.ID_PENGAJUAN_PEGAWAI}&mode=approve&jenis={$p.JENIS_PEGAWAI}'" class="ui-corner-tl ui-corner-br ui-button ui-state-default" style="padding: 5px;margin: 10px 5px">Approve</a>
                        {else}
                            <label style="color: green"> Sudah Di Setujui</label><br/>
                            NIK : {$p.NIK}
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{else if $smarty.get.mode=='approve'}
    <a onclick="window.location.href = '#utility-approve_pengajuan!approve_pengajuan.php'" class="ui-corner-tl ui-corner-br ui-button ui-state-default" style="padding: 5px;margin: 10px 5px">Kembali</a>
    {if $smarty.get.jenis==1}
        {* biodata dosen *}
        <form name="pengajuan_dosen" id="pengajuan_dosen" action="approve_pengajuan.php" method="post">
            <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
                <tr><th colspan="4" width="850"><strong>BIODATA DOSEN</strong></th></tr>
                <input type="hidden" name="action" value="pengajuan_dosen">
                <input type="hidden" name="id_pengajuan" value="{$pengajuan.ID_PENGAJUAN_PEGAWAI}">
                <tr>
                    <td>NIP/NIK</td>
                    <td>:</td>
                    <td><input type="text" name="nik" class="required"  style="width:200px;" value="" />&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td>NIDN</td>
                    <td>:</td>
                    <td><input type="text" name="nidn"  style="width:200px;" value="" />&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td>SERDOS</td>
                    <td>:</td>
                    <td><input type="text" name="serdos"  style="width:200px;" value="" />&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td>Fakultas</td>
                    <td>:</td>
                    <td>
                        <select name="kdfak" id="kdfak" onChange="ambil()">
                            <option value=''></option>
                            {foreach item="fak" from=$T_FAK}
                                {html_options values=$fak.ID_FAKULTAS output=$fak.NM_FAKULTAS selected=$kdfak}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Program Studi</td>
                    <td>:</td>
                    <td>
                        <select  class="required" name="prodi" id="prodi">
                            {foreach item="pro" from=$PRO}
                                {html_options values=$pro.ID_PROGRAM_STUDI output=$pro.NM_PROGRAM_STUDI selected=$pengajuan.ID_PROGRAM_STUDI}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Status Kepegawaian</td>
                    <td>:</td>
                    <td>
                        <select name="status_kepeg" id="status_dsn">
                            {foreach item="sts_dsn" from=$STS_DSN}
                                {html_options values=$sts_dsn.STATUS_DOSEN output=$sts_dsn.STATUS_DOSEN selected=$pengajuan.STATUS_KEPEGAWAIAN}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Status Aktif</td>
                    <td>:</td>
                    <td>
                        <select name="aktif" id="aktif">
                            {foreach item="aktif" from=$STS_AKTIF}
                                {html_options values=$aktif.ID_STATUS_PENGGUNA output=$aktif.NM_STATUS_PENGGUNA  selected=$pengajuan.STATUS_AKTIF}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Golongan</td>
                    <td>:</td>
                    <td>
                        <select name="gol" id="gol">
                            {foreach item="gol" from=$GOL}
                                {html_options values=$gol.ID_GOLONGAN output=$gol.NM_GOLONGAN  selected=$pengajuan.ID_GOLONGAN}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Jabatan Fungsional</td>
                    <td>:</td>
                    <td>
                        <select name="fsg" id="fsg">
                            <option value=''></option>
                            {foreach item="fsg" from=$FSG}
                                {html_options values=$fsg.ID_JABATAN_FUNGSIONAL output=$fsg.NM_JABATAN_FUNGSIONAL  selected=$pengajuan.ID_JABATAN_FUNGSIONAL}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Gelar Depan</td>
                    <td>:</td>
                    <td><input type="text" name="gelar_dpn" style="width:200px;" value="{$pengajuan.GELAR_DEPAN}" /></td>
                </tr>
                <tr>
                    <td>Gelar Belakang</td>
                    <td>:</td>
                    <td><input type="text" name="gelar_blkg" style="width:200;" value="{$pengajuan.GELAR_BELAKANG}" /></td>
                </tr>
                <tr>
                    <td>Nama Lengkap</td>
                    <td>:</td>
                    <td><input type="text" name="nm_lengkap" class="required" style="width:450px;" value="{$pengajuan.NAMA_LENGKAP}" /></td>
                </tr>
                <tr>
                    <td>Tempat Lahir</td>
                    <td>:</td>
                    <td>
                        <select name="id_kota_lahir">
                            {for $i=0 to $count_provinsi}
                                <optgroup label="{$data_kota[$i].nama}">
                                    {foreach $data_kota[$i].kota as $data}
                                        {html_options values=$data.ID_KOTA output=$data.KOTA selected=$pengajuan.ID_KOTA_LAHIR}
                                    {/foreach}
                                </optgroup>
                            {/for}
                        </select>
                        <br/> Tanggal Lahir : <input type="text"  class="required" name="tgl_lahir" id="tgl_lahir_dos" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lahir_dos', 'ddmmyyyy', '', '', '', '', 'past');" value="{$pengajuan.TGL_LAHIR}">
                    </td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td>
                        <select name="jk" id="jk">
                            {foreach item="jk" from=$JK}
                                {html_options values=$jk.KELAMIN_PENGGUNA output=$jk.NM_KELAMIN_PENGGUNA selected=$pengajuan.JENIS_KELAMIN}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td><input type="text" name="alamat"  class="required" style="width:650px;" value="{$pengajuan.ALAMAT}" /></td>
                </tr>
                <tr>
                    <td>Telepon</td>
                    <td>:</td>
                    <td><input type="text" name="telp"  class="required" style="text-align:center;" value="{$pengajuan.TELEPON}" maxlength="15" /> <br/> HP : <input type="text" name="hp" style="text-align:center;" value="{$pengajuan.HANDPHONE}" maxlength="15" /></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td><input type="text" name="email" style="width:335px;" value="{$pengajuan.EMAIL}" maxlength="100" /></td>
                </tr>
                <tr>
                    <td colspan="4" style="text-align: right;">
                        <span onclick="submitformDosen();" style="background: url('includes/images/save.png');padding: 8px 15px;border: none;margin-right: 20px" onMouseOver="this.style.cursor = 'pointer'"></span>
                    </td>
                </tr>
            </table>
        </form>
    {else if $smarty.get.jenis==2}
        {* biodata tenaga kependidikan *}
        <form name="pengajuan_tenaga_kependidikan" id="pengajuan_tenaga_kependidikan" action="approve_pengajuan.php" method="post">
            <table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
                <tr><th colspan="4" width="850"><strong>BIODATA TENAGA KEPENDIDIKAN</strong></th></tr>
                <input type="hidden" name="action" value="pengajuan_tenaga_kependidikan">
                <input type="hidden" name="id_pengajuan" value="{$pengajuan.ID_PENGAJUAN_PEGAWAI}">
                <tr>
                    <td>NIP/NIK</td>
                    <td>:</td>
                    <td><input type="text" name="nik" class="required"  style="width:200px;" value=""/>&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td>Unit Kerja</td>
                    <td>:</td>
                    <td>
                        <select  class="required" name="unit_kerja" id="unit_kerja">
                            <option value=''></option>
                            {foreach item="unit" from=$T_UNIT}
                                {html_options values=$unit.ID_UNIT_KERJA output=$unit.UNITKERJA selected=$pengajuan.ID_UNIT_KERJA}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Status Kepegawaian</td>
                    <td>:</td>
                    <td>
                        <select name="status_kepeg" id="status_dsn">
                            {foreach item="sts_dsn" from=$STS_DSN}
                                {html_options values=$sts_dsn.STATUS_DOSEN output=$sts_dsn.STATUS_DOSEN selected=$pengajuan.STATUS_KEPEGAWAIAN}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Status Aktif</td>
                    <td>:</td>
                    <td>
                        <select name="aktif" id="aktif">
                            {foreach item="aktif" from=$STS_AKTIF}
                                {html_options values=$aktif.ID_STATUS_PENGGUNA output=$aktif.NM_STATUS_PENGGUNA  selected=$pengajuan.STATUS_AKTIF}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Golongan</td>
                    <td>:</td>
                    <td>
                        <select name="gol" id="gol">
                            {foreach item="gol" from=$GOL}
                                {html_options values=$gol.ID_GOLONGAN output=$gol.NM_GOLONGAN  selected=$pengajuan.ID_GOLONGAN}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Jabatan Fungsional</td>
                    <td>:</td>
                    <td>
                        <select name="fsg" id="fsg">
                            <option value=''></option>
                            {foreach item="fsg" from=$FSG}
                                {html_options values=$fsg.ID_JABATAN_FUNGSIONAL output=$fsg.NM_JABATAN_FUNGSIONAL  selected=$pengajuan.ID_JABATAN_FUNGSIONAL}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Gelar Depan</td>
                    <td>:</td>
                    <td><input type="text" name="gelar_dpn" style="width:200px;" value="{$pengajuan.GELAR_DEPAN}" /></td>
                </tr>
                <tr>
                    <td>Gelar Belakang</td>
                    <td>:</td>
                    <td><input type="text" name="gelar_blkg" style="width:200;" value="{$pengajuan.GELAR_BELAKANG}" /></td>
                </tr>
                <tr>
                    <td>Nama Lengkap</td>
                    <td>:</td>
                    <td><input type="text" name="nm_lengkap" class="required" style="width:450px;" value="{$pengajuan.NAMA_LENGKAP}" /></td>
                </tr>
                <tr>
                    <td>Tempat Lahir</td>
                    <td>:</td>
                    <td>
                        <select name="id_kota_lahir">
                            {for $i=0 to $count_provinsi}
                                <optgroup label="{$data_kota[$i].nama}">
                                    {foreach $data_kota[$i].kota as $data}
                                        {html_options values=$data.ID_KOTA output=$data.KOTA selected=$pengajuan.ID_KOTA_LAHIR}
                                    {/foreach}
                                </optgroup>
                            {/for}
                        </select>
                        <br/> Tanggal Lahir : <input type="text"  class="required" name="tgl_lahir" id="tgl_lahir_dos" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lahir_dos', 'ddmmyyyy', '', '', '', '', 'past');" value="{$pengajuan.TGL_LAHIR}">
                    </td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td>
                        <select name="jk" id="jk">
                            {foreach item="jk" from=$JK}
                                {html_options values=$jk.KELAMIN_PENGGUNA output=$jk.NM_KELAMIN_PENGGUNA selected=$pengajuan.JENIS_KELAMIN}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td><input type="text" name="alamat"  class="required" style="width:650px;" value="{$pengajuan.ALAMAT}" /></td>
                </tr>
                <tr>
                    <td>Telepon</td>
                    <td>:</td>
                    <td><input type="text" name="telp"  class="required" style="text-align:center;" value="{$pengajuan.TELEPON}" maxlength="15" /> <br/> HP : <input type="text" name="hp" style="text-align:center;" value="{$pengajuan.HANDPHONE}" maxlength="15" /></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td><input type="text" name="email" style="width:335px;" value="{$pengajuan.EMAIL}" maxlength="100" /></td>
                </tr>
                <tr>
                    <td colspan="4" style="text-align: right;">
                        <span  onclick="submitformTP();"  style="background: url('includes/images/save.png');padding: 8px 15px;border: none;margin-right: 20px" onMouseOver="this.style.cursor = 'pointer'"></span>
                    </td>
                </tr>
            </table>
        </form>

    {/if}
    {literal}
        <style>
            label.error{
                color: red;
                display: block;
                font-size: 0.9em;
                font-family: sans-serif;
            }
            input.error ,textarea.error{
                border: 1px solid red;
            }
        </style>
    {/literal}
    <p><br/>&nbsp;</p>
{/if}