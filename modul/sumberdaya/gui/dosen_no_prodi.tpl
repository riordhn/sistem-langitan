<div class="center_title_bar">Cek Data Dosen yang Belum Memiliki Homebase Fakultas dan Program Studi</div>
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            2: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th>NIP/NIK</th>
			<th>Nama Dosen</th>
			<th class="noheader">Aksi</th>
		</tr>
		</thead>
		<tbody>
			{foreach item="dsn" from=$DOSEN}
		<tr class="row">
			<td>{$dsn.USERNAME}</td>
			<td>{$dsn.NM_PENGGUNA}</td>
			<td><center><a href="dosen_detail.php?action=detail&id={$dsn.ID_PENGGUNA}">Detail</a></center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
		</tbody>
	</table>
{if $DATA > 10}
<p align="center">
<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0,0); return false" /></span>
</p>
{else}
{/if}
<p><br/>&nbsp;</p>