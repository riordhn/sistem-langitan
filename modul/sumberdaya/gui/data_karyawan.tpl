<div class="center_title_bar"> Data Karyawan </div>
<table width="850" class="tableabout">
  <tr>
    <th width="38">No</th>
    <th width="178">Sub menu</th>
    <th width="634">Uraian</th>
  </tr>
  <tbody>
  <tr>
    <td><center>1</center></td>
    <td>Karyawan Tetap</td>
    <td>Difungsikan untuk menampilkan data Karyawan tetap di Universitas</td>
  </tr>
  <tr class="odd">
    <td><center>2</center></td>
    <td>Karyawan Kontrak</td>
    <td>Difungsikan untuk menampilkan data Karyawan kontrak di Universitas</td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Karyawan No Status</td>
    <td>Difungsikan untuk menampilkan data Karyawan yang belum diset status kepegawaiannya di Universitas</td>
  </tr>
  <tr class="odd">
    <td><center>4</center></td>
    <td>Karyawan Non Aktif</td>
    <td>Difungsikan untuk menampilkan data Karyawan non-aktif (Pensiun, Undur Diri, dll) di Universitas</td>
  </tr>
  <tr>
    <td><center>5</center></td>
    <td>Cari Karyawan</td>
    <td>Difungsikan untuk mencari data Karyawan</td>
  </tr>
  <tr class="odd">
    <td><center>6</center></td>
    <td>Input Karyawan Baru</td>
    <td>Difungsikan untuk menambah data Karyawan baru</td>
  </tr>
  </tbody>
</table>