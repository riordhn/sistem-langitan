<html>
    <head>
        <link rel="stylesheet" type="text/css" href="includes/iframe.css" />
        <script type="text/javascript" src="js/datetimepicker.js"></script>
        <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
    </head>
    <body>
        <form name="diklatinsert" action="insert_hukuman.php" id="klgdrinsert" method="post" onsubmit="return validate_form();">
            <input type="hidden" name="id_pengguna" value="{$smarty.get.id}">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr class="collapse">
                    <td class="labelrow">Kode Hukuman&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="kode">
                            <option value="">Pilih Kode Hukuman</option>
                            {foreach $kode_hukuman as $kh}
                                <option value="{$kh.KODE_HUKUMAN}" >{$kh.KODE_HUKUMAN} {$kh.NAMA_HUKUMAN} {$kh.KETERANGAN}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Nomor SK Hukuman&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="no_sk" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal SK Hukuman&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_sk" style="width:120px; text-align:center;" id="tgl_sk{$d.ID_SEJARAH_HUKUMAN}" style="text-align:center;" onclick="javascript:NewCssCal('tgl_sk{$d.ID_SEJARAH_HUKUMAN}', 'ddmmyyyy', '', '', '', '', 'past')" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal TMT Hukuman&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_tmt" style="width:120px; text-align:center;" id="tgl_tmt{$d.ID_SEJARAH_HUKUMAN}" style="text-align:center;" onclick="javascript:NewCssCal('tgl_tmt{$d.ID_SEJARAH_HUKUMAN}', 'ddmmyyyy', '', '', '', '', 'past')"  /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Pejabat SK Hukuman&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="pejabat" style="width:600px;" maxlength="100" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow"></td>
                    <td class="inputrow"><input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>