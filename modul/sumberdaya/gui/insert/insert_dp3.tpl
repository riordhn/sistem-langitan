<html>
    <head>
        <link rel="stylesheet" type="text/css" href="includes/iframe.css" />
        <script type="text/javascript" src="js/datetimepicker.js"></script>
        <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
    </head>
    <body>
        <form name="diklatinsert" action="insert_dp3.php" id="klgdrinsert" method="post" onsubmit="return validate_form();">
            <input type="hidden" name="id_pengguna" value="{$smarty.get.id}">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr class="collapse">
                    <td class="labelrow">NIP Pejabat Penilai&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nip_penilai" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">NIP Lama Pejabat Penilai&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nip_lama_penilai" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Jabatan Pejabat Penilai&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="jabatan_penilai" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Unit Pejabat Penilai&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="unit_penilai" style="width:600px;" maxlength="100" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">NIP Atasan Pejabat Penilai&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nip_atasan" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Jabatan Atasan Pejabat Penilai&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="jabatan_atasan" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Unit Atasan Pejabat Penilai&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="unit_atasan" style="width:600px;" maxlength="100" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Nilai Kesetiaan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="kesetiaan" style="width:100px;" maxlength="4" placeholder="1-100" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Nilai Kerjasama&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="kerjasama" style="width:100px;" maxlength="4" placeholder="1-100" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Nilai Prestasi Kerja&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="prestasi" style="width:100px;" maxlength="4" placeholder="1-100" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Nilai Prakarsa&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="prakarsa" style="width:100px;" maxlength="4" placeholder="1-100" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Nilai Tanggung Jawab&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tanggung_jawab" style="width:100px;" maxlength="4" placeholder="1-100" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Nilai Kepemimpinan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="kepemimpinan" style="width:100px;" maxlength="4" placeholder="1-100" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Nilai Ketaatan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="ketaatan" style="width:100px;" maxlength="4" placeholder="1-100" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Nilai Kerjujuran&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="kejujuran" style="width:100px;" maxlength="4" placeholder="1-100" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow"></td>
                    <td class="inputrow"><input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>