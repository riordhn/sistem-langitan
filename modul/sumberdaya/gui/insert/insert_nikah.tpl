<html>
    <head>
        <link rel="stylesheet" type="text/css" href="includes/iframe.css" />
        <script type="text/javascript" src="js/datetimepicker.js"></script>
        <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
        <script language="JavaScript">
            function validate_form() {
                form_document=document.forms["klgdrinsert"];
                if (form_document["nama"].value == null || form_document["karsisu"].value == " " || form_document["tgl_nikah"].value == "")
                {
                    alert("Tidak Boleh Kosong");
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>
        <form action="insert_nikah.php" id="nikahinsert" method="post" onsubmit="return validate_form();">
            <input type="hidden" name="id_pengguna" value="{$smarty.get.id}">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr class="collapse">
                    <td class="labelrow">Nama Pasangan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Nomor Karsis/Karsu&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="karsisu" style="width:600px;" maxlength="20" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal Lahir Pasangan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_lhr_psg" style="width:120px; text-align:center;" id="tgl_lhr_psg" style="text-align:center;" onclick="javascript:NewCssCal('tgl_lhr_psg', 'ddmmyyyy', '', '', '', '', 'past')" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal Nikah&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_nikah" style="width:120px; text-align:center;" id="tgl_nikah" style="text-align:center;" onclick="javascript:NewCssCal('tgl_nikah', 'ddmmyyyy', '', '', '', '', 'past')" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Pekerjaan Pasangan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="pekerjaan_psg" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Pendidikan Pasangan&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="pendidikan_psg" id="id_pdd">
                            {foreach item="pdd1" from=$ID_PDD}
                                {html_options values=$pdd1.ID_PENDIDIKAN_AKHIR output=$pdd1.NAMA_PENDIDIKAN_AKHIR}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Status Pasangan&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="status_psg">
                            <option value="1">Suami/Istri Saat ini</option>
                            <option value="2">Meninggal Duni</option>
                            <option value="3">Cerai</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow"></td>
                    <td class="inputrow"><input type="submit" name="submit" value="Simpan" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>