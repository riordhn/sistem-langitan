{* Yudi Sulistya, 29/03/2012 *}
<div class="center_title_bar">Tools</div>
<p><font color="red"><strong> {$display} </strong></font></p>
<form action="tools.php" method="post">
<input type="hidden" name="action" value="gantipwd">
<table width="850" class="tableabout">
	<tr>
		<th colspan="2">Ganti Password</th>
	</tr>
	<tbody>
	<tr>
		<td width="149">Password Lama</td>
		<td width="754"><input name="pwdlama" type="password" /></td>
	</tr>
	<tr>
		<td width="149">Password Baru</td>
		<td><input name="pwdbaru" type="password" /></td>
	</tr>
	<tr>
		<td width="149">Password Baru (ulangi)</td>
		<td><input name="pwdbaru2" type="password" /></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" name="Submit" value="Submit"></td>
	</tr>
	</tbody>
</table>
</form>
 
<table width="850" class="tableabout">
	<tr>
		<th colspan="2">Ganti Role</th>
	</tr>
	<tbody>
	<tr>
		<td>Role</td>
		<td>
			<input type="hidden" name="id_pengguna" value="{$id_pengguna}" />
            <select name="id_role">
            {foreach $role_set as $r}
                <option value="{$r.ID_ROLE}" {if $r.ID_ROLE == $id_role}selected="selected"{/if}>{$r.NM_ROLE}</option>
            {/foreach}
            </select>
            <button onclick="GantiRole(); return false;">Submit</button>
		</td>
	</tr>
	</tbody>
</table>
 
{literal}
 <script>
 $('#tableText').validate();
function GantiRole()
{
	if (confirm('Apakah Anda yakin akan ganti role ?') == true)
	{
		var id_role = $('select[name="id_role"]').val();
		var id_pengguna = $('input[name="id_pengguna"]').val();
		//var path = (id_fakultas == 8) ? '../' : '';
		var path = '';
		
		$.ajax({
			type: 'POST',
			url: path + 'tools.php',
			data: 'mode=change-role&id_pengguna='+id_pengguna+'&id_role='+id_role,
			success: function(data) {
				if (data == 1)
				{
					alert('Anda sudah ganti role. Akun anda akan di logout.');
					window.location = path + '../../logout.php';
				}
				else
				{
					alert('Gagal ganti role');
				}
			}
		});
	}
}
 </script>
{/literal}