<div class="center_title_bar"> Rekapitulasi Karyawan </div>
<table width="850" class="tableabout">
  <tr>
    <th width="38">No</th>
    <th width="178">Sub menu</th>
    <th width="634">Uraian</th>
  </tr>
  <tbody>
  <tr>
    <td><center>1</center></td>
    <td>Gol/Usia</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Karyawan Aktif berdasarkan Golongan dan Usia Karyawan di Unit Kerja Universitas atau Fakultas</td>
  </tr>
  <tr class="odd">
    <td><center>2</center></td>
    <td>Gol/Pend</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Karyawan Aktif berdasarkan Golongan dan Pendidikan Akhir Karyawan di Unit Kerja Universitas atau Fakultas</td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Pend/Usia</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Karyawan Aktif berdasarkan Pendidikan Akhir dan Usia Karyawan di Unit Kerja Universitas atau Fakultas</td>
  </tr>
{*
  <tr class="odd">
    <td><center>4</center></td>
    <td>Jab/Usia</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Karyawan Aktif berdasarkan Jabatan Fungsional dan Usia Karyawan di Unit Kerja Universitas atau Fakultas</td>
  </tr>
  <tr>
    <td><center>5</center></td>
    <td>Jab/Pend</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Karyawan Aktif berdasarkan Jabatan Fungsional dan Pendidikan Akhir Karyawan di Unit Kerja Universitas atau Fakultas</td>
  </tr>
  <tr class="odd">
    <td><center>6</center></td>
    <td>Gol/Jab</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Karyawan Aktif berdasarkan Golongan dan Jabatan Fungsional Karyawan di Unit Kerja Universitas atau Fakultas</td>
  </tr>
*}
  </tbody>
</table>