<div class="center_title_bar">Rekapitulasi Data Dosen Aktif berdasarkan Golongan dan Pendidikan {if $smarty.get.id == '%'}<li>UNIVERSITAS AIRLANGGA</li>{else}{foreach item="list" from=$FAK}<li>{$list.FAKULTAS}</li>{/foreach}{/if}</div>
<form action="rekap_dosen_gol_pend.php" method="get">
<p>Pilih Fakultas : 
	<select name="id" id="id">
	<option value="">----------</option>
	<option value='%' style="font-weight: bold; color: #FFF; background-color: #261831;">LIHAT SEMUA</option>
	{foreach item="fak" from=$KD_FAK}
	{html_options values=$fak.ID_FAKULTAS output=$fak.NM_FAKULTAS}
	{/foreach}
	</select>
	<input type="submit" name="View" value="View">
</p>
</form>
{if $smarty.get.id == ''}
{else}
	<table width="850" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th rowspan="2" width="130" style="vertical-align: middle;"><center>Golongan</center></th>
			<th colspan="7" width="720"><center>Pendidikan Akhir</center></th>
		</tr>
		<tr>
			<th width="90"><center>S3</center></th>
			<th width="90"><center>S2</center></th>
			<th width="90"><center>S1</center></th>
			<th width="135"><center>Spesialis</center></th>
			<th width="135"><center>Profesi</center></th>
			<th width="90"><center>&Sigma;</center></th>
			<th width="90"><center>&#37;</center></th>
		</tr>
		</thead>
		<tbody>
			{foreach item="gol" from=$GOL}
		<tr>
			<td><center>{$gol.GOLONGAN}</center></td>
			{if $gol.S3 == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_dsn-rekap_gol_pddkn_dsn!list_dosen.php?action=list&mode=pg&pdd=10&gol={$gol.ID_GOLONGAN}&fak={$smarty.get.id}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.S3}</td>{/if}
			{if $gol.S2 == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_dsn-rekap_gol_pddkn_dsn!list_dosen.php?action=list&mode=pg&pdd=1&gol={$gol.ID_GOLONGAN}&fak={$smarty.get.id}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.S2}</td>{/if}
			{if $gol.S1 == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_dsn-rekap_gol_pddkn_dsn!list_dosen.php?action=list&mode=pg&pdd=2&gol={$gol.ID_GOLONGAN}&fak={$smarty.get.id}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.S1}</td>{/if}
			{if $gol.SP == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_dsn-rekap_gol_pddkn_dsn!list_dosen.php?action=list&mode=pg&pdd=7&gol={$gol.ID_GOLONGAN}&fak={$smarty.get.id}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.SP}</td>{/if}
			{if $gol.PR == 0}<td></td>{else}<td class="link" onclick="window.location.href='#rekap_dsn-rekap_gol_pddkn_dsn!list_dosen.php?action=list&mode=pg&pdd=9&gol={$gol.ID_GOLONGAN}&fak={$smarty.get.id}';" alt="Klik untuk melihat daftar" title="Klik untuk melihat daftar">{$gol.PR}</td>{/if}
			<td><center>{$gol.TOTAL}</center></td>
			<td><center>{math equation="((x / y) * 100)" x=$gol.TOTAL y=$TTL format="%.2f"}</center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
			{foreach item="jml" from=$JML}
		<tr>
			<th><center>&Sigma;</center></th>
			<th><center>{$jml.S3}</center></th>
			<th><center>{$jml.S2}</center></th>
			<th><center>{$jml.S1}</center></th>
			<th><center>{$jml.SP}</center></th>
			<th><center>{$jml.PR}</center></th>
			<th><center>{$jml.TOTAL}</center></th>
			<th><center></center></th>
		</tr>
			{/foreach}
			{foreach item="psn" from=$PSN}
		<tr>
			<th><center>&#37;</center></th>
			<th><center>{$psn.S3|string_format:"%.2f"}</center></th>
			<th><center>{$psn.S2|string_format:"%.2f"}</center></th>
			<th><center>{$psn.S1|string_format:"%.2f"}</center></th>
			<th><center>{$psn.SP|string_format:"%.2f"}</center></th>
			<th><center>{$psn.PR|string_format:"%.2f"}</center></th>
			<th><center></center></th>
			<th><center>{$psn.PERSEN}</center></th>
		</tr>
			{/foreach}
		</tbody>
	</table>
<p><br/>&nbsp;</p>
{/if}