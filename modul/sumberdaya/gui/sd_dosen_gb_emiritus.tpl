<div class="center_title_bar">Data Dosen Guru Besar Emiritus {foreach item="list" from=$FAK}<li>{$list.FAKULTAS}</li>{/foreach}</div>
<form action="sd_dosen_gb_emiritus.php" method="get">
<p>Pilih Fakultas : 
	<select name="id" id="id">
	<option value="">----------</option>
	{foreach item="fak" from=$KD_FAK}
	{html_options values=$fak.ID_FAKULTAS output=$fak.NM_FAKULTAS}
	{/foreach}
	</select>
	<input type="submit" name="View" value="View">
</p>
</form>
{if $smarty.get.id == ''}
<iframe scrolling="no" src="proses/graph_dosen_gb_emiritus.php" width="100%" height="420" frameborder="0"></iframe>
{else}
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[3,0]],
		headers: {
            6: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th>NIDN</th>
			<th>Serdos</th>
			<th>NIP</th>
			<th>Nama Dosen</th>
			<th>Status</th>
			<th>Prodi</th>
			<th class="noheader">Aksi</th>
		</tr>
		</thead>
		<tbody>
			{foreach item="dsn" from=$DOSEN}
		<tr class="row">
			<td>{$dsn.NIDN_DOSEN}</td>
			<td>{$dsn.SERDOS}</td>
			<td>{$dsn.USERNAME}</td>
			<td>{$dsn.NM_PENGGUNA}</td>
			<td>{$dsn.NM_STATUS_PENGGUNA}</td>
			<td>{$dsn.NM_PROGRAM_STUDI}</td>
			<td><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/search.png" alt="Detail" title="Detail" onclick="window.location.href='#data_dosen-dosen_gb_emiritus!dosen_detail.php?action=detail&id={$dsn.ID_PENGGUNA}';"/></span></center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
		</tbody>
	</table>
{/if}
{if $DATA > 10}
<p align="center">
<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0,0); return false" /></span>
</p>
{else}
{/if}
<p><br/>&nbsp;</p>