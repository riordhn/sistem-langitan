<div class="center_title_bar">Cek Data Karyawan yang Belum Memiliki Homebase Fakultas dan Unit Kerja</div>
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            2: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th>NIP/NIK</th>
			<th>Nama Karyawan</th>
			<th class="noheader">Aksi</th>
		</tr>
		</thead>
		<tbody>
			{foreach item="peg" from=$PEGAWAI}
		<tr class="row">
			<td>{$peg.USERNAME}</td>
			<td>{$peg.NM_PENGGUNA}</td>
			<td><center><span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/search.png" alt="Detail" title="Detail" onclick="window.location.href='#utility-karyawan_no_unit_kerja!karyawan_detail.php?action=detail&id={$peg.ID_PENGGUNA}';"/></span></center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
		</tbody>
	</table>
{if $DATA > 10}
<p align="center">
<span onMouseOver="this.style.cursor='pointer'"><img src="includes/images/up.png" alt="Kembali ke atas" title="Kembali ke atas" onclick="window.scrollTo(0,0); return false" /></span>
</p>
{else}
{/if}
<p><br/>&nbsp;</p>