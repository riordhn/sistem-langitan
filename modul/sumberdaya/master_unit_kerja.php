<?php
include 'config.php';

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_unit_kerja = post('id_unit_kerja');
        $nm_unit_kerja = post('nm_unit_kerja');
        $desk_unit_kerja = post('desk_unit_kerja');
        $type_unit_kerja = post('type_unit_kerja');
        $id_program_studi = post('id_program_studi');
        if($id_program_studi=='0'){
            $id_program_studi = null;
        }
        $id_fakultas = post('id_fakultas');
        if($id_fakultas=='0'){
            $id_fakultas = null;
        }
        $id_unit_kerja_induk = post('id_unit_kerja_induk');
        $nm_singkatan_unit = post('nm_singkatan_unit');
        
        $result = $db->Query("update unit_kerja set nm_unit_kerja = '{$nm_unit_kerja}', deskripsi_unit_kerja = '{$desk_unit_kerja}', 
        						type_unit_kerja = '{$type_unit_kerja}', id_program_studi = '{$id_program_studi}', id_fakultas = '{$id_fakultas}',
        						id_unit_kerja_induk = '{$id_unit_kerja_induk}', nm_singkatan_unit = '{$nm_singkatan_unit}' 
        						where id_unit_kerja = '{$id_unit_kerja}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }
    if (post('mode') == 'add')
    {
        //$id_unit_kerja = post('id_unit_kerja');
        $nm_unit_kerja = post('nm_unit_kerja');
        $desk_unit_kerja = post('desk_unit_kerja');
        $type_unit_kerja = post('type_unit_kerja');
        $id_program_studi = post('id_program_studi');
        if($id_program_studi=='0'){
            $id_program_studi = null;
        }
        $id_fakultas = post('id_fakultas');
        if($id_fakultas=='0'){
            $id_fakultas = null;
        }
        $id_unit_kerja_induk = post('id_unit_kerja_induk');
        $nm_singkatan_unit = post('nm_singkatan_unit');

        $db->Parse(
            "INSERT INTO unit_kerja
            (nm_unit_kerja, deskripsi_unit_kerja, type_unit_kerja, id_program_studi, id_fakultas,
            id_unit_kerja_induk, nm_singkatan_unit, id_perguruan_tinggi)
            VALUES 
            (:nm_unit_kerja, :desk_unit_kerja, :type_unit_kerja, :id_program_studi, :id_fakultas,
            :id_unit_kerja_induk, :nm_singkatan_unit, :id_pt)");
        $db->BindByName(':nm_unit_kerja', $nm_unit_kerja);
        $db->BindByName(':desk_unit_kerja', $desk_unit_kerja);
        $db->BindByName(':type_unit_kerja', $type_unit_kerja);
        $db->BindByName(':id_program_studi', $id_program_studi);
        $db->BindByName(':id_fakultas', $id_fakultas);
        $db->BindByName(':id_unit_kerja_induk', $id_unit_kerja_induk);
        $db->BindByName(':nm_singkatan_unit', $nm_singkatan_unit);
        $db->BindByName(':id_pt', $id_pt);
        $result = $db->Execute();

        if ( ! $result) {
            $error = $db->Error;
        }

        if($id_unit_kerja_induk == '0'){
        	$queryMaxID = $db->Query("SELECT MAX(ID_UNIT_KERJA) AS MAXID FROM UNIT_KERJA");
			$arrayMaxID = $db->FetchAssoc();

			$maxID = $arrayMaxID['MAXID'];

			$update = $db->Query("update unit_kerja set id_unit_kerja_induk = '{$maxID}' where id_unit_kerja = '{$maxID}'");
        }

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan: {$error['message']}");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$unit_kerja_set = $db->QueryToArray("SELECT uk.*, ps.NM_PROGRAM_STUDI, f.NM_FAKULTAS, j.NM_JENJANG, ukk.NM_UNIT_KERJA AS NM_INDUK FROM UNIT_KERJA uk 
												LEFT JOIN PROGRAM_STUDI ps ON uk.ID_PROGRAM_STUDI = ps.ID_PROGRAM_STUDI
                                                LEFT JOIN JENJANG j ON ps.ID_JENJANG = j.ID_JENJANG
												LEFT JOIN FAKULTAS f ON uk.ID_FAKULTAS = f.ID_FAKULTAS
                                                LEFT JOIN UNIT_KERJA ukk ON uk.ID_UNIT_KERJA_INDUK = ukk.ID_UNIT_KERJA
                                                WHERE uk.ID_PERGURUAN_TINGGI = {$id_pt}
												ORDER BY uk.TYPE_UNIT_KERJA");
		$smarty->assignByRef('unit_kerja_set', $unit_kerja_set);

		//memanggil nama unit kerja induk
        /*$unit_kerja_induk_set = $db->QueryToArray("
            select id_unit_kerja_induk, id_unit_kerja 
            from unit_kerja
            where id_perguruan_tinggi = {$id_pt}
            order by id_unit_kerja");
        foreach ($unit_kerja_induk_set as $key => $value) {
            $id_uki = $value['id_unit_kerja_induk'];
            $nama_uki = $db->QueryToArray("
                select id_unit_kerja, nm_unit_kerja 
                from unit_kerja
                where id_unit_kerja = {$id_uki}");
        }  
        $smarty->assignByRef('nama_uki', $nama_uki);
        */
	}
	else if ($mode == 'edit' )
	{
		$id_unit_kerja 	= (int)get('id_unit_kerja', '');

		$unit_kerja = $db->QueryToArray("
            select * 
            from unit_kerja 
            where id_unit_kerja = {$id_unit_kerja}");
        $smarty->assign('unit_kerja', $unit_kerja[0]);

        /*$db->Query("SELECT TYPE_UNIT_KERJA FROM UNIT_KERJA WHERE ID_PERGURUAN_TINGGI = '{$id_pt}'");
        $type_unit_kerja = $db->FetchAssoc();

        $isi_type = $type_unit_kerja['TYPE_UNIT_KERJA'];

        $smarty->assign('isi_type', $isi_type);*/

	    $type_unit_kerja_set_full = $db->QueryToArray("
	            select type_unit_kerja 
	            from type_unit_kerja");
	    $smarty->assign('type_unit_kerja_set_full', $type_unit_kerja_set_full);
        
        $type_unit_kerja_set = $db->QueryToArray("
	           select type_unit_kerja 
	           from type_unit_kerja
	           where type_unit_kerja <> 'PERGURUAN TINGGI'");
	    $smarty->assign('type_unit_kerja_set', $type_unit_kerja_set);

        $fakultas_set = $db->QueryToArray("
            select id_fakultas, nm_fakultas 
            from fakultas
            where id_perguruan_tinggi = {$id_pt}
            order by id_fakultas");
        $smarty->assign('fakultas_set', $fakultas_set);

        $program_studi_set = $db->QueryToArray("
            select ps.id_program_studi, ps.nm_program_studi, j.nm_jenjang 
            from program_studi ps, jenjang j
            where ps.id_jenjang = j.id_jenjang
            and ps.id_fakultas IN (select id_fakultas from fakultas where id_perguruan_tinggi = {$id_pt})
            order by ps.id_program_studi");
        $smarty->assign('program_studi_set', $program_studi_set);

        $unit_kerja_induk_set = $db->QueryToArray("
            select uk.id_unit_kerja, uk.nm_unit_kerja, ps.nm_program_studi, j.nm_jenjang 
            from unit_kerja uk
            left join program_studi ps on ps.id_program_studi = uk.id_program_studi
            left join jenjang j on j.id_jenjang = ps.id_jenjang
            where uk.id_perguruan_tinggi = {$id_pt}
            and uk.id_unit_kerja <> {$id_unit_kerja}");
        $smarty->assign('unit_kerja_induk_set', $unit_kerja_induk_set);

        /*
        $queryIDUnitKerja = $db->Query("select id_unit_kerja_induk 
            from unit_kerja
            where id_unit_kerja = {$id_unit_kerja}");
        $arrayIDUnitKerja = $db->FetchAssoc();
        $IDUnitKerjaInduk = $arrayIDUnitKerja['id_unit_kerja_induk'];

        $nama_unit_kerja_induk = $db->QueryToArray("
            select nm_unit_kerja 
            from unit_kerja 
            where id_unit_kerja = {$IDUnitKerjaInduk}");
        $smarty->assign('nama_unit_kerja_induk', $nama_unit_kerja_induk[0]);
        */

	}
	else if($mode == 'add')
	{
		$db->Query("SELECT TYPE_UNIT_KERJA FROM UNIT_KERJA WHERE ID_PERGURUAN_TINGGI = '{$id_pt}' AND TYPE_UNIT_KERJA = 'PERGURUAN TINGGI'");
        $type_unit_kerja = $db->FetchAssoc();

        $isi_type = $type_unit_kerja['TYPE_UNIT_KERJA'];

        if(empty($isi_type)){
	        $type_unit_kerja_set = $db->QueryToArray("
	            select type_unit_kerja 
	            from type_unit_kerja");
	        $smarty->assign('type_unit_kerja_set', $type_unit_kerja_set);
        }
        else{
        	$type_unit_kerja_set = $db->QueryToArray("
	            select type_unit_kerja 
	            from type_unit_kerja
	            where type_unit_kerja <> 'PERGURUAN TINGGI'");
	        $smarty->assign('type_unit_kerja_set', $type_unit_kerja_set);
        }

        $fakultas_set = $db->QueryToArray("
            select id_fakultas, nm_fakultas 
            from fakultas
            where id_perguruan_tinggi = {$id_pt}
            order by id_fakultas");
        $smarty->assign('fakultas_set', $fakultas_set);

        $program_studi_set = $db->QueryToArray("
            select ps.id_program_studi, ps.nm_program_studi, j.nm_jenjang 
            from program_studi ps, jenjang j
            where ps.id_jenjang = j.id_jenjang
            and ps.id_fakultas IN (select id_fakultas from fakultas where id_perguruan_tinggi = {$id_pt})
            order by ps.id_program_studi");
        $smarty->assign('program_studi_set', $program_studi_set);

        $unit_kerja_induk_set = $db->QueryToArray("
            select uk.id_unit_kerja, uk.nm_unit_kerja, ps.nm_program_studi, j.nm_jenjang 
            from unit_kerja uk
            left join program_studi ps on ps.id_program_studi = uk.id_program_studi
            left join jenjang j on j.id_jenjang = ps.id_jenjang
            where uk.id_perguruan_tinggi = {$id_pt}
            ");
        $smarty->assign('unit_kerja_induk_set', $unit_kerja_induk_set);
	}
}

$smarty->display("master/unit_kerja/{$mode}.tpl");