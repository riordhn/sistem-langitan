<?php
include ('common.php');
require_once ('ociFunction.php');

$id_pt = $id_pt_user;

//die('Mohon Maaf Sementara fitur ini di non aktifkan');

if (isset($_POST['action']) == 'add') {
// TABEL PENGGUNA
	$username = $_POST['nip'];
	$gelar_depan = $_POST['gelar_dpn'];
	$gelar_belakang = $_POST['gelar_blkg'];
	$nm_pengguna = str_replace("'", "''", $_POST['nm_lengkap']);
	$id_kota_lahir = $_POST['id_kota_lahir'];
	$tgl_lahir_pengguna = $_POST['tgl_lahir'];
	$agama_pengguna = $_POST['ag'];
	$email_pengguna = $_POST['email'];
	$kelamin_pengguna = $_POST['jk'];
	$password_hash = sha1($_POST['nip']);
	$password_encrypted = $user->PublicKeyEncrypt($_POST['nip']);

	$cek_pengguna = $db->QuerySingle("select count(*) from pengguna where username='{$username}' and id_perguruan_tinggi = '{$id_pt}'");
	if ($cek_pengguna == 0) {

		$kolom_tabel_pengguna = "
		username, 
		gelar_depan, 
		gelar_belakang, 
		id_agama,
		nm_pengguna, 
		id_kota_lahir, 
		tgl_lahir_pengguna, 
		kelamin_pengguna, 
		email_alternate, 
		id_role, 
		join_table, 
		foto_pengguna,
		password_hash, 
		password_encrypted, 
		password_must_change,
		id_perguruan_tinggi
		";

		$data_tabel_pengguna = "
		trim('{$username}'), 
		trim('{$gelar_depan}'), 
		trim('{$gelar_belakang}'), 
		'{$agama_pengguna}',
		trim(upper('{$nm_pengguna}')), 
		'{$id_kota_lahir}', 
		to_date('{$tgl_lahir_pengguna}','DD-MM-YYYY'), 
		'{$kelamin_pengguna}',
		trim(lower('{$email_pengguna}')), 
		22, 
		1, 
		".$base_url."'foto_pegawai', 
		'{$password_hash}', 
		'{$password_encrypted}', 
		1,
		'{$id_pt}'
		";
		$db->Query("insert into pengguna ({$kolom_tabel_pengguna}) values ({$data_tabel_pengguna})");
	}


	$db->Query("select * from pengguna where username='{$username}' and id_perguruan_tinggi = '{$id_pt}'");
	$get_pengguna = $db->FetchAssoc();
	$id_pengguna = $get_pengguna['ID_PENGGUNA'];


	$cek_pegawai = $db->QuerySingle("select count(*) from pegawai pg
										join pengguna p on p.id_pengguna = pg.id_pengguna and p.id_perguruan_tinggi = '{$id_pt}' 
										where pg.nip_pegawai='{$username}'");
	if ($cek_pegawai == 0) {
		// TABEL PEGAWAI
		$nip_pegawai = $_POST['nip'];
		$id_golongan = $_POST['gol'];
		$id_unit_kerja = $_POST['unit_kerja'];
		$status_pegawai = $_POST['status_peg'];
		if($status_pegawai == 'TETAP'){
			$status_pegawai = 'PNS';
		}
		$id_status_pengguna = $_POST['aktif'];
		$alamat_rumah_pegawai = $_POST['alamat'];
		$telp_pegawai = $_POST['tlp'];

		$kolom_tabel_pegawai = "
		nip_pegawai, 
		id_golongan, 
		id_jabatan_pegawai,
		id_unit_kerja, 
		id_unit_kerja_sd,
		status_pegawai, 
		id_status_pengguna, 
		alamat_pegawai, 
		telp_pegawai, 
		id_pengguna, 
		flag_valid
		";

		$data_tabel_pegawai = "
		trim('" . $nip_pegawai . "'), 
		'{$id_golongan}', 
		26, 
		'{$id_unit_kerja}',
		'{$id_unit_kerja}',
		'{$status_pegawai}', 
		'{$id_status_pengguna}', 
		trim(upper('{$alamat_rumah_pegawai}')), 
		trim('{$telp_pegawai}'), 
		'{$id_pengguna}', 
		1
		";

		$db->Query("insert into pegawai ({$kolom_tabel_pegawai}) values ({$data_tabel_pegawai})");
	}

	// TABEL UNIT ESELON
	$usel1 = $_POST['unit_esselon1'];
	$usel2 = $_POST['unit_esselon2'];
	$usel3 = $_POST['unit_esselon3'];
	$usel4 = $_POST['unit_esselon4'];
	$usel5 = $_POST['unit_esselon5'];

	$db->Query("SELECT * FROM UNIT_ESSELON WHERE ID_PENGGUNA='{$id_pengguna}'");
	$d_usel = $db->FetchAssoc();
	$id_usel = $d_usel['ID_UNIT_ESSELON'];
	if ($id_usel != '') {
		$db->Query("UPDATE UNIT_ESSELON SET UNIT_ESSELON_I='{$usel1}',UNIT_ESSELON_II='{$usel2}',UNIT_ESSELON_III='{$usel3}',UNIT_ESSELON_IV='{$usel4}',UNIT_ESSELON_V='{$usel5}' WHERE ID_UNIT_ESSELON='{$id_usel}'");
	} else {
		$db->Query("INSERT INTO UNIT_ESSELON (ID_PENGGUNA,UNIT_ESSELON_I,UNIT_ESSELON_II,UNIT_ESSELON_III,UNIT_ESSELON_IV,UNIT_ESSELON_V) VALUES ('{$id_pengguna}','{$usel1}','{$usel2}','{$usel3}','{$usel4}','{$usel5}')");
	}

	header("location: {$_SERVER['HTTP_POST']}/modul/sumberdaya/#data_karyawan!karyawan_detail.php?action=detail&id=" . $id_pengguna . "");
} else {

	// $sts_peg = getData("select distinct status_pegawai from pegawai where status_pegawai is not null AND status_pegawai <> 'PNS' order by status_pegawai");

	$sts_peg = array(
		array('STATUS_PEGAWAI' => 'TETAP'),
		array('STATUS_PEGAWAI' => 'KONTRAK')
	);

	$smarty->assign('STS_PEG', $sts_peg);

	$sts_aktif = getData("select id_status_pengguna, upper(nm_status_pengguna) as nm_status_pengguna from status_pengguna where id_role=22 and status_aktif=1 order by nm_status_pengguna");
	$smarty->assign('STS_AKTIF', $sts_aktif);


	$listagama = getData("select * from agama order by id_agama");
	$smarty->assign('AGAMA', $listagama);

	$listunit = getData(
		"SELECT uk.id_unit_kerja, j.nm_jenjang || ' ' || upper(uk.nm_unit_kerja) as unitkerja,
		case when uk.type_unit_kerja='REKTORAT' then 0 
		when uk.type_unit_kerja='LEMBAGA' then 1 
		when uk.type_unit_kerja='FAKULTAS' then 2 
		when uk.type_unit_kerja='PRODI' then 3 end as urut0,
		case when uk.id_fakultas is null then 0 else uk.id_fakultas end as urut1
		from unit_kerja uk
		left join program_studi ps on uk.id_program_studi = ps.id_program_studi
		left join jenjang j on ps.id_jenjang = j.id_jenjang
		where uk.id_perguruan_tinggi = {$id_pt} and uk.type_unit_kerja in ('REKTORAT', 'LEMBAGA', 'FAKULTAS','PRODI')
		order by urut0, urut1, unitkerja");
	$smarty->assign('T_UNIT', $listunit);

	$jk = getData("select distinct kelamin_pengguna, case when kelamin_pengguna='1' then 'LAKI-LAKI' else 'PEREMPUAN' end as nm_kelamin_pengguna from pengguna where kelamin_pengguna in ('1', '2') order by kelamin_pengguna");
	$smarty->assign('JK', $jk);

	$gol = getData("select id_golongan, upper(nm_golongan) as nm_golongan from golongan where id_golongan!=41 order by id_golongan desc");
	$smarty->assign('GOL', $gol);

	$data_kota = array();
	$provinsi = getData("SELECT * FROM PROVINSI WHERE ID_NEGARA=114 ORDER BY NM_PROVINSI");
	foreach ($provinsi as $data) {
		array_push($data_kota, array(
			'nama' => $data['NM_PROVINSI'],
			'kota' => getData("SELECT id_kota, tipe_dati2||' '||nm_kota as kota FROM KOTA WHERE ID_PROVINSI='{$data['ID_PROVINSI']}' ORDER BY TIPE_DATI2, NM_KOTA")
		));
	}
	$smarty->assign('data_kota', $data_kota);
	$smarty->assign('count_provinsi', count($data_kota));

	$smarty->assign('id_pt', $id_pt);

	$smarty->display('tambah_karyawan.tpl');
}
?>