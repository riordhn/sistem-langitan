<?php
//Yudi Sulistya, 29/03/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pt = $id_pt_user;

$kd_fak=getData("SELECT id_fakultas, upper(nm_fakultas) as nm_fakultas from fakultas where id_perguruan_tinggi = {$id_pt} order by id_fakultas");
$smarty->assign('KD_FAK', $kd_fak);

if ($request_method == 'GET' or $request_method == 'POST')
{		

		$count=getvar("SELECT count(*) as cek from 
		(select dsn.id_pengguna, dsn.nip_dosen as username, dsn.nidn_dosen, dsn.serdos, 
		case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
		else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		jjg.nm_jenjang||' - ' ||pst.nm_program_studi as nm_program_studi, sts.nm_status_pengguna
		from dosen dsn
		left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
		left join status_pengguna sts on sts.id_status_pengguna=dsn.id_status_pengguna
		left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
		left join fakultas fak on pst.id_fakultas=fak.id_fakultas
		left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang
		where dsn.id_program_studi is null and pgg.id_perguruan_tinggi = '{$id_pt}')
		--and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1))");
		$smarty->assign('DATA', $count['CEK']);
		
		$dosen=getData("SELECT dsn.id_pengguna, dsn.nip_dosen as username, 
		case when (length(dsn.nidn_dosen)<10 or dsn.nidn_dosen is null) then '-' else dsn.nidn_dosen end as nidn_dosen, 
		case when (length(dsn.serdos)<10 or dsn.serdos is null) then '-' else dsn.serdos end as serdos, 
		case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
		else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		jjg.nm_jenjang||' - ' ||pst.nm_program_studi as nm_program_studi, sts.nm_status_pengguna
		from dosen dsn
		left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
		left join status_pengguna sts on sts.id_status_pengguna=dsn.id_status_pengguna
		left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
		left join fakultas fak on pst.id_fakultas=fak.id_fakultas
		left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang
		where dsn.id_program_studi is null and pgg.id_perguruan_tinggi = '{$id_pt}'
		--and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)");
		$smarty->assign('DOSEN', $dosen);
}

$smarty->display('sd_dosen_no_homebase.tpl');

?>