<?php
//Yudi Sulistya, 16/04/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$count=getvar("select count(*) as cek from 
(select peg.id_pengguna, peg.nip_pegawai as username, 
case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna
from pegawai peg
left join pengguna pgg on peg.id_pengguna=pgg.id_pengguna
where peg.id_unit_kerja_sd is null or peg.id_unit_kerja_sd='')");
$smarty->assign('DATA', $count['CEK']);

$pegawai=getData("select peg.id_pengguna, peg.nip_pegawai as username, 
		case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
		else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna
		from pegawai peg
		left join pengguna pgg on peg.id_pengguna=pgg.id_pengguna
		where (peg.id_unit_kerja_sd is null or peg.id_unit_kerja_sd='')
		and peg.id_pengguna not in (select id_pengguna from dosen)");
$smarty->assign('PEGAWAI', $pegawai);

$smarty->display('karyawan_no_unit_kerja.tpl');

?>