<?php
include ('common.php');
require_once ('ociFunction.php');

$id_pt = $id_pt_user;

//$db->Query("DELETE FROM AUCC.DOSEN WHERE ID_PENGGUNA IN (48765,192684,192679)");
//$db->Query("DELETE FROM AUCC.PENGGUNA WHERE ID_PENGGUNA IN (48765,192684,192679)");
//die('Mohon Maaf Sementara fitur ini di non aktifkan');
if (isset($_POST['action']) == 'add') {

	// TABEL PENGGUNA
	$username			= $_POST['nip'];
	$gelar_depan		= $_POST['gelar_dpn'];
	$gelar_belakang		= $_POST['gelar_blkg'];
	$nm_pengguna		= str_replace("'", "''", $_POST['nm_lengkap']);
	$id_kota_lahir		= $_POST['id_kota_lahir'];
	$tgl_lahir_pengguna = $_POST['tgl_lahir'];
	$agama_pengguna 	= $_POST['agama'];
	$email_pengguna 	= $_POST['email'];
	$kelamin_pengguna 	= $_POST['jk'];
	$password_hash 		= sha1($_POST['nip']);
	$password_encrypted = $user->PublicKeyEncrypt($_POST['nip']);

	if(empty($username)){
		$pesan = "Isian NIP / NIK Tidak Boleh Kosong";
		$smarty->assign('pesan', $pesan);
		
		//$smarty->display('tambah_dosen.tpl');
		header("location: /modul/sumberdaya/#data_dosen!tambah_dosen.php");
		exit();
	}

	$cek_pengguna = $db->QuerySingle("select count(*) from pengguna where username='{$username}' and id_perguruan_tinggi = '{$id_pt}'");
	
	if ($cek_pengguna == 0) {

		/* mendapatkan id_pengguna dari sequence */
		
		$id_pengguna = $db->QuerySingle("select pengguna_seq.nextval from dual");
		$kolom_tabel_pengguna = "
		id_pengguna,
		username, 
		gelar_depan, 
		gelar_belakang, 
		id_agama,
		nm_pengguna, 
		id_kota_lahir, 
		tgl_lahir_pengguna, 
		kelamin_pengguna, 
		email_alternate, 
		id_role, 
		join_table, 
		foto_pengguna,
		password_hash, 
		password_encrypted, 
		password_must_change,
		last_time_password,
		last_time_login,
		id_perguruan_tinggi
		";

		$data_tabel_pengguna = "
		{$id_pengguna},
		trim('{$username}'), 
		trim('{$gelar_depan}'), 
		trim('{$gelar_belakang}'), 
		{$agama_pengguna},
		trim(upper('{$nm_pengguna}')), 
		{$id_kota_lahir}, 
		to_date('{$tgl_lahir_pengguna}','DD-MM-YYYY'), 
		{$kelamin_pengguna},
		trim(lower('{$email_pengguna}')), 
		4, 
		2, 
		'http://langitan.umaha.ac.id/foto_pegawai', 
		'{$password_hash}', 
		'{$password_encrypted}', 
		0,
		'23-AUG-1945',
		'23-AUG-1945',
		{$id_pt}
		";
		$db->Query("insert into pengguna ({$kolom_tabel_pengguna}) values ({$data_tabel_pengguna})");
	}


	$db->Query("select * from pengguna where username='{$username}' and id_perguruan_tinggi = '{$id_pt}'");
	$get_pengguna = $db->FetchAssoc();
	// $id_pengguna = $get_pengguna['ID_PENGGUNA']; 
	/* Sudah diganti dengan mengambil dari sequenct pengguna */

	$cek_dosen = $db->QuerySingle("select count(*) from dosen d 
									join pengguna p on p.id_pengguna = d.id_pengguna and p.id_perguruan_tinggi = '{$id_pt}' 
									where d.nip_dosen='{$username}' ");
	if ($cek_dosen == 0) {
		// TABEL DOSEN
		$nip_dosen = $_POST['nip'];
		$nidn = $_POST['nidn'];
		$serdos = $_POST['serdos'];
		$id_program_studi = $_POST['prodi'];
		$status_dosen = $_POST['status_dsn'];
		$id_status_pengguna = $_POST['aktif'];
		$alamat_rumah_dosen = $_POST['alamat'];
		$nip_lama = $_POST['nip_lama'];
		$prajab_nomor = $_POST['prajab_nomor'];
		$prajab_tanggal = $_POST['prajab_tanggal'];
		$tgl_sumpah_pns = $_POST['tgl_sumpah_pns'];
		$telp_dosen = $_POST['tlp'];
		$mobile_dosen = $_POST['hp'];
		$tmt_cpns = $_POST['tmt_cpns'];
		$nomor_karpeg = $_POST['nomor_karpeg'];
		$nomor_npwp = $_POST['nomor_npwp'];
		$taspen = $_POST['taspen'];
		$kode_pos = $_POST['kode_pos'];
		$kolom_tabel_dosen = "
		id_pengguna,
		nip_dosen,
		nidn_dosen,
		serdos,
		id_program_studi,
		id_program_studi_sd,
		status_dosen,
		id_status_pengguna,
		alamat_rumah_dosen,
		nip_lama,
		prajab_nomor,
		prajab_tanggal,
		tgl_sumpah_pns,
		tlp_dosen,
		mobile_dosen,
		tmt_cpns,
		nomer_karpeg,
		nomor_npwp,
		taspen,
		kode_pos
		";
		$data_tabel_dosen = "
		'{$id_pengguna}',
		trim('{$nip_dosen}'),
		trim('{$$nidn}'),
		trim('{$serdos}'),
		'{$id_program_studi}',
		'{$id_program_studi}',
		'{$status_dosen}',
		'{$id_status_pengguna}',
		trim(upper('{$alamat_rumah_dosen}')),
		trim('{$nip_lama}'),
		trim('{$prajab_nomor}'),
		to_date('{$prajab_tanggal}','DD-MM-YYYY'),
		to_date('{$tgl_sumpah_pns}','DD-MM-YYYY'),
		trim('{$telp_dosen}'),
		trim('{$mobile_dosen}'),
		to_date('{$tmt_cpns}','DD-MM-YYYY'),
		trim('{$nomor_karpeg}'),    
		trim('{$nomor_npwp}'),
		'{$taspen}',
		trim('{$kode_pos}')
		";

		$db->Query("insert into dosen ({$kolom_tabel_dosen}) values ({$data_tabel_dosen})");
	}

	$db->Query("select * from dosen d 
									join pengguna p on p.id_pengguna = d.id_pengguna and p.id_perguruan_tinggi = '{$id_pt}' 
									where d.nip_dosen='{$username}' ");
	$get_dosen = $db->FetchAssoc();
	$id_dosen = $get_dosen['ID_DOSEN'];


	$depart = $_POST['dept'];
	$cek_dosen_departemen = $db->QuerySingle("select count(*) from dosen_departemen where id_dosen='{$id_dosen}' and id_departemen='{$depart}'");
	// TABEL DOSEN DEPARTEMEN
	if ($cek_dosen_departemen == 0) {

		$db->Query("update dosen_departemen set status=0 where id_dosen='{$id_dosen}' ");
		$db->Query("insert into dosen_departemen (id_dosen,id_departemen,status,tanggal) values ('{$id_dosen}','{$depart}',1,to_date(sysdate,'DD-MM-YYYY'))");
	} else if ($cek_dosen_departemen == 1) {
		$db->Query("update dosen_departemen set status=1,tanggal=to_date(sysdate,'DD-MM-YYYY') where id_dosen='{$id_dosen}' and id_departemen='{$depart}'");
	}

	
	// TABEL UNIT ESELON
	$usel1 = $_POST['unit_esselon1'];
	$usel2 = $_POST['unit_esselon2'];
	$usel3 = $_POST['unit_esselon3'];
	$usel4 = $_POST['unit_esselon4'];
	$usel5 = $_POST['unit_esselon5'];

	$db->Query("SELECT * FROM UNIT_ESSELON WHERE ID_PENGGUNA='{$id_pengguna}'");
	$d_usel = $db->FetchAssoc();
	$id_usel = $d_usel['ID_UNIT_ESSELON'];
	if ($id_usel != '') {
		$db->Query("UPDATE UNIT_ESSELON SET UNIT_ESSELON_I='{$usel1}',UNIT_ESSELON_II='{$usel2}',UNIT_ESSELON_III='{$usel3}',UNIT_ESSELON_IV='{$usel4}',UNIT_ESSELON_V='{$usel5}' WHERE ID_UNIT_ESSELON='{$id_usel}'");
	} else {
		$db->Query("INSERT INTO UNIT_ESSELON (ID_PENGGUNA,UNIT_ESSELON_I,UNIT_ESSELON_II,UNIT_ESSELON_III,UNIT_ESSELON_IV,UNIT_ESSELON_V) VALUES ('{$id_pengguna}','{$usel1}','{$usel2}','{$usel3}','{$usel4}','{$usel5}')");
	}
	header("location: /modul/sumberdaya/#data_dosen!dosen_detail.php?action=detail&id=". $id_pengguna);
} else {

	$sts_dsn = getData("select distinct status_dosen from dosen where status_dosen is not null order by status_dosen");
	$smarty->assign('STS_DSN', $sts_dsn);

	$sts_aktif = getData("select id_status_pengguna, upper(nm_status_pengguna) as nm_status_pengguna from status_pengguna where id_role=4 and status_aktif=1 order by nm_status_pengguna");
	$smarty->assign('STS_AKTIF', $sts_aktif);

	$kdfak = $_POST['kdfak'];

	$listfak = getData("select id_fakultas, upper(nm_fakultas) as nm_fakultas from fakultas where id_perguruan_tinggi = {$id_pt} order by id_fakultas");
	$smarty->assign('T_FAK', $listfak);

	$listagama = getData("select * from agama order by id_agama");
	$smarty->assign('AGAMA', $listagama);


	$kdfak1 = isset($_POST['kdfak']) ? $_POST['kdfak'] : '';
	$smarty->assign('FAKGET', $kdfak1);

	if (isset($_POST['kdfak'])) {
		$kdprodi = getData("
		select pst.id_program_studi, upper(substr(jjg.nm_jenjang,1,2)||' - '||pst.nm_program_studi) as nm_program_studi 
		from program_studi pst 
		left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang 
		where id_fakultas=$kdfak
		order by jjg.nm_jenjang, pst.nm_program_studi
		");
		$smarty->assign('PRO', $kdprodi);

		$dept = getData("select id_departemen, upper(nm_departemen) as nm_departemen from departemen where id_fakultas = {$kdfak} order by nm_departemen"); 
		$smarty->assign('DEPT', $dept);
	}

	$jk = getData("select distinct kelamin_pengguna, case when kelamin_pengguna='1' then 'LAKI-LAKI' else 'PEREMPUAN' end as nm_kelamin_pengguna from pengguna where kelamin_pengguna in ('1', '2') order by kelamin_pengguna");
	$smarty->assign('JK', $jk);

	$jab = getData("select id_jabatan_pegawai, upper(nm_jabatan_pegawai) as nm_jabatan_pegawai from jabatan_pegawai order by id_jabatan_pegawai");
	$smarty->assign('JAB', $jab);

	$gol = getData("select id_golongan, upper(nm_golongan) as nm_golongan from golongan order by nm_golongan desc");
	$smarty->assign('GOL', $gol);

	$fsg = getData("select id_jabatan_fungsional, upper(nm_jabatan_fungsional) as nm_jabatan_fungsional from jabatan_fungsional order by id_jabatan_fungsional");
	$smarty->assign('FSG', $fsg);

	$data_kota = array();
	$provinsi = getData("SELECT * FROM PROVINSI WHERE ID_NEGARA=114 ORDER BY NM_PROVINSI");
	foreach ($provinsi as $data) {
		array_push($data_kota, array(
			'nama' => $data['NM_PROVINSI'],
			'kota' => getData("SELECT id_kota, tipe_dati2||' '||nm_kota as kota FROM KOTA WHERE ID_PROVINSI='{$data['ID_PROVINSI']}' ORDER BY TIPE_DATI2, NM_KOTA")
		));
	}
	$smarty->assign('data_kota', $data_kota);
	$smarty->assign('count_provinsi', count($data_kota));

	$smarty->assign('id_pt', $id_pt);

	$smarty->display('tambah_dosen.tpl');
}
?>