<?php

//Yudi Sulistya, 15/08/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

// sementara, agar tidak edit satu-satu
/* FIKRIE */
if ($user->Role() == AUCC_ROLE_SDM OR $user->Role() == AUCC_ROLE_DOSEN) {
$id = $_GET['id'];

if (isset($_POST['submit'])) {
	$id_pengguna = $_POST['id_pengguna'];
	$id_jabatan_fungsional = $_POST['id_jabatan_fungsional'];
	$no_sk_sej_jab_fungsional = $_POST['no_sk_sej_jab_fungsional'];
	$ket_sk_sej_jab_fungsional = $_POST['ket_sk_sej_jab_fungsional'];
	$tmt_sej_jab_fungsional = $_POST['tmt_sej_jab_fungsional'];
	$tgl_sk_sej_jab_fungsional = $_POST['tgl_sk_sej_jab_fungsional'];
	$nama_sk_sej_jab_fungsional = $_POST['nama_sk_sej_jab_fungsional'];
	
	InsertData("insert into sejarah_jabatan_fungsional (
				id_pengguna, id_jabatan_fungsional, no_sk_sej_jab_fungsional, asal_sk_sej_jab_fungsional, ket_sk_sej_jab_fungsional,
				tmt_sej_jab_fungsional, valid_sd, tgl_sk_sej_jab_fungsional, nama_sk_sej_jab_fungsional
				) 
				values (
				$id_pengguna, $id_jabatan_fungsional, trim(upper('".$no_sk_sej_jab_fungsional."')), trim(upper('".$asal_sk_sej_jab_fungsional."')), trim(upper('".$ket_sk_sej_jab_fungsional."')),
				to_date('".$tmt_sej_jab_fungsional."','DD-MM-YYYY'), 1, to_date('".$tgl_sk_sej_jab_fungsional."','DD-MM-YYYY'), trim(upper('".$nama_sk_sej_jab_fungsional."'))
				)");

	echo '<script>alert("Data berhasil ditambahkan")</script>';
	echo '<script>window.parent.document.location.reload();</script>';
}

echo'
<html>
<head>
<link rel="stylesheet" type="text/css" href="includes/iframe.css" />
<script type="text/javascript" src="js/datetimepicker.js"></script>
<script language="JavaScript">
function validate_form(){
if (document.forms["fsginsert"]["no_sk_sej_jab_fungsional"].value==null || document.forms["fsginsert"]["no_sk_sej_jab_fungsional"].value==" " || document.forms["fsginsert"]["no_sk_sej_jab_fungsional"].value=="")
{
	alert ("No. SK harus terisi");
	return false;
}
else if (document.forms["fsginsert"]["asal_sk_sej_jab_fungsional"].value==null || document.forms["fsginsert"]["asal_sk_sej_jab_fungsional"].value==" " || document.forms["fsginsert"]["asal_sk_sej_jab_fungsional"].value=="")
{
	alert ("Asal SK harus terisi");
	return false;
}
else if (document.forms["fsginsert"]["tmt_sej_jab_fungsional"].value==null || document.forms["fsginsert"]["tmt_sej_jab_fungsional"].value==" " || document.forms["fsginsert"]["tmt_sej_jab_fungsional"].value=="")
{
	alert ("TMT SK harus terisi");
	return false;
}
else if (document.forms["fsginsert"]["tgl_sk_sej_jab_fungsional"].value==null || document.forms["fsginsert"]["tgl_sk_sej_jab_fungsional"].value==" " || document.forms["fsginsert"]["tgl_sk_sej_jab_fungsional"].value=="")
{
	alert ("Tanggal SK harus terisi");
	return false;
}
else if (document.forms["fsginsert"]["nama_sk_sej_jab_fungsional"].value==null || document.forms["fsginsert"]["nama_sk_sej_jab_fungsional"].value==" " || document.forms["fsginsert"]["nama_sk_sej_jab_fungsional"].value=="")
{
	alert ("Pejabat penandatangan SK harus terisi");
	return false;
}
	return true;
}
</script>
</head>
<body oncontextmenu="return false;">
	<form name="fsginsert" id="fsginsert" method="post" onsubmit="return validate_form();">
	<input type="hidden" name="id_pengguna" value="'.$id.'">
	<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td align="right">Jabatan<font color="red">*</font></td>
			<td>&nbsp;:&nbsp;<select name="id_jabatan_fungsional">';

$jjg="select id_jabatan_fungsional, upper(nm_jabatan_fungsional) from jabatan_fungsional where id_jabatan_fungsional in (5,6) order by id_jabatan_fungsional";
$result = $db->Query($jjg)or die("salah kueri golongan ");
while($r = $db->FetchRow()) {
echo '<option value="'.$r[0].'">'.$r[1].'</option>';
}
echo '
				</select>
				&nbsp;&nbsp;TMT<font color="red">*</font>&nbsp;:&nbsp;<input type="text" name="tmt_sej_jab_fungsional" id="tmt_sej_jab_fungsional" style="text-align:center;" onclick="javascript:NewCssCal(\'tmt_sej_jab_fungsional\',\'ddmmyyyy\',\'arrow\',\'\',\'\',\'\',\'past\')" value="'.date("d-m-Y").'" />
				&nbsp;&nbsp;Tanggal SK<font color="red">*</font>&nbsp;:&nbsp;<input type="text" name="tgl_sk_sej_jab_fungsional" id="tgl_sk_sej_jab_fungsional" style="text-align:center;" onclick="javascript:NewCssCal(\'tgl_sk_sej_jab_fungsional\',\'ddmmyyyy\',\'arrow\',\'\',\'\',\'\',\'past\')" value="'.date("d-m-Y").'" />
			</td>
		</tr>
		<tr>
			<td align="right">No. SK<font color="red">*</font></td>
			<td>&nbsp;:&nbsp;<input type="text" name="no_sk_sej_jab_fungsional" maxlength="50" style="width:603px;" /></td>
		</tr>
		<tr>
			<td align="right">Asal SK<font color="red">*</font></td>
			<td>&nbsp;:&nbsp;<input type="text" name="asal_sk_sej_jab_fungsional" maxlength="50" style="width:250px;" />
			&nbsp;&nbsp;Keterangan&nbsp;:&nbsp;<input type="text" name="ket_sk_sej_jab_fungsional" maxlength="50" style="width:256px;" /></td>
		</tr>
		<tr>
			<td align="right">Penandatangan SK<font color="red">*</font></td>
			<td>&nbsp;:&nbsp;<input type="text" name="nama_sk_sej_jab_fungsional" maxlength="120" style="width:603px;" /></td>
		</tr>
		<tr>
			<td></td>
			<td>
				&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="Simpan" />
			</td>
		</tr>
	</table>
	</form>
</body>
</html>';
} else {
    header("location: /logout.php");
    exit();
}
?>