<?php
include 'config.php';

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_jabatan_pegawai = post('id_jabatan_pegawai');
        $nm_jabatan_pegawai = post('nm_jabatan_pegawai');
        $desk_jabatan_pegawai = post('desk_jabatan_pegawai');
        $kode_jabatan_pegawai = post('kode_jabatan_pegawai');
        $id_fakultas = post('id_fakultas');
        if($id_fakultas=='0'){
            $id_fakultas = null;
        }
        
        $result = $db->Query("update jabatan_pegawai set nm_jabatan_pegawai = '{$nm_jabatan_pegawai}', deskripsi_jabatan = '{$desk_jabatan_pegawai}', 
        						kode_jabatan_pegawai = '{$kode_jabatan_pegawai}', id_fakultas = '{$id_fakultas}' 
        						where id_jabatan_pegawai = '{$id_jabatan_pegawai}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }
    if (post('mode') == 'add')
    {
        $nm_jabatan_pegawai = post('nm_jabatan_pegawai');
        $desk_jabatan_pegawai = post('desk_jabatan_pegawai');
        $kode_jabatan_pegawai = post('kode_jabatan_pegawai');
        $id_fakultas = post('id_fakultas');
        if($id_fakultas=='0'){
            $id_fakultas = null;
        }
        
        echo "insert into jabatan_pegawai (nm_jabatan_pegawai, deskripsi_jabatan_pegawai, kode_jabatan_pegawai, id_fakultas, id_perguruan_tinggi) 
                                values ('{$nm_jabatan_pegawai}','{$desk_jabatan_pegawai}','{$kode_jabatan_pegawai}','{$id_fakultas}','{$id_pt}')";

        $result = $db->Query("insert into jabatan_pegawai (nm_jabatan_pegawai, deskripsi_jabatan, kode_jabatan_pegawai, id_fakultas, id_perguruan_tinggi) 
        						values ('{$nm_jabatan_pegawai}','{$desk_jabatan_pegawai}','{$kode_jabatan_pegawai}','{$id_fakultas}','{$id_pt}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$jabatan_pegawai_set = $db->QueryToArray("SELECT jp.*, f.NM_FAKULTAS FROM JABATAN_PEGAWAI jp
												LEFT JOIN FAKULTAS f ON jp.ID_FAKULTAS = f.ID_FAKULTAS
                                                WHERE jp.ID_PERGURUAN_TINGGI = {$id_pt}
												ORDER BY jp.ID_JABATAN_PEGAWAI");
		$smarty->assignByRef('jabatan_pegawai_set', $jabatan_pegawai_set);

		//memanggil nama unit kerja induk
        /*$unit_kerja_induk_set = $db->QueryToArray("
            select id_unit_kerja_induk, id_unit_kerja 
            from unit_kerja
            where id_perguruan_tinggi = {$id_pt}
            order by id_unit_kerja");
        foreach ($unit_kerja_induk_set as $key => $value) {
            $id_uki = $value['id_unit_kerja_induk'];
            $nama_uki = $db->QueryToArray("
                select id_unit_kerja, nm_unit_kerja 
                from unit_kerja
                where id_unit_kerja = {$id_uki}");
        }  
        $smarty->assignByRef('nama_uki', $nama_uki);
        */
	}
	else if ($mode == 'edit' )
	{
		$id_jabatan_pegawai = (int)get('id_jabatan_pegawai', '');

		$jabatan_pegawai = $db->QueryToArray("
            select * 
            from jabatan_pegawai 
            where id_jabatan_pegawai = {$id_jabatan_pegawai}");
        $smarty->assign('jabatan_pegawai', $jabatan_pegawai[0]);

        $kode_jabatan_pegawai_set = $db->QueryToArray("
            select kode_jabatan_pegawai 
            from kode_jabatan_pegawai");
        $smarty->assign('kode_jabatan_pegawai_set', $kode_jabatan_pegawai_set);

        $fakultas_set = $db->QueryToArray("
            select id_fakultas, nm_fakultas 
            from fakultas
            where id_perguruan_tinggi = {$id_pt}
            order by id_fakultas");
        $smarty->assign('fakultas_set', $fakultas_set);

	}
	else if($mode == 'add')
	{
		$kode_jabatan_pegawai_set = $db->QueryToArray("
            select kode_jabatan_pegawai 
            from kode_jabatan_pegawai");
        $smarty->assign('kode_jabatan_pegawai_set', $kode_jabatan_pegawai_set);

        $fakultas_set = $db->QueryToArray("
            select id_fakultas, nm_fakultas 
            from fakultas
            where id_perguruan_tinggi = {$id_pt}
            order by id_fakultas");
        $smarty->assign('fakultas_set', $fakultas_set);
	}
}

$smarty->display("master/jabatan_pegawai/{$mode}.tpl");