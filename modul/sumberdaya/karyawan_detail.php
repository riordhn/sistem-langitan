<?php
include ('common.php');
require_once ('ociFunction.php');
include ('includes/encrypt.php');

$depan = time();
$belakang = strrev(time());

if ($request_method == 'GET') {
    if (get('action') == 'detail') {
        $id_pgg = get('id');
        // DATA UPLOAD FOLDER
        $db->Query("SELECT * FROM AUCC.UPLOAD_FOLDER WHERE ID_UPLOAD_FOLDER='5655'");
        $folder_file = $db->FetchAssoc();
        $link_file = "../..{$folder_file['PATH_UPLOAD_FOLDER']}/{$folder_file['NAMA_UPLOAD_FOLDER']}/{$id_pgg}";
        $smarty->assign('link_file', $link_file);

        $pegawai = getData("SELECT PEG.*, STAT_PGG.NM_STATUS_PENGGUNA,PEG.NIP_PEGAWAI AS USERNAME, UPPER(PEG.ALAMAT_PEGAWAI) AS ALAMAT_RUMAH_PEGAWAI, PEG.TELP_PEGAWAI, 
                    PEG.STATUS_PEGAWAI, UPPER(PGG.NM_PENGGUNA) AS NM_PENGGUNA, PGG.GELAR_DEPAN, PGG.GELAR_BELAKANG,
                    TO_CHAR(PGG.TGL_LAHIR_PENGGUNA, 'DD-MM-YYYY') AS TGL_LAHIR_PENGGUNA, PGG.KELAMIN_PENGGUNA, PGG.EMAIL_PENGGUNA, PGG.EMAIL_ALTERNATE,
                    UPPER(JAB.NM_JABATAN_PEGAWAI) AS NM_JABATAN_PEGAWAI,UPPER(UKR.NM_UNIT_KERJA) AS NM_UNIT_KERJA, 
                    TO_CHAR(GOL.TMT_SEJARAH_GOLONGAN, 'DD-MM-YYYY') TMT_GOLONGAN, upper(GOL.NM_GOLONGAN) as NM_GOLONGAN, UPPER(GOL.NM_PANGKAT) as NM_PANGKAT, 
                    UPPER(FSG.NM_JABATAN_FUNGSIONAL) as NM_JABATAN_FUNGSIONAL,TO_CHAR(FSG.TMT_SEJ_JAB_FUNGSIONAL, 'DD-MM-YYYY') TMT_JAB_FUNGSIONAL
                    , KT.TIPE_DATI2||' '||NM_KOTA as TEMPAT_LAHIR,UPPER(NM_JENJANG||' - '||NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI, 
                    UPPER(NM_FAKULTAS) AS NM_FAKULTAS ,AG.NM_AGAMA,SPEN.*,STRUK.*,SNIKAH.*,USEL.*,FUNG_UMUM.*,FUNG_TERTENTU.*
                    FROM AUCC.PEGAWAI PEG
                    JOIN AUCC.PENGGUNA PGG ON PEG.ID_PENGGUNA=PGG.ID_PENGGUNA
                    LEFT JOIN AUCC.STATUS_PENGGUNA STAT_PGG ON STAT_PGG.ID_STATUS_PENGGUNA = PEG.ID_STATUS_PENGGUNA
                    LEFT JOIN AUCC.KOTA KT ON KT.ID_KOTA=PGG.ID_KOTA_LAHIR
                    LEFT JOIN AUCC.UNIT_KERJA UKR ON UKR.ID_UNIT_KERJA=PEG.ID_UNIT_KERJA_SD
                    LEFT JOIN AUCC.PROGRAM_STUDI PST ON UKR.ID_PROGRAM_STUDI=PST.ID_PROGRAM_STUDI
                    LEFT JOIN AUCC.JENJANG J ON J.ID_JENJANG=PST.ID_JENJANG
                    LEFT JOIN AUCC.FAKULTAS FAK ON UKR.ID_FAKULTAS=FAK.ID_FAKULTAS
                    LEFT JOIN AUCC.JABATAN_PEGAWAI JAB ON JAB.ID_JABATAN_PEGAWAI=PEG.ID_JABATAN_PEGAWAI
                    LEFT JOIN AUCC.AGAMA AG ON AG.ID_AGAMA=PGG.ID_AGAMA
                    LEFT JOIN AUCC.STATUS_PERNIKAHAN SNIKAH ON SNIKAH.ID_STATUS_PERNIKAHAN=PGG.ID_STATUS_PERNIKAHAN
                    LEFT JOIN AUCC.UNIT_ESSELON USEL ON USEL.ID_PENGGUNA=PGG.ID_PENGGUNA
                    LEFT JOIN (
                                SELECT * FROM (
                                    SELECT * FROM (
                                      SELECT G.NM_GOLONGAN,G.NM_PANGKAT,SJ.*
                                      FROM AUCC.SEJARAH_GOLONGAN SJ
                                      JOIN AUCC.GOLONGAN G ON SJ.ID_GOLONGAN=G.ID_GOLONGAN
                                      WHERE SJ.ID_PENGGUNA={$id_pgg}
                                      ORDER BY SJ.TMT_SEJARAH_GOLONGAN DESC
                                    ) WHERE ROWNUM=1
                                  )
                                ) GOL ON GOL.ID_PENGGUNA=PEG.ID_PENGGUNA
                    LEFT JOIN (
                        SELECT * FROM (
                            SELECT * FROM (
                              SELECT G.NM_JABATAN_FUNGSIONAL,SJ.*
                              FROM AUCC.SEJARAH_JABATAN_FUNGSIONAL SJ
                              JOIN AUCC.JABATAN_FUNGSIONAL G ON SJ.ID_JABATAN_FUNGSIONAL=G.ID_JABATAN_FUNGSIONAL
                              WHERE SJ.ID_PENGGUNA={$id_pgg}
                              ORDER BY SJ.TMT_SEJ_JAB_FUNGSIONAL DESC
                            ) WHERE ROWNUM=1
                      )
                    ) FSG ON FSG.ID_PENGGUNA=PGG.ID_PENGGUNA
                    LEFT JOIN (
                        SELECT * FROM (
                            SELECT * FROM (
                              SELECT G.NM_JABATAN_STRUKTURAL,SJ.*
                              FROM AUCC.SEJARAH_JABATAN_STRUKTURAL SJ
                              JOIN AUCC.JABATAN_STRUKTURAL G ON SJ.ID_JABATAN_STRUKTURAL=G.ID_JABATAN_STRUKTURAL
                              WHERE SJ.ID_PENGGUNA={$id_pgg}
                              ORDER BY SJ.TMT_SEJ_JAB_STRUKTURAL DESC
                            ) WHERE ROWNUM=1
                      )
                    ) STRUK ON STRUK.ID_PENGGUNA=PEG.ID_PENGGUNA
                    LEFT JOIN (
                        SELECT * FROM (
                            SELECT * FROM (
                              SELECT G.NAMA_JABATAN_FUNGIONAL_PEGAWAI FUNG_UMUM,JK.NAMA_KATEGORI_JABATAN KAT_UMUM,SJ.TMT_JABATAN_FUNGSIONAL TMT_UMUM,SJ.*
                              FROM AUCC.SEJARAH_JABATAN_FUNGSIONAL_PEG SJ
                              JOIN AUCC.JABATAN_FUNGSIONAL_PEGAWAI G ON SJ.ID_JABATAN_FUNGSIONAL_PEGAWAI=G.ID_JABATAN_FUNGSIONAL_PEGAWAI
                              JOIN AUCC.JABATAN_FUNGSIONAL_KATEGORI JK ON JK.ID_JABATAN_FUNGSIONAL_KATEGORI=G.ID_JABATAN_FUNGSIONAL_KATEGORI
                              WHERE SJ.ID_PENGGUNA={$id_pgg} AND G.STATUS=1
                              ORDER BY SJ.TMT_JABATAN_FUNGSIONAL DESC
                            ) WHERE ROWNUM=1
                      )
                    ) FUNG_UMUM ON FUNG_UMUM.ID_PENGGUNA=PEG.ID_PENGGUNA
                    LEFT JOIN (
                        SELECT * FROM (
                            SELECT * FROM (
                              SELECT G.NAMA_JABATAN_FUNGIONAL_PEGAWAI FUNG_TERTENTU,JK.NAMA_KATEGORI_JABATAN KAT_TERTENTU,SJ.TMT_JABATAN_FUNGSIONAL TMT_TERTENTU,SJ.*
                              FROM AUCC.SEJARAH_JABATAN_FUNGSIONAL_PEG SJ
                              JOIN AUCC.JABATAN_FUNGSIONAL_PEGAWAI G ON SJ.ID_JABATAN_FUNGSIONAL_PEGAWAI=G.ID_JABATAN_FUNGSIONAL_PEGAWAI
                              JOIN AUCC.JABATAN_FUNGSIONAL_KATEGORI JK ON JK.ID_JABATAN_FUNGSIONAL_KATEGORI=G.ID_JABATAN_FUNGSIONAL_KATEGORI
                              WHERE SJ.ID_PENGGUNA={$id_pgg} AND G.STATUS=2
                              ORDER BY SJ.TMT_JABATAN_FUNGSIONAL DESC
                            ) WHERE ROWNUM=1
                      )
                    ) FUNG_TERTENTU ON FUNG_TERTENTU.ID_PENGGUNA=PEG.ID_PENGGUNA
                    LEFT JOIN (
                        SELECT * FROM (
                            SELECT * FROM (
                              SELECT NAMA_PENDIDIKAN_AKHIR,SP.*
                              FROM AUCC.SEJARAH_PENDIDIKAN SP
                              JOIN AUCC.PENDIDIKAN_AKHIR PA ON PA.ID_PENDIDIKAN_AKHIR=SP.ID_PENDIDIKAN_AKHIR
                              WHERE SP.ID_PENGGUNA={$id_pgg}
                              ORDER BY SP.TAHUN_LULUS_PENDIDIKAN DESC,ID_SEJARAH_PENDIDIKAN DESC
                            ) WHERE ROWNUM=1
                      )
                    ) SPEN ON SPEN.ID_PENGGUNA=PEG.ID_PENGGUNA
                    WHERE PGG.ID_PENGGUNA={$id_pgg}");
        $smarty->assign('PEGAWAI', $pegawai);

        $sms = getvar("select case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)) else trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)||', '||pgg.gelar_belakang) end as nm_pengguna,
		peg.telp_pegawai from pegawai peg left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
		where peg.id_pengguna=$id_pgg");
        $to = $sms['NM_PENGGUNA'];
        $send = $sms['TELP_PEGAWAI'];
        $link = 'proses/kirim_sms.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&yth=' . $to . '&kirim=' . $send . '&' . $belakang . '=' . $depan . $belakang) . '';
        $smarty->assign('LINK', $link);

        $foto = getvar("select case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)) else trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)||', '||pgg.gelar_belakang) end as nm_pengguna,
		peg.nip_pegawai from pegawai peg left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
		where peg.id_pengguna=$id_pgg");
        $to = $foto['NM_PENGGUNA'];
        $file = $foto['NIP_PEGAWAI'];
        $img = 'proses/upload_photo.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&yth=' . $to . '&file=' . $file . '&' . $belakang . '=' . $depan . $belakang) . '';
        $smarty->assign('IMG', $img);

        $get_photo = getvar("select username as photo from pengguna where id_pengguna=$id_pgg");
        $filename = "../../foto_pegawai/" . $nama_singkat . "/" . $get_photo['PHOTO'] . ".JPG";
        if (file_exists($filename)) {
            $photo = $filename;
        } else {
            $photo = 'includes/images/unknown.png';
        }
        $smarty->assign('PHOTO', $photo);

        //GET DATA DETAIL

        $gol_pegawai = getData("
		select a.*,UF.NAMA_UPLOAD_FILE FILES from
		(
		(select sg.id_sejarah_golongan, sg.id_golongan, upper(gol.nm_golongan) as nm_golongan,gol.nm_pangkat, sg.no_sk_sejarah_golongan, sg.asal_sk_sejarah_golongan,
		sg.keterangan_sk_sejarah_golongan, TO_CHAR(sg.tmt_sejarah_golongan, 'DD-MM-YYYY') as tmt_sejarah_golongan, sg.status_akhir, TO_CHAR(sg.tmt_sejarah_golongan, 'YYYY') as tahun,
		'SK_'||replace(upper(gol.nm_golongan),'/','_') as kategori, TO_CHAR(sg.tgl_sk_sejarah_golongan, 'DD-MM-YYYY') as tgl_sk_sejarah_golongan, sg.ttd_sk_nama_pejabat
		from sejarah_golongan sg
		left join pengguna pgg on pgg.id_pengguna=sg.id_pengguna
		left join golongan gol on gol.id_golongan=sg.id_golongan
		where pgg.id_pengguna=$id_pgg) a
		LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_pangkat_golongan_'||a.id_sejarah_golongan||'%')
		)
		order by a.status_akhir desc, a.tahun desc");
        $smarty->assign('GOL', $gol_pegawai);

        $pdd_pegawai = getData("
		select a.*,UF.NAMA_UPLOAD_FILE FILES from
		(
		(select pdd.*, to_date(pdd.tahun_lulus_pendidikan,'YYYY') as tahun_lulus, pda.nama_pendidikan_akhir, 'IJASAH_'||upper(pda.nama_pendidikan_akhir) as kategori
		from sejarah_pendidikan pdd
		left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		where pgg.id_pengguna=$id_pgg) a
		LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_pendidikan_'||a.id_sejarah_pendidikan||'%') 
                )
		order by a.status_akhir desc, a.tahun_lulus desc");
        $smarty->assign('PEND', $pdd_pegawai);


        $klgdr = getData("
		select * from
		(
		SELECT KD.*,K.NM_KOTA KOTA_LAHIR
                FROM KELUARGA_DIRI KD
                LEFT JOIN KOTA K ON K.ID_KOTA=KD.ID_KOTA_LAHIR
                WHERE KD.ID_PENGGUNA='{$id_pgg}'
		)
		order by TGL_LAHIR_KELUARGA desc");
        $smarty->assign('KLGDR', $klgdr);

        $klgpsg = getData("
		select * from
		(
		SELECT KD.*,K.NM_KOTA KOTA_LAHIR,TO_CHAR(KD.TGL_LAHIR_PASANGAN, 'DD-MM-YYYY') TGL_LAHIR
                FROM KELUARGA_PASANGAN KD
                LEFT JOIN KOTA K ON K.ID_KOTA=KD.ID_KOTA_LAHIR
                WHERE KD.ID_PENGGUNA='{$id_pgg}'
		)
		order by TGL_LAHIR_PASANGAN desc");
        $smarty->assign('KLGPSG', $klgpsg);

        $klgank = getData("
		select * from
		(
		SELECT KD.*,K.NM_KOTA KOTA_LAHIR,TO_CHAR(KD.TGL_LAHIR_ANAK, 'DD-MM-YYYY') TGL_LAHIR,PA.NAMA_PENDIDIKAN_AKHIR
                FROM KELUARGA_ANAK KD
                LEFT JOIN KOTA K ON K.ID_KOTA=KD.TEMPAT_LAHIR_ANAK
                LEFT JOIN PENDIDIKAN_AKHIR PA ON KD.ID_PENDIDIKAN_AKHIR=PA.ID_PENDIDIKAN_AKHIR
                WHERE KD.ID_PENGGUNA='{$id_pgg}'
		)
		order by TGL_LAHIR_ANAK desc");
        $smarty->assign('KLGANK', $klgank);

        $pernikahan = getData("
		select * from
                (
                  SELECT D.*,TO_CHAR(D.TGL_LAHIR_PASANGAN, 'DD-MM-YYYY') TGL_LAHIR,TO_CHAR(D.TGL_PERNIKAHAN, 'DD-MM-YYYY') TGL_NIKAH,UF.NAMA_UPLOAD_FILE FILES
                    FROM AUCC.SEJARAH_PERNIKAHAN D
                    LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_pernikahan_'||D.ID_SEJARAH_PERNIKAHAN||'%')
                    WHERE D.ID_PENGGUNA='{$id_pgg}'
                ) 
                order by tgl_nikah desc");
        $smarty->assign('NIKAH', $pernikahan);

        $sej_struk = getData("
		select * from
		(
		SELECT D.*,TO_CHAR(D.TGL_SK_SEJ_JAB_STRUKTURAL, 'DD-MM-YYYY') TGL_SK,TO_CHAR(D.TMT_SEJ_JAB_STRUKTURAL, 'DD-MM-YYYY') TGL_TMT,JS.NM_JABATAN_STRUKTURAL,UF.NAMA_UPLOAD_FILE FILES
                FROM SEJARAH_JABATAN_STRUKTURAL D
                JOIN JABATAN_STRUKTURAL JS ON D.ID_JABATAN_STRUKTURAL=JS.ID_JABATAN_STRUKTURAL
                LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_tugas_tambahan_'||D.ID_JABATAN_STRUKTURAL||'%')
                WHERE D.ID_PENGGUNA='{$id_pgg}'
		) 
                order by TGL_SK desc");
        $smarty->assign('STRUKTURAL', $sej_struk);
        
        $sej_fung_umum = getData("
		select * from
		(
		SELECT D.*,TO_CHAR(D.TGL_SK_JABATAN_FUNGSIONAL, 'DD-MM-YYYY') TGL_SK,TO_CHAR(D.TMT_JABATAN_FUNGSIONAL, 'DD-MM-YYYY') TGL_TMT,JS.NAMA_JABATAN_FUNGIONAL_PEGAWAI,UF.NAMA_UPLOAD_FILE FILES,JK.NAMA_KATEGORI_JABATAN
                FROM SEJARAH_JABATAN_FUNGSIONAL_PEG D
                JOIN JABATAN_FUNGSIONAL_PEGAWAI JS ON D.ID_JABATAN_FUNGSIONAL_PEGAWAI=JS.ID_JABATAN_FUNGSIONAL_PEGAWAI
                JOIN JABATAN_FUNGSIONAL_KATEGORI JK ON JK.ID_JABATAN_FUNGSIONAL_KATEGORI=JS.ID_JABATAN_FUNGSIONAL_KATEGORI
                LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_jabatan_fungsional_pegawai_'||D.ID_SEJARAH_JABATAN_FUNGSIONAL||'%')
                WHERE D.ID_PENGGUNA='{$id_pgg}' AND JS.STATUS=1
		) 
                order by TGL_SK desc");
        $smarty->assign('FUNG_UMUM', $sej_fung_umum);
        
        $sej_fung_tertentu = getData("
		select * from
		(
		SELECT D.*,TO_CHAR(D.TGL_SK_JABATAN_FUNGSIONAL, 'DD-MM-YYYY') TGL_SK,TO_CHAR(D.TMT_JABATAN_FUNGSIONAL, 'DD-MM-YYYY') TGL_TMT,JS.NAMA_JABATAN_FUNGIONAL_PEGAWAI,UF.NAMA_UPLOAD_FILE FILES,JK.NAMA_KATEGORI_JABATAN
                FROM SEJARAH_JABATAN_FUNGSIONAL_PEG D
                JOIN JABATAN_FUNGSIONAL_PEGAWAI JS ON D.ID_JABATAN_FUNGSIONAL_PEGAWAI=JS.ID_JABATAN_FUNGSIONAL_PEGAWAI
                JOIN JABATAN_FUNGSIONAL_KATEGORI JK ON JK.ID_JABATAN_FUNGSIONAL_KATEGORI=JS.ID_JABATAN_FUNGSIONAL_KATEGORI
                LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_jabatan_fungsional_pegawai_'||D.ID_SEJARAH_JABATAN_FUNGSIONAL||'%')
                WHERE D.ID_PENGGUNA='{$id_pgg}' AND JS.STATUS=2
		) 
                order by TGL_SK desc");
        $smarty->assign('FUNG_TERTENTU', $sej_fung_tertentu);

        $diklat = getData("
		select * from
		(
		SELECT D.*,TO_CHAR(D.TGL_MULAI, 'DD-MM-YYYY') T_MULAI,TO_CHAR(D.TGL_SELESAI, 'DD-MM-YYYY') T_SELESAI,K.NM_KOTA,UF.NAMA_UPLOAD_FILE FILES
                FROM SEJARAH_DIKLAT D
                LEFT JOIN KOTA K ON D.ID_KOTA_LOKASI=K.ID_KOTA
                LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_diklat_'||D.ID_SEJARAH_DIKLAT||'%')
                WHERE D.ID_PENGGUNA='{$id_pgg}'
		) 
                order by TGL_SELESAI desc");
        $smarty->assign('DIKLAT', $diklat);

        $phg_dosen = getData("select phg.*,TO_CHAR(phg.tgl_perolehan, 'DD-MM-YYYY') as tgl_oleh,UF.NAMA_UPLOAD_FILE FILES
		from sejarah_penghargaan phg
                LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_penghargaan_'||phg.id_sejarah_penghargaan||'%')
		where phg.id_pengguna=$id_pgg 
                order by TGL_PEROLEHAN desc");
        $smarty->assign('PEHG', $phg_dosen);

        $org_dosen = getData("select org.*,TO_CHAR(org.tgl_mulai, 'DD-MM-YYYY') as t_mulai,TO_CHAR(org.tgl_selesai, 'DD-MM-YYYY') as t_selesai,UF.NAMA_UPLOAD_FILE FILES
		from sejarah_organisasi org
                LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_organisasi_'||org.id_sejarah_organisasi||'%')
		where org.id_pengguna=$id_pgg order by tgl_mulai desc, tgl_selesai desc");
        $smarty->assign('PORG', $org_dosen);

        $trg = getData("select keg.*,TO_CHAR(keg.tgl_mulai_kegiatan_pegawai, 'DD-MM-YYYY') as t_mulai,
                TO_CHAR(keg.tgl_selesai_kegiatan_pegawai, 'DD-MM-YYYY') as t_selesai,k.NM_KOTA,UF.NAMA_UPLOAD_FILE FILES
		from aucc.sejarah_kegiatan_pegawai keg
		left join aucc.kota k on keg.id_kota_kegiatan_pegawai=k.id_kota
                LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_workshop_seminar_training_'||keg.id_sejarah_kegiatan_pegawai||'%')
		where keg.id_pengguna=$id_pgg order by tgl_selesai_kegiatan_pegawai desc");
        $smarty->assign('PTRG', $trg);

        $hukuman = getData("select keg.*,TO_CHAR(keg.tgl_sk_hukuman, 'DD-MM-YYYY') as t_sk,TO_CHAR(keg.tmt_sk_hukuman, 'DD-MM-YYYY') as t_tmt,jh.*
		from sejarah_hukuman keg
		left join jenis_hukuman jh on keg.kode_hukuman=jh.kode_hukuman
		where keg.id_pengguna=$id_pgg order by t_sk desc");
        $smarty->assign('HUKUMAN', $hukuman);

        $dp3 = getData("select p.*,round(((kesetiaaan+kerjasama+prestasi_kerja+prakarsa+tanggung_jawab+kepemimpinan+ketaatan+kejujuran)/8),2) nilai_rata,UF.NAMA_UPLOAD_FILE FILES
		from pegawai_dp3 p
                LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_dp3_'||p.id_pegawai_dp3||'%')
		where p.id_pengguna=$id_pgg order by id_pegawai_dp3 desc");
        $smarty->assign('DP3', $dp3);

        $pkrj = getData("select keg.*,TO_CHAR(keg.tgl_sk_jabatan, 'DD-MM-YYYY') as t_sk,TO_CHAR(keg.tmt_jabatan, 'DD-MM-YYYY') as t_tmt,TO_CHAR(keg.tgl_mulai_jabatan, 'DD-MM-YYYY') as t_mulai
		from sejarah_pekerjaan keg
		where keg.id_pengguna=$id_pgg order by t_sk desc");
        $smarty->assign('PEKERJAAN', $pkrj);

        // DATA MASTER

        $id_gol = getData("select id_golongan, upper(nm_golongan) as nm_golongan from golongan order by nm_golongan desc");
        $smarty->assign('ID_GOL', $id_gol);

        $data_jab_fung = getData("
            SELECT JFP.*,JFK.NAMA_KATEGORI_JABATAN
            FROM JABATAN_FUNGSIONAL_PEGAWAI JFP
            JOIN JABATAN_FUNGSIONAL_KATEGORI JFK ON JFK.ID_JABATAN_FUNGSIONAL_KATEGORI=JFP.ID_JABATAN_FUNGSIONAL_KATEGORI
            ORDER BY ID_JABATAN_FUNGSIONAL_PEGAWAI
            ");
        $smarty->assign('DATA_JAB_FUNG', $data_jab_fung);

        $id_fsg = getData("select id_jabatan_fungsional, nm_jabatan_fungsional from jabatan_fungsional where id_jabatan_fungsional not in (5,6) order by id_jabatan_fungsional");
        $smarty->assign('ID_FSG', $id_fsg);


        $kode_hukuman = getData("SELECT * FROM JENIS_HUKUMAN ORDER BY ID_JENIS_HUKUMAN");
        $smarty->assign('kode_hukuman', $kode_hukuman);

        $id_pdd = getData("select urut, id_pendidikan_akhir, nama_pendidikan_akhir
		from (
		select id_pendidikan_akhir, nama_pendidikan_akhir,
		case when id_pendidikan_akhir=2 then 11 
		when id_pendidikan_akhir=1 then 12
		when id_pendidikan_akhir=10 then 13  
		when id_pendidikan_akhir=9 then 10 
		when id_pendidikan_akhir=8 then 9
		when id_pendidikan_akhir=7 then 8   
		when id_pendidikan_akhir=3 then 7  
		when id_pendidikan_akhir=4 then 6 
		when id_pendidikan_akhir=5 then 5 
		when id_pendidikan_akhir=6 then 4 
		when id_pendidikan_akhir=13 then 3 
		when id_pendidikan_akhir=12 then 2 
		when id_pendidikan_akhir=11 then 1 
		else 0 end as urut
		from pendidikan_akhir)
		order by urut desc");
        $smarty->assign('ID_PDD', $id_pdd);
    }
}

if ($request_method == 'POST') {
    /* UPDATE STATUS */
    if (post('action') == 'update_status_pdd') {
        $id_pendidikan = post('id_pendidikan');

        $id_pengguna = getvar("select distinct id_pengguna from sejarah_pendidikan where id_sejarah_pendidikan=$id_pendidikan");
        UpdateData("update sejarah_pendidikan set status_akhir=0, valid_sd=0 where id_pengguna='$id_pengguna[ID_PENGGUNA]'");
        UpdateData("update sejarah_pendidikan set status_akhir=1, valid_sd=1 where id_sejarah_pendidikan=$id_pendidikan");

        echo "1";
        exit();
    }

    if (post('action') == 'update_status_klgdr') {
        $id = post('valid');

        $keluarga_diri = getvar("select * from keluarga_diri where id_keluarga_diri=$id");
        if ($keluarga_diri['STATUS_VALID'] == 1) {
            UpdateData("update keluarga_diri set status_valid=0 where id_keluarga_diri='$id'");
        } else {
            UpdateData("update keluarga_diri set status_valid=1 where id_keluarga_diri='$id'");
        }

        echo "1";
        exit();
    }

    if (post('action') == 'update_status_klgpsg') {
        $id = post('valid');

        $keluarga_diri = getvar("select * from keluarga_pasangan where id_keluarga_pasangan=$id");
        if ($keluarga_diri['STATUS_VALID'] == 1) {
            UpdateData("update keluarga_pasangan set status_valid=0 where id_keluarga_pasangan='$id'");
        } else {
            UpdateData("update keluarga_pasangan set status_valid=1 where id_keluarga_pasangan='$id'");
        }

        echo "1";
        exit();
    }

    if (post('action') == 'update_status_nikah') {
        $id = post('valid');

        $keluarga_diri = getvar("select * from sejarah_pernikahan where id_sejarah_pernikahan=$id");
        if ($keluarga_diri['STATUS_VALID'] == 1) {
            UpdateData("update sejarah_pernikahan set status_valid=0 where id_sejarah_pernikahan='$id'");
        } else {
            UpdateData("update sejarah_pernikahan set status_valid=1 where id_sejarah_pernikahan='$id'");
        }

        echo "1";
        exit();
    }

    if (post('action') == 'update_status_tambahan') {
        $id = post('valid');

        $keluarga_diri = getvar("select * from sejarah_jabatan_struktural where id_sejarah_jabatan_struktural=$id");
        if ($keluarga_diri['VALID_SD'] == 1) {
            UpdateData("update sejarah_jabatan_struktural set valid_sd=0 where id_sejarah_jabatan_struktural='$id'");
        } else {
            UpdateData("update sejarah_jabatan_struktural set valid_sd=1 where id_sejarah_jabatan_struktural='$id'");
        }

        echo "1";
        exit();
    }
    
    if (post('action') == 'update_status_fung_umum') {
        $id = post('valid');

        $keluarga_diri = getvar("select * from sejarah_jabatan_fungsional_peg where id_sejarah_jabatan_fungsional=$id");
        if ($keluarga_diri['VALID_SD'] == 1) {
            UpdateData("update sejarah_jabatan_fungsional_peg set valid_sd=0 where id_sejarah_jabatan_fungsional='$id'");
        } else {
            UpdateData("update sejarah_jabatan_fungsional_peg set valid_sd=1 where id_sejarah_jabatan_fungsional='$id'");
        }

        echo "1";
        exit();
    }
    
    if (post('action') == 'update_status_fung_tertentu') {
        $id = post('valid');

        $keluarga_diri = getvar("select * from sejarah_jabatan_fungsional_peg where id_sejarah_jabatan_fungsional=$id");
        if ($keluarga_diri['VALID_SD'] == 1) {
            UpdateData("update sejarah_jabatan_fungsional_peg set valid_sd=0 where id_sejarah_jabatan_fungsional='$id'");
        } else {
            UpdateData("update sejarah_jabatan_fungsional_peg set valid_sd=1 where id_sejarah_jabatan_fungsional='$id'");
        }

        echo "1";
        exit();
    }

    if (post('action') == 'update_status_diklat') {
        $id = post('valid');

        $keluarga_diri = getvar("select * from sejarah_diklat where id_sejarah_diklat=$id");
        if ($keluarga_diri['STATUS_VALID'] == 1) {
            UpdateData("update sejarah_diklat set status_valid=0 where id_sejarah_diklat='$id'");
        } else {
            UpdateData("update sejarah_diklat set status_valid=1 where id_sejarah_diklat='$id'");
        }

        echo "1";
        exit();
    }

    if (post('action') == 'update_status_penghar') {
        $id = post('valid');

        $keluarga_diri = getvar("select * from sejarah_penghargaan where id_sejarah_penghargaan=$id");
        if ($keluarga_diri['STATUS_VALID'] == 1) {
            UpdateData("update sejarah_penghargaan set status_valid=0 where id_sejarah_penghargaan='$id'");
        } else {
            UpdateData("update sejarah_penghargaan set status_valid=1 where id_sejarah_penghargaan='$id'");
        }

        echo "1";
        exit();
    }

    if (post('action') == 'update_status_organ') {
        $id = post('valid');

        $keluarga_diri = getvar("select * from sejarah_organisasi where id_sejarah_organisasi=$id");
        if ($keluarga_diri['STATUS_VALID'] == 1) {
            UpdateData("update sejarah_organisasi set status_valid=0 where id_sejarah_organisasi='$id'");
        } else {
            UpdateData("update sejarah_organisasi set status_valid=1 where id_sejarah_organisasi='$id'");
        }

        echo "1";
        exit();
    }

    if (post('action') == 'update_status_kegpeg') {
        $id = post('valid');

        $keluarga_diri = getvar("select * from sejarah_kegiatan_pegawai where id_sejarah_kegiatan_pegawai=$id");
        if ($keluarga_diri['STATUS_VALID'] == 1) {
            UpdateData("update sejarah_kegiatan_pegawai set status_valid=0 where id_sejarah_kegiatan_pegawai='$id'");
        } else {
            UpdateData("update sejarah_kegiatan_pegawai set status_valid=1 where id_sejarah_kegiatan_pegawai='$id'");
        }

        echo "1";
        exit();
    }

    if (post('action') == 'update_status_hukum') {
        $id = post('valid');

        $keluarga_diri = getvar("select * from sejarah_hukuman where id_sejarah_hukuman=$id");
        if ($keluarga_diri['STATUS_VALID'] == 1) {
            UpdateData("update sejarah_hukuman set status_valid=0 where id_sejarah_hukuman='$id'");
        } else {
            UpdateData("update sejarah_hukuman set status_valid=1 where id_sejarah_hukuman='$id'");
        }

        echo "1";
        exit();
    }

    if (post('action') == 'update_status_dp3') {
        $id = post('valid');

        $keluarga_diri = getvar("select * from pegawai_dp3 where id_pegawai_dp3=$id");
        if ($keluarga_diri['STATUS_VALID'] == 1) {
            UpdateData("update pegawai_dp3 set status_valid=0 where id_pegawai_dp3='$id'");
        } else {
            UpdateData("update pegawai_dp3 set status_valid=1 where id_pegawai_dp3='$id'");
        }

        echo "1";
        exit();
    }

    if (post('action') == 'update_status_pekerjaan') {
        $id = post('valid');

        $keluarga_diri = getvar("select * from sejarah_pekerjaan where id_sejarah_pekerjaan=$id");
        if ($keluarga_diri['STATUS_VALID'] == 1) {
            UpdateData("update sejarah_pekerjaan set status_valid=0 where id_sejarah_pekerjaan='$id'");
        } else {
            UpdateData("update sejarah_pekerjaan set status_valid=1 where id_sejarah_pekerjaan='$id'");
        }

        echo "1";
        exit();
    }

    if (post('action') == 'update_status_gol') {
        $id_golongan = post('id_golongan');

        $id_pengguna = getvar("select distinct id_pengguna from sejarah_golongan where id_sejarah_golongan=$id_golongan");
        UpdateData("update sejarah_golongan set status_akhir=0, valid_sd=0 where id_pengguna='$id_pengguna[ID_PENGGUNA]'");
        UpdateData("update sejarah_golongan set status_akhir=1, valid_sd=1 where id_sejarah_golongan=$id_golongan");

        $golongan = getvar("select id_golongan from sejarah_golongan where id_sejarah_golongan=$id_golongan");
        UpdateData("update dosen set id_golongan='$golongan[ID_GOLONGAN]' where id_pengguna='$id_pengguna[ID_PENGGUNA]'");

        echo "1";
        exit();
    }

    if (post('action') == 'update_status_fsg') {
        $id_fungsional = post('id_fungsional');

        $id_pengguna = getvar("select distinct id_pengguna from sejarah_jabatan_fungsional where id_sejarah_jabatan_fungsional=$id_fungsional");
        UpdateData("update sejarah_jabatan_fungsional set status_akhir=0, valid_sd=0 where id_pengguna='$id_pengguna[ID_PENGGUNA]'");
        UpdateData("update sejarah_jabatan_fungsional set status_akhir=1, valid_sd=1 where id_sejarah_jabatan_fungsional=$id_fungsional");

        $fungsional = getvar("select id_jabatan_fungsional from sejarah_jabatan_fungsional where id_sejarah_jabatan_fungsional=$id_fungsional");
        UpdateData("update dosen set id_jabatan_fungsional='$fungsional[ID_JABATAN_FUNGSIONAL]' where id_pengguna='$id_pengguna[ID_PENGGUNA]'");

        echo "1";
        exit();
    }


    /* DELETE DATA */
    if (post('action') == 'delete_pdd') {
        $id_pendidikan = post('hapus');

        deleteData("delete from sejarah_pendidikan where id_sejarah_pendidikan=$id_pendidikan");

        echo "1";
        exit();
    }

    if (post('action') == 'delete_klgdr') {
        $id = post('hapus');

        deleteData("delete from keluarga_diri where id_keluarga_diri=$id");

        echo "1";
        exit();
    }

    if (post('action') == 'delete_klgpsg') {
        $id = post('hapus');

        deleteData("delete from keluarga_pasangan where id_keluarga_pasangan=$id");

        echo "1";
        exit();
    }

    if (post('action') == 'delete_klgank') {
        $id = post('hapus');

        deleteData("delete from keluarga_anak where id_keluarga_anak=$id");

        echo "1";
        exit();
    }

    if (post('action') == 'delete_nikah') {
        $id = post('hapus');

        deleteData("delete from sejarah_pernikahan where id_sejarah_pernikahan=$id");

        echo "1";
        exit();
    }

    if (post('action') == 'delete_tambahan') {
        $id = post('hapus');

        deleteData("delete from sejarah_jabatan_struktural where id_sejarah_jabatan_struktural=$id");

        echo "1";
        exit();
    }
    
    if (post('action') == 'delete_fung_umum') {
        $id = post('hapus');

        deleteData("delete from sejarah_jabatan_fungsional_peg where id_sejarah_jabatan_fungsional=$id");

        echo "1";
        exit();
    }
    
    if (post('action') == 'delete_fung_tertentu') {
        $id = post('hapus');

        deleteData("delete from sejarah_jabatan_fungsional_peg where id_sejarah_jabatan_fungsional=$id");

        echo "1";
        exit();
    }

    if (post('action') == 'delete_gol') {
        $id_golongan = post('hapus');

        deleteData("delete from sejarah_golongan where id_sejarah_golongan=$id_golongan");

        echo "1";
        exit();
    }



    if (post('action') == 'delete_fsg') {
        $id_fungsional = post('hapus');

        deleteData("delete from sejarah_jabatan_fungsional where id_sejarah_jabatan_fungsional=$id_fungsional");

        echo "1";
        exit();
    }

    if (post('action') == 'delete_diklat') {
        $id = post('hapus');

        deleteData("delete from sejarah_diklat where id_sejarah_diklat=$id");

        echo "1";
        exit();
    }

    if (post('action') == 'delete_penghar') {
        $id = post('hapus');

        deleteData("delete from sejarah_penghargaan where id_sejarah_penghargaan=$id");

        echo "1";
        exit();
    }

    if (post('action') == 'delete_organ') {
        $id = post('hapus');

        deleteData("delete from sejarah_organisasi where id_sejarah_organisasi=$id");

        echo "1";
        exit();
    }

    if (post('action') == 'delete_kegpeg') {
        $id = post('hapus');

        deleteData("delete from sejarah_kegiatan_pegawai where id_sejarah_kegiatan_pegawai=$id");

        echo "1";
        exit();
    }

    if (post('action') == 'delete_hukum') {
        $id = post('hapus');

        deleteData("delete from sejarah_hukuman where id_sejarah_hukuman=$id");

        echo "1";
        exit();
    }

    if (post('action') == 'delete_dp3') {
        $id = post('hapus');

        deleteData("delete from pegawai_dp3 where id_pegawai_dp3=$id");

        echo "1";
        exit();
    }

    if (post('action') == 'delete_pekerjaan') {
        $id = post('hapus');

        deleteData("delete from sejarah_pekerjaan where id_sejarah_pekerjaan=$id");

        echo "1";
        exit();
    }

    /* UPDATE DATA */

    if (post('action') == 'edit_pdd') {
        $id_sej = post('id_sej');
        $id_pdd = post('id_pdd');
        $nm_skolah = post('nm_skolah');
        $jur_skolah = post('jur_skolah');
        $tpt_skolah = post('tpt_skolah');
        $lls_skolah = post('pddYear');

        UpdateData("update sejarah_pendidikan set id_pendidikan_akhir=$id_pdd, nm_sekolah_pendidikan=trim(upper('" . $nm_skolah . "')), nm_jurusan_pendidikan=trim(upper('" . $jur_skolah . "')), 
		tempat_pendidikan=trim(upper('" . $nm_skolah . "')), tahun_lulus_pendidikan='" . $lls_skolah . "'
		where id_sejarah_pendidikan=$id_sej");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }

    if (post('action') == 'edit_gol') {
        $id_sej = post('id_sej');
        $id_gol = post('id_gol');
        $no_sk = post('no_sk');
        $asal_sk = post('asal_sk');
        $ket_sk = post('ket_sk');
        $tmt_sej = post('tmt_sej');
        $tgl_sej = post('tgl_sej');
        $ttd_sk = post('ttd_sk');

        UpdateData("update sejarah_golongan set id_golongan=$id_gol, no_sk_sejarah_golongan=trim(upper('" . $no_sk . "')), asal_sk_sejarah_golongan=trim(upper('" . $asal_sk . "')), 
		keterangan_sk_sejarah_golongan=trim(upper('" . $ket_sk . "')), tmt_sejarah_golongan=to_date('" . $tmt_sej . "','DD-MM-YYYY'), tgl_sk_sejarah_golongan=to_date('" . $tgl_sej . "','DD-MM-YYYY'),
		ttd_sk_nama_pejabat=trim(upper('" . $ttd_sk . "'))
		where id_sejarah_golongan=$id_sej");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }

    if (post('action') == 'edit_fsg') {
        $id_sej = post('id_sej');
        $id_fsg = post('id_fsg');
        $no_sk = post('no_sk');
        $asal_sk = post('asal_sk');
        $ket_sk = post('ket_sk');
        $tmt_sej = post('tmt_sej');
        $tgl_sej = post('tgl_sej');
        $ttd_sk = post('ttd_sk');

        UpdateData("update sejarah_jabatan_fungsional set id_jabatan_fungsional=$id_fsg, no_sk_sej_jab_fungsional=trim(upper('" . $no_sk . "')), asal_sk_sej_jab_fungsional=trim(upper('" . $asal_sk . "')), 
		ket_sk_sej_jab_fungsional=trim(upper('" . $ket_sk . "')), tmt_sej_jab_fungsional=to_date('" . $tmt_sej . "','DD-MM-YYYY'), tgl_sk_sej_jab_fungsional=to_date('" . $tgl_sej . "','DD-MM-YYYY'),
		ttd_sk_sej_jab_fungsional=trim(upper('" . $ttd_sk . "'))
		where id_sejarah_jabatan_fungsional=$id_sej");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }

    if (post('action') == 'edit_klgdr') {
        $id_klgdr = post('id_klgdr');
        $nama = post('nama');
        $hubungan = post('hubungan');
        $pekerjaan = post('pekerjaan');
        $tgl_lahir = post('tgl_lahir');
        $kota_lahir = post('tmpat_lahir');
        $kondisi = post('kondisi');
        $kelamin = post('kelamin');

        UpdateData("update keluarga_diri set 
                nama_keluarga=upper('{$nama}'), 
                hubungan_keluarga='{$hubungan}', 
		pekerjaan_keluarga='{$pekerjaan}', 
                tgl_lahir_keluarga=to_date('{$tgl_lahir}','DD-MM-YYYY'),
                id_kota_lahir='{$kota_lahir}', 
                kelamin_keluarga='{$kelamin}', 
                kondisi='{$kondisi}'
		where id_keluarga_diri='{$id_klgdr}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }

    if (post('action') == 'edit_klgpsg') {
        $id_klgpsg = post('id_klgpsg');
        $nama = post('nama');
        $hubungan = post('hubungan');
        $pekerjaan = post('pekerjaan');
        $tgl_lahir = post('tgl_lahir');
        $kota_lahir = post('tmpat_lahir');
        $kondisi = post('kondisi');
        $kelamin = post('kelamin');

        UpdateData("update keluarga_pasangan set 
                nama_pasangan=upper('{$nama}'), 
                hubungan_pasangan='{$hubungan}', 
		pekerjaan_pasangan='{$pekerjaan}', 
                tgl_lahir_pasangan=to_date('{$tgl_lahir}','DD-MM-YYYY'),
                id_kota_lahir='{$kota_lahir}', 
                kelamin_pasangan='{$kelamin}', 
                kondisi='{$kondisi}'
		where id_keluarga_pasangan='{$id_klgpsg}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }

    if (post('action') == 'edit_klgank') {
        $id_klgank = post('id_klgank');
        $nama = post('nama');
        $status = post('status');
        $pekerjaan = post('pekerjaan');
        $tgl_lahir = post('tgl_lahir');
        $kota_lahir = post('tmpat_lahir');
        $pendidikan = post('pendidikan');
        $kelamin = post('kelamin');

        UpdateData("update keluarga_anak set 
                nama_anak=upper('{$nama}'), 
                status_anak='{$status}', 
		pekerjaan_anak='{$pekerjaan}', 
                tgl_lahir_anak=to_date('{$tgl_lahir}','DD-MM-YYYY'),
                tempat_lahir_anak='{$kota_lahir}', 
                kelamin_anak='{$kelamin}', 
                id_pendidikan_akhir='{$pendidikan}'
		where id_keluarga_anak='{$id_klgank}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }

    if (post('action') == 'edit_nikah') {
        $id = post('id_nikah');
        $id_pengguna = post('id_pengguna');
        $nama_pasangan = post('nama');
        $nomor = post('karsisu');
        $tgl_lahir_psg = post('tgl_lhr_psg');
        $tgl_nikah = post('tgl_nikah');
        $pendidikan = post('pendidikan_psg');
        $pekerjaan = post('pekerjaan_psg');
        $status = post('status_psg');

        UpdateData("update sejarah_pernikahan set 
                nama_pasangan=upper('{$nama_pasangan}'), 
                nomor_karsisu='{$nomor}', 
		tgl_lahir_pasangan=to_date('{$tgl_lahir_psg}','DD-MM-YYYY'),
                tgl_pernikahan=to_date('{$tgl_nikah}','DD-MM-YYYY'),
                id_pendidikan_pasangan='{$pendidikan}', 
                status_pasangan='{$status}', 
                pekerjaan_pasangan='{$pekerjaan}'
		where id_sejarah_pernikahan='{$id}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }

    if (post('action') == 'edit_tambahan') {
        $id = post('id_tambahan');
        $id_pengguna = post('id_pengguna');
        $jabatan = post('jabatan');
        $nomor = post('nomor');
        $tgl_sk = post('tgl_sk');
        $tgl_tmt = post('tgl_tmt');
        $asal = post('asal');
        $keterangan = post('keterangan');
        $ttd = post('ttd');

        UpdateData("update sejarah_jabatan_struktural set 
                id_jabatan_struktural='{$jabatan}', 
                no_sk_sej_jab_struktural='{$nomor}', 
                asal_sk_sej_jab_struktural='{$asal}', 
                ket_sk_sej_jab_struktural='{$keterangan}', 
		tmt_sej_jab_struktural=to_date('{$tgl_tmt}','DD-MM-YYYY'),
                tgl_sk_sej_jab_struktural=to_date('{$tgl_sk}','DD-MM-YYYY'),
                ttd_sk_sej_jab_struktural='{$ttd}'
		where id_sejarah_jabatan_struktural='{$id}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }
    
    if (post('action') == 'edit_fung_umum') {
        $id = post('id_fung_umum');
        $id_pengguna = post('id_pengguna');
        $jabatan = post('jabatan');
        $nomor = post('nomor');
        $tgl_sk = post('tgl_sk');
        $tgl_tmt = post('tgl_tmt');
        $asal = post('asal');

        UpdateData("update sejarah_jabatan_fungsional_peg set 
                id_jabatan_fungsional_pegawai='{$jabatan}', 
                no_sk_jabatan_fungsional='{$nomor}', 
                asal_sk_jabatan_fungsional='{$asal}', 
                tmt_jabatan_fungsional=to_date('{$tgl_tmt}','DD-MM-YYYY'),
                tgl_sk_jabatan_fungsional=to_date('{$tgl_sk}','DD-MM-YYYY')
                where id_sejarah_jabatan_fungsional='{$id}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }
    
    if (post('action') == 'edit_fung_tertentu') {
        $id = post('id_fung_tertentu');
        $id_pengguna = post('id_pengguna');
        $jabatan = post('jabatan');
        $nomor = post('nomor');
        $tgl_sk = post('tgl_sk');
        $tgl_tmt = post('tgl_tmt');
        $asal = post('asal');

        UpdateData("update sejarah_jabatan_fungsional_peg set 
                id_jabatan_fungsional_pegawai='{$jabatan}', 
                no_sk_jabatan_fungsional='{$nomor}', 
                asal_sk_jabatan_fungsional='{$asal}', 
                tmt_jabatan_fungsional=to_date('{$tgl_tmt}','DD-MM-YYYY'),
                tgl_sk_jabatan_fungsional=to_date('{$tgl_sk}','DD-MM-YYYY')
                where id_sejarah_jabatan_fungsional='{$id}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }


    if (post('action') == 'edit_diklat') {
        $id_diklat = post('id_diklat');
        $nama = post('nama');
        $status_luar = post('status_luar');
        $lokasi = post('lokasi');
        $kota_lokasi = post('kota_lokasi');
        $tgl_mulai = post('tgl_mulai');
        $tgl_selesai = post('tgl_selesai');
        $jumlah_jam = post('jumlah_jam');
        $penyelenggara = post('penyelenggara');
        $jenis = post('jenis');
        $keterangan = post('keterangan');
        $tahun = post('tahun');
        $predikat = post('predikat');
        $tingkat = post('tingkat');

        InsertData("update sejarah_diklat set 
                nama_diklat=upper('{$nama}'), 
                status_luar_negeri='{$status_luar}', 
		lokasi='{$lokasi}', 
                id_kota_lokasi='{$kota_lokasi}', 
                tgl_mulai=to_date('{$tgl_mulai}','DD-MM-YYYY'),
                tgl_selesai=to_date('{$tgl_selesai}','DD-MM-YYYY'),
                jumlah_jam='{$jumlah_jam}', 
                penyelenggara='{$penyelenggara}', 
                jenis_diklat='{$jenis}', 
                keterangan_diklat='{$keterangan}', 
                tahun_angkatan='{$tahun}', 
                predikat='{$predikat}', 
                tingkat_diklat='{$tingkat}'
		where id_sejarah_diklat='{$id_diklat}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }

    if (post('action') == 'edit_penghar') {
        $id_penghar = post('id_penghar');
        $nama = post('nama');
        $nomor = post('nomor');
        $bidang = post('bidang');
        $bentuk = post('bentuk');
        $tgl_perolehan = post('tgl_perolehan');
        $negara = post('negara');
        $pemberi = post('pemberi');
        $intansi = post('intansi');
        $jabatan = post('jabatan');
        $tingkat = post('tingkat');

        InsertData("update sejarah_penghargaan set 
                nama_penghargaan=upper('{$nama}'), 
                nomor_penghargaan='{$nomor}', 
		bidang_penghargaan='{$bidang}', 
                bentuk_penghargaan='{$bentuk}', 
                tgl_perolehan=to_date('{$tgl_perolehan}','DD-MM-YYYY'),
                id_negara_pemberi='{$negara}', 
                pemberi_penghargaan='{$pemberi}', 
                instansi_pemberi='{$intansi}', 
                jabatan_pemberi='{$jabatan}', 
                tingkat_penghargaan='{$tingkat}'
		where id_sejarah_penghargaan='{$id_penghar}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }

    if (post('action') == 'edit_organ') {
        $id_organ = post('id_organ');
        $nama = post('nama');
        $kedudukan = post('kedudukan');
        $tgl_mulai = post('tgl_mulai');
        $tgl_selesai = post('tgl_selesai');
        $no_sk = post('no_sk');
        $jabatan_pemberi = post('jabatan_pemberi');
        $tingkat = post('tingkat');

        InsertData("update sejarah_organisasi set 
                nama_organisasi=upper('{$nama}'), 
                kedudukan_organisasi='{$kedudukan}', 
		tgl_mulai=to_date('{$tgl_mulai}','DD-MM-YYYY'),
                tgl_selesai=to_date('{$tgl_selesai}','DD-MM-YYYY'),
                no_sk_organisasi='{$no_sk}', 
                jabatan_sk_organisasi='{$jabatan_pemberi}', 
                tingkat_organisasi='{$tingkat}'
		where id_sejarah_organisasi='{$id_organ}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }

    if (post('action') == 'edit_kegpeg') {
        $id_kegpeg = post('id_kegpeg');
        $nama = post('nama');
        $lokasi = post('lokasi');
        $status_luar = post('status_luar');
        $kota_lokasi = post('kota_lokasi');
        $tgl_mulai = post('tgl_mulai');
        $tgl_selesai = post('tgl_selesai');
        $penyelenggara = post('penyelenggara');
        $jenis = post('jenis');
        $kedudukan = post('kedudukan');
        $tingkat = post('tingkat');

        InsertData("update sejarah_kegiatan_pegawai set 
                nama_kegiatan_pegawai=upper('{$nama}'), 
                lokasi_kegiatan_pegawai='{$lokasi}', 
		id_kota_kegiatan_pegawai='{$kota_lokasi}',
                kedudukan_kegiatan_pegawai='{$kedudukan}',
                penyelenggara_kegiatan_pegawai='{$penyelenggara}', 
                tgl_mulai_kegiatan_pegawai=to_date('{$tgl_mulai}','DD-MM-YYYY'), 
                tgl_selesai_kegiatan_pegawai=to_date('{$tgl_selesai}','DD-MM-YYYY'), 
                jenis_kegiatan_pegawai='{$jenis}', 
                tingkat_kegiatan_pegawai='{$jabatan_pemberi}', 
                status_luar_negeri='{$tingkat}'
		where id_sejarah_kegiatan_pegawai='{$id_kegpeg}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }

    if (post('action') == 'edit_hukum') {
        $id_hukum = post('id_hukum');
        $kode = post('kode');
        $no_sk = post('no_sk');
        $tgl_sk = post('tgl_sk');
        $tgl_tmt = post('tgl_tmt');
        $pejabat = post('pejabat');

        InsertData("update sejarah_hukuman set 
                kode_hukuman='{$kode}', 
                no_sk_hukuman='{$no_sk}', 
		tgl_sk_hukuman=to_date('{$tgl_sk}','DD-MM-YYYY'), 
                tmt_sk_hukuman=to_date('{$tgl_tmt}','DD-MM-YYYY'), 
                pejabat_sk_hukuman='{$pejabat}'
		where id_sejarah_hukuman='{$id_hukum}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }

    if (post('action') == 'edit_dp3') {
        $id_dp3 = post('id_dp3');
        $nip_penilai = post('nip_penilai');
        $nip_lama_penilai = post('nip_lama_penilai');
        $jabatan_penilai = post('jabatan_penilai');
        $unit_penilai = post('unit_penilai');
        $nip_atasan = post('nip_atasan');
        $jabatan_atasan = post('jabatan_atasan');
        $unit_atasan = post('unit_atasan');
        $kesetiaan = post('kesetiaan');
        $kerjasama = post('kerjasama');
        $prestasi_kerja = post('prestasi');
        $prakarsa = post('prakarsa');
        $tanggung_jawab = post('tanggung_jawab');
        $kepemimpinan = post('kepemimpinan');
        $ketaatan = post('ketaatan');
        $kejujuran = post('kejujuran');

        InsertData("update pegawai_dp3 set 
                nip_pjbt_penilai='{$nip_penilai}', 
                nip_lama_pjbt_penilai='{$nip_lama_penilai}', 
                jabatan_pjbt_penilai='{$jabatan_penilai}',
                unit_pjbt_penilai='{$unit_penilai}',
                nip_pjbt_atasan='{$nip_atasan}',
                jabatan_pjbt_atasan='{$jabatan_atasan}',
                unit_pjbt_atasan='{$unit_atasan}',
                kesetiaaan='{$kesetiaan}',
                kerjasama='{$kerjasama}',
                prestasi_kerja='{$prestasi_kerja}',
                prakarsa='{$prakarsa}',
                tanggung_jawab='{$tanggung_jawab}',
                kepemimpinan='{$kepemimpinan}',
                ketaatan='{$ketaatan}', 
                kejujuran='{$kejujuran}'
		where id_pegawai_dp3='{$id_dp3}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }

    if (post('action') == 'edit_pekerjaan') {
        $id_pekerjaan = post('id_pekerjaan');
        $nama = post('nama');
        $intansi = post('intansi');
        $no_sk = post('no_sk');
        $tgl_sk = post('tgl_sk');
        $tgl_mulai = post('tgl_mulai');
        $tgl_tmt = post('tgl_tmt');
        $nip = post('nip');
        $nip_lama = post('nip_lama');
        $pejabat = post('pejabat');

        InsertData("update sejarah_pekerjaan set 
                nama_jabatan='{$nama}', 
                instansi='{$intansi}', 
                no_sk_jabatan='{$no_sk}',
                tgl_sk_jabatan=to_date('{$tgl_sk}','DD-MM-YYYY'), 
                tgl_mulai_jabatan=to_date('{$tgl_mulai}','DD-MM-YYYY'), 
                tmt_jabatan=to_date('{$tgl_tmt}','DD-MM-YYYY'), 
                nip_pjbt_sk='{$nip}',
                nip_lama_pjbt_sk='{$nip_lama}',
                pjbt_sk='{$pejabat}'
		where id_sejarah_pekerjaan='{$id_pekerjaan}'");

        echo '<script>location.href="javascript:history.go(-1)";</script>';
        exit();
    }
}

$negara = getData("SELECT * FROM NEGARA ORDER BY NM_NEGARA");
$smarty->assign('negara', $negara);

$smarty->display('karyawan_detail.tpl');
?>