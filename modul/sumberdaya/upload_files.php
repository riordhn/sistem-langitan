<?php

include 'common.php';

// security sementara terhadap no auth access ~sugenk.


include 'function-tampil-informasi.php';
include 'proses/upload.class.php';


$upload = new upload($db);


$id_jenis = get('id_jenis');
$id_pengguna = get('id_pengguna');
$id_riwayat = get('id_riwayat');
$jenis = $upload->GetJenisFile($id_jenis);


//$upload->ResetUpload($id_pengguna, $id_jenis);

$replace_jenis = strtolower(str_replace("/", "_", str_replace(" ", "_", $jenis)));
if (post('mode') == 'upload' && $user->ID_PENGGUNA != '') {

    $db->Query("SELECT * FROM UPLOAD_FOLDER WHERE ID_UPLOAD_FOLDER='5655'");
    $direktori = $db->FetchAssoc();
    $new_folder = '../..' . $direktori['PATH_UPLOAD_FOLDER'] . '/' . $direktori['NAMA_UPLOAD_FOLDER'] . '/' . $id_pengguna;
    $id_folder = $upload->GetIdFolderFile($id_pengguna);

    if (!is_dir($new_folder)) {
        // Buat Folder Baru
        mkdir($new_folder);
        chmod($new_folder, 0777);
    }

    if (empty($id_folder)) {
        // Input Folder Database
        $db->Query("
        INSERT INTO UPLOAD_FOLDER
            (ID_UPLOAD_SERVER,ID_PARENT_FOLDER,NAMA_UPLOAD_FOLDER,PATH_UPLOAD_FOLDER,KEDALAMAN,AKSES)
        VALUES
            ('2','{$direktori['ID_UPLOAD_FOLDER']}','{$id_pengguna}','','1','1')
        ");
        $id_folder = $upload->GetIdFolderFile($id_pengguna);
    }



    $allowed_exts = array("pdf", "jpeg", 'jpg');
    $allowed_type = array('application/pdf', 'image/jpeg', 'image/pjpeg');
    $error_upload = '';
    $success_upload = '';
    //Properties File
    $extension = end(explode(".", $_FILES["file"]["name"]));
    $type = $_FILES["file"]["type"];
    $name = $_FILES["file"]["name"];
    $tmp_name = $_FILES["file"]["tmp_name"];
    $size = $_FILES["file"]["size"];
    $error = $_FILES["file"]["error"];
    // Format Nama File

    $file_name = strtolower($replace_jenis . '_' . $id_riwayat . '.' . $extension);

    if ($id_riwayat != '' && $id_pengguna != '' && $id_jenis != '') {
        //Cek Extension dan Type
        if (in_array($extension, $allowed_exts) && in_array($type, $allowed_type)) {
            if ($error > 0) {
                $error_upload .= alert_error("Error file :" . $error);
            } else {
                // Simpan data fisik
                move_uploaded_file($tmp_name, "$new_folder/$file_name");
                // Menyimpan data file secara database
                $upload->InsertFileDatabase($id_folder, $id_pengguna, $file_name, $id_jenis, $jenis);
                $success_upload .= alert_success('File Berhasil di Upload, klik refresh data untuk melihat hasil');
            }
        } else {
            $error_upload .= alert_error("Format File {$name} tidak sesuai");
        }
    }

    $smarty->assign('success_upload', $success_upload);
    $smarty->assign('error_upload', $error_upload);
}

$smarty->assign('jenis', $jenis);
$smarty->assign('info_1', alert_success("Tipe Format File menggunakan PDF/JPG"));
$smarty->display('upload_files.tpl');

