<?php

if(isset($_POST['submit'])){

	include ("../../../config.php");
	require ("../includes/class.imageupload.php");

	$nama_singkat_pt = $nama_singkat;
	
	//$image = new ImageUloader($max_size, $max_width, $max_height, $upload_dir)
	$image = new ImageUploader(100, 200, 300, '../../../foto_pegawai/'.$nama_singkat_pt.'/');
	
	$image->setImage('file_photo');
	
	$errors = array();
	if(!$image->checkSize()){ //check image size
		$errors[] = "- Besar file terlalu besar. (Maks. 100KB)";
	}
	
	if(!$image->checkHeight()){ //check image height
		$errors[] = "- Ukuran tinggi file terlalu besar. (Maks. 300 px)";
	}
	
	if(!$image->checkWidth()){ //check image width
		$errors[] = "- Ukuran lebar file terlalu besar. (Maks. 200 px)";
	}
	
	if(!$image->checkExt()){ //check image extension
		$errors[] = "- Format file salah. (format harus jpg atau JPG)";
	}
	
	if(empty($errors)){
		$image->setImageName($_POST['photo']); //set image name
		$image->deleteExisting();
		$image->upload();
		
		echo '<script>alert("Photo berhasil di upload")</script>';
		
		echo '
		<html>
		<head>
		<title>Upload Photo</title>
		<link rel="stylesheet" type="text/css" href="../includes/iframe.css" />
		<script>
		function CloseWindow() {
		  timer=setTimeout("window.close()",100);
		  return true;
		}
		</script>
		</head>
		<body onload="CloseWindow();" onunload="window.opener.location.reload();">
		</body>
		</html>';
	} else {
		foreach ($errors as $msg){
			$display .= '\r\n'.$msg;
		}
		
		echo '<script>alert("Photo gagal di upload:'.$display.'")</script>';
		echo '<script>location.href="javascript:history.go(-1)";</script>';
	}
} else {

require ("../../../config.php");
if ($user->Role() == AUCC_ROLE_SDM) {
include ("../includes/encrypt.php");

$nama_singkat_pt = $nama_singkat;

$var = decode($_SERVER["REQUEST_URI"]);
$to = $var["yth"];
$id = $var["file"];

$filename="../../../foto_pegawai/".$nama_singkat_pt."/".$id.".JPG";
if (file_exists($filename)) {
	$photo = $filename;
} else {
	$photo = '../includes/images/unknown.png';
}

echo '
<html>
<head>
<title>Upload Photo</title>
<link rel="stylesheet" type="text/css" href="../includes/iframe.css" />
</head>
<body oncontextmenu="return false;">
<center>
<p>
<table>
	<tr>
		<td align="center">Upload photo untuk : '.$to.$photo.'</td>
	</tr>
	<tr>
		<td align="center">$photo<img src="'.$photo.'" border="0" width="160" /></td>
	</tr>
	<tr>
		<td align="center"><font color="grey">Besar file maks. 100KB.<br/>Lebar photo maks. 200px dan tinggi photo maks. 300px</font></td>
	</tr>
</table>
</p>
<form enctype="multipart/form-data" method="post" name="upload">
	<input name="photo" type="hidden" value="'.$id.'" />
	<input name="file_photo" type="file" /><br/><br/>
	<input type="submit" name="submit" value="Upload" />
</form>
</center>
</body>
</html>';

} else {
    header("location: /logout.php");
    exit();
}
}
?>