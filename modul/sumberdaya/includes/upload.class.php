<?php

class upload {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }
    
    function reset_password_pengguna($username){
        $this->db->Query("UPDATE PENGGUNA SET PASSWORD_PENGGUNA = USERNAME WHERE USERNAME='{$username}'");
    }

    function cek_pengguna($nik,$id_perguruan_tinggi) {
        $this->db->Query("SELECT * FROM PENGGUNA WHERE USERNAME='{$nik}' AND ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}'");
        return $this->db->FetchAssoc();
    }

    function cek_dosen_by_perguruan_tinggi($nik, $id_perguruan_tinggi) {
        $this->db->Query("SELECT * FROM DOSEN d
                            LEFT JOIN PENGGUNA p ON p.ID_PENGGUNA = d.ID_PENGGUNA AND p.ID_PERGURUAN_TINGGI = {$id_perguruan_tinggi}
                            WHERE d.NIP_DOSEN='{$nik}'");
        return $this->db->FetchAssoc();
    }

    function cek_karyawan_by_perguruan_tinggi($nik, $id_perguruan_tinggi) {
        $this->db->Query("SELECT * FROM PEGAWAI peg
                            LEFT JOIN PENGGUNA p ON p.ID_PENGGUNA = peg.ID_PENGGUNA AND p.ID_PERGURUAN_TINGGI = {$id_perguruan_tinggi}
                            WHERE peg.NIP_PEGAWAI='{$nik}'");
        return $this->db->FetchAssoc();
    }

    function cek_prodi($nama_prodi, $id_pt = 0) {
        $nama_prodi = trim($nama_prodi);
        $this->db->Query("SELECT PS.ID_PROGRAM_STUDI, PS.ID_FAKULTAS FROM PROGRAM_STUDI PS 
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS AND F.ID_PERGURUAN_TINGGI = {$id_pt}
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE (UPPER(J.NM_JENJANG) || ' ' || UPPER(PS.NM_PROGRAM_STUDI)) = UPPER('{$nama_prodi}')");
        return $this->db->FetchAssoc();
    }

    function cek_unit_kerja($nama_unit_kerja, $id_pt = 0) {
        $this->db->Query("SELECT ID_UNIT_KERJA FROM UNIT_KERJA
                            WHERE NM_UNIT_KERJA = '{$nama_unit_kerja}' AND ID_PERGURUAN_TINGGI = '{$id_pt}'");
        return $this->db->FetchAssoc();
    }

    function insert_pengguna_dosen($nama, $nik, $id_perguruan_tinggi) {
        $password_hash = sha1($nik);
        $this->db->Query("INSERT INTO PENGGUNA 
                (USERNAME,PASSWORD_HASH,NM_PENGGUNA,ID_ROLE,JOIN_TABLE,ID_PERGURUAN_TINGGI) 
                VALUES 
                ('{$nik}','{$password_hash}',UPPER('{$nama}'),4,2,'{$id_perguruan_tinggi}')");
    }

    function insert_pengguna_karyawan($nama, $nik, $id_perguruan_tinggi) {
        $password_hash = sha1($nik);
        $this->db->Query("INSERT INTO PENGGUNA 
                (USERNAME,PASSWORD_HASH,NM_PENGGUNA,ID_ROLE,JOIN_TABLE,ID_PERGURUAN_TINGGI) 
                VALUES 
                ('{$nik}','{$password_hash}',UPPER('{$nama}'),22,1,'{$id_perguruan_tinggi}')");
    }

    function update_pengguna_dosen($nama, $nik, $id_perguruan_tinggi) {
        $password_hash = sha1($nik);
        $this->db->Query("UPDATE PENGGUNA SET
                USERNAME = '{$nik}', PASSWORD_HASH = '{$password_hash}',NM_PENGGUNA = UPPER('{$nama}'),
                ID_ROLE = '4',JOIN_TABLE = '2'
                WHERE USERNAME = '{$nik}' AND ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}'");
    }

    function update_pengguna_karyawan($nama, $nik, $id_perguruan_tinggi) {
        $password_hash = sha1($nik);
        $this->db->Query("UPDATE PENGGUNA SET
                USERNAME = '{$nik}', PASSWORD_HASH = '{$password_hash}',NM_PENGGUNA = UPPER('{$nama}'),
                ID_ROLE = '22',JOIN_TABLE = '1'
                WHERE USERNAME = '{$nik}' AND ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}'");
    }

    function insert_dosen($id_pengguna, $nik, $nidn, $program_studi, $status) {
        //$nim_bank = substr($nim, 0, 16);
        $this->db->Query("INSERT INTO DOSEN 
                (ID_PENGGUNA,NIP_DOSEN,NIDN_DOSEN,ID_PROGRAM_STUDI,ID_STATUS_PENGGUNA,STATUS_DOSEN,ID_PROGRAM_STUDI_SD) 
                VALUES 
                ('{$id_pengguna}','{$nik}','{$nidn}','{$program_studi}','22','{$status}','{$program_studi}')");
    }

    function insert_karyawan($id_pengguna, $nik, $unit_kerja, $status) {
        //$nim_bank = substr($nim, 0, 16);
        $this->db->Query("INSERT INTO PEGAWAI 
                (ID_PENGGUNA,NIP_PEGAWAI,ID_GOLONGAN,ID_JABATAN_PEGAWAI,ID_UNIT_KERJA,STATUS_PEGAWAI,ID_STATUS_PENGGUNA,ID_UNIT_KERJA_SD) 
                VALUES 
                ('{$id_pengguna}','{$nik}','18','26','{$unit_kerja}','{$status}','46','{$unit_kerja}')");
    }

    function update_dosen($id_pengguna, $nik, $nidn, $program_studi, $status) {
        //$nim_bank = substr($nim, 0, 9);
        $this->db->Query("UPDATE DOSEN SET 
                ID_PENGGUNA = '{$id_pengguna}',NIP_DOSEN = '{$nik}',NIDN_DOSEN = '{$nidn}',ID_PROGRAM_STUDI = '{$program_studi}',
                STATUS_DOSEN = '{$status}',ID_PROGRAM_STUDI_SD = '{$program_studi}'
                WHERE ID_PENGGUNA = '{$id_pengguna}'");
    }

    function update_karyawan($id_pengguna, $nik, $unit_kerja, $status) {
        //$nim_bank = substr($nim, 0, 9);
        $this->db->Query("UPDATE PEGAWAI SET 
                ID_PENGGUNA = '{$id_pengguna}',NIP_PEGAWAI = '{$nik}',ID_UNIT_KERJA = '{$unit_kerja}',
                STATUS_PEGAWAI = '{$status}',ID_UNIT_KERJA_SD = '{$unit_kerja}'
                WHERE ID_PENGGUNA = '{$id_pengguna}'");
    }
	

    function update_pengguna($nama, $nim, $tgl_lahir, $id_pengguna, $id_pt = 0) {
        $this->db->Query("UPDATE PENGGUNA SET USERNAME = '{$nim}', NM_PENGGUNA =UPPER('$nama'), TGL_LAHIR_PENGGUNA = TO_DATE('$tgl_lahir', 'DD-MM-YYYY') 
		WHERE ID_PENGGUNA='{$id_pengguna}' AND ID_PERGURUAN_TINGGI = {$id_pt}");
    }


}

?>
