<?php
include('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_SDM){
	header("location: /logout.php");
    exit();
}

$location = explode("-", $_GET['location']);

foreach ($user->MODULs as $m)
{
    if ($m['NM_MODUL'] == $location[0])
    {
        $smarty->assign('nm_modul', $m['NM_MODUL']);
        $smarty->assign('menu_set', $m['MENUs']);
    }
}

$smarty->display('menu.tpl');
?>