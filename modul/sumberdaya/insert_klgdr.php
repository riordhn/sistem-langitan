<?php
include ('common.php');
require_once ('ociFunction.php');

// sementara, agar tidak edit satu-satu
/* FIKRIE */
if ($user->Role() == AUCC_ROLE_SDM OR $user->Role() == AUCC_ROLE_DOSEN) {
    $id = $_GET['id'];

    if (isset($_POST['submit'])) {
        $id_pengguna = $_POST['id_pengguna'];
        $nama_keluarga = $_POST['nama'];
        $hubungan = $_POST['hubungan'];
        $pekerjaan = $_POST['pekerjaan'];
        $tgl_lahir = $_POST['tgl_lahir'];
        $kota_lahir = $_POST['tmpat_lahir'];
        $kondisi = $_POST['kondisi'];
        $kelamin = $_POST['kelamin'];

        $result = InsertData("insert into keluarga_diri (id_pengguna, nama_keluarga, hubungan_keluarga,kelamin_keluarga, pekerjaan_keluarga, tgl_lahir_keluarga, id_kota_lahir,kondisi,status_valid) 
                    values ('{$id_pengguna}',upper('{$nama_keluarga}'),'{$hubungan}','{$kelamin}','{$pekerjaan}',to_date('{$tgl_lahir}','DD-MM-YYYY'),'{$kota_lahir}','{$kondisi}',1)");

        if (!$result)
        {
            echo '<script>alert("Data gagal ditambahkan")</script>';
        }
        else
        {
            echo '<script>alert("Data berhasil ditambahkan")</script>';
            echo '<script>window.parent.document.location.reload();</script>';    
        }
    }
    $negara = getData("SELECT * FROM NEGARA ORDER BY NM_NEGARA");
    $smarty->assign('negara',$negara);
    $smarty->display('insert/insert_klgdr.tpl');
} else {
    header("location: /logout.php");
    exit();
}
?>