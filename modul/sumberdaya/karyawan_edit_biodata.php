<?php
include ('common.php');
require_once ('ociFunction.php');
$id_perguruan_tinggi = $user->ID_PERGURUAN_TINGGI; 

if (isset($_GET['action']) == 'edit_biodata') {
    $id_peg = $_GET['id'];

    $id = getvar("select id_pengguna from pegawai where id_pegawai=$id_peg");
    $smarty->assign('ID', $id['ID_PENGGUNA']);

    $pegawai = getData("SELECT PEG.*, PGG.ID_KOTA_LAHIR,PEG.NIP_PEGAWAI AS USERNAME, UPPER(PEG.ALAMAT_PEGAWAI) AS ALAMAT_RUMAH_PEGAWAI, PEG.TELP_PEGAWAI, 
                    PEG.STATUS_PEGAWAI, UPPER(PGG.NM_PENGGUNA) AS NM_PENGGUNA, PGG.GELAR_DEPAN, PGG.GELAR_BELAKANG,
                    TO_CHAR(PGG.TGL_LAHIR_PENGGUNA, 'DD-MM-YYYY') AS TGL_LAHIR_PENGGUNA, PGG.KELAMIN_PENGGUNA, PGG.EMAIL_PENGGUNA, PGG.EMAIL_ALTERNATE,
                    UPPER(JAB.NM_JABATAN_PEGAWAI) AS NM_JABATAN_PEGAWAI,UPPER(UKR.NM_UNIT_KERJA) AS NM_UNIT_KERJA, 
                    TO_CHAR(GOL.TMT_SEJARAH_GOLONGAN, 'DD-MM-YYYY') TMT_GOLONGAN, upper(GOL.NM_GOLONGAN) as NM_GOLONGAN, UPPER(GOL.NM_PANGKAT) as NM_PANGKAT, 
                    UPPER(FSG.NM_JABATAN_FUNGSIONAL) as NM_JABATAN_FUNGSIONAL,TO_CHAR(FSG.TMT_SEJ_JAB_FUNGSIONAL, 'DD-MM-YYYY') TMT_JAB_FUNGSIONAL
                    , KT.TIPE_DATI2||' '||NM_KOTA as TEMPAT_LAHIR,UPPER(NM_JENJANG||' - '||NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI, 
                    UPPER(NM_FAKULTAS) AS NM_FAKULTAS ,AG.NM_AGAMA,SPEN.*,STRUK.*,SNIKAH.*,USEL.*, KT.*
                    FROM AUCC.PEGAWAI PEG
                    JOIN AUCC.PENGGUNA PGG ON PEG.ID_PENGGUNA=PGG.ID_PENGGUNA
                    LEFT JOIN AUCC.KOTA KT ON KT.ID_KOTA=PGG.ID_KOTA_LAHIR
                    LEFT JOIN AUCC.UNIT_KERJA UKR ON UKR.ID_UNIT_KERJA=PEG.ID_UNIT_KERJA_SD
                    LEFT JOIN AUCC.PROGRAM_STUDI PST ON UKR.ID_PROGRAM_STUDI=PST.ID_PROGRAM_STUDI
                    LEFT JOIN AUCC.JENJANG J ON J.ID_JENJANG=PST.ID_JENJANG
                    LEFT JOIN AUCC.FAKULTAS FAK ON UKR.ID_FAKULTAS=FAK.ID_FAKULTAS
                    LEFT JOIN AUCC.JABATAN_PEGAWAI JAB ON JAB.ID_JABATAN_PEGAWAI=PEG.ID_JABATAN_PEGAWAI
                    LEFT JOIN AUCC.AGAMA AG ON AG.ID_AGAMA=PGG.ID_AGAMA
                    LEFT JOIN AUCC.STATUS_PERNIKAHAN SNIKAH ON SNIKAH.ID_STATUS_PERNIKAHAN=PGG.ID_STATUS_PERNIKAHAN
                    LEFT JOIN AUCC.UNIT_ESSELON USEL ON USEL.ID_PENGGUNA=PGG.ID_PENGGUNA
                    LEFT JOIN (
                                SELECT * FROM (
                                    SELECT * FROM (
                                      SELECT G.NM_GOLONGAN,G.NM_PANGKAT,SJ.*
                                      FROM AUCC.SEJARAH_GOLONGAN SJ
                                      JOIN AUCC.GOLONGAN G ON SJ.ID_GOLONGAN=G.ID_GOLONGAN
                                      WHERE SJ.ID_PENGGUNA={$id['ID_PENGGUNA']}
                                      ORDER BY SJ.TMT_SEJARAH_GOLONGAN DESC
                                    ) WHERE ROWNUM=1
                                  )
                                ) GOL ON GOL.ID_PENGGUNA=PEG.ID_PENGGUNA
                    LEFT JOIN (
                        SELECT * FROM (
                            SELECT * FROM (
                              SELECT G.NM_JABATAN_FUNGSIONAL,SJ.*
                              FROM AUCC.SEJARAH_JABATAN_FUNGSIONAL SJ
                              JOIN AUCC.JABATAN_FUNGSIONAL G ON SJ.ID_JABATAN_FUNGSIONAL=G.ID_JABATAN_FUNGSIONAL
                              WHERE SJ.ID_PENGGUNA={$id['ID_PENGGUNA']}
                              ORDER BY SJ.TMT_SEJ_JAB_FUNGSIONAL DESC
                            ) WHERE ROWNUM=1
                      )
                    ) FSG ON FSG.ID_PENGGUNA=PGG.ID_PENGGUNA
                    LEFT JOIN (
                        SELECT * FROM (
                            SELECT * FROM (
                              SELECT G.NM_JABATAN_STRUKTURAL,SJ.*
                              FROM AUCC.SEJARAH_JABATAN_STRUKTURAL SJ
                              JOIN AUCC.JABATAN_STRUKTURAL G ON SJ.ID_JABATAN_STRUKTURAL=G.ID_JABATAN_STRUKTURAL
                              WHERE SJ.ID_PENGGUNA={$id['ID_PENGGUNA']}
                              ORDER BY SJ.TMT_SEJ_JAB_STRUKTURAL DESC
                            ) WHERE ROWNUM=1
                      )
                    ) STRUK ON STRUK.ID_PENGGUNA=PEG.ID_PENGGUNA
                    LEFT JOIN (
                        SELECT * FROM (
                            SELECT * FROM (
                              SELECT NAMA_PENDIDIKAN_AKHIR,SP.*
                              FROM AUCC.SEJARAH_PENDIDIKAN SP
                              JOIN AUCC.PENDIDIKAN_AKHIR PA ON PA.ID_PENDIDIKAN_AKHIR=SP.ID_PENDIDIKAN_AKHIR
                              WHERE SP.ID_PENGGUNA={$id['ID_PENGGUNA']}
                              ORDER BY SP.TAHUN_LULUS_PENDIDIKAN DESC,ID_SEJARAH_PENDIDIKAN DESC
                            ) WHERE ROWNUM=1
                      )
                    ) SPEN ON SPEN.ID_PENGGUNA=PEG.ID_PENGGUNA
                    WHERE PGG.ID_PENGGUNA={$id['ID_PENGGUNA']}");

    #print_r($pegawai);
    $smarty->assign('PEGAWAI', $pegawai);

    $get_photo = getvar("select nip_pegawai as photo from pegawai where id_pegawai=$id_peg");
    $filename = "../../foto_pegawai/" . $get_photo['PHOTO'] . ".JPG";
    if (file_exists($filename)) {
        $photo = $filename;
    } else {
        $photo = 'includes/images/unknown.png';
    }
    $smarty->assign('PHOTO', $photo);

    $sts_peg = getData("select distinct status_pegawai from pegawai where status_pegawai is not null order by status_pegawai");
    $smarty->assign('STS_PEG', $sts_peg);

    $sts_aktif = getData("select id_status_pengguna, upper(nm_status_pengguna) as nm_status_pengguna from status_pengguna where id_role=22 order by status_aktif desc, nm_status_pengguna");
    $smarty->assign('STS_AKTIF', $sts_aktif);

    $kd_uk = getData("SELECT id_unit_kerja, upper(nm_unit_kerja) as unitkerja,
		case when type_unit_kerja='REKTORAT' then 0 
		when type_unit_kerja='LEMBAGA' then 1 
		when type_unit_kerja='FAKULTAS' then 2 
                when type_unit_kerja='PRODI' then 3
                end as urut0,
		case when id_fakultas is null then 0 else id_fakultas end as urut1
		from unit_kerja where id_perguruan_tinggi = '{$id_perguruan_tinggi}' /* AND type_unit_kerja in ('REKTORAT', 'LEMBAGA', 'FAKULTAS','PRODI') */
		order by urut0, urut1, unitkerja");
    $smarty->assign('KD_UK', $kd_uk);

    $jk = getData("select distinct kelamin_pengguna, case when kelamin_pengguna='1' then 'LAKI-LAKI' else 'PEREMPUAN' end as nm_kelamin_pengguna from pengguna where kelamin_pengguna in ('1', '2') order by kelamin_pengguna");
    $smarty->assign('JK', $jk);

    $jab = getData("select id_jabatan_pegawai, upper(nm_jabatan_pegawai) as nm_jabatan_pegawai from jabatan_pegawai order by id_jabatan_pegawai");
    $smarty->assign('JAB', $jab);

    $agama = getData("select id_agama,nm_agama from agama order by id_agama");
    $smarty->assign('AGAMA', $agama);

    $nikah = getData("select * from status_pernikahan order by id_status_pernikahan");
    $smarty->assign('SNIKAH', $nikah);

    $data_kota = array();
    $provinsi = getData("SELECT * FROM PROVINSI WHERE ID_NEGARA=114 ORDER BY NM_PROVINSI");
    foreach ($provinsi as $data) {
        array_push($data_kota, array(
            'nama' => $data['NM_PROVINSI'],
            'kota' => getData("SELECT id_kota, tipe_dati2||' '||nm_kota as kota FROM KOTA WHERE ID_PROVINSI='{$data['ID_PROVINSI']}' ORDER BY TIPE_DATI2, NM_KOTA")
        ));
    }
    $smarty->assign('data_kota', $data_kota);
    $smarty->assign('count_provinsi', count($data_kota));

    $smarty->display('karyawan_edit_biodata.tpl');
}

if (isset($_POST['action']) == 'update') {
    $id_pegawai = $_POST['id_pegawai'];
    $id_pengguna = $_POST['id_pengguna'];

// tabel pengguna
    $username = $_POST['nip'];
    $gelar_depan = $_POST['gelar_dpn'];
    $gelar_belakang = $_POST['gelar_blkg'];
    $nm_pengguna = $_POST['nm_lengkap'];
    $id_kota_lahir = $_POST['id_kota_lahir'];
    $tgl_lahir_pengguna = $_POST['tgl_lahir'];
    $kelamin_pengguna = $_POST['jk'];
    $email_pengguna = $_POST['email'];
    $agama = $_POST['agama'];
    $snikah = $_POST['status_nikah'];

    gantidata("update pengguna set username=trim('" . $username . "'), gelar_depan=trim('" . $gelar_depan . "'), gelar_belakang=trim('" . $gelar_belakang . "'), nm_pengguna=trim(upper('" . $nm_pengguna . "')), id_kota_lahir=$id_kota_lahir, tgl_lahir_pengguna=to_date('" . $tgl_lahir_pengguna . "','DD-MM-YYYY'), kelamin_pengguna='" . $kelamin_pengguna . "',
        id_status_pernikahan='{$snikah}',id_agama='{$agama}'
	where id_pengguna=$id_pengguna");

    if( ! empty($_POST['email'])) {
        $email_alternate = $_POST['email'];

        if (!filter_var($email_alternate, FILTER_VALIDATE_EMAIL)) {
            echo '<script language="javascript">';
            echo 'alert("Email #2 Gagal Diupdate, Format salah!")';
            echo '</script>';
        }
        else {            
            gantidata("update pengguna set email_alternate=trim(lower('" . $email_alternate . "')) where id_pengguna=$id_pengguna");
        }
    }
    else {
        gantidata("update pengguna set email_alternate=null where id_pengguna=$id_pengguna");   
    }

// tabel pegawai
    $nip_pegawai = $_POST['nip'];
    $alamat_rumah_pegawai = $_POST['alamat'];
    $id_unit_kerja = $_POST['id_unit_kerja'];
    $status_pegawai = $_POST['status_peg'];
    $id_status_pengguna = $_POST['aktif'];
    $nip_lama = $_POST['nip_lama'];
    $prajab_nomor = $_POST['prajab_nomor'];
    $prajab_tanggal = $_POST['prajab_tanggal'];
    $tgl_sumpah_pns = $_POST['tgl_sumpah_pns'];
    $telp = $_POST['tlp'];
    $mobile = $_POST['hp'];
    $tmt_cpns = $_POST['tmt_cpns'];
    $nomor_karpeg = $_POST['nomor_karpeg'];
    $nomor_npwp = $_POST['nomor_npwp'];
    $taspen = $_POST['taspen'];
    $kode_pos = $_POST['kode_pos'];

    gantidata("update pegawai set nip_pegawai=trim('" . $nip_pegawai . "'), alamat_pegawai=trim(upper('" . $alamat_rumah_pegawai . "')), id_unit_kerja_sd='{$id_unit_kerja}', id_unit_kerja='{$id_unit_kerja}',
	status_pegawai='" . $status_pegawai . "', id_status_pengguna='{$id_status_pengguna}' ,nip_lama='" . $nip_lama . "',prajab_nomor='" . $prajab_nomor . "',
        prajab_tanggal=to_date('" . $prajab_tanggal . "','DD-MM-YYYY'),tgl_sumpah_pns=to_date('" . $tgl_sumpah_pns . "','DD-MM-YYYY'),telp_pegawai='{$telp}',mobile_pegawai='{$mobile}',
        nomer_karpeg='{$nomor_karpeg}',nomor_npwp='{$nomor_npwp}',taspen='{$taspen}', kode_pos = '{$kode_pos}'
	where id_pegawai='{$id_pegawai}'");

    

    // Tabel Unit Esselon
    $usel1 = $_POST['unit_esselon1'];
    $usel2 = $_POST['unit_esselon2'];
    $usel3 = $_POST['unit_esselon3'];
    $usel4 = $_POST['unit_esselon4'];
    $usel5 = $_POST['unit_esselon5'];
    $db->Query("SELECT * FROM UNIT_ESSELON WHERE ID_PENGGUNA='{$id_pengguna}'");
    $d_usel = $db->FetchAssoc();
    $id_usel = $d_usel['ID_UNIT_ESSELON'];
    if ($id_usel != '') {
        gantidata("UPDATE UNIT_ESSELON SET UNIT_ESSELON_I='{$usel1}',UNIT_ESSELON_II='{$usel2}',UNIT_ESSELON_III='{$usel3}',UNIT_ESSELON_IV='{$usel4}',UNIT_ESSELON_V='{$usel5}' WHERE ID_UNIT_ESSELON='{$id_usel}'");
    } else {
        InsertData("INSERT INTO UNIT_ESSELON (ID_PENGGUNA,UNIT_ESSELON_I,UNIT_ESSELON_II,UNIT_ESSELON_III,UNIT_ESSELON_IV,UNIT_ESSELON_V) VALUES ('{$id_pengguna}','{$usel1}','{$usel2}','{$usel3}','{$usel4}','{$usel5}')");
    }
    echo '<script>location.href="javascript:history.go(-1)";</script>';
}
?>