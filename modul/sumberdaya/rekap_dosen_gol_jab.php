<?php
//Yudi Sulistya, 20/04/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pt = $id_pt_user;

$kd_fak=getData("select id_fakultas, upper(nm_fakultas) as nm_fakultas from fakultas where id_perguruan_tinggi = {$id_pt} order by id_fakultas");
$smarty->assign('KD_FAK', $kd_fak);

if ($request_method == 'GET' or $request_method == 'POST')
{
		$display = get('id','');

		$fak=getData("select 'FAKULTAS '||upper(fak.nm_fakultas) as fakultas
		from fakultas fak
		where fak.id_fakultas='{$display}'");
		$smarty->assign('FAK', $fak);
		
		$ttl=getvar("select sum(A+B+C+D+E) as total
						from
						(
						select
						case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 1 else 0 end as A,
						case when dsn.id_jabatan_fungsional=1 then 1 else 0 end as B,
						case when dsn.id_jabatan_fungsional=2 then 1 else 0 end as C,
						case when dsn.id_jabatan_fungsional=3 then 1 else 0 end as D,
						case when dsn.id_jabatan_fungsional=4 then 1 else 0 end as E
						from dosen dsn
						left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
						left join golongan gol on gol.id_golongan=dsn.id_golongan
						where dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas like '$display')
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select id_golongan, upper(golongan) as golongan, sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 0 else dsn.id_jabatan_fungsional end as id_jabatan_fungsional,
						dsn.id_golongan, 
						case when dsn.id_golongan is not null then gol.nm_golongan else '-' end as golongan,
						case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 1 else 0 end as A,
						case when dsn.id_jabatan_fungsional=1 then 1 else 0 end as B,
						case when dsn.id_jabatan_fungsional=2 then 1 else 0 end as C,
						case when dsn.id_jabatan_fungsional=3 then 1 else 0 end as D,
						case when dsn.id_jabatan_fungsional=4 then 1 else 0 end as E
						from dosen dsn
						left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
						left join golongan gol on gol.id_golongan=dsn.id_golongan
						where dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas like '$display')
						)
						group by id_golongan, golongan
						order by golongan");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(A+B+C+D+E) as total
						from
						(
						select
						case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 1 else 0 end as A,
						case when dsn.id_jabatan_fungsional=1 then 1 else 0 end as B,
						case when dsn.id_jabatan_fungsional=2 then 1 else 0 end as C,
						case when dsn.id_jabatan_fungsional=3 then 1 else 0 end as D,
						case when dsn.id_jabatan_fungsional=4 then 1 else 0 end as E
						from dosen dsn
						left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
						left join golongan gol on gol.id_golongan=dsn.id_golongan
						where dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas like '$display')
						)");
		$smarty->assign('JML', $jml);
		
		$psn=getData("select case when sum(A)=0 then 0 else round((sum(A)/sum(A+B+C+D+E)*100),2) end as I,
						case when sum(B)=0 then 0 else round((sum(B)/sum(A+B+C+D+E)*100),2) end as II,
						case when sum(C)=0 then 0 else round((sum(C)/sum(A+B+C+D+E)*100),2) end as III,
						case when sum(D)=0 then 0 else round((sum(D)/sum(A+B+C+D+E)*100),2) end as IV,
						case when sum(E)=0 then 0 else round((sum(E)/sum(A+B+C+D+E)*100),2) end as V,
						round((sum(A+B+C+D+E)/sum(A+B+C+D+E)*100),2) as persen
						from 
						(
						select 
						case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 1 else 0 end as A,
						case when dsn.id_jabatan_fungsional=1 then 1 else 0 end as B,
						case when dsn.id_jabatan_fungsional=2 then 1 else 0 end as C,
						case when dsn.id_jabatan_fungsional=3 then 1 else 0 end as D,
						case when dsn.id_jabatan_fungsional=4 then 1 else 0 end as E
						from dosen dsn 
						left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
						left join golongan gol on gol.id_golongan=dsn.id_golongan
						where dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						and dsn.id_program_studi_sd in (select id_program_studi from program_studi where id_fakultas like '$display')
						)");
		$smarty->assign('PSN', $psn);
}

$smarty->display('rekap_dosen_gol_jab.tpl');

?>