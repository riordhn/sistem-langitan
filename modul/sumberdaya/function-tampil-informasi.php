<?php

function alert_error($text) {
    return "<div id='alert' class='ui-widget' style='margin-left:0px;margin-top:10px;'>
        <div class='ui-state-error ui-corner-all' style='padding: 5px;width:98%;'> 
            <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;margin-bottom:1em;'></span> 
                {$text}</p>
        </div>
    </div>";
}

function alert_success($text) {
    return "<div id='alert' class='ui-widget' style='margin-left:0px;margin-top:10px'>
        <div class='ui-state-highlight ui-corner-all' style='padding: 5px;width:98%;'> 
            <p><span class='ui-icon ui-icon-check' style='float: left; margin-right: .3em;margin-bottom:1em;'></span> 
                {$text}</p>
        </div>
    </div>";
}

?>
