<?php
//Yudi Sulistya, 28/08/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pt = $id_pt_user;

$kd_fak=getData("select id_fakultas, upper(nm_fakultas) as nm_fakultas from fakultas where id_perguruan_tinggi = {$id_pt} order by id_fakultas");
$smarty->assign('KD_FAK', $kd_fak);

if ($request_method == 'GET' or $request_method == 'POST')
{
		$id_fak = get('id','');
		$id_fak = ($id_fak == '' || !isset($id_fak)) ? 696969 : $id_fak; #BIAR GAK NEMU QUERYNYA


		$fak=getData("select 'FAKULTAS '||upper(fak.nm_fakultas)||' ('||count(dsn.nip_dosen)||' dosen)' as fakultas
		from dosen dsn
		left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
		left join fakultas fak on pst.id_fakultas=fak.id_fakultas
		where pst.id_fakultas=$id_fak 
		and (dsn.id_status_pengguna is null or dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=0))
		group by fak.nm_fakultas");
		$smarty->assign('FAK', $fak);
		
		$count=getvar("select count(*) as cek from 
		(select dsn.id_pengguna, dsn.nip_dosen as username, dsn.nidn_dosen, dsn.serdos, 
		case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
		else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		jjg.nm_jenjang||' - ' ||pst.nm_program_studi as nm_program_studi, sts.nm_status_pengguna
		from dosen dsn
		left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
		left join status_pengguna sts on sts.id_status_pengguna=dsn.id_status_pengguna
		left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
		left join fakultas fak on pst.id_fakultas=fak.id_fakultas
		left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang
		where pst.id_fakultas=$id_fak 
		and (dsn.id_status_pengguna is null or dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=0)))");
		$smarty->assign('DATA', $count['CEK']);
		
		$dosen=getData("select dsn.id_pengguna, dsn.nip_dosen as username, 
		case when (length(dsn.nidn_dosen)<10 or dsn.nidn_dosen is null) then '-' else dsn.nidn_dosen end as nidn_dosen, 
		case when (length(dsn.serdos)<10 or dsn.serdos is null) then '-' else dsn.serdos end as serdos, 
		case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
		else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		jjg.nm_jenjang||' - ' ||pst.nm_program_studi as nm_program_studi, sts.nm_status_pengguna
		from dosen dsn
		left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
		left join status_pengguna sts on sts.id_status_pengguna=dsn.id_status_pengguna
		left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
		left join fakultas fak on pst.id_fakultas=fak.id_fakultas
		left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang
		where pst.id_fakultas=$id_fak 
		and (dsn.id_status_pengguna is null or dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=0))");
		$smarty->assign('DOSEN', $dosen);
}

$smarty->display('sd_dosen_non_aktif.tpl');

?>