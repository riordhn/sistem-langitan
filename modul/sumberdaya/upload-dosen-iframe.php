<?php
//ini_set("display_errors", 1);
include '../../config.php';
include 'includes/upload.class.php';

$id_pt = $id_pt_user;

$upload = new upload($db);

$mode = 'view';

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	// Jika tidak ada masalah upload
	if ($_FILES['file_dosen']['error'] == UPLOAD_ERR_INI_SIZE || $_FILES['file_dosen']['error'] == UPLOAD_ERR_FORM_SIZE)
	{
		echo "Ukuran file terlalu besar"; exit();
	}
	else if ($_FILES['file_dosen']['error'] == UPLOAD_ERR_NO_FILE)
	{
		echo "File tidak ada"; exit();
	}
	else if ($_FILES['file_dosen']['error'] == UPLOAD_ERR_OK)
	{

		// cara load file excel
		$excel = \PhpOffice\PhpSpreadsheet\IOFactory::load($_FILES['file_dosen']['tmp_name']);

		// ambil sheet pertama
		$sheet = $excel->getSheet();

		// Perulangan
		for ($pRow = 2; $pRow <= $sheet->getHighestRow(); $pRow++)
		{
			// mengambil cell baris ke pRow kolom ke 2
			//$cell = $sheet->getCellByColumnAndRow(2, $pRow);  // k

			$nik = $sheet->getCellByColumnAndRow(0, $pRow)->getFormattedValue();
			$nidn = $sheet->getCellByColumnAndRow(1, $pRow)->getFormattedValue();
			$prodi = $sheet->getCellByColumnAndRow(2, $pRow)->getFormattedValue();
			$nama = $sheet->getCellByColumnAndRow(3, $pRow)->getFormattedValue();
			$status = $sheet->getCellByColumnAndRow(4, $pRow)->getFormattedValue();

				if(trim($nidn) == 0){
					$nidn = null;
				}

				if($status == 'TETAP'){
					$status = 'PNS';
				}

				if($status == 'GURU BESAR'){
					$status = 'GB UNAIR';
				}

				//cek master pengguna dan kebutuhan input
				$pengguna       = $upload->cek_pengguna($nik,$id_pt);
			    $dosen 	 	    = $upload->cek_dosen_by_perguruan_tinggi($nik,$id_pt);

			    //cek master data sebelum input
			    $prodi          = $upload->cek_prodi($prodi,$id_pt);

			    if(empty($prodi)){
			    	echo "Data Prodi Belum Terdaftar, Error Pada Baris Ke-".$pRow."</br>";
			    	exit();
			    }


			    if(!empty($prodi)){
			        if(empty($pengguna)){
			            $nama_pgg =  str_replace("'", "''", $nama);
			            $upload->insert_pengguna_dosen(stripslashes($nama_pgg), $nik, $id_pt);            
			        }
			        else{
			        	$nama_pgg =  str_replace("'", "''", $nama);
			            $upload->update_pengguna_dosen(stripslashes($nama_pgg), $nik, $id_pt);
			            //echo "Data Pengguna Sudah Ada";
			        }

			        if(empty($dosen)){
			            $pengguna1 = $upload->cek_pengguna($nik,$id_pt);
			            //echo "hmm ".$pengguna1['ID_PENGGUNA'];
			            $upload->insert_dosen($pengguna1['ID_PENGGUNA'], $nik, $nidn, $prodi['ID_PROGRAM_STUDI'], $status);
			        }
			        else{
			        	$pengguna1 = $upload->cek_pengguna($nik,$id_pt);
			            $upload->update_dosen($pengguna1['ID_PENGGUNA'], $nik, $nidn, $prodi['ID_PROGRAM_STUDI'], $status);
			            //echo "Data Mahasiswa Sudah Ada";
			        }
			    }

			//echo "huhu haha ".$nik->getFormattedValue();
			// Pengecekan nilai yg tidak pas
			/*if ($nik->getFormattedValue() == '')
			{
				// JUMP
				goto selesai;
			}*/
		}

		/*selesai:
		$excel->disconnectWorksheets();
		unset($excel);*/

		// convert csv menjadi array
		/*$csv = array_map('str_getcsv', file($_FILES['file_dosen']['tmp_name']));

		// Proses disini :
		// Edited By : FIKRIE
		// 08-10-2015
		$i=0;
		foreach ($csv as $baris) {
			$i++;
			if($i>1){
				$nik 		= $baris[0];
				$nidn 		= $baris[1];
				$prodi 		= $baris[2];
				$nama	 	= $baris[3];
				$status		= $baris[4];

				if(trim($nidn) == 0){
					$nidn = null;
				}

				if($status == 'TETAP'){
					$status = 'PNS';
				}

				if($status == 'GURU BESAR'){
					$status = 'GB UNAIR';
				}

				//cek master pengguna dan kebutuhan input
				$pengguna       = $upload->cek_pengguna($nik,$id_pt);
			    $dosen 	 	    = $upload->cek_dosen_by_perguruan_tinggi($nik,$id_pt);

			    //cek master data sebelum input
			    $prodi          = $upload->cek_$prodi($prodi,$id_pt);

			    if(empty($prodi)){
			    	echo "Data $prodi Belum Terdaftar, Error Pada Baris Ke-".$i."</br>";
			    	exit();
			    }


			    if(!empty($prodi)){
			        if(empty($pengguna)){
			            $nama_pgg =  str_replace("'", "''", $nama);
			            $upload->insert_pengguna_dosen(stripslashes($nama_pgg), $nik, $id_pt);            
			        }
			        else{
			        	$nama_pgg =  str_replace("'", "''", $nama);
			            $upload->update_pengguna_dosen(stripslashes($nama_pgg), $nik, $id_pt);
			            //echo "Data Pengguna Sudah Ada";
			        }

			        if(empty($dosen)){
			            $pengguna1 = $upload->cek_pengguna($nik,$id_pt);
			            //echo "hmm ".$pengguna1['ID_PENGGUNA'];
			            $upload->insert_dosen($pengguna1['ID_PENGGUNA'], $nik, $nidn, $prodi['ID_PROGRAM_STUDI'], $status);
			        }
			        else{
			        	$pengguna1 = $upload->cek_pengguna($nik,$id_pt);
			            $upload->update_dosen($pengguna1['ID_PENGGUNA'], $nik, $nidn, $prodi['ID_PROGRAM_STUDI'], $status);
			            //echo "Data Mahasiswa Sudah Ada";
			        }
			    }
			}

		}*/

		echo "Data Tenaga Dosen Berhasil Dimasukkan";

		exit();
	}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	$smarty->display('upload/iframe-dosen.tpl');
}

