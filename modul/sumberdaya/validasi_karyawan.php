<?php
//Yudi Sulistya, 27/08/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$count=getvar("select count(*) as cek from 
(select peg.id_pengguna, peg.nip_pegawai as username, 
case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
upper(nm_unit_kerja) as nm_unit_kerja
from pegawai peg
left join pengguna pgg on peg.id_pengguna=pgg.id_pengguna
left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
left join fakultas fak on fak.id_fakultas=uk.id_fakultas
left join program_studi pst on pst.id_program_studi=uk.id_program_studi
where peg.flag_valid=0)");
$smarty->assign('DATA', $count['CEK']);

$pegawai=getData("select peg.id_pengguna, peg.nip_pegawai as username, 
		case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
		else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		upper(nm_unit_kerja) as nm_unit_kerja
		from pegawai peg
		left join pengguna pgg on peg.id_pengguna=pgg.id_pengguna
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
		left join fakultas fak on fak.id_fakultas=uk.id_fakultas
		left join program_studi pst on pst.id_program_studi=uk.id_program_studi
		where peg.flag_valid=0");
$smarty->assign('PEGAWAI', $pegawai);

$smarty->display('validasi_karyawan.tpl');

?>