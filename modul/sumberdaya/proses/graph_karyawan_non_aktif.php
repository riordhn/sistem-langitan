<?php
//Yudi Sulistya, 23/08/2012

error_reporting (E_ALL & ~E_NOTICE);

require ("../../../config.php");

$id_pt = $id_pt_user;

if ($user->Role() == AUCC_ROLE_SDM) {
include ("../includes/jpgraph/jpgraph.php");
include ("../includes/jpgraph/jpgraph_bar.php");

$tot = array();
$fak = array();

$data="
select id_status_pengguna, nm_status_pengguna, count(id_pegawai) as jml
from 
(
select a.id_status_pengguna, a.nm_status_pengguna, b.id_pegawai
from
(
(select id_status_pengguna, nm_status_pengguna from status_pengguna where id_role=22) union (select 9999 as id_status_pengguna, 'Tanpa Status' as nm_status_pengguna from dual)
) a left join
(
SELECT
				CASE
			WHEN id_status_pengguna IS NULL THEN
				9999
			ELSE
				id_status_pengguna
			END AS id_status_pengguna,
			id_pegawai
		FROM
			pegawai peg,
			pengguna p
		WHERE
			peg.id_pengguna = p.id_pengguna and p.id_perguruan_tinggi = {$id_pt}
) b on a.id_status_pengguna=b.id_status_pengguna
)
group by id_status_pengguna, nm_status_pengguna
order by id_status_pengguna desc
";
$result = $db->Query($data) or die ("salah kueri : graph data");
while($r = $db->FetchRow()) {
	array_unshift($tot, $r[2]);
    array_unshift($fak, $r[1]);
}

$max="
select max(count(sts_pgg)) as max from 
(
select case when peg.id_status_pengguna is null then 'Tanpa Status' else sts.nm_status_pengguna end as sts_pgg, count(peg.id_pegawai) as jml
		from pegawai peg
		left join status_pengguna sts on sts.id_status_pengguna=peg.id_status_pengguna
		group by peg.id_status_pengguna, sts.nm_status_pengguna
)
group by sts_pgg";
$result = $db->Query($max) or die ("salah kueri : graph max");
while($r = $db->FetchRow()) {
	$Yaxis=round($r[0], -3);
}

$graph = new Graph(830,400,"auto");
$graph->SetScale("textlin",0,$Yaxis);
$graph->SetShadow(false);
$graph->SetMargin(50,20,30,50);
$graph->ygrid->SetWeight(1);
$graph->ygrid->SetColor("black");
$barplot=new BarPlot($tot);
$barplot->SetFillGradient('green','red',GRAD_HOR);
$barplot->value->show();
$barplot->value->SetFormat("%3.0f");
$graph->Add($barplot);
$graph->xaxis-> SetTickLabels($fak);
$graph->SetColor('#eaf4f9');
$graph->SetMarginColor('#eaf4f9');
$graph->title->Set("GRAFIK STATUS KARYAWAN");
$graph->xaxis->title->Set("STATUS");
$graph->yaxis->title->Set("TOTAL");
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->Stroke();

} else {
    header("location: /logout.php");
    exit();
}
?>