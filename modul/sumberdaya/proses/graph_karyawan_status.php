<?php
//Yudi Sulistya, 23/08/2012

error_reporting (E_ALL & ~E_NOTICE);

require ("../../../config.php");

$id_pt = $id_pt_user;

if ($user->Role() == AUCC_ROLE_SDM) {
include ("../includes/jpgraph/jpgraph.php");
include ("../includes/jpgraph/jpgraph_bar.php");

$tot = array();
$fak = array();

$data="
select urut, nm_unit, count(nip_pegawai) as total 
from 
(
select urut, case when unitkerja = 'DIREKTORAT SISTEM INFORMASI' then 'DIR. SISTEM INFORMASI' 
when unitkerja = 'DIREKTORAT PENDIDIKAN' then 'DIR. PENDIDIKAN'
when unitkerja = 'DIREKTORAT KEUANGAN' then 'DIR. KEUANGAN'
when unitkerja = 'DIREKTORAT KEMAHASISWAAN' then 'DIR. KEMAHASISWAAN'
else unitkerja end as nm_unit, nip_pegawai from 
(
select case when uk.nm_unit_kerja is null then 0
when type_unit_kerja='REKTORAT' then 1
when type_unit_kerja='LEMBAGA' then 2
else uk.id_fakultas+2 end as urut, 
case when (uk.type_unit_kerja='PRODI' or uk.type_unit_kerja='FAKULTAS') then fak.singkatan_fakultas 
when uk.nm_unit_kerja is null then 'NO UNIT KERJA' 
else upper(uk.nm_unit_kerja) end as unitkerja, peg.nip_pegawai, 
type_unit_kerja
		from pegawai peg
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
		left join fakultas fak on fak.id_fakultas=uk.id_fakultas
		where uk.id_perguruan_tinggi = {$id_pt} and (peg.status_pegawai='' or peg.status_pegawai is null)
		and peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=1)
)
)
group by urut, nm_unit
order by urut desc, nm_unit desc";
$result = $db->Query($data) or die ("salah kueri : graph data");
while($r = $db->FetchRow()) {
	array_unshift($tot, $r[2]);
    array_unshift($fak, $r[1]);
}

$total="
select count(peg.id_pegawai) as total
from pegawai peg, pengguna p
where peg.id_pengguna = p.id_pengguna
and p.id_perguruan_tinggi = {$id_pt}
and (peg.status_pegawai='' or peg.status_pegawai is null)
and peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=1)";
$result = $db->Query($total) or die ("salah kueri : graph total");
while($r = $db->FetchRow()) {
	$total=$r[0];
}

$max="
select max(total) as max from 
(
select urut, nm_unit, count(nip_pegawai) as total 
from 
(
select urut, case when unitkerja = 'DIREKTORAT SISTEM INFORMASI' then 'DIR. SISTEM INFORMASI' 
when unitkerja = 'DIREKTORAT PENDIDIKAN' then 'DIR. PENDIDIKAN'
when unitkerja = 'DIREKTORAT KEUANGAN' then 'DIR. KEUANGAN'
when unitkerja = 'DIREKTORAT KEMAHASISWAAN' then 'DIR. KEMAHASISWAAN'
else unitkerja end as nm_unit, nip_pegawai from 
(
select case when uk.nm_unit_kerja is null then 0
when type_unit_kerja='REKTORAT' then 1
when type_unit_kerja='LEMBAGA' then 2
else uk.id_fakultas+2 end as urut, 
case when (uk.type_unit_kerja='PRODI' or uk.type_unit_kerja='FAKULTAS') then fak.singkatan_fakultas 
when uk.nm_unit_kerja is null then 'NO UNIT KERJA' 
else upper(uk.nm_unit_kerja) end as unitkerja, peg.nip_pegawai, 
type_unit_kerja
		from pegawai peg
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja_sd
		left join fakultas fak on fak.id_fakultas=uk.id_fakultas
		where uk.id_perguruan_tinggi = {$id_pt} and (peg.status_pegawai='' or peg.status_pegawai is null)
		and peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=1)
)
)
group by urut, nm_unit
)
";
$result = $db->Query($max) or die ("salah kueri : graph max");
while($r = $db->FetchRow()) {
	$Yaxis=round($r[0], -2);
}
try {
$graph = new Graph(830,400,"auto");
$graph->SetScale("textlin",0,$Yaxis);
$graph->SetShadow(false);
$graph->SetMargin(50,20,30,150);
$graph->ygrid->SetWeight(1);
$graph->ygrid->SetColor("black");
$barplot=new BarPlot($tot);
$barplot->SetFillGradient('green','red',GRAD_HOR);
$barplot->value->show();
$barplot->value->SetFormat("%3.0f");
$graph->Add($barplot);
$graph->xaxis->SetFont(FF_FONT1,FS_NORMAL);
$graph->xaxis-> SetTickLabels($fak);
$graph->xaxis->SetLabelAngle(90);
$graph->SetColor('#eaf4f9');
$graph->SetMarginColor('#eaf4f9');
$graph->title->Set("GRAFIK KARYAWAN AKTIF TANPA STATUS KEPEGAWAIAN");
$graph->footer->center->Set("TOTAL KESELURUHAN: ".$total."");
$graph->xaxis->title->Set("UNIT KERJA","center");
$graph->yaxis->title->Set("TOTAL","center");
$graph->xaxis->SetTitleMargin(20);
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->footer->center->Set("TOTAL KESELURUHAN: ".$total."");
$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->Stroke();
} catch(JpGraphException $e) {
echo '
<link rel="stylesheet" type="text/css" href="../includes/iframe.css" />
<table align="center">
	<tr>
		<td>SEMUA KARYAWAN <b><u>AKTIF</u></b> SUDAH DI SET STATUS KEPEGAWAIANNYA</td>
	</tr>
</table>';
}

} else {
    header("location: /logout.php");
    exit();
}
?>