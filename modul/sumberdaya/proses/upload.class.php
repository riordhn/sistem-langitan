<?php

//include 'function-tampil-informasi.php';

class upload {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }
    
    function GetJenisFile($id_jenis){
        return $this->db->QuerySingle("SELECT NM_JENIS FROM UPLOAD_JENIS_FILE WHERE ID_UPLOAD_JENIS_FILE='{$id_jenis}'");
    }

    function GetIdFolderFile($id_pengguna) {
        return $this->db->QuerySingle("SELECT ID_UPLOAD_FOLDER FROM UPLOAD_FOLDER WHERE NAMA_UPLOAD_FOLDER LIKE '%{$id_pengguna}%'");
    }

    function InsertFileDatabase($id_folder, $id_pengguna, $nama, $id_jenis, $deskripsi) {
        $this->db->Query("
            INSERT INTO UPLOAD_FILE
                (ID_UPLOAD_FOLDER,ID_PENGGUNA,IP_UPLOAD_FILE,NAMA_UPLOAD_FILE,ID_JENIS_FILE,AKSES,DESKRIPSI,TGL_UPLOAD)
            VALUES
                ('{$id_folder}','{$id_pengguna}','{$_SERVER['REMOTE_ADDR']}','{$nama}','{$id_jenis}','1','{$deskripsi}',SYSDATE)
            ");
    }

    function GetLastIdFile($id_pengguna) {
        return $this->db->QuerySingle("SELECT ID_UPLOAD_FILE FROM UPLOAD_FILE WHERE ID_PENGGUNA='{$id_pengguna}' ORDER BY ID_UPLOAD_FILE DESC");
    }

    function ResetUpload($id_pengguna, $id_jenis) {
        $this->db->Query("DELETE FROM UPLOAD_FILE WHERE ID_PENGGUNA='{$id_pengguna}' AND ID_JENIS_FILE='{$id_jenis}'");
        $this->db->Query("DELETE FROM UPLOAD_FOLDER WHERE NAMA_UPLOAD_FOLDER LIKE '%{$id_pengguna}%'");
    }

}

?>
