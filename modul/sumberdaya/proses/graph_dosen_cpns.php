<?php
//Yudi Sulistya, 19/03/2014

error_reporting (E_ALL & ~E_NOTICE);

require ("../../../config.php");

$id_pt = $id_pt_user;

if ($user->Role() == AUCC_ROLE_SDM) {
include ("../includes/jpgraph/jpgraph.php");
include ("../includes/jpgraph/jpgraph_bar.php");

$tot = array();
$fak = array();

$data="
select b.singkatan_fakultas, case when a.total is null then 0 else a.total end as total from 
fakultas b left join
(
select singkatan_fakultas, id_fakultas, count(id_fakultas) as total from 
(
select fak.singkatan_fakultas, fak.id_fakultas
from dosen dsn
left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
left join fakultas fak on pst.id_fakultas=fak.id_fakultas
where fak.id_perguruan_tinggi = {$id_pt} and dsn.status_dosen='CPNS' and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
)
group by singkatan_fakultas, id_fakultas
) a
on b.singkatan_fakultas=a.singkatan_fakultas
where b.id_perguruan_tinggi = {$id_pt}
order by b.id_fakultas desc";
$result = $db->Query($data) or die ("salah kueri : graph data");
while($r = $db->FetchRow()) {
	array_unshift($tot, $r[1]);
    array_unshift($fak, $r[0]);
}

$total="
select count(dsn.id_dosen) as total
from dosen dsn, pengguna p
where dsn.id_pengguna = p.id_pengguna
and p.id_perguruan_tinggi = {$id_pt}
and dsn.status_dosen='CPNS' and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)";
$result = $db->Query($total) or die ("salah kueri : graph total");
while($r = $db->FetchRow()) {
	$total=$r[0];
}

$max="
select max(count(id_fakultas)) as max from 
(
select fak.id_fakultas
from dosen dsn
left join program_studi pst on dsn.id_program_studi_sd=pst.id_program_studi
left join fakultas fak on pst.id_fakultas=fak.id_fakultas
where fak.id_perguruan_tinggi = {$id_pt} and dsn.status_dosen='CPNS' and dsn.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
)
group by id_fakultas";
$result = $db->Query($max) or die ("salah kueri : graph max");
while($r = $db->FetchRow()) {
	$Yaxis=round($r[0], -3);
}

$graph = new Graph(830,400,"auto");
$graph->SetScale("textlin",0,$Yaxis);
$graph->SetShadow(false);
$graph->SetMargin(50,20,30,50);
$graph->ygrid->SetWeight(1);
$graph->ygrid->SetColor("black");
$barplot=new BarPlot($tot);
$barplot->SetFillGradient('green','red',GRAD_HOR);
$barplot->value->show();
$barplot->value->SetFormat("%3.0f");
$graph->Add($barplot);
$graph->xaxis-> SetTickLabels($fak);
$graph->SetColor('#eaf4f9');
$graph->SetMarginColor('#eaf4f9');
$graph->title->Set("GRAFIK DOSEN CPNS AKTIF");
$graph->footer->center->Set("TOTAL KESELURUHAN: ".$total."");
$graph->xaxis->title->Set("FAKULTAS");
$graph->yaxis->title->Set("TOTAL");
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->footer->center->SetFont(FF_FONT1,FS_BOLD);
$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->Stroke();

} else {
    header("location: /logout.php");
    exit();
}
?>