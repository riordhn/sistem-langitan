<?php
include ('common.php');
require_once ('ociFunction.php');
$mode = $_GET['mode'];
if ($mode == 'approve') {
    $id = $_GET['id'];
    $pengajuan = getvar(
            "SELECT 
        (CASE
          WHEN PP.JENIS_PEGAWAI=1
          THEN 'Dosen'
          WHEN PP.JENIS_PEGAWAI=2
          THEN 'Tenaga Kependidikan'
        END) JENIS,UK.NM_UNIT_KERJA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,G.NM_GOLONGAN,JF.NM_JABATAN_FUNGSIONAL,KL.NM_KOTA,PP.*
        FROM AUCC.PENGAJUAN_PEGAWAI PP
        LEFT JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=PP.ID_UNIT_KERJA
        LEFT JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=PP.ID_PROGRAM_STUDI
        LEFT JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
        LEFT JOIN AUCC.GOLONGAN G ON G.ID_GOLONGAN=PP.ID_GOLONGAN
        LEFT JOIN AUCC.JABATAN_FUNGSIONAL JF ON JF.ID_JABATAN_FUNGSIONAL=PP.ID_JABATAN_FUNGSIONAL
        LEFT JOIN AUCC.KOTA KL ON KL.ID_KOTA = PP.ID_KOTA_LAHIR
        WHERE PP.ID_PENGAJUAN_PEGAWAI='{$id}'
        ORDER BY PP.NAMA_LENGKAP,PP.TGL_LAHIR DESC");

    $sts_peg = getData("select distinct status_pegawai from pegawai where status_pegawai is not null order by status_pegawai");
    $smarty->assign('STS_PEG', $sts_peg);

    $sts_aktif = getData("select id_status_pengguna, upper(nm_status_pengguna) as nm_status_pengguna from status_pengguna where id_role=22 and status_aktif=1 order by nm_status_pengguna");
    $smarty->assign('STS_AKTIF', $sts_aktif);

    $listunit = getData("select id_unit_kerja,id_fakultas, upper(nm_unit_kerja) as unitkerja,
    case when type_unit_kerja='REKTORAT' then 0 
    when type_unit_kerja='LEMBAGA' then 1 
    when type_unit_kerja='FAKULTAS' then 2 end as urut0,
    case when id_fakultas is null then 0 else id_fakultas end as urut1
    from unit_kerja where type_unit_kerja in ('REKTORAT', 'LEMBAGA', 'FAKULTAS')
    order by urut0, urut1, unitkerja");
    $smarty->assign('T_UNIT', $listunit);

    $sts_dsn = getData("select distinct status_dosen from dosen where status_dosen is not null order by status_dosen");
    $smarty->assign('STS_DSN', $sts_dsn);

    $kdfak = getvar("SELECT ID_FAKULTAS FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI='{$pengajuan['ID_PROGRAM_STUDI']}'");

    $listfak = getData("select id_fakultas, upper(nm_fakultas) as nm_fakultas from fakultas order by id_fakultas");
    $smarty->assign('T_FAK', $listfak);
    $smarty->assign('kdfak', $kdfak['ID_FAKULTAS']);

    if ($kdfak['ID_FAKULTAS']) {
        $kdprodi = getData("
        select pst.id_program_studi, upper(substr(jjg.nm_jenjang,1,2)||' - '||pst.nm_program_studi) as nm_program_studi 
        from program_studi pst 
        left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang 
        where id_fakultas='{$kdfak['ID_FAKULTAS']}' 
        order by jjg.nm_jenjang, pst.nm_program_studi
        ");
        $smarty->assign('PRO', $kdprodi);
    }

    $jk = getData("select distinct kelamin_pengguna, case when kelamin_pengguna='1' then 'LAKI-LAKI' else 'PEREMPUAN' end as nm_kelamin_pengguna from pengguna where kelamin_pengguna in ('1', '2') order by kelamin_pengguna");
    $smarty->assign('JK', $jk);

    $jab = getData("select id_jabatan_pegawai, upper(nm_jabatan_pegawai) as nm_jabatan_pegawai from jabatan_pegawai order by id_jabatan_pegawai");
    $smarty->assign('JAB', $jab);

    $gol = getData("select id_golongan, upper(nm_golongan) as nm_golongan from golongan order by nm_golongan desc");
    $smarty->assign('GOL', $gol);

    $fsg = getData("select id_jabatan_fungsional, upper(nm_jabatan_fungsional) as nm_jabatan_fungsional from jabatan_fungsional order by id_jabatan_fungsional");
    $smarty->assign('FSG', $fsg);

    $data_kota = array();
    $provinsi = getData("SELECT * FROM PROVINSI WHERE ID_NEGARA=114 ORDER BY NM_PROVINSI");
    foreach ($provinsi as $data) {
        array_push($data_kota, array(
            'nama' => $data['NM_PROVINSI'],
            'kota' => getData("SELECT id_kota, tipe_dati2||' '||nm_kota as kota FROM KOTA WHERE ID_PROVINSI='{$data['ID_PROVINSI']}' ORDER BY TIPE_DATI2, NM_KOTA")
        ));
    }
    $smarty->assign('pengajuan', $pengajuan);
    $smarty->assign('id_fakultas', $user->ID_FAKULTAS);
    $smarty->assign('data_kota', $data_kota);
    $smarty->assign('count_provinsi', count($data_kota));
} else {
    if (!empty($_POST)) {
        $action = $_POST['action'];
        if ($action == 'pengajuan_dosen') {
            $id = $_POST['id_pengajuan'];
            $nik = $_POST['nik'];
            $nidn = $_POST['nidn'];
            $serdos = $_POST['serdos'];
            $password = sha1($nik);
            $prodi = $_POST['prodi'];
            $sts_kepeg = $_POST['status_kepeg'];
            $sts_aktif = $_POST['aktif'];
            $gol = $_POST['gol'];
            $jab_fung = $_POST['fsg'];
            $gelar_dpn = $_POST['gelar_dpn'];
            $gelar_blkg = $_POST['gelar_blkg'];
            $nama = $_POST['nm_lengkap'];
            $kota_lahir = $_POST['id_kota_lahir'];
            $tgl_lahir = $_POST['tgl_lahir'];
            $jenis_kelamin = $_POST['jk'];
            $alamat = $_POST['alamat'];
            $telp = $_POST['telp'];
            $mobile = $_POST['hp'];
            $email = $_POST['email'];
            UpdateData("
                UPDATE PENGAJUAN_PEGAWAI
                    SET 
                    ID_PROGRAM_STUDI='{$prodi}',
                    STATUS_KEPEGAWAIAN='{$sts_kepeg}',
                    STATUS_AKTIF='{$sts_aktif}',
                    ID_GOLONGAN='{$gol}',
                    ID_JABATAN_FUNGSIONAL='{$jab_fung}',
                    GELAR_DEPAN='{$gelar_dpn}',
                    GELAR_BELAKANG='{$gelar_blkg}',
                    NAMA_LENGKAP='{$nama}',
                    ID_KOTA_LAHIR='{$kota_lahir}',
                    JENIS_KELAMIN='{$jenis_kelamin}',
                    ALAMAT='{$alamat}',
                    TELEPON='{$telp}',
                    HANDPHONE='{$mobile}',
                    EMAIL='{$email}',
                    NIK='{$nik}',
                    STATUS_PENGAJUAN='1'
                WHERE ID_PENGAJUAN_PEGAWAI='{$id}'
                ");
            InsertData("
                INSERT INTO PENGGUNA
                    (USERNAME,NM_PENGGUNA,KELAMIN_PENGGUNA,TEMPAT_LAHIR,TGL_LAHIR_PENGGUNA,GELAR_DEPAN,GELAR_BELAKANG,PASSWORD_HASH,JOIN_TABLE,ID_ROLE)
                SELECT
                    NIK USERNAME,NAMA_LENGKAP NM_PENGGUNA,JENIS_KELAMIN KELAMIN_PENGGUNA,ID_KOTA_LAHIR TEMPAT_LAHIR,TGL_LAHIR TGL_LAHIR_PENGGUNA,
                    GELAR_DEPAN,GELAR_BELAKANG,'{$password}' PASSWORD_HASH,2 JOIN_TABLE,4 ID_ROLE
                FROM PENGAJUAN_PEGAWAI
                WHERE ID_PENGAJUAN_PEGAWAI='{$id}'
                ");
            $pengguna_last = getvar("
            SELECT * FROM
                (SELECT * FROM AUCC.PENGGUNA ORDER BY ID_PENGGUNA DESC)
            WHERE ROWNUM=1");
            InsertData("
                INSERT INTO DOSEN
                    (ID_PENGGUNA,ID_PROGRAM_STUDI,NIP_DOSEN,NIDN_DOSEN,SERDOS,ALAMAT_RUMAH_DOSEN,TLP_DOSEN,MOBILE_DOSEN,FLAG_VALID)
                SELECT
                    '{$pengguna_last['ID_PENGGUNA']}' ID_PENGGUNA,ID_PROGRAM_STUDI,NIK NIP_DOSEN,'{$nidn}' NIDN,'{$serdos}' SERDOS,ALAMAT ALAMAT_RUMAH_DOSEN,TELEPON TELP_DOSEN,HANDPHONE MOBILE_DOSEN,1 FLAG_VALID
                FROM PENGAJUAN_PEGAWAI
                WHERE ID_PENGAJUAN_PEGAWAI='{$id}'
                ");
            echo "<script>alert('Data berhasil diupdate')</script>";
        } else if ($action == 'pengajuan_tenaga_kependidikan') {
            $id = $_POST['id_pengajuan'];
            $nik = $_POST['nik'];
            $password = sha1($nik);
            $unit_kerja = $_POST['unit_kerja'];
            $sts_kepeg = $_POST['status_kepeg'];
            $sts_aktif = $_POST['aktif'];
            $gol = $_POST['gol'];
            $jab_fung = $_POST['fsg'];
            $gelar_dpn = $_POST['gelar_dpn'];
            $gelar_blkg = $_POST['gelar_blkg'];
            $nama = $_POST['nm_lengkap'];
            $kota_lahir = $_POST['id_kota_lahir'];
            $tgl_lahir = $_POST['tgl_lahir'];
            $jenis_kelamin = $_POST['jk'];
            $alamat = $_POST['alamat'];
            $telp = $_POST['telp'];
            $mobile = $_POST['hp'];
            $email = $_POST['email'];
            UpdateData("
                UPDATE PENGAJUAN_PEGAWAI
                    SET 
                    ID_UNIT_KERJA='{$unit_kerja}',
                    STATUS_KEPEGAWAIAN='{$sts_kepeg}',
                    STATUS_AKTIF='{$sts_aktif}',
                    ID_GOLONGAN='{$gol}',
                    ID_JABATAN_FUNGSIONAL='{$jab_fung}',
                    GELAR_DEPAN='{$gelar_dpn}',
                    GELAR_BELAKANG='{$gelar_blkg}',
                    NAMA_LENGKAP='{$nama}',
                    ID_KOTA_LAHIR='{$kota_lahir}',
                    JENIS_KELAMIN='{$jenis_kelamin}',
                    ALAMAT='{$alamat}',
                    TELEPON='{$telp}',
                    HANDPHONE='{$mobile}',
                    EMAIL='{$email}',
                    NIK='{$nik}',
                    STATUS_PENGAJUAN='1'
                WHERE ID_PENGAJUAN_PEGAWAI='{$id}'
                ");
            InsertData("
                INSERT INTO PENGGUNA
                    (USERNAME,NM_PENGGUNA,KELAMIN_PENGGUNA,TEMPAT_LAHIR,TGL_LAHIR_PENGGUNA,GELAR_DEPAN,GELAR_BELAKANG,PASSWORD_HASH,JOIN_TABLE,ID_ROLE)
                SELECT
                    NIK USERNAME,NAMA_LENGKAP NM_PENGGUNA,JENIS_KELAMIN KELAMIN_PENGGUNA,ID_KOTA_LAHIR TEMPAT_LAHIR,TGL_LAHIR TGL_LAHIR_PENGGUNA,
                    GELAR_DEPAN,GELAR_BELAKANG,'{$password}' PASSWORD_HASH,2 JOIN_TABLE,22 ID_ROLE
                FROM PENGAJUAN_PEGAWAI
                WHERE ID_PENGAJUAN_PEGAWAI='{$id}'
                ");
            $pengguna_last = getvar("
            SELECT * FROM
                (SELECT * FROM AUCC.PENGGUNA ORDER BY ID_PENGGUNA DESC)
            WHERE ROWNUM=1");
            InsertData("
                INSERT INTO PEGAWAI
                    (ID_PENGGUNA,ID_GOLONGAN,ID_JABATAN_PEGAWAI,NIP_PEGAWAI,ALAMAT_PEGAWAI,TELP_PEGAWAI,ID_UNIT_KERJA,MOBILE_PEGAWAI,FLAG_VALID)
                SELECT
                    '{$pengguna_last['ID_PENGGUNA']}' ID_PENGGUNA,ID_GOLONGAN,21 ID_JABATAN_PEGAWAI,NIK NIP_PEGAWAI,ALAMAT ALAMAT_PEGAWAI,TELEPON TELP_PEGAWAI,ID_UNIT_KERJA,HANDPHONE MOBILE_PEGAWAI,1 FLAG_VALID
                FROM PENGAJUAN_PEGAWAI
                WHERE ID_PENGAJUAN_PEGAWAI='{$id}'
                ");
            echo "<script>alert('Data berhasil diupdate')</script>";
        }
    }
    $pengajuan = getData("
        SELECT 
        (CASE
          WHEN PP.JENIS_PEGAWAI=1
          THEN 'Dosen'
          WHEN PP.JENIS_PEGAWAI=2
          THEN 'Tenaga Kependidikan'
        END) JENIS,UK.NM_UNIT_KERJA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,G.NM_GOLONGAN,JF.NM_JABATAN_FUNGSIONAL,KL.NM_KOTA,PP.*
        FROM AUCC.PENGAJUAN_PEGAWAI PP
        LEFT JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=PP.ID_UNIT_KERJA
        LEFT JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=PP.ID_PROGRAM_STUDI
        LEFT JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
        LEFT JOIN AUCC.GOLONGAN G ON G.ID_GOLONGAN=PP.ID_GOLONGAN
        LEFT JOIN AUCC.JABATAN_FUNGSIONAL JF ON JF.ID_JABATAN_FUNGSIONAL=PP.ID_JABATAN_FUNGSIONAL
        LEFT JOIN AUCC.KOTA KL ON KL.ID_KOTA = PP.ID_KOTA_LAHIR
        ORDER BY PP.NAMA_LENGKAP,PP.TGL_LAHIR DESC
        ");
    $smarty->assign('pengajuan', $pengajuan);
}
$smarty->display('tools/approve_pengajuan.tpl');
?>