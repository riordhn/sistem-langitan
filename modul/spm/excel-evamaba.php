<?php
include 'config.php';
include '../ppm/class/evaluasi.class.php';
include '../keuangan/class/list_data.class.php';

$eva = new evaluasi($db);
$list = new list_data($db);
$semester = get('semester');
$fakultas = get('fakultas');

$header_kelompok_aspek = $db->QueryToArray("
            SELECT EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK,COUNT(EA.ID_EVAL_ASPEK)  JUMLAH_ASPEK
            FROM EVALUASI_KELOMPOK_ASPEK EKS
            JOIN EVALUASI_ASPEK EA ON EA.ID_EVAL_KELOMPOK_ASPEK=EKS.ID_EVAL_KELOMPOK_ASPEK AND EA.TIPE_ASPEK=1 AND EA.ID_EVAL_ASPEK IN 
            (
                SELECT ID_EVAL_ASPEK 
                FROM EVALUASI_HASIL 
                WHERE ID_SEMESTER='{$semester}'
                AND ID_FAKULTAS='{$fakultas}'
            )
            WHERE EKS.ID_EVAL_INSTRUMEN=4
            GROUP BY EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK
            ORDER BY ID_EVAL_KELOMPOK_ASPEK");
$header_aspek = $db->QueryToArray("
            SELECT * FROM EVALUASI_ASPEK 
            WHERE ID_EVAL_INSTRUMEN=4
            AND TIPE_ASPEK=1 
            AND ID_EVAL_ASPEK IN (
                SELECT ID_EVAL_ASPEK 
                FROM EVALUASI_HASIL 
                WHERE ID_SEMESTER='{$semester}'
                AND ID_FAKULTAS='{$fakultas}'
            ) 
            ORDER BY URUTAN,ID_EVAL_ASPEK");

$data_excel = $eva->load_laporan_evaluasi_maba(get('fakultas'), get('semester'));
if (get('fakultas') != '' || get('prodi') != '') {
    $fakultas = $list->get_fakultas(get('fakultas'));
}
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #220344;
        text-transform:capitalize;
    }
    td{
        text-align: left;
    }
</style>
<table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="<?= count($header_aspek) + 3 ?>" class="header_text">
                <?php
                echo strtoupper('Rekapitulasi Evaluasi Mahasiswa Baru<br/>');
                if ($fakultas != '') {
                    echo strtoupper('Fakultas ' . $fakultas['NM_FAKULTAS'] . '<br/>');
                }
                ?>
            </td>
        </tr>
        <tr>
            <td class="header_text" rowspan="2">NO</td>
            <td class="header_text" rowspan="2">PRODI</td>
            <td class="header_text" rowspan="2">JUMLAH RESPONDEN</td>
            <?php
            foreach ($header_kelompok_aspek as $hka):
                $kelompok = strtoupper($hka['NAMA_KELOMPOK']);
                echo '<td class="header_text" colspan="' . $hka['JUMLAH_ASPEK'] . '">' . $kelompok . "</td>";
            endforeach;
            ?>
        </tr>
        <tr>
            <?php
            foreach ($header_aspek as $ha):
                $aspek = strtoupper($ha['EVALUASI_ASPEK']);
                echo '<td class="header_text">' . $aspek . "</td>";
            endforeach;
            ?>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data_excel as $d):
            echo "
                <tr>
                    <td>{$no}</td>
                    <td>{$d['NM_JENJANG']} {$d['NM_PROGRAM_STUDI']}</td>
                    <td>{$d['JUMLAH_RESPONDEN']}</td>";
            foreach ($d['DATA_HASIL'] as $h):
                $eva = round($h['RATA'], 2);
                echo "<td>{$eva}  <br/><br/> Nilai 0 : {$h['KOSONG']}</td>";
            endforeach;
            $no++;
        endforeach;
        ?>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "Data-Evaluasi-Mahasiswa-Baru(" . date('d-m-Y') . ")";
header("Content-disposition: attachment; filename={$nm_file}.xls");
header("Content-type: application/vnd.ms-excel");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
