<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';
include '../ppm/class/evaluasi.class.php';

$list = new list_data($db);
$eva = new evaluasi($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $semester = get('semester');
        $fakultas = get('fakultas');
        $q_prodi = $prodi==''?"":" AND ID_PROGRAM_STUDI='{$prodi}'";
        $smarty->assign('header_kelompok_aspek', $db->QueryToArray("
            SELECT EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK,COUNT(EA.ID_EVAL_ASPEK)  JUMLAH_ASPEK
            FROM EVALUASI_KELOMPOK_ASPEK EKS
            JOIN EVALUASI_ASPEK EA ON EA.ID_EVAL_KELOMPOK_ASPEK=EKS.ID_EVAL_KELOMPOK_ASPEK AND EA.TIPE_ASPEK=1 AND EA.ID_EVAL_ASPEK IN 
            (
                SELECT ID_EVAL_ASPEK 
                FROM EVALUASI_HASIL 
                WHERE ID_SEMESTER='{$semester}'
                AND ID_FAKULTAS='{$fakultas}' {$q_prodi}
            )
            WHERE EKS.ID_EVAL_INSTRUMEN=1
            GROUP BY EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK
            ORDER BY ID_EVAL_KELOMPOK_ASPEK
            ")
        );
        $smarty->assign('header_aspek', $db->QueryToArray("
            SELECT * FROM EVALUASI_ASPEK 
            WHERE ID_EVAL_INSTRUMEN=1 
            AND TIPE_ASPEK=1 
            AND ID_EVAL_ASPEK IN (
                SELECT ID_EVAL_ASPEK 
                FROM EVALUASI_HASIL 
                WHERE ID_SEMESTER='{$semester}'
                AND ID_FAKULTAS='{$fakultas}' {$q_prodi}
            ) 
            ORDER BY URUTAN,ID_EVAL_ASPEK"));
        $smarty->assign('data_evaluasi', $eva->load_laporan_evaluasi_kuliah(get('fakultas'), get('prodi'), get('semester')));
        $smarty->assign('data_rata_fakultas', $eva->load_laporan_rerata_fakultas_evaluasi_kuliah(get('fakultas'), get('semester')));
        $smarty->assign('data_rata_prodi', $eva->load_laporan_rerata_prodi_evaluasi_kuliah(get('prodi'), get('semester')));
    }
}
$smarty->assign('id_fakultas', $user->ID_FAKULTAS);
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas') != '' ? get('fakultas') : $user->ID_FAKULTAS));
$smarty->display('evakul.tpl');
?>
