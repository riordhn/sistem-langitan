<?php

class evaluasi {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    //menu evaluasi perkuliahan

    function load_row_evaluasi_kuliah($fakultas, $prodi, $semester) {
        $q_prodi = $prodi != '' ? "AND HEK.ID_PROGRAM_STUDI='{$prodi}'" : "";
        return $this->db->QueryToArray("
            SELECT D.ID_DOSEN,P.ID_PENGGUNA,HEK.ID_KELAS_MK,P.NM_PENGGUNA,MK.NM_MATA_KULIAH,MK.KD_MATA_KULIAH,NVL(K1.NMKELAS,K2.NAMA_KELAS) NM_KELAS ,
                COUNT(DISTINCT(HEK.ID_MHS)) JUMLAH_RESPONDEN
            FROM AUCC.HASIL_EV_KUL HEK
            JOIN AUCC.DOSEN D ON HEK.ID_DOSEN=D.ID_DOSEN
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
            JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = HEK.ID_KELAS_MK
            LEFT JOIN AUCC.PENGAMBILAN_MK PMK ON PMK.ID_KELAS_MK=KMK.ID_KELAS_MK
            LEFT JOIN AUCC.FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
            LEFT JOIN AUCC.NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
            JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
            JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
            WHERE HEK.ID_FAKULTAS='{$fakultas}' AND HEK.ID_SEMESTER='{$semester}' {$q_prodi}
            GROUP BY D.ID_DOSEN,HEK.ID_KELAS_MK,P.ID_PENGGUNA,P.NM_PENGGUNA,MK.NM_MATA_KULIAH,KMK.NM_KELAS_MK,K1.NMKELAS,K2.NAMA_KELAS,MK.KD_MATA_KULIAH
            ORDER BY MK.NM_MATA_KULIAH,NM_PENGGUNA,NM_KELAS");
    }

    function load_laporan_evaluasi_kuliah($fakultas, $prodi, $semester) {
        $data_laporan = array();
        foreach ($this->load_row_evaluasi_kuliah($fakultas, $prodi, $semester) as $row) {
            $jumlah_peserta = $this->db->QuerySingle("SELECT COUNT(ID_MHS) FROM AUCC.PENGAMBILAN_MK WHERE ID_KELAS_MK='{$row['ID_KELAS_MK']}'");
            $row_hasil = $this->db->QueryToArray("
                SELECT H.*,
                    (A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21)/21 RERATA_DOSEN,
                    (((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21)/21)/4)*100 RERATA_PERSEN_DOSEN
                FROM 
                (
                    SELECT HEK.ID_DOSEN,HEK.ID_KELAS_MK,
                    AVG(CASE WHEN A1<=4 THEN  A1 END) A1,
                    AVG(CASE WHEN A2<=4 THEN  A2 END) A2,
                    AVG(CASE WHEN A3<=4 THEN  A3 END) A3,
                    AVG(CASE WHEN A4<=4 THEN  A4 END) A4,
                    AVG(CASE WHEN A5<=4 THEN  A5 END) A5,
                    AVG(CASE WHEN A6<=4 THEN  A6 END) A6,
                    AVG(CASE WHEN A7<=4 THEN  A7 END) A7,
                    AVG(CASE WHEN A8<=4 THEN  A8 END) A8,
                    AVG(CASE WHEN A9<=4 THEN  A9 END) A9,
                    AVG(CASE WHEN A10<=4 THEN  A10 END) A10,
                    AVG(CASE WHEN A11<=4 THEN  A11 END) A11,
                    AVG(CASE WHEN A12<=4 THEN  A12 END) A12,
                    AVG(CASE WHEN A13<=4 THEN  A13 END) A13,
                    AVG(CASE WHEN A14<=4 THEN  A14 END) A14,
                    AVG(CASE WHEN A15<=4 THEN  A15 END) A15,
                    AVG(CASE WHEN A16<=4 THEN  A16 END) A16,
                    AVG(CASE WHEN A17<=4 THEN  A17 END) A17,
                    AVG(CASE WHEN A18<=4 THEN  A18 END) A18,
                    AVG(CASE WHEN A19<=4 THEN  A19 END) A19,
                    AVG(CASE WHEN A20<=4 THEN  A20 END) A20,
                    AVG(CASE WHEN A21<=4 THEN  A21 END) A21
                FROM AUCC.HASIL_EV_KUL HEK
                WHERE HEK.ID_DOSEN='{$row['ID_DOSEN']}' AND HEK.ID_KELAS_MK='{$row['ID_KELAS_MK']}'
                GROUP BY HEK.ID_DOSEN,HEK.ID_KELAS_MK
                ) H");
            array_push($data_laporan, array_merge($row, array(
                        'DATA_HASIL' => $row_hasil,
                        'JUMLAH_PESERTA' => $jumlah_peserta)
                    )
            );
        }
        return $data_laporan;
    }

    function load_laporan_rerata_fakultas_evaluasi_kuliah($fakultas, $semester) {
        $query = "SELECT HEK.ID_FAKULTAS,
                     AVG((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21)/21) RERATA,
                    (AVG((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21)/21)/4)*100 RERATA_PERSEN
                FROM AUCC.HASIL_EV_KUL HEK
                WHERE HEK.ID_FAKULTAS='{$fakultas}' AND HEK.ID_SEMESTER='{$semester}'
                GROUP BY HEK.ID_FAKULTAS";
        $this->db->Query($query);
        return $this->db->FetchAssoc();
    }

    function load_laporan_rerata_prodi_evaluasi_kuliah($prodi, $semester) {
        $query = "SELECT HEK.ID_PROGRAM_STUDI,
                     AVG((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21)/21) RERATA,
                    (AVG((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21)/21)/4)*100 RERATA_PERSEN
                FROM AUCC.HASIL_EV_KUL HEK
                WHERE HEK.ID_PROGRAM_STUDI='{$prodi}' AND ID_SEMESTER='{$semester}'
                GROUP BY HEK.ID_PROGRAM_STUDI";
        $this->db->Query($query);
        return $this->db->FetchAssoc();
    }

    // menu evaluasi praktikum

    function load_row_evaluasi_prak($fakultas, $prodi, $semester) {
        $q_prodi = $prodi != '' ? "AND HEK.ID_PROGRAM_STUDI='{$prodi}'" : "";
        return $this->db->QueryToArray("
            SELECT D.ID_DOSEN,P.ID_PENGGUNA,HEK.ID_KELAS_MK,P.NM_PENGGUNA,MK.NM_MATA_KULIAH,MK.KD_MATA_KULIAH,NVL(K1.NMKELAS,K2.NAMA_KELAS) NM_KELAS ,
                COUNT(DISTINCT(HEK.ID_MHS)) JUMLAH_RESPONDEN
            FROM AUCC.HASIL_EV_PRAK HEK
            JOIN AUCC.DOSEN D ON HEK.ID_DOSEN=D.ID_DOSEN
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
            JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = HEK.ID_KELAS_MK
            LEFT JOIN AUCC.PENGAMBILAN_MK PMK ON PMK.ID_KELAS_MK=KMK.ID_KELAS_MK
            LEFT JOIN AUCC.FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
            LEFT JOIN AUCC.NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
            JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
            JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
            WHERE HEK.ID_FAKULTAS='{$fakultas}' AND HEK.ID_SEMESTER='{$semester}' {$q_prodi}
            GROUP BY D.ID_DOSEN,HEK.ID_KELAS_MK,P.ID_PENGGUNA,P.NM_PENGGUNA,MK.NM_MATA_KULIAH,KMK.NM_KELAS_MK,K1.NMKELAS,K2.NAMA_KELAS,MK.KD_MATA_KULIAH
            ORDER BY MK.NM_MATA_KULIAH,NM_PENGGUNA,NM_KELAS");
    }

    function load_laporan_evaluasi_prak($fakultas, $prodi, $semester) {
        $data_laporan = array();
        foreach ($this->load_row_evaluasi_prak($fakultas, $prodi, $semester) as $row) {
            $jumlah_peserta = $this->db->QuerySingle("SELECT COUNT(ID_MHS) FROM AUCC.PENGAMBILAN_MK WHERE ID_KELAS_MK='{$row['ID_KELAS_MK']}'");
            $row_hasil = $this->db->QueryToArray("
                SELECT HEK.ID_DOSEN,HEK.ID_KELAS_MK,
                    AVG(A1) A1,
                    AVG(A2) A2,
                    AVG(A3) A3,
                    AVG(A4) A4,
                    AVG(A5) A5,
                    AVG(A6) A6,
                    AVG(A7) A7,
                    AVG(A8) A8,
                    AVG(A9) A9,
                    AVG(A10) A10,
                    AVG(A11) A11,
                    AVG(A12) A12,
                    AVG(A13) A13,
                    AVG(A14) A14,
                    AVG(A15) A15,
                    AVG((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15)/15) RERATA_DOSEN,
                    (AVG((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15)/15)/4)*100 RERATA_PERSEN_DOSEN
                FROM AUCC.HASIL_EV_PRAK HEK
                WHERE HEK.ID_DOSEN='{$row['ID_DOSEN']}' AND HEK.ID_KELAS_MK='{$row['ID_KELAS_MK']}'
                GROUP BY HEK.ID_DOSEN,HEK.ID_KELAS_MK");
            array_push($data_laporan, array_merge($row, array(
                        'DATA_HASIL' => $row_hasil,
                        'JUMLAH_PESERTA' => $jumlah_peserta)
                    )
            );
        }
        return $data_laporan;
    }

    function load_laporan_rerata_fakultas_evaluasi_prak($fakultas, $semester) {
        $query = "SELECT HEK.ID_FAKULTAS,
                     AVG((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15)/15) RERATA,
                    (AVG((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15)/15)/4)*100 RERATA_PERSEN
                FROM AUCC.HASIL_EV_PRAK HEK
                WHERE HEK.ID_FAKULTAS='{$fakultas}' AND HEK.ID_SEMESTER='{$semester}'
                GROUP BY HEK.ID_FAKULTAS";
        $this->db->Query($query);
        return $this->db->FetchAssoc();
    }

    function load_laporan_rerata_prodi_evaluasi_prak($prodi, $semester) {
        $query = "SELECT HEK.ID_PROGRAM_STUDI,
                     AVG((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15)/15) RERATA,
                    (AVG((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15)/15)/4)*100 RERATA_PERSEN
                FROM AUCC.HASIL_EV_PRAK HEK
                WHERE HEK.ID_PROGRAM_STUDI='{$prodi}' AND ID_SEMESTER='{$semester}'
                GROUP BY HEK.ID_PROGRAM_STUDI";
        $this->db->Query($query);
        return $this->db->FetchAssoc();
    }

    // menu evaluasi perwalian

    function load_row_evaluasi_wali($fakultas, $prodi, $semester) {
        $q_prodi = $prodi != '' ? "AND HEW.ID_PROGRAM_STUDI='{$prodi}'" : "";
        return $this->db->QueryToArray("
            SELECT D.ID_DOSEN,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,DEP.NM_DEPARTEMEN,COUNT(DISTINCT(HEW.NIM)) JUMLAH_RESPONDEN
                FROM AUCC.HASIL_EV_WALI HEW
                JOIN AUCC.MAHASISWA M ON M.NIM_MHS=HEW.NIM
                JOIN AUCC.DOSEN_WALI DW ON DW.ID_MHS=M.ID_MHS
                JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=DW.ID_SEMESTER
                JOIN AUCC.DOSEN D ON DW.ID_DOSEN=D.ID_DOSEN
                JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
                JOIN AUCC.DEPARTEMEN DEP ON DEP.ID_DEPARTEMEN = PS.ID_DEPARTEMEN
                WHERE HEW.ID_FAKULTAS='{$fakultas}' AND HEW.KDSEMESTER='{$semester}' {$q_prodi}
            GROUP BY D.ID_DOSEN,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,DEP.NM_DEPARTEMEN
            ORDER BY NM_PENGGUNA");
    }

    function load_laporan_evaluasi_wali($fakultas, $prodi, $semester) {
        $data_laporan = array();
        foreach ($this->load_row_evaluasi_wali($fakultas, $prodi, $semester) as $row) {
            $jumlah_peserta = $this->db->QuerySingle("SELECT COUNT(*) FROM (SELECT ID_MHS FROM AUCC.DOSEN_WALI WHERE ID_DOSEN='{$row['ID_DOSEN']}' GROUP BY ID_MHS)");
            $row_hasil = $this->db->QueryToArray("
                SELECT EV.*,
                    (A1+A2+A3+A4+A5+A6+A7+A8)/8 RERATA_DOSEN,
                    (((A1+A2+A3+A4+A5+A6+A7+A8)/8)/4)*100 RERATA_PERSEN_DOSEN
                    FROM
                    ( SELECT DW.ID_DOSEN,
                        AVG(
                        CASE WHEN A1<=4
                        THEN A1
                        END
                        ) A1,
                        AVG(
                        CASE WHEN A2<=4
                        THEN A2
                        END
                        ) A2,
                        AVG(
                        CASE WHEN A3<=4
                        THEN A3
                        END
                        ) A3,
                        AVG(
                        CASE WHEN A4<=4
                        THEN A4
                        END
                        ) A4,
                        AVG(
                        CASE WHEN A5<=4
                        THEN A5
                        END
                        ) A5,
                        AVG(
                        CASE WHEN A6<=4
                        THEN A6
                        END
                        ) A6,
                        AVG(
                        CASE WHEN A7<=4
                        THEN A7
                        END
                        ) A7,
                        AVG(
                        CASE WHEN A8<=4
                        THEN A8
                        END
                        ) A8
                        FROM AUCC.HASIL_EV_WALI HEW
                    JOIN AUCC.MAHASISWA M ON M.NIM_MHS = HEW.NIM
                    JOIN AUCC.DOSEN_WALI DW ON DW.ID_MHS = M.ID_MHS
                    JOIN AUCC.SEMESTER S ON S.ID_SEMESTER = DW.ID_SEMESTER
                    WHERE DW.ID_DOSEN='{$row['ID_DOSEN']}' 
                    GROUP BY DW.ID_DOSEN
                    ) EV");
            array_push($data_laporan, array_merge($row, array(
                        'DATA_HASIL' => $row_hasil,
                        'JUMLAH_PESERTA' => $jumlah_peserta)
                    )
            );
        }
        return $data_laporan;
    }

    function load_laporan_rerata_departemen_evaluasi_wali($prodi, $semester) {
        $query = "SELECT ID_DEPARTEMEN,NM_DEPARTEMEN,
                    (A1+A2+A3+A4+A5+A6+A7+A8)/8 RERATA,
                    (((A1+A2+A3+A4+A5+A6+A7+A8)/8)/4)*100 RERATA_PERSEN
                    FROM
                    (
                    SELECT PS.ID_DEPARTEMEN,DEP.NM_DEPARTEMEN,
                    AVG(
                        CASE WHEN A1<=4
                        THEN A1
                        END
                        ) A1,
                        AVG(
                        CASE WHEN A2<=4
                        THEN A2
                        END
                        ) A2,
                        AVG(
                        CASE WHEN A3<=4
                        THEN A3
                        END
                        ) A3,
                        AVG(
                        CASE WHEN A4<=4
                        THEN A4
                        END
                        ) A4,
                        AVG(
                        CASE WHEN A5<=4
                        THEN A5
                        END
                        ) A5,
                        AVG(
                        CASE WHEN A6<=4
                        THEN A6
                        END
                        ) A6,
                        AVG(
                        CASE WHEN A7<=4
                        THEN A7
                        END
                        ) A7,
                        AVG(
                        CASE WHEN A8<=4
                        THEN A8
                        END
                        ) A8
                    FROM AUCC.HASIL_EV_WALI HEW
                    JOIN AUCC.MAHASISWA M ON M.NIM_MHS=HEW.NIM
                    JOIN AUCC.DOSEN_WALI DW ON DW.ID_MHS=M.ID_MHS
                    JOIN AUCC.DOSEN D ON D.ID_DOSEN=DW.ID_DOSEN
                    JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
                    JOIN AUCC.DEPARTEMEN DEP ON PS.ID_DEPARTEMEN = DEP.ID_DEPARTEMEN
                    WHERE HEW.KDSEMESTER='{$semester}' AND HEW.ID_PROGRAM_STUDI='{$prodi}'
                    GROUP BY PS.ID_DEPARTEMEN,DEP.NM_DEPARTEMEN)";
        $this->db->Query($query);
        return $this->db->FetchAssoc();
    }

    function load_laporan_rerata_prodi_evaluasi_wali($prodi, $semester) {
        $query = "SELECT ID_PROGRAM_STUDI,NM_PROGRAM_STUDI,
                    (A1+A2+A3+A4+A5+A6+A7+A8)/8 RERATA,
                    (((A1+A2+A3+A4+A5+A6+A7+A8)/8)/4)*100 RERATA_PERSEN
                    FROM
                    (
                    SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,
                    AVG(
                        CASE WHEN A1<=4
                        THEN A1
                        END
                        ) A1,
                        AVG(
                        CASE WHEN A2<=4
                        THEN A2
                        END
                        ) A2,
                        AVG(
                        CASE WHEN A3<=4
                        THEN A3
                        END
                        ) A3,
                        AVG(
                        CASE WHEN A4<=4
                        THEN A4
                        END
                        ) A4,
                        AVG(
                        CASE WHEN A5<=4
                        THEN A5
                        END
                        ) A5,
                        AVG(
                        CASE WHEN A6<=4
                        THEN A6
                        END
                        ) A6,
                        AVG(
                        CASE WHEN A7<=4
                        THEN A7
                        END
                        ) A7,
                        AVG(
                        CASE WHEN A8<=4
                        THEN A8
                        END
                        ) A8
                    FROM AUCC.HASIL_EV_WALI HEW
                    JOIN AUCC.MAHASISWA M ON M.NIM_MHS=HEW.NIM
                    JOIN AUCC.DOSEN_WALI DW ON DW.ID_MHS=M.ID_MHS
                    JOIN AUCC.DOSEN D ON D.ID_DOSEN=DW.ID_DOSEN
                    JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
                    WHERE HEW.KDSEMESTER='{$semester}' AND HEW.ID_PROGRAM_STUDI='{$prodi}'
                    GROUP BY PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI)";
        $this->db->Query($query);
        return $this->db->FetchAssoc();
    }

    //Menu Evaluasi Mahasiswa Baru

    function load_row_evaluasi_maba($fakultas, $semester) {
        return $this->db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,COUNT(NIM) JUMLAH_RESPONDEN
            FROM AUCC.HASIL_EV_MABA HEM
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=HEM.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE PS.ID_FAKULTAS='{$fakultas}' AND HEM.KDSEMESTER='{$semester}' 
            GROUP BY PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
    }

    function load_laporan_evaluasi_maba($fakultas, $semester) {
        $data_laporan = array();
        foreach ($this->load_row_evaluasi_maba($fakultas, $semester) as $row) {
            $row_hasil = $this->db->QueryToArray("
                SELECT
                    H.*,
                    (A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21+A22+A23+A24+A25+A26+A27+A28+A29)/29 RERATA_PRODI,
                    (((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21+A22+A23+A24+A25+A26+A27+A28+A29)/29)/4)*100 RERATA_PERSEN_PRODI
                    FROM 
                    (
                    SELECT HEM.ID_PROGRAM_STUDI,
                        AVG(CASE WHEN A1<=4 THEN A1 END) A1,
                        AVG(CASE WHEN A2<=4 THEN A2 END) A2,
                        AVG(CASE WHEN A3<=4 THEN A3 END) A3,
                        AVG(CASE WHEN A4<=4 THEN A4 END) A4,
                        AVG(CASE WHEN A5<=4 THEN A5 END) A5,
                        AVG(CASE WHEN A6<=4 THEN A6 END) A6,
                        AVG(CASE WHEN A7<=4 THEN A7 END) A7,
                        AVG(CASE WHEN A8<=4 THEN A8 END) A8,
                        AVG(CASE WHEN A9<=4 THEN A9 END) A9,
                        AVG(CASE WHEN A10<=4 THEN A10 END) A10,
                        AVG(CASE WHEN A11<=4 THEN A11 END) A11,
                        AVG(CASE WHEN A12<=4 THEN A12 END) A12,
                        AVG(CASE WHEN A13<=4 THEN A13 END) A13,
                        AVG(CASE WHEN A14<=4 THEN A14 END) A14,
                        AVG(CASE WHEN A15<=4 THEN A15 END) A15,
                        AVG(CASE WHEN A16<=4 THEN A16 END) A16,
                        AVG(CASE WHEN A17<=4 THEN A17 END) A17,
                        AVG(CASE WHEN A18<=4 THEN A18 END) A18,
                        AVG(CASE WHEN A19<=4 THEN A19 END) A19,
                        AVG(CASE WHEN A20<=4 THEN A20 END) A20,
                        AVG(CASE WHEN A21<=4 THEN A21 END) A21,
                        AVG(CASE WHEN A22<=4 THEN A22 END) A22,
                        AVG(CASE WHEN A23<=4 THEN A23 END) A23,
                        AVG(CASE WHEN A24<=4 THEN A24 END) A24,
                        AVG(CASE WHEN A25<=4 THEN A25 END) A25,
                        AVG(CASE WHEN A26<=4 THEN A26 END) A26,
                        AVG(CASE WHEN A27<=4 THEN A27 END) A27,
                        AVG(CASE WHEN A28<=4 THEN A28 END) A28,
                        AVG(CASE WHEN A29<=4 THEN A29 END) A29
                    FROM AUCC.HASIL_EV_MABA HEM
                    WHERE HEM.ID_PROGRAM_STUDI='{$row['ID_PROGRAM_STUDI']}' AND HEM.KDSEMESTER='{$semester}'
                    GROUP BY HEM.ID_PROGRAM_STUDI
                    ) H");
            array_push($data_laporan, array_merge($row, array(
                        'DATA_HASIL' => $row_hasil)
                    )
            );
        }
        return $data_laporan;
    }

    function load_laporan_rerata_fakultas_evaluasi_maba($fakultas, $semester) {
        $query = "SELECT
                    H.*,
                    (A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21+A22+A23+A24+A25+A26+A27+A28+A29)/29 RERATA,
                    (((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21+A22+A23+A24+A25+A26+A27+A28+A29)/29)/4)*100 RERATA_PERSEN
                    FROM 
                    (
                    SELECT HEM.ID_FAKULTAS,
                        AVG(CASE WHEN A1<=4 THEN A1 END) A1,
                        AVG(CASE WHEN A2<=4 THEN A2 END) A2,
                        AVG(CASE WHEN A3<=4 THEN A3 END) A3,
                        AVG(CASE WHEN A4<=4 THEN A4 END) A4,
                        AVG(CASE WHEN A5<=4 THEN A5 END) A5,
                        AVG(CASE WHEN A6<=4 THEN A6 END) A6,
                        AVG(CASE WHEN A7<=4 THEN A7 END) A7,
                        AVG(CASE WHEN A8<=4 THEN A8 END) A8,
                        AVG(CASE WHEN A9<=4 THEN A9 END) A9,
                        AVG(CASE WHEN A10<=4 THEN A10 END) A10,
                        AVG(CASE WHEN A11<=4 THEN A11 END) A11,
                        AVG(CASE WHEN A12<=4 THEN A12 END) A12,
                        AVG(CASE WHEN A13<=4 THEN A13 END) A13,
                        AVG(CASE WHEN A14<=4 THEN A14 END) A14,
                        AVG(CASE WHEN A15<=4 THEN A15 END) A15,
                        AVG(CASE WHEN A16<=4 THEN A16 END) A16,
                        AVG(CASE WHEN A17<=4 THEN A17 END) A17,
                        AVG(CASE WHEN A18<=4 THEN A18 END) A18,
                        AVG(CASE WHEN A19<=4 THEN A19 END) A19,
                        AVG(CASE WHEN A20<=4 THEN A20 END) A20,
                        AVG(CASE WHEN A21<=4 THEN A21 END) A21,
                        AVG(CASE WHEN A22<=4 THEN A22 END) A22,
                        AVG(CASE WHEN A23<=4 THEN A23 END) A23,
                        AVG(CASE WHEN A24<=4 THEN A24 END) A24,
                        AVG(CASE WHEN A25<=4 THEN A25 END) A25,
                        AVG(CASE WHEN A26<=4 THEN A26 END) A26,
                        AVG(CASE WHEN A27<=4 THEN A27 END) A27,
                        AVG(CASE WHEN A28<=4 THEN A28 END) A28,
                        AVG(CASE WHEN A29<=4 THEN A29 END) A29
                    FROM AUCC.HASIL_EV_MABA HEM
                    WHERE HEM.ID_FAKULTAS='{$fakultas}' AND HEM.KDSEMESTER='{$semester}'
                    GROUP BY HEM.ID_FAKULTAS
                    ) H";
        $this->db->Query($query);
        return $this->db->FetchAssoc();
    }

    //Menu Evaluasi WIsuda

    function load_row_evaluasi_wisuda($fakultas, $semester) {
        return $this->db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,COUNT(NIM) JUMLAH_RESPONDEN
            FROM AUCC.HASIL_EV_WISUDA HEW
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=HEW.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE PS.ID_FAKULTAS='{$fakultas}' AND HEW.KDSEMESTER='{$semester}' 
            GROUP BY PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
    }

    function load_laporan_evaluasi_wisuda($fakultas, $semester) {
        $data_laporan = array();
        foreach ($this->load_row_evaluasi_wisuda($fakultas, $semester) as $row) {
            $row_hasil = $this->db->QueryToArray("
                SELECT
                    H.*,
                    (A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21+A22+A23+A24+A25+A26+A27+A28)/28 RERATA_PRODI,
                    (((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21+A22+A23+A24+A25+A26+A27+A28)/28)/4)*100 RERATA_PERSEN_PRODI
                    FROM 
                    (
                    SELECT HEW.ID_PROGRAM_STUDI,
                        AVG(CASE WHEN A1<=4 THEN A1 END) A1,
                        AVG(CASE WHEN A2<=4 THEN A2 END) A2,
                        AVG(CASE WHEN A3<=4 THEN A3 END) A3,
                        AVG(CASE WHEN A4<=4 THEN A4 END) A4,
                        AVG(CASE WHEN A5<=4 THEN A5 END) A5,
                        AVG(CASE WHEN A6<=4 THEN A6 END) A6,
                        AVG(CASE WHEN A7<=4 THEN A7 END) A7,
                        AVG(CASE WHEN A8<=4 THEN A8 END) A8,
                        AVG(CASE WHEN A9<=4 THEN A9 END) A9,
                        AVG(CASE WHEN A10<=4 THEN A10 END) A10,
                        AVG(CASE WHEN A11<=4 THEN A11 END) A11,
                        AVG(CASE WHEN A12<=4 THEN A12 END) A12,
                        AVG(CASE WHEN A13<=4 THEN A13 END) A13,
                        AVG(CASE WHEN A14<=4 THEN A14 END) A14,
                        AVG(CASE WHEN A15<=4 THEN A15 END) A15,
                        AVG(CASE WHEN A16<=4 THEN A16 END) A16,
                        AVG(CASE WHEN A17<=4 THEN A17 END) A17,
                        AVG(CASE WHEN A18<=4 THEN A18 END) A18,
                        AVG(CASE WHEN A19<=4 THEN A19 END) A19,
                        AVG(CASE WHEN A20<=4 THEN A20 END) A20,
                        AVG(CASE WHEN A21<=4 THEN A21 END) A21,
                        AVG(CASE WHEN A22<=4 THEN A22 END) A22,
                        AVG(CASE WHEN A23<=4 THEN A23 END) A23,
                        AVG(CASE WHEN A24<=4 THEN A24 END) A24,
                        AVG(CASE WHEN A25<=4 THEN A25 END) A25,
                        AVG(CASE WHEN A26<=4 THEN A26 END) A26,
                        AVG(CASE WHEN A27<=4 THEN A27 END) A27,
                        AVG(CASE WHEN A28<=4 THEN A28 END) A28
                    FROM AUCC.HASIL_EV_WISUDA HEW
                    WHERE HEW.ID_PROGRAM_STUDI='{$row['ID_PROGRAM_STUDI']}' AND HEW.KDSEMESTER='{$semester}'
                    GROUP BY HEW.ID_PROGRAM_STUDI
                    ) H");
            array_push($data_laporan, array_merge($row, array(
                        'DATA_HASIL' => $row_hasil)
                    )
            );
        }
        return $data_laporan;
    }

    function load_laporan_rerata_fakultas_evaluasi_wisuda($fakultas, $semester) {
        $query = "SELECT
                    H.*,
                    (A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21+A22+A23+A24+A25+A26+A27+A28)/28 RERATA,
                    (((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21+A22+A23+A24+A25+A26+A27+A28)/28)/4)*100 RERATA_PERSEN
                    FROM 
                    (
                    SELECT HEW.ID_FAKULTAS,
                        AVG(CASE WHEN A1<=4 THEN A1 END) A1,
                        AVG(CASE WHEN A2<=4 THEN A2 END) A2,
                        AVG(CASE WHEN A3<=4 THEN A3 END) A3,
                        AVG(CASE WHEN A4<=4 THEN A4 END) A4,
                        AVG(CASE WHEN A5<=4 THEN A5 END) A5,
                        AVG(CASE WHEN A6<=4 THEN A6 END) A6,
                        AVG(CASE WHEN A7<=4 THEN A7 END) A7,
                        AVG(CASE WHEN A8<=4 THEN A8 END) A8,
                        AVG(CASE WHEN A9<=4 THEN A9 END) A9,
                        AVG(CASE WHEN A10<=4 THEN A10 END) A10,
                        AVG(CASE WHEN A11<=4 THEN A11 END) A11,
                        AVG(CASE WHEN A12<=4 THEN A12 END) A12,
                        AVG(CASE WHEN A13<=4 THEN A13 END) A13,
                        AVG(CASE WHEN A14<=4 THEN A14 END) A14,
                        AVG(CASE WHEN A15<=4 THEN A15 END) A15,
                        AVG(CASE WHEN A16<=4 THEN A16 END) A16,
                        AVG(CASE WHEN A17<=4 THEN A17 END) A17,
                        AVG(CASE WHEN A18<=4 THEN A18 END) A18,
                        AVG(CASE WHEN A19<=4 THEN A19 END) A19,
                        AVG(CASE WHEN A20<=4 THEN A20 END) A20,
                        AVG(CASE WHEN A21<=4 THEN A21 END) A21,
                        AVG(CASE WHEN A22<=4 THEN A22 END) A22,
                        AVG(CASE WHEN A23<=4 THEN A23 END) A23,
                        AVG(CASE WHEN A24<=4 THEN A24 END) A24,
                        AVG(CASE WHEN A25<=4 THEN A25 END) A25,
                        AVG(CASE WHEN A26<=4 THEN A26 END) A26,
                        AVG(CASE WHEN A27<=4 THEN A27 END) A27,
                        AVG(CASE WHEN A28<=4 THEN A28 END) A28
                    FROM AUCC.HASIL_EV_WISUDA HEW
                    WHERE HEW.ID_FAKULTAS='{$fakultas}' AND HEW.KDSEMESTER='{$semester}'
                    GROUP BY HEW.ID_FAKULTAS
                    ) H";
        $this->db->Query($query);
        return $this->db->FetchAssoc();
    }

    //Menu Evaluasi Administrasi Departemen

    function load_row_evaluasi_adm_dep($fakultas, $semester) {
        return $this->db->QueryToArray("
            SELECT D.NM_DEPARTEMEN,D.ID_DEPARTEMEN,COUNT(ID_MHS) JUMLAH_RESPONDEN
            FROM AUCC.HASIL_EV_ADM_DEP_MHS H
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=H.ID_PROGRAM_STUDI
            JOIN AUCC.DEPARTEMEN D ON D.ID_DEPARTEMEN = PS.ID_DEPARTEMEN
            WHERE PS.ID_FAKULTAS='{$fakultas}' AND H.ID_SEMESTER='{$semester}' 
            GROUP BY D.NM_DEPARTEMEN,D.ID_DEPARTEMEN
            ORDER BY D.NM_DEPARTEMEN");
    }
  function load_row_evaluasi_kadep_dep($fakultas, $semester) {
        return $this->db->QueryToArray("
            SELECT D.NM_DEPARTEMEN,D.ID_DEPARTEMEN,COUNT(ID_MHS) JUMLAH_RESPONDEN
            FROM AUCC.HASIL_EV_ADM_DEP_MHS H
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=H.ID_PROGRAM_STUDI
            JOIN AUCC.DEPARTEMEN D ON D.ID_DEPARTEMEN = PS.ID_DEPARTEMEN
            WHERE PS.ID_FAKULTAS='{$fakultas}' AND H.ID_SEMESTER='{$semester}' 
            GROUP BY D.NM_DEPARTEMEN,D.ID_DEPARTEMEN
            ORDER BY D.NM_DEPARTEMEN");
    }
    function load_laporan_evaluasi_adm_dep($fakultas, $semester) {
        $data_laporan = array();
        foreach ($this->load_row_evaluasi_adm_dep($fakultas, $semester) as $row) {
            $row_hasil = $this->db->QueryToArray("
                SELECT
                    H.*,
                    (A1+A2+A3+A4+A5+A6+A7+A8)/8 RERATA_DEP,
                    (((A1+A2+A3+A4+A5+A6+A7+A8)/8)/4)*100 RERATA_PERSEN_DEP
                    FROM 
                    (
                    SELECT PS.ID_DEPARTEMEN,
                        AVG(CASE WHEN A1<=4 THEN A1 END) A1,
                        AVG(CASE WHEN A2<=4 THEN A2 END) A2,
                        AVG(CASE WHEN A3<=4 THEN A3 END) A3,
                        AVG(CASE WHEN A4<=4 THEN A4 END) A4,
                        AVG(CASE WHEN A5<=4 THEN A5 END) A5,
                        AVG(CASE WHEN A6<=4 THEN A6 END) A6,
                        AVG(CASE WHEN A7<=4 THEN A7 END) A7,
                        AVG(CASE WHEN A8<=4 THEN A8 END) A8
                        FROM AUCC.HASIL_EV_ADM_DEP_MHS H
                    JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=H.ID_PROGRAM_STUDI
                    WHERE PS.ID_DEPARTEMEN='{$row['ID_DEPARTEMEN']}' AND H.ID_SEMESTER='{$semester}'
                    GROUP BY PS.ID_DEPARTEMEN
                    ) H");
            array_push($data_laporan, array_merge($row, array(
                        'DATA_HASIL' => $row_hasil)
                    )
            );
        }
        return $data_laporan;
    }
////
function load_laporan_evaluasi_kadep_dep($fakultas, $semester) {
        $data_laporan = array();
        foreach ($this->load_row_evaluasi_kadep_dep($fakultas, $semester) as $row) {
            $row_hasil = $this->db->QueryToArray("
                SELECT
                    H.*,
                    (A1+A2+A3+A4+A5+A6+A7+A8)/8 RERATA_DEP,
                    (((A1+A2+A3+A4+A5+A6+A7+A8)/8)/4)*100 RERATA_PERSEN_DEP
                    FROM 
                    (
                    SELECT PS.ID_DEPARTEMEN,
                        AVG(CASE WHEN A1<=4 THEN A1 END) A1,
                        AVG(CASE WHEN A2<=4 THEN A2 END) A2,
                        AVG(CASE WHEN A3<=4 THEN A3 END) A3,
                        AVG(CASE WHEN A4<=4 THEN A4 END) A4,
                        AVG(CASE WHEN A5<=4 THEN A5 END) A5,
                        AVG(CASE WHEN A6<=4 THEN A6 END) A6,
                        AVG(CASE WHEN A7<=4 THEN A7 END) A7,
                        AVG(CASE WHEN A8<=4 THEN A8 END) A8
                        FROM AUCC.HASIL_EV_ADM_DEP_MHS H
                    JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=H.ID_PROGRAM_STUDI
                    WHERE PS.ID_DEPARTEMEN='{$row['ID_DEPARTEMEN']}' AND H.ID_SEMESTER='{$semester}'
                    GROUP BY PS.ID_DEPARTEMEN
                    ) H");
            array_push($data_laporan, array_merge($row, array(
                        'DATA_HASIL' => $row_hasil)
                    )
            );
        }
        return $data_laporan;
    }
////
    //Menu Evaluasi Administrasi Fakultas

    function load_laporan_evaluasi_adm_fak($semester) {
        return $this->db->QueryToArray("
                SELECT
                    H.*,
                    (A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21+A22+A23+A24+A25+A26+A27+A28+A29+A30+A31+A32+A33+A34+A35)/35 RERATA_FAKULTAS,
                    (((A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20+A21+A22+A23+A24+A25+A26+A27+A28+A29+A30+A31+A32+A33+A34+A35)/35)/4)*100 RERATA_PERSEN_FAKULTAS
                    FROM 
                    (
                    SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,COUNT(ID_MHS) JUMLAH_RESPONDEN,
                        AVG(CASE WHEN A1<=4 THEN A1 END) A1,
                        AVG(CASE WHEN A2<=4 THEN A2 END) A2,
                        AVG(CASE WHEN A3<=4 THEN A3 END) A3,
                        AVG(CASE WHEN A4<=4 THEN A4 END) A4,
                        AVG(CASE WHEN A5<=4 THEN A5 END) A5,
                        AVG(CASE WHEN A6<=4 THEN A6 END) A6,
                        AVG(CASE WHEN A7<=4 THEN A7 END) A7,
                        AVG(CASE WHEN A8<=4 THEN A8 END) A8,
                        AVG(CASE WHEN A9<=4 THEN A9 END) A9,
                        AVG(CASE WHEN A10<=4 THEN A10 END) A10,
                        AVG(CASE WHEN A11<=4 THEN A11 END) A11,
                        AVG(CASE WHEN A12<=4 THEN A12 END) A12,
                        AVG(CASE WHEN A13<=4 THEN A13 END) A13,
                        AVG(CASE WHEN A14<=4 THEN A14 END) A14,
                        AVG(CASE WHEN A15<=4 THEN A15 END) A15,
                        AVG(CASE WHEN A16<=4 THEN A16 END) A16,
                        AVG(CASE WHEN A17<=4 THEN A17 END) A17,
                        AVG(CASE WHEN A18<=4 THEN A18 END) A18,
                        AVG(CASE WHEN A19<=4 THEN A19 END) A19,
                        AVG(CASE WHEN A20<=4 THEN A20 END) A20,
                        AVG(CASE WHEN A21<=4 THEN A21 END) A21,
                        AVG(CASE WHEN A22<=4 THEN A22 END) A22,
                        AVG(CASE WHEN A23<=4 THEN A23 END) A23,
                        AVG(CASE WHEN A24<=4 THEN A24 END) A24,
                        AVG(CASE WHEN A25<=4 THEN A25 END) A25,
                        AVG(CASE WHEN A26<=4 THEN A26 END) A26,
                        AVG(CASE WHEN A27<=4 THEN A27 END) A27,
                        AVG(CASE WHEN A28<=4 THEN A28 END) A28,
                        AVG(CASE WHEN A29<=4 THEN A29 END) A29,
                        AVG(CASE WHEN A30<=4 THEN A30 END) A30,
                        AVG(CASE WHEN A31<=4 THEN A31 END) A31,
                        AVG(CASE WHEN A32<=4 THEN A32 END) A32,
                        AVG(CASE WHEN A33<=4 THEN A33 END) A33,
                        AVG(CASE WHEN A34<=4 THEN A34 END) A34,
                        AVG(CASE WHEN A35<=4 THEN A35 END) A35
                    FROM AUCC.HASIL_EV_AF H
                    JOIN AUCC.FAKULTAS F ON F.ID_FAKULTAS=H.ID_FAKULTAS
                    WHERE H.ID_SEMESTER='{$semester}'
                    GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
                    ORDER BY ID_FAKULTAS
                    ) H");
    }

}

?>
