<div class="center_title_bar">Laporan Evaluasi Ketua Departemen</div>
<form method="get" id="report_form" action="evkadep.php">
    <table class="ui-widget-content" style="width: 900px">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select name="fakultas" id="fakultas">
                    {foreach $data_fakultas as $data}
                        {if $data.ID_FAKULTAS==$id_fakultas}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
            <td>Semester</td>
            <td>
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$smarty.get.semester}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="center">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>

{if isset($data_evaluasi)}
    
{/if}

{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}