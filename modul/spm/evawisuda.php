<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';
include '../ppm/class/evaluasi.class.php';

$list = new list_data($db);
$eva = new evaluasi($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $periode = get('periode');
        $smarty->assign('header_kelompok_aspek', $db->QueryToArray("
            SELECT EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK,COUNT(EA.ID_EVAL_ASPEK)  JUMLAH_ASPEK
            FROM EVALUASI_KELOMPOK_ASPEK EKS
            JOIN EVALUASI_ASPEK EA ON EA.ID_EVAL_KELOMPOK_ASPEK=EKS.ID_EVAL_KELOMPOK_ASPEK AND EA.TIPE_ASPEK=1 AND EA.ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_HASIL WHERE ID_TARIF_WISUDA='{$periode}')
            WHERE EKS.ID_EVAL_INSTRUMEN=5
            GROUP BY EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK
            ORDER BY NAMA_KELOMPOK
            ")
        );
        $smarty->assign('header_aspek', $db->QueryToArray("SELECT * FROM EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=5 AND TIPE_ASPEK=1 AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_HASIL WHERE ID_TARIF_WISUDA='{$periode}') ORDER BY ID_EVAL_ASPEK"));
        $smarty->assign('data_evaluasi', $eva->load_laporan_evaluasi_wisuda(get('fakultas'), get('periode')));
        $smarty->assign('data_rata_fakultas', $eva->load_laporan_rerata_fakultas_evaluasi_wisuda(get('fakultas'), get('periode')));
    }
}

$smarty->assign('data_periode', $db->QueryToArray("
    SELECT * FROM TARIF_WISUDA 
    WHERE ID_TARIF_WISUDA IN (
        SELECT ID_TARIF_WISUDA 
        FROM PERIODE_WISUDA
        WHERE TO_CHAR(TGL_BAYAR_SELESAI,'YYYY')=TO_CHAR(SYSDATE,'YYYY')
        OR TO_CHAR(TGL_BAYAR_SELESAI,'YYYY')=(TO_CHAR(SYSDATE,'YYYY')-1)
    ) 
    ORDER BY ID_TARIF_WISUDA DESC"));
$smarty->assign('id_fakultas',$user->ID_FAKULTAS);
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->display('evawisuda.tpl');
?>
