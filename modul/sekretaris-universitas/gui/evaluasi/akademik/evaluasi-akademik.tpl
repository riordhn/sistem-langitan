<div class="center_title_bar">Evaluasi Akademik</div>
<form action="evaluasi-akademik.php" method="post" name="f2" id="f2">
	<table width="700">
    	<tr>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas">
					<option value="">Semua</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $id_fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
			<td>Jenjang</td>
            <td>
            	<select name="jenjang" id="jenjang">
					<option value="all">Semua Jenjang</option>
                    <option value="0" {if $id_jenjang=="0"} selected="selected" {/if}>S1 (Alih Jenis)</option>
                	{foreach $jenjang as $data}
                	<option value="{$data.ID_JENJANG}" {if $id_jenjang==$data.ID_JENJANG} selected="selected" {/if}>{$data.NM_JENJANG}</option>
					{/foreach}
            	</select>
            </td>
			<td>Tahun</td>
            <td>
            	<select name="tahun" id="tahun">
                	<option value="">Pilih Tahun</option>
						{if $id_jenjang == 1}
							<option value="2" {if $tahun==2}selected="selected"{/if}>2</option>
							<option value="4" {if $tahun==4}selected="selected"{/if}>4</option>
						{elseif $id_jenjang == 5}
							<option value="1" {if $tahun==1}selected="selected"{/if}>1</option>
							<option value="3" {if $tahun==3}selected="selected"{/if}>3</option>
                        {elseif $id_jenjang == '0'}
							<option value="1" {if $tahun==1}selected="selected"{/if}>1</option>
							<option value="3" {if $tahun==3}selected="selected"{/if}>3</option>
						{/if}	
            	</select>
            </td>
			<td>Semester</td>
            <td>
            	<select name="semester" class="required">
					<option value="">Pilih Semester</option>
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $id_semester==$data.ID_SEMESTER or $data.ID_SEMESTER==$semester_aktif} selected="selected" {/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="8" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($evaluasi)}
<form action="evaluasi-akademik.php" name="f1" id="f1" method="post">
<table style="font-size:11px">
	<tr>
		<th style="text-align:center">NO</th>
		<th style="text-align:center">NIM</th>
		<th style="text-align:center">NAMA</th>
		<th style="text-align:center">JENJANG</th>
		<th style="text-align:center">PRODI</th>
		<th style="text-align:center">STATUS TERKINI</th>
		<th style="text-align:center">SKS DIWAJIBKAN</th>
		<th style="text-align:center">SKS DIPEROLEH</th>
		<th style="text-align:center">IPK</th>
        <th style="text-align:center">SKS DENGAN IPK 2</th>
		<th style="text-align:center">% NILAI D</th>
		<th style="text-align:center">PIUTANG SEMESTER</th>
		<th style="text-align:center">CUTI SEMESTER</th>
		<th style="text-align:center">SEMESTER MASUK MHS</th>
		<th style="text-align:center">REKOMENDASI STATUS</th>
		<th style="text-align:center">TGL REKOMENDASI STATUS</th>
   		<th style="text-align:center">KETERANGAN</th>
	</tr>
	{$no = 1}
	{foreach $evaluasi as $data}
	<tr>
		<td>{$no++}</td>
		<td><a href="evaluasi-status-mhs.php?nim={$data.NIM_MHS}" target="_blank">{$data.NIM_MHS}</a></td>
		<td>{$data.NM_PENGGUNA}</td>
		<td>{$data.NM_JENJANG} {if $data.ID_JALUR == 4 or $data.ID_JALUR == 22}(Alih Jenis){/if}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
		<td>{$data.NM_STATUS_PENGGUNA}</td>
		<td>{$data.SKS_WAJIB}</td>
		<td>{$data.SKS}</td>
		<td>{$data.IPK}</td>
        <td>{$data.SKS_IPK_2}</td>
		<td>{$data.PERSEN_NILAI_D}</td>
		<td>{$data.PIUTANG}</td>
		<td>{$data.CUTI}</td>
		<td>{$data.NM_SEMESTER}</td>
		<td>
					<input type="hidden" value="{$data.ID_MHS}" name="id_mhs{$no}" />
					<input type="hidden" value="{$data.SKS}" name="sks{$no}" />
					<input type="hidden" value="{$data.IPK}" name="ipk{$no}" />
					<input type="hidden" value="{$data.ID_STATUS_PENGGUNA}" name="status_terkini{$no}" />
			<select name="rekomendasi{$no}">
				<option value="">Pilih Status</option>
				{foreach $status as $x}
					<option value="{$x.ID_STATUS_EVALUASI}" {if $data.REKOMENDASI_STATUS==$x.ID_STATUS_EVALUASI}selected="selected"{/if}>{$x.NM_STATUS_EVALUASI}</option>
				{/foreach}
			</select>
		</td>
		<td><input name="tgl_rekomendasi{$no}" type="text" class="datepicker" size="8" value="{$data.TGL_REKOMENDASI_STATUS}"/></td>
		<td><textarea name="keterangan{$no}" rows="4" cols="30">{$data.KETERANGAN}</textarea></td>
<!--	<td>    
        	<input type="button" value="Detail"  onclick="show_detail('{$data@index+1}','#detail','{$data.ID_MHS}','#detail2')"/>
      </td>
-->	</tr>
    <tr style="display:none" id="detail{$data@index+1}">
    	<td colspan="18">
        	<table style="width:100%" id="detail2{$data@index+1}">
               
                
            </table>
    	</td>
    </tr>
	{/foreach}
</table>
</form>
{/if}

{literal}
    <script>
			$("#f2").validate();
			$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getTahun.php',
                data:'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#tahun').html(data);
                }                    
           	 })
        	});
            $(".datepicker").datepicker({dateFormat:'dd-mm-yy',changeMonth: true,changeYear: true});
			
			function show_detail(index,tag,id_mhs,tag2){
				$.ajax({
                type:'get',
                url:'getEvaluasi.php',
                data:'id_mhs='+id_mhs,
                success:function(data){
                    $(tag2+index).html(data);
                }                    
           		})
				
            	$(tag+index).fadeToggle("slow");

			}

    </script>
{/literal}
