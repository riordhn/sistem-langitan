<div class="center_title_bar">Pengajuan Evaluasi</div>
<form action="evaluasi-pengajuan.php" method="post" name="f2" id="f2">
	<table width="700">
    	<tr>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas">
					<option value="">Semua Fakultas</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $id_fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
            <td>Semester</td>
            <td>
            	<select name="semester" class="required">
                	<option value="">Pilih Semester</option>
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $id_semester==$data.ID_SEMESTER or $data.ID_SEMESTER==$semester_aktif} selected="selected" {/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($evaluasi)}
<form action="evaluasi-pengajuan.php" name="f1" id="f1" method="post">
<table style="font-size:11px">
	<tr>
		<th style="text-align:center">NO</th>
		<th style="text-align:center">NIM</th>
		<th style="text-align:center">NAMA</th>
        <th style="text-align:center">JENJANG</th>
		<th style="text-align:center">PRODI</th>
		<th style="text-align:center">STATUS TERKINI</th>
		<th style="text-align:center">EVALUASI BWS</th>
        <th style="text-align:center">EVALUASI AKADEMIK</th>
        <th style="text-align:center">EVALUASI ADMINISTRASI</th>
        <th style="text-align:center">HASIL PENETAPAN</th>
        
	</tr>
	{$no = 1}
	{foreach $evaluasi as $data}
	<tr {if $data.PENGAJUAN_EVALUASI == 1}bgcolor="#E0EBFF"{/if}>
		<td>{$no++}</td>
		<td><a href="evaluasi-status-mhs.php?nim={$data.NIM_MHS}" target="_blank">{$data.NIM_MHS}</a></td>
		<td>{$data.NM_PENGGUNA}</td>
        <td>{$data.NM_JENJANG}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
		<td>{$data.STATUS_TERKINI}</td>
		<td>{$data.REKOMENDASI_STATUS_BWS}</td>
        <td>{$data.REKOMENDASI_STATUS_AKADEMIK}</td>
        <td>{$data.REKOMENDASI_ADMINISTRASI}</td>
        <td>
			{$data.PENETAPAN}

        </td>
    </tr>
	{/foreach}
</table>
</form>
{/if}

{literal}
    <script>
			$("#f2").validate();
            $(".datepicker").datepicker({dateFormat:'dd-mm-yy',changeMonth: true,changeYear: true});
			$("#cek_all").click(function(){
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
			});
    </script>
{/literal}
