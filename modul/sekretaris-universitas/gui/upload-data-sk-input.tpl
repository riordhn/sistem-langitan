{literal}
    <style>
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }
        * {
            font: 12px/1.5 'Trebuchet MS', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
        }
        /* --- Content Style --- */
        .content { font-size: 13px; }

        .content .center_title_bar {
            background: #145554;
            border-radius: 5px;
            color: #fff;
            font-size: 14px;
            font-weight: bold;
            margin: 0px -5px 20px -5px;
            padding: 10px;
            text-transform: uppercase;
        }
        .content h1 { font-size: 20px; }
        .content h2 { font-size: 18px; }
        .content h3 { font-size: 16px; }
        .content h4 { font-size: 14px; }

        .content table {
            width: auto;
        }

        .content table caption { 
            padding: 2px; 
            font-size: 14px;
            font-weight: bold;
        }

        .content table tr.row1 { background-color: #eee; }
        .content table tr.row2 { background-color: #fff; }
        .content table tr.row3 { }


        .content table tr th {
            background: #0c4443;
            border: 1px solid #969BA5;
            color: #fff;
            font-weight: bold;
            padding: 10px;
            text-transform :uppercase;
        }

        .content table tr td {
            border: 1px solid #969BA5;
            padding: 8px;
            vertical-align: middle;
        }
        .link{
            background-color:white;
            font-weight:bold;
            padding:15px;
        }
        .link_button{
            background-color:#056303;
            font-weight:bold;
            border:1px solid #cccccc;
            color:white;
        }

        .content .button{
            margin:3px;
            padding:8px;
            cursor:pointer;
            background-color:#0c4443;
            color: white;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            text-decoration:none;
            border: none;
            font-size:13px;
            display:inline-block;
        }
        .content .button:hover{
            color: #f8e861;

        }

        .small { font-size: 80%; }
        .center {
            text-align: center;
        }
        .middle {
            vertical-align: middle;
        }
        .cursor-pointer {
            cursor: pointer;
        }

        .content button {
            cursor: pointer;
        }
        .content a {
            color: #008000;
        }
        .content a:hover {
            color: #00aa00;
        }
        .content a:active {
            color: #00bb00;
        }
        .content pre {
            font-size: 11px;
        }
        .anchor-span {
            color: #008000;
            cursor: pointer;
            text-decoration: underline;
        }
        .anchor-span:hover {
            color: #00aa00;
        }

        .anchor-black {
            color: #000 !important;
            text-decoration: none;
        }
        .anchor-black:hover {
            color: #000 !important;
            text-decoration: underline;
        }

        /* FORM CONTENT */
        .content input[type=submit], .radio {
            cursor: pointer;
        }
        label.error {
            color: #c00;
            font-size: 12px;
        }
        .data-kosong{
            color:red;
            text-align:center;
        }
        .total{
            background-color:#c9f797;
            font-weight:bold;
        }
    </style>
{/literal}
<link rel="stylesheet" type="text/css" href="{$base_url}css/custom-theme/jquery-ui-smoothness.css" />

<script type="text/javascript" src="{$base_url}js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="{$base_url}js/jquery-ui-1.8.11.custom.min.js"></script>
<script type="text/javascript" src="{$base_url}js/jquery.validate.min.js"></script>
<script type="text/javascript" src="{$base_url}js/additional-methods.min.js"></script>
<div class="content">
    <form action="sk-input-data.php" method="post" id="add"  enctype="multipart/form-data">
        <table class="ui-widget-content" style="width: 70%">
            <tr>
                <th colspan="2" class="center">Upload Excel - Data Surat Keputusan</th>
            </tr>
            <tr>
                <td>Nama Surat Keputusan</td>
                <td>
                    {$data.NM_SK_MASTER}
                </td>
            </tr>
            <tr>
                <td>NO SK</td>
                <td>{$data.NO_SK_DATA}</td>
            </tr>
            <tr>
                <td>Tipe Data</td>
                <td>
                    <select name="tipe">
                        <option value="1">Mahasiswa</option>
                        <option value="2">Calon Mahasiswa</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>File Data</td>
                <td><input class="required" type="file" name="file"/></td>
            </tr>
            <tr>
                <td class="center" colspan="2">
                    <span style="color: #00aa00;font-style: italic;font-size: 0.8em">Format File excel yang digunakan di upload Harus sesuai template</span><br/>
                    <input type="hidden" name="mode" value="upload-excel"/>
                    <input type="hidden" name="id_sk_data" value="{$data.ID_SK_DATA}"/>
                    <a class="button" href="sk-input-data.php">Kembali</a>
                    <a class="button" href="excel-data-detail-template.php">Download Template</a>
                    <input type="submit" class="button" value="Upload"/>
                </td>
            </tr>
        </table>
    </form>
</div>
{literal}
    <script>
        $('form').validate()
    </script>
{/literal}