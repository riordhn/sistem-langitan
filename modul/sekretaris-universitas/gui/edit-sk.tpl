<div class="center_title_bar">Edit - Master Surat Keputusan</div>
<form action="sk.php" method="post" id="edit_group">
    <table class="ui-widget-content" style="width: 70%">
        <tr>
            <th colspan="2" class="center">Edit Surat Keputusan</th>
        </tr>
        <tr>
            <td>Jenis Surat Keputusan</td>
            <td>
                <select name="jenis">
                    {foreach $data_jenis as $dj}
                        <option value="{$dj.ID_SK_JENIS}" {if $data_sk.ID_SK_JENIS==$dj.ID_SK_JENIS}selected="true"{/if}>{$dj.NM_SK_JENIS}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama</td>
            <td><input class="required" type="text" name="nama" value="{$data_sk.NM_SK_MASTER}"/></td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td>
                <textarea name="keterangan">{$data_sk.KETERANGAN_SK_MASTER}</textarea>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" value="{$data_sk.ID_SK_MASTER}"/>
                <a class="disable-ajax button" onclick="history.back(-1)">Kembali</a>
                <input type="submit" class="button" value="Update"/>
            </td>
        </tr>
    </table>
</form>