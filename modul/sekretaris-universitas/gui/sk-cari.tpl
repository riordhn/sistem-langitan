<div class="center_title_bar">Pencarian File/Data Surat Keputusan</div>
<form method="get" action="sk-cari.php">
    <table style="width: 70%">
        <tr>
            <th colspan="2" class="center">Parameter</th>
        </tr>
        <tr>
            <td>Masukkan NO SK/Nama SK/Nama File/Nama Identitas Data</td>
            <td>
                <input type="search" name="cari" value="{$smarty.get.cari}"/>
                <input type="submit" class="button" value="Cari"/>
            </td>
        </tr>
    </table>
</form>
{if isset($data_hasil)}
    {if count($data_hasil)>0}
        <span style="color: green">{count($data_hasil)} Data Ditemukan ,Dengan Kata Kunci '{$smarty.get.cari}'</span><br/><br/>
        <table class="ui-widget-content" style="width: 100%">
            <tr>
                <th class="center" colspan="6">Hasil Pencarian Surat Keputusan</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>NAMA</th>
                <th>NO SK</th>
                <th>FILE</th>
                <th>KETERANGAN</th>
                <th>TGL INPUT DATA</th>
            </tr>
            {foreach $data_hasil as $d}
                <tr>
                    <td>{$d@index+1}</td>
                    <td>{$d.NM_SK_MASTER}</td>
                    <td>{$d.NO_SK_DATA}</td>
                    <td>
                        <a class="disable-ajax" href="file-sk/{$d.FILE_SK_DATA}" target="_blank">{$d.FILE_SK_DATA}</a>
                    </td>
                    <td>{$d.KETERANGAN_SK_DATE}</td>
                    <td>{$d.WAKTU}</td>
                </tr>
            {/foreach}
        </table>
    {else}
        <span style="color: red">Mohon Maaf Pencarian Tidak Ditemukan</span>
    {/if}
{/if}