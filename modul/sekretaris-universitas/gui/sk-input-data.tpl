{literal}
    <style>
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }
        * {
            font: 12px/1.5 'Trebuchet MS', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
        }
        /* --- Content Style --- */
        .content { font-size: 13px; }

        .content .center_title_bar {
            background: #145554;
            border-radius: 5px;
            color: #fff;
            font-size: 14px;
            font-weight: bold;
            margin: 0px -5px 20px -5px;
            padding: 10px;
            text-transform: uppercase;
        }
        .content h1 { font-size: 20px; }
        .content h2 { font-size: 18px; }
        .content h3 { font-size: 16px; }
        .content h4 { font-size: 14px; }

        .content table {
            width: auto;
        }

        .content table caption { 
            padding: 2px; 
            font-size: 14px;
            font-weight: bold;
        }

        .content table tr.row1 { background-color: #eee; }
        .content table tr.row2 { background-color: #fff; }
        .content table tr.row3 { }


        .content table tr th {
            background: #0c4443;
            border: 1px solid #969BA5;
            color: #fff;
            font-weight: bold;
            padding: 10px;
            text-transform :uppercase;
        }

        .content table tr td {
            border: 1px solid #969BA5;
            padding: 8px;
            vertical-align: middle;
        }
        .link{
            background-color:white;
            font-weight:bold;
            padding:15px;
        }
        .link_button{
            background-color:#056303;
            font-weight:bold;
            border:1px solid #cccccc;
            color:white;
        }

        .content .button{
            margin:3px;
            padding:8px;
            cursor:pointer;
            background-color:#0c4443;
            color: white;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            text-decoration:none;
            border: none;
            font-size:13px;
            display:inline-block;
        }
        .content .button:hover{
            color: #f8e861;

        }

        .small { font-size: 80%; }
        .center {
            text-align: center;
        }
        .middle {
            vertical-align: middle;
        }
        .cursor-pointer {
            cursor: pointer;
        }

        .content button {
            cursor: pointer;
        }
        .content a {
            color: #008000;
        }
        .content a:hover {
            color: #00aa00;
        }
        .content a:active {
            color: #00bb00;
        }
        .content pre {
            font-size: 11px;
        }
        .anchor-span {
            color: #008000;
            cursor: pointer;
            text-decoration: underline;
        }
        .anchor-span:hover {
            color: #00aa00;
        }

        .anchor-black {
            color: #000 !important;
            text-decoration: none;
        }
        .anchor-black:hover {
            color: #000 !important;
            text-decoration: underline;
        }

        /* FORM CONTENT */
        .content input[type=submit], .radio {
            cursor: pointer;
        }
        label.error {
            color: #c00;
            font-size: 12px;
        }
        .data-kosong{
            color:red;
            text-align:center;
        }
        .total{
            background-color:#c9f797;
            font-weight:bold;
        }
    </style>
{/literal}
<link rel="stylesheet" type="text/css" href="{$base_url}css/custom-theme/jquery-ui-smoothness.css" />

<script type="text/javascript" src="{$base_url}js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="{$base_url}js/jquery-ui-1.8.11.custom.min.js"></script>
<script type="text/javascript" src="{$base_url}js/jquery.validate.min.js"></script>
<script type="text/javascript" src="{$base_url}js/additional-methods.min.js"></script>

<div class="content">
    <table class="ui-widget-content" style="width: 100%">
        <tr>
            <th class="center" colspan="7">Data Surat Keputusan</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>NAMA</th>
            <th>NO SK</th>
            <th>FILE</th>
            <th>KETERANGAN</th>
            <th>TGL INPUT DATA</th>
            <th>OPERASI</th>
        </tr>
        {foreach $data as $d}
            <tr>
                <td>{$d@index+1}</td>
                <td>{$d.NM_SK_MASTER}</td>
                <td>{$d.NO_SK_DATA}</td>
                <td>
                    <a href="file-sk/{$d.FILE_SK_DATA}" target="_blank">{$d.FILE_SK_DATA}</a>
                </td>
                <td>{$d.KETERANGAN_SK_DATE}</td>
                <td>{$d.WAKTU}</td>
                <td class="center">
                    <a class="button" href="sk-input-data.php?mode=upload&id={$d.ID_SK_DATA}">Upload Data</a>
                    <a class="button" href="sk-input-data.php?mode=edit&id={$d.ID_SK_DATA}">Edit</a>
                    <a class="button" href="sk-input-data.php?mode=delete&id={$d.ID_SK_DATA}">Delete</a>
                    <a class="button" href="sk-input-data.php?mode=detail&id={$d.ID_SK_DATA}">Detail</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="7" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="7" class="center">
                <a class="button" href="sk-input-data.php?mode=add">Tambah</a>
            </td>
        </tr>
    </table>
</div>
