<div class="center_title_bar">Pencarian Dosen</div>
{if $smarty.get.mode=='' or $smarty.get.mode=='cari'}
    <form method="get" action="cari-dosen.php">
        <table>
            <tr>
                <th colspan="2" class="center"> Pencarian</th>
            </tr>
            <tr>
                <td>Cari Nama/NIP</td>
                <td>
                    <input type="text" size="30" name="cari" value="{$smarty.get.cari}" />
                    <input type="submit" value="Cari" class="button" />
                    <input type="hidden" name="mode" value="cari"/>
                </td>
            </tr>
        </table>
    </form>
{/if}
{if isset($smarty.get.mode)}
    {if isset($data_biodata)}
        <a class="button disable-ajax" onclick="history.back(-1)">Kembali</a>
        <p></p>
        <table class="ui-widget ui-widget-content" style="width: 100%">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn">Detail Biodata Dosen</th>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <img src="{$foto}" width="180" height="230"/>
                </td>
            </tr>
            <tr>
                <td>NAMA</td>
                <td>{$data_biodata.GELAR_DEPAN} {$data_biodata.NM_PENGGUNA} {$data_biodata.GELAR_BELAKANG}</td>
            </tr>
            <tr>
                <td>NIP</td>
                <td>{$data_biodata.NIP_DOSEN}</td>
            </tr>
            <tr>
                <td>TEMPAT,TGL LAHIR</td>
                <td>{$data_biodata.KOTA_LAHIR},{$data_biodata.TGL_LAHIR_PENGGUNA}</td>
            </tr>
            <tr>
                <td>KELAMIN</td>
                <td>
                    {if $data_biodata.KELAMIN_PENGGUNA==1}
                        Laki-laki
                    {else}
                        Perempuan
                    {/if}
                </td>
            </tr>
            <tr>
                <td>PRODI</td>
                <td>{$data_biodata.NM_JENJANG} {$data_biodata.NM_PROGRAM_STUDI}</td>
            </tr>
            <tr>
                <td>ALAMAT RUMAH</td>
                <td>{$data_biodata.ALAMAT_RUMAH_DOSEN}</td>
            </tr>
            <tr>
                <td>ALAMAT KANTOR</td>
                <td>{$data_biodata.ALAMAT_KANTOR_DOSEN}</td>
            </tr>
            <tr>
                <td>TELP , MOBILE</td>
                <td>{$data_biodata.TELP_DOSEN}, {$data_biodata.MOBILE_DOSEN}</td>
            </tr>
            <tr>
                <td>EMAIL</td>
                <td>{$data_biodata.EMAIL_PENGGUNA}</td>
            </tr>
            <tr>
                <td>BLOG </td>
                <td>{$data_biodata.BLOG_PENGGUNA}</td>
            </tr>
            <tr>
                <td>GOLONGAN</td>
                <td>{$data_biodata.NM_GOLONGAN}</td>
            </tr>
            <tr>
                <td>JABATAN PEGAWAI</td>
                <td>{$data_biodata.NM_JABATAN_PEGAWAI}</td>
            </tr>
            <tr>
                <td>JABATAN FUNGSIONAL</td>
                <td>{$data_biodata.NM_JABATAN_FUNGSIONAL}</td>
            </tr>
        </table>
    {else if isset($data_cari)}
        <table style="width: 98%">
            <tr>
                <th>NO</th>
                <th>NAMA</th>
                <th>NIP</th>
                <th>PROGRAM STUDI</th>
                <th>TELP, MOBILE</th>
                <th>DETAIL</th>
            </tr>
            {foreach $data_cari as $d}
                <tr>
                    <td>{$d@index+1}</td>
                    <td>{$d.GELAR_DEPAN} {$d.NM_PENGGUNA} {$d.GELAR_BELAKANG}</td>
                    <td>{$d.NIP_DOSEN}</td>
                    <td>{$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI}</td>
                    <td>{$d.TELP} {$d.MOBILE_DOSEN} </td>
                    <td>
                        <a class="button" href="cari-dosen.php?mode=detail&dos={$d.ID_DOSEN}">DETAIL</a>
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="7" class="data-kosong">Data Tidak Ditemukan</td>
                </tr>
            {/foreach}
        </table>
    {/if}
{/if}
