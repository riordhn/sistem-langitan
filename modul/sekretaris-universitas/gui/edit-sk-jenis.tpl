<div class="center_title_bar">Edit - Master Jenis Surat Keputusan</div>
<form action="sk-jenis.php" method="post" id="edit_group">
    <table class="ui-widget-content" style="width: 70%">
        <tr>
            <th colspan="2" class="center">Edit  Jenis Surat Keputusan</th>
        </tr>
        <tr>
            <td>Nama</td>
            <td><input class="required" type="text" name="nama" value="{$data_jenis.NM_SK_JENIS}"/></td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td>
                <textarea name="keterangan">{$data_jenis.KETERANGAN_SK_JENIS}</textarea>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" value="{$data_jenis.ID_SK_JENIS}"/>
                <a class="disable-ajax button" onclick="history.back(-1)">Kembali</a>
                <input type="submit" class="button" onclick="" value="Update"/>
            </td>
        </tr>
    </table>
</form>