<div class="center_title_bar">Master Surat Keputusan</div>
<table class="ui-widget-content" style="width: 90%">
    <tr>
        <th class="center" colspan="5">Surat Keputusan</th>
    </tr>
    <tr>
        <th>NO</th>
        <th>JENIS</th>
        <th>NAMA</th>
        <th>KETERANGAN</th>
        <th>OPERASI</th>
    </tr>
    {foreach $data_sk as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data.NM_SK_JENIS}</td>
            <td>{$data.NM_SK_MASTER}</td>
            <td>{$data.KETERANGAN_SK_MASTER}</td>
            <td class="center">
                <a class="button" href="sk.php?mode=edit&id={$data.ID_SK_MASTER}">Edit</a>
                <a class="button" href="sk.php?mode=delete&id={$data.ID_SK_MASTER}">Delete</a>
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="5" class="data-kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="5" class="center">
            <a class="button" href="sk.php?mode=add">Tambah</a>
        </td>
    </tr>
</table>

