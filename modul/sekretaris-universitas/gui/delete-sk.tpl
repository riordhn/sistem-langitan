<div class="center_title_bar">Delete - Master Surat Keputusan</div>
<form action="sk.php" method="post" id="edit_group">
    <table class="ui-widget-content" style="width: 70%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Delete Surat Keputusan</th>
        </tr>
        <tr>
            <td>Nama</td>
            <td>{$data_sk.NM_SK_MASTER}</td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <input type="hidden" name="mode" value="delete"/>
                <input type="hidden" name="id" value="{$data_sk.ID_SK_MASTER}"/>
                Apakah anda yakin menghapus item ini?<br/>
                <a class="disable-ajax button" onclick="history.back(-1)">Kembali</a>
                <input type="submit" class="button" onclick="" value="Hapus"/>
            </td>
        </tr>
    </table>
</form>