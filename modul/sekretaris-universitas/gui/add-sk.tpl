<div class="center_title_bar">Tambah - Master Surat Keputusan</div>
<form action="sk.php" method="post" id="add">
    <table class="ui-widget-content" style="width: 70%">
        <tr>
            <th colspan="2" class="center">Tambah Surat Keputusan</th>
        </tr>
        <tr>
            <td>Jenis Surat Keputusan</td>
            <td>
                <select name="jenis">
                    {foreach $data_jenis as $dj}
                        <option value="{$dj.ID_SK_JENIS}">{$dj.NM_SK_JENIS}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama</td>
            <td><input class="required" id="nama" type="text" name="nama"/></td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td>
                <textarea name="keterangan"></textarea>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <input type="hidden" name="mode" value="add"/>
                <a class="disable-ajax button" onclick="history.back(-1)">Kembali</a>
                <input type="submit" class="button" value="Tambah"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#add').validate();  
    </script>
{/literal}