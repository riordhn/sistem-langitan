<div class="center_title_bar">Master Jenis Surat Keputusan</div>
<table class="ui-widget-content" style="width: 90%">
    <tr>
        <th class="center" colspan="4">Jenis Surat Kerjasama</th>
    </tr>
    <tr>
        <th>NO</th>
        <th>NAMA</th>
        <th>KETERANGAN</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_jenis as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data.NM_SK_JENIS}</td>
            <td>{$data.KETERANGAN_SK_JENIS}</td>
            <td class="center">
                <a class="button" href="sk-jenis.php?mode=edit&id={$data.ID_SK_JENIS}">Edit</a>
                <a class="button" href="sk-jenis.php?mode=delete&id={$data.ID_SK_JENIS}">Delete</a>
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="4" class="data-kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="4" class="center">
            <a class="button" href="sk-jenis.php?mode=add">Tambah</a>
        </td>
    </tr>
</table>

