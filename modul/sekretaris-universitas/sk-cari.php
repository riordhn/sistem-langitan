<?php

include 'config.php';
include 'class/sk.class.php';

$sk = new sk($db);

if (isset($_GET)) {
    if (get('cari') != '') {
        $values = get('cari');
        $hasil = $db->QueryToArray("
            SELECT SD.FILE_SK_DATA,SD.NO_SK_DATA,SD.KETERANGAN_SK_DATE,SM.NM_SK_MASTER,SJ.NM_SK_JENIS,TO_CHAR(SD.TGL_SK_DATA,'DD-MM-YYYY , HH24:MI:SS') WAKTU
            FROM SK_DATA SD
            JOIN SK_MASTER SM ON SM.ID_SK_MASTER=SD.ID_SK_MASTER
            JOIN SK_JENIS SJ ON SJ.ID_SK_JENIS=SM.ID_SK_JENIS
            LEFT JOIN SK_DATA_DETAIL SKD ON SD.ID_SK_DATA=SKD.ID_SK_DATA
            LEFT JOIN MAHASISWA M ON M.ID_MHS=SKD.ID_MHS
            LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            WHERE UPPER(SM.NM_SK_MASTER) LIKE UPPER('%{$values}%')
            OR UPPER(SD.NO_SK_DATA) LIKE UPPER('%{$values}%') 
            OR UPPER(SD.FILE_SK_DATA) LIKE UPPER('%{$values}%')
            OR UPPER(P.NM_PENGGUNA) LIKE UPPER('%{$values}%')
            GROUP BY SD.FILE_SK_DATA,SD.NO_SK_DATA,SD.KETERANGAN_SK_DATE,SM.NM_SK_MASTER,SJ.NM_SK_JENIS,SD.TGL_SK_DATA
            ORDER BY SD.TGL_SK_DATA DESC
            ");
        $smarty->assign('data_hasil', $hasil);
    }
}

$smarty->display("sk-cari.tpl");
?>
