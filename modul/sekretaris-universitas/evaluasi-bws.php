<?php
error_reporting (E_ALL & ~E_NOTICE);
include 'config.php';
$id_pengguna= $user->ID_PENGGUNA; 

$fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY ID_FAKULTAS ASC");
$smarty->assign('fakultas', $fakultas);
$semester = $db->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER = 'Genap' AND ID_PERGURUAN_TINGGI = '{$id_pt_user}'
								ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
$smarty->assign('semester', $semester);
$jenjang = $db->QueryToArray("SELECT * FROM JENJANG WHERE ID_JENJANG = 1 OR ID_JENJANG = 5 OR ID_JENJANG = 2 OR ID_JENJANG = 3 
								ORDER BY NM_JENJANG ASC");
$smarty->assign('jenjang', $jenjang);
$status = $db->QueryToArray("SELECT * FROM STATUS_EVALUASI
							WHERE JENIS_STATUS_EVALUASI = 1");
$smarty->assign('status', $status);
$status_penetapan = $db->QueryToArray("SELECT * FROM STATUS_EVALUASI
										WHERE JENIS_STATUS_EVALUASI = 2");
$smarty->assign('status_penetapan', $status_penetapan);


if($_POST['mode'] == 'insert'){
	
	$no = $_POST['no'];
	$tgl = date('d-m-Y');
	for($i=2; $i<=$no; $i++){
		$id_mhs = $_POST['id_mhs'.$i];
		$rekomendasi = $_POST['rekomendasi'.$i];
		$penetapan = $_POST['penetapan'.$i];
		$tgl_rekomendasi = $_POST['tgl_rekomendasi'.$i];
		$tgl_penetapan = $_POST['tgl_penetapan'.$i];
		$keterangan = $_POST['keterangan'.$i];
		$sks = $_POST['sks'.$i];
		$ipk = $_POST['ipk'.$i];
		$piutang = $_POST['piutang'.$i];
		$status_terkini = $_POST['status_terkini'.$i];
		
		if($tgl_rekomendasi != '' and $rekomendasi != ''){
				
				$db->Query("SELECT REKOMENDASI_STATUS FROM EVALUASI_STUDI 
							WHERE IS_AKTIF = 1 AND ID_MHS = '$id_mhs' 
							AND ID_SEMESTER = '$_POST[semester]' AND JENIS_EVALUASI = 1");
				$cek = $db->FetchAssoc();
				
				$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '$_POST[semester]'");
				$semester_aktif = $db->FetchAssoc();
				
				if($cek['REKOMENDASI_STATUS'] == '$rekomendasi'){
				
				}else{
				
				$db->Query("UPDATE EVALUASI_STUDI SET IS_AKTIF = 0 WHERE ID_MHS = '$id_mhs' AND ID_SEMESTER = '$_POST[semester]' AND JENIS_EVALUASI = 1");
				
				$db->Query("INSERT INTO EVALUASI_STUDI (ID_MHS, ID_SEMESTER, REKOMENDASI_STATUS, TGL_REKOMENDASI_STATUS, 
							  KETERANGAN, JENIS_EVALUASI, SKS_DIPEROLEH, IPK_EVALUASI, STATUS_TERKINI, ID_PENGGUNA, WAKTU_UBAH, IS_AKTIF, PIUTANG) 
							  VALUES 
							 ('$id_mhs', '$_POST[semester]', '$rekomendasi', to_date('$tgl_rekomendasi', 'DD-MM-YYYY'), 
							  '$keterangan', 1, '$sks', '$ipk', '$status_terkini', '$id_pengguna', to_date('$tgl', 'DD-MM-YYYY'), 1, '$piutang')");
				
				$keterangan = "Evaluasi Batas Waktu Studi " . $semester_aktif['TAHUN_AJARAN'] . " " . $semester_aktif['NM_SEMESTER'];
				
				$db->Query("SELECT COUNT(*) AS CEK FROM CEKAL_PEMBAYARAN 
							WHERE STATUS_CEKAL = 1 AND ID_SEMESTER = '$_POST[semester]' AND ID_MHS = '$id_mhs' AND JENIS_CEKAL = 1");
				$cek = $db->FetchAssoc();
				
				if($cek['CEK'] < 1){
				$db->Query("INSERT INTO CEKAL_PEMBAYARAN (ID_MHS, ID_PENGGUNA, ID_SEMESTER, STATUS_CEKAL, TGL_UBAH, KETERANGAN, STATUS_CEKAL_FAKULTAS, JENIS_CEKAL)
					VALUES ('$id_mhs','$id_pengguna','$_POST[semester]','1',to_date('$tgl', 'DD-MM-YYYY'), '$keterangan', 1, 1)");
        		
				$db->Query("UPDATE MAHASISWA SET STATUS_CEKAL = 1 WHERE ID_MHS = '$id_mhs'");
				}
				
				}
			
		}

	}

}


if(isset($_REQUEST['jenjang']) and isset($_REQUEST['semester'])){

$where  = "";

		if($_REQUEST['fakultas'] <> ''){
			$where  = " AND FAKULTAS.ID_FAKULTAS = '$_REQUEST[fakultas]'";
			$where2  = " AND h.ID_FAKULTAS = '$_REQUEST[fakultas]'";
		}else{
			$where  = "";
			$where2  = "";
		}
		
		
$get_jenjang = $_REQUEST['jenjang'];
$thn = date('Y');
		
		if($get_jenjang == 1){
			$bws  = 7;
			$where .= " AND JALUR_MAHASISWA.ID_JALUR <> 4 AND JALUR_MAHASISWA.ID_JALUR <> 22 AND JALUR_MAHASISWA.ID_JALUR_AKTIF = 1";
		}elseif($get_jenjang == 5){
			$bws = 5;
		}elseif($get_jenjang == 2){
			$bws = 3;
		}elseif($get_jenjang == 3){
			$bws = 5;
		}elseif($get_jenjang == 0 and $get_jenjang <> ''){
			$bws = 5;
			$get_jenjang= 1;
			$where .= " AND (JALUR_MAHASISWA.ID_JALUR = 4 OR JALUR_MAHASISWA.ID_JALUR = 22) AND JALUR_MAHASISWA.ID_JALUR_AKTIF = 1";
		}else{
			$bws = 0;
		}


if($_GET['jenjang'] == 'all' or $_POST['jenjang'] == 'all'){

$sql2 = " AND (
			-- S1 BWS 7 tahun
			(
			PROGRAM_STUDI.ID_JENJANG = '1' AND (THN_ANGKATAN_MHS + CUTI) <= (TO_CHAR(SYSDATE, 'YYYY') - 7)
			AND JALUR_MAHASISWA.ID_JALUR <> 4 AND JALUR_MAHASISWA.ID_JALUR <> 22 AND JALUR_MAHASISWA.ID_JALUR_AKTIF = 1
			)
			OR 
			-- D3 BWS 5 TAHUN
			(
			PROGRAM_STUDI.ID_JENJANG = '5' AND (THN_ANGKATAN_MHS + CUTI) <= (TO_CHAR(SYSDATE, 'YYYY') - 5)
			)
			OR 
			-- S2 BWS 3 TAHUN
			(
			PROGRAM_STUDI.ID_JENJANG = '2' AND ((THN_ANGKATAN_MHS + CUTI) < (TO_CHAR(SYSDATE, 'YYYY') - 3)
			OR (THN_ANGKATAN_MHS + CUTI) = (TO_CHAR(SYSDATE, 'YYYY') - 3) AND NM_SEMESTER = 'Ganjil')
			)
			OR 
			-- S3 BWS 5 TAHUN
			(
			PROGRAM_STUDI.ID_JENJANG = '3' AND ((THN_ANGKATAN_MHS + CUTI) <= (TO_CHAR(SYSDATE, 'YYYY') - 5)
			OR (THN_ANGKATAN_MHS + CUTI) = (TO_CHAR(SYSDATE, 'YYYY') - 5) AND NM_SEMESTER = 'Ganjil')
			)
			OR 
			-- S1 ALIH JENIS BWS 5 TAHUN
			(
			PROGRAM_STUDI.ID_JENJANG = '1' AND (THN_ANGKATAN_MHS + CUTI) <= (TO_CHAR(SYSDATE, 'YYYY') - 5)
			AND (JALUR_MAHASISWA.ID_JALUR = 4 OR JALUR_MAHASISWA.ID_JALUR = 22) AND JALUR_MAHASISWA.ID_JALUR_AKTIF = 1
			)
			) ";

}else{
	
$sql2 = " AND ((THN_ANGKATAN_MHS + CUTI) < (TO_CHAR(SYSDATE, 'YYYY') - $bws) 
		  OR (THN_ANGKATAN_MHS + CUTI) = (TO_CHAR(SYSDATE, 'YYYY') - $bws) AND NM_SEMESTER = 'Ganjil')
		  AND PROGRAM_STUDI.ID_JENJANG = '$get_jenjang' $where ";
	
}


$evaluasi = $db->QueryToArray("SELECT MAHASISWA.ID_MHS, MAHASISWA.NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, 
			UPPER(NM_FAKULTAS) AS NM_FAKULTAS, NM_JENJANG, (TO_CHAR(SYSDATE, 'YYYY') - FLOOR((((7 * 2) - CUTI) / 2 ))) AS TES,
			(CASE WHEN IPK IS NULL THEN 0 ELSE IPK END) AS IPK, (CASE WHEN SKS IS NULL THEN 0 ELSE SKS END) AS SKS,
			NM_STATUS_PENGGUNA, JML_NILAI_D, JML_SKS_D, ROUND(JML_SKS_D / SKS * 100, 2) AS PERSEN_NILAI_D, 
			(CASE WHEN (IPK IS NULL OR IPK < 2) THEN ((IPK - 1) * SKS) ELSE SKS END) AS SKS_IPK_2,
			(CASE WHEN Z.PIUTANG IS NULL THEN 0 ELSE Z.PIUTANG END) AS PIUTANG, 
			CUTI, STATUS_PENGGUNA.ID_STATUS_PENGGUNA, EVALUASI_STUDI.KETERANGAN, REKOMENDASI_STATUS, PENETAPAN_STATUS,
			to_char(TGL_REKOMENDASI_STATUS, 'dd-mm-yyyy') as TGL_REKOMENDASI_STATUS, 
			to_char(TGL_PENETAPAN_STATUS, 'dd-mm-yyyy') as TGL_PENETAPAN_STATUS,
			NM_SEMESTER, JALUR_MAHASISWA.ID_JALUR, JENJANG.ID_JENJANG
			FROM MAHASISWA
			LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
			LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
			LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
			LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
			LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
			LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = MAHASISWA.ID_SEMESTER_MASUK
			LEFT JOIN JALUR_MAHASISWA ON JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS
			LEFT JOIN (select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
				from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
				c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
				row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
				count(*) over(partition by c.nm_mata_kuliah) terulang
					from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
					where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
					and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
					and h.id_program_studi = g.id_program_studi $where2
				) x
				where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
				group by id_mhs) C ON C.ID_MHS = MAHASISWA.ID_MHS
			LEFT JOIN (select id_mhs, sum(kredit_semester) as jml_sks_d, count(*) as jml_nilai_d from (
				select f.NIM_MHS,a.id_mhs,e.tahun_ajaran,e.nm_semester,c.kd_mata_kuliah,c.nm_mata_kuliah,
				d.kredit_semester,a.nilai_huruf, a.flagnilai,
				 row_number() over(partition by a.id_mhs,
				 c.nm_mata_kuliah order by a.nilai_huruf) rangking,count(*) over(partition by c.nm_mata_kuliah) terulang
					from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, mahasiswa f, program_studi h
					where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah  and a.status_hapus=0
				and a.id_semester=e.id_semester and a.STATUS_APV_PENGAMBILAN_MK='1' and f.id_mhs = a.id_mhs 
				and h.id_program_studi = f.id_program_studi $where2
					) apem
				where APEM.NILAI_HURUF = 'D' AND APEM.RANGKING = 1 and flagnilai = 1
				GROUP BY id_mhs
			) D ON D.ID_MHS = MAHASISWA.ID_MHS
			LEFT JOIN (SELECT MAHASISWA.ID_MHS, (CASE WHEN CUTI IS NULL THEN 0 ELSE CUTI END) AS CUTI FROM (
						SELECT MAHASISWA.ID_MHS, COUNT(*) AS CUTI FROM MAHASISWA
												JOIN ADMISI ON ADMISI.ID_MHS = MAHASISWA.ID_MHS AND STATUS_AKD_MHS = 2
												WHERE STATUS_APV = 1
												GROUP BY MAHASISWA.ID_MHS
						) A
						RIGHT JOIN MAHASISWA ON MAHASISWA.ID_MHS = A.ID_MHS
						JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
						JOIN FAKULTAS h ON h.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
						WHERE(STATUS_AKADEMIK_MHS <> 4 AND STATUS_AKADEMIK_MHS <> 5 AND STATUS_AKADEMIK_MHS <> 6 
						AND STATUS_AKADEMIK_MHS <> 7 AND STATUS_AKADEMIK_MHS <> 20) $where2
			) E ON E.ID_MHS = MAHASISWA.ID_MHS
			LEFT JOIN EVALUASI_STUDI ON EVALUASI_STUDI.ID_MHS = MAHASISWA.ID_MHS AND EVALUASI_STUDI.ID_SEMESTER = '$_REQUEST[semester]' AND IS_AKTIF = 1 AND JENIS_EVALUASI = 1
			LEFT JOIN (SELECT ID_MHS, COUNT(*) AS PIUTANG FROM 
						(
							SELECT MAHASISWA.ID_MHS, SEMESTER.ID_SEMESTER
							FROM PEMBAYARAN 
							JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
							JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
							JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PEMBAYARAN.ID_SEMESTER
							WHERE (ID_STATUS_PEMBAYARAN = 2 OR ID_STATUS_PEMBAYARAN IS NULL) AND STATUS_PENGGUNA.STATUS_AKTIF_BAYAR = 1
							AND STATUS_AKTIF_SEMESTER = 'False'
							GROUP BY MAHASISWA.ID_MHS, SEMESTER.ID_SEMESTER
						) GROUP BY ID_MHS HAVING COUNT(*) >= 1) Z ON Z.ID_MHS = MAHASISWA.ID_MHS
			WHERE STATUS_AKTIF_BAYAR = 1
			$sql2
			ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NIM_BANK ASC, NM_PROGRAM_STUDI ASC");
	
	$smarty->assign('evaluasi', $evaluasi);
	
	
	
}

if(isset($_REQUEST['semester'])){
	$smarty->assign('id_semester', $_REQUEST['semester']);
}else{
	$db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True' AND ID_PERGURUAN_TINGGI = '{$id_pt_user}'");
	$semester_aktif = $db->FetchAssoc();
	$smarty->assign('semester_aktif', $semester_aktif['ID_SEMESTER']);
}

$smarty->assign('id_fakultas', $_REQUEST['fakultas']);
$smarty->assign('id_jenjang', $_REQUEST['jenjang']);
$smarty->display("evaluasi/bws/evaluasi-bws.tpl");
?>