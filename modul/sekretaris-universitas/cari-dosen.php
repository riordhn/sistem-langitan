<?php

include 'config.php';

if (isset($_GET)) {
    if (get('mode') == 'cari') {
        $cari = get('cari');
        $query = "
            SELECT P.GELAR_DEPAN,P.GELAR_BELAKANG,P.NM_PENGGUNA,D.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            FROM DOSEN D
            JOIN PENGGUNA P ON D.ID_PENGGUNA=P.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            WHERE P.ID_PERGURUAN_TINGGI = '{$id_pt_user}' AND (UPPER(P.NM_PENGGUNA) LIKE UPPER('%{$cari}%') OR D.NIP_DOSEN LIKE '%{$cari}%' OR P.USERNAME LIKE '%{$cari}%')
            ";
        $data_cari = $db->QueryToArray($query);
        $smarty->assign('data_cari', $data_cari);
    } else if (get('mode') == 'detail') {
        $id = get('dos');
        $query = "
           SELECT P.USERNAME,P.BLOG_PENGGUNA,P.EMAIL_PENGGUNA,P.NM_PENGGUNA,P.GELAR_DEPAN,P.GELAR_BELAKANG,P.KELAMIN_PENGGUNA,D.*,P.TGL_LAHIR_PENGGUNA
                ,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JP.NM_JABATAN_PEGAWAI,JF.NM_JABATAN_FUNGSIONAL,G.NM_GOLONGAN,NVL(K.NM_KOTA,P.TEMPAT_LAHIR) KOTA_LAHIR
            FROM DOSEN D
            JOIN PENGGUNA P ON D.ID_PENGGUNA=P.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            LEFT JOIN KOTA K ON K.ID_KOTA=P.ID_KOTA_LAHIR
            LEFT JOIN JABATAN_FUNGSIONAL JF ON JF.ID_JABATAN_FUNGSIONAL=D.ID_JABATAN_FUNGSIONAL
            LEFT JOIN GOLONGAN G ON G.ID_GOLONGAN=D.ID_GOLONGAN
            LEFT JOIN JABATAN_PEGAWAI JP ON JP.ID_JABATAN_PEGAWAI=D.ID_JABATAN_PEGAWAI
            WHERE D.ID_DOSEN='{$id}'
           ";
        $db->Query($query);
        $data_biodata = $db->FetchAssoc();
        if (file_exists("../../foto_pegawai/{$nama_singkat}/{$data_biodata['USERNAME']}.JPG"))
            $smarty->assign('foto', $base_url."foto_pegawai/{$nama_singkat}/{$data_biodata['USERNAME']}.JPG");
        else if (file_exists("../../foto_pegawai/{$nama_singkat}/{$data_biodata['USERNAME']}.jpg"))
            $smarty->assign('foto', "../../foto_pegawai/{$nama_singkat}/{$data_biodata['USERNAME']}.jpg");
        else
            $smarty->assign('foto', $base_url."img/dosen/photo.png");
        $smarty->assign('data_biodata', $data_biodata);
    }
}

$smarty->display('cari-dosen.tpl');
?>
