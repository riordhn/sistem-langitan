<?php
error_reporting (E_ALL & ~E_NOTICE);
include 'config.php';
$id_pengguna= $user->ID_PENGGUNA; 
include "../pendidikan/class/aucc.class.php";
$aucc = new aucc($db);

$smarty->assign('get_fakultas', $aucc->get_fakultas($id_pt_user));

$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '$_POST[semester]'");
	$semester_aktif = $db->FetchAssoc();
	$smarty->assign('semester_aktif', $semester_aktif['ID_SEMESTER']);

$semester = $db->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER = 'Genap' AND ID_PERGURUAN_TINGGI = '{$id_pt_user}'
								ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
$smarty->assign('semester', $semester);

$fak = $_POST['fakultas'];


if($_POST['mode'] == 'insert'){
	$no = $_POST['no'];
	$tgl = date('d-m-Y');
	for($i=2; $i<=$no; $i++){
		$id_mhs = $_POST['id_mhs'.$i];
		$rekomendasi = $_POST['rekomendasi'.$i];
		//$tgl_rekomendasi = $_POST['tgl_rekomendasi'.$i];
		$keterangan = $_POST['keterangan'.$i];
		$sks = $_POST['sks'.$i];
		$ipk = $_POST['ipk'.$i];
		$status_terkini = $_POST['status_terkini'.$i];
		
		if($rekomendasi != ''){

			
		$db->Query("UPDATE EVALUASI_STUDI SET IS_AKTIF = 0 WHERE ID_MHS = '$id_mhs' AND ID_SEMESTER = '$semester_aktif[ID_SEMESTER]' AND JENIS_EVALUASI = 3");
		
		$db->Query("INSERT INTO EVALUASI_STUDI (ID_MHS, ID_SEMESTER, REKOMENDASI_STATUS, TGL_REKOMENDASI_STATUS, 
					  KETERANGAN, JENIS_EVALUASI, SKS_DIPEROLEH, IPK_EVALUASI, STATUS_TERKINI, ID_PENGGUNA, WAKTU_UBAH, IS_AKTIF)
					  VALUES 
					 ('$id_mhs', '$semester_aktif[ID_SEMESTER]', '$rekomendasi', to_date('$tgl', 'DD-MM-YYYY'), 
					  '$keterangan', 3, '$sks', '$ipk', '$status_terkini', '$id_pengguna',
					  to_date('$tgl', 'DD-MM-YYYY'), 1)");
		}

	}

}


if(isset($_POST['fakultas'])){


	
		if($_POST['fakultas'] <> ''){
			$where  = " AND FAKULTAS.ID_FAKULTAS = '$_POST[fakultas]'";
			$where2  = " AND h.ID_FAKULTAS = '$_POST[fakultas]'";
		}else{
			$where  = "";
			$where2  = "";
		}
		
$mhs = $db->QueryToArray("SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, NM_STATUS_PENGGUNA, NM_JENJANG
				, STATUS_CEKAL, FAKULTAS.ID_FAKULTAS, A.PIUTANG,
				(CASE WHEN B.SUDAH_KRS IS NULL THEN 0 ELSE B.SUDAH_KRS END) AS SUDAH_KRS, STATUS_PENGGUNA.ID_STATUS_PENGGUNA,
				EVALUASI_STUDI.KETERANGAN, REKOMENDASI_STATUS, PENETAPAN_STATUS,
				to_char(TGL_REKOMENDASI_STATUS, 'dd-mm-yyyy') as TGL_REKOMENDASI_STATUS, 
				to_char(TGL_PENETAPAN_STATUS, 'dd-mm-yyyy') as TGL_PENETAPAN_STATUS
				FROM MAHASISWA
				LEFT JOIN PENGGUNA ON MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA
				LEFT JOIN PROGRAM_STUDI ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				JOIN 
				(
						SELECT ID_MHS, COUNT(*) AS PIUTANG FROM 
						(
							SELECT MAHASISWA.ID_MHS, SEMESTER.ID_SEMESTER
							FROM PEMBAYARAN 
							JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
							JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
							JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PEMBAYARAN.ID_SEMESTER
							WHERE (ID_STATUS_PEMBAYARAN = 2 OR ID_STATUS_PEMBAYARAN IS NULL) AND STATUS_PENGGUNA.STATUS_AKTIF_BAYAR = 1
							AND STATUS_AKTIF_PEMBAYARAN = 'False'
							GROUP BY MAHASISWA.ID_MHS, SEMESTER.ID_SEMESTER
						) GROUP BY ID_MHS HAVING COUNT(*) > 1
				) A ON A.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN (
					SELECT COUNT(DISTINCT PENGAMBILAN_MK.ID_MHS) AS SUDAH_KRS, PENGAMBILAN_MK.ID_MHS 
					FROM PENGAMBILAN_MK
					LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PENGAMBILAN_MK.ID_SEMESTER
					LEFT JOIN MAHASISWA ON PENGAMBILAN_MK.ID_MHS = MAHASISWA.ID_MHS
					LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
					JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
					WHERE SEMESTER.STATUS_AKTIF_SEMESTER = 'True' $where
					GROUP BY PENGAMBILAN_MK.ID_MHS) B ON B.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN EVALUASI_STUDI ON EVALUASI_STUDI.ID_MHS = MAHASISWA.ID_MHS AND EVALUASI_STUDI.ID_SEMESTER = '$semester_aktif[ID_SEMESTER]' AND IS_AKTIF = 1 AND JENIS_EVALUASI = 3
				WHERE STATUS_AKTIF_BAYAR = 1 $where
				ORDER BY NM_JENJANG, NM_PROGRAM_STUDI, NIM_MHS");
				
$smarty->assign('mhs', $mhs);
	
}

$smarty->assign('id_semester', $semester_aktif['ID_SEMESTER']);
$smarty->assign('id_fakultas', $fak);
$smarty->display("evaluasi/administrasi/evaluasi-administrasi.tpl");
?>