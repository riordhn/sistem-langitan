<?php

include 'config.php';
include '../dosen/proses/perwalian.class.php';
$perwalian = new perwalian($db, $user->ID_PENGGUNA, $login->id_dosen);
if (isset($_GET)) {
    if (get('mode') == 'cari') {
        $cari = get('cari');
        $query = "
            SELECT M.*,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS,SP.NM_STATUS_PENGGUNA
            FROM PENGGUNA P
            JOIN MAHASISWA M ON M.ID_PENGGUNA=P.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA=M.STATUS_AKADEMIK_MHS
            WHERE P.ID_PERGURUAN_TINGGI = '{$id_pt_user}' AND (UPPER(P.NM_PENGGUNA) LIKE UPPER('%{$cari}%') OR M.NIM_MHS LIKE '%{$cari}%')
            ORDER BY PS.ID_FAKULTAS,PS.NM_PROGRAM_STUDI,M.NIM_MHS
            ";
        $data_cari = $db->QueryToArray($query);
        $smarty->assign('data_cari', $data_cari);
    } else if (get('mode') == 'detail') {
        $id = get('mhs');
        $data_biodata = $perwalian->get_biodata_mahasiswa($id);
        if (file_exists("../../foto_mhs/{$nama_singkat}/{$data_biodata['NIM_MHS']}.JPG"))
            $smarty->assign('foto', $base_url."foto_mhs/{$nama_singkat}/{$data_biodata['NIM_MHS']}.JPG");
        else if (file_exists("../../foto_mhs/{$nama_singkat}/{$data_biodata['NIM_MHS']}.jpg"))
            $smarty->assign('foto', "../../foto_mhs/{$nama_singkat}/{$data_biodata['NIM_MHS']}.jpg");
        else
            $smarty->assign('foto', $base_url."img/dosen/photo.png");
        $smarty->assign('data_biodata', $data_biodata);
        $smarty->assign('data_admisi', $perwalian->load_admisi_mhs($id));
        $smarty->assign('data_mhs_status', $perwalian->load_mhs_status($id));
        $smarty->assign('data_pembayaran', $perwalian->load_history_bayar_mhs($id));
        $smarty->assign('data_history_nilai', $perwalian->load_transkrip($id));
    }
}

$smarty->display('cari-mahasiswa.tpl');
?>
