<?php

class sk {

    public $db;
    public $role;

    function __construct($db) {
        $this->db = $db;
    }

    // Jenis SK

    function load_jenis_sk() {
        return $this->db->QueryToArray("SELECT * FROM SK_JENIS ORDER BY NM_SK_JENIS");
    }

    function get_jenis_sk($id) {
        $this->db->Query("SELECT * FROM SK_JENIS WHERE ID_SK_JENIS='{$id}'");
        return $this->db->FetchAssoc();
    }

    function add_jenis_sk($nama, $keterangan) {
        $this->db->Query("INSERT INTO SK_JENIS (NM_SK_JENIS,KETERANGAN_SK_JENIS) VALUES ('{$nama}','{$keterangan}')");
    }

    function update_jenis_sk($id, $nama, $keterangan) {
        $this->db->Query("UPDATE SK_JENIS SET NM_SK_JENIS='{$nama}',KETERANGAN_SK_JENIS='{$keterangan}' WHERE ID_SK_JENIS='{$id}'");
    }

    function delete_jenis_sk($id) {
        $this->db->Query("DELETE FROM SK_JENIS WHERE ID_SK_JENIS='{$id}'");
    }

    // Master SK

    function load_sk() {
        return $this->db->QueryToArray("
            SELECT SM.*,SJ.NM_SK_JENIS
            FROM SK_MASTER SM
            JOIN SK_JENIS SJ ON SJ.ID_SK_JENIS=SM.ID_SK_JENIS
            ORDER BY SM.NM_SK_MASTER
            ");
    }

    function get_sk($id) {
        $this->db->Query("SELECT * FROM SK_MASTER WHERE ID_SK_MASTER='{$id}'");
        return $this->db->FetchAssoc();
    }

    function add_sk($jenis, $nama, $keterangan) {
        $this->db->Query("INSERT INTO SK_MASTER (ID_SK_JENIS,NM_SK_MASTER,KETERANGAN_SK_MASTER) VALUES ('{$jenis}','{$nama}','{$keterangan}')");
    }

    function update_sk($id, $jenis, $nama, $keterangan) {
        $this->db->Query("
            UPDATE SK_MASTER 
                SET 
                    ID_SK_JENIS='{$jenis}',
                    NM_SK_MASTER='{$nama}',
                    KETERANGAN_SK_MASTER='{$keterangan}' 
            WHERE ID_SK_MASTER='{$id}'
            ");
    }

    function delete_sk($id) {
        $this->db->Query("DELETE FROM SK_MASTER WHERE ID_SK_MASTER='{$id}'");
    }

    // Data Input
    function UploadFile($path, $files, $file_name) {
        $error_message = '';
        $upload = false;
        // Format Nama File         
        $allowed_exts = array("pdf", "jpg", "png", "gif", "jpeg");
        $allowed_type = array('application/pdf', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/png');
        $extension = end(explode(".", $files["file"]["name"]));
        $type = $files["file"]["type"];
        $name = $files["file"]["name"];
        $tmp_name = $files["file"]["tmp_name"];
        $error = $files["file"]["error"];
        if (in_array($extension, $allowed_exts) && in_array($type, $allowed_type)) {
            if ($error > 0) {
                $error_message .= "Error file : " . $error;
            } else {
                if (file_exists("$path/$file_name")) {
                    $error_message .= $file_name . " Sudah Ada. ";
                } else {
                    move_uploaded_file($tmp_name, "$path/$file_name");
                    $upload = true;
                }
            }
        } else {
            $error_message .= "Format File {$name} tidak sesuai";
        }
        if (!$upload) {
            echo $error_message != '' ? alert_error($error_message) : "";
        }
        return $upload;
    }

    function load_input_data() {
        return $this->db->QueryToArray("
            SELECT SD.*,SM.NM_SK_MASTER,SJ.NM_SK_JENIS,TO_CHAR(SD.TGL_SK_DATA,'DD-MM-YYYY , HH24:MI:SS') WAKTU
            FROM SK_DATA SD
            JOIN SK_MASTER SM ON SM.ID_SK_MASTER=SD.ID_SK_MASTER
            JOIN SK_JENIS SJ ON SJ.ID_SK_JENIS=SM.ID_SK_JENIS
            ORDER BY SD.TGL_SK_DATA DESC
            ");
    }

    function load_detail_data($id) {
        
        return $this->db->QueryToArray("
            SELECT P.NM_PENGGUNA,M.NIM_MHS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            FROM SK_DATA_DETAIL SKD
            JOIN MAHASISWA M ON M.ID_MHS=SKD.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            WHERE SKD.ID_SK_DATA='{$id}'
            ");
    }

    function get_input_data($id) {
        $this->db->Query("
            SELECT SD.*,SM.NM_SK_MASTER,SJ.NM_SK_JENIS
            FROM SK_DATA SD
            JOIN SK_MASTER SM ON SM.ID_SK_MASTER=SD.ID_SK_MASTER
            JOIN SK_JENIS SJ ON SJ.ID_SK_JENIS=SM.ID_SK_JENIS
            WHERE SD.ID_SK_DATA='{$id}'");
        return $this->db->FetchAssoc();
    }

    function add_input_data($master, $no_sk, $keterangan, $files) {
        $file_name = str_replace(" ", "_", strtolower($files["file"]["name"]));
        $upload = $this->UploadFile("./file-sk", $files, $file_name);
        if ($upload) {
            $this->db->Query("
                INSERT INTO SK_DATA 
                    (ID_SK_MASTER,NO_SK_DATA,FILE_SK_DATA,KETERANGAN_SK_DATE)
                VALUES
                    ('{$master}','{$no_sk}','{$file_name}','{$keterangan}')
                ");
            echo alert_success("Data Berhasil DImasukkan");
        } else {
            echo alert_error("Gagal Upload");
        }
    }

    function update_input_data($id, $master, $no_sk, $keterangan, $files) {
        if ($files["file"]["name"] != '') {
            $file_name = str_replace(" ", "_", strtolower($files["file"]["name"]));
            $data = $this->get_input_data($id);
            unlink("./file-sk/{$data['FILE_SK_DATA']}");
            $upload = $this->UploadFile("./file-sk", $files, $file_name);
            if ($upload) {
                $this->db->Query("
                UPDATE SK_DATA
                    SET
                        ID_SK_MASTER='{$master}',
                        NO_SK_DATA='{$no_sk}',
                        FILE_SK_DATA='{$file_name}',
                        KETERANGAN_SK_DATE='{$keterangan}'
                WHERE ID_SK_DATA='{$id}'
                ");
                echo alert_success("Data Berhasil DImasukkan");
            } else {
                echo alert_error("Gagal Upload");
            }
        } else {
            $this->db->Query("
                UPDATE SK_DATA
                    SET
                        ID_SK_MASTER='{$master}',
                        NO_SK_DATA='{$no_sk}',
                        KETERANGAN_SK_DATE='{$keterangan}'
                WHERE ID_SK_DATA='{$id}'
                ");
            echo alert_success("Data Berhasil Di Update");
        }
    }

    function delete_input_data($id) {
        $data = $this->get_input_data($id);
        unlink("./file-sk/{$data['FILE_SK_DATA']}");
        $this->db->Query("DELETE FROM SK_DATA WHERE ID_SK_DATA='{$id}'");
    }

}

?>
