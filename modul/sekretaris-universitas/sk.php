<?php

include 'config.php';
include 'class/sk.class.php';

$sk = new sk($db);

if (isset($_GET['mode'])) {
    if (get('mode') == 'add') {
        $smarty->assign('data_jenis', $sk->load_jenis_sk());
        $smarty->display("add-sk.tpl");
    } elseif (get('mode') == 'edit') {
        $smarty->assign('data_jenis', $sk->load_jenis_sk());
        $smarty->assign('data_sk', $sk->get_sk(get('id')));
        $smarty->display("edit-sk.tpl");
    } elseif (get('mode') == 'delete') {
        $smarty->assign('data_sk', $sk->get_sk(get('id')));
        $smarty->display("delete-sk.tpl");
    }
} else {
    if (isset($_POST)) {
        if (post('mode') == 'add') {
            $sk->add_sk(post('jenis'), strip_tags(post('nama')), strip_tags(post('keterangan')));
        } else if (post('mode') == 'edit') {
            $sk->update_sk(post('id'), post('jenis'), strip_tags(post('nama')), strip_tags(post('keterangan')));
        } else if (post('mode') == 'delete') {
            $sk->delete_sk(post('id'));
        }
    }
    $smarty->assign('data_sk', $sk->load_sk());
    $smarty->display("sk.tpl");
}
?>