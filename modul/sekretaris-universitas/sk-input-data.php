<?php

include 'config.php';
include 'class/sk.class.php';

$sk = new sk($db);

if (isset($_GET['mode'])) {
    if (get('mode') == 'add') {
        $smarty->assign('data_sk', $sk->load_sk());
        $smarty->display("add-sk-input.tpl");
    } elseif (get('mode') == 'edit') {
        $smarty->assign('data_sk', $sk->load_sk());
        $smarty->assign('data', $sk->get_input_data(get('id')));
        $smarty->display("edit-sk-input.tpl");
    } elseif (get('mode') == 'upload') {
        $smarty->assign('data_sk', $sk->load_sk());
        $smarty->assign('data', $sk->get_input_data(get('id')));
        $smarty->display("upload-data-sk-input.tpl");
    } elseif (get('mode') == 'delete') {
        $smarty->assign('data_sk', $sk->load_sk());
        $smarty->assign('data', $sk->get_input_data(get('id')));
        $smarty->display("delete-sk-input.tpl");
    } elseif (get('mode') == 'detail') {
        $smarty->assign('data_detail', $sk->load_detail_data(get('id')));
        $smarty->assign('data', $sk->get_input_data(get('id')));
        $smarty->display("detail-sk-input.tpl");
    }
} else {
    if (isset($_POST)) {
        if (post('mode') == 'add') {
            $sk->add_input_data(post('nama'), post('no_sk'), post('keterangan'), $_FILES);
        } else if (post('mode') == 'edit') {
            $sk->update_input_data(post('id'), post('nama'), post('no_sk'), post('keterangan'), $_FILES);
        } else if (post('mode') == 'delete') {
            $sk->delete_input_data(post('id'));
        } else if (post('mode') == 'upload-excel') {
            // Test CVS
            include 'class/reader.php';

            // ExcelFile
            $data = new Spreadsheet_Excel_Reader();
            $id = post('id_sk_data');
            $tipe = post('tipe');

            // Set output Encoding.

            $filename = $_FILES["file"]["name"];
            if ($_FILES["file"]["error"] > 0) {
                $status_upload = '0';
            } else {
                if (file_exists("file-data-detail/" . $filename)) {
                    echo alert_error('Keterangan : ' . $filename . " Sudah Ada ");
                    $status_upload = '0';
                } else {
                    move_uploaded_file($_FILES["file"]["tmp_name"], "file-data-detail/" . $filename);
                    chmod("file-data-detail/" . $filename, 0755);
                    echo alert_success("Berhasil Upload File");
                    $db->Query("UPDATE SK_DATA SET FILE_SK_DATA_DETAIL='{$filename}' WHERE ID_SK_DATA='{$id}'");
                    $status_upload = '1';
                }
            }
            if ($status_upload) {
                $data->setOutputEncoding('CP1251');
                $data->read("file-data-detail/{$filename}");
                for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
                    $identitas = trim($data->sheets[0]['cells'][$i][1]);
                    if ($tipe == 1) {
                        $db->Query("SELECT * FROM MAHASISWA WHERE NIM_MHS='{$identitas}'");
                        $cek = $db->FetchAssoc();
                    } else if ($tipe == '2') {
                        
                    }
                    if ($cek != '') {
                        $db->Query("
                                INSERT INTO SK_DATA_DETAIL
                                    (ID_SK_DATA,ID_MHS,ID_C_MHS,TIPE_DATA)
                                VALUES
                                    ('{$id}','{$cek['ID_MHS']}','','1')
                                ");
                    } else {
                        echo alert_error("Data Mahasiswa {$identitas} Tidak Ada");
                    }
                }
            }
        }
    }
    $smarty->assign('data', $sk->load_input_data());
    $smarty->display("sk-input-data.tpl");
}
?>