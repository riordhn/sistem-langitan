<?php

include 'config.php';
include 'class/sk.class.php';

$sk = new sk($db);

if (isset($_GET['mode'])) {
    if (get('mode') == 'add') {
        $smarty->display("add-sk-jenis.tpl");
    } elseif (get('mode') == 'edit') {
        $smarty->assign('data_jenis', $sk->get_jenis_sk(get('id')));
        $smarty->display("edit-sk-jenis.tpl");
    } elseif (get('mode') == 'delete') {
        $smarty->assign('data_jenis', $sk->get_jenis_sk(get('id')));
        $smarty->display("delete-sk-jenis.tpl");
    }
} else {
    if (isset($_POST)) {
        if (post('mode') == 'add') {
            $sk->add_jenis_sk(strip_tags(post('nama')), strip_tags(post('keterangan')));
        } else if (post('mode') == 'edit') {
            $sk->update_jenis_sk(post('id'), strip_tags(post('nama')), strip_tags(post('keterangan')));
        } else if (post('mode') == 'delete') {
            $sk->delete_jenis_sk(post('id'));
        }
    }
    $smarty->assign('data_jenis', $sk->load_jenis_sk());
    $smarty->display("sk-jenis.tpl");
}
?>