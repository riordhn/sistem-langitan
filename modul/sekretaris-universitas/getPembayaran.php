<?php
include '../../config.php';

$mhs = $db->QueryToArray("SELECT TAHUN_AJARAN, NM_SEMESTER, NAMA_STATUS, TGL_BAYAR, SUM(BESAR_BIAYA) AS BESAR_BIAYA, PEMBAYARAN.ID_STATUS_PEMBAYARAN
							FROM PEMBAYARAN 
							LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PEMBAYARAN.ID_SEMESTER
							LEFT JOIN STATUS_PEMBAYARAN ON STATUS_PEMBAYARAN.ID_STATUS_PEMBAYARAN = PEMBAYARAN.ID_STATUS_PEMBAYARAN
							WHERE ID_MHS = '".$_GET['id_mhs']."' 
							GROUP BY TAHUN_AJARAN, NM_SEMESTER, NAMA_STATUS, TGL_BAYAR, PEMBAYARAN.ID_STATUS_PEMBAYARAN
							ORDER BY TAHUN_AJARAN DESC, NM_SEMESTER DESC");

	echo ' <tr class="tr" bgcolor="#99FF66">
                  <td colspan="4" style="text-align:center; font-weight:bold">Semester</td>
                  <td colspan="4" style="text-align:center;font-weight:bold">Status Pembayaran</td>
                  <td colspan="4" style="text-align:center;font-weight:bold">Besar Biaya</td>
                  <td colspan="3" style="text-align:center;font-weight:bold">Tgl Bayar</td>
                </tr>';
foreach($mhs as $data){
	if($data['ID_STATUS_PEMBAYARAN'] == 2 or $data['ID_STATUS_PEMBAYARAN'] == ''){
		$bgcolor = ' bgcolor="#FFCC99"';
	}else{
		$bgcolor = '';
	}
	echo '<tr '.$bgcolor.'>
                  <td colspan="4">'.$data['TAHUN_AJARAN'].' ('.$data['NM_SEMESTER'].')</td>
                  <td colspan="4">'.$data['NAMA_STATUS'].'</td>
                  <td colspan="4" style="text-align:right">'.number_format($data['BESAR_BIAYA']).'</td>
                  <td colspan="3">'.$data['TGL_BAYAR'].'</td>
          </tr>';	
}
?>
