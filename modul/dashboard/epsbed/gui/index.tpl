<html>
    <head>
        <title></title>
    </head>
    <body>
        
        <form action="index.php" method="get">
            <table>
                <tr>
                    <td>Fakultas</td>
                    <td>{if isset($smarty.get.id_fakultas)}{$id_fakultas=$smarty.get.id_fakultas}{else}{$id_fakultas=0}{/if}
                        <select name="id_fakultas">
                        <option value="">-- Pilih Fakultas --</option>
                        {foreach $fakultas_set as $f}
                            <option value="{$f.ID_FAKULTAS}" {if $f.ID_FAKULTAS==$id_fakultas}selected="selected"{/if}>{$f.NM_FAKULTAS}</option>
                        {/foreach}
                        </select>
                    </td>
                    <td>Program Studi</td>
                    <td>{if isset($smarty.get.id_program_studi)}{$id_program_studi=$smarty.get.id_program_studi}{else}{$id_program_studi=0}{/if}
                        <select name="id_program_studi">
                            <option value="">-- Pilih Program Studi --</option>
                            {if $program_studi_set}
                                {foreach $program_studi_set as $ps}
                                    <option value="{$ps.ID_PROGRAM_STUDI}" {if $ps.ID_PROGRAM_STUDI==$id_program_studi}selected="selected"{/if}>{ucwords(strtolower($ps.NM_PROGRAM_STUDI))}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </td>
                    <td>
                        <input type="submit" value="Lihat" />
                    </td>
                </tr>
            </table>
        </form>
    
    </body>
</html>