<?php
include('../../../config.php');

$id_fakultas = get('id_fakultas', 0);
$id_program_studi = get('id_program_studi', 0);

$smarty->assign('fakultas_set', $db->QueryToArray("select * from fakultas"));

if ($id_fakultas != 0)
{
    $smarty->assign('program_studi_set', $db->QueryToArray("
        select ps.id_program_studi, j.nm_jenjang || ' ' || ps.nm_program_studi as nm_program_studi from program_studi ps
        join jenjang j on j.id_jenjang = ps.id_jenjang
        where id_fakultas = {$id_fakultas}"));
        
}

if ($id_fakultas != 0 && $id_program_studi != 0)
{
    
}

$smarty->display("index.tpl");
?>
