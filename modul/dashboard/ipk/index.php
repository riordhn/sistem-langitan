<?php
    //header("Location: index.php?menu=home");
?>
<?php 
    ob_start("ob_gzhandler");
?>
<?php
    //We've included ../Includes/FusionCharts.php, which contains functions
    //to help us easily embed the charts.
    include '../../../config.php';
    include '../../../includes/dbconnect.php';
    include '../../../includes/FusionCharts.php';
    //include 'proses/c_dashboard_ipk.php';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href=""/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>DASHBOARD IPK</title>

    <link type="text/css" href="../menu.css" rel="stylesheet" />


    <script type="text/javascript" src="../menu.js"></script>
    <script type="text/javascript" src="../js/jquery.js"></script>
	
<style type="text/css">
<!--
body {
	background-image: url();
}
-->
</style></head>
<body>

<style type="text/css">
* { margin:0;
    padding:0;
}
html { background:#444; }
body {
    margin:0px auto;
    width:800px;
    height:auto;
    background:#FFF;
    overflow:auto;
}
div#menu {
    top:0px;
    left:0px;
    width:100%;
		background:transparent url(../images/header_bg.gif) repeat-x 0 0;  
}
div#copyright { display: none; }

span.footer{
	font-size:12px;
	font-family:sans-serif;
}
</style>


<div style="height:auto">

<div id="menu">
    <ul class="menu">
        <li><a href="index.php?menu=home"><span>home</span></a>
        <li><a href="index.php?menu=FST"><span>FST</span></a>
        <li><a href="index.php?menu=FKH"><span>FKH</span></a>
        <li><a href="index.php?menu=FF"><span>FF</span></a>
<?php
        $stid = oci_parse($conn, "select singkatan_fakultas 
                                    from fakultas
                                    where singkatan_fakultas not like 'FST'
                                    and singkatan_fakultas not like 'FKH'
                                    and singkatan_fakultas not like 'FF'
                                    ");
        oci_execute($stid);

        while (($row = oci_fetch_array($stid, OCI_NUM))) {
?>
        <li><a href="index.php?menu=<?php echo $row[0]; ?>"><span><?php echo $row[0]; ?></span></a>
        </li>
        <?php
            }
        ?>
    </ul>
</div>
<hr>


<div id="pageContent">
   <?php
   $menu = get('menu');
   //$menu = isSet($_GET["menu"]) ? $_GET["menu"] : '';
        if($menu == ''){
            include 'home.php';
        }
        
        if($menu == 'home'){
            include 'home.php';
        }
        if($menu == 'FK'){
            include 'fakultas.php';
        }
        if($menu == 'FKG'){
            include 'fakultas.php';
        }
        if($menu == 'FH'){
            include 'fakultas.php';
        }
        if($menu == 'FEB'){
            include 'fakultas.php';
        }
        if($menu == 'FF'){
            include 'fakultas.php';
        }
        if($menu == 'FKH'){
            include 'fakultas.php';
        }
        if($menu == 'FISIP'){
            include 'fakultas.php';
        }
        if($menu == 'FST'){
            include 'fakultas.php';
        }
        if($menu == 'PPS'){
            include 'fakultas.php';
        }
   ?>
</div>


</body>
</html>