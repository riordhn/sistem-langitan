<?php
include 'create-data-xml.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="../../../css/dashboard-style.css" />
        <link rel="SHORTCUT ICON" href="../../../img/keuangan/iconunair.ico"/>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <title>Dashboard - Universitas Airlangga</title>
    </head>
    <body>
        <div id="main_container">
            <div id="header">
                <div id="judul_header"></div>
                <div id="menu_tab">
                    <ul class="topnav">

                    </ul>
                </div>
            </div>
            <div id="center">
                <div id="center_content">
                    <div id="peta_sebaran" style="margin-bottom: 20px">
                        <center>
                            <div class="center_title_bar center">Sebaran Wilayah Asal Mahasiswa Baru <?php echo date('Y') ?></div>
                            <div id="map-sebaran" style="background-color: #CACFD3;padding: 10px;width: 90%">
                                <div id="map-title">
                                </div>
                                <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="98%" height="480" id="FlashID" title="map">
                                    <param name="movie" value="PETA SEBARAN_BARU.swf" />
                                    <param name="quality" value="high" />
                                    <param name="wmode" value="opaque" />
                                    <param name="swfversion" value="7.0.70.0" />
                                    <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
                                    <param name="expressinstall" value="Scripts/expressInstall.swf" />
                                    <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
                                    <!--[if !IE]>-->
                                    <object type="application/x-shockwave-flash" data="PETA SEBARAN_BARU.swf" width="98%" height="480">
                                        <!--<![endif]-->
                                        <param name="quality" value="high" />
                                        <param name="wmode" value="opaque" />
                                        <param name="swfversion" value="7.0.70.0" />
                                        <param name="expressinstall" value="Scripts/expressInstall.swf" />
                                        <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                                        <div>
                                            <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
                                            <p>
                                                <a href="http://www.adobe.com/go/getflashplayer">
                                                    <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                </a>
                                            </p>
                                        </div>
                                        <!--[if !IE]>-->
                                    </object>
                                    <!--<![endif]-->
                                </object>
                            </div>
                        </center>
                    </div>
                    <div id="penerimaan" style="width: 100%;margin-left: auto;margin-right: auto">
                        <table id="penerimaan-table" style="width: 95%" border="0">
                            <tr>
                                <td style="width: 50%;" align="center">
                                    <div id="penerimaan1">
                                        <div class="center_title_bar center">Penerimaan SNMPTN Undangan <?php echo date('Y') ?></div>
                                        <iframe  style="margin:0px auto; " src="penerimaan_data/penerimaan-data.php?id_penerimaan=137" height="400px" width="500px" frameborder="0" scrolling="no"></iframe>
                                    </div>
                                </td>
                            </tr>
                        </table>                        
                    </div>

                </div>
            </div>
            <div id="footer">
                <div id="left_footer">
                    <img src="../../../img/keuangan/footer_logo.jpg" width="170" height="37" />
                </div>
                <div id="center_footer">
                    Copyright © 2011 - Universitas Airlangga · All Rights Reserved<br />
                </div>
                <div id="right_footer">
                    <a href="#">home</a>
                    <a href="#">about</a>
                    <a href="#">sitemap</a>
                    <a href="#">rss</a>
                    <a href="#">contact us</a>
                </div>
            </div>
        </div>
    </body>
</html>
