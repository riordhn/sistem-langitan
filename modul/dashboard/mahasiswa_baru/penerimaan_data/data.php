<?php
include '../../../../config.php';

$id_penerimaan = get('id_penerimaan');

$rows = $db->QueryToArray("
    select
        p.id_penerimaan, jumlah_diterima, jumlah_bayar, nvl(pendidikan,0) as jumlah_verifikasi_pendidikan,
        nvl(keuangan,0) as jumlah_verifikasi_keuangan, nvl(regmaba,0) as jumlah_regmaba,
        nvl(peminat,0) as jumlah_peminat
    from penerimaan p
    left join (
        select id_penerimaan, count(id_c_mhs) jumlah_diterima from calon_mahasiswa_baru cmb
        where cmb.tgl_diterima is not null
        group by id_penerimaan) diterima on diterima.id_penerimaan = p.id_penerimaan
    left join (
        select id_penerimaan, count(id_c_mhs) jumlah_bayar from (
            select cmb.id_c_mhs, cmb.id_penerimaan,
                (select count(pc.id_c_mhs) from pembayaran_cmhs pc where pc.id_c_mhs = cmb.id_c_mhs  and PC.ID_STATUS_PEMBAYARAN != 2) as jumlah_bayar
            from calon_mahasiswa_baru cmb) t
        where jumlah_bayar > 0
        group by id_penerimaan) bayar on bayar.id_penerimaan = p.id_penerimaan
    left join (
        select id_penerimaan, count(id_c_mhs) pendidikan from calon_mahasiswa_baru cmb
        where cmb.tgl_diterima is not null and cmb.tgl_verifikasi_pendidikan is not null
        group by id_penerimaan) pendidikan on pendidikan.id_penerimaan = p.id_penerimaan
    left join (
        select id_penerimaan, count(id_c_mhs) keuangan from calon_mahasiswa_baru cmb
        where cmb.tgl_diterima is not null and cmb.tgl_verifikasi_keuangan is not null
        group by id_penerimaan) keuangan on keuangan.id_penerimaan = p.id_penerimaan
    left join (
        select id_penerimaan, count(id_c_mhs) regmaba from calon_mahasiswa_baru cmb
        where cmb.tgl_diterima is not null and cmb.tgl_regmaba is not null
        group by id_penerimaan) kesehatan on kesehatan.id_penerimaan = p.id_penerimaan
    left join (
        select id_penerimaan, count(id_c_mhs) peminat from calon_mahasiswa_baru cmb
        group by id_penerimaan) peminat on peminat.id_penerimaan = p.id_penerimaan
    where p.id_penerimaan = {$id_penerimaan}");

echo "<chart labelDisplay='ROTATE' slantLabels='1' seriesNameInToolTip='0' formatNumberScale='0' yAxisName='Jumlah Mahasiswa'>
          <set label='Peminat' value='".$rows[0]['JUMLAH_PEMINAT']."' />
          <set label='Diterima' value='".$rows[0]['JUMLAH_DITERIMA']."' />
          <set label='Regmaba' value='".$rows[0]['JUMLAH_REGMABA']."' />";
		  if($id_penerimaan == 44 or $id_penerimaan == 45){
          	echo "<set label='Verifikasi Keuangan' value='".$rows[0]['JUMLAH_VERIFIKASI_KEUANGAN']."' />";
		  }
echo "<set label='Bayar' value='".$rows[0]['JUMLAH_BAYAR']."' />
		  <set label='Verifikasi Pendidikan' value='".$rows[0]['JUMLAH_VERIFIKASI_PENDIDIKAN']."' />
        </chart>";
?>