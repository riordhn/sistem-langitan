<?php
require "common.php";
require "excel.php";
//echo "1<br>";
//exit;
$kode=get("kode","5");
$id_program_studi=get("id_program_studi");
$thn_angkatan_mhs=get("thn_angkatan_mhs");
if($thn_angkatan_mhs=="Semua"){
$thn_angkatan_mhs="%";
}
$db->Query("SELECT id_semester,nm_semester,tahun_ajaran FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True'");
$row=$db->FetchAssoc();
$id_semester = $row["ID_SEMESTER"];
$nm_semester = $row["NM_SEMESTER"];
$tahun_ajaran = $row["THN_AKADEMIK_SEMESTER"];
$db->Query("SELECT A.NM_PROGRAM_STUDI,B.NM_JENJANG FROM PROGRAM_STUDI A LEFT JOIN JENJANG B ON(A.ID_JENJANG=B.ID_JENJANG) WHERE A.ID_PROGRAM_STUDI='{$id_program_studi}'");
$row=$db->FetchAssoc();
$prodi = $row["NM_JENJANG"]."_".$row["NM_PROGRAM_STUDI"];


switch($kode)
{
case "0":
$sql="
    SELECT PS.ID_FAKULTAS, COUNT(ID_MHS) AS JUMLAH FROM MAHASISWA M
    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
    WHERE M.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM SESSION_PENGGUNA WHERE WAKTU_SESSION >= {$ten_minutes_ago})
	
";
$n_kode="";
break;
case "1": //belum regis
$sql="
select 
b.NIM_MHS,
d.NM_PENGGUNA NM_MAHASISWA,
e.NM_PENGGUNA NM_DOLI
from 
(
select id_mhs from pembayaran where id_semester={$id_semester} and id_status_pembayaran=2
group by id_mhs
)
a
left join (select NIM_MHS from mahasiswa where  thn_angkatan_mhs like '{$thn_angkatan_mhs}') b on (a.id_mhs=b.id_mhs)
left join (select * from dosen_wali where status_dosen_wali=1  ) c on(a.id_mhs=c.id_mhs)
left join dosen f on(f.id_dosen=c.id_dosen)
left join pengguna d on(b.id_pengguna=d.id_pengguna)
left join pengguna e on(f.id_pengguna=e.id_pengguna)
where  b.id_program_studi='{$id_program_studi}'  and b.id_mhs is not null

";
$nkode="belum_regis_";
break;
case "2": //sudah regis
$sql="
select 
b.NIM_MHS,
d.NM_PENGGUNA NM_MAHASISWA,
e.NM_PENGGUNA NM_DOLI,m.tgl_bayar

from 
(
select id_mhs from pembayaran where id_semester={$id_semester} and id_status_pembayaran not in ('1','3','4')

)
a
left join (select NIM_MHS from mahasiswa where thn_angkatan_mhs like '{$thn_angkatan_mhs}') b on (a.id_mhs=b.id_mhs)
left join (select * from dosen_wali where status_dosen_wali=1 ) c on(a.id_mhs=c.id_mhs)
left join dosen f on(f.id_dosen=c.id_dosen)
left join pengguna d on(b.id_pengguna=d.id_pengguna)
left join pengguna e on(f.id_pengguna=e.id_pengguna)
left join (
select a.id_mhs,a.tgl_bayar from pembayaran a
left join detail_biaya b on a.id_detail_biaya=b.id_detail_biaya where a.id_semester={$id_semester} and a.tgl_bayar is not null and b.id_biaya=47

) m on(m.id_mhs=a.id_mhs)
where  b.id_program_studi='{$id_program_studi}' and b.id_mhs is not null

";
$nkode="sudah_regis_";
break;
case "6": //belum krs
$sql="
select 
b.NIM_MHS,
d.NM_PENGGUNA NM_MAHASISWA,
e.NM_PENGGUNA NM_DOLI
from 
(select NIM_MHS from mahasiswa where thn_angkatan_mhs like '{$thn_angkatan_mhs}'
and id_mhs not in
(
select id_mhs from pengambilan_mk where id_semester={$id_semester} group by id_mhs
)
) b 
left join (select * from dosen_wali where status_dosen_wali=1 ) c on(b.id_mhs=c.id_mhs)
left join dosen f on(f.id_dosen=c.id_dosen)
left join pengguna d on(b.id_pengguna=d.id_pengguna)
left join pengguna e on(f.id_pengguna=e.id_pengguna)
left join (select id_mhs from pembayaran where id_semester={$id_semester} and tgl_bayar is not null group by id_mhs) g on (b.id_mhs=g.id_mhs)
where  b.id_program_studi='{$id_program_studi}' and g.id_mhs is not null and b.id_mhs is not null

";
$nkode="belum_krs_";
break;
case "5": //sudah krs
$sql="
select 
b.NIM_MHS,
d.NM_PENGGUNA NM_MAHASISWA,
e.NM_PENGGUNA NM_DOLI
from 
(
select id_mhs from pengambilan_mk where id_semester={$id_semester} group by id_mhs
)
a
left join (select nim_mhs from mahasiswa where thn_angkatan_mhs like '{$thn_angkatan_mhs}') b on (a.id_mhs=b.id_mhs)
left join (select * from dosen_wali where status_dosen_wali=1 ) c on(a.id_mhs=c.id_mhs)
left join dosen f on(f.id_dosen=c.id_dosen)
left join pengguna d on(b.id_pengguna=d.id_pengguna)
left join pengguna e on(f.id_pengguna=e.id_pengguna)
where  b.id_program_studi='{$id_program_studi}' and b.id_mhs is not null

";
$nkode="sudah_krs_";
break;
case "3": //belum diacc
$sql="
select 
b.NIM_MHS,
d.NM_PENGGUNA NM_MAHASISWA,
e.NM_PENGGUNA NM_DOLI
from 
(
select id_mhs from pengambilan_mk where id_semester={$id_semester} and status_apv_pengambilan_mk=0 group by id_mhs
)
a
left join (select nim_mhs from mahasiswa where thn_angkatan_mhs like '{$thn_angkatan_mhs}') b on (a.id_mhs=b.id_mhs)
left join (select * from dosen_wali where status_dosen_wali=1 ) c on(a.id_mhs=c.id_mhs)
left join dosen f on(f.id_dosen=c.id_dosen)
left join pengguna d on(b.id_pengguna=d.id_pengguna)
left join pengguna e on(f.id_pengguna=e.id_pengguna)
where  b.id_program_studi='{$id_program_studi}' and b.id_mhs is not null

";
$nkode="belum_diacc_";
break;
case "4": //sudah diacc
$sql="
select 
b.NIM_MHS,
d.NM_PENGGUNA NM_MAHASISWA,
e.NM_PENGGUNA NM_DOLI
from 
(
select id_mhs from pengambilan_mk where id_semester={$id_semester} and status_apv_pengambilan_mk=1 group by id_mhs
)
a
left join (select NIM_MHS from mahasiswa where thn_angkatan_mhs like '{$thn_angkatan_mhs}') b on (a.id_mhs=b.id_mhs)
left join (select * from dosen_wali where status_dosen_wali=1 ) c on(a.id_mhs=c.id_mhs)
left join dosen f on(f.id_dosen=c.id_dosen)
left join pengguna d on(b.id_pengguna=d.id_pengguna)
left join pengguna e on(f.id_pengguna=e.id_pengguna)
where  b.id_program_studi='{$id_program_studi}' and b.id_mhs is not null

";
$nkode="sudah_diacc_";
break;
}
$sql.=" ORDER BY NIM_MHS";
//echo $sql;
//exit;
$db->Query($sql); 
$data=array();$i=0;
while($Fields=$db->FetchAssoc())
{
$i++;
$nim=strtoupper($Fields['NIM_MHS']);
$nmmhs=$Fields['NM_MAHASISWA'];
$dosenwali=$Fields['NM_DOLI'];
if($kode=="2")
{
  $data[]=
  array("NO"=>$i,	"NIM"=>$nim,
		"NAMA"=>$nmmhs,
		"DOSEN WALI"=>$dosenwali,		
		"TGL BAYAR"=>$Fields['TGL_BAYAR']
	);
   
}else
{
  $data[]=
  array("NO"=>$i,	"NIM"=>$nim,
		"NAMA"=>$nmmhs,
		"DOSEN WALI"=>$dosenwali
	);
}
}
//echo $sql;
//print_r($data);exit;
if($thn_angkatan_mhs=="%")$thn_angkatan_mhs="semua";
$filename=$nkode.$prodi."_".$tahun_ajaran."_".$nm_semester."_angkatan_".$thn_angkatan_mhs.".xls";
$filename=str_replace("/","",$filename);
$fp = fopen("xlsfile://tmp/$filename", "wb");
fwrite($fp, serialize($data));
 fclose($fp);
        header ("Content-Type: application/x-msexcel");
        header ("Content-Disposition: attachment; filename=\"$filename\"" );
        readfile("xlsfile://tmp/$filename");
        exit;
?>
