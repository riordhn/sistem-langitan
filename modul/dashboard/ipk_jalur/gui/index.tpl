<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header"><div id="judul_header"></div></div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">Penerimaan Universitas Airlangga</h1>
<div id="content">

<table id="rounded-corner">
    <thead>
    	<tr>
            <th scope="col" class="rounded-company">Penerimaan</th>
            <th scope="col" class="rounded-q1" style="text-align: right">SOP</th>
            <th scope="col" class="rounded-q4" style="text-align: right">SP3</th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="2" class="rounded-foot-left"><em>&nbsp;</em></td>
            <td class="rounded-foot-right">&nbsp;</td>
        </tr>
    </tfoot>
    <tbody>
        {$sop = 0}{$sp3 = 0}
        {foreach $jenjang_set as $j}
        <tr>
            <td><a href="?mode=fakultas&kode={$j.N}">{$j.JALUR}<a/></td>
            <td style="text-align: right">Rp {number_format($j.SOP, 0, ",", ".")}</td>
            <td style="text-align: right">Rp {number_format($j.SP3, 0, ",", ".")}</td>
        </tr>
        {$sop = $sop + $j.SOP}
        {$sp3 = $sp3 + $j.SP3}
        {/foreach}
        <tr>
            <td><strong>TOTAL</strong></td>
            <td style="text-align: right"><strong>Rp {number_format($sop, 0, ",", ".")}</strong></td>
            <td style="text-align: right"><strong>Rp {number_format($sp3, 0, ",", ".")}</strong></td>
        </tr>
    </tbody>
</table>
</div>
</body>
</html>