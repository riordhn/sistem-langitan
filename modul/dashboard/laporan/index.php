<?php
    //We've included ../Includes/FusionCharts.php, which contains functions
    //to help us easily embed the charts.
    require_once "../../../includes/dbconnect.php";
    require_once "../../../includes/FusionCharts.php";
    //require_once "../../../config.php";
?>

<html>
    <head>
        <title>
            Laporan Perkembangan Maba
        </title>
	<script type="text/javascript">
            function reFresh() {
                location.reload()
            }            
            window.setInterval("reFresh()",50000);
        </script>

    </head>

    <body>
<!--div style="margin: auto; padding-top: 0; position: relative; width: 1024px; height: 765px; border-style: solid; border-width: 1px; border-color: #C0C0C0;"-->

    
                    <h1 align=center>Perkembangan Pendaftaran Mahasiswa Baru 2011</h1>
              
        <div style="padding-top: 32px; padding-left: 55px; padding-right: 50px;  float: middle;">
            <table width="920px;" height="610px;" border="1" cellpadding="1" cellspacing="1" align="center">
                <tr>
                    <td colspan=2>
					   <h2 align=center>Rekap Jumlah Mahasiswa Baru per Jalur</h2>
                        <?php
						include "rekap_jlr.php";
                        ?>
                    </td>
                </tr>				
				<tr>
                    <td>
					   <h2 align=center>Rekap Jumlah Mahasiswa Baru Jalur SNMPTN</h2>
                        <?php
						include "rekap_snmptn.php";
                        ?>
                    </td>
                    <td>
					   <h2 align=center>Rekap Jumlah Mahasiswa Baru Jalur Mandiri</h2>
                        <?php
                       include "rekap_mandiri.php";
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
						<h2 align=center>Rekap Jumlah Mahasiswa Baru Jalur PASCA</h2>
                        <?php
                        //include "rekap_snmptn.php";
                        ?>
                    </td>
                    <td>

                        <?php
                        //include "rekap_snmptn.php";
                        ?>

                    </td>
                </tr>
            </table>
        </div>

    <!--/div-->

    </body>
</html>
