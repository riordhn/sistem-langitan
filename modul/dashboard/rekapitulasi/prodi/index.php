<?php
include('../../../../config.php');

$fakultas_set = $db->QueryToArray("select * from fakultas");

for ($i = 0; $i < count($fakultas_set); $i++)
{
    $fakultas_set[$i]['program_studi_set'] = $db->QueryToArray("
        select id_prodi_spesifikasi,daya.id_prodi_dayatampung,pb.id_prodi_biaya, pb.is_mandiri, pb.sop_usulan, pb.sp3a_usulan, pb.sp3b_usulan, pb.sp3c_usulan, syarat.id_prodi_syarat, ps.id_program_studi, nm_jenjang, nm_program_studi,spek.is_alih_jenis,
            (select count(*) from prodi_spesifikasi spek2
             where spek2.id_prodi_spesifikasi = spek.id_prodi_spesifikasi and
             spek2.prodi_tahun is not null and spek2.prodi_akred is not null and spek2.prodi_skp_nomor is not null) prodi_spek,
            (select count(*) from prodi_biaya pb
             where sop_usulan is not null and sp3_usulan is not null and sop_usulan <> 0 and sp3_usulan <> 0 and
             pb.id_program_studi = ps.id_program_studi and pb.id_semester = 5) as prodi_biaya,
            (select count(*) from prodi_dayatampung pd
             where (semester_1_usulan is not null or semester_2_usulan is not null or snmptn_usulan is not null or mandiri_usulan is not null or total_usulan is not null or daya_tampung_usulan is not null) and
             pd.id_semester = 5 and pd.id_program_studi = ps.id_program_studi) as daya_tampung,
            (select count(*) from prodi_syarat psy
             where prodi_syarat_khusus is not null and psy.id_program_studi = ps.id_program_studi and psy.id_semester = 5 and psy.is_alih_jenis = spek.is_alih_jenis) as prodi_syarat
        from program_studi ps
        join jenjang j on j.id_jenjang = ps.id_jenjang
        join prodi_spesifikasi spek on spek.id_program_studi = ps.id_program_studi
        join prodi_biaya pb on pb.id_program_studi= ps.id_program_studi
        join prodi_dayatampung daya on daya.id_program_studi = ps.id_program_studi
        join prodi_syarat syarat on syarat.id_program_studi = ps.id_program_studi
        where ps.id_fakultas = {$fakultas_set[$i]['ID_FAKULTAS']} and ps.id_program_studi not in (47, 100, 197) and spek.is_alih_jenis=daya.is_alih_jenis and spek.is_alih_jenis=pb.is_alih_jenis and daya.is_alih_jenis=syarat.is_alih_jenis
        order by j.id_jenjang, ps.nm_program_studi");
}

$smarty->assign('fakultas_set', $fakultas_set);

$smarty->display('index.tpl');
?>
