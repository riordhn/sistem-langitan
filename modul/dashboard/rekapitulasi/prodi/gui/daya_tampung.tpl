<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header">
<div id="judul_header"></div>
</div>
<div id="content">
<a class="button" href="./#{$dt.ID_FAKULTAS}">Kembali ke dashboard</a>
<h2 style="font-family:'Trebuchet MS'">Program Studi {$dt.NM_JENJANG} - {$dt.NM_PROGRAM_STUDI} {if $dt.IS_ALIH_JENIS==1} [Alih Jenis] {/if}</h2>
<table id="hor-minimalist-b" summary="Employee Pay Sheet">
    <thead>
    	<tr>
            <th scope="col" style="width: 250px">Nama Spesifikasi Daya Tampung</th>
            <th scope="col">Jumlah Daya Tampung Program Studi</th>
        </tr>
    </thead>
    <tbody>
    	<tr>
            <td><strong>SNMPTN Sesuai SK</strong></td>
            <td>{$dt.SNMPTN_SK}</td>
        </tr>
        <tr>
            <td><strong>Mandiri Sesuai SK</strong></td>
            <td>{$dt.MANDIRI_SK}</td>
        </tr>
        <tr>
            <td><strong>SNMPTN Realisasi</strong></td>
            <td>{$dt.SNMPTN_REAL}</td>
        </tr>
        <tr>
            <td><strong>Mandiri Realisasi</strong></td>
            <td>{$dt.MANDIRI_REAL}</td>
        </tr>
        <tr>
            <td><strong>SNMPTN Usulan</strong></td>
            <td>{$dt.SNMPTN_USULAN}</td>
        </tr>
		<tr>
            <td><strong>Mandiri Usulan</strong></td>
            <td>{$dt.MANDIRI_USULAN}</td>
        </tr>
		<tr>
            <td><strong>Semester 1 Usulan</strong></td>
            <td>{$dt.SEMESTER_1_USULAN}</td>
        </tr>
		<tr>
            <td><strong>Semester 2 Usulan</strong></td>
            <td>{$dt.SEMESTER_1_USULAN}</td>
		<tr>
            <td><strong>Daya Tampung APK</strong></td>
            <td>{$dt.DAYA_TAMPUNG_APK}</td>
        </tr>
		<tr>
            <td><strong>Daya Tampung Usulan</strong></td>
            <td>{$dt.DAYA_TAMPUNG_USULAN}</td>
        </tr>
		<tr>
            <td><strong>Daya Tampung Total</strong></td>
            <td>{$dt.TOTAL}</td>
        </tr>
    </tbody>
</table>
</div>
</body>
</html>