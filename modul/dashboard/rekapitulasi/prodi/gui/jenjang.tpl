<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header">
<div id="judul_header"></div>
</div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">INFORMASI SPESIFIKASI PROGRAM STUDI PER JENJANG</h1>
<div id="content">
<table id="rounded-corner">
    <thead>
    	<tr>
            <th scope="col" class="rounded-company" style="width: 415px">Jenjang</th>
            <th scope="col" class="rounded-q2" style="text-align: center">Biaya</th>
            <th scope="col" class="rounded-q3" style="text-align: center">Daya Tampung</th>
            <th scope="col" class="rounded-q4" style="text-align: center">Syarat Prodi</th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="3" class="rounded-foot-left"><em>&nbsp;</em></td>
            <td class="rounded-foot-right">&nbsp;</td>
        </tr>
    </tfoot>
    <tbody>
    	<tr>
            <td>Sarjana (S1)</td>
            <td style="text-align: center"><a href="view_biaya_prodi.php?id_jenjang=1">Lihat</a></td>
            <td style="text-align: center"><a href="view_daya_tampung.php?id_jenjang=1">Lihat</a></td>
            <td style="text-align: center"><a href="view_syarat_prodi.php?id_jenjang=1">Lihat</a></td>
        </tr>
        <tr>
            <td>Diploma (D3)</td>
            <td style="text-align: center"><a href="view_biaya_prodi.php?id_jenjang=5">Lihat</a></td>
            <td style="text-align: center"><a href="view_daya_tampung.php?id_jenjang=5">Lihat</a></td>
            <td style="text-align: center"><a href="view_syarat_prodi.php?id_jenjang=5">Lihat</a></td>
        </tr>
        <tr>
            <td>Magister (S2)</td>
            <td style="text-align: center"><a href="view_biaya_prodi.php?id_jenjang=2">Lihat</a></td>
            <td style="text-align: center"><a href="view_daya_tampung.php?id_jenjang=2">Lihat</a></td>
            <td style="text-align: center"><a href="view_syarat_prodi.php?id_jenjang=2">Lihat</a></td>
        </tr>
        <tr>
            <td>Doktor (S3)</td>
            <td style="text-align: center"><a href="view_biaya_prodi.php?id_jenjang=3">Lihat</a></td>
            <td style="text-align: center"><a href="view_daya_tampung.php?id_jenjang=3">Lihat</a></td>
            <td style="text-align: center"><a href="view_syarat_prodi.php?id_jenjang=3">Lihat</a></td>
        </tr>
        <tr>
            <td>Profesi</td>
            <td style="text-align: center"><a href="view_biaya_prodi.php?id_jenjang=9">Lihat</a></td>
            <td style="text-align: center"><a href="view_daya_tampung.php?id_jenjang=9">Lihat</a></td>
            <td style="text-align: center"><a href="view_syarat_prodi.php?id_jenjang=9">Lihat</a></td>
        </tr>
        <tr>
            <td>Spesialis</td>
            <td style="text-align: center"><a href="view_biaya_prodi.php?id_jenjang=10">Lihat</a></td>
            <td style="text-align: center"><a href="view_daya_tampung.php?id_jenjang=10">Lihat</a></td>
            <td style="text-align: center"><a href="view_syarat_prodi.php?id_jenjang=10">Lihat</a></td>
        </tr>
    </tbody>
</table>
    <p align="center"><a class="button" href="index.php">Kembali ke dashboard</a></p>
<br/>
</div>

</body>
</html>