<?php
include('../../../../config.php');

$ips = get('ips', '');


$db->Query("
    select
        ps.id_program_studi, 
        ps.nm_program_studi,
		ps.id_fakultas,
        j.nm_jenjang, 
        prospek.prodi_nama,
        prospek.prodi_jenis,
        prospek.prodi_kategori,
        prospek.prodi_penjelasan,
        prospek.prodi_tahun,
        prospek.prodi_sk_oleh,
        prospek.prodi_sk_nomor,
        prospek.prodi_sk_terbit,
        prospek.prodi_sk_expired,
        prospek.prodi_skp_nomor,
        prospek.prodi_skp_terbit,
        prospek.prodi_skp_expired,
        prospek.prodi_ketua_nama,
        prospek.prodi_ketua_sk_nomor,
        prospek.prodi_ketua_sk_terbit,
        prospek.prodi_ketua_sk_expired,
        prospek.prodi_sekre_nama,
        prospek.prodi_sekre_sk_nomor,
        prospek.prodi_sekre_sk_terbit,
        prospek.prodi_sekre_sk_expired,
        prospek.prodi_dosen_tetap,
        prospek.prodi_dosen_tdktetap,
        prospek.prodi_dosen_s2,
        prospek.prodi_dosen_s3,
        prospek.prodi_dosen_gubes,
        prospek.prodi_kur_nama,
        prospek.prodi_kur_sk_nomor,
        prospek.prodi_kur_sk_terbit,
        prospek.prodi_kur_sk_expired,
        prospek.prodi_kur_masa_studi,
        prospek.prodi_kur_beban_studi,
        prospek.prodi_lo,
        prospek.prodi_gelar_nama,
        prospek.prodi_gelar_singkat,
        prospek.prodi_gelar_sk_nomor,
        prospek.prodi_jml_mhs,
        prospek.prodi_jml_peminat,
        5 as prodi_jml_maba_calon,
        6 as prodi_jml_maba_daftar,
        (SELECT COUNT(*) FROM MAHASISWA
         LEFT JOIN STATUS_PENGGUNA ON MAHASISWA.STATUS_AKADEMIK_MHS = STATUS_PENGGUNA.ID_STATUS_PENGGUNA 
         WHERE STATUS_AKTIF = 1 AND THN_ANGKATAN_MHS <> '2011' AND ID_ROLE = 3 AND ID_PROGRAM_STUDI = prospek.id_program_studi) as prodi_jml_mhs_lama,
        prospek.prodi_jml_alumni,
        prospek.prodi_akred,
        prospek.prodi_akred_sk_nomor,
        prospek.prodi_akred_sk_terbit,
        prospek.prodi_akred_sk_expired,
        prospek.prodi_akred_nilai,
        prospek.prodi_akred_peringkat,
        prospek.prodi_lo,
        prospek.is_alih_jenis
    from prodi_spesifikasi prospek
    left join program_studi ps on ps.id_program_studi = prospek.id_program_studi
    left join fakultas f on f.id_fakultas = ps.id_fakultas
    left join jenjang j on j.id_jenjang = ps.id_jenjang
    where id_prodi_spesifikasi = {$ips}");

/*
$db->Query("
    select
        ps.id_program_studi, 
        ps.nm_program_studi,
		ps.id_fakultas,
        j.nm_jenjang, 
        prospek.prodi_nama,
        prospek.prodi_jenis,
        prospek.prodi_kategori,
        prospek.prodi_penjelasan,
        prospek.prodi_tahun,
        prospek.prodi_sk_oleh,
        prospek.prodi_sk_nomor,
        prospek.prodi_sk_terbit,
        prospek.prodi_sk_expired,
        prospek.prodi_skp_nomor,
        prospek.prodi_skp_terbit,
        prospek.prodi_skp_expired,
        prospek.prodi_ketua_nama,
        prospek.prodi_ketua_sk_nomor,
        prospek.prodi_ketua_sk_terbit,
        prospek.prodi_ketua_sk_expired,
        prospek.prodi_sekre_nama,
        prospek.prodi_sekre_sk_nomor,
        prospek.prodi_sekre_sk_terbit,
        prospek.prodi_sekre_sk_expired,
        prospek.prodi_dosen_tetap,
        prospek.prodi_dosen_tdktetap,
        prospek.prodi_dosen_s2,
        prospek.prodi_dosen_s3,
        prospek.prodi_dosen_gubes,
        prospek.prodi_kur_nama,
        prospek.prodi_kur_sk_nomor,
        prospek.prodi_kur_sk_terbit,
        prospek.prodi_kur_sk_expired,
        prospek.prodi_kur_masa_studi,
        prospek.prodi_kur_beban_studi,
        prospek.prodi_lo,
        prospek.prodi_gelar_nama,
        prospek.prodi_gelar_singkat,
        prospek.prodi_gelar_sk_nomor,
        prospek.prodi_jml_mhs,
        prospek.prodi_jml_peminat,
        prospek.prodi_jml_alumni,
        prospek.prodi_akred,
        prospek.prodi_akred_sk_nomor,
        prospek.prodi_akred_sk_terbit,
        prospek.prodi_akred_sk_expired,
        prospek.prodi_akred_nilai,
        prospek.prodi_akred_peringkat,
        prospek.prodi_lo,
        prospek.is_alih_jenis
    from prodi_spesifikasi prospek
    left join program_studi ps on ps.id_program_studi = prospek.id_program_studi
    left join fakultas f on f.id_fakultas = ps.id_fakultas
    left join jenjang j on j.id_jenjang = ps.id_jenjang
    where id_prodi_spesifikasi = {$ips}");
*/
$row = $db->FetchAssoc();
$smarty->assign('ps', $row);

$smarty->display('spek_prodi.tpl');
?>
