<?php
include('../../../../config.php');

$id_jenjang = get('id_jenjang');

$db->Query("select nm_jenjang from jenjang where id_jenjang = {$id_jenjang}");
$row = $db->FetchAssoc();

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Disposition: attachment;filename=biaya_{$row['NM_JENJANG']}.xls");
header("Content-Transfer-Encoding: binary ");

$pb = $db->QueryToArray("
    select ps.nm_program_studi, pb.* from prodi_biaya pb
    join program_studi ps on (ps.id_program_studi = pb.id_program_studi and pb.id_semester = 5 and ps.id_jenjang = {$id_jenjang})
    where ps.id_program_studi <> 197
    order by ps.id_fakultas");
$smarty->assign('pb_set', $pb);

$smarty->display('print-biaya-prodi.tpl');
?>
