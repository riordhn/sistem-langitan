<?php
    //We've included ../Includes/FusionCharts.php, which contains functions
    //to help us easily embed the charts.
    require_once "../../../includes/dbconnect.php";
    require_once "../../../includes/FusionCharts.php";
    require_once "../../../config.php";
?>

<?php
$date = date('Y') - 6;
$tahun = array();
?>

<html>
    <head>
        <title>
            Statistik Proses Pengajaran
        </title>
<script language="JavaScript" src="../../../js/FusionCharts.js"></script>
    </head>

    <body>
<div style="margin: auto; padding-top: 0; position: relative; width: 1024px; height: 765px; border-style: solid; border-width: 1px; border-color: #C0C0C0;">

    <div id="peta" style="background-image: url(../../../img/dashboard/bg-4div.png); background-repeat: no-repeat; height: 100%; width: 100%; ">
        <div>
            <center>
                <span style="font-family:sans-serif; font-weight: bold; font-size: 36px; color: #FFF;">
                    Profil Distribusi Nilai Kuliah Jenjang D3, S1, S2, S3
                </span>
            </center>
        </div>
        <div style="padding-top: 32px; padding-left: 55px; padding-right: 50px;  float: left;">
            <table width="920px;" height="610px;" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <?php
                        // Distribusi Nilai D3
                        $strXML  = "";
                        $strXML  .= "<chart caption='Distribusi Nilai Kuliah Program D3' subcaption='' lineThickness='1' showValues='0' formatNumberScale='0' anchorRadius='2'   divLineAlpha='20' divLineColor='CC3300' divLineIsDashed='1' showAlternateHGridColor='1' alternateHGridColor='CC3300' shadowAlpha='40' labelStep='1' numvdivlines='5' chartRightMargin='35' bgColor='FFFFFF,0099CC' bgAngle='270' bgAlpha='10,10'>";
                        $strXML  .= "<categories >";
                                for($i = 0; $i < 5; $i++ ){
                                    $tahun[$i] = $date + ($i+1);
                                    $strXML .= "<category label='" . $tahun[$i] . "' />";
                                }
                        $strXML .= "</categories>";

                        $strXML .= "<dataset seriesName='A' color='1D8BD1' anchorBorderColor='1D8BD1' anchorBgColor='1D8BD1'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Mandiri'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .= "</dataset>";
                        //End Mandiri

                        //Universitas
                        $strXML .="<dataset seriesName='B' color='F1683C' anchorBorderColor='F1683C' anchorBgColor='F1683C'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Universitas'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .= "</dataset>";
                        //End Universitas

                        //Luar Dikti
                        $strXML .= "<dataset seriesName='C' color='2AD62A' anchorBorderColor='2AD62A' anchorBgColor='2AD62A'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Luar Dikti'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .="</dataset>";
                        //End Luar Dikti

                        //Dikti
                        $strXML .="<dataset seriesName='D' color='DBDC25' anchorBorderColor='DBDC25' anchorBgColor='DBDC25'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Dikti'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .="</dataset>";
                        //End Dikti

                        //Luar Negeri
                        $strXML .="<dataset seriesName='E' color='C0C0C0' anchorBorderColor='C0C0C0' anchorBgColor='C0C0C0'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Luar Negeri'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .="</dataset>";
                        //End Luar Negeri

                        $strXML .="<styles>
                                <definition>
                                <style name='CaptionFont' type='font' size='12'/>
                                </definition>
                                <application>
                                <apply toObject='CAPTION' styles='CaptionFont' />
                                <apply toObject='SUBCAPTION' styles='CaptionFont' />
                                </application>
                                </styles>

                                </chart>";

                           //Create the chart - Column 3D Chart with data from strXML variable using dataXML method
                           echo renderChartHTML("../../../swf/Charts/MSLine.swf", "", $strXML, "myNext", 450, 300, false);
                           //End Distribusi Nilai D3
                        ?>
                    </td>
                    <td>
                        <?php
                        // Distribusi Nilai S1
                        $strXML  = "";
                        $strXML  .= "<chart caption='Distribusi Nilai Kuliah Program S1' subcaption='' lineThickness='1' showValues='0' formatNumberScale='0' anchorRadius='2'   divLineAlpha='20' divLineColor='CC3300' divLineIsDashed='1' showAlternateHGridColor='1' alternateHGridColor='CC3300' shadowAlpha='40' labelStep='1' numvdivlines='5' chartRightMargin='35' bgColor='FFFFFF,0099CC' bgAngle='270' bgAlpha='10,10'>";
                        $strXML  .= "<categories >";
                                for($i = 0; $i < 5; $i++ ){
                                    $tahun[$i] = $date + ($i+1);
                                    $strXML .= "<category label='" . $tahun[$i] . "' />";
                                }
                        $strXML .= "</categories>";

                        $strXML .= "<dataset seriesName='A' color='1D8BD1' anchorBorderColor='1D8BD1' anchorBgColor='1D8BD1'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Mandiri'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .= "</dataset>";
                        //End Mandiri

                        //Universitas
                        $strXML .="<dataset seriesName='B' color='F1683C' anchorBorderColor='F1683C' anchorBgColor='F1683C'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Universitas'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .= "</dataset>";
                        //End Universitas

                        //Luar Dikti
                        $strXML .= "<dataset seriesName='C' color='2AD62A' anchorBorderColor='2AD62A' anchorBgColor='2AD62A'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Luar Dikti'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .="</dataset>";
                        //End Luar Dikti

                        //Dikti
                        $strXML .="<dataset seriesName='D' color='DBDC25' anchorBorderColor='DBDC25' anchorBgColor='DBDC25'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Dikti'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .="</dataset>";
                        //End Dikti

                        //Luar Negeri
                        $strXML .="<dataset seriesName='E' color='C0C0C0' anchorBorderColor='C0C0C0' anchorBgColor='C0C0C0'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Luar Negeri'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .="</dataset>";
                        //End Luar Negeri

                        $strXML .="<styles>
                                <definition>
                                <style name='CaptionFont' type='font' size='12'/>
                                </definition>
                                <application>
                                <apply toObject='CAPTION' styles='CaptionFont' />
                                <apply toObject='SUBCAPTION' styles='CaptionFont' />
                                </application>
                                </styles>

                                </chart>";

                           //Create the chart - Column 3D Chart with data from strXML variable using dataXML method
                           echo renderChartHTML("../../../swf/Charts/MSLine.swf", "", $strXML, "myNext", 450, 300, false);
                           //End Distribusi Nilai S1
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php
                        //Distribusi Nilai S2
                        $strXML  = "";
                        $strXML  .= "<chart caption='Distribusi Nilai Kuliah Program S2' subcaption='' lineThickness='1' showValues='0' formatNumberScale='0' anchorRadius='2'   divLineAlpha='20' divLineColor='CC3300' divLineIsDashed='1' showAlternateHGridColor='1' alternateHGridColor='CC3300' shadowAlpha='40' labelStep='1' numvdivlines='5' chartRightMargin='35' bgColor='FFFFFF,0099CC' bgAngle='270' bgAlpha='10,10'>";
                        $strXML  .= "<categories >";
                                for($i = 0; $i < 5; $i++ ){
                                    $tahun[$i] = $date + ($i+1);
                                    $strXML .= "<category label='" . $tahun[$i] . "' />";
                                }
                        $strXML .= "</categories>";

                        $strXML .= "<dataset seriesName='A' color='1D8BD1' anchorBorderColor='1D8BD1' anchorBgColor='1D8BD1'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Mandiri'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .= "</dataset>";
                        //End Mandiri

                        //Universitas
                        $strXML .="<dataset seriesName='B' color='F1683C' anchorBorderColor='F1683C' anchorBgColor='F1683C'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Universitas'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .= "</dataset>";
                        //End Universitas

                        //Luar Dikti
                        $strXML .= "<dataset seriesName='C' color='2AD62A' anchorBorderColor='2AD62A' anchorBgColor='2AD62A'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Luar Dikti'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .="</dataset>";
                        //End Luar Dikti

                        //Dikti
                        $strXML .="<dataset seriesName='D' color='DBDC25' anchorBorderColor='DBDC25' anchorBgColor='DBDC25'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Dikti'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .="</dataset>";
                        //End Dikti

                        //Luar Negeri
                        $strXML .="<dataset seriesName='E' color='C0C0C0' anchorBorderColor='C0C0C0' anchorBgColor='C0C0C0'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Luar Negeri'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .="</dataset>";
                        //End Luar Negeri

                        $strXML .="<styles>
                                <definition>
                                <style name='CaptionFont' type='font' size='12'/>
                                </definition>
                                <application>
                                <apply toObject='CAPTION' styles='CaptionFont' />
                                <apply toObject='SUBCAPTION' styles='CaptionFont' />
                                </application>
                                </styles>

                                </chart>";

                           //Create the chart - Column 3D Chart with data from strXML variable using dataXML method
                           echo renderChartHTML("../../../swf/Charts/MSLine.swf", "", $strXML, "myNext", 450, 300, false);
                           //End Distribusi Nilai S2
                        ?>
                    </td>
                    <td>

                        <?php
                        //Distribusi Nilai S3
                        $strXML  = "";
                        $strXML  .= "<chart caption='Distribusi Nilai Kuliah Program S3' subcaption='' lineThickness='1' showValues='0' formatNumberScale='0' anchorRadius='2'   divLineAlpha='20' divLineColor='CC3300' divLineIsDashed='1' showAlternateHGridColor='1' alternateHGridColor='CC3300' shadowAlpha='40' labelStep='1' numvdivlines='5' chartRightMargin='35' bgColor='FFFFFF,0099CC' bgAngle='270' bgAlpha='10,10'>";
                        $strXML  .= "<categories >";
                                for($i = 0; $i < 5; $i++ ){
                                    $tahun[$i] = $date + ($i+1);
                                    $strXML .= "<category label='" . $tahun[$i] . "' />";
                                }
                        $strXML .= "</categories>";

                        $strXML .= "<dataset seriesName='A' color='1D8BD1' anchorBorderColor='1D8BD1' anchorBgColor='1D8BD1'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Mandiri'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .= "</dataset>";
                        //End Mandiri

                        //Universitas
                        $strXML .="<dataset seriesName='B' color='F1683C' anchorBorderColor='F1683C' anchorBgColor='F1683C'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Universitas'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .= "</dataset>";
                        //End Universitas

                        //Luar Dikti
                        $strXML .= "<dataset seriesName='C' color='2AD62A' anchorBorderColor='2AD62A' anchorBgColor='2AD62A'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Luar Dikti'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .="</dataset>";
                        //End Luar Dikti

                        //Dikti
                        $strXML .="<dataset seriesName='D' color='DBDC25' anchorBorderColor='DBDC25' anchorBgColor='DBDC25'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Dikti'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .="</dataset>";
                        //End Dikti

                        //Luar Negeri
                        $strXML .="<dataset seriesName='E' color='C0C0C0' anchorBorderColor='C0C0C0' anchorBgColor='C0C0C0'>";
                        $stid = oci_parse($conn, "SELECT SUM(dana_penelitian)
                                                  FROM penelitian
                                                  WHERE sumber_dana_penelitian = 'Luar Negeri'
                                                  GROUP BY thn_penelitian, sumber_dana_penelitian
                                                  ORDER BY thn_penelitian");
                        if (!$stid) {
                            $e = oci_error($conn);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Perform the logic of the query
                        $r = oci_execute($stid);
                        if (!$r) {
                            $e = oci_error($stid);
                            trigger_error(htmlentities($e['message'], ENT_QUOTES),
                        E_USER_ERROR);
                        }

                        // Fetch the results of the query
                        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
                            foreach ($row as $item) {
                                //$item = (string)$item;
                                $strXML .= "<set value='" . $item . "' />";
                            }
                        }
                        $strXML .="</dataset>";
                        //End Luar Negeri

                        $strXML .="<styles>
                                <definition>
                                <style name='CaptionFont' type='font' size='12'/>
                                </definition>
                                <application>
                                <apply toObject='CAPTION' styles='CaptionFont' />
                                <apply toObject='SUBCAPTION' styles='CaptionFont' />
                                </application>
                                </styles>

                                </chart>";

                           //Create the chart - Column 3D Chart with data from strXML variable using dataXML method
                           echo renderChartHTML("../../../swf/Charts/MSLine.swf", "", $strXML, "myNext", 450, 300, false);
                           //End Distribusi Nilai S3
                        ?>

                    </td>
                </tr>
            </table>
        </div>

    </div>

</div>
    </body>
</html>
