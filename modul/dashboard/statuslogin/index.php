<?php
include('../../../config.php');

$ten_minutes_ago = time() - 300;

$session_set = $db->QueryToArray("
    SELECT NM_ROLE, JUMLAH FROM \"ROLE\" R
    RIGHT JOIN (
      SELECT ID_ROLE, COUNT(ID_PENGGUNA) AS JUMLAH FROM SESSION_PENGGUNA
      WHERE WAKTU_SESSION >= {$ten_minutes_ago}
      GROUP BY ID_ROLE) T ON T.ID_ROLE = R.ID_ROLE");

$fakultas_set = $db->QueryToArray("
    SELECT F.NM_FAKULTAS, COUNT(ID_MHS) AS JUMLAH FROM MAHASISWA M
    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
    JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
    WHERE M.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM SESSION_PENGGUNA WHERE WAKTU_SESSION >= {$ten_minutes_ago})
    GROUP BY PS.ID_FAKULTAS, F.NM_FAKULTAS");
	
$smarty->assign('ten_minute_time', date('H:i:s',$ten_minutes_ago).' - '.date('H:i:s'));

$net_stat = shell_exec('netstat -plan | grep :80 | wc -l');
$net_stat443 = shell_exec('netstat -plan | grep :443 | wc -l');
$w_stat80 = shell_exec("netstat -na|grep ':80'|awk '/tcp/ {print $6}'|sort|uniq -c");
$w_stat443 = shell_exec("netstat -na|grep ':443'|awk '/tcp/ {print $6}'|sort|uniq -c");

$smarty->assign('session_set', $session_set);
$smarty->assign('fakultas_set', $fakultas_set);
$smarty->assign('net_stat', $net_stat);
$smarty->assign('net_stat443', $net_stat443);
$smarty->assign('w_stat80', $w_stat80);
$smarty->assign('w_stat443', $w_stat443); 
$smarty->display('index.tpl');
?>