<html>
    <head>
        <title>Status Login Pengguna</title>
        <META HTTP-EQUIV="REFRESH" CONTENT="60">
        {literal}
        <style>
            #wrapAll { position: relative; width: auto; }
            #wrapBody { margin: auto; }
        </style>
        {/literal}
    </head>
    <body>
        <div id="wrapAll">
            <div id="wrapBody">
                <table border="1" cellspacing="0" cellpadding="3" style="width: 300px; margin-left: auto; margin-right: auto;">
                    <caption>Aktivitas 5 menit terakhir : {$ten_minute_time}</caption>
                    <tr>
                        <th style="witdh=250px; text-align: center">Role</th>
                        <th style="witdh=50px; text-align: center">Jumlah</th>
                    </tr>
                    {foreach $session_set as $s}
		    {$TOTALs = $TOTALs + $s.JUMLAH}
                        <tr>
                            <td style="witdh=250px; text-align: left">{$s.NM_ROLE}</td>
                            <td style="witdh=50px; text-align: center">{$s.JUMLAH}</td>
                        </tr>
                    {/foreach}
                    <tr>
                        <td style="witdh=250px; text-align: center"><b>Total</b></th>
                        <td style="witdh=50px; text-align: center">{$TOTALs}</th>
                    </tr>
                </table>
                <br/>
                <table border="1" cellspacing="0" cellpadding="3" style="width: 300px; margin-left: auto; margin-right: auto;">
                    <caption>Detail Mahasiswa Online</caption>
                    <tr>
                        <th>Fakultas</th>
                        <th>Jumlah</th>
                    </tr>
                    {foreach $fakultas_set as $f}
					{$TOTALf = $TOTALf + $f.JUMLAH}
                        <tr>
                            <td>{$f.NM_FAKULTAS}</td>
                            <td style="text-align: center">{$f.JUMLAH}</td>
                        </tr>
                    {/foreach}
                        <tr>
                            <td style="text-align: center"><b>Total</b></td>
                            <td style="text-align: center">{$TOTALf}</td>
                        </tr>
                </table>
				
		<br/>
		<table border="1" cellspacing="0" cellpadding="3" style="width: 300px; margin-left: auto; margin-right: auto;">
		
		    <caption>Koneksi Web Aktif</caption>
                    <tr>
						<td align="left"><pre>:80 = {$net_stat}</pre></td>
					</tr>
                    <tr>
						<td align="left"><pre>{$w_stat80}</pre></td>
					</tr>
                    <tr>
			<td align="left"><pre>:443 = {$net_stat443}</pre></td>
			</tr>
                    <tr>
						<td align="left"><pre>{$w_stat443}</pre></td>
					</tr>
		</table>
            </div>
        </div>
    </body>
</html>