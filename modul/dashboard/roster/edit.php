<?php
	
    include('../../../config.php');
?>
<html>

<head>
  <script src="../../../js/jquery-1.5.1.js"></script>
  <script type="text/javascript" src="jquery-1.2.3.min.js"></script>

		<script type="text/javascript">
		$(document).ready(function() {

			$().ajaxStart(function() {
				$('#loading').show();
				$('#result').hide();
			}).ajaxStop(function() {
				$('#loading').hide();
				$('#result').fadeIn('slow');
			});

			$('#myForm').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {
						$('#result').html(data);
					}
				})
				return false;
			});
		})
		</script>
		<style type="text/css">
		body, table, input, select, textarea { font: 12px/20px Verdana, sans-serif; }
		h4 { font-size: 18px; }
		input { padding: 3px; border: 1px solid #999; }
		td { padding: 5px; }
		#result { background-color: #FF6600; border: 1px; solid #215800; padding: 10px;}
</style>

</head>

<body>
<?php


	
	if($_GET){
		$fakultas = $user->ID_FAKULTAS;
		$id_fakultas =  get('fakultas');
		$id_semester = get('semester');
		$id_program_studi =  get('program_studi');
		$jadwal_kelas = get('jadwal_kelas');
		//$id_program_studi = $user->ID_PROGRAM_STUDI;


?>
	<?php

		$hari = array();
		$jam_mulai = array();
		$jam_selesai = array();
		$kd_mata_kuliah = array();
		$nm_mata_kuliah = array();
		$nama_kelas = array();
		$nm_pengguna = array();
		$ruangan = array();
		$tingkat_semester = array();
		$tahun_kurikulum = array();
		$jml_peserta = array();
		$jml_data = 0;


  
			$db->Query("
						  select distinct 
						  nm_jadwal_hari,
						  jj1,
						  jj2,
						  kd_mata_kuliah,
						  nm_mata_kuliah,
						  nama_kelas,
						  nm_pengguna,
						  nm_ruangan,
						  tingkat_semester,
						  tahun_kurikulum, 
						  count(id_mhs) 
						  from 
						  (select jhari.nm_jadwal_hari,
								  jjam_mulai.nm_jadwal_jam as jj1, 
								  jjam_selesai.nm_jadwal_jam as jj2, 
								  mk.kd_mata_kuliah, 
								  mk.nm_mata_kuliah, 
								  nkls.nama_kelas,
								  nm_pengguna,
								  ruang.nm_ruangan,  
										case
										  when kur_mk.tingkat_semester is not null then kur_mk.tingkat_semester
										  else 0
										end
										as tingkat_semester,
									kur_prodi.tahun_kurikulum,
									pemk.id_mhs,kmk.id_kelas_mk
									from AUCC.jadwal_kelas jkls
								  left join AUCC.jadwal_hari jhari on jhari.id_jadwal_hari = jkls.id_jadwal_hari
								  left join AUCC.jadwal_jam jjam on jjam.id_jadwal_jam = jkls.id_jadwal_jam
								  left join aucc.kelas_mk kmk on kmk.id_kelas_mk = jkls.id_kelas_mk
								  left JOIN AUCC.ruangan ruang on ruang.id_ruangan = jkls.id_ruangan
								  left join AUCC.kurikulum_mk kur_mk on kur_mk.id_kurikulum_mk = kmk.id_kurikulum_mk
								  left JOIN AUCC.mata_kuliah MK ON mk.id_mata_kuliah = kur_mk.id_mata_kuliah
								  left join AUCC.semester smt on smt.id_semester = kmk.id_semester
								  left join AUCC.program_studi ps on ps.id_program_studi = kmk.id_program_studi
								  left JOIN AUCC.jadwal_jam jjam_mulai on jjam_mulai.id_jadwal_jam = jkls.id_jadwal_jam_mulai
								  left JOIN AUCC.jadwal_jam jjam_selesai on jjam_selesai.id_jadwal_jam = jkls.id_jadwal_jam_selesai
							      left JOIN AUCC.kurikulum_prodi kur_prodi on kur_prodi.id_kurikulum_prodi = kur_mk.id_kurikulum_prodi
								  left join AUCC.nama_kelas nkls on nkls.id_nama_kelas = kmk.no_kelas_mk
								  left join AUCC.pengampu_mk pmk on kmk.id_kelas_mk=pmk.id_kelas_mk and pjmk_pengampu_mk=1
								  left join aucc.dosen on pmk.id_dosen=dosen.id_dosen
								  left join aucc.pengguna on dosen.id_pengguna=pengguna.id_pengguna
								  left join aucc.pengambilan_mk pemk on kmk.id_kelas_mk=pemk.id_kelas_mk -- and pemk.id_semester=5
									where kmk.id_program_studi = '$id_program_studi'
									and kmk.id_semester = '$id_semester'
									and jkls.id_jadwal_kelas = '$jadwal_kelas'
								  order by jhari.id_jadwal_hari, jjam.id_jadwal_jam)
						  group by nm_jadwal_hari,jj1,jj2,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,nm_pengguna,nm_ruangan,
						  tingkat_semester,tahun_kurikulum,id_kelas_mk
						  order by nm_jadwal_hari desc, to_number(jj1)
			");


									$i = 0;
									while ($row = $db->FetchRow()){ 
											$hari[$i] = $row[0];
											$jam_mulai[$i] = $row[1];
											$jam_selesai[$i] = $row[2];
											$kd_mata_kuliah[$i] = $row[3];
											$nm_mata_kuliah[$i] = $row[4];
											$nama_kelas[$i] = $row[5];
											$nm_pengguna[$i] = $row[6];
											$ruangan[$i] = $row[7];
											$tingkat_semester[$i] = $row[8];
											$tahun_kurikulum[$i] = $row[9];
											$jml_peserta[$i] = $row[10];
											$jml_data++;
											$i++;
									}

								
	?>

	
	<?php
		//hari
		$id_jadwal_harix = array();
		$nm_jadwal_harix = array();
		$jml_jadwal_harix = 0;

		//mata kuliah
		$id_mata_kuliahx = array();
		$nm_mata_kuliahx = array();
		$jml_mata_kuliahx = 0;
		
		//jam mulai
		$id_jadwal_jam_mulaix = array();
		$nm_jadwal_jam_mulaix = array();
		$jml_jadwal_jam_mulaix = 0;
		
		$id_jadwal_jam_selesaix = array();
		$nm_jadwal_jam_selesaix = array();
		$jml_jadwal_jam_selesaix = 0;
		
		//

		//query hari
		$db->Query("
					select id_jadwal_hari, nm_jadwal_hari from aucc.jadwal_hari order by id_jadwal_hari
		");
		$i = 0;
		while ($row = $db->FetchRow()){ 
				$id_jadwal_harix[$i] = $row[0];
				$nm_jadwal_harix[$i] = $row[1];
				$jml_jadwal_harix++;
				$i++;
		}
		
		$db->Query("
			select distinct jjam_mulai.id_jadwal_jam, jjam_mulai.nm_jadwal_jam
			from aucc.jadwal_kelas jkls 
			  --left join AUCC.ruangan r on jkls.id_ruangan = r.id_ruangan
			  left join aucc.kelas_mk kmk on jkls.id_kelas_mk = kmk.id_kelas_mk
			  left join aucc.program_studi ps on kmk.id_program_studi = ps.id_program_studi
			  --left join aucc.fakultas fak on ps.id_program_studi = fak.id_fakultas
			  left join AUCC.jadwal_jam jjam_mulai on jkls.id_jadwal_jam_mulai = jjam_mulai.id_jadwal_jam
			  --left join aucc.jadwal_jam jjam_selesai on jkls.id_jadwal_jam_selesai = jjam_selesai.id_jadwal_jam
			where kmk.id_program_studi = '$id_program_studi'
			order by to_number(jjam_mulai.nm_jadwal_jam)
		");
		$i = 0;
		while ($row = $db->FetchRow()){ 
				$id_jadwal_jam_mulaix[$i] = $row[0];
				$nm_jadwal_jam_mulaix[$i] = $row[1];
				$nm_jadwal_jam_selesaix[$i] = $row[1];
				$jml_jadwal_jam_mulaix++;
				$i++;
		}
		
		$db->Query("
			select distinct jjam_selesai.id_jadwal_jam, jjam_selesai.nm_jadwal_jam
			from aucc.jadwal_kelas jkls 
			  left join aucc.kelas_mk kmk on jkls.id_kelas_mk = kmk.id_kelas_mk
			  left join aucc.program_studi ps on kmk.id_program_studi = ps.id_program_studi
			  left join aucc.jadwal_jam jjam_selesai on jkls.id_jadwal_jam_selesai = jjam_selesai.id_jadwal_jam
			where kmk.id_program_studi = '$id_program_studi'
			order by to_number(jjam_selesai.nm_jadwal_jam)
		");
		$i = 0;
		while ($row = $db->FetchRow()){ 
				$id_jadwal_jam_selesaix[$i] = $row[0];
				$nm_jadwal_jam_selesaix[$i] = $row[1];
				$jml_jadwal_jam_selesaix++;
				$i++;
		}

	?>


<form method="post" id="myForm" name="myForm" action="roster_update.php" style="border-width:1px;border-color:#000;border-style:solid;">	
<table width="100%">
	<tr style="font-weight:bold;">

		<td>
			HARI
		</td>
		
		<td>
			JAM MULAI
		</td>
		
		<td>
			JAM SELESAI
		</td>
		
		<td>
			KODE MATA KULIAH
		</td>
		
		<td>
			MATA KULIAH
		</td>
		
		<td>
			KELAS
		</td>

		<td>
			PJMA
		</td>

		<td>
			RUANGAN
		</td>
		
		<td>
			TINGKAT SEMESTER
		</td>
		
		<td>
			TAHUN KURIKULUM
		</td>

		<td>
			JML PESERTA
		</td>
	</tr>


	<?php
		for($i=0; $i<$jml_data; $i++){
	?>

	<tr>
		<td>
			<select name="hari">
			<?php
				for($ihari=0;$ihari<$jml_jadwal_harix;$ihari++){
			?>
			<option value="<?php echo $id_jadwal_harix[$ihari]; ?>"><?php echo $nm_jadwal_harix[$ihari]; ?></option>
			<?php
				}
			?>
			<option selected value=""><?php echo $hari[$i]; ?></option>
			</select>
		</td>
		<td>
			<select name="jam_mulai">
			<?php
				for($ijam_mulai=0;$ijam_mulai<$jml_jadwal_jam_mulaix;$ijam_mulai++){
			?>
			<option value="<?php echo $id_jadwal_jam_mulaix[$ijam_mulai]; ?>"><?php echo $nm_jadwal_jam_mulaix[$ijam_mulai]; ?></option>
			<?php
				}
			?>
			<option selected value=""><?php echo $jam_mulai[$i]; ?></option>
			</select>
			
		</td>
		<td>
			<select name="jam_selesai">
			<?php
				for($ijam_selesai=0;$ijam_selesai<$jml_jadwal_jam_selesaix;$ijam_selesai++){
			?>
			<option value="<?php echo $id_jadwal_jam_selesaix[$ijam_selesai]; ?>"><?php echo $nm_jadwal_jam_selesaix[$ijam_selesai]; ?></option>
			<?php
				}
			?>
			<option selected value=""><?php echo $jam_selesai[$i]; ?></option>
			</select>
		</td>
		<td><?php echo $kd_mata_kuliah[$i]; ?></td>
		<td><?php echo $nm_mata_kuliah[$i]; ?></td>
		<td><?php echo $nama_kelas[$i]; ?></td>
		<td><?php echo $nm_pengguna[$i]; ?></td>
		<td><?php echo $ruangan[$i]; ?></td>
		<td>
			<input type="text" value="<?php echo $tingkat_semester[$i]; ?>" name="tingkat_semester" />
		</td>
		<td>
			<input type"text" value="<?php echo $tahun_kurikulum[$i]; ?>" name="tahun_kurikulum" />
		</td>
		<td>
			<input type="text" value="<?php echo $jml_peserta[$i]; ?>" name="jml_peserta" />
		</td>
	</tr>
			
	<?php
		}
	?>
</table>
<div style="float=right;">
	<input type="submit" value="Update" />
</div>
</form>

<div id="loading" style="display:none;"><img src="loading.gif" alt="loading..." /></div>
<div id="result" style="display:none;"></div>
</body>


<?php
	}
?>