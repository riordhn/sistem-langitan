<?php
	include('../../../config.php');
	ini_set('display_errors', 1);
	
	if($_POST){
		$id_fakultas =  post('fakultas');
		$id_semester = post('semester');
		if($user->ID_MHS){
			$id_program_studi = $user->ID_PROGRAM_STUDI;
		}
		else{
			$id_program_studi =  post('program_studi');
		}
?>
	<?php
		
		$id_hari = array();
		$hari = array();
		$jam_mulai = array();
		$id_jam_mulai = array();
			
		$id_jam_selesai = array();
		$jam_selesai = array();
		
		$id_mata_kuliah = array();
		$kd_mata_kuliah = array();
		$nm_mata_kuliah = array();
		// Penambahan Tampilan BEBAN SKS 28 Desember 2017
		$beban_kredit_mata_kuliah = array();
		
		$id_nama_kelas = array();
		$nama_kelas = array();
		
		$id_pengguna = array();
		$nm_pengguna = array();
		
		$id_ruangan = array();
		$ruangan = array();
		
		
		$tingkat_semester = array();
		$tahun_kurikulum = array();
		$jml_peserta = array();
		$jml_data = 0;
		$jadwal_kelas = array();
  
			$db->Query("SELECT distinct 
						  nm_jadwal_hari,
						  jj1,
						  jj2,
						  kd_mata_kuliah,
						  nm_mata_kuliah,
						  nama_kelas,
						  nm_pengguna,
						  nm_ruangan,
						  tingkat_semester,
						  kredit_semester,
						  count(id_mhs) 
						  from 
						  (select	  jhari.nm_jadwal_hari,
									  jjam.nm_jadwal_jam as jj1, 
									  jjam.nm_jadwal_jam as jj2, 
									  mk.kd_mata_kuliah, 
									  mk.nm_mata_kuliah,
									  mk.kredit_semester, 
									  nkls.nama_kelas,
									  nm_pengguna,
									  ruang.nm_ruangan,  
										case
										  when kur_mk.tingkat_semester is not null then kur_mk.tingkat_semester
										  else 0
										end
										as tingkat_semester,
									  pemk.id_mhs,kmk.id_kelas_mk
									from krs_prodi kprodi
									  left join kelas_mk kmk on kmk.id_kelas_mk = kprodi.id_kelas_mk
									  left join jadwal_kelas jkls on jkls.id_kelas_mk = kmk.id_kelas_mk
									  left join jadwal_hari jhari on jhari.id_jadwal_hari = jkls.id_jadwal_hari
									  left join jadwal_jam jjam on jjam.id_jadwal_jam = jkls.id_jadwal_jam
									  
									  left JOIN ruangan ruang on ruang.id_ruangan = jkls.id_ruangan
									  left join kurikulum_mk kur_mk on kur_mk.id_kurikulum_mk = kmk.id_kurikulum_mk
									  left JOIN mata_kuliah MK ON mk.id_mata_kuliah = kur_mk.id_mata_kuliah
									  left join semester smt on smt.id_semester = kmk.id_semester
									  left join program_studi ps on ps.id_program_studi = kmk.id_program_studi
										left join nama_kelas nkls on nkls.id_nama_kelas = kmk.no_kelas_mk
										left join pengampu_mk pmk on kmk.id_kelas_mk=pmk.id_kelas_mk and pjmk_pengampu_mk=1
										left join dosen on pmk.id_dosen=dosen.id_dosen
										left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
										left join pengambilan_mk pemk on kmk.id_kelas_mk=pemk.id_kelas_mk and pemk.id_semester=5
									where kprodi.id_program_studi = '$id_program_studi'
									and kprodi.id_semester = '$id_semester'
							order by jhari.id_jadwal_hari, jjam.id_jadwal_jam)
						  group by nm_jadwal_hari,jj1,jj2,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nama_kelas,nm_pengguna,nm_ruangan,
						  tingkat_semester,id_kelas_mk
						  order by nm_jadwal_hari desc");
			

									$i = 0;
									while ($row = $db->FetchRow()){ 
											/*$hari[$i] = $row[0];
											$jam_mulai[$i] = $row[1];
											$jam_selesai[$i] = $row[2];
											$kd_mata_kuliah[$i] = $row[3];
											$nm_mata_kuliah[$i] = $row[4];
											$nama_kelas[$i] = $row[5];
											$nm_pengguna[$i] = $row[6];
											$ruangan[$i] = $row[7];
											$tingkat_semester[$i] = $row[8];
											$tahun_kurikulum[$i] = $row[9];
											$jml_peserta[$i] = $row[10];
											$jadwal_kelas[$i] = $row[11];
											$jml_data++;
											$i++;
											*/
											$hari[$i] = $row[0];
											$jam_mulai[$i] = $row[1];
											$jam_selesai[$i] = $row[2];
											$kd_mata_kuliah[$i] = $row[3];
											$nm_mata_kuliah[$i] = $row[4];
											$nama_kelas[$i] = $row[5];
											$nm_pengguna[$i] = $row[6];
											$ruangan[$i] = $row[7];
											$tingkat_semester[$i] = $row[8];
											// ditutup sementara
											/*$tahun_kurikulum[$i] = $row[9];
											$jml_peserta[$i] = $row[10];
											$jadwal_kelas[$i] = $row[11];*/
											$beban_kredit_mata_kuliah[$i] = $row[9];
											$jml_data++;
											$i++;
									}

								
	?>

	
<table width="100%">
	<tr style="font-weight:bold;">
		<?php
			if($user->ID_JABATAN_PEGAWAI==5){
		?>
		<td>
			Edit
		</td>
		<?php
			}
		?>
		<td>
			HARI
		</td>
		
		<td>
			JAM MULAI
		</td>
		
		<td>
			JAM SELESAI
		</td>
		
		<td>
			KODE MATA KULIAH
		</td>
		
		<td>
			MATA KULIAH
		</td>

		<td>
			BEBAN SKS
		</td>
		
		<td>
			KELAS
		</td>

		<td>
			PJMA
		</td>

		<td>
			RUANGAN
		</td>
		
		<td>
			TINGKAT SEMESTER
		</td>
		
		<td>
			TAHUN KURIKULUM
		</td>

		<td>
			JML PESERTA
		</td>

		
	</tr>


	<?php
		for($i=0; $i<$jml_data; $i++){
	?>
	

		<tr>
			<?php
				if($user->ID_JABATAN_PEGAWAI==5){
			?>
			<td>
				<a href="edit.php?hari=<?php echo $id_jadwdal_hari[$i]; ?>&fakultas=<?php echo $_POST['fakultas']; ?>&program_studi=<?php echo $_POST['program_studi']; ?>&semester=<?php echo $_POST['semester']; ?>&jadwal_kelas=<?php echo $jadwal_kelas[$i]; ?>">Edit</a>
			</td>
			<?php
				}
			?>

			<td style="font-weight:bold;">
				<?php
					if($hari[$i]!=$hari[$i-1]){
						echo $hari[$i] . ' ';
					}
				?>
			</td>
			
			
				<?php
					if($jam_mulai[$i]!=$jam_mulai[$i-1]){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $jam_mulai[$i] . ' ';
						}
						else{
							echo '<td>';
							echo $jam_mulai[$i] . ' ';
						}
						
					}
					else if(($jam_mulai[$i] == 1 && $jam_mulai[$i-1] == 1) && ($hari[$i] != $hari[$i-1])){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo "1";
						}
						else{
							echo '<td>';
							echo $jam_mulai[$i] . ' ';
						}
						
					}
					else{
						echo '<td>';
					}
				?>
			</td>

				<?php
					if($jam_mulai[$i]!=$jam_mulai[$i-1]){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $jam_selesai[$i];
						}
						else{
							echo '<td>';
							echo $jam_selesai[$i];
						}
						
					}
					else if(($jam_mulai[$i] == 1 && $jam_mulai[$i-1] == 1) && ($hari[$i] != $hari[$i-1])){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $jam_selesai[$i];
						}
						else{
							echo '<td>';
							echo $jam_selesai[$i];
						}
						
					}
					else{
						echo '<td>';
						echo $jam_selesai[$i];
					}
				?>
			</td>

				<?php
					if($jam_mulai[$i]!=$jam_mulai[$i-1]){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $kd_mata_kuliah[$i];
						}
						else{
							echo '<td>';
							echo $kd_mata_kuliah[$i];
						}
						
					}
					else if(($jam_mulai[$i] == 1 && $jam_mulai[$i-1] == 1) && ($hari[$i] != $hari[$i-1])){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $kd_mata_kuliah[$i];
						}
						else{
							echo '<td>';
							echo $kd_mata_kuliah[$i];
						}
						
					}
					else{
						echo '<td>';
						echo $kd_mata_kuliah[$i];
					}
				?>
			</td>

				<?php
					if($jam_mulai[$i]!=$jam_mulai[$i-1]){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $nm_mata_kuliah[$i];
						}
						else{
							echo '<td>';
							echo $nm_mata_kuliah[$i];
						}
						
					}
					else if(($jam_mulai[$i] == 1 && $jam_mulai[$i-1] == 1) && ($hari[$i] != $hari[$i-1])){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $nm_mata_kuliah[$i];
						}
						else{
							echo '<td>';
							echo $nm_mata_kuliah[$i];
						}
						
					}
					else{
						echo '<td>';
						echo $nm_mata_kuliah[$i];
					}
				?>
				<!-- Penambahan Tampilan SKS 28 Desember 2017 -->
				<?php
					if($jam_mulai[$i]!=$jam_mulai[$i-1]){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $beban_kredit_mata_kuliah[$i];
						}
						else{
							echo '<td>';
							echo $beban_kredit_mata_kuliah[$i];
						}
						
					}
					else if(($jam_mulai[$i] == 1 && $jam_mulai[$i-1] == 1) && ($hari[$i] != $hari[$i-1])){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $beban_kredit_mata_kuliah[$i];
						}
						else{
							echo '<td>';
							echo $beban_kredit_mata_kuliah[$i];
						}
						
					}
					else{
						echo '<td>';
						echo $beban_kredit_mata_kuliah[$i];
					}
				?>
			</td>

				<?php
					if($jam_mulai[$i]!=$jam_mulai[$i-1]){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $nama_kelas[$i];
						}
						else{
							echo '<td>';
							echo $nama_kelas[$i];
						}
						
					}
					else if(($jam_mulai[$i] == 1 && $jam_mulai[$i-1] == 1) && ($hari[$i] != $hari[$i-1])){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $nama_kelas[$i];
						}
						else{
							echo '<td>';
							echo $nama_kelas[$i];
						}
						
					}
					else{
						echo '<td>';
						echo $nama_kelas[$i];
					}
				?>
			</td>

				<?php
					if($jam_mulai[$i]!=$jam_mulai[$i-1]){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $nm_pengguna[$i];
						}
						else{
							echo '<td>';
							echo $nm_pengguna[$i];
						}
						
					}
					else if(($jam_mulai[$i] == 1 && $jam_mulai[$i-1] == 1) && ($hari[$i] != $hari[$i-1])){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $nm_pengguna[$i];
						}
						else{
							echo '<td>';
							echo $nm_pengguna[$i];
						}
						
					}
					else{
						echo '<td>';
						echo $nm_pengguna[$i];
					}
				?>
			</td>

				<?php
					if($jam_mulai[$i]!=$jam_mulai[$i-1]){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $ruangan[$i];
						}
						else{
							echo '<td>';
							echo $ruangan[$i];
						}
						
					}
					else if(($jam_mulai[$i] == 1 && $jam_mulai[$i-1] == 1) && ($hari[$i] != $hari[$i-1])){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $ruangan[$i];
						}
						else{
							echo '<td>';
							echo $ruangan[$i];
						}
						
					}
					else{
						echo '<td>';
						echo $ruangan[$i];
					}
				?>
			</td>


				<?php
					if($jam_mulai[$i]!=$jam_mulai[$i-1]){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $tingkat_semester[$i];
						}
						else{
							echo '<td>';
							echo $tingkat_semester[$i];
						}
						
					}
					else if(($jam_mulai[$i] == 1 && $jam_mulai[$i-1] == 1) && ($hari[$i] != $hari[$i-1])){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $tingkat_semester[$i];
						}
						else{
							echo '<td>';
							echo $tingkat_semester[$i];
						}
						
					}
					else{
						echo '<td>';
						echo $tingkat_semester[$i];
					}
				?>
			</td>

				<?php
					if($jam_mulai[$i]!=$jam_mulai[$i-1]){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $tahun_kurikulum[$i];
						}
						else{
							echo '<td>';
							echo $tahun_kurikulum[$i];
						}
						
					}
					else if(($jam_mulai[$i] == 1 && $jam_mulai[$i-1] == 1) && ($hari[$i] != $hari[$i-1])){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $tahun_kurikulum[$i];
						}
						else{
							echo '<td>';
							echo $tahun_kurikulum[$i];
						}
						
					}
					else{
						echo '<td>';
						echo $tahun_kurikulum[$i];
					}
				?>
			</td>

				<?php
					if($jam_mulai[$i]!=$jam_mulai[$i-1]){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $jml_peserta[$i];
						}
						else{
							echo '<td>';
							echo $jml_peserta[$i];
						}
						
					}
					else if(($jam_mulai[$i] == 1 && $jam_mulai[$i-1] == 1) && ($hari[$i] != $hari[$i-1])){
						if($i != 0){

							echo '<td style="border-top-color:#000;border-top-width:1px;border-top-style:solid;">';
							echo $jml_peserta[$i];
						}
						else{
							echo '<td>';
							echo $jml_peserta[$i];
						}
						
					}
					else{
						echo '<td>';
						echo $jml_peserta[$i];
					}
				?>
				
			</td>



		</tr>
			
	<?php
		}
	?>
</table>


<?php
	}
?>