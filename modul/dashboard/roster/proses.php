<?php
//validasi
    include('../../../config.php');

if (trim(post('nim')) == '') {
	$error[] = '- NIM harus diisi';
}
if (trim(post('nama')) == '') {
	$error[] = '- Nama harus diisi';
}
if (trim(post('tempat_lahir')) == '') {
	$error[] = '- Tempat Lahir harus diisi';
}
//dan seterusnya

if (isset($error)) {
	echo '<b>Error</b>: <br />'.implode('<br />', $error);
} else {
	/*
	jika data mau dimasukkan ke database,
	maka perintah SQL INSERT bisa ditulis di sini
	*/

	$data = '';
	foreach ($_POST as $k => $v) {
		$data .= "$k : $v<br />";
	}

	echo '<b>Form berhasil disubmit. Berikut ini data anda:</b><br />';
	echo $data;
}
die();
?>