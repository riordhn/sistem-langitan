<?php 
    ob_start("ob_gzhandler");
?>
<?php
    //We've included ../Includes/FusionCharts.php, which contains functions
    //to help us easily embed the charts.
    require_once "../../../includes/dbconnect.php";
    require_once "../../../includes/FusionCharts.php";
    require_once "../../../config.php";
?>

<html>
    <head>
        <title>
            Peta Sebaran Mahasiswa
        </title>
<script language="JavaScript" src="../../../js/FusionCharts.js"></script>
    </head>
    <body>
            <div style="margin: auto; padding-top: 0; position: relative; width: 1024px; height: 765px; border-style: solid; border-width: 1px; border-color: #C0C0C0;">
                   <div id="peta" style="background-image: url(../../../img/dashboard/bg-4div.png); background-repeat: no-repeat; height: 100%; width: 100%; ">
                   
                        <div>
                            <center>
                                <span style="font-family:sans-serif; font-weight: bold; font-size: 36px; color: #FFF;">
                                    Monitoring Status Mahasiswa Aktif & Non Aktif
                                </span>
                            </center>
                        </div>  
                       
                       
        <div style="padding-top: 32px; padding-left: 50px; padding-right: 50px;  float: left;">
            <table width="920px;" height="610px;" border="0" cellpadding="0" cellspacing="0" style="background-color: #FFF;">
                <tr>
                    <td>
                        <div>
                            <span style="font-weight: bold; font-family: sans-serif;">
                                <center>Persentase Mahasiswa Aktif Kuliah</center>
                            </span>
                        </div>
                          <div id="chartMahasiswaAktif" align="center">
                           The chart will appear within this DIV. This text will be replaced by the chart.
                          </div>
                          <script type="text/javascript">
                             var myChart1 = new FusionCharts("../../../swf/Widgets/AngularGauge.swf", "myChart1Id", "450", "275", "0", "0");
                             myChart1.setDataURL("data/aktif_kuliah.php");
                             myChart1.render("chartMahasiswaAktif");
                          </script>
                    </td>
                    <td>
                        <div>
                            <span style="font-weight: bold; font-family: sans-serif;">
                                <center>Persentase Mahasiswa Non Aktif Kuliah</center>
                            </span>
                        </div>
                          <div id="chartMahasiswaNonAktif" align="center">
                           The chart will appear within this DIV. This text will be replaced by the chart.
                          </div>
                          <script type="text/javascript">
                             var myChartMahasiswaNonAktif = new FusionCharts("../../../swf/Widgets/AngularGauge.swf", "myChartMahasiswaNonAktif", "450", "275", "0", "0");
                             myChartMahasiswaNonAktif.setDataURL("data/non_aktif_kuliah.php");
                             myChartMahasiswaNonAktif.render("chartMahasiswaNonAktif");
                          </script>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <?php
                            $strXML ="";
                            $strXML .="<chart caption='Proporsi Mahasiswa Non Aktif Per Fakultas' xAxisName='Tahun' yAxisName='Jumlah' shownames='1' showvalues='0' decimals='2' numberPrefix='' formatNumber='0' formatNumberScale='0'>";

                            $stid = oci_parse($conn, "select fakultas.singkatan_fakultas, fakultas.singkatan_fakultas, count(fakultas.singkatan_fakultas) from mahasiswa, program_studi, fakultas, pengguna, sejarah_status
                                                      where mahasiswa.id_program_studi = program_studi.id_program_studi and program_studi.id_fakultas = fakultas.id_fakultas and mahasiswa.id_pengguna = pengguna.id_pengguna and pengguna.id_pengguna = sejarah_status.id_pengguna and sejarah_status.aktif_sejarah_status = 1
                                                      group by fakultas.nm_fakultas, fakultas.singkatan_fakultas");
                            oci_execute($stid);

                            while (($row = oci_fetch_array($stid, OCI_NUM))) {
                                $nm_fakultas = $row[1];
                                $jml = $row[2];

                                $strXML .="<set label='$nm_fakultas' value='$jml' />";
                            }
                            
                            $strXML .="</chart>";
                            echo renderChartHTML("../../../swf/Charts/Column3D.swf", "", $strXML, "myNext", 450, 300, false);
                        ?>
                    </td>
                    <td>
                        <?php
                            $strXML ="";
                            $strXML .="<chart caption='Proporsi Mahasiswa Non Aktif Per Fakultas' xAxisName='Tahun' yAxisName='Jumlah' shownames='1' showvalues='0' decimals='2' numberPrefix='' formatNumber='0' formatNumberScale='0'>";

                            $stid = oci_parse($conn, "select fakultas.nm_fakultas, fakultas.singkatan_fakultas, count(fakultas.nm_fakultas) from mahasiswa, program_studi, fakultas, pengguna, sejarah_status
                                                      where mahasiswa.id_program_studi = program_studi.id_program_studi and program_studi.id_fakultas = fakultas.id_fakultas and mahasiswa.id_pengguna = pengguna.id_pengguna and pengguna.id_pengguna = sejarah_status.id_pengguna and sejarah_status.aktif_sejarah_status = 0
                                                      group by fakultas.nm_fakultas, fakultas.singkatan_fakultas");
                            oci_execute($stid);

                            while (($row = oci_fetch_array($stid, OCI_NUM))) {

                                $strXML .="<set label='$row[1]' value='$row[2]' />";
                            }
                            
                            $strXML .="</chart>";
                            echo renderChartHTML("../../../swf/Charts/Column3D.swf", "", $strXML, "myNext", 450, 300, false);
                        ?>
                    </td>
                </tr>
            </table>
        </div>   
            

    </body>
</html>


