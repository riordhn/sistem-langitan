<?php
    //We've included ../Includes/FusionCharts.php, which contains functions
    //to help us easily embed the charts.
    require_once "../../../../includes/dbconnect.php";
    require_once "../../../../config.php";
?>
<?php
    $stid = oci_parse($conn, "select count(id_mhs) from mahasiswa");
    oci_execute($stid);

    while (($row = oci_fetch_array($stid, OCI_NUM))) {
            $total_mhs = $row[0];
    }
?>
<?php
    $stid = oci_parse($conn, "select count(mahasiswa.id_mhs) from mahasiswa, program_studi, fakultas, pengguna, sejarah_status
                              where mahasiswa.id_program_studi = program_studi.id_program_studi 
                              and program_studi.id_fakultas = fakultas.id_fakultas 
                              and mahasiswa.id_pengguna = pengguna.id_pengguna 
                              and pengguna.id_pengguna = sejarah_status.id_pengguna 
                              and sejarah_status.aktif_sejarah_status = 0
                              ");
    oci_execute($stid);

    while (($row = oci_fetch_array($stid, OCI_NUM))) {
           $total_mhs_aktif = $row[0];
    }
?>
<?php
$persentase_mhs_aktif = number_format($total_mhs_aktif/$total_mhs*100, 0);
?>
<?php

    echo "<chart lowerLimit='0' upperLimit='100' lowerLimitDisplay='Baik' upperLimitDisplay='Buruk' gaugeStartAngle='180' gaugeEndAngle='0' palette='1' numberSuffix='%' tickValueDistance='20' showValue='1'>
    <colorRange>
    <color minValue='0' maxValue='10' code='8BBA00'/>
    <color minValue='10' maxValue='25' code='F6BD0F'/>
    <color minValue='25' maxValue='100' code='FF654F'/>
    </colorRange>
    <dials>
    <dial value='$persentase_mhs_aktif' rearExtension='10'/>
    </dials>
    </chart>";

?>
