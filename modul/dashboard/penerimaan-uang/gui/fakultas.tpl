<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header"><div id="judul_header"></div></div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">Penerimaan Fakultas Universitas Airlangga</h1>
<div id="content">
{foreach $fakultas_set as $f}
{if !empty($f.program_studi_set)}
<h2 style="font-family:'Trebuchet MS'" id="{$f.ID_FAKULTAS}">Fakultas {$f.NM_FAKULTAS}</h2>
<table id="rounded-corner">
    <thead>
    	<tr>
            <th scope="col" class="rounded-company">Penerimaan</th>
            <th scope="col" class="rounded-q1" style="text-align: right; width: 150px;">SOP</th>
            <th scope="col" class="rounded-q4" style="text-align: right; width: 150px;">SP3</th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="2" class="rounded-foot-left"><em>&nbsp;</em></td>
            <td class="rounded-foot-right">&nbsp;</td>
        </tr>
    </tfoot>
    <tbody>
        {$sop = 0}{$sp3 = 0}
        {foreach $f.program_studi_set as $ps}
        <tr>
            <td>{$ps.NM_JENJANG} - {$ps.NM_PROGRAM_STUDI}</td>
            <td style="text-align: right">Rp {number_format($ps.SOP, 0, ",", ".")}</td>
            <td style="text-align: right">Rp {number_format($ps.SP3, 0, ",", ".")}</td>
        </tr>
        {$sop = $sop + $ps.SOP}
        {$sp3 = $sp3 + $ps.SP3}
        {/foreach}
        <tr>
            <td><strong>TOTAL</strong></td>
            <td style="text-align: right"><strong>Rp {number_format($sop, 0, ",", ".")}</strong></td>
            <td style="text-align: right"><strong>Rp {number_format($sp3, 0, ",", ".")}</strong></td>
        </tr>
    </tbody>
</table>
<br/>
{/if}
{/foreach}
<p align="center"><a class="button" href="index.php">Kembali ke dashboard</a></p>
</div>
</body>
</html>