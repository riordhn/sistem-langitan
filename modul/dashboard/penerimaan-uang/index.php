<?php
if (!in_array($_SERVER['REMOTE_ADDR'], array('210.57.212.66', '110.138.225.229', '120.162.141.95', '210.57.215.198'))) { echo "Anda tidak mempunyai akses. [{$_SERVER['REMOTE_ADDR']}]"; exit(); }
include('../../../config.php');

$mode = get('mode', 'index');

if ($mode == 'index')
{
    /**
    $jenjang_set = $db->QueryToArray("
        select j.id_jenjang, j.nm_jenjang, a.sop, b.sp3
        from jenjang j
        join (
          select j.id_jenjang, sum(pc.besar_biaya) as sop from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 47)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          group by j.id_jenjang) a on a.id_jenjang = j.id_jenjang
        join (
          select j.id_jenjang, sum(pc.besar_biaya) as sp3 from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 81)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          group by j.id_jenjang) b on b.id_jenjang = j.id_jenjang
        order by j.id_jenjang");
    */
    $jenjang_set = $db->QueryToArray("
        select a.n, 'SNMPTN' as jalur, a.sop, b.sp3 from (
          select '1' as n, sum(pc.besar_biaya) as sop from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 47)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (1)) a
        join (
          select '1' as n, sum(pc.besar_biaya) as sp3 from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 81)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (1)) b on b.n = a.n
        union
        select a.n, 'MANDIRI' as jalur, a.sop, b.sp3 from (
          select '2' as n, sum(pc.besar_biaya) as sop from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 47)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (2, 3, 6, 7)) a
        join (
          select '2' as n, sum(pc.besar_biaya) as sp3 from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 81)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (2, 3, 6, 7)) b on b.n = a.n
        union
        select a.n, 'D3 + Alih Jenis' as jalur, a.sop, b.sp3 from (
          select '3' as n, sum(pc.besar_biaya) as sop from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 47)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (4, 5)) a
        join (
          select '3' as n, sum(pc.besar_biaya) as sp3 from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 81)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (4, 5)) b on b.n = a.n
        union
        select a.n, 'S2' as jalur, a.sop, b.sp3 from (
          select '4' as n, sum(pc.besar_biaya) as sop from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 47)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (8, 10, 12, 14, 18, 19, 21, 22, 33)) a
        join (
          select '4' as n, sum(pc.besar_biaya) as sp3 from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 81)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (8, 10, 12, 14, 18, 19, 21, 22, 33)) b on b.n = a.n
        union
        select a.n, 'S3' as jalur, a.sop, b.sp3 from (
          select '5' as n, sum(pc.besar_biaya) as sop from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 47)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (9, 11, 13, 15, 34)) a
        join (
          select '5' as n, sum(pc.besar_biaya) as sp3 from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 81)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (9, 11, 13, 15, 34)) b on b.n = a.n
        union
        select a.n, 'Profesi' as jalur, a.sop, b.sp3 from (
          select '6' as n, sum(pc.besar_biaya) as sop from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 47)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (16, 20, 28, 30, 35)) a
        join (
          select '6' as n, sum(pc.besar_biaya) as sp3 from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 81)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (16, 20, 28, 30, 35)) b on b.n = a.n
        union
        select a.n, 'Spesialis' as jalur, a.sop, b.sp3 from (
          select '7' as n, sum(pc.besar_biaya) as sop from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 47)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (17, 29, 31)) a
        join (
          select '7' as n, sum(pc.besar_biaya) as sp3 from calon_mahasiswa_baru cm
          join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
          join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 81)
          join program_studi ps on ps.id_program_studi = cm.id_program_studi
          join jenjang j on j.id_jenjang = ps.id_jenjang
          join penerimaan pn on pn.id_penerimaan = cm.id_penerimaan
          where pn.id_penerimaan in (17, 29, 31)) b on b.n = a.n");
    $smarty->assign('jenjang_set', $jenjang_set);
}

if ($mode == 'fakultas')
{
    $kode = get('kode', 0);
    
    switch ($kode)
    {
        case 1: $kode = "select id_penerimaan from penerimaan where id_jenjang = 1 and id_jalur = 1 and tahun = 2011";
            break;
        case 2: $kode = "select id_penerimaan from penerimaan where id_jenjang = 1 and id_jalur in (3, 20) and tahun = 2011";
            break;
        case 3: $kode = "select id_penerimaan from penerimaan where id_jenjang in (1, 5) and id_jalur in (4, 5)";
            break;
        case 4: $kode = "select id_penerimaan from penerimaan where id_jenjang = 2 and tahun = 2011";
            break;
        case 5: $kode = "select id_penerimaan from penerimaan where id_jenjang = 3 and tahun = 2011";
            break;
        case 6: $kode = "select id_penerimaan from penerimaan where id_jenjang = 9 and tahun = 2011";
            break;
        case 7: $kode = "select id_penerimaan from penerimaan where id_jenjang = 10 and tahun = 2011";
            break;
    }
    
    $fakultas_set = $db->QueryToArray("select * from fakultas");
    
    for ($i = 0; $i < count($fakultas_set); $i++)
    {
        $fakultas_set[$i]['program_studi_set'] = $db->QueryToArray("
            select ps.id_program_studi, j.nm_jenjang, ps.nm_program_studi, a.sop, b.sp3
            from program_studi ps
            join jenjang j on (j.id_jenjang = ps.id_jenjang)
            join (
              select ps.id_program_studi, sum(pc.besar_biaya) as sop from calon_mahasiswa_baru cm
              join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
              join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 47)
              join program_studi ps on ps.id_program_studi = cm.id_program_studi
              where cm.id_penerimaan in ({$kode})
              group by ps.id_program_studi) a on a.id_program_studi = ps.id_program_studi
            join (
              select ps.id_program_studi, sum(pc.besar_biaya) as sp3 from calon_mahasiswa_baru cm
              join pembayaran_cmhs pc on (pc.id_c_mhs = cm.id_c_mhs and pc.tgl_bayar is not null)
              join detail_biaya db on (db.id_detail_biaya = pc.id_detail_biaya and db.id_biaya = 81)
              join program_studi ps on ps.id_program_studi = cm.id_program_studi
              where cm.id_penerimaan in ({$kode})
              group by ps.id_program_studi) b on b.id_program_studi = ps.id_program_studi
            where ps.id_fakultas = {$fakultas_set[$i]['ID_FAKULTAS']}");
    }
    
    $smarty->assign('fakultas_set', $fakultas_set);
}

$smarty->display("{$mode}.tpl");
?>
