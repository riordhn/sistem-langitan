<?php
include('../../../config.php');

	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Disposition: attachment;filename=syarat_prodi_{$_GET['id_jenjang']}.xls");
	header("Content-Transfer-Encoding: binary ");
	$id_fakultas = array();
	$nm_fakultas = array();
	$jml_fakultas = 0;
	
	$id_prodi = array();
	$nm_prodi = array();
	$nm_jenjang = array();
	$jml_prodi = 0;
	$prodi_syarat_umum = array();
	$prodi_syarat_khusus = array();
	$is_alih_jenis = array();
	


	$nm_fakultas_get = "";

	$nm_program_studi = "";

	$nm_program_studi = "";
	
		$get_jenjang = $_GET['id_jenjang'];
		$db->Query("select ps.id_program_studi, 
					ps.nm_program_studi, 
					j.nm_jenjang,
					prorat.prodi_syarat_umum,
					prorat.prodi_syarat_khusus,
					prorat.is_alih_jenis
					from prodi_syarat prorat
					left join program_studi ps on ps.id_program_studi = prorat.id_program_studi
					left join fakultas f on f.id_fakultas = ps.id_fakultas
					left join jenjang j on j.id_jenjang = ps.id_jenjang
					where j.id_jenjang = '" . $get_jenjang . "' 
					order by prorat.id_program_studi");

								$i = 0;
								while ($row = $db->FetchRow()){ 
									$id_prodi[$i] = $row[0];
									$nm_prodi[$i] = $row[1];
									$nm_jenjang[$i] = $row[2];
									$prodi_syarat_umum[$i] = $row[3];
									$prodi_syarat_khusus[$i] = $row[4];
									$is_alih_jenis[$i] = $row[5];
									$jml_prodi++;
									$i++;
								}
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Syarat Prodi</title>
</head>
<body>
<table border="1">
    <thead>
    	<tr>
            <th>Prodi</th>
            <th style="text-align: center">Syarat Umum</th>
            <th style="text-align: center">Syarat Khusus</th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="2" ><em>&nbsp;</em></td>
            <td >&nbsp;</td>
        </tr>
    </tfoot>
	<?php
		for($i=0;$i<$jml_prodi;$i++){
	?>
    <tbody>
        <tr>
            <td><?php echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i]; ?></td>
            <td><?php echo $prodi_syarat_umum[$i]; ?></td>
            <td><?php echo $prodi_syarat_khusus[$i]; ?></td>
        </tr>
    </tbody>
	<?php
		}
	?>
</table>
<br/>
</div>

</body>
</html>