<?php
	include('../../../config.php');
	$id_fakultas = array();
	$nm_fakultas = array();
	$jml_fakultas = 0;
	
	$id_prodi = array();
	$nm_prodi = array();
	$nm_jenjang = array();
	$jml_prodi = 0;
	$prodi_syarat_umum = array();
	$prodi_syarat_khusus = array();
	$is_alih_jenis = array();
	


	$nm_fakultas_get = "";

	$nm_program_studi = "";

	$nm_program_studi = "";
	
		$get_jenjang = $_GET['id_jenjang'];
		$db->Query("select ps.id_program_studi, 
					ps.nm_program_studi, 
					j.nm_jenjang,
					prorat.prodi_syarat_umum,
					prorat.prodi_syarat_khusus,
					prorat.is_alih_jenis
					from prodi_syarat prorat
					left join program_studi ps on ps.id_program_studi = prorat.id_program_studi
					left join fakultas f on f.id_fakultas = ps.id_fakultas
					left join jenjang j on j.id_jenjang = ps.id_jenjang
					where j.id_jenjang = '" . $get_jenjang . "' and prorat.id_program_studi not in (197, 47, 100)
					order by prorat.id_program_studi");

								$i = 0;
								while ($row = $db->FetchRow()){ 
									$id_prodi[$i] = $row[0];
									$nm_prodi[$i] = $row[1];
									$nm_jenjang[$i] = $row[2];
									$prodi_syarat_umum[$i] = $row[3];
									$prodi_syarat_khusus[$i] = $row[4];
									$is_alih_jenis[$i] = $row[5];
									$jml_prodi++;
									$i++;
								}
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
</head>
<body>
<div id="header">
<div id="judul_header"></div>
</div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">INFORMASI SYARAT PROGRAM STUDI</h1>
<div id="content">
<h2 style="font-family:'Trebuchet MS'"></h2>
<table id="rounded-corner" summary="">
    <thead>
    	<tr>
            <th scope="col" class="rounded-company">Prodi</th>
            <th scope="col" class="rounded-q1" style="text-align: center">Syarat Umum</th>
            <th scope="col" class="rounded-q2" style="text-align: center">Syarat Khusus</th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="2" class="rounded-foot-left"><em>&nbsp;</em></td>
            <td class="rounded-foot-right">&nbsp;</td>
        </tr>
    </tfoot>
	<?php
		for($i=0;$i<$jml_prodi;$i++){

		$key = array('Warga Indonesia', 'warga Negara Asing ', 'Warga Negara Indonesia', 'Lulusan SLTA', 'cacat tubuh atau ketunaan', 'Sarjana (S1)', 'Lulusan D3 atau S1', 'Lulusan S1', 'IPK S1 minimal 2,75', 'TOEFL', 'ELPT');
		$key_replace   = array('<strong style="color:GREEN;">Warga Indonesia</strong>', '<strong style="color:GREEN;">warga Negara Asing </strong>', '<strong style="color:GREEN;">Warga Negara Indonesia</strong>', '<strong style="color:GREEN;">Lulusan SLTA</strong>', '<strong style="color:GREEN;">cacat tubuh atau ketunaan</strong>', '<strong style="color:GREEN;">Sarjana (S1)</strong>', '<strong style="color:GREEN;">Lulusan D3 atau S1</strong>', '<strong style="color:GREEN;">Lulusan S1</strong>', '<strong style="color:GREEN;">IPK S1 minimal 2,75</strong>', '<strong style="color:GREEN;">TOEFL</strong>', '<strong style="color:GREEN;">ELPT</strong>');
		
		//$text    = 'Syaratnya Warga Indonesia yang baik';
		$prodi_syarat_umum[$i]  = str_replace($key, $key_replace, $prodi_syarat_umum[$i]);
		$prodi_syarat_khusus[$i]  = str_replace($key, $key_replace, $prodi_syarat_khusus[$i]);
		//echo $output;

	?>
    <tbody>
        <tr>
            <td><?php echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i]; ?></td>
            <td><?php echo $prodi_syarat_umum[$i]; ?></td>
            <td><?php echo $prodi_syarat_khusus[$i]; ?></td>
        </tr>
    </tbody>
	<?php
		}
	?>
</table>
<br/>
<a href="print_syarat_prodi.php?id_jenjang=<?php echo $_GET['id_jenjang']; ?>"><center>Download Excel</center></a>
<p align="center"><a class="button" href="jenjang.php">Kembali ke daftar jenjang</a></p>
</div>


</body>
</html>