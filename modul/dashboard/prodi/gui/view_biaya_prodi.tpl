<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header">
<div id="judul_header"></div>
</div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">INFORMASI BIAYA PROGRAM STUDI</h1>
<div id="content">
    <p align="center">
    <a href="print-biaya-prodi.php?id_jenjang={$smarty.get.id_jenjang}">Download Excel</a><br/>
    </p>
<table id="rounded-corner">
    <thead>
    	<tr>
            <th scope="col" class="rounded-company" rowspan="2">Prodi</th>
            <th scope="col" colspan="3" style="text-align: center">Sekarang</th>
            <th scope="col" class="rounded-q4" colspan="3" style="text-align: center">Usulan</th>
        </tr>
        <tr>
            <th scope="col" class="rounded-q1" style="text-align: center">SOP</th>
            <th scope="col" class="rounded-q2" style="text-align: center">SP3</th>
            <th scope="col" class="rounded-q3" style="text-align: center">Matrikulasi</th>
            <th scope="col" class="rounded-q1" style="text-align: center">SOP</th>
            <th scope="col" class="rounded-q2" style="text-align: center">SP3</th>
            <th scope="col" class="rounded-q3" style="text-align: center">Matrikulasi</th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="6" class="rounded-foot-left"><em>&nbsp;</em></td>
            <td class="rounded-foot-right">&nbsp;</td>
        </tr>
    </tfoot>
    <tbody>
        {foreach $pb_set as $pb}
    	<tr>
            <td>{$pb.NM_PROGRAM_STUDI} {if $pb.IS_ALIH_JENIS}[Alih Jenis]{/if}</td>
            <td style="text-align: right">{$pb.SOP}</td>
            <td style="text-align: right">{$pb.SP3}</td>
            <td style="text-align: right">{$pb.MATRIKULASI}</td>
            <td style="text-align: right">{$pb.SOP_USULAN}</td>
            <td style="text-align: right">{$pb.SP3_USULAN}</td>
            <td style="text-align: right">{$pb.MATRIKULASI_USULAN}</td>
        </tr>
        {/foreach}
    </tbody>
</table>
    <p align="center">
        <a href="print-biaya-prodi.php?id_jenjang={$smarty.get.id_jenjang}">Download Excel</a><br/><br/>
        <a class="button" href="jenjang.php">Kembali ke daftar jenjang</a>
    </p>
<br/>
</div>

</body>
</html>