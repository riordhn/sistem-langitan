<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header">
<div id="judul_header"></div>
</div>
<div id="content">
<a class="button" href="./#{$pb.ID_FAKULTAS}">Kembali ke dashboard</a>
<h2 style="font-family:'Trebuchet MS'">Program Studi {$pb.NM_JENJANG} - {$pb.NM_PROGRAM_STUDI} {if $pb.IS_ALIH_JENIS==1} [Alih Jenis] {/if}</h2>


<table id="hor-minimalist-b">
    <thead>
    	<tr>
            <th scope="col" style="width: 250px">Info</th>
            <th scope="col">Isian</th>
        </tr>
    </thead>
    <tbody>
    	<tr>
            <td><strong>Nama Program Studi</strong></td>
            <td>{$pb.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Sekarang</strong></td>
        </tr>
		
		{if $pb.IS_MANDIRI==1}
        <tr>
            <td>SOP</td>
            <td>{$pb.SOP}</td>
        </tr>
        <tr>
            <td>SP3</td>
            <td>{$pb.SP3}</td>
        </tr>
        <tr>
            <td>Matrikulasi</td>
            <td>{$pb.MATRIKULASI}</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Usulan</strong></td>
        </tr>
        <tr>
            <td>SOP</td>
            <td>{$pb.SOP_USULAN}</td>
        </tr>
        <tr>
            <td>SP3</td>
            <td>{$pb.SP3_USULAN}</td>
        </tr>
        <tr>
            <td>Matrikulasi</td>
            <td>{$pb.MATRIKULASI_MATRIKULASI}</td>
        </tr>
		{else}
			<tr>
				<td>SOP</td>
				<td>{$pb.SOP}</td>
			</tr>
			<tr>
				<td>SP3a</td>
				<td>{$pb.SP3A}</td>
			</tr>
			<tr>
				<td>SP3b</td>
				<td>{$pb.SP3B}</td>
			</tr>
			<tr>
				<td>SP3c</td>
				<td>{$pb.SP3C}</td>
			</tr>
			<tr>
				<td colspan="2"><strong>Usulan</strong></td>
			</tr>
			<tr>
				<td>SOP</td>
				<td>{$pb.SOP_USULAN}</td>
			</tr>
			<tr>
				<td>SP3a</td>
				<td>{$pb.SP3A_USULAN}</td>
			</tr>	
			<tr>
				<td>SP3b</td>
				<td>{$pb.SP3B_USULAN}</td>
			</tr>
			<tr>
				<td>SP3c</td>
				<td>{$pb.SP3C_USULAN}</td>
			</tr>
		{/if}
    </tbody>
</table>
</div>
</body>
</html>