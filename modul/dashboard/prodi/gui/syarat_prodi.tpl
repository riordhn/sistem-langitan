<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
{literal}
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
{/literal}
</head>
<body>
<div id="header">
<div id="judul_header"></div>
</div>
<div id="content">
<a class="button" href="./#{$ps.ID_FAKULTAS}">Kembali ke dashboard</a>
<h2 style="font-family:'Trebuchet MS'">Program Studi {$ps.NM_JENJANG} - {$ps.NM_PROGRAM_STUDI} {if $ps.IS_ALIH_JENIS==1} [Alih Jenis] {/if}</h2>
<table id="hor-minimalist-b" summary="Employee Pay Sheet">
    <thead>
    	<tr>
            <th scope="col" style="width: 250px">Nama Syarat</th>
            <th scope="col">Syarat Program Studi</th>
        </tr>
    </thead>
    <tbody>
    	<tr>
            <td><strong>Syarat Umum Program Studi</strong></td>
            <td>{$ps.PRODI_SYARAT_UMUM}</td>
        </tr>
        <tr>
            <td><strong>Syarat Khusus Program Studi</strong></td>
            <td>{$ps.PRODI_SYARAT_KHUSUS}</td>
        </tr>
    </tbody>
</table>
</div>
</body>
</html>