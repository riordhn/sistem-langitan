<?php
	include('../../../config.php');
	$id_fakultas = array();
	$nm_fakultas = array();
	$jml_fakultas = 0;
	
	$id_prodi = array();
	$nm_prodi = array();
	$nm_jenjang = array();
	$snmptn = array();
	$mandiri = array();

	$semester_1 = array();
	$semester_2 = array();
	$semester1_usulan = array();
	$semester2_usulan = array();

	$total = array();

	$total_usulan2 = array();
	
	$daya_tampung_sk = array();
	$daya_tampung_nyata = array();
	$daya_tampung_usulan = array();
	$daya_tampung_apk = array();
	
	$total_sk = array();
	$snmptn_sk = array();
	$mandiri_sk = array();
	
	$total_real = array();
	$snmptn_sk = array();
	$mandiri_sk = array();
	
	$total_usulan = array();	
	$snmptn_usulan = array();
	$mandiri_usulan = array();
	$is_alih_jenis = array();
	
	$jml_prodi = 0;

	$nm_fakultas_get = "";

	$nm_program_studi = "";
	
		$get_jenjang = get('id_jenjang');
		$db->Query("select ps.id_program_studi, ps.nm_program_studi, j.nm_jenjang, prodt.snmptn_sk, prodt.mandiri_sk, prodt.snmptn_real, prodt.mandiri_real, prodt.snmptn_usulan, prodt.mandiri_usulan, prodt.daya_tampung_apk, prodt.daya_tampung_usulan, prodt.semester_1_usulan, prodt.semester_2_usulan, prodt.is_alih_jenis 
					from prodi_dayatampung prodt
					left join program_studi ps on ps.id_program_studi = prodt.id_program_studi
					left join fakultas f on f.id_fakultas = ps.id_fakultas
					left join jenjang j on j.id_jenjang = ps.id_jenjang
					where j.id_jenjang = '" . $get_jenjang . "' and prodt.id_program_studi not in (197, 47, 100) 
					order by prodt.id_program_studi");

								$i = 0;
								while ($row = $db->FetchRow()){ 
									$id_prodi[$i] = $row[0];
									$nm_prodi[$i] = $row[1];
									$nm_jenjang[$i] = $row[2];
									$snmptn_sk[$i] = $row[3];
									$mandiri_sk[$i] = $row[4];
									$snmptn_real[$i] = $row[5];
									$mandiri_real[$i] = $row[6];
									$snmptn_usulan[$i] = $row[7];
									$mandiri_usulan[$i] = $row[8];
									$daya_tampung_apk[$i] = $row[9];
									$daya_tampung_usulan[$i] = $row[10];
									$semester1_usulan[$i] = $row[11];
									$semester2_usulan[$i] = $row[12];
									$is_alih_jenis[$i] = $row[13];
									$total_usulan[$i] = $snmptn_usulan[$i] + $mandiri_usulan[$i];
									$total_usulan2[$i] = $semester1_usulan[$i] + $semester2_usulan[$i];
									$jml_prodi++;
									$i++;
								}
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informasi Spek Prodi</title>
<style type="text/css">
<!--
@import url("style.css");

.green { color: #080; }
.red { color: #f00; }
-->
</style>
</head>
<body>
<div id="header">
<div id="judul_header"></div>
</div>
<h1 style="text-align:center;color:#0e2d50;font-family:'Trebuchet MS'">INFORMASI DAYA TAMPUNG PROGRAM STUDI</h1>
<div id="content">
<h2 style="font-family:'Trebuchet MS'"></h2>

<?php
	if($_GET['id_jenjang']==1){
?>

<table id="rounded-corner" summary="">
    <thead>
		<tr>
			<th scope="col" class="rounded-q1" colspan="1" rowspan="2">Prodi</th>	
			<th scope="col" class="rounded-q2" colspan="3" rowspan="1">Diterima</th>	
			<th scope="col" class="rounded-q2" colspan="3" rowspan="1">Usulan</th>
		</tr>
    	<tr>
            <th scope="col" class="rounded-q1" style="text-align: center">SNMPTN</th>
            <th scope="col" class="rounded-q2" style="text-align: center">MANDIRI</th>
			<th scope="col" class="rounded-q2" style="text-align: center">TOTAL</th>
            <th scope="col" class="rounded-q1" style="text-align: center">SNMPTN</th>
            <th scope="col" class="rounded-q2" style="text-align: center">MANDIRI</th>
			<th scope="col" class="rounded-q2" style="text-align: center">TOTAL</th>		
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="2" class="rounded-q1"><em>&nbsp;</em></td>
			<td colspan="2" class="rounded-q1"><em>&nbsp;</em></td>
			<td colspan="2" class="rounded-q1"><em>&nbsp;</em></td>
			<td colspan="2" class="rounded-q1"><em>&nbsp;</em></td>
        </tr>
    </tfoot>
	<?php
		for($i=0;$i<$jml_prodi;$i++){
			if($nm_jenjang[$i]=='S1'){
	?>
    <tbody>
        <tr>
            <td><?php echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i]; ?></td>
			<?php
				$prodi_diterima_snmptn = 0;
				$db->Query("select count(ID_C_MHS) from CALON_MAHASISWA where ID_JALUR = 1 and ID_PROGRAM_STUDI = '" . $id_prodi[$i] . "' order by ID_C_MHS
				");
				while ($row = $db->FetchRow()){ 
					$prodi_diterima_snmptn = $row[0];
				}
			?>
            <td><?php echo $prodi_diterima_snmptn; ?></td>
			<?php
				$prodi_diterima_mandiri = 0;
				$db->Query("select count(ID_C_MHS) from CALON_MAHASISWA where ID_JALUR = 3 and ID_PROGRAM_STUDI = '" . $id_prodi[$i] . "' order by ID_C_MHS
				");
				while ($row = $db->FetchRow()){ 
					$prodi_diterima_mandiri = $row[0];
				}
			?>
            <td><?php echo $prodi_diterima_mandiri; ?></td>
			<td><?php $total_real = 0; $total_real = $prodi_diterima_snmptn + $prodi_diterima_mandiri; echo $total_real; ?></td>
			<td><?php echo $snmptn_usulan[$i]; ?></td>
			<td><?php echo $mandiri_usulan[$i]; ?></td>
			<td><?php echo $total_usulan[$i]; ?></td>
        </tr>
    </tbody>
	<?php
			}
		}
	?>
</table>

<?php
	}
	else if($_GET['id_jenjang']==9 || $_GET['id_jenjang']==10 ){
?>

<table id="rounded-corner" summary="">
    <thead>
		<tr>
			<th scope="col" class="rounded-q1" colspan="1" rowspan="2">Prodi</th>	
			<th scope="col" class="rounded-q2" colspan="3" rowspan="1">Diterima</th>	
			<th scope="col" class="rounded-q2" colspan="3" rowspan="1">Usulan</th>
		</tr>
    	<tr>
            <th scope="col" class="rounded-q1" style="text-align: center">SEMESTER I</th>
            <th scope="col" class="rounded-q2" style="text-align: center">SEMESTER II</th>
			<th scope="col" class="rounded-q2" style="text-align: center">TOTAL</th>
            <th scope="col" class="rounded-q1" style="text-align: center">SEMESTER I</th>
            <th scope="col" class="rounded-q2" style="text-align: center">SEMESTER II</th>
			<th scope="col" class="rounded-q2" style="text-align: center">TOTAL</th>		
        </tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="2" class="rounded-foot-left"><em>&nbsp;</em></td>
			<td colspan="2" class="rounded-foot-left"><em>&nbsp;</em></td>
			<td colspan="2" class="rounded-foot-left"><em>&nbsp;</em></td>
			<td colspan="2" class="rounded-foot-left"><em>&nbsp;</em></td>
        </tr>
    </tfoot>
	<?php
		for($i=0;$i<$jml_prodi;$i++){
			if($nm_jenjang[$i]=='Spesialis' || $nm_jenjang[$i]=='Profesi'){
	?>
    <tbody>
        <tr>
            <td><?php echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i]; ?></td>
            <td><?php echo $snmptn_real[$i]; ?></td>
            <td><?php echo $mandiri_real[$i]; ?></td>
			<td><?php echo $total_real[$i]; ?></td>
			<td><?php echo $semester1_usulan[$i]; ?></td>
			<td><?php echo $semester2_usulan[$i]; ?></td>
			<td><?php echo $total_usulan2[$i]; ?></td>
        </tr>
    </tbody>
	<?php
			}
		}
	?>
</table>

<?php
	}
	else if($_GET['id_jenjang']==5 || $_GET['id_jenjang']==2 || $_GET['id_jenjang']==3 ){
?>
<table id="rounded-corner" summary="">
    <thead>
		<tr>
			<th scope="col" class="rounded-q1" >Prodi</th>	
			<th scope="col" class="rounded-q2" >Diterima</th>	
			<th scope="col" class="rounded-q2" >Usulan</th>
		</tr>
    </thead>
    <tfoot>
    	<tr>
            <td colspan="2" class="rounded-foot-left"><em>&nbsp;</em></td>
			<td colspan="2" class="rounded-foot-left"><em>&nbsp;</em></td>
        </tr>
    </tfoot>
	<?php
		for($i=0;$i<$jml_prodi;$i++){
			if($nm_jenjang[$i]=='D3' || $nm_jenjang[$i]=='S2' || $nm_jenjang=='S3'){
	?>
    <tbody>
        <tr>
            <td><?php echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i]; ?></td>
					<?php
						$prodi_diterima = 0;
						$db->Query("select count(*) from (
									select PCM.ID_C_MHS, sum(pcm.besar_biaya) from PEMBAYARAN_CMHS pcm
									left join DETAIL_BIAYA db on db.ID_DETAIL_BIAYA = PCM.ID_DETAIL_BIAYA
									left join BIAYA_KULIAH bk on bk.id_biaya_kuliah = DB.ID_BIAYA_KULIAH
									left join CALON_MAHASISWA cm on CM.ID_C_MHS = PCM.ID_C_MHS
									where bk.id_kelompok_biaya = 1 and CM.ID_PROGRAM_STUDI = '" . $id_prodi[$i] . "'
									group by PCM.ID_C_MHS)
						");
						while ($row = $db->FetchRow()){ 
							$prodi_diterima = $row[0];
						}
					?>
            <td><?php echo $prodi_diterima ?></td>
            <td><?php echo $daya_tampung_usulan[$i]; ?></td>
        </tr>
    </tbody>
	<?php
			}
		}
	?>
</table>
<?php
	}
?>
<br/>
<a href="print_daya_tampung.php?id_jenjang=<?php echo get('id_jenjang'); ?>"><center>Download Excel</center></a>
<p align="center"><a class="button" href="jenjang.php">Kembali ke daftar jenjang</a></p>
</div>

</body>
</html>