<?php 
    ob_start("ob_gzhandler");
?>
<?php
    //We've included ../Includes/FusionCharts.php, which contains functions
    //to help us easily embed the charts.
    include '../../../config.php';
    include '../../../includes/dbconnect.php';
    include '../../../includes/FusionCharts.php';
    include 'proses/c_dashboard_ipk.php';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href=""/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>DASHBOARD IPK</title>

    <link type="text/css" href="../menu.css" rel="stylesheet" />


    <script type="text/javascript" src="../menu.js"></script>
    <script type="text/javascript" src="../js/jquery.js"></script>
	
<style type="text/css">
<!--
body {
	background-image: url();
}
-->
</style></head>
<body>

<style type="text/css">
* { margin:0;
    padding:0;
}
html { background:#444; }
body {
    margin:0px auto;
    width:800px;
    height:auto;
    background:#FFF;
    overflow:auto;
}
div#menu {
    top:0px;
    left:0px;
    width:100%;
		background:transparent url(../images/header_bg.gif) repeat-x 0 0;  
}
div#copyright { display: none; }

span.footer{
	font-size:12px;
	font-family:sans-serif;
}
</style>

<!--
<div style="background-image:url(../images/header.jpg); height:200px;"></div>
-->

<div style="height:auto">

<div id="menu">
    <ul class="menu">
        <li><a href="index.php"><span>Kedokteran</span></a>
        </li>
		
		        
        <li><a href="index.php?#transaksi"><span>Ked. Gigi</span></a>
        </li>
        
        <li><a href="#" class="parent"><span>Ekonomi</span></a>
        </li>
      
        <li class="last" ><a href="index.php?#contact" id="contact"><span>Contacts</span></a></li>
		<li class="last" ><a href="index.php?menu=comments" id="comments"><span>Comments</span></a></li>
		<li class="last" ><a href="index.php?#about-us" id="contact"><span>About Us</span></a></li>
    </ul>
</div>
<hr>


<div id="pageContent">
    

<center>
    
    <div style="background-color: #B3D8FF; width: 650px; min-height: 1000px; padding: 50px; padding-top: 20px;">
        
        <div style="font-family: sans-serif; font-weight: bold; font-size: 18px;">
            Grafik IPK Mahasiswa Universitas Airlangga Tingkat Universitas
        </div>
        <br />

<?php // IPK D3


$strXML = "";
$strXML .= "<chart caption='Persentase IPK mahasiswa D3 tiga tahun terakhir' xAxisName='Tahun' yAxisName='Jumlah Mahasiswa' shownames='1' showvalues='0' decimals='2' numberPrefix='' formatNumber='0' formatNumberScale='0'>";

$strXML .= "<categories >";
        for($i = 0; $i < 3; $i++ ){
            $thn[$i] = $date + ($i+1);
            $strXML .= "<category label='" . $thn[$i] . "' />";
        }
$strXML .= "</categories>";

//2.5>IPK>2
$strXML .="<dataset seriesName='2 s/d 2.5' color='AFD8F8' showValues='0'>";

// Fetch the results of the query

    for($i=0;$i<3;$i++) {
            //$item = (string)$item;
            $strXML .= "<set value='" . $kol_1_jumlah[$i] . "' />";

    }

$strXML .= "</dataset>";
//End 2.5>IPK>2

//2.5<IPK<2.75
$strXML .="<dataset seriesName='2.5 s/d 2.75' color='8BBA00' showValues='0'>";

// Fetch the results of the query
;
    for($i=0;$i<3;$i++) {
            //$item = (string)$item;
            $strXML .= "<set value='" . $kol_2_jumlah[$i] . "' />";

    }

$strXML .= "</dataset>";
//End 2.5<IPK<2.75

//2.75<IPK<3.0
$strXML .="<dataset seriesName='2.75 s/d 3' color='F6BD0F' showValues='0'>";

// Fetch the results of the query
    //$i = 0;
    for($i=0;$i<3;$i++) {
            //$item = (string)$item;
            $strXML .= "<set value='" . $kol_3_jumlah[$i] . "' />";
            //$i++;
    }

$strXML .= "</dataset>";
//End 2.75<IPK<3.0

//3<IPK<3.5
$strXML .="<dataset seriesName='3 s/d 3.5' color='FF0000' showValues='0'>";

// Fetch the results of the query
    //$i = 0;
    for($i=0;$i<3;$i++) {
            //$item = (string)$item;
            $strXML .= "<set value='" . $kol_4_jumlah[$i] . "' />";
            //$i++;
    }

$strXML .= "</dataset>";
//3 < IPK < 3.5


// > 3.5
$strXML .="<dataset seriesName='> 3.5' color='FFFD0F' showValues='0'>";

// Fetch the results of the query
    //$i = 0;
    for($i=0;$i<3;$i++) {
            //$item = (string)$item;
            $strXML .= "<set value='" . $kol_5_jumlah[$i] . "' />";
            //$i++;
    }

$strXML .= "</dataset>";
//End > 3.5

$strXML .="</chart>";
echo renderChartHTML("../../../swf/Charts/MSColumn3D.swf", "", $strXML, "myNext", 600, 300, false);
?>

    <br /><br />
    

    
    

<?php //IPK S1

  $jml_mhs_S1;
  
          $stid = oci_parse($conn, "select count(mhs_status.id_mhs_status)
                                    from mhs_status, semester, mahasiswa, program_studi, jenjang 
                                    where mhs_status.id_semester = semester.id_semester
                                    and mhs_status.id_mhs = mahasiswa.id_mhs
                                    and mahasiswa.id_program_studi = program_studi.id_program_studi
                                    and program_studi.id_jenjang = jenjang.id_jenjang
                                    and semester.thn_akademik_semester = 2008
                                    and jenjang.id_jenjang = 1
                                    group by semester.thn_akademik_semester
                                    order by semester.thn_akademik_semester");
        oci_execute($stid);

        while (($row = oci_fetch_array($stid, OCI_NUM))) {
              $jml_mhs_S1 = $row[0];
        }
  ?>
  
  <?php
  
  
  //kol1
        $kol_1_jumlah_S1 = array();
        $kol_1_persen_S1 = array();
        $i = 0;
        $stid = oci_parse($conn, "select count(mhs_status.id_mhs_status)
                                    from mhs_status, semester, mahasiswa, program_studi, jenjang 
                                    where mhs_status.id_semester = semester.id_semester
                                    and mhs_status.id_mhs = mahasiswa.id_mhs
                                    and mahasiswa.id_program_studi = program_studi.id_program_studi
                                    and program_studi.id_jenjang = jenjang.id_jenjang
                                    and semester.thn_akademik_semester > 2007
                                    and jenjang.id_jenjang = 1
                                    and mhs_status.ipk_mhs_status between 2 and 2.25
                                    group by semester.thn_akademik_semester
                                    order by semester.thn_akademik_semester");
        oci_execute($stid);

        while (($row = oci_fetch_array($stid, OCI_NUM))) {
              $kol_1_jumlah_S1[$i] = $row[0];
              $kol_1_persen_S1[$i] = $kol_1_jumlah_S1[$i]/$jml_mhs_S1;
              $kol_1_persen_S1[$i] = $kol_1_persen_S1[$i] * 100;
              $i++;
        }
        
 //kol 2
        $kol_2_jumlah_S1 = array();
        $kol_2_persen_S1 = array();
        $i = 0;
        $stid = oci_parse($conn, "select count(mhs_status.id_mhs_status)
                                    from mhs_status, semester, mahasiswa, program_studi, jenjang 
                                    where mhs_status.id_semester = semester.id_semester
                                    and mhs_status.id_mhs = mahasiswa.id_mhs
                                    and mahasiswa.id_program_studi = program_studi.id_program_studi
                                    and program_studi.id_jenjang = jenjang.id_jenjang
                                    and semester.thn_akademik_semester > 2007
                                    and jenjang.id_jenjang = 1
                                    and mhs_status.ipk_mhs_status between 2.5 and 2.75
                                    group by semester.thn_akademik_semester
                                    order by semester.thn_akademik_semester");
        oci_execute($stid);

        while (($row = oci_fetch_array($stid, OCI_NUM))) {
            $kol_2_jumlah_S1[$i] = $row[0];
            $kol_2_persen_S1[$i] = $kol_2_jumlah_S1[$i]/$jml_mhs_S1;
            $kol_2_persen_S1[$i] = $kol_2_persen_S1[$i] * 100;
            $i++;
        }
        
 //kol 3
        $kol_3_jumlah = array();
        $kol_3_persen = array();
        $i = 0;
        $stid = oci_parse($conn, "select count(mhs_status.id_mhs_status)
                                    from mhs_status, semester, mahasiswa, program_studi, jenjang 
                                    where mhs_status.id_semester = semester.id_semester
                                    and mhs_status.id_mhs = mahasiswa.id_mhs
                                    and mahasiswa.id_program_studi = program_studi.id_program_studi
                                    and program_studi.id_jenjang = jenjang.id_jenjang
                                    and semester.thn_akademik_semester > 2007
                                    and jenjang.id_jenjang = 1
                                    and mhs_status.ipk_mhs_status between 2.75 and 3
                                    group by semester.thn_akademik_semester
                                    order by semester.thn_akademik_semester");
        oci_execute($stid);

        while (($row = oci_fetch_array($stid, OCI_NUM))) {
            $kol_3_jumlah_S1[$i] = $row[0];
            $kol_3_persen_S1[$i] = $kol_3_jumlah_S1[$i]/$jml_mhs_S1;
            $kol_3_persen_S1[$i] = $kol_3_persen_S1[$i] * 100;
            $i++;
        }
        
 //kol 4
        $kol_4_jumlah_S1 = array();
        $kol_4_persen_S1 = array();
        $i = 0;
        $stid = oci_parse($conn, "select count(mhs_status.id_mhs_status)
                                    from mhs_status, semester, mahasiswa, program_studi, jenjang 
                                    where mhs_status.id_semester = semester.id_semester
                                    and mhs_status.id_mhs = mahasiswa.id_mhs
                                    and mahasiswa.id_program_studi = program_studi.id_program_studi
                                    and program_studi.id_jenjang = jenjang.id_jenjang
                                    and semester.thn_akademik_semester > 2007
                                    and jenjang.id_jenjang = 1
                                    and mhs_status.ipk_mhs_status between 3 and 3.5
                                    group by semester.thn_akademik_semester
                                    order by semester.thn_akademik_semester");
        oci_execute($stid);

        while (($row = oci_fetch_array($stid, OCI_NUM))) {
            $kol_4_jumlah_S1[$i] = $row[0];
            $kol_4_persen_S1[$i] = $kol_4_jumlah_S1[$i]/$jml_mhs_S1;
            $kol_4_persen_S1[$i] = $kol_4_persen_S1[$i] * 100;
            $i++;
        }
        
 //kol 5
        $kol_5_jumlah_S1 = array();
        $kol_5_persen_S1 = array();
        $i = 0;
        $stid = oci_parse($conn, "select count(mhs_status.id_mhs_status)
                                    from mhs_status, semester, mahasiswa, program_studi, jenjang 
                                    where mhs_status.id_semester = semester.id_semester
                                    and mhs_status.id_mhs = mahasiswa.id_mhs
                                    and mahasiswa.id_program_studi = program_studi.id_program_studi
                                    and program_studi.id_jenjang = jenjang.id_jenjang
                                    and semester.thn_akademik_semester > 2007
                                    and jenjang.id_jenjang = 1
                                    and mhs_status.ipk_mhs_status > 3.5
                                    group by semester.thn_akademik_semester
                                    order by semester.thn_akademik_semester");
        oci_execute($stid);

        while (($row = oci_fetch_array($stid, OCI_NUM))) {
            $kol_5_jumlah_S1[$i] = $row[0];
            $kol_5_persen_S1[$i] = $kol_5_jumlah_S1[$i]/$jml_mhs_S1;
            $kol_5_persen_S1[$i] = $kol_5_persen_S1[$i] * 100;
            $i++;
        }
        
  //kol 6
        $kol_6_rerata_S1 = array();
  $stid = oci_parse($conn, "select avg(mhs_status.ipk_mhs_status)
                            from mhs_status, semester, mahasiswa, program_studi, jenjang 
                            where mhs_status.id_semester = semester.id_semester
                            and mhs_status.id_mhs = mahasiswa.id_mhs
                            and mahasiswa.id_program_studi = program_studi.id_program_studi
                            and program_studi.id_jenjang = jenjang.id_jenjang
                            and semester.thn_akademik_semester > 2007
                            and jenjang.id_jenjang = 1
                            group by semester.thn_akademik_semester
                            order by semester.thn_akademik_semester");
if (!$stid) {
    $e = oci_error($conn);
    trigger_error(htmlentities($e['message'], ENT_QUOTES),
E_USER_ERROR);
}

// Perform the logic of the query
$r = oci_execute($stid);
if (!$r) {
    $e = oci_error($stid);
    trigger_error(htmlentities($e['message'], ENT_QUOTES),
E_USER_ERROR);
}


// Fetch the results of the query
$i = 0;
while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    foreach ($row as $item) {
        $item = number_format($item, 2);
        $kol_6_rerata_S1[$i] = $item;
        $i++;
        
    }
}        
                        
?>

<?php // IPK S1




$strXML = "";
$strXML .= "<chart caption='Persentase IPK mahasiswa S1 tiga tahun terakhir' xAxisName='Tahun' yAxisName='Jumlah Mahasiswa' shownames='1' showvalues='0' decimals='2' numberPrefix='' formatNumber='0' formatNumberScale='0'>";

$strXML .= "<categories >";
        for($i = 0; $i < 3; $i++ ){
            $thn[$i] = $date + ($i+1);
            $strXML .= "<category label='" . $thn[$i] . "' />";
        }
$strXML .= "</categories>";

//2.5>IPK>2
$strXML .="<dataset seriesName='2 s/d 2.5' color='AFD8F8' showValues='0'>";

// Fetch the results of the query

    for($i=0;$i<3;$i++) {
            //$item = (string)$item;
            $strXML .= "<set value='" . $kol_1_jumlah_S1[$i] . "' />";

    }

$strXML .= "</dataset>";
//End 2.5>IPK>2

//2.5<IPK<2.75
$strXML .="<dataset seriesName='2.5 s/d 2.75' color='8BBA00' showValues='0'>";

// Fetch the results of the query
;
    for($i=0;$i<3;$i++) {
            //$item = (string)$item;
            $strXML .= "<set value='" . $kol_2_jumlah_S1[$i] . "' />";

    }

$strXML .= "</dataset>";
//End 2.5<IPK<2.75

//2.75<IPK<3.0
$strXML .="<dataset seriesName='2.75 s/d 3' color='F6BD0F' showValues='0'>";

// Fetch the results of the query
    //$i = 0;
    for($i=0;$i<3;$i++) {
            //$item = (string)$item;
            $strXML .= "<set value='" . $kol_3_jumlah_S1[$i] . "' />";
            //$i++;
    }

$strXML .= "</dataset>";
//End 2.75<IPK<3.0

//3<IPK<3.5
$strXML .="<dataset seriesName='3 s/d 3.5' color='FF0000' showValues='0'>";

// Fetch the results of the query
    //$i = 0;
    for($i=0;$i<3;$i++) {
            //$item = (string)$item;
            $strXML .= "<set value='" . $kol_4_jumlah_S1[$i] . "' />";
            //$i++;
    }

$strXML .= "</dataset>";
//3 < IPK < 3.5


// > 3.5
$strXML .="<dataset seriesName='> 3.5' color='FFFD0F' showValues='0'>";

// Fetch the results of the query
    //$i = 0;
    for($i=0;$i<3;$i++) {
            //$item = (string)$item;
            $strXML .= "<set value='" . $kol_5_jumlah_S1[$i] . "' />";
            //$i++;
    }

$strXML .= "</dataset>";
//End > 3.5

$strXML .="</chart>";
echo renderChartHTML("../../../swf/Charts/MSColumn3D.swf", "", $strXML, "myNext", 600, 300, false);
?>

</div>


<div class="footer" style="background-color:#333; height:auto; font-family:sans-serif; color:#666;">
	<div style="font-size:24px; font-weight:bold; text-align:center">
		<span>&copy; 2011 - CYBER CAMPUS - UNIVERSITAS AIRLANGGA</span>
	</div>
    
</div>

</div>


</body>
</html>