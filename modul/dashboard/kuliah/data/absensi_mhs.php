<?php
    //We've included ../Includes/FusionCharts.php, which contains functions
    //to help us easily embed the charts.
    require_once "../../../../includes/dbconnect.php";
    require_once "../../../../config.php";
?>

<?php
    $stid = oci_parse($conn, "select count(presensi_mkmhs.id_mhs) from mahasiswa, pengguna, sejarah_status, program_studi, fakultas, presensi_mkmhs
    where mahasiswa.id_program_studi = program_studi.id_program_studi
    and program_studi.id_fakultas = fakultas.id_fakultas
    and mahasiswa.id_pengguna = pengguna.id_pengguna
    and pengguna.id_pengguna = sejarah_status.id_pengguna
    and mahasiswa.id_mhs = presensi_mkmhs.id_mhs
    and sejarah_status.aktif_sejarah_status = 1 
    ");
    oci_execute($stid);

    while (($row = oci_fetch_array($stid, OCI_NUM))) {
           $total_mhs_aktif = $row[0];
                            }
?>

<?php
    $stid = oci_parse($conn, "select count(mahasiswa.id_mhs) from mahasiswa, pengguna, sejarah_status, program_studi, fakultas
    where mahasiswa.id_program_studi = program_studi.id_program_studi
    and program_studi.id_fakultas = fakultas.id_fakultas
    and mahasiswa.id_pengguna = pengguna.id_pengguna
    and pengguna.id_pengguna = sejarah_status.id_pengguna
    and sejarah_status.aktif_sejarah_status = 1 
    ");
    oci_execute($stid);

    while (($row = oci_fetch_array($stid, OCI_NUM))) {
           $total_mhs = $row[0];
                            }
?>
<?php
    $_75 = 0.75*$total_mhs;
    $_90 = 0.9*$total_mhs;
    $_75 = number_format($_75, 0);
    $_90 = number_format($_90, 0);
?>
<?php

    echo "<chart lowerLimit='0' upperLimit='100' lowerLimitDisplay='Buruk' upperLimitDisplay='Baik' gaugeStartAngle='180' gaugeEndAngle='0' palette='1' numberSuffix='' tickValueDistance='20' showValue='1'>
    <colorRange>
    <color minValue='0' maxValue='$_75' code='FF654F'/>
    <color minValue='$_75' maxValue='$_90' code='F6BD0F'/>
    <color minValue='$_90' maxValue='$total_mhs' code='8BBA00'/>
    </colorRange>
    <dials>
    <dial value='$total_mhs_aktif' rearExtension='10'/>
    </dials>
    </chart>";

?>
