<?php 
    ob_start("ob_gzhandler");
?>
<?php
    //We've included ../Includes/FusionCharts.php, which contains functions
    //to help us easily embed the charts.
    require_once "../../../includes/dbconnect.php";
    require_once "../../../includes/FusionCharts.php";
    require_once "../../../config.php";
?>

<html>
    <head>
        <title>
            Profil Mahasiswa & Dosen Aktif Kuliah
        </title>
<script language="JavaScript" src="../../../js/FusionCharts.js"></script>
    </head>
    <body style="background-image: url(../images/bg.jpg); padding: 0; margin: 0;">

            <div style="margin: auto; padding-top: 0; position: relative; width: 1024px; height: 50px;">
                   <div id="peta" style="background-image: none; background-repeat: no-repeat; height: 100%; width: 100%; ">
                   
                        <div style="float:left; padding-top: 0px;">
                                <span style="font-family:sans-serif; font-weight: bold; font-size: 24px; color: #FFF;">
                                    Monitoring Kuliah Tingkat Universitas & Fakultas
                                </span>

                        </div>
                                <span style="font-family:sans-serif; font-size: 18px; color: #FFF;">
                                    <marquee direction="right">Monitoring Kuliah Tingkat Universitas & Fakultas</marquee>
                                </span>

                   </div>
            </div>
            <div style="margin: auto; padding-top: 0; position: relative; width: 1024px; min-height: 600px; border-style: solid; border-width: 1px; border-color: #C0C0C0;">
                   <div id="peta" style=" background-color: #FFF; background-image: none; background-repeat: no-repeat; height: 100%; width: 100%; ">
                   
                        <div style="background-color: #D0DFE8; height: 20px;">
                            &nbsp;
                        </div>  
                       
                       
        <div style="padding-top: 32px; padding-left: 50px; padding-right: 50px;  float: left;">
            <table width="920px;" height="610px;" border="0" cellpadding="0" cellspacing="0" style="background-color: #FFF;">
                <tr>
                    <td align="center">
                        <div>
                            <span style="font-weight: bold; font-family: sans-serif;">
                                Persentase Mahasiswa Aktif Kuliah
                            </span>
                        </div>
                          <div id="chartMahasiswaAktif" align="center">
                           The chart will appear within this DIV. This text will be replaced by the chart.
                          </div>
                          <script type="text/javascript">
                             var myChart1 = new FusionCharts("../../../swf/Widgets/AngularGauge.swf", "myChart1Id", "450", "275", "0", "0");
                             myChart1.setDataURL("data/absensi_mhs.php");
                             myChart1.render("chartMahasiswaAktif");
                          </script>
                    </td>
                    <td align="center">
                        <div>
                            <span style="font-weight: bold; font-family: sans-serif;">
                                Persentase Dosen Aktif Mengajar
                            </span>
                        </div>
                          <div id="chartDosenAktif" align="center">
                           The chart will appear within this DIV. This text will be replaced by the chart.
                          </div>
                          <script type="text/javascript">
                             var myChartDosenAktif = new FusionCharts("../../../swf/Widgets/AngularGauge.swf", "myChartDosenAktif", "450", "275", "0", "0");
                             myChartDosenAktif.setDataURL("data/absensi_dosen.php");
                             myChartDosenAktif.render("chartDosenAktif");
                          </script>
                    </td>
                </tr>
                
                <tr>
                    <td align="center">
                        <div>
                            <span style="font-weight: bold; font-family: sans-serif;">
                                Jumlah Mahasiswa Aktif Kuliah Per Fakultas
                            </span>
                        </div>
                        <?php

                            $strXML  = "";
                            $strXML .= "<chart palette='4' decimals='0' enableSmartLabels='1' enableRotation='0' bgColor='FFFFFF,FFFFFF' bgAlpha='100,100' bgRatio='100,100' bgAngle='360' showBorder='1' startingAngle='70' >";

                            $stid = oci_parse($conn, "select count(pengguna.nm_pengguna), fakultas.singkatan_fakultas from mahasiswa, pengguna, sejarah_status, program_studi, fakultas, presensi_mkmhs
                            where mahasiswa.id_program_studi = program_studi.id_program_studi
                            and program_studi.id_fakultas = fakultas.id_fakultas
                            and mahasiswa.id_pengguna = pengguna.id_pengguna
                            and pengguna.id_pengguna = sejarah_status.id_pengguna
                            and mahasiswa.id_mhs = presensi_mkmhs.id_mhs
                            and sejarah_status.aktif_sejarah_status = 1 
                            group by fakultas.singkatan_fakultas");
                            oci_execute($stid);

                            while (($row = oci_fetch_array($stid, OCI_NUM))) {

                                $strXML .="<set label='$row[1]' value='$row[0]' />";
                            }
                             
                            $strXML .="</chart>";

                            echo renderChartHTML("../../../swf/Charts/Pie2D.swf", "", $strXML, "myNext", 450, 275, false);

                        ?>
                    </td>
                    <td align="center">
                        <div>
                            <span style="font-weight: bold; font-family: sans-serif;">
                                Jumlah Dosen Aktif Kuliah Per Fakultas
                            </span>
                        </div>
                        <?php

                            $strXML  = "";
                            $strXML .= "<chart palette='4' decimals='0' enableSmartLabels='1' enableRotation='0' bgColor='FFFFFF,FFFFFF' bgAlpha='100,100' bgRatio='100,100' bgAngle='360' showBorder='1' startingAngle='70' >";
        

                            $stid = oci_parse($conn, "select fakultas.nm_fakultas, fakultas.singkatan_fakultas, count(fakultas.nm_fakultas) from mahasiswa, program_studi, fakultas, pengguna, sejarah_status
                                                      where mahasiswa.id_program_studi = program_studi.id_program_studi and program_studi.id_fakultas = fakultas.id_fakultas and mahasiswa.id_pengguna = pengguna.id_pengguna and pengguna.id_pengguna = sejarah_status.id_pengguna and sejarah_status.aktif_sejarah_status = 0
                                                      group by fakultas.nm_fakultas, fakultas.singkatan_fakultas");
                            oci_execute($stid);

                            while (($row = oci_fetch_array($stid, OCI_NUM))) {

                                $strXML .="<set label='$row[1]' value='$row[2]' />";
                            }
                             
                            $strXML .="</chart>";

                            echo renderChartHTML("../../../swf/Charts/Pie2D.swf", "", $strXML, "myNext", 450, 275, false);

                        ?>
                    </td>
                </tr>
            </table>
        </div>   
            

    </body>
</html>

