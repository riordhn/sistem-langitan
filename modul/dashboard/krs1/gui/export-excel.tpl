{if $data_set}
	{if $smarty.get.view_id == 1}
		<table border="1" cellspacing="0" cellpadding="2" class="view">
			<tr>
				<th>NIM</th>
				<th>Nama</th>
				<th>Kls Msk</th>
				<th>Tmpt Lahir</th>
				<th>Tgl Lahir</th>
				<th>JK</th>
				<th>Thn Msk</th>
				<th>Smt Msk</th>
				<th>B. Studi</th>
				<th>A. Kota</th>
				<th>Tgl Msk</th>
				<th>Tgl Lls</th>
				<th>Sts Mhs</th>
				<th>Sts</th>
			</tr>
			{foreach $data_set as $m}
			<tr>
				<td>{$m.NIM_MHS}</td>
				<td>{ucwords(strtolower($m.NM_PENGGUNA))}</td>
				<td>{$m.KELAS_MASUK}</td>
				<td>{$m.KOTA_LAHIR}</td>
				<td>{date('d-m-y', strtotime($m.TGL_LAHIR_PENGGUNA))}</td>
				<td>{$m.JK}</td>
				<td>{$m.THN_ANGKATAN_MHS}</td>
				<td>{$m.SEMESTER_MASUK}</td>
				<td>{$m.BATAS_STUDI}</td>
				<td>{$m.KOTA_ASAL}</td>
				<td>{$m.TGL_TERDAFTAR_MHS}</td>
				<td>{$m.TGL_LULUS_MHS}</td>
				<td>{$m.STATUS_MHS}</td>
				<td>{$m.STATUS_MASUK}</td>
			</tr>
			{/foreach}
		</table>
	{/if}
	
	{if $smarty.get.view_id == 2}
		<table border="1" cellspacing="0" cellpadding="2" class="view">
			<tr>
				<th>Smt</th>
				<th>NIM</th>
				<th>Nama</th>
				<th>SKS Diambil</th>
				<th>Nilai IPS</th>
				<th>SKS Total</th>
				<th>Nilai IPK</th>
			</tr>
			{foreach $data_set as $m}
			<tr>
				<td>{$m.NM_SEMESTER}</td>
				<td>{$m.NIM_MHS}</td>
				<td>{$m.NM_PENGGUNA}</td>
				<td>{$m.SKS_SEMESTER}</td>
				<td>{if $m.IPS_MHS_STATUS != ''}{$m.IPS_MHS_STATUS|string_format:"%.2f"}{/if}</td>
				<td>{$m.SKS_TOTAL_MHS_STATUS}</td>
				<td>{if $m.IPK_MHS_STATUS != ''}{$m.IPK_MHS_STATUS|string_format:"%.2f"}{/if}</td>
			</tr>
			{/foreach}
		</table>
	{/if}
	
	{if $smarty.get.view_id == 3}
		<table border="1" cellspacing="0" cellpadding="2" class="view">
			<tr>
				<th>Tahun Semester</th>
				<th>NIM</th>
				<th>Nama</th>
				<th>Mata Kuliah</th>
				<th>Nilai Huruf</th>
				<th>Nilai Angka</th>
				<th>Kelas</th>
			</tr>
			{foreach $data_set as $m}
			<tr>
				<td>{$m.NM_SEMESTER}</td>
				<td>{$m.NIM_MHS} </td>
				<td>{ucwords(strtolower($m.NM_PENGGUNA))} </td>
				<td>{$m.NM_MATA_KULIAH} </td>
				<td>{$m.NILAI_HURUF} </td>
				<td>{$m.NILAI_ANGKA} </td>
				<td>{$m.NO_KELAS_MK} </td>
			</tr>
			{/foreach}
		</table>
	{/if}
	
	{if $smarty.get.view_id == 5}
		{if isset($smarty.get.id_dosen)}{$id_dosen=$smarty.get.id_dosen}{else}{$id_dosen==0}{/if}
		<table border="1" cellspacing="0" cellpadding="2" class="view">
			<tr>
				<th>Tahun Semester</th>
				{if $id_dosen == 0}
				<th>NIDN</th>
				<th>NIP</th>
				<th>Nama</th>
				{/if}
				<th>Mata Kuliah</th>
				<th>Kelas</th>
				<th>Tatap Muka Total</th>
				<th>Tatap Muka Sesungguhnya</th>
				<th>Jadwal Hari</th>
				<th>Jam</th>
			</tr>
			{foreach $data_set as $d}
			<tr>
				<td>{$d.NM_SEMESTER}</td>
				{if $id_dosen == 0}
				<td>{$d.NIDN_DOSEN}</td>
				<td>{$d.NIP_DOSEN}</td>
				<td>{$d.NM_PENGGUNA}</td>
				{/if}
				<td>{$d.NM_MATA_KULIAH}</td>
				<td>{$d.NO_KELAS_MK}</td>
				<td>{$d.TATAP_MUKA_TOTAL}</td>
				<td>{$d.TATAP_MUKA_ASLI}</td>
				<td>{$d.NM_JADWAL_HARI}</td>
				<td>{$d.JAM_MULAI} - {$d.JAM_SELESAI}</td>
			</tr>
			{/foreach}
		</table>
	{/if}
	
	{if $smarty.get.view_id == 6}
		<table border="1" cellspacing="0" cellpadding="2" class="view">
			<tr>
				<th>Tahun Semester</th>
				<th>Kode MK</th>
				<th>Nama MK</th>
				<th>SKS</th>
				<th>Tatap Muka</th>
				<th>Praktikum</th>
				<th>Wajib / Pilihan</th>
				<th>NIDN Dosen</th>
				<th>Nama</th>
				<th>Jenjang Dosen</th>
				<th>Prodi Dosen</th>
				<th>Status Mata Kuliah</th>
				<th>Silabus</th>
				<th>Bahan Ajar</th>
				<th>Diktat</th>
				<th>SAPP</th>
			</tr>
			{foreach $data_set as $m}
			<tr>
				<td>{$m.NM_SEMESTER}</td>
				<td>{$m.KD_MATA_KULIAH}</td>
				<td>{$m.NM_MATA_KULIAH}</td>
				<td>{$m.KREDIT_SEMESTER}</td>
				<td>{$m.KREDIT_TATAP_MUKA}</td>
				<td>{$m.KREDIT_PRAKTIKUM}</td>
				<td>{$m.NM_STATUS_MK}</td>
				<td>{$m.NIDN_DOSEN}</td>
				<td>{$m.NM_PENGGUNA}</td>
				<td>{$m.JENJANG_DOSEN}</td>
				<td>{$m.PRODI_DOSEN}</td>
				<td>{$m.STATUS_AKTIF}</td>
				<td>{$m.SILABUS}</td>
				<td>{$m.BAHAN_AJAR}</td>
				<td>{$m.DIKTAT}</td>
				<td>{$m.SAPP}</td>
			</tr>
			{/foreach}
		</table>
	{/if}
	
	{if $smarty.get.view_id == 7}
		<table border="1" cellspacing="0" cellpadding="2" class="view">
			<tr>
				<th>Tahun Semester</th>
				<th>Jenjang</th>
				<th>Prodi</th>
				<th>NIM</th>
				<th>Nama Mahasiswa</th>
				<th>Nama Dosen</th>
			</tr>
			{foreach $data_set as $d}
			<tr>
				<td>{$d.NM_SEMESTER}</td>
				<td>{$d.NM_JENJANG}</td>
				<td>{$d.NM_PROGRAM_STUDI}</td>
				<td>{$d.NIM_MHS}</td>
				<td>{$d.NM_MHS}</td>
				<td>{$d.NM_DOSEN}</td>
			</tr>
			{/foreach}
		</table>
	{/if}
	
	{if $smarty.get.view_id == 8}
		<table border="1" cellspacing="0" cellpadding="2" class="view">
			<tr>
				<th>Jenjang</th>
				<th>Prodi</th>
				<th>NIDN</th>
				<th>NIP</th>
				<th>Nama</th>
				<th>Golongan</th>
				<th>Jabatan</th>
				<th>Status</th>
			</tr>
			{foreach $data_set as $d}
			<tr>
				<td>{$d.NM_JENJANG}</td>
				<td>{$d.NM_PROGRAM_STUDI}</td>
				<td>{$d.NIDN_DOSEN}</td>
				<td>{$d.NIP_DOSEN}</td>
				<td>{$d.NM_PENGGUNA}</td>
				<td>{$d.NM_GOLONGAN}</td>
				<td>{$d.NM_JABATAN_PEGAWAI}</td>
				<td>{if $d.NM_GOLONGAN == 'DLB'}DLB{else}DT{/if}</td>
			</tr>
			{/foreach}
		</table>
	{/if}
	
	{if $smarty.get.view_id == 9}
		<table border="1" cellspacing="0" cellpadding="2" class="view">
			<tr>
				<th>No</th>
				<th>Nama Ruangan</th>
				<th>Kapasitas</th>
				<th>Jenis</th>
				<th>Deskripsi</th>
			</tr>
			{foreach $data_set as $d}
			<tr>
				<td>{$d@index+1}</td>
				<td>{$d.NM_RUANGAN}</td>
				<td>{$d.KAPASITAS_RUANGAN}</td>
				<td>{$d.TIPE_RUANGAN}</td>
				<td>{$d.DESKRIPSI_RUANGAN}</td>
			</tr>
			{/foreach}
		</table>   
	{/if}
	
	{if $smarty.get.view_id == 10}
		<table border="1" cellspacing="0" cellpadding="2" class="view">
			<tr>
				<th>NIP</th>
				<th>Nama</th>
				<th>Jabatan</th>
				<th>Fakultas</th>
				<th>No Telp</th>
			</tr>
			{foreach $data_set as $d}
			<tr>
				<td>{$d.USERNAME}</td>
				<td>{$d.NM_PENGGUNA}</td>
				<td>{$d.NM_JABATAN_PEGAWAI}</td>
				<td>{$d.SINGKATAN_FAKULTAS}</td>
				<td>{$d.TLP_DOSEN}</td>
			</tr>
			{/foreach}
		</table>   
	{/if}
{/if}