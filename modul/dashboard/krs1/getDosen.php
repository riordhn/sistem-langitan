<?php
include('../../../config.php');

$dosen_set = $db->QueryToArray("
    SELECT D.ID_DOSEN, NM_PENGGUNA FROM PENGGUNA P
    JOIN DOSEN D ON D.ID_PENGGUNA = P.ID_PENGGUNA
    WHERE D.ID_PROGRAM_STUDI = {$_GET['id_program_studi']} ORDER BY P.NM_PENGGUNA");
    
echo "<option value=\"\">-- Pilih Dosen --</option>";

foreach ($dosen_set as $d)
    echo "<option value=\"{$d['ID_DOSEN']}\">{$d['NM_PENGGUNA']}</option>";
?>
