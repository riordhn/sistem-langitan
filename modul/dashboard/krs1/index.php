<?php
include('../../../config.php');

if (!$_SERVER['REMOTE_ADDR'] == '210.57.215.198')
	exit();

// $id_pimpinan_set = array(1, 2, 3, 4, 5, 6, 7, 8, 30, 31, 32, 33, 53, 81);

// if (in_array($user->ID_JABATAN_PEGAWAI, $id_pimpinan_set) == false)
// {
    // if ($_SERVER['REMOTE_ADDR'] != '210.57.212.66')
    // {
        // die('Anda tidak punya akses ke sini.');
    // }
// }

$id_fakultas = get('id_fakultas', '');
$id_program_studi = get('id_program_studi', '');
$thn_angkatan_mhs = get('thn_angkatan_mhs', '');
$id_dosen = get('id_dosen', '');
$id_semester = get('id_semester', '');
$view_id = get('view_id', '');
$id_kurikulum_prodi = get('id_kurikulum_prodi', '');

if (in_array($user->ID_JABATAN_PEGAWAI, array(5, 6, 7, 8)))
    $smarty->assign('fakultas_set', $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_FAKULTAS = {$user->ID_FAKULTAS} ORDER BY ID_FAKULTAS"));
else
    $smarty->assign('fakultas_set', $db->QueryToArray("SELECT * FROM FAKULTAS ORDER BY ID_FAKULTAS"));


$smarty->assign('angkatan_set', $db->QueryToArray("SELECT DISTINCT THN_ANGKATAN_MHS FROM MAHASISWA ORDER BY THN_ANGKATAN_MHS DESC"));

$smarty->assign('view_set', array(
    array('ID' => '1', 'VIEW' => 'Data Master Mahasiswa'),
    array('ID' => '2', 'VIEW' => 'Data Aktivitas Kuliah Mahasiswa'),
    array('ID' => '3', 'VIEW' => 'Data Nilai Mahasiswa'),
    array('ID' => '4', 'VIEW' => 'Data Status (Cuti / Lulus / Non-Aktif / Keluar / D.O.)'),
    array('ID' => '5', 'VIEW' => 'Data Aktivitas Mengajar Dosen'),
    array('ID' => '6', 'VIEW' => 'Data Tabel Mata Kuliah'),
    array('ID' => '7', 'VIEW' => 'Data Dosen Wali'),
    array('ID' => '8', 'VIEW' => 'Data Kepangkatan Dosen'),
    array('ID' => '9', 'VIEW' => 'Data Sarana dan Prasaran'),
    array('ID' => '10', 'VIEW' => 'Data Pimpinan'),
    array('ID' => '11', 'VIEW' => 'Data Kurikulum'),
));

if ($id_fakultas != '')
{
    $smarty->assign('program_studi_set', $db->QueryToArray("
        SELECT ID_PROGRAM_STUDI, '[ ' || NM_JENJANG || '] ' || NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI FROM PROGRAM_STUDI PS
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        WHERE ID_FAKULTAS = {$id_fakultas} AND PS.ID_PROGRAM_STUDI <> 197"));
}

if ($view_id == 1 && $id_program_studi != '' && $thn_angkatan_mhs != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT 
          J.NM_JENJANG,
          PS.NM_PROGRAM_STUDI,
          M.NIM_MHS,
          P.NM_PENGGUNA,
          'REG' AS KELAS_MASUK,
          (select nm_kota from kota k where cast(k.id_kota as varchar2(30)) = p.tempat_lahir) as kota_lahir,
          p.tgl_lahir_pengguna,
          case p.kelamin_pengguna when '1' then 'L' when '2' then 'P' end as jk,
          m.thn_angkatan_mhs,
          '' as semester_masuk,
          '' as batas_studi,
          '' AS KOTA_ASAL,
          m.tgl_terdaftar_mhs,
          m.tgl_lulus_mhs,
          '' as status_mhs,
          case NVL(m.id_c_mhs,-1)
            when -1 then 'Mala'
            else case m.thn_angkatan_mhs when 2011 then 'Maba' else 'Maba Pindahan' end
          end as status_masuk
        FROM MAHASISWA M
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        WHERE M.ID_PROGRAM_STUDI = {$id_program_studi} AND M.THN_ANGKATAN_MHS = {$thn_angkatan_mhs}"));
}

if ($view_id == 2 && $id_program_studi != '' && $thn_angkatan_mhs != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT
          S.NM_SEMESTER || ' ' || s.tahun_ajaran AS NM_SEMESTER,
          J.NM_JENJANG, PS.NM_PROGRAM_STUDI,
          M.NIM_MHS, P.NM_PENGGUNA,
          MS.SKS_SEMESTER, MS.IPS_MHS_STATUS, MS.SKS_TOTAL_MHS_STATUS, MS.IPK_MHS_STATUS
        FROM MAHASISWA M
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        LEFT JOIN MHS_STATUS MS ON MS.ID_MHS = M.ID_MHS
        LEFT JOIN SEMESTER S ON S.ID_SEMESTER = MS.ID_SEMESTER
        WHERE M.ID_PROGRAM_STUDI = {$id_program_studi} AND M.THN_ANGKATAN_MHS = {$thn_angkatan_mhs}
        ORDER BY M.NIM_MHS, S.TAHUN_AJARAN, S.NM_SEMESTER"));
}

if ($view_id == 3 && $id_program_studi != '' && $thn_angkatan_mhs != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT
          S.NM_SEMESTER || ' ' || s.tahun_ajaran AS NM_SEMESTER,
          J.NM_JENJANG,
          PS.NM_PROGRAM_STUDI,
          M.NIM_MHS,
          p.nm_pengguna,
          mk.nm_mata_kuliah,
          pm.nilai_huruf, pm.nilai_angka,
          km.no_kelas_mk
        FROM PENGAMBILAN_MK PM
        LEFT JOIN SEMESTER S ON S.ID_SEMESTER = PM.ID_SEMESTER
        LEFT JOIN KELAS_MK KM ON km.id_kelas_mk = pm.id_kelas_mk
        LEFT JOIN KURIKULUM_MK KUR ON kur.id_kurikulum_mk = km.id_kurikulum_mk
        LEFT JOIN MATA_KULIAH MK ON mk.id_mata_kuliah = kur.id_mata_kuliah
        LEFT JOIN MAHASISWA M ON M.ID_MHS = PM.ID_MHS
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        WHERE PM.STATUS_APV_PENGAMBILAN_MK = 1 AND M.ID_PROGRAM_STUDI = {$id_program_studi} AND M.THN_ANGKATAN_MHS = {$thn_angkatan_mhs} order by m.nim_mhs"));
}

if ($view_id == 5 && $id_program_studi != '')
{
    $smarty->assign('dosen_set', $db->QueryToArray("
        SELECT D.ID_DOSEN, NM_PENGGUNA FROM PENGGUNA P
        JOIN DOSEN D ON D.ID_PENGGUNA = P.ID_PENGGUNA
        WHERE D.ID_PROGRAM_STUDI = {$id_program_studi} ORDER BY P.NM_PENGGUNA"));
        
    if ($id_dosen != '')
    {
        $filter_dosen = "AND D.ID_DOSEN = {$id_dosen}";
    }
    
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT 
          S.NM_SEMESTER || ' ' || s.tahun_ajaran AS NM_SEMESTER,
          J.NM_JENJANG, PS.NM_PROGRAM_STUDI,
          D.NIDN_DOSEN, D.NIP_DOSEN, P.NM_PENGGUNA,
          MK.NM_MATA_KULIAH,
          R.NM_RUANGAN,
          0 AS TATAP_MUKA_TOTAL,
          (0) AS TATAP_MUKA_ASLI,
          JH.NM_JADWAL_HARI,
          jm1.jam_mulai||':'||jm1.menit_mulai as jam_mulai,
          jm2.jam_selesai||':'||jm2.menit_selesai as jam_selesai
        FROM PENGAMPU_MK PENG
        LEFT JOIN DOSEN D ON D.ID_DOSEN = PENG.ID_DOSEN
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = D.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN KELAS_MK KM ON KM.ID_KELAS_MK = PENG.ID_KELAS_MK
        LEFT JOIN JADWAL_KELAS JK ON JK.ID_KELAS_MK = KM.ID_KELAS_MK
        LEFT JOIN JADWAL_HARI JH ON JH.ID_JADWAL_HARI = JK.ID_JADWAL_HARI
        left join jadwal_jam jm1 on jm1.id_jadwal_jam = jk.id_jadwal_jam_mulai
        left join jadwal_jam jm2 on jm2.id_jadwal_jam = jk.id_jadwal_jam_selesai
        LEFT JOIN RUANGAN R ON R.ID_RUANGAN = JK.ID_RUANGAN
        LEFT JOIN KURIKULUM_MK KUR ON kur.id_kurikulum_mk = km.id_kurikulum_mk
        LEFT JOIN MATA_KULIAH MK ON mk.id_mata_kuliah = kur.id_mata_kuliah
        LEFT JOIN SEMESTER S ON S.ID_SEMESTER = KM.ID_SEMESTER
        WHERE PS.ID_PROGRAM_STUDI = {$id_program_studi} {$filter_dosen} ORDER BY S.TAHUN_AJARAN DESC, S.NM_SEMESTER, D.ID_DOSEN"));
}

if ($view_id == 6 || $view_id == 11)
{
    $smarty->assign('semester_set', $db->QueryToArray("
        SELECT ID_SEMESTER, NM_SEMESTER, TAHUN_AJARAN FROM SEMESTER ORDER BY TAHUN_AJARAN DESC, NM_SEMESTER"));
}

if ($view_id == 6 && $id_program_studi != '')
{
    if ($id_semester != '')
    {
        $filter_semester = "AND S.ID_SEMESTER = {$id_semester}";
    }
    
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT MK.ID_MATA_KULIAH, MK.KD_MATA_KULIAH, MK.NM_MATA_KULIAH, NVL(KMK.KREDIT_SEMESTER, 0) AS KREDIT_SEMESTER,
          KMK.ID_KURIKULUM_MK, KMK.TINGKAT_SEMESTER,
          (SELECT COUNT(*) FROM KELAS_MK KLS WHERE KLS.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK AND KLS.ID_SEMESTER = {$id_semester}) AS JML_KELAS,
          (SELECT COUNT(*) FROM KELAS_MK KLS JOIN JADWAL_KELAS JK ON JK.ID_KELAS_MK = KLS.ID_KELAS_MK
           WHERE KLS.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK AND KLS.ID_SEMESTER = {$id_semester}) AS JML_TATAP_MUKA,
          CASE MK.STATUS_PRAKTIKUM WHEN '1' THEN 'Y' ELSE '-' END AS STATUS_PRAKTIKUM
        FROM MATA_KULIAH MK
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = MK.ID_PROGRAM_STUDI
        JOIN KURIKULUM_MK KMK ON KMK.ID_MATA_KULIAH = MK.ID_MATA_KULIAH
        WHERE MK.ID_PROGRAM_STUDI = {$id_program_studi}
        ORDER BY KMK.TINGKAT_SEMESTER, MK.KD_MATA_KULIAH"));
}

if ($view_id == 7 && $id_program_studi != '' && $thn_angkatan_mhs != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT 
          S.NM_SEMESTER || ' ' || s.tahun_ajaran AS NM_SEMESTER,
          J.NM_JENJANG,
          PS.NM_PROGRAM_STUDI,
          M.NIM_MHS,
          P.NM_PENGGUNA AS NM_MHS,
          P2.NM_PENGGUNA AS NM_DOSEN
        FROM MAHASISWA M
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        LEFT JOIN DOSEN_WALI DW ON DW.ID_MHS = M.ID_MHS
        LEFT JOIN DOSEN D ON D.ID_DOSEN = DW.ID_DOSEN
        LEFT JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = D.ID_PENGGUNA
        LEFT JOIN SEMESTER S ON S.ID_SEMESTER = DW.ID_SEMESTER
        WHERE M.ID_PROGRAM_STUDI = {$id_program_studi} AND THN_ANGKATAN_MHS = {$thn_angkatan_mhs}
        ORDER BY S.TAHUN_AJARAN, S.NM_SEMESTER, M.NIM_MHS"));
}

if ($view_id == 8 && $id_program_studi != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT
          J.NM_JENJANG,
          PS.NM_PROGRAM_STUDI,
          D.NIDN_DOSEN,
          D.NIP_DOSEN,
          P.NM_PENGGUNA,
          G.NM_GOLONGAN,
          JP.NM_JABATAN_PEGAWAI
        FROM DOSEN D
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = D.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN GOLONGAN G ON G.ID_GOLONGAN = D.ID_GOLONGAN
        LEFT JOIN JABATAN_PEGAWAI JP ON JP.ID_JABATAN_PEGAWAI = D.ID_JABATAN_PEGAWAI
        WHERE D.ID_PROGRAM_STUDI = {$id_program_studi} ORDER BY P.NM_PENGGUNA"));
}

if ($view_id == 9 && $id_fakultas != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT
          R.NM_RUANGAN, R.TIPE_RUANGAN, R.DESKRIPSI_RUANGAN, R.KAPASITAS_RUANGAN
        FROM RUANGAN R
        LEFT JOIN GEDUNG G ON G.ID_GEDUNG = R.ID_GEDUNG
        LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS= G.ID_FAKULTAS
        WHERE F.ID_FAKULTAS = {$id_fakultas} ORDER BY R.TIPE_RUANGAN"));
}

if ($view_id == 10)
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT 
            P.USERNAME, P.NM_PENGGUNA, JP.NM_JABATAN_PEGAWAI, D.TLP_DOSEN, D.ALAMAT_RUMAH_DOSEN, F.SINGKATAN_FAKULTAS
        FROM DOSEN D
        JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
        JOIN JABATAN_PEGAWAI JP ON JP.ID_JABATAN_PEGAWAI = D.ID_JABATAN_PEGAWAI
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = D.ID_PROGRAM_STUDI
        JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
        ORDER BY JP.ID_JABATAN_PEGAWAI"));
}

if ($view_id == 11 && $id_program_studi != '')    
{
    $smarty->assign('kurikulum_prodi_set', $db->QueryToArray("
        SELECT KP.ID_KURIKULUM_PRODI, K.NM_KURIKULUM FROM KURIKULUM_PRODI KP
        JOIN KURIKULUM K ON K.ID_KURIKULUM = KP.ID_KURIKULUM
        WHERE KP.ID_PROGRAM_STUDI = {$id_program_studi}"));
}

if ($view_id == 11 && $id_program_studi != '' && $id_kurikulum_prodi != '' && $id_semester != '')
{
    $data_set = $db->QueryToArray("
        SELECT KMK.ID_KURIKULUM_MK, MK.KD_MATA_KULIAH, MK.NM_MATA_KULIAH, KMK.TINGKAT_SEMESTER, KMK.KREDIT_SEMESTER
        FROM KURIKULUM_MK KMK
        JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KMK.ID_MATA_KULIAH
        WHERE KMK.ID_KURIKULUM_PRODI = {$id_kurikulum_prodi}
        ORDER BY KMK.TINGKAT_SEMESTER");
        
    // bind dengan jadwal + dosen
    for ($i = 0; $i < count($data_set); $i++)
    {
        $id_kurikulum_mk = $data_set[$i]['ID_KURIKULUM_MK'];
        
        $data_set[$i]['jadwal_set'] = array();
        $data_set[$i]['dosen_set'] = array();
        
        // binding jadwal kelas
        $data_set[$i]['jadwal_set'] = $db->QueryToArray("
            SELECT KLS.ID_KELAS_MK, JK.ID_JADWAL_KELAS,
              JH.NM_JADWAL_HARI, J1.NM_JADWAL_JAM as JAM_MULAI, J2.NM_JADWAL_JAM AS JAM_SELESAI, R.NM_RUANGAN
            FROM KELAS_MK KLS
            JOIN JADWAL_KELAS JK ON JK.ID_KELAS_MK = KLS.ID_KELAS_MK
            JOIN JADWAL_HARI JH ON JH.ID_JADWAL_HARI = JK.ID_JADWAL_HARI
            JOIN JADWAL_JAM J1 ON J1.ID_JADWAL_JAM = JK.ID_JADWAL_JAM_MULAI
            JOIN JADWAL_JAM J2 ON J2.ID_JADWAL_JAM = JK.ID_JADWAL_JAM_SELESAI
            JOIN RUANGAN R ON R.ID_RUANGAN = JK.ID_RUANGAN
            WHERE KLS.ID_KURIKULUM_MK = {$id_kurikulum_mk} AND KLS.ID_SEMESTER = {$id_semester}
            ORDER BY KLS.ID_KELAS_MK, JH.ID_JADWAL_HARI");
            
        // binding pengampu kelas
        $data_set[$i]['dosen_set'] = $db->QueryToArray("
            SELECT KLS.ID_KELAS_MK, P.NM_PENGGUNA
            FROM KELAS_MK KLS
            JOIN PENGAMPU_MK PM ON PM.ID_KELAS_MK = KLS.ID_KELAS_MK
            JOIN DOSEN D ON D.ID_DOSEN = PM.ID_DOSEN
            JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
            WHERE KLS.ID_KURIKULUM_MK = {$id_kurikulum_mk} AND KLS.ID_SEMESTER = {$id_semester}
            ORDER BY KLS.ID_KELAS_MK");
    }
        
    $smarty->assign('data_set', $data_set);
    
    //echo "<pre>";
    //print_r($data_set);
    //echo "</pre>";
    
    //exit();
}

$smarty->display('index.tpl');
?>