<?php
include('../../../config.php');

$filename = time() . ".xls";
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");;
header("Content-Disposition: attachment;filename={$filename}");
header("Content-Transfer-Encoding: binary ");

$id_fakultas = get('id_fakultas', '');
$id_program_studi = get('id_program_studi', '');
$thn_angkatan_mhs = get('thn_angkatan_mhs', '');
$id_dosen = get('id_dosen', '');
$id_semester = get('id_semester', '');
$view_id = get('view_id', '');

if (in_array($user->ID_JABATAN_PEGAWAI, array(5, 6, 7, 8)))
    $smarty->assign('fakultas_set', $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_FAKULTAS = {$user->ID_FAKULTAS} ORDER BY ID_FAKULTAS"));
else
    $smarty->assign('fakultas_set', $db->QueryToArray("SELECT * FROM FAKULTAS ORDER BY ID_FAKULTAS"));


$smarty->assign('angkatan_set', $db->QueryToArray("SELECT DISTINCT THN_ANGKATAN_MHS FROM MAHASISWA ORDER BY THN_ANGKATAN_MHS DESC"));

$smarty->assign('view_set', array(
    array('ID' => '1', 'VIEW' => 'Data Master Mahasiswa'),
    array('ID' => '2', 'VIEW' => 'Data Aktivitas Kuliah Mahasiswa'),
    array('ID' => '3', 'VIEW' => 'Data Nilai Mahasiswa'),
    array('ID' => '4', 'VIEW' => 'Data Status (Cuti / Lulus / Non-Aktif / Keluar / D.O.)'),
    array('ID' => '5', 'VIEW' => 'Data Aktivitas Mengajar Dosen'),
    array('ID' => '6', 'VIEW' => 'Data Tabel Mata Kuliah'),
    array('ID' => '7', 'VIEW' => 'Data Dosen Wali'),
    array('ID' => '8', 'VIEW' => 'Data Kepangkatan Dosen'),
    array('ID' => '9', 'VIEW' => 'Data Sarana dan Prasaran'),
    array('ID' => '10', 'VIEW' => 'Data Pimpinan')
));

if ($id_fakultas != '')
{
    $smarty->assign('program_studi_set', $db->QueryToArray("
        SELECT ID_PROGRAM_STUDI, '[ ' || NM_JENJANG || '] ' || NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI FROM PROGRAM_STUDI PS
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        WHERE ID_FAKULTAS = {$id_fakultas} AND PS.ID_PROGRAM_STUDI <> 197"));
}

if ($view_id == 1 && $id_program_studi != '' && $thn_angkatan_mhs != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT 
          J.NM_JENJANG,
          PS.NM_PROGRAM_STUDI,
          M.NIM_MHS,
          P.NM_PENGGUNA,
          'REG' AS KELAS_MASUK,
          (select nm_kota from kota k where cast(k.id_kota as varchar2(30)) = p.tempat_lahir) as kota_lahir,
          p.tgl_lahir_pengguna,
          case p.kelamin_pengguna when '1' then 'L' when '2' then 'P' end as jk,
          m.thn_angkatan_mhs,
          '' as semester_masuk,
          '' as batas_studi,
          '' AS KOTA_ASAL,
          m.tgl_terdaftar_mhs,
          m.tgl_lulus_mhs,
          '' as status_mhs,
          case NVL(m.id_c_mhs,-1)
            when -1 then 'Mala'
            else case m.thn_angkatan_mhs when 2011 then 'Maba' else 'Maba Pindahan' end
          end as status_masuk
        FROM MAHASISWA M
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        WHERE M.ID_PROGRAM_STUDI = {$id_program_studi} AND M.THN_ANGKATAN_MHS = {$thn_angkatan_mhs}"));
}

if ($view_id == 2 && $id_program_studi != '' && $thn_angkatan_mhs != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT
          S.NM_SEMESTER || ' ' || s.tahun_ajaran AS NM_SEMESTER,
          J.NM_JENJANG, PS.NM_PROGRAM_STUDI,
          M.NIM_MHS, P.NM_PENGGUNA,
          MS.SKS_SEMESTER, MS.IPS_MHS_STATUS, MS.SKS_TOTAL_MHS_STATUS, MS.IPK_MHS_STATUS
        FROM MAHASISWA M
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        LEFT JOIN MHS_STATUS MS ON MS.ID_MHS = M.ID_MHS
        LEFT JOIN SEMESTER S ON S.ID_SEMESTER = MS.ID_SEMESTER
        WHERE M.ID_PROGRAM_STUDI = {$id_program_studi} AND M.THN_ANGKATAN_MHS = {$thn_angkatan_mhs}
        ORDER BY M.NIM_MHS, S.TAHUN_AJARAN, S.NM_SEMESTER"));
}

if ($view_id == 3 && $id_program_studi != '' && $thn_angkatan_mhs != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT
          S.NM_SEMESTER || ' ' || s.tahun_ajaran AS NM_SEMESTER,
          J.NM_JENJANG,
          PS.NM_PROGRAM_STUDI,
          M.NIM_MHS,
          p.nm_pengguna,
          mk.nm_mata_kuliah,
          pm.nilai_huruf, pm.nilai_angka,
          km.no_kelas_mk
        FROM PENGAMBILAN_MK PM
        LEFT JOIN SEMESTER S ON S.ID_SEMESTER = PM.ID_SEMESTER
        LEFT JOIN KELAS_MK KM ON km.id_kelas_mk = pm.id_kelas_mk
        LEFT JOIN KURIKULUM_MK KUR ON kur.id_kurikulum_mk = km.id_kurikulum_mk
        LEFT JOIN MATA_KULIAH MK ON mk.id_mata_kuliah = kur.id_mata_kuliah
        LEFT JOIN MAHASISWA M ON M.ID_MHS = PM.ID_MHS
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        WHERE PM.STATUS_APV_PENGAMBILAN_MK = 1 AND M.ID_PROGRAM_STUDI = {$id_program_studi} AND M.THN_ANGKATAN_MHS = {$thn_angkatan_mhs} order by m.nim_mhs"));
}

if ($view_id == 5 && $id_program_studi != '')
{
    $smarty->assign('dosen_set', $db->QueryToArray("
        SELECT D.ID_DOSEN, NM_PENGGUNA FROM PENGGUNA P
        JOIN DOSEN D ON D.ID_PENGGUNA = P.ID_PENGGUNA
        WHERE D.ID_PROGRAM_STUDI = {$id_program_studi} ORDER BY P.NM_PENGGUNA"));
        
    if ($id_dosen != '')
    {
        $filter_dosen = "AND D.ID_DOSEN = {$id_dosen}";
    }
    
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT 
          S.NM_SEMESTER || ' ' || s.tahun_ajaran AS NM_SEMESTER,
          J.NM_JENJANG, PS.NM_PROGRAM_STUDI,
          D.NIDN_DOSEN, D.NIP_DOSEN, P.NM_PENGGUNA,
          MK.NM_MATA_KULIAH,
          KM.NO_KELAS_MK,
          0 AS TATAP_MUKA_TOTAL,
          (0) AS TATAP_MUKA_ASLI,
          JH.NM_JADWAL_HARI,
          jm1.jam_mulai||':'||jm1.menit_mulai as jam_mulai,
          jm2.jam_selesai||':'||jm2.menit_selesai as jam_selesai
        FROM PENGAMPU_MK PENG
        LEFT JOIN DOSEN D ON D.ID_DOSEN = PENG.ID_DOSEN
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = D.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN KELAS_MK KM ON KM.ID_KELAS_MK = PENG.ID_KELAS_MK
        LEFT JOIN JADWAL_KELAS JK ON JK.ID_KELAS_MK = KM.ID_KELAS_MK
        LEFT JOIN JADWAL_HARI JH ON JH.ID_JADWAL_HARI = JK.ID_JADWAL_HARI
        left join jadwal_jam jm1 on jm1.id_jadwal_jam = jk.id_jadwal_jam_mulai
        left join jadwal_jam jm2 on jm2.id_jadwal_jam = jk.id_jadwal_jam_selesai
        LEFT JOIN KURIKULUM_MK KUR ON kur.id_kurikulum_mk = km.id_kurikulum_mk
        LEFT JOIN MATA_KULIAH MK ON mk.id_mata_kuliah = kur.id_mata_kuliah
        LEFT JOIN SEMESTER S ON S.ID_SEMESTER = KM.ID_SEMESTER
        WHERE PS.ID_PROGRAM_STUDI = {$id_program_studi} {$filter_dosen} ORDER BY S.TAHUN_AJARAN DESC, S.NM_SEMESTER, D.ID_DOSEN"));
}

if ($view_id == 6)
{
    $smarty->assign('semester_set', $db->QueryToArray("
        SELECT ID_SEMESTER, NM_SEMESTER, TAHUN_AJARAN FROM SEMESTER ORDER BY TAHUN_AJARAN DESC, NM_SEMESTER"));
}

if ($view_id == 6 && $id_program_studi != '')
{
    if ($id_semester != '')
    {
        $filter_semester = "AND S.ID_SEMESTER = {$id_semester}";
    }
    
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT 
          S.NM_SEMESTER || ' ' || s.tahun_ajaran AS NM_SEMESTER,
          J1.NM_JENJANG, PS1.NM_PROGRAM_STUDI,
          MK.KD_MATA_KULIAH, MK.NM_MATA_KULIAH, MK.KREDIT_SEMESTER, KUR.KREDIT_TATAP_MUKA, KUR.KREDIT_PRAKTIKUM, SM.NM_STATUS_MK,
          D.NIDN_DOSEN, P.NM_PENGGUNA, J2.NM_JENJANG AS JENJANG_DOSEN, PS2.ID_PROGRAM_STUDI AS PRODI_DOSEN,
          KUR.STATUS_AKTIF
        FROM MATA_KULIAH MK
        LEFT JOIN PROGRAM_STUDI PS1 ON PS1.ID_PROGRAM_STUDI = MK.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J1 ON J1.ID_JENJANG = PS1.ID_JENJANG
        LEFT JOIN KURIKULUM_MK KUR ON KUR.ID_MATA_KULIAH = MK.ID_MATA_KULIAH
        LEFT JOIN KELAS_MK KM ON KM.ID_KURIKULUM_MK = KUR.ID_KURIKULUM_MK
        LEFT JOIN SEMESTER S ON S.ID_SEMESTER = KM.ID_SEMESTER
        LEFT JOIN STATUS_MK SM ON SM.ID_STATUS_MK = KUR.ID_STATUS_MK
        LEFT JOIN DOSEN D ON D.ID_DOSEN = KM.ID_DOSEN
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
        LEFT JOIN PROGRAM_STUDI PS2 ON PS2.ID_PROGRAM_STUDI = D.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J2 ON J2.ID_JENJANG = PS2.ID_JENJANG
        WHERE KUR.ID_PROGRAM_STUDI = {$id_program_studi} {$filter_semester} ORDER BY MK.ID_MATA_KULIAH, MK.ID_PROGRAM_STUDI"));
}

if ($view_id == 7 && $id_program_studi != '' && $thn_angkatan_mhs != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT 
          S.NM_SEMESTER || ' ' || s.tahun_ajaran AS NM_SEMESTER,
          J.NM_JENJANG,
          PS.NM_PROGRAM_STUDI,
          M.NIM_MHS,
          P.NM_PENGGUNA AS NM_MHS,
          P2.NM_PENGGUNA AS NM_DOSEN
        FROM MAHASISWA M
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        LEFT JOIN DOSEN_WALI DW ON DW.ID_MHS = M.ID_MHS
        LEFT JOIN DOSEN D ON D.ID_DOSEN = DW.ID_DOSEN
        LEFT JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = D.ID_PENGGUNA
        LEFT JOIN SEMESTER S ON S.ID_SEMESTER = DW.ID_SEMESTER
        WHERE M.ID_PROGRAM_STUDI = {$id_program_studi} AND THN_ANGKATAN_MHS = {$thn_angkatan_mhs}
        ORDER BY S.TAHUN_AJARAN, S.NM_SEMESTER, M.NIM_MHS"));
}

if ($view_id == 8 && $id_program_studi != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT
          J.NM_JENJANG,
          PS.NM_PROGRAM_STUDI,
          D.NIDN_DOSEN,
          D.NIP_DOSEN,
          P.NM_PENGGUNA,
          G.NM_GOLONGAN,
          JP.NM_JABATAN_PEGAWAI
        FROM DOSEN D
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = D.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN GOLONGAN G ON G.ID_GOLONGAN = D.ID_GOLONGAN
        LEFT JOIN JABATAN_PEGAWAI JP ON JP.ID_JABATAN_PEGAWAI = D.ID_JABATAN_PEGAWAI
        WHERE D.ID_PROGRAM_STUDI = {$id_program_studi} ORDER BY P.NM_PENGGUNA"));
}

if ($view_id == 9 && $id_fakultas != '')
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT
          R.NM_RUANGAN, R.TIPE_RUANGAN, R.DESKRIPSI_RUANGAN, R.KAPASITAS_RUANGAN
        FROM RUANGAN R
        LEFT JOIN GEDUNG G ON G.ID_GEDUNG = R.ID_GEDUNG
        LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS= G.ID_FAKULTAS
        WHERE F.ID_FAKULTAS = {$id_fakultas} ORDER BY R.TIPE_RUANGAN"));
}

if ($view_id == 10)
{
    $smarty->assign('data_set', $db->QueryToArray("
        SELECT 
            P.USERNAME, P.NM_PENGGUNA, JP.NM_JABATAN_PEGAWAI, D.TLP_DOSEN, D.ALAMAT_RUMAH_DOSEN, F.SINGKATAN_FAKULTAS
        FROM DOSEN D
        JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
        JOIN JABATAN_PEGAWAI JP ON JP.ID_JABATAN_PEGAWAI = D.ID_JABATAN_PEGAWAI
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = D.ID_PROGRAM_STUDI
        JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
        ORDER BY JP.ID_JABATAN_PEGAWAI"));
}

$smarty->display('export-excel.tpl');
?>