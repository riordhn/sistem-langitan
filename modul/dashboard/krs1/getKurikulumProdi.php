<?php
include('../../../config.php');

$kurikulum_prodi_set = $db->QueryToArray("
    SELECT KP.ID_KURIKULUM_PRODI, K.NM_KURIKULUM FROM KURIKULUM_PRODI KP
    JOIN KURIKULUM K ON K.ID_KURIKULUM = KP.ID_KURIKULUM
    WHERE KP.ID_PROGRAM_STUDI = {$_GET['id_program_studi']}");

echo "<option value=\"\">-- Pilih Kurikulum --</option>";

foreach ($kurikulum_prodi_set as $kp)
    echo "<option value=\"{$kp['ID_KURIKULUM_PRODI']}\">" . $kp['NM_KURIKULUM'] . "</option>";
?>
