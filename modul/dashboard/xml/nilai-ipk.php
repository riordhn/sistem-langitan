<?php
$host = $_SERVER['HTTP_HOST'];
?>
<?xml version="1.0" encoding="iso-8859-1"?>
<ipk>
	<nama><![CDATA[Fauzi Alfa A.]]></nama>
	<prodi><![CDATA[Sistem Informasi]]></prodi>
	<nilai><![CDATA[3.99]]></nilai>
	<foto><![CDATA[http://<?php echo $host; ?>/modul/dashboard/xml/fauzi.jpg]]></foto>
	<ranking><![CDATA[#1]]></ranking>
</ipk>
<ipk>
	<nama><![CDATA[Endah .]]></nama>
	<prodi><![CDATA[Fisika]]></prodi>
	<nilai><![CDATA[3.44]]></nilai>
	<foto><![CDATA[http://<?php echo $host; ?>/modul/dashboard/xml/endah.jpg]]></foto>
	<ranking><![CDATA[#1]]></ranking>
</ipk>
<ipk>
	<nama><![CDATA[Dian E]]></nama>
	<prodi><![CDATA[D3 Bahasa Inggris]]></prodi>
	<nilai><![CDATA[3.77]]></nilai>
	<foto><![CDATA[http://<?php echo $host; ?>/modul/dashboard/xml/dian.jpg]]></foto>
	<ranking><![CDATA[#1]]></ranking>
</ipk>
<ipk>
	<nama><![CDATA[Tony Subroto]]></nama>
	<prodi><![CDATA[Kedokteran]]></prodi>
	<nilai><![CDATA[3.33]]></nilai>
	<foto><![CDATA[http://<?php echo $host; ?>/modul/dashboard/xml/tony.jpg]]></foto>
	<ranking><![CDATA[#1]]></ranking>
</ipk>