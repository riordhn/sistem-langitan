<?php
function list_files($dir)
{
  if(is_dir($dir))
  {
    if($handle = opendir($dir))
    {
      while(($file = readdir($handle)) !== false)
      {
        if($file != "." && $file != ".." && $file != "index.php" && $file != "Thumbs.db" && strpos($file,".php")>1)
        {
          $file = str_replace(".php",".xml",$file);
		  echo '<a target="_blank" href="'.$dir.$file.'">'.$file.'</a><br>'."\n";
        }
      }
      closedir($handle);
    }
  }
}
 
list_files("./");

?>

