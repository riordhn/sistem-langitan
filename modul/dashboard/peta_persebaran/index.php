<html>
    <head>
        <title>Peta Sebaran Mahasiswa Unair</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" href="style.css" type="text/css" />
        <link rel="shortcut icon" href="../../../img/icon.ico"/>
    </head>
    <body>
        <div id="header-bg">
            <div id="header"></div>
        </div>
        <script src="Scripts/swfobject_modified.js" type="text/javascript"></script>        
        <div id="map-sebaran">
            <div id="map-title">
                PETA SEBARAN MAHASISWA UNAIR
            </div>
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="1024" height="480" id="FlashID" title="map">
                <param name="movie" value="preloader.swf" />
                <param name="quality" value="high" />
                <param name="wmode" value="opaque" />
                <param name="swfversion" value="7.0.70.0" />
                <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
                <param name="expressinstall" value="Scripts/expressInstall.swf" />
                <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="preloader.swf" width="1024" height="480">
                    <!--<![endif]-->
                    <param name="quality" value="high" />
                    <param name="wmode" value="opaque" />
                    <param name="swfversion" value="7.0.70.0" />
                    <param name="expressinstall" value="Scripts/expressInstall.swf" />
                    <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                    <div>
                        <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
                        <p>
                            <a href="http://www.adobe.com/go/getflashplayer">
                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                            </a>
                        </p>
                    </div>
                    <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
        </div>
        <div class="footer">

            <div class="left_footer">
                <img src="img/footer_logo.jpg" width="170" height="37" />       
            </div>

            <div class="center_footer">
                Copyright © 2011 - Universitas Airlangga ·<br/> All Rights Reserved<br />
            </div>

        </div>
    </body>
</html>
