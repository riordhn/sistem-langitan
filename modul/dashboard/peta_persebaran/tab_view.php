<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->

<?php
	include '../../../includes/dbconnect.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Peta Sebaran Mahasiswa Unair</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" href="style.css" type="text/css" />
        <link rel="shortcut icon" href="../../../img/icon.ico"/>
    </head>
    <body>
        <div id="header-bg">
            <div id="header"></div>
        </div>
        <script src="Scripts/swfobject_modified.js" type="text/javascript"></script>        
        <div id="map-sebaran">
            <div id="map-title">
                SEBARAN MAHASISWA UNAIR
            </div>
			
			<div id='tab' style="width:800px; margin:auto;">

				<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
					<tr style="height:30px; background-color:#EAEAEA; font-weight:bold;" >
						<td>
							PROVINSI
						</td>

						<td>
							JUMLAH MAHASISWA
						</td>

					<tr>

					<?php
                            $stid = oci_parse($conn, "select prov.nm_provinsi,  count(prov.nm_provinsi) from calon_mahasiswa cm
							left join sekolah skl on skl.id_sekolah = cm.id_sekolah_asal
							left join kota kot on kot.id_kota = skl.id_kota
							left join provinsi prov on prov.id_provinsi = kot.id_provinsi 
							left join negara n on n.id_negara = prov.id_negara
							where n.id_negara = 1
							and (cm.status = 13 or cm.status = 1)
							group by prov.nm_provinsi, prov.kode_provinsi
							order by prov.kode_provinsi");
                            oci_execute($stid);
                            
                            $i = 1;

                            while (($row = oci_fetch_array($stid, OCI_NUM))) {
					?>

					<tr >

						<td >
							<?php
								echo $row[0];
							?>
						</td>

						<td>
							<?php
								echo $row[1];
							?>
						</td>

					</tr>
					<?php
							$i++;
							}
					?>
				</table>
			</div>

        </div>
        <div class="footer">

            <div class="left_footer">
                <img src="img/footer_logo.jpg" width="170" height="37" />       
            </div>

            <div class="center_footer">
                Copyright � 2011 - Universitas Airlangga �<br/> All Rights Reserved<br />
            </div>

        </div>
    </body>
</html>