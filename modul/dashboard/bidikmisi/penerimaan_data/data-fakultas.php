<?php
include '../../../../config.php';

$id_penerimaan = get('id_penerimaan');

$rows = $db->QueryToArray("
    select
        p.id_penerimaan, jumlah_diterima, jumlah_bayar, nvl(pendidikan,0) as jumlah_verifikasi_pendidikan,
        nvl(keuangan,0) as jumlah_verifikasi_keuangan, nvl(regmaba,0) as jumlah_regmaba,
        nvl(peminat,0) as jumlah_peminat, diterima.singkatan_fakultas, diterima.id_fakultas
    from penerimaan p
    left join (
        select id_penerimaan, f.singkatan_fakultas, f.id_fakultas, count(id_c_mhs) jumlah_diterima from calon_mahasiswa_baru cmb
				left join program_studi s on s.id_program_studi = cmb.id_program_studi
				left join fakultas f on f.id_fakultas = s.id_fakultas
        where cmb.tgl_diterima is not null and cmb.id_penerimaan = {$id_penerimaan}
        group by id_penerimaan, f.singkatan_fakultas, f.id_fakultas) diterima on diterima.id_penerimaan = p.id_penerimaan
    left join (
        select id_penerimaan, t.singkatan_fakultas, count(id_c_mhs) jumlah_bayar from (
            select cmb.id_c_mhs, cmb.id_penerimaan, f.singkatan_fakultas,
                (select count(pc.id_c_mhs) from pembayaran_cmhs pc where pc.id_c_mhs = cmb.id_c_mhs and tgl_bayar is not null) as jumlah_bayar
            from calon_mahasiswa_baru cmb
						join program_studi s on s.id_program_studi = cmb.id_program_studi
						join fakultas f on f.id_fakultas = s.id_fakultas
						where cmb.id_penerimaan = {$id_penerimaan}) t						
        where jumlah_bayar > 0
        group by id_penerimaan, t.singkatan_fakultas) bayar on bayar.id_penerimaan = p.id_penerimaan and diterima.singkatan_fakultas = bayar.singkatan_fakultas
    left join (
        select id_penerimaan, f.singkatan_fakultas, count(id_c_mhs) pendidikan from calon_mahasiswa_baru cmb
				left join program_studi s on s.id_program_studi = cmb.id_program_studi
				left join fakultas f on f.id_fakultas = s.id_fakultas
        where cmb.tgl_diterima is not null and cmb.tgl_verifikasi_pendidikan is not null and cmb.id_penerimaan = {$id_penerimaan}
        group by id_penerimaan, f.singkatan_fakultas) pendidikan on pendidikan.id_penerimaan = p.id_penerimaan and diterima.singkatan_fakultas = pendidikan.singkatan_fakultas
    left join (
        select id_penerimaan, f.singkatan_fakultas, count(id_c_mhs) keuangan from calon_mahasiswa_baru cmb
				left join program_studi s on s.id_program_studi = cmb.id_program_studi
				left join fakultas f on f.id_fakultas = s.id_fakultas
        where cmb.tgl_diterima is not null and cmb.tgl_verifikasi_keuangan is not null and cmb.id_penerimaan = {$id_penerimaan}
        group by id_penerimaan, f.singkatan_fakultas) keuangan on keuangan.id_penerimaan = p.id_penerimaan and diterima.singkatan_fakultas = keuangan.singkatan_fakultas
    left join (
        select id_penerimaan, f.singkatan_fakultas, count(id_c_mhs) regmaba from calon_mahasiswa_baru cmb
				left join program_studi s on s.id_program_studi = cmb.id_program_studi
				left join fakultas f on f.id_fakultas = s.id_fakultas
        where cmb.tgl_diterima is not null and cmb.tgl_regmaba is not null and cmb.id_penerimaan = {$id_penerimaan}
        group by id_penerimaan, f.singkatan_fakultas) kesehatan on kesehatan.id_penerimaan = p.id_penerimaan and diterima.singkatan_fakultas = kesehatan.singkatan_fakultas
    left join (
        select id_penerimaan, f.singkatan_fakultas, count(id_c_mhs) peminat from calon_mahasiswa_baru cmb
				left join program_studi s on s.id_program_studi = cmb.id_pilihan_1
				left join fakultas f on f.id_fakultas = s.id_fakultas
				where cmb.id_penerimaan = {$id_penerimaan}
        group by id_penerimaan, f.singkatan_fakultas) peminat on peminat.id_penerimaan = p.id_penerimaan and diterima.singkatan_fakultas = peminat.singkatan_fakultas
    where p.id_penerimaan = {$id_penerimaan}
	order by id_fakultas
	");

echo "<chart labelDisplay='ROTATE' slantLabels='1' seriesNameInToolTip='0' formatNumberScale='0' yAxisName='Jumlah Mahasiswa' xAxisName='Fakultas' showValues='0'>";

echo "<categories>";
foreach($rows as $data){
echo "<category label='".$data['SINGKATAN_FAKULTAS']."' />";
}
echo "</categories>";


echo "<dataset seriesName='Peminat'>";
foreach($rows as $data){
      echo "<set value='".$data['JUMLAH_PEMINAT']."' />";
}
echo "</dataset>


<dataset seriesName='Diterima'>";
foreach($rows as $data){
      echo "<set value='".$data['JUMLAH_DITERIMA']."' />";
}
echo "</dataset>


<dataset seriesName='Regmaba'>";
foreach($rows as $data){
      echo "<set value='".$data['JUMLAH_REGMABA']."' />";
}
echo "</dataset>

<dataset seriesName='Verifikasi Keuangan'>";
foreach($rows as $data){
      echo "<set value='".$data['JUMLAH_VERIFIKASI_KEUANGAN']."' />";
}
echo "</dataset>

<dataset seriesName='Bayar'>";
foreach($rows as $data){
      echo "<set value='".$data['JUMLAH_BAYAR']."' />";
}
echo "</dataset>

<dataset seriesName='Verifikasi Pendidikan'>";
foreach($rows as $data){
      echo "<set value='".$data['JUMLAH_VERIFIKASI_PENDIDIKAN']."' />";
}
echo "</dataset>

   		</chart>";
?>