<?php

include '../../../config.php';
$tahun_sekarang = date('Y');
$tahun_min_1 = $tahun_sekarang - 1;
$tahun_min_2 = $tahun_sekarang - 2;
$tahun_min_3 = $tahun_sekarang - 3;
$querySebaran = $db->QueryToArray("
SELECT P.NM_PROVINSI,CM.* FROM AUCC.PROVINSI P
LEFT JOIN(
  SELECT P.ID_PROVINSI,
  COUNT(M.ID_MHS) TOTAL,
  SUM(
    CASE
      WHEN M.THN_ANGKATAN_MHS='{$tahun_sekarang}'
      THEN 1
      ELSE 0
    END  
  ) TAHUN_{$tahun_sekarang},
  SUM(
    CASE
      WHEN M.THN_ANGKATAN_MHS='{$tahun_min_1}'
      THEN 1
      ELSE 0
    END  
  ) TAHUN_{$tahun_min_1},
  SUM(
    CASE
      WHEN M.THN_ANGKATAN_MHS='{$tahun_min_2}'
      THEN 1
      ELSE 0
    END  
  ) TAHUN_{$tahun_min_2},
  SUM(
    CASE
      WHEN M.THN_ANGKATAN_MHS='{$tahun_min_3}'
      THEN 1
      ELSE 0
    END  
  ) TAHUN_{$tahun_min_3}
  FROM AUCC.SEJARAH_BEASISWA SB 
  JOIN AUCC.MAHASISWA M ON M.ID_MHS=SB.ID_MHS
  JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
  LEFT JOIN AUCC.KOTA K ON K.ID_KOTA=M.ASAL_KOTA_MHS
  LEFT JOIN AUCC.PROVINSI P ON P.ID_PROVINSI=K.ID_PROVINSI
  WHERE SB.ID_BEASISWA=2
  GROUP BY P.ID_PROVINSI
) CM ON CM.ID_PROVINSI=P.ID_PROVINSI
WHERE P.ID_NEGARA=114
ORDER BY P.NM_PROVINSI");


foreach ($querySebaran as $data) {
    if ($data["TAHUN_{$tahun_sekarang}"] == '') {
        $data["TAHUN_{$tahun_sekarang}"] = 0;
    }
    if ($data["TAHUN_{$tahun_min_1}"] == '') {
        $data["TAHUN_{$tahun_min_1}"] = 0;
    }
    if ($data["TAHUN_{$tahun_min_2}"] == '') {
        $data["TAHUN_{$tahun_min_2}"] = 0;
    }
    if ($data["TAHUN_{$tahun_min_3}"] == '') {
        $data["TAHUN_{$tahun_min_3}"] = 0;
    }
    switch ($data['NM_PROVINSI']) {
        case 'BALI':
            edit_xml('Data_sebaran/bali.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'BANTEN':
            edit_xml('Data_sebaran/banten.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'BENGKULU':
            edit_xml('Data_sebaran/bengkulu.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'DI YOGYAKARTA':
            edit_xml('Data_sebaran/jogja.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'DKI JAKARTA':
            edit_xml('Data_sebaran/jakarta.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'GORONTALO':
            edit_xml('Data_sebaran/gorontalo.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'JAMBI':
            edit_xml('Data_sebaran/jambi.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'JAWA BARAT':
            edit_xml('Data_sebaran/jabar.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'JAWA TENGAH':
            edit_xml('Data_sebaran/jateng.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'JAWA TIMUR':
            edit_xml('Data_sebaran/jatim.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'KALIMANTAN BARAT':
            edit_xml('Data_sebaran/kalbar.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'KALIMANTAN SELATAN':
            edit_xml('Data_sebaran/kalsel.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'KALIMANTAN BARAT':
            edit_xml('Data_sebaran/kalbar.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'KALIMANTAN TENGAH':
            edit_xml('Data_sebaran/kalteng.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'KALIMANTAN TIMUR':
            edit_xml('Data_sebaran/kaltim.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'KEPULAUAN BANGKA BELITUNG':
            edit_xml('Data_sebaran/belitung.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'KEPULAUAN RIAU':
            edit_xml('Data_sebaran/kepriau.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'LAMPUNG':
            edit_xml('Data_sebaran/lampung.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'MALUKU':
            edit_xml('Data_sebaran/maluku.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'MALUKU UTARA':
            edit_xml('Data_sebaran/malut.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'NANGGROE ACEH DARUSSALAM':
            edit_xml('Data_sebaran/nad.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'NUSA TENGGARA BARAT':
            edit_xml('Data_sebaran/ntb.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'NUSA TENGGARA TIMUR':
            edit_xml('Data_sebaran/ntt.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'PAPUA':
            edit_xml('Data_sebaran/irteng.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'PAPUA BARAT':
            edit_xml('Data_sebaran/irba.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'RIAU':
            edit_xml('Data_sebaran/riau.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'SULAWESI SELATAN':
            edit_xml('Data_sebaran/sulsel.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'SULAWESI BARAT':
            edit_xml('Data_sebaran/sulbar.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'SULAWESI TENGAH':
            edit_xml('Data_sebaran/sulteng.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'SULAWESI TENGGARA':
            edit_xml('Data_sebaran/sultra.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'SULAWESI UTARA':
            edit_xml('Data_sebaran/sulut.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'SUMATERA BARAT':
            edit_xml('Data_sebaran/sumbar.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'SUMATERA SELATAN':
            edit_xml('Data_sebaran/sumsel.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
        case 'SUMATERA UTARA':
            edit_xml('Data_sebaran/sumut.xml', $data["TAHUN_{$tahun_sekarang}"], $data["TAHUN_{$tahun_min_1}"], $data["TAHUN_{$tahun_min_2}"], $data["TAHUN_{$tahun_min_3}"], $data["TOTAL"]);
            break;
    }
}

function edit_xml($file, $tahun1, $tahun2, $tahun3, $tahun4, $total) {
    $xmlDoc = new SimpleXMLElement($file, NULL, TRUE);
    $jumlah = count($xmlDoc);
    // Jika isi xml tidak berubah
    if ($xmlDoc->content[$jumlah - 4] != $tahun1 || $xmlDoc->content[$jumlah - 3] != $tahun2 || $xmlDoc->content[$jumlah - 2] != $tahun3 || $xmlDoc->content[$jumlah - 1] != $tahun4) {
        $xmlDoc->content[$jumlah - 4] = $tahun1;
        $xmlDoc->content[$jumlah - 3] = $tahun2;
        $xmlDoc->content[$jumlah - 2] = $tahun3;
        $xmlDoc->content[$jumlah - 1] = $tahun4;
        $xmlDoc->saveXML($file);
    }
    $xmlTotal = new SimpleXMLElement('Data_sebaran/total.xml', NULL, TRUE);
    $content = str_replace('Data_sebaran/', '', str_replace('.xml', '', $file));
    // Jika isi xml tidak berubah
    if ($xmlTotal->$content != $total) {
        $xmlTotal->$content = $total!=0?$total:"0";
        $xmlTotal->saveXML('Data_sebaran/total.xml');
    }
}

?>
