<?php

include 'config.php';
include 'class/sm.class.php';

$sm = new sm($db);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $sm->tambah_periode_sm(post('awal'), post('akhir'), post('tahun'));
    } else if (post('mode') == 'edit') {
        $sm->update_periode_sm(post('id'), post('awal'), post('akhir'), post('tahun'));
    } else if (post('mode') == 'delete') {
        $sm->delete_periode_sm(post('id'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit' || get('mode') == 'delete') {
        $smarty->assign('periode', $sm->get_periode_sm(get('id')));
    }
}

$smarty->assign('data_periode', $sm->load_periode_sm());
$smarty->display('periode-sm.tpl');
?>
