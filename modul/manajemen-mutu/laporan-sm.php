<?php

include 'config.php';
include 'class/sm.class.php';

$sm = new sm($db);


$y_now = date('Y');

//echo "<pre>";
//var_dump($sm->load_laporan_sm($y_now - 2, $y_now - 1, $y_now, $y_now + 1, $y_now + 2, $y_now + 3));
//echo "</pre>";
$smarty->assign('data_laporan', $sm->load_laporan_sm($y_now - 2, $y_now - 1, $y_now, $y_now + 1, $y_now + 2, $y_now + 3));
$smarty->assign('tahun_baseline', $y_now - 2);
$smarty->assign('tahun_capaian', $y_now - 1);
$smarty->assign('tahun_target', array($y_now, $y_now + 1, $y_now + 2, $y_now + 3));
$smarty->display('laporan-sm.tpl');
?>
