<?php

include 'config.php';
include 'class/dm.class.php';

$id_pt = $id_pt_user;

$dm = new dm($db, $id_pt);
$filter_jenis = get('filter_jenis');
$filter_lingkup = get('filter_lingkup');
if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $dm->tambah_dokumen(post('lingkup'), strtoupper(post('kode')), post('isi'));
    } else if (post('mode') == 'edit') {
        $dm->update_dokumen(post('id'), post('lingkup'), post('kode'), post('isi'));
    } else if (post('mode') == 'hide') {
        $dm->hide_dokumen(post('id'));
    } else if (post('mode') == 'unhide') {
        $dm->unhide_dokumen(post('id'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'tambah') {
        $smarty->assign('lingkup', $dm->get_lingkup_dokumen(get('l')));
    } elseif (get('mode') == 'edit' || get('mode') == 'upload') {
        if (get('mode') == 'upload') {
            $id = get('d');
            $smarty->assign('count_file', $db->QuerySingle("SELECT COUNT(*) FROM MM_FILE_DOKUMEN WHERE ID_MM_DOKUMEN='{$id}'"));
        }
        $smarty->assign('lingkup', $dm->get_lingkup_dokumen(get('l')));
        $smarty->assign('dokumen', $dm->get_dokumen(get('d')));
    } else if (get('mode') == 'file') {
        if (isset($_POST)) {
            if (post('mode') == 'upload') {
                $dm->upload_dokumen(post('id_dokumen'), $user->ID_PENGGUNA, post('uraian'), post('sebelum'), post('sesudah'), $_FILES, post('nama_file'));
            } else if (post('mode') == 'verifikasi') {
                $dm->verifikasi_file_dokumen(post('id_file'), $user->ID_PENGGUNA);
            } else if (post('mode') == 'batal_verifikasi') {
                $dm->batal_verifikasi_file_dokumen(post('id_file'));
            } else if (post('mode') == 'hapus_file') {
                $dm->delete_file_dokumen(post('id_file'));
            } else if (post('mode') == 'tampil') {
                $dm->tampil_file_dokumen(post('id_file'), post('id_dokumen'));
            } else if (post('mode') == 'batal_tampil') {
                $dm->batal_tampil_file_dokumen(post('id_file'));
            }
        }
        $smarty->assign('dokumen', $dm->get_dokumen(get('d')));
        $smarty->assign('data_file', $dm->load_file_dokumen(get('d')));
    } else if (get('mode') == 'filter') {
        $filter_document = [
            'jenis' => $filter_jenis,
            'lingkup' => $filter_lingkup
        ];

    }
}
// echo '<pre>';
// var_dump($dm->load_dokumen($filter_document));
// echo '</pre>';
// die();
$smarty->assign('data_jenis', $dm->load_jenis_pedoman());
$smarty->assign('data_lingkup', $dm->load_lingkup_dokumen($filter_jenis));
$smarty->assign('data_dokumen', $dm->load_dokumen($filter_document));
$smarty->assign('filter_jenis', $filter_jenis);
$smarty->assign('filter_lingkup', $filter_lingkup);
$smarty->display('dokumen.tpl');
