<?php

include 'config.php';
include 'class/sm.class.php';

$sm = new sm($db);
//$db->Query("DELETE FROM MM_SASARAN_HASIL");
$y_now = date('Y');
$y_target = array($y_now, $y_now + 1, $y_now + 2, $y_now + 3);
if (isset($_POST)) {
    if (post('mode') == 'simpan') {
        $sm->pengisian_laporan_sm($_POST, $y_now - 1, $y_target);
    }
}

$smarty->assign('tahun_baseline', $y_now - 2);
$smarty->assign('tahun_capaian', $y_now - 1);
$smarty->assign('tahun_target', $y_target);

$smarty->assign('data_laporan', $sm->load_laporan_sm($y_now - 2, $y_now - 1, $y_now, $y_now + 1, $y_now + 2, $y_now + 3));

$smarty->display('pengisian-sm.tpl');
?>
