<?php

class sm {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    // Periode Sasaran Mutu

    function load_periode_sm() {
        return $this->db->QueryToArray("SELECT * FROM MM_SASARAN_PERIODE ORDER BY TAHUN_PERIODE DESC");
    }

    function get_periode_sm($id) {
        $this->db->Query("SELECT * FROM MM_SASARAN_PERIODE WHERE ID_MM_SASARAN_PERIODE='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_periode_sm($awal, $akhir, $tahun) {
        $this->db->Query("
            INSERT INTO MM_SASARAN_PERIODE 
                (TGL_AWAL,TGL_AKHIR,TAHUN_PERIODE) 
            VALUES 
            ('{$awal}','{$akhir}','{$tahun}')");
    }

    function update_periode_sm($id, $awal, $akhir, $tahun) {
        $this->db->Query("
            UPDATE MM_SASARAN_PERIODE
            SET 
                TGL_AWAL='{$awal}',
                TGL_AKHIR='{$akhir}',
                TAHUN_PERIODE='{$tahun}'
            WHERE ID_MM_SASARAN_PERIODE='{$id}'");
    }

    function delete_periode_sm($id) {
        $this->db->Query("DELETE FROM MM_SASARAN_PERIODE WHERE ID_MM_SASARAN_PERIODE='{$id}'");
    }

    // Group Sasaran Mutu

    function load_group_sm() {
        return $this->db->QueryToArray("SELECT * FROM MM_SASARAN_GROUP ORDER BY NAMA_GROUP DESC");
    }

    function get_group_sm($id) {
        $this->db->Query("SELECT * FROM MM_SASARAN_GROUP WHERE ID_MM_SASARAN_GROUP='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_group_sm($nama) {
        $this->db->Query("
            INSERT INTO MM_SASARAN_GROUP
                (NAMA_GROUP) 
            VALUES 
            ('{$nama}')");
    }

    function update_group_sm($id, $nama) {
        $this->db->Query("
            UPDATE MM_SASARAN_GROUP
            SET 
                NAMA_GROUP='{$nama}'    
            WHERE ID_MM_SASARAN_GROUP='{$id}'");
    }

    function delete_group_sm($id) {
        $this->db->Query("DELETE FROM MM_SASARAN_GROUP WHERE ID_MM_SASARAN_GROUP='{$id}'");
    }

    // Aspek Sasaran Mutu

    function load_aspek_sm() {
        return $this->db->QueryToArray("
            SELECT MSG.NAMA_GROUP,MS.* 
            FROM MM_SASARAN_GROUP MSG
            JOIN MM_SASARAN MS ON MS.ID_MM_SASARAN_GROUP=MSG.ID_MM_SASARAN_GROUP
            ORDER BY MS.JENIS,MSG.NAMA_GROUP,MS.DESKRIPSI");
    }

    function get_aspek_sm($id) {
        $this->db->Query("
            SELECT MSG.NAMA_GROUP,MS.* 
            FROM MM_SASARAN_GROUP MSG
            JOIN MM_SASARAN MS ON MS.ID_MM_SASARAN_GROUP=MSG.ID_MM_SASARAN_GROUP
            WHERE MS.ID_MM_SASARAN='{$id}'
            ");
        return $this->db->FetchAssoc();
    }

    function tambah_aspek_sm($group, $jenis, $deskripsi) {
        $this->db->Query("
            INSERT INTO MM_SASARAN
                (ID_MM_SASARAN_GROUP,JENIS,DESKRIPSI) 
            VALUES 
            ('{$group}','{$jenis}','{$deskripsi}')");
    }

    function update_aspek_sm($id, $group, $jenis, $deskripsi) {
        $this->db->Query("
            UPDATE MM_SASARAN
            SET 
                ID_MM_SASARAN_GROUP='{$group}',
                JENIS='{$jenis}',
                DESKRIPSI='{$deskripsi}'
            WHERE ID_MM_SASARAN='{$id}'");
    }

    function delete_aspek_sm($id) {
        $this->db->Query("DELETE FROM MM_SASARAN WHERE ID_MM_SASARAN='{$id}'");
    }

    // Laporan

    function load_laporan_sm($y_base, $y_capai, $y_target_1, $y_target_2, $y_target_3, $y_target_4) {
        $arr_hasil = array();
        $jenis = array(0 => array('VALUE' => 1, "NAMA" => 'Sasaran Mutu'), 1 => array('VALUE' => 2, "NAMA" => 'Rencana Mutu'));
        foreach ($jenis as $j) {
            $arr_aspek = array();
            foreach ($this->load_group_sm() as $g) {
                $aspek = $this->db->QueryToArray("SELECT * FROM MM_SASARAN WHERE JENIS='{$j['VALUE']}' AND ID_MM_SASARAN_GROUP='{$g['ID_MM_SASARAN_GROUP']}'");
                $arr_jawaban = array();
                foreach ($aspek as $a) {
                    $p_base = $this->get_id_periode_sm($y_base);
                    $this->db->Query("SELECT * FROM MM_SASARAN_HASIL WHERE ID_MM_SASARAN_PERIODE='{$p_base}' AND ID_MM_SASARAN='{$a['ID_MM_SASARAN']}'");
                    $j_base = $this->db->FetchAssoc();
                    $p_capai = $this->get_id_periode_sm($y_capai);
                    $this->db->Query("SELECT * FROM MM_SASARAN_HASIL WHERE ID_MM_SASARAN_PERIODE='{$p_capai}' AND ID_MM_SASARAN='{$a['ID_MM_SASARAN']}'");
                    $j_capai = $this->db->FetchAssoc();
                    $p_target_1 = $this->get_id_periode_sm($y_target_1);
                    $this->db->Query("SELECT * FROM MM_SASARAN_HASIL WHERE ID_MM_SASARAN_PERIODE='{$p_target_1}' AND ID_MM_SASARAN='{$a['ID_MM_SASARAN']}'");
                    $j_target_1 = $this->db->FetchAssoc();
                    $p_target_2 = $this->get_id_periode_sm($y_target_2);
                    $this->db->Query("SELECT * FROM MM_SASARAN_HASIL WHERE ID_MM_SASARAN_PERIODE='{$p_target_2}' AND ID_MM_SASARAN='{$a['ID_MM_SASARAN']}'");
                    $j_target_2 = $this->db->FetchAssoc();
                    $p_target_3 = $this->get_id_periode_sm($y_target_3);
                    $this->db->Query("SELECT * FROM MM_SASARAN_HASIL WHERE ID_MM_SASARAN_PERIODE='{$p_target_3}' AND ID_MM_SASARAN='{$a['ID_MM_SASARAN']}'");
                    $j_target_3 = $this->db->FetchAssoc();
                    $p_target_4 = $this->get_id_periode_sm($y_target_4);
                    $this->db->Query("SELECT * FROM MM_SASARAN_HASIL WHERE ID_MM_SASARAN_PERIODE='{$p_target_4}' AND ID_MM_SASARAN='{$a['ID_MM_SASARAN']}'");
                    $j_target_4 = $this->db->FetchAssoc();
                    array_push($arr_jawaban, array('ASPEK' => $a, 'J_B' => $j_base, 'J_C' => $j_capai, 'J_T_1' => $j_target_1, 'J_T_2' => $j_target_2, 'J_T_3' => $j_target_3, 'J_T_4' => $j_target_4));
                }
                array_push($arr_aspek, array_merge($g, array('A' => $arr_jawaban)));
            }
            array_push($arr_hasil, array_merge($j, array('GA' => $arr_aspek)));
        }
        return $arr_hasil;
    }

    // Pengisian

    function pengisian_laporan_sm($post, $tahun_capaian, $tahun_target) {
        $p_min_1 = $this->get_id_periode_sm($tahun_capaian);
        for ($i = 1; $i < $post['jumlah_data']; $i++) {
            if ($post['t_sebelum' . $i] != '' && $post['c_sebelum' . $i] != '') {
                $cek_isi = $this->db->QuerySingle("SELECT COUNT(*) FROM MM_SASARAN_HASIL WHERE ID_MM_SASARAN='{$post['id_aspek' . $i]}' AND ID_MM_SASARAN_PERIODE='{$p_min_1}'");
                if ($cek_isi > 0) {
                    $this->update_data_sm($post['id_aspek' . $i], $p_min_1, $post['t_sebelum' . $i], $post['c_sebelum' . $i], $post['ukur' . $i], $post['referensi' . $i], $post['aktivitas' . $i], $post['pj' . $i], $post['pelaksana' . $i], $post['monev' . $i], '', '');
                } else {
                    $this->insert_data_sm($post['id_aspek' . $i], $p_min_1, $post['t_sebelum' . $i], $post['c_sebelum' . $i], $post['ukur' . $i], $post['referensi' . $i], $post['aktivitas' . $i], $post['pj' . $i], $post['pelaksana' . $i], $post['monev' . $i], '', '');
                }
            }
        }
        foreach ($tahun_target as $t) {
            $p_t = $this->get_id_periode_sm($t);
            for ($i = 1; $i < $post['jumlah_data']; $i++) {
                if ($post['target_' . $t . '_' . $i] != '') {
                    $cek_isi = $this->db->QuerySingle("SELECT COUNT(*) FROM MM_SASARAN_HASIL WHERE ID_MM_SASARAN='{$post['id_aspek' . $i]}' AND ID_MM_SASARAN_PERIODE='{$p_t}'");
                    if ($cek_isi > 0) {
                        $this->update_data_sm($post['id_aspek' . $i], $p_t, $post['target_' . $t . '_' . $i], '', '', '', '', '');
                    } else {
                        $this->insert_data_sm($post['id_aspek' . $i], $p_t, $post['target_' . $t . '_' . $i], '', '', '', '', '');
                    }
                }
            }
        }
    }

    function insert_data_sm($sasaran, $periode, $target, $capaian, $pengukuran, $referensi, $aktivitas, $pj, $pelaksana, $monev, $fakultas, $lembaga) {
        $query = "
            INSERT INTO MM_SASARAN_HASIL 
            (ID_MM_SASARAN_PERIODE,ID_MM_SASARAN,TARGET,CAPAIAN,PENGUKURAN,REFERENSI,AKTIVITAS_UTAMA,PJ,PELAKSANA,MONEV,ID_FAKULTAS,ID_LEMBAGA)
                VALUES
            ('{$periode}','{$sasaran}','{$target}','{$capaian}','{$pengukuran}','{$referensi}','{$aktivitas}','{$pj}','{$pelaksana}','{$monev}','{$fakultas}','{$lembaga}')
            ";
//        echo $query . "<br/>";
        $this->db->Query($query);
    }

    function update_data_sm($sasaran, $periode, $target, $capaian, $pengukuran, $referensi, $aktivitas, $pj, $pelaksana, $monev, $fakultas, $lembaga) {
        $query = "
            UPDATE MM_SASARAN_HASIL SET
                TARGET='{$target}',
                CAPAIAN='{$capaian}',
                PENGUKURAN='{$pengukuran}',
                REFERENSI='{$referensi}',
                AKTIVITAS_UTAMA='{$aktivitas}',
                PJ='{$pj}',
                PELAKSANA='{$pelaksana}',
                MONEV='{$monev}'
            WHERE ID_MM_SASARAN_PERIODE='{$periode}'
                AND ID_MM_SASARAN='{$sasaran}'
            ";
        $this->db->Query($query);
    }

    function get_id_periode_sm($tahun) {
        return $this->db->QuerySingle("SELECT ID_MM_SASARAN_PERIODE FROM MM_SASARAN_PERIODE WHERE TAHUN_PERIODE='{$tahun}'");
    }

    // Notifikasi

    function load_data_pelaksana() {
        return $this->db->QueryToArray("
            SELECT * FROM AUCC.PENGGUNA P
            JOIN AUCC.PEGAWAI PEG ON P.ID_PENGGUNA=PEG.ID_PENGGUNA
            WHERE P.ID_PENGGUNA IN (
                SELECT ID_PENGGUNA FROM AUCC.ROLE_PENGGUNA WHERE ID_ROLE=39
            )
            ");
    }

}

?>
