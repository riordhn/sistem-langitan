<?php

class dm
{

    public $db;
    public $id_perguruan_tinggi;

    function __construct($db, $id_pt)
    {
        $this->db = $db;
        $this->id_perguruan_tinggi = $id_pt;
    }

    // Pedoman

    function load_jenis_pedoman($id = '')
    {
        if ($id) {
            return $this->db->QueryToArray("SELECT * FROM MM_JENIS_PEDOMAN WHERE ID_MM_JENIS_PEDOMAN='{$id}' ORDER BY HIDDEN,KODE_PEDOMAN");
        } else {
            return $this->db->QueryToArray("SELECT * FROM MM_JENIS_PEDOMAN ORDER BY HIDDEN,KODE_PEDOMAN");
        }
    }

    function get_jenis_pedoman($id)
    {
        $this->db->Query("SELECT * FROM MM_JENIS_PEDOMAN WHERE ID_MM_JENIS_PEDOMAN='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_jenis_pedoman($kode, $nama)
    {
        $this->db->Query("INSERT INTO MM_JENIS_PEDOMAN (KODE_PEDOMAN,NAMA_PEDOMAN) VALUES ('{$kode}','{$nama}')");
    }

    function update_jenis_pedoman($id, $kode, $nama)
    {
        $this->db->Query("
            UPDATE MM_JENIS_PEDOMAN
                SET
                    KODE_PEDOMAN='{$kode}',
                    NAMA_PEDOMAN='{$nama}'
            WHERE ID_MM_JENIS_PEDOMAN='{$id}'
            ");
    }

    function hide_jenis_pedoman($id)
    {
        $this->db->Query("UPDATE MM_JENIS_PEDOMAN SET HIDDEN='1' WHERE ID_MM_JENIS_PEDOMAN='{$id}' ");
    }

    function unhide_jenis_pedoman($id)
    {
        $this->db->Query("UPDATE MM_JENIS_PEDOMAN SET HIDDEN='0' WHERE ID_MM_JENIS_PEDOMAN='{$id}' ");
    }

    // Lingkup


    function load_lingkup_dokumen($id_jenis = '', $id_lingkup = '')
    {
        if (empty($id_jenis) && empty($id_lingkup)) {
            $query = "
            SELECT MG.*,MJ.NAMA_PEDOMAN 
            FROM MM_GROUP_DOKUMEN MG
            JOIN MM_JENIS_PEDOMAN MJ ON MJ.ID_MM_JENIS_PEDOMAN=MG.ID_MM_JENIS_PEDOMAN
            ORDER BY MG.HIDDEN,MG.KODE_GROUP_DOKUMEN";
        } else if (!empty($id_jenis) && empty($id_lingkup)) {
            $query = "
            SELECT MG.*,MJ.NAMA_PEDOMAN 
            FROM MM_GROUP_DOKUMEN MG
            JOIN MM_JENIS_PEDOMAN MJ ON MJ.ID_MM_JENIS_PEDOMAN=MG.ID_MM_JENIS_PEDOMAN
            WHERE MJ.ID_MM_JENIS_PEDOMAN='{$id_jenis}' AND MG.HIDDEN=0
            ORDER BY MG.HIDDEN,MG.KODE_GROUP_DOKUMEN";
        } else if (!empty($id_jenis) && !empty($id_lingkup)) {
            $query = "
            SELECT MG.*,MJ.NAMA_PEDOMAN 
            FROM MM_GROUP_DOKUMEN MG
            JOIN MM_JENIS_PEDOMAN MJ ON MJ.ID_MM_JENIS_PEDOMAN=MG.ID_MM_JENIS_PEDOMAN
            WHERE MJ.ID_MM_JENIS_PEDOMAN='{$id_jenis}' AND MG.ID_MM_GROUP_DOKUMEN='{$id_lingkup}' AND MG.HIDDEN=0
            ORDER BY MG.HIDDEN,MG.KODE_GROUP_DOKUMEN";
        }
        return $this->db->QueryToArray($query);
    }

    function get_lingkup_dokumen($id)
    {
        $this->db->Query("
            SELECT MG.*,MJ.NAMA_PEDOMAN
            FROM MM_GROUP_DOKUMEN MG
            JOIN MM_JENIS_PEDOMAN MJ ON MJ.ID_MM_JENIS_PEDOMAN=MG.ID_MM_JENIS_PEDOMAN
            WHERE ID_MM_GROUP_DOKUMEN='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_lingkup_dokumen($kode, $pedoman, $nama)
    {
        $this->db->Query("INSERT INTO MM_GROUP_DOKUMEN (ID_MM_JENIS_PEDOMAN,KODE_GROUP_DOKUMEN,NAMA_GROUP_DOKUMEN,ID_PERGURUAN_TINGGI) VALUES ('{$pedoman}','{$kode}','{$nama}','{$this->id_perguruan_tinggi}')");
    }

    function update_lingkup_dokumen($id, $kode, $pedoman, $nama, $singkatan, $jabatan)
    {
        $this->db->Query("
            UPDATE MM_GROUP_DOKUMEN
                SET
                    ID_MM_JENIS_PEDOMAN='{$pedoman}',
                    KODE_GROUP_DOKUMEN='{$kode}',
                    NAMA_GROUP_DOKUMEN='{$nama}',
                    SINGKATAN_LINGKUP='{$singkatan}',
                    ID_JABATAN_LINGKUP='{$jabatan}'
            WHERE ID_MM_GROUP_DOKUMEN='{$id}'
            ");
    }

    function hide_lingkup_dokumen($id)
    {
        $this->db->Query("UPDATE MM_GROUP_DOKUMEN SET HIDDEN='1' WHERE ID_MM_GROUP_DOKUMEN='{$id}' ");
    }

    function unhide_lingkup_dokumen($id)
    {
        $this->db->Query("UPDATE MM_GROUP_DOKUMEN SET HIDDEN='0' WHERE ID_MM_GROUP_DOKUMEN='{$id}' ");
    }

    // Dokumen

    function load_dokumen($filter = [])
    {
        $arr_hasil = array();
        if (empty($filter)) {
            foreach ($this->load_jenis_pedoman() as $p) {
                $arr_group = array();
                $lingkups=$this->load_lingkup_dokumen($p['ID_MM_JENIS_PEDOMAN']);
                foreach ($lingkups as $l) {
                    $dokumen = $this->db->QueryToArray("
                        SELECT MD.*,MG.NAMA_GROUP_DOKUMEN,MF.NAMA_FILE,(SELECT COUNT(*) FROM MM_FILE_DOKUMEN WHERE ID_MM_DOKUMEN=MD.ID_MM_DOKUMEN) JUMLAH_FILE
                        ,(SELECT COUNT(*) FROM MM_FILE_DOKUMEN WHERE ID_MM_DOKUMEN=MD.ID_MM_DOKUMEN AND ID_VERIFIKATOR IS NOT NULL) JUMLAH_VERIFIKASI
                        FROM MM_DOKUMEN MD
                        JOIN MM_GROUP_DOKUMEN MG ON MG.ID_MM_GROUP_DOKUMEN=MD.ID_MM_GROUP_DOKUMEN
                        LEFT JOIN MM_FILE_DOKUMEN MF ON MF.ID_MM_DOKUMEN=MD.ID_MM_DOKUMEN AND MF.TAMPIL=1
                        WHERE MD.ID_MM_GROUP_DOKUMEN='{$l['ID_MM_GROUP_DOKUMEN']}'
                        ORDER BY MD.HIDDEN,MD.ID_MM_GROUP_DOKUMEN,MD.KODE_DOKUMEN ASC
                        ");
                    array_push($arr_group, array_merge($l, array('DOKUMEN' => $dokumen)));
                }
                array_push($arr_hasil, array_merge($p, array('LINGKUP' => $arr_group)));
            }
        } else {
            foreach ($this->load_jenis_pedoman($filter['jenis']) as $p) {
                $arr_group = array();
                $lingkups = $this->load_lingkup_dokumen($p['ID_MM_JENIS_PEDOMAN'], $filter['lingkup']);
                foreach ($lingkups as $l) {
                    $dokumen = $this->db->QueryToArray("
                        SELECT MD.*,MG.NAMA_GROUP_DOKUMEN,MF.NAMA_FILE,(SELECT COUNT(*) FROM MM_FILE_DOKUMEN WHERE ID_MM_DOKUMEN=MD.ID_MM_DOKUMEN) JUMLAH_FILE
                        ,(SELECT COUNT(*) FROM MM_FILE_DOKUMEN WHERE ID_MM_DOKUMEN=MD.ID_MM_DOKUMEN AND ID_VERIFIKATOR IS NOT NULL) JUMLAH_VERIFIKASI
                        FROM MM_DOKUMEN MD
                        JOIN MM_GROUP_DOKUMEN MG ON MG.ID_MM_GROUP_DOKUMEN=MD.ID_MM_GROUP_DOKUMEN
                        LEFT JOIN MM_FILE_DOKUMEN MF ON MF.ID_MM_DOKUMEN=MD.ID_MM_DOKUMEN AND MF.TAMPIL=1
                        WHERE MD.ID_MM_GROUP_DOKUMEN='{$l['ID_MM_GROUP_DOKUMEN']}'
                        ORDER BY MD.HIDDEN,MD.ID_MM_GROUP_DOKUMEN,MD.KODE_DOKUMEN ASC
                        ");
                    array_push($arr_group, array_merge($l, array('DOKUMEN' => $dokumen)));
                }
                array_push($arr_hasil, array_merge($p, array('LINGKUP' => $arr_group)));
            }
        }
        return $arr_hasil;
    }

    function get_dokumen($id)
    {
        $this->db->Query("
        SELECT MD.*,MG.NAMA_GROUP_DOKUMEN
        FROM MM_DOKUMEN MD
        JOIN MM_GROUP_DOKUMEN MG ON MG.ID_MM_GROUP_DOKUMEN=MD.ID_MM_GROUP_DOKUMEN
        WHERE MD.ID_MM_DOKUMEN='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_dokumen($lingkup, $kode, $nama)
    {
        $this->db->Query("INSERT INTO MM_DOKUMEN (ID_MM_GROUP_DOKUMEN,KODE_DOKUMEN,NAMA_DOKUMEN) VALUES ('{$lingkup}','{$kode}','{$nama}')");
    }

    function update_dokumen($id, $lingkup, $kode, $nama)
    {
        $this->db->Query("
            UPDATE MM_DOKUMEN
                SET
                    ID_MM_GROUP_DOKUMEN='{$lingkup}',
                    KODE_DOKUMEN='{$kode}',
                    NAMA_DOKUMEN='{$nama}'
            WHERE ID_MM_DOKUMEN='{$id}'
            ");
    }

    function hide_dokumen($id)
    {
        $this->db->Query("UPDATE MM_DOKUMEN SET HIDDEN=1 WHERE ID_MM_DOKUMEN='{$id}'");
    }

    function unhide_dokumen($id)
    {
        $this->db->Query("UPDATE MM_DOKUMEN SET HIDDEN=0 WHERE ID_MM_DOKUMEN='{$id}'");
    }

    // File Dokumen

    function UploadFile($path, $files, $file_name)
    {
        $error_message = '';
        $upload = false;
        // Format Nama File         
        $allowed_exts = array("pdf");
        $allowed_type = array('application/pdf');
        $extension = end(explode(".", $files["file"]["name"]));
        $type = $files["file"]["type"];
        $name = $files["file"]["name"];
        $tmp_name = $files["file"]["tmp_name"];
        $error = $files["file"]["error"];
        if (in_array($extension, $allowed_exts) && in_array($type, $allowed_type)) {
            if ($error > 0) {
                $error_message .= "Error file : " . $error;
            } else {
                if (file_exists("$path/$file_name")) {
                    $error_message .= $file_name . " Sudah Ada. ";
                } else {
                    move_uploaded_file($tmp_name, "$path/$file_name.$extension");
                    $upload = true;
                }
            }
        } else {
            $error_message .= "Format File {$name} tidak sesuai";
        }
        if (!$upload) {
            echo $error_message != '' ? alert_error($error_message) : "";
        }
        return $upload;
    }

    function upload_dokumen($dokumen, $id_upload, $uraian, $sebelum, $sesudah, $files, $nama_file)
    {
        $extension = end(explode(".", $files["file"]["name"]));
        $nama_file = str_replace(" ", "-", $nama_file);
        $upload = $this->UploadFile("../../files/document-aims", $files, $nama_file);
        $file_name = $nama_file . '.' . $extension;
        if ($upload) {
            $this->db->Query("
                INSERT INTO MM_FILE_DOKUMEN 
                    (ID_MM_DOKUMEN,ID_PENGGUNA,URAIAN_PERUBAHAN,SEBELUM,SESUDAH,NAMA_FILE,STATUS_PAKAI,WAKTU_UPDATE)
                VALUES
                    ('{$dokumen}','{$id_upload}','{$uraian}','{$sebelum}','{$sesudah}','{$file_name}',0,CURRENT_TIMESTAMP)
                ");
            echo alert_success("Data Berhasil DImasukkan");
        } else {
            echo alert_error("Gagal Upload");
        }
    }

    function load_file_dokumen($id)
    {
        return $this->db->QueryToArray("
            SELECT MF.*,TO_CHAR(MF.WAKTU_UPDATE,'DD/MON/YYYY HH24:MI:SS') WAKTU,P.NM_PENGGUNA,P2.NM_PENGGUNA VERIFIKATOR
            FROM AUCC.MM_FILE_DOKUMEN MF
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=MF.ID_PENGGUNA
            LEFT JOIN AUCC.PENGGUNA P2 ON P2.ID_PENGGUNA=MF.ID_VERIFIKATOR
            WHERE ID_MM_DOKUMEN='{$id}' 
            ORDER BY WAKTU_UPDATE DESC
            ");
    }

    function verifikasi_file_dokumen($id, $verifikator)
    {
        $this->db->Query("UPDATE MM_FILE_DOKUMEN SET STATUS_PAKAI=1,ID_VERIFIKATOR='{$verifikator}' WHERE ID_MM_FILE_DOKUMEN='{$id}'");
    }

    function batal_verifikasi_file_dokumen($id)
    {
        $this->db->Query("UPDATE MM_FILE_DOKUMEN SET STATUS_PAKAI=0,ID_VERIFIKATOR=NULL WHERE ID_MM_FILE_DOKUMEN='{$id}'");
    }

    function tampil_file_dokumen($id, $dokumen)
    {
        $this->db->Query("UPDATE MM_FILE_DOKUMEN SET TAMPIL=0 WHERE TAMPIL=1 AND ID_MM_DOKUMEN='{$dokumen}'");
        $this->db->Query("UPDATE MM_FILE_DOKUMEN SET TAMPIL=1 WHERE ID_MM_FILE_DOKUMEN='{$id}'");
    }

    function batal_tampil_file_dokumen($id)
    {
        $this->db->Query("UPDATE MM_FILE_DOKUMEN SET TAMPIL=0 WHERE ID_MM_FILE_DOKUMEN='{$id}'");
    }

    function delete_file_dokumen($id)
    {
        $file = $this->db->QuerySingle("SELECT NAMA_FILE FROM MM_FILE_DOKUMEN WHERE ID_MM_FILE_DOKUMEN='{$id}'");
        unlink("../../files/document-aims/{$file}");
        unlink("../../files/document-aims-swf/{$file}.swf");
        $this->db->Query("DELETE FROM MM_FILE_DOKUMEN WHERE ID_MM_FILE_DOKUMEN='{$id}'");
    }

    function cari_dokumen($key)
    {
        return $this->db->QueryToArray("
            SELECT MF.*,TO_CHAR(MF.WAKTU_UPDATE,'DD/MON/YYYY HH24:MI:SS') WAKTU,P.NM_PENGGUNA,MD.NAMA_DOKUMEN,MD.KODE_DOKUMEN
            FROM AUCC.MM_FILE_DOKUMEN MF
            JOIN AUCC.MM_DOKUMEN MD ON MD.ID_MM_DOKUMEN=MF.ID_MM_DOKUMEN
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=MF.ID_PENGGUNA
            WHERE UPPER(MD.NAMA_DOKUMEN) LIKE UPPER('%{$key}%') OR UPPER(MD.KODE_DOKUMEN) LIKE UPPER('%{$key}%')
            ORDER BY MD.KODE_DOKUMEN DESC
            ");
    }
}
