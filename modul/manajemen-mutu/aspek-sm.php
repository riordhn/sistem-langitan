<?php

include 'config.php';
include 'class/sm.class.php';

$sm = new sm($db);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $sm->tambah_aspek_sm(post('group'), post('jenis'), post('deskripsi'));
    } else if (post('mode') == 'edit') {
        $sm->update_aspek_sm(post('id'), post('group'), post('jenis'), post('deskripsi'));
    } else if (post('mode') == 'delete') {
        $sm->delete_aspek_sm(post('id'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit' || get('mode') == 'delete') {
        $smarty->assign('aspek', $sm->get_aspek_sm(get('id')));
    }
}

$smarty->assign('data_group', $sm->load_group_sm());
$smarty->assign('data_aspek', $sm->load_aspek_sm());
$smarty->display('aspek-sm.tpl');
?>
