<?php

include 'config.php';
include 'class/dm.class.php';

$id_pt = $id_pt_user;

$dm = new dm($db,$id_pt);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $dm->tambah_jenis_pedoman(post('kode'), post('isi'));
    } else if (post('mode') == 'edit') {
        $dm->update_jenis_pedoman(post('id'), post('kode'), post('isi'));
    } else if (post('mode') == 'hide') {
        $dm->hide_jenis_pedoman(post('id'));
    } else if (post('mode') == 'unhide') {
        $dm->unhide_jenis_pedoman(post('id'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit' || get('mode') == 'delete') {
        $smarty->assign('pedoman', $dm->get_jenis_pedoman(get('id')));
    }
}

$smarty->assign('data_pedoman', $dm->load_jenis_pedoman());
$smarty->display('jenis-pedoman.tpl');
?>
