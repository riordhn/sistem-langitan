<div class="center_title_bar">Kirim Notifikasi Pengisian</div>
{if $notif!=''}
    {$notif}
{/if}
<form action="notifikasi-sm.php" method="post">
    <table style="width: 60%">
        <tr>
            <td>Pesan</td>
            <td><textarea name="pesan" class="required" style="width: 90%"></textarea></td>
        </tr>
    </table>
    <table style="width: 60%">
        <tr>
            <th class="center">NAMA</th>
            <th class="center">NOMER</th>
            <th class="center">OPERASI</th>
        </tr>
        {foreach $data_pelaksana as $p}
            <tr>
                <td>{$p.NM_PENGGUNA}</td>
                <td>{$p.TELP_PEGAWAI}</td>
                <td class="center">
                    {if $p.TELP_PEGAWAI==''}
                        <span class="data-kosong">Kontak Masih Kosong</span> 
                    {else}
                        <input type="checkbox" value="{$p.ID_PENGGUNA}" name="id{$p@index+1}"/>
                        <input type="hidden" value="{$p.TELP_PEGAWAI}" name="nomer{$p@index+1}"/>
                    {/if}
                </td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="3" class="center">
                <input type="hidden" value="{count($data_pelaksana)}" name="jumlah_data"/>
                <input type="hidden" value="kirim_pesan" name="mode"/>
                <input type="submit" value="Kirim" class="button" />
            </td>
        </tr>
    </table>
</form>
{literal}
    <script>
        $('form').validate();
    </script>
{/literal}