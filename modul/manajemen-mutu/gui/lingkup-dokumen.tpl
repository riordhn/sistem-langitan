{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Lingkup Dokumen Mutu - Tambah</div>
    <form action="lingkup-dokumen.php" method="post">
        <fieldset style="width: 80%">
            <legend>Tambah Lingkup  Dokumen Mutu</legend>
            <div class="field_form">
                <label>Jenis Dokumen : </label>
                <select name="pedoman" class="required">
                    <option value="">Pilih</option>
                    {foreach $data_pedoman as $p}
                        <option value="{$p.ID_MM_JENIS_PEDOMAN}">{$p.NAMA_PEDOMAN} </option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Kode Lingkup: </label>
                <input type="text" name="kode" style="text-transform: uppercase" size="20" maxlength="20" class="required"/>
            </div>
            <div class="field_form">
                <label>Nama Lingkup : </label>
                <textarea name="isi" style="width: 60%" class="required"></textarea>
            </div>
            <div class="bottom_field_form">
                <a href="lingkup-dokumen.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Lingkup Dokumen Mutu  - Edit</div>
    <form action="lingkup-dokumen.php" method="post">
        <fieldset style="width: 80%">
            <legend>Edit Lingkup Dokumen Mutu </legend>
            <div class="field_form">
                <label>Jenis Dokumen : </label>
                <select name="pedoman" class="required">
                    <option value="">Pilih Pedoman</option>
                    {foreach $data_pedoman as $p}
                        <option value="{$p.ID_MM_JENIS_PEDOMAN}" {if $p.ID_MM_JENIS_PEDOMAN==$lingkup.ID_MM_JENIS_PEDOMAN}selected="true"{/if}>{$p.NAMA_PEDOMAN} </option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Kode Lingkup: </label>
                <input type="text" name="kode" style="text-transform: uppercase"  size="20" maxlength="20" class="required" value="{$lingkup.KODE_GROUP_DOKUMEN}"/>
            </div>
            <div class="field_form">
                <label>Nama Lingkup : </label>
                <textarea name="isi" style="width: 60%" class="required">{$lingkup.NAMA_GROUP_DOKUMEN}</textarea>
            </div>
            <div class="field_form">
                <label>Singkatan Lingkup: </label>
                <input type="text" name="singkatan" style="text-transform: uppercase"  size="10" maxlength="10" class="required" value="{$lingkup.SINGKATAN_LINGKUP}"/>
            </div>
            <div class="field_form">
                <label>Penanggung Jawab Lingkup: </label>
                <select name="jabatan" >
                    <option value="">Pilih Jabatan</option>
                    {foreach $data_jabatan as $p}
                        <option value="{$p.ID_JABATAN_PEGAWAI}" {if $p.ID_JABATAN_PEGAWAI==$lingkup.ID_JABATAN_LINGKUP}selected="true"{/if}>{$p.NM_JABATAN_PEGAWAI} </option>
                    {/foreach}
                </select>
            </div>
            <div class="bottom_field_form">
                <a href="lingkup-dokumen.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$lingkup.ID_MM_GROUP_DOKUMEN}" />
                <input type="hidden" name="mode" value="edit"/>
                <input type="submit" class="button" value="Update"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">Jenis Dokumen Mutu  - Group Hapus</div>
    <form action="lingkup-dokumen.php" method="post">
        <fieldset style="width: 80%">
            <legend>Hapus Jenis Dokumen Mutu  - Group</legend>
            <div class="field_form">
                <label>Nama : </label>
                {$group.NAMA_GROUP|upper}
            </div>
            <div class="bottom_field_form">
                Apakah anda yakin menghapus item ini ?<br/>
                <a href="lingkup-dokumen.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$group.ID_MM_SASARAN_GROUP}" />
                <input type="hidden" name="mode" value="delete"/>
                <input type="submit" class="button" value="Ya"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Lingkup Dokumen Mutu</div>
    <table style="width: 88%">
        <tr>
            <th>No</th>
            <th>Jenis Pedoman</th>
            <th>Kode</th>
            <th>Nama</th>
            <th style="width: 120px" class="center">Operasi</th>
        </tr>
        {foreach $data_lingkup as $l}
            <tr {if $l.HIDDEN==1}style="background-color: lightcoral"{/if}>
                <td>{$l@index+1}</td>
                <td>{$l.NAMA_PEDOMAN}</td>
                <td>{$l.KODE_GROUP_DOKUMEN|upper}</td>
                <td>{$l.NAMA_GROUP_DOKUMEN}</td>
                <td class="center">
                    {if $l.HIDDEN==1}
                        <form style="display: inline" method="post" action="lingkup-dokumen.php">
                            <input type="hidden" name="mode" value="unhide"/>
                            <input type="hidden" name="id" value="{$l.ID_MM_GROUP_DOKUMEN}"/>
                            <input type="submit" style="background-color: #008000" value="Restore" class="button"/>
                        </form>
                    {else}
                        <a class="button" href="lingkup-dokumen.php?mode=edit&id={$l.ID_MM_GROUP_DOKUMEN}">Edit</a>
                        <form style="display: inline" method="post" action="lingkup-dokumen.php">
                            <input type="hidden" name="mode" value="hide"/>
                            <input type="hidden" name="id" value="{$l.ID_MM_GROUP_DOKUMEN}"/>
                            <input type="submit" style="background-color: red" value="Hapus" class="button"/>
                        </form>
                    {/if}
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="5" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="5" class="center">
                <a href="lingkup-dokumen.php?mode=tambah" class="button">Tambah</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
        $('form').validate();
    </script>
{/literal}