<div class="center_title_bar">Pencarian Dokumen Mutu </div>
{if $smarty.get.mode=='cari'}
    <a href="cari-dokumen.php" class="button">Kembali</a>
    <table style="width: 90%">
        <tr>
            <th>NO</th>
            <th>NAMA DOKUMEN</th>
            <th>WAKTU UPDATE</th>
            <th>UPLOAD BY</th>
            <th>STATUS</th>
            <th>FILE</th>
        </tr>
        {foreach $data_file as $f}
            <tr>
                <td class="center">{$f@index+1}</td>
                <td>{$f.NAMA_DOKUMEN} ({$f.KODE_DOKUMEN|upper})</td>
                <td>{$f.WAKTU}</td>
                <td>{$f.NM_PENGGUNA}</td>
                <td class="center">{if $f.STATUS_PAKAI==1}<span style="color: green">Dipakai</span>{else}<span style="color: red">Tidak</span>{/if}</td>
                <td class="center">
                    <a class="button disable-ajax" href="file-dokumen/{$f.NAMA_FILE}">Download</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="6" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
    </table>
{else}
    <form action="cari-dokumen.php" method="get" id="add" >
        <fieldset style="width: 80%">
            <legend>Pencarian Dokumen Mutu </legend>
            <div class="field_form">
                <label style="width: 30%">Nama Dokumen/Kode Dokumen : </label>
                <input type="text" name="key" size="30" class="required"></textarea>
            </div>
            <div class="bottom_field_form">
                <input type="hidden" name="mode" value="cari"/>
                <input type="submit" class="button" value="Cari"/>
            </div>
        </fieldset>
    </form>
{/if}