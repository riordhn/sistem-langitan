{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Sasaran / Rencana Mutu Tambah</div>
    <form action="aspek-sm.php" method="post">
        <fieldset style="width: 80%">
            <legend>Tambah Sasaran / Rencana Mutu</legend>
            <div class="field_form">
                <label>Nama Group : </label>
                <select name="group" class="required">
                    <option value="">Pilih Group</option>
                    {foreach $data_group as $g}
                        <option value="{$g.ID_MM_SASARAN_GROUP}">{$g.NAMA_GROUP|upper} </option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Jenis : </label>
                <select name="jenis" class="required">
                    <option value="">Pilih</option>
                    <option value="1">Sasaran Mutu</option>
                    <option value="2">Rencana Mutu</option>
                </select>
            </div>
            <div class="field_form">
                <label>Deskripsi : </label>
                <textarea name="deskripsi" cols="40" class="required"></textarea>
            </div>
            <div class="bottom_field_form">
                <a href="aspek-sm.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Sasaran / Rencana Mutu Edit</div>
    <form action="aspek-sm.php" method="post">
        <fieldset style="width: 80%">
            <legend>Edit Sasaran / Rencana Mutu</legend>
            <div class="field_form">
                <label>Nama Group : </label>
                <select name="group" class="required">
                    <option value="">Pilih Group</option>
                    {foreach $data_group as $g}
                        <option value="{$g.ID_MM_SASARAN_GROUP}"  {if $aspek.ID_MM_SASARAN_GROUP==$g.ID_MM_SASARAN_GROUP}selected="true"{/if}>{$g.NAMA_GROUP|upper} </option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Jenis : </label>
                <select name="jenis" class="required">
                    <option value="">Pilih</option>
                    <option value="1" {if $aspek.JENIS==1}selected="true"{/if}>Sasaran Mutu</option>
                    <option value="2" {if $aspek.JENIS==2}selected="true"{/if}>Rencana Mutu</option>
                </select>
            </div>
            <div class="field_form">
                <label>Deskripsi : </label>
                <textarea name="deskripsi" cols="40">{$aspek.DESKRIPSI}</textarea>
            </div>
            <div class="bottom_field_form">
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" value="{$aspek.ID_MM_SASARAN}" />
                <a href="aspek-sm.php" class="button">Batal</a>
                <input type="submit" class="button" value="Update"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">Sasaran / Rencana Mutu Hapus</div>
    <form action="aspek-sm.php" method="post">
        <fieldset style="width: 80%">
            <legend>Hapus Sasaran / Rencana Mutu</legend>
            <div class="field_form">
                <label>Kode Program : </label>
                {$aspek.NAMA_GROUP|upper}
            </div>
            <div class="field_form">
                <label>Jenis : </label>
                {if $aspek.JENIS==1}Sasaran Mutu{else} Rencana Mutu {/if}
            </div>
            <div class="field_form">
                <label>Deskripsi : </label>
                {$aspek.DESKRIPSI}
            </div>
            <div class="bottom_field_form">
                Apakah anda yakin menghapus item ini ?<br/>
                <input type="hidden" name="mode" value="delete"/>
                <input type="hidden" name="id" value="{$aspek.ID_MM_SASARAN}" />
                <a href="aspek-sm.php" class="button">Batal</a>
                <input type="submit" class="button" value="Ya"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Sasaran / Rencana Mutu</div>
    <table style="width: 98%">
        <tr>
            <th>No</th>
            <th>Group</th>
            <th>Jenis</th>
            <th>Deskripsi</th>
            <th style="width: 120px" class="center">Operasi</th>
        </tr>
        {foreach $data_aspek as $a}
            <tr>
                <td>{$a@index+1}</td>
                <td>{$a.NAMA_GROUP|upper}</td>
                <td>{if $a.JENIS==1}Sasaran Mutu{else} Rencana Mutu {/if}</td>
                <td>{$a.DESKRIPSI}</td>
                <td>
                    <a class="button" href="aspek-sm.php?mode=edit&id={$a.ID_MM_SASARAN}">Edit</a>
                    <a class="button" href="aspek-sm.php?mode=delete&id={$a.ID_MM_SASARAN}">Delete</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="5" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="5" class="center">
                <a href="aspek-sm.php?mode=tambah" class="button">Tambah</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
        $('form').validate();
    </script>
{/literal}