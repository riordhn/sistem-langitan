{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Jenis Dokumen Mutu - Tambah</div>
    <form action="jenis-pedoman.php" method="post">
        <fieldset style="width: 80%">
            <legend>Tambah Dokumen Mutu</legend>
            <div class="field_form">
                <label>Isi Pedoman: </label>
                <textarea name="isi" class="required"></textarea>
            </div>
            <div class="bottom_field_form">
                <a href="jenis-pedoman.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Jenis Dokumen Mutu  - Edit</div>
    <form action="jenis-pedoman.php" method="post">
        <fieldset style="width: 80%">
            <legend>Edit Jenis Dokumen Mutu </legend>
            <div class="field_form">
                <label>Isi Pedoman: </label>
                <textarea style="text-transform: uppercase" name="isi" class="required">{$pedoman.NAMA_PEDOMAN}</textarea>
            </div>
            <div class="bottom_field_form">
                <a href="jenis-pedoman.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$pedoman.ID_MM_JENIS_PEDOMAN}" />
                <input type="hidden" name="mode" value="edit"/>
                <input type="submit" class="button" value="Update"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='hidden'}
    <div class="center_title_bar">Jenis Dokumen Mutu  - Group Hapus</div>
    <form action="jenis-pedoman.php" method="post">
        <fieldset style="width: 80%">
            <legend>Hapus Jenis Dokumen Mutu  - Group</legend>
            <div class="field_form">
                <label>Nama : </label>
                {$group.NAMA_GROUP|upper}
            </div>
            <div class="bottom_field_form">
                Apakah anda yakin menghilangkan item ini ?<br/>
                <a href="jenis-pedoman.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$group.ID_MM_SASARAN_GROUP}" />
                <input type="hidden" name="mode" value="delete"/>
                <input type="submit" class="button" value="Ya"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Jenis Dokumen Mutu</div>
    <table style="width: 88%">
        <tr>
            <th>No</th>
            <th>Isi</th>
            <th style="width: 120px" class="center">Operasi</th>
        </tr>
        {foreach $data_pedoman as $p}
            <tr {if $p.HIDDEN==1}style="background-color: lightcoral"{/if}>
                <td>{$p@index+1}</td>
                <td>{$p.NAMA_PEDOMAN|upper}</td>
                <td class="center" style="width: 200px">
                    {if $p.HIDDEN==1}
                        <form style="display: inline" method="post" action="jenis-pedoman.php">
                            <input type="hidden" name="mode" value="unhide"/>
                            <input type="hidden" name="id" value="{$p.ID_MM_JENIS_PEDOMAN}"/>
                            <input type="submit" style="background-color: #008000" value="Restore" class="button"/>
                        </form>
                    {else}
                        <a class="button" href="jenis-pedoman.php?mode=edit&id={$p.ID_MM_JENIS_PEDOMAN}">Edit</a>
                        <form style="display: inline" method="post" action="jenis-pedoman.php">
                            <input type="hidden" name="mode" value="hide"/>
                            <input type="hidden" name="id" value="{$p.ID_MM_JENIS_PEDOMAN}"/>
                            <input type="submit" style="background-color: red" value="Hapus" class="button"/>
                        </form>
                    {/if}
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="3" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="3" class="center">
                <a href="jenis-pedoman.php?mode=tambah" class="button">Tambah</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
        $('form').validate();
    </script>
{/literal}