<div class="center_title_bar">Pengisian Sasaran dan Rencana Mutu</div>
<form method="get" action="pengisian-sm.php" id="form_jenis">
    <table>
        <tr>
            <td>Pilih Pengisian</td>
            <td>
                <select name="jenis" onchange="$('#form_jenis').submit()">
                    <option value="">Pilih</option>
                    <option value="1" {if $smarty.get.jenis==1}selected="true"{/if}>Sasaran Mutu</option>
                    <option value="2" {if $smarty.get.jenis==2}selected="true"{/if}>Rencana Mutu</option>
                </select>
            </td>
        </tr>
    </table>
</form>
{if $smarty.get.jenis!=''}
    <div style="overflow: scroll;width: 1050px">
        <form action="pengisian-sm.php?{$smarty.server.QUERY_STRING}" method="post">
            {foreach $data_laporan as $l}
                {$index_data=1}
                {if $l.VALUE==$smarty.get.jenis}
                    <table 
                        {if $l.VALUE==2}
                            style="width: 100%;"
                        {else}
                            style="width: 90%;"
                        {/if}>
                        <caption>A.{$l.NAMA}</caption>
                        <tr>
                            <th rowspan="2">{$l.NAMA}</th>
                            <th rowspan="2">Base Line ({$tahun_baseline})</th>
                            <th colspan="2" class="center">Tahun {$tahun_capaian}</th>
                            <th colspan="4" class="center">Target</th>
                            <th rowspan="2">{if $l.VALUE==2}Metode {else }Cara {/if}Pengukuran</th>
                            {if $l.VALUE==2}
                                <th rowspan="2">Aktivitas Utama</th>    
                            {/if}
                            <th rowspan="2">Referensi</th>
                            {if $l.VALUE==2}
                                <th rowspan="2">Penganggung Jawab</th>
                                <th rowspan="2">Pelaksana</th>
                                <th rowspan="2">Monev</th>
                            {/if}
                        </tr>
                        <tr>
                            <th>Target</th>
                            <th>Capaian</th>
                            {foreach $tahun_target as $tt}
                                <th>{$tt}</th>
                            {/foreach}
                        </tr>
                        {foreach $l.GA as $g}
                            <tr>
                                <td style="background: gainsboro" colspan="{if $l.VALUE==2}14{else }10{/if}">{$g.NAMA_GROUP|upper}</td>
                            </tr>
                            {foreach $g.A as $a}
                                <input type="hidden" name="id_aspek{$index_data}" value="{$a.ASPEK.ID_MM_SASARAN}"/>
                                <tr>
                                    <td>{$a.ASPEK.DESKRIPSI}</td>
                                    <td></td>
                                    <td>
                                        <input type="text" name="t_sebelum{$index_data}" value="{$a.J_C.TARGET}" size="3" />
                                    </td>
                                    <td>
                                        <input type="text" name="c_sebelum{$index_data}" value="{$a.J_C.CAPAIAN}" size="3" />
                                    </td>
                                    <td>
                                        <input type="text" name="target_{$tahun_target[0]}_{$index_data}" value="{$a.J_T_1.TARGET}" size="3" />
                                    </td>
                                    <td>
                                        <input type="text" name="target_{$tahun_target[1]}_{$index_data}" value="{$a.J_T_2.TARGET}" size="3" />
                                    </td>
                                    <td>
                                        <input type="text" name="target_{$tahun_target[2]}_{$index_data}" value="{$a.J_T_3.TARGET}" size="3" />
                                    </td>
                                    <td>
                                        <input type="text" name="target_{$tahun_target[3]}_{$index_data}" value="{$a.J_T_4.TARGET}" size="3" />
                                    </td>
                                    <td>
                                        <textarea cols="10" name="ukur{$index_data}" rows="5" style="resize: none">{$a.J_C.PENGUKURAN}</textarea>
                                    </td>
                                    {if $l.VALUE==2}
                                        <td>
                                            <textarea cols="10" rows="5" name="aktivitas{$index_data}" style="resize: none">{$a.J_C.AKTIVITAS_UTAMA}</textarea>
                                        </td>    
                                    {/if}
                                    <td><textarea cols="10" rows="5" name="referensi{$index_data}" style="resize: none">{$a.J_C.REFERENSI}</textarea></td>
                                    {if $l.VALUE==2}
                                        <td>
                                            <textarea cols="8" rows="5" name="pj{$index_data}" style="resize: none">{$a.J_C.PJ}</textarea>
                                        </td>
                                        <td>
                                            <textarea cols="8" rows="5" name="pelaksana{$index_data}" style="resize: none">{$a.J_C.PELAKSANA}</textarea>                                        
                                        </td>
                                        <td>
                                            <input type="text"  size="8"  name="monev{$index_data}" value="{$a.J_C.MONEV}" />
                                        </td>
                                    {/if}
                                </tr>
                                {$index_data=$index_data+1}
                            {/foreach}
                        {/foreach}
                        <tr>
                            <td class="center" colspan="{if $l.VALUE==2}14{else}10{/if}">
                                <input type="submit" value="Simpan" class="button"/>
                                <input type="hidden" name="mode" value="simpan"/>
                                <input type="hidden" name="jumlah_data" value="{$index_data}"/>
                            </td>
                        </tr>
                    </table>
                {/if}
            {/foreach}
        </form>
    </div>
{/if}