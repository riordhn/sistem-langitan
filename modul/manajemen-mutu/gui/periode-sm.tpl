{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Sasaran / Rencana Mutu  Periode Tambah</div>
    <form action="periode-sm.php" method="post">
        <fieldset style="width: 80%">
            <legend>Tambah Periode Sasaran / Rencana Mutu</legend>
            <div class="field_form">
                <label>Tahun Periode : </label>
                <input type="text" name="tahun" size="4" class="required number" />
            </div>
            <div class="field_form">
                <label>Awal Periode : </label>
                <input type="text" name="awal" size="10"  class="datepicker" />
            </div>
            <div class="field_form">
                <label>Akhir Periode : </label>
                <input type="text" name="akhir" size="10"  class="datepicker" />
            </div>
            <div class="bottom_field_form">
                <a href="periode-sm.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Sasaran / Rencana Mutu  Periode Edit</div>
    <form action="periode-sm.php" method="post">
        <fieldset style="width: 80%">
            <legend>Edit Periode Sasaran / Rencana Mutu </legend>
            <div class="field_form">
                <label>Tahun Periode : </label>
                <input type="text" name="tahun" class="required number" value="{$periode.TAHUN_PERIODE}" />
            </div>
            <div class="field_form">
                <label>Awal Periode : </label>
                <input type="text" name="awal" size="10" class="datepicker" value="{$periode.TGL_AWAL}" />
            </div>
            <div class="field_form">
                <label>Akhir Periode : </label>
                <input type="text" name="akhir" size="10"  class="datepicker" value="{$periode.TGL_AKHIR}" />
            </div>
            <div class="bottom_field_form">
                <a href="periode-sm.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$periode.ID_MM_SASARAN_PERIODE}"/>
                <input type="hidden" name="mode" value="edit"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">Sasaran / Rencana Mutu Periode - Hapus</div>
    <form action="periode-sm.php" method="post">
        <fieldset style="width: 80%">
            <legend>Delete Periode Sasaran / Rencana Mutu </legend>
            <div class="field_form">
                <label>Tahun Periode : </label>
                {$periode.TAHUN_PERIODE}
            </div>
            <div class="field_form">
                <label>Awal Periode : </label>
                {$periode.TGL_AWAL}
            </div>
            <div class="field_form">
                <label>Akhir Periode : </label>
                {$periode.TGL_AKHIR}
            </div>
            <div class="bottom_field_form">
                Apakah anda yakin menghapus item ini ?<br/>
                <a href="periode-sm.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$periode.ID_MM_SASARAN_PERIODE}"/>
                <input type="hidden" name="mode" value="delete"/>
                <input type="submit" class="button" value="Ya"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Sasaran / Rencana Mutu  Periode</div>
    <table style="width: 88%">
        <tr>
            <th>No</th>
            <th>Tahun</th>
            <th>Awal Periode</th>
            <th>Akhir Periode</th>
            <th style="width: 120px" class="center">Operasi</th>
        </tr>
        {foreach $data_periode as $pr}
            <tr>
                <td>{$pr@index+1}</td>
                <td>{$pr.TAHUN_PERIODE}</td>
                <td>{$pr.TGL_AWAL}</td>
                <td>{$pr.TGL_AKHIR}</td>
                <td>
                    <a class="button" href="periode-sm.php?mode=edit&id={$pr.ID_MM_SASARAN_PERIODE}">Edit</a>
                    <a class="button" href="periode-sm.php?mode=delete&id={$pr.ID_MM_SASARAN_PERIODE}">Delete</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="5" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="5" class="center">
                <a href="periode-sm.php?mode=tambah" class="button">Tambah</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
        $('form').validate();
        $( ".datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat : 'dd-M-y'
        }).css({'text-transform':'uppercase'});
    </script>
{/literal}