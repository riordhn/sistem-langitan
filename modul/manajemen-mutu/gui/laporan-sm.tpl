<div class="center_title_bar">Laporan Sasaran dan Rencana Mutu</div>
<form method="get" action="laporan-sm.php" id="form_jenis">
    <table>
        <tr>
            <td>Pilih Laporan</td>
            <td>
                <select name="jenis" onchange="$('#form_jenis').submit()">
                    <option value="">Pilih</option>
                    <option value="1" {if $smarty.get.jenis==1}selected="true"{/if}>Sasaran Mutu</option>
                    <option value="2" {if $smarty.get.jenis==2}selected="true"{/if}>Rencana Mutu</option>
                </select>
            </td>
        </tr>
    </table>
</form>
{if $smarty.get.jenis!=''}
    {foreach $data_laporan as $l}
        {if $l.VALUE==$smarty.get.jenis}
            <table 
                {if $l.VALUE==2}
                    style="width: 100%;"
                {else}
                    style="width: 90%;"
                {/if}>
                <caption>A.{$l.NAMA}</caption>
                <tr>
                    <th rowspan="2">{$l.NAMA}</th>
                    <th rowspan="2">Base Line ({$tahun_baseline})</th>
                    <th colspan="2" class="center">Tahun {$tahun_capaian}</th>
                    <th colspan="4" class="center">Target</th>
                    <th rowspan="2">{if $l.VALUE==2}Metode {else }Cara {/if}Pengukuran</th>
                    {if $l.VALUE==2}
                        <th rowspan="2">Aktivitas Utama</th>    
                    {/if}
                    <th rowspan="2">Referensi</th>
                    {if $l.VALUE==2}
                        <th rowspan="2">Penganggung Jawab</th>
                        <th rowspan="2">Pelaksana</th>
                        <th rowspan="2">Monev</th>
                    {/if}
                </tr>
                <tr>
                    <th>Target</th>
                    <th>Capaian</th>
                    {foreach $tahun_target as $tt}
                        <th>{$tt}</th>
                    {/foreach}
                </tr>
                {foreach $l.GA as $g}
                    <tr>
                        <td style="background: gainsboro" colspan="{if $l.VALUE==2}14{else }10{/if}">{$g.NAMA_GROUP|upper}</td>
                    </tr>
                    {foreach $g.A as $a}
                        <tr>
                            <td>{$a.ASPEK.DESKRIPSI}</td>
                            <td>{$a.J_B.TARGET}</td>
                            <td>{$a.J_C.TARGET}</td>
                            <td>{$a.J_C.CAPAIAN}</td>
                            <td>{$a.J_T_1.TARGET}</td>
                            <td>{$a.J_T_2.TARGET}</td>
                            <td>{$a.J_T_3.TARGET}</td>
                            <td>{$a.J_T_4.TARGET}</td>
                            <td>{$a.J_C.PENGUKURAN}</td>
                            {if $l.VALUE==2}
                                <td>{$a.J_C.AKTIVITAS_UTAMA}</td>    
                            {/if}
                            <td>{$a.J_C.REFERENSI}</td>
                            {if $l.VALUE==2}
                                <td>{$a.J_C.PJ}</td>
                                <td>{$a.J_C.PELAKSANA}</td>
                                <td>{$a.J_C.MONEV}</td>
                            {/if}
                        </tr>
                    {/foreach}
                {/foreach}
            </table>
        {/if}
    {/foreach}
{/if}