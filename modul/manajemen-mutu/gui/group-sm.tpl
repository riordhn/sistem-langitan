{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Sasaran / Rencana Mutu - Group Tambah</div>
    <form action="group-sm.php" method="post">
        <fieldset style="width: 80%">
            <legend>Tambah Sasaran / Rencana Mutu  - Group</legend>
            <div class="field_form">
                <label>Nama Group: </label>
                <input type="text" size="70" style="text-transform: uppercase" maxlength="100" name="nama" class="required" />
            </div>
            <div class="bottom_field_form">
                <a href="group-sm.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Sasaran / Rencana Mutu  - Group Edit</div>
    <form action="group-sm.php" method="post">
        <fieldset style="width: 80%">
            <legend>Edit Sasaran / Rencana Mutu  - Group</legend>
            <div class="field_form">
                <label>Nama Group : </label>
                <input type="text" size="70"  style="text-transform: uppercase" maxlength="100" name="nama" value="{$group.NAMA_GROUP}" class="required" />
            </div>
            <div class="bottom_field_form">
                <a href="group-sm.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$group.ID_MM_SASARAN_GROUP}" />
                <input type="hidden" name="mode" value="edit"/>
                <input type="submit" class="button" value="Update"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">Sasaran / Rencana Mutu  - Group Hapus</div>
    <form action="group-sm.php" method="post">
        <fieldset style="width: 80%">
            <legend>Hapus Sasaran / Rencana Mutu  - Group</legend>
            <div class="field_form">
                <label>Nama : </label>
                {$group.NAMA_GROUP|upper}
            </div>
            <div class="bottom_field_form">
                Apakah anda yakin menghapus item ini ?<br/>
                <a href="group-sm.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$group.ID_MM_SASARAN_GROUP}" />
                <input type="hidden" name="mode" value="delete"/>
                <input type="submit" class="button" value="Ya"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Sasaran / Rencana Mutu  - Group</div>
    <table style="width: 68%">
        <tr>
            <th>No</th>
            <th>Nama Group</th>
            <th style="width: 120px" class="center">Operasi</th>
        </tr>
        {foreach $data_group as $g}
            <tr>
                <td>{$g@index+1}</td>
                <td>{$g.NAMA_GROUP|upper}</td>
                <td>
                    <a class="button" href="group-sm.php?mode=edit&id={$g.ID_MM_SASARAN_GROUP}">Edit</a>
                    <a class="button" href="group-sm.php?mode=delete&id={$g.ID_MM_SASARAN_GROUP}">Delete</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="3" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="3" class="center">
                <a href="group-sm.php?mode=tambah" class="button">Tambah</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
        $('form').validate();
    </script>
{/literal}