<?php

include 'config.php';
include 'class/dm.class.php';
$id_pt = $id_pt_user;

$dm = new dm($db,$id_pt);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $dm->tambah_lingkup_dokumen(strtoupper(post('kode')), post('pedoman'), post('isi'));
    } else if (post('mode') == 'edit') {
        $dm->update_lingkup_dokumen(post('id'), post('kode'), post('pedoman'), post('isi'), post('singkatan'), post('jabatan'));
    } else if (post('mode') == 'hide') {
        $dm->hide_lingkup_dokumen(post('id'));
    } else if (post('mode') == 'unhide') {
        $dm->unhide_lingkup_dokumen(post('id'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit' || get('mode') == 'delete') {
        $smarty->assign('lingkup', $dm->get_lingkup_dokumen(get('id')));
    }
}

$smarty->assign('data_jabatan', $db->QueryToArray("SELECT * FROM JABATAN_PEGAWAI ORDER BY NM_JABATAN_PEGAWAI"));
$smarty->assign('data_pedoman', $dm->load_jenis_pedoman());
$smarty->assign('data_lingkup', $dm->load_lingkup_dokumen());
$smarty->display('lingkup-dokumen.tpl');
