<?php

function alert_error($text) {
    return "<div id='alert' class='ui-widget' style='margin:10px 0px;'>
        <div class='ui-state-error ui-corner-all' style='padding: 5px;width:80%;'> 
            <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span> 
                {$text}</p>
        </div>
    </div>";
}

function alert_success($text) {
    return "<div id='alert' class='ui-widget' style='margin:10px 0px;'>
        <div class='ui-state-highlight ui-corner-all' style='padding: 5px;width:80%;'> 
            <p><span class='ui-icon ui-icon-check' style='float: left; margin-right: .3em;'></span> 
                {$text}</p>
        </div>
    </div>";
}

?>
