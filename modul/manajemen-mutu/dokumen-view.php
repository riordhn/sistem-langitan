<?php
include('../../config.php');
require_once("php/lib/config.php");
require_once("php/lib/common.php");

$configManager = new Config();

if (base64_decode($_GET['key']) == 'nambisembilu_cybercampus') {
    $file = $_GET['file'];
    $id_pengguna = $_GET['data'];
    $id_dokumen = $_GET['dok'];
    $cek_sudah_baca = $db->QuerySingle("SELECT COUNT(*) FROM KOMPLAIN_BACA_PROSEDUR WHERE ID_PENGGUNA='{$id_pengguna}' AND ID_MM_DOKUMEN='{$id_dokumen}'");
    if ($cek_sudah_baca == 0 && $id_pengguna != '') {
        $query = "INSERT INTO KOMPLAIN_BACA_PROSEDUR (ID_MM_DOKUMEN,ID_PENGGUNA) VALUES ('{$id_dokumen}','{$id_pengguna}')";
        $db->Query($query);
    }
} else {
    die('Maaf Akses Di Blokir, Hanya Bisa Di Buka Melalui Aplikasi Cybercampus Unair');
}
?>

<!doctype html>
<html>
    <head>
        <title>Dokumen Manajemen <?= $_GET['l'] ?> - <?= $_GET['t'] ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width" />
        <style type="text/css" media="screen">
            html, body	{ height:100%; }
            body { margin:0px; padding:0; overflow:auto; }
            #flashContent { display:none; }
        </style>

        <link rel="stylesheet" type="text/css" href="css/flexpaper.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/flexpaper.js"></script>
        <script type="text/javascript" src="js/flexpaper_handlers.js"></script>
    </head>
    <body>
        <div style="margin: 0px auto;width: 100%">
            <div id="documentViewer" class="flexpaper_viewer" style="width:100%;height: 650px"></div>
            <script type="text/javascript" src="reader.js"></script>
            <script type="text/javascript">
                function getDocumentUrl(document) {
                    return "php/services/view.php?doc={doc}&format={format}&page={page}".replace("{doc}", document);
                }

                function getDocQueryServiceUrl(document) {
                    return "php/services/swfsize.php?doc={doc}&format={format}&page={page}".replace("{doc}", document);
                }

                var startDocument = ReadMyFile('<?= $file ?>');

                $('#documentViewer').FlexPaperViewer(
                        {config: {
                                DOC: escape(getDocumentUrl(startDocument)),
                                Scale: 0.6,
                                ZoomTransition: 'easeOut',
                                ZoomTime: 0.5,
                                ZoomInterval: 0.2,
                                FitPageOnLoad: false,
                                FitWidthOnLoad: true,
                                FullScreenAsMaxWindow: false,
                                ProgressiveLoading: false,
                                MinZoomSize: 0.2,
                                MaxZoomSize: 5,
                                SearchMatchAll: false,
                                InitViewMode: 'Portrait',
                                RenderingOrder: '<?php echo ($configManager->getConfig('renderingorder.primary') . ',' . $configManager->getConfig('renderingorder.secondary')) ?>',
                                ViewModeToolsVisible: true,
                                ZoomToolsVisible: true,
                                NavToolsVisible: true,
                                CursorToolsVisible: true,
                                SearchToolsVisible: true,
                                DocSizeQueryService: 'php/services/swfsize.php?doc=' + startDocument,
                                jsDirectory: 'js/',
                                localeDirectory: 'locale/',
                                JSONDataType: 'jsonp',
                                key: '<?php echo $configManager->getConfig('licensekey') ?>',
                                localeChain: 'en_US'

                            }}
                );
            </script>
        </div>

        <!-- THE FOLLOWING CODE BLOCK CAN SAFELY BE REMOVED, IT IS ONLY PLACED HERE TO HELP YOU GET STARTED. -->

    </div>
</body>
</html>