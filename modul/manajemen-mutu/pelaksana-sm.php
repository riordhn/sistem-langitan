<?php

include 'config.php';
include 'class/sm.class.php';

$sm = new sm($db);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $sm->tambah_group_sm(post('nama'));
    } else if (post('mode') == 'edit') {
        $sm->update_group_sm(post('id'), post('nama'));
    } else if (post('mode') == 'delete') {
        $sm->delete_group_sm(post('id'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit' || get('mode') == 'delete') {
        $smarty->assign('group', $sm->get_group_sm(get('id')));
    }
}

$smarty->assign('data_group', $sm->load_group_sm());
$smarty->display('pelaksana-sm.tpl');
?>
