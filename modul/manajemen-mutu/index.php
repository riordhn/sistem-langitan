<?php
include 'config.php';

if ($user->Role() == AUCC_ROLE_MANAJEMEN_MUTU)
{
    // data menu dari $user
    $smarty->assign('modul_set', $user->MODULs);

    $smarty->display("index.tpl");
}
else
{
    header("location: /logout.php");
    exit();
}
?>
