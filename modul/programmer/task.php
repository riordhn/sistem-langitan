<?php
include 'config.php';
include 'class/Programmer.class.php';

$mode = get('mode', 'view');

$Programmer = new Programmer($db);

if ($request_method == 'GET' && $mode == 'view')
{
    $smarty->assign('task_list', $Programmer->GetTaskList());
	$smarty->display("task/view.tpl");
}else if($request_method == 'GET' && $mode == 'edit')
{
	$id = get('id_task');
	$smarty->assign('taskListById', $Programmer->GetTaskListById($id));
	$smarty->display("task/edit.tpl");
}else if($request_method == 'GET' && $mode == 'add')
{
	if(isset($_GET['id_programmer'])){
		$id = get('id_programmer');
		$smarty->assign('programmer_set', $Programmer->GetListById($id));
	}else{
		$smarty->assign('programmer_set', $Programmer->GetList());
	}
	$smarty->display("task/add.tpl");
}
else if($request_method == 'GET' && $mode == 'delete')
{
	$id = get('id_task');
	$Programmer->deleteTaskById($id);
    $smarty->assign('task_list', $Programmer->GetTaskList());
	$smarty->display("task/view.tpl");
}
else if($request_method == 'GET' && $mode == 'finish')
{
	$id = get('id_task');
	$Programmer->UpdateFinishTaskListById($id);
    $smarty->assign('task_list', $Programmer->GetTaskList());
	$smarty->display("task/view.tpl");
}
else if($request_method == 'GET' && $mode == 'progress')
{
		$id = get('id_task');
		$smarty->assign('id', $id);
		$smarty->assign('progress', $Programmer->GetProgressByTaskId($id));
		$smarty->assign('programmer_info', $Programmer->GetProgrammerInfoByTaskId($id));
		
		if(isset($_GET['act'])){
			$smarty->display("progress/add.tpl");
		}else{
			$smarty->display("task/progress.tpl");
		}
}
else if($request_method == 'GET' && $mode == 'programmer')
{
		$id = get('id_programmer');
		$smarty->assign('id', $id);
		
		$smarty->assign('task_list', $Programmer->GetTaskByProgrammerId($id));
		
		$smarty->display("programmer/task.tpl");
}

if(isset($_POST['isi_task']))
{
	$id = post('id');
	$isi = post('isi_task');
	$urgensi = post('urgensi');
	$tglSelesai = post('tglSelesai');
	$status = post('status');
	$ket = post('keterangan');
	$Programmer->UpdateTaskListById($id, $isi, $urgensi, $tglSelesai, $status, $ket);
    $smarty->assign('task_list', $Programmer->GetTaskList());
	$smarty->display("task/view.tpl");
	
}else if(isset($_POST['tambah'])){
	$pid = post('pid');
	$isi = post('isi');
	$urgensi = post('urgensi');
	$deadline = post('deadLine');
	$ket = post('keterangan');
	$Programmer->insertTask($pid, $isi, $urgensi, $deadline, $ket);
    $smarty->assign('task_list', $Programmer->GetTaskList());
	$smarty->display("task/view.tpl");
}
else if(isset($_POST['progress'])){
		
	$id = get('id_task');
	$keterangan = post('keterangan');
	$progress = post('progress');
	
	$Programmer->insertProgress($id, $progress, $keterangan);
	
	$smarty->assign('id', $id);
	$smarty->assign('progress', $Programmer->GetProgressByTaskId($id));
	$smarty->assign('programmer_info', $Programmer->GetProgrammerInfoByTaskId($id));
	$smarty->display("task/progress.tpl");

}


?>
