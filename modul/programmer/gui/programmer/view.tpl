<div class="center_title_bar">Daftar Programmer</div>

<table>
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Email</th>
    </tr>
    {foreach $programmer_set as $p}
    <tr>
        <td class="center">{$p@index + 1}</td>
        <td><a  href="task.php?mode=programmer&id_programmer={$p.ID_PROGRAMMER}">{$p.NM_PENGGUNA}</a></td>
        <td>{$p.EMAIL_PROGRAMMER}</td>
    </tr>
    {/foreach}
</table>