<div class="center_title_bar">Tugas Per Programmer</div>

<table>
    <tr>
        <th>No</th>
        <th>Programmer</th>
        <th>Tugas</th>
        <th>Urgensi</th>
        <th>Dead Line</th>
		 <th>Tanggal Selesai</th>
        <th>Status</th>
		<th>Keterangan</th>
        <th style="width: 150px">Aksi</th>
    </tr>
	{foreach $task_list as $data}
    <tr>
        <td class="center">{$data@index+1}</td>
        <td>{$data.NM_PENGGUNA}</td>
        <td><a href="task.php?mode=progress&id_task={$data.ID_TASK}">{$data.ISI_TASK}</a></td>
        <td class="center">{$data.URGENSI}</td>
        <td class="center"><strong>{$data.TGL_DEADLINE}</strong></td>
		<td class="center"><strong>{$data.TGL_SELESAI}</strong></td>
        <td>
			{if $data.STATUS=='0'}
			BELUM SELESAI
			{else}
			SELESAI
			{/if}
		</td>
		<td>
			{$data.KETERANGAN}
		</td>
        <td class="center">
            <a href="{page_name()}?mode=edit&id_task={$data.ID_TASK}">Edit</a> |
            <a href="{page_name()}?mode=delete&id_task={$data.ID_TASK}">Hapus</a> |
            <a href="{page_name()}?mode=finish&id_task={$data.ID_TASK}">Selesai</a><br/>
            <a href="{page_name()}?mode=email&id_task={$data.ID_TASK}">Kirim Email</a>
        </td>
    </tr>
	{/foreach}
    <tr>
        <td colspan="9" class="center"><a href="{page_name()}?mode=add&id_programmer={$id}">Tambah</a></td>
    </tr>
</table>