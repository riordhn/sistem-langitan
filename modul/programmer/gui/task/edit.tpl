<div class="center_title_bar">Edit Task Programmer</div>

<form id="formEdit" name="formEdit" action="task.php" method="post">
	<table>
		<tr>
			<th>No</th>
			<th>Programmer</th>
			<th>Isi</th>
			<th>Urgensi</th>
			<th>Dead Line</th>
			<th>Tanggal Selesai</th>
			<th>Keterangan</th>
			<th>aksi</th>
		</tr>
		{foreach $taskListById as $data}
		
		<tr>
			<td class="center">1</td>
			<td>{$data.NM_PENGGUNA}</td>
			<td>
				<textarea name="isi_task" id="isi_task">
					{$data.ISI_TASK}
				</textarea>
			</td>
			<td class="center">
				<textarea id="urgensi" name="urgensi" cols="" rows="">
					{$data.URGENSI}
				</textarea>
			</td>
			<td class="center"><strong>{$data.TGL_DEADLINE}</strong></td>
			<td>
				<input type="text" style="width:75px;" name="tglSelesai" value="{$data.TGL_SELESAI}" id="tglSelesai" />
				<input type="hidden" name="id" id="id" value="{$data.ID_TASK}" />
			</td>
			<td>
				<textarea name="keterangan" id="keterangan">
				{$data.KETERANGAN}
				</textarea>
			</td>
			<td colspan="7" class="center">
			<input type="submit" value="simpan" name="submit" />
			</td>
		</tr>
		
		{/foreach}
	</table>
</form>

<script>
{literal}
jQuery('#tglSelesai').datepicker({'showAnim':'fadeIn','minDate':'0','dateFormat':'dd-mm-yy'});
{/literal}
</script>
<script>
	$(function(){
		$('textarea').autosize();
	});
</script>
