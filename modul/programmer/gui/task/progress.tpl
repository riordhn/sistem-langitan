<div class="center_title_bar">Progress Tugas Programmer</div>

{foreach $programmer_info as $p}
<div style="float:left;width:150;">
	<img src="{$p.FOTO}" style="height:150px;width:120px;"/>
</div>
<div>
	<table>
		<tr>
			<td>Tugas </td> <td> {$p.ISI_TASK} </td>
		</tr>
		<tr>
			<td>Urgensi </td> <td> {$p.URGENSI} </td>
		</tr>
		<tr>
			<td>Tanggal Deadline </td> <td> {$p.TGL_DEADLINE} </td>
		</tr>
		<tr>
			<td>Tanggal Selesai </td> <td> {$p.TGL_SELESAI} </td>
		</tr>
		<tr>
			<td>Status </td> <td> {$p.STATUS} </td>
		</tr>
		<tr>
			<td>Keterangan </td> <td> {$p.KETERANGAN} </td>
		</tr>
	</table>
</div>
<div style="clear:both">
	&nbsp;
</div>
{/foreach}
<hr>

<table>
	<tr>
		<th>
			No.
		</th>
		<th>
			Progrress.
		</th>
		<th>
			Tanggal
		</th>
		<th>
			Keterangan
		</th>
	</tr>
	{foreach $progress as $data}
	<tr>
		<td>
			{$data@index+1}
		</td>
		<td>
			{$data.PROGRESS}
		</td>
		<td>
			{$data.TANGGAL}
		</td>
		<td>
			{$data.KETERANGAN}
		</td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="5">
			<center>
			<a href="task.php?mode=progress&act=tambah&id_task={$id}"> Tambah </a>
			</center>
		</td>
	</tr>
</table>
