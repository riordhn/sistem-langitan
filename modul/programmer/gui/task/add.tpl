<div class="center_title_bar">Tambah Task Programmer</div>

<form id="formAdd" name="formAdd" action="task.php" method="post">
	<table>
		<tr>
			<th>No</th>
			<th>Programmer</th>
			<th>Tugas</th>
			<th>Urgensi</th>
			<th>Dead Line</th>
			<th>Keterangan</th>
			<th>aksi</th>
		</tr>
		
		
		<tr>
			<td class="center">1</td>
			<td>
				<select name="pid" id="pid">
					{foreach $programmer_set as $p}
					<option value="{$p.ID_PROGRAMMER}">
					{$p.NM_PENGGUNA}
					</option>
					{/foreach}
				</select>
			</td>
			<td>
				<textarea name="isi" cols="16" rows="1"/></textarea>
			</td>
			<td class="center">
				<input name="urgensi" type="text" value="" />
			</td>
			<td class="center">
			<input type="text" name="deadLine" id="deadLine" />
			</td>
			<td>
				<textarea name="keterangan" cols="16" rows="1"/></textarea>
			</td>
			<td colspan="7" class="center">
				<input type="submit" value="tambah" name="submit" />
				<input type="hidden" value="tambah" name="tambah" />
			</td>
		</tr>
	</table>
</form>


<script>
{literal}
jQuery('#deadLine').datepicker({'showAnim':'fadeIn','minDate':'0','dateFormat':'dd-mm-yy'});
{/literal}
</script>
<script>
	$(function(){
		$('textarea').autosize();
	});
</script>
