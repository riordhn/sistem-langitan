<?php
/**
 * Description of Programmer
 *
 * @author User
 */
class Programmer
{
    private $db;
    
    function __construct(MyOracle $db)
    {
        $this->db = $db;
    }
    
    function GetList()
    {
        return $this->db->QueryToArray("
            select pro.id_programmer, p.id_pengguna, nm_pengguna, email_programmer from pengguna p
            join programmer pro on pro.id_pengguna = p.id_pengguna
            order by p.nm_pengguna");
    }
	
    function GetListById($id)
    {
        return $this->db->QueryToArray("
            select pro.id_programmer, p.id_pengguna, nm_pengguna, email_programmer from pengguna p
            join programmer pro on pro.id_pengguna = p.id_pengguna
			where pro.id_programmer = '$id'
            ");
    }
    
    function GetTaskList()
    {
        return $this->db->QueryToArray("
            select t.id_task, nm_pengguna, isi_task, urgensi, to_char(tgl_deadline,'DD-MM-YYYY') tgl_deadline, to_char(tgl_selesai, 'DD-MM-YYYY') tgl_selesai, status, keterangan
            from programmer_task t
            join programmer pro on pro.id_programmer = t.id_programmer
            join pengguna p on p.id_pengguna = pro.id_pengguna
            order by tgl_deadline");
    }
	
    function GetTaskListById($id)
    {
        return $this->db->QueryToArray("
            select t.id_task, nm_pengguna, isi_task, urgensi, to_char(tgl_deadline,'DD-MM-YYYY') tgl_deadline, to_char(tgl_selesai, 'DD-MM-YYYY') tgl_selesai, status, keterangan
            from programmer_task t
			
            join programmer pro on pro.id_programmer = t.id_programmer
            join pengguna p on p.id_pengguna = pro.id_pengguna
			where t.id_task = '$id'
            order by tgl_deadline");
    }
	
    function GetTaskByProgrammerId($id)
    {
        return $this->db->QueryToArray("
            select t.id_task, nm_pengguna, isi_task, urgensi, to_char(tgl_deadline,'DD-MM-YYYY') tgl_deadline, to_char(tgl_selesai, 'DD-MM-YYYY') tgl_selesai, status, keterangan
            from programmer_task t
			
            join programmer pro on pro.id_programmer = t.id_programmer
            join pengguna p on p.id_pengguna = pro.id_pengguna
			where t.id_programmer = '$id'
            order by tgl_deadline");
    }
	
	function GetProgrammerInfoByTaskId($id){
		return $this->db->QueryToArray("
			select pg.nm_pengguna, pg.username, pg.foto_pengguna||'/'||pg.username||'.JPG' as foto, pt.isi_task, pt.urgensi, to_char(pt.tgl_deadline, 'dd-mm-yyyy') as tgl_deadline, to_char(pt.tgl_selesai, 'dd-mm-yyyy') as tgl_selesai, pt.status, pt.keterangan from programmer_task pt
			left join programmer p on p.id_programmer = pt.id_programmer
			left join pengguna pg on pg.id_pengguna = p.id_pengguna
			where pt.id_task = '$id'
		");
	}
	
	function GetProgressByTaskId($id){
		return $this->db->QueryToArray("
			select pp.progress, pp.keterangan, pp.tanggal from programmer_progress pp
			left join programmer_task pt on pt.id_task = pp.id_task
			where pt.id_task = '$id'
		");	
	}
	
	function UpdateTaskListById($id, $isi, $urgensi, $tglSelesai, $status, $ket)
	{
		$this->db->Query("update programmer_task set isi_task = '$isi', urgensi = '$urgensi', tgl_selesai = to_date('$tglSelesai','dd-mm-yyyy'), status = '$status', keterangan = '$ket' where id_task = '$id'");
	}
	
	function UpdateFinishTaskListById($id)
	{
		$this->db->Query("update programmer_task set status='1', tgl_selesai = sysdate where id_task = '$id'");
	}
	
	function insertTask($pid, $isi, $urgensi, $deadline, $ket)
	{
		$this->db->Query("insert into programmer_task (id_programmer, isi_task, urgensi, tgl_deadline, status, keterangan) values ('$pid', '$isi', '$urgensi', to_date('$deadline', 'dd-mm-yyyy'), '0', '$ket')");
	}
	
	function insertProgress($id, $progress, $keterangan){
		$this->db->Query("insert into programmer_progress (id_task, progress, keterangan, tanggal) values ('$id', '$progress', '$keterangan', sysdate)");
	}
	
	function deleteTaskById($id)
	{
		$this->db->Query("delete from programmer_task where id_task = '$id'");
	}
}

?>
