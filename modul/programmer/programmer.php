<?php
include 'config.php';
include 'class/Programmer.class.php';

$mode = get('mode', 'view');
$Programmer = new Programmer($db);

if ($request_method == 'GET' && $mode == 'view')
{
    $smarty->assign('programmer_set', $Programmer->GetList());
}

$smarty->display("programmer/{$mode}.tpl");
?>
