<?php
include('../../config.php');

$no_ujian = get('no_ujian','');

if ($request_method == 'POST')
{   
    $id_c_mhs = post('id_c_mhs');
    $db->Query("SELECT COUNT(*) as JML_BAYAR FROM PEMBAYARAN_CMHS WHERE ID_C_MHS = {$id_c_mhs} AND TGL_BAYAR IS NOT NULL");
    $row = $db->FetchAssoc();
    
    if ($row['JML_BAYAR'] == 0)
    {
        $db->Parse("UPDATE PEMBAYARAN_CMHS SET TGL_BAYAR = :tgl_bayar WHERE ID_C_MHS = :id_c_mhs");
        $db->BindByName(':tgl_bayar', post('tgl_bayar'));
        $db->BindByName(':id_c_mhs', post('id_c_mhs'));
        $db->Execute();
    }
}

if ($no_ujian != '')   
{
    $db->Query("
            SELECT ID_C_MHS, NM_C_MHS, NM_PROGRAM_STUDI FROM CALON_MAHASISWA CM
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI
            WHERE NO_UJIAN = '{$no_ujian}'");
    $cm = $db->FetchAssoc();
    
    if ($cm)
    {
        $smarty->assign('cm', $cm);
        
        $pc_set = $db->QueryToArray("
            SELECT ID_PEMBAYARAN_CMHS, NM_BIAYA, PC.BESAR_BIAYA, NM_BANK, NAMA_BANK_VIA, TGL_BAYAR, NO_TRANSAKSI, IS_TAGIH FROM PEMBAYARAN_CMHS PC
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PC.ID_DETAIL_BIAYA
            JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
            LEFT JOIN BANK BN ON BN.ID_BANK = PC.ID_BANK
            LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA = PC.ID_BANK_VIA
            WHERE ID_C_MHS = {$cm['ID_C_MHS']}");
        $smarty->assign('pc_set', $pc_set);
        
        $smarty->assign('current_date', date('d-M-Y'));
    }
}

$smarty->display('biaya/calon.tpl');
?>
