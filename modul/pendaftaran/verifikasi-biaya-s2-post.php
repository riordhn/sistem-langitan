<?php
include('../../config.php');

if ($request_method == 'POST')
{
    $post_keys = array_keys($_POST);
    
    $db->BeginTransaction();
    
    foreach ($post_keys as $pk)
    {
        // pilih yg terkena cek
        if (substr($pk, 0, 6) == 'check_')
        {
            $id_c_mhs = str_replace("check_", "", $pk);
            $id_program_studi = post("program_studi_{$id_c_mhs}");
            $id_jalur = post("jalur_{$id_c_mhs}");
            $id_kelompok_biaya = post("kelompok_biaya_{$id_c_mhs}");
            
            $sudah_dibayar = false;
            
            if ($id_kelompok_biaya != '')
            {
                // cek kondisi pembayaran calon mahasiswa apakah sudah dibayar atau belum
                $db->Query("SELECT COUNT(*) JUMLAH_BAYAR FROM PEMBAYARAN_CMHS WHERE ID_C_MHS = {$id_c_mhs} AND TGL_BAYAR IS NOT NULL");
                $pembayaran_cmhs = $db->FetchAssoc();
                if ($pembayaran_cmhs['JUMLAH_BAYAR'] > 0)
                {
                    $sudah_dibayar = true;
                }
                
                if ($sudah_dibayar == false)
                {   
                    // hardcode
                    $id_semester = 5;
                    
                    // Cek biaya dari tabel biaya kuliah
                    $db->Query("
                        SELECT ID_BIAYA_KULIAH FROM BIAYA_KULIAH
                        WHERE ID_PROGRAM_STUDI = {$id_program_studi} AND ID_JALUR = {$id_jalur} AND ID_KELOMPOK_BIAYA = {$id_kelompok_biaya} AND ID_SEMESTER = {$id_semester}");
                    $biaya_kuliah = $db->FetchAssoc();
                    
                    if ($biaya_kuliah)
                    {    
                        // hapus dari biaya kuliah cmhs dan pembayaran cmhs
                        $db->Query("DELETE FROM BIAYA_KULIAH_CMHS WHERE ID_C_MHS = {$id_c_mhs}");
                        $db->Query("DELETE FROM PEMBAYARAN_CMHS WHERE ID_C_MHS = {$id_c_mhs}");
                        
                        // ambil detail biaya kuliah
                        $detail_biaya_set = array();
                        $db->Query("SELECT ID_DETAIL_BIAYA, BESAR_BIAYA FROM DETAIL_BIAYA WHERE ID_BIAYA_KULIAH = {$biaya_kuliah['ID_BIAYA_KULIAH']}");
                        while ($detail_biaya = $db->FetchAssoc())
                            array_push($detail_biaya_set, $detail_biaya);
                        
                        // insert biaya kuliah
                        $db->Query("INSERT INTO BIAYA_KULIAH_CMHS (ID_C_MHS, ID_BIAYA_KULIAH) VALUES ({$id_c_mhs}, {$biaya_kuliah['ID_BIAYA_KULIAH']})");
                        
                        // insert pembayaran
                        foreach ($detail_biaya_set as $detail_biaya)
                        {
                            $db->Query("
                                INSERT INTO PEMBAYARAN_CMHS (ID_C_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, BESAR_BIAYA, IS_TAGIH)
                                VALUES ({$id_c_mhs},{$id_semester},{$detail_biaya['ID_DETAIL_BIAYA']},{$detail_biaya['BESAR_BIAYA']},'Y')");
                        }
                        
                        // update prodi, jalur, kelompok biaya ke mahasiswa
                        $db->Query("UPDATE CALON_MAHASISWA SET ID_PROGRAM_STUDI = {$id_program_studi}, ID_JALUR = {$id_jalur}, SP3 = {$id_kelompok_biaya} WHERE ID_C_MHS = {$id_c_mhs}");
                    }
                }
            }
        }
    }
    
    $db->Commit();
}
header('Location: verifikasi-biaya-s2.php');
?>
