<?php

include('config.php');
include('class/kesehatan.class.php');

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN) {
    $kesehatan = new kesehatan($db);
    $mode = get('mode', 'view');
    if (get('parameter') != '') {
        if (get('parameter') == 'kelompok') {
            $smarty->assign('data_hasil_tes', $kesehatan->load_data_hasil_kesehatan(get('kelompok'), null, null));
            $smarty->assign('kelompok', get('kelompok'));
        } else if (get('parameter') == 'hasil_tes') {
            $smarty->assign('data_hasil_tes', $kesehatan->load_data_hasil_kesehatan(null, get('hasil_tes'), null));
            $smarty->assign('hasil_tes', get('hasil_tes'));
        } else if (get('parameter') == 'tgl_tes') {
            $smarty->assign('data_hasil_tes', $kesehatan->load_data_hasil_kesehatan(null, null, get('tanggal_tes')));
            $smarty->assign('tgl_tes', get('tgl_tes'));
        } else if (get('parameter') == 'fakultas') {
            $smarty->assign('data_hasil_tes', $kesehatan->load_data_hasil_kesehatan_fakultas(get('fakultas')));
            $smarty->assign('fakultas', get('fakultas'));
        } else {
            $smarty->assign('data_hasil_tes', $kesehatan->load_data_hasil_kesehatan(null, null, null));
            $smarty->assign('semua', get('semua'));
        }
    }
    $smarty->assign('data_fakultas', $kesehatan->load_fakultas());
    $smarty->assign('data_tanggal_jadwal_kesehatan', $kesehatan->load_tanggal_jadwal_kesehatan());
    $smarty->assign('data_jadwal_kesehatan', $kesehatan->load_jadwal_kesehatan());
    $smarty->display("kesehatan/view/{$mode}.tpl");
} else {
    echo "<h2>Khusus area pegawai Test Kesehatan.</h2>";
};
?>
