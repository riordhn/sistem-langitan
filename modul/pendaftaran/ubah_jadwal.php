<?php

include_once '../../config.php';
include_once 'class/du_jadwal.class.php';

$jadwal = new du_jadwal($db);

if (isset($_GET['no_ujian'])) {
    $smarty->assign('data_mahasiswa', $jadwal->load_jadwal_calon_mahasiswa($_GET['no_ujian']));
    $smarty->assign('jadwal_kesehatan', $jadwal->jadwal_kesehatan());
    $smarty->assign('jadwal_elpt', $jadwal->jadwal_elpt());
    $smarty->assign('no_ujian', $_GET['no_ujian']);
}

if (isset($_POST['mode'])) {
    $jadwal->update_jadwal($_POST['no_ujian'], $_POST['jadwal_kesehatan'], $_POST['jadwal_elpt']);
    echo '<p></p><p><a class="success">Data Berhasil Kami Simpan</a></p>';
}

$smarty->display('verifikasi/ubah-jadwal.tpl');
?>
