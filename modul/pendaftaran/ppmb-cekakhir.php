<?php
include('config.php');

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN && $user->ID_UNIT_KERJA == 43) {
    
$no_ujian = get('no_ujian', '');

$db->Query("
    SELECT CM.NO_UJIAN, CM.NM_C_MHS, JP.TGL_TEST, CM.KODE_VOUCHER, CM.KODE_JURUSAN, CM.ID_JALUR, JP.LOKASI, JP.ALAMAT, JP.NM_RUANG
    FROM CALON_MAHASISWA CM
    LEFT JOIN JADWAL_PPMB JP ON JP.ID_JADWAL_PPMB = CM.ID_JADWAL_PPMB
    WHERE CM.STATUS_PRA_UJIAN = 1 AND CM.NO_UJIAN = '{$no_ujian}'");
    
$cm = $db->FetchAssoc();

if ($cm)
{
    $cm['TGL_TEST'] = strftime('%d %B %Y', date_create($cm['TGL_TEST'])->getTimestamp());
    $smarty->assign('cm', $cm);
}

$smarty->display('ppmb/verifikasi/cekakhir.tpl');
    
} else { echo "<h2>Khusus area Panitia PPMB.</h2>"; };
?>
