<?php

include('config.php');
include('class/elpt.class.php');
$elpt = new elpt($db);

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN) {

    $mode = get('mode', 'view');

    $ruang_toefl_table = new RUANG_TOEFL_TABLE($db);
    $jadwal_toefl_table = new JADWAL_TOEFL_TABLE($db);

    if ($request_method == 'POST') {
        if (post('mode') == 'add') {
            $jadwal_toefl = new JADWAL_TOEFL();
            $jadwal_toefl->ID_RUANG_TOEFL = post('id_ruang_toefl');
            $jadwal_toefl->TGL_TEST = date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year')));
            $jadwal_toefl->JAM_MULAI = post('mulai_Hour');
            $jadwal_toefl->MENIT_MULAI = post('mulai_Minute');
            $jadwal_toefl->TERISI = 0;
            $jadwal_toefl_table->Insert($jadwal_toefl);
        } else if (post('mode') == 'edit') {
            $jadwal_toefl = $jadwal_toefl_table->Single(post('id_jadwal_toefl'));
            $jadwal_toefl->ID_RUANG_TOEFL = post('id_ruang_toefl');
            $jadwal_toefl->TGL_TEST = date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year')));
            $jadwal_toefl->JAM_MULAI = post('mulai_Hour');
            $jadwal_toefl->MENIT_MULAI = post('mulai_Minute');
            $jadwal_toefl_table->Update($jadwal_toefl);
        } else if (post('mode') == 'delete') {
            $elpt->delete_jadwal_elpt(post('id_jadwal_toefl'));
        }
    }

    if ($mode == 'view') {
        $jadwal_toefl_set = $jadwal_toefl_table->SelectCriteria("ORDER BY ID_JADWAL_TOEFL");
        $ruang_toefl_table->FillJadwalToeflSet($jadwal_toefl_set);
        $smarty->assign('jadwal_toefl_set', $jadwal_toefl_set);
    } else if ($mode == 'add') {
        $smarty->assign('ruang_toefl_set', $ruang_toefl_table->SelectCriteria("ORDER BY NM_RUANG"));
    } else if ($mode == 'edit') {
        $jadwal_toefl = $jadwal_toefl_table->Single(get('id_jadwal_toefl'));
        $smarty->assign('jadwal_toefl', $jadwal_toefl);
        $smarty->assign('ruang_toefl_set', $ruang_toefl_table->SelectCriteria("ORDER BY NM_RUANG"));
    } else if ($mode == 'delete') {
        $jadwal_toefl = $jadwal_toefl_table->Single(get('id_jadwal_toefl'));
        $ruang_toefl_table->FillJadwalToefl($jadwal_toefl);
        $smarty->assign('jadwal_toefl', $jadwal_toefl);
    } else if ($mode == 'data_cm') {
        $smarty->assign('data_jadwal_toefl', $elpt->load_jadwal_eltp(get('id_jadwal_toefl')));
        $smarty->assign('data_calon_mahasiswa', $elpt->load_c_mhs_by_kelompok(get('id_jadwal_toefl')));
    }

    $smarty->display("toefl/jadwal/{$mode}.tpl");
} else {
    echo "<h2>Khusus area pegawai PINLABS.</h2>";
};
?>
