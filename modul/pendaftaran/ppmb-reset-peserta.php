<?php
include('config.php');

if ($request_method == 'POST')
{
    $id_c_mhs = post('id_c_mhs');
    $id_jadwal_ppmb = post('id_jadwal_ppmb');
    
    // update plot jadwal ppmb
    $db->Query("UPDATE PLOT_JADWAL_PPMB SET ID_C_MHS = NULL WHERE ID_C_MHS = {$id_c_mhs}");
    
    // update jadwal ppmb
    $db->Query("UPDATE JADWAL_PPMB SET TERISI = TERISI - 1 WHERE ID_JADWAL_PPMB = {$id_jadwal_ppmb}");
    
    // update calon mahasiswa
    $db->Query("
        UPDATE CALON_MAHASISWA SET
            SP3_1 = NULL,
            SP3_2 = NULL,
            SP3_3 = NULL,
            SP3_4 = NULL,

            BERKAS_IJAZAH = 0,
            BERKAS_SKHUN = 0,
            BERKAS_KESANGGUPAN_SP3 = 0, 
            BERKAS_LAIN = 0,
            BERKAS_LAIN_KET = NULL,
        
            NO_UJIAN = NULL,
            ID_JADWAL_PPMB = NULL,

            STATUS_PRA_UJIAN = NULL

        WHERE ID_C_MHS = {$id_c_mhs}");
}

$kode_voucher = get('kode_voucher', '');

$db->Query("
    SELECT CM.ID_C_MHS, NO_UJIAN, NM_C_MHS, NM_JALUR, ID_JADWAL_PPMB FROM CALON_MAHASISWA CM
    LEFT JOIN JALUR J ON J.ID_JALUR = CM.ID_JALUR
    WHERE KODE_VOUCHER = :kode_voucher");
$db->BindByName(':kode_voucher', $kode_voucher);
$db->Execute();

$cm = $db->FetchAssoc();
if ($cm)
{
    $smarty->assign('cm', $cm);
}

$smarty->display('ppmb/reset/peserta.tpl');
?>
