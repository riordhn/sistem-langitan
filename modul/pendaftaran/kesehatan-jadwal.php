<?php

include('config.php');
include('class/kesehatan.class.php');

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN) {

    $jadwal_kesehatan = new kesehatan($db);
    $mode = get('mode', 'view');

    if ($mode == 'edit') {
        $smarty->assign('data_jadwal_kesehatan', $jadwal_kesehatan->load_jadwal_kesehatan_by_id(get('id_jadwal')));
    } else if ($mode == 'delete') {
        $smarty->assign('data_jadwal_kesehatan', $jadwal_kesehatan->load_jadwal_kesehatan_by_id(get('id_jadwal')));
    } else {
        if ((post('mode')) == 'edit') {
            $jadwal_kesehatan->update_jadwal_kesehatan(post('id_jadwal'), post('kelompok_jam'), date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year'))), post('mulai_Hour'), post('mulai_Minute'), post('selesai_Hour'), post('selesai_Minute'), post('kapasitas'));
        } else if (post('mode') == 'add') {
            $jadwal_kesehatan->insert_jadwal_kesehatan(post('kelompok_jam'), date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year'))), post('mulai_Hour'), post('mulai_Minute'), post('selesai_Hour'), post('selesai_Minute'), post('kapasitas'));
        } else if (post('mode') == 'delete') {
            $jadwal_kesehatan->delete_jadwal_kesehatan(post('id_jadwal'));
        }
        $smarty->assign('data_ruang_kesehatan', $jadwal_kesehatan->load_ruang_kesehatan());
        $smarty->assign('data_jadwal_kesehatan', $jadwal_kesehatan->load_jadwal_kesehatan());
    }
    $smarty->display("kesehatan/jadwal/{$mode}.tpl");
} else {
    echo "<h2>Khusus area pegawai Test Kesehatan.</h2>";
};
?>
