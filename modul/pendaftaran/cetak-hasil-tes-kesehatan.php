<?php
include('../../config.php');
include_once('../../tcpdf/config/lang/ind.php');
include_once('../../tcpdf/tcpdf.php');
include_once('class/cetak.class.php');

$kesehatan = new cetak($db);

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$jadwal_kesehatan = $kesehatan->load_jadwal_kesehatan_by_id(get('kelompok'));
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .header { font-size: 20pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 14pt; font-family: serif; margin-top: 0px ;text-align:center; }
    td { font-size: 10pt; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="10%" align="right"><img src="../../img/maba/logounair.png" width="80px" height="80px"/></td>
        <td width="80%" align="center">
            <span class="header">UNIVERSITAS AIRLANGGA<br/></span>Hasil Pemeriksaan Kesehatan tanggal {tanggal} kelompok {kelompok} jam {jam_mulai}:{menit_mulai}<br/><span class="address">Kampus C Mulyorejo Surabaya 60115</span>
        </td>
    </tr>
</table>
<hr/>
<p><p/>
<p><p/>
<table cellpadding="2" border="0.5">
        <tr>
            <td>No</td>
            <td>No Ujian</td>
            <td>Nama</td>
            <td>Kesehatan Mata</td>
            <td>Kesehatan THT</td>
            <td>Kesimpulan Akhir</td>
        </tr>
    {data_mahasiswa}
</table>
<p><p/>

EOF;
$index = 1;

function get_hasil_tes($i) {
    if ($i == '1') {
        return 'Lulus';
    } else {
        return 'Tidak Lulus';
    }
}

$data_mahasiswa = '';
foreach ($kesehatan->load_calon_mahasiswa_by_id_jadwal(get('kelompok')) as $data) {
    $data_mahasiswa .= '<tr>
            <td>' . $index++ . '</td>
            <td>' . $data['NO_UJIAN'] . '</td>
            <td>' . $data['NM_C_MHS'] . '</td>
            <td>' . $data['KESEHATAN_KESIMPULAN_MATA'] . '</td>
            <td>' . $data['KESEHATAN_KESIMPULAN_THT'] . '</td>
            <td>' . get_hasil_tes($data['KESEHATAN_KESIMPULAN_AKHIR']) . '</td>
    </tr>';
}


$html = str_replace('{kelompok}', $jadwal_kesehatan['KELOMPOK_JAM'], $html);
$html = str_replace('{tanggal}', $jadwal_kesehatan['TGL_TEST'], $html);
$html = str_replace('{jam_mulai}', $jadwal_kesehatan['JAM_MULAI'], $html);
$html = str_replace('{menit_mulai}', $jadwal_kesehatan['MENIT_MULAI'], $html);
$html = str_replace('{data_mahasiswa}', $data_mahasiswa, $html);

$pdf->writeHTML($html);

$pdf->Output();
?>
