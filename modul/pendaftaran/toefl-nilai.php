<?php

include('config.php');
include 'class/elpt.class.php';

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN && $user->ID_UNIT_KERJA == 41) {
    $elpt = new elpt($db);
    $mode = get('mode', 'input');
    if (get('kelompok') != '') {
        $smarty->assign('kelompok', get('kelompok'));
        $smarty->assign('data_c_mhs_by_kelompok', $elpt->load_c_mhs_by_kelompok(get('kelompok')));
    } if (post('save') != '') {
        for ($i = 1; $i < post('jumlah'); $i++) {
            $elpt->update_nilai_eltp(post('id_c_mhs' . $i), post('listening' . $i), post('structure' . $i), post('reading' . $i), round((intval(post('listening' . $i)) + intval(post('structure' . $i)) + intval(post('reading' . $i))) * 10 / 3));
        }
    }
    $smarty->assign('data_jadwal_elpt', $elpt->load_jadwal_eltp());
    $smarty->display("toefl/nilai/{$mode}.tpl");
} else {
    echo "<h2>Khusus area pegawai PINLABS.</h2>";
};
?>
