<?php

include('config.php');

include_once('../../tcpdf/config/lang/ind.php');
include_once('../../tcpdf/tcpdf.php');
include_once 'class/cetak.class.php';
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$cetak = new cetak($db);

$data_mhs = $cetak->load_data_cetak_kesehatan(get('id_cmhs'));
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .header { font-size: 20pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 14pt; font-family: serif; margin-top: 0px ;text-align:center; }
    td { font-size: 10pt; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="10%" align="right"><img src="../../img/maba/logounair_hp.png" width="80px" height="80px"/></td>
        <td width="80%" align="center">
            <span class="header">UNIVERSITAS AIRLANGGA<br/>RUMAH SAKIT</span><br/><span class="address">Kampus C Mulyorejo Surabaya 60115</span>
        </td>
    </tr>
</table>
<hr/>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center">
            <h2>PEMERIKSAAN KESEHATAN CALON MAHASISWA</h2>
        </td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="10">
    <tr>
        <td width="20%"><b>PROGRAM STUDI</b></td>
        <td width="35%">: <b>{program_studi}</b></td>
        <td width="20%">Di periksa tgl</td>
        <td width="25%">:{tanggal}</td>
    </tr>
    <tr>
        <td><b>FAKULTAS</b></td>
        <td>: <b>{fakultas}</b></td>
        <td>Jam</td>
        <td>:{jam}</td>
    </tr>
    <tr>
        <td>Jalur Penerimaan</td>
        <td>: {jalur}</td>
        <td>No Test</td>
        <td>: ..........</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td><b>KELOMPOK TES</b></td>
        <td><b>: {kelompok_jam}</b></td>
    </tr>
</table>
<table cellpadding="1" width="100%">
    <tr>
        <td width="20%">Nama</td>
        <td width="80%">: {nama}</td>
    </tr>
    <tr>
        <td>No Ujian</td>
        <td>: {no_ujian}</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>: {alamat}</td>
    </tr>
    <tr>
        <td>Alamat di Surabaya</td>
        <td>: </td>
    </tr>
    <tr>
        <td>Telp</td>
        <td>: {telp}</td>
    </tr>
</table>
<p></p>
<table border="0" cellpadding="2" width="100%">
    <tr>
        <td colspan="2" bgcolor="#008" color="#ff0">PEMERIKSAAN MATA</td>
    </tr>
    <tr>
        <td width="40%">1. WARNA :</td>
        <td width="60%"></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Buta Warna Total</td>
        <td>
            <table width="50%">
                <tr>
                    <td align="center">YA</td>
                    <td></td>
                    <td align="center">TIDAK</td>
                </tr>
                <tr>
                    <td border="1" height="25px"></td>
                    <td></td>
                    <td border="1"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Buta Warna Parsial</td>
        <td>
            <table width="50%">
                <tr>
                    <td align="center">YA</td>
                    <td></td>
                    <td align="center">TIDAK</td>
                </tr>
                <tr>
                    <td border="1" height="25px"></td>
                    <td></td>
                    <td border="1"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>2. VISUS / Tajam Penglihatan :<br/>&nbsp;&nbsp;&nbsp;&nbsp;(Tanpa / dengan kacamata)</td>
        <td>
            <table width="50%">
                <tr>
                    <td align="center">OD</td>
                    <td></td>
                    <td align="center">OS</td>
                </tr>
                <tr>
                    <td border="1" height="25px"></td>
                    <td></td>
                    <td border="1"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>3. FUNDUS OKULI :</td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Papil N II</td>
        <td>
            <table width="50%">
                <tr>
                    <td align="center">NORMAL</td>
                    <td></td>
                    <td align="center">TIDAK</td>
                </tr>
                <tr>
                    <td border="1" height="25px"></td>
                    <td></td>
                    <td border="1"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Makula Fovea</td>
        <td>
            <table width="50%">
                <tr>
                    <td align="center">NORMAL</td>
                    <td></td>
                    <td align="center">TIDAK</td>
                </tr>
                <tr>
                    <td border="1" height="25px"></td>
                    <td></td>
                    <td border="1"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p></p>
<table cellpadding="1" width="100%">
    <tr>
        <td width="20%"><b>Kesimpulan</b></td>
        <td width="5%">:</td>
        <td width="75%">Memenuhi syarat / tidak memenuhi syarat*</td>
    </tr>
    <tr>
        <td>Catatan</td>
        <td>:</td>
        <td> ________________________________________________________ </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td> ________________________________________________________ </td>
    </tr>
    <tr>
        <td>Telp</td>
        <td>:</td>
        <td>telp</td>
    </tr>
</table>
<p></p>
<table width="100%">
    <tr>
        <td width="50%" align="center">PIC Departemen Ilmu Kesehatan Mata</td>
        <td width="50%" align="center">Pemeriksa</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td width="50%" align="center"> (...............................) </td>
        <td width="50%" align="center"> (...............................) </td>
    </tr>
</table>
EOF;

$html = str_replace('{program_studi}', ucwords(strtolower($data_mhs['NM_PROGRAM_STUDI'])), $html);
$html = str_replace('{fakultas}', strtoupper($data_mhs['NM_FAKULTAS']) , $html);
$html = str_replace('{tanggal}', $data_mhs['TGL_TEST'], $html);
$html = str_replace('{jam}', '[' . str_pad($data_mhs['JAM_MULAI'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($data_mhs['MENIT_MULAI'], 2, '0') . '] -' . ' [' . str_pad($data_mhs['JAM_SELESAI'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($data_mhs['MENIT_SELESAI'], 2, '0') . ']', $html);
$html = str_replace('{jalur}', $data_mhs['NM_JALUR'], $html);
$html = str_replace('{kelompok_jam}', $data_mhs['KELOMPOK_JAM'], $html);
$html = str_replace('{nama}', $data_mhs['NM_C_MHS'], $html);
$html = str_replace('{no_ujian}', $data_mhs['NO_UJIAN'], $html);
$html = str_replace('{alamat}', $data_mhs['ALAMAT'], $html);
$html = str_replace('{telp}', $data_mhs['TELP'], $html);


$pdf->AddPage();
$pdf->writeHTML($html);
$pdf->endPage();

$pdf->Output();
?>
