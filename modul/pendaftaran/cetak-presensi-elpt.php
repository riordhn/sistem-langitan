<?php
include('../../config.php');
include_once('../../tcpdf/config/lang/ind.php');
include_once('../../tcpdf/tcpdf.php');
include_once('class/elpt.class.php');

$cm = new elpt($db);

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$jadwal_elpt = $cm->load_jadwal_eltp(get('kelompok'));
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .header { font-size: 20pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 14pt; font-family: serif; margin-top: 0px ;text-align:center; }
    td { font-size: 10pt; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="10%" align="right"><img src="../../img/maba/logounair.png" width="80px" height="80px"/></td>
        <td width="80%" align="center">
            <span class="header">UNIVERSITAS AIRLANGGA<br/></span>Jadwal Absensi TEST ELPT tanggal {tanggal} {ruang} jam {jam_mulai}:{menit_mulai}<br/><span class="address">Kampus C Mulyorejo Surabaya 60115</span>
        </td>
    </tr>
</table>
<hr/>
<p><p/>
<p><p/>
<table cellpadding="5" border="0.5">
        <tr>
            <td width="30px">No</td>
            <td>No Ujian</td>
            <td width="160px">Nama</td>
            <td>Fakultas</td>
            <td>Program Studi</td>
            <td>Presensi</td>
        </tr>
    {data_mahasiswa}
</table>
EOF;
$index = 1;
$data_mahasiswa = '';
foreach ($cm->load_c_mhs_by_kelompok(get('kelompok')) as $data) {
    $data_mahasiswa .= '<tr>
            <td>' . $index++ . '</td>
            <td>' . $data['NO_UJIAN'] . '</td>
            <td>' . $data['NM_C_MHS'] . '</td>
            <td>' . $data['NM_FAKULTAS'] . '</td>
            <td>' . $data['NM_PROGRAM_STUDI'] . '</td>
            <td border="1" height="25px"></td>
    </tr>';
}
$html = str_replace('{tanggal}', $jadwal_elpt['TGL_TEST'], $html);
$html = str_replace('{ruang}', $jadwal_elpt['NM_RUANG'], $html);
$html = str_replace('{jam_mulai}', $jadwal_elpt['JAM_MULAI'], $html);
$html = str_replace('{menit_mulai}', $jadwal_elpt['MENIT_MULAI'], $html);
$html = str_replace('{data_mahasiswa}', $data_mahasiswa, $html);

$pdf->writeHTML($html);

$pdf->Output();
?>
