<?php
include('config.php');
include_once('../../tcpdf/config/lang/ind.php');
include_once('../../tcpdf/tcpdf.php');

$calon_mahasiswa_table = new CALON_MAHASISWA_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);
$fakultas_table = new FAKULTAS_TABLE($db);
$jenjang_table = new JENJANG_TABLE($db);
$kota_table = new KOTA_TABLE($db);
$agama_table = new AGAMA_TABLE($db);
$sekolah_table = new SEKOLAH_TABLE($db);
$provinsi_table = new PROVINSI_TABLE($db);

$calon_mahasiswa = $calon_mahasiswa_table->Single(get('id_c_mhs'));
$program_studi_table->FillCalonMahasiswa($calon_mahasiswa);
$fakultas_table->FillProgramStudi($calon_mahasiswa->PROGRAM_STUDI);
$jenjang_table->FillProgramStudi($calon_mahasiswa->PROGRAM_STUDI);
$kota_table->FillCalonMahasiswa($calon_mahasiswa);
$agama_table->FillCalonMahasiswa($calon_mahasiswa);
$sekolah_table->FillCalonMahasiswa($calon_mahasiswa);
$kota_table->FillCalonMahasiswa2($calon_mahasiswa); //ayah
$kota_table->FillCalonMahasiswa3($calon_mahasiswa); //ibu
$provinsi_table->FillKota($calon_mahasiswa->KOTA2);
$provinsi_table->FillKota($calon_mahasiswa->KOTA3);

// Replacement
if ($calon_mahasiswa->SP3 == 5) { $calon_mahasiswa->SP3 = "SP3b"; }
else if ($calon_mahasiswa->SP3 == 4) { $calon_mahasiswa->SP3 = "SP3a"; }
else if ($calon_mahasiswa->SP3 == 3) { $calon_mahasiswa->SP3 = "BEBAS SP3"; }
else if ($calon_mahasiswa->SP3 == 2) { $calon_mahasiswa->SP3 = "GAKIN"; }

if ($calon_mahasiswa->PENGHASILAN_ORTU == 1) { $calon_mahasiswa->PENGHASILAN_ORTU = "> Rp 7,5 jt"; }
else if ($calon_mahasiswa->PENGHASILAN_ORTU == 2) { $calon_mahasiswa->PENGHASILAN_ORTU = "Rp 2,5 - Rp 7,5 jt"; }
else if ($calon_mahasiswa->PENGHASILAN_ORTU == 3) { $calon_mahasiswa->PENGHASILAN_ORTU = "Rp 1,35 - Rp 2,5 jt"; }
else if ($calon_mahasiswa->PENGHASILAN_ORTU == 4) { $calon_mahasiswa->PENGHASILAN_ORTU = "< Rp 1,35 jt"; }

if ($calon_mahasiswa->PEKERJAAN_AYAH == 1) { $calon_mahasiswa->PEKERJAAN_AYAH = "Guru/Dosen"; }
else if ($calon_mahasiswa->PEKERJAAN_AYAH == 2) { $calon_mahasiswa->PEKERJAAN_AYAH = "PNS Bukan Guru/Dosen"; }
else if ($calon_mahasiswa->PEKERJAAN_AYAH == 3) { $calon_mahasiswa->PEKERJAAN_AYAH = "TNI / POLRI"; }
else if ($calon_mahasiswa->PEKERJAAN_AYAH == 4) { $calon_mahasiswa->PEKERJAAN_AYAH = "Guru / Dosen Swasta"; }
else if ($calon_mahasiswa->PEKERJAAN_AYAH == 5) { $calon_mahasiswa->PEKERJAAN_AYAH = "Karyawan Swasta"; }
else if ($calon_mahasiswa->PEKERJAAN_AYAH == 6) { $calon_mahasiswa->PEKERJAAN_AYAH = "Pedagang / Wiraswasta"; }
else if ($calon_mahasiswa->PEKERJAAN_AYAH == 7) { $calon_mahasiswa->PEKERJAAN_AYAH = "Petani / Nelayan"; }
else if ($calon_mahasiswa->PEKERJAAN_AYAH == 8) { $calon_mahasiswa->PEKERJAAN_AYAH = "Buruh"; }
else if ($calon_mahasiswa->PEKERJAAN_AYAH == 9) { $calon_mahasiswa->PEKERJAAN_AYAH = "Pensiunan PNS / TNI / POLRI"; }
else if ($calon_mahasiswa->PEKERJAAN_AYAH == 10) { $calon_mahasiswa->PEKERJAAN_AYAH = "Pensiunan K. Swasta"; }
else if ($calon_mahasiswa->PEKERJAAN_AYAH == 11) { $calon_mahasiswa->PEKERJAAN_AYAH = $calon_mahasiswa->PEKERJAAN_AYAH_LAIN; }

if ($calon_mahasiswa->PEKERJAAN_IBU == 1) { $calon_mahasiswa->PEKERJAAN_IBU = "Guru/Dosen"; }
else if ($calon_mahasiswa->PEKERJAAN_IBU == 2) { $calon_mahasiswa->PEKERJAAN_IBU = "PNS Bukan Guru/Dosen"; }
else if ($calon_mahasiswa->PEKERJAAN_IBU == 3) { $calon_mahasiswa->PEKERJAAN_IBU = "TNI / POLRI"; }
else if ($calon_mahasiswa->PEKERJAAN_IBU == 4) { $calon_mahasiswa->PEKERJAAN_IBU = "Guru / Dosen Swasta"; }
else if ($calon_mahasiswa->PEKERJAAN_IBU == 5) { $calon_mahasiswa->PEKERJAAN_IBU = "Karyawan Swasta"; }
else if ($calon_mahasiswa->PEKERJAAN_IBU == 6) { $calon_mahasiswa->PEKERJAAN_IBU = "Pedagang / Wiraswasta"; }
else if ($calon_mahasiswa->PEKERJAAN_IBU == 7) { $calon_mahasiswa->PEKERJAAN_IBU = "Petani / Nelayan"; }
else if ($calon_mahasiswa->PEKERJAAN_IBU == 8) { $calon_mahasiswa->PEKERJAAN_IBU = "Buruh"; }
else if ($calon_mahasiswa->PEKERJAAN_IBU == 9) { $calon_mahasiswa->PEKERJAAN_IBU = "Pensiunan PNS / TNI / POLRI"; }
else if ($calon_mahasiswa->PEKERJAAN_IBU == 10) { $calon_mahasiswa->PEKERJAAN_IBU = "Pensiunan K. Swasta"; }
else if ($calon_mahasiswa->PEKERJAAN_IBU == 11) { $calon_mahasiswa->PEKERJAAN_IBU = $calon_mahasiswa->PEKERJAAN_IBU_LAIN; }

if ($calon_mahasiswa->SKALA_PEKERJAAN_ORTU == 1) { $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = "Besar"; }
else if ($calon_mahasiswa->SKALA_PEKERJAAN_ORTU == 2) { $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = "Menengah"; }
else if ($calon_mahasiswa->SKALA_PEKERJAAN_ORTU == 3) { $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = "Kecil"; }
else if ($calon_mahasiswa->SKALA_PEKERJAAN_ORTU == 4) { $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = "Mikro"; }

if ($calon_mahasiswa->KEDIAMAN_ORTU == 1) { $calon_mahasiswa->KEDIAMAN_ORTU = "Mewah"; }
else if ($calon_mahasiswa->KEDIAMAN_ORTU == 2) { $calon_mahasiswa->KEDIAMAN_ORTU = "Sedang"; }
else if ($calon_mahasiswa->KEDIAMAN_ORTU == 3) { $calon_mahasiswa->KEDIAMAN_ORTU = "Rumah Sederhana"; }
else if ($calon_mahasiswa->KEDIAMAN_ORTU == 4) { $calon_mahasiswa->KEDIAMAN_ORTU = "Rumah Sangat Sederhana"; }

if ($calon_mahasiswa->LUAS_TANAH == 1) { $calon_mahasiswa->LUAS_TANAH = "> 200 m2"; }
else if ($calon_mahasiswa->LUAS_TANAH == 2) { $calon_mahasiswa->LUAS_TANAH = "100 - 200 m2"; }
else if ($calon_mahasiswa->LUAS_TANAH == 3) { $calon_mahasiswa->LUAS_TANAH = "45 - 100 m2"; }
else if ($calon_mahasiswa->LUAS_TANAH == 4) { $calon_mahasiswa->LUAS_TANAH = "< 45 m2"; }

if ($calon_mahasiswa->LUAS_BANGUNAN == 1) { $calon_mahasiswa->LUAS_BANGUNAN = "> 100 m2"; }
else if ($calon_mahasiswa->LUAS_BANGUNAN == 2) { $calon_mahasiswa->LUAS_BANGUNAN = "56 - 100 m2"; }
else if ($calon_mahasiswa->LUAS_BANGUNAN == 3) { $calon_mahasiswa->LUAS_BANGUNAN = "27 - 56 m2"; }
else if ($calon_mahasiswa->LUAS_BANGUNAN == 4) { $calon_mahasiswa->LUAS_BANGUNAN = "< 27 m2"; }

if ($calon_mahasiswa->NJOP == 1) { $calon_mahasiswa->NJOP = "> Rp 300 jt"; }
else if ($calon_mahasiswa->NJOP == 2) { $calon_mahasiswa->NJOP = "Rp 100 - Rp 300 jt"; }
else if ($calon_mahasiswa->NJOP == 3) { $calon_mahasiswa->NJOP = "Rp 50 - Rp 100 jt"; }
else if ($calon_mahasiswa->NJOP == 4) { $calon_mahasiswa->NJOP = "< Rp 50 jt"; }

if ($calon_mahasiswa->KENDARAAN_R4 == 1) { $calon_mahasiswa->KENDARAAN_R4 = "> 1"; }
else if ($calon_mahasiswa->KENDARAAN_R4 == 2) { $calon_mahasiswa->KENDARAAN_R4 = "1"; }
else if ($calon_mahasiswa->KENDARAAN_R4 == 3) { $calon_mahasiswa->KENDARAAN_R4 = "Tidak Punya"; }

if ($calon_mahasiswa->KENDARAAN_R2 == 1) { $calon_mahasiswa->KENDARAAN_R2 = "> 2"; }
else if ($calon_mahasiswa->KENDARAAN_R2 == 2) { $calon_mahasiswa->KENDARAAN_R2 = "2"; }
else if ($calon_mahasiswa->KENDARAAN_R2 == 3) { $calon_mahasiswa->KENDARAAN_R2 = "1"; }
else if ($calon_mahasiswa->KENDARAAN_R2 == 4) { $calon_mahasiswa->KENDARAAN_R2 = "Tidak Punya"; }

if ($calon_mahasiswa->LISTRIK == 1) { $calon_mahasiswa->LISTRIK = "450 VA"; }
else if ($calon_mahasiswa->LISTRIK == 2) { $calon_mahasiswa->LISTRIK = "900 VA"; }
else if ($calon_mahasiswa->LISTRIK == 3) { $calon_mahasiswa->LISTRIK = "1300 VA"; }
else if ($calon_mahasiswa->LISTRIK == 4) { $calon_mahasiswa->LISTRIK = ">= 2200 VA"; }

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    td { font-size: 26px; }
 </style>
<table border="1" cellpadding="3">
    <tr>
        <td align="center" style="background-color: #008" colspan="3"><h3 style="color: yellow">FORMULIR VERIFIKASI ADMINISTRASI</h3></td>
    </tr>
    <tr><td colspan="3"></td></tr>
    <tr>
        <td align="center" style="background-color: #008" colspan="3"><h4 style="color: yellow">IDENTITAS DIRI</h4></td>
    </tr>
    <tr>
        <td style="width: 20px; text-align: center;">1</td>
        <td style="width: 160px;">Nama CAMABA</td>
        <td style="width: auto">{nama}</td>
    </tr>
    <tr>
        <td align="center">2</td>
        <td>Nomor Ujian Tulis SNMPTN</td>
        <td>{no_ujian}</td>
    </tr>
    <tr>
        <td align="center">3</td>
        <td>Program Studi (Diterima)</td>
        <td>{program_studi}</td>
    </tr>
    <tr>
        <td align="center">4</td>
        <td>Nama Ayah</td>
        <td>{nama_ayah}</td>
    </tr>
    <tr>
        <td align="center">5</td>
        <td>Alamat Ayah</td>
        <td>{alamat_ayah}</td>
    </tr>
    <tr>
        <td align="center">6</td>
        <td>Nama Ibu</td>
        <td>{nama_ibu}</td>
    </tr>
    <tr>
        <td align="center">7</td>
        <td>Alamat Ibu</td>
        <td>{alamat_ibu}</td>
    </tr>
    <tr>
        <td align="center">8</td>
        <td>Jumlah Anggota Keluarga</td>
        <td>{jumlah_anggota_keluarga} orang, No KSK : {nomor_ksk}</td>
    </tr>
    <tr>
        <td align="center">9</td>
        <td>Sanggup Membayar SP3</td>
        <td>{sp3}</td>
    </tr>
    <tr>
        <td align="center">10</td>
        <td>Penghasilan Orang Tua / Bulan</td>
        <td>{penghasilan_ortu}</td>
    </tr>
    <tr>
        <td align="center" style="background-color: #008" colspan="3" width="auto"><h4 style="color: yellow">PEKERJAAN / PROFESI / JABATAN ORANG TUA</h4></td>
    </tr>
    <tr>
        <td style="width: 20px; text-align: center;">11</td>
        <td style="width: 160px;">Pekerjaan Ayah</td>
        <td style="width: auto">{pekerjaan_ayah}</td>
    </tr>
    <tr>
        <td style="text-align: center;">12</td>
        <td>Pekerjaan Ibu</td>
        <td>{pekerjaan_ibu}</td>
    </tr>
    <tr>
        <td align="center">13</td>
        <td>Skala Usaha / Pekerjaan</td>
        <td>{skala_pekerjaan_ortu}</td>
    </tr>
    <tr>
        <td align="center" style="background-color: #008" colspan="3" width="auto"><h4 style="color: yellow">KEDIAMAN DAN KENDARAAN KELUARGA</h4></td>
    </tr>
    <tr>
        <td style="width: 20px; text-align: center;">14</td>
        <td style="width: 160px;">Kediaman Orang Tua</td>
        <td style="width: auto">{kediaman_ortu}</td>
    </tr>
    <tr>
        <td align="center">15</td>
        <td>Luas Tanah Kediaman</td>
        <td>{luas_tanah}</td>
    </tr>
    <tr>
        <td align="center">16</td>
        <td>Luas Bangunan Kediaman</td>
        <td>{luas_bangunan}</td>
    </tr>
    <tr>
        <td align="center">17</td>
        <td>NJOP</td>
        <td>{njop}</td>
    </tr>
    <tr>
        <td align="center">18</td>
        <td>Kendaraan Bermotor R4</td>
        <td>{kendaraan_r4}</td>
    </tr>
    <tr>
        <td align="center">19</td>
        <td>Kendaraan Bermotor R2</td>
        <td>{kendaraan_r2}</td>
    </tr>
    <tr>
        <td align="center">20</td>
        <td>Daya Listrik (VA)</td>
        <td>{listrik}</td>
    </tr>
    <tr>
        <td align="center">21</td>
        <td>Kekayaan Lainya :</td>
        <td>{kekayaan_lain}</td>
    </tr>
    <tr>
        <td align="center">22</td>
        <td>Informasi Lainnya, sebutkan :</td>
        <td>{info_lain}</td>
    </tr>
    <tr>
        <td align="center" style="background-color: #008" colspan="3" width="auto"><h4 style="color: yellow">PERNYATAAN</h4></td>
    </tr>
    <tr>
        <td align="center">
        <font color="#f00"><b>Data dan informasi ini disampaikan dengan sesungguhnya dan sebenar-benarnya. Apabila ternyata data dan informasi
        ini tidak benar dan tidak lengkap, saya bersedia kehilangan dan atau melepaskan hak saya sebagai mahasiswa Universitas Airlangga
        selama 2 tahun pada semua program studi melalui program seleksi apapun.</b></font>
        <br/><br/><br/><br/>
        <table>
            <tr>
                <td>&nbsp;</td>
                <td align="center">{tgl_cetak}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center">Mengetahui,</td>
                <td align="center">Yang Menyatakan,</td>
            </tr>
            <tr>
                <td align="center">Orang Tua / Wali</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><font size="8px">(materai)</font></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center">_____________________</td>
                <td align="center">{nama}</td>
            </tr>
        </table>
        <br/>
        </td>
    </tr>
</table>
EOF;

$html = str_replace('{nama}', $calon_mahasiswa->NM_C_MHS, $html);
$html = str_replace('{no_ujian}', $calon_mahasiswa->NO_UJIAN, $html);
$html = str_replace('{program_studi}', $calon_mahasiswa->PROGRAM_STUDI->NM_PROGRAM_STUDI, $html);
$html = str_replace('{nama_ayah}', $calon_mahasiswa->NAMA_AYAH, $html);
$html = str_replace('{alamat_ayah}', $calon_mahasiswa->ALAMAT_AYAH, $html);
$html = str_replace('{nama_ibu}', $calon_mahasiswa->NAMA_IBU, $html);
$html = str_replace('{alamat_ibu}', $calon_mahasiswa->ALAMAT_IBU, $html);
$html = str_replace('{jumlah_anggota_keluarga}', $calon_mahasiswa->ANGGOTA_KELUARGA, $html);
$html = str_replace('{nomor_ksk}', $calon_mahasiswa->NOMOR_KSK, $html);
$html = str_replace('{sp3}', $calon_mahasiswa->SP3, $html);
$html = str_replace('{penghasilan_ortu}', $calon_mahasiswa->PENGHASILAN_ORTU, $html);

$html = str_replace('{pekerjaan_ayah}', $calon_mahasiswa->PEKERJAAN_AYAH, $html);
$html = str_replace('{pekerjaan_ibu}', $calon_mahasiswa->PEKERJAAN_IBU, $html);

$html = str_replace('{skala_pekerjaan_ortu}', $calon_mahasiswa->SKALA_PEKERJAAN_ORTU, $html);
$html = str_replace('{kediaman_ortu}', $calon_mahasiswa->KEDIAMAN_ORTU, $html);
$html = str_replace('{luas_tanah}', $calon_mahasiswa->LUAS_TANAH, $html);
$html = str_replace('{luas_bangunan}', $calon_mahasiswa->LUAS_BANGUNAN, $html);
$html = str_replace('{njop}', $calon_mahasiswa->NJOP, $html);
$html = str_replace('{kendaraan_r4}', $calon_mahasiswa->KENDARAAN_R4, $html);
$html = str_replace('{kendaraan_r2}', $calon_mahasiswa->KENDARAAN_R2, $html);
$html = str_replace('{listrik}', $calon_mahasiswa->LISTRIK, $html);
$html = str_replace('{kekayaan_lain}', $calon_mahasiswa->KEKAYAAN_LAIN, $html);
$html = str_replace('{info_lain}', $calon_mahasiswa->INFO_LAIN, $html);
$html = str_replace('{tgl_cetak}', strftime('%d %B %Y'), $html);

$pdf->writeHTML($html);

$pdf->Output();
?>
