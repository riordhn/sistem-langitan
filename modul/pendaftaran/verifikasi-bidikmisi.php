<?php
include('config.php');

if ($user->IsLogged() && $user->ID_UNIT_KERJA == 33) {

$calon_mahasiswa_table = new CALON_MAHASISWA_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);
$jenjang_table = new JENJANG_TABLE($db);
$kota_table = new KOTA_TABLE($db);
$pembayaran_cmhs_table = new PEMBAYARAN_CMHS_TABLE($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'bidikmisi')
    {
        // update tanggal bayar pada pembayaran calon mahasiswa
        $id_c_mhs = post('id_c_mhs');
        $pembayaran_cmhs_set = $pembayaran_cmhs_table->SelectWhere("ID_C_MHS = {$id_c_mhs}");
        
        for ($i = 0; $i < $pembayaran_cmhs_set->Count(); $i++)
            $pembayaran_cmhs_set->Get($i)->TGL_BAYAR = date('d-M-Y');
        
        $pembayaran_cmhs_table->UpdateSet($pembayaran_cmhs_set);
        
        // Update ke calon mahasiswa
        $calon_mahasiswa = $calon_mahasiswa_table->Single($id_c_mhs);
        $calon_mahasiswa->STATUS_BIDIK_MISI = post('status_bidik_misi');
        $calon_mahasiswa_table->Update($calon_mahasiswa);
    }
}

$no_ujian = get('no_ujian');
$calon_mahasiswa_set = $calon_mahasiswa_table->SelectWhere("NO_UJIAN = '{$no_ujian}'");

if ($calon_mahasiswa_set->Count() > 0)
{
    $calon_mahasiswa = $calon_mahasiswa_set->Get(0);
    $program_studi_table->FillCalonMahasiswa($calon_mahasiswa);
    $jenjang_table->FillProgramStudi($calon_mahasiswa->PROGRAM_STUDI);
    
    // Kota ayah & Ibu
    $calon_mahasiswa->KOTA_AYAH = $kota_table->Single($calon_mahasiswa->ID_KOTA_AYAH)->NM_KOTA;
    $calon_mahasiswa->KOTA_IBU = $kota_table->Single($calon_mahasiswa->ID_KOTA_IBU)->NM_KOTA;
    
    // Replacement
    if ($calon_mahasiswa->SP3 == 5) { $calon_mahasiswa->SP3 = "SP3b"; }
    else if ($calon_mahasiswa->SP3 == 4) { $calon_mahasiswa->SP3 = "SP3a"; }
    else if ($calon_mahasiswa->SP3 == 3) { $calon_mahasiswa->SP3 = "BEBAS SP3"; }
    else if ($calon_mahasiswa->SP3 == 2) { $calon_mahasiswa->SP3 = "GAKIN"; }

    if ($calon_mahasiswa->PENGHASILAN_ORTU == 1) { $calon_mahasiswa->PENGHASILAN_ORTU = "> Rp 7,5 jt"; }
    else if ($calon_mahasiswa->PENGHASILAN_ORTU == 2) { $calon_mahasiswa->PENGHASILAN_ORTU = "Rp 2,5 - Rp 7,5 jt"; }
    else if ($calon_mahasiswa->PENGHASILAN_ORTU == 3) { $calon_mahasiswa->PENGHASILAN_ORTU = "Rp 1,35 - Rp 2,5 jt"; }
    else if ($calon_mahasiswa->PENGHASILAN_ORTU == 4) { $calon_mahasiswa->PENGHASILAN_ORTU = "< Rp 1,35 jt"; }

    if ($calon_mahasiswa->PEKERJAAN_AYAH == 1) { $calon_mahasiswa->PEKERJAAN_AYAH = "Guru/Dosen"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 2) { $calon_mahasiswa->PEKERJAAN_AYAH = "PNS Bukan Guru/Dosen"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 3) { $calon_mahasiswa->PEKERJAAN_AYAH = "TNI / POLRI"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 4) { $calon_mahasiswa->PEKERJAAN_AYAH = "Guru / Dosen Swasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 5) { $calon_mahasiswa->PEKERJAAN_AYAH = "Karyawan Swasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 6) { $calon_mahasiswa->PEKERJAAN_AYAH = "Pedagang / Wiraswasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 7) { $calon_mahasiswa->PEKERJAAN_AYAH = "Petani / Nelayan"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 8) { $calon_mahasiswa->PEKERJAAN_AYAH = "Buruh"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 9) { $calon_mahasiswa->PEKERJAAN_AYAH = "Pensiunan PNS / TNI / POLRI"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 10) { $calon_mahasiswa->PEKERJAAN_AYAH = "Pensiunan K. Swasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 11) { $calon_mahasiswa->PEKERJAAN_AYAH = $calon_mahasiswa->PEKERJAAN_AYAH_LAIN; }

    if ($calon_mahasiswa->PEKERJAAN_IBU == 1) { $calon_mahasiswa->PEKERJAAN_IBU = "Guru/Dosen"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 2) { $calon_mahasiswa->PEKERJAAN_IBU = "PNS Bukan Guru/Dosen"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 3) { $calon_mahasiswa->PEKERJAAN_IBU = "TNI / POLRI"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 4) { $calon_mahasiswa->PEKERJAAN_IBU = "Guru / Dosen Swasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 5) { $calon_mahasiswa->PEKERJAAN_IBU = "Karyawan Swasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 6) { $calon_mahasiswa->PEKERJAAN_IBU = "Pedagang / Wiraswasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 7) { $calon_mahasiswa->PEKERJAAN_IBU = "Petani / Nelayan"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 8) { $calon_mahasiswa->PEKERJAAN_IBU = "Buruh"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 9) { $calon_mahasiswa->PEKERJAAN_IBU = "Pensiunan PNS / TNI / POLRI"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 10) { $calon_mahasiswa->PEKERJAAN_IBU = "Pensiunan K. Swasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 11) { $calon_mahasiswa->PEKERJAAN_IBU = $calon_mahasiswa->PEKERJAAN_IBU_LAIN; }

    if ($calon_mahasiswa->SKALA_PEKERJAAN_ORTU == 1) { $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = "Besar"; }
    else if ($calon_mahasiswa->SKALA_PEKERJAAN_ORTU == 2) { $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = "Menengah"; }
    else if ($calon_mahasiswa->SKALA_PEKERJAAN_ORTU == 3) { $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = "Kecil"; }
    else if ($calon_mahasiswa->SKALA_PEKERJAAN_ORTU == 4) { $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = "Mikro"; }

    if ($calon_mahasiswa->KEDIAMAN_ORTU == 1) { $calon_mahasiswa->KEDIAMAN_ORTU = "Mewah"; }
    else if ($calon_mahasiswa->KEDIAMAN_ORTU == 2) { $calon_mahasiswa->KEDIAMAN_ORTU = "Sedang"; }
    else if ($calon_mahasiswa->KEDIAMAN_ORTU == 3) { $calon_mahasiswa->KEDIAMAN_ORTU = "Rumah Sederhana"; }
    else if ($calon_mahasiswa->KEDIAMAN_ORTU == 4) { $calon_mahasiswa->KEDIAMAN_ORTU = "Rumah Sangat Sederhana"; }

    if ($calon_mahasiswa->LUAS_TANAH == 1) { $calon_mahasiswa->LUAS_TANAH = "> 200 m2"; }
    else if ($calon_mahasiswa->LUAS_TANAH == 2) { $calon_mahasiswa->LUAS_TANAH = "100 - 200 m2"; }
    else if ($calon_mahasiswa->LUAS_TANAH == 3) { $calon_mahasiswa->LUAS_TANAH = "45 - 100 m2"; }
    else if ($calon_mahasiswa->LUAS_TANAH == 4) { $calon_mahasiswa->LUAS_TANAH = "< 45 m2"; }

    if ($calon_mahasiswa->LUAS_BANGUNAN == 1) { $calon_mahasiswa->LUAS_BANGUNAN = "> 100 m2"; }
    else if ($calon_mahasiswa->LUAS_BANGUNAN == 2) { $calon_mahasiswa->LUAS_BANGUNAN = "56 - 100 m2"; }
    else if ($calon_mahasiswa->LUAS_BANGUNAN == 3) { $calon_mahasiswa->LUAS_BANGUNAN = "27 - 56 m2"; }
    else if ($calon_mahasiswa->LUAS_BANGUNAN == 4) { $calon_mahasiswa->LUAS_BANGUNAN = "< 27 m2"; }

    if ($calon_mahasiswa->NJOP == 1) { $calon_mahasiswa->NJOP = "> Rp 300 jt"; }
    else if ($calon_mahasiswa->NJOP == 2) { $calon_mahasiswa->NJOP = "Rp 100 - Rp 300 jt"; }
    else if ($calon_mahasiswa->NJOP == 3) { $calon_mahasiswa->NJOP = "Rp 50 - Rp 100 jt"; }
    else if ($calon_mahasiswa->NJOP == 4) { $calon_mahasiswa->NJOP = "< Rp 50 jt"; }

    if ($calon_mahasiswa->KENDARAAN_R4 == 1) { $calon_mahasiswa->KENDARAAN_R4 = "> 1"; }
    else if ($calon_mahasiswa->KENDARAAN_R4 == 2) { $calon_mahasiswa->KENDARAAN_R4 = "1"; }
    else if ($calon_mahasiswa->KENDARAAN_R4 == 3) { $calon_mahasiswa->KENDARAAN_R4 = "Tidak Punya"; }

    if ($calon_mahasiswa->KENDARAAN_R2 == 1) { $calon_mahasiswa->KENDARAAN_R2 = "> 2"; }
    else if ($calon_mahasiswa->KENDARAAN_R2 == 2) { $calon_mahasiswa->KENDARAAN_R2 = "2"; }
    else if ($calon_mahasiswa->KENDARAAN_R2 == 3) { $calon_mahasiswa->KENDARAAN_R2 = "1"; }
    else if ($calon_mahasiswa->KENDARAAN_R2 == 4) { $calon_mahasiswa->KENDARAAN_R2 = "Tidak Punya"; }

    if ($calon_mahasiswa->LISTRIK == 1) { $calon_mahasiswa->LISTRIK = "450 VA"; }
    else if ($calon_mahasiswa->LISTRIK == 2) { $calon_mahasiswa->LISTRIK = "900 VA"; }
    else if ($calon_mahasiswa->LISTRIK == 3) { $calon_mahasiswa->LISTRIK = "1300 VA"; }
    else if ($calon_mahasiswa->LISTRIK == 4) { $calon_mahasiswa->LISTRIK = ">= 2200 VA"; }
    
    $smarty->assign('calon_mahasiswa', $calon_mahasiswa);
}

$smarty->display('verifikasi/bidikmisi.tpl');

} else { echo "<h2>Khusus area pegawai Dit Kemahasiswaan.</h2>"; };?>
