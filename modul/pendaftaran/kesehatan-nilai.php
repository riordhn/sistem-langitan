<?php

include'config.php';
include 'class/kesehatan.class.php';

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN && $user->ID_UNIT_KERJA == 40) {
    $kesehatan = new kesehatan($db);
    $mode = get('mode', 'input');
    if ($mode == 'input') {
        if (get('jadwal_kesehatan') != '') {
            $smarty->assign('id_jadwal_kesehatan', get('jadwal_kesehatan'));
            $smarty->assign('data_c_mhs_by_kelompok', $kesehatan->load_calon_mahasiswa_by_id_jadwal(get('jadwal_kesehatan')));
            $smarty->assign('data_jadwal_kesehatan', $kesehatan->load_jadwal_kesehatan());
        } else {
            if (post('save') == '1') {
                for ($i = 1; $i < post('jumlah'); $i++) {
                    if (post('kesimpulan_mata' . $i) != null && post('kesimpulan_tht' . $i) != null && post('kesimpulan_akhir' . $i) != null) {
                        $kesehatan->update_data_kesehatan(post('no_ujian' . $i), post('kesimpulan_mata' . $i), post('kesimpulan_tht' . $i), post('kesimpulan_akhir' . $i), post('catatan' . $i));
                        write_log('Nilai Kesehatan CM ' . post('no_ujian' . $i) . ' Telah di update oleh ' . $user->ID_PENGGUNA . ' Di Rubah Menjadi =' . post('kesimpulan_akhir' . $i));
                    }
                }
            }
            $smarty->assign('data_jadwal_kesehatan', $kesehatan->load_jadwal_kesehatan());
        }
    }
    $smarty->display("kesehatan/nilai/{$mode}.tpl");
} else {
    echo "<h2>Khusus area pegawai Test Kesehatan.</h2>";
};
?>
