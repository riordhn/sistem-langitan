<?php

include'config.php';
include 'class/kesehatan.class.php';

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN && $user->ID_UNIT_KERJA == 40) {
    $kesehatan = new kesehatan($db);
    $mode = get('mode', 'view');
    if ((get('tanggal') != null) && (get('kelompok') == null)) {
        $smarty->assign('tanggal', get('tanggal'));
        $smarty->assign("data_jadwal_kesehatan_tanggal", $kesehatan->load_jadwal_kesehatan(get('tanggal')));
    } else if (get('tanggal') != null && get('kelompok') != null) {
        $smarty->assign('tanggal', get('tanggal'));
        $smarty->assign('kelompok', get('kelompok'));
        $smarty->assign("data_jadwal_kesehatan_tanggal", $kesehatan->load_jadwal_kesehatan(get('tanggal')));
        $smarty->assign('data_mahasiswa_kelompok', $kesehatan->load_calon_mahasiswa_by_kelompok(get('tanggal'), get('kelompok')));
    }

    $smarty->assign("data_jadwal_kesehatan", $kesehatan->load_tanggal_jadwal_kesehatan());
    $smarty->display("kesehatan/print/{$mode}.tpl");
} else {
    echo "<h2>Khusus area pegawai Test Kesehatan.</h2>";
};
?>
