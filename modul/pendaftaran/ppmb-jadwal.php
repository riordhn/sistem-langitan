<?php

include('config.php');
include('class/ppmb.class.php');

    $jadwal_ppmb = new ppmb($db);
    $mode = get('mode', 'view');
    if ($mode == 'edit') {
        $smarty->assign('data_jadwal_ppmb_by_id', $jadwal_ppmb->load_jadwal_ppmb_by_id(get('id_jadwal')));
    } else if ($mode == 'delete') {
        $smarty->assign('data_jadwal_ppmb_by_id', $jadwal_ppmb->load_jadwal_ppmb_by_id(get('id_jadwal')));
    } else if ($mode == 'aktifkan') {
        $jadwal = $jadwal_ppmb->load_jadwal_ppmb_by_id(get('id_jadwal'));
        $smarty->assign('no_awal', $jadwal_ppmb->get_no_awal($jadwal['GELOMBANG'], date('Y'), $jadwal['KODE_JALUR']));
        $smarty->assign('data_jadwal_ppmb_by_id', $jadwal);
    } else {
        if ((post('mode')) == 'edit') {
            if (post('gelombang') == '5') {
                $jadwal_ppmb->update_jadwal_ppmb(post('id_jadwal'), post('lokasi'), post('alamat'), post('ruang'), date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year'))), post('kapasitas'), '', post('gelombang'),  post('nomer_awal'));
            } else {
                $jadwal_ppmb->update_jadwal_ppmb(post('id_jadwal'), post('lokasi'), post('alamat'), post('ruang'), date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year'))), post('kapasitas'), post('kode_jalur'), post('gelombang'));
            }
        } else if (post('mode') == 'add') {
            if (post('gelombang') == '5') {
                $jadwal_ppmb->add_jadwal_ppmb(post('lokasi'), post('alamat'), post('ruang'), date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year'))), post('kapasitas'), '', post('gelombang'), post('nomer_awal'));
            } else {
                $jadwal_ppmb->add_jadwal_ppmb(post('lokasi'), post('alamat'), post('ruang'), date('d-M-Y', mktime(0, 0, 0, post('tgl_test_Month'), post('tgl_test_Day'), post('tgl_test_Year'))), post('kapasitas'), post('kode_jalur'), post('gelombang'));
            }
        } else if (post('mode') == 'delete') {
            $jadwal_ppmb->delete_jadwal_ppmb(post('id_jadwal'));
            $jadwal_ppmb->delete_plot_ppmb($id_jadwal);
        } else if (post('mode') == 'aktifkan') {
            $jadwal_ppmb->aktifkan(post('id_jadwal'));
        }
    }
    $smarty->assign('data_ruang_ppmb', $jadwal_ppmb->load_ruang_ppmb());
    $smarty->assign('data_jadwal_ppmb', $jadwal_ppmb->load_jadwal_ppmb());
    $smarty->display("ppmb/jadwal/{$mode}.tpl");
?>
