<?php

include('config.php');
include 'class/elpt.class.php';

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN && $user->ID_UNIT_KERJA == 41) {
    $elpt = new elpt($db);
    $data_view_hasil_elpt = array();
    $mode = get('mode', 'view');
    foreach ($elpt->load_data_view_elpt() as $fakultas) {
        array_push($data_view_hasil_elpt, array(
            'id_fakultas' => $fakultas['ID_FAKULTAS'],
            'fakultas' => $fakultas['NM_FAKULTAS'],
            'jumlah_peserta' => $fakultas['JUMLAH_PESERTA'],
            'peserta_hadir' => $fakultas['PESERTA_HADIR'],
            'kurang_400' => $elpt->get_elpt_score_less_than($fakultas['ID_FAKULTAS'], 401),
            '401_450' => $elpt->get_elpt_score_between($fakultas['ID_FAKULTAS'], 401, 450),
            '451_500' => $elpt->get_elpt_score_between($fakultas['ID_FAKULTAS'], 451, 500),
            '501_550' => $elpt->get_elpt_score_between($fakultas['ID_FAKULTAS'], 501, 550),
            '551_600' => $elpt->get_elpt_score_between($fakultas['ID_FAKULTAS'], 551, 600),
            'lebih_600' => $elpt->get_elpt_score_more_than($fakultas['ID_FAKULTAS'], 600)
        ));
    }
    if (get('mode') == 'data_cm') {
        $smarty->assign("data_mhs_view_hasil_elpt", $elpt->load_c_mhs_view(get('id_fakultas'), get('con'),get('range'),get('range1'),get('range2')));
    } else {
        $smarty->assign("data_view_hasil_elpt", $data_view_hasil_elpt);
    }
    $smarty->display("toefl/view/{$mode}.tpl");
} else {
    echo "<h2>Khusus area pegawai PINLABS.</h2>";
};
?>
