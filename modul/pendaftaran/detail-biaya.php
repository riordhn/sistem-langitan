<?php
include('../../config.php');

$no_ujian = get('no_ujian');

if ($no_ujian != '')
{
    $pembayaran_cmhs_set = array();
    $db->Query("
        SELECT PC.ID_PEMBAYARAN_CMHS, PC.ID_C_MHS, B.NM_BIAYA, PC.BESAR_BIAYA, PC.TGL_BAYAR, BNK.NM_BANK, BV.NAMA_BANK_VIA, PC.IS_TAGIH, PC.NO_TRANSAKSI FROM PEMBAYARAN_CMHS PC
        LEFT JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PC.ID_DETAIL_BIAYA
        LEFT JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
        LEFT JOIN BANK BNK ON BNK.ID_BANK = PC.ID_BANK
        LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA = PC.ID_BANK_VIA
        LEFT JOIN CALON_MAHASISWA CM ON CM.ID_C_MHS = PC.ID_C_MHS
        WHERE CM.NO_UJIAN = '{$no_ujian}'");
   while ($row = $db->FetchAssoc())
       array_push ($pembayaran_cmhs_set, $row);
   
   // mendapatkan total biaya
   $total_biaya = 0;
   $total_tagih = 0;
   $total_tidak_tagih = 0;
   foreach ($pembayaran_cmhs_set as $pc)
   {
       $total_biaya += $pc['BESAR_BIAYA'];
       if ($pc['IS_TAGIH'] == 'Y') {
           $total_tagih += $pc['BESAR_BIAYA'];
       }
       if ($pc['IS_TAGIH'] == 'T') {
           $total_tidak_tagih += $pc['BESAR_BIAYA'];
       }
   }
   
   $smarty->assign('total_biaya', $total_biaya);
   $smarty->assign('total_tagih', $total_tagih);
   $smarty->assign('total_tidak_tagih', $total_tidak_tagih);
   $smarty->assign('pembayaran_cmhs_set', $pembayaran_cmhs_set);
}

$smarty->display('verifikasi/detail-biaya.tpl');
?>
