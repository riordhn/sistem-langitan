<?php
include('config.php');

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN) {

$mode = get('mode', 'view');

$ruang_toefl_table = new RUANG_TOEFL_TABLE($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
        $ruang_toefl = new RUANG_TOEFL();
        $ruang_toefl->NM_RUANG = post('nm_ruang');
        $ruang_toefl->KAPASITAS = post('kapasitas');
        $ruang_toefl_table->Insert($ruang_toefl);
    }
    else if (post('mode') == 'edit')
    {
        $ruang_toefl = $ruang_toefl_table->Single(post('id_ruang_toefl'));
        $ruang_toefl->NM_RUANG = post('nm_ruang');
        $ruang_toefl->KAPASITAS = post('kapasitas');
        $ruang_toefl_table->Insert($ruang_toefl);
    }
    else if (post('mode') == 'delete')
    {
        $ruang_toefl = $ruang_toefl_table->Single(post('id_ruang_toefl'));
        $ruang_toefl_table->Delete($ruang_toefl);
    }
}

if ($mode == 'view')
{
    $smarty->assign('ruang_toefl_set', $ruang_toefl_table->SelectCriteria('ORDER BY NM_RUANG'));
}
else if ($mode == 'edit' || $mode == 'delete')
{
    $smarty->assign('ruang_toefl', $ruang_toefl_table->Single(get('id_ruang_toefl')));
}

$smarty->display("toefl/ruang/{$mode}.tpl");

} else { echo "<h2>Khusus area pegawai PINLABS.</h2>"; };
?>
