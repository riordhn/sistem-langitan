<?php
include('../../config.php');
include_once('../../tcpdf/config/lang/ind.php');
include_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');
$pdf->SetTitle('Cetak PPKMB');
$pdf->SetSubject('Daftar Hadir Peserta PPKMB Mahasiswa Baru Universitas Airlangga');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->AddPage();

$html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .header { font-size: 16pt; font-family: Tahoma; font-weight: bold; text-align:center;}
    .address { font-size: 14pt; font-family: serif; margin-top: 0px ;text-align:center; }
    .table_head { background-color:#999999;}
    td { font-size: 10pt; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center">
            <span class="header">DATA MAHASISWA FAKULTAS {fakultas_up}<br/>PPKMB MAHASISWA BARU UNIVERSITAS AIRLANGGA<br/>TAHUN AKADEMIK 2010/2011</span>
        </td>
    </tr>
</table>

<p><p/>
<p><p/>
<p><p/>
<table width="100%" cellpadding="3" border="0.5">
        <tr class="table_head">
            <td width="5%">NO</td>
            <td width="15%">NIM</td>
            <td width="40%">NAMA</td>
            <td width="40%">PRODI</td>
        </tr>
    {data_mahasiswa}
</table>
EOF;
$data = $db->QueryToArray("select mhs.nim_mhs, cm.nm_c_mhs as nama, ps.nm_program_studi
			from calon_mahasiswa cm 
			left join program_studi ps on ps.id_program_studi = cm.id_program_studi
			left join mahasiswa mhs on mhs.id_c_mhs = cm.id_c_mhs
			left join fakultas f on f.id_fakultas = ps.id_fakultas
			where (ps.id_jenjang = 1 or ps.id_jenjang = 5)
			and (cm.status >= 7 or cm.status = 1)
			and f.id_fakultas = '{$_GET['id']}'
			order by ps.id_program_studi,cm.no_ujian");

$i = 1;

$data_mahasiswa = '';
foreach ($data as $row) {
    $data_mahasiswa .='
    <tr>
        <td>' . $i++ . '</td>
        <td>' . $row['NIM_MHS'] . '</td>
        <td>' . $row['NAMA'] . '</td>
        <td>' . $row['NM_PROGRAM_STUDI'] . '</td>
  </tr>';
}


$db->Query("SELECT NM_FAKULTAS AS FAKULTAS FROM FAKULTAS WHERE SINGKATAN_FAKULTAS LIKE '%{$_GET['dest']}%' ");
$fakultas = $db->FetchAssoc();
$html = str_replace('{kelas}', $_GET['kelas'], $html);
$html = str_replace('{fakultas}', $fakultas['FAKULTAS'], $html);
$html = str_replace('{fakultas_up}', strtoupper($fakultas['FAKULTAS']), $html);
$html = str_replace('{data_mahasiswa}', $data_mahasiswa, $html);


$pdf->writeHTML($html);
$pdf->Output();
?>