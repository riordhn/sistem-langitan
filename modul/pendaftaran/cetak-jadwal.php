<?php
include('config.php');

include_once('../../tcpdf/config/lang/ind.php');
include_once('../../tcpdf/tcpdf.php');
include_once('class/du_jadwal.class.php');

$jadwal = new du_jadwal($db);
$calon_mahasiswa = $jadwal->load_calon_mahasiswa_jadwal(get('id_c_mhs'));

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$html = <<<EOF
<style>
    td{font-size: 30px;}
    h1{font-size: 60px;}
</style>
<table cellpadding="6" width="100%" border="0">
    <tr><td colspan="2" align="center"><h1>Jadwal Test Calon Mahasiswa</h1></td></tr>
    <tr>
        <td colspan="2"></td>  
    </tr>
    <tr>
        <td width="35%">Nama</td>
        <td width="65%">: {nama}</td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>: {program_studi}</td>
    </tr>
    <tr>
        <td colspan="2"></td>  
    </tr>
    <tr>
        <td>Tempat Test ELPT</td>
        <td>: PINLABS, Kampus B - Universitas Airlangga</td>
    </tr>
    <tr>
        <td>Jadwal Test ELPT</td>
        <td>: {jadwal_toefl}</td>
    </tr>
    <tr>
        <td>Ruang Test ELPT</td>
        <td>: {ruang_toefl}</td>
    </tr>
    <tr>
        <td>Waktu Test ELPT</td>
        <td>: {waktu_toefl}</td>
    </tr>
    <tr>
        <td colspan="2"></td>  
    </tr>
    <tr>
        <td>Tempat Test Kesehatan</td>
        <td>: Rumah Sakit Universitas Airlangga, Kampus C UNAIR,Surabaya</td>
    </tr>
    <tr>
        <td>Jadwal Test Kesehatan</td>
        <td>: {jadwal_kesehatan}</td>
    </tr>
    <tr>
        <td>Kelompok Test Kesehatan</td>
        <td>: {kelompok_jam}</td>
    </tr>
    <tr>
        <td>Waktu Test Kesehatan</td>
        <td>: {waktu_kesehatan}</td>
    </tr>
    <tr>
        <td colspan="2"></td>  
    </tr>
    <tr>
        <td><strong>NB :</strong> untuk tes kesehatan</td>
        <td>
            <ul>
                <li>Harap membawa kartu tanda peserta dan kartu tanda pengenal</li>
                <li>Harap membawa kacamata bagi yang berkaca mata</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>  
    </tr>
    <tr>
        <td></td>
        <td border="1">
            <table>
                <tr>
                    <td colspan="3" align="center"><strong>
                        BUKTI DAFTAR ULANG / PENGAMBILAN KTM<br/>
                        MAHASISWA BARU UNAIR 2011 / 2012</strong>
                    </td>
                </tr>
                <tr><td colspan="3"></td></tr>
                <tr>
                    <td>Nama</td>
                    <td colspan="2">: {nama}</td>
                </tr>
                <tr>
                    <td>No Ujian</td>
                    <td colspan="2">: {no_ujian}</td>
                </tr>
                <tr>
                    <td>Program Studi</td>
                    <td colspan="2">: {program_studi}</td>
                </tr>
                <tr><td colspan="3"></td></tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Surabaya,<br/>Petugas Registrasi<br/><br/><br/><br/><br/>_______________</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
EOF;

$html = str_replace('{nama}', $calon_mahasiswa['NM_C_MHS'], $html);
$html = str_replace('{no_ujian}', $calon_mahasiswa['NO_UJIAN'], $html);
$html = str_replace('{program_studi}', strtolower(ucwords($calon_mahasiswa['NM_PROGRAM_STUDI'])), $html);
$html = str_replace('{jadwal_toefl}', strftime('%d %B %Y', strtotime($calon_mahasiswa['TGL_TOEFL'])), $html);
$html = str_replace('{ruang_toefl}', $calon_mahasiswa['RUANG_TOEFL'], $html);
$html = str_replace('{waktu_toefl}', $calon_mahasiswa['JAM_TOEFL'] . ":" . $calon_mahasiswa['MENIT_TOEFL'], $html);
$html = str_replace('{jadwal_kesehatan}', strftime('%d %B %Y', strtotime($calon_mahasiswa['TGL_KESEHATAN'])), $html);
$html = str_replace('{kelompok_jam}', $calon_mahasiswa['KELOMPOK_JAM'], $html);
$html = str_replace('{waktu_kesehatan}', $calon_mahasiswa['JAM_KESEHATAN'] . ":" . $calon_mahasiswa['MENIT_KESEHATAN'], $html);

$pdf->AddPage();
$pdf->writeHTML($html);
$pdf->Output();
?>
