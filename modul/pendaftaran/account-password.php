<?php
include('config.php');

if ($request_method == 'POST')
{
    if ($user->ChangePassword(post('old_password'), post('new_password')))
    {
        $smarty->assign('password_changed', 1);
    }
    else
    {
        $smarty->assign('password_wrong', 1);
    }
}

$smarty->display('account/password.tpl');
?>
