<?php
include('../../config.php');

$id_fakultas = get('id_fakultas', '');

if ($request_method == 'POST' && post('mode') == 'generate')
{
    $post_keys = array_keys($_POST);
    
    foreach ($post_keys as $pk)
    {
        if (substr($pk, 0, 6) == 'check_')
        {
            $id_c_mhs = str_replace("check_", "", $pk);
            $id_program_studi = post("program_studi_{$id_c_mhs}");
            $sumbangan = str_replace(".", "", post("sumbangan_{$id_c_mhs}", 0));
            $id_jalur = 3; //pmdk umum
            $id_kelompok_biaya = 1; // mandiri
            
            // mendapatkan jalur calon mahasiswa
            $db->Query("SELECT ID_JALUR, SP3 FROM CALON_MAHASISWA WHERE ID_C_MHS = {$id_c_mhs}");
            $row = $db->FetchAssoc();
            $id_jalur = $row['ID_JALUR'];
            $id_kelompok_biaya = $row['SP3'];
            
            //echo $sumbangan . "<br/>";
            //continue;
            
            $sudah_dibayar = false;
            
            // cek kondisi pembayaran calon mahasiswa apakah sudah dibayar atau belum
            $db->Query("SELECT TGL_BAYAR FROM PEMBAYARAN_CMHS WHERE ID_C_MHS = {$id_c_mhs} AND ROWNUM <= 1");
            $pembayaran_cmhs = $db->FetchAssoc();
            if ($pembayaran_cmhs)
            {
                if ($pembayaran_cmhs['TGL_BAYAR'] != '')
                {
                    $sudah_dibayar = true;
                }
            }
            
            // ketika belum dibayar lakukan aksi set biaya
            if ($sudah_dibayar == false)
            {   
                // hardcode
                $id_semester = 5;

                // Cek biaya dari tabel biaya kuliah
                $db->Query("
                    SELECT ID_BIAYA_KULIAH FROM BIAYA_KULIAH
                    WHERE ID_PROGRAM_STUDI = {$id_program_studi} AND ID_JALUR = {$id_jalur} AND ID_KELOMPOK_BIAYA = {$id_kelompok_biaya} AND ID_SEMESTER = {$id_semester}");
                $biaya_kuliah = $db->FetchAssoc();

                if ($biaya_kuliah)
                {    
                    // hapus dari biaya kuliah cmhs dan pembayaran cmhs
                    $db->Query("DELETE FROM BIAYA_KULIAH_CMHS WHERE ID_C_MHS = {$id_c_mhs}");
                    $db->Query("DELETE FROM PEMBAYARAN_CMHS WHERE ID_C_MHS = {$id_c_mhs}");

                    // ambil detail biaya kuliah
                    $detail_biaya_set = array();
                    $db->Query("SELECT ID_DETAIL_BIAYA, BESAR_BIAYA, ID_BIAYA FROM DETAIL_BIAYA WHERE ID_BIAYA_KULIAH = {$biaya_kuliah['ID_BIAYA_KULIAH']}");
                    while ($detail_biaya = $db->FetchAssoc())
                        array_push($detail_biaya_set, $detail_biaya);

                    // insert biaya kuliah
                    $db->Query("INSERT INTO BIAYA_KULIAH_CMHS (ID_C_MHS, ID_BIAYA_KULIAH) VALUES ({$id_c_mhs}, {$biaya_kuliah['ID_BIAYA_KULIAH']})");

                    // insert pembayaran
                    foreach ($detail_biaya_set as $detail_biaya)
                    {
                        if ($detail_biaya['ID_BIAYA']  == 124) // Sumbangan sukarela
                        {
                            $besar_biaya = $sumbangan;
							
                            if($besar_biaya == ''){
                                $besar_biaya = 0;
                            }
							
                        }
                        else
                        {
                            $besar_biaya = $detail_biaya['BESAR_BIAYA'];
                        }
                        
                        $db->Query("
                            INSERT INTO PEMBAYARAN_CMHS (ID_C_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, BESAR_BIAYA, IS_TAGIH)
                            VALUES ({$id_c_mhs},{$id_semester},{$detail_biaya['ID_DETAIL_BIAYA']},{$besar_biaya},'Y')");
                    }

                    // update prodi, jalur, kelompok biaya ke mahasiswa
                    //$db->Query("UPDATE CALON_MAHASISWA SET ID_PROGRAM_STUDI = {$id_program_studi}, ID_JALUR = {$id_jalur}, SP3 = {$id_kelompok_biaya} WHERE ID_C_MHS = {$id_c_mhs}");
                }
            }
        }
    }
    
    //exit();
}

if ($id_fakultas != '')
{   
    /*
    $calon_mahasiswa_set = $db->QueryToArray("
        SELECT * FROM (
            SELECT CM.ID_C_MHS, CM.NO_UJIAN, CM.NM_C_MHS, CM.ID_PROGRAM_STUDI, PC.BESAR_BIAYA
            FROM CALON_MAHASISWA CM
            LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            LEFT JOIN BIAYA_KULIAH_CMHS BK ON BK.ID_C_MHS = CM.ID_C_MHS
            LEFT JOIN PEMBAYARAN_CMHS PC ON PC.ID_C_MHS = CM.ID_C_MHS
            LEFT JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PC.ID_DETAIL_BIAYA
            WHERE J.ID_JENJANG = 1 AND (CM.STATUS >= 4 OR CM.STATUS = -1) AND CM.ID_JALUR = 3 AND (DB.ID_BIAYA = 124 OR DB.ID_BIAYA IS NULL) AND PS.ID_FAKULTAS = {$id_fakultas} AND GELOMBANG_PMDK = 1
            ORDER BY CM.ID_PROGRAM_STUDI, CM.NM_C_MHS
        ) T "); */
    
    $jalur_d3_alih_jenis = "(CM.ID_JALUR = 4 or CM.ID_JALUR = 5)";
    $jalur_internasional = "(CM.ID_JALUR = 27)";
    $jalur_pindahan = "(CM.ID_JALUR = 7)";
    $jalur_snmptn = "(CM.ID_JALUR = 1)";
    $jalur_mandiri1 = "(CM.ID_JALUR = 3 AND GELOMBANG_PMDK = 1)";
    $jalur_mandiri2 = "(CM.ID_JALUR = 3 AND GELOMBANG_PMDK = 2)";
    $kelompok_d3_alih = "T.ID_KELOMPOK_BIAYA = 1";
    $kelompok_mandiri = "T.ID_KELOMPOK_BIAYA = 1";
    $kelompok_internasional = "T.ID_KELOMPOK_BIAYA = CM.SP3";
    $kelompok_snmptn = "T.ID_KELOMPOK_BIAYA = CM.SP3";
    $kelompok_pindahan = "T.ID_KELOMPOK_BIAYA = CM.SP3";
    
    $calon_mahasiswa_set = $db->QueryToArray("
        SELECT CM.ID_C_MHS, CM.NO_UJIAN, CM.NM_C_MHS, PS.NM_PROGRAM_STUDI, CM.ID_PROGRAM_STUDI, J.NM_JENJANG,
            (CASE CM.ID_PROGRAM_STUDI
                WHEN ID_PILIHAN_1 THEN SP3_1
                WHEN ID_PILIHAN_2 THEN SP3_2
                WHEN ID_PILIHAN_3 THEN SP3_3
                WHEN ID_PILIHAN_4 THEN SP3_4
            END) AS KESANGGUPAN, T.BESAR_BIAYA AS SP3,
            (SELECT COUNT(*) FROM PEMBAYARAN_CMHS PC WHERE PC.ID_C_MHS = CM.ID_C_MHS) AS DETAIL_PEMBAYARAN
        FROM CALON_MAHASISWA CM
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN (
          SELECT BK.ID_PROGRAM_STUDI, BK.ID_JALUR, DB.BESAR_BIAYA, BK.ID_KELOMPOK_BIAYA FROM BIAYA_KULIAH BK
          LEFT JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH
          LEFT JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
          WHERE B.ID_BIAYA = 81) T ON T.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI AND T.ID_JALUR = CM.ID_JALUR AND {$kelompok_pindahan}
        WHERE {$jalur_pindahan} AND CM.ID_PROGRAM_STUDI IS NOT NULL AND PS.ID_FAKULTAS = {$id_fakultas}
        ORDER BY PS.NM_PROGRAM_STUDI, CM.NO_UJIAN");
        
    // Untuk s1 mandiri
    // WHERE (CM.ID_JALUR = 3 OR CM.ID_JALUR = 20) AND GELOMBANG_PMDK = 2 AND CM.ID_PROGRAM_STUDI IS NOT NULL AND PS.ID_FAKULTAS = {$id_fakultas}
    
    // Melihat data sumbangan sebelumnya
    
    $smarty->assign('calon_mahasiswa_set', $calon_mahasiswa_set);
}

// prodi
 $program_studi_set = $db->QueryToArray("
    SELECT PS.ID_PROGRAM_STUDI, PS.NM_PROGRAM_STUDI, J.NM_JENJANG, PS.ID_FAKULTAS
    FROM PROGRAM_STUDI PS
    LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
    WHERE J.ID_JENJANG = 1
    ORDER BY PS.ID_FAKULTAS, PS.NM_PROGRAM_STUDI");

// fakultas
$fakultas_set = $db->QueryToArray("SELECT ID_FAKULTAS, NM_FAKULTAS FROM FAKULTAS");

$smarty->assign('program_studi_set', $program_studi_set);
$smarty->assign('fakultas_set', $fakultas_set);
$smarty->assign('detail_gui', 'gui/verifikasi/biaya/s1.tpl');
$smarty->display('verifikasi/biaya/biaya.tpl');
?>
