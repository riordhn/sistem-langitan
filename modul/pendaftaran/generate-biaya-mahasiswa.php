<?php
include('../../config.php');

// mendapatkan tahun ajaran
$smarty->assign('semester_set', $db->QueryToArray("
    SELECT * FROM SEMESTER
    ORDER BY THN_AKADEMIK_SEMESTER, NM_SEMESTER"));

$smarty->assign('fakultas_set', $db->QueryToArray("
    SELECT ID_FAKULTAS, NM_FAKULTAS FROM FAKULTAS
    ORDER BY NM_FAKULTAS"));

$id_semester = get('id_semester', '');
$id_program_studi = get('id_program_studi', '');

if ($id_semester != '' && $id_program_studi != '')
{
    $mahasiswa_set = $db->QueryToArray("
        SELECT M.ID_MHS, M.NIM_MHS, P.NM_PENGGUNA, KB.NM_KELOMPOK_BIAYA, J.NM_JALUR FROM MAHASISWA M
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
        LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
        LEFT JOIN JALUR J ON J.ID_JALUR = JM.ID_JALUR
        WHERE M.ID_PROGRAM_STUDI = {$id_program_studi} AND (JM.ID_SEMESTER = {$id_semester} OR JM.ID_SEMESTER IS NULL)
        ORDER BY M.ID_KELOMPOK_BIAYA");
   $smarty->assign('mahasiswa_set', $mahasiswa_set);
}

$smarty->display('generate/biaya/mahasiswa.tpl');
?>
