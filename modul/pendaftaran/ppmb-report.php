<?php

include('config.php');
include('class/ppmb.class.php');

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN && $user->ID_UNIT_KERJA == 43) {
    $jadwal_ppmb = new ppmb($db);
    $mode = get('mode', 'view');
    $smarty->assign('data_jadwal_ppmb', $jadwal_ppmb->load_jadwal_ppmb());
    $smarty->display("ppmb/report/{$mode}.tpl");
} else {
    echo "<h2>Khusus area Panitia PPMB.</h2>";
};
?>
