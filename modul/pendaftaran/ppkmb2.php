<?php
include('../../config.php');
include_once('../../tcpdf/config/lang/ind.php');
include_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');
$pdf->SetTitle('Cetak PPKMB');
$pdf->SetSubject('Daftar Hadir Peserta PPKMB Mahasiswa Baru Universitas Airlangga');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->AddPage();

$html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .header { font-size: 16pt; font-family: Tahoma; font-weight: bold; text-align:center;}
    .address { font-size: 14pt; font-family: serif; margin-top: 0px ;text-align:center; }
    .table_head { background-color:#999999;}
    td { font-size: 10pt; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center">
            <span class="header">DAFTAR HADIR<br/>PESERTA PPKMB MAHASISWA BARU UNIVERSITAS AIRLANGGA<br/>TAHUN AKADEMIK 2011/2012</span>
        </td>
    </tr>
</table>

<p><p/>
<p><p/>
<p><p/>
<table width="100%">
    <tr>
        <td align="left"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kelompok {kelas}</span></td>
        <td align="center"><span>Fakultas {fakultas}</span></td>
    </tr>
</table>
<table width="100%" cellpadding="3" border="0.5">
        <tr class="table_head">
            <td width="5%">NO</td>
            <td width="11%">NIM</td>
            <td width="39%">NAMA</td>
            <td width="35%">PRODI</td>
            <td width="10%">TTD</td>
        </tr>
    {data_mahasiswa}
</table>
EOF;
$data = $db->QueryToArray("select cm.nm_c_mhs,m.nim_mhs, upper(f.nm_fakultas) as fakultas,upper(ps.nm_program_studi) as prodi
				from aucc.calon_mahasiswa cm
                                left join aucc.mahasiswa m on m.id_c_mhs = cm.id_c_mhs
				left join aucc.ppkmb ppkmb on ppkmb.id_c_mhs = cm.id_c_mhs
				left join aucc.program_studi ps on ps.id_program_studi = ppkmb.id_program_studi
				left join aucc.fakultas f on f.id_fakultas = ps.id_fakultas
                                where (ps.id_jenjang = 1 or ps.id_jenjang = 5)
                                and (cm.status >= 7 or cm.status = 1)
                                and ppkmb.ruang_ppkmb = '{$_GET['dest']}'
                                and ppkmb.kelas_ppkmb = '{$_GET['kelas']}'
                                order by f.id_fakultas,ps.id_jenjang,ps.id_program_studi
                            ");

$i = 1;

$data_mahasiswa = '';
foreach ($data as $row) {
    $data_mahasiswa .='
    <tr>
        <td>' . $i++ . '</td>
        <td>' . $row['NIM_MHS'] . '</td>
        <td>' . $row['NM_C_MHS'] . '</td>
        <td>' . $row['PRODI'] . '</td>
        <td></td>
  </tr>';
}


$db->Query("SELECT NM_FAKULTAS AS FAKULTAS FROM FAKULTAS WHERE SINGKATAN_FAKULTAS LIKE '%{$_GET['dest']}%' ");
$fakultas = $db->FetchAssoc();
$html = str_replace('{kelas}', $_GET['kelas'], $html);
$html = str_replace('{fakultas}', $fakultas['FAKULTAS'], $html);
$html = str_replace('{data_mahasiswa}', $data_mahasiswa, $html);


$pdf->writeHTML($html);
$pdf->Output();
?>