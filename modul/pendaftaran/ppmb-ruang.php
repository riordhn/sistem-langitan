<?php

include('config.php');
include('class/ppmb.class.php');

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN && $user->ID_UNIT_KERJA == 43) {
    $ruang_ppmb = new ppmb($db);
    $mode = get('mode', 'view');
    if ($mode == 'edit') {
        $smarty->assign('data_ruang_ppmb', $ruang_ppmb->load_ruang_ppmb_by_id(get('id_ruang')));
    } else if ($mode == 'delete') {
        $smarty->assign('data_ruang_ppmb', $ruang_ppmb->load_ruang_ppmb_by_id(get('id_ruang')));
    } else {
        if ((post('mode')) == 'edit') {
            $ruang_ppmb->update_ruang_ppmb(post('id_ruang'), post('nm_ruang'), post('alamat'), post('kapasitas'));
        } else if (post('mode') == 'add') {
            $ruang_ppmb->add_ruang_ppmb(post('nm_ruang'), post('alamat'), post('kapasitas'));
        } else if (post('mode') == 'delete') {
            $ruang_ppmb->delete_ruang_ppmb(post('id_ruang'));
        }
        $smarty->assign('data_ruang_ppmb', $ruang_ppmb->load_ruang_ppmb());
        $smarty->assign('data_ruang_ppmb', $ruang_ppmb->load_ruang_ppmb());
    }
    $smarty->display("ppmb/ruang/{$mode}.tpl");
} else {
    echo "<h2>Khusus area Panitia PPMB.</h2>";
};
?>
