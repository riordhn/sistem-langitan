<?php
include('config.php');

if ($request_method == 'POST')
{
    if (post('mode') == 'save')
    {
        $id_c_mhs = post('id_c_mhs');
        
        $db->Parse("
            UPDATE CALON_MAHASISWA SET
                NM_C_MHS = :nm_c_mhs,
                KODE_JURUSAN = :kode_jurusan,
                ID_PILIHAN_1 = :id_pilihan_1,
                ID_PILIHAN_2 = :id_pilihan_2
            WHERE ID_C_MHS = :id_c_mhs");
        
        $db->BindByName(':id_c_mhs', $id_c_mhs);
        $db->BindByName(':nm_c_mhs', $_POST['nm_c_mhs']);
        $db->BindByName(':kode_jurusan', post('kode_jurusan'));
        $db->BindByName(':id_pilihan_1', post('id_pilihan_1', ''));
        $db->BindByName(':id_pilihan_2', post('id_pilihan_2', ''));
        
        $execute = $db->Execute();
        if ($execute)
        {
            echo "NGUUIK";
        }
        else
        {
            echo "GAGAL";
        }
    }
}

$kode_voucher = get('kode_voucher', '');

$db->Query("
    SELECT ID_C_MHS, NM_C_MHS, KODE_JURUSAN, ID_PILIHAN_1, ID_PILIHAN_2
    FROM CALON_MAHASISWA
    WHERE KODE_VOUCHER = '{$kode_voucher}'");
$cm = $db->FetchAssoc();

if ($cm)
{
    if ($cm['KODE_JURUSAN'] == '01')
    {
        $program_studi_set = array();
        $db->Query("SELECT * FROM PROGRAM_STUDI WHERE ID_JENJANG = 1 AND JURUSAN_SEKOLAH = 1");
        while ($row = $db->FetchAssoc())
            array_push($program_studi_set, $row);
        $smarty->assign('program_studi_set', $program_studi_set);
    }
    else if ($cm['KODE_JURUSAN'] == '02')
    {
        $program_studi_set = array();
        $db->Query("SELECT * FROM PROGRAM_STUDI WHERE ID_JENJANG = 1 AND JURUSAN_SEKOLAH = 2");
        while ($row = $db->FetchAssoc())
            array_push($program_studi_set, $row);
        $smarty->assign('program_studi_set', $program_studi_set);
    }
    
    $smarty->assign('cm', $cm);
}

$smarty->display('ppmb/depag/add.tpl');
?>
