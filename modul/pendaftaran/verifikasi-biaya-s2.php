<?php
include('../../config.php');

$id_fakultas = get('id_fakultas', '');

if ($id_fakultas != '')
{
    $id_fakultas = "AND PS.ID_FAKULTAS = {$id_fakultas} ";
}

$calon_mahasiswa_set = array();
$db->Query("
    SELECT * FROM (
    SELECT CM.ID_C_MHS, CM.NO_UJIAN, CM.NM_C_MHS, CM.ID_PROGRAM_STUDI, CM.ID_JALUR, CM.SP3
    FROM CALON_MAHASISWA CM
    LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI
    LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
    LEFT JOIN BIAYA_KULIAH_CMHS BK ON BK.ID_C_MHS = CM.ID_C_MHS
    WHERE (J.ID_JENJANG = 3) {$id_fakultas}
    ORDER BY CM.ID_PROGRAM_STUDI, CM.NM_C_MHS
    ) T ");

while ($row = $db->FetchAssoc())
    array_push($calon_mahasiswa_set, $row);

// pilihan jalur
$jalur_set = array();
$db->Query("SELECT ID_JALUR, NM_JALUR FROM JALUR ORDER BY NM_JALUR");
while ($row = $db->FetchAssoc())
    array_push($jalur_set, $row);

// pilihan kelompok biaya
$kelompok_biaya_set = array();
$db->Query("SELECT ID_KELOMPOK_BIAYA, NM_KELOMPOK_BIAYA FROM KELOMPOK_BIAYA ORDER BY NM_KELOMPOK_BIAYA");
while ($row = $db->FetchAssoc())
    array_push($kelompok_biaya_set, $row);

// prodi
$program_studi_set = array();
$db->Query("
    SELECT PS.ID_PROGRAM_STUDI, PS.NM_PROGRAM_STUDI, J.NM_JENJANG, PS.ID_FAKULTAS
    FROM PROGRAM_STUDI PS
    LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
    WHERE J.ID_JENJANG = 2 OR J.ID_JENJANG = 3 OR J.ID_JENJANG = 9 OR J.ID_JENJANG = 10
    ORDER BY PS.ID_FAKULTAS, PS.NM_PROGRAM_STUDI");
while ($row = $db->FetchAssoc())
    array_push($program_studi_set, $row);

// fakultas
$fakultas_set = array();
$db->Query("SELECT ID_FAKULTAS, NM_FAKULTAS FROM FAKULTAS");
while ($row = $db->FetchAssoc())
    array_push($fakultas_set, $row);

$smarty->assign('calon_mahasiswa_set', $calon_mahasiswa_set);
$smarty->assign('program_studi_set', $program_studi_set);
$smarty->assign('jalur_set', $jalur_set);
$smarty->assign('kelompok_biaya_set', $kelompok_biaya_set);
$smarty->assign('fakultas_set', $fakultas_set);
$smarty->assign('detail_gui', 'gui/verifikasi/biaya/s2.tpl');
$smarty->display('verifikasi/biaya/biaya.tpl');
?>
