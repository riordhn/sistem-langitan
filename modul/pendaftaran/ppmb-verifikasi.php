<?php
include('config.php');
include('class/ppmb.class.php');

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN && $user->ID_UNIT_KERJA == 43) {

$calon_mahasiswa_table = new CALON_MAHASISWA_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);
$negara_table = new NEGARA_TABLE($db);
$provinsi_table = new PROVINSI_TABLE($db);
$kota_table = new KOTA_TABLE($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'save_s1')
    {
        $id_c_ms = post('id_c_mhs');
        
        $db->Parse("UPDATE CALON_MAHASISWA SET
            NM_C_MHS = :nm_c_mhs,
            ID_KOTA_LAHIR = :id_kota_lahir,
            TGL_LAHIR = :tgl_lahir,
            ALAMAT = :alamat,
            TELP = :telp,
            JENIS_KELAMIN = :jenis_kelamin,
            KEWARGANEGARAAN = :kewarganegaraan,
            KEWARGANEGARAAN_LAIN = :kewarganegaraan_lain,
            SUMBER_BIAYA = :sumber_biaya,
            BEASISWA = :beasiswa,
            SUMBER_BIAYA_LAIN = :sumber_biaya_lain,
            ID_AGAMA = :id_agama,
            JUMLAH_KAKAK = :jumlah_kakak,
            JUMLAH_ADIK = :jumlah_adik,
            ID_SEKOLAH_ASAL = :id_sekolah_asal,
            JURUSAN_SEKOLAH = :jurusan_sekolah,
            JURUSAN_SEKOLAH_LAIN = :jurusan_sekolah_lain,
            NO_IJAZAH = :no_ijazah,
            TGL_IJAZAH = :tgl_ijazah,
            TAHUN_LULUS = :tahun_lulus,
            JUMLAH_PELAJARAN_IJAZAH = :jumlah_pelajaran_ijazah,
            NILAI_IJAZAH = :nilai_ijazah,
            TAHUN_UAN = :tahun_uan,
            JUMLAH_PELAJARAN_UAN = :jumlah_pelajaran_uan,
            NILAI_UAN = :nilai_uan,
            
            NAMA_AYAH = :nama_ayah,
            ALAMAT_AYAH = :alamat_ayah,
            ID_KOTA_AYAH = :id_kota_ayah,
            STATUS_ALAMAT_AYAH = :status_alamat_ayah,
            
            NAMA_IBU = :nama_ibu,
            ALAMAT_IBU = :alamat_ibu,
            ID_KOTA_IBU = :id_kota_ibu,
            STATUS_ALAMAT_IBU = :status_alamat_ibu,
            
            PENDIDIKAN_AYAH = :pendidikan_ayah,
            PENDIDIKAN_IBU = :pendidikan_ibu,
            PEKERJAAN_AYAH = :pekerjaan_ayah,
            PEKERJAAN_AYAH_LAIN = :pekerjaan_ayah_lain,
            PEKERJAAN_IBU = :pekerjaan_ibu,
            PEKERJAAN_IBU_LAIN = :pekerjaan_ibu_lain,
            STATUS_NIM_LAMA = :status_nim_lama,
            NIM_LAMA = :nim_lama,
            STATUS_PT_ASAL = :status_pt_asal,
            NAMA_PT_ASAL = :nama_pt_asal,
            
            ID_PILIHAN_1 = :id_pilihan_1,
            ID_PILIHAN_2 = :id_pilihan_2,
            ID_PILIHAN_3 = :id_pilihan_3,
            ID_PILIHAN_4 = :id_pilihan_4,
            
            BERKAS_STTB = :berkas_sttb,
            BERKAS_IJAZAH = :berkas_ijazah,
            BERKAS_KP = :berkas_kp,
            BERKAS_SKHUN = :berkas_skhun,
            BERKAS_LAIN = :berkas_lain,
            
            STATUS_PRA_UJIAN = :status_pra_ujian
            
            WHERE ID_C_MHS = :id_c_mhs");
        
        $db->BindByName(':id_c_mhs', $id_c_ms);
        
        $db->BindByName(':nm_c_mhs', post('nm_c_mhs'));
        $db->BindByName(':id_kota_lahir', post('id_kota_lahir'));
        $db->BindByName(':tgl_lahir', date('d-M-Y', mktime(0, 0, 0, intval(post('tgl_lahir_Month')), intval(post('tgl_lahir_Day')), post('tgl_lahir_Year'))));
        $db->BindByName(':alamat', post('alamat'));
        $db->BindByName(':telp', post('telp'));
        
        $db->BindByName(':jenis_kelamin', post('jenis_kelamin'));
        
        $db->BindByName(':kewarganegaraan', post('kewarganegaraan'));
        $db->BindByName(':kewarganegaraan_lain', post('kewarganegaraan_lain'));
        $db->BindByName(':sumber_biaya', post('sumber_biaya'));
        $db->BindByName(':beasiswa', post('beasiswa'));
        $db->BindByName(':sumber_biaya_lain', post('sumber_biaya_lain'));
        $db->BindByName(':id_agama', post('id_agama'));
        $db->BindByName(':jumlah_kakak', post('jumlah_kakak'));
        $db->BindByName(':jumlah_adik', post('jumlah_adik'));
        $db->BindByName(':id_sekolah_asal', post('id_sekolah_asal'));
        $db->BindByName(':jurusan_sekolah', post('jurusan_sekolah'));
        $db->BindByName(':jurusan_sekolah_lain', post('jurusan_sekolah_lain'));
        $db->BindByName(':no_ijazah', post('no_ijazah'));
        $db->BindByName(':tgl_ijazah', date('d-M-Y', mktime(0, 0, 0, intval(post('tgl_ijazah_Month')), intval(post('tgl_ijazah_Day')), post('tgl_ijazah_Year'))));
        $db->BindByName(':tahun_lulus', post('tahun_lulus'));
        $db->BindByName(':jumlah_pelajaran_ijazah', post('jumlah_pelajaran_ijazah'));
        $db->BindByName(':nilai_ijazah', post('nilai_ijazah'));
        $db->BindByName(':tahun_uan', post('tahun_uan'));
        $db->BindByName(':jumlah_pelajaran_uan', post('jumlah_pelajaran_uan'));
        $db->BindByName(':nilai_uan', post('nilai_uan'));
        
        $db->BindByName(':nama_ayah', post('nama_ayah'));
        $db->BindByName(':alamat_ayah', post('alamat_ayah'));
        $db->BindByName(':id_kota_ayah', post('id_kota_ayah'));
        $db->BindByName(':status_alamat_ayah', post('status_alamat_ayah'));
        
        $db->BindByName(':nama_ibu', post('nama_ayah'));
        $db->BindByName(':alamat_ibu', post('alamat_ayah'));
        $db->BindByName(':id_kota_ibu', post('id_kota_ayah'));
        $db->BindByName(':status_alamat_ibu', post('status_alamat_ayah'));
        
        $db->BindByName(':pendidikan_ayah', post('pendidikan_ayah'));
        $db->BindByName(':pendidikan_ibu', post('pendidikan_ibu'));
        $db->BindByName(':pekerjaan_ayah', post('pekerjaan_ayah'));
        $db->BindByName(':pekerjaan_ayah_lain', post('pekerjaan_ayah_lain'));
        $db->BindByName(':pekerjaan_ibu', post('pekerjaan_ibu'));
        $db->BindByName(':pekerjaan_ibu_lain', post('pekerjaan_ibu_lain'));
        $db->BindByName(':status_nim_lama', post('status_nim_lama'));
        $db->BindByName(':nim_lama', post('nim_lama'));
        $db->BindByName(':status_pt_asal', post('status_pt_asal'));
        $db->BindByName(':nama_pt_asal', post('nama_pt_asal'));
        
        $db->BindByName(':id_pilihan_1', post('id_pilihan_1'));
        $db->BindByName(':id_pilihan_2', post('id_pilihan_2'));
        $db->BindByName(':id_pilihan_3', post('id_pilihan_3'));
        $db->BindByName(':id_pilihan_4', post('id_pilihan_4'));
        
        $db->BindByName(':berkas_sttb', $berkas_sttb = post('berkas_sttb') ? 1 : 0 );
        $db->BindByName(':berkas_ijazah', $berkas_ijazah = post('berkas_ijazah') ? 1 : 0);
        $db->BindByName(':berkas_kp', $berkas_kp = post('berkas_kp') ? 1 : 0);
        $db->BindByName(':berkas_skhun', $berkas_skhun = post('berkas_skhun') ? 1 : 0);
        $db->BindByName(':berkas_lain', $berkas_lain = post('berkas_lain') ? 1 : 0);
        
        $status_pra_ujian = 1; // sudah verifikasi
        $db->BindByName(':status_pra_ujian', $status_pra_ujian);
        
        $execute = $db->Execute();
    }
    else if (post('mode') == 'save_verifikasi')
    {
        $id_c_mhs = post('id_c_mhs');
        $kode_jalur = post('kode_jalur');
        
        // cek jadwal
        $db->Query("SELECT ID_JADWAL_PPMB FROM CALON_MAHASISWA WHERE ID_C_MHS = {$id_c_mhs}");
        $row = $db->FetchAssoc();
        
        // belum punya jadwal
        if ($row['ID_JADWAL_PPMB'] == '')
        {
            // menghilangkan angka 0
            $kode_jalur = str_replace("0", "", $kode_jalur);
            
            
        }
        
        // update data pendaftar
        $db->Parse("
            UPDATE CALON_MAHASISWA SET
            
                SP3 = :sp3,
                
                SP3_1 = :sp3_1,
                SP3_2 = :sp3_2,
                SP3_3 = :sp3_3,
                SP3_4 = :sp3_4,
                
                BERKAS_IJAZAH = :berkas_ijazah,
                BERKAS_SKHUN = :berkas_skhun,
                BERKAS_KESANGGUPAN_SP3 = :berkas_kesanggupan_sp3, 
                BERKAS_LAIN = :berkas_lain,
                BERKAS_LAIN_KET = :berkas_lain_ket,
                
                STATUS_PRA_UJIAN = :status_pra_ujian
                
            WHERE ID_C_MHS = :id_c_mhs");
        
        $db->BindByName(':id_c_mhs', $id_c_mhs);
        $db->BindByName(':sp3', post('sp3', ''));
        $db->BindByName(':sp3_1', str_replace(".", "", post('sp3_1')));
        $db->BindByName(':sp3_2', str_replace(".", "", post('sp3_2', '')));
        $db->BindByName(':sp3_3', str_replace(".", "", post('sp3_3', '')));
        $db->BindByName(':sp3_4', str_replace(".", "", post('sp3_4', '')));
        
        $db->BindByName(':berkas_ijazah', $berkas_ijazah = post('berkas_ijazah') ? 1 : 0);
        $db->BindByName(':berkas_skhun', $berkas_skhun = post('berkas_skhun') ? 1 : 0);
        $db->BindByName(':berkas_kesanggupan_sp3', $berkas_kesanggupan_sp3 = post('berkas_kesanggupan_sp3') ? 1 : 0);
        $db->BindByName(':berkas_lain', $berkas_lain = post('berkas_lain') ? 1 : 0);
        $db->BindByName(':berkas_lain_ket', post('berkas_lain_ket', ''));
        
        $status_pra_ujian = 1;
        $db->BindByName(':status_pra_ujian', $status_pra_ujian);
        
        $execute = $db->Execute();
        if (!$execute)
        {
        }
    }
}

$kode_voucher = get('kode_voucher');

// cek kondisi jadwal
$db->Query("
    SELECT ID_C_MHS, NM_C_MHS, KODE_JURUSAN, ID_JADWAL_PPMB, ID_JALUR, ID_PILIHAN_1, PS.ID_JENJANG, SP3
    FROM CALON_MAHASISWA CM
    LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CM.ID_PILIHAN_1
    WHERE KODE_VOUCHER = '{$kode_voucher}'");
$cm = $db->FetchAssoc();
if ($cm)
{
    // jika belum mempunyai jadwal & memastikan sudah isi biodata dan memilih prodi
    if ($cm['ID_JADWAL_PPMB'] == '' && $cm['NM_C_MHS'] != '' && $cm['ID_PILIHAN_1'] != '')
    {
        // id_c_mhs
        $id_c_mhs = $cm['ID_C_MHS'];
        $kode_jalur = str_replace('0', '', $cm['KODE_JURUSAN']);
        
        switch ($cm['ID_JALUR'])
        {
            case '3': $gelombang = '2'; break;  //pmdk umum
            case '20': $gelombang = '7'; break; //depag
            case '5': $gelombang = '4'; break;  // diploma
            case '4': $gelombang = '3'; break;  // alih jenis
        }
        
        // melihat jenjang
        $start_no_ujian = ''; $end_no_ujian = '';
        if ($cm['ID_JENJANG'] == 2) // jenjang s2
        {
            if ($cm['ID_PILIHAN_1'] == 52)  // Sains Hukum dan Pembangunan : 51130701
            {
                $start_no_ujian = '51130701';
                $end_no_ujian   = '51130800';
            }
            else if ($cm['ID_PILIHAN_1'] == 53)  // Magister Hukum : 51130801 (non jakarta)   51131201 (jakarta)
            {
                if (substr($kode_voucher, 0, 4) == '9501')   // Magister hukum jakarta : 51131201
                {
                    $start_no_ujian = '51131201';
                    $end_no_ujian   = '51131300';
                }
                else   // magister hukum non jakarta : 51130801
                {
                    $start_no_ujian = '51130801';
                    $end_no_ujian   = '51130900';
                }
            }
            else if ($cm['ID_PILIHAN_1'] == 54)  // Kenotariatan : 51131001
            {
                $start_no_ujian = '51131001';
                $end_no_ujian   = '51131100';
            }
            else  // magister non fak hukum
            {
                $start_no_ujian = '51120901';
                $end_no_ujian = '51121100';
            }
        }
        else if ($cm['ID_JENJANG'] == 3)  // jenjang s3 : 51160201
        {
            $start_no_ujian = '51160201';
            $end_no_ujian   = '51160300';
        }
        else if ($cm['ID_JENJANG'] == 9)  // jenjang profesi
        {
            if ($cm['ID_PILIHAN_1'] == 63)  // profesi akuntansi : 51110301
            {
                $start_no_ujian = '51110301';
                $end_no_ujian   = '51110400';
            }
            else if ($cm['ID_PILIHAN_1'] == 83)  // profesi dokter hewan : 51110201
            {
                $start_no_ujian = '51110201';
                $end_no_ujian   = '51110300';
            }
        }
        
        // melakukan pemilihan jadwal untuk pasca
        $db->Query("
            SELECT T.ID_JADWAL_PPMB, T.ID_PLOT_JADWAL_PPMB, T.NO_UJIAN FROM (
                SELECT * FROM PLOT_JADWAL_PPMB
                WHERE NO_UJIAN BETWEEN '{$start_no_ujian}' AND '{$end_no_ujian}'
                AND ID_C_MHS IS NULL
                ORDER BY NO_UJIAN ASC) T
            LEFT JOIN JADWAL_PPMB J ON J.ID_JADWAL_PPMB = T.ID_JADWAL_PPMB
            WHERE J.GELOMBANG = 5 AND STATUS_AKTIF = 1 AND ROWNUM <= 1");
        $row = $db->FetchAssoc();
        
        // melakukan pemilihan random terhadap ruangan untuk s1 / d3 / alih jenis
        /*
        $db->Query("
            SELECT * FROM (
                SELECT ID_PLOT_JADWAL_PPMB, NO_UJIAN, ID_JADWAL_PPMB FROM PLOT_JADWAL_PPMB
                WHERE ID_JADWAL_PPMB = (
                    SELECT ID_JADWAL_PPMB FROM (
                        SELECT ID_JADWAL_PPMB FROM JADWAL_PPMB
                        WHERE TERISI < KAPASITAS AND STATUS_AKTIF = 1 AND GELOMBANG = {$gelombang} AND KODE_JALUR = {$kode_jalur}
                        ORDER BY DBMS_RANDOM.VALUE) T
                    WHERE ROWNUM <= 1)
                AND ID_C_MHS IS NULL
                ORDER BY DBMS_RANDOM.VALUE) T2
            WHERE ROWNUM <= 1");
        $row = $db->FetchAssoc();
        * */
        
        if ($row)
        {
            // update nomer ujian calon_mahasiswa di plot_jadwal
            $db->Query("UPDATE PLOT_JADWAL_PPMB SET ID_C_MHS = {$id_c_mhs} WHERE ID_PLOT_JADWAL_PPMB = {$row['ID_PLOT_JADWAL_PPMB']}");

            // update nomer ujian di calon mahasiswa
            $db->Query("UPDATE CALON_MAHASISWA SET NO_UJIAN = '{$row['NO_UJIAN']}', ID_JADWAL_PPMB = {$row['ID_JADWAL_PPMB']} WHERE ID_C_MHS = {$id_c_mhs}");

            // update jadawl ppmb
            $db->Query("UPDATE JADWAL_PPMB SET TERISI = TERISI + 1 WHERE ID_JADWAL_PPMB = {$row['ID_JADWAL_PPMB']}");
        }
    }
}

$db->Query("
    SELECT
        CM.ID_C_MHS, CM.NM_C_MHS, CM.KODE_JURUSAN, CM.KODE_VOUCHER, CM.NO_UJIAN, CM.ID_JALUR, SP3,
        CM.ID_PILIHAN_1, CM.ID_PILIHAN_2, CM.ID_PILIHAN_3, CM.ID_PILIHAN_4,
        PS1.NM_PROGRAM_STUDI AS PRODI1, PS2.NM_PROGRAM_STUDI AS PRODI2,
        PS3.NM_PROGRAM_STUDI AS PRODI3, PS4.NM_PROGRAM_STUDI AS PRODI4,
        CM.SP3_1, CM.SP3_2, CM.SP3_3, CM.SP3_4,
        CM.BERKAS_IJAZAH, CM.BERKAS_SKHUN, CM.BERKAS_KESANGGUPAN_SP3, CM.BERKAS_LAIN, CM.BERKAS_LAIN_KET
    FROM CALON_MAHASISWA CM
    LEFT JOIN PROGRAM_STUDI PS1 ON PS1.ID_PROGRAM_STUDI = CM.ID_PILIHAN_1
    LEFT JOIN PROGRAM_STUDI PS2 ON PS2.ID_PROGRAM_STUDI = CM.ID_PILIHAN_2
    LEFT JOIN PROGRAM_STUDI PS3 ON PS3.ID_PROGRAM_STUDI = CM.ID_PILIHAN_3
    LEFT JOIN PROGRAM_STUDI PS4 ON PS4.ID_PROGRAM_STUDI = CM.ID_PILIHAN_4
    WHERE CM.KODE_VOUCHER = '{$kode_voucher}'");
    
$calon_mahasiswa = $db->FetchAssoc();
$smarty->assign('calon_mahasiswa', $calon_mahasiswa);

// Mendapatkan biaya minimal sp3 1
$db->Query("
    SELECT DB.BESAR_BIAYA FROM DETAIL_BIAYA DB
    LEFT JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = DB.ID_BIAYA_KULIAH
    LEFT JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
    WHERE B.NM_BIAYA = 'SP3' AND BK.ID_SEMESTER = 5 AND BK.ID_JALUR = {$calon_mahasiswa['ID_JALUR']}
    AND BK.ID_KELOMPOK_BIAYA = 1 AND BK.ID_PROGRAM_STUDI = {$calon_mahasiswa['ID_PILIHAN_1']}");
$row = $db->FetchAssoc();
if ($row)
    $smarty->assign('sp3_1_min', $row['BESAR_BIAYA']);
else
    $smarty->assign('sp3_1_min', 0);

// Mendapatkan kelompok biaya untuk pasca
if ($calon_mahasiswa['ID_JALUR'] == 6 || $calon_mahasiswa['ID_JALUR'] == 23 || $calon_mahasiswa['ID_JALUR'] == 24)
{
    $kelompok_biaya_set = $db->QueryToArray("
        SELECT BK.ID_KELOMPOK_BIAYA, KB.NM_KELOMPOK_BIAYA FROM CALON_MAHASISWA CM
        LEFT JOIN BIAYA_KULIAH BK ON BK.ID_PROGRAM_STUDI = CM.ID_PILIHAN_1
        LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
        WHERE KODE_VOUCHER = '{$kode_voucher}' AND BK.ID_SEMESTER = 5 AND BK.ID_JALUR = {$calon_mahasiswa['ID_JALUR']}");
    $smarty->assign('kelompok_biaya_set', $kelompok_biaya_set);
    $smarty->assign('sp3_1_min', 0);
}

// Mendapatkan biaya minimal sp3 2
$db->Query("
    SELECT DB.BESAR_BIAYA FROM DETAIL_BIAYA DB
    LEFT JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = DB.ID_BIAYA_KULIAH
    LEFT JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
    WHERE B.NM_BIAYA = 'SP3' AND BK.ID_SEMESTER = 5 AND BK.ID_JALUR = {$calon_mahasiswa['ID_JALUR']}
    AND BK.ID_KELOMPOK_BIAYA = 1 AND BK.ID_PROGRAM_STUDI = {$calon_mahasiswa['ID_PILIHAN_2']}");
$row = $db->FetchAssoc();
if ($row)
    $smarty->assign('sp3_2_min', $row['BESAR_BIAYA']);
else
    $smarty->assign('sp3_2_min', 0);

if ($calon_mahasiswa['KODE_JURUSAN'] == '03')
{
    // Mendapatkan biaya minimal sp3 1
    $db->Query("
        SELECT DB.BESAR_BIAYA FROM DETAIL_BIAYA DB
        LEFT JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = DB.ID_BIAYA_KULIAH
        LEFT JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
        WHERE B.NM_BIAYA = 'SP3' AND BK.ID_SEMESTER = 5 AND BK.ID_JALUR = {$calon_mahasiswa['ID_JALUR']}
        AND BK.ID_KELOMPOK_BIAYA = 1 AND BK.ID_PROGRAM_STUDI = {$calon_mahasiswa['ID_PILIHAN_3']}");
    $row = $db->FetchAssoc();
    if ($row)
        $smarty->assign('sp3_3_min', $row['BESAR_BIAYA']);
    else
        $smarty->assign('sp3_3_min', 0);

    // Mendapatkan biaya minimal sp3 2
    $db->Query("
        SELECT DB.BESAR_BIAYA FROM DETAIL_BIAYA DB
        LEFT JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = DB.ID_BIAYA_KULIAH
        LEFT JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
        WHERE B.NM_BIAYA = 'SP3' AND BK.ID_SEMESTER = 5 AND BK.ID_JALUR = {$calon_mahasiswa['ID_JALUR']}
        AND BK.ID_KELOMPOK_BIAYA = 1 AND BK.ID_PROGRAM_STUDI = {$calon_mahasiswa['ID_PILIHAN_4']}");
    $row = $db->FetchAssoc();
    if ($row)
        $smarty->assign('sp3_4_min', $row['BESAR_BIAYA']);
    else
        $smarty->assign('sp3_4_min', 0);
}


/*
$calon_mahasiswa_set = $calon_mahasiswa_table->SelectWhere("KODE_VOUCHER = '{$kode_voucher}'");

if ($calon_mahasiswa_set->Count() > 0)
{
    $calon_mahasiswa = $calon_mahasiswa_set->Get(0);

    // pendidikan orang tua
    $smarty->assign('pendidikan_ortu', array(
        1 => 'Tidak Tamat SD', 2 => 'SD', 3 => 'SLTP', 4 => 'SLTA', 5 => 'Diploma', 6 => 'S1', 7 => 'S2', 8 => 'S3'
    ));

    // PROGRAM STUDI IPA & IPS & all
    $prodi_ipa_set = $program_studi_table->SelectCriteria("WHERE ID_JENJANG = 1 AND JURUSAN_SEKOLAH = 1");
    $prodi_ips_set = $program_studi_table->SelectCriteria("WHERE ID_JENJANG = 1 AND JURUSAN_SEKOLAH = 2");
    $program_studi_set = $program_studi_table->SelectCriteria("WHERE ID_JENJANG = 1");

    // negara dan kota
    $negara = $negara_table->SelectCriteria('ORDER BY NM_NEGARA ASC');
    $provinsi_set = $provinsi_table->Select();
    $kota_table->FillProvinsiSet($provinsi_set);

    // mendapatkan sususan combobox sekolah
    //$sekolah_set = $sekolah_table->SelectCriteria("WHERE ID_KOTA = (SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL}) ORDER BY NM_SEKOLAH ASC");
    if ($calon_mahasiswa->ID_SEKOLAH_ASAL > 0)
    {
        // mendapatkan sekolah
        $sekolah_set = array();
        $db->Query("SELECT * FROM SEKOLAH WHERE ID_KOTA = (SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL}) ORDER BY NM_SEKOLAH ASC");
        while ($row = $db->FetchAssoc())
            array_push($sekolah_set, $row);
        $smarty->assign('sekolah_set', $sekolah_set);

        // kota terpilih
        $db->Query("SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL}");
        $row = $db->FetchAssoc();
        $smarty->assign('id_kota_sekolah', $row['ID_KOTA']);

        // mendapatkan list kota sekolah
        $kota_set = array();
        $db->Query("
            SELECT * FROM KOTA WHERE ID_PROVINSI = (
                SELECT ID_PROVINSI FROM KOTA WHERE ID_KOTA = (SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL})
            ) ORDER BY NM_KOTA");
        while ($row = $db->FetchAssoc())
            array_push($kota_set, $row);
        $smarty->assign('kota_set', $kota_set);

        // provinsi terpilih
        $db->Query("SELECT ID_PROVINSI FROM KOTA WHERE ID_KOTA = (SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL})");
        $row = $db->FetchAssoc();
        $smarty->assign('id_provinsi_sekolah', $row['ID_PROVINSI']);

        // mendapatkan list provinsi sekolah
        $provinsi_sekolah_set = array();
        $db->Query("
            SELECT * FROM PROVINSI WHERE ID_NEGARA = (
                SELECT ID_NEGARA FROM PROVINSI WHERE ID_PROVINSI = (
                    SELECT ID_PROVINSI FROM KOTA WHERE ID_KOTA = (SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL})))");
        while ($row = $db->FetchAssoc())
            array_push ($provinsi_sekolah_set, $row);
        $smarty->assign('provinsi_sekolah_set', $provinsi_sekolah_set);

        // negara terpilih
        $db->Query("SELECT ID_NEGARA FROM PROVINSI WHERE ID_PROVINSI = (SELECT ID_PROVINSI FROM KOTA WHERE ID_KOTA = (SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL}))");
        $row = $db->FetchAssoc();
        $smarty->assign('id_negara_sekolah', $row['ID_NEGARA']);

    }

    $smarty->assign('program_studi_set', $program_studi_set);
    $smarty->assign('prodi_ipa_set', $prodi_ipa_set);
    $smarty->assign('prodi_ips_set', $prodi_ips_set);

    $smarty->assign('negara', $negara);
    $smarty->assign('provinsi_set', $provinsi_set);
    $smarty->assign('calon_mahasiswa', $calon_mahasiswa);
}
*/
$smarty->display("ppmb/verifikasi/verifikasi.tpl");

} else { echo "<h2>Khusus area Panitia PPMB.</h2>"; };
?>
