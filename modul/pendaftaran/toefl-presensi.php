<?php

include('config.php');
include 'class/elpt.class.php';

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN && $user->ID_UNIT_KERJA == 41) {
    $elpt = new elpt($db);
    $mode = get('mode', 'view');
    if (get('kelompok') != '') {
        $smarty->assign('kelompok', get('kelompok'));
        $smarty->assign('data_c_mhs_by_kelompok', $elpt->load_c_mhs_by_kelompok(get('kelompok')));
    } if (post('save') != '') {
        for ($i = 1; $i < post('jumlah'); $i++) {
            $absen = post('absen'.$i) == '' ? 0 : 1;
            $elpt->update_presensi_elpt(post('id_c_mhs' . $i), $absen);
        }
    }
    $smarty->assign('data_jadwal_elpt', $elpt->load_jadwal_eltp());
    $smarty->display("toefl/presensi/{$mode}.tpl");
} else {
    echo "<h2>Khusus area pegawai PINLABS.</h2>";
};
?>
