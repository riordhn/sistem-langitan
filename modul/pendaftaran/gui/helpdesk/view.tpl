<html><head><title>Aplikasi Help Desk</title></head>
    <body>
        <form action="help-desk.php" method="get">
            <input type="hidden" name="mode" value="search" />
            <table>
                <tr>
                    <td>NIM Mahasiswa</td>
                    <td><input type="text" name="nim_mhs" />
                        <input type="submit" value="CARI" />
                    </td>
                </tr>
            </table>
        </form>
        
        {if $mhs_found}
        <table border="1">
            <tr>
                <td>NIM</td><td>{$mahasiswa.NIM_MHS}</td>
            </tr>
            <tr>
                <td>Nama</td><td>{$mahasiswa.NM_PENGGUNA}</td>
            </tr>
            <tr>
                <td>Program Studi</td><td>{$mahasiswa.NM_PROGRAM_STUDI} - {$mahasiswa.NM_FAKULTAS}</td>
            </tr>
            <tr>
                <td>Angkatan</td><td>{$mahasiswa.THN_ANGKATAN_MHS}</td>
            </tr>
            <tr>
                <td>Status Cekal</td>
                <td><form action="help-desk.php" method="post">
                        <input type="hidden" name="mode" value="set_cekal" />
                        <input type="hidden" name="nim_mhs" value="{$mahasiswa.NIM_MHS}" />
                        {html_options name="status_cekal" options=$status_cekal selected=$mahasiswa.STATUS_CEKAL}
                        <input type="submit" value="Set" />
                    </form>
                </td>
            </tr>
            <tr>
                <td>Status Fingerprint</td>
                <td><form action="help-desk.php" method="post">
                    {if $mahasiswa.FINGER_DATA}SUDAH{else}BELUM{/if}
                    <input type="hidden" name="mode" value="set_fingerprint" />
                    <input type="hidden" name="nim_mhs" value="{$mahasiswa.NIM_MHS}" />
                    <input type="submit" value="Reset" />
                    </form>
                </td>
            </tr>
        </table>
        {/if}
    </body>
</html>