<div class="center_title_bar">Rubah Password</div>

{if isset($password_changed)}
<h3>Password berhasil dirubah !</h3>
{/if}

{if isset($password_wrong)}
<h3>Password lama tidak sesuai</h3>
{/if}

<form action="account-password.php" method="post">
<table>
    <tr>
        <td>Password Lama</td>
        <td><input type="text" name="old_password" maxlength="128" id="old_password" /></td>
    </tr>
    <tr>
        <td>Password Baru</td>
        <td><input type="text" name="new_password" maxlength="128" id="new_password" /></td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

{literal}<script language="javascript">
$('form').validate({
    rules: {
        old_password: { required: true },
        new_password: { required: true, minlength: 8 }
    }
});
</script>{/literal}