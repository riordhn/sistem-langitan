<div class="center_title_bar">Input Presensi ELPT </div>
<table>
    <tr>
        <td><label>Kelompok Jadwal ELPT</label></td>
        <td>
            <form action="toefl-presensi.php" method="get">
                <input type="hidden" name="mode" value="view"/>
                <select name="kelompok">
                {foreach $data_jadwal_elpt as $jadwal_elpt}
                    {if $jadwal_elpt['ID_JADWAL_TOEFL']==$kelompok}
                    <option value="{$jadwal_elpt['ID_JADWAL_TOEFL']}" selected="true">{$jadwal_elpt['TGL_TEST']} {$jadwal_elpt['NM_RUANG']} ({$jadwal_elpt['JAM_MULAI']} : {$jadwal_elpt['MENIT_MULAI']})</option>
                    {else}
                    <option value="{$jadwal_elpt['ID_JADWAL_TOEFL']}">{$jadwal_elpt['TGL_TEST']} {$jadwal_elpt['NM_RUANG']} ({$jadwal_elpt['JAM_MULAI']} : {$jadwal_elpt['MENIT_MULAI']})</option>
                    {/if}
                {/foreach}
                </select>
                <input type="submit" value="View..."/>
            </form>
        </td>
    </tr>
</table>
{if isset($kelompok)}
<form action="toefl-presensi.php" method="post">
    <table>
        <tr>
            <th>No</th>
            <th>No Ujian</th>
            <th>Nama</th>
            <th>Fakultas</th>
            <th>Program Studi</th>
            <th>Presensi</th>
        </tr>
    {$index=1}
    {foreach $data_c_mhs_by_kelompok as $data_c_mhs}
        <tr>
            <td>{$index}</td>
            <td>{$data_c_mhs['NO_UJIAN']}</td>
            <td>{$data_c_mhs['NM_C_MHS']}</td>
            <td>{$data_c_mhs['NM_FAKULTAS']}</td>
            <td>{$data_c_mhs['NM_PROGRAM_STUDI']}</td>
            <td><input type="checkbox" name="absen{$index}" value="1" {if $data_c_mhs['ELPT_KEHADIRAN']=='1'}checked="true"{/if}/></td>
        <input type="hidden" name="id_c_mhs{$index++}" value="{$data_c_mhs['ID_C_MHS']}"/>
        </tr>
    {/foreach}
        <tr>
            <td colspan="6">
                <input type="hidden" name="jumlah" value="{$index}"/>
                <input type="hidden" name="save" value="1"/>
                <input style="float: right;" type="submit" value="Proses" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <a class="disable-ajax" style="float: right;" target="_blank" href="cetak-presensi-elpt.php?kelompok={$kelompok}">cetak presensi</a>
            </td>
        </tr>
    </table>
</form>
{/if}