<div class="center_title_bar">Master Ruang ELPT</div>

<table>
    <tr>
        <th>NO</th>
        <th>Nama Ruang</th>
        <th>Kapasitas</th>
        <th></th>
    </tr>
    {for $i=0 to $ruang_toefl_set->Count()-1}
    {$ruang_toefl=$ruang_toefl_set->Get($i)}
    <tr>
        <td>{$i+1}</td>
        <td>{$ruang_toefl->NM_RUANG}</td>
        <td>{$ruang_toefl->KAPASITAS}</td>
        <td>
            <a href="toefl-ruang.php?mode=edit&id_ruang_toefl={$ruang_toefl->ID_RUANG_TOEFL}">Edit</a>
            <a href="toefl-ruang.php?mode=delete&id_ruang_toefl={$ruang_toefl->ID_RUANG_TOEFL}">Hapus</a>
        </td>
    </tr>
    {/for}
    <tr>
        <td colspan="4" style="text-align: center">
            <button href="toefl-ruang.php?mode=add">Tambah</button>
        </td>
    </tr>
</table>