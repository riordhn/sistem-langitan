<div class="center_title_bar">Master Ruang ELPT - Hapus Ruangan</div>
<h2>Apakah data ruangan ini akan dihapus ?</h2>
<form action="toefl-ruang.php" method="post">
<input type="hidden" name="mode" value="delete" />
<input type="hidden" name="id_ruang_toefl" value="{$ruang_toefl->ID_RUANG_TOEFL}" />
<table>
    <tr>
        <td>Nama Ruang</td>
        <td>{$ruang_toefl->NM_RUANG}</td>
    </tr>
    <tr>
        <td>Kapasitas</td>
        <td>{$ruang_toefl->KAPASITAS}</td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <button href="toefl-ruang.php">Batal</button>
            <input type="submit" value="Hapus" />
        </td>
    </tr>
</table>
</form>