<div class="center_title_bar">Jadwal Tes ELPT - Hapus Jadwal</div>

<form action="toefl-jadwal.php" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="id_jadwal_toefl" value="{$jadwal_toefl->ID_JADWAL_TOEFL}" />
    <table>
        <tr>
            <td>Ruang</td>
            <td>{$jadwal_toefl->RUANG_TOEFL->NM_RUANG}</td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>
                {$jadwal_toefl->TGL_TEST}
            </td>
        </tr>
        <tr>
            <td>Jam</td>
            <td>
                {$jadwal_toefl->JAM_MULAI} : {$jadwal_toefl->MENIT_MULAI}
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <button href="toefl-jadwal.php">Batal</button>
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>