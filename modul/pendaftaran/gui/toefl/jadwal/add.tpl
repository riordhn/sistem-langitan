<div class="center_title_bar">Jadwal Tes ELPT - Tambah</div>

<form action="toefl-jadwal.php" method="post">
<input type="hidden" name="mode" value="add" />
<table>
    <tr>
        <td>Ruang</td>
        <td><select name="id_ruang_toefl">
            {for $i=0 to $ruang_toefl_set->Count()-1}
                <option value="{$ruang_toefl_set->Get($i)->ID_RUANG_TOEFL}">{$ruang_toefl_set->Get($i)->NM_RUANG}</option>
            {/for}
            </select>
        </td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td>
            {html_select_date prefix="tgl_test_" field_order="DMY"}
        </td>
    </tr>
    <tr>
        <td>Jam</td>
        <td>
            {html_select_time prefix="mulai_" display_seconds=false use_24_hours=true minute_interval=5}
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center">
            <button href="toefl-jadwal.php">Batal</button>
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>