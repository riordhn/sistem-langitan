<div class="center_title_bar">Jadwal Tes ELPT</div>

<table style="width: 100%">
    <tr>
        <th class="center" style="width: 5%">No</th>
        <th>Nama Ruang</th>
        <th>Kapasitas</th>
        <th>Jadwal</th>
        <th>Terisi</th>
        <th></th>
    </tr>
    {for $i=0 to $jadwal_toefl_set->Count()-1}
    {$jadwal_toefl = $jadwal_toefl_set->Get($i)}
    <tr>
        <td class="center">{$i+1}</td>
        <td>{$jadwal_toefl->RUANG_TOEFL->NM_RUANG}</td>
        <td>{$jadwal_toefl->RUANG_TOEFL->KAPASITAS}</td>
        <td>{$jadwal_toefl->TGL_TEST|date_format:"%d %B %Y"} [{$jadwal_toefl->JAM_MULAI}:{$jadwal_toefl->MENIT_MULAI}]</td>
        <td>{$jadwal_toefl->TERISI}</td>
        <td>
            <a href="toefl-jadwal.php?mode=edit&id_jadwal_toefl={$jadwal_toefl->ID_JADWAL_TOEFL}">Edit</a>
            <a href="toefl-jadwal.php?mode=delete&id_jadwal_toefl={$jadwal_toefl->ID_JADWAL_TOEFL}">Hapus</a>
            <a href="toefl-jadwal.php?mode=data_cm&id_jadwal_toefl={$jadwal_toefl->ID_JADWAL_TOEFL}">View</a>
        </td>
    </tr>
    {/for}
    <tr>
        <td colspan="7" class="center">
            <button href="toefl-jadwal.php?mode=add">Tambah</button>
        </td>
    </tr>
</table>