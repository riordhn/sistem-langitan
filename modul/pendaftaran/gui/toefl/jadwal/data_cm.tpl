<div class="center_title_bar">Daftar Calon Mahasiswa {$data_jawdal_toefl.NM_RUANG} Jadwal {$data_jadwal_toefl.TGL_TEST} JAM {$data_jadwal_toefl.JAM_MULAI|str_pad:2:0:$smarty.const.STR_PAD_LEFT}:{$data_jadwal_toefl.JAM_SELESAI|str_pad:2:0:$smarty.const.STR_PAD_RIGHT}</div>

<table>
    <tr>
        <th>Nomer</th>
        <th>Nama</th>
        <th>Nomer Ujian</th>
        <th>Alamat</th>
        <th>Telp</th>
        <th>Fakultas</th>
        <th>Program Studi</th>
    </tr>
    {$nomer=1}
    {foreach $data_calon_mahasiswa as $data}
        <tr>
            <td>{$nomer++}</td>
            <td>{$data.NM_C_MHS}</td>
            <td>{$data.NO_UJIAN}</td>
            <td>{$data.ALAMAT}</td>
            <td>{$data.TELP}</td>
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_PROGRAM_STUDI}</td>
        </tr>
    {/foreach}

</table>

<button href="toefl-jadwal.php">Kembali</button>