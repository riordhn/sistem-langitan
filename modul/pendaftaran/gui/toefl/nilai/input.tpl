<div class="center_title_bar">Input Nilai ELPT - Nilai Mahasiswa</div>
<table>
    <tr>
        <td><label>Kelompok Jadwal ELPT</label></td>
        <td>
            <form action="toefl-nilai.php" method="get">
                <input type="hidden" name="mode" value="input"/>
                <select name="kelompok">
                {foreach $data_jadwal_elpt as $jadwal_elpt}
                    {if $jadwal_elpt['ID_JADWAL_TOEFL']==$kelompok}
                    <option value="{$jadwal_elpt['ID_JADWAL_TOEFL']}" selected="true">{$jadwal_elpt['TGL_TEST']} ({$jadwal_elpt['JAM_MULAI']}:{$jadwal_elpt['MENIT_MULAI']})</option>
                    {else}
                    <option value="{$jadwal_elpt['ID_JADWAL_TOEFL']}">{$jadwal_elpt['TGL_TEST']} ({$jadwal_elpt['JAM_MULAI']}:{$jadwal_elpt['MENIT_MULAI']})</option>
                    {/if}
                {/foreach}
                </select>
                <input type="submit" value="View..."/>
            </form>
        </td>
    </tr>
</table>
{if isset($kelompok)}
<form action="toefl-nilai.php" method="post">
    <table>
        <tr>
            <th>No</th>
            <th>No Ujian</th>
            <th>Nama</th>
            <th>Fakultas</th>
            <th>Program Studi</th>
            <th>Listening</th>
            <th>Structure</th>
            <th>Reading</th>
            <th>Score</th>
        </tr>
    {$index=1}
    {foreach $data_c_mhs_by_kelompok as $data_c_mhs}
        <tr>
            <td>{$index}</td>
            <td>{$data_c_mhs['NO_UJIAN']}</td>
            <td>{$data_c_mhs['NM_C_MHS']}</td>
            <td>{$data_c_mhs['NM_FAKULTAS']}</td>
            <td>{$data_c_mhs['NM_PROGRAM_STUDI']}</td>
            <td><input size="5" type="text" {if $data_c_mhs['ELPT_KEHADIRAN']==0 ||$data_c_mhs['ELPT_KEHADIRAN']==''} disabled="true" {/if} name="listening{$index}" value="{$data_c_mhs['ELPT_LISTENING']}"/></td>
            <td><input size="5" type="text" {if $data_c_mhs['ELPT_KEHADIRAN']==0||$data_c_mhs['ELPT_KEHADIRAN']==''} disabled="true" {/if} name="structure{$index}" value="{$data_c_mhs['ELPT_STRUCTURE']}"/></td>
            <td><input size="5" type="text" {if $data_c_mhs['ELPT_KEHADIRAN']==0||$data_c_mhs['ELPT_KEHADIRAN']==''} disabled="true" {/if} name="reading{$index}" value="{$data_c_mhs['ELPT_READING']}"/></td>
            <td><input size="5" type="text" disabled="true" name="score{$index}" value="{$data_c_mhs['ELPT_SCORE']}"/></td>
        <input type="hidden" name="id_c_mhs{$index++}" value="{$data_c_mhs['ID_C_MHS']}"/>
        </tr>
    {/foreach}
        <tr>
            <td colspan="9">
                <input type="hidden" name="jumlah" value="{$index}"/>
                <input type="hidden" name="save" value="1"/>
                <input style="float: right;" type="submit" value="Proses" />
            </td>
        </tr>
    </table>
</form>
{/if}