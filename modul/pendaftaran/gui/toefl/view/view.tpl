<div class="center_title_bar">View Data Hasil Tes ELPT</div>
<table>
    <tr>
        <th>No</th>
        <th>Fakultas</th>
        <th>Jumlah Peserta</th>
        <th>Peserta Hadir</th>
        <th>Kurang = 400</th>
        <th>Antara 401 - 450</th>
        <th>Antara 451 - 500</th>
        <th>Antara 501 - 550</th>
        <th>Antara 551 - 600</th>
        <th>Lebih dari 600</th>
    </tr>
    {$index=1}
    {foreach $data_view_hasil_elpt as $view_hasil_elpt}
    <tr>
        <td>{$index++}</td>
        <td>{$view_hasil_elpt['fakultas']}</a></td>
        <td><a href="toefl-view.php?mode=data_cm&id_fakultas={$view_hasil_elpt['id_fakultas']}&con=">{$view_hasil_elpt['jumlah_peserta']}</a></td>
        <td><a href="toefl-view.php?mode=data_cm&id_fakultas={$view_hasil_elpt['id_fakultas']}&con=hadir">{$view_hasil_elpt['peserta_hadir']}</a></td>
        <td><a href="toefl-view.php?mode=data_cm&id_fakultas={$view_hasil_elpt['id_fakultas']}&con=kur&range=400">{$view_hasil_elpt['kurang_400']['JUMLAH']}</a></td>
        <td><a href="toefl-view.php?mode=data_cm&id_fakultas={$view_hasil_elpt['id_fakultas']}&con=bet&range1=401&range2=450">{$view_hasil_elpt['401_450']['JUMLAH']}</a></td>
        <td><a href="toefl-view.php?mode=data_cm&id_fakultas={$view_hasil_elpt['id_fakultas']}&con=bet&range1=451&range2=500">{$view_hasil_elpt['451_500']['JUMLAH']}</a></td>
        <td><a href="toefl-view.php?mode=data_cm&id_fakultas={$view_hasil_elpt['id_fakultas']}&con=bet&range1=501&range2=550">{$view_hasil_elpt['501_550']['JUMLAH']}</a></td>
        <td><a href="toefl-view.php?mode=data_cm&id_fakultas={$view_hasil_elpt['id_fakultas']}&con=bet&range1=551&range2=600">{$view_hasil_elpt['551_600']['JUMLAH']}</a></td>
        <td><a href="toefl-view.php?mode=data_cm&id_fakultas={$view_hasil_elpt['id_fakultas']}&con=leb&range=600">{$view_hasil_elpt['lebih_600']['JUMLAH']}</a></td>
    </tr>
    {/foreach}
</table>