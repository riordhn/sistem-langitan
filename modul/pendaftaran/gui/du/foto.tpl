<div class="center_title_bar">Foto Calon Mahasiswa</div>

<table>
    <tr>
        <td>
            <label>Nomor Ujian :</label>
        </td>
        <td>
            <form action="du-foto.php" method="get">
            <input type="search" name="no_ujian" {if isset($smarty.get.no_ujian)}value="{$smarty.get.no_ujian}"{/if} />
            <input type="submit" value="Cari..." />
            </form>
        </td>
    </tr>
</table>

{if isset($calon_mahasiswa)}

{if $calon_mahasiswa->STATUS >= 9 || $calon_mahasiswa->STATUS == 1}

{if $calon_mahasiswa->STATUS >= 10 || $calon_mahasiswa->STATUS == 1}
<h3>Silahkan lanjutkan tahap berikutnya</h3>
{/if}

<form action="du-foto.php?no_ujian={$smarty.get.no_ujian}" method="post">
<input type="hidden" name="mode" value="foto" />
<input type="hidden" name="id_c_mhs" value="{$calon_mahasiswa->ID_C_MHS}" />

<table>
    <tr>
        <td>Nama</td>
        <td>{$calon_mahasiswa->NM_C_MHS}</td>
    </tr>
    <tr>
        <td>Status Foto</td>
        <td>
            <input type="checkbox" name="foto" {if $calon_mahasiswa->STATUS >= 10 || $calon_mahasiswa->STATUS == 1}checked="checked"{/if} />
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align:center">
            <input type="submit" value="Proses" />
        </td>
    </tr>
</table>

</form>
{else}
<h3>Mahasiswa belum melewati proses sebelumnya.</h3>
{/if}

{/if}