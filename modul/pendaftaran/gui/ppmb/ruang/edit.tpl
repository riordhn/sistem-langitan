<div class="center_title_bar">Master Ruang PPMB - Edit</div>
<form action="ppmb-ruang.php" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_ruang" value="{$data_ruang_ppmb['ID_RUANG_PPMB']}" />
<table>
    <tr>
        <td>Nama Ruang</td>
        <td><input type="text" name="nm_ruang" size="30" value="{$data_ruang_ppmb['NM_RUANG_PPMB']}" /></td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td><textarea name="alamat">{$data_ruang_ppmb['ALAMAT']}</textarea></td>
    </tr>
    <tr>
        <td>Kapasitas</td>
        <td><input type="text" name="kapasitas" size="2" maxlength="2" value="{$data_ruang_ppmb['KAPASITAS']}" /></td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <button href="ppmb-ruang.php">Batal</button><input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>