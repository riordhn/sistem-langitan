<div class="center_title_bar">Master Ruang PPMB</div>

<table>
    <tr>
        <th>NO</th>
        <th>Nama Ruang</th>
        <th>Alamat</th>
        <th>Kapasitas</th>
        <th>Operasi</th>
    </tr>
    {$index=1}
    {foreach $data_ruang_ppmb as $ruang_ppmb}
    <tr>
        <td>{$index++}</td>
        <td>{$ruang_ppmb['NM_RUANG_PPMB']}</td>
        <td>{$ruang_ppmb['ALAMAT']}</td>
        <td>{$ruang_ppmb['KAPASITAS']}</td>
        <td>
            <a href="ppmb-ruang.php?mode=edit&id_ruang={$ruang_ppmb['ID_RUANG_PPMB']}">Edit</a>
            <a href="ppmb-ruang.php?mode=delete&id_ruang={$ruang_ppmb['ID_RUANG_PPMB']}">Hapus</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="5" style="text-align: center">
            <button href="ppmb-ruang.php?mode=add">Tambah</button>
        </td>
    </tr>
</table>