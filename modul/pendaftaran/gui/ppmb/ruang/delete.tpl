<div class="center_title_bar">Master Ruang PPMB - Hapus Ruangan</div>
<h2>Apakah data ruangan ini akan dihapus ?</h2>
<form action="ppmb-ruang.php" method="post">
<input type="hidden" name="mode" value="delete" />
<input type="hidden" name="id_ruang" value="{$data_ruang_ppmb['ID_RUANG_PPMB']}" />
<table>
    <tr>
        <td>Nama Ruang</td>
        <td>{$data_ruang_ppmb['NM_RUANG_PPMB']}</td>
    </tr>
    <tr>
        <td>Kapasitas</td>
        <td>{$data_ruang_ppmb['KAPASITAS']}</td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <button href="ppmb-ruang.php">Batal</button>
            <input type="submit" value="Hapus" />
        </td>
    </tr>
</table>
</form>