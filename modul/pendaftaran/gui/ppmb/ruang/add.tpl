<div class="center_title_bar">Master Ruang PPMB - Tambah Ruangan</div>

<form action="ppmb-ruang.php" method="post">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <td>Nama Ruang</td>
            <td><input type="text" name="nm_ruang" size="30" /></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td><textarea name="alamat"></textarea></td>
        </tr>
        <tr>
            <td>Kapasitas</td>
            <td><input type="text" name="kapasitas" size="2" maxlength="2" /></td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <button href="ppmb-ruang.php">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>