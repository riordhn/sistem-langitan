<div class="center_title_bar">Verifikasi Akhir Pendaftar</div>

<table>
    <tr>
        <td>
            <label>No Ujian :</label>
        </td>
        <td>
            <form action="ppmb-cekakhir.php" method="get">
                <input type="search" name="no_ujian" />
                <input type="submit" value="Cari..." />
                {literal}<script type="text/javascript">$('input[name="no_ujian"]').focus();</script>{/literal}
            </form>
        </td>
    </tr>
</table>
            
{if isset($cm)}
    <table>
        <tr>
            <td>Nama :</td>
            <td>{$cm['NM_C_MHS']}</td>
        </tr>
        <tr>
            <td>Ruang :</td>
            <td>{$cm['NM_RUANG']}</td>
        </tr>
        <tr>
            <td>Lokasi :</td>
            <td>{$cm['LOKASI']} <br/> {$cm['ALAMAT']}</td>
        </tr>
        <tr>
            <td>Tanggal Ujian :</td>
            <td>{$cm['TGL_TEST']}</td>
        </tr>
    </table>
{/if}