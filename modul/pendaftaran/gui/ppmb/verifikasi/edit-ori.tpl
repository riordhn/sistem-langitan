<div class="center_title_bar">Edit Data Pendaftar</div>

<table>
    <tr>
        <td>
            <label>Kode Voucher :</label>
        </td>
        <td>
            <form action="ppmb-verifikasi-edit.php" method="get">
            <input type="search" name="kode_voucher" {if isset($smarty.get.kode_voucher)}value="{$smarty.get.kode_voucher}"{/if} />
            <input type="submit" value="Cari..." />
            </form>
        </td>
    </tr>
</table>

{if isset($calon_mahasiswa)}

{literal}<script type="text/javascript">
$("#id_negara").change(function(){
		id_negara = $("#id_negara").val();
		id_propinsi = '';
		id_kota = '';
		datanya = "&id_negara="+id_negara+"&id_propinsi="+id_propinsi+"&id_kota="+id_kota;
       
	   $.ajax({
		type: "POST",
        url: "getPropinsi.php",
        data: "op=getPropinsi"+datanya,
        cache: false,
        success: function(data){
            $('#id_propinsi').html(data);
        }
        });	
		
		
			$.ajax({
			type: "POST",
			url: "getKota.php",
			data: "op=getKota"+datanya,
			cache: false,
			success: function(data){
				$('#id_kota').html(data);
			}
			});	
			
			
				$.ajax({
				type: "POST",
				url: "getSekolah.php",
				data: "op=getSekolah"+datanya,
				cache: false,
				success: function(data){
					$('#id_smta').html(data);
				}
				});	
});


$("#id_propinsi").change(function(){
		id_propinsi = $("#id_propinsi").val();
		id_kota = '';
		datanya = "&id_propinsi="+id_propinsi+"&id_kota="+id_kota;
       
	   $.ajax({
		type: "POST",
        url: "getKota.php",
        data: "op=getKota"+datanya,
        cache: false,
        success: function(data){
            $('#id_kota').html(data);
        }
        });	
		
				$.ajax({
				type: "POST",
				url: "getSekolah.php",
				data: "op=getSekolah"+datanya,
				cache: false,
				success: function(data){
					$('#id_smta').html(data);
				}
				})
});


$("#id_kota").change(function(){
		id_kota = $("#id_kota").val();
		datanya = "&id_kota="+id_kota;
       
	   $.ajax({
		type: "POST",
        url: "getSekolah.php",
        data: "op=getSekolah"+datanya,
        cache: false,
        success: function(data){
            $('#id_smta').html(data);
        }
        });	
});
</script>{/literal}

{literal}<style>
    #formulir table tr td { font-size: 13px; }
    #formulir table { width: 95% }
    label.error { color: #f00; font-size: 12px; }
</style>{/literal}

<h2>Kode Voucher : {$calon_mahasiswa->KODE_VOUCHER}</h2>
<h2>Pilihan Jurusan :
    {if $calon_mahasiswa->KODE_JURUSAN==1}IPA{/if}
    {if $calon_mahasiswa->KODE_JURUSAN==2}IPS{/if}
    {if $calon_mahasiswa->KODE_JURUSAN==3}IPC{/if}
</h2>

<form action="ppmb-verifikasi-edit.php?kode_voucher={$calon_mahasiswa->KODE_VOUCHER}" method="post" id="formulir">
    <input type="hidden" name="id_c_mhs" value="{$calon_mahasiswa->ID_C_MHS}" />
    <input type="hidden" name="mode" value="save_s1" />
    <table>
        <tr>
          <th colspan="2" align="center"><font color="#FFFFFF"><b>I. Data Diri</b></font></th>
        </tr>
        <tr>
            <td>Nama</td>
            <td>
                <input type="text" name="nm_c_mhs" maxlength="64" size="50" class="required" value="{$calon_mahasiswa->NM_C_MHS}" />
                <br/>
                <label for="nm_c_mhs" class="error" style="display:none">* Isi nama lengkap</label>
            </td>
        </tr>
        <tr>
            <td>Tempat dan Tanggal Lahir</td>
            <td>
                <select name="id_kota_lahir" class="required">
                    <option value=""></option>
                {for $i=0 to $provinsi_set->Count()-1}{$provinsi=$provinsi_set->Get($i)}
                    <optgroup label="{$provinsi->NM_PROVINSI}">
                    {for $j=0 to $provinsi->KOTAs->Count()-1}{$kota=$provinsi->KOTAs->Get($j)}
                        <option value="{$kota->ID_KOTA}" {if $calon_mahasiswa->ID_KOTA_LAHIR==$kota->ID_KOTA}selected="selected"{/if}>{$kota->NM_KOTA}</option>
                    {/for}
                    </optgroup>
                {/for}
                </select>,
                {html_select_date prefix="tgl_lahir_" field_order="DMY" start_year="-80" end_year="-14" time=$calon_mahasiswa->TGL_LAHIR}
                <br/>
                <label for="id_kota_lahir" class="error" style="display: none;">Pilih kota kelahiran</label>
            </td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>
                <textarea name="alamat" maxlength="250" cols="50" class="required">{$calon_mahasiswa->ALAMAT}</textarea>
            </td>
        </tr>
        <tr>
            <td>No Telp</td>
            <td><input type="text" name="telp" class="required" maxlength="32" value="{$calon_mahasiswa->TELP}" /></td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>
                <label><input type="radio" name="jenis_kelamin" value="1" class="required" {if $calon_mahasiswa->JENIS_KELAMIN==1}checked="checked"{/if} >Laki-Laki</label>
                <label><input type="radio" name="jenis_kelamin" value="2" {if $calon_mahasiswa->JENIS_KELAMIN==2}checked="checked"{/if}>Perempuan</label>
                <br/>
                <label for="jenis_kelamin" class="error" style="display: none;">Pilih salah satu</label>
        </tr>
        <tr>
            <td>Kewarganegaraan</td>
            <td>
                <label><input type="radio" name="kewarganegaraan" value="1" class="required" {if $calon_mahasiswa->KEWARGANEGARAAN==1}checked="checked"{/if} />Indonesia</label>
                <label><input type="radio" name="kewarganegaraan" value="2" {if $calon_mahasiswa->KEWARGANEGARAAN==2}checked="checked"{/if}/>W.N.A. <input type="text" name="kewarganegaraan_lain" maxlength="20" value="{$calon_mahasiswa->KEWARGANEGARAAN_LAIN}" /><label for="kewarganegaraan_lain" class="error" style="display: none;">Harus di isi</label></label>
                <br/>
                <label for="kewarganegaraan" class="error" style="display: none;">Pilih salah satu</label>
            </td>
        </tr>
        <tr>
          <td>Agama</td>
          <td>
            <label><input type="radio" name="id_agama" value="1" class="required" {if $calon_mahasiswa->ID_AGAMA==1}checked="checked"{/if} />Islam</label>
            <label><input type="radio" name="id_agama" value="2" {if $calon_mahasiswa->ID_AGAMA==2}checked="checked"{/if}/>Kristen Protestan</label>
            <label><input type="radio" name="id_agama" value="3" {if $calon_mahasiswa->ID_AGAMA==3}checked="checked"{/if}/>Kristen Katholik</label>
            <label><input type="radio" name="id_agama" value="4" {if $calon_mahasiswa->ID_AGAMA==4}checked="checked"{/if}/>Hindu</label>
            <label><input type="radio" name="id_agama" value="5" {if $calon_mahasiswa->ID_AGAMA==5}checked="checked"{/if}/>Budha</label>
            <label><input type="radio" name="id_agama" value="6" {if $calon_mahasiswa->ID_AGAMA==6}checked="checked"{/if}/>Lain-lain</label>
            <br/>
            <label for="id_agama" class="error" style="display: none;">Pilih salah satu</label></td>
        </tr>
        <tr>
          <td>Sumber Biaya</td>
          <td><label>
            <input type="radio" name="sumber_biaya" value="1" class="required" {if $calon_mahasiswa->SUMBER_BIAYA==1}checked="checked"{/if}/>
            Orang Tua</label>
            <br/>
            <label>
              <input type="radio" name="sumber_biaya" value="2" {if $calon_mahasiswa->SUMBER_BIAYA==2}checked="checked"{/if}/>
              Orang Tua Asuh</label>
            <br/>
            <label>
              <input type="radio" name="sumber_biaya" value="3" {if $calon_mahasiswa->SUMBER_BIAYA==3}checked="checked"{/if}/>
              Beasiswa :
              <input type="text" name="beasiswa2" maxlength="30" value="{$calon_mahasiswa->BEASISWA}"/>
            </label>
            <br/>
            <label>
              <input type="radio" name="sumber_biaya" value="4" {if $calon_mahasiswa->SUMBER_BIAYA==4}checked="checked"{/if}/>
              Lain-lain :
              <input type="text" name="sumber_biaya_lain" maxlength="30" value="{$calon_mahasiswa->SUMBER_BIAYA_LAIN}"/>
            </label>
            <br/>
            <label for="sumber_biaya" class="error" style="display: none;">Pilih salah satu</label></td>
        </tr>
        <tr>
          <td colspan="2" align="center" ><font color="#FFFFFF"><b>II. Data Pendidikan</b></font></td>
        </tr>
        <tr>
          <td>Tempat Sekolah (Negara)</td>
          <td>
          	<select name="id_negara" id="id_negara" class="required">
          			<option value="">-- Pilih Negara--</option>
                {for $i=0 to $negara->Count()-1}{$ps=$negara->Get($i)}          
	                <option value="{$ps->ID_NEGARA}" {if isset($id_negara_sekolah)}{if $id_negara_sekolah==$ps->ID_NEGARA}selected="selected"{/if}{/if}>{$ps->NM_NEGARA}</option>
                {/for}
          	</select>
          </td>
        </tr>
        <tr>
          <td>Tempat Sekolah (Propinsi)</td>
          <td>
          	<select name="id_propinsi" id="id_propinsi" class="required">
                <option value="">-- Pilih Propinsi--</option>
                {if isset($provinsi_sekolah_set)}
                {foreach $provinsi_sekolah_set as $p}
                <option value="{$p['ID_PROVINSI']}" {if $p['ID_PROVINSI']==$id_provinsi_sekolah}selected="selected"{/if}>{$p['NM_PROVINSI']}</option>
                {/foreach}
                {/if}
          	</select>
          </td>
        </tr>
        <tr>
          <td>Tempat Sekolah (Kota)</td>
          <td>
          	<select name="id_kota" id="id_kota" class="required">
                <option value="">-- Pilih Kota--</option>
                {if isset($kota_set)}
                {foreach $kota_set as $k}
                <option value="{$k['ID_KOTA']}" {if $k['ID_KOTA']==$id_kota_sekolah}selected="selected"{/if}>{$k['NM_KOTA']}</option>
                {/foreach}
                {/if}
          	</select>
          </td>
        </tr>
        <tr>
          <td>Asal SMTA / MA</td>
          <td>
              <select name="id_sekolah_asal" id="id_smta" class="required">
                <option value="">-- Pilih SMTA / MA--</option>
                {if isset($sekolah_set)}
                {foreach $sekolah_set as $s}
                <option value="{$s['ID_SEKOLAH']}" {if $calon_mahasiswa->ID_SEKOLAH_ASAL==$s['ID_SEKOLAH']}selected="selected"{/if}>{$s['NM_SEKOLAH']}</option>
                {/foreach}
                {/if}
              </select>
          </td>
        </tr>
        <tr class="formulir">
          <td>Jurusan SMTA / MA</td>
          <td><label>
                  <input type="radio" name="jurusan_sekolah" value="1" class="required" {if $calon_mahasiswa->JURUSAN_SEKOLAH==1}checked="checked"{/if}/>
            SMU / MA IPA</label>
            <br/>
            <label>
              <input type="radio" name="jurusan_sekolah" value="2" {if $calon_mahasiswa->JURUSAN_SEKOLAH==2}checked="checked"{/if}/>
              SMU / MA IPS</label>
            <br/>
            <label>
              <input type="radio" name="jurusan_sekolah" value="3"  {if $calon_mahasiswa->JURUSAN_SEKOLAH==3}checked="checked"{/if}/>
              SMU / MA Bahasa</label>
            <br/>
            <label>
              <input type="radio" name="jurusan_sekolah" value="4" {if $calon_mahasiswa->JURUSAN_SEKOLAH==4}checked="checked"{/if}/>
              SMK</label>
            <br/>
            <label>
              <input type="radio" name="jurusan_sekolah" value="5" {if $calon_mahasiswa->JURUSAN_SEKOLAH==5}checked="checked"{/if}/>
              Lain-lain :
              <input type="text" name="jurusan_sekolah_lain" value="{$calon_mahasiswa->JURUSAN_SEKOLAH_LAIN}" />
            </label>
            <br/>
            <label for="jurusan_sekolah" class="error" style="display: none;">Pilih salah satu</label></td>
        </tr>
        <tr class="formulir">
          <td>No &amp; Tgl Ijazah SMTA</td>
          <td> No Ijazah:
            <input type="text" name="no_ijazah" class="required" maxlength="40" value="{$calon_mahasiswa->NO_IJAZAH}" />
            <br/>
            Tgl Ijazah: {html_select_date prefix="tgl_ijazah_" field_order="DMY" start_year="-15" time=$calon_mahasiswa->TGL_IJAZAH}
          </td>
        </tr>
        <tr class="formulir">
          <td>Ijazah</td>
          <td> Tahun :
            <input type="text" name="tahun_lulus" size="4" maxlength="4" class="required number" value="{$calon_mahasiswa->TAHUN_LULUS}"/>
            <br/>
            Jumlah Mata Pelajaran :
            <input type="text" name="jumlah_pelajaran_ijazah" size="2" maxlength="2" class="required number" value="{$calon_mahasiswa->JUMLAH_PELAJARAN_IJAZAH}"/>
            <br/>
            Jumlah Nilai Akhir :
            <input type="text" name="nilai_ijazah" size="5" maxlength="5" class="required number" value="{$calon_mahasiswa->NILAI_IJAZAH}" /> 
            * Bukan rata-rata</td>
        </tr>
        <tr class="formulir">
          <td>UAN</td>
          <td> Tahun :
            <input type="text" name="tahun_uan" size="4" maxlength="4" class="required number" value="{$calon_mahasiswa->TAHUN_UAN}"/>
            <br/>
            Jumlah Mata Pelajaran :
            <input type="text" name="jumlah_pelajaran_uan" size="2" maxlength="2" class="required number" value="{$calon_mahasiswa->JUMLAH_PELAJARAN_UAN}"/>
            <br/>
            Jumlah Nilai UAN :
            <input type="text" name="nilai_uan" size="5" maxlength="5" class="required number" value="{$calon_mahasiswa->NILAI_UAN}"/>
            * Bukan rata-rata</td>
        </tr>
        <tr>
         <th colspan="2" align="center"><font color="#FFFFFF"><b>III. Data Keluarga</b></font></th>
        </tr>
        <tr class="formulir">
          <td>Nama Ayah</td>
          <td><input type="text" name="nama_ayah" maxlength="64" class="required" value="{$calon_mahasiswa->NAMA_AYAH}"/></td>
        </tr>
        <tr class="formulir">
          <td>Alamat Ayah</td>
          <td><textarea name="alamat_ayah" cols="50" rows="2" maxlength="200" class="required">{$calon_mahasiswa->ALAMAT_AYAH}</textarea>
            <br/>
            <select name="id_kota_ayah" class="required">
              <option value=""></option>
	            {for $i=0 to $provinsi_set->Count()-1}{$provinsi=$provinsi_set->Get($i)}            
	              <optgroup label="{$provinsi->NM_PROVINSI}">
    	            {for $j=0 to $provinsi->KOTAs->Count()-1}{$kota=$provinsi->KOTAs->Get($j)}                    
                      <option value="{$kota->ID_KOTA}" {if $calon_mahasiswa->ID_KOTA_AYAH==$kota->ID_KOTA}selected="selected"{/if}>{$kota->NM_KOTA}</option>                
                	{/for}
                  </optgroup>              
            {/for}            
            </select>
            Posisi Alamat :
            <select name="status_alamat_ayah" class="required">
                <option value=""></option>
                <option value="1" {if $calon_mahasiswa->STATUS_ALAMAT_AYAH==1}selected="selected"{/if}>Kota</option>
                <option value="2" {if $calon_mahasiswa->STATUS_ALAMAT_AYAH==2}selected="selected"{/if}>Desa</option>
            </select></td>
        </tr>
        <tr class="formulir">
          <td>Pendidikan Ayah</td>
          <td>
              <label><input type="radio" name="pendidikan_ayah" value="1" class="required" {if $calon_mahasiswa->PENDIDIKAN_AYAH==1}checked="checked"{/if}>Tidak Tamat SD</label>
            <label><input type="radio" name="pendidikan_ayah" value="2" {if $calon_mahasiswa->PENDIDIKAN_AYAH==2}checked="checked"{/if}>SD</label>
            <label><input type="radio" name="pendidikan_ayah" value="3" {if $calon_mahasiswa->PENDIDIKAN_AYAH==3}checked="checked"{/if}>SLTP</label>
            <label><input type="radio" name="pendidikan_ayah" value="4" {if $calon_mahasiswa->PENDIDIKAN_AYAH==4}checked="checked"{/if}>SLTA</label>
            <label><input type="radio" name="pendidikan_ayah" value="5" {if $calon_mahasiswa->PENDIDIKAN_AYAH==5}checked="checked"{/if}>Diploma</label>
            <label><input type="radio" name="pendidikan_ayah" value="6" {if $calon_mahasiswa->PENDIDIKAN_AYAH==6}checked="checked"{/if}>S1</label>
            <label><input type="radio" name="pendidikan_ayah" value="7" {if $calon_mahasiswa->PENDIDIKAN_AYAH==7}checked="checked"{/if}>S2</label>
            <label><input type="radio" name="pendidikan_ayah" value="8" {if $calon_mahasiswa->PENDIDIKAN_AYAH==8}checked="checked"{/if}>S3</label> 
              <br>
              <label for="pendidikan_ayah" class="error" style="display: none; ">Pilih salah satu</label>
          </td>
        </tr>
        <tr class="formulir">
          <td>Pekerjaan Ayah</td>
          <td><label>
                  <input type="radio" name="pekerjaan_ayah" value="1" class="required" {if $calon_mahasiswa->PEKERJAAN_AYAH==1}checked="checked"{/if} />
            Guru/Dosen Negeri</label>
            <br/>
            <label><input type="radio" name="pekerjaan_ayah" value="2" {if $calon_mahasiswa->PEKERJAAN_AYAH==2}checked="checked"{/if} />PNS Bukan Guru/Dosen</label>
            <br/>
            <label><input type="radio" name="pekerjaan_ayah" value="3" {if $calon_mahasiswa->PEKERJAAN_AYAH==3}checked="checked"{/if} />TNI / POLRI</label>
            <br/>
            <label><input type="radio" name="pekerjaan_ayah" value="4" {if $calon_mahasiswa->PEKERJAAN_AYAH==4}checked="checked"{/if} />Guru / Dosen Swasta</label>
            <br/>
            <label><input type="radio" name="pekerjaan_ayah" value="5" {if $calon_mahasiswa->PEKERJAAN_AYAH==5}checked="checked"{/if} />Karyawan Swasta</label>
            <br/>
            <label><input type="radio" name="pekerjaan_ayah" value="6" {if $calon_mahasiswa->PEKERJAAN_AYAH==6}checked="checked"{/if} />Pedagang / Wiraswasta</label>
            <br/>
            <label><input type="radio" name="pekerjaan_ayah" value="7" {if $calon_mahasiswa->PEKERJAAN_AYAH==7}checked="checked"{/if} />Petani / Nelayan</label>
            <br/>
            <label><input type="radio" name="pekerjaan_ayah" value="8" {if $calon_mahasiswa->PEKERJAAN_AYAH==8}checked="checked"{/if} />Buruh</label>
            <br/>
            <label><input type="radio" name="pekerjaan_ayah" value="9" {if $calon_mahasiswa->PEKERJAAN_AYAH==9}checked="checked"{/if} />Pensiunan PNS / TNI / POLRI</label>
            <br/>
            <label><input type="radio" name="pekerjaan_ayah" value="10" {if $calon_mahasiswa->PEKERJAAN_AYAH==10}checked="checked"{/if} />Pensiunan K.Swasta</label>
            <br/>
            <label><input type="radio" name="pekerjaan_ayah" value="11" {if $calon_mahasiswa->PEKERJAAN_AYAH==11}checked="checked"{/if} />Lain-lain :
              <input type="text" name="pekerjaan_ayah_lain" value="{$calon_mahasiswa->PEKERJAAN_AYAH_LAIN}" />
            </label>
            <br/>
            <label for="pekerjaan_ayah" class="error" style="display: none;">Pilih salah satu</label></td>
        </tr>
        <tr class="formulir">
          <td>Nama Ibu </td>
          <td><input type="text" name="nama_ibu" maxlength="64" class="required" value="{$calon_mahasiswa->NAMA_IBU}"/></td>
        </tr>
        <tr class="formulir">
          <td>Alamat Ibu</td>
          <td><textarea name="alamat_ibu" cols="50" rows="2" maxlength="200" class="required">{$calon_mahasiswa->ALAMAT_IBU}</textarea>
            <br/>
            <select name="id_kota_ibu" class="required">
                <option value=""></option>
            {for $i=0 to $provinsi_set->Count()-1}{$provinsi=$provinsi_set->Get($i)}
                <optgroup label="{$provinsi->NM_PROVINSI}">
                {for $j=0 to $provinsi->KOTAs->Count()-1}{$kota=$provinsi->KOTAs->Get($j)}
                    <option value="{$kota->ID_KOTA}" {if $calon_mahasiswa->ID_KOTA_IBU==$kota->ID_KOTA}selected="selected"{/if}>{$kota->NM_KOTA}</option>
                {/for}
                </optgroup>
            {/for}
            </select>
            Posisi Alamat :
            <select name="status_alamat_ibu" class="required">
              <option value=""></option>
              <option value="1" {if $calon_mahasiswa->STATUS_ALAMAT_IBU==1}selected="selected"{/if}>Kota</option>
              <option value="2" {if $calon_mahasiswa->STATUS_ALAMAT_IBU==2}selected="selected"{/if}>Desa</option>
            </select></td>
        </tr>
        <tr class="formulir">
          <td>Pendidikan Ibu</td>
          <td>
              <label><input type="radio" name="pendidikan_ibu" value="1" class="required" {if $calon_mahasiswa->PENDIDIKAN_IBU==1}checked="checked"{/if}>Tidak Tamat SD</label>
            <label><input type="radio" name="pendidikan_ibu" value="2" {if $calon_mahasiswa->PENDIDIKAN_IBU==2}checked="checked"{/if}>SD</label>
            <label><input type="radio" name="pendidikan_ibu" value="3" {if $calon_mahasiswa->PENDIDIKAN_IBU==3}checked="checked"{/if}>SLTP</label>
            <label><input type="radio" name="pendidikan_ibu" value="4" {if $calon_mahasiswa->PENDIDIKAN_IBU==4}checked="checked"{/if}>SLTA</label>
            <label><input type="radio" name="pendidikan_ibu" value="5" {if $calon_mahasiswa->PENDIDIKAN_IBU==5}checked="checked"{/if}>Diploma</label>
            <label><input type="radio" name="pendidikan_ibu" value="6" {if $calon_mahasiswa->PENDIDIKAN_IBU==6}checked="checked"{/if}>S1</label>
            <label><input type="radio" name="pendidikan_ibu" value="7" {if $calon_mahasiswa->PENDIDIKAN_IBU==7}checked="checked"{/if}>S2</label>
            <label><input type="radio" name="pendidikan_ibu" value="8" {if $calon_mahasiswa->PENDIDIKAN_IBU==8}checked="checked"{/if}>S3</label>
            <br>
              <label for="pendidikan_ibu" class="error" style="display: none; ">Pilih salah satu</label>
          </td>
        </tr>
        <tr class="formulir">
          <td>Pekerjaan Ibu</td>
          <td><label>
                  <input type="radio" name="pekerjaan_ibu" value="1" class="required" {if $calon_mahasiswa->PEKERJAAN_IBU==1}checked="checked"{/if} />
            Guru/Dosen Negeri</label>
            <br/>
            <label>
              <input type="radio" name="pekerjaan_ibu" value="2" {if $calon_mahasiswa->PEKERJAAN_IBU==2}checked="checked"{/if} />
             PNS Bukan Guru/Dosen</label>
            <br/>
            <label>
              <input type="radio" name="pekerjaan_ibu" value="3" {if $calon_mahasiswa->PEKERJAAN_IBU==3}checked="checked"{/if} />
              TNI / POLRI</label>
            <br/>
            <label>
              <input type="radio" name="pekerjaan_ibu" value="4" {if $calon_mahasiswa->PEKERJAAN_IBU==4}checked="checked"{/if} />
              Guru / Dosen Swasta</label>
            <br/>
            <label>
              <input type="radio" name="pekerjaan_ibu" value="5" {if $calon_mahasiswa->PEKERJAAN_IBU==5}checked="checked"{/if} />
              Karyawan Swasta</label>
            <br/>
            <label>
              <input type="radio" name="pekerjaan_ibu" value="6" {if $calon_mahasiswa->PEKERJAAN_IBU==6}checked="checked"{/if} />
              Pedagang / Wiraswasta</label>
            <br/>
            <label>
              <input type="radio" name="pekerjaan_ibu" value="7" {if $calon_mahasiswa->PEKERJAAN_IBU==7}checked="checked"{/if} />
              Petani / Nelayan</label>
            <br/>
            <label>
              <input type="radio" name="pekerjaan_ibu" value="8" {if $calon_mahasiswa->PEKERJAAN_IBU==8}checked="checked"{/if} />
              Buruh</label>
            <br/>
            <label>
              <input type="radio" name="pekerjaan_ibu" value="9" {if $calon_mahasiswa->PEKERJAAN_IBU==9}checked="checked"{/if} />
              Pensiunan PNS / TNI / POLRI</label>
            <br/>
            <label>
              <input type="radio" name="pekerjaan_ibu" value="10" {if $calon_mahasiswa->PEKERJAAN_IBU==10}checked="checked"{/if} />
              Pensiunan K.Swasta</label>
            <br/>
            <label>
              <input type="radio" name="pekerjaan_ibu" value="11" {if $calon_mahasiswa->PEKERJAAN_IBU==11}checked="checked"{/if} />
              Lain-lain :
              <input type="text" name="pekerjaan_ibu_lain" value="{$calon_mahasiswa->PEKERJAAN_IBU_LAIN}"/>
            </label>
            <br/>
            <label for="pekerjaan_ibu" class="error" style="display: none;">Pilih salah satu</label></td>
        </tr>
        <tr>
          <td>Jumlah Kakak</td>
          <td><input type="text" name="jumlah_kakak" size="2" maxlength="2" class="required number" value="{$calon_mahasiswa->JUMLAH_KAKAK}" /></td>
        </tr>
        <tr>
          <td>Jumlah Adik</td>
          <td><input type="text" name="jumlah_adik" size="2" maxlength="2" class="required number" value="{$calon_mahasiswa->JUMLAH_ADIK}" /></td>
        </tr>
        <tr>
          <th colspan="2" align="center"><font color="#FFFFFF"><b>IV. Program Studi Pilihan</b></font></th>
        </tr>
        {if $calon_mahasiswa->KODE_JURUSAN == '01'}
        <tr>
          <td>Program Studi Pilihan I</td>
          <td>
          	<select name="id_pilihan_1" class="required">
          		<option value="">-- Pilih Program Studi Pilihan 1--</option>
                {for $i=0 to $prodi_ipa_set->Count()-1}{$ps=$prodi_ipa_set->Get($i)}          
	                <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_1==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                {/for}
            </select>
          </td>
        </tr>
        <tr>
          <td>Program Studi Pilihan II</td>
          <td>
              <select name="id_pilihan_2" class="required">
                <option value="">-- Pilih Program Studi Pilihan 2--</option>
                {for $i=0 to $prodi_ipa_set->Count()-1}{$ps=$prodi_ipa_set->Get($i)}          
	              <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_2==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                {/for}
          </select></td>
        </tr>
        {/if}
        {if $calon_mahasiswa->KODE_JURUSAN == '02'}
        <tr>
          <td>Program Studi Pilihan I</td>
          <td>
          	<select name="id_pilihan_1" class="required">
          		<option value="">-- Pilih Program Studi Pilihan 1--</option>
                {for $i=0 to $prodi_ips_set->Count()-1}{$ps=$prodi_ips_set->Get($i)}          
	                <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_1==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                {/for}
            </select>
          </td>
        </tr>
        <tr>
          <td>Program Studi Pilihan II</td>
          <td>
              <select name="id_pilihan_2" class="required">
                <option value="">-- Pilih Program Studi Pilihan 2--</option>
                {for $i=0 to $prodi_ips_set->Count()-1}{$ps=$prodi_ips_set->Get($i)}          
	              <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_2==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                {/for}
          </select></td>
        </tr>
        {/if}
        {if $calon_mahasiswa->KODE_JURUSAN == '03'}
        <tr>
          <td>Program Studi Pilihan I</td>
          <td>
          	<select name="id_pilihan_1" class="required">
          		<option value="">-- Pilih Program Studi Pilihan 1--</option>
                {for $i=0 to $program_studi_set->Count()-1}{$ps=$program_studi_set->Get($i)}          
	                <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_1==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                {/for}
            </select>
          </td>
        </tr>
        <tr>
          <td>Program Studi Pilihan II</td>
          <td>
              <select name="id_pilihan_2" class="required">
                <option value="">-- Pilih Program Studi Pilihan 2--</option>
                {for $i=0 to $program_studi_set->Count()-1}{$ps=$program_studi_set->Get($i)}          
	              <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_2==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                {/for}
          </select></td>
        </tr>
        <tr>
          <td>Program Studi Pilihan III</td>
          <td>
          	<select name="id_pilihan_3" class="required">
          		<option value="">-- Pilih Program Studi Pilihan 3--</option>
                {for $i=0 to $program_studi_set->Count()-1}{$ps=$program_studi_set->Get($i)}          
	                <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_3==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                {/for}
            </select>
          </td>
        </tr>
        <tr>
          <td>Program Studi Pilihan IV</td>
          <td>
              <select name="id_pilihan_4" class="required">
                <option value="">-- Pilih Program Studi Pilihan 4--</option>
                {for $i=0 to $program_studi_set->Count()-1}{$ps=$program_studi_set->Get($i)}          
	              <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_4==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                {/for}
          </select></td>
        </tr>
        {/if}
    <tr>
      <th colspan="2" align="center"><font color="#FFFFFF"><b>V. Pernyataan</b></font></th>
    </tr>
   <tr>
        <td colspan="2">Pernahkan Saudara(i) mempunyai Nomor Induk Mahasiswa / NIM yang berbeda dengan sekarang ?
            <label><input type="radio" name="status_nim_lama" value="1" class="required" 
            {if $calon_mahasiswa->STATUS_NIM_LAMA==1}checked="checked"{/if} />Ya</label>
            <label><input type="radio" name="status_nim_lama" value="0" {if $calon_mahasiswa->STATUS_NIM_LAMA==0}checked="checked"{/if}/>Tidak</label>
            <label for="status_nim_lama" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr id="nim_lama"  {if $calon_mahasiswa->STATUS_NIM_LAMA==0}style="display: none"{/if}>
        <td>Nomor Induk Mahasiswa / NIM Lama :</td>
        <td><input type="text" name="nim_lama" value="{$calon_mahasiswa->NIM_LAMA}" /></td>
    </tr>
    <tr>
        <td colspan="2">Apakah Saudara(i) pindahan dari Perguruan Tinggi lain ?
            <label><input type="radio" name="status_pt_asal" value="1" class="required" 
            {if $calon_mahasiswa->STATUS_PT_ASAL==1}checked="checked"{/if}/>Ya</label>
            <label><input type="radio" name="status_pt_asal" value="0" {if $calon_mahasiswa->STATUS_PT_ASAL==0}checked="checked"{/if}/>Tidak</label>
            <label for="status_pt_asal" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr id="pt_asal" style="display: none;">
        <td>Nama Perguruan Tinggi Asal :</td>
        <td><input type="text" name="nama_pt_asal" maxlength="12" value="{$calon_mahasiswa->NAMA_PT_ASAL}" /></td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center">
            <input type="submit" value="Submit" />
        </td>
    </tr>
    </table>
</form>


{literal}<script type="text/javascript">
$('input:radio').click(function() {
    if ($(this).attr('name') == 'status_nim_lama') {
        if ($(this).attr('value') == 1) {
            $('tr[id="nim_lama"]').css('display', '');
            $('input[name="nim_lama"]').addClass("required");
        }
        else {
            $('tr[id="nim_lama"]').css('display', 'none');
            $('input[name="nim_lama"]').removeClass("required");
        }    
    }
        
    if ($(this).attr('name') == 'status_pt_asal') {
        if ($(this).attr('value') == 1) {
            $('tr[id="pt_asal"]').css('display', '');
            $('input[name="pt_asal"]').addClass("required");
        }
        else {
            $('tr[id="pt_asal"]').css('display', 'none');
            $('input[name="pt_asal"]').removeClass("required");
        }    
    }
        
    if ($(this).attr('name') == 'kewarganegaraan') {
        if ($(this).attr('value') == 2) {
            $('input[name="kewarganegaraan_lain"]').addClass("required");
        }
        else {
            $('input[name="kewarganegaraan_lain"]').removeClass("required");
        }    
    }
    
    if ($(this).attr('name') == 'sumber_biaya') {
        if ($(this).attr('value') == 3) {
            $('input[name="beasiswa"]').addClass("required");
            $('input[name="sumber_biaya_lain"]').removeClass("required");
        }
        else if ($(this).attr('value') == 4) {
            $('input[name="beasiswa"]').removeClass("required");
            $('input[name="sumber_biaya_lain"]').addClass("required");
        }
        else {
            $('input[name="beasiswa"]').removeClass("required");
            $('input[name="sumber_biaya_lain"]').removeClass("required");
        }    
    }
        
    if ($(this).attr('name') == 'jurusan_sekolah') {
        if ($(this).attr('value') == 5) {
            $('input[name="jurusan_sekolah_lain"]').addClass("required");
        }
        else {
            $('input[name="jurusan_sekolah_lain"]').removeClass("required");
        }
    }
    
    if ($(this).attr('name') == 'pekerjaan_ayah') {
        if ($(this).attr('value') == 11) {
            $('input[name="pekerjaan_ayah_lain"]').addClass("required");
        }
        else {
            $('input[name="pekerjaan_ayah_lain"]').removeClass("required");
        }
    }
        
    if ($(this).attr('name') == 'pekerjaan_ibu') {
        if ($(this).attr('value') == 11) {
            $('input[name="pekerjaan_ibu_lain"]').addClass("required");
        }
        else {
            $('input[name="pekerjaan_ibu_lain"]').removeClass("required");
        }
    }
});
$('#formulir').validate();
</script>{/literal}

{/if}