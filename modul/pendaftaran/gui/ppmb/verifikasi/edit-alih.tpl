<div class="center_title_bar">Edit Data Pendaftar</div>

<table>
    <tr>
        <td>
            <label>Kode Voucher :</label>
        </td>
        <td>
            <form action="ppmb-verifikasi-edit.php" method="get">
            <input type="search" name="kode_voucher" {if isset($smarty.get.kode_voucher)}value="{$smarty.get.kode_voucher}"{/if} />
            <input type="submit" value="Cari..." />
            </form>
        </td>
    </tr>
</table>{$debug_app=false}
{if isset($smarty.get.debug_app)}{$debug_app='1'}<b>DEBUG_MODE</b>{/if}
{literal}<script type="text/javascript">
$("#id_negara").change(function(){
                id_negara = $("#id_negara").val();
                id_propinsi = '';
                id_kota = '';
                datanya = "&id_negara="+id_negara+"&id_propinsi="+id_propinsi+"&id_kota="+id_kota;
       
           $.ajax({
                type: "POST",
        url: "getPropinsi.php",
        data: "op=getPropinsi"+datanya,
        cache: false,
        success: function(data){
            $('#id_propinsi').html(data);
        }
        });	
		
		
                        $.ajax({
                        type: "POST",
                        url: "getKota.php",
                        data: "op=getKota"+datanya,
                        cache: false,
                        success: function(data){
                                $('#id_kota').html(data);
                        }
                        });	
			
			
                                $.ajax({
                                type: "POST",
                                url: "getSekolah.php",
                                data: "op=getSekolah"+datanya,
                                cache: false,
                                success: function(data){
                                        $('#id_smta').html(data);
                                }
                                });	
});


$("#id_propinsi").change(function(){
                id_propinsi = $("#id_propinsi").val();
                id_kota = '';
                datanya = "&id_propinsi="+id_propinsi+"&id_kota="+id_kota;
       
           $.ajax({
                type: "POST",
        url: "getKota.php",
        data: "op=getKota"+datanya,
        cache: false,
        success: function(data){
            $('#id_kota').html(data);
        }
        });	
		
                                $.ajax({
                                type: "POST",
                                url: "getSekolah.php",
                                data: "op=getSekolah"+datanya,
                                cache: false,
                                success: function(data){
                                        $('#id_smta').html(data);
                                }
                                })
});


$("#id_kota").change(function(){
                id_kota = $("#id_kota").val();
                datanya = "&id_kota="+id_kota;
       
           $.ajax({
                type: "POST",
        url: "getSekolah.php",
        data: "op=getSekolah"+datanya,
        cache: false,
        success: function(data){
            $('#id_smta').html(data);
        }
        });	
});
    
    </script>{/literal}
    {literal}<style>
            label.error { color: #f00; font-size: 12px; }
        </style>{/literal}
        <div class="center_title_bar">Formulir Registrasi Calon Mahasiswa Baru</div>
        <h2>Kode Voucher : {$calon_mahasiswa->KODE_VOUCHER}</h2>
        <h2>Pilihan Jurusan :
        {if $calon_mahasiswa->KODE_JURUSAN==1}IPA{/if}
    {if $calon_mahasiswa->KODE_JURUSAN==2}IPS{/if}
{if $calon_mahasiswa->KODE_JURUSAN==3}IPC{/if}
</h2>

<form action="ppmb-verifikasi-edit.php?kode_voucher={$calon_mahasiswa->KODE_VOUCHER}" method="post" id="formulir">
    <input type="hidden" name="id_c_mhs" value="{$calon_mahasiswa->ID_C_MHS}" />
    <input type="hidden" name="mode" value="save_alih" />
    <table>
        <tr>
            <th colspan="2" align="center"><font color="#FFFFFF"><b>I. Data Diri</b></font></th>
        </tr>
        <tr>
            <td>Nama</td>
            <td>
                <input type="text" name="nm_c_mhs" maxlength="64" size="50" class="required" value="{$calon_mahasiswa->NM_C_MHS}"/>
                <br/>
                <label for="nm_c_mhs" class="error" style="display:none">* Isi nama lengkap</label>
            </td>
        </tr>
        <tr>
            <td>Tempat dan Tanggal Lahir</td>
            <td>
                <select name="id_kota_lahir" class="required">
                    <option value=""></option>
                    {for $i=0 to $provinsi_set->Count()-1}{$provinsi=$provinsi_set->Get($i)}
                    <optgroup label="{$provinsi->NM_PROVINSI}">
                        {for $j=0 to $provinsi->KOTAs->Count()-1}{$kota=$provinsi->KOTAs->Get($j)}
                        <option value="{$kota->ID_KOTA}" {if $calon_mahasiswa->ID_KOTA_LAHIR==$kota->ID_KOTA}selected="selected"{/if}>{$kota->NM_KOTA}</option>
                        {/for}
                    </optgroup>
                    {/for}
                </select>,
                {html_select_date prefix="tgl_lahir_" field_order="DMY" start_year="-80" end_year="-14" time=$calon_mahasiswa->TGL_LAHIR}
                <br/>
                <label for="id_kota_lahir" class="error" style="display: none;">Pilih kota kelahiran</label>
            </td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>
                <textarea name="alamat" maxlength="250" cols="50" class="required">{$calon_mahasiswa->ALAMAT}</textarea>
            </td>
        </tr>
        <tr>
            <td>No Telp</td>
            <td><input type="text" name="telp" class="required" maxlength="32" value="{$calon_mahasiswa->TELP}"/></td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>
                <label><input type="radio" name="jenis_kelamin" value="1" class="required" {if $calon_mahasiswa->JENIS_KELAMIN==1}checked="checked"{/if} >Laki-Laki</label>
                <label><input type="radio" name="jenis_kelamin" value="2" {if $calon_mahasiswa->JENIS_KELAMIN==2}checked="checked"{/if}>Perempuan</label>
                <br/>
                <label for="jenis_kelamin" class="error" style="display: none;">Pilih salah satu</label>
        </tr>
        <tr>
            <td>Kewarganegaraan</td>
            <td>
                <label><input type="radio" name="kewarganegaraan" value="1" class="required" {if $calon_mahasiswa->KEWARGANEGARAAN==1}checked="checked"{/if} />Indonesia</label>
                <label><input type="radio" name="kewarganegaraan" value="2" {if $calon_mahasiswa->KEWARGANEGARAAN==2}checked="checked"{/if}/>W.N.A. <input type="text" name="kewarganegaraan_lain" maxlength="20" value="{$calon_mahasiswa->KEWARGANEGARAAN_LAIN}" /><label for="kewarganegaraan_lain" class="error" style="display: none;">Harus di isi</label></label>
                <br/>
                <label for="kewarganegaraan" class="error" style="display: none;">Pilih salah satu</label>
            </td>
        </tr>
        <tr>
            <td>Agama</td>
            <td>
                <label><input type="radio" name="id_agama" value="1" class="required" {if $calon_mahasiswa->ID_AGAMA==1}checked="checked"{/if} />Islam</label>
                <label><input type="radio" name="id_agama" value="2" {if $calon_mahasiswa->ID_AGAMA==2}checked="checked"{/if}/>Kristen Protestan</label>
                <label><input type="radio" name="id_agama" value="3" {if $calon_mahasiswa->ID_AGAMA==3}checked="checked"{/if}/>Kristen Katholik</label>
                <label><input type="radio" name="id_agama" value="4" {if $calon_mahasiswa->ID_AGAMA==4}checked="checked"{/if}/>Hindu</label>
                <label><input type="radio" name="id_agama" value="5" {if $calon_mahasiswa->ID_AGAMA==5}checked="checked"{/if}/>Budha</label>
                <label><input type="radio" name="id_agama" value="6" {if $calon_mahasiswa->ID_AGAMA==6}checked="checked"{/if}/>Lain-lain</label>
                <br/>
                <label for="id_agama" class="error" style="display: none;">Pilih salah satu</label></td>
        </tr>
        <tr>
            <td>Sumber Biaya</td>
            <td><label>
                    <input type="radio" name="sumber_biaya" value="1" class="required" {if $calon_mahasiswa->SUMBER_BIAYA==1}checked="checked"{/if}/>
                    Orang Tua</label>
                <br/>
                <label>
                    <input type="radio" name="sumber_biaya" value="2" {if $calon_mahasiswa->SUMBER_BIAYA==2}checked="checked"{/if}/>
                    Orang Tua Asuh</label>
                <br/>
                <label>
                    <input type="radio" name="sumber_biaya" value="3" {if $calon_mahasiswa->SUMBER_BIAYA==3}checked="checked"{/if}/>
                    Beasiswa :
                    <input type="text" name="beasiswa" maxlength="30" value="{$calon_mahasiswa->BEASISWA}"/>
                </label>
                <br/>
                <label>
                    <input type="radio" name="sumber_biaya" value="4" {if $calon_mahasiswa->SUMBER_BIAYA==4}checked="checked"{/if}/>
                    Lain-lain :
                    <input type="text" name="sumber_biaya_lain" maxlength="30" value="{$calon_mahasiswa->SUMBER_BIAYA_LAIN}"/>
                </label>
                <br/>
                <label for="sumber_biaya" class="error" style="display: none;">Pilih salah satu</label></td>
        </tr>

        <tr>
            <th colspan="2" align="center"><font color="#FFFFFF"><b>II. Data Pendidikan</b></font></th>
        </tr>
        <tr>
            <td>Perguruan Tinggi</td>
            <td>
                <input type="text" name="ptn_s1" maxlength="30" size="30" value="{$cmp.PTN_S1}" class="required"/>
            </td>
        </tr>
        <tr>
            <td>Status Perguruan Tinggi</td>
            <td>
                <label><input type="radio" name="status_ptn_s1" value="1" {if $cmp.STATUS_PTN_S1==1}checked="checked"{/if} class="required"/>Negeri</label>
                <label><input type="radio" name="status_ptn_s1" value="2" {if $cmp.STATUS_PTN_S1==2}checked="checked"{/if}/>Swasta</label>
                <label><input type="radio" name="status_ptn_s1" value="3" {if $cmp.STATUS_PTN_S1==3}checked="checked"{/if}/>Luar Negeri</label>
                <br/>
                <label for="status_ptn_s1" class="error" style="display:none;">Pilih salah satu</label>
            </td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td><input type="text" name="prodi_s1" maxlength="30" size="30" value="{$cmp.PRODI_S1}" class="required"/></td>
        </tr>
        <tr>
            <td>Tanggal Masuk</td>
            <td>{html_select_date prefix="tgl_masuk_s1_" field_order="DMY" start_year=-30 time=$cmp.TGL_MASUK_S1}</td>
        </tr>
        <tr>
            <td>Tanggal Lulus</td>
            <td>{html_select_date prefix="tgl_lulus_s1_" field_order="DMY" start_year=-30 time=$cmp.TGL_LULUS_S1}</td>
        </tr>
        <tr>
            <td>Lama Studi</td>
            <td><input type="text" name="lama_studi_s1" maxlength="4" size="4" class="number" value="{$cmp.LAMA_STUDI_S1}" /> tahun</td>
        </tr>
        <tr>
            <td>Index Prestasi</td>
            <td><input type="text" name="ip_s1" maxlength="5" size="5" class="number" value="{$cmp.IP_S1}" /></td>
        </tr>
        <tr>
            <th colspan="2" align="center"><font color="#FFFFFF"><b>III. Data Keluarga</b></font></th>
        </tr>
        <tr class="formulir">
            <td>Nama Ayah</td>
            <td><input type="text" name="nama_ayah" maxlength="64" class="required" value="{$calon_mahasiswa->NAMA_AYAH}"/></td>
        </tr>
        <tr class="formulir">
            <td>Alamat Ayah</td>
            <td><textarea name="alamat_ayah" cols="50" rows="2" maxlength="200" class="required">{$calon_mahasiswa->ALAMAT_AYAH}</textarea>
                <br/>
                <select name="id_kota_ayah" class="required">
                    <option value=""></option>
                    {for $i=0 to $provinsi_set->Count()-1}{$provinsi=$provinsi_set->Get($i)}            
                    <optgroup label="{$provinsi->NM_PROVINSI}">
                        {for $j=0 to $provinsi->KOTAs->Count()-1}{$kota=$provinsi->KOTAs->Get($j)}                    
                        <option value="{$kota->ID_KOTA}" {if $calon_mahasiswa->ID_KOTA_AYAH==$kota->ID_KOTA}selected="selected"{/if}>{$kota->NM_KOTA}</option>                
                        {/for}
                    </optgroup>              
                    {/for}            
                </select>
                Posisi Alamat :
                <select name="status_alamat_ayah" class="required">
                    <option value=""></option>
                    <option value="1" {if $calon_mahasiswa->STATUS_ALAMAT_AYAH==1}selected="selected"{/if}>Kota</option>
                    <option value="2" {if $calon_mahasiswa->STATUS_ALAMAT_AYAH==2}selected="selected"{/if}>Desa</option>
                </select></td>
        </tr>
        <tr class="formulir">
            <td>Pendidikan Ayah</td>
            <td>
                <label><input type="radio" name="pendidikan_ayah" value="1" class="required" {if $calon_mahasiswa->PENDIDIKAN_AYAH==1}checked="checked"{/if}>Tidak Tamat SD</label>
                <label><input type="radio" name="pendidikan_ayah" value="2" {if $calon_mahasiswa->PENDIDIKAN_AYAH==2}checked="checked"{/if}>SD</label>
                <label><input type="radio" name="pendidikan_ayah" value="3" {if $calon_mahasiswa->PENDIDIKAN_AYAH==3}checked="checked"{/if}>SLTP</label>
                <label><input type="radio" name="pendidikan_ayah" value="4" {if $calon_mahasiswa->PENDIDIKAN_AYAH==4}checked="checked"{/if}>SLTA</label>
                <label><input type="radio" name="pendidikan_ayah" value="5" {if $calon_mahasiswa->PENDIDIKAN_AYAH==5}checked="checked"{/if}>Diploma</label>
                <label><input type="radio" name="pendidikan_ayah" value="6" {if $calon_mahasiswa->PENDIDIKAN_AYAH==6}checked="checked"{/if}>S1</label>
                <label><input type="radio" name="pendidikan_ayah" value="7" {if $calon_mahasiswa->PENDIDIKAN_AYAH==7}checked="checked"{/if}>S2</label>
                <label><input type="radio" name="pendidikan_ayah" value="8" {if $calon_mahasiswa->PENDIDIKAN_AYAH==8}checked="checked"{/if}>S3</label> 
                <br>
                <label for="pendidikan_ayah" class="error" style="display: none; ">Pilih salah satu</label>
            </td>
        </tr>
        <tr class="formulir">
            <td>Pekerjaan Ayah</td>
            <td><label>
                    <input type="radio" name="pekerjaan_ayah" value="1" class="required" {if $calon_mahasiswa->PEKERJAAN_AYAH==1}checked="checked"{/if} />
                    Guru/Dosen Negeri</label>
                <br/>
                <label><input type="radio" name="pekerjaan_ayah" value="2" {if $calon_mahasiswa->PEKERJAAN_AYAH==2}checked="checked"{/if} />PNS Bukan Guru/Dosen</label>
                <br/>
                <label><input type="radio" name="pekerjaan_ayah" value="3" {if $calon_mahasiswa->PEKERJAAN_AYAH==3}checked="checked"{/if} />TNI / POLRI</label>
                <br/>
                <label><input type="radio" name="pekerjaan_ayah" value="4" {if $calon_mahasiswa->PEKERJAAN_AYAH==4}checked="checked"{/if} />Guru / Dosen Swasta</label>
                <br/>
                <label><input type="radio" name="pekerjaan_ayah" value="5" {if $calon_mahasiswa->PEKERJAAN_AYAH==5}checked="checked"{/if} />Karyawan Swasta</label>
                <br/>
                <label><input type="radio" name="pekerjaan_ayah" value="6" {if $calon_mahasiswa->PEKERJAAN_AYAH==6}checked="checked"{/if} />Pedagang / Wiraswasta</label>
                <br/>
                <label><input type="radio" name="pekerjaan_ayah" value="7" {if $calon_mahasiswa->PEKERJAAN_AYAH==7}checked="checked"{/if} />Petani / Nelayan</label>
                <br/>
                <label><input type="radio" name="pekerjaan_ayah" value="8" {if $calon_mahasiswa->PEKERJAAN_AYAH==8}checked="checked"{/if} />Buruh</label>
                <br/>
                <label><input type="radio" name="pekerjaan_ayah" value="9" {if $calon_mahasiswa->PEKERJAAN_AYAH==9}checked="checked"{/if} />Pensiunan PNS / TNI / POLRI</label>
                <br/>
                <label><input type="radio" name="pekerjaan_ayah" value="10" {if $calon_mahasiswa->PEKERJAAN_AYAH==10}checked="checked"{/if} />Pensiunan K.Swasta</label>
                <br/>
                <label><input type="radio" name="pekerjaan_ayah" value="11" {if $calon_mahasiswa->PEKERJAAN_AYAH==11}checked="checked"{/if} />Lain-lain :
                    <input type="text" name="pekerjaan_ayah_lain" value="{$calon_mahasiswa->PEKERJAAN_AYAH_LAIN}" />
                </label>
                <br/>
                <label for="pekerjaan_ayah" class="error" style="display: none;">Pilih salah satu</label></td>
        </tr>
        <tr class="formulir">
            <td>Nama Ibu </td>
            <td><input type="text" name="nama_ibu" maxlength="64" class="required" value="{$calon_mahasiswa->NAMA_IBU}"/></td>
        </tr>
        <tr class="formulir">
            <td>Alamat Ibu</td>
            <td><textarea name="alamat_ibu" cols="50" rows="2" maxlength="200" class="required">{$calon_mahasiswa->ALAMAT_IBU}</textarea>
                <br/>
                <select name="id_kota_ibu" class="required">
                    <option value=""></option>
                    {for $i=0 to $provinsi_set->Count()-1}{$provinsi=$provinsi_set->Get($i)}
                    <optgroup label="{$provinsi->NM_PROVINSI}">
                        {for $j=0 to $provinsi->KOTAs->Count()-1}{$kota=$provinsi->KOTAs->Get($j)}
                        <option value="{$kota->ID_KOTA}" {if $calon_mahasiswa->ID_KOTA_IBU==$kota->ID_KOTA}selected="selected"{/if}>{$kota->NM_KOTA}</option>
                        {/for}
                    </optgroup>
                    {/for}
                </select>
                Posisi Alamat :
                <select name="status_alamat_ibu" class="required">
                    <option value=""></option>
                    <option value="1" {if $calon_mahasiswa->STATUS_ALAMAT_IBU==1}selected="selected"{/if}>Kota</option>
                    <option value="2" {if $calon_mahasiswa->STATUS_ALAMAT_IBU==2}selected="selected"{/if}>Desa</option>
                </select></td>
        </tr>
        <tr class="formulir">
            <td>Pendidikan Ibu</td>
            <td>
                <label><input type="radio" name="pendidikan_ibu" value="1" class="required" {if $calon_mahasiswa->PENDIDIKAN_IBU==1}checked="checked"{/if}>Tidak Tamat SD</label>
                <label><input type="radio" name="pendidikan_ibu" value="2" {if $calon_mahasiswa->PENDIDIKAN_IBU==2}checked="checked"{/if}>SD</label>
                <label><input type="radio" name="pendidikan_ibu" value="3" {if $calon_mahasiswa->PENDIDIKAN_IBU==3}checked="checked"{/if}>SLTP</label>
                <label><input type="radio" name="pendidikan_ibu" value="4" {if $calon_mahasiswa->PENDIDIKAN_IBU==4}checked="checked"{/if}>SLTA</label>
                <label><input type="radio" name="pendidikan_ibu" value="5" {if $calon_mahasiswa->PENDIDIKAN_IBU==5}checked="checked"{/if}>Diploma</label>
                <label><input type="radio" name="pendidikan_ibu" value="6" {if $calon_mahasiswa->PENDIDIKAN_IBU==6}checked="checked"{/if}>S1</label>
                <label><input type="radio" name="pendidikan_ibu" value="7" {if $calon_mahasiswa->PENDIDIKAN_IBU==7}checked="checked"{/if}>S2</label>
                <label><input type="radio" name="pendidikan_ibu" value="8" {if $calon_mahasiswa->PENDIDIKAN_IBU==8}checked="checked"{/if}>S3</label>
                <br>
                <label for="pendidikan_ibu" class="error" style="display: none; ">Pilih salah satu</label>
            </td>
        </tr>
        <tr class="formulir">
            <td>Pekerjaan Ibu</td>
            <td><label>
                    <input type="radio" name="pekerjaan_ibu" value="1" class="required" {if $calon_mahasiswa->PEKERJAAN_IBU==1}checked="checked"{/if} />
                    Guru/Dosen Negeri</label>
                <br/>
                <label>
                    <input type="radio" name="pekerjaan_ibu" value="2" {if $calon_mahasiswa->PEKERJAAN_IBU==2}checked="checked"{/if} />
                    PNS Bukan Guru/Dosen</label>
                <br/>
                <label>
                    <input type="radio" name="pekerjaan_ibu" value="3" {if $calon_mahasiswa->PEKERJAAN_IBU==3}checked="checked"{/if} />
                    TNI / POLRI</label>
                <br/>
                <label>
                    <input type="radio" name="pekerjaan_ibu" value="4" {if $calon_mahasiswa->PEKERJAAN_IBU==4}checked="checked"{/if} />
                    Guru / Dosen Swasta</label>
                <br/>
                <label>
                    <input type="radio" name="pekerjaan_ibu" value="5" {if $calon_mahasiswa->PEKERJAAN_IBU==5}checked="checked"{/if} />
                    Karyawan Swasta</label>
                <br/>
                <label>
                    <input type="radio" name="pekerjaan_ibu" value="6" {if $calon_mahasiswa->PEKERJAAN_IBU==6}checked="checked"{/if} />
                    Pedagang / Wiraswasta</label>
                <br/>
                <label>
                    <input type="radio" name="pekerjaan_ibu" value="7" {if $calon_mahasiswa->PEKERJAAN_IBU==7}checked="checked"{/if} />
                    Petani / Nelayan</label>
                <br/>
                <label>
                    <input type="radio" name="pekerjaan_ibu" value="8" {if $calon_mahasiswa->PEKERJAAN_IBU==8}checked="checked"{/if} />
                    Buruh</label>
                <br/>
                <label>
                    <input type="radio" name="pekerjaan_ibu" value="9" {if $calon_mahasiswa->PEKERJAAN_IBU==9}checked="checked"{/if} />
                    Pensiunan PNS / TNI / POLRI</label>
                <br/>
                <label>
                    <input type="radio" name="pekerjaan_ibu" value="10" {if $calon_mahasiswa->PEKERJAAN_IBU==10}checked="checked"{/if} />
                    Pensiunan K.Swasta</label>
                <br/>
                <label>
                    <input type="radio" name="pekerjaan_ibu" value="11" {if $calon_mahasiswa->PEKERJAAN_IBU==11}checked="checked"{/if} />
                    Lain-lain :
                    <input type="text" name="pekerjaan_ibu_lain" value="{$calon_mahasiswa->PEKERJAAN_IBU_LAIN}"/>
                </label>
                <br/>
                <label for="pekerjaan_ibu" class="error" style="display: none;">Pilih salah satu</label></td>
        </tr>
        <tr>
            <td>Penghasilan Orang Tua/Bulan</td>
            <td>
                <label><input type="radio" name="penghasilan_ortu" value="1" {if $calon_mahasiswa->PENGHASILAN_ORTU==1}checked="checked"{/if} class="required"/>&gt; 7,5 juta</label>
                <label><input type="radio" name="penghasilan_ortu" value="2" {if $calon_mahasiswa->PENGHASILAN_ORTU==2}checked="checked"{/if}/>2,5 - 7,5 juta</label>
                <label><input type="radio" name="penghasilan_ortu" value="3" {if $calon_mahasiswa->PENGHASILAN_ORTU==3}checked="checked"{/if}/>1,35 - 2,5 juta</label>
                <label><input type="radio" name="penghasilan_ortu" value="4" {if $calon_mahasiswa->PENGHASILAN_ORTU==4}checked="checked"{/if}/>&lt; 1,35 juta</label>
                <br/>
                <label class="error" for="penghasilan_ortu" style="display: none;">Pilih Salah satu</label>
            </td>
        </tr>
        <tr>
            <td>Jumlah Kakak</td>
            <td><input type="text" name="jumlah_kakak" size="2" maxlength="2" class="required number" value="{$calon_mahasiswa->JUMLAH_KAKAK}" /></td>
        </tr>
        <tr>
            <td>Jumlah Adik</td>
            <td><input type="text" name="jumlah_adik" size="2" maxlength="2" class="required number" value="{$calon_mahasiswa->JUMLAH_ADIK}" /></td>
        </tr>
        <tr>
            <th colspan="2" align="center"><font color="#FFFFFF"><b>IV. Program Studi Pilihan</b></font></th>
        </tr>
        {literal}<script type="text/javascript">
            function load_biaya(id_program_studi, field)
            {
                if (id_program_studi > 0)
                {
                    $.get('getSP3.php?id_program_studi=' + id_program_studi, function(data){
                        $(field).attr('minimal', data);
                        $(field + '_min').html('Minimal : ' + data);
                    });
                }
                else
                {
                    $(field).attr('minimal', 0);
                    $(field + '_min').html('Minimal : 0');
                }
            }
            </script>{/literal}
            {if $calon_mahasiswa->KODE_JURUSAN == '01'}
                <tr>
                    <td>Program Studi Pilihan I</td>
                    <td>
                        <select name="id_pilihan_1" class="required">
                            <option value="">-- Pilih Program Studi Pilihan 1--</option>
                            {for $i=0 to $prodi_ipa_set->Count()-1}{$ps=$prodi_ipa_set->Get($i)}          
                            <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_1==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                            {/for}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Program Studi Pilihan II</td>
                    <td>
                        <select name="id_pilihan_2" class="required">
                            <option value="">-- Pilih Program Studi Pilihan 2--</option>
                            {for $i=0 to $prodi_ipa_set->Count()-1}{$ps=$prodi_ipa_set->Get($i)}          
                            <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_2==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                            {/for}
                        </select></td>
                </tr>
            {/if}
            {if $calon_mahasiswa->KODE_JURUSAN == '02'}
                <tr>
                    <td>Program Studi Pilihan I</td>
                    <td>
                        <select name="id_pilihan_1" class="required">
                            <option value="">-- Pilih Program Studi Pilihan 1--</option>
                            {for $i=0 to $prodi_ips_set->Count()-1}{$ps=$prodi_ips_set->Get($i)}          
                            <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_1==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                            {/for}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Program Studi Pilihan II</td>
                    <td>
                        <select name="id_pilihan_2" class="required">
                            <option value="">-- Pilih Program Studi Pilihan 2--</option>
                            {for $i=0 to $prodi_ips_set->Count()-1}{$ps=$prodi_ips_set->Get($i)}          
                            <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_2==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                            {/for}
                        </select></td>
                </tr>
            {/if}
            {if $calon_mahasiswa->KODE_JURUSAN == '03'}
                <tr>
                    <td>Program Studi Pilihan I</td>
                    <td>
                        <select name="id_pilihan_1" class="required">
                            <option value="">-- Pilih Program Studi Pilihan 1--</option>
                            {for $i=0 to $program_studi_set->Count()-1}{$ps=$program_studi_set->Get($i)}          
                            <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_1==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                            {/for}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Program Studi Pilihan II</td>
                    <td>
                        <select name="id_pilihan_2" class="required">
                            <option value="">-- Pilih Program Studi Pilihan 2--</option>
                            {for $i=0 to $program_studi_set->Count()-1}{$ps=$program_studi_set->Get($i)}          
                            <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_2==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                            {/for}
                        </select></td>
                </tr>
                <tr>
                    <td>Program Studi Pilihan III</td>
                    <td>
                        <select name="id_pilihan_3" class="required">
                            <option value="">-- Pilih Program Studi Pilihan 3--</option>
                            {for $i=0 to $program_studi_set->Count()-1}{$ps=$program_studi_set->Get($i)}          
                            <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_3==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                            {/for}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Program Studi Pilihan IV</td>
                    <td>
                        <select name="id_pilihan_4" class="required">
                            <option value="">-- Pilih Program Studi Pilihan 4--</option>
                            {for $i=0 to $program_studi_set->Count()-1}{$ps=$program_studi_set->Get($i)}          
                            <option value="{$ps->ID_PROGRAM_STUDI}" {if $calon_mahasiswa->ID_PILIHAN_4==$ps->ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps->NM_PROGRAM_STUDI}</option>
                            {/for}
                        </select></td>
                </tr>
            {/if}
            {if $debug_app}
                <tr>
                    <th colspan="2" align="center"><font color="#FFFFFF"><b>KESANGGUPAN PEMBAYARAN SP3</b></font></th>
                </tr>
                <tr>
                    <td>SP3 Program Studi Pilihan 1</td>
                    <td>
                        <input type="text" name="sp3_1" id="sp3_1" /> <label id="sp3_1_min"></label>
                    </td>
                </tr>
                <tr>
                    <td>SP3 Program Studi Pilihan 2</td>
                    <td>
                        <input type="text" name="sp3_2" id="sp3_2" /> <label id="sp3_2_min"></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><b>NB: Nilai kesanggupan SP3 akan mengikuti nilai minimal secara otomatis apabila inputan kurang dari nilai minimal.</b></td>
                </tr>
            {/if}
            <tr>
                <td colspan="2" style="text-align: center">
                    {if $calon_mahasiswa->NO_UJIAN == ''}
                        <input type="submit" value="Submit" />
                    {else}
                        ANDA SUDAH DAPAT NOMER UJIAN, DATA TIDAK BISA DI EDIT.
                    {/if}
                </td>
            </tr>
        </table>
    </form>


    {literal}<script type="text/javascript">
    $('input:radio').click(function() {
        if ($(this).attr('name') == 'status_nim_lama') {
            if ($(this).attr('value') == 1) {
                $('tr[id="nim_lama"]').css('display', '');
                $('input[name="nim_lama"]').addClass("required");
            }
            else {
                $('tr[id="nim_lama"]').css('display', 'none');
                $('input[name="nim_lama"]').removeClass("required");
            }    
        }
        
        if ($(this).attr('name') == 'status_pt_asal') {
            if ($(this).attr('value') == 1) {
                $('tr[id="pt_asal"]').css('display', '');
                $('input[name="pt_asal"]').addClass("required");
            }
            else {
                $('tr[id="pt_asal"]').css('display', 'none');
                $('input[name="pt_asal"]').removeClass("required");
            }    
        }
        
        if ($(this).attr('name') == 'kewarganegaraan') {
            if ($(this).attr('value') == 2) {
                $('input[name="kewarganegaraan_lain"]').addClass("required");
            }
            else {
                $('input[name="kewarganegaraan_lain"]').removeClass("required");
            }    
        }
    
        if ($(this).attr('name') == 'sumber_biaya') {
            if ($(this).attr('value') == 3) {
                $('input[name="beasiswa"]').addClass("required");
                $('input[name="sumber_biaya_lain"]').removeClass("required");
            }
            else if ($(this).attr('value') == 4) {
                $('input[name="beasiswa"]').removeClass("required");
                $('input[name="sumber_biaya_lain"]').addClass("required");
            }
            else {
                $('input[name="beasiswa"]').removeClass("required");
                $('input[name="sumber_biaya_lain"]').removeClass("required");
            }    
        }
        
        if ($(this).attr('name') == 'jurusan_sekolah') {
            if ($(this).attr('value') == 5) {
                $('input[name="jurusan_sekolah_lain"]').addClass("required");
            }
            else {
                $('input[name="jurusan_sekolah_lain"]').removeClass("required");
            }
        }
    
        if ($(this).attr('name') == 'pekerjaan_ayah') {
            if ($(this).attr('value') == 11) {
                $('input[name="pekerjaan_ayah_lain"]').addClass("required");
            }
            else {
                $('input[name="pekerjaan_ayah_lain"]').removeClass("required");
            }
        }
        
        if ($(this).attr('name') == 'pekerjaan_ibu') {
            if ($(this).attr('value') == 11) {
                $('input[name="pekerjaan_ibu_lain"]').addClass("required");
            }
            else {
                $('input[name="pekerjaan_ibu_lain"]').removeClass("required");
            }
        }
    });
    $('#formulir').validate();
        </script>{/literal}
