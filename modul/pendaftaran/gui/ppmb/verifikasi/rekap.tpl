<div class="center_title_bar">Rekap Verifikasi Pendaftar</div>

<table style="width: 100%">
    <tr>
        <th style="font-size: 20px">IPS</th>
        <th style="font-size: 20px">IPA</th>
        <th style="font-size: 20px">IPC</th>
    </tr>
    <tr>
        <td style="font-size: 56px; text-align: center">{$ips}</td>
        <td style="font-size: 56px; text-align: center">{$ipa}</td>
        <td style="font-size: 56px; text-align: center">{$ipc}</td>
    </tr>
</table>