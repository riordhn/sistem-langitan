<div class="center_title_bar">Verifikasi Pendaftar</div>

<table>
    <tr>
        <td>
            <label>Kode Voucher :</label>
        </td>
        <td>
            <form action="ppmb-verifikasi.php" method="get">
            <input type="search" name="kode_voucher" {if isset($smarty.get.kode_voucher)}value="{$smarty.get.kode_voucher}"{/if} />
            <input type="submit" value="Cari..." />
            </form>
        </td>
    </tr>
</table>

{if isset($calon_mahasiswa)}

{if $calon_mahasiswa['NM_C_MHS'] == ''}
    <h2>Mahasiswa belum melakukan registrasi online.</h2>
{else}

{literal}<style>
    #formulir table tr td { font-size: 13px; }
    #formulir table { width: 95% }
    label.error { color: #f00; font-size: 12px; }
</style>{/literal}

<h2>Pilihan Jurusan :
    {if $calon_mahasiswa['ID_JALUR']==4}ALIH JENIS - {/if}
    {if $calon_mahasiswa['ID_JALUR']==5}DIPLOMA - {/if}
    {if $calon_mahasiswa['ID_JALUR']==23}PROFESI{/if}
    {if $calon_mahasiswa['KODE_JURUSAN']==1}IPA{/if}
    {if $calon_mahasiswa['KODE_JURUSAN']==2}IPS{/if}
    {if $calon_mahasiswa['KODE_JURUSAN']==3}IPC{/if}
</h2>
<h2>Nomer Ujian : {$calon_mahasiswa['NO_UJIAN']}</h2>
<form action="ppmb-verifikasi.php?kode_voucher={$calon_mahasiswa['KODE_VOUCHER']}" method="post" id="formulir">
    <input type="hidden" name="id_c_mhs" value="{$calon_mahasiswa['ID_C_MHS']}" />
    <input type="hidden" name="kode_jalur" value="{$calon_mahasiswa['KODE_JURUSAN']}" />
    <input type="hidden" name="mode" value="save_verifikasi" />
    <table>
        <tr>
          <td colspan="2" align="center" bgcolor="#233D0E"><font color="#FFFFFF"><b>I. Data Diri</b></font></td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>{$calon_mahasiswa['NM_C_MHS']}</td>
        </tr>
        <tr>
          <td colspan="2" align="center" bgcolor="#233D0E"><font color="#FFFFFF"><b>II. Program Studi Pilihan</b></font></td>
        </tr>
        <tr>
            <td>Prodi Pilihan 1</td>
            <td>{$calon_mahasiswa['PRODI1']}</td>
        </tr>
        <tr>
            <td>Prodi Pilihan 2</td>
            <td>{$calon_mahasiswa['PRODI2']}</td>
        </tr>
        <tr>
            <td>Prodi Pilihan 3</td>
            <td>{$calon_mahasiswa['PRODI3']}</td>
        </tr>
        <tr>
            <td>Prodi Pilihan 4</td>
            <td>{$calon_mahasiswa['PRODI4']}</td>
        </tr>
        <tr>
            <td colspan="2" align="center" bgcolor="#233D0E"><font color="#FFFFFF"><b>III. Kesanggupan SP3</b></font></td>
        </tr>
        <tr>
            <td>SP3 untuk Prodi Pilihan 1</td>
            <td>
                <input type="text" name="sp3_1_view" value="{$calon_mahasiswa['SP3_1']}" style="font-size: 150%"/><br/>
                <label>Minimal: {number_format($sp3_1_min, 0, ",", ".")}</label><br/>
                <input type="text" name="sp3_1" class="required" style="display: none;" value="{$calon_mahasiswa['SP3_1']}" min="{$sp3_1_min}" />
            </td>
        </tr>
        {if $calon_mahasiswa['ID_JALUR'] != 6 && $calon_mahasiswa['ID_JALUR'] != 23 && $calon_mahasiswa['ID_JALUR'] == 24}
        <tr>
            <td>SP3 untuk Prodi Pilihan 2</td>
            <td>
                <input type="text" name="sp3_2_view" value="{$calon_mahasiswa['SP3_2']}" style="font-size: 150%"/><br/>
                <label>Minimal: {number_format($sp3_2_min, 0, ",", ".")}</label><br/>
                <input type="text" name="sp3_2" class="required" style="display: none;" value="{$calon_mahasiswa['SP3_2']}" min="{$sp3_2_min}" />
            </td>
        </tr>
        {if $calon_mahasiswa['KODE_JURUSAN'] == '03'}
        <tr>
            <td>SP3 untuk Prodi Pilihan 3</td>
            <td>
                <input type="text" name="sp3_3_view" value="{$calon_mahasiswa['SP3_3']}" style="font-size: 150%"/><br/>
                <label>Minimal: {number_format($sp3_3_min, 0, ",", ".")}</label><br/>
                <input type="text" name="sp3_3" class="required" style="display: none;" value="{$calon_mahasiswa['SP3_3']}" min="{$sp3_3_min}" />
            </td>
        </tr>
        <tr>
            <td>SP3 untuk Prodi Pilihan 4</td>
            <td>
                <input type="text" name="sp3_4_view" value="{$calon_mahasiswa['SP3_4']}" style="font-size: 150%"/><br/>
                <label>Minimal: {number_format($sp3_4_min, 0, ",", ".")}</label><br/>
                <input type="text" name="sp3_4" class="required" style="display: none;" value="{$calon_mahasiswa['SP3_4']}" min="{$sp3_4_min}" />
            </td>
        </tr>
        {/if}
        {else}
        <tr>
            <td>Kelompok</td>
            <td>
                <select name="sp3">
                    <option value=""></option>
                {foreach $kelompok_biaya_set as $kb}
                    <option value="{$kb.ID_KELOMPOK_BIAYA}" {if $calon_mahasiswa['SP3']==$kb.ID_KELOMPOK_BIAYA}selected="selected"{/if}>{$kb.NM_KELOMPOK_BIAYA}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        {/if}
        <tr>
            <td colspan="2" align="center" bgcolor="#233D0E"><font color="#FFFFFF"><b>IV. Berkas Verifikasi</b></font></td>
        </tr>
        <tr>

            <td colspan="2">
                <label><input type="checkbox" name="berkas_ijazah" class="required" {if $calon_mahasiswa['BERKAS_IJAZAH']==1}checked="checked"{/if}/>Ijazah</label>
                <label for="berkas_ijazah" class="error" style="display: none">Harus ada</label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <label><input type="checkbox" name="berkas_skhun" {if $calon_mahasiswa['BERKAS_SKHUN']==1}checked="checked"{/if}/>SKHUN</label>
                <label for="berkas_skhun" class="error" style="display: none">Harus ada</label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <label><input type="checkbox" name="berkas_kesanggupan_sp3" class="required" {if $calon_mahasiswa['BERKAS_KESANGGUPAN_SP3']==1}checked="checked"{/if}/>Berkas Kesanggupan SP3</label>
                <label for="berkas_kesanggupan_sp3" class="error" style="display: none">Harus ada</label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <label><input type="checkbox" name="berkas_lain" {if $calon_mahasiswa['BERKAS_LAIN']==1}checked="checked"{/if}/>Lain-lain</label>
                <input type="text" name="berkas_lain_ket" maxlength="30" value="{$calon_mahasiswa['BERKAS_LAIN_KET']}" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{if $calon_mahasiswa['KODE_JURUSAN'] == '03'}
    {literal}<script type="text/javascript">
    $('input[name="sp3_1_view"]').live('keyup', function() {
        $('input[name="sp3_1"]').attr('value', this.value.replace(/[^\d]/g, ''));
    });
    $('input[name="sp3_2_view"]').live('keyup', function() {
        $('input[name="sp3_2"]').attr('value', this.value.replace(/[^\d]/g, ''));
    });
    $('input[name="sp3_3_view"]').live('keyup', function() {
        $('input[name="sp3_3"]').attr('value', this.value.replace(/[^\d]/g, ''));
    });
    $('input[name="sp3_4_view"]').live('keyup', function() {
        $('input[name="sp3_4"]').attr('value', this.value.replace(/[^\d]/g, ''));
    });
        
    $('input[name="sp3_1_view"]').priceFormat({prefix: '',centsSeparator: '',thousandsSeparator: '.',centsLimit:0});
    $('input[name="sp3_2_view"]').priceFormat({prefix: '',centsSeparator: '',thousandsSeparator: '.',centsLimit:0});
    $('input[name="sp3_3_view"]').priceFormat({prefix: '',centsSeparator: '',thousandsSeparator: '.',centsLimit:0});
    $('input[name="sp3_4_view"]').priceFormat({prefix: '',centsSeparator: '',thousandsSeparator: '.',centsLimit:0});
    $('#formulir').validate();
    </script>{/literal}
{else}
    {literal}<script type="text/javascript">
    $('input[name="sp3_1_view"]').live('keyup', function() {
        $('input[name="sp3_1"]').attr('value', this.value.replace(/[^\d]/g, ''));
    });
    $('input[name="sp3_2_view"]').live('keyup', function() {
        $('input[name="sp3_2"]').attr('value', this.value.replace(/[^\d]/g, ''));
    });
    $('input[name="sp3_1_view"]').priceFormat({prefix: '',centsSeparator: '',thousandsSeparator: '.',centsLimit:0});
    $('input[name="sp3_2_view"]').priceFormat({prefix: '',centsSeparator: '',thousandsSeparator: '.',centsLimit:0});
    $('#formulir').validate();
    </script>{/literal}
{/if}
    
{/if}

{/if}