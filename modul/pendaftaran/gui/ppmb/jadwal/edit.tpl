<div class="center_title_bar">Jadwal Tes PPMB - Edit</div>

<form action="ppmb-jadwal.php" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_jadwal" value="{$data_jadwal_ppmb_by_id['ID_JADWAL_PPMB']}" />
    <table>
        <tr>
            <td>Lokasi</td>
            <td>
                <input name="lokasi" title="Tempat Ujian/Fakultas || contoh:Fakultas Farmasi" value="{$data_jadwal_ppmb_by_id['LOKASI']}"/>
            </td>
        </tr>
        <tr>
            <td>Lokasi</td>
            <td>
                <input name="alamat" title="Alamat Fakultas || contoh:Jalan Dharmawangsa" value="{$data_jadwal_ppmb_by_id['ALAMAT']}"/>
            </td>
        </tr>
        <tr>
            <td>Ruang</td>
            <td>
                <textarea name="ruang">{$data_jadwal_ppmb_by_id['NM_RUANG']}</textarea>
            </td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>
                {html_select_date prefix="tgl_test_" field_order="DMY" time=$data_jadwal_ppmb_by_id['TGL_TEST']}
            </td>
        </tr>
        <tr>
            <td>Kapasitas</td>
            <td><input name="kapasitas" value="{$data_jadwal_ppmb_by_id['KAPASITAS']}"/></td>
        </tr>
        <tr>
            <td>Nomer Awal</td>
            <td>
                <input name="nomer_awal" value="{$data_jadwal_ppmb_by_id['NOMER_AWAL']}"/>
            </td>
        </tr>
        <!--<tr>
            <td>Jalur</td>
            <td>
                <select name="kode_jalur">
                    <option value="1" {if $data_jadwal_ppmb_by_id['KODE_JALUR']=='1'} selected="true"{/if}>IPA</option>
                    <option value="2" {if $data_jadwal_ppmb_by_id['KODE_JALUR']=='2'} selected="true"{/if}>IPS</option>
                    <option value="3" {if $data_jadwal_ppmb_by_id['KODE_JALUR']=='3'} selected="true"{/if}>IPC</option>
                </select>
            </td>
        </tr>-->
        <tr>
            <td>Gelombang</td>
            <td>
                <select name="gelombang">
                    <!--<option value="2" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='2'} selected="true"{/if}>Mandiri 2</option>
                    <option value="9" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='9'} selected="true"{/if}>DEPAG</option> 
                    <option value="4" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='4'} selected="true"{/if}>DIPLOMA</option>
                    <option value="3" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='3'} selected="true"{/if}>ALIH JENIS</option>-->
                    <option value="3" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='5'} selected="true"{/if}>Pascasrjana</option>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <button href="ppmb-jadwal.php">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>