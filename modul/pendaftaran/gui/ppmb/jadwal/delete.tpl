<div class="center_title_bar">Jadwal Tes PPMB - Hapus Jadwal</div>

<form action="ppmb-jadwal.php" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="id_jadwal" value="{$data_jadwal_ppmb_by_id['ID_JADWAL_PPMB']}" />
    <table>
        <tr>
            <td>Lokasi</td>
            <td>{$data_jadwal_ppmb_by_id['LOKASI']} {$data_jadwal_ppmb_by_id['ALAMAT']}</td>
        </tr>
        <tr>
            <td>Ruang</td>
            <td>{$data_jadwal_ppmb_by_id['NM_RUANG']}</td>
        </tr>
        <tr>
            <td>Kapasitas</td>
            <td>{$data_jadwal_ppmb_by_id['KAPASITAS']}</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <button href="ppmb-jadwal.php">Batal</button>
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>