<div class="center_title_bar">Jadwal Tes ppmb</div>
<table style="width: 100%">
    <tr>
        <th class="center" style="width: 5%">No</th>
        <th>No.Peserta</th>
        <th width="100px">Lokasi</th>
        <th>Ruang</th>
        <th>Jadwal</th>
        <th>Terisi</th>
        <th>Kapasitas</th>
        <th>Jalur</th>
        <th>Gelombang</th>
        <th>Status</th>
        <th>Operasi</th>
    </tr>
    {$index=1}
    {foreach $data_jadwal_ppmb as $jadwal_ppmb}
        {if $jadwal_ppmb['GELOMBANG']=='5'}
            <tr {if $jadwal_ppmb['TERISI']<$jadwal_ppmb['KAPASITAS'] && $jadwal_ppmb['NOMER_AWAL'] != ''}style="background-color: #dfd;"{elseif $jadwal_ppmb['TERISI']==$jadwal_ppmb['KAPASITAS']}style="background-color: #fdd;"{/if}>
                <td class="center">{$index++}</td>
                <td>{$jadwal_ppmb['NOMER_AWAL']} - {$jadwal_ppmb['NOMER_AKHIR']}</td>
                <td>{$jadwal_ppmb['LOKASI']},{$jadwal_ppmb['ALAMAT']}</td>
                <td>{$jadwal_ppmb['NM_RUANG']}</td>
                <td>{$jadwal_ppmb['TGL_TEST']|date_format:"%d %B %Y"}</td>
                <td>{$jadwal_ppmb['TERISI']}</td>
                <td>{$jadwal_ppmb['KAPASITAS']}</td>
                <td>
                    {if $jadwal_ppmb['KODE_JALUR']=='1'}
                        IPA 
                    {else if $jadwal_ppmb['KODE_JALUR']=='2'} 
                        IPS 
                    {else if $jadwal_ppmb['KODE_JALUR']=='3'} 
                        IPC 
                    {else if $jadwal_ppmb['KODE_JALUR']=='7'} 
                        DEPAG 
                    {/if}
                </td>
                <td>
                    {if $jadwal_ppmb['GELOMBANG']=='1'}
                        MANDIRI 1
                    {else if $jadwal_ppmb['GELOMBANG']=='2'} 
                        MANDIRI 2 
                    {else if $jadwal_ppmb['GELOMBANG']=='7'}
                        DEPAG 
                    {else if $jadwal_ppmb['GELOMBANG']=='4'} 
                        DIPLOMA 
                    {else if $jadwal_ppmb['GELOMBANG']=='3'} 
                        ALIH JENIS
                    {else if $jadwal_ppmb['GELOMBANG']=='5'}
                        PASCASARJANA
                    {/if}
                </td>
                <td>
                    {if $jadwal_ppmb['STATUS_AKTIF']=='0'} 
                        Tidak Aktif 
                    {else if $jadwal_ppmb['STATUS_AKTIF']=='1'} 
                        Aktif
                    {/if}
                </td>
                <td>
                    {if $jadwal_ppmb['STATUS_AKTIF']=='0' || $jadwal_ppmb['STATUS_AKTIF']=='1'}
                        <a href="ppmb-jadwal.php?mode=edit&id_jadwal={$jadwal_ppmb['ID_JADWAL_PPMB']}">Edit</a>
                        <a href="ppmb-jadwal.php?mode=delete&id_jadwal={$jadwal_ppmb['ID_JADWAL_PPMB']}">Delete</a>
                        <a href="ppmb-jadwal.php?mode=aktifkan&id_jadwal={$jadwal_ppmb['ID_JADWAL_PPMB']}">Aktifkan</a>
                    {/if}
                </td>
            </tr>
        {/if}
    {/foreach}

    <tr>
        <td colspan="11" class="center">
            <button href="ppmb-jadwal.php?mode=add">Tambah</button>
        </td>
    </tr>
</table>