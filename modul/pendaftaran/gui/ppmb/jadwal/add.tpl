<div class="center_title_bar">Jadwal Tes PPMB - Tambah</div>

<form action="ppmb-jadwal.php" method="post">
    <table>
        <tr>
            <td>Lokasi</td>
            <td>
                <input name="lokasi" title="Tempat Ujian/Fakultas || contoh:Fakultas Farmasi" />
            </td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>
                <input name="alamat" title="Alamat Fakultas || contoh:Jalan Dharmawangsa" />
            </td>
        </tr>
        <tr>
            <td>Ruang</td>
            <td>
                <input type="text" name="ruang" title="Nama Ruang Fakultas || contoh:Ruang A"/>
            </td>
        </tr>
        <tr>
            <td>Tanggal Ujian</td>
            <td>
                {html_select_date prefix="tgl_test_" field_order="DMY"}
            </td>
        </tr>
        <tr>
            <td>Kapasitas</td>
            <td><input name="kapasitas"/></td>
        </tr>
        <!--<tr>
            <td>Jalur</td>
            <td>
                <select name="kode_jalur">
                    <option value="1">IPA</option>
                    <option value="2">IPS</option>
                    <option value="3">IPC</option>
                </select>
            </td>
        </tr>-->
        <tr>
            <td>Nomer Awal</td>
            <td>
                <input name="nomer_awal"/>
            </td>
        </tr>
        <tr>
            <td>Gelombang</td>
            <td>
                <select name="gelombang">
                    <!--<option value="2" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='2'} selected="true"{/if}>Mandiri 2</option>
                    <option value="9" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='9'} selected="true"{/if}>DEPAG</option> 
                    <option value="4" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='4'} selected="true"{/if}>DIPLOMA</option>
                    <option value="3" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='3'} selected="true"{/if}>ALIH JENIS</option>-->
                    <option value="5" {if $data_jadwal_ppmb_by_id['GELOMBANG']=='5'} selected="true" {/if}>Pascasarjana</option>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <button href="ppmb-jadwal.php">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
    <input type="hidden" name="mode" value="add" />
</form>