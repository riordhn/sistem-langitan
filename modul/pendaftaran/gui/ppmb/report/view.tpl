<div class="center_title_bar">Report Pendaftaran Mahasiswa Baru Jalur Mandiri Tahun 2011</div>
<table style="width: 100%" id="report">
    <tr>
        <td colspan="4" style="font-size: 25px;text-align: center;border: none;">
            <strong>Pendafataran Mahasiswa Bru Jalur Mandiri Tahun 2011 Distribusi Lokasi Ujian</strong>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="border: none;font-weight: bold;font-size: 20px;background-color: white;">
            <span>Kelompok IPA</span>
        </td>
    </tr>
    <tr>
        <th>Nomor Peserta</th>
        <th>Lokasi</th>
        <th>Kapasitas</th>
        <th>Jumlah Peserta</th>
    </tr>
    {$index=1}
    {foreach $data_jadwal_ppmb as $jadwal_ppmb}
        {if $jadwal_ppmb['KODE_JALUR']=='1'}
            <tr {if $index%2!=0} style="background-color: #99ff99;" {/if}>
                <td></td>
                <td>{$jadwal_ppmb['LOKASI']}</td>
                <td>{$jadwal_ppmb['KAPASITAS']}</td>
                <td>{$jadwal_ppmb['TERISI']}</td>
            </tr {$index++}>
        {/if}      
    {/foreach}
    <tr>
        <td colspan="4" style="border: none;background-color: white;"><p></p></td>
    </tr>
    <tr>
        <td colspan="4" style="border: none;font-weight: bold;font-size: 20px;background-color: white;">
            <span>Kelompok IPS</span>
        </td>
    </tr>
    <tr>
        <th>Nomor Peserta</th>
        <th>Lokasi</th>
        <th>Kapasitas</th>
        <th>Jumlah Peserta</th>
    </tr>
    {$index=1}
    {foreach $data_jadwal_ppmb as $jadwal_ppmb}
        {if $jadwal_ppmb['KODE_JALUR']=='2'}
            <tr {if $index%2!=0} style="background-color: #99ff99;" {/if}>
                <td></td>
                <td>{$jadwal_ppmb['LOKASI']}</td>
                <td>{$jadwal_ppmb['KAPASITAS']}</td>
                <td>{$jadwal_ppmb['TERISI']}</td>
            </tr {$index++}>
        {/if}      
    {/foreach}
    <tr>
        <td colspan="4" style="border: none;background-color: white;"><p></p></td>
    </tr>
    <tr>
        <td colspan="4" style="border: none;font-weight: bold;font-size: 20px;background-color: white;">
            <span>Kelompok IPC</span>
        </td>
    </tr>
    <tr>
        <th>Nomor Peserta</th>
        <th>Lokasi</th>
        <th>Kapasitas</th>
        <th>Jumlah Peserta</th>
    </tr>
    {$index=1}
    {foreach $data_jadwal_ppmb as $jadwal_ppmb}
        {if $jadwal_ppmb['KODE_JALUR']=='3'}
            <tr {if $index%2!=0} style="background-color: #99ff99;" {/if}>
                <td></td>
                <td>{$jadwal_ppmb['LOKASI']}</td>
                <td>{$jadwal_ppmb['KAPASITAS']}</td>
                <td>{$jadwal_ppmb['TERISI']}</td>
            </tr {$index++}>
        {/if}      
    {/foreach}
    <tr>
        <td colspan="4" style="border: none;background-color: white;"><p></p></td>
    </tr>
</table>
