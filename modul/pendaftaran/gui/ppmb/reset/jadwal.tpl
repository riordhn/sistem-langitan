<form action="ppmb-reset-jadwal.php" method="get">
    <table>
        <tr>
            <td>Jadwal</td>
            <td>
                <select name="id_jadwal_ppmb">
                    <option value=""></option>
                    {foreach $jadwal_set as $j}
                        <option value="{$j.ID_JADWAL_PPMB}">[{$j.NOMER_AWAL} - {$j.NOMER_AKHIR}] {$j.NM_RUANG} - {$j.LOKASI}</option>
                    {/foreach}
                </select>
                <input type="submit" value="Lihat" />
            </td>
        </tr>
    </table>
</form>
                
{if isset($plot_jadwal_set)}
    <table>
        <tr>
            <th>No</th>
            <th>No Ujian</th>
            <th>Nama</th>
            <th>Kode Voucher</th>
        </tr>
        {foreach $plot_jadwal_set as $p}
        <tr>
            <td>{$p@index+1}</td>
            <td>{$p.NO_UJIAN}</td>
            <td>{$p.NM_C_MHS}</td>
            <td>{$p.KODE_VOUCHER}</td>
        </tr>
        {/foreach}
    </table>
{/if}