<html>
    <head></head>
    <body>
        <form action="ppmb-depag-add.php" method="get">
            Kode Voucher : <input type="text" name="kode_voucher" {if isset($smarty.get.kode_voucher)}value="{$smarty.get.kode_voucher}"{/if} /> <input type="submit" value="OK" />
        </form>
        
        {if isset($cm)}
        <form action="ppmb-depag-add.php?kode_voucher={$smarty.get.kode_voucher}" method="post">
            <input type="hidden" name="id_c_mhs" value="{$cm['ID_C_MHS']}" />
            <input type="hidden" name="mode" value="save" />
        <table border="1">    
            <tr>
                <td>Nama</td>
                <td>
                    <input type="text" name="nm_c_mhs" size="50" value="{$cm['NM_C_MHS']}" />
                </td>
            </tr>
            <tr>
                <td>Pilihan Jalur Prodi</td>
                <td>
                    <select name="kode_jurusan">
                        <option value="01" {if $cm['KODE_JURUSAN'] == '01'}selected="selected"{/if}>IPA</option>
                        <option value="02" {if $cm['KODE_JURUSAN'] == '02'}selected="selected"{/if}>IPS</option>
                    </select>
                </td>
            </tr>
            
            {if $cm['KODE_JURUSAN'] != ''}
                <tr>
                    <td>Pilihan Prodi 1</td>
                    <td>
                        <select name="id_pilihan_1">
                        {foreach $program_studi_set as $ps}
                            <option value="{$ps['ID_PROGRAM_STUDI']}" {if $cm['ID_PILIHAN_1'] == $ps['ID_PROGRAM_STUDI']}selected="selected"{/if}>{$ps['NM_PROGRAM_STUDI']}</option>
                        {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Pilihan Prodi 2</td>
                    <td>
                        <select name="id_pilihan_2">
                        {foreach $program_studi_set as $ps}
                            <option value="{$ps['ID_PROGRAM_STUDI']}" {if $cm['ID_PILIHAN_2'] == $ps['ID_PROGRAM_STUDI']}selected="selected"{/if}>{$ps['NM_PROGRAM_STUDI']}</option>
                        {/foreach}
                        </select>
                    </td>
                </tr>
            {/if}
            
            <tr>
                <td colspan="2" align="center"><input type="submit" value="Simpan" /></td>
            </tr> 
        </table>
        </form>
        {/if}
    </body>
</html>