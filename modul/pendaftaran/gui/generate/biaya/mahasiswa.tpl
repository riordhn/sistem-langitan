<div class="center_title_bar">Generate Biaya Mahasiswa</div>
{literal}<script type="text/javascript">
$(document).ready(function() {
    $('#menu').hide('slow');
});
</script>{/literal}

<form action="generate-biaya-mahasiswa.php" method="get" id="filter">
<table style="margin: 5px auto;">
    <tr>
        <td>Tahun Ajaran
            <select name="id_semester">
                <option value="">--SEMESTER--</option>
            {foreach $semester_set as $s}
                <option value="{$s.ID_SEMESTER}">{$s.NM_SEMESTER} - {$s.THN_AKADEMIK_SEMESTER}</option>
            {/foreach}
            </select>
        </td>
        <td>
            Fakultas
            <select name="id_fakultas">
                <option value="">--PILIH FAKULTAS--</option>
            {foreach $fakultas_set as $f}
                <option value="{$f.ID_FAKULTAS}">{$f.NM_FAKULTAS}</option>
            {/foreach}
            </select>
            {literal}<script type="text/javascript">
                $('select[name="id_fakultas"]').change(function() {
                    $.get('getProgramStudi.php', 'id_fakultas=' + this.value, function(data) {
                        $('select[name="id_program_studi"]').html(data);
                    });
                });
            </script>{/literal}
        </td>
    </tr>
    <tr>
        <td colspan="2">
            Program Studi
            <select name="id_program_studi"></select>
            <input type="submit" value="Lihat" />
        </td>
    </tr>
</table>
</form>

{if isset($mahasiswa_set)}
<table>
    <tr>
        <th>No</th>
        <th>NIM</th>
        <th>NAMA</th>
        <th>KELOMPOK</th>
        <th>JALUR</th>
        <th>STATUS</th>
        <th></th>
    </tr>
    {foreach $mahasiswa_set as $m}
    <tr>
        <td>{$m@index + 1}</td>
        <td>{$m.NIM_MHS}</td>
        <td>{$m.NM_PENGGUNA}</td>
        <td>{$m.NM_KELOMPOK_BIAYA}</td>
        <td>{$m.NM_JALUR}</td>
        <td></td>
        <td><label><input type="checkbox" name="b{$m.ID_MHS}" />SET</label></td>
    </tr>
    {/foreach}
</table>
{/if}