<div class="center_title_bar">Verifikasi Bidikmisi</div>

<table>
    <tr>
        <td>
            <label>Nomer Ujian :</label>
        </td>
        <td>
            <form action="verifikasi-bidikmisi.php" method="get">
            <input type="search" name="no_ujian" {if isset($smarty.get.no_ujian)}value="{$smarty.get.no_ujian}"{/if} />
            <input type="submit" value="Cari..." />
            </form>
        </td>
    </tr>
</table>

{literal}<style>
.formulir tr td h3 { margin-bottom: 0px; }
</style>{/literal}

{if isset($calon_mahasiswa)}
<form action="verifikasi-bidikmisi.php" method="post">
<input type="hidden" name="mode" value="bidikmisi" />
<input type="hidden" name="id_c_mhs" value="{$calon_mahasiswa->ID_C_MHS}" />
<table class="formulir" style="width: 100%">
    <col style="width: 30px" />
    <col style="width: 250px" />
    <col style="" />
    <col style="" />
    <col style="" />
    <col style="" />
    <tr>
        <td colspan="6" class="center"><h3>IDENTITAS DIRI</h3></td>
    </tr>
    <tr>
        <td>1</td>
        <td>Nama CAMABA</td>
        <td><label>{$calon_mahasiswa->NM_C_MHS}</label></td>
    </tr>
    <tr>
        <td>2</td>
        <td>Nomor Ujian Tulis SNMPTN</td>
        <td><label>{$calon_mahasiswa->NO_UJIAN}</label></td>
    </tr>
    <tr>
        <td>3</td>
        <td>Program Studi (Diterima)</td>
        <td><label>{$calon_mahasiswa->PROGRAM_STUDI->JENJANG->NM_JENJANG} - {$calon_mahasiswa->PROGRAM_STUDI->NM_PROGRAM_STUDI}</label></td>
    </tr>
    <tr>
        <td>4</td>
        <td>Nama Ayah</td>
        <td>{$calon_mahasiswa->NAMA_AYAH}</td>
    </tr>
    <tr>
        <td>5</td>
        <td>Alamat Ayah</td>
        <td>{$calon_mahasiswa->ALAMAT_AYAH}<br/>
            {$calon_mahasiswa->KOTA_AYAH}
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>Nama Ibu </td>
        <td>{$calon_mahasiswa->NAMA_IBU}</td>
    </tr>
    <tr>
        <td>7</td>
        <td>Alamat Ibu</td>
        <td>{$calon_mahasiswa->ALAMAT_IBU}<br/>
            {$calon_mahasiswa->KOTA_IBU}
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>Jumlah Anggota Keluarga</td>
        <td>{$calon_mahasiswa->ANGGOTA_KELUARGA}</td>
    </tr>
    <tr>
        <td>9</td>
        <td>Nomor KSK</td>
        <td>{$calon_mahasiswa->NOMOR_KSK}</td>
    </tr>
    <tr>
        <td>10</td>
        <td>Sanggup Membayar SP3</td>
        <td>{$calon_mahasiswa->SP3}</td>
    </tr>
    <tr>
        <td>11</td>
        <td>Penghasilan Orang Tua/Bulan</td>
        <td>{$calon_mahasiswa->PENGHASILAN_ORTU}</td>
    </tr>
    <tr>
        <td colspan="6" class="center"><h3>PEKERJAAN / PROFESI / JABATAN ORANG TUA</h3></td>
    </tr>
    <tr>
        <td>12</td>
        <td>Pekerjaan Ayah</td>
        <td>{$calon_mahasiswa->PEKERJAAN_AYAH}</td>
    </tr>
    <tr>
        <td>13</td>
        <td>Pekerjaan Ibu</td>
        <td>{$calon_mahasiswa->PEKERJAAN_IBU}</td>
    </tr>
    <tr>
        <td>14</td>
        <td>Skala Usaha/Pekerjaan</td>
        <td>
            {$calon_mahasiswa->SKALA_PEKERJAAN_ORTU}
        </td>
    </tr>
    <tr>
        <td colspan="6" class="center"><h3>KEDIAMAN DAN KENDARAAN KELUARGA</h3></td>
    </tr>
    <tr>
        <td>15</td>
        <td>Kediaman Orang Tua</td>
        <td>{$calon_mahasiswa->KEDIAMAN_ORTU}</td>
    </tr>
    <tr>
        <td>16</td>
        <td>Luas Tanah Kediaman</td>
        <td>{$calon_mahasiswa->LUAS_TANAH}</td>
    </tr>
    <tr>
        <td>17</td>
        <td>Luas Bangunan Kediaman</td>
        <td>{$calon_mahasiswa->LUAS_BANGUNAN}
        </td>
    </tr>
    <tr>
        <td>18</td>
        <td>NJOP</td>
        <td>{$calon_mahasiswa->NJOP}</td>
    </tr>
    <tr>
        <td>19</td>
        <td>Daya Listrik</td>
        <td>{$calon_mahasiswa->LISTRIK}</td>
    </tr>
    <tr>
        <td>20</td>
        <td>Kendaraan Bermotor R4</td>
        <td>{$calon_mahasiswa->KENDARAAN_R4}</td>
    </tr>
    <tr>
        <td>21</td>
        <td>Kendaraan bermotor R2</td>
        <td>{$calon_mahasiswa->KENDARAAN_R2}</td>
    </tr>
    <tr>
        <td>22</td>
        <td>Kepemilikan Kekayaan Laiinya :</td>
        <td>{$calon_mahasiswa->KEKAYAAN_LAIN}</td>
    </tr>
    <tr>
        <td>23</td>
        <td>Informasi lainnya, sebutkan :</td>
        <td>{$calon_mahasiswa->INFO_LAIN}</td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center">
            <label><input type="radio" name="status_bidik_misi" value="1" disabled="disabled" />BIDIK MISI NASIONAL</label>
            <label><input type="radio" name="status_bidik_misi" value="2" disabled="disabled" />BIDIK MISI UNAIR</label>
            <br/>
            <input type="submit" value="Submit" disabled="disabled"/>
        </td>
    </tr>
</table>
</form>
{/if}