<div class="center_title_bar">Print Jadwal Mahasiswa</div>

<table>
    <tr>
        <td>
            <label>Nomer Ujian :</label>
        </td>
        <td>
            <form action="du-jadwal.php" method="get">
                <input type="search" name="no_ujian" {if isset($smarty.get.no_ujian)}value="{$smarty.get.no_ujian}"{/if} />
                <input type="submit" value="Cari..." />
            </form>
        </td>
    </tr>
</table>
{if isset($data_calon_mahasiswa)}
{if $status['STATUS']>=10 || $status['STATUS']==1}

<form action="du-jadwal.php?no_ujian={$data_calon_mahasiswa['NO_UJIAN']}" method="post">
    <input type="hidden" name="mode" value="terima" />
    <input type="hidden" name="id_c_mhs" value="{$data_calon_mahasiswa['ID_C_MHS']}" />
    <table>
        <tr>
            <td>NAMA</td>
            <td>{$data_calon_mahasiswa['NM_C_MHS']}</td>
        </tr>
        <tr>
            <td>NO UJIAN</td>
            <td>{$data_calon_mahasiswa['NO_UJIAN']}</td>
        </tr>
        <tr>
            <td>Terima Buku</td>
            <td><input type="checkbox" name="terima_buku" {if $data_calon_mahasiswa['TERIMA_BUKU']==1}checked="checked"{/if} /></td>
        </tr>
        <tr>
            <td>Terima Jadwal ELPT</td>
            <td><input type="checkbox" name="terima_jadwal_elpt" {if $data_calon_mahasiswa['TERIMA_JADWAL_ELPT']==1}checked="checked"{/if} /></td>
        </tr>
        <tr>
            <td>Terima Jadwal Kesehatan</td>
            <td><input type="checkbox" name="terima_jadwal_kesehatan" {if $data_calon_mahasiswa['TERIMA_JADWAL_KESEHATAN']==1}checked="checked"{/if} /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Submit" />
            </td>
        </tr>
    </table>
</form>
{if $data_jenjang=='1'||$data_jenjang=='5'}
<table>
    <tr>
        <td><a class="disable-ajax" target="_blank" href="cetak-jadwal.php?id_c_mhs={$data_calon_mahasiswa['ID_C_MHS']}">Cetak Jadwal dan Formulir Test</a></td>
    </tr>
</table>

<input type="hidden" name="id_c_mhs" value="{$data_calon_mahasiswa['ID_CALON_MAHASISWA']}" />
<table style="width: 100%;" class="formulir">
    <col style="width: 30px" />
    <col style="width: 250px" />
    <col style="" />
    <tr>
        <td colspan="3"><h3>Info Calon Mahasiswa</h3></td>
    </tr>
    <tr>
        <td>1</td>
        <td>No Ujian</td>
        <td>{$data_jadwal['NO_UJIAN']}</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Nama</td>
        <td>{$data_jadwal['NM_C_MHS']}</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Program Studi</td>
        <td>{$data_jadwal['NM_PROGRAM_STUDI']}</td>
    </tr>
    <tr>
        <td>4</td>
        <td>Jadwal Toefl</td>
        <td>Tanggal :{$data_jadwal['TGL_TOEFL']},<br/> Jam :{$data_jadwal['JAM_TOEFL']}:{$data_jadwal['MENIT_TOEFL']},<br/>Ruang :{$data_jadwal['RUANG_TOEFL']}</td>
    </tr>
    <tr>
        <td>5</td>
        <td>Jadwal Kesehatan</td>
        <td>Tanggal :{$data_jadwal['TGL_KESEHATAN']},<br/> Jam :{$data_jadwal['JAM_KESEHATAN']}:{$data_jadwal['MENIT_KESEHATAN']},<br/>Kelompok Jam :{$data_jadwal['KELOMPOK_JAM']} </td>
    </tr>

</table>
{/if}
{else}
<h2>Mahasiswa belum melalui proses sebelumnya.</h2>
{/if}
{/if}
