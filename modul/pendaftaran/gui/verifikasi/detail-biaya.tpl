<html><head><title>Cek Pembayaran</title></head><body>

<form action="detail-biaya.php" method="get">
    <table>
        <tr>
            <td>No Ujian</td>
            <td>
                <input type="text" name="no_ujian" {if isset($smarty.get.no_ujian)}value="{$smarty.get.no_ujian}" {/if} />
                <input type="submit" value="Cari" />
            </td>
        </tr>
    </table>
</form>

{if isset($pembayaran_cmhs_set)}
    <table border="1" cellpadding="2">
        <tr>
            <th>No</th>
	     <th>Id_c_mhs</th>
            <th>Detail Pembayaran</th>
            <th>Besar Pembayaran</th>
            <th>Tgl Bayar</th>
            <th>BANK</th>
            <th>VIA</th>
            <th>is tagih</th>
	     <th>no transaksi</th>
        </tr>
        {foreach $pembayaran_cmhs_set as $pc}
        <tr>
            <td>{$pc['ID_PEMBAYARAN_CMHS']}</td>
            <td>{$pc['ID_C_MHS']}</td>
            <td>{$pc['NM_BIAYA']}</td>
            <td align="right">{$pc['BESAR_BIAYA']}</td>
            <td>{$pc['TGL_BAYAR']}</td>
            <td>{$pc['NM_BANK']}</td>
            <td>{$pc['NAMA_BANK_VIA']}</td>
            <td>{$pc['IS_TAGIH']}</td>
            <td>{$pc['NO_TRANSAKSI']}</td>
        </tr>
        {/foreach}
        <tr>
            <td colspan="3" align="right">TOTAL</td>
            <td align="right">{$total_biaya}</td>
        </tr>
        <tr>
            <td colspan="3" align="right">TIDAK DITAGIH</td>
            <td align="right">{$total_tidak_tagih}</td>
        </tr>
        <tr>
            <td colspan="3" align="right">TOTAL TAGIHAN</td>
            <td align="right">{$total_tagih}</td>
        </tr>
    </table>

{/if}

</body></html>