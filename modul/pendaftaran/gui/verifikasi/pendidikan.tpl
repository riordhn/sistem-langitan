<div class="center_title_bar">Verifikasi Berkas Mahasiswa</div>

<table>
    <tr>
        <td>
            <label>Nomer Ujian :</label>
        </td>
        <td>
            <form action="verifikasi-pendidikan.php" method="get">
            <input type="search" name="no_ujian" {if isset($smarty.get.no_ujian)}value="{$smarty.get.no_ujian}"{/if} />
            <input type="submit" value="Cari..." />
            </form>
        </td>
    </tr>
</table>

{if isset($cm)}

{if $cm.STATUS >= 6}

    {if $cm.ID_JENJANG==1 || ($cm.ID_JENJANG>=4 && $cm.ID_JENJANG<=8)}
        {if $sudah_bayar==true}
            {include "gui/verifikasi/pendidikan_s1.tpl"}
        {else}
            <h2>Calon mahasiswa belum melakukan pembayaran.</h2>
            {*{include "gui/verifikasi/pendidikan_s1.tpl"}*}
        {/if}
    {elseif $cm.ID_JENJANG==2 || $cm.ID_JENJANG==3 || $cm.ID_JENJANG==9 || $cm.ID_JENJANG==10}
        {if $sudah_bayar==true}
            {include "gui/verifikasi/pendidikan_pasca.tpl"}
        {else}
            <h2>Calon mahasiswa belum melakukan pembayaran.</h2>
        {/if}
    {/if}
{else}
<h2>Verifikasi ke keuangan belum dilakukan.</h2>
{/if}

{/if}