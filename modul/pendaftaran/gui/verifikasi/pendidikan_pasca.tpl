{literal}<style type="text/css">
.center         { text-align: center; }
.formulir       { width: 100% }
.formulir tr td { font-size: 13px; }
.formulir tr td h2 { margin: 0px; }
.formulir tr td h3 { margin: 0px; }
.formulir tr td h4 { margin: 0px; }
label.error        { display: none; color: #f00; font-size: 12px; }
</style>{/literal}

{if $cm.STATUS >= 7}
<table>
    <tr>
        <td><a class="disable-ajax" target="_blank" href="cetak-biodata2.php?id_c_mhs={$cm.ID_C_MHS}">Cetak Biodata</a></td>
    </tr>
</table>
{/if}

<form action="verifikasi-pendidikan.php?no_ujian={$cm.NO_UJIAN}" method="post" id="biodata">
<input type="hidden" name="mode" value="save_biodata_pasca" />
<input type="hidden" name="id_c_mhs" value="{$cm.ID_C_MHS}" />
<table style="width: 100%;" class="formulir">
    <col style="width: 30px" />
    <col style="width: 250px" />
    <col style="" />
    <tr>
        <td colspan="3"><h2>Program Studi</h2></td>
    </tr>
    <tr>
        <td>1</td>
        <td>Fakultas</td>
        <td>{$cm.NM_FAKULTAS}</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Program Studi</td>
        <td>{$cm.NM_PROGRAM_STUDI}</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Jenjang Studi</td>
        <td>{$cm.NM_JENJANG}</td>
    </tr>
    <tr>
        <td colspan="3"><h2>Biodata Mahasiswa Baru</h2></td>
    </tr>
    <tr>
        <td>4</td>
        <td>No. Test</td>
        <td>{$cm.NO_UJIAN}</td>
    </tr>
    <tr>
        <td>5</td>
        <td>Nama Lengkap</td>
        <td><input type="text" name="nm_c_mhs" value="{$cm.NM_C_MHS}" size="50"/></td>
    </tr>
    <tr>
        <td>6</td>
        <td>No Mhs</td>
        <td></td>
    </tr>
    <tr>
        <td>7</td>
        <td>Tempat dan Tanggal Lahir</td>
        <td>
            <select name="id_kota_lahir" class="required">
                <option value=""></option>
                {foreach $provinsi_set as $p}
                    <optgroup label="{$p.NM_PROVINSI}">
                    {foreach $p.kota_set as $k}<option value="{$k.ID_KOTA}" {if $k.ID_KOTA==$cm.ID_KOTA_LAHIR}selected="selected"{/if}>{$k.NM_KOTA}</option>{/foreach}
                    </optgroup>
                {/foreach}
            </select>,
            {html_select_date prefix="tgl_lahir_" field_order="DMY" start_year="-80" end_year="-14" time=$cm.TGL_LAHIR}
            <br/>
            <label for="id_kota_lahir" class="error" style="display: none;">Pilih kota kelahiran</label>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>Alamat Mahasiswa</td>
        <td>
            <textarea name="alamat"  maxlength="200" cols="50" class="required">{$cm.ALAMAT}</textarea>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>Jenis Kelamin</td>
        <td>
            <label><input type="radio" name="jenis_kelamin" value="1" class="required" {if $cm.JENIS_KELAMIN == 1}checked="checked"{/if} />Laki-Laki</label>
            <label><input type="radio" name="jenis_kelamin" value="2" {if $cm.JENIS_KELAMIN == 2}checked="checked"{/if} />Perempuan</label>
            <br/>
            <label for="jenis_kelamin" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>9</td>
        <td>Agama</td>
        <td>{html_radios name="id_agama" options=$agama selected=$cm.ID_AGAMA}</td>
    </tr>
    <tr>
        <td>10</td>
        <td>Status Perkawinan</td>
        <td>{html_radios name="status_perkawinan" options=$status_perkawinan selected=$cm.STATUS_PERKAWINAN}</td>
    </tr>
    <tr>
        <td>11</td>
        <td>Biaya Pendidikan</td>
        <td>
            <label><input type="radio" name="sumber_biaya" value="5" class="required" {if $cm.SUMBER_BIAYA==5}checked="checked"{/if}/>Biaya Sendiri</label>
            <label><input type="radio" name="sumber_biaya" value="3" {if $cm.SUMBER_BIAYA==3}checked="checked"{/if} />Beasiswa : <input type="text" name="beasiswa" {if $cm.SUMBER_BIAYA==3}class="required" value="{$cm.BEASISWA}"{/if} /></label>
            <br/>
            <label for="sumber_biaya" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>12</td>
        <td>PT. Asal / Instansi Pengirim</td>
        <td>
            <input type="text" name="pt_instansi" maxlength="30" size="50" value="{$cm.PT_INSTANSI}" />
        </td>
    </tr>
    <tr>
        <td>13</td>
        <td>Alamat Kantor</td>
        <td>
            <textarea name="alamat_kantor" cols="50" maxlength="100" rows="2">{$cm.ALAMAT_KANTOR}</textarea>
        </td>
    </tr>
    <tr>
        <td colspan="3"><h3>Berkas Pendukung</h3></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">
            <label><input type="checkbox" name="berkas_ijazah" class="required" {if $cm.BERKAS_IJAZAH==1}checked="checked"{/if}/>Ijazah (Asli dan Foto copy)</label>
            <label for="berkas_ijazah" class="error" style="display: none">Harus ada</label>
        </td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">
            <label><input type="checkbox" name="berkas_transkrip" class="required" {if $cm.BERKAS_TRANSKRIP==1}checked="checked"{/if}/>Transkrip</label>
            <label for="berkas_transkrip" class="error" style="display: none">Harus ada</label>
        </td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">
            <label><input type="checkbox" name="berkas_kesehatan" class="required" {if $cm.BERKAS_KESEHATAN==1}checked="checked"{/if}/>Surat Kesehatan</label>
            <label for="berkas_kesehatan" class="error" style="display: none">Harus ada</label>
        </td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">
            <label><input type="checkbox" name="berkas_skck" class="required" {if $cm.BERKAS_SKCK==1}checked="checked"{/if}/>SKCK / SKKB</label>
            <label for="berkas_skck" class="error" style="display: none">Harus ada</label>
        </td>
    </tr>
</table>
<div style="text-align: center">
    <input type="submit" value="Proses" />
</div>
</form>

{literal}<script type="text/javascript">
$('input:radio').click(function() {
    if ($(this).attr('name') == 'sumber_biaya')
    {
        if ($(this).attr('value') == 3) {
            $('input[name="beasiswa"]').addClass("required");
            $('input[name="beasiswa"]').focus();
        }
        else {
            $('input[name="beasiswa"]').removeClass("required");
        }
    }
});
    
$('#biodata').validate();
</script>{/literal}