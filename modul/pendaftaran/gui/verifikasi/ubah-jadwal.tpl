{literal}<style>
    table{
        border-collapse: collapse;
    }
    td,th{
        padding:10px;
        border: #c3c4c4 1px solid;        
    }
    .success{
        padding: 10px;
        background: #daebd3;
        border: #c3c4c4 1px solid;
    }
</style>
{/literal}
{if empty($no_ujian)}
<form action="ubah_jadwal.php" method="get">
    <table>
        <tr>
            <td>Masukkan No Ujian</td>
            <td><input type="text" name="no_ujian"/></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{else}
<form action="ubah_jadwal.php" method="post">
    <table>
        <tr>
            <th colspan="2">Data Calon Mahasiswa</th>
        </tr>
        <tr>
            <td>Nama</td>
            <td>{$data_mahasiswa['NM_C_MHS']}</td>
        </tr>
        <tr>
            <td>No Ujian</td>
            <td>{$data_mahasiswa['NO_UJIAN']}</td>
        </tr>
        <tr>
            <td>Jadwal Kesehatan</td>
            <td>
                <select name="jadwal_kesehatan">
                   {foreach $jadwal_kesehatan as $jadwal}
                    <option {if $jadwal['ID_JADWAL_KESEHATAN']==$data_mahasiswa['ID_JADWAL_KESEHATAN']} selected="true" {/if} value="{$jadwal['ID_JADWAL_KESEHATAN']}">Tanggal :{$jadwal['TGL_TEST']}Kelompok : ({$jadwal['KELOMPOK_JAM']}) Jam : {$jadwal['JAM_MULAI']}:{$jadwal['MENIT_MULAI']}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Jadwal ELPT</td>
            <td>
                <select name="jadwal_elpt">
                    {foreach $jadwal_elpt as $jadwal}
                    <option {if $jadwal['ID_JADWAL_TOEFL']==$data_mahasiswa['ID_JADWAL_TOEFL']} selected="true" {/if} value="{$jadwal['ID_JADWAL_TOEFL']}"> Tanggal :{$jadwal['TGL_TEST']} Ruang : ({$jadwal['NM_RUANG']}) Jam : {$jadwal['JAM_MULAI']}:{$jadwal['MENIT_MULAI']}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="hidden" name="mode" value="save"/>
                <input type="hidden" name="no_ujian" value="{$data_mahasiswa['NO_UJIAN']}"/>
                <input type="submit" value="Ubah" style="float: right;" />
                <input type="button" onclick="javascript:location.href='ubah_jadwal.php'" style="float: right;" value="Kembali" />
            </td>
        </tr>
    </table>
</form>
<table>
    <tr>
        <td style="border: none;vertical-align: top;">
            <table>
                <tr>
                    <th colspan="6">DAFTAR JADWAL KESEHATAN</th>
                </tr>
                <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Kelompok</th>
                    <th>Jam</th>
                    <th>Kapasitas</th>
                    <th>Terisi</th>
                </tr>
                {$index=1}
                {foreach $jadwal_kesehatan as $jadwal}
                <tr>
                    <td>{$index++}</td>
                    <td>{$jadwal['TGL_TEST']}</td>
                    <td>{$jadwal['KELOMPOK_JAM']}</td>
                    <td>{$jadwal['JAM_MULAI']} : {$jadwal['MENIT_MULAI']}</td>
                    <td>{$jadwal['KAPASITAS']}</td>
                    <td>{$jadwal['TERISI']}</td>
                </tr>
                {/foreach}
            </table>
        </td>
        <td style="border: none;vertical-align: top;">
            <table>
                <tr>
                    <th colspan="6">DAFTAR JADWAL ELPT</th>
                </tr>
                <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Jam</th>
                    <th>Ruang</th>
                    <th>Kapasistas</th>
                    <th>Terisi</th>
                </tr>
                {$index=1}
                {foreach $jadwal_elpt as $jadwal}
                <tr>
                    <td>{$index++}</td>
                    <td>{$jadwal['TGL_TEST']}</td>
                    <td>{$jadwal['JAM_MULAI']} : {$jadwal['MENIT_MULAI']}</td>
                    <td>{$jadwal['NM_RUANG']}</td>
                    <td>{$jadwal['KAPASITAS']}</td>
                    <td>{$jadwal['TERISI']}</td>
                </tr>
                {/foreach}
            </table>
        </td>
    </tr>
</table>
{/if}