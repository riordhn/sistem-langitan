<div class="center_title_bar">Pengaturan Pembayaran MANDIRI</div>
{literal}<style>
    td { font-family: Arial; }
    .biaya { text-align: right; }
</style>{/literal}

<form action="verifikasi-biaya-s1.php" method="get">
<table>
    <tr>
        <td>Fakultas</td>
        <td>
            <select name="id_fakultas">
                <option value="">-- Semua Fakultas --</option>
            {foreach $fakultas_set as $f}
                <option value="{$f.ID_FAKULTAS}"
                    {if $smarty.get.id_fakultas!=""}{if $smarty.get.id_fakultas==$f.ID_FAKULTAS}selected="selected"{/if}{/if}>{$f.NM_FAKULTAS}</option>
            {/foreach}
            </select>
            <input type="submit" value="Tampilkan" />
        </td>
    </tr>
</table>
</form>

{if isset($calon_mahasiswa_set)}
<form action="verifikasi-biaya-s1.php?id_fakultas={$smarty.get.id_fakultas}" method="post">
    <input type="hidden" name="mode" value="generate" />
<table>
    <tr>
        <th>No.</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Prodi</th>
        <th>SP3</th>
        <th>Kesanggupan</th>
        <th>Sumbangan</th>
        <th>Aksi <input type="checkbox" name="c_all" /></th>
        <th>Status</th>
        {literal}<script type="text/javascript">
            $('input[name="c_all"]').change(function() {
                $('input[name^="check_"]').attr('checked', this.value);
            });
        </script>{/literal}
    </tr>
{foreach $calon_mahasiswa_set as $cm}
    <tr>
        <td>{$cm@index+1}</td>
        <td>{$cm.NO_UJIAN}</td>
        <td>{$cm.NM_C_MHS}</td>
        <td>[{$cm.NM_JENJANG}] {$cm.NM_PROGRAM_STUDI}
            <input type="hidden" name="program_studi_{$cm.ID_C_MHS}" value="{$cm.ID_PROGRAM_STUDI}" />
            {*
            <select name="program_studi_{$cm.ID_C_MHS}">
                {foreach $program_studi_set as $ps}
                <option value="{$ps.ID_PROGRAM_STUDI}" {if $ps.ID_PROGRAM_STUDI == $cm.ID_PROGRAM_STUDI}selected="selected"{/if}>[{str_pad($ps['ID_FAKULTAS'], 2, "0", 0)}] [{$ps.NM_JENJANG}] {$ps.NM_PROGRAM_STUDI}</option>
                {/foreach}
            </select>
            *}
        </td>
        <td style="text-align: right">
            {number_format($cm.SP3, 0, ',', '.')}
        </td>
        <td style="text-align: right">
            {number_format($cm.KESANGGUPAN, 0, ',', '.')}
        </td>
        <td style="text-align: right">
            {number_format($cm.KESANGGUPAN-$cm.SP3, 0, ',', '.')}
            <input type="hidden" name="sumbangan_{$cm.ID_C_MHS}" class="biaya" value="{$cm.KESANGGUPAN-$cm.SP3}" />
            {*<input type="text" name="sumbangan_{$cm.ID_C_MHS}" class="biaya" value="{$cm.BESAR_BIAYA}" />*}
        </td>
        <td>
            <label><input type="checkbox" name="check_{$cm.ID_C_MHS}" /></label>
        </td>
        <td>
            {if $cm.DETAIL_PEMBAYARAN > 0}SUDAH{/if}
        </td>
    </tr>
{/foreach}
    <tr>
        <td colspan="9" style="text-align: center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>
{*{literal}<script>
    $('.biaya').priceFormat({
        prefix: '',
        thousandsSeparator: '.',
        centsLimit: 0
    });
</script>{/literal}*}
{/if}