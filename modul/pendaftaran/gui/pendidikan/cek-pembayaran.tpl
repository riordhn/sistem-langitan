{literal}
    <style>
        table.detail{
            padding: 5px;
            border-collapse: collapse;
            font-family: arial;
        }
        table.detail th{
            background-color: #ccffcc;
            padding: 5px;
            border: 1px #999 groove;
        }
        table.detail td{
            background-color: #efefff;
            padding: 5px;
            border: 1px #999 groove;
        }
    </style>
{/literal}
<form method="post" action="cek-pembayaran.php">
    <table>
        <tr>
            <td>NIM Mahasiswa</td>
            <td><input type="text" name="nim"/></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if isset($data_pembayaran)}
    <table class="detail">
        <tr>
            <th colspan="2">Biodata</th>
        </tr>
        <tr>
            <td>NIM</td>
            <td>{$data_pembayaran[0]['NIM_MHS']}</td>            
        </tr>
	 <tr>
            <td>Nama</td>
            <td>{$data_pembayaran[0]['NM_PENGGUNA']}</td>            
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>{$data_pembayaran[0]['NM_PROGRAM_STUDI']}</td>            
        </tr>
        <tr>
            <td>Jenjang</td>
            <td>{$data_pembayaran[0]['NM_JENJANG']}</td>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>{$data_pembayaran[0]['NM_FAKULTAS']}</td>            
        </tr>
	<tr>
            <td>Status Cekal</td>
            <td>{$data_pembayaran[0]['STATUS_CEKAL']}</td>            
        </tr>
    </table>
    <p></p>
    <table class="detail">
        <tr>
            <th colspan="4">Status Pembayaran</th>
        </tr>
        <tr>
            <th>Nama Biaya</th>
            <th>Besar Biaya</th>
            <th>Status Tagihan Biaya</th>
            <th>Tanggal Bayar</th>
        </tr>
        {foreach $data_pembayaran as $data}
            {if $data['NM_BIAYA']}
                <tr>
                    <td>{$data['NM_BIAYA']}</td>
                    <td>{$data['BESAR_BIAYA']}</td>
                    <td>{$data['IS_TAGIH']}</td>
                    <td>{$data['TGL_BAYAR']}</td>
                </tr>
            {else}
                <tr>
                    <td colspan="4" style="text-align: center;">Data Pembayaran Kosong</td>
                </tr>
            {/if}

        {/foreach}
        <tr>
            <td colspan="4" style="text-align: center;"><strong>Total Biaya : {$data_pembayaran[0]['TOTAL_BIAYA']}</strong></td>
        </tr>
    </table>
{/if}