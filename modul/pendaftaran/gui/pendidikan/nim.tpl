<div class="center_title_bar">Generate NIM Mahasiswa</div>

<h3>Data Calon Mahasiswa</h3>
{literal}<style>
    .nim tr td { font-size: 85% }
</style>{/literal}
<table class="nim">
    <tr>
        <th>No</th>
        <th>ID</th>
        <th>No Ujian</th>
        <th>Nama Mahasiswa</th>
        <th>Program Studi</th>
        <th>Jenjang</th>
    </tr>
    {for $i=0 to count($calon_mahasiswa_set)-1}
    {$calon_mahasiswa=$calon_mahasiswa_set[$i]}
    <tr>
        <td>{$i+1}</td>
        <td>{$calon_mahasiswa['ID_C_MHS']}</td>
        <td>{$calon_mahasiswa['NO_UJIAN']}</td>
        <td>{$calon_mahasiswa['NM_C_MHS']}</td>
        <td>{$calon_mahasiswa['NM_PROGRAM_STUDI']}</td>
        <td>{$calon_mahasiswa['NM_JENJANG']}</td>
    </tr>
    {/for}
    <tr>
        <td colspan="6" style="text-align: center">
        <form action="pendidikan-nim.php" method="post">
            <input type="hidden" name="action" value="generate" />
            <input type="submit" value="Generate NIM" />
        </form>
        </td>
    </tr>
</table>