<div class="center_title_bar">Data Rekap Jaket dan Mutz</div>
<form  id="form-rekap-jaket-mutz" action="rekap-jaket-mutz.php" method="get">
    <table>
        <tr>
            <th colspan="2" class="center">View Data Berdasarkan Parameter</th>
        </tr>
        <tr>
            <td>Jaket atau Mutz</td>
            <td>
                <select name="jenis">
                    <option value="JAKET" {if $jenis=='JAKET'}selected="true"{/if}>Jaket</option>
                    <option value="MUTZ" {if $jenis=='MUTZ'}selected="true"{/if}>Mutz</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Tampilan Data</td>
            <td>
                <select name="tampil">
                    <option value="all" {if $tampil=='all'}selected="true"{/if}>Semua</option>
                    <option value="fakultas" {if $tampil=='fakultas'}selected="true"{/if}>Fakultas</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Kelompok</td>
            <td>
                <select name="kelompok">
                    <option value="all" {if $kelompok=='all'}selected="true"{/if}>Semua</option>
                    <option value="1" {if $kelompok=='1'}selected="true"{/if}>D3,S1 dan Profesi</option>
                    <option value="2" {if $kelompok=='2'}selected="true"{/if}>S2,S3,Spesialis dan Magister</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Rekap Selama</td>
            <td>
                <select name="range" id="range">
                    <option value="all" {if $range=='all'}selected="true"{/if}>Semua</option>
                    <option value="harian" {if $range=='harian'}selected="true"{/if}>Harian</option>
                    <option value="banyak_hari" {if $range=='banyak_hari'}selected="true"{/if}>Beberapa Hari</option>                    
                </select>
            </td>
        </tr>
        {if isset($range)&&$range!='semua'}
            <tr>
                {if $range =='harian'}
                    <td>Tanggal</td>
                    <td>
                        <select name="tanggal">
                            {foreach $data_tanggal as $data}
                                <option value="{$data['TANGGAL']}" {if $data['TANGGAL']==$tanggal}selected="true"{/if} >{$data['TANGGAL']}</option>
                            {/foreach}
                        </select>
                    </td>
                {else if $range =='banyak_hari'}
                    <td>Tanggal</td>
                    <td>
                        <select name="tanggal_mulai">
                            {foreach $data_tanggal as $data}
                                <option value="{$data['TANGGAL']}" {if $data['TANGGAL']==$tanggal_mulai}selected="true"{/if} >{$data['TANGGAL']}</option>
                            {/foreach}
                        </select>
                        -
                        <select name="tanggal_selesai">
                            {foreach $data_tanggal as $data}
                                <option value="{$data['TANGGAL']}" {if $data['TANGGAL']==$tanggal_selesai}selected="true"{/if} >{$data['TANGGAL']}</option>
                            {/foreach}
                        </select>
                    </td>
                {/if}
            </tr>
        {/if}
        <tr>
            <td colspan="2">
                <input type="submit" value="View..." style="float: right;"/>
            </td>
        </tr>
    </table>
</form>
{if isset($data_rekap)}
    <table>
        {$total=0}
        {if $jenis=='MUTZ'}
            <tr>
                {if $tampil!='all'}
                    <th>Fakultas</th>
                {/if}
                {if $kelompok!='all'}
                    <th>Jenjang</th>
                {/if}
                <th>Ukuran</th>
                <th>Jumlah</th>            
            </tr>
            {foreach $data_rekap as $data}
                <tr>
                    {if $tampil!='all'}
                        <td>{$data['NM_FAKULTAS']}</td>
                    {/if}
                    {if $kelompok!='all'}
                        <td>{$data['NM_JENJANG']}</td>
                    {/if}
                    <td>{$data['UKURAN']}</td>
                    <td><a href="rekap-jaket-mutz.php?mode=list-mhs&jenis={$jenis}&tampil={$tampil}&kelompok={$kelompok}&range={$range}&tanggal={$tanggal}&tanggal_mulai={$tanggal_mulai}&tanggal_selesai={$tanggal_selesai}&ukuran={$data['UKURAN']}&fakultas={$data['ID_FAKULTAS']}&jenjang={$data['ID_JENJANG']}">{$data['JUMLAH']}</a></td>
                </tr>
                {$total = $total+$data['JUMLAH']}
            {/foreach}
            <tr>
                {$colspan=1}
                {if $tampil!='all' && $kelompok=='all'}
                    {$colspan=2}
                {else if $tampil=='all'&& $kelompok!='all'}
                    {$colspan=2}
                {else if $tampil!='all' && $kelompok!='all'}
                    {$colspan=3}
                {/if}              
                <td colspan="{$colspan}"><strong>Jumlah</strong></td>
                <td><strong>{$total}</strong></td>
            </tr>
        {else}
            {$total=0}
            <tr>
                {if $tampil!='all'}
                    <th>Fakultas</th>
                {/if}
                {if $kelompok!='all'}
                    <th>Jenjang</th>
                {/if}                
                <th>Ukuran</th>
                <th>Jumlah</th>
            </tr>
            {foreach $data_rekap as $data}
                <tr>
                    {if $tampil!='all'}
                        <td>{$data['NM_FAKULTAS']}</td>
                    {/if}
                    {if $kelompok!='all'}
                        <td>{$data['NM_JENJANG']}</td>
                    {/if}
                    {if $data['UKURAN']=='1'}
                        <td>S</td>
                    {else if $data['UKURAN']=='2'}
                        <td>M</td>
                    {else if $data['UKURAN']=='3'}
                        <td>L</td>
                    {else if $data['UKURAN']=='4'}
                        <td>XL</td>
                    {else if $data['UKURAN']=='5'}
                        <td>XXL</td>
                    {else if $data['UKURAN']=='6'}
                        <td>XXXL</td>
                    {else}
                        <td>Ukur</td>
                    {/if}
                    <td><a href="rekap-jaket-mutz.php?mode=list-mhs&jenis={$jenis}&tampil={$tampil}&kelompok={$kelompok}&range={$range}&tanggal={$tanggal}&tanggal_mulai={$tanggal_mulai}&tanggal_selesai={$tanggal_selesai}&ukuran={$data['UKURAN']}&fakultas={$data['ID_FAKULTAS']}&jenjang={$data['ID_JENJANG']}">{$data['JUMLAH']}</a></td>
                </tr>
                {$total = $total+$data['JUMLAH']}
            {/foreach}
            <tr>
                {$colspan=1}
                {if $tampil!='all' && $kelompok=='all'}
                    {$colspan=2}
                {else if $tampil=='all'&& $kelompok!='all'}
                    {$colspan=2}
                {else if $tampil!='all' && $kelompok!='all'}
                    {$colspan=3}
                {/if}
                <td colspan="{$colspan}"><strong>Jumlah</strong></td>
                <td><strong>{$total}</strong></td>
            </tr>
        {/if}
    </table>
{/if}
{literal}
    <script>
    $('#range').change(function(){
        $('#form-rekap-jaket-mutz').submit();
    });
    </script>
{/literal}