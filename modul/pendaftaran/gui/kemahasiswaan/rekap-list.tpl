<div class="center_title_bar">Data List Mahasiswa Rekap Jaket dan Mutz</div>
<table>
    <tr>
        <th>NO</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Program Studi</th>
        <th>Fakultas</th>
        <th>Ukuran
    </tr>
    {$index=1}
    {foreach $data_calon_mahasiswa as $data}
        <tr>
            <td>{$index++}</td>
            <td>{$data['NO_UJIAN']}</td>
            <td>{$data['NM_C_MHS']}</td>
            <td>{$data['NM_PROGRAM_STUDI']}</td>
            <td>{$data['NM_FAKULTAS']|upper}</td>
            {if $jenis=='JAKET'}
                {if $data['UKURAN']=='1'}
                    <td>S</td>
                {else if $data['UKURAN']=='2'}
                    <td>M</td>
                {else if $data['UKURAN']=='3'}
                    <td>L</td>
                {else if $data['UKURAN']=='4'}
                    <td>XL</td>
                {else if $data['UKURAN']=='5'}
                    <td>XXL</td>
                {else if $data['UKURAN']=='6'}
                    <td>XXXL</td>
                {else}
                    <td>Ukur</td>
                {/if}
            {else}
                <td>{$data['UKURAN']}</td>
            {/if}
        </tr>
    {/foreach}
    <tr>
        <td colspan="6"><button href="rekap-jaket-mutz.php?jenis={$jenis}&tampil={$tampil}&kelompok={$kelompok}&range={$range}&tanggal={$tanggal}&tanggal_mulai={$tanggal_mulai}&tanggal_selesai={$tanggal_selesai}&ukuran={$data['UKURAN']}">Back</button></td>
    </tr>
</table>