<div class="center_title_bar">Pengukuran Jaket Mahasiswa</div>
<table>
    <tr>
        <td>
            <label>Nomor Ujian :</label>
        </td>
        <td>
            <form action="kemahasiswaan-jaket.php" method="get">
                <input type="search" name="no_ujian" {if isset($smarty.get.no_ujian)}value="{$smarty.get.no_ujian}"{/if} />
                <input type="submit" value="Cari..." />
            </form>
        </td>
    </tr>
</table>
{if isset($calon_mahasiswa)}

{if $calon_mahasiswa->STATUS >= 8 ||$calon_mahasiswa->STATUS==1}

<form action="kemahasiswaan-jaket.php?no_ujian={$calon_mahasiswa->NO_UJIAN}" method="post" id="ukur">
    <input type="hidden" name="mode" value="ukur" />
    <input type="hidden" name="id_c_mhs" value="{$calon_mahasiswa->ID_C_MHS}" />
    <table>
        <tr>
            <td>Nama</td>
            <td>{$calon_mahasiswa->NM_C_MHS}</td>
        </tr>
        <tr>
            <td>Status</td>
            <td style="font-size:25px;">{if $calon_mahasiswa->STATUS==8}Belum di ukur{else if $calon_mahasiswa->STATUS>8}<b>Sudah di ukur</b>{/if}</td>
        </tr>
        {if $calon_mahasiswa->STATUS>8 ||$calon_mahasiswa->STATUS==1}
        <tr>
            <td>Cetak Pengukuran Jaket Muts</td>
            <td><a href="cetak-jaket-mutz.php?id={$calon_mahasiswa->ID_C_MHS}" target="_blank" class="disable-ajax" >cetak</a></td>
        </tr>    
        {/if}
        <tr>
            <td>Ukuran Jaket</td>
            <td>
                <label><input type="radio" name="jaket" value="1" {if $calon_mahasiswa->JAKET==1}checked="true"{/if} class="required" />S</label></br>
                <label><input type="radio" name="jaket" value="2" {if $calon_mahasiswa->JAKET==2}checked="true"{/if} />M</label></br>
                <label><input type="radio" name="jaket" value="3" {if $calon_mahasiswa->JAKET==3}checked="true"{/if} />L</label></br>
                <label><input type="radio" name="jaket" value="4" {if $calon_mahasiswa->JAKET==4}checked="true"{/if} />XL</label></br>
                <label><input type="radio" name="jaket" value="5" {if $calon_mahasiswa->JAKET==5}checked="true"{/if} />XXL</label></br>
                <label><input type="radio" name="jaket" value="6" {if $calon_mahasiswa->JAKET==6}checked="true"{/if} />XXXL</label></br>
                <label><input type="radio" name="jaket" value="7" {if $calon_mahasiswa->JAKET==7}checked="true"{/if} />Ukur</label>
                <br/>
                <label for="jaket" class="error" style="display: none;">Pilih ukuran jaket</label>
            </td>
        </tr>
        <tr>
            <td>Ukuran Muts</td>
            <td>
                <label><input type="radio" name="mutz" value="5" {if $calon_mahasiswa->MUTZ==5}checked="true"{/if} class="required"/>5</label><br/>
                <label><input type="radio" name="mutz" value="6" {if $calon_mahasiswa->MUTZ==6}checked="true"{/if} />6</label><br/>
                <label><input type="radio" name="mutz" value="7" {if $calon_mahasiswa->MUTZ==7}checked="true"{/if} />7</label><br/>
                <label><input type="radio" name="mutz" value="8" {if $calon_mahasiswa->MUTZ==8}checked="true"{/if} />8</label><br/>
                <label><input type="radio" name="mutz" value="9" {if $calon_mahasiswa->MUTZ==9}checked="true"{/if} />9</label><br/>
                <label><input type="radio" name="mutz" value="10" {if $calon_mahasiswa->MUTZ==10}checked="true"{/if} />10</label><br/>
	         <label><input type="radio" name="mutz" value="12" {if $calon_mahasiswa->MUTZ==12}checked="true"{/if} />12</label><br/>
                <br/>
                <label for="mutz" class="error" style="display: none;">Pilih ukuran mutz</label>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                <input type="submit" value="Proses" />
            </td>
        </tr>
    </table>    
</form>
{literal}<script language="javascript">
$('#ukur').validate();
</script>{/literal}
{else}
<h3>Mahasiswa belum melakukan proses sebelumnya.</h3>
{/if}

{/if}