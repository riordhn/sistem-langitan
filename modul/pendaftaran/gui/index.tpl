<html>
    <head>
        <title>Pendaftaran Mahasiswa Baru - Universitas Airlangga</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="../../css/reset.css" />
        <link rel="stylesheet" type="text/css" href="../../css/text.css" />
        <link rel="stylesheet" type="text/css" href="../../css/pendaftaran.css" />
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-1.8.11.custom.css" />
        <link rel="shortcut icon" href="../../img/icon.ico" />
        <script type="text/javascript" src="../../js/jquery-1.5.1.js"></script>
        <script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.price_format.1.4.js"></script>
        <script type="text/javascript" src="../../js/pendaftaran.js"></script>
        <script type="text/javascript" src="gui/tab.js"></script>
    </head>
    <body>
        <table class="clear-margin-bottom">
            <colgroup>
                <col />
                <col class="main-width"/>
                <col />
            </colgroup>
            <thead>
                <tr>
                    <td class="header-left"></td>
                    <td class="header-center"></td>
                    <td class="header-right"></td>
                </tr>
                <tr>
                    <td class="tab-left"></td>
                    <td class="tab-center">
                        <ul>
                        {foreach $tabs as $tab}
                            <li><a rel="{$tab.ID}" href="{$tab.PAGE}">{$tab.TITLE}</a></li>
                            <li class="divider"></li>
                        {/foreach}
                            <li><a class="disable-ajax" href="../../logout.php">Logout</a></li>
                        </ul>
                    </td>
                    <td class="tab-right"></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="body-left">&nbsp;</td>
                    <td class="body-center">
                        <table class="content-table">
                            <colgroup>
                                <col />
                                <col />
                            </colgroup>
                            <tr>
                                <td colspan="2" id="breadcrumbs" class="breadcrumbs">Navigation : <a href="#">Tab</a> / <a href="#">Menu</a></td>
                            </tr>
                            <tr>
                                <td id="menu" class="menu"></td>
                                <td id="content" class="content">Loading data...</td>
                            </tr>
                        </table>
                    </td>
                    <td class="body-right">&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td class="foot-left">&nbsp;</td>
                    <td class="foot-center">
                        <div class="footer-nav">
                            <a href="#">Home</a> | <a href="#">About</a> | <a href="#">Sitemap</a> | <a href="#">RSS</a> | <a href="#">Contact Us</a>
                        </div>
                        <div class="footer">Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU <br />oleh <a target="_blank" href="http://unair.ac.id" class="disable-ajax">Universitas Airlangga</a></div>
                    </td>
                    <td class="foot-right">&nbsp;</td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>