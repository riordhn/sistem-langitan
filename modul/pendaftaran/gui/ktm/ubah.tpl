<html><head><title>Cek Status Calon Mahasiswa</title></head><body>

<form action="ktm-ubah.php" method="get">
    <table>
        <tr>
            <td>No Ujian</td>
            <td>
                <input type="text" name="no_ujian" {if isset($smarty.get.no_ujian)}value="{$smarty.get.no_ujian}" {/if} />
                <input type="submit" value="Cari" />
            </td>
        </tr>
    </table>
</form>

{if isset($cm)}
<form action="ktm-ubah.php" method="post">
<input type="hidden" name="mode" value="ganti_status" />
<input type="hidden" name="id_c_mhs" value="{$cm['ID_C_MHS']}" />
<table border="1">
    <tr>
        <td>Nama</td>
        <td>{$cm['NM_C_MHS']}</td>
    </tr>
    <tr>
        <td>Prodi</td>
        <td>{$cm['NM_PROGRAM_STUDI']}</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>{$cm['ALAMAT']}</td>
    </tr>
    <tr>
        <td>NIM</td>
        <td>{$cm['NIM_MHS']}</td>
    </tr>
    <tr>
        <td>Tes Kesehatan</td>
        <td>
            {if $cm['KESEHATAN_KESIMPULAN_AKHIR'] == 1}
                Lulus
            {else}
                Tidak Lulus
            {/if}
        </td>
    </tr>
    <tr>
        <td>Ganti Kelulusan Kesehatan</td>
        <td>
            <select name="kesehatan_kesimpulan_akhir">
                <option value="1" {if $cm['KESEHATAN_KESIMPULAN_AKHIR'] == 1}selected="selected"{/if}>Lulus</option>
                <option value="0" {if $cm['KESEHATAN_KESIMPULAN_AKHIR'] == 0}selected="selected"{/if}>Tidak Lulus</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>Status</td>
        <td>
            {if $cm['STATUS'] == 1}
                Sudah cetak
            {elseif $cm['STATUS'] == 4}
                Diterima, belum regmaba
            {elseif $cm['STATUS'] == 8}
                Sudah sidik jari, belum ukur jaket
            {elseif $cm['STATUS'] == 9}
                sudah ukur jaket, belum foto
            {elseif $cm['STATUS'] == 10}
                Sudah foto, belum print jadwal
            {elseif $cm['STATUS'] == 11}
                Sudah print jadwal, belum tes
            {elseif $cm['STATUS'] == 12}
                Sudah Tes, Siap Generate
            {elseif $cm['STATUS'] == 13}
                Siap Cetak
            {else}
                {$cm['STATUS']}
            {/if}
        </td>
    </tr>
    <tr>
        <td>Ganti Status</td>
        <td>
            <select name="status">
                <option value="1" {if $cm['STATUS'] == 1}selected="selected"{/if}>Sudah cetak</option>
                <option value="11" {if $cm['STATUS'] == 11}selected="selected"{/if}>Sudah Print, Siap Tes</option>
                <option value="12" {if $cm['STATUS'] == 12}selected="selected"{/if}>Sudah Tes, Siap Generate</option>
                <option value="13" {if $cm['STATUS'] == 13}selected="selected"{/if}>Siap Cetak</option>
            </select>
            
        </td>
    </tr>    
    <tr>
        <td></td>
        <td><input type="submit" value="Ganti" /></td>
    </tr>
    
</table>
</form>
{/if}

</body></html>