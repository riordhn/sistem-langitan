<div class="center_title_bar">Print Pemeriksaan Kesehatan Calon Mahasiswa</div>
<form action="kesehatan-print.php" method="get"><table>
        <tr>
            <td><label>Tanggal Jadwal Kesehatan</label></td>
            <td>
                <select name="tanggal">
                {foreach $data_jadwal_kesehatan as $jadwal_kesehatan}
                    {if $jadwal_kesehatan['TGL_TEST']==$tanggal}
                    <option value="{$jadwal_kesehatan['TGL_TEST']}" selected="true">{$jadwal_kesehatan['TGL_TEST']}</option>
                    {else}
                    <option value="{$jadwal_kesehatan['TGL_TEST']}">{$jadwal_kesehatan['TGL_TEST']}</option>
                    {/if}
                {/foreach}
                </select>
                {if empty($tanggal)}
                <input type="submit" value="View..."/>
                {/if}
            </td>
        </tr>
        {if isset($tanggal)}
        <tr>
            <td><label>Kelompok Jadwal Kesehatan</label></td>
            <td>           
                <select name="kelompok">
                {foreach $data_jadwal_kesehatan_tanggal as $jadwal_kesehatan}
                    {if $jadwal_kesehatan['KELOMPOK_JAM']==$kelompok}
                    <option value="{$jadwal_kesehatan['KELOMPOK_JAM']}" selected="true">{$jadwal_kesehatan['KELOMPOK_JAM']}</option>
                    {else}
                    <option value="{$jadwal_kesehatan['KELOMPOK_JAM']}">{$jadwal_kesehatan['KELOMPOK_JAM']}</option>
                    {/if}
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="View..."  style="float: right;"/></td>
        </tr>
        {/if}
    </table>
</form>
{if isset($kelompok)}
<table>
    <tr>
        <th class="center">No</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Program Studi</th>
        <th>fakultas</th>
        <th>Mata</th>
        <th>THT</th>
    </tr>
    {$index=1}
    {foreach $data_mahasiswa_kelompok as $mahasiswa_kelompok}
    <tr>
        <td>{$index++}</td>
        <td>{$mahasiswa_kelompok['NO_UJIAN']}</td>
        <td>{$mahasiswa_kelompok['NAMA']}</td>
        <td>{$mahasiswa_kelompok['NM_PROGRAM_STUDI']}</td>
        <td>{$mahasiswa_kelompok['NM_FAKULTAS']}</td>
        <td><a href="cetak-kesehatan-mata.php?id_cmhs={$mahasiswa_kelompok['ID_C_MHS']}" target="_blank" class="disable-ajax">Cetak</a></td>
        <td><a href="cetak-kesehatan-tht.php?id_cmhs={$mahasiswa_kelompok['ID_C_MHS']}" target="_blank" class="disable-ajax">Cetak</a></td>
    </tr>
    {/foreach}
</table>
{/if}