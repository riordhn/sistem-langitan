<div class="center_title_bar">Data Hasil Tes Kesehatan Calon Mahasiswa</div>
<form action="kesehatan-view.php" method="get"><table>
        <tr>
            <th colspan="2" class="center">View Data Berdasarkan Parameter</th>
        </tr>
        <tr>
            <td>Parameter</td>
            <td>
                <select name="parameter">
                    <option value="kelompok" {if isset($kelompok)}selected="true"{/if}>Kelompok</option>
                    <option value="hasil_tes" {if isset($hasil_tes)}selected="true"{/if}>Hasil Tes</option>
                    <option value="tgl_tes" {if isset($tgl_tes)}selected="true"{/if}>Tanggal Tes</option>
                    <option value="fakultas" {if isset($fakultas)} selected="true"{/if}>Fakultas</option>
                    <option value="semua" {if isset($semua)}selected="true"{/if}>Semua</option>
                </select>
            </td>
        </tr>
        {if isset($kelompok)}
        <tr>
            <td>Kelompok Jadwal Kesehatan</td>
            <td>
                <select name="kelompok">
                    {foreach $data_jadwal_kesehatan as $jadwal_kesehatan}
                    {if $jadwal_kesehatan['ID_JADWAL_KESEHATAN']==$kelompok}
                    <option value="{$jadwal_kesehatan['ID_JADWAL_KESEHATAN']}" selected="true">{$jadwal_kesehatan['TGL_TEST']} ({$jadwal_kesehatan['KELOMPOK_JAM']})</option>
                    {else}
                    <option value="{$jadwal_kesehatan['ID_JADWAL_KESEHATAN']}">{$jadwal_kesehatan['TGL_TEST']} ({$jadwal_kesehatan['KELOMPOK_JAM']})</option>
                    {/if}
                    {/foreach}
                </select>
            </td>
        </tr>
        {/if}
        {if isset($hasil_tes)}
        <tr>
            <td>Hasil Tes</td>
            <td>
                <select name="hasil_tes">
                    <option value="1" {if $hasil_tes=='1'}selected="true"{/if} >Lulus</option>
                    <option value="0" {if $hasil_tes=='0'}selected="true"{/if}>Tidak Lulus</option>
                </select>
            </td>
        </tr>
        {/if}
        {if isset($tgl_tes)}
        <tr>
            <td>Tanggal Tes</td>
            <td>
                <select name="tanggal_tes">
                    {foreach $data_tanggal_jadwal_kesehatan as $tanggal_jadwal_kesehatan}
                    {if $tanggal_jadwal_kesehatan['TGL_TEST']==$tgl_test}
                    <option value="{$tanggal_jadwal_kesehatan['TGL_TEST']}" selected="true">{$tanggal_jadwal_kesehatan['TGL_TEST']}</option>
                    {else}
                    <option value="{$tanggal_jadwal_kesehatan['TGL_TEST']}">{$tanggal_jadwal_kesehatan['TGL_TEST']}</option>
                    {/if}
                    {/foreach}
                </select>
            </td>
        </tr>
        {/if}
        {if isset($fakultas)}
        <tr>
            <td>Fakultas</td>
            <td>
                <select name="fakultas">
                    {foreach $data_fakultas as $list_fakultas}
                    {if $list_fakultas['ID_FAKULTAS']==$fakultas}
                    <option value="{$list_fakultas['ID_FAKULTAS']}" selected="true">{$list_fakultas['NM_FAKULTAS']}</option>
                    {else}
                    <option value="{$list_fakultas['ID_FAKULTAS']}">{$list_fakultas['NM_FAKULTAS']}</option>
                    {/if}
                    {/foreach}
                </select>
            </td>
        </tr>
        {/if}
        <tr>
            <td colspan="2"><input type="submit" value="View..." style="float: right;"/></td>
        </tr>
    </table>
</form>
{if isset($data_hasil_tes)}
<table>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Prodi</th>
        <th>Hasil</th>
    </tr>
    {$index=1}
    {foreach $data_hasil_tes as $hasil_tes}
    <tr>
        <td>{$index++}</td>
        <td>{$hasil_tes['NO_UJIAN']}</td>
        <td>{$hasil_tes['NM_C_MHS']}</td>
        <td>{$hasil_tes['NM_PROGRAM_STUDI']}</td>
        <td>{if $hasil_tes['KESEHATAN_KESIMPULAN_AKHIR']==1}Lulus{else}Tidak{/if}</td>
    </tr>
    {/foreach}
</table>
{/if}