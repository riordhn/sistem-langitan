<div class="center_title_bar">Master Ruang Kesehatan - Hapus Ruangan</div>
<h2>Apakah data ruangan ini akan dihapus ?</h2>
<form action="kesehatan-ruang.php" method="post">
<input type="hidden" name="mode" value="delete" />
<input type="hidden" name="id_ruang" value="{$data_ruang_kesehatan['ID_RUANG_KESEHATAN']}" />
<table>
    <tr>
        <td>Nama Ruang</td>
        <td>{$data_ruang_kesehatan['NM_RUANG']}</td>
    </tr>
    <tr>
        <td>Kapasitas</td>
        <td>{$data_ruang_kesehatan['KAPASITAS']}</td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <button href="kesehatan-ruang.php">Batal</button>
            <input type="submit" value="Hapus" />
        </td>
    </tr>
</table>
</form>