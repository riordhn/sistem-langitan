<div class="center_title_bar">Input Nilai Pemeriksaan Kesehatan Calon Mahasiswa</div>
<table>
    <tr>
        <td><label>Kelompok Jadwal Kesehatan</label></td>
        <td>
            <form action="kesehatan-nilai.php" method="get">
                <select name="jadwal_kesehatan">
                {foreach $data_jadwal_kesehatan as $jadwal_kesehatan}
                    {if $jadwal_kesehatan['ID_JADWAL_KESEHATAN']==$id_jadwal_kesehatan}
                    <option value="{$jadwal_kesehatan['ID_JADWAL_KESEHATAN']}" selected="true">{$jadwal_kesehatan['ID_JADWAL_KESEHATAN']}</option>
                    {else}
                    <option value="{$jadwal_kesehatan['ID_JADWAL_KESEHATAN']}">{$jadwal_kesehatan['ID_JADWAL_KESEHATAN']}</option>
                    {/if}
                {/foreach}
                </select>
                <input type="submit" value="View..."/>
            </form>
        </td>
    </tr>
</table>
{if isset($id_jadwal_kesehatan)}
<table>
    <tr>
        <th class="center">No</th>
        <th>Kelompok</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Program Studi</th>
        <th>fakultas</th>
        <th>Operasi</th>
    </tr>
    {$index=1}
    {foreach $data_c_mhs_by_kelompok as $c_mhs_by_kelompok}
    <tr>
        <td>{$index++}</td>
        <td>{$c_mhs_by_kelompok['KELOMPOK']}</td>
        <td>{$c_mhs_by_kelompok['NO_UJIAN']}</td>
        <td>{$c_mhs_by_kelompok['NAMA']}</td>
        <td>{$c_mhs_by_kelompok['NM_PROGRAM_STUDI']}</td>
        <td>{$c_mhs_by_kelompok['NM_FAKULTAS']}</td>
        <td><a href="kesehatan-nilai.php?mode=input&no_ujian={$c_mhs_by_kelompok['NO_UJIAN']}">Input</a></td>
    </tr>
    {/foreach}
</table>
{/if}