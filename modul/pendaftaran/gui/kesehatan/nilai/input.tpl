<div class="center_title_bar">Input Nilai Pemeriksaan Kesehatan Calon Mahasiswa</div>
<table>
    <tr>
        <td><label>Kelompok Jadwal Kesehatan</label></td>
        <td>
            <form action="kesehatan-nilai.php" method="get">
                <input type="hidden" name="mode" value="input"/>
                <select name="jadwal_kesehatan">
                    {foreach $data_jadwal_kesehatan as $jadwal_kesehatan}
                        {if $jadwal_kesehatan['ID_JADWAL_KESEHATAN']==$id_jadwal_kesehatan}
                            <option value="{$jadwal_kesehatan['ID_JADWAL_KESEHATAN']}" selected="true">{$jadwal_kesehatan['TGL_TEST']}:{$jadwal_kesehatan['KELOMPOK_JAM']}</option>
                        {else}
                            <option value="{$jadwal_kesehatan['ID_JADWAL_KESEHATAN']}">{$jadwal_kesehatan['TGL_TEST']}:{$jadwal_kesehatan['KELOMPOK_JAM']}</option>
                        {/if}
                    {/foreach}
                </select>
                <input type="submit" value="View..."/>
            </form>
        </td>
    </tr>
</table>

{if isset($id_jadwal_kesehatan)}
    <table>
        <tr><th class="center">Keterangan</th></tr>
        <tr>
            <td>&nbsp;Y&nbsp;:&nbsp;Ya&nbsp;|&nbsp;N&nbsp;:&nbsp;Normal&nbsp;|&nbsp;T&nbsp;:&nbsp;Tidak&nbsp;|&nbsp;MS&nbsp;:&nbsp;Memenuhi&nbsp;Syarat&nbsp;|&nbsp;TMS&nbsp;:&nbsp;Tidak&nbsp;Memenuhi&nbsp;Syarat&nbsp;|&nbsp;L&nbsp;:&nbsp;Lulus&nbsp;|&nbsp;TL&nbsp;:&nbsp;Tidak Lulus</td>
        </tr>
    </table>
    <form action="kesehatan-nilai.php" method="post">
        <table>
            <tr>
                <th rowspan="3">No</th>
                <th rowspan="3">No Ujian</th>
                <th rowspan="3">Nama</th>
            </tr>
            <tr>
                <th colspan="2">Mata</th>
                <th colspan="2">THT</th>
                <th colspan="2">Kesimpulan Akhir</th>
                <th rowspan="2">Catatan</th>
            </tr>
            <tr>
                <th>MS</th>
                <th>TMS</th>
                <th>MS</th>
                <th>TMS</th>
                <th>L</th>
                <th>TL</th>
            </tr>
            {$index=1}
            {foreach $data_c_mhs_by_kelompok as $c_mhs_by_kelompok}
                <tr>
                    <td>{$index}<input type="hidden" name="no_ujian{$index}" value="{$c_mhs_by_kelompok['NO_UJIAN']}"/></td>
                    <td>{$c_mhs_by_kelompok['NO_UJIAN']}</td>
                    <td>{$c_mhs_by_kelompok['NM_C_MHS']}</td>
                    <td><input name="kesimpulan_mata{$index}" type="radio" value="Memenuhi Syarat" {if isset($c_mhs_by_kelompok['KESEHATAN_KESIMPULAN_MATA'])} disabled="true" {if $c_mhs_by_kelompok['KESEHATAN_KESIMPULAN_MATA']=='Memenuhi Syarat'}checked="true"{/if} {/if}/></td>
                    <td><input name="kesimpulan_mata{$index}" type="radio" value="Tidak Memenuhi Syarat" {if isset($c_mhs_by_kelompok['KESEHATAN_KESIMPULAN_MATA'])} disabled="true" {if $c_mhs_by_kelompok['KESEHATAN_KESIMPULAN_MATA']=='Tidak Memenuhi Syarat'}checked="true"{/if} {/if}/></td>
                    <td><input name="kesimpulan_tht{$index}" type="radio" value="Memenuhi Syarat" {if isset($c_mhs_by_kelompok['KESEHATAN_KESIMPULAN_THT'])} disabled="true" {if $c_mhs_by_kelompok['KESEHATAN_KESIMPULAN_THT']=='Memenuhi Syarat'}checked="true"{/if}{/if}/></td>
                    <td><input name="kesimpulan_tht{$index}" type="radio" value="Tidak Memenuhi Syarat"{if isset($c_mhs_by_kelompok['KESEHATAN_KESIMPULAN_THT'])} disabled="true"{if $c_mhs_by_kelompok['KESEHATAN_KESIMPULAN_THT']=='Tidak Memenuhi Syarat'}checked="true"{/if}{/if}/></td>
                    <td><input name="kesimpulan_akhir{$index}" type="radio" value="1" {if isset($c_mhs_by_kelompok['KESEHATAN_KESIMPULAN_AKHIR'])} disabled="true" {if $c_mhs_by_kelompok['KESEHATAN_KESIMPULAN_AKHIR']=='1'} checked="true"{/if}{/if}/></td>
                    <td><input name="kesimpulan_akhir{$index}" type="radio" value="0"{if isset($c_mhs_by_kelompok['KESEHATAN_KESIMPULAN_AKHIR'])} disabled="true" {if $c_mhs_by_kelompok['KESEHATAN_KESIMPULAN_AKHIR']=='0'}checked="true"{/if}{/if}/></td>
                    <td><textarea name="catatan{$index++}" cols="30" rows="3">{$c_mhs_by_kelompok['KESEHATAN_CATATAN']}</textarea></td>
                </tr>
            {/foreach}
        </table>

        <input type="hidden" name="jumlah" value="{$index}"/>
        <input type="hidden" name="save" value="1"/>
        <a style="float: right;" href="cetak-hasil-tes-kesehatan.php?kelompok={$id_jadwal_kesehatan}" target="_blank" class="disable-ajax">Cetak Hasil</a>
        <input type="submit" value="Proses" style="float: right;margin: 10px;"/>
    </form>
{/if}