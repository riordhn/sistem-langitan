<div class="center_title_bar">Edit Nilai Pemeriksaan Kesehatan Calon Mahasiswa</div>
<table>
    <tr>
        <td><label>Nomer Ujian</label></td>
        <td>
            <form action="kesehatan-edit-nilai.php" method="get">
                <input type="text" name="no_ujian"/>
                <input type="submit" value="Edit.."/>
            </form>
        </td>
    </tr>
</table>

{if isset($calon_mahasiswa)}
    <table>
        <tr><th class="center">Keterangan</th></tr>
        <tr>
            <td>&nbsp;Y&nbsp;:&nbsp;Ya&nbsp;|&nbsp;N&nbsp;:&nbsp;Normal&nbsp;|&nbsp;T&nbsp;:&nbsp;Tidak&nbsp;|&nbsp;MS&nbsp;:&nbsp;Memenuhi&nbsp;Syarat&nbsp;|&nbsp;TMS&nbsp;:&nbsp;Tidak&nbsp;Memenuhi&nbsp;Syarat&nbsp;|&nbsp;L&nbsp;:&nbsp;Lulus&nbsp;|&nbsp;TL&nbsp;:&nbsp;Tidak Lulus</td>
        </tr>
    </table>
    <form action="kesehatan-edit-nilai.php" method="post">
        <table>
            <tr>
                <th rowspan="3">No Ujian</th>
                <th rowspan="3">Nama</th>
            </tr>
            <tr>
                <th colspan="2">Mata</th>
                <th colspan="2">THT</th>
                <th colspan="2">Kesimpulan Akhir</th>
                <th rowspan="2">Catatan</th>
            </tr>
            <tr>
                <th>MS</th>
                <th>TMS</th>
                <th>MS</th>
                <th>TMS</th>
                <th>L</th>
                <th>TL</th>
            </tr>
            <tr>
                <td>{$calon_mahasiswa['NO_UJIAN']}</td>
                <td>{$calon_mahasiswa['NM_C_MHS']}</td>
                <td><input name="kesimpulan_mata" type="radio" value="Memenuhi Syarat" {if $calon_mahasiswa['KESEHATAN_KESIMPULAN_MATA']=='Memenuhi Syarat'}checked="true"{/if}/></td>
                <td><input name="kesimpulan_mata" type="radio" value="Tidak Memenuhi Syarat" {if $calon_mahasiswa['KESEHATAN_KESIMPULAN_MATA']=='Tidak Memenuhi Syarat'}checked="true"{/if}/></td>
                <td><input name="kesimpulan_tht" type="radio" value="Memenuhi Syarat" {if $calon_mahasiswa['KESEHATAN_KESIMPULAN_THT']=='Memenuhi Syarat'}checked="true"{/if}/></td>
                <td><input name="kesimpulan_tht" type="radio" value="Tidak Memenuhi Syarat" {if $calon_mahasiswa['KESEHATAN_KESIMPULAN_THT']=='Tidak Memenuhi Syarat'}checked="true"{/if}/></td>
                <td><input name="kesimpulan_akhir" type="radio" value="1"  {if $calon_mahasiswa['KESEHATAN_KESIMPULAN_AKHIR']=='1'} checked="true"{/if}/></td>
                <td><input name="kesimpulan_akhir" type="radio" value="0" {if $calon_mahasiswa['KESEHATAN_KESIMPULAN_AKHIR']=='0'}checked="true"{/if}/></td>
                <td><textarea name="catatan" cols="30" rows="3">{$calon_mahasiswa['KESEHATAN_CATATAN']}</textarea></td>
            </tr>
        </table>
        <input type="hidden" name="no_ujian" value="{$calon_mahasiswa.NO_UJIAN}"/>
        <input type="hidden" name="save" value="1"/>
        <input type="submit" value="Proses" style="float: right;margin: 10px;"/>
    </form>
{/if}