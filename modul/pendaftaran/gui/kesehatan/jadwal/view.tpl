<div class="center_title_bar">Jadwal Tes kesehatan</div>
<table style="width: 100%">
    <tr>
        <th class="center" style="width: 5%">No</th>
        <th>Kelompok Jam</th>
        <th>Kapasitas</th>
        <th>Jadwal</th>
        <th>Terisi</th>
        <th></th>
    </tr>
    {$index=1}
    {foreach $data_jadwal_kesehatan as $jadwal_kesehatan}
    <tr>
        <td class="center">{$index++}</td>
        <td>{$jadwal_kesehatan['KELOMPOK_JAM']}</td>
        <td>{$jadwal_kesehatan['KAPASITAS']}</td>
        <td>{$jadwal_kesehatan['TGL_TEST']|date_format:"%d %B %Y"} [{$jadwal_kesehatan['JAM_MULAI']|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$jadwal_kesehatan['MENIT_MULAI']|str_pad:2:"0"}] - [{$jadwal_kesehatan['JAM_SELESAI']|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$jadwal_kesehatan['MENIT_SELESAI']|str_pad:2:"0"}]</td>
        <td>{$jadwal_kesehatan['TERISI']}</td>
        <td>
            <a href="kesehatan-jadwal.php?mode=edit&id_jadwal={$jadwal_kesehatan['ID_JADWAL_KESEHATAN']}">Edit</a>
            <a href="kesehatan-jadwal.php?mode=delete&id_jadwal={$jadwal_kesehatan['ID_JADWAL_KESEHATAN']}">Hapus</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="7" class="center">
            <button href="kesehatan-jadwal.php?mode=add">Tambah</button>
        </td>
    </tr>
</table>