<div class="center_title_bar">Jadwal Tes Kesehatan - Edit</div>

<form action="kesehatan-jadwal.php" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_jadwal" value="{$data_jadwal_kesehatan['ID_JADWAL_KESEHATAN']}" />
    <table>
        <tr>
            <td>Kelompok Jam</td>
            <td><input type="text" name="kelompok_jam" value="{$data_jadwal_kesehatan['KELOMPOK_JAM']}" />
            </td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>
            {html_select_date prefix="tgl_test_" field_order="DMY" time=$data_jadwal_kesehatan['TGL_TEST']}
            </td>
        </tr>
        <tr>
            <td>Jam Mulai</td>
            <td>
            {html_select_time prefix="mulai_" display_seconds=false use_24_hours=true minute_interval=5 time=mktime($data_jadwal_kesehatan['JAM_MULAI'], $data_jadwal_kesehatan['MENIT_MULAI'])}
            </td>
        </tr>
        <tr>
            <td>Jam Selesai</td>
            <td>
            {html_select_time prefix="selesai_" display_seconds=false use_24_hours=true minute_interval=5 time=mktime($data_jadwal_kesehatan['JAM_SELESAI'], $data_jadwal_kesehatan['MENIT_SELESAI'])}
            </td>
        </tr>
        <tr>
            <td>Kapasitas</td>
            <td><input type="text" name="kapasitas" value="{$data_jadwal_kesehatan['KAPASITAS']}" maxlength="3"/></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <button href="kesehatan-jadwal.php">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>