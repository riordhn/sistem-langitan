<div class="center_title_bar">Jadwal Tes Kesehatan - Hapus Jadwal</div>

<form action="kesehatan-jadwal.php" method="post">
<input type="hidden" name="mode" value="delete" />
<input type="hidden" name="id_jadwal" value="{$data_jadwal_kesehatan['ID_JADWAL_KESEHATAN']}" />
<table>
    <tr>
        <td>Ruang</td>
        <td>{$data_jadwal_kesehatan['NM_RUANG']}</td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td>
            {$data_jadwal_kesehatan['TGL_TEST']}
        </td>
    </tr>
    <tr>
        <td>Jam</td>
        <td>
            {$data_jadwal_kesehatan['JAM_MULAI']} : {$data_jadwal_kesehatan['MENIT_MULAI']}
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center">
            <button href="kesehatan-jadwal.php">Batal</button>
            <input type="submit" value="Hapus" />
        </td>
    </tr>
</table>
</form>