<div class="center_title_bar">Jadwal Tes Kesehatan - Tambah</div>

<form action="kesehatan-jadwal.php" method="post">
    <table>
        <tr>
            <td>Kelompok Jam</td>
            <td>
                <input type="text" name="kelompok_jam"/>
            </td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>
            {html_select_date prefix="tgl_test_" field_order="DMY"}
            </td>
        </tr>
        <tr>
            <td>Jam Mulai</td>
            <td>
            {html_select_time prefix="mulai_" display_seconds=false use_24_hours=true minute_interval=5}
            </td>
        </tr>
        <tr>
            <td>Jam Selesai</td>
            <td>
            {html_select_time prefix="selesai_" display_seconds=false use_24_hours=true minute_interval=5}
            </td>
        </tr>
        <tr>
            <td>Kapasitas</td>
            <td>
                <input type="text" name="kapasitas" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <button href="kesehatan-jadwal.php">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
    <input type="hidden" name="mode" value="add" />
</form>