<div class="center_title_bar">Ubah Biaya Calon Mahasiswa</div>
<form action="ubah-biaya-calon.php" method="get">
    <input type="hidden" name="mode" value="search" />
    <table>
        <tr>
            <td>No Ujian</td>
            <td><input type="text" name="no_ujian" />
                <input type="submit" value="CARI" />
            </td>
        </tr>
    </table>
</form>

{if isset($cm)}
    
    <form action="ubah-biaya-calon.php?{$smarty.server.QUERY_STRING}" method="post">
        <input type="hidden" name="id_c_mhs" value="{$cm.ID_C_MHS}" />
    <table>
        <tr>
            <td>Nama</td>
            <td>{$cm.NM_C_MHS}</td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>{$cm.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td>Tgl Bayar</td>
            <td>
                <input type="text" name="tgl_bayar" value={strtoupper(date('d-M-Y'))} />
            </td>
        </tr>
    </table>
    
    <table>
        <tr>
            <th>No</th>
            <th>ID_PC</th>
            <th>Rincian</th>
            <th>Besar Biaya</th>
            <th>Bank</th>
            <th>Via</th>
            <th>Tgl Bayar</th>
            <th>No Transaksi</th>
            <th>Tagih</th>
        </tr>
        {$total=0}
        {foreach $pc_set as $pc}
        <tr>
            <td>{$pc@index+1}</td>
            <td>{$pc.ID_PEMBAYARAN_CMHS}</td>
            <td>{$pc.NM_BIAYA}</td>
            <td style="text-align: right; {if $pc.IS_TAGIH=='T'}background-color: #333"{/if}">{number_format($pc.BESAR_BIAYA,0,',','.')}</td>
            <td>{$pc.NM_BANK}</td>
            <td>{$pc.NAMA_BANK_VIA}</td>
            <td>{$pc.TGL_BAYAR}</td>
            <td>{$pc.NO_TRANSAKSI}</td>
            <td>{$pc.IS_TAGIH}</td>
        </tr>
        {if $pc.IS_TAGIH=='Y'}
        {$total=$total+$pc.BESAR_BIAYA}
        {/if}
        {/foreach}
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td style="text-align: right">{number_format($total,0,',','.')}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="9" style="text-align: center">
                <input type="submit" value="Submit" />
            </td>
        </tr>
    </table>
    </form>
{/if}