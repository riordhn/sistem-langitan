
<?php

class kesehatan {

    public $db;

    function __construct($database) {
        $this->db = $database;
    }

    function load_calon_mahasiswa_by_kelompok($tanggal, $kelompok) {
        $data_c_mhs_by_kelompok = array();
        $this->db->Query("select CM.ID_C_MHS,CM.NO_UJIAN,CM.NM_C_MHS as NAMA, PR.NM_PROGRAM_STUDI,F.NM_FAKULTAS 
                from CALON_MAHASISWA CM,FAKULTAS F, PROGRAM_STUDI PR, JADWAL_KESEHATAN JK,URUTAN_KESEHATAN UK
                where CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI and PR.ID_FAKULTAS = F.ID_FAKULTAS and JK.ID_JADWAL_KESEHATAN = CM.ID_JADWAL_KESEHATAN 
                and UK.ID_CMHS=CM.ID_C_MHS and JK.TGL_TEST ='$tanggal' AND JK.KELOMPOK_JAM='$kelompok'
                order by UK.URUTAN");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_c_mhs_by_kelompok, $temp);
        }
        return $data_c_mhs_by_kelompok;
    }

    function load_calon_mahasiswa_by_no_ujian($no_ujian) {
        $this->db->Query("SELECT * FROM CALON_MAHASISWA WHERE NO_UJIAN = '$no_ujian'");
        return $this->db->FetchArray();
    }

    function load_calon_mahasiswa_by_id_jadwal($id_jadwal) {
        $data_c_mhs_by_id = array();
        $this->db->Query("SELECT CM.*
                FROM AUCC.CALON_MAHASISWA CM 
                LEFT JOIN AUCC.URUTAN_KESEHATAN UK ON UK.ID_CMHS=CM.ID_C_MHS 
                WHERE CM.ID_JADWAL_KESEHATAN='$id_jadwal'
                ORDER BY UK.URUTAN");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_c_mhs_by_id, $temp);
        }
        return $data_c_mhs_by_id;
    }

    function update_data_kesehatan($no_ujian, $kesimpulan_mata, $kesimpulan_tht, $kesimpulan_akhir, $catatan) {
        $data = $this->load_calon_mahasiswa_by_no_ujian($no_ujian);
        if ($data['STATUS'] < 13 ) {
            $this->db->Query("UPDATE CALON_MAHASISWA SET KESEHATAN_KESIMPULAN_MATA='$kesimpulan_mata',KESEHATAN_KESIMPULAN_THT='$kesimpulan_tht',KESEHATAN_KESIMPULAN_AKHIR='$kesimpulan_akhir',KESEHATAN_CATATAN='$catatan',STATUS ='12' WHERE NO_UJIAN = '$no_ujian'");
        }
    }

    function load_ruang_kesehatan($id_ruangan='') {
        $data_ruang_kesehatan = array();
        if (empty($id_ruangan)) {
            $this->db->Query("SELECT * FROM RUANG_KESEHATAN ORDER BY ID_RUANG_KESEHATAN");
            while ($temp = $this->db->FetchArray()) {
                array_push($data_ruang_kesehatan, $temp);
            }
        } else {
            $this->db->Query("SELECT * FROM RUANG_KESEHATAN WHERE ID_RUANG_KESEHATAN='$id_ruangan'");
            $data_ruang_kesehatan = $this->db->FetchArray();
        }
        return $data_ruang_kesehatan;
    }

    function update_ruang_kesehatan($id_ruang, $nm_ruang, $kapasitas) {
        $this->db->Query("UPDATE RUANG_KESEHATAN SET NM_RUANG='$nm_ruang',KAPASITAS='$kapasitas' WHERE ID_RUANG_KESEHATAN='$id_ruang'");
    }

    function insert_ruang_kesehatan($nm_ruang, $kapasitas) {
        $this->db->Query("INSERT INTO RUANG_KESEHATAN (NM_RUANG,KAPASITAS) VALUES ('$nm_ruang','$kapasitas')");
    }

    function delete_ruang_kesehatan($id_ruang) {
        $this->db->Query("DELETE RUANG_KESEHATAN WHERE ID_RUANG_KESEHATAN ='$id_ruang'");
    }

    function load_jadwal_kesehatan($tanggal=null) {
        $data_jadwal_kesehatan = array();
        if (empty($tanggal)) {
            $this->db->Query("SELECT * FROM JADWAL_KESEHATAN ORDER BY ID_JADWAL_KESEHATAN");
            while ($temp = $this->db->FetchArray()) {
                array_push($data_jadwal_kesehatan, $temp);
            }
        } else {
            $this->db->Query("SELECT * FROM JADWAL_KESEHATAN WHERE TGL_TEST = to_date('$tanggal','DD-MON-YY')");
            while ($temp = $this->db->FetchArray()) {
                array_push($data_jadwal_kesehatan, $temp);
            }
        }
        return $data_jadwal_kesehatan;
    }

    function load_jadwal_kesehatan_by_id($id) {
        $data_jadwal_kesehatan = array();
        $this->db->Query("SELECT * FROM JADWAL_KESEHATAN WHERE ID_JADWAL_KESEHATAN='$id' ORDER BY ID_JADWAL_KESEHATAN");
        $data_jadwal_kesehatan = $this->db->FetchArray();
        return $data_jadwal_kesehatan;
    }

    function load_calon_mahasiswa($id_c_mhs) {
        $this->db->Query("SELECT CM.NM_C_MHS,CM.NO_UJIAN,CM.ALAMAT,CM.TELP,J.NM_JALUR,PR.NM_PROGRAM_STUDI,F.NM_FAKULTAS,JK.ID_JADWAL_KESEHATAN,JK.KELOMPOK_JAM,JK.TGL_TEST,JK.JAM_MULAI,JK.MENIT_MULAI,JK.JAM_SELESAI,JK.MENIT_SELESAI from CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JADWAL_KESEHATAN JK,JALUR J
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS= F.ID_FAKULTAS AND CM.ID_JADWAL_KESEHATAN = JK.ID_JADWAL_KESEHATAN AND CM.ID_JALUR = J.ID_JALUR AND CM.ID_C_MHS='$id_c_mhs'");
        $data = $this->db->FetchArray();
        return $data;
    }

    function load_tanggal_jadwal_kesehatan() {
        $data_tanggal_jadwal_kesehatan = array();
        $this->db->Query("SELECT DISTINCT JK.TGL_TEST FROM JADWAL_KESEHATAN JK ORDER BY TGL_TEST");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_tanggal_jadwal_kesehatan, $temp);
        }
        return $data_tanggal_jadwal_kesehatan;
    }

    function update_jadwal_kesehatan($id_jadwal, $kelompok_jam, $tanggal, $jam_mulai, $menit_mulai, $jam_selesai, $menit_selesai, $kapasitas) {
        $this->db->Query("UPDATE JADWAL_KESEHATAN SET KELOMPOK_JAM='$kelompok_jam',TGL_TEST='$tanggal',JAM_MULAI='$jam_mulai',MENIT_MULAI='$menit_mulai',JAM_SELESAI='$jam_selesai',MENIT_SELESAI='$menit_selesai',KAPASITAS='$kapasitas' WHERE ID_JADWAL_KESEHATAN='$id_jadwal'");
    }

    function insert_jadwal_kesehatan($kelompok_jam, $tanggal, $jam_mulai, $menit_mulai, $jam_selesai, $menit_selesai, $kapasitas) {
        $this->db->Query("INSERT INTO JADWAL_KESEHATAN (KELOMPOK_JAM,TGL_TEST,JAM_MULAI,MENIT_MULAI,JAM_SELESAI,MENIT_SELESAI,KAPASITAS) VALUES ('$kelompok_jam','$tanggal','$jam_mulai','$menit_mulai','$jam_mulai','$menit_selesai','$kapasitas')");
    }

    function delete_jadwal_kesehatan($id_jadwal) {
        $this->db->Query("DELETE JADWAL_KESEHATAN WHERE ID_JADWAL_KESEHATAN ='$id_jadwal'");
    }

    function load_data_hasil_kesehatan($kelompok, $hasil, $tanggal) {
        $data_hasil_tes = array();
        if (isset($kelompok)) {
            $this->db->Query("SELECT CM.NO_UJIAN, CM.NM_C_MHS, PR.NM_PROGRAM_STUDI,CM.KESEHATAN_KESIMPULAN_AKHIR FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR, JADWAL_KESEHATAN JK
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI  AND CM.ID_JADWAL_KESEHATAN = JK.ID_JADWAL_KESEHATAN AND CM.ID_JADWAL_KESEHATAN='$kelompok'");
        } else if (isset($hasil)) {
            $this->db->Query("SELECT CM.NO_UJIAN, CM.NM_C_MHS, PR.NM_PROGRAM_STUDI,CM.KESEHATAN_KESIMPULAN_AKHIR FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR, JADWAL_KESEHATAN JK
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI  AND CM.ID_JADWAL_KESEHATAN = JK.ID_JADWAL_KESEHATAN AND CM.KESEHATAN_KESIMPULAN_AKHIR ='$hasil'");
        } else if (isset($tanggal)) {
            $this->db->Query("SELECT CM.NO_UJIAN, CM.NM_C_MHS, PR.NM_PROGRAM_STUDI,CM.KESEHATAN_KESIMPULAN_AKHIR FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR, JADWAL_KESEHATAN JK
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI  AND CM.ID_JADWAL_KESEHATAN = JK.ID_JADWAL_KESEHATAN  AND JK.TGL_TEST = to_date('$tanggal')");
        } else {
            $this->db->Query("SELECT CM.NO_UJIAN, CM.NM_C_MHS, PR.NM_PROGRAM_STUDI,CM.KESEHATAN_KESIMPULAN_AKHIR FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR, JADWAL_KESEHATAN JK
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI  AND CM.ID_JADWAL_KESEHATAN = JK.ID_JADWAL_KESEHATAN");
        }
        while ($temp = $this->db->FetchArray()) {
            array_push($data_hasil_tes, $temp);
        }
        return $data_hasil_tes;
    }

    function load_data_hasil_kesehatan_fakultas($fakultas) {
        $data_hasil_tes = array();
        $this->db->Query("SELECT CM.NO_UJIAN, CM.NM_C_MHS, PR.NM_PROGRAM_STUDI,CM.KESEHATAN_KESIMPULAN_AKHIR FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F, JADWAL_KESEHATAN JK
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI  AND CM.ID_JADWAL_KESEHATAN = JK.ID_JADWAL_KESEHATAN AND PR.ID_FAKULTAS =F.ID_FAKULTAS AND F.ID_FAKULTAS ='$fakultas'");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_hasil_tes, $temp);
        }
        return $data_hasil_tes;
    }

    function load_fakultas() {
        $data = array();
        $this->db->Query("SELECT * FROM FAKULTAS");
        while ($temp = $this->db->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

}

?>
