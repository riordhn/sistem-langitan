<?php

class elpt {

    public $db;

    function __construct($database) {
        $this->db = $database;
    }

    function load_jadwal_eltp($id=null) {
        $data_jadwal_elpt = array();
        if (empty($id)) {
            $this->db->Query("SELECT * FROM JADWAL_TOEFL JT,RUANG_TOEFL RT WHERE JT.ID_RUANG_TOEFL = RT.ID_RUANG_TOEFL ORDER BY ID_JADWAL_TOEFL");
            while ($temp = $this->db->FetchArray()) {
                array_push($data_jadwal_elpt, $temp);
            }
        } else {
            $this->db->Query("SELECT JT.JAM_MULAI,JT.MENIT_MULAI,RT.NM_RUANG,to_char(JT.TGL_TEST,'DD MONTH YYYY') as TGL_TEST FROM JADWAL_TOEFL JT,RUANG_TOEFL RT WHERE JT.ID_RUANG_TOEFL = RT.ID_RUANG_TOEFL AND JT.ID_JADWAL_TOEFL='$id' ORDER BY ID_JADWAL_TOEFL");
            $data_jadwal_elpt = $this->db->FetchArray();
        }
        return $data_jadwal_elpt;
    }

    function load_tanggal_jadwal_elpt() {
        $data_tanggal_jadwal_elpt = array();
        $this->db->Query("SELECT DISTICT TGL_TEST FROM JADWAL_TOEFL ORDER BY ID_JADWAL_TOEFL");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_tanggal_jadwal_elpt, $temp);
        }
        return $data_tanggal_jadwal_elpt;
    }

    function load_c_mhs_by_kelompok($id_jadwal_elpt) {
        $data_c_mhs = array();
        $this->db->Query("SELECT CM.ELPT_KEHADIRAN,CM.ALAMAT,CM.TELP,CM.ID_C_MHS,UPPER(CM.NM_C_MHS) AS NM_C_MHS,CM.NO_UJIAN,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI,CM.ELPT_LISTENING,CM.ELPT_STRUCTURE,CM.ELPT_READING,CM.ELPT_SCORE,CM.ELPT_KEHADIRAN FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR, FAKULTAS F
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS= F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL ='$id_jadwal_elpt'");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_c_mhs, $temp);
        }
        return $data_c_mhs;
    }

    function load_c_mhs_view($fakultas, $condition, $range, $range1, $range2) {
        $data = array();
        switch ($condition) {
            case 'hadir': {
                    $query = "SELECT UPPER(CM.NM_C_MHS) AS NM_C_MHS,CM.NO_UJIAN,UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,CM.ELPT_SCORE
                    FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR, FAKULTAS F
                    WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL IS NOT NULL AND CM.ELPT_KEHADIRAN =1 AND F.ID_FAKULTAS='$fakultas'
                    ";
                }
                break;
            case '': {
                    $query = "SELECT UPPER(CM.NM_C_MHS) AS NM_C_MHS,CM.NO_UJIAN,UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,CM.ELPT_SCORE
                    FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR, FAKULTAS F
                    WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL IS NOT NULL AND F.ID_FAKULTAS ='$fakultas'
                    ";
                }
                break;
            case 'kur': {
                    $query = "SELECT UPPER(CM.NM_C_MHS) AS NM_C_MHS,CM.NO_UJIAN,UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,CM.ELPT_SCORE
                    FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR, FAKULTAS F
                    WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL IS NOT NULL AND CM.ELPT_KEHADIRAN =1 AND CM.ELPT_SCORE < '$range' AND F.ID_FAKULTAS ='$fakultas'
                    ";
                }
                break;
            case 'bet': {
                    $query = "SELECT UPPER(CM.NM_C_MHS) AS NM_C_MHS,CM.NO_UJIAN,UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,CM.ELPT_SCORE
                    FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR, FAKULTAS F
                    WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL IS NOT NULL AND CM.ELPT_KEHADIRAN =1 AND CM.ELPT_SCORE BETWEEN '$range1' AND '$range2' AND F.ID_FAKULTAS ='$fakultas'
                    ";
                }
                break;
            case 'leb': {
                    $query = "SELECT UPPER(CM.NM_C_MHS) AS NM_C_MHS,CM.NO_UJIAN,UPPER(PR.NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,CM.ELPT_SCORE
                    FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR, FAKULTAS F
                    WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL IS NOT NULL AND CM.ELPT_KEHADIRAN =1 AND CM.ELPT_SCORE >'$range' AND F.ID_FAKULTAS ='$fakultas'
                    ";
                }
                break;
        }
        $this->db->Query($query);
        while ($temp = $this->db->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function update_nilai_eltp($id_c_mhs, $list, $struc, $reading, $score) {
        $this->db->Query("UPDATE CALON_MAHASISWA SET ELPT_LISTENING='$list',ELPT_STRUCTURE='$struc',ELPT_READING='$reading',ELPT_SCORE='$score' WHERE ID_C_MHS='$id_c_mhs'");
    }

    function update_presensi_elpt($id_c_mhs, $absen) {
        $this->db->Query("UPDATE CALON_MAHASISWA SET ELPT_KEHADIRAN='$absen' WHERE ID_C_MHS='$id_c_mhs'");
    }

    function delete_jadwal_elpt($id_jadwal_toefl) {
        $this->db->Query("DELETE JADWAL_TOEFL WHERE ID_JADWAL_TOEFL='$id_jadwal_toefl'");
    }

    function load_data_hasil_elpt($kelompok, $hasil, $tanggal) {

        while ($temp = $this->db->FetchArray()) {
            array_push($data_hasil_tes, $temp);
        }
        return $data_hasil_tes;
    }

    function load_data_view_elpt() {
        $data_view_elpt = array();
        $this->db->Query("SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,COUNT(CM.ID_C_MHS) AS JUMLAH_PESERTA,
        SUM(CASE WHEN CM.ELPT_KEHADIRAN=1 THEN 1 ELSE 0 END) AS PESERTA_HADIR 
        FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR, FAKULTAS F
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL IS NOT NULL
        GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
        ORDER BY F.ID_FAKULTAS
        ");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_view_elpt, $temp);
        }
        return $data_view_elpt;
    }

    function get_elpt_score_more_than($id_fakultas, $range) {
        $this->db->Query("SELECT COUNT(CM.ID_C_MHS) as JUMLAH FROM CALON_MAHASISWA CM, PROGRAM_STUDI PR,FAKULTAS F
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL IS NOT NULL AND CM.ELPT_KEHADIRAN =1 AND CM.ELPT_SCORE > '$range' AND F.ID_FAKULTAS ='$id_fakultas'");
        return $this->db->FetchArray();
    }

    function get_elpt_score_between($id_fakultas, $range1, $range2) {
        $this->db->Query("SELECT COUNT(CM.ID_C_MHS) as JUMLAH FROM CALON_MAHASISWA CM, PROGRAM_STUDI PR,FAKULTAS F
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL IS NOT NULL AND CM.ELPT_KEHADIRAN =1 AND CM.ELPT_SCORE BETWEEN '$range1' AND '$range2' AND F.ID_FAKULTAS ='$id_fakultas'");
        return $this->db->FetchArray();
    }

    function get_elpt_score_less_than($id_fakultas, $range) {
        $this->db->Query("SELECT COUNT(CM.ID_C_MHS) as JUMLAH FROM CALON_MAHASISWA CM, PROGRAM_STUDI PR,FAKULTAS F
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM.ID_JADWAL_TOEFL IS NOT NULL AND CM.ELPT_KEHADIRAN =1 AND CM.ELPT_SCORE < '$range' AND F.ID_FAKULTAS ='$id_fakultas'");
        return $this->db->FetchArray();
    }

}

?>
