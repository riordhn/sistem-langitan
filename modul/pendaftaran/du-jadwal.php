<?php

include('config.php');
include('class/du_jadwal.class.php');

$calon_mahasiswa_table = new CALON_MAHASISWA_TABLE($db);
$du_jadwal = new du_jadwal($db);


if ($user->IsLogged() && $user->ID_UNIT_KERJA == 2) {

    if ($request_method == 'POST') {
        if (post('mode') == 'terima') {
            $id_c_mhs = post('id_c_mhs');
            $terima_buku = post('terima_buku') == "on" ? 1 : 0;
            $terima_jadwal_elpt = post('terima_jadwal_elpt') == "on" ? 1 : 0;
            $terima_jadwal_kesehatan = post('terima_jadwal_kesehatan') == "on" ? 1 : 0;

            $db->Query("UPDATE CALON_MAHASISWA SET
                TERIMA_BUKU = {$terima_buku},
                TERIMA_JADWAL_ELPT = {$terima_jadwal_elpt},
                TERIMA_JADWAL_KESEHATAN = {$terima_jadwal_kesehatan}
                WHERE ID_C_MHS = {$id_c_mhs}");
        }
    }


    if ((get('no_ujian')) != null) {
        $data = $du_jadwal->load_jadwal_calon_mahasiswa(get('no_ujian'));
        $data_jenjang = $du_jadwal->jenjang_calon_mahasiswa(get('no_ujian'));
        if ($data['ID_JADWAL_TOEFL'] == null && $data['ID_JADWAL_KESEHATAN'] == null && ($data_jenjang['ID_JENJANG'] == AUCC_JENJANG_S1 || $data_jenjang['ID_JENJANG'] == AUCC_JENJANG_D3)) {
            $du_jadwal->update_jadwal_calon_mahasiswa(get('no_ujian'));
        } else {
            if ($data_jenjang['ID_JENJANG'] != AUCC_JENJANG_S1 && $data_jenjang['ID_JENJANG'] != AUCC_JENJANG_D3) {
                $du_jadwal->update_status_selain_S1_D3(get('no_ujian'));
            }
        }

        $smarty->assign('status', $du_jadwal->get_status(get('no_ujian')));
        $smarty->assign('data_jenjang', $data_jenjang['ID_JENJANG']);
        $smarty->assign('data_calon_mahasiswa', $du_jadwal->load_data_mahasiswa(get('no_ujian')));
        $smarty->assign('data_jadwal', $du_jadwal->load_jadwal_calon_mahasiswa(get('no_ujian')));
    }
    $smarty->display('verifikasi/du-jadwal.tpl');
} else {
    echo "<h2>Khusus area pegawai DIT. PENDIDIKAN.</h2>";
};
?>
