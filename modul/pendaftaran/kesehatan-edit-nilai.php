<?php

include'config.php';
include 'class/kesehatan.class.php';

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN && $user->ID_UNIT_KERJA == 40) {
    $kesehatan = new kesehatan($db);
    if (get('no_ujian') != '') {
        $data = $kesehatan->load_calon_mahasiswa_by_no_ujian(get('no_ujian'));
        if ($data) {
            $smarty->assign('calon_mahasiswa', $data);
        }
    } else if (post('save') == '1') {
        if (post('kesimpulan_mata') != null && post('kesimpulan_tht') != null && post('kesimpulan_akhir') != null) {
            $kesehatan->update_data_kesehatan(post('no_ujian'), post('kesimpulan_mata'), post('kesimpulan_tht'), post('kesimpulan_akhir'), post('catatan' ));
            write_log('Nilai Kesehatan CM ' . post('no_ujian') . ' Telah di update oleh develop' . $user->ID_PENGGUNA . ' Di Rubah Menjadi =' . post('kesimpulan_akhir'));
        }
    }
    $smarty->display("kesehatan/nilai/edit-nilai.tpl");
} else {
    echo "<h2>Khusus area pegawai Test Kesehatan.</h2>";
};
?>
