<?php
require_once('config.php');

$location = explode("-", $_GET['location']);

// Mendapatkan navigasi breadcrumbs
foreach ($xml_menu->tab as $tab)
{
    // Mendapatkan navigasi tab
    if ($tab->id == $location[0])
    {
        $smarty->assign(array(
            'TAB_ID'    => $tab->id,
            'TAB_TITLE' => $tab->title,
            'TAB_PAGE'  => $tab->page,
        ));
        
        if (count($location) == 2)
        {
            foreach ($tab->menu as $menu)
            {
                // Mendapatkan navigasi menu
                if ($menu->id == $location[1])
                {
                    $smarty->assign(array(
                        'MENU_ID'       => $menu->id,
                        'MENU_PAGE'     => $menu->page,
                        'MENU_TITLE'    => $menu->title,
                    ));
                    break;
                }
            }
        }
        
        break;
    }
}

if (count($location) == 3)
{
    $smarty->assign('action', ucwords($location[2]));
}

$smarty->display('breadcrumbs.tpl');
?>
