<?php
include('config.php');

$id_jadwal_ppmb = get('id_jadwal_ppmb', '');

$jadwal_set = $db->QueryToArray("
    SELECT ID_JADWAL_PPMB, NOMER_AWAL, NOMER_AKHIR, NM_RUANG, LOKASI FROM JADWAL_PPMB
    WHERE GELOMBANG = 5 AND STATUS_AKTIF = 1 ORDER BY NOMER_AWAL");

if ($id_jadwal_ppmb != '')
{
    $smarty->assign('plot_jadwal_set', $db->QueryToArray("
        SELECT PJ.NO_UJIAN, CM.NM_C_MHS, KODE_VOUCHER FROM PLOT_JADWAL_PPMB PJ
        LEFT JOIN CALON_MAHASISWA CM ON CM.ID_C_MHS = PJ.ID_C_MHS
        WHERE PJ.ID_JADWAL_PPMB = {$id_jadwal_ppmb}"));
}

$smarty->assign('jadwal_set', $jadwal_set);
$smarty->display('ppmb/reset/jadwal.tpl');
?>
