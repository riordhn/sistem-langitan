<?php
include('config.php');

if ($user->IsLogged() && $user->ID_UNIT_KERJA == 32) {

$calon_mahasiswa_table = new CALON_MAHASISWA_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);
$jenjang_table = new JENJANG_TABLE($db);
$kota_table = new KOTA_TABLE($db);
$provinsi_table = new PROVINSI_TABLE($db);
$biaya_kuliah_cmhs_table = new BIAYA_KULIAH_CMHS_TABLE($db);
$biaya_kuliah_table = new BIAYA_KULIAH_TABLE($db);
$detail_biaya_table = new DETAIL_BIAYA_TABLE($db);
$semester_table = new SEMESTER_TABLE($db);
$pembayaran_cmhs_table = new PEMBAYARAN_CMHS_TABLE($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'submit_verifikasi')
    {
        $calon_mahasiswa = $calon_mahasiswa_table->Single(post('id_c_mhs'));
        
        $calon_mahasiswa->NAMA_AYAH = post('nama_ayah');
        $calon_mahasiswa->ALAMAT_AYAH = post('alamat_ayah');
        $calon_mahasiswa->ID_KOTA_AYAH = post('id_kota_ayah');
        $calon_mahasiswa->STATUS_ALAMAT_AYAH = post('status_alamat_ayah');
        
        $calon_mahasiswa->NAMA_IBU = post('nama_ibu');
        $calon_mahasiswa->ALAMAT_IBU = post('alamat_ibu');
        $calon_mahasiswa->ID_KOTA_IBU = post('id_kota_ibu');
        $calon_mahasiswa->STATUS_ALAMAT_IBU = post('status_alamat_ibu');
        
        $calon_mahasiswa->ANGGOTA_KELUARGA = post('anggota_keluarga');
        $calon_mahasiswa->NOMOR_KSK = post('nomor_ksk');
        $calon_mahasiswa->SP3 = post('sp3');
        //$calon_mahasiswa->SP3_VERIFIKASI = post('sp3');
        $calon_mahasiswa->SP3_LAIN = post('sp3_lain');
        $calon_mahasiswa->PENGHASILAN_ORTU = post('penghasilan_ortu');
        
        // Pekerjaan ortu
        $calon_mahasiswa->PEKERJAAN_AYAH = post('pekerjaan_ayah');
        $calon_mahasiswa->PEKERJAAN_AYAH_LAIN = post('pekerjaan_ayah_lain');
        $calon_mahasiswa->PEKERJAAN_IBU = post('pekerjaan_ibu');
        $calon_mahasiswa->PEKERJAAN_IBU_LAIN = post('pekerjaan_ibu_lain');
        
        $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = post('skala_pekerjaan_ortu');
        $calon_mahasiswa->KEDIAMAN_ORTU = post('kediaman_ortu');
        $calon_mahasiswa->LUAS_TANAH = post('luas_tanah');
        $calon_mahasiswa->LUAS_BANGUNAN = post('luas_bangunan');
        $calon_mahasiswa->NJOP = post('njop');
        $calon_mahasiswa->LISTRIK = post('listrik');
        $calon_mahasiswa->KENDARAAN_R4 = post('kendaraan_r4');
        $calon_mahasiswa->KENDARAAN_R2 = post('kendaraan_r2');
        
        $calon_mahasiswa->KEKAYAAN_LAIN = post('kekayaan_lain');
        $calon_mahasiswa->INFO_LAIN = post('info_lain');
        
        $calon_mahasiswa->BERKAS_PENGHASILAN = post('berkas_penghasilan') ? 1 : 0;
        $calon_mahasiswa->BERKAS_AKTA = post('berkas_akta') ? 1 : 0;
        $calon_mahasiswa->BERKAS_TANI = post('berkas_tani') ? 1 : 0;
        $calon_mahasiswa->BERKAS_SPPT = post('berkas_sppt') ? 1 : 0;
        $calon_mahasiswa->BERKAS_LISTRIK = post('berkas_listrik') ? 1 : 0;
        $calon_mahasiswa->BERKAS_STNK = post('berkas_stnk') ? 1 : 0;
        $calon_mahasiswa->BERKAS_KSK = post('berkas_ksk') ? 1 : 0;
        
        // MENDAPATKAN JATUH_TEMPO_PEMBAYARAN
        for ($day = 4; $day <= 9; $day++)
        {
            $month = 7; $year = 2011;
            $tgl_jatuh_tempo = date('d-M-Y', mktime(0, 0, 0, $month, $day, $year));
            
            if ($calon_mahasiswa_table->CountWhere("TGL_JATUH_TEMPO = '{$tgl_jatuh_tempo}'") <= 460)
            {
                $calon_mahasiswa->TGL_JATUH_TEMPO = $tgl_jatuh_tempo;
                break;
            }
        }
        
        $calon_mahasiswa->TGL_VERIFIKASI = date('d-M-Y');
        $calon_mahasiswa->STATUS = 6;
        
        // Mendapatkan nomer invoice
        $db->Query("SELECT MAX(NO_INVOICE)+1 AS no_invoice FROM CALON_MAHASISWA");
        $row = $db->FetchAssoc();
        $calon_mahasiswa->NO_INVOICE = $row['NO_INVOICE'];
        
        // Petugas verifikator
        $calon_mahasiswa->ID_VERIFIKATOR = $user->ID_PENGGUNA;
        
        $calon_mahasiswa_table->Update($calon_mahasiswa);
        
        // Mencari biaya kuliah
        $biaya_kuliah_set = $biaya_kuliah_table->SelectWhere("
            ID_PROGRAM_STUDI = {$calon_mahasiswa->ID_PROGRAM_STUDI} AND ID_JALUR = {$calon_mahasiswa->ID_JALUR} AND ID_KELOMPOK_BIAYA = {$calon_mahasiswa->SP3}");

        $id_biaya_kuliah = $biaya_kuliah_set->Get(0)->ID_BIAYA_KULIAH;

        // cek apakah sudah ter insert apa belum
        $biaya_kuliah_cmhs_set = $biaya_kuliah_cmhs_table->SelectWhere("ID_C_MHS = {$calon_mahasiswa->ID_C_MHS}");

        // Insert / update biaya kuliah
        if ($biaya_kuliah_cmhs_set->Count() == 0)
        {
            $biaya_kuliah_cmhs = new BIAYA_KULIAH_CMHS();
            $biaya_kuliah_cmhs->ID_C_MHS = $calon_mahasiswa->ID_C_MHS;
            $biaya_kuliah_cmhs->ID_BIAYA_KULIAH = $id_biaya_kuliah;
            $biaya_kuliah_cmhs_table->Insert($biaya_kuliah_cmhs);
        }
        else
        {
            $biaya_kuliah_cmhs = $biaya_kuliah_cmhs_set->Get(0);
            $biaya_kuliah_cmhs->ID_BIAYA_KULIAH = $id_biaya_kuliah;
            $biaya_kuliah_cmhs_table->Update($biaya_kuliah_cmhs);
        }
        
        // Insert / update pembayaran_cmhs
        $detail_biaya_set = $detail_biaya_table->SelectCriteria("
            LEFT JOIN BIAYA B ON B.ID_BIAYA = DETAIL_BIAYA.ID_BIAYA
            WHERE DETAIL_BIAYA.ID_BIAYA_KULIAH = {$id_biaya_kuliah}
            ORDER BY B.NM_BIAYA");
        $id_semester = 5;
        
        // hapus data pembayaran terlebih dahulu (yg belum dibayar saja)
        $pembayaran_cmhs_set = $pembayaran_cmhs_table->SelectWhere("ID_C_MHS = {$calon_mahasiswa->ID_C_MHS} AND TGL_BAYAR IS NULL");
        $pembayaran_cmhs_table->DeleteSet($pembayaran_cmhs_set);
        
        for ($i = 0; $i < $detail_biaya_set->Count(); $i++)
        {            
            $detail_biaya = $detail_biaya_set->Get($i);
            
            // default biaya
            $besar_biaya = $detail_biaya->BESAR_BIAYA;
            
            // cek Sp3 custom
            if ($calon_mahasiswa->SP3 == AUCC_KELOMPOK_SP3b && $detail_biaya->ID_BIAYA == AUCC_BIAYA_SP3)
            {
                if ($calon_mahasiswa->SP3_LAIN >= $detail_biaya->BESAR_BIAYA)
                {
                    $besar_biaya = $calon_mahasiswa->SP3_LAIN;
                }
            }
            
            // cek pembayaran mhs 
            $pembayaran_cmhs_set = $pembayaran_cmhs_table->SelectWhere("
                ID_C_MHS = {$calon_mahasiswa->ID_C_MHS} AND
                ID_SEMESTER = {$id_semester} AND
                ID_DETAIL_BIAYA = {$detail_biaya->ID_DETAIL_BIAYA} AND
                TGL_BAYAR IS NOT NULL");
            
            if ($pembayaran_cmhs_set->Count() == 0)
            {
                // tambah pembayaran calon mahasiswa jika belum ada & belum bayar
                $pembayaran_cmhs = new PEMBAYARAN_CMHS();
                $pembayaran_cmhs->ID_C_MHS = $calon_mahasiswa->ID_C_MHS;
                $pembayaran_cmhs->ID_SEMESTER = $id_semester;
                $pembayaran_cmhs->ID_DETAIL_BIAYA = $detail_biaya->ID_DETAIL_BIAYA;
                $pembayaran_cmhs->BESAR_BIAYA = $besar_biaya;
                $pembayaran_cmhs->IS_TAGIH = 'Y';
                $pembayaran_cmhs_table->Insert($pembayaran_cmhs);
            }
        }
    }
}

$no_ujian = get('no_ujian');
$calon_mahasiswa_set = $calon_mahasiswa_table->SelectWhere("NO_UJIAN = '{$no_ujian}'");

if ($calon_mahasiswa_set->Count() > 0)
{
    // Inisialisasi data dr database
    $provinsi_set = $provinsi_table->Select();
    $kota_table->FillProvinsiSet($provinsi_set);
    
    // Inisialisasi data bukan dr database
    $smarty->assign('status_alamat', array(
        1 => 'Kota', 2 => 'Desa'
    ));
    
    $smarty->assign('penghasilan_ortu', array(
        1 => '&gt; 7,5 jt', 2 => '2,5 - 7,5 jt', 3 => '1,35 - 2,5 jt', 4 => '&lt; 1,35 jt'
    ));
    
    $smarty->assign('skala_pekerjaan_ortu', array(
        1 => 'Besar', 2 => 'Menengah', 3 => 'Kecil', 4 => 'Mikro'
    ));
    
    $smarty->assign('kediaman_ortu', array(
        1 => 'Mewah / Besar', 2 => 'Sedang', 3 => 'Rumah Sederhana', 4 => 'Rumah Sangat Sederhana'
    ));
    $smarty->assign('luas_tanah', array(
        1 => '&gt; 200 m2', 2 => '100 - 200 m2', 3 => '45 - 100', 4 => '&lt; 45 m2'
    ));
    $smarty->assign('luas_bangunan', array(
        1 => '&gt; 100 m2', 2 => '56 - 100 m2', 3 => '27 - 56 m2', 4 => '&lt; 27 m2'
    ));
    $smarty->assign('njop', array(
        1 => '&gt; Rp 300 jt', 2 => '100 - 300 jt', 3 => '50 - 100 jt', 4 => '&lt; 50 jt'
    ));
    $smarty->assign('listrik', array(
        1 => '450 atau kurang', 2 => '900', 3 => '1300', 4 => '2200 atau lebih'
    ));
    $smarty->assign('kendaraan_r4', array(
        1 => '&gt; 1', 2 => '1', 3 => 'Tidak Punya'
    ));
    $smarty->assign('kendaraan_r2', array(
        1 => '&gt; 2', 2 => '2', 3 => '1', 4 => 'Tidak Punya'
    ));
    
    $calon_mahasiswa = $calon_mahasiswa_set->Get(0);
    $program_studi_table->FillCalonMahasiswa($calon_mahasiswa);
    $jenjang_table->FillProgramStudi($calon_mahasiswa->PROGRAM_STUDI);
    
    // mendapatkan biaya sp3 untuk prodi mahasiswa
    $detail_biaya = $detail_biaya_table->SelectCriteria("
        LEFT JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = DETAIL_BIAYA.ID_BIAYA_KULIAH
        WHERE BK.ID_KELOMPOK_BIAYA = 5 AND DETAIL_BIAYA.ID_BIAYA = 81 AND
            BK.ID_PROGRAM_STUDI = {$calon_mahasiswa->ID_PROGRAM_STUDI} AND
            BK.ID_JALUR = {$calon_mahasiswa->ID_JALUR}")->Get(0);
    $smarty->assign('sp3_lain', $detail_biaya->BESAR_BIAYA);
    
    $smarty->assign('provinsi_set', $provinsi_set);
    $smarty->assign('calon_mahasiswa', $calon_mahasiswa);
}

$smarty->display('verifikasi/keuangan.tpl');

} else { echo "<h2>Khusus area pegawai DITKEU.</h2>"; };
?>
