<?php

include('config.php');
include('class/du_jaket.class.php');
$jaket= new du_jaket($db); 
if ($user->IsLogged() && $user->ID_UNIT_KERJA == 33) {

    $calon_mahasiswa_table = new CALON_MAHASISWA_TABLE($db);

    if ($request_method == 'POST') {
        if (post('mode') == 'ukur') {
            $calon_mahasiswa = $calon_mahasiswa_table->Single(post('id_c_mhs'));

            //$calon_mahasiswa->JAKET = post('jaket');
            //$calon_mahasiswa->MUTZ = post('mutz');

            //$calon_mahasiswa->STATUS = AUCC_CMHS_UKUR_JAKET;
	     $jaket->update_ukur_jaket(post('id_c_mhs'),post('jaket'),post('mutz'));
            //$calon_mahasiswa_table->Update($calon_mahasiswa);
            $smarty->assign('id', post('id_c_mhs'));
            $smarty->assign('mutz', post('mutz'));
            $smarty->assign('jaket', post('jaket'));
        }
    }

    $no_ujian = get('no_ujian');
    $calon_mahasiswa_set = $calon_mahasiswa_table->SelectWhere("NO_UJIAN = '{$no_ujian}'");

    if ($calon_mahasiswa_set->Count() > 0) {
        $calon_mahasiswa = $calon_mahasiswa_set->Get(0);
        $smarty->assign('calon_mahasiswa', $calon_mahasiswa);
    }

    $smarty->display('kemahasiswaan/jaket.tpl');
} else {
    echo "<h3>Halaman ini hanya untuk Dit. Kemahasiswaan.</h3>";
};
?>