<?php
include('config.php');

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDAFTARAN) {
    
$data_tab = array();

foreach ($xml_menu->tab as $tab)
{
    array_push($data_tab, array(
        'ID'    => $tab->id,
        'TITLE' => $tab->title,
        'MENU'  => $tab->menu,
        'PAGE'  => $tab->page
    ));
}

$smarty->assign('tabs', $data_tab);

$smarty->display('index.tpl');

} else { echo "<script type=\"text/javascript\">alert('Anda harus login terlebih dahulu'); window.location = 'http://".$_SERVER['HTTP_HOST']."';</script>"; }
?>