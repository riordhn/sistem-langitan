<?php
include('config.php');

if ($user->IsLogged() && $user->ID_UNIT_KERJA == 2) {

$calon_mahasiswa_table = new CALON_MAHASISWA_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);
$jenjang_table = new JENJANG_TABLE($db);
$fakultas_table = new FAKULTAS_TABLE($db);
$kota_table = new KOTA_TABLE($db);
$sekolah_table = new SEKOLAH_TABLE($db);
$provinsi_table = new PROVINSI_TABLE($db);
$pembayaran_cmhs_table = new PEMBAYARAN_CMHS_TABLE($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'save_biodata')
    {       
        $id_c_mhs = post('id_c_mhs');
        $db->Query("
            SELECT
                ID_JALUR, STATUS,
                NAMA_AYAH, ALAMAT_AYAH, ID_KOTA_AYAH, STATUS_ALAMAT_AYAH,
                NAMA_IBU, ALAMAT_IBU, ID_KOTA_IBU, STATUS_ALAMAT_IBU
            FROM CALON_MAHASISWA WHERE ID_C_MHS = {$id_c_mhs}");
        $cm = $db->FetchAssoc();
        
        $parse = $db->Parse("UPDATE CALON_MAHASISWA SET
            NM_C_MHS = :nm_c_mhs,
            ID_KOTA_LAHIR = :id_kota_lahir,
            TGL_LAHIR = :tgl_lahir,
            ALAMAT = :alamat,
            TELP = :telp,
            KEWARGANEGARAAN = :kewarganegaraan,
            KEWARGANEGARAAN_LAIN = :kewarganegaraan_lain,
            SUMBER_BIAYA = :sumber_biaya,
            BEASISWA = :beasiswa,
            SUMBER_BIAYA_LAIN = :sumber_biaya_lain,
            ID_AGAMA = :id_agama,
            JUMLAH_KAKAK = :jumlah_kakak,
            JUMLAH_ADIK = :jumlah_adik,
            ID_SEKOLAH_ASAL = :id_sekolah_asal,
            JURUSAN_SEKOLAH = :jurusan_sekolah,
            JURUSAN_SEKOLAH_LAIN = :jurusan_sekolah_lain,
            
            NO_IJAZAH = :no_ijazah,
            TGL_IJAZAH = :tgl_ijazah,
            TAHUN_LULUS = :tahun_lulus,
            JUMLAH_PELAJARAN_IJAZAH = :jumlah_pelajaran_ijazah,
            NILAI_IJAZAH = :nilai_ijazah,
            
            TAHUN_UAN = :tahun_uan,
            JUMLAH_PELAJARAN_UAN = :jumlah_pelajaran_uan,
            NILAI_UAN = :nilai_uan,
            
            PENDIDIKAN_AYAH = :pendidikan_ayah,
            PENDIDIKAN_IBU = :pendidikan_ibu,
            PEKERJAAN_AYAH = :pekerjaan_ayah,
            PEKERJAAN_AYAH_LAIN = :pekerjaan_ayah_lain,
            PEKERJAAN_IBU = :pekerjaan_ibu,
            PEKERJAAN_IBU_LAIN = :pekerjaan_ibu_lain,
            STATUS_NIM_LAMA = :status_nim_lama,
            NIM_LAMA = :nim_lama,
            STATUS_PT_ASAL = :status_pt_asal,
            NAMA_PT_ASAL = :nama_pt_asal,
            
            BERKAS_STTB = :berkas_sttb,
            BERKAS_IJAZAH = :berkas_ijazah,
            BERKAS_KP = :berkas_kp,
            BERKAS_SKHUN = :berkas_skhun,
            BERKAS_LAIN = :berkas_lain,
            
            NAMA_AYAH = :nama_ayah,
            ALAMAT_AYAH = :alamat_ayah,
            ID_KOTA_AYAH = :id_kota_ayah,
            STATUS_ALAMAT_AYAH = :status_alamat_ayah,
            
            NAMA_IBU = :nama_ibu,
            ALAMAT_IBU = :alamat_ibu,
            ID_KOTA_IBU = :id_kota_ibu,
            STATUS_ALAMAT_IBU = :status_alamat_ibu,
            
            TGL_VERIFIKASI = :tgl_verifikasi,
            STATUS = :status
            
            WHERE ID_C_MHS = :id_c_mhs");
        
        
        $db->BindByName(':id_c_mhs', post('id_c_mhs'));
        
        $db->BindByName(':nm_c_mhs', post('nm_c_mhs'));
        $db->BindByName(':id_kota_lahir', post('id_kota_lahir'));
        $db->BindByName(':tgl_lahir', date('d-M-Y', mktime(0, 0, 0, post('tgl_lahir_Month'), post('tgl_lahir_Day'), post('tgl_lahir_Year'))));
        $db->BindByName(':alamat', post('alamat'));
        $db->BindByName(':telp', post('telp'));
        $db->BindByName(':kewarganegaraan', post('kewarganegaraan'));
        $db->BindByName(':kewarganegaraan_lain', post('kewarganegaraan_lain'));
        $db->BindByName(':sumber_biaya', post('sumber_biaya'));
        $db->BindByName(':beasiswa', post('beasiswa'));
        $db->BindByName(':sumber_biaya_lain', post('sumber_biaya_lain'));
        $db->BindByName(':id_agama', post('id_agama'));
        $db->BindByName(':jumlah_kakak', post('jumlah_kakak'));
        $db->BindByName(':jumlah_adik', post('jumlah_adik'));
        
        $db->BindByName(':id_sekolah_asal', post('id_sekolah_asal'));
        $db->BindByName(':jurusan_sekolah', post('jurusan_sekolah'));
        $db->BindByName(':jurusan_sekolah_lain', post('jurusan_sekolah_lain'));
        $db->BindByName(':no_ijazah', post('no_ijazah'));
        
        if ($cm['ID_JALUR'] != 4)  // bukan alih jenis
        {   
            $db->BindByName(':tgl_ijazah', date('d-M-Y', mktime(0, 0, 0, post('tgl_ijazah_Month'), post('tgl_ijazah_Day'), post('tgl_ijazah_Year'))));    
        }
        else
        {
            $db->BindByName(':tgl_ijazah', $tgl_ijazah = '');
        }
        
        $db->BindByName(':tahun_lulus', post('tahun_lulus'));
        $db->BindByName(':jumlah_pelajaran_ijazah', post('jumlah_pelajaran_ijazah'));
        $db->BindByName(':nilai_ijazah', post('nilai_ijazah'));
        $db->BindByName(':tahun_uan', post('tahun_uan'));
        $db->BindByName(':jumlah_pelajaran_uan', post('jumlah_pelajaran_uan'));
        $db->BindByName(':nilai_uan', post('nilai_uan'));
        
        $db->BindByName(':pendidikan_ayah', post('pendidikan_ayah'));
        $db->BindByName(':pendidikan_ibu', post('pendidikan_ibu'));
        $db->BindByName(':pekerjaan_ayah', post('pekerjaan_ayah'));
        $db->BindByName(':pekerjaan_ayah_lain', post('pekerjaan_ayah_lain'));
        $db->BindByName(':pekerjaan_ibu', post('pekerjaan_ibu'));
        $db->BindByName(':pekerjaan_ibu_lain', post('pekerjaan_ibu_lain'));
        $db->BindByName(':status_nim_lama', post('status_nim_lama'));
        $db->BindByName(':nim_lama', post('nim_lama'));
        $db->BindByName(':status_pt_asal', post('status_pt_asal'));
        $db->BindByName(':nama_pt_asal', post('nama_pt_asal'));
        
        $db->BindByName(':berkas_sttb', $berkas_sttb = post('berkas_sttb') ? 1 : 0 );
        $db->BindByName(':berkas_ijazah', $berkas_ijazah = post('berkas_ijazah') ? 1 : 0);
        $db->BindByName(':berkas_kp', $berkas_kp = post('berkas_kp') ? 1 : 0);
        $db->BindByName(':berkas_skhun', $berkas_skhun = post('berkas_skhun') ? 1 : 0);
        $db->BindByName(':berkas_lain', $berkas_lain = post('berkas_lain') ? 1 : 0);
        
        $db->BindByName(':nama_ayah', post('nama_ayah', $cm['NAMA_AYAH']));
        $db->BindByName(':alamat_ayah', post('alamat_ayah', $cm['ALAMAT_AYAH']));
        $db->BindByName(':id_kota_ayah', post('id_kota_ayah', $cm['ID_KOTA_AYAH']));
        $db->BindByName(':status_alamat_ayah', post('status_alamat_ayah', $cm['STATUS_ALAMAT_AYAH']));
        
        $db->BindByName(':nama_ibu', post('nama_ibu', $cm['NAMA_IBU']));
        $db->BindByName(':alamat_ibu', post('alamat_ibu', $cm['ALAMAT_IBU']));
        $db->BindByName(':id_kota_ibu', post('id_kota_ibu', $cm['ID_KOTA_IBU']));
        $db->BindByName(':status_alamat_ibu', post('status_alamat_ibu', $cm['STATUS_ALAMAT_IBU']));
        
        $db->BindByName(':tgl_verifikasi', date('d-M-Y'));
        
        if ($cm['STATUS'] == AUCC_CMHS_VER_KEUANGAN)
            $db->BindByName(':status', $status = 7);
        else
            $db->BindByName(':status', $cm['STATUS']);
        
        $exec = $db->Execute();
        if (!$exec)
        {
            echo "<!-- Gagal Eksekusi -->";
            echo "<!-- " . print_r(error_get_last(), true) . " -->";
        }
        else
        {
            echo "<!-- BERHASIL -->";
        }
        
        write_log("{$user->ID_PENGGUNA} set berkas sttb " . post('berkas_sttb','off') . "; berkas ijazah " . post('berkas_ijazah','off') . "; berkas kp " . post('berkas_kp', 'off') . "; berkas skhun" . post('berkas_skhun', 'off') ."; berkas_lain " . post('berkas_lain', 'off') . "; ");
        
    }
    
    if (post('mode') == 'save_biodata_pasca')
    {
        $id_c_mhs = post('id_c_mhs');
        
        $db->Query("SELECT STATUS FROM CALON_MAHASISWA WHERE ID_C_MHS = {$id_c_mhs}");
        $row = $db->FetchAssoc();
        $status = $row['STATUS'];
        
        $db->Parse("
            UPDATE CALON_MAHASISWA SET
            
                NM_C_MHS = :nm_c_mhs,
                ID_KOTA_LAHIR = :id_kota_lahir,
                TGL_LAHIR = :tgl_lahir,
                ALAMAT = :alamat,
                JENIS_KELAMIN = :jenis_kelamin,
                ID_AGAMA = :id_agama,
                STATUS_PERKAWINAN = :status_perkawinan,
                SUMBER_BIAYA = :sumber_biaya,
                BEASISWA = :beasiswa,
                
                BERKAS_IJAZAH = :berkas_ijazah,
                BERKAS_TRANSKRIP = :berkas_transkrip,
                BERKAS_KESEHATAN = :berkas_kesehatan,
                BERKAS_SKCK = :berkas_skck,
                BERKAS_PEMBAYARAN = :berkas_pembayaran,
                
                TGL_VERIFIKASI = :tgl_verifikasi,
                STATUS = :status

            WHERE ID_C_MHS = {$id_c_mhs}");
        
        $db->BindByName(':nm_c_mhs', post('nm_c_mhs'));
        $db->BindByName(':id_kota_lahir', post('id_kota_lahir'));
        $db->BindByName(':tgl_lahir', date('d-M-Y', mktime(0, 0, 0, post('tgl_lahir_Month'), post('tgl_lahir_Day'), post('tgl_lahir_Year'))));
        $db->BindByName(':alamat', post('alamat'));
        $db->BindByName(':jenis_kelamin', post('jenis_kelamin'));
        $db->BindByName(':id_agama', post('id_agama'));
        $db->BindByName(':status_perkawinan', post('status_perkawinan'));
        $db->BindByName(':sumber_biaya', post('sumber_biaya'));
        $db->BindByName(':beasiswa', post('beasiswa'));
        
        $db->BindByName(':berkas_ijazah', $berkas_ijazah = post('berkas_ijazah') ? 1 : 0);
        $db->BindByName(':berkas_transkrip', $berkas_transkrip = post('berkas_transkrip') ? 1 : 0);
        $db->BindByName(':berkas_kesehatan', $berkas_kesehatan = post('berkas_kesehatan') ? 1 : 0);
        $db->BindByName(':berkas_skck', $berkas_skck = post('berkas_skck') ? 1 : 0);
        $db->BindByName(':berkas_pembayaran', $berkas_pembayaran = post('berkas_kesehatan') ? 1 : 0);
        
        $db->BindByName(':tgl_verifikasi', date('d-M-Y'));
        
        if ($status == 6) { $status = 7; }
        $db->BindByName(':status', $status);
        
        $result = $db->Execute();
        
        if ($result)
            echo "<!-- BERHASIL SIMPAN -->";
        else
            echo "<!-- GAGAL SIMPAN -->";
    }
}

$no_ujian = get('no_ujian');
//$calon_mahasiswa_set = $calon_mahasiswa_table->SelectWhere("NO_UJIAN = '{$no_ujian}'");

$db->Query("
    SELECT 
        CM.ID_C_MHS, CM.ID_JALUR, NM_FAKULTAS, NM_PROGRAM_STUDI, J.ID_JENJANG, NM_JENJANG, NM_C_MHS, NO_UJIAN, ID_KOTA_LAHIR, TGL_LAHIR, ALAMAT, TELP, JENIS_KELAMIN,
        KEWARGANEGARAAN, KEWARGANEGARAAN_LAIN, SUMBER_BIAYA, SUMBER_BIAYA_LAIN, BEASISWA, ID_AGAMA, JUMLAH_KAKAK, JUMLAH_ADIK, ID_SEKOLAH_ASAL,
        CM.JURUSAN_SEKOLAH, JURUSAN_SEKOLAH_LAIN, NO_IJAZAH, TGL_IJAZAH, TAHUN_LULUS, JUMLAH_PELAJARAN_IJAZAH, NILAI_IJAZAH, TAHUN_UAN, JUMLAH_PELAJARAN_UAN, NILAI_UAN,
        NAMA_AYAH, ALAMAT_AYAH, ID_KOTA_AYAH, STATUS_ALAMAT_AYAH, NAMA_IBU, ALAMAT_IBU, ID_KOTA_IBU, STATUS_ALAMAT_IBU,
        PEKERJAAN_AYAH, PEKERJAAN_AYAH_LAIN, PEKERJAAN_IBU, PEKERJAAN_IBU_LAIN, PENDIDIKAN_AYAH, PENDIDIKAN_IBU, STATUS_NIM_LAMA, NIM_LAMA, STATUS_PT_ASAL, NAMA_PT_ASAL,
        PENGHASILAN_ORTU, STATUS_PERKAWINAN, ALAMAT_KANTOR, PT_INSTANSI,
        CM.STATUS, KESEHATAN_KESIMPULAN_AKHIR,
        BERKAS_STTB, BERKAS_IJAZAH, BERKAS_SKHUN, BERKAS_KP, BERKAS_LAIN, BERKAS_TRANSKRIP, BERKAS_KESEHATAN, BERKAS_SKCK
    FROM CALON_MAHASISWA CM
    LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI
    LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
    LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
    WHERE CM.NO_UJIAN = '{$no_ujian}'");
$cm = $db->FetchAssoc();

if ($cm)
{
    $db->Query("
        SELECT * FROM CALON_MAHASISWA_PASCA
        WHERE ID_C_MHS = {$cm['ID_C_MHS']}");
    $cmp = $db->FetchAssoc();

    // parsing date
    $cm['TGL_LAHIR'] = date_to_timestamp($cm['TGL_LAHIR']);

    $smarty->assign('cm', $cm);
    $smarty->assign('cmp', $cmp);
    
    // Mendapatkan status pembayaran
    $db->Query("SELECT COUNT(*) AS JUMLAH_BAYAR FROM PEMBAYARAN_CMHS WHERE ID_C_MHS = {$cm['ID_C_MHS']} AND TGL_BAYAR IS NOT NULL");
    $pc = $db->FetchAssoc();
    $smarty->assign('sudah_bayar', ($pc['JUMLAH_BAYAR'] > 0) ? true : false);

    // mendapatkan provinsi dan kota
    $provinsi_set = $db->QueryToArray("SELECT ID_PROVINSI, NM_PROVINSI FROM PROVINSI");
    for ($i = 0; $i < count($provinsi_set); $i++)
        $provinsi_set[$i]['kota_set'] = $db->QueryToArray("SELECT ID_KOTA, NM_KOTA FROM KOTA WHERE ID_PROVINSI = {$provinsi_set[$i]['ID_PROVINSI']}");
    $smarty->assign('provinsi_set', $provinsi_set);

    // mendapatkan kota dan sekolah
    $sekolah_set = $db->QueryToArray("
        SELECT ID_SEKOLAH, NM_SEKOLAH, K.NM_KOTA FROM SEKOLAH S
        JOIN KOTA K ON K.ID_KOTA = S.ID_KOTA
        ORDER BY K.NM_KOTA, S.NM_SEKOLAH");
    $smarty->assign('sekolah_set', $sekolah_set);

    // mendapatkan kota
    $smarty->assign('kota_set', $db->QueryToArray("SELECT ID_KOTA, NM_KOTA FROM KOTA ORDER BY NM_KOTA"));
    
    $smarty->assign('agama', array(
        1 => 'Islam', 2 => 'Kristen Protestan', 3 => 'Kristen Katholik', 4 => 'Hindu', 5 => 'Budha', 6 => 'Lain-Lain'
    ));
    $smarty->assign('pendidikan_ortu', array(
        1 => 'Tidak Tamat SD', 2 => 'SD', 3 => 'SLTP', 4 => 'SLTA', 5 => 'Diploma', 6 => 'S1', 7 => 'S2', 8 => 'S3'
    ));
    
    $smarty->assign('status_perkawinan', array(
        1 => 'Belum Kawin', 2 => 'Kawin', 3 => 'Bercerai', 4 => 'Kawin, Suami/Istri Meninggal'
    ));
}

$smarty->display('verifikasi/pendidikan.tpl');

} else { echo "<h2>Khusus area pegawai DIT. PENDIDIKAN.</h2>"; };
?>
