<?php
include('../../config.php');

if ($request_method == 'POST')
{
    if (post('mode') == 'ganti_status')
    {
        $status = post('status');
        $id_c_mhs = post('id_c_mhs');
        $kesehatan_kesimpulan_akhir = post('kesehatan_kesimpulan_akhir');
        
        $db->Query("UPDATE CALON_MAHASISWA SET STATUS = {$status}, KESEHATAN_KESIMPULAN_AKHIR = {$kesehatan_kesimpulan_akhir} WHERE ID_C_MHS = {$id_c_mhs}");
        
        echo "Penggantian status berhasil. <a href=\"ktm-ubah.php\">Klik untuk kembali.</a>";
        exit();
    }
}

$no_ujian = get('no_ujian');

if ($no_ujian != '')
{
    $db->Query("
        SELECT
            CM.ID_C_MHS, CM.NM_C_MHS, M.NIM_MHS, CM.STATUS, CM.KESEHATAN_KESIMPULAN_AKHIR, ALAMAT, NM_JENJANG || ' ' || NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI FROM CALON_MAHASISWA CM
        LEFT JOIN MAHASISWA M ON M.ID_C_MHS = CM.ID_C_MHS
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        WHERE CM.NO_UJIAN = '{$no_ujian}'");
    $cm = $db->FetchAssoc();
    
    $smarty->assign('cm', $cm);
}

$smarty->display('ktm/ubah.tpl');
?>