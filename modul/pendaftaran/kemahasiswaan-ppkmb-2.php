<script src="jquery-1.3.2.min.js" language="javascript" type="text/javascript"></script>
<script src="jquery-ui-1.7.1.custom.min.js" language="javascript" type="text/javascript"></script>
<div class="center_title_bar">Proporsi Pembagian Mahasiswa PPKMB</div>
<?php

    include('../../config.php');
    //include('../../includes/dbconnect.php');
    //if ($user->IsLogged() && $user->ID_UNIT_KERJA == 33) {
    $jmlKol = 0;
    $jmlBar = 0;
    $p = array();
    $q = array();
    $fakultas = array();
    $dt = array();
    
    $loop_ppkmb = 0;
    $index_ppkmb = array();
    $kd_c_mhs = array();
    $id_program_studi = array();
    $nm_c_mhs = array();
    $ppkmb = array();
?>



<?php
  $db->Query("select
			count(fakultas.nm_fakultas) as daya_tampung,
			count(fakultas.nm_fakultas) / (select count(fakultas.nm_fakultas)
			from calon_mahasiswa, program_studi, fakultas, ppkmb
			where calon_mahasiswa.id_program_studi = program_studi.id_program_studi
			and program_studi.id_fakultas = fakultas.id_fakultas
			and PPKMB.ID_C_MHS = CALON_MAHASISWA.ID_C_MHS
			and ppkmb.id_c_mhs in (select distinct id_c_mhs from pembayaran_cmhs where tgl_bayar is not null)),
			fakultas.singkatan_fakultas
			from calon_mahasiswa, program_studi, fakultas, PPKMB
			where calon_mahasiswa.id_program_studi = program_studi.id_program_studi
			and program_studi.id_fakultas = fakultas.id_fakultas
			and PPKMB.ID_C_MHS = CALON_MAHASISWA.ID_C_MHS
			and PPKMB.id_c_mhs in (select distinct id_c_mhs from pembayaran_cmhs where tgl_bayar is not null)
			group by fakultas.singkatan_fakultas
			order by fakultas.singkatan_fakultas");

                            $i = 0;
//oci_fetch_array($stid, OCI_NUM))) {
                            while ($row = $db->FetchRow()){ 
                                $p[$i] = number_format($row[1], 6);
                                $q[$i] = $row[0];
                                $fakultas[$i] = $row[2];
                                $jmlKol++;
                                $jmlBar++;
                                $i++;
                            }
?>

<?php
                                for($i=1; $i<$jmlBar+1; $i++){
                                    
                                    for($j=1;$j<$jmlKol+1; $j++){
                                        
                                        if($j == $i){
                                            $dt[$i][$j] = round($p[$j-1]*$q[$i-1]);
                                            //$dt[$i][$j] = ($p[$j-1]*$q[$i-1]) - 2;
                                        }
                                        else{
                                            $dt[$i][$j] = round($p[$j-1]*$q[$i-1]);
                                            //$dt[$i][$j] = ($p[$j-1]*$q[$i-1]);
                                        }
                                        

                                    }
                                    
                                }
                                
                                //isi fakultas atas
                                for($i=1;$i<$jmlKol+1; $i++){
                                    $dt[0][$i] = $fakultas[$i-1];
                                }
                                
                                //isi fakultas kiri
                                for($i=1;$i<$jmlKol+1; $i++){
                                    $dt[$i][0] = $fakultas[$i-1];
                                }
                                
                                $dt[0][0] = ' ';
                                $dt[$jmlBar+1][0] = '<img src="gui/kemahasiswaan/sigma.JPG" />';

                                $dt[0][$jmlKol+1] = '<img src="gui/kemahasiswaan/sigma.JPG" />';
                                
                                for($i=1;$i<$jmlBar+1;$i++){
                                    $dt[$jmlBar+1][$i] = $q[$i-1];
                                }
                                
                                for($i=1;$i<$jmlKol+1;$i++){
                                    $dt[$i][$jmlKol+1] = $q[$i-1];
                                }
                                
                                //Jml Total Maba
                                $dt[$jmlBar+1][$jmlKol+1] = 0;
                                for($i=0;$i<$jmlKol;$i++){
                                    
                                    $dt[$jmlBar+1][$jmlKol+1] = $dt[$jmlBar+1][$jmlKol+1] + $q[$i];
                                    
                                }
?>
                                 
                                
<?php

                                for($i=1; $i<$jmlBar+1; $i++){
                                    
                                    for($j=1;$j<$jmlKol+1; $j++){
                                         $index_ppkmb[$i-1][$j-1] = $dt[$i][$j];
                                    }
                                    
                                }
                                
                                
                                $l = 0;
                                for($i=0; $i<$jmlBar; $i++){
                                    
                                    for($j=0;$j<$jmlKol; $j++){
                                        $batas = $index_ppkmb[$i][$j];
                                         for($k=0;$k<$batas;$k++){
                                             $ppkmb[$l] = $fakultas[$j];
                                             $l++;
                                         }
                                    }         
                                }
?>
          

<?php

    // mengetahui jumlah mahasiswa dan definisi 
    for($i=0;$i<$jmlBar;$i++){

    
    
                            //$stid = oci_parse($conn, "select calon_mahasiswa.id_c_mhs, calon_mahasiswa.no_ujian, calon_mahasiswa.nm_c_mhs as nama, program_studi.nm_program_studi as prodi, calon_mahasiswa.id_program_studi
                            $db->Query("select ppkmb.id_c_mhs, calon_mahasiswa.no_ujian, 
							calon_mahasiswa.nm_c_mhs as nama, program_studi.nm_program_studi as prodi, ppkmb.id_program_studi
							from calon_mahasiswa, program_studi, fakultas, ppkmb
							where calon_mahasiswa.id_program_studi = program_studi.id_program_studi
							and PPKMB.ID_C_MHS = CALON_MAHASISWA.ID_C_MHS
							and program_studi.id_fakultas = fakultas.id_fakultas
							--and (program_studi.id_jenjang = 1 or program_studi.id_jenjang = 5)
							and fakultas.singkatan_fakultas = '$fakultas[$i]'
							and ppkmb.id_c_mhs in (select id_c_mhs from pembayaran_cmhs where tgl_bayar is not null)
							ORDER BY dbms_random.value
                            ");
                            oci_execute($stid);
                            

                            //while (($row = oci_fetch_array($stid, OCI_NUM))) {
				while ($row = $db->FetchRow()){
        		   		$kd_c_mhs[$loop_ppkmb] = $row[0];
					$id_program_studi[$loop_ppkmb] = $row[4];
                            	$loop_ppkmb++;
                            }
    }


?>

<?php // Update Jumlah PPKMB Calon Maba

		$nppkmb = 0;
              //$stid = oci_parse($conn, "select count(id_c_mhs) from ppkmb");
		$db->Query("select count(id_c_mhs) from ppkmb");
              //oci_execute($stid);
              //while (($row = oci_fetch_array($stid, OCI_NUM))) {
		while ($row = $db->FetchRow()){
                     $nppkmb = $row[0];
              }

		if($nppkmb < $loop_ppkmb){


			//$db->Query("delete from ppkmb");

        		for($i=0; $i<$loop_ppkmb; $i++){
             			//$stid = oci_parse($conn, "INSERT INTO PPKMB (ID_C_MHS, ID_PROGRAM_STUDI) VALUES ('$kd_c_mhs[$i]', '$id_program_studi[$i]')");
				$db->Query("INSERT INTO PPKMB (ID_C_MHS, ID_PROGRAM_STUDI) VALUES ('$kd_c_mhs[$i]', '$id_program_studi[$i]')");
            			//oci_execute($stid); // The row is committed and immediately visible to other users

        		}
		}



	$isNULL = "0";
	$nNULL = 0;
	$jmlCalonMaba;

                            //$stid = oci_parse($conn, "select id_c_mhs from ppkmb where ruang_ppkmb IS NULL");
                            //oci_execute($stid);
				$db->Query("select id_c_mhs from ppkmb where ruang_ppkmb = '$isNULL' or ruang_ppkmb IS NULL");
                            //while ($row = oci_fetch_array($stid, OCI_NUM))) {
				while ($row = $db->FetchRow()){
                                $nNULL++;
                            }
    
    if($nNULL > 2){
	
        	for($i=0; $i<$loop_ppkmb; $i++){
             		//$stid = oci_parse($conn, "UPDATE PPKMB SET RUANG_PPKMB = '$ppkmb[$i]' WHERE ID_C_MHS = '$kd_c_mhs[$i]'");
            		//oci_execute($stid); // The row is committed and immediately visible to other users
			$db->Query("UPDATE PPKMB SET RUANG_PPKMB = '$ppkmb[$i]' WHERE ID_C_MHS = '$kd_c_mhs[$i]'");

        	}
        	//oci_commit($conn);
    }

?>

<?php // Membaca Jumlah PPKMB Calon Maba dari database

                              
                            $ke = 0;
                            for($i=1; $i<$jmlBar+1; $i++){
                            for($j=1; $j<$jmlKol+1; $j++){
                                
                                
                                $src = $fakultas[$i-1];
                                $dest = $fakultas[$j-1];
                                
                                //$stid = oci_parse($conn, "select 
								$db->Query("select
                                count(ppkmb.ruang_ppkmb)
                                from ppkmb, calon_mahasiswa, program_studi, fakultas 
                                where ppkmb.id_c_mhs = calon_mahasiswa.id_c_mhs
								and ppkmb.id_program_studi = program_studi.id_program_studi 
                                and program_studi.id_fakultas = fakultas.id_fakultas
								and (program_studi.id_jenjang = 1 or program_studi.id_jenjang = 5)
								and ppkmb.id_c_mhs in (select id_c_mhs from pembayaran_cmhs where tgl_bayar is not null)
								and calon_mahasiswa.id_jalur <> 4
                                and ppkmb.ruang_ppkmb = '$dest'
                                and fakultas.singkatan_fakultas = '$src'
                                ");
                                //oci_execute($stid);
                            
                                    //while (($row = oci_fetch_array($stid, OCI_NUM))) {
									while ($row = $db->FetchRow()){
                                             $dt[$i][$j] = $row[0];
                                    }
                                }

                                    
                            }


?>


                            <table cellpadding="0" cellspacing="0" border="0" style="font-size: 18px;">
                                <?php
                                for($i=0; $i<$jmlBar+2; $i++){
                                    echo '<tr>';
                                    for($j=0;$j<$jmlKol+2;$j++){
                                    ?>
                                    
                                    <?php
                                        if(is_numeric($dt[$i][$j])){
											
                                    ?>
                                        
                                            <td>
											<center>  

						 <?php if($i < 14){ ?>
						 <a href="kemahasiswaan-ppkmb-2.php?src=<?php echo $fakultas[$i-1]; ?>&amp;dest=<?php echo $fakultas[$j-1]; ?>&amp;n=<?php echo $dt[$i][$j]; ?>&amp;action=update" >
						  
                                            <?php
                                                echo $dt[$i][$j];
                                            ?>
                                            </a>
						  <?php }
							else{
						   ?>
											<a href="kemahasiswaan-ppkmb-2.php?dest=<?php echo $fakultas[$j-1]; ?>&amp;n=<?php echo $dt[$i][$j]; ?>&amp;action=cetak" >
						  
                                            <?php
                                                echo $dt[$i][$j];
                                            ?>
                                            </a>
						  <?php 
							}
						  ?>
                                            </center>    
                                            </td>
                                      <?php
                                        }else{
                                            
                                            echo '<td>';  
                                           
                                                echo $dt[$i][$j];
                                                                          
                                            echo '</td>';
                                        }
                                      ?>
                                        
                                        <?php
                                    }
                                    echo '</tr>';
                                }
                                ?>
                            </table>

<?php
	if($_GET['action']=='update'){

        $dest = $_GET['dest'];
        $src = $_GET['src'];

	$n = $_GET['n'];
	$no_ujian = array();
	$ruang_kelas = array();
	$jml_mhs = 0;
	$nperkelas = 0;
	$kelas = 0;
	$kd_c_mhs = array();


	$kelasFEB = 10;
	$kelasFF = 1;
	$kelasFH = 2;
	$kelasFIB = 2;
	$kelasFISIP = 2;
	$kelasFK = 3;
	$kelasFKG = 2;
	$kelasFKH = 2;
	$kelasFKM = 3;
	$kelasFKp = 2;
	$kelasFPK = 2;
	$kelasFPsi = 1;
	$kelasFST = 4;

	
	if($_GET['dest'] == 'FEB'){
		$nperkelas = ceil($n/$kelasFEB);
		$kelas = $kelasFEB;
	}
	if($_GET['dest'] == 'FF'){
		$nperkelas = ceil($n/$kelasFF);
		$kelas = $kelasFF;
	}
	if($_GET['dest'] == 'FH'){
		$nperkelas = ceil($n/$kelasFH);
		$kelas = $kelasFH;
	}
	if($_GET['dest'] == 'FIB'){
		$nperkelas = ceil($n/$kelasFIB);
		$kelas = $kelasFIB;
	}
	if($_GET['dest'] == 'FISIP'){
		$nperkelas = ceil($n/$kelasFISIP);
		$kelas = $kelasFISIP;
	}
	if($_GET['dest'] == 'FK'){
		$nperkelas = ceil($n/$kelasFK);
		$kelas = $kelasFK;
	}
	if($_GET['dest'] == 'FKG'){
		$nperkelas = ceil($n/$kelasFKG);
		$kelas = $kelasFKG;
	}
	if($_GET['dest'] == 'FKH'){
		$nperkelas = ceil($n/$kelasFKH);
		$kelas = $kelasFKH;
	}
	if($_GET['dest'] == 'FKM'){
		$nperkelas = ceil($n/$kelasFKM);
		$kelas = $kelasFKM;
	}
	if($_GET['dest'] == 'FKp'){
		$nperkelas = ceil($n/$kelasFKp);
		$kelas = $kelasFKp;
	}
	if($_GET['dest'] == 'FPK'){
		$nperkelas = ceil($n/$kelasFPK);
		$kelas = $kelasFPK;
	}
	if($_GET['dest'] == 'FPsi'){
		$nperkelas = ceil($n/$kelasFPsi);
		$kelas = $kelasFPsi;
	}
	if($_GET['dest'] == 'FST'){
		$nperkelas = ceil($n/$kelasFST);
		$kelas = $kelasFST;
	}

	echo "Kapasitas : " . $n;
	echo "<br />";

	echo "Jumlah Ruang : " . $kelas;
	echo "<br />";

	echo "Jumlah Mahasiswa Per kelas : " . $nperkelas;
	echo "<br />";
	


$nm_c_mhs = array();
$nm_program_studi = array();
$singkatan_fakultas = array();
$nim_mhs = array();



                            //$stid = oci_parse($conn, "select cm.nm_c_mhs, ps.nm_program_studi, f.singkatan_fakultas from calon_mahasiswa cm
				$db->Query("select cm.nm_c_mhs, ps.nm_program_studi, f.singkatan_fakultas, ppkmb.id_c_mhs, mhs.nim_mhs
							from calon_mahasiswa cm
							left join ppkmb ppkmb on ppkmb.id_c_mhs = cm.id_c_mhs
							left join program_studi ps on ps.id_program_studi = ppkmb.id_program_studi
							left join fakultas f on f.id_fakultas = ps.id_fakultas
							left join mahasiswa mhs on mhs.id_c_mhs = cm.id_c_mhs
							where (ps.id_jenjang = 1 or ps.id_jenjang = 5)
							and ppkmb.id_c_mhs in (select id_c_mhs from pembayaran_cmhs where tgl_bayar is not null)
							and cm.id_jalur <> 4
                            and ppkmb.ruang_ppkmb = '$dest'
                            and f.singkatan_fakultas = '$src'
                            ");
                            oci_execute($stid);
                            $i = 0;
                            //while (($row = oci_fetch_array($stid, OCI_NUM))) {
				while ($row = $db->FetchRow()){ 
                                $nm_c_mhs[$i] = $row[0];
				    $nm_program_studi[$i] = $row[1];
				    $singkatan_fakultas[$i] = $row[2];
					$kd_c_mhs[$i] = $row[3];
					$nim_mhs[$i] = $row[4];
                                $i++;
				    $jml_mhs++;                                  
                 }

				$ni = 0;
				for($i=0; $i<$kelas; $i++){
					for($j=0; $j<$nperkelas; $j++){
						$ruang_kelas[$ni] = $i+1;
						$ni++;
					}
				}

			
        	for($i=0; $i<$ni; $i++){
             	//	//$stid = oci_parse($conn, "UPDATE PPKMB SET RUANG_PPKMB = '$ppkmb[$i]' WHERE ID_C_MHS = '$kd_c_mhs[$i]'");
            	//	//oci_execute($stid); // The row is committed and immediately visible to other users
					$db->Query("UPDATE PPKMB SET KELAS_PPKMB = '$ruang_kelas[$i]' WHERE ID_C_MHS = '$kd_c_mhs[$i]'");

        	}
			
                            
				echo '<table cellpadding="1" cellspacing="1" border="1">';
				echo "<tr>";
					echo "<td>";
						echo "NO";
					echo "</td>";
					echo "<td>";
						echo "NAMA MAHASISWA";
					echo "</td>";
					echo "<td>";
						echo "NIM";
					echo "</td>";
					echo "<td>";
						echo "PROGRAM STUDI ASAL";
					echo "</td>";
					echo "<td>";
						echo "RUANG";
					echo "</td>";
					echo "<td>";
						echo "KELAS";
					echo "</td>";
				echo "</tr>";
				for($i=0; $i<$jml_mhs; $i++){
					echo "<tr>";
					echo "<td>";
					echo $i+1;
					echo "</td>";
					echo "<td>";
					echo $nm_c_mhs[$i];
					echo "</td>";

					echo "<td>";
					echo $nim_mhs[$i];
					echo "</td>";

					echo "<td>";
					echo $nm_program_studi[$i];
					echo "</td>";
					echo "<td>";
					echo $dest;
					echo "</td>";
					echo "<td>";
					echo $ruang_kelas[$i];
					echo "</td>";
					echo "</tr>";
				}
				echo "</table>";
				/*
				for($i=0; $i<$kelas; $i++){
					for($j=0; $j<$nperkelas; $j++){
						echo $nm_c_mhs[$i]; echo "  "; echo $nm_program_studi; echo "  "; echo $singkatan_fakultas; echo "  "; echo $i+1;
						echo "<br />";
					}
				}
				*/

	}
	if($_GET['action'] == 'cetak'){
        $dest = $_GET['dest'];

?>
<form action="cetak-ppkmb.php?dest=<?php echo $dest; ?>&amp;kelas=<?php echo $_GET['kelas']; ?>" method="post" onsubmit='$("#datatodisplay").val( $("<div>").append( $("#ReportTable").eq(0).clone() ).html() )'>
<?php
		echo "<table>";
		echo "<tr>";
			echo "<td>";
				echo "PILIH KELAS : ";	
			echo "</td>";
			echo "<td>";
				$db->Query("select ppkmb.kelas_ppkmb
							from calon_mahasiswa cm
							left join ppkmb ppkmb on ppkmb.id_c_mhs = cm.id_c_mhs
							left join program_studi ps on ps.id_program_studi = ppkmb.id_program_studi
							left join fakultas f on f.id_fakultas = ps.id_fakultas
							where (ps.id_jenjang = 1 or ps.id_jenjang = 5)
							and ppkmb.id_c_mhs in (select id_c_mhs from pembayaran_cmhs where tgl_bayar is not null)
							and cm.id_jalur <> 4
                            and ppkmb.ruang_ppkmb = '$dest'
							group by ppkmb.kelas_ppkmb
							order by ppkmb.kelas_ppkmb
                            ");	

			while ($row = $db->FetchRow()){
				?>
				
				<a href="kemahasiswaan-ppkmb-2.php?dest=<?php echo $dest; ?>&amp;n=<?php echo $dt[$i][$j]; ?>&amp;kelas=<?php echo $row[0]; ?>&amp;action=cetak" >
					<?php
						echo $row[0];
						echo "  ";
					?>
				</a>
				

				<?php
			}

			echo "</td>";
			echo "<td>";

			?>

<a href="kemahasiswaan-ppkmb-2.php?dest=<?php echo $dest; ?>&amp;n=<?php echo $dt[$i][$j]; ?>&amp;kelas=<?php echo $row[0]; ?>&amp;action=cetak" onclick="window.open('http://cybercampus.unair.ac.id/modul/pendaftaran/ppkmb-non-alih1.php?dest=<?php echo $dest; ?>&amp;kelas=<?php echo $_GET['kelas'];
?>','popup','width=1024,height=700,scrollbars=yes,resizable=yes,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false">
				CETAK KELULUSAN 
			</a>

			</td>
			<td>
<a href="kemahasiswaan-ppkmb-2.php?dest=<?php echo $dest; ?>&amp;n=<?php echo $dt[$i][$j]; ?>&amp;kelas=<?php echo $row[0]; ?>&amp;action=cetak" onclick="window.open('http://cybercampus.unair.ac.id/modul/pendaftaran/cetak-ppkmb.php?dest=<?php echo $dest; ?>&amp;kelas=<?php echo $_GET['kelas'];
?>','popup','width=1024,height=700,scrollbars=yes,resizable=yes,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false">
				CETAK ABSENSI 
			</a>
			</td>
		<?php
		echo "</tr>";
		echo "</table>";
		
		if($_GET['kelas']){
		$get_kelas = $_GET['kelas'];
                            //$stid = oci_parse($conn, "select cm.nm_c_mhs, ps.nm_program_studi, f.singkatan_fakultas from calon_mahasiswa cm
				$db->Query("select cm.nm_c_mhs, ps.nm_program_studi, ppkmb.ruang_ppkmb, ppkmb.kelas_ppkmb
							from calon_mahasiswa cm
							left join ppkmb ppkmb on ppkmb.id_c_mhs = cm.id_c_mhs
							left join program_studi ps on ps.id_program_studi = ppkmb.id_program_studi
							left join fakultas f on f.id_fakultas = ps.id_fakultas
							where (ps.id_jenjang = 1 or ps.id_jenjang = 5)
							and ppkmb.id_c_mhs in (select id_c_mhs from pembayaran_cmhs where tgl_bayar is not null)
							and ppkmb.ruang_ppkmb = '$dest'
							and cm.id_jalur <> 4
							and ppkmb.kelas_ppkmb = '$get_kelas'
                            ");
                            //oci_execute($stid);
                            $i = 0;
                            echo "Mahasiswa Baru dari " . $src . " Yang PPKMB di " . $dest . '<br />';
				echo '<table cellpadding="1" cellspacing="1" border="1">';
				echo "<tr>";
					echo "<td>";
						echo "NO";
					echo "</td>";
					echo "<td>";
						echo "NAMA MAHASISWA";
					echo "</td>";
					echo "<td>";
						echo "PROGRAM STUDI ASAL";
					echo "</td>";
					echo "<td>";
						echo "RUANG";
					echo "</td>";
					echo "<td>";
						echo "KELAS";
					echo "</td>";
					echo "</tr>";
				$i = 1;
				while ($row = $db->FetchRow()){                          
					echo "<tr>";
					echo "<td>";
					echo $i;
					echo "</td>";
					echo "<td>";
					echo $row[0];
					echo "</td>";
					echo "<td>";
					echo $row[1];
					echo "</td>";
					echo "<td>";
					echo $row[2];
					echo "</td>";
					echo "<td>";
					echo $row[3];
					echo "</td>";
					echo "</tr>";
					$i++;
				}
				echo "</table>";
		}
	}
?>
</form>



<?php
   // } else { echo "<h3>Halaman ini hanya untuk Dit. Kemahasiswaan.</h3>"; 
   //};
?>
