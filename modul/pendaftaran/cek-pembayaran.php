<?php

include "../../config.php";
$data = array();
if (post('nim')) {
    
    $nim = str_replace("'", "''", post('nim'));
    $query = "SELECT * FROM 
        (SELECT M.ID_MHS AS ID1,P.NM_PENGGUNA,NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,M.NIM_MHS,PEM.*,B.NM_BIAYA,J.NM_JENJANG,M.STATUS_CEKAL
        FROM PENGGUNA P
        LEFT JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
        LEFT JOIN PROGRAM_STUDI PR ON M.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
        LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS
        LEFT JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
        LEFT JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        LEFT JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
        ) X1
        LEFT JOIN
        (SELECT M.ID_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_BIAYA FROM PEMBAYARAN PEM
        LEFT JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
        GROUP BY M.ID_MHS
        ) X2 
        ON X1.ID1 = X2.ID2
        WHERE X1.NIM_MHS ='{$nim}'";
    $db->Query($query);
    
    while ($temp = $db->FetchArray()) {
        array_push($data, $temp);
    }
    if ($data)
        $smarty->assign('data_pembayaran', $data);
}

$smarty->display('cek-pembayaran.tpl');
?>
