<?php

include('config.php');
include('class/ppmb.class.php');

if ($user->IsLogged() && $user->ID_PENGGUNA == 48772) {

    $calon_mahasiswa_table = new CALON_MAHASISWA_TABLE($db);
    $program_studi_table = new PROGRAM_STUDI_TABLE($db);
    $negara_table = new NEGARA_TABLE($db);
    $provinsi_table = new PROVINSI_TABLE($db);
    $kota_table = new KOTA_TABLE($db);

    if ($request_method == 'POST') {
        if (post('mode') == 'save_s1') {
            $id_c_ms = post('id_c_mhs');

            $db->Parse("UPDATE CALON_MAHASISWA SET
            NM_C_MHS = :nm_c_mhs,
            ID_KOTA_LAHIR = :id_kota_lahir,
            TGL_LAHIR = :tgl_lahir,
            ALAMAT = :alamat,
            TELP = :telp,
            JENIS_KELAMIN = :jenis_kelamin,
            KEWARGANEGARAAN = :kewarganegaraan,
            KEWARGANEGARAAN_LAIN = :kewarganegaraan_lain,
            SUMBER_BIAYA = :sumber_biaya,
            BEASISWA = :beasiswa,
            SUMBER_BIAYA_LAIN = :sumber_biaya_lain,
            ID_AGAMA = :id_agama,
            JUMLAH_KAKAK = :jumlah_kakak,
            JUMLAH_ADIK = :jumlah_adik,
            ID_SEKOLAH_ASAL = :id_sekolah_asal,
            JURUSAN_SEKOLAH = :jurusan_sekolah,
            JURUSAN_SEKOLAH_LAIN = :jurusan_sekolah_lain,
            NO_IJAZAH = :no_ijazah,
            TGL_IJAZAH = :tgl_ijazah,
            TAHUN_LULUS = :tahun_lulus,
            JUMLAH_PELAJARAN_IJAZAH = :jumlah_pelajaran_ijazah,
            NILAI_IJAZAH = :nilai_ijazah,
            TAHUN_UAN = :tahun_uan,
            JUMLAH_PELAJARAN_UAN = :jumlah_pelajaran_uan,
            NILAI_UAN = :nilai_uan,
            
            NAMA_AYAH = :nama_ayah,
            ALAMAT_AYAH = :alamat_ayah,
            ID_KOTA_AYAH = :id_kota_ayah,
            STATUS_ALAMAT_AYAH = :status_alamat_ayah,
            
            NAMA_IBU = :nama_ibu,
            ALAMAT_IBU = :alamat_ibu,
            ID_KOTA_IBU = :id_kota_ibu,
            STATUS_ALAMAT_IBU = :status_alamat_ibu,
            
            PENDIDIKAN_AYAH = :pendidikan_ayah,
            PENDIDIKAN_IBU = :pendidikan_ibu,
            PEKERJAAN_AYAH = :pekerjaan_ayah,
            PEKERJAAN_AYAH_LAIN = :pekerjaan_ayah_lain,
            PEKERJAAN_IBU = :pekerjaan_ibu,
            PEKERJAAN_IBU_LAIN = :pekerjaan_ibu_lain,
            STATUS_NIM_LAMA = :status_nim_lama,
            NIM_LAMA = :nim_lama,
            STATUS_PT_ASAL = :status_pt_asal,
            NAMA_PT_ASAL = :nama_pt_asal,
            
            ID_PILIHAN_1 = :id_pilihan_1,
            ID_PILIHAN_2 = :id_pilihan_2,
            ID_PILIHAN_3 = :id_pilihan_3,
            ID_PILIHAN_4 = :id_pilihan_4
            
            WHERE ID_C_MHS = :id_c_mhs");

            $db->BindByName(':id_c_mhs', $id_c_ms);

            $db->BindByName(':nm_c_mhs', post('nm_c_mhs'));
            $db->BindByName(':id_kota_lahir', post('id_kota_lahir'));
            $db->BindByName(':tgl_lahir', date('d-M-Y', mktime(0, 0, 0, intval(post('tgl_lahir_Month')), intval(post('tgl_lahir_Day')), post('tgl_lahir_Year'))));
            $db->BindByName(':alamat', post('alamat'));
            $db->BindByName(':telp', post('telp'));

            $db->BindByName(':jenis_kelamin', post('jenis_kelamin'));

            $db->BindByName(':kewarganegaraan', post('kewarganegaraan'));
            $db->BindByName(':kewarganegaraan_lain', post('kewarganegaraan_lain'));
            $db->BindByName(':sumber_biaya', post('sumber_biaya'));
            $db->BindByName(':beasiswa', post('beasiswa'));
            $db->BindByName(':sumber_biaya_lain', post('sumber_biaya_lain'));
            $db->BindByName(':id_agama', post('id_agama'));
            $db->BindByName(':jumlah_kakak', post('jumlah_kakak'));
            $db->BindByName(':jumlah_adik', post('jumlah_adik'));
            $db->BindByName(':id_sekolah_asal', post('id_sekolah_asal'));
            $db->BindByName(':jurusan_sekolah', post('jurusan_sekolah'));
            $db->BindByName(':jurusan_sekolah_lain', post('jurusan_sekolah_lain'));
            $db->BindByName(':no_ijazah', post('no_ijazah'));
            $db->BindByName(':tgl_ijazah', date('d-M-Y', mktime(0, 0, 0, intval(post('tgl_ijazah_Month')), intval(post('tgl_ijazah_Day')), post('tgl_ijazah_Year'))));
            $db->BindByName(':tahun_lulus', post('tahun_lulus'));
            $db->BindByName(':jumlah_pelajaran_ijazah', post('jumlah_pelajaran_ijazah'));
            $db->BindByName(':nilai_ijazah', post('nilai_ijazah'));
            $db->BindByName(':tahun_uan', post('tahun_uan'));
            $db->BindByName(':jumlah_pelajaran_uan', post('jumlah_pelajaran_uan'));
            $db->BindByName(':nilai_uan', post('nilai_uan'));

            $db->BindByName(':nama_ayah', post('nama_ayah'));
            $db->BindByName(':alamat_ayah', post('alamat_ayah'));
            $db->BindByName(':id_kota_ayah', post('id_kota_ayah'));
            $db->BindByName(':status_alamat_ayah', post('status_alamat_ayah'));

            $db->BindByName(':nama_ibu', post('nama_ibu'));
            $db->BindByName(':alamat_ibu', post('alamat_ibu'));
            $db->BindByName(':id_kota_ibu', post('id_kota_ibu'));
            $db->BindByName(':status_alamat_ibu', post('status_alamat_ibu'));

            $db->BindByName(':pendidikan_ayah', post('pendidikan_ayah'));
            $db->BindByName(':pendidikan_ibu', post('pendidikan_ibu'));
            $db->BindByName(':pekerjaan_ayah', post('pekerjaan_ayah'));
            $db->BindByName(':pekerjaan_ayah_lain', post('pekerjaan_ayah_lain'));
            $db->BindByName(':pekerjaan_ibu', post('pekerjaan_ibu'));
            $db->BindByName(':pekerjaan_ibu_lain', post('pekerjaan_ibu_lain'));
            $db->BindByName(':status_nim_lama', post('status_nim_lama'));
            $db->BindByName(':nim_lama', post('nim_lama'));
            $db->BindByName(':status_pt_asal', post('status_pt_asal'));
            $db->BindByName(':nama_pt_asal', post('nama_pt_asal'));

            $db->BindByName(':id_pilihan_1', post('id_pilihan_1'));
            $db->BindByName(':id_pilihan_2', post('id_pilihan_2'));
            $db->BindByName(':id_pilihan_3', post('id_pilihan_3'));
            $db->BindByName(':id_pilihan_4', post('id_pilihan_4'));

            $execute = $db->Execute();
        } else if (post('mode') == 'save_alih') {
            $db->Parse("UPDATE CALON_MAHASISWA SET
            NM_C_MHS = :nm_c_mhs,
            ID_KOTA_LAHIR = :id_kota_lahir,
            TGL_LAHIR = :tgl_lahir,
            ALAMAT = :alamat,
            TELP = :telp,
            JENIS_KELAMIN = :jenis_kelamin,
            KEWARGANEGARAAN = :kewarganegaraan,
            KEWARGANEGARAAN_LAIN = :kewarganegaraan_lain,
            SUMBER_BIAYA = :sumber_biaya,
            BEASISWA = :beasiswa,
            SUMBER_BIAYA_LAIN = :sumber_biaya_lain,
            ID_AGAMA = :id_agama,
            JUMLAH_KAKAK = :jumlah_kakak,
            JUMLAH_ADIK = :jumlah_adik,
            
            PENGHASILAN_ORTU = :penghasilan_ortu,

            NAMA_AYAH = :nama_ayah,
            ALAMAT_AYAH = :alamat_ayah,
            ID_KOTA_AYAH = :id_kota_ayah,
            STATUS_ALAMAT_AYAH = :status_alamat_ayah,
            
            NAMA_IBU = :nama_ibu,
            ALAMAT_IBU = :alamat_ibu,
            ID_KOTA_IBU = :id_kota_ibu,
            STATUS_ALAMAT_IBU = :status_alamat_ibu,
            
            PENDIDIKAN_AYAH = :pendidikan_ayah,
            PENDIDIKAN_IBU = :pendidikan_ibu,
            PEKERJAAN_AYAH = :pekerjaan_ayah,
            PEKERJAAN_AYAH_LAIN = :pekerjaan_ayah_lain,
            PEKERJAAN_IBU = :pekerjaan_ibu,
            PEKERJAAN_IBU_LAIN = :pekerjaan_ibu_lain,
            
            ID_PILIHAN_1 = :id_pilihan_1,
            ID_PILIHAN_2 = :id_pilihan_2,
            ID_PILIHAN_3 = :id_pilihan_3,
            ID_PILIHAN_4 = :id_pilihan_4
            
            WHERE ID_C_MHS = :id_c_mhs");

            $db->BindByName(':id_c_mhs', post('id_c_mhs'));
            $db->BindByName(':nm_c_mhs', post('nm_c_mhs'));
            $db->BindByName(':id_kota_lahir', post('id_kota_lahir'));
            $db->BindByName(':tgl_lahir', date('d-M-Y', mktime(0, 0, 0, intval(post('tgl_lahir_Month')), intval(post('tgl_lahir_Day')), post('tgl_lahir_Year'))));
            $db->BindByName(':alamat', post('alamat'));
            $db->BindByName(':telp', post('telp'));

            $db->BindByName(':jenis_kelamin', post('jenis_kelamin'));

            $db->BindByName(':kewarganegaraan', post('kewarganegaraan'));
            $db->BindByName(':kewarganegaraan_lain', post('kewarganegaraan_lain'));
            $db->BindByName(':id_agama', post('id_agama'));
            $db->BindByName(':sumber_biaya', post('sumber_biaya'));
            $db->BindByName(':beasiswa', post('beasiswa'));
            $db->BindByName(':sumber_biaya_lain', post('sumber_biaya_lain'));
            $db->BindByName(':jumlah_kakak', post('jumlah_kakak'));
            $db->BindByName(':jumlah_adik', post('jumlah_adik'));
            $db->BindByName(':penghasilan_ortu', post('penghasilan_ortu'));
            $db->BindByName(':nama_ayah', post('nama_ayah'));
            $db->BindByName(':alamat_ayah', post('alamat_ayah'));
            $db->BindByName(':id_kota_ayah', post('id_kota_ayah'));
            $db->BindByName(':status_alamat_ayah', post('status_alamat_ayah'));
            $db->BindByName(':pendidikan_ayah', post('pendidikan_ayah'));
            $db->BindByName(':pekerjaan_ayah', post('pekerjaan_ayah'));
            $db->BindByName(':pekerjaan_ayah_lain', post('pekerjaan_ayah_lain'));

            $db->BindByName(':nama_ibu', post('nama_ibu'));
            $db->BindByName(':alamat_ibu', post('alamat_ibu'));
            $db->BindByName(':id_kota_ibu', post('id_kota_ibu'));
            $db->BindByName(':status_alamat_ibu', post('status_alamat_ibu'));
            $db->BindByName(':pendidikan_ibu', post('pendidikan_ibu'));
            $db->BindByName(':pekerjaan_ibu', post('pekerjaan_ibu'));
            $db->BindByName(':pekerjaan_ibu_lain', post('pekerjaan_ibu_lain'));

            $db->BindByName(':status_nim_lama', post('status_nim_lama'));
            $db->BindByName(':id_pilihan_1', post('id_pilihan_1'));
            $db->BindByName(':id_pilihan_2', post('id_pilihan_2'));
            $db->BindByName(':id_pilihan_3', post('id_pilihan_3'));
            $db->BindByName(':id_pilihan_4', post('id_pilihan_4'));

            $execute = $db->Execute();

            if (!$execute) {
                echo "GAGAL SIMPAN";
            }
            $db->Parse("
            UPDATE CALON_MAHASISWA_PASCA SET
                PTN_S1 = :ptn_s1,
                STATUS_PTN_S1 = :status_ptn_s1,
                PRODI_S1 = :prodi_s1,
                TGL_MASUK_S1 = :tgl_masuk_s1,
                TGL_LULUS_S1 = :tgl_lulus_s1,
                LAMA_STUDI_S1 = :lama_studi_s1,
                IP_S1 = :ip_s1
                
            WHERE ID_C_MHS = :id_c_mhs");

            $db->BindByName(':id_c_mhs', post('id_c_mhs'));
            $db->BindByName(':ptn_s1', post('ptn_s1'));
            $db->BindByName(':status_ptn_s1', post('status_ptn_s1'));
            $db->BindByName(':prodi_s1', post('prodi_s1'));
            $db->BindByName(':tgl_masuk_s1', date('d-M-Y', mktime(0, 0, 0, post('tgl_masuk_s1_Month'), post('tgl_masuk_s1_Day'), post('tgl_masuk_s1_Year'))));
            $db->BindByName(':tgl_lulus_s1', date('d-M-Y', mktime(0, 0, 0, post('tgl_lulus_s1_Month'), post('tgl_lulus_s1_Day'), post('tgl_lulus_s1_Year'))));
            $db->BindByName(':lama_studi_s1', post('lama_studi_s1'));
            $db->BindByName(':ip_s1', post('ip_s1'));

            $db->Execute();
        }
    }

    $kode_voucher = get('kode_voucher');

    $calon_mahasiswa_set = $calon_mahasiswa_table->SelectWhere("KODE_VOUCHER = '{$kode_voucher}'");

    if ($calon_mahasiswa_set->Count() > 0) {
        $calon_mahasiswa = $calon_mahasiswa_set->Get(0);
        if ($calon_mahasiswa->ID_JALUR == 3 || $calon_mahasiswa->ID_JALUR == 5) {
            // pendidikan orang tua
            $smarty->assign('pendidikan_ortu', array(
                1 => 'Tidak Tamat SD', 2 => 'SD', 3 => 'SLTP', 4 => 'SLTA', 5 => 'Diploma', 6 => 'S1', 7 => 'S2', 8 => 'S3'
            ));

            // negara dan kota
            $negara = $negara_table->SelectCriteria('ORDER BY NM_NEGARA ASC');
            $provinsi_set = $provinsi_table->Select();
            $kota_table->FillProvinsiSet($provinsi_set);

            // mendapatkan sususan combobox sekolah
            //$sekolah_set = $sekolah_table->SelectCriteria("WHERE ID_KOTA = (SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL}) ORDER BY NM_SEKOLAH ASC");
            if ($calon_mahasiswa->ID_SEKOLAH_ASAL > 0) {
                // mendapatkan sekolah
                $sekolah_set = array();
                $db->Query("SELECT * FROM SEKOLAH WHERE ID_KOTA = (SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL}) ORDER BY NM_SEKOLAH ASC");
                while ($row = $db->FetchAssoc())
                    array_push($sekolah_set, $row);
                $smarty->assign('sekolah_set', $sekolah_set);

                // kota terpilih
                $db->Query("SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL}");
                $row = $db->FetchAssoc();
                $smarty->assign('id_kota_sekolah', $row['ID_KOTA']);

                // mendapatkan list kota sekolah
                $kota_set = array();
                $db->Query("
            SELECT * FROM KOTA WHERE ID_PROVINSI = (
                SELECT ID_PROVINSI FROM KOTA WHERE ID_KOTA = (SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL})
            ) ORDER BY NM_KOTA");
                while ($row = $db->FetchAssoc())
                    array_push($kota_set, $row);
                $smarty->assign('kota_set', $kota_set);

                // provinsi terpilih
                $db->Query("SELECT ID_PROVINSI FROM KOTA WHERE ID_KOTA = (SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL})");
                $row = $db->FetchAssoc();
                $smarty->assign('id_provinsi_sekolah', $row['ID_PROVINSI']);

                // mendapatkan list provinsi sekolah
                $provinsi_sekolah_set = array();
                $db->Query("
            SELECT * FROM PROVINSI WHERE ID_NEGARA = (
                SELECT ID_NEGARA FROM PROVINSI WHERE ID_PROVINSI = (
                    SELECT ID_PROVINSI FROM KOTA WHERE ID_KOTA = (SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL})))");
                while ($row = $db->FetchAssoc())
                    array_push($provinsi_sekolah_set, $row);
                $smarty->assign('provinsi_sekolah_set', $provinsi_sekolah_set);

                // negara terpilih
                $db->Query("SELECT ID_NEGARA FROM PROVINSI WHERE ID_PROVINSI = (SELECT ID_PROVINSI FROM KOTA WHERE ID_KOTA = (SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL}))");
                $row = $db->FetchAssoc();
                $smarty->assign('id_negara_sekolah', $row['ID_NEGARA']);
            }
            $smarty->assign('negara', $negara);
            $smarty->assign('provinsi_set', $provinsi_set);
            $smarty->assign('calon_mahasiswa', $calon_mahasiswa);

            // PROGRAM STUDI IPA & IPS & all
            $prodi_ipa_set = $calon_mahasiswa->ID_JALUR == 3 ? $program_studi_table->SelectCriteria("WHERE ID_JENJANG = 1 AND JURUSAN_SEKOLAH = 1") : $program_studi_table->SelectCriteria("WHERE ID_JENJANG = 5 AND JURUSAN_SEKOLAH = 1 AND ID_PROGRAM_STUDI NOT IN (80,82,81)");
            $prodi_ips_set = $calon_mahasiswa->ID_JALUR == 3 ? $program_studi_table->SelectCriteria("WHERE ID_JENJANG = 1 AND JURUSAN_SEKOLAH = 2") : $program_studi_table->SelectCriteria("WHERE ID_JENJANG = 5 AND JURUSAN_SEKOLAH = 2");
            $program_studi_set = $calon_mahasiswa->ID_JALUR == 3 ? $program_studi_table->SelectCriteria("WHERE ID_JENJANG = 1") : $program_studi_table->SelectCriteria("WHERE ID_JENJANG = 5");

            $smarty->assign('program_studi_set', $program_studi_set);
            $smarty->assign('prodi_ipa_set', $prodi_ipa_set);
            $smarty->assign('prodi_ips_set', $prodi_ips_set);
        } else if ($calon_mahasiswa->ID_JALUR == 4) {
            $smarty->assign('pendidikan_ortu', array(
                1 => 'Tidak Tamat SD', 2 => 'SD', 3 => 'SLTP', 4 => 'SLTA', 5 => 'Diploma', 6 => 'S1', 7 => 'S2', 8 => 'S3'
            ));

            //UNTUK TTL
            $negara = $negara_table->SelectCriteria('ORDER BY NM_NEGARA ASC');
            $provinsi_set = $provinsi_table->Select();
            $kota_table->FillProvinsiSet($provinsi_set);

            // PROGRAM STUDI IPA & IPS & all untuk alih jenjang
            $prodi_ipa_set = $program_studi_table->SelectCriteria("WHERE ID_PROGRAM_STUDI IN (145,147,129,6) AND JURUSAN_SEKOLAH = 1");
            $prodi_ips_set = $program_studi_table->SelectCriteria("WHERE ID_PROGRAM_STUDI IN (66,64,142,96) AND JURUSAN_SEKOLAH = 2");
            $program_studi_set = $program_studi_table->SelectCriteria("WHERE ID_PROGRAM_STUDI IN (145,147,129,66,64,142,96,6)");

            // mendapatkan sususan combobox sekolah
            //$sekolah_set = $sekolah_table->SelectCriteria("WHERE ID_KOTA = (SELECT ID_KOTA FROM SEKOLAH WHERE ID_SEKOLAH = {$calon_mahasiswa->ID_SEKOLAH_ASAL}) ORDER BY NM_SEKOLAH ASC");

            $db->Query("SELECT * FROM CALON_MAHASISWA_PASCA WHERE ID_C_MHS = {$calon_mahasiswa->ID_C_MHS}");
            $cmp = $db->FetchAssoc();

            $smarty->assign('cmp', $cmp);
            $smarty->assign('program_studi_set', $program_studi_set);
            $smarty->assign('prodi_ipa_set', $prodi_ipa_set);
            $smarty->assign('prodi_ips_set', $prodi_ips_set);

            $smarty->assign('negara', $negara);
            $smarty->assign('provinsi_set', $provinsi_set);
            $smarty->assign('calon_mahasiswa', $calon_mahasiswa);
        }
    }
    switch ($calon_mahasiswa->ID_JALUR) {
        case 3: {
                $tpl_show = 'edit-ori';
            }break;
        case 5: {
                $tpl_show = 'edit-ori';
            }break;
        case 4: {
                $tpl_show = 'edit-alih';
            }break;
        default : {
                $tpl_show = 'edit-ori';
            }break;
    }
    $smarty->display('ppmb/verifikasi/' . $tpl_show . '.tpl');
} else {
    echo "<h2>Area Panitia PPMB Khusus.</h2>";
};
?>
