<?php
include('../../config.php');

// XML Menu
$xml_menu = new SimpleXMLElement(load_file('menu.xml'));

// User Login 
if ($user->IsLogged())
{
//    $pegawai_table = new PEGAWAI_TABLE($db);
//    $user->ID_UNIT_KERJA = $pegawai_table->SelectWhere("ID_PENGGUNA = {$user->ID_PENGGUNA}")->Get(0)->ID_UNIT_KERJA;   
    $db->Query("SELECT ID_UNIT_KERJA FROM PEGAWAI WHERE ID_PENGGUNA = {$user->ID_PENGGUNA}");
    $row = $db->FetchAssoc();
    $user->ID_UNIT_KERJA = $row['ID_UNIT_KERJA'];
}

function date_to_timestamp($date)
{
    $tgl_lahir_parse = date_parse($date);
    if ($tgl_lahir_parse['year'] > date('Y')) $tgl_lahir_parse['year'] -= 100;
    return mktime(0, 0, 0, $tgl_lahir_parse['month'], $tgl_lahir_parse['day'], $tgl_lahir_parse['year']);
}
?>
