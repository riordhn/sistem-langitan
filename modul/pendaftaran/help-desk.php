<?php
include('../../config.php');

$mhs_found = false;

if ($request_method == 'POST')
{
    if (post('mode') == 'set_fingerprint')
    {
        $return = $db->Query("UPDATE FINGERPRINT_MAHASISWA SET FINGER_DATA = NULL WHERE NIM_MHS = '{$_POST['nim_mhs']}'");
        if ($return)
        {
            echo "OK";
            exit();
        }
    }
    else if (post('mode') == 'set_cekal')
    {
        $return = $db->Query("UPDATE MAHASISWA SET STATUS_CEKAL = {$_POST['status_cekal']} WHERE NIM_MHS = '{$_POST['nim_mhs']}'");
        if ($return)
        {
            echo "OK";
            exit();
        }
    }
}

if ($request_method == 'GET')
{
    if (get('mode') == 'search')
    {
        $db->Query("
            SELECT M.NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, THN_ANGKATAN_MHS, STATUS_CEKAL, FM.FINGER_DATA FROM MAHASISWA M
            LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            LEFT JOIN FINGERPRINT_MAHASISWA FM ON FM.NIM_MHS = M.NIM_MHS
            WHERE M.NIM_MHS = '{$_GET['nim_mhs']}'");
        $mahasiswa = $db->FetchAssoc();
        
        if ($mahasiswa)
        {
            $mhs_found = true;
            
            $smarty->assign('status_cekal', array('0' => 'TIDAK DICEKAL', '1' => 'CEKAL'));
            $smarty->assign('mahasiswa', $mahasiswa);
        }
    }
}

$smarty->assign('mhs_found', $mhs_found);
$smarty->display('helpdesk/view.tpl');
?>