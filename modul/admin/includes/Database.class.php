<?php
/**
 * Class untuk menangani segala sesuatu yang mengenai database
 *
 * @author Fathoni
 * @copyright AUCC 2011
 */
class Database
{
    private $connection;
    private $statement;
    private $is_transaction = false;
    private $p_query = "";
      
    /**
     * Perintah untuk menjalankan koneksi ke database oracle
     */
    public function connect()
    {
        global $db_server, $db_port, $db_name, $db_user, $db_password;
        
        // Connection Settings
        $oci_tnsc = "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST={$db_server})(PORT={$db_port}))(CONNECT_DATA=(SERVER = DEDICATED)(SERVICE_NAME = {$db_name})))";
        $this->connection = oci_connect($db_user, $db_password, $oci_tnsc);
        unset($db_password);
    }
    
    public function __set($name, $value)
    {
        if ($name == 'query')
        {
            if (!is_null($this->statement)) oci_free_statement($this->statement);
            $this->statement = oci_parse($this->connection, $value);
            $this->p_query = $value;
        }
    }
    
    public function __get($name)
    {
        if ($name == 'query') return $this->p_query;
    }
    
    /**
     * Executes a statement
     * @param statement resource
     * @param mode int[optional]
     * @return bool
     */
    public function execute()
    {
        return oci_execute($this->statement);
    }
    
    /**
     * Returns the next row from the result data as an associative array
     * @param statement resource
     * @return array an associative array, or false if there are no more rows in the
     */
    public function fetch()
    {
        if (!is_null($this->statement))
        {
            return oci_fetch_assoc($this->statement);
        }
        else
        {
            return null;
        }
    }
    
    public function fetch_table($table, $start_page = null, $row_per_page = null)
    {
        $this->query = "SELECT * FROM $table";
        $this->execute();
        
        if (!is_null($this->statement))
        {
            oci_fetch_all($this->statement, $output, $start_page * $row_per_page, $row_per_page, OCI_FETCHSTATEMENT_BY_ROW);
            return $output;
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Mendapatkan satu nilai dari execute
     * @return object Nilai yg dikembalikan
     */
    public function single()
    {
        if (!is_null($this->statement))
        { 
            $row = oci_fetch_row($this->statement);
            return $row[0];
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Mendapatkan jumlah row yang terkena query
     */
    public function rows_affected()
    {
        if (!is_null($this->statement))
        {
            return oci_num_rows($this->statement);
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Menutup koneksi database
     * @return bool
     */
    public function close()
    {
        if (!is_null($this->statement))
        {
            oci_free_statement($this->statement);
        }
        
        if (!is_null($this->connection))
        {
            return oci_close($this->connection);
        }
    }
}
?>
