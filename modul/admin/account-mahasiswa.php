<?php
include('config.php');

if (!($user->IsLogged() && $user->Role() == AUCC_ROLE_ADMINISTRATOR)) { echo "Access Denied"; exit(); }

$mode = get('mode', 'view');

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $id_program_studi = get('id_program_studi', '');
        $thn_angkatan_mhs = get('thn_angkatan_mhs', '');
        
        $smarty->assign('program_studi_set', $db->QueryToArray("
            select ps.id_program_studi, j.nm_jenjang, ps.nm_program_studi from program_studi ps
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where ps.id_fakultas = {$user->ID_FAKULTAS} and status_aktif_prodi = 1
            order by nm_jenjang, nm_program_studi"));
        
        if ($id_program_studi != '' && $thn_angkatan_mhs != '')
        {
            $smarty->assign('mahasiswa_set', $db->QueryToArray("
                select p.id_pengguna, p.nm_pengguna, m.nim_mhs, sp.nm_status_pengguna from mahasiswa m
                join pengguna p on p.id_pengguna = m.id_pengguna
                left join status_pengguna sp on sp.id_status_pengguna = m.status_akademik_mhs
                where m.id_program_studi = {$id_program_studi} and m.thn_angkatan_mhs = {$thn_angkatan_mhs}
                order by sp.nm_status_pengguna, m.nim_mhs"));
        }
    }
}

$smarty->display("account/mahasiswa/{$mode}.tpl");
?>
