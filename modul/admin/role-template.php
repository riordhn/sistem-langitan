<?php
include('config.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
        $db->Parse("insert into template_role (id_fakultas, id_role, nm_template, deskripsi) values ({$user->ID_FAKULTAS}, :id_role, :nm_template, :deskripsi)");
        $db->BindByName(':id_role', post('id_role'));
        $db->BindByName(':nm_template', post('nm_template'));
        $db->BindByName(':deskripsi', post('deskripsi'));
        $result = $db->Execute();
        
        if ($result)
            $smarty->assign('added', "Data berhasil di tambahkan");
        else
            $smarty->assign('added', "Data gagal di tambahkan");
    }
    
    if (post('mode') == 'edit')
    {
        $id_template_role = post('id_template_role');
        
        $db->Parse("update template_role set nm_template = :nm_template, deskripsi = :deskripsi where id_template_role = {$id_template_role}");
        $db->BindByName(':nm_template', post('nm_template'));
        $db->BindByName(':deskripsi', post('deskripsi'));
        $result = $db->Execute();
        
        if ($result)
            $smarty->assign('edited', "Data berhasil di edit");
        else
            $smarty->assign('edited', "Data gagal di edit");
    }
    
    if (post('mode') == 'delete')
    {
        $id_template_role = post('id_template_role');
        
        $db->Query("delete from template_role where id_template_role = {$id_template_role}");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $template_set = $db->QueryToArray("
            select tr.*, r.nm_role from template_role tr
            join role r on r.id_role = tr.id_role
            where id_fakultas = {$user->ID_FAKULTAS}");
        $smarty->assign('template_set', $template_set);
    }
    
    if ($mode == 'add')
    {
        $smarty->assign('role_set', $db->QueryToArray("select * from role where id_role in (2, 3, 4, 5, 7, 12, 19)"));
    }
    
    if ($mode == 'edit')
    {
        $id_template_role = get('id_template_role');
        
        $smarty->assign('role_set', $db->QueryToArray("select * from role where id_role in (2, 3, 4, 5, 7, 12, 19)"));
        
        $template_set = $db->QueryToArray("
            select tr.*, r.nm_role from template_role tr
            join role r on r.id_role = tr.id_role
            where id_template_role = {$id_template_role}");
        $smarty->assign('template', $template_set[0]);
    }
}

$smarty->display("role/template/{$mode}.tpl");
?>