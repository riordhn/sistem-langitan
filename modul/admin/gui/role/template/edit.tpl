<div class="center_title_bar">Template Role - Edit</div>

{if $edited}<h2>{$edited}</h2>{/if}

<form action="role-template.php?{$smarty.server.QUERY_STRING}" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_template_role" value="{$template.ID_TEMPLATE_ROLE}" />
<table>
    <tr>
        <th colspan="2">Detail Template Role</th>
    </tr>
    <tr>
        <td>Role</td>
        <td>{$template.NM_ROLE}</td>
    </tr>
    <tr>
        <td>Nama Template</td>
        <td><input type="text" name="nm_template" maxlength="50" size="30" value="{$template.NM_TEMPLATE}"></td>
    </tr>
    <tr>
        <td>Deskripsi</td>
        <td><textarea name="deskripsi" cols="30" rows="2">{$template.DESKRIPSI}</textarea></td>
    </tr>
    <tr>
        <td colspan="2">
            <a href="role-template.php">Kembali</a>
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>