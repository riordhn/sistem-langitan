<div class="center_title_bar">Template Role - Tambah</div>

<form action="role-template.php" method="post">
    <input type="hidden" name="mode" value="add" />
<table>
    <tr>
        <th colspan="2">Detail Template Role</th>
    </tr>
    <tr>
        <td>Role</td>
        <td><select name="id_role">
                <option value="">--</option>
            {foreach $role_set as $r}
                <option value="{$r.ID_ROLE}">{$r.NM_ROLE}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Nama Template</td>
        <td><input type="text" name="nm_template" maxlength="50" size="30"></td>
    </tr>
    <tr>
        <td>Deskripsi</td>
        <td><textarea name="deskripsi" cols="30" rows="2"></textarea></td>
    </tr>
    <tr>
        <td colspan="2">
            <a href="role-template.php">Kembali</a>
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>