<div class="center_title_bar">Template Role</div>

{if $added}<h2>{$added}</h2>{/if}

<table>
    <tr>
        <th>No</th>
        <th>Role</th>
        <th>Nama Template</th>
        <th>Deskripsi</th>
        <th></th>
    </tr>
    {foreach $template_set as $t}
    <tr id="r{$t.ID_TEMPLATE_ROLE}">
        <td class="center">{$t@index + 1}</td>
        <td>{$t.NM_ROLE}</td>
        <td>{$t.NM_TEMPLATE}</td>
        <td>{$t.DESKRIPSI}</td>
        <td>
            <a href="role-template.php?mode=edit&id_template_role={$t.ID_TEMPLATE_ROLE}">Edit</a> |
            <a class="disable-ajax" onclick="javascript:deleteTemplateRole({$t.ID_TEMPLATE_ROLE});">Hapus</a> |
            <a href="role-modul.php?id_template_role={$t.ID_TEMPLATE_ROLE}">Modul</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="5" class="center">
            <a href="role-template.php?mode=add">Tambah</a>
        </td>
    </tr>
</table>
{literal}    
<script type="text/javascript">
    function deleteTemplateRole(id_template_role) {
        if (confirm('Apakah data akan di hapus ?'))
        {
            postPage('role-template.php','mode=delete&id_template_role=' + id_template_role);
        }
    }
</script>
{/literal}