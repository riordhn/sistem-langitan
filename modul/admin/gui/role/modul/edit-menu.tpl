<div class="center_title_bar">Modul {$modul.NM_TEMPLATE} - Edit Menu {$modul.TITLE}</div>

<h2>{$modul.TITLE}</h2>

<table>
    <tr>
        <th>No</th>
        <th>Menu</th>
        <th>Akses</th>
    </tr>
    {foreach $menu_set as $m}
    <tr {if $m@index is div by 2}class="row1"{/if}>
        <td class="center">{$m@index + 1}</td>
        <td>{$m.TITLE}</td>
        <td><select name="akses" id_tm={$m.ID_TEMPLATE_MENU}>
                <option value="0" {if $m.AKSES == 0}selected="selected"{/if}>Tidak Aktif</option>
                <option value="1" {if $m.AKSES == 1}selected="selected"{/if}>Aktif</option>
            </select>
        </td>
    </tr>
    {/foreach}
</table>
{literal}
<script type="text/javascript">
    $('select[name="akses"]').change(function() {
        var id_tm = this.getAttribute('id_tm');
        var akses = this.value;
        $.ajax({
            type: 'POST', url: 'role-modul.php', data: 'mode=edit-akses-menu&id_template_menu='+this.getAttribute('id_tm')+'&akses='+this.value
        });
    });
</script>
{/literal}

<a href="role-modul.php?id_template_role={$modul.ID_TEMPLATE_ROLE}">Kembali</a>