<div class="center_title_bar">Template Modul</div>

<form action="role-modul.php" method="get">
<table>
    <tr>
        <td>Template</td>
        <td>
            <select name="id_template_role">
                <option value="">--</option>
            {foreach $template_set as $t}
                <option value="{$t.ID_TEMPLATE_ROLE}" {if $t.ID_TEMPLATE_ROLE == $smarty.get.id_template_role}selected="selected"{/if}>[{$t.NM_ROLE}] {$t.NM_TEMPLATE}</option>
            {/foreach}
            </select>
            <input type="submit" value="Lihat" />
        </td>
    </tr>
</table>
</form>
            
{if $smarty.get.id_template_role != ''}
<table>
    <tr>
        <th>No</th>
        <th>Modul</th>
        <th>Akses</th>
        <th></th>
    </tr>
    {foreach $modul_set as $m}
    <tr {if $m@index is div by 2}class="row1"{/if}>
        <td class="center">{$m@index + 1}</td>
        <td>{$m.TITLE}</td>
        <td><select name="akses" id_tm={$m.ID_TEMPLATE_MODUL}>
                <option value="0" {if $m.AKSES == 0}selected="selected"{/if}>Tidak Aktif</option>
                <option value="1" {if $m.AKSES == 1}selected="selected"{/if}>Aktif</option>
            </select>
        </td>
        <td>
            <a href="role-modul.php?mode=edit-menu&id_template_modul={$m.ID_TEMPLATE_MODUL}">Edit Menu</a> |
            <a class="disable-ajax" onclick="deleteTemplateModul({$m.ID_TEMPLATE_MODUL}, {$smarty.get.id_template_role});">Hapus</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="4" class="center">
            <a href="role-modul.php?mode=add&id_template_role={$smarty.get.id_template_role}">Tambah</a>
        </td>
    </tr>
</table>
{literal}
<script type="text/javascript">
    $('select[name="akses"]').change(function() {
        var id_tm = this.getAttribute('id_tm');
        var akses = this.value;
        $.ajax({
            url: 'role-modul.php', type: 'POST', data: 'mode=edit-akses-modul&id_template_modul='+id_tm+'&akses='+akses
        });
    });
    
    function deleteTemplateModul(id_template_modul, id_template_role) {
        if (confirm('Apakah data akan di hapus ?'))
        {
            postPage('role-modul.php?id_template_role='+id_template_role,'mode=delete-modul&id_template_modul='+id_template_modul);
        }
    }
</script>
{/literal}
{/if}