<div class="center_title_bar">Modul - Tambah</div>

<h3>{$template.NM_TEMPLATE}</h3>
<p>{$template.DESKRIPSI}</p>

<form action="role-modul.php?id_template_role={$template.ID_TEMPLATE_ROLE}" method="post">
<input type="hidden" name="mode" value="add-modul" />
<input type="hidden" name="id_template_role" value="{$template.ID_TEMPLATE_ROLE}" />
<table>
    <tr>
        <th>No</th>
        <th>Modul</th>
        <th>Default Akses</th>
        <th></th>
    </tr>
    {foreach $modul_set as $m}
    <tr>
        <td class="center">{$m@index + 1}</td>
        <td>{$m.TITLE}</td>
        <td class="center">{if $m.AKSES == 1}Aktif{else}Tidak Aktif{/if}</td>
        <td>
            <input type="checkbox" name="id_modul_set[]" value="{$m.ID_MODUL}" />
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="4" class="center">
            <a href="role-modul.php?id_template_role={$template.ID_TEMPLATE_ROLE}">Kembali</a>
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>