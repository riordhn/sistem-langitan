<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Administrator - {$nama_pt}</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link rel="stylesheet" type="text/css" href="../../css/reset.css" />
		<link rel="stylesheet" type="text/css" href="../../css/text.css" />
		<link rel="stylesheet" type="text/css" href="../../css/admin_style.css" />
		<link rel="stylesheet" type="text/css" href="../../css/admin_content.css" />
		<link rel="stylesheet" type="text/css" href="../../css/jquery-ui-1.8.11.custom.css"  />
		<link rel="stylesheet" type="text/css" href="../../js/dependencies/screen.css" />
		
		<link rel="shorcut icon" href="../../img/icon.ico" />
	</head>

	<body>
		<div id="main_container">
			<div id="header" style="background-image: url('../../img/header/admin-{$nama_singkat}.png')">
				<div id="judul_header"></div>
			</div>

			<div id="main_content">
				<div id="menu_tab">
					<div class="left_menu_corner"></div>
					<ul class="menu">
						{foreach $modul_set as $m}
							{if $m.AKSES == 1}
								<li><a href="#{$m.NM_MODUL}!{$m.PAGE}" class="nav">{$m.TITLE}</a></li>
								<li class="divider"></li>
							{/if}
						{/foreach}
						<li><a href="../../logout.php" class="disable-ajax">Logout</a></li>
					</ul>
					<div class="right_menu_corner"></div>
				</div>

				<div class="crumb_navigation" id="breadcrumbs">
					<!-- Handle by AJAX -->
				</div> 
			</div>

			<div class="left_content">
				<div class="border_box">
					<img src="../../img/admin/Account.png" />
					<br />
					<br />
					<div id="menu">
						<!-- Handle by jQuery -->
					</div>
					<div></div>
					<br/>
					<br/>
				</div>
			</div>

			<div class="content" id="content">
				<!-- Handle by jQuery -->
			</div>

			<div class="footer">
				
				<div class="left_footer">
					<img src="../../img/admin/footer_logo.jpg" />
				</div>

				<div class="center_footer">Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU</div>

				<div class="right_footer"></div>   

			</div>  
		</div>

		{* <script type="text/javascript" src="../../js/?f=jquery-1.5.1.js"></script> *}
		<script
			  src="https://code.jquery.com/jquery-1.5.1.min.js"
			  integrity="sha256-dkuenzrThqqlzerpNoNTmU3mHAvt4IfI9+NXnLRD3js="
			  crossorigin="anonymous"></script>
		
        <script type="text/javascript" src="../../js/cupertino/js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.elastic.source.js"  charset="utf-8"></script>
        <script type="text/javascript" src="../../js/jquery.shiftenter.js"></script>

        <script type="text/javascript">var defaultRel = 'account'; var defaultPage = 'account.php'; var id_fakultas = null;</script>
        <script type="text/javascript" src="../../js/cybercampus.ajax-1.0.js"></script>
	</body>
</html>
