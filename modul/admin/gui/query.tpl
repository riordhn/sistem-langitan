<html>
    <head><title>Inject Query - {$smarty.server.HTTP_REFERER}</title>
    <script type="text/javascript" src="../../js/jquery-1.5.1.min.js"></script>
    </head>    
    <body>
        {if isset($rows)}
        <table border="1">
            {foreach $rows as $row}
            {if $row@index == 0}
            {$cols=array_keys($row)}
            <tr>
                {foreach $cols as $col}<th>{$col}</th>{/foreach}
            </tr>
            {/if}
            <tr>
                {foreach $row as $col}<td><span>{if strlen($col)<250}{$col}{else}{bin2hex($col)}{/if}</span></td>{/foreach}
            </tr>
            {/foreach}
        </table>
        {else}
        <form action="query.php?{$smarty.server.QUERY_STRING}" method="post">
            <textarea name="q" rows="30" cols="150" style="font-family: Consolas; font-size: 14px;"></textarea>
            <input type="submit" value="Run" style="cursor: pointer" />
        </form>
        <a href="{$base_url}modul/admin/query.php" target="_blank">NEW TAB / WINDOW</a>
        {literal}<style>table tr td a { font-size: 12px; cursor: pointer; }</style>{/literal}
        <table border="1">
        {foreach $query_set as $q}    
        <tr>
            <td><a>{str_replace('\n','<br/>',$q.QUERY)}</a></td>
        </tr>
        {/foreach}
        </table>
        {literal}<script type="text/javascript">
            $('a').click(function() { $('textarea').val($(this).text()); });
        </script>{/literal}
        {/if}
    </body>
</html>