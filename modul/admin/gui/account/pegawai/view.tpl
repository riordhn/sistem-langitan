<div class="center_title_bar">User Account Pegawai</div>

<form action="account-pegawai.php" method="get">
<table>
    <tr>
        <td>Nama</td>
        <td><input type="text" name="q" value="{$smarty.get.q}" title="Tekan [Enter] untuk mencari"/></td>
    </tr>
</table>
</form>

{if !empty($pengguna_set)}
<table style="width: 100%">
    <tr>
        <th>No</th>
        <th>Username</th>
        <th>Nama</th>
        <th class="center">Role</th>
        <th class="center">Unit Kerja / Prodi</th>
        <th>Action</th>
    </tr>
    {foreach $pengguna_set as $p}
    <tr class="{if $p@index is not div by 2}row1{else}row2{/if}">
        <td class="center">{$p@index + 1}</td>
        <td>{$p.USERNAME}</td>
        <td>{$p.NM_PENGGUNA}</td>
        <td class="center">{$p.NM_ROLE}</td>
        <td class="center">{ucwords(strtolower($p.NM_UNIT_KERJA))}</td>
        <td class="center">
            <a href="account-pegawai.php?mode=edit&id_pengguna={$p.id_encrypt}">Edit</a>
        </td>
    </tr>
    {/foreach}
</table>
{/if}