<div class="center_title_bar">Account - Edit Role</div>

<form action="account-pegawai.php?mode=edit&id_pengguna={$rp.ID_PENGGUNA}" method="post">
<input type="hidden" name="mode" value="edit-role" />
<input type="hidden" name="id_role_pengguna" value="{$smarty.get.id_role_pengguna}" />
<table>
    <tr>
        <th colspan="2">Detail Role</th>
    </tr>
    <tr>
        <td>Nama Pengguna</td>
        <td>{$rp.NM_PENGGUNA}</td>
    </tr>
    <tr>
        <td>Role</td>
        <td>{$rp.NM_ROLE}</td>
    </tr>
    <tr>
        <td>Template</td>
        <td>
            <select name="id_template_role">
                <option value="">Default</option>
            {foreach $template_role_set as $tr}
                <option value="{$tr.ID_TEMPLATE_ROLE}" {if $tr.ID_TEMPLATE_ROLE == $rp.ID_TEMPLATE_ROLE}selected="selected"{/if}>{$tr.NM_TEMPLATE}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <a href="account-pegawai.php?mode=edit&id_pengguna={$rp.ID_PENGGUNA}">Kembali</a>
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>