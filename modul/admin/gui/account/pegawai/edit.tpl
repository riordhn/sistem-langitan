<div class="center_title_bar">User Account - Edit : {$pengguna.USERNAME}</div>

{if $edited}
    <script type="text/javascript">alert('{$edited}');</script>
{/if}

<form action="account-pegawai.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_pengguna" value="{$smarty.get.id_pengguna}" />
<input type="hidden" name="join_table" value="{$pengguna.JOIN_TABLE}" />
<table>
    <tr>
        <th colspan="2">Detail Account</th>
    </tr>
    <tr>
        <td>Username</td>
        <td>{$pengguna.USERNAME}</td>
    </tr>
   
    <tr>
        <td>Nama</td>
        <td>{$pengguna.NM_PENGGUNA}</td>
    </tr>
    <tr>
        <td>Tipe Account</td>
        <td>{if $pengguna.JOIN_TABLE == 1}Pegawai{else if $pengguna.JOIN_TABLE == 2}Dosen{/if}
        </td>
    </tr>
    <tr>
        <td>Role Aktif</td>
        <td>
            <select name="id_role">
            {foreach $role_pengguna_set as $rp}
                <option value="{$rp.ID_ROLE}" {if $rp.ID_ROLE == $pengguna.ID_ROLE}selected="selected"{/if}>{$rp.NM_ROLE}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    {if $pengguna.JOIN_TABLE == 1}
    <tr>
        <td>Unit Kerja</td>
        <td>
            <select name="id_unit_kerja">
            {foreach $unit_kerja_set as $uk}
                <option value="{$uk.ID_UNIT_KERJA}" {if $pengguna.ID_UNIT_KERJA == $uk.ID_UNIT_KERJA}selected="selected"{/if}>{$uk.NM_UNIT_KERJA}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    {else if $pengguna.JOIN_TABLE == 2}
    <tr>
        <td>Program Studi</td>
        <td>
            <select name="id_program_studi">
            {foreach $program_studi_set as $ps}
                <option value="{$ps.ID_PROGRAM_STUDI}" {if $pengguna.ID_PROGRAM_STUDI == $uk.ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps.NM_PROGRAM_STUDI}</option>
            {/foreach}
            </select>
        </td>
    </tr>    
    {/if}
	<tr>
        <td>Status Login</td>
        <td>{if $pengguna.ID_SESSION}Online{else}Offline{/if}</td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>
    
<script type="text/javascript">
    function deleteRolePengguna(id_role_pengguna)
    {
        if (confirm('Apakah role ini akan di hapus') == true)
        {
            $.ajax({
                type: 'POST',
                url: 'account-pegawai.php',
                data: 'mode=delete-role&id_role_pengguna='+id_role_pengguna,
                success: function(data) {
                    if (data == '1') {
                        $('#r'+id_role_pengguna).remove();
                    }
                    else {
                        alert('Gagal dihapus');
                    }  
                }
            });
        }
    }
</script>
    
<table>
    <caption>Multirole</caption>
    <tr>
        <th>Aktif</th>
        <th>Role</th>
        <th>Template</th>
        <th>Aksi</th>
    </tr>
    {foreach $role_pengguna_set as $rp}
    <tr id="r{$rp.ID_ROLE_PENGGUNA}">
        <td class="center"><input type="radio" name="id_role" value="{$rp.ID_ROLE}" {if $rp.ID_ROLE == $pengguna.ID_ROLE}checked="checked"{/if} disabled="disabled"/></td>
        <td>{$rp.NM_ROLE}</td>
        <td>{if $rp.NM_TEMPLATE != ''}{$rp.NM_TEMPLATE}{else}Default{/if}</td>
        <td>
            <a href="account-pegawai.php?mode=edit-role&id_role_pengguna={$rp.ID_ROLE_PENGGUNA}" title="Edit"><img src="../../img/admin/navigation/edit.png" /></a>
            {if $pengguna.JOIN_TABLE == 1}
                {if $rp.ID_ROLE != $pengguna.ID_ROLE}
                <img src="../../img/admin/navigation/del-red.png" title="Hapus Role" style="cursor: pointer" onclick="deleteRolePengguna('{$rp.ID_ROLE_PENGGUNA}')" />
                {/if}
            {/if}
            {if $pengguna.JOIN_TABLE == 2}
                {if $rp.ID_ROLE != $pengguna.ID_ROLE and $rp.ID_ROLE != 2}
                <img src="../../img/admin/navigation/del-red.png" title="Hapus Role" style="cursor: pointer" onclick="deleteRolePengguna('{$rp.ID_ROLE_PENGGUNA}')" />
                {/if}
            {/if}
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="4" class="center">
            <a href="account-pegawai.php?mode=add-role&id_pengguna={$smarty.get.id_pengguna}">Tambah</a>
        </td>
    </tr>
</table>
    
<a href="account-pegawai.php">Kembali</a>