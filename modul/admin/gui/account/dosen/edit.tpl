<div class="center_title_bar">User Account Dosen - Edit :</div>

<form action="account-dosen.php?{$smarty.server.QUERY_STRING}" method="post">
    <input type="hidden" name="id_pengguna" value="{$smarty.get.id_pengguna}" />
    <input type="hidden" name="mode" value="edit" />
    
    <table>
        <tr>
            <th colspan="2">Detail Dosen</th>
        </tr>
        <tr>
            <td>Username</td>
            <td>{$dosen.USERNAME}</td>
        </tr>
        <tr>
            <td>Password</td>
            <td id="password-field"><input type="button" value="Reset" onclick="resetPassword('{$smarty.get.id_pengguna}');" />
                {literal}
                <script type="text/javascript">
                    function resetPassword(id_pengguna) {
                        if (confirm('Apakah akan di reset ?'))
                        {
                            $.ajax({
                                type: 'POST',
                                url: 'account-dosen.php',
                                data: 'mode=reset-password&id_pengguna=' + id_pengguna,
                                success: function (data) {
                                    if (data == '1')
                                        $('#password-field').text('Password Sudah di reset');
                                    else
                                        $('#password-field').text('Password gagal di reset-'+data);
                                }
                            });
                        }
                    }
                </script>
                {/literal}
            </td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>{$dosen.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td>NIP / NIK</td>
            <td>{$dosen.NIP_DOSEN}</td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>{$dosen.NM_JENJANG} {$dosen.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td colspan="2"><a href="account-dosen.php">Kembali</a></td>
        </tr>
    </table>
</form>