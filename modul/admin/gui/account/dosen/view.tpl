<div class="center_title_bar">User Account Dosen</div>

<form action="account-dosen.php" method="get">
<table>
    <tr>
        <td>Nama</td>
        <td><input type="text" name="q" value="{$smarty.get.q}" /></td>
    </tr>
</table>
</form>

{if !empty($dosen_set)}
<table style="width: 100%">
    <tr>
        <th>No</th>
        <th>NIP / NIK</th>
        <th>Nama</th>
        <th>Program Studi</th>
        <th></th>
    </tr>
    {foreach $dosen_set as $d}
    <tr {if $d@index is div by 2}class="row1"{/if}>
        <td class="center">{$d@index + 1}</td>
        <td>{$d.NIP_DOSEN}</td>
        <td>{$d.NM_PENGGUNA}</td>
        <td>{substr($d.NM_JENJANG, 0, 2)} {$d.NM_PROGRAM_STUDI}</td>
        <td>
            <a href="account-dosen.php?mode=edit&id_pengguna={$d.url}">Edit</a>
        </td>
    </tr>
    {/foreach}
</table>
{/if}