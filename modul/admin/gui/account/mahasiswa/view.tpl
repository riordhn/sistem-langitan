<div class="center_title_bar">User Account Mahasiswa</div>

<form action="account-mahasiswa.php" method="get">
    <table>
        <tr>
            <td>Filter</td>
            <td>
                <select name="id_program_studi">
                    <option value="">-- Program Studi --</option>
                {foreach $program_studi_set as $ps}
                    <option value="{$ps.ID_PROGRAM_STUDI}"
                            {if $ps.ID_PROGRAM_STUDI == $smarty.get.id_program_studi}selected="selected"{/if}>{substr($ps.NM_JENJANG, 0, 2)} {$ps.NM_PROGRAM_STUDI}</option>
                {/foreach}
                </select>
                <select name="thn_angkatan_mhs">
                    <option value="">-- Angkatan --</option>
                {for $i = 2015 to 1989 step -1}
                    <option value="{$i}" {if $i == $smarty.get.thn_angkatan_mhs}selected="selected"{/if}>{$i}</option>
                {/for}
                </select>
                <input type="submit" value="Lihat" />
            </td>
        </tr>
    </table>
</form>

{if !empty($mahasiswa_set)}
<table>
    <tr>
        <th>No</th>
        <th>NIM</th>
        <th>Nama</th>
        <th>Status</th>
    </tr>
    {foreach $mahasiswa_set as $m}
    <tr {if $m@index is div by 2}class="row1"{/if}>
        <td {if $m.NM_STATUS_PENGGUNA != 'Aktif'}style="color: #777"{/if} class="center">{$m@index + 1}</td>
        <td {if $m.NM_STATUS_PENGGUNA != 'Aktif'}style="color: #777"{/if}>{$m.NIM_MHS}</td>
        <td {if $m.NM_STATUS_PENGGUNA != 'Aktif'}style="color: #777"{/if}>{$m.NM_PENGGUNA}</td>
        <td {if $m.NM_STATUS_PENGGUNA != 'Aktif'}style="color: #777"{/if} class="center">{$m.NM_STATUS_PENGGUNA}</td>        
    </tr>
    {/foreach}
</table>
{/if}