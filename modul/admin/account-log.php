<?php
include('config.php');

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_ADMINISTRATOR)
{
    $smarty->display('account-log.tpl');
}
else $smarty->display('session-expired.tpl');
?>
