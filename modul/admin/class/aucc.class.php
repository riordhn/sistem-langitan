<?php
class AUCC_Admin {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    function detail_pembayaran($nim) {
        return $this->db->QueryToArray("
        SELECT * FROM 
            (SELECT M.ID_MHS AS ID1,P.NM_PENGGUNA,NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,M.NIM_MHS,PEM.*,B.NM_BIAYA,J.NM_JENJANG,M.STATUS_CEKAL,JAL.NM_JALUR,S.THN_AKADEMIK_SEMESTER
            ,KB.NM_KELOMPOK_BIAYA,M.NIM_BANK,BNK.NM_BANK,BNK.ID_BANK AS ID_BNK,BNKV.ID_BANK_VIA AS ID_BNKV,BNKV.NAMA_BANK_VIA,S2.NM_SEMESTER,S2.TAHUN_AJARAN
            FROM AUCC.PENGGUNA P
            LEFT JOIN AUCC.MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN AUCC.PROGRAM_STUDI PR ON M.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
            LEFT JOIN AUCC.JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            LEFT JOIN AUCC.JALUR_MAHASISWA JM ON M.ID_MHS = JM.ID_MHS
            LEFT JOIN AUCC.JALUR JAL ON JAL.ID_JALUR = JM.ID_JALUR
            LEFT JOIN AUCC.SEMESTER S ON S.ID_SEMESTER = JM.ID_SEMESTER
            LEFT JOIN AUCC.KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
            LEFT JOIN AUCC.FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS
            LEFT JOIN AUCC.PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
            LEFT JOIN AUCC.SEMESTER S2 ON S2.ID_SEMESTER = PEM.ID_SEMESTER
            LEFT JOIN AUCC.BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            LEFT JOIN AUCC.BANK_VIA BNKV ON BNKV.ID_BANK_VIA = PEM.ID_BANK_VIA
            LEFT JOIN AUCC.DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            LEFT JOIN AUCC.BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
            ) X1
        LEFT JOIN
            (SELECT M.ID_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_SPP,SUM(PEM.DENDA_BIAYA) AS TOTAL_DENDA_BIAYA FROM AUCC.PEMBAYARAN PEM
            LEFT JOIN AUCC.MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
            WHERE PEM.TGL_BAYAR IS NULL
            GROUP BY M.ID_MHS
            ) X2 
        ON X1.ID1 = X2.ID2
        WHERE X1.NIM_MHS ='{$nim}'
        ORDER BY X1.ID_PEMBAYARAN");
    }

    function get_detail_pembayaran_mahasiswa($nim) {
        return $this->db->QueryToArray("SELECT B.NM_BIAYA,P.* FROM AUCC.PEMBAYARAN P
        JOIN AUCC.DETAIL_BIAYA DB ON P.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN AUCC.BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
        JOIN AUCC.MAHASISWA M ON P.ID_MHS = M.ID_MHS
        WHERE NIM_MHS ='{$nim}'
        ");
    }

    function get_detail_pembayaran($id_pembayaran) {
        $this->db->Query("SELECT B.NM_BIAYA,P.* FROM AUCC.PEMBAYARAN P
        JOIN AUCC.DETAIL_BIAYA DB ON P.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN AUCC.BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
        JOIN AUCC.MAHASISWA M ON P.ID_MHS = M.ID_MHS
        WHERE P.ID_PEMBAYARAN ='{$id_pembayaran}'
        ");

        return $this->db->FetchAssoc();
    }

    function cek_data($nim) {
        $this->db->Query("
        SELECT M.NIM_MHS,P.NM_PENGGUNA,J.ID_JENJANG,J.NM_JENJANG,S.ID_SEMESTER,S.THN_AKADEMIK_SEMESTER,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.ID_JALUR,J.NM_JALUR,
        KB.ID_KELOMPOK_BIAYA,KB.NM_KELOMPOK_BIAYA 
            FROM AUCC.PENGGUNA P
            LEFT JOIN AUCC.MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA 
            LEFT JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            LEFT JOIN AUCC.JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
            LEFT JOIN AUCC.KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
            LEFT JOIN AUCC.JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
            LEFT JOIN AUCC.SEMESTER S ON JM.ID_SEMESTER = S.ID_SEMESTER 
            LEFT JOIN AUCC.JALUR J ON JM.ID_JALUR = J.ID_JALUR
        WHERE M.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_pengguna($nim) {
        $this->db->Query("SELECT * FROM PENGGUNA WHERE USERNAME='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_mahasiswa($nim) {
        $this->db->Query("SELECT * FROM MAHASISWA WHERE NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_jalur($nim) {
        $this->db->Query("SELECT * FROM JALUR_MAHASISWA WHERE NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function insert_pengguna_mahasiswa($nama, $nim) {
        $this->db->Query("INSERT INTO PENGGUNA 
                (USERNAME,PASSWORD_PENGGUNA,NM_PENGGUNA,ID_ROLE,JOIN_TABLE) 
                VALUES 
                ('{$nim}','{$nim}','{$nama}',3,3)");
    }

    function insert_mahasiswa($id_pengguna, $nim, $program_studi, $kelompok_biaya) {
        $nim_bank = substr($nim, 0, 9);
        $this->db->Query("INSERT INTO MAHASISWA 
                (ID_PENGGUNA,NIM_MHS,ID_PROGRAM_STUDI,ID_KELOMPOK_BIAYA,NIM_BANK,STATUS_CEKAL) 
                VALUES 
                ('{$id_pengguna}','{$nim}','{$program_studi}','{$kelompok_biaya}','{$nim_bank}','0')");
    }

    function insert_jalur_mahasiswa($id_mhs, $program_studi, $jalur, $semester, $nim_mhs) {
        $this->db->Query("INSERT INTO JALUR_MAHASISWA 
                (ID_MHS,ID_PROGRAM_STUDI,ID_JALUR,ID_SEMESTER,NIM_MHS)
                VALUES
                ('{$id_mhs}','{$program_studi}','{$jalur}','{$semester}','{$nim_mhs}')");
    }

    function update_pengguna($nama, $nim) {
        $this->db->Query("UPDATE PENGGUNA SET NM_PENGGUNA ='{$nama}' WHERE USERNAME='{$nim}'");
    }

    function update_mahasiswa($id_kelompok_biaya, $nim, $id_program_studi) {
        $nim_bank = substr($nim, 0, 9);
        $this->db->Query("UPDATE MAHASISWA SET ID_KELOMPOK_BIAYA='{$id_kelompok_biaya}',NIM_BANK='{$nim_bank}',ID_PROGRAM_STUDI='{$id_program_studi}',STATUS_CEKAL=0 WHERE NIM_MHS='{$nim}'");
    }

    function update_jalur_mahasiswa($id_mhs, $program_studi, $jalur, $semester, $nim) {
        $this->db->Query("UPDATE JALUR_MAHASISWA SET ID_MHS='{$id_mhs}',ID_PROGRAM_STUDI='{$program_studi}',ID_JALUR='{$jalur}',ID_SEMESTER='{$semester}' WHERE NIM_MHS='{$nim}'");
    }

    function get_jenjang() {
        return $this->db->QueryToArray("SELECT * FROM JENJANG ORDER BY NM_JENJANG,ID_JENJANG");
    }

    function get_semester() {
        return $this->db->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER = 'Ganjil' ORDER BY ID_SEMESTER");
    }

    function get_program_studi() {
        return $this->db->QueryToArray("SELECT * FROM PROGRAM_STUDI ORDER BY NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }

    function get_program_studi_by_jenjang($id_jenjang) {
        return $this->db->QueryToArray("SELECT * FROM PROGRAM_STUDI WHERE ID_JENJANG={$id_jenjang} ORDER BY NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }

    function get_jalur() {
        return $this->db->QueryToArray("SELECT * FROM JALUR ORDER BY NM_JALUR,ID_JALUR");
    }

    function get_kelompok() {
        return $this->db->QueryToArray("SELECT * FROM KELOMPOK_BIAYA ORDER BY NM_KELOMPOK_BIAYA,ID_KELOMPOK_BIAYA");
    }

    function get_bank() {
        return $this->db->QueryToArray("SELECT * FROM BANK ORDER BY NM_BANK,ID_BANK");
    }

    function get_via_bank() {
        return $this->db->QueryToArray("SELECT * FROM BANK_VIA ORDER BY NAMA_BANK_VIA,ID_BANK_VIA");
    }

    function get_semester_all() {
        return $this->db->QueryToArray("SELECT * FROM SEMESTER ORDER BY ID_SEMESTER");
    }

    function cek_biaya_kuliah_mhs($nim) {
        $this->db->Query("
          SELECT T.NIM_MHS,T.ID_MHS, BK.ID_BIAYA_KULIAH,BK.BESAR_BIAYA_KULIAH FROM AUCC.BIAYA_KULIAH BK
          RIGHT JOIN (
              SELECT M.NIM_MHS, M.ID_MHS, JM.ID_SEMESTER, M.ID_PROGRAM_STUDI, M.ID_KELOMPOK_BIAYA, JM.ID_JALUR FROM AUCC.MAHASISWA M
              JOIN AUCC.JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
              WHERE JM.ID_SEMESTER IS NOT NULL) T ON T.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
          WHERE
          BK.ID_KELOMPOK_BIAYA = T.ID_KELOMPOK_BIAYA AND BK.ID_SEMESTER = T.ID_SEMESTER AND BK.ID_JALUR = T.ID_JALUR
          AND T.ID_MHS NOT IN (SELECT ID_MHS FROM AUCC.BIAYA_KULIAH_MHS) AND T.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_pembayaran($nim) {
        $this->db->Query("
        SELECT M.NIM_MHS,BKM.ID_MHS, 5 AS ID_SEMESTER, DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, 'Y' AS IS_TAGIH FROM AUCC.BIAYA_KULIAH_MHS BKM
            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
            LEFT JOIN AUCC.MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
            WHERE
	M.ID_MHS IN (SELECT ID_MHS FROM AUCC.BIAYA_KULIAH_MHS) AND
	M.ID_MHS NOT IN (SELECT DISTINCT ID_MHS FROM AUCC.PEMBAYARAN) AND M.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function generate_pembayaran($nim) {
        $this->db->Query("
        INSERT INTO AUCC.PEMBAYARAN (ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, BESAR_BIAYA, IS_TAGIH)
            SELECT BKM.ID_MHS, 5 AS ID_SEMESTER, DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, 'Y' AS IS_TAGIH FROM AUCC.BIAYA_KULIAH_MHS BKM
            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
            LEFT JOIN AUCC.MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
            WHERE
        M.ID_MHS IN (SELECT ID_MHS FROM AUCC.BIAYA_KULIAH_MHS) AND
        M.ID_MHS NOT IN (SELECT DISTINCT ID_MHS FROM AUCC.PEMBAYARAN) AND
        M.NIM_MHS ='{$nim}'");
    }

    function generate_biaya_kuliah($nim) {
        $this->db->Query("
        INSERT INTO AUCC.BIAYA_KULIAH_MHS (ID_MHS, ID_BIAYA_KULIAH)
          SELECT T.ID_MHS, BK.ID_BIAYA_KULIAH FROM AUCC.BIAYA_KULIAH BK
          RIGHT JOIN (
            SELECT M.NIM_MHS, M.ID_MHS, JM.ID_SEMESTER, M.ID_PROGRAM_STUDI, M.ID_KELOMPOK_BIAYA, JM.ID_JALUR FROM AUCC.MAHASISWA M
            JOIN AUCC.JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
            WHERE JM.ID_SEMESTER IS NOT NULL) T ON T.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
          WHERE
            BK.ID_KELOMPOK_BIAYA = T.ID_KELOMPOK_BIAYA AND BK.ID_SEMESTER = T.ID_SEMESTER AND BK.ID_JALUR = T.ID_JALUR
            AND T.ID_MHS NOT IN (SELECT ID_MHS FROM AUCC.BIAYA_KULIAH_MHS) AND
            T.NIM_MHS ='{$nim}'");
    }

}

?>
