<?php
include('config.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'add-modul')
    {
        $id_modul_set = post('id_modul_set');
        $id_template_role = post('id_template_role');
        
        $db->BeginTransaction();
        
        foreach ($id_modul_set as $id_modul)
        {
            // get sequence
            $seq = $db->QueryToArray("select template_modul_seq.nextval as nextval from dual");
            $id_template_modul = $seq[0]['NEXTVAL'];
            
            // insert modul
            $db->Query("insert into template_modul (id_template_modul, id_template_role, id_modul) values ({$id_template_modul}, {$id_template_role}, {$id_modul})");
            
            // insert all menu auto
            $db->Query("insert into template_menu (id_template_modul, id_menu, akses) select {$id_template_modul}, id_menu, akses from menu where id_modul = {$id_modul}");
        }
        
        $db->Commit();
    }
    
    if (post('mode') == 'delete-modul')
    {
        $id_template_modul = post('id_template_modul');
        
        $db->BeginTransaction();
        
        $db->Query("delete from template_menu where id_template_modul = {$id_template_modul}");
        $db->Query("delete from template_modul where id_template_modul = {$id_template_modul}");
        
        $db->Commit();
    }
    
    if (post('mode') == 'edit-akses-modul')
    {
        $id_template_modul = post('id_template_modul', '');
        $akses = post('akses', 0);
        
        if ($id_template_modul != '')
        {
            $db->Query("update template_modul set akses = {$akses} where id_template_modul = {$id_template_modul}");
        }
        
        exit();
    }
    
    if (post('mode') == 'edit-akses-menu')
    {
        $id_template_menu = post('id_template_menu', '');
        $akses = post('akses', 0);
        
        if ($id_template_menu != '')
        {
            $db->Query("update template_menu set akses = {$akses} where id_template_menu = {$id_template_menu}");
        }
        
        exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $id_template_role = get('id_template_role');
        
        $smarty->assign('template_set', $db->QueryToArray("
            select tr.*, r.nm_role from template_role tr join role r on r.id_role = tr.id_role
            where id_fakultas = {$user->ID_FAKULTAS} order by r.id_role"));
        
        if ($id_template_role != '')
        {
            $smarty->assign('modul_set', $db->QueryToArray("
                select tm.id_template_modul, m.title, tm.akses from template_modul tm
                join modul m on m.id_modul = tm.id_modul
                where tm.id_template_role = {$id_template_role}
                order by m.urutan"));
        }
    }
    
    if ($mode == 'add')
    {
        $id_template_role = get('id_template_role');
        
        $template_set = $db->QueryToArray("select id_template_role, nm_template, deskripsi, id_role from template_role where id_template_role = {$id_template_role} and id_fakultas = {$user->ID_FAKULTAS}");
        $smarty->assign('template', $template_set[0]);
        
        $smarty->assign('modul_set', $db->QueryToArray("
            select id_modul, title, akses from modul
            where id_role = {$template_set[0]['ID_ROLE']} and id_modul not in (select id_modul from template_modul where id_template_role = {$id_template_role})
            order by urutan"));
    }
    
    if ($mode == 'edit-menu')
    {
        $id_template_modul = get('id_template_modul');
        
        $modul_set = $db->QueryToArray("
            select tr.nm_template, m.title, tr.id_template_role from template_modul tm
            join template_role tr on tr.id_template_role = tm.id_template_role
            join modul m on m.id_modul = tm.id_modul
            where tm.id_template_modul = {$id_template_modul}");
        $smarty->assign('modul', $modul_set[0]);
        
        $smarty->assign('menu_set', $db->QueryToArray("
            select tm.id_template_menu, m.title, tm.akses from template_menu tm
            join menu m on m.id_menu = tm.id_menu
            where tm.id_template_modul = {$id_template_modul}"));
    }
}

$smarty->display("role/modul/{$mode}.tpl");
?>
