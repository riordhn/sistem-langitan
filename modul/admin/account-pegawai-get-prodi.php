<?php
include('config.php');

$program_studi_set = $db->QueryToArray("
    select ps.id_program_studi, substr(j.nm_jenjang,1,2)||' '||ps.nm_program_studi as nm_program_studi from program_studi ps
    join jenjang j on j.id_jenjang = ps.id_jenjang
    where ps.id_fakultas = {$user->ID_FAKULTAS}");

?>
<?php foreach ($program_studi_set as $ps) { ?>
<option value="<?php echo $ps['ID_PROGRAM_STUDI']; ?>"><?php echo $ps['NM_PROGRAM_STUDI']; ?></option>
<?php } ?>

