<?php

include'config.php';

$mhs_found = false;

if ($request_method == 'POST') {
    if (post('mode') == 'set_fingerprint') {
        $return = $db->Query("
            UPDATE FINGERPRINT_MAHASISWA F SET
                FINGER_DATA = NULL,
                TGL_LAHIR = (SELECT TGL_LAHIR_PENGGUNA FROM PENGGUNA WHERE USERNAME = F.NIM_MHS),
                HURUF_DEPAN = (SELECT SUBSTR(UPPER(NM_PENGGUNA), 1, 1) FROM PENGGUNA WHERE USERNAME = F.NIM_MHS)
            WHERE F.NIM_MHS = '{$_POST['nim_mhs']}'");
    } else if (post('mode') == 'set_cekal') {
        $return = $db->Query("UPDATE MAHASISWA SET STATUS_CEKAL = {$_POST['status_cekal']} WHERE NIM_MHS = '{$_POST['nim_mhs']}'");
        write_log('Status Cekal Telah DI update oleh ID_PENGGUNA = ' . $user->ID_PENGGUNA . ' Menjadi' . $_POST['status_cekal'].' ');
    } else if (post('mode') == 'insert_fp') {
        // cek kondisi data fingerprint
        $db->Query("SELECT NIM_MHS FROM FINGERPRINT_MAHASISWA WHERE NIM_MHS = '{$_POST['nim_mhs']}'");
        $row = $db->FetchAssoc();

        // saat belum ada
        if (!$row) {
            // mendapatkan data yg akan di insert
            $db->Query("
                SELECT USERNAME AS NIM_MHS, SUBSTR(UPPER(NM_PENGGUNA), 1, 1) AS HURUF_DEPAN, TGL_LAHIR_PENGGUNA FROM PENGGUNA P
                JOIN MAHASISWA M ON M.ID_PENGGUNA = P.ID_PENGGUNA
                WHERE M.NIM_MHS = '{$_POST['nim_mhs']}' AND TGL_LAHIR_PENGGUNA IS NOT NULL");
            $row = $db->FetchAssoc();

            // insert data yg valid
            if ($row) {
                $db->Parse("INSERT INTO FINGERPRINT_MAHASISWA (NIM_MHS, TGL_LAHIR, HURUF_DEPAN) VALUES (:nim_mhs, :tgl_lahir, :huruf_depan)");
                $db->BindByName(':nim_mhs', $row['NIM_MHS']);
                $db->BindByName(':tgl_lahir', $row['TGL_LAHIR_PENGGUNA']);
                $db->BindByName(':huruf_depan', $row['HURUF_DEPAN']);
                $db->Execute();
            }
        }
    } else if (post('mode') == 'reset_password') {
        $db->Parse("UPDATE PENGGUNA SET SE1 = :password WHERE USERNAME = :nim_mhs AND ID_ROLE = 3");
        $db->BindByName(':password', $user->Encrypt(post('nim_mhs')));
        $db->BindByName(':nim_mhs', post('nim_mhs'));
        $db->Execute();
    }
}

if (get('mode') == 'search') {
    $db->Query("
        SELECT
            M.NIM_MHS, P.TGL_LAHIR_PENGGUNA, NM_PENGGUNA, PASSWORD_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, THN_ANGKATAN_MHS, STATUS_CEKAL, FM.FINGER_DATA, FM.TGL_LAHIR, FM.HURUF_DEPAN
        FROM MAHASISWA M
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
        LEFT JOIN FINGERPRINT_MAHASISWA FM ON FM.NIM_MHS = M.NIM_MHS
        WHERE M.NIM_MHS = '{$_GET['nim_mhs']}' AND PS.ID_FAKULTAS = {$user->ID_FAKULTAS}");
    $mahasiswa = $db->FetchAssoc();

    if ($mahasiswa) {
        $mhs_found = true;

        $smarty->assign('status_cekal', array('0' => 'TIDAK DICEKAL', '1' => 'CEKAL'));
        $smarty->assign('mahasiswa', $mahasiswa);
    }
}

$smarty->assign('mhs_found', $mhs_found);
$smarty->display('ubah-status.tpl');
?>