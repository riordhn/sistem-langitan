<?php
include('config.php');

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_ADMINISTRATOR)
{

$mode = get('mode');
$search = get('q');

if ($mode == '')
{
    $data_pengguna = array();
    
    $query = "
        SELECT p.ID_PENGGUNA, p.USERNAME, p.NM_PENGGUNA, r.NM_ROLE FROM PENGGUNA p
        LEFT JOIN ROLE_PENGGUNA rp ON rp.ID_PENGGUNA = p.ID_PENGGUNA
        LEFT JOIN ROLE r ON r.ID_ROLE = rp.ID_ROLE ";
    
    if ($search != '')
    {
        $query .= "WHERE
            UPPER(p.USERNAME) LIKE UPPER('%$search%') OR
            UPPER(p.NM_PENGGUNA) LIKE UPPER('%$search%') ";
    }
    else
    {
        $query .= "WHERE p.USERNAME LIKE '0809160%'";
    }
    
    $query .= "ORDER BY p.USERNAME";
    
    $db->Query($query);
    
    while ($row = $db->FetchAssoc())
        array_push($data_pengguna, $row);
    
    $db->Close();
    
    $smarty->assign('data_pengguna', $data_pengguna);
    $smarty->assign('q', $search);
    $smarty->assign('debug', '');
}


    if ($mode == 'edit')
        $smarty->display('account-user-edit.tpl');
    elseif ($mode == 'view')
        $smarty->display('account-user-view.tpl');
    else
        $smarty->display('account-user.tpl');
    
}
?>