<?php
include('config.php');

if (!($user->IsLogged() && $user->Role() == AUCC_ROLE_ADMINISTRATOR)) { echo "Access Denied"; exit(); }

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
	
	
	if (post('mode') == 'edit')
	{
		$id_pengguna = $user->Decrypt(pack("H*", post('id_pengguna')));
		$join_table = post('join_table');
		
		// Update role
		$db->Parse("
			update pengguna set
				id_role = :id_role,
				id_template_role = (select id_template_role from role_pengguna where id_role = :id_role and id_pengguna = :id_pengguna)
			where id_pengguna = :id_pengguna");
		$db->BindByName(':id_role', post('id_role'));
		$db->BindByName(':id_pengguna', $id_pengguna);
		$result = $db->Execute();
		
		// Update Program Studi / Unit Kerja
		if ($join_table == 1) // Pegawai
		{
			$result = $db->Query("update pegawai set id_unit_kerja = ".post('id_unit_kerja')." where id_pengguna = {$id_pengguna}");
		}
		
		if ($join_table == 2) // Dosen
		{
			$result = $db->Query("update dosen set id_program_studi = ".post('id_program_studi')." where id_pengguna = {$id_pengguna}");
		}
		
		$smarty->assign('edited', $result ? "Data berhasil di edit" : "Data gagal diedit");
	}
	
	
	if (post('mode') == 'add-role') // 2014-06-27 mengandung bug bisa nambah role tanpa batas.
	{
		$id_pengguna = $user->Decrypt(pack("H*", post('id_pengguna')));
		$id_roles = post('id_roles');
		
		foreach ($id_roles as $id_role)
		{
			/* WHITE LIST FOR ADMIN FAKULTAS, sumber Fauzi 2014-06-27
			2, ADMIN FAKULTAS
			4, DOSEN
			5, PRODI
			7, AKADEMIK PRODI
			12, SE
			13, PPM
			14, SPM
			19, KEUANGAN FAKULTAS
			22, TENAGA KEPENDIDIKAN
			24, KEPEGAWAIAN FAKULTAS
			27, SARANA PRASARANA
			34, LAKIP
			37, KEMAHASISWAAN FAKULTAS
			45, SEKRETARIAT			*/
			$whiteROLE = array(2,4,5,7,12,13,14,19,22,24,27,34,37,45); // add whitelist 2014-06-27
			if (in_array($id_role, $whiteROLE)) {  // add whitelist 2014-06-27

				$db->Query("select count(*) as cek 
									from aucc.role 
									where id_role not in (select id_role from aucc.role_pengguna where id_pengguna = {$id_pengguna}) 
									and tipe_role = 'F'
									and id_role = '$id_role'");
				$cek_role = $db->FetchArray();
				
				if($cek_role['CEK'] != '0'){					
					$db->Query("insert into role_pengguna (id_pengguna, id_role) values ({$id_pengguna}, $id_role)");
				}

			} else { die();}  // add whitelist 2014-06-27
		}
	}
	
	if (post('mode') == 'edit-role')
	{
		$id_role_pengguna = $user->Decrypt(pack("H*", post('id_role_pengguna')));
		$db->Parse("update role_pengguna set id_template_role = :id_template_role where id_role_pengguna = {$id_role_pengguna}");
		$db->BindByName(':id_template_role', post('id_template_role'));
		$db->Execute();
	}
	
	if (post('mode') == 'delete-role')
	{
		$id_role_pengguna = $user->Decrypt(pack("H*", post('id_role_pengguna')));
		
		$result = $db->Query("delete from role_pengguna where id_role_pengguna = {$id_role_pengguna}");
		
		echo $result ? "1" : "0";
		
		exit();
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		// Mendapatkan variabel
		$q = strtoupper(get('q',''));
		
		$where = ($q == '') ? "" : "and (upper(p.nm_pengguna) like '%{$q}%' or p.username like '%{$q}%')";
		
		// Mendapatkan dari db
		$pengguna_set = $db->QueryToArray("
			select p.id_pengguna, p.username, p.nm_pengguna, r.nm_role, dp.nm_unit_kerja from pengguna p
			join aucc.role r on r.id_role = p.id_role
			join (
				select d.id_pengguna, substr(j.nm_jenjang,1,2)||' '||ps.nm_program_studi as nm_unit_kerja from dosen d
				join program_studi ps on ps.id_program_studi = d.id_program_studi_sd
				join jenjang j on j.id_jenjang = ps.id_jenjang
				where ps.id_fakultas = {$user->ID_FAKULTAS}
				union all
				select pg.id_pengguna, uk.nm_unit_kerja from pegawai pg
				join unit_kerja uk on uk.id_unit_kerja = pg.id_unit_kerja_sd
				where uk.id_fakultas = {$user->ID_FAKULTAS}) dp on dp.id_pengguna = p.id_pengguna
			where r.tipe_role = 'F' {$where}
			order by r.id_role, p.nm_pengguna");
		
		foreach ($pengguna_set as &$p)
			$p['id_encrypt'] = bin2hex($user->Encrypt($p['ID_PENGGUNA']));

		$smarty->assign('pengguna_set', $pengguna_set);
	}
	
	if ($mode == 'edit')
	{   
		$id_pengguna = $user->Decrypt(pack("H*", get('id_pengguna')));

		// Mendapatkan Detail pengguna
		$db->Query("
			select p.id_pengguna, username, nm_pengguna, nip_pegawai, p.id_role, p.id_template_role, peg.id_unit_kerja, peg.id_jabatan_pegawai, p.join_table, d.id_program_studi
			from pengguna p
			left join pegawai peg on peg.id_pengguna = p.id_pengguna
			left join dosen d on d.id_pengguna = p.id_pengguna
			where p.id_pengguna = {$id_pengguna}");
		$pengguna = $db->FetchAssoc();
		
		$smarty->assign('pengguna', $pengguna);

		// Menampilkan role yang bisa di ganti / multi role
		$role_pengguna_set = $db->QueryToArray("
			select rp.id_role, rp.id_role_pengguna, r.nm_role, tr.nm_template from role_pengguna rp
			join role r on r.id_role = rp.id_role
			left join template_role tr on tr.id_template_role = rp.id_template_role
			where rp.id_pengguna = {$id_pengguna}");
		
		// encrypt
		foreach ($role_pengguna_set as &$rp)
			$rp['ID_ROLE_PENGGUNA'] = bin2hex($user->Encrypt($rp['ID_ROLE_PENGGUNA']));
		
		$smarty->assign('role_pengguna_set', $role_pengguna_set);
		
		// Mengambil data prodi dari fakultas terpilih (akun dosen)
		$smarty->assign('program_studi_set', $db->QueryToArray("
			select ps.id_program_studi, substr(j.nm_jenjang,1,2)||' '||ps.nm_program_studi as nm_program_studi from program_studi ps
			join jenjang j on j.id_jenjang = ps.id_jenjang
			where ps.id_fakultas = {$user->ID_FAKULTAS}
			order by j.id_jenjang"));
		
		// Mengambil data unit kerja dari fakultas terpilih (akun pegawai)
		$smarty->assign('unit_kerja_set', $db->QueryToArray("
			select uk.id_unit_kerja, uk.nm_unit_kerja from unit_kerja uk
			where uk.id_fakultas = {$user->ID_FAKULTAS}
			order by nm_unit_kerja"));
	}
	
	if ($mode == 'add-role')
	{
		$id_pengguna = $user->Decrypt(pack("H*", get('id_pengguna')));
		
		$pengguna = $db->QueryToArray("select p.id_pengguna, p.nm_pengguna from pengguna p where p.id_pengguna = {$id_pengguna}");
		$pengguna[0]['ID_PENGGUNA'] = bin2hex($user->Encrypt($pengguna[0]['ID_PENGGUNA']));
		$smarty->assign('pengguna', $pengguna[0]);
		
		$role_set = $db->QueryToArray("select * from aucc.role where id_role not in (select id_role from aucc.role_pengguna where id_pengguna = {$id_pengguna}) and tipe_role = 'F'");
		$smarty->assign('role_set', $role_set);
	}
	
	if ($mode == 'edit-role')
	{
		$id_role_pengguna = $user->Decrypt(pack("H*", get('id_role_pengguna')));
		
		$rp = $db->QueryToArray("
			select rp.id_role_pengguna, rp.id_template_role, rp.id_pengguna, p.nm_pengguna, r.nm_role from role_pengguna rp
			join pengguna p on p.id_pengguna = rp.id_pengguna
			join role r on r.id_role = rp.id_role
			where rp.id_role_pengguna = {$id_role_pengguna}");
		$rp[0]['ID_PENGGUNA'] = bin2hex($user->Encrypt($rp[0]['ID_PENGGUNA']));
		$smarty->assign('rp', $rp[0]);
		
		$smarty->assign('template_role_set', $db->QueryToArray("
			select t.id_template_role, t.nm_template from template_role t
			where t.id_fakultas = {$user->ID_FAKULTAS}"));
	}
	
	$smarty->display("account/pegawai/{$mode}.tpl");
}
?>