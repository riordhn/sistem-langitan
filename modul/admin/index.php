<?php
include('config.php');

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_ADMINISTRATOR)
{
	// force password to ISO27001:2009
	if($_SESSION['standar_iso']=='no'){
        header("Location: ../../login.php?mode=ltp-null");
        exit();
    }
	
	// data menu dari $user
    $smarty->assign('modul_set', $user->MODULs);
    
    $smarty->assign('user_session', print_r($user, true));

    $smarty->display("index.tpl");
}
else 
{
    header("Location: ".$base_url." ");
	exit();
}
?>
