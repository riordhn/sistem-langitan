<?php
include('config.php');

if (!($user->IsLogged() && $user->Role() == AUCC_ROLE_ADMINISTRATOR)) { exit(); }

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'reset-password')
    {
        $id_pengguna = $user->Decrypt(pack("H*", post('id_pengguna')));
        
        if (is_numeric($id_pengguna))
        {
            $db->Query("select username from pengguna where id_pengguna = {$id_pengguna}");
            $row = $db->FetchAssoc();
            
            //$result = $db->Query("update pengguna set se1 = '".$user->Encrypt($row['USERNAME'])."' where id_pengguna = {$id_pengguna}");
            
            if ($result)
                echo "1";
            else
                echo "0";
        }
        
        exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $q = strtoupper(get('q', ''));
        
        $where = ($q == '') ? "" : "and upper(p.nm_pengguna) like '%{$q}%'";
        
        $dosen_set = $db->QueryToArray("
            select p.id_pengguna, p.nm_pengguna, d.nip_dosen, j.nm_jenjang, ps.nm_program_studi from dosen d
            join pengguna p on p.id_pengguna = d.id_pengguna
            join program_studi ps on ps.id_program_studi = d.id_program_studi
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where ps.id_fakultas = {$user->ID_FAKULTAS} {$where}
            order by p.nm_pengguna");
        
        foreach ($dosen_set as &$d)
            $d['url'] = bin2hex($user->Encrypt($d['ID_PENGGUNA']));
        
        $smarty->assign('dosen_set', $dosen_set);
    }
    
    if ($mode == 'edit')
    {
        $id_pengguna = $user->Decrypt(pack("H*", get('id_pengguna')));
        
        $dosen = $db->QueryToArray("
            select p.id_pengguna, p.username, p.nm_pengguna, d.nip_dosen, j.nm_jenjang, ps.nm_program_studi
            from dosen d
            join pengguna p on p.id_pengguna = d.id_pengguna
            join program_studi ps on ps.id_program_studi = d.id_program_studi
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where p.id_pengguna = {$id_pengguna}");
            
        $smarty->assign('dosen', $dosen[0]);
    }
}

$smarty->display("account/dosen/{$mode}.tpl");
?>
