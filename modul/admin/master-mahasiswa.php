<?php
include('config.php');

$mode = get('mode');

$mahasiswa_data = array();
$page_data = array();

for ($i = 0; $i < 10; $i++)
{
    array_push($mahasiswa_data, array(
        'NIM'       => '0809160XX',
        'NAMA'      => 'Nama Mahasiswa',
        'JK'        => $i % 2 == 1 ? 'P' : 'L',
        'JURUSAN'   => 'Jurusan',
    ));
}

for ($i = 1; $i <=5; $i++)
{
    array_push($page_data, array(
        'VALUE'     => $i,
        'SELECTED'  => $i == 1 ? true : false,
    ));
}

$smarty->assign('mahasiswas', $mahasiswa_data);
$smarty->assign('pages', $page_data);

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_ADMINISTRATOR)
{    
    if ($mode == 'edit')
        $smarty->display('master-mahasiswa-edit.tpl');
    else if ($mode == 'view')
        $smarty->display('master-mahasiswa-view.tpl');
    else
        $smarty->display('master-mahasiswa.tpl');
}
else $smarty->display('session-expired.tpl');
?>
