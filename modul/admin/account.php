<div class="center_title_bar">Account</div>

<table>
    <tr>
        <th>Menu</th>
        <th>Deskripsi</th>
    </tr>
    <tr>
        <td>Pegawai</td>
        <td>Melakukan editing pada akun pegawai. Meliputi akun dosen, pegawai, prodi, akademik, dll.</td>
    </tr>
    <tr>
        <td>Mahasiswa</td>
        <td>Untuk melakukan reset password mahasiswa.</td>
    </tr>
    <tr>
        <td>Ganti Password</td>
        <td>Untuk melakukan penggantian password akun sendiri.</td>
    </tr>
</table>