<?php

require 'config.php';
include 'class/data.class.php';

$d = new data($db);
//$db->Query("DELETE FROM AUCC.MODUL WHERE ID_MODUL=241");
//$db->Query("DELETE FROM SPI_TEMUAN_ISI WHERE STATUS_AKTIF=0");
//$db->Query("DELETE FROM SPI_TEMUAN_REKOMENDASI");
//$db->Query("alter table AUCC.SPI_TEMUAN_REKOMENDASI disable  constraint SPI_TEMUAN_REKOMENDASI_PE_FK1");
//$db->Query("UPDATE SPI_TEMUAN_ISI SET STATUS_AKTIF=1");
if (!empty($_POST)) {
    if (post('mode') == 'tambah') {
        $d->tambahDataTemuanIsi($_POST);
    } else if (post('mode') == 'edit') {
        $d->updateDataTemuanIsi($_POST);
    } else if (post('mode') == 'hapus') {
        $d->hapusDataTemuanIsi(post('id'));
    } else if (post('mode') == 'tambah-rekomendasi') {
        $d->tambahDataTemuanRekomendasi($_POST);
    } else if (post('mode') == 'hapus-rekomendasi') {
        $d->hapusDataTemuanRekomendasi(post('id'));
    }
}

if (!empty($_GET['mode'])) {

    if (get('mode') == 'edit' || get('mode') == 'hapus') {
        if(get('mode')=='edit'){
            $data_aturan = $d->loadDataAturan();
            $smarty->assign('data_aturan',$data_aturan);
        }
        $smarty->assign('temuan',$d->getDataTemuanIsi(get('id')));
    }
}


$data_subkelompok = $d->loadDataTemuanSubKelompok();
$data_temuan = $d->loadDataTemuanIsi();
$smarty->assign('data_subkelompok',$data_subkelompok);
$smarty->assign('data_temuan',$data_temuan);
$smarty->display("data/d-temuan.tpl");
?>
