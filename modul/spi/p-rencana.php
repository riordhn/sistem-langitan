<?php

require 'config.php';
require 'class/pemeriksaan.class.php';
require 'class/pendampingan.class.php';
require 'class/data.class.php';

$p = new pemeriksaan($db);
$d = new data($db);
$pd = new pendampingan($db);


if (!empty($_POST)) {
    if (post('mode') == 'tambah') {
        $p->tambahDataPemeriksaan($_POST);
    } else if (post('mode') == 'tambah-dari-permintaan') {
        $p->tambahDataPemeriksaan($_POST);
    } else if (post('mode') == 'edit') {
        $p->updateDataPemeriksaan($_POST);
    } else if (post('mode') == 'hapus') {
        
    } else if (post('mode') == 'tambah-bidang') {
        $p->tambahDataBidangPemeriksaan($_POST);
    } else if (post('mode') == 'tambah-auditor') {
        $p->tambahDataAuditorBidangPemeriksaan($_POST);
    } else if (post('mode') == 'hapus-bidang') {
        $p->hapusDataBidangPemeriksaan(post('bidang'));
    } else if (post('mode') == 'hapus-auditor') {
        $p->hapusDataAuditorBidangPemeriksaan(post('auditor'));
    }
}

if (!empty($_GET)) {

    if (get('mode') == 'edit' || get('mode') == 'tambah' || get('mode') == 'tambah-dari-permintaan' || get('mode') == 'bidang') {
        if (get('mode') == 'edit' || get('mode') == 'bidang') {
            $smarty->assign('data_personil', $d->loadDataPersonil());
            $smarty->assign('pemeriksaan', $p->getDataPemeriksaan(get('id')));
        } else if (get('mode') == 'tambah-dari-permintaan') {
            $smarty->assign('permintaan', $pd->getDataPermintaanPendampingan(get('permintaan')));
        }
        $smarty->assign('data_bidang', $d->loadDataBidang());
        $smarty->assign('data_unit', $p->loadDataUnitKerja());
    }
}
$data_pemeriksaan = $p->loadDataPemeriksaan();
$smarty->assign('auto_rpp', str_pad(count($data_pemeriksaan) + 1, 3, "0", STR_PAD_LEFT) . '/RPP-SPI/2013');
$smarty->assign('data_pemeriksaan', $data_pemeriksaan);
$smarty->display("pemeriksaan/p-rencana.tpl");
?>
