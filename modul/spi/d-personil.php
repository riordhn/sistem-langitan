<?php

require 'config.php';
include 'class/data.class.php';

$d = new data($db);

if (!empty($_POST)) {
    if (post('mode') == 'tambah') {
        $d->tambahDataPersonil($_POST);
    } else if (post('mode') == 'edit') {
        $d->updateDataPersonil($_POST);
    } else if (post('mode') == 'hapus') {
        $d->hapusDataPersonil(post('id'));
    }
}

if (!empty($_GET['mode'])) {

    if (get('mode') == 'edit' || get('mode') == 'hapus') {
        $smarty->assign('personil',$d->getDataPersonil(get('id')));
    }
}

$data_pengguna = $db->QueryToArray("SELECT ID_PENGGUNA,NM_PENGGUNA,GELAR_DEPAN,GELAR_BELAKANG FROM PENGGUNA WHERE JOIN_TABLE IN (1,2) ORDER BY NM_PENGGUNA");
$data_personil = $d->loadDataPersonil();
$smarty->assign('data_personil',$data_personil);
$smarty->assign('data_pengguna',$data_pengguna);
$smarty->display("data/d-personil.tpl");
?>
