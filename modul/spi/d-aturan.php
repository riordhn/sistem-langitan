<?php

require 'config.php';
include 'class/data.class.php';

$d = new data($db);

if (!empty($_POST)) {
    if (post('mode') == 'tambah') {
        $d->tambahDataAturan($_POST);
    } else if (post('mode') == 'edit') {
        $d->updateDataAturan($_POST);
    } else if (post('mode') == 'hapus') {
        $d->hapusDataAturan(post('id'));
    }
}

if (!empty($_GET['mode'])) {

    if (get('mode') == 'edit' || get('mode') == 'hapus') {
        $smarty->assign('peraturan',$d->getDataAturan(get('id')));
    }
}

$data_peraturan = $d->loadDataAturan();
$data_jenis_aturan = $d->loadDataJenisAturan();
$smarty->assign('data_peraturan',$data_peraturan);
$smarty->assign('data_jenis_aturan',$data_jenis_aturan);
$smarty->display("data/d-aturan.tpl");
?>
