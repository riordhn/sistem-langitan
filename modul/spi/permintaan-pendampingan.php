<?php

require 'config.php';
require 'class/pemeriksaan.class.php';
require 'class/pendampingan.class.php';
require 'class/data.class.php';

$p = new pemeriksaan($db);
$d = new data($db);
$pd = new pendampingan($db);


if (!empty($_POST)) {
    if (post('mode') == 'tambah') {
        $pd->tambahDataPermintaanPendampingan($_POST);
    } else if (post('mode') == 'edit') {
        $pd->updateDataPermintaanPendampingan($_POST);
    } else if (post('mode') == 'hapus') {
        
    }
}

if (!empty($_GET)) {
    $mode_array = array('tambah', 'edit', 'hapus');
    if (in_array(get('mode'), $mode_array)) {
        if (get('mode') == 'edit') {
            $smarty->assign('permintaan', $pd->getDataPermintaanPendampingan(get('id')));
        }
        $smarty->assign('data_unit', $p->loadDataUnitKerja());
        $smarty->assign('data_bidang', $d->loadDataBidang());
    }
}
$data_permintaan = $pd->loadDataPermintaanPendampingan();
$smarty->assign('data_permintaan', $data_permintaan);
$smarty->display("pendampingan/permintaan-pendampingan.tpl");
?>
