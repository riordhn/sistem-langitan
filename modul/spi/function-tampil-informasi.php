<?php

function alert_error($text,$lebar=30) {
    return "
        <div class='ui red message'>
            <i class='attention icon'></i>
            {$text}
        </div>";
}

function alert_success($text,$lebar=30) {
    return "
        <div class='ui green message'>
            <i class='checkmark sign icon'></i>
            {$text}
        </div>
        ";
}

?>
