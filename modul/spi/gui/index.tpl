<html>
    <head>
        <title>SPI Universitas Airlangga</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <link rel="stylesheet" type="text/css" href="../../css/reset.css" />
        <link rel="stylesheet" type="text/css" href="../../css/text.css" />
        <link rel="stylesheet" type="text/css" href="css/semantic.css" />
        <link rel="stylesheet" type="text/css" href="../../css/spi.css" />
        <link rel="stylesheet" type="text/css" href="../../css/spi-nav.css" />
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-ppm.custom.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css" />
        <link rel="shortcut icon" href="../../img/icon.ico" />
        <link rel="stylesheet" type="text/css" href="css/chosen.css">
        <style type="text/css" media="all">
            /* fix rtl for demo */
            .chosen-rtl .chosen-drop { left: -9000px; }
        </style>

        <script type="text/javascript" src="../../js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="../../js/additional-methods.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.price_format.1.4.js"></script>
        <script type="text/javascript" src="js/jquery.dataTables.js" ></script>
        <script type="text/javascript" src="js/chosen.jquery.min.js" ></script>
        <script type="text/javascript" src="../../js/cybercampus.ajax-1.0.js"></script>
        <script type="text/javascript" src="../../js/keuangan-nav.js"></script>
        <script type="text/javascript">
            var defaultRel = 'home';
            var defaultPage = 'home.php';
            id_role = 10;
        </script>
    </head>
    <body>
        <div id="main_container">
            <div id="header">
                <div id="judul_header"></div>
                <div id="menu_tab">
                    <ul class="topnav">
                        {foreach $struktur_menu as $m}
                            {if $m.AKSES == 1}
                                <li><a href="#{$m.NM_MODUL}!{$m.PAGE}">{$m.TITLE}</a>
                                    {if $m.SUBMENU!=NULL}
                                        <ul class="subnav">
                                            {foreach $m.SUBMENU as $sm}
                                                <li>
                                                    <a href="#{$sm.NM_MENU}!{$sm.PAGE}">{$sm.TITLE}</a>
                                                </li>
                                            {/foreach}
                                        </ul>
                                    {/if}
                                </li>
                            {/if}
                        {/foreach}
                        <li>
                            <label style="cursor: pointer" onclick="window.location.href = '../../logout.php'">Log Out</label>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="center">
                <div id="top_content">
                    <p style="text-align: right;margin-right: 60px">
                        User Login :  <label style="color: goldenrod;font-size: 13px;width: 10%;height: 40px">{$user|strtolower|capitalize}</label>
                    </p>
                </div>
                <div id="center_content"></div>
            </div>
            <div id="footer">
                <div id="left_footer">
                    <img src="../../../img/keuangan/footer_logo.jpg" width="170" height="37" />
                </div>
                <div id="center_footer">
                    Copyright © 2011 - Universitas Airlangga · All Rights Reserved<br />
                </div>
                <div id="right_footer">
                    <a href="#">home</a>
                    <a href="#">about</a>
                    <a href="#">sitemap</a>
                    <a href="#">rss</a>
                    <a href="#">contact us</a>
                </div>
            </div>
        </div>
    </body>
</html>