{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Tambah Permintaan Pendampingan</div>
    <form action="permintaan-pendampingan.php" method="post">
        <fieldset style="width: 98%">
            <legend>Tambah Permintaan Pendampingan</legend>
            <div class="field_form">
                <label>Nomor Permintaan : </label>
                <input type="text" size="20" maxlength="20" name="no_permintaan" value=""/>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Tgl Permintaan : </label>
                <input type="text" class="required datepicker" name="tgl_permintaan" value="{date('d-M-y')|upper}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Unit Kerja : </label>
                <select name="unit_kerja" class="required">
                    <option value="">Pilih Unit Kerja</option>
                    {foreach $data_unit as $d}
                        <option value="{$d.ID_UNIT_KERJA}">{$d.NM_UNIT_KERJA} {if $d.DESKRIPSI_UNIT_KERJA!=''}- {$d.DESKRIPSI_UNIT_KERJA}{/if}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Bidang : </label>
                <select name="bidang" class="required">
                    <option value="">Pilih Bidang</option>
                    {foreach $data_bidang as $d}
                        <option value="{$d.ID_SPI_BIDANG}">{$d.NAMA_BIDANG}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Mulai Pelaksanaan : </label>
                <input type="text" class="required datepicker" name="tgl_awal" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Selesai : </label>
                <input type="text" class="required datepicker" name="tgl_akhir" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Konsentrasi : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="konsentrasi"></textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Bagian : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="bagian"></textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Alasan : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="alasan"></textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Tujuan : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="tujuan"></textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="permintaan-pendampingan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Edit Permintaan Pendampingan</div>
    <form action="permintaan-pendampingan.php" method="post">
        <fieldset style="width: 98%">
            <legend>Edit Permintaan Pendampingan</legend>
            <div class="field_form">
                <label>Nomor Permintaan : </label>
                <input type="text" size="20" maxlength="20" name="no_permintaan" value="{$permintaan.NOMOR_PERMINTAAN}"/>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Tgl Permintaan : </label>
                <input type="text" class="required datepicker" name="tgl_permintaan" value="{$permintaan.TGL_PERMINTAAN}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Unit Kerja : </label>
                <select name="unit_kerja" class="required">
                    <option value="">Pilih Unit Kerja</option>
                    {foreach $data_unit as $d}
                        <option value="{$d.ID_UNIT_KERJA}" {if $d.ID_UNIT_KERJA==$permintaan.ID_UNIT_KERJA}selected="true"{/if}>{$d.NM_UNIT_KERJA} {if $d.DESKRIPSI_UNIT_KERJA!=''}- {$d.DESKRIPSI_UNIT_KERJA}{/if}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Bidang : </label>
                <select name="bidang" class="required">
                    <option value="">Pilih Bidang</option>
                    {foreach $data_bidang as $d}
                        <option value="{$d.ID_SPI_BIDANG}" {if $d.ID_SPI_BIDANG==$permintaan.ID_SPI_BIDANG}selected="true"{/if}>{$d.NAMA_BIDANG}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Mulai Pelaksanaan : </label>
                <input type="text" class="required datepicker" name="tgl_awal" value="{$permintaan.TGL_AWAL_RENCANA}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Selesai : </label>
                <input type="text" class="required datepicker" name="tgl_akhir" value="{$permintaan.TGL_AKHIR_RENCANA}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Konsentrasi : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="konsentrasi">{$permintaan.KONSENTRASI}</textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Bagian : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="bagian">{$permintaan.BAGIAN}</textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Alasan : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="alasan">{$permintaan.ALASAN}</textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Tujuan : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="tujuan">{$permintaan.TUJUAN}</textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="permintaan-pendampingan.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$permintaan.ID_SPI_PERMINTAAN_PENDAMPINGAN}"/>
                <input type="hidden" name="mode" value="edit"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">Hapus Rencana Pemeriksaan</div>
    <form action="lakip-program.php" method="post">
        <fieldset style="width: 98%">
            <div class="field_form">
                <label>Nomor RPP : </label>
                <input type="text" size="20" maxlength="20" readonly="" name="rpp" class="required" value="{$pemeriksaan.NOMOR_RPP}"/>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Unit Kerja : </label>
                <select name="unit_kerja" class="required">
                    <option value="">Pilih Tujuan</option>
                    {foreach $data_unit as $d}
                        <option value="{$d.ID_UNIT_KERJA}" {if $d.ID_UNIT_KERJA==$pemeriksaan.ID_UNIT_KERJA} selected="true" {/if}>{$d.NM_UNIT_KERJA} {if $d.DESKRIPSI_UNIT_KERJA!=''}- {$d.DESKRIPSI_UNIT_KERJA}{/if}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label style="width: 98%;font-size: 1.2em;color: #ff6600;text-align: left;">-- Pelaksanaan --</label>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Mulai : </label>
                <input type="text" class="required datepicker" name="tgl_awal" readonly="" value="{$pemeriksaan.TGL_AWAL}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Selesai : </label>
                <input type="text" class="required datepicker" name="tgl_akhir" readonly="" value="{$pemeriksaan.TGL_AKHIR}" />
            </div>
            <div class="field_form">
                <label>Tahun Anggaran : </label>
                <input type="number" class="required number" size="4" maxlength="4" readonly="" name="tahun" value="{$pemeriksaan.TAHUN_PEMERIKSAAN}" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                Apakah anda yakin menghapus item ini ?<br/>
                <input type="hidden" name="mode" value="delete"/>
                <input type="hidden" name="id" value="{$program.ID_PROGRAM_UTAMA}" />
                <a href="permintaan-pendampingan.php" class="button">Batal</a>
                <input type="submit" class="button" value="Ya"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Daftar Permintaan Pendampingan</div>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            oTable = $('#datatable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <a href="permintaan-pendampingan.php?mode=tambah" class="button">Tambah Permintaan</a>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
        <thead>
            <tr>
                <th>No.Permintaan</th>
                <th>Unit Kerja</th>
                <th>Bidang</th>
                <th>Tgl Permintaan</th>
                <th>Periode Pelaksanaan</th>
                <th>No RPP</th>
                <th class="center">-----</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_permintaan as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td>{$d.NOMOR_PERMINTAAN}</td>
                    <td>{$d.NM_UNIT_KERJA}</td>
                    <td>{$d.NAMA_BIDANG}</td>
                    <td class="center">{$d.TGL_PERMINTAAN}</td>
                    <td>{$d.TGL_AWAL_RENCANA} - {$d.TGL_AKHIR_RENCANA}</td>
                    <td>{$d.NOMOR_RPP}</td>
                    <td class="center">
                        <a href="permintaan-pendampingan.php?mode=edit&id={$d.ID_SPI_PERMINTAAN_PENDAMPINGAN}" class="button">Edit</a>
                        <a href="permintaan-pendampingan.php?mode=hapus&id={$d.ID_SPI_PERMINTAAN_PENDAMPINGAN}" class="button">Hapus</a>
                        <a href="p-rencana.php?mode=tambah-dari-permintaan&permintaan={$d.ID_SPI_PERMINTAAN_PENDAMPINGAN}" class="button">Rencana</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="spacer"></div>
{/if}
{literal}
    <script>
        $('form').validate();
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-M-y'
        }).css({'text-transform': 'uppercase'});
    </script>
{/literal}