{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Tambah Subkelompok Temuan Audit</div>
    <form action="d-subkelompok-temuan.php?kelompok={$kelompok.ID_SPI_TEMUAN_KELOMPOK}" method="post">
        <fieldset style="width: 95%">
            <legend>Tambah Subkelompok Temuan Audit</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Kelompok :</label>
                {$kelompok.NAMA_KELOMPOK}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kode Subkelompok : </label>
                <input type="text" class="required" size="20" name="kode" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Subkelompok : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="nama"></textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-subkelompok-temuan.php?kelompok={$kelompok.ID_SPI_TEMUAN_KELOMPOK}" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="hidden" name="kelompok" value="{$kelompok.ID_SPI_TEMUAN_KELOMPOK}"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Edit Subkelompok Temuan Audit</div>
    <form action="d-subkelompok-temuan.php?kelompok={$kelompok.ID_SPI_TEMUAN_KELOMPOK}" method="post">
        <fieldset style="width: 95%">
            <legend>Edit Subkelompok Temuan Audit</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Kelompok :</label>
                {$kelompok.NAMA_KELOMPOK}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kode Subkelompok : </label>
                <input type="text" class="required" size="20" name="kode" value="{$subkelompok.KODE_SUBKELOMPOK}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Subkelompok : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="nama">{$subkelompok.NAMA_SUBKELOMPOK}</textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-subkelompok-temuan.php?kelompok={$kelompok.ID_SPI_TEMUAN_KELOMPOK}" class="button">Batal</a>
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" value="{$subkelompok.ID_SPI_TEMUAN_SUBKELOMPOK}"/>
                <input type="hidden" name="kelompok" value="{$kelompok.ID_SPI_TEMUAN_KELOMPOK}"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='hapus'}
    <div class="center_title_bar">Hapus Subkelompok Temuan Audit</div>
    <form action="d-subkelompok-temuan.php?kelompok={$kelompok.ID_SPI_TEMUAN_KELOMPOK}" method="post">
        <fieldset style="width: 95%">
            <legend>Hapus Subkelompok Temuan Audit</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Kelompok :</label>
                {$kelompok.NAMA_KELOMPOK}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kode Subkelompok : </label>
                <input readonly="" type="text" class="required" size="20" name="kode" value="{$subkelompok.KODE_SUBKELOMPOK}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Subkelompok : </label>
                <textarea readonly="" style="width: 70%;height: 100px;resize: none" name="nama">{$subkelompok.NAMA_SUBKELOMPOK}</textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-subkelompok-temuan.php?kelompok={$kelompok.ID_SPI_TEMUAN_KELOMPOK}" class="button">Batal</a>
                <input type="hidden" name="mode" value="hapus"/>
                <input type="hidden" name="id" value="{$subkelompok.ID_SPI_TEMUAN_SUBKELOMPOK}"/>
                <input type="submit" class="button" value="Hapus"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Daftar Data Subkelompok Temuan Audit </div>
    <b>NAMA KELOMPOK : {$kelompok.NAMA_KELOMPOK}</b>
    <hr/>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            oTable = $('#data-subkelompok-temuan-table').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <a href="d-subkelompok-temuan.php?{$smarty.server.QUERY_STRING}&mode=tambah" class="button">Tambah Subkelompok</a>
    <a href="d-kelompok-temuan.php" class="button">Data Kelompok</a>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="data-subkelompok-temuan-table">
        <thead>
            <tr>
                <th>Kode Subkelompok</th>
                <th>Nama Subkelompok</th>
                <th class="center">-----</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_subkelompok as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td>{$d.KODE_SUBKELOMPOK}</td>
                    <td>{$d.NAMA_SUBKELOMPOK}</td>
                    <td class="center">
                        <a href="d-subkelompok-temuan.php?{$smarty.server.QUERY_STRING}&mode=edit&id={$d.ID_SPI_TEMUAN_SUBKELOMPOK}" class="button">Edit</a>
                        <a href="d-subkelompok-temuan.php?{$smarty.server.QUERY_STRING}&mode=hapus&id={$d.ID_SPI_TEMUAN_SUBKELOMPOK}" class="button">Hapus</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="spacer"></div>
{/if}
{literal}
    <script>
        $('.chosen').chosen();
        $('form').validate();
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-M-y'
        }).css({'text-transform': 'uppercase'});
    </script>
{/literal}