{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Tambah Bidang</div>
    <form action="d-bidang.php" method="post">
        <fieldset style="width: 95%">
            <legend>Tambah Bidang</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Bidang : </label>
                <input type="text" class="required" size="70"  name="nama" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-bidang.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Edit Bidang</div>
    <form action="d-bidang.php" method="post">
        <fieldset style="width: 95%">
            <legend>Edit Bidang</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Bidang : </label>
                <input type="text" class="required" name="nama" size="70" value="{$bidang.NAMA_BIDANG}" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-bidang.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" value="{$bidang.ID_SPI_BIDANG}"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='hapus'}
    <div class="center_title_bar">Hapus Bidang</div>
    <form action="d-bidang.php" method="post">
        <fieldset style="width: 95%">
            <legend>Hapus Bidang</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Bidang : </label>
                <input type="text" readonly="" class="required" size="70"  name="nama" value="{$bidang.NAMA_BIDANG}" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-bidang.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="hapus"/>
                <input type="hidden" name="id" value="{$bidang.ID_SPI_BIDANG}"/>
                <input type="submit" class="button" value="Hapus"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Daftar Data Bidang Pemeriksaan</div>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            oTable = $('#data-bidang-table').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <a href="d-bidang.php?mode=tambah" class="button">Tambah Bidang Pemeriksaan</a>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="data-bidang-table">
        <thead>
            <tr>
                <th>Nama Bidang</th>
                <th class="center">-----</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_bidang as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td>{$d.NAMA_BIDANG}</td>
                    <td class="center">
                        <a href="d-bidang.php?mode=edit&id={$d.ID_SPI_BIDANG}" class="button">Edit</a>
                        <a href="d-bidang.php?mode=hapus&id={$d.ID_SPI_BIDANG}" class="button">Hapus</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="spacer"></div>
{/if}
{literal}
    <script>
        $('.chosen').chosen();
        $('form').validate();
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-M-y'
        }).css({'text-transform': 'uppercase'});
    </script>
{/literal}