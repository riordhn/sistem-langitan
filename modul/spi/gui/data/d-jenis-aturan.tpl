{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Tambah Jenis Peraturan</div>
    <form action="d-jenis-aturan.php" method="post">
        <fieldset style="width: 95%">
            <legend>Tambah Jenis Peraturan</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Jenis Peraturan :</label>
                <input type="text" class="required" size="50" name="nama" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-jenis-aturan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Edit Jenis Peraturan</div>
    <form action="d-jenis-aturan.php" method="post">
        <fieldset style="width: 95%">
            <legend>Edit Jenis Peraturan</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Jenis Peraturan :</label>
                <input type="text" class="required" size="50" name="nama" value="{$jenis_aturan.NAMA_JENIS}" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-jenis-aturan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" value="{$jenis_aturan.ID_SPI_JENIS_REF_PERATURAN}"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='hapus'}
    <div class="center_title_bar">Hapus Jenis Peraturan</div>
    <form action="d-jenis-aturan.php" method="post">
        <fieldset style="width: 95%">
            <legend>Hapus Jenis Peraturan</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Jenis Peraturan :</label>
                <input type="text" readonly="" class="required" name="nama" value="{$jenis_aturan.NAMA_JENIS}" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-jenis-aturan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="hapus"/>
                <input type="hidden" name="id" value="{$jenis_aturan.ID_SPI_JENIS_REF_PERATURAN}"/>
                <input type="submit" class="button" value="Hapus"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Daftar Data Jenis Peraturan</div>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            oTable = $('#data-jenis-aturan-table').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <a href="d-aturan.php" class="button">Kembali ke Data Peraturan</a>
    <a href="d-jenis-aturan.php?mode=tambah" class="button">Tambah Jenis Peraturan</a>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="data-jenis-aturan-table">
        <thead>
            <tr>
                <th>Nama Jenis Peraturan</th>
                <th class="center">-----</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_jenis_aturan as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td>{$d.NAMA_JENIS}</td>
                    <td class="center">
                        <a href="d-jenis-aturan.php?mode=edit&id={$d.ID_SPI_BIDANG}" class="button">Edit</a>
                        <a href="d-jenis-aturan.php?mode=hapus&id={$d.ID_SPI_BIDANG}" class="button">Hapus</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="spacer"></div>
{/if}
{literal}
    <script>
        $('.chosen').chosen();
        $('form').validate();
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-M-y'
        }).css({'text-transform': 'uppercase'});
    </script>
{/literal}