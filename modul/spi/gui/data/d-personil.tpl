{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Tambah Personil</div>
    <form action="d-personil.php" method="post">
        <fieldset style="width: 95%">
            <legend>Tambah Personil</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Personil : </label>
                <select name="pengguna" data-placeholder="Pilih Personil" class="chosen" style="width:350px;" >
                    {foreach $data_pengguna as $d}
                        <option value="{$d.ID_PENGGUNA}">{$d.GELAR_DEPAN} {$d.NM_PENGGUNA} {$d.GELAR_BELAKANG}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Jabatan : </label>
                <input type="text" class="required" size="70" name="jabatan" value="" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-personil.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Edit Personil</div>
    <form action="d-personil.php" method="post">
        <fieldset style="width: 95%">
            <legend>Edit Personil</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Personil : </label>
                <select name="pengguna" data-placeholder="Pilih Personil" class="chosen" style="width:350px;" >
                    {foreach $data_pengguna as $d}
                        <option value="{$d.ID_PENGGUNA}" {if $personil.ID_PENGGUNA==$d.ID_PENGGUNA}selected{/if}>{$d.GELAR_DEPAN} {$d.NM_PENGGUNA} {$d.GELAR_BELAKANG}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Jabatan : </label>
                <input type="text" class="required" size="70" name="jabatan" value="{$personil.JABATAN}" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-personil.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" value="{$personil.ID_SPI_PERSONIL}"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='hapus'}
    <div class="center_title_bar">Hapus Personil</div>
    <form action="d-personil.php" method="post">
        <fieldset style="width: 95%">
            <legend>Hapus Personil</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Personil : </label>
                <input type="text" readonly="" size="50" class="required" name="nama" value="{$personil.GELAR_DEPAN} {$personil.NM_PENGGUNA} {$personil.GELAR_BELAKANG}" />
            </div>
            <div class="field_form">
                <label>Jabatan : </label>
                <input type="text" readonly="" size="70"  class="required" name="nama" value="{$personil.JABATAN}" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-personil.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="hapus"/>
                <input type="hidden" name="id" value="{$personil.ID_SPI_PERSONIL}"/>
                <input type="submit" class="button" value="Hapus"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Daftar Data Personil Pemeriksaan</div>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            oTable = $('#data-bidang-table').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <a href="d-personil.php?mode=tambah" class="button">Tambah Personil Pemeriksaan</a>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="data-bidang-table">
        <thead>
            <tr>
                <th>Nama Personil</th>
                <th>Jabatan</th>
                <th class="center">-----</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_personil as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td>{$d.GELAR_DEPAN} {$d.NM_PENGGUNA} {$d.GELAR_BELAKANG}</td>
                    <td>{$d.JABATAN}</td>
                    <td class="center">
                        <a href="d-personil.php?mode=edit&id={$d.ID_SPI_PERSONIL}" class="button">Edit</a>
                        <a href="d-personil.php?mode=hapus&id={$d.ID_SPI_PERSONIL}" class="button">Hapus</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="spacer"></div>
{/if}
{literal}
    <script>
        $('.chosen').chosen();
        $('form').validate();
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-M-y'
        }).css({'text-transform': 'uppercase'});
    </script>
{/literal}