{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Tambah Temuan Audit</div>
    <form action="d-temuan.php" method="post">
        <fieldset style="width: 98%">
            <legend>Tambah Temuan Audit</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kelompok: </label>
                <b id="kelompok_label">Data Kosong</b>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Subelompok: </label>
                <b id="subkelompok_label">Data Kosong</b>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kode Subkelompok: </label>
                <select name="subkelompok" id="subkelompok_select" class="required">
                    <option value="">Pilih Subkelompok Temuan</option>
                    {foreach $data_subkelompok as $d}
                        <option value="{$d.ID_SPI_TEMUAN_SUBKELOMPOK}">Kode Kelompok {$d.KODE_KELOMPOK}, Kode Subkelompok {$d.KODE_SUBKELOMPOK}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kode Temuan : </label>
                <input type="text" class="required" size="20" name="kode" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Isi : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="isi"></textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-temuan.php" class="button">Batal</a>
                <input type="hidden" name="kelompok" id="kelompok" value=""/>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            oTable = $('#data-temuan-rekomendasi-table').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <div class="center_title_bar">Edit Temuan Audit</div>
    <form action="d-temuan.php" method="post">
        <fieldset style="width: 98%">
            <legend>Edit Temuan Audit</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kelompok: </label>
                <b id="kelompok_label">{$temuan.NAMA_KELOMPOK}</b>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Subelompok: </label>
                <b id="subkelompok_label">{$temuan.NAMA_SUBKELOMPOK}</b>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kode Subkelompok: </label>
                <select name="subkelompok" id="subkelompok_select" class="required">
                    <option value="">Pilih Subkelompok Temuan</option>
                    {foreach $data_subkelompok as $d}
                        <option value="{$d.ID_SPI_TEMUAN_SUBKELOMPOK}" {if $temuan.ID_SPI_TEMUAN_SUBKELOMPOK==$d.ID_SPI_TEMUAN_SUBKELOMPOK}selected="true"{/if}>Kode Kelompok {$d.KODE_KELOMPOK}, Kode Subkelompok {$d.KODE_SUBKELOMPOK}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kode Temuan : </label>
                <input type="text" class="required" size="20" name="kode" value="{$temuan.KODE_ISI}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Isi : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="isi">{$temuan.ISI}</textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <input type="hidden" name="kelompok" id="kelompok" value=""/>
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" id="temuan_id" value="{$temuan.ID_SPI_TEMUAN_ISI}"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
    <fieldset>
        <form action="d-temuan.php?{$smarty.server.QUERY_STRING}" method="post">

            <div class="clear-fix"></div>
            <div class="field_form">
                <label style="width: 98%;font-size: 1.2em;color: #ff6600;text-align: left;">-- Rekomendasi Ref.Peraturan --</label>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Ref.Aturan : </label>
                <select  name="aturan" >
                    <option value="">Pilih Ref.Aturan</option>
                    {foreach $data_aturan as $d}
                        <option value="{$d.ID_SPI_REF_PERATURAN}">Jenis : {$d.NAMA_JENIS} ,Nomor : {$d.NOMOR_PERATURAN}</option>
                    {/foreach}
                </select>
                {*<a class="button disable-ajax" style="padding: 3px;margin: 0px;cursor: pointer" id="rekomendasi_temuan_tambah_button">Tambah</a>*}

                <input type="hidden" name="mode" value="tambah-rekomendasi"/>
                <input type="hidden" name="temuan" value="{$temuan.ID_SPI_TEMUAN_ISI}"/>
                <input type="submit" class="ui-state-default ui-corner-all ui-icon ui-icon-plus" style="display:inline ;padding: 8px;cursor: pointer"/>
            </div>

        </form>
        <div class="clear-fix"></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="data-temuan-rekomendasi-table">
            <thead>
                <tr>
                    <th>Nomor</th>
                    <th>Tentang</th>
                    <th class="center">-----</th>
                </tr>
            </thead>
            <tbody>
                {foreach $temuan.DATA_REKOMENDASI as $d}
                    <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                        <td>{$d.NOMOR_PERATURAN}</td>
                        <td>{$d.NAMA_JENIS}<br/>{$d.ISI_PERATURAN}</td>
                        <td class="center">
                            <form action="d-temuan.php?{$smarty.server.QUERY_STRING}" method="post" >
                                <input type="hidden" name="mode" value="hapus-rekomendasi"/>
                                <input type="hidden" name="id" value="{$d.ID_SPI_TEMUAN_REKOMENDASI}"/>
                                <input type="submit" class="ui-state-default ui-corner-all ui-icon ui-icon-closethick" style="display:inline ;padding: 8px;margin: 3px;cursor: pointer;vertical-align: middle"/>
                            </form>
                        </td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </fieldset>
    <a href="d-temuan.php" class="button">Kembali</a>
{else if $smarty.get.mode=='hapus'}
    <div class="center_title_bar">Hapus Temuan Audit</div>
    <form action="d-temuan.php" method="post">
        <fieldset style="width: 98%">
            <legend>Hapus Temuan Audit</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kelompok: </label>
                <b id="kelompok_label">{$temuan.NAMA_KELOMPOK}</b>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Subelompok: </label>
                <b id="subkelompok_label">{$temuan.NAMA_SUBKELOMPOK}</b>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kode Temuan : </label>
                <input type="text" class="required" size="20" name="kode" value="{$temuan.KODE_ISI}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Isi : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="isi">{$temuan.ISI}</textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-temuan.php" class="button">Batal</a>
                <input type="hidden" name="kelompok" id="kelompok" value=""/>
                <input type="hidden" name="mode" value="hapus"/>
                <input type="hidden" name="id" value="{$temuan.ID_SPI_TEMUAN_ISI}"/>
                <input type="submit" class="button" value="Hapus"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Daftar Data Temuan Audit </div>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            oTable = $('#data-temuan-table').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <a href="d-temuan.php?mode=tambah" class="button">Tambah Temuan Audit </a>
    <a href="d-kelompok-temuan.php" class="button">Data Kelompok</a>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="data-temuan-table">
        <thead>
            <tr>
                <th>Kode Temuan</th>
                <th>Isi Temuan</th>
                <th>Rekomendasi Peraturan</th>
                <th class="center">-----</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_temuan as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td>{$d.KODE_ISI}</td>
                    <td>
                        <b>Kelompok </b>: {$d.NAMA_KELOMPOK}<br/>
                        <b>Subkelompok </b>: {$d.NAMA_SUBKELOMPOK}<br/>
                        <b>Jenis </b>:{$d.ISI}
                    </td>
                    <td>
                        <ul style="margin-left: 10px;font-size: 0.9em">
                        {foreach $d.DATA_REKOMENDASI as $rk}
                            <li><b>{$rk.NAMA_JENIS}</b> <br/>{$rk.NOMOR_PERATURAN} </li>
                        {/foreach}
                        </ul>
                    </td>
                    <td class="center">
                        <a href="d-temuan.php?mode=edit&id={$d.ID_SPI_TEMUAN_ISI}" class="button">Edit</a>
                        <a href="d-temuan.php?mode=hapus&id={$d.ID_SPI_TEMUAN_ISI}" class="button">Hapus</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="spacer"></div>
{/if}
{literal}
    <script>
        $('.chosen').chosen();
        $('form').validate();
        $('#subkelompok_select').change(function() {
            $.ajax({
                url: 'getSubkelompokDetail.php',
                data: 'subkelompok=' + $(this).val(),
                success: function(data) {
                    var subkelompok = $.parseJSON(data);
                    $('#kelompok_label').text(subkelompok.NAMA_KELOMPOK);
                    $('#subkelompok_label').text(subkelompok.NAMA_SUBKELOMPOK);
                    $('#kelompok').val(subkelompok.ID_SPI_TEMUAN_KELOMPOK);
                }
            });
        });

        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-M-y'
        }).css({'text-transform': 'uppercase'});
    </script>
{/literal}