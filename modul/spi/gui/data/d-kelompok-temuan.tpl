{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Tambah Kelompok Temuan Audit</div>
    <form action="d-kelompok-temuan.php" method="post">
        <fieldset style="width: 95%">
            <legend>Tambah Kelompok Temuan Audit</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kode Kelompok : </label>
                <input type="text" class="required" size="20" name="kode" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Kelompok : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="nama"></textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-kelompok-temuan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Edit Kelompok Temuan Audit</div>
    <form action="d-kelompok-temuan.php" method="post">
        <fieldset style="width: 95%">
            <legend>Edit Kelompok Temuan Audit</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kode Kelompok : </label>
                <input type="text" class="required" size="20" name="kode" value="{$kelompok.KODE_KELOMPOK}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Kelompok : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="nama">{$kelompok.NAMA_KELOMPOK}</textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-kelompok-temuan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" value="{$kelompok.ID_SPI_TEMUAN_KELOMPOK}"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='hapus'}
    <div class="center_title_bar">Hapus Kelompok Temuan Audit</div>
    <form action="d-kelompok-temuan.php" method="post">
        <fieldset style="width: 95%">
            <legend>Hapus Kelompok Temuan Audit</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Kode Kelompok : </label>
                <input type="text" readonly="" size="20" name="kode" value="{$kelompok.KODE_KELOMPOK}" />
            </div>
             <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nama Kelompok : </label>
                <textarea readonly="" style="width: 70%;height: 100px;resize: none" name="nama">{$kelompok.NAMA_KELOMPOK}</textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-kelompok-temuan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="hapus"/>
                <input type="hidden" name="id" value="{$kelompok.ID_SPI_TEMUAN_KELOMPOK}"/>
                <input type="submit" class="button" value="Hapus"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Daftar Data Kelompok Temuan Audit </div>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            oTable = $('#data-kelompok-temuan-table').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <a href="d-kelompok-temuan.php?mode=tambah" class="button">Tambah Kelompok</a>
    <a href="d-temuan.php" class="button">Data Temuan Audit</a>

    <table cellpadding="0" cellspacing="0" border="0" class="display" id="data-kelompok-temuan-table">
        <thead>
            <tr>
                <th>Kode Kelompok</th>
                <th>Nama Kelompok</th>
                <th class="center">-----</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_kelompok as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td>{$d.KODE_KELOMPOK}</td>
                    <td>{$d.NAMA_KELOMPOK}</td>
                    <td class="center">
                        <a href="d-kelompok-temuan.php?mode=edit&id={$d.ID_SPI_TEMUAN_KELOMPOK}" class="button">Edit</a>
                        <a href="d-kelompok-temuan.php?mode=hapus&id={$d.ID_SPI_TEMUAN_KELOMPOK}" class="button">Hapus</a>
                        <a href="d-subkelompok-temuan.php?kelompok={$d.ID_SPI_TEMUAN_KELOMPOK}" class="button">Subkelompok</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="spacer"></div>
{/if}
{literal}
    <script>
        $('.chosen').chosen();
        $('form').validate();
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-M-y'
        }).css({'text-transform': 'uppercase'});
    </script>
{/literal}