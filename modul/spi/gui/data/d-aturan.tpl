{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Tambah Peraturan</div>
    <form action="d-aturan.php" method="post">
        <fieldset style="width: 95%">
            <legend>Tambah Peraturan</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Jenis Peraturan: </label>
                <select name="jenis" class="required">
                    <option value="">Pilih Jenis Peraturan</option>
                    {foreach $data_jenis_aturan as $d}
                        <option value="{$d.ID_SPI_JENIS_REF_PERATURAN}">{$d.NAMA_JENIS}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nomor Peraturan : </label>
                <input type="text" class="required" size="30" name="nomor" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Tahun Peraturan : </label>
                <input type="text" class="required" size="4" maxlength="4" name="tahun" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Isi : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="isi"></textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-aturan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Edit Peraturan</div>
    <form action="d-aturan.php" method="post">
        <fieldset style="width: 95%">
            <legend>Edit Peraturan</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Jenis Peraturan: </label>
                <select name="jenis" class="required">
                    <option value="">Pilih Jenis Peraturan</option>
                    {foreach $data_jenis_aturan as $d}
                        <option value="{$d.ID_SPI_JENIS_REF_PERATURAN}" {if $peraturan.JENIS_PERATURAN==$d.ID_SPI_JENIS_REF_PERATURAN}selected="true"{/if}>{$d.NAMA_JENIS}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nomor Peraturan : </label>
                <input type="text" class="required" size="30" name="nomor"  value="{$peraturan.NOMOR_PERATURAN}"/>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Tahun Peraturan : </label>
                <input type="text" class="required" size="4" maxlength="4" name="tahun" value="{$peraturan.TAHUN_PERATURAN}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Isi : </label>
                <textarea style="width: 70%;height: 100px;resize: none" name="isi">{$peraturan.ISI_PERATURAN}</textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-aturan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" value="{$peraturan.ID_SPI_REF_PERATURAN}"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='hapus'}
    <div class="center_title_bar">Hapus Peraturan</div>
    <form action="d-aturan.php" method="post">
        <fieldset style="width: 95%">
            <legend>Hapus Peraturan</legend>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Jenis Peraturan: </label>
                    {foreach $data_jenis_aturan as $d}
                        {if $peraturan.JENIS_PERATURAN==$d.ID_SPI_JENIS_REF_PERATURAN}{$d.NAMA_JENIS}{/if}
                    {/foreach}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nomor Peraturan : </label>
                <input type="text" class="required" size="30" readonly="" name="nomor"  value="{$peraturan.NOMOR_PERATURAN}"/>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Tahun Peraturan : </label>
                <input type="text" class="required" size="4" readonly="" name="tahun" value="{$peraturan.TAHUN_PERATURAN}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Isi : </label>
                <textarea style="width: 70%;height: 100px;resize: none" readonly="" name="isi">{$peraturan.ISI_PERATURAN}</textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="d-aturan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="hapus"/>
                <input type="hidden" name="id" value="{$peraturan.ID_SPI_REF_PERATURAN}"/>
                <input type="submit" class="button" value="Hapus"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Daftar Data Peraturan </div>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            oTable = $('#data-bidang-table').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <a href="d-aturan.php?mode=tambah" class="button">Tambah Peraturan </a>
    <a href="d-jenis-aturan.php" class="button">Data Jenis Peraturan </a>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="data-bidang-table">
        <thead>
            <tr>
                <th>Jenis Peraturan</th>
                <th>Nomor</th>
                <th>Tahun</th>
                <th>Tentang</th>
                <th class="center">-----</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_peraturan as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td>{$d.NAMA_JENIS}</td>
                    <td>{$d.NOMOR_PERATURAN}</td>
                    <td>{$d.TAHUN_PERATURAN}</td>
                    <td>{$d.ISI_PERATURAN}</td>
                    <td class="center">
                        <a href="d-aturan.php?mode=edit&id={$d.ID_SPI_REF_PERATURAN}" class="button">Edit</a>
                        <a href="d-aturan.php?mode=hapus&id={$d.ID_SPI_REF_PERATURAN}" class="button">Hapus</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="spacer"></div>
{/if}
{literal}
    <script>
        $('.chosen').chosen();
        $('form').validate();
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-M-y'
        }).css({'text-transform': 'uppercase'});
    </script>
{/literal}