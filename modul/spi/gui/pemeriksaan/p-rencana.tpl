{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Tambah Rencana Penugasan Pemeriksaan</div>
    <form action="p-rencana.php" method="post">
        <fieldset style="width: 98%">
            <legend>Tambah Rencana Penugasan Pemeriksaan</legend>
            <div class="field_form">
                <label>Nomor RPP : </label>
                <input type="text" size="20" maxlength="20" name="rpp" value="{$auto_rpp}"/>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Unit Kerja : </label>
                <select name="unit_kerja" class="required">
                    <option value="">Pilih Unit Kerja</option>
                    {foreach $data_unit as $d}
                        <option value="{$d.ID_UNIT_KERJA}">{$d.NM_UNIT_KERJA} {if $d.DESKRIPSI_UNIT_KERJA!=''}- {$d.DESKRIPSI_UNIT_KERJA}{/if}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label style="width: 98%;font-size: 1.2em;color: #ff6600;text-align: left;">-- Pelaksanaan --</label>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Mulai : </label>
                <input type="text" class="required datepicker" name="tgl_awal" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Selesai : </label>
                <input type="text" class="required datepicker" name="tgl_akhir" />
            </div>
            <div class="field_form">
                <label>Tahun Anggaran : </label>
                <input type="number" class="required number" size="4" maxlength="4" name="tahun" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="p-rencana.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='tambah-dari-permintaan'}
    <div class="center_title_bar">Tambah Rencana Penugasan Pemeriksaan dari Permintaan</div>
    <form action="permintaan-pendampingan.php" method="post">
        <fieldset style="width: 98%">
            <legend>Data Permintaan Pendampingan</legend>
            <div class="field_form">
                <label>Nomor Permintaan : </label>
                {$permintaan.NOMOR_PERMINTAAN}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Tgl Permintaan : </label>
                {$permintaan.TGL_PERMINTAAN}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Unit Kerja : </label>
                {$permintaan.NM_UNIT_KERJA}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Bidang : </label>
                {$permintaan.BIDANG}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Mulai Pelaksanaan : </label>
                {$permintaan.TGL_AWAL_RENCANA}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Selesai : </label>
                {$permintaan.TGL_AKHIR_RENCANA}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Konsentrasi : </label>
                {$permintaan.KONSENTRASI}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Bagian : </label>
                {$permintaan.BAGIAN}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Alasan : </label>
                {$permintaan.ALASAN}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Tujuan : </label>
                {$permintaan.TUJUAN}
            </div>
        </fieldset>
    </form>
    <form action="p-rencana.php" method="post">
        <fieldset style="width: 98%">
            <legend>Tambah Rencana Penugasan Pemeriksaan</legend>
            <div class="field_form">
                <label>Nomor RPP : </label>
                <input type="text" size="20" maxlength="20" name="rpp" value="{$auto_rpp}"/>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Unit Kerja : </label>
                <select name="unit_kerja" class="required">
                    <option value="">Pilih Unit Kerja</option>
                    {foreach $data_unit as $d}
                        <option value="{$d.ID_UNIT_KERJA}">{$d.NM_UNIT_KERJA} {if $d.DESKRIPSI_UNIT_KERJA!=''}- {$d.DESKRIPSI_UNIT_KERJA}{/if}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label style="width: 98%;font-size: 1.2em;color: #ff6600;text-align: left;">-- Pelaksanaan --</label>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Mulai : </label>
                <input type="text" class="required datepicker" name="tgl_awal" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Selesai : </label>
                <input type="text" class="required datepicker" name="tgl_akhir" />
            </div>
            <div class="field_form">
                <label>Tahun Anggaran : </label>
                <input type="number" class="required number" size="4" maxlength="4" name="tahun" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="p-rencana.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah-dari-permintaan"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Edit Rencana Penugasan Pemeriksaan</div>
    <form action="p-rencana.php" method="post">
        <fieldset style="width: 98%">
            <legend>Edit Rencana Penugasan Pemeriksaan</legend>
            <div class="field_form">
                <label>Nomor RPP : </label>
                <input type="text" size="20" maxlength="20" name="rpp" class="required" value="{$pemeriksaan.NOMOR_RPP}"/>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Unit Kerja : </label>
                <select name="unit_kerja" class="required">
                    <option value="">Pilih Tujuan</option>
                    {foreach $data_unit as $d}
                        <option value="{$d.ID_UNIT_KERJA}" {if $d.ID_UNIT_KERJA==$pemeriksaan.ID_UNIT_KERJA} selected="true" {/if}>{$d.NM_UNIT_KERJA} {if $d.DESKRIPSI_UNIT_KERJA!=''}- {$d.DESKRIPSI_UNIT_KERJA}{/if}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label style="width: 98%;font-size: 1.2em;color: #ff6600;text-align: left;">-- Pelaksanaan --</label>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Mulai : </label>
                <input type="text" class="required datepicker" name="tgl_awal" value="{$pemeriksaan.TGL_AWAL}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Selesai : </label>
                <input type="text" class="required datepicker" name="tgl_akhir" value="{$pemeriksaan.TGL_AKHIR}" />
            </div>
            <div class="field_form">
                <label>Tahun Anggaran : </label>
                <input type="number" class="required number" size="4" maxlength="4" name="tahun" value="{$pemeriksaan.TAHUN_PEMERIKSAAN}" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="p-rencana.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$pemeriksaan.ID_SPI_PEMERIKSAAN}"/>
                <input type="hidden" name="mode" value="edit"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">Hapus Rencana Pemeriksaan</div>
    <form action="lakip-program.php" method="post">
        <fieldset style="width: 98%">
            <div class="field_form">
                <label>Nomor RPP : </label>
                <input type="text" size="20" maxlength="20" readonly="" name="rpp" class="required" value="{$pemeriksaan.NOMOR_RPP}"/>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Unit Kerja : </label>
                <select name="unit_kerja" class="required">
                    <option value="">Pilih Tujuan</option>
                    {foreach $data_unit as $d}
                        <option value="{$d.ID_UNIT_KERJA}" {if $d.ID_UNIT_KERJA==$pemeriksaan.ID_UNIT_KERJA} selected="true" {/if}>{$d.NM_UNIT_KERJA} {if $d.DESKRIPSI_UNIT_KERJA!=''}- {$d.DESKRIPSI_UNIT_KERJA}{/if}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label style="width: 98%;font-size: 1.2em;color: #ff6600;text-align: left;">-- Pelaksanaan --</label>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Mulai : </label>
                <input type="text" class="required datepicker" name="tgl_awal" readonly="" value="{$pemeriksaan.TGL_AWAL}" />
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Selesai : </label>
                <input type="text" class="required datepicker" name="tgl_akhir" readonly="" value="{$pemeriksaan.TGL_AKHIR}" />
            </div>
            <div class="field_form">
                <label>Tahun Anggaran : </label>
                <input type="number" class="required number" size="4" maxlength="4" readonly="" name="tahun" value="{$pemeriksaan.TAHUN_PEMERIKSAAN}" />
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                Apakah anda yakin menghapus item ini ?<br/>
                <input type="hidden" name="mode" value="delete"/>
                <input type="hidden" name="id" value="{$program.ID_PROGRAM_UTAMA}" />
                <a href="p-rencana.php" class="button">Batal</a>
                <input type="submit" class="button" value="Ya"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='bidang'}
    <div class="center_title_bar">Detail Rencana Pemeriksaan Penugasan</div>
    <form action="lakip-program.php" method="post">
        <fieldset style="width: 98%">
            <legend>Data Rencana Pemeriksaan Penugasan</legend>
            <div class="field_form">
                <label>Nomor RPP : </label>
                <input type="text" size="20" maxlength="20" readonly="" name="rpp" class="required" value="{$pemeriksaan.NOMOR_RPP}"/>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Unit Kerja : </label>
                {foreach $data_unit as $d}
                    {if $d.ID_UNIT_KERJA==$pemeriksaan.ID_UNIT_KERJA} {$d.NM_UNIT_KERJA} {if $d.DESKRIPSI_UNIT_KERJA!=''}- {$d.DESKRIPSI_UNIT_KERJA}{/if} {/if}
                {/foreach}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label style="width: 98%;font-size: 1.2em;color: #ff6600;text-align: left;">-- Pelaksanaan --</label>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Mulai : </label>
                {$pemeriksaan.TGL_AWAL}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Selesai : </label>
                {$pemeriksaan.TGL_AKHIR}
            </div>
            <div class="field_form">
                <label>Tahun Anggaran : </label>
                {$pemeriksaan.TAHUN_PEMERIKSAAN}
            </div>
        </fieldset>
    </form>
    <a class="button" href="p-rencana.php">Kembali</a>
    <form action="p-rencana.php?{$smarty.server.QUERY_STRING}" method="post">
        <fieldset style="width: 98%">
            <div class="field_form">
                <label style="width: 98%;font-size: 1.2em;color: #ff6600;text-align: left;">-- Bidang Pemeriksaan --</label>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Bidang : </label>
                <select  name="bidang" >
                    <option value="">Pilih Bidang</option>
                    {foreach $data_bidang as $d}
                        <option value="{$d.ID_SPI_BIDANG}">{$d.NAMA_BIDANG}</option>
                    {/foreach}
                </select>
                {*<a class="button disable-ajax" style="padding: 3px;margin: 0px;cursor: pointer" id="rekomendasi_temuan_tambah_button">Tambah</a>*}

                <input type="hidden" name="mode" value="tambah-bidang"/>
                <input type="hidden" name="pemeriksaan" value="{$pemeriksaan.ID_SPI_PEMERIKSAAN}"/>
                <input type="submit" class="ui-state-default ui-corner-all ui-icon ui-icon-plus" style="display:inline ;padding: 8px;cursor: pointer"/>
            </div>
        </fieldset>
    </form>
    <div class="clear-fix"></div>
    <table cellpadding="0" cellspacing="0" border="0" class="display">
        <thead>
            <tr>
                <th>Bidang</th>
                <th>Daftar Auditor</th>
                <th class="center">-----</th>
            </tr>
        </thead>
        <tbody>
            {foreach $pemeriksaan.DATA_BIDANG as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td>{$d.NAMA_BIDANG}</td>
                    <td>
                        <ul style="margin-left: 10px;font-size: 0.9em">
                            {foreach $d.DATA_AUDITOR as $au}
                                <li>{$au.GELAR_DEPAN} {$au.NM_PENGGUNA} {$au.GELAR_BELAKANG} ({$au.JABATAN})
                                    <form action="p-rencana.php?{$smarty.server.QUERY_STRING}" method="post" style="display: inline;" >
                                        <input type="hidden" name="mode" value="hapus-auditor"/>
                                        <input type="hidden" name="auditor" value="{$au.ID_SPI_PEMERIKSAAN_AUDITOR}"/>
                                        <input type="submit" class="ui-state-default ui-corner-all ui-icon ui-icon-closethick" style="display:inline ;padding: 8px;margin: 3px;cursor: pointer;vertical-align: middle"/>
                                    </form>
                                </li>
                            {/foreach}
                        </ul>
                    </td>
                    <td class="center">
                        <form action="p-rencana.php?{$smarty.server.QUERY_STRING}" method="post">
                            <input type="hidden" name="mode" value="hapus-bidang"/>
                            <input type="hidden" name="bidang" value="{$d.ID_SPI_PEMERIKSAAN_BIDANG}"/>
                            <input type="submit" class="ui-state-default ui-corner-all ui-icon ui-icon-closethick" style="display:inline-block ;padding: 8px;margin: 3px;cursor: pointer;vertical-align: top"/>
                            <input type="button" class="disable-ajax ui-state-default ui-corner-all ui-icon ui-icon-person" onclick="$('#tambah-auditor-{$d.ID_SPI_PEMERIKSAAN_BIDANG}').fadeIn();" style="display:inline-block ;padding: 8px;margin: 3px;cursor: pointer;vertical-align: top;"/>
                        </form>

                    </td>
                </tr>
                <tr style="display: none" id="tambah-auditor-{$d.ID_SPI_PEMERIKSAAN_BIDANG}" >
                    <td colspan="3">
                        <form action="p-rencana.php?{$smarty.server.QUERY_STRING}" method="post">
                            <fieldset style="width: 90%">
                                <legend>Tambah Auditor Pemeriksaan Bidang {$d.NAMA_BIDANG}</legend>
                                <div class="clear-fix"></div>
                                <div class="field_form">
                                    <label>Auditor : </label>
                                    <select name="auditor" class="required">
                                        <option value="">Pilih personil</option>
                                        {foreach $data_personil as $per}
                                            <option value="{$per.ID_SPI_PERSONIL}" >{$per.GELAR_DEPAN} {$per.NM_PENGGUNA} {$per.GELAR_BELAKANG}</option>
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="clear-fix"></div>
                                <div class="field_form">
                                    <label>Jabatan : </label>
                                    <input type="text" size="30" maxlength="30"  name="jabatan" class="required" />
                                </div>
                                <div class="clear-fix"></div>
                                <div class="bottom_field_form">
                                    <input type="hidden" name="mode" value="tambah-auditor"/>
                                    <input type="hidden" name="bidang" value="{$d.ID_SPI_PEMERIKSAAN_BIDANG}" />
                                    <a style="cursor: pointer" onclick="$('#tambah-auditor-{$d.ID_SPI_PEMERIKSAAN_BIDANG}').fadeOut();"  class="disable-ajax button">Batal</a>
                                    <input type="submit" class="button" value="Tambah"/>
                                </div>

                            </fieldset>
                        </form>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</fieldset>
</form>
{else}
    <div class="center_title_bar">Daftar Rencana Pemeriksaan</div>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            oTable = $('#datatable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <a href="p-rencana.php?mode=tambah" class="button">Tambah Rencana</a>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
        <thead>
            <tr>
                <th>No.RPP</th>
                <th>Unit Kerja</th>
                <th>Pelaksanaan</th>
                <th>Tahun Pemeriksaan</th>
                <th>Bidang Pemeriksaan</th>
                <th class="center">-----</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_pemeriksaan as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td>{$d.NOMOR_RPP}</td>
                    <td>{$d.NM_UNIT_KERJA}</td>
                    <td>{$d.TGL_AWAL} - {$d.TGL_AKHIR}</td>
                    <td class="center">{$d.TAHUN_PEMERIKSAAN}</td>
                    <td>
                        <ul style="margin-left: 10px;font-size: 0.9em">
                            {foreach $d.DATA_BIDANG as $db}
                                <li>{$db.NAMA_BIDANG}</li>
                                {/foreach}
                        </ul>
                    </td>
                    <td class="center">
                        <a href="p-rencana.php?mode=edit&id={$d.ID_SPI_PEMERIKSAAN}" class="button">Edit</a>
                        <a href="p-rencana.php?mode=hapus&id={$d.ID_SPI_PEMERIKSAAN}" class="button">Hapus</a>
                        <a href="p-rencana.php?mode=bidang&id={$d.ID_SPI_PEMERIKSAAN}" class="button">Bidang</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="spacer"></div>
{/if}
{literal}
    <script>
        $('form').validate();
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-M-y'
        }).css({'text-transform': 'uppercase'});
    </script>
{/literal}