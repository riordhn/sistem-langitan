{if $smarty.get.mode=='laporan'}
    <div class="center_title_bar">Detail Rencana Pemeriksaan Penugasan</div>
    <form action="lakip-program.php" method="post">
        <fieldset style="width: 98%">
            <legend>Data Rencana Pemeriksaan Penugasan</legend>
            <div class="field_form">
                <label>Nomor RPP : </label>
                <input type="text" size="20" maxlength="20" readonly="" name="rpp" class="required" value="{$pemeriksaan.NOMOR_RPP}"/>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Unit Kerja : </label>
                {foreach $data_unit as $d}
                    {if $d.ID_UNIT_KERJA==$pemeriksaan.ID_UNIT_KERJA} {$d.NM_UNIT_KERJA} {if $d.DESKRIPSI_UNIT_KERJA!=''}- {$d.DESKRIPSI_UNIT_KERJA}{/if} {/if}
                {/foreach}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label style="width: 98%;font-size: 1.2em;color: #ff6600;text-align: left;">-- Pelaksanaan --</label>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Mulai : </label>
                {$pemeriksaan.TGL_AWAL}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Selesai : </label>
                {$pemeriksaan.TGL_AKHIR}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Tahun Anggaran : </label>
                {$pemeriksaan.TAHUN_PEMERIKSAAN}
            </div>

        </fieldset>
    </form>
    <a class="button" href="p-laporan-hasil.php">Kembali</a>
    <div class="field_form">
        <label style="width: 98%;font-size: 1.2em;color: #ff6600;text-align: left;">-- Bidang Pemeriksaan --</label>
    </div>
    <div class="clear-fix"></div>
    <table cellpadding="0" cellspacing="0" border="0" class="display">
        <thead>
            <tr>
                <th>Bidang</th>
                <th>Daftar Auditor</th>
                <th class="center">-----</th>
            </tr>
        </thead>
        <tbody>
            {foreach $pemeriksaan.DATA_BIDANG as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td>{$d.NAMA_BIDANG}</td>
                    <td>
                        <ul style="margin-left: 10px;font-size: 0.9em">
                            {foreach $d.DATA_AUDITOR as $au}
                                <li>
                                    {$au.GELAR_DEPAN} {$au.NM_PENGGUNA} {$au.GELAR_BELAKANG} ({$au.JABATAN})
                                </li>
                            {/foreach}
                        </ul>
                    </td>
                    <td class="center">
                        <input onclick="window.open('p-laporan-hasil.php?mode=input-laporan&bidang={$d.ID_SPI_BIDANG}&pemeriksaan={$d.ID_SPI_PEMERIKSAAN}&pemeriksaan_bidang={$d.ID_SPI_PEMERIKSAAN_BIDANG}', '_blank')" type="button" title="Masukkan Laporan" class="ui-state-default ui-corner-all ui-icon ui-icon-note"  style="display:inline-block ;padding: 8px;margin: 3px;cursor: pointer;vertical-align: top;"/>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    {literal}
        <script>
            $('form').validate();
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-M-y'
            }).css({'text-transform': 'uppercase'});
        </script>
    {/literal}
{else if $smarty.get.mode==''}
    <div class="center_title_bar">Daftar Rencana Pemeriksaan</div>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            oTable = $('#datatable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
        <thead>
            <tr>
                <th>No.RPP</th>
                <th>Unit Kerja</th>
                <th>Pelaksanaan</th>
                <th>Tahun Pemeriksaan</th>
                <th>Bidang Pemeriksaan</th>
                <th class="center">-----</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_pemeriksaan as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td>{$d.NOMOR_RPP}</td>
                    <td>{$d.NM_UNIT_KERJA}</td>
                    <td>{$d.TGL_AWAL} - {$d.TGL_AKHIR}</td>
                    <td class="center">{$d.TAHUN_PEMERIKSAAN}</td>
                    <td>
                        <ul style="margin-left: 10px;font-size: 0.9em">
                            {foreach $d.DATA_BIDANG as $db}
                                <li>{$db.NAMA_BIDANG}</li>
                                {/foreach}
                        </ul>
                    </td>
                    <td class="center">
                        <a href="p-laporan-hasil.php?mode=laporan&id={$d.ID_SPI_PEMERIKSAAN}" class="button">Laporan</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="spacer"></div>
    {literal}
        <script>
            $('form').validate();
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-M-y'
            }).css({'text-transform': 'uppercase'});
        </script>
    {/literal}
{else if $smarty.get.mode=='input-laporan'}
    <html>
        <head>
            <title>SPI Universitas Airlangga</title>

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" type="text/css" href="../../css/reset.css" />
            <link rel="stylesheet" type="text/css" href="../../css/text.css" />
            <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Open+Sans:300italic,400,300,700" rel="stylesheet" type="text/css">
            <link rel="stylesheet" type="text/css" href="css/semantic.css" media="all" />
            <link rel="stylesheet" type="text/css" href="css/spi.css" />
            <link rel="stylesheet" type="text/css" href="../../css/custom-theme/jquery-ui-smoothness.css" />
            <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css" />
            <link rel="stylesheet" type="text/css" href="css/chosen.css">

            <style type="text/css" >
                /* fix rtl for demo */
                .chosen-rtl .chosen-drop { left: -9000px; }
            </style>

            <script type="text/javascript" src="../../js/jquery-1.7.2.min.js"></script>
            <script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
            <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
            <script type="text/javascript" src="../../js/additional-methods.min.js"></script>
            <script type="text/javascript" src="../../js/jquery.price_format.1.4.js"></script>
            <script type="text/javascript" src="js/semantic.min.js" ></script>
            <script type="text/javascript" src="js/jquery.dataTables.js" ></script>
            <script type="text/javascript" src="js/chosen.jquery.min.js" ></script>
        </head>
        <body>
            <div class="content" id="content" style="padding: 20px">
                {literal}
                    <script type="text/javascript">
                        $(document).ready(function() {

                            oTable = $('#datatable').dataTable({
                                "bJQueryUI": true,
                                "sPaginationType": "full_numbers"
                            });
                            $('.chosen').chosen();
                            $('#input-laporan').validate();
                            $(".datepicker").datepicker({
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: 'dd-M-y'
                            }).css({'text-transform': 'uppercase'});
                        });

                    </script>
                {/literal}
                <div class="center_title_bar">Form Pengisian Laporan Bidang Pemeriksaan</div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <div class="ui purple ribbon label">BIDANG KEGIATAN : {$bidang.NAMA_BIDANG}</div>
                </div>

                <form id="input-laporan" action="p-laporan-hasil.php?{$smarty.server.QUERY_STRING}" method="post">
                    <fieldset style="width: 98%">
                        <legend>Detail Hasil Laporan Pemeriksaan</legend>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>No RPP : </label>
                            <input type="text" size="20" maxlength="20" readonly="" name="rpp" class="required" value="{$pemeriksaan.NOMOR_RPP}"/>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Unit Kerja : </label>
                            {$pemeriksaan.NM_UNIT_KERJA} {if $pemeriksaan.DESKRIPSI_UNIT_KERJA!=''&&$pemeriksaan.NM_UNIT_KERJA!=$pemeriksaan.DESKRIPSI_UNIT_KERJA}({$pemeriksaan.DESKRIPSI_UNIT_KERJA}){/if}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Nomor Laporan : </label>
                            <input type="text" size="20" maxlength="20" name="no_laporan" class="required" value=""/>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Surat Tugas : </label>
                            <input type="text" size="20" maxlength="20" name="surat_tugas" class="required" value=""/>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Periode Pemeriksaan : </label>
                            {$pemeriksaan.TGL_AWAL} -  {$pemeriksaan.TGL_AKHIR}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Tahun Anggaran : </label>
                            {$pemeriksaan.TAHUN_PEMERIKSAAN}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Auditor : </label>
                            <div style="width: 60%;height: auto;display: inline-block">
                                {foreach $data_auditor as $da}
                                    {$da@index+1}. {$da.GELAR_DEPAN}{$da.NM_PENGGUNA}{$da.GELAR_BELAKANG} ({$da.JABATAN})<br/>
                                {/foreach}
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Temuan: </label>
                            <select name="temuan"  class="chosen required" style="width: 70%" data-placeholder="Pilih Temuan">
                                <option value="">Pilih Temuan</option>
                                {foreach $data_temuan as $d}
                                    <option value="{$d.ID_SPI_TEMUAN_ISI}">Kode Temuan : {$d.KODE_ISI}, Isi : {$d.ISI}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="bottom_field_form">
                            <input type="hidden" name="pemeriksaan_bidang" value="{$smarty.get.pemeriksaan_bidang}"/>
                            <input type="hidden" name="mode" value="tambah-laporan"/>
                            <input type="submit" class="button" value="Tambah Laporan Baru"/>
                            <input type="button" class="button" value="Kembali" onclick="javascript:window.close();"/>
                        </div>
                    </fieldset>
                </form>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <div class="ui purple ribbon label">DAFTAR LAPORAN </div>
                </div>
                <div class="clear-fix"></div>
                <table cellpadding="0" cellspacing="0" border="0" id="datatable" class="display">
                    <thead>
                        <tr>
                            <th>No Laporan</th>
                            <th>No Surat Tugas</th>
                            <th>Temuan</th>
                            <th>Judul Temuan</th>
                            <th>Status</th>
                            <th class="center">-----</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $data_laporan as $d}
                            <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                                <td>{$d.NOMOR_LAPORAN}</td>
                                <td>{$d.NOMOR_SURAT_TUGAS}</td>
                                <td>(Kode : {$d.KODE_ISI}) {$d.ISI}</td>
                                <td>{$d.JUDUL_TEMUAN}</td>
                                <td class="center">
                                    {if $d.STATUS==0}
                                        <span class="ui red label">BELUM DISETUJUI</span>
                                    {else $d.STATUS==1}
                                        <span class="ui green label">SUDAH DISETUJUI</span><br/>
                                        {$d.VERIFIKATOR} ({$d.TGL_PERSETUJUAN})
                                    {/if}
                                </td>                                
                                <td class="center" style="width: 150px">
                                    {if $d.STATUS==0}
                                        <button title="Isi/Edit Laporan" onclick="window.location.href = 'p-laporan-hasil.php?{$smarty.server.QUERY_STRING|replace:'input':'update'}&laporan={$d.ID_SPI_PEMERIKSAAN_LAPORAN}'" class="ui-state-default ui-corner-all ui-icon ui-icon-note"  style="display:inline-block ;padding: 8px;margin: 3px;cursor: pointer;vertical-align: top;"></button>
                                        <button title="Approve Laporan" onclick="window.location.href = 'p-laporan-hasil.php?{$smarty.server.QUERY_STRING|replace:'input':'approve'}&laporan={$d.ID_SPI_PEMERIKSAAN_LAPORAN}'" class="ui-state-default ui-corner-all ui-icon ui-icon-check"  style="display:inline-block ;padding: 8px;margin: 3px;cursor: pointer;vertical-align: top;"></button>
                                        <form action="p-laporan-hasil.php?{$smarty.server.QUERY_STRING}" method="post" style="display: inline;" >
                                            <input type="hidden" name="mode" value="hapus-laporan"/>
                                            <input type="hidden" name="laporan" value="{$d.ID_SPI_PEMERIKSAAN_LAPORAN}"/>
                                            <input type="submit" onclick="{literal}if (!confirm('Apakah anda yakin menghapus ini?')) {
                                                        return false;
                                                    }{/literal}" class="ui-state-default ui-corner-all ui-icon ui-icon-closethick" style="display:inline ;padding: 8px;margin: 3px;cursor: pointer;vertical-align: middle"/>
                                        </form>
                                    {else $d.STATUS==1}
                                        <button title="Approve Laporan" onclick="window.location.href = 'p-laporan-hasil.php?{$smarty.server.QUERY_STRING|replace:'input':'approve'}&laporan={$d.ID_SPI_PEMERIKSAAN_LAPORAN}'" class="ui-state-default ui-corner-all ui-icon ui-icon-check"  style="display:inline-block ;padding: 8px;margin: 3px;cursor: pointer;vertical-align: top;"></button>
                                        <button title="View Laporan" onclick="window.location.href = 'p-laporan-hasil.php?{$smarty.server.QUERY_STRING|replace:'input':'view'}&laporan={$d.ID_SPI_PEMERIKSAAN_LAPORAN}'" class="ui-state-default ui-corner-all ui-icon ui-icon-zoomin"  style="display:inline-block ;padding: 8px;margin: 3px;cursor: pointer;vertical-align: top;"></button>
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>

            </div>
        </body>
    </html>
{else if $smarty.get.mode=='update-laporan'}
    <html>
        <head>
            <title>SPI Universitas Airlangga</title>

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" type="text/css" href="../../css/reset.css" />
            <link rel="stylesheet" type="text/css" href="../../css/text.css" />
            <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Open+Sans:300italic,400,300,700" rel="stylesheet" type="text/css">
            <link rel="stylesheet" type="text/css" href="css/semantic.css" media="all" />
            <link rel="stylesheet" type="text/css" href="css/spi.css" />
            <link rel="stylesheet" type="text/css" href="../../css/custom-theme/jquery-ui-smoothness.css" />
            <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css" />
            <link rel="stylesheet" type="text/css" href="css/chosen.css">

            <style type="text/css" >
                /* fix rtl for demo */
                .chosen-rtl .chosen-drop { left: -9000px; }
            </style>

            <script type="text/javascript" src="../../js/jquery-1.7.2.min.js"></script>
            <script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
            <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
            <script type="text/javascript" src="../../js/additional-methods.min.js"></script>
            <script type="text/javascript" src="../../js/jquery.price_format.1.4.js"></script>
            <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
            <script type="text/javascript" src="js/semantic.min.js" ></script>
            <script type="text/javascript" src="js/jquery.dataTables.js" ></script>
            <script type="text/javascript" src="js/chosen.jquery.min.js" ></script>
        </head>
        <body>
            <div class="content" id="content" style="padding: 20px">
                {literal}
                    <script>
                        $(document).ready(function() {

                            oTable = $('#datatable').dataTable({
                                "bJQueryUI": true,
                                "sPaginationType": "full_numbers"
                            });
                            $('form').validate();
                            
                            $('.chosen').chosen();

                            $(".datepicker").datepicker({
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: 'dd-M-y'
                            }).css({'text-transform': 'uppercase'});

                            tinymce.init({
                                selector: ".editor",
                                width: '100%',
                                height: 300,
                                plugins: [
                                    "advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker",
                                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                                    "save table contextmenu directionality template paste textcolor"
                                ],
                            });
                        });

                    </script>
                {/literal}
                <div class="center_title_bar">Form Update Laporan Bidang Pemeriksaan</div>
                <form action="p-laporan-hasil.php?mode=input-laporan&bidang={$smarty.get.bidang}&pemeriksaan={$pemeriksaan.ID_SPI_PEMERIKSAAN}&pemeriksaan_bidang={$laporan.ID_SPI_PEMERIKSAAN_BIDANG}" method="post">
                    <fieldset style="width: 98%">
                        <legend>Detail Hasil Laporan Pemeriksaan</legend>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>No RPP : </label>
                            <input type="text" size="20" maxlength="20" readonly="" name="rpp" class="required" value="{$pemeriksaan.NOMOR_RPP}"/>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Unit Kerja : </label>
                            {$pemeriksaan.NM_UNIT_KERJA} {if $pemeriksaan.DESKRIPSI_UNIT_KERJA!=''&&$pemeriksaan.NM_UNIT_KERJA!=$pemeriksaan.DESKRIPSI_UNIT_KERJA}({$pemeriksaan.DESKRIPSI_UNIT_KERJA}){/if}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Nomor Laporan : </label>
                            <input type="text" size="20" maxlength="20" name="no_laporan" class="required" value="{$laporan.NOMOR_LAPORAN}"/>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Surat Tugas : </label>
                            <input type="text" size="20" maxlength="20" name="surat_tugas" class="required" value="{$laporan.NOMOR_SURAT_TUGAS}"/>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Periode Pemeriksaan : </label>
                            {$pemeriksaan.TGL_AWAL} -  {$pemeriksaan.TGL_AKHIR}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Tahun Anggaran : </label>
                            {$pemeriksaan.TAHUN_PEMERIKSAAN}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Auditor : </label>
                            <div style="width: 60%;height: auto;display: inline-block">
                                {foreach $data_auditor as $da}
                                    {$da@index+1}. {$da.GELAR_DEPAN}{$da.NM_PENGGUNA}{$da.GELAR_BELAKANG} ({$da.JABATAN})<br/>
                                {/foreach}
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Temuan: </label>
                            <select name="temuan"  class="chosen required" style="width: 70%">
                                <option value="">Pilih Temuan</option>
                                {foreach $data_temuan as $d}
                                    <option value="{$d.ID_SPI_TEMUAN_ISI}" {if $d.ID_SPI_TEMUAN_ISI==$laporan.ID_SPI_TEMUAN_ISI}selected="true"{/if}>Kode Temuan : {$d.KODE_ISI}, Isi : {$d.ISI}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <div class="ui teal ribbon label">BIDANG KEGIATAN : {$bidang.NAMA_BIDANG}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Judul Temuan: </label>
                            <textarea cols="180" rows="30" maxlength="1000" class="editor" name="judul">{$laporan.JUDUL_TEMUAN}</textarea>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Kriteria: </label>
                            <textarea cols="180" rows="30" maxlength="1000" class="editor" name="kriteria">{$laporan.KRITERIA}</textarea>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Sebab: </label>
                            <textarea cols="180" rows="30" maxlength="1000" class="editor" name="sebab">{$laporan.SEBAB}</textarea>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Akibat: </label>
                            <textarea cols="180" rows="30" maxlength="1000" class="editor" name="akibat">{$laporan.AKIBAT}</textarea>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Rekomendasi: </label>
                            <textarea cols="180" rows="30" maxlength="1000" class="editor" name="rekomendasi">{$laporan.REKOMENDASI}</textarea>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Tanggapan Auditi: </label>
                            <textarea cols="180" rows="30" maxlength="1000" class="editor" name="tanggapan">{$laporan.TANGGAPAN}</textarea>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Rencana Tindak Lanjut: </label>
                            <textarea cols="180" rows="30" maxlength="1000" class="editor" name="rencana">{$laporan.RENCANA}</textarea>
                        </div>
                        <div class="bottom_field_form">
                            <input type="hidden" name="laporan" value="{$smarty.get.laporan}"/>
                            <input type="hidden" name="pemeriksaan_bidang" value="{$smarty.get.pemeriksaan_bidang}"/>
                            <input type="hidden" name="mode" value="update-laporan"/>
                            <input type="submit" class="button" value="Update Laporan"/>
                        </div>
                    </fieldset>
                </form>
                <a class="button" href="p-laporan-hasil.php?mode=input-laporan&bidang={$smarty.get.bidang}&pemeriksaan={$pemeriksaan.ID_SPI_PEMERIKSAAN}&pemeriksaan_bidang={$laporan.ID_SPI_PEMERIKSAAN_BIDANG}">Kembali</a>
            </div>
        </body>
    </html>
{else if $smarty.get.mode=='approve-laporan'}
    <html>
        <head>
            <title>SPI Universitas Airlangga</title>

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" type="text/css" href="../../css/reset.css" />
            <link rel="stylesheet" type="text/css" href="../../css/text.css" />
            <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Open+Sans:300italic,400,300,700" rel="stylesheet" type="text/css">
            <link rel="stylesheet" type="text/css" href="css/semantic.css" media="all" />
            <link rel="stylesheet" type="text/css" href="css/spi.css" />
            <link rel="stylesheet" type="text/css" href="../../css/custom-theme/jquery-ui-smoothness.css" />
            <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css" />
            <link rel="stylesheet" type="text/css" href="css/chosen.css">

            <style type="text/css" >
                /* fix rtl for demo */
                .chosen-rtl .chosen-drop { left: -9000px; }
            </style>

            <script type="text/javascript" src="../../js/jquery-1.7.2.min.js"></script>
            <script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
            <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
            <script type="text/javascript" src="../../js/additional-methods.min.js"></script>
            <script type="text/javascript" src="../../js/jquery.price_format.1.4.js"></script>
            <script type="text/javascript" src="../../api/plugins/ckeditor/ckeditor.js"></script>
            <script type="text/javascript" src="../../api/plugins/ckeditor/adapters/jquery.js"></script>
            <script type="text/javascript" src="js/semantic.min.js" ></script>
            <script type="text/javascript" src="js/jquery.dataTables.js" ></script>
            <script type="text/javascript" src="js/chosen.jquery.min.js" ></script>
        </head>
        <body>
            <div class="content" id="content" style="padding: 20px">
                {literal}
                    <script>
                        $(document).ready(function() {

                            oTable = $('#datatable').dataTable({
                                "bJQueryUI": true,
                                "sPaginationType": "full_numbers"
                            });
                            $('form').validate();

                            $(".datepicker").datepicker({
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: 'dd-M-y'
                            }).css({'text-transform': 'uppercase'});

                            $('.editor').ckeditor(function(e) {
                                delete CKEDITOR.instances[$(e).attr('name')];
                            }, {
                                toolbar:
                                        [
                                            ['TextColor', 'Style', 'Font', 'FontSize', 'Bold', 'Italic', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Underline', 'Strike', '-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', '-']
                                        ],
                                skin: 'office2003'
                            }
                            );
                        });

                    </script>
                {/literal}
                <div class="center_title_bar">Form Update Laporan Bidang Pemeriksaan</div>
                <form action="p-laporan-hasil.php?mode=input-laporan&bidang={$smarty.get.bidang}&pemeriksaan={$pemeriksaan.ID_SPI_PEMERIKSAAN}&pemeriksaan_bidang={$laporan.ID_SPI_PEMERIKSAAN_BIDANG}" method="post">
                    <fieldset style="width: 98%">
                        <legend>Detail Hasil Laporan Pemeriksaan</legend>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>No RPP : </label>
                            <input type="text" size="20" maxlength="20" readonly="" name="rpp" class="required" value="{$pemeriksaan.NOMOR_RPP}"/>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Unit Kerja : </label>
                            {$pemeriksaan.NM_UNIT_KERJA} {if $pemeriksaan.DESKRIPSI_UNIT_KERJA!=''&&$pemeriksaan.NM_UNIT_KERJA!=$pemeriksaan.DESKRIPSI_UNIT_KERJA}({$pemeriksaan.DESKRIPSI_UNIT_KERJA}){/if}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Nomor Laporan : </label>
                            {$laporan.NOMOR_LAPORAN}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Surat Tugas : </label>
                            {$laporan.NOMOR_SURAT_TUGAS}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Periode Pemeriksaan : </label>
                            {$pemeriksaan.TGL_AWAL} -  {$pemeriksaan.TGL_AKHIR}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Tahun Anggaran : </label>
                            {$pemeriksaan.TAHUN_PEMERIKSAAN}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Auditor : </label>
                            <div style="width: 60%;height: auto;display: inline-block">
                                {foreach $data_auditor as $da}
                                    {$da@index+1}. {$da.GELAR_DEPAN}{$da.NM_PENGGUNA}{$da.GELAR_BELAKANG} ({$da.JABATAN})<br/>
                                {/foreach}
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Temuan: </label>
                            ({$laporan.KODE_ISI}) {$laporan.ISI}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <div class="ui teal ribbon label">BIDANG KEGIATAN : {$bidang.NAMA_BIDANG}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Judul Temuan: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.JUDUL_TEMUAN}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Kriteria: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.KRITERIA}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Sebab: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.SEBAB}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Akibat: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.AKIBAT}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Rekomendasi: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.REKOMENDASI}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Tanggapan Auditi: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.TANGGAPAN}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Rencana Tindak Lanjut: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.RENCANA}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <hr/>
                        <div class="clear-fix"></div>
                        <div class="bottom_field_form">
                            <div>
                                Surabaya {date('d-m-Y')}<br/>
                                Ketua Satuan Pengawas Intern <br/>
                                <br/><br/>
                                <span style="text-align: center">
                                    <input name="approve" type="checkbox" value="1" {if $laporan.STATUS==1}checked="true"{/if} />
                                </span>
                                <br/><br/><br/>
                                <input type="hidden" name="laporan" value="{$smarty.get.laporan}"/>
                                <input type="hidden" name="pemeriksaan_bidang" value="{$smarty.get.pemeriksaan_bidang}"/>
                                <input type="hidden" name="mode" value="approve-laporan"/>
                                {$nama_user}<br/>
                                NIP : {$nip}
                            </div>
                            <input type="submit" class="button" value="{if $laporan.STATUS==0}Approve {else}Batal Approve{/if} Laporan"/>
                        </div>
                    </fieldset>
                </form>
                <a class="button" href="p-laporan-hasil.php?mode=input-laporan&bidang={$smarty.get.bidang}&pemeriksaan={$pemeriksaan.ID_SPI_PEMERIKSAAN}&pemeriksaan_bidang={$laporan.ID_SPI_PEMERIKSAAN_BIDANG}">Kembali</a>
            </div>
        </body>
    </html>
{else if $smarty.get.mode=='view-laporan'}
    <html>
        <head>
            <title>SPI Universitas Airlangga</title>

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" type="text/css" href="../../css/reset.css" />
            <link rel="stylesheet" type="text/css" href="../../css/text.css" />
            <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Open+Sans:300italic,400,300,700" rel="stylesheet" type="text/css">
            <link rel="stylesheet" type="text/css" href="css/semantic.css" media="all" />
            <link rel="stylesheet" type="text/css" href="css/spi.css" />
            <link rel="stylesheet" type="text/css" href="../../css/custom-theme/jquery-ui-smoothness.css" />
            <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css" />
            <link rel="stylesheet" type="text/css" href="css/chosen.css">

            <style type="text/css" >
                /* fix rtl for demo */
                .chosen-rtl .chosen-drop { left: -9000px; }
            </style>

            <script type="text/javascript" src="../../js/jquery-1.7.2.min.js"></script>
            <script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
            <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
            <script type="text/javascript" src="../../js/additional-methods.min.js"></script>
            <script type="text/javascript" src="../../js/jquery.price_format.1.4.js"></script>
            <script type="text/javascript" src="../../api/plugins/ckeditor/ckeditor.js"></script>
            <script type="text/javascript" src="../../api/plugins/ckeditor/adapters/jquery.js"></script>
            <script type="text/javascript" src="js/semantic.min.js" ></script>
            <script type="text/javascript" src="js/jquery.dataTables.js" ></script>
            <script type="text/javascript" src="js/chosen.jquery.min.js" ></script>
        </head>
        <body>
            <div class="content" id="content" style="padding: 20px">
                {literal}
                    <script>
                        $(document).ready(function() {

                            oTable = $('#datatable').dataTable({
                                "bJQueryUI": true,
                                "sPaginationType": "full_numbers"
                            });
                            $('form').validate();

                            $(".datepicker").datepicker({
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: 'dd-M-y'
                            }).css({'text-transform': 'uppercase'});

                            $('.editor').ckeditor(function(e) {
                                delete CKEDITOR.instances[$(e).attr('name')];
                            }, {
                                toolbar:
                                        [
                                            ['TextColor', 'Style', 'Font', 'FontSize', 'Bold', 'Italic', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Underline', 'Strike', '-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', '-']
                                        ],
                                skin: 'office2003'
                            }
                            );
                        });

                    </script>
                {/literal}
                <div class="center_title_bar">Form Update Laporan Bidang Pemeriksaan</div>
                <form action="p-laporan-hasil.php?mode=input-laporan&bidang={$smarty.get.bidang}&pemeriksaan={$pemeriksaan.ID_SPI_PEMERIKSAAN}&pemeriksaan_bidang={$laporan.ID_SPI_PEMERIKSAAN_BIDANG}" method="post">
                    <fieldset style="width: 98%">
                        <legend>Detail Hasil Laporan Pemeriksaan</legend>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>No RPP : </label>
                            <input type="text" size="20" maxlength="20" readonly="" name="rpp" class="required" value="{$pemeriksaan.NOMOR_RPP}"/>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Unit Kerja : </label>
                            {$pemeriksaan.NM_UNIT_KERJA} {if $pemeriksaan.DESKRIPSI_UNIT_KERJA!=''&&$pemeriksaan.NM_UNIT_KERJA!=$pemeriksaan.DESKRIPSI_UNIT_KERJA}({$pemeriksaan.DESKRIPSI_UNIT_KERJA}){/if}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Nomor Laporan : </label>
                            {$laporan.NOMOR_LAPORAN}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Surat Tugas : </label>
                            {$laporan.NOMOR_SURAT_TUGAS}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Periode Pemeriksaan : </label>
                            {$pemeriksaan.TGL_AWAL} -  {$pemeriksaan.TGL_AKHIR}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Tahun Anggaran : </label>
                            {$pemeriksaan.TAHUN_PEMERIKSAAN}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Auditor : </label>
                            <div style="width: 60%;height: auto;display: inline-block">
                                {foreach $data_auditor as $da}
                                    {$da@index+1}. {$da.GELAR_DEPAN}{$da.NM_PENGGUNA}{$da.GELAR_BELAKANG} ({$da.JABATAN})<br/>
                                {/foreach}
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Temuan: </label>
                            ({$laporan.KODE_ISI}) {$laporan.ISI}
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <div class="ui teal ribbon label">BIDANG KEGIATAN : {$bidang.NAMA_BIDANG}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Judul Temuan: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.JUDUL_TEMUAN}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Kriteria: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.KRITERIA}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Sebab: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.SEBAB}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Akibat: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.AKIBAT}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Rekomendasi: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.REKOMENDASI}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Tanggapan Auditi: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.TANGGAPAN}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="field_form">
                            <label>Rencana Tindak Lanjut: </label>
                            <div style="width: 60%;height: auto;display: inline-block">{$laporan.RENCANA}</div>
                        </div>
                        <div class="clear-fix"></div>
                        <hr/>
                        <div class="clear-fix"></div>
                        <div class="bottom_field_form">
                            <div>
                                Surabaya {date('d-m-Y')}<br/>
                                Ketua Satuan Pengawas Intern <br/>
                                <br/><br/>
                                <span class="ui green label" style="text-align: center">
                                    SUDAH DISETUJUI
                                </span>
                                <br/><br/><br/>
                                <input type="hidden" name="laporan" value="{$smarty.get.laporan}"/>
                                <input type="hidden" name="pemeriksaan_bidang" value="{$smarty.get.pemeriksaan_bidang}"/>
                                <input type="hidden" name="mode" value="approve-laporan"/>
                                {$laporan.VERIFIKATOR}<br/>
                                NIP : {$laporan.NIP}
                            </div>
                        </div>
                    </fieldset>
                </form>
                <a class="button" href="p-laporan-hasil.php?mode=input-laporan&bidang={$smarty.get.bidang}&pemeriksaan={$pemeriksaan.ID_SPI_PEMERIKSAAN}&pemeriksaan_bidang={$laporan.ID_SPI_PEMERIKSAAN_BIDANG}">Kembali</a>
            </div>
        </body>
    </html>
{/if}
