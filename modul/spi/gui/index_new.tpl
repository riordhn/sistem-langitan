<html>
    <head>
        <title>SPI Universitas Airlangga</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Open+Sans:300italic,400,300,700" rel="stylesheet" type="text/css">

        <link rel="stylesheet" type="text/css" href="../../css/reset.css" />
        <link rel="stylesheet" type="text/css" href="../../css/text.css" />
        <link rel="stylesheet" type="text/css" href="css/semantic.css" media="all" />
        <link rel="stylesheet" type="text/css" href="css/spi.css" />
        <link rel="stylesheet" type="text/css" href="../../css/custom-theme/jquery-ui-smoothness.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css" />
        <link rel="shortcut icon" href="../../img/icon.ico" />
        <link rel="stylesheet" type="text/css" href="css/chosen.css">
        <style type="text/css" >
            /* fix rtl for demo */
            .chosen-rtl .chosen-drop { left: -9000px; }
        </style>

        <script type="text/javascript" src="../../js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="../../js/additional-methods.min.js"></script>
        <script type="text/javascript" src="../../js/jquery.price_format.1.4.js"></script>
        <script type="text/javascript" src="../../js/cybercampus.ajax-1.0.js"></script>
        <script type="text/javascript" src="js/semantic.min.js" ></script>
        <script type="text/javascript" src="js/jquery.dataTables.js" ></script>
        <script type="text/javascript" src="js/chosen.jquery.min.js" ></script>

    </head>
    <body>
        <div class="ui grid">
            <div class="sixteen wide column" style="background-color: #104090;margin-top: 0px;padding: 0px">
                <div class="ui segment" style="background-color: #104090;margin-bottom: 0px">
                    <div class="header-center"></div>
                </div>
                <div class="ui inverted menu small" style="margin-top: 0px">
                    <div class="menu">
                        {foreach $modul_set as $m}
                            {if $m.AKSES == 1}
                                <a href="#{$m.NM_MODUL}!{$m.PAGE}" class="item">
                                    {$m.TITLE}
                                </a>
                            {/if}
                        {/foreach}
                        <a href="../../logout.php" class="item disable-ajax">
                            <i class="sign out icon"></i> Logout
                        </a>
                    </div>
                </div>
            </div>
        </div>


        <div class="ui grid">
            <div class="three wide column">
                <div class="ui vertical menu small" style="font-size: 0.8em;margin-right: 10px;width: 100%">
                    <div class="header item">
                        <i class="user icon"></i>
                        Submenu
                    </div>
                    <div class="menu" id="menu">
                    </div>
                </div>
            </div>
            <div class="thirteen wide column">
                <div class="ui segment">
                    <div class="ui breadcrumb" id="breadcrumbs">

                    </div>
                    <div class="content" id="content">

                    </div>
                </div>
            </div>
        </div>
        <div class="foot-center fixed bottom">
            <div class="footer-nav">
                <a href="">Home</a> | <a href="">About</a> | <a href="">Sitemap</a> | <a href="">RSS</a> | <a href="">Contact Us</a>
            </div>
            <div class="footer">Copyright &copy; 2011 - Universitas Airlangga <br/>All Rights Reserved</div>
        </div>
        <script type="text/javascript" src="js/custom.js" ></script>
    </body>
</html>