<?php

require 'config.php';
include 'class/data.class.php';

$d = new data($db);

if (!empty($_POST)) {
    if (post('mode') == 'tambah') {
        $d->tambahDataJenisAturan($_POST);
    } else if (post('mode') == 'edit') {
        $d->updateDataJenisAturan($_POST);
    } else if (post('mode') == 'hapus') {
        $d->hapusDataJenisAturan(post('id'));
    }
}

if (!empty($_GET['mode'])) {

    if (get('mode') == 'edit' || get('mode') == 'hapus') {
        $smarty->assign('jenis_aturan',$d->getDataJenisAturan(get('id')));
    }
}

$data_jenis_aturan = $d->loadDataJenisAturan();
$smarty->assign('data_jenis_aturan',$data_jenis_aturan);
$smarty->display("data/d-jenis-aturan.tpl");
?>
