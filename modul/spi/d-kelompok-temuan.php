<?php

require 'config.php';
include 'class/data.class.php';

$d = new data($db);

if (!empty($_POST)) {
    if (post('mode') == 'tambah') {
        $d->tambahDataTemuanKelompok($_POST);
    } else if (post('mode') == 'edit') {
        $d->updateDataTemuanKelompok($_POST);
    } else if (post('mode') == 'hapus') {
        $d->hapusDataTemuanKelompok(post('id'));
    }
}

if (!empty($_GET['mode'])) {

    if (get('mode') == 'edit' || get('mode') == 'hapus') {
        $smarty->assign('kelompok',$d->getDataTemuanKelompok(get('id')));
    }
}

$data_kelompok = $d->loadDataTemuanKelompok();
$smarty->assign('data_kelompok',$data_kelompok);
$smarty->display("data/d-kelompok-temuan.tpl");
?>
