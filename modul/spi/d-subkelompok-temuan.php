<?php

require 'config.php';
include 'class/data.class.php';

$d = new data($db);

if (!empty($_POST)) {
    if (post('mode') == 'tambah') {
        $d->tambahDataTemuanSubKelompok($_POST);
    } else if (post('mode') == 'edit') {
        $d->updateDataTemuanSubKelompok($_POST);
    } else if (post('mode') == 'hapus') {
        $d->hapusDataTemuanSubKelompok(post('id'));
    }
}

if (!empty($_GET['mode'])) {

    if (get('mode') == 'edit' || get('mode') == 'hapus') {
        $smarty->assign('subkelompok',$d->getDataTemuanSubKelompok(get('id')));
    }
}

$kelompok=$d->getDataTemuanKelompok(get('kelompok'));
$data_subkelompok = $d->loadDataTemuanSubKelompokByKelompok(get('kelompok'));
$smarty->assign('kelompok',$kelompok);
$smarty->assign('data_subkelompok',$data_subkelompok);
$smarty->display("data/d-subkelompok-temuan.tpl");
?>
