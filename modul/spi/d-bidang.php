<?php

require 'config.php';
include 'class/data.class.php';

$d = new data($db);

if (!empty($_POST)) {
    if (post('mode') == 'tambah') {
        $d->tambahDataBidang($_POST);
    } else if (post('mode') == 'edit') {
        $d->updateDataBidang($_POST);
    } else if (post('mode') == 'hapus') {
        $d->hapusDataBidang(post('id'));
    }
}

if (!empty($_GET['mode'])) {

    if (get('mode') == 'edit' || get('mode') == 'hapus') {
        $smarty->assign('bidang',$d->getDataBidang(get('id')));
    }
}

$data_bidang = $d->loadDataBidang();
$smarty->assign('data_bidang',$data_bidang);
$smarty->display("data/d-bidang.tpl");
?>
