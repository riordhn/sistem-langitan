<?php

require 'config.php';
require 'class/pemeriksaan.class.php';
require 'class/data.class.php';

$p = new pemeriksaan($db);
$d = new data($db);


if (!empty($_POST)) {
    if (post('mode') == 'tambah-laporan') {
        $p->tambahDataLaporanBidangPemeriksaan($_POST);
    } else if (post('mode') == 'update-laporan') {
        $p->updateDataLaporanBidangPemeriksaan($_POST);
    } else if (post('mode') == 'hapus-laporan') {
        $p->hapusDataLaporanBidangPemeriksaan(post('laporan'));
    } else if (post('mode') == 'approve-laporan') {
        $p->approveDataLaporanBidangPemeriksaan($_POST, $user->ID_PENGGUNA);
    }
}

if (!empty($_GET)) {
    $mode_array = array('laporan', 'input-laporan', 'update-laporan', 'approve-laporan', 'view-laporan');
    if (in_array(get('mode'), $mode_array)) {
        if (get('mode') == 'laporan') {
            $smarty->assign('pemeriksaan', $p->getDataPemeriksaan(get('id')));
        } else if (get('mode') == 'input-laporan') {
            $smarty->assign('bidang', $d->getDataBidang(get('bidang')));
            $smarty->assign('pemeriksaan', $p->getDataPemeriksaan(get('pemeriksaan')));
            $smarty->assign('data_laporan', $p->loadDataLaporanBidangPemeriksaan(get('pemeriksaan_bidang')));
            $smarty->assign('data_auditor', $p->loadDataBidangAuditorPemeriksaan(get('pemeriksaan_bidang')));
            $smarty->assign('data_temuan', $d->loadDataTemuanIsi());
        } else if (get('mode') == 'update-laporan') {
            $smarty->assign('bidang', $d->getDataBidang(get('bidang')));
            $smarty->assign('pemeriksaan', $p->getDataPemeriksaan(get('pemeriksaan')));
            $smarty->assign('laporan', $p->getDataLaporanBidangPemeriksaan(get('laporan')));
            $smarty->assign('data_auditor', $p->loadDataBidangAuditorPemeriksaan(get('pemeriksaan_bidang')));
            $smarty->assign('data_temuan', $d->loadDataTemuanIsi());
        } else if (get('mode') == 'approve-laporan') {
            $smarty->assign('bidang', $d->getDataBidang(get('bidang')));
            $smarty->assign('pemeriksaan', $p->getDataPemeriksaan(get('pemeriksaan')));
            $smarty->assign('laporan', $p->getDataLaporanBidangPemeriksaan(get('laporan')));
            $smarty->assign('data_auditor', $p->loadDataBidangAuditorPemeriksaan(get('pemeriksaan_bidang')));
            $smarty->assign('nama_user', $user->GELAR_DEPAN . ' ' . $user->NM_PENGGUNA . ' ' . $user->GELAR_BELAKANG);
            $smarty->assign('nip', $user->USERNAME);
        } else if (get('mode') == 'view-laporan') {
            $smarty->assign('bidang', $d->getDataBidang(get('bidang')));
            $smarty->assign('pemeriksaan', $p->getDataPemeriksaan(get('pemeriksaan')));
            $smarty->assign('laporan', $p->getDataLaporanBidangPemeriksaan(get('laporan')));
            $smarty->assign('data_auditor', $p->loadDataBidangAuditorPemeriksaan(get('pemeriksaan_bidang')));
        }
    }
}
$data_pemeriksaan = $p->loadDataPemeriksaan();
$smarty->assign('data_pemeriksaan', $data_pemeriksaan);
$smarty->display("pemeriksaan/p-laporan-hasil.tpl");
?>
