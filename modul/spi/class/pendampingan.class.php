<?php

/**
 * Description of pendampingan
 *
 * @author NAMBISEMBILU
 */
class pendampingan {

    private $db;

    function __construct($db) {
        $this->db = $db;
    }

    function loadDataPermintaanPendampingan() {
        $query = "
            SELECT SPP.*,SB.NAMA_BIDANG,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA
            FROM SPI_PERMINTAAN_PENDAMPINGAN SPP
            JOIN SPI_BIDANG SB ON SPP.ID_SPI_BIDANG=SB.ID_SPI_BIDANG
            JOIN UNIT_KERJA UK ON SPP.ID_UNIT_KERJA=UK.ID_UNIT_KERJA
            ORDER BY SPP.TGL_AWAL_RENCANA,TGL_AKHIR_RENCANA
            ";
        return $this->db->QueryToArray($query);
    }

    function getDataPermintaanPendampingan($id) {
        $this->db->Query("
            SELECT SPP.*,SB.NAMA_BIDANG,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA
            FROM SPI_PERMINTAAN_PENDAMPINGAN SPP
            JOIN SPI_BIDANG SB ON SPP.ID_SPI_BIDANG=SB.ID_SPI_BIDANG
            JOIN UNIT_KERJA UK ON SPP.ID_UNIT_KERJA=UK.ID_UNIT_KERJA
            WHERE SPP.ID_SPI_PERMINTAAN_PENDAMPINGAN='{$id}'
            ");

        return $this->db->FetchAssoc();
    }

    function tambahDataPermintaanPendampingan($post) {
        $no_permintaan = $post['no_permintaan'];
        $unit_kerja = $post['unit_kerja'];
        $bidang = $post['bidang'];
        $tgl_permintaan = $post['tgl_permintaan'];
        $tgl_awal = $post['tgl_awal'];
        $tgl_akhir = $post['tgl_akhir'];
        $konsentrasi = $post['konsentrasi'];
        $bagian = $post['bagian'];
        $alasan = $post['alasan'];
        $tujuan = $post['tujuan'];
        $query = "
            INSERT INTO SPI_PERMINTAAN_PENDAMPINGAN
                (NOMOR_PERMINTAAN,ID_UNIT_KERJA,ID_SPI_BIDANG,KONSENTRASI,BAGIAN,TGL_PERMINTAAN,TGL_AWAL_RENCANA,TGL_AKHIR_RENCANA,ALASAN,TUJUAN)
            VALUES
                ('{$no_permintaan}','{$unit_kerja}','{$bidang}','{$konsentrasi}','{$bagian}','{$tgl_permintaan}','{$tgl_awal}','{$tgl_akhir}','{$alasan}','{$tujuan}')
            ";
        $this->db->Query($query);
    }

    function updateDataPermintaanPendampingan($post) {
        $id = $post['id'];
        $no_permintaan = $post['no_permintaan'];
        $unit_kerja = $post['unit_kerja'];
        $bidang = $post['bidang'];
        $tgl_permintaan = $post['tgl_permintaan'];
        $tgl_awal = $post['tgl_awal'];
        $tgl_akhir = $post['tgl_akhir'];
        $konsentrasi = $post['konsentrasi'];
        $bagian = $post['bagian'];
        $alasan = $post['alasan'];
        $tujuan = $post['tujuan'];
        $query = "
            UPDATE SPI_PERMINTAAN_PENDAMPINGAN
            SET
                NOMOR_PERMINTAAN='{$no_permintaan}',
                ID_UNIT_KERJA='{$unit_kerja}',
                ID_SPI_BIDANG='{$bidang}',
                KONSENTRASI='{$konsentrasi}',
                BAGIAN='{$bagian}',
                TGL_PERMINTAAN='{$tgl_permintaan}',
                TGL_AWAL_RENCANA='{$tgl_awal}',
                TGL_AKHIR_RENCANA='{$tgl_akhir}',
                ALASAN='{$alasan}',
                TUJUAN='{$tujuan}'
            WHERE ID_SPI_PERMINTAAN_PENDAMPINGAN='{$id}'
            ";
        $this->db->Query($query);
    }

    function hapusDataPermintaanPendampingan($id) {
        $query = "
           DELETE FROM SPI_PERMINTAAN_PENDAMPINGAN WHERE ID_SPI_PERMINTAAN_PENDAMPINGAN='{$id}'
            ";
        $this->db->Query($query);
    }

}
