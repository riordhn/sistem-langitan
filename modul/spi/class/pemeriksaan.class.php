<?php

class pemeriksaan {

    private $db;

    function __construct($db) {
        $this->db = $db;
    }

    function loadDataUnitKerja() {
        return $this->db->QueryToArray("SELECT * FROM UNIT_KERJA WHERE TYPE_UNIT_KERJA IN ('FAKULTAS','REKTORAT','LEMBAGA') ORDER BY NM_UNIT_KERJA");
    }

    function loadDataPemeriksaan() {
        $result = array();
        $query_pemeriksaan = "
            SELECT SP.*,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA
            FROM SPI_PEMERIKSAAN SP
            JOIN UNIT_KERJA UK ON SP.ID_UNIT_KERJA=UK.ID_UNIT_KERJA
            ";
        $data_pemeriksaan = $this->db->QueryToArray($query_pemeriksaan);
        foreach ($data_pemeriksaan as $d) {
            array_push($result, array_merge($d, array('DATA_BIDANG' => $this->loadDataBidangPemeriksaan($d['ID_SPI_PEMERIKSAAN']))));
        }

        return $result;
    }

    function loadDataBidangPemeriksaan($id) {
        $result = array();
        $query_bidang = "
            SELECT SB.NAMA_BIDANG,SPB.*
            FROM SPI_PEMERIKSAAN_BIDANG SPB
            JOIN SPI_BIDANG SB ON SPB.ID_SPI_BIDANG=SB.ID_SPI_BIDANG
            WHERE SPB.ID_SPI_PEMERIKSAAN='{$id}'
            ORDER BY SPB.ID_SPI_PEMERIKSAAN_BIDANG
            ";
        $data_bidang = $this->db->QueryToArray($query_bidang);

        foreach ($data_bidang as $d) {
            array_push($result, array_merge($d, array('DATA_AUDITOR' => $this->loadDataBidangAuditorPemeriksaan($d['ID_SPI_PEMERIKSAAN_BIDANG']))));
        }

        return $result;
    }

    function loadDataBidangAuditorPemeriksaan($id) {
        $query = "
            SELECT P.NM_PENGGUNA,P.GELAR_DEPAN,P.GELAR_BELAKANG,SPA.* 
            FROM SPI_PEMERIKSAAN_AUDITOR SPA
            JOIN SPI_PERSONIL SP ON SP.ID_SPI_PERSONIL=SPA.ID_SPI_PERSONIL
            JOIN PENGGUNA P ON P.ID_PENGGUNA=SP.ID_PENGGUNA
            WHERE SPA.ID_SPI_PEMERIKSAAN_BIDANG='{$id}'
            ";
        return $this->db->QueryToArray($query);
    }

    function getDataPemeriksaan($id) {
        $this->db->Query("
            SELECT SP.*,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA
            FROM SPI_PEMERIKSAAN SP
            JOIN UNIT_KERJA UK ON SP.ID_UNIT_KERJA=UK.ID_UNIT_KERJA
            WHERE SP.ID_SPI_PEMERIKSAAN='{$id}'
            ");
        $pemeriksaan = $this->db->FetchAssoc();

        return array_merge($pemeriksaan, array('DATA_BIDANG' => $this->loadDataBidangPemeriksaan($pemeriksaan['ID_SPI_PEMERIKSAAN'])));
    }

    function loadDataLaporanBidangPemeriksaan($id) {
        $query = "
            SELECT SPL.*,STI.KODE_ISI,ISI,(P.GELAR_DEPAN||''||P.NM_PENGGUNA||''||P.GELAR_BELAKANG) VERIFIKATOR,P.USERNAME NIP
            FROM SPI_PEMERIKSAAN_LAPORAN SPL
            LEFT JOIN SPI_TEMUAN_ISI STI ON STI.ID_SPI_TEMUAN_ISI=SPL.ID_SPI_TEMUAN_ISI
            LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA=SPL.ID_VERIFIKATOR_LAPORAN
            WHERE SPL.ID_SPI_PEMERIKSAAN_BIDANG='{$id}'
            ";
        return $this->db->QueryToArray($query);
    }

    function getDataLaporanBidangPemeriksaan($id) {
        $this->db->Query("
            SELECT SPL.*,STI.KODE_ISI,ISI,(P.GELAR_DEPAN||''||P.NM_PENGGUNA||P.GELAR_BELAKANG) VERIFIKATOR,P.USERNAME NIP
            FROM SPI_PEMERIKSAAN_LAPORAN SPL
            LEFT JOIN SPI_TEMUAN_ISI STI ON STI.ID_SPI_TEMUAN_ISI=SPL.ID_SPI_TEMUAN_ISI
            LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA=SPL.ID_VERIFIKATOR_LAPORAN
            WHERE SPL.ID_SPI_PEMERIKSAAN_LAPORAN='{$id}'
            ");

        return $this->db->FetchAssoc();
    }

    function tambahDataPemeriksaan($post) {
        $rpp = $post['rpp'];
        $unit_kerja = $post['unit_kerja'];
        $tgl_awal = $post['tgl_awal'];
        $tgl_akhir = $post['tgl_akhir'];
        $tahun = $post['tahun'];
        $query = "
            INSERT INTO SPI_PEMERIKSAAN
                (NOMOR_RPP,ID_UNIT_KERJA,TGL_AWAL,TGL_AKHIR,TAHUN_PEMERIKSAAN)
            VALUES
                ('{$rpp}','{$unit_kerja}','{$tgl_awal}','{$tgl_akhir}','{$tahun}')
            ";
        $this->db->Query($query);
    }

    function updateDataPemeriksaan($post) {
        $id = $post['id'];
        $rpp = $post['rpp'];
        $unit_kerja = $post['unit_kerja'];
        $tgl_awal = $post['tgl_awal'];
        $tgl_akhir = $post['tgl_akhir'];
        $tahun = $post['tahun'];
        $query = "
            UPDATE SPI_PEMERIKSAAN
            SET
                NOMOR_RPP='{$rpp}',
                ID_UNIT_KERJA='{$unit_kerja}',
                TGL_AWAL='{$tgl_awal}',
                TGL_AKHIR='{$tgl_akhir}',
                TAHUN_PEMERIKSAAN='{$tahun}'
            WHERE ID_SPI_PEMERIKSAAN='{$id}'
            ";
        $this->db->Query($query);
    }

    function hapusDataPemeriksaan($id) {
        $query = "
           DELETE FROM SPI_PEMERIKSAAN WHERE ID_SPI_PEMERIKSAAN='{$id}'
            ";
        $this->db->Query($query);
    }

    function tambahDataBidangPemeriksaan($post) {
        $pemeriksaan = $post['pemeriksaan'];
        $bidang = $post['bidang'];
        $query = "
            INSERT INTO SPI_PEMERIKSAAN_BIDANG
                (ID_SPI_PEMERIKSAAN,ID_SPI_BIDANG)
            VALUES
                ('{$pemeriksaan}','{$bidang}')
            ";
        $this->db->Query($query);
    }

    function hapusDataBidangPemeriksaan($id) {
        $query_delete_auditor = "
           DELETE FROM SPI_PEMERIKSAAN_AUDITOR WHERE ID_SPI_PEMERIKSAAN_BIDANG='{$id}'";
        $this->db->Query($query_delete_auditor);
        $query_delete_bidang = "
           DELETE FROM SPI_PEMERIKSAAN_BIDANG WHERE ID_SPI_PEMERIKSAAN_BIDANG='{$id}'
            ";
        $this->db->Query($query_delete_bidang);
    }

    function tambahDataAuditorBidangPemeriksaan($post) {
        $bidang = $post['bidang'];
        $auditor = $post['auditor'];
        $jabatan = $post['jabatan'];
        $query = "
            INSERT INTO SPI_PEMERIKSAAN_AUDITOR
                (ID_SPI_PEMERIKSAAN_BIDANG,ID_SPI_PERSONIL,JABATAN)
            VALUES
                ('{$bidang}','{$auditor}','{$jabatan}')
            ";
        $this->db->Query($query);
    }

    function hapusDataAuditorBidangPemeriksaan($id) {
        $query = "
           DELETE FROM SPI_PEMERIKSAAN_AUDITOR WHERE ID_SPI_PEMERIKSAAN_AUDITOR='{$id}'
            ";
        $this->db->Query($query);
    }

    function tambahDataLaporanBidangPemeriksaan($post) {
        $pemeriksaan_bidang = $post['pemeriksaan_bidang'];
        $no_laporan = $post['no_laporan'];
        $surat_tugas = $post['surat_tugas'];
        $temuan = $post['temuan'];
        $judul_temuan = '(Isi temuan/kondisi)';
        $status = 0;
        $query = "
            INSERT INTO SPI_PEMERIKSAAN_LAPORAN
                (ID_SPI_PEMERIKSAAN_BIDANG,ID_SPI_TEMUAN_ISI,NOMOR_LAPORAN,NOMOR_SURAT_TUGAS,JUDUL_TEMUAN,STATUS)
            VALUES
                ('{$pemeriksaan_bidang}','{$temuan}','{$no_laporan}','{$surat_tugas}','{$judul_temuan}','{$status}')
            ";
        $this->db->Query($query);
    }

    function updateDataLaporanBidangPemeriksaan($post) {
        $id = $post['laporan'];
        $no_laporan = $post['no_laporan'];
        $surat_tugas = $post['surat_tugas'];
        $temuan = $post['temuan'];
        $judul_temuan = $post['judul'];
        $kriteria = $post['kriteria'];
        $sebab = $post['sebab'];
        $akibat = $post['akibat'];
        $rekomendasi = $post['rekomendasi'];
        $tanggapan = $post['tanggapan'];
        $rencana = $post['rencana'];
        $hasil = $post['hasil'];
        $query = "
            UPDATE SPI_PEMERIKSAAN_LAPORAN
            SET
                ID_SPI_TEMUAN_ISI='{$temuan}',
                NOMOR_LAPORAN='{$no_laporan}',
                NOMOR_SURAT_TUGAS='{$surat_tugas}',
                JUDUL_TEMUAN='{$judul_temuan}',
                KRITERIA='{$kriteria}',
                SEBAB='{$sebab}',
                AKIBAT='{$akibat}',
                REKOMENDASI='{$rekomendasi}',
                TANGGAPAN='{$tanggapan}',
                RENCANA='{$rencana}',
                HASIL='{$hasil}'
            WHERE ID_SPI_PEMERIKSAAN_LAPORAN='{$id}'
            ";
        $this->db->Query($query);
    }

    function approveDataLaporanBidangPemeriksaan($post, $pengguna) {
        $id = $post['laporan'];
        $status = !empty($post['approve']) ? 1 : 0;
        $verifikator = $pengguna;

        if ($status == 1) {
            $query_approve = "
            UPDATE SPI_PEMERIKSAAN_LAPORAN
            SET
                STATUS='{$status}',
                ID_VERIFIKATOR_LAPORAN='{$verifikator}',
                TGL_PERSETUJUAN=SYSDATE
            WHERE ID_SPI_PEMERIKSAAN_LAPORAN='{$id}'
            ";

            $this->db->Query($query_approve);
        } else {
            $query_unapprove = "
            UPDATE SPI_PEMERIKSAAN_LAPORAN
            SET
                STATUS='{$status}',
                ID_VERIFIKATOR_LAPORAN='',
                TGL_PERSETUJUAN=''
            WHERE ID_SPI_PEMERIKSAAN_LAPORAN='{$id}'
            ";
            $this->db->Query($query_unapprove);
        }
    }

    function hapusDataLaporanBidangPemeriksaan($id) {
        $query = "
           DELETE FROM SPI_PEMERIKSAAN_LAPORAN WHERE ID_SPI_PEMERIKSAAN_LAPORAN='{$id}'
            ";
        $this->db->Query($query);
    }

}

?>
