<?php

class data {

    private $db;

    function __construct($db) {
        $this->db = $db;
    }

    // DATA BIDANG

    function loadDataBidang() {
        return $this->db->QueryToArray("SELECT * FROM SPI_BIDANG WHERE STATUS_AKTIF=1 ORDER BY NAMA_BIDANG");
    }

    function getDataBidang($id) {
        $this->db->Query("SELECT * FROM SPI_BIDANG WHERE ID_SPI_BIDANG='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambahDataBidang($post) {
        $nama_bidang = $post['nama'];
        $status = 1;
        $query = "
            INSERT INTO SPI_BIDANG
                (NAMA_BIDANG,STATUS_AKTIF)
            VALUES
                ('{$nama_bidang}','{$status}')
            ";
        $this->db->Query($query);
    }

    function updateDataBidang($post) {
        $id = $post['id'];
        $nama_bidang = $post['nama'];
        $query = "
            UPDATE SPI_BIDANG
            SET
                NAMA_BIDANG='{$nama_bidang}'
            WHERE ID_SPI_BIDANG='{$id}'
            ";
        $this->db->Query($query);
    }

    function hapusDataBidang($id) {
        $query = "
           UPDATE SPI_BIDANG SET STATUS_AKTIF=0 WHERE ID_SPI_BIDANG='{$id}'
            ";
        $this->db->Query($query);
    }

    // Data Personil

    function loadDataPersonil() {
        return $this->db->QueryToArray("
            SELECT P.NM_PENGGUNA,P.GELAR_DEPAN,P.GELAR_BELAKANG,SP.* 
            FROM SPI_PERSONIL SP
            JOIN PENGGUNA P ON P.ID_PENGGUNA=SP.ID_PENGGUNA
            WHERE SP.STATUS_AKTIF=1
            ORDER BY SP.ID_SPI_PERSONIL ASC
            ");
    }

    function getDataPersonil($id) {
        $this->db->Query("
            SELECT P.NM_PENGGUNA,P.GELAR_DEPAN,P.GELAR_BELAKANG,SP.* 
            FROM SPI_PERSONIL SP
            JOIN PENGGUNA P ON P.ID_PENGGUNA=SP.ID_PENGGUNA
            WHERE ID_SPI_PERSONIL='{$id}'
            ");
        return $this->db->FetchAssoc();
    }

    function tambahDataPersonil($post) {
        $pengguna = $post['pengguna'];
        $jabatan = $post['jabatan'];
        $status = 1;
        $query = "
            INSERT INTO SPI_PERSONIL
                (ID_PENGGUNA,JABATAN,STATUS_AKTIF)
            VALUES
                ('{$pengguna}','{$jabatan}','{$status}')
            ";
        $this->db->Query($query);
    }

    function updateDataPersonil($post) {
        $id = $post['id'];
        $pengguna = $post['pengguna'];
        $jabatan = $post['jabatan'];
        $query = "
            UPDATE SPI_PERSONIL
            SET
                ID_PENGGUNA='{$pengguna}',
                ID_PENGGUNA='{$jabatan}'
            WHERE ID_SPI_PERSONIL='{$id}'
            ";
        $this->db->Query($query);
    }

    function hapusDataPersonil($id) {
        $query = "
           UPDATE SPI_PERSONIL SET STATUS_AKTIF=0 WHERE ID_SPI_PERSONIL='{$id}'
            ";
        $this->db->Query($query);
    }

    // DATA ATURAN

    function loadDataAturan() {
        return $this->db->QueryToArray("
            SELECT SRP.* ,SJRP.NAMA_JENIS
            FROM SPI_REF_PERATURAN SRP
            JOIN SPI_JENIS_REF_PERATURAN SJRP ON SRP.JENIS_PERATURAN=SJRP.ID_SPI_JENIS_REF_PERATURAN
            ORDER BY SJRP.NAMA_JENIS,SRP.ID_SPI_REF_PERATURAN
            ");
    }

    function getDataAturan($id) {
        $this->db->Query("
            SELECT SRP.* ,SJRP.NAMA_JENIS
            FROM SPI_REF_PERATURAN SRP
            JOIN SPI_JENIS_REF_PERATURAN SJRP ON SRP.JENIS_PERATURAN=SJRP.ID_SPI_JENIS_REF_PERATURAN
            WHERE SRP.ID_SPI_REF_PERATURAN='{$id}'
            ");
        return $this->db->FetchAssoc();
    }

    function tambahDataAturan($post) {
        $jenis = $post['jenis'];
        $nomor = $post['nomor'];
        $tahun = $post['tahun'];
        $isi = $post['isi'];
        $query = "
            INSERT INTO SPI_REF_PERATURAN
                (JENIS_PERATURAN,NOMOR_PERATURAN,TAHUN_PERATURAN,ISI_PERATURAN)
            VALUES
                ('{$jenis}','{$nomor}','{$tahun}','{$isi}')
            ";
        $this->db->Query($query);
    }

    function updateDataAturan($post) {
        $id = $post['id'];
        $jenis = $post['jenis'];
        $nomor = $post['nomor'];
        $tahun = $post['tahun'];
        $isi = $post['isi'];
        $query = "
            UPDATE SPI_REF_PERATURAN
            SET
                JENIS_PERATURAN='{$jenis}',
                NOMOR_PERATURAN='{$nomor}',
                TAHUN_PERATURAN='{$tahun}',
                ISI_PERATURAN='{$isi}'
            WHERE ID_SPI_REF_PERATURAN='{$id}'
            ";
        $this->db->Query($query);
    }

    function hapusDataAturan($id) {
        $query = "
           DELETE FROM SPI_REF_PERATURAN WHERE ID_SPI_REF_PERATURAN='{$id}'
            ";
        $this->db->Query($query);
    }

    // DATA JENIS ATURAN

    function loadDataJenisAturan() {
        return $this->db->QueryToArray("SELECT * FROM SPI_JENIS_REF_PERATURAN");
    }

    function getDataJenisAturan($id) {
        $this->db->Query("SELECT * FROM SPI_JENIS_REF_PERATURAN WHERE ID_SPI_JENIS_REF_PERATURAN='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambahDataJenisAturan($post) {
        $jenis = $post['nama'];
        $query = "
            INSERT INTO SPI_JENIS_REF_PERATURAN
                (NAMA_JENIS)
            VALUES
                ('{$jenis}')
            ";
        $this->db->Query($query);
    }

    function updateDataJenisAturan($post) {
        $id = $post['id'];
        $jenis = $post['nama'];
        $query = "
            UPDATE SPI_JENIS_REF_PERATURAN
            SET
                NAMA_JENIS='{$jenis}'
            WHERE ID_SPI_JENIS_REF_PERATURAN='{$id}'
            ";
        $this->db->Query($query);
    }

    function hapusDataJenisAturan($id) {
        $query = "
           DELETE FROM SPI_JENIS_REF_PERATURAN WHERE ID_SPI_JENIS_REF_PERATURAN='{$id}'
            ";
        $this->db->Query($query);
    }

    // DATA TEMUAN KELOMPOK

    function loadDataTemuanKelompok() {
        return $this->db->QueryToArray("SELECT * FROM SPI_TEMUAN_KELOMPOK WHERE STATUS_AKTIF=1 ORDER BY ID_SPI_TEMUAN_KELOMPOK");
    }

    function getDataTemuanKelompok($id) {
        $this->db->Query("SELECT * FROM SPI_TEMUAN_KELOMPOK WHERE ID_SPI_TEMUAN_KELOMPOK='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambahDataTemuanKelompok($post) {
        $kode = $post['kode'];
        $nama = $post['nama'];
        $status = 1;
        $query = "
            INSERT INTO SPI_TEMUAN_KELOMPOK
                (KODE_KELOMPOK,NAMA_KELOMPOK,STATUS_AKTIF)
            VALUES
                ('{$kode}','{$nama}','{$status}')
            ";
        $this->db->Query($query);
    }

    function updateDataTemuanKelompok($post) {
        $id = $post['id'];
        $kode = $post['kode'];
        $nama = $post['nama'];
        $query = "
            UPDATE SPI_TEMUAN_KELOMPOK
            SET
                KODE_KELOMPOK='{$kode}',
                NAMA_KELOMPOK='{$nama}'
            WHERE ID_SPI_TEMUAN_KELOMPOK='{$id}'
            ";
        $this->db->Query($query);
    }

    function hapusDataTemuanKelompok($id) {
        $query = "
           UPDATE SPI_TEMUAN_KELOMPOK SET STATUS_AKTIF=0 WHERE ID_SPI_TEMUAN_KELOMPOK='{$id}'
            ";
        $this->db->Query($query);
    }

    // DATA TEMUAN SUBKELOMPOK
    function loadDataTemuanSubKelompok() {
        return $this->db->QueryToArray("
            SELECT STK.NAMA_KELOMPOK,STK.KODE_KELOMPOK,STS.*
            FROM SPI_TEMUAN_SUBKELOMPOK STS
            JOIN SPI_TEMUAN_KELOMPOK STK ON STS.ID_SPI_TEMUAN_KELOMPOK=STK.ID_SPI_TEMUAN_KELOMPOK
            WHERE STS.STATUS_AKTIF=1
            ORDER BY STS.ID_SPI_TEMUAN_SUBKELOMPOK
            ");
    }

    function loadDataTemuanSubKelompokByKelompok($kelompok) {
        return $this->db->QueryToArray("
            SELECT STK.NAMA_KELOMPOK,STK.KODE_KELOMPOK,STS.*
            FROM SPI_TEMUAN_SUBKELOMPOK STS
            JOIN SPI_TEMUAN_KELOMPOK STK ON STS.ID_SPI_TEMUAN_KELOMPOK=STK.ID_SPI_TEMUAN_KELOMPOK
            WHERE STS.STATUS_AKTIF=1 AND STS.ID_SPI_TEMUAN_KELOMPOK='{$kelompok}'
            ORDER BY STS.ID_SPI_TEMUAN_SUBKELOMPOK
            ");
    }

    function getDataTemuanSubKelompok($id) {
        $this->db->Query("
            SELECT STK.NAMA_KELOMPOK,STK.KODE_KELOMPOK,STS.*
            FROM SPI_TEMUAN_SUBKELOMPOK STS
            JOIN SPI_TEMUAN_KELOMPOK STK ON STS.ID_SPI_TEMUAN_KELOMPOK=STK.ID_SPI_TEMUAN_KELOMPOK
            WHERE STS.ID_SPI_TEMUAN_SUBKELOMPOK='{$id}'
            ");
        return $this->db->FetchAssoc();
    }

    function tambahDataTemuanSubKelompok($post) {
        $kelompok = $post['kelompok'];
        $kode = $post['kode'];
        $nama = $post['nama'];
        $status = 1;
        $query = "
            INSERT INTO SPI_TEMUAN_SUBKELOMPOK
                (ID_SPI_TEMUAN_KELOMPOK,KODE_SUBKELOMPOK,NAMA_SUBKELOMPOK,STATUS_AKTIF)
            VALUES
                ('{$kelompok}','{$kode}','{$nama}','{$status}')
            ";
        $this->db->Query($query);
    }

    function updateDataTemuanSubKelompok($post) {
        $id = $post['id'];
        $kelompok = $post['kelompok'];
        $kode = $post['kode'];
        $nama = $post['nama'];
        $query = "
            UPDATE SPI_TEMUAN_SUBKELOMPOK
            SET
                ID_SPI_TEMUAN_KELOMPOK='{$kelompok}',
                KODE_SUBKELOMPOK='{$kode}',
                NAMA_SUBKELOMPOK='{$nama}'
            WHERE ID_SPI_TEMUAN_SUBKELOMPOK='{$id}'
            ";
        $this->db->Query($query);
    }

    function hapusDataTemuanSubKelompok($id) {
        $query = "
           UPDATE SPI_TEMUAN_SUBKELOMPOK SET STATUS_AKTIF=0 WHERE ID_SPI_TEMUAN_SUBKELOMPOK='{$id}'
            ";
        $this->db->Query($query);
    }

    // DATA TEMUAN ISI

    function loadDataTemuanIsi() {
        $result = array();
        $data_temuan = $this->db->QueryToArray("
            SELECT STK.NAMA_KELOMPOK,STK.KODE_KELOMPOK,STS.NAMA_SUBKELOMPOK,STS.KODE_SUBKELOMPOK,STI.*
            FROM SPI_TEMUAN_ISI STI
            JOIN SPI_TEMUAN_SUBKELOMPOK STS ON STI.ID_SPI_TEMUAN_SUBKELOMPOK=STS.ID_SPI_TEMUAN_SUBKELOMPOK
            JOIN SPI_TEMUAN_KELOMPOK STK ON STS.ID_SPI_TEMUAN_KELOMPOK=STK.ID_SPI_TEMUAN_KELOMPOK
            WHERE STI.STATUS_AKTIF=1
            ORDER BY STI.ID_SPI_TEMUAN_ISI
            ");
        foreach ($data_temuan as $d) {
            array_push($result, array_merge($d, array('DATA_REKOMENDASI' => $this->loadDataTemuanRekomendasi($d['ID_SPI_TEMUAN_ISI']))));
        }
        return $result;
    }

    function getDataTemuanIsi($id) {
        $this->db->Query("
            SELECT STK.NAMA_KELOMPOK,STK.KODE_KELOMPOK,STS.NAMA_SUBKELOMPOK,STS.KODE_SUBKELOMPOK,STI.*
            FROM SPI_TEMUAN_ISI STI
            JOIN SPI_TEMUAN_SUBKELOMPOK STS ON STI.ID_SPI_TEMUAN_SUBKELOMPOK=STS.ID_SPI_TEMUAN_SUBKELOMPOK
            JOIN SPI_TEMUAN_KELOMPOK STK ON STS.ID_SPI_TEMUAN_KELOMPOK=STK.ID_SPI_TEMUAN_KELOMPOK
            WHERE STI.ID_SPI_TEMUAN_ISI='{$id}'
            ");
        $temuan = $this->db->FetchAssoc();
        return array_merge($temuan, array('DATA_REKOMENDASI' => $this->loadDataTemuanRekomendasi($id)));
    }

    function tambahDataTemuanIsi($post) {
        $kelompok = $post['kelompok'];
        $subkelompok = $post['subkelompok'];
        $kode = $post['kode'];
        $isi = $post['isi'];
        $status = 1;
        $query = "
            INSERT INTO SPI_TEMUAN_ISI
                (ID_SPI_TEMUAN_KELOMPOK,ID_SPI_TEMUAN_SUBKELOMPOK,KODE_ISI,ISI,STATUS_AKTIF)
            VALUES
                ('{$kelompok}','{$subkelompok}','{$kode}','{$isi}','{$status}')
            ";
        $this->db->Query($query);
    }

    function updateDataTemuanIsi($post) {
        $id = $post['id'];
        $kelompok = $post['kelompok'];
        $subkelompok = $post['subkelompok'];
        $kode = $post['kode'];
        $isi = $post['isi'];
        $query = "
            UPDATE SPI_TEMUAN_ISI
            SET
                ID_SPI_TEMUAN_KELOMPOK='{$kelompok}',
                ID_SPI_TEMUAN_SUBKELOMPOK='{$subkelompok}',
                KODE_ISI='{$kode}',
                ISI='{$isi}'
            WHERE ID_SPI_TEMUAN_ISI='{$id}'
            ";
        $this->db->Query($query);
    }

    function hapusDataTemuanIsi($id) {
        $query = "
          UPDATE SPI_TEMUAN_ISI SET STATUS_AKTIF=0 WHERE ID_SPI_TEMUAN_ISI='{$id}'
            ";
        $this->db->Query($query);
    }

    // DATA TEMUAN REKOMENDASI
    function loadDataTemuanRekomendasi($temuan) {
        return $this->db->QueryToArray("
            SELECT STR.*,SJRP.NAMA_JENIS,SRP.NOMOR_PERATURAN,SRP.ISI_PERATURAN
            FROM SPI_TEMUAN_REKOMENDASI STR
            JOIN SPI_REF_PERATURAN SRP ON STR.ID_PERATURAN=SRP.ID_SPI_REF_PERATURAN
            JOIN SPI_JENIS_REF_PERATURAN SJRP ON SJRP.ID_SPI_JENIS_REF_PERATURAN=SRP.JENIS_PERATURAN
            WHERE STR.ID_SPI_TEMUAN_ISI='{$temuan}'
            ORDER BY STR.ID_SPI_TEMUAN_REKOMENDASI
            ");
    }

    function tambahDataTemuanRekomendasi($post) {
        $temuan = $post['temuan'];
        $aturan = $post['aturan'];
        if (!empty($aturan) && !empty($temuan)) {
            $query = "
            INSERT INTO SPI_TEMUAN_REKOMENDASI
                (ID_SPI_TEMUAN_ISI,ID_PERATURAN)
            VALUES
                ('{$temuan}','{$aturan}')
            ";
            $this->db->Query($query);
        }
    }

    function hapusDataTemuanRekomendasi($id) {
        $query = "
          DELETE FROM SPI_TEMUAN_REKOMENDASI WHERE ID_SPI_TEMUAN_REKOMENDASI='{$id}'
            ";
        $this->db->Query($query);
    }

}

?>
