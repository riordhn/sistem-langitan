<?php

include('config.php');

$location = explode("-", $_GET['location']);

foreach ($user->MODULs as $modul) {
    if ($modul['NM_MODUL'] == $location[0]) {
        $smarty->assign('modul', $modul);

        foreach ($modul['MENUs'] as $menu) {
            if (count($location) > 1) {
                if ($menu['NM_MENU'] == $location[1]) {
                    $smarty->assign('menu', $menu);
                }
            }
        }
    }
}

$smarty->display('breadcrumbs.tpl');
?>
