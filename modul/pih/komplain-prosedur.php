<?php

include 'config.php';

//$db->Query("DELETE FROM KOMPLAIN_TANGGAPAN");
//$db->Query("UPDATE KOMPLAIN SET STATUS_KOMPLAIN=1 WHERE ID_KOMPLAIN='2'");
if (!empty($_GET)) {
    if (get('mode') == 'detail') {
        $id_komplain = get('k');
        $q_data_komplain = "
        SELECT P.NM_PENGGUNA,
        (CASE
            WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.DOSEN)
            THEN 'Dosen'
            WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.PEGAWAI)
            THEN 'Tenaga Kependidikan'
            ELSE 'Mahasiswa'
        END  
        ) STATUS
        ,KK.NM_KATEGORI,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA,K.*,TO_CHAR(K.WAKTU_KOMPLAIN,'DD-MM-YYYY HH24:MI:SS') WAKTU,
        KT.*,P2.NM_PENGGUNA PENJAWAB,
        (CASE
            WHEN P2.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.DOSEN)
            THEN 'Dosen'
            WHEN P2.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.PEGAWAI)
            THEN 'Tenaga Kependidikan'
        END  
        ) STATUS_PENJAWAB,TO_CHAR(KT.WAKTU_JAWAB,'DD-MM-YYYY HH24:MI:SS') WAKTU_JAWAB
        FROM AUCC.KOMPLAIN K
        JOIN AUCC.KOMPLAIN_KATEGORI KK ON K.ID_KOMPLAIN_KATEGORI=KK.ID_KOMPLAIN_KATEGORI
        JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=K.ID_UNIT_TERTUJU
        JOIN AUCC.PENGGUNA P ON K.ID_PELAPOR=P.ID_PENGGUNA
        LEFT JOIN AUCC.KOMPLAIN_TANGGAPAN KT ON KT.ID_KOMPLAIN=K.ID_KOMPLAIN
        LEFT JOIN AUCC.PENGGUNA P2 ON P2.ID_PENGGUNA=KT.ID_PENJAWAB
        WHERE K.ID_KOMPLAIN='{$id_komplain}'
        ";
        $db->Query($q_data_komplain);
        $data_komplain = $db->FetchAssoc();
        $smarty->assign('komplain', $data_komplain);
    }
}

if (!empty($_POST)) {
    if (post('mode') == 'arahkan') {
        $id_komplain = post('id_komplain');
        $q_u_komplain = "
            UPDATE KOMPLAIN SET STATUS_KOMPLAIN='2' WHERE ID_KOMPLAIN='{$id_komplain}'
            ";
        $db->Query($q_u_komplain);
    } else if (post('mode') == 'jawab') {
        $id_penjawab = $user->ID_PENGGUNA;
        $id_komplain = post('id_komplain');
        $analisa = post('analisa');
        $tindakan = post('tindakan');
        $keterangan = post('keterangan');
        $tgl_tindakan = post('tgl_tindakan');
        $tgl_realisasi = post('tgl_realisasi');
        $q_i_jawaban = "
            INSERT INTO KOMPLAIN_TANGGAPAN
                (ID_KOMPLAIN,ID_PENJAWAB,WAKTU_JAWAB,ANALISA_PENYEBAB,TINDAKAN,KETERANGAN,TGL_SELESAI_RENCANA,TGL_SELESAI_REALISASI)
            VALUES 
                ('{$id_komplain}','{$id_penjawab}',CURRENT_TIMESTAMP,'{$analisa}','{$tindakan}','{$keterangan}','{$tgl_tindakan}','{$tgl_realisasi}')
            ";
        $db->Query($q_i_jawaban);
        $q_u_komplain = "
            UPDATE KOMPLAIN SET STATUS_KOMPLAIN='3' WHERE ID_KOMPLAIN='{$id_komplain}'
            ";
        $db->Query($q_u_komplain);
    }
}


$q_data_komplain = "
SELECT P.NM_PENGGUNA,
(CASE
    WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.DOSEN)
    THEN 'Dosen'
    WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.PEGAWAI)
    THEN 'Tenaga Kependidikan'
    ELSE 'Mahasiswa'
END  
) STATUS
,KK.NM_KATEGORI,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA,K.*,TO_CHAR(K.WAKTU_KOMPLAIN,'DD-MM-YYYY HH24:MI:SS') WAKTU
FROM AUCC.KOMPLAIN K
JOIN AUCC.KOMPLAIN_KATEGORI KK ON K.ID_KOMPLAIN_KATEGORI=KK.ID_KOMPLAIN_KATEGORI
JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=K.ID_UNIT_TERTUJU
JOIN AUCC.PENGGUNA P ON K.ID_PELAPOR=P.ID_PENGGUNA
ORDER BY WAKTU DESC
";
$data_komplain = $db->QueryToArray($q_data_komplain);

$smarty->assign('info_komplain', alert_success("Anda hanya bisa melakukan komplain mengenai prosedur yang ada pada setiap Unit Kerja setelah membaca Dokumen Prosedur yang sudah ada. Terima Kasih", 98));
$smarty->assign('data_komplain', $data_komplain);
$smarty->display('aims/komplain-prosedur.tpl');
?>
