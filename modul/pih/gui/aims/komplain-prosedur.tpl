{if $smarty.get.mode=='detail'}
    <div class="center_title_bar">Detail Komplain Pedoman Prosedur</div>
    <fieldset style="width: 90%">
        <legend>Komplain Prosedur</legend>
        <div class="field_form">
            <label>Pelapor : </label>
            {$komplain.NM_PENGGUNA|upper} - {$komplain.STATUS} ({$komplain.WAKTU})
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Prosedur : </label>
            {$komplain.NM_KATEGORI|upper} - {$komplain.DESKRIPSI_UNIT_KERJA}
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Isi Komplain: </label>
            {$komplain.ISI_KOMPLAIN}
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Saran dan Tindakan: </label>
            {$komplain.SARAN_TINDAKAN}
        </div>
        {if $komplain.ID_KOMPLAIN_TANGGAPAN!=''}
            <div class="field_form">
                <label style="font-size: 1.2em;color: blue;">-- Jawaban --</label>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Penjawab</label>
                {$komplain.PENJAWAB} - {$komplain.STATUS_PENJAWAB} ({$komplain.WAKTU_JAWAB})
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Analisa Penyebab</label>
                {$komplain.ANALISA_PENYEBAB}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Tindakan</label>
                {$komplain.TINDAKAN}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Keterangan</label>
                {$komplain.KETERANGAN}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Rencana Tindakan</label>
                {$komplain.TGL_SELESAI_RENCANA}
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Rencana Realisasi</label>
                {$komplain.TGL_SELESAI_REALISASI}
            </div>
            <div class="clear-fix"></div>
            {if $komplain.KETERANGAN_TANGGAPAN!=''}
                <div class="field_form">
                    <label style="font-size: 1.2em;color: blue;">-- Penilaian --</label>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Nilai</label>
                    <div id="star" data-score="{$komplain.NILAI_TANGGAPAN}" style="display: inline-block;"></div>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Keterangan Nilai</label>
                    {$komplain.KETERANGAN_TANGGAPAN}
                </div>
                {literal}
                    <script type="text/javascript">
                        $('#star').raty({
                            score: function() {
                                return $(this).attr('data-score');
                            },
                            readOnly: true
                        });
                    </script>
                {/literal}
            {/if}
            <div class="bottom_field_form">
                <a href="komplain-prosedur.php" class="button">Kembali</a>
            </div>
        {else}
            <form id="tanggapan" method="post" action="komplain-prosedur.php" style="display: none">
                <div class="field_form">
                    <label style="font-size: 1.2em;color: blue;">-- Jawaban --</label>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Analisa Penyebab</label>
                    <textarea name="analisa" style="resize: none;width: 80%" class="required" rows="5"></textarea>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Tindakan</label>
                    <textarea name="tindakan" style="resize: none;width: 80%" class="required" rows="5"></textarea>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Keterangan</label>
                    <textarea name="keterangan" style="resize: none;width: 80%" class="required" rows="5"></textarea>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Rencana Tindakan</label>
                    <input type="text" name="tgl_tindakan" size="15" class="required datepicker" maxlength="30"/>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Rencana Realisasi</label>
                    <input type="text" name="tgl_realisasi"  size="15" class="required datepicker" readonly="" maxlength="30" />
                </div>
                <div class="clear-fix"></div>
                <div class="bottom_field_form">
                    <input type="hidden" name="mode" value="jawab"/>
                    <a style="cursor: pointer" class="disable-ajax button" onclick="$('#tanggapan').hide();
                            $('#aksi').fadeIn()">Batal</a>
                    <input type="submit" class="button" value="Submit Jawaban"/>
                </div>
                <input type="hidden" name="mode" value="jawab"/>
                <input type="hidden" name="id_komplain" value="{$smarty.get.k}"/>
            </form>
            <div class="clear-fix"></div>
            <div id="aksi" class="bottom_field_form">
                <a href="komplain-prosedur.php" class="button">Kembali</a>
                <a style="cursor: pointer" class="disable-ajax button" onclick="$('#tanggapan').fadeIn();
                            $('#aksi').hide()">Jawab</a>
                <form method="post" action="komplain-prosedur.php">
                    <input type="hidden" name="mode" value="arahkan"/>
                    <input type="hidden" name="id_komplain" value="{$smarty.get.k}"/>
                    <input type="submit" class="button" value="Arahkan Ke Unit Kerja"/>
                </form>
            </div>
        {/if}
    </fieldset>
{else}
    <div class="center_title_bar">Komplain Prosedur</div>
    <h2>Data Komplain Prosedur</h2>
    <table cellpadding="0" cellspacing="0" border="0" class="datatable display" >
        <thead>
            <tr>
                <th>Pelapor</th>
                <th>Prosedur & Unit Kerja Dituju</th>
                <th>Isi Komplain</th>
                <th>Saran</th>
                <th class="center">Status</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_komplain as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td style="width: 220px">
                        {$d.NM_PENGGUNA|upper} - {$d.STATUS}<br/>
                        {$d.WAKTU}
                    </td>
                    <td style="width: 220px">
                        {$d.NM_KATEGORI}<br/> ({$d.DESKRIPSI_UNIT_KERJA})
                    </td>
                    <td>{SplitWord($d.ISI_KOMPLAIN,10)}...</td>
                    <td>{SplitWord($d.SARAN_TINDAKAN,10)}...</td>
                    <td class="center">
                        {if $d.STATUS_KOMPLAIN==1}
                            <label style="color: #009999;font-weight: bold">Komplain Baru</label><br/>
                            <a href="komplain-prosedur.php?mode=detail&k={$d.ID_KOMPLAIN}" class="button">Aksi</a>
                        {elseif $d.STATUS_KOMPLAIN==2}
                            <label style="color: #ffcc33;font-weight: bold">Ditangani Unit Kerja</label>
                        {elseif $d.STATUS_KOMPLAIN==3}
                            <a href="komplain-prosedur.php?mode=detail&k={$d.ID_KOMPLAIN}" class="button">Terjawab</a>
                        {elseif $d.STATUS_KOMPLAIN==4}
                            <label style="color: #00bb00;font-weight: bold">Sudah Selesai</label><br/>
                            <a href="komplain-prosedur.php?mode=detail&k={$d.ID_KOMPLAIN}" class="button">Detail</a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{/if}
{literal}
    <script>
                        $(document).ready(function() {
                            oTable = $('.datatable').dataTable({
                                "bJQueryUI": true,
                                "sPaginationType": "full_numbers"
                            });
                            $('form').validate();
                            $(".datepicker").datepicker({
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: 'dd-M-y'
                            }).css({'text-transform': 'uppercase'});
                        });
    </script>
{/literal}