<?php

class tracer {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    // menu tracer studi

    function get_akad_mhs($id_pengguna) {
        $id_mhs = $this->db->QuerySingle("SELECT ID_MHS FROM MAHASISWA WHERE ID_PENGGUNA='{$id_pengguna}'");
        $id_fakultas = $this->db->QuerySingle("SELECT ID_FAKULTAS FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM MAHASISWA WHERE ID_MHS='{$id_mhs}')");
        $sql = "select a.id_mhs,sum(a.kredit_semester) skstotal,
            round(sum(a.kredit_semester*(case a.nilai_huruf 
            when 'A' then 4 
            when 'AB' then 3.5 
            when 'B' then 3
            when 'BC' then 2.5
            when 'C' then 2
            when 'D' then 1
            end))/sum(a.kredit_semester),2) IPK
            from
            (
            select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
            select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
            from pengambilan_mk a 
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null and a.flagnilai=1 and a.status_hapus=0
            ) a
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
            left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
            where rangking=1 and id_mhs='{$id_mhs}'
            ) a
            left join mahasiswa b on a.id_mhs=b.id_mhs
            left join program_studi f on b.id_program_studi=f.id_program_studi
            where a.id_mhs='{$id_mhs}'
            group by a.id_mhs order by a.id_mhs";

        // ngulang -> nilai terakhir yg berlaku
        if ($id_fakultas == 10 or $id_fakultas == 11) {
            $sql = "select a.id_mhs,sum(a.kredit_semester) skstotal,
                round(sum(a.kredit_semester*(case a.nilai_huruf 
                when 'A' then 4 
                when 'AB' then 3.5 
                when 'B' then 3
                when 'BC' then 2.5
                when 'C' then 2
                when 'D' then 1
                end))/sum(a.kredit_semester),2) IPK
                from
                (
                select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
                select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester desc,sm.nm_semester desc) rangking
                from pengambilan_mk a 
                left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                left join semester sm on sm.id_semester=a.id_semester
                where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.flagnilai=1 and a.id_semester is not null and a.status_hapus=0
                ) a
                left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                where rangking=1 and id_mhs='{$id_mhs}'
                ) a
                left join mahasiswa b on a.id_mhs=b.id_mhs
                left join program_studi f on b.id_program_studi=f.id_program_studi
                where a.id_mhs='{$id_mhs}'
                group by a.id_mhs order by a.id_mhs";
        }
        $this->db->Query($sql);
        return $this->db->FetchAssoc();
    }

    function get_data_mahasiswa($id_pengguna) {
        $this->db->Query("
            SELECT M.*,P.NM_PENGGUNA,TO_CHAR(P.TGL_LAHIR_PENGGUNA,'YYYY') TAHUN_LAHIR,P.KELAMIN_PENGGUNA,PS.NM_PROGRAM_STUDI,F.NM_FAKULTAS,J.NM_JENJANG,TO_CHAR(AD.TGL_LULUS,'YYYY') TAHUN_LULUS
            FROM PENGGUNA P
            JOIN MAHASISWA M ON M.ID_PENGGUNA=P.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            LEFT JOIN ADMISI AD ON AD.ID_MHS=M.ID_MHS
            WHERE P.ID_PENGGUNA='{$id_pengguna}'");
        return $this->db->FetchAssoc();
    }

    function load_data_soal($form, $kategori_soal) {
        $data_hasil = array();
        $data_soal = $this->db->QueryToArray("SELECT * FROM KUISIONER_SOAL WHERE ID_FORM='{$form}' AND ID_KATEGORI_SOAL='{$kategori_soal}' ORDER BY ID_SOAL");
        foreach ($data_soal as $data) {
            array_push($data_hasil, array_merge($data, array(
                        'JUMLAH_JAWABAN' => count($this->load_data_jawaban($data['ID_SOAL'])),
                        'DATA_JAWABAN' => $this->load_data_jawaban($data['ID_SOAL'])
                            )
                    )
            );
        }
        return $data_hasil;
    }

    function load_data_jawaban($soal) {
        return $this->db->QueryToArray("SELECT * FROM KUISIONER_JAWABAN WHERE ID_SOAL='{$soal}' ORDER BY ID_JAWABAN");
    }

    function cek_hasil_form($id_pengguna, $form) {
        $this->db->Query("SELECT * FROM KUISIONER_HASIL_FORM WHERE ID_PENGGUNA='{$id_pengguna}' AND ID_FORM='{$form}'");
        return $this->db->FetchAssoc();
    }

    function insert_hasil_form($id_pengguna, $form) {
        $this->db->Query("INSERT INTO KUISIONER_HASIL_FORM (ID_PENGGUNA,ID_FORM) VALUES ('{$id_pengguna}','{$form}')");
    }

    function cek_hasil_soal($hasil_form, $soal) {
        $this->db->Query("SELECT * FROM KUISIONER_HASIL_SOAL WHERE ID_HASIL_FORM='{$hasil_form}' AND ID_SOAL='{$soal}'");
        return $this->db->FetchAssoc();
    }

    function insert_hasil_soal($hasil_form, $soal) {
        $this->db->Query("INSERT INTO KUISIONER_HASIL_SOAL (ID_HASIL_FORM,ID_SOAL) VALUES ('{$hasil_form}','{$soal}')");
    }

    function insert_hasil_jawaban($soal, $jawaban, $isian, $ket) {
        $this->db->Query("INSERT INTO KUISIONER_HASIL_JAWABAN (ID_HASIL_SOAL,ID_JAWABAN,ISIAN,KETERANGAN) VALUES ('{$soal}','{$jawaban}','{$isian}','{$ket}')");
    }

    function get_jumlah_group_soal($soal) {
        return $this->db->QuerySingle("SELECT MAX(GROUP_JAWABAN) JUMLAH_GROUP FROM KUISIONER_JAWABAN WHERE ID_SOAL='{$soal}'");
    }

    function cek_hasil_jawaban($kategori_soal, $id_pengguna) {
        return $this->db->QuerySingle("
            SELECT COUNT(*) STATUS_ISI FROM KUISIONER_HASIL_JAWABAN KHJ
            LEFT JOIN KUISIONER_HASIL_SOAL KHS ON KHS.ID_HASIL_SOAL=KHJ.ID_HASIL_SOAL
            LEFT JOIN KUISIONER_SOAL KS ON KHS.ID_SOAL=KS.ID_SOAL
            LEFT JOIN KUISIONER_HASIL_FORM KHF ON KHF.ID_HASIL_FORM=KHS.ID_HASIL_FORM
            WHERE KS.ID_KATEGORI_SOAL='{$kategori_soal}' AND KHF.ID_PENGGUNA='{$id_pengguna}'");
    }

    function cek_soal_belum_terjawab($id_pengguna) {
        $text = '';
        $data_hasil = $this->db->QueryToArray("
            SELECT * FROM KUISIONER_KATEGORI_SOAL 
            WHERE ID_FORM=1 AND ID_KATEGORI_SOAL NOT IN
            (SELECT KS.ID_KATEGORI_SOAL FROM KUISIONER_HASIL_SOAL KHS
            JOIN KUISIONER_HASIL_FORM KHF ON KHF.ID_HASIL_FORM=KHS.ID_HASIL_FORM
            JOIN KUISIONER_SOAL KS ON KHS.ID_SOAL=KS.ID_SOAL
            WHERE KHF.ID_PENGGUNA='{$id_pengguna}')");

        foreach ($data_hasil as $data) {
            $text .= '&nbsp;&nbsp;' . $data['KODE_KATEGORI'] . ' ' . $data['NM_KATEGORI'] . '<br/>';
        }
        return $text;
    }

}

?>
