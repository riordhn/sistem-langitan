<?php
	include '../../config.php';
	$id_program_studi = $user->ID_PROGRAM_STUDI;
	$cur_id_pengguna = $user->ID_PENGGUNA;
	
	$id_pengguna = array();
	$username = array();
	$nm_pengguna = array();
	$prodi = array();
	$jenjang = array();
	$label = array();
	$jml_teman = 0;
	if(isset($_GET['cari']))
	$cari = $_GET['cari'];
	else $cari="";
	$db->Query("select mhs.id_pengguna, p.username, p.nm_pengguna, ps.nm_program_studi, j.nm_jenjang from mahasiswa mhs
				left join pengguna p on p.id_pengguna = mhs.id_pengguna
				left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
				left join jenjang j on j.id_jenjang = ps.id_jenjang
				where upper(p.nm_pengguna) = '$cari'
	");
	
	
	$i = 0;
	while ($row = $db->FetchRow()){ 
			$id_pengguna[$i] = $row[0];
			$username[$i] = $row[1];
			$nm_pengguna[$i] = $row[2];
			$prodi[$i] = $row[3];
			$jenjang[$i] = $row[4];
			
			$label[$i] = $nm_pengguna[$i] . ' [ ' . $jenjang[$i] . ' ]' . ' ' . $prodi[$i];
			$jml_teman++;
		$i++;
	}

	$smarty->assign('cur_id_pengguna', $cur_id_pengguna);
	$smarty->assign('id_pengguna', $id_pengguna);	
	$smarty->assign('username', $username);
	$smarty->assign('nm_pengguna', $nm_pengguna);
	$smarty->assign('label', $label);
	$smarty->assign('jml_teman', $jml_teman);
	
	$smarty->display('sn_hasil_pencarian.tpl');
?>