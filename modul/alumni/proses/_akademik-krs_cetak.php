<?php
require('../../../config.php');
$db2 = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'F4', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Transkrip');
$pdf->SetSubject('Transkrip Mahasiswa');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "20";
$title = "UNIVERSITAS AIRLANGGA";

$mhs_nama="";
$kueri = "select nm_pengguna from pengguna where id_pengguna='".$user->ID_PENGGUNA."'";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$mhs_nama = $r[0];
}

// ambil id_mhs
$id_mhs=""; $mhs_nim=""; $mhs_fakul=""; $mhs_prodi=""; $mhs_jenjang=""; $id_prodi=""; $id_jenjang="";  $id_fakultas="";
$kueri = "
select a.id_mhs, a.NIM_MHS, c.nm_fakultas, b.nm_program_studi, d.nm_jenjang, a.id_program_studi, b.id_fakultas, b.id_jenjang
from mahasiswa a, program_studi b, fakultas c, jenjang d
where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and b.id_jenjang=d.id_jenjang and a.id_pengguna='".$user->ID_PENGGUNA."'";
$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
while($r = $db->FetchRow()) {
	$id_mhs = $r[0];
	$mhs_nim = $r[1];
	$mhs_fakul = $r[2];
	$mhs_prodi = $r[3];
	$mhs_jenjang = $r[4];
	$id_prodi = $r[5];
	$id_fakultas = $r[6];
	$id_jenjang = $r[7];
}

// ambil semester_aktif
$sem_aktif=""; $sem_aktif_tahun=""; $sem_aktif_semes="";
$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where STATUS_AKTIF_SEMESTER='True' order by id_semester desc";
$result = $db->Query($kueri)or die ("salah kueri : ".$kueri);
while($r = $db->FetchRow()) {
	$sem_aktif = $r[0];
	$sem_aktif_tahun = $r[1];
	$sem_aktif_semes = $r[2];
}

// ambil sks terambil
$sks_terambil=0;
$kueri = "
select sum(d.kredit_semester)
from pengambilan_mk a, kelas_mk b, mata_kuliah c, kurikulum_mk d
where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah
and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' and a.STATUS_APV_PENGAMBILAN_MK ='1'
";
$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
while($r = $db->FetchRow()) {
	$sks_terambil = $r[0];
}
if(strlen($sks_terambil)==0) { $sks_terambil = '0'; }

	$kemarin_thn=""; $kemarin_sem="";
	if($sem_aktif_semes=="Ganjil") {
		$kemarin_thn = $sem_aktif_tahun-1;
		$kemarin_sem = "Genap";
	}else if($sem_aktif_semes=="Genap") {
		$kemarin_thn = $sem_aktif_tahun;
		$kemarin_sem = "Ganjil";
	}
	
	// ambil semester_kemarin
		$kemarin_idsem="";
		$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		while($r = $db->FetchRow()) {
			$kemarin_idsem = $r[0];
		}

		// apakah semester tsb cuti ?
		$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		$r = $db->FetchRow();
		if($r[0]>0) { // maka cuti ke-1
			// ambil semester sebelum cuti
			$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$sem_aktif_tahun = $r[1];
				$sem_aktif_semes = $r[2];
			}
			if($sem_aktif_semes=="Ganjil") {
				$kemarin_thn = $sem_aktif_tahun-1;
				$kemarin_sem = "Genap";
			}else if($sem_aktif_semes=="Genap") {
				$kemarin_thn = $sem_aktif_tahun;
				$kemarin_sem = "Ganjil";
			}
			$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$kemarin_idsem = $r[0];
			}
			// apakah semester tsb cuti ?
			$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			$r = $db->FetchRow();
			if($r[0]>0) { // maka cuti ke-2
				// ambil semester sebelum cuti
				$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$sem_aktif_tahun = $r[1];
					$sem_aktif_semes = $r[2];
				}
				if($sem_aktif_semes=="Ganjil") {
					$kemarin_thn = $sem_aktif_tahun-1;
					$kemarin_sem = "Genap";
				}else if($sem_aktif_semes=="Genap") {
					$kemarin_thn = $sem_aktif_tahun;
					$kemarin_sem = "Ganjil";
				}
				$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$kemarin_idsem = $r[0];
				}
			}
		}
	
	// ambil sks_maks
	$sks_maks = 0; $sks_total = 0; $ipk_mhs = 0; $ips_mhs = 0;
	$ips_atas='0'; $ips_bawah='0';
	$bobot["A"] = 4; $bobot["AB"] = 3.5; $bobot["B"] = 3; $bobot["BC"] = 2.5; $bobot["C"] = 2; $bobot["D"] = 1; $bobot["E"] = 0;
	$kueri2 = "
		select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester, d.nilai_huruf
		from mata_kuliah a, semester b, pengambilan_mk d, kurikulum_mk e
		where d.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and d.id_semester=b.id_semester
		and b.id_semester='".$kemarin_idsem."' and d.id_mhs='".$id_mhs."' and d.nilai_huruf is not null
	";
	$result = $db->Query($kueri2)or die("salah kueri : 4");
	while($r = $db->FetchRow()) {
		if(strtoupper($r[1])=='KKN' or strtoupper($r[1])=='KULIAH KERJA NYATA' or strtoupper($r[1])=='SKRIPSI' or strtoupper($r[1])=='TUGAS AKHIR' or strtoupper($r[1])=='PRAKTEK KERJA LAPANGAN' or strtoupper($r[1])=='PKL' or strtoupper($r[1])=='PKL (MAGANG PRAKTEK KERJA LAPANGAN)' or strtoupper($r[1])=='PRAKTEK KERJA LAPANGAN (PKL)' or strtoupper($r[1])=='RESIDENSI DAN PRAKTEK KERJA LAPANGAN (PKL)') {
			if($r[3]=="E") {
				// tidak dihitung
			}else{
				$ips_bawah += $r[2];
				$ips_atas += ($bobot[$r[3]]*$r[2]);
			}
		}else{
			$ips_bawah += $r[2];
			$ips_atas += ($bobot[$r[3]]*$r[2]);
		}
	}
	if($ips_bawah>0) {
		$ips_mhs = number_format(($ips_atas/$ips_bawah),2);
	}else{
		$ips_mhs = '0.00';
	}
	// ambil sks sems depan
		$kueri2 = "select max(sks_maksimal) as sks_maksimal from beban_sks where id_program_studi = ".$id_prodi." and ipk_minimum <= ".$ips_mhs."";
		$result = $db->Query($kueri2)or die("salah kueri : 131");
		while($r = $db->FetchRow()) {
			$sks_maks = $r[0];
		}
		if($id_fakultas=='2') {
			// ambil tingkat semester
			$thn_masuk="0";
			if(substr($mhs_nim,2,2) < 50) {
				$thn_masuk = "20".substr($mhs_nim,2,2);
			}else if(substr($mhs_nim,2,2) > 50) {
				$thn_masuk = "19".substr($mhs_nim,2,2);
			}
			$tingkat_semester='0';
			if(strtoupper($sem_aktif_semes)=="GANJIL") {
				$tingkat_semester = (($sem_aktif_tahun-$thn_masuk)*2)+1;
			}else if(strtoupper($sem_aktif_semes)=="GENAP") {
				$tingkat_semester = (($sem_aktif_tahun-$thn_masuk)*2)+2;
			}

			if($tingkat_semester<=4) {
				$sks_maks = '22';
			}
		}else if($id_fakultas=='3') {
			$sks_maks = $sks_maks+1;
		}else if($id_fakultas=='4') {
			if($id_jenjang=='1') {
				if(strlen($sks_maks)==0) { 
					$sks_maks = '0'; 
				}else{
					$sks_maks=$sks_maks+1; 
				}
			}else if($id_jenjang=='5') {
				if(strlen($sks_maks)==0) { 
					$sks_maks = '0'; 
				}else{
					$sks_maks=$sks_maks+2;
				}
			}
		}else if($id_fakultas=='5') {
			$sks_maks = $sks_maks+2;
		}
		if($sks_maks>24) {
			$sks_maks=24;
		}
/*
	//perhitungan sks total dan ipk versi lukman
		$sql="select a.id_mhs,sum(a.kredit_semester) skstotal,
		round(sum(a.kredit_semester*(case a.nilai_huruf 
		when 'A' then 4 
		when 'AB' then 3.5 
		when 'B' then 3
		when 'BC' then 2.5
		when 'C' then 2
		when 'D' then 1
		end))/sum(a.kredit_semester),2) IPK
		from
		(
			select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
				select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
				from pengambilan_mk a 
				left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
				left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
				where a.nilai_huruf<'E' and a.nilai_huruf is not null
			) a
			left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
			left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
			where rangking=1 and id_mhs='{$id_mhs}'
		) a
		left join mahasiswa b on a.id_mhs=b.id_mhs
		left join program_studi f on b.id_program_studi=f.id_program_studi
		where a.id_mhs='{$id_mhs}'
		group by a.id_mhs order by a.id_mhs";
*/
		//perhitungan sks total dan ipk versi lukman
		$sql="select a.id_mhs,sum(a.kredit_semester) skstotal,
		round(sum(a.kredit_semester*(case a.nilai_huruf 
		when 'A' then 4 
		when 'AB' then 3.5 
		when 'B' then 3
		when 'BC' then 2.5
		when 'C' then 2
		when 'D' then 1
		end))/sum(a.kredit_semester),2) IPK
		from
		(
		select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
		select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
		from pengambilan_mk a 
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null and a.flagnilai=1
		) a
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where rangking=1 and id_mhs='{$id_mhs}'
		) a
		left join mahasiswa b on a.id_mhs=b.id_mhs
		left join program_studi f on b.id_program_studi=f.id_program_studi
		where a.id_mhs='{$id_mhs}'
		group by a.id_mhs order by a.id_mhs";
		$result2 = $db->Query($sql);
		while($r2 = $db->FetchRow()) {
			$sks_total = $r2[1];
			$ipk_mhs = $r2[2];
		}

$content = "FAKULTAS ".strtoupper($mhs_fakul)."\n";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage();

/* NOTE:
 * *********************************************************
 * You can load external XHTML using :
 *
 * $html = file_get_contents('/path/to/your/file.html');
 *
 * External CSS files will be automatically loaded.
 * Sometimes you need to fix the path of the external CSS.
 * *********************************************************
 */

// define some HTML content with style
//$html = file_get_contents('gui/transkrip-cetak.tpl');

$html = '
<p>&nbsp;</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:8x;">
  <tr>
    <td colspan="7" align="center"><p><strong>KRS AKADEMIK</strong></p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
           <td width="12%">Nama</td>
           <td width="1%"><strong>: </strong></td>
           <td width="33%"><strong>'.$mhs_nama.'</strong></td>
           <td width="18%">&nbsp;</td>
           <td width="15%">IPK</td>
           <td width="1%"><strong>:</strong></td>
           <td width="20%"><strong>'.$ipk_mhs.'</strong></td>
  </tr>
         <tr>
           <td>NIM</td>
           <td><strong>: </strong></td>
           <td><strong>'.$mhs_nim.'</strong></td>
           <td>&nbsp;</td>
           <td>IPS</td>
           <td><strong>: </strong></td>
           <td><strong>'.$ips_mhs.'</strong></td>
         </tr>
         <tr>
           <td>Program Studi</td>
           <td><strong>: </strong></td>
           <td><strong>'.$mhs_jenjang.' '.$mhs_prodi.'</strong></td>
           <td>&nbsp;</td>
           <td>Sks Maks</td>
           <td><strong>: </strong></td>
           <td><strong>'.$sks_maks.'</strong></td>
         </tr>
</table>
<p>&nbsp;</p>
<table border="0" width="100%">
<tr>
<td width="100%">
<table cellspacing="0" cellpadding="3" border="1" width="100%" style="font-size:25px;">
  <thead>
    <tr bgcolor="#000000" style="color:#FFF;">
      <td width="20%" align="center" bgcolor="#333333"><font color="#FFFFFF">Kode MA</font></td>
      <td width="42%" align="center" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Ajar</font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">Kelas</font></td>
    </tr>
  </thead>
  <tbody>
';
/*
$kueri = "
select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester,d.id_pengambilan_mk,c.no_kelas_mk,d.STATUS_APV_PENGAMBILAN_MK
from mata_kuliah a, kelas_mk c, pengambilan_mk d, kurikulum_mk e
where c.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and c.id_semester=d.id_semester and c.id_kelas_mk=d.id_kelas_mk
and c.id_semester='".$sem_aktif."' and d.id_mhs='".$id_mhs."' and d.STATUS_APV_PENGAMBILAN_MK='1'
order by a.kd_mata_kuliah
";
*/
$kueri = "
select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester,d.id_pengambilan_mk,c.no_kelas_mk,d.STATUS_APV_PENGAMBILAN_MK
from mata_kuliah a, kelas_mk c, pengambilan_mk d, kurikulum_mk e
where c.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and c.id_kelas_mk=d.id_kelas_mk
and d.id_semester='".$sem_aktif."' and d.id_mhs='".$id_mhs."' and d.STATUS_APV_PENGAMBILAN_MK='1'
order by a.kd_mata_kuliah
";
$hit=0; $tot_sks=0;
$result = $db->Query($kueri)or die("salah kueri 31 ");
while($r = $db->FetchRow()) {
	$hit++;
	$tot_sks += $r[2];
	$nm_kelas = "";
	$result2 = $db2->Query("select nama_kelas from nama_kelas where id_nama_kelas='".$r[4]."'")or die("salah kueri 42 ");
	while($r2 = $db2->FetchRow()) {
		$nm_kelas = $r2[0];
	}
	$html .= '
		<tr>
			<td width="20%">'.$r[0].'</td>
			<td width="42%">'.$r[1].'</td>
			<td width="19%" align="center" >'.$r[2].'</td>
			<td width="19%" align="center" >'.$nm_kelas.'</td>
		</tr>
	';
}
$html .= '
	<tr>
		<td colspan="2" align="center">Total SKS</td>
		<td width="19%" align="center">'.$tot_sks.'</td>
		<td width="19%" align="center"></td>
	</tr>
  </tbody>
</table>
</td>
</tr>
</table>
<p>&nbsp;</p>
';
// ambil dosen wali
$sem_aktif="";
/*
$kueri = "select b.nip_dosen,c.nm_pengguna from dosen_wali a, dosen b, pengguna c
where a.id_dosen=b.id_dosen and b.id_pengguna=c.id_pengguna and a.id_mhs='".$id_mhs."'
";
*/
$kueri = "
select b.nip_dosen,c.nm_pengguna
from dosen_wali a, dosen b, pengguna c, semester d
where a.id_dosen=b.id_dosen and b.id_pengguna=c.id_pengguna and a.id_semester=d.id_semester and a.id_mhs='".$id_mhs."'
order by d.thn_akademik_semester desc, d.nm_semester desc
";
$result = $db->Query($kueri)or die ("salah kueri 48 ");
//while($r = $db->FetchRow()) {
//	$wali_nip = $r[0];
//	$wali_nama = $r[1];
//}
$r = $db->FetchRow();
$wali_nip = $r[0];
$wali_nama = $r[1];

$bulan['01'] = "Januari"; $bulan['02'] = "Februari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "November";  $bulan['12'] = "Desember";
$html .= '
<table border="0" width="100%" style="font-size:30px;">
  <tr valign="top">
    <td width="50%">&nbsp;</td>
    <td width="50%" rowspan="3"><p align="center">Surabaya, '.date("d").' '.$bulan[date("m")].' '.date("Y").'</p>
      <p align="center">Dosen Wali,</p>
      <p align="center">&nbsp;</p>
      <p align="center">&nbsp;</p>
      <p align="center">'.$wali_nama.'</p>
      <p align="center">NIP. '.$wali_nip.'</p>      <div align="center"></div>      <div align="center"></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>

';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// *******************************************************************
// HTML TIPS & TRICKS
// *******************************************************************

// REMOVE CELL PADDING
//
// $pdf->SetCellPadding(0);
// 
// This is used to remove any additional vertical space inside a 
// single cell of text.

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// REMOVE TAG TOP AND BOTTOM MARGINS
//
// $tagvs = array('p' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)));
// $pdf->setHtmlVSpace($tagvs);
// 
// Since the CSS margin command is not yet implemented on TCPDF, you
// need to set the spacing of block tags using the following method.

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// SET LINE HEIGHT
//
// $pdf->setCellHeightRatio(1.25);
// 
// You can use the following method to fine tune the line height
// (the number is a percentage relative to font height).

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// CHANGE THE PIXEL CONVERSION RATIO
//
// $pdf->setImageScale(0.47);
// 
// This is used to adjust the conversion ratio between pixels and 
// document units. Increase the value to get smaller objects.
// Since you are using pixel unit, this method is important to set the
// right zoom factor.
// 
// Suppose that you want to print a web page larger 1024 pixels to 
// fill all the available page width.
// An A4 page is larger 210mm equivalent to 8.268 inches, if you 
// subtract 13mm (0.512") of margins for each side, the remaining 
// space is 184mm (7.244 inches).
// The default resolution for a PDF document is 300 DPI (dots per 
// inch), so you have 7.244 * 300 = 2173.2 dots (this is the maximum 
// number of points you can print at 300 DPI for the given width).
// The conversion ratio is approximatively 1024 / 2173.2 = 0.47 px/dots
// If the web page is larger 1280 pixels, on the same A4 page the 
// conversion ratio to use is 1280 / 2173.2 = 0.59 pixels/dots

// *******************************************************************

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('transkrip.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+
