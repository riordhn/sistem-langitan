<!-- Author : Seto Priyanggoro -->

<!-- File ini berisi proses dari menu kalender akademik -->
<form name="frm">
    <table width="600" border="none">
		<td>Pilih Dosen </td>
		<td>
			<select>
				<option value="L">Drs. Kartono, M.Kom</option>
				<option value="P">Ir. Dyah Herawatie, M.Si</option>
			</select>
		</td>
	</table>
</form>	
<p align="center"><strong>EVALUASI KINERJA DOSEN</strong></p>
<p align="center"><strong>DALAM PERWALIAN</strong></p>
<form name="f2">
<table width="700" border="1">
  <tr>
    <td width="5%" rowspan="2" align="center" valign="middle" bgcolor="#33CC33"><strong>No</strong></td>
    <td width="35%" rowspan="2" align="center" valign="middle" bgcolor="#33CC33"><strong>Aspek yang dinilai</strong></td>
    <td colspan="5" align="center" bgcolor="#33CC33"><strong>Skala</strong></td>
  </tr>
  <tr>
    <td width="12%" align="center" bgcolor="#33CC33"><strong>1</strong></td>
    <td width="12%" align="center" bgcolor="#33CC33"><strong>2</strong></td>
    <td width="12%" align="center" bgcolor="#33CC33"><strong>3</strong></td>
    <td width="12%" align="center" bgcolor="#33CC33"><strong>4</strong></td>
    <td width="12%" align="center" bgcolor="#33CC33"><strong>0*</strong></td>
  </tr>
  <tr>
    <td>1.</td>
    <td>Perwalian bermanfaat dalam perencanaan studi mahasiswa.</td>
    <td><input type="radio" name="radio" id="radio" value="radio" />
      Tidak setuju</td>
    <td><input type="radio" name="radio" id="radio2" value="radio" />
      Kurang setuju</td>
    <td><input type="radio" name="radio" id="radio3" value="radio" />
      Setuju</td>
    <td><input type="radio" name="radio" id="radio4" value="radio" />
      Sangat Setuju</td>
    <td><input type="radio" name="radio" id="radio5" value="radio" />
      Tidak ada pendapat</td>
    </tr>
  <tr>
    <td>2.</td>
    <td>Perwalian bermanfaat dalam penyelesaian permasalahan akademik mahasiswa.</td>
    <td><input type="radio" name="radio" id="radio" value="radio" />
      Tidak setuju</td>
    <td><input type="radio" name="radio" id="radio2" value="radio" />
      Kurang setuju</td>
    <td><input type="radio" name="radio" id="radio3" value="radio" />
      Setuju</td>
    <td><input type="radio" name="radio" id="radio4" value="radio" />
      Sangat Setuju</td>
    <td><input type="radio" name="radio" id="radio5" value="radio" />
      Tidak ada pendapat</td>
    </tr>
</table>
</form>
<p><strong>* Tidak ada pendapat dipilih jika anda tidak dapat menilai atau tidak paham item evaluasi yang dimaksud.</strong></p>
<p>
  <input type="button" value="Simpan" />
</p>
