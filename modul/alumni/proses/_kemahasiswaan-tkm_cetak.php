<?php
require('../../../config.php');

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 006');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content
$html = '<table width="100%" align="center" cellpadding="0">
<tr>
	<td>
	<img src="../../../img/akademik_images/logounair.gif" width="70" height="70"><br><BR>
	<B>UNIVERSITAS AIRLANGGA<BR>
';
	// ambil Fakultas
	$kueri = "select c.nm_fakultas from mahasiswa a, program_studi b, fakultas c where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$fakultas = $r[0];
	}
	// ambil nama mhs
	$kueri = "select nm_pengguna from pengguna where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$nm_pengguna = $r[0];
	}
	// ambil nim mhs
	$kueri = "select a.nim_mhs, b.nm_program_studi,a.id_mhs from mahasiswa a, program_studi b where a.id_program_studi=b.id_program_studi and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$nim = $r[0];
		$prodi = $r[1];
		$idmhs = $r[2];
	}
$html .= '
	FAKULTAS '.strtoupper($fakultas).'<BR>
	TRANSKRIP KEGIATAN MAHASISWA (TKM)</B>
	</td>
</tr>
</table>
<br>
<table width="98%" cellpadding="0" style="font-size:8x;">
<tr>
	<td width="15%">Nama</td>
	<td width="1%"></td>
	<td>'.$nm_pengguna.'</td>
</tr>
<tr>
	<td>NIM</td>
	<td>:</td>
	<td>'.$nim.'</td>
</tr>
<tr>
	<td>Prog.Studi</td>
	<td>:</td>
	<td>'.$prodi.'</td>
</tr>		
</table>
<br>
<table width="98%" border="1" cellpadding="2" cellspacing="0" style="font-size:8x;">
<tr>
	<td width="3%" align="center"><b>No</b></td>
	<td width="87%" align="center"><b>Kriteria Kegiatan</b></td>
	<td width="10%" align="center"><b>Nilai skp</b></td>
</tr>
<tr>
	<td align="center">A</td>
	<td>Wajib Universitas</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td align="center">&nbsp;</td>
	<td>1. PPKMB</td>
	<td align="center">
	';
	$jumlah_skp=0;
	// ambil nilai kegiatan
	$kueri = "
	select sum(a.skor_krp_khp) 
	from krp_khp a, kegiatan_2 b, kegiatan_1 c
	where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_kegiatan_1=c.id_kegiatan_1 
	and c.nm_kegiatan_1='PPKMB' and id_mhs='".$idmhs."' ";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		if(strlen($r[0])==0) {
			$html .= '0';
		}else{
			$html .= $r[0];
			$jumlah_skp +=$r[0];
		}
	}
	$html .= '
	</td>
</tr>			
<tr>
	<td align="center">&nbsp;</td>
	<td>2. KKN-BBM</td>
	<td align="center">
	';
	// ambil nilai kegiatan
	$kueri = "
	select sum(a.skor_krp_khp) 
	from krp_khp a, kegiatan_2 b, kegiatan_1 c
	where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_kegiatan_1=c.id_kegiatan_1 
	and c.nm_kegiatan_1='KKN-BBM' and id_mhs='".$idmhs."' ";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		if(strlen($r[0])==0) {
			$html .= '0';
		}else{
			$html .= $r[0];
			$jumlah_skp +=$r[0];
		}
	}
	$html .= '
	</td>
</tr>			
<tr>
	<td align="center">B</td>
	<td>Pilihan</td>
	<td>&nbsp;</td>
</tr>		
<tr>
	<td align="center">&nbsp;</td>
	<td>1. Kegiatan Bidang Organisasi dan Kepemimpinan</td>
	<td align="center">
	';
	// ambil nilai kegiatan
	$kueri = "
	select sum(a.skor_krp_khp) 
	from krp_khp a, kegiatan_2 b, kegiatan_1 c
	where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_kegiatan_1=c.id_kegiatan_1 
	and c.id_kelompok_kegiatan='2' and id_mhs='".$idmhs."' ";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		if(strlen($r[0])==0) {
			$html .= '0';
		}else{
			$html .= $r[0];
			$jumlah_skp +=$r[0];
		}
	}
	$html .= '
	</td>
</tr>			
<tr>
	<td align="center">&nbsp;</td>
	<td>2. Kegiatan Bidang Penalaran dan Keilmuan</td>
	<td align="center">
	';
	// ambil nilai kegiatan
	$kueri = "
	select sum(a.skor_krp_khp) 
	from krp_khp a, kegiatan_2 b, kegiatan_1 c
	where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_kegiatan_1=c.id_kegiatan_1 
	and c.id_kelompok_kegiatan='3' and id_mhs='".$idmhs."' ";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		if(strlen($r[0])==0) {
			$html .= '0';
		}else{
			$html .= $r[0];
			$jumlah_skp +=$r[0];
		}
	}
	$html .= '
	</td>
</tr>			
<tr>
	<td align="center">&nbsp;</td>
	<td>3. Kegiatan Bidang Minat dan Bakat</td>
	<td align="center">
	';
	// ambil nilai kegiatan
	$kueri = "
	select sum(a.skor_krp_khp) 
	from krp_khp a, kegiatan_2 b, kegiatan_1 c
	where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_kegiatan_1=c.id_kegiatan_1 
	and c.id_kelompok_kegiatan='4' and id_mhs='".$idmhs."' ";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		if(strlen($r[0])==0) {
			$html .= '0';
		}else{
			$html .= $r[0];
			$jumlah_skp +=$r[0];
		}
	}
	$html .= '
	</td>
</tr>			
<tr>
	<td align="center">&nbsp;</td>
	<td>4. Kegiatan Bidang Kepedulian Sosial</td>
	<td align="center">
	';
	// ambil nilai kegiatan
	$kueri = "
	select sum(a.skor_krp_khp) 
	from krp_khp a, kegiatan_2 b, kegiatan_1 c
	where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_kegiatan_1=c.id_kegiatan_1 
	and c.id_kelompok_kegiatan='5' and id_mhs='".$idmhs."' ";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		if(strlen($r[0])==0) {
			$html .= '0';
		}else{
			$html .= $r[0];
			$jumlah_skp +=$r[0];
		}
	}
	$html .= '
	</td>
</tr>
<tr>
	<td align="center">&nbsp;</td>
	<td>5. Kegiatan Lainnya</td>
	<td align="center">
	';
	// ambil nilai kegiatan
	$kueri = "
	select sum(a.skor_krp_khp) 
	from krp_khp a, kegiatan_2 b, kegiatan_1 c
	where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_kegiatan_1=c.id_kegiatan_1 
	and c.id_kelompok_kegiatan='6' and id_mhs='".$idmhs."' ";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		if(strlen($r[0])==0) {
			$html .= '0';
		}else{
			$html .= $r[0];
			$jumlah_skp +=$r[0];
		}
	}
	$html .= '
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td align="right">Jumlah Perolehan skp</td>
	<td align="center">'.$jumlah_skp.'</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td align="right">Predikat</td>
	<td align="center">Sangat Baik</td>
</tr>
</table>
<br>
<table width="98%" cellpadding="0" cellspacing="0">
<tr>
	<td valign="top" width="60%">
		<div>
		<font size="8">Catatan :</font><br>
		<b><font size="8">Predikat SKP S1:</font></b>
		</div>
		<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td width="20%"><font size="8">Sangat Baik</font></td>
			<td width="2%">:</td>
			<td width="78%"><font size="8"> &gt; 250 skp</font></td>
		</tr>
		<tr>
			<td><font size="8">Baik</font></td>
			<td>:</td>
			<td><font size="8">201 - 250</font></td>
		</tr>
		<tr>
			<td><font size="8">Cukup</font></td>
			<td>:</td>
			<td><font size="8">100 - 200</font></td>
		</tr>
		</table><br>
		<div>
		<b><font size="8">Predikat SKP D3:</font></b>
		</div>
		<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td width="20%"><font size="8">Sangat Baik</font></td>
			<td width="2%">:</td>
			<td width="78%"><font size="8"> &gt; 200 skp</font></td>
		</tr>
		<tr>
			<td><font size="8">Baik</font></td>
			<td>:</td>
			<td><font size="8">101 - 200</font></td>
		</tr>
		<tr>
			<td><font size="8">Cukup</font></td>
			<td>:</td>
			<td><font size="8">75 - 100</font></td>
		</tr>
		</table>
	</td>
	<td valign="top" width="40%" align="center">
		<br><br>
		<font size="8">
		Surabaya, 11-05-2011<br>
				
		WADEK I<br>
		<br><br><br><br>
		Dr. R. Darmawan Setijanto, drg.,M.Kes<br>
		NIP : 196110051988031003
		</font>
	</td>
</tr>
</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('transkrip.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+
