<?php
require('../../../config.php');
$db2 = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'F4', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Transkrip');
$pdf->SetSubject('Transkrip Mahasiswa');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "20";
$title = "UNIVERSITAS AIRLANGGA";

$mhs_nama=""; $mhs_tgllahir=""; $mhs_tmplahir="";
$kueri = "select nm_pengguna, TGL_LAHIR_PENGGUNA, TEMPAT_LAHIR from pengguna where id_pengguna='".$user->ID_PENGGUNA."'";
$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
while($r = $db->FetchRow()) {
	$mhs_nama = $r[0];
	$mhs_tgllahir = $r[1];
	$mhs_tmplahir = $r[2];
}

// ambil id_mhs
$id_mhs=""; $mhs_nim=""; $mhs_fakul=""; $mhs_prodi=""; $mhs_jenjang=""; $mhs_tgl_lulus=""; $mhs_tgl_terdaftar="";
$kueri = "
select a.id_mhs, a.NIM_MHS, c.nm_fakultas, b.nm_program_studi, d.nm_jenjang, a.TGL_LULUS_MHS, a.TGL_TERDAFTAR_MHS, a.no_ijazah
from mahasiswa a, program_studi b, fakultas c, jenjang d
where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and b.id_jenjang=d.id_jenjang and a.id_pengguna='".$user->ID_PENGGUNA."'";
$result = $db->Query($kueri)or die("salah kueri 3: ".$kueri);
while($r = $db->FetchRow()) {
	$id_mhs = $r[0];
	$mhs_nim = $r[1];
	$mhs_fakul = $r[2];
	$mhs_prodi = $r[3];
	$mhs_jenjang = $r[4];
	$mhs_tgl_lulus = $r[5];
	$mhs_tgl_terdaftar = $r[6];
	$mhs_noijazah = $r[7];
}

$content = "FAKULTAS ".strtoupper($mhs_fakul)."\nKampus C Mulyorejo Surabaya 60115\nTelp. (031) 5914042, 5914043, 5912564 Fax (031) 5981841\nWebsite : http://www.unair.ac.id ; e-mail : rektor@unair.ac.id";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage();

/* NOTE:
 * *********************************************************
 * You can load external XHTML using :
 *
 * $html = file_get_contents('/path/to/your/file.html');
 *
 * External CSS files will be automatically loaded.
 * Sometimes you need to fix the path of the external CSS.
 * *********************************************************
 */

// define some HTML content with style
//$html = file_get_contents('gui/transkrip-cetak.tpl');

$html = '
<style>
table.transkrip{
	border:#999 1px solid;
	font-size:9px;
	border-collapse:collapse;}
</style>
<p>&nbsp;</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:8x;">
  <tr>
    <td colspan="7" align="center"><p><strong>TRANSKRIP AKADEMIK</strong></p>
    <p><strong>NO : ...</strong></p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
           <td width="17%">Nama</td>
           <td width="1%"><strong>: </strong></td>
           <td width="24%"><strong>'.$mhs_nama.'</strong></td>
           <td width="2%">&nbsp;</td>
           <td width="23%">Nomor Ijazah</td>
           <td width="1%"><strong>:</strong></td>
           <td width="32%"><strong>'.$mhs_noijazah.'</strong></td>
  </tr>
         <tr>
           <td width="17%">NIM</td>
           <td width="1%"><strong>: </strong></td>
           <td width="24%"><strong>'.$mhs_nim.'</strong></td>
           <td width="2%">&nbsp;</td>
           <td >Tanggal Terdaftar Pertama Kali</td>
           <td ><strong>: </strong></td>
           <td ><strong>'.$mhs_tgl_terdaftar.'</strong></td>
         </tr>
         <tr>
           <td width="17%">Program Studi</td>
           <td width="1%"><strong>: </strong></td>
           <td width="24%"><strong>'.$mhs_jenjang.' '.$mhs_prodi.'</strong></td>
           <td width="2%">&nbsp;</td>
           <td >Tanggal Lulus</td>
           <td ><strong>: </strong></td>
           <td ><strong>'.$mhs_tgl_lulus.'</strong></td>
         </tr>
         <tr>
           <td >Tempat, Tanggal Lahir</td>
           <td ><strong>: </strong></td>
           <td ><strong>'.$mhs_tmplahir.', '.$mhs_tgllahir.'</strong></td>
           <td >&nbsp;</td>
           <td >Judul Skripsi/ Tesis/ Disertasi</td>
           <td ><strong>: </strong></td>
           <td ><strong></strong></td>
         </tr>
</table>
<p>&nbsp;</p>
<table border="0" width="100%">
<tr>
<td width="49%">
';
	$batasan = '0';
	$kueri = "
	select count(*)
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_mhs='".$id_mhs."'
	order by a.id_semester,c.kd_mata_kuliah
	";
	$result = $db->Query($kueri)or die("salah kueri 4: ");
	while($r = $db->FetchRow()) {
		$batasan = $r[0];
	}
	$separuh = ceil($batasan/2);

$html .= '
<table cellspacing="0" cellpadding="1" border="1" width="100%" style="font-size:25px;">
  <thead>
    <tr bgcolor="#000000" style="color:#FFF;">
      <td width="20%" align="center" bgcolor="#333333"><font color="#FFFFFF">Kode MA</font></td>
      <td width="42%" align="center" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Ajar </font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">Nilai</font></td>
    </tr>
  </thead>
  <tbody>
  ';
	$jum_sks=0; $jum_bobot=0; $ipk=0; $muter=0;
	$kueri = "
	select a.id_semester,c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_mhs='".$id_mhs."'
	order by a.id_semester,c.kd_mata_kuliah
	";
	$result = $db->Query($kueri)or die("salah kueri 5: ");
	while($r = $db->FetchRow()) {
		$muter++;
		// ambil bobot nilai huruf
		$bobot=0;
		$kueri2 = "select NILAI_STANDAR_NILAI from standar_nilai where NM_STANDAR_NILAI='".$r[4]."'";
		$result2 = $db2->Query($kueri2)or die("salah kueri : ");
		while($r2 = $db2->FetchRow()) {
			$bobot = $r2[0];
		}
		$html .= '
		 <tr>
		  <td width="20%">'.$r[1].'</td>
		  <td width="42%">'.$r[2].'</td>
		  <td width="19%" align="center">'.$r[3].'</td>
		  <td width="19%" align="center">'.$r[4].'</td>
		</tr>
		';
		$jum_sks += $r[3];
		$jum_bobot += ($bobot*$r[3]);
		if($muter==$separuh) {
			break;
		}
	}
  $html .= '
  </tbody>
</table>
</td>
<td width="1%">&nbsp;</td>
<td width="49%">
<table cellspacing="0" cellpadding="1" border="1" width="100%" style="font-size:25px;">
  <thead>
    <tr bgcolor="#000000" style="color:#FFF;">
      <td width="20%" align="center" bgcolor="#333333"><font color="#FFFFFF">Kode MA</font></td>
      <td width="42%" align="center" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Ajar </font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">Nilai</font></td>
    </tr>
  </thead>
  <tbody>
  ';
	$kueri = "
	select a.id_semester,c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_mhs='".$id_mhs."'
	order by a.id_semester,c.kd_mata_kuliah
	";
	$muter=0;
	$result = $db->Query($kueri)or die("salah kueri 6: ");
	while($r = $db->FetchRow()) {
		$muter++;
		if($muter>$separuh) {
			// ambil bobot nilai huruf
			$bobot=0;
			$kueri2 = "select NILAI_STANDAR_NILAI from standar_nilai where NM_STANDAR_NILAI='".$r[4]."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri 7: ");
			while($r2 = $db2->FetchRow()) {
				$bobot = $r2[0];
			}
			$html .= '
			 <tr>
			  <td width="20%">'.$r[1].'</td>
			  <td width="42%">'.$r[2].'</td>
			  <td width="19%" align="center">'.$r[3].'</td>
			  <td width="19%" align="center">'.$r[4].'</td>
			</tr>
			';
			$jum_sks += $r[3];
			$jum_bobot += ($bobot*$r[3]);
		}
	}
	if($jum_sks==0) {
		$ipk='0.00';
	}else{
		$ipk = number_format(($jum_bobot/$jum_sks),2);
	}
  $html .= '
  </tbody>
</table>
</td>
</tr>
</table>
<p>&nbsp;</p>
';
/*
$mhs_skstot = '0'; $mhs_ipk = '0';
$kueri = "select sks_total_mhs_status, ipk_mhs_status from mhs_status where id_mhs='".$id_mhs."' ";
$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
while($r = $db->FetchRow()) {
	$mhs_skstot = $r[0];
	$mhs_ipk = $r[1];
}
*/
$html .= '
<table border="0" width="100%" style="font-size:30px;;">
  <tr valign="top">
    <td width="50%">
    <table border="0" width="100%" style="font-size:24px;">
      <tr>
        <td width="46%"><div align="left">Kredit Yang Ditempuh</div></td>
        <td width="6%"><div align="left">:</div></td>
        <td width="48%"><div align="left">'.$jum_sks.' SKS</div></td>
      </tr>
      <tr>
        <td><div align="left">Index Prestasi Kumulatif</div></td>
        <td><div align="left">:</div></td>
        <td><div align="left">'.$ipk.'</div></td>
      </tr>
      <tr>
        <td><div align="left">Predikat Kelulusan</div></td>
        <td><div align="left">:</div></td>
        <td><div align="left">Dengan Pujian</div></td>
      </tr>
    </table></td>
    <td width="50%" rowspan="3"><p align="center">Surabaya, 04 April 2011</p>
      <p align="center">Dekan,</p>
      <p align="center">&nbsp;</p>
      <p align="center">&nbsp;</p>
      <p align="center">Prof. Dr. Muslich Anshori, SE., MSc.,Ak</p>
      <p align="center">NIP. 131570339</p>      <div align="center"></div>      <div align="center"></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table border="1" width="100%" style="font-size:22px;">
      <tr>
        <td width="30%"><div align="center">Nilai</div></td>
        <td width="20%"><div align="center">Bobot</div></td>
        </tr>
	';
$kueri = "select nm_standar_nilai, nilai_standar_nilai from standar_nilai order by nm_standar_nilai ";
$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
while($r = $db->FetchRow()) {
	$html .= '
		<tr>
			<td><div align="center">'.$r[0].'</div></td>
			<td><div align="center">'.$r[1].'</div></td>
		</tr>
	';
}
	$html .= '
    </table></td>
  </tr>
</table>

';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// *******************************************************************
// HTML TIPS & TRICKS
// *******************************************************************

// REMOVE CELL PADDING
//
// $pdf->SetCellPadding(0);
// 
// This is used to remove any additional vertical space inside a 
// single cell of text.

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// REMOVE TAG TOP AND BOTTOM MARGINS
//
// $tagvs = array('p' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)));
// $pdf->setHtmlVSpace($tagvs);
// 
// Since the CSS margin command is not yet implemented on TCPDF, you
// need to set the spacing of block tags using the following method.

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// SET LINE HEIGHT
//
// $pdf->setCellHeightRatio(1.25);
// 
// You can use the following method to fine tune the line height
// (the number is a percentage relative to font height).

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// CHANGE THE PIXEL CONVERSION RATIO
//
// $pdf->setImageScale(0.47);
// 
// This is used to adjust the conversion ratio between pixels and 
// document units. Increase the value to get smaller objects.
// Since you are using pixel unit, this method is important to set the
// right zoom factor.
// 
// Suppose that you want to print a web page larger 1024 pixels to 
// fill all the available page width.
// An A4 page is larger 210mm equivalent to 8.268 inches, if you 
// subtract 13mm (0.512") of margins for each side, the remaining 
// space is 184mm (7.244 inches).
// The default resolution for a PDF document is 300 DPI (dots per 
// inch), so you have 7.244 * 300 = 2173.2 dots (this is the maximum 
// number of points you can print at 300 DPI for the given width).
// The conversion ratio is approximatively 1024 / 2173.2 = 0.47 px/dots
// If the web page is larger 1280 pixels, on the same A4 page the 
// conversion ratio to use is 1280 / 2173.2 = 0.59 pixels/dots

// *******************************************************************

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('transkrip.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+
