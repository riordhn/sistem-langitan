<?php
require '../../../config.php';

if ($user->IsLogged())
{
    $ips_set = $db->QueryToArray("
        SELECT 
            a.id_semester, s.thn_akademik_semester, s.nm_semester,
            round(sum(D.KREDIT_SEMESTER*(select nilai_standar_nilai from standar_nilai sn where sn.nm_standar_nilai = a.nilai_huruf))/sum(D.KREDIT_SEMESTER),2) IPS
        FROM PENGAMBILAN_MK A
        LEFT JOIN KELAS_MK B ON (A.ID_KELAS_MK=B.ID_KELAS_MK)
        LEFT JOIN KURIKULUM_MK C ON (B.ID_KURIKULUM_MK=C.ID_KURIKULUM_MK)
        LEFT JOIN MATA_KULIAH D ON (D.ID_MATA_KULIAH=C.ID_MATA_KULIAH)
        LEFT JOIN MAHASISWA M ON M.ID_MHS=A.ID_MHS
        LEFT JOIN PENGGUNA P ON (P.ID_PENGGUNA=M.ID_PENGGUNA)
        join semester s on s.id_semester = a.id_semester
        WHERE P.ID_PENGGUNA={$user->PENGGUNA} AND A.FLAGNILAI=1 AND NOT (d.status_praktikum=2 and a.nilai_huruf='E')
        group by a.id_semester, s.thn_akademik_semester, s.nm_semester
        order by s.thn_akademik_semester asc, s.nm_semester asc
    ");
    
    $rows = array();
    foreach ($ips_set as $ips)
    {
        array_push($rows, "['{$ips['THN_AKADEMIK_SEMESTER']} {$ips['NM_SEMESTER']}', {$ips['IPS']}]");
    }
    $json_rows = "[".implode(",", $rows)."]";
}
?>

<!--
You are free to copy and use this sample in accordance with the terms of the
Apache license (http://www.apache.org/licenses/LICENSE-2.0.html)
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>
      Google Visualization API Sample
    </title>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        // Create and populate the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'x');
        data.addColumn('number', 'IPS');
        data.addRows(<?php echo $json_rows; ?>);
       
        // Create and draw the visualization.
        new google.visualization.LineChart(document.getElementById('visualization')).
            draw(data, {curveType: "function",
                        width: 600, height: 400,
                        vAxis: {maxValue: 4},
                        title: 'Grafik IPS'
                        }
                );
      }
      

      google.setOnLoadCallback(drawVisualization);
    </script>
  </head>
  <body style="font-family: Arial;border: 0 none; margin: 0px; padding: 0px;">
    <div id="visualization" style="width: 600px; height: 400px;"></div>
  </body>
</html>