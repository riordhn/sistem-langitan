<?php
require('../../../config.php');

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 006');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content
$html = '<table width="100%" align="center" cellpadding="0">
<tr>
	<td>
	<img src="../../../img/akademik_images/logounair.gif" width="70" height="70"><br><BR>
	<B>UNIVERSITAS AIRLANGGA<BR>
';
	// ambil Fakultas
	$kueri = "select c.nm_fakultas from mahasiswa a, program_studi b, fakultas c where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$fakultas = $r[0];
	}
	// ambil nama mhs
	$kueri = "select nm_pengguna from pengguna where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$nm_pengguna = $r[0];
	}
	// ambil nim mhs
	$kueri = "select a.nim_mhs, b.nm_program_studi, a.id_mhs from mahasiswa a, program_studi b where a.id_program_studi=b.id_program_studi and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$nim = $r[0];
		$prodi = $r[1];
		$id_mhs = $r[2];
	}
	$kueri = "select thn_akademik_semester, nm_semester, id_semester from semester where STATUS_AKTIF_SEMESTER='True'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$semester = $r[0]." ".$r[1];
		$semester_id = $r[2];
	}
$html .= '
	FAKULTAS '.strtoupper($fakultas).'<BR>
	KARTU RENCANA PRESTASI (KRP)</B>
	</td>
</tr>
</table>
<br>
<table width="120%" cellpadding="0" style="font-size:8x;">
<tr>
	<td width="10%">Nama</td>
	<td width="1%">:</td>
	<td width="39%">'.$nm_pengguna.'</td>
	<td width="10%">Prog.Studi</td>
	<td width="1%">:</td>
	<td width="39%">'.$prodi.'</td>
</tr>
<tr>
	<td>NIM</td>
	<td>:</td>
	<td>'.$nim.'</td>
	<td>Semester</td>
	<td>:</td>
	<td>'.$semester.'</td>
</tr>	
</table>
<br>
<table width="120%" border="1" cellpadding="2" cellspacing="0" style="font-size:8x;">
<tr>
	<td width="3%" align="center"><b>No</b></td>
	<td align="center"><b>Nama Kegiatan</b></td>
	<td align="center"><b>Nilai SKP</b></td>
	<td align="center"><b>Perkiraan Waktu Pelaksanaan</b></td>
	<td align="center"><b>Penyelenggara</b></td>
</tr>
<tr>
	<td align="center">(1)</td>
	<td align="center">(2)</td>
	<td align="center">(3)</td>
	<td align="center">(4)</td>
	<td align="center">(5)</td>
</tr>
<tr>
	<td colspan="5">Kegiatan Wajib Universitas</td>
</tr>
';
$hit=0;
$kueria = "
select b.nm_krp_khp,b.SKOR_KRP_KHP, b.WAKTU_KRP_KHP, b.penyelenggara_krp_khp
from kegiatan_2 a, krp_khp b, bukti_fisik c, kegiatan_1 d
where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_bukti_fisik=c.id_bukti_fisik and a.id_kegiatan_1=d.id_kegiatan_1 and b.id_mhs='".$id_mhs."' and b.id_semester='".$semester_id."' and d.id_kelompok_kegiatan='1'
";
$result = $db->Query($kueria)or die("salah kueri : ".$kueria);
	while($r = $db->FetchRow()) {
	$hit++;
	$html .= '
	<tr>
		<td>'.$hit.'</td>
		<td>'.$r[0].'</td>
		<td align=center>'.$r[1].'</td>
		<td>'.$semester.'</td>
		<td>'.$r[2].'</td>
	</tr>
	';
}
$html .= '
<tr>
	<td colspan="5">&nbsp;</td>
</tr>
<tr>
	<td colspan="5">Kegiatan Bidang Organisasi dan Kepemimpinan</td>
</tr>
';
$hit=0;
$kueria = "
select b.nm_krp_khp,b.SKOR_KRP_KHP, b.WAKTU_KRP_KHP, b.penyelenggara_krp_khp
from kegiatan_2 a, krp_khp b, bukti_fisik c, kegiatan_1 d
where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_bukti_fisik=c.id_bukti_fisik and a.id_kegiatan_1=d.id_kegiatan_1 and b.id_mhs='".$id_mhs."' and b.id_semester='".$semester_id."' and d.id_kelompok_kegiatan='2'
";
$result = $db->Query($kueria)or die("salah kueri : ".$kueria);
	while($r = $db->FetchRow()) {
	$hit++;
	$html .= '
	<tr>
		<td>'.$hit.'</td>
		<td>'.$r[0].'</td>
		<td align=center>'.$r[1].'</td>
		<td>'.$semester.'</td>
		<td>'.$r[2].'</td>
	</tr>
	';
}
$html .= '
<tr>
	<td colspan="5">&nbsp;</td>
</tr>
<tr>
	<td colspan="5">Kegiatan Bidang Penalaran dan Keilmuan</td>
</tr>
';
$hit=0;
$kueria = "
select b.nm_krp_khp,b.SKOR_KRP_KHP, b.WAKTU_KRP_KHP, b.penyelenggara_krp_khp
from kegiatan_2 a, krp_khp b, bukti_fisik c, kegiatan_1 d
where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_bukti_fisik=c.id_bukti_fisik and a.id_kegiatan_1=d.id_kegiatan_1 and b.id_mhs='".$id_mhs."' and b.id_semester='".$semester_id."' and d.id_kelompok_kegiatan='3'
";
$result = $db->Query($kueria)or die("salah kueri : ".$kueria);
	while($r = $db->FetchRow()) {
	$hit++;
	$html .= '
	<tr>
		<td>'.$hit.'</td>
		<td>'.$r[0].'</td>
		<td align=center>'.$r[1].'</td>
		<td>'.$semester.'</td>
		<td>'.$r[2].'</td>
	</tr>
	';
}
$html .= '
<tr>
	<td colspan="5">&nbsp;</td>
</tr>
<tr>
	<td colspan="5">Kegiatan Bidang Minat dan Bakat</td>
</tr>
';
$hit=0;
$kueria = "
select b.nm_krp_khp,b.SKOR_KRP_KHP, b.WAKTU_KRP_KHP, b.penyelenggara_krp_khp
from kegiatan_2 a, krp_khp b, bukti_fisik c, kegiatan_1 d
where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_bukti_fisik=c.id_bukti_fisik and a.id_kegiatan_1=d.id_kegiatan_1 and b.id_mhs='".$id_mhs."' and b.id_semester='".$semester_id."' and d.id_kelompok_kegiatan='4'
";
$result = $db->Query($kueria)or die("salah kueri : ".$kueria);
	while($r = $db->FetchRow()) {
	$hit++;
	$html .= '
	<tr>
		<td>'.$hit.'</td>
		<td>'.$r[0].'</td>
		<td align=center>'.$r[1].'</td>
		<td>'.$semester.'</td>
		<td>'.$r[2].'</td>
	</tr>
	';
}
$html .= '
<tr>
	<td colspan="5">&nbsp;</td>
</tr>
<tr>
	<td colspan="5">Kegiatan Bidang Kepedulian Sosial</td>
</tr>
';
$hit=0;
$kueria = "
select b.nm_krp_khp,b.SKOR_KRP_KHP, b.WAKTU_KRP_KHP, b.penyelenggara_krp_khp
from kegiatan_2 a, krp_khp b, bukti_fisik c, kegiatan_1 d
where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_bukti_fisik=c.id_bukti_fisik and a.id_kegiatan_1=d.id_kegiatan_1 and b.id_mhs='".$id_mhs."' and b.id_semester='".$semester_id."' and d.id_kelompok_kegiatan='5'
";
$result = $db->Query($kueria)or die("salah kueri : ".$kueria);
	while($r = $db->FetchRow()) {
	$hit++;
	$html .= '
	<tr>
		<td>'.$hit.'</td>
		<td>'.$r[0].'</td>
		<td align=center>'.$r[1].'</td>
		<td>'.$semester.'</td>
		<td>'.$r[2].'</td>
	</tr>
	';
}
$html .= '
<tr>
	<td colspan="5">&nbsp;</td>
</tr>
<tr>
	<td colspan="5">Kegiatan Lainnya</td>
</tr>
';
$hit=0;
$kueria = "
select b.nm_krp_khp,b.SKOR_KRP_KHP, b.WAKTU_KRP_KHP, b.penyelenggara_krp_khp
from kegiatan_2 a, krp_khp b, bukti_fisik c, kegiatan_1 d
where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_bukti_fisik=c.id_bukti_fisik and a.id_kegiatan_1=d.id_kegiatan_1 and b.id_mhs='".$id_mhs."' and b.id_semester='".$semester_id."' and d.id_kelompok_kegiatan='6'
";
$result = $db->Query($kueria)or die("salah kueri : ".$kueria);
	while($r = $db->FetchRow()) {
	$hit++;
	$html .= '
	<tr>
		<td>'.$hit.'</td>
		<td>'.$r[0].'</td>
		<td align=center>'.$r[1].'</td>
		<td>'.$semester.'</td>
		<td>'.$r[2].'</td>
	</tr>
	';
}
$html .= '
</table>
<br>
<table width="120%" border="0" cellpadding="2" cellspacing="0" style="font-size:8x;">
<tr>
	<td>
		Menyetujui,<br>
		Dosen Wali<br>
		<br><br><br>
		.............................<br>
		NIP : ....................
	</td>
	<td>
		Surabaya, '.date("d-m-Y").'<br>
		<br>
		<br><br><br>
		.............................<br>
		NIP : ....................
	</td>
</tr>
</table>
';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('transkrip.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+
