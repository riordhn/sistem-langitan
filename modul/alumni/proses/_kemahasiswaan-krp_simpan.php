<?php
require('../../../config.php');
require('../includes/ceking3.php');
$isi="";
if($_POST["aksi"]=="tampil") {
	// ambil id_mhs
	$kueri = "select id_mhs from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

	// ambil id_semes
	$kueri = "select id_semester, nm_semester, thn_akademik_semester from semester where status_aktif_semester='True'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$id_semes = $r[0];
		$nama_semes = $r[2]." ".$r[1];
	}

	$isi="";
	$isi .= '
		<form name="frmkrp" id="frmkrp">
		<table class="add_publikasi">
		<tr>
			<th colspan="5">Input KRP</th>
		</tr>
		<tr>
			<td><span>Kelompok Kegiatan</span></td>
			<td>
			<select name="keg_kelom" onchange="kelom_keg(this.value)">
				<option value="0">----------</option>
			';
			$kueri = "select id_kelompok_kegiatan, nm_kelompok_kegiatan from kelompok_kegiatan order by id_kelompok_kegiatan";
			$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
			while($r = $db->FetchRow()) {
				$isi .= "<option value='".$r[0]."'>".$r[1]."</option>";
			}
			$isi .= '
			</select>
			</td>
		</tr>
		<tr>
			<td><a href="kemahasiswaan-krp.php" onclick="window.open(\'proses/_km_list_jenis_kegiatan.php\')">Jenis Kegiatan</a></td>
			<td>
			<select name="keg_jenis" id="keg_jenis" onchange="jenis_keg(this.value)">
				<option value="0">----------</option>
			</select>
			</td>
		</tr>
		<tr>
			<td><span>Tingkat</span></td>
			<td>
			<select name="keg_tingkat" id="keg_tingkat">
				<option value="0">----------</option>
			</select>
			</td>
		</tr>
		<tr>
			<td><span>Prestasi/Partisipasi/Jabatan</span></td>
			<td>
			<select name="keg_prestasi" id="keg_prestasi">
				<option value="0">----------</option>
			</select>
			</td>
		</tr>
		<tr>
			<td><span>Nama Kegiatan</span></td>
			<td><input type=text name="keg_nama"></td>
		</tr>
		<tr>
			<td><span>Waktu Penyelenggaraan</span></td>
			<td><input type=text name="keg_waktu"></td>
		</tr>
		<tr>
			<td><span>Penyelenggara</span></td>
			<td><input type=text name="keg_penyelenggara"></td>
		</tr>
		<tr>
			<td><span>Bukti Fisik</span></td>
			<td>
			<select name="bukti_fisik">
				<option value="0">----------</option>
			';
			$kueri = "select id_bukti_fisik, nm_bukti_fisik from bukti_fisik order by nm_bukti_fisik";
			$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
			while($r = $db->FetchRow()) {
				$isi .= "<option value='".$r[0]."'>".$r[1]."</option>";
			}
			$isi .= '
			</select>
			</td>
		</tr>
		<tr>
			<td><span>Nomor Bukti Fisik</span></td>
			<td><input type=text name="keg_nobuktifisik"></td>
		</tr>
		<tr>
			<td colspan="2" align=center>
			<input type="hidden" name="aksi" value="simpan">
			<input type="button" value="Save" class="submit" onclick="krp_simpan(\'frmkrp\')" >
			</td>
		</tr>
		</table>
		</form>

		<table border="1" width="100%"><tbody>
		<tr>
			<td align="center" colspan="5"><h2>Kartu Rencana Prestasi <font size=1 color="blue" onclick="window.open(\'proses/_kemahasiswaan-krp_cetak.php\',\'baru2\');">[<u>cetak</u>]</font></h2></td>
		</tr>
		<tr>
			<th align="center">Thn Akad</th>
			<th align="center">Kegiatan</th>
			<th align="center">Penyelenggara</th>
			<th align="center">Waktu Pelaksanaan</th>
			<th align="center">&nbsp;</th>
		</tr>
		';
		$kueri = "select id_krp_khp, nm_krp_khp, penyelenggara_krp_khp, waktu_krp_khp from krp_khp where id_mhs='".$id_mhs."' and id_semester='".$id_semes."'";
		$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
		while($r = $db->FetchRow()) {
			$isi .= '
			<tr>
				<td align=center><span>'.$nama_semes.'</span></td>
				<td align=center><span>'.$r[1].'</span></td>
				<td><span>'.$r[2].'</span></td>
				<td align="center">'.$r[3].'</td>
				<td align="center" onclick="if(confirm(\'Yakin hapus ?\')!=0){ krp_hapus(\''.$r[0].'\'); }">[Hapus]</td>
			</tr>
			';
		}
		$isi .= '
		</table>
	';
	echo $isi;
}else if($_POST["aksi"]=="simpan" and $_POST["keg_nama"] and $_POST["keg_waktu"] and $_POST["keg_penyelenggara"] ) {
	//print_r($_POST);

	$param_prestasi = "";
	if($_POST["keg_prestasi"]) {
		$param_prestasi = " and id_jabatan_prestasi='".Angka($_POST["keg_prestasi"])."' ";
	}else{
		$param_prestasi = " and id_jabatan_prestasi is null ";
	}

	$param_tingkat = "";
	if($_POST["keg_tingkat"]) {
		$param_tingkat = " and id_tingkat='".Angka($_POST["keg_tingkat"])."' ";
	}else{
		$param_tingkat = " and id_tingkat is null ";
	}
	
	// ambil id_kegiatan2
	$id_kegiatan2="";
	$kueri = "select id_kegiatan_2 from kegiatan_2 where id_kegiatan_1='".Angka($_POST["keg_jenis"])."' ".$param_tingkat." ".$param_prestasi." ";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$id_kegiatan2 = $r[0];
	}
	if(strlen($id_kegiatan2)>0) {
		// ambil id_semes
		$kueri = "select id_semester from semester where status_aktif_semester='True'";
		$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
		while($r = $db->FetchRow()) {
			$id_semes = $r[0];
		}
		// ambil id_mhs
		$kueri = "select id_mhs from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'";
		$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
		while($r = $db->FetchRow()) {
			$id_mhs = $r[0];
		}

		$kueri = "INSERT INTO krp_khp (ID_SEMESTER, ID_KEGIATAN_2, ID_BUKTI_FISIK, ID_MHS, PENYELENGGARA_KRP_KHP, NM_KRP_KHP, SKOR_KRP_KHP, WAKTU_KRP_KHP ) VALUES ('".$id_semes."', '".$id_kegiatan2."', '".$_POST["bukti_fisik"]."', '".$id_mhs."', '".$_POST["keg_penyelenggara"]."', '".$_POST["keg_nama"]."','0', '".$_POST["keg_waktu"]."')";
		$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
		if($db->numRows() > 0) {
			echo 'Input berhasil';
		}
	}else{
		echo "TIdak sesuai master";
	}
	
}else if($_POST["aksi"]=="hapus" and $_POST["id"] ) {
	// ambil id_mhs
	$kueri = "select id_mhs from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

	$kueri = "delete from krp_khp where id_krp_khp='".Angka($_POST["id"])."' and id_mhs='".$id_mhs."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	if($db->numRows() > 0) {
		echo 'Hapus berhasil';
	}
}else if ($_POST["aksi"]=="combo_kelomkeg" and $_POST["id"]) {
	$isi .= '<option value="0">----------</option>';
	$kueri = "select id_kegiatan_1, nm_kegiatan_1 from kegiatan_1 where id_kelompok_kegiatan='".Angka($_POST["id"])."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$isi .= "<option value='".$r[0]."'>".$r[1]."</option>";
	}
	echo $isi;
}else if ($_POST["aksi"]=="combo_jeniskeg" and $_POST["id"]) {
	$isi .= '<option value="0">----------</option>';
	$kueri = "select distinct a.id_tingkat,b.nm_tingkat from kegiatan_2 a, tingkat b where a.id_tingkat=b.id_tingkat and a.id_kegiatan_1='".Angka($_POST["id"])."' order by b.nm_tingkat ";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$isi .= "<option value='".$r[0]."'>".$r[1]."</option>";
	}
	echo $isi;
}else if ($_POST["aksi"]=="combo_prestasikeg" and $_POST["id"]){
	$isi .= '<option value="0">----------</option>';
	$kueri = "select distinct b.id_jabatan_prestasi, b.nm_jabatan_prestasi from kegiatan_2 a, jabatan_prestasi b where a.id_jabatan_prestasi=b.id_jabatan_prestasi and a.id_kegiatan_1='".Angka($_POST["id"])."' order by b.nm_jabatan_prestasi ";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$isi .= "<option value='".$r[0]."'>".$r[1]."</option>";
	}
	echo $isi;
}

?>