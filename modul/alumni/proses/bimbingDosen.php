<!-- Author : Seto Priyanggoro -->

<!-- File ini berisi proses dari menu bimbingan dosen -->

    <table width="500" border="none">
    <tr><td colspan="2" align="center"><h2>Bimbingan Dosen</h2></td></tr>
    <tr><td align="center"><span><h3>Dosen</h3></span></td><td align="center"><h3>Jumlah Mahasiswa Bimbingan</h3></td></tr>
    <tr><td><span>Dr. Eridani</span></td><td align="center">3</td></tr>
	<tr><td><span>Drs. Eto Wuryanto, DEA</span></td><td align="center">5</td></tr>
	<tr><td><span>Ir. Dyah Herawatie, M.Si</span></td><td align="center">5</td></tr>
    </table>
	
	<table width="500" border="none">
    <tr><td colspan="3" align="center"><h2>Detail Mahasiswa Bimbingan Dr. Eridani</h2></td></tr>
    <tr><td align="center"><span><h3>NIM</h3></span></td><td align="center"><h3>Nama Mahasiswa</h3></td><td align="center"><h3>Judul TA / Skripsi</h3></td></tr>
    <tr><td><span>080912345</span></td><td align="center">Joni</td><td align="center">Transformasi wavelet</td></tr>
	<tr><td><span>080954321</span></td><td align="center">Jono</td><td align="center">Integral Eigen</td></tr>
	<tr><td><span>080951423</span></td><td align="center">Jonu</td><td align="center">Matrik Tereduksi</td></tr>
    </table>