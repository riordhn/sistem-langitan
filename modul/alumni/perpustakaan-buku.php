<?php

require_once('config.php');
// no auth security sementara ~sugenk.
//if ($user->Role() != AUCC_ROLE_MAHASISWA){
//	header("location: /logout.php");
//    exit();
//} 
// end no auth security

include 'conf_perpus.php';
include 'proses/info_buku.class.php';

$info_buku = new info_buku($db_mysql);
$mode = get('mode', 'view');

if ($mode == 'view' && get('kunci') != '') {
    $smarty->assign('data_hasil_cari', $info_buku->load_pencarian_buku(get('kriteria'), get('kunci'), get('tempat')));
} else if ($mode == 'detail') {
    $smarty->assign('data_detail_buku', $info_buku->get_pencarian_buku(get('reg')));
}


$smarty->display("{$mode}_info_buku.tpl");
?>