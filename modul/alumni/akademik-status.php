<?php
require('config.php');
// no auth security sementara ~sugenk.
//if ($user->Role() != AUCC_ROLE_MAHASISWA){
//	header("location: /logout.php");
//    exit();
//} 
// end no auth security

	// ambil nim & password
	$mhs_pass=""; $mhs_nim="";
	$kueri = "select b.nim_mhs, a.password_hash, a.se1 from pengguna a, mahasiswa b where a.id_pengguna=b.id_pengguna and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 6 : ");
	while($r = $db->FetchRow()) {
		$mhs_pass = $r[2];
		$mhs_nim = $r[0];
	}

//if($mhs_pass == $mhs_nim) {
if ($user->IsLogged() && $user->Role() == 28) {
	$tampil_data = '
	<br>
	<table>
	<tr>
		<th align=center>Semester</th>
		<th align=center>Status</th>
		<th align=center>No. SK.</th>
		<th align=center>Tgl SK.</th>
		<th align=center>Keterangan</th>
	</tr>
	';

	$kueri = "
	SELECT 	NM_STATUS_PENGGUNA, THN_AKADEMIK_SEMESTER, NM_SEMESTER, ADMISI.NO_SK, TGL_SK, TGL_USULAN, TGL_APV, KETERANGAN, KURUN_WAKTU, ALASAN
				FROM MAHASISWA 
				LEFT JOIN PENGGUNA ON MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA   
				LEFT JOIN PROGRAM_STUDI ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS 
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				LEFT JOIN ADMISI ON MAHASISWA.ID_MHS = ADMISI.ID_MHS
				LEFT JOIN SEMESTER ON ADMISI.ID_SEMESTER = SEMESTER.ID_SEMESTER
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = ADMISI.STATUS_AKD_MHS 
				WHERE MAHASISWA.NIM_MHS = '$mhs_nim'
				ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC
	";
	$result = $db->Query($kueri)or die("salah kueri : 35");
	while($r = $db->FetchArray()) {
		$tampil_data .= '
		<tr>
			<td>'.$r[1]. ' / ' . $r[2] .'</td>
			<td>'.$r[0].'</td>
			<td>'.$r[3].'</td>
			<td>'.$r[4].'</td>
			<td>'.$r[7].'</td>
		</tr>
		';
	}

	$smarty->assign('tampil_data', $tampil_data);

	$smarty->display('akademik-status.tpl');
}else {
	$smarty->display('session-expired.tpl');
}
?>
