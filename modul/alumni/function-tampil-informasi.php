<?php

function alert_error($text, $widht = 90) {
    return "<div id='alert' class='ui-widget' style='margin:5px;'>
        <div class='ui-state-error ui-corner-all' style='padding: 5px;width:{$widht}%;'> 
            <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span> 
                {$text}</p>
        </div>
    </div>";
}

function alert_success($text, $width = 70, $align = 'left') {
    return "<div id='alert' class='ui-widget' style='margin:5px;'>
        <div class='ui-state-highlight ui-corner-all' style='padding: 5px;width:{$width}%;'> 
            <p style='text-align:{$align}'><span class='ui-icon ui-icon-check' style='float: left; margin-right: .3em;'></span> 
                {$text}</p>
        </div>
    </div>";
}

?>
