<?php

include 'config.php';
include 'funtion-tampil-informasi.php';
include 'class/tracer.class.php';
include '../ppkk/class/master.class.php';

$tracer = new tracer($db);
$master = new master($db);

if (isset($_GET)) {
    if (get('kat') != '') {
        $data_soal = $tracer->load_data_soal(1, get('kat'));
        if (isset($_POST)) {
            if (post('mode') == 'simpan') {
                $cek_hasil_form = $tracer->cek_hasil_form($user->ID_PENGGUNA, 1);
                if (empty($cek_hasil_form)) {
                    $tracer->insert_hasil_form($user->ID_PENGGUNA, 1);
                }
                $hasil_form = $tracer->cek_hasil_form($user->ID_PENGGUNA, 1);
                foreach ($data_soal as $data) {
                    $cek_hasil_soal = $tracer->cek_hasil_soal($hasil_form['ID_HASIL_FORM'], $data['ID_SOAL']);
                    if (empty($cek_hasil_soal)) {
                        $tracer->insert_hasil_soal($hasil_form['ID_HASIL_FORM'], $data['ID_SOAL']);
                        $hasil_soal = $tracer->cek_hasil_soal($hasil_form['ID_HASIL_FORM'], $data['ID_SOAL']);
                        $jumlah_jawaban = count($data['DATA_JAWABAN']);
                        // Jika Tipe Soal Categorical Variable
                        if ($data['TIPE_SOAL'] == 1) {
                            if (post($data['NOMER']) != '') {
                                $tracer->insert_hasil_jawaban($hasil_soal['ID_HASIL_SOAL'], post($data['NOMER']), '', post($data['NOMER'] . 'ket'));
                            }
                        }
                        // Jika Tipe Soal Multiple Dichotomous
                        else if ($data['TIPE_SOAL'] == 2) {
                            for ($i = 1; $i <= $jumlah_jawaban; $i++) {
                                if ($_POST[$data['NOMER'] . $i] != '') {
                                    $tracer->insert_hasil_jawaban($hasil_soal['ID_HASIL_SOAL'], post($data['NOMER'] . $i), '', post($data['NOMER'] . $i . 'ket'));
                                }
                            }
                        }
                        // Jika Tipe Soal Text Variable
                        else if ($data['TIPE_SOAL'] == 3) {
                            for ($i = 1; $i <= $jumlah_jawaban; $i++) {
                                if ($_POST[$data['NOMER'] . $i] != '') {
                                    $tracer->insert_hasil_jawaban($hasil_soal['ID_HASIL_SOAL'], post($data['NOMER'] . $i . 'jaw'), post($data['NOMER'] . $i), post($data['NOMER'] . $i . 'ket'));
                                }
                            }
                        }
                        // Jika Tipe Soal Text Long
                        else if ($data['TIPE_SOAL'] == 4) {
                            for ($i = 1; $i <= $jumlah_jawaban; $i++) {
                                if ($_POST[$data['NOMER'] . $i] != '') {
                                    $tracer->insert_hasil_jawaban($hasil_soal['ID_HASIL_SOAL'], post($data['NOMER'] . $i . 'jaw'), post($data['NOMER'] . $i), post($data['NOMER'] . $i . 'ket'));
                                }
                            }
                        }
                        // Jika Tipe Soal Metric Variable
                        else if ($data['TIPE_SOAL'] == 5) {
                            for ($i = 1; $i <= $jumlah_jawaban; $i++) {
                                if ($_POST[$data['NOMER'] . $i] != '') {
                                    $tracer->insert_hasil_jawaban($hasil_soal['ID_HASIL_SOAL'], post($data['NOMER'] . $i . 'jaw'), post($data['NOMER'] . $i), post($data['NOMER'] . $i . 'ket'));
                                }
                            }
                        }
                        // Jika Tipe Soal Ordinal Variable
                        else if ($data['TIPE_SOAL'] == 6) {
                            $group_jawaban = $tracer->get_jumlah_group_soal($data['ID_SOAL']);
                            for ($i = 1; $i <= $group_jawaban['JUMLAH_GROUP']; $i++) {
                                if ($_POST[$data['NOMER'] . 'g' . $i] != '') {
                                    $tracer->insert_hasil_jawaban($hasil_soal['ID_HASIL_SOAL'], $_POST[$data['NOMER'] . 'g' . $i], '', post($data['NOMER'] . 'g' . $i . 'ket'));
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($tracer->cek_hasil_jawaban(get('kat'), $user->ID_PENGGUNA) > 0) {
            $smarty->assign('status_isi', alert_success("Anda sudah berhasil mengisi kategori soal ini, Silahkan Mengisi kategori lain..."));
        }
        $smarty->assign('data_akad', $tracer->get_akad_mhs($user->ID_PENGGUNA));
        $smarty->assign('data_mhs', $tracer->get_data_mahasiswa($user->ID_PENGGUNA));
        $smarty->assign('data_soal', $data_soal);
    }
}
if ($tracer->cek_soal_belum_terjawab($user->ID_PENGGUNA) == '') {
    $smarty->assign('status_form', alert_success("Anda telah berhasil mengisi semua form Tracer Studi, terima ksih untuk partisipasinya"));
}
$info_isi = alert_success("<b>Kesempatan pengisian hanya 1 kali mohon diperiksa terlebih dahulu sebelum menyimpan data,terima kasih</b>", '100');
$smarty->assign('kategori_soal', $master->load_kategori_soal(1));
$smarty->assign('info_isi', $info_isi);
$smarty->display('tracer_studi/tracer-studi.tpl')
?>
