<?php
require('../../config.php');
include('../akademik/class/date.class.php');
$db = new MyOracle();

require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('DRAFT');
$pdf->SetSubject('DRAFT');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

$nim = $_GET['nim'];
$biomhs="SELECT NIM_MHS, NM_PENGGUNA, GELAR_DEPAN, GELAR_BELAKANG, NM_KOTA, TGL_LAHIR_PENGGUNA, NM_FAKULTAS,
		NM_PROGRAM_STUDI, NM_JENJANG_IJASAH, GELAR_PANJANG, GELAR_PENDEK, FAKULTAS.ID_FAKULTAS, NO_IJASAH, LAHIR_IJAZAH, JENJANG.ID_JENJANG,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, JUDUL_TA, NO_SERI_KERTAS, NM_FAKULTAS_ENG, NM_PROGRAM_STUDI_ENG, GELAR_INGGRIS
		FROM MAHASISWA
		LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
		LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
		LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
		LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
		LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
		LEFT JOIN KOTA ON KOTA.ID_KOTA = MAHASISWA.LAHIR_KOTA_MHS
		WHERE NIM_MHS = '$nim' AND (YUDISIUM = 1 OR YUDISIUM = 2)";
$result1 = $db->Query($biomhs)or die("salah kueri 2 ");
$r1 = $db->FetchRow();
	if($r1[1] <> strtoupper($r1[1])){
		$nm_mhs = $r1[1];
	}else{
		$nm_mhs = ucwords(strtolower($r1[1]));
	}
	$gelar_depan = $r1[2];
	$gelar_belakang = $r1[3];
	$nm_kota = $r1[4];
	$tgl_lahir = $r1[5];
	$nm_fakultas = strtoupper($r1[6]);
	$nm_program_studi = strtoupper($r1[7]);
	$nm_jenjang_ijasah = strtoupper($r1[8]);
	$gelar_panjang = ucwords(strtolower($r1[9]));
	$gelar_pendek = $r1[10];
	$id_fakultas = $r1[11];
	$no_ijasah = $r1[12];
	$lahir_ijazah = $r1[13];
	$id_jenjang = $r1[14];
	$id_prodi = $r1[15];
	$judul_ta = stripslashes($r1[16]);
	$no_seri = $r1[17];
	$nm_fakultas_eng = $r1[18];
	$nm_prodi_eng = $r1[19];
	$gelar_eng = $r1[20];
	
	$tgl_lahir = date("Y-m-d", strtotime($tgl_lahir));
	$tgl_lahir = tgl_indo($tgl_lahir);
	$tgl_skrg = tgl_indo(date("Y-m-d"));
	$nm_kota = str_ireplace("KOTA ", "", $nm_kota);
	$nm_kota = str_ireplace("KABUPATEN ", "", $nm_kota);
	$nm_kota = ucwords(strtolower($nm_kota));
	
	$filename = '../..//foto_wisuda/'.$nim.'.jpg';
	//if (file_exists($filename)==false) {
		// ## grabing
			$namafile = $nim.'.jpg';
			//original file
			$filenya = "http://10.0.110.13/foto/fotoijasah/".$namafile;
			//directory to copy to (must be CHMOD to 777)
			$copydir = "../..//foto_wisuda/";
			$data = file_get_contents($filenya);
			$file = fopen($copydir . $namafile, "w+");

			fputs($file, $data);
			fclose($file);

	// ## Edit
	$filename = '../..//foto_wisuda/'.$nim.'.jpg';
	if (file_exists($filename) and filesize($filename)>0) {
		//echo "The file $filename exists";
		$photo = $pdf->Image('../..//foto_wisuda/'.$nim.'.jpg', 50, 35, 25, '', '', '', '', false, 72);
		$photosource = "../..//foto_wisuda/$nim.jpg";
	}else {
		//echo "The file $filename does not exist";
		$photo = $pdf->Image('../..//foto_wisuda/BELUMFOTO.jpg', 50, 35, 25, '', '', '', '', false, 72);
		$photosource = "../..//foto_wisuda/BELUMFOTO.jpg";
	}

	
	if($gelar_belakang <> ''){
		$gelar_belakang = ", " . $gelar_belakang; 
	}
	if($gelar_depan <> ''){
		$gelar_depan = $gelar_depan . " ";
	}


$lulus = "SELECT TGL_LULUS FROM ADMISI
		LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = ADMISI.ID_MHS
		WHERE NIM_MHS = '$nim' AND STATUS_AKD_MHS = 4 AND STATUS_APV = 1 AND TGL_LULUS IS NOT NULL
		ORDER BY ID_ADMISI DESC";
$db->Query($lulus)or die("salah kueri 2 ");
$tes = $db->FetchArray();
$tgl_lulus = date("Y-m-d", strtotime($tes['TGL_LULUS']));
$tgl_lulus = tgl_indo($tgl_lulus);


				$sd = "SELECT TO_CHAR(TGL_LULUS, 'YYYY') AS TAHUN FROM ADMISI
								LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = ADMISI.ID_MHS
								WHERE NIM_MHS = '$nim' AND STATUS_AKD_MHS = 4
								ORDER BY ID_ADMISI DESC";
				$db->Query($sd)or die("salah kueri 2 ");
				$thn = $db->FetchArray();
				$thn_lulus = $thn['TAHUN'];
				
				if($thn_lulus <= '2011'){
					$head = "KEMENTERIAN PENDIDIKAN NASIONAL";
				}else{
					$head = "KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN";
				}

$rektor = "SELECT NM_PENGGUNA, GELAR_DEPAN, GELAR_BELAKANG FROM DOSEN 
			LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = DOSEN.ID_PENGGUNA
			WHERE ID_JABATAN_PEGAWAI = 1";
$db->Query($rektor)or die("salah kueri 2 ");
$r2 = $db->FetchRow();
$nm_rektor = ucwords(strtolower($r2[0]));
$gelar_depan_rektor = $r2[1];
$gelar_belakang_rektor = $r2[2];
	
	if($gelar_belakang_rektor <> ''){
		$gelar_belakang_rektor = ", " . $gelar_belakang_rektor; 
	}
	if($gelar_depan_rektor <> ''){
		$gelar_depan_rektor = $gelar_depan_rektor . " ";
	}

$dekan = "SELECT NM_PENGGUNA, GELAR_DEPAN, GELAR_BELAKANG FROM FAKULTAS 
			LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = FAKULTAS.ID_DEKAN
			WHERE FAKULTAS.ID_FAKULTAS = '$id_fakultas'";
$db->Query($dekan)or die("salah kueri 2 ");
$r3 = $db->FetchRow();
$nm_dekan = ucwords(strtolower($r3[0]));
$gelar_depan_dekan = $r3[1];
$gelar_belakang_dekan = $r3[2];
	
	if($gelar_belakang_dekan <> ''){
		$gelar_belakang_dekan = ", " . $gelar_belakang_dekan; 
	}
	if($gelar_depan_dekan <> ''){
		$gelar_depan_dekan = $gelar_depan_dekan . " ";
	}


if($lahir_ijazah <> ""){$lahir_ijazah;}else{$lahir_ijazah = $nm_kota.', '. $tgl_lahir;}
if($id_jenjang == 5 or $id_jenjang ==9){$sebutan = "sebutan";}else{$sebutan = "gelar";}
	if($id_prodi == 74){ 
		$prodi1 = "PROGRAM STUDI FARMASI"; $prodi2 = "PENDIDIKAN PROFESI APOTEKER"; 
	}else{ 
		$prodi1 = "PROGRAM PENDIDIKAN " .$nm_jenjang_ijasah; $prodi2 = "PROGRAM STUDI ".$nm_program_studi;
	}

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
//$pdf->SetHeaderData('', '', 'No. Ijasah : ', 'No. Ijasah :');

// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));


// set default monospaced font
//$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 3, 10);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default footer
//$pdf->setPrintFooter(false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 0);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('times', '', 13);

// add a page
$pdf->AddPage('L', 'A4');

// draw jpeg image


// set the starting point for the page content
$pdf->setPageMark();

if($nim == '071044021' or $nim == '071044022'){

$tgl_lulus = date("d F Y", strtotime($tes['TGL_LULUS']));

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left"><b>Number : '.$no_ijasah.'</b></ br></td>
	<td align="right"><b>Serial Number : '.$no_seri.'</b></ br></td>
  </tr>  
  <tr>
    <td colspan="4" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="center">&nbsp;<br /></td>
  </tr>
  <tr>
    <td colspan="4" align="center"><font size="+2"><b>REPUBLIC OF INDONESIA</b></font></td>
  </tr>
  <tr>
    <td colspan="4" align="center"><font size="+2"><b>MINISTRY OF EDUCATION AND CULTURE</b></font></td>
  </tr>
  <tr>
    <td colspan="4" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="center"><b><em>this is to certify that</em></b></td>
  </tr>
  <tr>
    <td colspan="4" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="center"><font size="+5" style="font-weight:bold">'.$gelar_depan.''.$nm_mhs.''.$gelar_belakang.'</font></td>
  </tr>
  <tr>
    <td colspan="4" align="center"><b>Student Registration Number : '.$nim.'</b></td>
  </tr>
  <tr>
    <td colspan="4" align="center"><b>Born In '.$lahir_ijazah.'</b></td>
  </tr>
  <tr>
    <td colspan="4" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="center"><b><em>having successfully fulfilled all the requirements in the</em></b></td>
  </tr>
  <tr>
    <td colspan="4" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="center"><font size="+4" style="font-weight:bold">FACULTY OF '.strtoupper($nm_fakultas_eng).'</font></td>
  </tr>
  <tr>
    <td colspan="4" align="center"><font size="+4" style="font-weight:bold">MASTER PROGRAM IN '.strtoupper($nm_prodi_eng).'</font></td>
  </tr>
  <tr>
    <td colspan="4" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="center"><b><em>has been admitted to the degree of</em></b></td>
  </tr>
  <tr>
    <td colspan="4" align="center"><font size="+3" style="font-weight:bold">'.$gelar_panjang.' ('.$gelar_pendek.')</font></td>
  </tr>
  <tr>
    <td colspan="4" align="center"><b><em>equivalent to</em></b></td>
  </tr>
  <tr>
    <td colspan="4" align="center"><font size="+3" style="font-weight:bold">'.strtoupper($gelar_eng).'</font></td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="center"><b>and to all the rights and duties attached<br />Surabaya, '.$tgl_lulus.'</b></td>
  </tr>
  <tr>
    <td align="center" width="19%"></td>	
    <td rowspan="7" align="center" width="12%"><img src="'.$photosource.'" width="100" height="130" /></td>	
	<td width="10%">&nbsp;</td>    
	<td align="center" width="50%"><b>DEAN</b></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center"></td>	
    <td>&nbsp;</td>
	<td align="center"><font size="+1" style="font-weight:bold">'. $gelar_depan_dekan . $nm_dekan . $gelar_belakang_dekan.'</font></td>	
  </tr>
</table>';


}else{
$pdf->Image('../akademik/includes/draft.png', 30, 70, 240, '40', '', '', '', false, 72);

if($id_fakultas == 9 and $id_jenjang == 3){		
$html = '
<style type="text/css">
p {line-height:80%;}
</style>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="3" align="left"><b>No Ijazah : '.$no_ijasah.'</b></ br></td>
  </tr>
  <tr>
		<td colspan="3" align="left"><b>Lampiran : Predikat Kelulusan</b></ br></td>
	</tr>
  <tr>
    <td colspan="3" align="center"><font size="+4"><b>'.$head.'</b></font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4"><b>UNIVERSITAS AIRLANGGA</b></font></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>dengan ini menyatakan bahwa :</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$gelar_depan.''.$nm_mhs.''.$gelar_belakang.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b>Nomor Induk Mahasiswa : '.$nim.'</b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b>Lahir di '.$lahir_ijazah.'</b></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>telah menyelesaikan pendidikan dengan baik pada</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$nm_fakultas.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$prodi1.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$prodi2.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><em><b>Dengan memperhatikan Disertasi berjudul : </b></em></td>
  </tr>
  <tr>
    <td colspan="3" align="center">'.$judul_ta.'</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>karena itu kepadanya diberikan ijazah dan '.$sebutan.'</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+3" style="font-weight:bold">'.$gelar_panjang.' ('.$gelar_pendek.')</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>beserta segala hak dan kewajiban yang melekat pada '.$sebutan.' tersebut.</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b>Diberikan di Surabaya pada tanggal, '.$tgl_lulus.'</b></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" width="44%"><b>DIREKTUR</b></td>
    <td rowspan="7" align="center" width="12%"><img src="'.$photosource.'" width="100" height="130" /></td>
    <td align="center" width="44%"><b>REKTOR</b></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><font size="+1" style="font-weight:bold">'. $gelar_depan_dekan . $nm_dekan . $gelar_belakang_dekan.'</font></td>
    <td align="center"><font size="+1" style="font-weight:bold">'. $gelar_depan_rektor . $nm_rektor . $gelar_belakang_rektor.'</font></td>
  </tr>
</table>';
}elseif($id_fakultas == 9 and $id_jenjang == 2){		
$html = '
<style type="text/css">
p {line-height:80%;}
</style>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="3" align="left"><b>No Ijazah : '.$no_ijasah.'</b></ br></td>
  </tr>
  <tr>
		<td colspan="3" align="left"><b>Lampiran : Predikat Kelulusan</b></ br></td>
	</tr>
  <tr>
    <td colspan="3" align="center"><font size="+4"><b>'.$head.'</b></font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4"><b>UNIVERSITAS AIRLANGGA</b></font></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>dengan ini menyatakan bahwa :</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$gelar_depan.''.$nm_mhs.''.$gelar_belakang.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b>Nomor Induk Mahasiswa : '.$nim.'</b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b>Lahir di '.$lahir_ijazah.'</b></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>telah menyelesaikan pendidikan dengan baik pada</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$nm_fakultas.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$prodi1.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$prodi2.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>karena itu kepadanya diberikan ijazah dan '.$sebutan.'</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+3" style="font-weight:bold">'.$gelar_panjang.' ('.$gelar_pendek.')</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>beserta segala hak dan kewajiban yang melekat pada '.$sebutan.' tersebut.</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b>Diberikan di Surabaya pada tanggal, '.$tgl_lulus.'</b></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" width="44%"><b>DIREKTUR</b></td>
    <td rowspan="7" align="center" width="12%"><img src="'.$photosource.'" width="100" height="130" /></td>
    <td align="center" width="44%"><b>REKTOR</b></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><font size="+1" style="font-weight:bold">'. $gelar_depan_dekan . $nm_dekan . $gelar_belakang_dekan.'</font></td>
    <td align="center"><font size="+1" style="font-weight:bold">'. $gelar_depan_rektor . $nm_rektor . $gelar_belakang_rektor.'</font></td>
  </tr>
</table>';
}elseif($id_jenjang == 3 and $id_fakultas != 9){		
$html = '
<style>
p {line-height: 80%;}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="3" align="left"><b>No Ijazah : '.$no_ijasah.'</b></ br></td>
  </tr>
  <tr>
		<td colspan="3" align="left"><b>Lampiran : Predikat Kelulusan</b></ br></td>
	</tr>
  <tr>
    <td colspan="3" align="center"><font size="+4"><b>'.$head.'</b></font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4"><b>UNIVERSITAS AIRLANGGA</b></font></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>dengan ini menyatakan bahwa :</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$gelar_depan.''.$nm_mhs.''.$gelar_belakang.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b>Nomor Induk Mahasiswa : '.$nim.'</b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b>Lahir di '.$lahir_ijazah.'</b></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>telah menyelesaikan pendidikan dengan baik pada</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">FAKULTAS '.$nm_fakultas.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$prodi1.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$prodi2.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><em><b>Dengan memperhatikan Disertasi berjudul : </b></em></td>
  </tr>
  <tr>
    <td colspan="3" align="center">'.$judul_ta.'</td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>karena itu kepadanya diberikan ijazah dan '.$sebutan.'</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+3" style="font-weight:bold">'.$gelar_panjang.' ('.$gelar_pendek.')</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>beserta segala hak dan kewajiban yang melekat pada '.$sebutan.' tersebut.</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b>Diberikan di Surabaya pada tanggal, '.$tgl_lulus.'</b></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" width="44%"><b>DEKAN</b></td>
    <td rowspan="7" align="center" width="12%"><img src="'.$photosource.'" width="100" height="130" /></td>
    <td align="center" width="44%"><b>REKTOR</b></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><font size="+1" style="font-weight:bold">'. $gelar_depan_dekan . $nm_dekan . $gelar_belakang_dekan.'</font></td>
	<td>&nbsp;</td>
    <td align="center"><font size="+1" style="font-weight:bold">'. $gelar_depan_rektor . $nm_rektor . $gelar_belakang_rektor.'</font></td>
  </tr>
</table>';
}else{
$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="3" align="left"><b>No Ijazah : '.$no_ijasah.'</b></ br></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4"><b>'.$head.'</b></font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4"><b>UNIVERSITAS AIRLANGGA</b></font></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>dengan ini menyatakan bahwa :</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$gelar_depan.''.$nm_mhs.''.$gelar_belakang.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b>Nomor Induk Mahasiswa : '.$nim.'</b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b>Lahir di '.$lahir_ijazah.'</b></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>telah menyelesaikan pendidikan dengan baik pada</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">FAKULTAS '.$nm_fakultas.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$prodi1.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+4" style="font-weight:bold">'.$prodi2.'</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>karena itu kepadanya diberikan ijazah dan '.$sebutan.'</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><font size="+3" style="font-weight:bold">'.$gelar_panjang.' ('.$gelar_pendek.')</font></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b><em>beserta segala hak dan kewajiban yang melekat pada '.$sebutan.' tersebut.</em></b></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><b>Diberikan di Surabaya pada tanggal, '.$tgl_lulus.'</b></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" width="44%"><b>DEKAN</b></td>
    <td rowspan="7" align="center" width="12%"><img src="'.$photosource.'" width="100" height="130" /></td>
    <td align="center" width="44%"><b>REKTOR</b></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><font size="+1" style="font-weight:bold">'. $gelar_depan_dekan . $nm_dekan . $gelar_belakang_dekan.'</font></td>
    <td align="center"><font size="+1" style="font-weight:bold">'. $gelar_depan_rektor . $nm_rektor . $gelar_belakang_rektor.'</font></td>
  </tr>
</table>';
}
}
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('Draft-'.strtoupper($nim).'.pdf', 'I');

?>