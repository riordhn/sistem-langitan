<?php

$oci_user = 'aucc_akademik'; // username
$oci_pass = 'akadem1k'; // password
//$oci_host = '210.57.212.67'; // hostname
$oci_host = '10.0.110.100'; // hostname
$oci_port = '1521'; // listener port
$oci_sidc = 'aucc.localdomain'; // sid name

$oci_tnsc = '
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = '.$oci_host.')(PORT = '.$oci_port.'))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = '.$oci_sidc.')
    )
  )';


@$conn = oci_connect($oci_user, $oci_pass, $oci_tnsc);
if (!$conn){
    $e = oci_error(); // For oci_connect errors do not pass a handle
	die('database tidak konek');
}


echo '
<form name=form1 method=post action="">
Fakultas : 
<select name="fakultas">
	<option value="0">-- Fakultas --</option>
';
	$query = "select id_fakultas,nm_fakultas from aucc.fakultas order by id_fakultas";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	$hit = 0;
	while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
		echo '<option value="'.$r[0].'" '; if($_POST["fakultas"]==$r[0]){ echo 'selected'; } echo '>'.$r[1].'</option>';
	}
echo '
</select><input type=submit name="ok" value="Tampil">
</form>
';

if($_POST["fakultas"]) {
	echo "<hr>";
	echo '
	<table border=1 cellpadding=3 cellspacing=0>
		<TR>
			<TD align=center rowspan=2><B>No</B></TD>
			<TD align=center rowspan=2><B>Fakultas</B></TD>
			<TD align=center rowspan=2><B>Jenjang</B></TD>
			<TD align=center rowspan=2><B>Prodi</B></TD>
			<TD align=center rowspan=2><B>Nim</B></TD>
			<TD align=center rowspan=2><B>Nama</B></TD>
			<TD align=center rowspan=2><B>Status</B></TD>
			<TD align=center colspan=2><B>2004/2005</B></TD>
			<TD align=center colspan=2><B>2005/2006</B></TD>
			<TD align=center colspan=2><B>2006/2007</B></TD>
			<TD align=center colspan=2><B>2007/2008</B></TD>
			<TD align=center colspan=2><B>2008/2009</B></TD>
			<TD align=center colspan=2><B>2009/2010</B></TD>
			<TD align=center colspan=2><B>2010/2011</B></TD>
			<TD align=center colspan=2><B>2011/2012</B></TD>
			<TD align=center colspan=2><B>Piutang</B></TD>
		</TR>
		<TR>
			<TD align=center><B>Ganjil</B></TD>
			<TD align=center><B>Genap</B></TD>
			<TD align=center><B>Ganjil</B></TD>
			<TD align=center><B>Genap</B></TD>
			<TD align=center><B>Ganjil</B></TD>
			<TD align=center><B>Genap</B></TD>
			<TD align=center><B>Ganjil</B></TD>
			<TD align=center><B>Genap</B></TD>
			<TD align=center><B>Ganjil</B></TD>
			<TD align=center><B>Genap</B></TD>
			<TD align=center><B>Ganjil</B></TD>
			<TD align=center><B>Genap</B></TD>
			<TD align=center><B>Ganjil</B></TD>
			<TD align=center><B>Genap</B></TD>
			<TD align=center><B>Ganjil</B></TD>
			<TD align=center><B>Genap</B></TD>
			<TD align=center><B>X sem</B></TD>
			<TD align=center><B>Jumlah</B></TD>
		</TR>
	';
		$query = "select a.id_mhs, c.nm_fakultas, d.nm_jenjang, b.nm_program_studi, a.nim_mhs, e.nm_pengguna, a.status_akademik_mhs
		from aucc.mahasiswa a, aucc.program_studi b, aucc.fakultas c, aucc.jenjang d, aucc.pengguna e
		where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and b.id_jenjang=d.id_jenjang and a.id_pengguna=e.id_pengguna
		and c.id_fakultas='".$_POST["fakultas"]."' and a.status_akademik_mhs not in ('4')
		order by a.nim_mhs
		";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$hit = 0;
		while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
			$hit++;
			$piutang=0;

			$query2 = "select nm_status_pengguna from aucc.status_pengguna where id_role='3' and id_status_pengguna='".$r[6]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$status_pengguna = $r2[0];

			// 2004 ganjil
			$j20041=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='45' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='45' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20041 += $r2[0];
					}
				}
			}else{
				$j20041 = "";
			}

			// 2004 Genap
			$j20042=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='46' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='46' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20042 += $r2[0];
					}
				}
			}else{
				$j20042 = "";
			}

			// 2005 Ganjil
			$j20051=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='43' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='43' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20051 += $r2[0];
					}
				}
			}else{
				$j20051 = "";
			}

			// 2005 Genap
			$j20052=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='44' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='44' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20052 += $r2[0];
					}
				}
			}else{
				$j20052 = "";
			}

			// 2006 Ganjil
			$j20061=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='41' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='41' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20061 += $r2[0];
					}
				}
			}else{
				$j20061 = "";
			}

			// 2006 Genap
			$j20062=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='42' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='42' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20062 += $r2[0];
					}
				}
			}else{
				$j20062 = "";
			}

			// 2007 Ganjil
			$j20071=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='27' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='27' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20071 += $r2[0];
					}
				}
			}else{
				$j20071 = "";
			}

			// 2007 Genap
			$j20072=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='28' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='28' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20072 += $r2[0];
					}
				}
			}else{
				$j20072 = "";
			}

			// 2008 Ganjil
			$j20081=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='25' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='25' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20081 += $r2[0];
					}
				}
			}else{
				$j20081 = "";
			}

			// 2008 Genap
			$j20082=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='26' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='26' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20082 += $r2[0];
					}
				}
			}else{
				$j20082 = "";
			}

			// 2009 Ganjil
			$j20091=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='24' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='24' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20091 += $r2[0];
					}
				}
			}else{
				$j20091 = "";
			}

			// 2009 Genap
			$j20092=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='6' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='6' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20092 += $r2[0];
					}
				}
			}else{
				$j20092 = "";
			}

			// 2010 Ganjil
			$j20101=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='22' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='22' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20101 += $r2[0];
					}
				}
			}else{
				$j20101 = "";
			}

			// 2010 Genap
			$j20102=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='23' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='23' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20102 += $r2[0];
					}
				}
			}else{
				$j20102 = "";
			}

			// 2011 Ganjil
			$j20111=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='5' and id_mhs='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='5' and id_mhs='".$r[0]."'";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20111 += $r2[0];
					}
				}
			}else{
				$j20111 = "";
			}

			// 2011 Genap
			$j20112=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='21' and id_mhs='".$r[0]."";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='21' and id_mhs='".$r[0]."";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20112 += $r2[0];
					}
				}
			}else{
				$j20112 = "";
			}

			// 2012 Ganjil
			$j20121=0;
			$query2 = "select count(*) from aucc.pembayaran where id_semester='1' and id_mhs='".$r[0]."";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			$ada = $r2[0];
			if($ada>0) {
				$query2 = "select (besar_biaya+denda_biaya) as tagihan, to_char(tgl_bayar,'YYYYMMDD') from aucc.pembayaran where id_semester='1' and id_mhs='".$r[0]."";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					if($r2[1]=="" or $r2[1]=="00000000") {
						$piutang += $r2[0];
					}else{
						$j20121 += $r2[0];
					}
				}
			}else{
				$j20121 = "";
			}

			echo '
			<TR>
				<TD>'.$hit.'</TD>
				<TD align=center>'.$r[1].'</TD>
				<TD align=center>'.$r[2].'</TD>
				<TD align=center>'.$r[3].'</TD>
				<TD align=center>'.$r[4].'</TD>
				<TD align=center>'.$r[5].'</TD>
				<TD align=center>'.$status_pengguna.'</TD>
				<TD align=right>'.number_format($j20041,0,',','.').'</TD>
				<TD align=right>'.number_format($j20042,0,',','.').'</TD>
				<TD align=right>'.number_format($j20051,0,',','.').'</TD>
				<TD align=right>'.number_format($j20052,0,',','.').'</TD>
				<TD align=right>'.number_format($j20061,0,',','.').'</TD>
				<TD align=right>'.number_format($j20062,0,',','.').'</TD>
				<TD align=right>'.number_format($j20071,0,',','.').'</TD>
				<TD align=right>'.number_format($j20072,0,',','.').'</TD>
				<TD align=right>'.number_format($j20081,0,',','.').'</TD>
				<TD align=right>'.number_format($j20082,0,',','.').'</TD>
				<TD align=right>'.number_format($j20091,0,',','.').'</TD>
				<TD align=right>'.number_format($j20092,0,',','.').'</TD>
				<TD align=right>'.number_format($j20101,0,',','.').'</TD>
				<TD align=right>'.number_format($j20102,0,',','.').'</TD>
				<TD align=right>'.number_format($j20111,0,',','.').'</TD>
				<TD align=right>'.number_format($j20112,0,',','.').'</TD>
				<TD align=center><B>X sem</B></TD>
				<TD align=right>'.number_format($piutang,0,',','.').'</TD>
			</TR>
			';
		}
	echo '
	</table>
	';
}



?>