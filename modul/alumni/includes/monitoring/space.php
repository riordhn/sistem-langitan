<?php
function ZahlenFormatieren($Wert) {
	if ($Wert > 1099511627776) {
		$Wert = number_format ( $Wert / 1099511627776, 2, ".", "," ) . " TB";
	} elseif ($Wert > 1073741824) {
		$Wert = number_format ( $Wert / 1073741824, 2, ".", "," ) . " GB";
	} elseif ($Wert > 1048576) {
		$Wert = number_format ( $Wert / 1048576, 2, ".", "," ) . " MB";
	} elseif ($Wert > 1024) {
		$Wert = number_format ( $Wert / 1024, 2, ".", "," ) . " kB";
	} else {
		$Wert = number_format ( $Wert, 2, ".", "," ) . " Bytes";
	}
	
	return $Wert;
}
?>
<html>
<head>
	<title>FusionGadgets Chart Gallery - Cylinder Gauge</title>
	<script language="JavaScript" src="FusionCharts.js"></script>
</head>
<body bgcolor="#ffffff">
<table width='98%' align='center' cellpadding='2' cellspacing='0'>
<tr>
	<td align="center" class="textBold"><font size="6" style="text-decoration:blink;"><b>CyberCampus</b></font></td>
</tr>
<tr>
	<td align="center">
			<div id="chartdiv" align="center" class="text">
			<p>&nbsp;</p>
			<p>FusionWidgets needs Adobe Flash Player to run. If you're unable to see the chart here, it means that your browser does not seem to have the Flash Player Installed. You can downloaded it <a href="http://www.adobe.com/products/flashplayer/" target="_blank"><u>here</u></a> for free.</p>
			</div>

			<script type="text/javascript">
				var myChart = new FusionCharts("Cylinder.swf", "myChartId", "200", "230", "0", "0");
				myChart.setDataURL("Cylinder1.xml");
				myChart.render("chartdiv");
			</script>
	</td>
</tr>
<tr>
	<td align="center">
	<table>
	<tr>
		<td>Total Space</td>
		<td>:</td>
		<td><?php echo ZahlenFormatieren(disk_total_space("./")) ?></td>
	</tr>
	<tr>
		<td>Usage Space</td>
		<td>:</td>
		<td><?php echo ZahlenFormatieren((disk_total_space("./")-disk_free_space("./"))) ?></td>
	</tr>
	<tr>
		<td>Free Space</td>
		<td>:</td>
		<td><?php echo ZahlenFormatieren(disk_free_space("./")) ?></td>
	</tr>
	</table>
	</td>
</tr>
</table>
</body>
</html>
