<?php

function Angka ($param){
	$pan = strlen($param);
	$hasil = "";
	for($x=0; $x<$pan; $x++) {
		if(substr($param,$x,1)=='1' or substr($param,$x,1)=='2' or substr($param,$x,1)=='3' or substr($param,$x,1)=='4' or substr($param,$x,1)=='5' or substr($param,$x,1)=='6' or substr($param,$x,1)=='7' or substr($param,$x,1)=='8' or substr($param,$x,1)=='9' or substr($param,$x,1)=='0' ) {
			$hasil .= substr($param,$x,1);
		}else{
			// lanjut
		}
	}
	return $hasil;
}

function Desimal ($param){
	$pan = strlen($param);
	$hasil = "";
	for($x=0; $x<$pan; $x++) {
		if(substr($param,$x,1)=='1' or substr($param,$x,1)=='2' or substr($param,$x,1)=='3' or substr($param,$x,1)=='4' or substr($param,$x,1)=='5' or substr($param,$x,1)=='6' or substr($param,$x,1)=='7' or substr($param,$x,1)=='8' or substr($param,$x,1)=='9' or substr($param,$x,1)=='0' or substr($param,$x,1)=='.' ) {
			$hasil .= substr($param,$x,1);
		}else{
			// lanjut
		}
	}
	return $hasil;
}

function Huruf ($param){
	$pan = strlen($param);
	$param = strtoupper($param);
	$hasil = "";
	for($x=0; $x<$pan; $x++) {
		if(substr($param,$x,1)=='A' or substr($param,$x,1)=='B' or substr($param,$x,1)=='C' or substr($param,$x,1)=='D' or substr($param,$x,1)=='E' or substr($param,$x,1)=='F' or substr($param,$x,1)=='G' or substr($param,$x,1)=='H' or substr($param,$x,1)=='I' or substr($param,$x,1)=='J' or substr($param,$x,1)=='K' or substr($param,$x,1)=='L' or substr($param,$x,1)=='M' or substr($param,$x,1)=='N' or substr($param,$x,1)=='O' or substr($param,$x,1)=='P' or substr($param,$x,1)=='Q' or substr($param,$x,1)=='R' or substr($param,$x,1)=='S' or substr($param,$x,1)=='T' or substr($param,$x,1)=='U' or substr($param,$x,1)=='V' or substr($param,$x,1)=='W' or substr($param,$x,1)=='X' or substr($param,$x,1)=='Y' or substr($param,$x,1)=='Z' ) {
			$hasil .= substr($param,$x,1);
		}else{
			// lanjut
		}
	}
	return $hasil;
}

function AngkaHuruf ($param){
	$pan = strlen($param);
	$param = strtoupper($param);
	$hasil = "";
	for($x=0; $x<$pan; $x++) {
		if(substr($param,$x,1)=='1' or substr($param,$x,1)=='2' or substr($param,$x,1)=='3' or substr($param,$x,1)=='4' or substr($param,$x,1)=='5' or substr($param,$x,1)=='6' or substr($param,$x,1)=='7' or substr($param,$x,1)=='8' or substr($param,$x,1)=='9' or substr($param,$x,1)=='0' or substr($param,$x,1)=='A' or substr($param,$x,1)=='B' or substr($param,$x,1)=='C' or substr($param,$x,1)=='D' or substr($param,$x,1)=='E' or substr($param,$x,1)=='F' or substr($param,$x,1)=='G' or substr($param,$x,1)=='H' or substr($param,$x,1)=='I' or substr($param,$x,1)=='J' or substr($param,$x,1)=='K' or substr($param,$x,1)=='L' or substr($param,$x,1)=='M' or substr($param,$x,1)=='N' or substr($param,$x,1)=='O' or substr($param,$x,1)=='P' or substr($param,$x,1)=='Q' or substr($param,$x,1)=='R' or substr($param,$x,1)=='S' or substr($param,$x,1)=='T' or substr($param,$x,1)=='U' or substr($param,$x,1)=='V' or substr($param,$x,1)=='W' or substr($param,$x,1)=='X' or substr($param,$x,1)=='Y' or substr($param,$x,1)=='Z' ) {
			$hasil .= substr($param,$x,1);
		}else{
			// lanjut
		}
	}
	return $hasil;
}

function Nama ($param){
	$pan = strlen($param);
	$param = strtoupper($param);
	$hasil = "";
	for($x=0; $x<$pan; $x++) {
		if(substr($param,$x,1)=='1' or substr($param,$x,1)=='2' or substr($param,$x,1)=='3' or substr($param,$x,1)=='4' or substr($param,$x,1)=='5' or substr($param,$x,1)=='6' or substr($param,$x,1)=='7' or substr($param,$x,1)=='8' or substr($param,$x,1)=='9' or substr($param,$x,1)=='0' or substr($param,$x,1)=='A' or substr($param,$x,1)=='B' or substr($param,$x,1)=='C' or substr($param,$x,1)=='D' or substr($param,$x,1)=='E' or substr($param,$x,1)=='F' or substr($param,$x,1)=='G' or substr($param,$x,1)=='H' or substr($param,$x,1)=='I' or substr($param,$x,1)=='J' or substr($param,$x,1)=='K' or substr($param,$x,1)=='L' or substr($param,$x,1)=='M' or substr($param,$x,1)=='N' or substr($param,$x,1)=='O' or substr($param,$x,1)=='P' or substr($param,$x,1)=='Q' or substr($param,$x,1)=='R' or substr($param,$x,1)=='S' or substr($param,$x,1)=='T' or substr($param,$x,1)=='U' or substr($param,$x,1)=='V' or substr($param,$x,1)=='W' or substr($param,$x,1)=='X' or substr($param,$x,1)=='Y' or substr($param,$x,1)=='Z' or substr($param,$x,1)==' ' or substr($param,$x,1)=='.' or substr($param,$x,1)==',' ) {
			$hasil .= substr($param,$x,1);
		}else{
			// lanjut
		}
	}
	return $hasil;
}

function semua_boleh_kecuali2 ($tmp_param){
	$hasil = $tmp_param;
	$hasil = str_replace("--", "", $hasil);
	$hasil = str_replace(";", "", $hasil);
	$hasil = str_replace("=", "", $hasil);
	$hasil = str_replace("'", "", $hasil);
	
	return $hasil;
}

?>