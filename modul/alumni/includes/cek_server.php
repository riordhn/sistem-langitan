<?php
session_start();
function cek2($param,$alias){
	$ling = "http://".$param;
	$isi = "";
	$isi = file_get_contents($ling);
	if(strpos("a".$isi, "<html") != 0) {
		$hasil = '<div style="padding:10px; font-size:30px; background:blue; color:white; text-align:center;">'.$alias.'</div>';
	}else{
		$hasil = '<div style="padding:10px; font-size:30px; background:red; color:white; text-align:center; font-weight:bold;">'.$alias.'</div>';
	}
	return $hasil;
}
?>

<html>
<head>
<title> Cek Server </title>
<META HTTP-EQUIV="Refresh" CONTENT="3600">
</head>
<body>
	<div align=center><h1>Monitoring Sub-domain Utama UNAIR</h1></div>
	<table cellpadding=3 cellspacing=0 border=0 align=center>
		<tr>
			<td colspan=6><?php echo "a".cek2("unair.ac.id","UNAIR"); ?></td>
		</tr>
	</table>
	
	<div align=center><br><font size=5><b>*** Direktorat ***</b></font></div>
	<table cellpadding=3 cellspacing=0 border=0 align=center>
		<tr>
			<td><?php echo cek2("dsi.unair.ac.id","DSI"); ?></td>
			<td><?php echo cek2("pendidikan.unair.ac.id","DITPEN"); ?></td>
			<td><?php echo cek2("ditkeu.unair.ac.id","DITKEU"); ?></td>
			<td><?php echo cek2("km.unair.ac.id","DITMAWA"); ?></td>
			<td><?php echo cek2("sd.unair.ac.id","DITSD"); ?></td>
		</tr>
	</table>

	<div align=center><br><font size=5><b>*** Unit ***</b></font></div>
	<table cellpadding=3 cellspacing=0 border=0 align=center>
		<tr>
			<td><?php echo cek2("fk.unair.ac.id","FK"); ?></td>
			<td><?php echo cek2("fkg.unair.ac.id","FKG"); ?></td>
			<td><?php echo cek2("fh.unair.ac.id","FH"); ?></td>
			<td><?php echo cek2("feb.unair.ac.id","FEB"); ?></td>
			<td><?php echo cek2("ff.unair.ac.id","FF"); ?></td>
			<td><?php echo cek2("fkh.unair.ac.id","FKH"); ?></td>
		</tr>
		<tr>
			<td><?php echo cek2("fisip.unair.ac.id","FISIP"); ?></td>
			<td><?php echo cek2("fst.unair.ac.id","FST"); ?></td>
			<td><?php echo cek2("pasca.unair.ac.id","PASCA"); ?></td>
			<td><?php echo cek2("fkm.unair.ac.id","FKM"); ?></td>
			<td><?php echo cek2("psikologi.unair.ac.id","PSIKO"); ?></td>
			<td><?php echo cek2("fib.unair.ac.id","FIB"); ?></td>
		</tr>
		<tr>
			<td><?php echo cek2("ners.unair.ac.id","NERS"); ?></td>
			<td><?php echo cek2("fpk.unair.ac.id","FPK"); ?></td>
			<td><?php echo cek2("lppm.unair.ac.id","LPPM"); ?></td>
			<td><?php echo cek2("rumahsakit.unair.ac.id","RSUA"); ?></td>
			<td><?php echo cek2("international.unair.ac.id","IOP"); ?></td>
			<td><?php echo cek2("ppmb.unair.ac.id","PPMB"); ?></td>
		</tr>
		<tr>
			<td><?php echo cek2("journal.unair.ac.id","JOURNAL"); ?></td>
			<td><?php echo cek2("pinlabs.unair.ac.id","PINLAB"); ?></td>
			<td><?php echo cek2("ppm.unair.ac.id","PPM"); ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>

	<div align=center><br><font size=5><b>*** Kolaborasi ***</b></font></div>
	<table cellpadding=3 cellspacing=0 border=0 align=center>
		<tr>
			<td><?php echo cek2("analisa.unair.ac.id","WEBO"); ?></td>
			<td><?php echo cek2("repo.unair.ac.id","REPO"); ?></td>
			<td>
			<?
				$hasil = cek2("mail.unair.ac.id","MAIL");
				echo $hasil;
			?>
			</td>
			<td>
			<?
				$hasil = cek2("web.unair.ac.id","BLOG");
				echo $hasil;
			?>
			</td>
			<td><?php echo cek2("cybercampus.unair.ac.id","CYBERCAMPUS"); ?></td>
		</tr>
		<tr>
			<td colspan=5><br>
			<font color="blue"><u>Keterangan</u> :</font>
			<ul>
				<li><font size=4>Warna Biru = Server hidup</font></li>
				<li><font size=4>Warna Merah = Server mati</font></li>
			</ul>
			</td>
		</tr>
	</table>
</body>
</html>