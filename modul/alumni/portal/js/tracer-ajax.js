var lastUrl = '';
var defaultRel = '';
var defaultPage = 'home/';

var disable = false;

$(document).ready(function() {
	$( "input:submit" ).button();
    // Handel event
	$('#left-content').load('left-content.php');
	$('#right-content').load('right-content.php');
	
    if (!$.browser.msie) {
        $(window).bind('hashchange', function() {
            checkUrl();
        });
        
        checkUrl();
    }
    else {
        setInterval('checkUrl()', 50);
    }
	
});

function loadHash(rel, url)
{
    if ($.browser.msie)
        location.hash = rel + '!' + url;
    else
        window.location.hash = rel + '!' + url;
}

function checkUrl()
{
    var hash = window.location.hash;
    var rel = hash.substring(1, hash.search('!'));
    var url = hash.substring(rel.length + 2);
    
    if (window.location.hash == '') {
        
        rel = defaultRel;
        url = defaultPage;
        
        loadHash(rel, url);
    }
    
    if (lastUrl != hash)
    {
        getPage(url);
    }
    
    lastUrl = hash;
}


function getPage(url)
{
	
    $.ajax({
        url: url,
        dataType: 'html',
        //beforeSend: function() {$('#center-content').html(data); },
        success: function(data) {
            if (data.indexOf('<html>') > -1)
                $('#content_data_content').html('<p>Halaman tidak ada...</p>');
            else
            {
                $('#center-content').html(data);
            }
        }
    });
}


function getPage(url, data)
{

	if( url.search( '/id/' ) ){
		url = url.replace('/', '.php?');
		if(url.search('/id/*/')){
			url = url.replace('id/', 'id=');
		}
		else{
			url = url + '.php';
		}
	}	
	
    $.ajax({
        url: url,
        dataType: 'html',
        //beforeSend: function() {$('#center-content').html(data); },
        success: function(data) {
            //if (data.indexOf('<html>') > -1)
            //    $('#content_data_content').html('<p>Halaman tidak ada...</p>');
            //else
            //{
                $('#center-content').html(data);
            //}
        }
    });
}