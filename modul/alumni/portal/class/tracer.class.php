<?php
/**
 * Description of Programmer
 *
 * @author User
 */
class tracer
{
    private $db;
    
    function __construct(MyOracle $db)
    {
        $this->db = $db;
    }
	
	function getTracerPerFakultas(){
		return $this->db->QueryToArray("
			SELECT F.ID_FAKULTAS as ID_FAKULTAS,NM_FAKULTAS as NM_FAKULTAS,I.PENGISI as PENGISI FROM AUCC.FAKULTAS F
			LEFT JOIN
			(
			SELECT PS.ID_FAKULTAS,COUNT(KHF.ID_PENGGUNA) PENGISI
			FROM AUCC.KUISIONER_HASIL_FORM KHF
			JOIN AUCC.MAHASISWA M ON M.ID_PENGGUNA=KHF.ID_PENGGUNA
			JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
			GROUP BY PS.ID_FAKULTAS
			) I ON I.ID_FAKULTAS=F.ID_FAKULTAS
			ORDER BY F.ID_FAKULTAS
		");
	}
}