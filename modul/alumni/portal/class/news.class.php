<?php
/**
 * Description of Programmer
 *
 * @author User
 */
class news
{
    private $db;
    
    function __construct(MyOracle $db)
    {
        $this->db = $db;
    }
    
    function GetLatestNews()
    {
        return $this->db->QueryToArray("
		select * from news where (id_news <> 53  and id_portal = 2) order by id_news desc");
    }
	
    function GetAllNews()
    {
        return $this->db->QueryToArray("
		select * from news where (id_news <> 53 and id_portal = 2)  order by id_news desc ");
    }

    function GetNewsById($id)
    {
        return $this->db->QueryToArray("
		select * from news where id_news = '$id'");
    }
}