<?php
	
	include 'config.php';
	include 'class/news.class.php';

	$news = new news($db);
	if(isset($_GET['id'])){
		$id = get('id');
		$smarty->assign(array(
						   'id'=>$id, 
						   'news'=>$news->GetNewsById($id)
					   )
		);
	}else{
		$smarty->assign('news', $news->GetAllNews());
	}
	$smarty->display("news.tpl");
	
?>