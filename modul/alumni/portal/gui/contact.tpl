			<script>
			  Recaptcha.create("6Ld1wMoSAAAAAETZfY7dJu_pcm3u9ZSP8x2dlcZt",
				"capca",
				{
				  theme: "clean",
				  lang: "id",
				  callback: Recaptcha.focus_response_field
				}
			  );
			</script>
			
			<!--
			<script>
				$(function() {

					$( ".portlet-contact" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
						.find( ".portlet-header-contact" )
							.addClass( "ui-widget-header ui-corner-all" )
							.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
							.end()
						.find( ".portlet-content-contact" );

					$( ".portlet-header-contact .ui-icon" ).click(function() {
						$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
						$( this ).parents( ".portlet-contact:first" ).find( ".portlet-content-contact" ).toggle();
					});

					//$( ".column" ).disableSelection();
					
				});

			</script>
			-->
			<script type="text/javascript">
			$(document).ready(function() {

				$().ajaxStart(function() {
					$('#loading').show();
					$('#result').hide();
				}).ajaxStop(function() {
					$('#loading').hide();
					$('#result').fadeIn('slow');
				});

				$('#MyFormAdd').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
							$('#result').html(data);
							$('#loading').hide();
							$('#result').fadeIn('slow');
						}
					})
					return false;
				});
			})
			</script>
			
			<div class="ui-widget-header sui-widget-header">
				Contact Us
			</div>	
			
			<div class="ui-widget-content sui-widget-content">

				{$contact}
			</div>
			<br />
			<div class="ui-widget-header sui-widget-header">
				Helpdesk Contact Form
			</div>
			<div class="ui-widget-content sui-widget-content">
			<div id="result" style="display:none;"></div>
			<form id="MyFormAdd" name="MyFormAdd" method="post" action="contact-insert.php">
				<table style="width:100%" id="hor-zebra">
					<tbody valign="top">
						<tr>	
							<td >Nama*</td>
							<td>
								<TEXTAREA id="nama" style="height:20px;" NAME="nama" COLS=30 ROWS=6></TEXTAREA>
								<TEXTAREA id="id_pengguna" style="height:20px; display:none;" NAME="id_pengguna" COLS=30 ROWS=6>{$id_pengguna}</TEXTAREA>
							</td>
						</tr>
						<tr>	
						<tr>	
							<td valign="top" style="text-align:top;">Telpon*</td>
							<td>
								<TEXTAREA id="telpon" style="height:20px;" NAME="telpon" COLS=30 ROWS=6></TEXTAREA>
							</td>
						</tr>
						<tr>	
							<td valign="top" style="text-align:top;">Email*</td>
							<td>
								<TEXTAREA id="email" style="height:20px;" NAME="email" COLS=30 ROWS=6></TEXTAREA>
							</td>
						</tr>
						<tr>	
							<td valign="top" style="text-align:top;">Alamat*</td>
							<td>
								<TEXTAREA id="alamat" style="height:20px;" NAME="alamat" COLS=55 ROWS=6></TEXTAREA>
							</td>
						</tr>
						<tr>	
							<td valign="top" style="text-align:top;">Subject*</td>
							<td>
								<TEXTAREA id="subject" style="height:20px;" NAME="subject" COLS=55 ROWS=6></TEXTAREA>
							</td>
						</tr>
						<tr>	
							<td valign="top" style="text-align:top;">Isi*</td>
							<td>
								<TEXTAREA id="isi" style="min-height:250px;" NAME="isi" COLS=55 ROWS=6></TEXTAREA>
							</td>
						</tr>
						<tr>	
							<td valign="top" style="text-align:top;">Captcha*</td>
							<td>
								<div id="capca"></div>
							</td>
						</tr>
						<tr>	
							<td></td>
							<td>
								<div id="submit" name="submit" >
									<input type="submit" name="btn_submit" id="btn_submit" value="Submit"  />
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				
			</form>
			* harus diisi
			</div>