		
			<div class="ui-widget-header sui-widget-header">
				<span><strong>Tracer Study - Universitas Airlangga</strong></span>
			</div>		
			<div class="ui-widget-content sui-widget-content">

				<div id="contactArea">
					<img style="float:left; padding-right:10px;" src="images/rektor.jpg" width="120" height="150"/>
					<p align="justify" style="margin:5px;font-size:1em;">
						{foreach $sambutan as $data}
							<div style="padding-right:10px;">{$data.ISI}</div>
						{/foreach}
						<br />
						<span style="float:right;padding-top:17px;padding-right:10px;">Rektor 
						<br />
						Prof. Dr. H. Fasich, Apt</span>
					</p>
					<div style="clear:both;">
						&nbsp;
					</div>
				</div>
			
			</div>
			<br />
			<div class="ui-widget-header sui-widget-header">
				Latest News
			</div>

			<div class="ui-widget-content sui-widget-content">
				{foreach $latestNews as $data}
				<div id="news_title">
					<span><strong>{$data.JUDUL}</strong></span>
				<div>
				<div id="news_content">
					<div>
					{$data.ISI|substr:0:500}
					</div>
					<div>
					<a class="more" href="#!news/id/{$data.ID_NEWS}" > ... Read more &rsaquo;&rsaquo; </a>
					</div>
				</div>
				
				<br />
				{/foreach}
			</div>
