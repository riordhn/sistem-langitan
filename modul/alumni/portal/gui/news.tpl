<div class="ui-widget-header sui-widget-header">
	News Pages
</div>

<div class="ui-widget-content sui-widget-content">
	{foreach $news as $data}
	<div id="news_title">
		<span style="padding:5px;">
			<strong>
			<a href="#!news/id/{$data.ID_NEWS}">{$data.JUDUL}</a>
			</strong>
		</span>
	<div>
	{if $id==''}
	<div id="news_content" style="padding:5px;">
		<p align="justify" style="font-size:1em;">
		{$data.ISI|substr:0:500}
		
		</p>
	<a class="more" href="#!news/id/{$data.ID_NEWS}" > ... Read more &rsaquo;&rsaquo; </a></p> 	
	</div>
	
	{else}
	<div id="news_content" style="padding:5px;">
		<p align="justify" style="font-size:1em;">
		{$data.ISI}
		</p>
	</div>
	{/if}	
	<br />
	{/foreach}
</div>