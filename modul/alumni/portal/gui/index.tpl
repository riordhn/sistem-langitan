<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<meta name="language" content="en">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print">
	<!--[if lt IE 8]>
	
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/form.css">
	<link type="text/css" href="css/custom-theme/jquery-ui-1.8.24.custom.css" rel="stylesheet" />
	<link rel="shortcut icon" href="images/iconunair.ico"/>
	<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.24.custom.min.js"></script>
	<script language="javascript" src="js/tracer-ajax.js"></script>
	<script type="text/javascript" src="/api/js/recaptcha_ajax.js"></script>
	<title>Tracer Study - Universitas Airlangga</title>
</head>

<body>

<div id="header">
	<div id="logo"></div>
    <div id="login">
		
		<form action="../login.php" method="post">
			<div style="float:left;" class="ui-icon ui-icon-person"></div>
			<input style="float:left;" type="text" name="username" placeholder="NIM">
			<div style="float:left;" class="ui-icon ui-icon-locked"></div>
			<input style="float:left;" type="password" name="password" placeholder="password">
			<input style="margin-left:4px;font-size:11px; font-weight:bold;" type="submit" value="Masuk">
		</form>
	</div>
	<div id="mainmenu">
		<ul id="yw0">
			<li><a href="#!home/">Home</a></li>
			<li><a href="#!about/">About</a></li>
			<li><a href="#!news/">News</a></li>
			<li><a href="#!contact/">Contact</a></li>
		</ul>	
	</div><!-- mainmenu -->
</div><!-- header -->
	
<div class="container" id="page" style="background:#fff;border:none;box-shadow: 0 5px 5px 5px #888;-webkit-box-shadow: 0 5px 5px 5px#888;boxshadow: 0 5px 5px 5px #888;">
	<!-- breadcrumbs -->
	<div style="width:950px;margin:0 auto;">
	<div id="left-content" class="span-4">
	<!-- left content -->
	</div>
	
	<div id="content" class="span-14">
		<div id="center-content">
		<!-- center content -->
		</div>
	</div><!-- content -->
	<div id="right-content" class="span-4" style="width:145px;">
	<!-- right content -->
	</div>
</div>
	<div class="clear"></div>

	<div id="footer">
		<div class="left">
			<img src="/img/dosen/footer_logo.jpg" />
		</div>
		<div class="center">
		Copyright &copy; 2012 Universitas Airlangga<br/>
		All Rights Reserved.<br/>	
		</div>
		<div class="right">
			<a href="/" >Home</a> | 
			<a href="#" >Site Map</a> | 
			<a href="#" >RSS</a> | 
			<a href="#" >Privacy Policy</a>
		</div>
		
		<div class="clear">
		</div>
	</div><!-- footer -->

</div><!-- page -->
</body></html>