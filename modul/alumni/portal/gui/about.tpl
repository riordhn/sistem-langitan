<div class="ui-widget-header sui-widget-header">
	About - Tracer Study Universitas Airlangga
</div>	
			<div class="ui-widget-content sui-widget-content">

				<div id="contactArea">
					<img style="float:left; padding-right:10px;" src="http://4.bp.blogspot.com/_19j87EQervM/SfPGw3AfH6I/AAAAAAAADtk/cwFW4V6btDU/s400/Fasichul+Lisan.jpg" width="120" height="150"/>
					Alumni Universitas Airlangga (UA) yang dibanggakan Almamater, 

					Apa kabar, semoga selalu dalam lindungan Tuhan Yang Maha Esa.Mohon maaf untuk mengganggu waktu dan pemikirannya beberapa saat.

					Saat ini, UA melalui Pusat Pembinaan Karier dan Kewirausahaan (PPKK) sedang terus meningkatkan hubungan dengan para Alumni, termasuk melalui Studi Penelusuran Alumni (Tracer Study) layaknya yang dilakukan universitas-universitas terkemuka di dunia. UA dapat mencapai status world class university tentunya dengan dukungan para Alumni yang dapat kami banggakan. Oleh karenanya, diharapkan kesediaan para alumni UA untuk bekerjasama mengisi kuesioner Tracer Study ini.

					Kuesioner ini ditujukan untuk alumni Program S1 yang lulus mulai tahun 2010.Studi ini bertujuan untuk mengetahui masa transisi dari dunia kampus menuju dunia kerja dan menganalisis tingkat kompetensi yang diperoleh pada saat pendidikan dan aplikasinya di dunia kerja. Studi ini juga akan menggali informasi terkait dengan proses dan kondisi pembelajaran di UA dan akan digunakan untuk memberikan feedback bagi penyempurnaan kurikulum UA.

					Hasil dari studi ini akan menjadi data berharga bagi UA yang akan digunakan sebagai masukan untuk berbagai upaya dan program pengembangan UA, termasuk diantaranya akreditasi dan pemeringkatan internasional. Sebagai informasi, Tracer Study yang dilaksanakan pada Alumni Program S1 yang dilaksanakan pada tahun sebelumnya telah berjalan dengan sukses. Kami berharap kesuksesan yang sama dapat diraih melalui partisipasi para Alumni UA Program S1 tahun 2010. 

					UA dapat menjamin kerahasiaan informasi yang diberikan.Atas perhatian dan partisipasinya disampaikan terima kasih.

					Salam dan doa kami dari Almamater untuk kesuksesan Anda semua para Alumni UA. 
					<br />
					<span style="float:right;padding-top:17px;">Rektor, 
					<br />
					Prof. Dr. H. Fasich, Apt</span>
					<div style="clear:both;">
						&nbsp;
					</div>
				</div>
			
			</div>