<?php
require_once('config.php');
// no auth security sementara ~sugenk.
//if ($user->Role() != AUCC_ROLE_MAHASISWA){
//	header("location: /logout.php");
//    exit();
//} 
// end no auth security

	$id_pengguna = $user->ID_PENGGUNA;
	function str_word_count_utf8($str) {
	  return str_word_count(preg_split('~[^\p{L}\p{N}\']+~u',$str));
	}

	if(isset($_GET['id'])){
		$id = get('id');
		$db->Query("
			select dok.id_ormawa_dok, dok.judul, dok.content, dok.tanggal, p.id_pengguna, p.nm_pengguna, p.foto_pengguna, p.username from ormawa_dok dok
			left join pengguna p on p.id_pengguna = dok.id_pengguna
			where dok.id_ormawa_dok = $id
		");

		$id_author = array();
		$id_ormawa = array();
		$judul = array();
		$content = array();
		$author = array();
		$foto = array();
		$username = array();
		$tanggal = array();
		$count = array();
		
		$i = 0;
		$jml_data = 0;
		while ($row = $db->FetchRow()){ 
			$id_ormawa[$i] = $row[0];
			$judul[$i] = $row[1];
			$content[$i] = urldecode($row[2]);
			$id_author[$i] = $row[4];
			$author[$i] = $row[5];
			$foto[$i] = $row[6];
			$username[$i] = $row[7];
			$foto[$i] = $foto[$i] . '/' . $username[$i] . '.JPG';
			$tanggal[$i] = $row[3];
			$tanggal[$i] = date("F j, Y");
			$count[$i] = str_word_count($content[$i]);
			$jml_data++;
			$i++;
		}
		

		$id_tanggapan = array();
		$tanggapan = array();
		$tgl_tanggapan = array();
		$penanggap = array();
		$foto_penanggap = array();
		$username_penanggap = array();
		
		$db->Query("select tgp.id_ormawa_tanggapan, tgp.content, tgp.tanggal, p.nm_pengguna, p.foto_pengguna, p.username from ormawa_tanggapan tgp
		left join ormawa_dok dok on dok.id_ormawa_dok = tgp.id_ormawa_dok
		left join pengguna p on p.id_pengguna = tgp.id_pengguna
		where dok.id_ormawa_dok= '$id'
		order by tgp.id_ormawa_tanggapan
		");
		
		$i = 0;
		$jml_tanggapan = 0;
		while ($row = $db->FetchRow()){ 
			$id_tanggapan[$i] = $row[0];
			$tanggapan[$i] = $row[1];
			$tgl_tanggapan[$i] = $row[2];
			$penanggap[$i] = $row[3];
			$foto_penanggap[$i] = $row[4];
			$username_penanggap[$i] = $row[5];
			$foto_penanggap[$i] = $foto_penanggap[$i] . '/' . $username_penanggap[$i] . '.JPG';
			$i++;
			$jml_tanggapan++;
		}
		
		$smarty->assign('id_tanggapan', $id_tanggapan);
		$smarty->assign('tanggapan', $tanggapan);
		$smarty->assign('penanggap', $penanggap);
		$smarty->assign('foto_penanggap', $foto_penanggap);
		$smarty->assign('tgl_tanggapan', $tgl_tanggapan);
		$smarty->assign('jml_tanggapan', $jml_tanggapan);
		
		$db->Query("select id_pengguna, nm_pengguna, foto_pengguna, username from pengguna where id_pengguna= '$id_pengguna'");
		while ($row = $db->FetchRow()){ 
			$id_pengguna = $row[0];
			$nm_pengguna = $row[1];
			$foto_pengguna = $row[2];
			$username = $row[3];
			$foto_pengguna = $foto_pengguna . '/' . $username . '.JPG';
		}
		
		$smarty->assign('id_author', $id_author);
		$smarty->assign('id_pengguna', $id_pengguna);
		$smarty->assign('nm_pengguna', $nm_pengguna);
		$smarty->assign('foto_pengguna', $foto_pengguna);

		$smarty->assign('id', $id);
		$smarty->assign('id_ormawa', $id_ormawa);
		$smarty->assign('judul', $judul);
		$smarty->assign('content', $content);
		$smarty->assign('tanggal', $tanggal);
		$smarty->assign('author', $author);
		$smarty->assign('foto', $foto);
		$smarty->assign('jml_data', $jml_data);
		$smarty->assign('count', $count);
		if(isset($_GET['act'])){
			if($_GET['act']=='edit'){
				$smarty->display('portal_ormawa_edit.tpl');
			}
			else{
				$smarty->display('portal_ormawa_display.tpl');
			}
		}
		else{
			$smarty->display('portal_ormawa_display.tpl');
		}

	}

	else{
		$db->Query("
			select dok.id_ormawa_dok, dok.judul, dok.content, dok.tanggal, p.id_pengguna, p.nm_pengguna, p.foto_pengguna, p.username from ormawa_dok dok
			left join pengguna p on p.id_pengguna = dok.id_pengguna
			order by id_ormawa_dok
		");

		$id_ormawa = array();
		$judul = array();
		$content = array();
		$author = array();
		$foto = array();
		$username = array();
		$tanggal = array();
		
		$i = 0;
		$jml_data = 0;
		while ($row = $db->FetchRow()){ 
			$id_ormawa[$i] = $row[0];
			$judul[$i] = $row[1];
			$content[$i] = urldecode($row[2]);
			if (strlen($content[$i]) > 500) {
				$content[$i] = substr($content[$i], 0, 500) . ' ...';
			}
			$author[$i] = $row[5];
			$foto[$i] = $row[6];
			$username[$i] = $row[7];
			$foto[$i] = $foto[$i] . '/' . $username[$i] . '.JPG';
			$tanggal[$i] = $row[3];
			$tanggal[$i] = date("F j, Y");
			$jml_data++;
			$i++;
		}
		


		$smarty->assign('id_ormawa', $id_ormawa);
		$smarty->assign('judul', $judul);
		$smarty->assign('content', $content);
		$smarty->assign('tanggal', $tanggal);
		$smarty->assign('author', $author);
		$smarty->assign('foto', $foto);
		$smarty->assign('jml_data', $jml_data);
		$smarty->display('portal_ormawa_display.tpl');
	}

?>