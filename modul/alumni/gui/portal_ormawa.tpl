	<style>
	#toolbar {
		padding: 10px 4px;
	}
	</style>
	

<script>
	
	$(document).ready(function(){
		
		$( "#btn-add-berita" ).button({
			text: true,
			icons: {
				primary: "ui-icon-plus"
			}
		});
		
		$( "#btn-broadcast" ).button({
			text: true,
			icons: {
				primary: "ui-icon-signal-diag"
			}
		});
		
		$('#btn-add-berita').click(function(){	
			$('#display').load('portal_ormawa_add.php?src=berita');
		})
		
		$('#btn-broadcast').click(function(){	
			$('#display').load('portal_ormawa_broadcast.php');
		})
		
		$('#display').load('portal_ormawa_display.php');
			
	});
</script>

<div class="center_title_bar">
	Organisasi Mahasiswa
</div>
<br />
<div class="demo">
{if $is_allow > 0}
<!--<span id="toolbar" class="ui-widget-header ui-corner-all">-->
	<div id="btn-add-berita" >Buat Berita</div>
	<div id="btn-broadcast" >Broadcast</div>
<!--</span>-->
{/if}
</div><!-- End demo -->
<br />
<div id="display">
</div>