<div class="center_title_bar">Riwayat</div>
	{foreach $data_history_bayar as $data}
		<h3 class="ui-widget-header ui-corner-all" style="padding: 8px;width: 20%;margin-top:7px;margin-bottom:7px;">{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</h3>
		<table class="ui-widget" style="width:100%">
			<tr class="ui-widget-header">
				<th>NAMA BIAYA</th>
				<th>BESAR BIAYA</th>
				<th>DENDA BIAYA</th>
				<th>TAGIHKAN</th>
				<th>NAMA BANK</th>
				<th>VIA BANK</th>
				<th>TGL.BAYAR</th>
				<th>KET.BAYAR</th>
				<th>STATUS</th>
				<th>KET.STATUS</th>
			</tr>
			{foreach $data.DATA_PEMBAYARAN as $data_pembayaran}			
				<tr class="ui-widget-content">
					<td>{$data_pembayaran.NM_BIAYA}</td>
					<td>{number_format($data_pembayaran.BESAR_BIAYA)}</td>
					<td>{number_format($data_pembayaran.DENDA_BIAYA)}</td>
					<td>
						{if $data_pembayaran.IS_TAGIH=='Y'}
							Ya
						{else}
							Tidak
						{/if}
					</td>
					<td>{$data_pembayaran.NM_BANK}</td>
					<td>{$data_pembayaran.NAMA_BANK_VIA}</td>
					<td>{$data_pembayaran.TGL_BAYAR}</td>
					<td>{$data_pembayaran.KETERANGAN}</td>
					<td>{$data_pembayaran.NAMA_STATUS}</td>
					<td>{$data_pembayaran.KETERANGAN_STATUS}</td>
				</tr>
			{/foreach}
		</table>
	{/foreach}