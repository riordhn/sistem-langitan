{literal}
<script type="text/javascript">
	jQuery(document).ready(function(){			
		$('#other-friends').load('other-friends.php');
		$('#container').load('social-network.php');
		$('#container').slideDown('medium');
	});	
	
	$("#account-page").button({
		icons: {
			primary: "ui-icon-contact",
		},
		text: true
	});
	
	$("#request-friend").button({
		icons: {
			primary: "ui-icon-person",
		},
		text: true
	});
	
	$("#btnMessages").button({
		icons: {
			primary: "ui-icon-mail-closed",
		},
		text: true
	});
	$("#btnNotif").button({
		icons: {
			primary: "ui-icon-info",
		},
		text: true
	});
	
	$("#account-home").button({
		icons: {
			primary: "ui-icon-home",
		},
		text: true
	});
	
	$("#btnPencarian").button({
		icons: {
			primary: "ui-icon-search",
		},
		text: false
	});
	
	$("#account-home").click(function(){
		$('#container').load('sn_beranda.php');
		$('#container').slideDown('fast');
	});
	$("#request-friend").click(function(){
		$('#container').load('sn_pertemanan.php');
	});
	$("#account-page").click(function(){
		$('#container').load('social-network.php');
		$('#container').slideDown('fast');
	});
</script>
	<script>
	$(function() {
		function log( message ) {
			
			$( "<div/>" ).text( message ).prependTo( "#log" );
			$( "#log" ).scrollTop( 0 );
			$('#formPencarian').submit();
		}

		$( "#cari" ).autocomplete({
			source: "/modul/mhs/sn_pencarian.php",
			minLength: 2,
			select: function( event, ui ) {
				log( ui.item ?
					"Selected: " + ui.item.value + "":
					"Nothing selected, input was " + this.value );
			}
		});
	});
	</script>
<script>
						$('#formPencarian').submit(function() {
									$.ajax({
										type: 'GET',
										url: $(this).attr('action'),
										data: $(this).serialize(),
										success: function(data) {
											$('#container').html(data);											
										}
									})
									
									return false;
									
						});
</script>
{/literal}


<div id="sn" style="width:750px; float:left;">	
	<div class="center_title_bar">
		<!-- {$nm_fakultas} | {$id_pengguna} | -->{$nm_pengguna} | Social Network Cyber Campus
	</div>
	
	<div id="account-box">
	<table border="0" class="ui-widget" style="width:700px;">
		<tr class="ui-widget-content">
			<td class="ui-widget-content" style="text-align:center;">
				<div id="request-friend" title="Permintaan teman">
					&nbsp;
				</div>
			</td>
			<td class="ui-widget-content" style="text-align:center;">
				<div id="btnMessages" title="Pesan">
					&nbsp;
				</div>
			</td>
			<td class="ui-widget-content" style="text-align:center;">
				<div id="btnNotif" title="Pemberitahuan">
					&nbsp;
				</div>
			</td>
			<td class="ui-widget-content" style="text-align:center;">
				<div id="pencarian" title="Pencarian">
					<form id="formPencarian" name="formPencarian" action="/modul/mhs/sn_hasil_pencarian.php" method="get">
						<input id="cari" name="cari" style="height:25px; width:170px;" class="ui-widget-content"/><div id="btnPencarian" title="Pencarian">&nbsp;</div>
					</form>
				</div>
			</td>
			<td class="ui-widget-content" style="text-align:center;">
				<div id="account-page" title="Halaman Anda">
					{$nm_pengguna}
				</div>
			</td>
			<td class="ui-widget-content" style="text-align:center;">
				<div id="account-home" title="Halaman Utama">
					Home
				</div>
			</td>
		</tr>
	</table>
	</div>
	<!--
	<div class="ui-widget" style="margin-top:2em; font-family:Arial">
		Result:
		<div id="log" style="height: 20px; width: 300px; overflow: auto;" class="ui-widget-content"></div>
	</div>
	-->	
	<div id="container" style="width:500px; float:left;" >
	

		
		<div id="loading"><img src="../../loading.gif" alt="loading..." />&nbsp;</div>
		
	</div>
	<div id="suggestion" class="demo" style="width:200px; float:left; padding-top:10px;">
	
			<div id="panel9" class="ui-widget" style="width:100%;">
				<div class="ui-widget-content">
						Tambahkan teman untuk berdiskusi :
				</div>
				<div>
					<div id="other-friends"></div>
				</div>
			</div>
		
	</div>
</div>