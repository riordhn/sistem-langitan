{literal}
	<!--<link href="../../css/demo.css" rel="stylesheet" type="text/css"/>-->
    <script src="../../js/tagit.js"></script>
    <script>
        $(function() {

            var availableTags = [
                "MPM",
                "BEM FAKULTAS",
                "BEM UNIVERSITAS"
            ];
			$('#pesan').autosize();
            $('#demo1').tagit({tagSource: availableTags, select: true});
			$( "#btn-send" ).button({
				text: true
			});
			$( "#btn-cancel" ).button({
				text: true
			});
			
		$('#btn-send').click(function(){	
			//var judul = document.getElementById('judul').value;
			//var content =  document.getElementById('get_isi').value;
			//var dataString = 'judul='+ judul + '&get_isi=' + content;
			var content =  document.getElementById('pesan').value;
			if(content ==''){
				$( "#dialog:ui-dialog" ).dialog( "destroy" );
			
				$( "#dialog-message" ).dialog({
					modal: true,
					buttons: {
						Ok: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
			else{
				$('#frmOrmawaBroadcast').submit();
			}
		})
		
		$('#btn-cancel').click(function(){	
			//var judul = document.getElementById('judul').value;
			//var content =  document.getElementById('get_isi').value;
			//var dataString = 'judul='+ judul + '&get_isi=' + content;
			//$('#frmOrmawaBroadcast').submit();
			//window.location.href = 'portal-ORMAWA!portal_ormawa.php';
			$('#display').load('portal_ormawa_display.php');
		})
		
		$('#frmOrmawaBroadcast').submit(function() {
			//var id = document.getElementById('id_ormawa').value;
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
					$('#wrap').html('Pesan telah dikirim');
				}
			})
			return false;
		});
        });
    </script>
{/literal}
<div id="dialog-message" title="Pemberitahuan" style="display:none;">
	<p>
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
		Anda belum mengisi pesan.
	</p>
	<p>
		Silakan mengisi pesan yang ingin anda sampaikan dengan ketentuan jumlah Max. karakter adalah 160.
	</p>
</div>
<div id="wrap">
<form id="frmOrmawaBroadcast" action="portal_ormawa_broadcast_insert.php" method="POST">
    <div class="box">

        <div class="box">
            <div class="note">Kepada : </div>
			<!--<ul id="demo1" name="tujuan[]"></ul>-->
			<SELECT name="tujuan">
				<OPTION VALUE="1">ANGGOTA BEM UNIVERSITAS</OPTION>
				<OPTION VALUE="2">KETUA BEM FAKULTAS</OPTION>
				<!--<OPTION VALUE="3">ANGGOTA BEM FAKULTAS</OPTION>-->
			</SELECT>
			<div class="note">Pesan : </div>
			<div>
				<div style="margin-bottom:10px;width:625px;">
					<textarea id="pesan" name="pesan" style="width:100%; height:50px;"></textarea>
				</div>
				<div><span style="color:#999;font-weight:500;float:left;"><img src="../../img/portal/Document-Write-icon.png" /></span><div id="btn-cancel" style="float:right;height:25px;margin-right:15px;" class="disable-ajax">Batal</div><div id="btn-send" style="float:right;height:25px;margin-right:15px;">Kirim</div></div>

			</div>
			<br /><br /><br />
			<div>&copy; SMS GATEWAY UNIVERSITAS AIRLANGGA CYBER CAMPUS</div>
        </div>
		
    </div>
</form>

</div>


