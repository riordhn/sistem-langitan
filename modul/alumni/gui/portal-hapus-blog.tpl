<script>
	$(function() {
	
		{for $foo=0 to $jml_data_blog-1}
		
			// Button Set
			$( "#submit{$id_blog[$foo]}").buttonset();
			
			$("#btnDelete{$id_blog[$foo]}").button({
				icons: {
					primary: "ui-icon-trash",
				},
				text: true
			});
			
			$("#btnApprove{$id_blog[$foo]}").button({
				icons: {
					primary: "ui-icon-check",
				},
				text: true
			});
			
			// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
			// Modal
			
			
			// Event Click
			$('#btnEdit{$id_blog[$foo]}').click(function(){		
				$('#judul_blog_div{$id_blog[$foo]}').hide();
				$('#link_blog_div{$id_blog[$foo]}').hide();
				$('#image_link_blog_div{$id_blog[$foo]}').hide();
				$('#isi_blog_div{$id_blog[$foo]}').hide();
				$('#resume_blog_div{$id_blog[$foo]}').hide();
				
				$('#judul_blog{$id_blog[$foo]}').show();					
				$('#link_blog{$id_blog[$foo]}').show();
				$('#image_link_blog{$id_blog[$foo]}').show();
				$('#isi_blog{$id_blog[$foo]}').show();
				$('#resume_blog{$id_blog[$foo]}').show();
				
				$('#btnEdit{$id_blog[$foo]}').hide();
				$('#submit{$id_blog[$foo]}').show();
			});
			
			$('#btnDelete{$id_blog[$foo]}').click(function(){
			
				$('#MyFormDelete{$id_blog[$foo]}').submit();
				
			});
			
			/*
			$('#btnDelete{$id_blog[$foo]}').click(function(){		
				$( "#dialog{$id_blog[$foo]}:ui-dialog" ).dialog( "destroy" );
				$( "#dialog-confirm{$id_blog[$foo]}" ).dialog({
					resizable: false,
					height:140,
					modal: true,
					buttons: {
						"Delete": function() {
							//$("#dialog-confirm{$id_blog[$foo]}").dialog( "destroy" );
							$('#btnDelete{$id_blog[$foo]}').click(function() {
							  $('#MyFormDelete{$id_blog[$foo]}').submit();
							});
							//$("#del_blog{$id_blog[$foo]}").hide();

							//alert("Data Berhasil Dihapus");
						},
						Cancel: function() {
							$("#dialog-confirm{$id_blog[$foo]}").dialog( "destroy" );
						}
					}
				});
			});
			*/
			
			// Event Submit
				$('#MyFormDelete{$id_blog[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
								//$("#dialog-confirm{$id_blog[$foo]}").dialog( "destroy" );
								//$('#btnDelete{$id_blog[$foo]}').click(function() {
								//$('#MyFormDelete{$id_blog[$foo]}').submit();
								//});
								//$("#del_blog{$id_blog[$foo]}").hide();

								alert("Data Berhasil Dihapus");
								window.location.href = '/modul/mhs/#portal-blog!portal-hapus-blog.php';
							
						}
					})
					return false;
				});
			
			
		{/for}
	
	});
</script>
	
<div class="center_title_bar">
	<a style="color:#CCC;" href="portal-blog.php">Semua blog</a> | 
	<a style="color:#CCC;" href="portal-tambah-blog.php"> Tambah blog </a> | 
	<a> Hapus blog </a> 
</div>
<br />
<a style="color:#CCC; font-size:14px; color:#000;" href="portal-hapus-blog.php"> Tampilkan Semua blog </a>
<br /><br />

<div class="column-blog">	
	<div class="portlet-blog">
		<section>
			<header class="ui-widget-header">
				<div class="portlet-header" style="padding:3px;">blog{$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun}</div>
			</header>
		<div id="blog-content" class="portlet-content-blog ui-widget-content" style="background:#fff;">
			<div style="margin:20px;">
				{for $foo=0 to $jml_data_blog-1}
						<form id="MyFormDelete{$id_blog[$foo]}" name="MyFormDelete{$id_blog[$foo]}" method="post" action="portal-aksi-hapus-blog.php">
							{if $id!=""}
								<div  id="btnDelete{$id_blog[$foo]}" name="btnDelete{$id_blog[$foo]}" style="height:30px; width:80px;" title="Hapus blog">
									Hapus
								</div>
							{/if}
							<blog id="blog{$foo}">
							<input style="display:none;" type="text" name="id_blog" value="{$id_blog[$foo]}" >
							<div>
								&nbsp;
							</div>
							<div class="title">
								
								Judul : <a class="disable-ajax" id="judul_blog_div{$id_blog[$foo]}" href="#portal-blog!portal-hapus-blog.php?id={$id_blog[$foo]}&view=all">{$judul_blog[$foo]}</a>
								<textarea style="width:100%; height:30px;display:none;" COLS=30 ROWS=3   id="judul_blog{$id_blog[$foo]}" name="judul_blog{$id_blog[$foo]}" >{$judul_blog[$foo]}</textarea>
							</div>
							<div class="date">
								Oleh : &nbsp; {$pengguna[$foo]}
								<br />
								Tanggal : &nbsp; {$tgl_blog[$foo]}
							</div>
							<div class="content_blog">
								{if $view == "all"}
									<legend>Link</legend>
									<div style="" id="link_blog_div{$id_blog[$foo]}">{$link_blog[$foo]}</div><br />								
									<TEXTAREA id="link_blog{$id_blog[$foo]}" style="width:100%;height:30px;display:none;" NAME="link_blog{$id_blog[$foo]}" COLS=30 ROWS=6>{$link_blog[$foo]}</TEXTAREA>
									
									<legend>Image Link</legend>
									<div style="" id="image_link_blog_div{$id_blog[$foo]}"><img style="width:100px; height:100px;" src="{$img_link[$foo]}"/></div><br />
									<TEXTAREA id="image_link_blog{$id_blog[$foo]}" style="width:100%; height:30px;display:none;" NAME="image_link_blog{$id_blog[$foo]}" COLS=30 ROWS=6>{$img_link[$foo]}</TEXTAREA>
									
									<legend>Resume</legend>
									<div style="" id="resume_blog_div{$id_blog[$foo]}">{$isi_blog[$foo]}</div><br />
									<TEXTAREA id="resume_blog{$id_blog[$foo]}" style="width:100%; display:none; min-height:150px;" NAME="resume_blog{$id_blog[$foo]}" COLS=30 ROWS=6>{$resume_blog[$foo]}</TEXTAREA>
									
									<legend>Isi</legend>
									<div style="" id="isi_blog_div{$id_blog[$foo]}">{$isi_blog[$foo]}</div><br />
									<TEXTAREA id="isi_blog{$id_blog[$foo]}" style="width:100%; display:none; min-height:700px;" NAME="isi_blog{$id_blog[$foo]}" COLS=30 ROWS=6>{$isi_blog[$foo]}</TEXTAREA>
								{else}
									{$resume_blog[$foo]}
								{/if}
							</div>
							</blog>
							
						</form>
					
				{/for}
			</div>
		</div>
		</section>
	</div>
</div>