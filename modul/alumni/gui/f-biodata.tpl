<div class="center_title_bar">A KARAKTERISTIK SOSIO-BIOGRAFI, PENDIDIKAN DAN PEKERJAAN SEBELUM KULIAH</div>
{literal}
    <style>
        .title{
            background-color: #ccccff;
            font-weight: bold;
            font-size: large;
            border-top: 2px solid black;
        }
        .ket_soal{
            font-weight: normal;
            font-size: smaller;
        }
        .ket_jawaban{
            font-size: 10px;
        }
        .info_jawaban{
            text-transform: uppercase;
            font-weight: bold;
        }
    </style>
{/literal}
{if $status_isi!=''}
    {$status_isi}
{else}
    <form id="form_soal" method="post" action="f-biodata.php">
        <table class="form" style="width: 90%;font-family: Arial">
            {foreach $data_soal as $data}
                <tr class="title">
                    <td>{$data.NOMER} {$data.SOAL}</td>
                </tr>
                {foreach $data.DATA_JAWABAN as $jawaban}
                    {if $data.TIPE_SOAL==1}
                        <tr {if $jawaban.URUTAN%2==0}class="row1"{/if}>
                            <td>
                                {if $jawaban.ID_SOAL==14}
                                    <input type="text" name="{$data.NOMER}" size="3" maxlength="3" /> Bulan {$jawaban.JAWABAN}&nbsp;&nbsp;&nbsp;
                                    <input type="hidden" name="{$data.NOMER}jaw" value="{$jawaban.ID_JAWABAN}" />
                                    {if $jawaban.TIPE_JAWABAN==3}
                                        <input type="text" name="{$data.NOMER}ket" size="{$jawaban.MAX_LENGTH}" maxlength="{$jawaban.MAX_LENGTH}" />
                                    {/if}
                                {else}
                                    {if count($data.DATA_JAWABAN)>1}
                                        {$jawaban.JAWABAN}&nbsp;&nbsp;&nbsp;
                                        <input type="text" name="{$data.NOMER}{$jawaban.URUTAN}" size="{$jawaban.MAX_LENGTH}" maxlength="{$jawaban.MAX_LENGTH}" />
                                        <input type="hidden" name="{$data.NOMER}{$jawaban.URUTAN}jaw" value="{$jawaban.ID_JAWABAN}" />
                                    {else}
                                        {$jawaban.JAWABAN}&nbsp;&nbsp;&nbsp;
                                        <input type="text" name="{$data.NOMER}" size="{$jawaban.MAX_LENGTH}" maxlength="{$jawaban.MAX_LENGTH}" />
                                        <input type="hidden" name="{$data.NOMER}jaw" value="{$jawaban.ID_JAWABAN}" />
                                    {/if}

                                {/if}
                            </td>
                        </tr>
                    {else if $data.TIPE_SOAL==2}
                        {if $jawaban.GROUP_JAWABAN!=''}
                            {if $jawaban.SKOR_JAWABAN==1}
                                <tr>
                                    <td>
                                        <span class="info_jawaban">{$jawaban.JAWABAN}</span>
                                        {if $jawaban.TIPE_JAWABAN==3}
                                            <input type="text" name="{$data.NOMER}g{$jawaban.GROUP_JAWABAN}ket" size="{$jawaban.MAX_LENGTH}" maxlength="{$jawaban.MAX_LENGTH}" />
                                        {/if}
                                        <br/>
                                        <span class="ket_soal"><i>{$data.KETERANGAN_SOAL}</i></span>
                                    </td>
                                </tr>
                            {/if}
                            <tr {if $jawaban.SKOR_JAWABAN%2==0}class="row1"{/if}>
                                <td>
                                    Skor {$jawaban.SKOR_JAWABAN}<input type="radio" name="{$data.NOMER}g{$jawaban.GROUP_JAWABAN}" value="{$jawaban.ID_JAWABAN}" />
                                </td>
                            </tr>
                        {else}
                            <tr {if $jawaban.URUTAN%2==0}class="row1"{/if}>
                                <td>
                                    <input type="radio" name="{$data.NOMER}" value="{$jawaban.ID_JAWABAN}" />{$jawaban.JAWABAN}
                                    {if $jawaban.TIPE_JAWABAN==3}
                                        <input type="text" name="{$data.NOMER}ket" size="{$jawaban.MAX_LENGTH}" maxlength="{$jawaban.MAX_LENGTH}" />
                                    {/if}
                                </td>
                            </tr>
                        {/if}
                    {else}
                        <tr {if $jawaban.URUTAN%2==0}class="row1"{/if}>
                            <td>
                                <input type="checkbox" name="{$data.NOMER}{$jawaban.URUTAN}" value="{$jawaban.ID_JAWABAN}"/>&nbsp;&nbsp;&nbsp;{$jawaban.JAWABAN}
                                {if $jawaban.TIPE_JAWABAN==3}
                                    <input type="text" name="{$data.NOMER}{$jawaban.URUTAN}ket" size="{$jawaban.MAX_LENGTH}" maxlength="{$jawaban.MAX_LENGTH}" />
                                {/if}
                            </td>
                        </tr>
                    {/if}
                {/foreach}
            {/foreach}
            <tr>
                <td class="center">
                    <input type="submit" class="ui-button ui-state-active" style="padding: 5px;cursor: pointer" value="Simpan"/>
                    <input type="hidden" name="mode" value="simpan"/>
                </td>
            </tr>
        </table>
    </form>
{/if}
{literal}
    <script type="text/javascript">
        $('#form_soal').validate();
    </script>
{/literal}