<div class="center_title_bar">FORM TRACER STUDI</div>
{if $status_form!=''}
    {$status_form}
{else}
    {if $smarty.get.kat==''}
        {$info_isi}
    {/if}
    <p></p>
    <form id="form_kat_soal" action="tracer-studi.php" method="get">
        <table style="width: 100%" class="ui-widget-content">
            <tr class="ui-widget-header">
                <th colspan="2" class="center">FORM PENGISIAN TRACER STUDI</th>
            </tr>
            <tr>
                <td>Pilih Kategori Soal</td>
                <td>
                    <select name="kat" onchange="$('#form_kat_soal').submit()">
                        <option value="">Pilih Kategori Soal</option>
                        {foreach $kategori_soal as $ks}
                            <option value="{$ks.ID_KATEGORI_SOAL}" {if $smarty.get.kat==$ks.ID_KATEGORI_SOAL} selected="true" {/if}>{$ks.KODE_KATEGORI} {$ks.NM_KATEGORI}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <!--                <td colspan="2" class="center"><a href="tracer-offline-pdf.php?f=1" class="disable-ajax ui-button ui-state-active" target="_blank" style="padding: 5px;cursor: pointer">Tracer Offline</a></td>-->
                <td colspan="2" class="center"><a href="tracer_offline.pdf" class="disable-ajax ui-button ui-state-active" target="_blank" style="padding: 5px;cursor: pointer">Tracer Offline</a></td>
            </tr>
        </table>
    </form>
    {if isset($data_soal)}
        {literal}
            <style>
                .title{
                    background-color: #ccccff;
                    font-weight: bold;
                    font-size: large;
                    border-top: 2px solid black;
                }
                .ket_soal{
                    font-weight: normal;
                    font-size: smaller;
                }
                .ket_jawaban{
                    font-size: 10px;
                }
                .info_jawaban{
                    text-transform: uppercase;
                    font-weight: bold;
                }
            </style>
        {/literal}
        {if $status_isi!=''}
            {$status_isi}
        {else}
            <form id="form_soal" method="post" action="tracer-studi.php?{$smarty.server.QUERY_STRING}">
                <table class="ui-widget" style="width: 100%;font-family: Arial">
                    {foreach $data_soal as $s}
                        <tr class="ui-widget-header">
                            <th style="padding: 8px">{$s.NOMER} {$s.SOAL}</th>
                        </tr>
                        <!--HARCODE DATA-->
                        {if $s.NOMER=='A1'||$s.NOMER=='A2'||$s.NOMER=='B1'||$s.NOMER=='B6UI'}
                            {if $s.NOMER=='A1'}
                                {foreach $s.DATA_JAWABAN as $j}
                                    <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                                        <td>
                                            <input type="radio" name="{$s.NOMER}" value="{$j.ID_JAWABAN}" {if $data_mhs.KELAMIN_PENGGUNA==$j.ID_JAWABAN}checked="true"{/if} />{$j.JAWABAN}
                                        </td>
                                    </tr>
                                {/foreach}
                            {else if $s.NOMER=='A2'}
                                {foreach $s.DATA_JAWABAN as $j}
                                    <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                                        <td>
                                            <input type="text" size="5"  name="{$s.NOMER}{$j.URUTAN}" value="{$data_mhs.TAHUN_LAHIR}"  />
                                            <input type="hidden" name="{$s.NOMER}{$j.URUTAN}jaw" value="{$j.ID_JAWABAN}" />
                                            {$j.JAWABAN}
                                            {if $j.TIPE_JAWABAN==2}
                                                <input type="text" name="{$s.NOMER}{$j.URUTAN}ket" size="{$j.MAX_LENGTH}" maxlength="{$j.MAX_LENGTH}" />
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {else if $s.NOMER=='B1'}
                                {foreach $s.DATA_JAWABAN as $j}
                                    <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                                        <td>
                                            <input type="text" size="50"  name="{$s.NOMER}{$j.URUTAN}" value="{$data_mhs.NM_FAKULTAS}, {$data_mhs.NM_JENJANG} {$data_mhs.NM_PROGRAM_STUDI}"  />
                                            <input type="hidden" name="{$s.NOMER}{$j.URUTAN}jaw" value="{$j.ID_JAWABAN}" />
                                            {$j.JAWABAN}
                                            {if $j.TIPE_JAWABAN==2}
                                                <input type="text" name="{$s.NOMER}{$j.URUTAN}ket" value="{$data_mhs.TAHUN_LULUS}" size="{$j.MAX_LENGTH}" maxlength="{$j.MAX_LENGTH}" />
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {else if $s.NOMER=='B6UI'}
                                {foreach $s.DATA_JAWABAN as $j}
                                    <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                                        <td>
                                            <input type="text" value="{$data_akad.IPK}" size="10" name="{$s.NOMER}{$j.URUTAN}"  />
                                            <input type="hidden" name="{$s.NOMER}{$j.URUTAN}jaw" value="{$j.ID_JAWABAN}" />
                                            {$j.JAWABAN}
                                            {if $j.TIPE_JAWABAN==2}
                                                <input type="text" name="{$s.NOMER}{$j.URUTAN}ket" size="{$j.MAX_LENGTH}" maxlength="{$j.MAX_LENGTH}" />
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                        {else}
                            {if $s.TIPE_SOAL==1}
                                <!--UNTUK TIPE SOAL CATEGORICAL VARIABEL-->
                                {foreach $s.DATA_JAWABAN as $j}
                                    <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                                        <td>
                                            <input type="radio" name="{$s.NOMER}" value="{$j.ID_JAWABAN}" />{$j.JAWABAN}
                                            {if $j.TIPE_JAWABAN==2}
                                                <input type="text" name="{$s.NOMER}ket" size="{$j.MAX_LENGTH}" maxlength="{$j.MAX_LENGTH}" />
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {else if $s.TIPE_SOAL==2}
                                <!--UNTUK TIPE SOAL MULTIPLE DICHOTOMOUS--> 
                                {foreach $s.DATA_JAWABAN as $j}
                                    <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                                        <td>
                                            <input type="checkbox" name="{$s.NOMER}{$j.URUTAN}" value="{$j.ID_JAWABAN}"/>&nbsp;&nbsp;&nbsp;{$j.JAWABAN}
                                            {if $j.TIPE_JAWABAN==2}
                                                <input type="text" name="{$s.NOMER}{$j.URUTAN}ket" size="{$j.MAX_LENGTH}" maxlength="{$j.MAX_LENGTH}" />
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {else if $s.TIPE_SOAL==3}
                                <!--UNTUK TIPE SOAL TEXT VARIABEL--> 
                                {foreach $s.DATA_JAWABAN as $j}
                                    <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                                        <td>
                                            <input type="text" size="10" name="{$s.NOMER}{$j.URUTAN}"  />
                                            <input type="hidden" name="{$s.NOMER}{$j.URUTAN}jaw" value="{$j.ID_JAWABAN}" />
                                            {$j.JAWABAN}
                                            {if $j.TIPE_JAWABAN==2}
                                                <input type="text" name="{$s.NOMER}{$j.URUTAN}ket" size="{$j.MAX_LENGTH}" maxlength="{$j.MAX_LENGTH}" />
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {else if $s.TIPE_SOAL==4}
                                <!--UNTUK TIPE SOAL LONG TEXT--> 
                                {foreach $s.DATA_JAWABAN as $j}
                                    <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                                        <td>
                                            <input type="text" size="50"  name="{$s.NOMER}{$j.URUTAN}"  />
                                            <input type="hidden" name="{$s.NOMER}{$j.URUTAN}jaw" value="{$j.ID_JAWABAN}" />
                                            {$j.JAWABAN}
                                            {if $j.TIPE_JAWABAN==2}
                                                <input type="text" name="{$s.NOMER}{$j.URUTAN}ket" size="{$j.MAX_LENGTH}" maxlength="{$j.MAX_LENGTH}" />
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {else if $s.TIPE_SOAL==5}
                                <!--UNTUK TIPE SOAL METRIC VARIABEL--> 
                                {foreach $s.DATA_JAWABAN as $j}
                                    <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                                        <td>
                                            <input type="text" size="5" maxlength="5" name="{$s.NOMER}{$j.URUTAN}"  />
                                            <input type="hidden" name="{$s.NOMER}{$j.URUTAN}jaw" value="{$j.ID_JAWABAN}" />
                                            {$j.JAWABAN}
                                            {if $j.TIPE_JAWABAN==2}
                                                <input type="text" name="{$s.NOMER}{$j.URUTAN}ket" size="{$j.MAX_LENGTH}" maxlength="{$j.MAX_LENGTH}" />
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            {else if $s.TIPE_SOAL==6}
                                <!--UNTUK TIPE SOAL ORDINAL VARIABEL--> 
                                <tr class="row1">
                                    <td>{$s.KETERANGAN_SOAL}</td>
                                </tr>
                                <tr>
                                    <td>
                                        {$group_jawaban=0}
                                        {$gj=0}
                                        {foreach $s.DATA_JAWABAN as $j}
                                            {if $group_jawaban!=$j.GROUP_JAWABAN}
                                                {if $group_jawaban!=0}
                                                    <br/><br/>
                                                {/if}
                                                <span class="info_jawaban">{$j.JAWABAN}</span>
                                                <label class="error" for="{$s.NOMER}g{$j.GROUP_JAWABAN}" style="font-size:0.8em ;display: none">Mohon Diisi</label>
                                                <hr style="margin: 4px 0px"/>
                                                {$group_jawaban=$j.GROUP_JAWABAN}
                                            {/if}
                                            {$j.SKOR_JAWABAN}<input type="radio" class="required" name="{$s.NOMER}g{$j.GROUP_JAWABAN}" value="{$j.ID_JAWABAN}" />
                                        {/foreach}
                                        <br/>

                                    </td>
                                </tr>
                            {/if}
                        {/if}
                    {/foreach}
                    <tr>
                        <td class="center">
                            <input type="submit" class="ui-button ui-state-active" style="padding: 5px;cursor: pointer" value="Simpan"/>
                            <input type="hidden" name="mode" value="simpan"/>
                        </td>
                    </tr>
                </table>
            </form>
        {/if}
    {/if}
{/if}
{literal}
    <script>
        $('#form_soal').validate();
    </script>
{/literal}