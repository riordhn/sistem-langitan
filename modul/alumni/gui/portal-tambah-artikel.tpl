	<script>
		$(function() {
			$( "#tabs" ).tabs();
			
		});	
	</script>
	
<script type="text/javascript">
tinyMCE.init({
        mode : "textareas",
        theme : "advanced",
        editor_selector : "mceEditor",
        editor_deselector : "mceNoEditor",
		  plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,",
		  theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		  theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		  theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		  theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
		  theme_advanced_toolbar_location : "top",
		  theme_advanced_toolbar_align : "left",
		  theme_advanced_statusbar_location : "bottom",
		  theme_advanced_resizing : true
});
</script>
	
	<script>
		$(function(){
				$("#submit").buttonset();
				$('#MyFormAdd').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
							$('#result').show();
							$('#MyFormAdd').hide();
							
							var judul_div = document.getElementById('judul_div');
							var isi_div = document.getElementById('isi_div');
							
							judul_div.innerHTML = document.getElementById('judul').value;
							isi_div.innerHTML = document.getElementById('isi').value;

						}
					})
					return false;
				});
		});
	</script>
	
	<!-- style article -->
	<script>
		$(function() {
			$( ".column-article" ).sortable({
				connectWith: ".column-article"
			});

			$( ".portlet-article" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
				.find( ".portlet-header-article" )
					.addClass( "ui-widget-header ui-corner-all" )
					.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
					.end()
				.find( ".portlet-content-article" );

			$( ".portlet-header-article .ui-icon" ).click(function() {
				$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
				$( this ).parents( ".portlet-article:first" ).find( ".portlet-content-article" ).toggle();
			});

			//$( ".column" ).disableSelection();
			
		});
		
		//$(document).ready(function() {
			//$("a").addClass("mceButton disable-ajax");
			//$("span").addClass("disable-ajax");
		//});

	</script>
	<!-- end style -->
<div class="center_title_bar">
	<a style="color:#CCC;"  href="portal-article.php">Semua Artikel</a> | <a> Tambah Artikel </a> | <a style="color:#CCC;" href="portal-hapus-artikel.php"> Hapus Artikel </a> 
</div>

<div id="result" style="display:none;">
	Artikel Berhasil ditambahkan 
	<br />
</div>

	<form id="MyFormAdd" name="MyFormAdd" method="post" action="portal-aksi-tambah-artikel.php">
		<table style="width:100%" class="ui-widget">
			<thead class="ui-widget-header">
				<tr>
					<td width="20%">Item</td><td>Isian</td>
				</tr>
			</thead>
			<tbody class="ui-widget-content" valign="top">
				<tr>	
					<td >Judul</td>
					<td>
						<TEXTAREA id="judul" style="width:600px; height:30px;" NAME="judul" COLS=30 ROWS=6></TEXTAREA>
						<input type="text" id="id_pengguna" style="width:600px; height:30px; display:none;" NAME="id_pengguna" value="{$id_pengguna}"/>
					</td>
				</tr>
				<tr>	
				<tr>	
					<td valign="top" style="text-align:top;">Image Link</td>
					<td>
						<TEXTAREA id="image_link" style="width:600px; height:30px;" NAME="image_link" COLS=30 ROWS=6></TEXTAREA>
					</td>
				</tr>
				<tr>	
					<td valign="top" style="text-align:top;">Resume</td>
					<td>
						<TEXTAREA id="resume" style="width:600px; height:120px;" NAME="resume" COLS=30 ROWS=6></TEXTAREA>
					</td>
				</tr>
				<tr>	
					<td valign="top" style="text-align:top;">Isi</td>
					<td>
						<TEXTAREA class="mceEditor" id="isi" style="width:600px; min-height:700px;" NAME="isi" COLS=30 ROWS=6></TEXTAREA>
					</td>
				</tr>
				<tr>	
					<td></td>
					<td>
						<div id="submit" name="submit" >
							<input type="submit" name="btn_submit" id="btn_submit" value="Submit"  />
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</form>