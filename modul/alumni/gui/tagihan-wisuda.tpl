<div class="center_title_bar">STATUS WISUDA </div>
{if $error==''}
    {$congrats}
    <p></p>
    <table class="ui-widget ui-widget-content">
        <tr class="ui-widget-header">
            <th colspan="2" class="center">DETAIL TAGIHAN WISUDA</th>
        </tr>
        <tr>
            <td>Nama </td>
            <td>{$data_bayar.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>{$data_bayar.NIM_MHS}</td>
        </tr>
        <tr>
            <td>Prodi/Fakultas</td>
            <td>{$data_bayar.NM_JENJANG} {$data_bayar.NM_PROGRAM_STUDI}, {$data_bayar.NM_FAKULTAS}</td>
        </tr>
        <tr>
            <td>Periode Wisuda</td>
            <td>{$data_bayar.NM_TARIF_WISUDA}</td>
        </tr>
        <tr>
            <td>Tarif Wisuda</td>
            <td>{number_format($data_bayar.TARIF_WISUDA)}</td>
        </tr>
        <tr>
            <td>Cicilan Wisuda</td>
            <td>{number_format($data_bayar.CICILAN)}</td>
        </tr>
        <tr>
            <td>Besar Tagihan Wisuda</td>
            <td>
                {if $data_bayar.TARIF_WISUDA<$data_bayar.CICILAN}
                    0
                {else}
                    {number_format($data_bayar.TARIF_WISUDA-$data_bayar.CICILAN)}
                {/if}
            </td>
        </tr>
        <tr class="ui-widget-header">
            <th colspan="2" class="center">STATUS PEMBAYARAN</th>
        </tr>
        <tr>
            <td>Tanggal Bayar</td>
            <td>{$data_bayar.TGL_BAYAR}</td>
        </tr>
        <tr>
            <td>No Transaksi</td>
            <td>{$data_bayar.NO_TRANSAKSI}</td>
        </tr>
        <tr>
            <td>Bank</td>
            <td>{$data_bayar.NM_BANK}</td>
        </tr>
        <tr>
            <td>Bank Via</td>
            <td>{$data_bayar.NAMA_BANK_VIA}</td>
        </tr>
        <tr>
            <td>Terbayar</td>
            <td>{number_format($data_bayar.BESAR_BIAYA)}</td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td>{$data_bayar.KETERANGAN}</td>
        </tr>
    </table>
{else}
    {$error}
{/if}