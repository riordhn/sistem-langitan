<script>
	
	$(document).ready(function(){
		$( "#btn-save" ).button({
			text: true
		});
		$( "#btn-cancel" ).button({
			text: true
		});
		
		$('#btn-save').click(function(){	
			var ed = tinyMCE.get('content');
			var isi = ed.getContent();
			$("#get_isi").val(isi);
			
			var judul = document.getElementById('judul').value;
			//var content =  tinyMCE.get('content').getContent();
			var content =  document.getElementById('get_isi').value;
			var dataString = 'judul='+ judul + '&get_isi=' + content;
			tinyMCE.triggerSave();
			$('#frmOrmawaUpdate').submit();
		})
		
		$('#btn-cancel').click(function(){	
			var id = document.getElementById('id_ormawa').value;
			window.location.href = '#portal-ORMAWA!portal_ormawa_display.php?id='+id;
		})
		
		$('#frmOrmawaUpdate').submit(function() {
			var id = document.getElementById('id_ormawa').value;
			$.ajax({
				type: 'GET',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
					//$('#display').load('portal_ormawa_display.php');
					tinyMCE.execCommand('mceRemoveControl', false, 'content');
					//tinyMCE.execCommand('mceAddControl', false, 'content');
					window.location.href = '#portal-ORMAWA!portal_ormawa_display.php?id='+id;
				}
			})
			return false;
		});
	});

</script>
<script type="text/javascript">
	tinyMCE.init({
			mode : "textareas",
			theme : "advanced",
			editor_selector : "mceEditor",
			editor_deselector : "mceNoEditor",
			theme_advanced_buttons1 : "mylistbox,mysplitbutton,bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,undo,redo,link,unlink",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom"
	});
</script>

{for $i=0 to $jml_data-1}
<form method="GET" action="portal_ormawa_update.php" id="frmOrmawaUpdate">
	<input type="hidden" id="get_isi" name="get_isi">
	<input type="hidden" id="id_ormawa" name="id_ormawa" value="{$id_ormawa[$i]}">
	<div style="width:700px;">
		<div style="float:left; width:60px;">
		<img src="{$foto[$i]}" style="width:40px; height:50px;" />
		</div>
		<div style="float:left; width:600px;">
			<div>
				<img src="../../img/portal/Document-Write-icon.png" style="float:left; margin-right:10px;"/> &nbsp; <input style="float:left;width:300px;padding:3px;" type="text" value="{$judul[$i]}" id="judul" name="judul"/>
				<div id="btn-cancel" style="float:right;height:25px;width:70px;font-size:11px;margin-right:0px;">
					Batalkan
				</div>
				<div id="btn-save" style="float:right;height:25px;width:70px;font-size:11px;margin-right:3px;">
					Simpan
				</div>

			</div>
			<br />
			<div style="width:600px;">
				<textarea id="content"  class="mceEditor" style="width:100%; min-height:700px;" NAME="content">{$content[$i]}</textarea>
			</div>
			<br />
			<div><span style="color:#999;font-weight:500;"><img src="../../img/portal/Document-Write-icon.png" />&nbsp;{$tanggal[$i]}</span>&nbsp;{$count[$i]}</div>
		</div>
	</div>
	
	<div style="clear:both;">
	</div>
	<br /><br />
</form>
{/for}