<script>
	function ImgError(source){
		source.src = "/css/images/android.jpeg";
		source.onerror = "";
		return true;
	}
	
	{for $i=0 to 6}
		$("#plus-friends{$id_pengguna[$i]}").button({
			icons: {
				primary: "ui-icon-circle-plus",
			},
			text: false
		});
		


		$('#plus-friends{$id_pengguna[$i]}').click(function(){		

			$( "#dialog:ui-dialog" ).dialog( "destroy" );
			$('#dest_req_friend').replaceWith('<span id="#dest_req_friend">{$id_pengguna[$i]}</span>');

			$( "#dialog-message" ).dialog({
				resizable: false,
				height:200,
				width:400,
				modal: true,
				buttons: {
					"Setujui permintaan pertemanan": function() {
						$( this ).dialog( "close" );
						$('#formRequestFriend{$id_pengguna[$i]}').submit();
						$('#recomend-friends').load('other-friends.php');
							
					},
					Cancel: function() {
						$( this ).dialog( "close" );
						$('#recomend-friends').load('other-friends.php');
					}
				}
			});
			
		});
		
		$('#formRequestFriend{$id_pengguna[$i]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						//$('#recomend-friends').html('data');
						//$('#new_comment{$id_status[$i]}').val('');	
						
						//$('#status{$id_status[$i]}').load('sn_comment.php?id={$id_status[$i]}');	
						//$('#status{$id_status[$i]}').slideDown('fast');										

							
						}
					})
					
					return false;
					
		});
		
	{/for}

</script>

<div id="dialog-message" title="Permintaan petemanan terkirim" style="display:none;">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;">
	</span>
	Anda telah meminta permintaan teman. Permintaan teman akan dikirim setelah anda menyutujuinya. 
	</span>
	</p>
</div>

<div id="recomend-friends">
<table width="100%" border="0" class="ui-widget">
{for $i=0 to 6}
	<tr class="ui-widget-content">
		<td class="ui-widget-content">
		<img alt="Smiley face" src="/foto_mhs/{$username[$i]}.JPG" onerror="ImgError(this);" style="width:35px; height:35px;"/>
		</td>
		<td class="ui-widget-content">
		{$nm_pengguna[$i]}
		</td>
		<td class="ui-widget-content">
		
			<div id="plus-friends{$id_pengguna[$i]}" title="Tambahkan teman {$id_pengguna[$i]}">
			&nbsp;
				<form id="formRequestFriend{$id_pengguna[$i]}" name="formRequestFriend{$id_pengguna[$i]}" action="/modul/mhs/sn_request_friend.php" method="post" style="display:none;">
					<input style="display:none;" type="text" id="dest_id_pengguna" name="dest_id_pengguna" value="{$id_pengguna[$i]}" />
					<input style="display:none;" type="text" id="cur_id_pengguna" name="cur_id_pengguna" value="{$cur_id_pengguna}" />
				</form>
			</div>
		</td>
	</tr>
{/for}
</table>
</div>