<script>
	function ImgError(source){
		source.src = "../../css/images/android.jpeg";
		source.onerror = "";
		return true;
	}
	
	{for $i=0 to $jml_permintaan_teman-1}
		$("#confirm-friends{$id_teman[$i]}").button({
			icons: {
				primary: "ui-icon-check",
			},
			text: true
		});
		$("#lain-kali-confirm-friends{$id_teman[$i]}").button({
			icons: {
				primary: "ui-icon-check",
			},
			text: true
		});
		
		$('#confirm-friends{$id_teman[$i]}').click(function(){		

			$( "#dialog:ui-dialog" ).dialog( "destroy" );
			$('#dest_req_friend').replaceWith('<span id="#dest_req_friend">{$id_teman[$i]}</span>');

			$( "#dialog-message" ).dialog({
				resizable: false,
				height:200,
				width:400,
				modal: true,
				buttons: {
					"Setujui permintaan pertemanan": function() {
						$( this ).dialog( "close" );
						$('#formConfirmRequestFriend{$id_teman[$i]}').submit();	
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				}
			});
			
		});
		
		$('#formConfirmRequestFriend{$id_teman[$i]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
							$('#request-friends').load('sn_pertemanan.php');							
						}
					})
					return false;			
		});
		
	{/for}

</script>

<div id="dialog-message" title="Permintaan petemanan terkirim" style="display:none;">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Fitur permintaan pertemanan akan segera aktif dalam waktu dekat. Terimakasih<br />
	{$cur_id_pengguna} menyetujui petemanan kepada <span id="dest_req_friend"> </span>
	</p>
</div>

<div id="request-friends">
<table width="490px" border="0" class="ui-widget" style="border:none;">
{for $i=0 to $jml_permintaan_teman-1}
	<tr class="ui-widget-content">
		<td class="ui-widget-content">
		<img alt="Smiley face" src="/foto_mhs/{$username_teman[$i]}.JPG" onerror="ImgError(this);" style="width:35px; height:35px;"/>
		</td>
		<td class="ui-widget-content">
		{$nm_teman[$i]}
		</td>
		<td class="ui-widget-content">
		
			<div id="confirm-friends{$id_teman[$i]}" title="Konfirmasi pertemanan {$id_teman[$i]}">
			&nbsp;Setujui
			
				<form id="formConfirmRequestFriend{$id_teman[$i]}" name="formConfirmRequestFriend{$id_teman[$i]}" action="sn_confirm_request_friend.php" method="post" style="display:none;">
					<input style="display:none;" type="text" id="id_teman" name="id_teman" value="{$id_teman[$i]}" />
					<input style="display:none;" type="text" id="cur_id_pengguna" name="cur_id_pengguna" value="{$cur_id_pengguna}" />
				</form>
			
			</div>

		</td>
		<td class="ui-widget-content">
			<div id="lain-kali-confirm-friends{$id_teman[$i]}" title="Tunda pertemanan {$id_teman[$i]}">
			&nbsp;Lain kali
			<!--
				<form id="formRequestFriend{$id_pengguna[$i]}" name="formRequestFriend{$id_pengguna[$i]}" action="sn_request_friend.php" method="post" style="display:none;">
					<input style="display:none;" type="text" id="dest_id_pengguna" name="dest_id_pengguna" value="{$id_pengguna[$i]}" />
					<input style="display:none;" type="text" id="cur_id_pengguna" name="cur_id_pengguna" value="{$cur_id_pengguna}" />
				</form>
			-->
			</div>
		</td>
	</tr>
{/for}
</table>
</div>