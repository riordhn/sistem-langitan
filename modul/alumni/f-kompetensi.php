<?php

require_once('config.php');
// no auth security sementara ~sugenk.
//if ($user->Role() != AUCC_ROLE_MAHASISWA){
//	header("location: /logout.php");
//    exit();
//} 
// end no auth security

include 'class/tracer.class.php';

$tracer = new tracer($db);
if ($_SERVER['REMOTE_ADDR'] != '210.57.212.66') {
    die('---');
}
$data_soal = $tracer->load_data_soal(1, 5);

if (isset($_POST)) {
    if (post('mode') == 'simpan') {
        $cek_hasil_form = $tracer->cek_hasil_form($user->ID_PENGGUNA, 1);
        if (empty($cek_hasil_form)) {
            $tracer->insert_hasil_form($user->ID_PENGGUNA, 1);
        }
        $hasil_form = $tracer->cek_hasil_form($user->ID_PENGGUNA, 1);
        foreach ($data_soal as $data) {
            $cek_hasil_soal = $tracer->cek_hasil_soal($hasil_form['ID_HASIL_FORM'], $data['ID_SOAL']);
            if (empty($cek_hasil_soal)) {
                $tracer->insert_hasil_soal($hasil_form['ID_HASIL_FORM'], $data['ID_SOAL']);
                $hasil_soal = $tracer->cek_hasil_soal($hasil_form['ID_HASIL_FORM'], $data['ID_SOAL']);
                if ($data['TIPE_SOAL'] == 1) {
                    $jumlah_jawaban = count($data['DATA_JAWABAN']);
                    if ($jumlah_jawaban > 1) {
                        for ($i = 1; $i <= $jumlah_jawaban; $i++) {
                            if ($_POST[$data['NOMER'] . $i ] != '') {
                                $tracer->insert_hasil_jawaban($hasil_soal['ID_HASIL_SOAL'], post($data['NOMER'] . $i . 'jaw'), post($data['NOMER'] . $i), post($data['NOMER'] . $i . 'ket'));
                            }
                        }
                    } else {
                        if ($_POST[$data['NOMER']] != '') {
                            $tracer->insert_hasil_jawaban($hasil_soal['ID_HASIL_SOAL'], post($data['NOMER'] . 'jaw'), post($data['NOMER']), post($data['NOMER'] . 'ket'));
                        }
                    }
                } else if ($data['TIPE_SOAL'] == 2) {
                    $group_jawaban = $tracer->get_jumlah_group_soal($data['ID_SOAL']);
                    if ($group_jawaban['JUMLAH_GROUP'] != '') {
                        for ($i = 1; $i <= $group_jawaban['JUMLAH_GROUP']; $i++) {
                            if ($_POST[$data['NOMER'] . 'g' . $i] != '') {
                                $tracer->insert_hasil_jawaban($hasil_soal['ID_HASIL_SOAL'], $_POST[$data['NOMER'] . 'g' . $i], '', post($data['NOMER'] . 'g' . $i . 'ket'));
                            }
                        }
                    } else {
                        if ($_POST[$data['NOMER']] != '') {
                            $tracer->insert_hasil_jawaban($hasil_soal['ID_HASIL_SOAL'], post($data['NOMER']), '', post($data['NOMER'] . 'ket'));
                        }
                    }
                } else if ($data['TIPE_SOAL'] == 3) {
                    for ($i = 1; $i <= count($data['DATA_JAWABAN']); $i++) {
                        if ($_POST[$data['NOMER'] . $i] != '') {
                            $tracer->insert_hasil_jawaban($hasil_soal['ID_HASIL_SOAL'], post($data['NOMER'] . $i), '', post($data['NOMER'] . $i . 'ket'));
                        }
                    }
                }
            }
        }
    }
}
$data_pengisian = $tracer->cek_hasil_jawaban(5, $user->ID_PENGGUNA);
if ($data_pengisian['STATUS_ISI'] > 0) {
    if ($tracer->cek_soal_belum_terjawab($user->ID_PENGGUNA) != '') {
        $smarty->assign('status_isi', alert_success("Anda sudah berhasil mengisi form ini, Silahkan Mengisi form lain...<br/>* Form Yang Belum Terjawab <br/>" . $tracer->cek_soal_belum_terjawab($user->ID_PENGGUNA)));
    } else {
        $smarty->assign('status_isi', alert_success("Anda telah berhasil mengisi semua form, terima ksih untuk partisipasinya"));
    }
}
$smarty->assign('data_soal', $data_soal);
$smarty->display('f-kompetensi.tpl');
?>
