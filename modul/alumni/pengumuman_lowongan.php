<?php
require_once('config.php');
// no auth security sementara ~sugenk.
//if ($user->Role() != AUCC_ROLE_MAHASISWA){
//	header("location: /logout.php");
//    exit();
//} 
// end no auth security
	
	$id_pengguna = $user->ID_PENGGUNA;
	
	
	if(isset($_GET['id'])){
		$id_pesan = $_GET['id'];
		
		$db->Query("SELECT 
					  b.nm_pengguna AS pengirim,
					  c.nm_pengguna      AS penerima,
					  a.judul_pesan,
					  a.isi_pesan,
					  b.foto_pengguna,
					  c.foto_pengguna,
					  b.username,
					  c.username, 
					  a.id_pesan
					FROM pesan a
					LEFT JOIN pengguna b
					ON b.id_pengguna = a.id_pengirim
					LEFT JOIN pengguna c
					ON c.id_pengguna    = a.id_penerima
					WHERE a.id_penerima = '$id_pengguna'
					AND a.kategori      = 2
					AND a.id_pesan = '$id_pesan'
		");
		$i = 0;
		$jml_info = 0;
		
		while($row = $db->FetchRow()){
			$id[$i] = $row[8];
			$pengirim[$i] = $row[0];
			$penerima[$i] = $row[1];
			$judul[$i] = $row[2];
			$pesan[$i] = $row[3];
			$foto_pengirim[$i] = $row[4] . '/' . $row[6] . '.JPG';
			//$foto_penerima[$i] = $row[7] . '/' . $row[5] . '.JPG';
			
			$i++;
			$jml_info++;
		}

	}
	else{
		$db->Query("SELECT 
					  b.nm_pengguna AS pengirim,
					  c.nm_pengguna      AS penerima,
					  a.judul_pesan,
					  a.isi_pesan,
					  b.foto_pengguna,
					  c.foto_pengguna,
					  b.username,
					  c.username, 
					  a.id_pesan
					FROM pesan a
					LEFT JOIN pengguna b
					ON b.id_pengguna = a.id_pengirim
					LEFT JOIN pengguna c
					ON c.id_pengguna    = a.id_penerima
					WHERE a.id_penerima = '$id_pengguna'
					AND a.kategori      = 2
		");
		
		$i = 0;
		$jml_info = 0;
		
		while($row = $db->FetchRow()){
			$id[$i] = $row[8];
			$pengirim[$i] = $row[0];
			$penerima[$i] = $row[1];
			$judul[$i] = $row[2];
			$pesan[$i] = substr($row[3], 0, 200);
			$foto_pengirim[$i] = $row[4] . '/' . $row[6] . '.JPG';
			//$foto_penerima[$i] = $row[7] . '/' . $row[5] . '.JPG';
			
			$i++;
			$jml_info++;
		}
	}
	

	
	
	$smarty->assign('id', $id);
	$smarty->assign('pengirim', $pengirim);
	$smarty->assign('penerima', $penerima);
	$smarty->assign('judul', $judul);
	$smarty->assign('pesan', $pesan);
	$smarty->assign('foto_pengirim', $foto_pengirim);
	//$smarty->assign('foto_penerima', $foto_penerima);
	$smarty->assign('jml_info', $jml_info);
	
	$smarty->display("pengumuman_lowongan.tpl");
?>