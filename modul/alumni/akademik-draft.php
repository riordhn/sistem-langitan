<?php

require('config.php');

if ($user->IsLogged() && $user->Role() == 28) {


    $db->Query("
    SELECT M.NIM_MHS,P.PASSWORD_PENGGUNA,P.SE1,M.ID_MHS,PS.ID_JENJANG 
    FROM PENGGUNA P
    JOIN MAHASISWA M ON M.ID_PENGGUNA=P.ID_PENGGUNA
    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
    WHERE P.ID_PENGGUNA='{$user->ID_PENGGUNA}'
    ");
    $data_mhs = $db->FetchAssoc();

    $smarty->assign("mhs", $data_mhs);
    $smarty->assign("info",  alert_success("Untuk Mencetak/Melihat Draft Ijazah silahkan klik link dibawah ini, Terima Kasih", 90));
    $smarty->display('akademik/akademik-draft.tpl');
} else {
    header("location: /logout.php");
    exit();
}
?>
