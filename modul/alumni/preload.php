<?php
require_once('config.php');
// no auth security sementara ~sugenk.
//if ($user->Role() != AUCC_ROLE_MAHASISWA){
//	header("location: /logout.php");
//    exit();
//} 
// end no auth security

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0044)http://yensdesign.com/tutorials/popupjquery/ -->
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>Tracer Study - Universitas Airlangga Cyber Campus</title>
	<link rel="stylesheet" type="text/css" href="/css/maba.css" />
	<link rel="stylesheet" href="css/popup.css" type="text/css" media="screen">
	<link href="jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
	<!-- Get Google CDN's jQuery and jQuery UI with fallback to local -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
	<script src="jquery/jquery-1.7.2.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
	<script src="jquery/jquery-ui-1.8.21.custom.min.js"></script>
	<script src="js/popup.js" type="text/javascript"></script>
	
	<script async="" src="js/cloudflare.min.js"></script>
	
	<script src="jquery.mousewheel.min.js"></script>
	<!-- custom scrollbars plugin -->
	<script src="jquery.mCustomScrollbar.js"></script>
	<script>
		(function($){
			$(window).load(function(){
				$("#contactArea").mCustomScrollbar({
					set_width:false, /*optional element width: boolean, pixels, percentage*/
					set_height:false, /*optional element height: boolean, pixels, percentage*/
					horizontalScroll:false, /*scroll horizontally: boolean*/
					scrollInertia:550, /*scrolling inertia: integer (milliseconds)*/
					scrollEasing:"easeOutCirc", /*scrolling easing: string*/
					mouseWheel:"auto", /*mousewheel support and velocity: boolean, "auto", integer*/
					autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
					scrollButtons:{ /*scroll buttons*/
						enable:false, /*scroll buttons support: boolean*/
						scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
						scrollSpeed:20, /*scroll buttons continuous scrolling speed: integer*/
						scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/
					},
					advanced:{
						updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
						updateOnContentResize:false, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
						autoExpandHorizontalScroll:false /*auto expand width for horizontal scrolling: boolean*/
					},
					callbacks:{
						onScroll:function(){}, /*user custom callback function on scroll event*/
						onTotalScroll:function(){}, /*user custom callback function on bottom reached event*/
						onTotalScrollOffset:0 /*bottom reached offset: integer (pixels)*/
					}
				});
			});
		})(jQuery);
	</script>
	<script>
		$(document).ready(function() {
			$('#popupContactClose').click(function() {
				
						//Getting the variable's value from a link 
				var loginBox = '#login-box';

				//Fade in the Popup
				$('#login-box').fadeIn(300);
				
				//Set the center alignment padding + border see css style
				var popMargTop = ($(loginBox).height() + 24) / 2; 
				var popMargLeft = ($(loginBox).width() + 24) / 2; 
				
				$(loginBox).css({ 
					'margin-top' : -popMargTop,
					'margin-left' : -popMargLeft
				});
				
				// Add the mask to body
				//$('body').append('<div id="mask"></div>');
				$('#mask').fadeIn(300);
				
				return false;
			});
			
			$('#buttonSubmit').click(function(){
				$('#loginForm').submit();
			});
			
			/*
			$('#loginForm').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {	
					}
				})
				return false;
			});
			*/
			
			// When clicking on the button close or the mask layer the popup closed
			$('a.close, #mask').live('click', function() { 
			  $('#mask , .login-popup').fadeOut(300 , function() {
				$('#mask').remove();  
			  }); 
			  return false;
			});
		});
	</script>
</head>
<body onload="centerPopup();loadPopup();" style="background:url(images/bg_body.gif)">
	<!--
	<center>
		<a href="http://www.yensdesign.com/"><img src="./yendesign_files/logo.jpg" alt="Go to yensdesign.com"></a>
		<div id="button"><input type="submit" value="Press me please!"></div>
	</center>
	-->
	<div id="popupContact">
		<a id="popupContactClose">x</a>
		<h1><center>Tracer Study - Universitas Airlangga</center></h1>
		<p id="contactArea">
			<img style="float:left; padding-right:10px;" src="http://4.bp.blogspot.com/_19j87EQervM/SfPGw3AfH6I/AAAAAAAADtk/cwFW4V6btDU/s400/Fasichul+Lisan.jpg" width="120" height="150"/>
			Alumni Universitas Airlangga (UA) yang dibanggakan Almamater, 

			Apa kabar, semoga selalu dalam lindungan Tuhan Yang Maha Esa.Mohon maaf untuk mengganggu waktu dan pemikirannya beberapa saat.

			Saat ini, UA melalui Pusat Pembinaan Karier dan Kewirausahaan (PPKK) sedang terus meningkatkan hubungan dengan para Alumni, termasuk melalui Studi Penelusuran Alumni (Tracer Study) layaknya yang dilakukan universitas-universitas terkemuka di dunia. UA dapat mencapai status world class university tentunya dengan dukungan para Alumni yang dapat kami banggakan. Oleh karenanya, diharapkan kesediaan para alumni UA untuk bekerjasama mengisi kuesioner Tracer Study ini.

			Kuesioner ini ditujukan untuk alumni Program S1 yang lulus mulai tahun 2010.Studi ini bertujuan untuk mengetahui masa transisi dari dunia kampus menuju dunia kerja dan menganalisis tingkat kompetensi yang diperoleh pada saat pendidikan dan aplikasinya di dunia kerja. Studi ini juga akan menggali informasi terkait dengan proses dan kondisi pembelajaran di UA dan akan digunakan untuk memberikan feedback bagi penyempurnaan kurikulum UA.

			Hasil dari studi ini akan menjadi data berharga bagi UA yang akan digunakan sebagai masukan untuk berbagai upaya dan program pengembangan UA, termasuk diantaranya akreditasi dan pemeringkatan internasional. Sebagai informasi, Tracer Study yang dilaksanakan pada Alumni Program S1 yang dilaksanakan pada tahun sebelumnya telah berjalan dengan sukses. Kami berharap kesuksesan yang sama dapat diraih melalui partisipasi para Alumni UA Program S1 tahun 2010. 

			UA dapat menjamin kerahasiaan informasi yang diberikan.Atas perhatian dan partisipasinya disampaikan terima kasih.

			Salam dan doa kami dari Almamater untuk kesuksesan Anda semua para Alumni UA. 
			<br />
			<span style="float:right;padding-top:17px;">Rektor, Prof. Dr. H. Fasich, Apt</span>
		</p>
	</div>
	<div id="backgroundPopup"></div>
	
    <div class="wrapper">
        <div class="login-box">
			<!--
            {if $false_login > 3}
            <div class="recaptcha-box" style="display: none;">    
                {$recaptcha}
            </div>
            {/if}
			-->
			<form action="login.php" method="post">
            <div class="username-box">
                <label>USERNAME</label>
                <input type="text" name="username" />
            </div>
            <div class="password-box">
                <label>PASSWORD</label>
                <input type="password" name="password" />
            </div>
            <div class="submit-box">
                <!-- {if $false_login <= 3}
                    <input type="button" value="Helpdesk!" onclick="setVisible('helpdesk')" />-->
					<input type="submit" value="Login" />
                <!--{else}
                    <button class="show-captcha">Login</button>
                    <input type="submit" value="Login" style="display: none;" />
                <!--{/if}-->
            </div>  
			
			</form>
			
        </div>
    </div>
	<div style="position:absolute;bottom:0;width:100%;margin:auto;text-align:center;">
	<span style="font-size:12px;color:#999;font-weight:500">Powered By Universitas Airlangga Cyber Campus &copy; 2012 Universitas Airlangga</span>
	</div>
</body></html>