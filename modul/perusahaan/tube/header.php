<div class="degree">
    <div class="wrapper">
        <a href="index.php"><img src="img/ocw-logo.png" height="90" width="357" alt="Logo" id="logo" /></a>

        <!-- search -->
        <div class="top-search">
            <form  method="get" id="searchform" action="">
                <div>
                    <input type="text" value="Search..." name="s" id="s" onfocus="defaultInput(this)" onblur="clearInput(this)" />
                    <input type="submit" id="searchsubmit" value=" " />
                </div>
            </form>
        </div>
        <!-- ENDS search -->

        <!-- navigation -->
        <div id="nav-holder">
            <ul id="nav" class="sf-menu">
                <li>
                    <a href="index.php">HOME</a>
                </li>
                <li>
                    <a href="about.php">ABOUT</a>
                </li>
                <li><a href="modul.php">MODUL</a></li>
                <li>
                    <a href="video-gallery.php">VIDEO GALLERY</a>
                </li>
                <li><a href="contact.php">CONTACT</a></li>

            </ul>
        </div>
        <!-- ENDS navigation -->

    </div>
    <!-- ENDS HEADER-wrapper -->
</div>