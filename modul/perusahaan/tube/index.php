<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">  
    <head>
        <?php
        include 'head.php';
        ?>

    </head>	

    <body>

        <!-- HEADER -->
        <div id="home-header">
            <?php
            include 'header.php';
            ?>
        </div>
        <!-- ENDS HEADER -->

        <!-- MAIN -->
        <div id="main">
            <!-- wrapper -->
            <div class="wrapper">
                <!-- home-content -->
                <div class="home-content">
                    <!-- slideshow -->
                    <div id="slideshow">
                        <a href="#" id="prev"></a>
                        <a href="#" id="next"></a>
                        <a href="" id="slideshow-link" ><span></span></a>
                        <!-- slides -->
                        <ul id="slides">
                            <li><a href="#1"><img src="images/1.jpg"  alt="Imagen" /></a></li>
                            <li><a href="#2"><img src="images/2.jpg"  alt="Imagen" /></a></li>
                            <li><a href="#3"><img src="images/3.jpg"  alt="Imagen" /></a></li>
                        </ul>
                        <!-- ENDS slides -->
                    </div>
                    <!-- ENDS slideshow -->
                    <!-- headline -->
                    <div class="headline">
                        SIMPLE THEME IS FREE AND EASY TO USE AND CONFIGURE.
                    </div>
                    <!-- ENDS headline -->
                    <div class="shadow-divider"></div>
                    <!-- front-left-col -->
                    <div class="front-left-col">
                        <div class="bullet-title">
                            <div class="big">RECENT COURSE</div>
                            <div class="small">A LIST OF THE NEWEST COURSE</div>
                        </div>
                        <!-- news list -->
                        <ul class="news-list">
                            <li>
                                <div class="news-title"><a href="#">Lorem Ipsum</a></div>
                                <div class="news-brief">
                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit Vestibulum tortor quam, feugiat vitae, ultricies eget.
                                </div>
                                <div class="news-date">10 May, 2011</div>
                            </li>
                            <li>
                                <div class="news-title"><a href="#">Lorem Ipsum</a></div>
                                <div class="news-brief">
                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit Vestibulum tortor quam, feugiat vitae, ultricies eget.
                                </div>
                                <div class="news-date">10 May, 2011</div>
                            </li>
                            <li>
                                <div class="news-title"><a href="#">Lorem Ipsum</a></div>
                                <div class="news-brief">
                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit Vestibulum tortor quam, feugiat vitae, ultricies eget.
                                </div>
                                <div class="news-date">10 May, 2011</div>
                            </li>
                        </ul>
                        <!-- ENDS news-list -->
                        <p><a href="#" class="link-button right"><span>MORE COURSE</span></a></p>
                    </div>
                    <!-- ENDS front-left-col -->

                    <!-- front-right-col-->
                    <div class="front-right-col">
                        <div class="bullet-title">
                            <div class="big">FAVORIT COURSE</div>
                            <div class="small">SELECT WICH COURSE TO APPEAR IN THE FRONT PAGE</div>
                        </div>
                        <ul class="blocks-holder">
                            <li class="block">
                                <div class="block-ribbon">
                                    <div class="left">
                                        <div class="block-title"><a href="#">LOREM IPSUMA</a></div>
                                        <div class="block-date">10 may, 2011</div>
                                    </div>
                                    <div class="right"></div>
                                </div>
                                <a href="about.html" ><img src="img/dummies/home-block-1.jpg" alt="Thumb" class="thumb" title="Thumbnail" /> </a>
                            </li>
                            <li class="block">
                                <div class="block-ribbon">
                                    <div class="left">
                                        <div class="block-title"><a href="#">A LOREM IPSUM LOREM IPSUM LOREM IPSUM</a></div>
                                        <div class="block-date">10 may, 2011</div>
                                    </div>
                                    <div class="right"></div>
                                </div>
                                <a href="about.html" ><img src="img/dummies/home-block-2.jpg" alt="Thumb" class="thumb" title="Thumbnail" /> </a>
                            </li>
                            <li class="block">
                                <div class="block-ribbon">
                                    <div class="left">
                                        <div class="block-title"><a href="#">A LOREM IPSUM LOREM IPSUM</a></div>
                                        <div class="block-date">10 may, 2011</div>
                                    </div>
                                    <div class="right"></div>
                                </div>
                                <a href="about.html" ><img src="img/dummies/home-block-2.jpg" alt="Thumb" class="thumb" title="Thumbnail" /> </a>
                            </li>
                            <li class="block">
                                <div class="block-ribbon">
                                    <div class="left">
                                        <div class="block-title"><a href="#">LOREM IPSUM</a></div>
                                        <div class="block-date">10 may, 2011</div>
                                    </div>
                                    <div class="right"></div>
                                </div>
                                <a href="about.html" ><img src="img/dummies/home-block-1.jpg" alt="Thumb" class="thumb" title="Thumbnail" /> </a>
                            </li>
                        </ul>
                    </div>
                    <!-- ENDS front-left-col -->
                </div>
                <!-- ENDS home-content -->
                <div class="clear"></div>
                <!-- twitter -->
                <div class="twitter-reader twitter-home">
                    <div id="twitter-holder"></div>
                </div>
                <!-- ENDS twitter -->

            </div>
            <!-- ENDS main-wrapper -->


        </div>		
        <!-- ENDS MAIN -->	

        <!-- FOOTER -->
        <div id="footer">
            <div class="degree">
                <!-- wrapper -->
                <div class="wrapper">
                    <!-- social bar -->
                    <div id="social-bar">
                        <ul class="follow-us">
                            <li><span>FOLLOW US</span></li>
                            <li ><a href="#" class="icon-32 twitter-32 social-tooltip" title="Follow our tweets">link</a></li>
                            <li ><a href="#" class="icon-32 vimeo-32 social-tooltip" title="Lorem ipsum dolor">link</a></li>
                            <li ><a href="#" class="icon-32 dribbble-32 social-tooltip" title="Lorem ipsum dolor">link</a></li>
                            <li ><a href="#" class="icon-32 flickr-32 social-tooltip" title="Lorem ipsum dolor">link</a></li>
                            <li ><a href="#" class="icon-32 facebook-32 social-tooltip" title="Lorem ipsum dolor">link</a></li>
                        </ul>
                    </div>
                    <!-- ENDS social bar -->
                    <!-- footer-cols -->
                    <ul class="footer-cols">
                        <li class="col">
                            <h6>CATEGORIES</h6>
                            <ul>
                                <li><a href="#">WEBDESIGN</a></li>
                                <li><a href="#/">WORDPRESS</a></li>
                                <li><a href="#">PHOTO</a></li>
                                <li><a href="#">CODING</a></li>
                            </ul>
                        </li>
                        <li class="col">
                            <h6>PAGES</h6>
                            <ul>
                                <li><a href="#">HOW TO CUSTOMIZE THIS TEMPLATE</a></li>
                                <li><a href="#">5 ESSENTIALS RULES</a></li>
                                <li><a href="#">LOREM IPSUM DOLOR</a></li>
                            </ul>
                        </li>
                        <li class="col">
                            <h6>FREE TEMPLATES &amp; THEMES</h6>
                            <p>Get tons of free high quality templates at <a href="http://templatecreme.com">Template Creme</a>.</p>
                        </li>	
                        <li class="col">
                            <h6>PAGES</h6>
                            <ul>
                                <li><a href="#">HOW TO CUSTOMIZE THIS TEMPLATE</a></li>
                                <li><a href="#">5 ESSENTIALS RULES</a></li>
                                <li><a href="#">LOREM IPSUM DOLOR</a></li>
                            </ul>
                        </li>		
                    </ul>
                    <!-- ENDS footer-cols -->
                </div>
                <!-- ENDS footer-wrapper -->
            </div>
        </div>
        <!-- ENDS FOOTER -->


        <!-- BOTTOM -->
        <div id="bottom">
            <!-- wrapper -->
            <div class="wrapper">
                SIMPLE THEME by <a href="http://www.luiszuno.com" >luiszuno.com</a>
            </div>
            <!-- ENDS bottom-wrapper -->
        </div>
        <!-- ENDS BOTTOM -->

        <!-- start cufon -->
        <script type="text/javascript"> Cufon.now(); </script>
        <!-- ENDS start cufon -->

    </body>
</html>
