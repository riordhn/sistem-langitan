<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">  
    <head>
        <?php
        include 'head.php';
        ?>

    </head>


    <body>

        <!-- HEADER -->
        <div id="header">
            <?php
            include 'header.php';
            ?>
        </div>
        <!-- ENDS HEADER -->

        <!-- MAIN -->
        <div id="main">
            <!-- wrapper -->
            <div class="wrapper">
                <!-- content -->
                <div class="content">
                    <!-- title -->
                    <div class="title-holder">
                        <span class="title">MODUL</span>
                        <span class="subtitle">Pellentesque habitant morbi tristique senectus et netus et malesuada fames .</span>
                    </div>
                    <!-- ENDS title -->

                    <!-- page-content -->
                    <div class="page-content">


                        <!-- holder -->
                        <div class="holder">
                            <!-- portfolio-sidebar -->
                            <div class="portfolio-sidebar">
                                <h2>CATEGORIES</h2>
                                <ul>
                                    <li><a href="#">LOREM IPSUM</a></li>
                                    <li><a href="#">MORTE VOLDMEORS</a></li>
                                    <li><a href="#">LOREM IPSUM</a></li>
                                    <li><a href="#">MORTE VOLDMEORS</a></li>
                                    <li><a href="#">LOREM IPSUM</a></li>
                                    <li><a href="#">MORTE VOLDMEORS</a></li>
                                    <li><a href="#">LOREM IPSUM</a></li>
                                    <li><a href="#">MORTE VOLDMEORS</a></li>
                                </ul>
                            </div>
                            <!-- ENDS portfolio-sidebar -->
                            <!-- portfolio-content -->
                            <div class="holder">
                                <ul class="portfolio-thumbs">
                                    <li>
                                        <a href="modul-detail.php"><img src="img/dummies/portfolio-thumb.gif" alt="Thumb" /></a>
                                        <div class="name"><a href="modul-detail.php">LOREM IPSUM DOLOR</a></div>
                                        <div class="sub-name">Client name</div>
                                    </li>
                                    <li>
                                        <a href="modul-detail.php"><img src="img/dummies/portfolio-thumb.gif" alt="Thumb" /></a>
                                        <div class="name"><a href="modul-detail.php">LOREM IPSUM DOLOR</a></div>
                                        <div class="sub-name">Client name</div>
                                    </li>

                                    <li>
                                        <a href="modul-detail.php"><img src="img/dummies/portfolio-thumb.gif" alt="Thumb" /></a>
                                        <div class="name"><a href="modul-detail.php">LOREM IPSUM DOLOR</a></div>
                                        <div class="sub-name">Client name</div>
                                    </li>

                                    <li>
                                        <a href="modul-detail.php"><img src="img/dummies/portfolio-thumb.gif" alt="Thumb" /></a>
                                        <div class="name"><a href="modul-detail.php">LOREM IPSUM DOLOR</a></div>
                                        <div class="sub-name">Client name</div>
                                    </li>
                                    <li>
                                        <a href="modul-detail.php"><img src="img/dummies/portfolio-thumb.gif" alt="Thumb" /></a>
                                        <div class="name"><a href="modul-detail.php">LOREM IPSUM DOLOR</a></div>
                                        <div class="sub-name">Client name</div>
                                    </li>
                                    <li>
                                        <a href="modul-detail.php"><img src="img/dummies/portfolio-thumb.gif" alt="Thumb" /></a>
                                        <div class="name"><a href="modul-detail.php">LOREM IPSUM DOLOR</a></div>
                                        <div class="sub-name">Client name</div>
                                    </li>

                                    <li>
                                        <a href="modul-detail.php"><img src="img/dummies/portfolio-thumb.gif" alt="Thumb" /></a>
                                        <div class="name"><a href="modul-detail.php">LOREM IPSUM DOLOR</a></div>
                                        <div class="sub-name">Client name</div>
                                    </li>

                                    <li>
                                        <a href="modul-detail.php"><img src="img/dummies/portfolio-thumb.gif" alt="Thumb" /></a>
                                        <div class="name"><a href="modul-detail.php">LOREM IPSUM DOLOR</a></div>
                                        <div class="sub-name">Client name</div>
                                    </li>
                                    <li>
                                        <a href="modul-detail.php"><img src="img/dummies/portfolio-thumb.gif" alt="Thumb" /></a>
                                        <div class="name"><a href="modul-detail.php">LOREM IPSUM DOLOR</a></div>
                                        <div class="sub-name">Client name</div>
                                    </li>
                                </ul>
                                <!-- portfolio-pager -->
                                <div class="pager">
                                    <p class="clear"></p>
                                    <ul class="portfolio-pager">
                                        <li><a href="#">NEWER</a></li>
                                        <li class="last-child"><a href="#">OLDER</a></li>
                                    </ul>
                                </div>
                                <!-- ENDS portfolio-pager -->
                            </div>
                            <!-- ENDS portfolio-content -->
                        </div>
                        <!-- ENDS holder -->	

                    </div>
                    <!-- ENDS page-content -->

                </div>
                <!-- ENDS content -->

                <!-- twitter -->
                <div class="twitter-reader">
                    <div id="twitter-holder"></div>
                </div>
                <!-- ENDS twitter -->

            </div>
            <!-- ENDS main-wrapper -->


        </div>		
        <!-- ENDS MAIN -->	

        <!-- FOOTER -->
        <div id="footer">
            <?php
            include 'footer.php';
            ?>
        </div>
        <!-- ENDS FOOTER -->


        <!-- BOTTOM -->
        <div id="bottom">
            <!-- wrapper -->
            <div class="wrapper">
                SIMPLE THEME by <a href="http://www.luiszuno.com" >luiszuno.com</a>
            </div>
            <!-- ENDS bottom-wrapper -->
        </div>
        <!-- ENDS BOTTOM -->

        <!-- start cufon -->
        <script type="text/javascript"> Cufon.now(); </script>
        <!-- ENDS start cufon -->

    </body>
</html>
