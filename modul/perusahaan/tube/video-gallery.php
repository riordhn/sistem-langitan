<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">  
    <head>
        <?php
        include 'head.php';
        ?>

    </head>


    <body>

        <!-- HEADER -->
        <div id="header">
            <?php
            include 'header.php';
            ?>
        </div>
        <!-- ENDS HEADER -->

        <!-- MAIN -->
        <div id="main">
            <!-- wrapper -->
            <div class="wrapper">
                <!-- content -->
                <div class="content">
                    <!-- title -->
                    <div class="title-holder">
                        <span class="title">GALLERY</span>
                        <span class="subtitle">Pellentesque habitant morbi tristique senectus et netus et malesuada fames .</span>
                    </div>
                    <!-- ENDS title -->

                    <!-- page-content -->
                    <div class="page-content">


                        <!-- filter -->
                        <ul id="portfolio-filter">
                            <li>FILTER </li>
                            <li><a href="#all" class="link-button"><span>ALL</span></a></li>
                            <li><a href="#filter-autumn" class="link-button"><span>AUTUMN</span></a></li>
                            <li><a href="#filter-winter" class="link-button"><span>WINTER</span></a></li>
                            <li><a href="#filter-city" class="link-button"><span>CITY</span></a></li>
                        </ul>
                        <!-- filter -->



                        <!-- Thumbnails -->
                        <ul id="portfolio-list" class="gallery" >
                            <li class="filter-autumn">
                                <a href="img/dummies/dummy-tool-t.jpg" title="The road" rel="prettyPhoto[gallery]">
                                    <span></span>
                                    <img src="img/dummies/portfolio-thumb.gif" alt="The Road" title="The Road"/></a>
                                <em>The Road</em>
                            </li>
                            <li class="filter-autumn">
                                <a href="img/dummies/dummy-tool-t.jpg" title="The road" rel="prettyPhoto[gallery]">
                                    <span></span>
                                    <img src="img/dummies/portfolio-thumb.gif" alt="The Road" title="The Road"/></a>
                                <em>The Road</em>
                            </li>
                            <li class="filter-winter">
                                <a href="img/dummies/dummy-tool-t.jpg" title="The road" rel="prettyPhoto[gallery]">
                                    <span></span>
                                    <img src="img/dummies/portfolio-thumb.gif" alt="The Road" title="The Road"/></a>
                                <em>The Road</em>
                            </li>
                            <li class="filter-winter">
                                <a href="img/dummies/dummy-tool-t.jpg" title="The road" rel="prettyPhoto[gallery]">
                                    <span></span>
                                    <img src="img/dummies/portfolio-thumb.gif" alt="The Road" title="The Road"/></a>
                                <em>The Road</em>
                            </li>
                            <li class="filter-winter">
                                <a href="img/dummies/dummy-tool-t.jpg" title="The road" rel="prettyPhoto[gallery]">
                                    <span></span>
                                    <img src="img/dummies/portfolio-thumb.gif" alt="The Road" title="The Road"/></a>
                                <em>The Road</em>
                            </li>
                            <li class="filter-winter">
                                <a href="img/dummies/dummy-tool-t.jpg" title="The road" rel="prettyPhoto[gallery]">
                                    <span></span>
                                    <img src="img/dummies/portfolio-thumb.gif" alt="The Road" title="The Road"/></a>
                                <em>The Road</em>
                            </li>
                            <li class="filter-city">
                                <a href="img/dummies/dummy-tool-t.jpg" title="The road" rel="prettyPhoto[gallery]">
                                    <span></span>
                                    <img src="img/dummies/portfolio-thumb.gif" alt="The Road" title="The Road"/></a>
                                <em>The Road</em>
                            </li>
                            <li class="filter-city">
                                <a href="img/dummies/dummy-tool-t.jpg" title="The road" rel="prettyPhoto[gallery]">
                                    <span></span>
                                    <img src="img/dummies/portfolio-thumb.gif" alt="The Road" title="The Road"/></a>
                                <em>The Road</em>
                            </li>
                            <li class="filter-city">
                                <a href="img/dummies/dummy-tool-t.jpg" title="The road" rel="prettyPhoto[gallery]">
                                    <span></span>
                                    <img src="img/dummies/portfolio-thumb.gif" alt="The Road" title="The Road"/></a>
                                <em>The Road</em>
                            </li>
                            <li class="filter-city">
                                <a href="img/dummies/dummy-tool-t.jpg" title="The road" rel="prettyPhoto[gallery]">
                                    <span></span>
                                    <img src="img/dummies/portfolio-thumb.gif" alt="The Road" title="The Road"/></a>
                                <em>The Road</em>
                            </li>
                            <li class="filter-city">
                                <a href="img/dummies/dummy-tool-t.jpg" title="The road" rel="prettyPhoto[gallery]">
                                    <span></span>
                                    <img src="img/dummies/portfolio-thumb.gif" alt="The Road" title="The Road"/></a>
                                <em>The Road</em>
                            </li>
                            <li class="filter-city">
                                <a href="img/dummies/dummy-tool-t.jpg" title="The road" rel="prettyPhoto[gallery]">
                                    <span></span>
                                    <img src="img/dummies/portfolio-thumb.gif" alt="The Road" title="The Road"/></a>
                                <em>The Road</em>
                            </li>
                        </ul>
                        <!-- ENDS Thumbnails -->

                        <!-- Pagination -->
                        <div class="holder">
                            <ul class='gallery-pager'>
                                <li><a href="#">prev</a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li class="last-child"><a href="#">next</a></li>
                            </ul>
                        </div>
                        <!-- ENDS Pagination -->			

                    </div>
                    <!-- ENDS page-content -->

                </div>
                <!-- ENDS content -->

                <!-- twitter -->
                <div class="twitter-reader">
                    <div id="twitter-holder"></div>
                </div>
                <!-- ENDS twitter -->

            </div>
            <!-- ENDS main-wrapper -->


        </div>		
        <!-- ENDS MAIN -->	

        <!-- FOOTER -->
        <div id="footer">
            <?php
            include 'footer.php';
            ?>
        </div>
        <!-- ENDS FOOTER -->


        <!-- BOTTOM -->
        <div id="bottom">
            <!-- wrapper -->
            <div class="wrapper">
                SIMPLE THEME by <a href="http://www.luiszuno.com" >luiszuno.com</a>
            </div>
            <!-- ENDS bottom-wrapper -->
        </div>
        <!-- ENDS BOTTOM -->

        <!-- start cufon -->
        <script type="text/javascript"> Cufon.now(); </script>
        <!-- ENDS start cufon -->

    </body>
</html>
