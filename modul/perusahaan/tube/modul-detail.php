<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">  
    <head>
        <?php
        include 'head.php';
        ?>
    </head>


    <body>

        <!-- HEADER -->
        <div id="header">
            <?php
            include 'header.php';
            ?>
        </div>
        <!-- ENDS HEADER -->

        <!-- MAIN -->
        <div id="main">
            <!-- wrapper -->
            <div class="wrapper">
                <!-- content -->
                <div class="content-blog">

                    <!-- POSTS -->
                    <div id="posts">
                        <!-- post -->
                        <div class="post">
                            <!-- post-header -->
                            <div class="post-header single">
                                <div class="post-title"><a href="single.html" >MODUL SUMBERDAYA MANUSIA </a></div>
                                <div class="post-meta">
                                    POSTED BY <a href="#">ADMIN</a> IN <a href="#">CATEGORY</a>
                                </div>
                            </div>
                            <!-- ENDS post-header -->

                            <!-- post-content -->								
                            <div>
                                <style>
                                    .flowplayer {
                                        width: 100%;
                                        height: auto;
                                    }
                                    #css-poster {
                                        background: #000 0 0 no-repeat;
                                        background-size: 200%;
                                        -webkit-transition: background 1s;
                                        -moz-transition: background 1s;
                                    }

                                    #css-poster.is-poster {
                                        background-size: 100%;
                                    }
                                </style>
                                <div style="width: 100%">
                                    <div style="width: 95%;height: 300px;margin: 20px auto;">
                                        <div id="css-poster" class="flowplayer minimalist is-splash" data-ratio="0.417">
                                            <video>
                                                <source src="http://stream.flowplayer.org/bauhaus/624x260.webm"/>
                                                <!--                                <source src="video/Budi - Do Re Mi.flv"/>-->
                                            </video>
                                        </div>
                                    </div>
                                </div>
                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est.</p>

                                <a href="#" class="link-button"><span>Download E-Book</span></a>
                            </div>
                            <!-- ENDS post-content -->

                            <!-- comments list -->
                            <div class="comments-header"><span class="n-comments">25</span><span class="text">COMMENTS</span></div>
                            <ol class="comments-list">
                                <li>
                                    <div class="comment-wrap">
                                        <img alt='avatar' src='img/dummies/avatar.jpg' class='avatar' />
                                        <div class="comments-right">
                                            <div class="meta-date">May 05, 2010 @ 08:19 pm</div>
                                            <div><a href='#' class='url'><strong>Admin</strong></a></div>
                                            <div class="brief"><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p></div>
                                            <p class="edit-comment"><a href="#">Edit</a></p>
                                        </div>
                                    </div>
                                    <ul class="children">
                                        <li>
                                            <div class="comment-wrap">
                                                <img alt='avatar' src='img/dummies/avatar.jpg' class='avatar' />
                                                <div class="comments-right">
                                                    <div class="meta-date">May 05, 2010 @ 08:19 pm</div>
                                                    <div><a href='#' class='url'><strong>Admin</strong></a></div>
                                                    <div class="brief"><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p></div>
                                                    <p class="edit-comment"><a href="#">Edit</a></p>
                                                </div>
                                            </div>
                                            <ul class="children">
                                                <li>
                                                    <div class="comment-wrap">
                                                        <img alt='avatar' src='img/dummies/avatar.jpg' class='avatar' />
                                                        <div class="comments-right">
                                                            <div class="meta-date">May 05, 2010 @ 08:19 pm</div>
                                                            <div><a href='#' class='url'><strong>Admin</strong></a></div>
                                                            <div class="brief"><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p></div>
                                                            <p class="edit-comment"><a href="#">Edit</a></p>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                            </ol>
                            <!-- ENDS comments list -->


                            <!-- comments form -->
                            <div class="leave-comment">
                                <h3>LEAVE A COMMENT</h3>	
                                <form action="" method="post" id="commentform">
                                    <fieldset>
                                        <p><label>NAME *</label>
                                            <input type="text"  value="" tabindex="1" /></p>
                                        <p><label for="email">EMAIL (Will not be published) *</label>
                                            <input type="text" name="email" id="email" value="" tabindex="2" /></p>
                                        <p><label for="url">WEBSITE</label><input type="text" name="url" id="url" value="" tabindex="3" /></p>
                                        <p><label for="comment">COMMENTS</label>
                                            <textarea name="comment" id="comment" cols="100%" rows="10" tabindex="4"></textarea></p>
                                        <p><input type="submit" name="submit" id="submit" tabindex="5" value="SEND" /></p>
                                        <p><input type="hidden" name="comment_post_ID" value="51" /></p>
                                    </fieldset>
                                </form>
                            </div>
                            <!-- ENDS comments form -->	



                        </div>
                        <!-- ENDS post -->


                    </div>	
                    <!-- ENDS POSTS -->

                    <!-- sidebar -->
                    <ul id="sidebar">
                        <!-- init sidebar -->
                        <li>
                            <h2 class="custom"><span>CATEGORIES</span></h2>		
                            <ul>
                                <li class="cat-item"><a href="#" title="View all posts filed under Design">DESIGN</a></li>
                                <li class="cat-item"><a href="#" title="View all posts filed under Design">WEB</a></li>
                                <li class="cat-item"><a href="#" title="View all posts filed under Design">PHOTO</a></li>
                                <li class="cat-item"><a href="#" title="View all posts filed under Design">CODING</a></li>
                            </ul>
                        </li>	
                        <!-- ENDS init sidebar -->

                        <!-- recent posts -->
                        <li class="recent-posts">
                            <h2 class="custom"><span>RECENT POSTS</span></h2>
                            <ul>
                                <li>
                                    <a href="#" title="EASY TO USE"><img src="img/dummies/dummy-post-48.gif" alt="Thumbnail" title="Thumbnail" /></a>
                                    <div class="recent-excerpt">
                                        <div><a href="#">LOREM IPSUM</a></div>
                                        <div>Pellentesque habitant morbi fames ac turpis egestas...</div>
                                    </div>		   					   		
                                </li>
                                <li>
                                    <a href="#" title="EASY TO USE"><img src="img/dummies/dummy-post-48.gif" alt="Thumbnail" title="Thumbnail" /></a>
                                    <div class="recent-excerpt">
                                        <div><a href="#">LOREM IPSUM</a></div>
                                        <div>Pellentesque habitant morbi fames ac turpis egestas...</div>
                                    </div>		   					   		
                                </li>
                                <li>
                                    <a href="#" title="EASY TO USE"><img src="img/dummies/dummy-post-48.gif" alt="Thumbnail" title="Thumbnail" /></a>
                                    <div class="recent-excerpt">
                                        <div><a href="#">LOREM IPSUM</a></div>
                                        <div>Pellentesque habitant morbi fames ac turpis egestas...</div>
                                    </div>		   					   		
                                </li>				
                            </ul>
                        </li>
                        <!-- ENDS recent posts -->
                    </ul>
                    <!-- ENDS sidebar -->



                </div>
                <!-- ENDS content-blog -->

                <!-- twitter -->
                <div class="twitter-reader">
                    <div id="twitter-holder"></div>
                </div>
                <!-- ENDS twitter -->

            </div>
            <!-- ENDS main-wrapper -->


        </div>		
        <!-- ENDS MAIN -->	

        <!-- FOOTER -->
        <div id="footer">
            <?php
            include 'footer.php';
            ?>
        </div>
        <!-- ENDS FOOTER -->


        <!-- BOTTOM -->
        <div id="bottom">
            <!-- wrapper -->
            <div class="wrapper">
                SIMPLE THEME by <a href="http://www.luiszuno.com" >luiszuno.com</a>
            </div>
            <!-- ENDS bottom-wrapper -->
        </div>
        <!-- ENDS BOTTOM -->

        <!-- start cufon -->
        <script type="text/javascript"> Cufon.now(); </script>
        <!-- ENDS start cufon -->

    </body>
</html>
