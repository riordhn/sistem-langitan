<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>SIMPLE</title>


<!-- CSS -->
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
<!-- player skin -->
<link rel="stylesheet" type="text/css" href="css/skin/minimalist.css" />
<!-- site specific styling -->
<!--[if IE 6]>
        <link rel="stylesheet" type="text/css" media="screen" href="css/ie-hacks.css" />
        <script type="text/javascript" src="js/DD_belatedPNG.js"></script>
        <script>
        /* EXAMPLE */
        DD_belatedPNG.fix('*');
</script>
<![endif]-->
<!--[if IE 7]>
        <link rel="stylesheet" href="css/ie7-hacks.css" type="text/css" media="screen" />
<![endif]-->
<!--[if IE 8]>
        <link rel="stylesheet" href="css/ie8-hacks.css" type="text/css" media="screen" />
<![endif]-->
<!-- ENDS CSS -->

<!-- prettyPhoto -->
<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
<!-- ENDS prettyPhoto -->
<!-- flowplayer depends on jQuery 1.7.1+ (for now) -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<!-- include flowplayer -->
<script type="text/javascript" src="js/flowplayer.min.js"></script>
<!-- JS -->
<script type="text/javascript" src="js/jquery_1.4.2.js"></script>
<script type="text/javascript" src="js/jqueryui.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
<script type="text/javascript" src="js/tooltip/jquery.tools.min.js"></script>
<script type="text/javascript" src="js/filterable.pack.js"></script>
<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="js/jquery.tabs/jquery.tabs.pack.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script>
    // run script after document is ready
    $(function() {
 
        // install flowplayer to an element with CSS class "player"
        $(".flowplayer").flowplayer({ 
            swf: "/js/flowplayer.swf"
        });
 
    });
</script>
<!-- ENDS JS -->

<!-- Cufon -->
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/fonts/bebas-neue_400.font.js" type="text/javascript"></script>
<!-- /Cufon -->

<!-- superfish -->
<link rel="stylesheet" type="text/css" media="screen" href="css/superfish-custom.css" /> 
<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script> 
<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script> 
<!-- ENDS superfish -->

<!-- tabs -->
<link rel="stylesheet" href="css/jquery.tabs.css" type="text/css" media="print, projection, screen" />
<!-- Additional IE/Win specific style sheet (Conditional Comments) -->
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/jquery.tabs-ie.css" type="text/css" media="projection, screen">
<![endif]-->
<!-- ENDS tabs -->