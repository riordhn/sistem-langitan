<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">  
    <head>
        <?php
        include 'head.php';
        ?>

    </head>


    <body>

        <!-- HEADER -->
        <div id="header">
            <?php
            include 'header.php';
            ?>
        </div>
        <!-- ENDS HEADER -->

        <!-- MAIN -->
        <div id="main">
            <!-- wrapper -->
            <div class="wrapper">
                <!-- content -->
                <div class="content">

                    <!-- title -->
                    <div class="title-holder">
                        <span class="title">FEATURES</span>
                        <span class="subtitle">Pellentesque habitant morbi tristique senectus et netus et malesuada fames .</span>
                    </div>
                    <!-- ENDS title -->


                    <!-- page-content -->
                    <div class="page-content">

                        <!-- one col -->
                        <h4>SPECIAL CHARACTERS SUCH</h4>
                        <h6>&aacute; &eacute; &iacute; &oacute; &uacute; &#324; &#323; &Aacute; &Eacute; &Iacute; &Oacute; &Uacute; &eacute; &egrave; &agrave; &ccedil; &ugrave; &ocirc; &ucirc; &icirc; &ecirc; &acirc; &Ouml; &Ucirc; &yuml; &Uuml; &Otilde;</h6>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, are sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
                        <div class="clear "></div>
                        <!-- ENDS one col -->
                        <!-- 2 cols -->
                        <div class="one-half">
                            <h4>TWO COLUMNS </h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugamet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vit, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
                        </div>
                        <div class="one-half last">
                            <h4>TWO COLUMNS </h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugamet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vit, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
                        </div>
                        <div class="clear "></div>
                        <!-- ENDS 2 cols -->
                        <!-- 3 cols -->
                        <div class="one-third">
                            <h4>THREE COLUMNS </h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
                        </div>
                        <div class="one-third">
                            <h4>THREE COLUMNS </h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
                        </div>
                        <div class="one-third last">
                            <h4>THREE COLUMNS </h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
                        </div>
                        <div class="clear "></div>
                        <!-- ENDS 3 cols -->
                        <!-- 2/3 cols -->
                        <div class="one-third">
                            <h4>ONE THIRD COLUMN</h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
                        </div>
                        <div class="two-third last">
                            <h4>TWO THIRD COLUMNS </h4>
                            <p>Pellentesque vit habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat Aenean fermentum, it sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. vit Aenean fermentum, Aenean fermentum, Aenean fermentum, Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
                        </div>
                        <div class="clear "></div>
                        <!-- ENDS 2/3 cols -->
                        <!-- 4 cols -->
                        <div class="one-fourth">
                            <h4>FOUR COLUMNS </h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor qenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis.</p>
                        </div>
                        <div class="one-fourth">
                            <h4>FOUR COLUMNS </h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor qenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis.</p>
                        </div>
                        <div class="one-fourth">
                            <h4>FOUR COLUMNS </h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor qenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis.</p>
                        </div>
                        <div class="one-fourth last">
                            <h4>FOUR COLUMNS </h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor qenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis.</p>
                        </div>
                        <div class="clear "></div>
                        <!-- ENDS 4 cols -->
                        <!-- 1/4-3/4 cols -->
                        <div class="one-fourth">
                            <h4>ONE FOURTH</h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, sit amet, ante. Donec eu libero sit amet quam egestas semper. </p>
                        </div>
                        <div class="three-fourth last">
                            <h4>THREE FOURTH COLUMNS </h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
                        </div>
                        <div class="clear"></div>
                        <!-- ENDS 1/4-3/4 cols -->


                    </div>
                    <!-- ENDS page-content -->

                </div>
                <!-- ENDS content -->

                <!-- twitter -->
                <div class="twitter-reader">
                    <div id="twitter-holder"></div>
                </div>
                <!-- ENDS twitter -->

            </div>
            <!-- ENDS main-wrapper -->


        </div>		
        <!-- ENDS MAIN -->	

        <!-- FOOTER -->
        <div id="footer">
            <?php
            include 'footer.php';
            ?>
        </div>
        <!-- ENDS FOOTER -->


        <!-- BOTTOM -->
        <div id="bottom">
            <!-- wrapper -->
            <div class="wrapper">
                SIMPLE THEME by <a href="http://www.luiszuno.com" >luiszuno.com</a>
            </div>
            <!-- ENDS bottom-wrapper -->
        </div>
        <!-- ENDS BOTTOM -->

        <!-- start cufon -->
        <script type="text/javascript"> Cufon.now(); </script>
        <!-- ENDS start cufon -->

    </body>
</html>
