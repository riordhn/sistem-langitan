<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">  
    <head>
        <?php
        include 'head.php';
        ?>

    </head>


    <body>

        <!-- HEADER -->
        <div id="header">
            <?php
            include 'header.php';
            ?>
        </div>
        <!-- ENDS HEADER -->

        <!-- MAIN -->
        <div id="main">
            <!-- wrapper -->
            <div class="wrapper">
                <!-- content -->
                <div class="content">
                    <!-- title -->
                    <div class="title-holder">
                        <span class="title">CONTACT</span>
                        <span class="subtitle">Pellentesque habitant morbi tristique senectus et netus et malesuada fames .</span>
                    </div>
                    <!-- ENDS title -->

                    <!-- page-content -->
                    <div class="page-content">

                        <!-- 2 cols -->
                        <div class="one-half">
                            <h4>CONTACT FORM</h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugamet, ante. </p>
                            <!-- form -->
                            <script type="text/javascript" src="js/form-validation.js"></script>
                            <form id="contactForm" action="#" method="post">
                                <fieldset>
                                    <p>
                                        <label>NAME:</label>
                                        <input name="name"  id="name" type="text" />
                                    </p>
                                    <p>
                                        <label>EMAIL:</label>
                                        <input name="email"  id="email" type="text" />
                                    </p>
                                    <p>
                                        <label>WEB:</label>
                                        <input name="web"  id="web" type="text" />
                                    </p>
                                    <p>
                                        <label>COMMENTS:</label>
                                        <textarea  name="comments"  id="comments" rows="5" cols="20" ></textarea>
                                    </p>

                                    <!-- send mail configuration -->
                                    <input type="hidden" value="luis@luiszuno.com" name="to" id="to" />
                                    <input type="hidden" value="youremail@luiszuno.com" name="from" id="from" />
                                    <input type="hidden" value="From torn wordpress online" name="subject" id="subject" />
                                    <input type="hidden" value="send-mail.php" name="sendMailUrl" id="sendMailUrl" />
                                    <!-- ENDS send mail configuration -->

                                    <p><input type="button" value="SEND" name="submit" id="submit" /></p>
                                </fieldset>
                                <p id="error" class="warning">Message</p>
                            </form>
                            <p id="success" class="success">Thanks for your comments.</p>
                            <!-- ENDS form -->

                        </div>
                        <div class="one-half last">
                            <h4>LOCATION </h4>

                            <p>Pellentest condimentum, eros ipsum rutrum orci, sagittis tempus.</p>
                            <p><img src="img/dummies/map.jpg" alt="Location" class="shadow-img"/></p>
                            <p>Chambers St 1254 New York City<br/>
                                USA ZIP 44511<br/>
                                (33) 1234 5677, (33) 12459876<br/>
                                <a href="mailto:email@server.com">email@server.com</a></p>
                        </div>
                        <div class="clear "></div>
                        <!-- ENDS 2 cols -->

                    </div>
                    <!-- ENDS page-content -->


                </div>
                <!-- ENDS content -->

                <!-- twitter -->
                <div class="twitter-reader">
                    <div id="twitter-holder"></div>
                </div>
                <!-- ENDS twitter -->

            </div>
            <!-- ENDS main-wrapper -->


        </div>		
        <!-- ENDS MAIN -->	

        <!-- FOOTER -->
        <div id="footer">
            <?php
            include 'footer.php';
            ?>
        </div>
        <!-- ENDS FOOTER -->


        <!-- BOTTOM -->
        <div id="bottom">
            <!-- wrapper -->
            <div class="wrapper">
                SIMPLE THEME by <a href="http://www.luiszuno.com" >luiszuno.com</a>
            </div>
            <!-- ENDS bottom-wrapper -->
        </div>
        <!-- ENDS BOTTOM -->

        <!-- start cufon -->
        <script type="text/javascript"> Cufon.now(); </script>
        <!-- ENDS start cufon -->

    </body>
</html>
