<?php


include '../../../../config.php';
include_once '../../../keuangan/class/BniEncryption.class.php';

// FROM BNI
$client_id = getenv('BNI_CLIENT_ID');
$secret_key = getenv('BNI_SECRET_KEY');

function loggingReturnValue($data, $message)
{
    $logs = date('Y-m-d H:i:s') . PHP_EOL;
    $logs .= "Result data" . PHP_EOL;
    $logs .= json_encode($data) . PHP_EOL;
    $logs .= "Return Message" . PHP_EOL;
    $logs .= $message . PHP_EOL . '-------------------------';
    file_put_contents('log/' . date("Ymd") . '.txt', $logs, FILE_APPEND);
}

// URL utk simulasi pembayaran: http://dev.bni-ecollection.com/

$data = file_get_contents('php://input');
$data_json = json_decode($data, true);
$result = '';

if (!$data_json) {
    // handling orang iseng
    $result =  '{"status":"999","message":"jangan iseng :D"}';
    loggingReturnValue($data, $result);
    echo $result;
} else {
    if ($data_json['client_id'] === $client_id) {
        $data_asli = BniEncryption::decrypt(
            $data_json['data'],
            $client_id,
            $secret_key
        );

        if (!$data_asli) {
            // handling jika waktu server salah/tdk sesuai atau secret key salah
            $result = '{"status":"999","message":"waktu server tidak sesuai NTP atau secret key salah."}';
            loggingReturnValue($data_json, $result);
            echo $result;
        } else {
            // insert data asli ke db
            /* $data_asli = array(
				'trx_id' => '', // silakan gunakan parameter berikut sebagai acuan nomor tagihan
				'virtual_account' => '',
				'customer_name' => '',
				'trx_amount' => '',
				'payment_amount' => '',
				'cumulative_payment_amount' => '',
				'payment_ntb' => '',
				'datetime_payment' => '',
				'datetime_payment_iso8601' => '',
            ); */
            // REPLACE SINGLE QUOTE NAME
            $data_asli['customer_name'] = str_replace("'", "''", $data_asli['customer_name']);
            $payment_ntb_custom = $data_asli['payment_ntb'] . '/' . rand(10, 99);
            try {
                $query_insert_payment = "
                INSERT INTO PEMBAYARAN_VA_BANK (
                    ID_BANK,
                    TRX_ID,
                    VIRTUAL_ACCOUNT,
                    CUSTOMER_NAME,
                    TRX_AMOUNT,
                    PAYMENT_AMOUNT,
                    CUMULATIVE_PAYMENT_AMOUNT,
                    PAYMENT_NTB,
                    PAYMENT_TIME,
                    NOTES,
                    CREATED_ON
                )
                    VALUES
                (
                    23,
                    '{$data_asli['trx_id']}',
                    '{$data_asli['virtual_account']}',
                    '{$data_asli['customer_name']}',
                    '{$data_asli['trx_amount']}',
                    '{$data_asli['payment_amount']}',
                    '{$data_asli['cumulative_payment_amount']}',
                    '{$payment_ntb_custom}',
                    TO_DATE(TO_CHAR('{$data_asli['datetime_payment']}'),'YYYY/MM/DD HH24:MI:SS'),
                    '',
                    CURRENT_TIMESTAMP
                )
                ";
                $db->Query($query_insert_payment);
                $result = '{"status":"000"}';
                $data_asli['query'] = $query_insert_payment;
                loggingReturnValue($data_asli, $result);
                echo $result;
                exit;
            } catch (\Exception $e) {
                $result = '{"status":"999","message":"' . $e->getMessage() . '"}';
                loggingReturnValue($data_asli, $result);
                echo $result;
            }
        }
    }
}
