<?php
require_once ('config.php');
require_once ('ociFunction.php');

if ($user->Role() == AUCC_ROLE_KEPEGAWAIAN)
{
$data_tab = array();

foreach ($xml_menu->tab as $tab)
{
    array_push($data_tab, array(
        'ID'    => $tab->id,
        'TITLE' => $tab->title,
        'MENU'  => $tab->menu,
        'PAGE'  => $tab->page
    ));
}

$smarty->assign('tabs', $data_tab);

$smarty->display('index.tpl');

    // data menu dari $user
    //$smarty->assign('modul_set', $user->MODULs);

    //$smarty->display("index.tpl");
}
else
{
    header("location: /logout.php");
    exit();
}
?>