<?php
//Yudi Sulistya, 23/04/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 
$id_fak=$user->ID_FAKULTAS;

		$fak=getData("select 'FAKULTAS '||upper(fak.nm_fakultas) as fakultas
		from fakultas fak
		where fak.id_fakultas=$id_fak");
		$smarty->assign('FAK', $fak);
		
		$ttl=getvar("select sum(A+B+C+D+E+F) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60 then 1 else 0 end as E,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60 then 1 else 0 end as F
						from pegawai peg
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where peg.id_unit_kerja in (select id_unit_kerja from unit_kerja where id_fakultas=$id_fak)
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select upper(golongan) as golongan, sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(F) as VI, sum(A+B+C+D+E+F) as total
						from
						(
						select case when peg.id_golongan is not null then gol.nm_golongan else '-' end as golongan,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60 then 1 else 0 end as E,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60 then 1 else 0 end as F
						from pegawai peg
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where peg.id_unit_kerja in (select id_unit_kerja from unit_kerja where id_fakultas=$id_fak)
						)
						group by golongan
						order by golongan");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(F) as VI, sum(A+B+C+D+E+F) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60 then 1 else 0 end as E,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60 then 1 else 0 end as F
						from pegawai peg
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where peg.id_unit_kerja in (select id_unit_kerja from unit_kerja where id_fakultas=$id_fak)
						)");
		$smarty->assign('JML', $jml);
		
		$psn=getData("select case when sum(A)=0 then 0 else round((sum(A)/sum(A+B+C+D+E+F)*100),2) end as I,
						case when sum(B)=0 then 0 else round((sum(B)/sum(A+B+C+D+E+F)*100),2) end as II,
						case when sum(C)=0 then 0 else round((sum(C)/sum(A+B+C+D+E+F)*100),2) end as III,
						case when sum(D)=0 then 0 else round((sum(D)/sum(A+B+C+D+E+F)*100),2) end as IV,
						case when sum(E)=0 then 0 else round((sum(E)/sum(A+B+C+D+E+F)*100),2) end as V,
						case when sum(F)=0 then 0 else round((sum(F)/sum(A+B+C+D+E+F)*100),2) end as VI,
						round((sum(A+B+C+D+E+F)/sum(A+B+C+D+E+F)*100),2) as persen
						from 
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60 then 1 else 0 end as E,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60 then 1 else 0 end as F
						from pegawai peg
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where peg.id_unit_kerja in (select id_unit_kerja from unit_kerja where id_fakultas=$id_fak)
						)");
		$smarty->assign('PSN', $psn);

$smarty->display('rekap_karyawan_gol_usia.tpl');

?>