<?php
//Yudi Sulistya, 29/03/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

if ($request_method == 'GET')
{
if (get('action')=='detail')
{
		$id_pgg = get('id');

		$dosen=getData("select dsn.id_pengguna, dsn.id_dosen, dsn.nip_dosen as username, dsn.nidn_dosen, dsn.serdos, upper(dsn.alamat_rumah_dosen) as alamat_rumah_dosen, dsn.tlp_dosen, dsn.mobile_dosen, dsn.status_dosen,
		upper(pgg.nm_pengguna) as nm_pengguna, pgg.gelar_depan, pgg.gelar_belakang, TO_CHAR(pgg.tgl_lahir_pengguna, 'DD-MM-YYYY') as tgl_lahir_pengguna, pgg.email_pengguna,
		pgg.kelamin_pengguna, upper(gol.nm_golongan) as nm_golongan, upper(gol.nm_pangkat) as nm_pangkat, upper(fgs.nm_jabatan_fungsional) as nm_jabatan_fungsional, kt.tipe_dati2||' '||nm_kota as tempat_lahir,
		upper(jab.nm_jabatan_pegawai) as nm_jabatan_pegawai, upper(stp.nm_status_pengguna) as nm_status_pengguna, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_fakultas) as nm_fakultas
		from dosen dsn
		left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
		left join kota kt on kt.id_kota=pgg.id_kota_lahir
		left join program_studi pst on dsn.id_program_studi=pst.id_program_studi
		left join fakultas fak on pst.id_fakultas=fak.id_fakultas
		left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang
		left join golongan gol on gol.id_golongan=dsn.id_golongan
		left join jabatan_fungsional fgs on fgs.id_jabatan_fungsional=dsn.id_jabatan_fungsional
		left join jabatan_pegawai jab on jab.id_jabatan_pegawai=dsn.id_jabatan_pegawai
		left join status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where pgg.id_pengguna=$id_pgg");
		$smarty->assign('DOSEN', $dosen);

		$get_photo=getvar("select username as photo from pengguna where id_pengguna=$id_pgg");
		$filename="../../foto_pegawai/".$get_photo['PHOTO'].".JPG";
		if (file_exists($filename)) {
			$photo = $filename;
		} else {
			$photo = 'includes/images/unknown.png';
		}
		$smarty->assign('PHOTO', $photo);
			
		$gol_dosen=getData("select sg.id_sejarah_golongan, sg.id_golongan, upper(gol.nm_golongan) as nm_golongan, sg.no_sk_sejarah_golongan, sg.asal_sk_sejarah_golongan,
		sg.keterangan_sk_sejarah_golongan, TO_CHAR(sg.tmt_sejarah_golongan, 'DD-MM-YYYY') as tmt_sejarah_golongan, sg.status_akhir
		from sejarah_golongan sg
		left join pengguna pgg on pgg.id_pengguna=sg.id_pengguna
		left join golongan gol on gol.id_golongan=sg.id_golongan
		where pgg.id_pengguna=$id_pgg order by sg.status_akhir desc, sg.tmt_sejarah_golongan desc");
		$smarty->assign('GOL', $gol_dosen);

		$jab_dosen=getData("select sf.id_sejarah_jabatan_fungsional, sf.id_jabatan_fungsional, jab.nm_jabatan_fungsional, sf.no_sk_sej_jab_fungsional, sf.asal_sk_sej_jab_fungsional,
		sf.ket_sk_sej_jab_fungsional, TO_CHAR(sf.tmt_sej_jab_fungsional, 'DD-MM-YYYY') as tmt_sej_jab_fungsional, sf.status_akhir
		from sejarah_jabatan_fungsional sf
		left join pengguna pgg on pgg.id_pengguna=sf.id_pengguna
		left join jabatan_fungsional jab on jab.id_jabatan_fungsional=sf.id_jabatan_fungsional
		where pgg.id_pengguna=$id_pgg order by sf.status_akhir desc, sf.tmt_sej_jab_fungsional desc");
		$smarty->assign('JAB', $jab_dosen);

		$pdd_dosen=getData("select pdd.*, to_date(pdd.tahun_lulus_pendidikan,'YYYY') as tahun_lulus, pda.nama_pendidikan_akhir
		from sejarah_pendidikan pdd
		left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		where pgg.id_pengguna=$id_pgg order by status_akhir desc, tahun_lulus_pendidikan desc");
		$smarty->assign('PEND', $pdd_dosen);

		$pms_dosen=getData("select pms.*
		from dosen_pengmas pms
		left join dosen dsn on dsn.id_dosen=pms.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by thn_dosen_pengmas desc");
		$smarty->assign('PEMS', $pms_dosen);

		$phg_dosen=getData("select phg.*
		from dosen_penghargaan phg
		left join dosen dsn on dsn.id_dosen=phg.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by thn_dosen_penghargaan desc");
		$smarty->assign('PEHG', $phg_dosen);

		$pbk_dosen=getData("select pbk.*
		from dosen_publikasi pbk
		left join dosen dsn on dsn.id_dosen=pbk.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by thn_dosen_publikasi desc");
		$smarty->assign('PEBK', $pbk_dosen);

		$org_dosen=getData("select org.*
		from dosen_organisasi org
		left join dosen dsn on dsn.id_dosen=org.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by mulai_dosen_org desc, selesai_dosen_org desc");
		$smarty->assign('PORG', $org_dosen);

		$trg_dosen=getData("select trg.*
		from dosen_training trg
		left join dosen dsn on dsn.id_dosen=trg.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by thn_dos_training desc");
		$smarty->assign('PTRG', $trg_dosen);

		$pro_dosen=getData("select pro.*
		from dosen_profesional pro
		left join dosen dsn on dsn.id_dosen=pro.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by thn_dos_prof desc");
		$smarty->assign('PROF', $pro_dosen);

		$rwt_dosen=getData("select rwt.nm_dos_riwayat, rwt.jenis_dos_riwayat,
		TO_CHAR(rwt.mulai_dos_riwayat, 'DD-MM-YYYY') as mulai_dos_riwayat,
		TO_CHAR(rwt.selesai_dos_riwayat, 'DD-MM-YYYY') as selesai_dos_riwayat
		from dosen_riwayat rwt
		left join dosen dsn on dsn.id_dosen=rwt.id_dosen
		left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where pgg.id_pengguna=$id_pgg order by mulai_dos_riwayat desc, selesai_dos_riwayat desc");
		$smarty->assign('RWYT', $rwt_dosen);
		
		$id_gol=getData("select id_golongan, upper(nm_golongan) as nm_golongan from golongan order by nm_golongan desc");
		$smarty->assign('ID_GOL', $id_gol);
		
		$id_fsg=getData("select id_jabatan_fungsional, nm_jabatan_fungsional from jabatan_fungsional order by id_jabatan_fungsional");
		$smarty->assign('ID_FSG', $id_fsg);

		$id_pdd=getData("select urut, id_pendidikan_akhir, nama_pendidikan_akhir
		from (
		select id_pendidikan_akhir, nama_pendidikan_akhir,
		case when id_pendidikan_akhir=2 then 11 
		when id_pendidikan_akhir=1 then 12
		when id_pendidikan_akhir=10 then 13  
		when id_pendidikan_akhir=9 then 10 
		when id_pendidikan_akhir=8 then 9
		when id_pendidikan_akhir=7 then 8   
		when id_pendidikan_akhir=3 then 7  
		when id_pendidikan_akhir=4 then 6 
		when id_pendidikan_akhir=5 then 5 
		when id_pendidikan_akhir=6 then 4 
		when id_pendidikan_akhir=13 then 3 
		when id_pendidikan_akhir=12 then 2 
		when id_pendidikan_akhir=11 then 1 
		else 0 end as urut
		from pendidikan_akhir)
		order by urut desc");
		$smarty->assign('ID_PDD', $id_pdd);
}
}

if ($request_method == 'POST')
{
if (post('action') == 'update_status_pdd')
{
	$id_pendidikan = post('id_pendidikan');
	
		$id_pengguna=getvar("select distinct id_pengguna from sejarah_pendidikan where id_sejarah_pendidikan=$id_pendidikan");
		UpdateData("update sejarah_pendidikan set status_akhir=0 where id_pengguna='$id_pengguna[ID_PENGGUNA]'");
		UpdateData("update sejarah_pendidikan set status_akhir=1 where id_sejarah_pendidikan=$id_pendidikan");
		
		echo "1";
		exit();
}

if (post('action') == 'delete_pdd')
{
	$id_pendidikan = post('hapus');
	
		deleteData("delete from sejarah_pendidikan where id_sejarah_pendidikan=$id_pendidikan");
		
		echo "1";
		exit();
}

if (post('action') == 'edit_pdd')
{
	$id_sej = post('id_sej');
	$id_pdd = post('id_pdd');
	$nm_skolah = post('nm_skolah');
	$jur_skolah = post('jur_skolah');
	$tpt_skolah = post('tpt_skolah');
	$lls_skolah = post('pddYear');
	
		UpdateData("update sejarah_pendidikan set id_pendidikan_akhir=$id_pdd, nm_sekolah_pendidikan=trim(upper('".$nm_skolah."')), nm_jurusan_pendidikan=trim(upper('".$jur_skolah."')), 
		tempat_pendidikan=trim(upper('".$nm_skolah."')), tahun_lulus_pendidikan='".$lls_skolah."'
		where id_sejarah_pendidikan=$id_sej");

		echo '<script>location.href="javascript:history.go(-1)";</script>';
		exit();
}

if (post('action') == 'update_status_gol')
{
	$id_golongan = post('id_golongan');
	
		$id_pengguna=getvar("select distinct id_pengguna from sejarah_golongan where id_sejarah_golongan=$id_golongan");
		UpdateData("update sejarah_golongan set status_akhir=0 where id_pengguna='$id_pengguna[ID_PENGGUNA]'");
		UpdateData("update sejarah_golongan set status_akhir=1 where id_sejarah_golongan=$id_golongan");
		
		$golongan=getvar("select id_golongan from sejarah_golongan where id_sejarah_golongan=$id_golongan");
		UpdateData("update dosen set id_golongan='$golongan[ID_GOLONGAN]' where id_pengguna='$id_pengguna[ID_PENGGUNA]'");
		
		echo "1";
		exit();
}

if (post('action') == 'delete_gol')
{
	$id_golongan = post('hapus');
	
		deleteData("delete from sejarah_golongan where id_sejarah_golongan=$id_golongan");
		
		echo "1";
		exit();
}

if (post('action') == 'edit_gol')
{
	$id_sej = post('id_sej');
	$id_gol = post('id_gol');
	$no_sk = post('no_sk');
	$asal_sk = post('asal_sk');
	$ket_sk = post('ket_sk');
	$tmt_sej = post('tmt_sej');
	
		UpdateData("update sejarah_golongan set id_golongan=$id_gol, no_sk_sejarah_golongan=trim(upper('".$no_sk."')), asal_sk_sejarah_golongan=trim(upper('".$asal_sk."')), 
		keterangan_sk_sejarah_golongan=trim(upper('".$ket_sk."')), tmt_sejarah_golongan=to_date('".$tmt_sej."','dd-mm-yyyy')
		where id_sejarah_golongan=$id_sej");

		echo '<script>location.href="javascript:history.go(-1)";</script>';
		exit();
}

if (post('action') == 'update_status_fsg')
{
	$id_fungsional = post('id_fungsional');
	
		$id_pengguna=getvar("select distinct id_pengguna from sejarah_jabatan_fungsional where id_sejarah_jabatan_fungsional=$id_fungsional");
		UpdateData("update sejarah_jabatan_fungsional set status_akhir=0 where id_pengguna='$id_pengguna[ID_PENGGUNA]'");
		UpdateData("update sejarah_jabatan_fungsional set status_akhir=1 where id_sejarah_jabatan_fungsional=$id_fungsional");
		
		$fungsional=getvar("select id_jabatan_fungsional from sejarah_jabatan_fungsional where id_sejarah_jabatan_fungsional=$id_fungsional");
		UpdateData("update dosen set id_jabatan_fungsional='$fungsional[ID_JABATAN_FUNGSIONAL]' where id_pengguna='$id_pengguna[ID_PENGGUNA]'");
		
		echo "1";
		exit();
}

if (post('action') == 'delete_fsg')
{
	$id_fungsional = post('hapus');
	
		deleteData("delete from sejarah_jabatan_fungsional where id_sejarah_jabatan_fungsional=$id_fungsional");
		
		echo "1";
		exit();
}

if (post('action') == 'edit_fsg')
{
	$id_sej = post('id_sej');
	$id_fsg = post('id_fsg');
	$no_sk = post('no_sk');
	$asal_sk = post('asal_sk');
	$ket_sk = post('ket_sk');
	$tmt_sej = post('tmt_sej');
	
		UpdateData("update sejarah_jabatan_fungsional set id_jabatan_fungsional=$id_fsg, no_sk_sej_jab_fungsional=trim(upper('".$no_sk."')), asal_sk_sej_jab_fungsional=trim(upper('".$asal_sk."')), 
		ket_sk_sej_jab_fungsional=trim(upper('".$ket_sk."')), tmt_sej_jab_fungsional=to_date('".$tmt_sej."','dd-mm-yyyy')
		where id_sejarah_jabatan_fungsional=$id_sej");

		echo '<script>location.href="javascript:history.go(-1)";</script>';
		exit();
}
}

$smarty->display('dosen_detail.tpl');

?>