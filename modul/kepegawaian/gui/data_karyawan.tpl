<div class="center_title_bar"> Data Karyawan </div>
<table width="850" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="38" bgcolor="#FFCC33"><div align="center">No</div></td>
    <td width="178" bgcolor="#FFCC33"><div align="center">Sub menu</div></td>
    <td width="634" bgcolor="#FFCC33"><div align="center">Uraian</div></td>
  </tr>
  <tr>
    <td><center>1</center></td>
    <td>Karyawan Tetap</td>
    <td>Difungsikan untuk menampilkan data Karyawan tetap di Fakultas</td>
  </tr>
  <tr>
    <td><center>2</center></td>
    <td>Karyawan Kontrak</td>
    <td>Difungsikan untuk menampilkan data Karyawan kontrak di Fakultas</td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Karyawan No Status</td>
    <td>Difungsikan untuk menampilkan data Karyawan yang belum diset status kepegawaiannya di Fakultas</td>
  </tr>
  <tr>
    <td><center>4</center></td>
    <td>Cari Karyawan</td>
    <td>Difungsikan untuk mencari data Karyawan</td>
  </tr>
</table>