<div class="center_title_bar">Pencarian Data Karyawan</div>
<form action="cari_karyawan.php" method="get">
<p>Nama Karyawan :  <input name="cari" type="text" style="width:300px;" /> <input type="submit" name="Cari_Karyawan" value="Cari Karyawan"></p>
</form>
{if $smarty.get.cari == ''}
{else}
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[2,0]],
		headers: {
            5: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th>Photo Kayawan</td>
			<th>NIP/NIK</td>
			<th>Nama Karyawan</td>
			<th>Unit Kerja</td>
			<th>Status</td>
			<th class="noheader">Aksi</td>
		</tr>
		</thead>
		<tbody>
			{foreach name=test item="list" from=$KARYAWAN}
		<tr>
			<td><img src="{$list.FOTO_PENGGUNA}/{$list.NIP_PEGAWAI}.JPG" width="160px"/></td>
			<td>{$list.NIP_PEGAWAI}</td>
			<td>{$list.NM_PENGGUNA}</td>
			<td>{$list.NM_UNIT_KERJA}</td>
			<td>{$list.NM_STATUS_PENGGUNA}</td>
			<td><center><a href="karyawan_detail.php?action=detail&id={$list.ID_PENGGUNA}">Detail</a></center></td>
		</tr>
			{/foreach}
		</tbody>
	</table>
{/if}