{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            2: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="center_title_bar">Cek Data Karyawan yang Belum Memiliki Homebase Fakultas dan Unit Kerja</div>
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th><center>NIP/NIK</center></th>
			<th><center>Nama Karyawan</center></th>
			<th class="noheader"><center>Aksi</center></th>
		</tr>
		</thead>
		<tbody>
			{foreach item="peg" from=$PEGAWAI}
		<tr>
			<td>{$peg.USERNAME}</td>
			<td>{$peg.NM_PENGGUNA}</td>
			<td><center><a href="karyawan_detail.php?action=detail&id={$peg.ID_PENGGUNA}">Detail</a></center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
		</tbody>
	</table>