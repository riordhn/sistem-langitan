{literal}
<script language="javascript" type="text/javascript">
	var popupWindow=null;
	function popup(mypage,myname,w,h,pos,infocus){
	if (pos == "random")
	{LeftPosition=(screen.width)?Math.floor(Math.random()*(screen.width-w)):100;TopPosition=(screen.height)?Math.floor(Math.random()*((screen.height-h)-75)):100;}
	else
	{LeftPosition=(screen.width)?(screen.width-w)/2:100;TopPosition=(screen.height)?(screen.height-h)/2:100;}
	settings="width="+ w + ",height="+ h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";popupWindow=window.open("",myname,settings);
	if(infocus=="front"){popupWindow.focus();popupWindow.location=mypage;}
	if(infocus=="back"){popupWindow.blur();popupWindow.location=mypage;popupWindow.blur();}
	}
</script>
{/literal}

<div class="center_title_bar">Detail Data Dosen</div>
{* biodata dosen *}
<p align="right">
<input type="button" name="kembali" value="Kembali" onclick="javascript:history.go(-1);">
</p>
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="4" width="850"><center><strong>BIODATA</strong></center></th></tr>
	{foreach item="dsn" from=$DOSEN}
	<tr>
		<td>NIP/NIK</td>
		<td><center>:</center></td>
		<td>{$dsn.USERNAME}</td>
		{if $dsn.NIDN_DOSEN != null || $dsn.NIDN_DOSEN != 0}
		<td rowspan="10"><center><img src="{$dsn.FOTO_PENGGUNA}/{$dsn.USERNAME}.JPG" border="0" width="160" /><br><br><input type="button" name="cek_nidn" value="Cek Data DIKTI" onclick="javascript:popup('http://www.evaluasi.dikti.go.id/epsbed/datadosen/{$dsn.NIDN_DOSEN}','name','960','435','center','front');"></center></td>
		{else}
		<td rowspan="10"><center><img src="{$dsn.FOTO_PENGGUNA}/{$dsn.USERNAME}.JPG" border="0" width="160" /></center></td>
		{/if}
	</tr>
	<tr>
		<td>NIDN</td>
		<td><center>:</center></td>
		{if $dsn.NIDN_DOSEN != null || $dsn.NIDN_DOSEN != 0}
		<td>{$dsn.NIDN_DOSEN}</td>
		{else}
		<td></td>
		{/if}
	</tr>
	<tr>
		<td>SERDOS</td>
		<td><center>:</center></td>
		<td>{$dsn.SERDOS}</td>
	</tr>
	<tr>
		<td>Program Studi</td>
		<td><center>:</center></td>
		<td>{$dsn.NM_PROGRAM_STUDI}</td>
	</tr>
	<tr>
		<td>Fakultas</td>
		<td><center>:</center></td>
		<td>{$dsn.NM_FAKULTAS}</td>
	</tr>
	<tr>
		<td>Status Kepegawaian</td>
		<td><center>:</center></td>
		<td>{$dsn.STATUS_DOSEN} ( {$dsn.NM_GOLONGAN} - {$dsn.NM_PANGKAT} )</td>
	</tr>
	<tr>
		<td>Jabatan Fungsional</td>
		<td><center>:</center></td>
		<td>{$dsn.NM_JABATAN_FUNGSIONAL}</td>
	</tr>
	<tr>
		<td>Jabatan Struktural</td>
		<td><center>:</center></td>
		<td>{$dsn.NM_JABATAN_PEGAWAI}</td>
	</tr>
	<tr>
		<td>Status Aktif</td>
		<td><center>:</center></td>
		<td>{$dsn.NM_STATUS_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Gelar Depan</td>
		<td><center>:</center></td>
		<td>{$dsn.GELAR_DEPAN}</td>
	</tr>
	<tr>
		<td>Gelar Belakang</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.GELAR_BELAKANG}</td>
	</tr>
	<tr>
		<td>Nama Lengkap</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.NM_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Tempat, Tanggal Lahir</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.TEMPAT_LAHIR}, {$dsn.TGL_LAHIR_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Jenis Kelamin</td>
		<td><center>:</center></td>
		{if $dsn.KELAMIN_PENGGUNA=='1'}
		<td colspan="2">LAKI-LAKI</td>
		{elseif $dsn.KELAMIN_PENGGUNA=='2'}
		<td colspan="2">PEREMPUAN</td>
		{else}
		<td>-</td>
		{/if}
	</tr>
	<tr>
		<td>Alamat</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.ALAMAT_RUMAH_DOSEN}</td>
	</tr>
	<tr>
		<td>Telepon/HP</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.TLP_DOSEN} / {$dsn.MOBILE_DOSEN}</td>
	</tr>
	<tr>
		<td>Email</td>
		<td><center>:</center></td>
		<td colspan="2">{$dsn.EMAIL_PENGGUNA}</td>
	</tr>
	{/foreach}
</table>

{* riwayat dosen *}
{*
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="3" width="850"><center><strong>RIWAYAT</strong></center></th></tr>
	<tr>
		<th><center><strong>Nama</strong></center></th>
		<th><center><strong>Jenis</strong></center></th>
		<th><center><strong>Periode</strong></center></th>
	</tr>
	{foreach item="rwt" from=$RWYT}
	<tr>
		<td>{$rwt.NM_DOS_RIWAYAT}</td>
		<td>{$rwt.JENIS_DOS_RIWAYAT}</td>
		<td><center>{$rwt.MULAI_DOS_RIWAYAT} s/d {$rwt.SELESAI_DOS_RIWAYAT}</center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>
*}

{* riwayat golongan *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="5" width="850"><center><strong>RIWAYAT GOLONGAN</strong></center></th></tr>
	<tr>
		<th><center><strong>Golongan</strong></center></th>
		<th><center><strong>Surat Keputusan</strong></center></th>
		<th><center><strong>Asal SK</strong></center></th>
		<th><center><strong>Keterangan</strong></center></th>
		<th><center><strong>TMT</strong></center></th>
	</tr>
	{foreach item="gol" from=$GOL}
	<tr>
		<td><center>{$gol.NM_GOLONGAN}</center></td>
		<td>{$gol.NO_SK_SEJARAH_GOLONGAN}</td>
		<td>{$gol.ASAL_SK_SEJARAH_GOLONGAN}</td>
		<td>{$gol.KETERANGAN_SK_SEJARAH_GOLONGAN}</td>
		<td><center>{$gol.TMT_SEJARAH_GOLONGAN}</center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>

{* riwayat jabatan fungsionl *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="5" width="850"><center><strong>RIWAYAT JABATAN FUNGSIONAL</strong></center></th></tr>
	<tr>
		<th><center><strong>Fungsional</strong></center></th>
		<th><center><strong>Surat Keputusan</strong></center></th>
		<th><center><strong>Asal SK</strong></center></th>
		<th><center><strong>Keterangan</strong></center></th>
		<th><center><strong>TMT</strong></center></th>
	</tr>
	{foreach item="jab" from=$JAB}
	<tr>
		<td>{$jab.NM_JABATAN_FUNGSIONAL}</td>
		<td>{$jab.NO_SK_SEJ_JAB_FUNGSIONAL}</td>
		<td>{$jab.ASAL_SK_SEJ_JAB_FUNGSIONAL}</td>
		<td>{$jab.KET_SK_SEJ_JAB_FUNGSIONAL}</td>
		<td><center>{$jab.TMT_SEJ_JAB_FUNGSIONAL}</center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>

{* riwayat pendidikan *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="5" width="850"><center><strong>PENDIDIKAN</strong></center></th></tr>
	<tr>
		<th><center><strong>Jenjang</strong></center></th>
		<th><center><strong>Nama</strong></center></th>
		<th><center><strong>Jurusan</strong></center></th>
		<th><center><strong>Tempat</strong></center></th>
		<th><center><strong>Tahun Lulus</strong></center></th>
	</tr>
	{foreach item="pdd" from=$PEND}
	<tr>
		<td>{$pdd.NAMA_PENDIDIKAN_AKHIR}</td>
		<td>{$pdd.NM_SEKOLAH_PENDIDIKAN}</td>
		<td>{$pdd.NM_JURUSAN_PENDIDIKAN}</td>
		<td>{$pdd.TEMPAT_PENDIDIKAN}</td>
		<td><center>{$pdd.TAHUN_LULUS_PENDIDIKAN}</center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>

{* riwayat pengmas *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="5" width="850"><center><strong>PENGABDIAN MASYARAKAT</strong></center></th></tr>
	<tr>
		<th><center><strong>Nama</strong></center></th>
		<th><center><strong>Bidang</strong></center></th>
		<th><center><strong>Tempat</strong></center></th>
		<th><center><strong>Peran</strong></center></th>
		<th><center><strong>Tahun</strong></center></th>
	</tr>
	{foreach item="pms" from=$PEMS}
	<tr>
		<td>{$pms.NM_DOSEN_PENGMAS}</td>
		<td>{$pms.BIDANG_DOSEN_PENGMAS}</td>
		<td>{$pms.TEMPAT_DOSEN_PENGMAS}</td>
		<td>{$pms.PERAN_DOSEN_PENGMAS}</td>
		<td><center>{$pms.THN_DOSEN_PENGMAS}</center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>

{* riwayat penghargaan *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="4" width="850"><center><strong>PENGHARGAAN</strong></center></th></tr>
	<tr>
		<th><center><strong>Bidang</strong></center></th>
		<th><center><strong>Bentuk</strong></center></th>
		<th><center><strong>Pemberi</strong></center></th>
		<th><center><strong>Tahun</strong></center></th>
	</tr>
	{foreach item="phg" from=$PEHG}
	<tr>
		<td>{$phg.BIDANG_DOSEN_PENGHARGAAN}</td>
		<td>{$phg.BENTUK_DOSEN_PENGHARGAAN}</td>
		<td>{$phg.PEMBERI_DOSEN_PENGHARGAAN}</td>
		<td><center>{$phg.THN_DOSEN_PENGHARGAAN}</center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>

{* riwayat publikasi *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="6" width="850"><center><strong>PUBLIKASI</strong></center></th></tr>
	<tr>
		<th><center><strong>Jenis</strong></center></th>
		<th><center><strong>Judul</strong></center></th>
		<th><center><strong>Media</strong></center></th>
		<th><center><strong>Pengarang</strong></center></th>
		<th><center><strong>Penerbit</strong></center></th>
		<th><center><strong>Tahun</strong></center></th>
	</tr>
	{foreach item="pbk" from=$PEBK}
	<tr>
		<td>{$pbk.JENIS_DOSEN_PUBLIKASI}</td>
		<td>{$pbk.JUDUL_DOSEN_PUBLIKASI}</td>
		<td>{$pbk.MEDIA_DOSEN_PUBLIKASI}</td>
		<td>{$pbk.PENGARANG_DOSEN_PUBLIKASI}</td>
		<td>{$pbk.PENERBIT_DOSEN_PUBLIKASI}</td>
		<td><center>{$pbk.THN_DOSEN_PUBLIKASI}</center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>

{* riwayat organisasi *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="4" width="850"><center><strong>ORGANISASI</strong></center></th></tr>
	<tr>
		<th><center><strong>Nama</strong></center></th>
		<th><center><strong>Jabatan</strong></center></th>
		<th><center><strong>Mulai</strong></center></th>
		<th><center><strong>Sampai</strong></center></th>
	</tr>
	{foreach item="org" from=$PORG}
	<tr>
		<td>{$org.NM_DOSEN_ORG}</td>
		<td>{$org.JABATAN_DOSEN_ORG}</td>
		<td><center>{$org.MULAI_DOSEN_ORG}</center></td>
		<td><center>{$org.SELESAI_DOSEN_ORG}</center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>

{* riwayat training *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="4" width="850"><center><strong>TRAINING/WORKSHOP/SEMINAR</strong></center></th></tr>
	<tr>
		<th><center><strong>Jenis</strong></center></th>
		<th><center><strong>Nama</strong></center></th>
		<th><center><strong>Instansi</strong></center></th>
		<th><center><strong>Tahun</strong></center></th>
	</tr>
	{foreach item="trg" from=$PTRG}
	<tr>
		<td>{$trg.JENIS_DOS_TRAINING}</td>
		<td>{$trg.NM_DOS_TRAINING}</td>
		<td>{$trg.INSTANSI_DOS_TRAINING}</td>
		<td><center>{$trg.THN_DOS_TRAINING}</center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>

{* riwayat kegiatan profesional *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="4" width="850"><center><strong>KEGIATAN PROFESIONAL</strong></center></th></tr>
	<tr>
		<th><center><strong>Nama Kegiatan</strong></center></th>
		<th><center><strong>Instansi</strong></center></th>
		<th><center><strong>Peran</strong></center></th>
		<th><center><strong>Tahun</strong></center></th>
	</tr>
	{foreach item="pro" from=$PROF}
	<tr>
		<td>{$pro.KEGIATAN_DOS_PROF}</td>
		<td>{$pro.INSTANSI_DOS_PROF}</td>
		<td>{$pro.PERAN_DOS_PROF}</td>
		<td><center>{$pro.THN_DOS_PROF}</center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>

<p align="center">
<input type="button" name="kembali" value="Ke atas" onclick="window.scrollTo(0,0); return false">
</p>