{literal}
<script language="javascript" type="text/javascript">
	var popupWindow=null;
	function popup(mypage,myname,w,h,pos,infocus){

	if (pos == "random")
	{LeftPosition=(screen.width)?Math.floor(Math.random()*(screen.width-w)):100;TopPosition=(screen.height)?Math.floor(Math.random()*((screen.height-h)-75)):100;}
	else
	{LeftPosition=(screen.width)?(screen.width-w)/2:100;TopPosition=(screen.height)?(screen.height-h)/2:100;}
	settings="width="+ w + ",height="+ h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";popupWindow=window.open("",myname,settings);
	if(infocus=="front"){popupWindow.focus();popupWindow.location=mypage;}
	if(infocus=="back"){popupWindow.blur();popupWindow.location=mypage;popupWindow.blur();}
	}
</script>
{/literal}

<div class="center_title_bar">Detail Data Karyawan</div>
{* biodata karwayan *}
<p align="right">
<input type="button" name="kembali" value="Kembali" onclick="javascript:history.go(-1);">
</p>
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="4" width="850"><center><strong>BIODATA</strong></center></th></tr>
	{foreach item="peg" from=$PEGAWAI}
	<tr>
		<td>NIP/NIK</td>
		<td><center>:</center></td>
		<td>{$peg.USERNAME}</td>
		<td rowspan="10"><center><img src="{$peg.FOTO_PENGGUNA}/{$peg.USERNAME}.JPG" border="0" width="160" /></center></td>
	</tr>
	<tr>
		<td>Unit Kerja</td>
		<td><center>:</center></td>
		<td>{$peg.NM_UNIT_KERJA}</td>
	</tr>
	<tr>
		<td>Status Kepegawaian</td>
		<td><center>:</center></td>
		<td>{$peg.STATUS_PEGAWAI} ( {$peg.NM_GOLONGAN} - {$peg.NM_PANGKAT} )</td>
	</tr>
	<tr>
		<td>Jabatan Struktural</td>
		<td><center>:</center></td>
		<td>{$peg.NM_JABATAN_PEGAWAI}</td>
	</tr>
	<tr>
		<td>Status Aktif</td>
		<td><center>:</center></td>
		<td></td>
	</tr>
	<tr>
		<td>Gelar Depan</td>
		<td><center>:</center></td>
		<td>{$peg.GELAR_DEPAN}</td>
	</tr>
	<tr>
		<td>Gelar Belakang</td>
		<td><center>:</center></td>
		<td>{$peg.GELAR_BELAKANG}</td>
	</tr>
	<tr>
		<td>Nama Lengkap</td>
		<td><center>:</center></td>
		<td>{$peg.NM_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Tempat, Tanggal Lahir</td>
		<td><center>:</center></td>
		<td>{$peg.TEMPAT_LAHIR}, {$peg.TGL_LAHIR_PENGGUNA}</td>
	</tr>
	<tr>
		<td>Jenis Kelamin</td>
		<td><center>:</center></td>
		{if $peg.KELAMIN_PENGGUNA=='1'}
		<td>LAKI-LAKI</td>
		{elseif $peg.KELAMIN_PENGGUNA=='2'}
		<td>PEREMPUAN</td>
		{else}
		<td>-</td>
		{/if}
	</tr>
	<tr>
		<td>Alamat</td>
		<td><center>:</center></td>
		<td colspan="2">{$peg.ALAMAT_RUMAH_PEGAWAI}</td>
	</tr>
	<tr>
		<td>Telepon/HP</td>
		<td><center>:</center></td>
		<td colspan="2">{$peg.TELP_PEGAWAI}</td>
	</tr>
	<tr>
		<td>Email</td>
		<td><center>:</center></td>
		<td colspan="2">{$peg.EMAIL_PENGGUNA}</td>
	</tr>
	{/foreach}
</table>

{* riwayat golongan *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="5" width="850"><center><strong>RIWAYAT GOLONGAN</strong></center></th></tr>
	<tr>
		<th><center><strong>Golongan</strong></center></th>
		<th><center><strong>Surat Keputusan</strong></center></th>
		<th><center><strong>Asal SK</strong></center></th>
		<th><center><strong>Keterangan</strong></center></th>
		<th><center><strong>TMT</strong></center></th>
	</tr>
	{foreach item="gol" from=$GOL}
	<tr>
		<td><center>{$gol.NM_GOLONGAN}</center></td>
		<td>{$gol.NO_SK_SEJARAH_GOLONGAN}</td>
		<td>{$gol.ASAL_SK_SEJARAH_GOLONGAN}</td>
		<td>{$gol.KETERANGAN_SK_SEJARAH_GOLONGAN}</td>
		<td><center>{$gol.TMT_SEJARAH_GOLONGAN}</center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>

{* riwayat pendidikan *}
<table class="tablesorter" cellspacing="0" cellpadding="0" border="0">
	<tr><th colspan="5" width="850"><center><strong>PENDIDIKAN</strong></center></th></tr>
	<tr>
		<th><center><strong>Jenjang</strong></center></th>
		<th><center><strong>Nama</strong></center></th>
		<th><center><strong>Jurusan</strong></center></th>
		<th><center><strong>Tempat</strong></center></th>
		<th><center><strong>Tahun Lulus</strong></center></th>
	</tr>
	{foreach item="pdd" from=$PEND}
	<tr>
		<td>{$pdd.NAMA_PENDIDIKAN_AKHIR}</td>
		<td>{$pdd.NM_SEKOLAH_PENDIDIKAN}</td>
		<td>{$pdd.NM_JURUSAN_PENDIDIKAN}</td>
		<td>{$pdd.TEMPAT_PENDIDIKAN}</td>
		<td><center>{$pdd.TAHUN_LULUS_PENDIDIKAN}</center></td>
	</tr>
	{foreachelse}
        <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>

<p align="center">
<input type="button" name="kembali" value="Ke atas" onclick="window.scrollTo(0,0); return false">
</p>