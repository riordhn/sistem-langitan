{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            2: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="center_title_bar">Cek Data Dosen yang Belum Memiliki Homebase Fakultas dan Program Studi</div>
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th><center>NIP/NIK</center></th>
			<th><center>Nama Dosen</center></th>
			<th class="noheader"><center>Aksi</center></th>
		</tr>
		</thead>
		<tbody>
			{foreach item="dsn" from=$DOSEN}
		<tr>
			<td>{$dsn.USERNAME}</td>
			<td>{$dsn.NM_PENGGUNA}</td>
			<td><center><a href="dosen_detail.php?action=detail&id={$dsn.ID_PENGGUNA}">Detail</a></center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
		</tbody>
	</table>