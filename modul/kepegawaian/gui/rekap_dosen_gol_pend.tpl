<div class="center_title_bar">Rekapitulasi Data Dosen Aktif berdasarkan Golongan dan Pendidikan {foreach item="list" from=$FAK}<li>{$list.FAKULTAS}</li>{/foreach}</div>
	<table  width="850" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th rowspan="2" width="130" style="vertical-align: middle;"><center>Golongan</center></th>
			<th colspan="7" width="720"><center>Pendidikan Akhir</center></th>
		</tr>
		<tr>
			<th width="90"><center>S3</center></th>
			<th width="90"><center>S2</center></th>
			<th width="90"><center>S1</center></th>
			<th width="135"><center>Spesialis</center></th>
			<th width="135"><center>Profesi</center></th>
			<th width="90"><center>&Sigma;</center></th>
			<th width="90"><center>&#37;</center></th>
		</tr>
		</thead>
		<tbody>
			{foreach item="gol" from=$GOL}
		<tr>
			<td><center>{$gol.GOLONGAN}</center></td>
			<td><center>{$gol.S3}</center></td>
			<td><center>{$gol.S2}</center></td>
			<td><center>{$gol.S1}</center></td>
			<td><center>{$gol.SP}</center></td>
			<td><center>{$gol.PR}</center></td>
			<td><center>{$gol.TOTAL}</center></td>
			<td><center>{math equation="((x / y) * 100)" x=$gol.TOTAL y=$TTL format="%.2f"}</center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
			{foreach item="jml" from=$JML}
		<tr>
			<th><center>&Sigma;</center></th>
			<th><center>{$jml.S3}</center></th>
			<th><center>{$jml.S2}</center></th>
			<th><center>{$jml.S1}</center></th>
			<th><center>{$jml.SP}</center></th>
			<th><center>{$jml.PR}</center></th>
			<th><center>{$jml.TOTAL}</center></th>
			<th><center></center></th>
		</tr>
			{/foreach}
			{foreach item="psn" from=$PSN}
		<tr>
			<th><center>&#37;</center></th>
			<th><center>{$psn.S3|string_format:"%.2f"}</center></th>
			<th><center>{$psn.S2|string_format:"%.2f"}</center></th>
			<th><center>{$psn.S1|string_format:"%.2f"}</center></th>
			<th><center>{$psn.SP|string_format:"%.2f"}</center></th>
			<th><center>{$psn.PR|string_format:"%.2f"}</center></th>
			<th><center></center></th>
			<th><center>{$psn.PERSEN}</center></th>
		</tr>
			{/foreach}
		</tbody>
	</table>