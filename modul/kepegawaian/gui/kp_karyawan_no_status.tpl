<div class="center_title_bar">Data Karyawan Yang Belum Memiliki Status {foreach item="list" from=$UK}<li>{$list.UNIT_KERJA}</li>{/foreach}</div>
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            3: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th>NIP/NIK</th>
			<th>Nama Karyawan</th>
			<th>Unit Kerja</th>
			<th class="noheader">Aksi</th>
		</tr>
		</thead>
		<tbody>
			{foreach item="peg" from=$PEGAWAI}
		<tr>
			<td>{$peg.USERNAME}</td>
			<td>{$peg.NM_PENGGUNA}</td>
			<td>{$peg.NM_UNIT_KERJA}</td>
			<td><center><a href="karyawan_detail.php?action=detail&id={$peg.ID_PENGGUNA}">Detail</a></center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
		</tbody>
	</table>