<div class="center_title_bar">Rekapitulasi Data Dosen Aktif berdasarkan Golongan dan Jabatan Fungsional {foreach item="list" from=$FAK}<li>{$list.FAKULTAS}</li>{/foreach}</div>
	<table  width="850" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th rowspan="2" width="130" style="vertical-align: middle;"><center>Golongan</center></th>
			<th colspan="7" width="720"><center>Jabatan Fungsional</center></th>
		</tr>
		<tr>
			<th width="180" style="vertical-align: middle;"><center>Belum memiliki jabatan fungsional</center></th>
			<th width="90" style="vertical-align: middle;"><center>Asisten Ahli</center></th>
			<th width="90" style="vertical-align: middle;"><center>Lektor</center></th>
			<th width="90" style="vertical-align: middle;"><center>Lektor Kepala</center></th>
			<th width="90" style="vertical-align: middle;"><center>Guru Besar</center></th>
			<th width="90" style="vertical-align: middle;"><center>&Sigma;</center></th>
			<th width="90" style="vertical-align: middle;"><center>&#37;</center></th>
		</tr>
		</thead>
		<tbody>
			{foreach item="gol" from=$GOL}
		<tr>
			<td><center>{$gol.GOLONGAN}</center></td>
			<td><center>{$gol.I}</center></td>
			<td><center>{$gol.II}</center></td>
			<td><center>{$gol.III}</center></td>
			<td><center>{$gol.IV}</center></td>
			<td><center>{$gol.V}</center></td>
			<td><center>{$gol.TOTAL}</center></td>
			<td><center>{math equation="((x / y) * 100)" x=$gol.TOTAL y=$TTL format="%.2f"}</center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
			{foreach item="jml" from=$JML}
		<tr>
			<th><center>&Sigma;</center></th>
			<th><center>{$jml.I}</center></th>
			<th><center>{$jml.II}</center></th>
			<th><center>{$jml.III}</center></th>
			<th><center>{$jml.IV}</center></th>
			<th><center>{$jml.V}</center></th>
			<th><center>{$jml.TOTAL}</center></th>
			<th><center></center></th>
		</tr>
			{/foreach}
			{foreach item="psn" from=$PSN}
		<tr>
			<th><center>&#37;</center></th>
			<th><center>{$psn.I|string_format:"%.2f"}</center></th>
			<th><center>{$psn.II|string_format:"%.2f"}</center></th>
			<th><center>{$psn.III|string_format:"%.2f"}</center></th>
			<th><center>{$psn.IV|string_format:"%.2f"}</center></th>
			<th><center>{$psn.V|string_format:"%.2f"}</center></th>
			<th><center></center></th>
			<th><center>{$psn.PERSEN}</center></th>
		</tr>
			{/foreach}
		</tbody>
	</table>