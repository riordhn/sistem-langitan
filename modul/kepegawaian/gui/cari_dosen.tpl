<div class="center_title_bar">Pencarian Data Dosen</div>
<form action="cari_dosen.php" method="get">
<p>Nama Dosen :  <input name="cari" type="text" style="width:300px;" /> <input type="submit" name="Cari_Dosen" value="Cari Dosen"></p>
</form>
{if $smarty.get.cari == ''}
{else}
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[2,0]],
		headers: {
            5: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th>Photo Dosen</td>
			<th>NIP/NIK</td>
			<th>Nama Dosen</td>
			<th>Program Studi</td>
			<th>Status</td>
			<th class="noheader">Aksi</td>
		</tr>
		</thead>
		<tbody>
			{foreach name=test item="list" from=$DOSEN}
		<tr>
			<td><img src="{$list.FOTO_PENGGUNA}/{$list.NIP_DOSEN}.JPG" width="160px"/></td>
			<td>{$list.NIP_DOSEN}</td>
			<td>{$list.NM_PENGGUNA}</td>
			<td>{$list.NM_PROGRAM_STUDI}</td>
			<td>{$list.NM_STATUS_PENGGUNA}</td>
			<td><center><a href="dosen_detail.php?action=detail&id={$list.ID_PENGGUNA}">Detail</a></center></td>
		</tr>
			{/foreach}
		</tbody>
	</table>
{/if}