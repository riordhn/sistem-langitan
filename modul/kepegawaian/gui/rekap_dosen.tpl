<div class="center_title_bar"> Rekapitulasi Dosen </div>
<table width="850" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="38" bgcolor="#FFCC33"><div align="center">No</div></td>
    <td width="178" bgcolor="#FFCC33"><div align="center">Sub menu</div></td>
    <td width="634" bgcolor="#FFCC33"><div align="center">Uraian</div></td>
  </tr>
  <tr>
    <td><center>1</center></td>
    <td>Gol/Usia</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Golongan dan Usia Dosen di Fakultas</td>
  </tr>
  <tr>
    <td><center>2</center></td>
    <td>Gol/Pend</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Golongan dan Pendidikan Akhir Dosen di Fakultas</td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Pend/Usia</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Pendidikan Akhir dan Usia Dosen di Fakultas</td>
  </tr>
  <tr>
    <td><center>4</center></td>
    <td>Jab/Usia</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Jabatan Fungsional dan Usia Dosen di Fakultas</td>
  </tr>
  <tr>
    <td><center>5</center></td>
    <td>Jab/Pend</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Jabatan Fungsional dan Pendidikan Akhir Dosen di Fakultas</td>
  </tr>
  <tr>
    <td><center>6</center></td>
    <td>Gol/Jab</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Golongan dan Jabatan Fungsional Dosen di Fakultas</td>
  </tr>
  <tr>
    <td><center>7</center></td>
    <td>Pend 5 Thn Terakhir</td>
    <td>Difungsikan untuk menampilkan rekapitulasi data Dosen Aktif berdasarkan Pendidikan Akhir Dosen di Fakultas dalam 5 Tahun Terakhir</td>
  </tr>
</table>