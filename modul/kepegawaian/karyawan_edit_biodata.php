<?php
//Yudi Sulistya, 15/06/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

if (isset($_GET['action'])=='edit_biodata')
{
		$id_peg = $_GET['id'];
		
		$id=getvar("select id_pengguna from pegawai where id_pegawai=$id_peg");
		$smarty->assign('ID', $id['ID_PENGGUNA']);
		
		$pegawai=getData("select peg.id_pegawai, peg.nip_pegawai as username, upper(peg.alamat_pegawai) as alamat_rumah_pegawai, peg.telp_pegawai, peg.status_pegawai,
		upper(pgg.nm_pengguna) as nm_pengguna, pgg.gelar_depan, pgg.gelar_belakang, pgg.id_kota_lahir, TO_CHAR(pgg.tgl_lahir_pengguna, 'DD-MM-YYYY') as tgl_lahir_pengguna, pgg.email_pengguna, pgg.kelamin_pengguna,
		upper(gol.nm_golongan) as nm_golongan, upper(gol.nm_pangkat) as nm_pangkat, upper(jab.nm_jabatan_pegawai) as nm_jabatan_pegawai, upper(ukr.nm_unit_kerja) as nm_unit_kerja,
		peg.id_status_pengguna
		from pegawai peg
		left join pengguna pgg on peg.id_pengguna=pgg.id_pengguna
		left join kota kt on kt.id_kota=pgg.id_kota_lahir
		left join unit_kerja ukr on ukr.id_unit_kerja=peg.id_unit_kerja
		left join program_studi pst on ukr.id_program_studi=pst.id_program_studi
		left join fakultas fak on ukr.id_fakultas=fak.id_fakultas
		left join golongan gol on gol.id_golongan=peg.id_golongan
		left join jabatan_pegawai jab on jab.id_jabatan_pegawai=peg.id_jabatan_pegawai
		left join status_pengguna stp on stp.id_status_pengguna=peg.id_status_pengguna
		where peg.id_pegawai=$id_peg");
		$smarty->assign('PEGAWAI', $pegawai);
		
		$get_photo=getvar("select nip_pegawai as photo from pegawai where id_pegawai=$id_peg");
		$filename="../../foto_pegawai/".$get_photo['PHOTO'].".JPG";
		if (file_exists($filename)) {
			$photo = $filename;
		} else {
			$photo = 'includes/images/unknown.png';
		}
		$smarty->assign('PHOTO', $photo);
		
		$sts_peg=getData("select distinct status_pegawai from pegawai where status_pegawai is not null order by status_pegawai");
		$smarty->assign('STS_PEG', $sts_peg);
		
		$sts_aktif=getData("select id_status_pengguna, upper(nm_status_pengguna) as nm_status_pengguna from status_pengguna where id_role=22 order by status_aktif desc, nm_status_pengguna");
		$smarty->assign('STS_AKTIF', $sts_aktif);
		
		$prodi=getvar("select id_fakultas, id_program_studi	from program_studi where id_program_studi=(select id_program_studi from dosen where id_dosen=$id_dsn)");

$kdfak=$_POST['kdfak'];

$listfak=getData("select id_fakultas, upper(nm_fakultas) as nm_fakultas from fakultas order by id_fakultas");
$smarty->assign('T_FAK', $listfak);

$kdfak1 = isSet($_POST['kdfak']) ? $_POST['kdfak'] : $prodi['ID_FAKULTAS'];
$smarty->assign('FAKGET',$kdfak1);

if (isset($_POST['kdfak'])) {
$kdprodi=getData("
select pst.id_program_studi, upper(substr(jjg.nm_jenjang,1,2)||' - '||pst.nm_program_studi) as nm_program_studi 
from program_studi pst 
left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang 
where id_fakultas=$kdfak
order by jjg.nm_jenjang, pst.nm_program_studi
");
$smarty->assign('PRO', $kdprodi);
} else {
$kdprodi=getData("
select pst.id_program_studi, upper(substr(jjg.nm_jenjang,1,2)||' - '||pst.nm_program_studi) as nm_program_studi 
from program_studi pst 
left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang 
where pst.id_fakultas = (select id_fakultas from program_studi where id_program_studi=(select id_program_studi from dosen where id_dosen=$id_dsn))
order by jjg.nm_jenjang, pst.nm_program_studi
");
$smarty->assign('PRO', $kdprodi);
}
			
		$jk=getData("select distinct kelamin_pengguna, case when kelamin_pengguna='1' then 'LAKI-LAKI' else 'PEREMPUAN' end as nm_kelamin_pengguna from pengguna where kelamin_pengguna in ('1', '2') order by kelamin_pengguna");
		$smarty->assign('JK', $jk);
		
		$jab=getData("select id_jabatan_pegawai, upper(nm_jabatan_pegawai) as nm_jabatan_pegawai from jabatan_pegawai order by id_jabatan_pegawai");
		$smarty->assign('JAB', $jab);
		
		$status=getData("select id_status_pengguna, upper(nm_status_pengguna) as nm_status_pengguna from status_pengguna order by id_status_pengguna");
		$smarty->assign('STATUS', $status);

        $data_kota = array();
        $provinsi = getData("SELECT * FROM PROVINSI WHERE ID_NEGARA=114 ORDER BY NM_PROVINSI");
        foreach ($provinsi as $data) {
            array_push($data_kota, array(
                'nama' => $data['NM_PROVINSI'],
                'kota' => getData("SELECT id_kota, tipe_dati2||' '||nm_kota as kota FROM KOTA WHERE ID_PROVINSI='{$data['ID_PROVINSI']}' ORDER BY TIPE_DATI2, NM_KOTA")
            ));
        }
		$smarty->assign('data_kota', $data_kota);
		$smarty->assign('count_provinsi', count($data_kota));
}

if (isset($_POST['action'])=='update')
{
	$id_pegawai = $_POST['id_pegawai'];
	$id_pengguna = $_POST['id_pengguna'];
	
// tabel pengguna
	$username = $_POST['nip'];
	$gelar_depan = $_POST['gelar_dpn'];
	$gelar_belakang = $_POST['gelar_blkg'];
	$nm_pengguna = $_POST['nm_lengkap'];
	$id_kota_lahir = $_POST['id_kota_lahir'];
	$tgl_lahir_pengguna = $_POST['tgl_lahir'];
	//$tgl_lahir_pengguna = date("dd/mm/yyyy", strtotime($_POST['tgl_lahir']));
	$kelamin_pengguna = $_POST['jk'];

	gantidata("update pengguna set username=trim('".$username."'), gelar_depan=trim('".$gelar_depan."'), gelar_belakang=trim('".$gelar_belakang."'), 
	nm_pengguna=trim(upper('".$nm_pengguna."')), id_kota_lahir=$id_kota_lahir, tgl_lahir_pengguna=to_date('".$tgl_lahir_pengguna."','dd-mm-yyyy'), kelamin_pengguna=$kelamin_pengguna
	where id_pengguna=$id_pengguna");
	
// tabel pegawai
	$nip_pegawai = $_POST['nip'];
	$alamat_rumah_pegawai = $_POST['alamat'];
	$id_unit_kerja = $_POST['id_unit_kerja'];
	$status_pegawai = $_POST['status_peg'];
	$id_status_pengguna = $_POST['aktif'];

	gantidata("update pegawai set nip_pegawai=trim('".$nip_pegawai."'), alamat_pegawai=trim(upper('".$alamat_rumah_pegawai."')), id_unit_kerja=$id_unit_kerja,
	status_pegawai='".$status_pegawai."', id_status_pengguna=$id_status_pengguna where id_pegawai=$id_pegawai");

	echo '<script>location.href="javascript:history.go(-1)";</script>';
}

$smarty->display('karyawan_edit_biodata.tpl');

?>