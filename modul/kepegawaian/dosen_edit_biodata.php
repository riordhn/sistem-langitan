<?php
//Yudi Sulistya, 15/06/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

if (isset($_GET['action'])=='edit_biodata')
{
		$id_dsn = $_GET['id'];
		
		$dosen=getData("select dsn.id_pengguna, dsn.id_dosen, dsn.nip_dosen as username, dsn.nidn_dosen, dsn.serdos, upper(dsn.alamat_rumah_dosen) as alamat_rumah_dosen, dsn.tlp_dosen, dsn.mobile_dosen, dsn.status_dosen,
		upper(pgg.nm_pengguna) as nm_pengguna, pgg.gelar_depan, pgg.gelar_belakang, pgg.id_kota_lahir, pgg.tgl_lahir_pengguna, case when pgg.email_pengguna not like '%unair.ac.id' then 'ganti' else pgg.email_pengguna end as ganti_email,
		pgg.email_pengguna, pgg.kelamin_pengguna, dsn.id_golongan, upper(gol.nm_golongan) as nm_golongan, upper(gol.nm_pangkat) as nm_pangkat, dsn.id_jabatan_fungsional, upper(fgs.nm_jabatan_fungsional) as nm_jabatan_fungsional,
		dsn.id_jabatan_pegawai, upper(jab.nm_jabatan_pegawai) as nm_jabatan_pegawai, dsn.id_status_pengguna, dsn.id_program_studi, upper(nm_jenjang||' - '||nm_program_studi) as nm_program_studi, upper(nm_fakultas) as nm_fakultas
		from dosen dsn
		left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
		left join program_studi pst on dsn.id_program_studi=pst.id_program_studi
		left join fakultas fak on pst.id_fakultas=fak.id_fakultas
		left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang
		left join golongan gol on gol.id_golongan=dsn.id_golongan
		left join jabatan_fungsional fgs on fgs.id_jabatan_fungsional=dsn.id_jabatan_fungsional
		left join jabatan_pegawai jab on jab.id_jabatan_pegawai=dsn.id_jabatan_pegawai
		where dsn.id_dosen=$id_dsn");
		$smarty->assign('DOSEN', $dosen);
		
		$get_photo=getvar("select nip_dosen as photo from dosen where id_dosen=$id_dsn");
		$filename="../../foto_pegawai/".$get_photo['PHOTO'].".JPG";
		if (file_exists($filename)) {
			$photo = $filename;
		} else {
			$photo = 'includes/images/unknown.png';
		}
		$smarty->assign('PHOTO', $photo);
		
		$sts_dsn=getData("select distinct status_dosen from dosen where status_dosen is not null order by status_dosen");
		$smarty->assign('STS_DSN', $sts_dsn);
		
		$sts_aktif=getData("select id_status_pengguna, upper(nm_status_pengguna) as nm_status_pengguna from status_pengguna where id_role=4 order by status_aktif desc, nm_status_pengguna");
		$smarty->assign('STS_AKTIF', $sts_aktif);
		
		$prodi=getvar("select id_fakultas, id_program_studi	from program_studi where id_program_studi=(select id_program_studi from dosen where id_dosen=$id_dsn)");

$kdfak=$_POST['kdfak'];

$listfak=getData("select id_fakultas, upper(nm_fakultas) as nm_fakultas from fakultas order by id_fakultas");
$smarty->assign('T_FAK', $listfak);

$kdfak1 = isSet($_POST['kdfak']) ? $_POST['kdfak'] : $prodi['ID_FAKULTAS'];
$smarty->assign('FAKGET',$kdfak1);

if (isset($_POST['kdfak'])) {
$kdprodi=getData("
select pst.id_program_studi, upper(substr(jjg.nm_jenjang,1,2)||' - '||pst.nm_program_studi) as nm_program_studi 
from program_studi pst 
left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang 
where id_fakultas=$kdfak
order by jjg.nm_jenjang, pst.nm_program_studi
");
$smarty->assign('PRO', $kdprodi);
} else {
$kdprodi=getData("
select pst.id_program_studi, upper(substr(jjg.nm_jenjang,1,2)||' - '||pst.nm_program_studi) as nm_program_studi 
from program_studi pst 
left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang 
where pst.id_fakultas = (select id_fakultas from program_studi where id_program_studi=(select id_program_studi from dosen where id_dosen=$id_dsn))
order by jjg.nm_jenjang, pst.nm_program_studi
");
$smarty->assign('PRO', $kdprodi);
}
		
		$jk=getData("select distinct kelamin_pengguna, case when kelamin_pengguna='1' then 'LAKI-LAKI' else 'PEREMPUAN' end as nm_kelamin_pengguna from pengguna where kelamin_pengguna in ('1', '2') order by kelamin_pengguna");
		$smarty->assign('JK', $jk);

		$jab=getData("select id_jabatan_pegawai, upper(nm_jabatan_pegawai) as nm_jabatan_pegawai from jabatan_pegawai order by id_jabatan_pegawai");
		$smarty->assign('JAB', $jab);
		
		$status=getData("select id_status_pengguna, upper(nm_status_pengguna) as nm_status_pengguna from status_pengguna order by id_status_pengguna");
		$smarty->assign('STATUS', $status);

        $data_kota = array();
        $provinsi = getData("SELECT * FROM PROVINSI WHERE ID_NEGARA=114 ORDER BY NM_PROVINSI");
        foreach ($provinsi as $data) {
            array_push($data_kota, array(
                'nama' => $data['NM_PROVINSI'],
                'kota' => getData("SELECT id_kota, tipe_dati2||' '||nm_kota as kota FROM KOTA WHERE ID_PROVINSI='{$data['ID_PROVINSI']}' ORDER BY TIPE_DATI2, NM_KOTA")
            ));
        }
		$smarty->assign('data_kota', $data_kota);
		$smarty->assign('count_provinsi', count($data_kota));
}

if (isset($_POST['action'])=='update')
{
	$id_dosen = $_POST['id_dosen'];
	$id_pengguna = $_POST['id_pengguna'];
	
// tabel pengguna
	$username = $_POST['nip'];
	$gelar_depan = $_POST['gelar_dpn'];
	$gelar_belakang = $_POST['gelar_blkg'];
	$nm_pengguna = $_POST['nm_lengkap'];
	$id_kota_lahir = $_POST['id_kota_lahir'];
	$tgl_lahir_pengguna = $_POST['tgl_lahir'];
	$email_pengguna = $_POST['email'];
	$kelamin_pengguna = $_POST['jk'];

	gantidata("update pengguna set username=trim('".$username."'), gelar_depan=trim('".$gelar_depan."'), gelar_belakang=trim('".$gelar_belakang."'), 
	nm_pengguna=trim(upper('".$nm_pengguna."')), id_kota_lahir=$id_kota_lahir, tgl_lahir_pengguna=to_date('".$tgl_lahir_pengguna."','dd-mm-yyyy'), kelamin_pengguna=$kelamin_pengguna,
	email_pengguna=trim(lower('".$email_pengguna."'))
	where id_pengguna=$id_pengguna");
	
// tabel dosen
	$nip_dosen = $_POST['nip'];
	$nidn = $_POST['nidn'];
	$serdos = $_POST['serdos'];
	$id_program_studi = $_POST['prodi'];
	$status_dosen = $_POST['status_dsn'];
	$id_status_pengguna = $_POST['aktif'];
	$alamat_rumah_dosen = $_POST['alamat'];

	gantidata("update dosen set nip_dosen=trim('".$nip_dosen."'), nidn=trim('".$nidn."'), serdos=trim('".$serdos."'), id_program_studi=$id_program_studi, 
	status_dosen='".$status_dosen."', id_status_pengguna=$id_status_pengguna, alamat_rumah_dosen=trim(upper('".$alamat_rumah_dosen."'))
	where id_dosen=$id_dosen");

	echo '<script>location.href="javascript:history.go(-1)";</script>';
}

$smarty->display('dosen_edit_biodata.tpl');

?>