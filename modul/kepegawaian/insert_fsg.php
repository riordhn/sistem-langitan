<?php

//Yudi Sulistya, 15/08/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

if ($user->Role() == AUCC_ROLE_SDM) {
$id = $_GET['id'];

if (isset($_POST['submit'])) {
	$id_pengguna = $_POST['id_pengguna'];
	$id_jabatan_fungsional = $_POST['id_jabatan_fungsional'];
	$no_sk_sej_jab_fungsional = $_POST['no_sk_sej_jab_fungsional'];
	$ket_sk_sej_jab_fungsional = $_POST['ket_sk_sej_jab_fungsional'];
	$tmt_sej_jab_fungsional = $_POST['tmt_sej_jab_fungsional'];
	
	InsertData("insert into sejarah_jabatan_fungsional (id_pengguna, id_jabatan_fungsional, no_sk_sej_jab_fungsional, asal_sk_sej_jab_fungsional, ket_sk_sej_jab_fungsional, tmt_sej_jab_fungsional) 
				values ($id_pengguna, $id_jabatan_fungsional, trim(upper('".$no_sk_sej_jab_fungsional."')), trim(upper('".$asal_sk_sej_jab_fungsional."')), trim(upper('".$ket_sk_sej_jab_fungsional."')), to_date('".$tmt_sej_jab_fungsional."','dd-mm-yyyy'))");

	echo '<script>alert("Data berhasil ditambahkan")</script>';
	echo '<script>window.parent.document.location.reload();</script>';
}

echo'
<html>
<head>
<link rel="stylesheet" type="text/css" href="includes/iframe.css" />
<script type="text/javascript" src="js/datetimepicker.js"></script>
<script language="JavaScript">
function validate_form(){
if (document.forms["fsginsert"]["no_sk_sej_jab_fungsional"].value==null || document.forms["fsginsert"]["no_sk_sej_jab_fungsional"].value==" " || document.forms["fsginsert"]["no_sk_sej_jab_fungsional"].value=="")
{
	alert ("No. SK harus terisi");
	return false;
}
else if (document.forms["fsginsert"]["asal_sk_sej_jab_fungsional"].value==null || document.forms["fsginsert"]["asal_sk_sej_jab_fungsional"].value==" " || document.forms["fsginsert"]["asal_sk_sej_jab_fungsional"].value=="")
{
	alert ("Asal SK harus terisi");
	return false;
}
else if (document.forms["fsginsert"]["tmt_sej_jab_fungsional"].value==null || document.forms["fsginsert"]["tmt_sej_jab_fungsional"].value==" " || document.forms["fsginsert"]["tmt_sej_jab_fungsional"].value=="")
{
	alert ("Asal SK harus terisi");
	return false;
}
	return true;
}
</script>
</head>
<body>
	<form name="fsginsert" id="fsginsert" method="post" onsubmit="return validate_form();">
	<input type="hidden" name="id_pengguna" value="'.$id.'">
	<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td>Jabatan</td>
			<td>&nbsp;:&nbsp;<select name="id_jabatan_fungsional">';

$jjg="select id_jabatan_fungsional, upper(nm_jabatan_fungsional) from jabatan_fungsional order by id_jabatan_fungsional";
$result = $db->Query($jjg)or die("salah kueri golongan ");
while($r = $db->FetchRow()) {
echo '<option value="'.$r[0].'">'.$r[1].'</option>';
}
echo '
				</select>
				&nbsp;&nbsp;TMT&nbsp;:&nbsp;<input type="text" name="tmt_sej_jab_fungsional" id="tmt_sej_jab_fungsional" style="text-align:center;" onclick="javascript:NewCssCal(\'tmt_sej_jab_fungsional\',\'ddmmyyyy\',\'arrow\',\'\',\'\',\'\',\'past\')" value="'.date("d-m-Y").'" />
			</td>
		</tr>
		<tr>
			<td>No. SK</td>
			<td>&nbsp;:&nbsp;<input type="text" name="no_sk_sej_jab_fungsional" maxlength="50" style="width:600px;" /></td>
		</tr>
		<tr>
			<td>Asal SK</td>
			<td>&nbsp;:&nbsp;<input type="text" name="asal_sk_sej_jab_fungsional" maxlength="50" style="width:600px;" /></td>
		</tr>
		<tr>
			<td>Keterangan</td>
			<td>&nbsp;:&nbsp;<input type="text" name="ket_sk_sej_jab_fungsional" maxlength="50" style="width:600px;" /></td>
		</tr>
		<tr>
			<td></td>
			<td>
				&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="Simpan" />
			</td>
		</tr>
	</table>
	</form>
</body>
</html>';
} else {
    header("location: /logout.php");
    exit();
}
?>