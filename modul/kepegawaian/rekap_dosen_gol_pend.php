<?php
//Yudi Sulistya, 20/04/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 
$id_fak=$user->ID_FAKULTAS;

		$fak=getData("select 'FAKULTAS '||upper(fak.nm_fakultas) as fakultas
		from fakultas fak
		where fak.id_fakultas=$id_fak");
		$smarty->assign('FAK', $fak);

		$ttl=getvar("select sum(S3+S2+S1+SP+PR) as total
						from
						(
						select
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join golongan gol on gol.id_golongan=dsn.id_golongan
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select id_golongan, upper(golongan) as golongan, sum(S3) as S3, sum(S2) as S2, sum(S1) as S1, sum(SP) as SP, sum(PR) as PR, sum(S3+S2+S1+SP+PR) as total
						from
						(
						select dsn.id_golongan,
						case when dsn.id_golongan is not null then gol.nm_golongan else '-' end as golongan,
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join golongan gol on gol.id_golongan=dsn.id_golongan
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)
						group by id_golongan, golongan
						order by golongan");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(S3) as S3, sum(S2) as S2, sum(S1) as S1, sum(SP) as SP, sum(PR) as PR, sum(S3+S2+S1+SP+PR) as total
						from
						(
						select
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join golongan gol on gol.id_golongan=dsn.id_golongan
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('JML', $jml);

		$psn=getData("select case when sum(S3)=0 then 0 else round((sum(S3)/sum(S3+S2+S1+SP+PR)*100),2) end as S3,
						case when sum(S2)=0 then 0 else round((sum(S2)/sum(S3+S2+S1+SP+PR)*100),2) end as S2,
						case when sum(S1)=0 then 0 else round((sum(S1)/sum(S3+S2+S1+SP+PR)*100),2) end as S1,
						case when sum(SP)=0 then 0 else round((sum(SP)/sum(S3+S2+S1+SP+PR)*100),2) end as SP,
						case when sum(PR)=0 then 0 else round((sum(PR)/sum(S3+S2+S1+SP+PR)*100),2) end as PR,
						round((sum(S3+S2+S1+SP+PR)/sum(S3+S2+S1+SP+PR)*100),2) as persen
						from
						(
						select
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='Spesialis' then 1 else 0 end as SP,
						case when pda.nama_pendidikan_akhir='Profesi' then 1 else 0 end as PR
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
						left join golongan gol on gol.id_golongan=dsn.id_golongan
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('PSN', $psn);

$smarty->display('rekap_dosen_gol_pend.tpl');

?>