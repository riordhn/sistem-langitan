<?php
//Yudi Sulistya, 16/04/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$dosen=getData("select dsn.id_pengguna, dsn.nip_dosen as username, dsn.nidn_dosen, dsn.serdos, 
		case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
		else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna
		from dosen dsn
		left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
		where dsn.id_program_studi is null or dsn.id_program_studi=''");
$smarty->assign('DOSEN', $dosen);

$smarty->display('dosen_no_prodi.tpl');

?>