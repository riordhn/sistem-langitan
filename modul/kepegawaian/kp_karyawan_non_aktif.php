<?php
//Yudi Sulistya, 01/10/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA;
$id_fak=$user->ID_FAKULTAS;

$uk=getData("select upper(uk.nm_unit_kerja)||' ('||count(peg.nip_pegawai)||' karyawan)' as unit_kerja
from pegawai peg
left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
where uk.id_fakultas=$id_fak
and (peg.id_status_pengguna is null or peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=0))
group by uk.nm_unit_kerja");
$smarty->assign('UK', $uk);

$count=getvar("select count(*) as cek from
(select peg.id_pengguna, peg.nip_pegawai as username,
case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
upper(nm_unit_kerja) as nm_unit_kerja
from pegawai peg
left join pengguna pgg on peg.id_pengguna=pgg.id_pengguna
left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
left join fakultas fak on fak.id_fakultas=uk.id_fakultas
left join program_studi pst on pst.id_program_studi=uk.id_program_studi
where uk.id_fakultas=$id_fak
and (peg.id_status_pengguna is null or peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=0)))");
$smarty->assign('DATA', $count['CEK']);

$pegawai=getData("select peg.id_pengguna, peg.nip_pegawai as username,
case when gelar_belakang is null then trim(gelar_depan||' '||upper(nm_pengguna))
else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna,
upper(nm_unit_kerja) as nm_unit_kerja
from pegawai peg
left join pengguna pgg on peg.id_pengguna=pgg.id_pengguna
left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
left join fakultas fak on fak.id_fakultas=uk.id_fakultas
left join program_studi pst on pst.id_program_studi=uk.id_program_studi
where uk.id_fakultas=$id_fak
and (peg.id_status_pengguna is null or peg.id_status_pengguna in (select id_status_pengguna from status_pengguna where id_role=22 and status_aktif=0))");
$smarty->assign('PEGAWAI', $pegawai);

$smarty->display('kp_karyawan_non_aktif.tpl');

?>