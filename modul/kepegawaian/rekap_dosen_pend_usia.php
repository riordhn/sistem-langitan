<?php
//Yudi Sulistya, 20/04/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 
$id_fak=$user->ID_FAKULTAS;

		$fak=getData("select 'FAKULTAS '||upper(fak.nm_fakultas) as fakultas
		from fakultas fak
		where fak.id_fakultas=$id_fak");
		$smarty->assign('FAK', $fak);

		$ttl=getvar("select sum(A+B+C+D+E+F) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60 then 1 else 0 end as E,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60 then 1 else 0 end as F
						from dosen dsn
						left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
						left join golongan gol on gol.id_golongan=dsn.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select id_pendidikan_akhir, nama_pendidikan_akhir, sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(F) as VI, sum(A+B+C+D+E+F) as total
						from
						(
						select pda.id_pendidikan_akhir,
						pda.nama_pendidikan_akhir,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60 then 1 else 0 end as E,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60 then 1 else 0 end as F
						from dosen dsn
						left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
						left join golongan gol on gol.id_golongan=dsn.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)
						group by id_pendidikan_akhir, nama_pendidikan_akhir");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(F) as VI, sum(A+B+C+D+E+F) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60 then 1 else 0 end as E,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60 then 1 else 0 end as F
						from dosen dsn
						left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
						left join golongan gol on gol.id_golongan=dsn.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('JML', $jml);

		$psn=getData("select case when sum(A)=0 then 0 else round((sum(A)/sum(A+B+C+D+E+F)*100),2) end as I,
						case when sum(B)=0 then 0 else round((sum(B)/sum(A+B+C+D+E+F)*100),2) end as II,
						case when sum(C)=0 then 0 else round((sum(C)/sum(A+B+C+D+E+F)*100),2) end as III,
						case when sum(D)=0 then 0 else round((sum(D)/sum(A+B+C+D+E+F)*100),2) end as IV,
						case when sum(E)=0 then 0 else round((sum(E)/sum(A+B+C+D+E+F)*100),2) end as V,
						case when sum(F)=0 then 0 else round((sum(F)/sum(A+B+C+D+E+F)*100),2) end as VI,
						round((sum(A+B+C+D+E+F)/sum(A+B+C+D+E+F)*100),2) as persen
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60 then 1 else 0 end as E,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60 then 1 else 0 end as F
						from dosen dsn
						left join sejarah_pendidikan pdd on dsn.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
						left join golongan gol on gol.id_golongan=dsn.id_golongan
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from dosen where id_program_studi in
						(select id_program_studi from program_studi where id_fakultas=$id_fak)
						and id_status_pengguna in
						(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('PSN', $psn);

$smarty->display('rekap_dosen_pend_usia.tpl');

?>