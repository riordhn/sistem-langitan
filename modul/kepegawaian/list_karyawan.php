<?php
//Yudi Sulistya, 20/08/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

if ($request_method == 'GET' or $request_method == 'POST')
{

// Golongan Usia
	if (get('action') == 'empty' && get('mode') == 'gu') {
		$id_gol = get('gol','');
		$id_uk = get('uk','');

		$uk=getData("select nm_unit_kerja from unit_kerja where id_unit_kerja=$id_uk");
		$smarty->assign('UK',$uk);
		
		$gol=getvar("select case when upper(nm_golongan) = 'HONORER' then 'HONORER' else 'GOLONGAN'||' '||upper(nm_golongan) end AS nm_golongan from golongan where id_golongan=$id_gol");
		$smarty->assign('GOL',$gol['NM_GOLONGAN']);
		
		$usia="tanpa tanggal lahir";
		$smarty->assign('USIA',$usia);
		
		$hasil=getData("select pgg.id_pengguna, nip_pegawai, 
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_unit_kerja) as nm_unit_kerja, upper(nm_role) as nm_role
		from pegawai peg
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
		left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		where peg.id_golongan=$id_gol and pgg.tgl_lahir_pengguna is null
		and (uk.id_unit_kerja like '$id_uk' or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja like '$id_uk') or 
		uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja like '$id_uk'))");
		$smarty->assign('KARYAWAN',$hasil);
	} 

	if(get('action') == 'min' && get('mode') == 'gu') {
		$id_gol = get('gol','');
		$id_uk = get('uk','');
		$mulai = get('mulai','');
		$sampai = get('sampai','');

		$uk=getData("select nm_unit_kerja from unit_kerja where id_unit_kerja=$id_uk");
		$smarty->assign('UK',$uk);
		
		$gol=getvar("select case when upper(nm_golongan) = 'HONORER' then 'HONORER' else 'GOLONGAN'||' '||upper(nm_golongan) end AS nm_golongan from golongan where id_golongan=$id_gol");
		$smarty->assign('GOL',$gol['NM_GOLONGAN']);
		
		$kurang=$sampai+1;
		$usia="berusia kurang dari ".$kurang."";
		$smarty->assign('USIA',$usia);
		
		$hasil=getData("select pgg.id_pengguna, nip_pegawai, 
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_unit_kerja) as nm_unit_kerja, upper(nm_role) as nm_role
		from pegawai peg
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
		left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		where peg.id_golongan=$id_gol and trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and (uk.id_unit_kerja like '$id_uk' or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja like '$id_uk') or 
		uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja like '$id_uk'))");
		$smarty->assign('KARYAWAN',$hasil);
	}
	
	if(get('action') == 'max' && get('mode') == 'gu') {
		$id_gol = get('gol','');
		$id_uk = get('uk','');
		$max = get('max','');

		$uk=getData("select nm_unit_kerja from unit_kerja where id_unit_kerja=$id_uk");
		$smarty->assign('UK',$uk);
		
		$gol=getvar("select case when upper(nm_golongan) = 'HONORER' then 'HONORER' else 'GOLONGAN'||' '||upper(nm_golongan) end AS nm_golongan from golongan where id_golongan=$id_gol");
		$smarty->assign('GOL',$gol['NM_GOLONGAN']);
		
		$usia="berusia lebih dari ".$max."";
		$smarty->assign('USIA',$usia);
		
		$hasil=getData("select pgg.id_pengguna, nip_pegawai, 
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_unit_kerja) as nm_unit_kerja, upper(nm_role) as nm_role
		from pegawai peg
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
		left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		where peg.id_golongan=$id_gol and trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > $max
		and (uk.id_unit_kerja like '$id_uk' or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja like '$id_uk') or 
		uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja like '$id_uk'))");
		$smarty->assign('KARYAWAN',$hasil);
	}
	
	if(get('action') == 'between' && get('mode') == 'gu') {
		$id_gol = get('gol','');
		$id_uk = get('uk','');
		$mulai = get('mulai','');
		$sampai = get('sampai','');

		$uk=getData("select nm_unit_kerja from unit_kerja where id_unit_kerja=$id_uk");
		$smarty->assign('UK',$uk);
		
		$gol=getvar("select case when upper(nm_golongan) = 'HONORER' then 'HONORER' else 'GOLONGAN'||' '||upper(nm_golongan) end AS nm_golongan from golongan where id_golongan=$id_gol");
		$smarty->assign('GOL',$gol['NM_GOLONGAN']);
		
		$usia="berusia antara ".$mulai." sampai dengan ".$sampai." tahun";
		$smarty->assign('USIA',$usia);
		
		$hasil=getData("select pgg.id_pengguna, nip_pegawai, 
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_unit_kerja) as nm_unit_kerja, upper(nm_role) as nm_role
		from pegawai peg
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
		left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
		left join role rl on rl.id_role=pgg.id_role
		where peg.id_golongan=$id_gol and trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and (uk.id_unit_kerja like '$id_uk' or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja like '$id_uk') or 
		uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja like '$id_uk'))");
		$smarty->assign('KARYAWAN',$hasil);
	}

// Pendidikan Usia
	if (get('action') == 'empty' && get('mode') == 'pu') {
		$id_pdd = get('pdd','');
		$id_uk = get('uk','');

		$uk=getData("select nm_unit_kerja from unit_kerja where id_unit_kerja=$id_uk");
		$smarty->assign('UK',$uk);
		
		$gol=getvar("select 'PENDIDIKAN TERAKHIR '||upper(nama_pendidikan_akhir) as nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('GOL',$gol['NAMA_PENDIDIKAN_AKHIR']);
		
		$usia="tanpa tanggal lahir";
		$smarty->assign('USIA',$usia);
		
		$hasil=getData("select * from 
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_pegawai, 
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_unit_kerja) as nm_unit_kerja, upper(nm_role) as nm_role
		from pegawai peg
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
		left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
		left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		where pgg.tgl_lahir_pengguna is null
		and pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
			(
			select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
			(
			select id_pengguna from pegawai where
			(uk.id_unit_kerja like '$id_uk' or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja like '$id_uk') or 
			uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja like '$id_uk'))
			)
			group by id_pengguna
			)
		)
		where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('KARYAWAN',$hasil);
	} 

	if(get('action') == 'min' && get('mode') == 'pu') {
		$id_pdd = get('pdd','');
		$id_uk = get('uk','');
		$mulai = get('mulai','');
		$sampai = get('sampai','');

		$uk=getData("select nm_unit_kerja from unit_kerja where id_unit_kerja=$id_uk");
		$smarty->assign('UK',$uk);
		
		$gol=getvar("select 'PENDIDIKAN TERAKHIR '||upper(nama_pendidikan_akhir) as nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('GOL',$gol['NAMA_PENDIDIKAN_AKHIR']);
		
		$kurang=$sampai+1;
		$usia="berusia kurang dari ".$kurang."";
		$smarty->assign('USIA',$usia);
		
		$hasil=getData("select * from 
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_pegawai, 
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_unit_kerja) as nm_unit_kerja, upper(nm_role) as nm_role
		from pegawai peg
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
		left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
		left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
			(
			select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
			(
			select id_pengguna from pegawai where
			(uk.id_unit_kerja like '$id_uk' or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja like '$id_uk') or 
			uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja like '$id_uk'))
			)
			group by id_pengguna
			)
		)
		where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('KARYAWAN',$hasil);
	}
	
	if(get('action') == 'max' && get('mode') == 'pu') {
		$id_pdd = get('pdd','');
		$id_uk = get('uk','');
		$max = get('max','');

		$uk=getData("select nm_unit_kerja from unit_kerja where id_unit_kerja=$id_uk");
		$smarty->assign('UK',$uk);
		
		$gol=getvar("select 'PENDIDIKAN TERAKHIR '||upper(nama_pendidikan_akhir) as nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('GOL',$gol['NAMA_PENDIDIKAN_AKHIR']);
		
		$usia="berusia lebih dari ".$max."";
		$smarty->assign('USIA',$usia);
		
		$hasil=getData("select * from 
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_pegawai, 
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_unit_kerja) as nm_unit_kerja, upper(nm_role) as nm_role
		from pegawai peg
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
		left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
		left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > $max
		and pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
			(
			select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
			(
			select id_pengguna from pegawai where
			(uk.id_unit_kerja like '$id_uk' or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja like '$id_uk') or 
			uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja like '$id_uk'))
			)
			group by id_pengguna
			)
		)
		where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('KARYAWAN',$hasil);
	}
	
	if(get('action') == 'between' && get('mode') == 'pu') {
		$id_pdd = get('pdd','');
		$id_uk = get('uk','');
		$mulai = get('mulai','');
		$sampai = get('sampai','');

		$uk=getData("select nm_unit_kerja from unit_kerja where id_unit_kerja=$id_uk");
		$smarty->assign('UK',$uk);
		
		$gol=getvar("select 'PENDIDIKAN TERAKHIR '||upper(nama_pendidikan_akhir) as nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('GOL',$gol['NAMA_PENDIDIKAN_AKHIR']);
		
		$usia="berusia antara ".$mulai." sampai dengan ".$sampai." tahun";
		$smarty->assign('USIA',$usia);
		
		$hasil=getData("select * from 
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, nip_pegawai, 
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_unit_kerja) as nm_unit_kerja, upper(nm_role) as nm_role
		from pegawai peg
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
		left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
		left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		where trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between $mulai and $sampai
		and pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
			(
			select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
			(
			select id_pengguna from pegawai where
			(uk.id_unit_kerja like '$id_uk' or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja like '$id_uk') or 
			uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja like '$id_uk'))
			)
			group by id_pengguna
			)
		)
		where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('KARYAWAN',$hasil);
	}
	
// Pendidikan Golongan
	if (get('action') == 'list' && get('mode') == 'pg') {
		$id_pdd = get('pdd','');
		$id_gol = get('gol','');
		$id_uk = get('uk','');

		$uk=getData("select nm_unit_kerja from unit_kerja where id_unit_kerja=$id_uk");
		$smarty->assign('UK',$uk);
		
		$gol=getvar("select case when upper(nm_golongan) = 'HONORER' then 'HONORER' else 'GOLONGAN'||' '||upper(nm_golongan) end AS nm_golongan from golongan where id_golongan=$id_gol");
		$smarty->assign('GOL',$gol['NM_GOLONGAN']);
		
		$usia=getvar("select 'PENDIDIKAN TERAKHIR '||upper(nama_pendidikan_akhir) as nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir=$id_pdd");
		$smarty->assign('USIA',$usia['NAMA_PENDIDIKAN_AKHIR']);
		
		$hasil=getData("select * from 
		(
		select pdd.id_pendidikan_akhir, pgg.id_pengguna, peg.id_golongan, nip_pegawai, 
		case when (gelar_belakang is null or gelar_belakang = '') then trim(gelar_depan||' '||upper(nm_pengguna)) else trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) end as nm_pengguna, 
		username||'.JPG' as photo, trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) as usia, upper(nm_unit_kerja) as nm_unit_kerja, upper(nm_role) as nm_role
		from pegawai peg
		left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
		left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
		left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		left join role rl on rl.id_role=pgg.id_role
		where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
			(
			select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
			(
			select id_pengguna from pegawai where
			(uk.id_unit_kerja like '$id_uk' or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja like '$id_uk') or 
			uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja like '$id_uk'))
			)
			group by id_pengguna
			)
		)
		where id_pendidikan_akhir=$id_pdd and id_golongan=$id_gol");
		$smarty->assign('KARYAWAN',$hasil);
	} 

}

$smarty->display('list_karyawan.tpl');

?>