<?php
//Yudi Sulistya, 16/04/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

if ($request_method == 'GET')
{
if (get('action')=='detail')
{
		$id_pgg = get('id');

		$pegawai=getData("select peg.id_pegawai, peg.nip_pegawai as username, upper(peg.alamat_pegawai) as alamat_rumah_pegawai, peg.telp_pegawai, peg.status_pegawai,
		upper(pgg.nm_pengguna) as nm_pengguna, pgg.gelar_depan, pgg.gelar_belakang, kt.tipe_dati2||' '||nm_kota as tempat_lahir, TO_CHAR(pgg.tgl_lahir_pengguna, 'DD-MM-YYYY') as tgl_lahir_pengguna, pgg.email_pengguna, pgg.kelamin_pengguna,
		upper(gol.nm_golongan) as nm_golongan, upper(gol.nm_pangkat) as nm_pangkat, upper(jab.nm_jabatan_pegawai) as nm_jabatan_pegawai, upper(ukr.nm_unit_kerja) as nm_unit_kerja,
		upper(stp.nm_status_pengguna) as nm_status_pengguna
		from pegawai peg
		left join pengguna pgg on peg.id_pengguna=pgg.id_pengguna
		left join kota kt on kt.id_kota=pgg.id_kota_lahir
		left join unit_kerja ukr on ukr.id_unit_kerja=peg.id_unit_kerja
		left join program_studi pst on ukr.id_program_studi=pst.id_program_studi
		left join fakultas fak on ukr.id_fakultas=fak.id_fakultas
		left join golongan gol on gol.id_golongan=peg.id_golongan
		left join jabatan_pegawai jab on jab.id_jabatan_pegawai=peg.id_jabatan_pegawai
		left join status_pengguna stp on stp.id_status_pengguna=peg.id_status_pengguna
		where pgg.id_pengguna=$id_pgg");
		$smarty->assign('PEGAWAI', $pegawai);

		$get_photo=getvar("select username as photo from pengguna where id_pengguna=$id_pgg");
		$filename="../../foto_pegawai/".$get_photo['PHOTO'].".JPG";
		if (file_exists($filename)) {
			$photo = $filename;
		} else {
			$photo = 'includes/images/unknown.png';
		}
		$smarty->assign('PHOTO', $photo);

		$gol_pegawai=getData("select sg.id_sejarah_golongan, sg.id_golongan, upper(gol.nm_golongan) as nm_golongan, sg.no_sk_sejarah_golongan, sg.asal_sk_sejarah_golongan,
		sg.keterangan_sk_sejarah_golongan, TO_CHAR(sg.tmt_sejarah_golongan, 'DD-MM-YYYY') as tmt_sejarah_golongan, sg.status_akhir
		from sejarah_golongan sg
		left join pengguna pgg on pgg.id_pengguna=sg.id_pengguna
		left join golongan gol on gol.id_golongan=sg.id_golongan
		where pgg.id_pengguna=$id_pgg order by sg.status_akhir desc, sg.tmt_sejarah_golongan desc");
		$smarty->assign('GOL', $gol_pegawai);

		$pdd_pegawai=getData("select pdd.*, to_date(pdd.tahun_lulus_pendidikan,'YYYY') as tahun_lulus, pda.nama_pendidikan_akhir
		from sejarah_pendidikan pdd
		left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
		left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
		where pgg.id_pengguna=$id_pgg order by status_akhir desc, tahun_lulus_pendidikan desc");
		$smarty->assign('PEND', $pdd_pegawai);
		
		$id_gol=getData("select id_golongan, upper(nm_golongan) as nm_golongan from golongan order by nm_golongan desc");
		$smarty->assign('ID_GOL', $id_gol);

		$id_pdd=getData("select urut, id_pendidikan_akhir, nama_pendidikan_akhir
		from (
		select id_pendidikan_akhir, nama_pendidikan_akhir,
		case when id_pendidikan_akhir=2 then 11 
		when id_pendidikan_akhir=1 then 12
		when id_pendidikan_akhir=10 then 13  
		when id_pendidikan_akhir=9 then 10 
		when id_pendidikan_akhir=8 then 9
		when id_pendidikan_akhir=7 then 8   
		when id_pendidikan_akhir=3 then 7  
		when id_pendidikan_akhir=4 then 6 
		when id_pendidikan_akhir=5 then 5 
		when id_pendidikan_akhir=6 then 4 
		when id_pendidikan_akhir=13 then 3 
		when id_pendidikan_akhir=12 then 2 
		when id_pendidikan_akhir=11 then 1 
		else 0 end as urut
		from pendidikan_akhir)
		order by urut desc");
		$smarty->assign('ID_PDD', $id_pdd);
}
}

if ($request_method == 'POST')
{
if (post('action') == 'update_status_pdd')
{
	$id_pendidikan = post('id_pendidikan');
	
		$id_pengguna=getvar("select distinct id_pengguna from sejarah_pendidikan where id_sejarah_pendidikan=$id_pendidikan");
		UpdateData("update sejarah_pendidikan set status_akhir=0 where id_pengguna='$id_pengguna[ID_PENGGUNA]'");
		UpdateData("update sejarah_pendidikan set status_akhir=1 where id_sejarah_pendidikan=$id_pendidikan");
		
		echo "1";
		exit();
}

if (post('action') == 'delete_pdd')
{
	$id_pendidikan = post('hapus');
	
		deleteData("delete from sejarah_pendidikan where id_sejarah_pendidikan=$id_pendidikan");
		
		echo "1";
		exit();
}

if (post('action') == 'edit_pdd')
{
	$id_sej = post('id_sej');
	$id_pdd = post('id_pdd');
	$nm_skolah = post('nm_skolah');
	$jur_skolah = post('jur_skolah');
	$tpt_skolah = post('tpt_skolah');
	$lls_skolah = post('pddYear');
	
		UpdateData("update sejarah_pendidikan set id_pendidikan_akhir=$id_pdd, nm_sekolah_pendidikan=trim(upper('".$nm_skolah."')), nm_jurusan_pendidikan=trim(upper('".$jur_skolah."')), 
		tempat_pendidikan=trim(upper('".$nm_skolah."')), tahun_lulus_pendidikan='".$lls_skolah."'
		where id_sejarah_pendidikan=$id_sej");
		
		echo '<script>location.href="javascript:history.go(-1)";</script>';
		exit();
}

if (post('action') == 'update_status_gol')
{
	$id_golongan = post('id_golongan');
	
		$id_pengguna=getvar("select distinct id_pengguna from sejarah_golongan where id_sejarah_golongan=$id_golongan");
		UpdateData("update sejarah_golongan set status_akhir=0 where id_pengguna='$id_pengguna[ID_PENGGUNA]'");
		UpdateData("update sejarah_golongan set status_akhir=1 where id_sejarah_golongan=$id_golongan");
		
		$golongan=getvar("select id_golongan from sejarah_golongan where id_sejarah_golongan=$id_golongan");
		UpdateData("update pegawai set id_golongan='$golongan[ID_GOLONGAN]' where id_pengguna='$id_pengguna[ID_PENGGUNA]'");
		
		echo "1";
		exit();
}

if (post('action') == 'delete_gol')
{
	$id_golongan = post('hapus');
	
		deleteData("delete from sejarah_golongan where id_sejarah_golongan=$id_golongan");
		
		echo "1";
		exit();
}

if (post('action') == 'edit_gol')
{
	$id_sej = post('id_sej');
	$id_gol = post('id_gol');
	$no_sk = post('no_sk');
	$asal_sk = post('asal_sk');
	$ket_sk = post('ket_sk');
	$tmt_sej = post('tmt_sej');
	
		UpdateData("update sejarah_golongan set id_golongan=$id_gol, no_sk_sejarah_golongan=trim(upper('".$no_sk."')), asal_sk_sejarah_golongan=trim(upper('".$asal_sk."')), 
		keterangan_sk_sejarah_golongan=trim(upper('".$ket_sk."')), tmt_sejarah_golongan=to_date('".$tmt_sej."','dd-mm-yyyy')
		where id_sejarah_golongan=$id_sej");

		echo '<script>location.href="javascript:history.go(-1)";</script>';
		exit();
}
}

$smarty->display('karyawan_detail.tpl');

?>