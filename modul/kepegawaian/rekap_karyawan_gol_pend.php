<?php
//Yudi Sulistya, 23/04/2012

error_reporting (E_ALL & ~E_NOTICE);

include ('common.php');
require_once ('ociFunction.php');

$id_pengguna= $user->ID_PENGGUNA; 
$id_fak=$user->ID_FAKULTAS;

		$fak=getData("select 'FAKULTAS '||upper(fak.nm_fakultas) as fakultas
		from fakultas fak
		where fak.id_fakultas=$id_fak");
		$smarty->assign('FAK', $fak);

		$ttl=getvar("select sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD) as total
						from
						(
						select 
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='D4' then 1 else 0 end as D4,
						case when pda.nama_pendidikan_akhir='D3' then 1 else 0 end as D3,
						case when pda.nama_pendidikan_akhir='D2' then 1 else 0 end as D2,
						case when pda.nama_pendidikan_akhir='D1' then 1 else 0 end as D1,
						case when pda.nama_pendidikan_akhir='SMA' then 1 else 0 end as SLTA,
						case when pda.nama_pendidikan_akhir='SMP' then 1 else 0 end as SLTP,
						case when pda.nama_pendidikan_akhir='SD' then 1 else 0 end as SD
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join pegawai peg on peg.id_pengguna=pdd.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where id_unit_kerja in
						(select id_unit_kerja from unit_kerja where id_fakultas=$id_fak)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select upper(golongan) as golongan, sum(S3) as S3, sum(S2) as S2, sum(S1) as S1, sum(D4) as D4, sum(D3) as D3, sum(D2) as D2, 
						sum(D1) as D1, sum(SLTA) as SLTA, sum(SLTP) as SLTP, sum(SD) as SD, sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD) as total
						from
						(
						select case when peg.id_golongan is not null then gol.nm_golongan else '-' end as golongan,
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='D4' then 1 else 0 end as D4,
						case when pda.nama_pendidikan_akhir='D3' then 1 else 0 end as D3,
						case when pda.nama_pendidikan_akhir='D2' then 1 else 0 end as D2,
						case when pda.nama_pendidikan_akhir='D1' then 1 else 0 end as D1,
						case when pda.nama_pendidikan_akhir='SMA' then 1 else 0 end as SLTA,
						case when pda.nama_pendidikan_akhir='SMP' then 1 else 0 end as SLTP,
						case when pda.nama_pendidikan_akhir='SD' then 1 else 0 end as SD
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join pegawai peg on peg.id_pengguna=pdd.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where id_unit_kerja in
						(select id_unit_kerja from unit_kerja where id_fakultas=$id_fak)
						)
						group by id_pengguna
						)
						)
						group by golongan
						order by golongan");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(S3) as S3, sum(S2) as S2, sum(S1) as S1, sum(D4) as D4, sum(D3) as D3, sum(D2) as D2, 
						sum(D1) as D1, sum(SLTA) as SLTA, sum(SLTP) as SLTP, sum(SD) as SD, sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD) as total
						from
						(
						select 
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='D4' then 1 else 0 end as D4,
						case when pda.nama_pendidikan_akhir='D3' then 1 else 0 end as D3,
						case when pda.nama_pendidikan_akhir='D2' then 1 else 0 end as D2,
						case when pda.nama_pendidikan_akhir='D1' then 1 else 0 end as D1,
						case when pda.nama_pendidikan_akhir='SMA' then 1 else 0 end as SLTA,
						case when pda.nama_pendidikan_akhir='SMP' then 1 else 0 end as SLTP,
						case when pda.nama_pendidikan_akhir='SD' then 1 else 0 end as SD
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join pegawai peg on peg.id_pengguna=pdd.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where id_unit_kerja in
						(select id_unit_kerja from unit_kerja where id_fakultas=$id_fak)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('JML', $jml);

		$psn=getData("select case when sum(S3)=0 then 0 else round((sum(S3)/sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD)*100),2) end as S3,
						case when sum(S2)=0 then 0 else round((sum(S2)/sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD)*100),2) end as S2,
						case when sum(S1)=0 then 0 else round((sum(S1)/sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD)*100),2) end as S1,
						case when sum(D4)=0 then 0 else round((sum(D4)/sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD)*100),2) end as D4,
						case when sum(D3)=0 then 0 else round((sum(D3)/sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD)*100),2) end as D3,
						case when sum(D2)=0 then 0 else round((sum(D2)/sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD)*100),2) end as D2,
						case when sum(D1)=0 then 0 else round((sum(D1)/sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD)*100),2) end as D1,
						case when sum(SLTA)=0 then 0 else round((sum(SLTA)/sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD)*100),2) end as SLTA,
						case when sum(SLTP)=0 then 0 else round((sum(SLTP)/sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD)*100),2) end as SLTP,
						case when sum(SD)=0 then 0 else round((sum(SD)/sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD)*100),2) end as SD,
						round((sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD)/sum(S3+S2+S1+D4+D3+D2+D1+SLTA+SLTP+SD)*100),2) as persen
						from
						(
						select 
						case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
						case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
						case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1,
						case when pda.nama_pendidikan_akhir='D4' then 1 else 0 end as D4,
						case when pda.nama_pendidikan_akhir='D3' then 1 else 0 end as D3,
						case when pda.nama_pendidikan_akhir='D2' then 1 else 0 end as D2,
						case when pda.nama_pendidikan_akhir='D1' then 1 else 0 end as D1,
						case when pda.nama_pendidikan_akhir='SMA' then 1 else 0 end as SLTA,
						case when pda.nama_pendidikan_akhir='SMP' then 1 else 0 end as SLTP,
						case when pda.nama_pendidikan_akhir='SD' then 1 else 0 end as SD
						from sejarah_pendidikan pdd
						left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
						left join pegawai peg on peg.id_pengguna=pdd.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						where pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
						(
						select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where id_unit_kerja in
						(select id_unit_kerja from unit_kerja where id_fakultas=$id_fak)
						)
						group by id_pengguna
						)
						)");
		$smarty->assign('PSN', $psn);

$smarty->display('rekap_karyawan_gol_pend.tpl');

?>