<?php

include('conf.php');

$mode = get('mode', 'view');
$id_fakultas = get('id_fakultas', 0);
$id_program_studi = get('id_program_studi', 0);
$thn_angkatan_mhs = get("thn_angkatan_mhs", 0);

// Fikrie //
$id_pt = $id_pt_user;

if ($request_method == 'GET' or $request_method == 'POST') {
    //$pimpinan_set = array(1, 2, 3, 4, 30, 31, 32, 33, 53, 81);

    // id_jabatan_pegawai diganti berdasarkan kode_jabatan_pegawai
    // Fikrie //
    $pimpinan_set = $db->QueryToArray("select id_jabatan_pegawai from jabatan_pegawai where kode_jabatan_pegawai in ('REKTOR', 'WAREK1', 'WAREK2', 'WAREK3');");

    if (in_array($user->ID_JABATAN_PEGAWAI, $pimpinan_set)) {
        $smarty->assign('fakultas_set', $db->QueryToArray("select id_fakultas, nm_fakultas from fakultas where id_perguruan_tinggi = {$id_pt}"));
    } else {
        $smarty->assign('fakultas_set', $db->QueryToArray("select id_fakultas, nm_fakultas from fakultas where id_fakultas = {$user->ID_FAKULTAS}"));
    }

    if ($id_fakultas) {
        $smarty->assign('program_studi_set', $db->QueryToArray("
                select id_program_studi, nm_jenjang, nm_program_studi from program_studi ps
                join jenjang j on j.id_jenjang = ps.id_jenjang
                where ps.id_fakultas = {$id_fakultas} and ps.id_program_studi <> 197
                order by j.id_jenjang"));
    }

    $smarty->assign('tahun_set', $db->QueryToArray("
            select distinct thn_angkatan_mhs from mahasiswa 
            where thn_angkatan_mhs>2000 and thn_angkatan_mhs <= to_char(sysdate,'YYYY')
            order by thn_angkatan_mhs desc"));

    if ($mode == 'advance-search') {
        // FILTER SEARCH
        $nama = get('nama');
        $fakultas = get('id_fakultas');
        $prodi = get('id_program_studi');
        $angkatan = get('thn_angkatan_mhs');
        $kota = get('kota');
        $sma = get('sma');
        $hp = get('hp');
        $range_bawah = get('range_bawah');
        $range_atas = get('range_atas');
        if ($nama != '') {
            $filter_nama = "AND UPPER(P.NM_PENGGUNA) LIKE UPPER('%{$nama}%')";
        }
        if ($fakultas != '') {
            $filter_fakultas = "AND F.ID_FAKULTAS='{$fakultas}'";
        }
        if ($prodi != '') {
            $filter_prodi = "AND PS.ID_PROGRAM_STUDI='{$prodi}'";
        }
        if ($angkatan != '') {
            $filter_angkatan = "AND M.THN_ANGKATAN_MHS='{$thn_angkatan_mhs}'";
        }
        if ($kota != '') {
            $filter_kota = "AND UPPER(KA.NM_KOTA) LIKE UPPER('%{$kota}%')";
        }
        if ($sma != '') {
            $filter_sma = "AND (UPPER(S.NM_SEKOLAH) LIKE UPPER('%{$sma}%') OR UPPER(SC.NM_SEKOLAH) LIKE UPPER('%{$sma}%'))";
        }
        if ($hp != '') {
            $filter_hp = "AND M.MOBILE_MHS LIKE '%{$hp}%'";
        }
        if ($range_bawah != '' && $range_atas != '') {
            $filter_penghasilan = "AND (M.PENGHASILAN_ORTU_MHS BETWEEN {$range_bawah} AND {$range_atas} OR CMO.TOTAL_PENDAPATAN_ORTU BETWEEN {$range_bawah} AND {$range_atas} )";
        }

        $query_find = "SELECT M.ID_MHS,M.MOBILE_MHS,M.NIM_MHS,P.USERNAME,P.NM_PENGGUNA,SP.NM_STATUS_PENGGUNA,SP.STATUS_AKTIF,SP.ID_STATUS_PENGGUNA
            FROM AUCC.MAHASISWA M
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN AUCC.STATUS_PENGGUNA SP on SP.ID_STATUS_PENGGUNA= M.STATUS_AKADEMIK_MHS
            LEFT JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            LEFT JOIN AUCC.FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            LEFT JOIN AUCC.KOTA KA ON KA.ID_KOTA=M.ASAL_KOTA_MHS
            LEFT JOIN AUCC.SEKOLAH S ON S.ID_SEKOLAH = M.ID_SEKOLAH_ASAL_MHS
            LEFT JOIN AUCC.CALON_MAHASISWA_BARU CMB ON CMB.NIM_MHS=M.NIM_MHS
            LEFT JOIN AUCC.CALON_MAHASISWA_SEKOLAH CMS ON CMS.ID_C_MHS=CMB.ID_C_MHS
            LEFT JOIN AUCC.SEKOLAH SC ON SC.ID_SEKOLAH=CMS.ID_SEKOLAH_ASAL
            LEFT JOIN AUCC.CALON_MAHASISWA_ORTU CMO ON CMO.ID_C_MHS=CMB.ID_C_MHS
            WHERE SP.ID_STATUS_PENGGUNA IS NOT NULL AND F.ID_PERGURUAN_TINGGI = {$id_pt}
            {$filter_nama}
            {$filter_fakultas}
            {$filter_prodi}
            {$filter_angkatan}
            {$filter_kota}
            {$filter_sma}
            {$filter_hp}
            {$filter_penghasilan}";
        $mahasiswa_set = $db->QueryToArray($query_find);

        $smarty->assign('mahasiswa_set', $mahasiswa_set);
    } elseif ($mode == 'get-prodi') {
        $prodi_set = $db->QueryToArray("
            select id_program_studi, nm_jenjang, nm_program_studi from program_studi ps
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where ps.id_fakultas = {$id_fakultas} and ps.id_program_studi <> 197
            order by j.id_jenjang");

        echo "<option value=\"\">-- Pilih Program Studi --</option>";

        foreach ($prodi_set as $ps) {
            echo "<option value=\"{$ps['ID_PROGRAM_STUDI']}\">[{$ps['NM_JENJANG']}] {$ps['NM_PROGRAM_STUDI']}</option>";
        }

       // exit();
    }
}

$smarty->display("sample/pimpinan/rekap-pencarian.tpl");
?>