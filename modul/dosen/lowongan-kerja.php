<?php
include 'config.php';
include 'proses/presensi.class.php';
include 'proses/penilaian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$smarty->display('../mhs/gui/lowongan-kerja.tpl');

?>