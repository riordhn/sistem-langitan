<?php
include 'config.php';
include "../../includes/FusionCharts.php";
	
// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }	
	
$id_fakultas = $user->ID_FAKULTAS;
?>

<div class="center_title_bar">IPK MAHASISWA < 2</div>
 <div id="chartdiv" align="center"> 
        FusionCharts. 
 </div>
	<div id="chartdivipkd3" align="center"> 
        FusionCharts. 
 </div>
       <?php
        if($id_fakultas=='8'){
      ?>
      <script type="text/javascript">
		   var chart = new FusionCharts("../../../swf/Charts/Pie2D.swf",  "chartdiv", "800", "400", "0", "0");
		   chart.setDataURL($base_url."modul/dosen/xml/ipk.php");		   
		   chart.render("chartdiv");
		   var chart = new FusionCharts("../../../swf/Charts/Pie2D.swf",  "chartdivipkd3", "800", "400", "0", "0");
		   chart.setDataURL($base_url."modul/dosen/xml/ipkd3.php");		   
		   chart.render("chartdivipkd3");
      </script>
      <?php
        }
        else{
      ?>
      <script type="text/javascript">
		   var chart = new FusionCharts("../../swf/Charts/Pie2D.swf", "chartdiv", "800", "400", "0", "0");
		   chart.setDataURL($base_url."modul/dosen/xml/ipk.php");		   
		   chart.render("chartdiv");
		   var chart = new FusionCharts("../../swf/Charts/Pie2D.swf",  "chartdivipkd3", "800", "400", "0", "0");
		   chart.setDataURL($base_url."modul/dosen/xml/ipkd3.php");		   
		   chart.render("chartdivipkd3");
      </script>
      <?php
        }
      ?>
