<?php
include 'create-data-xml.php';
?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<div id="peta_sebaran" style="margin-bottom: 20px">
	<center>
		<div class="center_title_bar center">Sebaran Wilayah Asal Mahasiswa Baru <?php echo date('Y') ?></div>
		<div id="map-sebaran" style="background-color: #CACFD3;padding: 10px;width: 90%">
			<div id="map-title">
			</div>
			<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="98%" height="480" id="FlashID" title="map">
				<param name="movie" value="PETA SEBARAN_BARU.swf" />
				<param name="quality" value="high" />
				<param name="wmode" value="opaque" />
				<param name="swfversion" value="7.0.70.0" />
				<!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
				<param name="expressinstall" value="Scripts/expressInstall.swf" />
				<!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
				<!--[if !IE]>-->
				<object type="application/x-shockwave-flash" data="PETA SEBARAN_BARU.swf" width="98%" height="480">
					<!--<![endif]-->
					<param name="quality" value="high" />
					<param name="wmode" value="opaque" />
					<param name="swfversion" value="7.0.70.0" />
					<param name="expressinstall" value="Scripts/expressInstall.swf" />
					<!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
					<div>
						<h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
						<p>
							<a href="http://www.adobe.com/go/getflashplayer">
								<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
							</a>
						</p>
					</div>
					<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
			</object>
		</div>
	</center>
</div>
