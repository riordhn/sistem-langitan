<?php
include 'config.php';
include '../../includes/FusionCharts.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$db->Query("SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE SEMESTER.STATUS_AKTIF_SEMESTER = 'True'");
$semester = $db->FetchAssoc();

$strXML .= "<chart palette='4' decimals='0' enableSmartLabels='1' enableRotation='0' bgColor='FFFFFF,FFFFFF' bgAlpha='100,100' bgRatio='100,100' bgAngle='360' showBorder='1' startingAngle='70' caption='MAHASISWA DO TAHUN $semester[THN_AKADEMIK_SEMESTER]'>";

$strquery =  $db->QueryToArray("SELECT FAKULTAS.ID_FAKULTAS, NM_FAKULTAS, COUNT(*) JUMLAH FROM MAHASISWA 
JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
JOIN ADMISI ON ADMISI.ID_MHS = MAHASISWA.ID_MHS
WHERE (STATUS_AKADEMIK_MHS = 6 OR STATUS_AKADEMIK_MHS = 20) AND TO_CHAR(TGL_SK, 'YYYY') = 
(SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE SEMESTER.STATUS_AKTIF_SEMESTER = 'True')
GROUP BY FAKULTAS.ID_FAKULTAS, NM_FAKULTAS
ORDER BY FAKULTAS.ID_FAKULTAS");

foreach($strquery as $r){
$strXML .= "<set label='$r[NM_FAKULTAS]' value='$r[JUMLAH]'/>";
}

$strXML .= "</chart>";

if ($user->ID_FAKULTAS != 8)
	$tes = renderChart("../../swf/Charts/Pie2D.swf", "", $strXML, "idchart", "500", "500", "0", "0");
else
	$tes = renderChart("../../../swf/Charts/Pie2D.swf", "", $strXML, "idchart", "500", "500", "0", "0");
$smarty->assign('tes', $tes);


$db->Query("SELECT COUNT(*) JUMLAH FROM MAHASISWA 
JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
JOIN ADMISI ON ADMISI.ID_MHS = MAHASISWA.ID_MHS
WHERE (STATUS_AKADEMIK_MHS = 6 OR STATUS_AKADEMIK_MHS = 20) AND TO_CHAR(TGL_SK, 'YYYY') = 
(SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE SEMESTER.STATUS_AKTIF_SEMESTER = 'True')");
$total = $db->FetchAssoc();
$smarty->assign('total', $total['JUMLAH']);

$smarty->display('sample/dashboard/dashboard-do.tpl');
?>