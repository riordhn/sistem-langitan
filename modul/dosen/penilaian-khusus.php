<?php

include 'config.php';
include 'proses/penilaian.class.php';
include 'proses/komponen_nilai.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}

$penilaian = new penilaian($db, $login->id_dosen);
$komponen_nilai = new komponen_nilai($db, $login->id_dosen);
$data_kelas_mk = $penilaian->load_kelas_mata_kuliah_khusus();

if (isset($_GET)) {
    if (get('mode') == 'load_mahasiswa') {
        $kode_kelas = explode('_', get('id_kelas'));
        $id_kelas_mk = $kode_kelas[0];
        $id_semester = $kode_kelas[1];
        $detail_mk = $penilaian->get_kelas_mk($id_kelas_mk);
        if (isset($_POST)) {
            if (post('mode') == 'save-nilai') {
                if (post('jumlah_komponen') == 0) {
                    // Update Nilai Tanpa Komponen
                    for ($i = 1; $i < post('total_nilai'); $i++) {
                        $penilaian->update_nilai_khusus(post('nilai' . $i), post('id_pmk' . $i));
                    }
                } else {
                    // Update Nilai dengan Komponen
                    if (post('tipe_penilaian') == 'up') {
                        // Khusus Untuk penilaian UP
                        for ($i = 1; $i < post('total_nilai'); $i++) {
                            $penilaian->update_nilai_up(post('nilai' . $i), post('id_nilai' . $i));
                        }
                        $penilaian->proses_nilai_akhir_up($id_kelas_mk);
                    } else {
                        for ($i = 1; $i < post('total_nilai'); $i++) {
                            $penilaian->update_nilai(post('nilai' . $i), post('id_nilai' . $i));
                        }
                        $penilaian->proses_nilai_akhir_sp($id_kelas_mk);
                    }
                }
            } else if (post('mode') == 'tampil') {
                $db->Query("UPDATE PENGAMBILAN_MK SET FLAGNILAI=1 WHERE ID_SEMESTER='{$id_semester}' AND ID_KELAS_MK='{$id_kelas_mk}'");
            } else if (post('mode') == 'tambah-komponen') {
                $nama = post('nama') != '' ? post('nama') : post('nama_utama');
                $komponen_nilai->insert_komponen($nama, post('persentase'), post('urutan'), post('id_kelas'));
            } else if (post('mode') == 'edit-komponen') {
                $komponen_nilai->update_komponen(post('id_komponen'), post('urutan'), post('nama'), post('persentase'));
            } else if (post('mode') == 'delete-komponen') {
                $komponen_nilai->delete_komponen(post('id_komponen'));
            } else if (post('mode') == 'tambah_akses') {
                $penilaian->tambah_akses_komponen(post('id_dosen'), $id_kelas_mk, post('id_komponen_mk'), post('id_semester'));
            } else if (post('mode') == 'delete_akses') {
                $penilaian->delete_akses_komponen(post('id_akses'));
            }
            // Komponen UP
            else if (post('mode') == 'tambah-komponen-up') {
                $nama = post('nama') != '' ? post('nama') : post('nama_utama');
                $komponen_nilai->insert_komponen_up($nama, post('persentase'), post('id_kelas'), $id_semester);
            } else if (post('mode') == 'edit-komponen-up') {
                $komponen_nilai->update_komponen_up(post('id_komponen'), post('nama'), post('persentase'));
            } else if (post('mode') == 'delete-komponen-up') {
                $komponen_nilai->delete_komponen_up(post('id_komponen'));
            }
        }
        // Cek Semester
        $cek_sem_sp = $db->QuerySingle("SELECT COUNT(*) FROM SEMESTER WHERE ID_SEMESTER='{$id_semester}' AND TIPE_SEMESTER='SP'");
        $cek_sem_up = $db->QuerySingle("SELECT COUNT(*) FROM SEMESTER WHERE ID_SEMESTER='{$id_semester}' AND TIPE_SEMESTER='UP'");
        if ($cek_sem_sp > 0) {
            // Semester Aktif SP
            $sem_aktif_sp = $db->QuerySingle("SELECT ID_SEMESTER FROM AUCC.SEMESTER WHERE STATUS_AKTIF_SP='True'");
            // Cek Edit KRS Akademik dan Benahi Tampilan Nilai
            $penilaian->benahi_tampilan_nilai_krs_akad($id_kelas_mk);
            $smarty->assign('cek_sem_sp_aktif', $sem_aktif_sp == $id_semester ? TRUE : FALSE);
            $smarty->assign('data_kelas_mk_detail', $detail_mk);
            $smarty->assign('status_pjmk', $penilaian->get_pjmk_status($id_kelas_mk));
            $smarty->assign('data_akses_dosen_mk', $penilaian->load_akses_komponen($id_kelas_mk));
            $smarty->assign('data_dosen_mk', $penilaian->load_dosen_mk($id_kelas_mk));
            $smarty->assign('info_pjma', alert_success("Anda mempunyai Hak Akses Sebagai PJMA", '100'));
            $smarty->assign('info', alert_success("Jika ingin memasukkan nilai akhir langsung adanya tanpa komponen, silahkan kosongkan komponen nilai", '100'));
            $smarty->assign('data_komponen_sp', $penilaian->load_komponen_mk($id_kelas_mk));
        }
        if ($cek_sem_up > 0) {
            $penilaian->benahi_tampilan_nilai_krs_akad_up($id_kelas_mk, $id_semester);
            $smarty->assign('status_pjmk', $penilaian->get_pjmk_status($id_kelas_mk));
            $smarty->assign('data_komponen_up', $komponen_nilai->load_komponen_mk_up($id_kelas_mk));
        }
        $smarty->assign('semester', $penilaian->get_semester_kelas_khusus($id_semester));
        $smarty->assign('pjmk_status', $penilaian->get_pjmk_status($id_kelas_mk));
        $smarty->assign('data_mahasiswa', $penilaian->load_data_mahasiswa_khusus(get('id_kelas')));
    }
}

$smarty->assign('id_fakultas', $login->id_fakultas);
$smarty->assign('data_kelas_mk', $data_kelas_mk);
$smarty->display('display-penilaian/penilaian-khusus.tpl');
?>
