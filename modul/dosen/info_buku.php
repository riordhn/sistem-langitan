<?php
include 'config.php';
include 'conf_perpus.php';
include 'proses/info_buku.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$info_buku = new info_buku($db_mysql);
$mode = get('mode', 'view');

if ($mode == 'view' && get('kunci') != '') {
    $smarty->assign('data_hasil_cari', $info_buku->load_pencarian_buku(get('kriteria'), get('kunci'), get('tempat')));
} else if ($mode == 'detail') {
    $smarty->assign('data_detail_buku', $info_buku->get_pencarian_buku(get('reg')));
}


$smarty->display("sample/pustaka/{$mode}_info_buku.tpl");
?>