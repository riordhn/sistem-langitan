<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$smarty->assign('TITLE', 'Jadwal Ujian');

$smarty->display('sample/ujian/jadwal_ujian.tpl');
?>