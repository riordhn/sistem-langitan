<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$smarty->assign('TITLE', 'Input Nilai');

$smarty->display('sample/penilaian/nilai/tugas.tpl');
?>