<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$smarty->assign('TITLE', 'Perbaikan Nilai');

$smarty->display('sample/penilaian/perbaikan_nilai.tpl');
?>