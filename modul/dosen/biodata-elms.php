<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_DOSEN){
	header("location: /logout.php");
    exit();
}


if ($user->IsLogged() && $user->Role() == AUCC_ROLE_DOSEN) {
	/*
	$kueri = "select nim_mhs from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'";
	$nim_mhs="";
	$result = $db->Query($kueri)or die("salah kueri 42 : ");
	while($r = $db->FetchRow()) {
		$nim_mhs = $r[0];
	}
	$in_jam = date("H");
	$in_menit = date("i");
	$in_detik = date("s");
	$waktu = $in_jam.$in_menit.$in_detik;

	$kode = md5("bam".$nim_mhs."bang".$waktu);
	$kode2 = substr($kode,0,2).$in_jam.substr($kode,2,14).$in_menit.substr($kode,16,8).$in_detik.substr($kode,24);

	$isi_blog = '
	<br>
	<u onclick="window.open(\'http://web.unair.ac.id/checkLogin.php?loginid='.$nim_mhs.'&kode='.$kode2.'\')"><font color="blue">Menuju ke manajemen blog</font></u>
	';

	$smarty->assign('isi_blog', $isi_blog);

	$smarty->display('blog-mhs.tpl');
	*/

	if(isset($_GET["proses"])) {
		if($_GET["proses"]=='proses_daftar') {
			function RandomString(){
				$tmpkar = "0123456789abcdefghijklmnopqrstuvwxyz";
				$acakkar = '';
				for ($i=1; $i<=10; $i++) {
					$acakkar .= substr($tmpkar, rand(0,35) ,1);
				}
				return $acakkar;
			}
			function KirimCurl($tujuan, $paramkirim) {
				$ch = curl_init($tujuan);
				curl_setopt ($ch, CURLOPT_POST, 1);
				curl_setopt ($ch, CURLOPT_POSTFIELDS, $paramkirim);
				curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt ($ch, CURLOPT_COOKIESESSION, true);
				$returndata = curl_exec ($ch);
				//echo curl_error($ch);
				curl_close ($ch);

				return $returndata;
			}

			$kueri2 = "select email_pengguna from aucc.pengguna where id_pengguna=".$user->ID_PENGGUNA."";
			$result = $db->Query($kueri2)or die("...");
			$r2 = $db->FetchRow();
			$emailnya = $r2[0];

			$psemen = RandomString();
			$query = "update aucc.pengguna set SE1='".sha1($psemen)."' where id_pengguna=".$user->ID_PENGGUNA." ";
			$result = $db->Query($query)or die("...");
			if($result) {
				$pesan = '<br>Kode Konfirmasi : '.$psemen;
			
				$param = "kepada_elms=".$emailnya."&pesan_elms=".$pesan;

				$isi = KirimCurl("http://ppm.unair.ac.id/audit/tmp/satu.php", $param);

				if($isi == 'OK') {
					echo 'Kode konfirmasi berhasil dikirim ke email : '.$emailnya."\n".'Silahkan buka email anda untuk melihat kode konfirmasi.';
				}

			}else{
				echo 'Kode konfirmasi gagal dikirim ke email : '.$emailnya.'';
			}
		}else if($_GET["proses"]=='proses_batal') {
			$kueri = "update aucc.pengguna set SE1=null where id_pengguna=".$user->ID_PENGGUNA;
			$result = $db->Query($kueri)or die("...");
			$kueri = "delete from aucc.elms_daftar where id_pengguna=".$user->ID_PENGGUNA;
			$result = $db->Query($kueri)or die("...");
			if($result) {
				echo 'Proses berhasil';
			}else{
				echo 'Proses gagal';
			}
		}else if($_GET["proses"]=='proses_konfirmasi' and isset($_GET["kode"])) {
			$dikode = $_GET["kode"];
			$query = "SELECT se1 from aucc.pengguna where id_pengguna=".$user->ID_PENGGUNA."";
			$result = $db->Query($query)or die("...");
			$r = $db->FetchRow();
			$db_kode = trim($r[0]);

			if(sha1($dikode) != $db_kode) {
				echo 'Kode konfirmasi tidak benar';
			}else{
				$kueri = "update aucc.pengguna set SE1=null where id_pengguna=".$user->ID_PENGGUNA;
				$result = $db->Query($kueri)or die("...");
				$kueri = "insert into aucc.elms_daftar (id_pengguna, tgl_daftar) values (".$user->ID_PENGGUNA.", TO_DATE('".date("YmdHis")."', 'YYYYMMDDHH24MISS') )";
				//echo $kueri;
				$result = $db->Query($kueri)or die("...");
				if($result) {
					echo 'Proses berhasil';
				}else{
					echo 'Proses gagal';
				}
			}
		}
	}else{
	
	
		$isi_blog = '';
		$isi_blog .= "
			<script type=\"text/javascript\">
				function proses_daftar2(){
					var kode_konf = $('#kode_konfirmasi').val();
					$.get('./biodata-elms.php?proses=proses_konfirmasi&kode='+kode_konf, function(data) {
						alert(data);
						if(data=='Proses berhasil'){
							location.reload();
						}
					});
				}
				function proses_daftar(){
					$.get('./biodata-elms.php?proses=proses_daftar', function(data) {
						alert(data);
						location.reload();
					});
				}
				function proses_batal(){
					$.get('./biodata-elms.php?proses=proses_batal', function(data) {
						alert(data);
						if(data=='Proses berhasil'){
							location.reload();
						}
					});
				}
			</script>
		";
		// cek apakah sudah ada email unair, di tabel pengguna
		$email_pengguna = ''; $email_konfirmasi = '';
		$kueri = "select count(*) from aucc.pengguna where id_pengguna=".$user->ID_PENGGUNA." and email_pengguna is not null";
		$result = $db->Query($kueri)or die("...");
		$r = $db->FetchRow();
		if($r[0] > 0) {
			$kueri2 = "select email_pengguna, se1 from aucc.pengguna where id_pengguna=".$user->ID_PENGGUNA."";
			$result = $db->Query($kueri2)or die("...");
			$r2 = $db->FetchRow();
			$email_pengguna = $r2[0];
			$email_konfirmasi = $r2[1];
		}
		// cek apakah sudah ada email unair, di tabel email
		$email_email = '';
		$kueri = "select count(*) from aucc.email where id_pengguna=".$user->ID_PENGGUNA."";
		$result = $db->Query($kueri)or die("...");
		$r = $db->FetchRow();
		if($r[0] > 0) {
			$kueri2 = "select email from aucc.email where id_pengguna=".$user->ID_PENGGUNA."";
			$result = $db->Query($kueri2)or die("...");
			$r2 = $db->FetchRow();
			$email_email = $r2[0];
		}

		if($email_pengguna == '' or $email_email== ' ') {
			$isi_blog .= 'Hubungi helpdesk untuk membuat email';
		}else if($email_pengguna != $email_email) {
			$isi_blog .= 'email tidak valid, Hubungi helpdesk untuk membetulkan email';
		}else if($email_konfirmasi != '') {
			$isi_blog .= '
			Kode konfirmasi : <input type=text name="kode_konfirmasi" id="kode_konfirmasi">
			<input type=button name="okdaftar2" value="Proses Pendaftaran" onclick="proses_daftar2()">
			<input type=button name="okbatal" value="Batal Daftar" onclick="proses_batal()">
			';
		}else{
			// cek apakah sudah mendaftar
			$kueri = "select count(*) from aucc.elms_daftar where id_pengguna=".$user->ID_PENGGUNA."";
			$result = $db->Query($kueri)or die("...");
			$r = $db->FetchRow();
			if($r[0]==0) { // belum daftar
				$isi_blog .= '<br><input type=button name="okdaftar" value="Daftar" onclick="proses_daftar()">';
			}else{ // sudah daftar
				$isi_blog .= '<br><input type=button name="okbatal" value="Batal Daftar" onclick="proses_batal()">';
			}
		}
		$smarty->assign('isi_blog', $isi_blog);
		$smarty->display('biodata-elms.tpl');

	}

}else {
	$smarty->display('session-expired.tpl');
}

?>
