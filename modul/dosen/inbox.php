<?php
include 'config.php';
include 'proses/pesan.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$inbox = new pesan($db, $login->id_dosen, $login->id_pengguna);

if (isset($_POST['mode'])) {
    if ($_POST['mode'] == 'delete') {
        $inbox->delete_pesan_penerima($_POST['id']);
    } else if ($_POST['mode'] == 'delete_checked') {
        foreach ($_POST['id'] as $item) {
            $inbox->delete_pesan_penerima($item[0]);
        }
    }
}
$data_inbox = $inbox->load_inbox();
$smarty->assign('title', 'Inbox');
$smarty->assign('data_inbox', $data_inbox);
$smarty->display('display-konsultasi/inbox.tpl');
?>