<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RequestController
 *
 * @author sani
 */
class RequestController {
        public function saveRequest($string)
        {
            if(get_magic_quotes_gpc())  // prevents duplicate backslashes
            {
                $string = stripslashes($string);
            }
            if (phpversion() >= '4.3.0')
            {
                $string = mysql_real_escape_string($string);
            }
            else
            {
                $string = mysql_escape_string($string);
            }
            return $string;
        }
}

?>