<?php
include('conf.php');

$kd_fak= $user->ID_FAKULTAS;
$id_pengguna= $user->ID_PENGGUNA;
 
$status="select count(*) as pim from pengguna
			where id_pengguna=$id_pengguna and  $id_pengguna in (
			select id_dekan as pimfak from FAKULTAS where id_fakultas=$kd_fak
			union select id_wadek1 as pimfak  from FAKULTAS where id_fakultas=$kd_fak
			union select id_wadek2 as pimfak  from FAKULTAS where id_fakultas=$kd_fak
			union select id_wadek3 as pimfak  from FAKULTAS where id_fakultas=$kd_fak)";

$result = $db->Query($status)or die("salah kueri status");
	while($r = $db->FetchRow()) {
		$statuspim=$r[0];
	}
	
$smt="select id_semester,thn_akademik_semester,nm_semester from semester where status_aktif_semester='True'";

$result = $db->Query($smt)or die("salah kueri smt");
	while($r1 = $db->FetchRow()) {
		$id_sem_aktif=$r1[0];
		$tahun_smt=$r1[1];
		$nm_smt=$r1[2];
	}
	$smarty->assign('tahun', $tahun_smt);
	$smarty->assign('smt', $nm_smt);
	
if ($statuspim==1) {
	$smarty->assign('status', 'Fakultas');
} else {
$smarty->assign('status', 'Univeristas');
}

$action = $_GET['action'];
$smarty->assign('action', $action);
$jen = $_GET['jen'];


if ($statuspim==1) {
if ($action=='detail') {
$smarty->assign('lap_kur', $db->QueryToArray("
                select nm_fakultas,nm_jenjang,prodi,krs,ttlkelas,ttlnil,kur,ttlkur,
				case when ttlnil>0 and ttlkur>0 then 'Completed' else 
				case when ttlkur>0 then 'Kurikulum Sdh, ' else 'Kurikulum Blm, ' end ||
				case when ttlnil>0 then 'Histori Nilai Sdh, ' else 'Historis Nilai Blm' end end as ket1,
				case when ttlkelas>0 and krs>10 then 'Telah KRS' else 
				case when ttlkelas>0 then 'Jadwal Sdh, ' else 'Jadwal Blm, ' end||
				case when krs>10 then 'Sdh KRS' else 'Blm KRS' end end as ket
				from 
				(select nm_fakultas,nm_jenjang,nm_jenjang||'-'||nm_program_studi as prodi,id_program_studi from program_studi 
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang 
				left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
				where status_aktif_prodi=1 and program_studi.id_fakultas=$kd_fak)s1 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as krs from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester=$id_sem_aktif  
				group by mahasiswa.id_program_studi)s2 on s1.id_program_studi=s2.id_program_studi 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as ttlnil from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester !=$id_sem_aktif 
				group by mahasiswa.id_program_studi)s3 on s1.id_program_studi=s3.id_program_studi 
				left join ( 
				select id_program_studi,wm_concat(kur) as kur from (
				select kurikulum_mk.id_program_studi,kurikulum_mk.id_kurikulum_prodi,' '||tahun_kurikulum||'('||count(*)||')' as kur from kurikulum_mk
				left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
				group by kurikulum_mk.id_program_studi, kurikulum_mk.id_kurikulum_prodi, tahun_kurikulum)
				group by id_program_studi)s4 on s1.id_program_studi=s4.id_program_studi 
				left join (
				select kelas_mk.id_program_studi,count(*) as ttlkelas from kelas_mk 
				where id_semester=$id_sem_aktif
				group by kelas_mk.id_program_studi) s5 on s1.id_program_studi=s5.id_program_studi
				left join ( 
				select kurikulum_mk.id_program_studi,count(*) as ttlkur from kurikulum_mk
				group by kurikulum_mk.id_program_studi)s6 on s1.id_program_studi=s6.id_program_studi
				where nm_jenjang='$jen'
				order by nm_fakultas,prodi"));
}
if ($action=='viewperfakultas') {
$smarty->assign('prosen_kur', $db->QueryToArray("select nm_fakultas,nm_jenjang,count(nm_jenjang) as ttljenjang,
				sum(case when ttlnil>0 and ttlkur>0 then 1 else 0 end) as compled,
				round(sum(case when ttlnil>0 and ttlkur>0 then 1 else 0 end)/count(nm_jenjang)*100,2) as prosen,
				round(sum(case when ttlkelas>0 then 1 else 0 end)/count(nm_jenjang)*100,2) as prosen_siap,
				round(sum(case when krs>0 then 1 else 0 end)/count(nm_jenjang)*100,2) as prosen_krs	
				from 
				(select nm_fakultas,nm_jenjang,nm_jenjang||'-'||nm_program_studi as prodi,id_program_studi from program_studi 
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang 
				left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
				where status_aktif_prodi=1 and program_studi.id_fakultas=$kd_fak)s1 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as krs from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester=$id_sem_aktif  
				group by mahasiswa.id_program_studi)s2 on s1.id_program_studi=s2.id_program_studi 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as ttlnil from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester !=$id_sem_aktif  
				group by mahasiswa.id_program_studi)s3 on s1.id_program_studi=s3.id_program_studi 
				left join ( 
				select id_program_studi,wm_concat(kur) as kur from (
				select kurikulum_mk.id_program_studi,kurikulum_mk.id_kurikulum_prodi,tahun_kurikulum||'('||count(*)||')' as kur from kurikulum_mk
				left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
				group by kurikulum_mk.id_program_studi, kurikulum_mk.id_kurikulum_prodi, tahun_kurikulum)
				group by id_program_studi)s4 on s1.id_program_studi=s4.id_program_studi 
				left join (
				select kelas_mk.id_program_studi,count(*) as ttlkelas from kelas_mk 
				where id_semester=$id_sem_aktif
				group by kelas_mk.id_program_studi) s5 on s1.id_program_studi=s5.id_program_studi
				left join ( 
				select kurikulum_mk.id_program_studi,count(*) as ttlkur from kurikulum_mk
				group by kurikulum_mk.id_program_studi)s6 on s1.id_program_studi=s6.id_program_studi group by nm_fakultas, nm_jenjang 
				order by nm_fakultas,nm_jenjang"));
}
$smarty->assign('prosen_kur_univ', $db->QueryToArray(" select nm_jenjang,count(nm_jenjang) as ttljenjang,
				sum(case when ttlnil>0 and ttlkur>0 then 1 else 0 end) as compled,
				round(sum(case when ttlnil>0 and ttlkur>0 then 1 else 0 end)/count(nm_jenjang)*100,2) as prosen,
				round(sum(case when ttlkelas>0 then 1 else 0 end)/count(nm_jenjang)*100,2) as prosen_siap,
				round(sum(case when krs>0 then 1 else 0 end)/count(nm_jenjang)*100,2) as prosen_krs	
				from 
				(select nm_fakultas,nm_jenjang,nm_jenjang||'-'||nm_program_studi as prodi,id_program_studi from program_studi 
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang 
				left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
				where status_aktif_prodi=1 and program_studi.id_fakultas=$kd_fak)s1 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as krs from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester=$id_sem_aktif  
				group by mahasiswa.id_program_studi)s2 on s1.id_program_studi=s2.id_program_studi 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as ttlnil from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester !=$id_sem_aktif  
				group by mahasiswa.id_program_studi)s3 on s1.id_program_studi=s3.id_program_studi 
				left join ( 
				select id_program_studi,wm_concat(kur) as kur from (
				select kurikulum_mk.id_program_studi,kurikulum_mk.id_kurikulum_prodi,tahun_kurikulum||'('||count(*)||')' as kur from kurikulum_mk
				left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
				group by kurikulum_mk.id_program_studi, kurikulum_mk.id_kurikulum_prodi, tahun_kurikulum)
				group by id_program_studi)s4 on s1.id_program_studi=s4.id_program_studi 
				left join (
				select kelas_mk.id_program_studi,count(*) as ttlkelas from kelas_mk 
				where id_semester=$id_sem_aktif
				group by kelas_mk.id_program_studi) s5 on s1.id_program_studi=s5.id_program_studi
				left join ( 
				select kurikulum_mk.id_program_studi,count(*) as ttlkur from kurikulum_mk
				group by kurikulum_mk.id_program_studi)s6 on s1.id_program_studi=s6.id_program_studi 
				group by nm_jenjang 
				order by nm_jenjang"));
} else {
if ($action=='detail') {
	$smarty->assign('lap_kur', $db->QueryToArray("
                select nm_fakultas,nm_jenjang,prodi,krs,ttlkelas,ttlnil,kur,ttlkur,
				case when ttlnil>0 and ttlkur>0 then 'Completed' else 
				case when ttlkur>0 then 'Kurikulum Sdh, ' else 'Kurikulum Blm, ' end ||
				case when ttlnil>0 then 'Histori Nilai Sdh, ' else 'Historis Nilai Blm' end end as ket1,
				case when ttlkelas>0 and krs>10 then 'Telah KRS' else 
				case when ttlkelas>0 then 'Jadwal Sdh, ' else 'Jadwal Blm, ' end||
				case when krs>10 then 'Sdh KRS' else 'Blm KRS' end end as ket
				from 
				(select nm_fakultas,nm_jenjang,nm_jenjang||'-'||nm_program_studi as prodi,id_program_studi from program_studi 
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang 
				left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
				where status_aktif_prodi=1)s1 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as krs from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester=$id_sem_aktif  
				group by mahasiswa.id_program_studi)s2 on s1.id_program_studi=s2.id_program_studi 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as ttlnil from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester !=$id_sem_aktif  
				group by mahasiswa.id_program_studi)s3 on s1.id_program_studi=s3.id_program_studi 
				left join ( 
				select id_program_studi,wm_concat(kur) as kur from (
				select kurikulum_mk.id_program_studi,kurikulum_mk.id_kurikulum_prodi,' '||tahun_kurikulum||'('||count(*)||')' as kur from kurikulum_mk
				left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
				group by kurikulum_mk.id_program_studi, kurikulum_mk.id_kurikulum_prodi, tahun_kurikulum)
				group by id_program_studi)s4 on s1.id_program_studi=s4.id_program_studi 
				left join (
				select kelas_mk.id_program_studi,count(*) as ttlkelas from kelas_mk 
				where id_semester=$id_sem_aktif
				group by kelas_mk.id_program_studi) s5 on s1.id_program_studi=s5.id_program_studi
				left join ( 
				select kurikulum_mk.id_program_studi,count(*) as ttlkur from kurikulum_mk
				group by kurikulum_mk.id_program_studi)s6 on s1.id_program_studi=s6.id_program_studi 
				where nm_jenjang='$jen'
				order by nm_fakultas,prodi"));
}
if ($action=='detailfak') {
	$smarty->assign('action','detail');
	$nm_fak = $_GET['nm_fak'];
	$smarty->assign('lap_kur', $db->QueryToArray("
                select nm_fakultas,nm_jenjang,prodi,krs,ttlkelas,ttlnil,kur,ttlkur,
				case when ttlnil>0 and ttlkur>0 then 'Completed' else 
				case when ttlkur>0 then 'Kurikulum Sdh, ' else 'Kurikulum Blm, ' end ||
				case when ttlnil>0 then 'Histori Nilai Sdh, ' else 'Historis Nilai Blm' end end as ket1,
				case when ttlkelas>0 and krs>10 then 'Telah KRS' else 
				case when ttlkelas>0 then 'Jadwal Sdh, ' else 'Jadwal Blm, ' end||
				case when krs>10 then 'Sdh KRS' else 'Blm KRS' end end as ket
				from 
				(select nm_fakultas,nm_jenjang,nm_jenjang||'-'||nm_program_studi as prodi,id_program_studi from program_studi 
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang 
				left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
				where status_aktif_prodi=1)s1 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as krs from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester=$id_sem_aktif  
				group by mahasiswa.id_program_studi)s2 on s1.id_program_studi=s2.id_program_studi 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as ttlnil from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester !=$id_sem_aktif  
				group by mahasiswa.id_program_studi)s3 on s1.id_program_studi=s3.id_program_studi 
				left join ( 
				select id_program_studi,wm_concat(kur) as kur from (
				select kurikulum_mk.id_program_studi,kurikulum_mk.id_kurikulum_prodi,' '||tahun_kurikulum||'('||count(*)||')' as kur from kurikulum_mk
				left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
				group by kurikulum_mk.id_program_studi, kurikulum_mk.id_kurikulum_prodi, tahun_kurikulum)
				group by id_program_studi)s4 on s1.id_program_studi=s4.id_program_studi 
				left join (
				select kelas_mk.id_program_studi,count(*) as ttlkelas from kelas_mk 
				where id_semester=$id_sem_aktif
				group by kelas_mk.id_program_studi) s5 on s1.id_program_studi=s5.id_program_studi
				left join ( 
				select kurikulum_mk.id_program_studi,count(*) as ttlkur from kurikulum_mk
				group by kurikulum_mk.id_program_studi)s6 on s1.id_program_studi=s6.id_program_studi 
				where nm_fakultas='$nm_fak'
				order by nm_fakultas,prodi"));
}
if ($action=='viewperfakultas') {
$smarty->assign('prosen_kur', $db->QueryToArray("select nm_fakultas,nm_jenjang,count(nm_jenjang) as ttljenjang,
				sum(case when ttlnil>0 and ttlkur>0 then 1 else 0 end) as compled,
				round(sum(case when ttlnil>0 and ttlkur>0 then 1 else 0 end)/count(nm_jenjang)*100,2) as prosen,
				round(sum(case when ttlkelas>0 then 1 else 0 end)/count(nm_jenjang)*100,2) as prosen_siap,
				round(sum(case when krs>0 then 1 else 0 end)/count(nm_jenjang)*100,2) as prosen_krs				
				from 
				(select nm_fakultas,nm_jenjang,nm_jenjang||'-'||nm_program_studi as prodi,id_program_studi from program_studi 
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang 
				left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
				where status_aktif_prodi=1)s1 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as krs from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester=$id_sem_aktif  
				group by mahasiswa.id_program_studi)s2 on s1.id_program_studi=s2.id_program_studi 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as ttlnil from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester !=$id_sem_aktif  
				group by mahasiswa.id_program_studi)s3 on s1.id_program_studi=s3.id_program_studi 
				left join ( 
				select id_program_studi,wm_concat(kur) as kur from (
				select kurikulum_mk.id_program_studi,kurikulum_mk.id_kurikulum_prodi,tahun_kurikulum||'('||count(*)||')' as kur from kurikulum_mk
				left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
				group by kurikulum_mk.id_program_studi, kurikulum_mk.id_kurikulum_prodi, tahun_kurikulum)
				group by id_program_studi)s4 on s1.id_program_studi=s4.id_program_studi 
				left join (
				select kelas_mk.id_program_studi,count(*) as ttlkelas from kelas_mk 
				where id_semester=$id_sem_aktif
				group by kelas_mk.id_program_studi) s5 on s1.id_program_studi=s5.id_program_studi
				left join ( 
				select kurikulum_mk.id_program_studi,count(*) as ttlkur from kurikulum_mk
				group by kurikulum_mk.id_program_studi)s6 on s1.id_program_studi=s6.id_program_studi group by nm_fakultas, nm_jenjang 
				order by nm_fakultas,nm_jenjang"));
}
$smarty->assign('prosen_kur_univ', $db->QueryToArray(" select nm_jenjang,count(nm_jenjang) as ttljenjang,
				sum(case when ttlnil>0 and ttlkur>0 then 1 else 0 end) as compled,
				round(sum(case when ttlnil>0 and ttlkur>0 then 1 else 0 end)/count(nm_jenjang)*100,2) as prosen,
				round(sum(case when ttlkelas>0 then 1 else 0 end)/count(nm_jenjang)*100,2) as prosen_siap,
				round(sum(case when krs>0 then 1 else 0 end)/count(nm_jenjang)*100,2) as prosen_krs
				from 
				(select nm_fakultas,nm_jenjang,nm_jenjang||'-'||nm_program_studi as prodi,id_program_studi from program_studi 
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang 
				left join fakultas on program_studi.id_fakultas=fakultas.id_fakultas 
				where status_aktif_prodi=1)s1 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as krs from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester=$id_sem_aktif 
				group by mahasiswa.id_program_studi)s2 on s1.id_program_studi=s2.id_program_studi 
				left join ( 
				select mahasiswa.id_program_studi,count(*) as ttlnil from pengambilan_mk 
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs 
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi 
				where id_semester !=$id_sem_aktif  
				group by mahasiswa.id_program_studi)s3 on s1.id_program_studi=s3.id_program_studi 
				left join ( 
				select id_program_studi,wm_concat(kur) as kur from (
				select kurikulum_mk.id_program_studi,kurikulum_mk.id_kurikulum_prodi,tahun_kurikulum||'('||count(*)||')' as kur from kurikulum_mk
				left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
				group by kurikulum_mk.id_program_studi, kurikulum_mk.id_kurikulum_prodi, tahun_kurikulum)
				group by id_program_studi)s4 on s1.id_program_studi=s4.id_program_studi 
				left join (
				select kelas_mk.id_program_studi,count(*) as ttlkelas from kelas_mk 
				where id_semester=$id_sem_aktif
				group by kelas_mk.id_program_studi) s5 on s1.id_program_studi=s5.id_program_studi
				left join ( 
				select kurikulum_mk.id_program_studi,count(*) as ttlkur from kurikulum_mk
				group by kurikulum_mk.id_program_studi)s6 on s1.id_program_studi=s6.id_program_studi 
				group by nm_jenjang 
				order by nm_jenjang"));
				
}

$smarty->display("sample/penilaian/laporan-akademik.tpl");
?>