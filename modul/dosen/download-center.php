<?php

include 'config.php';
include 'function-tampil-informasi.php';
include '../mhs/class/download.class.php';

$download = new download($db, $user->ID_PENGGUNA);
/*
  STATUS DOWNLOAD
  0=REQUEST
  1=APPROVE

  STATUS FILE
  0=REQUEST
  1=AVAILABLE
  2=NOT FOUND
 */

if (isset($_POST)) {
    if (post('mode') == 'add') {
        $download->AddRequestDownload(post('nama'), post('url'), post('jenis'));
    } else if (post('mode') == 'update') {
        $download->UpdateRequestDownload(post('id_download'), post('nama'), post('url'), post('jenis'));
    } else if (post('mode') == 'delete') {
        $download->DeleteRequestDownload(post('id_download'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'update' or get('mode') == 'delete') {
        $smarty->assign('download', $download->GetRequestDownload(get('id_download')));
    }
}


$smarty->assign('data_request', $download->LoadRequestDownload());
$smarty->display('download-center.tpl');
?>

