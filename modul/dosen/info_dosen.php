<?php

include 'config.php';
include ('../sumberdaya/includes/encrypt.php');

$depan = time();
$belakang = strrev(time());

$id_pgg = $user->ID_PENGGUNA;
$id_dsn = $user->ID_DOSEN;
$id_fak = $user->ID_FAKULTAS;

$nama_singkat_pt = $nama_singkat;

if (get('mode') == 'edit') {

    $sts_aktif = $db->QueryToArray("select id_status_pengguna, upper(nm_status_pengguna) as nm_status_pengguna from status_pengguna where id_role=4 order by status_aktif desc, nm_status_pengguna");
    $smarty->assign('STS_AKTIF', $sts_aktif);
    $db->Query("select id_fakultas, id_program_studi from program_studi where id_program_studi=(select id_program_studi from dosen where id_dosen=$id_dsn)");
    $prodi = $db->FetchAssoc();

    $kdfak = $_POST['kdfak'];

    $listfak = $db->QueryToArray("select id_fakultas, upper(nm_fakultas) as nm_fakultas from fakultas order by id_fakultas");
    $smarty->assign('T_FAK', $listfak);

    $kdfak1 = isSet($_POST['kdfak']) ? $_POST['kdfak'] : $prodi['ID_FAKULTAS'];
    $smarty->assign('FAKGET', $kdfak1);

    if (isset($_POST['kdfak'])) {
        $kdprodi = $db->QueryToArray("
        select pst.id_program_studi, upper(substr(jjg.nm_jenjang,1,2)||' - '||pst.nm_program_studi) as nm_program_studi 
        from program_studi pst 
        left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang 
        where id_fakultas=$kdfak
        order by jjg.nm_jenjang, pst.nm_program_studi
        ");
        $smarty->assign('PRO', $kdprodi);
    } else {
        $kdprodi = $db->QueryToArray("
        select pst.id_program_studi, upper(substr(jjg.nm_jenjang,1,2)||' - '||pst.nm_program_studi) as nm_program_studi 
        from program_studi pst 
        left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang 
        where pst.id_fakultas = (select id_fakultas from program_studi where id_program_studi=(select id_program_studi from dosen where id_dosen=$id_dsn))
        order by jjg.nm_jenjang, pst.nm_program_studi
        ");
        $smarty->assign('PRO', $kdprodi);
    }

    $jk = $db->QueryToArray("select distinct kelamin_pengguna, case when kelamin_pengguna='1' then 'LAKI-LAKI' else 'PEREMPUAN' end as nm_kelamin_pengguna from pengguna where kelamin_pengguna in ('1', '2') order by kelamin_pengguna");
    $smarty->assign('JK', $jk);

    $jab = $db->QueryToArray("select id_jabatan_pegawai, upper(nm_jabatan_pegawai) as nm_jabatan_pegawai from jabatan_pegawai order by id_jabatan_pegawai");
    $smarty->assign('JAB', $jab);

    $agama = $db->QueryToArray("select id_agama,nm_agama from agama order by id_agama");
    $smarty->assign('AGAMA', $agama);

    $nikah = $db->QueryToArray("select * from status_pernikahan order by id_status_pernikahan");
    $smarty->assign('SNIKAH', $nikah);

    $dept = $db->QueryToArray("select id_departemen, upper(nm_departemen) as nm_departemen from departemen where id_fakultas='{$id_fak}' order by nm_departemen");
    $smarty->assign('DEPT', $dept);

    $data_kota = array();
    $provinsi = $db->QueryToArray("SELECT * FROM PROVINSI WHERE ID_NEGARA=114 ORDER BY NM_PROVINSI");
    foreach ($provinsi as $data) {
        array_push($data_kota, array(
            'nama' => $data['NM_PROVINSI'],
            'kota' => $db->QueryToArray("SELECT id_kota, tipe_dati2||' '||nm_kota as kota FROM KOTA WHERE ID_PROVINSI='{$data['ID_PROVINSI']}' ORDER BY TIPE_DATI2, NM_KOTA")
        ));
    }
    $smarty->assign('data_kota', $data_kota);
    $smarty->assign('count_provinsi', count($data_kota));
}

if (!empty($_POST)) {
    if (post('action') == 'update') {
        $id_dosen = $_POST['id_dosen'];
        $id_pengguna = $_POST['id_pengguna'];

        // UPDATE TABEL PENGGUNA
        $username = $_POST['nip'];
        $gelar_depan = $_POST['gelar_dpn'];
        $gelar_belakang = $_POST['gelar_blkg'];
        $nm_pengguna = $_POST['nm_lengkap'];
        $id_kota_lahir = $_POST['id_kota_lahir'];
        $tgl_lahir_pengguna = $_POST['tgl_lahir'];
        $email_pengguna = $_POST['email'];
        $kelamin_pengguna = $_POST['jk'];
        $agama = $_POST['agama'];
        $snikah = $_POST['status_nikah'];

        $db->Query("update pengguna set gelar_depan=trim('" . $gelar_depan . "'),nm_pengguna=upper('{$nm_pengguna}'),
            gelar_belakang=trim('" . $gelar_belakang . "'), email_alternate=trim(lower('" . $email_pengguna . "')),
            id_kota_lahir=$id_kota_lahir, tgl_lahir_pengguna=to_date('" . $tgl_lahir_pengguna . "','DD-MM-YYYY'), 
            kelamin_pengguna='" . $kelamin_pengguna . "', id_agama='" . $agama . "', id_status_pernikahan='{$snikah}'
	where id_pengguna=$id_pengguna");

        // UPDATE TABEL DOSEN
        $nip_dosen = $_POST['nip'];
        $nidn = $_POST['nidn'];
        $serdos = $_POST['serdos'];
        $id_program_studi = $_POST['prodi'];
        $status_dosen = $_POST['status_dsn'];
        $id_status_pengguna = $_POST['aktif'];
        $alamat_rumah_dosen = $_POST['alamat'];
        $nip_lama = $_POST['nip_lama'];
        $prajab_nomor = $_POST['prajab_nomor'];
        $prajab_tanggal = $_POST['prajab_tanggal'];
        $tgl_sumpah_pns = $_POST['tgl_sumpah_pns'];
        $telp_dosen = $_POST['tlp'];
        $mobile_dosen = $_POST['hp'];
        $tmt_cpns = $_POST['tmt_cpns'];
        $nomor_karpeg = $_POST['nomor_karpeg'];
        $nomor_npwp = $_POST['nomor_npwp'];
        $taspen = $_POST['taspen'];
        $kode_pos = $_POST['kode_pos'];
        $no_ktp = $_POST['no_ktp'];


        $db->Query("update dosen set nidn_dosen=trim('" . $nidn . "'),nip_lama='" . $nip_lama . "',  serdos=trim('" . $serdos . "'), id_program_studi_sd=$id_program_studi,
	id_program_studi=$id_program_studi, status_dosen='" . $status_dosen . "', id_status_pengguna=$id_status_pengguna, alamat_rumah_dosen=trim(upper('" . $alamat_rumah_dosen . "'))
        ,prajab_nomor='" . $prajab_nomor . "',prajab_tanggal=to_date('" . $prajab_tanggal . "','DD-MM-YYYY'),tgl_sumpah_pns=to_date('" . $tgl_sumpah_pns . "','DD-MM-YYYY'),"
                . "tlp_dosen=trim('" . $telp_dosen . "'),mobile_dosen=trim('" . $mobile_dosen . "'),tmt_cpns=to_date('" . $tmt_cpns . "','DD-MM-YYYY'),nomer_karpeg='{$nomor_karpeg}',nomor_npwp='{$nomor_npwp}',taspen='{$taspen}',kode_pos='{$kode_pos}',no_ktp=trim('" . $no_ktp . "')
	where id_dosen=$id_dosen");
        // Tabel Dosen Departemen
        $depart = $_POST['dept'];
        $cek_dosen_depart = $db->QuerySingle("SELECT COUNT(*) FROM DOSEN_DEPARTEMEN WHERE ID_DEPARTEMEN='{$depart}' AND ID_DOSEN='{$id_dosen}' AND STATUS=1");
        if ($cek_dosen_depart == 0) {
            $db->Query("UPDATE DOSEN_DEPARTEMEN SET STATUS=0 WHERE ID_DOSEN='{$id_dosen}'");
            $db->Query("INSERT INTO DOSEN_DEPARTEMEN (ID_DOSEN,ID_DEPARTEMEN,STATUS,TANGGAL) VALUES ('{$id_dosen}','{$depart}',1,TO_DATE(SYSDATE,'DD-MM-YYYY'))");
        }

        // Tabel Unit Esselon
        $usel1 = $_POST['unit_esselon1'];
        $usel2 = $_POST['unit_esselon2'];
        $usel3 = $_POST['unit_esselon3'];
        $usel4 = $_POST['unit_esselon4'];
        $usel5 = $_POST['unit_esselon5'];
        $db->Query("SELECT * FROM UNIT_ESSELON WHERE ID_PENGGUNA='{$id_pengguna}'");
        $d_usel = $db->FetchAssoc();
        $id_usel = $d_usel['ID_UNIT_ESSELON'];
        if ($id_usel != '') {
            $db->Query("UPDATE UNIT_ESSELON SET UNIT_ESSELON_I='{$usel1}',UNIT_ESSELON_II='{$usel2}',UNIT_ESSELON_III='{$usel3}',UNIT_ESSELON_IV='{$usel4}',UNIT_ESSELON_V='{$usel5}' WHERE ID_UNIT_ESSELON='{$id_usel}'");
        } else {
            $db->Query("INSERT INTO UNIT_ESSELON (ID_PENGGUNA,UNIT_ESSELON_I,UNIT_ESSELON_II,UNIT_ESSELON_III,UNIT_ESSELON_IV,UNIT_ESSELON_V) VALUES ('{$id_pengguna}','{$usel1}','{$usel2}','{$usel3}','{$usel4}','{$usel5}')");
        }
    }
}

$nip_dosen = $db->QuerySingle("select NIP_DOSEN from DOSEN where id_pengguna='{$user->ID_PENGGUNA}'");

/* INTEGRASI BLOG (TAMBAHAN DARI MAS BAMBANG)
$DBhost = "10.0.110.1";
$DBuser = "lihat_blogmhs";
$DBpass = "lihat_blogmhs";
$DBName = "dbwebmhs";
mysql_connect($DBhost, $DBuser, $DBpass) or die("<i>Error Connection Database...</i>");
mysql_select_db("$DBName") or die("<i>Wrong Database...</i>");  */

$in_jam = date("H");
$in_menit = date("i");
$in_detik = date("s");
$waktu = $in_jam . $in_menit . $in_detik;
$ipnya = $_SERVER["REMOTE_ADDR"];
$ipnya_tmp = explode(".", $ipnya);

$kode = md5($ipnya_tmp[0] . $ipnya_tmp[3] . "bam" . $nip_dosen . "bang" . $waktu . $ipnya_tmp[2] . $ipnya_tmp[1]);
$kode2 = substr($kode, 0, 2) . $in_jam . substr($kode, 2, 14) . $in_menit . substr($kode, 16, 8) . $in_detik . substr($kode, 24);

$link_blog = "http://web.unair.ac.id/checkLogin.php?loginid=" . $nip_dosen . "&kode=" . $kode2;


$db->Query("SELECT EMAIL_PENGGUNA FROM PENGGUNA WHERE ID_PENGGUNA='{$user->ID_PENGGUNA}'");
$email_dosen = $db->FetchAssoc();
$nama_email = $email_dosen['EMAIL_PENGGUNA'];
if(!empty($nama_email))$link_email = substr($nama_email, strpos($nama_email, '@', 1) + 1);


$db->Query("SELECT * FROM AUCC.UPLOAD_FOLDER WHERE ID_UPLOAD_FOLDER='5655'");
$folder_file = $db->FetchAssoc();
$link_file = "../..{$folder_file['PATH_UPLOAD_FOLDER']}/{$folder_file['NAMA_UPLOAD_FOLDER']}/{$id_pgg}";
$smarty->assign('link_file', $link_file);

$dosen = $db->QueryToArray("
    select dsn.id_pengguna, dsn.id_dosen, dsn.nip_dosen as username, dsn.nidn_dosen, dsn.serdos, upper(dsn.alamat_rumah_dosen) as alamat_rumah_dosen, dsn.tlp_dosen, dsn.mobile_dosen, dsn.status_dosen,
    UPPER(PGG.NM_PENGGUNA) AS NM_PENGGUNA, PGG.GELAR_DEPAN, PGG.GELAR_BELAKANG, TO_CHAR(PGG.TGL_LAHIR_PENGGUNA, 'DD-MM-YYYY') AS TGL_LAHIR_PENGGUNA, PGG.EMAIL_PENGGUNA, PGG.EMAIL_ALTERNATE,
    pgg.kelamin_pengguna,TO_CHAR(gol.TMT_SEJARAH_GOLONGAN, 'DD-MM-YYYY') TMT_GOLONGAN, upper(gol.nm_golongan) as nm_golongan, upper(gol.nm_pangkat) as nm_pangkat, upper(fgs.nm_jabatan_fungsional) as nm_jabatan_fungsional,TO_CHAR(fgs.TMT_SEJ_JAB_FUNGSIONAL, 'DD-MM-YYYY') TMT_JAB_FUNGSIONAL
    , kt.tipe_dati2||' '||nm_kota as tempat_lahir,UPPER(JAB.NM_JABATAN_PEGAWAI) AS NM_JABATAN_PEGAWAI, UPPER(STP.NM_STATUS_PENGGUNA) AS NM_STATUS_PENGGUNA, UPPER(NM_JENJANG||' - '||NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI, UPPER(NM_FAKULTAS) AS NM_FAKULTAS
    ,AG.NM_AGAMA,DEP.NM_DEPARTEMEN,SPEN.*,dsn.NIP_LAMA,STRUK.*,SNIKAH.*,USEL.*,DSN.*,DDEP.ID_DEPARTEMEN DEPARTEMEN_DOSEN,
    UKTP.NAMA_UPLOAD_FILE FILE_KTP,UCPNS.NAMA_UPLOAD_FILE SK_CPNS,UPNS.NAMA_UPLOAD_FILE SK_PNS,UKON.NAMA_UPLOAD_FILE SK_KONVERSI,
    UPEN.NAMA_UPLOAD_FILE SK_PENEMPATAN,UTET.NAMA_UPLOAD_FILE SK_DOSEN_TETAP,PGG.ID_AGAMA,PGG.ID_KOTA_LAHIR
    from AUCC.dosen dsn
    JOIN AUCC.PENGGUNA PGG ON DSN.ID_PENGGUNA=PGG.ID_PENGGUNA
    LEFT JOIN AUCC.KOTA KT ON KT.ID_KOTA=PGG.ID_KOTA_LAHIR
    JOIN AUCC.PROGRAM_STUDI PST ON DSN.ID_PROGRAM_STUDI_SD=PST.ID_PROGRAM_STUDI
    JOIN AUCC.FAKULTAS FAK ON PST.ID_FAKULTAS=FAK.ID_FAKULTAS
    LEFT JOIN AUCC.DOSEN_DEPARTEMEN DDEP ON DDEP.ID_DOSEN=DSN.ID_DOSEN AND STATUS=1
    LEFT JOIN AUCC.DEPARTEMEN DEP ON DEP.ID_DEPARTEMEN=DDEP.ID_DEPARTEMEN
    join AUCC.jenjang jjg on pst.id_jenjang=jjg.id_jenjang
    LEFT JOIN AUCC.AGAMA AG ON AG.ID_AGAMA=PGG.ID_AGAMA
    LEFT JOIN AUCC.STATUS_PERNIKAHAN SNIKAH ON SNIKAH.ID_STATUS_PERNIKAHAN=PGG.ID_STATUS_PERNIKAHAN
    LEFT JOIN AUCC.UNIT_ESSELON USEL ON USEL.ID_PENGGUNA=PGG.ID_PENGGUNA
    LEFT JOIN AUCC.UPLOAD_FOLDER UF ON UPPER(UF.NAMA_UPLOAD_FOLDER) LIKE UPPER('%'||dsn.ID_PENGGUNA||'%')
    LEFT JOIN AUCC.UPLOAD_FILE UKTP ON UPPER(UKTP.NAMA_UPLOAD_FILE) LIKE UPPER('%file_ktp%') AND UKTP.ID_UPLOAD_FOLDER=UF.ID_UPLOAD_FOLDER
    LEFT JOIN AUCC.UPLOAD_FILE UCPNS ON UPPER(UCPNS.NAMA_UPLOAD_FILE) LIKE UPPER('%sk_cpns%') AND UCPNS.ID_UPLOAD_FOLDER=UF.ID_UPLOAD_FOLDER
    LEFT JOIN AUCC.UPLOAD_FILE UPNS ON UPPER(UPNS.NAMA_UPLOAD_FILE) LIKE UPPER('%sk_pns%') AND UPNS.ID_UPLOAD_FOLDER=UF.ID_UPLOAD_FOLDER
    LEFT JOIN AUCC.UPLOAD_FILE UKON ON UPPER(UKON.NAMA_UPLOAD_FILE) LIKE UPPER('%konversi_nip%') AND UKON.ID_UPLOAD_FOLDER=UF.ID_UPLOAD_FOLDER
    LEFT JOIN AUCC.UPLOAD_FILE UPEN ON UPPER(UPEN.NAMA_UPLOAD_FILE) LIKE UPPER('%sk_penempatan_prodi%') AND UPEN.ID_UPLOAD_FOLDER=UF.ID_UPLOAD_FOLDER
    LEFT JOIN AUCC.UPLOAD_FILE UTET ON UPPER(UTET.NAMA_UPLOAD_FILE) LIKE UPPER('%sk_dosen_tetap%') AND UTET.ID_UPLOAD_FOLDER=UF.ID_UPLOAD_FOLDER
    left join (
    SELECT * FROM (
        SELECT * FROM (
          SELECT G.NM_GOLONGAN,G.NM_PANGKAT,SJ.*
          FROM AUCC.SEJARAH_GOLONGAN SJ
          JOIN AUCC.GOLONGAN G ON SJ.ID_GOLONGAN=G.ID_GOLONGAN
          WHERE SJ.ID_PENGGUNA={$id_pgg}
          ORDER BY SJ.TMT_SEJARAH_GOLONGAN DESC
        ) WHERE ROWNUM=1
      )
    ) GOL ON GOL.ID_PENGGUNA=DSN.ID_PENGGUNA
    LEFT JOIN (
        SELECT * FROM (
            SELECT * FROM (
              SELECT G.NM_JABATAN_FUNGSIONAL,SJ.*
              FROM AUCC.SEJARAH_JABATAN_FUNGSIONAL SJ
              JOIN AUCC.JABATAN_FUNGSIONAL G ON SJ.ID_JABATAN_FUNGSIONAL=G.ID_JABATAN_FUNGSIONAL
              WHERE SJ.ID_PENGGUNA={$id_pgg}
              ORDER BY SJ.TMT_SEJ_JAB_FUNGSIONAL DESC
            ) WHERE ROWNUM=1
      )
    ) FGS ON FGS.ID_PENGGUNA=DSN.ID_PENGGUNA
    LEFT JOIN (
        SELECT * FROM (
            SELECT * FROM (
              SELECT G.NM_JABATAN_STRUKTURAL,SJ.*
              FROM AUCC.SEJARAH_JABATAN_STRUKTURAL SJ
              JOIN AUCC.JABATAN_STRUKTURAL G ON SJ.ID_JABATAN_STRUKTURAL=G.ID_JABATAN_STRUKTURAL
              WHERE SJ.ID_PENGGUNA={$id_pgg}
              ORDER BY SJ.TMT_SEJ_JAB_STRUKTURAL DESC
            ) WHERE ROWNUM=1
      )
    ) STRUK ON STRUK.ID_PENGGUNA=DSN.ID_PENGGUNA
    LEFT JOIN (
        SELECT * FROM (
            SELECT * FROM (
              SELECT NAMA_PENDIDIKAN_AKHIR,SP.*
              FROM AUCC.SEJARAH_PENDIDIKAN SP
              JOIN AUCC.PENDIDIKAN_AKHIR PA ON PA.ID_PENDIDIKAN_AKHIR=SP.ID_PENDIDIKAN_AKHIR
              WHERE SP.ID_PENGGUNA={$id_pgg}
              ORDER BY SP.TAHUN_LULUS_PENDIDIKAN DESC,ID_SEJARAH_PENDIDIKAN DESC
            ) WHERE ROWNUM=1
      )
    ) SPEN ON SPEN.ID_PENGGUNA=DSN.ID_PENGGUNA
    LEFT JOIN AUCC.JABATAN_PEGAWAI JAB ON JAB.ID_JABATAN_PEGAWAI=DSN.ID_JABATAN_PEGAWAI
    left join AUCC.status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
    where pgg.id_pengguna={$id_pgg}");
$smarty->assign('DOSEN', $dosen);


$db->Query("select case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)) else trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)||', '||pgg.gelar_belakang) end as nm_pengguna,
		dsn.nip_dosen from dosen dsn left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
		where dsn.id_pengguna=$id_pgg");
$foto = $db->FetchAssoc();
$to = '';
$file = '';
$img = 'proses/upload_photo.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&yth=' . $to . '&file=' . $file . '&' . $belakang . '=' . $depan . $belakang) . '';
$smarty->assign('IMG', $img);
$db->Query("select username as photo from pengguna where id_pengguna=$id_pgg");
$get_photo = $db->FetchAssoc();
$filename = "../../foto_pegawai/" . $nama_singkat_pt . "/" . $get_photo['PHOTO'] . ".JPG";
if (file_exists($filename)) {
    $photo = $filename;
} else {
    $photo = '../sumberdaya/includes/images/unknown.png';
}
$smarty->assign('PHOTO', $photo);


$smarty->assign('mode', get('mode'));
$smarty->display('display-biodata/info-dosen.tpl');
