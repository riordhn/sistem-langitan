<?php
require '../../config.php';
/*include '../../tcpdf/config/lang/ind.php';
include '../../tcpdf/tcpdf.php';*/
require 'proses/penilaian.class.php';

$penilaian = new penilaian($db, $user->ID_DOSEN);

$id_kelas_mk = get('id_kelas_mk');

$data_kelas_mk = $penilaian->get_kelas_mk(get('id_kelas_mk'));
$data_semester = $penilaian->get_semester_kelas_mk(get('id_kelas_mk'));
$jumlah_komponen = count($penilaian->load_komponen_mk(get('id_kelas_mk'))) + 4;
$komponen_width_cell = round(49 / count($penilaian->load_komponen_mk(get('id_kelas_mk'))));

// Antisipasi Jika di buka dosen dosen lain yang tidak mengajar
$cek_dosen_log = $db->QuerySingle("SELECT COUNT(*) FROM PENGAMPU_MK WHERE ID_DOSEN='{$user->ID_DOSEN}' AND ID_KELAS_MK='{$id_kelas_mk}'");
if ($cek_dosen_log == 0) {
    die('Access Denied...');
}

// Load Komponen Mk
$data_komponen_mk = '';
foreach ($penilaian->load_komponen_mk(get('id_kelas_mk')) as $data) {
    $data_komponen_mk .= '<th width="' . $komponen_width_cell . '%">' . $data['NM_KOMPONEN_MK'] . ' <br/> (' . $data['PERSENTASE_KOMPONEN_MK'] . ' %)</th>';
}

$data_mahasiswa = $penilaian->load_data_mahasiswa(get('id_kelas_mk'));
$data_distribusi_nilai = $penilaian->load_distribusi_nilai(get('id_kelas_mk'));

//Data Mahasiswa
$data_nilai_mahasiswa = '';
$no = 1;
foreach ($data_mahasiswa as $data) {
    $data_nilai_mahasiswa .= '<tr>';
    $data_nilai_mahasiswa .='<td>'.$no.'</td><td align="left">'.$data['nim'].'</td><td align="left">'.$data['nama'].'</td>';
    foreach ($data['data_nilai'] as $data_nilai) {
        $data_nilai_mahasiswa .= '<td align="center">'.$data_nilai['BESAR_NILAI_MK'].'</td>';
    }
    $data_nilai_mahasiswa .='<td align="center">'.$data['nilai_angka_akhir'].'</td><td align="center">'.$data['nilai_huruf_akhir'].'</td>';
    $data_nilai_mahasiswa .= '</tr>';
    $no++;
}

// Distribusi Nilai
$str_distribusi_nilai = "<h3>Distribusi Nilai Mata Kuliah </h3><br/>";
foreach ($data_distribusi_nilai as $dn) {
    $persen = round($dn['DISTRIBUSI_NILAI'], 2);
    $str_distribusi_nilai .= "<span class='keterangan'>Nilai {$dn['NILAI_HURUF']} : {$persen} % </span><br/> ";
}
$str_distribusi_nilai.="";

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Sistem Langitan Nahdlatul Ulama');
$pdf->SetAuthor('Sistem Langitan');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .header { font-size: 20pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 14pt; font-family: serif; margin-top: 0px ;text-align:center; }
    .keterangan { font-size: 8pt; }
    td { font-size: 8pt; }
    .center { font-weight:bold;text-align:center }
    th { background-color:black;color:white;font-size: 7pt;text-align:center; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="20%" align="right"><img src="../../img/akademik_images/logo-{nama_singkat}.gif" width="180px" height="180px"/></td>
        <td width="80%" align="center">
            <span class="header">{nama_pt}<br/></span><span class="address">CETAK PENILAIAN<br/>
            MATA AJAR {mata_ajar} <br/>KELAS {kelas}</span><br/><span class="address">SEMESTER {semester}</span><br/><span class="address">PROGRAM STUDI {prodi}</span>
        </td>
    </tr>
</table>
<hr/>
<p></p>
<table width="100%" border="1" cellpadding="3">
        <tr>
            <th width="5%">NO</th>
            <th width="13%">NIM</th>
            <th width="19%">Nama</th>
            {data_komponen}
            <th width="7%">Nilai Angka Akhir</th>
            <th width="7%">Nilai Huruf Akhir</th>
        </tr>
        {data_mahasiswa}
</table>
<p></p>
<table width="30%" border="0" align="left">
    <tr>
        <td align='left'>{distribusi_nilai}</td>
    </tr>
</table>
<table width="100%" border="0" align="center">
    {ttd}
</table>
EOF;

$html = str_replace('{nama_singkat}', $nama_singkat, $html);
$html = str_replace('{nama_pt}', strtoupper($nama_pt), $html);
$html = str_replace('{mata_ajar}', strtoupper($data_kelas_mk['NM_MATA_KULIAH'] . ' (' . $data_kelas_mk['KD_MATA_KULIAH'] . ')'), $html);
$html = str_replace('{kelas}', $data_kelas_mk['NM_KELAS'], $html);
$html = str_replace('{semester}', strtoupper($data_semester['NM_SEMESTER'] . ' ( ' . $data_semester['TAHUN_AJARAN'] . ' )'), $html);
$html = str_replace('{prodi}', strtoupper($data_kelas_mk['NM_PROGRAM_STUDI']), $html);
$html = str_replace('{colspan}', $jumlah_komponen, $html);
$html = str_replace('{data_komponen}', $data_komponen_mk, $html);
$html = str_replace('{data_mahasiswa}', $data_nilai_mahasiswa, $html);
$html = str_replace('{distribusi_nilai}', $str_distribusi_nilai, $html);
if (isset($user->ID_DOSEN)) {
    $ttd = '
            <tr>
                <td width="60%"></td>
                <td width="40%" align="center">
                {kota_pt}, ' . strftime('%d %B %Y') . '<br/>Dosen Pengajar
                <br/><br/><br/><br/><br/>' . $user->NM_PENGGUNA . '<br/>NIP.' . $user->USERNAME . '
                </td>
            </tr>';
    $html = str_replace('{ttd}', $ttd, $html);
    $html = str_replace('{kota_pt}', $kota_pt, $html);
//echo $html;exit;
} else {
    $html = str_replace('{ttd}', '', $html);
    $html = str_replace('{kota_pt}', $kota_pt, $html);
}
$pdf->writeHTML($html);

$nama = str_replace(' ', '_', $data_semester['NM_SEMESTER'] . '(' . $data_semester['TAHUN_AJARAN'] . ')') . '_' . $data_kelas_mk['NM_MATA_KULIAH'] . '_KELAS_' . $data_kelas_mk['NM_KELAS'];
$nama_pdf = strtoupper(str_replace(' ', '_', $nama)) . '.pdf';
$pdf->Output($nama_pdf, 'I');

