<?php

include 'config.php';

$cari = str_replace('+', ' ', $_REQUEST['term']);
$data = $db->QueryToArray("
    SELECT * FROM (
        SELECT KK.*,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA
        FROM AUCC.KOMPLAIN_KATEGORI KK
        JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=KK.ID_UNIT_KERJA
        WHERE UPPER(KK.NM_KATEGORI) LIKE UPPER('%{$cari}%') 
    ) WHERE ROWNUM<20");
$data_hasil = array();
foreach ($data as $d) {
    array_push($data_hasil, array(
        'value' => ucwords(strtolower($d['NM_KATEGORI'])),
        'label' => ucwords(strtolower($d['NM_KATEGORI'])),
        'id' => (int) $d['ID_KOMPLAIN_KATEGORY']
    ));
}
echo json_encode($data_hasil)
?>
