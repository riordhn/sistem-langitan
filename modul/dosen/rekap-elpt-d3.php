<?php
include 'config.php';

// Fikrie //
$id_pt = $id_pt_user;

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$fakultas_set = $db->QueryToArray("select id_fakultas, nm_fakultas from fakultas where id_perguruan_tinggi = {$id_pt}");

foreach ($fakultas_set as &$f)
	$f['program_studi_set'] = $db->QueryToArray("select id_program_studi, id_jenjang, nm_program_studi from program_studi where id_fakultas = {$f['ID_FAKULTAS']} and id_jenjang = 5 and id_program_studi not in (197)");
	
$data_set = $db->QueryToArray("
	select ps.id_program_studi,
	  (select count(me.id_mhs) from mahasiswa_elpt me where me.id_program_studi = ps.id_program_studi and (elpt_score <= 450)) elpt1,
	  (select count(me.id_mhs) from mahasiswa_elpt me where me.id_program_studi = ps.id_program_studi and (elpt_score > 450 and elpt_score <= 500)) elpt2,
	  (select count(me.id_mhs) from mahasiswa_elpt me where me.id_program_studi = ps.id_program_studi and (elpt_score > 500)) elpt3
	from program_studi ps
	where ps.id_jenjang = 5");
	
foreach ($data_set as &$d)
{
	$d['TOTAL'] = $d['ELPT1'] + $d['ELPT2'] + $d['ELPT3'];
	$d['PERSEN_ELPT1'] = round(($d['ELPT1'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_ELPT2'] = round(($d['ELPT2'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_ELPT3'] = round(($d['ELPT3'] / $d['TOTAL']) * 100, 2);
}
	
$smarty->assign('fakultas_set', $fakultas_set);
$smarty->assign('data_set', $data_set);

$smarty->display('sample/rekap/rekap-elpt-d3.tpl');
?>