<?php
include 'config.php';
include 'proses/penelitian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$penelitian = new Penelitian($db);

$mode = get('mode' ,'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'approve-departemen')
    {
        $id_penelitian = post('id_penelitian');
        
        $result = $db->Query("
            update penelitian set
                id_verifikator_departemen = {$user->ID_PENGGUNA}, tgl_approve_departemen = current_date, tgl_reject_departemen = null
            where id_penelitian = {$id_penelitian}");
        
        echo $result ? "1" : "0";
        
        exit();
    }
    
    if (post('mode') == 'reject-departemen')
    {
        $id_penelitian = post('id_penelitian');
        
        $result = $db->Query("
            update penelitian set
                id_verifikator_departemen = {$user->ID_PENGGUNA}, tgl_approve_departemen = null, tgl_reject_departemen = current_date,
                tgl_submit = null
            where id_penelitian = {$id_penelitian}");
        
        echo $result ? "1" : "0";
        
        exit();
    }
}

if ($request_method == 'GET')
{
    if ($mode == 'view')
    {
        if ($user->ID_JABATAN_PEGAWAI == 11) // Ketua departemen
        {
            // Mendapatkan list penelitian dosen yang ada dalam satu departemen
            $penelitian_set = $db->QueryToArray("
                select p.id_penelitian, p.judul, pg.nm_pengguna, to_char(tgl_penelitian,'YYYY') as tahun, pj.nama_jenis,
                    tgl_approve_departemen, tgl_reject_departemen
                from penelitian p
                join dosen d on d.id_dosen = p.id_dosen
                join pengguna pg on pg.id_pengguna = d.id_pengguna
                join program_studi ps on ps.id_program_studi = d.id_program_studi
                left join penelitian_jenis pj on pj.id_penelitian_jenis = p.id_penelitian_jenis
                where ps.id_departemen in (select id_departemen from departemen where id_kadep = {$user->ID_PENGGUNA}) and p.tgl_submit is not null");
        }
        
        if ($user->ID_JABATAN_PEGAWAI == 5)
        {
            // Mendapatkan list penelitian dosen yang ada dalam satu fakultas
            $penelitian_set = $db->QueryToArray("
                select p.id_penelitian, p.judul, pg.nm_pengguna, to_char(tgl_penelitian,'YYYY') as tahun, pj.nama_jenis,
                    tgl_approve_departemen, tgl_reject_departemen
                from penelitian p
                join dosen d on d.id_dosen = p.id_dosen
                join pengguna pg on pg.id_pengguna = d.id_pengguna
                join program_studi ps on ps.id_program_studi = d.id_program_studi
                left join penelitian_jenis pj on pj.id_penelitian_jenis = p.id_penelitian_jenis
                where ps.id_fakultas in (select id_fakultas from fakultas where id_dekan = {$user->ID_PENGGUNA}) and p.tgl_submit is not null and p.tgl_approve_departemen is not null");
        }
        
        $smarty->assign('id_jabatan_pegawai', $user->ID_JABATAN_PEGAWAI);
        $smarty->assign('penelitian_set', $penelitian_set);
    }
    
    if ($mode == 'edit')
    {
        $id_penelitian = get('id_penelitian');
        $p = $penelitian->GetData($id_penelitian, '');
        
        $smarty->assign('p', $p);
    }
}

$smarty->display("sample/approval/penelitian/{$mode}.tpl");
?>
