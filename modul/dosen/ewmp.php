<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$smarty->assign('TITLE', 'EWMP');

//detail dosen
require_once 'proses/info_dosen.class.php';
$info_dosen = new info_dosen($db, $login->id_pengguna);
$data_info_dosen = $info_dosen->set();
$smarty->assign(array(
    'nama' => $data_info_dosen['NAMA'],
    'nip' => $data_info_dosen['NIP'],
    'fakultas' => $data_info_dosen['FAKULTAS'],
    'prodi' => $data_info_dosen['PRODI']
));
// tab Rekap Beban ewmp
require_once 'proses/ewmp.class.php';
$ewmp= new ewmp($db);
$smarty->assign('isi_data', $ewmp->cetak_ewmp($login->id_dosen));



$smarty->display('sample/ewmp/ewmp.tpl');
?>