<?php
include('conf.php');

$kd_fak= $user->ID_FAKULTAS;
$id_pengguna= $user->ID_PENGGUNA; 

$nim = $_REQUEST['nim'];
$smarty->assign('nim', $nim);
$action = $_POST['action'];
$smarty->assign('action', $action);

if(($_GET['action'])=='proses'){
	$smarty->assign('action', 'viewdetail');
	
	$ip = $_SERVER['REMOTE_ADDR'];
	$id_pengambilan_mk =$_GET['id_pmk'];
	
	//echo "insert into log_granted_nilai (id_pengguna,id_mhs,id_pengambilan_mk,id_semester,status,ip_adrress,tgl_granted)
	// 	  select '$id_pengguna',id_mhs,id_pengambilan_mk,id_semester,1,'$ip', sysdate	from pengambilan_mk where id_pengambilan_mk=$id_pengambilan_mk";
	$db->Query("insert into log_granted_nilai (id_pengguna,id_mhs,id_pengambilan_mk,id_semester,status,ip_address,tgl_granted)
				select '$id_pengguna',id_mhs,id_pengambilan_mk,id_semester,1,'$ip', sysdate	from pengambilan_mk where id_pengambilan_mk=$id_pengambilan_mk");
	
	
}

$smarty->assign('detail_mhs', $db->QueryToArray("
                select nim_mhs,nm_pengguna,nm_jenjang||'-'||nm_program_studi as prodi
				from mahasiswa 
				join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
				join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
				where nim_mhs='$nim' and id_fakultas=$kd_fak"));

$smarty->assign('detail_nilai', $db->QueryToArray("
                select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nilai_huruf,
					pengambilan_mk.id_pengambilan_mk,mahasiswa.id_program_studi,pengambilan_mk.id_kurikulum_mk,
					thn_akademik_semester||'/'||group_semester||'/'||nm_semester as smt,log_granted_nilai.status
					from pengambilan_mk
					left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join semester on pengambilan_mk.id_semester=semester.id_semester
					left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
					left join log_granted_nilai on pengambilan_mk.id_pengambilan_mk=log_granted_nilai.id_pengambilan_mk 
					and pengambilan_mk.id_mhs=log_granted_nilai.id_mhs and pengambilan_mk.id_semester=log_granted_nilai.id_semester
					and log_granted_nilai.status=1
					where nim_mhs='$nim' 
					and id_program_studi in(select id_program_studi from program_studi where id_fakultas=$kd_fak)
					order by thn_akademik_semester,group_semester,nm_semester,kd_mata_kuliah"));
					


$smarty->display("sample/penilaian/granted-nilai.tpl");
?>