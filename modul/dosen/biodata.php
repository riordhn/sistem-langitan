<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$smarty->assign('title', 'Info Dosen');

$smarty->display('sample/biodata/info_dosen.tpl');
?>