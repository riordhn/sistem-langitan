<?php
include 'config.php';
include 'proses/training.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

//memanggil class training dan class file operasi
$training = new training($db, $login->id_dosen);

$mode = get('mode','view');
if ($request_method == 'POST')
{
	if (post('mode') == 'add')
	{
		
		$training->insert($_POST['nama'], $_POST['jenis'], $_POST['tahun'], $_POST['instansi'], $_POST['tingkat'], $_POST['peran']);
	}
	else if(post('mode')=='edit')
	{	
		$training->update($_POST['id_training'], $_POST['nama'], $_POST['jenis'], $_POST['tahun'], $_POST['instansi'], $_POST['tingkat'], $_POST['peran']);
	}
	else if(post('mode')=='delete')
	{
		$training->delete_training(post('id'));
	}

}
if($mode=='detail'||$mode=='edit'||$mode=='upload'||$mode=='delete'){
	$smarty->assign('data_training',$training->get_training(get('id')));
}else{
	$smarty->assign('data_training', $training->load_training());
}


$smarty->display("sample/biodata/{$mode}_training.tpl");

?>