<?php

include('config.php');


if (isset($_GET['fak'])) {
    if ($_GET['fak'] == '') {
        $where = "";
    } else {
        $where = "and program_studi.id_fakultas = '$_GET[fak]'";
    }
    $db->Query("SELECT * FROM FAKULTAS WHERE ID_FAKULTAS= '$_GET[fak]'");
    $smarty->assign('fakultas', $db->FetchAssoc());
    $mhs = $db->QueryToArray("select nim_mhs, nm_pengguna, nm_program_studi, IPK, SKS, NM_SEMESTER, THN_AKADEMIK_SEMESTER, TAHUN_AJARAN
									from mahasiswa 
									left join pengguna on pengguna.id_pengguna = mahasiswa.id_pengguna
									left join program_studi on mahasiswa.id_program_Studi = program_studi.id_program_studi
									left join pengambilan_mk on pengambilan_mk.id_mhs = mahasiswa.id_mhs
									left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk = pengambilan_mk.id_kurikulum_mk
									left join kelas_mk on kelas_mk.id_kelas_mk = pengambilan_mk.id_kelas_mk
									left join mata_kuliah on mata_kuliah.id_mata_kuliah = kurikulum_mk.id_mata_kuliah
									left join semester on semester.id_semester = pengambilan_mk.id_semester
									LEFT JOIN (select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, 
												sum(kredit_semester) as sks, id_mhs
										from (select a.id_mhs,nilai_standar_nilai,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
										row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
										count(*) over(partition by c.nm_mata_kuliah) terulang
											from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g
											where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah 
											and a.id_semester=e.id_semester
											and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs
										) x
										where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null 
										and nilai_huruf != '-'
										group by id_mhs) C ON C.ID_MHS = MAHASISWA.ID_MHS
									where status_aktif_semester = 'True' and status_mkta = 2 and STATUS_APV_PENGAMBILAN_MK='1'
									$where
							order by program_studi.id_fakultas, nm_program_studi, nim_mhs
							");
    $smarty->assign('mhs', $mhs);
} else {
    $fakultas = $db->QueryToArray("select nm_fakultas, a.* from fakultas
							left join (
									select fakultas.id_fakultas, count(pengambilan_mk.id_mhs) as JML_PESERTA
									from fakultas 
									left join program_studi on program_studi.id_fakultas = fakultas.id_fakultas
									left join mahasiswa on mahasiswa.id_program_Studi = program_studi.id_program_studi
									left join pengambilan_mk on pengambilan_mk.id_mhs = mahasiswa.id_mhs
									left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk = pengambilan_mk.id_kurikulum_mk
									left join kelas_mk on kelas_mk.id_kelas_mk = pengambilan_mk.id_kelas_mk
									left join mata_kuliah on mata_kuliah.id_mata_kuliah = kurikulum_mk.id_mata_kuliah
									left join semester on semester.id_semester = pengambilan_mk.id_semester
									where status_aktif_semester = 'True' and status_mkta = 2 and STATUS_APV_PENGAMBILAN_MK='1'
									group by fakultas.id_fakultas, nm_fakultas
							) a on a.id_fakultas = fakultas.id_fakultas
							order by fakultas.id_fakultas
							");
    $smarty->assign('fakultas', $fakultas);
}

$smarty->display("sample/penilaian/aktivitas-kkn-mhs.tpl");
?>