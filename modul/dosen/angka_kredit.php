<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$smarty->assign('TITLE', 'Rekap Angka Kredit');

//detail dosen
require_once 'proses/info_dosen.class.php';
$info_dosen = new info_dosen($db, $login->id_pengguna);
$data_info_dosen = $info_dosen->set();
$info_dosen->set();
$smarty->assign(array(
    'nama' => $data_info_dosen['NAMA'],
    'nip' => $data_info_dosen['NIP'],
    'fakultas' => $data_info_dosen['FAKULTAS'],
    'prodi' => $data_info_dosen['PRODI']
));

//angka kredit dosen
require_once 'proses/angka_kredit.class.php';
$angka_kredit = new angka_kredit($db);
$smarty->assign('isi_data', $angka_kredit->cetak_angka_kredit($login->id_dosen));

$smarty->display('sample/angka_kredit/angka_kredit.tpl');
?>