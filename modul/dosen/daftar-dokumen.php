<?php

include 'config.php';
include 'proses/aims.class.php';

$aims = new aims($db, $user->ID_DOSEN);

$smarty->assign('data_dokumen', $aims->load_dokumen());
//$smarty->assign('tutup',  alert_error("Mohon Maaf Untuk Sementara Halaman Ini Dalam Masa Perbaikan....", "90"))

$smarty->assign('lingkup_jabatan', $aims->get_lingkup_jabatan($user->ID_JABATAN_PEGAWAI));
$smarty->display('sample/aims/pedoman-prosedur.tpl');
?>