<?php
include 'config.php';
include 'proses/perwalian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$perwalian = new perwalian($db, $user->ID_PENGGUNA, $login->id_dosen);
if (isset($_GET['mode'])) {
    if (get('mode') == 'detail') {
        $data_biodata = $perwalian->get_biodata_mahasiswa(get('id_mhs'));
        if (file_exists("../../foto_mhs/{$nama_singkat}/{$data_biodata['NIM_MHS']}.JPG"))
            $smarty->assign('foto', "../../foto_mhs/{$nama_singkat}/{$data_biodata['NIM_MHS']}.JPG");
        else if (file_exists("../../foto_mhs/{$nama_singkat}/{$data_biodata['NIM_MHS']}.jpg"))
            $smarty->assign('foto', "../../foto_mhs/{$nama_singkat}/{$data_biodata['NIM_MHS']}.jpg");
        else $smarty->assign('foto', "../../foto_mhs/foto-tidak-ditemukan.png"); 
        $smarty->assign('data_biodata', $data_biodata);
		//$perwalian->load_mahasiswa_krs();
		//$data_biodata = $perwalian->get_biodata_mahasiswa(get('id_mhs'));
		//$smarty->assign('mobile', $data_biodata['NIM_MHS']);
        $smarty->assign('data_admisi', $perwalian->load_admisi_mhs(get('id_mhs')));
        $smarty->assign('data_mhs_status', $perwalian->load_mhs_status(get('id_mhs')));
        $smarty->assign('data_pembayaran', $perwalian->load_history_bayar_mhs(get('id_mhs')));
        $smarty->assign('data_beasiswa', $perwalian->load_sejarah_beasiswa(get('id_mhs')));
        $smarty->assign('data_bidik_misi_keu', $perwalian->load_bidik_misi_keuangan(get('id_mhs')));
        $smarty->assign('data_bidik_misi_kegiatan', $perwalian->load_bidik_misi_kegiatan(get('id_mhs')));
        $smarty->assign('data_history_nilai', $perwalian->load_transkrip(get('id_mhs')));
        $smarty->assign('data_khp', $perwalian->load_data_khp_mhs(get('id_mhs')));
    }
}

$smarty->assign('data_mahasiswa_wali', $perwalian->load_mahasiswa_wali());
$smarty->assign('id_fakultas', $login->id_fakultas);
$smarty->display('display-bimbingan/mahasiswa-wali.tpl');
?>