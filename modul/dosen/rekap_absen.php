<?php
include 'config.php';
include 'proses/presensi.class.php';
include 'proses/penilaian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$presensi = new presensi($db, $login->id_dosen);
$nilai = new penilaian($db, $user->ID_DOSEN);
if (isset($_POST)) {
    if (post('id_kelas_mk')) {
        $smarty->assign('data_pertemuan', $presensi->load_pertemuan(post('id_kelas_mk')));
        $smarty->assign('data_presensi_mhs',$presensi->load_rekap_presensi_mhs(post('id_kelas_mk')));
    }
}
$smarty->assign('id_kelas_mk', post('id_kelas_mk'));
$smarty->assign('data_kelas_mk', $nilai->load_kelas_mata_kuliah());
$smarty->display('display-presensi/rekap_absen.tpl');
?>