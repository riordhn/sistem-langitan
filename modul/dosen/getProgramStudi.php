<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$program_studi_set = $db->QueryToArray("
    SELECT ID_PROGRAM_STUDI, '( ' || NM_JENJANG || ') ' || NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI FROM PROGRAM_STUDI PS
    LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
    WHERE ID_FAKULTAS = {$_REQUEST['id_fakultas']} AND PS.ID_PROGRAM_STUDI <> 197");

echo "<option value=\"\">Pilih</option>";

foreach ($program_studi_set as $ps)
    echo "<option value=\"{$ps['ID_PROGRAM_STUDI']}\">" . ucwords(strtolower($ps['NM_PROGRAM_STUDI'])) . "</option>";
?>
