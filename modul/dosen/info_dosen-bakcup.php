<?php

include 'config.php';
include 'proses/info_dosen.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}


$mode = get('mode', 'view');

$info_dosen = new info_dosen($db, $login->id_pengguna);

if ($mode == 'edit') {
    $smarty->assign('data_agama', $info_dosen->get_agama());
    $smarty->assign('data_kota', $info_dosen->get_kota());
    $smarty->assign('count_provinsi', count($info_dosen->get_kota()));
}

if (isset($_POST)) {
    if ($_POST['mode'] == 'update') {
        $info_dosen->update($_POST['nama'], $_POST['gelar_depan'], $_POST['gelar_belakang'], $_POST['tempat_lahir'], $_POST['tgl_lahir'], $_POST['agama'], $_POST['kelamin'], $_POST['email'], $_POST['blog'], $_POST['alamat_rumah'], $_POST['alamat_kantor'], $_POST['telp'], $_POST['bidang_keahlian'], $login->id_pengguna);
    }
}

$smarty->assign('is_dekan', $user->IsDekan());
$smarty->assign('dosen', $info_dosen->get_biodata());
$smarty->display("sample/biodata/infodosen/{$mode}.tpl");
?>