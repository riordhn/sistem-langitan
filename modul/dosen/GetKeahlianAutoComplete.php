<?php

include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}

$cari = str_replace('+', ' ', $_REQUEST['term']);
$query = "
            SELECT P.GELAR_DEPAN,P.GELAR_BELAKANG,P.NM_PENGGUNA,D.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            FROM DOSEN D
            JOIN PENGGUNA P ON D.ID_PENGGUNA=P.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            WHERE UPPER(P.NM_PENGGUNA) LIKE UPPER('%{$cari}%') OR D.NIP_DOSEN LIKE '%{$cari}%' OR P.USERNAME LIKE '%{$cari}%' OR UPPER(D.BIDANG_KEAHLIAN_DOSEN) LIKE UPPER('%{$cari}%')
            ";
$data_pengguna = $db->QueryToArray("
    SELECT * FROM (
    {$query}
    ) WHERE ROWNUM<1000");
$data_hasil = array();
foreach ($data_pengguna as $data) {
    if ($data['BIDANG_KEAHLIAN_DOSEN'] != '') {
        array_push($data_hasil, array(
            'value' => $data['GELAR_DEPAN'] . " " . strtoupper($data['NM_PENGGUNA']) . " " . $data['GELAR_BELAKANG'] . "  (" . strtoupper($data['BIDANG_KEAHLIAN_DOSEN']) . ") ",
            'id' => (int) $data['ID_PENGGUNA']
        ));
    } else {
        array_push($data_hasil, array(
            'value' => $data['GELAR_DEPAN'] . " " . strtoupper($data['NM_PENGGUNA']) . " " . $data['GELAR_BELAKANG'] . "  ( Bidang Keahlian Kosong) ",
            'id' => (int) $data['ID_PENGGUNA']
        ));
    }
}
echo json_encode($data_hasil)
?>
