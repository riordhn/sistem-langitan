<?php
include 'config.php';
include 'proses/pesan.class.php';
include 'funtion-tampil-informasi.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$reply = new pesan($db, $login->id_dosen, $login->id_pengguna);
if (isset($_POST['mode'])) {
    if ($_POST['mode'] == 'save') {
        $reply->insert_pesan($_POST['id_penerima'], $_POST['subject'], $_POST['isi'], $_POST['id_balasan']);
        $smarty->assign('status', alert_success("Pesan berhasil dikirim..", "40"));;
    }
}
$data_reply = $reply->load_pesan(get('id'));
$smarty->assign('title', 'Reply Message');
$smarty->assign('data_reply', $data_reply);
$smarty->display('display-konsultasi/reply.tpl');
?>