<?php
include 'config.php';
include 'proses/pesan.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$trash = new pesan($db, $login->id_dosen, $login->id_pengguna);
if (isset($_POST['mode'])) {
    if ($_POST['mode'] == 'delete') {
        $trash->delete_permanen($_POST['id']);
    } else if ($_POST['mode'] == 'restore') {
        $trash->restore_pesan($_POST['id']);
    } else if ($_POST['mode'] == 'delete_checked') {
        foreach ($_POST['id'] as $item) {
            $trash->delete_permanen($item[0]);
        }
    }
}

$data_trash = $trash->load_trash();
$smarty->assign('id_pengguna', $login->id_pengguna);
$smarty->assign('title', 'Trash');
$smarty->assign('data_trash', $data_trash);
$smarty->display('display-konsultasi/trash.tpl');
?>