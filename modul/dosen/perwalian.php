<?php
require 'config.php';
require 'proses/perwalian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}

$id_pengguna    = $user->ID_PENGGUNA;
$perwalian      = new perwalian($db, $user->ID_PENGGUNA, $login->id_dosen);
$mode           = 'perwalian';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($_POST['krs'] != '') {
        $status_krs = 0;

        for ($i = 1; $i <= $_POST['sum']; $i++) {
            if (isset($_POST['confirm' . $i])) {
                $status_krs++;
            }
        }

        if ($status_krs == $_POST['sum']) {
            for ($i = 1; $i <= $_POST['sum']; $i++) {
                $perwalian->update($_POST['confirm' . $i], '1');
            }
        } else {
            for ($i = 1; $i <= $_POST['sum']; $i++) {
                if (isset($_POST['confirm' . $i])) {
                    $perwalian->update($_POST['confirm' . $i], '1');
                } else {
                    $perwalian->update($_POST['not_confirm' . $i], '0');
                }
            }
        }

        // update tabel MAHASISWA_STATUS
        $perwalian->update_status_mhs($_POST['id_mhs'], $_POST['id_semester']);

        $smarty->assign('data_mahasiswa_krs', $perwalian->load_mahasiswa_krs());
        $smarty->assign('title', 'Perwalian');
        
        $mode = 'perwalian';
    } elseif ($_POST['detail'] != '') {
        if ($_POST['detail'] == 'krs') {
            $smarty->assign('id_mhs', $_POST['id_mhs']);
            $smarty->assign('id_fak', $user->ID_FAKULTAS);
            $smarty->assign('data_mahasiswa', $perwalian->load_mhs_status($_POST['id_mhs']));
            $smarty->assign('data_transkrip', $perwalian->load_transkrip($_POST['id_mhs']));
            $smarty->assign('data_pesan_krs', $perwalian->load_krs_message($_POST['id_mhs']));
            // tambahan ambil jumlah tagihan
            $smarty->assign('data_tagihan_mahasiswa', $perwalian->get_jumlah_tagihan_mahasiswa($_POST['id_mhs']));
            $smarty->assign('data_aktivitas_mahasiswa', $perwalian->get_detail_aktivitas($_POST['id_mhs']));


            $db->Query("SELECT id_semester FROM semester WHERE id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND status_aktif_semester = 'True'");
            $semester_aktif = $db->FetchAssoc();
            $id_smt = $semester_aktif['ID_SEMESTER'];

            if ($_POST['st'] == 1) {
                $id_pmk = $_POST['id_pmk'];
                $user = $user->ID_PENGGUNA;
                //echo "delete from pengambilan_mk where id_pengambilan_mk=$id_pmk";
                $db->Query("insert into log_del_pmk (id_pengambilan_mk,id_kurikulum_mk,id_kelas_mk,id_mhs,id_semester,created_on,id_pengguna)
                select id_pengambilan_mk,id_kurikulum_mk,id_kelas_mk,id_mhs,id_semester,created_on,$user from pengambilan_mk where id_pengambilan_mk=$id_pmk");
                $db->Query("delete from pengambilan_mk where id_pengambilan_mk=$id_pmk");
                
                // tujuan halaman setelah hapus krs
                $mode = 'load_krs';
            } elseif ($_POST['st'] == 2) {
                $id_kelas_mk = $_POST['id_kelas_mk'];

                $id_mhs = $_POST['id_mhs'];

                // ambil jumlah SKS matkul yg dipilih
                $db->Query("SELECT SUM(mk.kredit_semester) AS JML_SKS
                                FROM kelas_mk kmk
                                JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
                                WHERE kmk.id_kelas_mk = '{$id_kelas_mk}'");
                $sks = $db->FetchAssoc();

                $jml_sks_dipilih = $sks['JML_SKS'];

                // Checking JUMLAH KRS By FIKRIE (31-01-2020)
                $db->Query("SELECT SUM(COALESCE(mk1.kredit_semester, mk2.kredit_semester)) AS JML_KRS
                                FROM pengambilan_mk
                                JOIN mahasiswa ON mahasiswa.id_mhs = pengambilan_mk.id_mhs
                                -- Via Kelas
                                LEFT JOIN kelas_mk      ON pengambilan_mk.id_kelas_mk = kelas_mk.id_kelas_mk
                                LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kelas_mk.id_mata_kuliah
                                LEFT JOIN nama_kelas    ON kelas_mk.no_kelas_mk = nama_kelas.id_nama_kelas
                                -- Via Kurikulum
                                LEFT JOIN kurikulum_mk  ON pengambilan_mk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk
                                LEFT JOIN mata_kuliah mk2 ON kurikulum_mk.id_mata_kuliah = mk2.id_mata_kuliah
                                WHERE pengambilan_mk.id_semester = '{$id_smt}' AND
                                    -- status_apv_pengambilan_mk = 1 AND 
                                    pengambilan_mk.status_hapus = 0 AND 
                                    pengambilan_mk.status_pengambilan_mk != 0 AND
                                    mahasiswa.id_mhs = '{$id_mhs}'");
                $krs = $db->FetchAssoc();

                $jml_krs_smt = 0;

                if (! empty($krs['JML_KRS'])) {
                    $jml_krs_smt = $krs['JML_KRS'];
                }

                $jml_krs = $jml_krs_smt + $jml_sks_dipilih;

                if($jml_krs > 24) {
                    $smarty->assign('keterangan_krs', alert_error_khusus('!!! Mahasiswa Tidak Diperbolehkan Mengambil <b>Lebih Dari 24 SKS Dalam Satu Semester</b>. Terima Kasih !!!', 88));
                }   
                else {
                    $db->Query("insert into pengambilan_mk (id_kurikulum_mk,id_kelas_mk,id_mhs,id_semester,status_apv_pengambilan_mk,status_pengambilan_mk)
                    select id_kurikulum_mk,id_kelas_mk,$id_mhs,$id_smt,1,8 from kelas_mk where id_kelas_mk=$id_kelas_mk");
                }

                // tujuan halaman setelah tambah krs
                $mode = 'load_krs';
            } elseif ($_POST['st'] == 'load_kelas') {
                $id_mhs = $_POST['id_mhs'];

                $db->Query("SELECT id_program_studi FROM mahasiswa WHERE id_mhs = '{$id_mhs}'");
                $prodi = $db->FetchAssoc();
                $id_program_studi = $prodi['ID_PROGRAM_STUDI'];

                $ips = $perwalian->get_ips_mhs($id_mhs, $perwalian->get_semester_kemarin());

                // Daftar Kelas
                $kelas_set = $db->QueryToArray(
                    "SELECT
                    kls.id_kelas_mk,
                    mk.kd_mata_kuliah, mk.nm_mata_kuliah, nk.nama_kelas, mk.kredit_semester, 
                    kmk.tingkat_semester,
                    kls.kapasitas_kelas_mk AS kapasitas,
                    (select count(*) from pengambilan_mk pmk where pmk.id_kelas_mk = kls.id_kelas_mk) as isi,
                    kmk.status_mkta,
                    (select count(*) from pengampu_mk pmk where pmk.id_kelas_mk = kls.id_kelas_mk and pjmk_pengampu_mk = 1) as jml_dosen,
                    (select count(*) from jadwal_kelas jdw where jdw.id_kelas_mk = kls.id_kelas_mk) as jml_jadwal,
                    (select count(*) from jadwal_kelas jdw join ruangan r on r.id_ruangan = jdw.id_ruangan where jdw.id_kelas_mk = kls.id_kelas_mk) as jml_ruangan
                  FROM kelas_mk kls
                  JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = kls.id_kurikulum_mk
                  JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
                  LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kls.no_kelas_mk
                  WHERE 
                    kls.id_program_studi = '{$id_program_studi}' AND
                    kls.id_semester = '{$id_smt}' 
                    AND mk.id_mata_kuliah NOT IN (SELECT MK.ID_MATA_KULIAH
                          FROM PENGAMBILAN_MK PMK
                          JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
                          JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PMK.ID_KELAS_MK
                          LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
                          JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
                          JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
                          JOIN SEMESTER S ON PMK.ID_SEMESTER =  S.ID_SEMESTER
                          LEFT JOIN BEBAN_SKS BS ON BS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI AND BS.IPK_MINIMUM<='{$ips}'
                      WHERE M.ID_MHS = '{$id_mhs}' AND S.STATUS_AKTIF_SEMESTER='True' AND PMK.STATUS_PENGAMBILAN_MK !=0)
                  ORDER BY mk.nm_mata_kuliah"
                );

                $smarty->assign('kelas_set', $kelas_set);

                // menampilkan kelas tersedia
                $mode = 'load_kelas';
            } else {
                $mode = 'load_krs';
            }

            // pembatasan perwalian krs dosen
            $is_krs = 1;

            $tgl = $perwalian->get_tgl_selesai_krs();
            $timestamp = strtotime($tgl);

            $batas_krs = strtotime("+1 month 1 day", $timestamp);

            if ($batas_krs < strtotime("now")) {
                $is_krs = 0;
            }

            $smarty->assign('is_krs', $is_krs);
            $smarty->assign('data_krs', $perwalian->load_krs($_POST['id_mhs']));
            $smarty->assign('sum_data', count($perwalian->load_krs($_POST['id_mhs'])));
        }
    } elseif ($_POST['send_message'] != '') {
        $perwalian->send_message_krs($_POST['id_mhs'], $_POST['judul_pesan'], $_POST['isi_pesan']);
        $smarty->assign('data_pesan_krs', $perwalian->load_krs_message($_POST['id_mhs']));
        $smarty->assign('id_mhs', $_POST['id_mhs']);
        $smarty->assign('data_mahasiswa', $perwalian->load_mhs_status($_POST['id_mhs']));
        $smarty->assign('data_transkrip', $perwalian->load_transkrip($_POST['id_mhs']));

        // pembatasan perwalian krs dosen
        $is_krs = 1;

        $tgl = $perwalian->get_tgl_selesai_krs();
        $timestamp = strtotime($tgl);

        $batas_krs = strtotime("+1 month 1 day", $timestamp);

        if ($batas_krs < strtotime("now")) {
            $is_krs = 0;
        }

        $smarty->assign('is_krs', $is_krs);

        $smarty->assign('data_krs', $perwalian->load_krs($_POST['id_mhs']));
        $smarty->assign('sum_data', count($perwalian->load_krs($_POST['id_mhs'])));

        $mode = 'load_krs';
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (get('mode') == 'lihat-khs') {
        $smarty->assign('data_mahasiswa', $perwalian->load_mhs_status(get('id_mhs')));
        $smarty->assign('data_khs', $perwalian->load_khs(get('id_mhs')));
    }
}

$smarty->assign('keterangan_perwalian', alert_success('Data yang ditampilkan adalah mahasiswa yang sudah melakukan KRS, untuk melihat semua mahasiswa bimbingan silahkan pilih menu <b>Perwalian Akademik</b> di sebelah kiri. Terima Kasih', 100));
$smarty->assign('data_mahasiswa_krs', $perwalian->load_mahasiswa_krs());
$smarty->assign('id_fakultas', $login->id_fakultas);
$smarty->assign('mode', get('mode'));
$smarty->display('display-bimbingan/' . $mode . '.tpl');
