<?php

include 'config.php';
include 'proses/kkn.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}

$kkn = new kkn($db, $login->id_dosen, $user->ID_PENGGUNA);
$peranan = $kkn->cek_peranan_dosen();
$angkatan_aktif = $kkn->get_angkatan_aktif();

if (isset($_POST)) {
    if (post('mode') == 'input_kkn') {
        for ($i = 1; $i < post('jumlah_nilai'); $i++) {
            $id_pengambilan = post('pengambilan' . $i);
            $nilai_angka = post('nilai' . $i);
            $nilai_huruf = $kkn->get_nilai_huruf_akhir($nilai_angka);
            if ($id_pengambilan != '' && $nilai_angka != '') {
                $db->Query("UPDATE PENGAMBILAN_MK SET NILAI_HURUF='{$nilai_huruf}', NILAI_ANGKA='{$nilai_angka}',FLAGNILAI=1 WHERE ID_PENGAMBILAN_MK='{$id_pengambilan}'");
            }
        }
    }
}

switch ($peranan) {
    case 1: $str_jabatan = "Anda sebagai Koordinator Kabupaten pada KKN Angkatan {$angkatan_aktif['NAMA_ANGKATAN']}";
        break;
    case 2: $str_jabatan = "Anda sebagai Koordinator Kecamatan pada KKN Angkatan {$angkatan_aktif['NAMA_ANGKATAN']}";
        break;
    case 3: $str_jabatan = "Anda sebagai Dosen Pembina Lapangan pada KKN Angkatan {$angkatan_aktif['NAMA_ANGKATAN']}";
        break;
    case 4: $str_jabatan = "Anda sebagai Koordinator Kabupaten dan Koordinator Kecamatan pada KKN Angkatan {$angkatan_aktif['NAMA_ANGKATAN']}";
        break;
    case 5: $str_jabatan = "Anda sebagai Koordinator Kabupaten dan Koordinator Kecamatan dan Dosen Pembina Lapangan pada KKN Angkatan {$angkatan_aktif['NAMA_ANGKATAN']}";
        break;
    case 6: $str_jabatan = "Anda sebagai Koordinator Kecamatan dan Dosen Pembina Lapangan pada KKN Angkatan {$angkatan_aktif['NAMA_ANGKATAN']}";
        break;
    case 7: $str_jabatan = "Anda sebagai Koordinator Kabupaten dan Dosen Pembina Lapangan pada KKN Angkatan {$angkatan_aktif['NAMA_ANGKATAN']}";
        break;
    case 0: $str_jabatan = "Anda tidak mempunyai peran pada KKN Angkatan {$angkatan_aktif['NAMA_ANGKATAN']}";
        break;
}
//echo "<pre>";
//var_dump($kkn->load_data_dpl());
//echo "</pre>";
if ($peranan == 1) {
    $smarty->assign('kota_korkab', $kkn->get_kabupaten_korkab());
    $smarty->assign('data_kec', $kkn->load_kecamatan_korkab());
    if (isset($_GET)) {
        if (get('mode') == 'tampil_kelompok') {
            $smarty->assign('data_kelompok', $kkn->load_kelompok_kkn_korkab(get('kelurahan')));
            $smarty->assign('kelurahan', $kkn->get_kelurahan_kkn(get('kelurahan')));
        }
    }
} else if ($peranan == 2) {
    $smarty->assign('kecamatan_korbing', $kkn->get_kecamatan_korbing());
    $smarty->assign('data_pembimbing', $kkn->load_data_pembimbing());
} else if ($peranan == 3) {
    $smarty->assign('data_dpl', $kkn->load_data_dpl());
} else if ($peranan == 4) {
    $smarty->assign('kecamatan_korbing', $kkn->get_kecamatan_korbing());
    $smarty->assign('kota_korkab', $kkn->get_kabupaten_korkab());
    $smarty->assign('data_kec', $kkn->load_kecamatan_korkab());
    if (isset($_GET)) {
        if (get('mode') == 'tampil_kelompok') {
            $smarty->assign('data_kelompok', $kkn->load_kelompok_kkn_korkab(get('kelurahan')));
            $smarty->assign('kelurahan', $kkn->get_kelurahan_kkn(get('kelurahan')));
        }
    }
} else if ($peranan == 5) {
    $smarty->assign('kota_korkab', $kkn->get_kabupaten_korkab());
    $smarty->assign('kecamatan_korbing', $kkn->get_kecamatan_korbing());
    $smarty->assign('kelurahan_dpl', $kkn->get_kelurahan_dpl());
    $smarty->assign('data_kec', $kkn->load_kecamatan_korkab());
    if (isset($_GET)) {
        if (get('mode') == 'tampil_kelompok') {
            $smarty->assign('status_dpl', $kkn->get_status_dpl() == get('kelurahan') ? 1 : 0);
            $smarty->assign('data_kelompok', $kkn->load_kelompok_kkn_korkab(get('kelurahan')));
            $smarty->assign('kelurahan', $kkn->get_kelurahan_kkn(get('kelurahan')));
        }
    }
} else if ($peranan == 6) {
    $smarty->assign('kecamatan_korbing', $kkn->get_kecamatan_korbing());
    $smarty->assign('kelurahan_dpl', $kkn->get_kelurahan_dpl());
    $smarty->assign('data_pembimbing', $kkn->load_data_pembimbing());
} else if ($peranan == 7) {
    $smarty->assign('kota_korkab', $kkn->get_kabupaten_korkab());
    $smarty->assign('kelurahan_dpl', $kkn->get_kelurahan_dpl());
    $smarty->assign('data_kec', $kkn->load_kecamatan_korkab());
    if (isset($_GET)) {
        if (get('mode') == 'tampil_kelompok') {
            $smarty->assign('status_dpl', $kkn->get_status_dpl() == get('kelurahan') ? 1 : 0);
            $smarty->assign('data_kelompok', $kkn->load_kelompok_kkn_korkab(get('kelurahan')));
            $smarty->assign('kelurahan', $kkn->get_kelurahan_kkn(get('kelurahan')));
        }
    }
}
$smarty->assign('peranan', $peranan);
$smarty->assign('str_jabatan', alert_success($str_jabatan, 98));
$smarty->display('sample/penilaian/kontrol-kkn.tpl');
?>
