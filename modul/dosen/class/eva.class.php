<?php

class eva {

    public $db;
    private $id_pengguna;

    function __construct($db, $id_pengguna) {
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
    }

    function GetSemesterAktif() {
        return $this->db->QuerySingle("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True'");
    }

    function GetSemesterSebelum() {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil'");
            $semester_kemarin = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1)");
            $semester_kemarin = $this->db->FetchAssoc();
        }
        return $semester_kemarin['ID_SEMESTER'];
    }
    function GetSemesterSetelah() {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Ganjil' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'+1)");
            $semester_setelah = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}'");
            $semester_setelah = $this->db->FetchAssoc();
        }
        return $semester_setelah;
    }

    function CekPembayaran($id_mhs, $semester_aktif) {
        $tagihan = $this->db->QuerySingle("SELECT COUNT(*) FROM AUCC.PEMBAYARAN WHERE ID_SEMESTER='{$semester_aktif}' AND ID_MHS='{$id_mhs}' AND ID_STATUS_PEMBAYARAN=2");
        return ($tagihan > 0);
    }

    function GetPeriodeWisudaAktif() {
        return $this->db->QuerySingle("
                SELECT ID_TARIF_WISUDA FROM AUCC.TARIF_WISUDA 
                WHERE ID_TARIF_WISUDA IN (
                  SELECT ID_TARIF_WISUDA 
                  FROM AUCC.PERIODE_WISUDA 
                  WHERE STATUS_AKTIF=1
                )
            ");
    }

    function LoadKelasMahasiswa($semester) {
        $fakultas = $this->db->QuerySingle("SELECT ID_FAKULTAS FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM MAHASISWA WHERE ID_PENGGUNA='{$this->id_pengguna}')");
        if ($fakultas == 1) {
            $query = "
                SELECT 
                    KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH,NVL(K1.NMKELAS,K2.NAMA_KELAS) NM_KELAS,MK.KD_MATA_KULIAH,P.NM_PENGGUNA,MK.STATUS_PRAKTIKUM,PMK.FLAGPBM,D.ID_DOSEN,
                    (SELECT COUNT(ID_EVAL_HASIL) FROM EVALUASI_HASIL WHERE ID_MHS=M.ID_MHS AND ID_SEMESTER='{$semester}' AND ID_KELAS_MK=PMK.ID_KELAS_MK AND ID_DOSEN=D.ID_DOSEN) STATUS_ISI
                FROM AUCC.PENGAMBILAN_MK PMK
                JOIN AUCC.MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
                JOIN AUCC.PENGAMPU_MK PENGMK ON PMK.ID_KELAS_MK=PENGMK.ID_KELAS_MK
                JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
                JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
                JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
                LEFT JOIN AUCC.FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
                LEFT JOIN AUCC.NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
                JOIN AUCC.SEMESTER S ON PMK.ID_SEMESTER = S.ID_SEMESTER
                JOIN AUCC.DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
                JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
                WHERE M.ID_PENGGUNA='{$this->id_pengguna}' AND S.ID_SEMESTER='{$semester}'
                ORDER BY NM_MATA_KULIAH,NM_KELAS
                ";
        } else {
            $query = "
                SELECT 
                    KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH,NVL(K1.NMKELAS,K2.NAMA_KELAS) NM_KELAS,MK.KD_MATA_KULIAH,P.NM_PENGGUNA,MK.STATUS_PRAKTIKUM,KUMK.STATUS_MKTA,PMK.FLAGPBM,D.ID_DOSEN,
                    (SELECT COUNT(ID_EVAL_HASIL) FROM AUCC.EVALUASI_HASIL WHERE ID_MHS=M.ID_MHS AND ID_SEMESTER='{$semester}' AND ID_KELAS_MK=PMK.ID_KELAS_MK AND ID_DOSEN=D.ID_DOSEN) STATUS_ISI
                FROM AUCC.PENGAMBILAN_MK PMK
                JOIN AUCC.MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
                JOIN (
                    SELECT PK.ID_KELAS_MK,PMK.ID_DOSEN 
                    FROM AUCC.PRESENSI_KELAS PK
                    JOIN AUCC.PRESENSI_MKDOS PMK ON PK.ID_PRESENSI_KELAS=PMK.ID_PRESENSI_KELAS
                    WHERE PMK.KEHADIRAN=1 AND PMK.ID_DOSEN!=0
                    GROUP BY PK.ID_KELAS_MK,PMK.ID_DOSEN 
                    ORDER BY ID_KELAS_MK
                ) PENGMK ON PMK.ID_KELAS_MK=PENGMK.ID_KELAS_MK
                JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
                JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
                JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
                LEFT JOIN AUCC.FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
                LEFT JOIN AUCC.NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
                JOIN AUCC.SEMESTER S ON PMK.ID_SEMESTER = S.ID_SEMESTER
                JOIN AUCC.DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
                JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
                WHERE M.ID_PENGGUNA='{$this->id_pengguna}' AND S.ID_SEMESTER='{$semester}' AND KUMK.STATUS_MKTA IN (0,5,6)
                ORDER BY NM_MATA_KULIAH,NM_KELAS";
        }
        return $this->db->QueryToArray($query);
    }

    function LoadEvaluasiAspek($id_instrumen) {
        $data_instrumen = array();
        $data_kelompok_aspek = $this->db->QueryToArray("SELECT * FROM EVALUASI_KELOMPOK_ASPEK WHERE ID_EVAL_INSTRUMEN='{$id_instrumen}' ORDER BY ID_EVAL_KELOMPOK_ASPEK");
        $index_kelompok = 0;
        foreach ($data_kelompok_aspek as $dk) {
            $data_aspek = $this->db->QueryToArray("SELECT * FROM EVALUASI_ASPEK WHERE ID_EVAL_KELOMPOK_ASPEK='{$dk['ID_EVAL_KELOMPOK_ASPEK']}' AND STATUS_AKTIF=0 ORDER BY URUTAN,ID_EVAL_ASPEK");
            array_push($data_instrumen, array_merge($dk, array('DATA_ASPEK' => $data_aspek)));
            $index_aspek = 0;
            foreach ($data_aspek as $da) {
                array_push($data_instrumen[$index_kelompok]['DATA_ASPEK'][$index_aspek], $this->db->QueryToArray("SELECT * FROM EVALUASI_NILAI WHERE ID_KELOMPOK_NILAI='{$da['ID_KELOMPOK_NILAI']}' ORDER BY URUTAN"))
                ;
                $index_aspek++;
            }
            $index_kelompok++;
        }
        return $data_instrumen;
    }

    function SaveEvaluasiKuliah($instrumen, $kel_aspek, $aspek, $id_mhs, $id_dosen, $prodi, $fakultas, $kelas, $semester, $nilai) {
        $this->db->Query("
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_MHS,ID_DOSEN,ID_PROGRAM_STUDI,ID_FAKULTAS,ID_KELAS_MK,ID_SEMESTER,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_mhs}','{$id_dosen}','{$prodi}','{$fakultas}','{$kelas}','{$semester}','{$nilai}')");
    }

    function CekPengisianEvaluasiKuliah($instrumen, $kelas, $id_dosen, $id_mhs, $semester) {
        //echo "SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN='{$instrumen}' AND ID_KELAS_MK='{$kelas}' AND ID_DOSEN='{$id_dosen}' AND ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester}'";
        return $this->db->QuerySingle("SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN IN (1,2) AND ID_KELAS_MK='{$kelas}' AND ID_DOSEN='{$id_dosen}' AND ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester}'");
    }

    function CekPengisianSemuaEvaluasiKuliah($id_mhs, $semester, $total_mk) {
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM
            (
                SELECT ID_KELAS_MK,ID_DOSEN,ID_MHS,ID_SEMESTER FROM AUCC.EVALUASI_HASIL 
                WHERE ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester}' AND (ID_EVAL_INSTRUMEN=1 OR ID_EVAL_INSTRUMEN=2)
                GROUP BY ID_KELAS_MK,ID_DOSEN,ID_MHS,ID_SEMESTER
            )");
        return ($jumlah_pengisian >= $total_mk && $total_mk != 0) || (($jumlah_pengisian == 0 || $jumlah_pengisian >= 0) && $total_mk == 0);
    }

    function CekPengisianEvaluasiWali($id_mhs, $id_dosen, $semester) {
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=3 AND ID_MHS='{$id_mhs}' AND ID_DOSEN='{$id_dosen}' AND ID_SEMESTER='{$semester}'
        ");
        return ($jumlah_pengisian > 0);
    }

    function SaveEvaluasiWali($instrumen, $kel_aspek, $aspek, $id_mhs, $id_dosen, $prodi, $fakultas, $semester, $nilai) {
        $this->db->Query("
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_MHS,ID_DOSEN,ID_PROGRAM_STUDI,ID_FAKULTAS,ID_SEMESTER,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_mhs}','{$id_dosen}','{$prodi}','{$fakultas}','{$semester}','{$nilai}')");
    }

    function CekPengisianEvaluasiAdmFak($id_mhs, $semester) {
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=6 AND ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester}'
        ");
        return ($jumlah_pengisian > 0);
    }

    function SaveEvaluasiAdmFak($instrumen, $kel_aspek, $aspek, $id_mhs, $prodi, $fakultas, $semester, $nilai) {
        $this->db->Query("
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_MHS,ID_PROGRAM_STUDI,ID_FAKULTAS,ID_SEMESTER,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_mhs}','{$prodi}','{$fakultas}','{$semester}','{$nilai}')");
    }

    function CekPengisianEvaluasiMaba($id_mhs) {
        //echo "SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='{$id_mhs}'";
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='{$id_mhs}'
        ");
        return ($jumlah_pengisian > 0);
    }

    function SaveEvaluasiMaba($instrumen, $kel_aspek, $aspek, $id_mhs, $prodi, $fakultas, $semester, $nilai) {
        $this->db->Query("
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_MHS,ID_PROGRAM_STUDI,ID_FAKULTAS,ID_SEMESTER,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_mhs}','{$prodi}','{$fakultas}','{$semester}','{$nilai}')");
    }

    function CekPengisianEvaluasiWisuda($id_mhs) {
        //echo "SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='{$id_mhs}'";
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=5 AND ID_MHS='{$id_mhs}'
        ");
        return ($jumlah_pengisian > 0);
    }

    function SaveEvaluasiWisuda($instrumen, $kel_aspek, $aspek, $id_mhs, $prodi, $fakultas, $periode, $nilai) {
        $this->db->Query("
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_MHS,ID_PROGRAM_STUDI,ID_FAKULTAS,ID_TARIF_WISUDA,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_mhs}','{$prodi}','{$fakultas}','{$periode}','{$nilai}')");
    }
    function SaveEvaluasiKadep($instrumen, $kel_aspek, $aspek, $id_dosen, $prodi, $fakultas, $semester, $nilai) {
        $this->db->Query("
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_DOSEN,ID_PROGRAM_STUDI,ID_FAKULTAS,ID_SEMESTER,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_dosen}','{$prodi}','{$fakultas}','{$semester}','{$nilai}')");
echo "
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_DOSEN,ID_PROGRAM_STUDI,ID_FAKULTAS,ID_SEMESTER,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_dosen}','{$prodi}','{$fakultas}','{$semester}','{$nilai}')";
    }
    function SaveEvaluasiKadep1($id_dosen,$semester,$a,$fakultas,$prodi) {
        $this->db->Query("
            INSERT INTO hasil_ev_kadep()
            VALUES
                ('{$id_dosen}','{$semester}','{$a[1]}',	'{$a[2]}',	'{$a[3]}',	'{$a[4]}',	'{$a[5]}',	'{$a[6]}',	'{$a[7]}',	'{$a[8]}',	'{$a[9]}',	'{$a[10]}',	'{$a[11]}',	'{$a[12]}',	'{$a[13]}',	'{$a[14]}',	'{$a[15]}',	'{$a[16]}',	'{$a[17]}',	'{$a[18]}',	'{$a[19]}',	'{$a[20]}',	'{$a[21]}',	'{$a[22]}',	'{$a[23]}',	'{$a[24]}',	'{$a[25]}',	'{$a[26]}',	'{$a[27]}',	'{$a[28]}',	'{$a[29]}'
,'{$fakultas}','{$prodi}')");
     echo "
            INSERT INTO hasil_ev_kadep()
            VALUES
                ('{$id_dosen}','{$semester}','{$a[1]}',	'{$a[2]}',	'{$a[3]}',	'{$a[4]}',	'{$a[5]}',	'{$a[6]}',	'{$a[7]}',	'{$a[8]}',	'{$a[9]}',	'{$a[10]}',	'{$a[11]}',	'{$a[12]}',	'{$a[13]}',	'{$a[14]}',	'{$a[15]}',	'{$a[16]}',	'{$a[17]}',	'{$a[18]}',	'{$a[19]}',	'{$a[20]}',	'{$a[21]}',	'{$a[22]}',	'{$a[23]}',	'{$a[24]}',	'{$a[25]}',	'{$a[26]}',	'{$a[27]}',	'{$a[28]}',	'{$a[29]}'
,'{$fakultas}','{$prodi}')";
    }
//
 function SaveEvaluasiAdmdep($instrumen, $kel_aspek, $aspek, $id_dosen, $prodi, $fakultas, $semester, $nilai) {
        $this->db->Query("
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_DOSEN,ID_PROGRAM_STUDI,ID_FAKULTAS,ID_SEMESTER,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_dosen}','{$prodi}','{$fakultas}','{$semester}','{$nilai}')");
echo "
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_DOSEN,ID_PROGRAM_STUDI,ID_FAKULTAS,ID_SEMESTER,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_dosen}','{$prodi}','{$fakultas}','{$semester}','{$nilai}')";
    }

//
   function CekPengisianEvaluasiKadep($id_dosen,$id_semester) {
        //echo "SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='{$id_mhs}'";
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=12 AND ID_DOSEN='{$id_dosen}' AND ID_SEMESTER='{$id_semester}'
        ");
        return ($jumlah_pengisian > 0);
    }
// 
 function CekPengisianEvaluasiAdmdep($id_dosen,$id_semester) {
        //echo "SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='{$id_mhs}'";
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=13 AND ID_DOSEN='{$id_dosen}' AND ID_SEMESTER='{$id_semester}'
        ");
        return ($jumlah_pengisian > 0);
    }

//
}

?>

