<?php
include 'config.php';
include 'proses/penilaian.class.php';

if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}

$start_exe_time = getTime();

$penilaian = new penilaian($db, $user->ID_DOSEN);
$data_kelas_mk = $penilaian->load_kelas_mata_kuliah_reg_sp();
$semester_aktif = $penilaian->get_semester_aktif();

if (!empty($_GET)) {
    if (get('mode') == 'load_penilaian') {
        $id_kelas_mk = get('id_kelas');
        $detail_mk = $penilaian->get_kelas_mk($id_kelas_mk);
        
        // Cek User Dosen Sebagai Pengampu selain itu di block
        if ($penilaian->cek_penilaian_dosen_login($id_kelas_mk) == 0) {
            echo '<div class="center_title_bar">Input Nilai</div>';
            die(alert_error("Access denied.", 90));
        }
        
        // Cek Edit KRS Akademik dan Benahi Tampilan Nilai
        $penilaian->benahi_tampilan_nilai_krs_akad($id_kelas_mk, $detail_mk['ID_SEMESTER']);

        if (!empty($_POST)) {
            if (post('mode') == 'save_nilai') {
                if ($user->ID_DOSEN != '') {

                    $db->BeginTransaction();
                    for ($i = 1; $i < post('total_nilai'); $i++) {
                        $penilaian->update_nilai(post('nilai' . $i), post('id_nilai' . $i));
                    }
                    $db->Commit();
                    
                    // update nilai angka + nilai huruf
                    $penilaian->proses_nilai_akhir($id_kelas_mk);
                    
                    // update ips,ipk,dll
                    $penilaian->proses_status_mhs($id_kelas_mk);
                    
                } else {
                    die('UPDATE GAGAL RELOAD KEMBALI HALAMAN INI');
                }
            } else if (post('mode') == 'tambah_akses') {
                $penilaian->tambah_akses_komponen(post('id_dosen'), $id_kelas_mk, post('id_komponen_mk'), post('id_semester'));
            } else if (post('mode') == 'delete_akses') {
                $penilaian->delete_akses_komponen(post('id_akses'));
            } else if (post('mode') == 'tampilkan_permhs') {
                $id = post('id_pengambilan');
                $db->Query("UPDATE PENGAMBILAN_MK SET FLAGNILAI=1 WHERE ID_PENGAMBILAN_MK='{$id}'");
            } else if (post('mode') == 'tidak_tampilkan_permhs') {
                $id = post('id_pengambilan');
                $db->Query("UPDATE PENGAMBILAN_MK SET FLAGNILAI=0 WHERE ID_PENGAMBILAN_MK='{$id}'");
            } else if (post('mode') == 'tampilkan') {
                if (post('status_tampil') == 0) {
                    $penilaian->tampilkan_nilai_mhs($id_kelas_mk, $detail_mk['ID_SEMESTER']);
                } else {
                    $penilaian->reset_tampilkan_nilai_mhs($id_kelas_mk, $detail_mk['ID_SEMESTER']);
                }
            }
        }

        $smarty->assign('data_mahasiswa', $penilaian->load_data_mahasiswa_reg_sp($id_kelas_mk));
        $smarty->assign('data_pengajar_mk', $penilaian->load_data_pengajar_kelas_mk($id_kelas_mk));
        $smarty->assign('data_kelas_mk_detail', $detail_mk);
        $smarty->assign('id_fakultas_kelas', $penilaian->get_id_fakultas_kelas_mk($id_kelas_mk));
        $smarty->assign('pjmk_status', $penilaian->get_pjmk_status($id_kelas_mk));
        $smarty->assign('data_komponen_mk', $penilaian->load_komponen_mk($id_kelas_mk));
        $smarty->assign('count_data_komponen_mk', count($penilaian->load_komponen_mk($id_kelas_mk)));
        $smarty->assign('id_kelas_mk', $id_kelas_mk);
        $smarty->assign('data_dosen_mk', $penilaian->load_dosen_mk($id_kelas_mk));
        $smarty->assign('data_akses_dosen_mk', $penilaian->load_akses_komponen($id_kelas_mk));
        $smarty->assign('status_tampil', $penilaian->status_tampilkan_nilai_mhs($id_kelas_mk, $detail_mk['ID_SEMESTER']));
        // tambahan ceking agar tetap bisa melihat nilai non-semester aktif
        // FIKRIE (05-09-2018)
        $smarty->assign('cek_kelas_mk', $penilaian->cek_kelas_mk($id_kelas_mk));
    }
}

/* PENUTUPAN PENILAIAN
 * UNTUK KEPERLUAN KRS
 * MENGIKUTI TABEL JADWAL KEGIATAN SEMESTER
 * NAMBI
 */
$db->Query("
    SELECT TO_CHAR(TGL_MULAI_JKS,'DD-MM-YYYY') MULAI,TO_CHAR(TGL_SELESAI_JKS,'DD-MM-YYYY') SELESAI 
    FROM JADWAL_KEGIATAN_SEMESTER 
    WHERE ID_KEGIATAN='56' 
    AND ID_SEMESTER=(SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI = {$user->ID_PERGURUAN_TINGGI})");
$jadwal_kegiatan = $db->FetchAssoc();
$tgl_sekarang = time();
$tgl_awal_penutupan = $jadwal_kegiatan['MULAI'];
$tgl_akhir_penutupan = $jadwal_kegiatan['SELESAI'];

if ($tgl_sekarang < strtotime($tgl_akhir_penutupan) && $tgl_sekarang > strtotime($tgl_awal_penutupan)) {
    $jam_sekarang = date('d-m-Y H:i');
    $jam_awal = date('H:i', strtotime('05:00:00'));
    $jam_akhir = date('H:i', strtotime($jam_awal . '+13 hours'));
    $batas_jam_awal = date('d-m-Y H:i', strtotime(date('d-m-Y') . $jam_awal));
    $batas_jam_akhir = date('d-m-Y H:i', strtotime($batas_jam_awal . '+13 hours'));
    
    if ($jam_sekarang < $batas_jam_akhir && $jam_sekarang > $batas_jam_awal) {
        $list_id_pengecualian = array();
        
        if (!in_array($user->ID_DOSEN, $list_id_pengecualian)) {
            $pesan = alert_error("Mohon Maaf, akses penilaian di tutup mulai tanggal {$tgl_awal_penutupan} sampai dengan {$tgl_akhir_penutupan} untuk keperluan KRS.<br/>
            Penilaian dapat di akses selain jam {$jam_awal} - {$jam_akhir}, terima kasih.", 90);
            $smarty->assign('tutup', $pesan);
        }
    }
}

$smarty->assign('semester_aktif', $semester_aktif);
$smarty->assign('id_fakultas', $db->QuerySingle("SELECT ID_FAKULTAS FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM KELAS_MK WHERE ID_KELAS_MK='{$id_kelas_mk}')"));
$smarty->assign('id_jenjang', $login->id_jenjang);
$smarty->assign('data_kelas_mk', $data_kelas_mk);

$exe_time = getTime() - $start_exe_time;
$smarty->assign('exe_time', number_format($exe_time, 2));

$smarty->display('display-penilaian/nilai.tpl');