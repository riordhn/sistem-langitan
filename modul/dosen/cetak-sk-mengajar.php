<?php
include('../../config.php');
/*include('../../tcpdf/config/lang/ind.php');
include('../../tcpdf/tcpdf.php');*/
include('proses/jadwal_kuliah.class.php');
include('proses/list_data.class.php');

$db->Query("SELECT P.* FROM PENGGUNA P WHERE P.ID_PENGGUNA='{$user->ID_PENGGUNA}'");
$pengguna = $db->FetchAssoc();

$list_data = new list_data($db, $id_pt_user);

$semester_aktif = $list_data->get_semester_aktif();

$jadwal_kuliah = new jadwal_kuliah($db, $user->ID_DOSEN, $semester_aktif['ID_SEMESTER']);

$perkuliahan_dosen = $jadwal_kuliah->load_mk_dosen();
$nama_singkat_upper = strtoupper($nama_singkat);
$urutan_sk = 45;
$tahun = date('Y');

$semester = $list_data->get_semester($semester_aktif['ID_SEMESTER']);

$fakultas = $list_data->get_fakultas($user->ID_FAKULTAS);

$prodi = $list_data->get_prodi($user->ID_PROGRAM_STUDI);
$nama_dosen = $pengguna['GELAR_DEPAN'] . '' . $pengguna['NM_PENGGUNA'] . ' ' . $pengguna['GELAR_BELAKANG'];
$nama_fakultas = $fakultas['NM_FAKULTAS'];
$nama_prodi = ucfirst(strtolower($prodi['NM_PROGRAM_STUDI']));
$nama_semester = $semester['NM_SEMESTER'] == 'Ganjil' ? 'Gasal' : $semester['NM_SEMESTER'];

$bulan_romawi = $list_data->NumberToRomanRepresentation($semester['NM_SEMESTER'] == 'Ganjil' ?9:2);

$nama_semester_capital = strtoupper($nama_semester);
$tgl_sk = $semester['NM_SEMESTER'] == 'Ganjil' ?('1 September '.$tahun):('1 Februari '.$tahun);
$tahun_ajaran = $semester['TAHUN_AJARAN'];
if ($id_pt_user == 1) {
    $yayasan_text = '<b><span class="address">YAYASAN PENDIDIKAN DAN SOSIAL MA’ARIF (YPM)</span></b>';
} else {
    $yayasan_text = "";
}



$index = 1;

$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Sistem Langitan PTNU');
$pdf->SetAuthor('Langitan');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

//$pdf->AddPage();

// add a page
$pdf->AddPage('P', 'F4');

$html = <<<EOF
<style>
    .keterangan {font-size: 7pt;font-weight:bold;}
    hr{
        margin:100px 0px;
    }
    table.data {border-collapse:collapse;}
    .data td{
        border: 1px solid black;
        vertical-align: baseline;
    }
    div { margin-top: 0pt; }
    .header { font-size: 18pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 12pt; font-family: serif; margin-top: 0px ;text-align:center; }
    .address2 { font-size: 11pt; font-family: serif; margin-top: 0px ;text-align:center; }
    p { font-size: 11pt;}
    td { font-size: 11pt;text-align:left}
    td.center { font-weight:bold;text-align:center }
    th { background-color:black;color:white;font-size: 11pt;text-align:center; }
    
</style>
<table width="100%" border="0">
    <tr>
        <td width="20%" align="right"><img src="../../img/akademik_images/logo-{$nama_singkat}.gif" width="80px" height="80px"/></td>
        <td width="80%" align="center">
            {$yayasan_text}
            <span class="header" style="color:#27b346">{$nama_pt_kapital}<br/></span>
            <span class="address">{$alamat}, {$kota_pt}</span><br/>
            <span class="address2">Telp.{$telp_pt},Website: {$web_pt},email: {$web_email_pt}, pos: {$kd_pos_pt}</span><br/>
        </td>
    </tr>
</table>
<hr/>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center">
            <br/><br/>
            <b style="font-size:13pt"><u>SURAT KEPUTUSAN</u></b>
            <br/>
            <span  style="font-size:13pt">Nomor : {$urutan_sk}/SK/{$nama_singkat_upper}/{$bulan_romawi}/{$tahun}
            </span>
            <p style="font-size:13pt">
            PENETAPAN DOSEN SEMESTER {$nama_semester_capital}
            <br/>
            TAHUN AKADEMIK {$tahun_ajaran}
            </p>
            <p style="font-size:13pt">
            <b>REKTOR
            <br/>
            {$nama_pt_kapital}
            </b>
            </p>            
        </td>
    </tr>
</table>
<p></p>
<table width="100%" border="0" align="center">
    <tr>
        <td width="15%">Menimbang</td>
        <td width="5%">:</td>
        <td width="80%">dan sebagainya</td>
    </tr>
    <tr>
        <td width="15%">Mengingat</td>
        <td width="5%">:</td>
        <td width="80%">dan sebagainya</td>
    </tr> 
    <tr>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td colspan="3">MEMUTUSKAN</td>
    </tr>
    <tr>
        <td width="15%">Menetapkan</td>
        <td width="5%">:</td>
        <td width="80%"></td>
    </tr>
    <tr>
        <td width="15%">Pertama</td>
        <td width="5%">:</td>
        <td width="80%"><p  style="text-align:justify;margin:0px">Mengangkat Dosen Pembina Mata Kuliah pada Prodi {$nama_prodi} Semester {$nama_semester} Tahun Akademik {$tahun_ajaran} sebagai berikut</p></td>
    </tr>    
    <tr>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td width="15%"></td>
        <td width="5%"></td>
        <td width="80%">
            <table width="80%">
                <tr>
                    <td width="25%">Nama Dosen</td>
                    <td width="5%">:</td>
                    <td width="55%">{$nama_dosen}</td>
                </tr>
                <tr>
                    <td>Status Dosen</td>
                    <td>:</td>
                    <td>Dosen Tetap</td>
                </tr>
            </table>
            <br/>
            <table border="1" cellpadding="5">
                <tr>
                    <td width="10%">No.</td>
                    <td width="35%">Mata Kuliah</td>
                    <td width="35%">Jurusan</td>
                    <td width="10%">Smt</td>
                    <td width="10%">SKS</td>
                </tr>               
                {data_kuliah}
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td width="15%">Kedua</td>
        <td width="5%">:</td>
        <td width="80%"><p  style="text-align:justify;margin:0px">Dosen tersebut diktum pertama bertugas membina dan menguji mata kuliah tersebut diatas pada  Semester {$nama_semester} Tahun Akademik {$tahun_ajaran}.</p></td>
    </tr>  
    <tr>
        <td width="15%">Ketiga</td>
        <td width="5%">:</td>
        <td width="80%"><p  style="text-align:justify;margin:0px">Beban biaya akibat dikeluarkannya keputusan ini dibebankan kepada kas Fakultas {$nama_fakultas} {$nama_pt}.</p></td>
    </tr> 
    <tr>
        <td width="15%">Keempat</td>
        <td width="5%">:</td>
        <td width="80%"><p  style="text-align:justify;margin:0px">Keputusan ini berlaku sejak tanggal ditetapkan dan akan diadakan perbaikan sebagimana mestinya apabila terjadi kekeliruan dalam penetapannya</p></td>
    </tr> 
</table>
<table width="100%" border="0" align="center">
    {ttd}
</table>
EOF;
$data_kuliah_str = '';
$ttd = '
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td width="60%" colspan="2"></td>
            <td width="40%" colspan="3" align="left">Ditetapkan di: ' . $kota_pt . ', <br/>
                Pada tanggal : ' . $tgl_sk . '
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <b>{pejabat_ttd}</b>
                <br/>
            </td>
        </tr>';
$index = 1;
foreach ($perkuliahan_dosen as $data) {
    $data_kuliah_str .= '
    <tr>
        <td>' . $index . '</td>
        <td>' . $data['NM_MATA_KULIAH'] . '</td>
        <td>' . $data['NM_JENJANG'] . ' ' . $data['NM_PROGRAM_STUDI'] . ' </td>
        <td>' . $data['TINGKAT_SEMESTER'] . '</td>
        <td>' . $data['KREDIT_SEMESTER'] . '</td>
    </tr>';
    $index++;
}
$html = str_replace('{data_kuliah}', $data_kuliah_str, $html);
$html = str_replace('{tanggal}', date('d F Y  H:i:s'), $html);
$html = str_replace('{semester}', 'SEMESTER ' . $semester['NM_SEMESTER'] . ' ( ' . $semester['TAHUN_AJARAN'] . ' )', $html);
$html = str_replace('{ttd}', $ttd, $html);
$html = str_replace('{pejabat_ttd}', $id_pt_user == 1 ? 'DR. H. Achmad Fathoni Rodli,M.Pd' : "", $html);
$pdf->writeHTML($html);

$pdf->Output();
