<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {       
        $db->Parse("
            INSERT INTO PROGRAM_STUDI
            ( ID_JENJANG, ID_FAKULTAS, NM_PROGRAM_STUDI, KODE_PROGRAM_STUDI, NO_SK) VALUES
            (:id_jenjang,:id_fakultas,:nm_program_studi,:kode_program_studi,:no_sk)");
        $db->BindByName(':id_jenjang', post('id_jenjang'));
        $db->BindByName(':id_fakultas', post('id_fakultas'));
        $db->BindByName(':nm_program_studi', post('nm_program_studi'));
        $db->BindByName(':kode_program_studi', post('kode_program_studi'));
        $db->BindByName(':no_sk', post('no_sk'));
        $result = $db->Execute();
        
        if ($result)
        {
            write_log("[BETA] {$user->ID_PENGGUNA} melakukan penambahan prodi : " . post('nm_program_studi'));
            $smarty->assign('prodi_added', true);
        }
    }
    
    if (post('mode') == 'edit')
    {
        $id_program_studi = post('id_program_studi');
        
        $db->Parse("
            UPDATE PROGRAM_STUDI SET
                ID_JENJANG = :id_jenjang,
                ID_FAKULTAS = :id_fakultas,
                NM_PROGRAM_STUDI = :nm_program_studi,
                KODE_PROGRAM_STUDI = :kode_program_studi,
                NO_SK = :no_sk,
                TGL_PENDIRIAN = TO_DATE(:tgl_pendirian, 'DD-MM-YYYY'),
                MASA_BERLAKU = :masa_berlaku
            WHERE ID_PROGRAM_STUDI = {$id_program_studi}");
        $db->BindByName(':id_jenjang', post('id_jenjang'));
        $db->BindByName(':id_fakultas', post('id_fakultas'));
        $db->BindByName(':nm_program_studi', post('nm_program_studi'));
        $db->BindByName(':kode_program_studi', post('kode_program_studi'));
        $db->BindByName(':no_sk', post('no_sk'));
        $db->BindByName(':tgl_pendirian', date('d-m-Y', mktime(0, 0, 0, post('tgl_pendirian_Month'), post('tgl_pendirian_Day'), post('tgl_pendirian_Year'))));
        $db->BindByName(':masa_berlaku', post('masa_berlaku'));
        $result = $db->Execute();
        
        if ($result)
        {
            write_log("[BETA] {$user->ID_PENGGUNA} melakukan pengeditan id_program_studi = {$id_program_studi}");
            $smarty->assign('prodi_changed', true);
        }
    }
    
    if (post('mode') == 'delete')
    {
        $id_program_studi = post('id_program_studi');
        
        $result = $db->Query("DELETE FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI = {$id_program_studi}");
        
        if ($result)
        {
            write_log("[BETA] {$user->ID_PENGGUNA} melakukan penghapusan id_program_studi = {$id_program_studi}");
            $smarty->assign('prodi_deleted', true);
        }
    }
}

if ($request_method == 'GET' || $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $id_jenjang = get('id_jenjang', '');
        
        $smarty->assign('id_jenjang', $id_jenjang);
        $smarty->assign('jenjang_set', $db->QueryToArray("SELECT * FROM JENJANG"));

        $filter_jenjang = "";
        if ($id_jenjang != '')
            $filter_jenjang = "WHERE PS.ID_JENJANG = {$id_jenjang}";
            
        $program_studi_set = $db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI, J.NM_JENJANG || ' ' || PS.NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI, PS.KODE_PROGRAM_STUDI, F.NM_FAKULTAS, F.SINGKATAN_FAKULTAS,
                (select count(distinct m.id_mhs) from mahasiswa m
				 join pembayaran p on p.id_mhs = m.id_mhs
				 where p.tgl_bayar is not null and m.thn_angkatan_mhs is not null and m.id_program_studi = ps.id_program_studi) AS JUMLAH_MAHASISWA,
                (SELECT COUNT(*) FROM BIAYA_KULIAH BK WHERE BK.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI) AS JUMLAH_VARIAN,
                PS.NO_SK, PS.TGL_PENDIRIAN , PS.MASA_BERLAKU
            FROM PROGRAM_STUDI PS
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            {$filter_jenjang}
            ORDER BY PS.ID_FAKULTAS, PS.ID_JENJANG");
        $smarty->assign('program_studi_set', $program_studi_set);
    }
    
    if ($mode == 'add')
    {
        $smarty->assign('jenjang_set', $db->QueryToArray("SELECT * FROM JENJANG"));
        $smarty->assign('fakultas_set', $db->QueryToArray("SELECT * FROM FAKULTAS"));
    }
    
    if ($mode == 'edit')
    {
        $id_program_studi = get('id_program_studi', '');
        
        $db->Query("
            SELECT PS.ID_PROGRAM_STUDI, PS.ID_JENJANG, PS.NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI, PS.KODE_PROGRAM_STUDI, PS.ID_FAKULTAS, PS.NO_SK,
                (SELECT COUNT(*) FROM MAHASISWA M WHERE M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI) AS JUMLAH_MAHASISWA,
                NVL(PS.TGL_PENDIRIAN, '01-JAN-1900') AS TGL_PENDIRIAN, PS.MASA_BERLAKU
            FROM PROGRAM_STUDI PS
            WHERE PS.ID_PROGRAM_STUDI = {$id_program_studi}");
        $row = $db->FetchAssoc();
        $smarty->assign('program_studi', $row);
        
        $smarty->assign('jenjang_set', $db->QueryToArray("SELECT * FROM JENJANG"));
        $smarty->assign('fakultas_set', $db->QueryToArray("SELECT * FROM FAKULTAS"));
        
        // detail varian biaya
        $smarty->assign('biaya_kuliah_set', $db->QueryToArray("
            SELECT S.NM_SEMESTER || ' ' || S.TAHUN_AJARAN AS NM_SEMESTER, J.NM_JALUR, KB.NM_KELOMPOK_BIAYA,
              BK.BESAR_BIAYA_KULIAH,
              (SELECT SUM(BESAR_BIAYA) FROM DETAIL_BIAYA DB WHERE DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH AND db.pendaftaran_biaya = 1) AS BIAYA_PENDAFTARAN,
              (SELECT SUM(BESAR_BIAYA) FROM DETAIL_BIAYA DB WHERE DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH AND db.pendaftaran_biaya = 0) AS BIAYA_SEMESTER
            FROM BIAYA_KULIAH BK
            LEFT JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
            LEFT JOIN JALUR J ON J.ID_JALUR = BK.ID_JALUR
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
            WHERE BK.ID_PROGRAM_STUDI = {$id_program_studi}
            ORDER BY S.TAHUN_AJARAN DESC, S.NM_SEMESTER, BK.ID_JALUR"));
    }
    
    if ($mode == 'delete')
    {
        $id_program_studi = get('id_program_studi', '');
        
        $db->Query("
            SELECT PS.ID_PROGRAM_STUDI, J.NM_JENJANG, PS.ID_JENJANG, PS.NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI, PS.KODE_PROGRAM_STUDI, F.NM_FAKULTAS, PS.NO_SK,
                (SELECT COUNT(*) FROM MAHASISWA M WHERE M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI) AS JUMLAH_MAHASISWA
            FROM PROGRAM_STUDI PS
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            WHERE PS.ID_PROGRAM_STUDI = {$id_program_studi}");
        $row = $db->FetchAssoc();
        $smarty->assign('program_studi', $row);
        
        // detail varian biaya
        $smarty->assign('biaya_kuliah_set', $db->QueryToArray("
            SELECT S.NM_SEMESTER || ' ' || S.TAHUN_AJARAN AS NM_SEMESTER, J.NM_JALUR, KB.NM_KELOMPOK_BIAYA,
              BK.BESAR_BIAYA_KULIAH,
              (SELECT SUM(BESAR_BIAYA) FROM DETAIL_BIAYA DB WHERE DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH AND db.pendaftaran_biaya = 1) AS BIAYA_PENDAFTARAN,
              (SELECT SUM(BESAR_BIAYA) FROM DETAIL_BIAYA DB WHERE DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH AND db.pendaftaran_biaya = 0) AS BIAYA_SEMESTER
            FROM BIAYA_KULIAH BK
            LEFT JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
            LEFT JOIN JALUR J ON J.ID_JALUR = BK.ID_JALUR
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
            WHERE BK.ID_PROGRAM_STUDI = {$id_program_studi}
            ORDER BY S.TAHUN_AJARAN DESC, S.NM_SEMESTER, BK.ID_JALUR"));
    }
}

$smarty->display("dirpendidikan/mprodi/{$mode}.tpl");
?>