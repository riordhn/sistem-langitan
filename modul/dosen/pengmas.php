<?php
include 'config.php';
include 'proses/pengmas.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

//memanggil object class user,database dan pengmas 
$pengmas = new pengmas($db, $login->id_dosen);


$mode = get('mode','view');

if ($request_method == 'POST')
{
	if (post('mode') == 'add')
	{
		$pengmas->insert($_POST['nama'], $_POST['tempat'], $_POST['bidang'], $_POST['peran'], $_POST['tahun'], $_POST['sumber_dana'], $_POST['jml_dana'], $_POST['tingkat'], $_POST['output']);
	}
	else if(post('mode')=='edit')
	{	
		$pengmas->update($_POST['id_pengmas'], $_POST['nama'], $_POST['tempat'], $_POST['bidang'], $_POST['peran'], $_POST['tahun'], $_POST['sumber_dana'], $_POST['jml_dana'], $_POST['tingkat'], $_POST['output']);
	}
	else if(post('mode')=='delete')
	{
		$pengmas->delete(post('id_pengmas'));
	}

}
if($mode=='detail'||$mode=='edit'||$mode=='upload'||$mode=='delete'){
	$smarty->assign('data_pengmas',$pengmas->get(get('id')));
}else{
	$smarty->assign('data_pengmas', $pengmas->load());
}


$smarty->display("sample/biodata/{$mode}_pengmas.tpl");

?>
