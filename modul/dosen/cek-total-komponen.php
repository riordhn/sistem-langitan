<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$post_persentase = $_REQUEST['persentase'];
$id_kelas_mk = $_GET['id_kelas_mk'];
$id_komponen_mk = $_GET['id_komponen'];
$valid = 'true';
$q_edit='';
if ($id_komponen_mk != '')
    $q_edit = "AND ID_KOMPONEN_MK !={$id_komponen_mk}";
$db->Query("SELECT SUM(PERSENTASE_KOMPONEN_MK) PERSENTASE FROM KOMPONEN_MK WHERE ID_KELAS_MK='{$id_kelas_mk}' {$q_edit}");
$data_persentase = $db->FetchAssoc();
if ($data_persentase['PERSENTASE'] + $post_persentase > 100) {
    $valid = 'false';
}

echo $valid;
?>