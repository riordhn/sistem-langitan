<?php
include 'config.php';
include 'proses/evaluasi.class.php';
include 'proses/penilaian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$eval = new evaluasi($db, $login->id_dosen);
$penilaian = new penilaian($db, $user->ID_DOSEN);

$smarty->assign('data_kelas_mk', $penilaian->load_kelas_mata_kuliah());
$smarty->assign('data_eva_wali', $eval->load_data_hasil_ev_wali());
$smarty->assign('data_eva_prak', $eval->load_data_hasil_ev_praktikum());
$smarty->display('sample/biodata/evaluasi_dosen.tpl');
?>