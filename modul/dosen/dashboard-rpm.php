<?php
include 'config.php';
include '../ppmb/class/snmptn.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$mode = get('mode', 'view');
$snmptn = new SnmptnClass($db);

if ($request_method == 'POST')
{
    $id_c_mhs = post('id_c_mhs');
    $status_verifikasi = post('status_verifikasi');
    
    $db->BeginTransaction();
    
    $db->Query("update calon_mahasiswa_pm set id_verifikator = {$user->ID_PENGGUNA}, status_verifikasi = {$status_verifikasi} where id_c_mhs = {$id_c_mhs}");
    
    if ($status_verifikasi == 2)
    {
        $db->Query("
            update nilai_cmhs_snmptn n set id_pilihan_pindah = (select id_pilihan_2 from calon_mahasiswa_baru cmb where cmb.id_c_mhs = n.id_c_mhs)
            where putaran = 2 and id_c_mhs = {$id_c_mhs}");
    }
    else
    {
        $db->Query("
            update nilai_cmhs_snmptn n set id_pilihan_pindah = (select id_pilihan_1 from calon_mahasiswa_baru cmb where cmb.id_c_mhs = n.id_c_mhs)
            where putaran = 2 and id_c_mhs = {$id_c_mhs}");
    }
    
    $result = $db->Commit();
    
    echo $result ? 1 : 0;
    
    exit();
}

if ($request_method == 'GET')
{
    if ($mode == 'view')
    {
        $smarty->assign('cmb_set', $snmptn->GetListPM());
    }
    
    if ($mode == 'detail')
    {
        $id_c_mhs = get('id_c_mhs');
		
		$smarty->assign('cmb', $snmptn->GetDetailPeserta($id_c_mhs));
    }
}

$smarty->display("sample/dashboard/dashboard-rpm-{$mode}.tpl");
?>