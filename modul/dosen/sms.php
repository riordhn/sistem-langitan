<?php
	include 'sms_controller.php';
	if($user->IsLogged()){
		$sms = new SmsController();

		if(isset($_GET['view'])){
			$view = $_GET['view'];
			
			if($view=='index'){
				$post = new RequestController();
				if(isset($_POST['mobile']) && isset($_POST['pesan'])){
					$mobile = $_POST['mobile'];
					$message = $_POST['pesan'];
					if($mobile!=''){
						$sms->sendMessage($mobile, $message);
					}
					else{
						echo '<script>window.location = "#jadwal-jadwal_kuliah!jadwal_kuliah.php"</script>';
					}
				}

				else{
					if(isset($_GET['mobile'])){
						
						$mobile = $post->saveRequest(htmlspecialchars($_GET['mobile']));
						if($mobile!=""){
							if(isset($_GET['mhs'])){
								$mhs = htmlspecialchars($_GET['mhs']);
							}

							$sms->requestPage('index');
							$smarty->display(
								$sms->page,
								array(
									'mhs'=>$mhs,
									'mobile'=>$mobile,
								)
							);
						}

						else{
							echo '<script>window.location = "#jadwal-jadwal_kuliah!jadwal_kuliah.php"</script>';
						}
					}

					else{
						echo '<script>window.location = "#jadwal-jadwal_kuliah!jadwal_kuliah.php"</script>';
					}

				}


			}

			else if($view=='peserta_ma'){

				if(isset($_POST['id_kelas_mk']) && isset($_POST['pesan'])){
					$post = new RequestController();
					$id_kelas_mk = $post->saveRequest((int)$_POST['id_kelas_mk']);
					$message = $post->saveRequest($_POST['pesan']);
					$sms->sendMessageToPMA($message, $id_kelas_mk);
				}

				else if(isset($_GET['id_kelas_mk']) && isset($_GET['nama_kelas'])){

					$id_kelas_mk = htmlspecialchars($_GET['id_kelas_mk']);
					$nama_kelas = htmlspecialchars($_GET['nama_kelas']);
					if($id_kelas_mk!="" && $nama_kelas!=""){
						$sms->requestPage('peserta_ma');
						
						$smarty->display(
							$sms->page,
							array(
								'id_kelas_mk'=>$id_kelas_mk,
								'nama_kelas'=>$nama_kelas,
								'id_kelas'=>$id_kelas_mk,
								'nama_kelas'=>$nama_kelas,
							)
						);
					}
					else{
						//echo '<script>window.location = "#jadwal-jadwal_kuliah!jadwal_kuliah.php"</script>';
					}

				}
				else{
					//echo '<script>window.location = "#jadwal-jadwal_kuliah!jadwal_kuliah.php"</script>';
				}


			}
			
	// SMS PERWALIAN
				else if($view=='perwalian'){

					if(isset($_POST['pesan'])){
						$post = new RequestController();
						$message = $post->saveRequest($_POST['pesan']);
						$sms->sendMessageToPerwalian($message, $id_kelas_mk);
					}

					//else if(isset($_GET['id_kelas_mk']) && isset($_GET['nama_kelas'])){

						//$id_kelas_mk = htmlspecialchars($_GET['id_kelas_mk']);
						//$nama_kelas = htmlspecialchars($_GET['nama_kelas']);
						//if($id_kelas_mk!="" && $nama_kelas!=""){
							$sms->requestPage('perwalian');
							
							$smarty->display(
								$sms->page,
								array(
									'id_kelas_mk'=>$id_kelas_mk,
									'nama_kelas'=>$nama_kelas,
									'id_kelas'=>$id_kelas_mk,
									'nama_kelas'=>$nama_kelas,
								)
							);
						//}
					//else{
						//echo '<script>window.location = "#jadwal-jadwal_kuliah!jadwal_kuliah.php"</script>';
					//}

				}
				else{
					//echo '<script>window.location = "#jadwal-jadwal_kuliah!jadwal_kuliah.php"</script>';
				}


			}
	}
	else{
		echo "Aduh, sepertinya anda mengunjungi website ini secara ilegal";
	}