<?php

include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}

// KHUSUS MENU WADEK 1 FAKULTAS PASCA SARJANAH
if ($user->ID_FAKULTAS != 9) {
    $blocked = alert_error("Mohon maaf fitur ini khusus fakultas tertentu saja. Terimakasih", 99);
    die($blocked);
}

require_once 'proses/perwalian.class.php';

$perwalian = new perwalian($db, $user->ID_PENGGUNA, $login->id_dosen);
if (isset($_POST)) {
    if (post('mode') == 'approve-all') {
        for ($i = 1; $i < post('jumlah'); $i++) {
            if (post('apvkrs' . $i) != '') {
                $id = post('mhs'.$i);
                $db->Query("UPDATE PENGAMBILAN_MK PMK SET PMK.STATUS_APV_PENGAMBILAN_MK = '1' WHERE PMK.ID_MHS ='{$id}'");
            }
        }
        $smarty->assign('data_mahasiswa_krs', $perwalian->load_mahasiswa_krs());
    }
}

if (isset($_GET)) {
    if (get('mode') == 'krs') {
        $id_mhs = get('id');
        $smarty->assign('id_mhs', $id_mhs);
        $smarty->assign('id_fak', $user->ID_FAKULTAS);
        $smarty->assign('data_mahasiswa', $perwalian->load_mhs_status($id_mhs));
        $smarty->assign('data_transkrip', $perwalian->load_transkrip($id_mhs));
        $smarty->assign('data_pesan_krs', $perwalian->load_krs_message($id_mhs));
        $smarty->assign('data_krs', $perwalian->load_krs($id_mhs));
        $smarty->assign('sum_data', count($perwalian->load_krs($id_mhs)));
        $smarty->assign('mode', 'load_krs');
    }
}

$smarty->assign('data_mahasiswa_krs', $perwalian->load_mahasiswa_krs_fakultas($user->ID_FAKULTAS));
$smarty->assign('id_fakultas', $login->id_fakultas);
$smarty->display('display-bimbingan/approval.tpl');
?>