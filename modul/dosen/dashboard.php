<div class="center_title_bar">Dashboard</div>
<style>
	.mystyle li a:hover {
		text-decoration: underline;
	}
	.mystyle li {
		font-size: 14px;
	}
</style>
<ul class="mystyle">
	<li><a href="dashboard-sebaran.php">Sebaran Mahasiswa</a></li>
	<li><a href="dashboard-ipk.php">IPK &lt; 2</a></li>
	<li><a href="dashboard-do.php">Mahasiswa DO</a></li>
	<li><a href="dashboard-ns1.php">Distribusi Nilai S1</a></li>
	<li><a href="dashboard-nd3.php">Distribusi Nilai D3</a></li>
	<li><a href="dashboard-ns2.php">Distribusi Nilai S2 &amp; S3</a></li>
	<li><a href="dashboard-reg.php">Registrasi Mahasiswa</a></li>
	<li><a href="dashboard-krs.php">KRS Mahasiswa</a></li>
</ul>