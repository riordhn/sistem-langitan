<?php
include 'config.php';
include 'proses/jadwal_kuliah.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

// $id_semester dapat dari config.php
$jadwal_kuliah = new jadwal_kuliah($db, $login->id_dosen, $id_semester);

if (isset($_GET)) {
    if (get('mode') == 'peserta_ma') {
        $smarty->assign('data_peserta_ma', $jadwal_kuliah->load_peserta_mata_kuliah(get('id_kelas_mk')));
        if (isset($_GET['nama_kelas'])) {
            $nama_kelas = $_GET['nama_kelas'];
            $smarty->assign('nama_kelas', $nama_kelas);
        }
        if (isset($_GET['id_kelas_mk'])) {
            $id_kelas_mk = $_GET['id_kelas_mk'];
            $smarty->assign('id_kelas_mk', $id_kelas_mk);
        }
    } else {
        //Khusus Psikologi
        if ($user->ID_FAKULTAS == 0.11) {
            $smarty->assign('data_jadwal_kuliah_psikologi', $jadwal_kuliah->load_jadwal_kuliah_psikologi());
            //Lainnya
        } else {
            $smarty->assign('data_jadwal_kuliah', $jadwal_kuliah->load_jadwal_kuliah());
        }
    }
}

//Khusus Psikologi
if ($user->ID_FAKULTAS == 0.11) {
    $smarty->assign('count_data_jadwal_kuliah', count($jadwal_kuliah->load_jadwal_kuliah_psikologi()));
    $smarty->display('display-jadwal/jadwal_kuliah_psikologi.tpl');
//Lainnya
} else {
    $smarty->assign('count_data_jadwal_kuliah', count($jadwal_kuliah->load_jadwal_kuliah()));
    $smarty->display('display-jadwal/jadwal_kuliah.tpl');
}
?>