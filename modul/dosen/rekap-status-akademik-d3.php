<?php
include 'config.php';
include 'proses/rekapitulasi.class.php';

// penambahan id sesuai ptnu dan parameter $id_pt pada construct class rekapitulasi
// Fikrie //
$id_pt = $id_pt_user;

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }
$rekap = new rekapitulasi($db, $user->ID_DOSEN, $id_pt);
$smarty->assign('thn_angkatan', $rekap->thn_angkatan());
$smarty->assign('thn_akademik', $rekap->thn_akademik());

if(isset($_REQUEST['thn_angkatan'])){
$fakultas_set = $db->QueryToArray("select id_fakultas, nm_fakultas from fakultas where id_perguruan_tinggi = {$id_pt}");

foreach ($fakultas_set as &$f)
	$f['program_studi_set'] = $rekap->program_studi($f['ID_FAKULTAS'], 3, '');
/*$data_set = $db->QueryToArray("
	select ps.id_program_studi, ps.id_jenjang, ps.nm_program_studi,
	  (select count(id_mhs) from mahasiswa m where m.id_program_studi = ps.id_program_studi and m.status_akademik_mhs in (1,2,3,16,17,18)) aktif,
	  (select count(m.id_mhs) from mahasiswa m left join admisi a on a.id_mhs = m.id_mhs
	   where m.id_program_studi = ps.id_program_studi and m.status_akademik_mhs = 4 and to_char(tgl_sk,'YYYY') = 2011) lulus,
	  (select count(m.id_mhs) from mahasiswa m left join admisi a on a.id_mhs = m.id_mhs
	   where m.id_program_studi = ps.id_program_studi and m.status_akademik_mhs = 5 and to_char(tgl_sk,'YYYY') = 2011) undur,
	  (select count(m.id_mhs) from mahasiswa m left join admisi a on a.id_mhs = m.id_mhs
	   where m.id_program_studi = ps.id_program_studi and m.status_akademik_mhs = 6 and to_char(tgl_sk,'YYYY') = 2011) do
	from program_studi ps where ps.id_jenjang = 5
	order by ps.id_fakultas, ps.nm_program_studi");*/	
	
$data_set = $rekap->status_akademik(5, $_REQUEST['thn_angkatan'], $_REQUEST['thn_akademik']);		
foreach ($data_set as &$d)
{
	$d['TOTAL'] = $d['AKTIF'] + $d['LULUS'] + $d['UNDUR'] + $d['DO'];
	$d['PERSEN_AKTIF'] = round(($d['AKTIF'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_LULUS'] = round(($d['LULUS'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_UNDUR'] = round(($d['UNDUR'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_DO'] = round(($d['DO'] / $d['TOTAL']) * 100, 2);
}

$smarty->assign('fakultas_set', $fakultas_set);
$smarty->assign('data_set', $data_set);
}
$smarty->display("sample/rekap/rekap-status-akademik-d3.tpl");
?>