<?php
include 'config.php';
include 'proses/penelitian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$mode = get('mode', 'view');

$Penelitian = new Penelitian($db);

if (1 == 2) { include 'User.class.php'; $user = new User(); }

if ($request_method == 'POST')
{
    if (post('mode') == 'approve-departemen')
    {
        $result = $Penelitian->ApproveDepartemen(post('id_penelitian'), $user->ID_PENGGUNA);
        if ($result) $smarty->assign('result', 'Penelitian berhasil di approve');
        else $smarty->assign('result', 'Penelitian gagal di approve');
    }
    
    if (post('mode') == 'approve-dekan')
    {
        $result = $Penelitian->ApproveDekan(post('id_penelitian'), $user->ID_PENGGUNA);
        if ($result) $smarty->assign('result', 'Penelitian berhasil di approve');
        else $smarty->assign('result', 'Penelitian gagal di approve');
    }
    
    if (post('mode') == 'reject-departemen')
    {
        $result = $Penelitian->RejectDepartemen(post('id_penelitian'), $user->ID_PENGGUNA);
        if ($result) $smarty->assign('result', 'Penelitian berhasil di batalkan');
        else $smarty->assign('result', 'Penelitian gagal di batalkan');
    }
    
    if (post('mode') == 'reject-dekan')
    {
        $result = $Penelitian->RejectDekan(post('id_penelitian'), $user->ID_PENGGUNA);
        if ($result) $smarty->assign('result', 'Penelitian berhasil di batalkan');
        else $smarty->assign('result', 'Penelitian gagal di batalkan');
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        if ($user->IsKetuaDepartemen)
        {
            $smarty->assign('is_departemen', 1);
            $smarty->assign('penelitian_set', $Penelitian->GetListPenelitianByKadep($user->ID_DEPARTEMEN_KADEP));
        }
        
        if ($user->IsDekan)
        {
            $smarty->assign('is_dekan', 1);
            $smarty->assign('penelitian_set', $Penelitian->GetListPenelitianByDekan($user->ID_FAKULTAS_DEKAN));
            
            $mode = "view";
        }
    }
    
    if ($mode == 'detail')
    {
        $id_penelitian = get('id_penelitian');
        $smarty->assign('p', $Penelitian->GetData($id_penelitian));
        $smarty->assign('penelitian_anggota_set', $Penelitian->GetListAnggotaPenelitian($id_penelitian));
        $smarty->assign('penelitian_biaya_set', $Penelitian->GetListBiayaPenelitian($id_penelitian));
    }
    
    if ($mode == 'approve')
    {
        $id_penelitian = get('id_penelitian');
        $smarty->assign('p', $Penelitian->GetData($id_penelitian));
        
        if ($user->IsDekan) { $smarty->assign('is_dekan', 1); }
        if ($user->IsKetuaDepartemen) { $smarty->assign('is_departemen', 1); }
    }
    
    if ($mode == 'reject')
    {
        $id_penelitian = get('id_penelitian');
        $smarty->assign('p', $Penelitian->GetData($id_penelitian));
        
        if ($user->IsDekan) { $smarty->assign('is_dekan', 1); }
        if ($user->IsKetuaDepartemen) { $smarty->assign('is_departemen', 1); }
    }
}

$smarty->display("sample/biodata/penelitian-approve/{$mode}.tpl");
?>
