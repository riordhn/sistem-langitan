<?php
include 'config.php';
include 'proses/organisasi.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$organisasi = new organisasi($db, $login->id_dosen);
$mode = get('mode','view');
if ($request_method == 'POST')
{
	if (post('mode') == 'add')
	{
		$organisasi->insert($_POST['organisasi'], $_POST['jabatan'], $_POST['mulai'], $_POST['selesai'], $_POST['tingkat']);
	}
	else if(post('mode')=='edit')
	{	
		$organisasi->update($_POST['id_organisasi'], $_POST['organisasi'], $_POST['jabatan'], $_POST['mulai'], $_POST['selesai'], $_POST['tingkat']);
	}
	else if(post('mode')=='delete')
	{
		$pengmas->delete(post('id_organisasi'));
	}

}

if($mode=='detail'||$mode=='edit'||$mode=='upload'||$mode=='delete'){
	$smarty->assign('data_organisasi',$organisasi->get_organisasi(get('id_organisasi')));
}else{
	$smarty->assign('data_organisasi', $organisasi->load_organisasi());
}

$smarty->display("sample/biodata/{$mode}_organisasi.tpl");
?>