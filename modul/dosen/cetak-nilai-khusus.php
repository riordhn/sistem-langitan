<?php
include 'config.php';
include '../../tcpdf/config/lang/ind.php';
include '../../tcpdf/tcpdf.php';
include 'proses/penilaian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$penilaian = new penilaian($db, $login->id_dosen);

$kode_kelas = explode('_', get('id_kelas'));

$data_kelas_mk = $penilaian->get_kelas_mk($kode_kelas['0']);
$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER='{$kode_kelas['1']}'");
$data_semester = $db->FetchAssoc();

$data_mahasiswa = $penilaian->load_data_mahasiswa_khusus(get('id_kelas'));

//Data Mahasiswa
$data_nilai_mahasiswa = '';
$no = 1;
foreach ($data_mahasiswa as $data) {
    $data_nilai_mahasiswa .= "<tr>";
    $data_nilai_mahasiswa .="<td>{$no}</td><td>{$data['nim']}</td><td>{$data['nama']}</td>";
    $data_nilai_mahasiswa .="<td >{$data['nilai_angka_akhir']}</td><td>{$data['nilai_huruf_akhir']}</td>";
    $data_nilai_mahasiswa .= "</tr>";
    $no++;
}


$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .header { font-size: 20pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 14pt; font-family: serif; margin-top: 0px ;text-align:center; }
    .keterangan { font-size: 8pt; }
    td { font-size: 8pt; }
    td.center { font-weight:bold;text-align:center }
    th { background-color:black;color:white;font-size: 7pt;text-align:center; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="10%" align="right"><img src="../../img/maba/logo_unair.gif" width="80px" height="80px"/></td>
        <td width="80%" align="center">
            <span class="header">UNIVERSITAS AIRLANGGA<br/></span><span class="address">CETAK PENILAIAN<br/>
            MATA AJAR {mata_ajar} <br/>KELAS {kelas}</span><br/><span class="address">SEMESTER {semester}</span><br/><span class="address">PROGRAM STUDI {prodi}</span>
        </td>
    </tr>
</table>
<hr/>
<p></p>
<table width="100%" border="1" align="left" cellpadding="5">
        <tr>
            <th width="5%">NO</th>
            <th width="15%">NIM</th>
            <th width="30%">Nama</th>
            <th width="25%">Nilai Angka Akhir</th>
            <th width="25%">Nilai Huruf Akhir</th>
        </tr>
        {data_mahasiswa}
</table>
<p></p>
<table width="30%" border="0" align="left">
    <tr>
        <td align='left'>{distribusi_nilai}</td>
    </tr>
</table>
<table width="100%" border="0" align="center">
    {ttd}
</table>
EOF;
//print_r($data_komponen_mk);exit;
$html = str_replace('{mata_ajar}', strtoupper($data_kelas_mk['NM_MATA_KULIAH'].' ('.$data_kelas_mk['KD_MATA_KULIAH'].')'), $html);
$html = str_replace('{kelas}', $data_kelas_mk['NM_KELAS'], $html);
$html = str_replace('{semester}', strtoupper($data_semester['NM_SEMESTER'] . ' ( ' . $data_semester['TAHUN_AJARAN'] . ' )'), $html);
$html = str_replace('{prodi}', strtoupper($data_kelas_mk['NM_PROGRAM_STUDI']), $html);
$html = str_replace('{colspan}', $jumlah_komponen, $html);
$html = str_replace('{data_komponen}', $data_komponen_mk, $html);
$html = str_replace('{data_mahasiswa}', $data_nilai_mahasiswa, $html);
$html = str_replace('{distribusi_nilai}', $str_distribusi_nilai, $html);
if (isset($login->id_dosen)) {
    $ttd = '
            <tr>
                <td width="60%"></td>
                <td width="40%" align="center">
                Surabaya, ' . strftime('%d %B %Y') . '<br/>Dosen Pengajar
                <br/><br/><br/><br/><br/>' . $user->NM_PENGGUNA . '<br/>NIP.' . $user->USERNAME . '
                </td>
            </tr>';
    $html = str_replace('{ttd}', $ttd, $html);
//echo $html;exit;
} else {
    $html = str_replace('{ttd}', '', $html);
}
$pdf->writeHTML($html);

$pdf->Output();
?>
