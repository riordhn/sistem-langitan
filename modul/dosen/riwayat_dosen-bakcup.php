<?php

include 'config.php';
include 'proses/riwayat_dosen.class.php';

if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}
$riwayat_dosen = new riwayat_dosen($db, $user->ID_PENGGUNA);



$smarty->assign('title', 'Riwayat Dosen');
$smarty->assign('data_golongan', $riwayat_dosen->load_sejarah_golongan());
$smarty->assign('data_fungsi', $riwayat_dosen->load_sejarah_fungsional());
$smarty->assign('data_struktur', $riwayat_dosen->load_sejarah_struktural());
$smarty->assign('data_pendidikan', $riwayat_dosen->load_sejarah_pendidikan());
$smarty->display('sample/biodata/riwayat_dosen.tpl');
?>