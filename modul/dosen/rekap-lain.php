<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$id_fakultas = $user->ID_FAKULTAS;
?>
<div class="center_title_bar">Rekapitulasi Lain</div>
<style>
	.my-list li a:hover {
		text-decoration: underline;
	}
	.my-list li label {
		font-size: 14px;
		cursor: pointer;
		color: #003351;
	}
	.my-list li label:hover {
		text-decoration: underline;
	}
	.my-list li {
		margin-bottom: 3px;
	}
</style>
<script>
	function OpenWindow(url)
	{
		if (id_fakultas == 8)
			newwindow = window.open('../'+url,'mywindow','height=768,width=1024,scrollbars=yes');
		else
			newwindow = window.open(url,'mywindow','height=768,width=1024,scrollbars=yes');
	}
</script>
<ul class="my-list">
	<li><label onclick="location.href=".$base_url."'rekapitulasi/mhs-aktif/';">Mahasiswa Aktif</label></li>
	<li><label onclick="OpenWindow('../../rekapitulasi/mhs-aktif')">Status Mahasiswa<img src="../../img/dosen/newwindow.png" /></label></li>
	<li><label onclick="OpenWindow('../../rekapitulasi/mhs-aktif/mahasiswa-aktif.php')">Mahasiswa Aktif (Jenis Kelamin) <img src="../../img/dosen/newwindow.png" /></label></li>
	<li><label onclick="OpenWindow('../../rekapitulasi/mhs-aktif/jml-mhs.php')">Mahasiswa Aktif (Per Jenjang) <img src="../../img/dosen/newwindow.png" /></label></li>
	<li><label onclick="OpenWindow('../../rekapitulasi/sekolah')">Mahasiswa Aktif (Berdasarkan Sekolah) <img src="../../img/dosen/newwindow.png" /></label></li>
	<li><label onclick="OpenWindow('../../rekapitulasi/penghasilan')">Penghasilan Orang Tua <img src="../../img/dosen/newwindow.png" /></label></li>
	<li><label onclick="OpenWindow('../../rekapitulasi/pendaftaran')">Penerimaan <img src="../../img/dosen/newwindow.png" /></label></li>
	<li><label onclick="OpenWindow('../../rekapitulasi/sekolah/sekolah-tahun.php')">Jumlah Mahasiswa Tiap Sekolah <img src="../../img/dosen/newwindow.png" /></label></li>
	
	<!-- Tidak Dibutuhkan di PTNU
	<li><label onclick="OpenWindow('../../rekapitulasi/sekolah/snmptn/')">Rasio Jumlah siswa pendaftar dan yang diterima (SNMPTN) <img src="../../img/dosen/newwindow.png" /></label></li>
	<li><label onclick="OpenWindow('../../rekapitulasi/sekolah/mandiri/')">Rasio Jumlah siswa pendaftar dan yang diterima (MANDIRI) <img src="../../img/dosen/newwindow.png" /></label></li>
	-->
	<li><label onclick="OpenWindow('../../rekapitulasi/jalur-mahasiswa/')">Jalur Mahasiswa <img src="../../img/dosen/newwindow.png" /></label></li>
</ul>