<?php
include 'config.php';
include 'proses/pesan.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$new_message = new pesan($db, $login->id_dosen,$login->id_pengguna);

if (isset($_POST['bagian'])||isset($_GET['bagian'])) {
    if ($_POST['bagian'] == 'Dosen'||isset($_GET['bagian'])=='Dosen') {
		$smarty->assign('data_fakultas',$new_message->load_fakultas());
		$data_penerima = $new_message->load_dosen(1);
		if($_GET['id_fak']){
			$smarty->assign('post_fakultas',$_GET['id_fak']);
			$data_penerima = $new_message->load_dosen($_GET['id_fak']);	
		}
    } else if ($_POST['bagian'] == 'Mahasiswa') {
        $data_penerima = $new_message->load_mahasiswa();	
    } else {
        $data_penerima = $new_message->load_ortu();
    }
} else if (isset($_POST['send'])) {
    $new_message->insert_pesan(intval($_POST['penerima']), $_POST['subject'], $_POST['pesan'],null);
    $data_penerima = $new_message->load_mahasiswa();
} else {
    $data_penerima = $new_message->load_mahasiswa();
}
$smarty->assign('data_penerima', $data_penerima);
$smarty->assign('post_bagian',isset($_POST['bagian'])?$_POST['bagian']:$_GET['bagian']);
$smarty->assign('data_bagian', array('Mahasiswa', 'Dosen', 'Ortu'));
$smarty->assign('title', 'New Message');
$smarty->display('display-konsultasi/new_message.tpl');
?>