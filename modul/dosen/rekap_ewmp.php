<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$smarty->assign('TITLE', 'Rekap EWMP');

$smarty->display('sample/ewmp/rekap_ewmp.tpl');
?>