<?php

include '../../config.php';
$tahun_sekarang = date('Y');
$querySebaran = $db->QueryToArray("
SELECT P.NM_PROVINSI,CM.* FROM AUCC.PROVINSI P
LEFT JOIN(
  SELECT P.ID_PROVINSI,
  COUNT(ID_C_MHS) PENDAFTAR,
  SUM(
    CASE
      WHEN CMB.TGL_DITERIMA IS NOT NULL
      THEN 1
      ELSE 0
    END  
  ) DITERIMA,
  SUM(
    CASE
      WHEN CMB.TGL_REGMABA IS NOT NULL
      THEN 1
      ELSE 0
    END  
  ) REGMABA,  
  SUM(
    CASE
      WHEN CMB.TGL_VERIFIKASI_PENDIDIKAN IS NOT NULL
      THEN 1
      ELSE 0
    END  
  ) VERIFIKASI
  FROM AUCC.CALON_MAHASISWA_BARU CMB
  LEFT JOIN AUCC.KOTA K ON K.ID_KOTA=CMB.ID_KOTA
  LEFT JOIN AUCC.PROVINSI P ON P.ID_PROVINSI=K.ID_PROVINSI
  JOIN PENERIMAAN PEN ON PEN.ID_PENERIMAAN=CMB.ID_PENERIMAAN
  WHERE PEN.TAHUN='{$tahun_sekarang}' AND PEN.ID_JENJANG IN (1,5)
  GROUP BY P.ID_PROVINSI
) CM ON CM.ID_PROVINSI=P.ID_PROVINSI
WHERE P.ID_NEGARA=114
ORDER BY P.NM_PROVINSI");

function edit_xml_data($file, $peminat, $diterima, $regmaba, $verifikasi) {
    $xmlDoc = new SimpleXMLElement($file, NULL, TRUE);
    $jumlah = count($xmlDoc);
    // Jika isi xml tidak berubah
    if ($xmlDoc->content[$jumlah - 4] != $peminat || $xmlDoc->content[$jumlah - 3] != $diterima || $xmlDoc->content[$jumlah - 2] != $regmaba || $xmlDoc->content[$jumlah - 1] != $verifikasi) {
        $xmlDoc->content[$jumlah - 4] = $peminat;
        $xmlDoc->content[$jumlah - 3] = $diterima;
        $xmlDoc->content[$jumlah - 2] = $regmaba;
        $xmlDoc->content[$jumlah - 1] = $verifikasi;
        $xmlDoc->saveXML($file);
    }
    $xmlTotal = new SimpleXMLElement('Data_sebaran/total.xml', NULL, TRUE);
    $content = str_replace('Data_sebaran/', '', str_replace('.xml', '', $file));
    // Jika isi xml tidak berubah
    if ($xmlTotal->$content != $verifikasi) {
        $xmlTotal->$content = $verifikasi;
        $xmlTotal->saveXML('Data_sebaran/total.xml');
    }
}

foreach ($querySebaran as $data) {
    if ($data['PENDAFTAR'] == '') {
        $data['PENDAFTAR'] = 0;
    }
    if ($data['DITERIMA'] == '') {
        $data['DITERIMA'] = 0;
    }
    if ($data['REGMABA'] == '') {
        $data['REGMABA'] = 0;
    }
    if ($data['VERIFIKASI'] == '') {
        $data['VERIFIKASI'] = 0;
    }
    switch ($data['NM_PROVINSI']) {
        case 'BALI':
            edit_xml_data('Data_sebaran/bali.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'BANTEN':
            edit_xml_data('Data_sebaran/banten.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'BENGKULU':
            edit_xml_data('Data_sebaran/bengkulu.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'DI YOGYAKARTA':
            edit_xml_data('Data_sebaran/jogja.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'DKI JAKARTA':
            edit_xml_data('Data_sebaran/jakarta.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'GORONTALO':
            edit_xml_data('Data_sebaran/gorontalo.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'JAMBI':
            edit_xml_data('Data_sebaran/jambi.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'JAWA BARAT':
            edit_xml_data('Data_sebaran/jabar.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'JAWA TENGAH':
            edit_xml_data('Data_sebaran/jateng.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'JAWA TIMUR':
            edit_xml_data('Data_sebaran/jatim.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'KALIMANTAN BARAT':
            edit_xml_data('Data_sebaran/kalbar.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'KALIMANTAN SELATAN':
            edit_xml_data('Data_sebaran/kalsel.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'KALIMANTAN BARAT':
            edit_xml_data('Data_sebaran/kalbar.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'KALIMANTAN TENGAH':
            edit_xml_data('Data_sebaran/kalteng.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'KALIMANTAN TIMUR':
            edit_xml_data('Data_sebaran/kaltim.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'KEPULAUAN BANGKA BELITUNG':
            edit_xml_data('Data_sebaran/belitung.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'KEPULAUAN RIAU':
            edit_xml_data('Data_sebaran/kepriau.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'LAMPUNG':
            edit_xml_data('Data_sebaran/lampung.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'MALUKU':
            edit_xml_data('Data_sebaran/maluku.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'MALUKU UTARA':
            edit_xml_data('Data_sebaran/malut.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'NANGGROE ACEH DARUSSALAM':
            edit_xml_data('Data_sebaran/nad.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'NUSA TENGGARA BARAT':
            edit_xml_data('Data_sebaran/ntb.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'NUSA TENGGARA TIMUR':
            edit_xml_data('Data_sebaran/ntt.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'PAPUA':
            edit_xml_data('Data_sebaran/irteng.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'PAPUA BARAT':
            edit_xml_data('Data_sebaran/irba.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'RIAU':
            edit_xml_data('Data_sebaran/riau.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'SULAWESI SELATAN':
            edit_xml_data('Data_sebaran/sulsel.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'SULAWESI BARAT':
            edit_xml_data('Data_sebaran/sulbar.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'SULAWESI TENGAH':
            edit_xml_data('Data_sebaran/sulteng.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'SULAWESI TENGGARA':
            edit_xml_data('Data_sebaran/sultra.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'SULAWESI UTARA':
            edit_xml_data('Data_sebaran/sulut.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'SUMATERA BARAT':
            edit_xml_data('Data_sebaran/sumbar.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'SUMATERA SELATAN':
            edit_xml_data('Data_sebaran/sumsel.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
        case 'SUMATERA UTARA':
            edit_xml_data('Data_sebaran/sumut.xml', $data['PENDAFTAR'], $data['DITERIMA'], $data['REGMABA'], $data['VERIFIKASI']);
            break;
    }
}
?>
