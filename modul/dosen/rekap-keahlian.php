<?php

include 'config.php';
include 'proses/history.class.php';

$history = new history($db, $login->id_dosen);

if (isset($_GET)) {
    if (get('mode') == 'cari') {
        $cari = get('cari');
        $query = "
            SELECT P.GELAR_DEPAN,P.GELAR_BELAKANG,P.NM_PENGGUNA,D.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            FROM DOSEN D
            JOIN PENGGUNA P ON D.ID_PENGGUNA=P.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            WHERE UPPER(P.NM_PENGGUNA) LIKE UPPER('%{$cari}%') OR D.NIP_DOSEN LIKE '%{$cari}%' OR P.USERNAME LIKE '%{$cari}%' OR UPPER(D.BIDANG_KEAHLIAN_DOSEN) LIKE UPPER('%{$cari}%')
            ";
        $data_cari = $db->QueryToArray($query);
        $smarty->assign('data_dosen', $data_cari);
    } else if (get('mode') == 'advance-search') {
        // FILTER SEARCH
        $nama = get('nama');
        $fakultas = get('id_fakultas');
        $prodi = get('id_program_studi');
        $matakuliah = get('matakuliah');
        $kota = get('kota');
        $hp = get('hp');
        if ($nama != '') {
            $filter_nama = "AND UPPER(P.NM_PENGGUNA) LIKE UPPER('%{$nama}%')";
        }
        if ($fakultas != '') {
            $filter_fakultas = "AND F.ID_FAKULTAS='{$fakultas}'";
        }
        if ($prodi != '') {
            $filter_prodi = "AND PS.ID_PROGRAM_STUDI='{$prodi}'";
        }
        if ($kota != '') {
            $filter_kota = "AND UPPER(KA.NM_KOTA) LIKE UPPER('%{$kota}%')";
        }
        if ($matakuliah != '') {
            $filter_matakuliah = "AND UPPER(MK.NM_MATA_KULIAH) LIKE UPPER('%{$matakuliah}%')";
        }
        if ($hp != '') {
            $filter_hp = "AND D.MOBILE_DOSEN LIKE '%{$hp}%'";
        }

        $query_find = "SELECT D.NIP_DOSEN,D.ID_DOSEN,D.ID_PENGGUNA,P.GELAR_DEPAN,P.GELAR_BELAKANG,P.NM_PENGGUNA,D.MOBILE_DOSEN,D.BIDANG_KEAHLIAN_DOSEN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS
            FROM AUCC.DOSEN D
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA 
            JOIN AUCC.STATUS_PENGGUNA SP on SP.ID_STATUS_PENGGUNA= D.ID_STATUS_PENGGUNA 
            LEFT JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI 
            LEFT JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            LEFT JOIN AUCC.FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS 
            LEFT JOIN AUCC.KOTA KA ON KA.ID_KOTA=P.ID_KOTA_LAHIR 
            LEFT JOIN AUCC.PENGAMPU_MK PENGMK ON PENGMK.ID_DOSEN=D.ID_DOSEN 
            LEFT JOIN AUCC.KELAS_MK KMK ON PENGMK.ID_KELAS_MK=KMK.ID_KELAS_MK 
            LEFT JOIN AUCC.KURIKULUM_MK KUMK ON KMK.ID_KURIKULUM_MK=KUMK.ID_KURIKULUM_MK 
            LEFT JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH=KUMK.ID_MATA_KULIAH 
            WHERE SP.ID_STATUS_PENGGUNA IS NOT NULL
            {$filter_nama}
            {$filter_fakultas}
            {$filter_prodi}
            {$filter_kota}
            {$filter_hp}
            {$filter_matakuliah} 
            GROUP BY D.NIP_DOSEN,D.ID_DOSEN,D.ID_PENGGUNA,P.GELAR_DEPAN,P.GELAR_BELAKANG,P.NM_PENGGUNA,D.MOBILE_DOSEN,D.BIDANG_KEAHLIAN_DOSEN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS";
        $data_cari = $db->QueryToArray($query_find);

        $smarty->assign('data_dosen', $data_cari);
    } else if (get('mode') == 'detail') {
        $id = get('id');
        $query = "
           select PGG.USERNAME,dsn.id_pengguna, dsn.id_dosen, dsn.nip_dosen as username, dsn.nidn_dosen, dsn.serdos, upper(dsn.alamat_rumah_dosen) as alamat_rumah_dosen, dsn.tlp_dosen, dsn.mobile_dosen, dsn.status_dosen,
		UPPER(PGG.NM_PENGGUNA) AS NM_PENGGUNA, PGG.GELAR_DEPAN, PGG.GELAR_BELAKANG, TO_CHAR(PGG.TGL_LAHIR_PENGGUNA, 'DD-MM-YYYY') AS TGL_LAHIR_PENGGUNA, PGG.EMAIL_PENGGUNA, PGG.EMAIL_ALTERNATE,
		pgg.kelamin_pengguna,TO_CHAR(gol.TMT_SEJARAH_GOLONGAN, 'DD-MM-YYYY') TMT_GOLONGAN, upper(gol.nm_golongan) as nm_golongan, upper(gol.nm_pangkat) as nm_pangkat, upper(fgs.nm_jabatan_fungsional) as nm_jabatan_fungsional,TO_CHAR(fgs.TMT_SEJ_JAB_FUNGSIONAL, 'DD-MM-YYYY') TMT_JAB_FUNGSIONAL
                , kt.tipe_dati2||' '||nm_kota as tempat_lahir,UPPER(JAB.NM_JABATAN_PEGAWAI) AS NM_JABATAN_PEGAWAI, UPPER(STP.NM_STATUS_PENGGUNA) AS NM_STATUS_PENGGUNA, UPPER(NM_JENJANG||' - '||NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI, UPPER(NM_FAKULTAS) AS NM_FAKULTAS
                ,AG.NM_AGAMA,DEP.NM_DEPARTEMEN,SPEN.*
		from AUCC.dosen dsn
		JOIN AUCC.PENGGUNA PGG ON DSN.ID_PENGGUNA=PGG.ID_PENGGUNA
		LEFT JOIN AUCC.KOTA KT ON KT.ID_KOTA=PGG.ID_KOTA_LAHIR
		JOIN AUCC.PROGRAM_STUDI PST ON DSN.ID_PROGRAM_STUDI_SD=PST.ID_PROGRAM_STUDI
		JOIN AUCC.FAKULTAS FAK ON PST.ID_FAKULTAS=FAK.ID_FAKULTAS
                LEFT JOIN AUCC.DEPARTEMEN DEP ON DEP.ID_DEPARTEMEN=PST.ID_DEPARTEMEN
		join AUCC.jenjang jjg on pst.id_jenjang=jjg.id_jenjang
                LEFT JOIN AUCC.AGAMA AG ON AG.ID_AGAMA=PGG.ID_AGAMA
		left join (
                SELECT * FROM (
                    SELECT * FROM (
                      SELECT G.NM_GOLONGAN,G.NM_PANGKAT,SJ.*
                      FROM AUCC.SEJARAH_GOLONGAN SJ
                      JOIN AUCC.GOLONGAN G ON SJ.ID_GOLONGAN=G.ID_GOLONGAN
                      WHERE SJ.ID_PENGGUNA={$id}
                      ORDER BY SJ.TMT_SEJARAH_GOLONGAN DESC
                    ) WHERE ROWNUM=1
                  )
                ) GOL ON GOL.ID_PENGGUNA=DSN.ID_PENGGUNA
		LEFT JOIN (
                    SELECT * FROM (
                        SELECT * FROM (
                          SELECT G.NM_JABATAN_FUNGSIONAL,SJ.*
                          FROM AUCC.SEJARAH_JABATAN_FUNGSIONAL SJ
                          JOIN AUCC.JABATAN_FUNGSIONAL G ON SJ.ID_JABATAN_FUNGSIONAL=G.ID_JABATAN_FUNGSIONAL
                          WHERE SJ.ID_PENGGUNA={$id}
                          ORDER BY SJ.TMT_SEJ_JAB_FUNGSIONAL DESC
                        ) WHERE ROWNUM=1
                  )
                ) FGS ON FGS.ID_PENGGUNA=DSN.ID_PENGGUNA
                LEFT JOIN (
                    SELECT * FROM (
                        SELECT * FROM (
                          SELECT NAMA_PENDIDIKAN_AKHIR,SP.*
                          FROM AUCC.SEJARAH_PENDIDIKAN SP
                          JOIN AUCC.PENDIDIKAN_AKHIR PA ON PA.ID_PENDIDIKAN_AKHIR=SP.ID_PENDIDIKAN_AKHIR
                          WHERE SP.ID_PENGGUNA={$id}
                          ORDER BY SP.TAHUN_LULUS_PENDIDIKAN DESC,ID_SEJARAH_PENDIDIKAN DESC
                        ) WHERE ROWNUM=1
                  )
                ) SPEN ON SPEN.ID_PENGGUNA=DSN.ID_PENGGUNA
		LEFT JOIN AUCC.JABATAN_PEGAWAI JAB ON JAB.ID_JABATAN_PEGAWAI=DSN.ID_JABATAN_PEGAWAI
		left join AUCC.status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
		where DSN.ID_PENGGUNA={$id}
           ";
        $db->Query($query);
        $data_biodata = $db->FetchAssoc();
        if (file_exists("../../foto_pegawai/{$data_biodata['USERNAME']}.JPG")) {
            $smarty->assign('PHOTO', $base_url."foto_pegawai/{$data_biodata['USERNAME']}.JPG");
        } else if (file_exists("../../modulx/images/{$data_biodata['USERNAME']}.JPG")) {
            $smarty->assign('PHOTO', "../dosen/modulx/images/{$data_biodata['USERNAME']}.JPG");
        } else {
            $smarty->assign('foto', $base_url."img/dosen/photo.png");
        }
        $smarty->assign('data_mata_kuliah', $history->load_mata_kuliah_dosen_by_id_pengguna($id));
        $smarty->assign('dsn', $data_biodata);
    } else if (get('mode') == 'detail_mk') {
        $smarty->assign('data_kelas_mk', $history->load_kelas_mk_dosen_external_link(get('dosen'),get('id_mk')));
    }
}

$pimpinan_set = array(1, 2, 3, 4, 30, 31, 32, 33, 53, 81);

if (in_array($user->ID_JABATAN_PEGAWAI, $pimpinan_set)) {
    $smarty->assign('fakultas_set', $db->QueryToArray("select id_fakultas, nm_fakultas from fakultas"));
} else {
    $smarty->assign('fakultas_set', $db->QueryToArray("select id_fakultas, nm_fakultas from fakultas where id_fakultas = {$user->ID_FAKULTAS}"));
}

$smarty->display('sample/rekap/rekap-keahlian.tpl');
?>
