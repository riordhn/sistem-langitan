<?php

include 'conf.php';
include 'proses/info_dosen.class.php';
include 'proses/history.class.php';

$info_dosen = new info_dosen($db, $login->id_pengguna);
$history = new history($db, $login->id_dosen);

$data_info_dosen = $info_dosen->get_biodata();

if (isset($_GET)) {
    if (get('mode') == 'detail') {
        $smarty->assign('data_kelas_mk', $history->load_kelas_mk_dosen(get('id_mk')));
    } else {
        $smarty->assign(array(
            'nama' => $data_info_dosen['NAMA'],
            'nip' => $data_info_dosen['NIP'],
            'fakultas' => $data_info_dosen['FAKULTAS'],
            'prodi' => $data_info_dosen['PRODI']
        ));

        $smarty->assign('data_mata_kuliah', $history->load_mata_kuliah_dosen());
    }
}


$smarty->display('sample/biodata/history.tpl');
?>