<?php

include 'conf.php';
include 'proses/komponen_nilai.class.php';

$komponen_nilai = new komponen_nilai($db, $login->id_dosen);

if (isset($_GET)) {
    if (get('mode') == 'edit') {
        $smarty->assign('data_komponen_up', $komponen_nilai->load_komponen_mk_up_by_id(get('id_komponen')));
        $smarty->assign('id_komponen', get('id_komponen'));
    } else if (get('mode') == 'delete') {
        $smarty->assign('data_komponen_up', $komponen_nilai->load_komponen_mk_up_by_id(get('id_komponen')));
        $smarty->assign('id_komponen', get('id_komponen'));
    }
}

$smarty->display("sample/penilaian/komponen_nilai_up.tpl");
?>