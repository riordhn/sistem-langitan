<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

	$id_program_studi = $user->ID_PROGRAM_STUDI;
	$cur_id_pengguna = $user->ID_PENGGUNA;
	
	$id_teman = array();
	$nm_teman = array();
	$nm_pengguna = array();
	$username_teman = array();
	$jml_permintaan_teman = 0;
	
	$db->Query("select snp.id_teman, p.nm_pengguna, p.username from sn_pertemanan snp 
				left join pengguna p on p.id_pengguna = snp.id_teman
				where snp.id_pengguna = '$cur_id_pengguna' and snp.is_teman = 0 and snp.add_from <> '$cur_id_pengguna'
	");
	$i = 0;
	while ($row = $db->FetchRow()){ 
		$id_teman[$i] = $row[0];
		$nm_teman[$i] = $row[1];
		$username_teman[$i] = $row[2];
		$jml_permintaan_teman++;
		$i++;
	}
	$smarty->assign('cur_id_pengguna', $cur_id_pengguna);
	$smarty->assign('id_teman', $id_teman);	
	$smarty->assign('nm_teman', $nm_teman);
	$smarty->assign('username_teman', $username_teman);
	$smarty->assign('jml_permintaan_teman', $jml_permintaan_teman);
	$smarty->display('sn_pertemanan.tpl');
?>