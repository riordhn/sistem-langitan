<?php

include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}

$cari = str_replace('+', ' ', $_REQUEST['term']);
$data_pengguna = $db->QueryToArray("
    SELECT * FROM (
        SELECT K.ID_KOTA,K.NM_KOTA,P.NM_PROVINSI,N.NM_NEGARA 
        FROM AUCC.KOTA K
        JOIN AUCC.PROVINSI P ON P.ID_PROVINSI=K.ID_PROVINSI
        JOIN AUCC.NEGARA N ON P.ID_NEGARA=N.ID_NEGARA
        WHERE UPPER(K.NM_KOTA) LIKE UPPER('%{$cari}%') 
    ) WHERE ROWNUM<20");
$data_hasil = array();
foreach ($data_pengguna as $d) {
    array_push($data_hasil, array(
        'value' => strtoupper($d['NM_KOTA']),
        'label' => ucwords(strtolower($d['NM_KOTA'])) . ", " . ucwords(strtolower($d['NM_PROVINSI'])).", ".  ucwords(strtolower($d['NM_NEGARA'])) ,
        'id' => (int) $d['ID_KOTA']
    ));
}
echo json_encode($data_hasil)
?>
