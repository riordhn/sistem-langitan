<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }
	
	$id_fakultas = array();
	$nm_fakultas = array();
	$jml_fakultas = 0;
	
	$id_prodi = array();
	$nm_prodi = array();
	$nm_jenjang = array();
	$snmptn = array();
	$mandiri = array();

	$semester_1 = array();
	$semester_2 = array();
	$semester1_usulan = array();
	$semester2_usulan = array();

	$total = array();

	$total_usulan2 = array();
	
	$daya_tampung_sk = array();
	$daya_tampung_nyata = array();
	$daya_tampung_usulan = array();
	$daya_tampung_apk = array();
	
	$total_sk = array();
	$snmptn_sk = array();
	$mandiri_sk = array();
	
	$total_real = array();
	$snmptn_sk = array();
	$mandiri_sk = array();
	
	$total_usulan = array();	
	$snmptn_usulan = array();
	$mandiri_usulan = array();
	$is_alih_jenis = array();
	
	$jml_prodi = 0;

	$nm_fakultas_get = "";

	$nm_program_studi = "";
	
		$get_id_fakultas = $user->ID_FAKULTAS_JABATAN;
		$db->Query("select ps.id_program_studi, ps.nm_program_studi, j.nm_jenjang, prodt.snmptn_sk, prodt.mandiri_sk, prodt.snmptn_real, prodt.mandiri_real, prodt.snmptn_usulan, prodt.mandiri_usulan, prodt.daya_tampung_apk, prodt.daya_tampung_usulan, prodt.semester_1_usulan, prodt.semester_2_usulan, prodt.is_alih_jenis 
					from prodi_dayatampung prodt
					left join program_studi ps on ps.id_program_studi = prodt.id_program_studi
					left join fakultas f on f.id_fakultas = ps.id_fakultas
					left join jenjang j on j.id_jenjang = ps.id_jenjang
					where ps.id_fakultas = '" . $get_id_fakultas . "' 
					order by prodt.id_program_studi");

								$i = 0;
								while ($row = $db->FetchRow()){ 
									$id_prodi[$i] = $row[0];
									$nm_prodi[$i] = $row[1];
									$nm_jenjang[$i] = $row[2];
									$snmptn_sk[$i] = $row[3];
									$mandiri_sk[$i] = $row[4];
									$snmptn_real[$i] = $row[5];
									$mandiri_real[$i] = $row[6];
									$snmptn_usulan[$i] = $row[7];
									$mandiri_usulan[$i] = $row[8];
									$daya_tampung_apk[$i] = $row[9];
									$daya_tampung_usulan[$i] = $row[10];
									$semester1_usulan[$i] = $row[11];
									$semester2_usulan[$i] = $row[12];
									$is_alih_jenis[$i] = $row[13];
									$total_usulan[$i] = $snmptn_usulan[$i] + $mandiri_usulan[$i];
									$total_usulan2[$i] = $semester1_usulan[$i] + $semester2_usulan[$i];
									$jml_prodi++;
									$i++;
								}
		
?>


 
	<script>
	$(function() {
		$( "#radio_PPCMB_1" ).buttonset();
		$( "#radio_PPCMB_3" ).buttonset();
		
		<?php
			for($i=0;$i<$jml_prodi;$i++){
			if($nm_jenjang[$i]=='S1'){
				if($is_alih_jenis[$i]==0){
		?>
				$("#submit<?php echo $id_prodi[$i]; ?>").buttonset();

				$("#btnEdit_div<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>").button({
					icons: {
						primary: "ui-icon-pencil",
					},
					text: false
				});
				
				
				$('#btnEdit_div<?php echo $id_prodi[$i]; ?>').click(function(){
					$('#snmptn_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
					$('#mandiri_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
					
					$('#snmptn_usulan<?php echo $id_prodi[$i]; ?>').show();
					$('#mandiri_usulan<?php echo $id_prodi[$i]; ?>').show();
					
					$('#btnEdit_div<?php echo $id_prodi[$i]; ?>').hide();
					$('#submit<?php echo $id_prodi[$i]; ?>').show();
					
				});
				
				$('#myForm<?php echo $id_prodi[$i]; ?>').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
							$('#total_usulan_div<?php echo $id_prodi[$i]; ?>').show();
							$('#snmptn_usulan_div<?php echo $id_prodi[$i]; ?>').show();
							$('#mandiri_usulan_div<?php echo $id_prodi[$i]; ?>').show();

							$('#btnEdit_div<?php echo $id_prodi[$i]; ?>').show();

							
							$('#total_usulan<?php echo $id_prodi[$i]; ?>').hide();
							$('#snmptn_usulan<?php echo $id_prodi[$i]; ?>').hide();
							$('#mandiri_usulan<?php echo $id_prodi[$i]; ?>').hide();

							$('#submit<?php echo $id_prodi[$i]; ?>').hide();


			
							var total_usulan_div = document.getElementById('total_usulan_div<?php echo $id_prodi[$i]; ?>');
							var snmptn_usulan_div = document.getElementById('snmptn_usulan_div<?php echo $id_prodi[$i]; ?>');
							var mandiri_usulan_div = document.getElementById('mandiri_usulan_div<?php echo $id_prodi[$i]; ?>');

							
							snmptn_usulan_div.innerHTML = document.getElementById('snmptn_usulan<?php echo $id_prodi[$i]; ?>').value;
							mandiri_usulan_div.innerHTML = document.getElementById('mandiri_usulan<?php echo $id_prodi[$i]; ?>').value;
							total_usulan_div.innerHTML = parseInt(document.getElementById('snmptn_usulan<?php echo $id_prodi[$i]; ?>').value) + parseInt(document.getElementById('mandiri_usulan<?php echo $id_prodi[$i]; ?>').value);
							//$('#result').fadeIn('fast');
							//$('#result').html(data);
							
						}
					})
					return false;
				});
		

		
		<?php
					}
				}
			}
		?>
		
		<?php
			for($i=0;$i<$jml_prodi;$i++){
				if($nm_jenjang[$i]!=='S1'){
					if($is_alih_jenis[$i]==0){
		?>

				$("#submit2<?php echo $id_prodi[$i]; ?>").buttonset();
				
				$("#btnEdit2_div<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>").button({
					icons: {
						primary: "ui-icon-pencil",
					},
					text: false
				});
				
				$('#btnEdit2_div<?php echo $id_prodi[$i]; ?>').click(function(){
					$('#daya_tampung_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
					
					$('#daya_tampung_usulan<?php echo $id_prodi[$i]; ?>').show();
					
					$('#btnEdit2_div<?php echo $id_prodi[$i]; ?>').hide();
					$('#submit2<?php echo $id_prodi[$i]; ?>').show();
				});
				
				$('#myForm2<?php echo $id_prodi[$i]; ?>').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
							$('#daya_tampung_usulan_div<?php echo $id_prodi[$i]; ?>').show();
							$('#btnEdit2_div<?php echo $id_prodi[$i]; ?>').show();
							
							$('#daya_tampung_usulan<?php echo $id_prodi[$i]; ?>').hide();

							$('#submit2<?php echo $id_prodi[$i]; ?>').hide();

							var daya_tampung_usulan_div = document.getElementById('daya_tampung_usulan_div<?php echo $id_prodi[$i]; ?>');
							
							daya_tampung_usulan_div.innerHTML = document.getElementById('daya_tampung_usulan<?php echo $id_prodi[$i]; ?>').value;
							
						}
					})
					return false;
				});
		

		
		<?php
					}
				}
			}
		?>
		
		
		<?php
			for($i=0;$i<$jml_prodi;$i++){
				if($nm_jenjang[$i]=='Spesialis' || $nm_jenjang[$i]=='Profesi'){
					if($is_alih_jenis[$i]==0){
		?>

				$("#submit3<?php echo $id_prodi[$i]; ?>").buttonset();

				$("#btnEdit3_div<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>").button({
					icons: {
						primary: "ui-icon-pencil",
					},
					text: false
				});
				
				
				$('#btnEdit3_div<?php echo $id_prodi[$i]; ?>').click(function(){
					$('#semester1_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
					$('#semester2_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
					
					$('#semester1_usulan<?php echo $id_prodi[$i]; ?>').show();
					$('#semester2_usulan<?php echo $id_prodi[$i]; ?>').show();
					
					$('#btnEdit3_div<?php echo $id_prodi[$i]; ?>').hide();
					$('#submit3<?php echo $id_prodi[$i]; ?>').show();
					
				});
				
				$('#myForm3<?php echo $id_prodi[$i]; ?>').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
							$('#total_usulan_div<?php echo $id_prodi[$i]; ?>').show();
							$('#semester1_usulan_div<?php echo $id_prodi[$i]; ?>').show();
							$('#semester2_usulan_div<?php echo $id_prodi[$i]; ?>').show();

							$('#btnEdit3_div<?php echo $id_prodi[$i]; ?>').show();

							
							$('#total_usulan<?php echo $id_prodi[$i]; ?>').hide();
							$('#semester1_usulan<?php echo $id_prodi[$i]; ?>').hide();
							$('#semester2_usulan<?php echo $id_prodi[$i]; ?>').hide();

							$('#submit3<?php echo $id_prodi[$i]; ?>').hide();


			
							var total_usulan_div = document.getElementById('total_usulan_div<?php echo $id_prodi[$i]; ?>');
							var semester1_usulan_div = document.getElementById('semester1_usulan_div<?php echo $id_prodi[$i]; ?>');
							var semester2_usulan_div = document.getElementById('semester2_usulan_div<?php echo $id_prodi[$i]; ?>');

							
							semester1_usulan_div.innerHTML = document.getElementById('semester1_usulan<?php echo $id_prodi[$i]; ?>').value;
							semester2_usulan_div.innerHTML = document.getElementById('semester2_usulan<?php echo $id_prodi[$i]; ?>').value;
							total_usulan_div.innerHTML = parseInt(document.getElementById('semester1_usulan<?php echo $id_prodi[$i]; ?>').value) + parseInt(document.getElementById('semester2_usulan<?php echo $id_prodi[$i]; ?>').value);
							
						}
					})
					return false;
				});
		

		
		<?php
					}
				}
			}
		?>
		
		
//Alih Jenjang
		<?php
			for($i=0;$i<$jml_prodi;$i++){
				if($nm_jenjang[$i]=='S1'){
					if($is_alih_jenis[$i]==1){
		?>
				$("#submit4<?php echo $id_prodi[$i]; ?>").buttonset();

				$("#btnEdit4_div<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>").button({
					icons: {
						primary: "ui-icon-pencil",
					},
					text: false
				});
				
				
				$('#btnEdit4_div<?php echo $id_prodi[$i]; ?>').click(function(){
					$('#snmptn_usulan4_div<?php echo $id_prodi[$i]; ?>').hide();
					$('#mandiri_usulan4_div<?php echo $id_prodi[$i]; ?>').hide();
					
					$('#snmptn_usulan4<?php echo $id_prodi[$i]; ?>').show();
					$('#mandiri_usulan4<?php echo $id_prodi[$i]; ?>').show();
					
					$('#btnEdit4_div<?php echo $id_prodi[$i]; ?>').hide();
					$('#submit4<?php echo $id_prodi[$i]; ?>').show();
					
				});
				
				$('#myForm4<?php echo $id_prodi[$i]; ?>').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
							$('#total_usulan4_div<?php echo $id_prodi[$i]; ?>').show();
							$('#snmptn_usulan4_div<?php echo $id_prodi[$i]; ?>').show();
							$('#mandiri_usulan4_div<?php echo $id_prodi[$i]; ?>').show();

							$('#btnEdit4_div<?php echo $id_prodi[$i]; ?>').show();

							
							$('#total_usulan4<?php echo $id_prodi[$i]; ?>').hide();
							$('#snmptn_usulan4<?php echo $id_prodi[$i]; ?>').hide();
							$('#mandiri_usulan4<?php echo $id_prodi[$i]; ?>').hide();

							$('#submit4<?php echo $id_prodi[$i]; ?>').hide();


			
							var total_usulan4_div = document.getElementById('total_usulan4_div<?php echo $id_prodi[$i]; ?>');
							var snmptn_usulan4_div = document.getElementById('snmptn_usulan4_div<?php echo $id_prodi[$i]; ?>');
							var mandiri_usulan4_div = document.getElementById('mandiri_usulan4_div<?php echo $id_prodi[$i]; ?>');

							
							snmptn_usulan4_div.innerHTML = document.getElementById('snmptn_usulan4<?php echo $id_prodi[$i]; ?>').value;
							mandiri_usulan4_div.innerHTML = document.getElementById('mandiri_usulan4<?php echo $id_prodi[$i]; ?>').value;
							total_usulan4_div.innerHTML = parseInt(document.getElementById('snmptn_usulan4<?php echo $id_prodi[$i]; ?>').value) + parseInt(document.getElementById('mandiri_usulan4<?php echo $id_prodi[$i]; ?>').value);
							//$('#result').fadeIn('fast');
							//$('#result').html(data);
							
						}
					})
					return false;
				});
		

		
		<?php
					}
				}
			}
		?>

		
	});
	</script>
	
<script>


</script>
	
	

		<style type="text/css">
			h4 { font-size: 18px; }
			input { padding: 3px; border: 1px solid #999; }
			td { padding: 5px; }
			#result { background-color: #ffffcc; border: 1px solid #ffff99; padding: 10px; width: 100%; margin-bottom: 20px; }
		</style>

<div class="center_title_bar">DAYA TAMPUNG PROGRAM STUDI</div> 
<?php
	include 'get_user_agent.php';
	$browser = $ua['name'];
	if($browser!=='Google Chrome'){
?>
<span style="font-size:18px;">Mohon gunakan browser</span> <img src="chrome_logo.gif" /> <span style="font-size:18px;">untuk mengakses modul ini</span>
<br />
Atau download Google Chrome di link ini, jika anda tidak memiliki browser tersebut : <a onclick="window.location.href='http://www.google.com/chrome/?hl=en&brand=chmo#eula'" href="">Google Chrome Downloads</a>
<?php
	}
	else{
?>
		<div id="loading" style="display:none;"><img src="loading.gif" alt="loading..." /></div>
		<div id="result" style="display:none; padding-top:10px; width:70%;"></div>

<script>
	$(function() {
		$( "#tabs" ).tabs();
	});
</script>


<div class="demo">
<div id="tabs" style="width:790px;">
	<ul>
		<li><a href="#tabs-1">S1</a></li>
		<li><a href="#tabs-2">D3, S2, S3</a></li>
		<li><a href="#tabs-3">SPESIALIS, PROFESI</a></li>
		<li><a href="#tabs-4">ALIH JENIS</a></li>
	</ul>
	<div id="tabs-1">
		<table class="ui-widget" width="70%" >
			<thead class="ui-widget-header">
				<tr class="ui-widget-header">
					<td class="header-coloumn" rowspan="2">No</td>
					<td class="header-coloumn" rowspan="2">Program Studi</td>
					<td class="header-coloumn" colspan="3" rowspan="1" ><center>Sesuai SK 2011</center></td>
					<td class="header-coloumn" colspan="3" rowspan="1" ><center>Realisasi</center></td>
					<td class="header-coloumn" colspan="3" rowspan="1" ><center>Usulan</center></td>
					<td class="header-coloumn" rowspan="2"><center>APK</center></td>					
					<td class="header-coloumn" rowspan="2">Action</td>
				</tr>
				<tr>
					<td class="header-coloumn" ><center>TOT</center></td>
					<td class="header-coloumn" ><center>SNMPTN</center></td>
					<td class="header-coloumn" ><center>MANDIRI</center></td>
					<td class="header-coloumn" ><center>TOT</center></td>
					<td class="header-coloumn" ><center>SNMPTN</center></td>
					<td class="header-coloumn" ><center>MANDIRI</center></td>
					<td class="header-coloumn" ><center>TOT</center></td>
					<td class="header-coloumn" ><center>SNMPTN</center></td>
					<td class="header-coloumn" ><center>MANDIRI</center></td>
				</tr>
			</thead>
			
			<?php
				for($i=0;$i<$jml_prodi;$i++){
					if($nm_jenjang[$i]=='S1'){
						if($is_alih_jenis[$i]==0){
			?>
			
			<form id="myForm<?php echo $id_prodi[$i]; ?>"  name="myForm<?php echo $id_prodi[$i]; ?>" method="post" action="daya_tampung_insert.php" style="display:none;">
			<tbody id="listDayaTampung">
				<tr>
					<td class="ui-widget-content"><?php echo $i+1; ?></td>
					<td class="ui-widget-content">
						<a href="spek_prodi.php?id=<?php echo $id_prodi[$i]; ?>&prodi=<?php echo $i; ?>">
						<?php
							echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i];
						?>
						</a>
						<input type="text" id="id_prodi" name="id_prodi" style="display:none;" value="<?php echo $id_prodi[$i]; ?>" />
						<input type="text" id="alihjenis" name="alihjenis" style="display:none;" value="<?php echo $is_alih_jenis[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="total_sk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $total_sk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="total_sk<?php echo $id_prodi[$i]; ?>" name="total_sk<?php echo $id_prodi[$i]; ?>" value="<?php echo $total_sk[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="snmptn_sk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $snmptn_sk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="snmptn_sk<?php echo $id_prodi[$i]; ?>" name="snmptn_sk<?php echo $id_prodi[$i]; ?>" value="<?php echo $snmptn_sk[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="mandiri_sk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $mandiri_sk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="mandiri_sk<?php echo $id_prodi[$i]; ?>" name="mandiri_sk<?php echo $id_prodi[$i]; ?>" value="<?php echo $mandiri_sk[$i]; ?>" />
					</td>
					<?php
						$prodi_diterima = 0;
						$db->Query("select count(*) from (
									select PCM.ID_C_MHS, sum(pcm.besar_biaya) from PEMBAYARAN_CMHS pcm
									left join DETAIL_BIAYA db on db.ID_DETAIL_BIAYA = PCM.ID_DETAIL_BIAYA
									left join BIAYA_KULIAH bk on bk.id_biaya_kuliah = DB.ID_BIAYA_KULIAH
									left join CALON_MAHASISWA cm on CM.ID_C_MHS = PCM.ID_C_MHS
									where bk.id_kelompok_biaya = 1 and CM.ID_PROGRAM_STUDI = '" . $id_prodi[$i] . "'
									group by PCM.ID_C_MHS)
						");
						while ($row = $db->FetchRow()){ 
							$prodi_diterima = $row[0];
						}
					?>
					<td class="ui-widget-content" >
						<div id="total_real_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $total_real[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="total_real<?php echo $id_prodi[$i]; ?>" name="total_real<?php echo $id_prodi[$i]; ?>" value="<?php echo $total_real[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="snmptn_real_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $snmptn_real[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="snmptn_real<?php echo $id_prodi[$i]; ?>" name="snmptn_real<?php echo $id_prodi[$i]; ?>" value="<?php echo $snmptn_real[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="mandiri_real_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $mandiri_real[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="mandiri_real<?php echo $id_prodi[$i]; ?>" name="mandiri_real<?php echo $id_prodi[$i]; ?>" value="<?php echo $mandiri_real[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="total_usulan_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $total_usulan[$i]; ?>
						</div>
						<input style="display:none; width:30px;" type="text" id="total_usulan<?php echo $id_prodi[$i]; ?>" name="total_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $total_usulan[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="snmptn_usulan_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $snmptn_usulan[$i]; ?>
						</div>
						<input style="display:none; width:30px;" type="text" id="snmptn_usulan<?php echo $id_prodi[$i]; ?>" name="snmptn_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $snmptn_usulan[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="mandiri_usulan_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $mandiri_usulan[$i]; ?>
						</div>
						<input style="display:none; width:30px;" type="text" id="mandiri_usulan<?php echo $id_prodi[$i]; ?>" name="mandiri_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $mandiri_usulan[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="daya_tampung_apk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $daya_tampung_apk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="daya_tampung_apk<?php echo $id_prodi[$i]; ?>" name="daya_tampung_apk<?php echo $id_prodi[$i]; ?>" value="<?php echo $daya_tampung_apk[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="btnEdit_div<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>" name="btnEdit<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>">
							Edit
						</div>
						<div style="display:none;" id="submit<?php echo $id_prodi[$i]; ?>" name="submit<?php echo $id_prodi[$i]; ?>">
							<input type="submit" name="btn_submit" id="btn_submit" value="Submit"  />
						</div>
					</td>
				</tr>
			</tbody>
			</form>
			<?php
						}
					}
				}
			?>
		</table>
	</div>
	<div id="tabs-2">
		<table class="ui-widget" width="70%" >
			<thead class="ui-widget-header">
				<tr class="ui-widget-header">
					<td class="header-coloumn" >No</td>
					<td class="header-coloumn" >Program Studi</td>
					<td class="header-coloumn" ><center>Sesuai SK 2011</center></td>
					<td class="header-coloumn" ><center>Realisasi</center></td>
					<td class="header-coloumn" ><center>Usulan</center></td>
					<td class="header-coloumn" ><center>APK</center></td>					
					<td class="header-coloumn" >Action</td>
				</tr>
			</thead>
			
			<?php
				for($i=0;$i<$jml_prodi;$i++){
					if($nm_jenjang[$i]=='D3' || $nm_jenjang[$i]=='S2' || $nm_jenjang[$i]=='S3'){
						if($is_alih_jenis[$i]==0){
			?>
			
			<form id="myForm2<?php echo $id_prodi[$i]; ?>"  name="myForm<?php echo $id_prodi[$i]; ?>" method="post" action="daya_tampung_insert.php" style="display:none;">
			<tbody id="listDayaTampung">
				<tr>
					<td class="ui-widget-content"><?php echo $i+1; ?></td>
					<td class="ui-widget-content">
						<a href="spek_prodi.php?id=<?php echo $id_prodi[$i]; ?>&prodi=<?php echo $i; ?>">
						<?php
							echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i];
						?>
						</a>
						<input type="text" id="id_prodi" name="id_prodi" style="display:none;" value="<?php echo $id_prodi[$i]; ?>" />
						<input type="text" id="alihjenis" name="alihjenis" style="display:none;" value="<?php echo $is_alih_jenis[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="daya_tampung_sk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $daya_tampung_sk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="daya_tampung_sk<?php echo $id_prodi[$i]; ?>" name="daya_tampung_sk<?php echo $id_prodi[$i]; ?>" value="<?php echo $daya_tampung_sk[$i]; ?>" />
					</td>
					<?php
						$prodi_diterima = 0;
						$db->Query("select count(*) from (
									select PCM.ID_C_MHS, sum(pcm.besar_biaya) from PEMBAYARAN_CMHS pcm
									left join DETAIL_BIAYA db on db.ID_DETAIL_BIAYA = PCM.ID_DETAIL_BIAYA
									left join BIAYA_KULIAH bk on bk.id_biaya_kuliah = DB.ID_BIAYA_KULIAH
									left join CALON_MAHASISWA cm on CM.ID_C_MHS = PCM.ID_C_MHS
									where bk.id_kelompok_biaya = 1 and CM.ID_PROGRAM_STUDI = '8'
									group by PCM.ID_C_MHS)
						");
						while ($row = $db->FetchRow()){ 
							$prodi_diterima = $row[0];
						}
					?>
					<td class="ui-widget-content" >
						<div id="daya_tampung_nyata_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $prodi_diterima; ?>
						</div>
						<input style="display:none;" type="text" id="daya_tampung_nyata<?php echo $id_prodi[$i]; ?>" name="daya_tampung_nyata<?php echo $id_prodi[$i]; ?>" value="<?php echo $daya_tampung_nyata[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="daya_tampung_usulan_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $daya_tampung_usulan[$i]; ?>
						</div>
						<input style="display:none; width:50px;" type="text" id="daya_tampung_usulan<?php echo $id_prodi[$i]; ?>" name="daya_tampung_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $daya_tampung_usulan[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="daya_tampung_apk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $daya_tampung_apk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="daya_tampung_apk<?php echo $id_prodi[$i]; ?>" name="daya_tampung_apk<?php echo $id_prodi[$i]; ?>" value="<?php echo $daya_tampung_apk[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="btnEdit2_div<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>" name="btnEdit<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>">
							Edit
						</div>
						<div style="display:none;" id="submit2<?php echo $id_prodi[$i]; ?>" name="submit2<?php echo $id_prodi[$i]; ?>">
							<input type="submit" name="btn_submit2" id="btn_submit2" value="Submit"  />
						</div>
					</td>
				</tr>
			</tbody>
			</form>
			<?php
						}
					}
				}
			?>
		</table>
	</div>
	
	<div id="tabs-3">
		<table class="ui-widget" width="70%" >
			<thead class="ui-widget-header">
				<tr class="ui-widget-header">
					<td class="header-coloumn" rowspan="2">No</td>
					<td class="header-coloumn" rowspan="2">Program Studi</td>
					<td class="header-coloumn" colspan="3" rowspan="1" ><center>Sesuai SK 2011</center></td>
					<td class="header-coloumn" colspan="3" rowspan="1" ><center>Realisasi</center></td>
					<td class="header-coloumn" colspan="3" rowspan="1" ><center>Usulan</center></td>
					<td class="header-coloumn" rowspan="2"><center>APK</center></td>					
					<td class="header-coloumn" rowspan="2">Action</td>
				</tr>
				<tr>
					<td class="header-coloumn" ><center>TOT</center></td>
					<td class="header-coloumn" ><center>SMT I</center></td>
					<td class="header-coloumn" ><center>SMT II</center></td>
					<td class="header-coloumn" ><center>TOT</center></td>
					<td class="header-coloumn" ><center>SMT I</center></td>
					<td class="header-coloumn" ><center>SMT II</center></td>
					<td class="header-coloumn" ><center>TOT</center></td>
					<td class="header-coloumn" ><center>SMT I</center></td>
					<td class="header-coloumn" ><center>SMT II</center></td>
				</tr>
			</thead>
			
			<?php
				for($i=0;$i<$jml_prodi;$i++){
					if($nm_jenjang[$i]=='Profesi' || $nm_jenjang[$i]=='Spesialis'){
						if($is_alih_jenis[$i]==0){
			?>
			
			<form id="myForm3<?php echo $id_prodi[$i]; ?>"  name="myForm3<?php echo $id_prodi[$i]; ?>" method="post" action="daya_tampung_insert.php">
			<tbody id="listDayaTampung">
				<tr>
					<td class="ui-widget-content"><?php echo $i+1; ?></td>
					<td class="ui-widget-content">
						<a href="spek_prodi.php?id=<?php echo $id_prodi[$i]; ?>&prodi=<?php echo $i; ?>">
						<?php
							echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i];
						?>
						</a>
						<input type="text" id="id_prodi" name="id_prodi" style="display:none;" value="<?php echo $id_prodi[$i]; ?>" />
						<input type="text" id="alihjenis" name="alihjenis" style="display:none;" value="<?php echo $is_alih_jenis[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="total_sk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $total_sk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="total_sk<?php echo $id_prodi[$i]; ?>" name="total_sk<?php echo $id_prodi[$i]; ?>" value="<?php echo $total_sk[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="semester1_sk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $semester1_sk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="snmptn_sk<?php echo $id_prodi[$i]; ?>" name="snmptn_sk<?php echo $id_prodi[$i]; ?>" value="<?php echo $snmptn_sk[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="semester2_sk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $semester2_sk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="mandiri_sk<?php echo $id_prodi[$i]; ?>" name="mandiri_sk<?php echo $id_prodi[$i]; ?>" value="<?php echo $mandiri_sk[$i]; ?>" />
					</td>
					<?php
						$prodi_diterima = 0;
						$db->Query("select count(*) from (
									select PCM.ID_C_MHS, sum(pcm.besar_biaya) from PEMBAYARAN_CMHS pcm
									left join DETAIL_BIAYA db on db.ID_DETAIL_BIAYA = PCM.ID_DETAIL_BIAYA
									left join BIAYA_KULIAH bk on bk.id_biaya_kuliah = DB.ID_BIAYA_KULIAH
									left join CALON_MAHASISWA cm on CM.ID_C_MHS = PCM.ID_C_MHS
									where bk.id_kelompok_biaya = 1 and CM.ID_PROGRAM_STUDI = '" . $id_prodi[$i] . "'
									group by PCM.ID_C_MHS)
						");
						while ($row = $db->FetchRow()){ 
							$prodi_diterima = $row[0];
						}
					?>
					<td class="ui-widget-content" >
						<div id="total_real_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $total_real[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="total_real<?php echo $id_prodi[$i]; ?>" name="total_real<?php echo $id_prodi[$i]; ?>" value="<?php echo $total_real[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="semester1_real_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $semester1_real[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="snmptn_real<?php echo $id_prodi[$i]; ?>" name="snmptn_real<?php echo $id_prodi[$i]; ?>" value="<?php echo $snmptn_real[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="semester2_real_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $semester2_real[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="mandiri_real<?php echo $id_prodi[$i]; ?>" name="mandiri_real<?php echo $id_prodi[$i]; ?>" value="<?php echo $mandiri_real[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="total_usulan_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $total_usulan2[$i]; ?>
						</div>
						<input style="display:none; width:30px;" type="text" id="total_usulan<?php echo $id_prodi[$i]; ?>" name="total_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $total_usulan2[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="semester1_usulan_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $semester1_usulan[$i]; ?>
						</div>
						<input style="display:none; width:30px;" type="text" id="semester1_usulan<?php echo $id_prodi[$i]; ?>" name="semester1_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $semester1_usulan[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="semester2_usulan_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $semester2_usulan[$i]; ?>
						</div>
						<input style="display:none; width:30px;" type="text" id="semester2_usulan<?php echo $id_prodi[$i]; ?>" name="semester2_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $semester2_usulan[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="daya_tampung_apk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $daya_tampung_apk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="daya_tampung_apk<?php echo $id_prodi[$i]; ?>" name="daya_tampung_apk<?php echo $id_prodi[$i]; ?>" value="<?php echo $daya_tampung_apk[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="btnEdit3_div<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>" name="btnEdit3<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>">
							Edit
						</div>
						<div style="display:none;" id="submit3<?php echo $id_prodi[$i]; ?>" name="submit3<?php echo $id_prodi[$i]; ?>">
							<input type="submit" name="btn_submit3" id="btn_submit3" value="Submit"  />
						</div>
					</td>
				</tr>
			</tbody>
			</form>
			<?php
						}
					}
				}
			?>
		</table>
	</div>
	<div id="tabs-4">
		<table class="ui-widget" width="70%" >
			<thead class="ui-widget-header">
				<tr class="ui-widget-header">
					<td class="header-coloumn" rowspan="2">No</td>
					<td class="header-coloumn" rowspan="2">Program Studi</td>
					<td class="header-coloumn" colspan="3" rowspan="1" ><center>Sesuai SK 2011</center></td>
					<td class="header-coloumn" colspan="3" rowspan="1" ><center>Realisasi</center></td>
					<td class="header-coloumn" colspan="3" rowspan="1" ><center>Usulan</center></td>
					<td class="header-coloumn" rowspan="2"><center>APK</center></td>					
					<td class="header-coloumn" rowspan="2">Action</td>
				</tr>
				<tr>
					<td class="header-coloumn" ><center>TOT</center></td>
					<td class="header-coloumn" ><center>SNMPTN</center></td>
					<td class="header-coloumn" ><center>MANDIRI</center></td>
					<td class="header-coloumn" ><center>TOT</center></td>
					<td class="header-coloumn" ><center>SNMPTN</center></td>
					<td class="header-coloumn" ><center>MANDIRI</center></td>
					<td class="header-coloumn" ><center>TOT</center></td>
					<td class="header-coloumn" ><center>SNMPTN</center></td>
					<td class="header-coloumn" ><center>MANDIRI</center></td>
				</tr>
			</thead>
			
			<?php
				for($i=0;$i<$jml_prodi;$i++){
					if($nm_jenjang[$i]=='S1'){
						if($is_alih_jenis[$i]==1){
			?>
			
			<form id="myForm4<?php echo $id_prodi[$i]; ?>"  name="myForm4<?php echo $id_prodi[$i]; ?>" method="post" action="daya_tampung_insert.php" style="display:none;">
			<tbody id="listDayaTampung">
				<tr>
					<td class="ui-widget-content"><?php echo $i+1; ?></td>
					<td class="ui-widget-content">
						<a href="spek_prodi.php?id=<?php echo $id_prodi[$i]; ?>&prodi=<?php echo $i; ?>">
						<?php
							echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i];
						?>
						</a>
						<input type="text" id="id_prodi" name="id_prodi" style="display:none;" value="<?php echo $id_prodi[$i]; ?>" />
						<input type="text" id="alihjenis" name="alihjenis" style="display:none;" value="<?php echo $is_alih_jenis[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="total_sk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $total_sk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="total_sk<?php echo $id_prodi[$i]; ?>" name="total_sk<?php echo $id_prodi[$i]; ?>" value="<?php echo $total_sk[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="snmptn_sk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $snmptn_sk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="snmptn_sk<?php echo $id_prodi[$i]; ?>" name="snmptn_sk<?php echo $id_prodi[$i]; ?>" value="<?php echo $snmptn_sk[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="mandiri_sk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $mandiri_sk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="mandiri_sk<?php echo $id_prodi[$i]; ?>" name="mandiri_sk<?php echo $id_prodi[$i]; ?>" value="<?php echo $mandiri_sk[$i]; ?>" />
					</td>
					<?php
						$prodi_diterima = 0;
						$db->Query("select count(*) from (
									select PCM.ID_C_MHS, sum(pcm.besar_biaya) from PEMBAYARAN_CMHS pcm
									left join DETAIL_BIAYA db on db.ID_DETAIL_BIAYA = PCM.ID_DETAIL_BIAYA
									left join BIAYA_KULIAH bk on bk.id_biaya_kuliah = DB.ID_BIAYA_KULIAH
									left join CALON_MAHASISWA cm on CM.ID_C_MHS = PCM.ID_C_MHS
									where bk.id_kelompok_biaya = 1 and CM.ID_PROGRAM_STUDI = '" . $id_prodi[$i] . "'
									group by PCM.ID_C_MHS)
						");
						while ($row = $db->FetchRow()){ 
							$prodi_diterima = $row[0];
						}
					?>
					<td class="ui-widget-content" >
						<div id="total_real_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $total_real[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="total_real<?php echo $id_prodi[$i]; ?>" name="total_real<?php echo $id_prodi[$i]; ?>" value="<?php echo $total_real[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="snmptn_real_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $snmptn_real[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="snmptn_real<?php echo $id_prodi[$i]; ?>" name="snmptn_real<?php echo $id_prodi[$i]; ?>" value="<?php echo $snmptn_real[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="mandiri_real_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $mandiri_real[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="mandiri_real<?php echo $id_prodi[$i]; ?>" name="mandiri_real<?php echo $id_prodi[$i]; ?>" value="<?php echo $mandiri_real[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="total_usulan4_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $total_usulan[$i]; ?>
						</div>
						<input style="display:none; width:30px;" type="text" id="total_usulan4<?php echo $id_prodi[$i]; ?>" name="total_usulan4<?php echo $id_prodi[$i]; ?>" value="<?php echo $total_usulan[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="snmptn_usulan4_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $snmptn_usulan[$i]; ?>
						</div>
						<input style="display:none; width:30px;" type="text" id="snmptn_usulan4<?php echo $id_prodi[$i]; ?>" name="snmptn_usulan4<?php echo $id_prodi[$i]; ?>" value="<?php echo $snmptn_usulan[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="mandiri_usulan4_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $mandiri_usulan[$i]; ?>
						</div>
						<input style="display:none; width:30px;" type="text" id="mandiri_usulan4<?php echo $id_prodi[$i]; ?>" name="mandiri_usulan4<?php echo $id_prodi[$i]; ?>" value="<?php echo $mandiri_usulan[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="daya_tampung_apk_div<?php echo $id_prodi[$i]; ?>">
							<?php echo $daya_tampung_apk[$i]; ?>
						</div>
						<input style="display:none;" type="text" id="daya_tampung_apk<?php echo $id_prodi[$i]; ?>" name="daya_tampung_apk<?php echo $id_prodi[$i]; ?>" value="<?php echo $daya_tampung_apk[$i]; ?>" />
					</td>
					<td class="ui-widget-content" >
						<div id="btnEdit4_div<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>" name="btnEdit4<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>">
							Edit
						</div>
						<div style="display:none;" id="submit4<?php echo $id_prodi[$i]; ?>" name="submit4<?php echo $id_prodi[$i]; ?>">
							<input type="submit" name="btn_submit4" id="btn_submit4" value="Submit"  />
						</div>
					</td>
				</tr>
			</tbody>
			</form>
			<?php
						}
					}
				}
			?>
		</table>
	</div>
</div>

</div><!-- End demo -->

<?php
	}
?>