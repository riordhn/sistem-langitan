<?php
include 'config.php';
include 'proses/penghargaan.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$penghargaan = new penghargaan($db, $login->id_dosen);

$mode = get('mode','view');
if ($request_method == 'POST')
{
	if (post('mode') == 'add')
	{
		$penghargaan->insert($_POST['bidang'], $_POST['bentuk'], $_POST['pemberi'], $_POST['tahun'], $_POST['tingkat'], $_POST['negara']);
	}
	else if(post('mode')=='edit')
	{	
		$penghargaan->update($_POST['id_penghargaan'], $_POST['bidang'], $_POST['bentuk'], $_POST['pemberi'], $_POST['tahun'], $_POST['tingkat'], $_POST['negara']);
	}

}

if($mode=='detail'||$mode=='edit'||$mode=='upload'||$mode=='delete'){
	$smarty->assign('data_penghargaan',$penghargaan->get_penghargaan(get('id_penghargaan')));
}else{
	$smarty->assign('data_penghargaan', $penghargaan->load_penghargaan());
}


$smarty->assign('negara', $db->QueryToArray("select * from negara"));

$smarty->display("sample/biodata/{$mode}_penghargaan.tpl");

?>