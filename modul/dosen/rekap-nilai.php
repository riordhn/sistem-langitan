<?php

include 'config.php';
include 'proses/rekap_nilai.class.php';
include 'proses/penilaian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN)
{
	exit();
}

$penilaian = new penilaian($db, $login->id_dosen);
$id_fakultas = $user->ID_FAKULTAS;
if (isset($_GET))
{
	if (get('mode') == 'tampil')
	{
		if (isset($_POST))
		{
			if (post('mode') == 'publish')
			{
				$penilaian->update_status_publish($user->ID_PENGGUNA, get('semester'), $id_fakultas, post('status'), post('jenjang'));
			}
			// Khusus Mahasiswa D3 FEB
			else if (post('mode') == 'publish_d3')
			{
				$penilaian->update_status_publish_d3(get('semester'), $id_fakultas, post('status'), post('jenjang'));
			}
		}
		// Khusus Mahasiswa D3 FEB
		if ($user->ID_FAKULTAS == 4)
		{
			$smarty->assign('mhs_d3_feb', $penilaian->load_mahasiswa_mau_yudisium_d3($id_fakultas));
		}
		
		$smarty->assign('rekap_publish', $penilaian->load_publish(get('semester'), $id_fakultas));
		$smarty->assign('data_nilai_masuk', $penilaian->load_rekap_nilai($id_fakultas, 1, get('semester')));
		$smarty->assign('data_nilai_belum_masuk', $penilaian->load_rekap_nilai($id_fakultas, 0, get('semester')));
	}
	else if (get('mode') == 'detail')
	{
		$id_kelas_mk = get('kelas');
		$detail_mk = $penilaian->get_kelas_mk($id_kelas_mk);
		$smarty->assign('data_mahasiswa', $penilaian->load_data_mahasiswa($id_kelas_mk));
		$smarty->assign('count_data_komponen_mk', count($penilaian->load_komponen_mk($id_kelas_mk)));
		$smarty->assign('data_pengajar_mk', $penilaian->load_data_pengajar_kelas_mk($id_kelas_mk));
		$smarty->assign('data_kelas_mk_detail', $detail_mk);
		$smarty->assign('id_fakultas_kelas', $penilaian->get_id_fakultas_kelas_mk($id_kelas_mk));
		$smarty->assign('data_komponen_mk', $penilaian->load_komponen_mk($id_kelas_mk));
	}
}
$smarty->assign('id_fakultas', $id_fakultas);
$smarty->assign('data_semester', $db->QueryToArray("SELECT * FROM AUCC.SEMESTER WHERE id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI} and THN_AKADEMIK_SEMESTER  BETWEEN TO_CHAR(SYSDATE,'YYYY')-4 AND TO_CHAR(SYSDATE,'YYYY')+1 AND TIPE_SEMESTER='REG' ORDER BY TAHUN_AJARAN DESC,NM_SEMESTER DESC"));
//$smarty->assign('punya_akses', in_array($user->ID_JABATAN_PEGAWAI, $id_pimpinan_wadek1) ? true : false);
$smarty->assign('punya_akses', in_array($user->ID_JABATAN_PEGAWAI, $id_pimpinan_set3) ? true : false);
$smarty->assign('status_message', alert_error("Anda tidak punya hak akses...", 60));
$smarty->assign('id_jabatan_pegawai', $user->ID_JABATAN_PEGAWAI);
$smarty->display('sample/penilaian/rekap-nilai.tpl');
