<?php
include 'config.php';
include 'proses/komponen_nilai.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$komponen_nilai = new komponen_nilai($db, $login->id_dosen);

if (isset($_GET)) {
     if (get('mode') == 'edit') {
        $smarty->assign('data_komponen_mk_by_id', $komponen_nilai->load_komponen_mk_by_id(get('id_komponen')));
        $smarty->assign('id_komponen', get('id_komponen'));
    } else if (get('mode') == 'delete') {
        $smarty->assign('data_komponen_mk_by_id', $komponen_nilai->load_komponen_mk_by_id(get('id_komponen')));
        $smarty->assign('id_komponen', get('id_komponen'));
    } 
}

$smarty->display("sample/penilaian/komponen_nilai_sp.tpl");
?>