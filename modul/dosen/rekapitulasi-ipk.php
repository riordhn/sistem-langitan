<?php
include 'config.php';
include 'proses/rekapitulasi.class.php';
include '../keuangan/class/list_data.class.php';

// penambahan id sesuai ptnu dan parameter $id_pt pada construct class rekapitulasi
// Fikrie //
$id_pt = $id_pt_user;

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$rekap = new rekapitulasi($db, $login->id_dosen,$id_pt);
$list = new list_data($db);
if (isset($_GET)) {
	
    if (get('mode') == 'tampil') {
		
		$no = 1;
		foreach($rekap->load_data_ipk(get('thn_angkatan')) as $data){
			$tampil .= "<tr><td>".$no++."</td>
						<td>".$data['NM_FAKULTAS']."</td>
						
						<td>".$data['NM_PROGRAM_STUDI']."</td>
						<td>".$data['NM_JALUR']."</td>";
			/*foreach($rekap->load_jalurs1() as $data_jalur){
				if($data['ID_JALUR'] == $data_jalur['ID_JALUR']){*/
				$tampil .= "<td>".$data['RERATA_IPK']." %</td>
							<td>".$data['RERATA_SKS_NON_D']." %</td>
							<td>".$data['RERATA_IPS']." %</td>
							<td>".$data['RERATA_SKS']." %</td>";
				//}
			//}	
			$tampil .= "</tr>";
		}
		$smarty->assign('tampil', $tampil);
    }
}
$smarty->assign('thn_angkatan', $rekap->load_thn_angkatan());
$smarty->assign('jalurs1', $rekap->load_jalurs1());
$smarty->display('sample/rekapitulasi/rekapitulasi-ipk.tpl');
?>
