<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

		$prodi = array();
        $jenjang = array();
        $sk_terbit = array();
        $sk_penyelenggaraan = array();
        $sk_expired = array();
        $sk_akreditasi = array();
        
        $db->Query("select prospek.id_prodi_spesifikasi, ps.nm_program_studi, j.nm_jenjang, prospek.prodi_sk_nomor, prospek.prodi_skp_nomor, prospek.prodi_skp_expired, prospek.prodi_akred
                    from prodi_spesifikasi prospek
                    left join program_studi ps on ps.id_program_studi = prospek.id_program_studi
                    left join jenjang j on j.id_jenjang = ps.id_jenjang
                    where prospek.is_alih_jenis = 0 order by prospek.id_program_studi
            ");
        $i = 0;
        $jml_data = 0;
        while ($row = $db->FetchRow()){ 
            $prodi[$i] = $row[1];
            $jenjang[$i] = $row[2];
            $sk_terbit[$i] = $row[3];
            $sk_penyelenggaraan[$i] = $row[4];
            $sk_expired[$i] = $row[5];
            $sk_akreditasi[$i] = $row[6];
            $i++;
            $jml_data++;
        }
        
        $smarty->assign('prodi', $prodi);
        $smarty->assign('jenjang', $jenjang);
        $smarty->assign('sk_terbit', $sk_terbit);
        $smarty->assign('sk_penyelenggaraan', $sk_penyelenggaraan);
        $smarty->assign('sk_expired', $sk_expired);
        $smarty->assign('sk_akreditasi', $sk_akreditasi);
        $smarty->assign('jml_data', $jml_data);
        $smarty->display("sample/rekap/rekap-prps.tpl");
?>
