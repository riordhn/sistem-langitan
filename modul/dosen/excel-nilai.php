<?php
include 'config.php';
include 'proses/penilaian.class.php';
include 'proses/excel/OLEwriter.php';
include 'proses/excel/BIFFwriter.php';
include 'proses/excel/Worksheet.php';
include 'proses/excel/Workbook.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN)
{
	exit();
}

$penilaian = new penilaian($db, $login->id_dosen);

$data_mahasiswa = $penilaian->load_data_mahasiswa(get('id_kelas_mk'));
$data_kelas_mk = $penilaian->get_kelas_mk(get('id_kelas_mk'));
$data_komponen_mk = $penilaian->load_komponen_mk(get('id_kelas_mk'));
$jumlah_komponen = count($penilaian->load_komponen_mk(get('id_kelas_mk')));

/**
 * Create 'header' function. if called, this will tell the browser that the file returned is an excel document
 * @param type $filename
 */
function HeaderingExcel($filename)
{
	// Safe filename
	$filename = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $filename);
	$filename = mb_ereg_replace("([\.]{2,})", '', $filename);
	
	// limit long name
	$filename = substr($filename, 0, 250);
	
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=\"".$filename.'.xls"');
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
	header("Pragma: public");
}

// HTTP headers
HeaderingExcel(strtoupper(str_replace(' ', '_', $data_kelas_mk['NM_MATA_KULIAH'] . ' (' . $data_kelas_mk['NM_KELAS'].')'))); //call the function above
// Creating a workbook instance
$workbook = new Workbook("-");

// woksheet 1
$worksheet1 = & $workbook->add_worksheet('Nilai');

$worksheet1->set_zoom(100); //75% zoom
$worksheet1->set_portrait();
$worksheet1->set_paper(9); //set A4
$worksheet1->hide_gridlines();  //hide gridlines

$worksheet1->write_string(0, 0, "NIM");
$worksheet1->write_string(0, 1, "NAMA");
for ($i = 2; $i < $jumlah_komponen + 2; $i++)
{
	$nama_komponen = $data_komponen_mk[$i - 2]['NM_KOMPONEN_MK'] . "({$data_komponen_mk[$i - 2]['PERSENTASE_KOMPONEN_MK']} %)";
	$worksheet1->write_string(0, $i, $nama_komponen);
}
$index_baris = 1;
foreach ($data_mahasiswa as $data)
{
	$index_kolom = 0;
	$worksheet1->write_string($index_baris, $index_kolom, $data['nim']);
	$index_kolom++;
	$worksheet1->write_string($index_baris, $index_kolom, $data['nama']);
	$index_kolom++;
	foreach ($data['data_nilai'] as $data_nilai)
	{
		$worksheet1->write_string($index_baris, $index_kolom, $data_nilai['BESAR_NILAI_MK']);
		$index_kolom++;
	}
	$index_baris++;
}
$workbook->close();
