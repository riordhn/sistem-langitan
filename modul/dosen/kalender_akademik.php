<?php
include 'config.php';
include 'proses/kalender_akademik.class.php';

$id_pt = $id_pt_user;

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$kalender_akademik = new kalender_akademik($db,$login->id_fakultas,$id_pt);
$smarty->assign('fakultas',$login->fakultas);
$smarty->assign('data_kalender_fakultas',$kalender_akademik->load_kalender_fakultas());
$smarty->assign('data_kalender_universitas',$kalender_akademik->load_kalender_universitas());
$smarty->display('display-jadwal/kalender_akademik.tpl');
?>