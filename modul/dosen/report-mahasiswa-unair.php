<?php
include 'config.php';
include 'proses/report_mahasiswa.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$rm = new report_mahasiswa($db);

if (get('mode') == 'list_mhs') {
    $smarty->assign('data_mahasiswa',$rm->get_mahasiswa('', get('prodi'), get('angkatan'), ''));
}
$smarty->assign('data_fakultas_prodi', $rm->get_fakultas_prodi());
$smarty->assign('count_data_fakultas_prodi', count($rm->get_fakultas_prodi()));
$smarty->assign('data_jumlah_mahasiswa', $rm->get_jumlah_mahasiswa_all());
$smarty->assign('count_data_jumlah_mahasiswa', count($rm->get_jumlah_mahasiswa_all()));
$smarty->assign('data_tahun_akademik', $rm->get_tahun_akademik());
$smarty->assign('count_data_tahun_akademik', count($rm->get_tahun_akademik()));

$smarty->display("sample/pimpinan/report-mahasiswa-unair.tpl");
?>
