<?php
include 'config.php';
include 'proses/Penelitian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$mode = get('mode', 'view');

$Penelitian = new Penelitian($db);
    
if ($request_method == 'POST')
{
    if (post('mode') == 'approve')
    {
        $result = $Penelitian->ApproveDepartemen(post('id_penelitian'), $user->ID_PENGGUNA);
        
        echo $result ? "1" : "0";
        
        exit();
    }
    
    if (post('mode') == 'reject')
    {
        $result = $Penelitian->RejectDepartemen(post('id_penelitian'));
        
        echo $result ? "1" : "0";
        
        exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $smarty->assign('penelitian_set', $Penelitian->GetListPenelitianByDepartemen($user->ID_DEPARTEMEN_KADEP));
    }
    
    if ($mode == 'preview')
    {
        $smarty->assign('p', $Penelitian->GetData(get('id_penelitian')));
        $smarty->assign('golongan_set', $Penelitian->GetListGolongan());
    }
}

$smarty->display("sample/penelitian/approval/{$mode}.tpl");
?>
