<?php

/*echo "Dalam Perbaikan..";
exit();
*/
include 'config.php';
include 'proses/penilaian.class.php';
include 'proses/komponen_nilai.class.php';

$id_pt = $id_pt_user;

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$mode = get('mode', 'view');
/*if ($user->ID_FAKULTAS == 8)
    $smarty->assign('page', '../komponen_nilai.php');
else
    $smarty->assign('page', 'komponen_nilai.php');*/
$komponen_nilai = new komponen_nilai($db, $login->id_dosen);
$penilaian = new penilaian($db, $login->id_dosen);
$data_kelas_mk = $penilaian->load_kelas_mata_kuliah_kkn();
if ($request_method == 'POST') {
    if (post('mode') == 'load_table') {
        if (post('id_kelas') != '') {
            $smarty->assign('data_komponen_mk', $komponen_nilai->load_komponen_mk(post('id_kelas')));
            $smarty->assign('data_kelas_mk_detail', $penilaian->get_kelas_mk(post('id_kelas')));
            $smarty->assign('id_kelas', post('id_kelas'));
        }
    } else if (post('mode') == 'tambah') {
        $nama = post('nama') != '' ? post('nama') : post('nama_utama');
        $komponen_nilai->insert_komponen($nama, post('persentase'), post('urutan'), post('id_kelas'));
        $smarty->assign('id_kelas', post('id_kelas'));
    } else if (post('mode') == 'edit') {
        $komponen_nilai->update_komponen(post('id_komponen'), post('urutan'), post('nama'), post('persentase'));
        $smarty->assign('id_kelas', post('id_kelas'));
    } else if (post('mode') == 'delete') {
        $komponen_nilai->delete_komponen(post('id_komponen'));
        $smarty->assign('id_kelas', post('id_kelas'));
    } else if (post('mode') == 'copy') {
        $urutan_terakhir = count($komponen_nilai->load_komponen_mk(post('id_kelas_copy'))) + 1;
        foreach ($komponen_nilai->load_komponen_mk(post('id_kelas_copy')) as $data) {
            $komponen_nilai->insert_komponen($data['NM_KOMPONEN_MK'], $data['PERSENTASE_KOMPONEN_MK'], $urutan_terakhir, post('id_kelas'));
            $urutan_terakhir++;
        }
        $smarty->assign('id_kelas', post('id_kelas'));
    }

    if (post('id_kelas') != '') {
        $smarty->assign('jumlah_kelas', $komponen_nilai->cek_kelas_pararel(post('id_kelas')));
        //$smarty->assign('pembimbing_status', $penilaian->get_pembimbing_status(post('id_kelas')));
        $smarty->assign('data_komponen_mk', $komponen_nilai->load_komponen_mk(post('id_kelas')));
        $smarty->assign('data_kelas_mk_detail', $penilaian->get_kelas_mk(post('id_kelas')));
    }
    $data_kelas_mk = $penilaian->load_kelas_mata_kuliah_kkn();
} else if ($request_method == 'GET') {
    if ($mode == 'tambah') {
        $smarty->assign('id_kelas', get('id_kelas'));
    } else if ($mode == 'edit') {
        $smarty->assign('data_komponen_mk_by_id', $komponen_nilai->load_komponen_mk_by_id(get('id_komponen')));
        $smarty->assign('id_komponen', get('id_komponen'));
        $smarty->assign('id_kelas', get('id_kelas'));
    } else if ($mode == 'delete') {
        $smarty->assign('data_komponen_mk_by_id', $komponen_nilai->load_komponen_mk_by_id(get('id_komponen')));
        $smarty->assign('id_komponen', get('id_komponen'));
        $smarty->assign('id_kelas', get('id_kelas'));
    } else if ($mode == 'copy') {
        $smarty->assign('data_kelas_pararel', $komponen_nilai->load_kelas_paralel(get('id_kelas')));
        $smarty->assign('id_kelas', get('id_kelas'));
    }
}

$smarty->assign('semester_aktif', $penilaian->get_semester_aktif());

$smarty->assign('data_kelas_mk', $data_kelas_mk);
$smarty->display("display-penilaian-kkn/{$mode}_komponen_nilai_kkn.tpl");
?>