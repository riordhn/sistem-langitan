<div class="center_title_bar">Data-Data Rekapitulasi</div>

<style>
	.my-list li a:hover {
		text-decoration: underline;
	}
	.my-list li a, .my-list li {
		font-size: 14px;
	}
	.my-list li {
		margin-bottom: 3px;
	}
</style>

<script type="text/javascript">
	$('.left_content').show('fast');
	$('#content').css('width','780px');
	
	function OpenRekap()
	{
		window.open('/modul/rekapitulasi/','mywindow','toolbar=yes');
	}
</script>

<ul class="my-list">
	<li><a href="rekap-mhsaktif.php">Mahasiswa Aktif</a></li>
	<li><a href="rekap-status-akademik-s1.php">Profil Mahasiswa S1 Berdasarkan Status Akademik</a></li>
	<li><a href="rekap-status-akademik-s2.php">Profil Mahasiswa S2 Berdasarkan Status Akademik</a></li>
	<li><a href="rekap-status-akademik-s3.php">Profil Mahasiswa S3 Berdasarkan Status Akademik</a></li>
	<li><a href="rekap-status-akademik-d3.php">Profil Mahasiswa D3 Berdasarkan Status Akademik</a></li>
	<li><a href="rekap-ipk-s1.php">Profil Mahasiswa S1 berdasarkan IPK</a></li>
	<li><a href="rekap-ipk-s2.php">Profil Mahasiswa S2 berdasarkan IPK</a></li>
	<li><a href="rekap-ipk-s3.php">Profil Mahasiswa S3 berdasarkan IPK</a></li>
	<li><a href="rekap-ipk-d3.php">Profil Mahasiswa D3 berdasarkan IPK</a></li>
	<li><a href="rekap-elpt-s1.php">Nilai ELPT Mahasiswa Baru S1</a></li>
	<li><a href="rekap-elpt-d3.php">Nilai ELPT Mahasiswa Baru D3</a></li>
	<li><a href="rekap-dist-s1.php">Distribusi Nilai Mahasiswa S1</a></li>
	<li><a href="rekap-dist-d3.php">Distribusi Nilai Mahasiswa D3</a></li>
	<li><a href="rekap-dist-s2.php">Distribusi Nilai Mahasiswa S2</a></li>
	<li><a href="rekap-dist-s3.php">Distribusi Nilai Mahasiswa S3</a></li>
</ul>