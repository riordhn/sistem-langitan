<?php

include 'config.php';
include 'proses/reader.php';
include 'proses/penilaian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}

$penilaian = new penilaian($db, $user->ID_DOSEN);
$data = new Spreadsheet_Excel_Reader();
$status_message = '';
if (isset($_POST['excel'])) {
    $id_kelas_mk = get('id_kelas_mk');
    // Cek User Dosen Sebagai Pengampu selain itu di block
    if ($penilaian->cek_penilaian_dosen_login($id_kelas_mk) == 0) {
        die(alert_error("Access denied.", 90));
    }
    $filename = $id_kelas_mk . '_' . $user->ID_DOSEN . '_' . date('dmy_h:i:s') . '_' . $_FILES['file']['name'];
    $filename = str_replace(' ', '', $filename);
    if ($_FILES["file"]["error"] > 0) {
        $status_message .= "Error : " . $_FILES["file"]["error"] . "<br />";
        $status_upload = '0';
    } else {
        $status_message .= "Nama File : " . $filename . "<br />";
        $status_message .= "Type : " . $_FILES["file"]["type"] . "<br />";
        $status_message .= "Size : " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";

        if (file_exists("file/excel_penilaian/" . $filename)) {
            $status_message .= 'Keterangan : ' . $filename . " Sudah Ada ";
            $status_upload = '0';
        } elseif($penilaian->get_komponen_mk($id_kelas_mk) == 0){
            $status_upload = '2';
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], "file/excel_penilaian/" . $filename);
            chmod("file/excel_penilaian/" . $filename, 0777);
            $status_upload = '1';
        }
    }

    $jumlah_insert = 0;
    if ($status_upload == '1') {
        $data->setOutputEncoding('CP1251');
        $data->read("file/excel_penilaian/{$filename}");
        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            $id_nilai_mk = $penilaian->get_id_nilai_mk_uas($data->sheets[0]['cells'][$i][1], $id_kelas_mk);
            //$status_cekal = $penilaian->cek_cekal_mhs($data->sheets[0]['cells'][$i][1], $id_kelas_mk);

                // Replace "," menjadi  "."
                $nilai = str_replace(',', '.', $data->sheets[0]['cells'][$i][3]);
                if($nilai != ''){
                    $penilaian->update_nilai_excel($nilai, $id_nilai_mk[$index_id_nilai]['ID_NILAI_MK']);
                }

            $jumlah_insert++;
        }
    }
    $penilaian->proses_nilai_akhir($id_kelas_mk);
    $smarty->assign('jumlah_insert', $jumlah_insert);
    $smarty->assign('status_upload', $status_upload);
    $smarty->assign('status_message', $status_message);
}


$smarty->display('display-penilaian/upload-nilai-uas.tpl');
?>
