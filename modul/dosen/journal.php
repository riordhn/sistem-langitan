<?php
include 'config.php';
include 'proses/journal.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

echo $user->USERNAME;

$Journal = new Journal($db);

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
        
        $result = $Journal->InsertJournal($user->ID_DOSEN, $_POST);
        
        if ($result)
            $smarty->assign('result', "Journal berhasil ditambahkan");
        else
        {
            $smarty->assign('result', "Journal gagal ditambahkan");
            print_r(error_get_last());
        }
        
        $mode = 'add-result';
    }
    
    if (post('mode') == 'edit')
    {
        $id_journal = post('id_journal', 0);
        
        $result = $Journal->UpdateJournal($user->ID_DOSEN, $id_journal, $_POST);
        
        if ($result)
            $smarty->assign('result', "Journal berhasil diperbarui");
        else
        {
            $smarty->assign('result', "Journal gagal diperbarui");
            print_r(error_get_last());
        }
        
        $mode = 'edit-result';
    }
    
    goto display;
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        // $smarty->assign('journal_set', $Journal->SelectAll($user->ID_DOSEN));
		
		// Koneksi ke db jurnal
		$db_amerta = new mysqli('10.0.110.12', 'jurnaltampil', 'jurn4lu4', 'jurnal_ua');
		if ($db_amerta->connect_error)
		{
			echo 'Connect Error (' . $db_amerta->connect_errno . ') '. $db_amerta->connect_error;
			exit();
		}
		
		$result = $db_amerta->query("
			select j.row_id as id_jurnal, j.title, j.id_media, m.namamedia, b.row_id as id_bidang,
				concat('http://journal.unair.ac.id/article_', j.row_id, '_media', j.id_media, '_category', b.row_id ,'.html') as link
			from jurnal j
			join media m on m.row_id = j.id_media
			join bidang b on b.row_id = m.id_bidang
			join penulis p on p.id_artikel = j.row_id
			where p.nip = '{$user->USERNAME}'");
			
		$jurnal_set = array();
		
		if ($result)
		{
			while ($row = $result->fetch_assoc())
				array_push($jurnal_set, $row);
			
			$result->free();
		}
		
		$db_amerta->close();
		
		$smarty->assign('jurnal_set', $jurnal_set);
    }
    
    if ($mode == 'add')
    {
        $smarty->assign('journal_media_set', $Journal->GetListJournalMedia());
    }
    
    if ($mode == 'edit')
    {
        $id_journal = get('id_journal', 0);
        
        $smarty->assign('journal', $Journal->Single($user->ID_DOSEN, $id_journal));
        $smarty->assign('journal_media_set', $Journal->GetListJournalMedia());
    }
}

display:
    $smarty->display("sample/biodata/journal/{$mode}.tpl");
?>