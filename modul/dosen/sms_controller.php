<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }
	
//if($user->IsLogged()){
	
	include 'sms_model.php';
	class SmsController{
		
		public $page = "";
		public $prev_page = "sms_";
		public $model = array();
		public $pageURL = "";
		public $model_pma = array();
		public $model_perwalian = array();


		function __construct(){
		}

		public function curPageURL() {
			 $this->pageURL = 'http';
			 if ($_SERVER["HTTPS"] == "on") {$this->pageURL .= "s";}
				$this->pageURL .= "://";
			 if ($_SERVER["SERVER_PORT"] != "80") {
				$this->pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
			 } else {
				$this->pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
			 }
			 return $this->pageURL;
		}
		
		public function requestPage($view){

			

			if($view=='index'){
				$this->page = $this->prev_page . $view . '.tpl';
			}

			if($view=='peserta_ma'){
				$this->page = $this->prev_page . $view . '.tpl';
			}

			else if($view=='mhs'){
				$this->model = new SmsModel();
				$this->model->requestData($view);
				$this->page = $this->prev_page . $view . '.tpl';
			}
			
			else if($view=='perwalian'){
				$this->model = new SmsModel();
				$this->model->requestData($view);
				$this->page = $this->prev_page . $view . '.tpl';
			}
		}

		public function sendMessage($mobile, $message){
				$this->model = new SmsModel();


				$view ='mobile-dosen';
				$this->model->requestData($view);

				include "../../smsgw/konek-gammu.php";
				$tujuan  = $mobile; 

				$mobile_dosen = $this->model->mobile_dosen;
				
				$pesan = $message;

				$TextDecoded="UACC SMS GATEWAY\n\n" . trim($pesan) . "\n\n" . 'Pengirim : ' . $this->model->nama_dosen;
				
				$DestinationNumber= $tujuan;
				$query = "insert into outbox (DestinationNumber, TextDecoded, CreatorID) values ('$DestinationNumber', '$TextDecoded', 'UACC')";
				mysql_query($query) or die(mysql_error());

				$DestinationNumberDosen= $mobile_dosen;
				$query2 = "insert into outbox (DestinationNumber, TextDecoded, CreatorID) values ('$DestinationNumberDosen', '$TextDecoded', 'UACC')";
				mysql_query($query2) or die(mysql_error());
				//echo 'SMS GATEWAY UNIVERSITAS AIRLANGGA CYBERCAMPUS RESPONSE : \n\n';
				//echo "Pesan anda yang terikirim adalah : " . $pesan;
		}

		public function sendMessageToPMA($message, $id_kelas_ma){
				$this->model = new SmsModel();
				$this->model_pma = new SmsModel();
				
				$this->model->requestData('mobile-dosen');
				
				$this->model_pma->requestDataPma($id_kelas_ma);

				include "../../smsgw/konek-gammu.php";
				$tujuan  = $mobile; 

				$mobile_dosen = $this->model->mobile_dosen;
				
				$pesan = $message;
				
				// Kirim Ke Peserta MA
				for($i=0;$i<$this->model_pma->jml_pma;$i++){

					$TextDecoded="UACC SMS GATEWAY\n\n" . trim($pesan) . "\n\n" . "Pengirim : " . $this->model->nama_dosen;
					
					$DestinationNumber = $this->model_pma->mobile_pma[$i];
					$query = "insert into outbox (DestinationNumber, TextDecoded, CreatorID) values ('$DestinationNumber', '$TextDecoded', 'UACC')";
					mysql_query($query) or die(mysql_error());

				}
				
				// Kirim SMS ke Dosen Pengirim
				$DestinationNumberDosen= $mobile_dosen;
				$query2 = "insert into outbox (DestinationNumber, TextDecoded, CreatorID) values ('$DestinationNumberDosen', '$TextDecoded', 'UACC')";
				mysql_query($query2) or die(mysql_error());
				
				echo "Pesan telah berhasil dikirim ke nomor berikut : " . "<br />";
				for($i=0;$i<$this->model_pma->jml_pma;$i++){
					
					echo $i+1 . ' - ' . $this->model_pma->nama_pma[$i] . ' ( ' . $this->model_pma->mobile_pma[$i] . ' ) ';
					echo "<br />";

				}

		}
		
		public function sendMessageToPerwalian($message){
				$this->model = new SmsModel();
				$this->model_perwalian = new SmsModel();
				
				$this->model->requestData('mobile-dosen');
				
				$this->model_perwalian->requestDataPerwalian();

				include "../../smsgw/konek-gammu.php";
				$tujuan  = $mobile; 

				$mobile_dosen = $this->model->mobile_dosen;
				
				$pesan = $message;
				
				// Kirim Ke Peserta MA
				for($i=0;$i<$this->model_perwalian->jml_perwalian;$i++){

					$TextDecoded="UACC SMS GATEWAY\n\n" . trim($pesan) . "\n\n" . "Pengirim : " . $this->model->nama_dosen;
					
					$DestinationNumber = $this->model_perwalian->mobile_perwalian[$i];
					$query = "insert into outbox (DestinationNumber, TextDecoded, CreatorID) values ('$DestinationNumber', '$TextDecoded', 'UACC')";
					mysql_query($query) or die(mysql_error());

				}
				
				// Kirim SMS ke Dosen Pengirim
				$DestinationNumberDosen= $mobile_dosen;
				$query2 = "insert into outbox (DestinationNumber, TextDecoded, CreatorID) values ('$DestinationNumberDosen', '$TextDecoded', 'UACC')";
				mysql_query($query2) or die(mysql_error());
				
				echo "Pesan telah berhasil dikirim ke nomor berikut : " . "<br />";
				for($i=0;$i<$this->model_perwalian->jml_perwalian;$i++){
					
					echo $i+1 . ' - ' . $this->model_perwalian->nama_perwalian[$i] . ' ( ' . $this->model_perwalian->mobile_perwalian[$i] . ' ) ';
					echo "<br />";

				}

		}
		
	}
//}