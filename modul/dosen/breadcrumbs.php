<?php

include 'config.php';
include 'proses/pesan.class.php';

$pesan = new pesan($db, $user->ID_DOSEN, $user->ID_PENGGUNA);

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}

$location = explode("-", $_GET['location']);


foreach ($user->MODULs as $modul) {
    if ($modul['NM_MODUL'] == $location[0]) {
        $smarty->assign('modul', $modul);

        foreach ($modul['MENUs'] as $menu) {
			if (isset($location[1])) {
				if ($menu['NM_MENU'] == $location[1]) {
					$smarty->assign('menu', $menu);
				}
			}
			else {
				$smarty->assign('menu', $modul['MENUs'][0]);
			}
        }
    }
}

// NOTIFIKASI
$total_notifikasi = 0;
$jumlah_notifikasi_pesan = 0;
$jumlah_notifikasi_komplain = 0;

// NOTIFIKASI PESAN
$jumlah_notifikasi_pesan = count($pesan->load_unread_inbox());
// NOTIFIKASI KOMPLAIN
$unit_kerja_pengguna = $db->QuerySingle("SELECT ID_UNIT_KERJA FROM AUCC.UNIT_KERJA WHERE TYPE_UNIT_KERJA='FAKULTAS' AND ID_FAKULTAS IN (SELECT ID_FAKULTAS FROM AUCC.PROGRAM_STUDI WHERE ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM AUCC.DOSEN WHERE ID_PENGGUNA='{$user->ID_PENGGUNA}'))");
$q_cek_pj_komplain = "
    SELECT COUNT(*) FROM KOMPLAIN_PJ WHERE ID_PENGGUNA=" . $user->ID_PENGGUNA . "AND ID_UNIT_KERJA='{$unit_kerja_pengguna}'";
$cek_pj_komplain = $db->QuerySingle($q_cek_pj_komplain);
if ($cek_pj_komplain > 0) {
    $q_data_komplain = "
    SELECT P.NM_PENGGUNA,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA,K.*,TO_CHAR(K.WAKTU_KOMPLAIN,'DD-MM-YYYY HH24:MI:SS') WAKTU
    FROM AUCC.KOMPLAIN K
    JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=K.ID_UNIT_TERTUJU
    JOIN AUCC.PENGGUNA P ON K.ID_PELAPOR=P.ID_PENGGUNA
    WHERE ({$unit_kerja_pengguna} IN (SELECT ID_UNIT_KERJA FROM KOMPLAIN_SHARE WHERE ID_KOMPLAIN=K.ID_KOMPLAIN) OR K.ID_UNIT_TERTUJU={$unit_kerja_pengguna}) AND K.STATUS_KOMPLAIN='1'
    ORDER BY K.WAKTU_KOMPLAIN DESC
    ";

    $data_komplain = $db->QueryToArray($q_data_komplain);
    $jumlah_notifikasi_komplain = count($data_komplain);
}


$total_notifikasi +=$jumlah_notifikasi_komplain + $jumlah_notifikasi_pesan;

$smarty->assign('total_notifikasi', $total_notifikasi);
$smarty->assign('user_login', $user->NM_PENGGUNA);
$smarty->display('breadcrumbs.tpl');
?>