<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

	
	$id_fakultas = array();
	$nm_fakultas = array();
	$jml_fakultas = 0;
	
	$id_prodi = array();
	$nm_prodi = array();
	$nm_jenjang = array();

	$sop = array();
	$sp3 = array();
	$sp3a = array();
	$sp3b = array();
	$sp3c = array();
	$sop_usulan = array();
	$sp3_usulan = array();
	$sp3a_usulan = array();
	$sp3b_usulan = array();
	$sp3c_usulan = array();
	$matrikulasi = array();
	$matrikulasi_usulan = array();

	
	$id_semester = 0;
	$thn_akademik_semester = "";
	$is_alih_jenis = array();
	$is_mandiri = array();
	$kelompok_biaya = array();
	
	$jml_prodi = 0;

	$nm_fakultas_get = "";

	$nm_program_studi = "";
	
		$db->Query("select id_semester, thn_akademik_semester from semester where id_semester = 5");

								$i = 0;
								while ($row = $db->FetchRow()){ 
									$id_semester = $row[0];
									$thn_akademik_semester = $row[1];
									$i++;
								}
	
		$get_id_fakultas = $user->ID_FAKULTAS_JABATAN;
		$db->Query("select ps.id_program_studi, ps.nm_program_studi, j.nm_jenjang, pbiaya.sop, pbiaya.sp3, pbiaya.sop_usulan, pbiaya.sp3_usulan, pbiaya.matrikulasi, pbiaya.matrikulasi_usulan, pbiaya.is_alih_jenis, pbiaya.sp3a, pbiaya.sp3b, pbiaya.sp3a_usulan, pbiaya.sp3b_usulan, pbiaya.sp3c, pbiaya.sp3c_usulan, pbiaya.is_mandiri, pbiaya.kelompok_biaya 
					from prodi_biaya pbiaya
					left join program_studi ps on ps.id_program_studi = pbiaya.id_program_studi
					left join fakultas f on f.id_fakultas = ps.id_fakultas
					left join jenjang j on j.id_jenjang = ps.id_jenjang
					where ps.id_fakultas = '$get_id_fakultas' 
					order by pbiaya.id_program_studi");

								$i = 0;
								while ($row = $db->FetchRow()){ 
									$id_prodi[$i] = $row[0];
									$nm_prodi[$i] = $row[1];
									$nm_jenjang[$i] = $row[2];
									$sop[$i] = $row[3];
									$sp3[$i] = $row[4];
									$sop_usulan[$i] = $row[5];
									$sp3_usulan[$i] = $row[6];
									$matrikulasi[$i] = $row[7];
									$matrikulasi_usulan[$i] = $row[8];
									$is_alih_jenis[$i] = $row[9];
									$sp3a[$i] = $row[10];
									$sp3b[$i] = $row[11];
									$sp3a_usulan[$i] = $row[12];
									$sp3b_usulan[$i] = $row[13];
									$sp3c[$i] = $row[14];
									$sp3c_usulan[$i] = $row[15];
									$is_mandiri[$i] = $row[16];
									$kelompok_biaya[$i] = $row[17];
									$jml_prodi++;
									$i++;
								}
		
?>

 
	<script>
	$(function() {
		$( "#radio_PPCMB_1" ).buttonset();
		$( "#radio_PPCMB_3" ).buttonset();
		$( "#submit" ).buttonset();
		$( "#btn_ubah" ).buttonset();
		
		<?php
			for($i=0;$i<$jml_prodi;$i++){
				if($is_alih_jenis[$i]==0){
					if($is_mandiri[$i]==0){
		?>
		$("#submit<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>").buttonset();
		$("#btnEdit_div<?php echo str_replace(" ", "", trim($id_prodi[$i] . $kelompok_biaya[$i])); ?>").button({
            icons: {
                primary: "ui-icon-pencil",
            },
			text: false
        });
		
		$('#btnEdit_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').click(function(){
			$('#sopS1_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
			$('#sop_usulan_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').hide();
			$('#sp3_usulan_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').hide();
			$('#sp3a_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
			$('#sp3b_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
			$('#sp3c_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
			$('#matrikulasi_usulan_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').hide();
			
			$('#sopS1_usulan<?php echo $id_prodi[$i]; ?>').show();
			$('#sop_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').show();
			$('#sp3_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').show();
			$('#sp3a_usulan<?php echo $id_prodi[$i]; ?>').show();
			$('#sp3b_usulan<?php echo $id_prodi[$i]; ?>').show();
			$('#sp3c_usulan<?php echo $id_prodi[$i]; ?>').show();
			$('#matrikulasi_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').show();
			
			$('#btnEdit_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').hide();
			$('#submit<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').show();
		});
		
		$('#myForm<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').submit(function() {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
				

				
					$('#sop_div<?php echo $id_prodi[$i]; ?>').show();
					$('#sp3_div<?php echo $id_prodi[$i]; ?>').show();

					$('#sop_usulan_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').show();
					$('#sp3_usulan_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').show();
					$('#matrikulasi_usulan_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').show();
					
					$('#sop<?php echo $id_prodi[$i]; ?>').hide();
					$('#sp3<?php echo $id_prodi[$i]; ?>').hide();

					$('#sop_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').hide();
					$('#sp3_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').hide();
					$('#matrikulasi_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').hide();
					
					
					$('#submit<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').hide();
					$('#btnEdit_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').show();
					
					var sop_div = document.getElementById('sop_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>');
					var sp3_div = document.getElementById('sp3_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>');
					
					var sop_usulan_div = document.getElementById('sop_usulan_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>');
					var sp3_usulan_div = document.getElementById('sp3_usulan_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>');
					
					var matrikulasi_usulan_div = document.getElementById('matrikulasi_usulan_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>');
					
					sop_div.innerHTML = document.getElementById('sop<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').value;
					sp3_div.innerHTML = document.getElementById('sp3<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').value;
					
					sop_usulan_div.innerHTML = document.getElementById('sop_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').value;
					sp3_usulan_div.innerHTML = document.getElementById('sp3_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').value;
					matrikulasi_usulan_div.innerHTML = document.getElementById('matrikulasi_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>').value;

				}
			})
			return false;
		});
		
		
		
		$('#myFormS1<?php echo $id_prodi[$i]; ?>').submit(function() {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
				
					$('#sopS1_usulan_div<?php echo $id_prodi[$i]; ?>').show();
					$('#sp3a_usulan_div<?php echo $id_prodi[$i]; ?>').show();
					$('#sp3b_usulan_div<?php echo $id_prodi[$i]; ?>').show();
					$('#sp3c_usulan_div<?php echo $id_prodi[$i]; ?>').show();
					
					$('#sopS1_usulan<?php echo $id_prodi[$i]; ?>').hide();
					$('#sp3a_usulan<?php echo $id_prodi[$i]; ?>').hide();
					$('#sp3b_usulan<?php echo $id_prodi[$i]; ?>').hide();
					$('#sp3c_usulan<?php echo $id_prodi[$i]; ?>').hide();
					
					
					$('#submit<?php echo $id_prodi[$i]; ?>').hide();
					$('#btnEdit_div<?php echo $id_prodi[$i]; ?>').show();
					
					var sopS1_usulan_div = document.getElementById('sopS1_usulan_div<?php echo $id_prodi[$i]; ?>');
					var sp3a_usulan_div = document.getElementById('sp3a_usulan_div<?php echo $id_prodi[$i]; ?>');
					var sp3b_usulan_div = document.getElementById('sp3b_usulan_div<?php echo $id_prodi[$i]; ?>');
					var sp3c_usulan_div = document.getElementById('sp3c_usulan_div<?php echo $id_prodi[$i]; ?>');
					
					sopS1_usulan_div.innerHTML = document.getElementById('sopS1_usulan<?php echo $id_prodi[$i]; ?>').value;
					sp3a_usulan_div.innerHTML = document.getElementById('sp3a_usulan<?php echo $id_prodi[$i]; ?>').value;
					sp3b_usulan_div.innerHTML = document.getElementById('sp3b_usulan<?php echo $id_prodi[$i]; ?>').value;
					sp3c_usulan_div.innerHTML = document.getElementById('sp3c_usulan<?php echo $id_prodi[$i]; ?>').value;

				}
			})
			return false;
		});
		
		<?php
					}
				}
			}
		?>
		
		<?php
			for($i=0;$i<$jml_prodi;$i++){
				if($is_alih_jenis[$i]==0){
					if($is_mandiri[$i]==1){
						if($nm_jenjang[$i]=='S1'){
		?>
		$("#submit_mandiri<?php echo $id_prodi[$i]; ?>").buttonset();
		$("#btnEdit_mandiri_div<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>").button({
            icons: {
                primary: "ui-icon-pencil",
            },
			text: false
        });
		
		$('#btnEdit_mandiri_div<?php echo $id_prodi[$i]; ?>').click(function(){

			$('#sop_mandiri_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
			$('#sp3_mandiri_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
			
			$('#sop_mandiri_usulan<?php echo $id_prodi[$i]; ?>').show();
			$('#sp3_mandiri_usulan<?php echo $id_prodi[$i]; ?>').show();
			
			$('#btnEdit_mandiri_div<?php echo $id_prodi[$i]; ?>').hide();
			$('#submit_mandiri<?php echo $id_prodi[$i]; ?>').show();

		});
		
		$('#myFormMandiri<?php echo $id_prodi[$i]; ?>').submit(function() {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
				
					$('#sop_mandiri_usulan_div<?php echo $id_prodi[$i]; ?>').show();
					$('#sp3_mandiri_usulan_div<?php echo $id_prodi[$i]; ?>').show();
					
					$('#sop_mandiri<?php echo $id_prodi[$i]; ?>').hide();
					$('#sp3_mandiri<?php echo $id_prodi[$i]; ?>').hide();
					$('#sop_mandiri_usulan<?php echo $id_prodi[$i]; ?>').hide();
					$('#sp3_mandiri_usulan<?php echo $id_prodi[$i]; ?>').hide();
					
					
					$('#submit_mandiri<?php echo $id_prodi[$i]; ?>').hide();
					$('#btnEdit_mandiri_div<?php echo $id_prodi[$i]; ?>').show();
					
					var sop_mandiri_div = document.getElementById('sop_mandiri_div<?php echo $id_prodi[$i]; ?>');
					var sp3_mandiri_div = document.getElementById('sp3_mandiri_div<?php echo $id_prodi[$i]; ?>');
					var sop_mandiri_usulan_div = document.getElementById('sop_mandiri_usulan_div<?php echo $id_prodi[$i]; ?>');
					var sp3_mandiri_usulan_div = document.getElementById('sp3_mandiri_usulan_div<?php echo $id_prodi[$i]; ?>');

					
					sop_mandiri_div.innerHTML = document.getElementById('sop_mandiri<?php echo $id_prodi[$i]; ?>').value;
					sp3_mandiri_div.innerHTML = document.getElementById('sp3_mandiri<?php echo $id_prodi[$i]; ?>').value;
					sop_mandiri_usulan_div.innerHTML = document.getElementById('sop_mandiri_usulan<?php echo $id_prodi[$i]; ?>').value;
					sp3_mandiri_usulan_div.innerHTML = document.getElementById('sp3_mandiri_usulan<?php echo $id_prodi[$i]; ?>').value;
					
				}
			})
			return false;
		});
		
		<?php
						}
					}
				}
			}
		?>
		
		<?php
			for($i=0;$i<$jml_prodi;$i++){
				if($is_alih_jenis[$i]==1){
		?>
				$("#submit_alih_jenis_div<?php echo $id_prodi[$i]; ?>").buttonset();
				$("#btnEdit_alih_jenis_div<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>").button({
					icons: {
						primary: "ui-icon-pencil",
					},
					text: false
				});
				
				$('#btnEdit_alih_jenis_div<?php echo $id_prodi[$i]; ?>').click(function(){
					$('#sop_alih_jenis_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
					//$('#sop_usulan2_div<?php echo $id_prodi[$i]; ?>').hide();
					$('#sp3_alih_jenis_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
					//$('#sp3a_usulan2_div<?php echo $id_prodi[$i]; ?>').hide();
					//$('#sp3b_usulan2_div<?php echo $id_prodi[$i]; ?>').hide();
					//$('#sp3c_usulan2_div<?php echo $id_prodi[$i]; ?>').hide();
					$('#matrikulasi_alih_jenis_usulan_div<?php echo $id_prodi[$i]; ?>').hide();
					
					$('#sop_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>').show();
					//$('#sop_usulan2<?php echo $id_prodi[$i]; ?>').show();
					$('#sp3_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>').show();
					//$('#sp3a_usulan2<?php echo $id_prodi[$i]; ?>').show();
					//$('#sp3b_usulan2<?php echo $id_prodi[$i]; ?>').show();
					//$('#sp3c_usulan2<?php echo $id_prodi[$i]; ?>').show();
					$('#matrikulasi_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>').show();
					
					$('#btnEdit_alih_jenis_div<?php echo $id_prodi[$i]; ?>').hide();
					$('#submit_alih_jenis_div<?php echo $id_prodi[$i]; ?>').show();

				});
	
		/*
		$('#myForm2<?php echo $id_prodi[$i]; ?>').submit(function() {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
				

				
					$('#sop_div<?php echo $id_prodi[$i]; ?>').show();
					$('#sp3_div<?php echo $id_prodi[$i]; ?>').show();
					$('#sop_usulan2_div<?php echo $id_prodi[$i]; ?>').show();
					$('#sp3_usulan2_div<?php echo $id_prodi[$i]; ?>').show();
					$('#sp3a_usulan2_div<?php echo $id_prodi[$i]; ?>').show();
					$('#sp3b_usulan2_div<?php echo $id_prodi[$i]; ?>').show();
					$('#sp3c_usulan2_div<?php echo $id_prodi[$i]; ?>').show();
					$('#matrikulasi_usulan2_div<?php echo $id_prodi[$i]; ?>').show();
					
					$('#sop<?php echo $id_prodi[$i]; ?>').hide();
					$('#sp3<?php echo $id_prodi[$i]; ?>').hide();
					$('#sop_usulan2<?php echo $id_prodi[$i]; ?>').hide();
					$('#sp3_usulan2<?php echo $id_prodi[$i]; ?>').hide();
					$('#sp3a_usulan2<?php echo $id_prodi[$i]; ?>').hide();
					$('#sp3b_usulan2<?php echo $id_prodi[$i]; ?>').hide();
					$('#sp3c_usulan2<?php echo $id_prodi[$i]; ?>').hide();
					$('#matrikulasi_usulan2<?php echo $id_prodi[$i]; ?>').hide();
					
					
					$('#submit_alih_jenis_div<?php echo $id_prodi[$i]; ?>').hide();
					$('#btnEdit_alih_jenis_div<?php echo $id_prodi[$i]; ?>').show();
					
					var sop_div = document.getElementById('sop_div<?php echo $id_prodi[$i]; ?>');
					var sp3_div = document.getElementById('sp3_div<?php echo $id_prodi[$i]; ?>');
					var sop_usulan2_div = document.getElementById('sop_usulan2_div<?php echo $id_prodi[$i]; ?>');
					var sp3_usulan2_div = document.getElementById('sp3_usulan2_div<?php echo $id_prodi[$i]; ?>');
					var sp3a_usulan2_div = document.getElementById('sp3a_usulan2_div<?php echo $id_prodi[$i]; ?>');
					var sp3b_usulan2_div = document.getElementById('sp3b_usulan2_div<?php echo $id_prodi[$i]; ?>');
					var sp3c_usulan2_div = document.getElementById('sp3c_usulan2_div<?php echo $id_prodi[$i]; ?>');
					var matrikulasi_usulan2_div = document.getElementById('matrikulasi_usulan2_div<?php echo $id_prodi[$i]; ?>');
					
					sop_div.innerHTML = document.getElementById('sop<?php echo $id_prodi[$i]; ?>').value;
					sp3_div.innerHTML = document.getElementById('sp3<?php echo $id_prodi[$i]; ?>').value;
					sop_usulan2_div.innerHTML = document.getElementById('sop_usulan2<?php echo $id_prodi[$i]; ?>').value;
					sp3_usulan2_div.innerHTML = document.getElementById('sp3_usulan2<?php echo $id_prodi[$i]; ?>').value;
					sp3a_usulan2_div.innerHTML = document.getElementById('sp3a_usulan2<?php echo $id_prodi[$i]; ?>').value;
					sp3b_usulan2_div.innerHTML = document.getElementById('sp3b_usulan2<?php echo $id_prodi[$i]; ?>').value;
					sp3c_usulan2_div.innerHTML = document.getElementById('sp3c_usulan2<?php echo $id_prodi[$i]; ?>').value;
					matrikulasi_usulan2_div.innerHTML = document.getElementById('matrikulasi_usulan2<?php echo $id_prodi[$i]; ?>').value;
					
				}
			})
			return false;
		});
		*/

		
				$('#myForm_alih_jenis<?php echo $id_prodi[$i]; ?>').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
							$('#sop_alih_jenis_usulan_div<?php echo $id_prodi[$i]; ?>').show();
							$('#sp3_alih_jenis_usulan_div<?php echo $id_prodi[$i]; ?>').show();
							$('#matrikulasi_alih_jenis_usulan_div<?php echo $id_prodi[$i]; ?>').show();
							
							$('#sop_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>').hide();
							$('#sp3_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>').hide();
							$('#matrikulasi_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>').hide();
							
							
							$('#submit_alih_jenis_div<?php echo $id_prodi[$i]; ?>').hide();
							$('#btnEdit_alih_jenis_div<?php echo $id_prodi[$i]; ?>').show();
							
							var sop_alih_jenis_usulan_div = document.getElementById('sop_alih_jenis_usulan_div<?php echo $id_prodi[$i]; ?>');
							var sp3_alih_jenis_usulan_div = document.getElementById('sp3_alih_jenis_usulan_div<?php echo $id_prodi[$i]; ?>');
							var matrikulasi_alih_jenis_usulan_div = document.getElementById('matrikulasi_alih_jenis_usulan_div<?php echo $id_prodi[$i]; ?>');
							
							sop_alih_jenis_usulan_div.innerHTML = document.getElementById('sop_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>').value;
							sp3_alih_jenis_usulan_div.innerHTML = document.getElementById('sp3_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>').value;
							matrikulasi_alih_jenis_usulan_div.innerHTML = document.getElementById('matrikulasi_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>').value;
							
						}
					})
					return false;
				});
		
		<?php
				}
			}
		?>
	});
	</script>
	
<script>
	$(function() {
		$( "#tabs" ).tabs();
	});
	$(function() {
		$( "#tabs2" ).tabs();
	});
	$(function() {
		$( "#tabs3" ).tabs();
	});
</script>
	
	

		<style type="text/css">
			h4 { font-size: 18px; }
			input { padding: 3px; border: 1px solid #999; }
			td { padding: 5px; }
			#result { background-color: #ffffcc; border: 1px solid #ffff99; padding: 10px; width: 100%; margin-bottom: 20px; }
		</style>

<div class="center_title_bar">SOP, SP3 PROGRAM STUDI</div> 
<?php
	include 'get_user_agent.php';
	$browser = $ua['name'];
	if($browser!=='Google Chrome'){
?>
<span style="font-size:18px;">Mohon gunakan browser </span> <img src="chrome_logo.gif" /> <span style="font-size:18px;">untuk mengakses modul ini</span>
<br />
Atau download Google Chrome di link ini, jika anda tidak memiliki browser tersebut : <a onclick="window.location.href='http://www.google.com/chrome/?hl=en&brand=chmo#eula'" href="">Google Chrome Downloads</a>
<?php
	}
	else{
?>
		<div id="loading" style="display:none;"><img src="loading.gif" alt="loading..." /></div>
		<div id="result" style="display:none; padding-top:10px; width:70%;"></div>



<div id="tabs" style="width:790px;">
	<ul>
		<li><a href="#tabs-1">NON ALIH JENIS</a></li>
		<li><a href="#tabs-2">ALIH JENIS</a></li>
	</ul>
	<div id="tabs-1">
		<div id="tabs2">
			<ul>
				<li><a href="#tabs-1">S1(SNMPTN)</a></li>
				<li><a href="#tabs-2">S1(MANDIRI)</a></li>
				<li><a href="#tabs-3">D3, S2, S3, Profesi, Spesialis</a></li>
			</ul>
			
			<div id="tabs-1">
				<table class="ui-widget" width="90%" >
					<thead class="ui-widget-header">
								<tr class="ui-widget-header">
									<td class="header-coloumn" rowspan="2" colspan="1">No</td>
									<td class="header-coloumn" rowspan="2" colspan="1">Prodi</td>
									<td class="header-coloumn" colspan="4"><center>Sekarang</center></td>
									<td class="header-coloumn" colspan="4"><center>Usulan</center></td>			
									<td class="header-coloumn" rowspan="2" colspan="1">Action</td>
								</tr>
								<tr>
									<td>SOP</td>
									<td>SP3 a</td>
									<td>SP3 b</td>
									<td>SP3 c</td>
									<td>SOP</td>
									<td>SP3 a</td>
									<td>SP3 b</td>
									<td>SP3 c</td>
								</tr>
					</thead>
					<?php
						for($i=0;$i<$jml_prodi;$i++){
							if($is_mandiri[$i]==0){
								if($is_alih_jenis[$i]==0){
									if($nm_jenjang[$i]=='S1'){

									
					?>
					
					<form id="myFormS1<?php echo $id_prodi[$i]; ?>"  name="myForm<?php echo $id_prodi[$i]; ?>" method="post" action="biaya_prodi_insert.php" style="display:none;">
					<tbody id="listDayaTampung">

					<tr>
						<td class="ui-widget-content">
							<?php
								echo $i+1;
							?>
						</td>
						<td class="ui-widget-content">
							<a href="spek_prodi.php?id=<?php echo $id_prodi[$i]; ?>&prodi=<?php echo $i; ?>">
							<?php
								echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i];
							?>
							</a>
							<input type="text" id="id_prodi" name="id_prodi" style="display:none;" value="<?php echo $id_prodi[$i]; ?>" />
							<input type="text" id="alihjenis" name="alihjenis" style="display:none;" value="<?php echo $is_alih_jenis[$i]; ?>" />
							<input type="text" id="ismandiri" name="ismandiri" style="display:none;" value="<?php echo $is_mandiri[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sop_div<?php echo $id_prodi[$i]; ?>">
								<?php echo $sop[$i]; ?>
							</div>
							<input style="display:none;" type="text" id="sop<?php echo $id_prodi[$i]; ?>" name="sop<?php echo $id_prodi[$i]; ?>" value="<?php echo $sop[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sp3a_div<?php echo $id_prodi[$i]; ?>">
								<?php echo $sp3a[$i]; ?>
							</div>
							<input style="display:none;" type="text" id="sp3a<?php echo $id_prodi[$i]; ?>" name="sp3a<?php echo $id_prodi[$i]; ?>" value="<?php echo $sp3a[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sp3b_div<?php echo $id_prodi[$i]; ?>">
								<?php echo $sp3b[$i]; ?>
							</div>
							<input style="display:none;" type="text" id="sp3b<?php echo $id_prodi[$i]; ?>" name="sp3b<?php echo $id_prodi[$i]; ?>" value="<?php echo $sp3b[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="mandiri_div<?php echo $id_prodi[$i]; ?>">
								<?php echo $sp3c[$i]; ?>
							</div>
							<input style="display:none;" type="text" id="sp3c<?php echo $id_prodi[$i]; ?>" name="sp3c<?php echo $id_prodi[$i]; ?>" value="<?php echo $sp3c[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sopS1_usulan_div<?php echo $id_prodi[$i]; ?>">
								<?php echo $sop_usulan[$i]; ?>
							</div>
							<input style="display:none; width:80px;" type="text" id="sopS1_usulan<?php echo $id_prodi[$i]; ?>" name="sop_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $sop_usulan[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sp3a_usulan_div<?php echo $id_prodi[$i]; ?>">
								<?php echo $sp3a_usulan[$i]; ?>
							</div>
							<input style="display:none; width:80px;" type="text" id="sp3a_usulan<?php echo $id_prodi[$i]; ?>" name="sp3a_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $sp3a_usulan[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sp3b_usulan_div<?php echo $id_prodi[$i]; ?>">
								<?php echo $sp3b_usulan[$i]; ?>
							</div>
							<input style="display:none; width:80px;" type="text" id="sp3b_usulan<?php echo $id_prodi[$i]; ?>" name="sp3b_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $sp3b_usulan[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sp3c_usulan_div<?php echo $id_prodi[$i]; ?>">
								<?php echo $sp3c_usulan[$i]; ?>
							</div>
							<input style="display:none; width:80px;" type="text" id="sp3c_usulan<?php echo $id_prodi[$i]; ?>" name="sp3c_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $sp3c_usulan[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="btnEdit_div<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>" name="btnEdit<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>">
								Edit
							</div>
							<div style="display:none;" id="submit<?php echo $id_prodi[$i]; ?>" name="submit<?php echo $id_prodi[$i]; ?>">
								<input type="submit" name="btn_submit" id="btn_submit" value="Submit"  />
							</div>
						</td>
						
					</tr>	
					</tbody>
					</form>
					<?php
									}
								}
							}
						}
					?>	
				</table>		
			</div>
			
			<div id="tabs-2">
				<table class="ui-widget" width="70%" >
					<thead class="ui-widget-header">
								<tr class="ui-widget-header">
									<td class="header-coloumn" rowspan="2" colspan="1">No</td>
									<td class="header-coloumn" rowspan="2" colspan="1">Prodi</td>
									<td class="header-coloumn" colspan="2"><center>Sekarang</center></td>
									<td class="header-coloumn" colspan="2"><center>Usulan</center></td>			
									<td class="header-coloumn" rowspan="2" colspan="1">Action</td>
								</tr>
								<tr>
									<td>SOP</td>
									<td>SP3</td>
									<td>SOP</td>
									<td>SP3</td>
								</tr>
					</thead>
					<?php
						for($i=0;$i<$jml_prodi;$i++){
						if($is_mandiri[$i]==1){
							if($is_alih_jenis[$i]==0){
								if($nm_jenjang[$i]=='S1'){

					?>
					
					<form id="myFormMandiri<?php echo $id_prodi[$i]; ?>"  name="myFormMandiri<?php echo $id_prodi[$i]; ?>" method="post" action="biaya_prodi_insert.php" style="display:none;">
					<tbody id="listDayaTampung">

					<tr>
						<td class="ui-widget-content">
							<?php
								echo $i+1;
							?>
						</td>
						<td class="ui-widget-content">
							<a href="spek_prodi.php?id=<?php echo $id_prodi[$i]; ?>&prodi=<?php echo $i; ?>">
							<?php
								echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i];
							?>
							</a>
							<input type="text" id="id_prodi" name="id_prodi" style="display:none;" value="<?php echo $id_prodi[$i]; ?>" />
							<input type="text" id="alihjenis" name="alihjenis" style="display:none;" value="<?php echo $is_alih_jenis[$i]; ?>" />
							<input type="text" id="ismandiri" name="ismandiri" style="display:none;" value="<?php echo $is_mandiri[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sop_mandiri_div<?php echo $id_prodi[$i]; ?>">
								<?php echo $sop[$i]; ?>
							</div>
							<input style="display:none;" type="text" id="sop_mandiri<?php echo $id_prodi[$i]; ?>" name="sop_mandiri<?php echo $id_prodi[$i]; ?>" value="<?php echo $sop[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sp3_mandiri_div<?php echo $id_prodi[$i]; ?>">
								<?php echo $sp3[$i]; ?>
							</div>
							<input style="display:none;" type="text" id="sp3_mandiri<?php echo $id_prodi[$i]; ?>" name="sp3_mandiri<?php echo $id_prodi[$i]; ?>" value="<?php echo $sp3[$i]; ?>" />
						</td>
						
						
						<td class="ui-widget-content" >
							<div id="sop_mandiri_usulan_div<?php echo $id_prodi[$i]; ?>">
								<?php echo $sop_usulan[$i]; ?>
							</div>
							<input style="display:none; width:80px;" type="text" id="sop_mandiri_usulan<?php echo $id_prodi[$i]; ?>" name="sop_mandiri_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $sop_usulan[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sp3_mandiri_usulan_div<?php echo $id_prodi[$i]; ?>">
								<?php echo $sp3_usulan[$i]; ?>
							</div>
							<input style="display:none; width:80px;" type="text" id="sp3_mandiri_usulan<?php echo $id_prodi[$i]; ?>" name="sp3_mandiri_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $sp3_usulan[$i]; ?>" />
						</td>
		
						
						<td class="ui-widget-content" >
							<div id="btnEdit_mandiri_div<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>" name="btnEdit_mandiri<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>">
								Edit
							</div>
							<div style="display:none;" id="submit_mandiri<?php echo $id_prodi[$i]; ?>" name="submit_mandiri<?php echo $id_prodi[$i]; ?>">
								<input type="submit" name="btn_submit_mandiri" id="btn_submit_mandiri" value="Submit"  />
							</div>
						</td>
						
					</tr>	
					</tbody>
					</form>
					<?php
									}
								}
							}
						}
					?>	
				</table>		
			</div>
			
			<div id="tabs-3">
				<table class="ui-widget" width="70%" >
					<thead class="ui-widget-header">
								<tr class="ui-widget-header">
									<td class="header-coloumn" rowspan="2" colspan="1">No</td>
									<td class="header-coloumn" rowspan="2" colspan="1">Prodi</td>
									<td class="header-coloumn" colspan="3"><center>Sekarang</center></td>
									<td class="header-coloumn" colspan="3"><center>Usulan</center></td>			
									<td class="header-coloumn" rowspan="2" colspan="1">Action</td>
								</tr>
								<tr>
									<td>SOP</td>
									<td>SP3</td>
									<td>MATRIKULASI</td>
									<td>SOP</td>
									<td>SP3</td>
									<td>MATRIKULASI</td>
								</tr>
					</thead>
					<?php
						for($i=0;$i<$jml_prodi;$i++){
							if($is_alih_jenis[$i]==0){
								if($nm_jenjang[$i]!='S1'){
					?>
					
					<form id="myForm<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>"  name="myForm<?php echo $id_prodi[$i] . $kelompok_biaya[$i];; ?>" method="post" action="biaya_prodi_insert.php" style="display:none;">
					<tbody id="listDayaTampung">

					<tr>
						<td class="ui-widget-content">
							<?php
								echo $i+1;
							?>
						</td>
						<td class="ui-widget-content">
							<a href="spek_prodi.php?id=<?php echo $id_prodi[$i]; ?>&prodi=<?php echo $i; ?>">
							<?php
								echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i] . ' ' . $kelompok_biaya[$i];
							?>
							</a>
							<input type="text" id="id_prodi" name="id_prodi" style="display:none;" value="<?php echo $id_prodi[$i]; ?>" />
							<input type="text" id="kelompok_biaya" name="kelompok_biaya" style="display:none;" value="<?php echo $kelompok_biaya[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sop_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>">
								<?php echo $sop[$i]; ?>
							</div>
							<input style="display:none;" type="text" id="sop<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>" name="sop<?php echo $id_prodi[$i]; ?>" value="<?php echo $sop[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sp3_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>">
								<?php echo $sp3[$i]; ?>
							</div>
							<input style="display:none;" type="text" id="sp3<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>" name="sp3<?php echo $id_prodi[$i]; ?>" value="<?php echo $sp3[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="matrikulasi_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>">
								<?php echo $matrikulasi[$i]; ?>
							</div>
							<input style="display:none;" type="text" id="matrikulasi<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>" name="matrikulasi<?php echo $id_prodi[$i]; ?>" value="<?php echo $matrikulasi[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sop_usulan_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>">
								<?php echo $sop_usulan[$i]; ?>
							</div>
							<input style="display:none; width:80px;" type="text" id="sop_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>" name="sop_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>" value="<?php echo $sop_usulan[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="sp3_usulan_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>">
								<?php echo $sp3_usulan[$i]; ?>
							</div>
							<input style="display:none; width:80px;" type="text" id="sp3_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>" name="sp3_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>" value="<?php echo $sp3_usulan[$i]?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="matrikulasi_usulan_div<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>">
								<?php echo $matrikulasi_usulan[$i]; ?>
							</div>
							<input style="display:none; width:80px;" type="text" id="matrikulasi_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>" name="matrikulasi_usulan<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>" value="<?php echo $matrikulasi_usulan[$i]; ?>" />
						</td>
						
						<td class="ui-widget-content" >
							<div id="btnEdit_div<?php echo str_replace(" ", "", trim($id_prodi[$i] . $kelompok_biaya[$i])); ?>" name="btnEdit<?php echo str_replace(" ", "", trim($id_prodi[$i] . $kelompok_biaya[$i])); ?>">
								Edit
							</div>
							<div style="display:none;" id="submit<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>" name="submit<?php echo $id_prodi[$i] . $kelompok_biaya[$i]; ?>">
								<input type="submit" name="btn_submit" id="btn_submit" value="Submit"  />
							</div>
						</td>
						
					</tr>	
					</tbody>
					</form>
					<?php
								}
							}
						}
					?>	
				</table>		
			</div>
			
		</div>
	</div>
	
	<div id="tabs-2">
		<div id="tabs3">
			<ul>
				<li><a href="#tabs-1">S1</a></li>
			</ul>
					<table class="ui-widget" width="70%" >
						<thead class="ui-widget-header">
									<tr class="ui-widget-header">
										<td class="header-coloumn" rowspan="2" colspan="1">No</td>
										<td class="header-coloumn" rowspan="2" colspan="1">Prodi</td>
										<td class="header-coloumn" colspan="3"><center>Sekarang</center></td>
										<td class="header-coloumn" colspan="3"><center>Usulan</center></td>			
										<td class="header-coloumn" rowspan="2" colspan="1">Action</td>
									</tr>
									<tr>
										<td>SOP</td>
										<td>SP3</td>
										<td>MATRIKULASI</td>
										<td>SOP</td>
										<td>SP3</td>
										<td>MANTRIKULASI</td>
									</tr>
						</thead>
						<?php
							for($i=0;$i<$jml_prodi;$i++){
								if($is_alih_jenis[$i]==1){
									if($nm_jenjang[$i]=='S1'){
						?>
						
						<form id="myForm_alih_jenis<?php echo $id_prodi[$i]; ?>"  name="myForm_alih_jenis<?php echo $id_prodi[$i]; ?>" method="post" action="biaya_prodi_insert.php" style="display:none;">
						<tbody id="listDayaTampung">

						<tr>
							<td class="ui-widget-content">
								<?php
									echo $i+1;
								?>
							</td>
							<td class="ui-widget-content">
								<a href="spek_prodi.php?id=<?php echo $id_prodi[$i]; ?>&prodi=<?php echo $i; ?>">
								<?php
									echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i];
								?>
								</a>
								<input type="text" id="id_prodi" name="id_prodi" style="display:none;" value="<?php echo $id_prodi[$i]; ?>" />
								<input type="text" id="alihjenis" name="alihjenis" style="display:none;" value="<?php echo $is_alih_jenis[$i]; ?>" />
							</td>
							
							<td class="ui-widget-content" >
								<div id="sop_alih_jenis_div<?php echo $id_prodi[$i]; ?>">
									<?php echo $sop[$i]; ?>
								</div>
								<input style="display:none;" type="text" id="sop_alih_jenis<?php echo $id_prodi[$i]; ?>" name="sop_alih_jenis<?php echo $id_prodi[$i]; ?>" value="<?php echo $sop[$i]; ?>" />
							</td>
							
							<td class="ui-widget-content" >
								<div id="sp3_alih_jenis_div<?php echo $id_prodi[$i]; ?>">
									<?php echo $sp3[$i]; ?>
								</div>
								<input style="display:none;" type="text" id="sp3_alih_jenis<?php echo $id_prodi[$i]; ?>" name="sp3_alih_jenis<?php echo $id_prodi[$i]; ?>" value="<?php echo $sp3[$i]; ?>" />
							</td>
							
							
							<td class="ui-widget-content" >
								<div id="matrikulasi_alih_jenis_div<?php echo $id_prodi[$i]; ?>">
									<?php echo $matrikulasi[$i]; ?>
								</div>
								<input style="display:none;" type="text" id="matrikulasi_alih_jenis<?php echo $id_prodi[$i]; ?>" name="matrikulasi_alih_jenis<?php echo $id_prodi[$i]; ?>" value="<?php echo $matrikulasi[$i]; ?>" />
							</td>
							
							<td class="ui-widget-content" >
								<div id="sop_alih_jenis_usulan_div<?php echo $id_prodi[$i]; ?>">
									<?php echo $sop_usulan[$i]; ?>
								</div>
								<input style="display:none; width:80px;" type="text" id="sop_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>" name="sop_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $sop_usulan[$i]; ?>" />
							</td>
							
							<td class="ui-widget-content" >
								<div id="sp3_alih_jenis_usulan_div<?php echo $id_prodi[$i]; ?>">
									<?php echo $sp3_usulan[$i]; ?>
								</div>
								<input style="display:none; width:80px;" type="text" id="sp3_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>" name="sp3_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $sp3_usulan[$i]; ?>" />
							</td>

							<td class="ui-widget-content" >
								<div id="matrikulasi_alih_jenis_usulan_div<?php echo $id_prodi[$i]; ?>">
									<?php echo $matrikulasi_usulan[$i]; ?>
								</div>
								<input style="display:none; width:80px;" type="text" id="matrikulasi_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>" name="matrikulasi_alih_jenis_usulan<?php echo $id_prodi[$i]; ?>" value="<?php echo $matrikulasi_usulan[$i]; ?>" />
							</td>							
							
							
							<td class="ui-widget-content" >
								<div id="btnEdit_alih_jenis_div<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>" name="btnEdit_alih_jenis<?php echo str_replace(" ", "", trim($id_prodi[$i])); ?>">
									Edit
								</div>
								<div style="display:none;" id="submit_alih_jenis_div<?php echo $id_prodi[$i]; ?>" name="submit_alih_jenis<?php echo $id_prodi[$i]; ?>">
									<input type="submit" name="btn_submit_alih_jenis" id="btn_submit_alih_jenis" value="Submit"  />
								</div>
							</td>
							
						</tr>	
						</tbody>
						</form>
						<?php
									}
								}
							}
						?>	
					</table>
				
			</div>
	</div>
</div>

<?php
	}
?>