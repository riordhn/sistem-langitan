<?php
require 'conf.php';
require '../sumberdaya/includes/encrypt.php';

$nama_singkat_pt = $nama_singkat;

$depan = time();
$belakang = strrev(time());

$id_pgg = $user->ID_PENGGUNA;
$id_dsn = $user->ID_DOSEN;
$id_fak = $user->ID_FAKULTAS;

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_DOSEN) {

	// force password to ISO27001:2009
	if($_SESSION['standar_iso']=='no'){
		header("Location: ../../login.php?mode=ltp-null");
		exit();
	}

    // data menu dari $user
    $smarty->assign('modul_set', $user->MODULs);

    

    if (file_exists("../../foto_pegawai/{$nama_singkat_pt}/{$user->USERNAME}.JPG"))
        $smarty->assign('foto', "/foto_pegawai/{$nama_singkat_pt}/{$user->USERNAME}.JPG");
    else
        $smarty->assign('foto', "/img/dosen/photo.png");

    $db->Query("select case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)) else trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)||', '||pgg.gelar_belakang) end as nm_pengguna,
            dsn.nip_dosen from dosen dsn left join pengguna pgg on pgg.id_pengguna=dsn.id_pengguna
            where dsn.id_pengguna=$id_pgg");
    $foto = $db->FetchAssoc();
    $to = $foto['NM_PENGGUNA'];
    $file = $foto['NIP_DOSEN'];

    $img = 'proses/upload_photo.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&yth=' . $to . '&file=' . $file . '&' . $belakang . '=' . $depan . $belakang) . '';
    $smarty->assign('IMG', $img);


    $db->Query("select username as photo from pengguna where id_pengguna=$id_pgg");
    $get_photo = $db->FetchAssoc();
    $filename = "../../foto_pegawai/" . $nama_singkat_pt . "/" . $get_photo['PHOTO'] . ".JPG";
    if (file_exists($filename)) {
        $photo = $filename;
    } else {
        $photo = '../sumberdaya/includes/images/unknown.png';
    }
    $smarty->assign('PHOTO', $photo);

    //menampilkan file template
    $smarty->assign('jumlah_menu_tampil',$jumlah_menu_tampil);
    $smarty->display('index.tpl');
} else {
    echo '<script>alert("Anda Belum Login ' . $user->Role() . '");window.location="../../."</script>';
}
