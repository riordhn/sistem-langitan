<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

		$is_alih_jenis = htmlspecialchars($_POST['alihjenis']);
		$prodi_ubah = htmlspecialchars($_POST['prodi_ubah']);          
		$id_fakultas = $user->ID_FAKULTAS_JABATAN;                             
		$id_program_studi = htmlspecialchars($_POST['id_prodi']);                        
		$prodi_nama = htmlspecialchars($_POST['prodi_nama']);                           
		$prodi_jenis = htmlspecialchars($_POST['prodi_jenis']);                           
		$prodi_kategori = htmlspecialchars($_POST['prodi_kategori']);                        
		$prodi_penjelasan = htmlspecialchars($_POST['prodi_penjelasan']);                    
		$prodi_tahun = htmlspecialchars($_POST['prodi_tahun']);                           
		$prodi_sk_oleh = htmlspecialchars($_POST['prodi_sk_oleh']);                         
		$prodi_sk_nomor = htmlspecialchars($_POST['prodi_sk_nomor']);                        
		$prodi_sk_terbit = htmlspecialchars($_POST['prodi_sk_terbit']);                       
		$prodi_sk_expired = htmlspecialchars($_POST['prodi_sk_expired']);                      
		$prodi_skp_nomor = htmlspecialchars($_POST['prodi_skp_nomor']);                       
		$prodi_skp_terbit = htmlspecialchars($_POST['prodi_skp_terbit']);                      
		$prodi_skp_expired = htmlspecialchars($_POST['prodi_skp_expired']);                     
		$prodi_ketua_nama = htmlspecialchars($_POST['prodi_ketua_nama']);                     
		$prodi_ketua_sk_nomor = htmlspecialchars($_POST['prodi_ketua_sk_nomor']);                  
		$prodi_ketua_sk_terbit = htmlspecialchars($_POST['prodi_ketua_sk_terbit']);                 
		$prodi_ketua_sk_expired = htmlspecialchars($_POST['prodi_ketua_sk_expired']);                
		$prodi_sekre_nama = htmlspecialchars($_POST['prodi_sekre_nama']);                     
		$prodi_sekre_sk_nomor = htmlspecialchars($_POST['prodi_sekre_sk_nomor']);                  
		$prodi_sekre_sk_terbit = htmlspecialchars($_POST['prodi_sekre_sk_terbit']);                 
		$prodi_sekre_sk_expired = htmlspecialchars($_POST['prodi_sekre_sk_expired']);                
		$prodi_dosen_tetap = htmlspecialchars($_POST['prodi_dosen_tetap']);                     
		$prodi_dosen_tdktetap = htmlspecialchars($_POST['prodi_dosen_tdktetap']);                  
		$prodi_dosen_s2 = htmlspecialchars($_POST['prodi_dosen_s2']);                        
		$prodi_dosen_s3 = htmlspecialchars($_POST['prodi_dosen_s3']);                        
		$prodi_dosen_gubes = htmlspecialchars($_POST['prodi_dosen_gubes']);                     
		$prodi_kur_nama = htmlspecialchars($_POST['prodi_kur_nama']);                        
		$prodi_kur_sk_nomor = htmlspecialchars($_POST['prodi_kur_sk_nomor']);                    
		$prodi_kur_sk_terbit = htmlspecialchars($_POST['prodi_kur_sk_terbit']);                   
		$prodi_kur_sk_expired = htmlspecialchars($_POST['prodi_kur_sk_expired']);                  
		$prodi_kur_masa_studi = htmlspecialchars($_POST['prodi_kur_masa_studi']);                  
		$prodi_kur_beban_studi= htmlspecialchars($_POST['prodi_kur_beban_studi']);                 
		$prodi_lo = htmlspecialchars($_POST['prodi_lo']);                             
		$prodi_gelar_nama = htmlspecialchars($_POST['prodi_gelar_nama']);                     
		$prodi_gelar_singkat = htmlspecialchars($_POST['prodi_gelar_singkat']);                   
		$prodi_gelar_sk_nomor = htmlspecialchars($_POST['prodi_gelar_sk_nomor']);                  
		$prodi_dayatampung = htmlspecialchars($_POST['prodi_dayatampung']);                     
		$prodi_dayatampung_sk_nomor = htmlspecialchars($_POST['prodi_dayatampung_sk_nomor']);            
		$prodi_jml_mhs = htmlspecialchars($_POST['prodi_jml_mhs']);                         
		$prodi_jml_peminat = htmlspecialchars($_POST['prodi_jml_peminat']);                     
		$prodi_jml_maba_calon = htmlspecialchars($_POST['prodi_jml_maba_calon']);                  
		$prodi_jml_maba_daftar = htmlspecialchars($_POST['prodi_jml_maba_daftar']);                 
		$prodi_jml_mhs_lama = htmlspecialchars($_POST['prodi_jml_mhs_lama']);                    
		$prodi_jml_alumni = htmlspecialchars($_POST['prodi_jml_alumni']);                      
		$prodi_akred = htmlspecialchars($_POST['prodi_akred']);                            
		$prodi_akred_sk_nomor = htmlspecialchars($_POST['prodi_akred_sk_nomor']);                  
		$prodi_akred_sk_terbit = htmlspecialchars($_POST['prodi_akred_sk_terbit']);                 
		$prodi_akred_sk_expired = htmlspecialchars($_POST['prodi_akred_sk_expired']);                
		$prodi_akred_nilai = htmlspecialchars($_POST['prodi_akred_nilai']);                     
		$prodi_akred_peringkat = htmlspecialchars($_POST['prodi_akred_peringkat']);                 
		$prodi_biaya_sk_nomor = htmlspecialchars($_POST['prodi_biaya_sk_nomor']);                  
		$prodi_biaya_sop = htmlspecialchars($_POST['prodi_biaya_sop']);                       
		$prodi_biaya_sp3 = htmlspecialchars($_POST['prodi_biaya_sp3']);                       
		$prodi_biaya_matrikulasi = htmlspecialchars($_POST['prodi_biaya_matrikulasi']);               
		$prodi_biaya_pendafataran = htmlspecialchars($_POST['prodi_biaya_pendaftaran']);               
		$prodi_ppcmb = htmlspecialchars($_POST['prodi_ppcmb']);                          
		$prodi_ppcmb_ijazah = htmlspecialchars($_POST['prodi_ppcmb_ijazah']);                   
		$prodi_ppcmb_umur = htmlspecialchars($_POST['prodi_ppcmb_umur']);                      
		$prodi_ppcmb_kelamin = htmlspecialchars($_POST['prodi_ppcmb_kelamin']);                   
		$prodi_ppcmb_ketunaan = htmlspecialchars($_POST['prodi_ppcmb_ketunaan']);                 
		$prodi_ppcmb_tunanetra = htmlspecialchars($_POST['prodi_ppcmb_tunanetra']);                
		$prodi_ppcmb_tunarungu = htmlspecialchars($_POST['prodi_ppcmb_tunarungu']);                
		$prodi_ppcmb_tunafisik = htmlspecialchars($_POST['prodi_ppcmb_tunafisik']);                
		$prodi_ppcmb_tunawicara = htmlspecialchars($_POST['prodi_ppcmb_tunawicara']);               
		$prodi_ppcmb_tunalain = htmlspecialchars($_POST['prodi_ppcmb_tunalain']);                 
		$prodi_ppcmb_tinggibadan = htmlspecialchars($_POST['prodi_ppcmb_tinggibadan']);              
		$prodi_ppcmb_elpt = htmlspecialchars($_POST['prodi_ppcmb_elpt']);  
 		
		//$id_semester = htmlspecialchars($_POST['id_semester']);                             
		/*
		$db->query("
					insert into prodi_spesifikasi (id_fakultas,                             
						id_program_studi,                        
						prodi_nama,                           
						prodi_jenis,                           
						prodi_kategori,                        
						prodi_penjelasan,                     
						prodi_tahun,                           
						prodi_sk_oleh,                         
						prodi_sk_nomor,                        
						prodi_sk_terbit,                       
						prodi_sk_expired,                      
						prodi_skp_nomor,                       
						prodi_skp_terbit,                      
						prodi_skp_expired,                     
						prodi_ketua_nama,                     
						prodi_ketua_sk_nomor,                  
						prodi_ketua_sk_terbit,                 
						prodi_ketua_sk_expired,                
						prodi_sekre_nama,                     
						prodi_sekre_sk_nomor,                  
						prodi_sekre_sk_terbit,                 
						prodi_sekre_sk_expired,                
						prodi_dosen_tetap,                     
						prodi_dosen_tdktetap,                  
						prodi_dosen_s2,                        
						prodi_dosen_s3,                        
						prodi_dosen_gubes,                     
						prodi_kur_nama,                        
						prodi_kur_sk_nomor,                    
						prodi_kur_sk_terbit,                   
						prodi_kur_sk_expired,                  
						prodi_kur_masa_studi,                  
						prodi_kur_beban_studi,                 
						prodi_lo,                             
						prodi_gelar_nama,                     
						prodi_gelar_singkat,                   
						prodi_gelar_sk_nomor,                  
						prodi_dayatampung,                     
						prodi_dayatampung_sk_nomor,            
						prodi_jml_mhs,                         
						prodi_jml_peminat,                     
						prodi_jml_maba_calon,                  
						prodi_jml_maba_daftar,                 
						prodi_jml_mhs_lama,                    
						prodi_jml_alumni,                      
						prodi_akred,                            
						prodi_akred_sk_nomor,                  
						prodi_akred_sk_terbit,                 
						prodi_akred_sk_expired,                
						prodi_akred_nilai,                     
						prodi_akred_peringkat,                 
						prodi_biaya_sk_nomor,                  
						prodi_biaya_sop,                       
						prodi_biaya_sp3,                       
						prodi_biaya_matrikulasi,               
						prodi_biaya_pendaftaran,               
						prodi_ppcmb,                          
						prodi_ppcmb_ijazah,                   
						prodi_ppcmb_umur,                      
						prodi_ppcmb_kelamin,                   
						prodi_ppcmb_ketunaan,                 
						prodi_ppcmb_tunanetra,                
						prodi_ppcmb_tunarungu,                
						prodi_ppcmb_tunafisik,                
						prodi_ppcmb_tunawicara,               
						prodi_ppcmb_tunalain,                 
						prodi_ppcmb_tinggibadan,              
						prodi_ppcmb_elpt                             
					)
					values ('$id_fakultas',                             
						'$id_program_studi',                        
						'$prodi_nama',                           
						'$prodi_jenis',                           
						'$prodi_kategori',                        
						'$prodi_penjelasan',                     
						'$prodi_tahun',                           
						'$prodi_sk_oleh',                         
						'$prodi_sk_nomor',                        
						'$prodi_sk_terbit',                       
						'$prodi_sk_expired',                      
						'$prodi_skp_nomor',                       
						'$prodi_skp_terbit',                      
						'$prodi_skp_expired',                     
						'$prodi_ketua_nama',                     
						'$prodi_ketua_sk_nomor',                  
						'$prodi_ketua_sk_terbit',                 
						'$prodi_ketua_sk_expired',                
						'$prodi_sekre_nama',                     
						'$prodi_sekre_sk_nomor',                  
						'$prodi_sekre_sk_terbit',                 
						'$prodi_sekre_sk_expired',                
						'$prodi_dosen_tetap',                     
						'$prodi_dosen_tdktetap',                  
						'$prodi_dosen_s2',                        
						'$prodi_dosen_s3',                        
						'$prodi_dosen_gubes',                     
						'$prodi_kur_nama',                        
						'$prodi_kur_sk_nomor',                    
						'$prodi_kur_sk_terbit',                   
						'$prodi_kur_sk_expired',                  
						'$prodi_kur_masa_studi',                  
						'$prodi_kur_beban_studi',                 
						'$prodi_lo',                             
						'$prodi_gelar_nama',                     
						'$prodi_gelar_singkat',                   
						'$prodi_gelar_sk_nomor',                  
						'$prodi_dayatampung',                     
						'$prodi_dayatampung_sk_nomor',            
						'$prodi_jml_mhs',                         
						'$prodi_jml_peminat',                     
						'$prodi_jml_maba_calon',                  
						'$prodi_jml_maba_daftar',                 
						'$prodi_jml_mhs_lama',                    
						'$prodi_jml_alumni',                      
						'$prodi_akred',                            
						'$prodi_akred_sk_nomor',                  
						'$prodi_akred_sk_terbit',                 
						'$prodi_akred_sk_expired',                
						'$prodi_akred_nilai',                     
						'$prodi_akred_peringkat',                 
						'$prodi_biaya_sk_nomor',                  
						'$prodi_biaya_sop',                       
						'$prodi_biaya_sp3',                       
						'$prodi_biaya_matrikulasi',               
						'$prodi_biaya_pendaftaran',               
						'$prodi_ppcmb',                          
						'$prodi_ppcmb_ijazah',                   
						'$prodi_ppcmb_umur',                      
						'$prodi_ppcmb_kelamin',                   
						'$prodi_ppcmb_ketunaan',                 
						'$prodi_ppcmb_tunanetra',                
						'$prodi_ppcmb_tunarungu',                
						'$prodi_ppcmb_tunafisik',                
						'$prodi_ppcmb_tunawicara',               
						'$prodi_ppcmb_tunalain',                 
						'$prodi_ppcmb_tinggibadan',              
						'$prodi_ppcmb_elpt'                             
					)
		");
		*/
		
		
		
		$db->Query("update prodi_spesifikasi set
					prodi_nama                           	=	'" . $prodi_nama . "',                           
					prodi_jenis                           	=	'" . $prodi_jenis . "',                           
					prodi_kategori                        	=	'" . $prodi_kategori . "',                        
					prodi_penjelasan                     	=	'" . $prodi_penjelasan . "',                     
					prodi_tahun                           	=	'" . $prodi_tahun . "',                           
					prodi_sk_oleh                         	=	'" . $prodi_sk_oleh . "',                         
					prodi_sk_nomor                        	=	'" . $prodi_sk_nomor . "',                        
					prodi_sk_terbit                       	=	'" . $prodi_sk_terbit . "',                       
					prodi_sk_expired                      	=	'" . $prodi_sk_expired . "',                      
					prodi_skp_nomor                       	=	'" . $prodi_skp_nomor . "',                       
					prodi_skp_terbit                      	=	'" . $prodi_skp_terbit . "',                      
					prodi_skp_expired                     	=	'" . $prodi_skp_expired . "',                     
					prodi_ketua_nama                     	=	'" . $prodi_ketua_nama . "',                     
					prodi_ketua_sk_nomor                  	=	'" . $prodi_ketua_sk_nomor . "',                  
					prodi_ketua_sk_terbit                 	=	'" . $prodi_ketua_sk_terbit . "',                 
					prodi_ketua_sk_expired                	=	'" . $prodi_ketua_sk_expired . "',                
					prodi_sekre_nama                     	=	'" . $prodi_sekre_nama . "',                     
					prodi_sekre_sk_nomor                  	=	'" . $prodi_sekre_sk_nomor . "',                  
					prodi_sekre_sk_terbit                 	=	'" . $prodi_sekre_sk_terbit . "',                 
					prodi_sekre_sk_expired                	=	'" . $prodi_sekre_sk_expired . "',                
					prodi_dosen_tetap                     	=	'" . $prodi_dosen_tetap . "',                     
					prodi_dosen_tdktetap                  	=	'" . $prodi_dosen_tdktetap . "',                  
					prodi_dosen_s2                        	=	'" . $prodi_dosen_s2 . "',                        
					prodi_dosen_s3                        	=	'" . $prodi_dosen_s3 . "',                        
					prodi_dosen_gubes                     	=	'" . $prodi_dosen_gubes . "',                     
					prodi_kur_nama                        	=	'" . $prodi_kur_nama . "',                        
					prodi_kur_sk_nomor                    	=	'" . $prodi_kur_sk_nomor . "',                    
					prodi_kur_sk_terbit                   	=	'" . $prodi_kur_sk_terbit . "',                   
					prodi_kur_sk_expired                  	=	'" . $prodi_kur_sk_expired . "',                  
					prodi_kur_masa_studi                  	=	'" . $prodi_kur_masa_studi . "',                  
					prodi_kur_beban_studi                 	=	'" . $prodi_kur_beban_studi . "',                 
					prodi_lo                             	=	'" . $prodi_lo . "',                             
					prodi_gelar_nama                     	=	'" . $prodi_gelar_nama . "',                     
					prodi_gelar_singkat                   	=	'" . $prodi_gelar_singkat . "',                   
					prodi_gelar_sk_nomor                  	=	'" . $prodi_gelar_sk_nomor . "',                  
					prodi_dayatampung                     	=	'" . $prodi_dayatampung . "',                     
					prodi_dayatampung_sk_nomor            	=	'" . $prodi_dayatampung_sk_nomor . "',            
					prodi_jml_mhs                         	=	'" . $prodi_jml_mhs . "',                         
					prodi_jml_peminat                     	=	'" . $prodi_jml_peminat . "',                     
					prodi_jml_maba_calon                  	=	'" . $prodi_jml_maba_calon . "',                  
					prodi_jml_maba_daftar                 	=	'" . $prodi_jml_maba_daftar . "',                 
					prodi_jml_mhs_lama                    	=	'" . $prodi_jml_mhs_lama . "',                    
					prodi_jml_alumni                      	=	'" . $prodi_jml_alumni . "',                      
					prodi_akred                            	=	'" . $prodi_akred . "',                            
					prodi_akred_sk_nomor                  	=	'" . $prodi_akred_sk_nomor . "',                  
					prodi_akred_sk_terbit                 	=	'" . $prodi_akred_sk_terbit . "',                 
					prodi_akred_sk_expired                	=	'" . $prodi_akred_sk_expired . "',                
					prodi_akred_nilai                     	=	'" . $prodi_akred_nilai . "',                     
					prodi_akred_peringkat                 	=	'" . $prodi_akred_peringkat . "'					
					where is_alih_jenis = '" . $is_alih_jenis . "' and id_program_studi = '" . $id_program_studi . "' and id_semester = '5'

		");
		
		echo "Data 'Spesifikasi Prodi' berhasil diperbarui";
		
?>