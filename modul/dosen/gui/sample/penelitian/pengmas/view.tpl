<div class="center_title_bar">Pengabdian Masyarakat</div>

<a class="disable-ajax" href="http://penelitian.unair.ac.id/login.php?usr={$dosen_amerta.id_peg}&amp;oto={$dosen_amerta.otoakses}" target="_blank">Tambah Artikel (Web Penelitian)</a>

<table>
    <tr>
        <th>No</th>
        <th>Judul</th>
        <th>Tahun</th>
		<th>Link</th>
    </tr>
    {foreach $penelitian_set as $p}
    <tr {if $p@index is not div by 2}class="alternate"{/if}>
        <td class="center">{$p@index + 1}</td>
        <td>{$p.judul}</td>
        <td class="center">{$p.tahun}</td>
		<td>{$p.link}</td>
    </tr>
    {/foreach}
</table>