<div class="center_title_bar">Proposal Penelitian</div>

{if $result}<h2>{$result}</h2>{/if}

<table>
    <tr>
        <th>No</th>
        <th>Judul</th>
        <th>Jenis</th>
        <th>Tahun</th>
        <th>Status</th>
        <th width="100px"></th>
        <th width="150px">Aksi</th>
    </tr>
    {foreach $penelitian_set as $p}
    <tr id="r{$p.ID_PENELITIAN}" {if $p@index is not div by 2}class="alternate"{/if}>
        <td class="center">{$p@index + 1}</td>
        <td><a href="penelitian-proposal.php?mode=preview&id_penelitian={$p.ID_PENELITIAN}">{$p.JUDUL}</a></td>
        <td>{$p.NAMA_JENIS}</td>
        <td class="center">{$p.TAHUN}</td>
        <td class="center">
            {if $p.TGL_SUBMIT == ''}
                <span>Draft</span>
            {else}
                {if $p.TGL_APPROVE_DEPARTEMEN != '' && $p.TGL_APPROVE_LPPM != ''}
                    <span>Disetujui</span>
                {else if $p.TGL_APPROVE_DEPARTEMEN != ''}
                    <span>Menunggu approval LPPM</span>
                {else}
                    <span>Menunggu approval Departemen</span>
                {/if}
            {/if}
        </td>
        <td class="center">
            <a href="penelitian-proposal.php?mode=view-anggota&id_penelitian={$p.ID_PENELITIAN}">Anggota</a> | 
            <a href="penelitian-proposal.php?mode=view-biaya&id_penelitian={$p.ID_PENELITIAN}">Biaya</a>
        </td>
        <td class="center">
            {if $p.TGL_APPROVE_DEPARTEMEN != '' && $p.TGL_APPROVE_LPPM != ''}
                <a href="penelitian-proposal.php?mode=get-pdf&id_penelitian={$p.ID_PENELITIAN}" class="disable-ajax" target="_blank">Download Bukti Registrasi</a>
            {else}
                <a href="penelitian-proposal.php?mode=edit&id_penelitian={$p.ID_PENELITIAN}">Edit</a> |
                <span class="link" id-penelitian="{$p.ID_PENELITIAN}" action="delete">Hapus</span>
                {if $p.TGL_SUBMIT == ''}
                    | <span class="link" id-penelitian="{$p.ID_PENELITIAN}" action="submit">Submit</a>
                {/if}
            {/if}
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="7" class="center">
            <a href="penelitian-proposal.php?mode=add">Tambah</a>
        </td>
    </tr>
</table>
    
<script>$.getScript('gui/sample/penelitian/proposal/view.js');</script>