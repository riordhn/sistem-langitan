<div class="center_title_bar">Penelitian - Daftar Anggota</div>

<h3>JUDUL PENELITIAN : {$penelitian.JUDUL}</h3>

<a href="penelitian-proposal.php">Kembali ke Daftar Penelitian</a>

<table id="table-anggota">
    <tr>
        <th>No</th>
        <th>NIP/NIK/NIM</th>
        <th>Nama</th>
        <th>Gelar Depan</th>
        <th>Gelar Belakang</th>
        <th>Aksi</th>
    </tr>
    {foreach $penelitian_anggota_set as $pa}
    <tr>
        <td>{$pa@index + 1}</td>
        {if $pa.ASAL_ANGGOTA == 2}
            <td>{$pa.NAMA1}</td>
            <td>{$pa.NIP1}</td>
            <td>{$pa.GELAR_DEPAN_1}</td>
            <td>{$pa.GELAR_BELAKANG_1}</td>
        {else}
            <td>{$pa.NAMA2}</td>
            <td>{$pa.NIP2}</td>
            <td>{$pa.GELAR_DEPAN_2}</td>
            <td>{$pa.GELAR_BELAKANG_2}</td>
        {/if}
        <td><button id-anggota="{$pa.ID_ANGGOTA}" action="delete">Hapus</button></td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="6" class="center">
            <button onclick="window.location = '#pen-penelitian!penelitian-proposal.php?mode=add-anggota&id_penelitian={$smarty.get.id_penelitian}';return false;">Tambah</button>
        </td>
    </tr>
</table>

<div id="dialog" style="display: none"><h2>Apakah anggota akan dihapus ?</h2></div>
<script>$.getScript('gui/sample/penelitian/proposal/view-anggota.js')</script>