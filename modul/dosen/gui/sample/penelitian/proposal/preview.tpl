<div class="center_title_bar">Penelitian - Lihat</div>

<a href="penelitian-proposal.php">Kembali</a>

<table>
    <tr>
        <th colspan="2">Penelitian</th>
    </tr>
    <tr>
        <td>Judul</td>
        <td>{$p.JUDUL}</td>
    </tr>
    <tr>
        <td>Jenis Penelitian</td>
        <td>{$p.NM_PENELITIAN_JENIS}</td>
    </tr>
    {if $p.ID_PENELITIAN_JENIS == 3}
    <tr id="row-kerjasama" style="display: none">
        <td>Nama Institusi (Kerjasama)</td>
        <td><input type="text" name="nama_institusi" size="50" maxlength="100" /></td>
    </tr>
    {/if}
    {if $p.ID_PENELITIAN_JENIS == 1}
    <tr id="row-skim" style="display: none">
        <td>SKIM (Dikti)</td>
        <td>
            <select name="id_penelitian_skim" id="penelitian-skim">
                <option value="">--</option>
            {foreach $penelitian_skim_set as $ps}
                <option value="{$ps.ID_PENELITIAN_SKIM}" {if $ps.ID_PENELITIAN_SKIM == $p.ID_PENELITIAN_SKIM}selected="selected"{/if}>[{$ps.KODE_SKIM}] {$ps.NAMA_SKIM}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    {/if}
    <tr>
        <td>Bidang / Tema Penelitian</td>
        <td>
            {if $p.NM_PENELITIAN_BIDANG != 'Lainnya'}
                {$p.NM_PENELITIAN_BIDANG}
            {else}
                {$p.PENELITIAN_BIDANG_LAIN}
            {/if}
        </td>
    </tr>
    <tr>
        <td>File Proposal</td>
        <td>
        {if $p.NM_FILE != ''}<a href="{$base_url}files/upload/dosen/penelitian/{$p.NM_FILE}" class="disable-ajax" target="_blank">{$p.NM_ASLI_FILE}</a>{/if}
        </td>
    </tr>
    <tr>
        <th colspan="2">Deskripsi Penelitian</th>
    </tr>
    <tr>
        <td style="width: 20%">Lokasi Penelitian</td>
        <td>{$p.LOKASI}</td>
    </tr>
    <tr>
        <td>Jangka Waktu Penelitian</td>
        <td>{$p.JANGKA_WAKTU} Tahun</td>
    </tr>
    <tr>
        <td>Penelitian Tahun Ke</td>
        <td>{$p.JANGKA_WAKTU_KE}</td>
    </tr>
    <tr>
        <td>Jumlah Anggota</td>
        <td>{$p.JUMLAH_ANGGOTA}</td>
    </tr>
    <tr>
        <td>Pembiayaan</td>
        <td>Rp {number_format($p.TOTAL_BIAYA)}</td>
    </tr>
    <tr>
        <th colspan="2">Ketua Tim Peneliti Mitra (TPM)</th>
    </tr>
    <tr>
        <td>Nama</td>
        <td>{$p.TPM_NAMA}</td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>{if $p.TPM_JENIS_KELAMIN == 'L'}Laki-Laki{/if}
            {if $p.TPM_JENIS_KELAMIN == 'P'}Perempuan{/if}
        </td>
    </tr>
    <tr>
        <td>NIP</td>
        <td>{$p.TPM_NIP}</td>
    </tr>
    <tr>
        <td>Golongan</td>
        <td>{foreach $golongan_set as $g}
                {if $g.ID_GOLONGAN == $p.TPM_ID_GOLONGAN}{$g.NM_GOLONGAN}{/if}
            {/foreach}
        </td>
    </tr>
    <tr>
        <td>Jabatan Fungsional</td>
        <td>{$p.TPM_JABATAN_FUNGSIONAL}</td>
    <tr>
    </tr>
        <td>Jabatan Struktural</td>
        <td>{$p.TPM_JABATAN_STRUKTURAL}</td>
    </tr>
    <tr>
        <td>Perguruan Tinggi</td>
        <td>{$p.TPM_PT}</td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>{$p.TPM_FAKULTAS}</td>
    </tr>
    <tr>
        <td>Jurusan</td>
        <td>{$p.TPM_PRODI}</td>
    </tr>
    <tr>
        <td>Alamat Kantor</td>
        <td>
            {$p.TPM_ALAMAT_KANTOR}<br/>
            {if $p.TPM_TELP_KANTOR != ''}Telp : {$p.TPM_TELP_KANTOR}{/if}
            {if $p.TPM_FAX_KANTOR != ''}Fax : {$p.TPM_FAX_KANTOR}{/if}
        </td>
    </tr>
    <tr>
        <td>Alamat Rumah</td>
        <td>{$p.TPM_ALAMAT_RUMAH}</td>
    </tr>
    <tr>
        <td>Telepon Rumah</td>
        <td>{$p.TPM_TELP_RUMAH}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{$p.TPM_EMAIL}</td>
    </tr>
    <tr>
        <th colspan="2">Dekan</th>
    </tr>
    <tr>
        <td>NIP Dekan</td>
        <td>{$p.NIP_DEKAN}</td>
    </tr>
    <tr>
        <td>Nama Dekan</td>
        <td>{$p.NM_DEKAN}</td>
    </tr>
    <tr>
        <th colspan="2">Ketua LPPM</th>
    </tr>
    <tr>
        <td>NIP Ketua LPPM</td>
        <td>{$p.NIP_KETUA_LPPM}</td>
    </tr>
    <tr>
        <td>Nama Ketua LPPM</td>
        <td>{$p.NM_KETUA_LPPM}</td>
    </tr>
    <tr>
        <th colspan="2">Data Pengisian</th>
    </tr>
    <tr>
        <td width="20%"><strong>Kota</strong></td>
        <td>{$p.KOTA}</td>
    </tr>
    <tr>
        <td><strong>Tanggal Pengisian</strong></td>
        <td>
            {$p.TGL_PENELITIAN|date_format:"%d %B %Y"}
        </td>
    </tr>
</table>