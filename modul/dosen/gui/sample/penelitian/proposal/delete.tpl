<div class="center_title_bar">Proposal &gt; Hapus Proposal</div>

<h2>Apakah proposal ini akan di hapus ?</h2>
<form action="penelitian-proposal.php?mode=delete" method="post">
	<input type="hidden" name="mode" value="delete" />
	<input type="hidden" name="id_penelitian" value="{$smarty.get.id_penelitian}" />
	<table>
		<tbody>
			<tr>
				<th colspan="2">Proposal Penelitian</th>
			</tr>
			<tr>
				<td>Judul</td>
				<td>{$proposal.JUDUL}</td>
			</tr>
			<tr>
				<td>Tahun</td>
				<td>{$proposal.TAHUN}</td>
			</tr>
			<tr>
				<td colspan="2">
					<a href="penelitian-proposal.php">Kembali</a>
					<input type="submit" value="Hapus" />
				</td>
			</tr>
		</tbody>
	</table>
</form>