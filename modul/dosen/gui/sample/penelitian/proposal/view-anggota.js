$(document).ready(function() {
   
    $('button[action="delete"]').each(function(i, e) {
        $(this).click(function() { 
            var button = this;
            $('#dialog').dialog({
                title: 'Konfirmasi hapus anggota',
                buttons: {
                    Tidak: {
                        text: 'Tidak',
                        click: function() {
                            $('#dialog').dialog('destroy');
                        }
                    },
                    Ya: {
                        text: 'Ya',
                        click: function() { 
                            $('#dialog').dialog('destroy');
                           
                            $.ajax({
                                type: 'POST',
                                url: 'penelitian-proposal.php',
                                data: 'mode=del-anggota&id_anggota=' + $(button).attr('id-anggota'),
                                success: function(r) {
                                    if (r == 1) {
                                        $(button).parent().parent().remove();
                                    }
                                    else {
                                        alert('Gagal hapus');
                                    }
                                }
                            });
                        }
                    }
                }
            });
           
        });
    });
   
});