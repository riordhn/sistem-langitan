<div class="center_title_bar">Penelitian - Tambah Pembiayaan</div>

<form action="penelitian-proposal.php?mode=view-biaya&id_penelitian={$smarty.get.id_penelitian}" method="post">
<input type="hidden" name="mode" value="add-biaya" />
<input type="hidden" name="id_penelitian" value="{$smarty.get.id_penelitian}" />
<table class="ui-widget">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="2">Biaya Penelitian</th>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Tahun Ke</strong></td>
        <td>
            <input type="text" name="tahun_ke" maxlength="2" size="2" />
        </td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Biaya Dikti</strong></td>
        <td>
            <input type="text" name="besar_biaya_dikti" value="0" />
        </td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Biaya Lain</strong></td>
        <td>
            <input type="text" name="besar_biaya_lain" value="0"/>
        </td>
    </tr>
    <tr class="ui-widget-content">
        <td colspan="2" class="center">
            <a href="penelitian-proposal.php?mode=view-biaya&id_penelitian={$smarty.get.id_penelitian}">Kembali</a>
            <input type="submit" value="Simpan" />
        </td>
</table>
</form>
        
<p>Contoh Pengisian :<br/>
    - Rp 1.500.000,00 maka yang di isikan adalah <strong>1500000</strong> (tanpa titik).</br>
    - Rp 2.499.999,99 (mengandung angka desimal), maka yg di isikan adalah <strong>2499999.99</strong> (koma diganti titik).</p>