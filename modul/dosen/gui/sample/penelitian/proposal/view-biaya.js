$(document).ready(function() {
    
    $('button[action="delete"]').each(function(index, dom) {
        $(this).click(function() { 
            var button = this;
            
            $('#dialog').dialog({
                title: 'Konfirmasi hapus biaya',
                buttons: {
                    1: {
                        text: 'Batal',
                        click: function() { $('#dialog').dialog('destroy'); }
                    },
                    2: {
                        text: 'Ya',
                        click: function() {
                            $.ajax({
                                type: 'POST',
                                url: 'penelitian-proposal.php',
                                data: 'mode=del-biaya&id_penelitian=' + $('input[name="id_penelitian"]').val() + '&id_biaya=' + $(button).attr('id-biaya'),
                                success: function(r) {
                                    if (r == 1)
                                        $(button).parent().parent().remove();
                                    else
                                        alert('Gagal dihapus');
                                    $('#dialog').dialog('destroy');
                                }
                            });
                        }
                    }
                }
            });
            
        });
    });
    
});