<html>
    <head>
        <style>body { margin: 0px; padding: 0px; font-family: 'Trebuchet MS'; font-size: 13px; }</style>
    </head>
    <body>
        {if $smarty.server.REQUEST_METHOD == 'GET'}
            <form action="penelitian-proposal.php?mode=upload-file" method="post" enctype="multipart/form-data">
                <input type="file" name="file" size="40" onchange="document.forms[0].submit()" accept="application/pdf" />
                <input type="hidden" name="mode" value="upload-file" />
                <input type="hidden" name="id_penelitian" value="{$smarty.get.id_penelitian}" />
                <input type="hidden" name="token" value="{$smarty.get.token}" />
            </form>
        {else}
            <div style="margin-top: 5px"><a href="{$lokasi_file}" target="_blank">{$nama_file}</a> <button onclick="location.href = '{$smarty.server.HTTP_REFERER}'; return false;">Ganti File</button></div>
        {/if}
    </body>
</html>