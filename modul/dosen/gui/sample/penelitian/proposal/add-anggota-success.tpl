<div class="center_title_bar">Penelitian - Tambah Anggota</div>

<table>
    <tr>
        <th>Informasi</th>
    </tr>
    <tr>
        <td>
            <h1>Anggota penelitian berhasil ditambahkan.</h1>
            <p><a href="penelitian-proposal.php?mode=view-anggota&id_penelitian={$smarty.get.id_penelitian}">Kembali ke daftar anggota</a></p>
            <p><a href="penelitian-proposal.php">Kembali ke daftar penelitian</a></p>
        </td>
    </tr>
</table>