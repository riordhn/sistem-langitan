<div class="center_title_bar">Proposal &gt; Edit Proposal</div>
<p><a href="penelitian-proposal.php">Kembali</a></p>

<form action="penelitian-proposal.php" method="post" id="form">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_penelitian" value="{$proposal.ID_PENELITIAN}" />
    <input type="hidden" name="id_dosen" value="{$proposal.ID_PENELITI}" />
    <input type="hidden" name="token" value="{$token}" />
    <table style="width: 100%">
        <tr>
            <th colspan="2">Info Peneliti</th>
        </tr>
        <tr>
            <td>Nama Peneliti</td>
            <td>
                {$proposal.NAMA_PENELITI}
                <input type="hidden" name="peneliti" size="80" value="{$proposal.NAMA_PENELITI}"/>
            </td>
        </tr>
        <tr>
            <td>NIP Peneliti</td>
            <td id="nip_peneliti">{$proposal.NIP_PENELITI}</td>
        </tr>
        <tr>
            <td>Asal Peneliti</td>
            <td id="asal_peneliti">{$proposal.ASAL_PENELITI}</td>
        </tr>
        <tr>
            <th colspan="2">Proposal Penelitian</th>
        </tr>
        <tr>
            <td>File Proposal</td>
            <td style="vertical-align: top">
			{if $proposal.NAMA_FILE != ''}<a href="../../files/upload/dosen/proposal/{$proposal.MD5_FILE}.pdf" target="_blank" class="disable-ajax" id="file-link">{$proposal.NAMA_FILE}</a> <span id="file-link-remove" onclick="deleteFile({$proposal.ID_PENELITIAN});return;" style="cursor: pointer">[X]</span><br/>{/if}
                <iframe src="penelitian-proposal.php?mode=upload-file&token={$token}&id_penelitian={$proposal.ID_PENELITIAN}" height="30px" width="350px" scrolling="no" style="border: 0px solid #000;overflow-y: hidden"></iframe>
            </td>
        </tr>
        <tr>
            <td>Judul</td>
            <td><textarea cols="75" rows="3" name="judul">{$proposal.JUDUL}</textarea></td>
        </tr>
        <tr>
            <td>Jenis Penelitian</td>
            <td><select name="id_penelitian_jenis" id="penelitian-jenis">
                    <option value="">--</option>
                {foreach $penelitian_jenis_set as $pj}
                    <option value="{$pj.ID_PENELITIAN_JENIS}"
                            {if $pj.ID_PENELITIAN_JENIS == $proposal.ID_PENELITIAN_JENIS}selected="selected"{/if}>{$pj.NAMA_JENIS}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr id="row-kerjasama" {if $proposal.ID_PENELITIAN_JENIS != 3}style="display:none"{/if}>
            <td>Nama Institusi (Kerjasama)</td>
            <td><input type="text" name="nama_institusi" size="50" maxlength="100" /></td>
        </tr>
        
        <tr id="row-skim" {if empty($penelitian_skim_set)}style="display: none"{/if}>
            <td>SKIM (Dikti)</td>
            <td>
                <select name="id_penelitian_skim" id="penelitian-skim">
                    <option value="">--</option>
                {foreach $penelitian_skim_set as $ps}
                    <option value="{$ps.ID_PENELITIAN_SKIM}" {if $ps.ID_PENELITIAN_SKIM == $proposal.ID_PENELITIAN_SKIM}selected="selected"{/if}>{if $ps.KODE_SKIM}[{$ps.KODE_SKIM}] {/if}{$ps.NAMA_SKIM}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        
        <tr>
            <td>Bidang / Tema Penelitian</td>
            <td>
                <select name="id_penelitian_bidang" id="penelitian-bidang">
                {foreach $penelitian_bidang_set as $pb}
                    <option value="{$pb.ID_PENELITIAN_BIDANG}" lainnya="{$pb.LAINNYA}" {if $pb.ID_PENELITIAN_BIDANG == $proposal.ID_PENELITIAN_BIDANG}selected="selected"{/if}>{$pb.NAMA_BIDANG} / {$pb.NAMA_TEMA}</option>
                {/foreach}
                </select>
				Lainnya : <input type="text" name="penelitian_bidang_lain" size="75" maxlength="100" value="{$proposal.PENELITIAN_BIDANG_LAIN}" />
            </td>
        </tr>
        <tr>
            <th colspan="2">Deskripsi</th>
        </tr>
		<tr>
            <td style="width: 20%">Lokasi Penelitian</td>
            <td><input type="text" name="lokasi" maxlength="50" size="30" value="{$proposal.LOKASI}" /></td>
        </tr>
        <tr>
            <td>Tahun</td>
            <td><input type="text" name="tahun" maxlength="5" size="4" value="{$proposal.TAHUN}"/></td>
        </tr>
        <tr>
            <td>Jangka Waktu Penelitian</td>
            <td><input type="text" name="jangka_waktu" maxlength="5" size="4" value="{$proposal.JANGKA_WAKTU}"/> Tahun</td>
        </tr>
        <tr>
            <td>Penelitian Tahun Ke</td>
            <td><input type="text" name="jangka_waktu_ke" maxlength="5" size="4" value="{$proposal.JANGKA_WAKTU_KE}"/></td>
        </tr>
        <tr>
            <td>Anggota Penelitian</td>
            <td>
                <table class="sub-table">
					<tbody>
						<tr>
							<th>No</th>
							<th>NIP / NIM</th>
							<th>Nama</th>
							<th>G. Depan</th>
							<th>G. Belakang</th>
						</tr>
						{for $iAnggota=1 to 6}
						<tr>
							<td class="center">{$iAnggota}<input type="hidden" name="id_anggota[]" value="{$proposal.anggota[$iAnggota - 1].ID_ANGGOTA}" /></td>
							<td><input type="text" name="nip_anggota[]" value="{$proposal.anggota[$iAnggota - 1].NIP_ANGGOTA}"/></td>
							<td><input type="text" name="nama_anggota[]" value="{$proposal.anggota[$iAnggota - 1].NAMA_ANGGOTA}"/></td>
							<td><input type="text" name="gelar_depan_anggota[]" size="6" value="{$proposal.anggota[$iAnggota - 1].GELAR_DEPAN}"/></td>
							<td><input type="text" name="gelar_belakang_anggota[]" size="6" value="{$proposal.anggota[$iAnggota - 1].GELAR_BELAKANG}"/></td>
						</tr>
						{/for}
					</tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>Pembiayaan</td>
            <td>
				<table class="sub-table">
					<tbody>
						<tr>
							<th>Tahun Ke</th>
							<th>Biaya dari DIKTI</th>
							<th>Biaya Lain</th>
							<th>Biaya dari PT</th>
						</tr>
						{for $iBiaya=1 to 5}
						<tr>
							<td class="center">{$iBiaya}<input type="hidden" name="id_biaya[]" value="{$proposal.biaya[$iBiaya - 1].ID_BIAYA}" /></td>
							<td><input type="text" name="besar_biaya_dikti[]" size="15" value="{$proposal.biaya[$iBiaya - 1].BESAR_BIAYA_DIKTI}"/></td>
							<td><input type="text" name="besar_biaya_lain[]" size="15" value="{$proposal.biaya[$iBiaya - 1].BESAR_BIAYA_LAIN}"/></td>
							<td><input type="text" name="besar_biaya_pt[]" size="15" value="{$proposal.biaya[$iBiaya - 1].BESAR_BIAYA_PT}"/></td>
						</tr>
						{/for}
					</tbody>
				</table>
			</td>
        </tr>
		<tr>
			<td>Mitra</td>
			<td>
				<table>
					<tr>
						<th colspan="2">Ketua Tim Peneliti Mitra (TPM)</th>
					</tr>
					<tr>
						<td>Nama</td>
						<td><input type="text" name="nama_mitra" maxlength="100" size="30" value="{$proposal.NAMA_MITRA}" /></td>
					</tr>
					<tr>
						<td>NIP</td>
						<td><input type="text" name="nip_mitra" maxlength="20" value="{$proposal.NIP_MITRA}"/></td>
					</tr>
					<tr>
						<td>Golongan</td>
						<td><input type="text" name="golongan_mitra" maxlength="10" value="{$proposal.GOLONGAN_MITRA}" /></td>
					</tr>
					<tr>
						<td>Jabatan Fungsional</td>
						<td><input type="text" name="jabatan_fungsional_mitra" size="30" maxlength="50" value="{$proposal.JABATAN_FUNGSIONAL_MITRA}"/></td>
					</tr>
					<tr>
						<td>Jabatan Struktural</td>
						<td><input type="text" name="jabatan_struktural_mitra" size="30" maxlength="50" value="{$proposal.JABATAN_STRUKTURAL_MITRA}"/></td>
					</tr>
					<tr>
						<td>Perguruan Tinggi</td>
						<td><input type="text" name="pt_mitra" size="50" maxlength="100" value="{$proposal.PT_MITRA}"/></td>
					</tr>
					<tr>
						<td>Fakultas</td>
						<td><input type="text" name="fakultas_mitra" size="30" maxlength="100" value="{$proposal.FAKULTAS_MITRA}"/></td>
					</tr>
					<tr>
						<td>Jurusan</td>
						<td><input type="text" name="prodi_mitra" size="30" maxlength="100" value="{$proposal.PRODI_MITRA}"/></td>
					</tr>
					<tr>
						<td>Alamat Kantor</td>
						<td><textarea name="alamat_kantor_mitra" rows="2" cols="50" style="width: auto;">{$proposal.ALAMAT_KANTOR_MITRA}</textarea></td>
					</tr>
					<tr>
						<td>Telp : </td>
						<td><input type="text" name="telp_kantor_mitra" size="20" maxlength="32" value="{$proposal.TELP_KANTOR_MITRA}"/></td>
					</tr>
					<tr>
						<td>Fax : </td>
						<td><input type="text" name="fax_kantor_mitra" size="20" maxlength="32" value="{$proposal.FAX_KANTOR_MITRA}"/></td>
					</tr>
					<tr>
						<td>Alamat Rumah</td>
						<td>
							<textarea name="alamat_rumah_mitra" rows="2" cols="50" style="width: auto;" maxlength="256">{$proposal.ALAMAT_RUMAH_MITRA}</textarea><br/>
						</td>
					</tr>
					<tr>
						<td>Telepon Rumah</td>
						<td><input type="text" name="telp_rumah_mitra" size="20" value="{$proposal.TELP_RUMAH_MITRA}"/></td>
					</tr>
					<tr>
						<td>Email</td>
						<td><input type="text" name="email_mitra" size="30" value="{$proposal.EMAIL_MITRA}"/></td>
					</tr>
				</table>
			</td>
		</tr>
        <tr>
            <th colspan="2">Dekan</th>
        </tr>
        <tr>
            <td>NIP Dekan</td>
            <td id="nip_dekan">{$proposal.NIP_DEKAN}</td>
        </tr>
        <tr>
            <td>Nama Dekan</td>
            <td id="nm_dekan">{$proposal.NAMA_DEKAN}</td>
        </tr>
        <tr>
            <th colspan="2">Ketua LPPM</th>
        </tr>
        <tr>
            <td>NIP Ketua LPPM</td>
            <td>{$lppm.USERNAME}</td>
        </tr>
        <tr>
            <td>Nama Ketua LPPM</td>
            <td>{$lppm.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <th colspan="2">Data Pengisian</th>
        </tr>
        <tr>
            <td>Kota</td>
            <td><input type="text" name="kota" value="{$proposal.KOTA}" /></td>
        </tr>
        <tr>
            <td>Tanggal Pengisian</td>
            <td>
                {html_select_date prefix="tgl_input_" start_year="-50" field_order="DMY" time=$proposal.TGL_INPUT}
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <a href="penelitian-proposal.php">Batal</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>

<div id="dialog"></div>

<script type="text/javascript">
$(document).ready(function() {

    // jenis penelitian
    $('#penelitian-jenis').change(function() {
        
        // Khusus kerjasama
        if (this.value == 3)
            $('#row-kerjasama').show();
        else
            $('#row-kerjasama').hide();
        
        // SKIM Penelitian
        $.ajax({
            type: 'GET',
            url: 'penelitian-proposal.php',
            data: 'mode=get-penelitian-skim&id_penelitian_jenis='+this.value,
            beforeSend: function() { $('#penelitian-skim').html(''); },
            success: function(data) {
                if (data != '0')
                {
                    $('#row-skim').show();    
                    $('#penelitian-skim').html(data);
                }
                else
                {
                    $('#row-skim').hide();
                }
            }
        });

        // Bidang / Tema Penelitian
        $.ajax({
            type: 'GET',
            url: 'penelitian-proposal.php',
            data: 'mode=get-penelitian-bidang&id_penelitian_jenis='+this.value,
            beforeSend: function() { $('#penelitian-bidang').html('<option>--</option>'); },
            success: function(data) { $('#penelitian-bidang').html(data); $('#penelitian-bidang').change(); }
        });
    });
    
    // Validasi form
    $('#form').validate({
        rules: {
            id_dosen:               { required: true },
            judul:                  { required: true },
            id_penelitian_jenis:    { required: true },
            id_penelitian_bidang:   { required: true },
            lokasi:                 { required: true },
			tahun:					{ required: true, number: true },
            jangka_waktu:           { required: true, number: true },
            jangka_waktu_ke:        { required: true, number: true },
            kota:                   { required: true }
        },
        messages: {
            judul:                  { required: 'Judul penelitian harus di isi' },
            id_penelitian_jenis:    { required: 'Jenis penelitian harus di isi' },
            id_penelitian_bidang:   { required: 'Bidang penelitian harus di isi' },
            lokasi:                 { required: 'Lokasi penelitian harus di isi' },
			tahun:					{ required: 'Tahun penelitian harus di isi', number: 'Format isian harus angka' },
            jangka_waktu:           { required: 'Jangka waktu penelitian harus di isi', number: 'Format isian harus angka' },
            jangka_waktu_ke:        { required: 'Jangka waktu ke penelitian harus di isi', number: 'Format isian harus angka' },
            kota:                   { required: 'Kota pengisian form' }
        },
        ignore: '.ignore',
        errorPlacement: function(error, element) { error.appendTo(element.parent()); }
    });
	
});

function deleteFile(id_penelitian)
{
	if (confirm('File akan di hapus ?'))
	{
		$.post('penelitian-proposal.php', 'mode=delete-file&id_penelitian='+id_penelitian, function(r) {
			console.log(r);
			$('#file-link').remove();
			$('#file-link-remove').remove();
		});
	}
}
</script>