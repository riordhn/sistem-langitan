<div class="center_title_bar">Proposal Penelitian</div>

<table class="ui-widget">
    <tbody>
		<tr class="ui-widget-header">
			<th class="header-coloumn" colspan="6">Proposal Penelitian</th>
		</tr>
		<tr class="ui-widget-content">
			<th width="20px">No</th>
			<th>Judul</th>
			<th>Jenis</th>
			<th>Tahun</th>
			<th>Status</th>
			<th class="center" width="80px">Operasi</th>
		</tr>
		{foreach $proposal_set as $proposal}
			<tr class="ui-widget-content">
				<td class="center">{$proposal@index + 1}</td>
				<td>{$proposal.JUDUL}</td>
				<td>{$proposal.NAMA_JENIS}</td>
				<td class="center">{$proposal.TAHUN}</td>
				<td class="center">-</td>
				<td>
					<a href="penelitian-proposal.php?mode=edit&id_penelitian={$proposal.ID_PENELITIAN}">Edit</a> |
					<a href="penelitian-proposal.php?mode=delete&id_penelitian={$proposal.ID_PENELITIAN}">Hapus</a>
				</td>
			</tr>
		{/foreach}
		<tr class="ui-widget-content">
			<td class="center" colspan="6">
				<a href="penelitian-proposal.php?mode=add" class="ui-button ui-state-default ui-corner-all" style="cursor: pointer;padding:3px;">Tambah</a>
			</td>
		</tr>
	</tbody>
</table>