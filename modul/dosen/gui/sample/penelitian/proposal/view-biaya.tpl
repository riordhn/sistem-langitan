<div class="center_title_bar">Penelitian - Detail Pembiayaan</div>

<h3>JUDUL PENELITIAN : {$penelitian.JUDUL}</h3>

{if $result}<h2>{$result}</h2>{/if}

<a href="penelitian-proposal.php">Kembali ke Daftar Penelitian</a>

<table>
    <tr>
        <th>Pembiayaan Tahun Ke</th>
        <th>Besar Biaya DIKTI</th>
        <th>Besar Biaya Lain</th>
        <th>Aksi</th>
    </tr>
    {foreach $penelitian_biaya_set as $pb}
    <tr>
        <td class="center">{$pb.TAHUN_KE}</td>
        <td class="right">Rp {number_format($pb.BESAR_BIAYA_DIKTI)}</td>
        <td class="right">Rp {number_format($pb.BESAR_BIAYA_LAIN)}</td>
        <td>
            <button id-biaya="{$pb.ID_BIAYA}" action="delete">Hapus</button>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="4">
            <a href="penelitian-proposal.php?mode=add-biaya&id_penelitian={$smarty.get.id_penelitian}">Tambah Biaya</a>
        </td>
    </tr>
</table>

<input type="hidden" name="id_penelitian" value="{$smarty.get.id_penelitian}" />
<div id="dialog" style="display: none">
    <h2>Apakah biaya penelitian akan dihapus ? </h2>
</div>
<script>$.getScript('gui/sample/penelitian/proposal/view-biaya.js');</script>