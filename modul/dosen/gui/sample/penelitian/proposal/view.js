$(document).ready(function() {
    
    var dialog = document.createElement('div');
    
    $('span[action="delete"]').each(function(i, e) {
        $(this).click(function() {
            var button = this;
            $(dialog).html('<h2>Apakah proposal penelitian akan dihapus ?</h2>');
            $(dialog).dialog({
                title: 'Konfirmasi penghapusan',
                modal: true,
                buttons: {
                    1: {
                        text: 'Batal',
                        click: function() { $(dialog).dialog('destroy'); }
                    },
                    2: {
                        text: 'Ya',
                        click: function() {
                            $.ajax({
                                type: 'POST',
                                url: 'penelitian-proposal.php',
                                data: 'mode=del-penelitian&id_penelitian=' + $(button).attr('id-penelitian'),
                                success: function(r) {
                                    $(dialog).dialog('destroy');
                                    location.reload();
                                }
                            });
                        }
                    }
                }
            });
            
        });
    });
    
    $('span[action="submit"]').each(function(i, e) {
        $(this).click(function() {
            var button = this;
            $(dialog).html('<h2>Apakah proposal penelitian akan disubmit untuk proses approval ?</h2>');
            $(dialog).dialog({
                title: 'Konfirmasi submit',
                modal: true,
                buttons: {
                    1: {
                        text: 'Batal',
                        click: function() { $(dialog).dialog('destroy'); }
                    },
                    2: {
                        text: 'Ya',
                        click: function() {
                            $.ajax({
                                type: 'POST',
                                url: 'penelitian-proposal.php',
                                data: 'mode=submit-penelitian&id_penelitian=' + $(button).attr('id-penelitian'),
                                success: function(r) {
                                    $(dialog).dialog('destroy');
                                    location.reload();
                                }
                            });
                        }
                    }
                }
            });
        });
    });
    
});