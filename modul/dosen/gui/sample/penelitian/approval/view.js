$(document).ready(function() {
    
    // handle object
    var dialog = document.createElement('div');
    
    // button approve
    $('span[action="approve"]').each(function(i, e) {
        $(this).click(function() {
            
            var button = this;
            
            $(dialog).html('<h2>Apakah proposal penelitian ini akan di setujui ?</h2>');
            
            $(dialog).dialog({
                title: 'Approval',
                modal: true,
                buttons: {
                    1: {
                        text: 'Batal',
                        click: function() { $(dialog).dialog('destroy'); }
                    },
                    2: {
                        text: 'Ya',
                        click: function() {
                            
                            $.ajax({
                                type: 'POST',
                                url: 'penelitian-approval.php',
                                data: 'mode=approve&id_penelitian=' + $(button).attr('id-penelitian'),
                                success: function(r) {
                                    if (r == 1)
                                        alert('Proposal penelitian berhasil disetujui');
                                    else
                                        alert('Gagal Approve');
                                    window.location.reload();
                                }
                            });
                            
                        }
                    }
                }
            });
            
        });
    });
    
    // button reject
    $('span[action="reject"]').each(function(i, e) {
        $(this).click(function() {
            
            var button = this;
            
            $(dialog).html('<h2>Apakah proposal penelitian ini ditolak?</h2>');
            
            $(dialog).dialog({
                title: 'Approval',
                modal: true,
                buttons: {
                    1: {
                        text: 'Batal',
                        click: function() { $(dialog).dialog('destroy'); }
                    },
                    2: {
                        text: 'Ya',
                        click: function() {
                            
                            $.ajax({
                                type: 'POST',
                                url: 'penelitian-approval.php',
                                data: 'mode=reject&id_penelitian=' + $(button).attr('id-penelitian'),
                                success: function(r) {
                                    if (r == 1)
                                        alert('Proposal penelitian berhasil ditolak');
                                    else
                                        alert('Gagal update');
                                    window.location.reload();
                                }
                            });
                            
                        }
                    }
                }
            });
            
        });
    });
    
});