<div class="center_title_bar">Approval Proposal Penelitian</div>

<table>
    <tr>
        <th>No</th>
        <th>Judul</th>
        <th>Jenis</th>
        <th>Tahun</th>
        <th>Peneliti</th>
        <th width="100px">Aksi</th>
    </tr>
    {foreach $penelitian_set as $p}
    <tr id="r{$p.ID_PENELITIAN}" {if $p@index is not div by 2}class="alternate"{/if}>
        <td class="center">{$p@index + 1}</td>
        <td><a href="penelitian-approval.php?mode=preview&id_penelitian={$p.ID_PENELITIAN}">{$p.JUDUL}</a></td>
        <td>{$p.NAMA_JENIS}</td>
        <td class="center">{$p.TAHUN}</td>
        <td>{$p.NM_PENELITI}</td>
        <td class="center">
            <span class="link" action="approve" id-penelitian="{$p.ID_PENELITIAN}">Setujui</span> |
            <span class="link" action="reject" id-penelitian="{$p.ID_PENELITIAN}">Ditolak</span>
        </td>
    </tr>
    {/foreach}
</table>

<script>$.getScript('gui/sample/penelitian/approval/view.js');</script>