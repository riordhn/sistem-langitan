<div class="center_title_bar">{$TITLE}</div> 
<table class="ui-widget">
    <tr class="ui-widget-header">
        <th colspan="4" class="header-coloumn"><h2>Detail Dosen</h2></th>
	</tr>
    <tr class="ui-widget-content">
        <td><span class="field">Nama</span> </td>
        <td><span>{$nama|upper}</span></td>
        <td><span class="field">Waktu</span></td>
        <td><span>Semester Ganjil Tahun 2010/2011</span></td>
    </tr>
    <tr class="ui-widget-content">
        <td><span class="field">Program Studi</span></td>
        <td><span>{$prodi|upper}</span></td>
	    <td><span class="field">Fakultas</span></td>
        <td><span>{$fakultas|upper}</span></td>
	</tr>
</table>

	<div class="overflow_content">
	{$isi_data}
	</div>        
