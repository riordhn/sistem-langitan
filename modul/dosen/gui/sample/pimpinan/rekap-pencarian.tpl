<div class="center_title_bar">Informasi Status Mahasiswa</div>

{literal}
    <style>
        .ui-widget tr td { padding: 2px; vertical-align: middle; }
        tr.back-yellow { background: #ff0; }
        tr.back-green { background: #8f8; }
        .ui-autocomplete {
            max-height: 200px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
            width: 700px;
        }
        /* IE 6 doesn't support max-height
        * we use height instead, but this forces the menu to always be this tall
        */
        * html .ui-autocomplete {
            height: 200px;
        }

    </style>
{/literal}
<table>
    <tr class="ui-widget-header">
        <th colspan="2" class="header-coloumn" style="font-size: 13pt">Cari Langsung</th>
    </tr>
    <tr class="ui-widget-content">
        <td>Berdasarkan Nama/NIM</td>
        <td>
            <input type="text" size="30" name="id_mhs" id="nama_nim" />
        </td>   
    </tr>
</table>
<span style="margin-left: 10px;padding:5px" id="button_advance" class="ui-button ui-state-default ui-corner-all" >Pencarian Selengkapnya</span>
<form style="display: none" id="advance_search" action="rekap-pencarian.php" method="get">
    <table class=" ui-widget-content" style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="font-size: 13pt">Filter Pencarian Berdasarkan Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama</td>
            <td>
                <input type="text" size="30" {if $smarty.get.nama!=''} value="{$smarty.get.nama}"{/if}  name="nama" />
            </td>   
        </tr>
        <tr>
            <td style="width: 20%">Fakultas</td>
            <td>
                <select id="select_fakultas" name="id_fakultas">
                    <option value="">Pilih</option>
                    {foreach $fakultas_set as $f}
                        <option value="{$f.ID_FAKULTAS}" {if $smarty.get.id_fakultas == $f.ID_FAKULTAS}selected="selected"{/if}>{$f.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>
                <select id="select_prodi" name="id_program_studi">
                    <option value="">Pilih</option>
                    {foreach $program_studi_set as $ps}
                        <option value="{$ps.ID_PROGRAM_STUDI}" {if $smarty.get.id_program_studi==$ps.ID_PROGRAM_STUDI}selected="true"{/if}>({$ps.NM_JENJANG}) {$ps.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Angkatan</td>
            <td>
                <select name="thn_angkatan_mhs">
                    <option value="">Pilih</option>
                    {foreach $tahun_set as $t}
                        <option value="{$t.THN_ANGKATAN_MHS}" {if $thn_angkatan_mhs == $t.THN_ANGKATAN_MHS}selected="selected"{/if}>{$t.THN_ANGKATAN_MHS}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Kota Domisili</td>
            <td><input type="text" id="kota" {if $smarty.get.kota!=''} value="{$smarty.get.kota}"{/if} name="kota" size="50"/></td>
        </tr>
        <tr>
            <td>SMA Asal</td>
            <td><input type="text" id="sma" {if $smarty.get.sma!=''} value="{$smarty.get.sma}"{/if}  name="sma" size="50"/></td>
        </tr>
        <tr>
            <td>No HP</td>
            <td><input type="text" name="hp" {if $smarty.get.hp!=''} value="{$smarty.get.hp}"{/if}   size="50"/></td>
        </tr>
        <tr>
            <td>Penghasilan Orang Tua</td>
            <td>
                Batas Bawah : <input type="text" {if $smarty.get.range_bawah!=''} value="{$smarty.get.range_bawah}"{/if}  name="range_bawah" size="25"/> 
                Batas Atas : <input type="text"  {if $smarty.get.range_atas!=''} value="{$smarty.get.range_atas}"{/if} name="range_atas" size="25"/> <br/>
                <i><b style="color: green">Isilah dengan angka</b></i>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <i><b style="color: green">Kosongi parameter jika tidak memakai parameter tersebut</b></i>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="advance-search" />
                <input type="submit" style="padding:5px" class="ui-button ui-state-default ui-corner-all" value="Lihat" />
            </td>
        </tr>
    </table>
</form>


{if !empty($smarty.get.mode)}
    <table class="ui-widget-content" width="100%">
        <tbody>
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="4" style="font-size: 13pt">Data Status Mahasiswa</th>
            </tr>
            <tr>
                <th class="center">
                    NO
                </th>
                <th class="center">
                    NIM
                </th>
                <th class="center">
                    NAMA
                </th>
                <th class="center">
                    STATUS
                </th>
            </tr>
            {foreach $mahasiswa_set as $m}
                <tr class="ui-widget-content
                    {if $m.STATUS_AKTIF == 0}
                        {if $m.ID_STATUS_PENGGUNA == 4}
                            back-green
                        {else}
                            back-yellow
                        {/if}
                    {/if}">
                    <td class="center">{$m@index+1}</td>
                    <td class="center">
                        <a href="mahasiswa-wali.php?mode=detail&id_mhs={$m.ID_MHS}">{$m.NIM_MHS}</a>
                    </td>
                    <td><a href="sms.php?view=index&mhs={$m.NM_PENGGUNA}&mobile={$m.MOBILE_MHS}">{$m.NM_PENGGUNA}</a></td>
                    <td class="center">{$m.NM_STATUS_PENGGUNA}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td class="kosong" colspan="4">Data Tidak Ditemukan</td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{/if}
{literal}
    <script type="text/javascript">
        $('#select_fakultas').change(function() {
            $.ajax({
                url: 'getProgramStudi.php',
                type: 'post',
                data: 'id_fakultas=' + $(this).val(),
                success: function(data) {
                    $('#select_prodi').html(data);
                }
            });
        });
        $('#button_advance').click(function() {
            $('#advance_search').toggle('slow');
        });
        $("#nama_nim").autocomplete({
            source: "cari_mahasiswa.php",
            minLength: 3,
            select: function(event, ui) {
                $('#content').load('mahasiswa-wali.php?rekap=1&mode=detail&id_mhs=' + ui.item.id + '');
            }
        });
        $("#kota").autocomplete({
            source: "GetKotaAutoComplete.php",
            minLength: 3,
            select: function(event, ui) {
                $(this).val("");
            }
        });
        $("#sma").autocomplete({
            source: "GetSekolahAutoComplete.php",
            minLength: 3,
            select: function(event, ui) {
                $(this).val("");
            }
        });
        $('form').validate();
    </script>
{/literal}