{literal}
    <style>
        .span_button{
            padding: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            background-color: #1878ab;
            color: #ffffff;
            cursor: pointer;
        }
    </style>
{/literal}   <div class="center_title_bar">Report Mahasiswa Unair</div>
{if isset($data_mahasiswa)}
    <div class="ui-widget">
        <table>
            <tr>
                <td colspan="8" style="border: none;padding: 5px 0px 15px 0px;">    
                    <span class="span_button" onclick="history.back()">Kembali</span>
                </td>
            </tr>
            <tr class="ui-widget-header">
                <th>NOMER</th>
                <th>NIM</th>
                <th>NAMA</th>
                <th>ANGKATAN</th>
                <th>JENJANG</th>
                <th>PROGRAM STUDI</th>
                <th>FAKULTAS</th>
                <th>STATUS</th>
            </tr>
            {$nomer=1}
            {foreach $data_mahasiswa as $data}
                <tr class="ui-widget-content">
                    <td>{$nomer++}</td>
                    <td><a style="color: #07a6f9" href="cari-mahasiswa.php?detail=ok&y={$data.NIM}">{$data.NIM}</a></td>
                    <td>{$data.NAMA}</td>
                    <td>{$data.ANGKATAN}</td>
                    <td>{$data.JENJANG}</td>
                    <td>{$data.PRODI}</td>
                    <td>{$data.FAKULTAS}</td>
                    <td>{$data.NM_STATUS_PENGGUNA}</td>
                </tr>
            {/foreach}
        </table>
    </div>
{else}
    <div class="ui-widget-content" style="overflow: scroll;width: 700px;height: 500px;">
        <table>
            <tr class="ui-widget-header" >
                <th style="color: white;" rowspan="2">
                    PROGRAM STUDI<br/>/TAHUN AKADEMIK
                </th>
                {$index_fakultas=1}
                {$fakultas=$data_fakultas_prodi[0]['FAKULTAS']}
                {for $i=0 to $count_data_fakultas_prodi}
					{if $data_fakultas_prodi[$i+1]['FAKULTAS']!=$fakultas}
						<th class="ui-widget-header" colspan="{$index_fakultas}" style="font-size: 15px;font-weight: bolder;text-align: center;color: white;" >{$data_fakultas_prodi[$i]['FAKULTAS']}</th>
						{$index_fakultas=1}
						{$fakultas=$data_fakultas_prodi[$i+1]['FAKULTAS']}
					{else}
						{$index_fakultas=$index_fakultas+1}
					{/if}
                {/for}
                <th style="font-size: 15px;vertical-align: middle;color: white;" rowspan="2">
                    &Sigma; Per-Angkatan
                </th>
            </tr>
            <tr class="ui-widget-header" >
                {foreach $data_fakultas_prodi as $data}
                    <th style="padding: 5px;color: white;">({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</th>
                {/foreach}
            </tr>
            {for $i=0 to $count_data_tahun_akademik-1}
            <tr class="row_tahun">
                <td>{$data_tahun_akademik[$i]['THN_AKADEMIK_SEMESTER']}</td>
                {for $j=0 to $count_data_fakultas_prodi-1}
                <td>
                    {$jumlah=$data_jumlah_mahasiswa[$i+($j*($count_data_tahun_akademik))]['JUMLAH']}
                    {$jumlah_per_angkatan[$i]=$jumlah_per_angkatan[$i]+$jumlah}
                    {$jumlah_per_prodi[$j]=$jumlah_per_prodi[$j]+$jumlah}
                    <a style="color: #07a6f9" href="report-mahasiswa-unair.php?mode=list_mhs&prodi={$data_fakultas_prodi[$j]['ID_PROGRAM_STUDI']}&angkatan={$data_tahun_akademik[$i]['THN_AKADEMIK_SEMESTER']}">{$jumlah}</a>
                </td>
                {/for}
                <td style="font-size: 13px;background-color: #ccffff;">{$jumlah_per_angkatan[$i]}</td>
            </tr>
            {/for}
            <tr style="font-size: 13px;background-color: #ccffff;">
                <td>
                    &Sigma; Per Prodi
                </td>
                {for $i=0 to $count_data_fakultas_prodi-1}                    
					<td>{$jumlah_per_prodi[$i]}</td>
					{$jumlah_all=$jumlah_all+$jumlah_per_prodi[$i]}
                {/for}
                <td style="background-color: #ffcccc;">{$jumlah_all}</td>
            </tr>

        </table>
    </div>
    {literal}
        <script>
                $('.row_tahun').click(
                    function(){
                        if($(this).hasClass('clicked')){
                            $(this).css("background-color","#ffffff");
                            $(this).removeClass('clicked');
                        }else{
                            $(this).css("background-color","#ccffcc");
                            $(this).addClass('clicked');  }  
                });
        </script>
    {/literal}
{/if}