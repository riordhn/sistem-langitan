{literal}
    <style>
        .span_button{
            padding: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            background-color: #1878ab;
            color: #ffffff;
            cursor: pointer;
        }
    </style>
{/literal} 
<div class="center_title_bar">Cari Mahasiswa</div>

{if isset($data_mahasiswa)}
    <table>
        <tr>
            <th>NAMA</th>
            <th>NIM</th>
            <th>FAKULTAS</th>
            <th>PROGRAM STUDI</th>
            <th></th>
        </tr>
        {if $data_mahasiswa==null}
            <tr>
                <td colspan="5" class="center">Hasil Pencarian Tidak Ditemukan</td>
            </tr>
        {else}
            {foreach $data_mahasiswa as $data}
                <tr>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>{$data.NIM_MHS}</td>
                    <td>{$data.NM_FAKULTAS|upper}</td>
                    <td>{$data.NM_PROGRAM_STUDI}</td>
                    <td><a href="cari-mahasiswa.php?detail=ok&y={$data.NIM_MHS}">Detail</a></td>
                </tr>
            {/foreach}
        {/if}
    </table>
{else if isset($biodata_mahasiswa)}
    <div class="ui-widget">
        <table style="width: 80%">
            <tr>
                <td colspan="8" style="border: none;padding: 5px 0px 15px 0px;">    
                    <span class="span_button" onclick="history.back()">Kembali</span>
                </td>
            </tr>
            <tr class="ui-widget-header">
                <th colspan="2" class="center">BIODATA MAHASISWA</th>
            </tr>
            <tr class="ui-widget-content">
                <td colspan="2" class="center">
                    <img src="/foto_mhs/{$smarty.get.y}.JPG" width="180" height="230"/>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>NAMA</td>
                <td>{$biodata_mahasiswa.NM_PENGGUNA}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>NIM</td>
                <td>{$smarty.get.y}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>ANGKATAN</td>
                <td>{$biodata_mahasiswa.THN_ANGKATAN_MHS}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>JENJANG</td>
                <td>{$biodata_mahasiswa.NM_JENJANG}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>FAKULTAS</td>
                <td>{$biodata_mahasiswa.NM_FAKULTAS|upper}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>PROGRAM STUDI</td>
                <td>{$biodata_mahasiswa.NM_PROGRAM_STUDI}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>TEMPAT/TANGGAL LAHIR</td>
                <td>{$biodata_mahasiswa.NM_KOTA} / {$biodata_mahasiswa.TGL_LAHIR_PENGGUNA}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>TELP</td>
                <td>{$biodata_mahasiswa.TELP} / {$biodata_mahasiswa.MOBILE_MHS}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>KELAMIN</td>
                <td>
                    {if $biodata_mahasiswa.KELAMIN_PENGGUNA==1}
                        Laki-laki
                    {else}
                        Perempuan
                    {/if}
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>EMAIL</td>
                <td>{$biodata_mahasiswa.EMAIL_PENGGUNA}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>BLOG MAHASISWA</td>
                <td></td>
            </tr>
            <tr class="ui-widget-content">
                <td>ALAMAT MAHASISWA</td>
                <td>{$biodata_mahasiswa.ALAMAT_MHS}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>NAMA AYAH</td>
                <td>{$biodata_mahasiswa.NM_AYAH_MHS}</td>
            </tr>
            <tr class="ui-widget-content">
                <td>ALAMAT AYAH</td>
                <td>{$biodata_mahasiswa.ALAMAT_AYAH_MHS}</td>
            </tr>
        </table>
		<table>
			<tr class="ui-widget-header">
				<th colspan="10" class="header-coloumn">History Pembayaran Mahasiswa</th>
			</tr>
			<tr class="ui-widget-header">
				<th>Nomer</th>
				<th>Semester Pembayaran</th>
				<th>Bank</th>
				<th>Via Bank</th>
				<th>Tanggal Bayar</th>
				<th>Nomer Transaksi</th>
				<th>Keterangan</th>
				<th>Biaya</th>
				<th>Besar Biaya</th>
				<th>Denda Biaya</th>
			</tr>
        {$nomer=1}
			{foreach $detail_pembayaran_mahasiswa as $data}
				<tr>
					<td>{$nomer++}</td>
					<td>{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
					<td>{$data.NM_BANK}</td>
					<td>{$data.NAMA_BANK_VIA}</td>
					<td>{$data.TGL_BAYAR}</td>
					<td>{$data.NO_TRANSAKSI}</td>
					<td>{$data.KETERANGAN}</td>
					<td>{$data.NM_BIAYA}</td>
					<td>{$data.BESAR_BIAYA}</td>
					<td>{$data.DENDA_BIAYA}</td>
				</tr>
			{/foreach}
		</table>
		<table>
			<tr class="ui-widget-header">
				<th colspan="6" class="header-coloumn">DETAIL STATUS AKADEMIK MAHASISWA</th>
			</tr>
			<tr class="ui-widget-header">
				<th>Nomer</th>
				<th>Tahun Ajaran</th>
				<th>Nama Semester</th>
				<th>IPK</th>
				<th>IPS</th>
				<th>Total SKS</th>
			</tr>
			{$nomer=1}
			{foreach $detail_status_mahasiswa as $data}
				<tr>
					<td>{$nomer++}</td>
					<td>{$data.TAHUN_AJARAN}</td>
					<td>{$data.NM_SEMETER}</td>
					<td>{$data.IPK}</td>
					<td>{$data.IPS}</td>
					<td>{$data.TOTAL_SKS}</td>
				</tr>
			{/foreach}
		</table>
		<table>
			<tr class="ui-widget-header">
				<th colspan="5" class="header-coloumn">DETAIL TRANSKRIP AKADEMIK MAHASISWA</th>
			</tr>
			<tr class="ui-widget-header">
				<th>Nomer</th>
				<th>Kode Ajar</th>
				<th>Nama Mata Kuliah</th>
				<th>Kredit Semester</th>
				<th>Nilai</th>
			</tr>
			{$nomer=1}
			{foreach $detail_mata_kuliah_mahasiswa as $data}
				<tr>
					<td>{$nomer++}</td>
					<td>{$data.KD_MATA_KULIAH}</td>
					<td>{$data.NM_MATA_KULIAH}</td>
					<td>{$data.KREDIT_SEMESTER}</td>
					<td>{$data.NILAI_HURUF}</td>
				</tr>
			{/foreach}
		</table>
    </div>
{else}
    <form action="cari-mahasiswa.php" method="get">
        <table>
            <tr>
                <td>NAMA / NIM MAHASISWA</td>
                <td><input type="text" name="x"/></td>
            </tr>
        </table>
    </form>
{/if}
