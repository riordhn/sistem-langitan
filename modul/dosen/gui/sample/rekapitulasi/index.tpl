        {literal}
        <style type="text/css">
            .view tr td {
                font-size: .95em;
                font-family: "Arial Narrow";
            }
            .view tr th {
                font-size: .95em;
                font-family: "Arial Narrow";
                font-weight: bold;
            }
        </style>
        {/literal}
        <form action="../rekapitulasi.php" method="get">
            <table border="1" style="width: 780px;" id="filter">
                <tr>
                    <td colspan="5" style="text-align: center" class="col3">
                        Lihat :
                        {if isset($smarty.get.view_id)}{$view_id=$smarty.get.view_id}{else}{$view_id=0}{/if}
                        <select name="view_id">
                                <option value="">-- Pilih Data --</option>
                            {foreach $view_set as $v}
                                <option value="{$v.ID}" {if $v.ID == $view_id}selected="selected"{/if}>{$v.VIEW}</option>
                            {/foreach}
                        </select>
                        {literal}
                        <script type="text/javascript">
                            $('select[name="view_id"]').change(function() {
                                if (this.value == 1 || this.value == 2 || this.value == 3 || this.value == 4 ) {
                                    $('#cell_fakultas').show('slow');
                                    $('#cell_prodi').show('slow');
                                    $('#cell_angkatan').show('slow');
                                    $('#cell_dosen').hide('slow');
                                    $('#cell_semester').hide('slow');
                                }
                                if (this.value == 5) {
                                    $('#cell_fakultas').show('slow');
                                    $('#cell_prodi').show('slow');
                                    $('#cell_angkatan').hide('slow');
                                    $('#cell_dosen').show('slow');
                                    $('#cell_semester').hide('slow');
                                }
                                if (this.value == 6) {
                                    $('#cell_fakultas').show('slow');
                                    $('#cell_prodi').show('slow');
                                    $('#cell_angkatan').hide('slow');
                                    $('#cell_dosen').hide('slow');
                                    $('#cell_semester').show('slow');
                                }
                                if (this.value == 7) {
                                    $('#cell_fakultas').show('slow');
                                    $('#cell_prodi').show('slow');
                                    $('#cell_angkatan').show('slow');
                                    $('#cell_dosen').hide('slow');
                                    $('#cell_semester').hide('slow');
                                    $('#cell_fakultas').show('slow');
                                }
                                if (this.value == 8) {
                                    $('#cell_fakultas').show('slow');
                                    $('#cell_prodi').show('slow');
                                    $('#cell_angkatan').hide('slow');
                                    $('#cell_dosen').hide('slow');
                                    $('#cell_semester').hide('slow');
                                }    
                                if (this.value == 9) {
                                    $('#cell_fakultas').show('slow');
                                    $('#cell_prodi').hide('slow');
                                    $('#cell_angkatan').hide('slow');
                                    $('#cell_dosen').hide('slow');
                                    $('#cell_semester').hide('slow');
                                }
                                if (this.value == 10) {
                                    $('#cell_fakultas').hide('slow');
                                    $('#cell_prodi').hide('slow');
                                    $('#cell_angkatan').hide('slow');
                                    $('#cell_dosen').hide('slow');
                                    $('#cell_semester').hide('slow');
                                }
                            });
                        </script>
                        {/literal}
                    </td>

                </tr>
                <tr>
                    <td id="cell_fakultas"
                        {if isset($smarty.get.view_id)}
                            {if $smarty.get.view_id == 10}style="display: none;"{/if}
                        {/if}
                        > Fakultas :<br/>
                        {if isset($smarty.get.id_fakultas)}{$id_fakultas=$smarty.get.id_fakultas}{else}{$id_fakultas=0}{/if}
                        <select name="id_fakultas">
                        <option value="">-- Pilih Fakultas --</option>
                        {foreach $fakultas_set as $f}
                            <option value="{$f.ID_FAKULTAS}" {if $f.ID_FAKULTAS==$id_fakultas}selected="selected"{/if}>{$f.NM_FAKULTAS}</option>
                        {/foreach}
                        </select>
                        {literal}
                        <script type="text/javascript">
                        $('select[name="id_fakultas"]').change(function() {
                            $.ajax({
                                url: "getProgramStudi.php",
                                data: "id_fakultas=" + this.value,
                                cache: false,
                                success: function(data) { $('select[name="id_program_studi"]').html(data); }
                            });
                        });
                        </script>
                        {/literal}
                    </td>
                    <td id="cell_prodi"
                        {if isset($smarty.get.view_id)}
                            {if $smarty.get.view_id == 9}style="display: none;"{/if}
                            {if $smarty.get.view_id == 10}style="display: none;"{/if}
                        {/if}
                        >Program Studi : <br/>
                        {if isset($smarty.get.id_program_studi)}{$id_program_studi=$smarty.get.id_program_studi}{else}{$id_program_studi=0}{/if}
                        <select name="id_program_studi">
                            <option value="">-- Pilih Program Studi --</option>
                            {if $program_studi_set}
                                {foreach $program_studi_set as $ps}
                                    <option value="{$ps.ID_PROGRAM_STUDI}" {if $ps.ID_PROGRAM_STUDI==$id_program_studi}selected="selected"{/if}>{ucwords(strtolower($ps.NM_PROGRAM_STUDI))}</option>
                                {/foreach}
                            {/if}
                        </select>
                        {literal}
                        <script type="text/javascript">
                            $('select[name="id_program_studi"]').change(function() {
                                if ($('select[name="view_id"]').val() == 5) {
                                    var id_prodi = this.value;
                                    $.ajax({
                                        url: 'getDosen.php',
                                        data: 'id_program_studi=' + id_prodi,
                                        success: function(data) { $('select[name="id_dosen"]').html(data); }
                                    });
                                }
                            });
                        </script>
                        {/literal}
                    </td>
                    <td id="cell_dosen"
                        {if isset($smarty.get.view_id)}
                            {if $smarty.get.view_id != 5}style="display: none;"{/if}
                            {if $smarty.get.view_id == 10}style="display: none;"{/if}
                        {/if}>
                        Dosen : <br/>
                        {if isset($smarty.get.id_dosen)}{$id_dosen=$smarty.get.id_dosen}{else}{$id_dosen=0}{/if}
                        <select name="id_dosen">
                            <option value="">-- Pilih Dosen --</option>
                            {if $dosen_set}
                                {foreach $dosen_set as $d}
                                <option value="{$d.ID_DOSEN}" {if $d.ID_DOSEN == $id_dosen}selected="selected"{/if} >{$d.NM_PENGGUNA}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </td>
                    <td id="cell_angkatan"
                        {if isset($smarty.get.view_id)}
                            {if $smarty.get.view_id == 5}style="display: none;"{/if}
                            {if $smarty.get.view_id == 6}style="display: none;"{/if}
                            {if $smarty.get.view_id == 8}style="display: none;"{/if}
                            {if $smarty.get.view_id == 9}style="display: none;"{/if}
                            {if $smarty.get.view_id == 10}style="display: none;"{/if}
                        {/if}>
                        Angkatan : <br/>
                        {if isset($smarty.get.thn_angkatan_mhs)}{$thn_angkatan_mhs=$smarty.get.thn_angkatan_mhs}{else}{$thn_angkatan_mhs=0}{/if}
                        <select name="thn_angkatan_mhs">
                            <option value="">-- Pilih Tahun Angkatan --</option>
                            {if $angkatan_set}
                                {foreach $angkatan_set as $thn}
                                <option value="{$thn['THN_ANGKATAN_MHS']}" {if $thn['THN_ANGKATAN_MHS'] == $thn_angkatan_mhs}selected="selected"{/if}>{$thn['THN_ANGKATAN_MHS']}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </td>
                    <td id="cell_semester" {if isset($smarty.get.view_id)}{if $smarty.get.view_id != 6}style="display: none;"{/if}{/if}>
                        Tahun Semester : <br/>
                        {if isset($smarty.get.id_semester)}{$id_semester=$smarty.get.id_semester}{else}{$id_semester=0}{/if}
                        <select name="id_semester">
                            <option value="">-- Pilih Semester --</option>
                            {if $semester_set}
                                {foreach $semester_set as $s}
                                <option value="{$s.ID_SEMESTER}" {if $s.ID_SEMESTER == $id_semester}selected="selected"{/if}>{$s.NM_SEMESTER} {$s.TAHUN_AJARAN}</value>
                                {/foreach}
                            {/if}
                        </select>
                    </td>
                </tr>
                <tr><td colspan="5" style="text-align: center;" class="col3"><input type="submit" value="Lihat" style="font-size: 120%"/></td></tr>
            </table>
        </form>
        
        {if $data_set}
            {if $smarty.get.view_id == 1}
                <table border="1" cellspacing="0" cellpadding="2" class="view">
                    <tr>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Kls Msk</th>
                        <th>Tmpt Lahir</th>
                        <th>Tgl Lahir</th>
                        <th>JK</th>
                        <th>Thn Msk</th>
                        <th>Smt Msk</th>
                        <th>B. Studi</th>
                        <th>A. Kota</th>
                        <th>Tgl Msk</th>
                        <th>Tgl Lls</th>
                        <th>Sts Mhs</th>
                        <th>Sts</th>
                    </tr>
                    {foreach $data_set as $m}
                    <tr>
                        <td>{$m.NIM_MHS}</td>
                        <td>{ucwords(strtolower($m.NM_PENGGUNA))}</td>
                        <td>{$m.KELAS_MASUK}</td>
                        <td>{$m.KOTA_LAHIR}</td>
                        <td>{date('d-m-y', strtotime($m.TGL_LAHIR_PENGGUNA))}</td>
                        <td>{$m.JK}</td>
                        <td>{$m.THN_ANGKATAN_MHS}</td>
                        <td>{$m.SEMESTER_MASUK}</td>
                        <td>{$m.BATAS_STUDI}</td>
                        <td>{$m.KOTA_ASAL}</td>
                        <td>{$m.TGL_TERDAFTAR_MHS}</td>
                        <td>{$m.TGL_LULUS_MHS}</td>
                        <td>{$m.STATUS_MHS}</td>
                        <td>{$m.STATUS_MASUK}</td>
                    </tr>
                    {/foreach}
                </table>
            {/if}
            
            {if $smarty.get.view_id == 2}
                <table border="1" cellspacing="0" cellpadding="2" class="view">
                    <tr>
                        <th>Smt</th>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>SKS Diambil</th>
                        <th>Nilai IPS</th>
                        <th>SKS Total</th>
                        <th>Nilai IPK</th>
                    </tr>
                    {foreach $data_set as $m}
                    <tr>
                        <td>{$m.NM_SEMESTER}</td>
                        <td>{$m.NIM_MHS}</td>
                        <td>{$m.NM_PENGGUNA}</td>
                        <td>{$m.SKS_SEMESTER}</td>
                        <td>{if $m.IPS_MHS_STATUS != ''}{$m.IPS_MHS_STATUS|string_format:"%.2f"}{/if}</td>
                        <td>{$m.SKS_TOTAL_MHS_STATUS}</td>
                        <td>{if $m.IPK_MHS_STATUS != ''}{$m.IPK_MHS_STATUS|string_format:"%.2f"}{/if}</td>
                    </tr>
                    {/foreach}
                </table>
            {/if}
            
            {if $smarty.get.view_id == 3}
                <table border="1" cellspacing="0" cellpadding="2" class="view">
                    <tr>
                        <th>Tahun Semester</th>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Mata Kuliah</th>
                        <th>Nilai Huruf</th>
                        <th>Nilai Angka</th>
                        <th>Kelas</th>
                    </tr>
                    {foreach $data_set as $m}
                    <tr>
                        <td>{$m.NM_SEMESTER}</td>
                        <td>{$m.NIM_MHS} </td>
                        <td>{ucwords(strtolower($m.NM_PENGGUNA))} </td>
                        <td>{$m.NM_MATA_KULIAH} </td>
                        <td>{$m.NILAI_HURUF} </td>
                        <td>{$m.NILAI_ANGKA} </td>
                        <td>{$m.NO_KELAS_MK} </td>
                    </tr>
                    {/foreach}
                </table>
            {/if}
            
            {if $smarty.get.view_id == 5}
                {if isset($smarty.get.id_dosen)}{$id_dosen=$smarty.get.id_dosen}{else}{$id_dosen==0}{/if}
                <table border="1" cellspacing="0" cellpadding="2" class="view">
                    <tr>
                        <th>Tahun Semester</th>
                        {if $id_dosen == 0}
                        <th>NIDN</th>
                        <th>NIP</th>
                        <th>Nama</th>
                        {/if}
                        <th>Mata Kuliah</th>
                        <th>Kelas</th>
                        <th>Tatap Muka Total</th>
                        <th>Tatap Muka Sesungguhnya</th>
                    </tr>
                    {foreach $data_set as $d}
                    <tr>
                        <td>{$d.NM_SEMESTER}</td>
                        {if $id_dosen == 0}
                        <td>{$d.NIDN_DOSEN}</td>
                        <td>{$d.NIP_DOSEN}</td>
                        <td>{$d.NM_PENGGUNA}</td>
                        {/if}
                        <td>{$d.NM_MATA_KULIAH}</td>
                        <td>{$d.NO_KELAS_MK}</td>
                        <td>{$d.TATAP_MUKA_TOTAL}</td>
                        <td>{$d.TATAP_MUKA_ASLI}</td>
                    </tr>
                    {/foreach}
                </table>
            {/if}
            
            {if $smarty.get.view_id == 6}
                <table border="1" cellspacing="0" cellpadding="2" class="view">
                    <tr>
                        <th>Tahun Semester</th>
                        <th>Kode MK</th>
                        <th>Nama MK</th>
                        <th>SKS</th>
                        <th>Tatap Muka</th>
                        <th>Praktikum</th>
                        <th>Wajib / Pilihan</th>
                        <th>NIDN Dosen</th>
                        <th>Nama</th>
                        <th>Jenjang Dosen</th>
                        <th>Prodi Dosen</th>
                        <th>Status Mata Kuliah</th>
                        <th>Silabus</th>
                        <th>Bahan Ajar</th>
                        <th>Diktat</th>
                        <th>SAPP</th>
                    </tr>
                    {foreach $data_set as $m}
                    <tr>
                        <td>{$m.NM_SEMESTER}</td>
                        <td>{$m.KD_MATA_KULIAH}</td>
                        <td>{$m.NM_MATA_KULIAH}</td>
                        <td>{$m.KREDIT_SEMESTER}</td>
                        <td>{$m.KREDIT_TATAP_MUKA}</td>
                        <td>{$m.KREDIT_PRAKTIKUM}</td>
                        <td>{$m.NM_STATUS_MK}</td>
                        <td>{$m.NIDN_DOSEN}</td>
                        <td>{$m.NM_PENGGUNA}</td>
                        <td>{$m.JENJANG_DOSEN}</td>
                        <td>{$m.PRODI_DOSEN}</td>
                        <td>{$m.STATUS_AKTIF}</td>
                        <td>{$m.SILABUS}</td>
                        <td>{$m.BAHAN_AJAR}</td>
                        <td>{$m.DIKTAT}</td>
                        <td>{$m.SAPP}</td>
                    </tr>
                    {/foreach}
                </table>
            {/if}
            
            {if $smarty.get.view_id == 7}
                <table border="1" cellspacing="0" cellpadding="2" class="view">
                    <tr>
                        <th>Tahun Semester</th>
                        <th>Jenjang</th>
                        <th>Prodi</th>
                        <th>NIM</th>
                        <th>Nama Mahasiswa</th>
                        <th>Nama Dosen</th>
                    </tr>
                    {foreach $data_set as $d}
                    <tr>
                        <td>{$d.NM_SEMESTER}</td>
                        <td>{$d.NM_JENJANG}</td>
                        <td>{$d.NM_PROGRAM_STUDI}</td>
                        <td>{$d.NIM_MHS}</td>
                        <td>{$d.NM_MHS}</td>
                        <td>{$d.NM_DOSEN}</td>
                    </tr>
                    {/foreach}
                </table>
            {/if}
            
            {if $smarty.get.view_id == 8}
                <table border="1" cellspacing="0" cellpadding="2" class="view">
                    <tr>
                        <th>Jenjang</th>
                        <th>Prodi</th>
                        <th>NIDN</th>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Golongan</th>
                        <th>Jabatan</th>
                        <th>Status</th>
                    </tr>
                    {foreach $data_set as $d}
                    <tr>
                        <td>{$d.NM_JENJANG}</td>
                        <td>{$d.NM_PROGRAM_STUDI}</td>
                        <td>{$d.NIDN_DOSEN}</td>
                        <td>{$d.NIP_DOSEN}</td>
                        <td>{$d.NM_PENGGUNA}</td>
                        <td>{$d.NM_GOLONGAN}</td>
                        <td>{$d.NM_JABATAN_PEGAWAI}</td>
                        <td>{if $d.NM_GOLONGAN == 'DLB'}DLB{else}DT{/if}</td>
                    </tr>
                    {/foreach}
                </table>
            {/if}
            
            {if $smarty.get.view_id == 9}
                <table border="1" cellspacing="0" cellpadding="2" class="view">
                    <tr>
                        <th>No</th>
                        <th>Nama Ruangan</th>
                        <th>Kapasitas</th>
                        <th>Jenis</th>
                        <th>Deskripsi</th>
                    </tr>
                    {foreach $data_set as $d}
                    <tr>
                        <td>{$d@index+1}</td>
                        <td>{$d.NM_RUANGAN}</td>
                        <td>{$d.KAPASITAS_RUANGAN}</td>
                        <td>{$d.TIPE_RUANGAN}</td>
                        <td>{$d.DESKRIPSI_RUANGAN}</td>
                    </tr>
                    {/foreach}
                </table>   
            {/if}
            
            {if $smarty.get.view_id == 10}
                <table border="1" cellspacing="0" cellpadding="2" class="view">
                    <tr>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Jabatan</th>
                        <th>Fakultas</th>
                        <th>No Telp</th>
                    </tr>
                    {foreach $data_set as $d}
                    <tr>
                        <td>{$d.USERNAME}</td>
                        <td>{$d.NM_PENGGUNA}</td>
                        <td>{$d.NM_JABATAN_PEGAWAI}</td>
                        <td>{$d.SINGKATAN_FAKULTAS}</td>
                        <td>{$d.TLP_DOSEN}</td>
                    </tr>
                    {/foreach}
                </table>   
            {/if}
        {/if}
        
     