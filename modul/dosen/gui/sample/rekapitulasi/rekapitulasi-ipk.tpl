<div class="center_title_bar">Rekapitulasi IPK Mahasiswa S1</div>
<form method="get" id="report_form" action="rekapitulasi-ipk.php">
    <table class="ui-widget" style="width:40%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Tahun Angkatan</td>
            <td>
                <select name="thn_angkatan" id="select_thn_angkatan">
                    {foreach $thn_angkatan as $data}
                        <option value="{$data.THN_ANGKATAN_MHS}" {if $data.THN_ANGKATAN_MHS==$smarty.get.thn_angkatan}selected="true"{/if}>{$data.THN_ANGKATAN_MHS}</option>
                    {/foreach}
                </select>
            </td><!--
            <td width="8%">Jalur</td>
            <td>
                <select name="jalur" id="select_jalur">
                		<option value="">Semua</option>
                    {foreach $jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $data.ID_JALUR==$smarty.get.jalur}selected="true"{/if}>{$data.NM_JALUR}</option>
                    {/foreach}
                </select>
            </td>-->
        </tr>
        <tr class="ui-widget-content">
            <td colspan="4" style="text-align:center">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($smarty.get.thn_angkatan)}
    <table class="ui-widget ui-widget-content" style="width: 100%" border="1">
        <tr class="ui-widget-header">
            <th colspan="9" class="header-coloumn">Rekapitulasi Data IPK Mahasiswa S1</th>
        </tr>
        <tr>
            <th>No</th>
            <th >Fakultas</th>
            <th >Prodi</th>
            <th>Jalur</th>
            <th>RERATA_IPK </th>
            <th>RERATA_SKS_NON_D</th>
            <th>RERATA_IPS</th>
            <th>RERATA_SKS</th>
        </tr>

        {$tampil}
    </table>
{/if}