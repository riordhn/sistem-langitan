<div class="center_title_bar">Rekapitulasi Registrasi Mahasiswa</div>
{literal}
    <style>
        .ui-widget-content a{
            text-decoration: none;
            color: brown;
        }
        .ui-widget-content a:hover{
            color: #f09a14;
        }
    </style>
{/literal}
<form method="get" id="report_form" action="rekap-registrasi.php">
    <table class="ui-widget" style="width:100%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="35%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Semester</td>
            <td>
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$smarty.get.semester}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="4" style="text-align:center">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_rekap_pembayaran)}
    {if empty($smarty.get.fakultas)}
        <table class="ui-widget" style="width: 100%">
            <tr class="ui-widget-header">
                <th style="text-align:center;text-transform:uppercase" colspan="{3+$count_data_status}">REKAPITULASI REGISTRASI MAHASISWA</th>
            </tr>
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>FAKULTAS</th>
                {foreach $data_status as $data}
                    <th>{$data.NAMA_STATUS|upper}</th>
                {/foreach}
				<th>SUDAH KRS</th>
            </tr>
			{foreach $data_rekap_pembayaran as $data}
                {$index_jumlah=0}
                {$col_name=array_keys($data)}
                <tr class="ui-widget-content">
                    <td>{$data@index+1}</td>
                    <td>{$data.NM_FAKULTAS}</td>
                    {foreach $col_name as $col}
                        {if $col!='ID_FAKULTAS'&&$col!='NM_FAKULTAS'&&$col!='JUMLAH_KRS'}
                            <td class="center">
                                <a href="rekap-registrasi.php?mode=load_mhs&fakultas={$data.ID_FAKULTAS}&semester={$smarty.get.semester}&status={$data_status[$index_jumlah].ID_STATUS_PEMBAYARAN}">{$data.$col}</a>
                            </td>
                            {$jumlah[$index_jumlah]=$jumlah[$index_jumlah]+$data.$col}
                            {$index_jumlah=$index_jumlah+1}
                        {/if}
                    {/foreach}
					<td><a href="rekap-registrasi.php?mode=load_mhs&fakultas={$data.ID_FAKULTAS}&semester={$smarty.get.semester}&krs=1">{$data.JUMLAH_KRS}</a>
					{$total_krs=$total_krs+$data.JUMLAH_KRS}</td>
                </tr>
            {foreachelse}
                <tr class="ui-widget-content">
                    <td class="center" colspan="{3+$count_data_status}"><span style="color: red">Data Kosong / Tidak Ada Pembayaran</span></td>
                </tr>
            {/foreach}
            <tr class="ui-widget-content">
                <td colspan="2" class="center">TOTAL</td>
                {foreach $jumlah as $j}
                    <td class="center">{$j}</td>
                {foreachelse}
                    <td colspan="{$count_data_status+1}"></td>
                {/foreach}
				<td>{$total_krs}</td>
            </tr>
        </table>
    {else}
        <table class="ui-widget" style="width: 100%">
            <tr class="ui-widget-header">
                <th style="text-align:center;text-transform:uppercase" colspan="{3+$count_data_status}">REKAPITULASI REGISTRASI MAHASISWA</th>
            </tr>
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>PROGRAM STUDI</th>
                {foreach $data_status as $data}
                    <th>{$data.NAMA_STATUS|upper}</th>
                {/foreach}
				<th>SUDAH KRS</th>
            </tr>
            {foreach $data_rekap_pembayaran as $data}
                {$index_jumlah=0}
                {$col_name=array_keys($data)}
                <tr class="ui-widget-content">
                    <td>{$data@index+1}</td>
                    <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                    {foreach $col_name as $col}
                        {if $col!='ID_PROGRAM_STUDI'&&$col!='NM_PROGRAM_STUDI'&&$col!='NM_JENJANG'&&$col!='JUMLAH_KRS'}
                            <td class="center">
                                <a href="rekap-registrasi.php?mode=load_mhs&fakultas={$smarty.get.fakultas}&prodi={$data.ID_PROGRAM_STUDI}&semester={$smarty.get.semester}&status={$data_status[$index_jumlah].ID_STATUS_PEMBAYARAN}">{$data.$col}</a>
                            </td>
                            {$jumlah[$index_jumlah]=$jumlah[$index_jumlah]+$data.$col}
                            {$index_jumlah=$index_jumlah+1}
                        {/if}
                    {/foreach}
					<td><a href="rekap-registrasi.php?mode=load_mhs&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}&semester={$smarty.get.semester}&krs=1">{$data.JUMLAH_KRS}</a>
					{$total_krs=$total_krs+$data.JUMLAH_KRS}
					</td>
				</tr>
            {foreachelse}
                <tr class="ui-widget-content">
                    <td class="center" colspan="{2+$count_data_status}"><span style="color: red">Data Kosong / Tidak Ada Pembayaran</span></td>
                </tr>
            {/foreach}
            <tr class="ui-widget-content">
                <td colspan="2" class="center">TOTAL</td>
                {foreach $jumlah as $j}
                    <td class="center">{$j}</td>
                {foreachelse}
                    <td colspan="{$count_data_status}"></td>
                {/foreach}
				<td>{$total_krs}</td>
            </tr>
        </table>
    {/if}
{else if isset($data_status_bayar)}
    <table class="ui-widget" style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="11" style="text-align:center;text-transform:uppercase">REKAPITULASI REGISTRASI MAHASISWA
                <br/>
                {if $smarty.get.prodi==''} 
                    Fakultas {$data_fakultas_one.NM_FAKULTAS} 
                {else} 
                    Prodi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI} 
                {/if}
                <br/>
                Status {if isset($smarty.get.krs)} Sudah KRS {else} {$data_status_one.NAMA_STATUS} {/if} 
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Prodi</th>
            <th>Jalur</th>
            <th>Status Mahasiswa</th>
            <th>Biaya</th>
            <th>Status Bayar</th>
            <th>Tgl Jatuh Tempo</th>
            <th>Ket. Pembayaran</th>
            <th>Ket. Status</th>
        </tr>
        {$total=0}
        {foreach $data_status_bayar as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                <td class="center">{$data.NM_JALUR}</td>
                <td class="center">{$data.NM_STATUS_PENGGUNA}</td>
                <td class="center">{number_format($data.TOTAL_BIAYA)}</td>
                <td class="center">{$data.NAMA_STATUS}</td>
                <td width="70px">{$data.TGL_JATUH_TEMPO}</td>
                <td class="center">{$data.KETERANGAN_PEMBAYARAN}</td>
                <td class="center">{$data.KETERANGAN_STATUS}</td>
            </tr>
            {$total=$total+$data.TOTAL_BIAYA}
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="11" class="center"><span style="color: red">Data Kosong</span></td>
            </tr>
        {/foreach}
        <tr class="ui-widget-content">
            <td colspan="6" class="center">TOTAL PEMBAYARAN</td>
            <td>{number_format($total)}</td>
            <td colspan="4"></td>
        </tr>
    </table>
    <button class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" onclick="history.back(-1)">Kembali</button>
{/if}