<div class="center_title_bar">{$TITLE}</div>
    <table width="80%" class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn"><h2>Detail Dosen</h2></th>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Nama</span></td>
            <td>{$nama}</td>
            <td><span class="field">Fakultas</span></td>
            <td>{$fakultas}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">NIP</span></td>
            <td>{$nip}</td>
            <td><span class="field">Prodi</span></td>
            <td>{$prodi}</td>
        </tr>
    </table>
	
	<div class="overflow_content">
	{$isi_data}
	</div>        
        
