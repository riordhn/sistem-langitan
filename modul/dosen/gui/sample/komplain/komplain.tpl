{if $smarty.get.mode=='komplain'}
    <div class="center_title_bar">Pengajuan Komplain (PTPP)</div>
    <form action="komplain.php" method="post">
        <fieldset style="width: 90%">
            <legend>Pengisian Komplain</legend>
            <div class="field_form">
                <label>Pelapor : </label>
                <input type="text" size="15" readonly="" maxlength="30" value="{$pelapor}"/>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Unit Kerja Tertuju : </label>
                <select name="unit_kerja" id="unit_kerja" class="required">
                    <option value="">Pilih Unit Kerja</option>
                    {foreach $data_unit_kerja as $d}
                        <option value="{$d.ID_UNIT_KERJA}">{$d.NM_UNIT_KERJA} {if $d.DESKRIPSI_UNIT_KERJA!=''}- {$d.DESKRIPSI_UNIT_KERJA}{/if}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Isi Komplain: </label>
                <textarea name="isi" style="resize: none;width: 80%" class="required" rows="5"></textarea>
            </div>
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="komplain.php"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer">Batal</a>
                <input type="hidden" name="mode" value="kirim"/>
                <input type="submit"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" value="Kirim"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='jawaban'}
    <div class="center_title_bar">Detail Komplain (PTPP)</div>
    <fieldset>
        <legend>Komplain (PTPP)</legend>
        <div class="field_form">
            <label>Pelapor : </label>
            <p>{$komplain.NM_PENGGUNA|upper} - {$komplain.STATUS} ({$komplain.WAKTU})</p>
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Isi Komplain: </label>
            <p style="display: inline-block;width: 80%">{if $komplain.NM_KATEGORI!=''}({$komplain.NM_KATEGORI}) {/if}{$komplain.ISI_KOMPLAIN}</p>
        </div>
        {if $komplain.STATUS_KOMPLAIN!=1}
            <div class="field_form">
                <label style="font-size: 1.1em;color: blue;">Respon Tanggapan</label>
            </div>
            <div class="clear-fix"></div>
            <p></p>
            <table cellpadding="0" cellspacing="0" border="0" class="datatable display" >
                <thead>
                    <tr>
                        <th>Penjawab</th>
                        <th>Tindakan</th>
                        <th>Tanggal Tindakan</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $data_tanggapan as $d}
                        <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                            <td style="width: 220px">
                                {$d.PENJAWAB|upper} <br/>
                                {$d.STATUS_PENJAWAB} <br/>
                                {$d.WAKTU_JAWAB}
                            </td>
                            <td>{$d.TINDAKAN}</td>
                            <td>
                                {if $d.TGL_SELESAI_RENCANA==''&&$d.TGL_SELESAI_REALISASI==''}
                                    <span style="color: chocolate">Tidak ada tindak Lanjut</span>
                                {else}
                                    Tgl Realisasi : {$d.TGL_SELESAI_REALISASI}
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
        {/if}
        {if $komplain.KETERANGAN_TANGGAPAN==''}
            <form method="post" action="komplain.php?{$smarty.server.QUERY_STRING}">
                <div class="field_form">
                    <label style="font-size: 1.1em;color: blue;">Penilaian</label>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Nilai</label>
                    <div id="star" style="display: inline-block;"></div>
                    <input type="hidden" name="rate" id="rate" />
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Keterangan Nilai</label>
                    <textarea name="keterangan_nilai" style="resize: none;width: 80%" class="required" rows="5"></textarea>
                </div>
                {literal}
                    <script type="text/javascript">
                        $('#star').raty({
                            score: 0,
                            hints: ['Buruk', 'Kurang Puas', 'Puas', 'Cukup Puas', 'Sangat Memuaskan'],
                            click: function(score, evt) {
                                $('#rate').val(score);
                            }
                        });
                    </script>
                {/literal}
                <div class="clear-fix"></div>
                <div class="bottom_field_form">
                    <input type="submit"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" value="Simpan Penilaian" />
                    <input type="hidden" name="mode" value="penilaian"/>
                    <a href="komplain.php"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer">Kembali</a>
                </div>
            </form>
        {else}
            <div class="field_form">
                <label style="font-size: 1.1em;color: blue;">Penilaian</label>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Nilai</label>
                <div id="star" data-score="{$komplain.NILAI_TANGGAPAN}" style="display: inline-block;"></div>
            </div>
            <div class="clear-fix"></div>
            <div class="field_form">
                <label>Keterangan Nilai</label>
                {$komplain.KETERANGAN_TANGGAPAN}
            </div>
            {literal}
                <script type="text/javascript">
                    $('#star').raty({
                        score: function() {
                            return $(this).attr('data-score');
                        },
                        readOnly: true
                    });
                </script>
            {/literal}
            <div class="clear-fix"></div>
            <div class="bottom_field_form">
                <a href="komplain.php"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer">Kembali</a>
            </div>
        {/if}
    </fieldset>
{else}
    <div class="center_title_bar">Daftar Pengajuan Komplain (PTPP)</div>
    <h2>Data Komplain</h2>
    <a  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" href="komplain.php?mode=komplain">Tambah Komplain</a>
    <p></p>
    <table cellpadding="0" cellspacing="0" border="0" class="datatable display" >
        <thead>
            <tr>
                <th>Unit Kerja</th>
                <th>Isi Komplain</th>
                <th>Waktu Komplain</th>
                <th>Dibaca Oleh</th>
                <th class="center">Status</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_komplain as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td>{$d.NM_UNIT_KERJA} </td>
                    <td>{SplitWord($d.ISI_KOMPLAIN,5)}...</td>
                    <td class="center">{$d.WAKTU}</td>
                    <td>
                        <ul style="margin-left: 5px">
                            {foreach $d.DATA_PEMBACA as $dp}
                                <li>{$dp.NM_PENGGUNA} ({$dp.WAKTU})</li>
                            {/foreach}
                        </ul>
                    </td>
                    <td class="center">
                        {if $d.STATUS_KOMPLAIN==1}
                            <label style="color: #ff0000;font-weight: bold">Belum Terjawab</label><br/>
                        {elseif $d.STATUS_KOMPLAIN==2}
                            <label style="color: #0480be;font-weight: bold">Terjawab</label><br/>
                            <a href="komplain.php?mode=jawaban&k={$d.ID_KOMPLAIN}"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer">Jawaban</a>
                        {elseif $d.STATUS_KOMPLAIN==3}
                            <label style="color: #003399;font-weight: bold">Sedang Tindak Lanjuti</label><br/>
                            <a href="komplain.php?mode=jawaban&k={$d.ID_KOMPLAIN}"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer">Detail</a>
                        {elseif $d.STATUS_KOMPLAIN==4}
                            <label style="color: #00bb00;font-weight: bold">Sudah Selesai</label><br/>
                            <a href="komplain.php?mode=jawaban&k={$d.ID_KOMPLAIN}"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer">Detail</a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{/if}
{literal}
    <script>
        $(document).ready(function() {
            oTable = $('.datatable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "bSort": false
            });
            $('form').validate();
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-M-y'
            }).css({'text-transform': 'uppercase'});
            $("#kategori").autocomplete({
                source: "getKategoriKomplain.php",
                minLength: 3
            });
        });
    </script>
{/literal}