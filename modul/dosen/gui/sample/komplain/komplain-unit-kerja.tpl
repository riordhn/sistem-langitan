{if $smarty.get.mode=='detail'}
    <div class="center_title_bar">Detail Komplain Ke Unit Kerja Terkait</div>
    <fieldset style="width: 98%">
        <legend>Komplain (PTPP)</legend>
        <div class="field_form">
            <label>Pelapor : </label>
            <p>{$komplain.NM_PENGGUNA|upper} - {$komplain.STATUS} ({$komplain.WAKTU})</p>
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Unit Tertuju : </label>
            <p>{$komplain.NM_UNIT_KERJA} {$komplain.DESKRIPSI_UNIT_KERJA}</p>
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Isi Komplain: </label>
            <p style="display: inline-block;width: 80%">{if $komplain.NM_KATEGORI!=''}({$komplain.NM_KATEGORI}) {/if}{$komplain.ISI_KOMPLAIN}</p>
        </div>
        <div class="clear-fix"></div>
        {if $komplain.STATUS_KOMPLAIN==4}
            <div class="field_form">
                <label style="font-size: 1.2em;color: blue;">Respon Tanggapan</label>
            </div>
            <div class="clear-fix"></div>
            <p></p>
            <table cellpadding="0" cellspacing="0" border="0" class="datatable display" >
                <thead>
                    <tr>
                        <th>Penjawab</th>
                        <th>Tindakan</th>
                        <th>Keterangan</th>
                        <th>Tanggal Tindakan</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $data_tanggapan as $d}
                        <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                            <td style="width: 220px">
                                {$d.PENJAWAB|upper} - {$d.STATUS_PENJAWAB}<br/>
                                {$d.WAKTU_JAWAB}
                            </td>
                            <td>{$d.TINDAKAN}</td>
                            <td>{$d.KETERANGAN}</td>
                            <td>
                                {if $d.TGL_SELESAI_REALISASI==''}
                                    <span style="color: chocolate">Tidak ada tindak Lanjut</span>
                                {else}
                                    Tgl Realisasi : {$d.TGL_SELESAI_REALISASI}
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
            {if $komplain.KETERANGAN_TANGGAPAN!=''}
                <div class="field_form">
                    <label style="font-size: 1.2em;color: blue;">Penilaian</label>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Nilai</label>
                    <div id="star" data-score="{$komplain.NILAI_TANGGAPAN}" style="display: inline-block;"></div>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Keterangan Nilai</label>
                    {$komplain.KETERANGAN_TANGGAPAN}
                </div>
                {literal}
                    <script type="text/javascript">
                        $('#star').raty({
                            score: function() {
                                return $(this).attr('data-score');
                            },
                            readOnly: true
                        });
                    </script>
                {/literal}
            {/if}
            <div class="bottom_field_form">
                <a href="komplain-unit-kerja.php"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer">Kembali</a>
            </div>
        {else if $komplain.STATUS_KOMPLAIN==2||$komplain.STATUS_KOMPLAIN==3}
            <div class="field_form">
                <label style="font-size: 1.2em;color: blue;">Respon Tanggapan</label>
            </div>
            <div class="clear-fix"></div>
            <p></p>
            <table cellpadding="0" cellspacing="0" border="0" class="datatable display" >
                <thead>
                    <tr>
                        <th>Penjawab</th>
                        <th>Tindakan</th>
                        <th>Keterangan</th>
                        <th>Tanggal Tindakan</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $data_tanggapan as $d}
                        <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                            <td style="width: 220px">
                                {$d.PENJAWAB|upper} - {$d.STATUS_PENJAWAB}<br/>
                                {$d.WAKTU_JAWAB}
                            </td>
                            <td>{$d.TINDAKAN}</td>
                            <td>{$d.KETERANGAN}</td>
                            <td>
                                {if $d.TGL_SELESAI_REALISASI==''}
                                    <span style="color: chocolate">Tidak ada tindak Lanjut</span>
                                {else}
                                    Realisasi : {$d.TGL_SELESAI_REALISASI}
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
            <form id="jawaban" method="post" action="komplain-unit-kerja.php" style="display: none">
                <div class="field_form">
                    <label style="font-size: 1.2em;color: blue;">Jawaban</label>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Tindakan</label>
                    <textarea name="j_tindakan" style="resize: none;width: 80%" class="required" rows="5"></textarea>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Keterangan</label>
                    <textarea name="j_keterangan" style="resize: none;width: 80%" class="required" rows="5"></textarea>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Tgl Realisasi</label>
                    <input type="text" name="j_tgl_realisasi"  size="15" class="datepicker" readonly="" maxlength="30" />
                </div>
                <div class="clear-fix"></div>
                <div class="bottom_field_form">
                    <input type="hidden" name="mode" value="jawab"/>
                    <a class=" disable-ajax ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" onclick="$('#jawaban').hide();
                            $('#aksi').fadeIn()">Batal</a>
                    <input type="submit"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" value="Submit Jawaban"/>
                </div>
                <input type="hidden" name="id_komplain" value="{$smarty.get.k}"/>
                <input type="hidden" name="unit_kerja" value="{$smarty.get.uk}"/>
            </form>
            <form id="tindakan" method="post" action="komplain-unit-kerja.php" style="display: none">
                <div class="field_form">
                    <label style="font-size: 1.2em;color: blue;">Tindakan</label>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Tindakan</label>
                    <textarea name="t_tindakan" style="resize: none;width: 80%" class="required" rows="5"></textarea>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Keterangan</label>
                    <textarea name="t_keterangan" style="resize: none;width: 80%" class="required" rows="5"></textarea>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Tgl Realisasi</label>
                    <input type="text" name="t_tgl_realisasi"  size="15" class="required datepicker" readonly="" maxlength="30" />
                </div>
                <div class="field_form">
                    <label style="display: inline-block">Unit Kerja Terkait</label>
                    <div style="display: inline-block">
                        {foreach $data_unit_kerja as $d}
                            <input type="checkbox"  name="unit_kerja{$d@index+1}"  value="{$d.ID_UNIT_KERJA}" /> <span>{$d.NM_UNIT_KERJA} -{$d.DESKRIPSI_UNIT_KERJA}</span><br/>
                        {/foreach}
                    </div>
                </div>
                <div class="clear-fix"></div>
                <div class="bottom_field_form">
                    <input type="hidden" name="mode" value="tindakan"/>
                    <input type="hidden" name="unit_kerja" value="{$smarty.get.uk}"/>
                    <input type="hidden" name="jumlah_unit" value="{count($data_unit_kerja)}"/>
                    <a class=" disable-ajax ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" onclick="$('#tindakan').hide();
                            $('#aksi').fadeIn()">Batal</a>
                    <input type="submit"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" value="Submit Tindakan"/>
                </div>
                <input type="hidden" name="id_komplain" value="{$smarty.get.k}"/>
            </form>
            <div class="clear-fix"></div>
            <div id="aksi" class="bottom_field_form">
                <a href="komplain-unit-kerja.php"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer">Kembali</a>
                {if $komplain.STATUS_KOMPLAIN==2}
                    <a class=" disable-ajax ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" onclick="$('#jawaban').fadeIn();
                            $('#aksi').hide()">Tambah Jawaban</a>
                {else if $komplain.STATUS_KOMPLAIN==3}
                    <a class=" disable-ajax ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" onclick="$('#tindakan').fadeIn();
                            $('#aksi').hide()">Tambah Tindakan</a>
                {/if}
            </div>
        {else if $komplain.STATUS_KOMPLAIN==1}
            <form id="jawaban" method="post" action="komplain-unit-kerja.php" style="display: none">
                <div class="field_form">
                    <label style="font-size: 1.2em;color: blue;">Jawaban</label>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Tindakan</label>
                    <textarea name="j_tindakan" style="resize: none;width: 80%" class="required" rows="5"></textarea>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Keterangan</label>
                    <textarea name="j_keterangan" style="resize: none;width: 80%" class="required" rows="5"></textarea>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Tgl Realisasi</label>
                    <input type="text" name="j_tgl_realisasi"  size="15" class="datepicker" readonly="" maxlength="30" />
                </div>
                <div class="clear-fix"></div>
                <div class="bottom_field_form">
                    <input type="hidden" name="unit_kerja" value="{$smarty.get.uk}"/>
                    <input type="hidden" name="mode" value="jawab"/>
                    <a class=" disable-ajax ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" onclick="$('#jawaban').hide();
                            $('#aksi').fadeIn()">Batal</a>
                    <input type="submit"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" value="Submit Jawaban"/>
                </div>
                <input type="hidden" name="id_komplain" value="{$smarty.get.k}"/>
            </form>
            <form id="tindakan" method="post" action="komplain-unit-kerja.php" style="display: none">
                <div class="field_form">
                    <label style="font-size: 1.2em;color: blue;">Tindakan</label>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Tindakan</label>
                    <textarea name="t_tindakan" style="resize: none;width: 80%" class="required" rows="5"></textarea>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Keterangan</label>
                    <textarea name="t_keterangan" style="resize: none;width: 80%" class="required" rows="5"></textarea>
                </div>
                <div class="clear-fix"></div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Tgl Realisasi</label>
                    <input type="text" name="t_tgl_realisasi"  size="15" class="required datepicker" readonly="" maxlength="30" />
                </div>
                <div class="field_form">
                    <label style="display: inline-block">Unit Kerja Terkait</label>
                    <div style="display: inline-block">
                        {foreach $data_unit_kerja as $d}
                            <input type="checkbox"  name="unit_kerja{$d@index+1}"  value="{$d.ID_UNIT_KERJA}" /> <span>{$d.NM_UNIT_KERJA} -{$d.DESKRIPSI_UNIT_KERJA}</span><br/>
                        {/foreach}
                    </div>
                </div>
                <div class="clear-fix"></div>
                <div class="bottom_field_form">
                    <input type="hidden" name="mode" value="tindakan"/>
                    <input type="hidden" name="unit_kerja" value="{$smarty.get.uk}"/>
                    <input type="hidden" name="jumlah_unit" value="{count($data_unit_kerja)}"/>
                    <a class=" disable-ajax ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" onclick="$('#tindakan').hide();$('#aksi').fadeIn()">Batal</a>
                    <input type="submit"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" value="Submit Tindakan"/>
                </div>
                <input type="hidden" name="id_komplain" value="{$smarty.get.k}"/>
            </form>
            <div class="clear-fix"></div>
            <div id="aksi" class="bottom_field_form">
                <a href="komplain-unit-kerja.php"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer">Kembali</a>
                <a class=" disable-ajax ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" onclick="$('#jawaban').fadeIn();
                        $('#aksi').hide()">Jawab</a>
                <a class=" disable-ajax ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer" onclick="$('#tindakan').fadeIn();
                        $('#aksi').hide()">Tindak Lanjuti</a>
            </div>
        {/if}
    </fieldset>
{else}
    <div class="center_title_bar">Komplain (PTPP)</div>
    <h2>Data Komplain Ke Unit Kerja Terkait</h2>
    <table cellpadding="0" cellspacing="0" border="0" class="datatable display" >
        <thead>
            <tr>
                <th>Pelapor</th>
                <th>Unit Kerja Dituju</th>
                <th>Isi Komplain</th>
                <th>Dibaca</th>
                <th class="center">Status</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_komplain as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td style="width: 220px">
                        {$d.NM_PENGGUNA|upper} - {$d.STATUS}<br/>
                        {$d.WAKTU}
                    </td>
                    <td style="width: 220px">
                        ({$d.NM_UNIT_KERJA})
                    </td>
                    <td>{SplitWord($d.ISI_KOMPLAIN,10)}...</td>
                    <td>
                        {if $d.STATUS_BACA>0}
                            <i style="color: green">Sudah</i>
                        {else}
                            <i style="color: red">Belum</i>
                        {/if}
                    </td>
                    <td class="center">
                        {if $d.STATUS_KOMPLAIN==1}
                            <label style="color: #ff0000;font-weight: bold">Belum Terjawab</label><br/>
                            <a href="komplain-unit-kerja.php?mode=detail&k={$d.ID_KOMPLAIN}&uk={$d.ID_UNIT_TERTUJU}"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer">Aksi</a>
                        {elseif $d.STATUS_KOMPLAIN==2}
                            <label style="color: #0480be;font-weight: bold">Terjawab</label><br/>
                            <a href="komplain-unit-kerja.php?mode=detail&k={$d.ID_KOMPLAIN}&uk={$d.ID_UNIT_TERTUJU}"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer">Jawaban</a>
                        {elseif $d.STATUS_KOMPLAIN==3}
                            <label style="color: #003399;font-weight: bold">Sedang Tindak Lanjuti</label><br/>
                            <a href="komplain-unit-kerja.php?mode=detail&k={$d.ID_KOMPLAIN}&uk={$d.ID_UNIT_TERTUJU}"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer">Detail</a>
                        {elseif $d.STATUS_KOMPLAIN==4}
                            <label style="color: #00bb00;font-weight: bold">Sudah Selesai</label><br/>
                            <a href="komplain-unit-kerja.php?mode=detail&k={$d.ID_KOMPLAIN}"  class="ui-button ui-state-default ui-corner-all" style="padding:5px;cursor:pointer">Detail</a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{/if}
{literal}
    <script type="text/javascript">
        $(document).ready(function() {
            oTable = $('.datatable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "bSort": false
            });
            $(".kategori").autocomplete({
                source: "getKategoriKomplain.php",
                minLength: 3
            });
            $('form').validate();
            $('#jawaban').validate();
            $('#tindakan').validate();
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-M-y'
            }).css({'text-transform': 'uppercase'});
        });
    </script>
{/literal}