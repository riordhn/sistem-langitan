<form id="form_delete_referensi" method="post" action="gbpp.php?id_kurikulum_mk={$data_deskripsi_mk.ID_KURIKULUM_MK}">
    <table class="ui-widget" width="665px">
        <tr class="ui-widget-header">
            <th class="header-coloumn"><h2>Delete Referensi Mata Kuliah</h2></th>
        </tr>
        <tr class="ui-widget-content">
            <td class="center">
                <p>Apakah anda yakin untuk menghapus Referensi <i><b>{$data_referensi_mk.REFERENSI_MK|substr:0:-10}.........</b></i>  ?</p>
                <input type="hidden" name="id_referensi_mk" value="{$data_referensi_mk.ID_REFERENSI_MK}" />
                <input type="hidden" name="mode" value="delete_referensi"/>
                <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#form_delete_referensi').submit();$('#dialog_delete_referensi').dialog('close');">Ya</span>
                <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_delete_referensi').dialog('close')">Cancel</span>
            </td>
        </tr>
    </table>
</form>