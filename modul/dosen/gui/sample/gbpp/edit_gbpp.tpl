<div class="center_title_bar">GBPP MATA KULIAH-EDIT</div>
<form id="form_add" action="gbpp.php?id_kurikulum_mk={$data_deskripsi_mk.ID_KURIKULUM_MK}" method="post">
    <table class="ui-widget" style="width: 100%;">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn"><h2>Edit GBPP Mata Kuliah</h2></th>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Nama Mata Kuliah</span></td>
            <td>{$data_deskripsi_mk.NM_MATA_KULIAH}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Kode Mata Kuliah</span></td>
            <td>{$data_deskripsi_mk.KD_MATA_KULIAH}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Beban Studi</span></td>
            <td>{$data_deskripsi_mk.KREDIT_SEMESTER}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Tingkat Semester</span></td>
            <td>{$data_deskripsi_mk.TINGKAT_SEMESTER}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Urutan</span></td>
            <td>
                <input type="text" name="urutan" maxlength="2"  size="2" value="{$data_gbpp_mk.URUTAN_GBPP}" class="required number"/>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">TIK</span></td>
            <td>
                <textarea cols="70" rows="4" name="tik" class="required">{$data_gbpp_mk.TIK_MATA_KULIAH}</textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Pokok Bahasan</span></td>
            <td>
                <textarea cols="70" rows="4" name="pokok" class="required">{$data_gbpp_mk.POKOK_BAHASAN}</textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Sub Pokok Bahasan</span></td>
            <td>
                <textarea cols="70" rows="8" name="sub_pokok" class="required">{$data_gbpp_mk.SUB_POKOK_BAHASAN}</textarea>
                <br/><span style="color: green;font-size: 13px;font-family: 'Trebuchet MS';font-style: italic">Untuk Menjadikan Pokok Bahasan menjadi Sebuah List Pisahkan List Pokok Bahasan dengan koma / ","</span>
                <br/>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="submit" style="cursor: pointer;padding:5px;" class="ui-button ui-state-default ui-corner-all" value="Update" />
                <a style="cursor: pointer;padding:5px;" class="ui-button ui-state-default ui-corner-all" href="gbpp.php?id_kurikulum_mk={$data_deskripsi_mk.ID_KURIKULUM_MK}">Kembali</a>
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id_gbpp_mk" value="{$smarty.get.id_gbpp_mk}" />
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#form_add').validate();
    </script>
{/literal}