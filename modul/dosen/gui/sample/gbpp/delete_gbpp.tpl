<form id="form_delete_gbpp" method="post" action="gbpp.php?id_kurikulum_mk={$data_deskripsi_mk.ID_KURIKULUM_MK}">
    <table class="ui-widget" width="665px">
        <tr class="ui-widget-header">
            <th class="header-coloumn"><h2>Delete GBPP Mata Kuliah</h2></th>
        </tr>
        <tr class="ui-widget-content">
            <td class="center">
                <p>Apakah anda yakin untuk menghapus GBPP ini <i><b>{$data_gbpp_mk.POKOK_BAHASAN|substr:0:-10}...</b></i> ?</p>
                <input type="hidden" name="id_gbpp_mk" value="{$data_gbpp_mk.ID_GBPP_MATA_KULIAH}" />
                <input type="hidden" name="mode" value="delete"/>
                <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#form_delete_gbpp').submit();$('#dialog_delete_gbpp').dialog('close');">Ya</span>
                <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_delete_gbpp').dialog('close')">Cancel</span>
            </td>
        </tr>
    </table>
</form>