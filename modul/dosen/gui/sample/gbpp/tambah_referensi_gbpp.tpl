<div class="center_title_bar">GBPP REFERENSI MATA KULIAH-TAMBAH</div>
<form id="form_add" action="gbpp.php?id_kurikulum_mk={$data_deskripsi_mk.ID_KURIKULUM_MK}" method="post">
    <table class="ui-widget" style="width: 100%;">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn"><h2>Tambah Referensi Mata Kuliah</h2></th>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Nama Mata Kuliah</span></td>
            <td>{$data_deskripsi_mk.NM_MATA_KULIAH}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Kode Mata Kuliah</span></td>
            <td>{$data_deskripsi_mk.KD_MATA_KULIAH}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Beban Studi</span></td>
            <td>{$data_deskripsi_mk.KREDIT_SEMESTER}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Tingkat Semester</span></td>
            <td>{$data_deskripsi_mk.TINGKAT_SEMESTER}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Referensi</span></td>
            <td>
                <textarea cols="70" rows="5" name="referensi" class="required"></textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="submit" style="cursor: pointer;padding:5px;" class="ui-button ui-state-default ui-corner-all" value="Tambah" />
                <a style="cursor: pointer;padding:5px;" class="ui-button ui-state-default ui-corner-all" href="gbpp.php?id_kurikulum_mk={$data_deskripsi_mk.ID_KURIKULUM_MK}">Kembali</a>
                <input type="hidden" name="mode" value="tambah_referensi"/>
                <input type="hidden" name="id_kurikulum_mk" value="{$smarty.get.id_kurikulum_mk}" />
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#form_add').validate();
    </script>
{/literal}