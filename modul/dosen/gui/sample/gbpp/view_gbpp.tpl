<div class="center_title_bar">GPP MATA AJAR</div>
<table>
    <tr class="ui-widget-content">
        <td><span>Mata Kuliah</span></td>
        <td>
            <select id="select_mata_kuliah" name="mata_kuliah">
                {foreach $data_mata_kuliah as $data}
                    <option value="{$data.ID_KURIKULUM_MK}" {if $data.ID_KURIKULUM_MK==$id_kurikulum_mk}selected="true" {/if}>{$data.NM_MATA_KULIAH|upper} Prodi ({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                {/foreach}
            </select>
        </td>
    </tr>
</table>
<div id="detail_gbpp">
    <table style="width: 100%;">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Deskripsi Mata Kuliah</th>
        </tr>
        <tr class="ui-widget-content">
            <td style="width: 30%"><span class="field">Nama Mata Kuliah</span></td>
            <td>{$data_deskripsi_mk.NM_MATA_KULIAH}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Kode Mata Kuliah</span></td>
            <td>{$data_deskripsi_mk.KD_MATA_KULIAH}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Beban Studi</span></td>
            <td>{$data_deskripsi_mk.KREDIT_SEMESTER}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Tingkat Semester</span></td>
            <td>{$data_deskripsi_mk.TINGKAT_SEMESTER}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Program Studi</span></td>
            <td>({$data_deskripsi_mk.NM_JENJANG}) {$data_deskripsi_mk.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Prasyarat Mata Kuliah</span></td>
            <td>
                <ol>
                    {foreach $data_prasyarat_mk as $data}
                        <li>{$data.NM_MATA_KULIAH} ({$data.KD_MATA_KULIAH})</li>
                    {/foreach}
                </ol>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">Deskripsi Mata Kuliah</span></td>
            <td>{$data_deskripsi_mk.DESKRIPSI_MK}</td>
        </tr>
        <tr class="ui-widget-content">
            <td><span class="field">TIU</span></td>
            <td>{$data_deskripsi_mk.TIU_MATA_KULIAH}</td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <a style="cursor: pointer;padding:5px;" class="ui-button ui-state-default ui-corner-all" href="gbpp.php?mode=edit_deskripsi_mk&id_kurikulum_mk={$data_deskripsi_mk.ID_KURIKULUM_MK}">Edit</a>
            </td>
        </tr>
    </table>

    <table style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="3" class="header-coloumn">Referensi Mata Kuliah</th>
        </tr>
        <tr class="ui-widget-content">
            <th style="width: 10%" class="center">No</th>
            <th style="width: 75%" class="center">Referensi</th>
            <th style="width: 15%" class="center">Operasi</th>
        </tr>
        {foreach $data_referensi_mk as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.REFERENSI_MK}</td>
                <td class="center">
                    <a style="cursor: pointer;padding:5px;" class="ui-button ui-state-default ui-corner-all" href="gbpp.php?mode=edit_referensi&id_kurikulum_mk={$data_deskripsi_mk.ID_KURIKULUM_MK}&id_referensi_mk={$data.ID_REFERENSI_MK}">Edit</a>
                    <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_delete_referensi').dialog('open').load('gbpp.php?mode=delete_referensi&id_kurikulum_mk={$data_deskripsi_mk.ID_KURIKULUM_MK}&id_referensi_mk={$data.ID_REFERENSI_MK}')">Delete</span>
                </td>
            </tr>
        {/foreach}
        <tr class="ui-widget-content">
            <td colspan="3" class="center">
                <a style="cursor: pointer;padding:5px;" class="ui-button ui-state-default ui-corner-all" href="gbpp.php?mode=tambah_referensi&id_kurikulum_mk={$data_deskripsi_mk.ID_KURIKULUM_MK}">Tambah</a>
            </td>
        </tr>
    </table>

    <table style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="5" class="header-coloumn">Detail GBPP Mata Kuliah</th>
        </tr>
        <tr class="ui-widget-content">
            <th style="width: 10%" class="center">No</th>
            <th style="width: 25%" class="center">TIK</th>
            <th style="width: 25%" class="center">Pokok Bahasan</th>
            <th style="width: 25%" class="center">Sub Pokok Bahasan</th>
            <th style="width: 15%" class="center">Operasi</th>
        </tr>
        {foreach $data_gbpp_mk as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.TIK_MATA_KULIAH}</td>
                <td>{$data.POKOK_BAHASAN}</td>
                <td>
                    {assign var=sub_pokok_bahasan value=","|explode:$data.SUB_POKOK_BAHASAN}
                    <ul>
                        {foreach $sub_pokok_bahasan as $list_sub_pokok}
                            <li>{$list_sub_pokok}</li>
                        {/foreach}
                    </ul>
                </td>
                <td class="center">
                    <a style="cursor: pointer;padding:5px;" class="ui-button ui-state-default ui-corner-all" href="gbpp.php?mode=edit&id_kurikulum_mk={$data_deskripsi_mk.ID_KURIKULUM_MK}&id_gbpp_mk={$data.ID_GBPP_MATA_KULIAH}">Edit</a>
                    <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_delete_gbpp').dialog('open').load('gbpp.php?mode=delete&id_kurikulum_mk={$data_deskripsi_mk.ID_KURIKULUM_MK}&id_gbpp_mk={$data.ID_GBPP_MATA_KULIAH}')">Delete</span>
                </td>
            </tr>
        {/foreach}
        <tr class="ui-widget-content">
            <td colspan="5" class="center">
                <a style="cursor: pointer;padding:5px;" class="ui-button ui-state-default ui-corner-all" href="gbpp.php?mode=tambah&id_kurikulum_mk={$data_deskripsi_mk.ID_KURIKULUM_MK}">Tambah</a>
            </td>
        </tr>
    </table>
</div>
<div id="dialog_delete_referensi" title="Delete Referensi Mata Kuliah"></div>
<div id="dialog_delete_gbpp" title="Delete GBPP Mata Kuliah"></div>
{literal}
    <script type="text/javascript">
        $('#select_mata_kuliah').change(function(){
            $.ajax({
                    url : '/modul/dosen/gbpp.php',
                    type : 'GET',
                    data : 'id_kurikulum_mk='+$(this).val()+'&mode=view',
                    beforeSend : function(){
                            $('#detail_gbpp').html('<div style="width: 100%;" align="center"><p></p><p></p><img src="/js/loading.gif" /></div>');
                    },
                    success :function(data){
                            $('#content').html(data);
                    }
            })
        });
        $( "#dialog_delete_referensi" ).dialog({
            width:'720',
            height:'250',
            modal: true,
            resizable:false,
            autoOpen:false
        });
        $( "#dialog_delete_gbpp" ).dialog({
            width:'720',
            height:'250',
            modal: true,
            resizable:false,
            autoOpen:false
        });
    </script>
{/literal}