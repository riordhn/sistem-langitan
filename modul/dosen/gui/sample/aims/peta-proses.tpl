<div class="center_title_bar">Daftar Peta Proses Universitas</div>
{if $tutup!=''}
    {$tutup}
{else}
    <table style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">DOKUMEN</th>
        </tr>
        {$index_pedoman=1}
        {foreach $data_dokumen as $d}
            {if $d.ID_MM_JENIS_PEDOMAN=='3'}
                {foreach $d.LINGKUP as $l}
                    <tr style="background-color: antiquewhite">
                        <td colspan="4" class="center"><b style="font-size: 1.3em">{$l.NAMA_GROUP_DOKUMEN} ({$l.KODE_GROUP_DOKUMEN|upper})</b></td>
                    </tr>
                    <tr>
                        <th>NO</th>
                        <th>DOKUMEN</th>
                        <th  style="width: 140px">KODE</th>
                        <th>FILE</th>
                    </tr>
                    {foreach $l.DOKUMEN as $d}
                        {$convert_file=$d.NAMA_FILE}
                        <tr>
                            <td  style="width: 20px">{$d@index+1}</td>
                            <td>{$d.NAMA_DOKUMEN}</td>
                            <td>{$d.KODE_DOKUMEN|upper}</td>
                            <td class="center">{if $d.NAMA_FILE!=''}<a class="disable-ajax" target="_blank" href="{$base_url}modul/manajemen-mutu/dokumen-view.php?l={$l.NAMA_GROUP_DOKUMEN}&t={$d.NAMA_DOKUMEN}&key={base64_encode('nambisembilu_cybercampus')}&file={base64_encode($convert_file)}">{$d.KODE_DOKUMEN|upper}</a>{else}<span style="color: red">File Tidak Ada</span>{/if}</td>
                        </tr>
                    {foreachelse}
                        <tr>
                            <td colspan="4" class="kosong">Data Masih Kosong</td>
                        </tr>
                    {/foreach}
                {/foreach}
                {$index_pedoman=$index_pedoman+1}
            {/if}
        {/foreach}
    </table>
{/if}