<div class="center_title_bar">Pengajuan Penelitian</div>

{if $id_jabatan_pegawai == 11}
<script type="text/javascript">
    var mode_app = 'approve-departemen';
    var mode_rej = 'reject-departemen';
</script>
{/if}
{if $id_jabatan_pegawai == 5}
<script type="text/javascript">
    var mode_app = 'approve-dekan';
    var mode_rej = 'reject-dekan';
</script>
{/if}
<script type="text/javascript">
    function ApprovePenelitian(id_penelitian) 
    {
        if (confirm('Apakah penelitian ini akan di setujui ?') == false) { return; }
    
        $.ajax({
            url: 'app-penelitian.php',
            type: 'POST',
            data: 'mode='+mode_app+'&id_penelitian='+id_penelitian,
            success: function(data) {
                if (data == 1)
                {
                    alert('Penelitian berhasil disetujui');
                    window.location.reload();
                }
                else
                    alert('Penelitian gagal disetujui');
            }
        });
    }
    
    function RejectPenelitian(id_penelitian) 
    {
        if (confirm('Apakah penelitian ini akan di tolak ?') == false) { return; }
    
        $.ajax({
            url: 'app-penelitian.php',
            type: 'POST',
            data: 'mode='+mode_rej+'&id_penelitian='+id_penelitian,
            success: function(data) {
                if (data == 1)
                {
                    alert('Penelitian berhasil ditolak');
                    window.location.reload();
                }
                else
                    alert('Penelitian gagal ditolak');
            }
        });
    }
</script>

<table class="ui-widget">
    <tr class="ui-widget-header">
        <th class="header-coloumn">No</th>
        <th class="header-coloumn">Nama Dosen</th>
        <th class="header-coloumn">Judul</th>
        <th class="header-coloumn">Jenis</th>
        <th class="header-coloumn">Tahun</th>
        <th class="header-coloumn">Status</th>
        <th class="header-coloumn">Aksi</th>
    </tr>
    {foreach $penelitian_set as $p}
    <tr class="ui-widget-content">
        <td class="center">{$p@index + 1}</td>
        <td>{$p.NM_PENGGUNA}</td>
        <td>{$p.JUDUL}</td>
        <td>{$p.NAMA_JENIS}</td>
        <td class="center">{$p.TAHUN}</td>
        <td class="center">
            {if $id_jabatan_pegawai == 11}
                {if $p.TGL_APPROVE_DEPARTEMEN != ''}
                    Disetujui
                {else if $p.TGL_REJECT_DEPARTEMEN != ''}
                    Ditolak
                {else}
                    --
                {/if}
            {else if $id_jabatan_pegawai == 5}
                {if $p.TGL_APPROVE_DEKAN != ''}
                    Disetujui
                {else if $p.TGL_REJECT_DEKAN != ''}
                    Ditolak
                {else}
                    --
                {/if}
            {/if}
        </td>
        <td class="center">
            <a href="app-penelitian.php?mode=edit&id_penelitian={$p.ID_PENELITIAN}" title="Lihat form"><img src="../../img/dosen/search.png" /></a>
            {if $id_jabatan_pegawai == 11}
                {if $p.TGL_APPROVE_DEPARTEMEN == ''}
                <span onclick="ApprovePenelitian({$p.ID_PENELITIAN})" title="Setujui" style="cursor: pointer"><img src="../../img/dosen/ok.png" /></span>
                {/if}
                {if $p.TGL_REJECT_DEPARTEMEN == ''}
                <span onclick="RejectPenelitian({$p.ID_PENELITIAN})" title="Ditolak" style="cursor: pointer"><img src="../../img/dosen/cancel.png" /></span>
                {/if}
            {else if $id_jabatan_pegawai = 5}
                {if $p.TGL_APPROVE_DEKAN == ''}
                <span onclick="ApprovePenelitian({$p.ID_PENELITIAN})" title="Setujui" style="cursor: pointer"><img src="../../img/dosen/ok.png" /></span>
                {/if}
                {if $p.TGL_REJECT_DEKAN == ''}
                <span onclick="RejectPenelitian({$p.ID_PENELITIAN})" title="Ditolak" style="cursor: pointer"><img src="../../img/dosen/cancel.png" /></span>
                {/if}
            {/if}
        </td>
    </tr>
    {/foreach}
</table>