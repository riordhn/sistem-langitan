<div class="center_title_bar">Form Penelitian</div>

<table class="ui-widget" width="100%">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="2">Penelitian</th>
    </tr>
    <tr class="ui-widget-content">
        <td style="width: 20%"><strong>Judul</strong></td>
        <td>{$p.JUDUL}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Jenis Penelitian</strong></td>
        <td>{$p.NAMA_JENIS}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Jenis SKIM</strong></td>
        <td>{$p.NAMA_SKIM}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Bidang Penelitian</strong></td>
        <td>{$p.NAMA_BIDANG}
        </td>
    </tr>
</table>
    
<table class="ui-widget" width="100%">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="4">Data Pribadi</th>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Nama</strong></td>
        <td colspan="3">{$p.NM_PENGGUNA}</td>
    </tr>
    <tr class="ui-widget-content">
        <td width="20%"><strong>Gelar Depan</strong></td>
        <td width="30%">{$p.GELAR_DEPAN}</td>
        <td width="20%"><strong>Gelar Belakang</strong></td>
        <td width="30%">{$p.GELAR_BELAKANG}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Jenis Kelamin</strong></td>
        <td colspan="3">{$p.JENIS_KELAMIN}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>NIP</strong></td>
        <td>{$p.NIP}</td>
        <td><strong>Golongan</strong></td>
        <td>{$p.NM_GOLONGAN}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Pendidikan Terakhir</strong></td>
        <td>{$p.NM_JENJANG_PENDIDIKAN}</td>
        <td><strong>Jabatan Fungsional</strong></td>
        <td>{$p.NM_JABATAN_FUNGSIONAL}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Jabatan Struktural</strong></td>
        <td colspan="3">{$p.NM_JABATAN_PEGAWAI}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Fakultas</strong></td>
        <td>{$p.NM_FAKULTAS}</td>
        <td><strong>Jurusan</strong></td>
        <td>{$p.NM_PROGRAM_STUDI}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Bidang Ilmu</strong></td>
        <td colspan="3">{$p.NAMA_BIDANG_ILMU}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Alamat Kantor</strong></td>
        <td colspan="3">
            {$p.ALAMAT_KANTOR}<br/>
            Telp : {$p.TELP_KANTOR}<br/>
            Fax : {$p.FAX_KANTOR}<br/>
            Email : {$p.EMAIL_KANTOR}
            
        </td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Alamat Rumah</strong></td>
        <td colspan="3">{$p.ALAMAT_RUMAH}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Telepon Rumah</strong></td>
        <td>{$p.TELP_RUMAH}</td>
        <td><strong>Email</strong></td>
        <td>{$p.EMAIL}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Mata Kuliah Yang Diampu</strong></td>
        <td colspan="3">
            <ol>
            </ol>
        </td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Penelitian Terakhir</strong></td>
        <td colspan="3">
            <ol>
            </ol>
        </td>
    </tr>
</table>
            
<table class="ui-widget" width="100%">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="2">Deskripsi Penelitian</th>
    </tr>
    <tr class="ui-widget-content">
        <td style="width: 20%"><strong>Lokasi Penelitian</strong></td>
        <td>{$p.LOKASI}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Jangka Waktu Penelitian</strong></td>
        <td>{$p.JANGKA_WAKTU}</td>
    </tr>
</table>
            
<table class="ui-widget" width="100%">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="5">Anggota</th>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>NIP / NIK / NIM</strong></td>
        <td><strong>Nama Anggota</strong></td>
        <td><strong>Gelar Depan</strong></td>
        <td><strong>Gelar Belakang</strong></td>
        <td width="50px"><strong>Aksi</strong></td>
    </tr>
    {foreach $penelitian_anggota_set as $pa}
    <tr class="ui-widget-content" id="pa{$pa.ID_PENELITIAN_ANGGOTA}">
        <td>{$pa.NIP_ANGGOTA}</td>
        <td>{$pa.NAMA_ANGGOTA}</td>
        <td>{$pa.GELAR_DEPAN}</td>
        <td>{$pa.GELAR_BELAKANG}</td>
        <td>
            <button onclick="HapusAnggota({$pa.ID_PENELITIAN_ANGGOTA}); return false;" style="margin: 0px; font-size: 11px;">Hapus</button>
        </td>
    </tr>
    {/foreach}
</table>
        
<table class="ui-widget" width="100%">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="4">Ketua Tim Peneliti Mitra (TPM)</th>
    </tr>
    <tr class="ui-widget-content">
        <td style="width: 20%"><strong>Nama</strong></td>
        <td colspan="3">{$p.TPM_NAMA}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Jenis Kelamin</strong></td>
        <td colspan="3">{if $p.TPM_JENIS_KELAMIN == 'L'}Laki-Laki{else}Perempuan{/if}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>NIP</strong></td>
        <td style="width: 30%">{$p.TPM_NIP}</td>
        <td><strong>Golongan</strong></td>
        <td style="width: 30%">{$p.TPM_NM_GOLONGAN}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Jabatan Fungsional</strong></td>
        <td>{$p.TPM_JABATAN_FUNGSIONAL}</td>
        <td><strong>Jabatan Struktural</strong></td>
        <td>{$p.TPM_JABATAN_STRUKTURAL}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Perguruan Tinggi</strong></td>
        <td colspan="3">{$p.TPM_PT}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Fakultas</strong></td>
        <td>{$p.TPM_FAKULTAS}</td>
        <td><strong>Jurusan</strong></td>
        <td>{$p.TPM_PRODI}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Alamat Kantor</strong></td>
        <td colspan="3">
            {$p.TPM_ALAMAT_KANTOR}
            Telp : {$p.TPM_TELP_KANTOR}
            Fax : {$p.TPM_FAX_KANTOR}
        </td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Alamat Rumah</strong></td>
        <td colspan="3">{$p.TPM_ALAMAT_RUMAH}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Telepon Rumah</strong></td>
        <td>{$p.TPM_TELP_RUMAH}</td>
        <td><strong>Email</strong></td>
        <td>{$p.TPM_EMAIL}</td>
    </tr>
</table>
    
<table class="ui-widget" width="100%">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="4">Pembiayaan</th>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Biaya Tahun Ke</strong></td>
        <td><strong>Biaya diajukan ke Dikti</strong></td>
        <td><strong>Biaya dari Instansi lain</strong></td>
        <td width="50px"><strong>Aksi</strong></td>
    </tr>
    {foreach $penelitian_biaya_set as $pb}
    <tr class="ui-widget-content" id="pb{$pb.ID_PENELITIAN_BIAYA}">
        <td>Tahun Ke-{$pb.TAHUN_KE}</td>
        <td>Rp {number_format($pb.BESAR_BIAYA_DIKTI,2)}</td>
        <td>Rp {number_format($pb.BESAR_BIAYA_LAIN,2)}</td>
        <td>
            <button onclick="HapusBiaya({$pb.ID_PENELITIAN_BIAYA}); return false;" style="margin: 0px; font-size: 11px;">Hapus</button>
        </td>
    </tr>
    {/foreach}
</table>
        
<table class="ui-widget" width="100%">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="2">Data Dekan</th>
    </tr>
    <tr class="ui-widget-content">
        <td width="20%"><strong>Nama Dekan</strong></td>
        <td>{$p.NM_DEKAN}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>NIP Dekan</strong></td>
        <td>{$p.NIP_DEKAN}</td>
    </tr>
</table>
        
<table class="ui-widget" width="100%">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="2">Data Ketua Lembaga Penelitian</th>
    </tr>
    <tr class="ui-widget-content">
        <td width="20%"><strong>Nama Ketua</strong></td>
        <td>{$p.NAMA_KETUA_LEMBAGA}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>NIP Ketua</strong></td>
        <td>{$p.NIP_KETUA_LEMBAGA}</td>
    </tr>
</table>
        
<table class="ui-widget" width="100%">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="2">Data Pengisian</th>
    </tr>
    <tr class="ui-widget-content">
        <td width="20%"><strong>Kota</strong></td>
        <td>{$p.KOTA}</td>
    </tr>
    <tr class="ui-widget-content">
        <td><strong>Tanggal Pengisian</strong></td>
        <td>{$p.TGL_PENELITIAN|date_format:"%d %B %Y"}</td>
    </tr>
</table>
<br/>
<div class="center"><a href="app-penelitian.php">Kembali</a></div>