{if $smarty.get.mode==''}
    <div class="center_title_bar">HISTORY DOSEN</div> 
    <table width="90%" class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Detail Dosen</th>
        </tr>
        <tr>
            <td><span class="field">Nama</span></td>
            <td>{$nama|upper}</td>
            <td><span class="field">Fakultas</span></td>
            <td>{$fakultas|upper}</td>
        </tr>
        <tr>
            <td><span class="field">NIP</span></td>
            <td>{$nip|upper}</td>
            <td><span class="field">Prodi</span></td>
            <td>{$prodi|upper}</td>
        </tr>
    </table>        

    <table width="90%" class="ui-widget">
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="5">History Mengajar Dosen</th>
        </tr>
        <tr>
            <th>No</th>
            <th>Kode Mata Kuliah</th>
            <th>Nama Mata Kuliah</th>
            <th>Jumlah Mahasiswa</th>
            <th>Detail</th>
        </tr>
        {foreach $data_mata_kuliah as $mk}
            <tr>
                <td>{$mk@index+1}</td>
                <td>{$mk.KD_MATA_KULIAH}</td>
                <td>{$mk.NM_MATA_KULIAH}</td>
                <td class="center">{$mk.JUMLAH_MHS}</td>
                <td>
                    <span class="ui-button ui-corner-all ui-state-default" onclick="$('#dialog-detail-kelas').load('history.php?mode=detail&id_mk={$mk.ID_MATA_KULIAH}').dialog('open')" style="padding: 5px;cursor: pointer">Detail</span>
                </td>
            </tr>
        {/foreach}
    </table>
    <div id="dialog-detail-kelas" title="Detail Kelas Mata Kuliah"></div>
{else}
    <table width="90%" class="ui-widget">
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="4">History Kelas Mata Kuliah</th>
        </tr>
        <tr>
            <th>No</th>
            <th>Nama Mata Kuliah (Kelas)</th>
            <th>Semester</th>
            <th>Jumlah Mahasiswa</th>
        </tr>
        {foreach $data_kelas_mk as $kmk}
            <tr>
                <td>{$kmk@index+1}</td>
                <td>{$kmk.NM_MATA_KULIAH} Kelas {$kmk.NM_KELAS}</td>
                <td>{$kmk.NM_SEMESTER} {$kmk.TAHUN_AJARAN}</td>
                <td class="center">{$kmk.JUMLAH_MHS}</td>
            </tr>
        {/foreach}
    </table>
{/if}
{literal}
    <script type="text/javascript">             
            $( "#dialog-detail-kelas" ).dialog({
                    width:'700',
                    modal: true,
                    resizable:false,
                    autoOpen:false
            });
    </script>
{/literal}

