<div class="center_title_bar">Add New Trainig</div> 
<form method="post" id="form_add" name="form" action="training.php">
	<table class="ui-widget">
		<tr class="ui-widget-header">
			<th colspan="2" class="header-coloumn"><h2>Add Training</h2></th>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Nama Training</span></td>
			<td>
				<textarea name="nama" class="required"></textarea>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Jenis Training</span></td>
			<td>
				<select id="input_jenis" name="jenis">
	                <option value="Workshop">Workshop</option>
                    <option value="Seminar">Seminar</option>
                    <option value="Pelatihan">Pelatihan</option>
                </select>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Tahun</span></td>
			<td>
				<input type="text" name="tahun" class="required number" />
			</td>
		</tr>

		<tr class="ui-widget-content">
			<td><span>Instansi</span></td>
			<td>
				<input type="text" name="instansi" class="required" size="50" />
			</td>
		</tr>
        <tr class="ui-widget-content">
			<td><span>Peran</span></td>
			<td>
				<select id="input_peran" name="peran">
	                <option value="Anggota">Anggota</option>
                    <option value="Trainer">Trainer</option>
                    <option value="Nara Sumber">Nara Sumber</option>
                </select>
			</td>
		</tr>
        <tr class="ui-widget-content">
			<td><span>Tingkat</span></td>
			<td>
				<select id="input_tingkat" name="tingkat">
	                <option value="Lokal">Lokal</option>
                    <option value="Nasional">Nasional</option>
                    <option value="Internasional">Internasional</option>
                </select>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Document</span></td>
			<td>
				<input type="file" name="file" />
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td colspan="2" style="text-align:right">
				<input type="hidden" name="mode" value="add" />
				<a id="save" style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all">Save</a>
				<a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="training.php">Cancel</a>
			</td>
		</tr>
	</table>
</form>
{literal}
<script type="text/javascript">
$('#save').click(function(){
    $('#form_add').submit();
});
$('#form_add').validate();
</script>
{/literal}