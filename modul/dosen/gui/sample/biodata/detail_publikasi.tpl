<table class="ui-widget" width="500px">
		<tr class="ui-widget-header">
			<th colspan="2" class="header-coloumn"><h2>Detail Publikasi</h2></th>
		</tr>
		<tr class="ui-widget-content">
			<td width="10%"><span>JUDUL</span></td>
			<td width="90%">: {$data_publikasi.JUDUL_DOSEN_PUBLIKASI}</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>JENIS</span></td>
			<td>: {$data_publikasi.JENIS_DOSEN_PUBLIKASI}</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>PENERBIT</span></td>
			<td>: {$data_publikasi.PENERBIT_DOSEN_PUBLIKASI}</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>PENGARANG</span></td>
			<td>: {$data_publikasi.PENGARANG_DOSEN_PUBLIKASI}</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>MEDIA</span></td>
			<td>: {$data_publikasi.MEDIA_DOSEN_PUBLIKASI}</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>TAHUN</span></td>
			<td>: {$data_publikasi.THN_DOSEN_PUBLIKASI}</td>
		</tr>
</table>
