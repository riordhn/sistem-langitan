<div class="center_title_bar">Info Publikasi</div> 
<table class="ui-widget" width="80%">
    <tr class="ui-widget-header">
        <th colspan="3" class="header-coloumn">Info Publikasi</th>
    </tr>
    <tr class="ui-widget-content">
        <th width="10%">No</th>
        <th width="50%">Judul</th>
        <th class="center" width="40%">Operasi</th>
    </tr>
    {foreach $data_publikasi as $data}
        <tr class="ui-widget-content">
            <td><span>{$data@index+1}</span></td>
            <td>{$data.JUDUL_DOSEN_PUBLIKASI}</span></td>
            <td class="center">
                <span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="window.open('publikasi.php?mode=upload&id={$data.ID_DOSEN_PUBLIKASI}','','width=425,height=190,location=no');">Upload</span>
                <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="publikasi.php?mode=edit&id={$data.ID_DOSEN_PUBLIKASI}">Edit</a>
                <span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="$('#dialog_detail_publikasi').dialog('open').load('publikasi.php?mode=detail&id={$data.ID_DOSEN_PUBLIKASI}');">Detail</span>
                <span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="$('#dialog_delete_publikasi').dialog('open').load('publikasi.php?mode=delete&id={$data.ID_DOSEN_PUBLIKASI}');">Delete</span>
            </td>
        </tr>
    {/foreach}
    <tr class="ui-widget-content">
        <td colspan="3" class="center">
            <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="publikasi.php?mode=add">Tambah</a>
        </td>
    </tr>
</table>
<div id="dialog_detail_publikasi" title="Detail Publikasi"></div>
<div id="dialog_delete_publikasi" title="Confirmation"></div>
{literal}
    <script type="text/javascript">
    $( "#dialog_detail_publikasi" ).dialog({
                width: 550,
                modal: true,
                autoOpen:false,
                resizable:false,
                close: function() { 
                        $( "#dialog_detail_publikasi" ).dialog( "widget" )
           }
        });
        $( "#dialog_delete_publikasi" ).dialog({
                width: 350,
                height:120,
                modal: true,
                autoOpen:false,
                resizable:false,
                close: function() { 
                        $( "#dialog_detail_publikasi" ).dialog( "widget" )
           }
        });
    $('.download').click(function(){
        window.location.href="proses/download.php?download_file="+$('#file'+$(this).attr('id')).text();
            //load('proses/download.php',{'download_file':});
    });
    </script>
{/literal}
