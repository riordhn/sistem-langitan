<div class="center_title_bar">{$title}</div>
<div id="hasil_ubah_password" {if empty($hasil_ubah)}style="display:none"{/if}>
		{if $hasil_ubah=='benar'}
			<div class="ui-state-highlight ui-corner-all" style="padding: 10px;width:40%"> 
					<span class="ui-icon ui-icon-info" style="float: left;margin-left:5px;">  </span>
					Password Berhasil Di ubah.
			</div>
		{else if $hasil_ubah=='salah'}
			<div class="ui-state-error ui-corner-all" style="padding: 10px;width:40%;"> 
				<span class="ui-icon ui-icon-alert" style="float: left;margin-left:5px;">  </span> 
				Password Gagal Di Ubah Silahkan Ulangi Kembali.
			</div>
		{/if}
</div>
<div id="nip-text" class="ui-widget" style="display: none;width:35%">
	<div class="ui-state-default ui-corner-all" style="margin-top: 15px; padding: .5em;">
		<p>
			<span style="float:left;margin-right: .5em;" class="ui-icon ui-icon-info"></span>
			NIP : <strong style="color:green">{$nip} </strong>
			<span style="float:right" class="ui-button ui-icon ui-icon-circle-close" onclick="$('#nip-text').fadeOut();"></span>
		</p>
	</div>
</div>
<div class="ui-widget">
    <form name="form_ubah_password" id="form_ubah_password" action="ubah_password.php" method="post" >
        <table style="width:350px;">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn">
            <h2>Form Ubah Password</h2>
            </th>
            </tr>
            <tr class="ui-widget-content">
                <td width="50%">
                    <span>Password Lama</span>
                </td>
                <td width="50%">
                    <input type="password" id="password_lama" name="password_lama" /><br/>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>
                    <span>Password Baru</span>
                </td>
                <td>
                    <input type="password" id="password_baru" name="password_baru" /><br/>
                </td>
            </tr >
            <tr class="ui-widget-content">
                <td>
                    <span>Retype Password Baru</span>
                </td>
                <td>
                    <input type="password" id="retype_password" name="retype_password"/><br/>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td colspan="2">
                    <span id="save" style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all">Save</span>
                    <span id="cancel" style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all" onclick="$('#center_content').load('ubah_password.php')">Cancel</span>
                    <span style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all" onclick="$('#nip-text').fadeIn();">Lihat NIP</span>
                    <input type="hidden" name="save"  />
                </td>
            </tr> 
        </table>
    </form>
</div>
{literal}
    <script type="text/javascript">
    jQuery.validator.addMethod("notEqualTo", function(value, element, param) {
        return this.optional(element) || value != $(param).val();
    }, "Harus beda dengan password lama");
    $('#save').click(function(){
        $('#form_ubah_password').submit();
		$('#hasil_ubah_password').show();
    });
    $('#form_ubah_password').validate({
        rules: {
            password_lama :{
                required : true        
            },
            password_baru :{
                notEqualTo:"#password_lama",
                required : true
            },    
            retype_password : {
                required : true,
                equalTo: "#password_baru"
            }
        }
    });
    function GantiRole()
    {
        if (confirm('Apakah Anda yakin akan ganti role ?') == true)
        {
            var id_role = $('select[name="id_role"]').val();
            var id_pengguna = $('input[name="id_pengguna"]').val();
            var path = (id_fakultas == 8) ? '../' : '';
            
            $.ajax({
                type: 'POST',
                url: path + 'ubah_password.php',
                data: 'mode=change-role&id_pengguna='+id_pengguna+'&id_role='+id_role,
                success: function(data) {
                    if (data == 1)
                    {
                        alert('Anda sudah ganti role. Akun dosen anda akan di logout.');
                        window.location = path + '../../logout.php';
                    }
                    else
                    {
                        alert('Gagal ganti role');
                    }
                }
            });
        }
    }
    </script>
{/literal}

<table>
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="2">Multi Role</th>
    </tr>
    <tr class="ui-widget-content">
        <td><span>Role</span></td>
        <td>
            <input type="hidden" name="id_pengguna" value="{$id_pengguna}" />
            <select name="id_role">
            {foreach $role_set as $r}
                <option value="{$r.ID_ROLE}" {if $r.ID_ROLE == $id_role}selected="selected"{/if}>{$r.NM_ROLE}</option>
            {/foreach}
            </select>
            <button onclick="GantiRole(); return false;">Ganti Role</button>
        </td>
    </tr>
</table>