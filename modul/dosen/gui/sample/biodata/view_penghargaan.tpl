<div class="center_title_bar">DOSEN PENGHARGAAN</div> 
<table class="ui-widget" width="100%">
    <tr class="ui-widget-header">
        <th colspan="8" class="header-coloumn">Info Penghargaan</th>
    </tr>
    <tr>
        <th>No</th>
        <th>Bentuk/Jenis</th>
        <th>Bidang</th>
        <th>Pemberi</th>
        <th>Negara Pemberi</th>
        <th>Tahun</th>
        <th>Tingkat</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_penghargaan as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data.BIDANG_DOSEN_PENGHARGAAN}</td>
            <td>{$data.BENTUK_DOSEN_PENGHARGAAN}</td>
            <td>{$data.PEMBERI_DOSEN_PENGHARGAAN}</td>
            <td>{$data.NM_NEGARA}</td>
            <td>{$data.THN_DOSEN_PENGHARGAAN}</td>
            <td>{$data.TINGKAT_DOSEN_PENGHARGAAN}</td>
            <td class="center">
                <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="penghargaan.php?mode=edit&id_penghargaan={$data.ID_DOSEN_PENGHARGAAN}">Edit</a>
            </td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="8" class="center">
            <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="penghargaan.php?mode=add">Tambah</a>
        </td>
    </tr>

</table>
{literal}
    <script type="text/javascript">
    </script>
{/literal}
