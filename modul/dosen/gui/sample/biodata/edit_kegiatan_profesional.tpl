<div class="center_title_bar">Edit Kegiatan Profesional</div> 
<form id="form_edit_kegiatan_profesional" name="form_edit_kegiatan_profesional" method="post" action="kegiatan_profesional.php">
	<table class="ui-widget" width="50%">
		<tr class="ui-widget-header">
			<th colspan="2" class="header-coloumn"><h2>Edit Kegiatan</h2></th>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Kegiatan</span></td>
			<td>
				<textarea name="kegiatan" class="required">{$data_kegiatan.KEGIATAN_DOS_PROF}</textarea>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Peran</span></td>
			<td>
				<input type="text" name="peran" class="required" value="{$data_kegiatan.PERAN_DOS_PROF}"/>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Instansi</span></td>
			<td>
				<input type="text" name="instansi" class="required" value="{$data_kegiatan.INSTANSI_DOS_PROF}"/>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Tahun</span></td>
			<td>
				<input type="text" name="tahun" class="required number" value="{$data_kegiatan.THN_DOS_PROF}"/>
			</td>
		</tr>
        <tr class="ui-widget-content">
			<td><span>Tingkat</span></td>
			<td>
				<select id="input_tingkat" name="tingkat">
	                <option {if $data_kegiatan.TINGKAT_DOS_PROF=='Lokal'} selected="true"{/if} value="Lokal">Lokal</option>
                    <option {if $data_kegiatan.TINGKAT_DOS_PROF=='Nasional'} selected="true"{/if} value="Nasional">Nasional</option>
                    <option {if $data_kegiatan.TINGKAT_DOS_PROF=='Internasional'} selected="true"{/if} value="Internasional">Internasional</option>
                </select>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td colspan="2" class="center">
				<input style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" value="Update" type="submit">
				<a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="kegiatan_profesional.php">Cancel</a>
				<input type="hidden" name="mode" value="edit"/>
				<input type="hidden" value="{$data_kegiatan.ID_DOSEN_PROFESIONAL}" name="id_kegiatan" />
			</td>
		</tr>

	</table>
</form>
{literal}
	<script type="text/javascript">
		$('#form_edit_kegiatan_profesional').validate();
	</script>
{/literal}
