<div class="center_title_bar">Add Pengmas</div> 
<div>
    <form id="form_add_pengmas" name="form_add_pengmas" method="post" action="pengmas.php">
        <table class="ui-widget ui-corner-all" style="width:60%">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn"><h2>Add Pengmas</h2></th>
            </tr>
            <tr class="ui-widget-content">
                <td style="width:30%"><span>Nama Pengmas</span></td>
                <td style="width:70%">
                    <textarea name="nama" class="required"></textarea>
                </td>             
            </tr>
            <tr class="ui-widget-content">
                <td><span>Lokasi Instansi</span></td>
                <td>
                    <input type="text" name="tempat" class="required" size="50" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Bidang</span></td>
                <td>
                    <input type="text" name="bidang" class="required" size="50" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Peran</span></td>
                <td>
                    <input type="text" name="peran" class="required" size="50" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Tahun</span></td>
                <td>
                    <input type="text" name="tahun" class="required number" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Sumber Dana</span></td>
                <td>
                    <input type="text" name="sumber_dana" class="required" size="50" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Tim Pengmas</span></td>
                <td>
                   
                </td>
            </tr><tr class="ui-widget-content">
                <td><span>Mahasiswa Yang Terlibat</span></td>
                <td>
                    
                </td>
            </tr><tr class="ui-widget-content">
                <td><span>Jumlah Dana</span></td>
                <td>
                    <input type="text" name="jml_dana" class="required number" size="50" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Tingkat</span></td>
                <td>
                   <select id="input_tingkat" name="tingkat">
	                <option value="Lokal">Lokal</option>
                    <option value="Nasional">Nasional</option>
                    <option value="Internasional">Internasional</option>
                	</select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Output</span></td>
                <td>
                    <textarea name="output" class="required"></textarea>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td colspan="2" style="text-align:right">
					<input type="hidden" name="mode" value="add" />
					<a id="save" style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all">Save</a>
					<a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="pengmas.php">Cancel</a>
				</td>
            </tr>
        </table>
    </form>
</div>
{literal}
<script type="text/javascript">
$('#form_add_pengmas').validate();
$('#save').click(function(){
    $('#form_add_pengmas').submit();
});
</script>
{/literal}