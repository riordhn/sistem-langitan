<div class="center_title_bar">EditPenghargaan</div>
<form id="form_edit_penghargaan" name="form_edit_penghargaan" method="post"  action="penghargaan.php">
	<table class="ui-widget" width="50%">
		<tr class="ui-widget-header">
			<th colspan="2" class="header-coloumn"><h2>EditPenghargaan</h2></th>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Bentuk Penghargaan</span></td>
			<td>
				<textarea name="bentuk" class="required">{$data_penghargaan.BENTUK_DOSEN_PENGHARGAAN}</textarea>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Bidang</span></td>
			<td>
				<input type="text" class="required" name="bidang" value="{$data_penghargaan.BIDANG_DOSEN_PENGHARGAAN}" />
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Pemberi</span></td>
			<td>
				<input type="text" class="required" name="pemberi" value="{$data_penghargaan.PEMBERI_DOSEN_PENGHARGAAN}" />
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Negara Pemberi</span></td>
			<td>
				<select name="negara">
                	{foreach $negara as $data}
                	<option value="{$data.ID_NEGARA}" {if $data_penghargaan.ID_NEGARA==$data.ID_NEGARA} selected="true"{/if}>{$data.NM_NEGARA}</option>
                    {/foreach}
                </select>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Tahun</span></td>
			<td>
				<input type="text" class="required number" name="tahun" value="{$data_penghargaan.THN_DOSEN_PENGHARGAAN}" />
			</td>
		</tr>
        <tr class="ui-widget-content">
			<td><span>Tingkat</span></td>
			<td>
				<select id="input_tingkat" name="tingkat">
	                <option {if $data_penghargaan.TINGKAT_DOSEN_PENGHARGAAN=='Lokal'} selected="true"{/if} value="Lokal">Lokal</option>
                    <option {if $data_penghargaan.TINGKAT_DOSEN_PENGHARGAAN=='Nasional'} selected="true"{/if} value="Nasional">Nasional</option>
                    <option {if $data_penghargaan.TINGKAT_DOSEN_PENGHARGAAN=='Internasional'} selected="true"{/if} value="Internasional">Internasional</option>
                </select>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td colspan="2" class="center">
				<input type="submit" value="Save" style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" />
				<a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="penghargaan.php">Cancel</a>
				<input type="hidden" value="edit" name="mode"/>
				<input type="hidden" value="{$data_penghargaan.ID_DOSEN_PENGHARGAAN}" name="id_penghargaan" />
			</td>
		</tr>

	</table>
</form>
{literal}
<script type="text/javascript">
$('#form_edit_penghargaan').validate();
</script>
{/literal}