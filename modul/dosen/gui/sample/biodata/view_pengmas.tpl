<div class="center_title_bar">Info Pengmas</div> 
<table class="ui-widget" width="100%">
    <tr class="ui-widget-header">
        <th colspan="13" class="header-coloumn">Info Pengabdian Masyarakat</th>
    </tr>
    <tr class="ui-widget-content">
        <th>No</th>
        <th>Nama</th>
        <th>Bidang</th>
        <th>Tempat</th>
        <th>Peran</th>
        <th>Sumber Dana</th>
        <th>Tim Pengmas</th>
        <th>Mahasiswa Yang Terlibat</th>
        <th>Jumlah Dana</th>
        <th>Tahun</th>
        <th>Tingkat</th>
        <th>Output</th>
        <th class="center">Operasi</th>
    </tr>
    {foreach $data_pengmas as $data}
        <tr  class="ui-widget-content">
            <td><span >{$data@index+1}</span></td>
            <td><span >{$data.NM_DOSEN_PENGMAS}</span></td>
            <td><span >{$data.BIDANG_DOSEN_PENGMAS}</span></td>
            <td><span >{$data.TEMPAT_DOSEN_PENGMAS}</span></td>
            <td><span >{$data.PERAN_DOSEN_PENGMAS}</span></td>
            <td><span >{$data.SUMBER_DANA_DOSEN_PENGMAS}</span></td>
            <td><span ></span></td>
            <td><span ></span></td>
            <td><span >{$data.DANA_DOSEN_PENGMAS}</span></td>
            <td><span >{$data.THN_DOSEN_PENGMAS}</span></td>
            <td><span >{$data.TINGKAT_DOSEN_PENGMAS}</span></td>
            <td><span >{$data.OUTPUT_DOSEN_PENGMAS}</span></td>
            <td class="center">
                <span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="window.open('pengmas.php?mode=upload&id={$data.ID_DOSEN_PENGMAS}','','width=425,height=190,location=no');">Upload</span>
                <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="pengmas.php?mode=edit&id={$data.ID_DOSEN_PENGMAS}">Edit</a>
                <span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="$('#dialog_detail_pengmas').dialog('open').load('pengmas.php?mode=detail&id={$data.ID_DOSEN_PENGMAS}');">Detail</span>
                <span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="$('#dialog_delete_pengmas').dialog('open').load('pengmas.php?mode=delete&id={$data.ID_DOSEN_PENGMAS}');">Delete</span>
            </td>
        </tr>
    {/foreach}
    <tr class="ui-widget-content">
        <td colspan="13" class="center">
            <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="pengmas.php?mode=add">Tambah</a>
        </td>
    </tr>
</table>
<div id="dialog_detail_pengmas" title="Detail Pengmas"></div>
<div id="dialog_delete_pengmas" title="Confirmation"></div>
{literal}
    <script type="text/javascript">
            $( "#dialog_detail_pengmas" ).dialog({
                    width: 550,
                    modal: true,
                    autoOpen:false,
                    resizable:false,
                    close: function() { 
                            $( "#dialog_detail_pengmas" ).dialog( "widget" )
               }
            });
            $( "#dialog_delete_pengmas" ).dialog({
                    width: 350,
                    height:120,
                    modal: true,
                    autoOpen:false,
                    resizable:false,
                    close: function() { 
                            $( "#dialog_detail_pengmas" ).dialog( "widget" )
               }
            });
        $('.download').click(function(){
            window.location.href="proses/download.php?download_file="+$('#file'+$(this).attr('id')).text();
                //load('proses/download.php',{'download_file':});
        });
    </script>
{/literal}