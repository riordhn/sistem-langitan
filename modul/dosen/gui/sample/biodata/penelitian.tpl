-<div class="center_title_bar">{$title}</div> 
<table class="ui-widget" width="80%">
    <tr class="ui-widget-header">
        <th colspan="4" class="header-coloumn"><h2>Info Penelitian</h2></th>
    </tr>
    <tr class="ui-widget-content">
        <th width="10%"><h3>No</h3></th>
        <th><h3>Judul Penelitian</h3></th>
        <th style="width: 40px"><h3>Tahun</h3></th>
        <th style="width: 155px"><h3>Operasi</h3></th>
    </tr>   
    {foreach $data_penelitian as $p}
    <tr class="ui-widget-content">
        <td class="center">{$p@index+1}</td>
        <td>{$p.JUDUL_PENELITIAN}<br/>
			{foreach $p.FILES as $file}
			<span style="font-weight: bold; text-decoration: underline; cursor: default;color:#0077cc" title="{$file.DESKRIPSI}">{$file.NAMA_FILE}</span>&nbsp;<span title="Hapus File" style="cursor: pointer;color: red" onclick="javascript: deleteFile({$file.ID_FILE_PENELITIAN})">[X]</span>,
			{/foreach}
		</td>
        <td class="center">{$p.THN_PENELITIAN}</td>
        <td class="center">
            <!--<span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="/*$('#dialog_upload_file').dialog('open').load('penelitian.php?mode=upload&idpen={$p.ID_PENELITIAN}');*/">Upload</span>-->
            <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="penelitian.php?mode=edit&id_penelitian={$p.ID_PENELITIAN}">Edit</a>
			<span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="$('#dialog_detail_penelitian').dialog('open').load('penelitian.php?mode=detail&idpen={$p.ID_PENELITIAN}');">Detail</span>
        </td>
    </tr>
    {/foreach}
    <tr class="ui-widget-content">
        <td colspan="4" class="center">
            <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="penelitian.php?mode=add" >Tambah</a>
        </td>
    </tr>
</table>
<div id="dialog_detail_penelitian" title="Detail Penelitian"><div>
<div id="dialog_upload_file" title="Upload File Penelitian"></div>
{literal}
<script type="text/javascript">
	function deleteFile(id_file_penelitian) {
		if (confirm('Apakah file akan dihapus ?') == true) {
			$.ajax({
				url: 'penelitian.php',
				type: 'POST',
				data: 'mode=delete_file&id_file_penelitian=' + id_file_penelitian,
				success: function(data) {
					alert(data);
					window.location.reload();
				}
			});
		}
	}
	$( "#dialog_detail_penelitian" ).dialog({
		width: 700,
		modal: true,
		autoOpen:false,
		resizable:false,
		close: function() { 
			$( "#dialog_detail_penelitian" ).dialog( "widget" )
	   }
	});
	$( "#dialog_upload_file" ).dialog({
		width: 700,
		modal: true,
		autoOpen:false,
		resizable:false
	});
</script>
{/literal}
