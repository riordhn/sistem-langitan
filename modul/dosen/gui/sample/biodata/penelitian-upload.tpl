<form id="form_upload_penelitian" action="penelitian-upload.php" method="post" enctype="multipart/form-data">
	<table class="ui-widget" style="width:657px">
		<tr class="ui-widget-content">
			<td><label>Nama File :</label></td>
			<td>
				<input id="upload_file_penelitian" type="file" name="file_penelitian" size="70"/><br/>
				<span id="status_upload"></span>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><label>Deskripsi</label></td>
			<td><input type="text" name="deskripsi" size="50" maxlength="50" /></td>
		</tr>
			<input type="hidden" name="id_penelitian" value="{$data_penelitian.ID_PENELITIAN}" />
			<input type="hidden" name="mode" value="upload" />
		<tr class="ui-widget-content">
			<td colspan="2" class="center">
				<!--<input  style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" type="submit" value="Upload" />-->
				<span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="$('#dialog_upload_file').dialog('close');">Cancel</span>
			</td>
		</tr>
	</table>
</form>
{literal}
	<script type="text/javascript">
		$('#form_upload_penelitian').validate({
			rules : {
				file_penelititan : {
					required:true
				}
			}
		});
		$('#upload_file_penelitian').change(function(){
			$('#form_upload_penelitian').submit();
		})
	</script>
{/literal}
