<div class="center_title_bar">Journal - Tambah</div>

<form action="journal.php?mode=add" method="post" id="form-journal">
<input type="hidden" name="mode" value="add" />
<table class="ui-widget">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="2">Form Tambah Jurnal</th>
    </tr>
    <tr class="ui-widget-content">
        <td>Judul</td>
        <td><textarea cols="75" rows="2" name="judul" style="width: auto"></textarea></td>
    </tr>
    <tr class="ui-widget-content">
        <td>Abstrak</td>
        <td><textarea cols="75" rows="20" name="abstrak" style="width: auto"></textarea></td>
    </tr>
    <tr class="ui-widget-content">
        <td>Keywords</td>
        <td><input name="keywords" style="width: auto" size="75" maxlength="100" /></td>
    </tr>
    <tr class="ui-widget-content">
        <td>Media</td>
        <td><select name="id_journal_media">
                <option value=""></option>
            {foreach $journal_media_set as $jm}
                <option value="{$jm.ID_JOURNAL_MEDIA}">{$jm.NM_JOURNAL_MEDIA}</option>
            {/foreach}
            </select></td>
    </tr>
    <tr class="ui-widget-content">
        <td>Volume</td>
        <td><input type="text" name="volume" size="5" maxlength="5" /></td>
    </tr>
    <tr class="ui-widget-content">
        <td>Nomer</td>
        <td><input type="text" name="nomer" size="5" maxlength="5" /></td>
    </tr>
    <tr class="ui-widget-content">
        <td>Halaman</td>
        <td><input type="text" name="halaman" size="5" maxlength="5" /></td>
    </tr>
    <tr class="ui-widget-content">
        <td colspan="2" class="center"><a href="journal.php">Batal</a><input type="submit" value="Simpan" /></td>
    </tr>
</table>
</form>
    
<script type="text/javascript">
    $('#form-journal').validate({
        rules: {
            judul:              { required: true },
            abstrak:            { required: true },
            id_journal_media:   { required: true },
            volume:             { number: true },
            nomer:              { number: true },
            halaman:            { number: true }
        },
        messages: {
            judul:              { required: 'Masukkan judul journal terlebih dahulu' },
            abstrak:            { required: 'Masukkan abstraksi dari journal terlebih dahulu' },
            id_journal_media:   { required: 'Pilih Media publikasi jurnal' },
            volume:             { number: 'Masukkan hanya angka saja' },
            nomer:              { number: 'Masukkan angka' },
            halaman:            { number: 'Masukkan angka' }
        }
    });
</script>