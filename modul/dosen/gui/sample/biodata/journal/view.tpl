<div class="center_title_bar">Journal</div>

<table class="ui-widget">
    <tr class="ui-widget-header">
        <th class="header-coloumn">No</th>
        <th class="header-coloumn">Judul Journal</th>
        <th class="header-coloumn">Media</th>
        <th class="header-coloumn" style="width: 120px">Link</th>
    </tr>
    {foreach $jurnal_set as $j}
    <tr class="ui-widget-content">
        <td class="center">{$j@index + 1}</td>
        <td>{$j.title}</td>
        <td>{$j.namamedia}</td>
        <td class="center">
            <a href="{$j.link}" class="disable-ajax" target="_blank">Open</a>
        </td>
    </tr>
    {/foreach}
</table>