<script type="text/javascript" src="../../js/jquery-1.5.1.js"></script>
<script type="text/javascript" src="../../js/jquery.validate.js"></script>
<link type="text/css" href="../../css/jquery-ui-1.8.11.custom.css" rel="stylesheet" />
{literal}
<style>
	.error {
		color:red;
		font-size:10px;
	}
</style>
{/literal}
        
<form id="form_upload_penelitian" action="file-upload.php" method="post" enctype="multipart/form-data">
	<table class="ui-widget" style="font-size:12px;width:400">
		<tr class="ui-widget-header">
			<th colspan="2" style="padding:10px;"><span style="color:white;font-size:16px;">Upload File Publikasi</span></th>
		</tr>
		<tr class="ui-widget-content">
			<td><label>Judul Publikasi :</label></td>
			<td>
				<span>{$data_publikasi.JUDUL_DOSEN_PUBLIKASI}</span>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><label>Nama File :</label></td>
			<td>
				<input id="upload_file_penelitian" type="file" name="file_penelitian" size="70"/><br/>
				<span id="status_upload"></span>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><label>Deskripsi</label></td>
			<td><input type="text" name="deskripsi" size="50" maxlength="50" class="required" /></td>
		</tr>
			<input type="hidden" name="id" value="{$smarty.get.id_publikasi}" />
			<input type="hidden" name="mode" value="upload" />
		<tr class="ui-widget-content">
			<td colspan="2" style="text-align:center">
				<input  style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" type="submit" value="Upload" />
				<span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="window.close();">Cancel</span>
			</td>
		</tr>
	</table>
</form>
{literal}
	<script type="text/javascript">
		$('#form_upload_penelitian').validate();
	</script>
{/literal}
