{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
{if isset($data_eva_kul)}
    <table class="tablesorter ui-widget"  width="90%">
        <thead>
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn">Evaluasi Praktikum</th>
            </tr>
            <tr class="ui-widget-content">
                <th>No</th>
                <th>Materi</th>
                <th width="20%">Rata Nilai</th>
                <th>Total Mahasiswa Eval</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_eva_kul as $data}
                <tr class="ui-widget-content">
                    <td class="center">{$data@index+1}</td>
                    <td>{$data.EVALUASI_ASPEK}</td>
                    <td class="center">
                        {round($data.RATA,2)}
                    </td>
                    <td>{$data.JUMLAH_MHS}</td>
                </tr>
            {foreachelse}
                <tr class="ui-widget-content">
                    <td class="center" colspan="4"><span class="ui-state-error-text">Data Masih Kosong</span></td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="dialog-eva-kul" id="dialog-eval-kul1" title="Komentar">
        <table class="ui-widget">
            <tr class="ui-widget-header">
                <th class="header-coloumn">{$data_eva_kul[21].ASPEK}</th>
            </tr>
            {foreach $data_eva_kul[0].KOMENTAR1 as $data}
                {if $data.A22!='--'&&$data.A22!='-'&&$data.A22!='tidak ada pendapat'}
                    <tr class="ui-widget-content">
                        <td>{$data.A22}</td>
                    </tr>
                {/if}
            {/foreach}
        </table>
    </div>
    <div class="dialog-eva-kul" id="dialog-eval-kul2" title="Komentar">
        <table class="ui-widget">
            <tr class="ui-widget-header">
                <th class="header-coloumn">{$data_eva_kul[22].ASPEK}</th>
            </tr>
            {foreach $data_eva_kul[0].KOMENTAR2 as $data}
                {if $data.A23!='--'&&$data.A23!='-'&&$data.A23!='tidak ada pendapat'}
                    <tr class="ui-widget-content">
                        <td>{$data.A23}</td>
                    </tr>
                {/if}
            {/foreach}
        </table>
    </div>
{/if}
{literal}
    <script type="text/javascript">
        $("#dialog-eval-kul1,#dialog-eval-kul2").dialog({
            width: 500,
            modal: true,
            autoOpen: false
        });
    </script>
{/literal}