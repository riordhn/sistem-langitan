<div class="center_title_bar">Add Kegiatan Profesional</div> 
<form id="form_add_kegiatan_profesional" name="form_add_kegiatan_profesional" method="post" action="kegiatan_profesional.php">
	<table class="ui-widget" width="50%">
		<tr class="ui-widget-header">
			<th colspan="2" class="header-coloumn"><h2>Add Kegiatan</h2></th>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Kegiatan</span></td>
			<td>
				<textarea name="kegiatan" class="required"></textarea>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Peran</span></td>
			<td>
				<input type="text" name="peran" class="required" />
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Instansi</span></td>
			<td>
				<input type="text" name="instansi" class="required" />
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Tahun</span></td>
			<td>
				<input type="text" name="tahun" class="required number" />
			</td>
		</tr>
        <tr class="ui-widget-content">
			<td><span>Tingkat</span></td>
			<td>
				<select id="input_tingkat" name="tingkat">
	                <option value="Lokal">Lokal</option>
                    <option value="Nasional">Nasional</option>
                    <option value="Internasional">Internasional</option>
                </select>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td colspan="2" class="center">
				<input style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" value="Save" type="submit">
				<a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="kegiatan_profesional.php">Cancel</a>
				<input type="hidden" name="mode" value="add"/>
			</td>
		</tr>

	</table>
</form>
{literal}
	<script type="text/javascript">
		$('#form_add_kegiatan_profesional').validate();
	</script>
{/literal}
