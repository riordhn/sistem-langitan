<div class="center_title_bar">Edit Pengmas</div> 
<div>
    <form id="form_edit_pengmas" name="form_edit_pengmas" method="post" action="pengmas.php"> 
        <table class="ui-widget">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn"><h2>Edit Pengmas</h2></th>
            </tr>
			<tr class="ui-widget-content">
                <td><span>Kegiatan Pemgas</span></td>
                <td>
                    <textarea name="nama" class="required">{$data_pengmas.NM_DOSEN_PENGMAS}</textarea>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Bidang</span></td>
                <td>
                    <input type="text" name="bidang" class="required" value="{$data_pengmas.BIDANG_DOSEN_PENGMAS}" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Peran</span></td>
                <td>
                    <input type="text" name="peran" class="required" value="{$data_pengmas.PERAN_DOSEN_PENGMAS}" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Tahun</span></td>
                <td>
                    <input type="text" name="tahun" class="required number" value="{$data_pengmas.THN_DOSEN_PENGMAS}"/>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Sumber Dana</span></td>
                <td>
                    <input type="text" name="sumber_dana" class="required" size="50"  value="{$data_pengmas.SUMBER_DANA_DOSEN_PENGMAS}" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Tim Pengmas</span></td>
                <td>
                   
                </td>
            </tr><tr class="ui-widget-content">
                <td><span>Mahasiswa Yang Terlibat</span></td>
                <td>
                    
                </td>
            </tr><tr class="ui-widget-content">
                <td><span>Jumlah Dana</span></td>
                <td>
                    <input type="text" name="jml_dana" class="required number" size="50"  value="{$data_pengmas.DANA_DOSEN_PENGMAS}" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Tingkat</span></td>
                <td>
                  <select id="input_tingkat" name="tingkat">
	                <option {if $data_pengmas.TINGKAT_DOSEN_PENGMAS=='Lokal'} selected="true"{/if}value="Lokal">Lokal</option>
                    <option {if $data_pengmas.TINGKAT_DOSEN_PENGMAS=='Nasional'} selected="true"{/if} value="Nasional">Nasional</option>
                    <option {if $data_pengmas.TINGKAT_DOSEN_PENGMAS=='Internasional'} selected="true"{/if} value="Internasional">Internasional</option>
                </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td><span>Output</span></td>
                <td>
                    <textarea name="output" class="required"  value="{$data_pengmas.OUTPUT_DOSEN_PENGMAS}"></textarea>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td colspan="2" style="text-align:right">
					<input type="hidden" name="id_pengmas" value="{$data_pengmas.ID_DOSEN_PENGMAS}"/>
					<input type="hidden" name="mode" value="edit"/>
					<a id="save" style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all">Save</a>
					<a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="pengmas.php">Cancel</a>
				</td>
            </tr>

        </table>
    </form>
</div>
{literal}
<script type="text/javascript">
$('#save').click(function(){
    $('#form_edit_pengmas').submit();
});
$('#form_edit_pengmas').validate();

</script>
{/literal}