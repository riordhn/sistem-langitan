<div class="center_title_bar">Add Publikasi</div>
<form id="form_add" action="publikasi.php" method="post" name="form_add" enctype="multipart/form-data">
    <table class="ui-widget" style="width:50%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn"><h2>Add Publikasi Perkuliahan</h2></th>
        </tr>
        <tr class="ui-widget-content">
            <td width="20%"><span>Menu Publikasi</span></td>
            <td width="80%">
                <select id="input_jenis" name="jenis" >
                    <option value="Buku">Buku</option>
                    <option value="Journal">Journal</option>
                    <option value="Petunjuk Praktikum">Petunjuk Praktikum</option>
                    <option value="Diktat">Diktat</option>
                    <option value="Seminar">Seminar</option>
                </select>
            </td>
        </tr>
		<tr class="ui-widget-content">
            <td>
                <span>Peran</span>
            </td>
            <td>
                <select id="input_peran" name="peran">
	                <option value="Anggota">Anggota</option>
                    <option value="Penulis Utama">Penulis Utama
                    </option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                <span>Judul</span>
            </td>
            <td>
                <textarea id="judul" name="judul" class="required" ></textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                <span>Penerbit</span>
            </td>
            <td>
                <input type="text" name="penerbit" size="50" id="penerbit" class="required"/>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td><span>Pengarang</span></td>
            <td>
				<textarea name="pengarang" id="pengarang" class="required"></textarea>
            </td>
        </tr>
		<tr class="ui-widget-content">
            <td><span>Media</span></td>
            <td>
                <input type="text"  size="50" name="media"  id="media" class="required"/>
            </td>
        </tr>
		<tr class="ui-widget-content">
            <td><span>Tahun</span></td>
            <td>
                <input type="text" name="tahun" id="tahun"  class="required number"/>
            </td>
        </tr>
         <tr class="ui-widget-content">
            <td>
                <span>Edisi</span>
            </td>
            <td>
                <input type="text" id="edisi"  size="50" name="edisi" />
            </td>
        </tr><tr class="ui-widget-content">
            <td>
                <span>Nama Lembaga Sitasi</span>
            </td>
            <td>
                <input type="text" id="lembaga"  size="50" name="lembaga" />
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                <span>Tingkat</span>
            </td>
            <td>
                <select id="input_tingkat" name="tingkat">
	                <option value="Lokal">Lokal</option>
                    <option value="Nasional">Nasional</option>
                    <option value="Internasional">Internasional</option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" style="text-align:right">
				<input type="hidden" name="mode" value="add" />
                <a id="save" style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all">Save</a>
				<a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="publikasi.php">Cancel</a>
            </td>
        </tr>

    </table>
</form>
{literal}
<script type="text/javascript">
$('#save').click(function(){
    $('#form_add').submit();
});
$('#form_add').validate();
</script>
{/literal}