<div class="center_title_bar">Edit Training</div> 
<form name="form_edit" id="form_edit" method="post" action="training.php">
	<table class="ui-widget">
		<tr class="ui-widget-header">
			<th colspan="2" class="header-coloumn"><h2>Edit Training</h2></th>
		</tr>

		<tr class="ui-widget-content">
			<td>
				<span>Nama Training</span>
			</td>
			<td>
				<textarea name="nama" class="required">{$data_training.NM_DOS_TRAINING}</textarea>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td>
				<span>Jenis Training</span>
			</td>
			<td>
				<select id="input_jenis" name="jenis">
	                <option {if $data_training.JENIS_DOS_TRAINING=='Workshop'} selected="true"{/if} value="Workshop">Workshop</option>
                    <option {if $data_training.JENIS_DOS_TRAINING=='Seminar'} selected="true"{/if} value="Seminar">Seminar</option>
                    <option {if $data_training.JENIS_DOS_TRAINING=='Pelatihan'} selected="true"{/if} value="Pelatihan">Pelatihan</option>
                </select>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td>
				<span>Tahun</span>
			</td>
			<td>
				<input type="text" name="tahun" class="required number" value="{$data_training.THN_DOS_TRAINING}" />
			</td>
		</tr>

		<tr class="ui-widget-content">
			<td>
				<span>Instansi</span>
			</td>
			<td>
				<input type="text" name="instansi" class="required" value="{$data_training.INSTANSI_DOS_TRAINING}" />
			</td>
		</tr>
        <tr class="ui-widget-content">
			<td><span>Peran</span></td>
			<td>
				<select id="input_peran" name="peran">
	                <option {if $data_training.PERAN_DOS_TRAINING=='Anggota'} selected="true"{/if} value="Anggota">Anggota</option>
                    <option {if $data_training.PERAN_DOS_TRAINING=='Trainer'} selected="true"{/if} value="Trainer">Trainer</option>
                    <option {if $data_training.PERAN_DOS_TRAINING=='Nara Sumber'} selected="true"{/if} value="Nara Sumber">Nara Sumber</option>
                </select>
			</td>
		</tr>
        <tr class="ui-widget-content">
			<td><span>Tingkat</span></td>
			<td>
				<select id="input_tingkat" name="tingkat">
	                <option {if $data_training.TINGKAT_DOS_TRAINING=='Lokal'} selected="true"{/if} value="Lokal">Lokal</option>
                    <option {if $data_training.TINGKAT_DOS_TRAINING=='Nasional'} selected="true"{/if} value="Nasional">Nasional</option>
                    <option {if $data_training.TINGKAT_DOS_TRAINING=='Internasional'} selected="true"{/if} value="Internasional">Internasional</option>
                </select>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Document</span></td>
			<td>
				<input type="file" name="file" />
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td colspan="2" style="text-align:right">
				<input type="hidden" name="id_training" value="{$data_training.ID_DOSEN_TRAINING}"/>
				<input type="hidden" name="mode" value="edit"/>
				<a id="save" style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all">Save</a>
				<a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="training.php">Cancel</a>
			</td>
		</tr>
	</table>
</form>
{literal}
<script type="text/javascript">
$('#save').click(function(){
    $('#form_edit').submit();
});
$('#form_edit').validate();
</script>
{/literal}