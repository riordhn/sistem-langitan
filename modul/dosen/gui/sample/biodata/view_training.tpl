<div class="center_title_bar">Info Training</div> 
<table class="ui-widget" style="width:100%">
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="10">Info Training</th>
    </tr>
    <tr class="ui-widget-content">
        <th>No</th>
        <th>Jenis</th>
        <th>Nama</th>
        <th>Peran</th>
        <th>Instansi</th>
        <th>Tahun</th>
        <th>Tingkat</th>
        <th class="center">Operasi</th>
    </tr>
    {foreach $data_training as $data}
        <tr class="ui-widget-content">
            <td>{$data@index+1}</td>
            <td><span>{$data.JENIS_DOS_TRAINING}</span></td>
            <td><span>{$data.NM_DOS_TRAINING}</span></td>
            <td><span>{$data.PERAN_DOS_TRAINING}</span></td>
            <td><span>{$data.INSTANSI_DOS_TRAINING}</span></td>
            <td><span>{$data.THN_DOS_TRAINING}</span></td>
            <td><span>{$data.TINGKAT_DOS_TRAINING}</span></td>
            <td>
                <span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="window.open('training.php?mode=upload&id={$data.ID_DOSEN_TRAINING}','','width=425,height=190,location=no');">Upload</span>
                <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="training.php?mode=edit&id={$data.ID_DOSEN_TRAINING}">Edit</a>
                <span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="$('#dialog_detail_training').dialog('open').load('training.php?mode=detail&id={$data.ID_DOSEN_TRAINING}');">Detail</span>
                <span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="$('#dialog_delete_training').dialog('open').load('training.php?mode=delete&id={$data.ID_DOSEN_TRAINING}');">Delete</span>
            </td>
        </tr>
    {/foreach}
    <tr class="ui-widget-content">
        <td colspan="10" class="center">
            <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="training.php?mode=add">Tambah</a></td>
    </tr>
</table>
<div id="dialog_detail_training" title="Detail Training"></div>
<div id="dialog_delete_training" title="Confirmation"></div>
{literal}
    <script type="text/javascript" >
            $( "#dialog_detail_training" ).dialog({
                    width: 550,
                    modal: true,
                    autoOpen:false,
                    resizable:false,
                    close: function() { 
                            $( "#dialog_detail_training" ).dialog( "widget" )
               }
            });
            $( "#dialog_delete_training" ).dialog({
                    width: 350,
                    height:120,
                    modal: true,
                    autoOpen:false,
                    resizable:false,
                    close: function() { 
                            $( "#dialog_detail_training" ).dialog( "widget" )
               }
            });
        $('.download').click(function(){
            window.location.href="proses/download.php?download_file="+$('#file'+$(this).attr('id')).text();
                //load('proses/download.php',{'download_file':});
        });
    </script>
{/literal}