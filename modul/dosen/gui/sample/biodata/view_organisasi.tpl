<div class="center_title_bar">DOSEN ORGANISASI</div> 
<table class="ui-widget" width="100%">
    <tr class="ui-widget-header">
        <th colspan="8" class="header-coloumn">Info Organisasi</th>
    </tr>
    <tr>
        <th>No</th>
        <th>Nama Organisasi</th>
        <th>Jabatan</th>
        <th>Mulai</th>
        <th>Selesai</th>
        <th>Tingkat</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_organisasi as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data.NM_DOSEN_ORG}</td>
            <td>{$data.JABATAN_DOSEN_ORG}</td>
            <td>{$data.MULAI_DOSEN_ORG}</td>
            <td>{$data.SELESAI_DOSEN_ORG}</td>
            <td>{$data.TINGKAT_DOSEN_ORG}</td>
            <td class="center">
                <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="organisasi.php?mode=edit&id_organisasi={$data.ID_DOSEN_ORGANISASI}" >Edit</a>
                <span style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" onclick="$('#dialog_delete_organisasi').dialog('open').load('organisasi.php?mode=delete&id={$data.ID_DOSEN_ORGANISASI}');">Delete</span>
            </td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="8" class="center">
            <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="organisasi.php?mode=add">Tambah</a>
        </td>
    </tr>
</table>
<div id="dialog_delete_organisasi" title="Confirmation"></div>
{literal}
    <script type="text/javascript">
    $( "#dialog_delete_organisasi" ).dialog({
                    width: 350,
                    height:120,
                    modal: true,
                    autoOpen:false,
                    resizable:false,
                    close: function() { 
                            $( "#dialog_delete_organisasi" ).dialog( "widget" )
               }
            });
    </script>
{/literal}