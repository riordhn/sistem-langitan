<div class="center_title_bar">Edit Penelitian</div> 
<div>
    <form id="form_edit_penelitian" name="form_edit_penelitian" method="post" action="penelitian.php">
        <table>
            <tr>
                <th colspan="2"><h2>edit Penelitian</h2></th>
            </tr>

            <tr>
                <td><span>Judul Penelitian</span></td>
                <td>
                    <input type="text" name="judul" value="{$data_edit['judul']}" />
                    <input type="hidden" name="id_penelitian" value="{$data_edit['id_penelitian']}" />
                </td>
                <td style="border: none;">
                    <label for="judul" style="display: none;" class="error">Tidak Boleh Kosong</label>
                </td>
            </tr>
            <tr>
                <td><span>Abstrak</span></td>
                <td><input type="text" name="abstrak" value="{$data_edit['abstrak']}" /></td>
                <td style="border: none;">
                    <label for="abstrak" style="display: none;" class="error">Tidak Boleh Kosong</label>
                </td>
            </tr>
            <tr>
                <td><span>Kategori</span></td>
                <td><input type="text" name="kategori" value="{$data_edit['kategori']}" /></td>
                <td style="border: none;">
                    <label for="abstrak" style="display: none;" class="error">Tidak Boleh Kosong</label>
                </td>
            </tr>
            <tr>
                <td><span>Sumber Dana</span></td>
                <td><input type="text" name="sumber" value="{$data_edit['sumber']}" /></td>
                <td style="border: none;">
                    <label for="sumber" style="display: none;" class="error">Tidak Boleh Kosong</label>
                </td>
            </tr>
            <tr>
                <td><span>Dana</span></td>
                <td><input type="text" name="dana" value="{$data_edit['dana']}" /></td>
                <td style="border: none;">
                    <label for="dana" style="display: none;" class="error">Tidak Boleh Kosong</label>
                </td>
            </tr>
            <tr>
                <td><span>Tahun</span></td>
                <td><input type="text" name="tahun" value="{$data_edit['tahun']}" /></td>
                <td style="border: none;">
                    <label for="tahun" style="display: none;" class="error">Tidak Boleh Kosong/format tahun salah</label>
                </td>
            </tr>
            <tr>
                <td><span>Tim Peneliti</span></td>
                <td><input type="text" name="tim" value="{$data_edit['tim']}" /></td>
                <td style="border: none;">
                    <label for="tim" style="display: none;" class="error">Tidak Boleh Kosong</label>
                </td>
            </tr>
            <tr>
                <td><span>Document</span></td>
                <td><input type="file" name="file" /></td>
                <td style="border: none;">
                    <label for="file" style="display: none;" class="error">Tidak Boleh Kosong</label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <a class="save">Save</a>
                    <a class="cancel">Cancel</a>
                    <input type="hidden" name="save" />
                </td>
            </tr>

        </table>
    </form>
</div>
{literal}
<script type="text/javascript">
$('.cancel').click(function(){
    $('#center_content').load('penelitian.php');
});
$('#form_edit_penelitian').validate({
    rules : {
        judul :{
            required :true
        },
        abstrak :{
            required :true
        },
        sumber :{
            required :true
        },
        dana :{
            required :true
        },
        tahun :{
            required :true,
            date :true
        },
        tim :{
            required :true
        }
    }
});
$('#form_edit_penelitian').ajaxForm({
        type : 'post',
        url : $(this).attr('action'),
        data : $(this).serialize(),
        success : function(data){
            $('#center_content').html(data);
        }
});
$('.save').click(function(){
    $('#form_edit_penelitian').submit();
});
</script>
{/literal}
