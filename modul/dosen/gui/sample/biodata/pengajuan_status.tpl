<div class="center_title_bar">PENGAJUAN STATUS DOSEN</div> 
<form id="form_pengajuan_status" action="pengajuan_status.php">
    <table class="ui-widget" width="80%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn"><h2>Form Pengajuan Status Dosen</h2></th>
        </tr>
        <tr>
            <td width="25%">
				<span>Nama</span>
			</td>
			<td width="75%">
				<span>{$data_info['nama']}</span>
			</td>
        </tr>
        <tr>
            <td>
				<span>Jenis Kelamin</span>
			</td>
			<td>
				<span>{if $data_info['kelamin'] == 2} Perempuan {else} Laki-Laki {/if}</span>
			</td>
        </tr>
        <tr>
            <td>
				<span>Tanggal Lahir</span>
			</td>
			<td>
				<span>{$data_info['tgl_lahir']}</span>
			</td>
        </tr>
        <tr>
            <td>
				<span>Tempat Lahir</span>
			</td>
			<td>
				<span>{$data_info['tempat_lahir']}</span>
			</td>
        </tr>
        <tr>
            <td>
				<span>Alamat</span>
			</td>
			<td>
				<span>{$data_info['alamat']}</span>
			</td>
        </tr>
        <tr>
            <td><span>Status</span></td>
            <td>
                <select name="status">
                    {foreach $status as $list}
                    {if $list['NM_STATUS_PENGGUNA']!='Aktif'}
                    <option value="{$list['NM_STATUS_PENGGUNA']}">{$list['NM_STATUS_PENGGUNA']}</option>
                    {/if}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td><span>Mulai</span></td>
            <td>
                <input type="text" name="mulai" id="mulai" size="20"/> <br/>
				<label for="mulai" style="display: none;" class="error">Tidak Boleh Kosong</label>
            </td>
        </tr>
        <tr>
            <td><span>Selesai</span></td>
            <td>
                <input type="text" name="selesai" id="selesai" size="20"/><br/>
				<label for="selesai" style="display: none;" class="error">Tidak Boleh Kosong</label>
            </td>
        </tr>
        <tr>
            <td><span>Keterangan</span></td>
            <td >
                <textarea rows="3" cols="30" name="keterangan" ></textarea><br/>
				<label for="keterangan" style="display: none;" class="error">Tidak Boleh Kosong</label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <a id="save" style="padding:5px;float:right;" class="ui-button ui-state-default ui-corner-all">Save</a>
                <input type="hidden" name="save"/>
            </td>            
        </tr>
    </table>
</form>

{literal}
<script>
	$( "#mulai" ).datepicker({
		changeMonth : true,
		changeYear : true,
		yearRange : '1900:2100'
	});
	$( "#selesai" ).datepicker({
		changeMonth : true,
		changeYear : true,
		yearRange : '1900:2100'
	});
	$('#save').click(function(){
		$('#form_pengajuan_status').submit();
	});
	$('#form_pengajuan_status').validate({
		rules:{
			mulai :{
				required:true
			},
			selesai:{
				required:true
			},
			keterangan:{
				required:true
			}
		}
	})
	$('td').addClass('ui-widget-content');			           
</script>
{/literal}
