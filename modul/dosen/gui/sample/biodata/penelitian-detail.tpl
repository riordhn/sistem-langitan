<table class="ui-widget" style="width:658px;">
		<tr class="ui-widget-header">
			<th colspan="2" class="header-coloumn"><h2>Edit Penelitian</h2></th>
		</tr>
		<tr class="ui-widget-content">
			<td width="25%"><span>Judul Penelitian</span></td>
			<td width="75%">{$data_penelitian.JUDUL_PENELITIAN}</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Abstrak</span></td>
			<td>{$data_penelitian.ABSTRAK_PENELITIAN}</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Kata Kunci</span></td>
			<td>{$data_penelitian.KEYWORD_PENELITIAN}</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Kategori</span></td>
			<td>{$data_penelitian.KATEGORI_PENELITIAN}</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Sumber Dana</span></td>
			<td>{$data_penelitian.SUMBER_DANA_PENELITIAN}</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Dana</span></td>
			<td>{$data_penelitian.DANA_PENELITIAN}</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Tahun</span></td>
			<td>{$data_penelitian.THN_PENELITIAN}</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Tim Peneliti</span></td>
			<td>{$data_penelitian.TIM_PENELITIAN}</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Publikasi</td>
			<td>{$data_penelitian.PUBLIKASI_PENELITIAN}</td>
		</tr>
		<tr class="ui-widget-content">
			<td>Halaman Naskah</td>
			<td>{$data_penelitian.HALAMAN_NASKAH_PENELITIAN}</td>
		</tr>
		<tr class="ui-widget-content">
			<td>SK Penetapan</td>
			<td>{$data_penelitian.SK_PENETAPAN_PENELITIAN}</td>
		</tr>
</table>
