{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">EVALUASI DOSEN</div> 
<div id="tabs" style="width: 100%">
    <ul>
        <li><a class="disable-ajax" href="#tabs-evaluasi-kuliah">Evaluasi Kuliah</a></li>
        <li><a class="disable-ajax" href="#tabs-evaluasi-perwalian">Perwalian</a></li>        
        <li><a class="disable-ajax" href="#tabs-evaluasi-praktikum">Evaluasi Praktikum</a></li>
        <li><a class="disable-ajax" href="#tabs-evaluasi-bimbingan-skripsi">Evaluasi Bimbingan Skripsi</a></li>
    </ul>
    <div id="tabs-evaluasi-kuliah">
        <div class="tab_content" style="padding:15px;border: 1px solid #4578a2;overflow:scroll;">
            <table class="ui-widget">
                <tr class="ui-widget-content" width="100%">
                    <td style="vertical-align:middle"><span>Mata Kuliah</span></td>
                    <td>
                        <select id="select_kelas_mk">
                            <option value="">Pilih</option>
                            {foreach $data_kelas_mk as $data}
                                <optgroup label="{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})">
                                    {foreach $data.DATA_KELAS_MK as $kelas}
                                        {if $kelas.STATUS_MKTA!='6' && $kelas.STATUS_PRAKTIKUM!='1'}
                                            <option value="{$kelas.ID_KELAS_MK}" 
                                                    {if $kelas.ID_KELAS_MK==$smarty.post.id_kelas_mk}
                                                        selected="true"
                                                    {/if}
                                                    >{$kelas.NM_MATA_KULIAH} ({$kelas.KD_MATA_KULIAH}) Kelas {$kelas.NM_KELAS} <br/> Prodi  {$kelas.NM_PROGRAM_STUDI}
                                        </option>
                                    {/if}
                                {/foreach}
                            {/foreach}
                        </select>
                    </td>
                </tr>
            </table>
            <div id="hasil-evaluasi-kuliah" ></div>
        </div>
    </div>
    <div id="tabs-evaluasi-perwalian">
        <div class="tab_content" style="padding:15px;border: 1px solid #4578a2;">
            <table class="tablesorter ui-widget"  width="90%">
                <thead>
                    <tr class="ui-widget-header">
                        <th colspan="4" class="header-coloumn">Evaluasi Perwalian</th>
                    </tr>
                    <tr class="ui-widget-content">
                        <th>No</th>
                        <th>Materi</th>
                        <th width="20%">Rata-Rata Nilai</th>
                        <th>Total Mahasiswa Eval</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $data_eva_wali as $data}
                        <tr class="ui-widget-content">
                            <td class="center">{$data@index+1}</td>
                            <td>{$data.EVALUASI_ASPEK}</td>
                            <td class="center">
                                {round($data.RATA, 2)}
                            </td>
                            <td class="center"> {$data.JUMLAH_MHS}</td>
                        </tr>
                    {foreachelse}
                        <tr class="ui-widget-content">
                            <td class="center" colspan="4"><span class="ui-state-error-text">Data Masih Kosong/Belum Di Olah</span></td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
            <div class="dialog-eva-wali" id="dialog-eval-wali1" title="Komentar Perwalian">
                <table class="ui-widget">
                    <tr class="ui-widget-header">
                        <th class="header-coloumn">{$data_eva_wali[8].ASPEK}</th>
                    </tr>
                    {foreach $data_eva_wali[0].KOMENTAR1 as $data}
                        {if $data.A9!='--'&&$data.A10!='-'}
                            <tr class="ui-widget-content">
                                <td>{$data.A9}</td>
                            </tr>
                        {/if}
                    {/foreach}
                </table>
            </div>
            <div class="dialog-eva-wali" id="dialog-eval-wali2" title="Komentar Perwalian">
                <table class="ui-widget">
                    <tr class="ui-widget-header">
                        <th class="header-coloumn">{$data_eva_wali[9].ASPEK}</th>
                    </tr>
                    {foreach $data_eva_wali[0].KOMENTAR2 as $data}
                        {if $data.A10!='--'&&$data.A10!='-'}
                            <tr class="ui-widget-content">
                                <td>{$data.A10}</td>
                            </tr>
                        {/if}
                    {/foreach}
                </table>
            </div>
        </div>
    </div>
    <div id="tabs-evaluasi-praktikum">
        <div class="tab_content"  style="padding:15px;border: 1px solid #4578a2;">
            <table class="ui-widget">
                <tr class="ui-widget-content" width="100%">
                    <td style="vertical-align:middle"><span>Mata Kuliah Praktikum</span></td>
                    <td>
                        <select id="select_kelas_mk_prak">
                            <option value="">Pilih</option>
                            {foreach $data_kelas_mk as $data}
                                <optgroup label="{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})">
                                    {foreach $data.DATA_KELAS_MK as $kelas}
                                        {if $kelas.STATUS_MKTA=='6' || $kelas.STATUS_PRAKTIKUM=='1'}
                                            <option value="{$kelas.ID_KELAS_MK}" 
                                                    {if $kelas.ID_KELAS_MK==$smarty.post.id_kelas_mk}
                                                        selected="true"
                                                    {/if}
                                                    >{$kelas.NM_MATA_KULIAH} ({$kelas.KD_MATA_KULIAH}) Kelas {$kelas.NM_KELAS} <br/> Prodi  {$kelas.NM_PROGRAM_STUDI}
                                        </option>
                                    {/if}
                                {/foreach}
                            {/foreach}
                        </select>
                    </td>
                </tr>
            </table>
            <div id="hasil-evaluasi-praktikum" ></div>
        </div>
    </div>
    <div id="tabs-evaluasi-bimbingan-skripsi">
        <div class="tab_content" style="padding:15px;border: 1px solid #4578a2;">
            <table class="tablesorter ui-widget"  width="80%">
                <thead>
                    <tr class="ui-widget-header">
                        <th colspan="3" class="header-coloumn">Evaluasi Bimbingan Skripsi</th>
                    </tr>
                    <tr class="ui-widget-content">
                        <th>No</th>
                        <th>Materi</th>
                        <th>Persentase / Nilai</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="ui-widget-content">
                        <td colspan="3" class="center" ><span class="ui-state-error-text">Data Masih Kosong/Belum Di Olah</span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
{literal}
    <script type="text/javascript">
        $("#tabs").tabs();
        $(".dialog-eva-kul, .dialog-eva-wali").dialog({
            width: 500,
            modal: true,
            autoOpen: false
        });
        $('#select_kelas_mk').change(function() {
            $.ajax({
                url: '/modul/dosen/load-data-evakul.php',
                type: 'post',
                data: 'id_kelas_mk=' + $('#select_kelas_mk').val(),
                beforeSend: function() {
                    $('#hasil-evaluasi-kuliah').html('<div style="width: 100%;" align="center"><img src="{$base_url}js/loading.gif" /></div>');
                },
                success: function(data) {
                    $('#hasil-evaluasi-kuliah').html(data);
                }
            })
        });
        $('#select_kelas_mk_prak').change(function() {
            $.ajax({
                url: '/modul/dosen/load-data-evaprak.php',
                type: 'post',
                data: 'id_kelas_mk=' + $('#select_kelas_mk_prak').val(),
                beforeSend: function() {
                    $('#hasil-evaluasi-praktikum').html('<div style="width: 100%;" align="center"><img src="{$base_url}js/loading.gif" /></div>');
                },
                success: function(data) {
                    $('#hasil-evaluasi-praktikum').html(data);
                }
            })
        });
    </script>
{/literal}
