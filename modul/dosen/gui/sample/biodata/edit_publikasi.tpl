<div class="center_title_bar">Edit Publikasi</div>
<form name="form_edit" id="form_edit" method="post" action="publikasi.php" enctype="multipart/form-data">
    <table class="ui-widget" style="width:80%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn"><h2>Edit Publikasi Perkuliahan</h2></th>
        </tr>
        <tr class="ui-widget-content">
            <td width="20%"><span>Media Publikasi</span></td>
            <td width="80%">
                <input type="hidden" name="id" value="{$data_publikasi.ID_DOSEN_PUBLIKASI}"/>
                <select id="input_jenis" name="jenis">
                    <option {if $data_publikasi.JENIS_DOSEN_PUBLIKASI=='Buku'} selected="true"{/if} value="Buku">Buku</option>
                    <option {if $data_publikasi.JENIS_DOSEN_PUBLIKASI=='Journal'} selected="true"{/if}value="Journal">Journal</option>
                    <option {if $data_publikasi.JENIS_DOSEN_PUBLIKASI=='Petunjuk Praktikum'} selected="true"{/if}value="Petunjuk Praktikum">Petunjuk Praktikum</option>
                    <option {if $data_publikasi.JENIS_DOSEN_PUBLIKASI=='Diktat'} selected="true"{/if}value="Diktat">Diktat</option>
                    <option {if $data_publikasi.JENIS_DOSEN_PUBLIKASI=='Seminar'} selected="true"{/if}value="Seminar">Seminar</option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                <span>Peran</span>
            </td>
            <td>
                <select id="input_peran" name="peran">
	                <option {if $data_publikasi.PERAN_DOSEN_PUBLIKASI=='Anggota'} selected="true"{/if}value="Anggota">Anggota</option>
                    <option {if $data_publikasi.PERAN_DOSEN_PUBLIKASI=='Penulis Utama'} selected="true"{/if} value="Penulis Utama">Penulis Utama
                    </option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                <span>Judul</span>
            </td>
            <td>
                <textarea id="judul" name="judul" class="required">{$data_publikasi.JUDUL_DOSEN_PUBLIKASI}</textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                <span>Penerbit</span>
            </td>
            <td>
                <input type="text" id="penerbit"  size="50" name="penerbit" value="{$data_publikasi.PENERBIT_DOSEN_PUBLIKASI}"  class="required"/>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td><span>Pengarang</span></td>
            <td>
				<textarea name="pengarang" id="pengarang" class="required">{$data_publikasi.PENGARANG_DOSEN_PUBLIKASI}</textarea>
            </td>
        </tr>
		<tr class="ui-widget-content">
            <td><span>Media</span></td>
            <td>
                <input type="text" size="50" id="media" name="media" value="{$data_publikasi.MEDIA_DOSEN_PUBLIKASI}"  class="required"/>
            </td>
        </tr>
		<tr class="ui-widget-content">
            <td><span>Tahun</span></td>
            <td>
                <input type="text" id="tahun" name="tahun" value="{$data_publikasi.THN_DOSEN_PUBLIKASI}"  class="required number"/>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                <span>Edisi</span>
            </td>
            <td>
                <input type="text" id="edisi"  size="50" name="edisi" value="{$data_publikasi.EDISI_DOSEN_PUBLIKASI}"  class="required"/>
            </td>
        </tr><tr class="ui-widget-content">
            <td>
                <span>Nama Lembaga Sitasi</span>
            </td>
            <td>
                <input type="text" id="lembaga"  size="50" name="lembaga" value="{$data_publikasi.LEMBAGA_DOSEN_PUBLIKASI}"  class="required"/>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>
                <span>Tingkat</span>
            </td>
            <td>
                <select id="input_tingkat" name="tingkat">
	                <option {if $data_publikasi.TINGKAT_DOSEN_PUBLIKASI=='Lokal'} selected="true"{/if}value="Lokal">Lokal</option>
                    <option {if $data_publikasi.TINGKAT_DOSEN_PUBLIKASI=='Nasional'} selected="true"{/if} value="Nasional">Nasional</option>
                    <option {if $data_publikasi.TINGKAT_DOSEN_PUBLIKASI=='Internasional'} selected="true"{/if} value="Internasional">Internasional</option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" style="text-align:right">
				<input type="hidden" name="mode" value="edit"/>
				<a id="save" style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all">Save</a>
				<a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="publikasi.php">Cancel</a>
			</td>
        </tr>

    </table>
</form>
{literal}
<script type="text/javascript">
$('#save').click(function(){
    $('#form_edit').submit();
});
$('#form_edit').validate();
</script>
{/literal}