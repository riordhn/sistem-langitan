<div class="center_title_bar">Edit Organisasi</div> 
<form id="form_edit_organisasi" name="form_add_organisasi" method="post" action="organisasi.php">
	<table class="ui-widget" width="50%">
		<tr class="ui-widget-header">
			<th colspan="2" class="header-coloumn"><h2>Edit Organisasi</h2></th>
		</tr>

		<tr class="ui-widget-content">
			<td><span>Organisasi</span></td>
			<td>
				<textarea name="organisasi" class="required">{$data_organisasi.NM_DOSEN_ORG}</textarea>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Jabatan</span></td>
			<td>
				<input type="text" name="jabatan" class="required" value="{$data_organisasi.JABATAN_DOSEN_ORG}"/>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Mulai Jabatan</span></td>
			<td>
				<input type="text" name="mulai" class="required number" value="{$data_organisasi.MULAI_DOSEN_ORG}" />
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Selesai Jabatan</span></td>
			<td>
				<input type="text" name="selesai" class="required number"  value="{$data_organisasi.SELESAI_DOSEN_ORG}"/>
			</td>
		</tr>
        <tr class="ui-widget-content">
            <td>
                <span>Tingkat</span>
            </td>
            <td>
                <select id="input_tingkat" name="tingkat">
	                <option {if $data_organisasi.TINGKAT_DOSEN_ORG=='Lokal'} selected="true"{/if}value="Lokal">Lokal</option>
                    <option {if $data_organisasi.TINGKAT_DOSEN_ORG=='Nasional'} selected="true"{/if} value="Nasional">Nasional</option>
                    <option {if $data_organisasi.TINGKAT_DOSEN_ORG=='Internasional'} selected="true"{/if} value="Internasional">Internasional</option>
                </select>
            </td>
        </tr>
		<tr class="ui-widget-content">
			<td class="center" colspan="2">
				<input type="submit" value="Update" style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" />
				<a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="organisasi.php">Cancel</a>
				<input type="hidden" value="edit" name="mode"/>
				<input type="hidden" value="{$data_organisasi.ID_DOSEN_ORGANISASI}" name="id_organisasi" />
			</td>
		</tr>
	</table>
</form>
{literal}
	<script type="text/javascript">
		$('#form_edit_organisasi').validate();
	</script>
{/literal}
