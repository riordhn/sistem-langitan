<form id="dialog-form">
<table style="border-collapse: collapse; width: 100%;">
    <tr>
        <td>Tahun Ke</td>
        <td>
            <input type="text" name="dialog_tahun_ke" id="dialog-tahun-ke" size="4" />
        </td>
    </tr>
    <tr>
        <td>Biaya diajukan ke DIKTI</td>
        <td>
            <input type="text" name="dialog_biaya_dikti" id="dialog-biaya-dikti" size="12" />
        </td>
    </tr>
    <tr>
        <td>Biaya dari instansi lain</td>
        <td>
            <input type="text" name="dialog_biaya_lain" id="dialog-biaya-lain" size="12" />
        </td>
    </tr>
</table>
</form>

<script type="text/javascript">
$(document).ready(function() {
    $('#dialog-form').validate({
        rules : {
            dialog_tahun_ke     : { required: true, number: true },
            dialog_biaya_dikti  : { required: true, number: true },
            dialog_biaya_lain   : { required: true, number: true }
        },
        messages : {
            dialog_tahun_ke     : { required: 'Isikan tahun ke', number: 'Isian harus berformat angka' },
            dialog_biaya_dikti  : { required: 'Isikan biaya dari dikti jika ada', number: 'Isian harus berformat angka' },
            dialog_biaya_lain   : { required: 'Isikan biaya lain jika ada', number: 'Isian harus berformat angka' }
        }
    });
});
</script>