<div class="center_title_bar">Penelitian</div>

<table>
    <tr>
        <th>No</th>
        <th style="width: 60%">Judul</th>
        <th>Tahun</th>
        <th>Status</th>
        <th colspan="2">Aksi</th>
    </tr>
    {foreach $penelitian_set as $p}
    <tr id="r{$p.ID_PENELITIAN}" {if $p@index is not div by 2}class="alternate"{/if}>
        <td class="center">{$p@index + 1}</td>
        <td>{$p.JUDUL}</td>
        <td class="center">{$p.TAHUN}</td>
        <td>-</td>
        <td>
            <a href="penelitian.php?mode=view-anggota&id_penelitian={$p.ID_PENELITIAN}" title="Edit Anggota"><img src="{$base_url}img/dosen/penelitian-anggota.png" /></a>
            <a href="penelitian.php?mode=view-biaya&id_penelitian={$p.ID_PENELITIAN}" title="Edit Pembiayaan"><img src="{$base_url}img/dosen/penelitian-biaya.png" /></a>
        </td>
        <td class="center">
            <a href="penelitian.php?mode=edit&id_penelitian={$p.ID_PENELITIAN}" title="Edit" style="margin: 0px 5px"><img src="{$base_url}img/dosen/penelitian-edit.png" /></a>
            <span style="cursor: pointer;margin: 0px 5px" title="Hapus" onclick="DeletePenelitian({$p.ID_PENELITIAN});"><img src="{$base_url}img/dosen/penelitian-delete.png" /></span>
            {if $p.TGL_SUBMIT == ''}
            <span id="s{$p.ID_PENELITIAN}" style="cursor: pointer;margin: 0px 5px" title="Submit form" onclick="SubmitPenelitian({$p.ID_PENELITIAN});"><img src="{$base_url}img/dosen/penelitian-submit.png" /></span>
            {/if}
            {if $p.TGL_APPROVE_DEKAN != ''}
            <a href="penelitian.php?mode=get-pdf&id_penelitian={$p.ID_PENELITIAN}" class="disable-ajax" title="Download PDF" target="_blank"><img src="{$base_url}img/dosen/pdf-16.png"/></a>
            {/if}
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="6" class="center">
            <a href="penelitian.php?mode=add">Tambah</a>
        </td>
    </tr>
</table>