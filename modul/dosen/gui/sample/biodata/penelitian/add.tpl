<div class="center_title_bar">Penelitian - Tambah</div>

<a href="penelitian.php">Kembali ke Daftar Penelitian</a>

<form action="penelitian.php?mode=add" method="post" id="form-penelitian">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <th colspan="2">Penelitian</th>
        </tr>
        <tr>
            <td>Judul</td>
            <td><textarea cols="75" rows="3" name="judul"></textarea></td>
        </tr>
        <tr>
            <td>Jenis Penelitian</td>
            <td>
                <select name="id_penelitian_jenis" id="penelitian-jenis">
                    <option value="">--</option>
                {foreach $penelitian_jenis_set as $pj}
                    <option value="{$pj.ID_PENELITIAN_JENIS}">{$pj.NAMA_JENIS}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr id="row-kerjasama" style="display: none">
            <td>Nama Institusi (Kerjasama)</td>
            <td><input type="text" name="nama_institusi" size="50" maxlength="100" /></td>
        </tr>
        <tr id="row-skim" style="display: none">
            <td>SKIM (Dikti)</td>
            <td>
                <select name="id_penelitian_skim" id="penelitian-skim">
                    <option value="">--</option>
                {foreach $penelitian_skim_set as $ps}
                    <option value="{$ps.ID_PENELITIAN_SKIM}">[{$ps.KODE_SKIM}] {$ps.NAMA_SKIM}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Bidang / Tema Penelitian</td>
            <td>
                <select name="id_penelitian_bidang" id="penelitian-bidang">
                {foreach $penelitian_bidang_set as $pb}
                    <option value="{$pb.ID_PENELITIAN_BIDANG}">{$pb.NAMA_TEMA}</option>
                {/foreach}
                </select>
                <input type="text" style="display: none" id="penelitian-bidang-lain" name="penelitian_bidang_lain" size="75" maxlength="100" value="{$p.PENELITIAN_BIDANG_LAIN}" />
            </td>
        </tr>
        <tr>
            <th colspan="2">Deskripsi Penelitian</th>
        </tr>
        <tr>
            <td style="width: 20%">Lokasi Penelitian</td>
            <td><input type="text" name="lokasi" maxlength="50" size="30" value="" /></td>
        </tr>
        <tr>
            <td>Jangka Waktu Penelitian</td>
            <td><input type="text" name="jangka_waktu" maxlength="5" size="4" value=""/> Tahun</td>
        </tr>
        <tr>
            <td>Penelitian Tahun Ke</td>
            <td><input type="text" name="jangka_waktu_ke" maxlength="5" size="4" value=""/></td>
        </tr>
        <tr>
            <td>Jumlah Anggota</td>
            <td>0</td>
        </tr>
        <tr>
            <td>Pembiayaan</td>
            <td>Rp 0</td>
        </tr>
        <tr>
            <th colspan="2">Ketua Tim Peneliti Mitra (TPM)</th>
        </tr>
        <tr>
            <td>Nama</td>
            <td><input type="text" name="tpm_nama" maxlength="100" size="30" /></td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>
                <select name="tpm_jenis_kelamin">
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>NIP</td>
            <td><input type="text" name="tpm_nip" maxlength="20" /></td>
        </tr>
        <tr>
            <td>Golongan</td>
            <td>
                <select name="tpm_id_golongan">
                    <option value="">--</option>
                {foreach $golongan_set as $g}
                    <option value="{$g.ID_GOLONGAN}">{$g.NM_GOLONGAN}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Jabatan Fungsional</td>
            <td><input type="text" name="tpm_jabatan_fungsional" size="30" maxlength="50" /></td>
        <tr>
        </tr>
            <td>Jabatan Struktural</td>
            <td><input type="text" name="tpm_jabatan_struktural" size="30" maxlength="50" /></td>
        </tr>
        <tr>
            <td>Perguruan Tinggi</td>
            <td><input type="text" name="tpm_pt" size="50" maxlength="50" /></td>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td><input type="text" name="tpm_fakultas" size="30" maxlength="50"/></td>
        </tr>
        <tr>
            <td>Jurusan</td>
            <td><input type="text" name="tpm_prodi" size="30" maxlength="50"/></td>
        </tr>
        <tr>
            <td>Alamat Kantor</td>
            <td>
                <textarea name="tpm_alamat_kantor" rows="2" cols="50" style="width: auto;"></textarea><br/>
                Telp : <input type="text" name="tpm_telp_kantor" size="20" maxlength="32"/>
                Fax : <input type="text" name="tpm_fax_kantor" size="20" maxlength="32"/>
            </td>
        </tr>
        <tr>
            <td>Alamat Rumah</td>
            <td>
                <textarea name="tpm_alamat_rumah" rows="2" cols="50" style="width: auto;" maxlength="256"></textarea><br/>
            </td>
        </tr>
        <tr>
            <td>Telepon Rumah</td>
            <td><input type="text" name="tpm_telp_rumah" size="20"/></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><input type="text" name="tpm_email" size="30" /></td>
        </tr>
        <tr>
            <th colspan="2">Dekan</th>
        </tr>
        <tr>
            <td>NIP Dekan</td>
            <td>{$dekan.NIP_DEKAN}</td>
        </tr>
        <tr>
            <td>Nama Dekan</td>
            <td>{$dekan.NM_DEKAN}</td>
        </tr>
        <tr>
            <th colspan="2">Ketua LPPM</th>
        </tr>
        <tr>
            <td>NIP Ketua LPPM</td>
            <td>{$lppm.NIP_KETUA_LPPM}</td>
        </tr>
        <tr>
            <td>Nama Ketua LPPM</td>
            <td>{$lppm.NAMA_KETUA_LPPM}</td>
        </tr>
        <tr>
            <th colspan="2">Data Pengisian</th>
        </tr>
        <tr>
            <td width="20%"><strong>Kota</strong></td>
            <td><input type="text" name="kota" value="{$p.KOTA}" /></td>
        </tr>
        <tr>
            <td><strong>Tanggal Pengisian</strong></td>
            <td>
                {html_select_date prefix="tgl_penelitian_" start_year="-50" field_order="DMY"}
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" name="id_dekan" value="{$dekan.ID_DEKAN}"/>
                <input type="hidden" name="id_ketua_lppm" value="{$lppm.ID_KETUA_LPPM}"/>
                <a href="penelitian.php">Batal</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>

<div id="dialog"></div>
<script type="text/javascript">$.getScript('gui/sample/biodata/penelitian/add.js');</script>