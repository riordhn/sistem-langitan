$(document).ready(function() {
    
    $('#penelitian-jenis').change(function() {
        
        // Khusus kerjasama
        if (this.value == 3)
            $('#row-kerjasama').show();
        else
            $('#row-kerjasama').hide();
        
        // SKIM Penelitian
        $.ajax({
            type: 'GET',
            url: 'penelitian.php',
            data: 'mode=get-penelitian-skim&id_penelitian_jenis='+this.value,
            beforeSend: function() { $('#penelitian-skim').html(''); },
            success: function(data) {
                if (data != '0')
                {
                    $('#row-skim').show();    
                    $('#penelitian-skim').html(data);
                }
                else
                {
                    $('#row-skim').hide();
                }
            }
        });

        // Bidang / Tema Penelitian
        $.ajax({
            type: 'GET',
            url: 'penelitian.php',
            data: 'mode=get-penelitian-bidang&id_penelitian_jenis='+this.value,
            beforeSend: function() { $('#penelitian-bidang').html('<option>--</option>'); },
            success: function(data) { $('#penelitian-bidang').html(data); $('#penelitian-bidang').change(); }
        });
    });
    
    // Bidang penelitian / tema
    $('#penelitian-bidang').change(function() {
        if ($('#penelitian-bidang :selected').text() == 'Lainnya')
            $('#penelitian-bidang-lain').show();
        else
            $('#penelitian-bidang-lain').hide();
    });
    
    
    // Validasi form
    $('#form-penelitian').validate({
        rules: {
            judul:                  { required: true },
            id_penelitian_jenis:    { required: true },
            id_penelitian_bidang:   { required: true },
            lokasi:                 { required: true },
            jangka_waktu:           { required: true, number: true },
            jangka_waktu_ke:        { required: true, number: true },
            kota:                   { required: true }
        },
        messages: {
            judul:                  { required: 'Judul penelitian harus di isi' },
            id_penelitian_jenis:    { required: 'Jenis penelitian harus di isi' },
            id_penelitian_bidang:   { required: 'Bidang penelitian harus di isi' },
            lokasi:                 { required: 'Lokasi penelitian harus di isi' },
            jangka_waktu:           { required: 'Jangka waktu penelitian harus di isi', number: 'Format isian harus angka' },
            jangka_waktu_ke:        { required: 'Jangka waktu ke penelitian harus di isi', number: 'Format isian harus angka' },
            kota:                   { required: 'Kota pengisian form' }
        },
        ignore: '.ignore',
        errorPlacement: function(error, element) { error.appendTo(element.parent()); }
    });
    
    // Trigger Jenis penelitian
    var penelitian_jenis = $('#penelitian-jenis :selected').text();
                
    if (penelitian_jenis == 'Kerjasama Institusi')
    {
        $('#row-kerjasama').show();
        $('#row-skim').hide();
    }
    else if (penelitian_jenis == 'DIKTI')
    {
        $('#row-kerjasama').hide();
        $('#row-skim').show();
    }
    else
    {
        $('#row-kerjasama').hide();
        $('#row-skim').hide();
    }
});