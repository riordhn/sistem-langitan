<div class="center_title_bar">Penelitian - Detail Pembiayaan</div>

<h3>JUDUL PENELITIAN : {$penelitian.JUDUL}</h3>

<a href="penelitian.php">Kembali ke Daftar Penelitian</a>

<table>
    <tr>
        <th>Pembiayaan Tahun Ke</th>
        <th>Besar Biaya DIKTI</th>
        <th>Besar Biaya Lain</th>
    </tr>
    {foreach $penelitian_biaya_set as $pb}
    <tr>
        <td class="center">{$pb.TAHUN_KE}</td>
        <td class="right">Rp {number_format($pb.BESAR_BIAYA_DIKTI)}</td>
        <td class="right">Rp {number_format($pb.BESAR_BIAYA_LAIN)}</td>
    </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="3">
            <a href="penelitian.php?mode=add-biaya&id_penelitian={$smarty.get.id_penelitian}">Tambah Biaya</a>
        </td>
    </tr>
</table>

