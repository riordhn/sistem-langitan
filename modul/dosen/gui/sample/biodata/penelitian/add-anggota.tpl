<div class="center_title_bar">Penelitian - Tambah Anggota</div>

<h3>JUDUL PENELITIAN : {$penelitian.JUDUL}</h3>

<a href="penelitian.php?mode=view-anggota&id_penelitian={$smarty.get.id_penelitian}">Kembali Ke Daftar Anggota</a>

<form action="penelitian.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="{$smarty.get.mode}" />
<input type="hidden" name="id_penelitian" value="{$smarty.get.id_penelitian}" />
<table>
    <tr>
        <td>Asal Anggota</td>
        <td>
            <select id="dialog-asal-anggota" name="asal_anggota">
                <option value="1">Universitas Airlangga</option>
                <option value="2">Non-Universitas Airlangga</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>Nama Anggota</td>
        <td>
            <input type="text" id="dialog-nama-anggota" name="nama_anggota" size="40"/><img id="dialog-ajax-loading" style="display:none" src="{$base_url}img/dosen/ajax-loader-16.gif" />
            <div id="dialog-ajax-result" style="width: 100%; border: 1px #A6C9E2 solid; display: none"></div>
        </td>
    </tr>
    <tr id="dialog-row-nip" style="display: none">
        <td>NIP / NIK / NIM</td>
        <td>
            <input type="text" id="dialog-nip-anggota" name="nip_anggota" />
        </td>
    </tr>
    <tr id="dialog-row-gelar-depan" style="display: none">
        <td>Gelar Depan</td>
        <td><input type="text" id="dialog-gelar-depan" name="gelar_depan"/></td>
    </tr>
    <tr id="dialog-row-gelar-belakang" style="display: none">
        <td>Gelar Belakang</td>
        <td><input type="text" id="dialog-gelar-belakang" name="gelar_belakang"/></td>
    </tr>
    <tr>
        <td class="center" colspan="2">
            <input type="submit" value="Tambahkan" />
        </td>
    </tr>
</table>
</form>

<p id="dialog-help" style="margin: 13px 0px; font-size: 12px">NB: Masukkan minimal 5 huruf pada isian nama anggota untuk melakukan pencarian</p>

<script type="text/javascript">
    $(document).ready(function() {
        
        // pilihan asal anggota
        $('#dialog-asal-anggota').change(function() {
            if (this.value == 1)
            {
                $('#dialog-row-nip').hide();
                $('#dialog-row-gelar-depan').hide();
                $('#dialog-row-gelar-belakang').hide();
                $('#dialog-help').show();
                
                $('#dialog-nama-anggota').val('');
            }
            else
            {
                $('#dialog-row-nip').show();
                $('#dialog-row-gelar-depan').show();
                $('#dialog-row-gelar-belakang').show();
                $('#dialog-help').hide();
                
                $('#dialog-ajax-result').html('');
                $('#dialog-ajax-result').hide();
            }
        });
        
        // Key UP ada nama anggota
        $('#dialog-nama-anggota').keyup(function() {
            
            // Jika unair, maka pakai auto-complete
            if ($('#dialog-asal-anggota').val() == 1)
            {
                // Memastikan minimal 3 huruf baru di eksekusi
                if ($.trim($(this).val()).length >= 5)
                {
                    // melakukan query pencarian
                    $.ajax({
                        url: 'penelitian.php', data: 'mode=find-anggota&q=' + $.trim($('#dialog-nama-anggota').val()),
                        beforeSend: function() { $('#dialog-ajax-loading').show(); },
                        success: function(r) {
                            // sembunyikan loading
                            $('#dialog-ajax-loading').hide();
                            
                            // set html
                            $('#dialog-ajax-result').html(r);
                            
                            // tampilkan hasil
                            $('#dialog-ajax-result').show();
                        }
                    });
                }
                else
                {
                    // Hidden result
                    $('#dialog-ajax-result').hide();
                }
            }
        });
        
        
        
    });
</script>