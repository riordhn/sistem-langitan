<div class="center_title_bar">Tambah Penelitian</div> 

<form id="form_edit_penelitian" name="form_edit_penelitian" method="post" action="penelitian.php">
    <input type="hidden" name="mode" value="add" />
    <input type="hidden" name="id_dosen" value="{$id_dosen}" />
    <input type="hidden" name="id_program_studi" value="{$id_program_studi}" />
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn"><h2>Info Penelitian</h2></th>
        </tr>
        <tr class="ui-widget-content">
            <td><span>Judul Penelitian</span></td>
            <td>
                <textarea cols="60" rows="2" name="judul_penelitian" maxlength="256"></textarea><br/>
				<label for="judul_penelitian" style="display: none;" class="error">Tidak Boleh Kosong</label>
            </td>                
        </tr>
        <tr class="ui-widget-content">
            <td><span>Abstrak</span></td>
            <td>
                <textarea cols="80" rows="8" name="abstrak_penelitian" maxlength="2048"></textarea><br/>
                <label for="abstrak_penelitian" style="display: none;" class="error">Tidak Boleh Kosong</label>
			</td>
		</tr>
        <tr class="ui-widget-content">
            <td><span>Kata Kunci</span></td>
            <td>
				<input type="text" name="keyword_penelitian" size="30" maxlength="64" /><br/>
				<label for="keyword_penelitian" style="display: none;" class="error">Tidak Boleh Kosong</label>
			</td>
		</tr>
        <tr class="ui-widget-content">
            <td><span>Kategori</span></td>
            <td>
				<input type="text" name="kategori_penelitian" size="50" maxlength="64" /><br/>
                <label for="kategori_penelitian" style="display: none;" class="error">Tidak Boleh Kosong</label>
			</td>
		</tr>
        <tr class="ui-widget-content">
            <td><span>Sumber Dana</span></td>
            <td>
				<input type="text" name="sumber_dana_penelitian" size="50" maxlength="128" /><br/>
                <label for="sumber_dana_penelitian" style="display: none;" class="error">Tidak Boleh Kosong</label>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td><span>Dana</span></td>
            <td>
				<input type="text" name="dana_penelitian" /><br/>
                <label for="dana_penelitian" style="display: none;" class="error">Harus angka</label>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td><span>Tahun</span></td>
            <td>
				<input type="text" name="thn_penelitian" /><br/>
                <label for="thn_penelitian" style="display: none;" class="error">Tidak Boleh Kosong/format tahun salah</label>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td><span>Tim Peneliti</span></td>
            <td>
				<textarea name="tim_penelitian" cols="25" rows="2" maxlength="512"></textarea><br/>
                <label for="tim_penelitian" style="display: none;" class="error">Tidak Boleh Kosong</label>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Publikasi</td>
            <td>
				<input type="text" name="publikasi_penelitian" maxlength="128" size="50" /><br/>
			</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Halaman Naskah</td>
            <td>
				<input type="text" name="halaman_naskah_penelitian" size="4" maxlength="4" />
			</td>
        </tr>
        <tr class="ui-widget-content">
            <td>SK Penetapan</td>
            <td>
				<input type="text" name="sk_penetapan_penelitian" maxlength="128" size="50" />
			</td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="penelitian.php">Kembali</a>
                <input  style="cursor: pointer;padding:2px;" class="ui-button ui-state-default ui-corner-all" type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
$('#form_edit_penelitian').validate({
    rules : {
        judul_penelitian :{ required :true },
        abstrak_penelitian :{ required :true },
        sumber_dana_penelitian :{ required :true },
        dana_penelitian :{ number: true },
        thn_penelitian :{ required :true, number: true },
        tim :{ required :true },
                halaman_naskah_penelitian: { number: true }
    }
});       
    </script>
{/literal}