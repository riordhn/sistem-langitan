<div class="center_title_bar">Add Organisasi</div> 
<form id="form_add_organisasi" name="form_add_organisasi" method="post" action="organisasi.php">
	<table class="ui-widget" width="50%">
		<tr class="ui-widget-header">
			<th colspan="2" class="header-coloumn"><h2>Add Organisasi</h2></th>
		</tr>

		<tr class="ui-widget-content">
			<td><span>Organisasi</span></td>
			<td>
				<textarea name="organisasi" class="required"></textarea>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Jabatan</span></td>
			<td>
				<input type="text" name="jabatan" class="required" />
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Mulai Jabatan</span></td>
			<td>
				<input type="text" name="mulai" class="required number" />
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Selesai Jabatan</span></td>
			<td>
				<input type="text" name="selesai" class="required number" />
			</td>
		</tr>
        <tr class="ui-widget-content">
            <td>
                <span>Tingkat</span>
            </td>
            <td>
                <select id="input_tingkat" name="tingkat">
	                <option value="Lokal">Lokal</option>
                    <option value="Nasional">Nasional</option>
                    <option value="Internasional">Internasional</option>
                </select>
            </td>
        </tr>
		<tr class="ui-widget-content">
			<td class="center" colspan="2">
				<input type="submit" value="Save" style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" />
				<a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="organisasi.php">Cancel</a>
				<input type="hidden" value="add" name="mode"/>
			</td>
		</tr>
	</table>
</form>
{literal}
	<script type="text/javascript">
		$('#form_add_organisasi').validate();
	</script>
{/literal}
