<div class="center_title_bar">Info Dosen</div>
<div id="dialog_info_dosen" style="display: none;">Data Info Dosen Berhasil disimpan</div>
<div id="alert_info_dosen" style="display: none;">Klik edit terlebih dahulu !</div>
<form name="info_dosen" id="form_info_dosen" action="info_dosen.php" method="post">
    <table class="none" style="width: 98%;">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">INFORMASI DATA PEGAWAI</th>
        </tr>
        <tr>
            <td width="20%"><span class="field">Fak/Unit</span></td>
            <td width="30%"><span>{$dosen['FAKULTAS']|upper}</span></td>
            <td width="20%"><span class="field">Jur/bag/lab</span></td>
            <td width="30%"><span>{$dosen['PRODI']|upper}</span></td>
        </tr>
        <tr>
            <td><span class="field">Jenis Peg</span></td>
            <td><span>{$dosen['JENIS_PEGAWAI']|upper}</span></td>
            <td><span class="field">Status</span></td>
            <td><span>AKTIF</span></td>
        </tr>
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">A.IDENTITAS PRIBADI</th>
        </tr>
        <tr>
            <td><span class="field">Nama  </span></td>
            <td>
                <textarea id="nama" name="nama">{$dosen['NAMA']|upper}</textarea>                    
            </td>
            <td><span class="field">NIP </span></td>
            <td><span>{$dosen['USERNAME']}</span></td>
        </tr>
        <tr>
            <td><span class="field">Gelar</span></td>
            <td colspan=3>
                <label for="gelar_depan">Gelar Depan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input type="text" size="8" name="gelar_depan" value="{$dosen['GELAR_DEPAN']}" />
                <br/><label for="gelar_belakang">Gelar Belakang&nbsp;</label><input type="text" size="8" name="gelar_belakang" value="{$dosen['GELAR_BELAKANG']}" />
            </td>
        </tr>
        <tr>
            <td><span class="field">Tempat Lahir</span></td>
            <td>
                <select name="tempat_lahir">
                    {for $i=0 to $count_provinsi}
                        <optgroup label="{$data_kota[$i].nama}">
                            {foreach $data_kota[$i].kota as $data}
                                <option value="{$data.ID_KOTA}" {if $dosen['ID_KOTA_LAHIR']==$data.ID_KOTA}selected="true" {/if}>{$data.TIPE_DATI2} {$data.NM_KOTA}</option>
                            {/foreach}
                        </optgroup>
                    {/for}
                </select>
            </td>
            <td><span class="field">Email  </span></td>
            <td>
                <input type="text" value="{$dosen['EMAIL']}" name="email" />
            </td>
        </tr>
        <tr>
            <td><span class="field">Blog Dosen</span>
            <td>
                <input type="text" size="40" value="{$dosen['BLOG_PENGGUNA']}" name="blog" />
            </td>
            <td><span class="field">Contact Person  </span></td>
            <td>
                <input type="text" value="{$dosen['TELP']}" name="telp" /><br/>                
            </td>
        </tr>
        <tr>
            <td><span class="field">Tanggal Lahir  </span></td>
            <td><input id="tgl_lahir" size="40" name="tgl_lahir" type="text "value="{$dosen['TGL_LAHIR']}" /></td>
            <td><span class="field">Agama</span></td>
            <td>
                <select name="agama">
                    {foreach $data_agama as $data}
                        <option value="{$data['ID_AGAMA']}" {if $dosen['ID_AGAMA'] == $data['ID_AGAMA']} selected="true"{/if}>{$data['NM_AGAMA']}</option>
                    {/foreach}
                </select>
        </tr>
        <tr>
            <td><span class="field">Alamat Praktek/Kantor</span></td>
            <td>
                <textarea rows="3" cols="32" name="alamat_kantor" >{$dosen['ALAMAT_KANTOR']|upper}</textarea>
            </td>
            <td><span class="field">Alamat  </span></td>
            <td>
                <textarea rows="3" cols="15" name="alamat_rumah" >{$dosen['ALAMAT_RUMAH']|upper}</textarea>
            </td>
        </tr>
        <tr>
            <td><span class="field">Jenis Kelamin  </span></td>
            <td>
                <select name="kelamin">
                    <option value="1" {if $dosen['KELAMIN'] == 1} selected="true"{/if}>Laki-Laki</option>
                    <option value="2" {if $dosen['KELAMIN'] == 2} selected="true"{/if}>Perempuan</option>
                </select>                    
            </td>
        </tr>
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">B.PROFIL STUDI</th>
        </tr>
        <tr>
            <td><span class="field">Pend.Akhir</span></td>
            <td><span>{$dosen['NAMA_PENDIDIKAN_AKHIR']|upper}</span></td>
            <td><span class="field">Lulus</span></td>
            <td><span>{$dosen['TAHUN_LULUS_PENDIDIKAN']|upper}</span></td>
        </tr>
        <tr>
            <td><span class="field">Bidang Ahli</span></td>
            <td>
                <textarea rows="3" cols="32" name="bidang_keahlian" >{$dosen['BIDANG_KEAHLIAN_DOSEN']|upper}</textarea>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <span id="save" style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all">Save</span>
                <span id="cancel" style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all" onclick="$('#content').load('info_dosen.php')">Cancel</span>
                <input type="hidden" name="mode" value="update"/>
            </td>
        </tr>
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">C.PROFILE KEPEGAWAIAN</th>
        </tr>
        <tr>
            <td><span class="field">Pangkat/GOl</span></td>
            <td><span>{$dosen['NM_GOLONGAN']|upper}</span></td>
            <td><span class="field">Tmt Pangkat</span></td>
            <td><span>{$dosen['TMT_GOL']}</span></td>
        </tr>
        <tr>
            <td><span class="field">Jab.Fungs</span></td>
            <td><span>{$dosen['NM_JABATAN_FUNGSIONAL']|upper}</span></td>
            <td><span class="field">Tmt Fungs</span></td>
            <td><span>{$dosen['TMT_FUNG']}</span></td>
        </tr>
        <tr>
            <td><span class="field">Jabatan Struktural</span></td>
            <td><span>{$dosen['NM_JABATAN_STRUKTURAL']|upper}</span></td>
            <td><span class="field">Tmt Struk</span></td>
            <td><span>{$dosen['TMT_STRUK']}</span></td>
        </tr>
        <tr>
            <td><span class="field">Lat.Jabatan</span></td>
            <td><span></span></td>
            <td><span class="field">Tmt Pensiun</span></td>
            <td><span></span></td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript" >
                    $('#save').click(function() {
                        if ($(':disabled').size() == '4') {
                            $('#alert_info_dosen').dialog('open')
                        } else {
                            $('#form_info_dosen').submit();
                        }
                    });
                    $("#tgl_lahir").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        yearRange: '1900:2011',
                        dateFormat: 'dd/M/yy'
                    });
                    $('#form_info_dosen').validate({
                        rules: {
                            nama: {
                                required: true
                            },
                            blog: {
                                required: true
                            },
                            telp: {
                                required: true,
                                number: true,
                                minlength: 8
                            },
                            tgl_lahir: {
                                required: true,
                                date: true
                            },
                            email: {
                                required: true,
                                email: true
                            },
                            alamat_rumah: {
                                required: true,
                                minlength: 8
                            },
                            alamat_kantor: {
                                required: true,
                                minlength: 8
                            },
                            bidang_keahlian: {
                                required: true
                            }
                        }
                    });
                    $('#form_info_dosen').ajaxForm({
                        type: 'post',
                        url: $(this).attr('action'),
                        data: $(this).serialize(),
                        success: function(data) {
                            $('#content').html(data);
                            $("#dialog_info_dosen").dialog("open")
                        }
                    });

                    $("#dialog_info_dosen").dialog({
                        autoOpen: false,
                        position: "middle",
                        dragable: false,
                        modal: true,
                        resizable: false,
                        title: "Confirmation Message"
                    });
                    $("#alert_info_dosen").dialog({
                        autoOpen: false,
                        position: "middle",
                        dragable: false,
                        modal: true,
                        resizable: false,
                        title: "Alert Message"
                    });
    </script>
{/literal}
