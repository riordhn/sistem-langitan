<div class="center_title_bar">Add Penghargaan</div>
<form id="form_add_penghargaan" name="form_add_penghargaan" method="post"  action="penghargaan.php">
	<table class="ui-widget" width="50%">
		<tr class="ui-widget-header">
			<th colspan="2" class="header-coloumn"><h2>Add Penghargaan</h2></th>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Bentuk Penghargaan</span></td>
			<td>
				<textarea name="bentuk" class="required"></textarea>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Bidang</span></td>
			<td>
				<input type="text" class="required" name="bidang" />
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Pemberi</span></td>
			<td>
				<input type="text" class="required" name="pemberi" />
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Negara Pemberi</span></td>
			<td>
				<select name="negara">
                	{foreach $negara as $data}
                	<option value="{$data.ID_NEGARA}">{$data.NM_NEGARA}</option>
                    {/foreach}
                </select>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td><span>Tahun</span></td>
			<td>
				<input type="text" class="required number" name="tahun" />
			</td>
		</tr>
        <tr class="ui-widget-content">
			<td><span>Tingkat</span></td>
			<td>
				<select id="input_tingkat" name="tingkat">
	                <option value="Lokal">Lokal</option>
                    <option value="Nasional">Nasional</option>
                    <option value="Internasional">Internasional</option>
                </select>
			</td>
		</tr>
		<tr class="ui-widget-content">
			<td colspan="2" class="center">
				<input type="submit" value="Save" style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" />
				<a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="penghargaan.php">Cancel</a>
				<input type="hidden" value="add" name="mode"/>
			</td>
		</tr>

	</table>
</form>
{literal}
<script type="text/javascript">
$('#form_add_penghargaan').validate();
</script>
{/literal}