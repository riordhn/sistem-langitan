<div class="center_title_bar">Approval Penelitian - {if $is_departemen}Pembatalan Departemen{/if}{if $is_dekan}Pembatalan Dekan{/if}</div>

<h2>Apakah penelitian ini akan di batalkan ?</h2>


<form action="penelitian-approve.php" method="post">
{if $is_departemen}
    <input type="hidden" name="mode" value="reject-departemen" />
{/if}
{if $is_dekan}
    <input type="hidden" name="mode" value="reject-dekan" />
{/if}
<input type="hidden" name="id_penelitian" value="{$p.ID_PENELITIAN}" />
<table class="ui-widget">
    <tr class="ui-widget-header">
        <th colspan="2">Penelitian</th>
    </tr>
    <tr class="ui-widget-content">
        <td>Dosen Peneliti</td>
        <td>{$p.NM_PENGGUNA}</td>
    </tr>
    <tr class="ui-widget-content">
        <td>Judul Penelitian</td>
        <td>{$p.JUDUL}</td>
    </tr>
    <tr>
        <td colspan="2">
            <a href="penelitian-approve.php">Kembali</a>
            <input type="submit" value="Batalkan" />
        </td>
    </tr>
</table>
</form>

