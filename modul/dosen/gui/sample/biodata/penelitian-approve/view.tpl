<div class="center_title_bar">Approval Penelitian</div>

{if $result}<h2>{$result}</h2>{/if}

<table class="ui-widget">
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Judul Penelitian</th>
        <th>Tahun</th>
        <th>Pendaftar</th>
        <th style="width: 120px">Aksi</th>
    </tr>
    {foreach $penelitian_set as $p}
    <tr class="ui-widget-content">
        <td class="center">{$p@index + 1}</td>
        <td>{$p.JUDUL}</td>
        <td>{$p.TAHUN}</td>
        <td>{$p.NM_DOSEN}</td>
        <td>
            <a href="penelitian-approve.php?mode=detail&id_penelitian={$p.ID_PENELITIAN}">Lihat</a> |
            <a href="penelitian-approve.php?mode=approve&id_penelitian={$p.ID_PENELITIAN}">Setujui</a> |
            <a href="penelitian-approve.php?mode=reject&id_penelitian={$p.ID_PENELITIAN}">Batal</a>
        </td>
    </tr>
    {foreachelse}
    <tr class="ui-widget-content">
        <td colspan="6" class="center">Tidak ada penelitian yg di submit</td>
    </tr>
    {/foreach}
</table>