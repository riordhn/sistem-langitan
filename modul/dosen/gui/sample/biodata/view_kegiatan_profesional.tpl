<div class="center_title_bar">KEGIATAN PROFESIONAL</div> 
<table class="ui-widget" width="80%">
    <tr class="ui-widget-header">
        <th colspan="8" class="header-coloumn">Info Kegiatan Profesional</th>
    </tr>
    <tr>
        <th class="center">No</h3></th>
        <th>Kegiatan</h3></th>
        <th>Peran</h3></th>
        <th>Instansi</h3></th>
        <th>Tahun</h3></th>
        <th>Tingkat</h3></th>
        <th class="center">Operasi</h3></th>
    </tr>
    {foreach $data_kegiatan as $data}
        <tr>
            <td class="center">{$data@index+1}</td>
            <td>{$data.KEGIATAN_DOS_PROF}</td>
            <td>{$data.PERAN_DOS_PROF}</td>
            <td>{$data.INSTANSI_DOS_PROF}</td>
            <td>{$data.THN_DOS_PROF}</td>
            <td>{$data.TINGKAT_DOS_PROF}</td>
            <td class="center">
                <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="kegiatan_profesional.php?mode=edit&id_kegiatan={$data.ID_DOSEN_PROFESIONAL}">Edit</a>
            </td>
        </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="8">
            <a style="cursor: pointer;padding:3px;" class="ui-button ui-state-default ui-corner-all" href="kegiatan_profesional.php?mode=add">Tambah</a>
        </td>
    </tr>

</table>
{literal}
    <script type="text/javascript">

    </script>
{/literal}