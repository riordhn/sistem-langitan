<div class="center_title_bar">Granted SKS</div>

{literal}
    <style>
        .ui-widget tr td { padding: 2px; vertical-align: middle; }
        tr.back-yellow { background: #ff0; }
        tr.back-green { background: #8f8; }
        .ui-autocomplete {
            max-height: 200px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
        }
        /* IE 6 doesn't support max-height
        * we use height instead, but this forces the menu to always be this tall
        */
        * html .ui-autocomplete {
            height: 200px;
        }

    </style>
{/literal}

{if $status==1}
<form action="granted-sks.php" method="post">
<input type="hidden" name="action" value="updateproses" >
    <table class="ui-widget ui-widget-content" style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="font-size: 13pt">Parameter</th>
        </tr>
		{foreach name=test item="list" from=$view_update}
		<tr class="ui-widget-content">
            <td>Masukkan NIM</td>
            <td>
                <input type="text" size="30" name="nim" value="{$list.NIM_MHS}" id="cari" />
            </td>
        </tr>
		<tr class="ui-widget-content">
            <td>Jumlah SKS</td>
            <td>
                <input type="text" size="30" name="sks" value="{$list.SKS_MAX}" id="cari" />
            </td>
        </tr>
				<input type="hidden" name="id_granted" value="{$list.ID_LOG_GRANTED_SKS}" >
		{/foreach}
        <tr class="ui-widget-content">
            <td>Semester</td>
			<td>
               <select name="kdthnsmt">
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT}
	 		   {/foreach}
			   </select>
			</td>
        </tr>
		<tr class="ui-widget-content">
            <td></td>
            <td>
				<input type="submit" style="padding:5px" class="ui-button ui-state-default ui-corner-all" value="Update" />
            </td>
        </tr>
		
    </table>
</form>

{else}
<form action="granted-sks.php" method="post">
<input type="hidden" name="action" value="proses" >
    <table class="ui-widget ui-widget-content" style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="font-size: 13pt">Parameter</th>
        </tr>
		<tr class="ui-widget-content">
            <td>Masukkan NIM</td>
            <td>
                <input type="text" size="30" name="nim" id="cari" />
            </td>
        </tr>
		<tr class="ui-widget-content">
            <td>Jumlah SKS</td>
            <td>
                <input type="text" size="30" name="sks" id="cari" />
            </td>
        </tr>
		<tr class="ui-widget-content">
            <td>Catatan</td>
            <td>
                <textarea name="catatan" cols="40" class="required"></textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester</td>
			<td>
               <select name="kdthnsmt">
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT}
	 		   {/foreach}
			   </select>
			</td>
        </tr>
		<tr class="ui-widget-content">
            <td></td>
            <td>
				<input type="submit" style="padding:5px" class="ui-button ui-state-default ui-corner-all" value="Proses" />
            </td>
        </tr>
    </table>
</form>
{/if}

<p></p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
			 <td width="2%" bgcolor="#FFCC33"><center>No</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>NIM</center></td>
			 <td width="20%" bgcolor="#FFCC33"><center>Nama Mhs</center></td>
			 <td width="15%" bgcolor="#FFCC33"><center>Prodi</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>TTL SKS</center></td>
			 <td width="20%" bgcolor="#FFCC33"><center>Catatan</center></td>
		  </tr>
		  {foreach name=test item="list" from=$detail_transfer}
			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.NIM_MHS}</td>
			 <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.PRODI}</td>
			 <td><center><a href="granted-sks.php?action=update&id_granted={$list.ID_LOG_GRANTED_SKS}">{$list.SKS_MAX}</a></center></td>
             <td>{$list.CATATAN}</td>
			</tr>
		  {/foreach}
</table>
