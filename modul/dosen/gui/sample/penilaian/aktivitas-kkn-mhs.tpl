<div class="center_title_bar">Kuliah Kerja Nyata Mahasiswa</div>

{if isset($mhs)}
    <table class="ui-widget-content" style="width: 98%">
        <tr class="ui-widget-header">
            <th colspan="7" class="header-coloumn">MAHASISWA KKN FAKULTA {$fakultas.NM_FAKULTAS}</th>
        </tr>
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>PRODI</th>
            <th>IPK</th>
            <th>SKS</th>
            <th>SEMESTER</th>
        </tr>
        {$no = 1}
        {foreach $mhs as $data}
            <tr>
                <td>{$no++}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>{$data.NM_PROGRAM_STUDI}</td>
                <td>{$data.IPK}</td>
                <td>{$data.SKS}</td>
                <td>{$data.TAHUN_AJARAN} {$data.NM_SEMESTER}</td>
            </tr>
        {/foreach}
    </table>
{else}
    <table class="ui-widget-content" style="width: 50%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">REKAP AKTIVITAS KKN SEMESTER AKTIF</th>
        </tr>
        <tr class="ui-widget-header">
            <th>Fakultas</th>
            <th>Jumlah</th>
        </tr>
        {$jml = 0}
        {foreach $fakultas as $data}
            <tr>
                <td>{$data.NM_FAKULTAS|upper}</td>
                <td style="text-align:right"><a href="aktivitas-kkn-mhs.php?fak={$data.ID_FAKULTAS}">{if $data.JML_PESERTA == ''}0{else}{$data.JML_PESERTA}{/if}</a></td>
            </tr>
            {$jml = $data.JML_PESERTA + $jml}
        {/foreach}
        <tr>
            <td>JUMLAH</td>
            <td style="text-align:right"><a href="aktivitas-kkn-mhs.php?fak=">{$jml}</a></td>
        </tr>
    </table>
{/if}