<div class="center_title_bar">Kontrol KKN Mahasiswa</div>
{$str_jabatan}
{if $peranan==1}
    <span class="ui-state-highlight" style="padding: 15px;display: block;margin: 10px;font-style: italic;font-weight: bold;font-size: 1.2em">
        Anda Sebagai Koordinator Kabupaten/Kota {$kota_korkab.NM_KOTA}
    </span>
    <form action="kontrol-kkn.php" method="get">
        <table class="ui-widget-content" style="width: 70%">
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="4">Kontrol Kelompok KKN Kota {$kota_korkab.NM_KOTA}</th>
            </tr>
            <tr>
                <td>Kecamatan</td>
                <td>
                    <select id="kec" name="kecamatan">
                        <option value="">Pilih</option>
                        {foreach $data_kec as $kc}
                            <option value="{$kc.ID_KECAMATAN}">{$kc.NM_KECAMATAN}</option>
                        {/foreach}
                    </select>
                </td>
                <td>Kelurahan</td>
                <td>
                    <select id="kel" name="kelurahan">
                        <option value="">Pilih</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="center">
                    <input type="hidden" name="mode" value="tampil_kelompok"/>
                    <input type="submit" style="padding:5px" value="Tampilkan" class="ui-button ui-state-default ui-corner-all"/>
                </td>
            </tr>
        </table>
    </form>
    {if isset($data_kelompok)}                    
        <table class="ui-widget-content" style="width: 98%">
            <tr class="ui-widget-header">
                <th colspan="8" class="header-coloumn">Mahasiswa KKN Kelompok {$kelurahan.NM_KELURAHAN}</th>
            </tr>
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>NIM</th>
                <th>NAMA</th>
                <th>PROGRAM STUDI</th>
                <th>FAKULTAS</th>
                <th>JALUR</th>
                <th>NILAI ANGKA KKN</th>
                <th>NILAI HURUF KKN</th>
            </tr>
            {foreach $data_kelompok as $m}
                <tr>
                    <td>{$m@index+1}</td>
                    <td>{$m.NIM_MHS}</td>
                    <td>{$m.NM_PENGGUNA}</td>
                    <td>{$m.NM_JENJANG} {$m.NM_PROGRAM_STUDI}</td>
                    <td>{$m.NM_FAKULTAS}</td>
                    <td>{$m.NM_JALUR}</td>
                    <td class="center">{$m.NILAI_ANGKA}</td>
                    <td class="center">{$m.NILAI_HURUF}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="8" class="kosong">Data Masih Kosong</td>
                </tr>
            {/foreach}
        </table>
    {/if}
    {literal}
        <script type="text/javascript">
            $('#kec').change(function() {
                $.ajax({
                    url: '/modul/dosen/get-kelurahan-korkab.php',
                    type: 'post',
                    data: 'id_kecamatan=' + $(this).val(),
                    beforeSend: function() {
                        $('#kel').html('<div style="width: 100%;" align="center"><img src="{$base_url}js/loading.gif" /></div>');
                    },
                    success: function(data) {
                        $('#kel').html(data);
                    }
                })
            });
        </script>
    {/literal}
{else if $peranan==2}
    <span class="ui-state-highlight" style="padding: 15px;display: block;margin: 10px;font-style: italic;font-weight: bold;font-size: 1.2em">
        Anda Sebagai Koordinator Kecamatan {$kecamatan_korbing.NM_KECAMATAN}
    </span>
    {foreach $data_pembimbing as $dp}
        <span class="ui-state-highlight  ui-corner-all" style="padding: 10px;display: block;margin: 10px;font-style: italic;width: 50%;font-weight: bold">{$dp.NAMA_KELOMPOK}</span>
        <table class="ui-widget-content" style="width: 98%">
            <tr class="ui-widget-header">
                <th colspan="8" class="header-coloumn">Mahasiswa KKN {$dp.NAMA_KELOMPOK}</th>
            </tr>
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>NIM</th>
                <th>NAMA</th>
                <th>PROGRAM STUDI</th>
                <th>FAKULTAS</th>
                <th>JALUR</th>
                <th>NILAI ANGKA KKN</th>
                <th>NILAI HURUF KKN</th>
            </tr>
            {foreach $dp.MHS_KKN as $m}
                <tr>
                    <td>{$m@index+1}</td>
                    <td>{$m.NIM_MHS}</td>
                    <td>{$m.NM_PENGGUNA}</td>
                    <td>{$m.NM_JENJANG} {$m.NM_PROGRAM_STUDI}</td>
                    <td>{$m.NM_FAKULTAS}</td>
                    <td>{$m.NM_JALUR}</td>
                    <td class="center">{$m.NILAI_ANGKA}</td>
                    <td class="center">{$m.NILAI_HURUF}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="8" class="kosong">Data Masih Kosong</td>
                </tr>
            {/foreach}
        </table>
    {/foreach}
{else if $peranan==3}
    {$index_nilai=1}
    {foreach $data_dpl as $dp}
        <span class="ui-state-highlight  ui-corner-all" style="padding: 10px;display: block;margin: 10px;font-style: italic;width: 50%;font-weight: bold"> 
            Dosen Pembina {$dp.NAMA_KELOMPOK} {if $dp.JENIS_KKN==1}Reguler{else if $dp.JENIS_KKN==2}Tematik{else}Magang Khusus{/if}</span>
        <form method="post" action="kontrol-kkn.php">
            <table class="ui-widget-content" style="width: 98%">
                <tr class="ui-widget-header">
                    <th colspan="8" class="header-coloumn">Mahasiswa KKN {$dp.NAMA_KELOMPOK}</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>NO</th>
                    <th>NIM</th>
                    <th>NAMA</th>
                    <th>PROGRAM STUDI</th>
                    <th>FAKULTAS</th>
                    <th>JALUR</th>
                    <th>NILAI ANGKA KKN</th>
                    <th>NILAI HURUF KKN</th>
                </tr>
                {foreach $dp.MHS_KKN as $m}
                    <tr>
                        <td>{$m@index+1}</td>
                        <td>{$m.NIM_MHS}</td>
                        <td>{$m.NM_PENGGUNA}</td>
                        <td>{$m.NM_JENJANG} {$m.NM_PROGRAM_STUDI}</td>
                        <td>{$m.NM_FAKULTAS}</td>
                        <td>{$m.NM_JALUR}</td>
                        <td class="center">
                            {if $m.ID_PENGAMBILAN_MK!=''}
                                <input type="text" name="nilai{$index_nilai}"  size="4" maxlength="3" value="{$m.NILAI_ANGKA}"/>
                                <input type="hidden" name="pengambilan{$index_nilai}" value="{$m.ID_PENGAMBILAN_MK}"/>
                                {$index_nilai=$index_nilai+1}
                            {else}
                                <span class="kosong" style="font-size: 0.8em">KRS KKN Kosong</span>
                            {/if}
                        </td>
                        <td class="center">{$m.NILAI_HURUF}</td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="8" class="kosong">Data Masih Kosong</td>
                    </tr>
                {/foreach}
                <tr>
                    <td colspan="8" class="center">
                        <input type="hidden" name="mode" value="input_kkn"/>
                        <input type="hidden" name="jumlah_nilai" value="{$index_nilai}"/>
                        <input type="submit" style="padding:5px" value="Input Nilai" class="ui-button ui-state-default ui-corner-all"/>
                    </td>
                </tr>
            </table>
        </form>
    {/foreach}
{else if $peranan==4}
    <span class="ui-state-highlight" style="padding: 15px;display: block;margin: 10px;font-style: italic;font-weight: bold;font-size: 1.2em">
        Anda Sebagai Koordinator Kabupaten/Kota {$kota_korkab.NM_KOTA} dan Koordinator Kecamatan {$kecamatan_korbing.NM_KECAMATAN}
    </span>
    <form action="kontrol-kkn.php" method="get">
        <table class="ui-widget-content" style="width: 70%">
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="4">Kontrol Kelompok KKN Kota {$kota_korkab.NM_KOTA}</th>
            </tr>
            <tr>
                <td>Kecamatan</td>
                <td>
                    <select id="kec" name="kecamatan">
                        <option value="">Pilih</option>
                        {foreach $data_kec as $kc}
                            <option value="{$kc.ID_KECAMATAN}">{$kc.NM_KECAMATAN}</option>
                        {/foreach}
                    </select>
                </td>
                <td>Kelurahan</td>
                <td>
                    <select id="kel" name="kelurahan">
                        <option value="">Pilih</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="center">
                    <input type="hidden" name="mode" value="tampil_kelompok"/>
                    <input type="submit" style="padding:5px" value="Tampilkan" class="ui-button ui-state-default ui-corner-all"/>
                </td>
            </tr>
        </table>
    </form>
    {if isset($data_kelompok)}                    
        <table class="ui-widget-content" style="width: 98%">
            <tr class="ui-widget-header">
                <th colspan="8" class="header-coloumn">Mahasiswa KKN Kelompok {$kelurahan.NM_KELURAHAN}</th>
            </tr>
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>NIM</th>
                <th>NAMA</th>
                <th>PROGRAM STUDI</th>
                <th>FAKULTAS</th>
                <th>JALUR</th>
                <th>NILAI ANGKA KKN</th>
                <th>NILAI HURUF KKN</th>
            </tr>
            {foreach $data_kelompok as $m}
                <tr>
                    <td>{$m@index+1}</td>
                    <td>{$m.NIM_MHS}</td>
                    <td>{$m.NM_PENGGUNA}</td>
                    <td>{$m.NM_JENJANG} {$m.NM_PROGRAM_STUDI}</td>
                    <td>{$m.NM_FAKULTAS}</td>
                    <td>{$m.NM_JALUR}</td>
                    <td class="center">{$m.NILAI_ANGKA}</td>
                    <td class="center">{$m.NILAI_HURUF}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="8" class="kosong">Data Masih Kosong</td>
                </tr>
            {/foreach}
        </table>
    {/if}
    {literal}
        <script type="text/javascript">
            $('#kec').change(function() {
                $.ajax({
                    url: '/modul/dosen/get-kelurahan-korkab.php',
                    type: 'post',
                    data: 'id_kecamatan=' + $(this).val(),
                    beforeSend: function() {
                        $('#kel').html('<div style="width: 100%;" align="center"><img src="{$base_url}js/loading.gif" /></div>');
                    },
                    success: function(data) {
                        $('#kel').html(data);
                    }
                })
            });
        </script>
    {/literal}
{else if $peranan==5}    
    <span class="ui-state-highlight" style="padding: 15px;display: block;margin: 10px;font-style: italic;font-weight: bold;font-size: 1.2em">
        Anda Sebagai Koordinator Kabupaten/Kota {$kota_korkab.NM_KOTA} dan Koordinator Kecamatan {$kecamatan_korbing.NM_KECAMATAN} juga Dosen Pembina
        {foreach $kelurahan_dpl as $kd}
            Kelurahan {$kd.NM_KELURAHAN}
            {if count($kelurahan_dpl)!=$kd@index+1}
                ,
            {/if}
        {/foreach}
    </span>
    <form action="kontrol-kkn.php" method="get">
        <table class="ui-widget-content" style="width: 70%">
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="4">Kontrol Kelompok KKN Kota {$kota_korkab.NM_KOTA}</th>
            </tr>
            <tr>
                <td>Kecamatan</td>
                <td>
                    <select id="kec" name="kecamatan">
                        <option value="">Pilih</option>
                        {foreach $data_kec as $kc}
                            <option value="{$kc.ID_KECAMATAN}">{$kc.NM_KECAMATAN}</option>
                        {/foreach}
                    </select>
                </td>
                <td>Kelurahan</td>
                <td>
                    <select id="kel" name="kelurahan">
                        <option value="">Pilih</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="center">
                    <input type="hidden" name="mode" value="tampil_kelompok"/>
                    <input type="submit" style="padding:5px" value="Tampilkan" class="ui-button ui-state-default ui-corner-all"/>
                </td>
            </tr>
        </table>
    </form>
    {if isset($data_kelompok)}                    
        {$index_nilai}
        <form method="post" action="kontrol-kkn.php?{$smarty.server.QUERY_STRING}">
            <table class="ui-widget-content" style="width: 98%">
                <tr class="ui-widget-header">
                    <th colspan="8" class="header-coloumn">Mahasiswa KKN Kelompok {$kelurahan.NM_KELURAHAN}</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>NO</th>
                    <th>NIM</th>
                    <th>NAMA</th>
                    <th>PROGRAM STUDI</th>
                    <th>FAKULTAS</th>
                    <th>JALUR</th>
                    <th>NILAI ANGKA KKN</th>
                    <th>NILAI HURUF KKN</th>
                </tr>
                {foreach $data_kelompok as $m}
                    <tr>
                        <td>{$m@index+1}</td>
                        <td>{$m.NIM_MHS}</td>
                        <td>{$m.NM_PENGGUNA}</td>
                        <td>{$m.NM_JENJANG} {$m.NM_PROGRAM_STUDI}</td>
                        <td>{$m.NM_FAKULTAS}</td>
                        <td>{$m.NM_JALUR}</td>
                        <td class="center">
                            {if $status_dpl==1}
                                {if $m.ID_PENGAMBILAN_MK!=''}
                                    <input type="text" name="nilai{$index_nilai}"  size="4" maxlength="3" value="{$m.NILAI_ANGKA}"/>
                                    <input type="hidden" name="pengambilan{$index_nilai}" value="{$m.ID_PENGAMBILAN_MK}"/>
                                    {$index_nilai=$index_nilai+1}
                                {else}
                                    <span class="kosong" style="font-size: 0.8em">KRS KKN Kosong</span>
                                {/if}
                            {else}
                                {$m.NILAI_ANGKA}
                            {/if}
                        </td>
                        <td class="center">{$m.NILAI_HURUF}</td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="8" class="kosong">Data Masih Kosong</td>
                    </tr>
                {/foreach}
                {if $status_dpl==1}
                    <tr>
                        <td colspan="8" class="center">
                            <input type="hidden" name="mode" value="input_kkn"/>
                            <input type="hidden" name="jumlah_nilai" value="{$index_nilai}"/>
                            <input type="submit" style="padding:5px" value="Input Nilai" class="ui-button ui-state-default ui-corner-all"/>
                        </td>
                    </tr>
                {/if}
            </table>
        </form>
    {/if}
    {literal}
        <script type="text/javascript">
            $('#kec').change(function() {
                $.ajax({
                    url: '/modul/dosen/get-kelurahan-korkab.php',
                    type: 'post',
                    data: 'id_kecamatan=' + $(this).val(),
                    beforeSend: function() {
                        $('#kel').html('<div style="width: 100%;" align="center"><img src="{$base_url}js/loading.gif" /></div>');
                    },
                    success: function(data) {
                        $('#kel').html(data);
                    }
                })
            });
        </script>
    {/literal}
{else if $peranan==6}
    <span class="ui-state-highlight" style="padding: 15px;display: block;margin: 10px;font-style: italic;font-weight: bold;font-size: 1.2em">
        Anda Sebagai Koordinator Kecamatan {$kecamatan_korbing.NM_KECAMATAN} juga Dosen Pembina
        {foreach $kelurahan_dpl as $kd}
            Kelurahan {$kd.NM_KELURAHAN}
            {if count($kelurahan_dpl)!=$kd@index+1}
                ,
            {/if}
        {/foreach}
    </span>
    {$index_nilai=1}
    {foreach $data_pembimbing as $dp} 
        <span class="ui-state-highlight  ui-corner-all" style="padding: 10px;display: block;margin: 10px;font-style: italic;width: 50%;font-weight: bold">
            {$dp.NAMA_KELOMPOK} Jenis KKN {if $dp.JENIS_KKN==1}
                (Reguler)
            {else if $dp.JENIS_KKN==2}
                (Tematik)
            {else}(Magang Khusus)
            {/if}
        </span>
        <form method="post" action="kontrol-kkn.php">
            <table class="ui-widget-content" style="width: 98%">
                <tr class="ui-widget-header">
                    <th colspan="8" class="header-coloumn">Mahasiswa KKN {$dp.NAMA_KELOMPOK} <br/>
                        Jenis KKN {if $dp.JENIS_KKN==1}(Reguler){else if $dp.JENIS_KKN==2}(Tematik){else}(Magang Khusus){/if}</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>NO</th>
                    <th>NIM</th>
                    <th>NAMA</th>
                    <th>PROGRAM STUDI</th>
                    <th>FAKULTAS</th>
                    <th>JALUR</th>
                    <th>NILAI ANGKA KKN</th>
                    <th>NILAI HURUF KKN</th>
                </tr>
                {foreach $dp.MHS_KKN as $m}
                    <tr>
                        <td>{$m@index+1}</td>
                        <td>{$m.NIM_MHS}</td>
                        <td>{$m.NM_PENGGUNA}</td>
                        <td>{$m.NM_JENJANG} {$m.NM_PROGRAM_STUDI}</td>
                        <td>{$m.NM_FAKULTAS}</td>
                        <td>{$m.NM_JALUR}</td>
                        <td class="center">
                            {if $dp.RANGKAP>0}
                                {if $m.ID_PENGAMBILAN_MK!=''}
                                    <input type="text" name="nilai{$index_nilai}"  size="4" maxlength="3" value="{$m.NILAI_ANGKA}"/>
                                    <input type="hidden" name="pengambilan{$index_nilai}" value="{$m.ID_PENGAMBILAN_MK}"/>
                                    {$index_nilai=$index_nilai+1}
                                {else}
                                    <span class="kosong" style="font-size: 0.8em">KRS KKN Kosong</span>
                                {/if}
                            {else}
                                {$m.NILAI_ANGKA}
                            {/if}
                        </td>
                        <td class="center">{$m.NILAI_HURUF}</td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="8" class="kosong">Data Masih Kosong</td>
                    </tr>
                {/foreach}
                {if $dp.RANGKAP>0}
                    <tr>
                        <td colspan="8" class="center">
                            <input type="hidden" name="mode" value="input_kkn"/>
                            <input type="hidden" name="jumlah_nilai" value="{$index_nilai}"/>
                            <input type="submit" style="padding:5px" value="Input Nilai" class="ui-button ui-state-default ui-corner-all"/>
                        </td>
                    </tr>
                {/if}
            </table>
        </form>
    {/foreach}
{else if $peranan==7}    
    <span class="ui-state-highlight" style="padding: 15px;display: block;margin: 10px;font-style: italic;font-weight: bold;font-size: 1.2em">
        Anda Sebagai Koordinator Kabupaten/Kota {$kota_korkab.NM_KOTA} juga Dosen Pembina
        {foreach $kelurahan_dpl as $kd}
            Kelurahan {$kd.NM_KELURAHAN}
            {if count($kelurahan_dpl)!=$kd@index+1}
                ,
            {/if}
        {/foreach}
    </span>
    <form action="kontrol-kkn.php" method="get">
        <table class="ui-widget-content" style="width: 70%">
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="4">
                    Kontrol Kelompok KKN Kota {$kota_korkab.NM_KOTA}
                </th>
            </tr>
            <tr>
                <td>Kecamatan</td>
                <td>
                    <select id="kec" name="kecamatan">
                        <option value="">Pilih</option>
                        {foreach $data_kec as $kc}
                            <option value="{$kc.ID_KECAMATAN}">{$kc.NM_KECAMATAN}</option>
                        {/foreach}
                    </select>
                </td>
                <td>Kelurahan</td>
                <td>
                    <select id="kel" name="kelurahan">
                        <option value="">Pilih</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="center">
                    <input type="hidden" name="mode" value="tampil_kelompok"/>
                    <input type="submit" style="padding:5px" value="Tampilkan" class="ui-button ui-state-default ui-corner-all"/>
                </td>
            </tr>
        </table>
    </form>
    {if isset($data_kelompok)}              
        {$index_nilai=1}
        <form method="post" action="kontrol-kkn.php?{$smarty.server.QUERY_STRING}">
            <table class="ui-widget-content" style="width: 98%">
                <tr class="ui-widget-header">
                    <th colspan="8" class="header-coloumn">
                        Mahasiswa KKN Kelompok {$kelurahan.NM_KELURAHAN}
                        <br/>
            {$data_kelompok[0].NAMA_KELOMPOK} Jenis KKN {if $data_kelompok[0].JENIS_KKN==1}(Reguler){else if $data_kelompok[0].JENIS_KKN==2}(Tematik){else}(Magang Khusus){/if}
        </th>
    </tr>
    <tr class="ui-widget-header">
        <th>NO</th>
        <th>NIM</th>
        <th>NAMA</th>
        <th>PROGRAM STUDI</th>
        <th>FAKULTAS</th>
        <th>JALUR</th>
        <th>NILAI ANGKA KKN</th>
        <th>NILAI HURUF KKN</th>
    </tr>
    {foreach $data_kelompok as $m}
        <tr>
            <td>{$m@index+1}</td>
            <td>{$m.NIM_MHS}</td>
            <td>{$m.NM_PENGGUNA}</td>
            <td>{$m.NM_JENJANG} {$m.NM_PROGRAM_STUDI}</td>
            <td>{$m.NM_FAKULTAS}</td>
            <td>{$m.NM_JALUR}</td>
            <td class="center">
                {if $status_dpl==1}
                    {if $m.ID_PENGAMBILAN_MK!=''}
                        <input type="text" name="nilai{$index_nilai}"  size="4" maxlength="3" value="{$m.NILAI_ANGKA}"/>
                        <input type="hidden" name="pengambilan{$index_nilai}" value="{$m.ID_PENGAMBILAN_MK}"/>
                        {$index_nilai=$index_nilai+1}
                    {else}
                        <span class="kosong" style="font-size: 0.8em">KRS KKN Kosong</span>
                    {/if}
                {else}
                    {$m.NILAI_ANGKA}
                {/if}
            </td>
            <td class="center">{$m.NILAI_HURUF}</td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="8" class="kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}
    {if $status_dpl==1}
        <tr>
            <td colspan="8" class="center">
                <input type="hidden" name="mode" value="input_kkn"/>
                <input type="hidden" name="jumlah_nilai" value="{$index_nilai}"/>
                <input type="submit" style="padding:5px" value="Input Nilai" class="ui-button ui-state-default ui-corner-all"/>
            </td>
        </tr>
    {/if}
</table>
</form>
{/if}
{literal}
    <script type="text/javascript">
        $('#kec').change(function() {
            $.ajax({
                url: '/modul/dosen/get-kelurahan-korkab.php',
                type: 'post',
                data: 'id_kecamatan=' + $(this).val(),
                beforeSend: function() {
                    $('#kel').html('<div style="width: 100%;" align="center"><img src="{$base_url}js/loading.gif" /></div>');
                },
                success: function(data) {
                    $('#kel').html(data);
                }
            })
        });
    </script>
{/literal}
{/if}