<div class="center_title_bar">Pembukaan Penilaian</div>
<table style="width: 90%">
    <tr class="ui-widget-header">
        <th colspan="5" class="header-coloumn">Pembukaan/Penutupan Penilaian Per Semester</th>
    </tr>
    <tr>
        <th>NO</th>
        <th>Semester</th>
        <th>Status Aktif</th>
        <th>Status Pembukaan</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_semester_penilaian as $sem}
        <tr>
            <td>{$sem@index+1}</td>
            <td>{$sem.NM_SEMESTER} {$sem.TAHUN_AJARAN}</td>
            <td>
                {if $sem.STATUS_AKTIF_SEMESTER=='True'}
                    Aktif
                {else}
                    Tidak Aktif
                {/if}
            </td>
            <td>
                {if $sem.STATUS_BUKA==1}
                    Terbuka Semua
                {else if $sem.STATUS_BUKA<1&&$sem.STATUS_BUKA>0}
                    Terbuka Sebagian
                {else}
                    Tertutup Semua
                {/if}
            </td>
            <td class="center">
                {if ($sem.STATUS_BUKA<1&&$sem.STATUS_BUKA>0)||$sem.STATUS_BUKA==1} 
                    <span class="status_buka_semester ui-button ui-state-default ui-corner-all" name="id_semester" sem="{$sem.ID_SEMESTER}" style="padding:5px;"  title="Tutup Semua Penilaian Pada Semester Ini">
                        <span style="float: left;margin: 0px 2px" class="ui-icon ui-icon-close">&nbsp;</span> Tutup Semua 
                    </span>
                   
                {else if ($sem.STATUS_BUKA<1&&$sem.STATUS_BUKA>0)||$sem.STATUS_BUKA==0}
                    <span class="status_buka_semester ui-button ui-state-default ui-corner-all" name="id_semester" sem="{$sem.ID_SEMESTER}" style="padding:5px;"  title="Buka Semua Penilaian Pada Semester Ini">
                        <span style="float: left;margin: 0px 2px" class="ui-icon ui-icon-check">&nbsp;</span>Buka Semua
                    </span>                    
                {/if}
            </td>
        </tr>
    {/foreach}
</table>
<p></p>
<table>
    <tr class="ui-widget-header">
        <th colspan="2" class="header-coloumn">Pembukaan/Penutupan Penilaian Per Mata Kuliah</th>
    </tr>
    <tr>
        <td>Mata Kuliah</td>
        <td>
            <select id="select_mata_kuliah" name="select_mata_kuliah">
                <option value="">Pilih</option>
                {foreach $data_mata_kuliah as $data}
                    <option {if $data.STATUS_NILAI==0||$data.STATUS_NILAI==''}style="font-size: 0.93em;background-color: #ffcccc "{else}style="font-size: 0.9em;background-color: #99ff99"{/if} value="{$data.ID_MATA_KULIAH}" {if $smarty.post.id_mata_kuliah==$data.ID_MATA_KULIAH}selected="true"{/if}>{$data.NM_MATA_KULIAH} ({$data.KD_MATA_KULIAH}) Prodi {$data.PRODI} {if $data.STATUS_NILAI==0||$data.STATUS_NILAI==''}Belum Terisi{else}Sudah{/if}</option>
                {/foreach}
            </select>
        </td>
    </tr>
</table>
<div  id="table_pembukaan">
    {if $data_kelas_mk!=''}
        <table style="width: 100%">
            <tr class="ui-widget-header">
                <th colspan="6" class="header-coloumn">Pembukaan Penilaian Mata Kuliah </th>
            </tr>
            <tr>
                <th>NO</th>
                <th>NAMA MATA KULIAH</th>
                <th>KELAS</th>
                <th>SEMESTER</th>
                <th>PROGRAM STUDI</th>
                <th>STATUS PEMBUKAAN</th>
            </tr>
            {foreach $data_kelas_mk as $data}
                <tr>
                    <td>{$data@index+1}</td>
                    <td>{$data.NM_MATA_KULIAH}</td>
                    <td>Kelas {$data.NM_KELAS}</td>
                    <td>{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
                    <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                    <td class="center">
                        <input class="status_buka" type="checkbox" name="id_kelas_mk" value="{$data.ID_KELAS_MK}" {if $data.IS_DIBUKA==1}checked="true"{/if}/>{if $data.IS_DIBUKA==1} Di Buka {else} Di Tutup{/if}
                    </td>
                </tr>
            {/foreach}
        </table>
    {/if}
</div>
{literal}
    <script type="text/javascript">
        $('#select_mata_kuliah').change(function(){
            if($('#select_mata_kuliah').val()!=''){
                $.ajax({
                    url : '/modul/dosen/buka-nilai.php',
                    type : 'post',
                    data : 'mode=load_kelas&id_mata_kuliah='+$('#select_mata_kuliah').val(),
                    beforeSend : function(){
                        $('#table_pembukaan').html('<div style="width: 100%;" align="center"><img src="{$base_url}js/loading.gif" /></div>');
                    },
                    success :function(data){
                        $('#content').html(data);
                    }
                })
            }
        });
        $('.status_buka').click(function(){
            $.ajax({
                type:'post',
                url:'/modul/dosen/buka-nilai.php',
                data:'mode=buka_kelas&id_kelas_mk='+$(this).val()+'&id_mata_kuliah='+$('#select_mata_kuliah').val(),
                success:function(data){
                    $('#content').html(data);
                }    
            });
        });
        $('.status_buka_semester').click(function(){
            $.ajax({
                type:'post',
                url:'/modul/dosen/buka-nilai.php',
                data:'mode=buka_kelas_semester&id_semester='+$(this).attr('sem'),
                success:function(data){
                    $('#content').html(data);
                }    
            });
        });
    </script>
{/literal}