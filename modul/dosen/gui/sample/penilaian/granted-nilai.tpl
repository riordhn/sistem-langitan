<div class="center_title_bar">Granted Edit Nilai</div>

{literal}
    <style>
        .ui-widget tr td { padding: 2px; vertical-align: middle; }
        tr.back-yellow { background: #ff0; }
        tr.back-green { background: #8f8; }
        .ui-autocomplete {
            max-height: 200px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
        }
        /* IE 6 doesn't support max-height
        * we use height instead, but this forces the menu to always be this tall
        */
        * html .ui-autocomplete {
            height: 200px;
        }

    </style>
{/literal}

<form action="granted-nilai.php" method="post">
<input type="hidden" name="action" value="viewdetail" >
    <table class="ui-widget ui-widget-content" style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="font-size: 13pt">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Masukkan NIM</td>
            <td>
                <input type="text" size="30" name="nim" id="cari" />
				<input type="submit" style="padding:5px" class="ui-button ui-state-default ui-corner-all" value="Lihat" />
            </td>
        </tr>
    </table>
</form>


{if $action=='viewdetail'}
	{foreach $detail_mhs as $m}
	<table class="ui-widget" width="100%">
        <tbody>
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="3" style="font-size: 13pt">Data Mahasiswa</th>
            </tr>
            <tr class="ui-widget-content">
                <td width="10%">NIM</td><td width="20%">{$m.NIM_MHS}</td>
            </tr>
			<tr class="ui-widget-content">
                <td width="10%">Nama Mhs</td><td width="20%">{$m.NM_PENGGUNA}</td>
            </tr>
			<tr class="ui-widget-content">
                <td width="10%">Prodi</td><td width="20%">{$m.PRODI}</td>
            </tr>
	</table>
	{/foreach}
	<p></p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
			 <td width="2%" bgcolor="#FFCC33"><center>No</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>Kode MA</center></td>
			 <td width="20%" bgcolor="#FFCC33"><center>Nama MA</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>SKS</center></td>
             <td width="10%" bgcolor="#FFCC33"><center>Smt</center></td>
			 <td width="5%" bgcolor="#FFCC33"><center>Nilai</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Aksi</center></td>
		  </tr>
		  {foreach name=test item="list" from=$detail_nilai}
			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.KD_MATA_KULIAH}</td>
			 <td>{$list.NM_MATA_KULIAH}</td>
             <td>{$list.KREDIT_SEMESTER}</td>
             <td>{$list.SMT}</td>
			 <td>{$list.NILAI_HURUF}</td>
			 {if $list.STATUS==1 }
			 <td></td>
			 {else}
			 <td height="0" align="center"><input name="Button" type="button" class="button" onClick="location.href='#penilaian-edt_nilai!granted-nilai.php?action=proses&id_pmk={$list.ID_PENGAMBILAN_MK}&nim={$nim}';disableButtons()" onMouseOver="window.status='Click untuk Insert';return true" onMouseOut="window.status='GRANTED Nilai'" value="EDIT NILAI" /> </td> 
			 {/if}
			</tr>
		  {/foreach}
</table>
{/if}
