<div class="center_title_bar">Edit Nilai</div>
<form id="form_edit_nilai" name="form_edit_nilai" action="penilaian.php">
    <table>
        <tr><th colspan="2"><h2>Edit Nilai {$komponen_child[0]['komponen']}</h2></th></tr>
        {$index=1}
        {foreach $komponen_child as $nilai}
        <tr>
            <td>{$nilai['komponen']} {$nilai['urutan']}</td>
            <td>
                <input type="text" name="nilai{$index}" value="{$nilai['nilai']}" />
                <input type="hidden" name="id_nilai{$index++}" value="{$nilai['id_nilai']}" />
            </td>
        </tr>
        {/foreach}
        <tr>
            <td colspan="2">
                <a class="save">Save</a>
                <input type="hidden" name="save"/>
            </td>
        </tr>
    </table>
</form>
{literal}
<script type="text/javascript">
$('.save').click(function(){
        $('#form_edit_nilai').submit();   
})
$('#form_edit_nilai').ajaxForm({
    url : $(this).attr('action'),
    data : $(this).serialize(),
    type : 'post',
    success : function(data){
        $('#center_content').html(data);
    } 
})
    
    
    
</script>
{/literal}