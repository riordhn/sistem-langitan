<div class="center_title_bar">REKAPITULASI PENILAIAN</div> 
{if $punya_akses}
    {if $smarty.get.mode!=='detail'}
        <form id="form_semester" action="rekap-nilai.php" method="get">
            <table class="ui-widget-content">
                <tr>
                    <td>Pilih Semester</td>
                    <td>
                        <select name="semester" onchange="$('#form_semester').submit()">
                            {foreach $data_semester as $sem}
                                <option value="{$sem.ID_SEMESTER}" {if $smarty.get.semester==$sem.ID_SEMESTER}selected="true"{/if}>{$sem.NM_SEMESTER} {$sem.TAHUN_AJARAN}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
            </table>
            <input type="hidden" name="mode" value="tampil"/>
        </form>
    {/if}
    {if $smarty.get.mode=='tampil'}
        <table class="ui-widget" style="font-size: 1.1em;width: 90%">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn">Status Tampil Nilai Mata Kuliah Per Semester</th>
            </tr>
            {foreach $rekap_publish as $rp}
                <tr>
                    <td>
                        Proses Semua Nilai Mata Kuliah Pada Semester ini untuk Jenjang {$rp.NM_JENJANG}:<br/>
                        <b>Status Publish : 
                            {if $rp.DATA_PUBLISH['BLM_PUBLISH']>0&&$rp.DATA_PUBLISH['PUBLISH']>0}
                                Sudah Sebagian
                            {else if $rp.DATA_PUBLISH['BLM_PUBLISH']==0&&$rp.DATA_PUBLISH['PUBLISH']>0}
                                Sudah Semua
                            {else if $rp.DATA_PUBLISH['PUBLISH']==0}
                                Belum Semua
                            {/if}
                        </b>
                    </td>
                    <td class="center">
                        {if $rp.DATA_PUBLISH['BLM_PUBLISH']>0&&$rp.DATA_PUBLISH['PUBLISH']>0}
                            <form action="rekap-nilai.php?{$smarty.server.QUERY_STRING}" method="post">
                                <input type="hidden" name="mode" value="publish"/>
                                <input type="hidden" name="jenjang" value="{$rp.ID_JENJANG}"/>
                                <input type="hidden" name="status" value="1"/>
                                <input style="padding:5px" class="ui-button ui-state-default ui-corner-all"  type="submit" value="Tampilkan Semua"/>
                            </form>
                        {else if $rp.DATA_PUBLISH['BLM_PUBLISH']==0&&$rp.DATA_PUBLISH['PUBLISH']>0}
                            <form action="rekap-nilai.php?{$smarty.server.QUERY_STRING}" method="post">
                                <input type="hidden" name="mode" value="publish"/>
                                <input type="hidden" name="jenjang" value="{$rp.ID_JENJANG}"/>
                                <input type="hidden" name="status" value="0"/>
                                <input style="padding:5px" class="ui-button ui-state-default ui-corner-all"  type="submit" value="Jangan Tampilkan Semua"/>
                            </form>
                        {else if $rp.DATA_PUBLISH['PUBLISH']==0}
                            <form action="rekap-nilai.php?{$smarty.server.QUERY_STRING}" method="post">
                                <input type="hidden" name="mode" value="publish"/>
                                <input type="hidden" name="jenjang" value="{$rp.ID_JENJANG}"/>
                                <input type="hidden" name="status" value="1"/>
                                <input style="padding:5px" class="ui-button ui-state-default ui-corner-all"  type="submit" value="Tampilkan Semua"/>
                            </form>
                        {/if}
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="2" class="kosong">Data Kosong</td>
                </tr>
            {/foreach}            
        </table>
        <p></p>
        <!--Menu Khusus Fakultas FEB-->
        {if $id_fakultas==4}
            <table class="ui-widget" style="font-size: 1.1em;width: 90%">
                <tr class="ui-widget-header">
                    <th colspan="2" class="header-coloumn">Penutupan Nilai Untuk Mahasiswa D3 FEB dengan SKS TOTAL >100</th>
                </tr>
                <tr>
                    <td>
                        Tutup Semua Nilai Pada semester ini untuk mahasiswa terkait : <br/>
                        <b>Terdapat {count($mhs_d3_feb)} Mahasiswa dengan dengan SKS TOTAL >100</b>
                    </td>
                    <td class="center">
                        <form action="rekap-nilai.php?{$smarty.server.QUERY_STRING}" method="post">
                            <input type="hidden" name="mode" value="publish_d3"/>
                            <input type="hidden" name="jenjang" value="5"/>
                            <input type="hidden" name="status" value="0"/>
                            <input style="padding:5px" class="ui-button ui-state-default ui-corner-all"  type="submit" value="Tutup Semua Nilai"/>
                        </form>
                    </td>
                </tr>       
            </table>
        {/if}
        <div id="tabs" style="width: 100%">
            <ul>
                <li><a class="disable-ajax" href="#tabs-nilai-masuk">Nilai Sudah Masuk</a></li>
                <li><a class="disable-ajax" href="#tabs-nilai-belum-masuk">Nilai Belum Masuk</a></li>
            </ul>
            <div id="tabs-nilai-masuk">
                <div class="tab_content" style="padding:10px;border: 1px solid #4578a2;">
                    <table style="width: 98%">
                        <tr class="ui-widget-header">
                            <th class="header-coloumn" colspan="10">Monitoring Penilaian</th>
                        </tr>
                        <tr class="ui-widget-header">
                            <th>No</th>
                            <th>Kode</th>
                            <th>Nama Mata Ajar</th>
                            <th>SKS</th>
                            <th>KLS</th>
                            <th>PJMA</th>
                            <th>NOMER</th>
                            <th>PST</th>
                            <th>NIL MSK</th>
                            <th>STATUS</th>
                        </tr>
                        {foreach $data_nilai_masuk as $dnm}
                            <tr>
                                <td>{$dnm@index+1}</td>
                                <td>{$dnm.KD_MATA_KULIAH}</td>
                                <td><a href="rekap-nilai.php?mode=detail&kelas={$dnm.ID_KELAS_MK}">{$dnm.NM_MATA_KULIAH}</a></td>
                                <td>{$dnm.KREDIT_SEMESTER}</td>
                                <td>{$dnm.NAMA_KELAS}</td>
                                <td>{$dnm.PJMA}</td>
                                <td>{$dnm.MOBILE_DOSEN}</td>
                                <td>{$dnm.PST}</td>
                                <td>{$dnm.NILMASUK}</td>
                                <td>{$dnm.TERPUBLISH}</td>
                            </tr>
                        {foreachelse}
                            <tr>
                                <td colspan="10" class="kosong">Data Kosong</td>
                            </tr>
                        {/foreach}
                    </table>
                </div>
            </div>
            <div id="tabs-nilai-belum-masuk">
                <div class="tab_content" style="padding:10px;border: 1px solid #4578a2;">
                    <table style="width: 98%">
                        <tr class="ui-widget-header">
                            <th class="header-coloumn" colspan="10">Monitoring Penilaian</th>
                        </tr>
                        <tr class="ui-widget-header">
                            <th>No</th>
                            <th>Kode</th>
                            <th>Nama Mata Ajar</th>
                            <th>SKS</th>
                            <th>KLS</th>
                            <th>PJMA</th>
                            <th>NOMER</th>
                            <th>PESERTA</th>
                            <th>NIL BLM MSK</th>
                            <th>STATUS</th>
                        </tr>
                        {foreach $data_nilai_belum_masuk as $dnbm}
                            <tr>
                                <td>{$dnbm@index+1}</td>
                                <td>{$dnbm.KD_MATA_KULIAH}</td>
                                <td>{$dnbm.NM_MATA_KULIAH}</td>
                                <td>{$dnbm.KREDIT_SEMESTER}</td>
                                <td>{$dnbm.NAMA_KELAS}</td>
                                <td>{$dnbm.PJMA}</td>
                                <td>{$dnbm.MOBILE_DOSEN}</td>
                                <td>{$dnbm.PST}</td>
                                <td>{$dnbm.NILMASUK}</td>
                                <td>{$dnbm.TERPUBLISH}</td>
                            </tr>
                        {foreachelse}
                            <tr>
                                <td colspan="10" class="kosong">Data Kosong</td>
                            </tr>
                        {/foreach}
                    </table>
                </div>
            </div>
        </div>
    {else if $smarty.get.mode=='detail'}
        <a style="padding:5px" class="ui-button ui-state-default ui-corner-all disable-ajax" onclick="history.back(-1)">Kembali</a>
        <table style="width:80%">
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="3">Daftar Pengajar Mata Kuliah {$data_kelas_mk_detail.NM_MATA_KULIAH} {$data_kelas_mk_detail.NM_KELAS}</th>
            </tr>
            <tr>
                <th>No</th>
                <th>Pengajar</th>
                <th>Status</th>
            </tr>
            {foreach $data_pengajar_mk as $data}
                <tr>
                    <td>{$data@index+1}</td>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>
                        {if $data.PJMK_PENGAMPU_MK==1}
                            PJMK
                        {else}
                            Anggota
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </table>
        <table  width="100%">
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="{$count_data_komponen_mk+6}">Data Nilai Mahasiswa {$data_kelas_mk_detail.NM_MATA_KULIAH} {$data_kelas_mk_detail.NM_KELAS}</th>
            </tr>
            <tr>
                <td class="center" colspan="{$count_data_komponen_mk+6}">
                    <a style="padding:5px" class="disable-ajax ui-button ui-state-default ui-corner-all" href="/modul/dosen/cetak-nilai.php?id_kelas_mk={$id_kelas_mk}&semester={$data_kelas_mk_detail.ID_SEMESTER}" target="_blank">Cetak Nilai</a>
                    <span style="padding:5px" class="ui-button ui-state-default ui-corner-all" onclick="window.location.reload()">Reload</span>
                </td>
            </tr>
            <tr>
                <td class="header-coloumn" colspan="{$count_data_komponen_mk+6}">
                    <p style="text-transform: uppercase">
                        Baris Berwarna <span style="color: lightcoral">Merah</span> = Status Mahasiswa Cekal
                    </p>
                </td>
            </tr>
            <tr>
                <th>
                    No
                </th>
                <th class="center">
                    NIM
                </th>
                <th class="center">
                    Nama
                </th>
                {foreach $data_komponen_mk as $data}
                    <th class="center">
                        {$data.NM_KOMPONEN_MK} <br/>({$data.PERSENTASE_KOMPONEN_MK}%)
                    </th>
                {/foreach}
                <th class="center">
                    Nilai Angka Akhir
                </th>
                <th class="center">
                    Nilai Huruf Akhir
                </th>
                <th class="center">
                    Tampilkan Ke Mahasiswa
                </th>
            </tr>
            {$index_nilai=1}
            {foreach $data_mahasiswa as $mhs}
                <tr> 
                    <td>{$mhs@index+1}</td>
                    <td>{$mhs.nim}</td>
                    <td>{$mhs.nama|upper}</td>
                    {foreach $mhs.data_nilai as $nilai}
                        <td {if ($mhs.status_cekal==0||$mhs.status_cekal==2)&&preg_match("/[U|u][A|a][S|s]/",$nilai.NM_KOMPONEN_MK)}style="background-color: lightcoral" {/if} class="center">
                            {$nilai.BESAR_NILAI_MK}
                        </td>
                    {/foreach}
                    <td class="center">{if $mhs.nilai_angka_akhir!=''}{$mhs.nilai_angka_akhir}{else}Kosong{/if}</td>
                    <td class="center">{if $mhs.nilai_huruf_akhir!=''}{$mhs.nilai_huruf_akhir}{else}Kosong{/if}</td>
                    <td class="center">{if $mhs.flagnilai==0} Belum Tampil{else}Tampil{/if}</td>
                </tr>
            {/foreach}
        </table>
    {/if}
{else}
    {$status_message}
{/if}
{literal}
    <script type="text/javascript">
        $( "#tabs" ).tabs();
    </script>
{/literal}
