<div class="center_title_bar">Laporan Akademik per Semester Aktif {$tahun}-{$smt}</div>

{literal}
    <style>
        .ui-widget tr td { padding: 2px; vertical-align: middle; }
        tr.back-yellow { background: #ff0; }
        tr.back-green { background: #8f8; }
        .ui-autocomplete {
            max-height: 200px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
        }
        /* IE 6 doesn't support max-height
        * we use height instead, but this forces the menu to always be this tall
        */
        * html .ui-autocomplete {
            height: 200px;
        }

    </style>
{/literal}
<form action="laporan-akademik.php" method="post">
<input type="hidden" name="action" value="tampil" > 

	Cek Kelengkapan Akademik :
	<select name="filter">
    <option value='1'>Kurikulum</option>
	<option value='2'>Histori Nilai</option>
	<option value='3'>Jadwal Kuliah</option>
	<option value='4'>Krs</option>
	</select>
	Jenjang :
	<select name="jenjang">
    <option value=''>-- ALL --</option>
	{foreach item="jjg" from=$jenjang}
    {html_options  values=$jjg.NM_JENJANG output=$jjg.NM_JENJANG}
	{/foreach}
	</select>
	{if $status=='Univeristas'}
		Fakultas :
		<select name="fakultas">
		<option value=''>-- ALL --</option>
		{foreach item="fak" from=$fakultas}
		{html_options  values=$fak.NM_FAKULTAS output=$fak.NM_FAKULTAS}
		{/foreach}
		</select>
	{/if}
	<input type="submit" name="View" value="View">

</form>
<div> Rekapitulasi {$status}</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
			 <td width="2%" bgcolor="#FFCC33"><center>No</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Jenjang</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Total prodi</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Kurikulum</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Histori Nilai</center></td>
             <td width="10%" bgcolor="#FFCC33"><center>Clompeted(Kurikulum,histori-nilai)</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Siap KRS</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Telah KRS</center></td>
		  </tr>
		  {foreach name=test item="list1" from=$prosen_kur_univ}
			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
			 <td><a href="laporan-akademik.php?action=detail&jen={$list1.NM_JENJANG}">{$list1.NM_JENJANG}</a></td>
			 <td>{$list1.TTLJENJANG}</td>
			 <td>{$list1.PROSEN1}%</td>
			 <td>{$list1.PROSEN2}%</td>
             <td>{$list1.PROSEN}%</td>
			 <td>{$list1.PROSEN_SIAP}%</td>
			 <td>{$list1.PROSEN_KRS}%</td>
			</tr>
		  {/foreach}
</table>
<p></p>
<br height="0" align="center"><input name="Button" type="button" class="button" onClick="location.href='#penilaian-laporan_aka!laporan-akademik.php?action=viewperfakultas';disableButtons()" onMouseOver="window.status='Click untuk Insert';return true" onMouseOut="window.status='Rekapitulasi Fakultas'" value="Rekapitulasi Fakultas" /> </br> 

{if $action=='viewperfakultas'}
<p></p>
<div> Rekapitulasi Per Fakultas</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
			 <td width="2%" bgcolor="#FFCC33"><center>No</center></td>
             <td width="20%" bgcolor="#FFCC33"><center>Fakultas</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Jenjang</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Total prodi</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Kurikulum</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Histori Nilai</center></td>
             <td width="10%" bgcolor="#FFCC33"><center>Clompeted(Kurikulum,histori-nilai)</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Siap KRS</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Telah KRS</center></td>
		  </tr>
		  {foreach name=test item="list1" from=$prosen_kur}
			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
			 <td><a href="laporan-akademik.php?action=detailfak&nm_fak={$list1.NM_FAKULTAS}">{$list1.NM_FAKULTAS}</a></td>
			 <td>{$list1.NM_JENJANG}</td>
			 <td>{$list1.TTLJENJANG}</td>
			 <td>{$list1.PROSEN1}%</td>
			 <td>{$list1.PROSEN2}%</td>
             <td>{$list1.PROSEN}%</td>
			 <td>{$list1.PROSEN_SIAP}%</td>
			 <td>{$list1.PROSEN_KRS}%</td>
			</tr>
		  {/foreach}
</table>
{/if}

{if $action=='detail'}
<p></p>
<div> Detail Per Prodi</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
			 <td width="2%" bgcolor="#FFCC33"><center>No</center></td>
             <td width="20%" bgcolor="#FFCC33"><center>Fakultas</center></td>
			 <td width="20%" bgcolor="#FFCC33"><center>Prodi</center></td>
             <td width="8%" bgcolor="#FFCC33"><center>KRS(record)</center></td>
             <td width="8%" bgcolor="#FFCC33"><center>Histori Nilai(record)</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Kurikulum(mk)</center></td>
			 <td width="15%" bgcolor="#FFCC33"><center>(Hist. Nilai + Kurikulum)</center></td>
			 <td width="15%" bgcolor="#FFCC33"><center>Status KRS</center></td>
		  </tr>
		  {foreach name=test item="list1" from=$lap_kur}
			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list1.NM_FAKULTAS}</td>
			 <td>{$list1.PRODI}</td>
             <td>{$list1.KRS}</td>
             <td>{$list1.TTLNIL}</td>
			 <td>{$list1.KUR}</td>	 
			 <td>{$list1.KET1}</td>
			 <td>{$list1.KET}</td>
			</tr>
		  {/foreach}
</table>
{/if}

{if $action=='tampil'}
<p></p>
<div> Kelengkapan Data {if ($filter==1) } Kurikulum {elseif ($filter==2)} Historis Nilai {elseif ($filter==3)} Jadwal Kuliah  {else} KRS {/if}</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
			 <td width="2%" bgcolor="#FFCC33"><center>No</center></td>
             <td width="20%" bgcolor="#FFCC33"><center>Fakultas</center></td>
			 <td width="20%" bgcolor="#FFCC33"><center>Prodi</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Jumlah (record)</center></td>
             <td width="10%" bgcolor="#FFCC33"><center>Keterangan</center></td>
		  </tr>
		  {foreach name=test item="list1" from=$lap_kur}
			{if (($filter==1 and {$list1.TTLKUR} < 1) or ($filter==2 and {$list1.TTLNIL} < 1) or ($filter==3 and {$list1.TTLKELAS} < 1) or ($filter==4 and {$list1.KRS} < 10))}  
				<tr bgcolor="yellow">
				{else}
				<tr>
			{/if}
						
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list1.NM_FAKULTAS}</td>
			 <td>{$list1.PRODI}</td>
			 <td>
			 {if ($filter==1) } {$list1.KUR} {elseif ($filter==2)} {$list1.TTLNIL} {elseif ($filter==3)} {$list1.TTLKELAS}  {else} {$list1.KRS} {/if}
			 </td>
             <td>{if (($filter==1 and {$list1.TTLKUR} > 1) or ($filter==2 and {$list1.TTLNIL} > 1) or ($filter==3 and {$list1.TTLKELAS} > 1) or ($filter==4 and {$list1.KRS} > 10))}  
				Sudah Masuk
				{else}
				Belum Masuk
			{/if}
			 </td>
			</tr>
		  {/foreach}
</table>
{/if}
