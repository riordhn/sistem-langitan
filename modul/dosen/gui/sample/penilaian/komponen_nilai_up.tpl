{if $smarty.get.mode=='tambah'}
    <form id="form_tambah_komponen" method="post" action="penilaian-khusus.php?id_kelas={$smarty.get.id_kelas}_{$smarty.get.id_semester}&mode=load_mahasiswa">
        <table class="ui-widget" width="90%">
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="2">Tambah Komponen Nilai Semester Ujian Perbaikan</th>
            </tr>
            <tr class="ui-widget-content">
                <td>Tipe Komponen</td>
                <td>
                    <select id="tipe_komponen" class="required">
                        <option value="">Pilih Tipe Komponen</option>
                        <option value="1">UAS/UTS</option>
                        <option value="2">Komponen Lain</option>
                    </select>
                </td>
            </tr>
            <tr id="uts_uas" style="display: none" class="ui-widget-content">
                <td>Nama Komponen</td>
                <td>
                    <select name="nama_utama">
                        <option value="UTS">UTS</option>
                        <option value="UAS">UAS</option>
                    </select>
                </td>
            </tr>
            <tr id="lain" style="display: none" class="ui-widget-content">
                <td>Nama Komponen</td>
                <td><input type="text" id="nama" name="nama" class="required"/></td>
            </tr>
            <tr class="ui-widget-content">
                <td>Persentase Komponen</td>
                <td>
                    <input type="text" id="persentase" remote="/modul/dosen/cek-total-komponen-up.php?id_kelas_mk={$smarty.get.id_kelas}" name="persentase" class="required number" />
                    <label class="error" style="display: none" for="persentase">Tidak Boleh Kosong / Total Komponen Melebihi Maksimal</label>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td class="center" colspan="2">
                    <input type="hidden" name="mode" value="tambah-komponen-up"/>
                    <input type="hidden" name="id_kelas" value="{$smarty.get.id_kelas}"/>
                    <input type="submit" class="ui-button ui-state-default ui-corner-all" style="padding:3px" value="Tambah"/>
                    <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_tambah_komponen').dialog('close')">Cancel</span>
                </td>
            </tr>
        </table>
    </form>
    {literal}
        <script type="text/javascript">
                        $('#form_tambah_komponen').validate();
                        $('#form_tambah_komponen').submit(function() {
                            if ($('#form_tambah_komponen').valid()) {
                                $('#dialog_tambah_komponen').dialog('close')
                            }
                        });
                        $('#tipe_komponen').change(function() {
                            if ($(this).val() == 1) {
                                $('#lain').hide();
                                $('#uts_uas').show();
                            } else if ($(this).val() == 2) {
                                $('#uts_uas').hide();
                                $('#lain').show();
                            } else {
                                $('#uts_uas').hide();
                                $('#lain').hide();
                            }
                        })
        </script>
    {/literal}
{else if $smarty.get.mode=='edit'}
    <form id="form_edit_komponen" method="post" action="penilaian-khusus.php?id_kelas={$smarty.get.id_kelas}_{$smarty.get.id_semester}&mode=load_mahasiswa">
        <table class="ui-widget" width="90%">
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="2">Update Komponen Nilai Semester Ujian Perbaikan</th>
            </tr>
            <tr class="ui-widget-content">
                <td>Nama Komponen</td>
                <td><input type="text" id="nama" name="nama" value="{$data_komponen_up['NM_KOMPONEN_UP']}" class="required"/></td>
            </tr>
            <tr class="ui-widget-content">
                <td>Persentase Komponen</td>
                <td>
                    <input type="text" id="persentase" remote="/modul/dosen/cek-total-komponen-up.php?id_kelas_mk={$smarty.get.id_kelas}&id_komponen={$smarty.get.id_komponen}"  name="persentase" value="{$data_komponen_up['PROSENTASE_KOMPONEN']}" class="required number" />
                    <label class="error" style="display: none" for="persentase">Tidak Boleh Kosong / Total Komponen Melebihi Maksimal</label>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td class="center" colspan="2">
                    <input type="hidden" name="id_komponen" value="{$data_komponen_up['ID_KOMPONEN_UP']}" />
                    <input type="hidden" name="mode" value="edit-komponen-up"/>
                    <input type="submit" class="ui-button ui-state-default ui-corner-all" style="padding:3px" value="Update"/>
                    <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_edit_komponen').dialog('close')">Cancel</span>
                </td>
            </tr>
        </table>
    </form>
    {literal}
        <script type="text/javascript">
                        $('#form_edit_komponen').validate();
                        $('#form_edit_komponen').submit(function() {
                            if ($('#form_edit_komponen').validate())
                                $('#dialog_edit_komponen').dialog('close')
                        })
        </script>
    {/literal}
{else if $smarty.get.mode=='delete'}
    <form id="form_delete_komponen" method="post" action="penilaian-khusus.php?id_kelas={$smarty.get.id_kelas}_{$smarty.get.id_semester}&mode=load_mahasiswa">
        <table class="ui-widget" width="370px">
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="2">Delete Komponen Nilai Semester Ujian Perbaikan</th>
            </tr>
            <tr class="ui-widget-content">
                <td class="center">
                    <p>Apakah anda yakin untuk menghapus komponen nilai {$data_komponen_up['NM_KOMPONEN_UP']} ?</p>
                    <input type="hidden" name="id_komponen" value="{$data_komponen_up['ID_KOMPONEN_UP']}" />
                    <input type="hidden" name="mode" value="delete-komponen-up"/>
                    <input type="hidden" name="id_kelas" value="{$smarty.get.id_kelas}"/>
                    <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#form_delete_komponen').submit();
                                    $('#dialog_delete_komponen').dialog('close');">Ya</span>
                    <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_delete_komponen').dialog('close')">Cancel</span>
                </td>
            </tr>
        </table>
    </form>
{/if}