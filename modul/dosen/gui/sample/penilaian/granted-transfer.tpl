<div class="center_title_bar">Granted Nilai Transfer / Insert Nilai</div>

{literal}
    <style>
        .ui-widget tr td { padding: 2px; vertical-align: middle; }
        tr.back-yellow { background: #ff0; }
        tr.back-green { background: #8f8; }
        .ui-autocomplete {
            max-height: 200px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
        }
        /* IE 6 doesn't support max-height
        * we use height instead, but this forces the menu to always be this tall
        */
        * html .ui-autocomplete {
            height: 200px;
        }

    </style>
{/literal}

<form action="granted-transfer.php" method="post">
<input type="hidden" name="action" value="proses" >
    <table class="ui-widget ui-widget-content" style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="font-size: 13pt">Parameter</th>
        </tr>
		<tr class="ui-widget-content">
            <td>Masukkan NIM</td>
            <td>
                <input type="text" size="30" name="nim" id="cari" />
            </td>
        </tr>
		<tr class="ui-widget-content">
            <td>Jumlah MK Diakui</td>
            <td>
                <input type="text" size="30" name="mk" id="cari" />
            </td>
        </tr>
		<tr class="ui-widget-content">
            <td>Catatan</td>
            <td>
                <textarea name="catatan" cols="40" class="required"></textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester</td>
			<td>
               <select name="kdthnsmt">
    		   <option value=''>------</option>
	 		   {foreach item="list_smt" from=$T_THNSMT}
    		   {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT}
	 		   {/foreach}
			   </select>
			</td>
        </tr>
		<tr class="ui-widget-content">
            <td></td>
            <td>
				<input type="submit" style="padding:5px" class="ui-button ui-state-default ui-corner-all" value="Proses" />
            </td>
        </tr>
    </table>
</form>

<p></p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
			 <td width="2%" bgcolor="#FFCC33"><center>No</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>NIM</center></td>
			 <td width="20%" bgcolor="#FFCC33"><center>Nama Mhs</center></td>
			 <td width="15%" bgcolor="#FFCC33"><center>Prodi</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>TTL MK Diakui</center></td>
			 <td width="5%" bgcolor="#FFCC33"><center>TTL MK DiEntry</center></td>
			 <td width="5%" bgcolor="#FFCC33"><center>Status</center></td>
		  </tr>
		  {foreach name=test item="list" from=$detail_transfer}
			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.NIM_MHS}</td>
			 <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.PRODI}</td>
			 <td>{$list.TOTAL_MATA_KULIAH}</td>
             <td>{$list.TTLINSERT}</td>
			 <td>{$list.STATUS}</td>
			</tr>
		  {/foreach}
</table>
