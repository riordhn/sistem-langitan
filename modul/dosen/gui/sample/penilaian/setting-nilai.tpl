<div class="center_title_bar">Input Nilai</div>
{$informasi_menu}
<table class="ui-widget">
    <tr class="ui-widget-content" width="100%">
        <td style="vertical-align:middle"><span>Mata Kuliah</span></td>
        <td>
            <select id="select_kelas_mk">
                <option>Pilih</option>
                {foreach $data_kelas_mk as $data}
                    <optgroup label="{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})">
                        {foreach $data.DATA_KELAS_MK as $kelas}
                            <option value="{$kelas.ID_KELAS_MK}" 
                                    {if $kelas.ID_KELAS_MK==$smarty.post.id_kelas_mk}
                                        selected="true"
                                    {/if}
                                    >{$kelas.NM_MATA_KULIAH} ({$kelas.KD_MATA_KULIAH}) Kelas {$kelas.NM_KELAS} <br/> Prodi  {$kelas.NM_PROGRAM_STUDI}
                        </option>
                    {/foreach}
                {/foreach}
            </select>
        </td>
    </tr>
</table>
<div id="table_penilaian">
    {if isset($data_mahasiswa)}
        <form action="setting-nilai.php" method="post">
            <table class="ui-widget-content" style="width: 90%">
                <tr class="ui-widget-header">
                    <th class="header-coloumn" colspan="6">Setting Penilaian Perbaikan</th>
                </tr>
                <tr>
                    <th>NO</th>
                    <th>NIM</th>
                    <th>NAMA</th>
                    <th>Nilai</th>
                    <th>Status Remidi</th>
                    <th>Status Ujian Perbaikan</th>
                </tr>
                {foreach $data_mahasiswa as $data}
                    <tr>
                        <td>{$data@index+1}</td>
                        <td>{$data.nim}</td>
                        <td>{$data.nama}</td>
                        <td>{$data.nilai} </td>
                        <td>
                            {if $data.status_remidi>0}
                                <input type="checkbox" checked="true" value="{$data.id_mhs}" name="status_remidi" class="remidi"/>
                                <label for="status_remidi">Sudah Terdaftar</label>
                            {else}
                                <input type="checkbox" value="{$data.id_mhs}" name="status_remidi" class="remidi"/>
                                <label for="status_remidi">Belum Terdaftar</label>
                            {/if}
                        </td>
                        <td>
                            {if $data.status_up>0}
                                <input type="checkbox" checked="true" value="{$data.id_mhs}" name="status_up" class="up"/>
                                <label for="status_up">Sudah Terdaftar</label>
                            {else}
                                <input type="checkbox" value="{$data.id_mhs}" name="status_up" class="up"/>
                                <label for="status_up">Belum Terdaftar</label>
                            {/if}
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="6" class="kosong center">Data Kosong / Semester Sudah Berakhir / Status Pembukaan Kelas (Di Tutup)</td>
                    </tr>
                {/foreach}
            </table>
        </form>
    {/if}
</div>
{literal}
    <script type="text/javascript">        
        $('#select_kelas_mk').change(function(){
            $.ajax({
                url : '/modul/dosen/setting-nilai.php',
                type : 'post',
                data : 'mode=load_mahasiswa&id_kelas_mk='+$('#select_kelas_mk').val(),
                beforeSend : function(){
                    $('#table_penilaian').html('<div style="width: 100%;" align="center"><img src="{$base_url}js/loading.gif" /></div>');
                },
                success :function(data){
                    $('#content').html(data);
                }
            })
        });
        $('.remidi').click(function(){
            $.ajax({
                type:'post',
                url:'/modul/dosen/setting-nilai.php',
                data:'mode=load_mahasiswa&status=remidi&id_mhs='+$(this).val()+'&id_kelas_mk='+$('#select_kelas_mk').val(),
                success:function(data){
                    $('#content').html(data);
                }    
            });
        });
        $('.up').click(function(){
            $.ajax({
                type:'post',
                url:'/modul/dosen/setting-nilai.php',
                data:'mode=load_mahasiswa&status=up&id_mhs='+$(this).val()+'&id_kelas_mk='+$('#select_kelas_mk').val(),
                success:function(data){
                    $('#content').html(data);
                }    
            });
        });
    </script>
{/literal}