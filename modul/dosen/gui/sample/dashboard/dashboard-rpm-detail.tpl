<div class="center_title_bar">Rekap PM SNMPTN - Detail Peserta</div>

<style>
#myTable tr th { border: 1px solid #A6C9E2; padding: 2px; }
#myTable tr td { border: 1px solid #A6C9E2; padding: 2px; }
</style>
<a href="dashboard-rpm.php">Kembali</a>
<table id="myTable">
    <tr>
        <th colspan="3">Detail Peserta</th>
    </tr>
    <tr>
        <td rowspan="12">
            <img src="/img/foto/ujian-snmptn/2012/{$cmb.NO_UJIAN}.jpg" width="200px" />
        </td>
        <td class="tebal">No Ujian</td>
        <td>{$cmb.NO_UJIAN}</td>
    </tr>
    <tr>
        <td class="tebal">Nama</td>
        <td>{$cmb.NM_C_MHS}</td>
    </tr>
    <tr>
        <td class="tebal">Pilihan 1</td>
        <td>{$cmb.NM_PROGRAM_STUDI1}</td>
    </tr>
    <tr>
        <td class="tebal">Pilihan 2</td>
        <td>{$cmb.NM_PROGRAM_STUDI2}</td>
    </tr>
    <tr>
        <td class="tebal">Sekolah Asal</td>
        <td>{$cmb.NM_SEKOLAH}</td>
    </tr>
    <tr>
        <td class="tebal">Akreditasi Sekolah</td>
        <td>{$cmb.AKREDITASI_SEKOLAH}</td>
    </tr>
    <tr>
        <td class="tebal">Jenis Kelas</td>
        <td>{$cmb.JENIS_KELAS}</td>
    </tr>
    <tr>
        <td class="tebal">Rata Nilai</td>
        <td>{$cmb.NILAI_RAPOR}</td>
    </tr>
    <tr>
        <td>Semester 3</td>
        <td>{$cmb.RATA_SEMESTER_3}</td>
    </tr>
    <tr>
        <td>Semester 4</td>
        <td>{$cmb.RATA_SEMESTER_4}</td>
    </tr>
    <tr>
        <td>Semester 5</td>
        <td>{$cmb.RATA_SEMESTER_5}</td>
    </tr>
	<tr>
		<td>Detail Skor PM</td>
		<td>
			<ul>
			{foreach $cmb.cpm_set as $cpm}
				<li>{$cpm.NILAI} - {$cpm.NM_SKOR_PM}</li>
			{/foreach}
			</ul>
		</td>
	</tr>
</table>