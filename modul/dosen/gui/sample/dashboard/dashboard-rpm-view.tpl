<div class="center_title_bar">Rekap PM SNMPTN</div>

<style>
#myTable tr th { border: 1px solid #A6C9E2; padding: 2px; }
#myTable tr td { border: 1px solid #A6C9E2; padding: 2px; }
</style>

<table id="myTable">
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Nama</th>
		<th>Prodi</th>
        <th>Skor PM</th>
        <th>Nama Ortu</th>
        <th>Sponsor</th>
        <th>Keterangan</th>
		<th>Approval</th>
		<th>Urutan</th>
		<th>Sekolah</th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr>
        <td class="center">{$cmb@index + 1}</td>
        <td><a href="dashboard-rpm.php?mode=detail&id_c_mhs={$cmb.ID_C_MHS}">{$cmb.NO_UJIAN}<a/></td>
        <td>{$cmb.NM_C_MHS}</td>
		<td>{$cmb.NM_PROGRAM_STUDI}</td>
        <td class="center">{$cmb.NILAI_PM}</td>
        <td>{$cmb.NM_ORTU}</td>
        <td>{$cmb.NM_SPONSOR}</td>
        <td>{$cmb.KETERANGAN}</td>
		<td>
			<select name="action{$cmb.ID_C_MHS}">
				<option value="-1" {if $cmb.STATUS_VERIFIKASI == '-1'}selected="selected"{/if}>--</option>
				<option value="1" {if $cmb.STATUS_VERIFIKASI == '1'}selected="selected"{/if}>Setuju</option>
				<option value="0" {if $cmb.STATUS_VERIFIKASI == '0'}selected="selected"{/if}>Tidak Setuju</option>
				<!--<option value="2" {if $cmb.STATUS_VERIFIKASI == '2'}selected="selected"{/if}>Pindah</option>-->
			</select>
		</td>
		<td class="center">{$cmb.URUTAN}</td>
		<td class="center">{$cmb.NM_SEKOLAH}</td>
    </tr>
    {/foreach}
</table>

<script type="text/javascript">
	$('select[name^="action"]').change(function() {
		var id_c_mhs = this.name.replace('action','');
		var status_verifikasi = this.value;
		
		$.ajax({
			url: 'dashboard-rpm.php',
			type: 'POST',
			data: 'mode=update-pm&id_c_mhs='+id_c_mhs+'&status_verifikasi='+status_verifikasi,
			success: function(r) {
				if (r != 1) { alert('Gagal approve, ulangi approval.'); }
			}
		});
	});
</script>