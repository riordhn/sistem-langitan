<div class="center_title_bar">KUMPULAN JOURNAL PERPUSTAKAAN</div>
{if isset($data_journal)}
    <a style="padding:8px" class="ui-button ui-state-default ui-corner-all" href="journal.php">Kembali</a>
    <table class="ui-widget" style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">List Journal {$data_journal[0].keterangan}</th>
        </tr>
        <tr class="ui-widget-header">
            <th class="header-coloumn">No</th>
            <th class="header-coloumn">No. Panggil</th>
            <th class="header-coloumn">Judul</th>
            <th class="header-coloumn">Tempat</th>
        </tr>
        {foreach $data_journal as $data}
            <tr class="ui-widget-content">
                <td class="ui-widget-content center">{$data@index+1}</td>
                <td>{$data.ddc1} {$data.ddc2}</td>
                <td><a onclick="$('#dialog_detail_buku').dialog('open').load('info_buku.php?mode=detail&reg={$data.kode_register}')">{$data.judul}</a></td>
                <td>{$data.keterangan}</td>
            </tr>
        {/foreach}
    </table>
{else}
    <h2 class="ui-widget-header ui-corner-all" style="padding: 8px;width: 50%">List Direktori Journal</h2>
    {foreach $data_list_journal as $data}
        <div class="ui-widget" style="margin-left:20px">
            <div class="ui-state-default ui-corner-all" style="width: 40%;margin:5px;padding: 5px;"> 
                <p><span class="ui-icon ui-icon-folder-collapsed" style="float: left; margin-right: 10px;"></span>
                    <a href="journal.php?mode=data&tempat={$data.kode_tempat}">{$data.keterangan} (<span style="color: darkgoldenrod">{$data.jumlah}</span>)</a>
            </div>
        </div>

    {/foreach}
{/if}
<div id="dialog_detail_buku" title="Detail Journal"></div>
{literal}
    <script type="text/javascript">
        $('#dialog_detail_buku').dialog({
                width:'600',
                modal: true,
                resizable:false,
                autoOpen:false
        });
        $('.ui-widget a').css({'color':'#0896b5',
            'cursor': 'pointer'});
    </script>
{/literal}