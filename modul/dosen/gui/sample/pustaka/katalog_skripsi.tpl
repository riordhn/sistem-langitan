<div class="center_title_bar">{$TITLE}</div> 
<p><h2>Katalog Penerbit :</h2></p>
<ul>
<li><a href="#">Katalog Cengage</a></li>
<li><a href="#">Katalog Discovery Publishing</a></li>
<li><a href="#">Katalog Elsevier </a><br>

</li>
<li><a href="#">Katalog John Wiley</a></li>
<li><a href="#">Katalog Pearson</a></li>
<li><a href="#">Katalog Springer</a><br/>
</li>
<li><a href="#">Katalog World Scientific</a></li>
<li><a href="#">Katalog Koleksi Umum</a>&nbsp;</li>
</ul>

<p><h2>Katalog Penerbit Berdasarkan Fakultas:</h2></p>
<ul>
<li><a href="#">Fakultas Kedokteran</a></li>
<li><a href="#">Fakultas Kedokteran Gigi</a></li>
<li><a href="#">Fakultas Hukum</a><br/></li>
<li><a href="#">Fakultas Ekonomi</a></li>
<li><a href="#">Fakultas Farmasi</a></li>
<li><a href="#">Fakultas Kedokteran Hewan</a></li>
<li><a href="#">Fakultas ISIP</a></li>
<li><a href="#">Fakultas Sains dan Teknologi</a></li>
<li><a href="#">Fakultas Psikologi</a></li>
<li><a href="#">Fakultas Kesehatan Masyarakat</a><br/></li>
<li><a href="#">Fakultas Ilmu Budaya</a></li>
<li><a href="#">Fakultas Ilmu Keperawatan</a></li>
<li><a href="#">Fakultas Perikanan dan Kelautan</a>
</li></ul>

</ul>
