<table class="ui-widget" style="width: 560px">
    <tr class="ui-widget-header">
        <th colspan="2" class="header-coloumn"><h2>Detail Catalouge</h2></th>
    </tr>
    <tbody>
        <tr class="ui-widget-content">
            <td style="width: 90px" class="field">
                Title
            </td>
            <td>
                 {$data_detail_buku.judul}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="field">
                Register
            </td>
            <td>
                {$data_detail_buku.kode_register}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="field">Call Number
            </td>
            <td>
                {$data_detail_buku.ddc1} {$data_detail_buku.ddc2}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="field">Creator
            </td>
            <td>
                {$data_detail_buku.entri_utama}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="field">Publisher
            </td>
            <td>
                {$data_detail_buku.penerbitan}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="field">
                Year
            </td>
            <td>
                {$data_detail_buku.tahun}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="field">Subject
            </td>
            <td>
                {$data_detail_buku.subjek}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="field">Description
            </td>
            <td>
                {$data_detail_buku.deskripsi_fisik}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="field">Book Location</td>
            <td>
                {$data_detail_buku.keterangan}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="field">Status</td>
            <td> Baik</td>
        </tr>
    </tbody>
    <tr class="ui-widget-content">
        <td colspan="2" class="center">
            <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_detail_buku').dialog('close')">Close</span>
        </td>
    </tr>
</table>