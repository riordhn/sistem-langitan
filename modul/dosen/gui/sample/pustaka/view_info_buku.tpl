{literal}
    <style>
        .highlight { 
            background-color: #e3ed12;
        }
    </style>
{/literal}
<div class="center_title_bar">INFORMASI BUKU PERPUSTAKAAN UNAIR</div> 
<form action="info_buku.php" method="get">
    <table class="ui-widget" style="width: 60%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Form Pencarian</th>
        </tr>
        <tr class="ui-widget-content">
            <td style="width: 25%">Pilihan Pencarian</td>
            <td>
                <select name="pilihan">
                    <option value="buku">Buku</option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td style="width: 25%">Kriteria</td>
            <td>
                <select name="kriteria">
                    <option value="judul" {if $smarty.get.kriteria=='judul'}selected="true"{/if} >Judul</option>
                    <option value="subjek" {if $smarty.get.kriteria=='subjek'}selected="true"{/if} >Subjek</option>
                    <option value="pengarang" {if $smarty.get.kriteria=='pengarang'}selected="true"{/if} >Pengarang</option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Kunci Pencarian</td>
            <td>
                <input type="text" name="kunci" value="{$smarty.get.kunci}" />
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Tempat Koleksi</td>
            <td>
                <select size="1" name="tempat">
                    <!--<OPTION value=4 selected>---Pilih Database---</OPTION>-->
                    <option value="">---ALL---</option>
                    <option value="0" {if $smarty.get.tempat=='0'}selected="true"{/if}>Perpustakaan Unair</option>
                    <option value="9-01" {if $smarty.get.tempat=='9-01'}selected="true"{/if}>Nation Building Corner</option>
                    <option value="4-01" {if $smarty.get.tempat=='4-01'}selected="true"{/if}>Psycho Corner</option>
                    <option value="6-0" {if $smarty.get.tempat=='6-0'}selected="true"{/if}>CPPS Corner</option>
                    <option value="8-01" {if $smarty.get.tempat=='8-01'}selected="true"{/if}>American Corner</option>
                    <option value="13-1" {if $smarty.get.tempat=='13-1'}selected="true"{/if}>R. Baca Fak. Hukum</option>
                    <option value="12-1" {if $smarty.get.tempat=='12-1'}selected="true"{/if}>R. Baca Fak. Kes. Masyarakat</option>
                    <option value="18-1" {if $smarty.get.tempat=='18-1'}selected="true"{/if}>R. Baca Fak. MIPA</option>
                    <option value="11-1" {if $smarty.get.tempat=='11-1'}selected="true"{/if}>Lab. Fak. Kedokteran</option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input style="padding:5px" class="ui-button ui-state-default ui-corner-all" type="submit" value="Cari"/>
                <input type="hidden" name="mode" value="view" />
            </td>
        </tr>
    </table>
</form>
{if isset($data_hasil_cari)}
    <table class="ui-widget" style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="5" class="header-coloumn">HASIL PENCARIAN<br/> hasil pencarian {$data_hasil_cari|@count} record mengandung {$smarty.get.kriteria} {$smarty.get.kunci}</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>No. Panggil</th>
            <th>Judul</th>
            <th>Pengarang</th>
            <th>Tempat</th>
        </tr>
        {foreach $data_hasil_cari as $data}
            <tr class="ui-widget-content hasil_cari">
                <td class="ui-widget-content center">{$data@index+1}</td>
                <td>{$data.ddc1} {$data.ddc2}</td>
                <td><a class="disable-ajax" onclick="$('#dialog_detail_buku').dialog('open').load('info_buku.php?mode=detail&reg={$data.kode_register}')">{$data.judul}</a></td>
                <td>{$data.entri_utama}</td>
                <td>{$data.keterangan}</td>
            </tr>
        {/foreach}
    </table>
{/if}
<div id="dialog_detail_buku" title="Detail Buku"></div>
{literal}
    <script type="text/javascript">
        $('#dialog_detail_buku').dialog({
                width:'600',
                modal: true,
                resizable:false,
                autoOpen:false
        });
        function getUrlVars(){
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for(var i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
        var kunci = getUrlVars()['kunci'].replace('+',' ');
        $('.hasil_cari').highlight(kunci);
        $('.ui-widget a').css({'color':'#0896b5',
            'cursor': 'pointer'});
    </script>
{/literal}
