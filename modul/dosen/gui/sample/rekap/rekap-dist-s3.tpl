<div class="center_title_bar">Profil Distribusi Nilai Mata Kuliah Program S3
</div> 
<a href="rekap.php">Kembali</a>

<table width="100%" class="ui-widget">
    <tr class="ui-widget-header">
        <td rowspan="3">PRODI</td>
    <td colspan="7">2011/2012</td>
    </tr>
    <tr class="ui-widget-header">
    <td colspan="7">% Distribusi ratarata nilai kuliah dan praktikum
</td>
    </tr>
    <tr class="ui-widget-header">
    <td>A</td>
    <td>AB</td>
    <td>B</td>
    <td>BC</td>
    <td>C</td>
    <td>D</td>
    <td>E</td>
    </tr>
    
    {for $i=0 to $jml_data-1}
    <tr class="ui-widget-content">
    <td>{$prodi[$i]}</td>
    {if $nilai_A[$i]==0}
        <td style="background-color: red;">
        &nbsp;
    </td>
    {else}
    <td>
        {$nilai_A[$i]}
    </td>
    {/if}
    {if $nilai_AB[$i]==0}
        <td style="background-color: red;">
        &nbsp;
    </td>
    {else}
    <td>
        {$nilai_AB[$i]}
    </td>
    {/if}
    
    {if $nilai_B[$i]==0}
        <td style="background-color: red;">
        &nbsp;
    </td>
    {else}
    <td>
        {$nilai_B[$i]}
    </td>
    {/if}
    
    {if $nilai_BC[$i]==0}
        <td style="background-color: red;">
        &nbsp;
    </td>
    {else}
    <td>
        {$nilai_BC[$i]}
    </td>
    {/if}
    
    {if $nilai_C[$i]==0}
        <td style="background-color: red;">
        &nbsp;
    </td>
    {else}
    <td>
        {$nilai_C[$i]}
    </td>
    {/if}
    
    {if $nilai_D[$i]==0}
        <td style="background-color: red;">
        &nbsp;
    </td>
    {else}
    <td>
        {$nilai_D[$i]}
    </td>
    {/if}
    
    {if $nilai_E[$i]==0}
        <td style="background-color: red;">
        &nbsp;
    </td>
    {else}
    <td>
        {$nilai_E[$i]}
    </td>
    {/if}
    </tr>
    {/for}
</table>