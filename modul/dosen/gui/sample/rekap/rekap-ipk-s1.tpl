<div class="center_title_bar">Rekapitulasi IPK Mahasiswa S1</div>
<a href="rekap.php">Kembali</a>
<style>
	.ui-widget tr td { font-size: 12px; }
	.center { text-align: center }
</style>
<table class="ui-widget">
	<tr class="ui-widget-header">
		<th class="header-column" rowspan="2">Prodi</th>
		<th class="header-column" colspan="2">IPK&lt;2</th>
		<th class="header-column" colspan="2">2&lt;IPK&lt;2.5</th>
		<th class="header-column" colspan="2">2.5&lt;IPK&lt;2.75</th>
		<th class="header-column" colspan="2">2.75&lt;IPK&lt;3</th>
		<th class="header-column" colspan="2">3&lt;IPK&lt;3.5</th>
		<th class="header-column" colspan="2">IPK&gt;3.5</th>
		<th class="header-column" rowspan="2">Jumlah</th>
	</tr>
	<tr class="ui-widget-header">
		<th class="header-column">Jml</th><th class="header-column">%</th>
		<th class="header-column">Jml</th><th class="header-column">%</th>
		<th class="header-column">Jml</th><th class="header-column">%</th>
		<th class="header-column">Jml</th><th class="header-column">%</th>
		<th class="header-column">Jml</th><th class="header-column">%</th>
		<th class="header-column">Jml</th><th class="header-column">%</th>
	</tr>
	{foreach $fakultas_set as $f}
	<tr>
		<td class="ui-widget-content" style="font-weight: bold; font-size:14px" colspan="14">{$f.NM_FAKULTAS}</td>
	</tr>
		{foreach $f.program_studi_set as $ps}
		<tr/>
			<td class="ui-widget-content">{$ps.NM_PROGRAM_STUDI}</td>
			{foreach $data_set as $d}
				{if $d.ID_PROGRAM_STUDI == $ps.ID_PROGRAM_STUDI}
					<td class="ui-widget-content center">{$d.IP1}</td>
					<td class="ui-widget-content center">{$d.PERSEN_IP1}%</td>
					<td class="ui-widget-content center">{$d.IP2}</td>
					<td class="ui-widget-content center">{$d.PERSEN_IP2}%</td>
					<td class="ui-widget-content center">{$d.IP3}</td>
					<td class="ui-widget-content center">{$d.PERSEN_IP3}%</td>
					<td class="ui-widget-content center">{$d.IP4}</td>
					<td class="ui-widget-content center">{$d.PERSEN_IP4}%</td>
					<td class="ui-widget-content center">{$d.IP5}</td>
					<td class="ui-widget-content center">{$d.PERSEN_IP5}%</td>
					<td class="ui-widget-content center">{$d.IP6}</td>
					<td class="ui-widget-content center">{$d.PERSEN_IP6}%</td>
					<td class="ui-widget-content center">{$d.TOTAL}</td>
				{/if}
			{/foreach}
			
		</tr>
		{/foreach}
	{/foreach}
</table>