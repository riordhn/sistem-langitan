<div class="center_title_bar">Rekapitulasi ELPT Mahasiswa Baru D3</div>
<a href="rekap.php">Kembali</a>
<style>
	.ui-widget tr td { font-size: 12px; }
	.center { text-align: center }
</style>
<table class="ui-widget">
	<tr class="ui-widget-header">
		<th class="header-column" rowspan="2">Prodi</th>
		<th class="header-column" colspan="2">Score&lt; 450</th>
		<th class="header-column" colspan="2">450&lt;Score&lt;500</th>
		<th class="header-column" colspan="2">Score&gt;500</th>
		<th class="header-column" rowspan="2">Jumlah</th>
	</tr>
	<tr class="ui-widget-header">
		<th class="header-column">Jml</th><th class="header-column">%</th>
		<th class="header-column">Jml</th><th class="header-column">%</th>
		<th class="header-column">Jml</th><th class="header-column">%</th>
	</tr>
	{foreach $fakultas_set as $f}
	<tr>
		<td class="ui-widget-content" style="font-weight: bold; font-size:14px" colspan="8">{$f.NM_FAKULTAS}</td>
	</tr>
		{foreach $f.program_studi_set as $ps}
		<tr/>
			<td class="ui-widget-content">{$ps.NM_PROGRAM_STUDI}</td>
			{foreach $data_set as $d}
				{if $d.ID_PROGRAM_STUDI == $ps.ID_PROGRAM_STUDI}
					<td class="ui-widget-content center">{$d.ELPT1}</td>
					<td class="ui-widget-content center">{$d.PERSEN_ELPT1}%</td>
					<td class="ui-widget-content center">{$d.ELPT2}</td>
					<td class="ui-widget-content center">{$d.PERSEN_ELPT2}%</td>
					<td class="ui-widget-content center">{$d.ELPT3}</td>
					<td class="ui-widget-content center">{$d.PERSEN_ELPT3}%</td>
					<td class="ui-widget-content center">{$d.TOTAL}</td>
				{/if}
			{/foreach}
			
		</tr>
		{/foreach}
	{/foreach}
</table>