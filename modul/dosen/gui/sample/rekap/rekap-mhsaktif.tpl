<div class="center_title_bar">Rekapitulasi Mahasiswa Aktif</div>
<a href="rekap.php">Kembali</a>
<style>
	.ui-widget tr td { font-size: 12px; }
</style>
<table class="ui-widget">
	<tr class="ui-widget-header">
		<th class="header-column" rowspan="2">Prodi</th>
		{foreach $jenjang_set as $j}
		<th class="header-column" colspan="2">{$j.NM_JENJANG}</th>
		{/foreach}
	</tr>
	<tr class="ui-widget-header">
		{foreach $jenjang_set as $j}
		<th class="header-column">Lama</th><th class="header-column">Baru</th>
		{/foreach}
	</tr>
	{foreach $fakultas_set as $f}
	<tr>
		<td class="ui-widget-content" style="font-weight: bold; font-size:14px" colspan="13">{$f.NM_FAKULTAS}</td>
	</tr>
		{foreach $f.program_studi_set as $ps}
		<tr/>
			<td class="ui-widget-content">{$ps.NM_PROGRAM_STUDI}</td>
			{foreach $jenjang_set as $j}
			<td class="ui-widget-content" style="text-align:center">
				{foreach $data_set as $d}{if $d.ID_PROGRAM_STUDI == $ps.ID_PROGRAM_STUDI && $d.ID_JENJANG == $j.ID_JENJANG}{$d.TLAIN}{/if}{/foreach}
			</td>
			<td class="ui-widget-content" style="text-align:center">
				{foreach $data_set as $d}{if $d.ID_PROGRAM_STUDI == $ps.ID_PROGRAM_STUDI && $d.ID_JENJANG == $j.ID_JENJANG}{$d.T2011}{/if}{/foreach}
			</td>
			{/foreach}
		</tr>
		{/foreach}
	{/foreach}
</table>