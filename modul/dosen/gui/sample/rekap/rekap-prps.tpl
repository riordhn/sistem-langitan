<div class="center_title_bar">Profil Program Studi {$nama_pt}</div>

<style type="text/css">
	tr.s1 td.ui-widget-content {
		background-color: #000000;
	}
	tr.s2 td.ui-widget-content {
		background-color: #ffffff;
	}
	tr.s3 td.ui-widget-content {
		background-color: #ffffff;
	}
</style>

<table class="ui-widget">
    <tr class="ui-widget-header">
        <td>NO</td>
        <td>PRODI</td>
        <td>JENJANG</td>
        <td>SK PENDIRIAN</td>
        <td>IZIN PENYELENGGARAAN</td>
        <td>AKHIR IZIN s.d</td>
        <td>SK AKREDITASI</td>
    </tr>
    {for $i=0 to $jml_data-1}
    <tr>
        <td class="ui-widget-content">{$i+1}</td>
        <td class="ui-widget-content">{$prodi[$i]}</td>
        <td class="ui-widget-content">{$jenjang[$i]}</td>
        <td class="ui-widget-content">{$sk_terbit[$i]}</td>
        <td class="ui-widget-content">{$sk_penyelenggaraan[$i]}</td>
        <td class="ui-widget-content">{$sk_expired[$i]}</td>
        <td class="ui-widget-content">{$sk_akreditasi[$i]}</td>
    </tr>
    {/for}
</table>
    