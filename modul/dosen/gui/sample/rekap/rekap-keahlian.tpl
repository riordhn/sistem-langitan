{literal}
    <style>
        .ui-widget tr td { padding: 2px; vertical-align: middle; }
        tr.back-yellow { background: #ff0; }
        tr.back-green { background: #8f8; }
        .ui-autocomplete {
            max-height: 200px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
            width: 700px;
        }
        /* IE 6 doesn't support max-height
        * we use height instead, but this forces the menu to always be this tall
        */
        * html .ui-autocomplete {
            height: 200px;
        }

    </style>
{/literal}
{if $smarty.get.mode==''}
    <div class="center_title_bar">Rekapitulasi Pencarian Keahlian Dosen</div>
    <table>
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="font-size: 13pt">Cari</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Berdasarkan Nama/NIP/Bidang Keahlian</td>
            <td>
                <form method="get" action="rekap-keahlian.php">
                    <input type="text" size="70" name="cari" id="cari" value="{$smarty.get.cari}" />
                    <input type="hidden" size="70" name="mode" value="cari"/>
                    <input type="submit" value="Cari" class="ui-button ui-corner-all ui-state-default" style="padding: 5px;cursor: pointer" />
                </form>
            </td>   
        </tr>
    </table>
    <span style="margin-left: 10px;padding:5px" id="button_advance" class="ui-button ui-state-default ui-corner-all" >Pencarian Selengkapnya</span>
    <form style="display: none" id="advance_search" action="rekap-keahlian.php" method="get">
        <table class=" ui-widget-content" style="width: 100%">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn" style="font-size: 13pt">Filter Pencarian Berdasarkan Parameter</th>
            </tr>
            <tr class="ui-widget-content">
                <td>Nama</td>
                <td>
                    <input type="text" size="30" {if $smarty.get.nama!=''} value="{$smarty.get.nama}"{/if}  name="nama" />
                </td>   
            </tr>
            <tr>
                <td style="width: 20%">Fakultas</td>
                <td>
                    <select id="select_fakultas" name="id_fakultas">
                        <option value="">Pilih</option>
                        {foreach $fakultas_set as $f}
                            <option value="{$f.ID_FAKULTAS}" {if $smarty.get.id_fakultas == $f.ID_FAKULTAS}selected="selected"{/if}>{$f.NM_FAKULTAS}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Program Studi</td>
                <td>
                    <select id="select_prodi" name="id_program_studi">
                        <option value="">Pilih</option>
                        {foreach $program_studi_set as $ps}
                            <option value="{$ps.ID_PROGRAM_STUDI}" {if $smarty.get.id_program_studi==$ps.ID_PROGRAM_STUDI}selected="true"{/if}>({$ps.NM_JENJANG}) {$ps.NM_PROGRAM_STUDI}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Kota Domisili</td>
                <td><input type="text" id="kota" {if $smarty.get.kota!=''} value="{$smarty.get.kota}"{/if} name="kota" size="50"/></td>
            </tr>
            <tr>
                <td>Mata Kuliah Pengajar</td>
                <td><input type="text" id="matakuliah" {if $smarty.get.matakuliah!=''} value="{$smarty.get.matakuliah}"{/if}  name="matakuliah" size="50"/></td>
            </tr>
            <tr>
                <td>No HP</td>
                <td><input type="text" name="hp" {if $smarty.get.hp!=''} value="{$smarty.get.hp}"{/if}   size="50"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <i><b style="color: green">Kosongi parameter jika tidak memakai parameter tersebut</b></i>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <input type="hidden" name="mode" value="advance-search" />
                    <input type="submit" style="padding:5px" class="ui-button ui-state-default ui-corner-all" value="Lihat" />
                </td>
            </tr>
        </table>
    </form>
{/if}

{if $smarty.get.mode=='cari'||$smarty.get.mode=='advance-search'}
    <div class="center_title_bar">Rekapitulasi Pencarian Keahlian Dosen</div>
    <a  class="ui-button ui-corner-all ui-state-default" style="padding: 5px;cursor: pointer;margin: 5px" href="rekap-keahlian.php">Kembali</a>
    <table class="tablesorter ui-widget-content" width="100%">
        <thead>
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="6" style="font-size: 13pt">Data Pencarian Dosen</th>
            </tr>
            <tr>
                <th class="center">
                    NO
                </th>
                <th class="center">
                    NIP
                </th>
                <th class="center">
                    NAMA
                </th>
                <th class="center">
                    PRODI/FAKULTAS
                </th>
                <th class="center">
                    BIDANG KEAHLIAN
                </th>
                <th class="center">-</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_dosen as $d}
                <tr class="ui-widget-content">
                    <td class="center">{$d@index+1}</td>
                    <td>
                        {$d.NIP_DOSEN}
                    </td>
                    <td>{$d.GELAR_DEPAN} {$d.NM_PENGGUNA} {$d.GELAR_BELAKANG}</td>
                    <td class="center">{$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI} /{$d.NM_FAKULTAS}</td>
                    <td>{$d.BIDANG_KEAHLIAN_DOSEN}</td>
                    <td>
                        <a class="ui-button ui-corner-all ui-state-default" style="padding: 5px;cursor: pointer" href="rekap-keahlian.php?mode=detail&id={$d.ID_PENGGUNA}">Detail</a>
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td class="kosong" colspan="5">Data Tidak Ditemukan</td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{else if $smarty.get.mode=='detail'}
    <div class="center_title_bar">Rekapitulasi Detail Pencarian Dosen</div>
    {if $smarty.get.history==''}
        <a  class="disable-ajax ui-button ui-corner-all ui-state-default" style="padding: 5px;cursor: pointer;margin: 5px" onclick="history.back(-1)">Kembali</a>
    {else}
        <a  class="disable-ajax ui-button ui-corner-all ui-state-default" style="padding: 5px;cursor: pointer;margin: 5px" onclick="$('#content').load('rekap-keahlian.php')">Kembali</a>
    {/if}
    <table class="ui-widget-content" style="width: 95%">
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="4" style="font-size: 13pt">Data Detail Dosen</th>
        </tr>
        <tr>
            <td>NIP/NIK</td>
            <td>:</td>
            <td>{$dsn.USERNAME}</td>
            <td rowspan="10" style="text-align: center;width: 250px"><img src="{$PHOTO}" border="0" width="160" /><br/><br/></td>
        </tr>
        <tr>
            <td>NIDN</td>
            <td>:</td>
            {if $dsn.NIDN_DOSEN > 0}
                <td>{$dsn.NIDN_DOSEN} 
                    <input type="button" name="cek_nidn" value="Cek Data DIKTI" onclick="javascript:popup('http://evaluasi.pdpt.dikti.go.id/epsbed/datadosen/{$dsn.NIDN_DOSEN}', 'name', '960', '435', 'center', 'front');">
                </td>
                {else}
                <td>-</td>
            {/if}
        </tr>
        <tr>
            <td>SERDOS</td>
            <td>:</td>
            {if $dsn.SERDOS > 0}
                <td>{$dsn.SERDOS}</td>
            {else}
                <td>-</td>
            {/if}		
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>:</td>
            <td>{$dsn.NM_PROGRAM_STUDI}</td>
        </tr>        
        <tr>
            <td>Departemen</td>
            <td>:</td>
            <td>{$dsn.NM_DEPARTEMEN}</td>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>:</td>
            <td>{$dsn.NM_FAKULTAS}</td>
        </tr>
        <tr>
            <td>Status Kepegawaian</td>
            <td>:</td>
            <td>{$dsn.STATUS_DOSEN}</td>
        </tr>
        <tr>
            <td>Pangkat (Gol.) Terakhir / TMT</td>
            <td>:</td>
            <td>{$dsn.NM_GOLONGAN} - {$dsn.NM_PANGKAT}  / {$dsn.TMT_GOLONGAN}</td>
        </tr>
        <tr>
            <td>Jabatan Fungsional / TMT </td>
            <td>:</td>
            <td>{$dsn.NM_JABATAN_FUNGSIONAL}  / {$dsn.TMT_JAB_FUNGSIONAL} </td>
        </tr>
        <tr>
            <td>Tugas Tambahan</td>
            <td>:</td>
            {if $dsn.NM_JABATAN_PEGAWAI != null}
                <td>{$dsn.NM_JABATAN_PEGAWAI}</td>
            {else}
                <td>-</td>
            {/if}	
        </tr>
        <tr>
            <td>Status Aktif</td>
            <td>:</td>
            <td colspan="2">{$dsn.NM_STATUS_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Pendidikan Akhir</td>
            <td>:</td>
            <td colspan="2">{if $dsn.NAMA_PENDIDIKAN_AKHIR!=''}{$dsn.NAMA_PENDIDIKAN_AKHIR} {$dsn.NM_SEKOLAH_PENDIDIKAN} {$dsn.NM_JURUSAN_PENDIDIKAN} ({$dsn.TAHUN_MASUK_PENDIDIKAN}-{$dsn.TAHUN_LULUS_PENDIDIKAN}){else}-{/if}</td>
        </tr>
        <tr>
            <td>Gelar Depan</td>
            <td>:</td>
            <td colspan="2">{$dsn.GELAR_DEPAN}</td>
        </tr>
        <tr>
            <td>Gelar Belakang</td>
            <td>:</td>
            <td colspan="2">{$dsn.GELAR_BELAKANG}</td>
        </tr>
        <tr>
            <td>Nama Lengkap</td>
            <td>:</td>
            <td colspan="2">{$dsn.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Tempat, Tanggal Lahir</td>
            <td>:</td>
            <td colspan="2">{$dsn.TEMPAT_LAHIR}, {$dsn.TGL_LAHIR_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>:</td>
            {if $dsn.KELAMIN_PENGGUNA=='1'}
                <td colspan="2">LAKI-LAKI</td>
            {elseif $dsn.KELAMIN_PENGGUNA=='2'}
                <td colspan="2">PEREMPUAN</td>
            {else}
                <td colspan="2">-</td>
            {/if}
        </tr>
        <tr>
            <td>Agama</td>
            <td>:</td>
            <td colspan="2">{$dsn.NM_AGAMA}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td colspan="2">{$dsn.ALAMAT_RUMAH_DOSEN}</td>
        </tr>
        <tr>
            <td>Telepon/HP</td>
            <td>:</td>
            {if $dsn.TLP_DOSEN == null}
                <td colspan="2">{$dsn.MOBILE_DOSEN}</td>
            {else if $dsn.MOBILE_DOSEN == null}
                <td colspan="2">{$dsn.TLP_DOSEN}</td>
            {else}
                <td colspan="2">{$dsn.TLP_DOSEN} / {$dsn.MOBILE_DOSEN} </td>
            {/if}
        </tr>
        <tr>
            <td>Email #1</td>
            <td>:</td>
            <td colspan="2">{$dsn.EMAIL_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Email #2</td>
            <td>:</td>
            <td colspan="2">{$dsn.EMAIL_ALTERNATE}</td>
        </tr>
    </table>

    <table width="90%" class="tablesorter ui-widget-content">
        <thead>
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="5">History Mengajar Dosen</th>
            </tr>
            <tr>
                <th>No</th>
                <th>Kode Mata Kuliah</th>
                <th>Nama Mata Kuliah</th>
                <th>Jumlah Mahasiswa</th>
                <th>Detail</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_mata_kuliah as $mk}
                <tr>
                    <td>{$mk@index+1}</td>
                    <td>{$mk.KD_MATA_KULIAH}</td>
                    <td>{$mk.NM_MATA_KULIAH}</td>
                    <td class="center">{$mk.JUMLAH_MHS}</td>
                    <td class="center">
                        <span class="ui-button ui-corner-all ui-state-default" onclick="$('#dialog-detail-kelas').load('rekap-keahlian.php?mode=detail_mk&dosen={$dsn.ID_DOSEN}&id_mk={$mk.ID_MATA_KULIAH}').dialog('open')" style="padding: 5px;cursor: pointer">Detail</span>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div id="dialog-detail-kelas" title="Detail Kelas Mata Kuliah"></div>
{else if $smarty.get.mode=='detail_mk'}
    <table width="98%" class="ui-widget">
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="4">History Kelas Mata Kuliah</th>
        </tr>
        <tr>
            <th>No</th>
            <th>Nama Mata Kuliah (Kelas)</th>
            <th>Semester</th>
            <th>Jumlah Mahasiswa</th>
        </tr>
        {foreach $data_kelas_mk as $kmk}
            <tr>
                <td>{$kmk@index+1}</td>
                <td>{$kmk.NM_MATA_KULIAH} Kelas {$kmk.NM_KELAS}</td>
                <td>{$kmk.NM_SEMESTER} {$kmk.TAHUN_AJARAN}</td>
                <td class="center">{$kmk.JUMLAH_MHS}</td>
            </tr>
        {/foreach}
    </table>
{/if}
{literal}
    <script type="text/javascript">
        $(function() {
            $('form').validate();
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
            $("#cari").autocomplete({
                source: "GetKeahlianAutoComplete.php",
                minLength: 3,
                select: function(event, ui) {
                    $('#content').load('rekap-keahlian.php?history=reload&mode=detail&id=' + ui.item.id + '');
                }
            });
            $('#select_fakultas').change(function() {
                $.ajax({
                    url: 'getProgramStudi.php',
                    type: 'post',
                    data: 'id_fakultas=' + $(this).val(),
                    success: function(data) {
                        $('#select_prodi').html(data);
                    }
                });
            });
            $('#button_advance').click(function() {
                $('#advance_search').toggle('slow');
            });
            $("#kota").autocomplete({
                source: "GetKotaAutoComplete.php",
                minLength: 3,
                select: function(event, ui) {
                    $(this).val("");
                }
            });
            $("#matakuliah").autocomplete({
                source: "GetMataKuliahAjarAutoComplete.php",
                minLength: 3,
                select: function(event, ui) {
                    $(this).val("");
                }
            });
            $("#dialog-detail-kelas").dialog({
                width: '700',
                modal: true,
                resizable: false,
                autoOpen: false
            });
        });
    </script>
{/literal}