{literal}
    <script type="text/javascript">

        $(".tablesorter").tablesorter(
                {
                    sortList: [[1, 0]],
                }
        );
    </script>
{/literal}
<div class="center_title_bar">Monitoring Bidik Misi {$NM_PRODI}, Tahun Akademik {$THN_AKAD}</div>  
<form class="ui-widget" action="monitoring-bidikmisi.php" method="post" name="id_daftarmk" id="id_daftarmk">
    <input type="hidden" name="action" value="view" >
    <p>
        Prodi : 
        <select name="kdprodi">
            <option value='ALL'>-- ALL --</option>
            {foreach item="list" from=$T_PRODI}
                {html_options  values=$list.ID_PROGRAM_STUDI output=$list.PRODI}
            {/foreach}
        </select>
        Tahun/Semester akademik : 
        <select name="kdthnsmt">
            <option value=''>------</option>
            {foreach item="list_smt" from=$T_THNSMT}
                {html_options values=$list_smt.ID_SEMESTER output=$list_smt.THN_SMT}
            {/foreach}
        </select>
        <input type="submit" name="View" value="View">
    </p>
</form>	

<p>
    <input style="display: none" type=button name="cetak" value="Ekspor ke Excel" onclick="window.open('proses/mon_bidik_misi-xls.php?prodi={$PRODI}&smt={$SMT_AKAD}&nm_prodi={$NM_PRODI}&tahun={$TAHUN_SMT}&thn_hitung={$THN_HITUNG}', 'baru2');">&nbsp;
</p>

<table id="myTable" class="tablesorter ui-widget" cellspacing="1" cellpadding="0" border="0">
    <thead>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>Nim</th>
            <th>Nama Mahasiswa</th>
            <th>Prodi</th>
            <th>Status</th>
            <th>Dosen Wali</th>
            <th>SKS SMT</th>
            <th>IPS</th>
            <th>TTL SKS</th>
            <th>IPK</th>
        </tr>
    </thead>
    <tbody>
        {foreach name=test item="list" from=$T_MK}
            <tr class="ui-widget-content">
                <td style="text-align:center">{$smarty.foreach.test.iteration}</td>
                <td>{$list.NIM_MHS}</td>
                <td>{$list.NM_MHS}</td>
                <td>{$list.PRODI}</td>
                <td>{$list.STATUS}</td>
                <td>{$list.DOLI}</td>
                <td style="text-align:center">{$list.SKS_SEM}</td>
                <td style="text-align:center">{$list.IPS}</td>
                <td style="text-align:center">{$list.SKS_TOTAL_MHS}</td>
                <td style="text-align:center">{$list.IPK_MHS}</td>
            </tr>
        {foreachelse}
            <tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
    </tbody>
</table>
