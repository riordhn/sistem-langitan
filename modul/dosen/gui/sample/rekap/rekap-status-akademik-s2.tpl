<div class="center_title_bar">Rekapitulasi Status Mahasiswa S2</div>
<a href="rekap.php">Kembali</a>
<style>
	.ui-widget tr td { font-size: 12px; }
	.center { text-align: center }
</style>
<form method="post" action="rekap-status-akademik-s2.php">
<table class="ui-widget">
	<tr>
		<td>Thn Angkatan</td>
		<td>
			<select name="thn_angkatan">
					<option value="">Pilih Tahun Angkatan</option>
				{foreach $thn_angkatan as $data}
					<option value="{$data.THN_ANGKATAN_MHS}" {if $smarty.request.thn_angkatan==$data.THN_ANGKATAN_MHS} selected="selected"{/if}>{$data.THN_ANGKATAN_MHS}</option>
				{/foreach}
			</select>
		</td>
		<td>Atau</td>
		<td>Thn Akademik</td>
		<td>
			<select name="thn_akademik">
					<option value="">Pilih Tahun Akademik</option>
				{foreach $thn_akademik as $data}
					<option value="{$data.THN_AKADEMIK_SEMESTER}" {if $smarty.request.thn_akademik==$data.THN_AKADEMIK_SEMESTER} selected="selected"{/if}>{$data.TAHUN_AJARAN}</option>
				{/foreach}
			</select>
		</td>
		<td><input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampil" /></td>
	</tr>
</table>
</form>
{if isset($smarty.request.thn_angkatan)}
{if $smarty.request.thn_akademik != ''}* Berdasarkan Tahun Akademik{else}* Berdasarkan Tahun Angkatan{/if}
<table class="ui-widget">
	<tr class="ui-widget-header">
		<th class="header-column" rowspan="2" style="width: 40%">Prodi</th>
		<th class="header-column" colspan="2">Terdaftar</th>
		<th class="header-column" colspan="2">Undur Diri</th>
		<th class="header-column" colspan="2">Mhs DO</th>
		<th class="header-column" colspan="2">Lulus</th>
		<th class="header-column" rowspan="2">Jumlah</th>
	</tr>
	<tr class="ui-widget-header">
		<th class="header-column">Jml</th><th class="header-column">%</th>
		<th class="header-column">Jml</th><th class="header-column">%</th>
		<th class="header-column">Jml</th><th class="header-column">%</th>
		<th class="header-column">Jml</th><th class="header-column">%</th>
	</tr>
	{foreach $fakultas_set as $f}
	<tr>
		<td class="ui-widget-content" style="font-weight: bold; font-size:14px" colspan="13">{$f.NM_FAKULTAS}</td>
	</tr>
		{foreach $f.program_studi_set as $ps}
		<tr/>
			<td class="ui-widget-content">{$ps.NM_PROGRAM_STUDI}</td>
			{foreach $data_set as $d}
				{if $d.ID_PROGRAM_STUDI == $ps.ID_PROGRAM_STUDI}
					<td class="ui-widget-content center">{$d.AKTIF}</td>
					<td class="ui-widget-content center">{$d.PERSEN_AKTIF}%</td>
					<td class="ui-widget-content center">{$d.UNDUR}</td>
					<td class="ui-widget-content center">{$d.PERSEN_UNDUR}%</td>
					<td class="ui-widget-content center">{$d.DO}</td>
					<td class="ui-widget-content center">{$d.PERSEN_DO}%</td>
					<td class="ui-widget-content center">{$d.LULUS}</td>
					<td class="ui-widget-content center">{$d.PERSEN_LULUS}%</td>
					<td class="ui-widget-content center">{$d.TOTAL}</td>
				{/if}
			{/foreach}
			
		</tr>
		{/foreach}
	{/foreach}
</table>
{/if}