<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<title>Dosen - {$nama_pt}</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="https://fonts.googleapis.com/css?family=Noto+Sans+HK&display=swap" rel="stylesheet">
		<link type="text/css" rel="stylesheet" href="../../css/dosen_style.css" />
		<link type="text/css" rel="stylesheet" href="../../css/jquery-ui-1.8.11.custom.css" />
		<link type="text/css" rel="stylesheet" href="../../js/dependencies/screen.css"  />
		<!-- CSS TAMBAHAN DARI NAMBI -->
		<link type="text/css" rel="stylesheet" href="css/jquery.dataTables.css" />
		<link type="text/css" rel="stylesheet" href="css/themes/blue/style.css" media="print, projection, screen" />
	</head>
	<body>
		<!-- CSS TAMBAHAN DARI NAMBI - UNTUK MENU {$jumlah_menu_tampil}-->
		{if $jumlah_menu_tampil<=12}
			<style>
				a.nav:link, a.nav:visited, ul.menu li a.disable-ajax:link, ul.menu li a.disable-ajax:visited
				{
					display: block;
					float: left;
					padding: 0% 1.9%;
					height: 53px;
					text-decoration: none;
					font-weight: bold;
					color: #ffffff;
				}
			</style>
		{/if}
		<div id="main_container">
			<div id="header" style="background-image: url('../../img/header/dosen-{$nama_singkat}.png')">

			</div>
			<div id="main_content">
				<div id="menu_tab">
					<div class="left_menu_corner"></div>
					<ul class="menu">
						<!-- Untuk Menampilkan Menu Utama  -->
						{foreach $modul_set as $m}
							{if $m.AKSES==1}
								<li>
									<a href="#{$m.NM_MODUL}!{$m.PAGE}" class="nav">{$m.TITLE}</a>
								</li>
								<li class="divider">

								</li>
							{/if}
						{/foreach}
					</ul>
					<div class="right_menu_corner"></div>
				</div>
				<div class="crumb_navigation" id="breadcrumbs">
					<!-- handled by ajax  -->
				</div>
			</div>

			<div class="left_content" >                 

				<div id="image-photo" >
					<img src="{$foto}" style="height:150px;width:130px; margin: auto;padding: 4px"/>

				</div>
				<input type="button" name="ganti_photo" value="Ganti Photo" onclick="javascript:popup('{$IMG}', 'name', '600', '400', 'center', 'front')" style="text-align:center">
				<div id="menu" class="border_box">
					<!-- handled by ajax  -->
				</div>
			</div>
			<!-- coba tandai lukman -->
			<div id="content" class="center_content">
				<!-- handled by ajax -->
			</div>
			<div class="footer">
				<div class="left_footer">
					<img src="../../img/dosen/footer_logo.jpg" style="display: none" />
				</div>

				<div class="center_footer">
					Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU
				</div>

				<div class="right_footer">
					<a href="//{$smarty.server.SERVER_NAME}" class="disable-ajax">Home</a>
					<a href="//{$smarty.server.SERVER_NAME}" class="disable-ajax">Manual</a>
					<a href="//{$smarty.server.SERVER_NAME}" class="disable-ajax">Sitemap</a>
					<a href="//{$smarty.server.SERVER_NAME}" class="disable-ajax">RSS</a>
					<a href="//{$smarty.server.SERVER_NAME}" class="disable-ajax">Contact us</a>
				</div>   

			</div>  
		</div>

		<!-- Script -->
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
		<script language="JavaScript" src="../../js/FusionCharts.js"></script>
		<script type="text/javascript" src="../../js/jquery.elastic.source.js" charset="utf-8"></script>
		<script type="text/javascript" src="../../js/jquery.shiftenter.js"></script>
		<script type="text/javascript" src="../../js/jquery.fixedtable.js"></script>
		<script type="text/javascript" src="../../js/jquery.form.js"></script>
		<script type="text/javascript" src="../../js/jquery.validate.js"></script>
		<script type="text/javascript" src="../../js/jquery.highlight.js"></script>
		<script type="text/javascript" src="../../js/cybercampus.ajax-1.0.js"></script>
		<!-- JS TAMBAHAN DARI NAMBI -->
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/jquery.dataTables.js" ></script>
		<script type="text/javascript" src="js/jquery.raty.min.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>

{literal}
    <script language="javascript">

        var popupWindow = null;
        function popup(mypage, myname, w, h, pos, infocus) {
            if (pos == "random")
            {
                LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
                TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
            }
            else
            {
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
            }
            settings = "width=" + w + ",height=" + h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
            popupWindow = window.open("", myname, settings);
            if (infocus == "front") {
                popupWindow.focus();
                popupWindow.location = mypage;
            }
            if (infocus == "back") {
                popupWindow.blur();
                popupWindow.location = mypage;
                popupWindow.blur();
            }
        }

        function new_window(mypage, myname, w, h, pos, infocus) {
            if (pos == "random")
            {
                LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
                TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
            }
            else
            {
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
            }
            settings = "width=" + w + ",height=" + h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
            popupWindow = window.open("", myname, settings);
            if (infocus == "front") {
                popupWindow.focus();
                popupWindow.location = mypage;
            }
            if (infocus == "back") {
                popupWindow.blur();
                popupWindow.location = mypage;
                popupWindow.blur();
            }
        }

        
    </script>
{/literal}
	</body>
</html>