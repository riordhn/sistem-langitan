{if $smarty.get.mode==''}
    <link rel="stylesheet" href="../../css/jquery-ui-custom-mhs.css" type="text/css">
    <script src="../../js/?f=jquery-1.5.1.js"></script>
    <script src="../../js/cupertino/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script src="../../js/jquery.validate.js"></script>
    <style>
        body{
            font-size: 12px;
        }
        table {
            border-width: 1px;
            border-style: solid;
            border-color: #999999;
            border-collapse: collapse;
            margin: 10px 0px;
            font-family:Lucida Grande,Lucida Sans,Arial,sans-serif;
        }
        th{
            border: 1px solid #D49768;
            background: #CB842E url(images-mhs/ui-bg_glass_25_cb842e_1x400.png) 50% 50% repeat-x;
            color: white;
            font-weight: bold;
            font-size:14px;
            padding:8px;
            text-transform:uppercase;
        }
        th h2{
            font-size:13px;
            padding:5px;
            margin:0px;
        }
        .ui-widget-header th{
            font-size:13px;
        }
        td{
            padding: 8px;
            font-size: 12px;
            vertical-align: top;
            border-width: 1px;
            border-style: solid;
            border-color: #969BA5;
            border-collapse: collapse;
        }
        textarea.error, input.error, select.error { border: 1px solid red; width: auto; background-color:#ffe9e9;}
        label.error {
            color: red;
            font-size:11px;
            display: block;
        }
    </style>
    {if $smarty.post.id_pengguna!=''}
        {if $error_upload!=''}
            <div style="font-size: 0.8em;">{$error_upload}</div>
        {else}
            {$success_upload}
        {/if}
    {else}
        {$info_1}
    {/if}
    <form action="upload_files.php?{$smarty.server.QUERY_STRING}" method="post" enctype="multipart/form-data">
        <table class="ui-widget-content" style="width: 99%">
            <tr>
                <th colspan="2" class="ui-widget-header">Upload File {$jenis}</th>
            </tr>
            <tr>
                <td>FILE UPLOAD (Jika Sudah ada file akan di replace)</td>
                <td>

                    <input type="file" class="required" name="file"/>
                    <input type="hidden" name="id_pengguna" value="{$smarty.get.id_pengguna}"/>
                    <input type="hidden" name="id_jenis" value="{$smarty.get.jenis}"/>
                    <input type="hidden" name="id_riwayat" value="{$smarty.get.riwayat}"/>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <input type="hidden" name="mode" value="upload"/>
                    <span class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer" onclick="close_window()">Refresh Data</span>
                    <input type="submit" value="Upload" class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer"/>
                </td>
            </tr>
        </table>
    </form>
    {literal}
        <script>
            $('form').validate();
            function close_window()
            {
                window.opener.location.reload();
                window.close();
            }
        </script>
    {/literal}
{/if}