<html>
    <head>
        <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="/modul/sumberdaya/js/datetimepicker.js"></script>
    </head>
    <body>
        <form action="insert_pengmas.php" id="pengmasinsert" method="post" onsubmit="return validate_form();">
            <input type="hidden" name="id_dosen" value="{$smarty.get.id}">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr class="collapse">
                    <td class="labelrow">Nama Pengmas&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tempat Pengmas&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tempat" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Bidang Pengmas&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="bidang" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Peran Pengmas&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="peran" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tahun Pengmas&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tahun" size="4" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Total Dana Pengmas&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="dana" size="20" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Sumber Dana Pengmas&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="sumber" style="width:600px;" maxlength="50"  /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tingkat Pengmas&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tingkat" placeholder="Nasional/International/Lokal" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Hasil Pengmas&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="hasil" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow"></td>
                    <td class="inputrow"><input type="submit" name="submit" value="Simpan" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>