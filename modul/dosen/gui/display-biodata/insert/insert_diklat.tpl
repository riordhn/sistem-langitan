<html>
    <head>
        <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="/modul/sumberdaya/js/datetimepicker.js"></script>
    </head>
    <body>
        <form name="diklatinsert" action="insert_diklat.php" id="klgdrinsert" method="post" onsubmit="return validate_form();">
            <input type="hidden" name="id_pengguna" value="{$smarty.get.id}">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr class="collapse">
                    <td class="labelrow">Nama Diklat&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="nama" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Status Luar Negeri&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="status_luar">
                            <option value="1">Luar Negeri</option>
                            <option value="2">Dalam Negeri</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Lokasi&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <textarea name="lokasi" style="width: 90%;height: 120px;resize: none"></textarea>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tempat Lahir &nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select id="negara" name="negara">
                            <option value="">Pilih Negara</option>
                            {foreach $negara as $n}
                                <option value="{$n.ID_NEGARA}">{$n.NM_NEGARA}</option>
                            {/foreach}
                        </select>
                        <select id="propinsi" name="propinsi">
                            <option value="">Pilih Propinsi</option>
                        </select>
                        <select id="kota" name="kota_lokasi">
                            <option value="1">Pilih Kota</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal Mulai&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_mulai" style="width:120px; text-align:center;" id="tgl_mulai" style="text-align:center;" onclick="javascript:NewCssCal('tgl_mulai', 'ddmmyyyy', '', '', '', '', 'past')" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tanggal Selesai&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tgl_selesai" style="width:120px; text-align:center;" id="tgl_selesai" style="text-align:center;" onclick="javascript:NewCssCal('tgl_selesai', 'ddmmyyyy', '', '', '', '', 'past')" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Jumlah Jam&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="jumlah_jam" size="10" maxlength="10" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Penyelenggara&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="penyelenggara" style="width:600px;" maxlength="50" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Jenis Diklat&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="jenis">
                            <option value="Fungsional">Fungsional</option>
                            <option value="Teknis">Teknis</option>
                            <option value="Penjenjangan/Struktural">Penjenjangan/Struktural</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tingkat Diklat&nbsp;:&nbsp;</td>
                    <td class="inputrow">
                        <select name="tingkat">
                            <option value="">Kosongi</option>
                            <option value="1">Diklatpim TK-I</option>
                            <option value="2">Diklatpim TK-II</option>
                            <option value="3">Diklatpim TK-III</option>
                            <option value="4">Diklatpim TK-IV</option>
                            <option value="5">Diklat Lain yang Setara</option>
                        </select>
                    </td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Tahun Angkatan&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="tahun" size="4" maxlength="4" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow">Predikat Dikat&nbsp;:&nbsp;</td>
                    <td class="inputrow"><input type="text" name="predikat" size="20" maxlength="20" /></td>
                </tr>
                <tr class="collapse">
                    <td class="labelrow"></td>
                    <td class="inputrow"><input type="submit" name="submit" value="Simpan" /><input type="button" name="cancel" value="Batal" /></td>
                </tr>
            </table>
        </form>
        {literal}
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#negara').change(function() {
                        $.ajax({
                            type: "POST",
                            url: "../getNegaraPropinsi.php",
                            data: {id: $('#negara').val()},
                            success: function(data) {
                                $('#propinsi').html(data);
                            }
                        })
                    });
                    $('#propinsi').change(function() {
                        $.ajax({
                            type: "POST",
                            url: "../getPropinsiKota.php",
                            data: {id: $('#propinsi').val()},
                            success: function(data) {
                                $('#kota').html(data);
                            }
                        })
                    });
                });
            </script>
        {/literal}
    </body>
</html>