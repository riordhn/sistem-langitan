<div class="center_title_bar">Info Dosen</div>
{literal}
    <script language="javascript">

        var popupWindow = null;
        function popup(mypage, myname, w, h, pos, infocus) {
            if (pos == "random")
            {
                LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
                TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
            }
            else
            {
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
            }
            settings = "width=" + w + ",height=" + h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
            popupWindow = window.open("", myname, settings);
            if (infocus == "front") {
                popupWindow.focus();
                popupWindow.location = mypage;
            }
            if (infocus == "back") {
                popupWindow.blur();
                popupWindow.location = mypage;
                popupWindow.blur();
            }
        }

        function new_window(mypage, myname, w, h, pos, infocus) {
            if (pos == "random")
            {
                LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
                TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
            }
            else
            {
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
            }
            settings = "width=" + w + ",height=" + h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
            popupWindow = window.open("", myname, settings);
            if (infocus == "front") {
                popupWindow.focus();
                popupWindow.location = mypage;
            }
            if (infocus == "back") {
                popupWindow.blur();
                popupWindow.location = mypage;
                popupWindow.blur();
            }
        }

        $(document).ready(function() {


            $("#nip").blur(function()
            {
                $("#msgbox1").text("Checking...").fadeIn("slow");
                $.post("../sumberdaya/cek_nip.php", {nip: $("#nip").val(), id: $("#id_pengguna").val()}, function(data)
                {
                    if (data == "ada")
                    {
                        $("#msgbox1").fadeTo(200, 0.1, function()
                        {
                            $(this).html("<font color='red'>NIP/NIK tidak valid, sudah digunakan</font>").fadeTo(900, 1);
                        });
                    }
                    else
                    {
                        $("#msgbox1").fadeTo(200, 0.1, function()
                        {
                            $(this).html("").fadeTo(900, 1);
                        });
                    }

                });

            });

            $("#nidn").blur(function()
            {
                $("#msgbox2").text("Checking...").fadeIn("slow");
                $.post("../sumberdaya/cek_nidn.php", {nidn: $("#nidn").val(), id: $("#id_dosen").val()}, function(data)
                {
                    if (data == "ada")
                    {
                        $("#msgbox2").fadeTo(200, 0.1, function()
                        {
                            $(this).html("<font color='red'>NIDN tidak valid, sudah digunakan</font>").fadeTo(900, 1);
                        });
                    }
                    else
                    {
                        $("#msgbox2").fadeTo(200, 0.1, function()
                        {
                            $(this).html("<font color='darkgreen'>NIDN valid, </font><input type='button' name='cek_nidn' value='Cek Data DIKTI' onclick=\"javascript:popup('http://evaluasi.pdpt.dikti.go.id/epsbed/datadosen/" + $("#nidn").val() + "','name','960','435','center','front');\">").fadeTo(900, 1);
                        });
                    }

                });

            });

            $("#serdos").blur(function()
            {
                $("#msgbox3").text("Checking...").fadeIn("slow");
                $.post("../sumberdaya/cek_serdos.php", {serdos: $("#serdos").val(), id: $("#id_dosen").val()}, function(data)
                {
                    if (data == "ada")
                    {
                        $("#msgbox3").fadeTo(200, 0.1, function()
                        {
                            $(this).html("<font color='red'>Serdos tidak valid, sudah digunakan</font>").fadeTo(900, 1);
                        });
                    }
                    else
                    {
                        $("#msgbox3").fadeTo(200, 0.1, function()
                        {
                            $(this).html("").fadeTo(900, 1);
                        });
                    }

                });

            });

        });
    </script>
{/literal}
{if $mode == 'edit'}

    <form action="info_dosen.php" method="post">
        <table  class="ui-widget none" cellspacing="0" style="width: 98%" cellpadding="0" border="0">
            {foreach item="dsn" from=$DOSEN}
                <input type="hidden" name="id_dosen" id="id_dosen" value="{$dsn.ID_DOSEN}">
                <input type="hidden" name="id_pengguna" id="id_pengguna" value="{$dsn.ID_PENGGUNA}">
                <input type="hidden" name="action" value="update">
                <tr class="ui-widget-header">
                    <th colspan="4" class="header-coloumn">PROFILE KEPEGAWAIAN</th>
                </tr>
                <tr>
                    <td style="width: 30%"><span class="field">NIP/NIK</span></td>
                    <td style="width: 5%" style="text-align:center">:</td>
                    <td style="width: 65%" colspan="2">{$dsn.USERNAME}</td>
                    <!--
                    <td rowspan="10"  style="text-align: center"><img src="{$PHOTO}" border="0" width="160" /></td>
                    -->
                </tr>
                <tr>
                    <td><span class="field">NIP/NIK LAMA</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input  type="text" name="nip_lama" style="width:200px;" value="{$dsn.NIP_LAMA}"/></td>		
                </tr>
                <tr>
                    <td><span class="field">NIDN</span></td>
                    <td style="text-align:center">:</td>
                    {if $dsn.NIDN_DOSEN > 0}
                        <td colspan="2">
                            <input type="text" name="nidn" id="nidn" style="width:200px;" value="{$dsn.NIDN_DOSEN}"/>&nbsp;&nbsp;&nbsp;<span id="msgbox2" style="display:none"></span>
                        </td>
                    {else}
                        <td colspan="2">
                            <input type="text"  name="nidn" id="nidn" style="width:200px;" value=""/>&nbsp;&nbsp;&nbsp;<span id="msgbox2" style="display:none"></span>
                        </td>
                    {/if}
                </tr>
                <tr>
                    <td><span class="field">SERDOS</span></td>
                    <td style="text-align:center">:</td>
                    {if $dsn.SERDOS > 0}
                        <td colspan="2">
                            <input type="text" name="serdos" id="nidn" style="width:200px;" value="{$dsn.SERDOS}"/>&nbsp;&nbsp;&nbsp;<span id="msgbox2" style="display:none"></span>
                        </td>
                    {else}
                        <td colspan="2">
                            <input type="text"  name="serdos" id="nidn" style="width:200px;" value=""/>&nbsp;&nbsp;&nbsp;<span id="msgbox2" style="display:none"></span>
                        </td>
                    {/if}	
                </tr>
                <tr>
                    <td><span class="field">Fakultas</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2">
                        <select name="kdfak" id="kdfak" onChange="ambil()">
                            {foreach item="fak" from=$T_FAK}
                                {html_options values=$fak.ID_FAKULTAS output=$fak.NM_FAKULTAS selected=$FAKGET}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><span class="field">Program Studi</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2">
                        <select  name="prodi" id="prodi">
                            {foreach item="pro" from=$PRO}
                                {html_options values=$pro.ID_PROGRAM_STUDI output=$pro.NM_PROGRAM_STUDI selected=$dsn.ID_PROGRAM_STUDI}
                            {/foreach}
                        </select>
                    </td>
                </tr>	
                <tr>
                    <td><span class="field">Departemen Dosen</span></td>
                    <td style="text-align:center">:</td>
                    <td>
                        <select name="dept" id="dept">
                            {foreach item="dept" from=$DEPT}
                                {html_options values=$dept.ID_DEPARTEMEN output=$dept.NM_DEPARTEMEN selected=$dsn.DEPARTEMEN_DOSEN}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><span class="field">Status Kepegawaian</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2">
                        <select name="status_dsn" id="status_dsn">
                            <option value="PNS" {if $dsn.STATUS_DOSEN=='PNS'}selected="true"{/if}>TETAP</option>
                            <option value="HONORER" {if $dsn.STATUS_DOSEN=='HONORER'}selected="true"{/if}>HONORER</option>
                            <option value="KONTRAK" {if $dsn.STATUS_DOSEN=='KONTRAK'}selected="true"{/if}>KONTRAK</option>
                            <option value="DPK" {if $dsn.STATUS_DOSEN=='DPK'}selected="true"{/if}>DPK</option>
                            <option value="GB UNAIR" {if $dsn.STATUS_DOSEN=='GB UNAIR'}selected="true"{/if}>GURU BESAR</option>
                            <option value="GB EMIRITUS" {if $dsn.STATUS_DOSEN=='GB EMIRITUS'}selected="true"{/if}>GURU BESAR EMIRITUS</option>
                            <option value="LB" {if $dsn.STATUS_DOSEN=='LB'}selected="true"{/if}>LB</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><span class="field">Prajabatan Nomor</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input  type="text" name="prajab_nomor" style="width:200px;" value="{$dsn.PRAJAB_NOMOR}"/></td>
                </tr>
                <tr>
                    <td><span class="field">Prajabatan Tanggal</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input class="datepick"  type="text" name="prajab_tanggal" id="prajab_tanggal" style="width:200px;" value="{$dsn.PRAJAB_TANGGAL|date_format:'%d-%m-%Y'}"/></td>
                </tr>
                <tr>
                    <td><span class="field">TGL Sumpah PNS</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input class="datepick" type="text" name="tgl_sumpah_pns" id="tgl_sumpah_pns" style="width:200px;"  value="{$dsn.TGL_SUMPAH_PNS|date_format:'%d-%m-%Y'}"/></td>
                </tr>
                <tr>
                    <td><span class="field">TMT CPNS</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input class="datepick"  type="text" name="tmt_cpns" id="tmt_cpns" style="width:200px;" value="{$dsn.TMT_CPNS|date_format:'%d-%m-%Y'}"/></td>
                </tr>
                <tr>
                    <td><span class="field">Nomer Karpeg</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input  type="text" name="nomor_karpeg" style="width:500px;" value="{$dsn.NOMER_KARPEG}"/></td>
                </tr>
                <tr>
                    <td><span class="field">Nomer NPWP</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input  type="text" name="nomor_npwp" style="width:500px;" value="{$dsn.NOMOR_NPWP}"/></td>
                </tr>
                <tr>
                    <td><span class="field">Taspen</td>
                    <td style="text-align:center">:</td>
                    <td colspan="2">
                        <select name="taspen">
                            <option value="1" {if $dsn.TASPEN==1}selected="true"{/if}>Sudah</option>
                            <option value="0" {if $dsn.TASPEN==0}selected="true"{/if}>Belum</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><span class="field">Pangkat (Gol.) Terakhir / TMT</td>
                    <td style="text-align:center">:</td>
                    {if $dsn.NM_GOLONGAN != null}
                        <td colspan="2" >{$dsn.NM_GOLONGAN} - {$dsn.NM_PANGKAT}  / {$dsn.TMT_GOLONGAN}</td>
                    {else}
                        <td colspan="2" >-</td>
                    {/if}
                </tr>
                <tr>
                    <td><span class="field">Unit Esselon I</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input  type="text" name="unit_esselon1" style="width:500px;" value="{$dsn.UNIT_ESSELON_I}"/></td>
                </tr>
                <tr>
                    <td><span class="field">Unit Esselon II</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input  type="text" name="unit_esselon2" style="width:500px;" value="{$dsn.UNIT_ESSELON_II}"/></td>
                </tr>
                <tr>
                    <td><span class="field">Unit Esselon III</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input  type="text" name="unit_esselon3" style="width:500px;" value="{$dsn.UNIT_ESSELON_III}"/></td>
                </tr>
                <tr>
                    <td><span class="field">Unit Esselon IV</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input  type="text" name="unit_esselon4" style="width:500px;" value="{$dsn.UNIT_ESSELON_IV}"/></td>
                </tr>
                <tr>
                    <td><span class="field">Unit Esselon V</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input  type="text" name="unit_esselon5" style="width:500px;" value="{$dsn.UNIT_ESSELON_V}"/></td>
                </tr>
                <tr>
                    <td><span class="field">Jabatan Fungsional / TMT </span></td>
                    <td style="text-align:center">:</td>
                    {if $dsn.NM_JABATAN_FUNGSIONAL != null}
                        <td colspan="2" >{$dsn.NM_JABATAN_FUNGSIONAL}  / {$dsn.TMT_JAB_FUNGSIONAL} </td>
                    {else}
                        <td colspan="2" >-</td>
                    {/if}
                </tr>
                <tr>
                    <td><span class="field">Tugas Tambahan</span></td>
                    <td style="text-align:center">:</td>
                    {if $dsn.NM_JABATAN_PEGAWAI != null}
                        <td colspan="2" >{$dsn.NM_JABATAN_PEGAWAI}</td>
                    {else}
                        <td colspan="2" >-</td>
                    {/if}
                </tr>
                <tr>
                    <td><span class="field">Pendidikan Akhir</span></td>
                    <td style="text-align:center">:</td>
                    {if $dsn.NAMA_PENDIDIKAN_AKHIR != null}
                        <td colspan="2" >{$dsn.NAMA_PENDIDIKAN_AKHIR} {$dsn.NM_SEKOLAH_PENDIDIKAN} {$dsn.NM_JURUSAN_PENDIDIKAN} ({$dsn.TAHUN_MASUK_PENDIDIKAN}-{$dsn.TAHUN_LULUS_PENDIDIKAN})</td>
                    {else}
                        <td colspan="2" >-</td>
                    {/if}
                </tr>
                <tr>
                    <td><span class="field">Status Aktif</span></td>
                    <td style="text-align:center">:</td>
                    {if $dsn.ID_STATUS_PENGGUNA != null}
                        <td colspan="2" >
                            <select name="aktif" id="aktif">
                                {foreach item="aktif" from=$STS_AKTIF}
                                    {html_options values=$aktif.ID_STATUS_PENGGUNA output=$aktif.NM_STATUS_PENGGUNA selected=$dsn.ID_STATUS_PENGGUNA}
                                {/foreach}
                            </select>
                        </td>
                    {else}
                        <td colspan="2" >
                            <select name="aktif" id="aktif">
                                <option value=''></option>
                                {foreach item="aktif" from=$STS_AKTIF}
                                    {html_options values=$aktif.ID_STATUS_PENGGUNA output=$aktif.NM_STATUS_PENGGUNA}
                                {/foreach}
                            </select>
                        </td>
                    {/if}
                </tr>
                <tr class="ui-widget-header">
                    <th colspan="4" class="header-coloumn">IDENTITAS PRIBADI</th>
                </tr>
                <tr>
                    <td><span class="field">Gelar Depan</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input  type="text" name="gelar_dpn" style="width:200px;" value="{$dsn.GELAR_DEPAN}"/></td>
                </tr>
                <tr>
                    <td><span class="field">Gelar Belakang</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input  type="text" name="gelar_blkg" style="width:200;" value="{$dsn.GELAR_BELAKANG}"/></td>
                </tr>
                <tr>
                    <td><span class="field">Nomer KTP</span></td>
                    <td style="text-align:center"></td>
                    <td colspan="2"><input  type="text" name="no_ktp" style="width:500px;" value="{$dsn.NO_KTP}"/></td>
                </tr>
                <tr>
                    <td><span class="field">Nama Lengkap</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input type="text" name="nm_lengkap" style="width:450px;" value="{$dsn.NM_PENGGUNA}"/></td>
                </tr>
                <tr>
                    <td><span class="field">Tempat Lahir</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2">
                        {if $dsn.ID_KOTA_LAHIR == null}
                            <select name="id_kota_lahir">
                                <option value="null" selected="true"></option>
                                {for $i=0 to $count_provinsi}
                                    <optgroup label="{$data_kota[$i].nama}">
                                        {foreach $data_kota[$i].kota as $data}
                                            {html_options values=$data.ID_KOTA output=$data.KOTA}
                                        {/foreach}
                                    </optgroup>
                                {/for}
                            </select>
                        {else}
                            <select name="id_kota_lahir">
                                {for $i=0 to $count_provinsi}
                                    <optgroup label="{$data_kota[$i].nama}">
                                        {foreach $data_kota[$i].kota as $data}
                                            {html_options values=$data.ID_KOTA output=$data.KOTA selected=$dsn.ID_KOTA_LAHIR}
                                        {/foreach}
                                    </optgroup>
                                {/for}
                            </select>
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td><span class="field">Tanggal Lahir</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2">
                        <input class="datepick"  type="text" name="tgl_lahir" id="tgl_lahir" style="text-align:center;" value="{$dsn.TGL_LAHIR_PENGGUNA|date_format:'%d-%m-%Y'}">
                    </td>
                </tr>
                <tr>
                    <td><span class="field">Jenis Kelamin</span></td>
                    <td style="text-align:center">:</td>
                    {if $dsn.KELAMIN_PENGGUNA == '1' || $dsn.KELAMIN_PENGGUNA == '2'}
                        <td colspan="2">
                            <select name="jk" id="jk">
                                {foreach item="jk" from=$JK}
                                    {html_options values=$jk.KELAMIN_PENGGUNA output=$jk.NM_KELAMIN_PENGGUNA selected=$dsn.KELAMIN_PENGGUNA}
                                {/foreach}
                            </select>
                        </td>
                    {else}
                        <td colspan="2">
                            <select name="jk" id="jk">
                                <option value=''></option>
                                {foreach item="jk" from=$JK}
                                    {html_options values=$jk.KELAMIN_PENGGUNA output=$jk.NM_KELAMIN_PENGGUNA}
                                {/foreach}
                            </select>
                        </td>
                    {/if}
                </tr>
                <tr>
                    <td><span class="field">Status Pernikahan</span></td>
                    <td style="text-align:center">:</td>
                    {if $dsn.ID_STATUS_PERNIKAHAN != null}
                        <td colspan="2" >
                            <select name="status_nikah" id="agama">
                                {foreach item="sn" from=$SNIKAH}
                                    {html_options values=$sn.ID_STATUS_PERNIKAHAN output=$sn.NM_STATUS_PERNIKAHAN selected=$dsn.ID_STATUS_PERNIKAHAN}
                                {/foreach}
                            </select>
                        </td>
                    {else}
                        <td colspan="2" >
                            <select name="status_nikah" id="agama">
                                {foreach item="sn" from=$SNIKAH}
                                    {html_options values=$sn.ID_STATUS_PERNIKAHAN output=$sn.NM_STATUS_PERNIKAHAN}
                                {/foreach}
                            </select>
                        </td>
                    {/if}
                </tr>
                <tr>
                    <td><span class="field">Agama</span></td>
                    <td style="text-align:center">:</td>
                    {if $dsn.ID_AGAMA!=''}
                        <td colspan="2">
                            <select name="agama" id="jk">
                                {foreach item="ag" from=$AGAMA}
                                    {html_options values=$ag.ID_AGAMA output=$ag.NM_AGAMA selected=$dsn.ID_AGAMA}
                                {/foreach}
                            </select>
                        </td>
                    {else}
                        <td colspan="2">
                            <select name="agama" id="jk">
                                <option value=''></option>
                                {foreach item="ag" from=$AGAMA}
                                    {html_options values=$ag.ID_AGAMA  output=$ag.NM_AGAMA}
                                {/foreach}
                            </select>
                        </td>
                    {/if}
                </tr>
                <tr>
                    <td><span class="field">Alamat</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2">
                        <textarea name="alamat" style="width: 90%;height: 50px;resize: none">{$dsn.ALAMAT_RUMAH_DOSEN}</textarea>
                    </td>
                </tr>
                <tr>
                    <td><span class="field">Kode Pos</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input type="text" name="kode_pos" style="width:50px;" value="{$dsn.KODE_POS}" /></td>
                </tr>
                <tr>
                    <td><span class="field">Telepon/HP</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input type="text" name="tlp" style="text-align:center;" value="{$dsn.TLP_DOSEN}" maxlength="15"/> / <input type="text" name="hp" style="text-align:center;" value="{$dsn.MOBILE_DOSEN}" maxlength="15"/></td>
                </tr>
                <tr>
                    <td><span class="field">Email #1</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2">{$dsn.EMAIL_PENGGUNA}</td>
                </tr>
                <tr>
                    <td><span class="field">Email #2</span></td>
                    <td style="text-align:center">:</td>
                    <td colspan="2"><input type="text" name="email" style="width:335px;" value="{$dsn.EMAIL_ALTERNATE}" maxlength="100"/></td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="4" style="text-align: right;">
                    <a href="info_dosen.php"style="cursor: pointer;padding:5px;" class="ui-button ui-state-default ui-corner-all">Batal</a>
                    <input type="submit" style="cursor: pointer;padding:4px;" class="ui-button ui-state-default ui-corner-all" value="Simpan"/>
                </td>
            </tr>
        </table>
    </form>
{else}
    <table class="ui-widget none" cellspacing="0" style="width: 98%" cellpadding="0" border="0">
        {foreach item="dsn" from=$DOSEN}
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn">PROFILE KEPEGAWAIAN</th>
            </tr>
            <tr>
                <td style="width: 30%"><span class="field">NIP/NIK</span></td>
                <td style="width: 5%">:</td>
                <td style="width: 65%" colspan="2">
                    {$dsn.USERNAME}
                    {if $dsn.SK_KONVERSI!=''}
                        <button class="disable-ajax" alt="PDF" title="Download SK Konversi NIP" onclick="javascript:window.open('{$link_file}/{$dsn.SK_KONVERSI}');">
                            <img src="../sumberdaya/includes/images/pdf.png" /> SK Konversi NIP
                        </button>
                    {/if}
                    <button class="disable-ajax" title="Upload SK Konversi NIP" onMouseOver="this.style.cursor = 'pointer'" onclick="javascript:new_window('upload_single_file.php?id_pengguna={$dsn.ID_PENGGUNA}&id_jenis=21', 'name', '600', '400', 'center', 'front');" >
                        <img src="../sumberdaya/includes/images/export.png"  /> SK Konversi NIP
                    </button>
                </td>
                <!--
                <td rowspan="10"  style="text-align: center"><img src="{$PHOTO}" border="0" width="160" /><br/><br/><input type="button" name="ganti_photo" value="Ganti Photo" onclick="javascript:popup('{$IMG}', 'name', '600', '400', 'center', 'front')"></td>
                -->
            </tr>
            <tr>
                <td><span class="field">NIP/NIK LAMA</span></td>
                <td>:</td>
                {if $dsn.NIP_LAMA!=''}
                    <td colspan="2">{$dsn.NIP_LAMA}</td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">NIDN</span></td>
                <td>:</td>
                {if $dsn.NIDN_DOSEN > 0}
                    <td colspan="2">{$dsn.NIDN_DOSEN} </td>
                {else}
                    <td colspan="2">-</td>
                {/if}
            </tr>
            <tr>
                <td><span class="field">SERDOS</span></td>
                <td>:</td>
                {if $dsn.SERDOS > 0}
                    <td colspan="2">
                        {$dsn.SERDOS}
                        {if $dsn.SK_DOSEN_TETAP!=''}
                            <button class="disable-ajax" alt="PDF" title="Download SK Dosen Tetap" onclick="javascript:window.open('{$link_file}/{$dsn.SK_DOSEN_TETAP}');">
                                <img src="../sumberdaya/includes/images/pdf.png" /> SK Dosen Tetap
                            </button>
                        {/if}
                        <button class="disable-ajax" title="Upload SK Dosen Tetap" onMouseOver="this.style.cursor = 'pointer'" onclick="javascript:new_window('upload_single_file.php?id_pengguna={$dsn.ID_PENGGUNA}&id_jenis=23', 'name', '600', '400', 'center', 'front');" >
                            <img src="../sumberdaya/includes/images/export.png"  /> SK Dosen Tetap
                        </button>
                    </td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Program Studi</span></td>
                <td>:</td>
                <td colspan="2">
                    {$dsn.NM_PROGRAM_STUDI}
                    {if $dsn.SK_PENEMPATAN!=''}
                        <button class="disable-ajax" alt="PDF" title="Download SK Penempatan Prodi" onclick="javascript:window.open('{$link_file}/{$dsn.SK_PENEMPATAN}');">
                            <img src="../sumberdaya/includes/images/pdf.png"  /> SK Penempatan
                        </button>
                    {/if}
                    <button class="disable-ajax" title="Upload SK Penempatan Prodi" onMouseOver="this.style.cursor = 'pointer'" onclick="javascript:new_window('upload_single_file.php?id_pengguna={$dsn.ID_PENGGUNA}&id_jenis=22', 'name', '600', '400', 'center', 'front');" >
                        <img src="../sumberdaya/includes/images/export.png"  /> SK Penempatan
                    </button>
                </td>
            </tr>        
            <tr>
                <td><span class="field">Departemen</span></td>
                <td>:</td>
                <td colspan="2">{$dsn.NM_DEPARTEMEN}</td>
            </tr>
            <tr>
                <td><span class="field">Fakultas</span></td>
                <td>:</td>
                <td colspan="2">{$dsn.NM_FAKULTAS}</td>
            </tr>
            <tr>
                <td><span class="field">Status Kepegawaian</span></td>
                <td>:</td>
                <td colspan="2">{if $dsn.STATUS_DOSEN == 'PNS'}TETAP{else if $dsn.STATUS_DOSEN == 'GB UNAIR'}GURU BESAR {$nama_singkat|upper}{else}{$dsn.STATUS_DOSEN}{/if}</td>
            </tr>
            <tr>
                <td><span class="field">Prajabatan Nomor</span></td>
                <td>:</td>
                {if $dsn.PRAJAB_NOMOR!=''}
                    <td colspan="2">{$dsn.PRAJAB_NOMOR}</td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Prajabatan Tanggal</span></td>
                <td>:</td>
                {if $dsn.PRAJAB_TANGGAL!=''}
                    <td colspan="2">{$dsn.PRAJAB_TANGGAL}</td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Tanggal Sumpah PNS</span></td>
                <td>:</td>
                {if $dsn.TGL_SUMPAH_PNS!=''}
                    <td colspan="2">
                        {$dsn.TGL_SUMPAH_PNS}
                        {if $dsn.SK_PNS!=''}
                            <button class="disable-ajax" alt="PDF" title="Download SK PNS" onclick="javascript:window.open('{$link_file}/{$dsn.SK_PNS}');">
                                <img src="../sumberdaya/includes/images/[df.png"  /> SK PNS
                            </button>
                        {/if}
                        <button class="disable-ajax" title="Upload SK PNS" onMouseOver="this.style.cursor = 'pointer'" onclick="javascript:new_window('upload_single_file.php?id_pengguna={$dsn.ID_PENGGUNA}&id_jenis=20', 'name', '600', '400', 'center', 'front');" >
                            <img src="../sumberdaya/includes/images/export.png"  /> SK PNS
                        </button>
                    </td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">TMT CPNS</span></td>
                <td>:</td>
                {if $dsn.TMT_CPNS!=''}
                    <td colspan="2">
                        {$dsn.TMT_CPNS}
                        {if $dsn.SK_CPNS!=''}
                            <button class="disable-ajax" alt="PDF" title="Download SK CPNS" onclick="javascript:window.open('{$link_file}/{$dsn.SK_CPNS}');">
                                <img src="../sumberdaya/includes/images/pdf.png"  /> SK CPNS
                            </button>
                        {/if}
                        <button class="disable-ajax" title="Upload SK CPNS" onMouseOver="this.style.cursor = 'pointer'" onclick="javascript:new_window('upload_single_file.php?id_pengguna={$dsn.ID_PENGGUNA}&id_jenis=19', 'name', '600', '400', 'center', 'front');" >
                            <img src="../sumberdaya/includes/images/export.png"  /> SK CPNS
                        </button>
                    </td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Nomor Karpeg</span></td>
                <td>:</td>
                {if $dsn.NOMER_KARPEG!=''}
                    <td colspan="2">{$dsn.NOMER_KARPEG}</td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Nomor NPWP</span></td>
                <td>:</td>
                {if $dsn.NOMOR_NPWP!=''}
                    <td colspan="2">{$dsn.NOMOR_NPWP}</td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Taspen</span></td>
                <td>:</td>
                {if $dsn.TASPEN!=''}
                    <td colspan="2">{if $dsn.TASPEN==1} Sudah {else if $dsn.TASPEN==0} Belum{/if}</td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Unit Esselon I</span></td>
                <td>:</td>
                {if $dsn.UNIT_ESSELON_I!=''}
                    <td colspan="2">{$dsn.UNIT_ESSELON_I}</td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Unit Esselon II</span></td>
                <td>:</td>
                {if $dsn.UNIT_ESSELON_II!=''}
                    <td colspan="2">{$dsn.UNIT_ESSELON_II}</td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Unit Esselon III</span></td>
                <td>:</td>
                {if $dsn.UNIT_ESSELON_III!=''}
                    <td colspan="2">{$dsn.UNIT_ESSELON_III}</td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Unit Esselon IV</span></td>
                <td>:</td>
                {if $dsn.UNIT_ESSELON_IV!=''}
                    <td colspan="2">{$dsn.UNIT_ESSELON_IV}</td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Unit Esselon V</span></td>
                <td>:</td>
                {if $dsn.UNIT_ESSELON_IV!=''}
                    <td colspan="2">{$dsn.UNIT_ESSELON_IV}</td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Pangkat (Gol.) Terakhir / TMT</span></td>
                <td>:</td>
                <td colspan="2">{$dsn.NM_GOLONGAN} - {$dsn.NM_PANGKAT}  / {$dsn.TMT_GOLONGAN}</td>
            </tr>
            <tr>
                <td><span class="field">Jabatan Fungsional / TMT</span> </td>
                <td>:</td>
                <td colspan="2">{$dsn.NM_JABATAN_FUNGSIONAL}  / {$dsn.TMT_JAB_FUNGSIONAL} </td>
            </tr>
            <tr>
                <td><span class="field">Tugas Tambahan / TMT</span></td>
                <td>:</td>
                {if $dsn.NM_JABATAN_STRUKTURAL != null}
                    <td colspan="2">{$dsn.NM_JABATAN_STRUKTURAL} ({$dsn.TGL_SK_SEJ_JAB_STRUKTURAL}-{$dsn.TMT_SEJ_JAB_STRUKTURAL})</td>
                {else}
                    <td colspan="2">-</td>
                {/if}	
            </tr>
            <tr>
                <td><span class="field">Status Aktif</span></td>
                <td>:</td>
                <td colspan="2">{$dsn.NM_STATUS_PENGGUNA}</td>
            </tr>
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn">IDENTITAS PRIBADI</th>
            </tr>
            <tr>
                <td><span class="field">Pendidikan Akhir</span></td>
                <td>:</td>
                <td colspan="2">{if $dsn.NAMA_PENDIDIKAN_AKHIR!=''}{$dsn.NAMA_PENDIDIKAN_AKHIR} {$dsn.NM_SEKOLAH_PENDIDIKAN} {$dsn.NM_JURUSAN_PENDIDIKAN} ({$dsn.TAHUN_MASUK_PENDIDIKAN}-{$dsn.TAHUN_LULUS_PENDIDIKAN}){else}-{/if}</td>
            </tr>
            <tr>
                <td><span class="field">Gelar Depan</span></td>
                <td>:</td>
                <td colspan="2">{$dsn.GELAR_DEPAN}</td>
            </tr>
            <tr>
                <td><span class="field">Gelar Belakang</span></td>
                <td>:</td>
                <td colspan="2">{$dsn.GELAR_BELAKANG}</td>
            </tr>
            <tr>
                <td><span class="field">NO KTP</span></td>
                <td>:</td>
                {if $dsn.NO_KTP!=''}
                    <td colspan="2">
                        {$dsn.NO_KTP}
                        {if $dsn.FILE_KTP!=''}
                            <button class="disable-ajax" alt="PDF" title="Download File KTP" onclick="javascript:window.open('{$link_file}/{$dsn.FILE_KTP}');">
                                <img src="../sumberdaya/includes/images/pdf.png"  /> File KTP
                            </button>
                        {/if}
                        <button class="disable-ajax" title="Upload File KTP" onMouseOver="this.style.cursor = 'pointer'" onclick="javascript:new_window('upload_single_file.php?id_pengguna={$dsn.ID_PENGGUNA}&id_jenis=18', 'name', '600', '400', 'center', 'front');" >
                            <img src="../sumberdaya/includes/images/export.png"  /> File KTP
                        </button>
                    </td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Nama Lengkap</span></td>
                <td>:</td>
                <td colspan="2">{$dsn.NM_PENGGUNA}</td>
            </tr>
            <tr>
                <td><span class="field">Tempat, Tanggal Lahir</span></td>
                <td>:</td>
                <td colspan="2">{$dsn.TEMPAT_LAHIR}, {$dsn.TGL_LAHIR_PENGGUNA}</td>
            </tr>
            <tr>
                <td><span class="field">Jenis Kelamin</span></td>
                <td>:</td>
                {if $dsn.KELAMIN_PENGGUNA=='1'}
                    <td colspan="2">LAKI-LAKI</td>
                {elseif $dsn.KELAMIN_PENGGUNA=='2'}
                    <td colspan="2">PEREMPUAN</td>
                {else}
                    <td colspan="2">-</td>
                {/if}
            </tr>
            <tr>
                <td><span class="field">Agama</span></td>
                <td>:</td>
                <td colspan="2">{$dsn.NM_AGAMA}</td>
            </tr>
            <tr>
                <td><span class="field">Status Pernikahan</span></td>
                <td>:</td>
                {if $dsn.NM_STATUS_PERNIKAHAN!=''}
                    <td colspan="2">{$dsn.NM_STATUS_PERNIKAHAN}</td>
                {else}
                    <td colspan="2">-</td>
                {/if}		
            </tr>
            <tr>
                <td><span class="field">Alamat</span></td>
                <td>:</td>
                <td colspan="2">{$dsn.ALAMAT_RUMAH_DOSEN}</td>
            </tr>
            <tr>
                <td><span class="field">Kode Pos</span></td>
                <td>:</td>
                <td colspan="2">{$dsn.KODE_POS}</td>
            </tr>
            <tr>
                <td><span class="field">Telepon/HP</span></td>
                <td>:</td>
                {if $dsn.TLP_DOSEN == null}
                    <td colspan="2">{$dsn.MOBILE_DOSEN}</td>
                {else if $dsn.MOBILE_DOSEN == null}
                    <td colspan="2">{$dsn.TLP_DOSEN}</td>
                {else}
                    <td colspan="2">{$dsn.TLP_DOSEN} / {$dsn.MOBILE_DOSEN} </td>
                {/if}
            </tr>
            <tr>
                <td><span class="field">Email #1</span></td>
                <td>:</td>
                <td colspan="2"><a class="disable-ajax" href="http://mail.google.com/a/{$dsn.LINK_EMAIL}" target="_blank">{$dsn.EMAIL_PENGGUNA}</a></td>
            </tr>
            <tr>
                <td><span class="field">Blog Dosen</span></td>
                <td>:</td>
                <td colspan="2">
                    {if isset($dsn.BLOG_PENGGUNA)}
                    <a class="disable-ajax" href="{$dsn.LINK_BLOG}" target="_blank">{$dsn.BLOG_PENGGUNA}
                    {/if}
                </td>
            </tr>
            <tr>
                <td><span class="field">Email #2</span></td>
                <td>:</td>
                <td colspan="2">{$dsn.EMAIL_ALTERNATE}</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="4" style="text-align: right;">
                <input type=button value="Cetak Biodata" class="ui-button ui-state-default ui-corner-all" onclick="window.open('proses-biodata/_biodata-dosen_cetak.php');">
                <a href="info_dosen.php?mode=edit" style="cursor: pointer;padding:5px;" class="ui-button ui-state-default ui-corner-all">Edit</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script type="text/javascript" >
        $(".datepick").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "c-70:c+10",
            dateFormat: 'dd-mm-yy'
        });
    </script>
{/literal}