{if $smarty.get.mode==''}
    <div class="center_title_bar">Data Sharing Universitas Airlangga</div>
    <div id="tabs" style="width:98%">
        <ul>
            <li><a class="disable-ajax" href="#tabs-search">SEARCH</a></li>
            <li><a class="disable-ajax" href="#tabs-explorer">EXPLORER</a></li>
            <li><a class="disable-ajax" href="#tabs-request">REQUEST</a></li>
        </ul>
        <div id="tabs-search">
            <iframe src="search-link.php" width="98%" height="450px"  frameborder="0">
            </iframe>
        </div>
        <div id="tabs-explorer">
            <h2 style="text-align: center">Klik 2 Kali pada file untuk download...</h2>
            <div>
                <iframe src="download-link.php" width="100%" style="min-height: 700px" frameborder="0">
                </iframe>
            </div>
        </div>
        <div id="tabs-request">
            <table class="ui-widget-content" style="width: 98%">
                <tr class="ui-widget-header">
                    <th colspan="10" class="header-coloumn">REQUEST DOWNLOAD</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>NO</th>
                    <th>TANGGAL REQUEST</th>
                    <th>TANGGAL APPROVE</th>
                    <th>NAMA FILE</th>
                    <th>DOWNLOAD URL</th>
                    <th>JENIS FILE</th>
                    <th>STATUS VERIFIKASI DOWNLOAD</th>
                    <th>STATUS FILE</th>
                    <th>KETERANGAN APPROVE</th>
                    <th>OPERASI</th>
                </tr>
                {foreach $data_request as $d}
                    <tr>
                        <td>{$d@index+1}</td>
                        <td style="width: 70px">{$d.TGL_REQUEST}</td>
                        <td style="text-align: center;width: 70px">
                            {if $d.ID_APPROVEBY!=''}
                                {$d.TGL_READY}
                            {else}
                                -
                            {/if}
                        </td>
                        <td>{$d.NAMA_FILE}</td>
                        <td style="text-align: center;width: 130px">
                            <a class="disable-ajax ui-button ui-corner-all ui-state-hover" target="_blank" style="padding: 3px;cursor: pointer" href="{$d.DOWNLOAD_URL}">Link Download</a>
                        </td>
                        <td>
                            {$d.JENIS_FILE}
                        </td>
                        <td style="text-align: center">
                            {if $d.ID_APPROVEBY!=''}
                                <span style="color: green">Sudah</span>
                            {else}
                                <span style="color: red">Belum</span>
                            {/if}
                        </td>
                        <td style="width: 100px;text-align: center">
                            {if $d.STATUS_FILE==0}
                                Sedang Request
                            {else if $d.STATUS_FILE==1}
                                Sudah Di Download
                            {else}
                                <span style="color: red;font-size: 0.9em;font-style: italic">
                                    Mohon Periksa Link Download
                                    Lalu Update Data Request
                                </span>
                            {/if}
                        </td>
                        <td style="text-align: center">
                            {if $d.KETERANGAN_APPROVE!=''}
                                {$d.KETERANGAN_APPROVE}
                            {else}
                                -
                            {/if}
                        </td>
                        <td style="width: 100px;text-align: center">
                            {if $d.ID_APPROVEBY==''}
                                <span class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer" onclick="$('#edit-request').load('download-center.php?mode=update&id_download={$d.ID_DOWNLOAD}').dialog('open')">Edit</span>
                                <span class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer" onclick="$('#delete-request').load('download-center.php?mode=delete&id_download={$d.ID_DOWNLOAD}').dialog('open')">Delete</span>
                            {else}
                                -
                            {/if}
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="10" style="text-align: center;color: red">Data Kosong</td>
                    </tr>
                {/foreach}
                <tr>
                    <td colspan="10" style="text-align: center;padding: 10px" >
                        <span class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer" onclick="$('#tambah-request').load('download-center.php?mode=add').dialog('open')">Tambah Request</span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="tambah-request" title="Tambah Request"></div>
    <div id="edit-request" title="Edit Request"></div>
    <div id="delete-request" title="Delete Request"></div>
    {literal}
        <script>
            $( "#tabs" ).tabs();
            $( "#tambah-request:ui-dialog, #edit-request:ui-dialog, #delete-request:ui-dialog" ).dialog( "destroy" );
            $( "#tambah-request, #edit-request, #delete-request" ).dialog({
                    modal: true,
                    autoOpen: false,
                    height: 'auto',
                    width:700
            });
        </script>
    {/literal}
{else if $smarty.get.mode=='add'}
    <form action="download-center.php" method="post">
        <table class="ui-widget" style="width: 98%">
            <tr class="ui-widget-header">
                <th>NAMA FILE</th>
                <th>DOWNLOAD URL</th>
                <th>JENIS FILE</th>
            </tr>
            <tr>
                <td><textarea class="required" style="width: 80%;height: 100px;resize: none;margin: 8px" name="nama"></textarea></td>
                <td><textarea class="required" style="width: 80%;height: 100px;resize: none;margin: 8px" name="url"></textarea></td>
                <td  style="vertical-align: middle">
                    <select name="jenis">
                        <option value="Edukasi">Edukasi</option>
                        <option value="Hiburan">Hiburan</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <input type="hidden" name="mode" value="add"/>
                    <input type="submit"  class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer" value="Simpan"/>
                </td>
            </tr>
        </table>
    </form>
    {literal}
        <script>
            $('form').submit(function(){
                if($('form').valid()){
                    $('#tambah-request').dialog('close');
                    return true;
                }else{
                    return false;
                }
            });            
        </script>
    {/literal}
{else if $smarty.get.mode=='update'}
    <form action="download-center.php" method="post">
        <table class="ui-widget" style="width: 98%">
            <tr class="ui-widget-header">
                <th>NAMA FILE</th>
                <th>DOWNLOAD URL</th>
                <th>JENIS FILE</th>
            </tr>
            <tr>
                <td><textarea class="required" style="width: 80%;height: 100px;resize: none;margin: 8px" name="nama">{$download.NAMA_FILE}</textarea></td>
                <td><textarea class="required" style="width: 80%;height: 100px;resize: none;margin: 8px" name="url">{$download.DOWNLOAD_URL}</textarea></td>
                <td  style="vertical-align: middle">
                    <select name="jenis">
                        <option value="Edukasi" {if $download.JENIS_FILE=='Edukasi'}selected="true"{/if}>Edukasi</option>
                        <option value="Hiburan" {if $download.JENIS_FILE=='Hiburan'}selected="true"{/if}>Hiburan</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <input type="hidden" name="mode" value="update"/>
                    <input type="hidden" name="id_download" value="{$download.ID_DOWNLOAD}"/>
                    <input type="submit"  class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer" value="Update"/>
                    <span class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer" onclick="$('#edit-request').dialog('close')">Batal</span>
                </td>
            </tr>
        </table>
    </form>
    {literal}
        <script>
            $('form').submit(function(){
                if($('form').valid()){
                    $('#edit-request').dialog('close');
                    return true;
                }else{
                    return false;
                }
            });            
        </script>
    {/literal}
{else if $smarty.get.mode=='delete'}
    <form action="download-center.php" method="post">
        <table class="ui-widget" style="width: 98%">
            <tr class="ui-widget-header">
                <th>NAMA FILE</th>
                <th>DOWNLOAD URL</th>
                <th>JENIS FILE</th>
            </tr>
            <tr>
                <td>{$download.NAMA_FILE}</td>
                <td>{substr($download.DOWNLOAD_URL,0,40)}....</td>
                <td  style="vertical-align: middle">
                    {$download.JENIS_FILE}
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <input type="hidden" name="mode" value="delete"/>
                    <input type="hidden" name="id_download" value="{$download.ID_DOWNLOAD}"/>
                    <input type="submit"  class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer" value="Ya"/>
                    <span class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer" onclick="$('#delete-request').dialog('close')">Batal</span>
                </td>
            </tr>
        </table>
    </form>
    {literal}
        <script>
            $('form').submit(function(){
                if($('form').valid()){
                    $('#delete-request').dialog('close');
                    return true;
                }else{
                    return false;
                }
            });            
        </script>
    {/literal}
{/if} 