<script src="../../js/dependencies/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="../../js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
	jQuery.noConflict();
	jQuery(document).ready(function(){			
		jQuery('textarea').elastic();
		jQuery('textarea').trigger('update');
		
		$(function() {

			$('textarea').focus(function() {
				
			  var status = $('textarea').val();
			  $(this).val('');

			});

		 });
	});	
</script>


<script>
	$(function() {
				$("#btnSubmit").button({
					icons: {
						primary: "ui-icon-check",
					},
					text: true
				});
				
				$("#btnLike").button({
					icons: {
						primary: "ui-icon-heart",
					},
					text: true
				});
				$("#btnComment").button({
					icons: {
						primary: "ui-icon-comment",
					},
					text: true
				});
		
				$("#btnDelete{$i}").button({
					icons: {
						primary: "ui-icon-trash",
					},
					text: true
				});
				
				$("#btnLike").button({
					icons: {
						primary: "ui-icon-heart",
					},
					text: true
				});
				
				$("#btnComment").button({
					icons: {
						primary: "ui-icon-comment",
					},
					text: true
				});			
		
				$('#NewStatusForm').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
						$('#new_status').val('Apa yang Anda pikirkan?');
						$('#btnSend').hide(); 

						$('#result').load('social-network.php');	
						$('#result').show();				

							
						}
					})
					
					return false;
					
				});
				
				
				$('#new_status').click(function(){		
					$('#btnSend').show();
				});
				
				$('#btnSubmit').click(function(){		
					$('#NewStatusForm').submit();
				});
				
				
	});
</script>
{for $i=0 to $jml_status-1}

<div style="border-bottom-color:#eaeaea; border-bottom-style:solid; border-bottom-width:1px; padding-bottom:20px;">
	<form id="myStatus{$id_status[$i]}" name="myStatus{$id_status[$i]}" style="display:none;">
		<input type="text" id="id_status" name="id_status" value="{$id_status[$i]}" />
	</form>
	<form id="myStatusLike{$id_status[$i]}" name="myStatusLike{$id_status[$i]}" style="display:none;">
		<input type="text" id="id_status" name="id_status" value="{$id_status[$i]}" />
	</form>
	<form id="myStatusComment{$id_status[$i]}" name="myStatusComment{$id_status[$i]}" style="display:none;">
		<input type="text" id="id_status" name="id_status" value="{$id_status[$i]}" />
	</form>
			<table width="100%" class="ui-widget" border="0">
				<tr>
					<td>
						<div class="full-width-hack">
							<strong>{$nm_pengguna_status[$i]}</strong>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="full-width-hack">
							{$status[$i]}
						</div>
					</td>
				</tr>
				<!--
				<tr>
					<td class="ui-widget-content">
						<div style="float:left;" id="btnLike" name="btnLike">
							Suka
						</div>
						<div style="float:left;" id="btnComment" name="btnComment">
							Komentari
						</div>
					</td>
				</tr>
				<tr style="display:none;">
					<td class="ui-widget-content">
						Sani Iman Pribadi menyukai ini
					</td>
				</tr>
				-->
			</table>
</div>
{/for}