{foreach item="list0" from=$T_MHS}
<div class="panel" id="panel1" style="display: block">
<table width="100%" border="1" cellspacing="0" cellpadding="0">
           <tr>
             <td>NIM</td>
			 <td><center>:</center></td>
			 <td>{$list0.NIM_MHS}</td>
           <tr>			 
             <td>Nama Mahasiswa</td>
			 <td><center>:</center></td>
			 <td>{$list0.NM_PENGGUNA}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
           </tr>
           <tr>
             <td>Program Studi</td>
			 <td><center>:</center></td>
			 <td>{$list0.NM_JENJANG} {$list0.NM_PROGRAM_STUDI}</td>
           </tr>
</table>
{/foreach}
<table width="100%" border="1" cellspacing="0" cellpadding="0">
           <tr class="left_menu">
             <td width="3%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
             <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Smtr</font></td>
			 <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Kode MA</font></td>
			 <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Status</font></td>
			 <td width="40%" bgcolor="#333333"><font color="#FFFFFF">Nama MA</font></td>
             <td width="5%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
			 <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Nilai</font></td>
			 <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Bobot</font></td>
			 <td width="7%" bgcolor="#333333"><font color="#FFFFFF">P1</font></td>
			 <td width="7%" bgcolor="#333333"><font color="#FFFFFF">P2</font></td>
			 <td width="7%" bgcolor="#333333"><font color="#FFFFFF">P3</font></td>
			 <td width="7%" bgcolor="#333333"><font color="#FFFFFF">P4</font></td>
           </tr>
		   {foreach name=test item="list" from=$T_MK}
		{if $list.NILAI=='D' or $list.NILAI=='E'}
			{if $list.STATUS_HAPUS=='0'}
			<tr bgcolor="yellow">
			{else}
			<tr bgcolor="#772244">
			{/if}
		{else}
		{if $smarty.foreach.test.iteration%2==0}
			{if $list.STATUS_HAPUS=='0'}
           <tr bgcolor="#ffffff">
			{else}
			<tr bgcolor="#772244">
			{/if}		
		{else}
			{if $list.STATUS_HAPUS=='0'}
           <tr bgcolor="#cccccc">
			{else}		
			<tr bgcolor="#772244">
			{/if}		
	   {/if}
		{/if}
             <td >{$smarty.foreach.test.iteration}.</td>
		<td >{$list.SMTR}</td>
             <td >{$list.KODE}</td>
             <td >&nbsp;</td>
             <td >{$list.NAMA}</td>
             <td >{$list.SKS}</td>
             <td >{$list.NILAI}</td>
			 <td >{($list.BOBOT*$list.SKS)|number_format:2:".":","}</td>
		{if $list.P1 eq '- Nilai:'}
		<td >-</td>
		{else}
		<td >{$list.P1}</td>
		{/if}
		{if $list.P2 eq '- Nilai:'}
		<td >-</td>
		{else}
		<td >{$list.P2}</td>
		{/if}
		{if $list.P3 eq '- Nilai:'}
		<td >-</td>
		{else}
		<td >{$list.P3}</td>
		{/if}
		{if $list.P4 eq '- Nilai:'}
		<td >-</td>
		{else}
		<td >{$list.P4}</td>
		{/if}

           </tr>
            {foreachelse}
        <tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		{foreach item="footer" from=$T_IPK}
           <tr>
             <td colspan="4">&nbsp;</td>
             <td><b>Total SKS dan Bobot</b></td>
             <td><b>{$footer.SKS_TOTAL}</b></td>
			 <td>&nbsp;</td>
			 <td><b>{$footer.BOBOT_TOTAL}</b></td>
		<td colspan="4">&nbsp;</td>
           </tr>
           <tr>
             <td colspan="4">&nbsp;</td>
			 <td><b>Indeks Prestasi Kumulatif</b></td>
             <td colspan="3"><b>{$footer.IPK}</b></td>
		<td colspan="4">&nbsp;</td>
           </tr>
        {/foreach}
</table>
</div>
