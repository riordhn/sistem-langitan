<div class="center_title_bar">{$title}</div> 
<table>
    <tr class="ui-widget-header">
        <th colspan="5" class="header-coloumn">Materi Ujian</th>
    </tr>
    <tr>
        <th>Mata Kuliah</th>
        <th>Jenis Ujian</th>
        <th>Sifat</th>
        <th>Materi</th>
        <th>Keterangan</th>
    </tr>
    {foreach $data_materi_ujian as $materi_ujian}
        <tr>
            <td><span>{$materi_ujian['mata_kuliah']}</span></td>
            <td><span>{$materi_ujian['jenis']}</span></td>
            <td><span>{$materi_ujian['sifat']}</span></td>
            <td>
                <ul>
                    {foreach $materi_ujian['materi'] as $materi}
                        <li>{$materi}</li>
                    {/foreach}
                </ul>
            </td>
            <td><span>{$materi_ujian['keterangan']}</span></td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="5" class="center ui-state-error-text">Data Masih Kosong</td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="5" class="center"><span id="add" class="ui-button ui-corner-all ui-state-default" style="padding:5px;cursor:pointer;">Tambah Materi</span></td>
    </tr>
</table>
{literal}
    <script type="text/javascript">
    $('#add').click(function(){
        $('#center_content').load('materi_ujian.php',{mode:'add'})
    });
    </script>
{/literal}