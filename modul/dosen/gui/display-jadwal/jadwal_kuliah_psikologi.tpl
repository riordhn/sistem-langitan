{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">JADWAL KULIAH</div> 

{if isset($data_jadwal_kuliah_psikologi)}
    <table class="tablesorter ui-widget ui-widget-content" style="width: 98%" >
        <thead>
            <tr class="ui-widget-header">
                <th colspan="8" class="header-coloumn">Jadwal Perkuliahan</th>
            </tr>
            <tr>
                <th class="center">HARI</th>
                <th class="center">MATA KULIAH</th>
                <th class="center" width="50px">KELAS</th>
                <th class="center" width="100px">JAM</th>
                <th class="center">RUANG</th>
                <th class="center">PESERTA</th>
                <th class="center">PRODI</th>
            </tr>
        </thead>
        <tbody>

            {foreach $data_jadwal_kuliah_psikologi as $data}
                <tr>
                    <td>{$data.HARI|upper}</td>
                    <td>{$data.NMMK|upper}</td>
                    <td class="center">{$data.NM_KELAS}</td>
                    <td class="center">{$data.JAM}</td>
                    <td class="center">{$data.NM_RUANGAN}</td>
                    <td class="center">
                        <a href="jadwal_kuliah.php?mode=peserta_ma&id_kelas_mk={$data.ID_KELAS_MK}&nama_kelas={$data.NMMK|upper}">{$data.JUMLAH_MHS}</a>
                    </td>
                    <td class="center">{$data.PRODI}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="7" style="text-align:center" class="ui-state-error-text">Data Kosong</td>
                </tr>
            {/foreach}
        </tbody>
        <tfoot>
            <tr>
                <td colspan="7" class="center">
                    <span id="rooster_link" class="ui-button ui-state-default ui-corner-all" style="padding:5px;">Lihat Rooster</span>
                </td>
            </tr>
        </tfoot>
    </table>
{else if isset($data_peserta_ma)}
    <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" onclick="history.back(-1)" >Kembali</span>
    <span><a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="sms.php?view=peserta_ma&id_kelas_mk={$id_kelas_mk}&nama_kelas={$nama_kelas}" >Kirim SMS ke Mahasiswa Kelas {$nama_kelas}</a></span>
    <table class="ui-widget tablesorter ui-widget-content" style="width: 98%">
        <thead>
            <tr class="ui-widget-header">
                <th colspan="5" class="header-coloumn">Peserta Mata Kuliah</th>
            </tr>
            <tr>
                <th>No</th>
                <th>Foto</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Mobile</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_peserta_ma as $data}
                <tr>
                    <td>{$data@index+1}</td>
                    <td>
                        <img src="{$data.FOTO_MHS}" width="50" height="70"/>                        
                    </td>
                    <td>{$data.NIM_MHS}</td>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td><a href="sms.php?view=index&mhs={$data.NM_PENGGUNA}&mobile={$data.MOBILE_MHS}">{$data.MOBILE_MHS}</a></td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{/if}

{literal}
    <script type="text/javascript">
        $('td').addClass('ui-widget-content');
        $('#rooster_link').click(function() {
            window.open('{$base_url}modul/dashboard/roster/');
        });
    </script>
{/literal}

