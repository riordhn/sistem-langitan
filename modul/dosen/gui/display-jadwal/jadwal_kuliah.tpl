{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">JADWAL KULIAH</div> 

{if isset($data_jadwal_kuliah)}
    <a class="ui-button ui-state-active ui-corner-all disable-ajax" style="padding:5px;margin:5px" target="_blank" href="cetak-sk-mengajar.php">CETAK SK MENGAJAR</a>
    <table class="tablesorter ui-widget" style="width: 98%">
        <thead>
            <tr class="ui-widget-header">
                <th colspan="8" class="header-coloumn">Jadwal Perkuliahan</th>
            </tr>
            <tr>
                <th class="center">HARI</th>
                <th class="center">KODE AJAR</th>
                <th>MATA KULIAH</th>
                <th class="center" width="50px">KELAS</th>
                <th class="center" width="50px">JAM KE</th>
                <th class="center" width="100px">WAKTU</th>
                <th class="center">RUANG</th>
                <th class="center">Jumlah Mahasiswa</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_jadwal_kuliah as $data}
                <tr class="ui-widget-content">
                    <td>{$data.HARI|upper}</td>
                    <td>{$data.KD_MATA_KULIAH}</td>
                    <td>{$data.NM_MATA_KULIAH|upper}</td>
                    <td>{$data.NM_KELAS}</td>
                    <td class="center">{$data.JAM_KE}</td>
                    <td class="center">{if $data.JAM_MULAI!=''}{$data.JAM_MULAI}:{$data.MENIT_MULAI} -  {$data.JAM_SELESAI}:{$data.MENIT_SELESAI}{/if}</td>
                    <td>{$data.NM_RUANGAN}</td>
                    <td class="center">
                        <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="jadwal_kuliah.php?mode=peserta_ma&id_kelas_mk={$data.ID_KELAS_MK}&nama_kelas={$data.NM_MATA_KULIAH|upper}">{$data.JUMLAH_MHS}</a>
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="8" style="text-align:center" class="ui-state-error-text">Data Kosong</td>
                </tr>
            {/foreach}
        </tbody>
        <tfoot>
            <tr class="ui-widget-content">
                <td colspan="8" class="center">
                    <span id="rooster_link" class="ui-button ui-state-default ui-corner-all" style="padding:5px;">Lihat Rooster</span>
                </td>
            </tr>
        </tfoot>
    </table>
{else if isset($data_peserta_ma)}
    <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" onclick="history.back(-1)" >Kembali</span>
    {* <a  class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="sms.php?view=peserta_ma&id_kelas_mk={$id_kelas_mk}&nama_kelas={$nama_kelas}" >Kirim SMS ke Mahasiswa Kelas {$nama_kelas}</a> *}
    <table class="tablesorter ui-widget"  style="width: 98%">
        <thead>
            <tr class="ui-widget-header">
                <th colspan="7" class="header-coloumn">Peserta Mata Kuliah</th>
            </tr>
            <tr>
                <th>No</th>
                <th>Foto</th>
                <th>NIM</th>
                <th>Jenis Kelamin</th>
                <th>Nama</th>
                <th>HP</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_peserta_ma as $data}
                <tr class="ui-widget-content">
                    <td style="width:30px">{$data@index+1}</td>
                    <td class="center">
                        {if $data.FOTO_MHS!=''}
                            <img src="{$data.FOTO_MHS}" width="70"/>
                        {else}                   
                            <span style="color: red;font-size: 0.8em;text-align: center">Foto Belum Ada</span>
                        {/if}
                    </td>
                    <td>{$data.NIM_MHS}</td>
                    <td>{if $data.KELAMIN==1}Laki-Laki{else}Perempuan{/if}</td>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td class="center">
                        <b>{$data.MOBILE_MHS}</b>
                        <hr/>
                        <a class="ui-button ui-corner-all disable-ajax" style="background-color:green;color:white;font-weight:bold;padding:4px;cursor:pointer;margin:2px;" href="https://wa.me/+62{$data.MOBILE_MHS|substr:1}" target="_blank">KIRIM WA</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{/if}

{literal}
    <script type="text/javascript">
        $('#rooster_link').click(function() {
            window.open('/modul/dashboard/roster/');
        });
    </script>
{/literal}

