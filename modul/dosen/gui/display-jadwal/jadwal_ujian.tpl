<div class="center_title_bar">{$title}</div>
<div id="tabs" style="width:98%">
    <ul>
        <li><a class="disable-ajax" href="#tabs-uts">TIM Pengawas Ujian UTS</a></li>
        <li><a class="disable-ajax" href="#tabs-uas">TIM Pengawas Ujian UAS</a></li>
    </ul>
    <div id="tabs-uts">
        <table class="ui-widget" style="width: 100%">
            <tr class="ui-widget-header">
                <th colspan="8" class="header-coloumn">Jadwal Ujian UTS</th>
            </tr>
            <tr>
                <th width="5%">No</th>
                <th width="25%">Mata Kuliah</th>
                <th width="10%">Prodi</th>
				<th width="15%">Jenis Ujian</th>
                <th width="10%">Tanggal</th>
                <th width="15%">Jam</th>
                <th width="10%">Ruangan</th>
                <th width="10%">Pengawas</th>
            </tr>
            {foreach $jadwal_uts as $j}
			{if $j.TIM == 'YA'}
				<tr bgcolor="lightblue">
			{else}
                <tr>
			{/if}
                    <td align="center">{$j@index+1}</td>
                    <td>{$j.KD_MATA_KULIAH} - {$j.NM_MATA_KULIAH}<br/>({$j.SKS} SKS)</td>
                    <td align="center">{$j.PRODI}</td>
					<td align="center">{$j.NM_UJIAN_MK}</td>
                    <td align="center">{$j.TGL_UJIAN}</td>
                    <td align="center">{$j.JAM}</td>
                    <td align="center">{$j.NM_RUANGAN}</td>
                    <td align="center">{$j.TIM}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="8" class="center ui-state-error-text">Data Kosong</td>
                </tr>
            {/foreach}
        </table>
    </div>
    <div id="tabs-uas">
        <table class="ui-widget" style="width: 100%">
            <tr class="ui-widget-header">
                <th colspan="8" class="header-coloumn">Jadwal Ujian UAS</th>
            </tr>
            <tr>
                <th width="5%">No</th>
                <th width="25%">Mata Kuliah</th>
                <th width="10%">Prodi</th>
				<th width="15%">Jenis Ujian</th>
                <th width="10%">Tanggal</th>
                <th width="15%">Jam</th>
                <th width="10%">Ruangan</th>
                <th width="10%">Pengawas</th>
            </tr>
            {foreach $jadwal_uas as $j}
			{if $j.TIM == 'YA'}
				<tr bgcolor="lightblue">
			{else}
                <tr>
			{/if}
                    <td align="center">{$j@index+1}</td>
                    <td>{$j.KD_MATA_KULIAH} - {$j.NM_MATA_KULIAH}<br/>({$j.SKS} SKS)</td>
                    <td align="center">{$j.PRODI}</td>
					<td align="center">{$j.NM_UJIAN_MK}</td>
                    <td align="center">{$j.TGL_UJIAN}</td>
                    <td align="center">{$j.JAM}</td>
                    <td align="center">{$j.NM_RUANGAN}</td>
                    <td align="center">{$j.TIM}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="8" class="center ui-state-error-text">Data Kosong</td>
                </tr>
            {/foreach}
        </table>
    </div>
</div>
{literal}
    <script>
        $( "#tabs" ).tabs();
    </script>
{/literal}


