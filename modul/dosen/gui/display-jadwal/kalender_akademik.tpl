{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">KALENDER AKADEMIK</div> 
<table class="tablesorter ui-widget" style="width: 98%">
    <thead>
        <tr class="ui-widget-header">
            <th colspan="3" class="header-coloumn">
                Kalender Akademik Fakultas {$fakultas}<br/>
                {$data_kalender_fakultas[0].NM_SEMESTER}<br/>
                {$data_kalender_fakultas[0].TAHUN_AJARAN}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>WAKTU PERIODE</th>
            <th>KEGIATAN AKADEMIK</th>
            <th>DESKRIPSI</th>        
        </tr>
    </thead>
    <tbody>
        {foreach $data_kalender_fakultas as $kf}
            <tr class="ui-widget-content">
                <td style="width: 150px">{if $kf.TANGGAL_MULAI!=''||$kf.TANGGAL_SELESAI!=''}{$kf.TANGGAL_MULAI} - {$kf.TANGGAL_SELESAI}{else}<span class="error">Periode Kegiatan Masih Kosong</span>{/if}</td>
                <td>{$kf.KEGIATAN}</td>
                <td>{$kf.DESKRIPSI_KEGIATAN}</td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="3" class="center"><span class="ui-state-error-text">Data Masih Kosong</span></td>
            </tr>
        {/foreach}
    </tbody>
</table>
<p></p>
<table class="tablesorter ui-widget" style="width: 98%">
    <thead>
        <tr class="ui-widget-header">
            <th colspan="3" class="header-coloumn">Kalender Akademik Universitas</th>
        </tr>
        <tr class="ui-widget-header">
            <th>Semester</th>
            <th>Kegiatan Akademik</th>
            <th>Deskripsi Kegitatan</th>        
        </tr>
    </thead>
    <tbody>
        {foreach $data_kalender_universitas as $kalender_universitas}
            <tr class="ui-widget-content">
                <td><span>{$kalender_universitas['NM_SEMESTER']} ({$kalender_universitas['TAHUN_AJARAN']})</span></td>
                <td><span>{$kalender_universitas['KEGIATAN']}</span></td>
                <td><span>{$kalender_universitas['DESKRIPSI_KEGIATAN']}</span></td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="3" class="center"><span class="ui-state-error-text">Data Masih Kosong</span></td>
            </tr>

        {/foreach}
    </tbody>
</table>
