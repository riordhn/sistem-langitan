<div class="center_title_bar">{$title}</div> 
<form name="add_materi_ujian" id="add_materi_ujian" action="materi_ujian.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn"><h2>Add Materi Ujian</h2></th>
        </tr>
        <tr class="ui-widget-content">
            <td><span>Mata Kuliah</span></td>
            <td>
                <select name="mata_kuliah">
                {foreach $data_mata_kuliah as $mata_kuliah}
                    <option value="{$mata_kuliah['id_mata_kuliah']}">{$mata_kuliah['mata_kuliah']}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td><span>Jenis Ujian</span></td>
            <td><input type="text" name="jenis"/></td>
        </tr>
        <tr class="ui-widget-content">
            <td><span>Sifat</span></td>
            <td><input type="text" name="sifat"/></td>
        </tr>
        <tr class="ui-widget-content">
            <td><span>Materi</span></td>
            <td><textarea name="materi" rows="5" cols="30" title="Untuk menjadikan sebuah list bedakan materi dengan koma"></textarea></td>
        </tr>
        <tr class="ui-widget-content">
            <td><span>Keterangan</span></td>
            <td><textarea name="keterangan" rows="5" cols="30"></textarea></td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <span class="ui-button ui-corner-all ui-state-default" style="padding:5px;cursor:pointer;" id="save">Submit</span>
                <span class="ui-button ui-corner-all ui-state-default" style="padding:5px;cursor:pointer;" id="cancel">Cancel</span>
                <input type="hidden" name="mode" value="save"/>
            </td>
        </tr>
    </table>
</form>
{literal}
<script type="text/javascript">
$('#save').click(function(){
    $('#add_materi_ujian').submit();
})
$('#add_materi_ujian').ajaxForm({
    url:$(this).attr('action'),
    type:'post',
    data:$(this).serialize(),
    success:function(data){
        $('#center_content').html(data);
    }
})
$('#cancel').click(function(){
    $('#center_content').load('materi_ujian.php');
})
</script>
{/literal}