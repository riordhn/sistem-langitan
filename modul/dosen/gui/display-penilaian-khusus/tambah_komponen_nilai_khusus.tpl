<form id="form_tambah_komponen" method="post" action="komponen-khusus.php">
    <table class="ui-widget" width="90%">
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="2"><h2>Tambah Komponen Nilai Khusus</h2></th>
        </tr>
        <tr class="ui-widget-content">
            <td width="50%">Urutan Komponen</td>
            <td><input type="text" id="urutan" remote="/modul/dosen/cek-urutan-komponen.php?id_kelas_mk={$id_kelas}" name="urutan" class="required number"/></td>
        </tr>
        <tr class="ui-widget-content">
            <td>Tipe Komponen</td>
            <td>
                <select id="tipe_komponen">
                    <option value="">Pilih Tipe Komponen</option>
                    <option value="1">UAS/UTS</option>
                    <option value="2">Komponen Lain</option>
                </select>
            </td>
        </tr>
        <tr id="uts_uas" style="display: none" class="ui-widget-content">
            <td>Nama Komponen</td>
            <td>
                <select name="nama_utama">
                    <option value="UTS">UTS</option>
                    <option value="UAS">UAS</option>
                </select>
            </td>
        </tr>
        <tr id="lain" style="display: none" class="ui-widget-content">
            <td>Nama Komponen</td>
            <td><input type="text" id="nama" name="nama" class="required"/></td>
        </tr>
        <tr class="ui-widget-content">
            <td>Persentase Komponen</td>
            <td>
                <input type="text" id="persentase" remote="/modul/dosen/cek-total-komponen.php?id_kelas_mk={$id_kelas}" name="persentase" class="required number" />
                <label class="error" style="display: none" for="persentase">Tidak Boleh Kosong / Total Komponen Melebihi Maksimal</label>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="center" colspan="2">
                <input type="hidden" name="mode" value="tambah"/>
                <input type="hidden" name="id_kelas" value="{$id_kelas}"/>
                <input type="submit" class="ui-button ui-state-default ui-corner-all" style="padding:5px" value="Simpan"/>
                <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_tambah_komponen').dialog('close')">Batal</span>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_tambah_komponen').validate();
            $('#form_tambah_komponen').submit(function(){
            if($('#form_tambah_komponen').validate())
                    $('#dialog_tambah_komponen').dialog('close')
            });
            $('#tipe_komponen').change(function(){
                if($(this).val()==1){
                    $('#lain').hide();
                    $('#uts_uas').show();                    
                }else if($(this).val()==2){                    
                    $('#uts_uas').hide();
                    $('#lain').show();
                }else{
                    $('#uts_uas').hide();
                    $('#lain').hide();
                }
            })
    </script>
{/literal}