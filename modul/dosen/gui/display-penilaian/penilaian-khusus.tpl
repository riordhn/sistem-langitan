<div class="center_title_bar">Input Nilai Khusus</div>
<form id="form_select_mk" action="penilaian-khusus.php" method="get">
    <table>
        <tr width="100%">
            <td style="vertical-align:middle"><span>Mata Kuliah</span></td>
            <td>
                <select id="select_kelas_mk" name="id_kelas" onchange="$('#form_select_mk').submit()">
                    <option>Pilih</option>
                    {foreach $data_kelas_mk as $data}
                        <optgroup label="{$data.NM_SEMESTER} {$data.GROUP_SEMESTER} ({$data.TAHUN_AJARAN})">
                            {foreach $data.DATA_KELAS_MK as $kelas}
                                {$id_kelas_mk=array($kelas.ID_KELAS_MK,$kelas.ID_SEMESTER)}
                                {$im_id_kelas_mk=implode('_',$id_kelas_mk)}
                                <option value="{$im_id_kelas_mk}" 
                                        {if $im_id_kelas_mk==$smarty.get.id_kelas}
                                            selected="true"
                                        {/if}
                                        >{$kelas.NM_MATA_KULIAH} ({$kelas.KD_MATA_KULIAH}) Kelas {$kelas.NM_KELAS}  Prodi  {$kelas.NM_PROGRAM_STUDI} Semester {$kelas.NM_SEMESTER} {$kelas.TAHUN_AJARAN} {$kelas.GROUP_SEMESTER}
                                </option>
                            {/foreach}
                        {/foreach}
                    <input type="hidden" name="mode" value="load_mahasiswa"/>
                </select>
            </td>
        </tr>
    </table>
</form>
{if $smarty.get.mode=='load_mahasiswa'}
    {$kode_kelas=explode('_',$smarty.get.id_kelas)}
    {if $semester.TIPE_SEMESTER=='SP'}
        {if $pjmk_status==1&&($data_kelas_mk_detail.IS_DIBUKA==1||$cek_sem_sp_aktif)}
            {$info_pjma}
            <form action="penilaian-khusus.php?{$smarty.server.QUERY_STRING}" method="post">
                <table style="width:100%">
                    <tr class="ui-widget-header">
                        <th class="header-coloumn" colspan="3">Akses Komponen Nilai Dosen Semester Pendek</th>
                    </tr>
                    <tr>
                        <th class="center">Nama Dosen</th>
                        <th class="center">Akses Komponen Nilai</th>
                        <th class="center">Operasi</th>
                    </tr>
                    {if count($data_akses_dosen_mk)>0}
                        {foreach $data_akses_dosen_mk as $data}
                            <tr>
                                <td>{$data.NM_PENGGUNA}</td>
                                <td>{$data.NM_KOMPONEN_MK}</td>
                                <td class="center">
                                    <span style="padding:5px" class="ui-button ui-state-default ui-corner-all" onclick="open_dialog_delete_akses({$data.ID_AKSES_KOMPONEN_MK})">Delete</span>
                                </td>
                            </tr> 
                            <tr class="ui-widget-header">
                                <td colspan="3" class="center">TAMBAH AKES KOMPONEN</td>
                            </tr>
                            <tr>
                                <td colspan="3" class="center">
                                    <select name="id_dosen">
                                        {foreach $data_dosen_mk as $data}
                                            <option value="{$data.ID_DOSEN}">{$data.NM_PENGGUNA}</option>
                                        {/foreach}
                                    </select>
                                    <select name="id_komponen_mk">
                                        {foreach $data_komponen_sp as $data}
                                            <option value="{$data.ID_KOMPONEN_MK}">{$data.NM_KOMPONEN_MK}</option>
                                        {/foreach}
                                    </select>
                                    <br/><br/>
                                    <input type="hidden" name="id_semester" value="{$data_dosen_mk[0].ID_SEMESTER}"/>
                                    <input type="hidden" name="id_kelas_mk" value="{$id_kelas_mk}" />
                                    <input type="hidden" name="mode" value="tambah_akses"/>
                                    <input style="padding:5px" class="ui-button ui-state-default ui-corner-all"  type="submit" value="Tambah"/>
                                </td>
                            </tr>
                        {/foreach}
                    {else}
                        <tr>
                            <td colspan="3" class="center kosong">Akses Komponen Kosong</td>
                        </tr>
                    {/if}

                </table>
            </form>
            <div id="dialog_delete_akses" title="Delete Akses">
                <p class="center">Apakah Anda Yakin untuk menghapus Akses Dosen ini?<br/><br/>
                    <span style="padding:5px" class="ui-button ui-state-default ui-corner-all" onclick="delete_akses_komponen('penilaian-khusus.php?{$smarty.server.QUERY_STRING}')">Ya</span>
                    <span style="padding:5px" class="ui-button ui-state-default ui-corner-all" onclick="$('#dialog_delete_akses').dialog('close')">Tidak</span>
                </p>
            </div>

            <table class="ui-widget-content" style="width: 100%">
                <tr class="ui-widget-header">
                    <th colspan="4" class="header-coloumn">Komponen Nilai Semester Pendek</th>
                </tr>
                <tr>
                    <th>URUTAN KOMPONEN</th>
                    <th>NAMA KOMPONEN</th>
                    <th>PROSEN</th>
                    <th>OPERASI</th>
                </tr>
                {foreach $data_komponen_sp as $dk}
                    <tr>
                        <td class="center">{$dk.URUTAN_KOMPONEN_MK}</td>
                        <td>{$dk.NM_KOMPONEN_MK}</td>
                        <td>{$dk.PERSENTASE_KOMPONEN_MK} %</td>
                        <td style="width: 220px" class="center">
                            <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_edit_komponen').dialog('open').load('komponen_nilai_sp.php?mode=edit&id_komponen={$dk.ID_KOMPONEN_MK}&id_kelas={$kode_kelas[0]}&id_semester={$kode_kelas[1]}')">Edit</span>
                            <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_delete_komponen').dialog('open').load('komponen_nilai_sp.php?mode=delete&id_komponen={$dk.ID_KOMPONEN_MK}&id_kelas={$kode_kelas[0]}&id_semester={$kode_kelas[1]}')">Delete</span>
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="4" class="kosong">Data Komponen Kosong</td>
                    </tr>
                {/foreach}
                <tr>
                    <td colspan="4" class="center">
                        <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_tambah_komponen').dialog('open').load('komponen_nilai_sp.php?mode=tambah&id_kelas={$kode_kelas[0]}&id_semester={$kode_kelas[1]}')">Tambah</span>
                    </td>
                </tr>
            </table>
        {/if}
        {if $data_kelas_mk_detail.IS_DIBUKA==1}
            {$info}    
        {/if}
    {else if $semester.TIPE_SEMESTER=='UP'}
        {*UNTUK SEMESTER UP PENAMBAHAN KOMPONEN*}
        <table class="ui-widget-content" style="width: 100%">
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn">Komponen Nilai Semester Ujian Perbaikan</th>
            </tr>
            <tr>
                <th>NAMA KOMPONEN</th>
                <th>PROSEN</th>
                <th>OPERASI</th>
            </tr>
            {foreach $data_komponen_up as $dk}
                <tr>
                    <td>{$dk.NM_KOMPONEN_UP}</td>
                    <td>{$dk.PROSENTASE_KOMPONEN} %</td>
                    <td style="width: 220px" class="center">
                        <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_edit_komponen').dialog('open').load('komponen_nilai_up.php?mode=edit&id_komponen={$dk.ID_KOMPONEN_UP}&id_kelas={$kode_kelas[0]}&id_semester={$kode_kelas[1]}')">Edit</span>
                        <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_delete_komponen').dialog('open').load('komponen_nilai_up.php?mode=delete&id_komponen={$dk.ID_KOMPONEN_UP}&id_kelas={$kode_kelas[0]}&id_semester={$kode_kelas[1]}')">Delete</span>
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="4" class="kosong">Data Komponen Kosong</td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="4" class="center">
                    <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_tambah_komponen').dialog('open').load('komponen_nilai_up.php?mode=tambah&id_kelas={$kode_kelas[0]}&id_semester={$kode_kelas[1]}')">Tambah</span>
                </td>
            </tr>
        </table>
    {/if}
    {if count($data_komponen_sp)==0 && count($data_komponen_up)==0}
        {*JIKA KOMPONEN UP DAN SP SAMA-SAMA KOSONG*}
        <form id="form_entry_nilai" action="penilaian-khusus.php?{$smarty.server.QUERY_STRING}" method="post">
            <table width="100%">
                <tr class="ui-widget-header">
                    <th class="header-coloumn" colspan="6">Input Nilai Mahasiswa</th>
                </tr>
                <tr>
                    <th>
                        No
                    </th>
                    <th class="center">
                        NIM
                    </th>
                    <th class="center">
                        Nama
                    </th>
                    <th class="center">
                        Nilai Angka Akhir
                    </th>
                    <th class="center">
                        Nilai Huruf Akhir
                    </th>
                    <th class="center">
                        Tampilkan Ke Mahasiswa
                    </th>
                </tr>
                {$index_nilai=1}
                {foreach $data_mahasiswa as $mhs}
                    <tr> 
                        <td>{$mhs@index+1}</td>
                        <td>{$mhs.nim}</td>
                        <td>{$mhs.nama|upper}</td>
                        <td class="center">
                            {if ($semester.TIPE_SEMESTER!='SP')||($data_kelas_mk_detail.IS_DIBUKA==1||$cek_sem_sp_aktif)}
                                <input type="text" class="nilai" maxlength="5" size="5" name="nilai{$index_nilai}" value="{if $mhs.nilai_angka_akhir!=''}{$mhs.nilai_angka_akhir}{else}0{/if}" />
                                <br/><label for="nilai{$index_nilai}" class="error" style="display: none">Harus Angka dan Nilai tidak Lebih dari 100</label>
                                <input type="hidden" name="id_pmk{$index_nilai}" value="{$mhs.id_pengambilan_mk}" />
                            {else}
                                {if $mhs.nilai_angka_akhir!=''}
                                    {$mhs.nilai_angka_akhir}
                                {else}
                                    0
                                {/if}
                            {/if}
                        </td>
                        <td class="center">
                            {if $mhs.nilai_huruf_akhir!=''}
                                {$mhs.nilai_huruf_akhir}
                            {else}
                                Kosong
                            {/if}
                        </td>
                        <td class="center">
                            {if $mhs.flagnilai==0} 
                                Belum Tampil
                            {else}
                                Tampil
                            {/if}
                        </td>
                    </tr>
                    {$index_nilai=$index_nilai+1}
                {foreachelse}
                    <tr>
                        <td colspan="{$count_data_komponen_mk+6}" class="kosong center">Data Kosong</td>
                    </tr>
                {/foreach}
                <tr>
                    <td class="center" colspan="{$count_data_komponen_mk+6}">
                        <input type="hidden" name="jumlah_komponen" value="{count($data_komponen_sp)}"/>
                        <input type="hidden" name="total_nilai" value="{$index_nilai}" />
                        <input type="hidden" name="mode" value="save-nilai" />
                        <input type="hidden" name="id_kelas_mk" value="{$smarty.get.id_kelas_mk}" />
                        <input style="padding:5px" class="ui-button ui-state-default ui-corner-all"  type="submit" value="Save Nilai"/>
                    </td>
                </tr>			
            </table>
        </form>
    {else if count($data_komponen_sp)>0}
        {*JIKA KOMPONEN  SP TIDAK KOSONG*}
        <form id="form_entry_nilai" action="penilaian-khusus.php?{$smarty.server.QUERY_STRING}" method="post">
            <table width="100%">
                <tr class="ui-widget-header">
                    <th class="header-coloumn" colspan="{count($data_komponen_sp)+6}">Input Nilai Mahasiswa</th>
                </tr>
                <tr>
                    <th>
                        No
                    </th>
                    <th class="center">
                        NIM
                    </th>
                    <th class="center">
                        Nama
                    </th>
                    {foreach $data_komponen_sp as $dk}
                        <th class="center">
                            {$dk.NM_KOMPONEN_MK}<br/>
                            ({$dk.PERSENTASE_KOMPONEN_MK})
                        </th>
                    {/foreach}
                    <th class="center">
                        Nilai Angka Akhir
                    </th>
                    <th class="center">
                        Nilai Huruf Akhir
                    </th>
                    <th class="center">
                        Tampilkan Ke Mahasiswa
                    </th>
                </tr>
                {$index_nilai=1}
                {foreach $data_mahasiswa as $mhs}
                    <tr> 
                        <td>{$mhs@index+1}</td>
                        <td>{$mhs.nim}</td>
                        <td>{$mhs.nama|upper}</td>
                        {foreach $mhs.data_nilai as $n}
                            <td class="center">
                                {if $data_kelas_mk_detail.IS_DIBUKA==1||$cek_sem_sp_aktif}
                                    <input type="text" class="nilai" maxlength="5" size="5" name="nilai{$index_nilai}" {if $n.BESAR_NILAI_MK==''} value="0" {else} value="{$n.BESAR_NILAI_MK}" {/if} />
                                    <input type="hidden" name="id_nilai{$index_nilai}" value="{$n.ID_NILAI_MK}" />
                                    <br/><label for="nilai{$index_nilai}" class="error" style="display: none;font-size: 0.8em">Harus Angka dan Nilai tidak Lebih dari 100</label>
                                {else}
                                    {if $n.BESAR_NILAI_MK==''} 
                                        0 
                                    {else} 
                                        {$n.BESAR_NILAI_MK}
                                    {/if}
                                {/if}
                            </td>
                            {$index_nilai=$index_nilai+1}
                        {/foreach}
                        <td class="center">
                            {if $mhs.nilai_angka_akhir!=''}
                                {$mhs.nilai_angka_akhir}
                            {else}
                                0
                            {/if}
                        </td>
                        <td class="center">
                            {if $mhs.nilai_huruf_akhir!=''}
                                {$mhs.nilai_huruf_akhir}
                            {else}
                                Kosong
                            {/if}
                        </td>
                        <td class="center">
                            {if $mhs.flagnilai==0} 
                                Belum Tampil
                            {else}
                                Tampil
                            {/if}
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="{count($data_komponen_sp)+6}" class="kosong center">Data Kosong</td>
                    </tr>
                {/foreach}
                <tr>
                    <td class="center" colspan="{count($data_komponen_sp)+6}">
                        <input type="hidden" name="jumlah_komponen" value="{count($data_komponen_sp)}"/>
                        <input type="hidden" name="total_nilai" value="{$index_nilai}" />
                        <input type="hidden" name="mode" value="save-nilai" />
                        <input type="hidden" name="id_kelas_mk" value="{$smarty.get.id_kelas_mk}" />
                        {if $data_kelas_mk_detail.IS_DIBUKA==1||$cek_sem_sp_aktif}
                            <input style="padding:5px" class="ui-button ui-state-default ui-corner-all"  type="submit" value="Save Nilai"/>
                        {/if}
                    </td>
                </tr>			
            </table>
        </form>
    {else if count($data_komponen_up)>0}
        {*JIKA KOMPONEN UP TIDAK KOSONG*}
        <form id="form_entry_nilai" action="penilaian-khusus.php?{$smarty.server.QUERY_STRING}" method="post">
            <table width="100%">
                <tr class="ui-widget-header">
                    <th class="header-coloumn" colspan="{count($data_komponen_up)+6}">Input Nilai Mahasiswa</th>
                </tr>
                <tr>
                    <th>
                        No
                    </th>
                    <th class="center">
                        NIM
                    </th>
                    <th class="center">
                        Nama
                    </th>
                    {foreach $data_komponen_up as $dk}
                        <th class="center">
                            {$dk.NM_KOMPONEN_UP}<br/>
                            ({$dk.PROSENTASE_KOMPONEN})
                        </th>
                    {/foreach}
                    <th class="center">
                        Nilai Angka Akhir
                    </th>
                    <th class="center">
                        Nilai Huruf Akhir
                    </th>
                    <th class="center">
                        Tampilkan Ke Mahasiswa
                    </th>
                </tr>
                {$index_nilai=1}
                {foreach $data_mahasiswa as $mhs}
                    <tr> 
                        <td>{$mhs@index+1}</td>
                        <td>{$mhs.nim}</td>
                        <td>{$mhs.nama|upper}</td>
                        {foreach $mhs.data_nilai as $n}
                            <td class="center">
                                <input type="text" class="nilai" maxlength="5" size="5" name="nilai{$index_nilai}" {if $n.BESAR_NILAI==''} value="0" {else} value="{$n.BESAR_NILAI}" {/if} />
                                <input type="hidden" name="id_nilai{$index_nilai}" value="{$n.ID_NILAI_KOMPONEN_UP}" />
                                <br/><label for="nilai{$index_nilai}" class="error" style="display: none;font-size: 0.8em">Harus Angka dan Nilai tidak Lebih dari 100</label>
                            </td>
                            {$index_nilai=$index_nilai+1}
                        {/foreach}
                        <td class="center">
                            {if $mhs.nilai_angka_akhir!=''}
                                {$mhs.nilai_angka_akhir}
                            {else}
                                0
                            {/if}
                        </td>
                        <td class="center">
                            {if $mhs.nilai_huruf_akhir!=''}
                                {$mhs.nilai_huruf_akhir}
                            {else}
                                Kosong
                            {/if}
                        </td>
                        <td class="center">
                            {if $mhs.flagnilai==0} 
                                Belum Tampil
                            {else}
                                Tampil
                            {/if}
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="{count($data_komponen_up)+6}" class="kosong center">Data Kosong</td>
                    </tr>
                {/foreach}
                <tr>
                    <td class="center" colspan="{count($data_komponen_up)+6}">
                        <input type="hidden" name="jumlah_komponen" value="{count($data_komponen_up)}"/>
                        <input type="hidden" name="tipe_penilaian" value="up" />
                        <input type="hidden" name="total_nilai" value="{$index_nilai}" />
                        <input type="hidden" name="mode" value="save-nilai" />
                        <input type="hidden" name="id_kelas_mk" value="{$smarty.get.id_kelas_mk}" />
                        <input style="padding:5px" class="ui-button ui-state-default ui-corner-all"  type="submit" value="Save Nilai"/>
                    </td>
                </tr>			
            </table>
        </form>
    {/if}
    <table style="width: 100%">
        <tr>
            <td class="center">
                <form action="penilaian-khusus.php?{$smarty.server.QUERY_STRING}" method="post">
                    <input type="hidden" name="mode" value="tampil" />
                    <input type="hidden" name="id_kelas_mk" value="{$smarty.get.id_kelas_mk}" />
                    {if $pjmk_status==1}
                        <input style="padding:5px" class="ui-button ui-state-default ui-corner-all"  type="submit" value="Tampilkan"/>
                    {else}
                        <span style="padding:5px" class="ui-button ui-state-active ui-corner-all" onclick="$('#dialog_generate_button').dialog('open')">Tampilkan Ke Mahasiswa</span>
                    {/if}
                    <a style="padding:5px" target="_blank" class="disable-ajax ui-button ui-state-default ui-corner-all" href="cetak-nilai-khusus.php?id_kelas={$smarty.get.id_kelas}">Cetak Nilai</a>
                </form>
            </td>
        </tr>
    </table>
{/if}
<div id="dialog_generate_button" title="Konfirmasi">
    <p class="center ui-state-active" style="padding: 10px">Hanya PJMA yang bisa generate nilai akhir Mahasiswa.</p>
    <p class="center">
        <span style="padding:5px" class="center ui-button ui-state-default ui-corner-all" onclick="$('#dialog_generate_button').dialog('close')">Close</span>
    </p>
</div>
<!--    <span  style="padding:8px;font-size:13px;margin:5px;" class="center ui-button ui-state-error ui-corner-all">Halaman Masih Belum Tersedia</span>-->
<div id="dialog_tambah_komponen" title="Tambah Komponen Nilai"></div>
<div id="dialog_edit_komponen" title="Edit Komponen Nilai"></div>
<div id="dialog_delete_komponen" title="Delete Komponen Nilai"></div>
{literal}
    <script type="text/javascript">
        var id_akses = 0;
        function open_dialog_delete_akses(id) {
            $('#dialog_delete_akses').dialog('open');
            id_akses = id;
        }
        function delete_akses_komponen(url) {
            $.ajax({
                url: url,
                type: 'post',
                data: 'mode=delete_akses&id_akses=' + id_akses + '&id_kelas_mk=' + $('#select_kelas_mk').val(),
                beforeSend: function() {
                    $('#content').html('<div style="width: 100%;" align="center"><img src="/img/dosen/ajax_loader.gif" /></div>');
                },
                success: function(data) {
                    $('#content').html(data);
                }
            });
            $('#dialog_delete_akses').dialog('close');
        }
        $("#dialog_generate_button").dialog({
            width: '300',
            modal: true,
            resizable: false,
            autoOpen: false
        });
        $('#form_entry_nilai').validate();
        $(".nilai").each(function() {
            $(this).rules("add", {
                required: true,
                max: 100,
                number: true
            });
        });
        $("#dialog:ui-dialog").dialog("destroy");
        $("#dialog_tambah_komponen").dialog({
            width: '500',
            modal: true,
            resizable: false,
            autoOpen: false
        });
        $("#dialog_edit_komponen").dialog({
            width: '500',
            modal: true,
            resizable: false,
            autoOpen: false
        });
        $("#dialog_delete_komponen").dialog({
            width: '420',
            modal: true,
            resizable: false,
            autoOpen: false
        });
        $("#dialog_delete_akses").dialog({
            width: '300',
            modal: true,
            resizable: false,
            autoOpen: false
        });
    </script>
{/literal}