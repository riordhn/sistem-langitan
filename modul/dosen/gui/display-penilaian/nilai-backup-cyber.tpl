<div class="center_title_bar">Input Nilai</div>

<!--UNTUK PENUTUPAN PENILAIAN-->
{if $tutup!=''}
    {$tutup}
{else}
    <form id="form_select_mk" action="penilaian.php" method="get">
        <table>
            <tr width="100%">
                <td style="vertical-align:middle"><span>Mata Kuliah</span></td>
                <td>
                    <select name="id_kelas" onchange="$('#form_select_mk').submit()">
                        <option>Pilih</option>
                        {foreach $data_kelas_mk as $data}
                            <optgroup label="{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})">
                                {foreach $data.DATA_KELAS_MK as $kelas}
                                    <option value="{$kelas.ID_KELAS_MK}"{if $kelas.ID_KELAS_MK==$id_kelas_mk}selected="true"{/if}>
                                        {$kelas.NM_MATA_KULIAH} ({$kelas.KD_MATA_KULIAH}) Kelas {$kelas.NM_KELAS} <br/> Prodi  {$kelas.NM_PROGRAM_STUDI}
                                        {if $kelas.PJMK_PENGAMPU_MK==1}
                                    (PJMK)
                                {else}
                                    (Anggota)
                                {/if}
                                </option>
                            {/foreach}
                        {/foreach}
                    </select>
                    <input type="hidden" name="mode" value="load_penilaian"/>
                </td>
            </tr>
        </table>
    </form>
    <div id="table_penilaian">
        {if isset($data_mahasiswa)}
            <table style="width:80%">
                <tr class="ui-widget-header">
                    <th class="header-coloumn" colspan="3">Daftar Pengajar Mata Kuliah</th>
                </tr>
                <tr>
                    <th>No</th>
                    <th>Pengajar</th>
                    <th>Status</th>
                </tr>
                {foreach $data_pengajar_mk as $data}
                    <tr>
                        <td>{$data@index+1}</td>
                        <td>{$data.NM_PENGGUNA}</td>
                        <td>
                            {if $data.PJMK_PENGAMPU_MK==1}
                                PJMK
                            {else}
                                Anggota
                            {/if}
                        </td>
                    </tr>
                {/foreach}
            </table>
            <!--Jika Semester Bukan Pada Semester Aktif dan Mata Kuliah Tidak dalam Kondisi Di Buka $data_kelas_mk_detail.IS_DIBUKA=='0'&&$semester_aktif!=$data_kelas_mk_detail.ID_SEMESTER-->
            {*{if ($data_kelas_mk_detail.IS_DIBUKA=='0'&&$semester_aktif!=$data_kelas_mk_detail.ID_SEMESTER) or ($data_kelas_mk_detail.IS_DIBUKA=='0'&&$id_fakultas==4)}*}
            {if ($data_kelas_mk_detail.IS_DIBUKA=='0')}

                <table  width="100%">
                    <tr class="ui-widget-header">
                        <th class="header-coloumn" colspan="{$count_data_komponen_mk+6}">Data Nilai Mahasiswa</th>
                    </tr>
                    <tr>
                        <td class="center" colspan="{$count_data_komponen_mk+6}">
                            <a style="padding:5px;" class="disable-ajax ui-button ui-state-default ui-corner-all" href="/modul/dosen/cetak-nilai.php?id_kelas_mk={$id_kelas_mk}&semester={$data_kelas_mk_detail.ID_SEMESTER}" title="Hasil Nilai PDF" target="_blank">
                                <span style="float: left;margin: 0px 3px" class="ui-icon ui-icon-print">&nbsp;</span>Format PDF
                            </a>
                            <a style="padding:5px" class="disable-ajax ui-button ui-state-default ui-corner-all" href="/modul/dosen/excel-hasil-nilai.php?id_kelas_mk={$id_kelas_mk}&semester={$data_kelas_mk_detail.ID_SEMESTER}" title="Hasil Nilai Excel"  target="_blank">
                                <span style="float: left;margin: 0px 3px" class="ui-icon ui-icon-arrowstop-1-s">&nbsp;</span>Format Excel
                            </a>
                            <br/><br/>
                            <span style="padding:5px" class="ui-button ui-state-default ui-corner-all" onclick="window.location.reload()">
                                <span style="float: left;margin: 0px 3px" class="ui-icon ui-icon-arrowrefresh-1-n">&nbsp;</span>Reload
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="header-coloumn" colspan="{$count_data_komponen_mk+6}">
                            <p style="text-transform: uppercase">
                                Baris Berwarna <span style="color: lightcoral">Merah</span> = Status Mahasiswa Cekal
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            No
                        </th>
                        <th class="center">
                            NIM
                        </th>
                        <th class="center">
                            Nama
                        </th>
                        {foreach $data_komponen_mk as $data}
                            <th class="center">
                                {$data.NM_KOMPONEN_MK} <br/>({$data.PERSENTASE_KOMPONEN_MK}%)
                            </th>
                        {/foreach}
                        <th class="center">
                            Nilai Angka Akhir
                        </th>
                        <th class="center">
                            Nilai Huruf Akhir
                        </th>
                        <th class="center">
                            Tampilkan Ke Mahasiswa
                        </th>
                    </tr>
                    {$index_nilai=1}
                    {foreach $data_mahasiswa as $mhs}
                        <tr> 
                            <td>{$mhs@index+1}</td>
                            <td>{$mhs.nim}</td>
                            <td>{$mhs.nama|upper}</td>
                            {foreach $mhs.data_nilai as $nilai}
                                <td {if ($mhs.status_cekal==0||$mhs.status_cekal==2)}style="background-color: lightcoral" {/if} class="center">
                                    {$nilai.BESAR_NILAI_MK}
                                </td>
                            {/foreach}
                            <td class="center">{if $mhs.nilai_angka_akhir!=''}{$mhs.nilai_angka_akhir}{else}Kosong{/if}</td>
                            <td class="center">{if $mhs.nilai_huruf_akhir!=''}{$mhs.nilai_huruf_akhir}{else}Kosong{/if}</td>
                            <td class="center">
                                {if $mhs.flagnilai==0} 
                                    <span style="padding:5px 7px" class="ui-button ui-state-default ui-corner-all" title="Belum Di Tampilkan Ke Akun Mahasiswa">
                                        <span style="float: left" class="ui-icon ui-icon-closethick">&nbsp;</span>Belum
                                    </span>
                                {else}
                                    <span style="padding:5px 7px" class="ui-button ui-state-default ui-corner-all" title="Sudah Di Tampilkan Ke Akun Mahasiswa">
                                        <span style="float: left" class="ui-icon ui-icon-check">&nbsp;</span>Tampil
                                    </span>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                </table>
                <br/>
                <br/>
                <div style="font-size: 10px; text-align: center">Exe time : {$exe_time} secs</div>
            {else}
                <!--Jika Dalam Kondisi Pada Semester Aktif atau Kelas Sedang Di Buka-->
                {if $pjmk_status==1}
                    <form action="penilaian.php?{$smarty.server.QUERY_STRING}" method="post">
                        <table style="width:80%">
                            <tr class="ui-widget-header">
                                <th class="header-coloumn" colspan="3">Akses Komponen Nilai Dosen</th>
                            </tr>
                            <tr>
                                <th class="center">Nama Dosen</th>
                                <th class="center">Akses Komponen Nilai</th>
                                <th class="center">Operasi</th>
                            </tr>
                            {foreach $data_akses_dosen_mk as $data}
                                <tr>
                                    <td>{$data.NM_PENGGUNA}</td>
                                    <td>{$data.NM_KOMPONEN_MK}</td>
                                    <td class="center">
                                        <span style="padding:5px" class="ui-button ui-state-default ui-corner-all" onclick="open_dialog_delete_akses({$data.ID_AKSES_KOMPONEN_MK})">Delete</span>
                                    </td>
                                </tr>
                            {foreachelse}
                                <tr>
                                    <td colspan="3" class="center kosong">Akses Komponen Kosong</td>
                                </tr>
                            {/foreach}
                            <tr class="ui-widget-header">
                                <td colspan="3" class="center">TAMBAH AKES KOMPONEN</td>
                            </tr>
                            <tr>
                                <td colspan="3" class="center">
                                    <select name="id_dosen">
                                        {foreach $data_dosen_mk as $data}
                                            <option value="{$data.ID_DOSEN}">{$data.NM_PENGGUNA}</option>
                                        {/foreach}
                                    </select>
                                    <select name="id_komponen_mk">
                                        {foreach $data_komponen_mk as $data}
                                            <option value="{$data.ID_KOMPONEN_MK}">{$data.NM_KOMPONEN_MK}</option>
                                        {/foreach}
                                    </select>
                                    <br/><br/>
                                    <input type="hidden" name="id_semester" value="{$data_dosen_mk[0].ID_SEMESTER}"/>
                                    <input type="hidden" name="id_kelas_mk" value="{$id_kelas_mk}" />
                                    <input type="hidden" name="mode" value="tambah_akses"/>
                                    <input style="padding:5px" class="ui-button ui-state-default ui-corner-all"  type="submit" value="Tambah"/>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <div id="dialog_delete_akses" title="Delete Akses">
                        <p class="center">Apakah Anda Yakin untuk menghapus Akses Dosen ini?<br/><br/>
                            <span style="padding:5px" class="ui-button ui-state-default ui-corner-all" onclick="delete_akses_komponen('penilaian.php?{$smarty.server.QUERY_STRING}')">Ya</span>
                            <span style="padding:5px" class="ui-button ui-state-default ui-corner-all" onclick="$('#dialog_delete_akses').dialog('close')">Tidak</span>
                        </p>
                    </div>

                {/if}
                <form id="form_entry_nilai" action="penilaian.php?{$smarty.server.QUERY_STRING}" method="post">
                    <table  width="100%">
                        <tr class="ui-widget-header">
                            <th class="header-coloumn" colspan="{$count_data_komponen_mk+6}">Input Nilai Mahasiswa</th>
                        </tr>
                        <tr>
                            <td  class="center" colspan="{$count_data_komponen_mk+6}">
                                <div id="upload_nilai">
                                    Upload File Excel Penilaian
                                    <iframe src="/modul/dosen/upload-nilai.php?id_kelas_mk={$id_kelas_mk}" width="100%" height="120" frameborder="0"></iframe>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="center" colspan="{$count_data_komponen_mk+6}">
                                <a style="padding:5px;" class="disable-ajax ui-button ui-state-default ui-corner-all" href="/modul/dosen/cetak-nilai.php?id_kelas_mk={$id_kelas_mk}&semester={$data_kelas_mk_detail.ID_SEMESTER}" title="Hasil Nilai PDF" target="_blank">
                                    <span style="float: left;margin: 0px 3px" class="ui-icon ui-icon-print">&nbsp;</span>Format PDF
                                </a>
                                <a style="padding:5px" class="disable-ajax ui-button ui-state-default ui-corner-all" href="/modul/dosen/excel-hasil-nilai.php?id_kelas_mk={$id_kelas_mk}&semester={$data_kelas_mk_detail.ID_SEMESTER}" title="Hasil Nilai Excel"  target="_blank">
                                    <span style="float: left;margin: 0px 3px" class="ui-icon ui-icon-arrowstop-1-s">&nbsp;</span>Format Excel
                                </a>
                                <br/><br/>
                                <a style="padding:5px" class="disable-ajax ui-button ui-state-default ui-corner-all" href="/modul/dosen/excel-nilai.php?id_kelas_mk={$id_kelas_mk}&semester={$data_kelas_mk_detail.ID_SEMESTER}">
                                    <span style="float: left;margin: 0px 3px" class="ui-icon ui-icon-note">&nbsp;</span>Download Template Excel
                                </a>
                                <br/><br/>
                                <span style="padding:5px" class="ui-button ui-state-default ui-corner-all" onclick="window.location.reload()">
                                    <span style="float: left;margin: 0px 3px" class="ui-icon ui-icon-arrowrefresh-1-n">&nbsp;</span>Reload
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="header-coloumn" colspan="{$count_data_komponen_mk+6}">
                                <p style="text-transform: uppercase">
                                    Baris Berwarna <span style="color: lightcoral">Merah</span> = Status Mahasiswa Cekal
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                No
                            </th>
                            <th class="center">
                                NIM
                            </th>
                            <th class="center">
                                Nama
                            </th>
                            {foreach $data_komponen_mk as $data}
                                <th class="center">
                                    {$data.NM_KOMPONEN_MK} <br/>({$data.PERSENTASE_KOMPONEN_MK}%)
                                </th>
                            {/foreach}
                            <th class="center">
                                Nilai Angka Akhir
                            </th>
                            <th class="center">
                                Nilai Huruf Akhir
                            </th>
                            <th class="center">
                                Tampilkan Ke Mahasiswa
                            </th>
                        </tr>
                        {$index_nilai=1}
                        {foreach $data_mahasiswa as $mhs}
                            <tr> 
                                <td>{$mhs@index+1}</td>
                                <td>{$mhs.nim}</td>
                                <td>{$mhs.nama|upper}</td>
                                {foreach $mhs.data_nilai as $nilai}
                                    {if ((($mhs.status_cekal!=1&&(preg_match("/[U|u][A|a][S|s]/",$nilai.NM_KOMPONEN_MK)||preg_match("/[A|a][K|k][H|h][I|i][R|r]/",$nilai.NM_KOMPONEN_MK)))|| $mhs.status_cekal_uts!=1&&preg_match("/[U|u][T|s][S|s]/",$nilai.NM_KOMPONEN_MK)))}
                                        <td style="background-color: lightcoral" class="center">
                                            <input type="text" readonly="true" maxlength="5" size="3" name="nilai{$index_nilai}" {if $nilai.BESAR_NILAI_MK==''} value="0" {else} value="{$nilai.BESAR_NILAI_MK}" {/if} />
                                            <input type="hidden" name="id_nilai{$index_nilai}" value="{$nilai.ID_NILAI_MK}" />
                                            <br/><label for="nilai{$index_nilai}" class="error" style="display: none;font-size: 0.8em">Harus Angka dan Nilai tidak Lebih dari 100</label>
                                        </td>
                                    {else}
                                        <td class="center">
                                            <input type="text" class="nilai" maxlength="5" size="3" name="nilai{$index_nilai}" {if $nilai.BESAR_NILAI_MK==''} value="0" {else} value="{$nilai.BESAR_NILAI_MK}" {/if} />
                                            <input type="hidden" name="id_nilai{$index_nilai}" value="{$nilai.ID_NILAI_MK}" />
                                            <br/><label for="nilai{$index_nilai}" class="error" style="display: none;font-size: 0.8em">Harus Angka dan Nilai tidak Lebih dari 100</label>
                                        </td>
                                    {/if}
                                    {$index_nilai=$index_nilai+1}
                                {/foreach}
                                <td class="center">{if $mhs.nilai_angka_akhir!=''}{$mhs.nilai_angka_akhir}{else}Kosong{/if}</td>
                                <td class="center">{if $mhs.nilai_huruf_akhir!=''}{$mhs.nilai_huruf_akhir}{else}Kosong{/if}</td>
                                <td class="center">
                                    {if $mhs.flagnilai==0} 
                                        <span style="padding:5px 7px" class="ui-button ui-state-default ui-corner-all" title="Belum Di Tampilkan Ke Akun Mahasiswa">
                                            {if $pjmk_status==1}
                                                <input style="float: left" type="checkbox" onclick="tampilkan_permhs('penilaian.php?{$smarty.server.QUERY_STRING}',{$mhs.id_pengambilan})"/>
                                            {/if}
                                            <span style="float: left" class="ui-icon ui-icon-closethick">&nbsp;</span>Belum
                                        </span>
                                    {else}

                                        <span style="padding:5px 7px" class="ui-button ui-state-default ui-corner-all" title="Sudah Di Tampilkan Ke Akun Mahasiswa">
                                            {if $pjmk_status==1}
                                                <input style="float: left" type="checkbox" checked="true" onclick="tidak_tampilkan_permhs('penilaian.php?{$smarty.server.QUERY_STRING}',{$mhs.id_pengambilan})"/>
                                            {/if}
                                            <span style="float: left" class="ui-icon ui-icon-check">&nbsp;</span>Tampil
                                        </span>
                                    {/if}
                                </td>
                            </tr>
                        {foreachelse}
                            <tr>
                                <td colspan="6" class="kosong">Data Kosong</td>
                            </tr>
                        {/foreach}
                        <tr>
                            <td class="center" colspan="{$count_data_komponen_mk+6}">
                                <input type="hidden" name="total_nilai" value="{$index_nilai}" />
                                <input type="hidden" name="mode" value="save_nilai" />
                                <input type="hidden" name="id_kelas_mk" value="{$id_kelas_mk}" />
                                <input style="padding:5px" class="ui-button ui-state-default ui-corner-all"  type="submit" value="Simpan Nilai"/>
                            </td>
                        </tr>			
                    </table>
                </form>
                <form action="penilaian.php?{$smarty.server.QUERY_STRING}" method="post">
                    <table width="100%">
                        <tr>
                            <td class="center">
                                {if $pjmk_status==1}
                                    <input type="hidden" name="mode" value="tampilkan" />
                                    <input type="hidden" name="id_kelas_mk" value="{$id_kelas_mk}" />
                                    <input type="hidden" name="id_semester" value="{$data_kelas_mk_detail.ID_SEMESTER}" />
                                    {if $status_tampil==0}
                                        <input type="hidden" name="status_tampil" value="{$status_tampil}" />
                                        <input style="padding:5px" class="ui-button ui-state-default ui-corner-all"  type="submit" value="Tampilkan Semua"/>
                                    {else}
                                        <input type="hidden" name="status_tampil" value="{$status_tampil}" />
                                        <input style="padding:5px" class="ui-button ui-state-default ui-corner-all"  type="submit" value="Tidak Tampilkan Semua"/>
                                    {/if}                                
                                {else}
                                    <span style="padding:5px" class="ui-button ui-state-active ui-corner-all" onclick="$('#dialog_generate_button').dialog('open')">Tampilkan</span>
                                {/if}
                            </td>
                        </tr>
                    </table>
                </form>
            {/if}
        {/if}
    </div>

    <div id="dialog_generate_button" title="Konfirmasi">
        <p class="center ui-state-active" style="padding: 10px">Tidak punya Hak Akses Untuk Menampilkan Nilai Ke Mahasiswa</p>
        <p class="center">
            <span style="padding:5px" class="center ui-button ui-state-default ui-corner-all" onclick="$('#dialog_generate_button').dialog('close')">Close</span>
        </p>
    </div>
    {if empty($smarty.post.id_kelas_mk)}                
        <div id="dialog_pengumuman" title="Pengumuman">
            <p class="center" style="font-size: 14px">
                PEMBERITAHUAN
            </p>
            <ol style="font-weight:bold;font-size:13px;letter-spacing:0.5px;">
                <li>Mohon Di Periksa Kembali Nilai Akhir Mahasiswa Sudah Sesuai Atau Tidak Dengan Nilai Komponen Yang ada</li>
                <li>Jangan Lupa Untuk Menekan Tombol Tampilkan jika nilai sudah benar Agar Mahasiswa bisa melihat nilai Tersebut</li>
            </ol>
        </div>
        {literal}
            <script type="text/javascript">
                $("#dialog_pengumuman").dialog({
                    width: '500',
                    modal: true,
                    resizable: false,
                    autoOpen: false
                });
            </script>
        {/literal}
    {/if}
{/if}

<script type="text/javascript">var page_penilaian = '{$page_penilaian}';</script>
{literal}
    <script type="text/javascript">
        var id_akses = 0;
        function open_dialog_delete_akses(id) {
            $('#dialog_delete_akses').dialog('open');
            id_akses = id;
        }
        $('#button-aturan-nilai').click(function() {
            $('#aturan-nilai').slideDown();
        });
        $('#button-close-aturan-nilai').click(function() {
            $('#aturan-nilai').slideUp();
        });
        $("#dialog_generate_button").dialog({
            width: '300',
            modal: true,
            resizable: false,
            autoOpen: false
        });
        $("#dialog_delete_akses").dialog({
            width: '300',
            modal: true,
            resizable: false,
            autoOpen: false
        });
        $('#form_entry_nilai').validate();
        $(".nilai").each(function() {
            $(this).rules("add", {
                required: true,
                max: 100,
                number: true
            });
        })
        function delete_akses_komponen(url) {
            $.ajax({
                url: url,
                type: 'post',
                data: 'mode=delete_akses&id_akses=' + id_akses + '&id_kelas_mk=' + $('#select_kelas_mk').val(),
                beforeSend: function() {
                    $('#content').html('<div style="width: 100%;" align="center"><img src="/img/dosen/ajax_loader.gif" /></div>');
                },
                success: function(data) {
                    $('#content').html(data);
                }
            });
            $('#dialog_delete_akses').dialog('close');
        }
        function tampilkan_permhs(url, id) {
            $.ajax({
                url: url,
                type: 'post',
                data: 'mode=tampilkan_permhs&id_pengambilan=' + id,
                success: function(data) {
                    $('#content').html(data);
                }
            });
        }
        function tidak_tampilkan_permhs(url, id) {
            $.ajax({
                url: url,
                type: 'post',
                data: 'mode=tidak_tampilkan_permhs&id_pengambilan=' + id,
                success: function(data) {
                    $('#content').html(data);
                }
            });
        }
    </script>
{/literal}