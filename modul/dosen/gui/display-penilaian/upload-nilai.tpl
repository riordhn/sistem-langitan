<link type="text/css" href="../../css/jquery-ui-1.8.11.custom.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery-1.5.1.js"></script>
<script language="javascript" src="../../js/jquery.validate.js"></script>
{literal}
    <style>
        label.error {
            color: red;
            font-size:11px;
            font-family: "Trebuchet MS";
        }
    </style>
{/literal}
<div id="hasil_upload_nilai" {if $status_upload==''}style="display:none"{/if}>
    {if $status_upload==1}
        <div class="ui-state-highlight ui-corner-all" style="margin: 10px auto;padding: 10px;width:90%;font-family: 'Trebuchet MS'"> 
            <span class="ui-icon ui-icon-info" style="float: left;margin:0px 5px;color: #008000">  </span>
            Berhasil Upload File {$jumlah_insert} Data Penilaian Mahasiswa Berhasil Di Update, klik reload untuk melihat hasil<br/>
        </div>
    {else if $status_upload==0}
        <div class="ui-state-error ui-corner-all" style="margin: 10px auto;padding: 10px;width:90%"> 
            <span class="ui-icon ui-icon-alert" style="float: left;margin:0px 5px;">  </span> 
            Gagal Upload file <br/>
        </div>
    {else if $status_upload==2}
        <div class="ui-state-error ui-corner-all" style="margin: 10px auto;padding: 10px;width:90%"> 
            <span class="ui-icon ui-icon-alert" style="float: left;margin:0px 5px;">  </span> 
            Gagal Upload file, Komponen Nilai belum terisi untuk mata kuliah ini.  <br/>
        </div>
    {/if}
</div>
{if $status_upload==''}
    <form action="/modul/dosen/upload-nilai.php?id_kelas_mk={$smarty.get.id_kelas_mk}" method="post" style="text-align: center;width: 100%" id="form_upload"  enctype="multipart/form-data">
        <input type="file" class="required" name="file" />
        <input type="hidden" name="excel" value="excel" />
        <input type="submit" name="upload" class="ui-button ui-corner-all ui-state-default" value="Upload" style="padding:5px;font-size: 12px;cursor:pointer;"/>
    </form>
{/if}
{literal}
    <script>
            $('#form_upload').validate();
    </script>
{/literal}