<form id="form_copy_komponen" method="post" action="komponen_nilai.php">
    <table class="ui-widget" width="100%">
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="2"><h2>Copy Komponen Nilai</h2></th>
        </tr>
        <tr class="ui-widget-content">
            <td width="30%">Komponen Dari Kelas</td>
            <td>
                <select name="id_kelas_copy">
                    {foreach $data_kelas_pararel as $data}
                        <option value="{$data['ID_KELAS_MK']}" >{$data['NM_MATA_KULIAH']}<b>Kelas</b>  {$data['NM_KELAS']} <b>Prodi</b> {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="center" colspan="2">
                <input type="hidden" name="mode" value="copy"/>
                <input type="hidden" name="id_kelas" value="{$id_kelas}"/>
                <input type="submit" class="ui-button ui-state-default ui-corner-all" style="padding:5px" value="Copy"/>
                <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_copy_komponen').dialog('close')">Cancel</span>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_copy_komponen').submit(function(){
            if($('#form_copy_komponen').validate())
                    $('#dialog_copy_komponen').dialog('close')
            })
    </script>
{/literal}