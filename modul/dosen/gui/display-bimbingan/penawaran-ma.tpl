{literal}
    <style>
        .center{ text-align:center;}
    </style>
{/literal}
<div class="center_title_bar">Daftar Usulan Mata Kuliah</div>
{if !empty($data_kelas)}
    <table class="ui-widget ui-widget-content" style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="12" class="header-coloumn" style="font-size: 16pt">Daftar Mata Kuliah Usulan</th>
        </tr>
        <tr class="ui-widget-header">
            <th class="header-coloumn">NO</th>
            <th class="header-coloumn">KODE MK</th>
            <th class="header-coloumn">NAMA MATA KULIAH</th>
            <th class="header-coloumn">SKS</th>
            <th class="header-coloumn">KLS</th>
            <th class="header-coloumn">TGKT SEM</th>
            <th class="header-coloumn">PRODI</th>
            <th class="header-coloumn">HARI</th>
            <th class="header-coloumn">JAM</th>
            <th class="header-coloumn">RUANG</th>
            <th class="header-coloumn">KPST</th>
            <th class="header-coloumn">TERISI</th>
        </tr>
        {foreach $data_kelas as $data}
            {$list_pengajar="-"|explode:$data.PENGAJAR} 
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.KD_MATA_KULIAH}</td>
                <td>{$data.NM_MATA_KULIAH}</td>
                <td>{$data.KREDIT_SEMESTER}</td>
                <td>{$data.NAMA_KELAS}</td>
                <td>
                    <ol>
                        {foreach $list_pengajar as $pengajar}
                            {if $pengajar!=''}
                                <li {if $pengajar|strpos:"pjmk"} style="color: #008000"{/if}>{$pengajar}</li>
                            {/if}
                        {/foreach}
                    </ol>
                </td>
                <td>{$data.NM_JADWAL_HARI}</td>
                <td>{$data.NM_JADWAL_JAM}</td>
                <td>{$data.NM_RUANGAN}</td>
                <td>
                    <a href="penawaran-ma.php?mode=peserta&id_kelas={$data.ID_KELAS_MK}" style="padding: 3px;" class="status ui-button ui-state-default ui-corner-all">Peserta</a>
                </td>
            </tr>
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="12" class="center"><span style="color: red">Data Kosong</span></td>
            </tr>
        {/foreach}
    </table>
{else if !empty($data_peserta_mk)}
    <table class="ui-widget ui-widget-content">
        <tr class="ui-widget-header">
            <th colspan="8" class="header-coloumn" style="font-size: 16pt">Daftar Peserta Mata Kuliah</th>
        </tr>
        <tr class="ui-widget-header">
            <th class="header-coloumn">NO</th>
            <th class="header-coloumn">NIM</th>
            <th class="header-coloumn">NAMA</th>
            <th class="header-coloumn">ANGKATAN</th>
            <th class="header-coloumn">IPK</th>
            <th class="header-coloumn">IPS</th>
            <th class="header-coloumn">SKS TOTAL</th>
            <th class="header-coloumn">STATUS APPROVE</th>
        </tr>
    </table>
{/if}
{literal}
    <script type="text/javascript">
        $('.ui-button').hover(
            function(){
                $(this).addClass('ui-state-highlight')
                }, 
            function(){        
                $(this).removeClass('ui-state-highlight')
                }
            );
        function show_detail(index,tag){
            $(tag+index).fadeToggle();
        }
        function hide_detail(index,tag){
            $(tag+index).fadeOut();
        }
        
    </script>
{/literal}