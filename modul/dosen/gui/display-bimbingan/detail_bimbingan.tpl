<div id="tugas_akhir">
    <form name="form_detail_bimbingan" id="form_detail_bimbingan" action="bimbingan.php">
        <table>
            <tr>
                <th colspan="3"><h2>Progress Tugas Akhir</h2></th>
            </tr>
            <tr>
                <td><span>Mahasiswa</span></td>
                <td colspan="2"><span>{$data_detail_bimbingan[0]['nama']}</span></td>
            </tr>
            <tr>
                <td><span>Judul</span></td>
                <td colspan="2"><span>{$data_detail_bimbingan[0]['judul']}</span></td>
            </tr>
            <tr>
                <th><h3>Bab</h3></th>
                <th><h3>Progress</h3></th>
                <th><h3>Hide / Show</h3></th>
            </tr>
            {foreach $data_detail_bimbingan as $detail_bimbingan}
            <tr>
                <td><span>Bab {$detail_bimbingan['bab']}</span></td>
                <td>
                    <label for="bab{$detail_bimbingan['bab']}">Progress:</label>
                    <input type="text" id="bab{$detail_bimbingan['bab']}" name="progress_bab{$detail_bimbingan['bab']}" style="color : #66C2EA; font-weight:bold;" value="{$detail_bimbingan['progress']}"/>
                    <div id="slider_bab{$detail_bimbingan['bab']}" style="display:none;"></div>
                </td>
                <td><input type="checkbox" class="slider_bab" id="{$detail_bimbingan['bab']}" /></td>
            </tr>
            {/foreach}
            <tr>
                <td colspan="3"><a class="save">Sumbit</a></td>
                <input type="hidden" name="save"/>
                <input type="hidden" name="id_ta" value="{$data_detail_bimbingan[0]['id_ta']}" />
            </tr>
        </table>
    </form>
</div>

{literal}
<script type="text/javascript">
$('.slider_bab').click(function(){
    $('#slider_bab'+$(this).attr('id')).toggle();
});
$('.save').click(function(){
    $('#form_detail_bimbingan').submit();
});
$('#form_detail_bimbingan').ajaxForm({
    url : $(this).attr('action'),
    type : 'post',
    data : $(this).serialize(),
    success :function(data){
        $('#center_content').html(data);
    }
});
$(function() {
    $( "#slider_bab1" ).slider({
        range: "min",
        value: parseInt($('#bab1').val()),
        min: 0,
        max: 100,
        slide: function( event, ui ) {
            $( '#bab1' ).val(  ui.value + " %");
                
        }
    });
    $( "#bab1" ).val($( "#slider_bab1" ).slider( "value" )+" %" );
        
});
    
$(function() {
    $( "#slider_bab2" ).slider({
        range: "min",
        value: parseInt($('#bab2').val()),
        min: 0,
        max: 100,
        slide: function( event, ui ) {
            $( '#bab2' ).val(  ui.value + " %");
                
        }
    });
    $( "#bab2" ).val($( "#slider_bab2" ).slider( "value" )+" %" );
        
});
$(function() {
    $( "#slider_bab3" ).slider({
        range: "min",
        value: parseInt($('#bab3').val()),
        min: 0,
        max: 100,
        slide: function( event, ui ) {
            $( '#bab3' ).val(  ui.value + " %");
                
        }
    });
    $( "#bab3" ).val($( "#slider_bab3" ).slider( "value" )+" %" );
        
});
$(function() {
    $( "#slider_bab4" ).slider({
        range: "min",
        value: parseInt($('#bab4').val()),
        min: 0,
        max: 100,
        slide: function( event, ui ) {
            $( '#bab4' ).val(  ui.value + " %");
                
        }
    });
    $( "#bab4" ).val($( "#slider_bab4" ).slider( "value" )+" %" );
        
});
$(function() {
    $( "#slider_bab5" ).slider({
        range: "min",
        value: parseInt($('#bab5').val()),
        min: 0,
        max: 100,
        slide: function( event, ui ) {
            $( '#bab5' ).val(  ui.value + " %");
                
        }
    });
    $( "#bab5" ).val($( "#slider_bab5" ).slider( "value" )+" %" );
        
});
$(function() {
    $( "#slider_bab6" ).slider({
        range: "min",
        value: parseInt($('#bab6').val()),
        min: 0,
        max: 100,
        slide: function( event, ui ) {
            $( '#bab6' ).val(  ui.value + " %");
                
        }
    });
    $( "#bab6" ).val($( "#slider_bab6" ).slider( "value" )+" %" );
        
});
$(function() {
    $( "#slider_bab7" ).slider({
        range: "min",
        value: parseInt($('#bab7').val()),
        min: 0,
        max: 100,
        slide: function( event, ui ) {
            $( '#bab7' ).val(  ui.value + " %");
                
        }
    });
    $( "#bab7" ).val($( "#slider_bab7" ).slider( "value" )+" %" );
        
});
        
            
</script>
{/literal}