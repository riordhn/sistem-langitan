{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>

    <style type="text/css">
        tr.kuning.ui-widget-content>td, table.tablesorter tbody tr.odd.kuning td {
            background-color:#ffff00;
        }
    </style>
{/literal}
<div class="center_title_bar">Approve SKKK Mahasiswa</div> 
{if $id_fakultas!=''}
    {if $smarty.get.mode=='lihat-skkk'}
        <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:7px;" onclick="history.back(-1)" >Kembali</span>
        <p></p>
        <table style="width: 90%;">
            <tr class="ui-widget-header">
                <th colspan="6" class="header-coloumn">Detail Mahasiswa</th>
            </tr>
            <tr>
                <td>NAMA</td>
                <td style="width: 230px;">: {$data_mahasiswa.NM_PENGGUNA}</td>
                <td>JML SKKK</td>
                <td>: {$data_mahasiswa.JML_SKKK}</td>
                <td>SKOR SKKK DIPEROLEH</td>
                <td>: {if $data_mahasiswa.SKOR_SKKK == ''}0{else}{$data_mahasiswa.SKOR_SKKK}{/if}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>: {$data_mahasiswa.NIM_MHS}</td>
                <td>FILE VERIFIED</td>
                <td>: {$data_mahasiswa.JML_FILE_VERIFIED}</td>
                <td>FILE NOT VERIFIED</td>
                <td>: {$data_mahasiswa.JML_FILE_NOT_VERIFIED}</td>           
            </tr>
            <tr>
                <td>ANGKATAN</td>
                <td colspan="5">: {$data_mahasiswa.THN_ANGKATAN_MHS}</td>
            </tr>
            <tr>
                <td>PROGRAM STUDI</td>
                <td colspan="5">: ({$data_mahasiswa.NM_JENJANG}) {$data_mahasiswa.NM_PROGRAM_STUDI}</td>
            </tr>
        </table>
        <p></p>
        {foreach $data_krp as $krp}            
            <br/><hr style="width:70%"/><br/>
            <table class="tablesorter" style="width: 90%">
                <thead>
                    <tr class="ui-widget-header">
                        <th colspan="7" class="header-coloumn">DETAIL SKKK<br/>SEMESTER {$krp.NM_SEMESTER} {$krp.TAHUN_AJARAN}</th>
                    </tr>
                    <tr class="ui-widget-header">
                        <th>NO</th>
                        <th>PENYELENGGARA</th>
                        <th>WAKTU</th>
                        <th>NAMA KEGIATAN</th>
                        <th>BOBOT</th>
                        <th>SKOR</th>
                        <th>AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $krp.DATA_KRP as $dk}
                        {if $dk.ADA_FILE > 0}
                            <tr class="ui-widget-content" >
                                <td>{$dk@index+1}</td>
                                <td>{$dk.PENYELENGGARA_KRP_KHP}</td>
                                <td>{$dk.WAKTU_KRP_KHP}</td>
                                <td>{$dk.NM_KRP_KHP}</td>
                                <td>{$dk.BOBOT_KEGIATAN_2}</td>
                                <td>{$dk.SKOR_KRP_KHP}</td>
                                <td><a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:7px;" href="approve-skkk.php?mode=lihat-file&id_mhs={$dk.ID_MHS}&id_krp_khp={$dk.ID_KRP_KHP}" >Lihat File</a></td>
                            </tr>
                        {else}
                            <tr class="ui-widget-content kuning">
                                <td>{$dk@index+1}</td>
                                <td>{$dk.PENYELENGGARA_KRP_KHP}</td>
                                <td>{$dk.WAKTU_KRP_KHP}</td>
                                <td>{$dk.NM_KRP_KHP}</td>
                                <td>{$dk.BOBOT_KEGIATAN_2}</td>
                                <td>{$dk.SKOR_KRP_KHP}</td>
                                <td>Belum Ada File Di Upload</td>
                            </tr>
                        {/if}
                    {/foreach}
                </tbody>
                <!-- <tfoot>
                    <tr class="ui-widget-content">
                        <td colspan="4" class="center">
                            <p style="font-weight: bold;font-size: 1.2em">
                                IPS : {$khs.IPS}<br/>
                                SKS SEMESTER : {$sks_semester}
                            </p>
                        </td>
                    </tr>       
                </tfoot> -->
            </table>
        {/foreach}
    {elseif $smarty.get.mode=='lihat-file'}
        {if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

        <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:7px;" onclick="history.back(-1)" >Kembali</span>
        <p></p>
        <table style="width: 90%;">
            <tr class="ui-widget-header">
                <th colspan="6" class="header-coloumn">Detail Mahasiswa</th>
            </tr>
            <tr>
                <td>NAMA</td>
                <td style="width: 230px;">: {$data_mahasiswa.NM_PENGGUNA}</td>
                <td>JML SKKK</td>
                <td>: {$data_mahasiswa.JML_SKKK}</td>
                <td>SKOR SKKK DIPEROLEH</td>
                <td>: {if $data_mahasiswa.SKOR_SKKK == ''}0{else}{$data_mahasiswa.SKOR_SKKK}{/if}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>: {$data_mahasiswa.NIM_MHS}</td>
                <td>FILE VERIFIED</td>
                <td>: {$data_mahasiswa.JML_FILE_VERIFIED}</td>
                <td>FILE NOT VERIFIED</td>
                <td>: {$data_mahasiswa.JML_FILE_NOT_VERIFIED}</td>           
            </tr>
            <tr>
                <td>ANGKATAN</td>
                <td colspan="5">: {$data_mahasiswa.THN_ANGKATAN_MHS}</td>
            </tr>
            <tr>
                <td>PROGRAM STUDI</td>
                <td colspan="5">: ({$data_mahasiswa.NM_JENJANG}) {$data_mahasiswa.NM_PROGRAM_STUDI}</td>
            </tr>
        </table>
        <p></p>
        {foreach $data_file_krp as $krp}            
            <br/><hr style="width:70%"/><br/>
            <form action="approve-skkk.php?{$smarty.server.QUERY_STRING}" method="post">
            <table style="width: 90%">
                    <tr class="ui-widget-header">
                        <th colspan="7" class="header-coloumn">DETAIL FILE<br/>KEGIATAN {$krp.NM_KRP_KHP}</th>
                    </tr>

                    <tr>
                        <td>NAMA KEGIATAN </td>
                        <td style="width: 230px;">: {$krp.NM_KRP_KHP}</td>
                        <td style="width: 70px;">TINGKAT</td>
                        <td>: {if $krp.NM_TINGKAT == ''}-{else}{$krp.NM_TINGKAT}{/if}</td>
                        <td>JABATAN</td>
                        <td>: {if $krp.NM_JABATAN_PRESTASI == ''}-{else}{$krp.NM_JABATAN_PRESTASI}{/if}</td>
                    </tr>
                    <tr>
                        <td>PENYELENGGARA</td>
                        <td>: {$krp.PENYELENGGARA_KRP_KHP}</td>
                        <td style="width: 70px;">SKOR DIPEROLEH</td>
                        <td>: {$krp.SKOR_KRP_KHP}</td>
                        <td>BOBOT NILAI</td>
                        <td>: {$krp.BOBOT_KEGIATAN_2}</td>           
                    </tr>
                    <tr>
                        <td>WAKTU KEGIATAN</td>
                        <td colspan="5">: {$krp.WAKTU_KRP_KHP}</td>
                    </tr>
                    <tr>
                        <td>BUKTI FISIK</td>
                        <td colspan="5">: {if $krp.NM_BUKTI_FISIK == ''}-{else}{$krp.NM_BUKTI_FISIK}{/if}</td>
                    </tr>

            </table>

            <table style="width: 90%">
                <thead>
                    <tr class="ui-widget-header">
                        <th>NO</th>
                        <th>#</th>
                        <th>NAMA FILE</th>
                        <th>TIPE FILE</th>
                        <th>STATUS FILE</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $krp.DATA_FILE_KRP as $uk}
                        <tr class="ui-widget-content" >
                            <td class="center">{$uk@index + 1}</td>
                            {assign var="image" 
                            value="../../files/kemahasiswaan/{$uk.ID_MHS}/{$uk.NM_FILE_KRP_KHP}"} 
                            {if file_exists($image)}
                                {if $uk.TIPE_FILE_KRP_KHP == 'image/gif' OR $uk.TIPE_FILE_KRP_KHP == 'image/jpeg' OR $uk.TIPE_FILE_KRP_KHP == 'image/jpg' OR $uk.TIPE_FILE_KRP_KHP == 'image/pjpeg' OR $uk.TIPE_FILE_KRP_KHP == 'image/x-png' OR $uk.TIPE_FILE_KRP_KHP == 'image/png'}
                                    <td>
                                        <img src="../../files/kemahasiswaan/{$uk.ID_MHS}/{$uk.NM_FILE_KRP_KHP}?t=timestamp" border="0" width="50" />
                                        <br/>
                                        <a href="{$base_url}files/kemahasiswaan/{$uk.ID_MHS}/{$uk.NM_FILE_KRP_KHP}" class="disable-ajax" target="_blank"><u>Lihat File Scan</u></a>
                                    </td>
                                {else}
                                    <td>
                                        <a href="{$base_url}files/kemahasiswaan/{$uk.ID_MHS}/{$uk.NM_FILE_KRP_KHP}" class="disable-ajax" target="_blank"><u>Download File</u></a>
                                    </td>
                                {/if}
                            {else}
                                <td class="center"><img src="includes/images/cancel.png" border="0" width="50" /></td>
                            {/if}
                            <td><strong>{$uk.NM_FILE_KRP_KHP}</strong></td>
                            <td><strong>{$uk.TIPE_FILE_KRP_KHP}</strong></td>
                            <td>{if $uk.IS_VERIFIED == 0}Belum Verified{else}Sudah Verified{/if}</td>
                        </tr>
                    {/foreach}
                </tbody>
                <tfoot>
                    <input type="hidden" name="id_krp_khp" value="{$krp.ID_KRP_KHP}" />
                    {if $krp.IS_VERIFIED == 0}
                    <input type="hidden" name="mode" value="verifikasi" />
                        <tr class="ui-widget-content">
                            <td colspan="5" class="center">
                                <input type="submit" class="ui-button ui-corner-all ui-state-hover" value="Verifikasi">
                            </td>
                        </tr>  
                    {else}
                    <input type="hidden" name="mode" value="not-verifikasi" />
                        <tr class="ui-widget-content">
                            <td colspan="5" class="center">
                                <input type="submit" class="ui-button ui-corner-all ui-state-hover" value="Batalkan Verifikasi">
                            </td>
                        </tr>  
                    {/if}  
                </tfoot>
            </table>
            </form>   
        {/foreach}
    {else}
        {if $keterangan_perwalian!=''}
            {$keterangan_perwalian}
        {/if}
        <div>
            <table class="tablesorter" style="width: 100%">
                <thead>
                    <tr class="ui-widget-header">
                        <th colspan="10" class="header-coloumn">Detail Approve SKKK Mahasiswa</th>
                    </tr>
                    <tr class="ui-widget-header">
                        <th>NO.</th>
                        <th>NIM</th>
                        <th>NAMA MHS</th>
                        <th>ANGKATAN</th>
                        <th>JUMLAH INPUT SKKK</th>
                        <th>JUMLAH SKOR SKKK</th>
                        <th>JUMLAH FILE VERIFIED</th>
                        <th>JUMLAH FILE NOT VERIFIED</th>
                        <th>DETAIL</th>
                    </tr>
                </thead>
                <tbody>
                    {$index=1}
                    {foreach $data_mahasiswa_krp as $mhs}
                        <tr class="ui-widget-content">
                            <td class="center"><span>{$index++}</span></td>
                            {assign var="image" 
                                value="../../foto_mhs/{$nama_singkat}/{$mhs['NIM']}.jpg"} 

                            {assign var="image1" 
                                value="../../foto_mhs/{$nama_singkat}/{$mhs['NIM']}.JPG"} 

                            {if file_exists($image)}
                                <td><img src="../../foto_mhs/{$nama_singkat}/{$mhs['NIM']}.jpg" width="61px" /><br/><span>{$mhs['NIM']}</span></td>
                            {else if file_exists($image1)}
                                <td><img src="../../foto_mhs/{$nama_singkat}/{$mhs['NIM']}.JPG" width="61px" /><br/><span>{$mhs['NIM']}</span></td>
                            {else}
                                <td><img src="../../foto_mhs/foto-tidak-ditemukan.png" width="61px" /><br/><span>{$mhs['NIM']}</span></td>
                            {/if}
                            <td><span>{$mhs['NAMA']}</span></td>
                            <td class="center"><span>{$mhs['ANGKATAN']} ({$mhs['NM_JENJANG']})</span></td>
                            <td class="center"><span>{$mhs['JML_SKKK']}</span></td>
                            <td class="center"><span>{if $mhs['SKOR_SKKK'] == ''}0{else}{$mhs['SKOR_SKKK']}{/if}</span></td>
                            <td class="center"><span>{$mhs['JML_FILE_VERIFIED']}</span></td>
                            <td class="center"><span>{$mhs['JML_FILE_NOT_VERIFIED']}</span></td>
                            <td class="center" style="width: 80px;">
                                <a href="approve-skkk.php?mode=lihat-skkk&id_mhs={$mhs.ID_MHS}" style="padding: 3px;" class="ui-button ui-state-default ui-corner-all">Lihat File SKKK</a>
                            </td>
                        </tr>
                    {foreachelse}
                        <tr>
                            <td colspan="10" class="kosong">Data Masih Kosong</td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>
    {/if}
{else}
    <table>
        <tr><td class="ui-state-error" style="padding: 10px;margin: 10px;font-size: 13px;">UNTUK SEMENTARA PERWALIAN FAKULTAS EKONOMI DAN BISNIS DI NON AKTIFKAN</td></tr>
    </table>
{/if}
{literal}
    <script type="text/javascript">
        $(".status").click(function() {
            $.ajax({
                url: 'perwalian.php',
                type: 'post',
                data: 'id_mhs=' + $(this).attr('id') + '&detail=' + $(this).attr('rel'),
                beforeSend: function() {
                    $('#content').html('<div style="width: 90%;" align="center"><img src="../../img/dosen/ajax_loader.gif" /></div>');
                },
                success: function(data) {
                    $('#content').html(data);
                }
            });
        });
    </script>
{/literal}