{literal}
    <style>
        .center{ text-align:center;}
    </style>
{/literal}
{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">Tambah KRS Mahasiswa</div> 
<div >
<span id="{$id_mhs}" style="padding: 5px;float: left;margin-bottom: 15px;"  class="kembali ui-button ui-state-default ui-corner-all" rel="krs">Kembali</span>

    <form name="form_krs" id="form_krs" action="perwalian.php" method="post">
        <table class="tablesorter" >
            <thead>
                <tr class="ui-widget-header">
                    <th colspan="8" class="header-coloumn">Detail Kelas KRS</th>
                </tr>
                <tr>
                    <th>NO.</th>
                    <th>KODE</th>
                    <th>NAMA MATA KULIAH</th>
                    <th>SEMESTER</font></th>
                    <th>KELAS</font></th>
                    <th>KAPASITAS</th>
                    <th>ISI</th>
                    <th>AKSI</th>
                    
                </tr>
            </thead>
            <tbody>
                {if $kelas_set!=''}
                    {foreach $kelas_set as $kelas}
                        <tr class="ui-widget-content">
                            <td>{$kelas@index + 1}</td>
                            <td>{$kelas.KD_MATA_KULIAH}</td>
                            <td>{$kelas.NM_MATA_KULIAH} ({$kelas.KREDIT_SEMESTER} SKS)</td>
                            <td class="center">{$kelas.TINGKAT_SEMESTER}</td>
                            <td class="center">{$kelas.NAMA_KELAS}</td>
                            <td class="center">{$kelas.KAPASITAS}</td>
                            <td class="center">{$kelas.ISI}</td>
                            <td>
                                
                                <span id="{$id_mhs}" style="padding: 5px;float: right;width: 110px;" class="tambah ui-button ui-state-default ui-corner-all" rel="krs" st="2" id_kelas_mk="{$kelas.ID_KELAS_MK}">Tambah</span>

                            </td> 
                        </tr>
                    {/foreach}
                {else}
                    <tr>
                        <td colspan="8" style="text-align: center;color: #fb6666;"><b>KELAS UNTUK KRS BELUM ADA</b></td>
                    </tr>
                {/if}
            </tbody>
        </table>
    </form>
</div>
{literal}
    <script type="text/javascript">

        $(function() {
            var id_mhs = $('#id_mhs').val();
            $('.kembali').click(function() {
                $.ajax({
                    url: 'perwalian.php',
                    type: 'post',
                    data: 'id_mhs=' + $(this).attr('id') + '&detail=' + $(this).attr('rel'),
                    beforeSend: function() {
                        $('#content').html('<div style="width: 90%;" align="center"><img src="../../img/dosen/ajax_loader.gif" /></div>');
                    },
                    success: function(data) {
                        $('#content').html(data);
                    }
                });

            });
            //utk menghapus KRS
            $('.tambah').click(function() {
                    $.ajax({
                        url: 'perwalian.php',
                        type: 'post',
                        data: 'id_mhs=' + $(this).attr('id') + '&detail=' + $(this).attr('rel') + '&st=' + $(this).attr('st') + '&id_kelas_mk=' + $(this).attr('id_kelas_mk'),
                        beforeSend: function() {
                            $('#content').html('<div style="width: 90%;" align="center"><img src="../../img/dosen/ajax_loader.gif" /></div>');
                        },
                        success: function(data) {
                            $('#content').html(data);
                        }
                    });
                
            });
        })

    </script>
{/literal}