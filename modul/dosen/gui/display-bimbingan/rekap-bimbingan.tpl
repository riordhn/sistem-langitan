<div class="center_title_bar">REKAP BIMBINGAN TUGAS AKHIR/SKRIPSI/TESIS/DESERTASI</div>
{if isset($smarty.get.id)}
    <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:8px;" href="rekap-bimbingan.php">Kembali</a>
    <table style="width: 50%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">DOSEN</th>
        </tr>
        <tr>
            <td>Dosen</td>
            <td>: {$detail[0]['NM_DOSEN']}</td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenis</td>
            <td>: {$detail[0]['NM_JENIS_PEMBIMBING']}</td>
        </tr>
    </table>

    <table>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Tipe</th>
            <th>Judul</th>
        </tr>
        {$no = 1}
        {foreach $detail as $data}
            <tr class="ui-widget-content">
                <td>{$no++}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_MHS}</td>
                <td>{$data.NM_TIPE_TA}</td>
                <td>{$data.JUDUL_TUGAS_AKHIR}</td>
            </tr>
        {/foreach}
    </table>

{else}
    <table>
        <tr class="ui-widget-header">
            <th colspan="7" class="header-coloumn">REKAP BIMBINGAN</th>
        </tr>
        <tr class="ui-widget-header">
            <th rowspan="2" style="text-align:center">No</th>
            <th rowspan="2" style="text-align:center">Dosen Pembimbing</th>
            <th colspan="5" style="text-align:center">Jumlah Mhs</th>        
        </tr>
        <tr>
            {foreach $jenis_pembimbing as $data}
                <th>
                    {$data.NM_JENIS_PEMBIMBING}
                </th>
            {/foreach}
        </tr>
        {$no = 1}
        {foreach $pembimbing as $data}
            <tr class="ui-widget-content">
                <td style="text-align:center">{$no++}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td style="text-align:center"><a href="rekap-bimbingan.php?id={$data.ID_DOSEN}&jenis=1">{$data.PEMBIMBING_1}</a></td>
                <td style="text-align:center">{$data.PEMBIMBING_2}</td>
                <td style="text-align:center">{$data.PEMBIMBING_3}</td>
                <td style="text-align:center">{$data.PEMBIMBING_4}</td>
                <td style="text-align:center">{$data.PEMBIMBING_5}</td>
            </tr>
        {/foreach}
    </table>
{/if}