{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
{literal}
    <style>
        .center{ text-align:center;}
    </style>
{/literal}
{if $smarty.get.mode==''}
    <div class="center_title_bar">Aproval Semester Pendek KRS</div> 
    <div class="ui-widget">
        <table class="ui-widget-content tablesorter" style="width: 100%">
            <thead>
                <tr class="ui-widget-header">
                    <th colspan="9" class="header-coloumn">Detail Mahasiswa</th>
                </tr>
                <tr>
                    <th>NO.</th>
                    <th>NIM</th>
                    <th>NAMA MHS</th>
                    <th>ANGKATAN</th>
                    <th>IPS</th>
                    <th>IPK</th>
                    <th>TOTAL SKS</th>
                    <th>STATUS</th>
                    <th>APPROVED</th>
                </tr>
            </thead>
            <tbody>
                {foreach $data_mahasiswa_sp as $mhs}
                    <tr>
                        <td class="center"><span>{$mhs@index+1}</span></td>
                        <td><span>{$mhs['NIM']}</span></td>
                        <td><span>{$mhs['NAMA']}</span></td>
                        <td class="center"><span>{$mhs['ANGKATAN']}</span></td>
                        <td class="center"><span>{$mhs['IPS']}</span></td>
                        <td class="center"><span>{$mhs['IPK']}</span></td>
                        <td class="center"><span>{$mhs['TOTAL_SKS']}</span></td>
                        <td class="center">
                            {if $mhs['STATUS']==null&&$mhs['STATUS_APV']==null}
                                <span>Tidak Ambil SP</span>							
                            {else if $mhs['STATUS']<=1&&$mhs['STATUS_APV']>0}
                                <span 
                                    {if $mhs['STATUS_APV']<1} 
                                        style="color: #ff3333" 
                                    {else if $mhs['STATUS_APV']==1} 
                                        style="color: #008000" 
                                    {/if}>
                                    Sudah Ambil SP
                                </span>
                            {else if $mhs['STATUS']>1}
                                <span 
                                    {if $mhs['STATUS_APV']<1} 
                                        style="color: brown" 
                                    {else if $mhs['STATUS_APV']==1} 
                                        style="color: #00ac99" 
                                    {/if}>
                                    SP Akademik
                                </span>
                            {else}
                                <span style="color: red">Mata Kuliah SP Terhapus</span>
                            {/if}
                        </td>
                        <td>
                            {if $mhs['STATUS_APV']==1}
                                <a href="approve-sp.php?mode=load_sp&id_mhs={$mhs['ID_MHS']}" style="padding: 3px;width: 110px;" class="status ui-button ui-state-default ui-corner-all" rel="krs">Sudah Approve</a>
                            {else if $mhs['STATUS_APV']<1&&$mhs['STATUS_APV']>0}
                                <a href="approve-sp.php?mode=load_sp&id_mhs={$mhs['ID_MHS']}" style="padding: 3px;width: 110px;" class="status ui-button ui-state-default ui-corner-all" rel="krs">Belum Approve Semua</a>
                            {else}
                                <a href="approve-sp.php?mode=load_sp&id_mhs={$mhs['ID_MHS']}" style="padding: 3px;width: 110px;" class="status ui-button ui-state-default ui-corner-all" rel="krs">Belum Approve</a>
                            {/if}
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="9" style="color: red;text-align: center">Data Kosong
                        </td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
{else if $smarty.get.mode=='load_sp'}
    {literal}
        <style>
            .center{ text-align:center;}
        </style>
    {/literal}
    <div class="center_title_bar">Kartu Rencana Studi Mahasiswa</div>
    <div class="ui-widget">
        <table style="width: 90%;">
            <tr class="ui-widget-header">
                <th colspan="6" class="header-coloumn">Detail Mahasiswa</th>
            </tr>
            <tr>
                <td>NAMA</td>
                <td style="width: 230px;">: {$data_mahasiswa.NM_PENGGUNA}</td>
                <td>IPS</td>
                <td>: {$data_mahasiswa.IPS}</td>
                <td>TOTAL SKS</td>
                <td>: {$data_mahasiswa.TOTAL_SKS}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>: {$data_mahasiswa.NIM_MHS}</td>
                <td>IPK</td>
                <td>: {$data_mahasiswa.IPK}</td>
                <td>ANGKATAN</td>
                <td>: {$data_mahasiswa.THN_ANGKATAN_MHS}</td>           
            </tr>
            <tr>
                <td>PROGRAM STUDI</td>
                <td colspan="5">:({$data_mahasiswa.NM_JENJANG}) {$data_mahasiswa.NM_PROGRAM_STUDI}</td>
            </tr>
        </table>
        <form action="approve-sp.php" method="post">
            <table style="width:90%;">
                <tr class="ui-widget-header">
                    <th colspan="6" class="header-coloumn">Detail Mata Kuliah SP</th>
                </tr>
                <tr>
                    <th>NO.</th>
                    <th>KODE MTK</th>
                    <th colspan="2">NAMA MATA KULIAH</th>
                    <th>SKS MTK</font></th>
                    <th>CONFIRM</th>
                </tr>
                {$total_sks=0}
                {$total_sks_mhs=0}
                {foreach $data_mk_sp as $sp}
                    {if $sp['STATUS_APV']=='1'}
                        {$total_sks =$total_sks+$sp['SKS']}
                    {/if}
                    {$total_sks_mhs =$total_sks_mhs+$sp['SKS']}
                    <tr>
                        <td class="center">{$sp@index+1}</td>
                        <td>{$sp['KODE_MK']}</td>
                        <td colspan="2">
                            {$sp['NAMA_MK']}
                        </td>
                        <td>{$sp['SKS']}</td>
                        <td>
                            <input type="checkbox" name="confirm{$sp@index+1}" {if $sp['STATUS_APV']==1}checked="true"{/if} value="{$sp['ID_PENGAMBILAN_MK']}" /> Confirm
                            <input type="hidden" name="not_confirm{$sp@index+1}" value="{$sp['ID_PENGAMBILAN_MK']}" />
                            <input type="hidden" name="sks{$sp@index+1}" value="{$sp['SKS']}"/>
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="6" style="text-align: center;color: #fb6666;"><b>MATA KULIAH SP MAHASISWA BELUM ADA</b></td>
                    </tr>
                {/foreach}
                <tr>
                    <td colspan="6">
                        <span style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all" onclick="uncheck_all()">Uncheck All</span>
                        <span style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all" onclick="check_all()">Check All</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <br/>
                        Total SKS Diambil Approve Dosen : <b>{$total_sks}</b><br/><br/>
                        Total SKS Diambil Mahasiswa : <b>{$total_sks_mhs}</b><br/><br/>
                        <input type="hidden" name="id_mhs" value="{$data_mahasiswa.ID_MHS}" />
                        <input type="hidden" name="mode" value="approve"/>
                        <input type="submit" style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all" value="Simpan">
                        <input type="hidden" name="jumlah_data" value="{count($data_mk_sp)}" /> 
                    </td>
                </tr>  
            </table>
        </form>
    </div>
    <a href="approve-sp.php" style="padding: 5px;margin: 10px;font-size: 14px;font-weight: bold" class="ui-button ui-state-default ui-corner-all">Kembali</a>
{/if}
{literal}
    <script type="text/javascript">
        function check_all(chk)
        {
            x = $(':checkbox');
            for (i = 0; i < x.length; i++)
                x[i].checked = true;
        }

        function uncheck_all(chk)
        {
            x = $(':checkbox');
            for (i = 0; i < x.length; i++)
                x[i].checked = false;
        }
        $('.ui-button').hover(
                function() {
                    $(this).addClass('ui-state-highlight')
                },
                function() {
                    $(this).removeClass('ui-state-highlight')
                }
        );
    </script>
{/literal}