{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">Approval KRS Massal</div> 
{if $mode==''}
    {if $keterangan_perwalian!=''}
        {$keterangan_perwalian}
    {/if}
    <div>
        <span style="margin-left: 10px;padding: 10px;" id="check_status_button" class="status ui-button ui-state-default ui-corner-all" >Check/Uncheck All <input type="checkbox"  id="check_all_krs"/></span>
        <form action="approval.php" method="post">
            <table class="tablesorter" style="width: 100%">
                <thead>
                    <tr class="ui-widget-header">
                        <th colspan="10" class="header-coloumn">Detail Perwalian Mahasiswa</th>
                    </tr>
                    <tr class="ui-widget-header">
                        <th>NO.</th>
                        <th>NIM</th>
                        <th>NAMA MHS</th>
                        <th>ANGKATAN</th>
                        <th>IPS</th>
                        <th>IPK</th>
                        <th>TOTAL SKS</th>
                        <th>STATUS</th>
                        <th>PERSETUJUAN</th>
                    </tr>
                </thead>
                <tbody>
                    {$index=1}
                    {foreach $data_mahasiswa_krs as $mhs}
                        <tr class="ui-widget-content">
                            <td class="center"><span>{$index}</span></td>
                            <td><span>{$mhs['NIM']}</span></td>
                            <td><span>{$mhs['NAMA']}</span></td>
                            <td class="center"><span>{$mhs['ANGKATAN']} ({$mhs['NM_JENJANG']})</span></td>
                            <td class="center"><span>{$mhs['IPS']}</span></td>
                            <td class="center"><span>{$mhs['IPK']}</span></td>
                            <td class="center"><span>{$mhs['TOTAL_SKS']}</span></td>
                            <td class="center">
                                {if $mhs['STATUS']==null&&$mhs['STATUS_APV']==null}
                                    <span>Belum KRS</span>							
                                {else if $mhs['STATUS']<=1}
                                    <span 
                                        {if $mhs['STATUS_APV']<1} 
                                            style="color: #ff3333" 
                                        {else if $mhs['STATUS_APV']==1} 
                                            style="color: #008000" 
                                        {/if}>
                                        KRS
                                    </span>
                                {else if $mhs['STATUS']<=2}
                                    <span 
                                        {if $mhs['STATUS_APV']<1} 
                                            style="color: orange" 
                                        {else if $mhs['STATUS_APV']==1} 
                                            style="color: #008866" 
                                        {/if}>
                                        KPRS
                                    </span>
                                {else if $mhs['STATUS']>=2}
                                    <span 
                                        {if $mhs['STATUS_APV']<1} 
                                            style="color: brown" 
                                        {else if $mhs['STATUS_APV']==1} 
                                            style="color: #00ac99" 
                                        {/if}>
                                        KRS Akademik
                                    </span>
                                {/if}
                            </td>
                            <td style="text-align: center">
                                {if $mhs['STATUS_APV']==1}
                                    <a href="approval.php?mode=krs&id={$mhs['ID_MHS']}" style="padding: 3px;width: 110px;" class="status ui-button ui-state-default ui-corner-all">Sudah Disetujui</a>
                                {else if $mhs['STATUS_APV']<1&&$mhs['STATUS_APV']>0}
                                    <a href="approval.php?mode=krs&id={$mhs['ID_MHS']}" style="padding: 3px;width: 110px;" class="status ui-button ui-state-default ui-corner-all">Belum Semua</a>
                                    <br/>
                                    <input class="check_krs" type="checkbox" name="apvkrs{$index}" />
                                {else}
                                    <a href="approval.php?mode=krs&id={$mhs['ID_MHS']}" style="padding: 3px;width: 110px;" class="status ui-button ui-state-default ui-corner-all" rel="krs">Belum Disetujui</a>
                                    <br/>
                                    <input class="check_krs" type="checkbox" name="apvkrs{$index}" />
                                {/if}
                                <input type="hidden" name="mhs{$index++}" value="{$mhs.ID_MHS}"/>
                            </td>
                        </tr>
                    {foreachelse}
                        <tr>
                            <td colspan="10" class="kosong">Data Masih Kosong</td>
                        </tr>
                    {/foreach}
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10" style="text-align: center">
                            <input type="submit" style="padding: 10px;" class="status ui-button ui-state-default ui-corner-all" value="Approve All" />
                        </td>
                    </tr>
                </tfoot>
            </table>
            <input type="hidden" name="jumlah" value="{$index}"/>
            <input type="hidden" name="mode" value="approve-all"/>
        </form>
    </div>
{else}
    <div class="center_title_bar">Kartu Rencana Studi Mahasiswa</div> 
    <div >
        <table style="width: 98%;">
            <tr class="ui-widget-header">
                <th colspan="6" class="header-coloumn">Detail Mahasiswa</th>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center"><img width="280" height="340" src="/foto_mhs/{$data_mahasiswa.NIM_MHS}.JPG" /></td>
            </tr>
            <tr>
                <td>NAMA</td>
                <td style="width: 230px;">: {$data_mahasiswa.NM_PENGGUNA}</td>
                <td>IPS</td>
                <td>: {$data_mahasiswa.IPS}</td>
                <td>TOTAL SKS</td>
                <td>: {$data_mahasiswa.TOTAL_SKS}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>: {$data_mahasiswa.NIM_MHS}</td>
                <td>IPK</td>
                <td>: {$data_mahasiswa.IPK}</td>
                <td>ANGKATAN</td>
                <td>: {$data_mahasiswa.THN_ANGKATAN_MHS}</td>           
            </tr>
            <tr>
                <td>PROGRAM STUDI</td>
                <td colspan="5">:({$data_mahasiswa.NM_JENJANG}) {$data_mahasiswa.NM_PROGRAM_STUDI}</td>
            </tr>
        </table>
        <form name="form_krs" id="form_krs" action="perwalian.php" method="post">
            <table class="tablesorter" >
                <thead>
                    <tr class="ui-widget-header">
                        <th colspan="7" class="header-coloumn">Detail KRS</th>
                    </tr>
                    <tr>
                        <th>NO.</th>
                        <th>KODE MTK</th>
                        <th colspan="2">NAMA MATA KULIAH</th>
                        <th>SKS MTK</font></th>
                        <th>KELAS</font></th>
                        <th>PERSETUJUAN</th>
                    </tr>
                </thead>
                <tbody>
                    {$index=1}
                    {$total_sks=0}
                    {$total_sks_mhs=0}
                    {if $data_krs!=''}
                        {foreach $data_krs as $krs}
                            {if $krs['STATUS_APV']=='1'}
                                {$total_sks =$total_sks+$krs['SKS']}
                            {/if}
                            {$total_sks_mhs =$total_sks_mhs+$krs['SKS']}
                            <tr class="ui-widget-content">
                                <td>{$index}</td>
                                <td>{$krs['KODE_MK']}</td>
                                <td colspan="2">{$krs['NAMA_MK']} 
                                    {if $krs['STATUS']=='2'}
                                        ( <span style="color:orange">KPRS</span> )
                                    {/if} 
                                    {if $krs['STATUS_ULANG']=='U'}
                                        ( <span style="color:#ff3333">Mengulang</span> )
                                    {/if}
                                </td>
                                <td>{$krs['SKS']}</td>
                                <td>{$krs['NM_KELAS']}</td>
                                <td class="center">
                                    <input type="checkbox" disabled="true" name="confirm{$index}" {if $krs['STATUS_APV']==1}checked="true"{/if} value="{$krs['ID_PENGAMBILAN_MK']}" /> Setujui
                                    <input type="hidden" name="not_confirm{$index++}" value="{$krs['ID_PENGAMBILAN_MK']}" />
                                </td> 
                            </tr>
                        {/foreach}
                    {else}
                        <tr>
                            <td colspan="7" style="text-align: center;color: #fb6666;"><b>KRS MAHASISWA BELUM ADA</b></td>
                        </tr>
                    {/if}
                </tbody>
                <tfoot>
                    <tr class="ui-widget-content">
                        <td colspan="7">
                            <br/>
                            Total SKS Diambil Approve Dosen : <b>{$total_sks}</b><br/><br/>
                            Total SKS Diambil Mahasiswa : <b>{$total_sks_mhs}</b><br/><br/>
                            Total SKS Maksimum : <b>{$data_krs[0]['SKS_MAX']}</b><br/>
                            <a href="approval.php" style="padding: 5px;float: right;" class="kembali ui-button ui-state-default ui-corner-all">Kembali</a>
                            <input type="hidden" name="krs" value="1"/>
                            <input type="hidden" name="sum" value="{$sum_data}" /> 
                        </td>
                    </tr>
                </tfoot>
            </table>
        </form>
    </div>
{/if}
{literal}
    <script>
        $('#check_all_krs').click(function() {
            if ($(this).is(':checked')) {
                $('.check_krs').attr('checked', true);
            } else {
                $('.check_krs').attr('checked', false);
            }
        });
    </script>
{/literal}