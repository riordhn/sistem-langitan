{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
{literal}
    <style>
        .center{ text-align:center;}
    </style>
{/literal}
<div class="center_title_bar">Perwalian Akademik</div>
<!-- <div style="margin:5px;">
    <a style="padding: 5px;" class="ui-button ui-state-default ui-corner-all" href="sms.php?view=perwalian" > Kirim SMS Ke Semua Mahasiswa Perwalian</a>
</div> -->
{if empty($smarty.get.id_mhs)}
    <table class="ui-widget-content tablesorter" style="width: 100%;padding: 5px">
        <thead>
            <tr class="ui-widget-header">
                <th colspan="8" class="header-coloumn">Perwalian Akademik Mahasiswa</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>NIM</th>
                <th>NAMA</th>
                <th>ANGKATAN</th>
                <th>STATUS AKADEMIK</th>
                <th>STATUS KRS</th>
                <th>KHS</th>
                <th>DETAIL</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_mahasiswa_wali as $data}
                <tr>
                    <td>{$data@index+1}</td>
                    {if $data.MOBILE_MHS == null}
                        <td>{$data.NIM_MHS}</td>
                    {else}
                        <td><a style="padding: 3px;" class="ui-button ui-state-default ui-corner-all" title="SMS Ke Mahasiswa" href="sms.php?view=index&mhs={$data.NM_PENGGUNA}&mobile={$data.MOBILE_MHS}">{$data.NIM_MHS}</a></td>
                        {/if}
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>{$data.THN_ANGKATAN_MHS} ({$data.NM_JENJANG})</td>
                    <td>{$data.NM_STATUS_PENGGUNA}</td>
                    <td>
                        {if $data.STATUS_KRS>0}
                            <span style="color: #008000">Sudah KRS</span>
                        {else}
                            <span style="color: #FF0000">Belum KRS</span>
                        {/if}
                    </td>
                    <td class="center" style="width: 90px;">
                        <a href="perwalian.php?mode=lihat-khs&id_mhs={$data.ID_MHS}" style="padding: 3px;" class="ui-button ui-state-default ui-corner-all">Lihat KHS</a>
                    </td>				
                    <td class="center" style="width: 70px;">
                        <a title="Detail Data Akademik Mahasiswa"  href="mahasiswa-wali.php?mode=detail&id_mhs={$data.ID_MHS}" style="padding: 3px;" class="status ui-button ui-state-default ui-corner-all">Detail</a>
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="8" class="kosong">Data Kosong</td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{else}
    <div style="margin: 5px">
        <span class="ui-button ui-corner-all ui-state-default" style="padding:4px;cursor:pointer;margin:2px;" {if isset($smarty.get.rekap)}onclick="$('#content').load('rekap-pencarian.php')"{else}onclick="history.back(-1)"{/if} >Kembali</span>
    </div>
    <table class="ui-widget ui-widget-content" style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="6" class="header-coloumn">Detail Status Akademik Mahasiswa</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>Semester</th>
            <th>Status</th>
            <th>NO SK</th>
            <th>TGL SK</th>
            <th>Keterangan</th>
        </tr>
        {foreach $data_admisi as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
                <td>{$data.NM_STATUS_PENGGUNA}</td>
                <td>{$data.NO_SK}</td>
                <td>{$data.TGL_SK}</td>
                <td>{$data.KETERANGAN}</td>
            </tr>
        {/foreach}   
    </table>
    <br/><hr style="width:90%"/><br/>
    <table class="ui-widget ui-widget-content" style="width: 100%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Detail Biodata Mahasiswa</th>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <img src="{$foto}" width="180" height="230"/>
            </td>
        </tr>
        <tr>
            <td>NAMA</td>
            <td>{$data_biodata.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>{$data_biodata.NIM_MHS}</td>
        </tr>
        <tr>
            <td>ANGKATAN</td>
            <td>{$data_biodata.THN_ANGKATAN_MHS}</td>
        </tr>
        <tr>
            <td>JENJANG</td>
            <td>{$data_biodata.NM_JENJANG}</td>
        </tr>
        <tr>
            <td>FAKULTAS</td>
            <td>{$data_biodata.NM_FAKULTAS|upper}</td>
        </tr>
        <tr>
            <td>PROGRAM STUDI</td>
            <td>{$data_biodata.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td>TEMPAT/TANGGAL LAHIR</td>
            <td> {$data_biodata.TEMPAT_LAHIR} / {$data_biodata.TGL_LAHIR_PENGGUNA|date_format:"%d %B %Y"}</td>
        </tr>
        <tr>
            <td>KONTAK</td>
            <td>{$data_biodata.MOBILE_MHS}</td>
        </tr>
        <tr>
            <td>KELAMIN</td>
            <td>
                {if $data_biodata.KELAMIN_PENGGUNA==1}
                    Laki-laki
                {else}
                    Perempuan
                {/if}
            </td>
        </tr>
        <tr>
            <td>EMAIL</td>
            <td>{$data_biodata.EMAIL_PENGGUNA}</td>
        </tr>
        <tr>
            <td>BLOG MAHASISWA</td>
            <td>{$data_biodata.BLOG_PENGGUNA}</td>
        </tr>
        <tr>
            <td>ALAMAT MAHASISWA</td>
            <td>{$data_biodata.ALAMAT_MHS}</td>
        </tr>
        <tr>
            <td>NAMA AYAH</td>
            <td>{$data_biodata.NM_AYAH_MHS}</td>
        </tr>
        <tr>
            <td>ALAMAT AYAH</td>
            <td>{$data_biodata.ALAMAT_AYAH_MHS}</td>
        </tr>
        <tr>
            <td>PENGHASILAN ORTU</td>
            <td>{number_format($data_biodata.PENGHASILAN_ORTU_MHS)}</td>
        </tr>
    </table>
    <br/><hr style="width:90%"/><br/>
    <table style="width: 100%;">
        <tr class="ui-widget-header">
            <th colspan="6" class="header-coloumn">Detail Akademik Mahasiswa</th>
        </tr>
        <tr>
            <td>NAMA</td>
            <td style="width: 230px;">: {$data_mhs_status.NM_PENGGUNA}</td>
            <td>IPS</td>
            <td>: {$data_mhs_status.IPS}</td>
            <td>TOTAL SKS</td>
            <td>: {$data_mhs_status.TOTAL_SKS}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>: {$data_mhs_status.NIM_MHS}</td>
            <td>IPK</td>
            <td>: {$data_mhs_status.IPK}</td>
            <td>ANGKATAN</td>
            <td>: {$data_mhs_status.THN_ANGKATAN_MHS}</td>           
        </tr>
        <tr>
            <td>PROGRAM STUDI</td>
            <td colspan="5">: ({$data_mhs_status.NM_JENJANG}) {$data_mhs_status.NM_PROGRAM_STUDI}</td>
        </tr>
    </table>
    <br/><hr style="width:90%"/><br/>
    <table  class="ui-widget ui-widget-content" style="width:100% ">
        <tr class="ui-widget-header">
            <th colspan="6" class="header-coloumn">Detail History Nilai Mahasiswa</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>Semester</th>
            <th>KODE AJAR</th>
            <th>NAMA MATA KULIAH</th>
            <th>SKS</th>
            <th>NILAI</th>
        </tr>
        {foreach $data_history_nilai as $data}
            <tr 
                {if $data.STATUS_ULANG>1&&$data.ULANG_KE>1}
                    style="background-color: #ffcccc"
                {elseif $data.STATUS_ULANG>1&&$data.ULANG_KE==1}
                    style="background-color: #8f8"
                {else}

                {/if}
                >
                <td>{$data@index+1}</td>
                <td>{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
                <td>{$data.KD_MATA_KULIAH}</td>
                <td>{$data.NM_MATA_KULIAH}</td>
                <td>{$data.KREDIT_SEMESTER}</td>
                <td>
                    {if $data.NILAI_HURUF==''}
                        Nilai Kosong
                    {else}
                        {$data.NILAI_HURUF}
                    {/if}
                </td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="6">
                <p style="font-weight: bold">Keterangan : </p>
                <ol>
                    <li>
                        <p>Warna <span class="ui-corner-all" style="background-color: #ffcccc;padding: 3px">Merah</span> : Sudah diulang, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif</p>
                    </li>
                    <li>
                        <p>Warna <span class="ui-corner-all" style="background-color: #8f8;padding: 3px">Hijau</span> : Ulangan, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif</p>
                    </li>
                </ol>
            </td>
        </tr>
    </table>
    <br/><hr style="width:90%"/><br/>
    <table width="100%">
        <tr class="ui-widget-header">
            <th colspan="10" class="header-coloumn">Detail Pembayaran {if $jenis=='cmhs'} Calon {/if} Mahasiswa</th>
        </tr>
        <tr>
            <th>No</th>
            <th>Semester</th>
            <th>Tagihkan</th>
            <th>Total Biaya</th>
            <th>Nama Bank</th>
            <th>Via Bank</th>
            <th>Tanggal Bayar</th>
            <th>Keterangan</th>
            <th>Status Bayar</th>
            <th>Operasi</th>
        </tr>
        {foreach $data_pembayaran as $data}
            <tr class="total ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.NM_SEMESTER}<br/> ({$data.TAHUN_AJARAN})</td>
                <td>
                    {if $data.IS_TAGIH=='Y'}
                        Ya
                    {else}
                        Tidak
                    {/if}
                </td>
                <td class="center">{number_format($data.JUMLAH_PEMBAYARAN)}</td>
                <td>{$data.NM_BANK}</td>
                <td>{$data.NAMA_BANK_VIA}</td>
                <td>{$data.TGL_BAYAR}</td>
                <td>{$data.KETERANGAN}</td>
                <td>{$data.NAMA_STATUS}</td>
                <td>
                    <span onclick="show_detail('{$data@index+1}', '#detail')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Detail</span><br/>
                    {if $smarty.get.mode!='edit'&&$jenis=='mhs'}
                        <span onclick="confirm_delete('{$smarty.get.cari}', '{$data.ID_SEMESTER}', '{$data.TGL_BAYAR}', '{$data.ID_BANK}', '{$data.ID_BANK_VIA}', '{$data.NO_TRANSAKSI}', '{$data.KETERANGAN}', '{$data.IS_TAGIH}')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Delete</span><br/>
                    {/if}
                    {if $jenis=='mhs'}
                        <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="history-bayar.php?mode=edit&id_semester={$data.ID_SEMESTER}&tgl_bayar={$data.TGL_BAYAR}&id_bank={$data.ID_BANK}&id_bank_via={$data.ID_BANK_VIA}&no_transaksi={$data.NO_TRANSAKSI}&keterangan={$data.KETERANGAN}&cari={$smarty.get.cari}&is_tagih={$data.IS_TAGIH}">Edit</a>
                    {/if}
                </td>
            </tr>
            <tr id="detail{$data@index+1}" style="display: none">
                <td colspan="10">
                    <table width="95%">
                        <tr class="ui-widget-header" style="color: white">
                            <th>Nama Biaya</th>
                            <th>Besar Biaya</th>
                                {if $jenis=='mhs'}
                                <th>Denda Biaya</th>
                                {/if}
                            <th>Tagihkan</th>
                            <th>Nama Bank</th>
                            <th>Via Bank</th>
                            <th>Tanggal Bayar</th>
                            <th>Keterangan</th>
                        </tr>
                        {foreach $data.DATA_PEMBAYARAN as $data_pembayaran}
                            {if $jenis=='cmhs'}
                                {if $data_pembayaran.TGL_BAYAR==NULL}
                                    {$total_tagihan=$total_tagihan+$data_pembayaran.BESAR_BIAYA}
                                {else}
                                    {$total_pembayaran=$total_pembayaran+$data_pembayaran.BESAR_BIAYA}
                                {/if}
                            {else}
                                {if $data_pembayaran.TGL_BAYAR==NULL}
                                    {$total_tagihan=$total_tagihan+$data_pembayaran.BESAR_BIAYA+$data_pembayaran.DENDA_BIAYA}
                                {else}
                                    {$total_pembayaran=$total_pembayaran+$data_pembayaran.BESAR_BIAYA+$data_pembayaran.DENDA_BIAYA}
                                {/if}
                            {/if}
                            <tr>
                                <td>
                                    {$data_pembayaran['NM_BIAYA']}
                                </td>
                                <td>
                                    {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$smarty.get.keterangan==$data.KETERANGAN}
                                        <input type="text" name="besar_biaya{$data_pembayaran@index+1}" value="{number_format($data_pembayaran['BESAR_BIAYA'])}"/>
                                        {$total_detail=$data_pembayaran@index+1}
                                    {else}
                                        {number_format($data_pembayaran['BESAR_BIAYA'])}
                                    {/if}                                            
                                </td>

                                {if $jenis=='mhs'}
                                    <td>
                                        {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$smarty.get.keterangan==$data.KETERANGAN}
                                            <input type="text" name="denda_biaya{$data_pembayaran@index+1}" value="{number_format($data_pembayaran['DENDA_BIAYA'])}"/>
                                            <input type="hidden" name="id_pembayaran{$data_pembayaran@index+1}" value="{$data_pembayaran.ID_PEMBAYARAN}"/>
                                            {$total_detail=$data_pembayaran@index+1}
                                        {else}
                                            {number_format($data_pembayaran['DENDA_BIAYA'])}
                                        {/if}
                                    </td>
                                {/if}


                                <td>
                                    {if $data_pembayaran['IS_TAGIH']=='Y'}
                                        Y
                                    {else}
                                        Tidak
                                    {/if}
                                </td>
                                <td>
                                    {$data_pembayaran['NM_BANK']}
                                </td>
                                <td>
                                    {$data_pembayaran['NAMA_BANK_VIA']}
                                </td>
                                <td>
                                    {$data_pembayaran['TGL_BAYAR']}
                                </td>
                                <td>
                                    {$data_pembayaran['KETERANGAN']}
                                </td>
                            </tr>
                        {/foreach}

                        <tr>
                            <td class="center" colspan="{if $jenis=='cmhs'}8{else}9{/if}">
                                <span onclick="hide_detail('{$data@index+1}', '#detail')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Tutup</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        {foreachelse}
            <tr class="total ui-widget-content">
                <td class="center" colspan="10">
                    <span style="color:red">
                        Data Pembayaran Kosong
                    </span>
                </td>
            </tr>
        {/foreach}
    </table>  
    <br/><hr style="width:90%"/><br/>
    <table  class="ui-widget ui-widget-content" style="width:100% ">
        <tr class="ui-widget-header">
            <th colspan="7" class="header-coloumn">Detail History Kegiatan Mahasiswa</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>SEMESTER</th>
            <th>JENIS KEGIATAN</th>
            <th>PENYELENGGARA KEGIATAN</th>
            <th>NAMA KEGIATAN</th>
            <th>WAKTU KEGIATAN</th>
            <th>NILAI SKOR</th>
        </tr>
        {foreach $data_khp as $d}
            <tr>
                <td>{$d@index+1}</td>
                <td>{$d.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
                <td>{$d.NM_KEGIATAN_1}</td>
                <td>{$d.PENYELENGGARA_KRP_KHP|upper}</td>
                <td>{$d.NM_KRP_KHP|upper}</td>
                <td>{$d.WAKTU_KRP_KHP}</td>
                <td>{$d.SKOR_KRP_KHP}</td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="7" class="kosong">Data Kosong</td>
            </tr>
        {/foreach}
    </table>
    <br/><hr style="width:90%"/><br/>
    <table  class="ui-widget ui-widget-content" style="width:100% ">
        <tr class="ui-widget-header">
            <th colspan="5" class="header-coloumn">Detail History Beasiswa Mahasiswa</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>NAMA BEASISWA</th>
            <th>PERIODE</th>
            <th>KETERANGAN</th>
            <th>STATUS AKTIF</th>
        </tr>
        {foreach $data_beasiswa as $d}
            <tr>
                <td>{$d@index+1}</td>
                <td>{$d.NM_BEASISWA} Oleh {$d.PENYELENGGARA_BEASISWA}</td>
                <td>{$d.TGL_MULAI}-{$d.TGL_SELESAI}</td>
                <td>{$d.KETERANGAN}</td>
                <td>{if $d.BEASISWA_AKTIF==1}Aktif{else}Tidak Aktif{/if}</td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="5" class="kosong">Data Kosong</td>
            </tr>
        {/foreach}
    </table>
    <br/><hr style="width:90%"/><br/>
    <table  class="ui-widget ui-widget-content" style="width:100% ">
        <tr class="ui-widget-header">
            <th colspan="6" class="header-coloumn">Detail Keuangan Mahasiswa Bidik Misi</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>TGL POSTING</th>
            <th>POS TRANSAKSI</th>
            <th>JENIS TRANSAKSI</th>
            <th>KETERANGAN</th>
            <th>NOMINAL</th>
        </tr>
        {foreach $data_bidik_misi_keu as $d}
            <tr>
                <td>{$d@index+1}</td>
                <td>{$d.TGL_POSTING}</td>
                <td>{$d.POS_TRANSAKSI}</td>
                <td>{$d.JENIS_TRANSAKSI}</td>
                <td>{$d.KETERANGAN}</td>
                <td>{number_format($d.NOMINAL)}</td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="6" class="kosong">Data Kosong</td>
            </tr>
        {/foreach}
    </table>
    <br/><hr style="width:90%"/><br/>
    <table  class="ui-widget ui-widget-content" style="width:100% ">
        <tr class="ui-widget-header">
            <th colspan="6" class="header-coloumn">Detail Kegiatan Mahasiswa Bidik Misi</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>TGL POSTING</th>
            <th>JENIS KEGIATAN</th>
            <th>NAMA KEGIATAN</th>
        </tr>
        {foreach $data_bidik_misi_kegiatan as $d}
            <tr>
                <td>{$d@index+1}</td>
                <td>{$d.TGL_POSTING}</td>
                <td>{$d.JENIS_KEGIATAN}</td>
                <td>{$d.KEGIATAN}</td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="6" class="kosong">Data Kosong</td>
            </tr>
        {/foreach}
    </table>
{/if}
{literal}
    <script type="text/javascript">
        $('.ui-button').hover(
                function() {
                    $(this).addClass('ui-state-highlight')
                },
                function() {
                    $(this).removeClass('ui-state-highlight')
                }
        );
        function show_detail(index, tag) {
            $(tag + index).fadeToggle();
        }
        function hide_detail(index, tag) {
            $(tag + index).fadeOut();
        }

    </script>
{/literal}