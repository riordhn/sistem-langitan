{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>

    <style type="text/css">
        tr.kuning.ui-widget-content>td, table.tablesorter tbody tr.odd.kuning td {
            background-color:#ffff00;
        }
    </style>
{/literal}
<div class="center_title_bar">Perwalian KRS</div> 
{if $id_fakultas!=''}
    {if $mode == 'lihat-khs'}
        <span class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:7px;" onclick="history.back(-1)" >Kembali</span>
        <p></p>
        <table style="width: 90%;">
            <tr class="ui-widget-header">
                <th colspan="6" class="header-coloumn">Detail Mahasiswa</th>
            </tr>
            <tr>
                <td>NAMA</td>
                <td style="width: 230px;">: {$data_mahasiswa.NM_PENGGUNA}</td>
                <td>IPS</td>
                <td>: {$data_mahasiswa.IPS}</td>
                <td>TOTAL SKS</td>
                <td>: {$data_mahasiswa.TOTAL_SKS}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>: {$data_mahasiswa.NIM_MHS}</td>
                <td>IPK</td>
                <td>: {$data_mahasiswa.IPK}</td>
                <td>ANGKATAN</td>
                <td>: {$data_mahasiswa.THN_ANGKATAN_MHS}</td>           
            </tr>
            <tr>
                <td>PROGRAM STUDI</td>
                <td colspan="5">: ({$data_mahasiswa.NM_JENJANG}) {$data_mahasiswa.NM_PROGRAM_STUDI}</td>
            </tr>
        </table>
        <p></p>
        {foreach $data_khs as $khs}            
            <br/><hr style="width:70%"/><br/>
            <table class="tablesorter" style="width: 90%">
                <thead>
                    <tr class="ui-widget-header">
                        <th colspan="4" class="header-coloumn">DETAIL KARTU HASIL STUDI<br/>SEMESTER {$khs.NM_SEMESTER} {$khs.TAHUN_AJARAN}</th>
                    </tr>
                    <tr class="ui-widget-header">
                        <th>NO</th>
                        <th>MATA KULIAH</th>
                        <th>SKS</th>
                        <th>NILAI</th>
                    </tr>
                </thead>
                <tbody>
                    {$sks_semester=0}
                    {foreach $khs.DATA_KHS as $dk}
                        {$sks_semester=$sks_semester+$dk.SKS}
                        {if trim($dk.NILAI) == '-'}
                            <tr class="ui-widget-content kuning" >
                                <td>{$dk@index+1}</td>
                                {if $dk.NAMA_KELAS == '-'}
                                    <td>( {$dk.KODE} ) {$dk.NAMA}</td>
                                {else}
                                    <td>( {$dk.KODE} ) {$dk.NAMA} {$dk.NAMA_KELAS}</td>
                                {/if}
                                <td>{$dk.SKS}</td>
                                <td>{$dk.NILAI}</td>
                            </tr>
                        {else}
                            <tr class="ui-widget-content">
                                <td>{$dk@index+1}</td>
                                {if $dk.NAMA_KELAS == '-' or $dk.NAMA_KELAS == 'Insert Data'}
                                    <td>( {$dk.KODE} ) {$dk.NAMA}</td>
                                {else}
                                    <td>( {$dk.KODE} ) {$dk.NAMA} {$dk.NAMA_KELAS}</td>
                                {/if}
                                <td>{$dk.SKS}</td>
                                <td>{$dk.NILAI}</td>
                            </tr>
                        {/if}
                    {/foreach}
                </tbody>
                <tfoot>
                    <tr class="ui-widget-content">
                        <td colspan="4" class="center">
                            <p style="font-weight: bold;font-size: 1.2em">
                                IPS : {$khs.IPS}<br/>
                                SKS SEMESTER : {$sks_semester}
                            </p>
                        </td>
                    </tr>       
                </tfoot>
            </table>
        {/foreach}
    {else}
        {if $keterangan_perwalian!=''}
            {$keterangan_perwalian}
        {/if}
        <div id="alert" class="ui-widget" style="margin-left:10px;">
            <div class="ui-state-highlight ui-corner-all" style="padding: 5px;width:100%;"> 
                <p>Data IPK, IPS, Total SKS yang kosong atau tidak tampil berarti belum dihitung oleh bagian Pendidikan / Akademik</p>
            </div>
        </div>
        <div>
            <table class="tablesorter" style="width: 100%">
                <thead>
                    <tr class="ui-widget-header">
                        <th colspan="11" class="header-coloumn">Detail Perwalian Mahasiswa</th>
                    </tr>
                    <tr class="ui-widget-header">
                        <th>NO.</th>
                        <th>NIM</th>
                        <th>NAMA MHS</th>
                        <th>ANGKATAN</th>
                        <th>STATUS MHS</th>
                        <th>IPS</th>
                        <th>IPK</th>
                        <th>TOTAL SKS</th>
                        <th>KHS</th>
                        <th>STATUS</th>
                        <th>PERSETUJUAN</th>
                    </tr>
                </thead>
                <tbody>
                    {$index=1}
                    {foreach $data_mahasiswa_krs as $mhs}
                        <tr class="ui-widget-content">
                            <td class="center"><span>{$index++}</span></td>
                            {assign var="image" value="../../foto_mhs/{$nama_singkat}/{$mhs['NIM']}.jpg"} 

                            {assign var="image1" value="../../foto_mhs/{$nama_singkat}/{$mhs['NIM']}.JPG"} 

                            {if file_exists($image)}
                                <td><img src="../../foto_mhs/{$nama_singkat}/{$mhs['NIM']}.jpg" width="61px" /><br/><span>{$mhs['NIM']}</span></td>
                            {else if file_exists($image1)}
                                <td><img src="../../foto_mhs/{$nama_singkat}/{$mhs['NIM']}.JPG" width="61px" /><br/><span>{$mhs['NIM']}</span></td>
                            {else}
                                <td><img src="../../foto_mhs/foto-tidak-ditemukan.png" width="61px" /><br/><span>{$mhs['NIM']}</span></td>
                            {/if}
                            <td><span>{$mhs['NAMA']}</span></td>
                            <td class="center"><span>{$mhs['ANGKATAN']} ({$mhs['NM_JENJANG']})</span></td>
                            <td class="center"><span>{$mhs['NM_STATUS_PENGGUNA']}</span></td>
                            <td class="center"><span>{$mhs['IPS']}</span></td>
                            <td class="center"><span>{$mhs['IPK']}</span></td>
                            <td class="center"><span>{$mhs['TOTAL_SKS']}</span></td>
                            <td class="center" style="width: 80px;">
                                <a href="perwalian.php?mode=lihat-khs&id_mhs={$mhs.ID_MHS}" style="padding: 3px;" class="ui-button ui-state-default ui-corner-all">Lihat KHS</a>
                            </td>
                            <td class="center">
                                {if $mhs['STATUS'] == null && $mhs['STATUS_APV'] == null}
                                    <span>Belum KRS</span>							
                                {else if $mhs['STATUS']<=1}
                                    <span 
                                        {if $mhs['STATUS_APV'] < 1} 
                                            style="color: #ff3333" 
                                        {else if $mhs['STATUS_APV'] == 1} 
                                            style="color: #008000" 
                                        {/if}>
                                        KRS
                                    </span>
                                {else if $mhs['STATUS']<=2}
                                    <span 
                                        {if $mhs['STATUS_APV'] < 1} 
                                            style="color: orange" 
                                        {else if $mhs['STATUS_APV'] == 1} 
                                            style="color: #008866" 
                                        {/if}>
                                        KPRS
                                    </span>
                                {else if $mhs['STATUS']>=2}
                                    <span 
                                        {if $mhs['STATUS_APV'] < 1} 
                                            style="color: brown" 
                                        {else if $mhs['STATUS_APV'] == 1} 
                                            style="color: #00ac99" 
                                        {/if}>
                                        KRS Akademik
                                    </span>
                                {/if}
                            </td>
                            <td style="width: 120px;">
                                {if $mhs['STATUS_APV']==1}
                                    <span id="{$mhs['ID_MHS']}" style="padding: 3px;" class="status ui-button ui-state-default ui-corner-all" rel="krs">Sudah Di Setujui</span>
                                {else if $mhs['STATUS_APV'] < 1 && $mhs['STATUS_APV'] > 0}
                                    <span id="{$mhs['ID_MHS']}" style="padding: 3px;" class="status ui-button ui-state-default ui-corner-all" rel="krs">Belum Di Setujui Semua</span>
                                {else}
                                    <span id="{$mhs['ID_MHS']}" style="padding: 3px;" class="status ui-button ui-state-default ui-corner-all" rel="krs">Belum Di Setujui</span>
                                {/if}
                            </td>
                        </tr>
                    {foreachelse}
                        <tr>
                            <td colspan="11" class="kosong">Data Masih Kosong</td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>
    {/if}
{else}
    <table>
        <tr><td class="ui-state-error" style="padding: 10px;margin: 10px;font-size: 13px;">UNTUK SEMENTARA PERWALIAN FAKULTAS EKONOMI DAN BISNIS DI NON AKTIFKAN</td></tr>
    </table>
{/if}
{literal}
    <script type="text/javascript">
        $(".status").click(function() {
            $.ajax({
                url: 'perwalian.php',
                type: 'post',
                data: 'id_mhs=' + $(this).attr('id') + '&detail=' + $(this).attr('rel'),
                beforeSend: function() {
                    $('#content').html('<div style="width: 90%;" align="center"><img src="../../img/dosen/ajax_loader.gif" /></div>');
                },
                success: function(data) {
                    $('#content').html(data);
                }
            });
        });
    </script>
{/literal}