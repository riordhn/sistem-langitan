{literal}
    <style>
        .center{ text-align:center;}
    </style>
{/literal}
<div class="center_title_bar">Kartu Rencana Studi Mahasiswa</div> 
<div >
<span style="padding: 5px;float: left;margin-bottom: 15px;"  class="kembali ui-button ui-state-default ui-corner-all">Kembali</span>
    <table style="width: 98%;">
        <tr class="ui-widget-header">
            <th colspan="6" class="header-coloumn">Detail Mahasiswa</th>
        </tr>
        <tr>
            {assign var="image" 
                 value="../../foto_mhs/{$nama_singkat}/{$data_mahasiswa.NIM_MHS}.jpg"} 

            {assign var="image1" 
                 value="../../foto_mhs/{$nama_singkat}/{$data_mahasiswa.NIM_MHS}.JPG"}

            {if file_exists($image)} 
                <td colspan="6" style="text-align: center"><img width="160" height="220" src="../../foto_mhs/{$nama_singkat}/{$data_mahasiswa.NIM_MHS}.jpg" /></td>
            {else if file_exists($image1)}
                <td colspan="6" style="text-align: center"><img width="160" height="220" src="../../foto_mhs/{$nama_singkat}/{$data_mahasiswa.NIM_MHS}.JPG" /></td>
            {else}
                <td colspan="6" style="text-align: center"><img width="160" height="220" src="../../foto_mhs/foto-tidak-ditemukan.png" /></td>
            {/if}

        </tr>
        <tr>
            <td>NAMA</td>
            <td style="width: 230px;">: {$data_mahasiswa.NM_PENGGUNA}</td>
            <td>IPS</td>
            <td>: {$data_mahasiswa.IPS}</td>
            <td>TOTAL SKS</td>
            <td>: {$data_mahasiswa.TOTAL_SKS}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>: {$data_mahasiswa.NIM_MHS}</td>
            <td>IPK</td>
            <td>: {$data_mahasiswa.IPK}</td>
            <td>ANGKATAN</td>
            <td>: {$data_mahasiswa.THN_ANGKATAN_MHS}</td>           
        </tr>
        <tr>
            <td>PROGRAM STUDI</td>
            <td>: ({$data_mahasiswa.NM_JENJANG}) {$data_mahasiswa.NM_PROGRAM_STUDI}</td>
            <td>NOMOR HP</td>
            <td colspan="5">: {$data_mahasiswa.MOBILE_MHS}</td>
        </tr>
        <tr>
            <td>TELP AYAH</td>
            <td>: {$data_mahasiswa.TELP_AYAH}</td>
            <td>TELP IBU</td>
            <td colspan="5">: {$data_mahasiswa.TELP_IBU}</td>
        </tr>
        <tr>
            <td colspan="6">
                <span style="padding: 5px;float: right;"  class="kembali ui-button ui-state-default ui-corner-all">Kembali</span>
                <span id="load_transkrip{$id_mhs}" style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all">Lihat Transkrip</span>
            </td>
        </tr> 
    </table>
    <!--
    {if $id_fak==8} 
        <form name="form_krs1" id="form_krs1" action="perwalian.php" method="post">
            <input type="hidden" name="detail" value="krs" >
            <input type="hidden" name="st" value="2" >
            <input type="hidden" name="id_mhs" value="{$id_mhs}" >
            <table style="width:90%;">
                <tr>
                    <td>Tambah Mata Ajar :</td>
                    <td>    <select name="mk">
                            {foreach item="mkt" from=$mk_tawar}
                                {html_options values=$mkt.ID_KELAS_MK output=$mkt.TAMPIL}
                            {/foreach}
                        </select>
                    </td>
                    <td>
                        <input type="submit" name="Tambah" value="Tambah">
                    </td>
                </tr>
            </table>
        </form>
    {/if}
    -->



        <table class="tablesorter">
            <thead>
                <tr class="ui-widget-header">
                    <th colspan="8" class="header-coloumn">TAGIHAN MAHASISWA</th>
                </tr>
                <tr class="ui-widget-content">
                    <th class="center">SEMESTER</th>
                    <th>JUMLAH TAGIHAN MAHASISWA</th>
                    <th>Per Tanggal</th>
                </tr>
            </thead>
            <tbody>
                {foreach $data_tagihan_mahasiswa as $tagihan}
                    <tr class="ui-widget-content">
                        <td class="center">{$tagihan.TAHUN_AJARAN} {$tagihan.NM_SEMESTER}</td>
                        {if $tagihan.JUMLAH_TAGIHAN == '1'}
                            <td class="center">Drop Out</td>
                        {elseif $tagihan.JUMLAH_TAGIHAN == '2'}
                            <td class="center">Mengundurkan Diri</td>
                        {else}
                            <td class="center"><font color="red"><strong>Rp{number_format($tagihan.JUMLAH_TAGIHAN)}</strong></font></td>
                        {/if}
                        <td class="center">{$tagihan.TANGGAL} {$tagihan.WAKTU}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>

    <form name="form_krs" id="form_krs" action="perwalian.php" method="post">
        <table class="tablesorter" >
            <thead>
                <tr class="ui-widget-header">
                    <th colspan="8" class="header-coloumn">Detail KRS</th>
                </tr>
                <tr>
                    <th>NO.</th>
                    <th>KODE MTK</th>
                    <th colspan="2">NAMA MATA KULIAH</th>
                    <th>SKS MTK</font></th>
                    <th>KELAS</font></th>
                    <th>PERSETUJUAN</th>
                    <th>AKSI</th>
                    
                </tr>
            </thead>
            <tbody>
                {$index=1}
                {$total_sks=0}
                {$total_sks_mhs=0}
                {if $data_krs!=''}
                    {foreach $data_krs as $krs}
                        {if $krs['STATUS_APV']=='1'}
                            {$total_sks =$total_sks+$krs['SKS']}
                        {/if}
                        {$total_sks_mhs =$total_sks_mhs+$krs['SKS']}
                        <tr class="ui-widget-content">
                            <td>{$index}</td>
                            <td>{$krs['KODE_MK']}</td>
                            <td colspan="2">{$krs['NAMA_MK']} 
                                {if $krs['STATUS']=='2'}
                                    ( <span style="color:orange">KPRS</span> )
                                {/if} 
                                {if $krs['STATUS_ULANG']=='U'}
                                    ( <span style="color:#ff3333">Mengulang</span> )
                                {/if}
                            </td>
                            <td>{$krs['SKS']}</td>
                            <td>{$krs['NM_KELAS']}</td>
                            <td class="center">
                                {if $is_krs == 1}
                                    <input type="checkbox" name="confirm{$index}" {if $krs['STATUS_APV']==1}checked="true"{/if} value="{$krs['ID_PENGAMBILAN_MK']}" /> Setujui
                                    <input type="hidden" name="not_confirm{$index++}" value="{$krs['ID_PENGAMBILAN_MK']}" />
                                {else}
                                    {if $krs['STATUS_APV']==1}
                                        <span style="color:orange" {$index++}>Periode Perwalian Selesai (Approved)</span>
                                    {else}
                                        <span style="color:red" {$index++}>Periode Perwalian Selesai (Not Approved)</span>
                                    {/if}
                                {/if}
                            </td>
                            <td>
                                {if $is_krs == 1}
                                    <span id="{$id_mhs}" style="padding: 5px;float: right;width: 110px;color:red;" class="status ui-button ui-state-default ui-corner-all" rel="krs" st="1" id_pmk="{$krs['ID_PENGAMBILAN_MK']}">Hapus</span>
                                {else}
                                    <span style="color:red">-</span>
                                {/if}

                            </td> 
                            <!--
                            {if $id_fakultas== 8}
                           
                                <td class="center">
                                    <span id="{$krs['ID_MHS']}" style="padding: 3px;width: 110px;" class="status ui-button ui-state-default ui-corner-all" rel="krs" st="1" id_pmk="{$krs['ID_PENGAMBILAN_MK']}">HAPUS</span>
                                </td>

                            {else}
                                <td class="center">
                                    -
                                </td>
                            {/if}
                             -->
                        </tr>
                    {/foreach}
                {else}
                    <tr>
                        <td colspan="8" style="text-align: center;color: #fb6666;"><b>KRS MAHASISWA BELUM ADA</b></td>
                    </tr>
                {/if}
            </tbody>
            <tfoot>
                <tr class="ui-widget-content">
                    <td colspan="8">
                        {if $is_krs == 1}
                            <span style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all" onclick="uncheck_all()">Hilangkan Centang Semua</span>
                            <span style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all" onclick="check_all()">Centang Semua</span>
                            {if $total_sks_mhs < 24}
                                <span id={$id_mhs} style="padding: 5px;float: right;" class="tambahkrs ui-button ui-state-default ui-corner-all" rel="krs" st="load_kelas">Tambah KRS</span>
                            {/if}
                            <!-- id bikin baru -->
                            <!-- <span id="load_transkrip{$id_mhs}" style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all">Tambah KRS</span> -->
                        {else}
                            <span style="color:red">Periode Perwalian Selesai</span>
                        {/if}
                    </td>
                </tr>
                <tr class="ui-widget-content">
                    <td colspan="8">
                        {if ! empty($keterangan_krs)}
                            {if $keterangan_krs!=''}
                                {$keterangan_krs}
                            {/if}
                        {/if}
                        <br/>
                        Total SKS Diambil Approve Dosen : <b>{$total_sks}</b><br/><br/>
                        Total SKS Diambil Mahasiswa : <b>{$total_sks_mhs}</b><br/><br/>
                        Total SKS Maksimum : <b>{$data_krs[0]['SKS_MAX']}</b><br/>
                        <span style="padding: 5px;float: right;" class="kembali ui-button ui-state-default ui-corner-all">Kembali</span>
                        <input type="hidden" name="krs" value="1"/>
                        <input type="hidden" name="id_mhs" value="{$id_mhs}"/>
                        <input type="hidden" name="id_semester" value="{$data_krs[0]['ID_SEMESTER']}"/>
                        {if $is_krs == 1}
                            <span id="save" style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all">Simpan KRS</span>
                        {/if}
                        <input type="hidden" name="sum" value="{$sum_data}" /> 
                    </td>
                </tr>
            </tfoot>
        </table>

        <table class="tablesorter">
            <thead>
                <tr class="ui-widget-header">
                    <th colspan="8" class="header-coloumn">AKTIVITAS MAHASISWA</th>
                </tr>
                <tr class="ui-widget-content">
                    <th class="center">SEMESTER</th>
                    <th>STATUS</th>
                    <th>SKS SEMESTER</th>
                    <th>IPS</th>
                    <th>SKS TOTAL</th>
                    <th>IPK</th>
                </tr>
            </thead>
            <tbody>
                {foreach $data_aktivitas_mahasiswa as $status}
                    <tr class="ui-widget-content">
                        <td>{$status.TAHUN_AJARAN} {$status.NM_SEMESTER}</td>
                        <td>{$status.NM_STATUS_PENGGUNA}</td>
                        <td style="text-align: center">{$status.SKS_SEMESTER}</td>
                        <td style="text-align: center">{if $status.IPS != ''}{$status.IPS|floatval}{/if}</td>
                        <td style="text-align: center">{$status.SKS_TOTAL}</td>
                        <td style="text-align: center">{$status.IPK|floatval}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>

        <table class="tablesorter">
            <thead>
                <tr class="ui-widget-header">
                    <th colspan="8" class="header-coloumn">PESAN KRS</th>
                </tr>
                <tr class="ui-widget-content">
                    <th class="center">NO.</th>
                    <th>JUDUL</th>
                    <th colspan="6">ISI PESAN</th>
                </tr>
            </thead>
            <tbody>
                {$index=1}
                {foreach $data_pesan_krs as $data}
                    <tr>
                        <td>{$index++}</td>
                        <td>{$data.JUDUL_PESAN}</td>
                        <td colspan="6">{$data.ISI_PESAN}</td>
                    </tr>
                {/foreach}
                <tr class="ui-widget-content">
                    <td colspan="7" class="center">
                        <span id="create_message{$id_mhs}" style="padding: 5px;" class="ui-button ui-state-default ui-corner-all">Buat Pesan</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div id="dialog_transkrip{$id_mhs}" title="Transkrip Mahasiswa"  style="padding: 20px;">
    <table class="tablesorter" style="width:550px ">
        <thead>
            <tr class="ui-widget-header">
                <th>NO.</th>
                <th>KODE AJAR</th>
                <th>NAMA MATA KULIAH</th>
                <th>SKS</th>
                <th>NILAI</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_transkrip as $data}
                <tr 
                    {if $data.STATUS_ULANG>1&&$data.ULANG_KE>1}
                        style="background-color: #fb6666"
                    {elseif $data.STATUS_ULANG>1&&$data.ULANG_KE==1}
                        style="background-color: #00FF00"
                    {else}

                    {/if}
                    >
                    <td>{$data@index+1}</td>
                    <td>{$data.KD_MATA_KULIAH}</td>
                    <td>{$data.NM_MATA_KULIAH}</td>
                    <td>{$data.KREDIT_SEMESTER}</td>
                    <td>
                        {if $data.NILAI_HURUF==''}
                            E
                        {else}
                            {$data.NILAI_HURUF}
                        {/if}
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="5" class="kosong">Data Masih Kosong</td>
                </tr>
            {/foreach}
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5">
                    <p style="font-weight: bold">Keterangan : </p>
                    <ol>
                        <li>
                            <p>Warna <span class="ui-corner-all" style="background-color: #fb6666;padding: 3px">Merah</span> : Sudah diulang, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif</p>
                        </li>
                        <li>
                            <p>Warna <span class="ui-corner-all" style="background-color: #00FF00;padding: 3px">Hijau</span> : Ulangan, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif</p>
                        </li>
                    </ol>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <span id="dialog_transkrip_close{$id_mhs}" style="padding: 5px;float: right;" class="ui-button ui-state-default ui-corner-all">Tutup</span>
                </td>
            </tr>
        </tfoot>
    </table>
</div>                
<div id="dialog_message{$id_mhs}" title="Message For KRS" >
    <form id="dialog_message_form{$id_mhs}" action="perwalian.php" method="post">
        <table style="width: 96%">
            <tr>
                <td>
                    <label for="judul_pesan">Judul</label>
                </td>
                <td>                        
                    <input type="text" size="80" name="judul_pesan" id="judul_pesan"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="pesan">Isi Pesan</label>
                </td>
                <td>
                    <textarea style="resize: none;width: 485px;" name="isi_pesan" rows="5" id="isi_pesan"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <input type="hidden" name="send_message" value="1" />
                    <input type="hidden" id="id_mhs" name="id_mhs" value="{$id_mhs}"/>
                    <span id="dialog_message_send{$id_mhs}" style="padding: 5px;" class="ui-button ui-state-default ui-corner-all">Kirim Pesan</span>
                    <span id="dialog_message_cancel{$id_mhs}" style="padding: 5px;" class="ui-button ui-state-default ui-corner-all">Batal</span>
                </td>                    
            </tr>
        </table>
    </form>
</div>
{literal}
    <script type="text/javascript">

        function check_all(chk)
        {
            x = $(':checkbox');
            for (i = 0; i < x.length; i++)
                x[i].checked = true;
        }

        function uncheck_all(chk)
        {
            x = $(':checkbox');
            for (i = 0; i < x.length; i++)
                x[i].checked = false;
        }
        $(function() {
            var id_mhs = $('#id_mhs').val();
            $('.ui-button').hover(
                    function() {
                        $(this).addClass('ui-state-hover')
                    },
                    function() {
                        $(this).removeClass('ui-state-hover')
                    }
            );

            $('#dialog_message' + id_mhs).dialog({
                autoOpen: false,
                height: 310,
                width: 680,
                draggable: false,
                resizable: false,
                modal: true
            });

            $('#dialog_transkrip' + id_mhs).dialog({
                autoOpen: false,
                width: 620,
                draggable: false,
                resizable: false,
                modal: true
            });

            $('#dialog_message_send' + id_mhs).click(function() {
                if ($('#dialog_message_form' + id_mhs).valid()) {
                    submit_form_message();
                    $('#dialog_message' + id_mhs).dialog("close");
                } else {
                    return false;
                }
            });

            $("#dialog_message_cancel" + id_mhs).click(function() {
                $("#dialog_message" + id_mhs).dialog("close");
            });
            function submit_form_message() {
                $('#dialog_message_form' + id_mhs).submit();
            }
            $("#dialog_transkrip_close" + id_mhs).click(function() {
                $("#dialog_transkrip" + id_mhs).dialog("close");
            });

            $("#create_message" + id_mhs).click(function() {
                $("#dialog_message" + id_mhs).dialog("open");
            });

            $("#load_transkrip" + id_mhs).click(function() {
                $("#dialog_transkrip" + id_mhs).dialog("open");
                $("#dialog_transkrip" + id_mhs).dialog("moveToTop");
            });

            $('#save').click(function() {
                $('#form_krs').submit();
            });
            $('.kembali').click(function() {
                $.ajax({
                    url: 'perwalian.php',
                    dataType: 'html',
                    beforeSend: function() {
                        $('#content').html('<div style="width: 80%;" align="center"><img src="../../img/dosen/ajax_loader.gif" /></div>');
                    },
                    success: function(data) {
                        $('#content').html(data);
                    }
                });

            });
            //utk menghapus KRS
            $('.status').click(function() {
                if (confirm("Anda Yakin Menghapus KRS Tersebut?") == true) {
                    $.ajax({
                        url: 'perwalian.php',
                        type: 'post',
                        data: 'id_mhs=' + $(this).attr('id') + '&detail=' + $(this).attr('rel') + '&st=' + $(this).attr('st') + '&id_pmk=' + $(this).attr('id_pmk'),
                        beforeSend: function() {
                            $('#content').html('<div style="width: 90%;" align="center"><img src="../../img/dosen/ajax_loader.gif" /></div>');
                        },
                        success: function(data) {
                            $('#content').html(data);
                        }
                    });
                } else {
                    //x = "You pressed Cancel!";
                }
                
            });
            // utk menambah krs mhs
            $('.tambahkrs').click(function() {
                $.ajax({
                    url: 'perwalian.php',
                    type: 'post',
                    data: 'id_mhs=' + $(this).attr('id') + '&detail=' + $(this).attr('rel') + '&st=' + $(this).attr('st'),
                    beforeSend: function() {
                        $('#content').html('<div style="width: 90%;" align="center"><img src="../../img/dosen/ajax_loader.gif" /></div>');
                    },
                    success: function(data) {
                        $('#content').html(data);
                    }
                });
                
            });
            $("#dialog_message_form" + id_mhs).validate({
                rules: {
                    judul_pesan: {required: true},
                    isi_pesan: {required: true}
                }
            });
        })

    </script>
{/literal}