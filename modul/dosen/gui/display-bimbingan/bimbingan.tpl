<div class="center_title_bar">Bimbingan Tugas Akhir</div> 

{if $smarty.get.mode=='detail-bimbingan'}
    <a style="padding:5px;margin-left: 8px" class="ui-button ui-state-default ui-corner-all" href="bimbingan.php" >Kembali</a>
    <table style="width: 90%">
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="2">Detail Tugas Akhir Mahasiswa<br/>{$db.NM_JENIS_PEMBIMBING}</th>
        </tr>
        <tr>
            <td style="width: 20%;font-weight: bold;font-size:0.9em">NAMA</td>
            <td style="width: 80%">{$tugas_akhir.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td style="width: 20%;font-weight: bold;font-size:0.9em">NIM</td>
            <td>{$tugas_akhir.NIM_MHS}</td>
        </tr>
        <tr>
            <td style="width: 20%;font-weight: bold;font-size:0.9em">PRODI</td>
            <td>{$tugas_akhir.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td style="width: 20%;font-weight: bold;font-size:0.9em">ANGKATAN</td>
            <td>{$tugas_akhir.THN_ANGKATAN_MHS}</td>
        </tr>
        <tr>
            <td style="width: 20%;font-weight: bold;font-size:0.9em">TINGKAT SEMESTER</td>
            <td>{$tugas_akhir.SEMESTER}</td>
        </tr>
        <tr>
            <td style="width: 20%;font-weight: bold;font-size:0.9em">IPK</td>
            <td>{$tugas_akhir.IPK}</td>
        </tr>
        <tr>
            <td style="width: 20%;font-weight: bold;font-size:0.9em">JENIS TUGAS AKHIR</td>
            <td>{$tugas_akhir.NM_TIPE_TA}</td>
        </tr>
        <tr>
            <td style="width: 20%;font-weight: bold;font-size:0.9em">JUDUL</td>
            <td>{$tugas_akhir.JUDUL_TUGAS_AKHIR}</td>
        </tr>
        <tr>
            <td class="center ui-widget-header" colspan="2">TIM BIMBINGAN</td>
        </tr>
        {foreach $tugas_akhir.PEMBIMBING as $tpb}
            <tr>
                <td style="width: 20%;font-weight: bold;font-size:0.9em">{$tpb.NM_JENIS_PEMBIMBING|upper}</td>
                <td>{$tpb.NIP_DOSEN} - {$tpb.GELAR_DEPAN} {$tpb.NM_PENGGUNA} {$tpb.GELAR_BELAKANG}</td>
            </tr>
        {/foreach}
        <tr>
            <td style="width: 20%;font-weight: bold;font-size:0.9em">PENGUJI</td>
            <td>
                <ul style="margin-left:5px;padding-left: 10px ">
                    {foreach $tugas_akhir.PENGUJI as $tpu}
                        <li>
                            {$tpu.NIP_DOSEN} - {$tpu.GELAR_DEPAN} {$tpu.NM_PENGGUNA} {$tpu.GELAR_BELAKANG}
                        </li>
                    {/foreach}
                </ul>
            </td>
        </tr>
        <tr>
            <td class="center ui-widget-header" colspan="2">PENGERJAAN</td>
        </tr>
        <tr>
            <td style="width: 20%;font-weight: bold;font-size:0.9em">KESELURUHAN PROGRESS</td>
            <td>
                {if $tugas_akhir.STATUS_BIMBINGAN>0}
                    <div id="progress-ta">
                        <p>
                            <input type="hidden" id="id_mhs" value="{$smarty.get.id}" />
                            <input type="hidden" id="id_ta" value="{$tugas_akhir.ID_TUGAS_AKHIR}" />
                            <input type="hidden" id="p_value" value="{if $tugas_akhir.PROGRESS==''}0{else}{$tugas_akhir.PROGRESS}{/if}" />
                            <label for="amount">Prosentase: </label>
                            <input type="text" id="amount" value="{if $tugas_akhir.PROGRESS==''}0{else}{$tugas_akhir.PROGRESS}{/if} %" style="border: 0; color: #f6931f; font-weight: bold;" />
                        </p>

                        <div id="slider" style="margin: 8px"></div>
                        {literal}
                            <script type="text/javascript">
                                $(function() {
                                    var id_ta = $('#id_ta').val();
                                    $("#slider").slider({
                                        orientation: "horizontal",
                                        range: "min",
                                        min: 0,
                                        max: 100,
                                        value: $('#p_value').val(),
                                        stop: function(event, ui) {
                                            $.ajax({
                                                url: 'bimbingan.php?mode=update-progress&id=',
                                                type: 'post',
                                                data: 'id_ta=' + id_ta + '&p_value=' + ui.value,
                                            });
                                            $("#amount").val($("#slider").slider("value") + " %");
                                        }
                                    });
                                });
                            </script>
                        {/literal}
                    </div>
                {else}
                    {literal}
                        <style>
                            .ui-progressbar {
                                position: relative;
                            }
                            .progress-label {
                                position: absolute;
                                left: 48%;
                                top: 4px;
                                font-weight: bold;
                                text-shadow: 1px 1px 0 #fff;
                            }
                        </style>
                    {/literal}
                    <input type="hidden" id="p_value" value="{if $tugas_akhir.PROGRESS==''}0{else}{$tugas_akhir.PROGRESS}{/if}" />
                    <div id="progressbar"><div class="progress-label">{if $tugas_akhir.PROGRESS==''}0{else}{$tugas_akhir.PROGRESS}{/if} %</div></div>
                    {literal}
                        <script>
                            $("#progressbar").progressbar({
                                value: $('#p_value').val()
                            });
                        </script>
                    {/literal}
                {/if}
            </td>
        </tr>
    </table>

{else}
    <div id="tabs" style="width: 98%">
        <ul>
            <li><a class="disable-ajax" href="#pembimbing">Sebagai Pembimbing </a></li>
            <li><a class="disable-ajax" href="#penguji">Sebagai Penguji</a></li>
        </ul>
        <div id="pembimbing">
            {foreach $data_bimbingan as $db}
                {if count($db.DATA_MHS)>0}
                    <table style="width: 98%">
                        <tr class="ui-widget-header">
                            <th class="header-coloumn" colspan="10">Daftar Mahasiswa Bimbingan Sebagai <br/>{$db.NM_JENIS_PEMBIMBING}</th>
                        </tr>
                        <tr class="ui-widget-header">
                            <th>No</th>
                            <th>NIM (ANGKATAN)</th>
                            <th>NAMA</th>
                            <th>PRODI</th>
                            <th>DATA AKAD</th>
                            <th>JADWAL UJIAN</th>
                            <th>DETAIL</th>
                        </tr>
                        {foreach $db.DATA_MHS as $d}
                            <tr>
                                <td>{$d@index+1}</td>
                                <td>{$d.NIM} ({$d.ANGKATAN})</td>
                                <td>{$d.NAMA}</td>
                                <td>{$d.NM_JENJANG} {$d.PRODI|replace:'PSIKOLOGI':'PSI'}</td>
                                <td>
                                    IPK : <b>{$d.IPK}</b><br/>
                                    TOTAL SKS : <b>{$d.TOTAL_SKS}</b><br/>
                                    TINGKAT SEMESTER: <b>{$d.SEMESTER}</b><br/>
                                    PROGRESS: <b>{if $d.PROGRESS==''}0{else}{$d.PROGRESS}{/if} %</b><br/>
                                    STATUS : <b>{$d.STATUS}</b>
                                </td>
                                <td>
                                    {foreach $d.JADWAL_UJIAN as $ju}
                                        TGL : <b>{$ju.TGL_UJIAN|date_format:"%d/%m/%Y"}</b><br/>
                                        JAM : <b>{$ju.JAM_MULAI}-{$ju.JAM_SELESAI}</b><br/>
                                        RUANG : <b>{$ju.NM_RUANGAN}</b>
                                    {foreachelse}
                                        <span style="color: red">Jadwal Belum Ada</span>
                                    {/foreach}
                                </td>
                                <td>
                                    <a style="padding:5px" class="ui-button ui-state-default ui-corner-all" href="bimbingan.php?mode=detail-bimbingan&id={$d.ID_MHS}" >Detail</a>
                                </td>
                            </tr>
                        {foreachelse}
                            <tr>
                                <td colspan="10" class="kosong">Data Kosong</td>
                            </tr>
                        {/foreach}
                    </table>
                {/if}
            {/foreach}
        </div>
        <div id="penguji">
            <table style="width: 98%">
                <tr class="ui-widget-header">
                    <th class="header-coloumn" colspan="10">Daftar Mahasiswa </th>
                </tr>
                <tr class="ui-widget-header">
                    <th>No</th>
                    <th>NIM (ANGKATAN)</th>
                    <th>NAMA</th>
                    <th>PRODI</th>
                    <th>DATA AKAD</th>
                    <th>JADWAL UJIAN</th>
                    <th>DETAIL</th>
                </tr>
                {foreach $data_penguji as $d}
                    <tr>
                        <td>{$d@index+1}</td>
                        <td>{$d.NIM} ({$d.ANGKATAN})</td>
                        <td>{$d.NAMA}</td>
                        <td>{$d.NM_JENJANG} {$d.PRODI|replace:'PSIKOLOGI':'PSI'}</td>
                        <td>
                            IPK : <b>{$d.IPK}</b><br/>
                            TOTAL SKS : <b>{$d.TOTAL_SKS}</b><br/>
                            TINGKAT SEMESTER: <b>{$d.SEMESTER}</b><br/>
                            STATUS : <b>{$d.STATUS}</b>
                        </td>
                        <td>
                            {foreach $d.JADWAL_UJIAN as $ju}
                                TGL : <b>{$ju.TGL_UJIAN|date_format:"%d/%m/%Y"}</b><br/>
                                JAM : <b>{$ju.JAM_MULAI}-{$ju.JAM_SELESAI}</b><br/>
                                RUANG : <b>{$ju.NM_RUANGAN}</b>
                            {foreachelse}
                                <span style="color: red">Jadwal Belum Ada</span>
                            {/foreach}
                        </td>
                        <td>
                            <a style="padding:5px" class="ui-button ui-state-default ui-corner-all" href="bimbingan.php?mode=detail-bimbingan&id={$d.ID_MHS}" >Detail</a>
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="10" class="kosong">Data Kosong</td>
                    </tr>
                {/foreach}
            </table>
        </div>
    </div>
{/if}
{literal}
    <script type="text/javascript">
        $("#tabs").tabs();
        $('#value_progress1').click(function() {
            alert($(this).text());
        });

        $(function() {
            for (i = 1; i <= $('.progressbar').length; i++) {
                x = parseInt($('#value_progress' + i).text());
                $("#progressbar" + i).progressbar({
                    value: x
                });

            }
        });
        $('.detail').click(function() {
            $('#center_content').load('bimbingan.php', {id_ta: $(this).attr('id'), mode: 'detail_bimbingan'})
        })

    </script>
{/literal}