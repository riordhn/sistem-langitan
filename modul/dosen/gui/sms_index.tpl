<div class="center_title_bar">SMS GATEWAY DOSEN</div>

{literal}
    <script>
        $(function() {

		$( "#btn-send" ).button({
			text: true
		});

		$( "#btn-cancel" ).button({
			text: true
		});
			
		$('#btn-send').click(function(){	
			var content =  document.getElementById('pesan').value;
			if(content ==''){
				$( "#dialog:ui-dialog" ).dialog( "destroy" );
			
				$( "#dialog-message" ).dialog({
					modal: true,
					buttons: {
						Ok: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
			else{
				$('#frmOrmawaBroadcast').submit();
			}
		})
		
		$('#btn-cancel').click(function(){	
			window.history.back();
		})
		
		$('#frmOrmawaBroadcast').submit(function() {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
					$('#wrap').html('Pesan telah dikirim');
				}
			})
			return false;
		});
		
		$("#pesan").attr('maxlength','120');
        });
    </script>
{/literal}
<div id="dialog-message" title="Pemberitahuan" style="display:none;">
	<p>
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
		Anda belum mengisi pesan.
	</p>
	<p>
		Silakan mengisi pesan yang ingin anda sampaikan dengan ketentuan jumlah Max. karakter adalah 160.
	</p>
</div>
<div id="wrap">
<form id="frmOrmawaBroadcast" action="sms.php?view=index" method="POST">
    <div class="box">

        <div class="box">
            <div class="note">Kepada : {$mhs}&nbsp;({$mobile})
	    </div>
			<div class="note">Pesan : </div>
			<div>
				<div style="margin-bottom:10px;width:625px;">
					<textarea id="pesan" name="pesan" style="width:100%; height:50px;"></textarea>
					<input type="hidden" value="{$mobile}" name="mobile" id="mobile" />
				</div>
				<div>
					<div id="btn-cancel" style="padding:5px;cursor:pointer;" class="disable-ajax">Batal</div>
					<div id="btn-send" style="padding:5px;cursor:pointer;">Kirim</div>
				</div>

			</div>
			<br /><br /><br />
			<div>&copy; SMS GATEWAY SISTEM LANGITAN NAHDLATUL ULAMA</div>
        </div>
		
    </div>
</form>

</div>