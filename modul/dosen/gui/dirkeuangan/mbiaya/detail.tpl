<div class="center_title_bar">Master Varian Biaya - Detail Biaya Kuliah</div>

{if $biaya_kuliah}
{$bk=$biaya_kuliah}

<table border="1">
    <tr>
        <td>Fakultas</td>
        <td>{$bk.NM_FAKULTAS}</td>
    </tr>
    <tr>
        <td>Nama Program Studi</td>
        <td>{$bk.NM_PROGRAM_STUDI}</td>
    </tr>
	<tr>
        <td>Semester</td>
        <td>{$bk.NM_SEMESTER}</td>
    </tr>
	<tr>
        <td>Jalur</td>
        <td>{$bk.NM_JALUR}</td>
    </tr>
	<tr>
        <td>Kelompok Biaya</td>
        <td>{$bk.NM_KELOMPOK_BIAYA}</td>
    </tr>
</table>

<table border="1">
	<tr>
		<th>No</th>
		<th>Detail Tagihan</th>
		<th>Pendaftaran</th>
		<th>Aktif</th>
		<th>Besar Biaya</th>
	</tr>
	{foreach $detail_biaya_set as $db}
	<tr>
		<td>{$db@index+1}</td>
		<td>{$db.NM_BIAYA}</td>
		<td style="text-align:center">{if $db.PENDAFTARAN_BIAYA == 1}Y{/if}</td>
		<td style="text-align:center">{if $db.STATUS_BIAYA == 1}Y{/if}</td>
		<td style="text-align:right">{number_format($db.BESAR_BIAYA, 0, ",", ".")}</td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="5">{$bk.KETERANGAN_BIAYA_KULIAH}<br/>
		</td>
	</tr>
	<tr>
		<td  colspan="5" style="text-align:center"><button href="mbiaya.php?mode=prodi&id_program_studi={$bk.ID_PROGRAM_STUDI}">Kembali</button></td>
	</tr>
</table>



{/if}