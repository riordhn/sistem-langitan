<div class="center_title_bar">Master Varian Biaya - Detail Program Studi</div>


{if $program_studi}
{$ps=$program_studi}

<table border="1">
    <tr>
        <td>Jenjang</td>
        <td>{$ps.NM_JENJANG}</td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>{$ps.NM_FAKULTAS}
        </td>
    </tr>
    <tr>
        <td>Nama Program Studi</td>
        <td>{$ps.NM_PROGRAM_STUDI}</td>
    </tr>
    <tr>
        <td>Kode Program Studi</td>
        <td>{$ps.KODE_PROGRAM_STUDI}</td>
    </tr>
    <tr>
        <td>No. SK Rektor</td>
        <td>{$ps.NO_SK}</td>
    </tr>
    <tr>
        <td>Jumlah Mahasiswa</td>
        <td>{$ps.JUMLAH_MAHASISWA}</td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center">
            <button href="mbiaya.php?id_jenjang={$ps.ID_JENJANG}">Kembali</button>
            {if $ps.JUMLAH_MAHASISWA == 0 && count($biaya_kuliah_set) == 0}
            <input type="submit" value="Hapus" />
            {/if}
        </td>
    </tr>
</table>     

<table border="1">
    <caption>Detail Varian Biaya</caption>
    <tr>
        <th>No</th>
        <th>Semester</th>
        <th>Jalur</th>
        <th>Kelompok</th>
        <th>Pendaftaran</th>
        <th>Per Semester</th>
        <th>Biaya Total</th>
		<th>Aksi</th>
    </tr>
    {foreach $biaya_kuliah_set as $bk}
    <tr>
        <td>{$bk@index+1}</td>
        <td>{$bk.NM_SEMESTER}</td>
        <td>{$bk.NM_JALUR}</td>
        <td>{$bk.NM_KELOMPOK_BIAYA}</td>
        <td style="text-align: right">{number_format($bk.BIAYA_PENDAFTARAN, 0, ",", ".")}</td>
        <td style="text-align: right">{number_format($bk.BIAYA_SEMESTER, 0, ",", ".")}</td>
        <td style="text-align: right">{number_format($bk.BESAR_BIAYA_KULIAH, 0, ",", ".")}</td>
		<td style="text-align: center">
			<a href="mbiaya.php?mode=detail&id_biaya_kuliah={$bk.ID_BIAYA_KULIAH}">Lihat</a>
		</td>
    </tr>
    {/foreach}
</table>

{/if}