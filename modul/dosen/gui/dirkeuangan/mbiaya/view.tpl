<div class="center_title_bar">Master Varian Biaya - Program Studi</div>

<form action="mbiaya.php" method="get">
    <table>
        <tr>
            <td>Jenjang :
                <select name="id_jenjang">
                    <option value="">-- Semua --</option>
                {foreach $jenjang_set as $j}
                    <option value="{$j.ID_JENJANG}" {if $j.ID_JENJANG == $id_jenjang}selected="selected"{/if}>{$j.NM_JENJANG}</option>
                {/foreach}
                </select>
                <input type="submit" value="Lihat" />
            </td>
        </tr>
    </table>
</form>
                
{if $biaya_kuliah_added}
<h2>Biaya Kuliah berhasil ditambahkan.</h2>
{/if}

{if $biaya_kuliah_deleted}
<h2>Biaya Kuliah berhasil dihapus.</h2>
{/if}
                
{if $program_studi_set}
    
<table border="1">
    <tr>
        <th>No</th>
        <th>Nama Program Studi</th>
        <th>Kode</th>
        <th>Fakultas</th>
        <th>Mahasiswa</th>
        <th>Varian Biaya</th>
        <th style="width: 70px">Aksi</th>
    </tr>
    {foreach $program_studi_set as $ps}
    <tr>
        <td>{$ps@index+1}</td>
        <td>{$ps.NM_PROGRAM_STUDI}</td>
        <td>{$ps.KODE_PROGRAM_STUDI}</td>
        <td>{$ps.NM_FAKULTAS}</td>
        <td style="text-align: center">{$ps.JUMLAH_MAHASISWA}</td>
        <td style="text-align: center">{$ps.JUMLAH_VARIAN}</td>
        <td style="text-align: center">
            <a href="mbiaya.php?mode=prodi&id_program_studi={$ps.ID_PROGRAM_STUDI}">Lihat</a>
        </td>
    </tr>
    {/foreach}
</table>
    
{/if}