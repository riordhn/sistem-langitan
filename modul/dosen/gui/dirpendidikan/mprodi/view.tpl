<div class="center_title_bar">Master Program Studi</div>
{literal}<style type="text/css">
    .center { text-align: center; }
</style>{/literal}
<form action="mprodi.php" method="get">
    <table>
        <tr>
            <td>Jenjang :
                <select name="id_jenjang">
                    <option value="">-- Semua --</option>
                {foreach $jenjang_set as $j}
                    <option value="{$j.ID_JENJANG}" {if $j.ID_JENJANG == $id_jenjang}selected="selected"{/if}>{$j.NM_JENJANG}</option>
                {/foreach}
                </select>
                <input type="submit" value="Lihat" />
            </td>
        </tr>
    </table>
</form>
                
{if $prodi_added}
<h2>Program studi berhasil ditambahkan.</h2>
{/if}

{if $prodi_deleted}
<h2>Program studi berhasil dihapus.</h2>
{/if}


                
{if $program_studi_set}
    
<table border="1">
    <tr>
        <th>No</th>
        <th>Nama Program Studi</th>
        <th>Fakultas</th>
        <th>Mhs</th>
        <th>V. Biaya</th>
        <th>No. SK</th>
        <th>Tgl Pendirian</th>
        <th>Masa Berlaku</th>
        <th style="width: 70px">Aksi</th>
    </tr>
    {$urutan=1}
    {foreach $program_studi_set as $ps}
    {if $ps.ID_PROGRAM_STUDI != 197}
    <tr>
        <td>{$urutan}</td>
        <td>{ucwords(strtolower($ps.NM_PROGRAM_STUDI))}</td>
        <td>{$ps.SINGKATAN_FAKULTAS}</td>
        <td class="center">{$ps.JUMLAH_MAHASISWA}</td>
        <td class="center">{$ps.JUMLAH_VARIAN}</td>
        <td>{$ps.NO_SK}</td>
        <td>{$ps.TGL_PENDIRIAN}</td>
        <td class="center">{$ps.MASA_BERLAKU}</td>
        <td>
            <a href="mprodi.php?mode=edit&id_program_studi={$ps.ID_PROGRAM_STUDI}">Edit</a> |
            <a href="mprodi.php?mode=delete&id_program_studi={$ps.ID_PROGRAM_STUDI}">Hapus</a>
        </td>
    </tr>
    {$urutan=$urutan+1}
    {/if}
    {/foreach}
    <tr>
        <td colspan="9" style="text-align: center"><button href="mprodi.php?mode=add">Tambah Prodi</href></td>
    </tr>
</table>
    
{/if}