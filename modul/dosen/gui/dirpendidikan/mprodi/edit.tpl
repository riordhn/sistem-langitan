<div class="center_title_bar">Master Program Studi - Edit</div>

{if $program_studi}
{$ps=$program_studi}

{if $prodi_changed}
<h2>Perubahan program studi berhasil.</h2>
{/if}

<form action="mprodi.php?mode=edit&id_program_studi={$ps.ID_PROGRAM_STUDI}" method="post" id="form_prodi">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_program_studi" value="{$ps.ID_PROGRAM_STUDI}" />

<table border="1">
    <tr>
        <td>Jenjang</td>
        <td>
            <select name="id_jenjang">
            {foreach $jenjang_set as $j}
                <option value="{$j.ID_JENJANG}" {if $j.ID_JENJANG == $ps.ID_JENJANG}selected="selected"{/if}>{$j.NM_JENJANG}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>
            <select name="id_fakultas">
            {foreach $fakultas_set as $f}
                <option value="{$f.ID_FAKULTAS}" {if $f.ID_FAKULTAS == $ps.ID_FAKULTAS}selected="selected"{/if}>{$f.NM_FAKULTAS}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Nama Program Studi</td>
        <td><input type="text" name="nm_program_studi" maxlength="64" size="50" value="{$ps.NM_PROGRAM_STUDI}" /></td>
    </tr>
    <tr>
        <td>Kode Program Studi</td>
        <td><input type="text" name="kode_program_studi" maxlength="16" size="8" value="{$ps.KODE_PROGRAM_STUDI}" /></td>
    </tr>
    <tr>
        <td>No. SK</td>
        <td><input type="text" name="no_sk" maxlength="64" size="50" value="{$ps.NO_SK}" /></td>
    </tr>
    <tr>
        <td>Tanggal Didirikan</td>
        <td>
            {html_select_date prefix="tgl_pendirian_" field_order="DMY" start_year="-10" end_year="+10" all_empty="" time=$ps.TGL_PENDIRIAN}
        </td>
    </tr>
    <tr>
        <td>Masa Berlaku Prodi</td>
        <td>
            <input type="text" size="3" maxlength="2" name="masa_berlaku" value="{$ps.MASA_BERLAKU}" /> Tahun
        </td>
    </tr>
    <tr>
        <td>Jumlah Mahasiswa</td>
        <td>{$ps.JUMLAH_MAHASISWA}</td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center">
            <button href="mprodi.php?id_jenjang={$ps.ID_JENJANG}">Kembali</button>
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
            
</form>

{literal}
<script type="text/javascript">
    $('#form_prodi').validate({
        rules: {
            nm_program_studi: { required: true },
            kode_program_studi: { required: true },
            no_sk: { required: true },
            tgl_didirikan_Day: { required: true },
            tgl_didirikan_Month: { required: true },
            tgl_didirikan_Year: { required: true }
        },
        messages: {
            nm_program_studi: { required: 'Nama Program Studi harus di isi' },
            kode_program_studi: { required:  'Kode Program Studi harus di isi' },
            no_sk: { required: 'Nomor SK Rektor harus di isi' },
            tgl_didirikan_Day: { required: 'Tanggal pendirian prodi' },
            tgl_didirikan_Month: { required: 'Bulan pendirian prodi' },
            tgl_didirikan_Year: { required: 'Tahun pendirian prodi' }
        }
    });
</script>
{/literal}

<table border="1">
    <caption>Detail Varian Biaya</caption>
    <tr>
        <th>No</th>
        <th>Semester</th>
        <th>Jalur</th>
        <th>Kelompok</th>
        <th>Pendaftaran</th>
        <th>Per Semester</th>
        <th>Biaya Total</th>
    </tr>
    {foreach $biaya_kuliah_set as $bk}
    <tr>
        <td>{$bk@index+1}</td>
        <td>{$bk.NM_SEMESTER}</td>
        <td>{$bk.NM_JALUR}</td>
        <td>{$bk.NM_KELOMPOK_BIAYA}</td>
        <td style="text-align: right">{number_format($bk.BIAYA_PENDAFTARAN, 0, ",", ".")}</td>
        <td style="text-align: right">{number_format($bk.BIAYA_SEMESTER, 0, ",", ".")}</td>
        <td style="text-align: right">{number_format($bk.BESAR_BIAYA_KULIAH, 0, ",", ".")}</td>
    </tr>
    {/foreach}
</table>

{/if}