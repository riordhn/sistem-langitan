<div class="center_title_bar">Master Program Studi - Tambah</div>

<form action="mprodi.php" method="post" id="form_prodi">
<input type="hidden" name="mode" value="add" />

<table border="1">
    <tr>
        <td>Jenjang</td>
        <td>
            <select name="id_jenjang">
            {foreach $jenjang_set as $j}
                <option value="{$j.ID_JENJANG}">{$j.NM_JENJANG}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>
            <select name="id_fakultas">
            {foreach $fakultas_set as $f}
                <option value="{$f.ID_FAKULTAS}">{$f.NM_FAKULTAS}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Nama Program Studi</td>
        <td><input type="text" name="nm_program_studi" maxlength="64" size="50" value="" /></td>
    </tr>
    <tr>
        <td>Kode Program Studi</td>
        <td><input type="text" name="kode_program_studi" maxlength="16" size="8" value="" /></td>
    </tr>
    <tr>
        <td>No. SK Rektor</td>
        <td><input type="text" name="no_sk" maxlength="64" size="50" value="" /></td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center">
            <button href="mprodi.php">Kembali</button>
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
            
</form>

{literal}
<script type="text/javascript">
    $('#form_prodi').validate({
        rules: {
            nm_program_studi: { required: true },
            kode_program_studi: { required: true },
            no_sk: { required: true }
        },
        messages: {
            nm_program_studi: { required: 'Nama Program Studi harus di isi' },
            kode_program_studi: { required:  'Kode Program Studi harus di isi' },
            no_sk: { required: 'Nomor SK Rektor harus di isi' }
        }
    });
</script>
{/literal}