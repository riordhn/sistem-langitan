<div class="center_title_bar">{$title}</div> 
<div id="message">
    <table class="none" style="width: 98%">
        <tr class="ui-widget-header">
            <th colspan="3" class="header-coloumn">Detail Pesan</th>
        </tr>
        <tr>
            <td style="width: 10%">
                <b>Subject</b>
            </td>
            <td style="width: 3%">
                :
            </td>
            <td style="width: 87%">
                {$data_message[0]['judul']}
            </td>
        </tr>
        <tr>
            <td>
                <b>To/From</b>
            </td>
            <td>
                :
            </td>
            <td>
                <input type="hidden" name="id_penerima" value="{$data_message[0]['id_pengirim']}" readonly="true"/>
                <span>{$data_message[0]['nama_pengirim']}</span>
            </td>
        </tr>
        <tr>
            <td>
               <b> Pesan</b>
            </td>
            <td>
                :
            </td>
            <td>
                {$data_message[0]['isi']}
            </td>
        </tr>
        <tr>
            <td colspan="3">
                {if $trash == 0}
                    <a href="reply.php?id={$data_message[0]['id_pesan']}" class="reply">Balas</a>
                {/if}
                <a class="disable-ajax back" onclick="history.back()">Kembali</a>
            </td>
        </tr>
    </table>
</div>