<div class="center_title_bar">{$title}</div> 
<div id="tabs" style="width:98%">
    <ul>
        <li><a class="disable-ajax" href="#tabs-inbox">INBOX</a></li>
    </ul>
    <div id="tabs-inbox">
        <div style="width: 98%;border: 1px solid #4578a2;padding: 10px">                                          
            <form id="form1" name="form1" method="post" action="">
                <table style="width: 98%">
                    <tr class="ui-widget-header">
                        <th></th>
                        <th>Pengirim</th>
                        <th>Judul</th>
                        <th>Waktu</th>
                        <th>Tipe Pesan</th>
                        <th class="center" style="width:160px;">Operasi</th>
                    </tr>
                    {foreach $data_inbox as $inbox}
                        {if $inbox['baca_penerima']==''||$inbox['baca_penerima']==0}
                            <tr style="background-color: #EAEAEA">
                                <td>
                                    <input type="checkbox" id="{$inbox['id_pesan']}" class="checkbox" />
                                </td>
                                <td>
                                    {$inbox['nama_pengirim']}
                                </td>
                                <td>
                                    <a class="ui-button ui-state-active ui-corner-all" style="padding:5px;" href="message_detail.php?id={$inbox['id_pesan']}">{$inbox['judul']}</a>
                                </td>
                                <td>{$inbox['waktu']}</td>
                                <td>{$inbox['tipe_pesan']}</td>
                                <td>
                                    <span id="{$inbox['id_pesan']}" class="delete">Hapus</span>
                                    <a href="reply.php?id={$inbox['id_pesan']}" class="reply">Balas</a>
                                </td>
                            </tr>
                        {else}
                            <tr>
                                <td>
                                    <input type="checkbox" id="{$inbox['id_pesan']}" class="checkbox" />
                                </td>
                                <td>
                                    {$inbox['nama_pengirim']}
                                </td>
                                <td>
                                    <a class="ui-button ui-state-active ui-corner-all" style="padding:5px;" href="message_detail.php?id={$inbox['id_pesan']}">{$inbox['judul']}</a>
                                </td>
                                <td>{$inbox['waktu']}</td>
                                <td>{$inbox['tipe_pesan']}</td>
                                <td>
                                    <span id="{$inbox['id_pesan']}" class="delete">Hapus</span>
                                    <a href="reply.php?id={$inbox['id_pesan']}" class="reply">Balas</a>
                                </td>
                            </tr>
                        {/if}

                    {/foreach}
                    <tr>
                        <td colspan=6>
                            <p>
                                <b>KETERANGAN</b>: Background Abu-abu Belum Terbaca
                            </p>
                            <span class="delete_checked">Hapus Yang Di Pilih</span>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
{literal}
    <script type="text/javascript">
        $("#tabs").tabs();
        $('.delete').click(function() {
            var c = confirm("Apakah anda yakin menghapus untuk Pesan ini??");
            if (c == true) {
                $('#content').load('inbox.php', {id: $(this).attr('id'), mode: 'delete'});
            }
            else {
                return false;
            }

        })
        $('.reply').click(function() {
            $('#content').load('reply.php', {id: $(this).attr('id')});
        })
        $('.message_detail').click(function() {
            $('#content').load('message_detail.php', {id: $(this).attr('id')});
        })
        $('.delete_checked').click(function() {
            if ($('.checkbox:checked').length == 0) {
                alert('Tidak Ada Yang Di Pilih !!');
            }
            else {
                var c = confirm("Apakah anda yakin untuk menghapus banyak Pesan ?");
                if (c == true) {
                    var element = new Array();
                    $('.checkbox:checked').each(function() {
                        var child = new Array($(this).attr('id'));
                        element.push(child);
                    });
                    $('#content').load('inbox.php', {
                        mode: 'delete_checked',
                        id: element
                    });
                }
                else {
                    return false;
                }
            }

        });
    </script>
{/literal}