<div class="center_title_bar">{$title}</div>
<div id="tabs" style="width:98%">
    <ul>
        <li><a class="disable-ajax" href="#tabs-sentitem">SENT ITEM</a></li>
    </ul>
    <div id="tabs-sentitem">
        <div style="width: 98%;border: 1px solid #4578a2;padding: 10px"> 
            <form id="form1" name="form1" method="post" action="">
                <table style="width: 98%">
                    <tr class="ui-widget-header">
                        <th></th>
                        <th>Kepada</th>
                        <th>Judul</th>
                        <th>Waktu</th>
                        <th>Tipe Pesan</th>
                        <th class="center">Operasi</th>
                    </tr>
                    {foreach $data_sent_item as $sent_item}
                        <tr>
                            <td>
                                <input type="checkbox" id="{$sent_item['id_pesan']}" class="checkbox" />
                            </td>
                            <td>
                                <a href="message_detail.php?id={$sent_item['id_pesan']}&mode=no_reply" class="message_detail">{$sent_item['nama_penerima']}</a>
                            </td>
                            <td>
                                <span>{$sent_item['judul']}</span>
                            </td>
                            <td>{$sent_item['waktu']}</td>
                            <td>{$sent_item['tipe_pesan']}</td>
                            <td class="center"><span style="float: none" class="delete" id="{$sent_item['id_pesan']}">Hapus</span></td>
                        </tr>
                    {/foreach}
                    <tr>
                        <td colspan="6">
                            <span class="delete_checked">Hapus Yang Di Pilih</span>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
{literal}
    <script type="text/javascript">
    $('td').addClass('ui-widget-content');
    $( "#tabs" ).tabs();
    $('.delete').click(function(){
        var c= confirm("Apakah anda yakin menghapus untuk Pesan ini??");
        if(c== true){
            $('#content').load('sent_item.php',{id:$(this).attr('id'),mode:'delete'});
        }
        else{
            return false;
        }
    
    })
    $('.delete_checked').click(function(){
        if($('.checkbox:checked').length==0){
            alert('Nothing Checked !!');
        }
        else{
            var c = confirm("Are you sure to delete many conversation ?");
            if(c==true){
                var element = new Array();
                $('.checkbox:checked').each(function(){
                    var child = new Array($(this).attr('id'));
                    element.push(child);            
                });
                $('#content').load('sent_item.php',{
                    mode:'delete_checked',
                    id:element
                });
            }
            else{
                return false;
            }
        }
    
    });
    </script>
{/literal}
