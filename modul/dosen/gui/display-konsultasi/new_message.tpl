<div class="center_title_bar">{$title}</div> 
<div id="new_message" class="ui-widget">
    <form id="form_new_message" method="post" name="form_new_message" action="new_message.php">
        <table class="none" style="width: 98%">
            <tr class="ui-widget-header">
                <th colspan="3" class="header-coloumn"><h2>Pesan Baru</h2></th>
            </tr>
            <tr class="ui-widget-content">
                <td style="vertical-align:middle;width: 10%">
                    Subject
                </td>
                <td style="vertical-align:middle;width: 3%">
                    :
                </td>
                <td style="vertical-align:middle;width: 87%">
                    <input type="text" name="subject" style="width: 98%" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td style="vertical-align:middle">
                    To
                </td>
                <td style="vertical-align:middle">
                    :
                </td>
                <td style="vertical-align:middle">
                    User &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:

                    <select id="bagian" onChange="getFoto()">
                        {foreach $data_bagian as $bagian}
                            {if $bagian==$post_bagian}
                                <option name="bagian" value="{$bagian}" selected="true">{$bagian}</option>
                            {else}
                                <option name="bagian" value="{$bagian}">{$bagian}</option>
                            {/if}
                        {/foreach}
                    </select>

                    <br/>

                    <div id="pilih_fakultas" {if $post_bagian==null||$post_bagian!='Dosen'}style="display:none"{/if}>
                        <span>Fakultas&nbsp;&nbsp;:</span>
                        <select name="fakultas" id="fakultas">
                            {foreach $data_fakultas as $data}
                                {if $data.ID_FAKULTAS==$post_fakultas}
                                    <option value="{$data.ID_FAKULTAS}" selected="true">{$data.NM_FAKULTAS|upper}</option>
                                {else}
                                    <option value="{$data.ID_FAKULTAS}">{$data.NM_FAKULTAS|upper}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <br/>
                    </div>
                    Nama &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                    <form action="get_foto.php" method="post" id="getFoto" name="getFoto">
                        <select name="penerima" id="penerima">
                            {foreach $data_penerima as $penerima}
                                {if $post_bagian=='Ortu'}
                                    <option  value="{$penerima['id']}">{$penerima['nama']|upper} ({$penerima['nama_mhs']|upper})</option>
                                {else}
                                    <option  value="{$penerima['id']}">{$penerima['nama']|upper}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </form>
                    <br/>
                    <div id="foto">                        
                    </div>

                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>
                    Pesan
                </td>
                <td>
                    :
                </td>
                <td style="vertical-align:middle">
                    <textarea  name="pesan" rows="5" style="width: 98%;resize: none" ></textarea>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td colspan="3">
                    <span class="send" id="send">Kirim</span>
                    <input type="hidden" name="send" />
                </td>
            </tr>
        </table>
    </form>
</div>
{literal}
    <script type="text/javascript"> 
	
	
            $('#bagian').change(function(){
                    if($(this).val()=='Dosen'){
                            $('#pilih_fakultas').fadeIn('slow');
                    }else{
                            $('#pilih_fakultas').fadeOut('slow');
                    }
                    $('#content').load('new_message.php',{bagian:$(this).val()});
		

            });
            $('#pilih_fakultas').change(function(){
                    $('#content').load('new_message.php', 'bagian='+$('#bagian').val()+'&id_fak='+$('#fakultas').val());
            })
            $('#form_new_message').validate({
                    rules :{
                pesan:{
                    required :true
                },
                            subject:{
                                    required:true
                            }
            }
            });
            $('#send').click(function(){
                    //postPage('new_message.php',$('#form_new_message').serialize());
                    $('#form_new_message').submit();
            });
	
            $(document).ready(function() {
                    $("#penerima").change(function(){
                            $("#foto").load('get_foto.php', 'penerima='+$('#penerima').val()+'&bagian='+$('#bagian').val());
                    });
        });


	
    </script>
{/literal}