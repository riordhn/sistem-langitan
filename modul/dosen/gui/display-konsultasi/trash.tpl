<div class="center_title_bar">{$title}</div>
<div id="tabs" style="width:98%">
    <ul>
        <li><a class="disable-ajax" href="#tabs-trash">TRASH</a></li>
    </ul>
    <div id="tabs-trash">
        <div style="width: 98%;  border: 1px solid #4578a2;padding: 10px">                                          
            <form id="form1" name="form1" method="post" action="">
                <table style="width: 98%">
                    <tr class="ui-widget-header">
                        <th></th>
                        <th>Dari / Kepada</th>
                        <th>Judul</th>
                        <th>Waktu</th>
                        <th>Asal</th>
                        <th class="center" style="width:170px;">Operasi</th>
                    </tr>
                    {foreach $data_trash as $trash}
                        <tr>
                            <td>
                                <input type="checkbox" id="{$trash['id_pesan']}" class="checkbox" />
                            </td>
                            <td>
                                {if $id_pengguna == $trash['id_penerima']}
                                    <span>{$trash['nama_pengirim']}</span>
                                {else}
                                    <span>{$trash['nama_penerima']}</span>
                                {/if}
                            </td>
                            <td><a href="message_detail.php?id={$trash['id_pesan']}&mode=no_reply" class="message_detail" style="cursor: pointer;color:#3399ff;">{$trash['judul']}</span></td>
                            <td>{$trash['waktu']}</td>
                            <td>
                                {if $id_pengguna == $trash['id_penerima']}
                                    <span>Inbox</span>
                                {else}
                                    <span>Sent Item</span>
                                {/if}
                            </td>
                            <td style="width:140px">
                                <span class="delete" id="{$trash['id_pesan']}">Hapus</span>
                                <span class="restore" id="{$trash['id_pesan']}">Kembalikan</span>
                            </td>
                        </tr>
                    {/foreach}
                    <tr>
                        <td colspan="6">
                            <span class="delete_checked">Hapus Yand Di Pilih</span>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
{literal}
    <script type="text/javascript">
    $('td').addClass('ui-widget-content');
    $( "#tabs" ).tabs();
    $('.delete').click(function(){
        var c= confirm("Apakah anda yakin untuk menghapus Pesan ini secara Permanen?");
        if(c== true){
            $('#content').load('trash.php',{id:$(this).attr('id'),mode:'delete'});
        }
        else{
            return false;
        }
    
    })
    $('.restore').click(function(){
        var c= confirm("Apakah anda yakin mengambalikan Pesan ini ke tempat asal??");
        if(c== true){
            $('#content').load('trash.php',{id:$(this).attr('id'),mode:'restore'});
        }
        else{
            return false;
        }
    
    })
    $('.message_detail').click(function(){
        $('#content').load('message_detail.php',{id:$(this).attr('id'),mode:'trash'});
    })
    $('.delete_checked').click(function(){
        if($('.checkbox:checked').length==0){
            alert('Nothing Checked !!');
        }
        else{
            var c = confirm("Are you sure to delete many conversation ?");
            if(c==true){
                var element = new Array();
                $('.checkbox:checked').each(function(){
                    var child = new Array($(this).attr('id'));
                    element.push(child);            
                });
                $('#content').load('trash.php',{
                    mode:'delete_checked',
                    id:element
                });
            }
            else{
                return false;
            }
        }
    
    });
    </script>
{/literal}
