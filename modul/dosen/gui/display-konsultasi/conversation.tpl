<div class="center_title_bar">{$title}</div>
<div id="tabs" style="width:98%">
    <ul>
        <li><a class="disable-ajax" href="#tabs-dosen">DOSEN</a></li>
        <li><a class="disable-ajax" href="#tabs-mahasiswa">MAHASISWA</a></li>
        <li><a class="disable-ajax" href="#tabs-ortu">ORTU</a></li>
    </ul>
    <div id="tabs-dosen">
        <div class="ui-widget" style="border: 1px solid #4578a2; padding: 10px;width: 98%">
            <table style="width: 98%">
                {foreach $data_conversation_dosen as $conversation}
                    {if $conversation['hapus_penerima']!=3 && $conversation['hapus_pengirim']!=3 && $conversation['id_balasan']==null}
                        <tr class="ui-widget-content">
                            <td rowspan="3">
                                {if $conversation['id_penerima']==$id_pengguna}
                                    <input type="checkbox" id="{$conversation['id_pesan']}" status="terima" class="checkbox" />
                                {else}
                                    <input type="checkbox" id="{$conversation['id_pesan']}" status="kirim" class="checkbox" />
                                {/if}
                            </td>
                            <td>
                                <font size="2px" color="#4176AF">
                                {if $conversation['id_penerima']==$id_pengguna}
                                    <strong>Pesan Diterima</strong>
                                {else}
                                    <strong>Pesan Dikirim</strong>
                                {/if}
                                <br/>
                                <strong>Waktu:</strong> <span>{$conversation['waktu']}</span> 
                                {if $conversation['id_penerima']==$id_pengguna}
                                    <strong>Dari:</strong> <span>{$conversation['nama_pengirim']}</span>
                                {else}
                                    <strong>Kepada</strong> <span>{$conversation['nama_penerima']}</span>
                                {/if}
                                <br/>
                                <strong>Judul:</strong> <span class="judul" id="{$conversation['id_pesan']}">{$conversation['judul']}</span>
                                </font>
                            </td>
                        </tr>
                        <tr class="ui-widget-content">
                            <td>
                                <p>{$conversation['isi']}</p>
                                <br/>
                                <hr/>
                                {if $conversation['id_penerima']==$id_pengguna}
                                    <span id="{$conversation['id_pesan']}" rel="terima" class="delete">Hapus</span>
                                    <a href="reply.php?id={$conversation['id_pesan']}" class="reply">Balas</a>
                                {else}
                                    <span id="{$conversation['id_pesan']}" rel="kirim" class="delete">Hapus</span>

                                {/if}
                            </td>
                        </tr>
                        <tr class="ui-widget-content">
                            <td>

                                {foreach $conversation['pesan_balasan'] as $pesan}
                                    {if $pesan['hapus_penerima']!=3 && $pesan['hapus_pengirim']!=3}
                                        <div style="padding-left: 30px;padding-bottom: 20px;">
                                            <font size="2px" color="#4176AF">
                                            {if $pesan['id_penerima']==$id_pengguna}
                                                <strong>Pesan Diterima</strong>
                                            {else}
                                                <strong>Pesan Dikirim</strong>
                                            {/if}
                                            <br/>
                                            <strong>Waktu:</strong> <span>{$pesan['waktu']}</span> 
                                            {if $pesan['id_penerima']==$id_pengguna}
                                                <strong>Dari:</strong> <span>{$pesan['nama_penerima']}</span>
                                            {else}
                                                <strong>Kepada</strong> <span>{$pesan['nama_pengirim']}</span>
                                            {/if}
                                            <br/>
                                            <strong>Judul:</strong> <span class="judul" id="{$pesan['id_pesan']}">{$pesan['judul']}</span>
                                            </font>
                                            <p>{$pesan['isi']}</p>
                                            <br/>
                                            <hr/>
                                            {if $pesan['id_penerima']==$id_pengguna}
                                                <span id="{$pesan['id_pesan']}" rel="terima" class="delete">Hapus</span>
                                                <a href="reply.php?id={$pesan['id_pesan']}" class="reply">Balas</a>
                                            {else}
                                                <span id="{$pesan['id_pesan']}" rel="kirim" class="delete">Hapus</span>                   
                                            {/if}
                                        </div>
                                    {/if}
                                {/foreach}

                            </td>
                        </tr>

                    {/if}
                {/foreach}
                <tr class="ui-widget-content">
                    <td colspan="2">
                        <span id="{$pesan['id_pesan']}" class="delete_checked">Hapus Yang Di Pilih</span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="tabs-mahasiswa">
        <div  class="ui-widget"  style=" border: 1px solid #4578a2; padding: 10px;width: 95%">
            <table style="width: 90%">
                {foreach $data_conversation_mahasiswa as $conversation}
                    {if $conversation['hapus_penerima']!=3 && $conversation['hapus_pengirim']!=3 && $conversation['id_balasan']==null}
                        <tr class="ui-widget-content">
                            <td rowspan="3">
                                {if $conversation['id_penerima']==$id_pengguna}
                                    <input type="checkbox" id="{$conversation['id_pesan']}" status="terima" class="checkbox" />
                                {else}
                                    <input type="checkbox" id="{$conversation['id_pesan']}" status="kirim" class="checkbox" />
                                {/if}
                            </td>
                            <td>
                                <font size="2px" color="#4176AF">
                                {if $conversation['id_penerima']==$id_pengguna}
                                    <strong>Pesan Diterima</strong>
                                {else}
                                    <strong>Pesan Dikirim</strong>
                                {/if}
                                <br/>
                                <strong>Waktu:</strong> <span>{$conversation['waktu']}</span> 
                                {if $conversation['id_penerima']==$id_pengguna}
                                    <strong>Dari:</strong> <span>{$conversation['nama_pengirim']}</span>
                                {else}
                                    <strong>Kepada</strong> <span>{$conversation['nama_penerima']}</span>
                                {/if}
                                <br/>
                                <strong>Judul:</strong> <span class="judul" id="{$conversation['id_pesan']}">{$conversation['judul']}</span>
                                </font>
                            </td>
                        </tr>
                        <tr class="ui-widget-content">
                            <td>
                                <p>{$conversation['isi']}</p>
                                <br/>
                                <hr/>
                                {if $conversation['id_penerima']==$id_pengguna}
                                    <span id="{$conversation['id_pesan']}" rel="terima" class="delete">Hapus</span>
                                    <a href="reply.php?id={$conversation['id_pesan']}" class="reply">Balas</a>
                                {else}
                                    <span id="{$conversation['id_pesan']}" rel="kirim" class="delete">Hapus</span>

                                {/if}
                            </td>
                        </tr>
                        <tr class="ui-widget-content">
                            <td>

                                {foreach $conversation['pesan_balasan'] as $pesan}
                                    {if $pesan['hapus_penerima']!=3 && $pesan['hapus_pengirim']!=3}

                                        <div style="padding-left: 30px;padding-bottom: 20px;">
                                            <font size="2px" color="#4176AF">
                                            {if $pesan['id_penerima']==$id_pengguna}
                                                <strong>Pesan Diterima</strong>
                                            {else}
                                                <strong>Pesan Dikirim</strong>
                                            {/if}
                                            <br/>
                                            <strong>Waktu:</strong> <span>{$pesan['waktu']}</span> 
                                            {if $pesan['id_penerima']==$id_pengguna}
                                                <strong>Dari:</strong> <span>{$pesan['nama_penerima']}</span>
                                            {else}
                                                <strong>Kepada</strong> <span>{$pesan['nama_pengirim']}</span>
                                            {/if}
                                            <br/>
                                            <strong>Judul:</strong> <span class="judul" id="{$pesan['id_pesan']}">{$pesan['judul']}</span>
                                            </font>
                                            <p>{$pesan['isi']}</p>
                                            <br/>
                                            <hr/>
                                            {if $pesan['id_penerima']==$id_pengguna}
                                                <span id="{$pesan['id_pesan']}" rel="terima" class="delete">Hapus</span>
                                            {else}
                                                <span id="{$pesan['id_pesan']}" rel="krim" class="delete">Hapus</span>                   
                                            {/if}
                                        </div>
                                    {/if}
                                {/foreach}

                            </td>
                        </tr>

                    {/if}
                {/foreach}
                <tr class="ui-widget-content">
                    <td colspan="2">
                        <span id="{$pesan['id_pesan']}" class="delete_checked">Hapus Yang Di Pilih</span>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="tabs-ortu">
        <div  class="ui-widget"  style="width: 95%;  border: 1px solid #4578a2; padding: 10px;">                                          
            <table style="width: 90%">
                {foreach $data_conversation_ortu as $conversation}
                    {if $conversation['hapus_penerima']!=3 && $conversation['hapus_pengirim']!=3 && $conversation['id_balasan']==null}
                        <tr class="ui-widget-content">
                            <td rowspan="3">
                                {if $conversation['id_penerima']==$id_pengguna}
                                    <input type="checkbox" id="{$conversation['id_pesan']}" status="terima" class="checkbox" />
                                {else}
                                    <input type="checkbox" id="{$conversation['id_pesan']}" status="kirim" class="checkbox" />
                                {/if}
                            </td>
                            <td>
                                <font size="2px" color="#4176AF">
                                {if $conversation['id_penerima']==$id_pengguna}
                                    <strong>Pesan Diterima</strong>
                                {else}
                                    <strong>Pesan Dikirim</strong>
                                {/if}
                                <br/>
                                <strong>Waktu:</strong> <span>{$conversation['waktu']}</span> 
                                {if $conversation['id_penerima']==$id_pengguna}
                                    <strong>Dari:</strong> <span>{$conversation['nama_pengirim']}</span>
                                {else}
                                    <strong>Kepada</strong> <span>{$conversation['nama_penerima']}</span>
                                {/if}
                                <br/>
                                <strong>Judul:</strong> <span class="judul" id="{$conversation['id_pesan']}">{$conversation['judul']}</span>
                                </font>
                            </td>
                        </tr>
                        <tr class="ui-widget-content">
                            <td>
                                <p>{$conversation['isi']}</p>
                                <br/>
                                <hr/>
                                {if $conversation['id_penerima']==$id_pengguna}
                                    <span id="{$conversation['id_pesan']}" rel="terima" class="delete">Hapus</span>
                                    <a href="reply.php?id={$conversation['id_pesan']}" class="reply">Balas</a>
                                {else}
                                    <span id="{$conversation['id_pesan']}" rel="kirim" class="delete">Hapus</span>

                                {/if}
                            </td>
                        </tr>
                        <tr class="ui-widget-content">
                            <td>

                                {foreach $conversation['pesan_balasan'] as $pesan}
                                    {if $pesan['hapus_penerima']!=3 && $pesan['hapus_pengirim']!=3}

                                        <div style="padding-left: 30px;padding-bottom: 20px;">
                                            <font size="2px" color="#4176AF">
                                            {if $pesan['id_penerima']==$id_pengguna}
                                                <strong>Pesan Diterima</strong>
                                            {else}
                                                <strong>Pesan Dikirim</strong>
                                            {/if}
                                            <br/>
                                            <strong>Waktu:</strong> <span>{$pesan['waktu']}</span> 
                                            {if $pesan['id_penerima']==$id_pengguna}
                                                <strong>Dari:</strong> <span>{$pesan['nama_penerima']}</span>
                                            {else}
                                                <strong>Kepada</strong> <span>{$pesan['nama_pengirim']}</span>
                                            {/if}
                                            <br/>
                                            <strong>Judul:</strong> <span class="judul" id="{$pesan['id_pesan']}">{$pesan['judul']}</span>
                                            </font>
                                            <p>{$pesan['isi']}</p>
                                            <br/>
                                            <hr/>
                                            {if $pesan['id_penerima']==$id_pengguna}
                                                <span id="{$pesan['id_pesan']}" rel="terima" class="delete">Hapus</span>
                                            {else}
                                                <span id="{$pesan['id_pesan']}" rel="krim" class="delete">Hapus</span>                   
                                            {/if}
                                        </div>
                                    {/if}
                                {/foreach}

                            </td>
                        </tr class="ui-widget-content">

                    {/if}
                {/foreach}
                <tr class="ui-widget-content">
                    <td colspan="2">
                        <span id="{$pesan['id_pesan']}" class="delete_checked">Hapus Yang Di Pilih</span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
{literal}
    <script type="text/javascript">
    $( "#tabs" ).tabs();
    $('.delete').click(function(){
        var c = confirm("Apakah anda yakin menghapus untuk Pesan ini?");
        if(c==true){
            $('#content').load("conversation.php",{id:$(this).attr('id'),mode:'delete',status:$(this).attr('rel')})
        }
        else{
            return false;
        }
    
    });
    $('.delete_checked').click(function(){
        if($('.checkbox:checked').length==0){
            alert('Tidak ada Yang Di Pilih !!');
        }
        else{
            var c = confirm("Apakah anda yakin untuk menghapus banyak Pesan ?");
            if(c==true){
                var element = new Array();
                $('.checkbox:checked').each(function(){
                    var child = new Array($(this).attr('id'),$(this).attr('status'));
                    element.push(child);            
                });
                $('#content').load('conversation.php',{
                    mode:'delete_checked',
                    id:element
                });
            }
            else{
                return false;
            }
        }
    
    });
    </script>
{/literal}