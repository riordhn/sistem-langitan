<div class="center_title_bar">{$title}</div> 
{$status}
<div id="new_message" style="width:98%">
    <form id="form_reply_message" method="post" name="form_reply_message" action="reply.php?{$smarty.server.QUERY_STRING}">
        <table class="none" style="width: 98%">
            <tr class="ui-widget-header">
                <th colspan="3" class="header-coloumn">Balas Message</th>
            </tr>
            <tr>
                <td style="width: 10%">
                    <b>Subject</b>
                </td>
                <td style="width: 3%">
                    :
                </td>
                <td style="width: 87%">
                    <input style="width: 90%" type="text" name="subject" size="80" value="re :{$data_reply[0]['judul']}" />
                </td>
            </tr>
            <tr>
                <td>
                    <b>To</b>
                </td>
                <td>
                    :
                </td>
                <td>
                    <input type="hidden" name="id_penerima" value="{$data_reply[0]['id_pengirim']}"/>
                    <span>{$data_reply[0]['nama_pengirim']|upper}</span>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Pesan</b>
                </td>
                <td>
                    :
                </td>
                <td>
                    <textarea  name="isi" rows="5" cols="60" style="resize: none;width: 90%"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <span id="send" class="send">Kirim</span>
                    <span class="back" onclick="history.back()">Kembali</span>
                    <input type="hidden" name="mode" value="save" />
                    <input type="hidden" name="id_balasan" value="{$data_reply[0]['id_pesan']}" />
                </td>
            </tr>
        </table>
    </form>
</div>
{literal}
    <script type="text/javascript">
        $('#form_reply_message').validate({
            rules: {
                isi: {
                    required: true
                },
                subject: {
                    required: true
                }
            }
        });
        $('#send').click(function() {
            $('#form_reply_message').submit()
            // $('#form_reply_message').submit();
        });
    </script>
{/literal}