<form id="form_edit_komponen" method="post" action="komponen-kkn.php">
    <table class="ui-widget" width="90%">
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="2">Edit Komponen Nilai KKN</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="50%">Urutan Komponen</td>
            <td><input type="text" id="urutan" name="urutan" value="{$data_komponen_mk_by_id['URUTAN_KOMPONEN_MK']}" class="required number"/></td>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama Komponen</td>
            <td><input type="text" id="nama" name="nama" value="{$data_komponen_mk_by_id['NM_KOMPONEN_MK']}" class="required"/></td>
        </tr>
        <tr class="ui-widget-content">
            <td>Persentase Komponen</td>
            <td>
                <input type="text" id="persentase" remote="/modul/dosen/cek-total-komponen.php?id_kelas_mk={$id_kelas}&id_komponen={$id_komponen}"  name="persentase" value="{$data_komponen_mk_by_id['PERSENTASE_KOMPONEN_MK']}" class="required number" />
                <label class="error" style="display: none" for="persentase">Tidak Boleh Kosong / Total Komponen Melebihi Maksimal</label>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td class="center" colspan="2">
                <input type="hidden" name="id_komponen" value="{$data_komponen_mk_by_id['ID_KOMPONEN_MK']}" />
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id_kelas" value="{$id_kelas}"/>
                <input type="submit" class="ui-button ui-state-default ui-corner-all" style="padding:5px" value="Simpan"/>
                <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_edit_komponen').dialog('close')">Cancel</span>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_edit_komponen').validate();
            $('#form_edit_komponen').submit(function(){
            if($('#form_edit_komponen').validate())
                        $('#dialog_edit_komponen').dialog('close')
                })
    </script>
{/literal}