<div class="center_title_bar">Komponen Nilai KKN</div> 
<table class="ui-widget">
    <tr>
        <td style="vertical-align:middle">Mata Kuliah</td>
        <td>
            <select id="select_kelas_mk">
                <option>Pilih</option>
                {foreach $data_kelas_mk as $data}
                    <optgroup label="{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})">
                        {foreach $data.DATA_KELAS_MK as $kelas}
                            <option value="{$kelas.ID_KELAS_MK}" 
                                    {if $kelas.ID_KELAS_MK==$id_kelas}
                                        selected="true"
                                    {/if}
                                    >{$kelas.NM_MATA_KULIAH} ({$kelas.KD_MATA_KULIAH}) Kelas {$kelas.NM_KELAS} <br/> Prodi  {$kelas.NM_PROGRAM_STUDI}
                            ({$kelas.NM_JENIS_PENILAI})
                        </option>
                    {/foreach}
                {/foreach}
            </select>
        </td>
    </tr>
</table>

<div id="table_komponen_nilai">
    {if $data_kelas_mk_detail.IS_DIBUKA=='1'||$data_kelas_mk_detail.ID_SEMESTER==$semester_aktif}
            <table class="ui-widget" style="width: 90%">
                <tr class="ui-widget-header">
                    <th colspan="4" class="header-coloumn">Komponen Penilaian</th>
                </tr>
                <tr>
                    <th>Urutan Komponen</th>
                    <th>Nama Komponen</th>
                    <th>Persentase</th>
                    <th>Operasi</th>
                </tr>
                {$index=1}
                {$total_persentase_komponen=0}
                {foreach $data_komponen_mk as $data}
                    <tr>
                        <td>{$data['URUTAN_KOMPONEN_MK']}</td>
                        <td>{$data['NM_KOMPONEN_MK']}</td>
                        <td>{$data['PERSENTASE_KOMPONEN_MK']} %</td>
                        <td>
                            <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_edit_komponen').dialog('open').load('/modul/dosen/komponen-kkn.php?mode=edit&id_komponen={$data.ID_KOMPONEN_MK}&id_kelas={$id_kelas}')">Edit</span>
                            <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_delete_komponen').dialog('open').load('/modul/dosen/komponen-kkn.php?mode=delete&id_komponen={$data.ID_KOMPONEN_MK}&id_kelas={$id_kelas}')">Hapus</span>
                        </td>
                    </tr>
                    {$total_persentase_komponen = $total_persentase_komponen+$data['PERSENTASE_KOMPONEN_MK']}
                {foreachelse}
                    <tr>
                        <td colspan="4" class="kosong">Data Kosong</td>
                    </tr>
                {/foreach}
                <tr>
                    <td class="center" colspan="4">
                        {if $jumlah_kelas>1}
                            <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_copy_komponen').dialog('open').load('/modul/dosen/komponen-kkn.php?mode=copy&id_kelas={$id_kelas}')">Copy</span>
                        {/if}
                        <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_tambah_komponen').dialog('open').load('/modul/dosen/komponen-kkn.php?mode=tambah&id_kelas={$id_kelas}')">Tambah</span>
                    </td>
                </tr>
            </table>
            {if $total_persentase_komponen!=0 && ($total_persentase_komponen<100 || $total_persentase_komponen>100)}
                <div class="ui-widget ui-state-error ui-corner-all" style="padding: 10px;width:60%;margin: 10px"> 
                    <span class="ui-icon ui-icon-alert" style="float: left;margin:5px;">  </span> 
                    Total persentase komponen Mata Kuliah Harus 100 % tidak boleh lebih atau kurang .. Silahkan diperiksa kembali
                </div>
            {/if}
    {elseif $data_kelas_mk_detail.IS_DIBUKA=='0'}
        <table class="ui-widget" style="width: 70%">
            <tr class="ui-widget-header">
                <th colspan="3" class="header-coloumn">Komponen Penilaian</th>
            </tr>
            <tr>
                <th>Urutan Komponen</th>
                <th>Nama Komponen</th>
                <th>Persentase</th>
            </tr>
            {$index=1}
            {$total_persentase_komponen=0}
            {foreach $data_komponen_mk as $data}
                <tr>
                    <td>{$data['URUTAN_KOMPONEN_MK']}</td>
                    <td>{$data['NM_KOMPONEN_MK']}</td>
                    <td>{$data['PERSENTASE_KOMPONEN_MK']} %</td>                        
                </tr>
                {$total_persentase_komponen = $total_persentase_komponen+$data['PERSENTASE_KOMPONEN_MK']}
            {foreachelse}
                <tr>
                    <td colspan="4" class="kosong">Data Kosong</td>
                </tr>
            {/foreach}
        </table>
        {if $total_persentase_komponen!=0 && ($total_persentase_komponen<100 || $total_persentase_komponen>100)}
            <div class="ui-widget ui-state-error ui-corner-all" style="padding: 10px;width:60%;margin: 10px"> 
                <span class="ui-icon ui-icon-alert" style="float: left;margin:5px;">  </span> 
                Total persentase komponen Mata Kuliah Harus 100 % tidak boleh lebih atau kurang .. Silahkan diperiksa kembali
            </div>
        {/if}
    {/if}
</div>
<div id="dialog_copy_komponen" title="Copy Komponen Nilai"></div>
<div id="dialog_tambah_komponen" title="Tambah Komponen Nilai"></div>
<div id="dialog_edit_komponen" title="Edit Komponen Nilai"></div>
<div id="dialog_delete_komponen" title="Delete Komponen Nilai"></div>
<script type="text/javascript">var page = '{$page}';</script>
{literal}
    <script type="text/javascript">
            $('#select_kelas_mk').change(function(){
                    $.ajax({
                            url : '/modul/dosen/komponen-kkn.php',
                            type : 'post',
                            data : 'id_kelas='+$(this).val()+'&mode=load_table',
                            beforeSend : function(){
                                    $('#table_komponen_nilai').html('<div style="width: 100%;" align="center"><img src="/js/loading.gif" /></div>');
                            },
                            success :function(data){
                                    $('#content').html(data);
                            }
                    })
            })
            $( "#dialog:ui-dialog" ).dialog( "destroy" );
            $( "#dialog_tambah_komponen" ).dialog({
                    width:'500',
                    modal: true,
                    resizable:false,
                    autoOpen:false
            });
            $( "#dialog_edit_komponen" ).dialog({
                    width:'500',
                    modal: true,
                    resizable:false,
                    autoOpen:false
            });
            $( "#dialog_copy_komponen" ).dialog({
                    width:'700',
                    modal: true,
                    resizable:false,
                    autoOpen:false
            });    
            $( "#dialog_delete_komponen" ).dialog({
                    width:'420',
                    modal: true,
                    resizable:false,
                    autoOpen:false
            });
            $('#table_komponen_nilai td').addClass('center ui-widget-content');
    </script>
{/literal}
