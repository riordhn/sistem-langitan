{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">REKAPITULASI ABSEN DOSEN</div>
<table class="ui-widget">
    <tr>
        <td><span>Mata Kuliah</span></td>
        <td>
            <select name="kelas_mk" id="kelas_mk">
                <option>Pilih</option>
                {foreach $data_kelas_mk as $data}
                    <optgroup label="{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})">
                        {foreach $data.DATA_KELAS_MK as $kelas}
                            <option value="{$kelas.ID_KELAS_MK}"{if $kelas.ID_KELAS_MK==$id_kelas_mk}selected="true"{/if}>
                                {$kelas.NM_MATA_KULIAH} ({$kelas.KD_MATA_KULIAH}) Kelas {$kelas.NM_KELAS} <br/> Prodi  {$kelas.NM_PROGRAM_STUDI}
                                {if $kelas.PJMK_PENGAMPU_MK==1}
                            (PJMK)
                        {else}
                            (Anggota)
                        {/if}
                        </option>
                    {/foreach}
                {/foreach}
            </select>
        </td>
    </tr>
</table>
<div id="detail_presensi">
    {if isset($data_pertemuan)}
        <table class="tablesorter ui-widget" style="width: 100%">
            <thead>
                <tr class="ui-widget-header">
                    <th class="header-coloumn" colspan="8">Rekapitulasi Absen</th>
                </tr>
                <tr class="ui-widget-header">
                    <th class="center" >Pertemuan Ke</th>
                    <th class="center" style="width: 150px" >Tanggal</th>
                    <th class="center" style="width: 100px">Waktu</th>
                    <th class="center" >Tempat</th>
                    <th class="center" >Materi</th>
                    <th class="center" >Prosentase</th>
                    <th class="center" >Mhs Hadir</th>
                    <th class="center" >Dosen</th>
                </tr>
            </thead>
            <tbody>
                {$total_persentase=0}
                {foreach $data_pertemuan as $pertemuan}
                    <tr class="ui-widget-content">
                        <td class="center" >{$pertemuan@index+1}</td>
                        <td>{$pertemuan.TGL_PRESENSI_KELAS}</td>
                        <td class="center">{$pertemuan.WAKTU_MULAI} - {$pertemuan.WAKTU_SELESAI}</td>
                        <td><p>{if $pertemuan.NM_RUANGAN!=''}{$pertemuan.NM_RUANGAN} ,{$pertemuan.NM_GEDUNG} ({$pertemuan.LOKASI_GEDUNG}){/if}</p></td>
                        <td>{$pertemuan.ISI_MATERI_MK}</td>
                        <td class="center">{$pertemuan.PERSENTASE_PRESENSI_KELAS} % </td>
                        <td class="center">{$pertemuan.JUMLAH_MHS}  </td>
                        <td class="center">{$pertemuan.NM_PENGGUNA}  </td>
                        {$total_pertemuan = $pertemuan@index+1}
                        {$total_persentase =$total_persentase+$pertemuan.PERSENTASE_PRESENSI_KELAS}
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="8" class="kosong">Data Kosong</td>
                    </tr>
                {/foreach}
            </tbody>
            <tfoot>
                {if count($data_pertemuan)>0}
                    <tr class="ui-widget-content">
                        <td colspan="8" class="center ui-state-highlight">
                            <p style="font-size: 14px">Total Pertemuan <strong>{$total_pertemuan}</strong> <br/> 
                                Rata-Rata Prosentase Kehadiran <strong>{($total_persentase/$total_pertemuan)|string_format:"%.2f"} %</strong> </p>
                        </td>
                    </tr>
                {/if}
            </tfoot>
        </table>
        <p></p>
        <table class="tablesorter ui-widget" style="width: 100%">
            <thead>
                <tr class="ui-widget-header">
                    <th class="header-coloumn" colspan="7">Rekapitulasi Absen Mahasiswa</th>
                </tr>
                <tr>
                    <th class="center">NO</th>
                    <th class="center">NIM</th>
                    <th class="center">Nama</th>
                    <th class="center">Pertemuan</th>
                    <th class="center">Kehadiran</th>
                    <th class="center">Prosentase</th>
                    <th class="center">Status Cekal</th>
                </tr>
            </thead>
            <tbody>
                {if count($data_pertemuan)>0}
                    {foreach $data_presensi_mhs as $dpm}
                        <tr class="ui-widget-content">
                            <td>{$dpm@index+1}</td>
                            <td>{$dpm.NIM_MHS}</td>
                            <td>{$dpm.NM_PENGGUNA}</td>
                            <td>{$dpm.PERTEMUAN}</td>
                            <td>{if $dpm.MASUK!=''}{$dpm.MASUK}{else}0{/if}</td>
                            <td>{if $dpm.KEHADIRAN!=''}{$dpm.KEHADIRAN} %{else}0 %{/if}</td>
                            <td class="center">
                                {if $dpm.STATUS_CEKAL==1&&$dpm.KEHADIRAN<75}
                                    <span style="color: #1878ab">Cekal Belum Di Proses</span>
                                {elseif $dpm.STATUS_CEKAL!=1&&$dpm.KEHADIRAN<75}
                                    <span style="color: red">Di Cekal</span>
                                {else}
                                    <span style="color: green">Tidak Di Cekal</span>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                {else}
                    <tr>
                        <td colspan="7" class="kosong">Data Pertemuan Masih Kosong</td>
                    </tr>
                {/if}
            </tbody>
        </table>
    {/if}
</div>
{literal}
    <script type="text/javascript">
        $('#kelas_mk').change(function() {
            $.ajax({
                url: '/modul/dosen/rekap_absen.php',
                type: 'post',
                data: 'id_kelas_mk=' + $(this).val(),
                beforeSend: function() {
                    $('#detail_presensi').html('<div style="width: 100%;" align="center"><img src="/js/loading.gif" /></div>');
                },
                success: function(data) {
                    $('#content').html(data);
                }
            })
        });
    </script>
{/literal}
