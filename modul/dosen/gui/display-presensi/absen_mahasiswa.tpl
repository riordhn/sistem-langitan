{literal}
    <script type="text/javascript">
        $(function() {
            $("table").tablesorter({sortList: [[0, 0]], widgets: ['zebra']});
        });

    </script>
{/literal}
<div class="center_title_bar">ABSEN MAHASISWA</div>
<table style="width: 98%;font-size: 0.98em;margin-left: 0px">
    <tr>
        <td><span>Mata Kuliah</span></td>
        <td>
            <select name="kelas_mk" id="kelas_mk">
                <option>Pilih</option>
                {foreach $data_kelas_mk as $data}
                    <optgroup label="{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})">
                        {foreach $data.DATA_KELAS_MK as $kelas}
                            <option value="{$kelas.ID_KELAS_MK}"{if $kelas.ID_KELAS_MK==$id_kelas_mk}selected="true"{/if}>
                                {$kelas.NM_MATA_KULIAH} ({$kelas.KD_MATA_KULIAH}) Kelas {$kelas.NM_KELAS} <br/> Prodi  {$kelas.NM_PROGRAM_STUDI}
                                {if $kelas.PJMK_PENGAMPU_MK==1}
                            (PJMK)
                        {else}
                            (Anggota)
                        {/if}
                        </option>
                    {/foreach}
                {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td><span>Pertemuan</span></td>
        <td>
            <select name="pertemuan" id="pertemuan">
                <option>-</option>
                {$index=1}
                {foreach $data_pertemuan as $pertemuan}
                    {if $pertemuan['ID_PRESENSI_KELAS']==$id_presensi_kelas}
                        <option value="{$pertemuan['ID_PRESENSI_KELAS']}" selected="true">Ke {$index++}</option>
                    {else}
                        <option value="{$pertemuan['ID_PRESENSI_KELAS']}">Ke {$index++}</option>
                    {/if}
                {/foreach}
            </select>
        </td>
    </tr>
</table>
<div id="detail_presensi">
    {if isset($data_presensi_mahasiswa)}
        <table style="width: 80%">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn">
                    Detail Pertemuan Mata Kuliah
                </th>
            </tr>
            {if $data_detail_pertemuan!=''}
                <tr>
                    <td class="field">Tanggal Pertemuan</td>
                    <td>: {$data_detail_pertemuan.TGL_PRESENSI_KELAS}</td>
                </tr>
                <tr>
                    <td class="field">Waktu</td>
                    <td>: {$data_detail_pertemuan.WAKTU_MULAI}-{$data_detail_pertemuan.WAKTU_SELESAI}</td>
                </tr>
                <tr>
                    <td class="field">Tempat</td>
                    <td>: {if $pertemuan.NM_RUANGAN!=''}{$pertemuan.NM_RUANGAN} ,{$pertemuan.NM_GEDUNG} ({$pertemuan.LOKASI_GEDUNG}){/if}</td>
                </tr>
                <tr>
                    <td class="field">Persentase Kehadiran</td>
                    <td>: {$data_detail_pertemuan.PERSENTASE_PRESENSI_KELAS} %</td>
                </tr>
                <tr>
                    <td class="field">Dosen Pengajar</td>
                    <td>: {$data_detail_pertemuan.NM_PENGGUNA} </td>
                </tr>
            {else}
                <td colspan="2" class="kosong">Data Pertemuan Kosong</td>
            {/if}
        </table>
        <table class="tablesorter" style="width: 98%">
            <thead>
                <tr>
                    <th colspan="3" class="header-coloumn">Absen Mahasiswa</th>
                </tr>
                <tr class="ui-widget-header">
                    <th style="width: 10%">NIM</th>
                    <th style="width: 65%">Nama</th>
                    <th style="width: 25%">Status Absen</th>
                </tr>
            </thead>
            <tbody>
                {foreach $data_presensi_mahasiswa as $presensi_mahasiswa}
                    <tr class="ui-widget-content">
                        <td><span>{$presensi_mahasiswa['NIM']}</span></td>
                        <td><span>{$presensi_mahasiswa['NAMA']}</span></td>
                        <td class="center">
                            {if $presensi_mahasiswa.KEHADIRAN==1}
                                <div>
                                    <div class="ui-state-highlight ui-corner-all" style="padding: 3px"> 
                                        <p><span class="ui-icon ui-icon-check" style="float: left;"></span>
                                            Hadir</p>
                                    </div>
                                </div>
                            {else}
                                <div>
                                    <div class="ui-state-error ui-corner-all" style="padding: 3px"> 
                                        <p><span class="ui-icon ui-icon-closethick" style="float: left;"></span> 
                                            Tidak Hadir</p>
                                    </div>
                                </div>
                            {/if}
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="3" class="center"><span class="ui-state-error-text">Data Masih Kosong</span></td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    {/if}
</div>
{literal}
    <script type="text/javascript">
        $('#kelas_mk').change(function() {
            $.ajax({
                url: '/modul/dosen/absen_mahasiswa.php',
                type: 'post',
                data: 'id_kelas_mk=' + $(this).val(),
                beforeSend: function() {
                    $('#content').html('<div style="width: 100%;" align="center"><img src="/js/loading.gif" /></div>');
                },
                success: function(data) {
                    $('#content').html(data);
                }
            })
        });
        $('#pertemuan').change(function() {
            $.ajax({
                url: '/modul/dosen/absen_mahasiswa.php',
                type: 'post',
                data: 'id_kelas_mk=' + $('#kelas_mk').val() + '&id_presensi_kelas=' + $(this).val(),
                beforeSend: function() {
                    $('#content').html('<div style="width: 100%;" align="center"><img src="/js/loading.gif" /></div>');
                },
                success: function(data) {
                    $('#content').html(data);
                }
            })
        });
    </script>
{/literal}
