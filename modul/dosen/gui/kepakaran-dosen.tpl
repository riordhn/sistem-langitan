<style type="text/css">
	/*demo page css*/
	body{ font: 80% Cambria, Georgia, serif; margin:0px; padding:0px;}
	.demoHeaders { margin-top: 2em; }
	ul#icons { margin: 0; padding: 0;}
	ul#icons li { margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left; list-style: none; }
	ul#icons span.ui-icon { float: left; margin: 0 4px; }
	.columnbox { width: 500px; }
	#eq span { height: 120px; float: left; margin: 15px; }
	#countries { width: 300px; }
</style>	
<script language="javascript">
	$(document).ready(function() {
		$('#fakultas').change(function(){
			var id_fakultas = $('#fakultas').val();
			$('#get-prodi').load('get-prodi.php?id_fakultas='+id_fakultas);
		})
	});
</script>

<form>
	<table class="ui-widget">
		<tr>
			<td class="ui-widget-content">Pilih Fakultas</td>
			<td class="ui-widget-content">
				<select id="fakultas">
					{for $i=0 to $jml_fakultas-1}
					<option value="{$id_fakultas[$i]}">{$nm_fakultas[$i]}</option>
					{/for}
				</select>
			</td>
		</tr>
		<tr>
			<td class="ui-widget-content">Pilih Prodi</td>
			<td class="ui-widget-content">
				<div id="get-prodi">
				</div>
			</td>
		</tr>
	</table>
</form>

<div id="display">
</div>