<?php
include '../../../config.php';
$smarty->setTemplateDir('../gui');
$smarty->setCompileDir('../gui_c');

$id = $_GET['id'];

if (isset($_POST['submit'])) {
    $id_pengguna = $_POST['id_pengguna'];
    $nama_anak = $_POST['nama'];
    $status = $_POST['status'];
    $pekerjaan = $_POST['pekerjaan'];
    $tgl_lahir = $_POST['tgl_lahir'];
    $kota_lahir = $_POST['tmpat_lahir'];
    $pendidikan = $_POST['pendidikan'];
    $kelamin = $_POST['kelamin'];

    $db->Query("insert into keluarga_anak (id_pengguna, nama_anak, status_anak, kelamin_anak, pekerjaan_anak, tgl_lahir_anak, tempat_lahir_anak ,id_pendidikan_akhir) 
                    values ('{$id_pengguna}',upper('{$nama_anak}'),'{$status}','{$kelamin}','{$pekerjaan}',to_date('{$tgl_lahir}','DD-MM-YYYY'),'{$kota_lahir}','{$pendidikan}')");

    echo '<script>alert("Data berhasil ditambahkan")</script>';
    echo '<script>window.parent.document.location.reload();</script>';
}
$negara = $db->QueryToArray("SELECT * FROM NEGARA ORDER BY NM_NEGARA");
$smarty->assign('negara', $negara);
$id_pdd = $db->QueryToArray("select urut, id_pendidikan_akhir, nama_pendidikan_akhir
		from (
		select id_pendidikan_akhir, nama_pendidikan_akhir,
		case when id_pendidikan_akhir=2 then 11 
		when id_pendidikan_akhir=1 then 12
		when id_pendidikan_akhir=10 then 13  
		when id_pendidikan_akhir=9 then 10 
		when id_pendidikan_akhir=8 then 9
		when id_pendidikan_akhir=7 then 8   
		when id_pendidikan_akhir=3 then 7  
		when id_pendidikan_akhir=4 then 6 
		when id_pendidikan_akhir=5 then 5 
		when id_pendidikan_akhir=6 then 4 
		when id_pendidikan_akhir=13 then 3 
		when id_pendidikan_akhir=12 then 2 
		when id_pendidikan_akhir=11 then 1 
		else 0 end as urut
		from pendidikan_akhir)
		order by urut desc");
$smarty->assign('ID_PDD', $id_pdd);
$smarty->display('display-biodata/insert/insert_klgank.tpl');
?>