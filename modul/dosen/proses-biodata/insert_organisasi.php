<?php
include '../../../config.php';
$smarty->setTemplateDir('../gui');
$smarty->setCompileDir('../gui_c');

$id = $_GET['id'];

if (isset($_POST['submit'])) {
    $id_pengguna = $_POST['id_pengguna'];
    $nama = $_POST['nama'];
    $kedudukan = $_POST['kedudukan'];
    $tgl_mulai = $_POST['tgl_mulai'];
    $tgl_selesai = $_POST['tgl_selesai'];
    $no_sk = $_POST['no_sk'];
    $jabatan_pemberi = $_POST['jabatan_pemberi'];
    $tingkat = $_POST['tingkat'];

    $db->Query("insert into sejarah_organisasi (id_pengguna, nama_organisasi, kedudukan_organisasi, tgl_mulai , tgl_selesai, no_sk_organisasi, jabatan_sk_organisasi ,tingkat_organisasi,status_valid) 
                    values ('{$id_pengguna}',upper('{$nama}'),'{$kedudukan}',to_date('{$tgl_mulai}','DD-MM-YYYY'),to_date('{$tgl_selesai}','DD-MM-YYYY'),'{$no_sk}','{$jabatan_pemberi}','{$tingkat}',1)");

    echo '<script>alert("Data berhasil ditambahkan")</script>';
    echo '<script>window.parent.document.location.reload();</script>';
}
$smarty->display('display-biodata/insert/insert_organisasi.tpl');
?>