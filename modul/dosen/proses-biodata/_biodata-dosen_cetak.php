<?php
require('../../../config.php');
$db = new MyOracle();
$id_pt = $id_pt_user;
$kota_pt = $PT->kota_pt;

$id_pengguna = $user->ID_PENGGUNA;


require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

if ($user->Role() != AUCC_ROLE_DOSEN) {
	echo "anda tidak berhal mengakses halaman ini";
	exit();
}

# untuk info perguruan tinggi by putra rieskha
#$id_pt = $user->ID_PERGURUAN_TINGGI;
#$kota_pt = $user->KOTA_PERGURUAN_TINGGI;
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('langitan');
$pdf->SetTitle('BIODATA DOSEN');
$pdf->SetSubject('BIODATA DOSEN');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logo-".$nama_singkat.".gif";
$logo_size = "25";
$title = strtoupper($nama_pt);


	$db->Query("SELECT dsn.id_pengguna, dsn.id_dosen, dsn.nip_dosen as username, dsn.nidn_dosen, dsn.serdos, 
					upper(dsn.alamat_rumah_dosen) as alamat_rumah_dosen, dsn.tlp_dosen, dsn.mobile_dosen, dsn.status_dosen,
    				UPPER(PGG.NM_PENGGUNA) AS NM_PENGGUNA, PGG.GELAR_DEPAN, PGG.GELAR_BELAKANG, 
    				TO_CHAR(PGG.TGL_LAHIR_PENGGUNA, 'DD-MM-YYYY') AS TGL_LAHIR_PENGGUNA, PGG.EMAIL_PENGGUNA, 
    				PGG.EMAIL_ALTERNATE,
    pgg.kelamin_pengguna,TO_CHAR(gol.TMT_SEJARAH_GOLONGAN, 'DD-MM-YYYY') TMT_GOLONGAN, 
    upper(gol.nm_golongan) as nm_golongan, upper(gol.nm_pangkat) as nm_pangkat, 
    upper(fgs.nm_jabatan_fungsional) as nm_jabatan_fungsional,
    TO_CHAR(fgs.TMT_SEJ_JAB_FUNGSIONAL, 'DD-MM-YYYY') TMT_JAB_FUNGSIONAL, kt.tipe_dati2||' '||nm_kota as tempat_lahir,
    UPPER(JAB.NM_JABATAN_PEGAWAI) AS NM_JABATAN_PEGAWAI, UPPER(STP.NM_STATUS_PENGGUNA) AS NM_STATUS_PENGGUNA, 
    UPPER(NM_JENJANG||' - '||NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI, UPPER(NM_FAKULTAS) AS NM_FAKULTAS,
    AG.NM_AGAMA,DEP.NM_DEPARTEMEN,SPEN.*,dsn.NIP_LAMA,STRUK.*,SNIKAH.*,USEL.*,DSN.*,DDEP.ID_DEPARTEMEN DEPARTEMEN_DOSEN,
    UKTP.NAMA_UPLOAD_FILE FILE_KTP,UCPNS.NAMA_UPLOAD_FILE SK_CPNS,UPNS.NAMA_UPLOAD_FILE SK_PNS,
    UKON.NAMA_UPLOAD_FILE SK_KONVERSI,
    UPEN.NAMA_UPLOAD_FILE SK_PENEMPATAN,UTET.NAMA_UPLOAD_FILE SK_DOSEN_TETAP,PGG.ID_AGAMA,PGG.ID_KOTA_LAHIR
    from AUCC.dosen dsn
    JOIN AUCC.PENGGUNA PGG ON DSN.ID_PENGGUNA=PGG.ID_PENGGUNA
    LEFT JOIN AUCC.KOTA KT ON KT.ID_KOTA=PGG.ID_KOTA_LAHIR
    JOIN AUCC.PROGRAM_STUDI PST ON DSN.ID_PROGRAM_STUDI_SD=PST.ID_PROGRAM_STUDI
    JOIN AUCC.FAKULTAS FAK ON PST.ID_FAKULTAS=FAK.ID_FAKULTAS
    LEFT JOIN AUCC.DOSEN_DEPARTEMEN DDEP ON DDEP.ID_DOSEN=DSN.ID_DOSEN AND STATUS=1
    LEFT JOIN AUCC.DEPARTEMEN DEP ON DEP.ID_DEPARTEMEN=DDEP.ID_DEPARTEMEN
    join AUCC.jenjang jjg on pst.id_jenjang=jjg.id_jenjang
    LEFT JOIN AUCC.AGAMA AG ON AG.ID_AGAMA=PGG.ID_AGAMA
    LEFT JOIN AUCC.STATUS_PERNIKAHAN SNIKAH ON SNIKAH.ID_STATUS_PERNIKAHAN=PGG.ID_STATUS_PERNIKAHAN
    LEFT JOIN AUCC.UNIT_ESSELON USEL ON USEL.ID_PENGGUNA=PGG.ID_PENGGUNA
    LEFT JOIN AUCC.UPLOAD_FOLDER UF ON UPPER(UF.NAMA_UPLOAD_FOLDER) LIKE UPPER('%'||dsn.ID_PENGGUNA||'%')
    LEFT JOIN AUCC.UPLOAD_FILE UKTP ON UPPER(UKTP.NAMA_UPLOAD_FILE) LIKE UPPER('%file_ktp%') AND UKTP.ID_UPLOAD_FOLDER=UF.ID_UPLOAD_FOLDER
    LEFT JOIN AUCC.UPLOAD_FILE UCPNS ON UPPER(UCPNS.NAMA_UPLOAD_FILE) LIKE UPPER('%sk_cpns%') AND UCPNS.ID_UPLOAD_FOLDER=UF.ID_UPLOAD_FOLDER
    LEFT JOIN AUCC.UPLOAD_FILE UPNS ON UPPER(UPNS.NAMA_UPLOAD_FILE) LIKE UPPER('%sk_pns%') AND UPNS.ID_UPLOAD_FOLDER=UF.ID_UPLOAD_FOLDER
    LEFT JOIN AUCC.UPLOAD_FILE UKON ON UPPER(UKON.NAMA_UPLOAD_FILE) LIKE UPPER('%konversi_nip%') AND UKON.ID_UPLOAD_FOLDER=UF.ID_UPLOAD_FOLDER
    LEFT JOIN AUCC.UPLOAD_FILE UPEN ON UPPER(UPEN.NAMA_UPLOAD_FILE) LIKE UPPER('%sk_penempatan_prodi%') AND UPEN.ID_UPLOAD_FOLDER=UF.ID_UPLOAD_FOLDER
    LEFT JOIN AUCC.UPLOAD_FILE UTET ON UPPER(UTET.NAMA_UPLOAD_FILE) LIKE UPPER('%sk_dosen_tetap%') AND UTET.ID_UPLOAD_FOLDER=UF.ID_UPLOAD_FOLDER
    left join (
    SELECT * FROM (
        SELECT * FROM (
          SELECT G.NM_GOLONGAN,G.NM_PANGKAT,SJ.*
          FROM AUCC.SEJARAH_GOLONGAN SJ
          JOIN AUCC.GOLONGAN G ON SJ.ID_GOLONGAN=G.ID_GOLONGAN
          WHERE SJ.ID_PENGGUNA={$id_pengguna}
          ORDER BY SJ.TMT_SEJARAH_GOLONGAN DESC
        ) WHERE ROWNUM=1
      )
    ) GOL ON GOL.ID_PENGGUNA=DSN.ID_PENGGUNA
    LEFT JOIN (
        SELECT * FROM (
            SELECT * FROM (
              SELECT G.NM_JABATAN_FUNGSIONAL,SJ.*
              FROM AUCC.SEJARAH_JABATAN_FUNGSIONAL SJ
              JOIN AUCC.JABATAN_FUNGSIONAL G ON SJ.ID_JABATAN_FUNGSIONAL=G.ID_JABATAN_FUNGSIONAL
              WHERE SJ.ID_PENGGUNA={$id_pengguna}
              ORDER BY SJ.TMT_SEJ_JAB_FUNGSIONAL DESC
            ) WHERE ROWNUM=1
      )
    ) FGS ON FGS.ID_PENGGUNA=DSN.ID_PENGGUNA
    LEFT JOIN (
        SELECT * FROM (
            SELECT * FROM (
              SELECT G.NM_JABATAN_STRUKTURAL,SJ.*
              FROM AUCC.SEJARAH_JABATAN_STRUKTURAL SJ
              JOIN AUCC.JABATAN_STRUKTURAL G ON SJ.ID_JABATAN_STRUKTURAL=G.ID_JABATAN_STRUKTURAL
              WHERE SJ.ID_PENGGUNA={$id_pengguna}
              ORDER BY SJ.TMT_SEJ_JAB_STRUKTURAL DESC
            ) WHERE ROWNUM=1
      )
    ) STRUK ON STRUK.ID_PENGGUNA=DSN.ID_PENGGUNA
    LEFT JOIN (
        SELECT * FROM (
            SELECT * FROM (
              SELECT NAMA_PENDIDIKAN_AKHIR,SP.*
              FROM AUCC.SEJARAH_PENDIDIKAN SP
              JOIN AUCC.PENDIDIKAN_AKHIR PA ON PA.ID_PENDIDIKAN_AKHIR=SP.ID_PENDIDIKAN_AKHIR
              WHERE SP.ID_PENGGUNA={$id_pengguna}
              ORDER BY SP.TAHUN_LULUS_PENDIDIKAN DESC,ID_SEJARAH_PENDIDIKAN DESC
            ) WHERE ROWNUM=1
      )
    ) SPEN ON SPEN.ID_PENGGUNA=DSN.ID_PENGGUNA
    LEFT JOIN AUCC.JABATAN_PEGAWAI JAB ON JAB.ID_JABATAN_PEGAWAI=DSN.ID_JABATAN_PEGAWAI
    left join AUCC.status_pengguna stp on stp.id_status_pengguna=dsn.id_status_pengguna
    where pgg.id_pengguna='".$user->ID_PENGGUNA."'")or die("salah kueri 21 ");;


$dsn = $db->FetchAssoc();

$nip_dosen = $dsn['USERNAME'];
// nip lama
if($dsn['NIP_LAMA'] != ''){
	$nip_lama_dosen = $dsn['NIP_LAMA'];
}
else{
	$nip_lama_dosen = '-';
}
// nidn dosen
if($dsn['NIDN_DOSEN'] > 0){
	$nidn_dosen = $dsn['NIDN_DOSEN'];
}
else{
	$nidn_dosen = '-';
}
// pendidikan akhir dosen
if ($dsn['NAMA_PENDIDIKAN_AKHIR'] !=''){
  $pendidikan_akhir_dosen = $dsn['NAMA_PENDIDIKAN_AKHIR'].' '.$dsn['NM_SEKOLAH_PENDIDIKAN'].' '.$dsn['NM_JURUSAN_PENDIDIKAN'].' ('.$dsn['TAHUN_MASUK_PENDIDIKAN'].'-'.$dsn['TAHUN_LULUS_PENDIDIKAN'].')';
}
else{
  $pendidikan_akhir_dosen = '-';
}
$gelar_depan_dosen = $dsn['GELAR_DEPAN'];
$gelar_belakang_dosen = $dsn['GELAR_BELAKANG'];
// no ktp dosen
if($dsn['NO_KTP'] != ''){
  $ktp_dosen = $dsn['NO_KTP'];
}
else{
  $ktp_dosen = '-';
}
$nm_dosen = $dsn['NM_PENGGUNA'];
$prodi_dosen = $dsn['NM_PROGRAM_STUDI'];
$ttl_dosen = $dsn['TEMPAT_LAHIR'].', '.$dsn['TGL_LAHIR_PENGGUNA'];
// jenis kelamin dosen
if($dsn['KELAMIN_PENGGUNA'] =='1'){
	$jenkel_dosen = 'LAKI-LAKI';
}
elseif($dsn['KELAMIN_PENGGUNA'] =='2'){
	$jenkel_dosen = 'PEREMPUAN';
}
else{
	$jenkel_dosen = '-';
}
$agama_dosen = $dsn['NM_AGAMA'];
// status nikah dosen
if($dsn['NM_STATUS_PERNIKAHAN'] != ''){
	$status_nikah_dosen = $dsn['NM_STATUS_PERNIKAHAN'];
}
else{
	$status_nikah_dosen = '-';
}
$alamat_dosen = $dsn['ALAMAT_RUMAH_DOSEN'];
$kodepos_dosen = $dsn['KODE_POS'];
// telp hp dosen
if($dsn['TLP_DOSEN'] == ''){
	$telp_hp_dosen = $dsn['MOBILE_DOSEN'];
}
elseif($dsn['MOBILE_DOSEN'] == ''){
	$telp_hp_dosen = $dsn['TLP_DOSEN'];
}
else{
	$telp_hp_dosen = $dsn['TLP_DOSEN'].' / '.$dsn['MOBILE_DOSEN'];
}
$email1_dosen = $dsn['EMAIL_PENGGUNA'];
$blog_dosen = $dsn['BLOG_PENGGUNA'];
$email2_dosen = $dsn['EMAIL_ALTERNATE'];



$tahun_sekarang = date('Y');
$bulan_sekarang = date('m');
$tgl_sekarang = date('d');
$jam_sekarang = date('H:i:s');

if($bulan_sekarang=='01'){ $bulan_sekarang = 'Januari'; }
elseif($bulan_sekarang=='02'){ $bulan_sekarang = 'Februari'; }
elseif($bulan_sekarang=='03'){ $bulan_sekarang = 'Maret'; }
elseif($bulan_sekarang=='04'){ $bulan_sekarang = 'April'; }
elseif($bulan_sekarang=='05'){ $bulan_sekarang = 'Mei'; }
elseif($bulan_sekarang=='06'){ $bulan_sekarang = 'Juni'; }
elseif($bulan_sekarang=='07'){ $bulan_sekarang = 'Juli'; }
elseif($bulan_sekarang=='08'){ $bulan_sekarang = 'Agustus'; }
elseif($bulan_sekarang=='09'){ $bulan_sekarang = 'September'; }
elseif($bulan_sekarang=='10'){ $bulan_sekarang = 'Oktober'; }
elseif($bulan_sekarang=='11'){ $bulan_sekarang = 'November'; }
elseif($bulan_sekarang=='12'){ $bulan_sekarang = 'Desember'; }



$biomhs="select d.nip_dosen, upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi,  
        ps.id_program_studi,
			 case when f.id_fakultas=9 then 'PROGRAM '||f.nm_fakultas else 'FAKULTAS '||f.nm_fakultas end, 
			f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas, f.id_fakultas,
			d.id_program_studi,d.id_dosen
			from dosen d 
			left join pengguna p on d.id_pengguna=p.id_pengguna and p.id_perguruan_tinggi = '{$id_pt}' 
			left join program_studi ps on d.id_program_studi=ps.id_program_studi
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			where d.nip_dosen='".$nip_dosen."'";
$result1 = $db->Query($biomhs)or die("salah kueri 2 ");
while($r1 = $db->FetchRow()) {
	$dosen_nip = $r1[0];
	$dosen_nama = $r1[1];
	$jenjang = $r1[2];
	$prodi = $r1[3];
  $id_prodi = $r1[4];
	$fak = $r1[5];
	$alm_fak = $r1[6];
	$pos_fak = $r1[7];
	$tel_fak = $r1[8];
	$fax_fak = $r1[9];
	$web_fak = $r1[10];
	$eml_fak = $r1[11];
	$kd_fak = $r1[12];
	$kd_prodi = $r1[13];
	$id_dosen=$r1[14];
}



	$fotodosen = "../../../foto_pegawai/{$nama_singkat}/".$mhs_nim.".jpg";
	## jika extension .jpg dalam huruf kapital
	if (file_exists($fotodosen) == 0) 
	{
		$fotodosen = "../../../foto_mhs/{$nama_singkat}/".$mhs_nim.".JPG";
	}

	if (file_exists($fotodosen) == 0) 
	{
		$fotodosen = "../../../img/dosen/photo.png";
	}

$klgdr = $db->QueryToArray("
    select * from
    (
    SELECT KD.*,K.NM_KOTA KOTA_LAHIR
                FROM KELUARGA_DIRI KD
                LEFT JOIN KOTA K ON K.ID_KOTA=KD.ID_KOTA_LAHIR
                WHERE KD.ID_PENGGUNA='{$id_pengguna}'
    )
    order by TGL_LAHIR_KELUARGA desc");
//$smarty->assign('KLGDR', $klgdr);

$klgpsg = $db->QueryToArray("
    select * from
    (
    SELECT KD.*,K.NM_KOTA KOTA_LAHIR,TO_CHAR(KD.TGL_LAHIR_PASANGAN, 'DD-MM-YYYY') TGL_LAHIR
                FROM KELUARGA_PASANGAN KD
                LEFT JOIN KOTA K ON K.ID_KOTA=KD.ID_KOTA_LAHIR
                WHERE KD.ID_PENGGUNA='{$id_pengguna}'
    )
    order by TGL_LAHIR_PASANGAN desc");
//$smarty->assign('KLGPSG', $klgpsg);

$klgank = $db->QueryToArray("
    select * from
    (
    SELECT KD.*,K.NM_KOTA KOTA_LAHIR,TO_CHAR(KD.TGL_LAHIR_ANAK, 'DD-MM-YYYY') TGL_LAHIR,PA.NAMA_PENDIDIKAN_AKHIR
                FROM KELUARGA_ANAK KD
                LEFT JOIN KOTA K ON K.ID_KOTA=KD.TEMPAT_LAHIR_ANAK
                LEFT JOIN PENDIDIKAN_AKHIR PA ON KD.ID_PENDIDIKAN_AKHIR=PA.ID_PENDIDIKAN_AKHIR
                WHERE KD.ID_PENGGUNA='{$id_pengguna}'
    )
    order by TGL_LAHIR_ANAK desc");
//$smarty->assign('KLGANK', $klgank);

$pernikahan = $db->QueryToArray("
    select * from
                (
                  SELECT D.*,TO_CHAR(D.TGL_LAHIR_PASANGAN, 'DD-MM-YYYY') TGL_LAHIR,TO_CHAR(D.TGL_PERNIKAHAN, 'DD-MM-YYYY') TGL_NIKAH,UF.NAMA_UPLOAD_FILE FILES
                    FROM AUCC.SEJARAH_PERNIKAHAN D
                    LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_pernikahan_'||D.ID_SEJARAH_PERNIKAHAN||'%')
                    WHERE D.ID_PENGGUNA='{$id_pengguna}'
                ) 
                order by tgl_nikah desc");
//$smarty->assign('NIKAH', $pernikahan);

$gol_dosen = $db->QueryToArray("
    select a.*,UF.NAMA_UPLOAD_FILE FILES from
    (
    (select sg.id_sejarah_golongan, sg.id_golongan, upper(gol.nm_golongan) as nm_golongan,gol.nm_pangkat, sg.no_sk_sejarah_golongan, sg.asal_sk_sejarah_golongan,
    sg.keterangan_sk_sejarah_golongan, TO_CHAR(sg.tmt_sejarah_golongan, 'DD-MM-YYYY') as tmt_sejarah_golongan, sg.status_akhir, TO_CHAR(sg.tmt_sejarah_golongan, 'YYYY') as tahun,
    'SK_'||replace(upper(gol.nm_golongan),'/','_') as kategori, TO_CHAR(sg.tgl_sk_sejarah_golongan, 'DD-MM-YYYY') as tgl_sk_sejarah_golongan, sg.ttd_sk_nama_pejabat
    from sejarah_golongan sg
    left join pengguna pgg on pgg.id_pengguna=sg.id_pengguna
    left join golongan gol on gol.id_golongan=sg.id_golongan
    where pgg.id_pengguna=$id_pengguna) a
    LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_pangkat_golongan_'||a.id_sejarah_golongan||'%')
    )
    order by a.status_akhir desc, a.tahun desc
    ");
//$smarty->assign('GOL', $gol_dosen);

$jab_dosen = $db->QueryToArray("
    select A.*, UF.*, UF.NAMA_UPLOAD_FILE FILES from
    (
    (select sf.id_sejarah_jabatan_fungsional, sf.id_jabatan_fungsional, jab.nm_jabatan_fungsional, sf.no_sk_sej_jab_fungsional, sf.asal_sk_sej_jab_fungsional,
    sf.ket_sk_sej_jab_fungsional, TO_CHAR(sf.tmt_sej_jab_fungsional, 'DD-MM-YYYY') as tmt_sej_jab_fungsional, sf.status_akhir, TO_CHAR(sf.tmt_sej_jab_fungsional, 'YYYY') as tahun,
    'SK_'||replace(upper(jab.nm_jabatan_fungsional),' ','_') as kategori, TO_CHAR(sf.tgl_sk_sej_jab_fungsional, 'DD-MM-YYYY') as tgl_sk_sej_jab_fungsional, sf.ttd_sk_sej_jab_fungsional
    from sejarah_jabatan_fungsional sf
    left join pengguna pgg on pgg.id_pengguna=sf.id_pengguna
    left join jabatan_fungsional jab on jab.id_jabatan_fungsional=sf.id_jabatan_fungsional
    where pgg.id_pengguna=$id_pengguna) a
    LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_jabatan_fungsional_'||a.id_sejarah_jabatan_fungsional||'%')
    )
    order by a.status_akhir desc, a.tahun desc");
//$smarty->assign('JAB', $jab_dosen);

$pdd_dosen = $db->QueryToArray("
    select a.*,UF1.NAMA_UPLOAD_FILE IJAZAH,UF2.NAMA_UPLOAD_FILE TRANSKRIP,UF3.NAMA_UPLOAD_FILE PENYETARAAN from
    (
    (select pdd.*, to_date(pdd.tahun_lulus_pendidikan,'YYYY') as tahun_lulus, pda.nama_pendidikan_akhir, 'IJASAH_'||upper(pda.nama_pendidikan_akhir) as kategori
    from sejarah_pendidikan pdd
    left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
    left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
    where pgg.id_pengguna=$id_pengguna) a
    LEFT JOIN AUCC.UPLOAD_FILE UF1 ON UPPER(UF1.NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_pendidikan_ijazah_'||a.id_sejarah_pendidikan||'%') 
                LEFT JOIN AUCC.UPLOAD_FILE UF2 ON UPPER(UF2.NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_pendidikan_transkrip_'||a.id_sejarah_pendidikan||'%') 
                LEFT JOIN AUCC.UPLOAD_FILE UF3 ON UPPER(UF3.NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_pendidikan_penyetaraan_'||a.id_sejarah_pendidikan||'%') 
                )
    order by a.status_akhir desc, a.tahun_lulus desc");
//$smarty->assign('PEND', $pdd_dosen);

$org_dosen = $db->QueryToArray("select org.*,TO_CHAR(org.tgl_mulai, 'DD-MM-YYYY') as t_mulai,TO_CHAR(org.tgl_selesai, 'DD-MM-YYYY') as t_selesai,UF.NAMA_UPLOAD_FILE FILES
    from sejarah_organisasi org
                LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_organisasi_'||org.id_sejarah_organisasi||'%')
    where org.id_pengguna=$id_pengguna order by tgl_mulai desc, tgl_selesai desc");
//$smarty->assign('PORG', $org_dosen);

$trg = $db->QueryToArray("select keg.*,TO_CHAR(keg.tgl_mulai_kegiatan_pegawai, 'DD-MM-YYYY') as t_mulai,
                TO_CHAR(keg.tgl_selesai_kegiatan_pegawai, 'DD-MM-YYYY') as t_selesai,k.NM_KOTA,UF.NAMA_UPLOAD_FILE FILES
    from aucc.sejarah_kegiatan_pegawai keg
    left join aucc.kota k on keg.id_kota_kegiatan_pegawai=k.id_kota
                LEFT JOIN AUCC.UPLOAD_FILE UF ON UPPER(NAMA_UPLOAD_FILE) LIKE UPPER('%riwayat_workshop_seminar_training_'||keg.id_sejarah_kegiatan_pegawai||'%')
    where keg.id_pengguna=$id_pengguna order by tgl_selesai_kegiatan_pegawai desc");
//$smarty->assign('PTRG', $trg);

$pkrj = $db->QueryToArray("select keg.*,TO_CHAR(keg.tgl_sk_jabatan, 'DD-MM-YYYY') as t_sk,TO_CHAR(keg.tmt_jabatan, 'DD-MM-YYYY') as t_tmt,TO_CHAR(keg.tgl_mulai_jabatan, 'DD-MM-YYYY') as t_mulai
    from sejarah_pekerjaan keg
    where keg.id_pengguna=$id_pengguna order by t_sk desc");
//$smarty->assign('PEKERJAAN', $pkrj);

//echo "hahahvvvaaaddds".$gelar_belakang_dosen;

$content = strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default footer
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('P', 'A4');

// draw jpeg image
//$pdf->Image('../includes/draft.png', 30, 70, 140, '40', '', '', '', false, 72);

// set the starting point for the page content
$pdf->setPageMark();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="center"><span style="font-size: large;"><b>BIODATA DOSEN</b></span></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="left"><b>Identitas Pribadi Dosen</b></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td width="26%">&nbsp;&nbsp;NIP / NIK</td>
	<td width="2%" align="center">:</td>
	<td width="52%">'.$nip_dosen.'</td>
	<td rowspan="6" width="20%"><img src="'.$fotodosen.'" width="120" height="150"></td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;NIP / NIK Lama</td>
	<td width="2%" align="center">:</td>
	<td width="52%">'.$nip_lama_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;NIDN</td>
	<td width="2%" align="center">:</td>
	<td width="52%">'.$nidn_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Pendidikan Akhir</td>
	<td width="2%" align="center">:</td>
	<td width="52%">'.$pendidikan_akhir_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Gelar Depan</td>
	<td width="2%" align="center">:</td>
	<td width="52%">'.$gelar_depan_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Gelar Belakang</td>
	<td width="2%" align="center">:</td>
	<td width="52%">'.$gelar_belakang_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;NO KTP</td>
  <td width="2%" align="center">:</td>
  <td width="52%">'.$ktp_dosen.'</td>
  </tr>
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td width="26%">&nbsp;&nbsp;Nama</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$nm_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Program Studi</td>
  <td width="2%" align="center">:</td>
  <td width="72%">'.$prodi_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Tempat, Tanggal Lahir</td>
  <td width="2%" align="center">:</td>
  <td width="72%">'.$ttl_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Jenis Kelamin</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$jenkel_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Agama</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$agama_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Status Pernikahan</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$status_nikah_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Alamat</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$alamat_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Kode Pos</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$kodepos_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Telepon/HP</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$telp_hp_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Email #1</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$email1_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Blog Dosen</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$blog_dosen.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Email #2</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$email2_dosen.'</td>
  </tr>
</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// Next page
$pdf->AddPage();

$html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="center"><span style="font-size: large;"><b>KELUARGA DIRI</b></span></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table>
    ';
if(!empty($klgdr)){
  foreach ($klgdr as $anak) {
    if($anak['KELAMIN_KELUARGA'] == '1'){
      $kelamin_keluarga = 'Laki Laki'; 
    }
    elseif($anak['KELAMIN_KELUARGA'] == '2'){
      $kelamin_keluarga = 'Perempuan'; 
    }

    if($anak['HUBUNGAN_KELUARGA'] == '1'){
      $status_keluarga = 'Ayah'; 
    }
    elseif($anak['HUBUNGAN_KELUARGA'] == '2'){
      $status_keluarga = 'Ibu'; 
    }
    elseif($anak['HUBUNGAN_KELUARGA'] == '3'){
      $status_keluarga = 'Kakak'; 
    }
    elseif($anak['HUBUNGAN_KELUARGA'] == '4'){
      $status_keluarga = 'Adik'; 
    }

    if($anak['KONDISI'] == '1'){
      $kondisi_keluarga = 'Sudah Meninggal'; 
    }
    elseif($anak['KONDISI'] == '2'){
      $kondisi_keluarga = 'Masih Hidup'; 
    }

    $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NAMA_KELUARGA'].' - '.$kelamin_keluarga.'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Hubungan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$status_keluarga.'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Pekerjaan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['PEKERJAAN_KELUARGA'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tempat, Tanggal Lahir</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['KOTA_LAHIR'].', '.$anak['TGL_LAHIR_KELUARGA'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Kondisi</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$kondisi_keluarga.'</td>
      </tr>
      <br/>
      ';
  }
}
else{
  $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Hubungan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Pekerjaan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tempat, Tanggal Lahir</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Kondisi</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <br/>
      ';
}

$html .= '
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="center"><span style="font-size: large;"><b>KELUARGA PASANGAN</b></span></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table>
    ';
if(!empty($klgpsg)){
  foreach ($klgpsg as $anak) {
    if($anak['KELAMIN_PASANGAN'] == '1'){
      $kelamin_pasangan = 'Laki Laki'; 
    }
    elseif($anak['KELAMIN_PASANGAN'] == '2'){
      $kelamin_pasangan = 'Perempuan'; 
    }

    if($anak['HUBUNGAN_PASANGAN'] == '1'){
      $status_pasangan = 'Ayah'; 
    }
    elseif($anak['HUBUNGAN_PASANGAN'] == '2'){
      $status_pasangan = 'Ibu'; 
    }
    elseif($anak['HUBUNGAN_PASANGAN'] == '3'){
      $status_pasangan = 'Kakak'; 
    }
    elseif($anak['HUBUNGAN_PASANGAN'] == '4'){
      $status_pasangan = 'Adik'; 
    }

    if($anak['KONDISI'] == '1'){
      $kondisi_pasangan = 'Sudah Meninggal'; 
    }
    elseif($anak['KONDISI'] == '2'){
      $kondisi_pasangan = 'Masih Hidup'; 
    }

    $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NAMA_PASANGAN'].' - '.$kelamin_pasangan.'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Hubungan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$status_pasangan.'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Pekerjaan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['PEKERJAAN_PASANGAN'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tempat, Tanggal Lahir</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['KOTA_LAHIR'].', '.$anak['TGL_LAHIR_PASANGAN'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Kondisi</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$kondisi_pasangan.'</td>
      </tr>
      <br/>
      ';
  }
}
else{
  $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Hubungan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Pekerjaan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tempat, Tanggal Lahir</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Kondisi</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <br/>
      ';
}

$html .= '
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="center"><span style="font-size: large;"><b>KELUARGA ANAK</b></span></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table>
    ';
if(!empty($klgank)){
  foreach ($klgank as $anak) {
    if($anak['KELAMIN_ANAK'] == '1'){
      $kelamin_anak = 'Laki Laki'; 
    }
    elseif($anak['KELAMIN_ANAK'] == '2'){
      $kelamin_anak = 'Perempuan'; 
    }

    if($anak['STATUS_ANAK'] == '1'){
      $status_anak = 'Anak Kandung'; 
    }
    elseif($anak['STATUS_ANAK'] == '2'){
      $status_anak = 'Anak Tiri'; 
    }
    elseif($anak['STATUS_ANAK'] == '3'){
      $status_anak = 'Anak Angkat'; 
    }

    $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NAMA_ANAK'].' - '.$kelamin_anak.'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tempat, Tanggal Lahir</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['KOTA_LAHIR'].', '.$anak['TGL_LAHIR'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Pendidikan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['PENDIDIKAN_AKHIR'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Status Anak</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$status_anak.'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Pekerjaan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['PEKERJAAN_ANAK'].'</td>
      </tr>
      <br/>
      ';
  }
}
else{
  $html .= '
    <tr>
        <td width="26%">&nbsp;&nbsp;Nama</td>
        <td width="2%" align="center">:</td>
        <td width="72%"></td>
    </tr>
    <tr>
        <td width="26%">&nbsp;&nbsp;Tempat, Tanggal Lahir</td>
        <td width="2%" align="center">:</td>
        <td width="72%"></td>
    </tr>
    <tr>
        <td width="26%">&nbsp;&nbsp;Pendidikan</td>
        <td width="2%" align="center">:</td>
        <td width="72%"></td>
    </tr>
    <tr>
        <td width="26%">&nbsp;&nbsp;Status Anak</td>
        <td width="2%" align="center">:</td>
        <td width="72%"></td>
    </tr>
    <tr>
        <td width="26%">&nbsp;&nbsp;Pekerjaan</td>
        <td width="2%" align="center">:</td>
        <td width="72%"></td>
    </tr>
    <br/>
    ';
}

/*// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// Next page
$pdf->AddPage();*/

$html .= '
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="center"><span style="font-size: large;"><b>RIWAYAT PERNIKAHAN</b></span></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table>
    ';

if(!empty($pernikahan)){
  foreach ($pernikahan as $anak) {
    if($anak['STATUS_PASANGAN'] == '1'){
      $status_pasangan = 'Suami/Istri Saat Ini'; 
    }
    elseif($anak['STATUS_PASANGAN'] == '2'){
      $status_pasangan = 'Sudah Meninggal'; 
    }
    else{
      $status_pasangan = 'Cerai'; 
    }

    $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama Pasangan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NAMA_PASANGAN'].' - '.$status_pasangan.'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Nomor Karsis/Karsu</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NOMOR_KARSISU'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tgl Lahir</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['TGL_LAHIR'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tgl Nikah</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['TGL_NIKAH'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Pekerjaan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['PEKERJAAN_PASANGAN'].'</td>
      </tr>
      <br/>
      ';
  }
}
else{
  $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama Pasangan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Nomor Karsis/Karsu</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tgl Lahir</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tgl Nikah</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Pekerjaan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <br/>
      ';
}


$html .= '
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="center"><span style="font-size: large;"><b>RIWAYAT PANGKAT GOLONGAN</b></span></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table>
    ';
if(!empty($gol_dosen)){
  foreach ($gol_dosen as $anak) {

    $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Pangkat</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NM_PANGKAT'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Golongan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NM_GOLONGAN'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Surat Keputusan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NO_SK_SEJARAH_GOLONGAN'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Asal SK/Tanggal SK</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['ASAL_SK_SEJARAH_GOLONGAN'].' - '.$anak['TGL_SK_SEJARAH_GOLONGAN'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Penandatangan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['TTD_SK_NAMA_PEJABAT'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;TMT</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['TMT_SEJARAH_GOLONGAN'].'</td>
      </tr>
      <br/>
      ';
  }
}
else{
  $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Pangkat</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Golongan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Surat Keputusan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Asal SK/Tanggal SK</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Penandatangan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;TMT</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <br/>
      ';
}

$html .= '
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="center"><span style="font-size: large;"><b>RIWAYAT JABATAN FUNGSIONAL</b></span></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table>
    ';
if(!empty($jab_dosen)){
  foreach ($jab_dosen as $anak) {

    $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Fungsional</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NM_JABATAN_FUNGSIONAL'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Surat Keputusan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NO_SK_SEJ_JAB_FUNGSIONAL'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Asal SK/Tanggal SK</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['ASAL_SK_SEJ_JAB_FUNGSIONAL'].' / '.$anak['TGL_SK_SEJ_JAB_FUNGSIONAL'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Penandatangan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['TTD_SK_SEJ_JAB_FUNGSIONAL'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;TMT</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['TMT_SEJ_JAB_FUNGSIONAL'].'</td>
      </tr>
      <br/>
      ';
  }
}
else{
  $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Fungsional</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Surat Keputusan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Asal SK/Tanggal SK</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Penandatangan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;TMT</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <br/>
      ';
}

$html .= '
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="center"><span style="font-size: large;"><b>PENDIDIKAN</b></span></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table>
    ';
if(!empty($pdd_dosen)){
  foreach ($pdd_dosen as $anak) {

    $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Jenjang</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NAMA_PENDIDIKAN_AKHIR'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama Sekolah</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NM_SEKOLAH_PENDIDIKAN'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Jurusan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NM_JURUSAN_PENDIDIKAN'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tempat</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['TEMPAT_PENDIDIKAN'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tahun Lulus</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['TAHUN_LULUS_PENDIDIKAN'].'</td>
      </tr>
      <br/>
      ';
  }
}
else{
  $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Jenjang</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama Sekolah</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Jurusan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tempat</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tahun Lulus</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <br/>
      ';
}

$html .= '
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="center"><span style="font-size: large;"><b>ORGANISASI</b></span></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table>
    ';
if(!empty($org_dosen)){
  foreach ($org_dosen as $anak) {

    $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NAMA_ORGANISASI'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Jabatan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['KEDUDUKAN_ORGANISASI'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Waktu</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['T_MULAI'].' - '.$anak['T_SELESAI'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;SK Organisasi</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NO_SK_ORGANISASI'].' '.$anak['JABATAN_SK_ORGANISASI'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tingkat Organisasi</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['TINGKAT_ORGANISASI'].'</td>
      </tr>
      <br/>
      ';
  }
}
else{
  $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Jabatan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Waktu</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;SK Organisasi</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tingkat Organisasi</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <br/>
      ';
}

$html .= '
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="center"><span style="font-size: large;"><b>TRAINING/WORKSHOP/SEMINAR</b></span></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table>
    ';
if(!empty($trg)){
  foreach ($trg as $anak) {
    if($anak['TINGKAT_KEGIATAN_PEGAWAI'] == '1'){
      $tingkat_trg = 'Nasional'; 
    }
    elseif($anak['TINGKAT_KEGIATAN_PEGAWAI'] == '2'){
      $tingkat_trg = 'Internasional'; 
    }

    $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Jenis</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['JENIS_KEGIATAN_PEGAWAI'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NAMA_KEGIATAN_PEGAWAI'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Lokasi</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['LOKASI_KEGIATAN_PEGAWAI'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Waktu</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['TGL_MULAI'].' - '.$anak['TGL_SELESAI'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Kedudukan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['KEDUDUKAN_KEGIATAN_PEGAWAI'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tingkat</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$tingkat_trg.'</td>
      </tr>
      <br/>
      ';
  }
}
else{
  $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Jenis</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Lokasi</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Waktu</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Kedudukan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Tingkat</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <br/>
      ';
}

$html .= '
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="center"><span style="font-size: large;"><b>RIWAYAT PEKERJAAN/JABATAN</b></span></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table>
    ';
if(!empty($pkrj)){
  foreach ($pkrj as $anak) {

    $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama Jabatan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NAMA_JABATAN'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Intansi</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['INSTANSI'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;SK Jabatan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['NO_SK_JABATAN'].' '.$anak['TGL_SK_JABATAN'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;TMT SK Jabatan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['TGL_MULAI_JABATAN'].' - '.$anak['TMT_JABATAN'].'</td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Pejabat SK Jabatan</td>
          <td width="2%" align="center">:</td>
          <td width="72%">'.$anak['PJBT_SK'].'</td>
      </tr>
      <br/>
      ';
  }
}
else{
  $html .= '
      <tr>
          <td width="26%">&nbsp;&nbsp;Nama Jabatan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Intansi</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;SK Jabatan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;TMT SK Jabatan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <tr>
          <td width="26%">&nbsp;&nbsp;Pejabat SK Jabatan</td>
          <td width="2%" align="center">:</td>
          <td width="72%"></td>
      </tr>
      <br/>
      ';
}

$html .='
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="left"><i>Waktu cetak PDF '.$tgl_sekarang.' '.$bulan_sekarang.' '.$tahun_sekarang.' '.$jam_sekarang.'</i></td>
  </tr>
</table>
';


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('BIODATA-'.strtoupper($nip_dosen).'.pdf', 'I');