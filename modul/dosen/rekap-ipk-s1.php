<?php
include 'config.php';
include 'proses/rekapitulasi.class.php';

// Fikrie //
$id_pt = $id_pt_user;

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$rekap = new rekapitulasi($db, $user->ID_DOSEN, $id_pt);
$fakultas_set = $db->QueryToArray("select id_fakultas, nm_fakultas from fakultas where id_perguruan_tinggi = {$id_pt}");

foreach ($fakultas_set as &$f)
	$f['program_studi_set'] = $rekap->program_studi($f['ID_FAKULTAS'], 1, '');


$data_set =	$rekap->rekap_ipk(1);
foreach ($data_set as &$d)
{
	$d['TOTAL'] = $d['IP1'] + $d['IP2'] + $d['IP3'] + $d['IP4'] + $d['IP5'] + $d['IP6'];
	$d['PERSEN_IP1'] = round(($d['IP1'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_IP2'] = round(($d['IP2'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_IP3'] = round(($d['IP3'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_IP4'] = round(($d['IP4'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_IP5'] = round(($d['IP5'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_IP6'] = round(($d['IP6'] / $d['TOTAL']) * 100, 2);
}	
	
$smarty->assign('fakultas_set', $fakultas_set);
$smarty->assign('data_set', $data_set);

$smarty->display('sample/rekap/rekap-ipk-s1.tpl');
?>