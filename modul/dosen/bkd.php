<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$smarty->assign('TITLE', 'Beban Kerja Dosen');

//detail dosen
require_once 'proses/info_dosen.class.php';
$info_dosen = new info_dosen($db, $login->id_pengguna);
$data_info_dosen = $info_dosen->set();
$smarty->assign(array(
    'nama' => $data_info_dosen['NAMA'],
    'nip' => $data_info_dosen['NIP'],
    'fakultas' => $data_info_dosen['FAKULTAS'],
    'prodi' => $data_info_dosen['PRODI']
));
// tab BKD - Rekap Beban Kerja Dosen
require_once 'proses/bkd.class.php';
$bkd = new bkd($db);
$smarty->assign('isi_data', $bkd->cetak_bkd($login->id_dosen));

$smarty->display('sample/bkd/bkd.tpl');
?>