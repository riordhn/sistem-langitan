<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$mode = get('mode', 'view');

if ($request_method == 'GET' || $request_method == 'POST')
{
	if ($mode == 'view')
    {
        $id_jenjang = get('id_jenjang', '');
        
        $smarty->assign('id_jenjang', $id_jenjang);
        $smarty->assign('jenjang_set', $db->QueryToArray("SELECT * FROM JENJANG"));

        $filter_jenjang = "";
        if ($id_jenjang != '')
            $filter_jenjang = "WHERE PS.ID_JENJANG = {$id_jenjang}";

        $smarty->assign('program_studi_set', $db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI, J.NM_JENJANG || ' ' || PS.NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI, PS.KODE_PROGRAM_STUDI, F.NM_FAKULTAS,
                (SELECT COUNT(*) FROM MAHASISWA M WHERE M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI) AS JUMLAH_MAHASISWA,
                (SELECT COUNT(*) FROM BIAYA_KULIAH BK WHERE BK.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI) AS JUMLAH_VARIAN
            FROM PROGRAM_STUDI PS
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            {$filter_jenjang}
            ORDER BY PS.ID_FAKULTAS, PS.ID_JENJANG"));
    }
	
	if ($mode == 'prodi')
	{
		$id_program_studi = get('id_program_studi', '');
        
        $db->Query("
            SELECT PS.ID_PROGRAM_STUDI, J.NM_JENJANG, PS.ID_JENJANG, PS.NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI, PS.KODE_PROGRAM_STUDI, F.NM_FAKULTAS, PS.NO_SK,
                (SELECT COUNT(*) FROM MAHASISWA M WHERE M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI) AS JUMLAH_MAHASISWA
            FROM PROGRAM_STUDI PS
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            WHERE PS.ID_PROGRAM_STUDI = {$id_program_studi}");
        $row = $db->FetchAssoc();
        $smarty->assign('program_studi', $row);
        
        // detail varian biaya
        $smarty->assign('biaya_kuliah_set', $db->QueryToArray("
            SELECT BK.ID_BIAYA_KULIAH, S.NM_SEMESTER || ' ' || S.TAHUN_AJARAN AS NM_SEMESTER, J.NM_JALUR, KB.NM_KELOMPOK_BIAYA,
              BK.BESAR_BIAYA_KULIAH,
              (SELECT SUM(BESAR_BIAYA) FROM DETAIL_BIAYA DB WHERE DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH AND db.pendaftaran_biaya = 1) AS BIAYA_PENDAFTARAN,
              (SELECT SUM(BESAR_BIAYA) FROM DETAIL_BIAYA DB WHERE DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH AND db.pendaftaran_biaya = 0) AS BIAYA_SEMESTER
            FROM BIAYA_KULIAH BK
            LEFT JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
            LEFT JOIN JALUR J ON J.ID_JALUR = BK.ID_JALUR
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
            WHERE BK.ID_PROGRAM_STUDI = {$id_program_studi}
            ORDER BY S.TAHUN_AJARAN DESC, S.NM_SEMESTER, BK.ID_JALUR"));
	}
	
	if ($mode == 'detail')
	{
		$id_biaya_kuliah = get('id_biaya_kuliah', '');
		
		// Biaya Kuliah + Program Studi
		$db->Query("
			SELECT J1.NM_JENJANG || ' ' || PS.NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI, F.NM_FAKULTAS, J2.NM_JALUR, KB.NM_KELOMPOK_BIAYA, PS.ID_PROGRAM_STUDI,
				S.NM_SEMESTER || ' ' || S.TAHUN_AJARAN AS NM_SEMESTER, BK.KETERANGAN_BIAYA_KULIAH
			FROM BIAYA_KULIAH BK
			LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
			LEFT JOIN JENJANG J1 ON J1.ID_JENJANG = PS.ID_JENJANG
			LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
			LEFT JOIN JALUR J2 ON J2.ID_JALUR = BK.ID_JALUR
			LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
			LEFT JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
			WHERE BK.ID_BIAYA_KULIAH = {$id_biaya_kuliah}");
		$row = $db->FetchAssoc();
		$smarty->assign('biaya_kuliah', $row);
		
		// Detail biaya kuliah
		$smarty->assign('detail_biaya_set', $db->QueryToArray("
			SELECT B.NM_BIAYA, DB.BESAR_BIAYA, DB.PENDAFTARAN_BIAYA, DB.STATUS_BIAYA FROM DETAIL_BIAYA DB
			LEFT JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
			WHERE DB.ID_BIAYA_KULIAH = {$id_biaya_kuliah}"));
	}
}

$smarty->display("dirkeuangan/mbiaya/{$mode}.tpl");
?>