<?php
include 'config.php';
include 'proses/perwalian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$wali = new perwalian($db, $user->ID_PENGGUNA, $login->id_dosen);

if (isset($_POST)) {
    if (post('mode') == 'approve') {
        $id_mhs = post('id_mhs');
        for ($i = 1; $i <= post('jumlah_data'); $i++) {
            if (post('confirm' . $i) != '') {
                $wali->update(post('confirm' . $i), '1');
                $sks_apv += post('sks' . $i);
            } else {
                $wali->update(post('not_confirm' . $i), '0');
            }
        }
        // PEMBAYARAN SP
        // Besar Biaya 
		$semester_sp = $db->QuerySingle("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SP='True'");
        $biaya_sks = $db->QuerySingle("
                        SELECT
                            (SELECT BESAR_BIAYA_SP FROM AUCC.BIAYA_SP WHERE ID_JALUR=JM.ID_JALUR AND ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI AND ID_SEMESTER='{$semester_sp}') BIAYA
                        FROM AUCC.MAHASISWA M
                        JOIN AUCC.JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND ID_JALUR_AKTIF=1
                        WHERE M.ID_MHS={$id_mhs}
                    ");
        $besar_biaya_sp = $biaya_sks * $sks_apv;
        // Jika ada Yang di approve

        if ($sks_apv > 0) {
            $semester_sp = $db->QuerySingle("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SP='True'");
            $cek_pembayaran = $db->QuerySingle("SELECT COUNT(*) FROM PEMBAYARAN_SP WHERE ID_SEMESTER='{$semester_sp}' AND ID_MHS='{$id_mhs}'");
            // Jika Sudah Ada di Pembayaran
            if ($cek_pembayaran > 0) {
                // Jika Sudah Terbayar
                $sudah_bayar = $db->QuerySingle("SELECT COUNT(*) FROM PEMBAYARAN_SP WHERE ID_SEMESTER='{$semester_sp}' AND ID_MHS='{$id_mhs}' AND TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL");
                if ($sudah_bayar == 0) {
                    $db->Query("UPDATE PEMBAYARAN_SP SET JUMLAH_SKS='{$sks_apv}',BESAR_BIAYA='{$besar_biaya_sp}' WHERE ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester_sp}'");
                    $db->Query("UPDATE MAHASISWA SET JENIS_TAGIHAN='SPENDEK' WHERE ID_MHS='{$id_mhs}'");
                } else {
                    $db->Query("SELECT SUM(JUMLAH_SKS) JUMLAH_SKS FROM PEMBAYARAN_SP WHERE ID_SEMESTER='{$semester_sp}' AND ID_MHS='{$id_mhs}' AND TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL");
                    $data_pembayaran = $db->FetchAssoc();
                    $db->Query("SELECT SUM(JUMLAH_SKS) JUMLAH_SKS FROM PEMBAYARAN_SP WHERE ID_SEMESTER='{$semester_sp}' AND ID_MHS='{$id_mhs}' AND TGL_BAYAR IS NULL AND ID_BANK IS NULL");
                    $data_tagihan = $db->FetchAssoc();
                    // Jika sudah bayar namun ada mata kuliah sp yang masih belum terbayar
                    if ($data_pembayaran['JUMLAH_SKS'] < $sks_apv && ($data_tagihan['JUMLAH_SKS'] + $data_pembayaran['JUMLAH_SKS']) != $sks_apv) {
                        $sks_belum_terbayar = $sks_apv - $data_pembayaran['JUMLAH_SKS'];
                        $besar_biaya_sp = $biaya_sks * $sks_belum_terbayar;
                        $db->Query("
                        INSERT INTO PEMBAYARAN_SP
                            (ID_MHS,ID_SEMESTER,JUMLAH_SKS,BESAR_BIAYA,IS_TAGIH,ID_STATUS_PEMBAYARAN,KETERANGAN,IS_TARIK)
                        VALUES
                            ('{$id_mhs}','{$semester_sp}','{$sks_belum_terbayar}','{$besar_biaya_sp}','Y','2','SEMESTER PENDEK',0)
                        ");
                        $db->Query("UPDATE MAHASISWA SET JENIS_TAGIHAN='SPENDEK' WHERE ID_MHS='{$id_mhs}'");
                    }
                }
            }// Jika Belum ada
            else {
                $db->Query("
                    INSERT INTO PEMBAYARAN_SP
                        (ID_MHS,ID_SEMESTER,JUMLAH_SKS,BESAR_BIAYA,IS_TAGIH,ID_STATUS_PEMBAYARAN,KETERANGAN,IS_TARIK)
                    VALUES
                        ('{$id_mhs}','{$semester_sp}','{$sks_apv}','{$besar_biaya_sp}','Y','2','SEMESTER PENDEK',0)
                    ");
                $db->Query("UPDATE MAHASISWA SET JENIS_TAGIHAN='SPENDEK' WHERE ID_MHS='{$id_mhs}'");
            }
        }
    }
}


if (isset($_GET)) {
    if (get('mode') == 'load_sp') {
        $smarty->assign('data_mahasiswa', $wali->load_mhs_status(get('id_mhs')));
        $smarty->assign('data_transkrip', $wali->load_transkrip(get('id_mhs')));
        $smarty->assign('data_mk_sp', $wali->load_mata_kuliah_sp(get('id_mhs')));
    }
}

$smarty->assign('data_mahasiswa_sp', $wali->load_mahasiswa_sp());
$smarty->assign('id_fakultas', $login->id_fakultas);
$smarty->display('display-bimbingan/approve-sp.tpl');
?>
