<?php
include 'config.php';
include 'proses/publikasi.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$publikasi = new publikasi($db, $login->id_dosen);

$mode = get('mode','view');

if ($request_method == 'POST')
{
	if (post('mode') == 'add')
	{
		
		$publikasi->insert_publikasi(post('jenis'),post('judul'),post('penerbit'),post('pengarang'),post('media'),post('tahun'),post('peran'),post('edisi'),post('lembaga'),post('tingkat'));
	}
	else if(post('mode')=='edit')
	{	
		$publikasi->update_publikasi(post('id'),post('jenis'),post('judul'),post('penerbit'),post('pengarang'),post('media'),post('tahun'),post('peran'),post('edisi'),post('lembaga'),post('tingkat'));
	}
	else if(post('mode')=='delete')
	{
		$publikasi->delete_publikasi(post('id'));
	}

}
if($mode=='detail'||$mode=='edit'||$mode=='upload'||$mode=='delete'){
	$smarty->assign('data_publikasi',$publikasi->get_publikasi(get('id')));
}else{
	$smarty->assign('data_publikasi', $publikasi->load_publikasi());
}


$smarty->display("sample/biodata/{$mode}_publikasi.tpl");





// memanggil class publikasi 
?>