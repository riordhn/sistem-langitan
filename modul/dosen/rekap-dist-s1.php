<?php
include 'config.php';

// Fikrie //
$id_pt = $id_pt_user;

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

        $prodi = array();
        $nilai_A = array();
        $nilai_AB = array();
        $nilai_B = array();
        $nilai_BC = array();
        $nilai_C = array();
        $nilai_D = array();
        $nilai_E = array();


        $db->Query("select id_program_studi, nm_program_studi,
                round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_A,
                round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_AB,
                round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_B,
                round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_BC,
                round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_C,
                round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_D,
                round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_E
                from
                (select ps.id_program_studi, ps.nm_program_studi,
                case when nilai_huruf='A' then 1 else 0 end as A,
                case when nilai_huruf='AB' then 1 else 0 end as AB,
                case when nilai_huruf='B' then 1 else 0 end as B,
                case when nilai_huruf='BC' then 1 else 0 end as BC,
                case when nilai_huruf='C' then 1 else 0 end as C,
                case when nilai_huruf='D' then 1 else 0 end as D,
                case when (nilai_huruf='E' or nilai_huruf='T' or nilai_huruf is null) then 1 else 0 end as E
                from pengambilan_mk pmk
                left join mahasiswa mhs on pmk.id_mhs=mhs.id_mhs
                left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
                left join fakultas f on f.ID_FAKULTAS = ps.ID_FAKULTAS
                where PMK.id_semester=23 and ps.id_fakultas in (select id_fakultas from fakultas where id_perguruan_tinggi = {$id_pt}) and ps.id_jenjang= 1) group by id_program_studi, nm_program_studi order by id_program_studi");

        $i = 0;
        $jml_data = 0;
        while ($row = $db->FetchRow()){ 
                $prodi[$i] = $row[1];
                $nilai_A[$i] = $row[2];
                $nilai_AB[$i] = $row[3];
                $nilai_B[$i] = $row[4];
                $nilai_BC[$i] = $row[5];
                $nilai_C[$i] = $row[6];
                $nilai_D[$i] = $row[7];
                $nilai_E[$i] = $row[8];
                $i++;
                $jml_data++;
        }
        
        $smarty->assign('jml_data', $jml_data);
        $smarty->assign('prodi', $prodi);    
        $smarty->assign('nilai_A', $nilai_A);
        $smarty->assign('nilai_AB', $nilai_AB);
        $smarty->assign('nilai_B', $nilai_B);
        $smarty->assign('nilai_BC', $nilai_BC);
        $smarty->assign('nilai_C', $nilai_C);
        $smarty->assign('nilai_D', $nilai_D);
        $smarty->assign('nilai_E', $nilai_E);
        $smarty->display("sample/rekap/rekap-dist-s1.tpl");
?>
