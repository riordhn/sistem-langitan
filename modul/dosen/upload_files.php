<?php

//ini_set("display_errors", 1);

include 'config.php';

// security sementara terhadap no auth access ~sugenk.


//include 'function-tampil-informasi.php';
include 'proses/upload.class.php';


//echo "haha ";


$upload = new upload($db);
$is_debug = TRUE;

$id_jenis = get('id_jenis');
$id_pengguna = get('id_pengguna');
$id_riwayat = get('id_riwayat');

if ($is_debug) echo "\$id_riwayat:".$id_riwayat.'<br/>';

// Mendapatkan Jenis File
$db->Query("SELECT * FROM upload_jenis_file WHERE id_upload_jenis_file = {$id_jenis}");
$row = $db->FetchAssoc();
$jenis = $row['NM_JENIS'];

if ($is_debug) echo "\$jenis:".$jenis.'<br/>';

//$upload->ResetUpload($id_pengguna, $id_jenis);

$replace_jenis = strtolower(str_replace("/", "_", str_replace(" ", "_", $jenis)));

if ($is_debug) echo "\$replace_jenis:".$replace_jenis.'<br/>';

if (post('mode') == 'upload' && $user->ID_PENGGUNA != '')
{
	// Mengambil path /files/sumberdaya/[id_pengguna]/
	$db->Query("SELECT * FROM UPLOAD_FOLDER WHERE ID_UPLOAD_FOLDER='5655'");
	$direktori = $db->FetchAssoc();
	$new_folder = '../..' . $direktori['PATH_UPLOAD_FOLDER'] . '/' . $direktori['NAMA_UPLOAD_FOLDER'] . '/' . $id_pengguna;
	$id_folder = $upload->GetIdFolderFile($id_pengguna);

	if ($is_debug) echo "\$id_pengguna:".$id_pengguna.'<br/>';
	if ($is_debug) echo "\$id_folder:".$id_folder.'<br/>';
	if ($is_debug) echo "\$new_folder: ".$new_folder.'<br/>';
	if ($is_debug) echo "\$new_folder (realpath): ".realpath($new_folder).'<br/>';

	// Cek eksistensi folder
	if ( ! file_exists($new_folder)) {
		if ($is_debug) echo "masuk is_dir()<br/>";
		// Buat Folder Baru
		//echo "\$new_folder: $new_folder<br/>";

		$mkdir = mkdir($new_folder);
		$chmod = chmod($new_folder, 0777);

		if ($mkdir || $chmod)
		{
			if ($is_debug) echo "Creating and setting permission: ".realpath($new_folder)." -- OK <br/>";
		}
	}

	if (empty($id_folder)) {

		if ($is_debug) echo "masuk empty(id_folder)... ";
		// Input Folder Database
		$insert_upload_folder = $db->Query("
			INSERT INTO UPLOAD_FOLDER (
				ID_UPLOAD_SERVER,
				ID_PARENT_FOLDER,
				NAMA_UPLOAD_FOLDER,
				PATH_UPLOAD_FOLDER,
				KEDALAMAN,
				AKSES
			)
			VALUES (
				'2',
				'{$direktori['ID_UPLOAD_FOLDER']}',
				'{$id_pengguna}',
				'',
				'1',
				'1'
			)");
		if ($insert_upload_folder)
		{
			if ($is_debug) echo "insert into UPLOAD_FOLDER ... -- OK<br/>";
		}

		$id_folder = $upload->GetIdFolderFile($id_pengguna);
	}

	// exit();

	$allowed_exts = array("pdf", "jpeg", 'jpg');
	$allowed_type = array('application/pdf', 'image/jpeg', 'image/pjpeg');
	$error_upload = '';
	$success_upload = '';
	//Properties File
	$extension = end(explode(".", $_FILES["file"]["name"]));
	$type = $_FILES["file"]["type"];
	$name = $_FILES["file"]["name"];
	$tmp_name = $_FILES["file"]["tmp_name"];
	$size = $_FILES["file"]["size"];
	$error = $_FILES["file"]["error"];
	// Format Nama File

	$file_name = strtolower($replace_jenis . '_' . $id_riwayat . '.' . $extension);

	if ($is_debug) echo "\$file_name: $file_name<br/>";

	if ($id_riwayat != '' && $id_pengguna != '' && $id_jenis != '') {
		//Cek Extension dan Type
		if (in_array($extension, $allowed_exts) && in_array($type, $allowed_type)) {
			if ($error > 0) {
				$error_upload .= alert_error("Error file :" . $error);
			} else {
				// Simpan data fisik
				$move_uploaded = move_uploaded_file($tmp_name, "$new_folder/$file_name");

				echo "\$tmp_name: $tmp_name<br/>";

				if ($is_debug) echo "\$move_uploaded: $move_uploaded<br/>";

				// Menyimpan data file secara database
				$upload->InsertFileDatabase($id_folder, $id_pengguna, $file_name, $id_jenis, $jenis);
				$success_upload .= alert_success('File Berhasil di Upload, klik refresh data untuk melihat hasil',98);
			}
		} else {
			$error_upload .= alert_error("Format File {$name} tidak sesuai",98);
		}
	}

	$smarty->assign('success_upload', $success_upload);
	$smarty->assign('error_upload', $error_upload);
}

$smarty->assign('jenis', $jenis);
$smarty->assign('info_1', alert_success("Tipe Format File menggunakan PDF/JPG",98));
$smarty->display('display-plugins/upload_files.tpl');

