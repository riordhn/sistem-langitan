<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$smarty->assign('TITLE', 'Katalog Skipsi');

$smarty->display('sample/pustaka/katalog_skripsi.tpl');
?>