<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

	$id_pengguna = $user->ID_PENGGUNA;
	
	$id_status = array();
	$id_pengguna_status = array();
	$status = array();
	$tgl_status = array();
	$nm_pengguna_status = array();
	
	$like = array();
	$comment = array();
	
	$jml_status = 0;
	
	$db->Query("select sn.id_sn_status, sn.id_pengguna, sn.sn_status, sn.sn_status_tgl, p.nm_pengguna from sn_status sn
				left join pengguna p on p.id_pengguna = sn.id_pengguna
				where sn.id_sn_status = (select max(id_sn_status) from sn_status where id_pengguna = '$id_pengguna')
	");
	
	$i = 0;
	while ($row = $db->FetchRow()){ 
		$id_status[$i] = $row[0];
		$id_pengguna_status[$i] = $row[1];
		$status[$i] = $row[2];
		$tgl_status[$i] = $row[3];
		$nm_pengguna_status[$i] = $row[4];
		$jml_status++;
		$i++;
	}
	
	$smarty->assign('id_status', $id_status);
	$smarty->assign('id_pengguna_status', $id_pengguna_status);
	$smarty->assign('status', $status);
	$smarty->assign('tgl_status', $tgl_status);
	$smarty->assign('nm_pengguna_status', $nm_pengguna_status);
	$smarty->assign('jml_status', $jml_status);
	
	
	$smarty->assign('id_pengguna', $id_pengguna);
	$smarty->display('latest-update-status.tpl');
?>