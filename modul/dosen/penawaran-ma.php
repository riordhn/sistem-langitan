<?php
include 'config.php';
include 'proses/perwalian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$perwalian = new perwalian($db, $user->ID_PENGGUNA, $login->id_dosen);
if($_SERVER['REMOTE_ADDR']!='210.57.212.66'){
    die('halaman belum tersedia');
}
if (isset($_GET)) {
    if (get('mode') == 'peserta') {
        $smarty->assign('data_peserta_mk', $perwalian->load_peserta_mata_kuliah(get('id_kelas')));
    } else {
        $smarty->assign('data_usulan_mk', $perwalian->load_mata_kuliah_usulan($login->id_fakultas, get('prodi'), get('semester')));
    }
}
$smarty->assign('data_prodi', $perwalian->load_data_prodi($login->id_fakultas));
$smarty->assign('data_semester', $perwalian->load_semester());
$smarty->assign('id_fakultas', $login->id_fakultas);
$smarty->display('display-bimbingan/penawaran-ma.tpl');
?>