<?php
include 'config.php';
include 'proses/evaluasi.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$eval = new evaluasi($db, $login->id_dosen);
if (post('id_kelas_mk') != '') {
    $smarty->assign('data_eva_kul', $eval->load_data_hasil_ev_kul(post('id_kelas_mk')));
}
$smarty->display('sample/biodata/load-data-evakul.tpl');
?>
