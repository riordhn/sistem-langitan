<?php
include 'config.php';
include 'proses/presensi.class.php';
include 'proses/penilaian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$presensi = new presensi($db, $login->id_dosen);
$nilai = new penilaian($db, $user->ID_DOSEN);
if (isset($_POST)) {
    if (post('id_kelas_mk') != '' && post('id_presensi_kelas') == '') {
        $data_pertemuan = $presensi->load_pertemuan(post('id_kelas_mk'));
        $data_presensi_mahasiswa = $presensi->load_data_presensi_mahasiswa($data_pertemuan[0]['ID_PRESENSI_KELAS']);
        $data_detail_pertemuan = $presensi->get_detail_pertemuan($data_pertemuan[0]['ID_PRESENSI_KELAS']);
    } else if (post('id_kelas_mk') != '' && post('id_presensi_kelas') != '') {
        $data_pertemuan = $presensi->load_pertemuan(post('id_kelas_mk'));
        $data_presensi_mahasiswa = $presensi->load_data_presensi_mahasiswa(post('id_presensi_kelas'));
        $data_detail_pertemuan = $presensi->get_detail_pertemuan(post('id_presensi_kelas'));
    }
}
//echo "haha ".post('id_kelas_mk');
$smarty->assign('id_kelas_mk', post('id_kelas_mk'));
$smarty->assign('id_presensi_kelas', post('id_presensi_kelas')==""?$data_pertemuan[0]['ID_PRESENSI_KELAS']:post('id_presensi_kelas'));
$smarty->assign('data_kelas_mk', $nilai->load_kelas_mata_kuliah());
$smarty->assign('data_pertemuan', $data_pertemuan);
$smarty->assign('data_presensi_mahasiswa', $data_presensi_mahasiswa);
$smarty->assign('data_detail_pertemuan', $data_detail_pertemuan);
$smarty->display('display-presensi/absen_mahasiswa.tpl');
?>