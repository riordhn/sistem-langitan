<?php
include 'config.php';

// Fikrie //
$id_pt = $id_pt_user;

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$fakultas_set = $db->QueryToArray("select id_fakultas, nm_fakultas from fakultas where id_perguruan_tinggi = {$id_pt}");

foreach ($fakultas_set as &$f)
	$f['program_studi_set'] = $db->QueryToArray("select id_program_studi, id_jenjang, nm_program_studi from program_studi where id_fakultas = {$f['ID_FAKULTAS']} and id_jenjang = 5 and id_program_studi not in (197)");
	
$data_set = $db->QueryToArray("
	select ps.id_program_studi,
	  (select count(mi.id_mhs) from mahasiswa_ipk mi where mi.id_program_studi = ps.id_program_studi and (ipk <= 2)) ip1,
	  (select count(mi.id_mhs) from mahasiswa_ipk mi where mi.id_program_studi = ps.id_program_studi and (ipk > 2 and ipk <= 2.5)) ip2,
	  (select count(mi.id_mhs) from mahasiswa_ipk mi where mi.id_program_studi = ps.id_program_studi and (ipk > 2.5 and ipk <= 2.75)) ip3,
	  (select count(mi.id_mhs) from mahasiswa_ipk mi where mi.id_program_studi = ps.id_program_studi and (ipk > 2.75 and ipk <= 3)) ip4,
	  (select count(mi.id_mhs) from mahasiswa_ipk mi where mi.id_program_studi = ps.id_program_studi and (ipk > 3 and ipk <= 3.5)) ip5,
	  (select count(mi.id_mhs) from mahasiswa_ipk mi where mi.id_program_studi = ps.id_program_studi and (ipk > 3.5)) ip6
	from program_studi ps
	where ps.id_jenjang = 5");
	
foreach ($data_set as &$d)
{
	$d['TOTAL'] = $d['IP1'] + $d['IP2'] + $d['IP3'] + $d['IP4'] + $d['IP5'] + $d['IP6'];
	$d['PERSEN_IP1'] = round(($d['IP1'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_IP2'] = round(($d['IP2'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_IP3'] = round(($d['IP3'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_IP4'] = round(($d['IP4'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_IP5'] = round(($d['IP5'] / $d['TOTAL']) * 100, 2);
	$d['PERSEN_IP6'] = round(($d['IP6'] / $d['TOTAL']) * 100, 2);
}	
	
$smarty->assign('fakultas_set', $fakultas_set);
$smarty->assign('data_set', $data_set);

$smarty->display('sample/rekap/rekap-ipk-d3.tpl');
?>