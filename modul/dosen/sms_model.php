<?php
	
	/*
	Created By Sani Iman Pribadi
	*/
	
	class SmsModel{

		public $db;
		public $user = "";
		public $mobile_dosen = "";
		public $nama_dosen = "";

		public $nama_pma = array();
		public $mobile_pma = array();
		public $jml_pma = "";
		
		public $jml_perwalian = "";
		public $nama_perwalian = array();
		public $mobile_perwalian = array();
		
		
		function __construct(){
			$this->db = $db;
		}
		
		public function requestData($view){
			if($view=='peserta_ma'){
			}
			else if($view=='mobile-dosen'){
				$this->db = new MyOracle();
				$this->user = $_SESSION['user']['ID_PENGGUNA'];
				$this->db->Open();
				$this->db->Query("select d.mobile_dosen, p.nm_pengguna from pengguna p 
						left join dosen d on d.id_pengguna = p.id_pengguna
						where p.id_pengguna = '$this->user' 
				");

				while ($row = $this->db->FetchRow()){
					$this->mobile_dosen = $row[0];
					$this->nama_dosen = $row[1];
				}
				$this->db->Close();
			}
		}

		public function requestDataPma($id_kelas_mk){
				$this->db = new MyOracle();
				$this->user = $_SESSION['user']['ID_PENGGUNA'];
				$this->db->Open();
				$this->db->Query("            
						SELECT P.NM_PENGGUNA,M.ID_MHS,M.NIM_MHS,M.MOBILE_MHS FROM AUCC.PENGAMBILAN_MK PMK
						JOIN AUCC.MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
						JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
						WHERE PMK.ID_KELAS_MK='{$id_kelas_mk}' 
				");
				
				$this->jml_pma = 0;
				while ($row = $this->db->FetchRow()){
					$this->nama_pma[$this->jml_pma] = $row[0];
					$this->mobile_pma[$this->jml_pma] = $row[3];
					$this->jml_pma++;
				}
				$this->db->Close();
		}
		// edited by Yudi Sulistya, 07 Aug 2012
		// filter no hp mhs
		public function requestDataPerwalian(){
				$this->db = new MyOracle();
				$this->user = $_SESSION['user']['ID_DOSEN'];
				$this->db->Open();
				$this->db->Query("            
					SELECT M.ID_MHS, M.MOBILE_MHS, P.NM_PENGGUNA,M.NIM_MHS,M.THN_ANGKATAN_MHS,SP.NM_STATUS_PENGGUNA,COUNT(PMK.ID_MHS) STATUS_KRS FROM DOSEN_WALI DW
					JOIN MAHASISWA M ON DW.ID_MHS = M.ID_MHS
					LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
					JOIN PENGGUNA P ON P.ID_PENGGUNA =M.ID_PENGGUNA
					LEFT JOIN (
						SELECT DISTINCT(PMK.ID_MHS) FROM
						PENGAMBILAN_MK PMK
						JOIN SEMESTER S ON PMK.ID_SEMESTER= S.ID_SEMESTER
						WHERE S.STATUS_AKTIF_SEMESTER='True'
					) PMK ON PMK.ID_MHS = M.ID_MHS
					WHERE DW.ID_DOSEN='{$this->user}' AND SP.STATUS_AKTIF=1 AND DW.STATUS_DOSEN_WALI=1 AND
					M.MOBILE_MHS is not null and M.MOBILE_MHS not like '% %' and length(M.MOBILE_MHS) between 10 and 13
					GROUP BY M.ID_MHS, M.MOBILE_MHS, P.NM_PENGGUNA,M.NIM_MHS,M.THN_ANGKATAN_MHS,SP.NM_STATUS_PENGGUNA
					ORDER BY M.THN_ANGKATAN_MHS,M.NIM_MHS 
				");
				
				$this->jml_perwalian = 0;
				while ($row = $this->db->FetchRow()){
					$this->nama_perwalian[$this->jml_perwalian] = $row[2];
					$this->mobile_perwalian[$this->jml_perwalian] = $row[1];
					$this->jml_perwalian++;
				}
				$this->db->Close();
		}
	
	}