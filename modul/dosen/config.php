<?php
//include file class config dan class login
include '../../config.php';
include 'proses/login.class.php';
include 'function-tampil-informasi.php';
include 'RequestControllerClass.php';
ini_set('display_errors', 1);
// no auth security sementara ~sugenk.
if (!$user->IsLogged() && $user->Role() != AUCC_ROLE_DOSEN) {
    echo "<script>alert('Anda belum Login....'); window.location.href='../../logout.php'</script>";
    exit();
}
// end no auth security

if ($user->Role() == AUCC_ROLE_DOSEN) {
    /* TUTUP DARI NAMBI BIKIN ERROR
    // Bantuan Autocomplete netbeans
    if (1 == 2) {
        include 'User.class.php';
        $user = new User();
    }
    */

    /* -- DITUTUP KARENA KONEKSI GAGAL (FIKRIE)
    Koneksi MySQL blog mahasiswa
    $DBhost = "10.0.110.1";
    $DBuser = "lihat_blogmhs";
    $DBpass = "lihat_blogmhs";
    $DBName = "dbwebmhs";
    mysql_connect($DBhost, $DBuser, $DBpass);
    mysql_select_db("$DBName");
    */


    /** disable untuk langitan
    if (!empty($user->ID_PENGGUNA)) {
        if (str_replace(__DIR__, '', $_SERVER['SCRIPT_FILENAME']) != '/index.php') {
            if ($_SERVER['SERVER_NAME'] == 'cybercampus.unair.ac.id') {
                if (strpos($_SERVER['HTTP_REFERER'], $_SERVER['SERVER_NAME']) === false || $_SERVER['SERVER_NAME'] == '') {
                    //die('access denied');
                }
            }
        }
    }
     */

    // Penambahan Filter Untuk Lintas Fakultas
    $id_fakultas_lintas_pimpinan_fakultas = $db->QuerySingle("select id_fakultas from fakultas where id_dekan = '{$user->ID_PENGGUNA}' or id_wadek1 = '{$user->ID_PENGGUNA}' or id_wadek2 = '{$user->ID_PENGGUNA}' or id_wadek3 = '{$user->ID_PENGGUNA}'");
    $user->ID_FAKULTAS = $id_fakultas_lintas_pimpinan_fakultas == '' ? $user->ID_FAKULTAS : $id_fakultas_lintas_pimpinan_fakultas;


    $jumlah_menu_tampil = 0;

    //membuat object class
    $login = new login($db, $user->ID_PENGGUNA);
    $login->get_login();


    // lihat $pimpinan_set di rekap-pencarian.php, SAMA!!

    $id_pimpinan_set = array(1, 2, 3, 4, 5, 6, 7, 8, 11, 30, 31, 32, 33, 53, 81, 205, 206, 207, 208, 209, 210, 211, 212, 214, 215, 216, 217, 218);

    // Rektor, Warek, Dekan
    $id_pimpinan_set2 = array(1, 2, 3, 4, 30, 31, 32, 33, 53, 81, 175, 203, 205, 206, 207, 208, 209, 210, 211, 212, 214, 215, 216, 217, 218);

    // Rektor, warek, Dir SI, Dekan, Wadek 123
    $id_pimpinan_set3 = array(1, 2, 3, 4, 5, 6, 7, 8, 30, 53);

    //wadek 1
    $id_pimpinan_wadek1 = array(6, 603);

    // warek 3
    $id_jabatan_warek1 = 2;
    $id_jabatan_dsi = 30;
    $id_jabatan_ppmb = 175;

    // data dekan
    $dekan_set = array(
        '205' => '1',
        '206' => '2',
        '207' => '3',
        '208' => '4',
        '209' => '5',
        '210' => '6',
        '211' => '7',
        '212' => '8',
        '214' => '10',
        '215' => '11',
        '216' => '12',
        '217' => '13',
        '218' => '14'
    );

    foreach ($user->MODULs as &$modul) {
        // Rekapitulasi dan dashboard
        if (in_array($user->ID_JABATAN_PEGAWAI, $id_pimpinan_set2)) {
            if ($modul['NM_MODUL'] == 'rekap') {
                $modul['AKSES'] = 1;
            }
            if ($modul['NM_MODUL'] == 'dashboard') {
                $modul['AKSES'] = 1;
            }
        }
        // Tambahan Hitung Menu Tampil
        if ($modul['AKSES'] == 1) {
            $jumlah_menu_tampil++;
        }



        foreach ($modul['MENUs'] as &$menu) {
            // rekap penilaian
            if (in_array($user->ID_JABATAN_PEGAWAI, $id_pimpinan_set3)) {
                if ($menu['NM_MENU'] == 'rpenilaian' || $menu['NM_MENU'] == 'laporan_aka') {
                    $menu['AKSES'] = 1;
                }
            }

            // Buka Menu khusus Wadek 1
            if (in_array($user->ID_JABATAN_PEGAWAI, $id_pimpinan_wadek1)) {
                $wadek1_menu = array('apprv', 'bknilai', 'kkn', 'edt_nilai', 'grand_kon', 'grand_trans', 'grand_sks', 'laporan_aka', 'rekap_bimbingan');
                if (in_array($menu['NM_MENU'], $wadek1_menu)) {
                    $menu['AKSES'] = 1;
                }
            }

            if ($user->ID_JABATAN_PEGAWAI == $id_jabatan_warek1 || $user->ID_JABATAN_PEGAWAI == $id_jabatan_dsi || $user->ID_JABATAN_PEGAWAI == $id_jabatan_ppmb) {
                if ($modul['NM_MODUL'] == 'dashboard' && $menu['NM_MENU'] == 'rpm') {
                    $menu['AKSES'] = 1;
                }
                if ($modul['NM_MODUL'] == 'dashboard' && $menu['NM_MENU'] == 'rsnmptn') {
                    $menu['AKSES'] = 1;
                }
            }

            if ($user->IsKetuaDepartemen) {
                if ($modul['NM_MODUL'] == 'pen' && $menu['NM_MENU'] == 'approval') {
                    $menu['AKSES'] = 1;
                }
            }
        }
    }

    // mengaktifkan menu pimpinan
    if (in_array($user->ID_JABATAN_PEGAWAI, $id_pimpinan_set)) {
        for ($i = 0; $i < count($user->MODULs); $i++) {
            if ($user->MODULs[$i]['NM_MODUL'] == 'biodata') {
                for ($j = 0; $j < count($user->MODULs[$i]['MENUs']); $j++) {
                    // mengaktifkan rekapitulasi
                    if ($user->MODULs[$i]['MENUs'][$j]['NM_MENU'] == 'rekapitulasi')
                        $user->MODULs[$i]['MENUs'][$j]['AKSES'] = 1;
                }
            }

            // mengaktifkan menu dekan
            if (in_array($user->ID_JABATAN_PEGAWAI, array_keys($dekan_set))) {
                $user->ID_FAKULTAS_JABATAN = $dekan_set["{$user->ID_JABATAN_PEGAWAI}"];

                if ($user->MODULs[$i]['NM_MODUL'] == 'spekprodi') {
                    $user->MODULs[$i]['AKSES'] = 1;
                }
            }
        }
    }

    // Mengambil id semester aktif
    $db->Query("SELECT ID_SEMESTER,THN_AKADEMIK_SEMESTER,NM_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI = {$id_pt_user}");
    $row = $db->FetchAssoc();
    $id_semester = $row['ID_SEMESTER'];

    // Exetime
    function getTime()
    {
        $a = explode(' ', microtime());
        return (float) $a[0] + $a[1];
    }

    // Fungsi Pecah Kata

    function SplitWord($str, $length)
    {
        $split = explode(" ", strip_tags($str));
        for ($i = 0; $i < $length; $i++) {
            $result .= $split[$i] . ' ';
        }
        return $result;
    }

    function NumberToRomanRepresentation($number)
    {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if ($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }
} else {
    echo "<script>alert('Anda belum Login....');window.location.href='../../logout.php'</script>";
}
