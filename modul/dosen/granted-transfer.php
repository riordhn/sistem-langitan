<?php
include('conf.php');

$kd_fak= $user->ID_FAKULTAS;
$id_pengguna= $user->ID_PENGGUNA; 

$smarty->assign('T_THNSMT', $db->QueryToArray("select id_semester as id_semester, tahun_ajaran||' - '||nm_semester as thn_smt from semester where thn_akademik_semester in (
select distinct thn_akademik_semester from semester where thn_akademik_semester >EXTRACT(YEAR FROM sysdate)-9 )
and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc"));

$nim = $_REQUEST['nim'];
$smarty->assign('nim', $nim);


if(($_POST['action'])=='proses'){
	
	$ip = $_SERVER['REMOTE_ADDR'];
	$id_semester =$_POST['kdthnsmt'];
	$ttl_mk =$_POST['mk'];
	$catatan =$_POST['catatan'];
	
	if ($catatan=='' or $catatan==null)
	{
		echo '<script>alert("Proses Gagal, Catatan harus DI-ISI..")</script>';
	} else
	{
	
	//echo "insert into log_granted_transfer (id_pengguna,id_mhs,id_semester,status,total_mata_kuliah,ip_address,tanggal)
	//	  select '$id_pengguna',id_mhs,$id_semester,1,$ttl_mk,'$ip', sysdate	from mahasiswa where nim_mhs='$nim' and id_program_studi in
	//	  (select id_program_studi from program_studi where id_fakultas=$kd_fak)";
	$db->Query("insert into log_granted_transfer (id_pengguna,id_mhs,SEMESTER_PROSES,status,total_mata_kuliah,ip_address,tanggal,alasan)
		  select '$id_pengguna',id_mhs,$id_semester,1,$ttl_mk,'$ip', sysdate,'$catatan' from mahasiswa where nim_mhs='$nim' and id_program_studi in
		  (select id_program_studi from program_studi where id_fakultas=$kd_fak)");
	}
	
	
}


$smarty->assign('detail_transfer', $db->QueryToArray("
                		select nim_mhs,nm_pengguna,nm_jenjang||'-'||nm_program_studi as prodi,
				total_mata_kuliah,
				case when lgt.status=1 then 'PROSES' else 'SELESAI' end as status,
				coalesce(s1.ttlinsert,0) as  ttlinsert
				from log_granted_transfer lgt
				join mahasiswa m on lgt.id_mhs=m.id_mhs
				join pengguna p on m.id_pengguna=p.id_pengguna
				join program_studi ps on m.id_program_studi=ps.id_program_studi
				join jenjang j on ps.id_jenjang=j.id_jenjang
				left join 
				(select id_log_granted_transfer,id_semester,count(*) as ttlinsert from log_transfer_nilai
				where id_mhs in (select id_mhs from program_studi where id_fakultas=$kd_fak) 
        			group by id_log_granted_transfer, id_semester) s1 on lgt.id_log_granted_transfer=s1.id_log_granted_transfer
				where ps.id_fakultas=$kd_fak
				order by tanggal desc"));


$smarty->display("sample/penilaian/granted-transfer.tpl");
?>