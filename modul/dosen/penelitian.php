<?php
include 'config.php';
include 'proses/Penelitian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$mode = get('mode', 'view');

$Penelitian = new Penelitian($db);

if ($request_method == 'GET')
{
    if ($mode == 'view')
    {
    	/**
    	 * DIDISABLE DI UNU
		// Koneksi ke db Penelitian
		$db_amerta = new mysqli('10.0.110.12', 'penelitian', 'artikelunair', 'db_penelitian');
		if ($db_amerta->connect_error)
		{       echo "Gagal Konek Database";
			///echo 'Connect Error (' . $db_amerta->connect_errno . ') '. $db_amerta->connect_error;
			exit();
		}

		// mendapatkan id_dosen & otoakses
		$result = $db_amerta->query("
			select d.id_peg, otoakses from dt_unair d
			join otoakses on otoakses.id_peg = d.id_peg
			where d.nip = '{$user->USERNAME}'");
		$row = $result->fetch_assoc();
		$smarty->assign('dosen_amerta', $row);
		
		$result = $db_amerta->query("
			select id_dosen, judul, tahun from dt_unair
			join tb_artikel on tb_artikel.id_dosen = dt_unair.id_peg
			where nip = '{$user->USERNAME}' and tb_artikel.validasi = 1
			order by 3 desc");
			
		$penelitian_set = array();
		
		if ($result)
		{
			while ($row = $result->fetch_assoc())
				array_push($penelitian_set, $row);
			
			$result->free();
		}
		
		$db_amerta->close();

		*/
		
		for ($i = 0; $i < count($penelitian_set); $i++)
		{
			$penelitian_set[$i]['link'] = "<a class=\"disable-ajax\" target=\"_blank\" href=\"http://penelitian.unair.ac.id/artikel_dosen_" . implode(" ", array_slice(explode(" ",strip_tags($penelitian_set[$i]['judul'])),0,5))."_".$penelitian_set[$i]['id_dosen']."\">Open</a>";
		}
		
		$smarty->assign('penelitian_set', $penelitian_set);
    }
    
    if ($mode == 'preview')
    {
        $smarty->assign('p', $Penelitian->GetData(get('id_penelitian')));
        $smarty->assign('golongan_set', $Penelitian->GetListGolongan());
    }
}

$smarty->display("sample/penelitian/penelitian/{$mode}.tpl");
?>
