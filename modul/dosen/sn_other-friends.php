<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

	$id_program_studi = $user->ID_PROGRAM_STUDI;
	$cur_id_pengguna = $user->ID_PENGGUNA;
	
	$id_pengguna = array();
	$username = array();
	$nm_pengguna = array();
	$jml_teman = 0;
	
	$db->Query("select d.id_pengguna, p.username, p.nm_pengguna from dosen d
				left join pengguna p on p.id_pengguna = d.id_pengguna
				where d.id_program_studi = '$id_program_studi' and p.id_pengguna not in (select id_pengguna from sn_pertemanan) and p.id_pengguna not in (select id_teman from sn_pertemanan) ORDER BY dbms_random.value()
	");
	
	
	$i = 0;
	while ($row = $db->FetchRow()){ 
		$id_pengguna[$i] = $row[0];
		$username[$i] = $row[1];
		$nm_pengguna[$i] = $row[2];
		$jml_teman++;
		$i++;
	}

	$smarty->assign('cur_id_pengguna', $cur_id_pengguna);
	$smarty->assign('id_pengguna', $id_pengguna);	
	$smarty->assign('username', $username);
	$smarty->assign('nm_pengguna', $nm_pengguna);
	$smarty->assign('jml_teman', $jml_teman);
	
	$smarty->display('sn_other-friends.tpl');
?>