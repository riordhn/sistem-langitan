<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

	$is_alih_jenis = $_POST['alihjenis'];
	$id_program_studi = htmlspecialchars($_POST['id_program_studi']);
	$id_semester = htmlspecialchars($_POST['id_semester']);
	$prodi_ket_lulus = htmlspecialchars($_POST['prodi_ket_lulus']);                   
	$prodi_batas_umur = htmlspecialchars($_POST['prodi_batas_umur']);                      
	$prodi_kelamin = htmlspecialchars($_POST['prodi_kelamin']);                                    
	$prodi_tuna_warna = htmlspecialchars($_POST['prodi_tuna_warna']);                
	$prodi_tuna_rungu = htmlspecialchars($_POST['prodi_tuna_rungu']);                
	$prodi_tuna_fisik = htmlspecialchars($_POST['prodi_tuna_fisik']);                
	$prodi_artikulator = htmlspecialchars($_POST['prodi_artikulator']);               
	$prodi_cacat_tubuh = htmlspecialchars($_POST['prodi_cacat_tubuh']);                 
	$prodi_tinggi_badan = htmlspecialchars($_POST['prodi_tinggi_badan']);              
	$prodi_elpt = htmlspecialchars($_POST['prodi_elpt']); 
	$prodi_syarat_umum = htmlspecialchars($_POST['prodi_syarat_umum']);  	
	$prodi_syarat_khusus = htmlspecialchars($_POST['prodi_syarat_khusus']); 
	
	$prodi_syarat_umum2 = htmlspecialchars($_POST['prodi_syarat_umum2']);  	
	$prodi_syarat_khusus2 = htmlspecialchars($_POST['prodi_syarat_khusus2']); 
	
	if($is_alih_jenis==0){
		$db->query("update prodi_syarat set
					prodi_ket_lulus = '" . $prodi_ket_lulus . "',
					prodi_batas_umur = '" . $prodi_batas_umur . "',
					prodi_kelamin = '" . $prodi_kelamin . "',
					prodi_tuna_warna = '" . $prodi_tuna_warna . "',
					prodi_tuna_rungu = '" . $prodi_tuna_rungu . "',
					prodi_tuna_fisik = '" . $prodi_tuna_fisik . "',
					prodi_artikulator = '" . $prodi_artikulator . "',
					prodi_cacat_tubuh = '" . $prodi_cacat_tubuh . "',
					prodi_tinggi_badan = '" . $prodi_tinggi_badan . "',
					prodi_elpt = '" . $prodi_elpt . "',
					prodi_syarat_umum  = '" . $prodi_syarat_umum . "',
					prodi_syarat_khusus = '" . $prodi_syarat_khusus . "'
					where id_program_studi = '" . $id_program_studi . "' and id_semester = '" . $id_semester . "' and is_alih_jenis = 0
		");
	}
	else if($is_alih_jenis==1){
		$db->query("update prodi_syarat set
					prodi_ket_lulus = '" . $prodi_ket_lulus . "',
					prodi_batas_umur = '" . $prodi_batas_umur . "',
					prodi_kelamin = '" . $prodi_kelamin . "',
					prodi_tuna_warna = '" . $prodi_tuna_warna . "',
					prodi_tuna_rungu = '" . $prodi_tuna_rungu . "',
					prodi_tuna_fisik = '" . $prodi_tuna_fisik . "',
					prodi_artikulator = '" . $prodi_artikulator . "',
					prodi_cacat_tubuh = '" . $prodi_cacat_tubuh . "',
					prodi_tinggi_badan = '" . $prodi_tinggi_badan . "',
					prodi_elpt = '" . $prodi_elpt . "',
					prodi_syarat_umum  = '" . $prodi_syarat_umum2 . "',
					prodi_syarat_khusus = '" . $prodi_syarat_khusus2 . "'
					where id_program_studi = '" . $id_program_studi . "' and id_semester = '" . $id_semester . "' and is_alih_jenis = 1
		");
	}
	
	echo "Data berhasil diperbarui";
?>