<?php

include 'config.php';
include 'proses/pesan.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}
$id_pesan = get('id');
$message_detail = new pesan($db, $id_dosen, $user);

if (isset($_GET)) {
    if (get('mode') == 'no_reply') {
        $smarty->assign('trash', 1);
    } else {
        // SET STATUS TERBACA
        $db->Query("UPDATE PESAN SET BACA_PENERIMA='1' WHERE ID_PESAN='{$id_pesan}'");
        $smarty->assign('trash', 0);
    }
}

$data_message = $message_detail->load_pesan($id_pesan);


$smarty->assign('title', 'Message Detail');
$smarty->assign('data_message', $data_message);
$smarty->display('display-konsultasi/message_detail.tpl');
?>