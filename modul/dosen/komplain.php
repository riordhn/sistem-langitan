<?php

include 'config.php';
 

//$db->Query("UPDATE AUCC.PEGAWAI SET ID_UNIT_KERJA=35 WHERE ID_PENGGUNA=103879");

if (!empty($_GET)) {
    if (get('mode') == 'komplain') {
        $unit_kerja_all = $db->QueryToArray("SELECT * FROM UNIT_KERJA WHERE TYPE_UNIT_KERJA NOT IN ('PRODI') ORDER BY NM_UNIT_KERJA");
        $smarty->assign('data_unit_kerja', $unit_kerja_all);
        $smarty->assign('pelapor', $user->NM_PENGGUNA);
    } else if (get('mode') == 'jawaban') {
        if (!empty($_POST)) {
            if (post('mode') == 'penilaian') {
                $id_komplain = get('k');
                $rate = post('rate');
                $keterangan = post('keterangan_nilai');
                $q_u_komplain = "
                    UPDATE KOMPLAIN SET 
                        NILAI_TANGGAPAN='{$rate}',
                        KETERANGAN_TANGGAPAN='{$keterangan}',
                        STATUS_KOMPLAIN=4
                    WHERE ID_KOMPLAIN='{$id_komplain}'
                    ";
                $db->Query($q_u_komplain);
            }
        }
        $id_komplain = get('k');
        $q_data_komplain = "
            SELECT P.NM_PENGGUNA,
            (CASE
                WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.DOSEN)
                THEN 'Dosen'
                WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.PEGAWAI)
                THEN 'Tenaga Kependidikan'
                ELSE 'Mahasiswa'
            END  
            ) STATUS
            ,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA,K.*,TO_CHAR(K.WAKTU_KOMPLAIN,'DD-MM-YYYY HH24:MI:SS') WAKTU,KK.NM_KATEGORI
            FROM AUCC.KOMPLAIN K
            JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=K.ID_UNIT_TERTUJU
            LEFT JOIN AUCC.KOMPLAIN_KATEGORI KK ON K.ID_KOMPLAIN_KATEGORI=KK.ID_KOMPLAIN_KATEGORI
            JOIN AUCC.PENGGUNA P ON K.ID_PELAPOR=P.ID_PENGGUNA
            WHERE K.ID_KOMPLAIN='{$id_komplain}'
            ";
        $db->Query($q_data_komplain);
        $data_komplain = $db->FetchAssoc();
        $q_data_tanggapan = "
            SELECT
            KT.*,P.NM_PENGGUNA PENJAWAB,
            (CASE
                WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.DOSEN)
                THEN 'Dosen'
                WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.PEGAWAI)
                THEN 'Tenaga Kependidikan'
            END  
            ) STATUS_PENJAWAB,TO_CHAR(KT.WAKTU_JAWAB,'DD-MM-YYYY HH24:MI:SS') WAKTU_JAWAB
            FROM AUCC.KOMPLAIN K
            JOIN AUCC.KOMPLAIN_TANGGAPAN KT ON KT.ID_KOMPLAIN=K.ID_KOMPLAIN
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=KT.ID_PENJAWAB
            WHERE K.ID_KOMPLAIN='{$id_komplain}'
            ";
        $data_tanggapan = $db->QueryToArray($q_data_tanggapan);
        $smarty->assign('data_tanggapan', $data_tanggapan);
        $smarty->assign('komplain', $data_komplain);
    }
}

if (!empty($_POST)) {
    if (post('mode') == 'kirim') {
        /*
          $cek_kategori = $db->QuerySingle("SELECT COUNT(*) FROM KOMPLAIN_KATEGORI WHERE NM_KATEGORI='{$_POST['kategori']}' AND ID_UNIT_KERJA='{$_POST['unit_kerja']}'");
          if ($cek_kategori == 0) {
          $q_i_kategori = "
          INSERT INTO KOMPLAIN_KATEGORI
          (NM_KATEGORI,ID_UNIT_KERJA)
          VALUES
          ('{$_POST['kategori']}','{$_POST['unit_kerja']}')
          ";
          $db->Query($q_i_kategori);
          }
          $id_kategori = $db->QuerySingle("SELECT ID_KOMPLAIN_KATEGORI FROM KOMPLAIN_KATEGORI WHERE NM_KATEGORI='{$_POST['kategori']}' AND ID_UNIT_KERJA='{$_POST['unit_kerja']}'");
         */
        $q_i_komplain = "
            INSERT INTO KOMPLAIN
                (ID_PELAPOR,ID_UNIT_TERTUJU,ID_KOMPLAIN_VIA,ISI_KOMPLAIN,STATUS_KOMPLAIN)
            VALUES
                ('{$user->ID_PENGGUNA}','{$_POST['unit_kerja']}',1,'{$_POST['isi']}','1')
            ";
        $db->Query($q_i_komplain);
    }
}

$q_data_komplain = "
SELECT UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA,K.*,TO_CHAR(K.WAKTU_KOMPLAIN,'DD-MM-YYYY HH24:MI:SS') WAKTU
FROM AUCC.KOMPLAIN K
JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=K.ID_UNIT_TERTUJU
WHERE K.ID_PELAPOR='{$user->ID_PENGGUNA}'
ORDER BY WAKTU_KOMPLAIN DESC
";
$data_komplain = $db->QueryToArray($q_data_komplain);

$arr_data_komplain = array();
foreach ($data_komplain as $d) {
    $query_data_pembaca = "
        SELECT KB.*,P.NM_PENGGUNA,TO_CHAR(WAKTU_BACA,'DD-MM-YYYY HH24:MI:SS') WAKTU 
        FROM AUCC.KOMPLAIN_BACA KB
        JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=KB.ID_PEMBACA
        WHERE ID_KOMPLAIN='{$d['ID_KOMPLAIN']}'
        ";
    $data_pembaca = $db->QueryToArray($query_data_pembaca);
    array_push($arr_data_komplain, array_merge($d, array('DATA_PEMBACA' => $data_pembaca)));
}

$smarty->assign('data_komplain', $arr_data_komplain);
$smarty->display('display-komplain/komplain.tpl');
?>
