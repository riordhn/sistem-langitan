<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$smarty->assign('TITLE', 'Forum Pustaka');

$smarty->display('sample/pustaka/forum_pustaka.tpl');
?>