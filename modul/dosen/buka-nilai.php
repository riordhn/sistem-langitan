<?php
include 'config.php';
include 'proses/penilaian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$penilaian = new penilaian($db, $user->ID_DOSEN);
if ($request_method == 'POST') {
    if (post('mode') == 'load_kelas') {
        $smarty->assign('data_kelas_mk', $penilaian->load_kelas_pembukaan(post('id_mata_kuliah')));
    } else if (post('mode') == 'buka_kelas') {
        $penilaian->pembukaan_kelas(post('id_kelas_mk'));
        $smarty->assign('data_kelas_mk', $penilaian->load_kelas_pembukaan(post('id_mata_kuliah')));
    }else if(post('mode')=='buka_kelas_semester'){
        $penilaian->pembukaan_semester(post('id_semester'),$user->ID_FAKULTAS);
    }
}
$smarty->assign('data_semester_penilaian',$penilaian->load_semester_pembukaan_fakultas($user->ID_FAKULTAS));
$smarty->assign('data_mata_kuliah', $penilaian->load_mata_kuliah_wadek1($user->ID_FAKULTAS));
$smarty->display('sample/penilaian/buka-nilai.tpl');
?>
