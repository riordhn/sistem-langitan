<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }
	
	if(isset($_GET['id_prodi'])){
		$id_prodi = $_GET['id_prodi'];
		$id_dosen = array();
		$nm_dosen = array();
		$nm_matkul = array();
		$nm_jenjang = array();
		
		$db->Query("select d.id_dosen, p.nm_pengguna, mk.nm_mata_kuliah, mk.status_praktikum from pengampu_mk amk
					left join kelas_mk kmk on kmk.id_kelas_mk = amk.id_kelas_mk
					left join kurikulum_mk kurmk on kurmk.id_kurikulum_mk = kmk.id_kurikulum_mk
					left join mata_kuliah mk on mk.id_mata_kuliah = kurmk.id_mata_kuliah
					left join dosen d on d.id_dosen = amk.id_dosen
					left join pengguna p on p.id_pengguna = d.id_pengguna
					left join semester smt on smt.id_semester = kmk.id_semester
					left join program_studi ps on ps.id_program_studi = d.id_program_studi
					where smt.status_aktif_semester = 'True' and d.id_program_studi = '$id_prodi' and mk.status_praktikum = 0
		");
		
		$i = 0;
		$jml_dosen = 0;
		while ($row = $db->FetchRow()){ 
			$id_dosen[$i] = $row[0];
			$nm_dosen[$i] = $row[1];
			$nm_matkul[$i] = $row[2];
			$jml_dosen++;
			$i++;
		}
		
		$smarty->assign('jml_dosen', $jml_dosen);
		$smarty->assign('id_dosen', $id_dosen);
		$smarty->assign('nm_dosen', $nm_dosen);
		$smarty->assign('nm_matkul', $nm_matkul);
		$smarty->display('kepakaran-dosen-display.tpl');
	
	}
?>