<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

	$id_fakultas = array();
	$nm_fakultas = array();
	$jml_fakultas = 0;
	
	$id_prodi = array();
	$nm_prodi = array();
	$nm_jenjang = array();
	$jml_prodi = 0;
	
	$prodi_ket_lulus = array();
	$prodi_batas_umur = array();
	$prodi_kelamin = array();                       
	$prodi_tuna_warna = array();           
	$prodi_tuna_rungu = array();
	$prodi_tuna_fisik = array();
	$prodi_artikulator = array();
	$prodi_cacat_tubuh = array();
	$prodi_tinggi_badan = array();
	$prodi_elpt = array();
	$prodi_syarat_umum = array();
	$prodi_syarat_khusus = array();
	$is_alih_jenis = array();
	


	$nm_fakultas_get = "";

	$nm_program_studi = "";
	
	//$id_fakultas = $_GET['fakultas'];
		$db->Query("select id_fakultas, nm_fakultas from fakultas order by id_fakultas");

								$i = 0;
								while ($row = $db->FetchRow()){ 
									$id_fakultas[$i] = $row[0];
									$nm_fakultas[$i] = $row[1];
									$jml_fakultas++;
									$i++;
								}
		$get_id_fakultas = $user->ID_FAKULTAS_JABATAN;
		$db->Query("select ps.id_program_studi, 
					ps.nm_program_studi, 
					j.nm_jenjang,
					prorat.prodi_ket_lulus,
					prorat.prodi_batas_umur,
					prorat.prodi_kelamin,
					prorat.prodi_tuna_warna,
					prorat.prodi_tuna_rungu,
					prorat.prodi_tuna_fisik,
					prorat.prodi_artikulator,
					prorat.prodi_cacat_tubuh,
					prorat.prodi_tinggi_badan,
					prorat.prodi_elpt, 
					prorat.prodi_syarat_umum,
					prorat.prodi_syarat_khusus,
					prorat.is_alih_jenis
					from prodi_syarat prorat
					left join program_studi ps on ps.id_program_studi = prorat.id_program_studi
					left join fakultas f on f.id_fakultas = ps.id_fakultas
					left join jenjang j on j.id_jenjang = ps.id_jenjang
					where ps.id_fakultas = '$get_id_fakultas' 
					order by prorat.id_program_studi");

								$i = 0;
								while ($row = $db->FetchRow()){ 
									$id_prodi[$i] = $row[0];
									$nm_prodi[$i] = $row[1];
									$nm_jenjang[$i] = $row[2];
									$prodi_ket_lulus[$i] = $row[3];
									$prodi_batas_umur[$i] = $row[4];
									$prodi_kelamin[$i] = $row[5];                       
									$prodi_tuna_warna[$i] = $row[6];           
									$prodi_tuna_rungu[$i] = $row[7];
									$prodi_tuna_fisik[$i] = $row[8];
									$prodi_artikulator[$i] = $row[9];
									$prodi_cacat_tubuh[$i] = $row[10];
									$prodi_tinggi_badan[$i] = $row[11];
									$prodi_elpt[$i] = $row[12];
									$prodi_syarat_umum[$i] = $row[13];
									$prodi_syarat_khusus[$i] = $row[14];
									$is_alih_jenis[$i] = $row[15];
									$jml_prodi++;
									$i++;
								}
		
?>
	<style>
		#feedback { font-size: 1.2em; }
		#selectable .ui-selecting { background: #FECA40; }
		#selectable .ui-selected { background: #F39814; color: white; }
		#selectable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
		#selectable td { margin: 3px; padding: 0.4em; font-size: 1.2em; height: 14px; }
	</style>
 
	<script>
	$(function() {
		$( "#radio_jenis_kelamin" ).buttonset();
		$( "#radio_surat_kelulusan" ).buttonset();
		$( "#submit" ).buttonset();
		$( "#submit2" ).buttonset();
		$( "#btn_ubah" ).buttonset();
		$( "#btn_ubah2" ).buttonset();
		$("#btnPlus").button({
            icons: {
                primary: "ui-icon-circle-plus",
            },
			text: false
        });
	});
	</script>
	<script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script> 
	<script type="text/javascript">
		bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
	</script>
	

	
	<script>
	<?php
		$t = $_GET['prodi'];
	?>
		$(function() {
			$( "#slider_tuna_warna" ).slider({
				value:<?php echo $prodi_tuna_warna[$t]; ?>,
				min: 0,
				max: 100,
				step: 1,
				slide: function( event, ui ) {
					$( "#prodi_tuna_warna" ).val(ui.value);
				}
			});
			$( "#prodi_tuna_warna" ).val($( "#slider_tuna_warna" ).slider( "value" ));
		});
		
		$(function() {
			$( "#slider_tuna_rungu" ).slider({
				value:<?php echo $prodi_tuna_rungu[$t]; ?>,
				min: 0,
				max: 100,
				step: 1,
				slide: function( event, ui ) {
					$( "#prodi_tuna_rungu" ).val(ui.value);
				}
			});
			$( "#prodi_tuna_rungu" ).val($( "#slider_tuna_rungu" ).slider( "value" ));
		});
		
	</script>
	
<script>
		$('#btnPlus').click(function(){
			$('#myForm').show();
			$('#btnPlus').hide();
		});
</script>

<script>
	$(function() {
		$( "#tabs" ).tabs();
	});
</script>


		<style type="text/css">
			h4 { font-size: 18px; }
			input { padding: 3px; border: 1px solid #999; }
			td { padding: 5px; }
			#result { background-color: #ffffcc; border: 1px solid #ffff99; padding: 10px; width: 100%; margin-bottom: 20px; }
		</style>

<div class="center_title_bar">SYARAT KHUSUS PROGRAM STUDI</div> 

		<div id="loading" style="display:none;"><img src="loading.gif" alt="loading..." /></div>
		<div id="result" style="display:none; padding-top:10px; width:80%;"></div>

	<script>
		$(document).ready(function(){

				$().ajaxStart(function() {
					$('#loading').fadeIn();
					$('#result').hide();
				}).ajaxStop(function() {
					$('#loading').hide();
					$('#result').fadeIn('fast');
				});

				$('#myForm').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
							
							$('body').animate({scrollTop: $("body").offset().top},'slow');
							$('#result').fadeIn('fast');
							$('#result').html(data);
							
						}
					})
					return false;
				});
				
				$('#myForm2').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
							
							$('body').animate({scrollTop: $("body").offset().top},'slow');
							$('#result').fadeIn('fast');
							$('#result').html(data);
							
						}
					})
					return false;
				});
		});
	</script>

<div id="tabs" style="width:790px;">
	<ul>
		<li><a href="#tabs-1">NON ALIH JENIS</a></li>
		<li><a href="#tabs-2">ALIH JENIS</a></li>
	</ul>
	<div id="tabs-1">	
		<table class="ui-widget" width="90%" >
			<thead class="ui-widget-header">
				<tr class="ui-widget-header">
					<td class="header-coloumn">
						NO.
					</td>
					<td class="header-coloumn">
						Program Studi
					</td>
				</tr>
			</thead>
			
			<tbody >
			<?php
				for($i=0;$i<$jml_prodi;$i++){
					if($is_alih_jenis[$i]==0){
			?>
			<tr>
				<td class="ui-widget-content">
					<?php
						echo $i+1;
					?>
				</td>
				<td class="ui-widget-content">
					<a href="syarat_prodi.php?id=<?php echo $id_prodi[$i]; ?>&prodi=<?php echo $i; ?>">
					<?php
						echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i];
					?>
					</a>
				</td>
			</tr>
			<?php
					}
				}
			?>
			</tbody>
		</table>

		<?php
			if($_GET['id']){
			$i = $_GET['prodi'];
			
			$id_prodi_syarat = 0;
			$id_prodi_syarat_detail = array();
			$syarat_prodi = array();
			$jml_syarat_prodi = 0;
			
				$db->Query("select id_prodi_syarat from prodi_syarat
							where id_program_studi = '" . $id_prodi[$i] . "' and id_semester = 5
				");

				while ($row = $db->FetchRow()){ 
					$id_prodi_syarat = $row[0];
				}
			
				$db->Query("select prosd.id_prodi_syarat_detail, prosd.detail_syarat from prodi_syarat_detail prosd
							left join prodi_syarat pros on pros.id_prodi_syarat = prosd.id_prodi_syarat
							where pros.id_program_studi = '" . $id_prodi[$i] . "'
				");

				$t = 0;
				while ($row = $db->FetchRow()){ 
					$id_prodi_syarat_detail[$t] = $row[0];
					$syarat_prodi[$t] = $row[1];
					$jml_syarat_prodi++;
					$t++;
				}
				
		?>

			<div class="center_title_bar">
						<?php
							echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i];
						?>
			</div>
			
		<form id="myForm"  name="myForm" method="post" action="syarat_prodi_insert.php"> 	
		<table class="ui-widget" width="90%" >

			
			<tbody >
						<tr class="ui-widget-content">
							<td><strong>Persyaratan Pendaftaran Calon Mahasiswa Baru</strong></td>
							<td>
								<div style="width:300px;" >
									<input style="display:none;" type="text" name="id_program_studi" id="id_program_studi" value="<?php if(isset($_GET['id'])){echo $_GET['id'];} ?>" >
									<input style="display:none;" type="text" name="id_semester" id="id_semester" value="5" >
									<input style="display:none;" type="text" name="alihjenis" id="alihjenis" value="0" >
								</div>
							</td>
							<td></td>
						</tr>
						<!--
						<tr class="ui-widget-content">
							<td>Ijazah/Surat Keterangan Lulus</td>
							<td>
							<div id="radio_surat_kelulusan">
								<input type="radio" id="radio_ket_lulus" name="prodi_ket_lulus" value="1" checked="checked" /><label for="radio_ket_lulus">Ket. Lulus</label>
								<input type="radio" id="radio_ijazah" name="prodi_ket_lulus" value="2"  /><label for="radio_ijazah">Ijazah</label>
							</div>
							</td>
							<td><label style="width:100%;">UNAIR tidak menerima pendaftaran calon mahasiswa yang belum memiliki ijazah atau belum dinyatakan lulus dalam yudisium</label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Batas Umur</td>
							<td><input type="text" id="prodi_batas_umur" name="prodi_batas_umur" value="<?php echo $prodi_batas_umur[$i]; ?>" style="width:100%;" ></td>
							<td><label style="width:100%;" ></label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Jenis Kelamin</td>
							<td>
							<div id="radio_jenis_kelamin">
								<input type="radio" id="radio_kelamin_L" name="prodi_kelamin" value="1" /><label for="radio_kelamin_L">Laki-laki</label>
								<input type="radio" id="radio_kelamin_P" name="prodi_kelamin" value="2" checked="checked" /><label for="radio_kelamin_P">Perempuan</label>
							</div>
							</td>
							<td><label style="width:100%;" NAME="PPCMB_3_KET" ></label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Tes Ketunaan</td>
							<td><input type="text" disabled="disabled" id="PPCMB_4" value="Dulu disebut sebagai Tes Kesehatan" style="width:100%;" ></td>
							<td><label  style="width:100%;">Bagi mahasiswa baru Prodi yang tidak mensyaratkan ketunaan, seharusnya tdak perlu mengikuti Tes Ketunaan, agar efektif & efisien</label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Penglihatan</td>
							<td>
								<p>
									<label>Tuna Warna Total 100% : </label>
									<input type="text" name="prodi_tuna_warna" id="prodi_tuna_warna" value="<?php echo $prodi_tuna_warna[$i]; ?>" style="border:0; color:#f6931f;  width:20px;  font-weight:bold;" />%
								</p>
								<div title="Geser untuk merubah nilai" id="slider_tuna_warna"></div>
								<br />
							</td>
							<td><label style="width:100%;" >Harap dibuatkan naskah ilmiahnya sehigga Prodi mensyaratkannya</label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Pendengaran</td>
							<td>
								<p>
									<label>Tuna Rungu Total 100% : </label>
									<input type="text" name="prodi_tuna_rungu" id="prodi_tuna_rungu" value="<?php echo $prodi_tuna_rungu[$i]; ?>" style="border:0; color:#f6931f; width:20px; font-weight:bold;" />%
								</p>
								<div title="Geser untuk merubah nilai" id="slider_tuna_rungu"></div>
								<br />
							</td>
							<td><label style="width:100%;">Kalau ada, misalnya Prodi Pendidikan Dokter harap dibuatkan naskah ilmiahnya sehingga prodi mensyaratkannya</TEXTAREA></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Ketunaan fisik/anggota badan</td>
							<td><input type="text" name="prodi_tuna_fisik" id="prodi_tuna_fisik" value="<?php echo $prodi_tuna_fisik[$i]; ?>" style="width:100%;" ></td>
							<td><label style="width:100%;" ></label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Kelainan ucapan/artikulator</td>
							<td><input type="text" name="prodi_artikulator" id="prodi_artikulator" value="<?php echo $prodi_artikulator[$i]; ?>" style="width:100%;" ></td>
							<td><label style="width:100%;">Selama ini ada untuk Prodi Sastra Inggris</label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Cacat Tubuh Lainnya</td>
							<td><input type="text" name="prodi_cacat_tubuh" id="prodi_cacat_tubuh" value="<?php echo $prodi_cacat_tubuh[$i]; ?>" style="width:100%;" ></td>
							<td><label style="width:100%;" ></label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Tinggi Badan</td>
							<td><input type="text" id="prodi_tinggi_badan" name="prodi_tinggi_badan" value="<?php echo $prodi_tinggi_badan[$i]; ?>" style="width:100%;" ></td>
							<td><label style="width:100%;" >Selama ini ada untuk Prodi Keperawatan dan Kebidanan</label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Kemampuan Bahasa Inggris/ELPT</td>
							<td><input type="text" id="prodi_elpt" name="prodi_elpt" value="<?php echo $prodi_elpt[$i]; ?>" style="width:100%;" ></td>
							<td><label style="width:100%;" >Sejak awal pendaftaran diinformasikan bahwa ada persyaratan kemampuan bahasa Inggris, yaitu ELPT harus > 450, sebelum yudisium sarjana</label></td>
						</tr>
						-->
						<tr class="ui-widget-content">
							<td>Syarat Umum</td>
							<td colspan="2"><textarea id="prodi_syarat_umum" name="prodi_syarat_umum" style="width:90%;" COLS=30 ROWS=6 ><?php echo $prodi_syarat_umum[$i]; ?></textarea></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Syarat Khusus</td>
							<td colspan="2"><textarea id="prodi_syarat_khusus" name="prodi_syarat_khusus" style="width:90%;" COLS=30 ROWS=6 ><?php echo $prodi_syarat_khusus[$i]; ?></textarea></td>
						</tr>
						<tr class="ui-widget-content">
							<td>&nbsp;</td>
							<td style="text-align:center;">
								<div id="submit" name="submit">
									<input type="submit" name="btn_submit" id="btn_submit" value="Submit"  />
								</div>
							</td>
							<td>&nbsp;</td>
						</tr>
			</tbody>
		</table>
		</form>
<?php
		}
?>
	</div>
	
	<div id="tabs-2">
		<table class="ui-widget" width="90%" >
			<thead class="ui-widget-header">
				<tr class="ui-widget-header">
					<td class="header-coloumn">
						NO.
					</td>
					<td class="header-coloumn">
						Program Studi
					</td>
				</tr>
			</thead>
			
			<tbody >
			<?php
				for($i=0;$i<$jml_prodi;$i++){
					if($is_alih_jenis[$i]==1){
			?>
			<tr>
				<td class="ui-widget-content">
					<?php
						echo $i+1;
					?>
				</td>
				<td class="ui-widget-content">
					<a href="syarat_prodi.php?id=<?php echo $id_prodi[$i]; ?>&prodi=<?php echo $i; ?>&alihjenis=1">
					<?php
						echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i];
					?>
					</a>
				</td>
			</tr>
			<?php
					}
				}
			?>
			</tbody>
		</table>

		<?php
			if($_GET['id']){
			$i = $_GET['prodi'];
			
			$id_prodi_syarat = 0;
			$id_prodi_syarat_detail = array();
			$syarat_prodi = array();
			$jml_syarat_prodi = 0;
			
				$db->Query("select id_prodi_syarat from prodi_syarat
							where id_program_studi = '" . $id_prodi[$i] . "' and id_semester = 5
				");

				while ($row = $db->FetchRow()){ 
					$id_prodi_syarat = $row[0];
				}
			
				$db->Query("select prosd.id_prodi_syarat_detail, prosd.detail_syarat from prodi_syarat_detail prosd
							left join prodi_syarat pros on pros.id_prodi_syarat = prosd.id_prodi_syarat
							where pros.id_program_studi = '" . $id_prodi[$i] . "'
				");

				$t = 0;
				while ($row = $db->FetchRow()){ 
					$id_prodi_syarat_detail[$t] = $row[0];
					$syarat_prodi[$t] = $row[1];
					$jml_syarat_prodi++;
					$t++;
				}
				
		?>

			<div class="center_title_bar">
						<?php
							echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i] . ' ';
							if($_GET['alihjenis']==1){
								echo 'ALIH JENIS';
							}
						?>
			</div>
			
		<form id="myForm2"  name="myForm" method="post" action="syarat_prodi_insert.php"> 	
		<table class="ui-widget" width="90%" >

			
			<tbody >
						<tr class="ui-widget-content">
							<td><strong>Persyaratan Pendaftaran Calon Mahasiswa Baru</strong></td>
							<td>
								<div style="width:300px;" >
									<input style="display:none;" type="text" name="id_program_studi" id="id_program_studi" value="<?php if(isset($_GET['id'])){echo $_GET['id'];} ?>" >
									<input style="display:none;" type="text" name="id_semester" id="id_semester" value="5" >
									<input style="display:none;" type="text" name="alihjenis" id="alihjenis" value="1" >
								</div>
							</td>
							<td></td>
						</tr>
						<!--
						<tr class="ui-widget-content">
							<td>Ijazah/Surat Keterangan Lulus</td>
							<td>
							<div id="radio_surat_kelulusan">
								<input type="radio" id="radio_ket_lulus" name="prodi_ket_lulus" value="1" checked="checked" /><label for="radio_ket_lulus">Ket. Lulus</label>
								<input type="radio" id="radio_ijazah" name="prodi_ket_lulus" value="2"  /><label for="radio_ijazah">Ijazah</label>
							</div>
							</td>
							<td><label style="width:100%;">UNAIR tidak menerima pendaftaran calon mahasiswa yang belum memiliki ijazah atau belum dinyatakan lulus dalam yudisium</label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Batas Umur</td>
							<td><input type="text" id="prodi_batas_umur" name="prodi_batas_umur" value="<?php echo $prodi_batas_umur[$i]; ?>" style="width:100%;" ></td>
							<td><label style="width:100%;" ></label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Jenis Kelamin</td>
							<td>
							<div id="radio_jenis_kelamin">
								<input type="radio" id="radio_kelamin_L" name="prodi_kelamin" value="1" /><label for="radio_kelamin_L">Laki-laki</label>
								<input type="radio" id="radio_kelamin_P" name="prodi_kelamin" value="2" checked="checked" /><label for="radio_kelamin_P">Perempuan</label>
							</div>
							</td>
							<td><label style="width:100%;" NAME="PPCMB_3_KET" ></label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Tes Ketunaan</td>
							<td><input type="text" disabled="disabled" id="PPCMB_4" value="Dulu disebut sebagai Tes Kesehatan" style="width:100%;" ></td>
							<td><label  style="width:100%;">Bagi mahasiswa baru Prodi yang tidak mensyaratkan ketunaan, seharusnya tdak perlu mengikuti Tes Ketunaan, agar efektif & efisien</label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Penglihatan</td>
							<td>
								<p>
									<label>Tuna Warna Total 100% : </label>
									<input type="text" name="prodi_tuna_warna" id="prodi_tuna_warna" value="<?php echo $prodi_tuna_warna[$i]; ?>" style="border:0; color:#f6931f;  width:20px;  font-weight:bold;" />%
								</p>
								<div title="Geser untuk merubah nilai" id="slider_tuna_warna"></div>
								<br />
							</td>
							<td><label style="width:100%;" >Harap dibuatkan naskah ilmiahnya sehigga Prodi mensyaratkannya</label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Pendengaran</td>
							<td>
								<p>
									<label>Tuna Rungu Total 100% : </label>
									<input type="text" name="prodi_tuna_rungu" id="prodi_tuna_rungu" value="<?php echo $prodi_tuna_rungu[$i]; ?>" style="border:0; color:#f6931f; width:20px; font-weight:bold;" />%
								</p>
								<div title="Geser untuk merubah nilai" id="slider_tuna_rungu"></div>
								<br />
							</td>
							<td><label style="width:100%;">Kalau ada, misalnya Prodi Pendidikan Dokter harap dibuatkan naskah ilmiahnya sehingga prodi mensyaratkannya</TEXTAREA></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Ketunaan fisik/anggota badan</td>
							<td><input type="text" name="prodi_tuna_fisik" id="prodi_tuna_fisik" value="<?php echo $prodi_tuna_fisik[$i]; ?>" style="width:100%;" ></td>
							<td><label style="width:100%;" ></label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Kelainan ucapan/artikulator</td>
							<td><input type="text" name="prodi_artikulator" id="prodi_artikulator" value="<?php echo $prodi_artikulator[$i]; ?>" style="width:100%;" ></td>
							<td><label style="width:100%;">Selama ini ada untuk Prodi Sastra Inggris</label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Cacat Tubuh Lainnya</td>
							<td><input type="text" name="prodi_cacat_tubuh" id="prodi_cacat_tubuh" value="<?php echo $prodi_cacat_tubuh[$i]; ?>" style="width:100%;" ></td>
							<td><label style="width:100%;" ></label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Tinggi Badan</td>
							<td><input type="text" id="prodi_tinggi_badan" name="prodi_tinggi_badan" value="<?php echo $prodi_tinggi_badan[$i]; ?>" style="width:100%;" ></td>
							<td><label style="width:100%;" >Selama ini ada untuk Prodi Keperawatan dan Kebidanan</label></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Kemampuan Bahasa Inggris/ELPT</td>
							<td><input type="text" id="prodi_elpt" name="prodi_elpt" value="<?php echo $prodi_elpt[$i]; ?>" style="width:100%;" ></td>
							<td><label style="width:100%;" >Sejak awal pendaftaran diinformasikan bahwa ada persyaratan kemampuan bahasa Inggris, yaitu ELPT harus > 450, sebelum yudisium sarjana</label></td>
						</tr>
						-->
						<tr class="ui-widget-content">
							<td>Syarat Umum</td>
							<td colspan="2"><textarea id="prodi_syarat_umum2" name="prodi_syarat_umum2" style="width:90%;" COLS=30 ROWS=6 ><?php echo $prodi_syarat_umum[$i]; ?></textarea></td>
						</tr>
						<tr class="ui-widget-content">
							<td>Syarat Khusus</td>
							<td colspan="2"><textarea id="prodi_syarat_khusus2" name="prodi_syarat_khusus2" style="width:90%;" COLS=30 ROWS=6 ><?php echo $prodi_syarat_khusus[$i]; ?></textarea></td>
						</tr>
						<tr class="ui-widget-content">
							<td>&nbsp;</td>
							<td style="text-align:center;">
								<div id="submit2" name="submit2">
									<input type="submit" name="btn_submit2" id="btn_submit2" value="Submit"  />
								</div>
							</td>
							<td>&nbsp;</td>
						</tr>
			</tbody>
		</table>
		</form>
<?php
		}
?>
	</div>

</div>


<?php
?>