<?php
include 'config.php';
include 'proses/pesan.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$sent_item = new pesan($db, $login->id_dosen, $login->id_pengguna);

if (isset($_POST['mode'])) {
    if ($_POST['mode'] == 'delete') {
        $sent_item->delete_pesan_pengirim($_POST['id']);
    } else if ($_POST['mode'] == 'delete_checked') {
        foreach ($_POST['id'] as $item) {
            $sent_item->delete_pesan_pengirim($item[0]);
        }
    }
}

$data_sent_item = $sent_item->load_sent_item();
$smarty->assign('title', 'Sent Item');
$smarty->assign('data_sent_item', $data_sent_item);
$smarty->display('display-konsultasi/sent_item.tpl');
?>