<?php
/*
lukman
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

if (isset($_GET['nim'])) {
$nim = $_GET['nim'];
$mode = 1;
$smarty->assign('MOD', $mode);
		
$biomhs=getData("select mhs.nim_mhs, upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs
				from mahasiswa mhs 
				left join pengguna p on mhs.id_pengguna=p.id_pengguna 
				left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
				left join jenjang j on ps.id_jenjang=j.id_jenjang
				where mhs.nim_mhs='".$nim."' and mhs.id_mhs in (select id_mhs from dosen_wali where id_dosen='".$user->ID_DOSEN."' and id_semester='1')");
$smarty->assign('T_MHS', $biomhs);
if(!$biomhs)die('Bukan anak wali anda');
if($mode == 1){
// versi lukman, untuk mengatasi bug mata kuliah yg sudah diulang tetap muncul semua gara2 kuliah yg diulang kodenya berbeda
$jaf=getData("
select
a.*,
b.thn_akademik_semester||'-'||b.nm_semester||' Nilai:'||b.nilai p1,
c.thn_akademik_semester||'-'||c.nm_semester||' Nilai:'||c.nilai p2,
d.thn_akademik_semester||'-'||d.nm_semester||' Nilai:'||d.nilai p3,
e.thn_akademik_semester||'-'||e.nm_semester||' Nilai:'||e.nilai p4
from(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p,d.id_status_mk status,a.status_hapus
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where  m.nim_mhs='{$nim}' and rangking=1) a
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where  m.nim_mhs='{$nim}' and p=1
) b on a.nama=b.nama
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where  m.nim_mhs='{$nim}' and p=2
) c on a.nama=c.nama
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where  m.nim_mhs='{$nim}' and p=3
) d on a.nama=d.nama
left join
(
select thn_akademik_semester,nm_semester, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot,d.tingkat_semester smtr,a.p
 from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking,
row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester,sm.nm_semester) p,
thn_akademik_semester,nm_semester
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where  m.nim_mhs='{$nim}' and p=4
) e on a.nama=e.nama

order by a.smtr
");

$smarty->assign('T_MK', $jaf);

$ipk=getData("select sum(sks) as sks_total, sum((bobot*sks)) as bobot_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
from 
(

select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='{$nim}'
) uu
");
// print_r($jaf);exit;
$smarty->assign('T_IPK', $ipk);
} else{
}
}

$smarty->display('rekap_nilai.tpl');  
?>