<?php
include 'config.php';
include 'proses/report_mahasiswa.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$rm = new report_mahasiswa($db);

if (isset($_GET['x'])) {
    $smarty->assign('data_mahasiswa', $rm->get_hasil_cari_mahasiswa(get('x')));
} else if (isset($_GET['detail'])) {
    $smarty->assign('biodata_mahasiswa', $rm->get_biodata_mahasiswa(get('y')));
    $smarty->assign('detail_pembayaran_mahasiswa', $rm->get_detail_pembayaran_mahasiswa(get('y')));
    $smarty->assign('detail_status_mahasiswa', $rm->get_detail_akademik_status(get('y')));
    $smarty->assign('detail_mata_kuliah_mahasiswa', $rm->get_detail_akademik_mata_kuliah(get('y')));
}
$smarty->display("sample/pimpinan/cari-mahasiswa.tpl");
?>
