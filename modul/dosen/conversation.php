<?php
include 'config.php';
include 'proses/pesan.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$conversation = new pesan($db, $login->id_dosen, $login->id_pengguna);
$data_conversation_dosen = array();
$data_conversation_mahasiswa = array();
$data_conversation_ortu = array();
if (isset($_POST['mode'])) {
    if ($_POST['mode'] == 'delete_checked') {
        foreach ($_POST['id'] as $item) {
            if ($item[1] == 'terima') {
                $conversation->delete_pesan_penerima($item['0']);
            } else {
                $conversation->delete_pesan_pengirim($item['0']);
            }
        }
    } else if ($_POST['mode'] == 'delete') {
        if ($_POST['status'] == 'terima') {
            $conversation->delete_pesan_penerima($_POST['id']);
        } else {
            $conversation->delete_pesan_pengirim($_POST['id']);
        }
    } else if ($_POST['mode'] == 'save') {
        
    }
}
foreach ($conversation->load_conversation_dosen() as $temp) {
    array_push($data_conversation_dosen, array(
        'id_pesan' => $temp['id_pesan'],
        'id_balasan' => $temp['id_balasan'],
        'id_penerima' => $temp['id_penerima'],
        'id_pengirim' => $temp['id_pengirim'],
        'nama_pengirim' => $temp['nama_pengirim'],
        'nama_penerima' => $temp['nama_penerima'],
        'judul' => $temp['judul'],
        'isi' => $temp['isi'],
        'waktu' => $temp['waktu'],
        'hapus_pengirim' => $temp['hapus_pengirim'],
        'hapus_penerima' => $temp['hapus_penerima'],
        'pesan_balasan' => $conversation->load_balasan($temp['id_pesan'])
    ));
}
foreach ($conversation->load_conversation_mahasiswa() as $temp) {
    array_push($data_conversation_mahasiswa, array(
        'id_pesan' => $temp['id_pesan'],
        'id_balasan' => $temp['id_balasan'],
        'id_penerima' => $temp['id_penerima'],
        'id_pengirim' => $temp['id_pengirim'],
        'nama_pengirim' => $temp['nama_pengirim'],
        'nama_penerima' => $temp['nama_penerima'],
        'judul' => $temp['judul'],
        'isi' => $temp['isi'],
        'waktu' => $temp['waktu'],
        'hapus_pengirim' => $temp['hapus_pengirim'],
        'hapus_penerima' => $temp['hapus_penerima'],
        'pesan_balasan' => $conversation->load_balasan($temp['id_pesan'])
    ));
}foreach ($conversation->load_conversation_ortu() as $temp) {
    array_push($data_conversation_ortu, array(
        'id_pesan' => $temp['id_pesan'],
        'id_balasan' => $temp['id_balasan'],
        'id_penerima' => $temp['id_penerima'],
        'id_pengirim' => $temp['id_pengirim'],
        'nama_pengirim' => $temp['nama_pengirim'],
        'nama_penerima' => $temp['nama_penerima'],
        'judul' => $temp['judul'],
        'isi' => $temp['isi'],
        'waktu' => $temp['waktu'],
        'hapus_pengirim' => $temp['hapus_pengirim'],
        'hapus_penerima' => $temp['hapus_penerima'],
        'pesan_balasan' => $conversation->load_balasan($temp['id_pesan'])
    ));
}$smarty->assign('id_pengguna', $login->id_pengguna);
$smarty->assign('data_conversation_dosen', $data_conversation_dosen);
$smarty->assign('data_conversation_mahasiswa', $data_conversation_mahasiswa);
$smarty->assign('data_conversation_ortu', $data_conversation_ortu);
$smarty->assign('title', 'Conversation');
$smarty->display('display-konsultasi/conversation.tpl');
?>