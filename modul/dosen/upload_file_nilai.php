<?php
include 'config.php';
include 'proses/uploadfile.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

//$penelitian = new penelitian($db, $login->id_dosen,$login->id_program_studi);

if (post('mode') == 'upload')
	{
			// setting
			$upload_setting = array(
				'allowed_ext'	=>	array('.pdf', '.jpeg', '.jpg', '.zip'),
				'max_size'		=>	(1024 * 1024 * 2),
				'destination'	=>	'/var/www/html/modul/dosen/file/'
			);
			$upload_file = new UACC_UploadFile($_FILES['file_penelitian'], $upload_setting);
			print_r($_FILES);
			if ($upload_file->uploaded)
			{
				if ($upload_file->save())
				{
					echo 'File Berhasil Di Upload';
				}
				else echo 'File Gagal Di Upload';
			}			
	}
?>