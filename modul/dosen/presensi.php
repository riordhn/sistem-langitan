<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$smarty->assign('TITLE', 'Rekap Absen');

$smarty->display('sample/presensi/rekap_absen.tpl');
?>