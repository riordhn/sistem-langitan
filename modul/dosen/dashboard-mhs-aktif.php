<?php
include 'config.php';
include '../../includes/FusionCharts.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$db->Query("SELECT * FROM SEMESTER WHERE SEMESTER.STATUS_AKTIF_SEMESTER = 'True'");
$semester = $db->FetchAssoc();

$strXML .= "<chart palette='5' formatNumber='1' formatNumberScale='0' enableSmartLabels='1' enableRotation='0' bgColor='FFFFFF,FFFFFF' bgAlpha='100,100' bgRatio='100,100' bgAngle='360' showBorder='1' startingAngle='70' caption='MAHASISWA AKTIF TAHUN AKADEMIK $semester[TAHUN_AJARAN]'>";

$strquery =  $db->QueryToArray("SELECT JENJANG.ID_JENJANG, NM_JENJANG, COUNT(*) JUMLAH 
FROM MAHASISWA 
JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
WHERE STATUS_PENGGUNA.AKTIF_STATUS_PENGGUNA = 1 and program_studi.status_aktif_prodi = 1
GROUP BY JENJANG.ID_JENJANG, NM_JENJANG
ORDER BY JENJANG.ID_JENJANG, NM_JENJANG");

foreach($strquery as $r){
$strXML .= "<set label='$r[NM_JENJANG]' value='$r[JUMLAH]' isSliced='1'/>";
}

$strXML .= "</chart>";

/*if ($user->ID_FAKULTAS != 8)
	$tes = renderChart("../../swf/Charts/Pie2D.swf", "", $strXML, "idchart", "500", "500", "0", "0");
else*/
	$tes = renderChart("../../../swf/Charts/Pie2D.swf", "", $strXML, "idchart", "500", "500", "0", "0");
$smarty->assign('tes', $tes);


$db->Query("SELECT COUNT(*) JUMLAH 
FROM MAHASISWA 
JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
WHERE STATUS_PENGGUNA.AKTIF_STATUS_PENGGUNA = 1  and program_studi.status_aktif_prodi = 1");
$total = $db->FetchAssoc();
$smarty->assign('total', $total['JUMLAH']);

$smarty->display('sample/dashboard/dashboard-mhs-aktif.tpl');
?>