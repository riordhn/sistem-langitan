<?php

// membuat class file operasi untuk menangani file upload
class file_operation {

    //pendefinisian variabel untuk menampung data
    public $db;
    public $id;
    public $file_name;
    public $file_address;
    public $file_size;
    public $file_type;

    // contructor yang berasal dari class user dan class database
    function __construct($db, $user) {
        $this->db = $db;
        $this->user = $user;
    }

    // fungsi upload file
    function check_upload($id, $file_name, $file_temp_name, $file_type, $file_error) {
        //cek apakah file yang di upload sesuai dengan document yang dibutuhkan
        if (($file_type == 'application/pdf') || ($file_type == 'application/vnd.ms-excel') || ($file_type == 'application/msword') || ($file_type == 'application/vnd.ms-powerpoint')
                || ($file_type == 'text/plain') || ($file_type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                || ($file_type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                || ($file_type == 'application/vnd.openxmlformats-officedocument.presentationml.presentation') || ($file_type == 'image/jpeg') || $file_type == 'image/png') {
            //cek errot pada saat upload file
            if ($file_error == 0) {
                // cek apakah file yang di upload sudah tersimpan,, jika sudah maka upload file gagal
                if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/file/' . $file_name)) {
                    echo '<script> alert("' . $file_name . 'File Sudah Ada coba menggunakan nama lain")';
                    $statement = false;
                } else {
                    $statement = true;
                }
            }
        } else {
            echo '<script> alert("Invalid File Upload")';
            $statement = false;
        }

        return $statement;
    }

    function upload($id, $file_name, $file_temp_name, $file_type, $file_error) {
        move_uploaded_file($file_temp_name, $_SERVER['DOCUMENT_ROOT'] . '/file/' . $file_name);
        $this->file_name = $file_temp_name;
        $this->file_address = $_SERVER['DOCUMENT_ROOT'] . '/file' . $file_name;
        $this->file_type = $file_type;
        // menyimpan data file yang di upload ke dalam database  
        //$this->db->Query("INSERT INTO DOKUMEN_UMUM(ID_PENGGUNA,NM_DOK_UMUM,DESKRIPSI_DOK_UMUM,SIFAT_DOK_UMUM,LINK_DOKUMEN) VALUES ('$this->id','$this->file_name','File Dosen','$this->file_type','$this->file_address')");
    }

}

?>
