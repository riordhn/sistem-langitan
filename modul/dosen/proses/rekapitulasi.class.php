<?php

class rekapitulasi {

    public $db;
    public $id_dosen;
    public $id_perguruan_tinggi;

    function __construct($db, $id_dosen, $id_perguruan_tinggi) {
        $this->db = $db;
        $this->id_dosen = $id_dosen;
        $this->id_perguruan_tinggi = $id_perguruan_tinggi;
    }

    //fungsi rekapitulasi dosen

    function load_data_dosen($fakultas, $prodi) {
        $data_dosen = array();
        $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI='{$prodi}'" : "";
        $data = $this->db->QueryToArray("
        SELECT D.ID_DOSEN,D.NIP_DOSEN,(P.GELAR_DEPAN||' '||P.NM_PENGGUNA||' '||P.GELAR_BELAKANG) NM_PENGGUNA,NVL(D.MOBILE_DOSEN,D.TLP_DOSEN) TELP
                ,PS.NM_PROGRAM_STUDI,SP.NM_STATUS_PENGGUNA,D.BIDANG_KEAHLIAN_DOSEN FROM DOSEN D
        JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = D.ID_PROGRAM_STUDI
        JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = D.ID_STATUS_PENGGUNA
        WHERE D.FLAG_VALID=1 AND PS.ID_FAKULTAS='{$fakultas}' {$q_prodi}
        ORDER BY NM_PROGRAM_STUDI");
        foreach ($data as $temp) {
            if (file_exists("../../foto_pegawai/{$temp['NIP_DOSEN']}.JPG"))
                $foto = $base_url."foto_pegawai/{$temp['NIP_DOSEN']}.JPG";
            else if (file_exists("../../modulx/images/{$temp['NIP_DOSEN']}.JPG"))
                $foto = "/modulx/images/{$temp['NIP_DOSEN']}.JPG";
            else
                $foto = $base_url."img/dosen/photo.png";
            array_push($data_dosen, array(
                'ID_DOSEN' => $temp['ID_DOSEN'],
                'NIP_DOSEN' => $temp['NIP_DOSEN'],
                'NM_PROGRAM_STUDI' => $temp['NM_PROGRAM_STUDI'],
                'NM_PENGGUNA' => $temp['NM_PENGGUNA'],
                'TELP' => $temp['TELP'],
                'STATUS' => $temp['NM_STATUS_PENGGUNA'],
                'BIDANG' => $temp['BIDANG_KEAHLIAN_DOSEN'],
                'FOTO' => $foto
            ));
        }
        return $data_dosen;
    }
	
	
	function load_thn_angkatan() {
		return $this->db->QueryToArray("select distinct thn_angkatan_mhs from mahasiswa order by thn_angkatan_mhs desc");
	}
	
	function load_jalurs1() {
		return $this->db->QueryToArray("select * from jalur where id_perguruan_tinggi = '{$this->id_perguruan_tinggi}' order by nm_jalur asc");
	}
	
	
	function load_data_ipk($thn_angakatan) {
		
		return $this->db->QueryToArray("select nm_fakultas,nm_program_studi,nm_jalur,round(avg(IPK_MHS),2) as rerata_ipk, id_jalur,
round(avg(non_D),2) as rerata_sks_non_D,round(avg(rerata_ips),2) as rerata_ips,round(avg(rerata_sks),2) as rerata_sks from
(select s1.id_mhs,s1.nm_program_studi,s1.nm_fakultas,s1.nm_jalur,id_jalur,s2.IPK_MHS,s3.non_D,s4.rerata_ips,s4.rerata_sks from
(select mahasiswa.id_mhs,nm_program_studi,nm_fakultas,nm_jalur,j.id_jalur from mahasiswa
		join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
		join fakultas on program_studi.id_fakultas=fakultas.id_fakultas
		join jalur_mahasiswa jm on mahasiswa.id_mhs=jm.id_mhs and id_jalur_aktif=1 and jm.id_jalur in (1,2,3)
		join jalur j on jm.id_jalur=j.id_jalur and j.id_jalur in (1,2,3)
		where thn_angkatan_mhs in ({$thn_angakatan}))s1
		left join 
		(select id_mhs,sum(kredit_semester) as SKS_TOTAL_MHS, 
		round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
		from 
		(select a.id_mhs,c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
		case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
		row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
		from pengambilan_mk a
		join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
		join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
		join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
		join semester smt on a.id_semester=smt.id_semester
		join mahasiswa m on a.id_mhs=m.id_mhs
		join program_studi ps on m.id_program_studi=ps.id_program_studi
		join jalur_mahasiswa jm on m.id_mhs=jm.id_mhs and id_jalur_aktif=1 and jm.id_jalur in (1,2,3)
		join jalur j on jm.id_jalur=j.id_jalur and j.id_jalur in (1,2,3)
		where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
		and a.status_hapus=0  and m.thn_angkatan_mhs in ({$thn_angakatan}) and ps.id_jenjang=1
		and a.status_pengambilan_mk !=0)
		where tahun<=20112 and rangking=1
		group by id_mhs)s2 on s1.id_mhs=s2.id_mhs
		left join
		(select id_mhs,sum(kredit_semester) as non_D
		from 
		(select a.id_mhs,c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
		case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
		row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
		from pengambilan_mk a
		join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
		join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
		join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
		join semester smt on a.id_semester=smt.id_semester
		join mahasiswa m on a.id_mhs=m.id_mhs
		join program_studi ps on m.id_program_studi=ps.id_program_studi
		join jalur_mahasiswa jm on m.id_mhs=jm.id_mhs and id_jalur_aktif=1 and jm.id_jalur in (1,2,3)
		join jalur j on jm.id_jalur=j.id_jalur and j.id_jalur in (1,2,3)
		where a.status_apv_pengambilan_mk = 1 and ((a.nilai_huruf<>'E' and a.nilai_huruf<>'D') or a.nilai_huruf<>null) 
		and a.status_hapus=0  and m.thn_angkatan_mhs in ({$thn_angakatan}) and ps.id_jenjang=1
		and a.status_pengambilan_mk !=0)
		where tahun<=20112 and rangking=1
		group by id_mhs)s3 on s1.id_mhs=s3.id_mhs
		left join
		(select id_mhs,round(avg(ips),2) as rerata_ips,round(avg(sks_sem),2) as rerata_sks from 
		(select id_mhs,tahun,case when sum(bobot*kredit_semester)=0 then 0 else 
		round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
		sum(kredit_semester)as sks_sem 
		from 
		(select id_mhs,tahun,id_kurikulum_mk,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
		(select a.id_mhs,ps.id_fakultas,a.id_kurikulum_mk,
		case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
		case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
		and d.status_mkta in (1,2) then 0
		else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
		case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
		case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
		from pengambilan_mk a
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
		left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
		left join mahasiswa m on a.id_mhs=m.id_mhs
		left join program_studi ps on m.id_program_studi=ps.id_program_studi
		left join semester s on a.id_semester=s.id_semester
		where a.status_apv_pengambilan_mk='1' and a.status_hapus=0 and m.thn_angkatan_mhs in ({$thn_angakatan})
		and a.status_pengambilan_mk !=0 and ps.id_jenjang=1
		)group by id_mhs, tahun, id_kurikulum_mk, kredit_semester, sksreal
		)
		group by id_mhs,tahun)
		group by id_mhs)s4 on s1.id_mhs=s4.id_mhs)
		group by nm_fakultas,nm_program_studi,nm_jalur,id_jalur
		order by nm_fakultas,nm_program_studi
		");
	}
	
	
	function fakultas() {
        return $this->db->QueryToArray("
            SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$this->id_perguruan_tinggi}' ORDER BY ID_FAKULTAS ASC
            ");
    }
	
	function jenjang() {
        return $this->db->QueryToArray("
            SELECT * FROM JENJANG ORDER BY NM_JENJANG ASC
            ");
    }
	
	
	function semester() {
        return $this->db->QueryToArray("
            SELECT * FROM SEMESTER WHERE ID_PERGURUAN_TINGGI = '{$this->id_perguruan_tinggi}' AND NM_SEMESTER = 'Genap' or NM_SEMESTER = 'Ganjil' ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC
            ");
    }
	
	
	function thn_akademik() {
        return $this->db->QueryToArray("
            SELECT TAHUN_AJARAN, THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE ID_PERGURUAN_TINGGI = '{$this->id_perguruan_tinggi}' GROUP BY TAHUN_AJARAN, THN_AKADEMIK_SEMESTER 
			ORDER BY THN_AKADEMIK_SEMESTER DESC
            ");
    }
	
	function thn_angkatan() {
        return $this->db->QueryToArray("
            SELECT DISTINCT THN_ANGKATAN_MHS FROM MAHASISWA M JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
            WHERE P.ID_PERGURUAN_TINGGI = '{$this->id_perguruan_tinggi}' ORDER BY THN_ANGKATAN_MHS DESC
            ");
    }
	
	private function get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi) {
        if ($id_fakultas != "" && $id_prodi != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}' 
						AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_fakultas != "" && $id_prodi != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } elseif ($id_fakultas != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_prodi != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}' AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_prodi != "") {
            $query = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } elseif ($id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } else if ($id_fakultas != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' ";
        } else {
            $query = " ";
        }
        return $query;
    }
	
	private function get_ipk($id_fakultas, $id_jenjang, $id_prodi) {
	
		$condition_akademik =  $this->get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi);
		
            $query = "
				SELECT ROUND(SUM(KREDIT_SEMESTER*NILAI_STANDAR_NILAI)/SUM(KREDIT_SEMESTER), 2) AS IPK, SUM(KREDIT_SEMESTER) AS SKS, ID_MHS
				FROM (
					SELECT MAHASISWA.ID_MHS, MAHASISWA.NIM_MHS, STANDAR_NILAI.NILAI_STANDAR_NILAI, TAHUN_AJARAN, NM_SEMESTER, 
					KD_MATA_KULIAH, NM_MATA_KULIAH, KURIKULUM_MK.KREDIT_SEMESTER, NILAI_HURUF, FLAGNILAI, 
					ROW_NUMBER() OVER(PARTITION BY MAHASISWA.ID_MHS, NM_MATA_KULIAH ORDER BY NILAI_HURUF) RANGKING,
					COUNT(*) OVER(PARTITION BY NM_MATA_KULIAH) TERULANG
					FROM PENGAMBILAN_MK
					JOIN KURIKULUM_MK ON KURIKULUM_MK.ID_KURIKULUM_MK = PENGAMBILAN_MK.ID_KURIKULUM_MK
					JOIN MATA_KULIAH ON MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
					JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PENGAMBILAN_MK.ID_SEMESTER
					JOIN STANDAR_NILAI ON STANDAR_NILAI.NM_STANDAR_NILAI = PENGAMBILAN_MK.NILAI_HURUF 
					JOIN MAHASISWA ON MAHASISWA.ID_MHS = PENGAMBILAN_MK.ID_MHS
					JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
					JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
					JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
					WHERE PENGAMBILAN_MK.STATUS_APV_PENGAMBILAN_MK = '1' AND PENGAMBILAN_MK.STATUS_HAPUS = 0 {$condition_akademik}
					 ) X
				WHERE RANGKING = 1 AND FLAGNILAI = 1 AND NILAI_HURUF < 'E' AND NILAI_HURUF IS NOT NULL AND NILAI_HURUF != '-'
				GROUP BY ID_MHS
				";
        return $query;
    }
	
	
	function program_studi($id_fakultas, $id_jenjang, $id_prodi) {
		$condition_akademik =  $this->get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi);
		return $this->db->QueryToArray("SELECT * FROM PROGRAM_STUDI 
										JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
										JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
										WHERE STATUS_AKTIF_PRODI = 1 {$condition_akademik} ");
			
	}
	
	
	function status_akademik($jenjang, $thn_angkatan, $thn_akademik) {
		
		
		if($thn_akademik != ''){
		return $this->db->QueryToArray("
            select ps.id_program_studi, ps.id_jenjang, ps.nm_program_studi,
			  (select count(id_mhs) from mahasiswa m 
					join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
					where m.id_program_studi = ps.id_program_studi and aktif_status_pengguna = 1 and thn_angkatan_mhs <= '{$thn_akademik}'
			  ) aktif,
			  (select count(m.id_mhs) from mahasiswa m 
					left join admisi a on a.id_mhs = m.id_mhs
					where m.id_program_studi = ps.id_program_studi and m.status_akademik_mhs = 4 and status_apv = 1 and thn_angkatan_mhs <= '{$thn_akademik}'
			  ) lulus,
			  (select count(m.id_mhs) from mahasiswa m 
			  		left join admisi a on a.id_mhs = m.id_mhs
			   		where m.id_program_studi = ps.id_program_studi and m.status_akademik_mhs = 5 and status_apv = 1 and thn_angkatan_mhs <= '{$thn_akademik}'
			  ) undur,
			  (select count(m.id_mhs) from mahasiswa m 
			  		left join admisi a on a.id_mhs = m.id_mhs
			   		where m.id_program_studi = ps.id_program_studi and m.status_akademik_mhs = 6 and status_apv = 1 and thn_angkatan_mhs <= '{$thn_akademik}'
			  ) do
			from program_studi ps where ps.id_jenjang = {$jenjang} and status_aktif_prodi = 1
			order by ps.id_fakultas, ps.nm_program_studi
            ");
		}elseif($thn_angkatan != ''){

        return $this->db->QueryToArray("
            select ps.id_program_studi, ps.id_jenjang, ps.nm_program_studi,
			  (select count(id_mhs) from mahasiswa m 
					join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
					where m.id_program_studi = ps.id_program_studi and aktif_status_pengguna = 1 and thn_angkatan_mhs = '{$thn_angkatan}'
			  ) aktif,
			  (select count(m.id_mhs) from mahasiswa m 
					left join admisi a on a.id_mhs = m.id_mhs
					where m.id_program_studi = ps.id_program_studi and m.status_akademik_mhs = 4 and status_apv = 1 and thn_angkatan_mhs = '{$thn_angkatan}'
			  ) lulus,
			  (select count(m.id_mhs) from mahasiswa m 
			  		left join admisi a on a.id_mhs = m.id_mhs
			   		where m.id_program_studi = ps.id_program_studi and m.status_akademik_mhs = 5 and status_apv = 1 and thn_angkatan_mhs = '{$thn_angkatan}'
			  ) undur,
			  (select count(m.id_mhs) from mahasiswa m 
			  		left join admisi a on a.id_mhs = m.id_mhs
			   		where m.id_program_studi = ps.id_program_studi and m.status_akademik_mhs = 6 and status_apv = 1 and thn_angkatan_mhs = '{$thn_angkatan}'
			  ) do
			from program_studi ps where ps.id_jenjang = {$jenjang} and status_aktif_prodi = 1
			order by ps.id_fakultas, ps.nm_program_studi
            ");
		}
    }


	function rekap_ipk($jenjang) {
		
		$ipk =  $this->get_ipk('', $id_jenjang, '');
		/*",
				  (select count(mi.id_mhs) from mahasiswa mi 
						join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
						join ($ipk) a on a.id_mhs = mi.id_mhs 
						where mi.id_program_studi = ps.id_program_studi and aktif_status_pengguna = 1 and (ipk <= 2)
				  ) ip1,
				  (select count(mi.id_mhs) from mahasiswa mi 
						join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
						join ($ipk) a on a.id_mhs = mi.id_mhs 
						where mi.id_program_studi = ps.id_program_studi and aktif_status_pengguna = 1 and (ipk > 2 and ipk <= 2.5)
				  ) ip2,
				  (select count(mi.id_mhs) from mahasiswa mi 
						join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
						join ($ipk) a on a.id_mhs = mi.id_mhs 
						where mi.id_program_studi = ps.id_program_studi and aktif_status_pengguna = 1 and (ipk > 2.5 and ipk <= 2.75)
				  ) ip3,
				  (select count(mi.id_mhs) from mahasiswa mi 
						join ($ipk) a on a.id_mhs = mi.id_mhs 
						join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
						where mi.id_program_studi = ps.id_program_studi and aktif_status_pengguna = 1 and (ipk > 2.75 and ipk <= 3)
				  ) ip4,
				  (select count(mi.id_mhs) from mahasiswa mi 
						join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
						join ($ipk) a on a.id_mhs = mi.id_mhs 
						where mi.id_program_studi = ps.id_program_studi and aktif_status_pengguna = 1 and (ipk > 3 and ipk <= 3.5)
				  ) ip5,
				  (select count(mi.id_mhs) from mahasiswa mi 
						join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
						join ($ipk) a on a.id_mhs = mi.id_mhs 
						where mi.id_program_studi = ps.id_program_studi and aktif_status_pengguna = 1 and (ipk > 3.5)
				  ) ip6"*/

		return $this->db->QueryToArray("
			select program_studi.id_program_studi, ip1, ip2
			from program_studi
			left join (
				select count(*) as ip1, id_program_studi
				from mahasiswa
				join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
				left join ($ipk) a on a.id_mhs = mahasiswa.id_mhs
				where aktif_status_pengguna = 1 and ipk <= 2
				group by id_program_studi) a on a.id_program_studi = program_studi.id_program_studi
			left join (
				select count(*) as ip2, id_program_studi
				from mahasiswa
				join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
				left join ($ipk) a on a.id_mhs = mahasiswa.id_mhs
				where aktif_status_pengguna = 1 and (ipk > 2 and ipk <= 2.5)
				group by id_program_studi) b on b.id_program_studi = program_studi.id_program_studi
			left join (
				select count(*) as ip3, id_program_studi
				from mahasiswa
				join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
				left join ($ipk) a on a.id_mhs = mahasiswa.id_mhs
				where aktif_status_pengguna = 1 and (ipk > 2.5 and ipk <= 2.75)
				group by id_program_studi) c on c.id_program_studi = program_studi.id_program_studi
			left join (
				select count(*) as ip4, id_program_studi
				from mahasiswa
				join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
				left join ($ipk) a on a.id_mhs = mahasiswa.id_mhs
				where aktif_status_pengguna = 1 and (ipk > 2.75 and ipk <= 3)
				group by id_program_studi) d on d.id_program_studi = program_studi.id_program_studi
			left join (
				select count(*) as ip5, id_program_studi
				from mahasiswa
				join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
				left join ($ipk) a on a.id_mhs = mahasiswa.id_mhs
				where aktif_status_pengguna = 1 and (ipk > 3 and ipk <= 3.5)
				group by id_program_studi) e on e.id_program_studi = program_studi.id_program_studi
			left join (
				select count(*) as ip6, id_program_studi
				from mahasiswa
				join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
				left join ($ipk) a on a.id_mhs = mahasiswa.id_mhs
				where aktif_status_pengguna = 1 and (ipk > 3.5)
				group by id_program_studi) f on f.id_program_studi = program_studi.id_program_studi
			where id_jenjang = {$jenjang} and status_aktif_prodi = 1
            ");
    }
}

?>
