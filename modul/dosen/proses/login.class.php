<?php

class login {

    public $db;
    public $id_pengguna;
    public $username;
    public $nama_pengguna;
    public $id_dosen;
    public $fakultas;
    public $id_fakultas;
    public $id_program_studi;
    public $id_role;
	public $id_jenjang;
    function __construct($database, $id_pengguna) {
        $this->db = $database;
        $this->id_pengguna = $id_pengguna;
    }

    function get_login() {
        $this->db->Query(
        "SELECT H.*,F.ID_FAKULTAS,F.NM_FAKULTAS as fakultas
        from FAKULTAS F 
            join (select P.USERNAME,P.NM_PENGGUNA,D.ID_PROGRAM_STUDI,D.ID_DOSEN, PR.ID_FAKULTAS,P.ID_ROLE,PR.ID_JENJANG from DOSEN D 
                join PROGRAM_STUDI PR on D.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
                join PENGGUNA P on P.ID_PENGGUNA = D.ID_PENGGUNA
                where D.ID_PENGGUNA='$this->id_pengguna') 
        H on H.ID_FAKULTAS = F.ID_FAKULTAS");

        $temp = $this->db->FetchAssoc();
        $this->username = $temp['USERNAME'];
        $this->nama_pengguna = $temp['NM_PENGGUNA'];
        $this->id_program_studi = $temp['ID_PROGRAM_STUDI'];
        $this->id_fakultas = $temp['ID_FAKULTAS'];
        $this->fakultas = $temp['FAKULTAS'];
        $this->id_dosen = $temp['ID_DOSEN'];
        $this->id_role = $temp['ID_ROLE'];
        $this->id_jenjang = $temp['ID_JENJANG'];
    }

}

?>
