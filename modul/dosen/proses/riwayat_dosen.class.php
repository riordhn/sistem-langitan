<?php

class riwayat_dosen {

    public $db;
    public $id_pengguna;

    function __construct($db, $id_pengguna) {
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
    }

    function load_sejarah_golongan() {
        return $this->db->QueryToArray("
            SELECT SG.*,TO_CHAR(SG.TMT_SEJARAH_GOLONGAN,'DD-MM-YYYY') TMT_GOL,
            TO_CHAR(SG.TGL_SK_SEJARAH_GOLONGAN,'DD-MM-YYYY') TGL_SK,G.NM_GOLONGAN 
            FROM SEJARAH_GOLONGAN SG
            JOIN GOLONGAN G ON G.ID_GOLONGAN=SG.ID_GOLONGAN
            WHERE SG.ID_PENGGUNA='{$this->id_pengguna}'
        ");
    }

    function load_sejarah_fungsional() {
        return $this->db->QueryToArray("
            SELECT SF.*,TO_CHAR(SF.TMT_SEJ_JAB_FUNGSIONAL,'DD-MM-YYYY') TMT_FUNG,
            TO_CHAR(SF.TGL_SK_SEJ_JAB_FUNGSIONAL,'DD-MM-YYYY') TGL_SK,JF.NM_JABATAN_FUNGSIONAL
            FROM SEJARAH_JABATAN_FUNGSIONAL SF
            JOIN JABATAN_FUNGSIONAL JF ON SF.ID_JABATAN_FUNGSIONAL=JF.ID_JABATAN_FUNGSIONAL
            WHERE SF.ID_PENGGUNA='{$this->id_pengguna}'
        ");
    }

    function load_sejarah_struktural() {
        return $this->db->QueryToArray("
            SELECT SS.*,TO_CHAR(SS.TMT_SEJ_JAB_STRUKTURAL,'DD-MM-YYYY') TMT_STRUK,
            TO_CHAR(SS.TGL_SK_SEJ_JAB_STRUKTURAL,'DD-MM-YYYY') TGL_SK,JF.NM_JABATAN_FUNGSIONAL
            FROM SEJARAH_JABATAN_STRUKTURAL SS
            JOIN JABATAN_STRUKTURAL JS ON JS.ID_JABATAN_STRUKTURAL=SS.ID_JABATAN_STRUKTURAL
            WHERE SF.ID_PENGGUNA='{$this->id_pengguna}'
        ");
    }
    
    function load_sejarah_pendidikan(){
        return $this->db->QueryToArray("
           SELECT SP.*,PA.NAMA_PENDIDIKAN_AKHIR
            FROM SEJARAH_PENDIDIKAN SP
            JOIN PENDIDIKAN_AKHIR PA ON SP.ID_PENDIDIKAN_AKHIR=PA.ID_PENDIDIKAN_AKHIR
            WHERE SP.ID_PENGGUNA='{$this->id_pengguna}'
        ");
    }

    function set() {
        $this->db->Open();
        $this->db->Query("SELECT * FROM DOSEN_RIWAYAT WHERE ID_DOSEN= '$this->id_dosen'");
        $i = 1;
        $j = 1;
        $k = 1;
        while ($temp = $this->db->FetchArray()) {
            if ($temp['JENIS_DOS_RIWAYAT'] == 'pangkat') {
                array_push($this->riwayat_pangkat, array(
                    'index' => $i++,
                    'id_riwayat' => $temp['ID_DOSEN_RIWAYAT'],
                    'nama' => $temp['NM_DOS_RIWAYAT'],
                    'mulai' => date('d F Y', strtotime($temp['MULAI_DOS_RIWAYAT'])),
                    'selesai' => date('d F Y', strtotime($temp['SELESAI_DOS_RIWAYAT']))
                ));
            } else if ($temp['JENIS_DOS_RIWAYAT'] == 'fungsi') {
                array_push($this->riwayat_fungsi, array(
                    'index' => $j++,
                    'id_riwayat' => $temp['ID_DOSEN_RIWAYAT'],
                    'nama' => $temp['NM_DOS_RIWAYAT'],
                    'mulai' => date('d F Y', strtotime($temp['MULAI_DOS_RIWAYAT'])),
                    'selesai' => date('d F Y', strtotime($temp['SELESAI_DOS_RIWAYAT']))
                ));
            } else {
                array_push($this->riwayat_struktur, array(
                    'index' => $k++,
                    'id_riwayat' => $temp['ID_DOSEN_RIWAYAT'],
                    'nama' => $temp['NM_DOS_RIWAYAT'],
                    'mulai' => date('d F Y', strtotime($temp['MULAI_DOS_RIWAYAT'])),
                    'selesai' => date('d F Y', strtotime($temp['SELESAI_DOS_RIWAYAT']))
                ));
            }
        }
        $this->db->Close();
    }

}

?>
