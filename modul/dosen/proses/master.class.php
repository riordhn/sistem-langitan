<?php

class master {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    private function get_condition_biaya_kuliah($semester, $fakultas, $prodi, $jenjang, $jalur, $kelompok_biaya) {
        $query = "";
        if ($semester != '') {
            $query .="AND BK.ID_SEMESTER={$semester}";
        }
        if ($prodi != '') {
            $query .="AND BK.ID_PROGRAM_STUDI={$prodi}";
        }
        if ($fakultas != '') {
            $query .="AND PS.ID_FAKULTAS={$fakultas}";
        }
        if ($jenjang != '') {
            $query .="AND BK.ID_JENJANG={$jenjang}";
        }
        if ($jalur != '') {
            $query .="AND BK.ID_JALUR={$jalur}";
        }
        if ($kelompok_biaya != '') {
            $query .="AND BK.ID_KELOMPOK_BIAYA={$kelompok_biaya}";
        }
        return $query;
    }

    function load_biaya_kuliah($semester, $fakultas, $prodi, $jenjang, $jalur, $kelompok_biaya) {
        $condition_biaya_kuliah = $this->get_condition_biaya_kuliah($semester, $fakultas, $prodi, $jenjang, $jalur, $kelompok_biaya);
        return $this->db->QueryToArray("
            SELECT BK.*,S.NM_SEMESTER,S.TAHUN_AJARAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,KB.KHUSUS_KELOMPOK_BIAYA
                FROM BIAYA_KULIAH BK
            JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG= BK.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR =BK.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
            WHERE BK.ID_BIAYA_KULIAH IS NOT NULL {$condition_biaya_kuliah}
            ORDER BY BK.ID_PROGRAM_STUDI,BK.ID_PROGRAM_STUDI,S.NM_SEMESTER DESC,S.TAHUN_AJARAN DESC
            ");
    }

    function load_biaya_kuliah_mhs($nim) {
        return $this->db->QueryToArray("
            SELECT BK.ID_BIAYA_KULIAH,BK.BESAR_BIAYA_KULIAH,S.NM_SEMESTER,S.TAHUN_AJARAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,KB.KHUSUS_KELOMPOK_BIAYA
                FROM BIAYA_KULIAH BK
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH
            JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
            JOIN MAHASISWA M ON BK.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG= BK.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR =BK.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
            WHERE BK.ID_BIAYA_KULIAH IS NOT NULL AND M.NIM_MHS='{$nim}'
            GROUP BY BK.ID_BIAYA_KULIAH,BK.BESAR_BIAYA_KULIAH,S.NM_SEMESTER,S.TAHUN_AJARAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,KB.KHUSUS_KELOMPOK_BIAYA
            ORDER BY S.NM_SEMESTER DESC,S.TAHUN_AJARAN DESC");
    }

    function get_biaya_kuliah_mhs($nim) {
        $this->db->Query("
            SELECT BK.*,S.NM_SEMESTER,S.TAHUN_AJARAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,KB.KHUSUS_KELOMPOK_BIAYA
                FROM BIAYA_KULIAH BK
            JOIN BIAYA_KULIAH_MHS BKM ON BKM.ID_BIAYA_KULIAH = BK.ID_BIAYA_KULIAH
            JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
            JOIN MAHASISWA M ON BKM.ID_MHS=M.ID_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG= BK.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR =BK.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
            WHERE BK.ID_BIAYA_KULIAH IS NOT NULL AND M.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function update_biaya_kuliah_mhs($nim, $id_biaya_kuliah) {
        $this->db->Query("UPDATE BIAYA_KULIAH_MHS SET ID_BIAYA_KULIAH='{$id_biaya_kuliah}' WHERE ID_MHS IN (SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS='{$nim}')");
    }

    function insert_biaya_kuliah_mhs($nim, $id_biaya_kuliah) {
        $this->db->Query("INSERT INTO BIAYA_KULIAH_MHS (ID_MHS,ID_BIAYA_KULIAH) SELECT ID_MHS,'{$id_biaya_kuliah}' AS ID_BIAYA_KULIAH FROM MAHASISWA WHERE NIM_MHS='{$nim}'");
    }

    function get_biaya_kuliah_added() {
        $this->db->Query("SELECT BK.*,S.NM_SEMESTER,S.TAHUN_AJARAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,KB.KHUSUS_KELOMPOK_BIAYA
            FROM BIAYA_KULIAH BK
            JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG= BK.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR =BK.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
            WHERE BK.BESAR_BIAYA_KULIAH IS NULL 
            ORDER BY BK.ID_BIAYA_KULIAH DESC");
        return $this->db->FetchAssoc();
    }

    function get_biaya_kuliah($id_biaya_kuliah) {
        $this->db->Query("
            SELECT BK.*,S.NM_SEMESTER,S.TAHUN_AJARAN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,KB.KHUSUS_KELOMPOK_BIAYA FROM BIAYA_KULIAH BK
            JOIN SEMESTER S ON S.ID_SEMESTER = BK.ID_SEMESTER
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG= BK.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR =BK.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = BK.ID_KELOMPOK_BIAYA
            WHERE BK.ID_BIAYA_KULIAH='{$id_biaya_kuliah}'");
        return $this->db->FetchAssoc();
    }

    function get_detail_biaya($id_biaya_kuliah, $id_biaya) {
        $this->db->Query("SELECT * FROM DETAIL_BIAYA WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}' AND ID_BIAYA='{$id_biaya}'");
        return $this->db->FetchAssoc();
    }

    function load_detail_biaya($id_biaya_kuliah) {
        $data_detail_biaya = array();
        foreach ($this->db->QueryToArray("SELECT * FROM BIAYA ORDER BY NM_BIAYA") as $data) {
            array_push($data_detail_biaya, array(
                'ID_BIAYA' => $data['ID_BIAYA'],
                'NM_BIAYA' => $data['NM_BIAYA'],
                'DETAIL_BIAYA' => $this->get_detail_biaya($id_biaya_kuliah, $data['ID_BIAYA'])
            ));
        }
        return $data_detail_biaya;
    }

    function add_detail_biaya($id_biaya_kuliah, $id_biaya, $besar_biaya, $pendaftaran) {
        $this->db->Query("
            INSERT INTO DETAIL_BIAYA
                (ID_BIAYA_KULIAH,ID_BIAYA,BESAR_BIAYA,PENDAFTARAN_BIAYA)
            VALUES
                ('{$id_biaya_kuliah}','{$id_biaya}','{$besar_biaya}','{$pendaftaran}')");
    }

    function update_detail_biaya($id_biaya_kuliah, $id_biaya, $besar_biaya, $pendaftaran) {
        $this->db->Query("
            UPDATE DETAIL_BIAYA SET
                BESAR_BIAYA='{$besar_biaya}',
                PENDAFTARAN_BIAYA='{$pendaftaran}'
            WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}' AND ID_BIAYA='{$id_biaya}'");
    }

    function update_biaya_kuliah($id_biaya_kuliah, $besar_biaya_kuliah) {
        $this->db->Query("
            UPDATE BIAYA_KULIAH SET
                BESAR_BIAYA_KULIAH='{$besar_biaya_kuliah}'
            WHERE ID_BIAYA_KULIAH='{$id_biaya_kuliah}'");
    }

    function add_biaya_kuliah($semester, $prodi, $jenjang, $jalur, $kelompok_biaya, $keterangan) {
        $this->db->Query("
            INSERT INTO BIAYA_KULIAH
                (ID_SEMESTER,ID_PROGRAM_STUDI,ID_JENJANG,ID_JALUR,ID_KELOMPOK_BIAYA,KETERANGAN_BIAYA_KULIAH)
            VALUES
                ('{$semester}','{$prodi}','{$jenjang}','{$jalur}','{$kelompok_biaya}','{$keterangan}')");
    }

    function load_kelompok_biaya() {
        return $this->db->QueryToArray("SELECT * FROM KELOMPOK_BIAYA ORDER BY NM_KELOMPOK_BIAYA ASC");
    }

    function get_kelompok_biaya($id) {
        $this->db->Query("SELECT * FROM KELOMPOK_BIAYA WHERE ID_KELOMPOK_BIAYA='{$id}'");
        return $this->db->FetchAssoc();
    }

    function add_kelompok_biaya($nama, $jenis, $keterangan) {
        $this->db->Query("
		INSERT INTO KELOMPOK_BIAYA
			(NM_KELOMPOK_BIAYA,KHUSUS_KELOMPOK_BIAYA,KETERANGAN_KELOMPOK_BIAYA)
		VALUES
			('{$nama}','{$jenis}','{$keterangan}')");
    }

    function edit_kelompok_biaya($nama, $jenis, $keterangan, $id) {
        $this->db->Query("
		UPDATE KELOMPOK_BIAYA SET
			NM_KELOMPOK_BIAYA='{$nama}',
			KHUSUS_KELOMPOK_BIAYA='{$jenis}',
			KETERANGAN_KELOMPOK_BIAYA='{$keterangan}'
		WHERE ID_KELOMPOK_BIAYA={$id}
		");
    }

    function load_biaya_sks() {
        return $this->db->QueryToArray("
            SELECT BS.*,F.NM_FAKULTAS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,S.NM_SEMESTER,S.TAHUN_AJARAN
            FROM BIAYA_SP BS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = BS.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN SEMESTER S ON S.ID_SEMESTER = BS.ID_SEMESTER
            ORDER BY PS.ID_FAKULTAS,S.ID_SEMESTER");
    }

    function get_biaya_sks($id_biaya_sks) {
        $this->db->Query("SELECT * FROM BIAYA_SP WHERE ID_BIAYA_SP='{$id_biaya_sks}'");
        return $this->db->FetchAssoc();
    }

    function add_biaya_sks($semester, $prodi, $besar_biaya, $keterangan) {
        $this->db->Query("
            INSERT INTO BIAYA_SP
                (ID_SEMESTER,ID_PROGRAM_STUDI,BESAR_BIAYA_SP,KETERANGAN_BIAYA_SP)
            VALUES
                ('{$semester}','{$prodi}','{$besar_biaya}','{$keterangan}')");
    }

    function edit_biaya_sks($semester, $prodi, $besar_biaya, $keterangan, $id_biaya_sks) {
        $this->db->Query("
            UPDATE BIAYA_SP SET
                ID_SEMESTER='{$semester}',
                ID_PROGRAM_STUDI='{$prodi}',
                BESAR_BIAYA_SP='{$besar_biaya}',
                KETERANGAN_BIAYA_SP='{$keterangan}'
            WHERE ID_BIAYA_SP='{$id_biaya_sks}'");
    }

    function load_nama_biaya() {
        return $this->db->QueryToArray("SELECT * FROM BIAYA ORDER BY NM_BIAYA");
    }

    function get_nama_biaya($id_biaya) {
        $this->db->Query("SELECT * FROM BIAYA WHERE ID_BIAYA='{$id_biaya}'");
        return $this->db->FetchAssoc();
    }

    function add_nama_biaya($nama, $keterangan) {
        $this->db->Query("
            INSERT INTO BIAYA
                (NM_BIAYA,KETERANGAN_BIAYA)
            VALUES
                ('{$nama}','{$keterangan}')");
    }

    function edit_nama_biaya($nama, $keterangan, $id_biaya) {
        $this->db->Query("
            UPDATE BIAYA SET
                NM_BIAYA='{$nama}',
                KETERANGAN_BIAYA='{$keterangan}'
            WHERE ID_BIAYA='{$id_biaya}'");
    }

    function load_bank() {
        return $this->db->QueryToArray("SELECT * FROM BANK ORDER BY NM_BANK");
    }

    function get_bank($id_bank) {
        $this->db->Query("SELECT * FROM BANK WHERE ID_BANK='{$id_bank}'");
        return $this->db->FetchAssoc();
    }

    function add_bank($nama) {
        $this->db->Query("
            INSERT INTO BANK (NM_BANK) VALUES ('{$nama}')");
    }

    function edit_bank($nama, $id_bank) {
        $this->db->Query("
            UPDATE BANK SET NM_BANK='{$nama}' WHERE ID_BANK='{$id_bank}'");
    }

    function load_bank_via() {
        return $this->db->QueryToArray("SELECT * FROM BANK_VIA ORDER BY NAMA_BANK_VIA");
    }

    function get_bank_via($id_bank_via) {
        $this->db->Query("SELECT * FROM BANK_VIA WHERE ID_BANK_VIA='{$id_bank_via}'");
        return $this->db->FetchAssoc();
    }

    function add_bank_via($kode, $nama) {
        $this->db->Query("SELECT ID_BANK_VIA FROM BANK_VIA ORDER BY ID_BANK_VIA DESC");
        $temp_data = $this->db->FetchAssoc();
        $id_bank_via = $temp_data['ID_BANK_VIA'] + 1;
        $this->db->Query("
            INSERT INTO BANK_VIA (ID_BANK_VIA,KODE_BANK_VIA,NAMA_BANK_VIA) VALUES ('{$id_bank_via}','{$kode}','{$nama}')");
    }

    function edit_bank_via($kode, $nama, $id_bank_via) {
        $this->db->Query("
            UPDATE BANK_VIA SET KODE_BANK_VIA='{$kode}',NAMA_BANK_VIA='{$nama}' WHERE ID_BANK_VIA='{$id_bank_via}'");
    }

    function load_status_pembayaran() {
        return $this->db->QueryToArray("SELECT * FROM STATUS_PEMBAYARAN ORDER BY ID_STATUS_PEMBAYARAN");
    }

    function get_status_pembayaran($id_status) {
        $this->db->Query("SELECT * FROM STATUS_PEMBAYARAN WHERE ID_STATUS_PEMBAYARAN='{$id_status}'");
        return $this->db->FetchAssoc();
    }

    function add_status_pembayaran($nama, $keterangan) {
        $this->db->Query("INSERT INTO STATUS_PEMBAYARAN (NAMA_STATUS,KETERANGAN) VALUES ('{$nama}','{$keterangan}')");
    }

    function edit_status_pembayaran($id_status, $nama, $keterangan) {
        $this->db->Query("UPDATE STATUS_PEMBAYARAN SET NAMA_STATUS='{$nama}',KETERANGAN='{$keterangan}' WHERE ID_STATUS_PEMBAYARAN='{$id_status}'");
    }

    function load_periode_bayar_mhs() {
        return $this->db->QueryToArray("
            SELECT PB.*,S.NM_SEMESTER,S.TAHUN_AJARAN,J.NM_JENJANG 
            FROM PERIODE_BAYAR PB
            JOIN SEMESTER S ON S.ID_SEMESTER = PB.ID_SEMESTER
            JOIN JENJANG J ON J.ID_JENJANG = PB.ID_JENJANG
            ORDER BY S.NM_SEMESTER DESC,S.TAHUN_AJARAN DESC,J.NM_JENJANG
            ");
    }

    function load_periode_bayar_cmhs() {
        return $this->db->QueryToArray("
            SELECT PB.*,S.NM_SEMESTER,S.TAHUN_AJARAN,J.NM_JENJANG 
            FROM PERIODE_BAYAR_CMHS PB
            JOIN SEMESTER S ON S.ID_SEMESTER = PB.ID_SEMESTER
            JOIN JENJANG J ON J.ID_JENJANG = PB.ID_JENJANG
            ORDER BY S.NM_SEMESTER DESC,S.TAHUN_AJARAN DESC,J.NM_JENJANG");
    }

}

?>
