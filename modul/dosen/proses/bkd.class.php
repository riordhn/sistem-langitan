<?php

//==================================
require_once 'conf.php';

class bkd {

    public $db;
    public $data_bkd = array();

    function bkd($db) {
        $this->db = $db;
    }

    function load_data($id_dosen) {
        $this->db->open();
        $this->db->query("select * from dosen_bkd where id_dosen='" . $id_dosen . "' order by id_dosen_bkd asc");
        $xyz = 0;
        while ($tmp = $this->db->fetcharray()) {
            $xyz++;
            array_push($this->data_bkd, array(
                'no_urut' => $xyz,
                'id_dosen_bkd' => $tmp['ID_DOSEN_BKD'],
                'id_dosen' => $tmp['ID_DOSEN_BKD'],
                'no_sertifikat' => $tmp['NO_SERTIFIKAT'],
                'pd' => $tmp['PD_DOSEN_BKD'],
                'pl' => $tmp['PL_DOSEN_BKD'],
                'pk' => $tmp['PK_DOSEN_BKD'],
                'pg' => $tmp['PG_DOSEN_BKD'],
                'prof' => $tmp['PROF_WAJIB_DOSEN_BKD'],
                'status' => $tmp['STATUS_DOSEN_BKD'],
                'kesimpulan' => $tmp['KESIMPULAN_DOSEN_BKD']
            ));
        }
        $this->db->close();
    }

    function cetak_bkd($id_dosen) {
        $tmp = $id_dosen;
        $this->load_data($id_dosen);
        $tmp = "";
        $tmp = $tmp . "<table class='ui-widget'>";
        $tmp = $tmp . "<tr class='ui-widget-header'><th colspan=9 class='header-coloumn'><h2>Rekap Beban Kerja Dosen</h2></th></tr>";
        $tmp = $tmp . "<tr class='ui-widget-content'><td><h3>No.</td></h3><td><h3>No. Sertifikasi</td></h3><td><h3>PD</td></h3><td><h3>PL</td></h3><td><h3>PK</td></h3><td><h3>PG</td></h3><td><h3>Kewajiban Profesor</td></h3><td><h3>Status</td></h3><td><h3>Kesimpulan</td></h3></tr>";
        foreach ($this->data_bkd as $data) {
            $tmp = $tmp . "<tr class='ui-widget-content'>";
            $tmp = $tmp . "<td>" . $data['no_urut'] . "</td>";
            $tmp = $tmp . "<td>" . $data['no_sertifikat'] . "</td>";
            $tmp = $tmp . "<td>" . $data['pd'] . "</td>";
            $tmp = $tmp . "<td>" . $data['pl'] . "</td>";
            $tmp = $tmp . "<td>" . $data['pk'] . "</td>";
            $tmp = $tmp . "<td>" . $data['pg'] . "</td>";
            $tmp = $tmp . "<td>" . $data['prof'] . "</td>";
            $tmp = $tmp . "<td>" . $data['status'] . "</td>";
            $tmp = $tmp . "<td>" . $data['kesimpulan'] . "</td>";
            $tmp = $tmp . "</tr>";
        }
        $tmp = $tmp . "</table>";

        return $tmp;
    }

}

//==================================?>