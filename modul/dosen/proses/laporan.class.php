<?php

class laporan {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    private function get_condition_akademik($id_fakultas, $id_prodi) {
        if ($id_fakultas != "" && $id_prodi != "") {
            $query = " AND PS.ID_FAKULTAS = '{$id_fakultas}' AND PS.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } else if ($id_fakultas != "") {
            $query = " AND PS.ID_FAKULTAS = '{$id_fakultas}' ";
        } else {
            $query = " ";
        }
        return $query;
    }

    private function get_condition_bank($id_bank) {
        if (empty($id_bank)) {
            $query = "AND ID_BANK IS NOT NULL";
        } else {
            if ($id_bank == 'NULL') {
                $query = "AND ID_BANK IS NULL";
            } else {
                $query = "AND ID_BANK={$id_bank}";
            }
        }
        return $query;
    }

    private function get_condition_jalur($id_jalur) {
        if ($id_jalur == '') {
            $query = "";
        } else {
            $query = "AND JAL.ID_JALUR='{$id_jalur}'";
        }
        return $query;
    }

    private function get_row_report_fakultas_mhs($id_fakultas) {
        if ($id_fakultas != '') {
            return $this->db->QueryToArray("
            SELECT F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS,PS.ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.ID_JALUR,UPPER(JAL.NM_JALUR) NM_JALUR
            FROM MAHASISWA M 
            LEFT JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR_AKTIF=1
            JOIN JALUR JAL ON JAL.ID_JALUR = JM.ID_JALUR
            JOIN PEMBAYARAN PEM ON PEM.ID_MHS = M.ID_MHS
            WHERE PEM.TGL_BAYAR IS NOT NULL AND F.ID_FAKULTAS='{$id_fakultas}' AND ID_BANK IS NOT NULL
            GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.ID_JALUR,JAL.NM_JALUR
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR");
        } else {
            return $this->db->QueryToArray("
            SELECT F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS
            FROM MAHASISWA M 
            LEFT JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN PEMBAYARAN PEM ON PEM.ID_MHS = M.ID_MHS
            WHERE PEM.TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL
            GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
            ORDER BY F.ID_FAKULTAS");
        }
    }

    private function get_jumlah_mhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur) {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_jalur = $this->get_condition_jalur($id_jalur);
        $this->db->Query("
            SELECT COUNT(COUNT(PEM.ID_MHS)) JUMLAH_MHS FROM MAHASISWA M
            JOIN PEMBAYARAN PEM ON PEM.ID_MHS=M.ID_MHS
            LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR_AKTIF=1
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR    
            JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' and TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
            {$condition_akademik} {$condition_bank} {$condition_jalur} AND ID_BANK IS NOT NULL
            GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_MHS");
        $data_jumlah_mhs = $this->db->FetchAssoc();
        $jumlah_mhs = $data_jumlah_mhs['JUMLAH_MHS'] == '' ? 0 : $data_jumlah_mhs['JUMLAH_MHS'];
        return $jumlah_mhs;
    }

    function load_report_bank_mhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi) {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        return $this->db->QueryToArray("
		SELECT B.ID_BANK,B.NM_BANK,
		  (CASE WHEN COUNT(PEM.ID_MHS) IS NULL
				THEN 0
			  ELSE
				COUNT(PEM.ID_MHS)
			  END) JUMLAH_MHS,
		  (CASE WHEN SUM(PEM.BESAR_BIAYA+PEM.DENDA_BIAYA) IS NULL 
			  THEN 0
			ELSE 
			   SUM(PEM.BESAR_BIAYA+PEM.DENDA_BIAYA)
			END) BESAR_BIAYA
		FROM BANK B
		LEFT JOIN (
		  SELECT PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_MHS,PEM.ID_BANK,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                  FROM MAHASISWA M
                  LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR_AKTIF=1
                  LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR 
		  JOIN PEMBAYARAN PEM ON PEM.ID_MHS=M.ID_MHS
		  JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                  WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}'
		  {$condition_akademik}
		  GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_MHS,PEM.ID_BANK
		  ) PEM ON PEM.ID_BANK = B.ID_BANK
		GROUP BY B.ID_BANK,B.NM_BANK
		ORDER BY B.NM_BANK");
    }

    function load_report_detail_mhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur) {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_jalur = $this->get_condition_jalur($id_jalur);
        return $this->db->QueryToArray("
		SELECT PS.ID_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,PS.ID_JENJANG,PS.NIM_MHS,PS.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,PS.NM_JALUR,PS.NM_JENJANG,PB.TGL_BAYAR,PB.BIAYA,PB.DENDA FROM
		  (SELECT M.ID_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,J.ID_JENJANG,M.NIM_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,J.NM_JENJANG 
		  FROM PENGGUNA P
		  JOIN MAHASISWA M ON P.ID_PENGGUNA  = M.ID_PENGGUNA
		  JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                  LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
                  LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR AND JM.ID_JALUR_AKTIF=1
		  JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                  WHERE PS.ID_PROGRAM_STUDI IS NOT NULL {$condition_jalur}  ) PS
		  ,
		  (SELECT ID_MHS,TGL_BAYAR,SUM(BESAR_BIAYA) BIAYA,SUM(DENDA_BIAYA) DENDA
		  FROM PEMBAYARAN
		  WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' and TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' {$condition_bank} AND ID_BANK IS NOT NULL 
		  GROUP BY NO_TRANSAKSI,ID_MHS,TGL_BAYAR ) PB
		WHERE PS.ID_MHS = PB.ID_MHS {$condition_akademik}
		ORDER BY PS.NM_PENGGUNA");
    }

    function load_report_detail_mhs_page($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur, $batas, $posisi) {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_jalur = $this->get_condition_jalur($id_jalur);
        return $this->db->QueryToArray("
		SELECT * FROM ( SELECT PAGE.*, ROWNUM rnum FROM
		(SELECT PS.ID_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,PS.ID_JENJANG,PS.NIM_MHS,PS.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,PS.NM_JALUR,PS.NM_JENJANG,PB.TGL_BAYAR,PB.BIAYA,PB.DENDA FROM
			  (SELECT M.ID_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,J.ID_JENJANG,M.NIM_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,J.NM_JENJANG 
			  FROM PENGGUNA P
			  JOIN MAHASISWA M ON P.ID_PENGGUNA  = M.ID_PENGGUNA
                          LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR_AKTIF=1
                          LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
			  JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
			  JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                          WHERE PS.ID_PROGRAM_STUDI IS NOT NULL {$condition_jalur}  ) PS
			  ,
			  (SELECT ID_MHS,TGL_BAYAR,SUM(BESAR_BIAYA) BIAYA,SUM(DENDA_BIAYA) DENDA
			  FROM PEMBAYARAN
			  WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' and TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' {$condition_bank} AND ID_BANK IS NOT NULL 
			  GROUP BY NO_TRANSAKSI,ID_MHS,TGL_BAYAR ) PB
			WHERE PS.ID_MHS = PB.ID_MHS {$condition_akademik}
		ORDER BY PS.NM_PENGGUNA) PAGE
		WHERE ROWNUM <= ({$batas}+{$posisi}))
		WHERE rnum >= ({$posisi}+1)");
    }

    function load_report_fakultas_mhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur) {
        $condition_bank = $this->get_condition_bank($id_bank);
        $data_report_fakultas = array();
        //saat report fakultas tertentu
        if ($id_prodi != '') {
            foreach ($this->load_report_detail_mhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur) as $data) {
                array_push($data_report_fakultas, array(
                    'NM_PENGGUNA' => $data['NM_PENGGUNA'],
                    'NIM_MHS' => $data['NIM_MHS'],
                    'NM_PROGRAM_STUDI' => '( ' . $data['NM_JENJANG'] . ' ) ' . $data['NM_PROGRAM_STUDI'],
                    'TOTAL_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,
                            (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                                THEN 0
                            ELSE 
                                SUM(PEM.BESAR_BIAYA)
                            END) BESAR_BIAYA,
                            (CASE WHEN SUM(PEM.DENDA_BIAYA) IS NULL 
                                THEN 0
                            ELSE 
                                SUM(PEM.DENDA_BIAYA)
                            END) DENDA_BIAYA
                            FROM BIAYA B
                        LEFT JOIN (
                              SELECT B.ID_BIAYA,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                              FROM MAHASISWA M
                              JOIN PEMBAYARAN PEM ON PEM.ID_MHS=M.ID_MHS
                              JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                              JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                              JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                              WHERE TO_CHAR(PEM.TGL_BAYAR,'DD-MON-YY') = '{$data['TGL_BAYAR']}' AND
                              M.ID_MHS='{$data['ID_MHS']}' {$condition_bank} AND ID_BANK IS NOT NULL
                              GROUP BY B.ID_BIAYA
                              ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                        GROUP BY B.ID_BIAYA,B.NM_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        } else if ($id_fakultas != '') {
            foreach ($this->get_row_report_fakultas_mhs($id_fakultas) as $data) {
                array_push($data_report_fakultas, array(
                    'ID_PROGRAM_STUDI' => $data['ID_PROGRAM_STUDI'],
                    'NM_PROGRAM_STUDI' => $data['NM_PROGRAM_STUDI'],
                    'NM_JENJANG' => $data['NM_JENJANG'],
                    'ID_JALUR' => $data['ID_JALUR'],
                    'NM_JALUR' => $data['NM_JALUR'],
                    'JUMLAH_MHS' => $this->get_jumlah_mhs($tgl_awal, $tgl_akhir, $data['ID_FAKULTAS'], $data['ID_PROGRAM_STUDI'], $id_bank, $data['ID_JALUR']),
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'TOTAL_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,
                            (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                                THEN 0
                            ELSE 
                                SUM(PEM.BESAR_BIAYA)
                            END) BESAR_BIAYA,
                            (CASE WHEN SUM(PEM.DENDA_BIAYA) IS NULL 
                                THEN 0
                            ELSE 
                                SUM(PEM.DENDA_BIAYA)
                            END) DENDA_BIAYA
                            FROM BIAYA B
                        LEFT JOIN (
                              SELECT B.ID_BIAYA,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                              FROM MAHASISWA M
                              LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR_AKTIF=1
                              LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
                              JOIN PEMBAYARAN PEM ON PEM.ID_MHS=M.ID_MHS
                              JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                              JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                              JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                              WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' AND
                              PS.ID_PROGRAM_STUDI='{$data['ID_PROGRAM_STUDI']}' {$condition_bank} AND JAL.ID_JALUR='{$data['ID_JALUR']}' AND ID_BANK IS NOT NULL
                              GROUP BY B.ID_BIAYA
                              ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                        GROUP BY B.ID_BIAYA,B.NM_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        }
        // saat report semua fakultas
        else {
            foreach ($this->get_row_report_fakultas_mhs($id_fakultas) as $data) {
                array_push($data_report_fakultas, array(
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'NM_FAKULTAS' => $data['NM_FAKULTAS'],
                    'JUMLAH_MHS' => $this->get_jumlah_mhs($tgl_awal, $tgl_akhir, $data['ID_FAKULTAS'], '', $id_bank, ''),
                    'TOTAL_PEMBAYARAN' => $this->db->QueryToArray("
                            SELECT B.ID_BIAYA,B.NM_BIAYA,
                            (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                                THEN 0
                            ELSE 
                                SUM(PEM.BESAR_BIAYA)
                            END) BESAR_BIAYA,
                            (CASE WHEN SUM(PEM.DENDA_BIAYA) IS NULL 
                                THEN 0
                            ELSE 
                                SUM(PEM.DENDA_BIAYA)
                            END) DENDA_BIAYA
                            FROM BIAYA B
                        LEFT JOIN (
                              SELECT B.ID_BIAYA,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                              FROM MAHASISWA M
                              JOIN PEMBAYARAN PEM ON PEM.ID_MHS=M.ID_MHS
                              JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                              JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                              JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                              WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
                              AND PS.ID_FAKULTAS='{$data['ID_FAKULTAS']}' {$condition_bank} AND ID_BANK IS NOT NULL
                              GROUP BY B.ID_BIAYA
                              ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                        GROUP BY B.ID_BIAYA,B.NM_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        }
        return $data_report_fakultas;
    }

    private function get_jumlah_cmhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur) {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_jalur = $this->get_condition_jalur($id_jalur);
        $this->db->Query("
            SELECT COUNT(COUNT(PEM.ID_C_MHS)) JUMLAH_MHS FROM CALON_MAHASISWA_BARU CMB
            LEFT JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
            JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
            WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= '{$tgl_awal}' and TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' {$condition_akademik} {$condition_bank} {$condition_jalur} AND ID_BANK IS NOT NULL
            GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS");
        $data_jumlah_mhs = $this->db->FetchAssoc();
        $jumlah_mhs = $data_jumlah_mhs['JUMLAH_MHS'] == '' ? 0 : $data_jumlah_mhs['JUMLAH_MHS'];
        return $jumlah_mhs;
    }

    private function get_row_report_fakultas_cmhs($id_fakultas) {
        if ($id_fakultas != '') {
            return $this->db->QueryToArray("
                SELECT F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS,PS.ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.ID_JALUR,UPPER(JAL.NM_JALUR) NM_JALUR
                FROM CALON_MAHASISWA_BARU CMB 
                LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
                JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
                JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
                JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS = CMB.ID_C_MHS
                WHERE PEM.TGL_BAYAR IS NOT NULL AND F.ID_FAKULTAS='{$id_fakultas}' AND ID_BANK IS NOT NULL
                GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.ID_JALUR,JAL.NM_JALUR
                ORDER BY F.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.ID_JALUR,JAL.NM_JALUR");
        } else {
            return $this->db->QueryToArray("
                SELECT F.ID_FAKULTAS,UPPER(F.NM_FAKULTAS) NM_FAKULTAS
                FROM CALON_MAHASISWA_BARU CMB 
                LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
                JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
                JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS = CMB.ID_C_MHS
                WHERE PEM.TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL
                GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
                ORDER BY F.ID_FAKULTAS");
        }
    }

    function load_report_bank_cmhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi) {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        return $this->db->QueryToArray("
		SELECT B.ID_BANK,B.NM_BANK,
		  (CASE WHEN COUNT(PEM.ID_C_MHS) IS NULL
				THEN 0
			  ELSE
				COUNT(PEM.ID_C_MHS)
			  END) JUMLAH_MHS,
		  (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
			  THEN 0
			ELSE 
			   SUM(PEM.BESAR_BIAYA)
			END) BESAR_BIAYA
		FROM BANK B
		LEFT JOIN (
		  SELECT PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,PEM.ID_BANK,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                    FROM CALON_MAHASISWA_BARU CMB
		  JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
		  LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
		  WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= UPPER('{$tgl_awal}') AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}'
		  {$condition_akademik}
		  GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,PEM.ID_BANK
		  ) PEM ON PEM.ID_BANK = B.ID_BANK
		GROUP BY B.ID_BANK,B.NM_BANK
		ORDER BY B.NM_BANK");
    }

    function load_report_bank_cmhs_bidik_misi($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi) {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $this->db->Query("SELECT B.ID_BANK,B.NM_BANK,
		  (CASE WHEN COUNT(PEM.ID_C_MHS) IS NULL
				THEN 0
			  ELSE
				COUNT(PEM.ID_C_MHS)
			  END) JUMLAH_MHS,
		  (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
			  THEN 0
			ELSE 
			   SUM(PEM.BESAR_BIAYA)
			END) BESAR_BIAYA
		FROM BANK B
		RIGHT JOIN (
		  SELECT PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,PEM.ID_BANK,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                    FROM CALON_MAHASISWA_BARU CMB
		  JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
		  LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
		  WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= UPPER('{$tgl_awal}') AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}'
		  {$condition_akademik}
		  GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,PEM.ID_BANK
		  ) PEM ON PEM.ID_BANK = B.ID_BANK
                WHERE B.ID_BANK IS NULL  
		GROUP BY B.ID_BANK,B.NM_BANK
		ORDER BY B.NM_BANK");
        return $this->db->FetchAssoc();
    }

    function load_report_detail_cmhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur) {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_bank = $this->get_condition_bank($id_bank);
        $condition_jalur = $this->get_condition_jalur($id_jalur);
        return $this->db->QueryToArray("
		SELECT PS.ID_C_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,PS.ID_JENJANG,PS.NO_UJIAN,PS.NM_C_MHS,PS.NM_PROGRAM_STUDI,PS.NM_JALUR,PS.NM_JENJANG,PB.TGL_BAYAR,PB.BIAYA,PB.DENDA FROM
		  (SELECT CMB.ID_C_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,J.ID_JENJANG,CMB.NO_UJIAN,CMB.NM_C_MHS,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,J.NM_JENJANG 
		  FROM CALON_MAHASISWA_BARU CMB
		  LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
		  JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
		  JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
                  {$condition_jalur}  ) PS
		  ,
		  (SELECT ID_C_MHS,TGL_BAYAR,SUM(BESAR_BIAYA) BIAYA,SUM(DENDA_BIAYA) DENDA
		  FROM PEMBAYARAN_CMHS
		  WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= UPPER('{$tgl_awal}') and TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' {$condition_bank}
		  GROUP BY NO_TRANSAKSI,ID_C_MHS,TGL_BAYAR ) PB
		WHERE PS.ID_C_MHS = PB.ID_C_MHS {$condition_akademik}
		ORDER BY PS.NM_C_MHS");
    }

    function load_report_detail_cmhs_page($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $batas, $posisi) {
        $condition_akademik = $this->get_condition_akademik($id_fakultas, $id_prodi);
        $condition_bank = $this->get_condition_bank($id_bank);
        return $this->db->QueryToArray("
		SELECT * FROM ( SELECT PAGE.*, ROWNUM rnum FROM
		(SELECT PS.ID_C_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,PS.ID_JENJANG,PS.NO_UJIAN,PS.NM_C_MHS,PS.NM_PROGRAM_STUDI,PS.NM_JALUR,PS.NM_JENJANG,PB.TGL_BAYAR,PB.BIAYA,PB.DENDA FROM
		  (SELECT CMB.ID_C_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,J.ID_JENJANG,CMB.NO_UJIAN,CMB.NM_C_MHS,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,J.NM_JENJANG 
		  FROM CALON_MAHASISWA_BARU CMB
		  LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CMB.ID_PROGRAM_STUDI
		  LEFT JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
		  JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG) PS
		  ,
		  (SELECT ID_C_MHS,TGL_BAYAR,SUM(BESAR_BIAYA) BIAYA,SUM(DENDA_BIAYA) DENDA
		  FROM PEMBAYARAN_CMHS
		  WHERE TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') >= UPPER('{$tgl_awal}') and TO_CHAR(TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' {$condition_bank}
		  GROUP BY NO_TRANSAKSI,ID_C_MHS,TGL_BAYAR ) PB
		WHERE PS.ID_C_MHS = PB.ID_C_MHS {$condition_akademik}
		ORDER BY PS.NM_C_MHS) PAGE
		WHERE ROWNUM <= ({$batas}+{$posisi}))
		WHERE rnum >= ({$posisi}+1)");
    }

    function load_report_fakultas_cmhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur) {
        $condition_bank = $this->get_condition_bank($id_bank);
        $data_report_fakultas = array();
        if ($id_prodi != '') {
            foreach ($this->load_report_detail_cmhs($tgl_awal, $tgl_akhir, $id_fakultas, $id_prodi, $id_bank, $id_jalur) as $data) {
                array_push($data_report_fakultas, array(
                    'NM_C_MHS' => $data['NM_C_MHS'],
                    'NO_UJIAN' => $data['NO_UJIAN'],
                    'NM_PROGRAM_STUDI' => '( ' . $data['NM_JENJANG'] . ' ) ' . $data['NM_PROGRAM_STUDI'],
                    'TOTAL_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,
                            (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                                THEN 0
                            ELSE 
                                SUM(PEM.BESAR_BIAYA)
                            END) BESAR_BIAYA
                            FROM BIAYA B
                        LEFT JOIN (
                              SELECT B.ID_BIAYA,PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                              FROM CALON_MAHASISWA_BARU CMB
                              JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
                              JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
                              JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                              JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                              LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                              WHERE TO_CHAR(PEM.TGL_BAYAR,'DD-MON-YY') = UPPER('{$data['TGL_BAYAR']}') {$condition_bank} AND CMB.ID_C_MHS='{$data['ID_C_MHS']}'
                              GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,B.ID_BIAYA
                              ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                        GROUP BY B.ID_BIAYA,B.NM_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        }
        //saat report fakultas tertentu
        else if ($id_fakultas != '') {
            foreach ($this->get_row_report_fakultas_cmhs($id_fakultas) as $data) {
                array_push($data_report_fakultas, array(
                    'ID_PROGRAM_STUDI' => $data['ID_PROGRAM_STUDI'],
                    'NM_PROGRAM_STUDI' => $data['NM_PROGRAM_STUDI'],
                    'NM_JENJANG' => $data['NM_JENJANG'],
                    'NM_JALUR' => $data['NM_JALUR'],
                    'ID_JALUR' => $data['ID_JALUR'],
                    'JUMLAH_MHS' => $this->get_jumlah_cmhs($tgl_awal, $tgl_akhir, $data['ID_FAKULTAS'], $data['ID_PROGRAM_STUDI'], $id_bank, $data['ID_JALUR']),
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'TOTAL_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,
                            (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                                THEN 0
                            ELSE 
                                SUM(PEM.BESAR_BIAYA)
                            END) BESAR_BIAYA
                            FROM BIAYA B
                        LEFT JOIN (
                              SELECT B.ID_BIAYA,PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                              FROM CALON_MAHASISWA_BARU CMB
                              JOIN JALUR JAL ON JAL.ID_JALUR = CMB.ID_JALUR
                              JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
                              JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                              JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                              LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                              WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= UPPER('{$tgl_awal}') AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
                              AND PS.ID_PROGRAM_STUDI='{$data['ID_PROGRAM_STUDI']}' {$condition_bank} AND JAL.ID_JALUR='{$data['ID_JALUR']}'
                              GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,B.ID_BIAYA
                              ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                        GROUP BY B.ID_BIAYA,B.NM_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        }
        // saat report semua fakultas
        else {
            foreach ($this->get_row_report_fakultas_cmhs($id_fakultas) as $data) {
                array_push($data_report_fakultas, array(
                    'ID_FAKULTAS' => $data['ID_FAKULTAS'],
                    'NM_FAKULTAS' => $data['NM_FAKULTAS'],
                    'NM_JALUR' => $data['NM_JALUR'],
                    'JUMLAH_MHS' => $this->get_jumlah_cmhs($tgl_awal, $tgl_akhir, $data['ID_FAKULTAS'], '', $id_bank, $data['ID_JALUR']),
                    'TOTAL_PEMBAYARAN' => $this->db->QueryToArray("
                        SELECT B.ID_BIAYA,B.NM_BIAYA,
                            (CASE WHEN SUM(PEM.BESAR_BIAYA) IS NULL 
                                THEN 0
                            ELSE 
                                SUM(PEM.BESAR_BIAYA)
                            END) BESAR_BIAYA
                            FROM BIAYA B
                        LEFT JOIN (
                              SELECT B.ID_BIAYA,PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,SUM(PEM.BESAR_BIAYA) BESAR_BIAYA,SUM(PEM.DENDA_BIAYA) DENDA_BIAYA 
                              FROM CALON_MAHASISWA_BARU CMB
                              JOIN PEMBAYARAN_CMHS PEM ON PEM.ID_C_MHS=CMB.ID_C_MHS
                              JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                              JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                              LEFT JOIN PROGRAM_STUDI PS ON CMB.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                              WHERE TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') >= UPPER('{$tgl_awal}') AND TO_CHAR(PEM.TGL_BAYAR,'YYYY-MM-DD') <= '{$tgl_akhir}' 
                              AND PS.ID_FAKULTAS='{$data['ID_FAKULTAS']}' {$condition_bank}
                              GROUP BY PEM.NO_TRANSAKSI,PEM.TGL_BAYAR,PEM.ID_C_MHS,B.ID_BIAYA
                              ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                        GROUP BY B.ID_BIAYA,B.NM_BIAYA
                        ORDER BY B.NM_BIAYA")
                ));
            }
        }
        return $data_report_fakultas;
    }

    function load_data_status_pembayaran($fakultas, $prodi, $semester, $status, $jalur) {
        $q_fakultas = $fakultas != '' ? "AND PS.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI='{$prodi}'" : "";
        $q_jalur = $jalur != '' ? "AND JM.ID_JALUR='{$jalur}'" : "";
        
        return $this->db->QueryToArray("
        SELECT M.NIM_MHS,P.NM_PENGGUNA,SUM(PEM.BESAR_BIAYA) TOTAL_BIAYA,NM_JENJANG,PS.NM_PROGRAM_STUDI,SP.NAMA_STATUS
        ,PEM.KETERANGAN KETERANGAN_PEMBAYARAN,SSP.KETERANGAN KETERANGAN_STATUS,SSP.TGL_JATUH_TEMPO,JAL.NM_JALUR,SPENG.NM_STATUS_PENGGUNA
        FROM MAHASISWA M
        LEFT JOIN STATUS_PENGGUNA SPENG ON SPENG.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
        LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR_AKTIF=1
        LEFT JOIN JALUR JAL ON JM.ID_JALUR = JAL.ID_JALUR
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        JOIN PEMBAYARAN PEM ON PEM.ID_MHS= M.ID_MHS
        JOIN SEMESTER S ON S.ID_SEMESTER= PEM.ID_SEMESTER
        LEFT JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN=PEM.ID_STATUS_PEMBAYARAN
        LEFT JOIN SEJARAH_STATUS_BAYAR SSP ON SSP.ID_MHS=PEM.ID_MHS AND SSP.ID_SEMESTER = PEM.ID_SEMESTER AND SSP.IS_AKTIF=1 
        WHERE SP.ID_STATUS_PEMBAYARAN='{$status}' AND S.ID_SEMESTER='{$semester}' {$q_fakultas} {$q_prodi} {$q_jalur}
        GROUP BY M.NIM_MHS,P.NM_PENGGUNA,SP.NAMA_STATUS,PEM.KETERANGAN,SSP.KETERANGAN,SSP.TGL_JATUH_TEMPO,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,SPENG.NM_STATUS_PENGGUNA
        ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,M.NIM_MHS");
    }

    function load_rekap_pembayaran($id_fakultas, $id_semester) {
        $kolom_status = "";
        foreach ($this->db->QueryToArray("SELECT * FROM STATUS_PEMBAYARAN ORDER BY ID_STATUS_PEMBAYARAN") as $data_status) {
            $nama_status = strtoupper(str_replace(' ', '_', $data_status['NAMA_STATUS']));
            $kolom_status.="
            ,SUM(
                CASE WHEN ID_STATUS_PEMBAYARAN={$data_status['ID_STATUS_PEMBAYARAN']}
                    THEN 1
                    ELSE 0
                END
            ) {$nama_status}";
        }
        if ($id_fakultas != '') {
            return $this->db->QueryToArray("
            SELECT J.NM_JENJANG,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI{$kolom_status},COUNT(DISTINCT(PMK.ID_MHS)) JUMLAH_KRS
            FROM (SELECT ID_MHS,TGL_BAYAR,NO_TRANSAKSI,ID_BANK,ID_BANK_VIA,KETERANGAN,ID_STATUS_PEMBAYARAN
            FROM PEMBAYARAN WHERE ID_SEMESTER='{$id_semester}'
            GROUP BY ID_MHS,TGL_BAYAR,NO_TRANSAKSI,ID_BANK,ID_BANK_VIA,KETERANGAN,ID_STATUS_PEMBAYARAN
            ORDER BY ID_MHS) PEM
            JOIN MAHASISWA M ON M.ID_MHS= PEM.ID_MHS
			LEFT JOIN (SELECT DISTINCT(ID_MHS) ID_MHS FROM PENGAMBILAN_MK WHERE ID_SEMESTER='{$id_semester}') PMK ON PMK.ID_MHS = PEM.ID_MHS
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI AND PS.ID_FAKULTAS='{$id_fakultas}'
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            GROUP BY J.NM_JENJANG,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
        } else {
		
            return $this->db->QueryToArray("
            SELECT F.ID_FAKULTAS,F.NM_FAKULTAS{$kolom_status},COUNT(DISTINCT(PMK.ID_MHS)) JUMLAH_KRS
            FROM (SELECT ID_MHS,TGL_BAYAR,NO_TRANSAKSI,ID_BANK,ID_BANK_VIA,KETERANGAN,ID_STATUS_PEMBAYARAN
            FROM PEMBAYARAN WHERE ID_SEMESTER='{$id_semester}'
            GROUP BY ID_MHS,TGL_BAYAR,NO_TRANSAKSI,ID_BANK,ID_BANK_VIA,KETERANGAN,ID_STATUS_PEMBAYARAN
            ORDER BY ID_MHS) PEM
            JOIN MAHASISWA M ON M.ID_MHS= PEM.ID_MHS
			LEFT JOIN (SELECT DISTINCT(ID_MHS) ID_MHS FROM PENGAMBILAN_MK WHERE ID_SEMESTER='{$id_semester}') PMK ON PMK.ID_MHS = PEM.ID_MHS
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
            ORDER BY F.ID_FAKULTAS");
			
        }
    }

}

?>
