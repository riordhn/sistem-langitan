<?php

//membuat class organisasi
class organisasi {

    public $db;
    public $id_dosen;

    function __construct($db, $id_dosen) {
        $this->db = $db;
        $this->id_dosen = $id_dosen;
    }

    function load_organisasi(){
        return $this->db->QueryToArray("SELECT * FROM DOSEN_ORGANISASI WHERE ID_DOSEN= '{$this->id_dosen}'");
    }
	
	function get_organisasi($id_organisasi){
		$this->db->Query("SELECT * FROM DOSEN_ORGANISASI WHERE ID_DOSEN_ORGANISASI='{$id_organisasi}'");
		return $this->db->FetchAssoc();
	}

    function update($id_organisasi, $organisasi, $jabatan, $mulai, $selesai, $tingkat) {
        $this->db->Query("
			UPDATE DOSEN_ORGANISASI 
			SET 
				NM_DOSEN_ORG='{$organisasi}',
				JABATAN_DOSEN_ORG='{$jabatan}',
				MULAI_DOSEN_ORG='{$mulai}',
				SELESAI_DOSEN_ORG='{$selesai}',
				TINGKAT_DOSEN_ORG = '{$tingkat}' 
			WHERE ID_DOSEN_ORGANISASI ='{$id_organisasi}'");
	}

    function insert($organisasi, $jabatan, $mulai, $selesai, $tingkat) {
        $this->db->Query("
			INSERT INTO DOSEN_ORGANISASI 
				(ID_DOSEN,NM_DOSEN_ORG,JABATAN_DOSEN_ORG,MULAI_DOSEN_ORG,SELESAI_DOSEN_ORG, TINGKAT_DOSEN_ORG) 
			VALUES 
				('{$this->id_dosen}','{$organisasi}','{$jabatan}','{$mulai}','{$selesai}','{$tingkat}')");
	}

	function delete($id_organisasi) {
		echo "DELETE DOSEN_ORGANISASI WHERE ID_DOSEN_ORGANISASI ='{$id_organisasi}'";
        $this->db->Query("DELETE DOSEN_ORGANISASI WHERE ID_DOSEN_ORGANISASI ='{$id_organisasi}'");
	}
}

?>
