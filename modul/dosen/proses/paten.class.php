<?php

// membuat class training
class training {

    public $db;
    public $id_dosen;

    //membuat constructor yang berasal dari class user dan database
    function __construct($db, $id) {
        $this->db = $db;
        $this->id_dosen = $id;
    }

    //Fungsi untuk mengambil data dari database dan disimpan ke dalam sebuah variabel array
    function load_training() {
        return $this->db->QueryToArray("SELECT * FROM DOSEN_TRAINING WHERE ID_DOSEN = '{$this->id_dosen}'");
    }
	
	function get_training($id_training){
		$this->db->Query("SELECT * FROM DOSEN_TRAINING WHERE ID_DOSEN_TRAINING='{$id_training}'");
		return $this->db->FetchAssoc();
	}

    //fungsi untuk update data 
    function update($id_training, $nama, $jenis, $tahun, $instansi, $tingkat, $peran) {

		$this->db->Query("UPDATE DOSEN_TRAINING 
			SET 
				NM_DOS_TRAINING ='{$nama}',
				JENIS_DOS_TRAINING='{$jenis}',
				THN_DOS_TRAINING='{$tahun}',
				INSTANSI_DOS_TRAINING='{$instansi}',
				PERAN_DOS_TRAINING = '{$peran}',
				TINGKAT_DOS_TRAINING = '{$tingkat}'
			WHERE ID_DOSEN_TRAINING = '{$id_training}'");
	}

    function insert($nama, $jenis, $tahun, $instansi, $tingkat, $peran) {
        $this->db->Query("INSERT INTO DOSEN_TRAINING 
			(ID_DOSEN,NM_DOS_TRAINING,JENIS_DOS_TRAINING,THN_DOS_TRAINING,INSTANSI_DOS_TRAINING, PERAN_DOS_TRAINING, TINGKAT_DOS_TRAINING) 
		VALUES 
			('{$this->id_dosen}','{$nama}','{$jenis}','{$tahun}','{$instansi}','{$tingkat}','{$peran}')");
	}
	
	function delete_training($id_training){
		$this->db->Query("DELETE FROM DOSEN_TRAINING WHERE ID_DOSEN_TRAINING='{$id_training}'");
	}

}

?>
