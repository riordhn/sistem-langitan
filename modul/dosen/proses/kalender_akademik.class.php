<?php

class kalender_akademik {

    public $db;
    public $id_fakultas;
    public $id_perguruan_tinggi;
    public $data_kalender_universitas = array();
    public $data_kalender_fakultas = array();

    function __construct($db, $id_fakultas, $id_perguruan_tinggi) {
        $this->db = $db;
        $this->id_fakultas = $id_fakultas;
        $this->id_perguruan_tinggi = $id_perguruan_tinggi;
    }

    function load_kalender_universitas() {
        return $this->db->QueryToArray("
		SELECT S.TAHUN_AJARAN,S.NM_SEMESTER,JKS.TGL_MULAI_JKS AS TANGGAL_MULAI,JKS.TGL_SELESAI_JKS AS TANGGAL_SELESAI,K.NM_KEGIATAN AS KEGIATAN,K.DESKRIPSI_KEGIATAN AS DESKRIPSI_KEGIATAN 
                FROM JADWAL_KEGIATAN_SEMESTER JKS
                JOIN SEMESTER S ON S.ID_SEMESTER = JKS.ID_SEMESTER
                JOIN KEGIATAN K ON K.ID_KEGIATAN = JKS.ID_KEGIATAN
                WHERE S.STATUS_AKTIF_SEMESTER='True' AND K.ID_PERGURUAN_TINGGI = '{$this->id_perguruan_tinggi}'");
    }

    function load_kalender_fakultas() {
        return $this->db->QueryToArray("
		SELECT S.TAHUN_AJARAN,S.NM_SEMESTER,JSF.TGL_MULAI_JSF AS TANGGAL_MULAI, JSF.TGL_SELESAI_JSF AS TANGGAL_SELESAI,K.NM_KEGIATAN AS KEGIATAN,K.DESKRIPSI_KEGIATAN AS DESKRIPSI_KEGIATAN 
                FROM JADWAL_SEMESTER_FAKULTAS JSF
                JOIN KEGIATAN K ON K.ID_KEGIATAN = JSF.ID_KEGIATAN
                JOIN SEMESTER S ON S.ID_SEMESTER = JSF.ID_SEMESTER 
                JOIN FAKULTAS F ON F. ID_FAKULTAS = JSF.ID_FAKULTAS
                WHERE F.ID_FAKULTAS = '{$this->id_fakultas}' AND S.STATUS_AKTIF_SEMESTER = 'True'
                ORDER BY TANGGAL_MULAI");
    }

}

?>
