<?php

class Journal
{
    private $db;
    
    function __construct($db)
    {
        $this->db = $db;
    }
    
    function SelectAll($id_dosen)
    {
        return $this->db->QueryToArray("
            select id_journal, judul, nm_journal_media from journal j
            left join journal_media jm on jm.id_journal_media = j.id_journal_media
            where id_dosen = {$id_dosen}");
    }
    
    function InsertJournal($id_dosen, &$post)
    {
        $this->db->Parse("
            insert into journal
                ( id_dosen, judul, abstrak, keywords, id_journal_media, volume, nomer) values 
                (:id_dosen,:judul,:abstrak,:keywords,:id_journal_media,:volume,:nomer)");
        
        $this->db->BindByName(':id_dosen', $id_dosen);
        $this->db->BindByName(':judul', $post['judul']);
        $this->db->BindByName(':abstrak', $post['abstrak']);
        $this->db->BindByName(':keywords', $post['keywords']);
        $this->db->BindByName(':id_journal_media', $post['id_journal_media']);
        $this->db->BindByName(':volume', $post['volume']);
        $this->db->BindByName(':nomer', $post['nomer']);
        return $this->db->Execute();
    }
    
    function UpdateJournal($id_dosen, $id_journal, &$post)
    {
        $this->db->Parse("
            update journal set
                judul       = :judul,
                abstrak     = :abstrak,
                keywords    = :keywords,
                id_journal_media = :id_journal_media,
                volume      = :volume,
                nomer       = :nomer
            where id_journal = {$id_journal} and id_dosen = {$id_dosen}");
        $this->db->BindByName(':judul', $post['judul']);
        $this->db->BindByName(':abstrak', $post['abstrak']);
        $this->db->BindByName(':keywords', $post['keywords']);
        $this->db->BindByName(':id_journal_media', $post['id_journal_media']);
        $this->db->BindByName(':volume', $post['volume']);
        $this->db->BindByName(':nomer', $post['nomer']);
        return $this->db->Execute();
    }
    
    function Single($id_dosen, $id_journal)
    {
        $rows = $this->db->QueryToArray("select * from journal where id_journal = {$id_journal} and id_dosen = {$id_dosen}");
        
        return $rows[0];
    }
    
    function GetListJournalMedia()
    {
        return $this->db->QueryToArray("select * from journal_media order by nm_journal_media");
    }
}
?>
