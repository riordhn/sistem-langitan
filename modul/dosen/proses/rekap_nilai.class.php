<?php

class rekap_nilai {

    public $db;
    public $id_dosen;
    public $id_fakultas;

    function __construct($db, $id_dosen, $id_fakultas) {
        $this->db = $db;
        $this->id_dosen = $id_dosen;
        $this->id_fakultas = $id_fakultas;
    }
    
    function load_rekap_nilai(){
        return $this->db->QueryToArray("
            SELECT 
                D.NIP_DOSEN,P.NM_PENGGUNA,D.TLP_DOSEN,('('||J.NM_JENJANG||') '||INITCAP(PS.NM_PROGRAM_STUDI)) NM_PROGRAM_STUDI,KMK.ID_KELAS_MK
,               (MK.NM_MATA_KULIAH||'<br/> Kelas ( '||NVL(K1.NMKELAS,K2.NAMA_KELAS)||' )') MATA_AJAR,SUM(PMK.NILAI_ANGKA) AS NILAI,COUNT(PMK.ID_MHS) AS JUMLAH_MHS
            FROM PENGAMPU_MK PENGMK
            LEFT JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
            LEFT JOIN PENGAMBILAN_MK PMK ON PMK.ID_KELAS_MK = KMK.ID_KELAS_MK
            LEFT JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
            LEFT JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
            LEFT JOIN FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
            LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = KMK.ID_PROGRAM_STUDI OR K1.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            LEFT JOIN SEMESTER S ON KMK.ID_SEMESTER = S.ID_SEMESTER
            JOIN DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
            JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
            WHERE ID_FAKULTAS='{$this->id_fakultas}' AND S.STATUS_AKTIF_SEMESTER='True' AND PENGMK.PJMK_PENGAMPU_MK=1
            GROUP BY NIP_DOSEN,NM_PENGGUNA,TLP_DOSEN,('('||J.NM_JENJANG||') '||INITCAP(PS.NM_PROGRAM_STUDI)),KMK.ID_KELAS_MK,
                (MK.NM_MATA_KULIAH||'<br/> Kelas ( '||NVL(K1.NMKELAS,K2.NAMA_KELAS)||' )')
            ORDER BY NIP_DOSEN");
    }

}

?>
