<?php

class komponen_nilai {

    public $db;
    public $id_dosen;

    function __construct($db, $id_dosen) {
        $this->db = $db;
        $this->id_dosen = $id_dosen;
    }

    function load_komponen_mk($id_kelas_mk) {
        return $this->db->QueryToArray("
			SELECT * FROM KOMPONEN_MK WHERE ID_KELAS_MK='{$id_kelas_mk}' ORDER BY URUTAN_KOMPONEN_MK
        ");
    }

    function load_komponen_mk_by_id($id_komponen) {
        $this->db->Query("SELECT * FROM KOMPONEN_MK WHERE ID_KOMPONEN_MK='{$id_komponen}'");
        return $this->db->FetchAssoc();
    }

    function load_kelas_mata_kuliah() {
        return $this->db->QueryToArray("
			SELECT 
			  KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH,NVL(K1.NMKELAS,K2.NAMA_KELAS) NM_KELAS,INITCAP(PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI
			FROM PENGAMPU_MK PENGMK
			JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
			JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
			JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
			LEFT JOIN FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
			LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = KMK.ID_PROGRAM_STUDI OR K1.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
			LEFT JOIN SEMESTER S ON KMK.ID_SEMESTER = S.ID_SEMESTER
			JOIN DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
			WHERE D.ID_DOSEN='{$this->id_dosen}' AND S.STATUS_AKTIF_SEMESTER='True'
			ORDER BY NM_MATA_KULIAH,NM_KELAS
		");
    }

    function update_komponen($id_komponen, $urutan, $nm_komponen, $persentase) {
        $persentase = str_replace('%', '', $persentase);
        $this->db->Query("
		UPDATE KOMPONEN_MK
			SET PERSENTASE_KOMPONEN_MK='{$persentase}' ,
			URUTAN_KOMPONEN_MK ='{$urutan}',
			NM_KOMPONEN_MK='{$nm_komponen}'
			WHERE ID_KOMPONEN_MK ='{$id_komponen}'");
    }

    function insert_komponen($komponen, $persentase, $urutan, $id_kelas_mk) {
        $persentase = str_replace('%', '', $persentase);
        $this->db->Query("
		INSERT INTO KOMPONEN_MK 
			(ID_KELAS_MK,NM_KOMPONEN_MK,PERSENTASE_KOMPONEN_MK,URUTAN_KOMPONEN_MK) 
		VALUES 
			('{$id_kelas_mk}','{$komponen}','{$persentase}','{$urutan}')");
    }

    function delete_komponen($id_komponen) {
        $this->db->Query("UPDATE PENGAMBILAN_MK SET NILAI_ANGKA=NULL,NILAI_HURUF=NULL WHERE ID_KELAS_MK=(SELECT ID_KELAS_MK FROM KOMPONEN_MK WHERE ID_KOMPONEN_MK='{$id_komponen}')");
        $this->db->Query("DELETE FROM KOMPONEN_MK WHERE ID_KOMPONEN_MK='{$id_komponen}'");
    }

    function get_id_kurikulum_mk($id_kelas_mk) {
        $this->db->Query("SELECT ID_KURIKULUM_MK FROM KELAS_MK WHERE ID_KELAS_MK='{$id_kelas_mk}'");
        $data_kurikulum_mk = $this->db->FetchAssoc();
        return $data_kurikulum_mk['ID_KURIKULUM_MK'];
    }

    function cek_kelas_pararel($id_kelas_mk) {
        $id_kurikulum_mk = $this->get_id_kurikulum_mk($id_kelas_mk);
        $this->db->Query("
            SELECT COUNT(ID_KELAS_MK) JUMLAH_KELAS FROM KELAS_MK WHERE ID_KURIKULUM_MK='{$id_kurikulum_mk}'");
        $data_jumlah_kelas = $this->db->FetchAssoc();
        return $data_jumlah_kelas['JUMLAH_KELAS'];
    }

    function load_kelas_paralel($id_kelas_mk) {
        $id_kurikulum_mk = $this->get_id_kurikulum_mk($id_kelas_mk);
        return $this->db->QueryToArray("
            SELECT 
                KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH,
                --NVL(K1.NMKELAS,K2.NAMA_KELAS) NM_KELAS,
                K2.NAMA_KELAS AS NM_KELAS,
                INITCAP(PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI
            FROM KELAS_MK KMK
            JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
            JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
            --LEFT JOIN FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
            LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = KMK.ID_PROGRAM_STUDI --OR K1.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            LEFT JOIN SEMESTER S ON KMK.ID_SEMESTER = S.ID_SEMESTER
            WHERE S.STATUS_AKTIF_SEMESTER='True' AND KMK.ID_KURIKULUM_MK='{$id_kurikulum_mk}'
            ORDER BY NM_MATA_KULIAH,NM_KELAS
        ");
    }

    // Komponen UP

    function insert_komponen_up($komponen, $persentase, $id_kelas_mk, $id_semester) {
        $persentase = str_replace('%', '', $persentase);
        $this->db->Query("
		INSERT INTO KOMPONEN_UP 
			(ID_KELAS_MK,NM_KOMPONEN_UP,PROSENTASE_KOMPONEN) 
		VALUES 
			('{$id_kelas_mk}','{$komponen}','{$persentase}')");
        $id_komponen_up = $this->db->QuerySingle("SELECT ID_KOMPONEN_UP FROM KOMPONEN_UP WHERE ID_KELAS_MK='{$id_kelas_mk}' AND ROWNUM=1 ORDER BY ID_KOMPONEN_UP DESC");
        $this->db->Query("
            INSERT INTO NILAI_KOMPONEN_UP 
                (ID_PENGAMBILAN_MK,ID_KOMPONEN_UP,BESAR_NILAI)
            SELECT ID_PENGAMBILAN_MK, '{$id_komponen_up}' ID_KOMPONEN_UP,'0' BESAR_NILAI 
            FROM PENGAMBILAN_MK 
            WHERE ID_KELAS_MK='{$id_kelas_mk}' AND ID_SEMESTER='{$id_semester}'
            ");
    }

    function update_komponen_up($id_komponen, $nm_komponen, $persentase) {
        $persentase = str_replace('%', '', $persentase);
        $this->db->Query("
		UPDATE KOMPONEN_UP
                SET PROSENTASE_KOMPONEN='{$persentase}' ,
                NM_KOMPONEN_UP='{$nm_komponen}'
                WHERE ID_KOMPONEN_UP ='{$id_komponen}'");
    }

    function delete_komponen_up($id_komponen) {
        $this->db->Query("DELETE FROM NILAI_KOMPONEN_UP WHERE ID_KOMPONEN_UP='{$id_komponen}'");
        $this->db->Query("DELETE FROM KOMPONEN_UP WHERE ID_KOMPONEN_UP='{$id_komponen}'");
    }

    function load_komponen_mk_up($id_kelas_mk) {
        return $this->db->QueryToArray("SELECT * FROM KOMPONEN_UP WHERE ID_KELAS_MK='{$id_kelas_mk}' ORDER BY NM_KOMPONEN_UP
        ");
    }

    function load_komponen_mk_up_by_id($id_komponen) {
        $this->db->Query("SELECT * FROM KOMPONEN_UP WHERE ID_KOMPONEN_UP='{$id_komponen}'");
        return $this->db->FetchAssoc();
    }

}

?>
