<?php

//==================================
require_once 'conf.php';

class ewmp {

    public $db;
    public $data_ewmp = array();

    function ewmp($db) {
        $this->db = $db;
    }

    function load_data($id_dosen) {
        $this->db->open();
        $this->db->query("select * from dosen_ewmp where id_dosen='" . $id_dosen . "' order by id_dosen_ewmp asc");
        $xyz = 0;
        while ($tmp = $this->db->fetcharray()) {
            $xyz++;
            array_push($this->data_ewmp, array(
                'no_urut' => $xyz,
                'id_dosen_ewmp' => $tmp['ID_DOSEN_EWMP'],
                'id_dosen' => $tmp['ID_DOSEN_EWMP'],
                'kegiatan' => $tmp['KEGIATAN_DOSEN_EWMP'],
                'beban' => $tmp['BEBAN_DOSEN_EWMP']
            ));
        }
        $this->db->close();
    }

    function cetak_ewmp($id_dosen) {
        $tmp = $id_dosen;
        $this->load_data($id_dosen);
        $tmp = "";
        $total = "";
        $tmp = $tmp . "<table width=70% class='ui-widget'>";
        $tmp = $tmp . "<tr class='ui-widget-header'><th colspan=3 class='header-coloumn'><h2>Ringkasan EWMP Dosen</h2></th></tr>";
        $tmp = $tmp . "<tr class='ui-widget-content'><td width=5%><h3>No.</td></h3><td><h3>Kegiatan</td></h3><td><h3>SKS</td></h3></tr>";
        foreach ($this->data_ewmp as $data) {
            $tmp = $tmp . "<tr class='ui-widget-content'>";
            $tmp = $tmp . "<td>" . $data['no_urut'] . "</td>";
            $tmp = $tmp . "<td>" . $data['kegiatan'] . "</td>";
            $tmp = $tmp . "<td>" . $data['beban'] . "</td>";
            $tmp = $tmp . "</tr>";
            $total = $total + $data['beban'];
        }
        $tmp = $tmp . "<tr class='ui-widget-content'><td colspan=1>&nbsp;</td><td><h3>Total</h3></td><td><h3>" . $total . " SKS</h3></td></tr>";
        $tmp = $tmp . "</table>";

        return $tmp;
    }

}

//==================================?>