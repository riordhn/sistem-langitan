	<?php

// membuat class publikasi
class publikasi {

    public $db;
    public $id_dosen;
    
    //constructor class publikasi yang berasal dari class user dan class database
    function __construct($db, $id) {
        $this->db = $db;
        $this->id_dosen = $id;
    }

    //load data dosen publikasi dari database
    function load_publikasi() {
        return $this->db->QueryToArray("SELECT * FROM DOSEN_PUBLIKASI WHERE ID_DOSEN = '{$this->id_dosen}'");
	}
	
	function get_publikasi($id_publikasi){
		$this->db->Query("SELECT * FROM DOSEN_PUBLIKASI WHERE ID_DOSEN_PUBLIKASI='{$id_publikasi}'");
		return $this->db->FetchAssoc();
	}

    //update data publikasi dari databasi
    function update_publikasi($id, $jenis, $judul,$penerbit, $pengarang,$media,$tahun,$peran,$edisi,$lembaga,$tingkat) {
        $this->db->Parse("
			UPDATE DOSEN_PUBLIKASI SET 
				JENIS_DOSEN_PUBLIKASI =:jenis,
				JUDUL_DOSEN_PUBLIKASI =:judul,
				PENGARANG_DOSEN_PUBLIKASI =:pengarang,
				PENERBIT_DOSEN_PUBLIKASI =:penerbit,
				MEDIA_DOSEN_PUBLIKASI =:media,
				THN_DOSEN_PUBLIKASI =:tahun,
				PERAN_DOSEN_PUBLIKASI =:peran,
				TINGKAT_DOSEN_PUBLIKASI =:tingkat,
				EDISI_DOSEN_PUBLIKASI =:edisi,
				LEMBAGA_DOSEN_PUBLIKASI =:lembaga
			WHERE ID_DOSEN_PUBLIKASI=:id");
		$this->db->BindByName(':jenis',$jenis);
		$this->db->BindByName(':judul',$judul);
		$this->db->BindByName(':pengarang',$penerbit);
		$this->db->BindByName(':penerbit',$pengarang);
		$this->db->BindByName(':media',$media);
		$this->db->BindByName(':tahun',$tahun);
		$this->db->BindByName(':id',$id);
		$this->db->BindByName(':peran',$peran);
		$this->db->BindByName(':edisi',$edisi);
		$this->db->BindByName(':lembaga',$lembaga);
		$this->db->BindByName(':tingkat',$tingkat);
		$this->db->Execute();
    }

    //insert data publikasi 
    function insert_publikasi($jenis, $judul,$penerbit, $pengarang,$media,$tahun,$peran,$edisi,$lembaga,$tingkat) {
	$this->db->Parse("
		INSERT INTO DOSEN_PUBLIKASI (
			ID_DOSEN,
			JENIS_DOSEN_PUBLIKASI,
			JUDUL_DOSEN_PUBLIKASI,
			PENERBIT_DOSEN_PUBLIKASI,
			PENGARANG_DOSEN_PUBLIKASI,
			MEDIA_DOSEN_PUBLIKASI,
			THN_DOSEN_PUBLIKASI,
			PERAN_DOSEN_PUBLIKASI,
			TINGKAT_DOSEN_PUBLIKASI,
			EDISI_DOSEN_PUBLIKASI,
			LEMBAGA_DOSEN_PUBLIKASI)
			VALUES 
			(:id_dosen,
			:jenis,
			:judul,
			:penerbit,
			:pengarang,
			:media,
			:tahun,
			:peran,
			:tingkat,
			:edisi,
			:lembaga
		)");
        $this->db->BindByName(':id_dosen',$this->id_dosen);
		$this->db->BindByName(':jenis',$jenis);
		$this->db->BindByName(':judul',$judul);
		$this->db->BindByName(':penerbit',$penerbit);
		$this->db->BindByName(':pengarang',$pengarang);
		$this->db->BindByName(':media',$media);
		$this->db->BindByName(':tahun',$tahun);
		$this->db->BindByName(':peran',$peran);
		$this->db->BindByName(':edisi',$edisi);
		$this->db->BindByName(':lembaga',$lembaga);
		$this->db->BindByName(':tingkat',$tingkat);
		$this->db->Execute();
    }
	
	function delete_publikasi($id_publikasi){
		$this->db->Query("DELETE FROM DOSEN_PUBLIKASI WHERE ID_DOSEN_PUBLIKASI='{$id_publikasi}'");
	}

}

?>
