<?php

//membuat class info dosen 
class info_dosen {

    public $db;
    public $id_pengguna;

    function __construct($db, $id_pengguna) {
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
    }

    function get_kota() {
        $data_kota = array();
        $provinsi = $this->db->QueryToArray("SELECT * FROM PROVINSI WHERE ID_NEGARA=114 ORDER BY NM_PROVINSI");
        foreach ($provinsi as $data) {
            array_push($data_kota, array(
                'nama' => $data['NM_PROVINSI'],
                'kota' => $this->db->QueryToArray("SELECT * FROM KOTA WHERE ID_PROVINSI='{$data['ID_PROVINSI']}' ORDER BY NM_KOTA, TIPE_DATI2")
            ));
        }
        return $data_kota;
    }

    function get_agama() {
        return $this->db->QueryToArray("SELECT * FROM AGAMA ORDER BY ID_AGAMA");
    }

    function get_biodata() {

        $tmp_nim = $this->db->QueryToArray("select NIP_DOSEN from DOSEN where id_pengguna='" . $this->id_pengguna . "'");
		foreach ($tmp_nim as $data) {
			$nip3 = $data['NIP_DOSEN'];
		}
        
        /* INTEGRASI BLOG (TAMBAHAN DARI MAS BAMBANG) */
        /**
         * DISABLE
        $DBhost = "10.0.110.1";
        $DBuser = "lihat_blogmhs";
        $DBpass = "lihat_blogmhs";
        $DBName = "dbwebmhs";
        mysql_connect($DBhost, $DBuser, $DBpass) or die("<i>Error Connection Database...</i>");
        mysql_select_db("$DBName") or die("<i>Wrong Database...</i>");

        $mhs_blog = "";
        $q = mysql_query("select subdomainMhs from tb_mahasiswa where nim='" . $nip3 . "'");
        while ($r = mysql_fetch_row($q)) {
            $mhs_blog = $r[0] . '.web.unair.ac.id';
        }
        if (strlen($mhs_blog) == 0) {
            $mhs_blog = "Registrasi Blog";
        }
        */

        $in_jam = date("H");
        $in_menit = date("i");
        $in_detik = date("s");
        $waktu = $in_jam . $in_menit . $in_detik;
        $ipnya = $_SERVER["REMOTE_ADDR"];
        $ipnya_tmp = explode(".", $ipnya);

        $kode = md5($ipnya_tmp[0] . $ipnya_tmp[3] . "bam" . $nip3 . "bang" . $waktu . $ipnya_tmp[2] . $ipnya_tmp[1]);
        $kode2 = substr($kode, 0, 2) . $in_jam . substr($kode, 2, 14) . $in_menit . substr($kode, 16, 8) . $in_detik . substr($kode, 24);


        $link_blog = "http://web.unair.ac.id/checkLogin.php?loginid=" . $nip3 . "&kode=" . $kode2;



        $provinsi = $this->db->QueryToArray("SELECT EMAIL_PENGGUNA FROM PENGGUNA WHERE ID_PENGGUNA='$this->id_pengguna'");
        foreach ($provinsi as $data) {
            $nama_email = $data['EMAIL_PENGGUNA'];
        }
        $link_email = substr($nama_email, strpos($nama_email, '@', 1) + 1);


        /* AMBIL DATA BIODATA */
        $this->db->Query("
        SELECT P.USERNAME,P.EMAIL_JABATAN,P.GELAR_DEPAN,P.GELAR_BELAKANG,
            P.NM_PENGGUNA AS NAMA,TO_CHAR(P.TGL_LAHIR_PENGGUNA,'DD / MONTH/ YYYY') AS TGL_LAHIR,P.KELAMIN_PENGGUNA AS KELAMIN,P.EMAIL_PENGGUNA AS EMAIL
            , '{$mhs_blog}' as BLOG_PENGGUNA,AG.NM_AGAMA AS AGAMA, P.ID_KOTA_LAHIR,D.NIP_DOSEN AS NIP,F.NM_FAKULTAS AS FAKULTAS, PR.NM_PROGRAM_STUDI AS PRODI,
            D.ALAMAT_RUMAH_DOSEN AS ALAMAT_RUMAH, D.ALAMAT_KANTOR_DOSEN AS ALAMAT_KANTOR,D.TLP_DOSEN AS TELP,D.MOBILE_DOSEN AS MOBILE,D.THN_LULUS_DOSEN AS TAHUN_LULUS,
            R.NM_ROLE AS JENIS_PEGAWAI,AG.ID_AGAMA,D.BIDANG_KEAHLIAN_DOSEN, '{$link_blog}' AS LINK_BLOG,SG.*,SS.*,SF.*,SP.*,
            P.PASSWORD_PENGGUNA, '{$link_email}' AS LINKEMAIL,K.TIPE_DATI2||' '||K.NM_KOTA AS KOTA_LAHIR,G.NM_GOLONGAN,JF.NM_JABATAN_FUNGSIONAL,JS.NM_JABATAN_STRUKTURAL,PA.NAMA_PENDIDIKAN_AKHIR
        FROM PENGGUNA P 
        JOIN DOSEN D ON P.ID_PENGGUNA = D.ID_PENGGUNA
        /*JABATAN GOLONGAN */
        LEFT JOIN (
          SELECT S.ID_GOLONGAN,S.ID_PENGGUNA,TO_CHAR(S.TMT_SEJARAH_GOLONGAN ,'DD-MONTH-YYYY') TMT_GOL, ROW_NUMBER() OVER (PARTITION BY ID_PENGGUNA ORDER BY TGL_SK_SEJARAH_GOLONGAN DESC) R 
          FROM SEJARAH_GOLONGAN S
        ) SG ON SG.ID_PENGGUNA=P.ID_PENGGUNA AND SG.R=1
        LEFT JOIN GOLONGAN G ON G.ID_GOLONGAN=SG.ID_GOLONGAN
        
        /*JABATAN FUNGSIONAL */
        LEFT JOIN (
          SELECT S.ID_JABATAN_FUNGSIONAL,S.ID_PENGGUNA,TO_CHAR(S.TMT_SEJ_JAB_FUNGSIONAL,'DD-MONTH-YYYY') TMT_FUNG, ROW_NUMBER() OVER (PARTITION BY ID_PENGGUNA ORDER BY TGL_SK_SEJ_JAB_FUNGSIONAL DESC) R 
          FROM SEJARAH_JABATAN_FUNGSIONAL S
        ) SF ON SF.ID_PENGGUNA=P.ID_PENGGUNA AND SF.R=1
        LEFT JOIN JABATAN_FUNGSIONAL JF ON JF.ID_JABATAN_FUNGSIONAL=SF.ID_JABATAN_FUNGSIONAL
        
        /*JABATAN STRUKTURAL */
        LEFT JOIN (
          SELECT S.ID_JABATAN_STRUKTURAL,S.ID_PENGGUNA,TO_CHAR(S.TMT_SEJ_JAB_STRUKTURAL,'DD-MONTH-YYYY') TMT_STRUK, ROW_NUMBER() OVER (PARTITION BY ID_PENGGUNA ORDER BY TGL_SK_SEJ_JAB_STRUKTURAL DESC) R 
          FROM SEJARAH_JABATAN_STRUKTURAL S
        ) SS ON SS.ID_PENGGUNA=P.ID_PENGGUNA AND SS.R=1
        LEFT JOIN JABATAN_STRUKTURAL JS ON JS.ID_JABATAN_STRUKTURAL=SS.ID_JABATAN_STRUKTURAL
        
        /*SEJARAH PENDIDIKAN*/
        LEFT JOIN (
          SELECT S.*, ROW_NUMBER() OVER (PARTITION BY ID_PENGGUNA ORDER BY TAHUN_LULUS_PENDIDIKAN DESC) R 
          FROM SEJARAH_PENDIDIKAN S
        ) SP ON SP.ID_PENGGUNA=P.ID_PENGGUNA AND SP.R=1
        LEFT JOIN PENDIDIKAN_AKHIR PA ON PA.ID_PENDIDIKAN_AKHIR=SP.ID_PENDIDIKAN_AKHIR
        JOIN PROGRAM_STUDI PR ON D.ID_PROGRAM_STUDI_SD = PR.ID_PROGRAM_STUDI
        JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS 
        LEFT JOIN KOTA K ON K.ID_KOTA = P.ID_KOTA_LAHIR
        JOIN ROLE R ON R.ID_ROLE = P.ID_ROLE
        LEFT JOIN AGAMA AG ON P.ID_AGAMA = AG.ID_AGAMA
        WHERE P.ID_PENGGUNA ='{$this->id_pengguna}'");
        return $this->db->FetchAssoc();
    }

    function update($nama, $gelar_depan, $gelar_belakang, $tempat_lahir, $tgl_lahir, $agama, $kelamin, $email, $blog, $alamat_rumah, $alamat_kantor, $telp, $bidang_keahlian, $id) {

        // UPDATE DOSEN
        $this->db->Query("
            UPDATE DOSEN SET 
                ALAMAT_RUMAH_DOSEN = '{$alamat_rumah}',
                ALAMAT_KANTOR_DOSEN= '{$alamat_kantor}',
                TLP_DOSEN= '{$telp}',
                BIDANG_KEAHLIAN_DOSEN= '{$bidang_keahlian}'
            WHERE ID_PENGGUNA = '{$id}'");
        // UPDATE PENGGUNA
        $this->db->Query("
            UPDATE PENGGUNA SET 
                NM_PENGGUNA = '{$nama}',
                GELAR_DEPAN = '{$gelar_depan}',
                GELAR_BELAKANG = '{$gelar_belakang}',
                ID_KOTA_LAHIR = '{$tempat_lahir}',
                TGL_LAHIR_PENGGUNA = '{$tgl_lahir}',
                ID_AGAMA = '{$agama}',
                KELAMIN_PENGGUNA = '{$kelamin}',
                EMAIL_PENGGUNA = '{$email}',
                BLOG_PENGGUNA ='{$blog}'
            WHERE ID_PENGGUNA = '{$id}'");
    }

}

?>
