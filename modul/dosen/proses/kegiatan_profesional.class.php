<?php

//membuat class kegiatan profesional
class kegiatan_profesional {

    public $id_dosen;
    public $db;
    
    function __construct($db, $id_dosen) {
        $this->db = $db;
        $this->id_dosen = $id_dosen;
    }

    function load_kegiatan(){
        return $this->db->QueryToArray("SELECT * FROM DOSEN_PROFESIONAL WHERE ID_DOSEN ='{$this->id_dosen}'");
    }
	
	function get($id_kegiatan){
		$this->db->Query("SELECT * FROM DOSEN_PROFESIONAL WHERE ID_DOSEN_PROFESIONAL='{$id_kegiatan}'");
		return $this->db->FetchAssoc();
	}

    function update($id_kegiatan, $kegiatan, $peran, $instansi, $tahun, $tingkat) {
        $this->db->Query("
			UPDATE DOSEN_PROFESIONAL 
				SET KEGIATAN_DOS_PROF ='{$kegiatan}', 
				PERAN_DOS_PROF ='{$peran}', 
				INSTANSI_DOS_PROF = '{$instansi}', 
				THN_DOS_PROF = '{$tahun}',
				TINGKAT_DOS_PROF = '{$tingkat}'
			WHERE ID_DOSEN_PROFESIONAL ='{$id_kegiatan}'");
    }

    function insert($id_dosen, $kegiatan, $peran, $instansi, $tahun, $tingkat) {
        $this->db->Query("
			INSERT INTO DOSEN_PROFESIONAL 
				(ID_DOSEN,KEGIATAN_DOS_PROF,PERAN_DOS_PROF,INSTANSI_DOS_PROF,THN_DOS_PROF,TINGKAT_DOS_PROF) 
			VALUES 
				('{$id_dosen}','{$kegiatan}','{$peran}','{$instansi}','{$tahun}','{$tingkat}') ");
    }

}

?>
