<?php

class presensi {

    public $db;
    public $id_dosen;

    function __construct($db, $id_dosen) {
        $this->db = $db;
        $this->id_dosen = $id_dosen;
    }

    function load_kelas_mk_dosen() {
        return $this->db->QueryToArray("
            SELECT 
              KMK.JUMLAH_PERTEMUAN_KELAS_MK AS PERTEMUAN,KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH,NVL(K1.NMKELAS,K2.NAMA_KELAS) AS KELAS
            FROM PENGAMPU_MK PENGMK
            JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
            JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
            JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
            JOIN SEMESTER S ON S.ID_SEMESTER= KMK.ID_SEMESTER
            LEFT JOIN FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
            LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
            WHERE PENGMK.ID_DOSEN='{$this->id_dosen}' AND S.STATUS_AKTIF_SEMESTER='True'
            ORDER BY MK.NM_MATA_KULIAH,KELAS");
    }

    function load_pertemuan($id_kelas_mk) {
        return $this->db->QueryToArray("
        SELECT PK.ID_PRESENSI_KELAS,PK.WAKTU_MULAI,PK.WAKTU_SELESAI,TO_CHAR(PK.TGL_PRESENSI_KELAS,'DD / MONTH/ YYYY') TGL_PRESENSI_KELAS
            ,R.NM_RUANGAN,G.NM_GEDUNG,G.LOKASI_GEDUNG,ROUND((PQ.MHS/PP.MHS)*100,1) PERSENTASE_PRESENSI_KELAS,PQ.MHS AS JUMLAH_MHS,P.NM_PENGGUNA
            ,MMK.ISI_MATERI_MK
        FROM PRESENSI_KELAS PK
        JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PK.ID_KELAS_MK
        JOIN MATERI_MK MMK ON MMK.ID_MATERI_MK=PK.ID_MATERI_MK AND MMK.ID_KELAS_MK=KMK.ID_KELAS_MK
        LEFT JOIN PRESENSI_MKDOS PDOS ON PDOS.ID_PRESENSI_KELAS = PK.ID_PRESENSI_KELAS
        LEFT JOIN DOSEN D ON PDOS.ID_DOSEN = D.ID_DOSEN
        LEFT JOIN PENGGUNA P ON D.ID_PENGGUNA = P.ID_PENGGUNA
        LEFT JOIN RUANGAN R ON PK.ID_RUANGAN = R.ID_RUANGAN
        LEFT JOIN GEDUNG G ON G.ID_GEDUNG =R.ID_GEDUNG
        LEFT JOIN (SELECT ID_PRESENSI_KELAS,COUNT(*) MHS FROM PRESENSI_MKMHS GROUP BY ID_PRESENSI_KELAS) PP ON PP.ID_PRESENSI_KELAS=PK.ID_PRESENSI_KELAS
        LEFT JOIN (SELECT ID_PRESENSI_KELAS,COUNT(*) MHS FROM PRESENSI_MKMHS WHERE KEHADIRAN=1 GROUP BY ID_PRESENSI_KELAS) PQ ON PQ.ID_PRESENSI_KELAS=PK.ID_PRESENSI_KELAS
        WHERE KMK.ID_KELAS_MK = '{$id_kelas_mk}' AND PDOS.KEHADIRAN=1
        ORDER BY PK.TGL_PRESENSI_KELAS");
    }

    function get_detail_pertemuan($id_presensi_kelas) {
        $this->db->Query("
        SELECT PK.ID_PRESENSI_KELAS,PK.WAKTU_MULAI,PK.WAKTU_SELESAI,TO_CHAR(PK.TGL_PRESENSI_KELAS,'DD / MONTH/ YYYY') TGL_PRESENSI_KELAS
                ,R.NM_RUANGAN,G.NM_GEDUNG,G.LOKASI_GEDUNG,round((PQ.MHS/PP.MHS)*100,1) PERSENTASE_PRESENSI_KELAS,P.NM_PENGGUNA
        FROM PRESENSI_KELAS PK
        LEFT JOIN PRESENSI_MKDOS PDOS ON PDOS.ID_PRESENSI_KELAS = PK.ID_PRESENSI_KELAS
        LEFT JOIN DOSEN D ON PDOS.ID_DOSEN = D.ID_DOSEN
        LEFT JOIN PENGGUNA P ON D.ID_PENGGUNA = P.ID_PENGGUNA
        LEFT JOIN RUANGAN R ON PK.ID_RUANGAN = R.ID_RUANGAN
        LEFT JOIN GEDUNG G ON G.ID_GEDUNG =R.ID_GEDUNG
	LEFT JOIN (SELECT ID_PRESENSI_KELAS,COUNT(*) MHS FROM PRESENSI_MKMHS GROUP BY ID_PRESENSI_KELAS) PP ON PP.ID_PRESENSI_KELAS=PK.ID_PRESENSI_KELAS
	LEFT JOIN (SELECT ID_PRESENSI_KELAS,COUNT(*) MHS FROM PRESENSI_MKMHS WHERE KEHADIRAN=1 GROUP BY ID_PRESENSI_KELAS) PQ ON PQ.ID_PRESENSI_KELAS=PK.ID_PRESENSI_KELAS


        WHERE PK.ID_PRESENSI_KELAS='{$id_presensi_kelas}' AND PDOS.KEHADIRAN=1");
        return $this->db->FetchAssoc();
    }

    function load_data_presensi_mahasiswa($id_presensi_kelas) {
        return $this->db->QueryToArray("
        SELECT M.NIM_MHS AS NIM,P.NM_PENGGUNA AS NAMA,PMHS.KEHADIRAN
        FROM PRESENSI_MKMHS PMHS
        JOIN MAHASISWA M ON M.ID_MHS =  PMHS.ID_MHS
        JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        JOIN PRESENSI_KELAS PK ON PK.ID_PRESENSI_KELAS = PMHS.ID_PRESENSI_KELAS
        WHERE PK.ID_PRESENSI_KELAS = '{$id_presensi_kelas}'");
    }

    function load_rekap_presensi_mhs($id_kelas_mk) {
        return $this->db->QueryToArray("
            SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,PMK.STATUS_CEKAL_UTS,PMK.STATUS_CEKAL,PP.PERTEMUAN,PQ.MASUK,ROUND((PQ.MASUK/PP.PERTEMUAN)*100,1) KEHADIRAN
            FROM PENGAMBILAN_MK PMK 
            JOIN MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            LEFT JOIN (
                SELECT PK.ID_KELAS_MK,COUNT(ID_PRESENSI_KELAS) PERTEMUAN
                FROM PRESENSI_KELAS PK
                GROUP BY PK.ID_KELAS_MK
            ) PP ON PP.ID_KELAS_MK=PMK.ID_KELAS_MK
            LEFT JOIN (
                SELECT PK.ID_KELAS_MK,PRMK.ID_MHS,COUNT(ID_PRESENSI_MKMHS) MASUK
                FROM PRESENSI_KELAS PK
                LEFT JOIN PRESENSI_MKMHS PRMK ON PK.ID_PRESENSI_KELAS=PRMK.ID_PRESENSI_KELAS 
                WHERE PRMK.KEHADIRAN=1
                GROUP BY PK.ID_KELAS_MK,PRMK.ID_MHS
            ) PQ ON PQ.ID_KELAS_MK=PMK.ID_KELAS_MK AND PQ.ID_MHS=PMK.ID_MHS
            WHERE PMK.ID_KELAS_MK='{$id_kelas_mk}' AND PMK.STATUS_APV_PENGAMBILAN_MK=1
            ");
    }

}

?>
