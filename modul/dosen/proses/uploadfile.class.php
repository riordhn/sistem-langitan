<?php
/* - Upload Class - by Fathoni - */
/* - First : 17 Agustus 2011   - */

class UACC_UploadFile
{
	var $file_name;
	var $file_size;
	var $file_temp;
	var $setting;
	var $uploaded;
	var $saved;

	function __construct(&$file, &$upload_setting)
	{
		$this->uploaded = false;
		$this->saved = false;
		
		if ($file['error'] === UPLOAD_ERR_OK) // cek file terupload sempurna
		{
			$this->setting = $upload_setting;
			$this->file_name = $file['name'];
			$this->file_size = $file['size'];
			$this->file_temp = $file['tmp_name'];
			
			// memastikan file terupload dan tersimpan
			if (is_uploaded_file($this->file_temp))
			{
				if ($this->validate_extension())
				{
					if ($this->validate_size())
					{
						$this->uploaded = true;
						echo 'File Berhasil Di Upload';
					}
					else echo 'Ukuran Tidak Sesuai';
				}
				else echo 'Type File tidak sesuai';
			}
			else echo 'File Sudah Ada di direktori';
		}
	}

	function save()
	{
		$this->check_dir($this->setting['destination']);  // create directory if not exist

		if (move_uploaded_file($this->file_temp, $this->setting['destination'] . $this->file_name))   // move file
		{ 
			$this->saved = true;
			return true;
		}
		else
		{
			$this->saved = false;
			return false;
		}
	}

	private function check_dir($directory)
	{
		if (!is_dir($directory))
		{
			umask(0);
			mkdir($directory, 0777);
		}
	}

	private function get_extension($from_file)
	{
		return strtolower(strrchr($from_file,"."));
	}

	private	function validate_extension()
	{
		$extension = $this->get_extension($this->file_name);
		if (in_array($extension, $this->setting['allowed_ext'])) { 
			return true;
		} else {
			return false;
		}
	}

	private function validate_size()
	{
		if ($this->file_size <= $this->setting['max_size'])
			return true;
		else
			return false;
	}
}
?>