<?php

//membuat class penghargaan


class penghargaan {

    public $db;
    public $id_dosen;

    function __construct($db, $id) {
        $this->db = $db;
        $this->id_dosen = $id;
    }

    function load_penghargaan(){
        return $this->db->QueryToArray("SELECT * FROM DOSEN_PENGHARGAAN 
										LEFT JOIN NEGARA ON NEGARA.ID_NEGARA = DOSEN_PENGHARGAAN.ID_NEGARA
										WHERE ID_DOSEN ='{$this->id_dosen}'");
	}
	
	function get_penghargaan($id_penghargaan){
		$this->db->Query("SELECT * FROM DOSEN_PENGHARGAAN WHERE ID_DOSEN_PENGHARGAAN='{$id_penghargaan}'");
		return $this->db->FetchAssoc();
	}

    function insert($bidang, $bentuk, $pemberi, $tahun, $tingkat, $negara) {
        $this->db->Query("
			INSERT INTO DOSEN_PENGHARGAAN 
				(ID_DOSEN,BIDANG_DOSEN_PENGHARGAAN,BENTUK_DOSEN_PENGHARGAAN,PEMBERI_DOSEN_PENGHARGAAN,THN_DOSEN_PENGHARGAAN,
				ID_NEGARA, TINGKAT_DOSEN_PENGHARGAAN) 
			VALUES 
				('{$this->id_dosen}','{$bidang}','{$bentuk}','{$pemberi}','{$tahun}','{$negara}','{$tingkat}')");
	}

    function update($id_penghargaan, $bidang, $bentuk, $pemberi, $tahun, $tingkat, $negara) {
        $this->db->Query("
			UPDATE DOSEN_PENGHARGAAN 
			SET 
				BIDANG_DOSEN_PENGHARGAAN='{$bidang}',
				BENTUK_DOSEN_PENGHARGAAN ='{$bentuk}',
				PEMBERI_DOSEN_PENGHARGAAN='{$pemberi}',
				THN_DOSEN_PENGHARGAAN='{$tahun}' ,
				ID_NEGARA = '{$negara}',
				TINGKAT_DOSEN_PENGHARGAAN = '{$tingkat}'
			WHERE ID_DOSEN_PENGHARGAAN = '{$id_penghargaan}'");
    }

}

?>
