<?php

class bimbingan {

    public $db;
    public $id_dosen;
    public $id_pengguna;
    public $id_semester_aktif;
    public $id_semester_sebelum;

    function __construct($db, $id_pengguna, $id_dosen) {
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
        $this->id_dosen = $id_dosen;
        $this->id_semester_aktif = $this->db->QuerySingle("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True'");
        $this->id_semester_sebelum = $this->get_semester_kemarin();
    }

    function get_semester_aktif() {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True'");
        return $this->db->FetchAssoc();
    }

    function get_semester_kemarin() {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil'");
            $semester_kemarin = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1)");
            $semester_kemarin = $this->db->FetchAssoc();
        }
        return $semester_kemarin['ID_SEMESTER'];
    }

    function LoadMahasiswaBimbingan() {
        $result = array();
        $jenis = $this->db->QueryToArray("SELECT * FROM JENIS_PEMBIMBING WHERE UPPER(NM_JENIS_PEMBIMBING) LIKE '%PEMBIMBING%'");
        foreach ($jenis as $j) {
            $mahasiswa = array();
            $row = $this->db->QueryToArray("
            SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,M.THN_ANGKATAN_MHS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,PMK.SEMESTER,JP.NM_JENIS_PEMBIMBING,SP.NM_STATUS_PENGGUNA,M.MOBILE_MHS,
            TA.PROGRESS
            FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            JOIN (
              SELECT ID_MHS,COUNT(DISTINCT(ID_SEMESTER)) SEMESTER
              FROM PENGAMBILAN_MK
              GROUP BY ID_MHS
            ) PMK ON PMK.ID_MHS=M.ID_MHS
            JOIN PEMBIMBING_TA PT ON PT.ID_MHS=M.ID_MHS
            JOIN JENIS_PEMBIMBING JP ON JP.ID_JENIS_PEMBIMBING = PT.ID_JENIS_PEMBIMBING
            LEFT JOIN TUGAS_AKHIR TA ON TA.ID_MHS=M.ID_MHS AND TA.STATUS=1
            WHERE PT.ID_DOSEN='{$this->id_dosen}' AND JP.ID_JENIS_PEMBIMBING='{$j['ID_JENIS_PEMBIMBING']}' AND PT.STATUS_DOSEN='1'
            ORDER BY M.THN_ANGKATAN_MHS,P.NM_PENGGUNA");
            foreach ($row as $temp) {
                $mhs_status = $this->load_mhs_status($temp['ID_MHS']);
                $jadwal_ujian = $this->GetJadwalUjianAkhir($temp['ID_MHS']);
                array_push($mahasiswa, array(
                    'ID_MHS' => $temp['ID_MHS'],
                    'NIM' => $temp['NIM_MHS'],
                    'NAMA' => $temp['NM_PENGGUNA'],
                    'NM_JENJANG' => $temp['NM_JENJANG'],
                    'PRODI' => $temp['NM_PROGRAM_STUDI'],
                    'IPS' => $mhs_status['IPS'],
                    'IPK' => $mhs_status['IPK'],
                    'TOTAL_SKS' => $mhs_status['TOTAL_SKS'],
                    'ANGKATAN' => $temp['THN_ANGKATAN_MHS'],
                    'STATUS' => $temp['NM_STATUS_PENGGUNA'],
                    'SEMESTER' => $temp['SEMESTER'],
                    'JADWAL_UJIAN' => $jadwal_ujian,
                    'PROGRESS' => $temp['PROGRESS'],
                    'MOBILE_MHS' => $temp['MOBILE_MHS']));
            }
            array_push($result, array_merge($j, array('DATA_MHS' => $mahasiswa)));
        }
        return $result;
    }

    function GetDetailTugasAkhirMhs($id_mhs) {
        $mhs_status = $this->load_mhs_status($id_mhs);
        $this->db->Query("
            SELECT TA.*,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,SP.NM_STATUS_PENGGUNA STATUS,PMK.SEMESTER,TT.NM_TIPE_TA
            FROM TUGAS_AKHIR TA
            JOIN MAHASISWA M ON M.ID_MHS=TA.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            JOIN TIPE_TA TT ON TT.ID_TIPE_TA=TA.ID_TIPE_TA
            JOIN (
              SELECT ID_MHS,COUNT(DISTINCT(ID_SEMESTER)) SEMESTER
              FROM PENGAMBILAN_MK
              GROUP BY ID_MHS
            ) PMK ON PMK.ID_MHS=M.ID_MHS
            WHERE TA.ID_MHS='{$id_mhs}' AND TA.STATUS=1
            ");
        $result = array_merge($this->db->FetchAssoc(), array(
            'IPS' => $mhs_status['IPS'],
            'IPK' => $mhs_status['IPK'],
            'PEMBIMBING' => $this->GetPembimbingMhs($id_mhs),
            'PENGUJI' => $this->GetPengujiMhs($id_mhs),
            'STATUS_BIMBINGAN' => $this->CekMahasiswaBimbingan($id_mhs),
            'TOTAL_SKS' => $mhs_status['TOTAL_SKS']));
        return $result;
    }

    function GetJadwalUjianAkhir($mhs) {
        return $this->db->QueryToArray("
            SELECT UMK.*,R.NM_RUANGAN, NVL(K1.NMKELAS,K2.NAMA_KELAS) NM_KELAS,MK.KD_MATA_KULIAH AS KODE_MK,
            MK.NM_MATA_KULIAH AS NAMA_MK,S.NM_SEMESTER,S.TAHUN_AJARAN
            FROM UJIAN_MK UMK
            JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = UMK.ID_KELAS_MK
            JOIN SEMESTER S ON S.ID_SEMESTER=KMK.ID_SEMESTER
            LEFT JOIN FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
            LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
            JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
            JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
            JOIN JADWAL_UJIAN_MK JUMK ON UMK.ID_UJIAN_MK=JUMK.ID_UJIAN_MK
            JOIN RUANGAN R ON R.ID_RUANGAN=JUMK.ID_RUANGAN
            JOIN UJIAN_MK_PESERTA UMKP ON UMKP.ID_UJIAN_MK=UMK.ID_UJIAN_MK
            WHERE UMKP.ID_MHS='{$mhs}' AND UMK.ID_KEGIATAN=71
            ");
    }

    function GetPembimbingMhs($mhs) {
        return $this->db->QueryToArray("
            SELECT JP.NM_JENIS_PEMBIMBING,P.NM_PENGGUNA,P.GELAR_DEPAN,P.GELAR_BELAKANG,D.NIP_DOSEN
            FROM PEMBIMBING_TA PT
            JOIN DOSEN D ON D.ID_DOSEN=PT.ID_DOSEN
            JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
            JOIN JENIS_PEMBIMBING JP ON JP.ID_JENIS_PEMBIMBING = PT.ID_JENIS_PEMBIMBING
            WHERE PT.ID_MHS='{$mhs}'
            ORDER BY JP.ID_JENIS_PEMBIMBING
            ");
    }

    function GetPengujiMhs($mhs) {
        return $this->db->QueryToArray("
            SELECT P.NM_PENGGUNA,P.GELAR_DEPAN,P.GELAR_BELAKANG,D.NIP_DOSEN
            FROM TIM_PENGAWAS_UJIAN TPU
            JOIN JADWAL_UJIAN_MK JUMK ON TPU.ID_JADWAL_UJIAN_MK=JUMK.ID_JADWAL_UJIAN_MK
            JOIN UJIAN_MK UMK ON UMK.ID_UJIAN_MK=JUMK.ID_UJIAN_MK
            JOIN UJIAN_MK_PESERTA UMKP ON UMKP.ID_UJIAN_MK=UMK.ID_UJIAN_MK
            JOIN DOSEN D ON D.ID_PENGGUNA=TPU.ID_PENGGUNA
            JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
            WHERE UMKP.ID_MHS='{$mhs}' AND UMK.ID_KEGIATAN='71'
            ORDER BY P.NM_PENGGUNA
            ");
    }

    function LoadMahasiswaPenguji() {
        $mahasiswa = array();
        $row = $this->db->QueryToArray("
            SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,M.THN_ANGKATAN_MHS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,
            PMK.SEMESTER,SP.NM_STATUS_PENGGUNA,M.MOBILE_MHS
            FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            JOIN (
              SELECT ID_MHS,COUNT(DISTINCT(ID_SEMESTER)) SEMESTER
              FROM PENGAMBILAN_MK
              GROUP BY ID_MHS
            ) PMK ON PMK.ID_MHS=M.ID_MHS
            JOIN UJIAN_MK_PESERTA UMKP ON UMKP.ID_MHS=M.ID_MHS
            JOIN UJIAN_MK UMK ON UMK.ID_UJIAN_MK=UMKP.ID_UJIAN_MK
            JOIN JADWAL_UJIAN_MK JUMK ON JUMK.ID_UJIAN_MK=UMK.ID_UJIAN_MK
            JOIN TIM_PENGAWAS_UJIAN TPU ON JUMK.ID_JADWAL_UJIAN_MK=TPU.ID_JADWAL_UJIAN_MK
            WHERE TPU.ID_PENGGUNA='{$this->id_pengguna}' AND UMK.ID_KEGIATAN=71
            ORDER BY M.THN_ANGKATAN_MHS,P.NM_PENGGUNA");
        foreach ($row as $temp) {
            $mhs_status = $this->load_mhs_status($temp['ID_MHS']);
            $jadwal_ujian = $this->GetJadwalUjianAkhir($temp['ID_MHS']);
            array_push($mahasiswa, array(
                'ID_MHS' => $temp['ID_MHS'],
                'NIM' => $temp['NIM_MHS'],
                'NAMA' => $temp['NM_PENGGUNA'],
                'NM_JENJANG' => $temp['NM_JENJANG'],
                'PRODI' => $temp['NM_PROGRAM_STUDI'],
                'IPS' => $mhs_status['IPS'],
                'IPK' => $mhs_status['IPK'],
                'TOTAL_SKS' => $mhs_status['TOTAL_SKS'],
                'ANGKATAN' => $temp['THN_ANGKATAN_MHS'],
                'STATUS' => $temp['NM_STATUS_PENGGUNA'],
                'SEMESTER' => $temp['SEMESTER'],
                'JADWAL_UJIAN' => $jadwal_ujian,
                'MOBILE_MHS' => $temp['MOBILE_MHS']));
        }
        return $mahasiswa;
    }

    function CekMahasiswaBimbingan($mhs) {
        return $this->db->QuerySingle("SELECT COUNT(*) FROM PEMBIMBING_TA WHERE ID_DOSEN='{$this->id_dosen}' AND ID_MHS='{$mhs}' AND STATUS_DOSEN='1'");
    }

    function get_ips_mhs($id_mhs, $id_semester) {
        $this->db->Query("
            select id_mhs,case when sum(bobot*kredit_semester)=0 then 0 else 
            round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
            case when id_fakultas=7 then sum(sksreal) else sum(kredit_semester) end as sks_sem 
            from 
            (select id_mhs,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
            (select a.id_mhs,ps.id_fakultas,a.id_kurikulum_mk, 
            case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
            and d.status_mkta in (1,2) then 0
            else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
            case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
            case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
            from pengambilan_mk a
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
            left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
            left join mahasiswa m on a.id_mhs=m.id_mhs
            left join program_studi ps on m.id_program_studi=ps.id_program_studi
            left join semester s on a.id_semester=s.id_semester
            where group_semester||thn_akademik_semester in
            (select group_semester||thn_akademik_semester from semester where id_Semester='" . $id_semester . "')
            and tipe_semester in ('UP','REG','RD') 
            and a.status_apv_pengambilan_mk='1' and m.id_mhs='" . $id_mhs . "' and a.status_hapus=0 and a.flagnilai='1'
            and a.status_pengambilan_mk !=0
            )
            group by id_mhs, id_kurikulum_mk, id_fakultas, kredit_semester, sksreal
            )
            group by id_mhs,id_fakultas
            ");
        $data = $this->db->FetchAssoc();
        return $data['IPS'];
    }

    function load_mhs_status($id_mhs) {
        $ips = $this->get_ips_mhs($id_mhs, $this->id_semester_sebelum);
        $mhs_status = $this->get_mhs_status($id_mhs);
        $this->db->Query("
        SELECT P.NM_PENGGUNA,M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,PS.NM_PROGRAM_STUDI,'{$ips}' AS IPS,'{$mhs_status['IPK']}' AS IPK,'{$mhs_status['SKSTOTAL']}' AS TOTAL_SKS,J.NM_JENJANG FROM MAHASISWA M
            LEFT JOIN PENGGUNA P ON M.ID_PENGGUNA  = P.ID_PENGGUNA
            LEFT JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        WHERE M.ID_MHS ='{$id_mhs}'");
        return $this->db->FetchAssoc();
    }

    function get_mhs_status($id_mhs) {
        $this->db->Query("SELECT PS.ID_FAKULTAS FROM MAHASISWA M JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI WHERE M.ID_MHS='{$id_mhs}'");
        $id_fakultas_mhs = $this->db->FetchAssoc();
        // ngulang -> nilai terakhir yg berlaku
        if ($id_fakultas_mhs['ID_FAKULTAS'] == 10) {
            $sql = "select a.id_mhs,sum(a.kredit_semester) skstotal,
                    round(sum(a.kredit_semester*(case a.nilai_huruf 
                    when 'A' then 4 
                    when 'AB' then 3.5 
                    when 'B' then 3
                    when 'BC' then 2.5
                    when 'C' then 2
                    when 'D' then 1
                    end))/sum(a.kredit_semester),2) IPK
                    from
                    (
                    select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
                    select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester desc,sm.nm_semester desc) rangking
                    from pengambilan_mk a 
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    left join semester sm on sm.id_semester=a.id_semester
                    where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.flagnilai=1 and a.id_semester is not null
                    ) a
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    where rangking=1 and id_mhs='{$id_mhs}'
                    ) a
                    left join mahasiswa b on a.id_mhs=b.id_mhs
                    left join program_studi f on b.id_program_studi=f.id_program_studi
                    where a.id_mhs='{$id_mhs}'
                    group by a.id_mhs order by a.id_mhs";
            $this->db->Query($sql);
            $mhs_status = $this->db->FetchAssoc();
        } else {
            // ipk versi lukman, ngulang diambil nilai terbaik.
            $sql = "select a.id_mhs,sum(a.kredit_semester) skstotal,
                    round(sum(a.kredit_semester*(case a.nilai_huruf 
                    when 'A' then 4 
                    when 'AB' then 3.5 
                    when 'B' then 3
                    when 'BC' then 2.5
                    when 'C' then 2
                    when 'D' then 1
                    end))/sum(a.kredit_semester),2) IPK
                    from
                    (
                    select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
                    select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
                    from pengambilan_mk a 
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null 
                    ) a
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    where rangking=1 and id_mhs='{$id_mhs}'
                    ) a
                    left join mahasiswa b on a.id_mhs=b.id_mhs
                    left join program_studi f on b.id_program_studi=f.id_program_studi
                    where a.id_mhs='{$id_mhs}'
                    group by a.id_mhs order by a.id_mhs";
            $this->db->Query($sql);
            $mhs_status = $this->db->FetchAssoc();
        }
        return $mhs_status;
    }

}

?>
