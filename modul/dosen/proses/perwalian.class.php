<?php

$id_pt = $id_pt_user;

class perwalian {

    public $db;
    public $id_dosen;
    public $id_pengguna;
    public $id_semester_aktif;
    public $id_semester_sebelum;

    function __construct($db, $id_pengguna, $id_dosen) {
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
        $this->id_dosen = $id_dosen;
        $this->id_semester_aktif = $this->db->QuerySingle("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI = '{$GLOBALS['id_pt']}'");
        $this->id_semester_sebelum = $this->get_semester_kemarin();
    }

    function get_tgl_selesai_krs()
    {
        return $this->db->QuerySingle("SELECT jk.TGL_SELESAI_JKS 
                                        FROM JADWAL_KEGIATAN_SEMESTER jk
                                        JOIN KEGIATAN k ON k.ID_KEGIATAN = jk.ID_KEGIATAN
                                        WHERE k.KODE_KEGIATAN = 'KRS' AND jk.ID_SEMESTER = '{$this->id_semester_aktif}'");
    }

    function get_detail_aktivitas($id_mhs)
    {
        return $this->db->QueryToArray(
            "SELECT 
                s.tahun_ajaran, s.nm_semester, sp.nm_status_pengguna, ms.sks_semester, ms.ips, ms.sks_total, ms.ipk
            FROM mahasiswa_status ms
            JOIN mahasiswa M ON M.id_mhs = ms.id_mhs
            JOIN pengguna P ON P.id_pengguna = M.id_pengguna
            JOIN status_pengguna sp ON sp.id_status_pengguna = ms.id_status_pengguna
            JOIN semester s on s.id_semester = ms.id_semester
            WHERE 
                M.ID_MHS = '{$id_mhs}'
            ORDER BY S.thn_akademik_semester DESC, S.nm_semester DESC"
        );
    }

    function get_jumlah_tagihan_mahasiswa ($id_mhs){
        return $this->db->QueryToArray("
        SELECT mhs.NIM_MHS, keuangan.JUMLAH_TAGIHAN, TO_DATE(TANGGAL_INPUT,
            'DD-MM-YYYY') AS TANGGAL, TO_CHAR(TANGGAL_INPUT,
            'HH24:MI:SS') AS WAKTU, s.NM_SEMESTER, s.TAHUN_AJARAN, s.FD_ID_SMT 
            FROM KEUANGAN_KHS keuangan
            JOIN MAHASISWA mhs ON mhs.ID_MHS = keuangan.ID_MHS
            JOIN SEMESTER s on s.ID_SEMESTER = keuangan.ID_SEMESTER  
            WHERE mhs.ID_MHS = '{$id_mhs}' AND s.FD_ID_SMT >='20181'
            ORDER BY s.THN_AKADEMIK_SEMESTER DESC, s.NM_SEMESTER DESC
        ");
    }

    function get_semester_aktif() {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI = '{$GLOBALS['id_pt']}'");
        return $this->db->FetchAssoc();
    }

    function get_semester_kemarin() {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI = '{$GLOBALS['id_pt']}'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT ID_SEMESTER FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil' AND ID_PERGURUAN_TINGGI = '{$GLOBALS['id_pt']}'");
            $semester_kemarin = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT ID_SEMESTER FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1) AND ID_PERGURUAN_TINGGI = '{$GLOBALS['id_pt']}'");
            $semester_kemarin = $this->db->FetchAssoc();
        }
        return $semester_kemarin['ID_SEMESTER'];
    }

    function load_mahasiswa_krs_fakultas($id_fakultas) {
        $mahasiswa_krs = array();
        $row = $this->db->QueryToArray("
        SELECT M.ID_MHS,M.NIM_MHS,UPPER(P.NM_PENGGUNA) AS NM_PENGGUNA,M.THN_ANGKATAN_MHS,
                PMK.STATUS_APPROVE,PMK.STATUS_PENGAMBILAN,M.ID_PROGRAM_STUDI,
                ROW_NUMBER() OVER(PARTITION BY M.ID_MHS ORDER BY SDW.TAHUN_AJARAN DESC,SDW.NM_SEMESTER DESC) WALI_KE, 
                M.MOBILE_MHS, JJG.NM_JENJANG
            FROM DOSEN_WALI DW
            JOIN MAHASISWA M ON DW.ID_MHS = M.ID_MHS
            JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
            LEFT JOIN (
              SELECT ID_MHS,ID_SEMESTER,
                AVG(STATUS_APV_PENGAMBILAN_MK) STATUS_APPROVE,
                AVG(STATUS_PENGAMBILAN_MK) STATUS_PENGAMBILAN
                FROM PENGAMBILAN_MK 
              GROUP BY ID_MHS,ID_SEMESTER
            ) PMK ON PMK.ID_MHS = M.ID_MHS 
            LEFT JOIN SEMESTER S ON PMK.ID_SEMESTER=S.ID_SEMESTER
            LEFT JOIN SEMESTER SDW ON SDW.ID_SEMESTER=DW.ID_SEMESTER
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG JJG ON JJG.ID_JENJANG=PS.ID_JENJANG
        WHERE PS.ID_FAKULTAS= '{$id_fakultas}' AND S.STATUS_AKTIF_SEMESTER='True'");
        foreach ($row as $temp) {
            if ($temp['WALI_KE'] == '1') {
                $mhs_status = $this->load_mhs_status($temp['ID_MHS']);
                array_push($mahasiswa_krs, array(
                    'ID_MHS' => $temp['ID_MHS'],
                    'NIM' => $temp['NIM_MHS'],
                    'NAMA' => $temp['NM_PENGGUNA'],
                    'NM_JENJANG' => $temp['NM_JENJANG'],
                    'IPS' => $mhs_status['IPS'],
                    'SKS_MAKS' => $this->get_sks_maks_mhs($temp['ID_PROGRAM_STUDI'], $mhs_status['IPS']),
                    'IPK' => $mhs_status['IPK'],
                    'TOTAL_SKS' => $mhs_status['TOTAL_SKS'],
                    'ANGKATAN' => $temp['THN_ANGKATAN_MHS'],
                    'STATUS' => $temp['STATUS_PENGAMBILAN'],
                    'STATUS_APV' => $temp['STATUS_APPROVE'],
                    'MOBILE_MHS' => $temp['MOBILE_MHS']
                ));
            }
        }
        return $mahasiswa_krs;
    }

    function load_mahasiswa_krs() {

        $mahasiswa_krs = array();

        // Query Baru +efisiensi index
        $sql =
            "SELECT 
                dw.id_mhs, m.nim_mhs, p.nm_pengguna, m.thn_angkatan_mhs, 
                m.id_program_studi, 1 AS wali_ke, m.mobile_mhs, j.nm_jenjang,
                pmk.status_approve, pmk.status_pengambilan, sp.nm_status_pengguna,
                ms.ips, ms.ipk, ms.sks_total as total_sks
            FROM dosen_wali dw
            JOIN mahasiswa m ON m.id_mhs = dw.id_mhs
            JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs
            JOIN pengguna p ON p.id_pengguna = m.id_pengguna
            JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
            JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
            LEFT JOIN mahasiswa_status ms on ms.id_mhs = m.id_mhs and ms.id_semester = '{$this->id_semester_sebelum}'
            LEFT JOIN 
            (
                SELECT id_mhs, avg(status_apv_pengambilan_mk) status_approve, avg(status_pengambilan_mk) status_pengambilan
                FROM pengambilan_mk pmk 
                WHERE 
                    id_semester IN ($this->id_semester_aktif) AND
                    id_mhs IN (SELECT id_mhs FROM dosen_wali WHERE id_dosen = '{$this->id_dosen}' AND status_dosen_wali = 1)
                GROUP BY id_mhs
            ) pmk on pmk.id_mhs = m.id_mhs
            WHERE 
                dw.id_dosen = '{$this->id_dosen}' AND dw.status_dosen_wali = 1 AND sp.aktif_status_pengguna = 1
            ORDER BY thn_angkatan_mhs, nim_mhs";
        
        $row = $this->db->QueryToArray($sql);

        foreach ($row as $temp) {
            array_push($mahasiswa_krs, array(
                'ID_MHS' => $temp['ID_MHS'],
                'NIM' => $temp['NIM_MHS'],
                'NAMA' => $temp['NM_PENGGUNA'],
                'NM_JENJANG' => $temp['NM_JENJANG'],
                'IPS' => $temp['IPS'],
                'SKS_MAKS' => ($temp['IPS'] == '') ? '' : $this->get_sks_maks_mhs($temp['ID_PROGRAM_STUDI'], $temp['IPS']), // baru ambil data ketika ada nilai ips
                'IPK' => $temp['IPK'],
                'TOTAL_SKS' => $temp['TOTAL_SKS'],
                'ANGKATAN' => $temp['THN_ANGKATAN_MHS'],
                'STATUS' => $temp['STATUS_PENGAMBILAN'],
                'STATUS_APV' => $temp['STATUS_APPROVE'],
                'MOBILE_MHS' => $temp['MOBILE_MHS'],
                'NM_STATUS_PENGGUNA' => $temp['NM_STATUS_PENGGUNA']
            ));
        }
        return $mahasiswa_krs;
    }

    function load_mahasiswa_krp() {

        $mahasiswa_krp = array();

        // Query Baru +efisiensi index
        $sql =
            "SELECT 
                dw.id_mhs, m.nim_mhs, p.nm_pengguna, m.thn_angkatan_mhs, 
                m.id_program_studi, 1 AS wali_ke, m.mobile_mhs, j.nm_jenjang,
                                (SELECT COUNT(ID_KRP_KHP) 
                                    FROM KRP_KHP kk 
                                    WHERE kk.ID_MHS = m.ID_MHS) AS JML_SKKK,
                                (SELECT SUM(SKOR_KRP_KHP) 
                                    FROM KRP_KHP kk 
                                    WHERE kk.ID_MHS = m.ID_MHS) AS SKOR_SKKK,
                                (SELECT COUNT(ID_FILE_KRP_KHP) 
                                    FROM FILE_KRP_KHP fkk 
                                    JOIN KRP_KHP kk ON kk.ID_KRP_KHP = fkk.ID_KRP_KHP 
                                    WHERE kk.ID_MHS = m.ID_MHS AND fkk.IS_VERIFIED = 0) AS JML_FILE_NOT_VERIFIED,
                                (SELECT COUNT(ID_FILE_KRP_KHP) 
                                    FROM FILE_KRP_KHP fkk 
                                    JOIN KRP_KHP kk ON kk.ID_KRP_KHP = fkk.ID_KRP_KHP 
                                    WHERE kk.ID_MHS = m.ID_MHS AND fkk.IS_VERIFIED = 1) AS JML_FILE_VERIFIED
            FROM dosen_wali dw
            JOIN mahasiswa m ON m.id_mhs = dw.id_mhs
            JOIN pengguna p ON p.id_pengguna = m.id_pengguna
            JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
            JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
            WHERE 
                dw.id_dosen = '{$this->id_dosen}' AND dw.status_dosen_wali = 1
            ORDER BY JML_FILE_NOT_VERIFIED DESC, JML_FILE_VERIFIED DESC, thn_angkatan_mhs, nim_mhs";
            // echo '<pre>'.$sql.'</pre>'; exit();
        $row = $this->db->QueryToArray($sql);

        foreach ($row as $temp) {
            if ($temp['WALI_KE'] == '1') {
                array_push($mahasiswa_krp, array(
                    'ID_MHS' => $temp['ID_MHS'],
                    'NIM' => $temp['NIM_MHS'],
                    'NAMA' => $temp['NM_PENGGUNA'],
                    'NM_JENJANG' => $temp['NM_JENJANG'],
                    'ANGKATAN' => $temp['THN_ANGKATAN_MHS'],
                    'MOBILE_MHS' => $temp['MOBILE_MHS'],
                    'JML_SKKK' => $temp['JML_SKKK'],
                    'SKOR_SKKK' => $temp['SKOR_SKKK'],
                    'JML_FILE_NOT_VERIFIED' => $temp['JML_FILE_NOT_VERIFIED'],
                    'JML_FILE_VERIFIED' => $temp['JML_FILE_VERIFIED']
                ));
            }
        }
        return $mahasiswa_krp;
    }

    function get_ips_mhs($id_mhs, $id_semester) {

        // Dimatikan per 29-09-2019, dirubah menjadi data dari tabel MAHASISWA_STATUS
        // Deprecated
        // $this->db->Query("SELECT SUM(sks) AS sks_total, SUM((bobot*sks)) as bobot_total, round(SUM(nilai_mutu) / sum(sks), 2) as ips from (
        //                 SELECT
        //                     mhs.id_mhs,
        //                     /* Kode MK utk grouping total mutu */
        //                     COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
        //                     /* SKS */
        //                     COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
        //                     /* Bobot */
        //                     sn.nilai_standar_nilai AS bobot,
        //                     /* Nilai Mutu = SKS * Bobot */
        //                     COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu
        //                 FROM pengambilan_mk pmk
        //                 JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
        //                 JOIN semester S ON S.id_semester = pmk.id_semester
        //                 -- Via Kelas
        //                 LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
        //                 LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
        //                 -- Via Kurikulum
        //                 LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
        //                 LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
        //                 -- nilai bobot
        //                 JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
        //                 WHERE 
        //                     pmk.status_apv_pengambilan_mk = 1 AND 
        //                     mhs.id_mhs = '".$id_mhs."' AND 
        //                     S.id_semester = '".$id_semester."'
        //             )
        //             group by id_mhs
        //     ");

        $this->db->Query("SELECT IPS FROM MAHASISWA_STATUS WHERE ID_MHS = '{$id_mhs}' AND ID_SEMESTER = '{$id_semester}'");
        $data = $this->db->FetchAssoc();
        return $data['IPS'];
    }

    function get_sks_maks_mhs($id_program_studi, $ips) {
        $this->db->Query("SELECT MAX(SKS_MAKSIMAL) SKS_MAKSIMAL FROM BEBAN_SKS WHERE ID_PROGRAM_STUDI='{$id_program_studi}' AND IPK_MINIMUM <='{$ips}'");
        $data_sks_maks = $this->db->FetchAssoc();
        return $data_sks_maks['SKS_MAKSIMAL'];
    }

    function load_krs($id_mhs) {
        $ips = $this->get_ips_mhs($id_mhs, $this->id_semester_sebelum);
        return $this->db->QueryToArray("
        SELECT PMK.ID_PENGAMBILAN_MK AS ID_PENGAMBILAN_MK, MK.KD_MATA_KULIAH AS KODE_MK,MK.NM_MATA_KULIAH AS NAMA_MK,
            KUMK.KREDIT_SEMESTER AS SKS,PMK.STATUS_APV_PENGAMBILAN_MK AS STATUS_APV,PMK.STATUS_PENGAMBILAN_MK 
            AS STATUS,PMK.STATUS_ULANG,MAX(BS.SKS_MAKSIMAL) AS SKS_MAX,K2.NAMA_KELAS AS NM_KELAS,PMK.ID_SEMESTER
            FROM PENGAMBILAN_MK PMK
            JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
            JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PMK.ID_KELAS_MK
            LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
            JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
            JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
            JOIN SEMESTER S ON PMK.ID_SEMESTER =  S.ID_SEMESTER
            LEFT JOIN BEBAN_SKS BS ON BS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI AND BS.IPK_MINIMUM<='{$ips}'
        WHERE M.ID_MHS = '{$id_mhs}' AND S.STATUS_AKTIF_SEMESTER='True' AND PMK.STATUS_PENGAMBILAN_MK !=0
        GROUP BY PMK.ID_PENGAMBILAN_MK,MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,KUMK.KREDIT_SEMESTER,PMK.STATUS_APV_PENGAMBILAN_MK,PMK.STATUS_PENGAMBILAN_MK,PMK.STATUS_ULANG,K2.NAMA_KELAS,PMK.ID_SEMESTER
        ");
    }

    function get_id_pengguna_mhs($id_mhs) {
        $this->db->Query("SELECT ID_PENGGUNA FROM MAHASISWA WHERE ID_MHS='{$id_mhs}'");
        $data = $this->db->FetchAssoc();
        return $data['ID_PENGGUNA'];
    }

    function send_message_krs($id_mhs, $judul_pesan, $isi_pesan) {
        $semester_aktif = $this->get_semester_aktif();
        $this->db->Query("INSERT INTO PESAN (ID_PENGIRIM,ID_PENERIMA,JUDUL_PESAN,ISI_PESAN,TIPE_PESAN,WAKTU_PESAN,ID_SEMESTER) VALUES ('{$this->id_pengguna}','{$this->get_id_pengguna_mhs($id_mhs)}','{$judul_pesan}','{$isi_pesan}','KRS',CURRENT_TIMESTAMP,'{$semester_aktif['ID_SEMESTER']}')");
    }

    function load_krs_message($id_mhs) {
        $semester_aktif = $this->get_semester_aktif();
        return $this->db->QueryToArray("SELECT * FROM PESAN WHERE ID_PENERIMA='{$this->get_id_pengguna_mhs($id_mhs)}' AND ID_PENGIRIM='{$this->id_pengguna}' AND TIPE_PESAN='KRS' AND ID_SEMESTER='{$semester_aktif['ID_SEMESTER']}' ORDER BY ID_PESAN");
    }

    function load_transkrip($id_mhs) {
        return $this->db->QueryToArray("
        SELECT S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,KUMK.KREDIT_SEMESTER,PMK.NILAI_HURUF,PMK.FLAGNILAI
            ,COUNT(*) OVER(PARTITION BY MK.NM_MATA_KULIAH) STATUS_ULANG,
        ROW_NUMBER() OVER(PARTITION BY PMK.ID_MHS,MK.NM_MATA_KULIAH ORDER BY PMK.NILAI_HURUF) ULANG_KE 
        FROM PENGAMBILAN_MK PMK
        LEFT JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = PMK.ID_KURIKULUM_MK
        LEFT JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
        JOIN SEMESTER S ON S.ID_SEMESTER= PMK.ID_SEMESTER
        WHERE PMK.ID_MHS='{$id_mhs}' AND PMK.STATUS_APV_PENGAMBILAN_MK='1'
        ORDER BY S.TAHUN_AJARAN DESC,S.NM_SEMESTER DESC,MK.NM_MATA_KULIAH
        ");
    }

    function load_mhs_status($id_mhs) {
        $ips = $this->get_ips_mhs($id_mhs, $this->id_semester_sebelum);
        $mhs_status = $this->get_mhs_status($id_mhs);

        $this->db->Query("
        SELECT P.NM_PENGGUNA,M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,PS.NM_PROGRAM_STUDI,'{$ips}' AS IPS,'{$mhs_status['IPK']}' AS IPK,'{$mhs_status['SKSTOTAL']}' AS TOTAL_SKS,J.NM_JENJANG,M.MOBILE_MHS,CMO.TELP_AYAH,CMO.TELP_IBU,
                                (SELECT COUNT(ID_KRP_KHP) 
                                    FROM KRP_KHP kk 
                                    WHERE kk.ID_MHS = m.ID_MHS) AS JML_SKKK,
                                (SELECT SUM(SKOR_KRP_KHP) 
                                    FROM KRP_KHP kk 
                                    WHERE kk.ID_MHS = m.ID_MHS) AS SKOR_SKKK,
                                (SELECT COUNT(ID_FILE_KRP_KHP) 
                                    FROM FILE_KRP_KHP fkk 
                                    JOIN KRP_KHP kk ON kk.ID_KRP_KHP = fkk.ID_KRP_KHP 
                                    WHERE kk.ID_MHS = m.ID_MHS AND fkk.IS_VERIFIED = 0) AS JML_FILE_NOT_VERIFIED,
                                (SELECT COUNT(ID_FILE_KRP_KHP) 
                                    FROM FILE_KRP_KHP fkk 
                                    JOIN KRP_KHP kk ON kk.ID_KRP_KHP = fkk.ID_KRP_KHP 
                                    WHERE kk.ID_MHS = m.ID_MHS AND fkk.IS_VERIFIED = 1) AS JML_FILE_VERIFIED 
            FROM MAHASISWA M
            LEFT JOIN PENGGUNA P ON M.ID_PENGGUNA  = P.ID_PENGGUNA
            LEFT JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            LEFT JOIN CALON_MAHASISWA_BARU CM ON CM.ID_C_MHS = M.ID_C_MHS
            LEFT JOIN CALON_MAHASISWA_ORTU CMO ON CMO.ID_C_MHS = CM.ID_C_MHS 
        WHERE M.ID_MHS ='{$id_mhs}'");
        return $this->db->FetchAssoc();
    }

    function get_mhs_status($id_mhs) {
        $this->db->Query("SELECT PS.ID_FAKULTAS FROM MAHASISWA M JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI WHERE M.ID_MHS='{$id_mhs}'");

        $semester = $this->get_semester_aktif();
        $sem_aktif = $semester['ID_SEMESTER'];

        $tipe_smt="SELECT upper(nm_semester) as nm_semester,
                        case 
                            when (group_semester='Ganjil' and tipe_semester in ('UP','REG','RD')) then thn_akademik_semester || 1  
                            else
                                case 
                                    when (group_semester='Genap' and tipe_semester in ('UP','REG','RD')) then thn_akademik_semester || 2 
                                    else thn_akademik_semester || 4 
                                end 
                        end, fd_id_smt
                    from semester where id_semester='".$sem_aktif."'";

        $this->db->Query($tipe_smt);
        $semester_set = $this->db->FetchAssoc();

        $fd_id_smt = $semester_set['FD_ID_SMT'];

        $sql = "SELECT id_mhs,
                    SUM(sks) AS skstotal, 
                    round(SUM(nilai_mutu) / SUM(sks), 2) AS ipk 
                FROM (
                    SELECT
                        mhs.id_mhs,
                        /* Kode MK utk grouping total mutu */
                        COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
                        /* SKS */
                        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
                        /* Nilai Mutu = SKS * Bobot */
                        COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu,
                        /* Urutan Nilai Terbaik */
                        row_number() OVER (PARTITION BY mhs.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY sn.nilai_standar_nilai DESC) urut
                    FROM pengambilan_mk pmk
                    JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
                    JOIN semester S ON S.id_semester = pmk.id_semester
                    -- Via Kelas
                    LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
                    LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
                    -- Via Kurikulum
                    LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
                    LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
                    -- Penyetaraan
                    LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
                    LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
                    -- nilai bobot
                    JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
                    WHERE 
                        pmk.status_apv_pengambilan_mk = 1 AND 
                        mhs.id_mhs = '".$id_mhs."' AND 
                        S.fd_id_smt <= '".$fd_id_smt."'
                )
                GROUP BY id_mhs";

        $this->db->Query($sql);
        $mhs_status = $this->db->FetchAssoc();
        
        return $mhs_status;
    }

    function update($id_pengambilan_mk, $status_apv) {
        $this->db->Query("update PENGAMBILAN_MK PMK set PMK.STATUS_APV_PENGAMBILAN_MK = '$status_apv' where PMK.ID_PENGAMBILAN_MK ='$id_pengambilan_mk'");
    }

    function update_status_mhs($id_mhs, $id_semester){

            $this->db->Query("SELECT * FROM MAHASISWA_STATUS WHERE ID_MHS='{$id_mhs}' AND ID_SEMESTER = '{$id_semester}'");
            $mhs_status = $this->db->FetchAssoc();
            $id_mhs_status = $mhs_status['ID_MHS_STATUS'];

            $this->db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '{$id_semester}'");
            $smt = $this->db->FetchAssoc();
            $fd_id_smt = $smt['FD_ID_SMT'];

            // query get ipk
            $this->db->Query("SELECT id_mhs,
                                    SUM(sks) AS total_sks, 
                                    round(SUM(nilai_mutu) / SUM(sks), 2) AS ipk 
                                FROM (
                                    SELECT
                                        mhs.id_mhs,
                                        /* Kode MK utk grouping total mutu */
                                        COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
                                        /* SKS */
                                        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
                                        /* Nilai Mutu = SKS * Bobot */
                                        COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu,
                                        /* Urutan Nilai Terbaik */
                                        row_number() OVER (PARTITION BY mhs.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY sn.nilai_standar_nilai DESC) urut
                                    FROM pengambilan_mk pmk
                                    JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
                                    JOIN semester S ON S.id_semester = pmk.id_semester
                                    -- Via Kelas
                                    LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
                                    LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
                                    -- Via Kurikulum
                                    LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
                                    LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
                                    -- Penyetaraan
                                    LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
                                    LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
                                    -- nilai bobot
                                    JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
                                    WHERE 
                                        pmk.status_apv_pengambilan_mk = 1 AND 
                                        mhs.id_mhs = '{$id_mhs}' AND 
                                        S.fd_id_smt <= '{$fd_id_smt}'
                                )
                                GROUP BY id_mhs");
            $data_ipk = $this->db->FetchAssoc();
            $ipk = $data_ipk['IPK'];
            $total_sks = $data_ipk['TOTAL_SKS'];

            // query get ips
            $this->db->Query("SELECT id_mhs, SUM(sks) AS total_sks, round(SUM(nilai_mutu) / sum(sks), 2) as ips from (
                        SELECT
                            mhs.id_mhs,
                            /* Kode MK utk grouping total mutu */
                            COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
                            /* SKS */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
                            /* Nilai Mutu = SKS * Bobot */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu
                        FROM pengambilan_mk pmk
                        JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
                        JOIN semester S ON S.id_semester = pmk.id_semester
                        -- Via Kelas
                        LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
                        LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
                        -- Via Kurikulum
                        LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
                        LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
                        -- nilai bobot
                        JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
                        WHERE 
                            pmk.status_apv_pengambilan_mk = 1 AND 
                            mhs.id_mhs = '{$id_mhs}' AND 
                            S.id_semester = '{$id_semester}'
                    )
                    group by id_mhs");
            $data_ips = $this->db->FetchAssoc();
            $ips = $data_ips['IPS'];
            $total_sks_semester = $data_ips['TOTAL_SKS'];

            if($id_mhs_status == ''){
                $this->db->Query("INSERT INTO MAHASISWA_STATUS (ID_MHS, ID_SEMESTER, ID_STATUS_PENGGUNA, IPS, IPK, SKS_TOTAL, SKS_SEMESTER, CREATED_BY)
                                    VALUES ('{$id_mhs}', '{$id_semester}', '1', '{$ips}', '{$ipk}', '{$total_sks}', '{$total_sks_semester}', '{$this->id_pengguna}'");
            }
            else{
                $this->db->Query("UPDATE MAHASISWA_STATUS SET ID_STATUS_PENGGUNA = 1, IPS = '{$ips}', IPK = '{$ipk}', SKS_TOTAL = '{$total_sks}', SKS_SEMESTER = '{$total_sks_semester}', UPDATED_ON = SYSDATE, UPDATED_BY = '{$this->id_pengguna}' WHERE ID_MHS_STATUS = '{$id_mhs_status}'");
            }
        
    }

    function load_mahasiswa_wali() {
        return $this->db->QueryToArray("
            SELECT M.ID_MHS, M.MOBILE_MHS, P.NM_PENGGUNA,M.NIM_MHS,M.THN_ANGKATAN_MHS,SP.NM_STATUS_PENGGUNA,COUNT(PMK.ID_MHS) STATUS_KRS, JJG.NM_JENJANG FROM DOSEN_WALI DW
            JOIN MAHASISWA M ON DW.ID_MHS = M.ID_MHS
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
			LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
			LEFT JOIN JENJANG JJG ON JJG.ID_JENJANG=PS.ID_JENJANG
            JOIN PENGGUNA P ON P.ID_PENGGUNA =M.ID_PENGGUNA
            LEFT JOIN (
                SELECT DISTINCT(PMK.ID_MHS) FROM
                PENGAMBILAN_MK PMK
                JOIN SEMESTER S ON PMK.ID_SEMESTER= S.ID_SEMESTER
                WHERE S.STATUS_AKTIF_SEMESTER='True'
            ) PMK ON PMK.ID_MHS = M.ID_MHS
            WHERE DW.ID_DOSEN='{$this->id_dosen}' AND SP.STATUS_AKTIF=1 AND DW.STATUS_DOSEN_WALI=1
            GROUP BY M.ID_MHS,M.MOBILE_MHS,P.NM_PENGGUNA,M.NIM_MHS,M.THN_ANGKATAN_MHS,SP.NM_STATUS_PENGGUNA,JJG.NM_JENJANG
        ");
    }

    function load_admisi_mhs($id_mhs) {
        return $this->db->QueryToArray("SELECT AD.*,S.NM_SEMESTER,S.TAHUN_AJARAN,SP.NM_STATUS_PENGGUNA 
                FROM ADMISI AD
                JOIN SEMESTER S ON S.ID_SEMESTER = AD.ID_SEMESTER
                JOIN STATUS_PENGGUNA SP ON AD.STATUS_AKD_MHS = SP.ID_STATUS_PENGGUNA
                WHERE ID_MHS='{$id_mhs}'
                ORDER BY S.TAHUN_AJARAN DESC,S.NM_SEMESTER DESC");
    }

    function load_history_bayar_mhs($nim) {
        $data_history_bayar = array();
        if (is_numeric($nim) && strlen($nim) < 9) {
            $query = "M.ID_MHS='{$nim}'";
        } else {
            $query = "M.NIM_MHS='{$nim}'";
        }
        foreach ($this->db->QueryToArray("
           SELECT SUM(PEM.BESAR_BIAYA+PEM.DENDA_BIAYA) JUMLAH_PEMBAYARAN,PEM.ID_MHS,S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN
                ,S.THN_AKADEMIK_SEMESTER,B.ID_BANK,B.NM_BANK,BV.ID_BANK_VIA,BV.NAMA_BANK_VIA,PEM.NO_TRANSAKSI,TO_CHAR(PEM.TGL_BAYAR,'DD-MON-YY') TGL_BAYAR,PEM.KETERANGAN,PEM.IS_TAGIH,SP.NAMA_STATUS,SP.ID_STATUS_PEMBAYARAN
           FROM PEMBAYARAN PEM 
           JOIN MAHASISWA M ON M.ID_MHS = PEM.ID_MHS
           JOIN SEMESTER S ON S.ID_SEMESTER = PEM.ID_SEMESTER
           LEFT JOIN BANK B ON B.ID_BANK=PEM.ID_BANK
           LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA=PEM.ID_BANK_VIA
           LEFT JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN = PEM.ID_STATUS_PEMBAYARAN 
           WHERE {$query}
           GROUP BY PEM.NO_TRANSAKSI,B.ID_BANK,B.NM_BANK,BV.ID_BANK_VIA,BV.NAMA_BANK_VIA,PEM.TGL_BAYAR,PEM.KETERANGAN,S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,S.THN_AKADEMIK_SEMESTER,SP.ID_STATUS_PEMBAYARAN
           ,PEM.ID_MHS,PEM.IS_TAGIH,SP.NAMA_STATUS
           ORDER BY S.TAHUN_AJARAN DESC,S.NM_SEMESTER DESC") as $data) {
            array_push($data_history_bayar, array(
                'ID_MHS' => $data['ID_MHS'],
                'ID_SEMESTER' => $data['ID_SEMESTER'],
                'NM_SEMESTER' => $data['NM_SEMESTER'],
                'TAHUN_AJARAN' => $data['TAHUN_AJARAN'],
                'JUMLAH_PEMBAYARAN' => $data['JUMLAH_PEMBAYARAN'],
                'NO_TRANSAKSI' => $data['NO_TRANSAKSI'],
                'TGL_BAYAR' => $data['TGL_BAYAR'],
                'NM_BANK' => $data['NM_BANK'],
                'NAMA_BANK_VIA' => $data['NAMA_BANK_VIA'],
                'KETERANGAN' => $data['KETERANGAN'],
                'ID_BANK' => $data['ID_BANK'],
                'ID_BANK_VIA' => $data['ID_BANK_VIA'],
                'IS_TAGIH' => $data['IS_TAGIH'],
                'NAMA_STATUS' => $data['NAMA_STATUS'],
                'ID_STATUS_PEMBAYARAN' => $data['ID_STATUS_PEMBAYARAN'],
                'DATA_PEMBAYARAN' => $this->get_history_bayar_mhs($data['ID_MHS'], $data['ID_SEMESTER'], $data['NO_TRANSAKSI'], $data['TGL_BAYAR'], $data['KETERANGAN'])
            ));
        }
        return $data_history_bayar;
    }

    function get_history_bayar_mhs($id_mhs, $semester, $no_transaksi, $tgl_bayar, $keterangan) {
        $q_no_transaksi = $no_transaksi != '' ? "AND PEM.NO_TRANSAKSI='{$no_transaksi}'" : "AND PEM.NO_TRANSAKSI IS NULL";
        $q_tgl_bayar = $tgl_bayar != '' ? "AND TO_DATE(PEM.TGL_BAYAR)='{$tgl_bayar}'" : "AND PEM.TGL_BAYAR IS NULL";
        $q_keterangan = $keterangan != '' ? "AND PEM.KETERANGAN ='{$keterangan}'" : "AND PEM.KETERANGAN IS NULL";
        return $this->db->QueryToArray("
        SELECT PEM.ID_PEMBAYARAN,B.NM_BIAYA,PEM.BESAR_BIAYA,PEM.DENDA_BIAYA,PEM.TGL_BAYAR,BNK.NM_BANK,BV.NAMA_BANK_VIA,PEM.NO_TRANSAKSI,PEM.KETERANGAN,PEM.IS_TAGIH,SP.NAMA_STATUS
        FROM PEMBAYARAN PEM
        JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN = PEM.ID_STATUS_PEMBAYARAN
        JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
        LEFT JOIN BANK BNK ON BNK.ID_BANK=PEM.ID_BANK
        LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA = PEM.ID_BANK_VIA
        WHERE PEM.ID_MHS='{$id_mhs}' AND PEM.ID_SEMESTER='{$semester}' {$q_tgl_bayar} {$q_no_transaksi} {$q_keterangan}
        ORDER BY PEM.ID_PEMBAYARAN");
    }

    function get_biodata_mahasiswa($id_mhs) {
        $this->db->Query("select NIM_MHS from MAHASISWA where id_mhs='" . $id_mhs . "'");
        $tmp_nim = $this->db->FetchAssoc();
        $mhs_nim = $tmp_nim['NIM_MHS'];

        $mhs_blog = '';
        $this->db->Query("
            SELECT P.NM_PENGGUNA,M.NIM_MHS,M.THN_ANGKATAN_MHS,J.NM_JENJANG,F.NM_FAKULTAS,PS.NM_PROGRAM_STUDI,
                NVL(K.NM_KOTA,P.TEMPAT_LAHIR) TEMPAT_LAHIR,M.MOBILE_MHS,P.KELAMIN_PENGGUNA,P.EMAIL_PENGGUNA, '" . $mhs_blog . "' as BLOG_PENGGUNA,
                M.ALAMAT_MHS,M.ALAMAT_AYAH_MHS,P.FOTO_PENGGUNA,P.TGL_LAHIR_PENGGUNA,M.PENGHASILAN_ORTU_MHS,M.NM_AYAH_MHS
            FROM PENGGUNA P 
            LEFT JOIN KOTA K ON K.ID_KOTA LIKE ''||P.TEMPAT_LAHIR||''
            LEFT JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN CALON_MAHASISWA_BARU CM ON CM.ID_C_MHS = M.ID_C_MHS       
            LEFT JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
            LEFT JOIN FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
            WHERE M.ID_MHS='{$id_mhs}'
            ");
        return $this->db->FetchAssoc();
    }

    function load_mata_kuliah_usulan($kdfak, $kdprodi, $idsmt) {
        $this->db->Query("select id_jenjang from program_studi where id_program_studi='" . $kdprodi . "'");
        $datajenjang = $this->db->FetchAssoc();
        $idjenjang = $datajenjang['ID_JENJANG'];
        if ($kdfak == 8) {
            if ($idjenjang == 5) {
                $datausulmk = $this->db->QueryToArray("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nmkelas nama_kelas,
                nm_jadwal_hari,jm.nm_jadwal_jam||' s/d '||js.nm_jadwal_jam nm_jadwal_jam,nm_ruangan, 
                wm_concat(case when pjmk_pengampu_mk=1 then gelar_depan||nm_pengguna||','||gelar_belakang||' (pjmk)-' else 
                gelar_depan||nm_pengguna||','||gelar_belakang||' (team)-' end ) as pengajar
                from kelas_mk
                left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
                left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
                left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
                left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
                left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
                left join jadwal_jam jm on jadwal_kelas.id_jadwal_jam_mulai=jm.id_jadwal_jam and jm.id_fakultas=8
                left join jadwal_jam js on jadwal_kelas.id_jadwal_jam_selesai=js.id_jadwal_jam and js.id_fakultas=8 
                left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
                left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
                left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
                left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
                where 

                kelas_mk.id_semester={$idsmt} and kelas_mk.id_program_studi={$kdprodi}
                group by kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nmkelas,
                nm_jadwal_hari,jm.nm_jadwal_jam,js.nm_jadwal_jam,nm_ruangan
                order by nm_mata_kuliah");
            } else {

                if ($kdprodi == 107) {
                    $nmk = "A";
                } elseif ($kdprodi == 108) {
                    $nmk = "B";
                } elseif ($kdprodi == 109) {
                    $nmk = "C";
                } elseif ($kdprodi == 110) {
                    $nmk = "D";
                } elseif ($kdprodi == 111) {
                    $nmk = "L";
                } elseif ($kdprodi == 112) {
                    $nmk = "I";
                } elseif ($kdprodi == 113) {
                    $nmk = "T";
                } elseif ($kdprodi == 115) {
                    $nmk = "CC";
                } elseif ($kdprodi == 116) {
                    $nmk = "DD";
                } elseif ($kdprodi == 175) {
                    $nmk = "S";
                }


                $datausulmk = $this->db->QueryToArray("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nmkelas nama_kelas,
                nm_jadwal_hari,jm.nm_jadwal_jam||' s/d '||js.nm_jadwal_jam nm_jadwal_jam,nm_ruangan, 
                wm_concat(case when pjmk_pengampu_mk=1 then gelar_depan||nm_pengguna||','||gelar_belakang||' (pjmk)-' else 
                gelar_depan||nm_pengguna||','||gelar_belakang||' (team)-' end ) as pengajar
                from kelas_mk
                left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
                left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
                left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
                left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
                left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
                left join jadwal_jam jm on jadwal_kelas.id_jadwal_jam_mulai=jm.id_jadwal_jam and jm.id_fakultas=8
                left join jadwal_jam js on jadwal_kelas.id_jadwal_jam_selesai=js.id_jadwal_jam and js.id_fakultas=8 
                left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
                left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
                left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
                left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
                where 
                (
                kelas_mk.id_program_studi=107 or
                kelas_mk.id_program_studi=108 or
                kelas_mk.id_program_studi=109 or
                kelas_mk.id_program_studi=110 or
                kelas_mk.id_program_studi=111 or
                kelas_mk.id_program_studi=112 or
                kelas_mk.id_program_studi=113 or
                kelas_mk.id_program_studi=115 or
                kelas_mk.id_program_studi=116 or
                kelas_mk.id_program_studi=175 
                ) 

                and kelas_mk.id_semester={$idsmt} and nmkelas like '%{$nmk}%' and program_studi.id_jenjang={$idjenjang}
                group by kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nmkelas,
                nm_jadwal_hari,jm.nm_jadwal_jam,js.nm_jadwal_jam,nm_ruangan
                order by nm_mata_kuliah");
            }
            //print_r($datausulmk);
        } else {
            $datausulmk = $this->db->QueryToArray("select kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nama_kelas,
            nm_jadwal_hari,nm_jadwal_jam,nm_ruangan, 
            wm_concat(case when pjmk_pengampu_mk=1 then gelar_depan||nm_pengguna||','||gelar_belakang||' (pjmk)-' else 
            gelar_depan||nm_pengguna||','||gelar_belakang||' (team)-' end ) as pengajar
            from kelas_mk
            left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
            left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
            left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
            left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk 
            left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari 
            left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam 
            left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan 
            left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
            left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
            left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
            where kelas_mk.id_program_studi={$kdprodi} and kelas_mk.id_semester={$idsmt}
            group by kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nama_kelas,
            nm_jadwal_hari,nm_jadwal_jam,nm_ruangan
            order by kd_mata_kuliah");
        }
        return $datausulmk;
    }

    function load_peserta_mata_kuliah($id_kelas) {
        $data_peserta = array();
        $data_mahasiswa = $this->db->QueryToArray("
            SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,M.THN_ANGKATAN_MHS,PMK.STATUS_APV_PENGAMBILAN_MK STATUS_APPROVE FROM PENGAMBILAN_MK PMK
            JOIN MAHASISWA M ON M.ID_MHS = PMK.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            WHERE PMK.ID_KELAS_MK='{$id_kelas}'");
        foreach ($data_mahasiswa as $temp) {
            $mhs_status = $this->get_mhs_status($temp['ID_MHS']);
            $ips_mhs = $this->get_ips_mhs($temp['ID_MHS'], $this->id_semester_sebelum);
            array_push($data_peserta, array(
                'NIM_MHS' => $temp['NIM_MHS'],
                'NM_PENGGUNA' => $temp['NM_PENGGUNA'],
                'ANGKATAN' => $temp['NIM_MHS'],
                'STATUS_APPROVE' => $temp['STATUS_APPROVE'],
                'IPK' => $mhs_status['IPK'],
                'SKS_TOTAL' => $mhs_status['SKSTOTAL'],
                'IPS' => $ips_mhs,
            ));
        }
    }

    function load_data_prodi($fakultas) {
        return $this->db->QueryToArray("
            SELECT J.NM_JENJANG,PS.* 
            FROM PROGRAM_STUDI PS
            JOIN JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
            WHERE PS.ID_FAKULTAS='{$fakultas}'
            ");
    }

    function load_semester() {
        return $this->db->QueryToArray("SELECT * FROM SEMESTER WHERE THN_AKADEMIK_SEMESTER>2008 ORDER BY TAHUN_AJARAN DESC, NM_SEMESTER DESC");
    }

    function load_mahasiswa_sp() {
        $mahasiswa_sp = array();
        $this->db->Open();
        $row = $this->db->QueryToArray("
        SELECT M.ID_MHS,M.NIM_MHS,UPPER(P.NM_PENGGUNA) AS NM_PENGGUNA,M.THN_ANGKATAN_MHS,
                PMK.STATUS_APPROVE,PMK.STATUS_PENGAMBILAN,M.ID_PROGRAM_STUDI,
                ROW_NUMBER() OVER(PARTITION BY M.ID_MHS ORDER BY SDW.TAHUN_AJARAN DESC,SDW.NM_SEMESTER DESC) WALI_KE
            FROM DOSEN_WALI DW
            JOIN MAHASISWA M ON DW.ID_MHS = M.ID_MHS
            JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
            JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA=M.STATUS_AKADEMIK_MHS
            LEFT JOIN (
              SELECT ID_MHS,ID_SEMESTER,
                AVG(STATUS_APV_PENGAMBILAN_MK) STATUS_APPROVE,
                AVG(STATUS_PENGAMBILAN_MK) STATUS_PENGAMBILAN
                FROM PENGAMBILAN_MK
                WHERE ID_KELAS_MK IS NOT NULL
              GROUP BY ID_MHS,ID_SEMESTER
            ) PMK ON PMK.ID_MHS = M.ID_MHS 
            LEFT JOIN SEMESTER S ON PMK.ID_SEMESTER=S.ID_SEMESTER
            LEFT JOIN SEMESTER SDW ON SDW.ID_SEMESTER=DW.ID_SEMESTER
        WHERE DW.ID_DOSEN = '{$this->id_dosen}' AND S.STATUS_AKTIF_SP='True' AND SP.STATUS_AKTIF=1
        ORDER BY THN_ANGKATAN_MHS,NM_PENGGUNA");
        foreach ($row as $temp) {
            if ($temp['WALI_KE'] == '1') {
                $mhs_status = $this->get_mhs_status($temp['ID_MHS']);
                $ips_mhs = $this->get_ips_mhs($temp['ID_MHS'], $this->id_semester_sebelum);
                array_push($mahasiswa_sp, array(
                    'ID_MHS' => $temp['ID_MHS'],
                    'NIM' => $temp['NIM_MHS'],
                    'NAMA' => $temp['NM_PENGGUNA'],
                    'IPS' => $ips_mhs,
                    'IPK' => $mhs_status['IPK'],
                    'TOTAL_SKS' => $mhs_status['SKSTOTAL'],
                    'ANGKATAN' => $temp['THN_ANGKATAN_MHS'],
                    'STATUS' => $temp['STATUS_PENGAMBILAN'],
                    'STATUS_APV' => $temp['STATUS_APPROVE']
                ));
            }
        }
        return $mahasiswa_sp;
    }

    function load_mata_kuliah_sp($id_mhs) {
        return $this->db->QueryToArray("
        SELECT PMK.ID_PENGAMBILAN_MK AS ID_PENGAMBILAN_MK, MK.KD_MATA_KULIAH AS KODE_MK,MK.NM_MATA_KULIAH AS NAMA_MK,
            KUMK.KREDIT_SEMESTER AS SKS,PMK.STATUS_APV_PENGAMBILAN_MK AS STATUS_APV,PMK.STATUS_PENGAMBILAN_MK 
            AS STATUS
            FROM PENGAMBILAN_MK PMK
            LEFT JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
            LEFT JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PMK.ID_KELAS_MK
            LEFT JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
            LEFT JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
            LEFT JOIN SEMESTER S ON PMK.ID_SEMESTER =  S.ID_SEMESTER
        WHERE M.ID_MHS = '{$id_mhs}' AND S.STATUS_AKTIF_SP='True' AND PMK.ID_KELAS_MK IS NOT NULL AND PMK.STATUS_PENGAMBILAN_MK !=0
        GROUP BY PMK.ID_PENGAMBILAN_MK,MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,KUMK.KREDIT_SEMESTER,PMK.STATUS_APV_PENGAMBILAN_MK
        ,PMK.STATUS_PENGAMBILAN_MK
        ORDER BY MK.NM_MATA_KULIAH
        ");
    }

    // Fungsi Load KHS
    function load_khs($id_mhs) {
        $data_khs = array();
        $data_semester = $this->db->QueryToArray("
            SELECT * FROM SEMESTER 
            WHERE (NM_SEMESTER='Ganjil' OR NM_SEMESTER='Genap') 
            AND ID_SEMESTER IN (
                SELECT ID_SEMESTER FROM PENGAMBILAN_MK
                WHERE ID_MHS='{$id_mhs}'
                /* AND NILAI_ANGKA IS NOT NULL AND NILAI_HURUF IS NOT NULL (dimatikan karena ada nilai kosong)*/
                ) 
            ORDER BY TAHUN_AJARAN DESC,NM_SEMESTER DESC
            ");
        foreach ($data_semester as $s) {
            $query_khs = "SELECT 
                    kd_mata_kuliah AS kode,
                    upper(nm_mata_kuliah) AS nama,
                    nama_kelas,
                    tipe_semester,
                    kredit_semester AS sks,
                    /* Status Belum Tampil */
                    CASE WHEN flagnilai = 1 THEN nilai_huruf ELSE '*BT' END AS nilai,
                    bobot,
                    bobot_total,
                    id_pengambilan_mk
                FROM (
                    SELECT 
                        pengambilan_mk.id_mhs,
                        /* Kode MK */
                        COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
                        /* Nama MK */
                        COALESCE(mk1.nm_mata_kuliah, mk2.nm_mata_kuliah) AS nm_mata_kuliah,
                        /* SKS */
                        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS kredit_semester,
                        nama_kelas,
                        tipe_semester,

                        CASE 
                          WHEN pengambilan_mk.nilai_huruf IS NULL THEN '*K' 
                          ELSE pengambilan_mk.nilai_huruf
                        END AS nilai_huruf,

                        /* Bobot */
                        CASE
                          WHEN standar_nilai.nilai_standar_nilai IS NULL THEN 0
                          ELSE standar_nilai.nilai_standar_nilai
                        END AS bobot,

                        /* Bobot Total = Standar Nilai x SKS */
                        COALESCE(standar_nilai.nilai_standar_nilai * kurikulum_mk.kredit_semester, standar_nilai.nilai_standar_nilai * kelas_mk.kredit_semester, 0) AS bobot_total,

                        /* Ranking berdasar kode */
                        row_number() OVER(PARTITION BY pengambilan_mk.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY nilai_huruf) AS rangking,

                        pengambilan_mk.id_pengambilan_mk,
                        pengambilan_mk.flagnilai
                    FROM pengambilan_mk
                    JOIN mahasiswa          ON pengambilan_mk.id_mhs = mahasiswa.id_mhs
                    JOIN semester           ON pengambilan_mk.id_semester = semester.id_semester
                    JOIN program_studi      ON mahasiswa.id_program_studi = program_studi.id_program_studi
                    -- Via Kelas
                    LEFT JOIN kelas_mk      ON pengambilan_mk.id_kelas_mk = kelas_mk.id_kelas_mk
                    LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kelas_mk.id_mata_kuliah
                    LEFT JOIN nama_kelas    ON kelas_mk.no_kelas_mk = nama_kelas.id_nama_kelas
                    -- Via Kurikulum
                    LEFT JOIN kurikulum_mk  ON pengambilan_mk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk
                    LEFT JOIN mata_kuliah mk2 ON kurikulum_mk.id_mata_kuliah = mk2.id_mata_kuliah
                    -- Penyetaraan
                    LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
                    LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
                    LEFT JOIN standar_nilai ON pengambilan_mk.nilai_huruf = standar_nilai.nm_standar_nilai
                    WHERE
                        group_semester||thn_akademik_semester IN (SELECT group_semester||thn_akademik_semester FROM semester WHERE id_semester = '{$s['ID_SEMESTER']}') AND
                        tipe_semester IN ('UP','REG') AND
                        status_apv_pengambilan_mk = 1 AND 
                        pengambilan_mk.status_hapus = 0 AND 
                        pengambilan_mk.status_pengambilan_mk != 0 AND
                        mahasiswa.id_mhs = '" . $id_mhs . "'
                )
                WHERE rangking = 1
                ";
            $mhs_status = $this->get_mhs_status($id_mhs);
            array_push($data_khs, array_merge($s, array(
                'DATA_KHS' => $this->db->QueryToArray($query_khs),
                'IPK' => $mhs_status['IPK'],
                'IPS' => $this->get_ips_mhs($id_mhs, $s['ID_SEMESTER'])
                            )
                    )
            );
        }
        return $data_khs;
    }

    // Fungsi Load KRP
    function load_krp($id_mhs) {
        $data_krp = array();
        $data_semester = $this->db->QueryToArray("
            SELECT * FROM SEMESTER 
            WHERE (NM_SEMESTER='Ganjil' OR NM_SEMESTER='Genap') 
            AND ID_SEMESTER IN (
                SELECT ID_SEMESTER FROM KRP_KHP
                WHERE ID_MHS='{$id_mhs}'
                ) 
            ORDER BY TAHUN_AJARAN DESC,NM_SEMESTER DESC
            ");
        foreach ($data_semester as $s) {
            $query_krp = "SELECT kk.*, k2.BOBOT_KEGIATAN_2, 
            				(SELECT COUNT(ID_KRP_KHP) FROM FILE_KRP_KHP fkk WHERE fkk.ID_KRP_KHP = kk.ID_KRP_KHP) AS ADA_FILE 
                            FROM KRP_KHP kk
                            LEFT JOIN KEGIATAN_2 k2 ON k2.ID_KEGIATAN_2 =  kk.ID_KEGIATAN_2
                            WHERE kk.ID_SEMESTER = '{$s['ID_SEMESTER']}'
                                AND kk.ID_MHS = '{$id_mhs}'
                ";
            $mhs_status = $this->get_mhs_status($id_mhs);
            array_push($data_krp, array_merge($s, array(
                'DATA_KRP' => $this->db->QueryToArray($query_krp)/*,
                'IPK' => $mhs_status['IPK'],
                'IPS' => $this->get_ips_mhs($id_mhs, $s['ID_SEMESTER'])*/
                            )
                    )
            );
        }
        return $data_krp;
    }

     // Fungsi Load FILE KRP
    function load_file_krp($id_mhs,$id_krp_khp) {
        $data_file_krp = array();
        $data_krp_khp = $this->db->QueryToArray("
           SELECT kk.ID_MHS, kk.ID_KRP_KHP, kk.PENYELENGGARA_KRP_KHP, kk.WAKTU_KRP_KHP, kk.NM_KRP_KHP, kk.SKOR_KRP_KHP,
            	k2.BOBOT_KEGIATAN_2, bf.NM_BUKTI_FISIK, t.NM_TINGKAT, jp.NM_JABATAN_PRESTASI,
            	(SELECT DISTINCT(IS_VERIFIED) FROM FILE_KRP_KHP WHERE ID_KRP_KHP = kk.ID_KRP_KHP) AS IS_VERIFIED
            FROM KRP_KHP kk
            LEFT JOIN BUKTI_FISIK bf ON bf.ID_BUKTI_FISIK = kk.ID_BUKTI_FISIK
            JOIN KEGIATAN_2 k2 ON k2.ID_KEGIATAN_2 = kk.ID_KEGIATAN_2
            LEFT JOIN TINGKAT t ON t.ID_TINGKAT = k2.ID_TINGKAT
            LEFT JOIN JABATAN_PRESTASI jp ON jp.ID_JABATAN_PRESTASI = k2.ID_JABATAN_PRESTASI
            WHERE kk.ID_MHS = '{$id_mhs}' 
            	AND kk.ID_KRP_KHP = '{$id_krp_khp}'
            ");
        foreach ($data_krp_khp as $s) {
	        $data_file_krp_set = "SELECT fkk.*, kk.ID_MHS
	            FROM FILE_KRP_KHP fkk
	            JOIN KRP_KHP kk ON kk.ID_KRP_KHP = fkk.ID_KRP_KHP
	            WHERE kk.ID_MHS = '{$id_mhs}' 
	            	AND fkk.ID_KRP_KHP = '{$id_krp_khp}'
	            ORDER BY ID_FILE_KRP_KHP
	            ";
	        array_push($data_file_krp, array_merge($s, array(
	        	'DATA_FILE_KRP' => $this->db->QueryToArray($data_file_krp_set),
	        	'ID_MHS' => $s['ID_MHS'],
	        	'ID_KRP_KHP' => $s['ID_KRP_KHP'],
	            'PENYELENGGARA_KRP_KHP' => $s['PENYELENGGARA_KRP_KHP'],
	            'WAKTU_KRP_KHP' => $s['WAKTU_KRP_KHP'],
	            'NM_KRP_KHP' => $s['NM_KRP_KHP'],
	            'SKOR_KRP_KHP' => $s['SKOR_KRP_KHP'],
	            'BOBOT_KEGIATAN_2' => $s['BOBOT_KEGIATAN_2'],
	            'NM_BUKTI_FISIK' => $s['NM_BUKTI_FISIK'],
	            'NM_TINGKAT' => $s['NM_TINGKAT'],
	            'NM_JABATAN_PRESTASI' => $s['NM_JABATAN_PRESTASI'],
	            'IS_VERIFIED' => $s['IS_VERIFIED']
	            	)
	        	)
	        );
    	}
        return $data_file_krp;
    }

    function load_data_khp_mhs($id) {
        return $this->db->QueryToArray("
        SELECT K1.NM_KEGIATAN_1,KK.*,S.NM_SEMESTER,S.TAHUN_AJARAN,S.THN_AKADEMIK_SEMESTER 
        FROM KRP_KHP KK
        JOIN KEGIATAN_2 K2 ON K2.ID_KEGIATAN_2=KK.ID_KEGIATAN_2
        JOIN KEGIATAN_1 K1 ON K1.ID_KEGIATAN_1=K2.ID_KEGIATAN_1
        JOIN SEMESTER S ON S.ID_SEMESTER=KK.ID_SEMESTER
        WHERE KK.ID_MHS='{$id}'
        ORDER BY kk.WAKTU_KRP_KHP
        ");
    }

    // DATA BIDIK MISI MAHASISWA

    function load_sejarah_beasiswa($id_mhs) {
        $query = "SELECT SB.*,B.NM_BEASISWA,B.PENYELENGGARA_BEASISWA
                FROM SEJARAH_BEASISWA SB
                LEFT JOIN BEASISWA B ON SB.ID_BEASISWA=B.ID_BEASISWA
                WHERE SB.ID_MHS='{$id_mhs}'";
        return $this->db->QueryToArray($query);
    }

    function load_bidik_misi_kegiatan($id_mhs) {
        $query = "SELECT *
                FROM BIDIKMISI_KEGIATAN
                WHERE ID_MHS='{$id_mhs}'";
        return $this->db->QueryToArray($query);
    }

    function load_bidik_misi_keuangan($id_mhs) {
        $query = "SELECT *
                FROM BIDIKMISI_KEU BKEU
                JOIN BIDIKMISI_KEU_POS BKP ON BKEU.ID_BIDIKMISI_KEU_POS=BKP.ID_BIDIKMISI_KEU_POS
                WHERE BKEU.ID_MHS='{$id_mhs}'";
        return $this->db->QueryToArray($query);
    }

}
