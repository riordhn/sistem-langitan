<?php

class pesan {

    public $db;
    public $id_pengguna;
    public $id_dosen;

    function __construct($db, $id_dosen, $id_pengguna) {
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
        $this->id_dosen = $id_dosen;
    }

    function load_dosen($id_fakultas) {
        $data_dosen = array();
        $this->db->Query("SELECT P.ID_PENGGUNA AS ID,P.NM_PENGGUNA AS NAMA FROM DOSEN D
        JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
		JOIN PROGRAM_STUDI PS ON D.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
		JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
		WHERE F.ID_FAKULTAS='{$id_fakultas}'
		ORDER BY P.NM_PENGGUNA");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_dosen, array(
                'id' => $temp['ID'],
                'nama' => $temp['NAMA']
            ));
        }
        return $data_dosen;
    }
	
	function load_fakultas(){
		return $this->db->QueryToArray("SELECT * FROM FAKULTAS ORDER BY ID_FAKULTAS");
	}

    function load_ortu() {
        $data_ortu = array();
        $this->db->Query("select distinct P1.ID_PENGGUNA as id,P1.NM_PENGGUNA as nama,O.HUBUNGAN as hubungan,P2.NM_PENGGUNA as nama_mhs from ORTU O
        join PENGGUNA P1 on P1.ID_PENGGUNA = O.ID_PENGGUNA
        join DOSEN_WALI DW on DW.ID_MHS =  O.ID_MHS
        join MAHASISWA M on M.ID_MHS = O.ID_MHS
        join PENGGUNA P2 on P2.ID_PENGGUNA = M.ID_PENGGUNA
        where DW.ID_DOSEN = '$this->id_dosen'");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_ortu, array(
                'id' => $temp['ID'],
                'nama' => $temp['NAMA'],
                'hubungan' => $temp['HUBUNGAN'],
                'nama_mhs' => $temp['NAMA_MHS']
            ));
        }
        return $data_ortu;
    }

    function load_mahasiswa() {
        $data_mahasiswa = array();
        $this->db->Query("select distinct P.ID_PENGGUNA as id,P.NM_PENGGUNA as nama, p.username as username, p.foto_pengguna as foto 
        from MAHASISWA M
        join STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA=M.STATUS_AKADEMIK_MHS
        join PENGGUNA P on P.ID_PENGGUNA = M.ID_PENGGUNA
        join DOSEN_WALI DW on DW.ID_MHS =  M.ID_MHS
        where DW.ID_DOSEN = '{$this->id_dosen}' AND SP.STATUS_AKTIF=1");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_mahasiswa, array(
                'id' => $temp['ID'],
                'nama' => $temp['NAMA']
            ));
        }
        return $data_mahasiswa;
    }

    function load_conversation_dosen() {
        $data_conversation_dosen = array();
        $this->db->Query("select PES.ID_PESAN as id_pesan,PES.ID_BALASAN as id_balasan,PES.ID_PENERIMA as id_penerima,PES.ID_PENGIRIM as id_pengirim,P1.NM_PENGGUNA as nama_pengirim,P2.NM_PENGGUNA as nama_penerima,PES.JUDUL_PESAN as judul,PES.ISI_PESAN as isi,to_char(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') as waktu,PES.HAPUS_PENGIRIM,PES.HAPUS_PENERIMA from PESAN PES
        join PENGGUNA P1 on P1.ID_PENGGUNA = PES.ID_PENGIRIM
        join DOSEN D1 on D1.ID_PENGGUNA = P1.ID_PENGGUNA
        join PENGGUNA P2 on P2.ID_PENGGUNA = PES.ID_PENERIMA
        join DOSEN D2 on D2.ID_PENGGUNA = P2.ID_PENGGUNA
        where (D2.ID_DOSEN='$this->id_dosen' or D1.ID_DOSEN='$this->id_dosen')
        order by PES.WAKTU_PESAN desc");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_conversation_dosen, array(
                'id_pesan' => $temp['ID_PESAN'],
                'id_balasan' => $temp['ID_BALASAN'],
                'id_penerima' => $temp['ID_PENERIMA'],
                'id_pengirim' => $temp['ID_PENGIRIM'],
                'nama_pengirim' => $temp['NAMA_PENGIRIM'],
                'nama_penerima' => $temp['NAMA_PENERIMA'],
                'judul' => $this->cut_long_text($temp['JUDUL']),
                'isi' => $this->cut_long_text($temp['ISI']),
                'waktu' => $temp['WAKTU'],
                'hapus_pengirim' => $temp['HAPUS_PENGIRIM'],
                'hapus_penerima' => $temp['HAPUS_PENERIMA']
            ));
        }

        return $data_conversation_dosen;
    }

    function load_conversation_mahasiswa() {
        $data_conversation_mahasiswa = array();
        $this->db->Query("select * from (select PES.ID_PESAN as id_pesan,PES.ID_BALASAN as id_balasan,PES.ID_PENERIMA as id_penerima,PES.ID_PENGIRIM as id_pengirim,P1.NM_PENGGUNA as nama_pengirim,P2.NM_PENGGUNA as nama_penerima,PES.JUDUL_PESAN as judul,PES.ISI_PESAN as isi,to_char(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') as waktu,PES.HAPUS_PENGIRIM,PES.HAPUS_PENERIMA from PESAN PES
        join DOSEN D on D.ID_PENGGUNA = PES.ID_PENGIRIM
        join MAHASISWA M on M.ID_PENGGUNA = PES.ID_PENERIMA
        join PENGGUNA P1 on P1.ID_PENGGUNA = PES.ID_PENGIRIM
        join PENGGUNA P2 on P2.ID_PENGGUNA = PES.ID_PENERIMA
        where D.ID_DOSEN ='$this->id_dosen'
        union
        select PES.ID_PESAN as id_pesan,PES.ID_BALASAN as id_balasan,PES.ID_PENERIMA as id_penerima,PES.ID_PENGIRIM as id_pengirim,P1.NM_PENGGUNA as nama_pengirim,P2.NM_PENGGUNA as nama_penerima,PES.JUDUL_PESAN as judul,PES.ISI_PESAN as isi,to_char(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') as waktu,PES.HAPUS_PENGIRIM,PES.HAPUS_PENERIMA from PESAN PES
        join DOSEN D on D.ID_PENGGUNA = PES.ID_PENERIMA
        join MAHASISWA M on M.ID_PENGGUNA = PES.ID_PENGIRIM
        join PENGGUNA P1 on P1.ID_PENGGUNA = PES.ID_PENGIRIM
        join PENGGUNA P2 on P2.ID_PENGGUNA = PES.ID_PENERIMA
        where D.ID_DOSEN ='$this->id_dosen') a
        order by a.ID_PESAN desc");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_conversation_mahasiswa, array(
                'id_pesan' => $temp['ID_PESAN'],
                'id_balasan' => $temp['ID_BALASAN'],
                'id_penerima' => $temp['ID_PENERIMA'],
                'id_pengirim' => $temp['ID_PENGIRIM'],
                'nama_pengirim' => $temp['NAMA_PENGIRIM'],
                'nama_penerima' => $temp['NAMA_PENERIMA'],
                'judul' => $this->cut_long_text($temp['JUDUL']),
                'isi' => $this->cut_long_text($temp['ISI']),
                'waktu' => $temp['WAKTU'],
                'hapus_pengirim' => $temp['HAPUS_PENGIRIM'],
                'hapus_penerima' => $temp['HAPUS_PENERIMA']
            ));
        }
        return $data_conversation_mahasiswa;
    }

    function load_conversation_ortu() {
        $data_conversation_ortu = array();
        $this->db->Query("select * from (select PES.ID_PESAN as id_pesan,PES.ID_BALASAN as id_balasan,PES.ID_PENERIMA as id_penerima,PES.ID_PENGIRIM as id_pengirim,P1.NM_PENGGUNA as nama_pengirim,P2.NM_PENGGUNA as nama_penerima,PES.JUDUL_PESAN as judul,PES.ISI_PESAN as isi,to_char(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') as waktu,PES.HAPUS_PENGIRIM,PES.HAPUS_PENERIMA  from PESAN PES
        join ORTU O on PES.ID_PENGIRIM = O.ID_PENGGUNA
        join DOSEN D on D.ID_PENGGUNA =PES.ID_PENERIMA
        join PENGGUNA P1 on P1.ID_PENGGUNA = O.ID_PENGGUNA
        join PENGGUNA P2 on P2.ID_PENGGUNA = D.ID_PENGGUNA
        where D.ID_DOSEN = '$this->id_dosen'
        union
        select PES.ID_PESAN as id_pesan,PES.ID_BALASAN as id_balasan,PES.ID_PENERIMA as id_penerima,PES.ID_PENGIRIM as id_pengirim,P2.NM_PENGGUNA as nama_pengirim,P1.NM_PENGGUNA as nama_penerima,PES.JUDUL_PESAN as judul,PES.ISI_PESAN as isi,to_char(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') as waktu,PES.HAPUS_PENGIRIM,PES.HAPUS_PENERIMA  from PESAN PES
        join ORTU O on PES.ID_PENERIMA = O.ID_PENGGUNA
        join DOSEN D on D.ID_PENGGUNA =PES.ID_PENGIRIM
        join PENGGUNA P1 on P1.ID_PENGGUNA = O.ID_PENGGUNA
        join PENGGUNA P2 on P2.ID_PENGGUNA = D.ID_PENGGUNA
        where D.ID_DOSEN = '$this->id_dosen') a
        order by a.ID_PESAN desc
        ");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_conversation_ortu, array(
                'id_pesan' => $temp['ID_PESAN'],
                'id_balasan' => $temp['ID_BALASAN'],
                'id_penerima' => $temp['ID_PENERIMA'],
                'id_pengirim' => $temp['ID_PENGIRIM'],
                'nama_pengirim' => $temp['NAMA_PENGIRIM'],
                'nama_penerima' => $temp['NAMA_PENERIMA'],
                'judul' => $this->cut_long_text($temp['JUDUL']),
                'isi' => $this->cut_long_text($temp['ISI']),
                'waktu' => $temp['WAKTU'],
                'hapus_pengirim' => $temp['HAPUS_PENGIRIM'],
                'hapus_penerima' => $temp['HAPUS_PENERIMA']
            ));
        }
        return $data_conversation_ortu;
    }
    function load_balasan($id_pesan) {
        $data_balasan = array();
        $this->db->Query("SELECT PES.ID_PESAN,PES.ID_PENGIRIM,PES.ID_PENERIMA,PES.ID_BALASAN,P1.NM_PENGGUNA AS NAMA_PENERIMA,P2.NM_PENGGUNA AS NAMA_PENGIRIM,PES.JUDUL_PESAN AS JUDUL,PES.ISI_PESAN AS ISI,TO_CHAR(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') AS WAKTU,PES.HAPUS_PENGIRIM,PES.HAPUS_PENERIMA,PES.TIPE_PESAN
		FROM PESAN PES 
        JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = PES.ID_PENERIMA
        JOIN PENGGUNA P1 ON P1.ID_PENGGUNA = PES.ID_PENGIRIM
        WHERE PES.ID_BALASAN ='$id_pesan'
        ORDER BY PES.ID_PESAN");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_balasan, array(
                'id_pesan' => $temp['ID_PESAN'],
                'id_balasan' => $temp['ID_BALASAN'],
                'id_penerima' => $temp['ID_PENERIMA'],
                'id_pengirim' => $temp['ID_PENGIRIM'],
                'nama_pengirim' => $temp['NAMA_PENGIRIM'],
                'nama_penerima' => $temp['NAMA_PENERIMA'],
                'judul' => $this->cut_long_text($temp['JUDUL']),
                'isi' => $this->cut_long_text($temp['ISI']),
                'waktu' => $temp['WAKTU'],
                'hapus_pengirim' => $temp['HAPUS_PENGIRIM'],
                'hapus_penerima' => $temp['HAPUS_PENERIMA']
            ));
        }
        return $data_balasan;
    }

    function load_inbox() {
        $data_inbox = array();
        $this->db->Query("
        SELECT PES.ID_PESAN AS ID_PESAN,PES.ID_BALASAN AS ID_BALASAN,PES.ID_PENERIMA AS ID_PENERIMA,PES.ID_PENGIRIM AS ID_PENGIRIM,PES.BACA_PENERIMA,
        P1.NM_PENGGUNA AS NAMA_PENERIMA,P2.NM_PENGGUNA AS NAMA_PENGIRIM,PES.JUDUL_PESAN AS JUDUL,PES.ISI_PESAN AS ISI,
        TO_CHAR(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') AS WAKTU,PES.HAPUS_PENGIRIM,PES.HAPUS_PENERIMA,PES.TIPE_PESAN
	FROM PESAN PES
        JOIN DOSEN D ON D.ID_PENGGUNA = PES.ID_PENERIMA
        JOIN PENGGUNA P1 ON P1.ID_PENGGUNA = D.ID_PENGGUNA
        JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = PES.ID_PENGIRIM
        WHERE D.ID_DOSEN = '$this->id_dosen' AND (PES.HAPUS_PENERIMA !=3 OR PES.HAPUS_PENERIMA IS NULL)
        ORDER BY PES.WAKTU_PESAN DESC");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_inbox, array(
                'id_pesan' => $temp['ID_PESAN'],
                'id_balasan' => $temp['ID_BALASAN'],
                'id_penerima' => $temp['ID_PENERIMA'],
                'id_pengirim' => $temp['ID_PENGIRIM'],
                'nama_pengirim' => $temp['NAMA_PENGIRIM'],
                'nama_penerima' => $temp['NAMA_PENERIMA'],
                'judul' => $this->cut_long_text($temp['JUDUL']),
                'isi' => $this->cut_long_text($temp['ISI']),
                'waktu' => $temp['WAKTU'],
                'baca_penerima' => $temp['BACA_PENERIMA'],
                'hapus_pengirim' => $temp['HAPUS_PENGIRIM'],
                'hapus_penerima' => $temp['HAPUS_PENERIMA'],
                'tipe_pesan' => $temp['TIPE_PESAN']
            ));
        }
        return $data_inbox;
    }
    
    function load_unread_inbox() {
        $data_inbox=$this->db->QueryToArray("
        SELECT PES.ID_PESAN AS ID_PESAN,PES.ID_BALASAN AS ID_BALASAN,PES.ID_PENERIMA AS ID_PENERIMA,PES.ID_PENGIRIM AS ID_PENGIRIM,PES.BACA_PENERIMA,
        P1.NM_PENGGUNA AS NAMA_PENERIMA,P2.NM_PENGGUNA AS NAMA_PENGIRIM,PES.JUDUL_PESAN AS JUDUL,PES.ISI_PESAN AS ISI,
        TO_CHAR(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') AS WAKTU,PES.HAPUS_PENGIRIM,PES.HAPUS_PENERIMA,PES.TIPE_PESAN
	FROM PESAN PES
        JOIN DOSEN D ON D.ID_PENGGUNA = PES.ID_PENERIMA
        JOIN PENGGUNA P1 ON P1.ID_PENGGUNA = D.ID_PENGGUNA
        JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = PES.ID_PENGIRIM
        WHERE D.ID_DOSEN = '{$this->id_dosen}' 
        AND (PES.HAPUS_PENERIMA !=3 OR PES.HAPUS_PENERIMA IS NULL)
        AND (PES.BACA_PENERIMA IS NULL OR PES.BACA_PENERIMA=0)");  
        return $data_inbox;
    }

    function load_sent_item() {
        $data_sent_item = array();
        $this->db->Query("SELECT PES.ID_PESAN AS ID_PESAN,PES.ID_BALASAN AS ID_BALASAN,PES.ID_PENERIMA AS ID_PENERIMA,PES.ID_PENGIRIM AS ID_PENGIRIM,P2.NM_PENGGUNA AS NAMA_PENERIMA,P1.NM_PENGGUNA AS NAMA_PENGIRIM,PES.JUDUL_PESAN AS JUDUL,PES.ISI_PESAN AS ISI,TO_CHAR(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') AS WAKTU,PES.HAPUS_PENGIRIM,PES.HAPUS_PENERIMA,PES.TIPE_PESAN
		FROM PESAN PES
        JOIN DOSEN D ON D.ID_PENGGUNA = PES.ID_PENGIRIM
        JOIN PENGGUNA P1 ON P1.ID_PENGGUNA = D.ID_PENGGUNA
        JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = PES.ID_PENERIMA
        WHERE D.ID_DOSEN = '$this->id_dosen' AND (PES.HAPUS_PENGIRIM !=3 OR PES.HAPUS_PENGIRIM IS NULL)
        ORDER BY PES.WAKTU_PESAN DESC");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_sent_item, array(
                'id_pesan' => $temp['ID_PESAN'],
                'id_balasan' => $temp['ID_BALASAN'],
                'id_penerima' => $temp['ID_PENERIMA'],
                'id_pengirim' => $temp['ID_PENGIRIM'],
                'nama_pengirim' => $temp['NAMA_PENGIRIM'],
                'nama_penerima' => $temp['NAMA_PENERIMA'],
                'judul' => $this->cut_long_text($temp['JUDUL']),
                'isi' => $this->cut_long_text($temp['ISI']),
                'waktu' => $temp['WAKTU'],
                'hapus_pengirim' => $temp['HAPUS_PENGIRIM'],
                'hapus_penerima' => $temp['HAPUS_PENERIMA'],
				'tipe_pesan' => $temp['TIPE_PESAN']
            ));
        }
        return $data_sent_item;
    }

    function load_trash() {
        $data_trash = array();
        $this->db->Query("SELECT PES.ID_PESAN AS ID_PESAN,PES.ID_BALASAN AS ID_BALASAN,PES.ID_PENERIMA AS ID_PENERIMA,PES.ID_PENGIRIM AS ID_PENGIRIM,P1.NM_PENGGUNA AS NAMA_PENERIMA,P2.NM_PENGGUNA AS NAMA_PENGIRIM,PES.JUDUL_PESAN AS JUDUL,PES.ISI_PESAN AS ISI,TO_CHAR(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') AS WAKTU,PES.HAPUS_PENGIRIM,PES.HAPUS_PENERIMA,PES.TIPE_PESAN
		FROM PESAN PES
        JOIN PENGGUNA P1 ON P1.ID_PENGGUNA = PES.ID_PENERIMA
        JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = PES.ID_PENGIRIM
        LEFT JOIN DOSEN D1 ON D1.ID_PENGGUNA = P1.ID_PENGGUNA
        LEFT JOIN DOSEN D2 ON D2.ID_PENGGUNA = P2.ID_PENGGUNA
        WHERE (D1.ID_DOSEN ='$this->id_dosen' OR D2.ID_DOSEN='$this->id_dosen') AND (PES.HAPUS_PENERIMA = 3 OR PES.HAPUS_PENGIRIM=3)
        order by PES.ID_PESAN desc");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_trash, array(
                'id_pesan' => $temp['ID_PESAN'],
                'id_balasan' => $temp['ID_BALASAN'],
                'id_penerima' => $temp['ID_PENERIMA'],
                'id_pengirim' => $temp['ID_PENGIRIM'],
                'nama_pengirim' => $temp['NAMA_PENGIRIM'],
                'nama_penerima' => $temp['NAMA_PENERIMA'],
                'judul' => $this->cut_long_text($temp['JUDUL']),
                'isi' => $this->cut_long_text($temp['ISI']),
                'waktu' => $temp['WAKTU'],
                'hapus_pengirim' => $temp['HAPUS_PENGIRIM'],
                'hapus_penerima' => $temp['HAPUS_PENERIMA'],
				'tipe_pesan' => $temp['TIPE_PESAN']
            ));
        }
        return $data_trash;
    }

    function delete_pesan_penerima($id) {
        $this->db->Query("UPDATE PESAN PES SET PES.HAPUS_PENERIMA='3' WHERE PES.ID_PESAN='$id'");
    }

    function delete_pesan_pengirim($id) {
        $this->db->Query("UPDATE PESAN PES SET PES.HAPUS_PENGIRIM='3' WHERE PES.ID_PESAN='$id'");
    }

    function load_pesan($id) {
        $data_pesan = array();
        $this->db->Query("SELECT PES.ID_PESAN,PES.ID_PENERIMA,PES.ID_PENGIRIM,P1.NM_PENGGUNA AS NAMA_PENGIRIM,P2.NM_PENGGUNA AS NAMA_PENERIMA,PES.JUDUL_PESAN AS JUDUL,PES.ISI_PESAN AS ISI,TO_CHAR(PES.WAKTU_PESAN,'DD-MM-YYYY HH24:MI:SS') AS WAKTU FROM PESAN PES
        JOIN PENGGUNA P1 ON P1.ID_PENGGUNA = PES.ID_PENGIRIM
        JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = PES.ID_PENERIMA
        WHERE PES.ID_PESAN='$id'");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_pesan, array(
                'id_pesan' => $temp['ID_PESAN'],
                'id_penerima' => $temp['ID_PENERIMA'],
                'id_pengirim' => $temp['ID_PENGIRIM'],
                'nama_pengirim' => $temp['NAMA_PENGIRIM'],
                'nama_penerima' =>$temp['NAMA_PENERIMA'],
                'isi'=>$temp['ISI'],
                'judul' => $this->cut_long_text($temp['JUDUL'])
            ));
        }
        return $data_pesan;
    }

    function insert_pesan($id_penerima, $judul, $isi, $id_balasan) {
        $this->db->Query("INSERT INTO PESAN (ID_PENGIRIM,ID_PENERIMA,JUDUL_PESAN,ISI_PESAN,ID_BALASAN,WAKTU_PESAN,BACA_PENGIRIM,BACA_PENERIMA,HAPUS_PENGIRIM,HAPUS_PENERIMA) values ('$this->id_pengguna','$id_penerima','$judul','$isi','$id_balasan',to_date('" . date('Ymd His') . "','YYYYMMDD HH24MISS '),0,0,0,0)");
    }

    function cut_long_text($string) {
        if (strpos($string, ' ') == false && strlen($string) > 20) {
            $arr_str = str_split($string, 20);
            $string = implode(' ', $arr_str);
        }
        return $string;
    }

    function restore_pesan($id) {
        $this->db->Query("UPDATE PESAN PES set PES.HAPUS_PENGIRIM='0',PES.HAPUS_PENERIMA='0' where PES.ID_PESAN='$id' ");
    }

    function delete_permanen($id) {
        $this->db->Query("DELETE PESAN PES where PES.ID_PESAN='$id'");
    }

}

?>
