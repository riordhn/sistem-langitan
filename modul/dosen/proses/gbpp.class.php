<?php

class gbpp {

    public $db;
    public $id_dosen;

    function __construct($db, $id_dosen) {
        $this->db = $db;
        $this->id_dosen = $id_dosen;
    }

    function load_mata_kuliah() {
        return $this->db->QueryToArray("
            SELECT KUMK.ID_KURIKULUM_MK,MK.NM_MATA_KULIAH,J.NM_JENJANG,PS.NM_PROGRAM_STUDI 
                FROM PENGAMPU_MK PENGMK
            JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
            JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
            JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = MK.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE PENGMK.ID_DOSEN='{$this->id_dosen}' AND PENGMK.PJMK_PENGAMPU_MK=1
            GROUP BY KUMK.ID_KURIKULUM_MK,MK.NM_MATA_KULIAH,J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
    }
    
    function load_prasyarat_mk($id_kurikulum_mk){
        return $this->db->QueryToArray("
            SELECT MK.* FROM PRASYARAT_MK PSMK
            JOIN GROUP_PRASYARAT_MK GPMK ON GPMK.ID_PRASYARAT_MK = PSMK.ID_PRASYARAT_MK
            JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = GPMK.ID_KURIKULUM_MK
            JOIN MATA_KULIAH MK ON KUMK.ID_MATA_KULIAH = MK.ID_MATA_KULIAH
            WHERE PSMK.ID_KURIKULUM_MK='{$id_kurikulum_mk}'");
    }

    function load_referensi_mk($id_kurikulum_mk) {
        return $this->db->QueryToArray("
            SELECT * FROM REFERENSI_MK WHERE ID_KURIKULUM_MK='{$id_kurikulum_mk}' ORDER BY REFERENSI_MK");
    }

    function get_referensi_mk($id_referensi_mk) {
        $this->db->Query("SELECT * FROM REFERENSI_MK WHERE ID_REFERENSI_MK='{$id_referensi_mk}'");
        return $this->db->FetchAssoc();
    }

    function insert_referensi_mk($id_kurikulum_mk, $referensi) {
        $this->db->Query("
            INSERT INTO REFERENSI_MK
                (ID_KURIKULUM_MK,REFERENSI_MK,TGL_UPDATE)
            VALUES 
                ('{$id_kurikulum_mk}','{$referensi}',TO_CHAR(CURRENT_DATE, 'DD-MON-YYYY'))");
    }

    function update_referensi_mk($id_referensi_mk, $referensi) {
        $this->db->Query("
            UPDATE REFERENSI_MK 
            SET
                REFERENSI_MK='{$referensi}',
                TGL_UPDATE=TO_CHAR(CURRENT_DATE, 'DD-MON-YYYY')
            WHERE ID_REFERENSI_MK='{$id_referensi_mk}'");
    }
    
    function delete_referensi_mk($id_refrensi_mk){
        $this->db->Query("DELETE FROM REFERENSI_MK WHERE ID_REFERENSI_MK='{$id_refrensi_mk}'");
    }

    function load_gbpp_mk($id_kurikulum_mk) {
        return $this->db->QueryToArray("
            SELECT * FROM GBPP_MATA_KULIAH WHERE ID_KURIKULUM_MK='{$id_kurikulum_mk}'
            ORDER BY URUTAN_GBPP");
    }

    function get_gbpp_mk($id_gbpp_mata_kuliah) {
        $this->db->Query("SELECT * FROM GBPP_MATA_KULIAH WHERE ID_GBPP_MATA_KULIAH='{$id_gbpp_mata_kuliah}'");
        return $this->db->FetchAssoc();
    }

    function insert_gbpp_mk($id_kurikulum_mk,$urutan, $tik, $pokok, $sub_pokok) {
        $this->db->Query("
            INSERT INTO GBPP_MATA_KULIAH
                (ID_KURIKULUM_MK,URUTAN_GBPP,TIK_MATA_KULIAH,POKOK_BAHASAN,SUB_POKOK_BAHASAN,TGL_UPDATE)
            VALUES
                ('{$id_kurikulum_mk}','{$urutan}','{$tik}','{$pokok}','{$sub_pokok}',TO_CHAR(CURRENT_DATE, 'DD-MON-YYYY'))");
    }
    
    function update_gbpp_mk($id_gbpp_mk,$urutan, $tik, $pokok, $sub_pokok){
        $this->db->Query("
            UPDATE GBPP_MATA_KULIAH
            SET
               URUTAN_GBPP='{$urutan}',
               TIK_MATA_KULIAH='{$tik}',
               POKOK_BAHASAN='{$pokok}',
               SUB_POKOK_BAHASAN='{$sub_pokok}'
           WHERE ID_GBPP_MATA_KULIAH='{$id_gbpp_mk}'");
    }
    
    function delete_gbpp_mk($id_gbpp_mk){
        $this->db->Query("DELETE FROM GBPP_MATA_KULIAH WHERE ID_GBPP_MATA_KULIAH='{$id_gbpp_mk}'");
    }

    function cek_deskripsi_mk($id_kurikulum_mk) {
        return $this->db->QueryToArray("SELECT * FROM DESKRIPSI_MK WHERE ID_KURIKULUM_MK='{$id_kurikulum_mk}'");
    }

    function get_deskripsi_mk($id_kurikulum_mk) {
        $this->db->Query("
            SELECT KUMK.ID_KURIKULUM_MK,MK.NM_MATA_KULIAH,P.NM_PENGGUNA,NVL(MK.KD_MATA_KULIAH,MK.KD_MATA_KULIAH2) KD_MATA_KULIAH,MK.KREDIT_SEMESTER,KUMK.TINGKAT_SEMESTER,
                DMK.DESKRIPSI_MK,DMK.TIU_MATA_KULIAH,DMK.TGL_UPDATE,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
            FROM KURIKULUM_MK KUMK
            LEFT JOIN DESKRIPSI_MK DMK ON KUMK.ID_KURIKULUM_MK = DMK.ID_KURIKULUM_MK
            JOIN MATA_KULIAH MK ON KUMK.ID_MATA_KULIAH = MK.ID_MATA_KULIAH
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = MK.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            LEFT JOIN DOSEN D ON DMK.ID_DOSEN = D.ID_DOSEN
            LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
            WHERE KUMK.ID_KURIKULUM_MK='{$id_kurikulum_mk}'");
        return $this->db->FetchAssoc();
    }

    function insert_deskripsi_mk($id_kurikulum_mk, $deskripsi, $tiu) {
        $this->db->Query("
            INSERT INTO DESKRIPSI_MK
                (ID_KURIKULUM_MK,DESKRIPSI_MK,TIU_MATA_KULIAH,TGL_UPDATE)
            VALUES
                ('{$id_kurikulum_mk}','$deskripsi','{$tiu}',TO_CHAR(CURRENT_DATE, 'DD-MON-YYYY'))");
    }

    function update_deskripsi_mk($id_kurikulum_mk, $deskripsi, $tiu) {
        $this->db->Query("
            UPDATE DESKRIPSI_MK
            SET 
                DESKRIPSI_MK='{$deskripsi}',
                TIU_MATA_KULIAH='{$tiu}',
                TGL_UPDATE=TO_CHAR(CURRENT_DATE, 'DD-MON-YYYY')
            WHERE ID_KURIKULUM_MK='{$id_kurikulum_mk}'");
    }

}

?>