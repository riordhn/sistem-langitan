<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pengmas
 *
 * @author UUAC25
 */
class pengmas {

    public $id_dosen;
    public $db;

    function __construct($db, $id) {
        $this->db = $db;
        $this->id_dosen = $id;
    }

    function load() {
        return $this->db->QueryToArray("SELECT * FROM DOSEN_PENGMAS WHERE ID_DOSEN ='{$this->id_dosen}'");
    }
	
	function get($id){
		$this->db->Query("SELECT * FROM DOSEN_PENGMAS WHERE ID_DOSEN_PENGMAS='{$id}'");
		return $this->db->FetchAssoc();
	}

    function insert($nama, $tempat, $bidang, $peran, $tahun, $sumber_dana, $jml_dana, $tingkat, $output) {
        $this->db->Query("
		INSERT INTO DOSEN_PENGMAS 
			(ID_DOSEN,NM_DOSEN_PENGMAS,TEMPAT_DOSEN_PENGMAS,BIDANG_DOSEN_PENGMAS,PERAN_DOSEN_PENGMAS,THN_DOSEN_PENGMAS, 
				DANA_DOSEN_PENGMAS, SUMBER_DANA_DOSEN_PENGMAS, TINGKAT_DOSEN_PENGMAS, OUTPUT_DOSEN_PENGMAS) 
		VALUES 
			('{$this->id_dosen}','{$nama}','{$tempat}','{$bidang}','{$peran}','{$tahun}',
			'{$jml_dana}', '{$sumber_dana}', '{$tingkat}', '{$output}')");
	}

    function update($id, $nama, $tempat, $bidang, $peran, $tahun) {
        $this->db->Query("
		UPDATE DOSEN_PENGMAS SET 
			NM_DOSEN_PENGMAS='{$nama}',
			TEMPAT_DOSEN_PENGMAS ='{$tempat}',
			BIDANG_DOSEN_PENGMAS='{$bidang}',
			PERAN_DOSEN_PENGMAS='{$peran}',
			THN_DOSEN_PENGMAS='{$tahun}' 
		WHERE ID_DOSEN_PENGMAS = '{$id}'");
	}
	
	function delete($id){
		$this->db->Query("DELETE FROM DOSEN_PENGMAS WHERE ID_DOSEN_PENGMAS={$id}");
	}

}

?>
