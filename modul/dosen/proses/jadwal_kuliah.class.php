<?php

class jadwal_kuliah {

    public $db;
    public $id_dosen;
    public $id_semester;

    function __construct($db, $id_dosen, $id_semester) {
        $this->db = $db;
        $this->id_dosen = $id_dosen;
        $this->id_semester = $id_semester;
    }

    function load_mk_dosen() {
        return $this->db->QueryToArray("
        SELECT 
            MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,J.NM_JENJANG,PS.NM_PROGRAM_STUDI, MK.KREDIT_SEMESTER,KUMK.TINGKAT_SEMESTER
        FROM PENGAMPU_MK PENGMK
        JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
        JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
        JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH      
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=MK.ID_PROGRAM_STUDI 
        JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
        JOIN DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
        WHERE D.ID_DOSEN='{$this->id_dosen}' AND KMK.ID_SEMESTER='{$this->id_semester}'
        GROUP BY MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,J.NM_JENJANG,PS.NM_PROGRAM_STUDI, MK.KREDIT_SEMESTER,KUMK.TINGKAT_SEMESTER
        ");
    }

    function load_jadwal_kuliah() {
        return $this->db->QueryToArray("
        SELECT 
            KMK.ID_KELAS_MK,JH.NM_JADWAL_HARI AS HARI,MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH, K2.NAMA_KELAS AS NM_KELAS,
            NVL(JJ.NM_JADWAL_JAM,JM.NM_JADWAL_JAM||'-'||JS.NM_JADWAL_JAM) AS JAM_KE,JS.NM_JADWAL_JAM JAM_KES
            ,NVL(JJ.JAM_MULAI,JM.JAM_MULAI) JAM_MULAI,NVL(JJ.MENIT_MULAI,JM.MENIT_MULAI) MENIT_MULAI,
            NVL(JJ.JAM_SELESAI,JS.JAM_SELESAI) JAM_SELESAI,NVL(JJ.MENIT_SELESAI,JS.MENIT_SELESAI) MENIT_SELESAI,R.NM_RUANGAN,
            coalesce(PMK.JUMLAH_MHS,0) as JUMLAH_MHS
        FROM PENGAMPU_MK PENGMK
        JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
        LEFT JOIN (
            SELECT ID_KELAS_MK, COUNT(ID_MHS) JUMLAH_MHS FROM PENGAMBILAN_MK WHERE STATUS_APV_PENGAMBILAN_MK = 1 GROUP BY ID_KELAS_MK
        ) PMK ON PMK.ID_KELAS_MK = KMK.ID_KELAS_MK
        JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
        JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
        LEFT JOIN JADWAL_KELAS JK ON JK.ID_KELAS_MK = KMK.ID_KELAS_MK
        LEFT JOIN JADWAL_HARI JH ON JH.ID_JADWAL_HARI = JK.ID_JADWAL_HARI
        LEFT JOIN JADWAL_JAM JJ ON JJ.ID_JADWAL_JAM = JK.ID_JADWAL_JAM
        LEFT JOIN RUANGAN R ON R.ID_RUANGAN = JK.ID_RUANGAN
        LEFT JOIN JADWAL_JAM JM ON JM.ID_JADWAL_JAM = JK.ID_JADWAL_JAM_MULAI
        LEFT JOIN JADWAL_JAM JS ON JS.ID_JADWAL_JAM = JK.ID_JADWAL_JAM_SELESAI
        LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
        JOIN DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
        WHERE D.ID_DOSEN='{$this->id_dosen}' AND KMK.ID_SEMESTER='{$this->id_semester}'
        ");
    }

    //Khusus Psikologi
    function load_jadwal_kuliah_psikologi() {
        return $this->db->QueryToArray("
		SELECT 
            KMK.ID_KELAS_MK,JH.NM_JADWAL_HARI AS HARI, MK.KD_MATA_KULIAH||' - '||MK.NM_MATA_KULIAH AS NMMK, K2.NAMA_KELAS AS NM_KELAS,
            JJ.JAM_MULAI||':'||JJ.MENIT_MULAI||' - '||JJ.JAM_SELESAI||':'||JJ.MENIT_SELESAI AS JAM, R.NM_RUANGAN,
            COALESCE(PMK.JUMLAH_MHS,0) as JUMLAH_MHS, JJG.NM_JENJANG||' - '||COALESCE(PS.NM_SINGKAT_PRODI,'PSI') AS PRODI
        FROM PENGAMPU_MK PENGMK
        JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
        LEFT JOIN (
            SELECT ID_KELAS_MK,COUNT(ID_MHS) JUMLAH_MHS FROM PENGAMBILAN_MK GROUP BY ID_KELAS_MK
        ) PMK ON PMK.ID_KELAS_MK = KMK.ID_KELAS_MK
        JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
        JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
		LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = KUMK.ID_PROGRAM_STUDI
		LEFT JOIN JENJANG JJG ON JJG.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN JADWAL_KELAS JK ON JK.ID_KELAS_MK = KMK.ID_KELAS_MK
        LEFT JOIN JADWAL_HARI JH ON JH.ID_JADWAL_HARI = JK.ID_JADWAL_HARI
        LEFT JOIN JADWAL_JAM JJ ON JJ.ID_JADWAL_JAM = JK.ID_JADWAL_JAM
        LEFT JOIN RUANGAN R ON R.ID_RUANGAN = JK.ID_RUANGAN
        LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
        JOIN DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
        WHERE D.ID_DOSEN='{$this->id_dosen}' AND KMK.ID_SEMESTER='{$this->id_semester}'
        ORDER BY JH.ID_JADWAL_HARI, JJ.NM_JADWAL_JAM
        ");
    }

    function load_peserta_mata_kuliah($id_kelas_mk) {
        
        $data_peserta_mk = $this->db->QueryToArray("
            SELECT P.NM_PENGGUNA,M.ID_MHS,M.NIM_MHS,M.MOBILE_MHS,P.KELAMIN_PENGGUNA as KELAMIN
            FROM PENGAMBILAN_MK PMK
            JOIN MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            WHERE PMK.ID_KELAS_MK='{$id_kelas_mk}' AND PMK.STATUS_APV_PENGAMBILAN_MK = 1");

        foreach ($data_peserta_mk as &$temp) {
            if (file_exists("../../foto_mhs/{$GLOBALS['nama_singkat']}/{$temp['NIM_MHS']}.JPG")) {
                $foto = "/foto_mhs/{$GLOBALS['nama_singkat']}/{$temp['NIM_MHS']}.JPG";
            } else if (file_exists("../../foto_mhs/{$GLOBALS['nama_singkat']}/{$temp['NIM_MHS']}.jpg")) {
                $foto = "/foto_mhs/{$GLOBALS['nama_singkat']}/{$temp['NIM_MHS']}.jpg";
            } else {
                $foto = '/foto_mhs/foto-tidak-ditemukan.png';
            }

            // $temp['NIM_MHS'] = "../../../foto_mhs/{$GLOBALS['nama_singkat']}/{$temp['NIM_MHS']}.JPG";
            $temp['FOTO_MHS'] = $foto;
        }

        return $data_peserta_mk;
    }

}

?>
