<?php

//membuat class penelittian
class Penelitian
{
    private $db;
    
    const UPLOAD_FOR_ADD = 2;
    const UPLOAD_FOR_EDIT = 3;
    const PAIR_FOR_ADD = 2;
    const PAIR_FOR_EDIT = 3;

    function __construct(MyOracle $db)
    {
        $this->db = $db;
    }
    
    function GetData($id_penelitian)
    {
        $this->db->Query("
            select
                -- Penelitian
                p.id_penelitian, p.id_dosen, p.judul, p.id_penelitian_jenis, p.id_penelitian_skim, p.id_penelitian_bidang, p.penelitian_bidang_lain,
            
                -- Deskripsi
                p.lokasi, p.jangka_waktu, p.jangka_waktu_ke,
            
                -- file
                pf.nm_asli as nm_asli_file, pf.nm_file,
            
                -- TPM
                tpm_nama, tpm_jenis_kelamin, tpm_nip, tpm_id_golongan, tpm_jabatan_fungsional, tpm_jabatan_struktural, tpm_pt, tpm_fakultas, tpm_prodi,
                tpm_alamat_kantor, tpm_telp_kantor, tpm_fax_kantor, tpm_alamat_rumah, tpm_telp_rumah, tpm_email,
            
                -- Dekan & lppm
                p.id_dekan, p.id_ketua_lppm,
                dekan.username as nip_dekan, dekan.nm_pengguna as nm_dekan,
                lppm.username as nip_ketua_lppm, (lppm.gelar_depan||' '||lppm.nm_pengguna||', '||lppm.gelar_belakang) as nm_ketua_lppm,
            
                -- Data pengisian
                p.kota, to_char(p.tgl_penelitian, 'YYYY-MM-DD') as tgl_penelitian,
            
                -- lookup jenis penelitian
                (select nama_jenis from penelitian_jenis pj where pj.id_penelitian_jenis = p.id_penelitian_jenis) as nm_penelitian_jenis,
            
                -- lookup bidang penelitian
                (select nama_tema from penelitian_bidang pb where pb.id_penelitian_bidang = p.id_penelitian_bidang) as nm_penelitian_bidang,
            
                -- lookup anggota
                (select count(id_anggota) from penelitian_anggota pa where pa.id_penelitian = p.id_penelitian) as jumlah_anggota,
            
                -- lookup biaya
                (select sum(besar_biaya_dikti + besar_biaya_lain) from penelitian_biaya pb where pb.id_penelitian = p.id_penelitian) as total_biaya,
            
                -- lookup peneliti, fakultas
                f.nm_fakultas, peneliti.nm_pengguna as nm_peneliti, peneliti.gelar_depan, peneliti.gelar_belakang,
            
                -- nomer registrasi
                f.id_fakultas as kode_fakultas, to_char(p.tgl_penelitian, 'YY') as kode_tahun, p.no_urut,
                
                -- tanggal approval
                to_char(p.tgl_approve_lppm,'YYYY-MM-DD') as tgl_approve_lppm
                
            from penelitian p
            join dosen on dosen.id_dosen = p.id_dosen
            join pengguna peneliti on peneliti.id_pengguna = dosen.id_pengguna
            join program_studi ps on ps.id_program_studi = dosen.id_program_studi
            join fakultas f on f.id_fakultas = ps.id_fakultas
            left join pengguna dekan on dekan.id_pengguna = p.id_dekan
            left join pengguna lppm on lppm.id_pengguna = p.id_ketua_lppm
            left join penelitian_file pf on pf.id_penelitian = p.id_penelitian
            where p.id_penelitian = {$id_penelitian}");
        
        return $this->db->FetchAssoc();
    }
    
    function Add($id_dosen, &$post)
    {
        $tgl_penelitian = $post['tgl_penelitian_Year'] . $post['tgl_penelitian_Month'] . $post['tgl_penelitian_Day'];
        
        $this->db->Parse("
            insert into penelitian (
                id_dosen, judul, id_penelitian_jenis, id_penelitian_skim, id_penelitian_bidang, penelitian_bidang_lain, lokasi, jangka_waktu, jangka_waktu_ke,
                
                tpm_nama, tpm_jenis_kelamin, tpm_nip, tpm_id_golongan, tpm_jabatan_fungsional, tpm_jabatan_struktural, tpm_pt,  tpm_fakultas, tpm_prodi,
                tpm_alamat_kantor, tpm_telp_kantor, tpm_fax_kantor, tpm_alamat_rumah, tpm_telp_rumah, tpm_email,
                
                kota, tgl_penelitian,
                id_dekan, id_ketua_lppm
            ) values (
                :id_dosen, :judul, :id_penelitian_jenis, :id_penelitian_skim, :id_penelitian_bidang, :penelitian_bidang_lain, :lokasi, :jangka_waktu, :jangka_waktu_ke,
                
                :tpm_nama, :tpm_jenis_kelamin, :tpm_nip, :tpm_id_golongan, :tpm_jabatan_fungsional, :tpm_jabatan_struktural, :tpm_pt, :tpm_fakultas, :tpm_prodi,
                :tpm_alamat_kantor, :tpm_telp_kantor, :tpm_fax_kantor, :tpm_alamat_rumah, :tpm_telp_rumah, :tpm_email,
                
                :kota, to_date(:tgl_penelitian,'YYYYMMDD'),
                :id_dekan, :id_ketua_lppm
            )");
        
        $this->db->BindByName(':id_dosen',              $id_dosen);
        $this->db->BindByName(':judul',                 $post['judul']);
        $this->db->BindByName(':id_penelitian_jenis',   $post['id_penelitian_jenis']);
        $this->db->BindByName(':id_penelitian_skim',    $post['id_penelitian_skim']);
        $this->db->BindByName(':id_penelitian_bidang',  $post['id_penelitian_bidang']);
        $this->db->BindByName(':penelitian_bidang_lain',$post['penelitian_bidang_lain']);
        $this->db->BindByName(':lokasi',                $post['lokasi']);
        $this->db->BindByName(':jangka_waktu',          $post['jangka_waktu']);
        $this->db->BindByName(':jangka_waktu_ke',       $post['jangka_waktu_ke']);
        
        $this->db->BindByName(':tpm_nama',              $post['tpm_nama']);
        $this->db->BindByName(':tpm_jenis_kelamin',     $post['tpm_jenis_kelamin']);
        $this->db->BindByName(':tpm_nip',               $post['tpm_nip']);
        $this->db->BindByName(':tpm_id_golongan',       $post['tpm_id_golongan']);
        $this->db->BindByName(':tpm_jabatan_fungsional',$post['tpm_jabatan_fungsional']);
        $this->db->BindByName(':tpm_jabatan_struktural',$post['tpm_jabatan_struktural']);
        $this->db->BindByName(':tpm_pt',                $post['tpm_pt']);
        $this->db->BindByName(':tpm_fakultas',          $post['tpm_fakultas']);
        $this->db->BindByName(':tpm_prodi',             $post['tpm_prodi']);
        $this->db->BindByName(':tpm_alamat_kantor',     $post['tpm_alamat_kantor']);
        $this->db->BindByName(':tpm_telp_kantor',       $post['tpm_telp_kantor']);
        $this->db->BindByName(':tpm_fax_kantor',        $post['tpm_fax_kantor']);
        $this->db->BindByName(':tpm_alamat_rumah',      $post['tpm_alamat_rumah']);
        $this->db->BindByName(':tpm_telp_rumah',        $post['tpm_telp_kantor']);
        $this->db->BindByName(':tpm_email',             $post['tpm_email']);
        
        $this->db->BindByName(':kota',                  $post['kota']);
        $this->db->BindByName(':tgl_penelitian',        $tgl_penelitian);
        $this->db->BindByName(':id_dekan',              $post['id_dekan']);
        $this->db->BindByName(':id_ketua_lppm',         $post['id_ketua_lppm']);
        
        return $this->db->Execute();
    }
    
    function EditPenelitian($id_penelitian, &$post)
    {       
        $this->db->Parse("
            update penelitian set
                judul                   = :judul,
                id_penelitian_jenis     = :id_penelitian_jenis,
                id_penelitian_skim      = :id_penelitian_skim,
                id_penelitian_bidang    = :id_penelitian_bidang,
                penelitian_bidang_lain  = :penelitian_bidang_lain,
                jangka_waktu            = :jangka_waktu,
                jangka_waktu_ke         = :jangka_waktu_ke,

                tpm_nama                = :tpm_nama,
                tpm_jenis_kelamin       = :tpm_jenis_kelamin,
                tpm_nip                 = :tpm_nip,
                tpm_id_golongan         = :tpm_id_golongan,
                tpm_jabatan_fungsional  = :tpm_jabatan_fungsional,
                tpm_jabatan_struktural  = :tpm_jabatan_struktural,
                tpm_pt                  = :tpm_pt,
                tpm_fakultas            = :tpm_fakultas,
                tpm_prodi               = :tpm_prodi,
                tpm_alamat_kantor       = :tpm_alamat_kantor,
                tpm_telp_kantor         = :tpm_telp_kantor,
                tpm_fax_kantor          = :tpm_fax_kantor,
                tpm_alamat_rumah        = :tpm_alamat_rumah,
                tpm_telp_rumah          = :tpm_telp_rumah,
                tpm_email               = :tpm_email,
                
                kota                    = :kota,
                tgl_penelitian          = to_date(:tgl_penelitian, 'YYYYMMDD')

            where id_penelitian = :id_penelitian");
        $this->db->BindByName(':id_penelitian',         $id_penelitian);
        $this->db->BindByName(':judul',                 $post['judul']);
        $this->db->BindByName(':id_penelitian_jenis',   $post['id_penelitian_jenis']);
        $this->db->BindByName(':id_penelitian_skim',    $post['id_penelitian_skim']);
        $this->db->BindByName(':id_penelitian_bidang',  $post['id_penelitian_bidang']);
        $this->db->BindByName(':penelitian_bidang_lain',$post['penelitian_bidang_lain']);
        $this->db->BindByName(':lokasi',                $post['lokasi']);
        $this->db->BindByName(':jangka_waktu',          $post['jangka_waktu']);
        $this->db->BindByName(':jangka_waktu_ke',       $post['jangka_waktu_ke']);
        
        $this->db->BindByName(':tpm_nama',              $post['tpm_nama']);
        $this->db->BindByName(':tpm_jenis_kelamin',     $post['tpm_jenis_kelamin']);
        $this->db->BindByName(':tpm_nip',               $post['tpm_nip']);
        $this->db->BindByName(':tpm_id_golongan',       $post['tpm_id_golongan']);
        $this->db->BindByName(':tpm_jabatan_fungsional',$post['tpm_jabatan_fungsional']);
        $this->db->BindByName(':tpm_jabatan_struktural',$post['tpm_jabatan_struktural']);
        $this->db->BindByName(':tpm_pt',                $post['tpm_pt']);
        $this->db->BindByName(':tpm_fakultas',          $post['tpm_fakultas']);
        $this->db->BindByName(':tpm_prodi',             $post['tpm_prodi']);
        $this->db->BindByName(':tpm_alamat_kantor',     $post['tpm_alamat_kantor']);
        $this->db->BindByName(':tpm_telp_kantor',       $post['tpm_telp_kantor']);
        $this->db->BindByName(':tpm_fax_kantor',        $post['tpm_fax_kantor']);
        $this->db->BindByName(':tpm_alamat_rumah',      $post['tpm_alamat_rumah']);
        $this->db->BindByName(':tpm_telp_rumah',        $post['tpm_telp_kantor']);
        $this->db->BindByName(':tpm_email',             $post['tpm_email']);
        
        $this->db->BindByName(':kota',                  $post['kota']);
        $this->db->BindByName(':tgl_penelitian',        $tgl_penelitian = $post['tgl_penelitian_Year'] . $post['tgl_penelitian_Month'] . $post['tgl_penelitian_Day']);
        
        return $this->db->Execute();
    }
    
    function GetListPenelitian($id_dosen)
    {
        return $this->db->QueryToArray("
            select id_penelitian, judul, pj.nama_jenis, to_char(tgl_penelitian,'YYYY') as tahun, ps.kode_skim,
                tgl_submit, tgl_approve_departemen, tgl_approve_lppm
            from penelitian p
            left join penelitian_jenis pj on pj.id_penelitian_jenis = p.id_penelitian_jenis
            left join penelitian_skim ps on ps.id_penelitian_skim = p.id_penelitian_skim
            where id_dosen = {$id_dosen} order by id_penelitian");
    }
    
    function GetFileProposal($id_penelitian)
    {
        $this->db->Query("select nm_asli as nm_asli_file, nm_file from penelitian_file where id_penelitian = {$id_penelitian}");
        return $this->db->FetchAssoc();
    }
    
    function UploadFile($id_dosen, $md5, &$file, $mode)
    {
        $destination = "/var/www/html/files/upload/dosen/penelitian/{$id_dosen}_{$md5}.pdf";
        
        if (move_uploaded_file($file['tmp_name'], $destination))
        {
            $this->db->Query("
                insert into penelitian_file (id_dosen, md5, nm_asli, nm_file, status)
                    values ({$id_dosen}, '{$md5}', '{$file['name']}', '{$id_dosen}_{$md5}.pdf', {$mode})");
            
            return true;
        }
        
        return false;
    }
    
    function PairFile($md5, $id_dosen, $id_penelitian, $mode)
    {
        if ($mode == Penelitian::PAIR_FOR_ADD)
        {
            // Mendapatkan id_penelitian yang baru di insert
            // dengan cara mengambil id_penelitian terbesar dari dosen
            $this->db->Query("select max(id_penelitian) id_penelitian from penelitian where id_dosen = {$id_dosen}");
            $row = $this->db->FetchAssoc();
            $id_penelitian = $row['ID_PENELITIAN'];
            
            $this->db->Query("update penelitian_file set id_penelitian = {$id_penelitian}, status = 1 where md5 = '{$md5}' and id_dosen = {$id_dosen}");            
        }
        
        if ($mode == Penelitian::PAIR_FOR_EDIT)
        {
            // clear pairing
            $this->db->Query("update penelitian_file set id_penelitian = null, status = 0 where id_penelitian = {$id_penelitian} and id_dosen = {$id_dosen}");
            
            // pairing
            $this->db->Query("update penelitian_file set id_penelitian = {$id_penelitian}, status = 1 where md5 = '{$md5}' and id_dosen = {$id_dosen}");
        }
        
    }
    
    function GetListPenelitianApproved($id_dosen)
    {
        return $this->db->QueryToArray("
            select id_penelitian, judul, pj.nama_jenis, to_char(tgl_penelitian,'YYYY') as tahun, ps.kode_skim,
                tgl_submit, tgl_approve_departemen, tgl_approve_lppm
            from penelitian p
            left join penelitian_jenis pj on pj.id_penelitian_jenis = p.id_penelitian_jenis
            left join penelitian_skim ps on ps.id_penelitian_skim = p.id_penelitian_skim
            where id_dosen = {$id_dosen} and tgl_submit is not null and tgl_approve_departemen is not null and tgl_approve_lppm is not null");
    }
    
    function DeletePenelitian($id_dosen, $id_penelitian)
    {
        $this->db->BeginTransaction();
        $this->db->Query("delete from penelitian_anggota where id_penelitian = {$id_penelitian}");
        $this->db->Query("delete from penelitian_biaya where id_penelitian = {$id_penelitian}");
        $this->db->Query("delete from penelitian where id_dosen = {$id_dosen} and id_penelitian = {$id_penelitian}");
        return $this->db->Commit();
    }
    
    function SubmitPenelitian($id_penelitian)
    {
        $this->db->Query("
            select j.is_lppm from penelitian p
            join penelitian_jenis j on j.id_penelitian_jenis = p.id_penelitian_jenis
            where p.id_penelitian = {$id_penelitian}");
        $penelitian = $this->db->FetchAssoc();
        
        $tanggal_sekarang = date('d-M-Y');
        
        if ($penelitian['IS_LPPM'] == 1)
            return $this->db->Query("update penelitian set tgl_approve_departemen = '{$tanggal_sekarang}', tgl_submit = '{$tanggal_sekarang}' where id_penelitian = {$id_penelitian}");
        else
            return $this->db->Query("update penelitian set tgl_submit = '{$tanggal_sekarang}' where id_penelitian = {$id_penelitian}");
    }
    
    function GetListJenisPenelitian()
    {
        return $this->db->QueryToArray("select * from penelitian_jenis order by nama_jenis");
    }
    
    function GetListSKIM($id_penelitian_jenis)
    {
        return $this->db->QueryToArray("select * from penelitian_skim where id_penelitian_jenis = {$id_penelitian_jenis} order by kode_skim");
    }
    
    function GetListBidangPenelitian($id_penelitian_jenis)
    {
        return $this->db->QueryToArray("select * from penelitian_bidang where id_penelitian_jenis = {$id_penelitian_jenis} order by nama_tema");
    }
    
    function GetKetuaLPPM()
    {
        $this->db->Query("
            select p.id_pengguna as id_ketua_lppm, p.username as nip_ketua_lppm, p.nm_pengguna as nama_ketua_lppm from dosen d
            join pengguna p on p.id_pengguna = d.id_pengguna
            where id_jabatan_pegawai = 220");
        return $this->db->FetchAssoc();
    }
    
    function GetDekan($id_dosen)
    {
        $this->db->Query("
            select p.id_pengguna as id_dekan, p.nm_pengguna as nm_dekan, p.username as nip_dekan
            from fakultas f
            join pengguna p on p.id_pengguna = f.id_dekan
            join program_studi ps on ps.id_fakultas = f.id_fakultas
            where ps.id_program_studi in (select id_program_studi from dosen where id_dosen = {$id_dosen})");
        return $this->db->FetchAssoc();
    }
    
    function FindAnggota($q)
    {
        $q = strtoupper($q);
        return $this->db->QueryToArray("
            select
                id_pengguna as id,
                username as nip,
                nm_pengguna as nama,
                gelar_depan, gelar_belakang
            from pengguna
            where id_pengguna in (select id_pengguna from dosen) and upper(nm_pengguna) like '%{$q}%'
            order by nm_pengguna");
    }
    
    function AddAnggota($id_penelitian, &$post)
    {   
        if ($post['id_pengguna'] != '')
        {
            $query = "
                insert into penelitian_anggota (id_penelitian, asal_anggota, id_pengguna, nama_anggota, nip_anggota, gelar_depan, gelar_belakang)
                select :id_penelitian, :asal_anggota, :id_pengguna, nm_pengguna, username, gelar_depan, gelar_belakang from pengguna
                where id_pengguna = :id_pengguna";
        }
        else
        {
            $query = "
                insert into penelitian_anggota (id_penelitian, asal_anggota, id_pengguna, nama_anggota, nip_anggota, gelar_depan, gelar_belakang)
                values (:id_penelitian, :asal_anggota, :id_pengguna, :nama_anggota, :nip_anggota, :gelar_depan, :gelar_belakang)";
        }
        
        $this->db->Parse($query);
        
        $this->db->BindByName(':id_penelitian',     $id_penelitian);
        $this->db->BindByName(':asal_anggota',      $post['asal_anggota']);
        $this->db->BindByName(':id_pengguna',       $post['id_pengguna']);
        $this->db->BindByName(':nama_anggota',      $post['nama_anggota']);
        $this->db->BindByName(':nip_anggota',       $post['nip_anggota']);
        $this->db->BindByName(':gelar_depan',       $post['gelar_depan']);
        $this->db->BindByName(':gelar_belakang',    $post['gelar_belakang']);
        
        return $this->db->Execute();
    }
    
    function DeleteAnggota($id_anggota)
    {
        return $this->db->Query("delete from penelitian_anggota where id_anggota = {$id_anggota}");
    }
    
    function GetAnggotaPenelitian($id_penelitian)
    {
        $rows = $this->db->QueryToArray("
            select nama_anggota, gelar_depan, gelar_belakang from penelitian_anggota pa
            where id_penelitian = {$id_penelitian} order by id_anggota");
        
        $anggota_set = array();
        foreach ($rows as $row)
        {
            array_push($anggota_set, "{$row['GELAR_DEPAN']} {$row['NAMA_ANGGOTA']} {$row['GELAR_BELAKANG']} sebagai Anggota Peneliti");
        }
        
        return implode("\n", $anggota_set);
    }
    
    function GetListAnggotaPenelitian($id_penelitian)
    {
        return $this->db->QueryToArray("
            select 
                pa.id_anggota, pa.asal_anggota,
                pa.nama_anggota as nama1,
                pa.nip_anggota as nip1,
                pa.gelar_depan as gelar_depan_1,
                pa.gelar_belakang as gelar_belakang_1,
                p.nm_pengguna as nama2,
                p.username as nip2,
                p.gelar_depan as gelar_depan_1,
                p.gelar_belakang as gelar_belakang_2
            from penelitian_anggota pa
            left join pengguna p on p.id_pengguna = pa.id_pengguna
            where pa.id_penelitian = {$id_penelitian}
            order by id_anggota asc");
    }
    
    function GetListBiayaPenelitian($id_penelitian)
    {
        return $this->db->QueryToArray("select * from penelitian_biaya where id_penelitian = {$id_penelitian} order by tahun_ke");
    }
    
    function AddBiaya($id_penelitian, &$post)
    {
        $this->db->Parse("
            insert into penelitian_biaya
                ( id_penelitian, tahun_ke, besar_biaya_dikti, besar_biaya_lain) values
                (:id_penelitian, :tahun_ke,:besar_biaya_dikti,:besar_biaya_lain)");
        $this->db->BindByName(':id_penelitian', $id_penelitian);
        $this->db->BindByName(':tahun_ke', $post['tahun_ke']);
        $this->db->BindByName(':besar_biaya_dikti', $post['besar_biaya_dikti']);
        $this->db->BindByName(':besar_biaya_lain', $post['besar_biaya_lain']);
        return $this->db->Execute();
    }
    
    function DeleteBiaya($id_penelitian, $id_biaya)
    {
        return $this->db->Query("delete from penelitian_biaya where id_penelitian = {$id_penelitian} and id_biaya = {$id_biaya}");
    }
    
    function GetListPenelitianByDepartemen($id_departemen)
    {
        return $this->db->QueryToArray("
            select id_penelitian, judul, pj.nama_jenis, to_char(tgl_penelitian,'YYYY') as tahun, ps.kode_skim,
                tgl_submit, tgl_approve_departemen, tgl_approve_lppm,
                gelar_depan||' '||peneliti.nm_pengguna||', '||gelar_belakang as nm_peneliti
            from penelitian p
            join dosen d on d.id_dosen = p.id_dosen
            join program_studi ps on ps.id_program_studi = d.id_program_studi
            join pengguna peneliti on peneliti.id_pengguna = d.id_pengguna
            left join penelitian_jenis pj on pj.id_penelitian_jenis = p.id_penelitian_jenis
            left join penelitian_skim ps on ps.id_penelitian_skim = p.id_penelitian_skim
            where pj.is_lppm = 0 and ps.id_departemen = {$id_departemen} and tgl_submit is not null and tgl_approve_departemen is null");
    }
    
    function ApproveDepartemen($id_penelitian, $id_verifikator)
    {
        $tanggal_sekarang = date('d-M-Y');
        $this->db->Query("update penelitian set tgl_approve_departemen = '{$tanggal_sekarang}', id_verifikator_departemen = {$id_verifikator} where id_penelitian = {$id_penelitian}");
        return ($this->db->affected_rows > 0);
    }
    
    function RejectDepartemen($id_penelitian)
    {
        $this->db->Query("update penelitian set tgl_submit = null where id_penelitian = {$id_penelitian}");
        return ($this->db->affected_rows > 0);
    }
    
    function GetListApprovalPenelitianByLPPM()
    {
        return $this->db->QueryToArray("
            select id_penelitian, judul, pj.nama_jenis, to_char(tgl_penelitian,'YYYY') as tahun, ps.kode_skim,
                tgl_submit, tgl_approve_departemen, tgl_approve_lppm,
                gelar_depan||' '||peneliti.nm_pengguna||', '||gelar_belakang as nm_peneliti
            from penelitian p
            join dosen d on d.id_dosen = p.id_dosen
            join program_studi ps on ps.id_program_studi = d.id_program_studi
            join pengguna peneliti on peneliti.id_pengguna = d.id_pengguna
            left join penelitian_jenis pj on pj.id_penelitian_jenis = p.id_penelitian_jenis
            left join penelitian_skim ps on ps.id_penelitian_skim = p.id_penelitian_skim
            where tgl_submit is not null and tgl_approve_departemen is not null and tgl_approve_lppm is null");
    }
    
    function GetListPenelitianByLPPMApproved()
    {
        return $this->db->QueryToArray("
            select id_penelitian, judul, pj.nama_jenis, to_char(tgl_penelitian,'YYYY') as tahun, ps.kode_skim,
                tgl_submit, tgl_approve_departemen, tgl_approve_lppm,
                gelar_depan||' '||peneliti.nm_pengguna||', '||gelar_belakang as nm_peneliti
            from penelitian p
            join dosen d on d.id_dosen = p.id_dosen
            join program_studi ps on ps.id_program_studi = d.id_program_studi
            join pengguna peneliti on peneliti.id_pengguna = d.id_pengguna
            left join penelitian_jenis pj on pj.id_penelitian_jenis = p.id_penelitian_jenis
            left join penelitian_skim ps on ps.id_penelitian_skim = p.id_penelitian_skim
            where tgl_submit is not null and tgl_approve_departemen is not null and tgl_approve_lppm is not null");
    }
    
    function ApproveLPPM($id_penelitian, $id_verifikator)
    {
        $tahun_sekarang = date('Y');
        // Mendapatkan nomer urut
        $this->db->Query("
            select (nvl(max(no_urut),0)+1) as no_urut
            from penelitian
            where 
                tgl_approve_departemen is not null and
                tgl_approve_lppm is not null and
                to_char(tgl_penelitian,'YYYY') = '{$tahun_sekarang}'");
        $row = $this->db->FetchAssoc();
        $no_urut = $row['NO_URUT'];
        
        $tanggal_sekarang = date('d-M-Y');
        $this->db->Query("update penelitian set tgl_approve_lppm = '{$tanggal_sekarang}', id_verifikator_lppm = {$id_verifikator}, no_urut = {$no_urut} where id_penelitian = {$id_penelitian}");
        return ($this->db->affected_rows > 0);
    }
    
    function RejectLPPM($id_penelitian)
    {
        $this->db->Query("update penelitian set tgl_submit = null, tgl_approve_departemen = null where id_penelitian = {$id_penelitian}");
        return ($this->db->affected_rows > 0);
    }
    
    function GetListGolongan()
    {
        return $this->db->QueryToArray("select id_golongan, nm_golongan||' - '||nm_pangkat as nm_golongan from golongan order by nm_golongan");
    }
    
    function PrintPDF($id_penelitian, FPDF &$pdf)
    {
        $p = $this->GetData($id_penelitian);
        
        $no_registrasi = str_pad($p['KODE_FAKULTAS'], 2, '0', STR_PAD_LEFT) . "." . $p['KODE_TAHUN'] . "." . str_pad($p['NO_URUT'],3,'0', STR_PAD_LEFT);
        $judul = $p['JUDUL'];
        $jenis_penelitian = $p['NAMA_JENIS'];
        $jangka_waktu = $p['JANGKA_WAKTU'];
        $jangka_waktu_ke = $p['JANGKA_WAKTU_KE'];
        $total_biaya = number_format($p['TOTAL_BIAYA'], 2, ",", ".");
        $nama = $p['NM_PENELITI'];
        $gelar_depan = $p['GELAR_DEPAN'];
        $gelar_belakang = $p['GELAR_BELAKANG'];
        $fakultas = $p['NM_FAKULTAS'];
        
        $dekan = $p['NM_FAKULTAS'];
        $nm_ketua_lppm = $p['NM_KETUA_LPPM'];
        $nip_ketua_lppm = $p['NIP_KETUA_LPPM'];
        $tgl_approve_lppm = strftime('%d %B %Y', strtotime($p['TGL_APPROVE_LPPM']));
        
        $border = 0;
        
        //$pdf->SetMargins(10, 10);
        
        // Tambah Font
        $pdf->AddFont('RockwellC', '', 'rockwell-condensed.php');
        $pdf->AddFont('RockwellC', 'B', 'rockwellcb.php');
        $pdf->AddFont('Algerian','','algerian.php');
        $pdf->AddFont('Georgia','','georgia.php');
        $pdf->AddFont('Gabriola', '', 'gabriola.php');
        
        // set halaman
        $pdf->SetTitle('Sertifikat Registrasi Penelitian');
        $pdf->AddPage('L', 'A4');
        
        // Border images
        $pdf->Image('img/border2.gif', 5, 1, 287, 205, 'GIF');
        
        // Logo images
        $pdf->Image('../../img/logo-unair.png', 60, 28, 30, 30, 'PNG');
        
        // Tulisan LPPM
        $pdf->SetXY(95, 35);
        $pdf->SetFont('RockwellC', '', 18);
        $pdf->Write('2', "LEMBAGA PENELITIAN DAN PENGABDIAN KEPADA MASYARAKAT");
        
        // Tulisan Universitas Airlangga
        $pdf->SetXY(95, 45);
        $pdf->SetFont('RockwellC', 'B', 30);
        $pdf->Write('2', 'UNIVERSITAS AIRLANGGA');
        
        // Garis
        $pdf->SetLineWidth(0.5);
        $pdf->Line(95, 52, 230, 52);
        $pdf->Line(95, 54, 230, 54);
        
        $pdf->SetLineWidth(0.2);
        $pdf->SetXY(25, 62);
        $pdf->SetFont('Algerian', 'U', 15);
        $pdf->Cell(245, 5, 'REGISTRASI PENELITIAN', 0, '', 'C');
        
        $pdf->SetXY(25, 67);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(245, 4, "Nomor : {$no_registrasi}", 0, '', 'C');
        
        $pdf->SetXY(25, 78);
        $pdf->SetFont('Arial', '', 11);
        $pdf->Cell(245, 4, 'Diberikan kepada :', 0, '', 'C');
        
        // Anggota Penelitian
        $pdf->SetXY(25, 84);
        $pdf->SetFont('RockwellC', '', 15);
        $pdf->Cell(245, 3, "{$gelar_depan} {$nama}, {$gelar_belakang} sebagai Ketua Peneliti", 0, '', 'C');
        
        $pdf->SetXY(25, 89);
        $pdf->SetFont('RockwellC', '', 13);
        $pdf->MultiCell(245, 5, $this->GetAnggotaPenelitian(get('id_penelitian')), 0, 'C');
        // -----------------------------------------------------------------------------

        $pdf->SetXY(25, $pdf->GetY() + 4);
        $pdf->SetFont('Arial', '', 11);
        $pdf->Cell(245, 4, 'Judul Penelitian :', 0, '', 'C');
        
        $pdf->SetXY(49, $pdf->GetY() + 5);
        $pdf->SetFont('Arial');
        $pdf->SetFontSize(14);
        $pdf->MultiCell(200, 5, $judul,0,'C');
        
        $pdf->SetXY(25, $pdf->GetY() + 4);
        $pdf->SetFont('Arial', '', 11);
        $pdf->MultiCell(245, 5, "Sumber dana {$jenis_penelitian} sebesar Rp {$total_biaya}",0,'C');
        
        $pdf->SetXY(25, $pdf->GetY());
        $pdf->MultiCell(245, 5, "Penelitian tahun ke {$jangka_waktu_ke} dari {$jangka_waktu} tahun", 0, 'C');
        
        $pdf->SetXY(25, 155);
        $pdf->SetFont('Arial', '', 11);
        $pdf->Cell(245, 4, "Surabaya, ". $tgl_approve_lppm, 0, true, 'C');
        
        //$pdf->SetXY(25, 160);
        //$pdf->SetFont('Times', '', 12);
        $pdf->SetX(25);
        $pdf->Cell(245, 4, "Ketua LPPM", 0, '', 'C');
        
        $pdf->SetXY(25, 180);
        $pdf->SetFont('Arial', 'BU', 11);
        $pdf->Cell(245, 4, $nm_ketua_lppm, 0, '', 'C');
        
        $pdf->SetXY(25, 184);
        $pdf->SetFont('Arial', 'B', 11);
        $pdf->Cell(245, 4, "NIP. ". $nip_ketua_lppm, 0, '', 'C');
        
        $pdf->Output();
        
        exit();
    }
}
?>
