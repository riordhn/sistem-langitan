<?php

class evaluasi {

    public $db;
    public $id_dosen;

    function __construct($db, $id_dosen) {
        $this->db = $db;
        $this->id_dosen = $id_dosen;
    }

    function get_materi_ev_kul() {
        return $this->db->QueryToArray("SELECT * FROM EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=1 AND TIPE_ASPEK=1 ORDER BY ID_EVAL_ASPEK");
    }

    function get_materi_ev_wali() {
        return $this->db->QueryToArray("SELECT * FROM EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=3 AND TIPE_ASPEK=1 ORDER BY ID_EVAL_ASPEK");
    }

    function get_materi_ev_prak() {
        return $this->db->QueryToArray("SELECT * FROM EV_PRAK ORDER BY ID");
    }

    function get_materi_ev_skripsi() {
        return $this->db->QueryToArray("SELECT * FROM EV_SKRIPSI ORDER BY ID");
    }
    
    function GetSemesterSebelum() {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil'");
            $semester_kemarin = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1)");
            $semester_kemarin = $this->db->FetchAssoc();
        }
        return $semester_kemarin['ID_SEMESTER'];
    }

    function load_data_hasil_ev_kul($id_kelas_mk) {
        return $this->db->QueryToArray("
            SELECT EA.EVALUASI_ASPEK,AVG(
                CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4'
                    THEN NILAI_EVAL
                END
            ) RATA,COUNT(EH.ID_MHS) JUMLAH_MHS,EA.URUTAN,EA.ID_EVAL_KELOMPOK_ASPEK
            FROM EVALUASI_ASPEK EA
            JOIN EVALUASI_HASIL EH ON EH.ID_EVAL_ASPEK=EA.ID_EVAL_ASPEK AND EA.TIPE_ASPEK=1 AND EA.ID_EVAL_INSTRUMEN=1
            WHERE EH.ID_DOSEN='{$this->id_dosen}' AND EH.ID_KELAS_MK='{$id_kelas_mk}'
            GROUP BY EA.ID_EVAL_KELOMPOK_ASPEK,EA.URUTAN,EA.EVALUASI_ASPEK
            ");
    }
    
    function load_data_hasil_ev_prak($id_kelas_mk) {
        return $this->db->QueryToArray("
            SELECT EA.EVALUASI_ASPEK,AVG(
                CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4'
                    THEN NILAI_EVAL
                END
            ) RATA,COUNT(EH.ID_MHS) JUMLAH_MHS,EA.URUTAN,EA.ID_EVAL_KELOMPOK_ASPEK
            FROM EVALUASI_ASPEK EA
            JOIN EVALUASI_HASIL EH ON EH.ID_EVAL_ASPEK=EA.ID_EVAL_ASPEK AND EA.TIPE_ASPEK=1 AND EA.ID_EVAL_INSTRUMEN=2
            WHERE EH.ID_DOSEN='{$this->id_dosen}' AND EH.ID_KELAS_MK='{$id_kelas_mk}'
            GROUP BY EA.ID_EVAL_KELOMPOK_ASPEK,EA.URUTAN,EA.EVALUASI_ASPEK
            ");
    }

    function load_data_hasil_ev_wali() {
        $semester = $this->GetSemesterSebelum();
        return $this->db->QueryToArray("
            SELECT EA.EVALUASI_ASPEK,AVG(
                CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4'
                    THEN NILAI_EVAL
                END
            ) RATA,COUNT(EH.ID_MHS) JUMLAH_MHS
            FROM EVALUASI_ASPEK EA
            JOIN EVALUASI_HASIL EH ON EH.ID_EVAL_ASPEK=EA.ID_EVAL_ASPEK AND EA.TIPE_ASPEK=1 AND EA.ID_EVAL_INSTRUMEN=3 AND EH.ID_SEMESTER='{$semester}'
            WHERE EH.ID_DOSEN='{$this->id_dosen}'
            GROUP BY EA.EVALUASI_ASPEK
            ");
    }

    function load_data_hasil_ev_praktikum() {
        return $this->db->QueryToArray("
            SELECT EA.EVALUASI_ASPEK,AVG(
                CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4'
                    THEN NILAI_EVAL
                END
            ) RATA,COUNT(EH.ID_MHS) JUMLAH_MHS
            FROM EVALUASI_ASPEK EA
            JOIN EVALUASI_HASIL EH ON EH.ID_EVAL_ASPEK=EA.ID_EVAL_ASPEK AND EA.TIPE_ASPEK=1 AND EA.ID_EVAL_INSTRUMEN=2
            WHERE EH.ID_DOSEN='{$this->id_dosen}'
            GROUP BY EA.EVALUASI_ASPEK
            ");
    }

}

?>
