$('#button-aturan-nilai').click(function(){
    $('#aturan-nilai').slideDown();
});
$('#button-close-aturan-nilai').click(function(){
    $('#aturan-nilai').slideUp();
});
$( "#dialog_tambah_komponen" ).dialog({
    width:'420',
    modal: true,
    resizable:false,
    autoOpen:false
});
$('#select_kelas_mk').change(function(){
    $.ajax({
        url : page_penilaian,
        type : 'post',
        data : 'mode=load_mahasiswa&id_kelas_mk='+$('#select_kelas_mk').val(),
        beforeSend : function(){
            $('#table_penilaian').html('<div style="width: 100%;" align="center"><img src="../../img/dosen/ajax_loader.gif" /></div>');
        },
        success :function(data){
            $('#center_content').html(data);
        }
    })
});
$('#form_entry_nilai').validate();
$(".nilai").each(function(){
    $(this).rules("add", { 
        required:true,  
        max:100,
        number: true    
    });
})