<?php

$id_pt = $id_pt_user;

class kkn {

    public $db;
    public $id_dosen;
    public $id_pengguna;

    function __construct($db, $id_dosen, $id_pengguna) {
        $this->db = $db;
        $this->id_dosen = $id_dosen;
        $this->id_pengguna = $id_pengguna;
    }

    function get_semester_kkn() {
        $this->db->Query("SELECT * FROM SEMESTER 
                            WHERE ID_SEMESTER IN (SELECT ID_SEMESTER FROM KKN_ANGKATAN WHERE STATUS_AKTIF=1) 
                            AND ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'");
        $semester_kkn = $this->db->FetchAssoc();
        return $semester_kkn['ID_SEMESTER'];
    }
    
    function get_angkatan_aktif(){
        $this->db->Query("SELECT * FROM KKN_ANGKATAN ka
                            JOIN SEMESTER s ON s.ID_SEMESTER = ka.ID_SEMESTER
                            WHERE ka.STATUS_AKTIF=1 AND s.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'");
        return $this->db->FetchAssoc();
    }

    function GetSemesterSebelum() {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil' AND ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'");
            $semester_kemarin = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1) AND ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'");
            $semester_kemarin = $this->db->FetchAssoc();
        }
        return $semester_kemarin['ID_SEMESTER'];
    }

    function cek_peranan_dosen() {
        $semester = $this->get_semester_kkn();
        $cek_dpl = $this->db->QuerySingle("SELECT COUNT(*) FROM KKN_DPL WHERE ID_SEMESTER='{$semester}' AND ID_DOSEN='{$this->id_dosen}'");
        $cek_korbing = $this->db->QuerySingle("SELECT COUNT(*) FROM KKN_KORBING WHERE ID_SEMESTER='{$semester}' AND ID_DOSEN='{$this->id_dosen}'");
        $cek_korkab = $this->db->QuerySingle("SELECT COUNT(*) FROM KKN_KORKAB WHERE ID_SEMESTER='{$semester}' AND ID_DOSEN='{$this->id_dosen}'");
        /*
          0. Jabatan Dalam KKN tidak ada
          1. Jabatan Koordinator Kabupaten
          2. Jabatan Koordinator Kecamatan
          3. Jabatan Dosen Pembina Lapangan
          4. Jabatan Koordinator Kabupaten dan Koordinator Kecamatan
          5. Jabatan Koordinator Kabupaten dan Koordinator Kecamatan dan Dosen Pembina Lapangan
          6. Jabatan Koordinator Kecamatan dan Dosen Pembina Lapangan
          7. Jabatan Koordinator Kabupaten dan Dosen Pembina Lapangan
         */
        $jabatan = 0;

        if ($cek_korkab > 0 && $cek_korbing == 0 && $cek_dpl == 0) {
            $jabatan = 1;
        } elseif ($cek_korkab == 0 && $cek_korbing > 0 && $cek_dpl == 0) {
            $jabatan = 2;
        } elseif ($cek_korkab == 0 && $cek_korbing == 0 && $cek_dpl > 0) {
            $jabatan = 3;
        } elseif ($cek_korkab > 0 && $cek_korbing > 0 && $cek_dpl == 0) {
            $jabatan = 4;
        } elseif ($cek_korkab > 0 && $cek_korbing > 0 && $cek_dpl > 0) {
            $jabatan = 5;
        } elseif ($cek_korkab == 0 && $cek_korbing > 0 && $cek_dpl > 0) {
            $jabatan = 6;
        } else if ($cek_korkab > 0 && $cek_korbing == 0 && $cek_dpl > 0) {
            $jabatan = 7;
        }

        return $jabatan;
    }

    function get_kabupaten_korkab() {
        $semester = $this->get_semester_kkn();
        $this->db->Query("SELECT * FROM KOTA WHERE ID_KOTA IN (SELECT ID_KABUPATEN FROM KKN_KORKAB WHERE ID_DOSEN='{$this->id_dosen}' AND ID_SEMESTER='{$semester}')");
        return $this->db->FetchAssoc();
    }

    function get_kecamatan_korbing() {
        $semester = $this->get_semester_kkn();
        $this->db->Query("SELECT * FROM KECAMATAN WHERE ID_KECAMATAN IN (SELECT ID_KECAMATAN FROM KKN_KORBING WHERE ID_DOSEN='{$this->id_dosen}' AND ID_SEMESTER='{$semester}')");
        return $this->db->FetchAssoc();
    }

    function get_kelurahan_dpl() {
        $semester = $this->get_semester_kkn();
        $query ="
            SELECT KEL.*
            FROM KKN_DPL KD
            JOIN KKN_KELOMPOK KK ON KD.ID_KKN_KELOMPOK=KK.ID_KKN_KELOMPOK
            JOIN KELURAHAN KEL ON KEL.ID_KELURAHAN=KK.ID_KELURAHAN
            WHERE KD.ID_DOSEN='{$this->id_dosen}' AND KD.ID_SEMESTER='{$semester}'
            ";
        return $this->db->QueryToArray($query);
    }

    function get_kelurahan_kkn($id_kel) {
        $this->db->Query("
            SELECT * FROM KELURAHAN WHERE ID_KELURAHAN='{$id_kel}'
            ");
        return $this->db->FetchAssoc();
    }

    function get_status_dpl() {
        $semester = $this->get_semester_kkn();
        return $this->db->QuerySingle("
            SELECT KK.ID_KELURAHAN 
            FROM KKN_DPL KD
            JOIN KKN_KELOMPOK KK ON KD.ID_KKN_KELOMPOK=KK.ID_KKN_KELOMPOK
            WHERE KD.ID_DOSEN='{$this->id_dosen}' AND KD.ID_SEMESTER='{$semester}'
            ");
    }

    function load_kecamatan_korkab() {
        $kab = $this->get_kabupaten_korkab();
        return $this->db->QueryToArray("SELECT * FROM KECAMATAN WHERE ID_KOTA='{$kab['ID_KOTA']}' ORDER BY NM_KECAMATAN");
    }

    function load_kelompok_kkn_korkab($id_kel) {
        $semester = $this->get_semester_kkn();
        return $this->db->QueryToArray("
            SELECT KKM.*,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS,JAL.NM_JALUR,PMK.ID_PENGAMBILAN_MK
                ,PMK.NILAI_HURUF,PMK.NILAI_ANGKA,KK.JENIS_KKN
            FROM KKN_KELOMPOK_MHS KKM
            JOIN KKN_KELOMPOK KK ON KK.ID_KKN_KELOMPOK=KKM.ID_KKN_KELOMPOK
            JOIN MAHASISWA M ON M.ID_MHS=KKM.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            JOIN JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND JM.IS_JALUR_AKTIF=1
            JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
            LEFT JOIN (
              SELECT PMK.ID_SEMESTER,PMK.ID_MHS,PMK.ID_PENGAMBILAN_MK,PMK.NILAI_HURUF,PMK.NILAI_ANGKA
              FROM PENGAMBILAN_MK PMK
              LEFT JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK=PMK.ID_KELAS_MK
              LEFT JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
              WHERE KUMK.STATUS_MKTA = 2 
              AND PMK.STATUS_APV_PENGAMBILAN_MK = 1                
            ) PMK ON PMK.ID_MHS=KKM.ID_MHS AND PMK.ID_SEMESTER=KK.ID_SEMESTER
            WHERE KK.ID_KELURAHAN='{$id_kel}' AND KKM.TERJUN=1 AND KK.ID_SEMESTER='{$semester['ID_SEMESTER']}'
            ");
    }

    function load_data_pembimbing() {
        $arr_hasil = array();
        $semester = $this->get_semester_kkn();
        $dpl = $this->db->QueryToArray("
            SELECT KB.*,KK.NAMA_KELOMPOK,KK.JENIS_KKN,KK.ID_KKN_KELOMPOK,(SELECT COUNT(*) FROM KKN_DPL KD WHERE KD.ID_DOSEN='{$this->id_dosen}' AND KD.ID_KKN_KELOMPOK=KK.ID_KKN_KELOMPOK) RANGKAP
            FROM KKN_KORBING KB
            JOIN KELURAHAN KEL ON KEL.ID_KECAMATAN=KB.ID_KECAMATAN
            JOIN KKN_KELOMPOK KK ON KK.ID_KELURAHAN=KEL.ID_KELURAHAN
            WHERE KB.ID_DOSEN='{$this->id_dosen}' AND KB.ID_SEMESTER='{$semester}'
            ORDER BY KK.NAMA_KELOMPOK
            ");

        foreach ($dpl as $d) {
            array_push($arr_hasil, array_merge($d, array("MHS_KKN" => $this->load_mahasiswa_kelompok($d['ID_KKN_KELOMPOK']))));
        }
        return $arr_hasil;
    }

    function load_data_dpl() {
        $arr_hasil = array();
        $semester = $this->get_semester_kkn();
        $dpl = $this->db->QueryToArray("
            SELECT KD.*,KK.NAMA_KELOMPOK 
            FROM KKN_DPL KD
            JOIN KKN_KELOMPOK KK ON KD.ID_KKN_KELOMPOK=KK.ID_KKN_KELOMPOK
            WHERE KD.ID_DOSEN='{$this->id_dosen}' AND KD.ID_SEMESTER='{$semester}'
            ORDER BY KK.NAMA_KELOMPOK
            ");

        foreach ($dpl as $d) {
            array_push($arr_hasil, array_merge($d, array("MHS_KKN" => $this->load_mahasiswa_kelompok($d['ID_KKN_KELOMPOK']))));
        }
        return $arr_hasil;
    }

    function load_mahasiswa_kelompok($id_kkn) {
        return $this->db->QueryToArray("
            SELECT KKM.*,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS,JAL.NM_JALUR,
            NVL(PMKG.ID_PENGAMBILAN_MK,KKM.ID_PENGAMBILAN_MK) ID_PENGAMBILAN_MK,NVL(PMKG.NILAI_HURUF,PMK.NILAI_HURUF) NILAI_HURUF,NVL(PMKG.NILAI_ANGKA,PMK.NILAI_ANGKA) NILAI_ANGKA
            FROM KKN_KELOMPOK_MHS KKM
            JOIN KKN_KELOMPOK KK ON KK.ID_KKN_KELOMPOK=KKM.ID_KKN_KELOMPOK
            JOIN MAHASISWA M ON M.ID_MHS=KKM.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            JOIN JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND JM.IS_JALUR_AKTIF=1
            JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
            LEFT JOIN PENGAMBILAN_MK PMK ON PMK.ID_PENGAMBILAN_MK=KKM.ID_PENGAMBILAN_MK
            LEFT JOIN (
              SELECT PMK.ID_SEMESTER,PMK.ID_MHS,PMK.ID_PENGAMBILAN_MK,PMK.NILAI_HURUF,PMK.NILAI_ANGKA
              FROM PENGAMBILAN_MK PMK
              LEFT JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK=PMK.ID_KELAS_MK
              LEFT JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
              WHERE KUMK.STATUS_MKTA = 2 
              AND PMK.STATUS_APV_PENGAMBILAN_MK = 1                
            ) PMKG ON PMKG.ID_MHS=KKM.ID_MHS AND PMKG.ID_SEMESTER=KK.ID_SEMESTER
            WHERE KKM.ID_KKN_KELOMPOK='{$id_kkn}' AND KKM.TERJUN=1
            ");
    }

    // Penilaian

    function get_nilai_huruf_akhir($nilai) {
        $nilai_huruf_akhir = '';
        $query = "SELECT SN.NM_STANDAR_NILAI,PN.NILAI_MAX_PERATURAN_NILAI AS NILAI_MAX,PN.NILAI_MIN_PERATURAN_NILAI AS NILAI_MIN 
			FROM PERATURAN_NILAI PN
			JOIN STANDAR_NILAI SN ON PN.ID_STANDAR_NILAI = SN.ID_STANDAR_NILAI
            WHERE PN.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'";

        // Cari Standar Nilai Untuk Nilai Angka
        foreach ($this->db->QueryToArray($query) as $data) {
            if ($nilai <= $data['NILAI_MAX'] && $nilai >= $data['NILAI_MIN']) {
                $nilai_huruf_akhir = $data['NM_STANDAR_NILAI'];
            }
        }
        return $nilai_huruf_akhir;
    }

}

?>
