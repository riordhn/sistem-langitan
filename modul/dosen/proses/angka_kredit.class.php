<?php

//==================================

class angka_kredit {

    public $db;
    public $data_angka_kredit = array();

    function angka_kredit($db) {
        $this->db = $db;
    }

    function load_data($id_dosen) {
        $this->db->open();
        $this->db->query("select * from dosen_kredit where id_dosen='" . $id_dosen . "' order by id_dosen_kredit asc");
        $xyz = 0;
        while ($tmp = $this->db->fetcharray()) {
            $xyz++;
            array_push($this->data_angka_kredit, array(
                'no_urut' => $xyz,
                'id_dosen_kredit' => $tmp['ID_DOSEN_KREDIT'],
                'id_dosen' => $tmp['ID_DOSEN'],
                'kegiatan' => $tmp['KEGIATAN_DOSEN_KREDIT'],
                'kode' => $tmp['KODE_DOSEN_KREDIT'],
                'bukti' => $tmp['BUKTI_DOSEN_KREDIT'],
                'kepatuhan' => $tmp['KEPATUHAN_DOSEN_KREDIT'],
                'nilai' => $tmp['NILAI_DOSEN_KREDIT'],
            ));
        }
        $this->db->close();
    }

    function cetak_angka_kredit($id_dosen) {
        $tmp = $id_dosen;
        $this->load_data($id_dosen);
        $tmp = "";
        $total = 0;
        $tmp = $tmp . "<table class='ui-widget' width='70%'>";
        $tmp = $tmp . "<tr class='ui-widget-header'><th colspan=6 class='header-coloumn'><h2>Angka Kredit Dosen</h2></th></tr>";
        $tmp = $tmp . "<tr class='ui-widget-content'><td><h3>No.</h3></td><td><h3>Kegiatan</h3></td><td><h3>Kode</h3></td>
						<td><h3>Bukti</h3></td><td><h3>Kepatuhan</h3></td><td><h3>Nilai</h3></td></tr>";
        foreach ($this->data_angka_kredit as $data) {
            $tmp = $tmp . "<tr class='ui-widget-content'>";
            $tmp = $tmp . "<td>" . $data['no_urut'] . "</td>";
            $tmp = $tmp . "<td>" . $data['kegiatan'] . "</td>";
            $tmp = $tmp . "<td>" . $data['kode'] . "</td>";
            $tmp = $tmp . "<td>" . $data['bukti'] . "</td>";
            $tmp = $tmp . "<td>" . $data['kepatuhan'] . "</td>";
            $tmp = $tmp . "<td>" . $data['nilai'] . "</td>";
            $tmp = $tmp . "</tr>";
            $total = $total + $data['nilai'];
        }

        $tmp = $tmp . "<tr class='ui-widget-content'><td colspan=4>&nbsp;</td><td><h3>Total</h3></td><td><h3>" . $total . "</h3></td></tr>";
        $tmp = $tmp . "</table>";

        return $tmp;
    }

}

//==================================?>