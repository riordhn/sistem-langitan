<?php
include 'config.php';
include 'proses/Penelitian.class.php';
include '../../modul/lppm/class/ProposalPenelitian.class.php';
include '../../includes/fpdf/fpdf.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$mode = get('mode', 'list');

$ProposalPenelitian = new ProposalPenelitian($db);

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if (post('mode') == 'upload-file')
    {
		$ProposalPenelitian->UploadFileProposal($_POST, $_FILES['file']);
		$smarty->assign('lokasi_file', "../../files/upload/dosen/proposal/" . $_POST['token'] . ".pdf");
		$smarty->assign('nama_file', $_FILES['file']['name']);
    }
	
	if (post('mode') == 'delete-file')
	{
		$result = $ProposalPenelitian->DeleteFileProposal($_POST);
		echo $result; 
		exit();
	}
	
	if (post('mode') == 'add')
    {	
		$result = $ProposalPenelitian->Add($_POST);
		$smarty->assign('result', $result);
		$mode = 'add-result';
    }
    
    if (post('mode') == 'edit')
    {
		$result = $ProposalPenelitian->Edit($_POST);
		$smarty->assign('result', $result);
		$smarty->assign('id_penelitian', post('id_penelitian'));

		$mode = 'edit-result';
    }
	
	if (post('mode') == 'delete')
	{
		$result = $ProposalPenelitian->Delete($_POST);
		$smarty->assign('result', $result);
		
		$mode = 'delete-result';
	}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	if ($mode == 'list')
	{
		$smarty->assignByRef('proposal_set', $ProposalPenelitian->GetListProposalForDosen($user->ID_DOSEN));
	}
	
	if ($mode == 'add')
	{
		$new_id = $ProposalPenelitian->GetNewID();
        
        $smarty->assign('token', md5($new_id . time()));
        $smarty->assign('id_penelitian', $new_id);
		$smarty->assign('peneliti', $ProposalPenelitian->GetPeneliti($user->ID_DOSEN));
        $smarty->assign('penelitian_jenis_set', $ProposalPenelitian->GetListJenisPenelitian());
        $smarty->assign('lppm', $ProposalPenelitian->GetKetuaLPPM());
        $smarty->assign('golongan_set', $ProposalPenelitian->GetListGolongan());
	}
	
	if ($mode == 'edit')
    {
        $id_penelitian = get('id_penelitian');
        $proposal = $ProposalPenelitian->GetProposal($id_penelitian);
        
        $smarty->assign('token', md5($id_penelitian . time()));
        $smarty->assign('proposal', $proposal);
        $smarty->assign('penelitian_jenis_set', $ProposalPenelitian->GetListJenisPenelitian());
        $smarty->assign('penelitian_skim_set', $ProposalPenelitian->GetListSKIM($proposal['ID_PENELITIAN_JENIS']));
        
        $penelitian_bidang_set = $ProposalPenelitian->GetListBidangPenelitian($proposal['ID_PENELITIAN_JENIS']);
        $smarty->assign('penelitian_bidang_set', $penelitian_bidang_set);
        
        // Mengecek apakah pilihan bidang / tema pada yang lainnya
        foreach ($penelitian_bidang_set as $bidang)
            if ($bidang['LAINNYA'] == 1 && $bidang['ID_PENELITIAN_BIDANG'] == $proposal['ID_PENELITIAN_BIDANG'])
                $smarty->assign('bidang_lain', true); 
            
        $smarty->assign('lppm', $ProposalPenelitian->GetKetuaLPPM());
    }
	
	if ($mode == 'delete')
	{
		$id_penelitian = get('id_penelitian');
        $proposal = $ProposalPenelitian->GetProposal($id_penelitian);
		$smarty->assign('proposal', $proposal);
	}
	
	if ($mode == 'get-penelitian-skim')
    {   
        $penelitian_skim_set = $ProposalPenelitian->GetListSKIM(get('id_penelitian_jenis'));
        
        if (count($penelitian_skim_set) > 0)
        {
            foreach ($penelitian_skim_set as $ps)
            {
                if ($ps['KODE_SKIM'] != '')
                    echo "<option value=\"{$ps['ID_PENELITIAN_SKIM']}\">[{$ps['KODE_SKIM']}] - {$ps['NAMA_SKIM']}</option>";
                else
                    echo "<option value=\"{$ps['ID_PENELITIAN_SKIM']}\">{$ps['NAMA_SKIM']}</option>";
            }
        }
        else
        {
            echo "0";
        }
            
        exit();
    }
    
    if ($mode == 'get-penelitian-bidang')
    {       
        $penelitian_bidang_set = $ProposalPenelitian->GetListBidangPenelitian(get('id_penelitian_jenis'));
        
        foreach ($penelitian_bidang_set as $pb)
            echo "<option value=\"{$pb['ID_PENELITIAN_BIDANG']}\">{$pb['NAMA_BIDANG']} \ {$pb['NAMA_TEMA']}</option>";
        
        exit();
    }
}

$smarty->display("sample/penelitian/proposal/{$mode}.tpl");
?>
