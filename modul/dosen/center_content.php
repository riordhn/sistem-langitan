<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

// mendapatkan lokasi halaman
$location = explode('-',  $_GET['page']);
$tab_index = 0;
$page = "";

// Cek Tab
for ($i = 0; $i < count($xml_menu->menu); $i++) {
    if ($xml_menu->menu[$i]->id == $location[0]) {
        $tab_index = $i;
        $page = $xml_menu->menu[$i]->page;
    }
}

// Cek Menu
if (count($location) == 2) {
    for ($i = 0; $i < count($xml_menu->menu[$tab_index]->tab); $i++) {
        if ($xml_menu->menu[$tab_index]->tab[$i]->id == $location[1]) {
            $page = $xml_menu->menu[$tab_index]->tab[$i]->page;
        }
    }
}
if (count($location) == 3) {
    $page = $location[2] . '.php';
}

// Callback page
include($page);
?>