<?php

include 'config.php';

include 'proses/ujian.class.php';

$uj = new ujian($db, $user->ID_PENGGUNA, $user->ID_DOSEN);

$smarty->assign('jadwal_uts', $uj->load_jadwal_pengawas_uts());
$smarty->assign('jadwal_uas', $uj->load_jadwal_pengawas_uas());
$smarty->assign('title', 'Jadwal Ujian');
$smarty->display('display-jadwal/jadwal_ujian.tpl');
?>