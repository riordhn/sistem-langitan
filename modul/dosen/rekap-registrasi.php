<?php
include 'config.php';
include 'proses/utility.class.php';
include '../admin/class/laporan.class.php';
include 'proses/master.class.php';
include 'proses/list_data.class.php';

// penambahan id sesuai ptnu dan parameter $id_pt pada construct class list_data
// Fikrie //
$id_pt = $id_pt_user;


// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$laporan = new laporan($db);
$master = new master($db);
$list = new list_data($db,$id_pt);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $smarty->assign('data_rekap_pembayaran', $laporan->load_rekap_pembayaran(get('fakultas'), get('semester')));
    } else if (get('mode') == 'load_mhs') {
        $smarty->assign('data_status_bayar', $laporan->load_data_status_pembayaran(get('fakultas'), get('prodi'), get('semester'), get('status'), get('jalur'),get('krs')));
        $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
        $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        $smarty->assign('data_status_one', $list->get_status(get('status')));
    }
}

$smarty->assign('count_data_status', count($master->load_status_pembayaran()));
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_status', $master->load_status_pembayaran());
$smarty->display('sample/rekapitulasi/rekap-registrasi.tpl');
?>
