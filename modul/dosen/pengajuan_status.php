<?php
include 'config.php';
include 'proses/pengajuan_status.class.php';
include 'proses/info_dosen.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$pengajuan_status = new pengajuan_status($db, $login->id_pengguna, $login->id_role);
$info_dosen = new info_dosen($db, $login->id_pengguna);

$list_status=$pengajuan_status->get_list_status();
$data_info_dosen = $info_dosen->set();

if (isset($_POST['save'])) {
    $pengajuan_status->insert($_POST['status'], date('d-M-y', strtotime($_POST['mulai'])), date('d-M-y', strtotime($_POST['selesai'])), $_POST['keterangan']);
}

$smarty->assign('status', $list_status);
$smarty->assign('data_info', array(
    'nama' => $data_info_dosen['NAMA'],
    'kelamin' => $data_info_dosen['KELAMIN'],
    'tgl_lahir' => $data_info_dosen['TGL_LAHIR'],
    'tempat_lahir' => $data_info_dosen['TEMPAT_LAHIR'],
    'alamat' => $data_info_dosen['ALAMAT_RUMAH']
));
$smarty->display('sample/biodata/pengajuan_status.tpl');
?>