<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

	$id_fakultas = array();
	$nm_fakultas = array();
	$jml_fakultas = 0;
	
	$id_prodi = array();
	$nm_prodi = array();
	$nm_jenjang = array();
	$prodi_nama = array();
	$prodi_peminat = 0;
	$prodi_diterima = 0;
	$prodi_daftar_ulang = 0;
	$prodi_mala_aktif = 0;
	$jml_prodi = 0;

	
//Spek Prodi
	$id_program_studi = array();                        
	$prodi_nama = array();                           
	$prodi_jenis = array();                           
	$prodi_kategori = array();                        
	$prodi_penjelasan = array();                     
	$prodi_tahun = array();                           
	$prodi_sk_oleh = array();                         
	$prodi_sk_nomor = array();                        
	$prodi_sk_terbit = array();                       
	$prodi_sk_expired = array();                      
	$prodi_skp_nomor = array();                       
	$prodi_skp_terbit = array();                      
	$prodi_skp_expired = array();                     
	$prodi_ketua_nama = array();                     
	$prodi_ketua_sk_nomor = array();                  
	$prodi_ketua_sk_terbit = array();                 
	$prodi_ketua_sk_expired = array();                
	$prodi_sekre_nama = array();                     
	$prodi_sekre_sk_nomor = array();                  
	$prodi_sekre_sk_terbit = array();                 
	$prodi_sekre_sk_expired = array();                
	$prodi_dosen_tetap = array();                     
	$prodi_dosen_tdktetap = array();                  
	$prodi_dosen_s2 = array();                        
	$prodi_dosen_s3 = array();                        
	$prodi_dosen_gubes = array();                     
	$prodi_kur_nama = array();                        
	$prodi_kur_sk_nomor = array();                    
	$prodi_kur_sk_terbit = array();                   
	$prodi_kur_sk_expired = array();                  
	$prodi_kur_masa_studi = array();                  
	$prodi_kur_beban_studi = array();                 
	$prodi_lo = array();                             
	$prodi_gelar_nama = array();                     
	$prodi_gelar_singkat = array();                   
	$prodi_gelar_sk_nomor = array();                  
	$prodi_dayatampung = array();                     
	$prodi_dayatampung_sk_nomor = array();            
	$prodi_jml_mhs = array();                         
	$prodi_jml_peminat = array();                     
	$prodi_jml_maba_calon = array();                  
	$prodi_jml_maba_daftar = array();                 
	$prodi_jml_mhs_lama = array();                    
	$prodi_jml_alumni = array();                      
	$prodi_akred = array();                            
	$prodi_akred_sk_nomor = array();                  
	$prodi_akred_sk_terbit = array();                 
	$prodi_akred_sk_expired = array();                
	$prodi_akred_nilai = array();                     
	$prodi_akred_peringkat = array();                 
	$prodi_biaya_sk_nomor = array();                  
	$prodi_biaya_sop = array();                       
	$prodi_biaya_sp3 = array();                       
	$prodi_biaya_matrikulasi = array();               
	$prodi_biaya_pendaftaran = array();               
	$prodi_ppcmb = array();                          
	$prodi_ppcmb_ijazah = array();                   
	$prodi_ppcmb_umur = array();                      
	$prodi_ppcmb_kelamin = array();                   
	$prodi_ppcmb_ketunaan = array();                 
	$prodi_ppcmb_tunanetra = array();                
	$prodi_ppcmb_tunarungu = array();                
	$prodi_ppcmb_tunafisik = array();                
	$prodi_ppcmb_tunawicara = array();               
	$prodi_ppcmb_tunalain = array();                 
	$prodi_ppcmb_tinggibadan = array();              
	$prodi_ppcmb_elpt = array();
	$is_alih_jenis = array();


	$nm_fakultas_get = "";

	$nm_program_studi = "";
	
	
		$get_id_fakultas = $user->ID_FAKULTAS_JABATAN;
		
		$db->Query("select ps.id_program_studi, 
					ps.nm_program_studi, 
					j.nm_jenjang, 
					prospek.prodi_nama,
					prospek.prodi_jenis,
					prospek.prodi_kategori,
					prospek.prodi_penjelasan,
					prospek.prodi_tahun,
					prospek.prodi_sk_oleh,
					prospek.prodi_sk_nomor,
					prospek.prodi_sk_terbit,
					prospek.prodi_sk_expired,
					prospek.prodi_skp_nomor,
					prospek.prodi_skp_terbit,
					prospek.prodi_skp_expired,
					prospek.prodi_ketua_nama,
					prospek.prodi_ketua_sk_nomor,
					prospek.prodi_ketua_sk_terbit,
					prospek.prodi_ketua_sk_expired,
					prospek.prodi_sekre_nama,
					prospek.prodi_sekre_sk_nomor,
					prospek.prodi_sekre_sk_terbit,
					prospek.prodi_sekre_sk_expired,
					prospek.prodi_dosen_tetap,
					prospek.prodi_dosen_tdktetap,
					prospek.prodi_dosen_s2,
					prospek.prodi_dosen_s3,
					prospek.prodi_dosen_gubes,
					prospek.prodi_kur_nama,
					prospek.prodi_kur_sk_nomor,
					prospek.prodi_kur_sk_terbit,
					prospek.prodi_kur_sk_expired,
					prospek.prodi_kur_masa_studi,
					prospek.prodi_kur_beban_studi,
					prospek.prodi_lo,
					prospek.prodi_gelar_nama,
					prospek.prodi_gelar_singkat,
					prospek.prodi_gelar_sk_nomor,
					prospek.prodi_jml_mhs,
					prospek.prodi_jml_peminat,
					prospek.prodi_jml_maba_calon,
					prospek.prodi_jml_maba_daftar,
					prospek.prodi_jml_mhs_lama,
					prospek.prodi_jml_alumni,
					prospek.prodi_akred,
					prospek.prodi_akred_sk_nomor,
					prospek.prodi_akred_sk_terbit,
					prospek.prodi_akred_sk_expired,
					prospek.prodi_akred_nilai,
					prospek.prodi_akred_peringkat,
					prospek.prodi_lo,
					prospek.is_alih_jenis
					from prodi_spesifikasi prospek
					left join program_studi ps on ps.id_program_studi = prospek.id_program_studi
					left join fakultas f on f.id_fakultas = ps.id_fakultas
					left join jenjang j on j.id_jenjang = ps.id_jenjang
					where ps.id_fakultas = '$get_id_fakultas' 
					order by prospek.id_program_studi");

								$i = 0;
								while ($row = $db->FetchRow()){ 
									$id_prodi[$i] = $row[0];
									$nm_prodi[$i] = $row[1];
									$nm_jenjang[$i] = $row[2];
									$prodi_nama[$i] = $row[3];
									$prodi_jenis[$i] = $row[4];                        
									$prodi_kategori[$i] = $row[5];                      
									$prodi_penjelasan[$i] = $row[6];                  
									$prodi_tahun[$i] = $row[7];                            
									$prodi_sk_oleh[$i] = $row[8];                          
									$prodi_sk_nomor[$i] = $row[9];                         
									$prodi_sk_terbit[$i] = $row[10];                        
									$prodi_sk_expired[$i] = $row[11];                       
									$prodi_skp_nomor[$i] = $row[12];                        
									$prodi_skp_terbit[$i] = $row[13];                       
									$prodi_skp_expired[$i] = $row[14];                      
									$prodi_ketua_nama[$i] = $row[15];                      
									$prodi_ketua_sk_nomor[$i] = $row[16];                   
									$prodi_ketua_sk_terbit[$i] = $row[17];                  
									$prodi_ketua_sk_expired[$i] = $row[18];             
									$prodi_sekre_nama[$i] = $row[19];                      
									$prodi_sekre_sk_nomor[$i] = $row[20];                  
									$prodi_sekre_sk_terbit[$i] = $row[21];                 
									$prodi_sekre_sk_expired[$i] = $row[22];                
									$prodi_dosen_tetap[$i] = $row[23];                     
									$prodi_dosen_tdktetap[$i] = $row[24];                  
									$prodi_dosen_s2[$i] = $row[25];                        
									$prodi_dosen_s3[$i] = $row[26];                        
									$prodi_dosen_gubes[$i] = $row[27];                     
									$prodi_kur_nama[$i] = $row[28];                        
									$prodi_kur_sk_nomor[$i] = $row[29];                    
									$prodi_kur_sk_terbit[$i] = $row[30];                   
									$prodi_kur_sk_expired[$i] = $row[31];                  
									$prodi_kur_masa_studi[$i] = $row[32];                  
									$prodi_kur_beban_studi[$i] = $row[33];            
									$prodi_lo[$i] = $row[34];                            
									$prodi_gelar_nama[$i] = $row[35];                     
									$prodi_gelar_singkat[$i] = $row[36];                   
									$prodi_gelar_sk_nomor[$i] = $row[37];                            
									$prodi_jml_mhs[$i] = $row[38];                         
									$prodi_jml_peminat[$i] = $row[39];                     
									$prodi_jml_maba_calon[$i] = $row[40];                  
									$prodi_jml_maba_daftar[$i] = $row[41];                 
									$prodi_jml_mhs_lama[$i] = $row[42];                    
									$prodi_jml_alumni[$i] = $row[43];                      
									$prodi_akred[$i] = $row[44];                            
									$prodi_akred_sk_nomor[$i] = $row[45];                 
									$prodi_akred_sk_terbit[$i] = $row[46];                 
									$prodi_akred_sk_expired[$i] = $row[47];                
									$prodi_akred_nilai[$i] = $row[48];                     
									$prodi_akred_peringkat[$i] = $row[49];
									$prodi_lo[$i] = $row[50];
									$is_alih_jenis[$i] = $row[51];
									$jml_prodi++;
									$i++;
								}
		
?>

<!--
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
  -->
  <!--
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
  -->
	<style>
		#feedback { font-size: 1.2em; }
		#selectable .ui-selecting { background: #FECA40; }
		#selectable .ui-selected { background: #F39814; color: white; }
		#selectable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
		#selectable td { margin: 3px; padding: 0.4em; font-size: 1.2em; height: 14px; }
	</style>
 
	<script>
	$(function() {
		$( "#radio_PPCMB_1" ).buttonset();
		$( "#radio_PPCMB_3" ).buttonset();
		$( "#submit" ).buttonset();
		$( "#btn_ubah" ).buttonset();
		$("#prodi_sk_terbit").datepicker();
		$("#prodi_sk_expired").datepicker();
		$("#prodi_skp_terbit").datepicker();
		$("#prodi_skp_expired").datepicker();
		$("#prodi_ketua_sk_terbit").datepicker();
		$("#prodi_ketua_sk_expired").datepicker();
		$("#prodi_sekre_sk_terbit").datepicker();
		$("#prodi_sekre_sk_expired").datepicker();
		$("#prodi_kur_sk_terbit").datepicker();
		$("#prodi_kur_sk_expired").datepicker();
		$("#prodi_gelar_sk_terbit").datepicker();
		$("#prodi_gelar_sk_expired").datepicker();
		$("#prodi_akred_sk_terbit").datepicker();
		$("#prodi_akred_sk_expired").datepicker();
	});
	</script>
	
	<script>
		$(document).ready(function(){

				$().ajaxStart(function() {
					$('#loading').fadeIn();
					$('#result').hide();
				}).ajaxStop(function() {
					$('#loading').hide();
					$('#result').fadeIn('fast');
				});

				$('#prodi_spesifikasi').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
							$('#list_prodi').hide();
							$('body').animate({scrollTop: $("body").offset().top},'slow');
							$('#result').fadeIn('fast');
							$('#result').html(data);
							
						}
					})
					return false;
				});
		});
	</script>
	
	<script>
		$(function() {
			$( "#slider_PPCMB_5" ).slider({
				value:31,
				min: 0,
				max: 100,
				step: 1,
				slide: function( event, ui ) {
					$( "#prodi_ppcmb_tunanetra" ).val(ui.value + " %");
				}
			});
			$( "#prodi_ppcmb_tunanetra" ).val($( "#slider_PPCMB_5" ).slider( "value" ) + " %" );
		});
		
		$(function() {
			$( "#slider_PPCMB_6" ).slider({
				value:31,
				min: 0,
				max: 100,
				step: 1,
				slide: function( event, ui ) {
					$( "#prodi_ppcmb_tunarungu" ).val(ui.value + " %");
				}
			});
			$( "#prodi_ppcmb_tunarungu" ).val($( "#slider_PPCMB_6" ).slider( "value" ) + " %" );
		});
		
	</script>

<script>
	$(function() {
		$( "#tabs" ).tabs();
	});
</script>

		<style type="text/css">
			h4 { font-size: 18px; }
			input { padding: 3px; border: 1px solid #999; }
			td { padding: 5px; }
			#result { background-color: #ffffcc; border: 1px solid #ffff99; padding: 10px; width: 100%; margin-bottom: 20px; }
		</style>

<div class="center_title_bar">SPESIFIKASI PROGRAM STUDI <?php if($_GET['alihjenis']==1){echo 'ALIH JENIS';} ?></div> 
 
<?php
	include 'get_user_agent.php';
	$browser = $ua['name'];
	if($browser!=='Google Chrome'){
?>
<span style="font-size:18px;">Mohon gunakan browser </span> <img src="chrome_logo.gif" /> <span style="font-size:18px;">untuk mengakses modul ini</span>
<br />
Atau download Google Chrome di link ini, jika anda tidak memiliki browser tersebut : <a onclick="window.location.href='http://www.google.com/chrome/?hl=en&brand=chmo#eula'" href="">Google Chrome Downloads</a>
<?php
	}
	else{
?>
		<div id="loading" style="display:none;"><img src="loading.gif" alt="loading..." /></div>
		<div id="result" style="display:none; padding-top:10px; width:80%;"></div>
<div id="list_prodi">
	<div id="tabs" style="width:790px;">
		<ul>
			<li><a href="#tabs-1">Non Alih Jenis</a></li>
			<li><a href="#tabs-2">Alih Jenis</a></li>
		</ul>
		<div id="tabs-1">
			<table class="ui-widget" width="90%" >
				<thead class="ui-widget-header">
					<tr class="ui-widget-header">
						<td class="header-coloumn">
							NO.
						</td>
						<td class="header-coloumn">
							Nama Program Studi Sekarang
						</td>
						<!--
						<td class="header-coloumn">
							Keterangan
						</td>
						-->
					</tr>
				</thead>
				
				<tbody >
				<?php
					for($i=0;$i<$jml_prodi;$i++){
					if($is_alih_jenis[$i]==0){
				?>
				<tr>
					<td class="ui-widget-content">
						<?php
							echo $i+1;
						?>
					</td>
					<td class="ui-widget-content">
						<a href="spek_prodi.php?id=<?php echo $id_prodi[$i]; ?>&prodi=<?php echo $i; ?>&alihjenis=0">
						<?php
							echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i];
						?>
						</a>
					</td>
				</tr>
				<?php
						}
					}
				?>
				</tbody>
			</table>
		</div>

	<div id="tabs-2">
			<table class="ui-widget" width="90%" >
				<thead class="ui-widget-header">
					<tr class="ui-widget-header">
						<td class="header-coloumn">
							NO.
						</td>
						<td class="header-coloumn">
							Nama Program Studi Sekarang
						</td>
						<!--
						<td class="header-coloumn">
							Keterangan
						</td>
						-->
					</tr>
				</thead>
				
				<tbody >
				<?php
					for($i=0;$i<$jml_prodi;$i++){
					if($is_alih_jenis[$i]==1){
				?>
				<tr>
					<td class="ui-widget-content">
						<?php
							echo $i+1;
						?>
					</td>
					<td class="ui-widget-content">
						<a href="spek_prodi.php?id=<?php echo $id_prodi[$i]; ?>&prodi=<?php echo $i; ?>&alihjenis=1">
						<?php
							echo $nm_jenjang[$i] . ' ' . $nm_prodi[$i];
						?>
						</a>
					</td>
				</tr>
				<?php
						}
					}
				?>
				</tbody>

			</table>
	</div>

	</div>
</div>

<div style="clear:both;"></div>

<?php
	if(isset($_GET['prodi']) && isset($_GET['id'])){
?>


<div>

<div class="center_title_bar"><?php if(isset($_GET['prodi'])){$i = $_GET['prodi']; echo $nm_prodi[$i];} ?>&nbsp;<?php if($_GET['alihjenis']==1){echo 'ALIH JENIS';} ?></div>

<form id="prodi_spesifikasi"  name="prodi_spesifikasi" method="post" action="spek_prodi_insert.php">  
	<table width="90%" >
	<thead class="ui-widget-header">
		<tr>
			<td>Nama Spesifikasi</td>
			<td>Spesifikasi Program Studi</td>
			<td>Keterangan</td>
		</tr>
	</thead>
	
	<tbody class="ui-widget-content">
				<tr class="ui-widget-content">
					<td><strong>Nama Program Studi</strong></td>
					<td>
					<div id="program_studi_div" name="program_studi_div">
						<input style="display:none;" type="text" name="id_prodi" value="<?php if(isset($_GET['id'])){echo $_GET['id'];} ?>" >
						<input style="display:none;" type="text" name="alihjenis" value="<?php if(isset($_GET['alihjenis'])){echo $_GET['alihjenis'];} ?>" >
						<TEXTAREA style="width:100%;" id="prodi" disabled="disabled"  NAME="prodi" COLS=30 ROWS=3><?php if(isset($_GET['prodi'])){$i = $_GET['prodi']; echo $nm_prodi[$i];} ?></TEXTAREA>
					</div>
					</td>
					<td><label style="width:100%;" NAME="prodi_ket"></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td style="width:200px;"><strong>Nama Prodi Baru</strong></td>
					<td>
					<div id="program_studi_div" name="program_studi_div">
						<TEXTAREA style="width:100%;" id="prodi_nama" NAME="prodi_nama" COLS=30 ROWS=3><?php if(isset($_GET['prodi'])){$i = $_GET['prodi']; echo $prodi_nama[$i];} ?></TEXTAREA>	
					</div>
					</td>
					<td><label style="width:100%;" NAME="prodi_ket" COLS=30 ROWS=3>Harus sesuai dengan yang tertulis di SK Pendirian (kalau belum ada perubahan) </label></td>
				</tr>
				<tr class="ui-widget-content">
					<td><strong>Jenis Prodi</strong></td>
					<td>
						<select name="prodi_jenis" id="prodi_jenis" >
						<option value="vokasi">Vokasi</option>
						<option value="akademik">Akademik</option>
						<option value="profesi">Profesi</option>
						<option selected value="<?php echo $prodi_jenis[$i]; ?>"><?php echo $prodi_jenis[$i]; ?></option>
						</select>
					</td>
					<td><label style="width:100%;" NAME="jenis_prodi_ket" COLS=30 ROWS=3>Vokasi = 40% Teori + 60% Praktek Akademik = 60% Teori + 40% Praktek</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td><strong>Kategori Prodi</strong></td>
					<td>
						<select name="prodi_kategori" id="prodi_kategori" >
						<option value="mono">Mono</option>
						<option value="inter">Inter</option>
						<option value="multi disiplin">Multi Disiplin</option>
						<option selected value="<?php echo $prodi_kategori[$i]; ?>"><?php echo $prodi_kategori[$i]; ?></option>
						</select>
					</td>
					<td></td>
				</tr>
				<tr class="ui-widget-content">
					<td><strong>Penjelasan Kategori Prodi</td>
					<td>
						<TEXTAREA style="width:300px;" NAME="prodi_penjelasan" COLS=30 ROWS=10><?php echo $prodi_penjelasan[$i]; ?></TEXTAREA>
					</td>
					<td><label style="width:100%;" NAME="PKP_KET" COLS=30 ROWS=3></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td><strong>Berdiri sejak tahun</strong></td>
					<td><input type="text" id="prodi_tahun" name="prodi_tahun" value="<?php echo $prodi_tahun[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="BST_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Ditetapkan dgn SK dari</td>
					<td><input type="text" id="prodi_sk_oleh" name="prodi_sk_oleh" value="<?php echo $prodi_sk_oleh[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="BST_1_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Nomor SK</td>
					<td><input type="text" id="prodi_sk_nomor" name="prodi_sk_nomor" value="<?php echo $prodi_sk_nomor[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="BST_2_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Tanggal Terbit SK</td>
					<td><input type="text" id="prodi_sk_terbit" name="prodi_sk_terbit" value="<?php echo $prodi_sk_terbit[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="BST_3_KET" COLS=30 ROWS=2>Format : mm/dd/yyyy</label></td>
				</tr>
				<!--
				<tr class="ui-widget-content">
					<td>Masa Berlaku s.d.</td>
					<td><input type="text" id="prodi_sk_expired" name="prodi_sk_expired" value="<?php echo $prodi_sk_expired[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="BST_4_KET" COLS=30 ROWS=2></label></td>
				</tr>
				-->
				<tr class="ui-widget-content">
					<td><strong>SK Izin Penyelenggaraan</strong></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr class="ui-widget-content">
					<td>Nomor SK</td>
					<td><input type="text" id="prodi_skp_nomor" name="prodi_skp_nomor" value="<?php echo $prodi_skp_nomor[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="SPI_1_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Tanggal Terbit SK</td>
					<td><input type="text" id="prodi_skp_terbit" name="prodi_skp_terbit" value="<?php echo $prodi_skp_terbit[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="SPI_2_KET" COLS=30 ROWS=2>Format : mm/dd/yyyy</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Masa Berlaku s.d.</td>
					<td><input type="text" id="prodi_skp_expired" name="prodi_skp_expired" value="<?php echo $prodi_skp_expired[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="SPI_3_KET" COLS=30 ROWS=2>Format : mm/dd/yyyy</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td><strong>Nama Ketua Prodi</strong></td>
					<td><input type="text" id="prodi_ketua_nama" name="prodi_ketua_nama" value="<?php echo $prodi_ketua_nama[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="NKP_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Nomor SK Rektor</td>
					<td><input type="text" id="prodi_ketua_sk_nomor" name="prodi_ketua_sk_nomor" value="<?php echo $prodi_ketua_sk_nomor[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="NKP_1_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Tanggal terbit SK</td>
					<td><input type="text" id="prodi_ketua_sk_terbit" name="prodi_ketua_sk_terbit" value="<?php echo $prodi_ketua_sk_terbit[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="NKP_1_KET" COLS=30 ROWS=2>Format : mm/dd/yyyy</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Masa jabatan s.d.</td>
					<td><input type="text" id="prodi_ketua_sk_expired" name="prodi_ketua_sk_expired" value="<?php echo $prodi_ketua_sk_expired[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="NKP_2_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td><strong>Nama Sekretaris Prodi</strong></td>
					<td><input type="text" id="prodi_sekre_nama" name="prodi_sekre_nama" value="<?php echo $prodi_sekre_nama[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="NSP_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Nomor SK Rektor</td>
					<td><input type="text" id="prodi_sekre_sk_nomor" name="prodi_sekre_sk_nomor" value="<?php echo $prodi_sekre_sk_nomor[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="NSP_1_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Tanggal terbit SK</td>
					<td><input type="text" id="prodi_sekre_sk_terbit" name="prodi_sekre_sk_terbit" value="<?php echo $prodi_sekre_sk_terbit[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="NSP_1_KET" COLS=30 ROWS=2>Format : mm/dd/yyyy</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Masa jabatan s.d.</td>
					<td><input type="text" id="prodi_sekre_sk_expired" name="prodi_sekre_sk_expired" value="<?php echo $prodi_sekre_sk_expired[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="NSP_2_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td><strong>Jumlah Dosen</strong></td>
					<td>&nbsp;</td>
					<td></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Jumlah Dosen Tetap</td>
					<td><input disabled="disabled" type="text" id="prodi_dosen_tetap" name="prodi_dosen_tetap" value="<?php echo $prodi_dosen_tetap[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="JD_1_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Jumlah Dosen Tidak Tetap</td>
					<td><input disabled="disabled" type="text" id="prodi_dosen_tdktetap" name="prodi_dosen_tdktetap" value="<?php $prodi_dosen_tdktetap[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="JD_2_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Jumlah Dosen Lulusan S-2</td>
					<td><input disabled="disabled" type="text" id="prodi_dosen_s2" name="prodi_dosen_s2" value="<?php echo $prodi_dosen_s2[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="JD_3_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Jumlah Dosen Lulusan S-3</td>
					<td><input disabled="disabled" type="text" id="prodi_dosen_s3" name="prodi_dosen_s3" value="<?php echo $prodi_dosen_s3[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="JD_3_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Jumlah Dosen Guru Besar</td>
					<td><input disabled="disabled" type="text" id="prodi_dosen_gubes" name="prodi_dosen_gubes" value="<?php echo $prodi_dosen_gubes[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="JD_5_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td><strong>Kurikulum Prodi</strong></td>
					<td><input type="text" id="prodi_kur_nama" name="prodi_kur_nama" value="<?php echo $prodi_kur_nama[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="KP_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Nomor SK Rektor</td>
					<td><input type="text" id="prodi_kur_sk_nomor" name="prodi_kur_sk_nomor" value="<?php echo $prodi_kur_sk_nomor[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="KP_1_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Tanggal terbit SK</td>
					<td><input type="text" id="prodi_kur_sk_terbit" name="prodi_kur_sk_terbit" value="<?php echo $prodi_kur_sk_terbit[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="KP_3_KET" COLS=30 ROWS=2>Format : mm/dd/yyyy</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Masa Berlaku SK</td>
					<td><input type="text" id="prodi_kur_sk_expired" name="prodi_kur_sk_expired" value="<?php echo $prodi_kur_sk_expired[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="KP_3_KET" COLS=30 ROWS=2>Format : mm/dd/yyyy</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Masa Studi</td>
					<td><input type="text" id="prodi_kur_sk_masa_studi" name="prodi_kur_masa_studi" value="<?php echo $prodi_kur_masa_studi[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="KP_4_KET" COLS=30 ROWS=2>SEMESTER</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Beban Studi</td>
					<td><input type="text" id="prodi_kur_beban_studi" name="prodi_kur_beban_studi" value="<?php echo $prodi_kur_beban_studi[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="KP_5_KET" COLS=30 ROWS=2>SKS</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td><strong>Learning Outcome Prodi</strong></td>
					<td colspan="2"><TEXTAREA style="width:98%;" NAME="prodi_lo" COLS=30 ROWS=6><?php echo $prodi_lo[$i]; ?></TEXTAREA></td>
				</tr>
				<tr class="ui-widget-content">
					<td><strong>Gelar Lulusan</strong></td>
					<td><input type="text" id="prodi_gelar_nama" name="prodi_gelar_nama" value="<?php echo $prodi_gelar_nama[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="GL_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Singkatan Gelar</td>
					<td><input type="text" id="prodi_gelar_singkat" name="prodi_gelar_singkat" value="<?php echo $prodi_gelar_singkat[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="GL_1_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Ditetapkan dgn SK Rektor</td>
					<td><input type="text" id="prodi_gelar_sk_nomor" name="prodi_gelar_sk_nomor" value="<?php echo $prodi_gelar_sk_nomor[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="GL_2_KET" COLS=30 ROWS=2></label></td>
				</tr>

				<tr class="ui-widget-content">
					<td><strong>Jumlah Mahasiswa</strong></td>
					<td><input disabled="disabled" type="text" style="width:100%; display:none;" id="prodi_jml_mhs" name="prodi_jml_mhs" value="<?php echo $prodi_jml_mhs[$i]; ?>" /></td>
					<td>&nbsp;</td>
				</tr>
				<tr class="ui-widget-content">
					<td>Jumlah Peminat TA 2011</td>
					<td>
					<?php //echo $prodi_jml_peminat[$i];
					$db->Query("select count(*) from CALON_MAHASISWA where ID_PILIHAN_1 = '" . $id_prodi[$i] . "' or ID_PILIHAN_2 = '" . $id_prodi[$i] . "' or ID_PILIHAN_3 = '" . $id_prodi[$i] . "' or ID_PILIHAN_4 = '" . $id_prodi[$i] . "'");
						while ($row = $db->FetchRow()){ 
							$prodi_peminat = $row[0];				
						} 
					?>
					<input disabled="disabled" type="text" id="prodi_jml_peminat" name="prodi_jml_peminat" 
					value="<?php //echo $prodi_peminat; ?>" style="width:100%;" >
					</td>
					<td><label style="width:100%;" NAME="JM_1_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Calon Maba yang Diterima TA 2011</td>
					<?php
					$db->Query("select count(*) from CALON_MAHASISWA where ID_PROGRAM_STUDI = '" . $id_prodi[$i] . "'");
					while ($row = $db->FetchRow()){ 
						$prodi_diterima = $row[0];
					}
					?>
					<td><input disabled="disabled" type="text" id="prodi_jml_maba" name="prodi_jml_maba_calon" value="<?php /*echo $prodi_jml_maba_calon[$i];*/ echo $prodi_diterima; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="JM_2_KET" COLS=30 ROWS=2>Ditetapkan dgn SK Rektor</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Maba Mendaftar Ulang TA 2011</td>
					<?php
					$db->Query("
						select count(*) from (
						select id_c_mhs, ID_PROGRAM_STUDI,
						(select count(*) from pembayaran_cmhs pc where tgl_bayar is not null and pc.id_c_mhs = cm.id_c_mhs) as bayar,
						length(finger_data) as sudah_finger
						from calon_mahasiswa cm)
						where bayar is not null and sudah_finger is not null and id_program_studi = '" . $id_prodi[$i] . "'
					");
					while ($row = $db->FetchRow()){ 
						$prodi_daftar_ulang = $row[0];
					}
					?>
					<td><input disabled="disabled" type="text" id="prodi_jml_maba_daftar" name="prodi_jml_maba_daftar" value="<?php echo $prodi_daftar_ulang; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="JM_3_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Mala masih terdaftar TA 2011</td>
					<?php
					$db->Query("SELECT COUNT(*) FROM MAHASISWA
								LEFT JOIN STATUS_PENGGUNA ON MAHASISWA.STATUS_AKADEMIK_MHS = STATUS_PENGGUNA.ID_STATUS_PENGGUNA 
								WHERE STATUS_AKTIF = 1 AND THN_ANGKATAN_MHS <> '2011' AND ID_ROLE = 3 AND ID_PROGRAM_STUDI = '" . $id_prodi[$i] . "'");
					while ($row = $db->FetchRow()){ 
						$prodi_mala_aktif = $row[0];
					}
					?>
					<td><input disabled="disabled" type="text" id="prodi_jml_mhs_lama" name="prodi_jml_mhs_lama" value="<?php echo $prodi_mala_aktif; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="JM_4_KET" COLS=30 ROWS=2>Ditetapkan dgn SK Rektor</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td><strong>Jumlah Alumni saat ini</strong></td>
					<td><input disabled="disabled" type="text" id="prodi_jml_alumni" name="prodi_jml_alumni" value="<?php echo $prodi_jml_alumni[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="JA_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td><strong>Akreditasi</strong></td>
					<td><input type="text" id="prodi_akred" name="prodi_akred" value="<?php echo $prodi_akred[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="AKRE_KET" COLS=30 ROWS=2>BAN PT</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Nomor SK</td>
					<td><input type="text" id="prodi_akred_sk_nomor" name="prodi_akred_sk_nomor" value="<?php echo $prodi_akred_sk_nomor[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="AKRE_1_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Tanggal terbit SK</td>
					<td><input type="text" id="prodi_akred_sk_terbit" name="prodi_akred_sk_terbit" value="<?php echo $prodi_akred_sk_terbit[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="AKRE_2_KET" COLS=30 ROWS=2>Format : mm/dd/yyyy</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Masa Berlaku SK</td>
					<td><input type="text" id="prodi_akred_sk_expired" name="prodi_akred_sk_expired" value="<?php echo $prodi_akred_sk_expired[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="AKRE_2_KET" COLS=30 ROWS=2>Format : mm/dd/yyyy</label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Nilai</td>
					<td><input type="text" id="prodi_akred_nilai" name="prodi_akred_nilai" value="<?php echo $prodi_akred_nilai[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="AKRE_3_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>Peringkat</td>
					<td><input type="text" id="prodi_akred_peringat" name="prodi_akred_peringkat" value="<?php echo $prodi_akred_peringkat[$i]; ?>" style="width:100%;" ></td>
					<td><label style="width:100%;" NAME="AKRE_4_KET" COLS=30 ROWS=2></label></td>
				</tr>
				<tr class="ui-widget-content">
					<td>&nbsp;</td>
					<td style="text-align:center;">
						<div id="submit" name="submit">
							<input type="submit" name="btn_submit" id="btn_submit" value="Submit"  />
						</div>
					</td>
					<td>&nbsp;</td>
				</tr>


		</tbody>
	</table>
	</form>
	</div>
<?php
	}
?>

<?php
	}
?>