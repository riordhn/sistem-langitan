<?php
include '../conf.php';


$prodi = $db->QueryToArray("SELECT FAKULTAS.ID_FAKULTAS, SINGKATAN_FAKULTAS, COUNT(DISTINCT(PENGAMBILAN_MK.ID_MHS)) JML
				FROM PENGAMBILAN_MK 
				JOIN MAHASISWA ON MAHASISWA.ID_MHS = PENGAMBILAN_MK.ID_MHS
				JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
				JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PENGAMBILAN_MK.ID_SEMESTER
				WHERE PENGAMBILAN_MK.STATUS_APV_PENGAMBILAN_MK = 1 AND SEMESTER.STATUS_AKTIF_SEMESTER = 'True'
				GROUP BY FAKULTAS.ID_FAKULTAS, SINGKATAN_FAKULTAS
				ORDER BY FAKULTAS.ID_FAKULTAS");

echo "<chart caption='KRS MAHASISWA PER PRODI' 
		subcaption='Jumlah Mhs yang sudah KRS $jml_data'
		xAxisName='Prodi' 
		yAxisName='Jumlah Mahasiswa'
		shownames='1' showValues='1' decimals='0' numberPrefix='' formatNumberScale='0' >
";
echo "<dataset seriesName='Jumlah Mhs Aktif' showValues='1'>";		
foreach($prodi as $data){
	echo "<set value='$data[JML]' />";
}
echo "</dataset>";

echo "</chart>";
?>
