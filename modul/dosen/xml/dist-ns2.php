<?php
include '../conf.php';

	$jml_data = 0;
	
	$nilai_A = array();
	$nilai_AB = array();
	$nilai_B = array();
	$nilai_BC = array();
	$nilai_C = array();
	$nilai_D = array();
	$nilai_E = array();
	$id_fakultas = array();
	$nm_fakultas = array();
	
	$db->Query("
			select id_fakultas, singkatan_fakultas,
			round(sum(A)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_A,
			round(sum(AB)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_AB,
			round(sum(B)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_B,
			round(sum(BC)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_BC,
			round(sum(C)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_C,
			round(sum(D)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_D,
			round(sum(E)*100/(sum(A)+sum(AB)+sum(B)+sum(BC)+sum(C)+sum(D)+sum(E)),2) as nilai_E
			from
			(select ps.id_fakultas, f.singkatan_fakultas,
			case when nilai_huruf='A' then 1 else 0 end as A,
			case when nilai_huruf='AB' then 1 else 0 end as AB,
			case when nilai_huruf='B' then 1 else 0 end as B,
			case when nilai_huruf='BC' then 1 else 0 end as BC,
			case when nilai_huruf='C' then 1 else 0 end as C,
			case when nilai_huruf='D' then 1 else 0 end as D,
			case when (nilai_huruf='E' or nilai_huruf='T' or nilai_huruf is null) then 1 else 0 end as E
			from pengambilan_mk pmk
			left join mahasiswa mhs on pmk.id_mhs=mhs.id_mhs
			left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
			left join fakultas f on f.ID_FAKULTAS = ps.ID_FAKULTAS
			left join semester smt on smt.id_semester = pmk.id_semester
			where smt.status_aktif_semester='True' and ps.id_fakultas in (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) and ps.id_jenjang = 2)
			group by id_fakultas, singkatan_fakultas order by id_fakultas 
	");
        
	$i=0;
	while ($row = $db->FetchRow()){ 
			$nilai_A[$i] = $row[2];
			$nilai_AB[$i] = $row[3];
			$nilai_B[$i] = $row[4];
			$nilai_BC[$i] = $row[5];
			$nilai_C[$i] = $row[6];
			$nilai_D[$i] = $row[7];
			$nilai_E[$i] = $row[8];
			$id_fakultas[$i] = $row[0];
			$nm_fakultas[$i] = $row[1];
			$jml_data++;
			$i++;
	}
header('Content-Type: text/xml');

echo "<chart palette='1' caption='PROFIL DISTRIBUSI NILAI (DALAM %)' shownames='1' showvalues='1' decimals='2' showSum='1' numberPrefix='' placeValuesInside='1' rotateValues='1'>
<categories>";
for($i=0; $i<12; $i++){
    echo "<category label='$nm_fakultas[$i]' />";
}
echo"</categories>";

echo "<dataset seriesName='A' color='66FF00' showValues='1'>";

for($i=0;$i<12;$i++){
    echo "<set value='$nilai_A[$i]' />";
}
echo "</dataset>";

echo "<dataset seriesName='AB' color='FFFF00' showValues='1'>";

for($i=0;$i<12;$i++){
    echo "<set value='$nilai_AB[$i]' />";
}
echo "</dataset>";

echo "<dataset seriesName='B' color='0000FF' showValues='1'>";
for($i=0;$i<12;$i++){
    echo"<set value='$nilai_B[$i]' />";
}
echo "</dataset>";


echo "<dataset seriesName='BC' color='FFCC00' showValues='1'>";

for($i=0;$i<12;$i++){
    echo "<set value='$nilai_BC[$i]' />";
}
echo "</dataset>";

echo "<dataset seriesName='C' color='CC9933' showValues='1'>";

for($i=0;$i<12;$i++){
    echo "<set value='$nilai_C[$i]' />";
}
echo "</dataset>";

echo "<dataset seriesName='D' color='FF3399' showValues='1'>";
for($i=0;$i<12;$i++){
    echo"<set value='$nilai_D[$i]' />";
}
echo "</dataset>";

echo "<dataset seriesName='E' color='CC3300' showValues='1'>";
for($i=0;$i<12;$i++){
    echo"<set value='$nilai_E[$i]' />";
}
echo "</dataset>";

echo "</chart>";
?>

