<?php
include '../conf.php';
	
	$db->Query("select
				SUM((select count(mi.id_mhs) from mahasiswa_ipk mi where mi.id_program_studi = PROGRAM_STUDI.id_program_studi and (ipk < 2))) ip1
				from fakultas 
				join program_studi on PROGRAM_STUDI.ID_FAKULTAS = FAKULTAS.ID_FAKULTAS
				where ID_JENJANG = 5");
	$a = $db->FetchRow();
	
	$db->Query("select FAKULTAS.ID_FAKULTAS, NM_FAKULTAS, 
				SUM((select count(mi.id_mhs) from mahasiswa_ipk mi where mi.id_program_studi = PROGRAM_STUDI.id_program_studi and (ipk < 2))) ip1
				from fakultas 
				join program_studi on PROGRAM_STUDI.ID_FAKULTAS = FAKULTAS.ID_FAKULTAS
				where ID_JENJANG = 5
				GROUP BY FAKULTAS.ID_FAKULTAS, NM_FAKULTAS
				ORDER BY FAKULTAS.ID_FAKULTAS, NM_FAKULTAS");

    header('Content-Type: text/xml');	
	 echo "<chart palette='4' decimals='0' enableSmartLabels='1' enableRotation='0' bgColor='FFFFFF,FFFFFF' bgAlpha='100,100' bgRatio='100,100' bgAngle='360' showBorder='1' startingAngle='70' caption='MAHASISWA D3 IPK &lt; 2' subcaption='Jumlah mhs $a[0]'>";
	while ($row = $db->FetchRow()){ 
			echo "<set label='$row[1]' value='$row[2]'/>";
	}

echo "</chart>";
?>
