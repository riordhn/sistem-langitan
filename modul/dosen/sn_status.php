<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

	$id_pengguna = $user->ID_PENGGUNA;
	$username = "";
	$nm_pengguna = "";
	
	$id_status = array();
	$id_pengguna_status = array();
	$status = array();
	$tgl_status = array();
	$nm_pengguna_status = array();
	$username_status = array();
	
	$id_status_balas = array();
	$id_pengguna_status_balas = array();
	$status_balas = array();
	$tgl_status_balas = array();
	$nm_pengguna_status_balas = array();
	$username_status_balas = array();
	
	$like = array();
	$comment = array();
	
	$jml_status = 0;
	$db->Query("select username, nm_pengguna from pengguna where id_pengguna = '$id_pengguna'");
	
	
	while ($row = $db->FetchRow()){
		$username = $row[0];
		$nm_pengguna = $row[1];
	}
	
	$db->Query("select sn.id_sn_status, sn.id_pengguna, sn.sn_status, sn.sn_status_tgl, p.nm_pengguna, p.username from sn_status sn
				left join pengguna p on p.id_pengguna = sn.id_pengguna
				where sn.id_sn_status = (select max(id_sn_status) from sn_status where id_pengguna = '$id_pengguna' and id_sn_comment = 0)
	");
	$i = 0;
	while ($row = $db->FetchRow()){ 
		$id_status[$i] = $row[0];
		$id_pengguna_status[$i] = $row[1];
		$status[$i] = $row[2];
		$tgl_status[$i] = $row[3];
		$nm_pengguna_status[$i] = $row[4];
		$username_status[$i] = $row[5];
		$jml_status++;
		$i++;
	}
	
	$db->Query("select sn.id_sn_comment, sn.id_pengguna, sn.sn_status, sn.sn_status_tgl, p.nm_pengguna, p.username from sn_status sn
				left join pengguna p on p.id_pengguna = sn.id_pengguna
				where sn.id_sn_comment <> 0 order by sn.id_sn_comment asc
	");
	
	$i = 0;
	while ($row = $db->FetchRow()){ 
		$id_status_balas[$i] = $row[0];
		$id_pengguna_status_balas[$i] = $row[1];
		$status_balas[$i] = $row[2];
		$tgl_status_balas[$i] = $row[3];
		$nm_pengguna_status_balas[$i] = $row[4];
		$username_status_balas[$i] = $row[5];
		$jml_status_balas++;
		$i++;
	}
	
	$id_sn_suka = array();
	$id_sn_status_suka = array();
	$nm_pengguna_suka = array();
	$jml_data_sn_status_suka = 0;

	
		
	$jml_data_sn_status_suka = 0;
	
	$db->Query("select id_sn_status from sn_suka group by id_sn_status order by id_sn_status
	");
	
	$i = 0;
	while ($row = $db->FetchRow()){ 
		$id_sn_status_suka[$i] = $row[0];
		$jml_data_sn_status_suka++;
		$i++;
	}
	
	$count_list_pengguna_suka = 0;
	$nm_pengguna_suka_1stpos = array();
	for($j=0;$j<$jml_data_sn_status_suka;$j++){
		$db->Query("select p.nm_pengguna from sn_suka s
				  left join pengguna p on p.id_pengguna = s.id_pengguna
				  where s.id_sn_status = '" . $id_sn_status_suka[$j] . "'
		");
		
		while ($row = $db->FetchRow()){ 
			
			$nm_pengguna_suka[$count_list_pengguna_suka] = $nm_pengguna_suka[$count_list_pengguna_suka] . $row[0];
			
		}
		
		$string = $nm_pengguna_suka[$count_list_pengguna_suka]; 
		$find = $nm_pengguna; 

		if(strpos($string, $find) === false){ 
		$nm_pengguna_suka_1stpos[$count_list_pengguna_suka] = '';
		}else{ 
		 $nm_pengguna_suka_1stpos[$count_list_pengguna_suka] = $nm_pengguna;
		 $nm_pengguna_suka[$count_list_pengguna_suka] = str_replace($nm_pengguna, '', $nm_pengguna_suka[$count_list_pengguna_suka]);
		}
		$count_list_pengguna_suka++;
	}
	
	//Tidak Suka
	$id_sn_tdksuka = array();
	$id_sn_status_tdksuka = array();
	$nm_pengguna_tdksuka = array();
	$jml_data_sn_status_tdksuka = 0;

	
		
	$jml_data_sn_status_tdksuka = 0;
	
	$db->Query("select id_sn_status from sn_suka where is_suka = 0 group by id_sn_status order by id_sn_status
	");
	
	$i = 0;
	while ($row = $db->FetchRow()){ 
		$id_sn_status_tdksuka[$i] = $row[0];
		$jml_data_sn_status_tdksuka++;
		$i++;
	}
	
	$count_list_pengguna_tdksuka = 0;
	$nm_pengguna_tdksuka_1stpos = array();
	for($j=0;$j<$jml_data_sn_status_tdksuka;$j++){
		$db->Query("select p.nm_pengguna from sn_suka s
				  left join pengguna p on p.id_pengguna = s.id_pengguna
				  where s.id_sn_status = '" . $id_sn_status_tdksuka[$j] . "' and s.is_suka = 0
		");
		
		while ($row = $db->FetchRow()){ 
			
			$nm_pengguna_tdksuka[$count_list_pengguna_tdksuka] = $nm_pengguna_tdksuka[$count_list_pengguna_tdksuka] . $row[0];
			
		}
		
		$string = $nm_pengguna_tdksuka[$count_list_pengguna_tdksuka]; 
		$find = $nm_pengguna; 

		if(strpos($string, $find) === false){ 
		$nm_pengguna_tdksuka_1stpos[$count_list_pengguna_tdksuka] = '';
		}else{ 
		 $nm_pengguna_tdksuka_1stpos[$count_list_pengguna_tdksuka] = $nm_pengguna;
		 $nm_pengguna_tdksuka[$count_list_pengguna_tdksuka] = str_replace($nm_pengguna, '', $nm_pengguna_tdksuka[$count_list_pengguna_tdksuka]);
		}
		$count_list_pengguna_tdksuka++;
	}
	
	//$smarty->assign('id_sn_suka', $id_sn_suka);
	$smarty->assign('nm_pengguna_suka_1stpos', $nm_pengguna_suka_1stpos);
	$smarty->assign('id_sn_status_suka', $id_sn_status_suka);
	$smarty->assign('nm_pengguna_suka', $nm_pengguna_suka);
	$smarty->assign('jml_data_sn_status_suka', $jml_data_sn_status_suka);
	
	$smarty->assign('nm_pengguna_tdksuka_1stpos', $nm_pengguna_tdksuka_1stpos);
	$smarty->assign('id_sn_status_tdksuka', $id_sn_status_tdksuka);
	$smarty->assign('nm_pengguna_tdksuka', $nm_pengguna_tdksuka);
	$smarty->assign('jml_data_sn_status_tdksuka', $jml_data_sn_status_tdksuka);	
	
	$smarty->assign('id_status', $id_status);
	$smarty->assign('id_pengguna_status', $id_pengguna_status);
	$smarty->assign('status', $status);
	$smarty->assign('tgl_status', $tgl_status);
	$smarty->assign('nm_pengguna_status', $nm_pengguna_status);
	$smarty->assign('username_status', $username_status);
	$smarty->assign('jml_status', $jml_status);
	
	$smarty->assign('id_status_balas', $id_status_balas);
	$smarty->assign('id_pengguna_status_balas', $id_pengguna_status_balas);
	$smarty->assign('status_balas', $status_balas);
	$smarty->assign('tgl_status_balas', $tgl_status_balas);
	$smarty->assign('nm_pengguna_status_balas', $nm_pengguna_status_balas);
	$smarty->assign('username_status_balas', $username_status_balas);
	$smarty->assign('jml_status_balas', $jml_status_balas);
	
	$smarty->assign('id_pengguna', $id_pengguna);
	$smarty->assign('username', $username);
	$smarty->assign('nm_pengguna', $nm_pengguna);
	
	$smarty->display('sn_status.tpl');
?>