<?php

include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}

$cari = str_replace('+', ' ', $_REQUEST['term']);
$query = "
            SELECT PS.ID_FAKULTAS,MK.ID_MATA_KULIAH,MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            FROM MATA_KULIAH MK
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=MK.ID_PROGRAM_STUDI
            JOIN JENJANG  J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN KURIKULUM_MK KUMK ON MK.ID_MATA_KULIAH=KUMK.ID_MATA_KULIAH
            JOIN KELAS_MK KMK ON KMK.ID_KURIKULUM_MK=KUMK.ID_KURIKULUM_MK
            JOIN PENGAMPU_MK PENGMK ON PENGMK.ID_KELAS_MK=KMK.ID_KELAS_MK
            WHERE UPPER(MK.NM_MATA_KULIAH) LIKE UPPER('%{$cari}%')
            GROUP BY PS.ID_FAKULTAS,MK.ID_MATA_KULIAH,MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            ORDER BY PS.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
            ";
$data_query = $db->QueryToArray("
    SELECT * FROM (
    {$query}
    ) WHERE ROWNUM<1000");
$data_hasil = array();
foreach ($data_query as $d) {
    array_push($data_hasil, array(
        'value' => strtoupper($d['NM_MATA_KULIAH']),
        'label' => "(".strtoupper($d['KD_MATA_KULIAH']) . ") " . strtoupper($d['NM_MATA_KULIAH'])." (" . strtoupper($d['NM_JENJANG'])." ". strtoupper($d['NM_PROGRAM_STUDI']).")",
        'id' => (int) $d['ID_MATA_KULIAH']
    ));
}
echo json_encode($data_hasil)
?>
