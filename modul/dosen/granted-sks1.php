<?php
include('conf.php');

$kd_fak= $user->ID_FAKULTAS;
$id_pengguna= $user->ID_PENGGUNA; 

$smarty->assign('T_THNSMT', $db->QueryToArray("select id_semester as id_semester, tahun_ajaran||' - '||nm_semester as thn_smt from semester where status_aktif_semester='True'"));

$nim = $_REQUEST['nim'];
$smarty->assign('nim', $nim);


if(($_POST['action'])=='proses'){
	
	$ip = $_SERVER['REMOTE_ADDR'];
	$id_semester =$_POST['kdthnsmt'];
	$ttl_sks =$_POST['sks'];
	$catatan =$_POST['catatan'];
	
	if ($catatan=='' or $catatan==null)
	{
		echo '<script>alert("Proses Gagal, Catatan harus DI-ISI..")</script>';
	} else
	{
	if ($ttl_sks<=24)
	{
	//echo "insert into log_granted_sks (id_pengguna,id_mhs,id_semester,ip_address,sks_max,catatan)
	//	  select '$id_pengguna',id_mhs,$id_semester,'$ip',$ttl_sks,'$catatan' from mahasiswa where nim_mhs='$nim' and id_program_studi in
	//	  (select id_program_studi from program_studi where id_fakultas=$kd_fak)";
	$db->Query("insert into log_granted_sks (id_pengguna,id_mhs,id_semester,ip_address,sks_max,catatan)
		  select '$id_pengguna',id_mhs,$id_semester,'$ip',$ttl_sks,'$catatan' from mahasiswa where nim_mhs='$nim' and id_program_studi in
		  (select id_program_studi from program_studi where id_fakultas=$kd_fak)");
	} else
	{
		echo '<script>alert("Proses Gagal, SKS Maksimum 24..")</script>';
	}
	}
	
	
}


$smarty->assign('detail_transfer', $db->QueryToArray("
                		select id_log_granted_sks,nim_mhs,nm_pengguna,nm_jenjang||'-'||nm_program_studi as prodi,
				sks_max,tgl_granted,catatan
				from log_granted_sks lgs
				join mahasiswa m on lgs.id_mhs=m.id_mhs
				join pengguna p on m.id_pengguna=p.id_pengguna
				join program_studi ps on m.id_program_studi=ps.id_program_studi
				join jenjang j on ps.id_jenjang=j.id_jenjang
				where ps.id_fakultas=$kd_fak
				order by tgl_granted desc"));

$smarty->display("sample/penilaian/granted-sks.tpl");
?>