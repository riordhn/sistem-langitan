<?php
include 'config.php';
include '../ppmb/class/snmptn.class.php';
include '../ppmb/class/PHPExcel.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$mode = get('mode');

if ($mode == 'download')
{
    // Create new PHPExcel object
    $excel = new PHPExcel();

    // Set properties
    $excel->getProperties()->setCreator("Universitas Airlangga Cyber Campus");
    $excel->getProperties()->setLastModifiedBy("Universitas Airlangga Cyber Campus");
    $excel->getProperties()->setTitle("");
    $excel->getProperties()->setSubject("");
    
    // create sheet
    $sheet = $excel->getActiveSheet();
    $sheet->setTitle('SNMPTN 2012');

    // insert header
    $sheet->setCellValue('A1', 'No Ujian');
    $sheet->setCellValue('B1', 'Nama Peserta');
    $sheet->setCellValue('C1', 'Skor');
    $sheet->setCellValue('D1', 'Sekolah');
    $sheet->setCellValue('E1', 'Program Studi');
    $sheet->setCellValue('F1', 'Kode Prodi');
    
    $snmptn = new SnmptnClass($db);
    $rows = $snmptn->GetListPesertaPutaran2All(44);
    
    for ($i = 0; $i < count($rows); $i++)
    {
        $row = &$rows[$i];
        
        $sheet->setCellValue('A'.($i+2), $row['NO_UJIAN']);
        $sheet->setCellValue('B'.($i+2), $row['NM_C_MHS']);
        $sheet->setCellValue('C'.($i+2), $row['NILAI_TOTAL']);
        $sheet->setCellValue('D'.($i+2), $row['NM_SEKOLAH']);
        $sheet->setCellValue('E'.($i+2), $row['PS1']);
        $sheet->setCellValue('F'.($i+2), $row['KODE_SNMPTN']);
    }
    
    header('Content-Type: application/vnd.ms-excel');
    header("Content-Disposition: attachment;filename=\"SNMPTN.xls\"");
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
    $objWriter->save('php://output');
        
    exit();
}
?>
<div class="center_title_bar">Rekap Peserta Diterima</div>

<a href="dashboard-rsnmptn.php?mode=download" class="disable-ajax">Download File</a>