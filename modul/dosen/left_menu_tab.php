<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

    //mendapatkan lokasi halaman
	$location = explode('-', $_GET['page']);

    //meload struktur menu dari modul user (session)
	foreach ($user->MODULs as $m)
	{
		if ($m['NM_MODUL'] == $location[0])
		{
			$smarty->assign('nm_modul', $m['NM_MODUL']);
			$smarty->assign('menu_set', $m['MENUs']);
		}
	}
	
	$smarty->assign('foto_dosen', $base_url."foto_pegawai/{$user->USERNAME}.JPG");
	
	/**
	foreach ($xml_menu->menu as $menu) {
		//jika tab menu yang diinginkan ditemukan
		if ($menu->id == $location[0]) {
			$data_menu = array();
			array_push($data_menu, array(
				'menu_id' => $menu->id,
				'menu_image' => $menu->image
			));
			$data_tab = array();

			foreach ($menu->tab as $tab) {
				array_push($data_tab, array(
					'tab_id' => $tab->id,
					'tab_title' => $tab->title,
					'tab_page' => $tab->page
				));
				//deklarasi array untuk menu tab
			}
			$smarty->assign('tabs', $data_tab);
			$smarty->assign('menus', $data_menu);
			break;
		}
	}
	*/
// menampilkan tag left menu tab
$smarty->display('left_menu_tab.tpl');
?>