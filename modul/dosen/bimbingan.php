<?php

include 'conf.php';
include 'proses/bimbingan.class.php';

$b = new bimbingan($db, $user->ID_PENGGUNA, $user->ID_DOSEN);

if (isset($_GET)) {
    if (get('mode') == 'detail-bimbingan') {
        $smarty->assign('tugas_akhir', $b->GetDetailTugasAkhirMhs(get('id')));
    } else if (get('mode') == 'update-progress') {
        $id_ta = post('id_ta');
        $p_value = post('p_value');
        $db->Query("UPDATE TUGAS_AKHIR SET PROGRESS='{$p_value}' WHERE ID_TUGAS_AKHIR='{$id_ta}'");
        exit();
    } else {
        $data_bimbingan = $b->LoadMahasiswaBimbingan();
        $data_penguji = $b->LoadMahasiswaPenguji();
        $smarty->assign('data_bimbingan', $data_bimbingan);
        $smarty->assign('data_penguji', $data_penguji);
    }
}

$smarty->display('display-bimbingan/bimbingan.tpl');
?>