<?php

include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}

$cari = str_replace('+', ' ', $_REQUEST['term']);
$data_pengguna = $db->QueryToArray("
    SELECT * FROM (
        SELECT S.*,K.NM_KOTA,P.NM_PROVINSI,N.NM_NEGARA 
        FROM AUCC.SEKOLAH S
        JOIN AUCC.KOTA K ON K.ID_KOTA=S.ID_KOTA
        JOIN AUCC.PROVINSI P ON P.ID_PROVINSI=K.ID_PROVINSI
        JOIN AUCC.NEGARA N ON P.ID_NEGARA=N.ID_NEGARA
        WHERE UPPER(S.NM_SEKOLAH) LIKE UPPER('%{$cari}%')
    ) WHERE ROWNUM<20");
$data_hasil = array();
foreach ($data_pengguna as $d) {
    array_push($data_hasil, array(
        'value' => strtoupper($d['NM_SEKOLAH']),
        'label' => strtoupper($d['NM_SEKOLAH']) . ", " . ucwords(strtolower($d['NM_KOTA'])) . ", " . ucwords(strtolower($d['NM_PROVINSI'])).", ".  ucwords(strtolower($d['NM_NEGARA'])) ,
        'id' => (int) $d['ID_SEKOLAH']
    ));
}
echo json_encode($data_hasil)
?>
