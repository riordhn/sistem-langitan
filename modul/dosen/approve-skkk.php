<?php
/*echo "Fitur Dalam Perbaikan";
exit();
*/

include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) {
    exit();
}

require_once 'proses/perwalian.class.php';

$perwalian = new perwalian($db, $user->ID_PENGGUNA, $login->id_dosen);
$mode = 'approve-skkk';


if (isset($_POST)) {
	if ($_POST['mode'] == 'verifikasi') {
		$id_krp_khp = $_POST['id_krp_khp'];

		$db->Query("SELECT BOBOT_KEGIATAN_2 
						FROM KEGIATAN_2 
						WHERE ID_KEGIATAN_2 = (SELECT ID_KEGIATAN_2 FROM KRP_KHP WHERE ID_KRP_KHP = '{$id_krp_khp}')");
        $bobot = $db->FetchAssoc();
        $bobot_kegiatan = $bobot['BOBOT_KEGIATAN_2'];

		$result = $db->Query("UPDATE FILE_KRP_KHP SET IS_VERIFIED = 1 WHERE ID_KRP_KHP = '{$id_krp_khp}'");
		$result = $db->Query("UPDATE KRP_KHP SET SKOR_KRP_KHP = SKOR_KRP_KHP+{$bobot_kegiatan} WHERE ID_KRP_KHP = '{$id_krp_khp}'");


        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
	}
	if ($_POST['mode'] == 'not-verifikasi') {
		$id_krp_khp = $_POST['id_krp_khp'];

		$db->Query("SELECT BOBOT_KEGIATAN_2 
						FROM KEGIATAN_2 
						WHERE ID_KEGIATAN_2 = (SELECT ID_KEGIATAN_2 FROM KRP_KHP WHERE ID_KRP_KHP = '{$id_krp_khp}')");
        $bobot = $db->FetchAssoc();
        $bobot_kegiatan = $bobot['BOBOT_KEGIATAN_2'];

		$result = $db->Query("UPDATE FILE_KRP_KHP SET IS_VERIFIED = 0 WHERE ID_KRP_KHP = '{$id_krp_khp}'");
		$result = $db->Query("UPDATE KRP_KHP SET SKOR_KRP_KHP = SKOR_KRP_KHP-{$bobot_kegiatan} WHERE ID_KRP_KHP = '{$id_krp_khp}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
	}
}




if (isset($_GET)) {
    if (get('mode') == 'lihat-skkk') {
        $smarty->assign('data_mahasiswa', $perwalian->load_mhs_status(get('id_mhs')));
        $smarty->assign('data_krp', $perwalian->load_krp(get('id_mhs')));
    }
    if (get('mode') == 'lihat-file') {
        $smarty->assign('data_mahasiswa', $perwalian->load_mhs_status(get('id_mhs')));
        $smarty->assign('data_file_krp', $perwalian->load_file_krp(get('id_mhs'),get('id_krp_khp')));
    }
}

/*$smarty->assign('keterangan_perwalian',alert_success('Data yang ditampilkan adalah mahasiswa yang sudah melakukan KRS, untuk melihat semua mahasiswa bimbingan silahkan pilih menu <b>Perwalian Akademik</b> di sebelah kiri. Terima Kasih',100));*/
$smarty->assign('data_mahasiswa_krp', $perwalian->load_mahasiswa_krp());
$smarty->assign('id_fakultas', $login->id_fakultas);
// echo $mode; exit();
$smarty->display('display-bimbingan/' . $mode . '.tpl');