<?php
include 'config.php';
require_once "../../includes/FusionCharts.php";

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$id_fakultas = $user->ID_FAKULTAS;
?>

<div class="center_title_bar">REGISTRASI MAHASISWA</div>
 <div id="chartdiv" align="center"> 
        FusionCharts. 
 </div>

       <?php
        if($id_fakultas=='8'){
      ?>
      <script type="text/javascript">
		   var chart = new FusionCharts("../../../swf/Charts/MSColumn3D.swf", "chartdiv", "800", "400", "0", "0");
		   chart.setDataURL($base_url."modul/dosen/xml/registrasi.php");		   
		   chart.render("chartdiv");
      </script>
      <?php
        }
        else{
      ?>
      <script type="text/javascript">
		   var chart = new FusionCharts("../../swf/Charts/MSColumn3D.swf", "chartdiv", "800", "400", "0", "0");
		   chart.setDataURL($base_url."modul/dosen/xml/registrasi.php");		   
		   chart.render("chartdiv");
      </script>
      <?php
        }
      ?>
