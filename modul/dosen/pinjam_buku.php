<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$smarty->assign('TITLE', 'Peminjaman Buku');

$smarty->display('sample/pustaka/pinjam_buku.tpl');
?>