<?php
include 'config.php';
include 'proses/kegiatan_profesional.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$kegiatan = new kegiatan_profesional($db, $login->id_dosen);

$mode = get('mode','view');
if ($request_method == 'POST')
{
	if (post('mode') == 'add')
	{
		$kegiatan->insert($login->id_dosen, $_POST['kegiatan'], $_POST['peran'], $_POST['instansi'], $_POST['tahun'], $_POST['tingkat']);
	}
	else if(post('mode')=='edit')
	{	
		$kegiatan->update($_POST['id_kegiatan'], $_POST['kegiatan'], $_POST['peran'], $_POST['instansi'], $_POST['tahun'], $_POST['tingkat']);
	}

}
if($mode=='detail'||$mode=='edit'||$mode=='upload'||$mode=='delete'){
	$smarty->assign('data_kegiatan',$kegiatan->get(get('id_kegiatan')));
}else{
	$smarty->assign('data_kegiatan', $kegiatan->load_kegiatan());
}

$smarty->display("sample/biodata/{$mode}_kegiatan_profesional.tpl");
?>