<?php
include 'config.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$id_pengguna = $user->ID_PENGGUNA;

$cek_pimpinan_fakultas = $db->QuerySingle("
        SELECT COUNT(*) 
        FROM FAKULTAS 
        WHERE ID_FAKULTAS='{$user->ID_FAKULTAS}' AND (ID_DEKAN='{$id_pengguna}' OR ID_WADEK1='{$id_pengguna}' OR ID_WADEK2='{$id_pengguna}' OR ID_WADEK3='{$id_pengguna}')");

if ($cek_pimpinan_fakultas > 0) {
    $f_fakultas = "AND PS.ID_FAKULTAS='{$user->ID_FAKULTAS}'";
} else {
    $f_fakultas = "";
}

$cari = str_replace('+', ' ', $_REQUEST['term']);
$data_pengguna = $db->QueryToArray("
        SELECT * FROM (
    SELECT ID_MHS,NM_PENGGUNA,NIM_MHS,NM_PROGRAM_STUDI,NM_JENJANG
    FROM PENGGUNA P
    JOIN MAHASISWA M ON P.ID_PENGGUNA =M.ID_PENGGUNA
    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI {$f_fakultas}
    JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
    WHERE NIM_MHS LIKE '%{$cari}%' OR UPPER(NM_PENGGUNA) LIKE UPPER('%{$cari}%')
    ) WHERE ROWNUM<20");
$data_hasil = array();
foreach ($data_pengguna as $data) {
    array_push($data_hasil, array(
        'value' => $data['NIM_MHS'] . " " . ucwords($data['NM_PENGGUNA']) . " (" . $data['NM_JENJANG'] . ") " . $data['NM_PROGRAM_STUDI'],
        'id' => (int) $data['ID_MHS']
    ));
}
echo json_encode($data_hasil)
?>
