<?php
include 'config.php';
include 'proses/gbpp.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$gbpp = new gbpp($db, $login->id_dosen);
$mode = get('mode', 'view');

$data_mata_kuliah = $gbpp->load_mata_kuliah();

if ($request_method == 'POST') {
    if (post('mode') == 'edit_deskripsi_mk') {
        if (count($gbpp->cek_deskripsi_mk(post('id_kurikulum_mk'))) > 0) {
            $gbpp->update_deskripsi_mk(post('id_kurikulum_mk'), post('deskripsi'), post('tiu'));
        } else {
            $gbpp->insert_deskripsi_mk(post('id_kurikulum_mk'), post('deskripsi'), post('tiu'));
        }
    } else if (post('mode') == 'tambah_referensi') {
        $gbpp->insert_referensi_mk(post('id_kurikulum_mk'), post('referensi'));
    } else if (post('mode') == 'edit_referensi') {
        $gbpp->update_referensi_mk(post('id_referensi_mk'), post('referensi'));
    } else if (post('mode') == 'delete_referensi') {
        $gbpp->delete_referensi_mk(post('id_referensi_mk'));
    } else if (post('mode') == 'tambah') {
        $gbpp->insert_gbpp_mk(post('id_kurikulum_mk'), post('urutan'), post('tik'), post('pokok'), post('sub_pokok'));
    } else if (post('mode') == 'edit') {
        $gbpp->update_gbpp_mk(post('id_gbpp_mk'), post('urutan'), post('tik'), post('pokok'), post('sub_pokok'));
    } else if (post('mode') == 'delete') {
        $gbpp->delete_gbpp_mk(post('id_gbpp_mk'));
    }
}
if ($mode == 'edit_deskripsi_mk' || $mode == 'tambah_referensi' || $mode == 'edit_referensi' || $mode == 'delete_referensi' || $mode == 'tambah' || $mode == 'edit' || $mode == 'delete') {
    $smarty->assign('data_deskripsi_mk', $gbpp->get_deskripsi_mk(get('id_kurikulum_mk')));
    $smarty->assign('data_prasyarat_mk', $gbpp->load_prasyarat_mk(get('id_kurikulum_mk')));
    $smarty->assign('data_referensi_mk', $gbpp->get_referensi_mk(get('id_referensi_mk')));
    $smarty->assign('data_gbpp_mk', $gbpp->get_gbpp_mk(get('id_gbpp_mk')));
} else {
    if (get('id_kurikulum_mk') != '') {
        $smarty->assign('data_deskripsi_mk', $gbpp->get_deskripsi_mk(get('id_kurikulum_mk')));
        $smarty->assign('data_referensi_mk', $gbpp->load_referensi_mk(get('id_kurikulum_mk')));
        $smarty->assign('data_gbpp_mk', $gbpp->load_gbpp_mk(get('id_kurikulum_mk')));
        $smarty->assign('data_prasyarat_mk', $gbpp->load_prasyarat_mk(get('id_kurikulum_mk')));
    } else {
        $smarty->assign('data_deskripsi_mk', $gbpp->get_deskripsi_mk($data_mata_kuliah[0]['ID_KURIKULUM_MK']));
        $smarty->assign('data_referensi_mk', $gbpp->load_referensi_mk($data_mata_kuliah[0]['ID_KURIKULUM_MK']));
        $smarty->assign('data_gbpp_mk', $gbpp->load_gbpp_mk($data_mata_kuliah[0]['ID_KURIKULUM_MK']));
        $smarty->assign('data_prasyarat_mk', $gbpp->load_prasyarat_mk($data_mata_kuliah[0]['ID_KURIKULUM_MK']));
    }
}

$smarty->assign('id_kurikulum_mk', get('id_kurikulum_mk'));
$smarty->assign('data_mata_kuliah', $gbpp->load_mata_kuliah());
$smarty->display("sample/gbpp/{$mode}_gbpp.tpl");
?>