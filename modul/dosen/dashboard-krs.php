<?php
include 'config.php';
require_once "../../includes/FusionCharts.php";

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$id_fakultas = $user->ID_FAKULTAS;
?>

<div class="center_title_bar">KRS MAHASISWA</div>
 <div id="chartdiv" align="center"> 
        FusionCharts. 
 </div>
 
 <div id="chartdiv2" align="center"> 
        FusionCharts. 
 </div>
      
      

      <script type="text/javascript">
		   var chart = new FusionCharts("../../swf/Charts/MSColumn3D.swf", "ChartId", "800", "400", "0", "0");
		   chart.setDataURL($base_url."modul/dosen/xml/krs.php");		   
		   chart.render("chartdiv");
      </script>
      
      <script type="text/javascript">
		   var chart = new FusionCharts("../../swf/Charts/MSColumn3D.swf", "ChartId", "800", "400", "0", "0");
		   chart.setDataURL($base_url."modul/dosen/xml/krs_jenjang.php");		   
		   chart.render("chartdiv2");
      </script>

