<?php
include 'config.php';
include 'proses/ujian.class.php';

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$materi_ujian = new ujian($db, $login->id_dosen);


if (isset($_POST['mode'])) {
    if ($_POST['mode'] == 'add') {
        $smarty->assign('data_mata_kuliah', $materi_ujian->load_mata_kuliah());
        $smarty->assign('title', 'Add Materi Ujian');
        $smarty->display('display-ujian/add_materi_ujian.tpl');
    } else if ($_POST['mode'] == 'save') {
        $materi_ujian->insert_materi_ujian($_POST['mata_kuliah'], $_POST['jenis'], $_POST['sifat'], $_POST['materi'], $_POST['keterangan']);
        $materi_ujian->load_materi_ujian();
        $smarty->assign('data_materi_ujian', $materi_ujian->data_materi_ujian);
        $smarty->assign('title', 'Materi Ujian');
        $smarty->display('display-ujian/materi_ujian.tpl');
    }
} else {
    $materi_ujian->load_materi_ujian();
    $smarty->assign('data_materi_ujian', $materi_ujian->data_materi_ujian);
    $smarty->assign('title', 'Materi Ujian');
    $smarty->display('display-jadwal/materi_ujian.tpl');
}
?>