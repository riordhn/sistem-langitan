<?php
include 'config.php';

$id_pt = $id_pt_user;

// Cegah non user akses langsung by Fathoni
if ($user->Role() != AUCC_ROLE_DOSEN) { exit(); }

$fakultas_set = $db->QueryToArray("select id_fakultas, nm_fakultas from fakultas where id_perguruan_tinggi = {$id_pt}");

foreach ($fakultas_set as &$f)
	$f['program_studi_set'] = $db->QueryToArray("select id_program_studi, id_jenjang, nm_program_studi from program_studi where id_fakultas = {$f['ID_FAKULTAS']} and status_aktif_prodi = 1");
		
$jenjang_set = $db->QueryToArray("select id_jenjang, nm_jenjang from jenjang j where id_jenjang in (1, 2, 3, 4, 5, 9, 10)");

$data_set = $db->QueryToArray("
	select ps.id_program_studi, ps.id_jenjang, ps.nm_program_studi,
	  (select count(id_mhs) from mahasiswa m 
	  	join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs	
	  	where m.id_program_studi = ps.id_program_studi and aktif_status_pengguna = 1 and thn_angkatan_mhs in 
			(select thn_akademik_semester from semester where status_aktif_semester = 'True')
	  ) t2011,
	  (select count(id_mhs) from mahasiswa m 
	  	join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
	  	where m.id_program_studi = ps.id_program_studi and aktif_status_pengguna = 1 and thn_angkatan_mhs not in 
	  	(select thn_akademik_semester from semester where status_aktif_semester = 'True')
	  ) tlain
	from program_studi ps
	order by ps.id_fakultas, ps.nm_program_studi");

$smarty->assign('fakultas_set', $fakultas_set);
$smarty->assign('jenjang_set', $jenjang_set);
$smarty->assign('data_set', $data_set);

$smarty->display("sample/rekap/rekap-mhsaktif.tpl");
?>