<div class="center_title_bar">Master Kota</div>

<table>
	<tr>
		<td>Nama Kota / Provinsi</td>
		<td>
			<form action="kota.php" method="get">
			<input type="search" size="50" name="q" value="{$smarty.get.q}" />
			<input type="submit" value="Cari..." />
			<a href="kota.php?mode=add">Tambah</a>
			</form>
		</td>
	</tr>
</table>

{if $result}<h2>{$result}</h2>{/if}

{if !empty($kota_set)}
<table>
	<tr>
		<th>Provinsi</th>
		<th>Kota</th>
	</tr>
	{foreach $kota_set as $k}
	<tr>
		<td>{$k.NM_PROVINSI}</td>
		<td>{$k.NM_KOTA}</td>
	</tr>
	{/foreach}
</table>
{/if}