<div class="center_title_bar">Status Pengguna</div>
<form action="status-pengguna.php" method="get">
    <table>
        <tr>
            <td>USERNAME</td>
            <td><input type="text" name="username" size="30"/></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if isset($result_message)}
	<h3>{$result_message}</h3>
{/if}
{if isset($pengguna)}
    <form action="status-pengguna.php?username={$smarty.get.username}" method="post">
        <table>
            <tr>
                <th colspan="2">DETAIL PENGGUNA</th>
            </tr>
			<tr>
                <td>Nama</td>
                <td>{$pengguna.NM_PENGGUNA}</td>
            </tr>
            <tr>
                <td>Role</td>
                <td>{$pengguna.NM_ROLE}</td>
            </tr>
            <tr>
                <td>Username</td>
                <td>{$pengguna.USERNAME}</td>
            </tr>
			<tr>
                <td>Email</td>
                <td>{$pengguna.EMAIL_PENGGUNA}</td>
            </tr>
			<tr>
				<td>Terakhir Login</td>
				<td>{$pengguna.LAST_TIME_LOGIN}</td>
			</tr>
			<tr>
				<td>Terakhir Ganti Password</td>
				<td>{$pengguna.LAST_TIME_PASSWORD}</td>
			</tr>
			<tr>
				<td>Status Password</td>
				<td>{if $pengguna.PASSWORD_HASH_TEMP != ''}Password Sementara{else}-{/if}</td>
			</tr>
			<tr>
				<td>Password Sementara</td>
				<td><input type="text" name="password_temp"/>
				</td>
			</tr>
			<tr>
				<td>Ganti Password Next Login</td>
				<td><input type="checkbox" name="must_change" {if $pengguna.PASSWORD_MUST_CHANGE == 1}checked="checked"{/if}/></td>
			</tr>
            <tr>
                <td colspan="2" class="center">
                    <input type="hidden" name="mode" value="ubah"/>
                    <input type="hidden" name="id_pengguna" value="{$pengguna.ID_PENGGUNA}"/>
                    <input type="submit" value="Reset Password"/>
                </td>
            </tr>
        </table>
		<p>NB :<br/>
		- Untuk mereset password saja, kosongi PASSWORD SEMENTARA dan centang GANTI PASSWORD NEXT LOGIN.<br/>
		- Untuk melakukan login ke user untuk melakukan pengecekan saja, isi PASSWORD SEMENTARA saja.<br/>
		- Untuk menghilangkan password sementara, cukup kosongi PASSWORD SEMETNARA saja.
		</p>
    </form>
{/if}