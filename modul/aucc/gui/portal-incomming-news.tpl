<script>

	$(function() {
		$( "#tabs" ).tabs({
			ajaxOptions: {
				error: function( xhr, status, index, anchor ) {
					$( anchor.hash ).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo." );
				}
			}
		});
	});

</script>

<script>
	$(function() {
	
		{for $foo=0 to $jml_data_news-1}
			$("#btnEdit{$id_news[$foo]}").button({
				icons: {
					primary: "ui-icon-pencil",
				},
				text: true
			});
			$("#btnApprove{$id_news[$foo]}").button({
				icons: {
					primary: "ui-icon-check",
				},
				text: true
			});
			$("#btnDelete{$id_news[$foo]}").button({
				icons: {
					primary: "ui-icon-trash",
				},
				text: true
			});
			$("#btnSubmit{$id_news[$foo]}").button({
				icons: {
					primary: "ui-icon-check",
				},
				text: true
			});
			$("#btnCancel{$id_news[$foo]}").button({
				icons: {
					primary: "ui-icon-cancel",
				},
				text: true
			});
			
			
	
			//Event Submit ( Edit )
			$('#MyForm{$id_news[$foo]}').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {
					
						$('#link_judul_news_div{$id_news[$foo]}').show();
						$('#isi_news_div{$id_news[$foo]}').show();
						$('#image_link_news_div{$id_news[$foo]}').show();
						$('#resume_news_div{$id_news[$foo]}').show();
						
						$('#judul_news{$id_news[$foo]}').hide();
						$('#resume_news{$id_news[$foo]}').hide();
						$('#isi_news{$id_news[$foo]}').remove();
						$('#image_link_news{$id_news[$foo]}').hide();
						
						tinyMCE.execCommand('mceRemoveControl',false,'isi_news{$id_news[$foo]}'); //removing tiny_mce 
					
						$('#btnEdit{$id_news[$foo]}').show();
					
						$('#btnSubmit{$id_news[$foo]}').hide();
						$('#btnCancel{$id_news[$foo]}').hide();	
						
						var link_judul = document.getElementById('link_judul_news_div{$id_news[$foo]}');
						var image_link = document.getElementById('image_link_news_div{$id_news[$foo]}');
						var resume_news = document.getElementById('resume_news_div{$id_news[$foo]}');
						var isi_news = document.getElementById('isi_news_div{$id_news[$foo]}');
						
						link_judul.innerHTML = document.getElementById('judul_news{$id_news[$foo]}').value;
						image_link.innerHTML = '<img id="image_link_news_div{$id_news[$foo]}" style="width:200px; height:150px;" src="' + document.getElementById('image_link_news{$id_news[$foo]}').value + '"/>';
						resume_news.innerHTML = document.getElementById('resume_news{$id_news[$foo]}').value;
						isi_news.innerHTML = document.getElementById('get_isi{$id_news[$foo]}').value;
						
						//window.location.href = "{$base_url}modul/aucc/#portal-news!portal-incomming-news.php?id={$id_news[$foo]}&view=all";

					}
				})
				return false;
			});
			
			
			//Submit Approve
			$('#MyFormApprove{$id_news[$foo]}').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {
						alert('Berhasil DiApprove');
						window.location.href = "{$base_url}modul/aucc/#portal-news!portal-incomming-news.php?";
					}
				})
				return false;
			});
			
			//Submit Delete
			$('#MyFormDelete{$id_news[$foo]}').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {
						alert('Berhasil Didelete');
						window.location.href = "{$base_url}modul/aucc/#portal-news!portal-incomming-news.php?";
					}
				})
				return false;
			});
			
			/////////////////////////////////////  End Of Event Submit ///////////////////////////////////////////////
			
			//Event Click
			$('#btnEdit{$id_news[$foo]}').click(function(){		
				
					$('#link_judul_news_div{$id_news[$foo]}').hide();
					$('#isi_news_div{$id_news[$foo]}').hide();
					$('#image_link_news_div{$id_news[$foo]}').hide();
					$('#resume_news_div{$id_news[$foo]}').hide();
					
					$('#judul_news{$id_news[$foo]}').show();
					$('#resume_news{$id_news[$foo]}').show();
					$('#image_link_news{$id_news[$foo]}').show();
					
					//$('.isi').prepend('<TEXTAREA class="mceEditor" id="isi_news{$id_news[$foo]}" onclick="javascript:alert("hello")" style="width:100%; min-height:700px;" NAME="isi_news{$id_news[$foo]}" COLS=30 ROWS=6>{$isi_news[$foo]}</TEXTAREA>');
					
					setup(); // Call TinyMCE 
				
					$('#btnEdit{$id_news[$foo]}').hide();
				
					$('#btnSubmit{$id_news[$foo]}').show();
					$('#btnCancel{$id_news[$foo]}').show();
				
			});
			
			$('#btnCancel{$id_news[$foo]}').click(function(){		
					
					$('#link_judul_news_div{$id_news[$foo]}').show();
					$('#isi_news_div{$id_news[$foo]}').show();
					$('#image_link_news_div{$id_news[$foo]}').show();
					$('#resume_news_div{$id_news[$foo]}').show();
					
					$('#judul_news{$id_news[$foo]}').hide();
					$('#resume_news{$id_news[$foo]}').hide();
					$('#isi_news{$id_news[$foo]}').remove();
					$('#image_link_news{$id_news[$foo]}').hide();
					
					tinyMCE.execCommand('mceRemoveControl',false,'isi_news{$id_news[$foo]}'); //removing tiny_mce 
				
					$('#btnEdit{$id_news[$foo]}').show();
				
					$('#btnSubmit{$id_news[$foo]}').hide();
					$('#btnCancel{$id_news[$foo]}').hide();	
				
			});
			

			//When Button Submit is Clicking
			$('#btnSubmit{$id_news[$foo]}').click(function(){	
				//$("#isi_news{$id_news[$foo]}").clone().prependTo("#get_isi{$id_news[$foo]}");
				//$('#MyForm{$id_news[$foo]}').submit();
				var ed = tinyMCE.get('isi_news{$id_news[$foo]}');
				var isi = ed.getContent();
				$("#get_isi{$id_news[$foo]}").val(isi);
				$('#MyForm{$id_news[$foo]}').submit();
				//alert(ed.getContent());
			});			
			
			//When Button Approve is Clicking
			$('#btnApprove{$id_news[$foo]}').click(function(){		
				$('#MyFormApprove{$id_news[$foo]}').submit();
				//alert('hello');
			});
			
			//When Button Delete Is Clicking 
			$('#btnDelete{$id_news[$foo]}').click(function(){		
				$('#MyFormDelete{$id_news[$foo]}').submit();
				//alert('hello');
			});
			
			
			/////////////////////////////////////////////////// End Of Event Click ///////////////////////////
			
			
			
		{/for}
	
	});
</script>

<script type="text/javascript">
	function unSetup() {
		//tinyMCE.execCommand('mceRemoveControl',false,'isi_news{$id_news[$foo]}');
	}
</script>

<script type="text/javascript">
	function setup() {
		tinyMCE.init({
				mode : "textareas",
				theme : "advanced",
				editor_selector : "mceEditor",
				editor_deselector : "mceNoEditor",
				plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

				// Theme options
				theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
				theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
				theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
				theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,

				// Skin options
				skin : "o2k7",
				skin_variant : "silver",

				// Example content CSS (should be your site CSS)
				content_css : "../../css/south-street/jquery-ui-1.8.16.custom.css"
		});
	}
</script>

<div id="tabs">
			Pages : <a href="#portal-news!portal-news.php">ALL news</a>
			<br /><br />
			<div class="column-news">	
				<div class="portlet-news">
					<section>

					<div id="news-content" class="portlet-content-news ui-widget-content" style="background:#fff;">
						<div style="margin:20px;">
							{for $foo=0 to $jml_data_news-1}
									
									<form  style="display:none;" id="MyFormApprove{$id_news[$foo]}" name="MyFormApprove{$id_news[$foo]}" method="post" action="portal-app-news.php">										
										<input style="display:none;" type="text" name="id_news" value="{$id_news[$foo]}" >
									</form>
									<form  style="display:none;" id="MyFormDelete{$id_news[$foo]}" name="MyFormDelete{$id_news[$foo]}" method="post" action="portal-del-news.php">								
										<input style="display:none;" type="text" name="id_news" value="{$id_news[$foo]}" >
									</form>
									
									<form  id="MyForm{$id_news[$foo]}" name="MyForm{$id_news[$foo]}" method="post" action="portal-news-edit-update.php">
										
										{if $id!=""}
											<input value="0" type="text" id="get_isi{$id_news[$foo]}" name="get_isi{$id_news[$foo]}" style="display:none;">
											<div style="float:left;" id="btnEdit{$id_news[$foo]}" name="btnEdit{$id_news[$foo]}" style="height:30px; width:80px;" title="Edit news">
												Edit
											</div>
											<div  id="btnSubmit{$id_news[$foo]}" name="btnSubmit{$id_news[$foo]}" style="float:left; display:none;" title="Submit">
												Submit
											</div>
											<div  id="btnCancel{$id_news[$foo]}" name="btnCancel{$id_news[$foo]}" style="float:left; display:none;" title="Cancel">
												Cancel
											</div>
											<div style="float:left;" id="btnApprove{$id_news[$foo]}" name="btnApprove{$id_news[$foo]}" style="height:30px; width:80px;" title="Approve News">
												Approve
											</div>
											<div style="float:left;" id="btnDelete{$id_news[$foo]}" name="btnDelete{$id_news[$foo]}" style="height:30px; width:80px;" title="Delete News">
												Delete
											</div>

										{/if}
										<news id="news{$foo}">
										<input style="display:none;" type="text" name="id_news" value="{$id_news[$foo]}" >
										<div>
											&nbsp;
										</div>
										<br />
										<div class="title">
											Judul : <a class="disable-ajax" id="link_judul_news_div{$id_news[$foo]}" href="#portal-news!portal-incomming-news.php?id={$id_news[$foo]}&view=all">{$judul_news[$foo]}</a>
											<textarea style="width:100%; height:30px;display:none;" COLS=30 ROWS=3   id="judul_news{$id_news[$foo]}" name="judul_news{$id_news[$foo]}" >{$judul_news[$foo]}</textarea>
										</div>
										<div class="date">
											Oleh : &nbsp; {$pengguna[$foo]}
											<br />
											Tanggal : &nbsp; {$tgl_news[$foo]}
										</div>
										<div class="content_news">
											{if $view == "all"}
												
												<legend>Image Link</legend>
												<div style="width:100%;" id="image_link_news_div{$id_news[$foo]}">
													<img id="image_link_news_div{$id_news[$foo]}" style="width:200px; height:150px;" src="{$img_link[$foo]}"/>
												</div>
												<br />
												<TEXTAREA id="image_link_news{$id_news[$foo]}" style="width:100%; height:30px;display:none;" NAME="image_link_news{$id_news[$foo]}" COLS=30 ROWS=6>{$img_link[$foo]}</TEXTAREA>
												
												<legend>Resume</legend>
												<div style="width:100%;" id="resume_news_div{$id_news[$foo]}">
													{$resume_news[$foo]}
												</div>
												<br />
												<TEXTAREA id="resume_news{$id_news[$foo]}" style="display:none; width:100%; min-height:150px;" NAME="resume_news{$id_news[$foo]}" COLS=30 ROWS=6>{$resume_news[$foo]}</TEXTAREA>
												
												<legend>Isi</legend>
												<div style="width:100%;" id="isi_news_div{$id_news[$foo]}">{$isi_news[$foo]}</div><br />
												<div class="isi" id="isi">
													<TEXTAREA class="mceEditor" id="isi_news{$id_news[$foo]}" style="width:100%; min-height:700px;" NAME="isi_news{$id_news[$foo]}" COLS=30 ROWS=6>{$isi_news[$foo]}</TEXTAREA>	
												</div>
											{else}
												{$resume_news[$foo]}
											{/if}
										</div>
										</news>
										
									</form>
								
							{/for}
						</div>
					</div>
					</section>
				</div>
			</div>
</div>