{literal}
<style type="text/css">
    .center { text-align: center; }
    .right { text-align: right; }
    .row2 { background-color: #eee; }
    .prodi { font-size: 10px; }
</style>
{/literal}
<div class="center_title_bar">Generate Biaya Maba Pasca</div>

<form action="generate-biaya-maba-pasca.php" method="get" id="filter">
<table>
    <tr>
        <td>Penerimaan</td>
        <td>
            <select name="id_penerimaan" onchange="$('#filter').submit()">
                <option value="">-- Pilih Penerimaan --</option>
            {foreach $penerimaan_set as $p}
                <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                {foreach $p.p_set as $p2}
                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN}</option>
                {/foreach}
                </optgroup>
            {/foreach}
            </select>
        </td>
    </tr>
</table>
</form>

<table>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Kelompok Biaya</th>
        <th>Varian</th>
        <th>Status Tagihan</th>
        <th></th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr {if $cmb@index is div by 2}class="row2"{/if}>
        <td class="center">{$cmb@index+1}</td>
        <td>{$cmb.NO_UJIAN}<br/>{$cmb.KODE_VOUCHER}</td>
        <td>{$cmb.NM_C_MHS}<br/>
            <span class="prodi">{$cmb.NM_JENJANG} {$cmb.NM_PROGRAM_STUDI} {$cmb.KELAS_PILIHAN}{if $cmb.NM_PRODI_MINAT != ''}<br/>{$cmb.NM_PRODI_MINAT}{/if}</span>
            <span class="prodi"><br/>PT S1:{$cmb.PTN_S1}<br/>PT S2:{$cmb.PTN_S2}</span>
        </td>
        <td>
            <select name="kb{$cmb.ID_C_MHS}">
                <option value="">-</option>
                {foreach $cmb.kb_set as $kb}
                    <option value="{$kb.ID_KELOMPOK_BIAYA}" {if $cmb.SP3 == $kb.ID_KELOMPOK_BIAYA}selected="selected"{/if}>{$kb.NM_KELOMPOK_BIAYA}</option>
                {/foreach}
            </select>
            <br/><span class="prodi" {if $cmb.SP3_1 > $cmb.VARIAN_SP3}style="background-color: yellow"{/if}>SP3 : {number_format($cmb.SP3_1,0)}</span>
        </td>
        <td class="right" style="font-size: 12px">{number_format($cmb.VARIAN, 0, ",", ".")}<br/><span class="prodi">{number_format($cmb.VARIAN_SP3,0)}</span>
        </td>
        <td class="center" style="font-size: 11px">
            {if $cmb.PEMBAYARAN_CMHS > 0}
                {number_format($cmb.TAGIHAN, 0, ",", ".")}<br/>
                <span class="prodi">{if $cmb.SUDAH_BAYAR > 0}SUDAH BAYAR{else}<span style="color: #f00;">BELUM BAYAR</span>{/if}</span>
                {if $cmb.SUDAH_BAYAR > 0}<br/>P: {number_format($cmb.BAYAR_TAGIHAN, 0, ",", ".")}{/if}
            {else}
                -
            {/if}
        <td class="center">{if $cmb.PEMBAYARAN_CMHS == 0}<button name="gb{$cmb.ID_C_MHS}">Generate</button>{else}{if $cmb.SUDAH_BAYAR == 0}<button name="gb{$cmb.ID_C_MHS}">Generate</button>{/if}{/if}</td>
    </tr>
    {/foreach}
</table>

{literal}<script type="text/javascript">
    $('select[name^="kb"]').change(function() {
        var me = this;
        var id_c_mhs = me.name.replace('kb', '');
        var sp3 = me.value;
        $.ajax({
            url: 'generate-biaya-maba-pasca.php',
            type: 'POST',
            data: 'mode=edit_bk&id_c_mhs=' + id_c_mhs + '&sp3=' + sp3,
            success: function(data) {
                if (data == 'OK')
                    $(me).parent().next().text('REFRESH');
            }
        });
    });
    $('button').die('click');
    $('button[name^="gb"]').click(function() {
        var me = this;
        var id_c_mhs = me.name.replace('gb', '');
        $.ajax({
            url: 'generate-biaya-maba-pasca.php',
            type: 'POST',
            data: 'mode=generate&id_c_mhs=' + id_c_mhs,
            success: function(data) {
                if (data == 'OK')
                    $(me).parent().prev().text('REFRESH');
                else
                    alert(data);
            }
        });
    })
</script>{/literal}