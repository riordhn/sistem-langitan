<div class="center_title_bar">Ubah Status</div>
<form action="ubah-status.php" method="get">
    <input type="hidden" name="mode" value="search" />
    <table>
        <tr>
            <td>NIM Mahasiswa</td>
            <td><input type="text" name="nim_mhs" />
                <input type="submit" value="CARI" />
            </td>
        </tr>
    </table>
</form>

{if $mhs_found}
    <table border="1">
        <tr>
            <td>NIM</td>
            <td><form action="ubah-status.php?{$smarty.server.QUERY_STRING}" method="post">
                    <label>{$mahasiswa.NIM_MHS}</label>
                    <input type="hidden" name="mode" value="reset_password" />
                    <input type="hidden" name="nim_mhs" value="{$mahasiswa.NIM_MHS}" />
                    <input type="submit" value="Reset Password" />
                </form>
            </td>
        </tr>
		<tr>
			<td>Password</td>
			<td style="text-decoration: line-through; color: #e0e0e0">{$mahasiswa.PASSWORD_PENGGUNA}</td>
		</tr>
        <tr>
            <td>Nama</td><td>{$mahasiswa.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Tanggal Lahir</td><td>{$mahasiswa.TGL_LAHIR_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Program Studi</td><td>{$mahasiswa.NM_PROGRAM_STUDI} - {$mahasiswa.NM_FAKULTAS}</td>
        </tr>
        <tr>
            <td>Angkatan</td><td>{$mahasiswa.THN_ANGKATAN_MHS}</td>
        </tr>
        <tr>
            <td>Status Cekal</td>
            <td><form action="ubah-status.php?{$smarty.server.QUERY_STRING}" method="post">
                    <input type="hidden" name="mode" value="set_cekal" />
                    <input type="hidden" name="nim_mhs" value="{$mahasiswa.NIM_MHS}" />
                    {html_options name="status_cekal" options=$status_cekal selected=$mahasiswa.STATUS_CEKAL}
                    <input type="submit" value="Set" />
                </form>
            </td>
        </tr>
        <tr>
            <td>Status Fingerprint</td>
            <td><form action="ubah-status.php?{$smarty.server.QUERY_STRING}" method="post">
                {if $mahasiswa.FINGER_DATA}SUDAH{else}BELUM{/if}
                <input type="hidden" name="mode" value="set_fingerprint" />
                <input type="hidden" name="nim_mhs" value="{$mahasiswa.NIM_MHS}" />
                <input type="submit" value="Reset" />
                </form>
            </td>
        </tr>
        <tr>
            <td>Status Data</td>
            <td>{if $mahasiswa.TGL_LAHIR_PENGGUNA}
                    <form action="ubah-status.php?{$smarty.server.QUERY_STRING}" method="post">    
                    {if $mahasiswa.TGL_LAHIR}<label>SUDAH ADA</label>{else}<label>BELUM ADA</label>
                        <input type="hidden" name="mode" value="insert_fp" />
                        <input type="hidden" name="nim_mhs" value="{$mahasiswa.NIM_MHS}" />
                        <input type="submit" value="Insert" />
                    {/if}
                    </form>
                {else}
                    Disuruh melengkapi biodata
                {/if}
            </td>
        </tr>
</table>
{/if}
