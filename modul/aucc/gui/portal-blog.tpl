	<script>
		$(function() {
			$( "#tabs" ).tabs();
		});
	</script>
	
	<script>
		$(function(){
				$("#submit").buttonset();
				$('#MyFormAdd').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
							$('#result').show();
							$('#MyFormAdd').hide();
							
							var judul_div = document.getElementById('judul_div');
							var isi_div = document.getElementById('isi_div');
							
							judul_div.innerHTML = document.getElementById('judul').value;
							isi_div.innerHTML = document.getElementById('isi').value;

						}
					})
					return false;
				});
		});
	</script>
	
	<script>
		$(function() {
		
			{for $foo=0 to $jml_data_blog-1}
			
				// Button Set
				$( "#submit{$id_blog[$foo]}").buttonset();
				$("#btnEdit{$id_blog[$foo]}").button({
					icons: {
						primary: "ui-icon-pencil",
					},
					text: true
				});
				
				$("#btnDelete{$id_blog[$foo]}").button({
					icons: {
						primary: "ui-icon-trash",
					},
					text: true
				});
				
				$("#btnDeleteIncome{$id_blog[$foo]}").button({
					icons: {
						primary: "ui-icon-trash",
					},
					text: true
				});
				
				$("#btnApprove{$id_blog[$foo]}").button({
					icons: {
						primary: "ui-icon-check",
					},
					text: true
				});
				
				// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
				// Modal
				
				
				// Event Click
				$('#btnEdit{$id_blog[$foo]}').click(function(){		
					$('#judul_blog_div{$id_blog[$foo]}').hide();
					$('#link_blog_div{$id_blog[$foo]}').hide();
					$('#image_link_blog_div{$id_blog[$foo]}').hide();
					$('#isi_blog_div{$id_blog[$foo]}').hide();
					$('#resume_blog_div{$id_blog[$foo]}').hide();
					
					$('#judul_blog{$id_blog[$foo]}').show();					
					$('#link_blog{$id_blog[$foo]}').show();
					$('#image_link_blog{$id_blog[$foo]}').show();
					$('#isi_blog{$id_blog[$foo]}').show();
					$('#resume_blog{$id_blog[$foo]}').show();
					
					$('#btnEdit{$id_blog[$foo]}').hide();
					$('#submit{$id_blog[$foo]}').show();
				});
				
				$('#btnDelete{$id_blog[$foo]}').click(function(){		
					$( "#dialog{$id_blog[$foo]}:ui-dialog" ).dialog( "destroy" );
					$( "#dialog-confirm{$id_blog[$foo]}" ).dialog({
						resizable: false,
						height:180,
						modal: true,
						buttons: {
							"Delete": function() {
								$("#dialog-confirm{$id_blog[$foo]}").dialog( "destroy" );
								//$('#btnDelete{$id_blog[$foo]}').click(function() {
								  $('#MyFormDelete{$id_blog[$foo]}').submit();
								//});
								$("#del_blog{$id_blog[$foo]}").hide();

								//alert("Data Berhasil Dihapus");
							},
							Cancel: function() {
								$("#dialog-confirm{$id_blog[$foo]}").dialog( "destroy" );
							}
						}
					});
				});
				
				$('#btnDeleteIncome{$id_blog[$foo]}').click(function(){		
					$( "#dialog{$id_blog[$foo]}:ui-dialog" ).dialog( "destroy" );
					$( "#dialog-confirm{$id_blog[$foo]}" ).dialog({
						resizable: false,
						height:180,
						modal: true,
						buttons: {
							"Delete": function() {
								$("#dialog-confirm{$id_blog[$foo]}").dialog( "destroy" );
								//$('#btnDelete{$id_blog[$foo]}').click(function() {
								  $('#MyFormDeleteIncome{$id_blog[$foo]}').submit();
								//});
								$("#approve_blog{$id_blog[$foo]}").hide();

								//alert("Data Berhasil Dihapus");
							},
							Cancel: function() {
								$("#dialog-confirm{$id_blog[$foo]}").dialog( "destroy" );
							}
						}
					});
				});
				
				
				// Event Submit
				$('#MyForm{$id_blog[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
						$('#judul_blog_div{$id_blog[$foo]}').show();
						$('#link_blog_div{$id_blog[$foo]}').show();
						$('#image_link_blog_div{$id_blog[$foo]}').show();
						$('#isi_blog_div{$id_blog[$foo]}').show();
						$('#resume_blog_div{$id_blog[$foo]}').show();
						
						$('#resume_blog{$id_blog[$foo]}').hide();
						$('#judul_blog{$id_blog[$foo]}').hide();					
						$('#link_blog{$id_blog[$foo]}').hide();
						$('#image_link_blog{$id_blog[$foo]}').hide();
						$('#isi_blog{$id_blog[$foo]}').hide();
						
						$('#btnEdit{$id_blog[$foo]}').show();
						$('#submit{$id_blog[$foo]}').hide();
							
						var judul_blog_div = document.getElementById('judul_blog_div{$id_blog[$foo]}');
						var isi_blog = document.getElementById('isi_blog{$id_blog[$foo]}');
						var resume_blog = document.getElementById('resume_blog{$id_blog[$foo]}');
						
						var judul_blog_div = document.getElementById('judul_blog_div{$id_blog[$foo]}');
						var link_blog_div = document.getElementById('link_blog_div{$id_blog[$foo]}');
						var image_link_blog_div = document.getElementById('image_link_blog_div{$id_blog[$foo]}');
						var resume_blog_div = document.getElementById('resume_blog_div{$id_blog[$foo]}');
						var isi_blog_div = document.getElementById('isi_blog_div{$id_blog[$foo]}');
						
						judul_blog_div.innerHTML = document.getElementById('judul_blog{$id_blog[$foo]}').value;
						link_blog_div.innerHTML = document.getElementById('link_blog{$id_blog[$foo]}').value;
						image_link_blog_div.innerHTML = document.getElementById('image_link_blog{$id_blog[$foo]}').value;
						resume_blog_div.innerHTML = document.getElementById('resume_blog{$id_blog[$foo]}').value;
						isi_blog_div.innerHTML = document.getElementById('isi_blog{$id_blog[$foo]}').value;

						}
					})
					return false;
				});
				
				$('#MyFormDelete{$id_blog[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
								//$("#dialog-confirm{$id_blog[$foo]}").dialog( "destroy" );
								//$('#btnDelete{$id_blog[$foo]}').click(function() {
								//$('#MyFormDelete{$id_blog[$foo]}').submit();
								//});
								$("#del_blog{$id_blog[$foo]}").hide();

								//alert("Data Berhasil Dihapus");
							
						}
					})
					return false;
				});
				
				$('#MyFormDeleteIncome{$id_blog[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
								//$("#dialog-confirm{$id_blog[$foo]}").dialog( "destroy" );
								//$('#btnDelete{$id_blog[$foo]}').click(function() {
								//$('#MyFormDelete{$id_blog[$foo]}').submit();
								//});
								$("#approve_blog{$id_blog[$foo]}").hide();

								alert("Data Berhasil Dihapus");
							
						}
					})
					return false;
				});
				
				$('#MyFormApprove{$id_blog[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
								//$("#dialog-confirm{$id_blog[$foo]}").dialog( "destroy" );
								//$('#btnDelete{$id_blog[$foo]}').click(function() {
								//$('#MyFormDelete{$id_blog[$foo]}').submit();
								//});
								$("#approve_blog{$id_blog[$foo]}").hide();

								alert("Data Berhasil Diapprove");
							
						}
					})
					return false;
				});
				
				$('#btnApprove{$id_blog[$foo]}').click(function(){
					$('#MyFormApprove{$id_blog[$foo]}').submit();
				});
				
			{/for}
		
		});
	</script>
	<!-- style blog -->
	<script>
		$(function() {
			$( ".column-blog" ).sortable({
				connectWith: ".column-blog"
			});

			$( ".portlet-blog" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
				.find( ".portlet-header-blog" )
					.addClass( "ui-widget-header ui-corner-all" )
					.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
					.end()
				.find( ".portlet-content-blog" );

			$( ".portlet-header-blog .ui-icon" ).click(function() {
				$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
				$( this ).parents( ".portlet-blog:first" ).find( ".portlet-content-blog" ).toggle();
			});

			//$( ".column" ).disableSelection();
			
		});

	</script>
	<!-- end style -->


<div id="tabs">
	<ul>
		<li><a class="disable-ajax" href="#tabs-1">Editor blog</a></li>
		<li><a class="disable-ajax" href="#tabs-2">Add blog</a></li>
		<li><a class="disable-ajax" href="#tabs-3">Delete blog</a></li>
		<li><a class="disable-ajax" href="#tabs-4">Incomming blog</a></li>
	</ul>
	<div id="tabs-1">

			Pages : <a href="#portal-blog!portal-blog.php">ALL blog</a>
			<br /><br />
			<div class="column-blog">	
				<div class="portlet-blog">
					<section>
						<header class="ui-widget-header">
							<div class="portlet-header">blog{$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun}</div>
						</header>
						<span><strong>Pages &nbsp; : &nbsp;</strong></span>
						<div id="paging-blog">
						</div>

					<div id="blog-content" class="portlet-content-blog ui-widget-content" style="background:#fff;">
						<div style="margin:20px;">
							{for $foo=0 to $jml_data_blog-1}
									{if $is_approve[$foo]=='1'}
									<form id="MyForm{$id_blog[$foo]}" name="MyForm{$id_blog[$foo]}" method="post" action="portal-blog-edit-update.php">
										{if $id!=""}
											<div id="btnEdit{$id_blog[$foo]}" name="btnEdit{$id_blog[$foo]}" style="height:30px; width:80px;" title="Edit blog">
												Edit
											</div>
											<div id="submit{$id_blog[$foo]}" name="submit{$id_blog[$foo]}" style="display:none;">
												<input type="submit" name="btn_submit" id="btn_submit" value="Submit"  />
											</div>
										{/if}
										<blog id="blog{$foo}">
										<input style="display:none;" type="text" name="id_blog" value="{$id_blog[$foo]}" >
										<div>
											&nbsp;
										</div>
										<div class="title">
											
											Judul : <a id="judul_blog_div{$id_blog[$foo]}" href="#portal-blog!portal-blog.php?id={$id_blog[$foo]}&view=all">{$judul_blog[$foo]}</a>
											<textarea style="width:700px; height:30px;display:none;" COLS=30 ROWS=3   id="judul_blog{$id_blog[$foo]}" name="judul_blog{$id_blog[$foo]}" >{$judul_blog[$foo]}</textarea>
										</div>
										<div class="date">
											Oleh : &nbsp; {$pengguna[$foo]}
											<br />
											Tanggal : &nbsp; {$tgl_blog[$foo]}
										</div>
										<div class="content_blog">
											{if $view == "all"}
												<legend>Link</legend>
												<div style="width:700px;" id="link_blog_div{$id_blog[$foo]}">{$link_blog[$foo]}</div><br />								
												<TEXTAREA id="link_blog{$id_blog[$foo]}" style="width:700px; height:30px;display:none;" NAME="link_blog{$id_blog[$foo]}" COLS=30 ROWS=6>{$link_blog[$foo]}</TEXTAREA>
												
												<legend>Image Link</legend>
												<div style="width:700px;" id="image_link_blog_div{$id_blog[$foo]}"><img style="width:100px; height:100px;" src="{$img_link[$foo]}"/></div><br />
												<TEXTAREA id="image_link_blog{$id_blog[$foo]}" style="width:700px; height:30px;display:none;" NAME="image_link_blog{$id_blog[$foo]}" COLS=30 ROWS=6>{$img_link[$foo]}</TEXTAREA>
												
												<legend>Resume</legend>
												<div style="width:700px;" id="resume_blog_div{$id_blog[$foo]}">{$isi_blog[$foo]}</div><br />
												<TEXTAREA id="resume_blog{$id_blog[$foo]}" style="display:none; width:700px; min-height:150px;" NAME="resume_blog{$id_blog[$foo]}" COLS=30 ROWS=6>{$resume_blog[$foo]}</TEXTAREA>
												
												<legend>Isi</legend>
												<div style="width:700px;" id="isi_blog_div{$id_blog[$foo]}">{$isi_blog[$foo]}</div><br />
												<TEXTAREA id="isi_blog{$id_blog[$foo]}" style="display:none; width:700px; min-height:700px;" NAME="isi_blog{$id_blog[$foo]}" COLS=30 ROWS=6>{$isi_blog[$foo]}</TEXTAREA>
											{else}
												{$resume_blog[$foo]}
											{/if}
										</div>
										</blog>
										
									</form>
									{/if}
								
							{/for}
						</div>
					</div>
					</section>
				</div>
			</div>
	</div>
	<div id="tabs-2">
	Pages : <a href="#portal-blog!portal-blog.php">ALL blog</a>
	<div id="result" style="display:none;">
		Berhasil ditambahkan
		<div id="judul_div"></div>
		<div id="resume_div"></div>
		<div id="isi_div"></div>
	</div>
	{$id_pengguna}
	<form id="MyFormAdd" name="MyFormAdd" method="post" action="portal-insert-blog.php">
		<table style="width:100%" class="ui-widget">
			<thead class="ui-widget-header">
				<tr>
					<td width="20%">Item</td><td>Isian</td>
				</tr>
			</thead>
			<tbody class="ui-widget-content" valign="top">
				<tr>	
					<td >Judul</td>
					<td>
						<TEXTAREA id="judul" style="width:600px; height:30px;" NAME="judul" COLS=30 ROWS=6></TEXTAREA>
						<TEXTAREA id="id_pengguna" style="width:600px; height:30px; display:none;" NAME="id_pengguna" COLS=30 ROWS=6>{$id_pengguna}</TEXTAREA>
					</td>
				</tr>
				<tr>	
				<tr>	
					<td valign="top" style="text-align:top;">Link</td>
					<td>
						<TEXTAREA id="link" style="width:600px; height:30px;" NAME="link" COLS=30 ROWS=6></TEXTAREA>
					</td>
				</tr>
				<tr>	
					<td valign="top" style="text-align:top;">Image Link</td>
					<td>
						<TEXTAREA id="image_link" style="width:600px; height:30px;" NAME="image_link" COLS=30 ROWS=6></TEXTAREA>
					</td>
				</tr>
				<tr>	
					<td valign="top" style="text-align:top;">Resume</td>
					<td>
						<TEXTAREA id="resume" style="width:600px; height:30px;" NAME="resume" COLS=30 ROWS=6></TEXTAREA>
					</td>
				</tr>
				<tr>	
					<td valign="top" style="text-align:top;">Isi</td>
					<td>
						<TEXTAREA id="isi" style="width:600px; min-height:700px;" NAME="isi" COLS=30 ROWS=6></TEXTAREA>
					</td>
				</tr>
				<tr>	
					<td></td>
					<td>
						<div id="submit" name="submit" >
							<input type="submit" name="btn_submit" id="btn_submit" value="Submit"  />
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	</div>

	<div id="tabs-3">
		<div class="column ui-widget">
			<div class="portlet-blog">
				<header>
					<div class="ui-widget-header">All blog</div>
				</header>
				<div class="portlet-content-blog">
					{for $foo=0 to $jml_data_blog-1}
						{if $is_approve[$foo]==1}
					<div id="del_blog{$id_blog[$foo]}">
					<div style="width:300px; display:none;" id="dialog-confirm{$id_blog[$foo]}" title="Delete : {$judul_blog[$foo]}???">
						<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
					</div>
					
						<form id="MyFormDelete{$id_blog[$foo]}" name="MyFormDelete{$id_blog[$foo]}" method="post" action="portal-del-blog.php">
						<TEXTAREA id="id_blog" style="display:none;" NAME="id_blog" COLS=30 ROWS=6>{$id_blog[$foo]}</TEXTAREA>
						<div class="title disable-ajax">
							<a  href="{$link_blog[$foo]}" target="_blank" class="disable-ajax" >{$judul_blog[$foo]}</a>
						</div>
						<div class="date">
							Date : {$tgl_blog[$foo]}
						</div>
						<div class="resume">
							<table class="ui-widget" width="100%">
								<tbody class="ui-widget-content">
								<tr>
									<td style="width:100px;"><img style="width:100px; height:100px;" src="{$img_link_blog[$foo]}"/></td>
									<td style="width:100%" valign="top">{$resume_blog[$foo]}</td>
								</tr>
								<tr>
									<td style="width:100px;"></td>
									<td style="width:100%"valign="top">
										<div id="btnDelete{$id_blog[$foo]}">
											<!--<input type="submit" value="Hapus" />-->
											Hapus
										</div>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
						</form>
						</div>
						{/if}
					{/for}
				</div>
			</div>
		</div>
	</div>
	
	<div id="tabs-4">
		<div class="column ui-widget">
			<div class="portlet-blog">
				<header>
					<div class="ui-widget-header">All blog</div>
				</header>
				<div class="portlet-content-blog">
					{for $foo=0 to $jml_data_blog-1}
						{if $is_approve[$foo]==0 && $is_active[$foo]==1}
					<div id="approve_blog{$id_blog[$foo]}">
					<div style="width:300px; display:none;" id="dialog-confirm{$id_blog[$foo]}" title="Delete : {$judul_blog[$foo]}???">
						<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
					</div>
						<form id="MyFormDeleteIncome{$id_blog[$foo]}" name="MyFormDeleteIncome{$id_blog[$foo]}" method="post" action="portal-del-blog.php">
						<TEXTAREA id="id_blog" style="display:none;" NAME="id_blog" COLS=30 ROWS=6>{$id_blog[$foo]}</TEXTAREA>
						</form>						
						<form id="MyFormApprove{$id_blog[$foo]}" name="MyFormApprove{$id_blog[$foo]}" method="post" action="portal-app-blog.php">
						<TEXTAREA id="id_blog" style="display:none;" NAME="id_blog" COLS=30 ROWS=6>{$id_blog[$foo]}</TEXTAREA>
						<div class="title disable-ajax">
							<a  href="{$link_blog[$foo]}" target="_blank" class="disable-ajax" >{$judul_blog[$foo]}</a>
						</div>
						<div class="date">
							Date : {$tgl_blog[$foo]}
						</div>
						<div class="resume">
							<table class="ui-widget" width="100%">
								<tbody class="ui-widget-content">
								<tr>
									<td style="width:100px;"><img style="width:100px; height:100px;" src="{$img_link_blog[$foo]}"/></td>
									<td style="width:100%" valign="top">{$resume_blog[$foo]}</td>
								</tr>
								<tr>
									<td style="width:100px;"></td>
									<td style="width:100%"valign="top">
										<div id="btnApprove{$id_blog[$foo]}" style="float:left;">
											<!--<input type="submit" value="Approve" />-->
											Approve
										</div>

										<div id="btnDeleteIncome{$id_blog[$foo]}" style="float:left;">
											<!--<input type="submit" value="Delete" />-->
											Delete
										</div>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
						</form>
						</div>
						{/if}
					{/for}
				</div>
			</div>
		</div>
	</div>
</div>