<div class="center_title_bar">Tambah Sekolah</div>

<form action="sekolah.php" method="post">
<input type="hidden" name="mode" value="add" />
<table>
    <tr>
        <td>Lokasi Sekolah</td>
        <td>
            <select id="provinsi">
                {foreach $provinsi_set as $p}
                    <option value="{$p.ID_PROVINSI}">{$p.NM_PROVINSI}</option>
                {/foreach}
            </select>
            <select name="id_kota" id="kota"></select>
            {literal}<script type="text/javascript">
                $('#provinsi').change(function() {
                    $.ajax({
                        url: 'sekolah.php?mode=get-kota&id_provinsi=' + this.value,
                        success: function(data) {
                            $('#kota').html(data);
                        }
                    });
                });
            </script>{/literal}
        </td>
    </tr>
    <tr>
        <td>Nama Sekolah</td>
        <td>
            <input type="text" name="nm_sekolah" size="50" />
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>