<div class="center_title_bar">Master Sekolah</div>

<form action="sekolah.php" method="get">
    <table>
        <tr>
            <td>Nama Sekolah</td>
            <td><input type="text" name="q" size="50" value="{$smarty.get.q}" autofocus="true"/><input type="submit" value="Cari"/><a href="sekolah.php?mode=tambah">Tambah</a></td>
        </tr>
    </table>
</form>
        
{if $added}<h2>{$added}</h2>{/if}

{if !empty($sekolah_set)}
<table>
    <tr>
        <th>ID</th>
        <th>Nama Sekolah</th>
        <th>Kota</th>
        <th>Provinsi</th>
    </tr>
    {foreach $sekolah_set as $s}
    <tr {if $s@index is div by 2}style="background-color: #eee"{/if}>
        <td><a href="sekolah.php?mode=edit&id_sekolah={$s.ID_SEKOLAH}">{$s.ID_SEKOLAH}</a></td>
        <td>{$s.NM_SEKOLAH}</td>
        <td>{$s.NM_KOTA}</td>
        <td>{$s.NM_PROVINSI}</td>
    </tr>
    {/foreach}
</table>
{/if}