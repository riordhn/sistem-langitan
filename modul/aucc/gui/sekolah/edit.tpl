<div class="center_title_bar">Edit Sekolah</div>

<form action="sekolah.php" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_sekolah" value={$sekolah.ID_SEKOLAH} />
<table>
    <tr>
        <td>Lokasi Sekolah</td>
        <td>
            <select id="provinsi">
                {foreach $provinsi_set as $p}
                    <option value="{$p.ID_PROVINSI}" {if $p.ID_PROVINSI == $sekolah.ID_PROVINSI}selected="selected"{/if}>{$p.NM_PROVINSI}</option>
                {/foreach}
            </select>
            <select name="id_kota" id="kota">
                {foreach $kota_set as $k}
                    <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $sekolah.ID_KOTA}selected="selected"{/if}>{$k.NM_KOTA}</option>
                {/foreach}
            </select>
            {literal}<script type="text/javascript">
                $('#provinsi').change(function() {
                    $.ajax({
                        url: 'sekolah.php?mode=get-kota&id_provinsi=' + this.value,
                        success: function(data) {
                            $('#kota').html(data);
                        }
                    });
                });
            </script>{/literal}
        </td>
    </tr>
    <tr>
        <td>Nama Sekolah</td>
        <td>
            <input type="text" name="nm_sekolah" size="50" value="{$sekolah.NM_SEKOLAH}"/>
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>
            
<form action="sekolah.php" method="post">
<input type="hidden" name="mode" value="delete" />
<input type="hidden" name="id_sekolah" value={$sekolah.ID_SEKOLAH} />
<a href="sekolah.php">Kembali</a><input type="submit" value="Hapus" />
</form>