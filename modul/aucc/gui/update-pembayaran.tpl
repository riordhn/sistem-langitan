<div class="center_title_bar">Update Pembayaran</div> 
<form method="post" action="update-pembayaran.php">
    <table>
        <tr>
            <td>NIM Mahasiswa</td>
            <td><input type="text" name="cari_nim"/></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if isset($cari_nim)}
    <form action="update-pembayaran.php?mode=biaya" method="post">
        <table>
            <tr>
                <th colspan="10" style="text-align: center;">Updata Biaya Mahasiswa Detail</th>
            </tr>
            <tr>
                <th>Nama Biaya</th>
                <th>Besar Biaya</th>
                <th>Denda Biaya</th>
                <th>Is Tagih</th>
                <th>Tanggal Bayar</th>
                <th>Nama Bank</th>
                <th>Via Bank</th>
                <th>No Transaksi</th>
                <th>Keterangan</th>
                <th>Semester Pembayaran</th>
            </tr>
            {$index=1}
            {foreach $data_pembayaran as $data}
                {if $data['NM_BIAYA']}
                    <tr>
                        <td>
                            <input type="hidden" name="id{$index}" value="{$data['ID_PEMBAYARAN']}"/>
                            {$data['NM_BIAYA']}
                        </td>
                        <td>
                            <input type="text" size="8" name="besar_biaya{$index}" value="{$data['BESAR_BIAYA']}" />
                        </td>
                        <td>
                            <input type="text" size="8" name="denda_biaya{$index}" value="{$data['DENDA_BIAYA']}"/>
                        </td>
                        <td>
                            <select name="is_tagih{$index}">
                                <option value="Y" {if $data['IS_TAGIH']=='Y'}selected="true"{/if}>Y</option>
                                <option value="T" {if $data['IS_TAGIH']=='T'}selected="true"{/if}>T</option>
                            </select>
                        </td>
                        <td>
							<select name="bank{$index}">
								<option value=""></option>
								{foreach $data_bank as $data_b}
									<option value="{$data_b['ID_BANK']}" {if $data_b['ID_BANK']==$data['ID_BNK']}selected="true"{/if}>{$data_b['NM_BANK']}</option>
								{/foreach}
							</select>
						</td>
						<td>
							<select name="bank_via{$index}">
								<option value=""></option>
								{foreach $data_bank_via as $data_bv}
									<option value="{$data_bv['ID_BANK_VIA']}" {if $data_bv['ID_BANK_VIA']==$data['ID_BNKV']}selected="true"{/if}>{$data_bv['NAMA_BANK_VIA']}</option>
								{/foreach}
							</select>
						</td>
						<td>
							<input type="text" class="date" name="tgl_bayar{$index}" size="7" value="{$data['TGL_BAYAR']}" />
						</td>
						<td>
							<input type="text" name="no_transaksi{$index}" size="10" value="{$data['NO_TRANSAKSI']}" />
						</td>
						<td>
							<input type="text" name="keterangan{$index++}" size="10" value="{$data['KETERANGAN']}" />
						</td>
                        <td>    
							{$data['NM_SEMESTER']} ({$data['TAHUN_AJARAN']}) 
						</td>
                    </tr>
                {else}
                    <tr>
                        <td colspan="10" style="text-align: center;">Data Pembayaran Kosong</td>
                    </tr>
                {/if}
            {/foreach}
            <tr>
                <td colspan="10">
                    <input type="hidden" name="cari_nim" value="{$data_pembayaran[0]['NIM_MHS']}"/>
                    <input type="hidden" name="jumlah" value="{$index}"/>
                    <input style="float: right;" type="submit" value="Update"/>
                </td>
            </tr>
        </table>
    </form>
    <form action="update-pembayaran.php?mode=status" method="post">
        <table>
            <tr>
                <th colspan="8" class="center">Update Biaya Mahasiswa All</th>
            </tr>
            <tr>
                <th colspan="4">NAMA</th>
                <th colspan="4">NIM</th>
            </tr>
            <tr>
                <td colspan="4">
                    <input type="hidden" name="cari_nim" value="{$data_pembayaran[0]['NIM_MHS']}"/>
                    <input type="hidden" name="id" value="{$data_pembayaran[0]['ID_MHS']}"/>
                    {$data_pembayaran[0]['NM_PENGGUNA']}
                </td>
                <td colspan="4">{$data_pembayaran[0]['NIM_MHS']}</td>
            </tr>
            <tr>
                <th>Semester Bayar</th>
				<th>Is Tagih</th>
                <th>Nama Bank</th>
                <th>Via Bank</th>
                <th>Tanggal Bayar</th>
                <th>No Transaksi</th>
                <th>Keterangan</th>
                <th>Proses</th>
            </tr>
            <tr>
                <td>
                    <select name="semester">
                        {foreach $data_semester as $data}
                            <option value="{$data['ID_SEMESTER']}" {if $data['ID_SEMESTER']==$data_pembayaran[0]['ID_SEMESTER']}selected="true"{/if}>{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                        {/foreach}
                    </select>
                </td>
				<td>
					<select name="is_tagih">
						<option value="Y" {if $data_pembayaran[0]['IS_TAGIH']=='Y'}selected="true"{/if}>Y</option>
						<option value="T" {if $data_pembayaran[0]['IS_TAGIH']=='T'}selected="true"{/if}>T</option>
					</select>
				</td>
                <td>
                    <select name="bank">
                        <option value=""></option>
                        {foreach $data_bank as $data}
                            <option value="{$data['ID_BANK']}" {if $data['ID_BANK']==$data_pembayaran[0]['ID_BNK']}selected="true"{/if}>{$data['NM_BANK']}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <select name="bank_via">
                        <option value=""></option>
                        {foreach $data_bank_via as $data}
                            <option value="{$data['ID_BANK_VIA']}" {if $data['ID_BANK_VIA']==$data_pembayaran[0]['ID_BNKV']}selected="true"{/if}>{$data['NAMA_BANK_VIA']}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <input type="text" class="date" name="tgl_bayar" size="7" value="{$data_pembayaran[0]['TGL_BAYAR']}" />
                </td>
                <td>
                    <input type="text" name="no_transaksi" size="15" value="{$data_pembayaran[0]['NO_TRANSAKSI']}" />
                </td>
                <td>
                    <input type="text" name="keterangan" size="15" value="{$data_pembayaran[0]['KETERANGAN']}" />
                </td>
                <td>
                    <input type="submit" value="Update"/>
                </td>
            </tr>
        </table>
    </form>
{/if}
{literal}
    <script>
            $(function() {
                    $( ".date" ).datepicker({dateFormat:'dd-M-yy'});
            });
    </script>
{/literal}