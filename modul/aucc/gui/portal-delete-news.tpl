<script>
	$(function() {
		$( "#tabs" ).tabs({
			ajaxOptions: {
				error: function( xhr, status, index, anchor ) {
					$( anchor.hash ).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo." );
				}
			}
		});
	});
</script>
	
	<script>
		$(function() {
		
			{for $foo=0 to $jml_data_news-1}
				$( "#submit{$id_news[$foo]}").buttonset();
				$("#btnDelete{$id_news[$foo]}").button({
					icons: {
						primary: "ui-icon-trash",
					},
					text: true
				});
				
				$('#btnDelete{$id_news[$foo]}').click(function(){		
					$('#link_judul{$id_news[$foo]}').hide();
					$('#isi_news{$id_news[$foo]}').hide();
					
					$('#judul{$id_news[$foo]}').show();
					$('#isi{$id_news[$foo]}').show();
					$('#btnEdit{$id_news[$foo]}').hide();
					//$('#submit{$id_news[$foo]}').show();
					
					$('#MyFormDelete{$id_news[$foo]}').submit();
				});
				
				$('#MyFormDelete{$id_news[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
							/*
						
							$('#link_judul{$id_news[$foo]}').show();
							$('#isi_news{$id_news[$foo]}').show();
							
							$('#judul{$id_news[$foo]}').hide();
							$('#isi{$id_news[$foo]}').hide();
							$('#btnEdit{$id_news[$foo]}').show();
							$('#submit{$id_news[$foo]}').hide();
							
							var link_judul = document.getElementById('link_judul{$id_news[$foo]}');
							var isi_news = document.getElementById('isi_news{$id_news[$foo]}');
							
							link_judul.innerHTML = document.getElementById('judul{$id_news[$foo]}').value;
							isi_news.innerHTML = document.getElementById('isi{$id_news[$foo]}').value;
							*/
							alert('Berhasil Menghapus News');
							window.location.href = "/modul/aucc/#portal-news!portal-delete-news.php";

						}
					})
					return false;
				});
				
			{/for}
		
		});
	</script>

<div id='tabs'>
			Pages : <a href="#portal-news!portal-news.php">ALL news</a>
			<br /><br />
			<div class="column-news">	
				<div class="portlet-news">
					<section>

					<div id="news-content" class="portlet-content-news ui-widget-content" style="background:#fff;">
						<div style="margin:20px;">
							{for $foo=0 to $jml_data_news-1}
									{if $is_approve[$foo]=='1'}
									<form id="MyFormDelete{$id_news[$foo]}" name="MyForm{$id_news[$foo]}" method="post" action="portal-del-news.php">
										{if $id!=""}
											<div id="btnDelete{$id_news[$foo]}" name="btnDelete{$id_news[$foo]}" style="height:30px; width:80px;" title="Delete News">
												Delete
											</div>
											<div id="submit{$id_news[$foo]}" name="submit{$id_news[$foo]}" style="display:none;">
												<input type="submit" name="btn_submit" id="btn_submit" value="Submit"  />
											</div>
										{/if}
										<news id="news{$foo}">
										<input style="display:none;" type="text" name="id_news" value="{$id_news[$foo]}" >
										<div>
											&nbsp;
										</div>
										<div class="title">
											
											Judul : <a class="disable-ajax" id="judul_news_div{$id_news[$foo]}" href="#portal-news!portal-delete-news.php?id={$id_news[$foo]}&view=all">{$judul_news[$foo]}</a>
											<textarea style="width:700px; height:30px;display:none;" COLS=30 ROWS=3   id="judul_news{$id_news[$foo]}" name="judul_news{$id_news[$foo]}" >{$judul_news[$foo]}</textarea>
										</div>
										<div class="date">
											Oleh : &nbsp; {$pengguna[$foo]}
											<br />
											Tanggal : &nbsp; {$tgl_news[$foo]}
										</div>
										<div class="content_news">
											{if $view == "all"}
												
												<legend>Image Link</legend>
												<div style="width:700px;" id="image_link_news_div{$id_news[$foo]}"><img style="width:100px; height:100px;" src="{$img_link[$foo]}"/></div><br />
												<TEXTAREA id="image_link_news{$id_news[$foo]}" style="width:700px; height:30px;display:none;" NAME="image_link_news{$id_news[$foo]}" COLS=30 ROWS=6>{$img_link[$foo]}</TEXTAREA>
												
												<legend>Resume</legend>
												<div style="width:700px;" id="resume_news_div{$id_news[$foo]}">{$resume_news[$foo]}</div><br />
												<TEXTAREA id="resume_news{$id_news[$foo]}" style="display:none; width:700px; min-height:150px;" NAME="resume_news{$id_news[$foo]}" COLS=30 ROWS=6>{$resume_news[$foo]}</TEXTAREA>
												
												<legend>Isi</legend>
												<div style="width:700px;" id="isi_news_div{$id_news[$foo]}">{$isi_news[$foo]}</div><br />
												<TEXTAREA id="isi_news{$id_news[$foo]}" style="display:none; width:700px; min-height:700px;" NAME="isi_news{$id_news[$foo]}" COLS=30 ROWS=6>{$isi_news[$foo]}</TEXTAREA>
											{else}
												{$resume_news[$foo]}
											{/if}
										</div>
										</news>
										
									</form>
									{/if}
								
							{/for}
						</div>
					</div>
					</section>
				</div>
			</div>
</div>
	


