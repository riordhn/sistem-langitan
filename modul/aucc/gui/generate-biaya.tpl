<div class="center_title_bar">Generate Biaya</div>
<form method="post" action="generate-biaya.php?mode=nim">
    <table>
        <tr>
            <td>NIM Mahasiswa</td>
            <td><input type="text" name="nim"/></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if isset($nim) && isset($data_biaya_kuliah)}
    <form action="generate-biaya.php?mode=biaya_kuliah" method="post">
        <table>
            <tr>
                <th colspan="4">Generate Biaya Kuliah</th>
            </tr>
            <tr>
                <th>NIM</th>
                <th>Besar Biaya</th>
                <th>Status Biaya</th>
                <th></th>
            </tr>
            {if $data_biaya_kuliah!=null}
                <tr>
                    <td>{$data_biaya_kuliah.NIM_MHS}</td>
                    <td>{$data_biaya_kuliah.BESAR_BIAYA_KULIAH}</td>
                    <td>Biaya Kuliah Ditemukan</td>
                    <td>
                        <input type="submit" value="Generate Biaya Kuliah"/>
                        <input type="hidden" name="nim" value="{$nim}"/>
                    </td>
                </tr>
            {/if}
        </table>
    </form>
{else if isset($nim) && isset($data_pembayaran)}
    <form action="generate-biaya.php?mode=pembayaran" method="post">
        <table>
            <tr>
                <th colspan="2">Generate Pembayaran</th>
            </tr>
            <tr>
            <input type="hidden" name="nim" value="{$nim}"/>
            <td>{$data_pembayaran.NIM_MHS} Terdeteksi Pembayarannya</td>
            <td><input type="submit" value="Generate Pembayaran"/></td>
            </tr>
        </table>
    </form>
{/if}