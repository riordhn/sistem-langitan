	<script>
		$(function() {
		
			{for $foo=0 to $jml_data_news-1}
				$( "#submit{$id_news[$foo]}").buttonset();
				$("#btnEdit{$id_news[$foo]}").button({
					icons: {
						primary: "ui-icon-pencil",
					},
					text: true
				});
				
				$('#btnEdit{$id_news[$foo]}').click(function(){		
					$('#link_judul{$id_news[$foo]}').hide();
					$('#isi_news{$id_news[$foo]}').hide();
					
					$('#judul{$id_news[$foo]}').show();
					$('#isi{$id_news[$foo]}').show();
					$('#btnEdit{$id_news[$foo]}').hide();
					$('#submit{$id_news[$foo]}').show();
				});
				
				$('#MyForm{$id_news[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
							$('#link_judul{$id_news[$foo]}').show();
							$('#isi_news{$id_news[$foo]}').show();
							
							$('#judul{$id_news[$foo]}').hide();
							$('#isi{$id_news[$foo]}').hide();
							$('#btnEdit{$id_news[$foo]}').show();
							$('#submit{$id_news[$foo]}').hide();
							
							var link_judul = document.getElementById('link_judul{$id_news[$foo]}');
							var isi_news = document.getElementById('isi_news{$id_news[$foo]}');
							
							link_judul.innerHTML = document.getElementById('judul{$id_news[$foo]}').value;
							isi_news.innerHTML = document.getElementById('isi{$id_news[$foo]}').value;

						}
					})
					return false;
				});
				
			{/for}
		
		});
	</script>
Pages : <a href="portal-news.php">ALL NEWS</a>
<br /><br />
<div class="column-news">	
	<div class="portlet-news">
		<section>
			<header class="ui-widget-header">
				<div class="portlet-header">News{$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun}</div>
			</header>
			<span><strong>Pages &nbsp; : &nbsp;</strong></span>
			<div id="paging-news">
			</div>

		<div id="news-content" class="portlet-content-news ui-widget-content" style="background:#fff;">
			<div style="margin:20px;">
				{for $foo=0 to $jml_data_news-1}

				<form id="MyForm{$id_news[$foo]}" name="MyForm{$id_news[$foo]}" method="post" action="portal-news-update.php">
					{if $id!=""}
						<div id="btnEdit{$id_news[$foo]}" name="btnEdit{$id_news[$foo]}" style="height:30px; width:80px;" title="Edit News">
							Edit
						</div>
						<div id="submit{$id_news[$foo]}" name="submit{$id_news[$foo]}" style="display:none;">
							<input type="submit" name="btn_submit" id="btn_submit" value="Submit"  />
						</div>
					{/if}
					<article id="article{$foo}">
					<input style="display:none;" type="text" name="id_news" value="{$id_news[$foo]}" >
					<div>
						&nbsp;
					</div>
					<div class="title">
						<a id="link_judul{$id_news[$foo]}" href="portal-news.php?id={$id_news[$foo]}&view=all">{$judul_news[$foo]}</a>
						<TEXTAREA style="display:none;"  id="judul{$id_news[$foo]}" name="judul{$id_news[$foo]}" >{$judul_news[$foo]}</TEXTAREA>
					</div>
					<div class="date">
						Date : &nbsp; {$tgl_news[$foo]}
					</div>
					<div class="content_news">
						{if $view == "all"}
							<div style="width:500px;" id="isi_news{$id_news[$foo]}">{$isi_news[$foo]}</div>
							<TEXTAREA id="isi{$id_news[$foo]}" style="display:none; width:700px; min-height:700px;" NAME="isi{$id_news[$foo]}" COLS=30 ROWS=6>{$isi_news[$foo]}</TEXTAREA>
						{else}
							{$resume_news[$foo]}
						{/if}
					</div>
					</article>
					
				</form>
				{/for}
			</div>
		</div>
		</section>
	</div>