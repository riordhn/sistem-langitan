<div class="center_title_bar">Utility Propinsi</div>

<form action="utility_propinsi.php" method="get">
	<table>
		<tr>
			<td>Negara</td>
			<td>
				<select name="id_negara">
					<option value=""> -- </option>
					{foreach $negara_set as $negara}
						<option value="{$negara.ID_NEGARA}" {if !empty($smarty.get.id_negara)}{if $smarty.get.id_negara == $negara.ID_NEGARA}selected{/if}{/if}>{$negara.NM_NEGARA}</option>
					{/foreach}
				</select>
			</td>
		</tr>
	</table>
</form>

<script type="text/javascript">
	jQuery(document).ready(function() {
		$('[name=id_negara]').change(function() {
			$('form').submit();
		});
	});
</script>

{if isset($smarty.get.id_negara)}
	<table>
		<tbody>
			<tr>
				<td>Tambah Provinsi Baru</td>
				<td>
					<form action="utility_propinsi.php?id_negara={$smarty.get.id_negara}" method="post">
						<input type="text" name="nm_provinsi" value="" placeholder="Nama Provinsi"/>
						<input type="submit" value="Tambah" />
					</form>
				</td>
			</tr>
		</tbody>
	</table>
{/if}

{if $provinsi_set}

	<table>
		<tbody>
			<tr>
				<th>No</th>
				<th>Provinsi</th>
			</tr>
			{foreach $provinsi_set as $provinsi}
				<tr>
					<td>{$provinsi@index + 1}</td>
					<td>{$provinsi.NM_PROVINSI}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>

{/if}