<div class="center_title_bar">Utility Kota</div>

<form action="utility_kota.php" method="get" id="filter">
	<table>
		<tr>
			<td>Negara</td>
			<td>
				<select name="id_negara">
					<option value=""> -- </option>
					{foreach $negara_set as $negara}
						<option value="{$negara.ID_NEGARA}" {if !empty($smarty.get.id_negara)}{if $smarty.get.id_negara == $negara.ID_NEGARA}selected{/if}{/if}>{$negara.NM_NEGARA}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Provinsi</td>
			<td>
				{if $provinsi_set}
					<select name="id_provinsi">
						<option value=""> -- </option>
						{foreach $provinsi_set as $provinsi}
							<option value="{$provinsi.ID_PROVINSI}" {if !empty($smarty.get.id_provinsi)}{if $smarty.get.id_provinsi == $provinsi.ID_PROVINSI}selected{/if}{/if}>{$provinsi.NM_PROVINSI}</option>
						{/foreach}
					</select>
				{/if}
			</td>
		</tr>
	</table>
</form>

<script type="text/javascript">
	jQuery(document).ready(function() {
		$('[name=id_negara]').change(function() {
			$('[name=id_provinsi]').val('');
			$('#filter').submit();
		});
		$('[name=id_provinsi]').change(function() {
			$('#filter').submit();
		});
	});
</script>

{if !empty($smarty.get.id_provinsi)}
<table>
	<tbody>
		<tr>
			<td>Tambah Kota Baru</td>
			<td>
				<form action="utility_kota.php?id_negara={$smarty.get.id_negara}&id_provinsi={$smarty.get.id_provinsi}" method="post">
					<input type="text" name="nm_kota" value="" placeholder="Nama kota"/>
					<select name="tipe_dati2">
						<option value="">--</option>
						<option value="Kabupaten">Kabupaten</option>
						<option value="Kota">Kota</option>
					</select>
					<input type="submit" value="Tambah" />
				</form>
			</td>

		</tr>
	</tbody>
</table>
{/if}

{if $kota_set}

<table>
	<tbody>
		<tr>
			<th>No</th>
			<th>Kota</th>
			<th>Tipe Dati II</th>
		</tr>
		{foreach $kota_set as $kota}
			<tr>
				<td>{$kota@index + 1}</td>
				<td>{$kota.NM_KOTA}</td>
				<td>{$kota.TIPE_DATI2}</td>
			</tr>
		{/foreach}
	</tbody>
</table>

{/if}