<script>
	$(function(){
	
			$("#btnSubmit").button({
				icons: {
					primary: "ui-icon-check",
				},
				text: true
			});

			$('#MyFormAdd').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {
						
						tinyMCE.execCommand('mceRemoveControl',false,'isi'); //removing tiny_mce 
						$('#result').show();
						$('#MyFormAdd').hide();
						
						var judul_div = document.getElementById('judul_div');
						var isi_div = document.getElementById('isi_div');
						
						judul_div.innerHTML = document.getElementById('judul').value;
						isi_div.innerHTML = document.getElementById('isi').value;

					}
				})
				return false;
			});
			
			$('#btnSubmit').click(function(){
				var ed = tinyMCE.get('isi');
				var isi = ed.getContent();
				$("#get_isi").val(isi);
				$('#MyFormAdd').submit();
			});
			
	});
</script>


<script type="text/javascript">
	tinyMCE.init({
			mode : "textareas",
			theme : "advanced",
			editor_selector : "mceEditor",
			editor_deselector : "mceNoEditor",
			  plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,",
			  theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
			  theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			  theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			  theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			  theme_advanced_toolbar_location : "top",
			  theme_advanced_toolbar_align : "left",
			  theme_advanced_statusbar_location : "bottom",
			  theme_advanced_resizing : true
	});
</script>
	<!-- end style -->
	Pages : <a href="#portal-news!portal-news.php">ALL news</a>
	<div id="result" style="display:none;">
		Berhasil ditambahkan
		<div id="judul_div"></div>
		<div id="resume_div"></div>
		<div id="isi_div"></div>
	</div>
	<form id="MyFormAdd" name="MyFormAdd" method="post" action="portal-insert-news.php">
		<input value="0" type="text" id="get_isi" name="get_isi" style="display:none;">
		<table style="width:100%" class="ui-widget">
			<thead class="ui-widget-header">
				<tr>
					<td width="20%">Item</td><td>Isian</td>
				</tr>
			</thead>
			<tbody class="ui-widget-content" valign="top">
				<tr>	
					<td >Judul</td>
					<td>
						<TEXTAREA id="judul" style="width:100%; height:30px;" NAME="judul" COLS=30 ROWS=6></TEXTAREA>
						<input type="text" id="id_pengguna" style="display:none;" NAME="id_pengguna" value="{$id_pengguna}"/>
					</td>
				</tr>
				<tr>	
				<tr>	
					<td valign="top" style="text-align:top;">Image Link</td>
					<td>
						<TEXTAREA id="image_link" style="width:100%; height:30px;" NAME="image_link" COLS=30 ROWS=6></TEXTAREA>
					</td>
				</tr>
				<tr>	
					<td valign="top" style="text-align:top;">Resume</td>
					<td>
						<TEXTAREA id="resume" style="width:100%; height:60px;" NAME="resume" COLS=30 ROWS=6></TEXTAREA>
					</td>
				</tr>
				<tr>	
					<td valign="top" style="text-align:top;">Isi</td>
					<td>
						<TEXTAREA id="isi" class="mceEditor" style="width:100%; min-height:700px;" NAME="isi" COLS=30 ROWS=6></TEXTAREA>
					</td>
				</tr>
				<tr>	
					<td></td>
					<td>
						<div id="btnSubmit" name="btnSubmit" >
							<!--<input type="submit" name="btn_submit" id="btn_submit" value="Submit"  />-->
							Submit
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</form>