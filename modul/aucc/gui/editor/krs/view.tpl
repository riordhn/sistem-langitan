<div class="center_title_bar">Report KRS</div>
{$boleh_krs=0}
{$sudah_krs=0}
{$sudah_app=0}
{$belum_app=0}
{$belum_krs=0}
{$sudah_krs_persen=0}
{$belum_krs_persen=0}
{$jumlah=0}
{if isset($prodi)}
<table>
	<tr>
		<th style="text-align:center">Prodi</th>
		<th style="text-align:center">Boleh KRS</th>
        <th style="text-align:center">Sudah KRS</th>
        <th style="text-align:center">Sudah Di Approve</th>
        <th style="text-align:center">Belum Di Approve</th>
        <th style="text-align:center">Belum KRS</th>
        <th style="text-align:center">Sudah KRS (%)</th>
        <th style="text-align:center">Belum KRS (%)</th>
	</tr>
	{foreach $prodi as $data}
	<tr>
		<td>{$data.NM_PROGRAM_STUDI} ({$data.NM_JENJANG})</td>
		<td style="text-align:center">{$data.BOLEH_KRS}</td>
        <td style="text-align:center">{$data.SUDAH_KRS}</td>
        <td style="text-align:center">{$data.SUDAH_APP}</td>
        <td style="text-align:center">{$data.BELUM_APP}</td>
        <td style="text-align:center">{$data.BELUM_KRS}</td>
        <td style="text-align:center">{$data.SUDAH_KRS_PERSEN}</td>
        <td style="text-align:center">{$data.BELUM_KRS_PERSEN}</td>
	</tr>
    	{$boleh_krs = $boleh_krs + $data.BOLEH_KRS}
        {$sudah_krs = $sudah_krs + $data.SUDAH_KRS}
        {$sudah_app = $sudah_app + $data.SUDAH_APP}
        {$belum_app = $belum_app + $data.BELUM_APP}
        {$belum_krs = $belum_krs + $data.BELUM_KRS}
        {$sudah_krs_persen = $sudah_krs_persen + $data.SUDAH_KRS_PERSEN}
        {$belum_krs_persen = $belum_krs_persen + $data.BELUM_KRS_PERSEN}
        {$jumlah = $jumlah +1}
	{/foreach}
    {$sudah_krs_persen = $sudah_krs_persen/$jumlah}
    {$belum_krs_persen = $belum_krs_persen/$jumlah}
    <tr>
    	<th>TOTAL</th>
        <th style="text-align:center">{$boleh_krs}</th>
        <th style="text-align:center">{$sudah_krs}</th>
        <th style="text-align:center">{$sudah_app}</th>
        <th style="text-align:center">{$belum_app}</th>
        <th style="text-align:center">{$belum_krs}</th>
        <th style="text-align:center">{$sudah_krs_persen|string_format:"%.0f"}%</th>
        <th style="text-align:center">{$belum_krs_persen|string_format:"%.0f"}%</th>
    </tr></table>
{else}
<table>
	<tr>
		<th style="text-align:center">Fakultas</th>
		<th style="text-align:center">Boleh KRS</th>
        <th style="text-align:center">Sudah KRS</th>
        <th style="text-align:center">Sudah Di Approve</th>
        <th style="text-align:center">Belum Di Approve</th>
        <th style="text-align:center">Belum KRS</th>
        <th style="text-align:center">Sudah KRS (%)</th>
        <th style="text-align:center">Belum KRS (%)</th>
	</tr>
	{foreach $fakultas as $data}
	<tr>
		<td><a href="editor-krs.php?id_fakultas={$data.ID_FAKULTAS}">{$data.NM_FAKULTAS}</a></td>
		<td style="text-align:center">{$data.BOLEH_KRS}</td>
        <td style="text-align:center">{$data.SUDAH_KRS}</td>
        <td style="text-align:center">{$data.SUDAH_APP}</td>
        <td style="text-align:center">{$data.BELUM_APP}</td>
        <td style="text-align:center">{$data.BELUM_KRS}</td>
        <td style="text-align:center">{$data.SUDAH_KRS_PERSEN}</td>
        <td style="text-align:center">{$data.BELUM_KRS_PERSEN}</td>
	</tr>
    	{$boleh_krs = $boleh_krs + $data.BOLEH_KRS}
        {$sudah_krs = $sudah_krs + $data.SUDAH_KRS}
        {$sudah_app = $sudah_app + $data.SUDAH_APP}
        {$belum_app = $belum_app + $data.BELUM_APP}
        {$belum_krs = $belum_krs + $data.BELUM_KRS}
        {$sudah_krs_persen = $sudah_krs_persen + $data.SUDAH_KRS_PERSEN}
        {$belum_krs_persen = $belum_krs_persen + $data.BELUM_KRS_PERSEN}
        {$jumlah = $jumlah +1}
	{/foreach}
    {$sudah_krs_persen = $sudah_krs_persen/$jumlah}
    {$belum_krs_persen = $belum_krs_persen/$jumlah}
    <tr>
    	<th>TOTAL</th>
        <th style="text-align:center">{$boleh_krs}</th>
        <th style="text-align:center">{$sudah_krs}</th>
        <th style="text-align:center">{$sudah_app}</th>
        <th style="text-align:center">{$belum_app}</th>
        <th style="text-align:center">{$belum_krs}</th>
        <th style="text-align:center">{$sudah_krs_persen|string_format:"%.0f"}%</th>
        <th style="text-align:center">{$belum_krs_persen|string_format:"%.0f"}%</th>
    </tr>
</table>
{/if}