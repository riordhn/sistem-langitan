{literal}
<script>
	function CheckAll()
	{
		$('input:checkbox').each(function(i) {
			this.checked = true;
		});
	}
</script>
{/literal}
<div class="center_title_bar">Validasi Data Dosen yang Belum Memiliki Homebase Fakultas dan Prodi</div>
<span style="cursor:pointer" onclick="CheckAll(); return false;">Check All</span>
	<form action="editor-noprodi.php" method="post">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#333333"><font color="#FFFFFF">No</font></td>
			<td bgcolor="#333333"><font color="#FFFFFF">NIP/NIK</font></td>
			<td bgcolor="#333333"><font color="#FFFFFF">Nama Dosen</font></td>
			<td bgcolor="#333333"><font color="#FFFFFF">Aksi</font></td>
		</tr>
			{foreach item="dsn" from=$DOSEN}
		<tr id="row{$dsn.ID_PENGGUNA}">
			<td>{$dsn@index + 1}</td>
			<td>{$dsn.USERNAME}</td>
			<td>{$dsn.NM_PENGGUNA}</td>
			<td>
				<input type="checkbox" name="id_penggunas[]" value="{$dsn.ID_PENGGUNA}">
			</td>
		</tr>
			{foreachelse}
        <tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
		<tr><td colspan="4"><input type="submit" value="Simpan" /></td></tr>
	</table>
	</form>