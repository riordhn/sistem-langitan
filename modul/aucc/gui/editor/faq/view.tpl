<div class="center_title_bar">Editor FAQ</div>

{if $result}<h2>{$result}</h2>{/if}
<table>
	<tr>
		<th>No</th>
		<th>Pertanyaan</th>
		<th>Jawaban</th>
		<th>Action</th>
	</tr>
	{foreach $faq_set as $f}
	<tr>
		<td class="center">{$f@index + 1}</td>
		<td>{htmlentities($f.PERTANYAAN)}</td>
		<td>{htmlentities($f.JAWABAN)}</td>
		<td class="center">
			<a href="editor-faq.php?mode=edit&id_faq={$f.ID_FAQ}">Edit</a> |
			{if $f.IS_PUBLISH}<a href="editor-faq.php?mode=unpublish&id_faq={$f.ID_FAQ}">Un-Publish</a>{else}<a href="editor-faq.php?mode=publish&id_faq={$f.ID_FAQ}">Publish</a>{/if}
		</td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="4" class="center"><a href="editor-faq.php?mode=add">Tambah FAQ</a></td>
	</tr>
</table>