<div class="center_title_bar">Editor FAQ</div>

{if $result}<h2>{$result}</h2>{/if}

<form action="editor-faq.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_faq" value="{$faq.ID_FAQ}" />
<table>
	<tr>
		<th colspan="2">Detail FAQ : Edit</th>
	</tr>
	<tr>
		<td>Pertanyaan</td>
		<td>
			<textarea name="pertanyaan" cols="100" rows="5" style="font-family: 'Trebuchet MS'; font-size: 13px">{$faq.PERTANYAAN}</textarea>
		</td>
	</tr>
	<tr>
		<td>Jawaban</td>
		<td>
			<textarea name="jawaban" cols="100" rows="10" style="font-family: 'Trebuchet MS'; font-size: 13px">{$faq.JAWABAN}</textarea>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="center">
			<a href="editor-faq.php">Kembali</a>
			<input type="submit" value="Simpan"/>
		</td>
	</tr>
</table>
</form>