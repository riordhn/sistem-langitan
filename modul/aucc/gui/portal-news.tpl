	<script>
		$(function() {
			$( "#tabs" ).tabs();
		});
	</script>
	
	<script>
	$(function() {
		$( "#tabs" ).tabs({
			ajaxOptions: {
				error: function( xhr, status, index, anchor ) {
					$( anchor.hash ).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo." );
				}
			}
		});
	});
	</script>
	
	<script>
		$(function(){
				$("#submit").buttonset();
				$('#MyFormAdd').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
							$('#result').show();
							$('#MyFormAdd').hide();
							
							var judul_div = document.getElementById('judul_div');
							var isi_div = document.getElementById('isi_div');
							
							judul_div.innerHTML = document.getElementById('judul').value;
							isi_div.innerHTML = document.getElementById('isi').value;

						}
					})
					return false;
				});
		});
	</script>
	
	<script>
		$(function() {
		
			{for $foo=0 to $jml_data_news-1}
			
				// Button Set
				$( "#submit{$id_news[$foo]}").buttonset();
				$("#btnEdit{$id_news[$foo]}").button({
					icons: {
						primary: "ui-icon-pencil",
					},
					text: true
				});
				
				$("#btnDelete{$id_news[$foo]}").button({
					icons: {
						primary: "ui-icon-trash",
					},
					text: true
				});
				
				$("#btnApprove{$id_news[$foo]}").button({
					icons: {
						primary: "ui-icon-check",
					},
					text: true
				});
				
				// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
				// Modal
				
				
				// Event Click
				$('#btnEdit{$id_news[$foo]}').click(function(){		
					$('#judul_news_div{$id_news[$foo]}').hide();
					$('#link_news_div{$id_news[$foo]}').hide();
					$('#image_link_news_div{$id_news[$foo]}').hide();
					$('#isi_news_div{$id_news[$foo]}').hide();
					$('#resume_news_div{$id_news[$foo]}').hide();
					
					$('#judul_news{$id_news[$foo]}').show();					
					$('#link_news{$id_news[$foo]}').show();
					$('#image_link_news{$id_news[$foo]}').show();
					$('#isi_news{$id_news[$foo]}').show();
					$('#resume_news{$id_news[$foo]}').show();
					
					$('#btnEdit{$id_news[$foo]}').hide();
					$('#submit{$id_news[$foo]}').show();
				});
				/*
				$('#btnDelete{$id_news[$foo]}').click(function(){		
					$( "#dialog{$id_news[$foo]}:ui-dialog" ).dialog( "destroy" );
					$( "#dialog-confirm{$id_news[$foo]}" ).dialog({
						resizable: false,
						height:140,
						modal: true,
						buttons: {
							"Delete": function() {
								//$("#dialog-confirm{$id_news[$foo]}").dialog( "destroy" );
								$('#btnDelete{$id_news[$foo]}').click(function() {
								  $('#MyFormDelete{$id_news[$foo]}').submit();
								});
								//$("#del_news{$id_news[$foo]}").hide();

								//alert("Data Berhasil Dihapus");
							},
							Cancel: function() {
								$("#dialog-confirm{$id_news[$foo]}").dialog( "destroy" );
							}
						}
					});
				});
				*/
				
				// Event Submit
				$('#MyForm{$id_news[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
						$('#judul_news_div{$id_news[$foo]}').show();
						$('#link_news_div{$id_news[$foo]}').show();
						$('#image_link_news_div{$id_news[$foo]}').show();
						$('#isi_news_div{$id_news[$foo]}').show();
						$('#resume_news_div{$id_news[$foo]}').show();
						
						$('#resume_news{$id_news[$foo]}').hide();
						$('#judul_news{$id_news[$foo]}').hide();					
						$('#link_news{$id_news[$foo]}').hide();
						$('#image_link_news{$id_news[$foo]}').hide();
						$('#isi_news{$id_news[$foo]}').hide();
						
						$('#btnEdit{$id_news[$foo]}').show();
						$('#submit{$id_news[$foo]}').hide();
							
						var judul_news_div = document.getElementById('judul_news_div{$id_news[$foo]}');
						var isi_news = document.getElementById('isi_news{$id_news[$foo]}');
						var resume_news = document.getElementById('resume_news{$id_news[$foo]}');
						
						var judul_news_div = document.getElementById('judul_news_div{$id_news[$foo]}');
						var link_news_div = document.getElementById('link_news_div{$id_news[$foo]}');
						var image_link_news_div = document.getElementById('image_link_news_div{$id_news[$foo]}');
						var resume_news_div = document.getElementById('resume_news_div{$id_news[$foo]}');
						var isi_news_div = document.getElementById('isi_news_div{$id_news[$foo]}');
						
						judul_news_div.innerHTML = document.getElementById('judul_news{$id_news[$foo]}').value;
						link_news_div.innerHTML = document.getElementById('link_news{$id_news[$foo]}').value;
						image_link_news_div.innerHTML = document.getElementById('image_link_news{$id_news[$foo]}').value;
						resume_news_div.innerHTML = document.getElementById('resume_news{$id_news[$foo]}').value;
						isi_news_div.innerHTML = document.getElementById('isi_news{$id_news[$foo]}').value;

						}
					})
					return false;
				});
				
				$('#MyFormDelete{$id_news[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
								//$("#dialog-confirm{$id_news[$foo]}").dialog( "destroy" );
								//$('#btnDelete{$id_news[$foo]}').click(function() {
								//$('#MyFormDelete{$id_news[$foo]}').submit();
								//});
								$("#del_news{$id_news[$foo]}").hide();

								alert("Data Berhasil Dihapus");
							
						}
					})
					return false;
				});
				
				$('#MyFormApprove{$id_news[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
								//$("#dialog-confirm{$id_news[$foo]}").dialog( "destroy" );
								//$('#btnDelete{$id_news[$foo]}').click(function() {
								//$('#MyFormDelete{$id_news[$foo]}').submit();
								//});
								$("#approve_news{$id_news[$foo]}").hide();

								alert("Data Berhasil Diapprove");
							
						}
					})
					return false;
				});
				
			{/for}
		
		});
	</script>
	<!-- style news -->
	<script>
		$(function() {
			$( ".column-news" ).sortable({
				connectWith: ".column-news"
			});

			$( ".portlet-news" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
				.find( ".portlet-header-news" )
					.addClass( "ui-widget-header ui-corner-all" )
					.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
					.end()
				.find( ".portlet-content-news" );

			$( ".portlet-header-news .ui-icon" ).click(function() {
				$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
				$( this ).parents( ".portlet-news:first" ).find( ".portlet-content-news" ).toggle();
			});

			//$( ".column" ).disableSelection();
			
		});

	</script>
	<!-- end style -->


<div id="tabs">
	<ul>
		<li><a class="disable-ajax" href="portal-edit-news.php">Editor news</a></li>
		<li><a class="disable-ajax" href="portal-add-news.php">Add news</a></li>
		<li><a class="disable-ajax" href="portal-delete-news.php">Delete news</a></li>
		<li><a class="disable-ajax" href="portal-incomming-news.php">Incomming news</a></li>
	</ul>
	
	<!--
	<div id="tabs-1">
	</div>
	<div id="tabs-2">
	</div>

	<div id="tabs-3">
	<!--
		<div class="column ui-widget">
			<div class="portlet-news">
				<header>
					<div class="ui-widget-header">All news</div>
				</header>
				<div class="portlet-content-news">
					{for $foo=0 to $jml_data_news-1}
						{if $is_approve[$foo]==1}
					<div id="del_news{$id_news[$foo]}">
					<div style="width:300px; display:none;" id="dialog-confirm{$id_news[$foo]}" title="Delete : {$judul_news[$foo]}???">
						<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
					</div>
					
						<form id="MyFormDelete{$id_news[$foo]}" name="MyFormDelete{$id_news[$foo]}" method="post" action="portal-del-news.php">
						<TEXTAREA id="id_news" style="display:none;" NAME="id_news" COLS=30 ROWS=6>{$id_news[$foo]}</TEXTAREA>
						<div class="title disable-ajax">
							<a  href="{$link_news[$foo]}" target="_blank" class="disable-ajax" >{$judul_news[$foo]}</a>
						</div>
						<div class="date">
							Date : {$tgl_news[$foo]}
						</div>
						<div class="resume">
							<table class="ui-widget" width="100%">
								<tbody class="ui-widget-content">
								<tr>
									<td style="width:100px;"><img style="width:100px; height:100px;" src="{$img_link_news[$foo]}"/></td>
									<td style="width:100%" valign="top">{$resume_news[$foo]}</td>
								</tr>
								<tr>
									<td style="width:100px;"></td>
									<td style="width:100%"valign="top">
										<div id="btnDelete{$id_news[$foo]}">
											<input type="submit" value="Hapus" />
										</div>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
						</form>
						</div>
						{/if}
					{/for}
				</div>
			</div>
		</div>

	</div>
	
	<div id="tabs-4">
			<!--
		<div class="column ui-widget">

			<div class="portlet-news">
				<header>
					<div class="ui-widget-header">All news</div>
				</header>
				<div class="portlet-content-news">
					{for $foo=0 to $jml_data_news-1}
						{if $is_approve[$foo]==0 && $is_active[$foo]==1}
					<div id="approve_news{$id_news[$foo]}">
					<div style="width:300px; display:none;" id="dialog-confirm{$id_news[$foo]}" title="Delete : {$judul_news[$foo]}???">
						<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
					</div>
					
						<form id="MyFormApprove{$id_news[$foo]}" name="MyFormApprove{$id_news[$foo]}" method="post" action="portal-app-news.php">
						<TEXTAREA id="id_news" style="display:none;" NAME="id_news" COLS=30 ROWS=6>{$id_news[$foo]}</TEXTAREA>
						<div class="title disable-ajax">
							<a  href="{$link_news[$foo]}" target="_blank" class="disable-ajax" >{$judul_news[$foo]}</a>
						</div>
						<div class="date">
							Date : {$tgl_news[$foo]}
						</div>
						<div class="resume">
							<table class="ui-widget" width="100%">
								<tbody class="ui-widget-content">
								<tr>
									<td style="width:100px;"><img style="width:100px; height:100px;" src="{$img_link_news[$foo]}"/></td>
									<td style="width:100%" valign="top">{$resume_news[$foo]}</td>
								</tr>
								<tr>
									<td style="width:100px;"></td>
									<td style="width:100%"valign="top">
										<div id="btnApprove{$id_news[$foo]}">
											<input type="submit" value="Approve" />
										</div>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
						</form>
						</div>
						{/if}
					{/for}
				</div>
			</div>

		</div>
	</div>
	-->
</div>