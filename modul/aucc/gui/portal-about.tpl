<script>

	$(function() {

		$( ".portlet-about" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header-about" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
				.end()
			.find( ".portlet-content-about" );

		$( ".portlet-header-about .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet-about:first" ).find( ".portlet-content-about" ).toggle();
		});
		
		// Event Click
		$('#edit_icon').click(function(){		
			$('#about_div').hide();
			$('#edit_icon').hide();
			
			$('#about').show();
			$('#submit').show();
			$('#btnCancel').show();

		});
		
		$('#btnCancel').click(function(){		
			$('#about_div').show();
			$('#edit_icon').show();
			
			$('#about').hide();
			$('#submit').hide();
			$('#btnCancel').hide();

		});
		
		// Event Submit
		/*
		$('#MyFormAbout').submit(function() {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
				
				$('#about_div').show();
				$('#edit_icon').show();
				
				$('#about').hide();
				$('#submit').hide();
					
				var about_div = document.getElementById('about_div');
				
				about_div.innerHTML = document.getElementById('about_edit').value;

				}
			})
			//return false;
		});
		*/
				
	});
	
	function setup() {
	   tinyMCE.init({
		  mode : "textareas",
		  theme : "advanced",
		  plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		  theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		  theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		  theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		  theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
		  theme_advanced_toolbar_location : "top",
		  theme_advanced_toolbar_align : "left",
		  theme_advanced_statusbar_location : "bottom",
		  theme_advanced_resizing : true
	   });
	}

</script>


<script type="text/javascript">

</script>

<div class="ui-widget">
	<div class="column-about">
	<div class="portlet-about">
		<div class="ui-widget-header " style="padding:3px; padding-left:10px;">About Cyber Campus<div id="arrowPanel" style="float:right;" class="ui-icon ui-icon-arrow-4-diag"></div><div id="edit_icon" style="float:right;" class="ui-icon ui-icon-pencil" title="edit">edit</div></div> 

		
			<div class="portlet-content-about" style="padding:10px;">
				
				<div id="about_div">
					{$about}
				</div>
				
					<div id="about" style="display:none; width:100%; height:auto;">
						<form id="MyFormAbout" name="MyFormAbout" method="post" action="portal-about-update.php">
						<textarea  id="about_edit" name="about_edit" cols=80 rows=10 style="text-align:justify;">{$about}</textarea>
						<input value="{$id_cybercampus}" type="text" id="id_cybercampus" name="id_cybercampus" cols=30 rows=30 style="width:100%; display:none; min-height:500px;text-align:justify;">
						<input type="text" value="{$id_pengguna}" id="id_pengguna" name="id_pengguna" cols=30 rows=30 style="width:100%; display:none; min-height:500px;text-align:justify;">
						<input id="submit" type="submit" name="submit" id="submit" value="Submit">
						<input type="button" id="btnCancel" name="btnCancel" value="Cancel" />
						<a class="disable-ajax" href="javascript:setup();">Load Editor</a>
						</form>
					</div>
					

			</div>
			<span class="ui-icon ui-icon-grip-diagonal-se" style="float:right;"></span>		

	</div>
	</div>
</div>
					
