<form method="post" action="cek-nim.php">
    <table style="border: 1px #000 solid;border-collapse: collapse;">
        <tr>
            <td style="border: 1px #000 solid;padding :5px;">NIM/NAMA Mahasiswa</td>
            <td style="border: 1px #000 solid;padding :5px;"><input type="text" name="nim"/></td>
            <td style="border: 1px #000 solid;padding :5px;"><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if isset($data_mahasiswa)}
    {if $data_mahasiswa.NIM_MHS==null}
        <span>Data Mahasiswa tidak ada</span>
    {else}
        <table style="border: 1px #000 solid;border-collapse: collapse;">
            <tr>
                <th style="border: 1px #000 solid;padding :5px;">Kolom</th>
                <th style="border: 1px #000 solid;padding :5px;">Data</th>
            </tr>
            <tr>
                <td style="border: 1px #000 solid;padding :5px;">NIM</td>
                <td style="border: 1px #000 solid;padding :5px;">{$data_mahasiswa.NIM_MHS}</td>
            </tr>
            <tr>
                <td style="border: 1px #000 solid;padding :5px;">Nama</td>
                <td style="border: 1px #000 solid;padding :5px;">{$data_mahasiswa.NM_PENGGUNA}</td>
            </tr>
            <tr>
                <td style="border: 1px #000 solid;padding :5px;">Fakultas</td>
                <td style="border: 1px #000 solid;padding :5px;">{$data_mahasiswa.NM_FAKULTAS}</td>
            </tr>
            <tr>
                <td style="border: 1px #000 solid;padding :5px;">Prodi</td>
                <td style="border: 1px #000 solid;padding :5px;">{$data_mahasiswa.NM_PROGRAM_STUDI}</td>
            </tr>
            <tr>
                <td style="border: 1px #000 solid;padding :5px;">Alamat</td>
                <td style="border: 1px #000 solid;padding :5px;">{$data_mahasiswa.ALAMAT_MHS}</td>
            </tr>
            <tr>
                <td style="border: 1px #000 solid;padding :5px;">No Ujian</td>
                <td style="border: 1px #000 solid;padding :5px;">{$data_mahasiswa.NO_UJIAN}</td>
            </tr>
        </table>

    {/if}
{/if}