<div class="center_title_bar">Update Pembayaran Maba</div> 
<form method="post" action="update-pembayaran-cmhs.php">
    <table>
        <tr>
            <td>No Ujian</td>
            <td><input type="text" name="no_ujian"/></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if isset($cari_no_ujian)}
    <table class="detail">
        <tr>
            <th colspan="2" style="text-align: center;">Biodata</th>
        </tr>
        <tr>
            <td>No Ujian</td>
            <td>{$data_pembayaran[0]['NO_UJIAN']}</td>            
        </tr>
        <tr>
            <td>Nama</td>
            <td>{$data_pembayaran[0]['NM_C_MHS']}</td>            
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>{$data_pembayaran[0]['NM_PROGRAM_STUDI']}</td>            
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>{$data_pembayaran[0]['NM_FAKULTAS']}</td>            
        </tr>
    </table>
    <p></p>
    <form action="update-pembayaran-cmhs.php?mode=biaya_detail" method="post">
        <table>
            <tr>
                <th colspan="10" style="text-align: center;">Update Biaya Calon Mahasiswa Detail</th>
            </tr>
            <tr>
                <th>Nama Biaya</th>
                <th>Besar Biaya</th>
                <th>Is Tagih</th>
                <th>Nama Bank</th>
                <th>Via Bank</th>
                <th>Tanggal Bayar</th>
                <th>No Transaksi</th>
				<th>Keterangan</th>
            </tr>
            {$index=1}
            {foreach $data_pembayaran as $data}
                {if $data['NM_BIAYA']}
                    <tr>
                        <td>
                            <input type="hidden" name="id{$index}" value="{$data['ID_PEMBAYARAN_CMHS']}"/>
                            {$data['NM_BIAYA']}
                        </td>
                        <td>
                            <input type="text" size="8" name="besar_biaya{$index}" value="{$data['BESAR_BIAYA']}" />
                        </td>
                        <td>
                            <select name="is_tagih{$index}">
                                <option value="Y" {if $data['IS_TAGIH']=='Y'}selected="true"{/if}>Y</option>
                                <option value="T" {if $data['IS_TAGIH']=='T'}selected="true"{/if}>T</option>
                            </select>
                        </td>
                        <td>
                            <select name="bank{$index}">
                                <option value=""></option>
                                {foreach $data_bank as $bank}
                                    <option value="{$bank['ID_BANK']}" {if $bank['ID_BANK']==$data['ID_BANK']}selected="true"{/if}>{$bank['NM_BANK']}</option>
                                {/foreach}
                            </select>
                        </td>
                        <td>
                            <select name="bank_via{$index}">
                                <option value=""></option>
                                {foreach $data_bank_via as $bank_via}
                                    <option value="{$bank_via['ID_BANK_VIA']}" {if $bank_via['ID_BANK_VIA']==$data['ID_BANK_VIA']}selected="true"{/if}>{$bank_via['NAMA_BANK_VIA']}</option>
                                {/foreach}
                            </select>
                        </td>
                        <td>
                            <input type="text" class="date" name="tgl_bayar{$index}" size="7" value="{$data['TGL_BAYAR']}" />
                        </td>
                        <td>
                            <input type="text" name="no_transaksi{$index}" size="10" value="{$data['NO_TRANSAKSI']}" />
                        </td>
						<td>
							<input type="text" name="keterangan{$index++}" size="10" value="{$data['KETERANGAN']}" />
						</td>
                    </tr>
                {else}
                    <tr>
                        <td colspan="7" style="text-align: center;">Data Pembayaran Kosong</td>
                    </tr>
                {/if}
            {/foreach}
            <tr>
                <td colspan="8">
                    <input type="hidden" name="no_ujian" value="{$data_pembayaran[0]['NO_UJIAN']}"/>
                    <input type="hidden" name="id_c_mhs" value="{$data_pembayaran[0]['ID_C_MHS']}"/>
                    <input type="hidden" name="jumlah" value="{$index}"/>
                    <input style="float: right;" type="submit" value="Update"/>
                </td>
            </tr>
        </table>
    </form>
	<form action="update-pembayaran-cmhs.php?mode=biaya_all" method="post">
        <table>
            <tr>
                <th colspan="8">Update Biaya Calon Mahasiswa All</th>
            </tr>
            <tr>
				<th>Is Tagih</th>
                <th>Nama Bank</th>
                <th>Via Bank</th>
                <th>Tanggal Bayar</th>
                <th>No Transaksi</th>
                <th>Keterangan</th>
                <th>Proses</th>
            </tr>
            <tr>
				<td>
					<select name="is_tagih">
						<option value="Y" {if $data_pembayaran[0]['IS_TAGIH']=='Y'}selected="true"{/if}>Y</option>
						<option value="T" {if $data_pembayaran[0]['IS_TAGIH']=='T'}selected="true"{/if}>T</option>
					</select>
				</td>
				<td>
                    <select name="bank">
                        <option value=""></option>
                        {foreach $data_bank as $data}
                            <option value="{$data['ID_BANK']}" {if $data['ID_BANK']==$data_pembayaran[0]['ID_BNK']}selected="true"{/if}>{$data['NM_BANK']}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <select name="bank_via">
                        <option value=""></option>
                        {foreach $data_bank_via as $data}
                            <option value="{$data['ID_BANK_VIA']}" {if $data['ID_BANK_VIA']==$data_pembayaran[0]['ID_BNKV']}selected="true"{/if}>{$data['NAMA_BANK_VIA']}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <input type="text" class="date" name="tgl_bayar" size="7" value="{$data_pembayaran[0]['TGL_BAYAR']}" />
                </td>
                <td>
                    <input type="text" name="no_transaksi" size="15" value="{$data_pembayaran[0]['NO_TRANSAKSI']}" />
                </td>
                <td>
                    <input type="text" name="keterangan" size="15" value="{$data_pembayaran[0]['KETERANGAN']}" />
                </td>
                <td>
					<input type="hidden" name="no_ujian" value="{$data_pembayaran[0]['NO_UJIAN']}"/>                    
					<input type="hidden" name="id_c_mhs" value="{$data_pembayaran[0]['ID_C_MHS']}"/>
                    <input type="submit" value="Update"/>
                </td>
            </tr>
        </table>
{/if}
{literal}
    <script>
            $(function() {
                    $( ".date" ).datepicker({dateFormat:'dd-M-yy'});
            });
    </script>
{/literal}