	<script>
		$(function() {
			$( "#tabs" ).tabs();
		});
	</script>
	
	<script>
	$(function() {
		$( "#tabs" ).tabs({
			ajaxOptions: {
				error: function( xhr, status, index, anchor ) {
					$( anchor.hash ).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo." );
				}
			}
		});
	});
	</script>
	
	<script>
		$(function(){
				$("#submit").buttonset();
				$('#MyFormAdd').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
							$('#result').show();
							$('#MyFormAdd').hide();
							
							var judul_div = document.getElementById('judul_div');
							var isi_div = document.getElementById('isi_div');
							
							judul_div.innerHTML = document.getElementById('judul').value;
							isi_div.innerHTML = document.getElementById('isi').value;

						}
					})
					return false;
				});
		});
	</script>
	
	<script>
		$(function() {
		
			{for $foo=0 to $jml_data_article-1}
			
				// Button Set
				$( "#submit{$id_article[$foo]}").buttonset();
				$("#btnEdit{$id_article[$foo]}").button({
					icons: {
						primary: "ui-icon-pencil",
					},
					text: true
				});
				
				$("#btnDelete{$id_article[$foo]}").button({
					icons: {
						primary: "ui-icon-trash",
					},
					text: true
				});
				
				$("#btnApprove{$id_article[$foo]}").button({
					icons: {
						primary: "ui-icon-check",
					},
					text: true
				});
				
				// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
				// Modal
				
				
				// Event Click
				$('#btnEdit{$id_article[$foo]}').click(function(){		
					$('#judul_article_div{$id_article[$foo]}').hide();
					$('#link_article_div{$id_article[$foo]}').hide();
					$('#image_link_article_div{$id_article[$foo]}').hide();
					$('#isi_article_div{$id_article[$foo]}').hide();
					$('#resume_article_div{$id_article[$foo]}').hide();
					
					$('#judul_article{$id_article[$foo]}').show();					
					$('#link_article{$id_article[$foo]}').show();
					$('#image_link_article{$id_article[$foo]}').show();
					$('#isi_article{$id_article[$foo]}').show();
					$('#resume_article{$id_article[$foo]}').show();
					
					$('#btnEdit{$id_article[$foo]}').hide();
					$('#submit{$id_article[$foo]}').show();
				});
				/*
				$('#btnDelete{$id_article[$foo]}').click(function(){		
					$( "#dialog{$id_article[$foo]}:ui-dialog" ).dialog( "destroy" );
					$( "#dialog-confirm{$id_article[$foo]}" ).dialog({
						resizable: false,
						height:140,
						modal: true,
						buttons: {
							"Delete": function() {
								//$("#dialog-confirm{$id_article[$foo]}").dialog( "destroy" );
								$('#btnDelete{$id_article[$foo]}').click(function() {
								  $('#MyFormDelete{$id_article[$foo]}').submit();
								});
								//$("#del_article{$id_article[$foo]}").hide();

								//alert("Data Berhasil Dihapus");
							},
							Cancel: function() {
								$("#dialog-confirm{$id_article[$foo]}").dialog( "destroy" );
							}
						}
					});
				});
				*/
				
				// Event Submit
				$('#MyForm{$id_article[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
						$('#judul_article_div{$id_article[$foo]}').show();
						$('#link_article_div{$id_article[$foo]}').show();
						$('#image_link_article_div{$id_article[$foo]}').show();
						$('#isi_article_div{$id_article[$foo]}').show();
						$('#resume_article_div{$id_article[$foo]}').show();
						
						$('#resume_article{$id_article[$foo]}').hide();
						$('#judul_article{$id_article[$foo]}').hide();					
						$('#link_article{$id_article[$foo]}').hide();
						$('#image_link_article{$id_article[$foo]}').hide();
						$('#isi_article{$id_article[$foo]}').hide();
						
						$('#btnEdit{$id_article[$foo]}').show();
						$('#submit{$id_article[$foo]}').hide();
							
						var judul_article_div = document.getElementById('judul_article_div{$id_article[$foo]}');
						var isi_article = document.getElementById('isi_article{$id_article[$foo]}');
						var resume_article = document.getElementById('resume_article{$id_article[$foo]}');
						
						var judul_article_div = document.getElementById('judul_article_div{$id_article[$foo]}');
						var link_article_div = document.getElementById('link_article_div{$id_article[$foo]}');
						var image_link_article_div = document.getElementById('image_link_article_div{$id_article[$foo]}');
						var resume_article_div = document.getElementById('resume_article_div{$id_article[$foo]}');
						var isi_article_div = document.getElementById('isi_article_div{$id_article[$foo]}');
						
						judul_article_div.innerHTML = document.getElementById('judul_article{$id_article[$foo]}').value;
						link_article_div.innerHTML = document.getElementById('link_article{$id_article[$foo]}').value;
						image_link_article_div.innerHTML = document.getElementById('image_link_article{$id_article[$foo]}').value;
						resume_article_div.innerHTML = document.getElementById('resume_article{$id_article[$foo]}').value;
						isi_article_div.innerHTML = document.getElementById('isi_article{$id_article[$foo]}').value;

						}
					})
					return false;
				});
				
				$('#MyFormDelete{$id_article[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
								//$("#dialog-confirm{$id_article[$foo]}").dialog( "destroy" );
								//$('#btnDelete{$id_article[$foo]}').click(function() {
								//$('#MyFormDelete{$id_article[$foo]}').submit();
								//});
								$("#del_article{$id_article[$foo]}").hide();

								alert("Data Berhasil Dihapus");
							
						}
					})
					return false;
				});
				
				$('#MyFormApprove{$id_article[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
								//$("#dialog-confirm{$id_article[$foo]}").dialog( "destroy" );
								//$('#btnDelete{$id_article[$foo]}').click(function() {
								//$('#MyFormDelete{$id_article[$foo]}').submit();
								//});
								$("#approve_article{$id_article[$foo]}").hide();

								alert("Data Berhasil Diapprove");
							
						}
					})
					return false;
				});
				
			{/for}
		
		});
	</script>
	<!-- style article -->
	<script>
		$(function() {
			$( ".column-article" ).sortable({
				connectWith: ".column-article"
			});

			$( ".portlet-article" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
				.find( ".portlet-header-article" )
					.addClass( "ui-widget-header ui-corner-all" )
					.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
					.end()
				.find( ".portlet-content-article" );

			$( ".portlet-header-article .ui-icon" ).click(function() {
				$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
				$( this ).parents( ".portlet-article:first" ).find( ".portlet-content-article" ).toggle();
			});

			//$( ".column" ).disableSelection();
			
		});

	</script>
	<!-- end style -->


<div id="tabs">
	<ul>
		<li><a class="disable-ajax" href="portal-edit-article.php">Editor article</a></li>
		<li><a class="disable-ajax" href="portal-add-article.php">Add article</a></li>
		<li><a class="disable-ajax" href="portal-delete-article.php">Delete article</a></li>
		<li><a class="disable-ajax" href="portal-incomming-article.php">Incomming article</a></li>
	</ul>
	
	<!--
	<div id="tabs-1">
	</div>
	<div id="tabs-2">
	</div>

	<div id="tabs-3">
	<!--
		<div class="column ui-widget">
			<div class="portlet-article">
				<header>
					<div class="ui-widget-header">All article</div>
				</header>
				<div class="portlet-content-article">
					{for $foo=0 to $jml_data_article-1}
						{if $is_approve[$foo]==1}
					<div id="del_article{$id_article[$foo]}">
					<div style="width:300px; display:none;" id="dialog-confirm{$id_article[$foo]}" title="Delete : {$judul_article[$foo]}???">
						<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
					</div>
					
						<form id="MyFormDelete{$id_article[$foo]}" name="MyFormDelete{$id_article[$foo]}" method="post" action="portal-del-article.php">
						<TEXTAREA id="id_article" style="display:none;" NAME="id_article" COLS=30 ROWS=6>{$id_article[$foo]}</TEXTAREA>
						<div class="title disable-ajax">
							<a  href="{$link_article[$foo]}" target="_blank" class="disable-ajax" >{$judul_article[$foo]}</a>
						</div>
						<div class="date">
							Date : {$tgl_article[$foo]}
						</div>
						<div class="resume">
							<table class="ui-widget" width="100%">
								<tbody class="ui-widget-content">
								<tr>
									<td style="width:100px;"><img style="width:100px; height:100px;" src="{$img_link_article[$foo]}"/></td>
									<td style="width:100%" valign="top">{$resume_article[$foo]}</td>
								</tr>
								<tr>
									<td style="width:100px;"></td>
									<td style="width:100%"valign="top">
										<div id="btnDelete{$id_article[$foo]}">
											<input type="submit" value="Hapus" />
										</div>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
						</form>
						</div>
						{/if}
					{/for}
				</div>
			</div>
		</div>

	</div>
	
	<div id="tabs-4">
			<!--
		<div class="column ui-widget">

			<div class="portlet-article">
				<header>
					<div class="ui-widget-header">All article</div>
				</header>
				<div class="portlet-content-article">
					{for $foo=0 to $jml_data_article-1}
						{if $is_approve[$foo]==0 && $is_active[$foo]==1}
					<div id="approve_article{$id_article[$foo]}">
					<div style="width:300px; display:none;" id="dialog-confirm{$id_article[$foo]}" title="Delete : {$judul_article[$foo]}???">
						<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
					</div>
					
						<form id="MyFormApprove{$id_article[$foo]}" name="MyFormApprove{$id_article[$foo]}" method="post" action="portal-app-article.php">
						<TEXTAREA id="id_article" style="display:none;" NAME="id_article" COLS=30 ROWS=6>{$id_article[$foo]}</TEXTAREA>
						<div class="title disable-ajax">
							<a  href="{$link_article[$foo]}" target="_blank" class="disable-ajax" >{$judul_article[$foo]}</a>
						</div>
						<div class="date">
							Date : {$tgl_article[$foo]}
						</div>
						<div class="resume">
							<table class="ui-widget" width="100%">
								<tbody class="ui-widget-content">
								<tr>
									<td style="width:100px;"><img style="width:100px; height:100px;" src="{$img_link_article[$foo]}"/></td>
									<td style="width:100%" valign="top">{$resume_article[$foo]}</td>
								</tr>
								<tr>
									<td style="width:100px;"></td>
									<td style="width:100%"valign="top">
										<div id="btnApprove{$id_article[$foo]}">
											<input type="submit" value="Approve" />
										</div>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
						</form>
						</div>
						{/if}
					{/for}
				</div>
			</div>

		</div>
	</div>
	-->
</div>