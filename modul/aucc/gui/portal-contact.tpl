<script>
	$(function() {

		$( ".portlet-contact" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header-contact" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
				.end()
			.find( ".portlet-content-contact" );

		$( ".portlet-header-contact .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet-contact:first" ).find( ".portlet-content-contact" ).toggle();
		});
		
		// Event Click
		$('#edit_icon').click(function(){		
			$('#contact_div').hide();
			$('#edit_icon').hide();
			
			$('#contact').show();
			$('#submit').show();
			$('#btnCancel').show();

		});
		
		$('#btnCancel').click(function(){		
			$('#contact_div').show();
			$('#edit_icon').show();
			
			$('#contact').hide();
			$('#submit').hide();
			$('#btnCancel').hide();
		});
		
		// Event Submit
		$('#MyFormcontact').submit(function() {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
				
				$('#contact_div').show();
				$('#edit_icon').show();
				
				$('#contact').hide();
				$('#submit').hide();
					
				var contact_div = document.getElementById('contact_div');
				
				contact_div.innerHTML = document.getElementById('contact_edit').value;

				}
			})
			//return false;
		});
				
	});

	function setup() {
	   tinyMCE.init({
		  mode : "textareas",
		  theme : "advanced",
		  plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		  theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		  theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		  theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		  theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
		  theme_advanced_toolbar_location : "top",
		  theme_advanced_toolbar_align : "left",
		  theme_advanced_statusbar_location : "bottom",
		  theme_advanced_resizing : true
	   });
	}	
</script>
<div class="ui-widget">
	<div class="column-contact">
	<div class="portlet-contact">
		<div class="ui-widget-header " style="padding:3px; padding-left:10px;">Contact Cyber Campus &nbsp; {$id_cybercampus}<div id="arrowPanel" style="float:right;" class="ui-icon ui-icon-arrow-4-diag"></div><div id="edit_icon" style="float:right;" class="ui-icon ui-icon-pencil" title="edit">edit</div></div> 
		
			<div class="portlet-content-contact" style="padding:10px;">
				<div id="contact_div">
					{$contact}
				</div>
					<div id="contact" style="display:none; width:100%; height:auto;">
						<form id="MyFormcontact" name="MyFormcontact" method="post" action="portal-contact-update.php">
						<textarea id="contact_edit" name="contact_edit" cols=80 rows=10 style="text-align:justify;">{$contact}</textarea>
						<input value="{$id_cybercampus}" type="text" id="id_cybercampus" name="id_cybercampus" cols=30 rows=30 style="width:100%; display:none; min-height:500px;text-align:justify;">
						<input type="text" value="{$id_pengguna}" id="id_pengguna" name="id_pengguna" cols=30 rows=30 style="width:100%; display:none; min-height:500px;text-align:justify;">
						<input id="submit" type="submit" name="submit" id="submit" value="Submit">
						<input type="button" id="btnCancel" name="btnCancel" value="Cancel" />
						<a class="disable-ajax" href="javascript:setup();">Load Editor</a>
						</form>
					</div>
			</div>
			<span class="ui-icon ui-icon-grip-diagonal-se" style="float:right;"></span>
	</div>
	</div>
</div>