<div class="center_title_bar">Pengaturan pembayaran S2 / S3 / Spesialis / Profesi</div>
{literal}<style>
    td { font-size: 90%; }
</style>{/literal}

<form action="verifikasi-biaya-s2.php" method="get">
<table>
    <tr>
        <td>Fakultas</td>
        <td>
            <select name="id_fakultas">
                <option value="">-- Semua Fakultas --</option>
            {foreach $fakultas_set as $fakultas}
                <option value="{$fakultas['ID_FAKULTAS']}"
                    {if isset($smarty.get.id_fakultas)}{if $smarty.get.id_fakultas==$fakultas['ID_FAKULTAS']}selected="selected"{/if}{/if}>{$fakultas['NM_FAKULTAS']}</option>
            {/foreach}
            </select>
            <input type="submit" value="Tampilkan" />
        </td>
    </tr>
</table>
</form>

<form action="verifikasi-biaya-s2-post.php" method="post">
<table>
    <tr>
        <th>No.</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Prodi</th>
        <th>Jalur</th>
        <th>Kelompok Biaya</th>
        <th>Aksi</th>
    </tr>
{foreach $calon_mahasiswa_set as $cm}
    <tr>
        <td>{$cm@index+1}</td>
        <td>{$cm['NO_UJIAN']}</td>
        <td>{$cm['NM_C_MHS']}</td>
        <td>{*$cm['NM_PROGRAM_STUDI']*}
            <select name="program_studi_{$cm['ID_C_MHS']}">
                {foreach $program_studi_set as $ps}
                <option value="{$ps['ID_PROGRAM_STUDI']}" {if $ps['ID_PROGRAM_STUDI'] == $cm['ID_PROGRAM_STUDI']}selected="selected"{/if}>[{str_pad($ps['ID_FAKULTAS'], 2, "0", 0)}] [{$ps['NM_JENJANG']}] {$ps['NM_PROGRAM_STUDI']}</option>
                {/foreach}
            </select>
        </td>
        <td>
            <select name="jalur_{$cm['ID_C_MHS']}">
                {foreach $jalur_set as $jalur}
                <option value="{$jalur['ID_JALUR']}" {if $jalur['ID_JALUR'] == $cm['ID_JALUR']}selected="selected"{/if}>{$jalur['NM_JALUR']}</option>
                {/foreach}
            </select>
        </td>
        <td>
            <select name="kelompok_biaya_{$cm['ID_C_MHS']}">
                <option value=""></option>
                {foreach $kelompok_biaya_set as $kb}
                <option value="{$kb['ID_KELOMPOK_BIAYA']}" {if $kb['ID_KELOMPOK_BIAYA'] == $cm['SP3']}selected="selected"{/if}>{$kb['NM_KELOMPOK_BIAYA']}</option>
                {/foreach}
            </select>
        </td>
        <td>
            <label><input type="checkbox" name="check_{$cm['ID_C_MHS']}" /></label>
        </td>
    </tr>
{/foreach}
    <tr>
        <td colspan="7" style="text-align: center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>