<div class="center_title_bar">Cek Pembayaran Maba</div> 
<form method="post" action="cek-pembayaran-cmhs.php">
    <table>
        <tr>
            <td>No UJIAN</td>
            <td><input type="text" name="no_ujian"/></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if isset($data_pembayaran)}
    <table class="detail">
        <tr>
            <th colspan="2" style="text-align: center;">Biodata</th>
        </tr>
        <tr>
            <td>No Ujian</td>
            <td>{$data_pembayaran[0]['NO_UJIAN']}</td>            
        </tr>
        <tr>
            <td>Nama</td>
            <td>{$data_pembayaran[0]['NM_C_MHS']}</td>            
        </tr>
        <tr>
            <td>Jenjang</td>
            <td>{$data_pembayaran[0]['NM_JENJANG']}</td>            
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>{$data_pembayaran[0]['NM_PROGRAM_STUDI']}</td>            
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>{$data_pembayaran[0]['NM_FAKULTAS']}</td>            
        </tr>
    </table>
    <p></p>
    <table class="detail">
        <tr>
            <th colspan="10" style="text-align: center;">Status Pembayaran</th>
        </tr>
        <tr>
            <th>Nama Biaya</th>
            <th>Besar Biaya</th>
            <th>Status Tagihan Biaya</th>
            <th>Tanggal Bayar</th>
            <th>Nama Bank</th>
            <th>Via Bank</th>
            <th>Nomer Transaksi</th>
        </tr>
        {foreach $data_pembayaran as $data}
            {if $data['NM_BIAYA']}
                <tr>
                    <td>{$data['NM_BIAYA']}</td>
                    <td style="text-align: right">{number_format($data['BESAR_BIAYA'])}</td>
                    <td>{$data['IS_TAGIH']}</td>
                    <td>{$data['TGL_BAYAR']}</td>
                    <td>{$data['NM_BANK']}</td>
                    <td>{$data['NAMA_BANK_VIA']}</td>
                    <td>{$data['NO_TRANSAKSI']}</td>
                </tr>
            {else}
                <tr>
                    <td colspan="7" style="text-align: center;">Data Pembayaran Kosong</td>
                </tr>
            {/if}

        {/foreach}
        <tr>
            <td colspan="10" style="text-align: center;">
                <p>
                    <span style="color: #007000;font-weight: bold;">TOTAL BIAYA </span><br/><strong>{number_format($data_pembayaran[0]['TOTAL_BIAYA'])}</strong><br/>
                    <span style="color: #ff3300;font-weight: bold;">DAFTAR TAGIHAN</span><br/>Biaya Pendaftaran : <strong>{if $data_pembayaran[0]['TOTAL_BIAYA_TAGIH']==null}0{else}{number_format($data_pembayaran[0]['TOTAL_BIAYA_TAGIH'])}{/if}</strong>
                </p>
            </td>
        </tr>
    </table>
{else if isset($data_kosong)}
    <span>{$data_kosong}</span>
{/if}