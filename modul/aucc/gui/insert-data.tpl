<div class="center_title_bar">Insert Data</div> 
<form method="get" action="insert-data.php">
    <table>
        <tr>
            <td>Username</td>
            <td><input type="text" name="cari_nim" value='{$smarty.get.cari_nim}'/></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if $smarty.get.cari_nim!=''}
    <form method="post" action="insert-data.php?{$smarty.server.QUERY_STRING}">
        <table>
            <tr>
                <th>Kolom</th>
                <th>Input</th>
            </tr>
            <tr>
                <td>Username</td>
                <td>{$mahasiswa.USERNAME}</td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>{$mahasiswa.NM_PENGGUNA}</td>
            </tr>
			<tr>
                <td>No. Hp. Lama</td>
                <td>{$mahasiswa.TELP}
                	<input type="hidden" name="telp_lama" value="{$mahasiswa.TELP}" />
                </td>
            </tr>
            <tr>
                <td>No. Hp. Baru</td>
                <td><input type="text" name="telp" /> </td>
            </tr>
            <tr>
				<input type="hidden" name="id_pengguna" value="{$mahasiswa.ID_PENGGUNA}"/>
                <input type="hidden" name="update" value="1"/>
                <td colspan="2"><input style="float: right;" type="submit" value="update"/></td>
            </tr>
        </table>
    </form>
{/if}
