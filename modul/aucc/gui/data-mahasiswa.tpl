<div class="center_title_bar">Pencarian Mahasiswa</div>
{if $smarty.get.mode=='' or $smarty.get.mode=='cari'}
    <form method="get" action="data-mahasiswa.php">
        <table>
            <tr>
                <th colspan="2" class="center"> Pencarian</th>
            </tr>
            <tr>
                <td>Cari Nama/NIM</td>
                <td>
                    <input type="text" size="30" name="cari" value="{$smarty.get.cari}" />
                    <input type="submit" value="Cari" class="button" />
                    <input type="hidden" name="mode" value="cari"/>
                </td>
            </tr>
        </table>
    </form>
{/if}
{if isset($smarty.get.mode)}
    {if isset($data_biodata)}
        <a class="button disable-ajax" onclick="history.back(-1)">Kembali</a>
        <p></p>
        <table class="ui-widget ui-widget-content" style="width: 100%">
            <tr class="ui-widget-header">
                <th colspan="6" class="header-coloumn">Detail Status Akademik Mahasiswa</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>Semester</th>
                <th>Status</th>
                <th>NO SK</th>
                <th>TGL SK</th>
                <th>Keterangan</th>
            </tr>
            {foreach $data_admisi as $data}
                <tr>
                    <td>{$data@index+1}</td>
                    <td>{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
                    <td>{$data.NM_STATUS_PENGGUNA}</td>
                    <td>{$data.NO_SK}</td>
                    <td>{$data.TGL_SK}</td>
                    <td>{$data.KETERANGAN}</td>
                </tr>
            {/foreach}   
        </table>
        <br/><hr style="width:90%"/><br/>
        <table class="ui-widget ui-widget-content" style="width: 100%">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn">Detail Biodata Mahasiswa</th>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <img src="{$foto}" width="180" height="230"/>
                </td>
            </tr>
            <tr>
                <td>NAMA</td>
                <td>{$data_biodata.NM_PENGGUNA}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>{$data_biodata.NIM_MHS}</td>
            </tr>
            <tr>
                <td>ANGKATAN</td>
                <td>{$data_biodata.THN_ANGKATAN_MHS}</td>
            </tr>
            <tr>
                <td>JENJANG</td>
                <td>{$data_biodata.NM_JENJANG}</td>
            </tr>
            <tr>
                <td>FAKULTAS</td>
                <td>{$data_biodata.NM_FAKULTAS|upper}</td>
            </tr>
            <tr>
                <td>PROGRAM STUDI</td>
                <td>{$data_biodata.NM_PROGRAM_STUDI}</td>
            </tr>
            <tr>
                <td>TEMPAT/TANGGAL LAHIR</td>
                <td> {$data_biodata.TEMPAT_LAHIR} / {$data_biodata.TGL_LAHIR_PENGGUNA|date_format:"%d %B %Y"}</td>
            </tr>
            <tr>
                <td>KONTAK</td>
                <td>{$data_biodata.MOBILE_MHS}</td>
            </tr>
            <tr>
                <td>KELAMIN</td>
                <td>
                    {if $data_biodata.KELAMIN_PENGGUNA==1}
                        Laki-laki
                    {else}
                        Perempuan
                    {/if}
                </td>
            </tr>
            <tr>
                <td>EMAIL</td>
                <td>{$data_biodata.EMAIL_PENGGUNA}</td>
            </tr>
            <tr>
                <td>BLOG MAHASISWA</td>
                <td>{$data_biodata.BLOG_PENGGUNA}</td>
            </tr>
            <tr>
                <td>ALAMAT MAHASISWA</td>
                <td>{$data_biodata.ALAMAT_MHS}</td>
            </tr>
            <tr>
                <td>NAMA AYAH</td>
                <td>{$data_biodata.NM_AYAH_MHS}</td>
            </tr>
            <tr>
                <td>ALAMAT AYAH</td>
                <td>{$data_biodata.ALAMAT_AYAH_MHS}</td>
            </tr>
            <tr>
                <td>PENGHASILAN ORTU</td>
                <td>{number_format($data_biodata.PENGHASILAN_ORTU_MHS)}</td>
            </tr>
        </table>
        <br/><hr style="width:90%"/><br/>
        <table style="width: 100%;">
            <tr class="ui-widget-header">
                <th colspan="6" class="header-coloumn">Detail Akademik Mahasiswa</th>
            </tr>
            <tr>
                <td>NAMA</td>
                <td style="width: 230px;">: {$data_mhs_status.NM_PENGGUNA}</td>
                <td>IPS</td>
                <td>: {$data_mhs_status.IPS}</td>
                <td>TOTAL SKS</td>
                <td>: {$data_mhs_status.TOTAL_SKS}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>: {$data_mhs_status.NIM_MHS}</td>
                <td>IPK</td>
                <td>: {$data_mhs_status.IPK}</td>
                <td>ANGKATAN</td>
                <td>: {$data_mhs_status.THN_ANGKATAN_MHS}</td>           
            </tr>
            <tr>
                <td>PROGRAM STUDI</td>
                <td colspan="5">: ({$data_mhs_status.NM_JENJANG}) {$data_mhs_status.NM_PROGRAM_STUDI}</td>
            </tr>
        </table>
        <br/><hr style="width:90%"/><br/>
        <table  class="ui-widget ui-widget-content" style="width:100% ">
            <tr class="ui-widget-header">
                <th colspan="6" class="header-coloumn">Detail History Nilai Mahasiswa</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>Semester</th>
                <th>KODE AJAR</th>
                <th>NAMA MATA KULIAH</th>
                <th>SKS</th>
                <th>NILAI</th>
            </tr>
            {foreach $data_history_nilai as $data}
                <tr 
                    {if $data.STATUS_ULANG>1&&$data.ULANG_KE>1}
                        style="background-color: #ffcccc"
                    {elseif $data.STATUS_ULANG>1&&$data.ULANG_KE==1}
                        style="background-color: #8f8"
                    {else}

                    {/if}
                    >
                    <td>{$data@index+1}</td>
                    <td>{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
                    <td>{$data.KD_MATA_KULIAH}</td>
                    <td>{$data.NM_MATA_KULIAH}</td>
                    <td>{$data.KREDIT_SEMESTER}</td>
                    <td>
                        {if $data.NILAI_HURUF==''}
                            Nilai Kosong
                        {else}
                            {$data.NILAI_HURUF}
                        {/if}
                    </td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="6">
                    <p style="font-weight: bold">Keterangan : </p>
                    <ol>
                        <li>
                            <p>Warna <span class="ui-corner-all" style="background-color: #ffcccc;padding: 3px">Merah</span> : Sudah diulang, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif</p>
                        </li>
                        <li>
                            <p>Warna <span class="ui-corner-all" style="background-color: #8f8;padding: 3px">Hijau</span> : Ulangan, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif</p>
                        </li>
                    </ol>
                </td>
            </tr>
        </table>
        <br/><hr style="width:90%"/><br/>
        <table width="100%">
            <tr class="ui-widget-header">
                <th colspan="10" class="header-coloumn">Detail Pembayaran {if $jenis=='cmhs'} Calon {/if} Mahasiswa</th>
            </tr>
            <tr>
                <th>No</th>
                <th>Semester</th>
                <th>Tagihkan</th>
                <th>Total Biaya</th>
                <th>Nama Bank</th>
                <th>Via Bank</th>
                <th>Tanggal Bayar</th>
                <th>Keterangan</th>
                <th>Status Bayar</th>
                <th>Operasi</th>
            </tr>
            {foreach $data_pembayaran as $data}
                <tr class="total ui-widget-content">
                    <td>{$data@index+1}</td>
                    <td>{$data.NM_SEMESTER}<br/> ({$data.TAHUN_AJARAN})</td>
                    <td>
                        {if $data.IS_TAGIH=='Y'}
                            Ya
                        {else}
                            Tidak
                        {/if}
                    </td>
                    <td class="center">{number_format($data.JUMLAH_PEMBAYARAN)}</td>
                    <td>{$data.NM_BANK}</td>
                    <td>{$data.NAMA_BANK_VIA}</td>
                    <td>{$data.TGL_BAYAR}</td>
                    <td>{$data.KETERANGAN}</td>
                    <td>{$data.NAMA_STATUS}</td>
                    <td>
                        <span onclick="show_detail('{$data@index+1}','#detail')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Detail</span><br/>
                        {if $smarty.get.mode!='edit'&&$jenis=='mhs'}
                            <span onclick="confirm_delete('{$smarty.get.cari}','{$data.ID_SEMESTER}','{$data.TGL_BAYAR}','{$data.ID_BANK}','{$data.ID_BANK_VIA}','{$data.NO_TRANSAKSI}','{$data.KETERANGAN}','{$data.IS_TAGIH}')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Delete</span><br/>
                        {/if}
                        {if $jenis=='mhs'}
                            <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="history-bayar.php?mode=edit&id_semester={$data.ID_SEMESTER}&tgl_bayar={$data.TGL_BAYAR}&id_bank={$data.ID_BANK}&id_bank_via={$data.ID_BANK_VIA}&no_transaksi={$data.NO_TRANSAKSI}&keterangan={$data.KETERANGAN}&cari={$smarty.get.cari}&is_tagih={$data.IS_TAGIH}">Edit</a>
                        {/if}
                    </td>
                </tr>
                <tr id="detail{$data@index+1}" style="display: none">
                    <td colspan="10">
                        <table width="95%">
                            <tr class="ui-widget-header" style="color: white">
                                <th>Nama Biaya</th>
                                <th>Besar Biaya</th>
                                {if $jenis=='mhs'}
                                    <th>Denda Biaya</th>
                                {/if}
                                <th>Tagihkan</th>
                                <th>Nama Bank</th>
                                <th>Via Bank</th>
                                <th>Tanggal Bayar</th>
                                <th>Keterangan</th>
                            </tr>
                            {foreach $data.DATA_PEMBAYARAN as $data_pembayaran}
                                {if $jenis=='cmhs'}
                                    {if $data_pembayaran.TGL_BAYAR==NULL}
                                        {$total_tagihan=$total_tagihan+$data_pembayaran.BESAR_BIAYA}
                                    {else}
                                        {$total_pembayaran=$total_pembayaran+$data_pembayaran.BESAR_BIAYA}
                                    {/if}
                                {else}
                                    {if $data_pembayaran.TGL_BAYAR==NULL}
                                        {$total_tagihan=$total_tagihan+$data_pembayaran.BESAR_BIAYA+$data_pembayaran.DENDA_BIAYA}
                                    {else}
                                        {$total_pembayaran=$total_pembayaran+$data_pembayaran.BESAR_BIAYA+$data_pembayaran.DENDA_BIAYA}
                                    {/if}
                                {/if}
                                <tr>
                                    <td>
                                        {$data_pembayaran['NM_BIAYA']}
                                    </td>
                                    <td>
                                        {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$smarty.get.keterangan==$data.KETERANGAN}
                                            <input type="text" name="besar_biaya{$data_pembayaran@index+1}" value="{number_format($data_pembayaran['BESAR_BIAYA'])}"/>
                                            {$total_detail=$data_pembayaran@index+1}
                                        {else}
                                            {number_format($data_pembayaran['BESAR_BIAYA'])}
                                        {/if}                                            
                                    </td>

                                    {if $jenis=='mhs'}
                                        <td>
                                            {if $smarty.get.id_semester==$data.ID_SEMESTER&&$smarty.get.tgl_bayar==$data.TGL_BAYAR&&$smarty.get.id_bank==$data.ID_BANK&&$smarty.get.id_bank_via==$data.ID_BANK_VIA&&$smarty.get.no_transaksi==$data.NO_TRANSAKSI&&$smarty.get.keterangan==$data.KETERANGAN}
                                                <input type="text" name="denda_biaya{$data_pembayaran@index+1}" value="{number_format($data_pembayaran['DENDA_BIAYA'])}"/>
                                                <input type="hidden" name="id_pembayaran{$data_pembayaran@index+1}" value="{$data_pembayaran.ID_PEMBAYARAN}"/>
                                                {$total_detail=$data_pembayaran@index+1}
                                            {else}
                                                {number_format($data_pembayaran['DENDA_BIAYA'])}
                                            {/if}
                                        </td>
                                    {/if}


                                    <td>
                                        {if $data_pembayaran['IS_TAGIH']=='Y'}
                                            Y
                                        {else}
                                            Tidak
                                        {/if}
                                    </td>
                                    <td>
                                        {$data_pembayaran['NM_BANK']}
                                    </td>
                                    <td>
                                        {$data_pembayaran['NAMA_BANK_VIA']}
                                    </td>
                                    <td>
                                        {$data_pembayaran['TGL_BAYAR']}
                                    </td>
                                    <td>
                                        {$data_pembayaran['KETERANGAN']}
                                    </td>
                                </tr>
                            {/foreach}

                            <tr>
                                <td class="center" colspan="{if $jenis=='cmhs'}8{else}9{/if}">
                                    <span onclick="hide_detail('{$data@index+1}','#detail')" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Tutup</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            {foreachelse}
                <tr class="total ui-widget-content">
                    <td class="center" colspan="10">
                        <span style="color:red">
                            Data Pembayaran Kosong
                        </span>
                    </td>
                </tr>
            {/foreach}
        </table>
        {literal}
            <script type="text/javascript">
                $('.ui-button').hover(
                    function(){
                        $(this).addClass('ui-state-active')
                        }, 
                    function(){        
                        $(this).removeClass('ui-state-active')
                        }
                    );
                function show_detail(index,tag){
                    $(tag+index).fadeToggle();
                }
                function hide_detail(index,tag){
                    $(tag+index).fadeOut();
                }
        
            </script>
        {/literal}
    {else if isset($data_cari)}
        <table style="width: 98%">
            <tr>
                <th>NO</th>
                <th>NAMA</th>
                <th>NIM</th>
                <th>PROGRAM STUDI</th>
                <th>FAKULTAS</th>
                <th>STATUS</th>
                <th>ROLE</th>
                <th>DETAIL</th>
            </tr>
            {foreach $data_cari as $d}
                <tr>
                    <td>{$d@index+1}</td>
                    <td>{$d.NM_PENGGUNA}</td>
                    <td>{$d.NIM_MHS}</td>
                    <td>{$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI}</td>
                    <td>{$d.NM_FAKULTAS}</td>
                    <td>{$d.NM_STATUS_PENGGUNA}</td>
                    <td style="text-align:center">
                        {if $d.ID_ROLE==3}
                            {$d.NM_ROLE}
                        {else}
                            {$d.NM_ROLE}
                            <br/>
                            <form method="post" action="data-mahasiswa.php?{$smarty.server.QUERY_STRING}"> 
                                <input type="hidden" name="mode" value="ubah-role"/>
                                <input type="hidden" name="id_pengguna" value="{$d.ID_PENGGUNA}"/>
                                <input type="submit"  value="Ubah Mahasiswa"/>
                            </form>
                        {/if}
                    </td>
                    <td>
                        <a class="button" href="data-mahasiswa.php?mode=detail&mhs={$d.ID_MHS}">DETAIL</a>
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="7" class="data-kosong">Data Tidak Ditemukan</td>
                </tr>
            {/foreach}
        </table>
    {/if}
{/if}
