{literal}
    <style>
        .span_button{
            padding: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            background-color: #009933;
            color: #ffffff;
            cursor: pointer;
        }
    </style>
{/literal} 
<div class="center_title_bar">Cari Mahasiswa</div>
{if isset($data_mahasiswa)}
    <table>
        <tr>
            <th>NAMA</th>
            <th>NIM</th>
			<th>NO UJIAN</th>
            <th>FAKULTAS</th>
            <th>PROGRAM STUDI</th>
        </tr>
        {if $data_mahasiswa==null}
            <tr>
                <td colspan="5" class="center">Hasil Pencarian Tidak Ditemukan</td>
            </tr>
        {else}
            {foreach $data_mahasiswa as $data}
                <tr>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>{$data.NIM_MHS}</td>
					<td>{$data.NO_UJIAN}</td>
                    <td>{$data.NM_FAKULTAS|upper}</td>
                    <td>{$data.NM_PROGRAM_STUDI}</td>
                </tr>
            {/foreach}
        {/if}
    </table>
	<a href="cari-mahasiswa.php">Kembali</a>
{else}
    <form action="cari-mahasiswa.php" method="get">
        <table>
            <tr>
                <td>NAMA / NIM MAHASISWA / NOMER UJIAN</td>
                <td><input type="text" name="x"/></td>
            </tr>
        </table>
    </form>
{/if}
