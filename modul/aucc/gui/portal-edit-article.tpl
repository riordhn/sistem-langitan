<script>

	$(function() {
		$( "#tabs" ).tabs({
			ajaxOptions: {
				error: function( xhr, status, index, anchor ) {
					$( anchor.hash ).html(
						"Couldn't load this tab. We'll try to fix this as soon as possible. " +
						"If this wouldn't be a demo." );
				}
			}
		});
	});

</script>

<script>
	$(function() {
	
		{for $foo=0 to $jml_data_article-1}
			$("#btnEdit{$id_article[$foo]}").button({
				icons: {
					primary: "ui-icon-pencil",
				},
				text: true
			});
			$("#btnApprove{$id_article[$foo]}").button({
				icons: {
					primary: "ui-icon-check",
				},
				text: true
			});
			$("#btnDelete{$id_article[$foo]}").button({
				icons: {
					primary: "ui-icon-trash",
				},
				text: true
			});
			$("#btnSubmit{$id_article[$foo]}").button({
				icons: {
					primary: "ui-icon-check",
				},
				text: true
			});
			$("#btnCancel{$id_article[$foo]}").button({
				icons: {
					primary: "ui-icon-cancel",
				},
				text: true
			});
			
			
	
			//Event Submit ( Edit )
			$('#MyForm{$id_article[$foo]}').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {
						
						$('#link_judul_article_div{$id_article[$foo]}').show();
						$('#isi_article_div{$id_article[$foo]}').show();
						$('#image_link_article_div{$id_article[$foo]}').show();
						$('#resume_article_div{$id_article[$foo]}').show();
						
						$('#judul_article{$id_article[$foo]}').hide();
						$('#resume_article{$id_article[$foo]}').hide();
						$('#isi_article{$id_article[$foo]}').remove();
						$('#image_link_article{$id_article[$foo]}').hide();
						
						tinyMCE.execCommand('mceRemoveControl',false,'isi_article{$id_article[$foo]}'); //removing tiny_mce 
					
						$('#btnEdit{$id_article[$foo]}').show();
					
						$('#btnSubmit{$id_article[$foo]}').hide();
						$('#btnCancel{$id_article[$foo]}').hide();	
						
						var link_judul = document.getElementById('link_judul_article_div{$id_article[$foo]}');
						var image_link = document.getElementById('image_link_article_div{$id_article[$foo]}');
						var resume_article = document.getElementById('resume_article_div{$id_article[$foo]}');
						var isi_article = document.getElementById('isi_article_div{$id_article[$foo]}');
						
						link_judul.innerHTML = document.getElementById('judul_article{$id_article[$foo]}').value;
						image_link.innerHTML = '<img id="image_link_article_div{$id_article[$foo]}" style="width:200px; height:150px;" src="' + document.getElementById('image_link_article{$id_article[$foo]}').value + '"/>';
						resume_article.innerHTML = document.getElementById('resume_article{$id_article[$foo]}').value;
						isi_article.innerHTML = document.getElementById('get_isi{$id_article[$foo]}').value;
						
						//window.location.href = "{$base_url}modul/aucc/#portal-article!portal-incomming-article.php?id={$id_article[$foo]}&view=all";

					}
				})
				return false;
			});
			
			
			//Submit Approve
			$('#MyFormApprove{$id_article[$foo]}').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {
						alert('Berhasil DiApprove');
						window.location.href = "{$base_url}modul/aucc/#portal-article!portal-edit-article.php?";
					}
				})
				return false;
			});
			
			//Submit Delete
			$('#MyFormDelete{$id_article[$foo]}').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {
						alert('Berhasil Didelete');
						window.location.href = "{$base_url}modul/aucc/#portal-article!portal-edit-article.php?";
					}
				})
				return false;
			});
			
			/////////////////////////////////////  End Of Event Submit ///////////////////////////////////////////////
			
			//Event Click
			$('#btnEdit{$id_article[$foo]}').click(function(){		
				
					$('#link_judul_article_div{$id_article[$foo]}').hide();
					$('#isi_article_div{$id_article[$foo]}').hide();
					$('#image_link_article_div{$id_article[$foo]}').hide();
					$('#resume_article_div{$id_article[$foo]}').hide();
					
					$('#judul_article{$id_article[$foo]}').show();
					$('#resume_article{$id_article[$foo]}').show();
					$('#image_link_article{$id_article[$foo]}').show();
					
					$('.isi').prepend('<TEXTAREA class="mceEditor1" id="isi_article{$id_article[$foo]}" style="width:100%; min-height:700px;" NAME="isi_article{$id_article[$foo]}" COLS=30 ROWS=6>{$isi_article[$foo]}</TEXTAREA>');
					
					setup(); // Call TinyMCE 
				
					$('#btnEdit{$id_article[$foo]}').hide();
				
					$('#btnSubmit{$id_article[$foo]}').show();
					$('#btnCancel{$id_article[$foo]}').show();
				
			});
			
			
			$('#btnCancel{$id_article[$foo]}').click(function(){		
					
					$('#link_judul_article_div{$id_article[$foo]}').show();
					$('#isi_article_div{$id_article[$foo]}').show();
					$('#image_link_article_div{$id_article[$foo]}').show();
					$('#resume_article_div{$id_article[$foo]}').show();
					
					$('#judul_article{$id_article[$foo]}').hide();
					$('#resume_article{$id_article[$foo]}').hide();
					$('#isi_article{$id_article[$foo]}').remove();
					$('#image_link_article{$id_article[$foo]}').hide();
					
					unSetup();
					tinyMCE.execCommand('mceRemoveControl',false,'isi_article{$id_article[$foo]}'); //removing tiny_mce 
				
					$('#btnEdit{$id_article[$foo]}').show();
				
					$('#btnSubmit{$id_article[$foo]}').hide();
					$('#btnCancel{$id_article[$foo]}').hide();	
				
			});
			

			//When Button Submit is Clicking
			$('#btnSubmit{$id_article[$foo]}').click(function(){	
				var ed = tinyMCE.get('isi_article{$id_article[$foo]}');
				var isi = ed.getContent();
				$("#get_isi{$id_article[$foo]}").val(isi);
				$('#MyForm{$id_article[$foo]}').submit();
				//alert(ed.getContent());
			});			
			
			//When Button Approve is Clicking
			$('#btnApprove{$id_article[$foo]}').click(function(){		
				$('#MyFormApprove{$id_article[$foo]}').submit();
				//alert('hello');
			});
			
			//When Button Delete Is Clicking 
			$('#btnDelete{$id_article[$foo]}').click(function(){		
				$('#MyFormDelete{$id_article[$foo]}').submit();
				//alert('hello');
			});
			
			
			/////////////////////////////////////////////////// End Of Event Click ///////////////////////////
		
			
			
		{/for}
	
	});
</script>

<script type="text/javascript">
		function unSetup() {
			tinyMCE.execCommand('mceRemoveControl',false,'isi_article{$id_article[$foo]}');
		}
</script>

<script type="text/javascript">
		function setup() {
			tinyMCE.init({
					mode : "textareas",
					theme : "advanced",
					editor_selector : "mceEditor1",
					editor_deselector : "mceNoEditor",
					  plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,",
					  theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
					  theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,|,insertdate,inserttime,preview,|,forecolor,backcolor",
					  theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
					  theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
					  theme_advanced_toolbar_location : "top",
					  theme_advanced_toolbar_align : "left",
					  theme_advanced_statusbar_location : "bottom",
					  theme_advanced_resizing : true
			});
		}
</script>

<div id="tabs">
			Pages : <a href="#portal-article!portal-article.php">ALL article</a>
			<br /><br />
			<div class="column-article">	
				<div class="portlet-article">
					<section>

					<div id="article-content" class="portlet-content-article ui-widget-content" style="background:#fff;">
						<div style="margin:20px;">
							{for $foo=0 to $jml_data_article-1}
									
									<form  style="display:none;" id="MyFormApprove{$id_article[$foo]}" name="MyFormApprove{$id_article[$foo]}" method="post" action="portal-app-article.php">										
										<input style="display:none;" type="text" name="id_article" value="{$id_article[$foo]}" >
									</form>
									<form  style="display:none;" id="MyFormDelete{$id_article[$foo]}" name="MyFormDelete{$id_article[$foo]}" method="post" action="portal-del-article.php">								
										<input style="display:none;" type="text" name="id_article" value="{$id_article[$foo]}" >
									</form>
									
									<form  id="MyForm{$id_article[$foo]}" name="MyForm{$id_article[$foo]}" method="post" action="portal-article-edit-update.php">
										
										{if $id!=""}
											<input value="0" type="text" id="get_isi{$id_article[$foo]}" name="get_isi{$id_article[$foo]}" style="display:none;">
											<div style="float:left;" id="btnEdit{$id_article[$foo]}" name="btnEdit{$id_article[$foo]}" style="height:30px; width:80px;" title="Edit article">
												Edit
											</div>
											<div  id="btnSubmit{$id_article[$foo]}" name="btnSubmit{$id_article[$foo]}" style="float:left; display:none;" title="Submit">
												Submit
											</div>
											<div  id="btnCancel{$id_article[$foo]}" name="btnCancel{$id_article[$foo]}" style="float:left; display:none;" title="Cancel">
												Cancel
											</div>
											<div style="float:left;" id="btnApprove{$id_article[$foo]}" name="btnApprove{$id_article[$foo]}" style="height:30px; width:80px;" title="Approve article">
												Approve
											</div>
											<div style="float:left;" id="btnDelete{$id_article[$foo]}" name="btnDelete{$id_article[$foo]}" style="height:30px; width:80px;" title="Delete article">
												Delete
											</div>

										{/if}
										<article id="article{$foo}">
										<input style="display:none;" type="text" name="id_article" value="{$id_article[$foo]}" >
										<div>
											&nbsp;
										</div>
										<br />
										<div class="title">
											Judul : <a class="disable-ajax" id="link_judul_article_div{$id_article[$foo]}" href="#portal-article!portal-edit-article.php?id={$id_article[$foo]}&view=all">{$judul_article[$foo]}</a>
											<textarea style="width:100%; height:30px;display:none;" COLS=30 ROWS=3   id="judul_article{$id_article[$foo]}" name="judul_article{$id_article[$foo]}" >{$judul_article[$foo]}</textarea>
										</div>
										<div class="date">
											Oleh : &nbsp; {$pengguna[$foo]}
											<br />
											Tanggal : &nbsp; {$tgl_article[$foo]}
										</div>
										<div class="content_article">
											{if $view == "all"}
												
												<legend>Image Link</legend>
												<div style="width:100%;" id="image_link_article_div{$id_article[$foo]}">
													<img id="image_link_article_div{$id_article[$foo]}" style="width:200px; height:150px;" src="{$img_link[$foo]}"/>
												</div>
												<br />
												<TEXTAREA id="image_link_article{$id_article[$foo]}" style="width:100%; height:30px;display:none;" NAME="image_link_article{$id_article[$foo]}" COLS=30 ROWS=6>{$img_link[$foo]}</TEXTAREA>
												
												<legend>Resume</legend>
												<div style="width:100%;" id="resume_article_div{$id_article[$foo]}">
													{$resume_article[$foo]}
												</div>
												<br />
												<TEXTAREA id="resume_article{$id_article[$foo]}" style="display:none; width:100%; min-height:150px;" NAME="resume_article{$id_article[$foo]}" COLS=30 ROWS=6>{$resume_article[$foo]}</TEXTAREA>
												
												<legend>Isi</legend>
												<div style="width:100%;" id="isi_article_div{$id_article[$foo]}">{$isi_article[$foo]}</div><br />
												<div class="isi" id="isi">
													
												</div>
											{else}
												{$resume_article[$foo]}
											{/if}
										</div>
										</article>
										
									</form>
								
							{/for}
						</div>
					</div>
					</section>
				</div>
			</div>
</div>