<script>
	$(function(){
			$("#submit").buttonset();
			$('#MyFormAdd').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {
					
						$('#result').show();
						$('#MyFormAdd').hide();
						
						var judul_div = document.getElementById('judul_div');
						var isi_div = document.getElementById('isi_div');
						
						judul_div.innerHTML = document.getElementById('judul').value;
						isi_div.innerHTML = document.getElementById('isi').value;

					}
				})
				return false;
			});
	});
</script>

<script>
	$(function() {
	
		{for $foo=0 to $jml_data_article-1}
		
			// Button Set
			$( "#submit{$id_article[$foo]}").buttonset();
			$("#btnEdit{$id_article[$foo]}").button({
				icons: {
					primary: "ui-icon-pencil",
				},
				text: true
			});
			
			$("#btnDelete{$id_article[$foo]}").button({
				icons: {
					primary: "ui-icon-trash",
				},
				text: true
			});
			
			$("#btnApprove{$id_article[$foo]}").button({
				icons: {
					primary: "ui-icon-check",
				},
				text: true
			});
			
			// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
			// Modal
			
			
			// Event Click
			$('#btnEdit{$id_article[$foo]}').click(function(){		
				$('#judul_article_div{$id_article[$foo]}').hide();
				$('#link_article_div{$id_article[$foo]}').hide();
				$('#image_link_article_div{$id_article[$foo]}').hide();
				$('#isi_article_div{$id_article[$foo]}').hide();
				$('#resume_article_div{$id_article[$foo]}').hide();
				
				$('#judul_article{$id_article[$foo]}').show();					
				$('#link_article{$id_article[$foo]}').show();
				$('#image_link_article{$id_article[$foo]}').show();
				$('#isi_article{$id_article[$foo]}').show();
				$('#resume_article{$id_article[$foo]}').show();
				
				$('#btnEdit{$id_article[$foo]}').hide();
				$('#submit{$id_article[$foo]}').show();
			});
			/*
			$('#btnDelete{$id_article[$foo]}').click(function(){		
				$( "#dialog{$id_article[$foo]}:ui-dialog" ).dialog( "destroy" );
				$( "#dialog-confirm{$id_article[$foo]}" ).dialog({
					resizable: false,
					height:140,
					modal: true,
					buttons: {
						"Delete": function() {
							//$("#dialog-confirm{$id_article[$foo]}").dialog( "destroy" );
							$('#btnDelete{$id_article[$foo]}').click(function() {
							  $('#MyFormDelete{$id_article[$foo]}').submit();
							});
							//$("#del_article{$id_article[$foo]}").hide();

							//alert("Data Berhasil Dihapus");
						},
						Cancel: function() {
							$("#dialog-confirm{$id_article[$foo]}").dialog( "destroy" );
						}
					}
				});
			});
			*/
			
		{/for}
	
	});
</script>
<script type="text/javascript">
	tinyMCE.init({
			mode : "textareas",
			theme : "advanced",
			editor_selector : "mceEditor",
			editor_deselector : "mceNoEditor",
			  plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,",
			  theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
			  theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			  theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			  theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			  theme_advanced_toolbar_location : "top",
			  theme_advanced_toolbar_align : "left",
			  theme_advanced_statusbar_location : "bottom",
			  theme_advanced_resizing : true
	});
</script>
	<!-- end style -->
	Pages : <a href="#portal-article!portal-article.php">ALL article</a>
	<div id="result" style="display:none;">
		Berhasil ditambahkan
		<div id="judul_div"></div>
		<div id="resume_div"></div>
		<div id="isi_div"></div>
	</div>
	<form id="MyFormAdd" name="MyFormAdd" method="post" action="portal-insert-article.php">
		<table style="width:100%" class="ui-widget">
			<thead class="ui-widget-header">
				<tr>
					<td width="20%">Item</td><td>Isian</td>
				</tr>
			</thead>
			<tbody class="ui-widget-content" valign="top">
				<tr>	
					<td >Judul</td>
					<td>
						<TEXTAREA id="judul" style="width:100%; height:30px;" NAME="judul" COLS=30 ROWS=6></TEXTAREA>
						<input type="text" id="id_pengguna" style="display:none;" NAME="id_pengguna" value="{$id_pengguna}"/>
					</td>
				</tr>
				<tr>	
				<tr>	
					<td valign="top" style="text-align:top;">Image Link</td>
					<td>
						<TEXTAREA id="image_link" style="width:100%; height:30px;" NAME="image_link" COLS=30 ROWS=6></TEXTAREA>
					</td>
				</tr>
				<tr>	
					<td valign="top" style="text-align:top;">Resume</td>
					<td>
						<TEXTAREA id="resume" style="width:100%; height:60px;" NAME="resume" COLS=30 ROWS=6></TEXTAREA>
					</td>
				</tr>
				<tr>	
					<td valign="top" style="text-align:top;">Isi</td>
					<td>
						<TEXTAREA id="isi" class="mceEditor" style="width:100%; min-height:700px;" NAME="isi" COLS=30 ROWS=6></TEXTAREA>
					</td>
				</tr>
				<tr>	
					<td></td>
					<td>
						<div id="submit" name="submit" >
							<input type="submit" name="btn_submit" id="btn_submit" value="Submit"  />
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</form>