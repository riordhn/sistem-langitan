<div class="center_title_bar">Info Dosen</div>
<div id="biodata_info_dosen" class="ui-widget"  >
    <div id="dialog_info_dosen" style="display: none;">Data Info Dosen Berhasil Di Simpan</div>
    <div id="alert_info_dosen" style="display: none;">Klik edit mode terlebih dahulu !</div>
    <form name="info_dosen" id="form_info_dosen" action="info_dosen.php" method="post">
        <table class="none" style="width: 90%;">
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn"><h2>INFORMASI DATA DOSEN</h2></th>
            </tr>
            <tr>
                <td width="20%"><span class="field">Fak/Unit</span></td>
                <td width="30%"><span>{$data_info_dosen['FAKULTAS']|upper}</span></td>
                <td width="20%"><span class="field">Jur/bag/lab</span></td>
                <td width="30%"><span>{$data_info_dosen['PRODI']|upper}</span></td>
            </tr>
            <tr>
                <td><span class="field">Jenis Peg</span></td>
                <td><span>{$data_info_dosen['JENIS_PEGAWAI']|upper}</span></td>
                <td><span class="field">Status</span></td>
                <td><span>AKTIF</span></td>
            </tr>
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn"><h3>A.IDENTITAS PRIBADI</h3></th>
            </tr>
            <tr>
                <td><span class="field">Nama  </span></td>
                <td><span>{$data_info_dosen['GELAR_DEPAN']} {$data_info_dosen['NAMA']|upper} {$data_info_dosen['GELAR_BELAKANG']}</span></td>
                <td><span class="field">NIP </span></td>
                <td><span>{$data_info_dosen['USERNAME']}</span></td>
            </tr>
            <tr>
                <td><span class="field">Tempat Lahir</span></td>
                <td><span>{$data_info_dosen['KOTA_LAHIR']|upper}</span></td>
                <td><span class="field">Email  </span></td>
                <td>
                    <a class="disable-ajax" href="http://mail.google.com/a/{$data_info_dosen['LINKEMAIL']}" target="_blank">{$data_info_dosen['EMAIL']}</a><br/>
                    ({$data_info_dosen['PASSWORD_PENGGUNA']})<br>{$data_info_dosen['RENAMEEMAIL']}
                </td>
            </tr>
            <tr>
                <td>
                    <span class="field">Blog Dosen</span>
                </td>
                <td>
                    <a class="disable-ajax" href="{$data_info_dosen['LINK_BLOG']}" target="_blank">{$data_info_dosen['BLOG_PENGGUNA']}</a>
                </td>
                <td><span class="field">Contact Person  </span></td>
                <td>
                    {$data_info_dosen['TELP']}
                </td>
            </tr>
            <tr>
                <td><span class="field">Tanggal Lahir  </span></td>
                <td><span>{$data_info_dosen['TGL_LAHIR']}</span></td>
                <td><span class="field">Agama</span></td>
                <td><span>{$data_info_dosen['AGAMA']|upper}</span></td>
            </tr>
            <tr>
                <td><span class="field">Alamat Praktek/Kantor</span></td>
                <td>
                    {$data_info_dosen['ALAMAT_KANTOR']|upper}
                </td>
                <td><span class="field">Alamat  </span></td>
                <td>
                    {$data_info_dosen['ALAMAT_RUMAH']|upper}
                </td>
            </tr>
            <tr>
                <td><span class="field">Jenis Kelamin  </span></td>
                <td colspan='3'>
                    {if $data_info_dosen['KELAMIN'] == 2}
                        <span>PEREMPUAN</span>
                    {else}
                        <span>LAKI-LAKI</span>
                    {/if}
                </td>
            </tr>
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn"><h3>B.PROFIL STUDI</h3></th>
            </tr>
            <tr>
                <td><span class="field">Pend.Akhir</span></td>
                <td><span></span></td>
                <td><span class="field">Lulus</span></td>
                <td><span>{$data_info_dosen['TAHUN_LULUS']|upper}</span></td>
            </tr>
            <tr>
                <td ><span class="field">Bidang Ahli</span></td>
                <td colspan="3"><span>{$data_info_dosen['BIDANG_KEAHLIAN_DOSEN']|upper}</span></td>
            </tr>
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn"><h3>C.PROFILE KEPEGAWAIAN</h3></th>
            </tr>
            <tr>
                <td><span class="field">Pangkat/GOl</span></td>
                <td><span>{$data_info_dosen['NM_GOLONGAN']|upper}</span></td>
                <td><span class="field">Tmt Pangkat</span></td>
                <td><span></span></td>
            </tr>
            <tr>
                <td><span class="field">Jab.Fungs</span></td>
                <td><a href="#"></a></td>
                <td><span class="field">Tmt Fungs</span></td>
                <td><span></span></td>
            </tr>
            <tr>
                <td><span class="field">Jabatan Struktural</span></td>
                <td><span>{$data_info_dosen['JABATAN']|upper}</span></td>
                <td><span class="field">Tmt Struk</span></td>
                <td><span></span></td>
            </tr>
            <tr>
                <td><span class="field">Lat.Jabatan</span></td>
                <td><span></span></td>
                <td><span class="field">Tmt Pensiun</span></td>
                <td><span></span></td>
            </tr>
        </table>
    </form>
</div>
{literal}
    <script type="text/javascript" >
    $('#save').click(function(){
        if($(':disabled').size()=='4'){
            $('#alert_info_dosen').dialog('open')
        }else{
            $('#form_info_dosen').submit();
        }
    });
    $('#form_info_dosen').validate({
        rules :{
            telp :{
                required :true,
                number : true
            },
            email : {
                required :true,
                email :true
            },
            alamat_rumah:{
                required :true
            },
            alamat_kantor:{
                required :true
            }
        }
    });
    $('#form_info_dosen').ajaxForm({
        type : 'post',
        url : $(this).attr('action'),
        data : $(this).serialize(),
        success : function(data){
            $('#center_content').html(data);
            $("#dialog_info_dosen").dialog("open")
        }
    });

    function enabled_textbox(){
        document.info_dosen.alamat_rumah.disabled=false;
        document.info_dosen.alamat_kantor.disabled=false;
        document.info_dosen.email.disabled=false;
        document.info_dosen.telp.disabled=false;
    }
    function disabled_textbox(){
        document.info_dosen.alamat_rumah.disabled=true;
        document.info_dosen.alamat_kantor.disabled=true;
        document.info_dosen.email.disabled=true;
        document.info_dosen.telp.disabled=true;
    }
    $("#dialog_info_dosen").dialog({
        autoOpen:false,
        position:"middle",
        dragable:false,
        modal:true,
        resizable:false,
        title:"Confirmation Message"
    });
    $("#alert_info_dosen").dialog({
        autoOpen:false,
        position:"middle",
        dragable:false,
        modal:true,
        resizable:false,
        title:"Alert Message"
    });
    </script>
{/literal}