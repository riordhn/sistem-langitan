<div class="center_title_bar">Account : {$pengguna.USERNAME}</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="informasi-user.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_pengguna" value="{$pengguna.ID_PENGGUNA}" />
<table>
    <tr>
        <th colspan="2">Detail Account</th>
    </tr>
    <tr>
        <td>ID</td>
        <td>{$pengguna.ID_PENGGUNA}</td>
    </tr>
    <tr>
        <td>Username</td>
        <td>{$pengguna.USERNAME}</td>
    </tr>
    <tr>
        <td>Password</td>
        <td style="text-decoration: line-through;">{$pengguna.SE1}</td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>{$pengguna.NM_PENGGUNA}</td>
    </tr>
    <tr>
        <td>Tipe Account</td>
        <td>{if $pengguna.JOIN_TABLE == 1}Pegawai{else if $pengguna.JOIN_TABLE == 2}Dosen{/if}
        </td>
    </tr>
    <tr>
        <td>Role Aktif</td>
        <td>
            <select name="id_role">
            {foreach $role_pengguna_set as $rp}
                <option value="{$rp.ID_ROLE}" {if $rp.ID_ROLE == $pengguna.ID_ROLE}selected="selected"{/if}>{$rp.NM_ROLE}</option>
            {/foreach}
            </select>
        </td>
    </tr>
	<tr>
        <td>Status Login</td>
        <td>{if $pengguna.ID_SESSION}Online{else}Offline{/if}</td>
    </tr>
	<!--
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
	-->
</table>
</form>

<script type="text/javascript">
    function deleteRolePengguna(id_role_pengguna)
    {
        if (confirm('Apakah role ini akan di hapus') == true)
        {
            $.ajax({
                type: 'POST',
                url: 'account-search.php',
                data: 'mode=delete-role&id_role_pengguna='+id_role_pengguna,
                success: function(data) {
                    if (data == '1') {
                        $('#r'+id_role_pengguna).remove();
                    }
                    else {
                        alert('Gagal dihapus');
                    }  
                }
            });
        }
    }
</script>
   
<!--   
<table>
    <tr>
        <th>Default</th>
        <th>Role</th>
        <th>Template</th>
        <th>Action</th>
    </tr>
    {foreach $role_pengguna_set as $rp}
    <tr {if $rp@index is not div by 2}class="row1"{/if} id="r{$rp.ID_ROLE_PENGGUNA}">
        <td class="center"><input type="radio" name="id_role" value="{$rp.ID_ROLE}" {if $rp.ID_ROLE == $pengguna.ID_ROLE}checked="checked"{/if} disabled="disabled"/></td>
        <td>{$rp.NM_ROLE}</td>
        <td>{if $rp.NM_TEMPLATE == ''}Default{else}{$rp.NM_TEMPLATE}{/if}</td>
        <td>
            <a href="account-search.php?mode=edit-role&id_role_pengguna={$rp.ID_ROLE_PENGGUNA}" title="Edit"><img src="{$base_url}img/superadmin/navigation/edit.png" /></a>
            {if $rp.ID_ROLE != $pengguna.ID_ROLE}
            <img src="{$base_url}img/superadmin/navigation/del-red.png" title="Hapus Role" style="cursor: pointer" onclick='deleteRolePengguna({$rp.ID_ROLE_PENGGUNA})' />
            {/if}
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="4" class="center">
            <a href="account-search.php?mode=add-role&id_pengguna={$pengguna.ID_PENGGUNA}&q={$smarty.get.q}">Tambah</a>
        </td>
    </tr>
</table>
-->
<a href="informasi-user.php?q={$smarty.get.q}">Kembali</a>