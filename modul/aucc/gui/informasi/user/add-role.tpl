<div class="center_title_bar">Account - Add Role</div>

<h3>Nama Pengguna : {$pengguna.NM_PENGGUNA}</h3>

<form action="account-search.php?mode=edit&id_pengguna={$pengguna.ID_PENGGUNA}" method="post">
<input type="hidden" name="mode" value="add-role" />
<input type="hidden" name="id_pengguna" value="{$pengguna.ID_PENGGUNA}" />
<table>
    <tr>
        <th>Pilih</th>
        <th>Nama Role</th>
        <th>Deskripsi</th>
    </tr>
    {foreach $role_set as $r}
    <tr>
        <td class="center"><input type="checkbox" name="id_roles[]" value="{$r.ID_ROLE}" /></td>
        <td>{$r.NM_ROLE}</td>
        <td>{$r.DESKRIPSI_ROLE}</td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="3" class="center">
            <a href="account-search.php?mode=edit&id_pengguna={$pengguna.ID_PENGGUNA}">Kembali</a>
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>