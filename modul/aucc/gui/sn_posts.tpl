<style>
	#toolbar {
		padding: 10px 4px;
	}
	.btn-like{
		background:url('{$base_url}img/social-network/thumbs_up.png');
		background-repeat:no-repeat;
		width:18px;
		height:18px;
		float:left;
	}
	.btn-dislike{
		background:url('{$base_url}img/social-network/thumbs_down.png');
		background-repeat:no-repeat;
		width:18px;
		height:18px;
		float:left;
	}
	.btn-comment{
		background:url('{$base_url}img/social-network/comment.png');
		background-repeat:no-repeat;
		width:18px;
		height:18px;
		float:left;
	}
	.btn-like:hover{
		cursor:pointer;
	}
	
	{for $i=0 to $jml_status-1}
		#btn_like{$id_status[$i]}:hover{
			cursor:pointer;
			text-decoration:underline;
		}
		#btn_dislike{$id_status[$i]}:hover{
			cursor:pointer;
			text-decoration:underline;
		}
		#btn_comment{$id_status[$i]}:hover{
			cursor:pointer;
			text-decoration:underline;
		}
	{/for}
</style>

{literal}
<script>
	function ImgError(source){
		source.src = "{$base_url}images/noimages.jpg";
		source.onerror = "";
		return true;
	}
</script>
<script type="text/javascript">
	//jQuery.noConflict();
	jQuery(document).ready(function(){			
		jQuery('textarea').elastic();
		jQuery('textarea').trigger('update');
		
		$(function() {

			$('textarea').focus(function() {
				
			  var status = $('textarea').val();
				$(this).val('');

			});

		 });
		
	});	
</script>
{/literal}

<script>
	$(function() {
	
				
				$("#btnSubmit").button({
					icons: {
						primary: "ui-icon-check",
					},
					text: true
				});
				
				$("#btnLike").button({
					icons: {
						primary: "ui-icon-heart",
					},
					text: true
				});
				$("#btnComment").button({
					icons: {
						primary: "ui-icon-comment",
					},
					text: true
				});
				

		
				{for $i=0 to $jml_status-1}
						$("#btnDelete{$i}").button({
							icons: {
								primary: "ui-icon-trash",
							},
							text: true
						});
						
						$("#btnComment{$id_status[$i]}").button({
							icons: {
								primary: "ui-icon-comment",
							},
							text: true
						});			
						
						
						$("#btn_like{$id_status[$i]}").click(function(){
							$('#space-like-anda{$id_status[$i]}').replaceWith('<span id="space-like-anda{$id_status[$i]}">Anda </span>');
							$('#space-dislike-anda{$id_status[$i]}').replaceWith('<span id="space-dislike-anda{$id_status[$i]}"></span>');
							$('#form-like-status{$id_status[$i]}').submit();
						});
						
						$("#btn_dislike{$id_status[$i]}").click(function(){
							$('#space-dislike-anda{$id_status[$i]}').replaceWith('<span id="space-dislike-anda{$id_status[$i]}">Anda</span>');
							$('#space-like-anda{$id_status[$i]}').replaceWith('<span id="space-like-anda{$id_status[$i]}"></span>');
							$('#form-dislike-status{$id_status[$i]}').submit();
						});
						
						$("#btn_comment{$id_status[$i]}").click(function(){
							$('#comment{$id_status[$i]}').show();
						});
						
						$('#btnComment{$id_status[$i]}').click(function(){		
							$('#comment{$id_status[$i]}').show();

						});
						
						
						$('#new_comment{$id_status[$i]}').shiftenter();
						
						$('#commentForm{$id_status[$i]}').submit(function() {
									$.ajax({
										type: 'POST',
										url: $(this).attr('action'),
										data: $(this).serialize(),
										success: function(data) {
										
										$('#new_comment{$id_status[$i]}').val('');	
										$('#status{$id_status[$i]}').load('sn_posts.php?id={$id_status[$i]}');	
										$('#status{$id_status[$i]}').slideDown('fast');										

											
										}
									})
									
									return false;
									
						});
						
						$('#form-like-status{$id_status[$i]}').submit(function() {
									$.ajax({
										type: 'POST',
										url: $(this).attr('action'),
										data: $(this).serialize(),
										success: function(data) {
											//$('#space-like{$id_status[$i]}').append('Anda');
											//$('#status{$id_status[$i]}').load('sn_comment.php?id={$id_status[$i]}');	
											//$('#status{$id_status[$i]}').show('fast');											
										}
									})
									
									return false;
									
						});
						
						$('#form-dislike-status{$id_status[$i]}').submit(function() {
									$.ajax({
										type: 'POST',
										url: $(this).attr('action'),
										data: $(this).serialize(),
										success: function(data) {
											//$('#space-like{$id_status[$i]}').append('Anda');
											//$('#status{$id_status[$i]}').load('sn_comment.php?id={$id_status[$i]}');	
											//$('#status{$id_status[$i]}').show('fast');											
										}
									})
									
									return false;
									
						});
						

				{/for}			
	});
</script>


	{for $i=0 to $jml_status-1}

	<div>
		<form id="myStatus{$id_status[$i]}" name="myStatus{$id_status[$i]}" style="display:none;">
			<input type="text" id="id_status" name="id_status" value="{$id_status[$i]}" />
		</form>
		<form id="myStatusLike{$id_status[$i]}" name="myStatusLike{$id_status[$i]}" style="display:none;">
			<input type="text" id="id_status" name="id_status" value="{$id_status[$i]}" />
		</form>
		<form id="myStatusComment{$id_status[$i]}" name="myStatusComment{$id_status[$i]}" style="display:none;">
			<input type="text" id="id_status" name="id_status" value="{$id_status[$i]}" />
		</form>
		
		<div id="status{$id_status[$i]}">
		<div style="float:left; width:60px;">
			<img src="{$base_url}foto_mhs/{$username_status[$i]}.JPG" style="height:60px; width:60px; padding-top:10px;" />
		</div>
		<div style="float:left; width:420px; padding-left:10px;" >
				<table width="100%">
					<tr>
						<td>
							<div class="full-width-hack">
								<!--<strong><span style="color:red;">{$id_status[$i]} status ke : {$i}</span> - -->{$nm_pengguna_status[$i]}</strong>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="full-width-hack" style="text-align:justify;">
								<p>{$status[$i]}</p>
							</div>
						</td>
					</tr>
					<tr>
						<td class="ui-widget-content">
							<span id="btn_like{$id_status[$i]}">Suka</span> 
							&nbsp; - &nbsp;
							<span id="btn_dislike{$id_status[$i]}">Tidak Suka</span> 
							 &nbsp; - &nbsp;
							<span id="btn_comment{$id_status[$i]}">Komentari</span>
						</td>
					</tr>
					<tr>
						<td class="ui-widget-content">
							<div class="btn-like">
								<form style="display:none;" action="{$base_url}modul/mhs/is-like.php" id="form-like-status{$id_status[$i]}" name="form-like-status{$id_status[$i]}" method="post">
									<input type="text" name="id_status" value="{$id_status[$i]}" style="display:none"/>
									<input type="text" name="id_pengguna" value="{$id_pengguna}" style="display:none;" />
								</form>
							</div>
							
							<span id="space-likes{$id_status[$i]}">
								<span id="space-like-anda{$id_status[$i]}">
								{for $suka = 0 to $jml_data_sn_status_suka}
									{if $id_sn_status_suka[$suka] eq $id_status[$i]}
										<span id="space-like{$id_status[$i]}">
											{if $nm_pengguna_suka_1stpos[$suka]==$nm_pengguna}
											Anda
											{/if}
										</span>
									{/if}
								{/for}
								</span>
								{for $suka = 0 to $jml_data_sn_status_suka}
									{if $id_sn_status_suka[$suka] eq $id_status[$i]}
										<span id="space-like{$id_status[$i]}" style="display:none;">
											{if $nm_pengguna_suka_1stpos[$suka]==$nm_pengguna}
											Anda
											{/if}
										</span>
										{$nm_pengguna_suka[$suka]}
										&nbsp; Menyukai ini
									{/if}

								{/for}
							</span>
						</td>
					</tr>
					<tr>
						<td class="ui-widget-content">
							<div class="btn-dislike">
								&nbsp;
								<form style="display:none;" action="{$base_url}modul/mhs/sn_is_dislike.php" id="form-dislike-status{$id_status[$i]}" name="form-dislike-status{$id_status[$i]}" method="post">
									<input type="text" name="id_status" value="{$id_status[$i]}" style="display:none"/>
									<input type="text" name="id_pengguna" value="{$id_pengguna}" style="display:none;" />
								</form>
							</div>
							<span id="space-dislike{$id_status[$i]}">&nbsp;
								<span id="space-dislike-anda{$id_status[$i]}">
								{for $tdksuka = 0 to $jml_data_sn_status_tdksuka}
									{if $id_sn_status_tdksuka[$tdksuka] eq $id_status[$i]}
										<span id="space-dislike{$id_status[$i]}">
											{if $nm_pengguna_tdksuka_1stpos[$tdksuka]==$nm_pengguna}
											Anda
											{/if}
										</span>
									{/if}
								{/for}
								</span>
								{for $tdksuka = 0 to $jml_data_sn_status_tdksuka}
									{if $id_sn_status_tdksuka[$tdksuka] eq $id_status[$i]}
										<span id="space-dislike{$id_status[$i]}" style="display:none;">
											{if $nm_pengguna_tdksuka_1stpos[$tdksuka]==$nm_pengguna}
											Anda
											{/if}
										</span>
										{$nm_pengguna_tdksuka[$tdksuka]}
										&nbsp; Tidak menyukai ini
									{/if}

								{/for}
							</span>
						</td>
					</tr>
					<tr>
						<td class="ui-widget-content">
							<div class="btn-comment">
								&nbsp;
							</div>
							<span id="space-comment-count{$id_status[$i]}">&nbsp;</span>
						</td>
					</tr>


					{for $j=0 to $jml_status_balas-1}
						{if $id_status_balas[$j]==$id_status[$i]}
						<div id="comment-box{$id_status[$i]}">
						<tr id="space-comment{$id_status_balas[$j]}">
							<td class="ui-widget-content">
								<div style="float:left; width:35px; height:100%;">
									<img src="{$base_url}foto_mhs/{$username_status_balas[$j]}.JPG" style="width:35px; height:35px;"/>
								</div>
								<div style="float:left; padding-left:10px; width:300px; text-align:justify;">
									<strong>{$nm_pengguna_status_balas[$j]}</strong> &nbsp; {$status_balas[$j]}
								</div>
							</td>
						</tr>
						</div>
						{/if}						
					{/for}
					
					
					<tr id="comment{$id_status[$i]}" style="display:none;">
						<td class="ui-widget-content">
							<div style="float:left; width:35px;">
								<img src="{$base_url}foto_mhs/{$username}.JPG" style="width:35px; height:35px;"/>
							</div>
							<div style="float:left; width:350px; padding-left:10px;">
								<form id="commentForm{$id_status[$i]}" name="commentForm{$id_status[$i]}" action="{$base_url}modul/mhs/update-status.php" method="post" >
									<input type="text" id="id_status_balas" name="id_status_balas" value="{$id_status[$i]}" style="display:none"/>
									<input type="text" id="id_pengguna" name="id_pengguna" value="{$id_pengguna}" style="display:none;" />
									<textarea name="new_comment{$id_status[$i]}" id="new_comment{$id_status[$i]}" style="height:20px;" >Tulis komentar... </textarea>
								</form>
							</div>
						</td>
					</tr>
					
				</table>
			</div>
			</div>
	</div>
	{/for}