<div class="center_title_bar">Insert Pembayaran</div> 
<form method="post" action="insert-pembayaran.php">
    <table>
        <tr>
            <td>NIM Mahasiswa</td>
            <td><input type="text" name="cari_nim"/></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if isset($cari_nim)}
    <table class="detail">
        <tr>
            <th colspan="8" style="text-align: center;">Status Pembayaran</th>
        </tr>
        <tr>
            <th>Nama Biaya</th>
            <th>Besar Biaya</th>
            <th>Status Tagihan Biaya</th>
            <th>Tanggal Bayar</th>
            <th>Nama Bank</th>
            <th>Via Bank</th>
            <th>Nomer Transaksi</th>
            <th>Semester Pembayaran</th>
        </tr>
        {foreach $data_pembayaran as $data}
            {if $data['NM_BIAYA']}
                <tr>
                    <td>{$data['NM_BIAYA']}</td>
                    <td>{$data['BESAR_BIAYA']}</td>
                    <td>{$data['IS_TAGIH']}</td>
                    <td>{$data['TGL_BAYAR']}</td>
                    <td>{$data['NM_BANK']}</td>
                    <td>{$data['NAMA_BANK_VIA']}</td>
                    <td>{$data['NO_TRANSAKSI']}</td>
                    <td>{$data['NM_SEMESTER']} ({$data['TAHUN_AJARAN']}) </td>
                </tr>
            {else}
                <tr>
                    <td colspan="8" style="text-align: center;">Data Pembayaran Kosong</td>
                </tr>
            {/if}

        {/foreach}
        <tr>
            <td colspan="8" style="text-align: center;">
                <p>Biaya Kuliah : <strong>{$data_pembayaran[0]['TOTAL_SPP']}</strong> + Denda Biaya <strong>{if $data_pembayaran[0]['DENDA_BIAYA']==''}0{else} {$data_pembayaran[0]['DENDA_BIAYA']} {/if}</strong><br/>
                    <strong>Total Biaya : {$data_pembayaran[0]['TOTAL_SPP']+$data_pembayaran[0]['TOTAL_DENDA_BIAYA']}</strong></p>
            </td>
        </tr>
    </table>
    <form action="insert-pembayaran.php" method="post">
        <table>
            <tr>
                <th colspan="4">Insert Pembayaran Baru</th>
            </tr>
            <tr>
                <th>Nama</th>
                <th>NIM</th>
                <th>Semester Pembayaran</th>
                <th>Proses</th>
            </tr>
            <tr>
                <td>
                    <input type="hidden" name="insert" value="1"/>
                    <input type="hidden" name="cari_nim" value="{$data_pembayaran[0]['NIM_MHS']}"/>
                    {$data_pembayaran[0]['NM_PENGGUNA']}
                </td>
                <td>{$data_pembayaran[0]['NIM_MHS']}</td>
                <td>
                    <select name="semester">
                        {foreach $data_semester as $data}
                            <option value="{$data['ID_SEMESTER']}" {if $data['ID_SEMESTER']==$data_pembayaran[0]['ID_SEMESTER']}selected="true"{/if}>{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <input type="submit" value="Insert"/>
                </td>
            </tr>
        </table>
    </form>

    <form action="insert-pembayaran.php" method="post">
        <table>
            <tr>
                <th colspan="5">Insert Detail Pembayaran Baru</th>
            </tr>
            <tr>
                <th>Nama</th>
                <th>NIM</th>
                <th>Detail Pembayaran</th>
                <th>Semester Pembayaran</th>
                <th>Proses</th>
            </tr>
            <tr>
                <td>
                    <input type="hidden" name="insert_one" value="1"/>
                    <input type="hidden" name="cari_nim" value="{$data_pembayaran[0]['NIM_MHS']}"/>
                    {$data_pembayaran[0]['NM_PENGGUNA']}
                </td>
                <td>{$data_pembayaran[0]['NIM_MHS']}</td>
                <td>
                    <select name="id_pembayaran">
                        {foreach $data_detail_pembayaran as $data}
                            <option value="{$data['ID_PEMBAYARAN']}">{$data['NM_BIAYA']}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <select name="semester">
                        {foreach $data_semester as $data}
                            <option value="{$data['ID_SEMESTER']}" {if $data['ID_SEMESTER']==$data_pembayaran[0]['ID_SEMESTER']}selected="true"{/if}>{$data['NM_SEMESTER']}({$data['TAHUN_AJARAN']})</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <input type="submit" value="Insert"/>
                </td>
            </tr>
        </table>
    </form>

{/if}