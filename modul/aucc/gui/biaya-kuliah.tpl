<div class="center_title_bar">Varian Biaya Kuliah Mahasiswa</div>
<table>
    <tr>
        <th>NOMER</th>
        <th>TAHUN AKADEMIK</th>
		<th>SEMESTER</th>
        <th>PROGRAM STUDI</th>
        <th>JENJANG</th>
        <th>JALUR</th>
        <th>KELOMPOK BIAYA</th>
        <th>BESAR BIAYA</th>
    </tr>
    {$nomer=1}
    {foreach $data_biaya_kuliah as $data}
        <tr>
            <td>{$nomer++}</td>
            <td>{$data.THN_AKADEMIK_SEMESTER}</td>
			<td>{$data.NM_SEMESTER}</td>
            <td>{$data.NM_PROGRAM_STUDI}</td>
            <td>{$data.NM_JENJANG}</td>
            <td>{$data.NM_JALUR}</td>
            <td>{$data.NM_KELOMPOK_BIAYA}</td>
            <td>{$data.BESAR_BIAYA_KULIAH}</td>
        </tr>
    {/foreach}
</table>