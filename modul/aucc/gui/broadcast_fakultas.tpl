<div class="center_title_bar">HELPDESK SMS GATEWAY</div>

{literal}
    <script>
        $(function() {

		$( "#btn-send" ).button({
			text: true
		});

		$( "#btn-cancel" ).button({
			text: true
		});
			
		$('#btn-send').click(function(){	
			var content =  document.getElementById('pesan').value;
			if(content ==''){
				$( "#dialog:ui-dialog" ).dialog( "destroy" );
			
				$( "#dialog-message" ).dialog({
					modal: true,
					buttons: {
						Ok: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
			else{
				$('#frmUaccBroadcast').submit();
			}
		})
		
		$('#btn-cancel').click(function(){	
			window.history.back();
		})
		
		$('#frmUaccBroadcast').submit(function() {
			var id = $("#fakultas").val();
			var pesan = $('#pesan').val();
			$.ajax({
				type: 'GET',
				url: 'broadcast.php?dest=fakultas&id='+id,
				data: $(this).serialize(),
				success: function(data) {
					$('#wrap').html('Pesan telah dikirim');
				}
			})
			return false;
		});
		
		$("#pesan").attr('maxlength','142');
		
		
        });
		
		
    </script>
{/literal}
<div id="dialog-message" title="Pemberitahuan" style="display:none;">
	<p>
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
		Anda belum mengisi pesan.
	</p>
	<p>
		Silakan mengisi pesan yang ingin anda sampaikan dengan ketentuan jumlah Max. karakter adalah 160.
	</p>
</div>
<div id="wrap">
<form id="frmUaccBroadcast"  method="GET">
    <div class="box">

        <div class="box">
            <div class="note">Kepada FAKULTAS : 
			<select id="fakultas">
				{for $i=0 to $jml_fakultas-1}
				<option value="{$id_fakultas[$i]}">{$nm_fakultas[$i]}</option>
				{/for}
			</select>
	    </div>
			<div class="note">Pesan : </div>
			<div>
				<div style="margin-bottom:10px;width:625px;">
					<textarea id="pesan" name="pesan" style="width:100%; height:50px;"></textarea>
				</div>
				<div>
					<div id="btn-cancel" style="padding:5px;cursor:pointer;" class="disable-ajax">Batal</div>
					<div id="btn-send" style="padding:5px;cursor:pointer;">Kirim</div>
				</div>

			</div>
			<br /><br /><br />
			<div>&copy; SMS GATEWAY SISTEM LANGITAN {$nama_pt|upper}</div>
        </div>
		
    </div>
</form>

</div>