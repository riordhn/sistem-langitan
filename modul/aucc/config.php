<?php
require '../../config.php';
/* TAMBAHAN FUNGSI ALERT DAR NAMBI*/

function alert_error($text, $lebar = 60)
{
    return "<div id='alert' class='ui-widget' style='margin:10px;'>
        <div class='ui-state-error ui-corner-all' style='padding: 5px;width:{$lebar}%;'> 
            <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span> 
                {$text}</p>
        </div>
    </div>";
}

function alert_success($text, $lebar = 60)
{
    return "<div id='alert' class='ui-widget' style='margin:10px;'>
        <div class='ui-state-highlight ui-corner-all' style='padding: 5px;width:{$lebar}%;'> 
            <p><span class='ui-icon ui-icon-check' style='float: left; margin-right: .3em;'></span> 
                {$text}</p>
        </div>
    </div>";
}


if ($user->IsLogged()) {
    // XML Menu
    if ($user->ID_PENGGUNA == '58855') {
        $xml_menu = new SimpleXMLElement(load_file('menu_miko.xml'));
    } else {
        $xml_menu = new SimpleXMLElement(load_file('menu.xml'));
    }

}