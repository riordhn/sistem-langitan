<?php
include "config.php";

//$db->Query("INSERT INTO AUCC.ROLE (NM_ROLE,DESKRIPSI_ROLE,TIPE_ROLE,PATH)  VALUES ('PTPP','Permintaan Tindakan Perbaikan dan Pencegahan','R','/modul/ptpp/')");

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC)
{
	if ($request_method == 'POST')
	{
		$id_pengguna = post('id_pengguna');
		
		$password_temp = trim(post('password_temp'));
		$password_hash_temp = sha1($password_temp);
		$password_must_change = post('must_change') == "on" ? "1" : "0";
		
		// Jika tidak di kosongi
		if ($password_temp != '')
		{
			// update di pengguna
			$result = $db->Query("
				update pengguna set
					password_hash_temp = '{$password_hash_temp}',
					password_must_change = {$password_must_change}
				where id_pengguna = {$id_pengguna}");
				
			// update di email
			$db->Query("update email set password_hash = '{$password_hash_temp}' where id_pengguna = {$id_pengguna}");
		}
		else // jika kosong
		{
			// update di tabel pengguna
			$result = $db->Query("
				update pengguna set
					password_hash_temp = null,
					password_must_change = {$password_must_change}
				where id_pengguna = {$id_pengguna}");
				
			// update di tabel email
			$db->Query("update email set password_hash = (select password_hash from pengguna where pengguna.id_pengguna = email.id_pengguna) where id_pengguna = {$id_pengguna}");
		}
			
		if ($result)
			$smarty->assign('result_message', "Berhasil disimpan");
		else 
			$smarty->assign('result_message', "Gagal disimpan");
	}
	
	if ($request_method == 'GET' or $request_method == 'POST')
	{
		$username = get('username', '');
		
		if ($username != '')
		{
			$db->Query("
				select id_pengguna, username, nm_pengguna, nm_role,
					to_char(last_time_password,'DD/MM/YYYY') as last_time_password,
					to_char(last_time_login,'DD/MM/YYYY') as last_time_login,
					password_hash_temp, password_must_change, email_pengguna
				from pengguna 
				join role on role.id_role = pengguna.id_role
				where username = '{$username}'");
			$pengguna = $db->FetchAssoc();
			
			$smarty->assign('pengguna', $pengguna);
		}
	}

	$smarty->display('status-pengguna.tpl');
}
?>
