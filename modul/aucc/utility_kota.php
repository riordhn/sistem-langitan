<?php 
include '../../config.php';

if ($user->IsLogged() == false || $user->Role() != AUCC_ROLE_AUCC) { 
	echo '<script>window.location = \'/index.php\';</script>';
	exit(); 
}

$id_negara		= get('id_negara', '');
$id_provinsi	= get('id_provinsi', '');

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$nm_kota = trim(post('nm_kota', ''));
	$tipe_dati2 = post('tipe_dati2', '');

	if ($nm_kota != '')
	{
		$db->Query("insert into kota (id_provinsi, nm_kota, tipe_dati2) values ('{$id_provinsi}','{$nm_kota}', '{$tipe_dati2}')");
	}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET' || $_SERVER['REQUEST_METHOD'] == 'POST')
{
	$negara_set = $db->QueryToArray("select * from negara order by nm_negara");
	$smarty->assignByRef('negara_set', $negara_set);
	
	if ($id_negara != '')
	{
		$provinsi_set = $db->QueryToArray("select * from provinsi where id_negara = {$id_negara} order by nm_provinsi");
		$smarty->assignByRef('provinsi_set', $provinsi_set);
	}

	if ($id_provinsi != '')
	{
		$kota_set = $db->QueryToArray("
			select kota.* from kota
			join provinsi on provinsi.id_provinsi = kota.id_provinsi and provinsi.id_negara = {$id_negara}
			where kota.id_provinsi = {$id_provinsi} order by tipe_dati2, nm_kota");
		$smarty->assignByRef('kota_set', $kota_set);
	}

	$smarty->display('utility/kota/index.tpl');
} 

?>