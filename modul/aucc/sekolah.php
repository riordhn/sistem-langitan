<?php
include('config.php');
	if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC){
$mode = get('mode', 'sekolah');
$keyword = get('q');

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
        $result = $db->Query("insert into sekolah (nm_sekolah, id_kota) values (upper('".post('nm_sekolah')."'),".post('id_kota').")");
        if ($result)
            $smarty->assign('added', 'Sekolah berhasil ditambahkan');
        else
            $smarty->assign('added', 'Sekolah gagal ditambahkan');
    }
    
    if (post('mode') == 'edit')
    {
        $result = $db->Query("update sekolah set nm_sekolah = upper('".post('nm_sekolah')."'), id_kota = ".post('id_kota')." where id_sekolah = ".post('id_sekolah'));
        if ($result)
            $smarty->assign('added', 'Sekolah berhasil diedit');
        else
            $smarty->assign('added', 'Sekolah gagal diedit');
    }
    
    if (post('mode') == 'delete')
    {
        $result = $db->Query("delete from sekolah where id_sekolah = ".post('id_sekolah'));
        if ($result)
            $smarty->assign('added', 'Sekolah berhasil dihapus');
        else
            $smarty->assign('added', 'Sekolah gagal dihapus');
    }
}

if ($request_method == 'GET')
{
    if ($mode == 'sekolah' && $keyword != '')
    {
        $smarty->assign('sekolah_set', $db->QueryToArray("
            select id_sekolah, upper(nm_sekolah) nm_sekolah, upper(nm_kota) nm_kota, upper(nm_provinsi) nm_provinsi from sekolah s
            join kota k on k.id_kota = s.id_kota
            join provinsi p on p.id_provinsi = k.id_provinsi
            where (lower(nm_sekolah) like '%".strtolower($keyword)."%') or (lower(nm_kota) like '%".strtolower($keyword)."%') or (lower(nm_provinsi) like '%".strtolower($keyword)."%')
            order by id_negara, nm_provinsi, nm_kota, nm_sekolah"));

    }
    
    if ($mode == 'tambah')
    {
        $smarty->assign('provinsi_set', $db->QueryToArray("select id_provinsi, nm_provinsi from provinsi where id_negara = 114 order by nm_provinsi, id_negara"));
    }
    
    if ($mode == 'get-kota')
    {
        $id_provinsi = get('id_provinsi');
        $smarty->assign('kota_set', $db->QueryToArray("select id_kota, nm_kota||' ['||id_kota||']' nm_kota from kota where id_provinsi = {$id_provinsi} order by nm_kota, id_kota"));
    }
    
    if ($mode == 'edit')
    {
        $id_sekolah = get('id_sekolah');
        
        $sekolah_set = $db->QueryToArray("
            select s.id_sekolah, s.nm_sekolah, s.id_kota, k.id_provinsi from sekolah s
            join kota k on k.id_kota = s.id_kota where s.id_sekolah = {$id_sekolah}");
        $smarty->assign('sekolah', $sekolah_set[0]);
        
        $smarty->assign('provinsi_set', $db->QueryToArray("select id_provinsi, nm_provinsi from provinsi where id_negara = 1 order by nm_provinsi, id_negara"));
        $smarty->assign('kota_set', $db->QueryToArray("select id_kota, nm_kota from kota where id_provinsi = {$sekolah_set[0]['ID_PROVINSI']} order by nm_kota"));
    }
}


$smarty->display("sekolah/{$mode}.tpl");
}
?>
