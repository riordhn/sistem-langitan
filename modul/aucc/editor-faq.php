<?php
include('config.php');
if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC){
$mode = get('mode', 'view');

if ($request_method == 'POST')
{
	if (post('mode') == 'add')
	{
		$db->Parse("insert into faq (pertanyaan, jawaban) values (:pertanyaan, :jawaban)");
		$db->BindByName(':pertanyaan', post('pertanyaan'));
		$db->BindByName(':jawaban', post('jawaban'));
		$result = $db->Execute();
		
		$smarty->assign('result', $result ? 'Data berhasil ditambahkan' : 'Data gagal ditambahkan');
	}
	
	if (post('mode') == 'edit')
	{
		$db->Parse("update faq set pertanyaan = :pertanyaan, jawaban = :jawaban where id_faq = :id_faq");
		$db->BindByName(':pertanyaan', post('pertanyaan'));
		$db->BindByName(':jawaban', post('jawaban'));
		$db->BindByName(':id_faq', post('id_faq'));
		$result = $db->Execute();
		
		$smarty->assign('result', $result ? 'Data berhasil di edit' : 'Data gagal di edit');
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$faq_set = $db->QueryToArray("select * from faq order by waktu_insert");
		$smarty->assign('faq_set', $faq_set);
	}
	
	if ($mode == 'edit')
	{
		$id_faq = get('id_faq', '');
		$faq_set = $db->QueryToArray("select * from faq where id_faq = {$id_faq}");
		$smarty->assign('faq', $faq_set[0]);
	}
	
	if ($mode == 'publish')
	{
		$id_faq = get('id_faq');
		$db->Query("update faq set is_publish = 1 where id_faq = {$id_faq}");
		echo "<script>window.location='#editor-faq!editor-faq.php';</script>";
		exit();
	}
	
	if ($mode == 'unpublish')
	{
		$id_faq = get('id_faq');
		$db->Query("update faq set is_publish = 0 where id_faq = {$id_faq}");
		echo "<script>window.location='#editor-faq!editor-faq.php';</script>";
		exit();
	}
}
$smarty->display("editor/faq/{$mode}.tpl");

}