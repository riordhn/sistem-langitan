<?php
	include 'config.php';
	
	if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC){
	$db->Query("
		SELECT id_fakultas, nm_fakultas FROM fakultas WHERE id_perguruan_tinggi = {$id_pt_user} ORDER BY id_fakultas
	");
	
	$i = 0;
	$jml_fakultas = 0;
	while ($row = $db->FetchRow()){ 
		$id_fakultas[$i] = $row[0];
		$nm_fakultas[$i] = $row[1];
		$i++;
		$jml_fakultas++;
	}
	
	$smarty->assign('jml_fakultas', $jml_fakultas);
	$smarty->assign('nm_fakultas', $nm_fakultas);
	$smarty->assign('id_fakultas', $id_fakultas);
	$smarty->display('broadcast_prodi.tpl');
	}
?>