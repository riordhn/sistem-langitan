<?php

include 'config.php';
include 'class/aucc.class.php';

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC){

$aucc = new AUCC_Modul($db);
if (isset($_POST['cari_nim'])) {
    $nim = str_replace("'", "''", $_POST['cari_nim']);
    $smarty->assign('data_pembayaran', $aucc->detail_pembayaran($nim));
    $smarty->assign('data_bank', $aucc->get_bank());
    $smarty->assign('data_semester', $aucc->get_semester_all());
    $smarty->assign('data_bank_via', $aucc->get_via_bank());
    $smarty->assign('cari_nim', 1);
}
if (isset($_GET['mode'])) {
    $nim = str_replace("'", "''", $_POST['cari_nim']);
    if ($_GET['mode'] == 'biaya') {
        for ($i = 1; $i < $_POST['jumlah']; $i++) {
            $db->Query("UPDATE PEMBAYARAN SET BESAR_BIAYA='{$_POST['besar_biaya' . $i]}',DENDA_BIAYA='{$_POST['denda_biaya' . $i]}',
			IS_TAGIH='{$_POST['is_tagih' . $i]}',ID_BANK='{$_POST['bank'. $i]}',
			ID_BANK_VIA='{$_POST['bank_via'. $i]}',TGL_BAYAR='{$_POST['tgl_bayar'. $i]}',NO_TRANSAKSI='{$_POST['no_transaksi'. $i]}'
			,KETERANGAN='{$_POST['keterangan'. $i]}' WHERE ID_PEMBAYARAN='{$_POST['id' . $i]}'");
            write_log($user->ID_PENGGUNA . " Telah Mengupdate Pembayaran Biaya : " . $_POST['besar_biaya' . $i] . " Denda : " . $_POST['denda_biaya' . $i] . " Dengan ID_PEMBAYARAN : " . $_POST['id' . $i]);
        }
    } else if ($_GET['mode'] == 'status') {
        $db->Query("UPDATE PEMBAYARAN SET IS_TAGIH='{$_POST['is_tagih']}',ID_BANK='{$_POST['bank']}',ID_BANK_VIA='{$_POST['bank_via']}',TGL_BAYAR='{$_POST['tgl_bayar']}',NO_TRANSAKSI='{$_POST['no_transaksi']}',KETERANGAN='{$_POST['keterangan']}' WHERE ID_MHS='{$_POST['id']}' AND ID_SEMESTER='{$_POST['semester']}'");
        write_log($user->ID_PENGGUNA . " Telah Mengupdate Status Pembayaran Bank : " . $_POST['bank'] . "Via Bank : " . $_POST['bank_via'] . " Dengan ID_MHS :" . $_POST['id'] . " Dan Pada Semester :" . $_POST['semester']);
    }
    $smarty->assign('data_pembayaran', $aucc->detail_pembayaran($nim));
    $smarty->assign('data_semester', $aucc->get_semester_all());
    $smarty->assign('data_bank', $aucc->get_bank());
    $smarty->assign('data_bank_via', $aucc->get_via_bank());
    $smarty->assign('cari_nim', 1);
}

$smarty->display('update-pembayaran.tpl');
}
?>
