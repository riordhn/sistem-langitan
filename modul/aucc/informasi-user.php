<?php
include('config.php');

$nama_singkat_pt = $nama_singkat;

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_AUCC){
	header("location: /logout.php");
    exit();
}

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_pengguna = post('id_pengguna');
        $id_role = post('id_role');
        
        $result = $db->Query("update pengguna set id_role = {$id_role} where id_pengguna = {$id_pengguna}");
        
        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }
    
    if (post('mode') == 'add-role')
    {
        $id_pengguna = post('id_pengguna');
        $id_roles = post('id_roles');
        
        foreach ($id_roles as $id_role)
        {
            $db->Query("insert into role_pengguna (id_pengguna, id_role) values ({$id_pengguna}, $id_role)");
        }
    }
    
    if (post('mode') == 'delete-role')
    {
        $id_role_pengguna = post('id_role_pengguna');
        
        $result = $db->Query("delete from role_pengguna where id_role_pengguna = {$id_role_pengguna}");
        
        echo $result ? "1" : "0";
        
        exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $q = strtoupper(get('q', ''));
        
        if (strlen(trim($q)) >= 3)
        {
            $pengguna_set = $db->QueryToArray("
                select p.id_pengguna, p.username, p.nm_pengguna, p.email_pengguna, p.join_table, p.id_role, r.nm_role, count(rp.id_role) jumlah_role
                from pengguna p
                join role r on r.id_role = p.id_role
                left join role_pengguna rp on rp.id_pengguna = p.id_pengguna
                where p.id_perguruan_tinggi = {$id_pt_user} and p.id_role not in (6, 8) and (upper(p.nm_pengguna) like '%{$q}%' or p.username like '{$q}%')
                group by p.id_pengguna, p.username, p.nm_pengguna, p.email_pengguna, p.join_table, p.id_role, r.nm_role
                order by p.nm_pengguna");
            $smarty->assign('pengguna_set', $pengguna_set);
        }
		
		$smarty->display("informasi/user/{$mode}.tpl");
    }
    
    if ($mode == 'edit')
    {
        $id_pengguna = get('id_pengguna');
        
		if(isset($_GET['role'])){
		
			$role = get('role');
			if($role == '3'){
			
				//$username = $user->USERNAME;
				//$username_mhs = substr($username, 0, -2);

				//$id = $username_mhs;
				
				$nama_lengkap_ayah = "";
				$pendidikan_ayah = "";
				$pekerjaan_ayah = "";
				$alamat_ayah = "";
				
				$nama_lengkap_ibu = "";
				$pendidikan_ibu = "";
				$pekerjaan_ibu = "";
				$alamat_ibu = "";
				
				$nm_mhs = "";
				$username = "";
				$kelamin_pengguna = "";
				$tgl_lahir_pengguna = "";
				$tempat_lahir = "";
				$alamat_asal_mhs_kota = ""; 
				$mobile_mhs = "";
				$agama_mhs = "";
				$email_pengguna = ""; 
				$blog_pengguna = "";
				$lulus_dayat = "";
				$nilai_skhun_mhs = "";
				$cita_cita_mhs = "";
				$fakultas_mhs = "";
				$nm_program_studi = "";
				
				$db->query("select mhs.NM_AYAH_MHS, 
							mhs.ALAMAT_AYAH_MHS, 
							payah.nama_pendidikan_akhir as PENDIDIKAN_AYAH_MHS, 
							krj_ayah.nm_pekerjaan as PEKERJAAN_AYAH_MHS, 
							mhs.NM_IBU_MHS, 
							mhs.ALAMAT_IBU_MHS, 
							pabu.nama_pendidikan_akhir as PENDIDIKAN_IBU_MHS, 
							krj_IBU.nm_pekerjaan as PEKERJAAN_IBU_MHS,
							p.NM_PENGGUNA, 
							p.USERNAME, 
							p.KELAMIN_PENGGUNA, 
							to_char(p.TGL_LAHIR_PENGGUNA, 'DD-MM-YYYY'), 
							PROV.NM_PROVINSI, 
							KOTA.NM_KOTA, mhs.ALAMAT_ASAL_MHS, 
							mhs.MOBILE_MHS, 
							a.nm_agama as agama_mhs, 
							p.EMAIL_PENGGUNA, 
							p.BLOG_PENGGUNA, 
							MHS.NILAI_SKHUN_MHS, 
							mhs.CITA_CITA_MHS, 
							mhs.ID_FACEBOOK, mhs.ID_TWITTER,
							f.nm_fakultas as fakultas_mhs, 
							ps.nm_program_studi,
							no_ujian,mhs.THN_LULUS_MHS
							from pengguna p
							left join mahasiswa mhs on mhs.ID_PENGGUNA = p.ID_PENGGUNA
							left join PENDIDIKAN_AKHIR payah on payah.id_pendidikan_akhir = mhs.PENDIDIKAN_AYAH_MHS
							left join PENDIDIKAN_AKHIR pabu on pabu.id_pendidikan_akhir = mhs.PENDIDIKAN_IBU_MHS
							left join PEKERJAAN krj_ayah on KRJ_AYAH.ID_PEKERJAAN = mhs.PEKERJAAN_AYAH_MHS
							left join PEKERJAAN krj_ibu on KRJ_IBU.ID_PEKERJAAN = mhs.PEKERJAAN_IBU_MHS
							left join agama a on a.ID_AGAMA = p.ID_AGAMA
							left join PROGRAM_STUDI ps on ps.ID_PROGRAM_STUDI = mhs.ID_PROGRAM_STUDI
							left join fakultas f on f.id_fakultas = ps.ID_FAKULTAS
							left join PROVINSI prov on PROV.ID_PROVINSI = MHS.LAHIR_PROP_MHS
							left join kota kota on kota.ID_KOTA = mhs.LAHIR_KOTA_MHS
							left join calon_mahasiswa_baru on calon_mahasiswa_baru.id_c_mhs = mhs.id_c_mhs
							where p.id_pengguna = '$id_pengguna'
				
				");
				
				
				$i = 0;
				$jml_data = 0;
				while ($row = $db->fetchrow()){
			//
					$nama_lengkap_ayah = $row[0];
					$alamat_ayah = $row[1];
					$pendidikan_ayah = $row[2];
					$pekerjaan_ayah= $row[3];

					
					$nama_lengkap_ibu = $row[4];
					$alamat_ibu = $row[5];
					$pendidikan_ibu = $row[6];
					$pekerjaan_ibu = $row[7];
					
					$nm_mhs = $row[8];
					$username = $row[9];
					$kelamin_pengguna = $row[10];
					$tgl_lahir_pengguna = $row[11];
					$tempat_lahir_prov = $row[12];
					$tempat_lahir_kota = $row[13];
					$alamat_asal_mhs_kota = $row[14]; 
					$mobile_mhs = $row[15];
					$agama_mhs = $row[16];
					$email_pengguna = $row[17]; 
					$blog_pengguna = $row[18];
					$nilai_skhun_mhs = $row[19];
					$cita_cita_mhs = $row[20];
					$fakultas_mhs = $row[21];
					$nm_program_studi = $row[22];
					$noujian = $row[23];
					$lulus_dayat = $row[24];
					$jml_data++;
					$i++;
				}
				
				
				$fotomahasiswa = "../../foto_mhs/{$nama_singkat_pt}/" . $username . ".jpg";
				if (file_exists($fotomahasiswa)==0) {
					$fotomahasiswa = "../../foto_mhs/{$nama_singkat_pt}/" . $username . ".JPG";
					if (file_exists($fotomahasiswa)==0) {
						$fotomahasiswa = "../../foto_mhs/{$nama_singkat_pt}/".$noujian.".JPG";	
						if (file_exists($fotomahasiswa)==0) {
							$fotomahasiswa = "../../foto_mhs/{$nama_singkat_pt}/foto-tidak-ditemukan.png";	
						}
					}
				}
					
					
				$smarty->assign('nim', $username);
				$smarty->assign('fotomahasiswa', $fotomahasiswa);
				$smarty->assign('nama_ayah', $nama_lengkap_ayah);
				$smarty->assign('pendidikan_ayah', $pendidikan_ayah);
				$smarty->assign('pekerjaan_ayah', $pekerjaan_ayah);
				$smarty->assign('alamat_ayah', $alamat_ayah);
				
				$smarty->assign('nama_ibu', $nama_lengkap_ibu);
				$smarty->assign('pendidikan_ibu', $pendidikan_ibu);
				$smarty->assign('pekerjaan_ibu', $pekerjaan_ibu);
				$smarty->assign('alamat_ibu', $alamat_ibu);
				
			   $smarty->assign('nm_mhs', $nm_mhs);
			   $smarty->assign('kelamin_pengguna', $kelamin_pengguna);
			   $smarty->assign('tgl_lahir_pengguna', $tgl_lahir_pengguna);
			   $smarty->assign('tempat_lahir_prov', $tempat_lahir_prov);
			   $smarty->assign('tempat_lahir_kota', $tempat_lahir_kota);
			   $smarty->assign('alamat_asal_mhs_kota', $alamat_asal_mhs_kota); 
			   $smarty->assign('mobile_mhs', $mobile_mhs);
			   $smarty->assign('agama_mhs', $agama_mhs);
			   $smarty->assign('email_pengguna', $email_pengguna); 
			   $smarty->assign('blog_pengguna' , $blog_pengguna);
			   $smarty->assign('lulus_dayat', $lulus_dayat);
			   $smarty->assign('nilai_skhun_mhs', $nilai_skhun_mhs);
			   $smarty->assign('cita_cita_mhs', $cita_cita_mhs);
			   $smarty->assign('fakultas_mhs', $fakultas_mhs);
			   $smarty->assign('nm_program_studi', $nm_program_studi);
				
			   $smarty->display("informasi/user/mhs.tpl");
			   
			}
			
			else if($role == '4'){
				include 'class/dosen.class.php';
				$info_dosen = new info_dosen($db, $id_pengguna);
				$data_info_dosen = array();
				$smarty->assign('data_info_dosen', $info_dosen->set());							
				$smarty->display("informasi/user/dosen.tpl");
			}
			
			else if($role !='3' or $role !='4'){
				include 'class/pegawai.class.php';
				$info_pegawai = new pegawai($db, $id_pengguna);
				$data_info_pegawai = array();
				$smarty->assign('data_info_pegawai', $info_pegawai->set());	
				
				//print_r($info_pegawai->set());
				$smarty->display("informasi/user/pegawai.tpl");
			}
		
		}else{
			$pengguna = $db->QueryToArray("
				select p.id_pengguna, p.username, p.nm_pengguna, p.join_table, p.id_role, se1, sp.id_session from pengguna p
				left join session_pengguna sp on sp.id_pengguna = p.id_pengguna
				where p.id_pengguna = {$id_pengguna}");
			$pengguna[0]['SE1'] = 'rahasia';//$user->Decrypt($pengguna[0]['SE1']);
			$smarty->assign('pengguna', $pengguna[0]);
			
			
			$role_pengguna_set = $db->QueryToArray("
				select rp.id_role_pengguna, rp.id_role, tr.nm_template, r.nm_role from role_pengguna rp
				join role r on r.id_role = rp.id_role
				left join template_role tr on tr.id_template_role = rp.id_template_role
				where rp.id_pengguna = {$id_pengguna}");
			$smarty->assign('role_pengguna_set', $role_pengguna_set);
		}
	}
    
    if ($mode == 'add-role')
    {
        $id_pengguna = get('id_pengguna');
        
        $pengguna = $db->QueryToArray("select p.id_pengguna, p.nm_pengguna from pengguna p where p.id_pengguna = {$id_pengguna}");
        $smarty->assign('pengguna', $pengguna[0]);
        
        $role_set = $db->QueryToArray("select * from aucc.role where id_role not in (select id_role from aucc.role_pengguna where id_pengguna = {$id_pengguna}) and id_role not in (3,6,28)");
        $smarty->assign('role_set', $role_set);
    }
}

  //$smarty->display("informasi/user/{$mode}.tpl");
?>
