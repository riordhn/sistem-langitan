<?php

include 'config.php';
echo '
<div class="center_title_bar">Cek Tagihan Host To Host Mahasiswa Lama</div> 
<form method="post" action="cek-pembayaran.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">INPUT</td>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM BANK: <input type="text" name="nim" value="' . $_POST["nim"] . '"></td>
            <td>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" name="ok" value="Lihat">
                <input type="hidden" name="ok" value="ok"/>
            </td>
        </tr>
    </table>
</form>
';


if ($_POST["nim"] && $_POST["ok"]) {
    $nim = addslashes($_POST['nim']);
    $waktu_sekarang = date("YmdHis");
    $db->Query("
        SELECT P.NM_PENGGUNA,M.ID_MHS,M.NIM_MHS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS,J.ID_JENJANG,PS.ID_PROGRAM_STUDI,M.STATUS_CEKAL
        FROM MAHASISWA M
        JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA AND P.ID_PERGURUAN_TINGGI = {$id_pt_user}
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
        JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
        WHERE M.NIM_BANK='{$nim}'");
    $cek_data_mhs = $db->FetchAssoc();
    $db->Query("
        SELECT TO_CHAR(PB.TGL_AWAL_PERIODE_BAYAR,'YYYYMMDDHH24MISS') TGL_AWAL,TO_CHAR(PB.TGL_AKHIR_PERIODE_BAYAR,'YYYYMMDDHH24MISS') TGL_AKHIR
        FROM PERIODE_BAYAR PB
        JOIN SEMESTER S ON PB.ID_SEMESTER=S.ID_SEMESTER AND S.ID_PERGURUAN_TINGGI = {$id_pt_user}
        WHERE S.STATUS_AKTIF_PEMBAYARAN='True'");
    $cek_periode_bayar = $db->FetchAssoc();
    $db->Query("
        SELECT TO_CHAR(BATAS_BAYAR,'YYYYMMDDHH24MISS') BATAS_BAYAR FROM MAHASISWA WHERE ID_MHS='{$cek_data_mhs['ID_MHS']}'
        ");
    $cek_batas_bayar = $db->FetchAssoc();
    $db->Query("
        SELECT SUM(BESAR_BIAYA+DENDA_BIAYA) TAGIHAN
        FROM PEMBAYARAN
        WHERE IS_TAGIH='Y' AND ID_STATUS_PEMBAYARAN=2 AND TGL_BAYAR IS NULL AND ID_MHS='{$cek_data_mhs['ID_MHS']}'");
    $cek_tagihan = $db->FetchAssoc();
    if ($cek_data_mhs != '') {
        if ($cek_periode_bayar != '' || $cek_batas_bayar != '') {
            if ($cek_tagihan['TAGIHAN'] != '') {
                if (($waktu_sekarang > $cek_periode_bayar['TGL_AWAL'] && $waktu_sekarang < $cek_periode_bayar['TGL_AKHIR']) || ($waktu_sekarang < $cek_batas_bayar['BATAS_BAYAR'] && $cek_batas_bayar['BATAS_BAYAR'] != '')) {
                    if ($cek_data_mhs['STATUS_CEKAL'] != '1') {
                        echo "
                        <table class='ui-widget-content' style='width:40%'>
                            <tr class='ui-widget-header'>
                                <th colspan='2'>TAGIHAN MAHASISWA</th>
                            </tr>
                            <tr>
                                <td>NIM</td>
                                <td>{$cek_data_mhs['NIM_MHS']}</td>
                            </tr>
                            <tr>
                                <td>NAMA</td>
                                <td>{$cek_data_mhs['NM_PENGGUNA']}</td>
                            </tr>
                            <tr>
                                <td>PRODI</td>
                                <td>{$cek_data_mhs['NM_JENJANG']} {$cek_data_mhs['NM_PROGRAM_STUDI']}</td>
                            </tr>
                            <tr>
                                <td>FAKULTAS</td>
                                <td>{$cek_data_mhs['NM_FAKULTAS']}</td>
                            </tr>
                            <tr>
                                <td>TAGIHAN</td>
                                <td>{$cek_tagihan['TAGIHAN']}</td>
                            </tr>
                        </table>";
                    } else {
                        echo alert_error("Status Mahasiswa Di Cekal, Hubungi pendidikan.");
                        $history_cekal = $db->QueryToArray("
                            SELECT CK.*,S.NM_SEMESTER,S.TAHUN_AJARAN,S.THN_AKADEMIK_SEMESTER
                            FROM CEKAL_PEMBAYARAN CK
                            JOIN SEMESTER S ON S.ID_SEMESTER=CK.ID_SEMESTER
                            WHERE CK.ID_MHS='{$cek_data_mhs['ID_MHS']}'
                            ORDER BY S.THN_AKADEMIK_SEMESTER DESC, S.NM_SEMESTER
                            ");
                        echo "
                        <table class='ui-widget-content' style='width:90%'>
                            <tr class='ui-widget-header'>
                                <th colspan='5'>HISTORY PERUBAHAN CEKAL</th>
                            </tr>
                            <tr class='ui-widget-header'>
                                <th>SEMESTER</th>
                                <th>TGL PERUBAHAN</th>
                                <th>KETERANGAN</th>
                                <th>STATUS CEKAL</th>
                            </tr>
                            ";
                        foreach ($history_cekal as $h) {
                            echo "
                                <tr>
                                    <td>{$h['NM_SEMESTER']} {$h['TAHUN_AJARAN']}</td>
                                    <td>{$h['TGL_UBAH']}</td>
                                    <td>{$h['KETERANGAN']} {$h['TAHUN_AJARAN']}</td>
                                    <td>" . ($h['STATUS_CEKAL'] == 1 ? 'Cekal' : 'Bebas Cekal') . "</td>
                                </tr>
                                ";
                        }
                        echo "</table>";
                    }
                } else {
                    echo alert_error("Periode Bayar bermasalah atau Batas Bayar tidak benar.");
                }
            } else {
                echo alert_error("Tagihan tidak ada, Tagihan adalah pembayaran yang statusnya :<br/>1. Tagihkan = YA<br/>2. Status Pembayaran = Belum Bayar<br/>3. Tanggal Bayar Masih Kosong");
            }
        }
    } else {
        echo alert_error("Data Mahasiswa Tidak Ditemukan.");
    }
}
?>