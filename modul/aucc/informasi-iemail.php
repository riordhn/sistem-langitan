<?php
include "config.php";

if ($user->IsLogged()  == false || $user->Role() != AUCC_ROLE_AUCC)
{
	echo "TIDAK PUNYA AKSES";
	exit();
}

if ($request_method == 'POST')
{
	function get_url_content($url){
		$ch = curl_init();
		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // force
		curl_setopt($ch, CURLOPT_HEADER, false); // force
		// grab URL and pass it to the browser
		$return = curl_exec($ch);
		// close cURL resource, and free up system resources
		curl_close($ch);
		return $return;
	}

	if (post('mode') == 'email-jabatan')
	{
		$email = post('email');
		$password = post('password');
		
		// ambil data email
		$email_set = $db->QueryToArray("select * from email where email = '{$email}'");
		
		// Cek jika sudah ada / belum
		if (count($email_set) > 0)
		{
			$pdikirim = $email.';'.$password;
			$hasilnya = get_url_content("http://10.0.110.43/modul/mhs/includes/gaps_emailunair/update_password.php?param=".base64_encode($pdikirim));

			if(empty($hasilnya)) {
				$smarty->assign('message', 'Password Email jabatan gagal di update 1 ');
			}else{
				$hasil = json_decode($hasilnya);
				if($hasil->StatusCode == '200') {
					$password_hash = sha1($password);
					$result = $db->Query("update email set password_hash = '{$password_hash}' where email = '{$email}'");
					
					if ($result)
						$smarty->assign('message', 'Password Email jabatan berhasil di update');
					else
						$smarty->assign('message', 'Password Email jabatan gagal di update 2');

				}else if($hasil->StatusCode == '100') {
					$smarty->assign('message', 'Password Email jabatan gagal di update. Gaps Error-1');
				}else if($hasil->StatusCode == '101') {
					$smarty->assign('message', 'Password Email jabatan gagal di update. Server Gaps Sibuk');
				}else if($hasil->StatusCode == '103') {
					$smarty->assign('message', 'Password Email jabatan gagal di update, email GAPS tidak ada');
				}else if($hasil->StatusCode == '104') {
					$smarty->assign('message', 'Password Email jabatan gagal di update. Gaps Error-2');
				}else{
					$smarty->assign('message', 'Password Email jabatan gagal di update. Gaps error : '.$hasil->StatusCode.' ');
				}
			}
		}
		else
		{
			$namadepan="Universitas"; $namabelakang="Airlangga";
			$pdikirim = $email.';'.$password.';'.$namadepan.';'.$namabelakang;
			$hasilnya = get_url_content("http://10.0.110.43/modul/mhs/includes/gaps_emailunair/create_account.php?param=".base64_encode($pdikirim));
			if(empty($hasilnya)) {
				$smarty->assign('message', 'Email jabatan gagal di input 1 ');
			}else{
				$hasil = json_decode($hasilnya);
				if($hasil->StatusCode == '200' or $hasil->StatusCode == '102' ) {
					$password_hash = sha1($password);
					$result = $db->Query("insert into email (email, password_hash) values ('{$email}','{$password_hash}')");
					
					if ($result and $hasil->StatusCode == '200')
						$smarty->assign('message', 'Email jabatan berhasil di insert cyber & Gaps');
					else if ($result and $hasil->StatusCode == '102')
						$smarty->assign('message', 'Email jabatan berhasil di insert cyber & Gaps sudah ada');
					else
						$smarty->assign('message', 'Email jabatan gagal di input 2');
					
				}else if($hasil->StatusCode == '100') {
					$smarty->assign('message', 'Email jabatan gagal di input. Gaps Error-1');
				}else if($hasil->StatusCode == '101') {
					$smarty->assign('message', 'Email jabatan gagal di input. Server Gaps Sibuk');
				}else if($hasil->StatusCode == '103') {
					$smarty->assign('message', 'Email jabatan gagal di input. Gaps Error-2');
				}else if($hasil->StatusCode == '104') {
					$smarty->assign('message', 'Email jabatan gagal di input. Gaps Error-3');
				}else{
					$smarty->assign('message', 'Email jabatan gagal di input. Gaps error : '.$hasil->StatusCode.' ');
				}
			}
		}
	}
	
	if (post('mode') == 'email-pengguna')
	{
		$username	= post('username');
		$email 		= post('email');
		$password 	= post('password');

		// cek apakah nip pengguna ada
		$pengguna_cek = $db->QueryToArray("SELECT id_pengguna, password_hash, email_pengguna from pengguna where id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI} AND username = '{$username}' ");
		// email ganda cek
		$ganda_cek = $db->QueryToArray("SELECT id_pengguna from pengguna where id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI} AND email_pengguna='{$email}' and username != '{$username}'");
		
		if (count($pengguna_cek) == 0){
			$smarty->assign('message', 'Pengguna tidak ditemukan');
		}else if (count($ganda_cek) > 0){
			$smarty->assign('message', 'Email sudah digunakan ');
		}else if($pengguna_cek[0]['EMAIL_PENGGUNA'] == $email) {
			$smarty->assign('message', 'Email pengguna sudah yang terbaru ');
		}else if ( (count($pengguna_cek) > 0) and (count($ganda_cek) == 0) and ($pengguna_cek[0]['EMAIL_PENGGUNA'] != $email) ) {

			// by Fathoni
			// akses ke gapps sementara dimatikan karena masih belum jalan

			$db->BeginTransaction();
				
			// update pengguna
			$db->Query("update pengguna set email_pengguna = '{$email}' where username = '{$username}'");
			
			// delete email lama
			$db->Query("delete from email where id_pengguna = {$pengguna_cek[0]['ID_PENGGUNA']}");
			
			// re-insert email
			$db->Query("insert into email (email, password_hash, id_pengguna) values ('{$email}', '{$password_hash}', {$pengguna_cek[0]['ID_PENGGUNA']})");
			
			$result = $db->Commit();

			$smarty->assign('message', 'Email pengguna berhasil di update');

			/** AKSES GAPPS SEMENTARA DI CLOSE 
			$password_hash = $pengguna_cek[0]['PASSWORD_HASH'];
			$pdikirim = $pengguna_cek[0]['EMAIL_PENGGUNA'].';'.$email;
			$hasilnya = get_url_content("http://10.0.110.43/modul/mhs/includes/gaps_emailunair/rename_email.php?param=".base64_encode($pdikirim));
			if(empty($hasilnya)) {
				$smarty->assign('message', 'Email pengguna gagal di update 1 ');
			}else{
				$hasil = json_decode($hasilnya);
				if($hasil->StatusCode == '200' or $hasil->StatusCode == '102' or $hasil->StatusCode == '103') {
					// 102 --> EmailExists(102) - Request gagal karena email address (tujuan) sudah terdaftar di Google Apps
					// 103 --> EmailDoesNotExist(103) - Request gagal karena email address (yang mau diubah) tidak ditemukan di Google Apps
					$db->BeginTransaction();
				
					// update pengguna
					$db->Query("update pengguna set email_pengguna = '{$email}' where username = '{$username}'");
					
					// delete email lama
					$db->Query("delete from email where id_pengguna = {$pengguna_cek[0]['ID_PENGGUNA']}");
					
					// re-insert email
					$db->Query("insert into email (email, password_hash, id_pengguna) values ('{$email}', '{$password_hash}', {$pengguna_cek[0]['ID_PENGGUNA']})");
					
					$result = $db->Commit();
					
					if ($result){
						if($hasil->StatusCode == '200') {
							$smarty->assign('message', 'Email pengguna (db & gaps) berhasil di update');
						}else if($hasil->StatusCode == '102') {
							$smarty->assign('message', 'Email pengguna (db) berhasil di update. Gaps sudah ada : 102.');
						}else if($hasil->StatusCode == '103') {
							$smarty->assign('message', 'Email pengguna (db) berhasil di update. Gaps sudah ada : 103.');
						}
					}else {
						$smarty->assign('message', 'Email pengguna (db) gagal di update. Status gaps : '.$hasil->StatusCode);
					}
				}else if($hasil->StatusCode == '100') {
					$smarty->assign('message', 'Email pengguna gagal di update. Gaps Error-1');
				}else if($hasil->StatusCode == '101') {
					$smarty->assign('message', 'Email pengguna gagal di update. Server Gaps Sibuk');
				//}else if($hasil->StatusCode == '103') {
				//	$smarty->assign('message', 'Gagal, email lama belum dibuat di GAPS');
				}else if($hasil->StatusCode == '104') {
					$smarty->assign('message', 'Email pengguna gagal di update. Gaps Error-2<br>'.base64_encode($pdikirim));
				}else{
					$smarty->assign('message', 'Email pengguna gagal di update. Gaps error : '.$hasil->StatusCode.' <br>'.base64_encode($pdikirim));
				}
			}
			*/
		}
	}
	if (post('mode') == 'email-pengguna-buatbaru')
	{
		$username = post('username');
		$email = post('email');
		$password = "12345678";

		// cek apakah nip pengguna ada
		$pengguna_cek = $db->QueryToArray("select id_pengguna, password_hash, email_pengguna, nm_pengguna from pengguna where username = '{$username}'");
		// cek apakah pengguna sudah punya email
		$pengguna_cek2 = $db->QueryToArray("select id_pengguna from pengguna where username = '{$username}' and email_pengguna is null");
		// email ganda cek
		$ganda_cek = $db->QueryToArray("select id_pengguna from pengguna where email_pengguna='{$email}' and username != '{$username}'");
		
		if (count($pengguna_cek) == 0){
			$smarty->assign('message', 'Pengguna tidak ditemukan');
		}else if (count($pengguna_cek2) == 0){
			$smarty->assign('message', 'Pengguna sudah memiliki email');
		}else if (count($ganda_cek) > 0){
			$smarty->assign('message', 'Email GANDA !!!');
		}else if ( (count($pengguna_cek) > 0) and (count($pengguna_cek2) > 0) and (count($ganda_cek) == 0) ) {
			$tmpnama = explode(" ",$pengguna_cek[0]['NM_PENGGUNA']);
			if(count($tmpnama) > 1) {
				$namadepan = $tmpnama[0];
				$namabelakang = $tmpnama[1];
			}else if(count($tmpnama) == 1) {
				$namadepan = $tmpnama[0];
				$namabelakang = $tmpnama[0];
			}else {
				$namadepan = 'Civitas';
				$namabelakang = 'Akademika';
			}
			if(trim($namadepan)=='') {
				$namadepan="Civitas";
			}
			if(trim($namabelakang)=='') {
				$namabelakang="Akademika";
			}

			$pdikirim = $email.';'.$password.';'.$namadepan.';'.$namabelakang;
			$hasilnya = get_url_content("http://10.0.110.43/modul/mhs/includes/gaps_emailunair/create_account.php?param=".base64_encode($pdikirim));
			if(empty($hasilnya)) {
				$smarty->assign('message', 'Email pengguna gagal di input 1 ');
			}else{
				$hasil = json_decode($hasilnya);
				if($hasil->StatusCode == '200') {
					// 102 --> EmailExists(102) - Request gagal karena email address  sudah terdaftar di Google Apps
					$db->BeginTransaction();
				
					// update pengguna
					$db->Query("update pengguna set email_pengguna = '{$email}' where username = '{$username}'");
					
					// delete email lama
					$db->Query("delete from email where id_pengguna = {$pengguna_cek[0]['ID_PENGGUNA']}");
					
					// re-insert email
					$db->Query("insert into email (email, password_hash, id_pengguna) values ('{$email}', '{$password}', {$pengguna_cek[0]['ID_PENGGUNA']})");
					
					$result = $db->Commit();
					
					if ($result){
						$smarty->assign('message', 'Email pengguna (db & gaps) berhasil di input');
					}else {
						$smarty->assign('message', 'Email pengguna (db) gagal di input. Status gaps : '.$hasil->StatusCode);
					}
				}else if($hasil->StatusCode == '100') {
					$smarty->assign('message', 'Email pengguna gagal di input. Gaps Error-1');
				}else if($hasil->StatusCode == '101') {
					$smarty->assign('message', 'Email pengguna gagal di input. Server Gaps Sibuk');
				}else if($hasil->StatusCode == '102') {
					$smarty->assign('message', 'Email pengguna gagal di input. Email sudah ada di GAPS');
				}else if($hasil->StatusCode == '103') {
					$smarty->assign('message', 'Email pengguna gagal di input. Gaps Error-2');
				}else if($hasil->StatusCode == '104') {
					$smarty->assign('message', 'Email pengguna gagal di input. Gaps Error-3<br>'.base64_encode($pdikirim));
				}else{
					$smarty->assign('message', 'Email pengguna gagal di input. Gaps error : '.$hasil->StatusCode.' ');
				}
			}
		}else{
			$smarty->assign('message', 'tidak tau');
		}
	}
}

$smarty->display('informasi/iemail/view.tpl');
?>