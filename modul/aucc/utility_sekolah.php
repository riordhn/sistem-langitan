<?php 
include '../../config.php';

if ($user->IsLogged() == false || $user->Role() != AUCC_ROLE_AUCC) { 
	echo '<script>window.location = \'/index.php\';</script>';
	exit(); 
}

$id_negara		= get('id_negara', '');
$id_provinsi	= get('id_provinsi', '');
$id_kota		= get('id_kota', '');

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$nm_sekolah = str_replace("'", "''", trim(post('nm_sekolah', '')));

	if ($nm_sekolah != '')
	{
		$db->Query("insert into sekolah (id_negara, id_provinsi, id_kota, nm_sekolah) values ({$id_negara}, {$id_provinsi}, {$id_kota}, '{$nm_sekolah}')");
	}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET' || $_SERVER['REQUEST_METHOD'] == 'POST')
{
	$negara_set = $db->QueryToArray("select * from negara order by nm_negara");
	$smarty->assignByRef('negara_set', $negara_set);
	
	if ($id_negara != '')
	{
		$provinsi_set = $db->QueryToArray("select * from provinsi where id_negara = {$id_negara} order by nm_provinsi");
		$smarty->assignByRef('provinsi_set', $provinsi_set);
	}

	if ($id_provinsi != '')
	{
		$kota_set = $db->QueryToArray("select * from kota where id_provinsi = {$id_provinsi} order by tipe_dati2, nm_kota");
		$smarty->assignByRef('kota_set', $kota_set);
	}

	if ($id_kota != '')
	{
		$sekolah_set = $db->QueryToArray("select * from sekolah where id_kota = {$id_kota} order by nm_sekolah");
		$smarty->assignByRef('sekolah_set', $sekolah_set);
	}

	$smarty->display('utility/sekolah/index.tpl');
} 

?>
