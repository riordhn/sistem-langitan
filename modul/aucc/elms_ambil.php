<?php
require '../../config.php';

	$db = new MyOracle();

	//tes koneksi
	if ($db->conn == false){
		echo OCIError($db->conn)."Not Connected!";
		exit;
	} else {
		// echo "<br>success connecting to my oracle databases";
	} 
	//mengambil koneksi dari class
	$conn = $db->conn;

date_default_timezone_set('Asia/Jakarta');      // Timezone set ke Asia/Jakarta


if(!isset($_POST["ok"])) {
	echo '
	<script type="text/javascript">
		function cek(field, list){
			if(field.checked){ 
				cek_semua(list,1); 
			}else{ 
				cek_semua(list,0); 
			}
		}
		function cek_semua (field, value){
			var len = document.form2.elements[field].length;
			for(i=0; i<len; i++){
				document.form2.elements[field][i].checked = value;
			}
		}
	</script>

	';
	echo '
	<form name="form2" method="post" action="">
	<table cellpadding=5 cellspacing=0 border=1>
	<tr>
		<td>Urut</td>
		<td>NIM</td>
		<td>Nama</td>
		<td>Email</td>
		<td>Waktu Daftar</td>
		<td><input type="checkbox" name="ceksemua" onclick="cek(this,\'centang[]\')"></td>
	</tr>
	';
		$query = "select a.id_elms_daftar, b.username, b.nm_pengguna, b.email_pengguna, to_char(a.tgl_daftar,'DD-MM-YYYY HH24:MI:SS')
		from aucc.elms_daftar a, aucc.pengguna b
		where a.id_pengguna=b.id_pengguna and a.status_proses is null";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt); $urut=0;
		while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
			$urut++;
			echo '
			<tr>
				<td>'.$urut.'</td>
				<td>'.$r[1].'</td>
				<td>'.$r[2].'</td>
				<td>'.$r[3].'</td>
				<td>'.$r[4].'</td>
				<td><input type="checkbox" name="centang[]" value="'.$r[0].'"></td>
			</tr>
			';
		}
	echo '
	<tr>
		<td colspan=6 align=center><input type="submit" name="ok" value="Proses"></td>
	</tr>
	</table>
	</form>
	';
}else{
	$dicentang = $_POST["centang"];
	if(count($dicentang)>0) {
		// include all files needed
			require_once('../../modul/mhs/includes/bank/excel/OLEwriter.php');
			require_once('../../modul/mhs/includes/bank/excel/BIFFwriter.php');
			require_once('../../modul/mhs/includes/bank/excel/Worksheet.php');
			require_once('../../modul/mhs/includes/bank/excel/Workbook.php');

		//create 'header' function. if called, this will tell the browser that the file returned is an excel document
			function HeaderingExcel($filename) {
			  header("Content-type: application/vnd.ms-excel");
			  header("Content-Disposition: attachment; filename=$filename" );
			  header("Expires: 0");
			  header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
			  header("Pragma: public");
			}

		// HTTP headers
			HeaderingExcel('daftar_elms.xls'); //call the function above

		// Creating a workbook instance
			$workbook = new Workbook("-");

		// woksheet 1
			$worksheet1 =& $workbook->add_worksheet('Daftar ELMS');

			$worksheet1->set_zoom(100); //75% zoom
			$worksheet1->set_portrait();
			$worksheet1->set_paper(9); //set A4

			$worksheet1->set_column(0, 0, 10);
			$worksheet1->set_column(0, 1, 10);
			$worksheet1->set_column(0, 2, 10);
			$worksheet1->set_column(0, 3, 10);
			$worksheet1->set_column(0, 4, 10);
		  
			$worksheet1->write_string(0,0, "URUT");
			$worksheet1->write_string(0,1, "NIM");
			$worksheet1->write_string(0,2, "NAMA");
			$worksheet1->write_string(0,3, "EMAIL");
			$worksheet1->write_string(0,4, "WAKTU DAFTAR");
		
		$query = "select a.id_elms_daftar, b.username, b.nm_pengguna, b.email_pengguna, to_char(a.tgl_daftar,'DD-MM-YYYY HH24:MI:SS')
		from aucc.elms_daftar a, aucc.pengguna b
		where a.id_pengguna=b.id_pengguna and a.status_proses is null";
		//echo $query;
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt); $urut=0; $baris=0;
		while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
			$urut++; $baris++;
			$worksheet1->write_string($baris,0, $urut);
			$worksheet1->write_string($baris,1, $r[1]);
			$worksheet1->write_string($baris,2, $r[2]);
			$worksheet1->write_string($baris,3, $r[3]);
			$worksheet1->write_string($baris,4, $r[4]);

			$query2 = "update aucc.elms_daftar set status_proses='1' where id_elms_daftar='".$r[0]."'";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
		}
		$workbook->close();
	}
}


?>