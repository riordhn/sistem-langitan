<?php

//membuat class info dosen 
class info_dosen{

    public $db;
    public $id_pengguna;

    function __construct($db, $id_pengguna) {
        //construct variable database
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
    }

    function get_kota() {
        $data_kota = array();
        $provinsi = $this->db->QueryToArray("SELECT * FROM PROVINSI WHERE ID_NEGARA=114 ORDER BY NM_PROVINSI");
        foreach ($provinsi as $data) {
            array_push($data_kota, array(
                'nama' => $data['NM_PROVINSI'],
                'kota' => $this->db->QueryToArray("SELECT * FROM KOTA WHERE ID_PROVINSI='{$data['ID_PROVINSI']}' ORDER BY NM_KOTA, TIPE_DATI2")
            ));
        }
        return $data_kota;
    }

    function get_agama() {
        return $this->db->QueryToArray("SELECT * FROM AGAMA ORDER BY ID_AGAMA");
    }

    function set() {
        // integrasi dg blog

        $this->db->Query("select NIP_DOSEN from DOSEN where id_pengguna='" . $this->id_pengguna . "'");
        $tmp_nim = $this->db->FetchAssoc();
        $nip3 = $tmp_nim['NIP_DOSEN'];

        /*$DBhost = "10.0.110.1";
        $DBuser = "lihat_blogmhs";
        $DBpass = "lihat_blogmhs";
        $DBName = "dbwebmhs";
        mysql_connect($DBhost, $DBuser, $DBpass) or die("salah");
        mysql_select_db("$DBName") or die("Tidak konek database");

        $mhs_blog = "";
        $q = mysql_query("select subdomainMhs from tb_mahasiswa where nim='" . $nip3 . "'");
        while ($r = mysql_fetch_row($q)) {
            $mhs_blog = $r[0] . '.web.unair.ac.id';
        }
        if (strlen($mhs_blog) == 0) {
            $mhs_blog = "Registrasi Blog";
        }

        $in_jam = date("H");
        $in_menit = date("i");
        $in_detik = date("s");
        $waktu = $in_jam . $in_menit . $in_detik;
		$ipnya = $_SERVER["REMOTE_ADDR"];
		$ipnya_tmp = explode(".",$ipnya);

        //$kode = md5("bam" . $nip3 . "bang" . $waktu);
		$kode = md5($ipnya_tmp[0].$ipnya_tmp[3]."bam".$nip3."bang".$waktu.$ipnya_tmp[2].$ipnya_tmp[1]);
        $kode2 = substr($kode, 0, 2) . $in_jam . substr($kode, 2, 14) . $in_menit . substr($kode, 16, 8) . $in_detik . substr($kode, 24);


        $link_blog = "http://web.unair.ac.id/checkLogin.php?loginid=" . $nip3 . "&kode=" . $kode2;*/



        $provinsi = $this->db->QueryToArray("SELECT EMAIL_PENGGUNA FROM PENGGUNA WHERE ID_PENGGUNA='$this->id_pengguna'");
        foreach ($provinsi as $data) {
            $nama_email = $data['EMAIL_PENGGUNA'];
        }
        $link_email = substr($nama_email, strpos($nama_email, '@', 1) + 1);


        //load data info dosen dari database
        $this->db->Query("SELECT P.USERNAME,P.GELAR_DEPAN,P.GELAR_BELAKANG,P.NM_PENGGUNA AS NAMA,TO_CHAR(P.TGL_LAHIR_PENGGUNA,'DD / MONTH/ YYYY') AS TGL_LAHIR,P.KELAMIN_PENGGUNA AS KELAMIN,P.EMAIL_PENGGUNA AS EMAIL, '" . $mhs_blog . "' as BLOG_PENGGUNA,AG.NM_AGAMA AS AGAMA,
        P.ID_KOTA_LAHIR,D.NIP_DOSEN AS NIP,F.NM_FAKULTAS AS FAKULTAS, PR.NM_PROGRAM_STUDI AS PRODI,D.ALAMAT_RUMAH_DOSEN AS ALAMAT_RUMAH,
        D.ALAMAT_KANTOR_DOSEN AS ALAMAT_KANTOR,D.TLP_DOSEN AS TELP,D.MOBILE_DOSEN AS MOBILE,D.THN_LULUS_DOSEN AS TAHUN_LULUS,H.NM_ROLE AS JENIS_PEGAWAI,
        JP.NM_JABATAN_PEGAWAI AS JABATAN,G.NM_GOLONGAN,AG.ID_AGAMA,D.BIDANG_KEAHLIAN_DOSEN, '" . $link_blog . "' as LINK_BLOG, '" . $link_email . "' as LINKEMAIL,
		K.TIPE_DATI2||' '||K.NM_KOTA AS KOTA_LAHIR
                FROM PENGGUNA P 
                LEFT JOIN DOSEN D ON P.ID_PENGGUNA = D.ID_PENGGUNA
                LEFT JOIN GOLONGAN G ON D.ID_GOLONGAN = G.ID_GOLONGAN 
                LEFT JOIN JABATAN_PEGAWAI JP ON D.ID_JABATAN_PEGAWAI = JP.ID_JABATAN_PEGAWAI
                LEFT JOIN PROGRAM_STUDI PR ON D.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
                LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS 
				LEFT JOIN KOTA K ON K.ID_KOTA = P.ID_KOTA_LAHIR
                LEFT JOIN 
                  (SELECT R.NM_ROLE,RP.ID_PENGGUNA FROM ROLE R 
                  LEFT JOIN ROLE_PENGGUNA RP ON RP.ID_ROLE = R.ID_ROLE) H ON H.ID_PENGGUNA = P.ID_PENGGUNA
                LEFT JOIN AGAMA AG ON P.ID_AGAMA = AG.ID_AGAMA
        WHERE P.ID_PENGGUNA ='$this->id_pengguna'");
        return $this->db->FetchAssoc();
    }

}

?>