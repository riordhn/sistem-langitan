<?php

class cetak {

    public $db;

    function __construct($database) {
        $this->db = $database;
    }

    function load_data_cetak_kesehatan($id_c_mhs) {
        $this->db->Query("SELECT CM.NM_C_MHS,CM.NO_UJIAN,CM.ALAMAT,CM.TELP,J.NM_JALUR,PR.NM_PROGRAM_STUDI,F.NM_FAKULTAS,JK.ID_JADWAL_KESEHATAN,JK.KELOMPOK_JAM,JK.TGL_TEST,JK.JAM_MULAI,JK.MENIT_MULAI,JK.JAM_SELESAI,JK.MENIT_SELESAI from CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JADWAL_KESEHATAN JK,JALUR J
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS= F.ID_FAKULTAS AND CM.ID_JADWAL_KESEHATAN = JK.ID_JADWAL_KESEHATAN AND CM.ID_JALUR = J.ID_JALUR AND CM.ID_C_MHS='$id_c_mhs'");
        $data = $this->db->FetchArray();
        return $data;
    }

    function load_calon_mahasiswa_by_id_jadwal($id_jadwal) {
        $data_c_mhs_by_id = array();
        $this->db->Query("SELECT * FROM CALON_MAHASISWA CM WHERE CM.ID_JADWAL_KESEHATAN='$id_jadwal'");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_c_mhs_by_id, $temp);
        }
        return $data_c_mhs_by_id;
    }

    function load_jadwal_kesehatan_by_id($id) {
        $data_jadwal_kesehatan = array();
        $this->db->Query("SELECT * FROM JADWAL_KESEHATAN WHERE ID_JADWAL_KESEHATAN='$id' ORDER BY ID_JADWAL_KESEHATAN");
        $data_jadwal_kesehatan = $this->db->FetchArray();
        return $data_jadwal_kesehatan;
    }

    function load_data_hasil_kesehatan_fakultas($fakultas) {
        $data_hasil_tes = array();
        $this->db->Query("SELECT CM.NO_UJIAN, CM.NM_C_MHS, PR.NM_PROGRAM_STUDI,CM.KESEHATAN_KESIMPULAN_THT,CM.KESEHATAN_KESIMPULAN_MATA,CM.KESEHATAN_KESIMPULAN_AKHIR FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F, JADWAL_KESEHATAN JK
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI  AND CM.ID_JADWAL_KESEHATAN = JK.ID_JADWAL_KESEHATAN AND PR.ID_FAKULTAS =F.ID_FAKULTAS AND F.ID_FAKULTAS ='$fakultas'");
        while ($temp = $this->db->FetchArray()) {
            array_push($data_hasil_tes, $temp);
        }
        return $data_hasil_tes;
    }

}

?>
