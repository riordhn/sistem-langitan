<?php

class list_data {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    function load_list_fakultas() {
        return $this->db->QueryToArray("SELECT ID_FAKULTAS,NM_FAKULTAS FROM FAKULTAS");
    }

    function load_list_prodi($id_fakultas, $id_jenjang = null) {
        if ($id_jenjang != '') {
            $q_jenjang = "PS.ID_JENJANG = '{$id_jenjang}'";
        } else {
            $q_jenjang = "";
        }
        if ($id_fakultas != '') {
            $q_fakultas = "PS.ID_FAKULTAS = '{$id_fakultas}'";
        } else {
            $q_fakultas = "";
        }
        if ($id_jenjang != '' && $id_fakultas != '') {
            $q_operator = "AND";
        } else {
            $q_operator = "";
        }

        return $this->db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            FROM PROGRAM_STUDI PS
            JOIN JENJANG J ON PS.ID_JENJANG=J.ID_JENJANG
            WHERE {$q_fakultas} {$q_operator} {$q_jenjang}");
    }

    function load_list_prodi_all() {
        return $this->db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            FROM PROGRAM_STUDI PS
            JOIN JENJANG J ON PS.ID_JENJANG=J.ID_JENJANG");
    }

    function load_list_jalur() {
        return $this->db->QueryToArray("
            SELECT * FROM JALUR ORDER BY NM_JALUR");
    }

    function load_list_jenjang() {
        return $this->db->QueryToArray("
            SELECT * FROM JENJANG ORDER BY NM_JENJANG");
    }

    function load_list_semester() {
        return $this->db->QueryToArray("
            SELECT * FROM SEMESTER WHERE NM_SEMESTER='Ganjil' OR NM_SEMESTER='Genap' ORDER BY TAHUN_AJARAN DESC,NM_SEMESTER DESC");
    }

    function load_list_kelompok_biaya() {
        return $this->db->QueryToArray("
            SELECT * FROM KELOMPOK_BIAYA ORDER BY NM_KELOMPOK_BIAYA");
    }

    function load_bank() {
        return $this->db->QueryToArray("
            SELECT * FROM BANK ORDER BY NM_BANK");
    }

    function load_bank_via() {
        return $this->db->QueryToArray("
            SELECT * FROM BANK_VIA ORDER BY NAMA_BANK_VIA");
    }

    function load_biaya() {
        return $this->db->QueryToArray("SELECT * FROM BIAYA ORDER BY NM_BIAYA");
    }

    function get_fakultas($id_fakultas) {
        $this->db->Query("SELECT * FROM FAKULTAS WHERE ID_FAKULTAS={$id_fakultas}");
        return $this->db->FetchAssoc();
    }

    function get_semester($id_semester) {
        $this->db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER={$id_semester}");
        return $this->db->FetchAssoc();
    }

    function get_prodi($id_prodi) {
        $this->db->Query("SELECT * FROM PROGRAM_STUDI PS JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG WHERE ID_PROGRAM_STUDI={$id_prodi}");
        return $this->db->FetchAssoc();
    }

    function get_jalur($id_jalur) {
        $this->db->Query("SELECT * FROM JALUR WHERE ID_JALUR ='{$id_jalur}'");
        return $this->db->FetchAssoc();
    }

    function get_status($id_status) {
        $this->db->Query("SELECT * FROM STATUS_PEMBAYARAN WHERE ID_STATUS_PEMBAYARAN ='{$id_status}'");
        return $this->db->FetchAssoc();
    }

}

?>
