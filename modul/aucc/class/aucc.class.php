<?php

class AUCC_Modul {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

	
	function get_hasil_cari_mahasiswa($var) {
		$var_trim = trim($var);
        return $this->db->QueryToArray("
            select p.nm_pengguna,m.nim_mhs, cmb.no_ujian no_ujian,f.nm_fakultas,ps.nm_program_studi from pengguna p
            join mahasiswa m on p.id_pengguna = m.id_pengguna
			left join calon_mahasiswa_baru cmb on m.id_c_mhs = cmb.id_c_mhs
            join program_studi ps on ps.id_program_studi = m.id_program_studi
            join fakultas f on ps.id_fakultas = f.id_fakultas
            where m.nim_mhs like '%{$var_trim}%' or upper(p.nm_pengguna) like upper('%{$var}%') or cmb.no_ujian like '%{$var_trim}%'
            ");
    }

    function detail_pembayaran($nim) {
        return $this->db->QueryToArray("
        SELECT * FROM 
            (SELECT M.ID_MHS AS ID1,M.NIM_LAMA,P.NM_PENGGUNA,NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,M.NIM_MHS,PEM.*,B.NM_BIAYA,J.NM_JENJANG,M.STATUS_CEKAL,JAL.NM_JALUR,S.THN_AKADEMIK_SEMESTER
            ,KB.NM_KELOMPOK_BIAYA,M.NIM_BANK,BNK.NM_BANK,BNK.ID_BANK AS ID_BNK,BNKV.ID_BANK_VIA AS ID_BNKV,BNKV.NAMA_BANK_VIA,S2.NM_SEMESTER,S2.TAHUN_AJARAN
            FROM PENGGUNA P
            LEFT JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN PROGRAM_STUDI PR ON M.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            LEFT JOIN JALUR_MAHASISWA JM ON M.ID_MHS = JM.ID_MHS
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR = JM.ID_JALUR
            LEFT JOIN SEMESTER S ON S.ID_SEMESTER = JM.ID_SEMESTER
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS
            LEFT JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
            LEFT JOIN SEMESTER S2 ON S2.ID_SEMESTER = PEM.ID_SEMESTER
            LEFT JOIN BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            LEFT JOIN BANK_VIA BNKV ON BNKV.ID_BANK_VIA = PEM.ID_BANK_VIA
            LEFT JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            LEFT JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
            ) X1
        LEFT JOIN
            (SELECT M.ID_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_SPP_TAGIH,SUM(PEM.DENDA_BIAYA) AS TOTAL_DENDA_BIAYA FROM PEMBAYARAN PEM
            LEFT JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
            WHERE PEM.TGL_BAYAR IS NULL AND PEM.IS_TAGIH ='Y'
            GROUP BY M.ID_MHS
            ) X2 
        ON X1.ID1 = X2.ID2
        LEFT JOIN
            (SELECT M.ID_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_SPP,SUM(PEM.DENDA_BIAYA) AS TOTAL_DENDA FROM PEMBAYARAN PEM
            LEFT JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
            GROUP BY M.ID_MHS
            ) X3 
        ON X1.ID1 = X3.ID2
        WHERE X1.NIM_MHS ='{$nim}'
        ORDER BY X1.ID_PEMBAYARAN");
    }
    
    function detail_pembayaran_cmhs($no_ujian){
		$calon_mahasiswa=$this->db->QueryToArray("SELECT * FROM CALON_MAHASISWA WHERE NO_UJIAN ='{$no_ujian}'")==NULL? "_BARU":"";
        return $this->db->QueryToArray("
        SELECT * FROM 
            (SELECT CM.ID_C_MHS AS ID1,CM.NM_C_MHS,CM.NO_UJIAN,J.NM_JENJANG,PR.NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,PEM.*,B.NM_BIAYA,
            BNK.NM_BANK,BNK.ID_BANK AS ID_BNK,BNKV.ID_BANK_VIA AS ID_BNKV,BNKV.NAMA_BANK_VIA
            FROM CALON_MAHASISWA{$calon_mahasiswa} CM
            LEFT JOIN PROGRAM_STUDI PR ON CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS
            LEFT JOIN PEMBAYARAN_CMHS PEM ON CM.ID_C_MHS = PEM.ID_C_MHS
            LEFT JOIN SEMESTER S2 ON S2.ID_SEMESTER = PEM.ID_SEMESTER
            LEFT JOIN BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            LEFT JOIN BANK_VIA BNKV ON BNKV.ID_BANK_VIA = PEM.ID_BANK_VIA
            LEFT JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            LEFT JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
            ) X1
        LEFT JOIN
            (SELECT CM.ID_C_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_BIAYA_TAGIH
            FROM PEMBAYARAN_CMHS PEM
            LEFT JOIN CALON_MAHASISWA{$calon_mahasiswa} CM ON PEM.ID_C_MHS = CM.ID_C_MHS
            WHERE PEM.TGL_BAYAR IS NULL
            GROUP BY CM.ID_C_MHS
            ) X2 
        ON X1.ID1 = X2.ID2
        LEFT JOIN
            (SELECT CM.ID_C_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_BIAYA
            FROM PEMBAYARAN_CMHS PEM
            LEFT JOIN CALON_MAHASISWA{$calon_mahasiswa} CM ON PEM.ID_C_MHS = CM.ID_C_MHS
            GROUP BY CM.ID_C_MHS
            ) X3
        ON X1.ID1 = X3.ID2
        WHERE X1.NO_UJIAN ='{$no_ujian}'
        ORDER BY X1.ID_PEMBAYARAN_CMHS");
    }

    function get_detail_pembayaran_mahasiswa($nim) {
        return $this->db->QueryToArray("SELECT B.NM_BIAYA,P.* FROM PEMBAYARAN P
        JOIN DETAIL_BIAYA DB ON P.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
        JOIN MAHASISWA M ON P.ID_MHS = M.ID_MHS
        WHERE NIM_MHS ='{$nim}'
        ");
    }

    function get_detail_pembayaran($id_pembayaran) {
        $this->db->Query("SELECT B.NM_BIAYA,P.* FROM PEMBAYARAN P
        JOIN DETAIL_BIAYA DB ON P.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
        JOIN MAHASISWA M ON P.ID_MHS = M.ID_MHS
        WHERE P.ID_PEMBAYARAN ='{$id_pembayaran}'
        ");

        return $this->db->FetchAssoc();
    }
    
    function get_detail_pengguna($username){
        $this->db->Query("
            SELECT * FROM PENGGUNA P
            LEFT JOIN ROLE R ON R.ID_ROLE = P.ID_ROLE
            WHERE P.USERNAME='$username'
        ");
        
        return $this->db->FetchAssoc();
    }
    
    function reset_password_pengguna($username, $pass){
        $this->db->Query("UPDATE PENGGUNA SET SE1 = '{$pass}' WHERE USERNAME='{$username}'");
    }

    function cek_data($nim) {
        $this->db->Query("
        SELECT M.NIM_MHS,P.NM_PENGGUNA,J.ID_JENJANG,J.NM_JENJANG,S.ID_SEMESTER,S.THN_AKADEMIK_SEMESTER,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.ID_JALUR,J.NM_JALUR,
        KB.ID_KELOMPOK_BIAYA,KB.NM_KELOMPOK_BIAYA 
            FROM PENGGUNA P
            LEFT JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA 
            LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
            LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
            LEFT JOIN SEMESTER S ON JM.ID_SEMESTER = S.ID_SEMESTER 
            LEFT JOIN JALUR J ON JM.ID_JALUR = J.ID_JALUR
        WHERE M.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_pengguna($nim) {
        $this->db->Query("SELECT * FROM PENGGUNA WHERE USERNAME='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_mahasiswa($nim) {
        $this->db->Query("SELECT * FROM MAHASISWA WHERE NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_jalur($nim) {
        $this->db->Query("SELECT * FROM JALUR_MAHASISWA WHERE NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function insert_pengguna_mahasiswa($nama, $nim) {
        $this->db->Query("INSERT INTO PENGGUNA 
                (USERNAME,PASSWORD_PENGGUNA,NM_PENGGUNA,ID_ROLE,JOIN_TABLE) 
                VALUES 
                ('{$nim}','{$nim}','{$nama}',3,3)");
    }

    function insert_mahasiswa($id_pengguna, $nim, $program_studi, $kelompok_biaya) {
        $nim_bank = substr($nim, 0, 9);
        $this->db->Query("INSERT INTO MAHASISWA 
                (ID_PENGGUNA,NIM_MHS,ID_PROGRAM_STUDI,ID_KELOMPOK_BIAYA,NIM_BANK,STATUS_CEKAL) 
                VALUES 
                ('{$id_pengguna}','{$nim}','{$program_studi}','{$kelompok_biaya}','{$nim_bank}','0')");
    }

    function insert_jalur_mahasiswa($id_mhs, $program_studi, $jalur, $semester, $nim_mhs) {
        $this->db->Query("INSERT INTO JALUR_MAHASISWA 
                (ID_MHS,ID_PROGRAM_STUDI,ID_JALUR,ID_SEMESTER,NIM_MHS,ID_JALUR_AKTIF)
                VALUES
                ('{$id_mhs}','{$program_studi}','{$jalur}','{$semester}','{$nim_mhs}',1)");
    }

    function update_pengguna($nama, $nim) {
	$nama = str_replace("'","''",$nama);
        $this->db->Query("UPDATE PENGGUNA SET NM_PENGGUNA ='{$nama}' WHERE USERNAME='{$nim}'");
    }

    function update_mahasiswa($id_kelompok_biaya, $nim, $id_program_studi) {
        $nim_bank = substr($nim, 0, 9);
		$this->db->Query("UPDATE MAHASISWA SET ID_KELOMPOK_BIAYA='{$id_kelompok_biaya}',NIM_BANK='{$nim_bank}',ID_PROGRAM_STUDI='{$id_program_studi}',STATUS_CEKAL=0 WHERE NIM_MHS='{$nim}'");
    }

    function update_jalur_mahasiswa($id_mhs, $program_studi, $jalur, $semester, $nim) {
		$this->db->Query("UPDATE JALUR_MAHASISWA SET ID_MHS = '{$id_mhs}', ID_PROGRAM_STUDI='{$program_studi}',ID_JALUR='{$jalur}',ID_SEMESTER='{$semester}',ID_JALUR_AKTIF=1 WHERE NIM_MHS='{$nim}'");
    }

    function get_jenjang() {
        return $this->db->QueryToArray("SELECT * FROM JENJANG ORDER BY NM_JENJANG,ID_JENJANG");
    }

    function get_semester() {
        return $this->db->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER = 'Ganjil' ORDER BY ID_SEMESTER");
    }

    function get_program_studi() {
        return $this->db->QueryToArray("SELECT * FROM PROGRAM_STUDI ORDER BY NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }

    function get_program_studi_by_jenjang($id_jenjang) {
        return $this->db->QueryToArray("SELECT * FROM PROGRAM_STUDI WHERE ID_JENJANG={$id_jenjang} ORDER BY NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }

    function get_jalur() {
        return $this->db->QueryToArray("SELECT * FROM JALUR ORDER BY NM_JALUR,ID_JALUR");
    }

    function get_kelompok() {
        return $this->db->QueryToArray("SELECT * FROM KELOMPOK_BIAYA ORDER BY NM_KELOMPOK_BIAYA,ID_KELOMPOK_BIAYA");
    }

    function get_bank() {
        return $this->db->QueryToArray("SELECT * FROM BANK ORDER BY NM_BANK,ID_BANK");
    }

    function get_via_bank() {
        return $this->db->QueryToArray("SELECT * FROM BANK_VIA ORDER BY NAMA_BANK_VIA,ID_BANK_VIA");
    }
    
    function get_biaya_kuliah_mahasiswa(){
        return $this->db->QueryToArray("
            SELECT S.THN_AKADEMIK_SEMESTER,S.NM_SEMESTER,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,BK.BESAR_BIAYA_KULIAH FROM BIAYA_KULIAH BK
            JOIN SEMESTER S ON BK.ID_SEMESTER = S.ID_SEMESTER
            JOIN PROGRAM_STUDI PS ON BK.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = BK.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR = BK.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON BK.ID_KELOMPOK_BIAYA = KB.ID_KELOMPOK_BIAYA
            ORDER BY PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,S.THN_AKADEMIK_SEMESTER
            ");
    }

    function get_semester_all() {
        return $this->db->QueryToArray("SELECT * FROM SEMESTER ORDER BY THN_AKADEMIK_SEMESTER DESC");
    }

    function cek_biaya_kuliah_mhs($nim) {
        $this->db->Query("
          SELECT T.NIM_MHS,T.ID_MHS, BK.ID_BIAYA_KULIAH,BK.BESAR_BIAYA_KULIAH FROM BIAYA_KULIAH BK
          RIGHT JOIN (
              SELECT M.NIM_MHS, M.ID_MHS, JM.ID_SEMESTER, M.ID_PROGRAM_STUDI, M.ID_KELOMPOK_BIAYA, JM.ID_JALUR FROM MAHASISWA M
              JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
              WHERE JM.ID_SEMESTER IS NOT NULL) T ON T.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
          WHERE
          BK.ID_KELOMPOK_BIAYA = T.ID_KELOMPOK_BIAYA AND BK.ID_SEMESTER = T.ID_SEMESTER AND BK.ID_JALUR = T.ID_JALUR
          AND T.ID_MHS NOT IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND T.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_pembayaran($nim) {
        $this->db->Query("
        SELECT M.NIM_MHS,BKM.ID_MHS, 5 AS ID_SEMESTER, DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, 'Y' AS IS_TAGIH FROM BIAYA_KULIAH_MHS BKM
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
            LEFT JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
            WHERE
	M.ID_MHS IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND
	M.ID_MHS NOT IN (SELECT DISTINCT ID_MHS FROM PEMBAYARAN) AND M.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function generate_pembayaran($nim) {
        $this->db->Query("
        INSERT INTO PEMBAYARAN (ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, BESAR_BIAYA, IS_TAGIH)
            SELECT BKM.ID_MHS, 5 AS ID_SEMESTER, DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, 'Y' AS IS_TAGIH FROM BIAYA_KULIAH_MHS BKM
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
            LEFT JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
            WHERE
        M.ID_MHS IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND
        M.ID_MHS NOT IN (SELECT DISTINCT ID_MHS FROM PEMBAYARAN) AND
        M.NIM_MHS ='{$nim}'");
    }

    function generate_biaya_kuliah($nim) {
        $this->db->Query("
        INSERT INTO BIAYA_KULIAH_MHS (ID_MHS, ID_BIAYA_KULIAH)
          SELECT T.ID_MHS, BK.ID_BIAYA_KULIAH FROM BIAYA_KULIAH BK
          RIGHT JOIN (
            SELECT M.NIM_MHS, M.ID_MHS, JM.ID_SEMESTER, M.ID_PROGRAM_STUDI, M.ID_KELOMPOK_BIAYA, JM.ID_JALUR FROM MAHASISWA M
            JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
            WHERE JM.ID_SEMESTER IS NOT NULL) T ON T.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
          WHERE
            BK.ID_KELOMPOK_BIAYA = T.ID_KELOMPOK_BIAYA AND BK.ID_SEMESTER = T.ID_SEMESTER AND BK.ID_JALUR = T.ID_JALUR
            AND T.ID_MHS NOT IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND
            T.NIM_MHS ='{$nim}'");
    }
	
	function get_prodi_by_fakultas($id_fakultas){
		return $this->db->QueryToArray("
			select ps.id_program_studi as ID_PROGRAM_STUDI, (j.nm_jenjang||' '||ps.nm_program_studi) as NAMA_PRODI from program_studi ps
			left join jenjang j on j.id_jenjang = ps.id_jenjang
			where ps.id_fakultas = '$id_fakultas'
		");
	}

}

?>
