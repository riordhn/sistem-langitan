<?php

class ppmb {

    public $database;

    function __construct($database) {
        $this->database = $database;
    }

    function load_jadwal_ppmb() {
        $data = array();
        $this->database->Query("SELECT * FROM JADWAL_PPMB ORDER BY GELOMBANG,KODE_JALUR,NOMER_AWAL,ID_JADWAL_PPMB");
        while ($temp = $this->database->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function load_jadwal_ppmb_by_id($id_jadwal) {
        $this->database->Query("SELECT * FROM JADWAL_PPMB WHERE ID_JADWAL_PPMB ='$id_jadwal'");
        $temp = $this->database->FetchArray();
        return $temp;
    }

    function load_ruang_ppmb() {
        $data = array();
        $this->database->Query("SELECT * FROM RUANG_PPMB ORDER BY ID_RUANG_PPMB");
        while ($temp = $this->database->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function load_ruang_ppmb_by_id($id_ruang) {
        $this->database->Query("SELECT * FROM RUANG_PPMB WHERE ID_RUANG_PPMB ='$id_ruang'");
        $temp = $this->database->FetchArray();
        return $temp;
    }

    function add_jadwal_ppmb($lokasi, $alamat, $ruang, $tgl, $kapasitas, $jalur, $gelombang, $nomer_awal) {
        if ($gelombang == 5) {
            $this->database->Query("INSERT INTO JADWAL_PPMB (LOKASI,ALAMAT,NM_RUANG,TGL_TEST,KAPASITAS,KODE_JALUR,NOMER_AWAL,GELOMBANG) VALUES ('$lokasi','$alamat','$ruang','$tgl','$kapasitas','0','$nomer_awal','$gelombang')");
        } else {
            $this->database->Query("INSERT INTO JADWAL_PPMB (LOKASI,ALAMAT,NM_RUANG,TGL_TEST,KAPASITAS,KODE_JALUR,GELOMBANG) VALUES ('$lokasi','$alamat','$ruang','$tgl','$kapasitas','$jalur','$gelombang')");
        }
    }

    function add_ruang_ppmb($nm_ruang, $alamat, $kapasitas) {
        $this->database->Query("INSERT INTO RUANG_PPMB (NM_RUANG_PPMB,ALAMAT,KAPASITAS) VALUES ('$nm_ruang','$alamat','$kapasitas')");
    }

    function update_jadwal_ppmb($id_jadwal, $lokasi, $alamat, $ruang, $tgl, $kapasitas, $jalur, $gelombang, $nomer_awal) {
        if ($gelombang == 5) {
            $this->database->Query("UPDATE JADWAL_PPMB SET LOKASI='$lokasi',ALAMAT='$alamat',NM_RUANG='$ruang',TGL_TEST='$tgl',KAPASITAS='$kapasitas',NOMER_AWAL='$nomer_awal',GELOMBANG='$gelombang' WHERE ID_JADWAL_PPMB='$id_jadwal'");
        } else {
            $this->database->Query("UPDATE JADWAL_PPMB SET LOKASI='$lokasi',ALAMAT='$alamat',NM_RUANG='$ruang',TGL_TEST='$tgl',KAPASITAS='$kapasitas',KODE_JALUR='$jalur',GELOMBANG='$gelombang' WHERE ID_JADWAL_PPMB='$id_jadwal'");
        }
    }

    function update_jadwal_ppmb_nomer($id_jadwal, $no_awal, $no_akhir) {
        $this->database->Query("UPDATE JADWAL_PPMB SET NOMER_AWAL ='$no_awal',NOMER_AKHIR ='$no_akhir' WHERE ID_JADWAL_PPMB ='$id_jadwal'");
    }

    function delete_jadwal_ppmb($id_jadwal) {
        $this->database->Query("DELETE JADWAL_PPMB WHERE ID_JADWAL_PPMB ='$id_jadwal'");
    }

    function cek_no_peserta($kode_jalur, $gelombang) {
        $this->database->Query("SELECT * FROM (SELECT ID_JADWAL_PPMB,NOMER_AKHIR FROM JADWAL_PPMB WHERE KODE_JALUR ='$kode_jalur' AND GELOMBANG='$gelombang' AND NOMER_AKHIR IS NOT NULL ORDER BY ID_JADWAL_PPMB DESC) T WHERE ROWNUM <= 1");
        $temp = $this->database->FetchArray();
        return $temp;
    }

    function cek_no_peserta_pasca($gelombang) {
        $this->database->Query("SELECT * FROM (SELECT ID_JADWAL_PPMB,NOMER_AKHIR,NOMER_AWAL FROM JADWAL_PPMB WHERE GELOMBANG='$gelombang' ORDER BY ID_JADWAL_PPMB DESC) T WHERE ROWNUM <= 1");
        $temp = $this->database->FetchArray();
        return $temp;
    }

    function aktifkan($id_jadwal) {
        $data = $this->load_jadwal_ppmb_by_id($id_jadwal);
        $no_akhir = intval($this->get_no_awal($data['GELOMBANG'], date('Y'), $data['KODE_JALUR']));
        $this->update_jadwal_ppmb_nomer($id_jadwal, $no_akhir, $no_akhir + ($data['KAPASITAS'] - 1));
        $this->plot_jadwal_ppmb($id_jadwal);
        $this->database->Query("UPDATE JADWAL_PPMB SET STATUS_AKTIF ='1' WHERE ID_JADWAL_PPMB='$id_jadwal'");
    }

    function get_no_awal($jalur, $tgl, $kode_jurusan) {
        $no_ujian;
        $no_terakhir = $this->cek_no_peserta($kode_jurusan, $jalur);
        $no_terakhir_pasca = $this->cek_no_peserta_pasca($jalur);
        switch ($jalur) {
            case '1':
                break;
            case '2': {
                    switch ($kode_jurusan) {
                        case'1':
                            if (empty($no_terakhir['NOMER_AKHIR'])) {
                                $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . '3001';
                            } else {
                                $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . (intval(substr($no_terakhir['NOMER_AKHIR'], -4)) + 1);
                            }
                            break;
                        case'2':
                            if (empty($no_terakhir['NOMER_AKHIR'])) {
                                $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . '3001';
                            } else {
                                $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . (intval(substr($no_terakhir['NOMER_AKHIR'], -4)) + 1);
                            }
                            break;
                        case'3':
                            if (empty($no_terakhir)) {
                                $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . '1001';
                            } else {
                                $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . (intval(substr($no_terakhir['NOMER_AKHIR'], -4)) + 1);
                            }
                            break;
                    }
                }
                break;
            case '4': {
                    switch ($kode_jurusan) {
                        case'1':
                            if (empty($no_terakhir['NOMER_AKHIR'])) {
                                $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . '0001';
                            } else {
                                $no_ujian = $jalur . substr($tgl, -2) . (intval(substr($no_terakhir['NOMER_AKHIR'], -5)) + 1);
                            }
                            break;
                        case'2':
                            if (empty($no_terakhir['NOMER_AKHIR'])) {
                                $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . '0001';
                            } else {
                                $no_ujian = $jalur . substr($tgl, -2) . (intval(substr($no_terakhir['NOMER_AKHIR'], -5)) + 1);
                            }
                            break;
                        case'3':
                            if (empty($no_terakhir)) {
                                $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . '0001';
                            } else {
                                $no_ujian = $jalur . substr($tgl, -2) . (intval(substr($no_terakhir['NOMER_AKHIR'], -5)) + 1);
                            }
                            break;
                    }
                }
                break;
            case '3': {
                    switch ($kode_jurusan) {
                        case'1':
                            if (empty($no_terakhir['NOMER_AKHIR'])) {
                                $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . '0001';
                            } else {
                                $no_ujian = $jalur . substr($tgl, -2) . (intval(substr($no_terakhir['NOMER_AKHIR'], -5)) + 1);
                            }
                            break;
                        case'2':
                            if (empty($no_terakhir['NOMER_AKHIR'])) {
                                $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . '0001';
                            } else {
                                $no_ujian = $jalur . substr($tgl, -2) . (intval(substr($no_terakhir['NOMER_AKHIR'], -5)) + 1);
                            }
                            break;
                    }
                }
                break;
            case '7':
                switch ($kode_jurusan) {
                    case'1':
                        if (empty($no_terakhir['NOMER_AKHIR'])) {
                            $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . '0101';
                        } else {
                            $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . '0' . (intval(substr($no_terakhir['NOMER_AKHIR'], -4)) + 1);
                        }
                        break;
                    case'2':
                        if (empty($no_terakhir['NOMER_AKHIR'])) {
                            $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . '0101';
                        } else {
                            $no_ujian = $jalur . substr($tgl, -2) . $kode_jurusan . '0' . (intval(substr($no_terakhir['NOMER_AKHIR'], -4)) + 1);
                        }
                        break;
                }
                break;
            case '5':
                $no_ujian = $no_terakhir_pasca['NOMER_AWAL'];
                break;
        }
        return $no_ujian;
    }
    
    function delete_plot_ppmb($id_jadwal){
        $this->database->Query("DELETE FROM PLOT JADWAL PPMB WHERE ID_JADWAL_PPMB ='$id_jadwal'");
    }

    function plot_jadwal_ppmb($id_jadwal) {
        $temp = $this->load_jadwal_ppmb_by_id($id_jadwal);
        for ($i = intval($temp['NOMER_AWAL']); $i <= intval($temp['NOMER_AKHIR']); $i++) {
            $this->database->Query("INSERT INTO PLOT_JADWAL_PPMB (NO_UJIAN,KODE_JALUR,ID_JADWAL_PPMB,GELOMBANG) VALUES ('$i','{$temp['KODE_JALUR']}','{$temp['ID_JADWAL_PPMB']}','{$temp['GELOMBANG']}')");
        }
    }

}

?>
