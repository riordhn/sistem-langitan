<?php

class du_jaket {

    public $database;

    function __construct($database) {
        $this->database = $database;
    }

    function load_calon_mahasiswa($no_ujian) {
        $this->database->Query("SELECT ID_C_MHS,STATUS,NO_UJIAN FROM CALON_MAHASISWA WHERE NO_UJIAN='$no_ujian'");
        return $this->database->FetchArray();
    }

    function load_calon_mahasiswa_by_id($id_c_mhs) {
        $this->database->Query("SELECT CM.ALAMAT,CM.NM_C_MHS,CM.ID_C_MHS,CM.NO_UJIAN,CM.MUTZ,CM.JAKET,PR.NM_PROGRAM_STUDI,F.NM_FAKULTAS,JEN.ID_JENJANG,JEN.NM_JENJANG,JAL.NM_JALUR FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG JEN,JALUR JAL 
        WHERE CM.ID_PROGRAM_STUDI=PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = JEN.ID_JENJANG AND CM.ID_JALUR = JAL.ID_JALUR AND ID_C_MHS= '$id_c_mhs'");
        return $this->database->FetchArray();
    }

    function update_status($no_ujian) {
        $this->database->Query("UPDATE CALON_MAHASISWA SET STATUS='10' WHERE NO_UJIAN='$no_ujian'");
    }

    function update_ukur_jaket($id_c_mhs, $jaket, $mutz) {
        $this->database->Query("UPDATE CALON_MAHASISWA SET STATUS='9',JAKET='$jaket',MUTZ='$mutz',TGL_PSN_JAKET_MUTZ='" . date('d-M-y') . "' WHERE ID_C_MHS='$id_c_mhs'");
    }

    function load_tanggal_jaket_mutz() {
        $tanggal = array();
        $this->database->Query("SELECT DISTINCT TGL_PSN_JAKET_MUTZ AS TANGGAL FROM CALON_MAHASISWA WHERE TGL_PSN_JAKET_MUTZ IS NOT NULL ORDER BY TGL_PSN_JAKET_MUTZ");
        while ($temp = $this->database->FetchArray()) {
            array_push($tanggal, $temp);
        }
        return $tanggal;
    }

    function get_jumlah_jaket_mutz($jenis, $tampilan, $kelompok, $range, $tanggal=null, $tanggal_mulai=null, $tanggal_selesai=null) {
        $data = array();
        if ($tampilan == 'all' && $kelompok == 'all' && $range == 'all') {
            $query = "SELECT CM." . $jenis . " AS UKURAN,COUNT(CM.ID_C_MHS) AS JUMLAH 
            FROM CALON_MAHASISWA CM
            WHERE CM." . $jenis . " IS NOT NULL
            GROUP BY CM." . $jenis . "
            ORDER BY CM." . $jenis . "";
        } else if ($tampilan != 'all' && $kelompok == 'all' && $range == 'all') {
            $query = "SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,CM." . $jenis . " AS UKURAN,COUNT(CM.ID_C_MHS) AS JUMLAH 
            FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM." . $jenis . " IS NOT NULL
            GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS,CM." . $jenis . "
            ORDER BY F.ID_FAKULTAS,CM." . $jenis . "";
        } else if ($tampilan != 'all' && $kelompok != 'all' && $range == 'all') {
            if ($kelompok == '1') {
                $query = "SELECT J.ID_JENJANG,J.NM_JENJANG,F.ID_FAKULTAS,F.NM_FAKULTAS,CM." . $jenis . " AS UKURAN,COUNT(CM.ID_C_MHS) AS JUMLAH 
                FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
                WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND (J.ID_JENJANG ='1' OR J.ID_JENJANG ='5' OR J.ID_JENJANG ='9')
                GROUP BY J.ID_JENJANG,J.NM_JENJANG,F.ID_FAKULTAS,F.NM_FAKULTAS,CM." . $jenis . "
                ORDER BY J.ID_JENJANG,F.ID_FAKULTAS,CM." . $jenis . "";
            } else {
                $query = "SELECT J.ID_JENJANG,J.NM_JENJANG,F.ID_FAKULTAS,F.NM_FAKULTAS,CM." . $jenis . " AS UKURAN,COUNT(CM.ID_C_MHS) AS JUMLAH 
                FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
                WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND (J.ID_JENJANG ='2' OR J.ID_JENJANG ='3' OR J.ID_JENJANG ='8' OR J.ID_JENJANG ='10' )
                GROUP BY J.ID_JENJANG,J.NM_JENJANG,F.ID_FAKULTAS,F.NM_FAKULTAS,CM." . $jenis . "
                ORDER BY J.ID_JENJANG,F.ID_FAKULTAS,CM." . $jenis . "";
            }
        } else if ($tampilan == 'all' && $kelompok != 'all' && $range != 'all') {
            if ($kelompok == '1') {
                if ($range == 'harian') {
                    $filter_range = "TGL_PSN_JAKET_MUTZ ='$tanggal'";
                } else if ($range == 'banyak_hari') {
                    $filter_range = "TGL_PSN_JAKET_MUTZ BETWEEN '$tanggal_mulai' AND '$tanggal_selesai'";
                } else {
                    $filter_range = 'TGL_PSN_JAKET_MUTZ IS NOT NULL';
                }
                $query = "SELECT J.ID_JENJANG,J.NM_JENJANG,CM." . $jenis . " AS UKURAN,COUNT(CM.ID_C_MHS) AS JUMLAH 
                FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,JENJANG J
                WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND (J.ID_JENJANG ='1' OR J.ID_JENJANG ='5' OR J.ID_JENJANG ='9') AND " . $filter_range . "
                GROUP BY J.ID_JENJANG,J.NM_JENJANG,CM." . $jenis . "
                ORDER BY J.ID_JENJANG,CM." . $jenis . "";
            } else {
                if ($range == 'harian') {
                    $filter_range = "TGL_PSN_JAKET_MUTZ ='$tanggal'";
                } else if ($range == 'banyak_hari') {
                    $filter_range = "TGL_PSN_JAKET_MUTZ BETWEEN '$tanggal_mulai' AND '$tanggal_selesai'";
                } else {
                    $filter_range = 'TGL_PSN_JAKET_MUTZ IS NOT NULL';
                }
                $query = "SELECT J.ID_JENJANG,J.NM_JENJANG,CM." . $jenis . " AS UKURAN,COUNT(CM.ID_C_MHS) AS JUMLAH 
                FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,JENJANG J
                WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND (J.ID_JENJANG ='2' OR J.ID_JENJANG ='3' OR J.ID_JENJANG ='8' OR J.ID_JENJANG ='10' ) AND " . $filter_range . "
                GROUP BY J.ID_JENJANG,J.NM_JENJANG,CM." . $jenis . "
                ORDER BY J.ID_JENJANG,CM." . $jenis . "";
            }
        } else if ($tampilan == 'all' && $kelompok == 'all' && $range != 'all') {
            if ($range == 'harian') {
                $filter_range = "TGL_PSN_JAKET_MUTZ ='$tanggal'";
            } else if ($range == 'banyak_hari') {
                $filter_range = "TGL_PSN_JAKET_MUTZ BETWEEN '$tanggal_mulai' AND '$tanggal_selesai'";
            } else {
                $filter_range = 'TGL_PSN_JAKET_MUTZ IS NOT NULL';
            }
            $query = "SELECT CM." . $jenis . " AS UKURAN,COUNT(CM.ID_C_MHS) AS JUMLAH 
            FROM CALON_MAHASISWA CM
            WHERE CM." . $jenis . " IS NOT NULL AND " . $filter_range . "
            GROUP BY CM." . $jenis . "
            ORDER BY CM." . $jenis . "";
        } else if ($tampilan != 'all' && $kelompok == 'all' && $range != 'all') {
            if ($range == 'harian') {
                $filter_range = "TGL_PSN_JAKET_MUTZ ='$tanggal'";
            } else if ($range == 'banyak_hari') {
                $filter_range = "TGL_PSN_JAKET_MUTZ BETWEEN '$tanggal_mulai' AND '$tanggal_selesai'";
            } else {
                $filter_range = 'TGL_PSN_JAKET_MUTZ IS NOT NULL';
            }
            $query = "SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,CM." . $jenis . " AS UKURAN,COUNT(CM.ID_C_MHS) AS JUMLAH
            FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND CM." . $jenis . " IS NOT NULL AND " . $filter_range . "
            GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS,CM." . $jenis . "
            ORDER BY F.ID_FAKULTAS";
        } else if ($tampilan != 'all' && $kelompok != 'all' && $range != 'all') {
            if ($kelompok == '1') {
                if ($range == 'harian') {
                    $filter_range = "TGL_PSN_JAKET_MUTZ ='$tanggal'";
                } else if ($range == 'banyak_hari') {
                    $filter_range = "TGL_PSN_JAKET_MUTZ BETWEEN '$tanggal_mulai' AND '$tanggal_selesai'";
                } else {
                    $filter_range = 'TGL_PSN_JAKET_MUTZ IS NOT NULL';
                }
                $query = "SELECT J.ID_JENJANG,J.NM_JENJANG,F.ID_FAKULTAS,F.NM_FAKULTAS,CM." . $jenis . " AS UKURAN,COUNT(CM.ID_C_MHS) AS JUMLAH 
                FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
                WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND (J.ID_JENJANG ='1' OR J.ID_JENJANG ='5' OR J.ID_JENJANG ='9') AND " . $filter_range . "
                GROUP BY J.ID_JENJANG,J.NM_JENJANG,F.ID_FAKULTAS,F.NM_FAKULTAS,CM." . $jenis . "
                ORDER BY J.ID_JENJANG,F.ID_FAKULTAS,CM." . $jenis . "";
            } else {
                if ($range == 'harian') {
                    $filter_range = "TGL_PSN_JAKET_MUTZ ='$tanggal'";
                } else if ($range == 'banyak_hari') {
                    $filter_range = "TGL_PSN_JAKET_MUTZ BETWEEN '$tanggal_mulai' AND '$tanggal_selesai'";
                } else {
                    $filter_range = 'TGL_PSN_JAKET_MUTZ IS NOT NULL';
                }
                $query = "SELECT J.ID_JENJANG,J.NM_JENJANG,F.ID_FAKULTAS,F.NM_FAKULTAS,CM." . $jenis . " AS UKURAN,COUNT(CM.ID_C_MHS) AS JUMLAH 
                FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
                WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND (J.ID_JENJANG ='2' OR J.ID_JENJANG ='3' OR J.ID_JENJANG ='8' OR J.ID_JENJANG ='10' ) AND " . $filter_range . "
                GROUP BY J.ID_JENJANG,J.NM_JENJANG,F.ID_FAKULTAS,F.NM_FAKULTAS,CM." . $jenis . "
                ORDER BY J.ID_JENJANG,F.ID_FAKULTAS,CM." . $jenis . "";
            }
        } else if ($tampilan == 'all' && $kelompok != 'all' && $range == 'all') {
            if ($kelompok == '1') {
                $query = "SELECT J.ID_JENJANG,J.NM_JENJANG,CM." . $jenis . " AS UKURAN,COUNT(CM.ID_C_MHS) AS JUMLAH 
                FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
                WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND (J.ID_JENJANG ='1' OR J.ID_JENJANG ='5' OR J.ID_JENJANG ='9')
                GROUP BY J.ID_JENJANG,J.NM_JENJANG,CM." . $jenis . "
                ORDER BY J.ID_JENJANG,CM." . $jenis . "";
            } else {
                $query = "SELECT J.ID_JENJANG,J.NM_JENJANG,CM." . $jenis . " AS UKURAN,COUNT(CM.ID_C_MHS) AS JUMLAH 
                FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
                WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND (J.ID_JENJANG ='2' OR J.ID_JENJANG ='3' OR J.ID_JENJANG ='8' OR J.ID_JENJANG ='10' )
                GROUP BY J.ID_JENJANG,J.NM_JENJANG,CM." . $jenis . "
                ORDER BY J.ID_JENJANG,CM." . $jenis . "";
            }
        }
        $this->database->Query($query);
        while ($temp = $this->database->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function load_calon_mahasiswa_terima_jaket_mutz($jenis, $tampilan, $kelompok, $range, $ukuran, $tanggal=null, $tanggal_mulai=null, $tanggal_selesai=null, $fakultas=null, $jenjang=null) {
        $data = array();
        if ($tampilan == 'all' && $kelompok == 'all' && $range == 'all') {
            $query = "SELECT CM.NM_C_MHS,CM.NO_UJIAN,PR.NM_PROGRAM_STUDI,F.NM_FAKULTAS ,CM." . $jenis . " AS UKURAN
            FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND CM." . $jenis . " ='$ukuran'
            ";
        } else if ($tampilan != 'all' && $kelompok == 'all' && $range == 'all') {
            $query = "SELECT CM.NM_C_MHS,CM.NO_UJIAN,PR.NM_PROGRAM_STUDI,F.NM_FAKULTAS ,CM." . $jenis . " AS UKURAN
            FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND CM." . $jenis . " ='$ukuran' AND F.ID_FAKULTAS='$fakultas'
            ";
        } else if ($tampilan != 'all' && $kelompok != 'all' && $range == 'all') {
            $query = "SELECT CM.NM_C_MHS,CM.NO_UJIAN,PR.NM_PROGRAM_STUDI,F.NM_FAKULTAS ,CM." . $jenis . " AS UKURAN
            FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND CM." . $jenis . " ='$ukuran' AND F.ID_FAKULTAS='$fakultas' AND J.ID_JENJANG='$jenjang'";
        } else if ($tampilan == 'all' && $kelompok != 'all' && $range != 'all') {
            if ($range == 'harian') {
                $filter_range = "TGL_PSN_JAKET_MUTZ ='$tanggal'";
            } else if ($range == 'banyak_hari') {
                $filter_range = "TGL_PSN_JAKET_MUTZ BETWEEN '$tanggal_mulai' AND '$tanggal_selesai'";
            } else {
                $filter_range = 'TGL_PSN_JAKET_MUTZ IS NOT NULL';
            }
            $query = "SELECT CM.NM_C_MHS,CM.NO_UJIAN,PR.NM_PROGRAM_STUDI,F.NM_FAKULTAS ,CM." . $jenis . " AS UKURAN
            FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND CM." . $jenis . " ='$ukuran' AND J.ID_JENJANG='$jenjang' AND " . $filter_range . "";
        } else if ($tampilan == 'all' && $kelompok == 'all' && $range != 'all') {
            if ($range == 'harian') {
                $filter_range = "TGL_PSN_JAKET_MUTZ ='$tanggal'";
            } else if ($range == 'banyak_hari') {
                $filter_range = "TGL_PSN_JAKET_MUTZ BETWEEN '$tanggal_mulai' AND '$tanggal_selesai'";
            } else {
                $filter_range = 'TGL_PSN_JAKET_MUTZ IS NOT NULL';
            }
            $query = "SELECT CM.NM_C_MHS,CM.NO_UJIAN,PR.NM_PROGRAM_STUDI,F.NM_FAKULTAS ,CM." . $jenis . " AS UKURAN
            FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND CM." . $jenis . " ='$ukuran' AND " . $filter_range . "";
        } else if ($tampilan != 'all' && $kelompok == 'all' && $range != 'all') {
            if ($range == 'harian') {
                $filter_range = "TGL_PSN_JAKET_MUTZ ='$tanggal'";
            } else if ($range == 'banyak_hari') {
                $filter_range = "TGL_PSN_JAKET_MUTZ BETWEEN '$tanggal_mulai' AND '$tanggal_selesai'";
            } else {
                $filter_range = 'TGL_PSN_JAKET_MUTZ IS NOT NULL';
            }
            $query = "SELECT CM.NM_C_MHS,CM.NO_UJIAN,PR.NM_PROGRAM_STUDI,F.NM_FAKULTAS ,CM." . $jenis . " AS UKURAN
            FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND CM." . $jenis . " ='$ukuran' AND F.ID_FAKULTAS='$fakultas' AND " . $filter_range . "";
        } else if ($tampilan != 'all' && $kelompok != 'all' && $range != 'all') {
            if ($range == 'harian') {
                $filter_range = "TGL_PSN_JAKET_MUTZ ='$tanggal'";
            } else if ($range == 'banyak_hari') {
                $filter_range = "TGL_PSN_JAKET_MUTZ BETWEEN '$tanggal_mulai' AND '$tanggal_selesai'";
            } else {
                $filter_range = 'TGL_PSN_JAKET_MUTZ IS NOT NULL';
            }
            $query = "SELECT CM.NM_C_MHS,CM.NO_UJIAN,PR.NM_PROGRAM_STUDI,F.NM_FAKULTAS ,CM." . $jenis . " AS UKURAN
            FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND CM." . $jenis . " ='$ukuran' AND F.ID_FAKULTAS='$fakultas' AND J.ID_JENJANG='$jenjang' AND " . $filter_range . "";
        } else if ($tampilan == 'all' && $kelompok != 'all' && $range == 'all') {
            $query = "SELECT CM.NM_C_MHS,CM.NO_UJIAN,PR.NM_PROGRAM_STUDI,F.NM_FAKULTAS ,CM." . $jenis . " AS UKURAN
            FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J
            WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG AND CM." . $jenis . " IS NOT NULL AND CM." . $jenis . " ='$ukuran' AND J.ID_JENJANG='$jenjang'";
        }
        $this->database->Query($query);
        while ($temp = $this->database->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function load_jenjang() {
        $data = array();
        $this->database->Query("SELECT J.ID_JENJANG,J.NM_JENJANG FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,JENJANG J WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_JENJANG = J.ID_JENJANG 
        GROUP BY J.ID_JENJANG,J.NM_JENJANG
        ORDER BY J.ID_JENJANG");
        while ($temp = $this->database->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function load_fakultas() {
        $data = array();
        $this->database->Query("SELECT F.ID_FAKULTAS,F.NM_FAKULTAS
        FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,FAKULTAS F,JENJANG J 
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_FAKULTAS = F.ID_FAKULTAS AND PR.ID_JENJANG = J.ID_JENJANG
        GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
        ORDER BY F.ID_FAKULTAS");
        while ($temp = $this->database->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

}

?>
