<?php

class du_jadwal {

    public $database;

    function __construct($database) {
        $this->database = $database;
    }

    function load_calon_mahasiswa_jadwal($id_c_mhs) {
        $this->database->Query("SELECT CM.ID_JADWAL_TOEFL,CM.ID_C_MHS,CM.NO_UJIAN,CM.NM_C_MHS,PR.NM_PROGRAM_STUDI,CM.STATUS,JT.ID_RUANG_TOEFL,JT.TGL_TEST as TGL_TOEFL,JT.JAM_MULAI as JAM_TOEFL,JT.MENIT_MULAI as MENIT_TOEFL,RT.ID_RUANG_TOEFL,RT.NM_RUANG AS RUANG_TOEFL,JK.ID_RUANG_KESEHATAN,JK.TGL_TEST as TGL_KESEHATAN,JK.JAM_MULAI AS JAM_KESEHATAN,JK.MENIT_MULAI AS MENIT_KESEHATAN,JK.KELOMPOK_JAM,
        CM.TERIMA_BUKU, CM.TERIMA_JADWAL_ELPT, CM.TERIMA_JADWAL_KESEHATAN
        FROM CALON_MAHASISWA CM, PROGRAM_STUDI PR,JADWAL_TOEFL JT,RUANG_TOEFL RT,JADWAL_KESEHATAN JK
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND CM.ID_JADWAL_TOEFL = JT.ID_JADWAL_TOEFL AND CM.ID_JADWAL_KESEHATAN = JK.ID_JADWAL_KESEHATAN AND JT.ID_RUANG_TOEFL = RT.ID_RUANG_TOEFL AND CM.ID_C_MHS = '$id_c_mhs'");
        $temp = $this->database->FetchArray();
        return $temp;
    }

    function load_jadwal_calon_mahasiswa($no_ujian) {
        $this->database->Query("SELECT CM.ID_JADWAL_KESEHATAN,CM.ID_JADWAL_TOEFL,CM.ID_C_MHS,CM.NO_UJIAN,CM.NM_C_MHS,PR.NM_PROGRAM_STUDI,CM.STATUS,JT.ID_RUANG_TOEFL,JT.TGL_TEST as TGL_TOEFL,JT.JAM_MULAI as JAM_TOEFL,JT.MENIT_MULAI as MENIT_TOEFL,RT.ID_RUANG_TOEFL,RT.NM_RUANG AS RUANG_TOEFL,JK.ID_RUANG_KESEHATAN,JK.TGL_TEST as TGL_KESEHATAN,JK.JAM_MULAI AS JAM_KESEHATAN,JK.MENIT_MULAI AS MENIT_KESEHATAN,JK.KELOMPOK_JAM,
        CM.TERIMA_BUKU, CM.TERIMA_JADWAL_ELPT, CM.TERIMA_JADWAL_KESEHATAN
        FROM CALON_MAHASISWA CM, PROGRAM_STUDI PR,JADWAL_TOEFL JT,RUANG_TOEFL RT,JADWAL_KESEHATAN JK
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND CM.ID_JADWAL_TOEFL = JT.ID_JADWAL_TOEFL AND CM.ID_JADWAL_KESEHATAN = JK.ID_JADWAL_KESEHATAN AND JT.ID_RUANG_TOEFL = RT.ID_RUANG_TOEFL AND CM.NO_UJIAN = '$no_ujian'");
        return $this->database->FetchArray();
    }
    
    function load_data_mahasiswa($no_ujian){
        $this->database->Query("SELECT * FROM CALON_MAHASISWA WHERE NO_UJIAN = '$no_ujian'");
        return $this->database->FetchArray();
    }
    
    function get_status($no_ujian){
        $this->database->Query("SELECT STATUS FROM CALON_MAHASISWA WHERE NO_UJIAN = '$no_ujian'");
        return $this->database->FetchArray();
    }

    function jadwal_elpt() {
        $data = array();
        $this->database->Query("SELECT * FROM JADWAL_TOEFL,RUANG_TOEFL WHERE JADWAL_TOEFL.ID_RUANG_TOEFL= RUANG_TOEFL.ID_RUANG_TOEFL ORDER BY ID_JADWAL_TOEFL");
        while ($temp = $this->database->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function jadwal_kesehatan() {
        $data = array();
        $this->database->Query("SELECT * FROM JADWAL_KESEHATAN ORDER BY ID_JADWAL_KESEHATAN ");
        while ($temp = $this->database->FetchArray()) {
            array_push($data, $temp);
        }
        return $data;
    }

    function jenjang_calon_mahasiswa($no_ujian) {
        $this->database->Query("SELECT J.ID_JENJANG,J.NM_JENJANG FROM CALON_MAHASISWA CM,PROGRAM_STUDI PR,JENJANG J
        WHERE CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI AND PR.ID_JENJANG = J.ID_JENJANG AND CM.NO_UJIAN ='$no_ujian'");
        $temp = $this->database->FetchArray();
        return $temp;
    }

    function update_status_selain_S1_D3($no_ujian) {
        $this->database->Query("UPDATE CALON_MAHASISWA SET STATUS='12',KESEHATAN_KESIMPULAN_AKHIR='1' WHERE NO_UJIAN='$no_ujian' ");
    }
    
    function update_jadwal($no_ujian,$kesehatan,$elpt){
        $this->database->Query("UPDATE CALON_MAHASISWA SET ID_JADWAL_KESEHATAN ='$kesehatan',ID_JADWAL_TOEFL='$elpt' WHERE NO_UJIAN='$no_ujian'");
        $this->database->Query("UPDATE JADWAL_TOEFL JT SET TERISI=(SELECT COUNT(CM.ID_C_MHS) FROM CALON_MAHASISWA CM WHERE JT.ID_JADWAL_TOEFL =  CM.ID_JADWAL_TOEFL)");
        $this->database->Query("UPDATE JADWAL_KESEHATAN JK SET TERISI=(SELECT COUNT(CM.ID_C_MHS) FROM CALON_MAHASISWA CM WHERE JK.ID_JADWAL_KESEHATAN =  CM.ID_JADWAL_KESEHATAN)");
    }

    function update_jadwal_calon_mahasiswa($no_ujian) {

        //if ($this->count_urutan() % 2 == 0) {
            $jadwal_kesehatan = $this->load_jadwal_kesehatan();
            $this->update_jadwal_kesehatan($jadwal_kesehatan['ID_JADWAL_KESEHATAN']);
            $this->database->Query("UPDATE CALON_MAHASISWA SET ID_JADWAL_KESEHATAN='" . $jadwal_kesehatan['ID_JADWAL_KESEHATAN'] . "' WHERE NO_UJIAN='$no_ujian' ");
            $jadwal_elpt = $this->load_jadwal_elpt_advance($no_ujian);
            $this->update_jadwal_elpt($jadwal_elpt['ID_JADWAL_TOEFL']);
            $this->database->Query("UPDATE CALON_MAHASISWA SET ID_JADWAL_TOEFL='" . $jadwal_elpt['ID_JADWAL_TOEFL'] . "' WHERE NO_UJIAN='$no_ujian'");
            $this->update_status_jadwal($no_ujian);
        /*} else {
            $jadwal_elpt = $this->load_jadwal_elpt();
            $this->update_jadwal_elpt($jadwal_elpt['ID_JADWAL_TOEFL']);
            $this->database->Query("UPDATE CALON_MAHASISWA SET ID_JADWAL_TOEFL='" . $jadwal_elpt['ID_JADWAL_TOEFL'] . "' WHERE NO_UJIAN='$no_ujian'");
            $jadwal_kesehatan = $this->load_jadwal_kesehatan_advance($no_ujian);
            $this->update_jadwal_kesehatan($jadwal_kesehatan['ID_JADWAL_KESEHATAN']);
            $this->database->Query("UPDATE CALON_MAHASISWA SET ID_JADWAL_KESEHATAN='" . $jadwal_kesehatan['ID_JADWAL_KESEHATAN'] . "' WHERE NO_UJIAN='$no_ujian'");
            $this->update_status_jadwal($no_ujian);
        }*/
    }

    function update_jadwal_kesehatan($id_jadwal) {
        $this->database->Query("UPDATE JADWAL_KESEHATAN SET TERISI = TERISI+1 WHERE ID_JADWAL_KESEHATAN='$id_jadwal'");
    }

    function update_jadwal_elpt($id_jadwal) {
        $this->database->Query("UPDATE JADWAL_TOEFL SET TERISI = TERISI+1 WHERE ID_JADWAL_TOEFL='$id_jadwal'");
    }

    function load_jadwal_kesehatan() {
        $data = array();
        $this->database->Query("SELECT * FROM(SELECT * FROM JADWAL_KESEHATAN WHERE TERISI < KAPASITAS AND TGL_TEST > '11-AUG-2011' ORDER BY ID_JADWAL_KESEHATAN) WHERE ROWNUM =1");
        $temp = $this->database->FetchArray();
        return $temp;
    }

    function load_jadwal_elpt() {
        $data = array();
        $this->database->Query("SELECT * FROM(SELECT JT.*,RT.NM_RUANG,RT.KAPASITAS FROM JADWAL_TOEFL JT,RUANG_TOEFL RT WHERE JT.ID_RUANG_TOEFL = RT.ID_RUANG_TOEFL AND JT.TERISI<RT.KAPASITAS AND JT.TGL_TEST >'" . date('d-M-y') . "' ORDER BY JT.ID_JADWAL_TOEFL) WHERE ROWNUM=1");
        $temp = $this->database->FetchArray();
        return $temp;
    }

    function load_jadwal_kesehatan_advance($no_ujian) {
        $data = array();
        $this->database->Query("SELECT * FROM (SELECT JK.* FROM JADWAL_KESEHATAN JK,JADWAL_TOEFL JT,CALON_MAHASISWA CM
        WHERE JT.ID_JADWAL_TOEFL = CM.ID_JADWAL_TOEFL AND CM.NO_UJIAN ='$no_ujian' AND JK.TERISI < JK.KAPASITAS AND JK.TGL_TEST != JT.TGL_TEST AND JK.TGL_TEST > '" . date('d-M-y') . "'
        ORDER BY JK.ID_JADWAL_KESEHATAN) WHERE ROWNUM = 1");
        $temp = $this->database->FetchArray();
        return $temp;
    }

    function load_jadwal_elpt_advance($no_ujian) {
        $data = array();
        $this->database->Query("SELECT * FROM (SELECT JT.* FROM JADWAL_TOEFL JT,RUANG_TOEFL RT,JADWAL_KESEHATAN JK,CALON_MAHASISWA CM
        WHERE JT.ID_RUANG_TOEFL= RT.ID_RUANG_TOEFL AND JT.TGL_TEST > '" . date('d-M-y') . "' AND JK.ID_JADWAL_KESEHATAN = CM.ID_JADWAL_KESEHATAN AND JT.TGL_TEST != JK.TGL_TEST AND JT.TERISI<RT.KAPASITAS AND CM.NO_UJIAN='$no_ujian'
        ORDER BY JT.ID_JADWAL_TOEFL) WHERE ROWNUM =1");
        $temp = $this->database->FetchArray();
        return $temp;
    }

    function update_status_jadwal($no_ujian) {
        $this->database->Query("UPDATE CALON_MAHASISWA SET STATUS='11' WHERE NO_UJIAN = '$no_ujian'");
    }

    function count_urutan() {
        $this->database->Query("SELECT COUNT(ID_C_MHS) AS JUMLAH FROM CALON_MAHASISWA WHERE STATUS > '10'");
        $temp = $this->database->FetchArray();
        return $temp['JUMLAH'];
    }

}

?>
