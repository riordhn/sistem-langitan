<?php
include "config.php";

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC)
{
	
	$email = get('email', '');
	
	if ($email != '')
	{
		$data_set = $db->QueryToArray("
			select email, '-- LEMBAGA --' as nm_pengguna from email
			where id_pengguna is null and email like '%{$email}%'
			UNION
			select email, nm_pengguna from email
			left join pengguna on pengguna.id_pengguna = email.id_pengguna
			where email.id_pengguna is not null and email like '%{$email}%'
			order by email");
		$smarty->assign('data_set', $data_set);
	}
	
	$smarty->display('informasi/email/view.tpl');
}

?>