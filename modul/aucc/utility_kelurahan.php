<?php
	include '../../config.php';
	if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC){
	if(empty($_GET['negara']) && empty($_GET['propinsi']) && empty($_GET['kota']) && empty($_GET['kecamatan'])){
		
		unset($_SESSION['negara']);
		unset($_SESSION['propinsi']);
		unset($_SESSION['kota']);
		
		$nav = '<a href="utility_kelurahan.php">Negara</a><br />';
		$db->Query("select id_negara, nm_negara from negara order by id_negara");
		$Jml_Data = 0;
		$i = 0;
		while($row = $db->FetchRow()){
			$Id_Negara[$i] = $row[0];
			$Nm_Negara[$i] = $row[1];
			$i++;
			$Jml_Data++;
		}
	
	
		
		$smarty->assign('Id_Negara', $Id_Negara);
		$smarty->assign('Nm_Negara', $Nm_Negara);
		$smarty->assign('Jml_Data', $Jml_Data);
		$smarty->display('utility/kelurahan/index.tpl');
	}
	
	else if(empty($_GET['propinsi']) && empty($_GET['kota']) && empty($_GET['kecamatan'])){
		$nav = '<a href="utility_kelurahan.php">Negara</a><br />';
		
		$id_negara = (int)($_GET['negara']);
		$_SESSION['negara'] = $id_negara;
		
		$db->Query("select id_provinsi, nm_provinsi from provinsi where id_negara = '$id_negara' order by id_provinsi");
		$Jml_Data = 0;
		$i = 0;
		while($row = $db->FetchRow()){
			$Id_Propinsi[$i] = $row[0];
			$Nm_Propinsi[$i] = $row[1];
			$i++;
			$Jml_Data++;
		}
	
	
		$smarty->assign('nav', $nav);
		$smarty->assign('Id_Propinsi', $Id_Propinsi);
		$smarty->assign('Nm_Propinsi', $Nm_Propinsi);
		$smarty->assign('Jml_Data', $Jml_Data);
		$smarty->display('utility/kelurahan/pilihPropinsi.tpl');
	}
	
	else if(empty($_GET['kota']) && empty($_GET['kecamatan'])){
		$nav = '<a href="utility_kelurahan.php">Negara</a> / <a href="utility_kelurahan.php?negara=' . $_SESSION['negara'] . '">Propinsi</a><br />';
		
		$id_propinsi = (int)($_GET['propinsi']);
		$db->Query("select id_kota, nm_kota_old from kota where id_provinsi = '$id_propinsi' order by id_kota");
		$Jml_Data = 0;
		$i = 0;
		while($row = $db->FetchRow()){
			$Id_Kota[$i] = $row[0];
			$Nm_Kota[$i] = $row[1];
			$i++;
			$Jml_Data++;
		}
	
		
		$smarty->assign('nav', $nav);
		$smarty->assign('Id_Kota', $Id_Kota);
		$smarty->assign('Nm_Kota', $Nm_Kota);
		$smarty->assign('Jml_Data', $Jml_Data);
		$smarty->display('utility/kelurahan/pilihKota.tpl');
	}
	
	else if(empty($_GET['kecamatan'])){
		

		$nav = '<a href="utility_kelurahan.php">Negara</a> / <a href="utility_kelurahan.php?negara=' . $_SESSION['negara'] . '">Propinsi</a> / <a class="disable-ajax" href="#utility-utility_kelurahan!utility_kelurahan.php?kota=' . $_GET['kota'] . '">Kota</a><br />';
		
		if(isset($_GET['jmlKecamatan'])){
			
			
			$jmlKecamatan = $_GET['jmlKecamatan'];
			$id_kota = $_GET['kota'];
			$_SESSION['kota'] = $id_kota;
		
			$db->Query("select c.nm_negara, b.nm_provinsi, a.nm_kota from kota a
						left join provinsi b on b.id_provinsi = a.id_provinsi
						left join negara c on c.id_negara = b.id_negara
						where a.id_kota = '$id_kota'
			");
			
			while($row = $db->FetchRow()){
				$nm_negara = $row[0];
				$nm_propinsi = $row[1];
				$nm_kota = $row[2];
			}
			
			$direction = $nm_negara . ' > ' . $nm_propinsi . ' > ' . $nm_kota;
			
			
			$smarty->assign('nav', $nav);
			$smarty->assign('id_kota', $id_kota);
			$smarty->assign('jmlKecamatan', $jmlKecamatan);
			$smarty->assign('direction', $direction);
			$smarty->display('utility/kelurahan/add.tpl');
			
		}
		else{
			if(isset($_GET['kota'])){
			
				
				$id_kota = (int)($_GET['kota']);
				$_SESSION['kota'] = $id_kota;
				
				$db->Query("select c.nm_negara, b.nm_provinsi, a.nm_kota from kota a
							left join provinsi b on b.id_provinsi = a.id_provinsi
							left join negara c on c.id_negara = b.id_negara
							where a.id_kota = '$id_kota'
				");
				
				while($row = $db->FetchRow()){
					$nm_negara = $row[0];
					$nm_propinsi = $row[1];
					$nm_kota = $row[2];
				}
				
				$direction = $nm_negara . ' > ' . $nm_propinsi . ' > ' . $nm_kota;
				
				if(isset($_POST['kecamatan'])){
					$kecamatan = $_POST['kecamatan'];
					foreach($kecamatan as $data){
						$db->Query("insert into kecamatan (id_kota, nm_kecamatan) values ('$id_kota', '$data')");
					}
				}
				
				
				$db->Query("select id_kecamatan, nm_kecamatan from kecamatan where id_kota = '$id_kota' order by id_kota");
				$Jml_Data = 0;
				$i = 0;
				while($row = $db->FetchRow()){
					$Id_Kecamatan[$i] = $row[0];
					$Nm_Kecamatan[$i] = $row[1];
					$i++;
					$Jml_Data++;
				}
				
				$smarty->assign('nav', $nav);
				$smarty->assign('Id_Kecamatan', $Id_Kecamatan);
				$smarty->assign('Nm_Kecamatan', $Nm_Kecamatan);
				$smarty->assign('Jml_Data', $Jml_Data);
				$smarty->assign('id_kota', $id_kota);
				$smarty->assign('direction', $direction);
				$smarty->display('utility/kelurahan/kecamatan.tpl');
			}
		}
	}
	
	else{
		$nav = '<a href="utility_kelurahan.php">Negara</a> / <a href="utility_kelurahan.php?negara=' . $_SESSION['negara'] . '">Propinsi</a> / <a class="disable-ajax" href="#utility-utility_kelurahan!utility_kelurahan.php?kota=' . $_SESSION['kota'] . '">Kota</a> / <a class="disable-ajax" href="#utility-utility_kelurahan!utility_kelurahan.php?kecamatan=' . $_GET['kecamatan'] . '">Kecamatan</a><br />';
		
		if(isset($_GET['jmlKelurahan'])){
			
			
			$jmlKelurahan = $_GET['jmlKelurahan'];
			$id_kecamatan = (int)($_GET['kecamatan']);
			$_SESSION['kecamatan'] = $id_kecamatan;
			
			$db->Query("select d.nm_negara, c.nm_provinsi, b.nm_kota, a.nm_kecamatan from kecamatan a
						left join kota b on b.id_kota = a.id_kota
						left join provinsi c on c.id_provinsi = b.id_provinsi
						left join negara d on d.id_negara = c.id_negara
						where a.id_kecamatan = '$id_kecamatan'
			");
			
			while($row = $db->FetchRow()){
				$nm_negara = $row[0];
				$nm_propinsi = $row[1];
				$nm_kota = $row[2];
				$nm_kecamatan = $row[3];
			}
			
			$direction = $nm_negara . ' > ' . $nm_propinsi . ' > ' . $nm_kota . ' > ' . $nm_kecamatan;
			
			
			$smarty->assign('nav', $nav);
			$smarty->assign('id_kecamatan', $id_kecamatan);
			$smarty->assign('jmlKelurahan', $jmlKelurahan);
			$smarty->assign('direction', $direction);
			$smarty->display('utility/kelurahan/add.tpl');
			
			
		}
		else{
			if(isset($_GET['kecamatan'])){
			
				
				$id_kecamatan = (int)($_GET['kecamatan']);
				$_SESSION['kecamatan'] = $id_kecamatan;
				
				$db->Query("select d.nm_negara, c.nm_provinsi, b.nm_kota, a.nm_kecamatan from kecamatan a
							left join kota b on b.id_kota = a.id_kota
							left join provinsi c on c.id_provinsi = b.id_provinsi
							left join negara d on d.id_negara = c.id_negara
							where a.id_kecamatan = '$id_kecamatan'
				");
				
				while($row = $db->FetchRow()){
					$nm_negara = $row[0];
					$nm_propinsi = $row[1];
					$nm_kota = $row[2];
					$nm_kecamatan = $row[3];
				}
				
				$direction = $nm_negara . ' > ' . $nm_propinsi . ' > ' . $nm_kota . ' > ' . $nm_kecamatan;
				
				
				if(isset($_POST['kelurahan'])){
					$kelurahan = $_POST['kelurahan'];
					foreach($kelurahan as $data){
						$db->Query("insert into kelurahan (id_kecamatan, nm_kelurahan) values ('$id_kecamatan', '$data')");
						//echo $data . '<br />';
					}
				}
				
				//echo $id_kecamatan;
				
				
				$db->Query("select id_kelurahan, nm_kelurahan from kelurahan where id_kecamatan = '$id_kecamatan' order by id_kelurahan");
				$Jml_Data = 0;
				$i = 0;
				while($row = $db->FetchRow()){
					$Id_Kelurahan[$i] = $row[0];
					$Nm_Kelurahan[$i] = $row[1];
					$i++;
					$Jml_Data++;
				}
				
				$smarty->assign('nav', $nav);
				$smarty->assign('Id_Kelurahan', $Id_Kelurahan);
				$smarty->assign('Nm_Kelurahan', $Nm_Kelurahan);
				$smarty->assign('Jml_Data', $Jml_Data);
				$smarty->assign('id_kecamatan', $id_kecamatan);
				$smarty->assign('direction', $direction);
				$smarty->display('utility/kelurahan/kelurahan.tpl');
			}
		}
	}
	
	}
	
?>