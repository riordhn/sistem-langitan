<?php
	include '../../config.php';
	if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC){
	if(empty($_GET['negara']) && empty($_GET['propinsi']) && empty($_GET['kota'])){
		
		unset($_SESSION['negara']);
		unset($_SESSION['propinsi']);
		unset($_SESSION['kota']);
		
		$nav = '<a href="utility_kecamatan.php">Negara</a><br />';
		$db->Query("select id_negara, nm_negara from negara order by id_negara");
		$Jml_Data = 0;
		$i = 0;
		while($row = $db->FetchRow()){
			$Id_Negara[$i] = $row[0];
			$Nm_Negara[$i] = $row[1];
			$i++;
			$Jml_Data++;
		}
	
	
		
		$smarty->assign('Id_Negara', $Id_Negara);
		$smarty->assign('Nm_Negara', $Nm_Negara);
		$smarty->assign('Jml_Data', $Jml_Data);
		$smarty->display('utility/kecamatan/index.tpl');
	}
	
	else if(empty($_GET['propinsi']) && empty($_GET['kota'])){
		$nav = '<a href="utility_kecamatan.php">Negara</a><br />';
		
		$id_negara = (int)($_GET['negara']);
		$_SESSION['negara'] = $id_negara;
		
		$db->Query("select id_provinsi, nm_provinsi from provinsi where id_negara = '$id_negara' order by id_provinsi");
		$Jml_Data = 0;
		$i = 0;
		while($row = $db->FetchRow()){
			$Id_Propinsi[$i] = $row[0];
			$Nm_Propinsi[$i] = $row[1];
			$i++;
			$Jml_Data++;
		}
	
	
		$smarty->assign('nav', $nav);
		$smarty->assign('Id_Propinsi', $Id_Propinsi);
		$smarty->assign('Nm_Propinsi', $Nm_Propinsi);
		$smarty->assign('Jml_Data', $Jml_Data);
		$smarty->display('utility/kecamatan/pilihPropinsi.tpl');
	}
	
	else if(empty($_GET['kota'])){
		$nav = '<a href="utility_kecamatan.php">Negara</a> / <a href="utility_kecamatan.php?negara=' . $_SESSION['negara'] . '">Propinsi</a><br />';
		
		$id_propinsi = (int)($_GET['propinsi']);
		$db->Query("select id_kota, nm_kota_old from kota where id_provinsi = '$id_propinsi' order by id_kota");
		$Jml_Data = 0;
		$i = 0;
		while($row = $db->FetchRow()){
			$Id_Kota[$i] = $row[0];
			$Nm_Kota[$i] = $row[1];
			$i++;
			$Jml_Data++;
		}
	
		
		$smarty->assign('nav', $nav);
		$smarty->assign('Id_Kota', $Id_Kota);
		$smarty->assign('Nm_Kota', $Nm_Kota);
		$smarty->assign('Jml_Data', $Jml_Data);
		$smarty->display('utility/kecamatan/pilihKota.tpl');
	}
	
	else{
		

		$nav = '<a href="utility_kecamatan.php">Negara</a> / <a href="utility_kecamatan.php?negara=' . $_SESSION['negara'] . '">Propinsi</a> / <a class="disable-ajax" href="#utility-utility_kecamatan!utility_kecamatan.php?kota=' . $_GET['kota'] . '">Kota</a><br />';
		
		if(isset($_GET['jmlKecamatan'])){
			
			
			$jmlKecamatan = $_GET['jmlKecamatan'];
			$id_kota = $_GET['kota'];
			$_SESSION['kota'] = $id_kota;
		
			$db->Query("select c.nm_negara, b.nm_provinsi, a.nm_kota from kota a
						left join provinsi b on b.id_provinsi = a.id_provinsi
						left join negara c on c.id_negara = b.id_negara
						where a.id_kota = '$id_kota'
			");
			
			while($row = $db->FetchRow()){
				$nm_negara = $row[0];
				$nm_propinsi = $row[1];
				$nm_kota = $row[2];
			}
			
			$direction = $nm_negara . ' > ' . $nm_propinsi . ' > ' . $nm_kota;
			
			
			$smarty->assign('nav', $nav);
			$smarty->assign('id_kota', $id_kota);
			$smarty->assign('jmlKecamatan', $jmlKecamatan);
			$smarty->assign('direction', $direction);
			$smarty->display('utility/kecamatan/add.tpl');
			
		}
		else{
			if(isset($_GET['kota'])){
			
				
				$id_kota = (int)($_GET['kota']);
				$_SESSION['kota'] = $id_kota;
				
				$db->Query("select c.nm_negara, b.nm_provinsi, a.nm_kota from kota a
							left join provinsi b on b.id_provinsi = a.id_provinsi
							left join negara c on c.id_negara = b.id_negara
							where a.id_kota = '$id_kota'
				");
				
				while($row = $db->FetchRow()){
					$nm_negara = $row[0];
					$nm_propinsi = $row[1];
					$nm_kota = $row[2];
				}
				
				$direction = $nm_negara . ' > ' . $nm_propinsi . ' > ' . $nm_kota;
				
				if(isset($_POST['kecamatan'])){
					$kecamatan = $_POST['kecamatan'];
					foreach($kecamatan as $data){
						$db->Query("insert into kecamatan (id_kota, nm_kecamatan) values ('$id_kota', '$data')");
					}
				}
				
				
				$db->Query("select id_kecamatan, nm_kecamatan from kecamatan where id_kota = '$id_kota' order by id_kota");
				$Jml_Data = 0;
				$i = 0;
				while($row = $db->FetchRow()){
					$Id_Kecamatan[$i] = $row[0];
					$Nm_Kecamatan[$i] = $row[1];
					$i++;
					$Jml_Data++;
				}
				
				$smarty->assign('nav', $nav);
				$smarty->assign('Id_Kecamatan', $Id_Kecamatan);
				$smarty->assign('Nm_Kecamatan', $Nm_Kecamatan);
				$smarty->assign('Jml_Data', $Jml_Data);
				$smarty->assign('id_kota', $id_kota);
				$smarty->assign('direction', $direction);
				$smarty->display('utility/kecamatan/kecamatan.tpl');
			}
		}
	}
	}
	
?>