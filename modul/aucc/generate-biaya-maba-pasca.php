<?php
include('config.php');
include('../ppmb/class/penerimaan.class.php');
if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC){
//$id_semester = 5;  // 2011 - Ganjil
//$id_semester = 21; // 2011 - Genap

if ($request_method == 'POST')
{
    if (post('mode') == 'generate')
    {
        $id_c_mhs = $_POST['id_c_mhs'];
        
        // mengecek pembayaran apakah sudah dibayar / belum
        $db->Query("select count(*) jumlah_bayar from pembayaran_cmhs where tgl_bayar is not null and id_c_mhs = {$id_c_mhs}");
        $row = $db->FetchAssoc();
        
        if ($row['JUMLAH_BAYAR'] == 0)
        {
            
            // mendapatkan id_biaya_kuliah
            $db->Query("
                select bk.id_biaya_kuliah, cmb.sp3_1, nvl(db.besar_biaya,0) sp3, s.id_semester from calon_mahasiswa_baru cmb
                join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
                join semester s on (s.tipe_semester = 'REG' and s.thn_akademik_semester = p.tahun and s.nm_semester = p.semester)
                join biaya_kuliah bk on (bk.id_program_studi = cmb.id_program_studi and bk.id_jalur = p.id_jalur and bk.id_semester = s.id_semester and bk.id_kelompok_biaya = cmb.id_kelompok_biaya)
                left join detail_biaya db on db.id_biaya_kuliah = bk.id_biaya_kuliah
                where cmb.id_c_mhs = {$id_c_mhs}");
            $cmb = $db->FetchAssoc();

            $id_biaya_kuliah = $cmb['ID_BIAYA_KULIAH'];
            $id_semester = $cmb['ID_SEMESTER'];
            
            $db->BeginTransaction();
            
            if ($id_biaya_kuliah != '')
            {   
                // menghapus biaya_kuliah_cmhs dan pembayaran yg mungkin sudah di generate
                $db->Query("delete from pembayaran_cmhs where id_c_mhs = {$id_c_mhs}");
                $db->Query("delete from biaya_kuliah_cmhs where id_c_mhs = {$id_c_mhs}");

                // insert biaya kuliah cmhs
                $result = $db->Query("insert into biaya_kuliah_cmhs (id_c_mhs, id_biaya_kuliah) values ({$id_c_mhs}, {$id_biaya_kuliah})");

                // insert pembayaran calon mahasiswa
                $result = $db->Query("
                    insert into pembayaran_cmhs (id_c_mhs, id_semester, id_detail_biaya, besar_biaya, is_tagih)
                        select bkc.id_c_mhs, {$id_semester} as id_semester, db.id_detail_biaya, db.besar_biaya, 'Y' as is_tagih from biaya_kuliah_cmhs bkc
                        left join detail_biaya db on db.id_biaya_kuliah = bkc.id_biaya_kuliah
                        where bkc.id_c_mhs = {$id_c_mhs}");
                        
                // update sp3 yang membayar lebih
                if ($cmb['SP3_1'] > $cmb['SP3'])
                {
                    $db->Query("
                        update pembayaran_cmhs pc set
                            pc.besar_biaya = (select sp3_1 from calon_mahasiswa_baru cmb where cmb.id_c_mhs = pc.id_c_mhs)
                        where pc.id_detail_biaya in (
                            select db.id_detail_biaya from calon_mahasiswa_baru cmb
                            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
                            join biaya_kuliah bk on (bk.id_jalur = p.id_jalur and bk.id_program_studi = cmb.id_program_studi and bk.id_kelompok_biaya = cmb.id_kelompok_biaya and bk.id_semester = {$id_semester})
                            join detail_biaya db on (db.id_biaya_kuliah = bk.id_biaya_kuliah and db.id_biaya = 81)
                            where cmb.id_c_mhs = {$id_c_mhs}) and
                            pc.id_c_mhs = {$id_c_mhs}");
                }
            }
            
            $result = $db->Commit();
            
            if ($result) { echo "OK"; }
        }
        
        exit();
    }
    
    if (post('mode') == 'edit_bk')
    {
        $result = $db->Query("update calon_mahasiswa_baru set id_kelompok_biaya = {$_post['sp3']} where id_c_mhs = {$_post['id_c_mhs']}");
        if ($result)
            echo "OK";
        else
            echo "GAGAL";
        
        exit();
    }
}

if ($request_method == 'GET' || $request_method == 'POST')
{
    $id_penerimaan = get('id_penerimaan', '');
    
    $penerimaan = new Penerimaan($db);
    $smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
    
    if ($id_penerimaan != '')
    {
        $cmb_set = $db->QueryToArray("
            select
                cmb.id_c_mhs, kode_voucher, no_ujian, nm_c_mhs, nm_jenjang, nm_program_studi, nm_prodi_minat, kelas_pilihan,
                cmp.ptn_s1, cmp.ptn_s2, cmb.sp3_1,
                (select count(*) from pembayaran_cmhs pc where pc.id_c_mhs = cmb.id_c_mhs) as pembayaran_cmhs,
                (select count(*) from pembayaran_cmhs pc where pc.id_c_mhs = cmb.id_c_mhs and tgl_bayar is not null) as sudah_bayar,
                (select sum(besar_biaya) from pembayaran_cmhs pc where pc.id_c_mhs = cmb.id_c_mhs) as tagihan,
                (select sum(besar_biaya) from pembayaran_cmhs pc where pc.id_c_mhs = cmb.id_c_mhs and tgl_bayar is not null) as bayar_tagihan,
                cmb.id_program_studi, cmb.id_kelompok_biaya as sp3, p.id_jalur,
                bk.id_semester, bk.besar_biaya_kuliah as varian, nvl(db.besar_biaya,0) as varian_sp3
            from calon_mahasiswa_baru cmb
            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
            join program_studi ps on ps.id_program_studi = cmb.id_program_studi
            join jenjang j on j.id_jenjang = ps.id_jenjang
            left join semester s on (s.tipe_semester = 'REG' and s.thn_akademik_semester = p.tahun and s.nm_semester = p.semester)
            left join biaya_kuliah bk on (bk.id_jalur = p.id_jalur and bk.id_program_studi = cmb.id_program_studi and bk.id_kelompok_biaya = cmb.id_kelompok_biaya and bk.id_semester = s.id_semester)
            left join detail_biaya db on (db.id_biaya_kuliah = bk.id_biaya_kuliah and db.id_biaya = 81)
            left join calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
            left join prodi_minat pm on pm.id_prodi_minat = cmp.id_prodi_minat
            where cmb.id_penerimaan in ({$id_penerimaan}) and tgl_diterima is not null
            order by cmb.no_ujian");

        // Gelombang 3 Diterima
        $where = "WHERE (GELOMBANG_PMDK = 3 AND CM.ID_PROGRAM_STUDI IS NOT NULL) OR (NO_UJIAN IN ('51160115', '51160119', '51150417'))";

        // Spesialis 2010                       : 25
        // Magister 2010 Genap Gelombang II     : 26
        // Doktor 2010 Genap Gelombang III      : 27
        // Profesi 2011 Gasal Gelombang I       : 28
        // Magister 2011 Gasal Gelombang I      : 8
        // Doktor 2011 Gasal Gelombang I        : 9
        // Spesialis 2011 Ganjil Gelombang I    : 29
        // Profesi 2011 Ganjil Gelombang II     : 30
        // Magister 2011 Gasal Gelombang II     : 10

        foreach ($cmb_set as &$cmb)
        {
            $cmb['kb_set'] = $db->QueryToArray("
                select bk.id_kelompok_biaya, kb.nm_kelompok_biaya from biaya_kuliah bk
                left join kelompok_biaya kb on kb.id_kelompok_biaya = bk.id_kelompok_biaya
                where id_program_studi = {$cmb['ID_PROGRAM_STUDI']} and id_jalur = {$cmb['ID_JALUR']} and id_semester = {$cmb['ID_SEMESTER']}");
        }

        $smarty->assign('cmb_set', $cmb_set);

        $smarty->assign('kb_set', $db->QueryToArray("select id_kelompok_biaya, nm_kelompok_biaya from kelompok_biaya order by id_kelompok_biaya"));
    }
}

$smarty->display('generate-biaya-maba-pasca.tpl');
}
?>