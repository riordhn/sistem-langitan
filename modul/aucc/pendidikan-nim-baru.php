<?php
include('../../config.php');
if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC){
$start_execute = microtime(true);

// truncate nim_mhs
//$db->Query("TRUNCATE TABLE NIM_MHS");

// insert calon sementara ke tabel nim_mhs
//$db->Query("
//    INSERT INTO NIM_MHS (ID_C_MHS, NIM_MHS) SELECT * FROM (
//        SELECT CM.ID_C_MHS, LPAD(F.ID_FAKULTAS,2,'0') || '11' || PS.KODE_PROGRAM_STUDI AS NIM_MHS
//        FROM CALON_MAHASISWA CM
//        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI
//        LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
//        WHERE
//          CM.STATUS = 12 AND CM.KESEHATAN_KESIMPULAN_AKHIR = 1 AND
//          CM.ID_C_MHS NOT IN (SELECT ID_C_MHS FROM MAHASISWA WHERE ID_C_MHS IS NOT NULL) AND CM.NO_UJIAN = '51160114'
//        ) T");

// mengambil semua data dari tabel nim_mhs yg belum tergenerate
$nim_mhs_set = array();
$db->Query("
    SELECT NM.ID_C_MHS, NM.NIM_MHS, CM.TGL_VERIFIKASI FROM NIM_MHS NM
    JOIN CALON_MAHASISWA CM ON CM.ID_C_MHS = NM.ID_C_MHS
    WHERE LENGTH(NM.NIM_MHS) < 9
    ORDER BY CM.TGL_VERIFIKASI"); // order by tgl verifikasi
while ($row = $db->FetchAssoc())
    array_push($nim_mhs_set, $row);

// melakukan pencarian slot
foreach ($nim_mhs_set as $nim_mhs)
{
    echo "{$nim_mhs['ID_C_MHS']} -&gt; ";

    if (strlen($nim_mhs['NIM_MHS']) == 6) // untuk format : FFTTPPXXX
    {
        // melakukan pencarian slot
        for ($i = 1; $i < 1000; $i++)
        {
            // mendapatkan nomer urut slot
            $no_urut = str_pad($i, 3, '0', STR_PAD_LEFT);
            $db->Query("
                SELECT COUNT(*) AS HASIL FROM (
                    SELECT NIM_MHS FROM MAHASISWA
                    WHERE ID_C_MHS IS NOT NULL
                    UNION
                    SELECT NIM_MHS FROM NIM_MHS
                    WHERE LENGTH(NIM_MHS) = 9
                ) T
                WHERE T.NIM_MHS = '{$nim_mhs['NIM_MHS']}{$no_urut}'");
            $row = $db->FetchAssoc();
           
            // saat slot kosong
            if ($row['HASIL'] == 0)
            {
                // update data di nim_mhs
                $db->Query("UPDATE NIM_MHS SET NIM_MHS = '{$nim_mhs['NIM_MHS']}{$no_urut}' WHERE ID_C_MHS = {$nim_mhs['ID_C_MHS']}");
                
                // tampilkan data
                echo "{$nim_mhs['NIM_MHS']}{$no_urut}";
                break;
            }
        }
    }
    else if (strlen($nim_mhs['NIM_MHS']) == 7) // untuk format : FFTTPPPXX
    {
        // melakukan pencarian slot
        for ($i = 1; $i < 100; $i++)
        {
            // mendapatkan nomer urut slot
            $no_urut = str_pad($i, 2, '0', STR_PAD_LEFT);
            $db->Query("
                SELECT COUNT(*) AS HASIL FROM (
                    SELECT NIM_MHS FROM MAHASISWA
                    WHERE ID_C_MHS IS NOT NULL
                    UNION
                    SELECT NIM_MHS FROM NIM_MHS
                    WHERE LENGTH(NIM_MHS) = 9
                ) T
                WHERE T.NIM_MHS = '{$nim_mhs['NIM_MHS']}{$no_urut}'");
            $row = $db->FetchAssoc();
           
            // saat slot kosong
            if ($row['HASIL'] == 0)
            {
                // update data di nim_mhs
                $db->Query("UPDATE NIM_MHS SET NIM_MHS = '{$nim_mhs['NIM_MHS']}{$no_urut}' WHERE ID_C_MHS = {$nim_mhs['ID_C_MHS']}");
                
                // tampilkan hasil generate
                echo "{$nim_mhs['NIM_MHS']}{$no_urut}";
                break;
            }
        }
    }
    
    echo "<br/>";
}

// insert ke tabel pengguna dari nim yg sudah jadi
/*
 * // QUERY 1
$db->Query("
    INSERT INTO PENGGUNA (USERNAME, PASSWORD_PENGGUNA, NM_PENGGUNA, ID_AGAMA, KELAMIN_PENGGUNA, TGL_LAHIR_PENGGUNA, TEMPAT_LAHIR, ID_ROLE, JOIN_TABLE)
        SELECT NM.NIM_MHS, NM.NIM_MHS, CM.NM_C_MHS, CM.ID_AGAMA, CM.JENIS_KELAMIN, CM.TGL_LAHIR, CM.ID_KOTA_LAHIR, 3 AS ID_ROLE, 3 AS JOIN_TABLE FROM NIM_MHS NM
        LEFT JOIN CALON_MAHASISWA CM ON CM.ID_C_MHS = NM.ID_C_MHS
        WHERE LENGTH(NM.NIM_MHS) = 9 AND NM.ID_PENGGUNA IS NULL AND NM.ID_MHS IS NULL");


// update tabel nim_mhs dari pengguna
$db->Query("UPDATE NIM_MHS NM SET NM.ID_PENGGUNA = (SELECT P.ID_PENGGUNA FROM PENGGUNA P WHERE P.USERNAME = NM.NIM_MHS) WHERE NM.ID_PENGGUNA IS NULL");

// insert ke tabel mahasiswa dari nim_mhs
$tgl_sekarang = date('d-m-Y');
$tahun_angkatan = date('Y');
$db->Query("INSERT INTO MAHASISWA (ID_PENGGUNA, ID_C_MHS, NIM_MHS, ID_PROGRAM_STUDI, LAHIR_KOTA_MHS, NO_IJAZAH, TGL_TERDAFTAR_MHS, THN_ANGKATAN_MHS,
  ALAMAT_MHS, NM_AYAH_MHS, NM_IBU_MHS, ALAMAT_AYAH_MHS, ALAMAT_IBU_MHS, SIDIK_JARI_MHS)
  SELECT NM.ID_PENGGUNA, NM.ID_C_MHS, NM.NIM_MHS, CM.ID_PROGRAM_STUDI, CM.ID_KOTA_LAHIR, CM.NO_IJAZAH,
  TO_DATE('{$tgl_sekarang}', 'DD-MM-YYYY') AS TGL_DAFTAR, {$tahun_angkatan} AS ANGKATAN, CM.ALAMAT,
  CM.NAMA_AYAH, CM.NAMA_IBU, CM.ALAMAT_AYAH, CM.ALAMAT_IBU, CM.FINGER_DATA
  FROM NIM_MHS NM
  LEFT JOIN CALON_MAHASISWA CM ON CM.ID_C_MHS = NM.ID_C_MHS
  WHERE NM.ID_PENGGUNA IS NOT NULL AND NM.ID_MHS IS NULL");
  
// update nim_mhs dari mahasiswa
$db->Query("UPDATE NIM_MHS NM SET NM.ID_MHS = (SELECT M.ID_MHS FROM MAHASISWA M WHERE M.NIM_MHS = NM.NIM_MHS AND M.ID_C_MHS = NM.ID_C_MHS)");

// update calon_mahasiswa dari nim_mhs
$db->Query("UPDATE CALON_MAHASISWA SET STATUS = 13 WHERE ID_C_MHS IN (SELECT NM.ID_C_MHS FROM NIM_MHS NM)");
*/
$end_execute = microtime(true);

echo "<h2>Nim tergenerate dalam " . ($end_execute - $start_execute) . " detik.</h2>";
}
?>
