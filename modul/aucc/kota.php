<?php
include('config.php');
if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC){
$mode = get('mode', 'view');

if ($request_method == 'POST')
{
	if (post('mode') == 'add')
	{
		$db->Parse("insert into kota (id_provinsi, nm_kota) values (:id_provinsi,:nm_kota)");
		$db->BindByName(':id_provinsi', post('id_provinsi'));
		$db->BindByName(':nm_kota', post('nm_kota'));
		$result = $db->Execute();
		
		$smarty->assign('result', $result ? "Data berhasil ditambahkan" : "Data gagal ditambahkan");
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$q = get('q', '');
		
		if (strlen($q) >= 3)
		{
			$smarty->assign('kota_set', $db->QueryToArray("
				select p.id_provinsi, p.nm_provinsi, k.id_kota, k.nm_kota from kota k
				join provinsi p on p.id_provinsi = k.id_provinsi
				where p.id_negara = 1 and ((lower(nm_provinsi) like '%".strtolower($q)."%') or (lower(nm_kota) like '%".strtolower($q)."%'))
				order by nm_provinsi, nm_kota"));
		}
	}
	
	if ($mode == 'add')
	{
		$smarty->assign('provinsi_set', $db->QueryToArray("select * from provinsi where id_negara = 1 order by nm_provinsi"));
	}
}

$smarty->display("kota/{$mode}.tpl");
}
?>