<?php

include 'config.php';
if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC){

function cek_file_image($link) {
    if (fopen($link,'r')) {
        $new_link = $link;
    } else {
        if (strpos($link, 'jpg')) {
            $new_link = str_replace('jpg', 'JPG', $link);
        } else if (strpos($link, 'png')) {
            $new_link = str_replace('png', 'PNG', $link);
        } else if (strpos($link, 'jpeg')) {
            $new_link = str_replace('jpeg', 'JPEG', $link);
        } else if (strpos($link, 'gif')) {
            $new_link = str_replace('gif', 'GIF', $link);
        }
    }
    if (!fopen($new_link,'r')) {
        return '';
    }
    return $new_link;
}
?>

<?php if (isset($_GET['nim'])) { ?>
    <img src="<?php echo cek_file_image('http://pendidikan.unair.ac.id/foto/foto_ktm/' . $_GET['nim'] . '.jpg'); ?>"/>
    <?php
} else {
    ?>
    <form  action="cek-image.php" method="get">
        <input type="text" name="nim"/><input type="submit" value="Cari Foto"/>
    </form>
    <?php
		}
}
?>
