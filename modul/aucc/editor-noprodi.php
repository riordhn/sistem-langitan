<?php
include ('config.php');
if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC){
if ($request_method == 'POST')
{
	$id_penggunas = implode(",", post('id_penggunas'));
	$db->Query("update validasi_dosen set status_hapus = 1 where id_pengguna in ({$id_penggunas})");
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	$kd_fak=$db->QueryToArray("select id_fakultas, nm_fakultas from fakultas order by id_fakultas");
	$smarty->assign('KD_FAK', $kd_fak);
					
	$status=$db->QueryToArray("select distinct status_dosen from validasi_dosen order by status_dosen");
	$smarty->assign('STATUS', $status);

	$kd_prodi=$db->QueryToArray("select pst.id_program_studi, jjg.nm_jenjang||' - '||pst.nm_program_studi as program_studi
	from program_studi pst
	left join jenjang jjg on pst.id_jenjang=jjg.id_jenjang
	order by jjg.nm_jenjang, pst.nm_program_studi");
	$smarty->assign('KD_PRODI', $kd_prodi);

	$dosen=$db->QueryToArray("select id_pengguna, username, nm_pengguna from validasi_dosen
	where (id_fakultas is null or id_fakultas='' or  id_program_studi is null or id_program_studi='')
	and status_hapus=0
	order by nm_pengguna");
	$smarty->assign('DOSEN', $dosen);
}
$smarty->display('editor/noprodi/view.tpl');
}
?>