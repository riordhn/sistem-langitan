<?php
include 'config-mysql.php';

ob_start();
$query = mysql_query("select f.nm_fakulta, p.nm_prodi, mhs.nim, mhs.nama, j.nm_jenjang, jl.nm_jalur,p.nama_lengkap, np.nm_program as nm_program
        from mhs_ua mhs
        left join kd_prodi p on p.id_prod = mhs.id_prod
        left join nm_program np on np.id_program = p.program
        left join kd_fakul f on f.kd_fakulta = p.kd_fakulta
        left join kd_jenja j on j.kd_jenjang = p.kd_jenjang
        left join kd_jalur jl on jl.kd_jalur = p.kd_jalur");

  

?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
<table cellpadding="3" cellspacing="3" border="1">
    <thead>
        <tr>
            <td class="header_text">NAMA</td>
			<td class="header_text">NIM</td>
            <td class="header_text">JENJANG</td>
            <td class="header_text">JALUR</td>
            <td class="header_text">PRODI</td>
			<td class="header_text">PRODI LENGKAP</td>
			<td class="header_text">FAKULTAS</td>
            <td class="header_text">PROGRAM</td>
        </tr>
    </thead>
    <tbody>
        <?php
        while($data=mysql_fetch_array($query)) {
            echo
            "<tr>
                <td>{$data['nama']}</td>
                <td>'{$data['nim']}</td>
                <td>{$data['nm_jenjang']}</td>
                <td>{$data['nm_jalur']}</td>
                <td>{$data['nm_prodi']}</td>
				<td>{$data['nama_lengkap']}</td>
                <td>{$data['nm_fakulta']}</td>
                <td>{$data['nm_program']}</td>
            </tr>";
        }
        ?>
    </tbody>
</table>

<?php
$filename = 'Excel Mahasiswa Unair ' . date('Y-m-d');
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
