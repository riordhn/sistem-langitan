<?php

include 'config.php';
include 'class/aucc.class.php';
if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC){
$aucc = new AUCC_Modul($db);
if (isset($_POST['cari_nim'])) {
    $nim = str_replace("'", "''", $_POST['cari_nim']);
    $smarty->assign('data_pembayaran', $aucc->detail_pembayaran($nim));
    $smarty->assign('data_semester', $aucc->get_semester_all());
    $smarty->assign('cari_nim', 1);
}
if ($_POST['insert']) {
    $db->Query("
        INSERT INTO AUCC.PEMBAYARAN (ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, BESAR_BIAYA, IS_TAGIH)
            SELECT BKM.ID_MHS, {$_POST['semester']} AS ID_SEMESTER, DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, 'Y' AS IS_TAGIH FROM AUCC.BIAYA_KULIAH_MHS BKM
            JOIN AUCC.DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
            LEFT JOIN AUCC.MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
            WHERE
	M.ID_MHS IN (SELECT ID_MHS FROM AUCC.BIAYA_KULIAH_MHS) AND
	M.NIM_MHS='{$_POST['cari_nim']}'");
    write_log(' ' . $user->ID_PENGGUNA . " Menambah pembayaran baru NIM_MHS : " . $_POST['cari_nim'] . '');
} else if ($_POST['insert_one']) {
    $data = $aucc->get_detail_pembayaran($_POST['id_pembayaran']);
    $db->Query("INSERT INTO AUCC.PEMBAYARAN (ID_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,BESAR_BIAYA,IS_TAGIH) 
        VALUES ('{$data['ID_MHS']}','{$_POST['semester']}','{$data['ID_DETAIL_BIAYA']}','{$data['BESAR_BIAYA']}','Y')");
}
$smarty->assign('data_detail_pembayaran', $aucc->get_detail_pembayaran_mahasiswa($nim));
$smarty->assign('data_pembayaran', $aucc->detail_pembayaran($nim));
$smarty->display('insert-pembayaran.tpl');
}
?>
