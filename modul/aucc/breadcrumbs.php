<?php
require_once('config.php');
if ($user->IsLogged() && $user->Role() == AUCC_ROLE_AUCC){
$location = explode("-", $_GET['location']);

foreach ($user->MODULs as $modul)
{
	if ($modul['NM_MODUL'] == $location[0])
	{
		$smarty->assign('modul', $modul);
		
		foreach ($modul['MENUs'] as $menu)
		{
            if (isset($location[1]))
            {
                if ($menu['NM_MENU'] == $location[1])
                {
                    $smarty->assign('menu', $menu);
                }
            }
            else
            {
                $smarty->assign('menu', $modul['MENUs'][0]);
            }
		}
	}
}

$smarty->display('breadcrumbs.tpl');
}
?>
