<?php

$id_pt = $id_pt_user;

class keuangan
{

    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    function get_semester($id_semester)
    {
        $this->db->Query("SELECT * FROM AUCC.SEMESTER WHERE ID_SEMESTER='{$id_semester}'");
        return $this->db->FetchAssoc();
    }

    private function get_condition_akademik($id_fakultas, $id_prodi)
    {
        if ($id_fakultas != "" && $id_prodi != "") {
            $query = " AND PS.ID_FAKULTAS = '{$id_fakultas}' AND PS.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } else if ($id_fakultas != "") {
            $query = " AND PS.ID_FAKULTAS = '{$id_fakultas}' ";
        } else {
            $query = " ";
        }
        return $query;
    }


    function load_biaya()
    {
        return $this->db->QueryToArray("SELECT * FROM BIAYA WHERE ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "' ORDER BY NM_BIAYA");
    }

    // fungsi laporan tagihan / piutang

    function load_rekap_tagihan_mahasiswa($fakultas, $status, $semester)
    {
        $c_status = count($status);
        if ($c_status > 1) {
            $arr_s = '';
            for ($i = 0; $i < $c_status; $i++) {
                if ($i == ($c_status - 1)) {
                    $arr_s .= $status[$i];
                } else {
                    $arr_s .= $status[$i] . ',';
                }
            }
            $q_status = "AND SP.ID_STATUS_PENGGUNA IN ({$arr_s})";
        } else {
            $q_status = $status[0] != '' ? "AND SP.ID_STATUS_PENGGUNA ='{$status[0]}'" : "";
        }

        if ($fakultas != '') {
            $query =
                "
                SELECT
                    PS.ID_PROGRAM_STUDI,
                    J.NM_JENJANG,
                    PS.NM_PROGRAM_STUDI,
                    COUNT( TAG.ID_MHS ) JUMLAH_MHS,
                    SUM( TAG.TOTAL_TAGIHAN ) TOTAL_TAGIHAN,
                    SUM(TAG.TOTAL_TERBAYAR  ) TOTAL_TERBAYAR
                FROM
                    PROGRAM_STUDI PS
                    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
                    LEFT JOIN (
                    SELECT
                        PS.ID_PROGRAM_STUDI,
                        TM.ID_SEMESTER,
                        M.ID_MHS,
                        SUM(CASE WHEN T.BESAR_BIAYA IS NOT NULL THEN T.BESAR_BIAYA ELSE 0 END) TOTAL_TAGIHAN,
                        SUM(CASE WHEN PEM.BESAR_PEMBAYARAN IS NOT NULL THEN PEM.BESAR_PEMBAYARAN ELSE 0 END) TOTAL_TERBAYAR 
                    FROM
                        TAGIHAN_MHS TM
                        JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS = TM.ID_TAGIHAN_MHS
                        LEFT JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN = T.ID_TAGIHAN
                        JOIN MAHASISWA M ON M.ID_MHS = TM.ID_MHS
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI 
                        JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
                    WHERE
                        TM.ID_SEMESTER = '{$semester}' 
                        {$q_status}
                    GROUP BY
                        PS.ID_PROGRAM_STUDI,
                        TM.ID_SEMESTER,
                        M.ID_MHS 
                    HAVING 
                        SUM(
                            COALESCE( T.BESAR_BIAYA, 0 )) >
                        SUM(
                            COALESCE( PEM.BESAR_PEMBAYARAN, 0 ))
                    ) TAG ON TAG.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
                WHERE
                    F.ID_FAKULTAS='{$fakultas}' AND
                    F.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "' 
                GROUP BY                    
                    PS.ID_PROGRAM_STUDI,
                    J.NM_JENJANG,
                    PS.NM_PROGRAM_STUDI
                ";
        } else {

            $query =
                "
                SELECT
                    F.ID_FAKULTAS,
                    F.NM_FAKULTAS,
                    COUNT( TAG.ID_MHS ) JUMLAH_MHS,
                    SUM( TAG.TOTAL_TAGIHAN ) TOTAL_TAGIHAN,
                    SUM(TAG.TOTAL_TERBAYAR  ) TOTAL_TERBAYAR
                FROM
                    FAKULTAS F
                    LEFT JOIN (
                    SELECT
                        PS.ID_FAKULTAS,
                        TM.ID_SEMESTER,
                        M.ID_MHS,
                        SUM(CASE WHEN T.BESAR_BIAYA IS NOT NULL THEN T.BESAR_BIAYA ELSE 0 END) TOTAL_TAGIHAN,
                        SUM(CASE WHEN PEM.BESAR_PEMBAYARAN IS NOT NULL THEN PEM.BESAR_PEMBAYARAN ELSE 0 END) TOTAL_TERBAYAR 
                    FROM
                        TAGIHAN_MHS TM
                        JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS = TM.ID_TAGIHAN_MHS
                        LEFT JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN = T.ID_TAGIHAN
                        JOIN MAHASISWA M ON M.ID_MHS = TM.ID_MHS
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI 
                        JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
                    WHERE
                        TM.ID_SEMESTER = '{$semester}' 
                        {$q_status}
                    GROUP BY
                        PS.ID_FAKULTAS,
                        TM.ID_SEMESTER,
                        M.ID_MHS                     
                    HAVING 
                        SUM(
                            COALESCE( T.BESAR_BIAYA, 0 )) >
                        SUM(
                            COALESCE( PEM.BESAR_PEMBAYARAN, 0 ))
                    ) TAG ON TAG.ID_FAKULTAS = F.ID_FAKULTAS 
                WHERE
                    F.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "' 
                GROUP BY
                    F.ID_FAKULTAS,
                F.NM_FAKULTAS                
                ";
        }
        // die($query);
        return $this->db->QueryToArray($query);
    }

    function load_detail_tagihan_mhs_row($fakultas, $prodi, $status, $semester)
    {
        $condition_akademik = $this->get_condition_akademik($fakultas, $prodi);
        $c_status = count($status);
        if ($c_status > 1) {
            $arr_s = '';
            for ($i = 0; $i < $c_status; $i++) {
                if ($i == ($c_status - 1)) {
                    $arr_s .= $status[$i];
                } else {
                    $arr_s .= $status[$i] . ',';
                }
            }
            $q_status = "AND SP.ID_STATUS_PENGGUNA IN ({$arr_s})";
        } else {
            $q_status = $status[0] != '' ? "AND SP.ID_STATUS_PENGGUNA ='{$status[0]}'" : "";
        }
        $query = "
        SELECT PS.ID_MHS,PS.ID_PROGRAM_STUDI,PS.THN_ANGKATAN_MHS,PS.ID_FAKULTAS,
        PS.NM_FAKULTAS,PS.ID_PERGURUAN_TINGGI,PS.NAMA_PERGURUAN_TINGGI,
        PS.ID_JENJANG,PS.NIM_MHS,PS.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,PS.NM_JALUR,
        PS.NM_JENJANG,PB.TAGIHAN
        FROM
        (
            SELECT M.ID_MHS,PS.ID_PROGRAM_STUDI,PS.ID_FAKULTAS,FAK.NM_FAKULTAS,
            PG.ID_PERGURUAN_TINGGI,PG.NAMA_PERGURUAN_TINGGI,J.ID_JENJANG,M.NIM_MHS,P.NM_PENGGUNA,
            PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,J.NM_JENJANG,M.THN_ANGKATAN_MHS
            FROM PENGGUNA P
            JOIN MAHASISWA M ON P.ID_PENGGUNA  = M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=PS.ID_FAKULTAS
            JOIN PERGURUAN_TINGGI PG ON PG.ID_PERGURUAN_TINGGI=FAK.ID_PERGURUAN_TINGGI
            LEFT JOIN ADMISI JM ON JM.ID_MHS = M.ID_MHS AND JM.ID_JALUR IS NOT NULL
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR 
		    JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            WHERE FAK.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'             
            {$q_status}
        ) PS
		  ,
		  (SELECT TM.ID_MHS,( SUM(CASE WHEN TAG.BESAR_BIAYA IS NOT NULL THEN TAG.BESAR_BIAYA ELSE 0 END) -
            SUM(CASE WHEN PEM.BESAR_PEMBAYARAN IS NOT NULL THEN PEM.BESAR_PEMBAYARAN ELSE 0 END) ) TAGIHAN
          FROM TAGIHAN TAG 
          LEFT JOIN PEMBAYARAN PEM ON TAG.ID_TAGIHAN=PEM.ID_TAGIHAN
          JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS=TAG.ID_TAGIHAN_MHS
		  WHERE TM.ID_SEMESTER='{$semester}'
          GROUP BY TM.ID_MHS
          HAVING 
            SUM(
                COALESCE( TAG.BESAR_BIAYA, 0 )) >
            SUM(
                COALESCE( PEM.BESAR_PEMBAYARAN, 0 ))
        ) PB
        WHERE PS.ID_MHS = PB.ID_MHS {$condition_akademik}
        GROUP BY PS.ID_MHS,
            PS.ID_PROGRAM_STUDI,
            PS.THN_ANGKATAN_MHS,
            PS.ID_FAKULTAS,
            PS.NM_FAKULTAS,
            PS.ID_PERGURUAN_TINGGI,
            PS.NAMA_PERGURUAN_TINGGI,
            PS.ID_JENJANG,
            PS.NIM_MHS,
            PS.NM_PENGGUNA,
            PS.NM_PROGRAM_STUDI,
            PS.NM_JALUR,
            PS.NM_JENJANG,
            PB.TAGIHAN 
        ORDER BY PS.NM_PENGGUNA";
        // die($query);
        return $this->db->QueryToArray($query);
    }

    function save_wh_detail_tagihan_mhs($fakultas, $prodi, $status, $semester, $form_params)
    {
        // REFRESH DATA
        $this->db->Query("
            DELETE WH_KEU_TAGIHAN_MHS 
            WHERE ID_FAKULTAS='{$fakultas}'            
            AND ID_PRODI='{$prodi}'
            AND ID_SEMESTER='{$semester}'
            AND FORM_PARAMS='{$form_params}'
            ");
        $smstr = $this->get_semester($semester);
        $biayas = $this->load_biaya();
        $query_insert_columns = "
                ID_MHS,
                NIM,
                NAMA,
                ID_PRODI,
                PRODI,
                ID_FAKULTAS,
                FAKULTAS,
                ID_UNIVERSITAS,
                UNIVERSITAS,
                ANGKATAN,
                JALUR,
                ID_SEMESTER,
                SEMESTER,";
        $index_biaya = 1;
        foreach ($biayas as $b) {
            $query_insert_columns .= "BIAYA_" . $index_biaya . ",";
            $index_biaya++;
        }
        $query_insert_columns .= "UPDATED_AT,TOTAL,FORM_PARAMS";
        $rows = $this->load_detail_tagihan_mhs_row($fakultas, $prodi, $status, $semester);
        foreach ($rows as $data) {
            $query = "
                SELECT B.ID_BIAYA,B.NM_BIAYA,
                ( 
                    SUM(BESAR_BIAYA) -
                    SUM(BESAR_PEMBAYARAN)
                 )
                BESAR_BIAYA
                    FROM BIAYA B
                LEFT JOIN (
                      SELECT B.ID_BIAYA,
                      SUM(CASE WHEN TAG.BESAR_BIAYA IS NOT NULL THEN TAG.BESAR_BIAYA ELSE 0 END) BESAR_BIAYA,
                      SUM(CASE WHEN PEM.BESAR_PEMBAYARAN IS NOT NULL THEN PEM.BESAR_PEMBAYARAN ELSE 0 END) BESAR_PEMBAYARAN
                      FROM MAHASISWA M
                      JOIN TAGIHAN_MHS TM ON TM.ID_MHS=M.ID_MHS
                      JOIN TAGIHAN TAG ON TAG.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
                      LEFT JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN = TAG.ID_TAGIHAN
                      JOIN DETAIL_BIAYA DB ON TAG.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
                      JOIN BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
                      WHERE M.ID_MHS='{$data['ID_MHS']}' AND TM.ID_SEMESTER='{$semester}'
                      AND B.ID_PERGURUAN_TINGGI = '" . getenv('ID_PT') . "'
                      GROUP BY B.ID_BIAYA
                      ) PEM ON B.ID_BIAYA = PEM.ID_BIAYA
                WHERE B.ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'
                GROUP BY B.ID_BIAYA,B.NM_BIAYA
                ORDER BY B.NM_BIAYA";
            // die($query);
            $str_semester = $smstr['NM_SEMESTER'] . ' ' . $smstr['TAHUN_AJARAN'];
            $nama = preg_replace("/'/", "", $data['NM_PENGGUNA']);
            $prodi = '( ' . $data['NM_JENJANG'] . ' ) ' . $data['NM_PROGRAM_STUDI'];
            $query_insert_values = "
                {$data['ID_MHS']},
                '{$data['NIM_MHS']}',
                '{$nama}',
                {$data['ID_PROGRAM_STUDI']},
                '{$prodi}',
                {$data['ID_FAKULTAS']},
                '{$data['NM_FAKULTAS']}',
                {$data['ID_PERGURUAN_TINGGI']},
                '{$data['NAMA_PERGURUAN_TINGGI']}',
                '{$data['THN_ANGKATAN_MHS']}',
                '{$data['NM_JALUR']}',
                {$smstr['ID_SEMESTER']},
                '{$str_semester}',
            ";
            $tagihans = $this->db->QueryToArray($query);
            $total = 0;
            foreach ($tagihans as $t) {
                $biaya = $t['BESAR_BIAYA'] == '' ? 0 : $t['BESAR_BIAYA'];
                $query_insert_values .= "{$biaya},";
                $total += $biaya;
            }
            $query_insert_values .= "CURRENT_TIMESTAMP,{$total},'{$form_params}'";

            $query_insert_rows .= "
            INTO WH_KEU_TAGIHAN_MHS
                ({$query_insert_columns})
            VALUES
                ({$query_insert_values})
            ";
        }
        $query_insert = " 
        INSERT ALL
            {$query_insert_rows}
        SELECT * FROM dual
        ";
        $this->db->Query($query_insert);
    }
}
