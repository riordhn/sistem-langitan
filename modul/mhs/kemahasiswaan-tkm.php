<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {

	// ambil id_mhs
	$id_mhs = "";
	$kueri = "select id_mhs from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

	$tkm_lihat = "";
	$kueri = "select b.nm_semester, b.thn_akademik_semester, a.nm_krp_khp, a.waktu_krp_khp, a.skor_krp_khp from krp_khp a, semester b where a.id_semester=b.id_semester and a.id_mhs='".$id_mhs."' ";
	$result = $db->Query($kueri)or die("salah kueri : ");
	while($r = $db->FetchRow()) {
		$tkm_lihat .= '
		<tr>
			<td align="center"><span>'.$r[1].' '.$r[0].'</span></td>
			<td align="center"><span>'.$r[2].'</span></td>
			<td align="center"><span>'.$r[3].'</span></td>
			<td align="center">'.$r[4].'</td>
		</tr>
		';
	}

	$smarty->assign('tkm_lihat', $tkm_lihat );

	$smarty->display('kemahasiswaan-tkm.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>