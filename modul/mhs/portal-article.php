<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

	$date = getdate();
	$kd_hari = $date['wday'];
	$kd_hari = $kd_hari + 1;
	$bulan = $date['month'];
	$tanggal = $date['mday'];
	$tahun = $date['year'];
	$id_pengguna = $user->ID_PENGGUNA;
	$id_article = array();
	$judul_article = array();
	$tgl_article = array();
	$isi_article = array();
	$resume_article = array();
	$link_article = array();
	$img_link = array();
	$jml_data_article = 0;
	$id = get('id');
	$view = get('view');
	$get_id_article = 0;
	$is_active = array();
	$is_approve = array();
	$pengguna = array();
	
	$today = date("Y-m-d"); 

	if ($request_method == 'GET' or $request_method == 'POST')
	{
		$get_id_article = (get('id'));
		$get_id_article = preg_replace('#\[script\](.*?)\[/script\]#', '<img src="$1" />', $get_id_article); 		
	}
	

	if($_GET['id']){
		$db->Query("select n.id_article, n.judul, n.tgl, n.resume, n.img_link, n.link, p.nm_pengguna, n.isi, n.is_active, n.is_approve from article n 
		left join pengguna p on p.id_pengguna = n.id_pengguna
		where n.id_article = '" . get('id') . "' and n.is_active = 1 order by n.id_article");
		$i = 0;
		while ($row = $db->FetchRow()){ 
			$id_article[$i] = $row[0];
			$judul_article[$i] = $row[1];
			$tgl_article[$i] = $row[2];
			$isi_article[$i] = $row[7];
			$resume_article[$i] = $row[3];
			$img_link[$i] = $row[4];
			//$resume_article[$i] = substr($resume_article[$i], 0, 256) . ' ...'; 
			$link_article[$i] = $row[5];
			$pengguna[$i] = $row[6];
			$is_active[$i] = $row[8];
			$is_approve[$i] = $row[9];
			$jml_data_article++;
			$i++;
		}
	}
	else{
		$db->Query("select n.id_article, n.judul, n.tgl, n.resume, n.img_link, n.link, p.nm_pengguna, n.isi, n.is_active from article n 
		left join pengguna p on p.id_pengguna = n.id_pengguna
		where n.id_pengguna = '" . $id_pengguna . "' and n.is_active = 1
		order by n.id_article");
		$i = 0;
		while ($row = $db->FetchRow()){ 
			$id_article[$i] = $row[0];
			$judul_article[$i] = $row[1];
			$tgl_article[$i] = $row[2];
			$isi_article[$i] = $row[7];
			$resume_article[$i] = $row[3];
			//$resume_article[$i] = substr($resume_article[$i], 0, 256) . ''; 
			$img_link[$i] = $row[4];
			$link_article[$i] = $row[5];
			$pengguna[$i] = $row[6];
			$is_active[$i] = $row[8];
			$is_approve[$i] = $row[9];
			$jml_data_article++;
			$i++;
		}
	}
	
	$db->Query("select nm_pengguna from pengguna where id_pengguna = '" . $id_pengguna . "'");
	$i = 0;
	while ($row = $db->FetchRow()){ 
		$nm_pengguna = $row[0];
		$i++;
	}
	
	
	$count_article = round($jml_data_article/5);
	$smarty->assign('get_id_article', $get_id_article);
	$smarty->assign('id', $id);
	$smarty->assign('view', $view);
	$smarty->assign('count_article', $count_article);
	$smarty->assign('id_article',$id_article);
	$smarty->assign('judul_article', $judul_article);
	$smarty->assign('tgl_article', $tgl_article);
	$smarty->assign('isi_article', $isi_article); 
	$smarty->assign('resume_article', $resume_article); 
	$smarty->assign('link_article', $link_article); 
	$smarty->assign('img_link', $img_link);
	$smarty->assign('jml_data_article', $jml_data_article); 
	$smarty->assign('is_active', $is_active);
	$smarty->assign('is_approve', $is_approve);
	
	$smarty->assign('id_pengguna', $id_pengguna);
	$smarty->assign('nm_pengguna', $nm_pengguna);
	$smarty->assign('pengguna', $pengguna);
	$smarty->assign('today', $today);
	$smarty->assign('kd_hari', $kd_hari);
	$smarty->assign('id_hari', $id_hari);
	$smarty->assign('hari', $hari[0]); 
	$smarty->assign('tanggal', $tanggal); 
	$smarty->assign('bulan', $bulan); 
	$smarty->assign('tahun', $tahun); 
	$smarty->display('portal-article.tpl');
?>