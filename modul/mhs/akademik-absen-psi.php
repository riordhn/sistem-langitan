<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

// Yudi Sulistya, 02 Aug 2013 (tablesorter)
echo '
<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[4,0]], widgets: ["zebra"]
		}
	);
}
);
</script>
';

$db2 = new MyOracle();

	$kueri = "select id_mhs from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 6 : ");
	while($r = $db->FetchRow()) {
		$id_mhs=$r[0];
	}
//smt_aktif
$kueri = "select id_semester from semester where status_aktif_semester='True'";
	$result = $db->Query($kueri)or die("salah kueri 6 : ");
	while($r = $db->FetchRow()) {
		$smt_aktif = $r[0];
	}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	$isi_absen = '
	<p></p>
	<table width="850" id="myTable" class="tablesorter">
	<thead>
	<tr>
		<th>Mata Kuliah</th>
		<th>Kelas</th>
		<th>Jumlah<br>Pertemuan</th>
		<th>Jumlah<br>Kehadiran</th>
		<th>Prosentase</th>
		<th>Cekal Ujian</th>
	</tr>
	</thead>
	<tbody>
	';
	
	$kueri = "select s1.id_pengambilan_mk,s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nama_kelas,
			count(presensi_kelas.id_kelas_mk) as tm,s1.status,s1.id_mhs,
			sum(case when presensi_mkmhs.kehadiran>0 then 1 else 0 end) as hadir,
			case when count(presensi_kelas.id_kelas_mk)=0 then 0 else round(sum(case when presensi_mkmhs.kehadiran>0 then 1 else 0 end)/count(presensi_kelas.id_kelas_mk)*100,2) end as prosen,
			cekal_uts,cekal_uas
			from (select id_pengambilan_mk,pengambilan_mk.id_mhs,pengambilan_mk.id_kelas_mk,
			kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,
			case when pengambilan_mk.status_ulangke > 0 then 'ULANG' else 'BARU' end as status,
			case when status_cekal_uts='3' then 'CEKAL' else
			case when status_cekal_uts='1' then 'TDK DICEKAL' else 'BLM DIPROSES' end end as cekal_uts,
			case when status_cekal='0' then 'CEKAL' else
			case when status_cekal='1' then 'TDK DICEKAL' else 'BLM DIPROSES' end end as cekal_uas
			from pengambilan_mk
			left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk and kelas_mk.id_semester='$smt_aktif'
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			where pengambilan_mk.id_semester='$smt_aktif' and status_apv_pengambilan_mk='1' and pengambilan_mk.id_mhs=$id_mhs)s1
			left join presensi_kelas on s1.id_kelas_mk=presensi_kelas.id_kelas_mk
			left join presensi_mkmhs on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas and s1.id_mhs=presensi_mkmhs.id_mhs
			group by s1.id_pengambilan_mk,s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,s1.status,s1.id_mhs,cekal_uts,cekal_uas,kredit_semester";
				 
	$result = $db->Query($kueri)or die("salah kueri : 76");
	$row="";$i=0;
	while($r = $db->FetchArray()) {
		$i++;
		if($i%2==0){
		$row="#cccccc";
		}
		else{
		$row="#ffffff";
		}
		$cekal = 'Tidak Cekal';
		$isi_absen .= '
		<tr bgcolor='.$row.'>
			<td><span>'.$r[2].' - '.$r[3].'</span></td>
			<td align="center">'.$r[5].'</td>
			<td align="center"><a style="text-decoration:underline;cursor:pointer;" onclick="window.open(\'proses/_akademik_absen_tampil_det.php?mk='.$r[1].'\',\'baru3\');">'.$r[6].'</a></td>
			<td align="center">'.$r[9].'</td>
			<td align="center">'.$r[10].'%</td>
			<td align="center">'.$r[12].'</td>
		</tr>
		';
	}

	$isi_absen .= '
	</tbody>
	</table>
	';
	
	$smarty->assign('isi_absen', $isi_absen);

	$smarty->display('akademik-absen.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>
