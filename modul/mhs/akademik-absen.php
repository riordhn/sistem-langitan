<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

	if ($user->ID_FAKULTAS == 11 ) // Psikologi
	{
		echo '
		<script>
		location.href="#akademik-Rekap Absen!akademik-absen-psi.php";
		</script>';
		exit;
	}

// Yudi Sulistya, 02 Aug 2013 (tablesorter)
echo '
<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[4,0]], widgets: ["zebra"]
		}
	);
}
);
</script>
';

$db2 = new MyOracle();

// ambil nim & password
	$mhs_pass=""; $mhs_nim="";
	$kueri = "select b.nim_mhs, a.se1, c.id_fakultas,b.id_mhs, thn_angkatan_mhs from pengguna a, mahasiswa b, program_studi c 
				where a.id_pengguna=b.id_pengguna and c.id_program_studi = b.id_program_studi and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 6 : ");
	while($r = $db->FetchRow()) {
		$mhs_pass = $r[1];
		$mhs_nim = $r[0];
		$id_fakultas = $r[2];
		$id_mhs=$r[3];
		$thn_angkatan=$r[4];
	}
$smarty->assign('fak', $id_fakultas);

//smt_aktif
$kueri = "select id_semester from semester where id_perguruan_tinggi = {$id_pt_user} and status_aktif_semester='True'";
	$result = $db->Query($kueri)or die("salah kueri 6 : ");
	while($r = $db->FetchRow()) {
		$smt_aktif = $r[0];
	}
if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	$kueri1 = "select id_semester, tahun_ajaran || ' / ' || nm_semester from semester where id_perguruan_tinggi = {$id_pt_user} and thn_akademik_semester>='$thn_angkatan' and nm_semester in ('Ganjil', 'Genap') 
				order by thn_akademik_semester desc, nm_semester desc ";
	$result2 = $db->Query($kueri1)or die("salah kueri 6 : ");
	
	$smt_aktif = isset($_REQUEST['smt']) ? $_REQUEST['smt'] : $smt_aktif;
	
	$isi_absen .= '<form action="akademik-absen.php">
		<select name="smt">';
	
	while($r2 = $db->FetchRow()) {
		$isi_absen .= '<option value="'.$r2[0].'"'; 
		if($smt_aktif == $r2[0]){
			$isi_absen .= ' selected="selected"';
		}
		$isi_absen .= '>'.$r2[1].'</option>';
	}
			
	
	$isi_absen .= '
		</select>
		<input type="submit" value="Tampil" />
	</form>
	<p></p>
	<table width="850" id="myTable" class="tablesorter">
	<thead>
	<tr>
		<th>Mata Ajar</th>
		<th>Kelas</th>
		<th>Jumlah Pertemuan</th>
		<th>Jumlah Kedatangan</th>
		<th>Prosentase</th>
		<th>Cekal UTS</th>
		<th>Cekal UAS</th>
	</tr>
	</thead>
	<tbody>
	';

/*	$kueri = "select pengambilan_mk.id_pengambilan_mk,presensi_kelas.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas, 
			presensi_mkmhs.id_mhs,nim_mhs,nm_pengguna,
			sum(kehadiran) as hadir , count(presensi_kelas.id_presensi_kelas) as tm, 
			round(sum(kehadiran)/count(presensi_kelas.id_presensi_kelas)*100,2)as persen 
			from presensi_mkmhs 
			left join presensi_kelas on presensi_mkmhs.id_presensi_kelas=presensi_kelas.id_presensi_kelas 
			left join mahasiswa on presensi_mkmhs.id_mhs=mahasiswa.id_mhs 
			inner join pengambilan_mk on mahasiswa.id_mhs=pengambilan_mk.id_mhs and presensi_kelas.id_kelas_mk=pengambilan_mk.id_kelas_mk and pengambilan_mk.id_semester='$smt_aktif' 
			left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna 
			left join kelas_mk on presensi_kelas.id_kelas_mk=kelas_mk.id_kelas_mk and kelas_mk.id_semester='$smt_aktif' 
			left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi 
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
			left join semester on semester.id_semester = kelas_mk.id_semester
			where program_studi.id_fakultas='$id_fakultas' and kelas_mk.id_semester='$smt_aktif' and nim_mhs = '$mhs_nim'
			group by pengambilan_mk.id_pengambilan_mk, presensi_kelas.id_kelas_mk, 
			kd_mata_kuliah, nm_mata_kuliah, nama_kelas, presensi_mkmhs.id_mhs, nim_mhs, nm_pengguna";*/
	
	$kueri = "select s1.id_pengambilan_mk,s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nama_kelas,
			count(presensi_kelas.id_kelas_mk) as tm,s1.status,s1.id_mhs,
			sum(case when presensi_mkmhs.kehadiran>0 then 1 else 0 end) as hadir,
			case when count(presensi_kelas.id_kelas_mk)=0 then 0 else round(sum(case when presensi_mkmhs.kehadiran>0 then 1 else 0 end)/count(presensi_kelas.id_kelas_mk)*100,2) end as prosen,
			cekal_uts,cekal_uas
			from (select id_pengambilan_mk,pengambilan_mk.id_mhs,pengambilan_mk.id_kelas_mk,
			kd_mata_kuliah,nm_mata_kuliah,nama_kelas,kurikulum_mk.kredit_semester,
			case when pengambilan_mk.pengulangan_ke > 0 then 'ULANG' else 'BARU' end as status,
			case when status_cekal_uts='3' then 'CEKAL' else
			case when status_cekal_uts='1' then 'TDK DICEKAL' else 'BLM DIPROSES' end end as cekal_uts,
			case when status_cekal='0' then 'CEKAL' else
			case when status_cekal='1' then 'TDK DICEKAL' else 'BLM DIPROSES' end end as cekal_uas
			from pengambilan_mk
			left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk and kelas_mk.id_semester='$smt_aktif'
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			where pengambilan_mk.id_semester='$smt_aktif' and status_apv_pengambilan_mk='1' and pengambilan_mk.id_mhs=$id_mhs)s1
			left join presensi_kelas on s1.id_kelas_mk=presensi_kelas.id_kelas_mk
			left join presensi_mkmhs on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas and s1.id_mhs=presensi_mkmhs.id_mhs
			group by s1.id_pengambilan_mk,s1.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,nama_kelas,s1.status,s1.id_mhs,cekal_uts,cekal_uas,kredit_semester";
				 
	$result = $db->Query($kueri)or die("salah kueri : 76");
	while($r = $db->FetchArray()) {
		
		$cekal = 'Tidak Cekal';
		$isi_absen .= '
		<tr>
			<td><span>'.$r[2].' - '.$r[3].'</span></td>
			<td align="center">'.$r[5].'</td>
			<td align="center"><a style="text-decoration:underline;cursor:pointer;" onclick="window.open(\'proses/_akademik_absen_tampil_det.php?mk='.$r[1].'\',\'baru3\');">'.$r[6].'</a></td>
			<td align="center">'.$r[9].'</td>
			<td align="center"><b>'.$r[10].'%</b></td>
			<td align="center">'.$r[11].'</td>
			<td align="center">'.$r[12].'</td>
		</tr>
		';
	}

	$isi_absen .= '
	</tbody>
	</table>
	';
	
	$smarty->assign('isi_absen', $isi_absen);

	$smarty->display('akademik-absen.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>
