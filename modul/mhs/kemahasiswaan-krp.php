<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	$jkueri_input = '
	<script type="text/javascript">
		function tampildata(){
			$.ajax({
				type:"POST",
				url:"proses/_kemahasiswaan-krp_simpan.php",
				data: "aksi=tampil",
				success: function(data){
					$("#input_buat").html(data);
				}
			});
		}
		tampildata();

		function krp_simpan (f){
			$.ajax({
				type:"POST",
				url:"proses/_kemahasiswaan-krp_simpan.php",
				data: $("#"+f).serialize(),
				success: function(data){
					alert(data);
					tampildata();
				}
			});
		}
		function krp_hapus (v){
			$.ajax({
				type:"POST",
				url:"proses/_kemahasiswaan-krp_simpan.php",
				data: "aksi=hapus&id="+v,
				success: function(data){
					alert(data);
					tampildata();
				}
			});
		}
		function kelom_keg(v){
			if(v>0){
				$.ajax({
					type:"POST",
					url:"proses/_kemahasiswaan-krp_simpan.php",
					data: "aksi=combo_kelomkeg&id="+v,
					success: function(data){
						$("#keg_jenis").html(data);
					}
				});
			}else{
				$.ajax({
					type:"POST",
					url:"proses/_kemahasiswaan-krp_simpan.php",
					data: "aksi=combo_kelomkeg&id=-1",
					success: function(data){
						$("#keg_jenis").html(data);
					}
				});
				jenis_keg2(-1);
				prestasi_keg(-1);
			}
		}
		function jenis_keg2(v){
			$.ajax({
				type:"POST",
				url:"proses/_kemahasiswaan-krp_simpan.php",
				data: "aksi=combo_jeniskeg&id="+v,
				success: function(data){
					$("#keg_tingkat").html(data);
				}
			});
		}
		function prestasi_keg(v){
			$.ajax({
				type:"POST",
				url:"proses/_kemahasiswaan-krp_simpan.php",
				data: "aksi=combo_prestasikeg&id="+v,
				success: function(data){
					$("#keg_prestasi").html(data);
				}
			});
		}
		function jenis_keg(v){
			if(v>0){
				jenis_keg2(v);
				prestasi_keg(v);
			}else{
				jenis_keg2(-1);
				prestasi_keg(-1);
			}
		}
	</script>
	';
	$smarty->assign('jkueri_input', $jkueri_input);

	$smarty->display('kemahasiswaan-krp.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>
