<?php
require('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	$jkueri_khs = '
		<script type="text/javascript">
			function tampilkhs(n){
				$.ajax({
					type:"POST",
					url:"/modul/mhs/proses/_akademik-khs-tampil_fpsi.php",
					data: "aksi=tampil&semes="+n,
					success: function(data){
						$("#tampil_khs").html(data);
					}
				});
			}
		</script>
		';
	$smarty->assign('jkueri_khs', $jkueri_khs);
	
	$judul_khs = "KHS";

	$smarty->assign('khs_judul', $judul_khs);

	$khs_thakad = '
	<form name="form8" method=post action="">
	<table>
	<tr>
		<td>Tahun Akademik</td>
		<td>:</td>
		<td>
		<select name="thn_akademik" onchange="tampilkhs(this.value)">
			<option value="0">---------</option>
	';
		// ambil tahun semester
		//$kweri = "select id_semester,TAHUN_AJARAN,NM_SEMESTER from semester order by thn_akademik_semester,nm_semester";
		$kweri = "
		select a.id_semester,a.tahun_ajaran,a.nm_semester
		from semester a, pengambilan_mk b, mahasiswa c
		where a.id_semester=b.id_semester and b.id_mhs=c.id_mhs and c.id_pengguna='".$user->ID_PENGGUNA."'
		and a.tipe_semester!='UP'
		group by a.id_semester,a.tahun_ajaran,a.nm_semester
		order by a.tahun_ajaran desc, a.nm_semester desc
		";
		$result = $db->Query($kweri)or die("salah kueri 48: ");
		while($r = $db->FetchRow()) {
			$khs_thakad .= '<option value="'.$r[0].'">'.$r[1].' - '.$r[2].'</option>';
		}
	$khs_thakad .= '
		</select>
		</td>
	</tr>
	</table>
	</form>
	';

	$smarty->assign('khs_thakad', $khs_thakad);

	$smarty->display('akademik-khs.tpl');

} else {
	$smarty->display('session-expired.tpl');
}
?>
