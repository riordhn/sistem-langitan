<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

	// ambil nim & password
	/*
	$mhs_pass=""; $mhs_nim="";
	$kueri = "select b.nim_mhs, a.password_pengguna, a.se1 from pengguna a, mahasiswa b where a.id_pengguna=b.id_pengguna and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 6 : ");
	while($r = $db->FetchRow()) {
		$mhs_pass = $r[2];
		$mhs_nim = $r[0];
	}
	*/

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {

	// ambil semester_aktif
	$sem_aktif="";
	$kueri = "select id_semester from semester where STATUS_AKTIF_SEMESTER='True' order by id_semester desc";
	$result = $db->Query($kueri)or die ("salah kueri : ");
	while($r = $db->FetchRow()) {
		$sem_aktif = $r[0];
	}
	
	// ambil id_mhs
	$id_mhs=""; $id_prodi = ""; $id_fakul = ""; $stat_aktif="1"; $status_krs="0"; $nim_mhs = ""; $angkatan = ""; $id_c_mhs = ""; $status_cekal_evabyr="";
	$kueri = "select a.id_mhs, a.id_program_studi, b.id_fakultas, b.status_krs, a.nim_mhs, a.STATUS_AKADEMIK_MHS, a.thn_angkatan_mhs, a.id_c_mhs, a.status_cekal from mahasiswa a, program_studi b where a.id_program_studi=b.id_program_studi and a.id_pengguna='".$user->ID_PENGGUNA."'";
	//echo $kueri;
	$result = $db->Query($kueri)or die ("salah kueri : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
		$id_prodi = $r[1];
		$id_fakul = $r[2];
		$status_krs = $r[3];
		$nim_mhs = $r[4];
		$stat_aktif = $r[5];
		$angkatan = $r[6];
		$id_c_mhs = $r[7];
		$status_cekal_evabyr = $r[8];
	}

	// ambil data cekal
	$kueri = "select status_aktif from status_pengguna where id_status_pengguna='".$stat_aktif."'";
	$result = $db->Query($kueri)or die ("salah kueri : ");
	while($r = $db->FetchRow()) {
		$stat_aktif = $r[0];
	}

	// ambil data bayar
	$bayar_ada = false;
	$kueri = "select count(*) as jumlah_bayar from pembayaran where id_mhs={$id_mhs} and id_semester={$sem_aktif} and id_status_pembayaran not in (2, 4) ";  // --> Perbaikan Fathoni
	//echo $kueri;
	$result = $db->Query($kueri)or die ("salah kueri 433 ");
	$r = $db->FetchAssoc();
	if ($r['JUMLAH_BAYAR'] > 0) { $bayar_ada = true; }

	
	// ambil data Finger
	$finger_ada = false;
	$kueri = "
	SELECT count(*) as JARI FROM FINGERPRINT_MAHASISWA 
	WHERE FINGERPRINT_MAHASISWA.NIM_MHS = '".$nim_mhs."' and FINGERPRINT_MAHASISWA.FINGER_DATA is not null
	";
	$result = $db->Query($kueri)or die ("salah kueri 50 ");
	$r = $db->FetchAssoc();
	if ($r['JARI'] > 0){ 
		$finger_ada = true; 
	}
	
	
	if($bayar_ada == false){
		$jkueri_penawaranmk="Maaf, anda belum bisa KPRS, karena data pembayaran semester ini kosong"; $jkueri_mkterambil=""; $jkueri_respondosen="";
		$smarty->assign('jkueri_penawaranmk', $jkueri_penawaranmk);
		$smarty->assign('jkueri_mkterambil', $jkueri_mkterambil);
		$smarty->assign('jkueri_respondosen', $jkueri_respondosen);
	}else if($finger_ada == false){
		$jkueri_penawaranmk="<br>Maaf, anda belum bisa KPRS, karena belum melakukan sidik jari<br><br>"; $jkueri_mkterambil=""; $jkueri_respondosen="";
		$smarty->assign('jkueri_penawaranmk', $jkueri_penawaranmk);
		$smarty->assign('jkueri_mkterambil', $jkueri_mkterambil);
		$smarty->assign('jkueri_respondosen', $jkueri_respondosen);
	}else if($stat_aktif == '0'){
		$jkueri_penawaranmk="<br>Maaf, anda belum bisa KPRS, karena status anda adalah cekal, hubungi bagian akademik<br><br>"; $jkueri_mkterambil=""; $jkueri_respondosen="";
		$smarty->assign('jkueri_penawaranmk', $jkueri_penawaranmk);
		$smarty->assign('jkueri_mkterambil', $jkueri_mkterambil);
		$smarty->assign('jkueri_respondosen', $jkueri_respondosen);
	}else if($status_cekal_evabyr == '1'){
		$jkueri_penawaranmk="<br>Maaf, anda belum bisa KPRS, karena status anda adalah cekal, hubungi bagian pendidikan rektorat<br><br>"; $jkueri_mkterambil=""; $jkueri_respondosen="";
		$smarty->assign('jkueri_penawaranmk', $jkueri_penawaranmk);
		$smarty->assign('jkueri_mkterambil', $jkueri_mkterambil);
		$smarty->assign('jkueri_respondosen', $jkueri_respondosen);
	}else {

		if($id_prodi==137 || $id_prodi==136) { // KHAS S2 MAPRO & S2 SAINS PSIKOLOGI
			// *****************
			// *** INPUT KPRS ***
			// *****************
			$jkueri_penawaranmk = '
			<script type="text/javascript">
				function tampildatasatu(){
					$.ajax({
						type:"POST",
						url:"proses/_akademik-kprs_ditambah_mapro_psi.php",
						data: "aksi=tampil",
						success: function(data){
							$("#krs_ditambah").html(data);
						}
					});
				}
				tampildatasatu();

				function krstambah_kirim(f){
					$.ajax({
						type:"POST",
						url:"proses/_akademik-kprs_ditambah_mapro_psi.php",
						data: "aksi=input&kelas="+f+"&sid=' . session_id() . '",
						success: function(data){
							alert(data);
							tampildatasatu();
							tampildatadua();
							tampildatatiga();
						}
					});
				}
			</script>
			';
			$smarty->assign('jkueri_penawaranmk', $jkueri_penawaranmk);

			// *****************
			// *** HAPUS KPRS ***
			// *****************
			$jkueri_mkterambil = '
			<script type="text/javascript">
				function tampildatadua(){
					$.ajax({
						type:"POST",
						url:"proses/_akademik-kprs_hapus_psi.php",
						data: "aksi=tampil",
						success: function(data){
							$("#krs_dihapus").html(data);
						}
					});
				}
				tampildatadua();

				function krshapus_kirim(f){
					$.ajax({
						type:"POST",
						url:"proses/_akademik-kprs_hapus_psi.php",
						data: "aksi=hapus&pengambilan_mk="+f,
						success: function(data){
							alert(data);
							tampildatasatu();
							tampildatadua();
							tampildatatiga();
						}
					});
				}
			</script>
			';
			$smarty->assign('jkueri_mkterambil', $jkueri_mkterambil);

			// ********************
			// *** Respon dosen ***
			// ********************
			$jkueri_respondosen = '
			<script type="text/javascript">
				function tampildatatiga(){
					$.ajax({
						type:"POST",
						url:"proses/_akademik-kprs_dilihat_psi.php",
						data: "aksi=tampil",
						success: function(data){
							$("#krs_ditampil").html(data);
						}
					});
				}
				tampildatatiga();
			</script>
			';
			$smarty->assign('jkueri_respondosen', $jkueri_respondosen);
		}else if($id_prodi==134) { // // KHAS S1 PSIKOLOGI
			// *****************
			// *** INPUT KPRS ***
			// *****************
			$jkueri_penawaranmk = '
			<script type="text/javascript">
				function tampildatasatu(){
					$.ajax({
						type:"POST",
						url:"proses/_akademik-kprs_ditambah_s1_psi.php",
						data: "aksi=tampil",
						success: function(data){
							$("#krs_ditambah").html(data);
						}
					});
				}
				tampildatasatu();

				function krstambah_kirim(f){
					$.ajax({
						type:"POST",
						url:"proses/_akademik-kprs_ditambah_s1_psi.php",
						data: "aksi=input&kelas="+f+"&sid=' . session_id() . '",
						success: function(data){
							alert(data);
							tampildatasatu();
							tampildatadua();
							tampildatatiga();
						}
					});
				}
			</script>
			';
			$smarty->assign('jkueri_penawaranmk', $jkueri_penawaranmk);

			// *****************
			// *** HAPUS KPRS ***
			// *****************
			$jkueri_mkterambil = '
			<script type="text/javascript">
				function tampildatadua(){
					$.ajax({
						type:"POST",
						url:"proses/_akademik-kprs_hapus_psi.php",
						data: "aksi=tampil",
						success: function(data){
							$("#krs_dihapus").html(data);
						}
					});
				}
				tampildatadua();

				function krshapus_kirim(f){
					$.ajax({
						type:"POST",
						url:"proses/_akademik-kprs_hapus_psi.php",
						data: "aksi=hapus&pengambilan_mk="+f,
						success: function(data){
							alert(data);
							tampildatasatu();
							tampildatadua();
							tampildatatiga();
						}
					});
				}
			</script>
			';
			$smarty->assign('jkueri_mkterambil', $jkueri_mkterambil);

			// ********************
			// *** Respon dosen ***
			// ********************
			$jkueri_respondosen = '
			<script type="text/javascript">
				function tampildatatiga(){
					$.ajax({
						type:"POST",
						url:"proses/_akademik-kprs_dilihat_psi.php",
						data: "aksi=tampil",
						success: function(data){
							$("#krs_ditampil").html(data);
						}
					});
				}
				tampildatatiga();
			</script>
			';
			$smarty->assign('jkueri_respondosen', $jkueri_respondosen);
		}
	}


	$smarty->display('akademik-kprs.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>