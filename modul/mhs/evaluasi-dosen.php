<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

?>
<!-- Author : Seto Priyanggoro -->

<!-- File ini berisi proses dari menu kalender akademik -->
<div class="center_title_bar">Dosen</div>
<form name="frm">
    <table width="600" border="none">
		<td>Pilih Dosen </td>
		<td>
			<select>
				<option value="L">Drs. Kartono, M.Kom</option>
				<option value="P">Ir. Dyah Herawatie, M.Si</option>
			</select>
		</td>
	</table>
</form>	
<img src="images/quisoner_dosen.jpg" />