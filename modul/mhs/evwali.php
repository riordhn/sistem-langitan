<?php

include 'config.php';
include 'class/eva.class.php';
include 'function-tampil-informasi.php';

//echo $_SERVER['REMOTE_ADDR'];

$eva = new eva($db, $user->ID_PENGGUNA);

//Semester Pengisian
$id_semester_isi = $eva->GetSemesterAktifEvaluasi();
$id_semester_aktif = $eva->GetSemesterAktif();

$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER='{$id_semester_isi}'");
$isi = $db->FetchAssoc();
$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER='{$id_semester_aktif}'");
$setelah = $db->FetchAssoc();

//Data Mahasiswa
$db->Query("
        SELECT M.*,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI ,PS.ID_FAKULTAS
        FROM MAHASISWA M
        JOIN PENGGUNA P ON M.ID_PENGGUNA=P.ID_PENGGUNA
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
        WHERE M.ID_PENGGUNA='{$user->ID_PENGGUNA}'
    ");
$data_mhs = $db->FetchAssoc();

//Cek Pembayaran
$cek_pembayaran = $eva->CekPembayaran($data_mhs['ID_MHS'], $id_semester_isi);

//Data Dosen Wali

$db->Query("
        SELECT D.ID_DOSEN,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI
        FROM DOSEN_WALI DW
        JOIN DOSEN D ON DW.ID_DOSEN=D.ID_DOSEN
        JOIN PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
        WHERE DW.ID_MHS='{$data_mhs['ID_MHS']}' AND DW.STATUS_DOSEN_WALI=1
    ");
$data_dosen_wali = $db->FetchAssoc();
#echo empty($data_dosen_wali).print_r($data_dosen_wali,true); exit();
if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
    if (isset($_POST)) {
        if (post('mode') == 'simpan') {
            //Simpan Evaluasi Perwalian
            for ($i = 1; $i <= post('jumlah_aspek'); $i++) {
                $eva->SaveEvaluasiWali(3, post('kel_aspek' . $i), post('aspek' . $i), $data_mhs['ID_MHS'], $data_dosen_wali['ID_DOSEN'], post('prodi')
                        , $data_mhs['ID_FAKULTAS'], $id_semester_isi, post('nilai' . $i));
            }
        }
    }

    // Jika Mahasiswa Belum Melakukan Pembayaran
    if ($cek_pembayaran) {
        $smarty->assign('alert', alert_error("Anda mempunyai tagihan pada semester ini , silahkan hubungi Direktorat Keuangan."));
    }
    // Jika Data Dosen Wali Kosong
    else if (empty($data_dosen_wali)) {
        $var = "Mohon Maaf data Dosen Wali anda belum terdaftar di dalam Sistem, silahkan hubungi Bag.Akademik Fakultas/Prodi".$data_mhs['ID_MHS'];
        $error_wali = alert_error($var);
        $error_wali .=alert_error("Jika Anda Sebelumnya sudah melakukan Pengisian Evaluasi Perwalian maka data tidak akan tersimpan.. Terima Kasih");
        $smarty->assign('alert', $error_wali);
    }
    // Jika Sudah Melakukan Pengisian Evaluasi 
    else if ($eva->CekPengisianEvaluasiWali($data_mhs['ID_MHS'], $data_dosen_wali['ID_DOSEN'], $id_semester_isi)) {
        $smarty->assign('alert', alert_success("Anda telah berhasil mengisi Evaluasi Perwalian {$isi['NM_SEMESTER']} {$isi['TAHUN_AJARAN']}, Pengisian Evaluasi Selanjutnya {$setelah['NM_SEMESTER']} {$setelah['TAHUN_AJARAN']}.Terima kasih atas partisipasinya..."));
    } else {
        $status_buka_wali = $db->QuerySingle("SELECT STATUS_BUKA FROM EVALUASI_INSTRUMEN WHERE ID_EVAL_INSTRUMEN=3");
        if ($status_buka_wali == 1) {
            $smarty->assign('data_aspek', $eva->LoadEvaluasiAspek(3));
            $smarty->assign('data_dosen', $data_dosen_wali);
        }else{
            $pengumuman =$db->QuerySingle("SELECT PENGUMUMAN FROM EVALUASI_INSTRUMEN GROUP BY PENGUMUMAN");
            if($pengumuman==''){
                $smarty->assign('alert', alert_error("Mohon Maaf proses pengisian evaluasi sudah tertutup. Terima Kasih"));
            }else{
                $smarty->assign('alert', alert_error($pengumuman));    
            }
        }
    }
    $smarty->display('evwali.tpl');
} else {
    $smarty->display('session-expired.tpl');
}
?>
