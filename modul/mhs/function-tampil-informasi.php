<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

function alert_error($text) {
    return "<div id='alert' class='alert alert-danger my-2' role='alert'>
                {$text}
            </div>";
}

function alert_success($text) {
    return "<div id='alert' class='alert alert-success my-2' role='alert'>
                {$text}
            </div>";
}
?>
