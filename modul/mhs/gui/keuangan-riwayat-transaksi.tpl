<table class="table table-stripped" style="width:70%;">
    <caption style="text-align:left;font-weight:bold">
    INFORMASI TAGIHAN <br/>
    SEMESTER {$data_tagihan_pembayaran[0]['SEMESTER']}
    <hr>
    
    </caption>
    <tr class="">
        <th>NO</th>
        <th>Nama Biaya</th>
        <th>Jumlah Tagihan</th>
        <th>Jumlah Terbayar</th>
        <th>Lunas</th>
    </tr>
        {$total_tagihan=0}
        {$total_terbayar=0}    
        {foreach $data_tagihan_pembayaran as $d}
        <tr class="">
            <td>{$d@index+1}</td>
            <td>{$d['NAMA_BIAYA']}</td>
            <td style="text-align:right">{number_format($d['BESAR_TAGIHAN'])}</td>
            <td style="text-align:right">{number_format($d['BESAR_TERBAYAR'])}</td>
            <td class="center">{if ($d['IS_LUNAS']=='1'&&count($data_tagihan_pembayaran)>0)||($d['BESAR_TAGIHAN']==0&&count($data_tagihan_pembayaran)>0)} <b style=';color:green'>Sudah</b> {else} <b style=';color:red'>Belum</b>{/if}</td>        
        </tr>
        {$total_tagihan=$total_tagihan+$d['BESAR_TAGIHAN']}
        {$total_terbayar=$total_terbayar+$d['BESAR_TERBAYAR']}
        {/foreach}
    <tr class="total" >
        <td colspan="2">TOTAL</td>
        <td style="text-align:right">{number_format($total_tagihan)}</td>
        <td style="text-align:right">{number_format($total_terbayar)}</td>
        <td></td>
    </tr>
</table>
<table class="table table-stripped" style="width:70%;">
    <caption style="text-align:left;font-weight:bold">
    RIWAYAT PEMBAYARAN
    <hr/>
    </caption>
    {foreach $data_riwayat_pembayaran as $d}
    <tr class="">
        <td colspan=5>
        <p style="font-size:1.1em">
            <b style="color:#c42700"><u>PEMBAYARAN KE-{$d@index+1} </u></b> <br/>
            <b>NO.KUITANSI</b> : {$d.NO_KUITANSI} <br/>
            <b>TANGGAL BAYAR</b> : {date("d/m/Y",strtotime($d.TGL_BAYAR))} <br/>
            <b>DIBAYARKAN KE</b> : {$d.NM_BANK} <br/>
            <b>CARA BAYAR</b> : {$d.NAMA_BANK_VIA} <br/>
            <b>TOTAL TERBAYAR</b> : {number_format($d.TOTAL_BAYAR)} <br/>
            <b>KETERANGAN</b> : {$d.KETERANGAN} <br/>
            {if $id_pt==1}
              <span class="btn btn-sm btn-primary" style="padding:4px;cursor:pointer;margin:2px;" onclick="window.open('cetak-kuitansi-bayar.php?cari={$cari}&no_transaksi={$d.NO_TRANSAKSI}&no_kuitansi={$d.NO_KUITANSI}','_blank')">Cetak Bukti Bayar</span>
            {/if}
        </p>
        </td>
    </tr>
    <tr class="">
        <th>NO</th>
        <th>Nama Biaya</th>
        <th>Jumlah Pembayaran</th>
    </tr>
        {$total_bayar=0}    
        {foreach $d.DATA_BIAYA_PEMBAYARAN as $db}
        <tr class="ui-widget-content">
            <td>{$db@index+1}</td>
            <td>{$db['NM_BIAYA']}</td>
            <td style="text-align:right">{number_format($db['BESAR_PEMBAYARAN'])}</td>
        </tr>
        {$total_bayar=$total_bayar+$db['BESAR_PEMBAYARAN']}            
        {/foreach}
        <tr class="total" >
            <td colspan="2">TOTAL</td>
            <td style="text-align:right">{number_format($total_bayar)}</td>
        </tr>
    {foreachelse}
    <tr>
        <td colspan=5><label style="color:red">Tidak ada transaksi pembayaran</label></td>
    </tr>
    {/foreach}
</table>