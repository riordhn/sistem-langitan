{* Yudi Sulistya, 02 Aug 2013 (tablesorter) *}
{literal}
<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#uts").tablesorter(
		{
		sortList: [[3,0],[4,0]], widgets: ['zebra']
		}
	);
}
);
</script>
{/literal}

<!-- Yudi Sulistya, 14-10-2012 -->
<div class="center_title_bar">Jadwal Ujian</div>
{literal}
<script>
	var panels=new Array('panel1','panel2','panel3');
	var tabs=new Array('tab1','tab2','tab3');

	function displayPanel(nval)
	{
		for(i=0;i<panels.length;i++)
		{
			document.getElementById(panels[i]).style.display=(nval-1==i)?'block':'none';
			document.getElementById(tabs[i]).className=(nval-1==i)?'tab_sel':'tab'
		}
	}
</script>
{/literal}

<div id="tabs">
	<div id="tab1" class="tab_sel" align="center"  onClick="javascript: displayPanel('1');">Jadwal UTS</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onClick="javascript: displayPanel('2');">Jadwal UAS</div>
	<div id="tab3" class="tab" style="margin-left:1px;" align="center" onClick="javascript: displayPanel('3');">Jadwal TA</div>
</div>
             
<div class="tab_bdr"></div>

<div class="panel" id="panel1" style="display: block">
<table id="uts" class="tablesorter">
	<thead>
	<tr>
		<th>Kode MA</th>
		<th>Nama Mata Ajar</th>
		<th>Jenis Ujian</th>
		<th>Tanggal Ujian</th>
		<th>Waktu</th>
		<th>Ruang</th>
	</tr>
	</thead>
	<tbody>
	{if $UTS == ''}
	<tr><td colspan="6"><em>UTS belum dijadwalkan oleh akademik fakultas.</em></td></tr>
	{else}
		{$UTS}
	{/if}
	</tbody>
</table>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#uas").tablesorter(
		{
		sortList: [[3,0],[4,0]], widgets: ['zebra']
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel2" style="display: none">
<table id="uas" class="tablesorter">
	<thead>
	<tr>
		<th>Kode MA</th>
		<th>Nama Mata Ajar</th>
		<th>Jenis Ujian</th>
		<th>Tanggal Ujian</th>
		<th>Waktu</th>
		<th>Ruang</th>
	</tr>
	</thead>
	<tbody>
	{if $UAS == ''}
	<tr><td colspan="6"><em>UAS belum dijadwalkan oleh akademik fakultas.</em></td></tr>
	{else}
		{$UAS}
	{/if}
	</tbody>
</table>
</div>

{* Yudi Sulistya, 13-01-2014 *}
<div class="panel" id="panel3" style="display: none">
<table id="ta" class="tablesorter">
	<thead>
	<tr>
		<th>Bentuk Ujian</th>
		<th>Judul TA</th>
		<th>Tanggal Ujian</th>
		<th>Waktu</th>
		<th>Ruang</th>
		<th>Penguji</th>
	</tr>
	</thead>
	<tbody>
	{if $TA == ''}
	<tr><td colspan="6"><em>Belum dijadwalkan oleh akademik fakultas.</em></td></tr>
	{else}
		{$TA}
	{/if}
	</tbody>
</table>
</div>