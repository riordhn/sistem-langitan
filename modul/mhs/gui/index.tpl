<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Mahasiswa - {$nama_pt}</title>
    <meta name="viewport" content="initial-scale=0.1">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap-4.5.2-custom-mhs.min.css" />        
    {if getenv('APP_ENV') == 'production'}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.min.css" integrity="sha256-vpKTO4Ob1M4bZ8RAvZvYMtinMz1XjH0QYdAO2861V9M=" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css" integrity="sha256-R91pD48xW+oHbpJYGn5xR0Q7tMhH4xOrWn1QqMRINtA=" crossorigin="anonymous" />
    {else}
        <!-- Composer Library -->
        <link rel="stylesheet" href="/vendor/components/jqueryui/themes/smoothness/jquery-ui.min.css" />
        <!-- NPM Library -->
        <link rel="stylesheet" href="/node_modules/toastr/build/toastr.min.css">
    {/if}
    <link rel="stylesheet" href="/css/mhs_style.css" />
</head>

<body>
    <div id="main_container" class="container">
        <div id="header" style="background-image: url('../../img/header/mahasiswa-{$nama_singkat}.png')"></div>
        <div id="main_content">
            <div id="menu_tab">
                <div class="left_menu_corner"></div>
                <ul class="menu">
                    {foreach $modul_set as $modul}
                        {if $modul.AKSES == 1}
                            <li><a class="nav1" rel="{$modul.NM_MODUL}" href="#{$modul.NM_MODUL}!{$modul.PAGE}">{$modul.TITLE}</a></li>
                            <li class="divider"></li>
                        {/if}
                        {if $modul@last}
                            <li><a class="disable-ajax" href="../../files/indonesia_hebat.apk">Game Indonesia H.E.B.A.T</a></li>
                            <li class="divider"></li>
                            <li><a class="disable-ajax" href="../../logout.php">Logout</a></li>
                        {/if}
                    {/foreach}
                </ul>
                <div class="right_menu_corner"></div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <nav aria-label="breadcrumb" class="mx-3 mt-2" id="breadcrumbs">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a rel="" class="text-success" href="">Biodata Mahasiswa</a></li>          
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
        <div class="left_content col-md-2" >
            <div class="border_box">
                <div class="card mx-3 mb-1">
                    <img class="card-img-top" src="{$fotomahasiswa}?t=time" alt="Card image cap">
                    <div class="card-body"style="padding:5px">
                        <p class="card-text" >{$namamahasiswa}</p>
                        {if $id_pt == 5}{* UNU Lampung *}
                            <input type="button" class="btn btn-sm btn-primary" name="ganti_photo" value="Ganti Photo" onclick="javascript:popup('{$IMG}', 'name', '600', '400', 'center', 'front')" style="text-align:center">
                        {/if}
                    </div>
                </div>                
                
                <div class="list-group mx-3 mb-2" id="menu"></div>
            </div>
        </div>

        <div class="center_content col-md-10" id="content" style="min-height: 400px">
            <div class="judul_title_bar">
                <span class="current_center_title" id="posisi">Biodata Mahasiswa</span>  
            </div>
        </div>
         <!-- penutup center_content -->
        </div>
        <div class="footer row mx-0">

            <div class="left_footer col-sm-1">
                <img src="/img/mhs/footer_logo.jpg" />
            </div>

            <div class="center_footer col-sm-8">Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU</div>

            <div class="right_footer col-sm-3 text-right">
                <a href="{$base_url}" class="disable-ajax">home</a>
                <a href="{$base_url}file/manual_mahasiswa.pdf" class="disable-ajax">manual</a>
            </div>   

        </div>  
     </div>

    <!-- Script -->
    {if getenv('APP_ENV') == 'production'}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.1/jquery-migrate.min.js" integrity="sha256-SOuLUArmo4YXtXONKz+uxIGSKneCJG4x0nVcA0pFzV0=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
    {else}
        <!-- NPM Library -->
        <script src="/node_modules/jquery/dist/jquery.min.js"></script>
        <script src="/node_modules/jquery-migrate/dist/jquery-migrate.min.js"></script>
        <script src="/node_modules/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="/node_modules/toastr/build/toastr.min.js"></script>
        <!-- Composer Library -->
        <script src="/vendor/components/jqueryui/jquery-ui.min.js"></script>
    {/if}
    <script src="/js/jquery-history.js" type="text/javascript" ></script>
    <script type="text/javascript">
        var defaultRel = 'biodata';
        var defaultPage = 'biodata-data.php';
    </script>
    <script src="/js/cybercampus.ajax-1.0.js" type="text/javascript" ></script>
    <!-- Load Notification -->
    <script src="/js/mhs_notification.js"></script>

{literal}
    <script language="javascript">

        var popupWindow = null;
        function popup(mypage, myname, w, h, pos, infocus) {
            if (pos == "random")
            {
                LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
                TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
            }
            else
            {
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
            }
            settings = "width=" + w + ",height=" + h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
            popupWindow = window.open("", myname, settings);
            if (infocus == "front") {
                popupWindow.focus();
                popupWindow.location = mypage;
            }
            if (infocus == "back") {
                popupWindow.blur();
                popupWindow.location = mypage;
                popupWindow.blur();
            }
        }

        function new_window(mypage, myname, w, h, pos, infocus) {
            if (pos == "random")
            {
                LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
                TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
            }
            else
            {
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
            }
            settings = "width=" + w + ",height=" + h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
            popupWindow = window.open("", myname, settings);
            if (infocus == "front") {
                popupWindow.focus();
                popupWindow.location = mypage;
            }
            if (infocus == "back") {
                popupWindow.blur();
                popupWindow.location = mypage;
                popupWindow.blur();
            }
        }

        
    </script>
{/literal}

</body>
</html>