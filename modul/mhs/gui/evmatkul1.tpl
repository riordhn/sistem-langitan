<!-- Author : Seto Priyanggoro -->

<!-- File ini berisi proses dari menu katalog buku -->

<div class="center_title_bar">Evaluasi Perkuliahan Mahasiswa</div>
<form action="evkul.php" method="post">
<table>
<tr>
{if $statusprak eq 0}
<td><b><center>EVALUASI KINERJA DOSEN DALAM PERKULIAHAN</center></b><br><p align=justify>Evaluasi kinerja dosen dalam perkuliahan ini ditujukan untuk memastikan bahwa kinerja dosen dalam pembelajaran tiap semester telah dilaksanakan dengan baik sesuai dengan tugas pokok dan fungsinya. Evaluasi ini juga ditujukan untuk mengidentifikasi praktek baik (good practices) dalam rangka meningkatkan kualitas proses pembelajaran.
Mengingat pentingnya Informasi ini untuk meningkatkan kualitas proses pembelajaran, mohon agar diisi dengan sebenar-benarnya.</p></td>
{elseif $statusprak eq 1}
<td><b><center>EVALUASI KINERJA DOSEN DALAM PRAKTIKUM</center></b><br><p align=justify>Evaluasi kinerja dosen/asisten dosen dalam praktikum ini ditujukan untuk memastikan bahwa kinerja dosen/asisten dosen dalam praktikum tiap semester telah dilaksanakan dengan baik sesuai dengan tugas pokok dan fungsinya. Evaluasi ini juga ditujukan untuk mengidentifikasi praktek baik (good practices) dalam rangka meningkatkan kualitas proses praktikum.
Mengingat pentingnya Informasi ini untuk meningkatkan kualitas proses praktikum, mohon agar diisi dengan sebenar-benarnya.</p></td>
{/if}
</tr>
<tr><td><table><tr><td>Nama Dosen</td><td>:</td><td>{$nmdosen}</td></tr><tr><td>Nama Mata Ajar</td><td>:</td><td>{$nmmk}</td></tr></table></td></tr>
<tr><td><table border=1><tr><td><center>No</center></td><td><center>PERNYATAAN</center></td><td>&nbsp;</td><td color=red><center>Sangat Tidak Puas</center></td><td><center>Tidak Puas</center></td><td><center>Puas</center></td><td><center>Sangat Puas</center></td><td><center>Tidak Berpendapat</center></td></tr>
{foreach $evmatkul1 as $e}
{if $statusprak eq 0}

      {if $e@index eq 0}
          <tr><td colspan=8 bgcolor=#BFBFBF>a. Dosen sebagai Perencanaan perkuliahan.</td></tr>
      {/if}
      {if $e@index eq 4}
          <tr><td colspan=8 bgcolor=#BFBFBF>b. Dosen sebagai Pelaksana perkuliahan.</td></tr>
      {/if}
      {if $e@index eq 15}
          <tr><td colspan=8 bgcolor=#BFBFBF>c. Dosen sebagai Evaluator Perkuliahan.</td></tr>
      {/if}
      {if $e@index le 20}
          <tr {if $e@index is div by 2}bgcolor="#EEEEEE"{else}bgcolor="#AAAAAA"{/if}>
		<td>{$e@index+1}</td>
		<td>{$e.ASPEK}</td>
		<td>&nbsp;</td>
		<td color=red><center><input type=radio name=nilai[{$e@index}] value="1"></center></td>
		<td><center><input type=radio name=nilai[{$e@index}] value="2"></center></td>
		<td><center><input type=radio name=nilai[{$e@index}] value="3"></center></td>
		<td><center><input type=radio name=nilai[{$e@index}] value="4"></center></td>		
		<td><center><input type=radio name=nilai[{$e@index}] value="5"></center></td>
	  </tr>
      {/if}
   {if $e@index eq 21}
   <tr><td colspan=8>KOMENTAR TERTULIS</td></tr>
   <tr><td colspan=8>1.{$e.ASPEK}</td></tr>
   <tr><td colspan=8><textarea name=nilai[{$e@index}] cols=75 rows=5></textarea></td></tr>
   {/if}
   {if $e@index eq 22}
   <tr><td colspan=8>2.{$e.ASPEK}</td></tr>
   <tr><td colspan=8><textarea name=nilai[{$e@index}] cols=75 rows=5></textarea></td></tr>
   {/if}
{/if}
{if $statusprak eq 1}

      {if $e@index eq 0}
          <tr><td colspan=8 bgcolor=#BFBFBF>a. Dosen sebagai Perencanaan praktikum.</td></tr>
      {/if}
      {if $e@index eq 4}
          <tr><td colspan=8 bgcolor=#BFBFBF>b. Dosen sebagai Pelaksana praktikum.</td></tr>
      {/if}
      {if $e@index eq 11}
          <tr><td colspan=8 bgcolor=#BFBFBF>c. Dosen sebagai Evaluator Praktikum.</td></tr>
      {/if}
      {if $e@index eq 14}
          <tr {if $e@index is div by 2}bgcolor="#EEEEEE"{else}bgcolor="#AAAAAA"{/if}>
		<td>{$e@index+1}</td>
		<td>{$e.ASPEK}</td>
		<td>&nbsp;</td>
		<td color=red><center><input type=radio name=nilai[{$e@index}] value="1"></center></td>
		<td><center><input type=radio name=nilai[{$e@index}] value="2"></center></td>
		<td><center><input type=radio name=nilai[{$e@index}] value="3"></center></td>
		<td><center><input type=radio name=nilai[{$e@index}] value="4"></center></td>		
		<td><center><input type=radio name=nilai[{$e@index}] value="5"></center></td>
	  </tr>
      {/if}
   {if $e@index eq 15}
   <tr><td colspan=8>KOMENTAR TERTULIS</td></tr>
   <tr><td colspan=8>1.{$e.ASPEK}</td></tr>
   <tr><td colspan=8><textarea name=nilai[{$e@index}] cols=75 rows=5></textarea></td></tr>
   {/if}
   {if $e@index eq 16}
   <tr><td colspan=8>2.{$e.ASPEK}</td></tr>
   <tr><td colspan=8><textarea name=nilai[{$e@index}] cols=75 rows=5></textarea></td></tr>
   {/if}
{/if}
{/foreach}

<tr><td colspan=8><font color=red>*Tidak ada pendapat dipilih jika anda tidak dapat menilai atau tidak paham dengan item evaluasi yang dimaksud.</font><br>Terima Kasih Atas Kesediaan Saudara Mengisi Kuesioner Dengan Sebenar-Benarnya.</td></tr>
</table> 

<input name="idmatkul" id="idmatkul" type="hidden" value={$idmatkul}>
<input name="mode" id="mode" type="hidden" value="save_evkul1">
<input type="submit" name="simpan" value="simpan"/>
</form>
