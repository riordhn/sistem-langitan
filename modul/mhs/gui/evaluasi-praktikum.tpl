<!-- Author : Seto Priyanggoro -->

<!-- File ini berisi proses dari menu kalender akademik -->
<div class="center_title_bar">Praktikum</div>
<form name="frm">
    <table width="600" border="none">
		<td>Pilih Dosen </td>
		<td>
			<select>
				<option value="L">Drs. Kartono, M.Kom</option>
				<option value="P">Ir. Dyah Herawatie, M.Si</option>
			</select>
		</td>
	</table>
</form>	
<p align="center"><strong>EVALUASI KINERJA DOSEN</strong></p>
<p align="center"><strong>DALAM PRAKTIKUM</strong></p>
<form name="f2">
<table width="700" border="1">
  <tr>
    <td width="5%" rowspan="2" align="center" valign="middle" bgcolor="#33CC33"><strong>No</strong></td>
    <td width="35%" rowspan="2" align="center" valign="middle" bgcolor="#33CC33"><strong>Aspek yang dinilai</strong></td>
    <td colspan="5" align="center" bgcolor="#33CC33"><strong>Skala</strong></td>
  </tr>
  <tr>
    <td width="12%" align="center" bgcolor="#33CC33"><strong>1</strong></td>
    <td width="12%" align="center" bgcolor="#33CC33"><strong>2</strong></td>
    <td width="12%" align="center" bgcolor="#33CC33"><strong>3</strong></td>
    <td width="12%" align="center" bgcolor="#33CC33"><strong>4</strong></td>
    <td width="12%" align="center" bgcolor="#33CC33"><strong>0*</strong></td>
  </tr>
  <tr>
    <td colspan="7" bgcolor="#339933"><strong>Dosen Sebagai Perencanaan Praktikum</strong></td>
  </tr>
  <tr>
    <td>1.</td>
    <td>penyampaian tujuan dan manfaat praktikum</td>
    <td><input type="radio" name="radio" id="radio" value="radio" />
      Tidak jelaas</td>
    <td><input type="radio" name="radio" id="radio2" value="radio" />
      Kurang Jelas</td>
    <td><input type="radio" name="radio" id="radio3" value="radio" />
      Cukup Jelas</td>
    <td><input type="radio" name="radio" id="radio4" value="radio" />
      Sangat jelas</td>
    <td><input type="radio" name="radio" id="radio5" value="radio" />
      Tidak ada pendapat</td>
  </tr>
  <tr>
    <td>2.</td>
    <td>Penyampaian kontrak  pada awal praktikum</td>
    <td><input type="radio" name="radio" id="radio" value="radio" />
      Tidak ada</td>
    <td><input type="radio" name="radio" id="radio2" value="radio" />
      Ada hanya jadual</td>
    <td><input type="radio" name="radio" id="radio3" value="radio" />
    Ada jadual dan pustaka</td>
    <td><input type="radio" name="radio" id="radio4" value="radio" />
      Ada lengkap</td>
    <td><input type="radio" name="radio" id="radio5" value="radio" />
      Tidak ada pendapat</td>
    </tr>
  <tr>
    <td colspan="7" bgcolor="#339933"><strong>Dosen Sebagai Pelaksana Praktikum</strong></td>
  </tr>
  <tr>
    <td>3.</td>
    <td>Memulai dan Mengakhiri praktikum tepat waktu sesuai jadual yang ditentukan</td>
    <td><input type="radio" name="radio" id="radio" value="radio" />
      Tidak Pernah</td>
    <td><input type="radio" name="radio" id="radio2" value="radio" />
      jarang</td>
    <td><input type="radio" name="radio" id="radio3" value="radio" />
      Sering</td>
    <td><input type="radio" name="radio" id="radio4" value="radio" />
      Selalu</td>
    <td><input type="radio" name="radio" id="radio5" value="radio" />
      Tidak ada pendapat</td>
    </tr>
  <tr>
    <td>4.</td>
    <td>Melakukan pretest sebelum praktikum</td>
    <td><input type="radio" name="radio" id="radio" value="radio" />
      Tidak Pernah</td>
    <td><input type="radio" name="radio" id="radio2" value="radio" />
      jarang</td>
    <td><input type="radio" name="radio" id="radio3" value="radio" />
      Sering</td>
    <td><input type="radio" name="radio" id="radio4" value="radio" />
      Selalu</td>
    <td><input type="radio" name="radio" id="radio5" value="radio" />
      Tidak ada pendapat</td>
    </tr>
  <tr>
    <td colspan="7" bgcolor="#339933"><strong>Dosen Sebagai Evaluator Praktikum</strong></td>
  </tr>
  <tr>
    <td>5.</td>
    <td>Menyampaikan tata cara penilaian dalam praktikum.</td>
    <td><input type="radio" name="radio" id="radio" value="radio" />
      Tidak Pernah</td>
    <td><input type="radio" name="radio" id="radio2" value="radio" />
      pernah, pada akhir perkuliahan</td>
    <td><input type="radio" name="radio" id="radio3" value="radio" />
      pernah, di tengah masa perkuliahan</td>
    <td><input type="radio" name="radio" id="radio4" value="radio" />
      pernah, pada saat kontrak perkuliahan</td>
    <td><input type="radio" name="radio" id="radio5" value="radio" />
      Tidak ada pendapat</td>
    </tr>
  <tr>
    <td>6.</td>
    <td>Dosen menginformasikan kisi-kisi tes tulis dan praktek yang dilakukan.</td>
    <td><input type="radio" name="radio" id="radio" value="radio" />
      Tidak Pernah</td>
    <td><input type="radio" name="radio" id="radio2" value="radio" />
      jarang</td>
    <td><input type="radio" name="radio" id="radio3" value="radio" />
      Sering</td>
    <td><input type="radio" name="radio" id="radio4" value="radio" />
      Selalu</td>
    <td><input type="radio" name="radio" id="radio5" value="radio" />
      Tidak ada pendapat</td>
    </tr>
</table>
</form>
<p><strong>* Tidak ada pendapat dipilih jika anda tidak dapat menilai atau tidak paham item evaluasi yang dimaksud.</strong></p>
<p>
  <input type="button" value="Simpan" />
</p>
