<script type="text/javascript">
    function tampilkhs(n) {
        $.ajax({
            type: "POST",
            url: "/modul/mhs/proses/akademik-history-tampil.php",
            data: "aksi=tampil&semes=" + n,
            success: function (data) {
                $("#tampil_khs").html(data);
            }
        });
    }
</script>

<div class="center_title_bar">Kartu Hasil Studi</div>

<form name="form8" method="post" action="">
    <table>
        <tr>
            <td>Tahun Akademik</td>
            <td>:</td>
            <td>
                <select name="thn_akademik" onchange="tampilkhs(this.value)">
                    <option value="0">---------</option>
                    {foreach $semester_set as $semester}
                        <option value="{$semester.ID_SEMESTER}">{$semester.NM_SEMESTER} {$semester.TAHUN_AJARAN}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>

<div id="tampil_khs"></div>

<p>Keterangan :<br/>
    *K : Kosong.<br/>
    *BT : Belum Tampil. Ada Nilai tetapi belum ditampilkan dosen</p>

<iframe src="proses/_akademik-khs-tampil-ips.php" style="width: 629px; height: 420px" frameborder="0"></iframe>