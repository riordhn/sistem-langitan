<!-- Author : Seto Priyanggoro -->
<div class="center_title_bar">Histori Nilai</div>
<!-- File ini berisi tentang proses transkrip -->
{if empty($message_evaluasi)}
{$isitranskrip}
{else}
<h3>Untuk bisa melihat menu <i>history</i> nilai, harap mengisi evaluasi terlebih dahulu</h3>
{$message_evaluasi}
{/if}
