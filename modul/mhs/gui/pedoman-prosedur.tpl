<div class="center_title_bar">Daftar Pedoman Prosedur</div>
{if $tutup!=''}
    {$tutup}
{else}
    <table style="width: 100%">
        {$index_pedoman=1}
        {foreach $data_dokumen as $d}
            {if $d.ID_MM_JENIS_PEDOMAN==2}
                <tr style="background-color: lightgreen">
                    <td colspan="4" class="center" style="padding: 10px;text-align: center"><b style="font-size: 1.4em">{$d.NAMA_PEDOMAN}</b></td>
                </tr>
                {foreach $d.LINGKUP as $l}
                    {if ($l.ID_MM_GROUP_DOKUMEN==3 or $l.ID_MM_GROUP_DOKUMEN==4 or $l.ID_MM_GROUP_DOKUMEN==13)}
                        <tr style="background-color: antiquewhite">
                            <td colspan="4" style="padding: 10px;text-align: center"><b style="font-size: 1.3em">{$l.NAMA_GROUP_DOKUMEN} ({$l.KODE_GROUP_DOKUMEN|upper})</b></td>
                        </tr>
                        <tr>
                            <th>NO</th>
                            <th>DOKUMEN</th>
                            <th  style="width: 140px">KODE</th>
                            <th>FILE</th>
                        </tr>
                        {foreach $l.DOKUMEN as $d}
                            {if $d.HIDDEN!=1}
                                {$convert_file=$d.NAMA_FILE}
                                <tr>
                                    <td  style="width: 20px">{$d@index+1}</td>
                                    <td>{$d.NAMA_DOKUMEN}</td>
                                    <td>{$d.KODE_DOKUMEN|upper}</td>
                                    <td class="center">
                                        {if $d.HIDDEN==1}
                                            <span style="color: white">Telah Di Hapus</span>
                                        {else}
                                            {if $d.NAMA_FILE!=''}
                                                <a class="disable-ajax" target="_blank" href="{$base_url}modul/manajemen-mutu/dokumen-view.php?l={$l.NAMA_GROUP_DOKUMEN}&t={$d.NAMA_DOKUMEN}&key={base64_encode('nambisembilu_cybercampus')}&file={base64_encode($convert_file)}">{$d.KODE_DOKUMEN|upper}</a>
                                            {elseif $d.JUMLAH_VERIFIKASI>0}
                                                <span style="color: red">Belum Di Publish</span>
                                            {elseif $d.JUMLAH_FILE==0}
                                                <span style="color: red">File Kosong</span>
                                            {elseif $d.JUMLAH_FILE>0}
                                                <span style="color: red">Belum Di verifikasi</span>
                                            {/if}
                                        {/if}
                                    </td>
                                </tr>
                            {/if}
                        {foreachelse}
                            <tr>
                                <td colspan="4" class="kosong">Data Masih Kosong</td>
                            </tr>
                        {/foreach}
                    {/if}
                {/foreach}
                {$index_pedoman=$index_pedoman+1}
            {/if}
        {/foreach}
    </table>
{/if}