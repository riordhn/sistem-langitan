<script>
	$(document).ready(function(){
		{for $i=0 to $jml_kelompok-1}
		$( "#btn-gabung{$i}" ).button({
			text: true
		});
		
		$("#btn-gabung{$i}").click(function() {
			var val = $('#id_kelompok{$i}').val();
			$('#kelompok').val(val);
			$('#formKelompokKkn').submit();
		});
		
		$("#male{$i}").load("akademik-kkn-report-gender.php?gender=1&kelompok={$id_kelompok[$i]}");
		$("#female{$i}").load("akademik-kkn-report-gender.php?gender=2&kelompok={$id_kelompok[$i]}");
		
		{/for}
		
		$('#formKelompokKkn').submit(function() {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
					
					$('#response').html(data);
					$('#status-kkn').load('akademik-kkn.php?view=status');
					$('#report').load('akademik-kkn.php?view=report&cat={$category}');
					
					
				}
			})
			return false;
		});
		
	});

</script>

<form name="formKelompokKkn" id="formKelompokKkn" method="post" action="akademik-kkn.php?mode=update">
	<input type="hidden"  name="kelompok" id="kelompok" />
	<table class="ui-widget" style="width:100%;">
		<tr class="ui-widget-header">
			<td>#</td>
			<td>Kelurahan</td>
			<td>Kelompok</td>
			<td>Jenis</td>
			
			<!-- <td>Quota</td> -->
			<td>Laki - Laki</td>
			<td>Perempuan</td>
			<!-- 
			<td>Terisi</td>
			<td>Tersisa</td> -->
		</tr>
		{for $i = 0 to $jml_kelompok-1}
		<tr class="ui-widget-content">
			<td>{$i+1}</td>
			<td>{$kelurahan[$i]}</td>
			<td>
				<a href="akademik-kkn.php?view=detail&kelompok={$id_kelompok[$i]}" >{$kelompok[$i]}</a>
				<input type="hidden" value="{$id_kelompok[$i]}" name="id_kelompok{$i}" id="id_kelompok{$i}" />
			</td>
			<td>
				{if $jenis[$i]=='1'}
					REGULER
				{else}
					TEMATIK
				{/if}
			</td>
			<!-- <td align="center">{$quota[$i]}</td> -->
			<td align="center">
				<div id="male{$i}">
					<img src="../../../../../api/images/gif/facebook-loading.gif" />
				</div>
			</td>
			<td align="center">
				<div id="female{$i}">
					<img src="../../../../../api/images/gif/facebook-loading.gif" />
				</div>
			</td>
			<!-- 
			<td align="center">
				{if $peserta[$i]!=""}
					{$peserta[$i]}
				{else}
					0
				{/if}
			</td>
			
			<td align="center">
				{if $sisa[$i]!=""}
					{$sisa[$i]}
				{else}
					20
				{/if}
			</td>
			<td align="center"><div id="btn-gabung{$i}" style="height:25px;font-size:11px;margin-top:10px;margin-bottom:10px;">Pilih</div></td> -->
		</tr>
		{/for}
	</table>
</form>