<div class="center_title_bar">Pemilihan Kelompok KKN</div>

<br />
<b>DETAIL PESERTA KKN BERDASARKAN KELOMPOK</b>
<div style="border-color:#CCC;border-bottom-width:1px;border-bottom-style:solid;">
	&nbsp;
</div>
<div style="width:660px;border-color:#CCC;border-bottom-width:1px;border-bottom-style:solid; margin-bottom:20px; margin-top:20px;">
	
	<div style="float:left; width:600px;">
		<div style="width:600px;border-color:#CCC;border-left-width:1px;border-left-style:dotted;padding-left:10px;">
			<div style="width:600px;float:left;">
				<div id="mahasiswa-area">
					<div id="get-provinsi-mhs" style="border-color:#CCC;border-bottom-width:1px;border-bottom-style:dotted; padding:5px;">
						<b>Provinsi</b> : <span style="color:blue;">{$provinsi}</span>
					</div>
					<div id="get-kabupaten-mhs" style="border-color:#CCC;border-bottom-width:1px;border-bottom-style:dotted; padding:5px;">
						<b>Kabupaten / Kota :</b> <span style="color:blue;">{$kota}</span>
					</div>
					<div id="get-kecamatan-mhs" style="border-color:#CCC;border-bottom-width:1px;border-bottom-style:dotted; padding:5px;">
						<b>Kecamatan :</b> <span style="color:blue;">{$kecamatan}</span>
					</div>
					<div id="get-kelurahan-mhs" style="border-color:#CCC;border-bottom-width:1px;border-bottom-style:dotted; padding:5px;">
						<b>Kelurahan / Desa :</b> <span style="color:blue;">{$kelurahan}</span>
					</div>
					<div id="get-korkab-mhs" style="border-color:#CCC;border-bottom-width:1px;border-bottom-style:dotted; padding:5px;">
						<b>Koordinator Kabupaten(Korkab) :</b> <span style="color:blue;">{$korkab}</span>
					</div>
					<div id="get-korbing-mhs" style="border-color:#CCC;border-bottom-width:1px;border-bottom-style:dotted; padding:5px;">
						<b>Koordinator Pembimbing(Korbing) :</b> <span style="color:blue;">{$korbing}</span>
					</div>
					<div id="get-dpl-mhs" style="border-color:#CCC;border-bottom-width:1px;border-bottom-style:dotted; padding:5px;">
						<b>Dosen Pembimbing Lapangan(DPL) :</b> <span style="color:blue;">{$dpl}</span>
					</div>
					<div id="get-kelompok-mhs" style="border-color:#CCC;border-bottom-width:1px;border-bottom-style:dotted; padding:5px;">
						<b>Kelompok KKN :</b> <span style="color:blue;">{$kelompok}</span>
					</div>
				</div>
			</div>
			<div style="clear:both;">
				&nbsp;
			</div>
		</div>
	</div>
			<div style="clear:both;">
				&nbsp;
			</div>
</div>


<table class="ui-widget" style="width:100%;">
	<tr class="ui-widget-header">
		<td>#</td>
		<td>FOTO</td>
		<td>NAMA</td>
		<td>PRODI</td>
	</tr>
	
	{for $i=0 to $jml_data-1}
	<tr>
		<td class="ui-widget-content" width="20px">{$i+1}</td>
		<td class="ui-widget-content" width="40px"><img src="{$foto[$i]}" style="width:40px; height:50px;" /></td>
		<td class="ui-widget-content">{$nama[$i]}<br />{$username[$i]}</td>
		<td class="ui-widget-content">{$prodi[$i]}</td>
	</tr>
	{/for}
	
</table>