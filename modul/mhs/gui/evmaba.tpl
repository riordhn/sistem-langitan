<div class="center_title_bar">Evaluasi Mahasiswa Baru/Pengalaman Belajar Tahun Pertama</div>
{if isset($data_aspek)}
    <form method="post" id="eva" action="evmaba.php">
        <table class="ui-widget" style="width: 100%">
            <tr class="ui-widget-header">
                <th colspan="6" style="border-bottom:1px solid #eaeafa ;">
                    <font style="font-size: 1.5em">SURVEY MAHASISWA BARU {$nama_pt}</font>
                    <br>
                    Umpan balik yang Saudara berikan melalui kuesioner ini akan sangat membantu {$nama_pt} untuk meningkatkan kualitas belajar 
                    dan peningkatan keterampilan mahasiswa. Kuesioner ini juga untuk mengetahui bahwa kinerja insitusi telah sesuai dengan harapan 
                    Saudara sebelum menjadi mahasiswa. Hasil survey ini digunakan untuk mengidentifikasi praktik baik dan memberikan rekomendasi konstruktif 
                    untuk perbaikan kinerja dalam penyelenggaraan program pendidikan tahun pertama Mahasiswa Jenjang S1. 
                    Cara mengisi kuesioner: berikanlah respon berdasarkan pengalaman pribadi Saudara selama di {$nama_pt} dengan cara memilih 1 (satu) 
                    respon terhadap setiap pernyataan yang disediakan. Selamat Mengisi dan Terima Kasih atas kontribusi Saudara dalam peningkatan mutu {$nama_pt}
                </th>
            </tr>
            {$jumlah_aspek=0}
            {foreach $data_aspek as $dk}
                <tr class="ui-widget-header">
                    <th colspan="6" style="font-size: 1.1em">{$dk.NAMA_KELOMPOK}</th>
                </tr>
                {foreach $dk.DATA_ASPEK as $da}
                    <tr class="ui-widget-content">
                        {$jumlah_aspek=$jumlah_aspek+1}
                        <td style="padding: 5px">
                            {$da.EVALUASI_ASPEK}
                            <br/>
                            <label class="error" for="nilai{$jumlah_aspek}" style="font-size:0.8em ;display: none">Harus Diisi</label>
                        </td>
                        {if $da.TIPE_ASPEK==1}
                            {foreach $da[0] as $dn}
                                <td align="center" style="font-size: 0.8em">
                                    {$dn.KET_NILAI_ASPEK}<br/>
                                    <input type="radio" class="required" name="nilai{$jumlah_aspek}" value="{$dn.NILAI_ASPEK}"/>       
                                </td>                                
                            {/foreach}
                        {else}
                            <td colspan="5">
                                <textarea class="required" style="resize: none;width: 90%" maxlength="120" name="nilai{$jumlah_aspek}"></textarea>
                            </td>
                        {/if}
                    <input type="hidden" name="aspek{$jumlah_aspek}" value="{$da.ID_EVAL_ASPEK}"/>
                    <input type="hidden" name="kel_aspek{$jumlah_aspek}" value="{$da.ID_EVAL_KELOMPOK_ASPEK}"/>
                    </tr>
                {/foreach}
            {/foreach}
            <tr>
                <td colspan="6" style="text-align: center">
                    <span style="color: red;text-align: left">*  Tidak ada pendapat dipilih jika anda tidak dapat menilai atau tidak paham dengan item evaluasi yang dimaksud.</span><br/>
                    <span style="font-size: 1.5em;font-family: Trebuchet MS">Terima Kasih Atas Kesediaan Saudara Mengisi Kuesioner Dengan Sebenar-Benarnya.</span>
                </td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <input type="hidden" name="jumlah_aspek" value="{$jumlah_aspek}" />
                    <input type="hidden" name="mode" value="simpan"/>
                    <input type="submit" class="ui-widget-content ui-state-hover ui-corner-all" style="padding: 8px;" value="Simpan"/>
                </td>
            </tr>
        </table>
    </form>
{else}
    {$alert}
{/if}
{literal}
    <script>
        $('#eva').validate();
    </script>
{/literal}
