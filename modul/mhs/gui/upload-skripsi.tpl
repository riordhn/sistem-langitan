{if $smarty.get.mode==''}
    <div class="center_title_bar">Upload File Tugas Akhir</div>
    {if $cek_wisuda==''}
        {if $status_isi!=''}
            {$status_isi}
        {else}
            {$info_1}
            {$info_2}
            <iframe frameborder="0" marginwidth="5" marginheight="10" width="100%" height="800px" src="upload-skripsi.php?mode=frame"></iframe>
        {/if}
    {else}
        {$cek_wisuda}
    {/if}
{else if $smarty.get.mode=='frame'}
    <link rel="stylesheet" href="../../css/jquery-ui-custom-mhs.css" type="text/css">
    <script src="../../js/?f=jquery-1.5.1.js"></script>
    <script src="../../js/cupertino/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script src="../../js/jquery.validate.js"></script>
    <style>
        table {
            border-width: 1px;
            border-style: solid;
            border-color: #999999;
            border-collapse: collapse;
            margin: 10px 0px;
            font-family:Lucida Grande,Lucida Sans,Arial,sans-serif;
        }
        th{
            border: 1px solid #D49768;
            background: #CB842E url(images-mhs/ui-bg_glass_25_cb842e_1x400.png) 50% 50% repeat-x;
            color: white;
            font-weight: bold;
            font-size:14px;
            padding:8px;
            text-transform:uppercase;
        }
        th h2{
            font-size:13px;
            padding:5px;
            margin:0px;
        }
        .ui-widget-header th{
            font-size:13px;
        }
        td{
            padding: 8px;
            font-size: 12px;
            vertical-align: top;
            border-width: 1px;
            border-style: solid;
            border-color: #969BA5;
            border-collapse: collapse;
        }
        textarea.error, input.error, select.error { border: 1px solid red; width: auto; background-color:#ffe9e9;}
        label.error {
            color: red;
            font-size:11px;
            display: block;
        }
    </style>
    <div style="font-size: 0.8em;">{$error_upload}</div>
    <form action="upload-skripsi.php?{$smarty.server.QUERY_STRING}" method="post" enctype="multipart/form-data">
        <table class="ui-widget-content" style="width: 60%">
            <tr class="ui-widget-header">
                <th>NO</th>
                <th>NAMA BAGIAN</th>
                <th>FILE</th>
            </tr>
            {foreach $data_bagian as $bag}
                <tr>
                    <td>{$bag@index+1}</td>
                    <td>{$bag.ISI}</td>
                    <td>
                        {if $bag.SUDAH>0}
                            <span style="color: green">File Ini Sudah Berhasil Di Upload</span>
                        {else}
                            <input type="file" class="required" name="file{$bag@index+1}"/>
                            <input type="hidden" name="id_bag{$bag@index+1}" value="{$bag.ID_UPLOAD_TUGAS_AKHIR}"/>
                            <input type="hidden" name="nama{$bag@index+1}" value="{$bag.ISI|lower|replace:' ':'_'}"/>
                            <input type="hidden" name="des{$bag@index+1}" value="{$bag.ISI}"/>
                        {/if}
                    </td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="3" style="text-align: center">
                    <input type="hidden" name="mode" value="upload"/>
                    <input type="hidden" name="jumlah_file" value="{count($data_bagian)}"/>
                    <input type="submit" value="Upload" class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer"/>
                </td>
            </tr>
        </table>
    </form>
{/if}
{literal}
    <script>
        $('form').validate();
    </script>
{/literal}