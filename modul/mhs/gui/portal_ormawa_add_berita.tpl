{literal}
<script>
	
	$(document).ready(function(){
		
		$( "#btn-save" ).button({
			text: true
		});
		
		$( "#btn-cancel" ).button({
			text: true
		});
		
		$('#btn-save').click(function(){	
			var ed = tinyMCE.get('content');
			var isi = ed.getContent();
			$("#get_isi").val(isi);
			$('#frmOrmawaInsert').submit();
		})
		
		$('#btn-cancel').click(function(){	
			$('#display').load('portal_ormawa_display.php');
		})
		
		$('#frmOrmawaInsert').submit(function() {
					// Get the HTML contents of the currently active editor
					//console.debug(tinyMCE.activeEditor.getContent());

					// Get the raw contents of the currently active editor
					//tinyMCE.activeEditor.getContent({format : 'raw'});

					// Get content of a specific editor:
					//tinyMCE.get('content id').getContent();
					var judul = document.getElementById('judul').value;
					//var content =  tinyMCE.get('content').getContent();
					var content =  document.getElementById('get_isi').value;
					var dataString = 'judul='+ judul + '&get_isi=' + content;
					$.ajax({
						type: 'GET',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						//data : dataString,
						success: function(data) {	
							//$('#result').html(data);
							$('#display').load('portal_ormawa_display.php');
							
						}
					})
					return false;
		});
			
	});
</script>
{/literal}
<script type="text/javascript">
	tinyMCE.init({
			mode : "textareas",
			theme : "advanced",
			editor_selector : "mceEditor",
			editor_deselector : "mceNoEditor",
			theme_advanced_buttons1 : "mylistbox,mysplitbutton,bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,undo,redo,link,unlink",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom"
	});
</script>

<form method="GET" action="portal_ormawa_insert.php" id="frmOrmawaInsert">
		<input type="hidden" id="get_isi" name="get_isi">
		<input type="text" value="Judul" name="judul" id="judul" style="padding:2px; color:#AEAEAE;"/><div style="float:right;height:25px;font-size:11px;width:80px;" id="btn-cancel" >Batal</div><div style="float:right;margin-right:10px;height:25px;width:80px;font-size:11px;" id="btn-save" >Simpan</div><br /><br />
		<TEXTAREA id="content"  class="mceEditor" style="width:100%; min-height:700px;" NAME="content"></TEXTAREA>
</form>