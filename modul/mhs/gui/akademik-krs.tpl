<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#myTable").tablesorter({
            sortList: [[1, 0], [3, 0]], widgets: ["zebra"]
        });
    });
</script>

<script type="text/javascript">
    var panels = new Array('panel1', 'panel2', 'panel3', 'panel4', 'panel5');
    var tabs = new Array('tab1', 'tab2', 'tab3', 'tab4', 'tab5');

    function displayPanel(nval) {
        for (i = 0; i < panels.length; i++) {
            document.getElementById(panels[i]).style.display = (nval - 1 == i) ? 'block' : 'none';
            document.getElementById(tabs[i]).className = (nval - 1 == i) ? 'tab_sel' : 'tab'
        }
    }
</script>

<div class="center_title_bar">KRS</div>

<div id="tabs">
    <!-- khusus UMAHA memakai KRS Manual Dari Prodi -->
    {if $id_pt == $smarty.const.PT_UMAHA}
        <div id="tab1" class="tab_sel" align="center" onClick="javascript: displayPanel('1');">MA Terambil</div>
        <div id="tab2" class="tab" style="margin-left:1px;" align="center" onClick="javascript: displayPanel('2');">Cetak KRS</div>
    {else}
        <div id="tab1" class="tab_sel" align="center" onClick="javascript: displayPanel('1');">Penawaran MA</div>
        <div id="tab2" class="tab" style="margin-left:1px;" align="center" onClick="javascript: displayPanel('2');">MA Terambil</div>
        <div id="tab3" class="tab" style="margin-left:1px;" align="center" onClick="javascript: displayPanel('3');">Cetak KRS</div>
    {/if}
</div>

<!-- khusus UMAHA memakai KRS Manual Dari Prodi -->             
{if $id_pt == $smarty.const.PT_UMAHA}
    <div class="tab_bdr"></div>
    <div class="panel" id="panel1" style="display: block">
        {$jkueri_mkterambil}
        <div style="width: 98%;  " id="krs_dihapus">
            <!-- diambilkan dari tabel "pengambilan_mk", semua status -->
        </div>		
    </div>

    <div class="panel" id="panel2" style="display: none">
        {$jkueri_respondosen}
        <div style="width: 98%;  " id="krs_ditampil">
            <!-- diambilkan dari tabel "pengambilan_mk", semua status -->
        </div>
    </div>
{else}
    <div class="tab_bdr"></div>
    <div class="panel" id="panel1" style="display: block">
        {$jkueri_penawaranmk}
        <div style="width: 98%;  " id="krs_ditambah">
            <!-- diambilkan dari tabel "kelas_mk", semester aktif-->
        </div>
    </div>

    <div class="panel" id="panel2" style="display: none">
        {$jkueri_mkterambil} 
        <div style="width: 98%;  " id="krs_dihapus">
            <!-- diambilkan dari tabel "pengambilan_mk", semua status -->
        </div>		
    </div>

    <div class="panel" id="panel3" style="display: none">
        {$jkueri_respondosen}
        <div style="width: 98%;  " id="krs_ditampil">
            <!-- diambilkan dari tabel "pengambilan_mk", semua status -->
        </div>
    </div>
{/if}             
