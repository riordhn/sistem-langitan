<div class="center_title_bar">Biodata Mahasiswa</div>

<script type="text/javascript">
		
    function tanggal(){
        $("#datepicker" ).datepicker();
    }

    function tampildatadua(){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=tampil",
            success: function(data){
                $("#biodata-update").html(data);
            }
        });
    }

    tampildatadua();

    function biodata_simpan(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: $("#"+f).serialize(),
            success: function(data){
                alert(data);
                tampildatadua();
            }
        });
    }

    function propinsi_ambil(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=propinsi&prop="+f,
            success: function(data){
                $("#lahir_kota").html(data);
            }
        });
    }

    function negara_ambil(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=negara&neg="+f,
            success: function(data){
                $("#lahir_propinsi").html(data);
                propinsi_ambil("#lahir_propinsi").val();
            }
        });
    }

    function propinsi_ambil_alamat(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=propinsi&prop="+f,
            success: function(data){
                $("#alamat_kota").html(data);
            }
        });
    }

    function negara_ambil_alamat(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=negara&neg="+f,
            success: function(data){
                $("#alamat_propinsi").html(data);
                propinsi_ambil_alamat("#alamat_propinsi").val();
            }
        });
    }

    function propinsi_ambil_alamat_ayah(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=propinsi&prop="+f,
            success: function(data){
                $("#alamat_kota_ayah").html(data);
            }
        });
    }

    function negara_ambil_alamat_ayah(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=negara&neg="+f,
            success: function(data){
                $("#alamat_propinsi_ayah").html(data);
                propinsi_ambil_alamat_ayah("#alamat_propinsi_ayah").val();
            }
        });
    }

    function propinsi_ambil_alamat_wali(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=propinsi&prop="+f,
            success: function(data){
                $("#alamat_kota_wali").html(data);
            }
        });
    }

    function negara_ambil_alamat_wali(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=negara&neg="+f,
            success: function(data){
                $("#alamat_propinsi_wali").html(data);
                propinsi_ambil_alamat_wali("#alamat_propinsi_wali").val();
            }
        });
    }

    function propinsi_ambil_alamat_ibu(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=propinsi&prop="+f,
            success: function(data){
                $("#alamat_kota_ibu").html(data);
            }
        });
    }
    function negara_ambil_alamat_ibu(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=negara&neg="+f,
            success: function(data){
                $("#alamat_propinsi_ibu").html(data);
                propinsi_ambil_alamat_ibu("#alamat_propinsi_ibu").val();
            }
        });
    }
    function propinsi_ambil_alamat_asli(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=propinsi&prop="+f,
            success: function(data){
                $("#alamat_kota_asli").html(data);
            }
        });
    }
    function negara_ambil_alamat_asli(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=negara&neg="+f,
            success: function(data){
                $("#alamat_propinsi_asli").html(data);
                propinsi_ambil_alamat_asli("#alamat_propinsi_asli").val();
            }
        });
    }
    function propinsi_ambil_sekolah(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=propinsi&prop="+f,
            success: function(data){
                $("#sekolah_kota").html(data);
            }
        });
    }
    function negara_ambil_sekolah(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=negara&neg="+f,
            success: function(data){
                $("#sekolah_propinsi").html(data);
                propinsi_ambil_sekolah("#sekolah_propinsi").val();
            }
        });
    }
    function kota_ambil_sekolah(f){
        $.ajax({
            type: "POST",
            url: "proses/_biodata-data_simpan.php",
            data: "aksi=kotasekolah&kota="+f,
            success: function(data){
                $("#sekolah_asal").html(data);
            }
        });
    }
</script>

<div id="biodata-update">

</div>