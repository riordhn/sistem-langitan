<!-- Author : Seto Priyanggoro -->

<!-- File ini berisi proses dari menu katalog buku -->

<div class="center_title_bar">Evaluasi Perkuliahan Mahasiswa</div>
<form action="evmaba.php" method="post">
<table>
<tr><td><b><center>EVALUASI KINERJA DOSEN DALAM PERKULIAHAN</center></b><br><p align=justify>Evaluasi kinerja dosen dalam perkuliahan ini ditujukan untuk memastikan bahwa kinerja dosen dalam pembelajaran tiap semester telah dilaksanakan dengan baik sesuai dengan tugas pokok dan fungsinya. Evaluasi ini juga ditujukan untuk mengidentifikasi praktek baik (good practices) dalam rangka meningkatkan kualitas proses pembelajaran.

Mengingat pentingnya Informasi ini untuk meningkatkan kualitas proses pembelajaran, mohon agar diisi dengan sebenar-benarnya.</p></td></tr>
<tr><td><table border=1><tr><td><center>PERNYATAAN</center></td><td>&nbsp;</td><td color=red><center>Sangat Tidak Puas</center></td><td><center>Tidak Puas</center></td><td><center>Puas</center></td><td><center>Sangat Puas</center></td></tr>
{foreach $evmaba as $e}
      {if $e@index eq 0}
          <tr><td colspan=6 bgcolor=#BFBFBF>a. Dosen sebagai Perencanaan perkuliahan.</td></tr>
      {/if}
      {if $e@index eq 4}
          <tr><td colspan=6 bgcolor=#BFBFBF>b. Dosen sebagai Pelaksana perkuliahan.</td></tr>
      {/if}
      {if $e@index eq 15}
          <tr><td colspan=6 bgcolor=#BFBFBF>c. Dosen sebagai Evaluator Perkuliahan.</td></tr>
      {/if}
      
          <tr {if $e@index is div by 2}bgcolor="#EEEEEE"{else}bgcolor="#AAAAAA"{/if}>
		<td>{$e.ASPEK}<input name=idmaba type=hidden value="{$e.ID}"></td>
		<td>&nbsp;</td>
		<td color=red><center><input type=radio name=nilai[{$e@index}] value="1"></center></td>
		<td><center><input type=radio name=nilai[{$e@index}] value="2"></center></td>
		<td><center><input type=radio name=nilai[{$e@index}] value="3"></center></td>
		<td><center><input type=radio name=nilai[{$e@index}] value="4"></center></td>		
		<td><center><input type=radio name=nilai[{$e@index}] value="5"></center></td>
	  </tr>
{/foreach}

<tr><td colspan=6><font color=red>*Tidak ada pendapat dipilih jika anda tidak dapat menilai atau tidak paham dengan item evaluasi yang dimaksud.</font><br>Terima Kasih Atas Kesediaan Saudara Mengisi Kuesioner Dengan Sebenar-Benarnya.</td></tr>
</table> 
<input name="mode" id="mode" type="hidden" value="save_evmaba">
<input type="submit" name="simpan" value="simpan"/>
</form>
