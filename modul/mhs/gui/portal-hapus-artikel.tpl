<script>
	$(function() {
	
		{for $foo=0 to $jml_data_article-1}
		
			// Button Set
			$( "#submit{$id_article[$foo]}").buttonset();
			
			$("#btnDelete{$id_article[$foo]}").button({
				icons: {
					primary: "ui-icon-trash",
				},
				text: true
			});
			
			$("#btnApprove{$id_article[$foo]}").button({
				icons: {
					primary: "ui-icon-check",
				},
				text: true
			});
			
			// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
			// Modal
			
			
			// Event Click
			$('#btnEdit{$id_article[$foo]}').click(function(){		
				$('#judul_article_div{$id_article[$foo]}').hide();
				$('#link_article_div{$id_article[$foo]}').hide();
				$('#image_link_article_div{$id_article[$foo]}').hide();
				$('#isi_article_div{$id_article[$foo]}').hide();
				$('#resume_article_div{$id_article[$foo]}').hide();
				
				$('#judul_article{$id_article[$foo]}').show();					
				$('#link_article{$id_article[$foo]}').show();
				$('#image_link_article{$id_article[$foo]}').show();
				$('#isi_article{$id_article[$foo]}').show();
				$('#resume_article{$id_article[$foo]}').show();
				
				$('#btnEdit{$id_article[$foo]}').hide();
				$('#submit{$id_article[$foo]}').show();
			});
			
			$('#btnDelete{$id_article[$foo]}').click(function(){
			
				$('#MyFormDelete{$id_article[$foo]}').submit();
				
			});
			
			/*
			$('#btnDelete{$id_article[$foo]}').click(function(){		
				$( "#dialog{$id_article[$foo]}:ui-dialog" ).dialog( "destroy" );
				$( "#dialog-confirm{$id_article[$foo]}" ).dialog({
					resizable: false,
					height:140,
					modal: true,
					buttons: {
						"Delete": function() {
							//$("#dialog-confirm{$id_article[$foo]}").dialog( "destroy" );
							$('#btnDelete{$id_article[$foo]}').click(function() {
							  $('#MyFormDelete{$id_article[$foo]}').submit();
							});
							//$("#del_article{$id_article[$foo]}").hide();

							//alert("Data Berhasil Dihapus");
						},
						Cancel: function() {
							$("#dialog-confirm{$id_article[$foo]}").dialog( "destroy" );
						}
					}
				});
			});
			*/
			
			// Event Submit
				$('#MyFormDelete{$id_article[$foo]}').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
						
								//$("#dialog-confirm{$id_article[$foo]}").dialog( "destroy" );
								//$('#btnDelete{$id_article[$foo]}').click(function() {
								//$('#MyFormDelete{$id_article[$foo]}').submit();
								//});
								//$("#del_article{$id_article[$foo]}").hide();

								alert("Data Berhasil Dihapus");
								window.location.href = '{$base_url}modul/mhs/#portal-article!portal-hapus-artikel.php';
							
						}
					})
					return false;
				});
			
			
		{/for}
	
	});
</script>
	
<div class="center_title_bar">
	<a style="color:#CCC;" href="portal-article.php">Semua Artikel</a> | 
	<a style="color:#CCC;" href="portal-tambah-artikel.php"> Tambah Artikel </a> | 
	<a> Hapus Artikel </a> 
</div>
<br />
<a style="color:#CCC; font-size:14px; color:#000;" href="portal-hapus-artikel.php"> Tampilkan Semua Artikel </a>
<br /><br />

<div class="column-article">	
	<div class="portlet-article">
		<section>
			<header class="ui-widget-header">
				<div class="portlet-header" style="padding:3px;">Article{$hari}, &nbsp; {$tanggal} &nbsp; {$bulan} &nbsp; {$tahun}</div>
			</header>
		<div id="article-content" class="portlet-content-article ui-widget-content" style="background:#fff;">
			<div style="margin:20px;">
				{for $foo=0 to $jml_data_article-1}
						<form id="MyFormDelete{$id_article[$foo]}" name="MyFormDelete{$id_article[$foo]}" method="post" action="portal-aksi-hapus-artikel.php">
							{if $id!=""}
								<div  id="btnDelete{$id_article[$foo]}" name="btnDelete{$id_article[$foo]}" style="height:30px; width:80px;" title="Hapus Artikel">
									Hapus
								</div>
							{/if}
							<article id="article{$foo}">
							<input style="display:none;" type="text" name="id_article" value="{$id_article[$foo]}" >
							<div>
								&nbsp;
							</div>
							<div class="title">
								
								Judul : <a class="disable-ajax" id="judul_article_div{$id_article[$foo]}" href="#portal-article!portal-hapus-artikel.php?id={$id_article[$foo]}&view=all">{$judul_article[$foo]}</a>
								<textarea style="width:100%; height:30px;display:none;" COLS=30 ROWS=3   id="judul_article{$id_article[$foo]}" name="judul_article{$id_article[$foo]}" >{$judul_article[$foo]}</textarea>
							</div>
							<div class="date">
								Oleh : &nbsp; {$pengguna[$foo]}
								<br />
								Tanggal : &nbsp; {$tgl_article[$foo]}
							</div>
							<div class="content_article">
								{if $view == "all"}
									<legend>Link</legend>
									<div style="" id="link_article_div{$id_article[$foo]}">{$link_article[$foo]}</div><br />								
									<TEXTAREA id="link_article{$id_article[$foo]}" style="width:100%;height:30px;display:none;" NAME="link_article{$id_article[$foo]}" COLS=30 ROWS=6>{$link_article[$foo]}</TEXTAREA>
									
									<legend>Image Link</legend>
									<div style="" id="image_link_article_div{$id_article[$foo]}"><img style="width:100px; height:100px;" src="{$img_link[$foo]}"/></div><br />
									<TEXTAREA id="image_link_article{$id_article[$foo]}" style="width:100%; height:30px;display:none;" NAME="image_link_article{$id_article[$foo]}" COLS=30 ROWS=6>{$img_link[$foo]}</TEXTAREA>
									
									<legend>Resume</legend>
									<div style="" id="resume_article_div{$id_article[$foo]}">{$isi_article[$foo]}</div><br />
									<TEXTAREA id="resume_article{$id_article[$foo]}" style="width:100%; display:none; min-height:150px;" NAME="resume_article{$id_article[$foo]}" COLS=30 ROWS=6>{$resume_article[$foo]}</TEXTAREA>
									
									<legend>Isi</legend>
									<div style="" id="isi_article_div{$id_article[$foo]}">{$isi_article[$foo]}</div><br />
									<TEXTAREA id="isi_article{$id_article[$foo]}" style="width:100%; display:none; min-height:700px;" NAME="isi_article{$id_article[$foo]}" COLS=30 ROWS=6>{$isi_article[$foo]}</TEXTAREA>
								{else}
									{$resume_article[$foo]}
								{/if}
							</div>
							</article>
							
						</form>
					
				{/for}
			</div>
		</div>
		</section>
	</div>
</div>