<script>
	$(document).ready(function(){
		{for $i=0 to $jml_kelompok-1}
		$( "#btn-gabung{$i}" ).button({
			text: true
		});
		{/for}
	});

</script>
<table class="ui-widget" style="width:100%;">
	<tr class="ui-widget-header">
		<td>#</td>
		<td>Kelurahan</td>
		<td>Kelompok</td>
		<td>Quota</td>
		<td>Terisi</td>
		<td>Tersisa</td>
		<td></td>
	</tr>
	{for $i = 0 to $jml_kelompok-1}
	<tr class="ui-widget-content">
		<td>{$i+1}</td>
		<td>{$kelurahan[$i]}</td>
		<td><a href="akademik-kkn.php?view=detail&kelompok={$id_kelompok[$i]}" >{$kelompok[$i]}</a></td>
		<td align="center">{$quota[$i]}</td>
		<td align="center">{$peserta[$i]}</td>
		<td align="center">{$sisa[$i]}</td>
		<td align="center"><div id="btn-gabung{$i}" style="height:25px;font-size:11px;margin-top:10px;margin-bottom:10px;">Gabung ke Kelompok</div></td>
	</tr>
	{/for}
</table>