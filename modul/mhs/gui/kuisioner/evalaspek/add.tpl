<div class="center_title_bar">Master Kelompok Aspek - Tambah</div>

<form action="kuisioner-evalaspek.php?mode=add" method="post" id="gen_mhs">
    <input type="hidden" name="mode" value="add" />
    <table width="693">
	<tr>
            <td width="196">Nama Aspek</td>
      <td width="485"><textarea name="EVALUASI_ASPEK" class="required" rows="2" cols="50"></textarea></td>
      </tr>
	  <tr>
        	<td>Instrumen</td>
            <td>
            	<select id="id_eval_instrumen"  name="id_eval_instrumen" class="required">
                	<option value="">PILIH INSTRUMEN</option>
                    {foreach $instrument as $data}
                        <option value="{$data.ID_EVAL_INSTRUMEN}" {if $id_eval_instrumen==$data.ID_EVAL_INSTRUMEN}selected="true"{/if}>{$data.NAMA_EVAL_INSTRUMEN|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
	    <tr>
        	<td>Kelompok Aspek</td>
            <td>
            	<select id="kelompok_aspek" name="kelompok_aspek" class="required">
                    <option value="">PILIH KELOMPOK ASPEK</option>
                    {foreach $data_program_studi as $data}
                         <option value="{$data.ID_EVAL_KELOMPOK_ASPEK}" {if $kelompok_aspek==$data.ID_EVAL_KELOMPOK_ASPEK}selected="true"{/if}>{$data.NAMA_KELOMPOK}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
	<tr>
            <td>Tipe Aspek</td>
            <td>
            	<input type="radio" value="1" name="TIPE_ASPEK"  class="required"/>
				Pilihan
            	<input type="radio" value="2" name="TIPE_ASPEK" />
            	Komentar Tertulis    
				</td>
        </tr>
       <tr>
            <td>Kelompok Nilai </td>
            <td>
            	<select name="ID_KELOMPOK_NILAI" class="required" id="id_kelompok_nilai">
                	<option value="">-- Pilih Kelompok Nilai --</option>
                	{foreach $evaluasi_kelompok_nilai_set as $data}
            		<option value="{$data.ID_KELOMPOK_NILAI}">{$data.NM_KELOMPOK}</option>
                    {/foreach}
                </select>         </td>
        </tr>
	<tr>
            <td>Skala Nilai </td>
            <td><div id="skala_nilai"></div></td>
        </tr> 
	<tr>
            <td>Aktifasi</td>
            <td>
            	<input type="radio" value="1" name="STATUS_AKTIF"  class="required"/>
				Aktif
            	<input type="radio" value="0" name="STATUS_AKTIF" />
            	Tidak Aktif   
				</td>
        </tr>
	<tr>
            <td>Urutan</td>
            <td>
		<select name="URUTAN" class="required">
			<option value="">No Urut</option>
			{for $no=1 to 30}
				<option value="{$no}">{$no}</option>
			{/for}
		</select>
	    </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
                <input type="submit" value="Simpan" />            
		</td>
        </tr>
  </table>
</form>

{literal}
    <script type="text/javascript">
		$('#gen_mhs').validate();
		
$('#id_eval_instrumen').change(function(){
            $.ajax({
                type:'post',
                url:'getkelompok_eval_aspek.php',
                data:'id_eval_instrumen='+$('#id_eval_instrumen').val(),
                success:function(data){
                    $('#kelompok_aspek').html(data);
                }                    
            })
        });

$('#id_kelompok_nilai').change(function(){
            $.ajax({
                type:'post',
                url:'getSkalaNilai.php',
                data:'id_kelompok_nilai='+$('#id_kelompok_nilai').val(),
                success:function(data){
		    $('#skala_nilai').html(data);
                }                    
            })
        });
		
</script>
{/literal}