<div class="center_title_bar">Master Kelompok Aspek - Tambah</div>

<form action="kuisioner-evalkelompokaspek.php" method="post" id="prod">
    <input type="hidden" name="mode" value="add" />
    <table>
	<tr>
            <td>Nama Kelompok Aspek</td>
            <td><input name="NAMA_KELOMPOK" type="text" class="required" size="80" /></td>
        </tr>
	    <tr>
            <td>Instrumen Evaluasi</td>
            <td>
            	<select name="instrumen" class="required">
                	<option value="">-- Pilih Instrumen Evaluasi --</option>
                	{foreach $instrumen_set as $data}
            		<option value="{$data.ID_EVAL_INSTRUMEN}">{$data.NAMA_EVAL_INSTRUMEN}</option>
                    {/foreach}
                </select>         </td>
        </tr>

        
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
                <input type="submit" value="Simpan" />            </td>
        </tr>
    </table>
</form>

{literal}
<script language="javascript">
	$('form').validate();
</script>
{/literal}