<div class="center_title_bar">Master Evaluasi Instrumen</div>

<table>
    <tr>
        <th>No</th>
        <th>Nama Evaluasi Kelompok Aspek</th>
        <th>Nama Instrumen</th>
        <th></th>
    </tr>
    {$i=1}
    {foreach $evaluasi_kelompok_aspek_set as $evaluasi_kelompok_aspek}
    <tr>
        <td>{$i++}</td>
        <td>{$evaluasi_kelompok_aspek.NAMA_KELOMPOK}</td>
        <td>{$evaluasi_kelompok_aspek.NAMA_EVAL_INSTRUMEN}</td>
        <td>
            <a href="kuisioner-evalkelompokaspek.php?mode=edit&id_eval_kelompok_aspek={$evaluasi_kelompok_aspek.ID_EVAL_KELOMPOK_ASPEK}">Edit</a>
            <a href="kuisioner-evalkelompokaspek.php?mode=delete&id_eval_kelompok_aspek={$evaluasi_kelompok_aspek.ID_EVAL_KELOMPOK_ASPEK}">Delete</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="5"><A href="kuisioner-evalkelompokaspek.php?mode=add">Tambah</A></td>
    </tr>
</table>