<div class="center_title_bar">Master Evaluasi Instrumen</div>

<table>
    <tr>
        <th>No</th>
        <th>Nama Evaluasi Instrumen</th>
        <th>Keterangan Evaluasi Instrumen</th>
        <th></th>
    </tr>
    {$i=1}
    {foreach $evaluasi_instrumen_set as $evaluasi_instrumen}
    <tr>
        <td>{$i++}</td>
        <td>{$evaluasi_instrumen.NAMA_EVAL_INSTRUMEN}</td>
        <td>{$evaluasi_instrumen.KET_EVAL_INSTRUMEN}</td>
        <td>
            <a href="kuisioner-evalinstrumen.php?mode=edit&id_eval_instrumen={$evaluasi_instrumen.ID_EVAL_INSTRUMEN}">Edit</a>
            <a href="kuisioner-evalinstrumen.php?mode=delete&id_eval_instrumen={$evaluasi_instrumen.ID_EVAL_INSTRUMEN}">Delete</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="5"><A href="kuisioner-evalinstrumen.php?mode=add">Tambah</A></td>
    </tr>
</table>