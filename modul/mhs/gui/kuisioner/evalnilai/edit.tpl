<div class="center_title_bar">Master Evaluasi Instrumen - Edit</div>

<form action="kuisioner-evalnilai.php" method="post" id="prod">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_eval_nilai_aspek" value="{$smarty.request.id_eval_nilai_aspek}" />
    <table>
	    <tr>
          <td>Nama Kelompok Nilai </td>
	      <td><select name="ID_KELOMPOK_NILAI" class="required">
                	<option value="">-- Pilih Kelompok Nilai --</option>
                	{foreach $evaluasi_kelompok_nilai_set as $data}
            		<option value="{$data.ID_KELOMPOK_NILAI}" {if $evaluasi_nilai_set[0]['ID_KELOMPOK_NILAI']==$data.ID_KELOMPOK_NILAI} selected="selected" {/if}>{$data.NM_KELOMPOK}</option>
                    {/foreach}
                </select>
          </td>
      </tr>
        <tr>
          <td>Nama Keterangan Nilai Aspek</td>
          <td><input name="KET_NILAI_ASPEK" type="text" class="required" value="{$evaluasi_nilai_set[0]['KET_NILAI_ASPEK']}" size="80" /></td>
        </tr>
		<tr>
            <td>Urutan</td>
            <td>
			
            	<input type="radio" value="1" name="URUTAN" {if {$evaluasi_nilai_set[0]['URUTAN']} == 1}checked="checked"{/if} />
				1
            	<input type="radio" value="2" name="URUTAN" {if {$evaluasi_nilai_set[0]['URUTAN']} == 2}checked="checked"{/if} />
            	2    
				<input type="radio" value="3" name="URUTAN" {if {$evaluasi_nilai_set[0]['URUTAN']} == 3}checked="checked"{/if} />
				3
            	<input type="radio" value="4" name="URUTAN" {if {$evaluasi_nilai_set[0]['URUTAN']} == 4}checked="checked"{/if} />
            	4 
				<input type="radio" value="5" name="URUTAN" {if {$evaluasi_nilai_set[0]['URUTAN']} == 5}checked="checked"{/if} />
            	5
				</td>
        </tr>
		<tr>
            <td>Nilai Aspek</td>
            <td>
            	<input type="radio" value="0" name="NILAI_ASPEK"  {if {$evaluasi_nilai_set[0]['NILAI_ASPEK']} == 0}checked="checked"{/if}/>
				0
            	<input type="radio" value="1" name="NILAI_ASPEK"  {if {$evaluasi_nilai_set[0]['NILAI_ASPEK']} == 1}checked="checked"{/if}/>
            	1
				<input type="radio" value="2" name="NILAI_ASPEK"  {if {$evaluasi_nilai_set[0]['NILAI_ASPEK']} == 2}checked="checked"{/if}/>
				2
            	<input type="radio" value="3" name="NILAI_ASPEK"  {if {$evaluasi_nilai_set[0]['NILAI_ASPEK']} == 3}checked="checked"{/if} />
            	3
				<input type="radio" value="4" name="NILAI_ASPEK" {if {$evaluasi_nilai_set[0]['NILAI_ASPEK']} == 4}checked="checked"{/if} />
            	4
        
        <tr>
		
		<tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
                <input type="submit" value="Simpan" />            </td>
        </tr>
    </table>
</form>
