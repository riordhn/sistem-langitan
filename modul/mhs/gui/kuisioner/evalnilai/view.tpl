<div class="center_title_bar">Master Nilai Aspek</div>

<table>
    <tr>
        <th>No</th>
		<th>Nama Kelompok Nilai </th>
        <th>Nama Keterangan Nilai Aspek</th>
        <th>Nilai Aspek </th>
        <th>Urutan </th>
        <th>Aksi</th>
    </tr>
    {$i=1}
    {foreach $evaluasi_nilai_set as $evaluasi_nilai}
    <tr>
        <td>{$i++}</td>
        <td>{$evaluasi_nilai.NM_KELOMPOK}</td>
        <td>{$evaluasi_nilai.KET_NILAI_ASPEK}</td>
        <td>{$evaluasi_nilai.NILAI_ASPEK}</td>
        <td>{$evaluasi_nilai.URUTAN}</td>
        <td>
            <a href="kuisioner-evalnilai.php?mode=edit&id_eval_nilai_aspek={$evaluasi_nilai.ID_EVAL_NILAI_ASPEK}">Edit</a>
            <a href="kuisioner-evalnilai.php?mode=delete&id_eval_nilai_aspek={$evaluasi_nilai.ID_EVAL_NILAI_ASPEK}">Delete</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="6"><A href="kuisioner-evalnilai.php?mode=add">Tambah</A></td>
    </tr>
</table>