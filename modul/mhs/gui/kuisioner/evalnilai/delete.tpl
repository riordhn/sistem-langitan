<div class="center_title_bar">Master Program Studi - Hapus</div>

<form action="perkuliahan-prodi.php" method="post" id="prod">
     <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="id_prodi" value="{$prodi_set[0]['ID_PROGRAM_STUDI']}" />
    <table>
	    <tr>
            <td>Fakultas</td>
            <td>
            	<select name="fakultas" class="required">
                	<option value="">-- Pilih Fakultas --</option>
                	{foreach $fakultas as $data}
            		<option value="{$data.ID_FAKULTAS}" {if $prodi_set[0]['ID_FAKULTAS']==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Jenjang</td>
            <td>
            	<select name="jenjang" class="required">
                	<option value="">-- Pilih Jenjang --</option>
                	{foreach $jenjang as $data}
            		<option value="{$data.ID_JENJANG}" {if $prodi_set[0]['ID_JENJANG']==$data.ID_JENJANG} selected="selected" {/if}>{$data.NM_JENJANG}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama Prodi</td>
            <td><input name="nm_prodi" type="text" class="required" value="{$prodi_set[0]['NM_PROGRAM_STUDI']}" size="80" /></td>
        </tr>
        <tr>
            <td>No SK</td>
            <td><input name="no_sk" type="text" class="required" value="{$prodi_set[0]['NO_SK']}" size="50" /></td>
        </tr>
        <tr>
            <td>Tgl Pendirian</td>
            <td><input name="tgl_pendirian" type="text" value="{$prodi_set[0]['TGL_PENDIRIAN']}" id="tgl_pendirian" class="datepicker"/></td>
        </tr>
        <tr>
            <td>Masa Berlaku</td>
            <td><input name="masa_berlaku" type="text" class="required number" value="{$prodi_set[0]['MASA_BERLAKU']}"  /></td>
        </tr>
        <tr>
            <td>Gelar Panjang</td>
            <td><input name="gelar_panjang" type="text" class="required" value="{$prodi_set[0]['GELAR_PANJANG']}" size="50" /></td>
        </tr>
        <tr>
            <td>Gelar Pendek</td>
            <td><input name="gelar_pendek" type="text" class="required" value="{$prodi_set[0]['GELAR_PENDEK']}" /></td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
               <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>