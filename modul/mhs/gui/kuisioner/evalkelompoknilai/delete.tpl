<div class="center_title_bar">Master Evaluasi Instrumen- Hapus</div>
<h4>Apakah data evaluasi instrumen ini akan dihapus ?</h4>
<form action="kuisioner-evalinstrumen.php" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="ID_EVAL_INSTRUMEN" value="{$evaluasi_instrumen.ID_EVAL_INSTRUMEN}" />
    <table>
        <tr>
            <td>Nama Evaluasi Instrumen</td>
            <td>{$evaluasi_instrumen.NAMA_EVAL_INSTRUMEN}</td>
        </tr>
        <tr>
            <td>Keterangan Evaluasi Instrumen</td>
            <td>{$evaluasi_instrumen.KET_EVAL_INSTRUMEN}</td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button href="kuisioner-evalinstrumen.php">Batal</button>
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>