<div class="center_title_bar">SKP</div>
<table border="1" width="500">
    <tbody><tr><td align="center" colspan="5"><h2>Kegiatan Kemahasiswaan</h2></td></tr>
    <tr><th align="center"><span><h3>Kegiatan</h3></span></th><th align="center"><h3>Tingkat</h3></th><th align="center"><h3>Keterangan</h3></th><th align="center"><h3>Bobot Nilai</h3></th><th align="center"><h3>Dokumen</h3></th></tr>
	
	<tr><td><span>Anggota Aktif Organisasi</span></td><td align="center">Universitas</td><td align="center">Sie IT</td><td align="center">40</td><td align="center">SK</td></tr>
	
	<tr><td><span>LKMM</span></td><td align="center">Universitas</td><td align="center">LKMM TL</td><td align="center">40</td><td align="center">Sertifikat</td></tr>
	
	<tr><td><span>Panitia Kegiatan Olimpiade Matematika Se-Indonesia</span></td><td align="center">Nasional</td><td align="center">Ketua Panitia</td><td align="center">40</td><td align="center">Sertifikat</td></tr>
	
	<tr><td><span>Juara 1 Karya Tulis Mahasiswa</span></td><td align="center">Nasional</td><td align="center">Ketua Tim</td><td align="center">100</td><td align="center">Piagam</td></tr>
    </tbody></table>