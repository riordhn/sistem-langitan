{* Yudi Sulistya, 01 Aug 2013 (tablesorter) *}
{literal}
<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]], widgets: ['zebra']
		}
	);
}
);
</script>
{/literal}
<div class="center_title_bar">Daftar Peserta Mata Ajar</div>
<p><center><h2>Peserta Mata Ajar Semester {$semes}</h2></center></p>
<table width="850" id="myTable" class="tablesorter">
<thead>
<tr>
	<th align="center">Mata Ajar</th>
	<th align="center">Kelas</th>
	<th align="center">Jadwal</th>
	<th align="center">Ruang</th>
	<th align="center">Kuota</th>
	<th align="center">Peserta</th>
</tr>
</thead>
<tbody>
{$isitabel}
</tbody>
</table>