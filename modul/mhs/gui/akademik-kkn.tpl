<div class="center_title_bar">Pemilihan Kelompok KKN</div>
{if $error==''}
    <div class="ui-button ui-state-active ui-corner-all" style="width: 98%;padding: 10px;font-size: 1.1em">PENGUMUMAN KKN</div>
    {foreach $data_kegiatan as $dk}
        <div style="width: 98%;padding: 10px;margin: 10px 0px">
            <i>Tanggal Posting : <strong>{$dk.TGL_POST}</strong></i>
            {$dk.ISI}
        </div>
    {/foreach}
    </div>
    <form action="akademik-kkn.php?view=index" method="post">
        <table style="width: 60%" class="ui-widget">
            <tr>
                <th colspan="2">DETAIL MAHASISWA</th>
            </tr>
		<tr>
			<td>Nama</td>
			<td>: {$data_mhs.NM_PENGGUNA}</td>
		</tr>
		<tr>
			<td>NIM</td>
			<td>: {$data_mhs.NIM_MHS}</td>
		</tr>
		<tr>
			<td>No. Rekening</td>
			<td>: <input type="text" name="no_rek" value="{$data_mhs.NO_REKENING}" /></td>
		</tr>
		<tr>
			<td>Bank</td>
			<td>: <input type="text" name="bank" value="{$data_mhs.BANK_REKENING}"/></td>
		</tr>
		<tr>
			<td>Atas Nama</td>
			<td>: <input type="text" name="nama" value="{$data_mhs.ATAS_NAMA_REKENING}" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="submit" value="Simpan"></td>
		</tr>
        </table>
	</form>
    <div class="ui-button ui-state-active ui-corner-all" style="width: 98%;padding: 10px;font-size: 1.1em">KELOMPOK KKN</div>
    {if $error_kelompok==''}
	
        <table style="width: 60%" class="ui-widget">
            <tr>
                <th colspan="2">DETAIL KELOMPOK KKN</th>
            </tr>
            <tr>
                <td>ANGKATAN</td>
                <td>: {$angkatan.NAMA_ANGKATAN}</td>
            </tr>
            <tr>
                <td>NAMA PEMBINA</td>
                <td>: {$kelompok_kkn.DPL}</td>
            </tr>
            <tr>
                <td>NAMA KELOMPOK</td>
                <td>: {$kelompok_kkn.NAMA_KELOMPOK}</td>
            </tr>
            <tr>
                <td>KELURAHAN</td>
                <td>: {$kelompok_kkn.NM_KELURAHAN}</td>
            </tr>
            <tr>
                <td>KECAMATAN</td>
                <td>: {$kelompok_kkn.NM_KECAMATAN}</td>
            </tr>
            <tr>
                <td>KOTA</td>
                <td>: {$kelompok_kkn.NM_KOTA}</td>
            </tr>
        </table>
        <table style="width: 98%" class="ui-widget">
            <tr>
                <th colspan="7">DATA KELOMPOK KKN MAHASISWA</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>NIM</th>
                <th>NAMA</th>
                <th>ANGKATAN</th>
                <th>FAKULTAS</th>
                <th>PROGRAM STUDI</th>
                <th>KONTAK</th>
            </tr>
            {foreach $data_kelompok as $dk}
                <tr {if $id_mhs==$dk.ID_MHS}style="background-color: #99ff99"{/if}>
                    <td>{$dk@index+1}</td>
                    <td>{$dk.NIM_MHS}</td>
                    <td>{$dk.NM_PENGGUNA} <br/><i>{if $dk.KELAMIN_PENGGUNA==1}Laki-laki{else}Perempuan{/if}</i></td>
                    <td>{$dk.THN_ANGKATAN_MHS}</td>
                    <td>{$dk.NM_FAKULTAS}</td>
                    <td>{$dk.NM_JENJANG} {$dk.NM_PROGRAM_STUDI}</td>
                    <td>{$dk.MOBILE_MHS}</td>
                </tr>
            {/foreach}
        </table>
    {else}
        <div style="width: 98%;color: red;margin: 20px 0px;padding: 20px;"><i>{$error_kelompok}</i></div>
    {/if}
{else}
    {$error}
{/if}