<!-- Author : Seto Priyanggoro -->

<!-- File ini berisi proses dari menu katalog buku -->

<div class="center_title_bar">Evaluasi Perkuliahan Mahasiswa</div>

<form action="evkul.php" method="post">
<table>
<tr><td><select name=kdmtk id=kdmtk>
        <option value=\"-\">-------------------------------</option>
	 {foreach $evmatkul0 as $e}
        {if $e@index eq 0}
        <option value={$e.ID_KELAS_MK}_{$e.ID_DOSEN}_{$e.STATUS_PRAKTIKUM}_{$e.NM_PENGGUNA}_{$e.NM_MATA_KULIAH} selected>{$e.NM_MATA_KULIAH} {$e.NM_PENGGUNA}</option>
	 
        {elseif $e@index gt 0}
        <option value={$e.ID_KELAS_MK}_{$e.ID_DOSEN}_{$e.STATUS_PRAKTIKUM}_{$e.NM_PENGGUNA}_{$e.NM_MATA_KULIAH}>{$e.NM_MATA_KULIAH} {$e.NM_PENGGUNA}</option>
	 {/if}
	 {/foreach}
        </option>
</td><td><input name="mode" id="mode" type="hidden" value="save_evkul0">
<input type="submit" name="simpan" value="simpan"/>
</td></tr></table>
</form>
{if $totalmatkul eq 0} 
Terima Kasih, Anda sudah mengisi evaluasi perkuliahan dan praktikum.
{/if}
