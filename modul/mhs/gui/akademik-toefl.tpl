{* Yudi Sulistya, 02 Aug 2013 (tablesorter) *}
{literal}
<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#toefl").tablesorter(
		{
		sortList: [[0,0]], widgets: ['zebra']
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">ELPT</div>
<p><center><h2>Hasil Ujian ELPT</h2></center></p>
<table id="toefl" class="tablesorter">
<thead>
<tr>
	<th>Kemampuan</th>
	<th>Nilai</th>
</tr>
<tbody>
{$tampil_data}
</tbody>
</table>
Total : {$tampil_data2}