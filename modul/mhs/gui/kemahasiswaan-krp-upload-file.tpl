<!DOCTYPE html>
<head>
	<meta charset="UTF-8"> 
	<title>Upload Dokumen SKKK - {$nama_pt}</title>
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
	<link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
	<script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
	<script language="javascript" src="../../js/jquery.validate.js"></script>
	<script language="javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
</head>
<body>
{literal}
	<style>
		label.error {
			color: red;
			font-size:11px;
		}
	</style>

    <script language="javascript">

        var popupWindow = null;
        function popup(mypage, myname, w, h, pos, infocus) {
            if (pos == "random")
            {
                LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
                TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
            }
            else
            {
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
            }
            settings = "width=" + w + ",height=" + h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
            popupWindow = window.open("", myname, settings);
            if (infocus == "front") {
                popupWindow.focus();
                popupWindow.location = mypage;
            }
            if (infocus == "back") {
                popupWindow.blur();
                popupWindow.location = mypage;
                popupWindow.blur();
            }
        }

        function new_window(mypage, myname, w, h, pos, infocus) {
            if (pos == "random")
            {
                LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100;
                TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100;
            }
            else
            {
                LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100;
                TopPosition = (screen.height) ? (screen.height - h) / 2 : 100;
            }
            settings = "width=" + w + ",height=" + h + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
            popupWindow = window.open("", myname, settings);
            if (infocus == "front") {
                popupWindow.focus();
                popupWindow.location = mypage;
            }
            if (infocus == "back") {
                popupWindow.blur();
                popupWindow.location = mypage;
                popupWindow.blur();
            }
        }

        
    </script>

	<script type="text/javascript">
		function tampildata(y){
			$.ajax({
				type:"POST",
				url:"kemahasiswaan-krp-upload-file.php?id_file_krp_khp="+y,
				data: "aksi=tampil",
				success: function(data){
					//$("#upload_section").html(data);
					location.reload(true);
				}
			});
		}
		function krp_file_hapus (v,y){
			$.ajax({
				type:"POST",
				url:"kemahasiswaan-krp-upload-file.php",
				data: "mode=delete_file&id_file_krp_khp="+v,
				success: function(data){
					//alert(data);
					tampildata(y);
					//location.reload(true);
				}
			});
		}
	</script>
{/literal}
<div id="upload_section">
	{if isset($sukses_simpan)}
		<div class="ui-state-highlight ui-corner-all" style="margin: 10px auto;padding: 10px;width:40%"> 
			<span class="ui-icon ui-icon-info" style="float: left;margin:0px 5px;">  </span>
			<p>Data Tagihan Berhasil Di Simpan.<br/><br/></p>
				{* {$berhasil} <span style="color: green">Data Berhasil di rubah</span><br/>
				{$gagal} <span style="color: red">Data tidak di rubah</span></p> *}
		</div>
	{/if}


	<table class="ui-widget" width="70%" style="margin: 20px auto;">
			<tr class="ui-widget-header">
				<th class="header-coloumn">Kegiatan : {$nama_kegiatan}</th>
			</tr>
			<tr class="ui-widget-header">
				<th colspan="2" class="header-coloumn">Penyelenggara : {$nama_kegiatan}</th>
			</tr>
			<tr class="ui-widget-header">
				<th colspan="2" class="header-coloumn">Waktu Pelaksanaan : {$waktu_kegiatan}</th>
			</tr>
	</table>
	<table class="ui-widget" width="70%" style="margin: 20px auto;">		
		<thead>
			<tr>
				<th>No</th>
				<th>#</th>
				<th>Nama File</th>
				<th>Tipe File</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			{foreach $file_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					{assign var="image" 
					value="../../files/kemahasiswaan/{$uk.ID_MHS}/{$uk.NM_FILE_KRP_KHP}"} 
					{if file_exists($image)}
						{if $uk.TIPE_FILE_KRP_KHP == 'image/gif' OR $uk.TIPE_FILE_KRP_KHP == 'image/jpeg' OR $uk.TIPE_FILE_KRP_KHP == 'image/jpg' OR $uk.TIPE_FILE_KRP_KHP == 'image/pjpeg' OR $uk.TIPE_FILE_KRP_KHP == 'image/x-png' OR $uk.TIPE_FILE_KRP_KHP == 'image/png'}
							<td>
								<img src="../../files/kemahasiswaan/{$uk.ID_MHS}/{$uk.NM_FILE_KRP_KHP}?t=timestamp" border="0" width="50" />
							</td>
						{else}
							<td>
								<a href="{$base_url}files/kemahasiswaan/{$uk.ID_MHS}/{$uk.NM_FILE_KRP_KHP}" class="disable-ajax" target="_blank">Download File</a>
							</td>
						{/if}
					{else}
						<td class="center"><img src="includes/images/cancel.png" border="0" width="50" /></td>
					{/if}
					<td><strong>{$uk.NM_FILE_KRP_KHP}</strong></td>
					<td><strong>{$uk.TIPE_FILE_KRP_KHP}</strong></td>
					<!-- <td></td> -->
					<td align="center" onclick="if(confirm('Yakin hapus ?')!=0){ krp_file_hapus({$uk.ID_FILE_KRP_KHP},{$uk.ID_KRP_KHP}); }"><font color="red">[Hapus]</font></td>
				</tr>
			{foreachelse}
				<tr>
					<td class="center" colspan="5"><font color="red"><strong>Belum Ada Dokumen Di Upload</strong></font></td>
				</tr>
			{/foreach}
			<!-- <tr>
				<td colspan="9" class="center">
					<a href="master-dokumen.php?mode=add"><b style="color: red">Tambah Unit Kerja</b></a>
				</td>
			</tr> -->
		</tbody>
	</table>

	<form id="form_upload" action="kemahasiswaan-krp-upload-file.php?{$smarty.server.QUERY_STRING}" method="post" enctype="multipart/form-data">
		<table class="ui-widget" width="70%" style="margin: 20px auto;">
			<tr class="ui-widget-header">
				<th colspan="2" class="header-coloumn">Tambah File Untuk Kegiatan {$nama_kegiatan}</th>
			</tr>

			<tr class="ui-widget-content">
				<td class="center">Upload File (Gambar atau Pdf)</td>
			</tr>

			<tr class="ui-widget-content">
				<td class="center"><input type="button" name="upload_dokumen" value="Upload Dokumen" onclick="javascript:popup('{$URL}', 'name', '600', '400', 'center', 'front')" style="text-align:center"> </td>
			</tr>

			<!-- <tr class="ui-widget-content">
				<td>Upload File (Gambar atau Pdf)</td>
				<td><input type="file" name="file" id="file" class="required" /> </td>
			</tr>
			<tr class="ui-widget-content">
				<td colspan="2" class="center">
					<input type="submit" name="excel" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Submit" />
					<input type="hidden" name="mode" value="add_upload"/>
					<input type="hidden" name="id_krp_khp" value="{$id_krp_khp}"/>
				</td>
			</tr> -->
		</table>
	</form>

	<div id="hasil_upload" {if $status_upload==''}style="display:none"{/if}>
		{if $status_upload==1}
			<div class="ui-state-highlight ui-corner-all" style="margin: 10px auto;padding: 10px;width:40%"> 
				<span class="ui-icon ui-icon-info" style="float: left;margin:0px 5px;">  </span>
				<p>Berhasil Upload File <br/><br/>{$status_message}</p>
			</div>
		{else if $status_upload==0}
			<div class="ui-state-error ui-corner-all" style="margin: 10px auto;padding: 10px;width:40%"> 
				<span class="ui-icon ui-icon-alert" style="float: left;margin:0px 5px;">  </span> 
				<p>Gagal Upload file <br/><br/>{$status_message}></p>
			</div>
		{/if}
	</div>

	<div id="hapus_upload" {if $status_hapus==''}style="display:none"{/if}>
		{if $status_hapus==1}
			<div class="ui-state-highlight ui-corner-all" style="margin: 10px auto;padding: 10px;width:40%"> 
				<span class="ui-icon ui-icon-info" style="float: left;margin:0px 5px;">  </span>
				<p>Berhasil Hapus File <br/><br/>{$status_message}</p>
			</div>
		{else if $status_hapus==0}
			<div class="ui-state-error ui-corner-all" style="margin: 10px auto;padding: 10px;width:40%"> 
				<span class="ui-icon ui-icon-alert" style="float: left;margin:0px 5px;">  </span> 
				<p>Gagal Hapus file <br/><br/>{$status_message}></p>
			</div>
		{/if}
	</div>
</div>
</body>
</html>