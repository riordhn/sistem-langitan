<div class="center_title_bar">Evaluasi Perkuliahan/Praktikum</div>
{literal}
    <style type="text/css">
        label.error{color:red;}
    </style>
{/literal}
{if isset($data_mk)}
    <form action="evkul.php" method="get" id="filter">
        <table class="ui-widget">
            <tr>
                <td style="padding: 5px">
                    Pilih Mata Kuliah :<br/>
                    <p style="font-size: 0.77em"><span style="color: red">Merah</span>:Belum,<span style="color: green">Hijau</span>:Sudah</p>
                </td>
                <td style="vertical-align: middle">
                    <select name="kelas" onchange="$('#filter').submit()">
                        <option value="">Pilih Mata Kuliah</option>        
                        {foreach $data_mk as $mk}
                            {if $mk.STATUS_PRAKTIKUM==''}
                                {$status_praktikum='X'}
                            {else}
                                {$status_praktikum=$mk.STATUS_PRAKTIKUM}
                            {/if}
                            {$kode_kelas=$mk.ID_KELAS_MK|cat:'_'|cat:$mk.ID_DOSEN|cat:'_'|cat:$status_praktikum|cat:'_'|cat:$mk.STATUS_MKTA}
                            <option value="{$kode_kelas}" {if isset($smarty.get.kelas)}{if $smarty.get.kelas==$kode_kelas}selected="true"{/if}{/if}
                                    {if $mk.STATUS_ISI>0}
                                        style="color: green"
                                    {else}
                                        style="color: red"
                                    {/if}
                                    >
                                {$mk.NM_MATA_KULIAH} Kelas {$mk.NAMA_KELAS} ({$mk.NM_PENGGUNA}) 
                            </option>
                        {/foreach}
                    </select>
                    <input type="hidden" name="mode" value="tampil"/>
                </td>
            </tr>
        </table>
    </form>
    {if $mode == 'tampil' && isset($data_aspek)}
        {$kode_kelas=explode('_',$smarty.get.kelas)}
        <form method="post" id="eva" action="evkul.php?{$smarty.server.QUERY_STRING}">
            <table class="ui-widget" style="width: 100%">
                <tr class="ui-widget-header">
                    {if $praktikum==0}
                        <th colspan="6" style="border-bottom:1px solid #eaeafa ;">
                            <font style="font-size: 1.5em">EVALUASI KINERJA DOSEN DALAM PERKULIAHAN</font>
                            <br>
                            Evaluasi kinerja dosen dalam perkuliahan ini ditujukan untuk memastikan bahwa kinerja dosen dalam pembelajaran tiap semester telah dilaksanakan dengan baik sesuai dengan tugas pokok dan fungsinya. Evaluasi ini juga ditujukan untuk mengidentifikasi praktek baik (good practices) dalam rangka meningkatkan kualitas proses pembelajaran.
                            Mengingat pentingnya Informasi ini untuk meningkatkan kualitas proses pembelajaran, mohon agar diisi dengan sebenar-benarnya.
                        </th>
                    {else if $praktikum==1}
                        <th colspan="6" style="border-bottom:1px solid white ;">
                            <font style="font-size: 1.5em">EVALUASI KINERJA DOSEN DALAM PRAKTIKUM</font>
                            <br>
                            Evaluasi kinerja dosen/asisten dosen dalam praktikum ini ditujukan untuk memastikan bahwa kinerja dosen/asisten dosen dalam praktikum tiap semester telah dilaksanakan dengan baik sesuai dengan tugas pokok dan fungsinya. Evaluasi ini juga ditujukan untuk mengidentifikasi praktek baik (good practices) dalam rangka meningkatkan kualitas proses praktikum.
                            Mengingat pentingnya Informasi ini untuk meningkatkan kualitas proses praktikum, mohon agar diisi dengan sebenar-benarnya.
                        </th>
                    {/if}
                </tr>
                {$jumlah_aspek=0}
                {foreach $data_aspek as $dk}
                    <tr class="ui-widget-header">
                        <th colspan="6" style="font-size: 1.1em">{$dk.NAMA_KELOMPOK}</th>
                    </tr>
                    {foreach $dk.DATA_ASPEK as $da}
                        <tr class="ui-widget-content">
                            {$jumlah_aspek=$jumlah_aspek+1}
                            <td style="padding: 5px">
                                {$da.EVALUASI_ASPEK}
                                <br/>
                                <label class="error" for="nilai{$jumlah_aspek}" style="font-size:0.8em ;display: none">Harus Diisi</label>
                            </td>
                            {if $da.TIPE_ASPEK==1}
                                {foreach $da[0] as $dn}
                                    {if $dn.NILAI_ASPEK!=0}
                                    <td align="center" style="font-size: 0.8em">
                                        {$dn.KET_NILAI_ASPEK}<br/>
                                        <input type="radio" class="required" name="nilai{$jumlah_aspek}" value="{$dn.NILAI_ASPEK}"/>       
                                    </td>          
                                    {/if}                      
                                {/foreach}
                            {else}
                                <td colspan="5">
                                    <textarea class="required" style="resize: none;width: 90%" maxlength="120" name="nilai{$jumlah_aspek}"></textarea>
                                </td>
                            {/if}
                        <input type="hidden" name="aspek{$jumlah_aspek}" value="{$da.ID_EVAL_ASPEK}"/>
                        <input type="hidden" name="kel_aspek{$jumlah_aspek}" value="{$da.ID_EVAL_KELOMPOK_ASPEK}"/>
                        </tr>
                    {/foreach}
                {/foreach}
                <tr>
                    <td colspan="6" style="text-align: center">
                        <span style="color: red;text-align: left">*  Tidak ada pendapat dipilih jika anda tidak dapat menilai atau tidak paham dengan item evaluasi yang dimaksud.</span><br/>
                        <span style="font-size: 1.5em;font-family: Trebuchet MS">Terima Kasih Atas Kesediaan Saudara Mengisi Kuesioner Dengan Sebenar-Benarnya.</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center">
                        <input type="hidden" name="jumlah_aspek" value="{$jumlah_aspek}" />
                        <input type="hidden" name="id_kelas_mk" value="{$kode_kelas[0]}"/>
                        <input type="hidden" name="status_praktikum" value="{$praktikum}"/>
                        <input type="hidden" name="id_dosen" value="{$kode_kelas[1]}"/>
                        <input type="hidden" name="mode" value="simpan"/>
                        <input type="submit" class="ui-widget-content ui-state-hover ui-corner-all" style="padding: 8px;" value="Simpan"/>
                    </td>
                </tr>
            </table>
        </form>
    {else}
        {if isset($alert)}{$alert}{/if}
    {/if}
{else}
    {if isset($alert)}{$alert}{/if}
{/if}
{literal}
    <script>
        $('#eva').validate();
    </script>
{/literal}