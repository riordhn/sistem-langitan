{if $boleh_lihat_khs and $proses_evaluasi.status_isi == 1}
    <h2>Hasil Studi Semester {$semester.NM_SEMESTER} {$semester.TAHUN_AJARAN}</h2>
    <a style="padding:5px" class="ui-button ui-state-default ui-corner-all disable-ajax" onclick="window.open('{$cetak_khs_link}');"><b>Cetak</b></a>
    <table id="myTable" class="tablesorter">
        <thead>
            <tr>
                <th>Kode</th>
                <th>Mata Kuliah</th>
                <th>Kelas</th>
                <th>SKS</th>
                <th>Nilai Huruf</th>
                {if $boleh_lihat_detail}
                    <th>Detail</th>
                {/if}
            </tr>
        </thead>
        <tbody>
            {foreach $pengambilan_mk_set as $pengambilan_mk}
                <tr>
                    <td>{$pengambilan_mk.KD_MATA_KULIAH}</td>
                    <td>{$pengambilan_mk.NM_MATA_KULIAH}</td>
                    <td>{$pengambilan_mk.NAMA_KELAS}</td>
                    <td align="center">{$pengambilan_mk.KREDIT_SEMESTER}</td>
                    <td align="center">
                        {if $pengambilan_mk.NILAI_HURUF == ''}
                            *K
                        {else}
                            {if $pengambilan_mk.FLAGNILAI == 1}
                                {$pengambilan_mk.NILAI_HURUF}
                            {else}
                                *BT
                            {/if}
                        {/if}
                    </td>
                    {if $boleh_lihat_detail}
                        <td align="center"><a style="text-decoration:underline;cursor:pointer;" onclick="window.open('proses/_akademik-khs-tampil_det.php?mk={$pengambilan_mk.ID_PENGAMBILAN_MK}', 'baru2');">Detail</a></td>
                    {/if}
                </tr>
            {/foreach}
        </tbody>
    </table>
{elseif $proses_evaluasi.status_isi == 0}
    <h2>Hasil Studi Semester {$semester.NM_SEMESTER} {$semester.TAHUN_AJARAN} Belum Bisa Dicetak</h2>
    {$proses_evaluasi.message}
{elseif not $boleh_lihat_khs}
    <h2>Hasil Studi Semester {$semester.NM_SEMESTER} {$semester.TAHUN_AJARAN} Belum Bisa Dicetak</h2>
    <h4>Pastikan tagihan keuangan Anda terselesaikan. Pengecekan tagihan keuangan bisa Anda lihat di menu Keuangan.</h4>
{/if}