<div class="center_title_bar">Pengajuan Wisuda</div>
<!--{literal}
    <script type="text/javascript">
		alert("Alur / syarat Wisuda : \r\n 1. Sudah menempuh Skripsi/ TA/ Tesis \r\n 2. Tidak mempunyai piutang pembayaran \r\n 3. Nilai D tidak boleh lebih dari 20% \r\n 4. Mengisi biodata dan evaluasi \r\n 5. Mengisi abstrak dan judul \r\n 6. Melakukan pembayaran wisuda (Bank)\r\n \r\n Kemudian ke akademik fakultas dengan membawa : \r\n 1. Berkas lolos Laboratorium \r\n 2. Berkas lolos Perpustakaan \r\n 3. KTP/ SIM/ KK (Copy) \r\n 4. Ijazah SMA (Copy) \r\n 5. Berkas lolos ELPT");
    </script>
{/literal}
-->
{$tampil_data}

{literal}
    <script>
		$('#lengkap').submit(function() {
                    if($('#bulan_awal_bimbingan').val()==''||$('#bulan_akhir_bimbingan').val()==''||$('#judul').val()==''){
                            //$('#alert').fadeIn();
                            alert("Tanggal Bulan Bimbingan dan Judul Skripsi/TA Harus Diisi.");
                    return false;
                    }  
            });
            $('#lengkap').validate();
            $( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy',changeMonth: true,
      changeYear: true});
    </script>
{/literal}

{literal}
<script language="javascript" type="text/javascript">

function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

</script>
{/literal}