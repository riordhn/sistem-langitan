<div class="center_title_bar">Tagihan SPP Bulanan</div>
{if !empty($informasi)}
	{$informasi}
{else}
	<div class="alert alert-primary mt-3 mb-3" role="alert">
		<strong>Informasi Tambahan!</strong> Jika terdapat data yang belum sesuai, maka data pembayaran masih dalam proses.		
	</div>
	<h3 style="padding: 5px;margin-top:20px;margin-bottom:10px;">Semester berjalan : {$semester_bayar.NM_SEMESTER} ({$semester_bayar.TAHUN_AJARAN})</h3>
	<hr/>
	<h4>Tagihan Pembayaran SPP anda tiap Bulan adalah <b class="text-primary">Rp. {number_format($tagihan_perbulan)}</b></h4>
	<hr/>
	<table class="table table-consended table-striped">
		<tr class="">
			<th>Bulan</th>
			<th class="text-center">Status Pembayaran</th>
		</tr>
		{foreach $pembayaran_bulanan as $pem}			
			<tr>
				<td>{$pem.NM_BULAN}</td>
				<td class="text-center">
					{if $pem.PEMBAYARAN_BULANAN>0}
						<h4><span class="badge badge-success">Sudah<span></h4>
					{else}
						<h4><span class="badge badge-danger">Belum<span></h4>
					{/if}
				</td>
			</tr>
		{/foreach}
	</table>
{/if}
