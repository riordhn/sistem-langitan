<!-- Author : Seto Priyanggoro -->

<!-- File ini berisi proses dari menu katalog buku -->

<div class="center_title_bar">Evaluasi Administrasi Fakultas</div>
<form action="evafmhs.php" method="post">
<table>
<tr><td><b><center>SURVEY ADMINISTRASI FAKULTAS UNIVERSITAS AIRLANGGA</center></b><br><p align=justify>Evaluasi kinerja tenaga administrasi dalam pelayanan akademik mahasiswa ini ditujukan untuk mengevaluasi kinerja pelayanan administratif di tingkat fakultas dan mengidentifikasi praktek baik (good practices) dalam rangka meningkatkan kualitas layanan akademik.</p><br><p align=justify>Mengingat pentingnya Informasi ini untuk meningkatkan kualitas pelayanan akademik, mohon agar diisi dengan sebenar-benarnya. Selamat Mengisi dan Terima Kasih atas kontribusi Saudara dalam peningkatan mutu Universitas Airlangga</p></td></tr>
<tr><td><table border=1><tr><td><center>PERNYATAAN</center></td><td>&nbsp;</td><td color=red><center>Sangat Tidak Puas</center></td><td><center>Tidak Puas</center></td><td><center>Puas</center></td><td><center>Sangat Puas</center></td><td><center>Tidak Ada Pendapat*</center></td></tr>
{foreach $evaf as $e}
      {if $e@index eq 0}
          <tr><td colspan=7 bgcolor=#BFBFBF>SUB BAGIAN AKADEMIK</td></tr>
      {/if}
      {if $e@index eq 7}
          <tr><td colspan=7 bgcolor=#BFBFBF>SUB BAGIAN KEMAHASISWAAN</td></tr>
      {/if}
      {if $e@index  eq 14}
          <tr><td colspan=7 bgcolor=#BFBFBF>SUB BAGIAN KEUANGAN</td></tr>
      {/if}
      {if $e@index eq 20}
          <tr><td colspan=7 bgcolor=#BFBFBF>SUB BAGIAN SARANA PRASARANA</td></tr>
      {/if}
      {if $e@index eq 28}
          <tr><td colspan=7 bgcolor=#BFBFBF>UNIT SISTEM INFORMASI</td></tr>
      {/if}
          <tr {if $e@index is div by 2}bgcolor="#EEEEEE"{else}bgcolor="#AAAAAA"{/if}>
		<td>{$e.ASPEK}<input name=idmaba type=hidden value="{$e.ID}"></td>
		<td>&nbsp;</td>
		<td color=red><center>{$e.A1}<input type=radio name=nilai[{$e@index}] value="1"></center></td>
		<td><center>{$e.A2}<input type=radio name=nilai[{$e@index}] value="2"></center></td>
		<td><center>{$e.A3}<input type=radio name=nilai[{$e@index}] value="3"></center></td>
		<td><center>{$e.A4}<input type=radio name=nilai[{$e@index}] value="4"></center></td>		
		<td><center>{$e.A5}<input type=radio name=nilai[{$e@index}] value="5"></center></td>

	  </tr>
{/foreach}

<tr><td colspan=7><font color=red>*Tidak ada pendapat dipilih jika anda tidak dapat menilai atau tidak paham dengan item evaluasi yang dimaksud.</font><br>Terima Kasih Atas Kesediaan Saudara Mengisi Kuesioner Dengan Sebenar-Benarnya.</td></tr>
</table> 
<input name="mode" id="mode" type="hidden" value="save_evaf">
<input type="submit" name="simpan" value="simpan"/>
</form>
