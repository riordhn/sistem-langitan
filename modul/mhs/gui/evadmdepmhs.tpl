<!-- Author : Seto Priyanggoro -->

<!-- File ini berisi proses dari menu katalog buku -->

<div class="center_title_bar">Evaluasi Administrasi Prodi oleh Mahasiswa</div>
<form action="evadmdepmhs.php" method="post">
<table>
    <tr><td class=judul_head>&#187; EVALUASI KINERJA ADMINISTRASI PRODI &#187;</td></tr>
    <tr><td class=kembali><p align=justify>
Evaluasi kinerja tenaga administrasi dalam pelayanan akademik mahasiswa ini ditujukan untuk mengevaluasi kinerja pelayanan administratif di tingkat departemen dan mengidentifikasi praktek baik (good practices) dalam rangka meningkatkan pelayanan akademik. 
Mengingat pentingnya Informasi ini untuk meningkatkan kualitas pelayanan akademik, mohon agar diisi dengan sebenar-benarnya.  
</p></td></tr>
</table>
<table><tr><td>&nbsp;NAMA TENAGA ADMINISTRASI</td><td>:</td><td>
{foreach $dosenad_mhs as $e}
  {$e.NM_PENGGUNA}
{/foreach}
</td></tr>
<tr><td class=judul>PRODI</td><td>:</td><td>
{foreach $dosenad_mhs as $e}
  {$e.NM_PROGRAM_STUDI}
{/foreach}
</td></tr></table>
<tr><td class=isi><table border=1><tr><td rowspan=2>No.</td><td rowspan=2>Aspek yang dinilai</td><td colspan=5><center>Skala</center></td></tr><tr><td><center>1</center></td><td><center>2</center></td><td><center>3</center></td><td><center>4</center></td><td><center>5</center></td></tr>
{foreach $evad_mhs as $e}
          <tr {if $e@index is div by 2}bgcolor="#EEEEEE"{else}bgcolor="#AAAAAA"{/if}>
 {if $e@index < 8}
<td>{$e@index+1}</td>		
<td>{$e.ASPEKAD}<input name=idwali type=hidden value="{$e.IDAD}"></td>
		
		<td color=red><center><input type=radio name=nilai[{$e@index}] value="1">{$e.A1}</center></td>
		<td><center><input type=radio name=nilai[{$e@index}] value="2">{$e.A2}</center></td>
		<td><center><input type=radio name=nilai[{$e@index}] value="3">{$e.A3}</center></td>
		<td><center><input type=radio name=nilai[{$e@index}] value="4">{$e.A4}</center></td>
		<td><center><input type=radio name=nilai[{$e@index}] value="5">{$e.A5}</center></td>
	  </tr>
{/if}
 {if $e@index eq 8}
          <tr><td colspan=7>KOMENTAR TERTULIS.</td></tr>
<tr><td colspan=7>1.Menurut saudara, kinerja terbaik apakah yang sudah dilakukan tenaga administrasi tersebut dalam perwalian?</td></tr>
<tr><td colspan=7><textarea name=nilai[{$e@index}] cols=75 rows=5></textarea></td></tr>

      {/if}
 {if $e@index eq 9}
<tr><td colspan=7>2.Harapan apakah yang dapat saudara usulkan untuk meningkatkan kinerja tenaga administrasi tersebut dalam perwalian? </td></tr>
<tr><td colspan=7><textarea name=nilai[{$e@index}] cols=75 rows=5></textarea></td></tr>
      {/if}

{/foreach}

<tr><td colspan=7><font color=red>*Tidak ada pendapat dipilih jika anda tidak dapat menilai atau tidak paham dengan item evaluasi yang dimaksud.</font><br>Terima Kasih Atas Kesediaan Saudara Mengisi Kuesioner Dengan Sebenar-Benarnya.</td></tr>
</table> 
<input name="mode" id="mode" type="hidden" value="save_evad_mhs">
<input type="submit" name="simpan" value="simpan"/>
</form>
