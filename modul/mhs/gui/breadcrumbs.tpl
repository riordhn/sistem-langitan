<ol class="breadcrumb">
<li class="breadcrumb-item"><a class="text-success" rel="{$modul.NM_MODUL}" href="{$modul.PAGE}">{$modul.TITLE}</a></li>
{if isset($menu)}<li class="breadcrumb-item"> <a class="text-success" rel="{$modul.NM_MODUL}-{$menu.NM_MENU}" href="{$menu.PAGE}">{$menu.TITLE}</a></li>{/if}
{if isset($action)}<li class="breadcrumb-item active" aria-current="page"><span>{$action}</span></li>{/if}
</ol>
