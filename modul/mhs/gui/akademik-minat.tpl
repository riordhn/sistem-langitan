<div class="center_title_bar">MINAT PROGRAM STUDI</div>
<form method="post" action="akademik-minat.php">
<table>
	<tr>
    	<th>Nama Minat</th>
        <th>Pilihan</th>
    </tr>
{foreach $minat as $data}


	<tr>
    	<td>{$data.NM_PRODI_MINAT}</td>   
    	<td style="text-align:center"><input type="radio" name="pilih" value="{$data.ID_PRODI_MINAT}" {if $id_prodi_minat == $data.ID_PRODI_MINAT} checked="checked" {/if} /></td>
    </tr>
{foreachelse}
	<tr>
    	<td colspan="2">Belum ada peminatan di prodi anda</td>
    </tr>
{/foreach}
	{if isset($minat)}
	<tr>
    	<td colspan="2" style="text-align:right"><input type="submit" value="Simpan"  {if $id_prodi_minat != ''}disabled="disabled"{/if} /></td>
    </tr>
    {/if}
</table>
</form>

<b>* Minat Program Studi hanya bisa di isi sekali</b></br>
<b>* Jika ingin mengubah Minat Program Studi, silahkan hubungi Kepala Program Studi (KAPRODI)</b>