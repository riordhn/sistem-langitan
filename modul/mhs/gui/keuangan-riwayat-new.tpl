<div class="center_title_bar">RIWAYAT PEMBAYARAN MAHASISWA</div>
{$info}

<table class="table table-striped" style="width: 99%;">
    <tr>
        <th>Nama Bank</th>
        <th>No VA</th>
    </tr>
    {foreach $data_va_mhs as $data}
         <tr>
            <td class="center"><b>{$data.NAMA_BANK}</b></td>
            <td class="center"><b>{$data.NO_VA}</b></td>
        </tr>
    {foreachelse}
        <tr class="total ui-widget-content">
            <td class="text-center" colspan="8">
                <span style="color:red">
                    Va belum pernah dibuat atau sudah expired
                </span>
            </td>
        </tr>
    {/foreach}
</table>

<table class="table table-striped" style="width: 99%;">
    <tr>
        <th>No</th>
        <th>Semester</th>
        <th>Total Tagihan / Semester</th>
        <th>Total Terbayar</th>
        <th>Total Belum Terbayar</th>
        <th>Lunas</th>
        <th>Cetak Kartu</th>
        <!--<th>Detail Biaya</th>-->
        <th>Operasi</th>
    </tr>
    {foreach $data_pembayaran as $data}
        <tr >
           <td>{$data@index+1}</td>
            <td>{$data.NM_SEMESTER}<br/> ({$data.TAHUN_AJARAN})</td>
            <td class="center">{number_format($data.TOTAL_BESAR_BIAYA)}</td>
            <td class="center">{number_format($data.TOTAL_TERBAYAR)}</td>
            <td class="center">{number_format($data.TOTAL_BELUM_TERBAYAR)}</td>
            <td class="center">{if $data.TOTAL_BELUM_TERBAYAR>0} <b style="color:red">Belum</b>{else} <b style="color:green">Sudah</b>{/if}</td>
            <!--
            <td style="min-width:120px">
                {if count($data.DETAIL_BIAYA)>0}
                    <ul>
                        {$total_tagihan=0}
                        {foreach $data.DETAIL_BIAYA as $db}
                            <li style="font-size:0.8em;font-size:bold">{$db['NM_BIAYA']} <span style="float:right">{number_format($db['BESAR_BIAYA'])}</span></li>
                            {$total_tagihan=$total_tagihan+$db['BESAR_BIAYA']}
                        {/foreach}
                        <hr/>
                        <li style="font-size:1em;font-size:bold">TOTAL <span style="float:right">{number_format($total_tagihan)}</span></li>
                    </ul>
                {/if}
            </td>
            -->
            <td class="center">
            {foreach $data.DETAIL_BIAYA_CETAK as $data_detail}
                <span class="btn btn-sm btn-info disable-ajax m-1" onclick="window.open('{$cetak_kartu_link}{$data_detail.ID_TAGIHAN}');">Cetak Kartu {$data_detail.NM_BIAYA}</span>
            {foreachelse}
                <span style="color:red">
                    Data Kosong
                </span>
            {/foreach}
            </td>
            <td class="center">
                <span onclick="ajax_get_page_dialog('keuangan-riwayat-transaksi.php?cari={$nim_mhs}&id_tagihan_mhs={$data.ID_TAGIHAN_MHS}','#riwayat_transaksi_pembayaran')" class="btn btn-sm btn-secondary" style="margin:2px;">Riwayat</span><br/>
            </td>
        </tr>
    {foreachelse}
        <tr class="total ui-widget-content">
            <td class="center" colspan="8">
                <span style="color:red">
                    Data Pembayaran Kosong
                </span>
            </td>
        </tr>
    {/foreach}
</table>
<!--RIWAYAT TRANSAKSI MAHASISWA-->
<div id="riwayat_transaksi_pembayaran" title="Riwayat Transaksi Pembayaran"></div>
{literal}
<script>
  var base_url = window.location.protocol+'//'+window.location.hostname;
  var htmlLoading ='<div style="width: 100%;height:auto;padding:15px" align="center"><img height="80" src="'+base_url+'/img/spinner.gif" /></div>';
  function ajax_get_page_dialog(url,element){
    $.ajax({
        url: url,
        type: "get",
        beforeSend: function() {          
          $(element).dialog('open')
          $(element).html(
            htmlLoading
          );
        },
        success: function(data) {
          $(element).html(data);
        }
      });
  }
  $("#riwayat_transaksi_pembayaran").dialog({
    width: "80%",
    height: "550",
    modal: true,
    resizable: false,
    autoOpen: false
  });
</script>
{/literal}
