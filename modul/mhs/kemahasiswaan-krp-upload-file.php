<?php

// Test CVS
include 'config.php';
include ('includes/encrypt.php');

$status_message = '';

$id_pengguna = $user->ID_PENGGUNA;

$depan = time();
$belakang = strrev(time());

if ($request_method == 'POST')
{
    /*if (post('mode') == 'add_upload')
    {
        $id_krp_khp = (int)post('id_krp_khp', '');

        $db->Query("SELECT ID_KRP_KHP, ID_SEMESTER, ID_KEGIATAN_2, ID_BUKTI_FISIK, ID_MHS 
                        FROM KRP_KHP 
                        WHERE ID_KRP_KHP = '{$id_krp_khp}'");
        $krp_khp_set = $db->FetchAssoc();
        $id_semester = $krp_khp_set['ID_SEMESTER'];
        $id_kegiatan2 = $krp_khp_set['ID_KEGIATAN_2'];
        $id_bukti_fisik = $krp_khp_set['ID_BUKTI_FISIK'];
        $id_mhs = $krp_khp_set['ID_MHS'];

        // mulai action upload file
        $tipefile = $_FILES["file"]["type"];
        $nama_file_asli = $_FILES["file"]["name"];
        $filename = date('dmy_H_i_s') . '_' . $id_semester . '_' . $id_kegiatan2 . '_' . $id_bukti_fisik . '_' . $nama_file_asli;

        // Persiapkan direktori id_mhs jika belum ada
        if ( ! file_exists("../../files/kemahasiswaan/" . $id_mhs . "/"))
        {
            mkdir("../../files/kemahasiswaan/" . $id_mhs . "/", 0777, true);
        }

        if (file_exists("../../files/kemahasiswaan/". $id_mhs ."/" . $filename)) {
            $status_message .= 'Keterangan : ' . $filename . " Sudah Ada ";
            $status_upload = '0';
            $smarty->assign('status_message',$status_message);
            $smarty->assign('status_upload',$status_upload);
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], "../../files/kemahasiswaan/". $id_mhs ."/" . $filename);
            chmod("../../files/kemahasiswaan/". $id_mhs ."/" . $filename, 0755);
            $status_upload = '1';
        }


        if($status_upload == 1){
            $kueri = "INSERT INTO FILE_KRP_KHP (ID_KRP_KHP, NM_FILE_KRP_KHP, TIPE_FILE_KRP_KHP, IS_VERIFIED) VALUES ('".$id_krp_khp."', '".$filename."', '".$tipefile."', '0')";
            $result = $db->Query($kueri)or die("salah kueri : maintenance 101");
            if($db->numRows() > 0) {
                
                $smarty->assign('status_upload',$status_upload);

                echo '	<html>
							<head>
							<title>Upload Photo</title>
							<link rel="stylesheet" type="text/css" href="includes/iframe.css" />
							<script>
							function CloseWindow() {
							  timer=setTimeout("window.close()",100);
							  return true;
							}
							</script>
							</head>
							<body onload="CloseWindow();" onunload="window.opener.location.reload();">
							</body>
						</html>';

                //header('kemahasiswaan-krp-upload-file.php?id_krp_khp={$id_krp_khp}');
            }
        }

    }*/

    if(post('mode') == 'delete_file'){
    	$id_file_krp_khp = (int)post('id_file_krp_khp', '');

    	$id_mhs = $user->MAHASISWA->ID_MHS;

 		$db->Query("SELECT ID_FILE_KRP_KHP, NM_FILE_KRP_KHP, IS_VERIFIED 
                        FROM FILE_KRP_KHP
                        WHERE ID_FILE_KRP_KHP = '{$id_file_krp_khp}'");
        $file_krp_khp_set = $db->FetchAssoc();
        $nm_file_krp_khp = $file_krp_khp_set['NM_FILE_KRP_KHP'];
        $is_verified = $file_krp_khp_set['IS_VERIFIED'];

        if($is_verified == 1){
        	$status_hapus = 0;
        	$status_message = 'File Sudah Terverifikasi';

            $smarty->assign('status_hapus',$status_hapus);
        	$smarty->assign('status_message',$status_message);
        }
        else{
        	unlink("../../files/kemahasiswaan/". $id_mhs ."/" . $nm_file_krp_khp);
        	$status_hapus = 1;

        	$kueri = "DELETE FROM FILE_KRP_KHP WHERE ID_FILE_KRP_KHP = '{$id_file_krp_khp}'";
            $result = $db->Query($kueri)or die("salah kueri : maintenance 102");
            if($db->numRows() > 0) {
                
                $smarty->assign('status_hapus',$status_hapus);
                echo 'Hapus berhasil';
                //header('kemahasiswaan-krp-upload-file.php?id_krp_khp={$id_krp_khp}');
            }
        }
    }

}

if ($request_method == 'GET' or $request_method == 'POST')
{


	$id_krp_khp = (int)get('id_krp_khp', '');

    $db->Query("SELECT ID_KRP_KHP, NM_KRP_KHP, ID_MHS, PENYELENGGARA_KRP_KHP, WAKTU_KRP_KHP FROM KRP_KHP WHERE ID_KRP_KHP = '{$id_krp_khp}'");
    $krp_khp_set = $db->FetchAssoc();

    $id_mhs = $krp_khp_set['ID_MHS'];

    if($id_mhs == $user->MAHASISWA->ID_MHS){

	    $nama_kegiatan = $krp_khp_set['NM_KRP_KHP'];
	    $smarty->assign('nama_kegiatan', $nama_kegiatan);

	    $penyelenggara_kegiatan = $krp_khp_set['PENYELENGGARA_KRP_KHP'];
	    $smarty->assign('penyelenggara_kegiatan', $penyelenggara_kegiatan);

	    $waktu_kegiatan = $krp_khp_set['WAKTU_KRP_KHP'];
	    $smarty->assign('waktu_kegiatan', $waktu_kegiatan);

	    $smarty->assign('id_krp_khp', $id_krp_khp);

	    $file_set = $db->QueryToArray("SELECT fkk.ID_FILE_KRP_KHP, fkk.ID_KRP_KHP, fkk.NM_FILE_KRP_KHP, 
	                                        fkk.TIPE_FILE_KRP_KHP, fkk.IS_VERIFIED,
	                                        kk.ID_MHS, kk.NM_KRP_KHP 
	                                    FROM FILE_KRP_KHP fkk
	                                    JOIN KRP_KHP kk ON kk.ID_KRP_KHP = fkk.ID_KRP_KHP 
	                                    WHERE fkk.ID_KRP_KHP = '{$id_krp_khp}'");
	    $smarty->assignByRef('file_set', $file_set);


	$url = 'kemahasiswaan-krp-upload-file-iframe.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&id_krp_khp=' . $id_krp_khp . '&nama_kegiatan=' . $nama_kegiatan . '&penyelenggara_kegiatan=' . $penyelenggara_kegiatan . '&' . $belakang . '=' . $depan . $belakang) . '';
    $smarty->assign('URL', $url);

    }
    else{
    	echo "Anda Tidak Mempunyai Hak Akses Untuk File Ini! ".$id_mhs." session ".$user->MAHASISWA->ID_MHS;
    	exit();
    }

}

$smarty->display('kemahasiswaan-krp-upload-file.tpl');