<?php

require 'config.php';
require 'class/eva.class.php';
require 'function-tampil-informasi.php';

$eva = new eva($db, $user->ID_PENGGUNA);

$id_semester_isi = $eva->GetSemesterAktifEvaluasi();
$id_semester_aktif = $eva->GetSemesterAktif();

$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER='{$id_semester_isi}'");
$isi = $db->FetchAssoc();
$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER='{$id_semester_aktif}'");
$setelah = $db->FetchAssoc();

//Data Mahasiswa
$db->Query("
        SELECT M.*,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI ,PS.ID_FAKULTAS
        FROM MAHASISWA M
        JOIN PENGGUNA P ON M.ID_PENGGUNA=P.ID_PENGGUNA
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
        WHERE M.ID_PENGGUNA='{$user->ID_PENGGUNA}'
    ");
$data_mhs = $db->FetchAssoc();

//Cek Pembayaran
$cek_pembayaran = $eva->CekPembayaran($data_mhs['ID_MHS'], $id_semester_isi);

// Load Matakuliah Mahasiswa
$data_mata_kuliah = $eva->LoadKelasMahasiswa($id_semester_isi);

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
    if (isset($_GET)) {
        if (get('mode') == 'tampil') {
            $kode_kelas = explode('_', get('kelas'));
            $status_praktikum = $kode_kelas[2];
            $status_mkta = $kode_kelas[3];
            if (isset($_POST)) {
                if (post('mode') == 'simpan') {
                    //Simpan Evaluasi Perkuliahan
                    if (post('status_praktikum') == 0) {
                        for ($i = 1; $i <= post('jumlah_aspek'); $i++) {
                            $eva->SaveEvaluasiKuliah(1, post('kel_aspek' . $i), post('aspek' . $i), $data_mhs['ID_MHS'], $kode_kelas[1], $data_mhs['ID_PROGRAM_STUDI']
                                    , $data_mhs['ID_FAKULTAS'], $kode_kelas[0], $id_semester_isi, post('nilai' . $i));
                        }
                    }
                    //Simpan Evaluasi Praktikum
                    else if (post('status_praktikum') == 1) {
                        for ($i = 1; $i <= post('jumlah_aspek'); $i++) {
                            $eva->SaveEvaluasiKuliah(2, post('kel_aspek' . $i), post('aspek' . $i), $data_mhs['ID_MHS'], $kode_kelas[1], $data_mhs['ID_PROGRAM_STUDI']
                                    , $data_mhs['ID_FAKULTAS'], $kode_kelas[0], $id_semester_isi, post('nilai' . $i));
                        }
                    }
                }
            }
            // Jika Mata Kuliah Praktikum 
            if (($status_praktikum == 1 || $status_mkta == 5 || $status_mkta == 6) && get('kelas') != '') {
                if ($eva->CekPengisianEvaluasiKuliah(2, $kode_kelas[0], $kode_kelas[1], $data_mhs['ID_MHS'], $id_semester_isi) > 0) {
                    $smarty->assign('alert', alert_success("Pengisian Evaluasi Praktikum ini Sudah Berhasil Di Simpan, silahkan isi evaluasi lain..."));
                } else {
                    $smarty->assign('praktikum', 1);
                    $smarty->assign('data_aspek', $eva->LoadEvaluasiAspek(2));
                }
            }
            // Jika Mata Kuliah biasa 
            else if ($status_mkta == 0 && $status_praktikum != 1 && get('kelas') != '') {
                if ($eva->CekPengisianEvaluasiKuliah(1, $kode_kelas[0], $kode_kelas[1], $data_mhs['ID_MHS'], $id_semester_isi) > 0) {
                    $smarty->assign('alert', alert_success("Pengisian Evaluasi Perkuliahan ini Sudah Berhasil Di Simpan, silahkan isi evaluasi lain..."));
                } else {
                    $smarty->assign('praktikum', 0);
                    $smarty->assign('data_aspek', $eva->LoadEvaluasiAspek(1));
                }
            }
        }
    }
    // Jika Mahasiswa Belum Melakukan Pembayaran
    if ($cek_pembayaran) {
        $smarty->assign('alert', alert_error("Anda mempunyai tagihan pada semester ini , silahkan hubungi Direktorat Keuangan."));
    }
    // Jika Semua Evaluasi Telah Diisi    
    else if ($eva->CekPengisianSemuaEvaluasiKuliah($data_mhs['ID_MHS'], $id_semester_isi, count($data_mata_kuliah))) {
        if (count($data_mata_kuliah) == 0) {
            //$db->Query("UPDATE PENGAMBILAN_MK SET FLAGPBM=1 WHERE ID_MHS='{$data_mhs['ID_MHS']}' AND ID_SEMESTER='{$id_semester_isi}' AND (FLAGPBM=0 OR FLAGPBM IS NULL)");
            $smarty->assign('alert', alert_success("Mohon maaf Daftar Mata Kuliah/Pengajar mata kuliah masih Kosong / belum di masukkan..."));
        } else {
            $smarty->assign('alert', alert_success("Anda telah berhasil mengisi Evaluasi Perkuliahan {$isi['NM_SEMESTER']} {$isi['TAHUN_AJARAN']}, Pengisian Evaluasi Selanjutnya {$setelah['NM_SEMESTER']} {$setelah['TAHUN_AJARAN']}.Terima kasih atas partisipasinya..."));
        }
    } else {
        // Cek Status Pembukaan
        $status_buka_kuliah = $db->QuerySingle("SELECT STATUS_BUKA FROM EVALUASI_INSTRUMEN WHERE ID_EVAL_INSTRUMEN=1");
        $status_buka_praktikum = $db->QuerySingle("SELECT STATUS_BUKA FROM EVALUASI_INSTRUMEN WHERE ID_EVAL_INSTRUMEN=2");
        if ($status_buka_kuliah == 1 && $status_buka_praktikum == 1) {
            $data_mata_kuliah = $eva->LoadKelasMahasiswa($id_semester_isi);
            $smarty->assign('data_mk', $data_mata_kuliah);
        } else {
            $pengumuman = $db->QuerySingle("SELECT PENGUMUMAN FROM EVALUASI_INSTRUMEN GROUP BY PENGUMUMAN");
            if($pengumuman==''){
                $smarty->assign('alert', alert_error("Mohon Maaf proses pengisian evaluasi sudah tertutup. Terima Kasih"));
            }else{
                $smarty->assign('alert', alert_error($pengumuman));    
            }
        }
    }
    $smarty->assign('mode', get('mode'));
    $smarty->display('evkul.tpl');
} else {
    $smarty->display('session-expired.tpl');
}
?>
