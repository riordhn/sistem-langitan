<?php
require_once('config.php');
if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	$jkueri_bidikmisi = "
		<script type=\"text/javascript\">
			function edit_data(diedit){
				$('#tampil_'+diedit).hide();
				$('#edit_'+diedit).fadeIn();
			}
			function edit_batal(diedit){
				$('#edit_'+diedit).hide();
				$('#tampil_'+diedit).fadeIn();
			}
			function edit_simpan(diedit){
				var jenis = $('#jenis_'+diedit).val();
				var kegiatan = $('#kegiatan_'+diedit).val();
				var jumlah = $('#jumlah_'+diedit).val();

				$.ajax({
					type:'POST',
					url:'proses/_bidikmisiteman_simpan.php',
					data: 'aksi=simpanedit&jenis='+jenis+'&kegiatan='+kegiatan+'&jumlah='+jumlah+'&diedit='+diedit,
					success: function(data){
						alert(data);
						tampildata();
					}
				});
			}
			function tambah_data(){
				var nim = $('#baru_nim').val();

				var param = 'aksi=simpanbaru&nim='+nim;
				
				$.ajax({
					type:'POST',
					url:'proses/_bidikmisiteman_simpan.php',
					data: param,
					success: function(data){
						alert(data);
						tampildata();
					}
				});
			}
			function hapus_data(hapus){
				var param = 'aksi=hapus&dihapus='+hapus;

				$.ajax({
					type:'POST',
					url:'proses/_bidikmisiteman_simpan.php',
					data: param,
					success: function(data){
						alert(data);
						tampildata();
					}
				});
			}
			function tampildata(){
				$.ajax({
					type:'POST',
					url:'proses/_bidikmisiteman_simpan.php',
					data: 'aksi=tampil',
					success: function(data){
						$('#biodata-update').html(data);
					}
				});
			}
			tampildata();
		</script>
	";

	$kueri2 = "
	select count(*) 
	from aucc.mahasiswa a, aucc.sejarah_beasiswa b
	where a.id_mhs=b.id_mhs and a.id_pengguna='".$user->ID_PENGGUNA."' and b.beasiswa_aktif='1' and id_beasiswa in (1,2)
	";
	$is_bidikmisi = 0;
	$result2 = $db->Query($kueri2)or die("salah kueri 76 : ");
	while($r2 = $db->FetchRow()) {
		$is_bidikmisi = $r2[0];
	}
	if($is_bidikmisi=='1') {
		$smarty->assign('jkueri_bidikmisi', $jkueri_bidikmisi);
	}else{
		$smarty->assign('jkueri_bidikmisi', 'Bukan Mahasiswa Bidik Misi');
	}

	$smarty->display('bidikmisiteman-data.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>
