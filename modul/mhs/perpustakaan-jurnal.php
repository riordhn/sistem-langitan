<?php

include('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

include 'conf_perpus.php';
include 'proses/info_buku.class.php';

$journal = new info_buku($db_mysql);

if(get('mode')!=''){
    if(get('mode')=='data'){
       $smarty->assign('data_journal', $journal->load_data_journal(get('tempat')));
    }
}

$smarty->assign('data_list_journal', $journal->load_list_journal());
$smarty->display("perpustakaan-jurnal.tpl");
?>