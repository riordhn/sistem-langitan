<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	$jkueri_outbox = '
	<script type="text/javascript">
		function tampildata(){
			$.ajax({
				type:"POST",
				url:"proses/_dosen-outbox_hapus.php",
				data: "aksi=tampil",
				success: function(data){
					$("#outbox_hapus").html(data);
				}
			});
		}
		tampildata();

		function hapus_kirim(f){
			$.ajax({
				type:"POST",
				url:"proses/_dosen-outbox_hapus.php",
				data: $("#"+f).serialize(),
				success: function(data){
					alert(data);
					tampildata();
				}
			});
		}
		function hapustunggal_kirim(f){
			$.ajax({
				type:"POST",
				url:"proses/_dosen-outbox_hapus.php",
				data: "aksi=hapustunggal&ygdihapus="+f,
				success: function(data){
					alert(data);
					tampildata();
				}
			});
		}
		function bacapesan(f){
			$.ajax({
				type:"POST",
				url:"proses/_dosen-outbox_hapus.php",
				data: "aksi=bacapesan&ygdibaca="+f,
				success: function(data){
					$("#outbox_detail").html(data);
				}
			});
		}
	</script>
	';
	$smarty->assign('jkueri_outbox', $jkueri_outbox);

	$smarty->display('dosen-outbox.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>
