<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
// Yudi Sulistya, 02 Aug 2013 (tablesorter)
echo '
<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[0,1]], widgets: ["zebra"]
		}
	);
}
);
</script>
';

	$tampil_data = '
	<br>
	<table width="850" id="myTable" class="tablesorter">
	<thead>
	<tr>
		<th>Semester</th>
		<th>Status</th>
		<th>No. SK.</th>
		<th>Tgl SK.</th>
		<th>Keterangan</th>
	</tr>
	</thead>
	<tbody>
	';

	$kueri = "SELECT NM_STATUS_PENGGUNA, THN_AKADEMIK_SEMESTER, NM_SEMESTER, ADMISI.NO_SK, TGL_SK, TGL_USULAN, TGL_APV, KETERANGAN, KURUN_WAKTU, ALASAN
				FROM MAHASISWA 
				LEFT JOIN PENGGUNA ON MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA   
				LEFT JOIN PROGRAM_STUDI ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS 
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				LEFT JOIN ADMISI ON MAHASISWA.ID_MHS = ADMISI.ID_MHS
				LEFT JOIN SEMESTER ON ADMISI.ID_SEMESTER = SEMESTER.ID_SEMESTER
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = ADMISI.STATUS_AKD_MHS 
				WHERE MAHASISWA.ID_PENGGUNA = '".$user->ID_PENGGUNA."' AND STATUS_APV = 1";
	
	$result = $db->Query($kueri)or die("salah kueri : 35");
	while($r = $db->FetchAssoc()) {
		$tampil_data .= '
		<tr>
			<td>'.$r['THN_AKADEMIK_SEMESTER']. ' / ' . $r['NM_SEMESTER'] .'</td>
			<td>'.$r['NM_STATUS_PENGGUNA'].'</td>
			<td>'.$r['NO_SK'].'</td>
			<td>'.$r['TGL_SK'].'</td>
			<td>'.$r['ALASAN'].'</td>
		</tr>
		';
	}

		$tampil_data .= '
		</tbody>
		</table>
		';
	
	$smarty->assign('tampil_data', $tampil_data);

	$smarty->display('akademik-status.tpl');
}else {
	$smarty->display('session-expired.tpl');
}
?>
