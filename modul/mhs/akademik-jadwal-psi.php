<?php
require('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

$db2 = new MyOracle();

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
// Yudi Sulistya, 02 Aug 2013 (tablesorter)
echo '
<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[0,0],[2,0]], widgets: ["zebra"]
		}
	);
}
);
</script>
';

	// ambil prodi
	$id_prodi="";
	$kueri = "select a.ID_PROGRAM_STUDI,b.id_fakultas from mahasiswa a, program_studi b where a.id_program_studi=b.id_program_studi and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : 26");
	while($r = $db->FetchRow()) {
		$id_prodi = $r[0];
		$id_fakul = $r[1];
	}
	// ambil semester_aktif
	$sem_aktif=""; $nm_semester = "";
	$kueri = "select id_semester, nm_semester,tahun_ajaran from semester where STATUS_AKTIF_SEMESTER='True' order by id_semester desc";
	$result = $db->Query($kueri)or die ("salah kueri : 34");
	while($r = $db->FetchRow()) {
		$sem_aktif = $r[0];
		$nm_semester = $r[1]." ".$r[2];
	}

	$tampil_data = '
	<p><center><h2>Jadwal Kuliah Semester '.$nm_semester.'</center></h2></p>
	<table width="850" id="myTable" class="tablesorter">
	<thead>
		<tr>
			<th width="300">Mata Kuliah</th>
			<th width="50">SKS</th>
			<th width="50">Kelas</th>
			<th width="100">Jadwal</th>
			<th width="50">Ruang</th>
			<th width="300">PJMK</th>
		</tr>
		</thead>
		<tbody>
	';

	// nama_mk sks kelas jadwal(hari+jam) JMMA ruangan petugas
	$kueri = "";
		// psikologi
		$kueri = "
		select a.kd_mata_kuliah, a.nm_mata_kuliah, d.kredit_semester, d.id_mata_kuliah, d.id_kurikulum_mk, c.no_kelas_mk, c.id_kelas_mk,
		c.terisi_kelas_mk, c.kapasitas_kelas_mk
		from mata_kuliah a, krs_prodi b, kelas_mk c, kurikulum_mk d, jadwal_kelas e
		where c.id_kurikulum_mk=d.id_kurikulum_mk and a.id_mata_kuliah=d.id_mata_kuliah and c.id_kelas_mk=b.id_kelas_mk and c.id_kelas_mk=e.id_kelas_mk
		and b.id_semester='".$sem_aktif."' and b.id_program_studi='".$id_prodi."'
		";
	$result = $db->Query($kueri)or die("salah kueri 141 ");
	$row="";$i=0;
	while($r = $db->FetchRow()) {
	$i++;
		$nm_kelas = "";
		$result2 = $db2->Query("select nama_kelas from nama_kelas where id_nama_kelas='".$r[5]."'")or die("salah kueri 144 : ");
		while($r2 = $db2->FetchRow()) {
			$nm_kelas = $r2[0];
		}

		// AMBIL JADWAL
		$jamnya = ""; $harinya = ""; $ruangannya = "";
		$result2 = $db2->Query("select a.jam_mulai||':'||menit_mulai||' - '||jam_selesai||':'||menit_selesai as nm_jadwal_jam,b.id_jadwal_hari,b.id_jadwal_jam, b.id_ruangan from aucc.jadwal_jam a, aucc.jadwal_kelas b 
		where a.id_jadwal_jam=b.id_jadwal_jam and b.id_kelas_mk='".$r[6]."'")or die("salah kueri 151 : ");
		while($r2 = $db2->FetchRow()) {
			if($r2[1]=='1') {
				$harinya = "Minggu";
			}else if($r2[1]=='2') {
				$harinya = "Senin";
			}else if($r2[1]=='3') {
				$harinya = "Selasa";
			}else if($r2[1]=='4') {
				$harinya = "Rabu";
			}else if($r2[1]=='5') {
				$harinya = "Kamis";
			}else if($r2[1]=='6') {
				$harinya = "Jumat";
			}else if($r2[1]=='7') {
				$harinya = "Sabtu";
			}
			$jamnya = $r2[0];
			$ruangannya = $r2[3];
		}

		$ruangan=""; $ruangan_penjaga="";
		// ambil ruangan
		$kueri2 = "select nm_ruangan, id_penanggung_jawab from ruangan where id_ruangan='".$ruangannya."'";
		$result2 = $db2->Query($kueri2)or die("salah kueri 175 ");
		while($r2 = $db2->FetchRow()) {
			$ruangan = $r2[0];
			$ruangan_penjaga = $r2[1];
		}

		// ambil pjmk
		$pjmk="";
		$kueri2 = "select c.nm_pengguna from pengampu_mk a, dosen b, pengguna c where a.id_dosen=b.id_dosen and b.id_pengguna=c.id_pengguna and a.id_kelas_mk='".$r[6]."'";
		$result2 = $db2->Query($kueri2)or die("salah kueri 184 ");
		while($r2 = $db2->FetchRow()) {
			$pjmk = $r2[0];
		}

		$tampil_data .= '
		<tr>
			<td align="left">'.$r[0].' - '.$r[1].'</td>
			<td align="center">'.$r[2].'</td>
			<td align="center">'.$nm_kelas.'</td>
			<td align="center">'.$harinya.'<br>'.$jamnya.'</td>
			<td align="center">'.$ruangan.'</td>
			<td align="left">'.$pjmk.'</td>
		</tr>
		';
	}
		$tampil_data .= '
		</tbody>
		</table>
		';

	$smarty->assign('tampil_data', $tampil_data);

	$smarty->display('akademik-jadwal.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>
