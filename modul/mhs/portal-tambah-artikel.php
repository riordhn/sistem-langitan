<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

	$id_pengguna = $user->ID_PENGGUNA;
	$smarty->assign('id_pengguna', $id_pengguna);
	$smarty->display('portal-tambah-artikel.tpl');
?>