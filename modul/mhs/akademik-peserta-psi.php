<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

$db2 = new MyOracle();

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	// ambil id_mhs
	$kueri = "select id_mhs from mahasiswa where id_pengguna = '".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : 23");
	while($r = $db->FetchArray()) {
		$id_mhs = $r[0];
	}
	
	// Cek data
	$cek = "
	select count(*) as ada
	from semester b, pengambilan_mk d
	where d.id_semester=b.id_semester and b.status_aktif_semester='True' and d.id_mhs='".$id_mhs."' and (d.status_pengambilan_mk!='4' or d.status_pengambilan_mk is null) and d.status_apv_pengambilan_mk='1'
	";
	$result = $db->Query($cek)or die("salah kueri : 11");
	$d = $db->FetchArray();
	$sdh = $d[0];
	$smarty->assign('ada', $sdh );
	
	// Data dari KRS
	$kueri = "
	select a.kd_mata_kuliah||' - '||a.nm_mata_kuliah,c.kapasitas_kelas_mk,c.id_kelas_mk,g.nama_kelas, f.id_ruangan,f.id_jadwal_hari,f.id_jadwal_jam
	from mata_kuliah a, semester b, kelas_mk c, pengambilan_mk d, kurikulum_mk e, jadwal_kelas f, nama_kelas g
	where c.id_kelas_mk=d.id_kelas_mk and d.id_semester=b.id_semester and c.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and c.id_kelas_mk=f.id_kelas_mk
	and g.id_nama_kelas=c.no_kelas_mk
	and b.status_aktif_semester='True' and d.id_mhs='".$id_mhs."' and (d.status_pengambilan_mk!='4' or d.status_pengambilan_mk is null) and d.status_apv_pengambilan_mk='1'
	";
	$result = $db->Query($kueri)or die("salah kueri : 35");
	$isi_tabel = '';$row="";$i=0;
	while($r = $db->FetchArray()) {
	$i++;
		// ambil peserta mk
		//$kueri2 = "select count(*) from pengambilan_mk where id_kelas_mk='".$r[2]."'";
		$kueri2 = "
		select count(*)
		from pengambilan_mk a, semester b 
		where a.id_semester=b.id_semester and b.status_aktif_semester='True'
		and a.status_apv_pengambilan_mk='1' and a.status_pengambilan_mk!='4' and a.id_kelas_mk='".$r[2]."'
		";
		$result2 = $db2->Query($kueri2)or die("salah kueri : 46");
		$peserta='0';
		while($r2 = $db2->FetchArray()) {
			$peserta = $r2[0];
		}

		$kueri2 = "select nm_jadwal_hari from jadwal_hari where id_jadwal_hari='".$r[5]."'";
		$result2 = $db2->Query($kueri2)or die("salah kueri : 47");
		$hari='0';
		while($r2 = $db2->FetchArray()) {
			$hari = $r2[0];
		}
		
		$kueri2 = "select jam_mulai||':'||menit_mulai||' - '||jam_selesai||':'||menit_selesai from jadwal_jam where id_jadwal_jam='".$r[6]."'";
		$result2 = $db2->Query($kueri2)or die("salah kueri : jam");
		$jam='0';
		while($r2 = $db2->FetchArray()) {
			$jam = $r2[0];
		}

		$kueri2 = "select nm_ruangan from ruangan where id_ruangan='".$r[4]."'";
		$result2 = $db2->Query($kueri2)or die("salah kueri : 48");
		$ruangan='0';
		while($r2 = $db2->FetchArray()) {
			$ruangan = $r2[0];
		}

		$isi_tabel .= '
		<tr>
			<td><span>'.$r[0].'</span></td>
			<td align="center">'.$r[3].'</td>
			<td align="center">'.$hari.' '.$jam.'</td>
			<td align="center">'.$ruangan.'</td>
			<td align="center">'.$r[1].'</td>
			<td align="center"><a style="text-decoration:underline;cursor:pointer;" onclick="window.open(\'/modul/mhs/proses/_akademik-peserta_det.php?mk='.$r[2].'\',\'baru2\');">'.$peserta.'</a></td>
		</tr>
		';
	}

	// ambil semester aktif
	$kueri = "
		select thn_akademik_semester,nm_semester from semester where status_aktif_semester='True'
	";
	$result = $db->Query($kueri)or die("salah kueri 35 : ");
	$sem=""; $tahun="";
	while($r = $db->FetchArray()) {
		$tahun = $r[0]."/".($r[0]+1);
		$sem = $r[1];
	}
	$semester = $sem." ".$tahun." ";

	$smarty->assign('isitabel', $isi_tabel);
	$smarty->assign('semes', $semester);

	$smarty->display('akademik-peserta-psi.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>