<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	$tampil_data = '
	<br>
	<table>
	<tr>
		<th align=center>Kegiatan</th>
		<th align=center>Tanggal Mulai</th>
		<th align=center>Tanggal Selesai</th>
	</tr>
	';

	$kueri = "
	select c.nm_kegiatan, a.tgl_mulai_jks, a.tgl_selesai_jks
	from jadwal_kegiatan_semester a, semester b, kegiatan c
	where a.id_semester=b.id_semester and a.id_kegiatan=c.id_kegiatan and b.status_aktif_semester='True' 
	order by c.id_kegiatan
	";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchArray()) {
		$tampil_data .= '
		<tr>
			<td>'.$r[0].'</td>
			<td>'.$r[1].'</td>
			<td>'.$r[2].'</td>
		</tr>
		';
	}

	$tampil_data .= '
	</table>
	';

	$smarty->assign('tampil_data', $tampil_data);

	$smarty->display('akademik-kalender.tpl');
}else {
	$smarty->display('session-expired.tpl');
}
?>
