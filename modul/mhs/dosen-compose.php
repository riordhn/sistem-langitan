<?php

require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	$jkueri_compose = '
	<script type="text/javascript">
		function tampildata(){
			$.ajax({
				type:"POST",
				url:"proses/_dosen-compose_simpan.php",
				data: "aksi=tampil",
				success: function(data){
					$("#compose_buat").html(data);
				}
			});
		}
		tampildata();

		function compose_simpan (f){
			$.ajax({
				type:"POST",
				url:"proses/_dosen-compose_simpan.php",
				data: $("#"+f).serialize(),
				success: function(data){
					alert(data);
					tampildata();
				}
			});
		}
		function tujuan(v){
			$.ajax({
				type:"POST",
				url:"proses/_dosen-compose_simpan.php",
				data: "aksi=kepada&jenis="+v,
				success: function(data){
					$("#kepada2").html(data);
				}
			});
		}
	</script>
	';
	$smarty->assign('jkueri_compose', $jkueri_compose);

	$smarty->display('dosen-compose.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>