<?php
include 'config.php';
include 'function-tampil-informasi.php';

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}
/*

	// ambil nim & password
	$mhs_pass=""; $mhs_nim="";
	$kueri = "select b.nim_mhs, a.se1, b.id_mhs, c.id_jenjang from pengguna a, mahasiswa b, program_studi c 
				where a.id_pengguna=b.id_pengguna and a.id_pengguna='".$user->ID_PENGGUNA."'
				and c.id_program_studi = b.id_program_studi";
	$result = $db->Query($kueri)or die("salah kueri 6 : ");
	while($r = $db->FetchRow()) {
		$mhs_pass = $r[2];
		$mhs_nim = $r[0];
		$id_mhs = $r[3];
	}

	if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	
	
	//cicilan
	$kueri = "SELECT SUM(PEMBAYARAN.BESAR_PEMBAYARAN) AS JML FROM PEMBAYARAN
									JOIN TAGIHAN ON TAGIHAN.ID_TAGIHAN = PEMBAYARAN.ID_TAGIHAN
									LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = TAGIHAN.ID_MHS
									--LEFT JOIN DETAIL_BIAYA ON DETAIL_BIAYA.ID_DETAIL_BIAYA = TAGIHAN.ID_DETAIL_BIAYA
									LEFT JOIN BIAYA ON BIAYA.ID_BIAYA = TAGIHAN.ID_BIAYA
									WHERE TGL_BAYAR IS NOT NULL 
									AND TAGIHAN.ID_BIAYA = 115 
									AND NIM_MHS = '$mhs_nim'";
	$result = $db->Query($kueri)or die("salah kueri 1 : ");
	$cicilan = $db->FetchRow();	
	
	
	//tarif
	$kueri = "SELECT PERIODE_WISUDA.BESAR_BIAYA FROM PERIODE_WISUDA
							JOIN TARIF_WISUDA ON TARIF_WISUDA.ID_TARIF_WISUDA = PERIODE_WISUDA.ID_TARIF_WISUDA AND TARIF_WISUDA.ID_PERGURUAN_TINGGI = {$id_pt_user}
							LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
							LEFT JOIN MAHASISWA ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
							WHERE mahasiswa.NIM_MHS = '$mhs_nim' AND periode_wisuda.STATUS_AKTIF = 1";
	$result = $db->Query($kueri)or die("salah kueri 2 : ");
	$tarif = $db->FetchRow();	
	
		if($cicilan[0] == 0){
			$tagihan = $tarif[0];
			$tampil_data = "<table width='300'>
								<tr>
									<td>Tarif Wisuda</td>
									<td>:</td>
									<td style='text-align:right'>Rp. ".number_format($tarif[0],0,',','.')."</td>
								</tr>
								<tr>
									<td>Cicilan Wisuda</td>
									<td>:</td>
									<td style='text-align:right'>Rp. ".number_format($cicilan[0],0,',','.')."</td>
								</tr>
								<tr>
									<td><b>Tagihan Wisuda</b></td>
									<td><b>:</b></td>
									<td style='text-align:right'><b>Rp. ".number_format($tagihan,0,',','.')."</b></td>
								</tr>
							</table>
							</br>* Tagihan = Tarif - Cicilan";
			
		}else{
			if($cicilan[0]>$tarif[0]){
				$tagihan =  0;
			}else{
				$tagihan = $tarif[0] - $cicilan[0];
			}
			$tampil_data = "<table width='300'>
								<tr>
									<td>Tarif Wisuda</td>
									<td>:</td>
									<td style='text-align:right'>Rp. ".number_format($tarif[0],0,',','.')."</td>
								</tr>
								<tr>
									<td>Cicilan Wisuda</td>
									<td>:</td>
									<td style='text-align:right'>Rp. ".number_format($cicilan[0],0,',','.')."</td>
								</tr>
								<tr>
									<td><b>Tagihan Wisuda</b></td>
									<td><b>:</b></td>
									<td style='text-align:right'><b>Rp. ".number_format($tagihan,0,',','.')."</b></td>
								</tr>
							</table>
							</br>* Tagihan = Tarif - Cicilan";
		}
		
		//pembayaran wisuda
	//$kueri = "SELECT THN_AKADEMIK_SEMESTER, NM_SEMESTER, NM_BANK, NAMA_BANK_VIA, NM_PERIODE_WISUDA, 
	$kueri = "SELECT THN_AKADEMIK_SEMESTER, NM_SEMESTER, NM_BANK, NAMA_BANK_VIA, 
				PEMBAYARAN_WISUDA.BESAR_BIAYA, TGL_BAYAR, NO_TRANSAKSI
				FROM PEMBAYARAN_WISUDA
				JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS
				JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PEMBAYARAN_WISUDA.ID_SEMESTER
				JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PEMBAYARAN_WISUDA.ID_PERIODE_WISUDA
				JOIN TARIF_WISUDA ON TARIF_WISUDA.ID_TARIF_WISUDA = PERIODE_WISUDA.ID_TARIF_WISUDA AND TARIF_WISUDA.ID_PERGURUAN_TINGGI = {$id_pt_user}
				JOIN BANK ON BANK.ID_BANK = PEMBAYARAN_WISUDA.ID_BANK
				JOIN BANK_VIA ON BANK_VIA.ID_BANK_VIA = PEMBAYARAN_WISUDA.ID_BANK_VIA
				WHERE NIM_MHS = '$mhs_nim'";
	$result = $db->Query($kueri)or die("salah kueri 3 : ");
	$tarif = $db->FetchArray();	
		
/*
		$tampil_data = $tampil_data . "<table width='80%'>
								<tr>
									<th>Wisuda</th>
									<th>Semester</th>
									<th>Besar Biaya</th>
									<th>Nama Bank</th>
									<th>Via Bank</th>
									<th>Tanggal Bayar</th>
									<th>No Transaksi</th>
								</tr>
								<tr>
									<td>$tarif[NM_PERIODE_WISUDA]</td>
									<td>$tarif[THN_AKADEMIK_SEMESTER] ($tarif[NM_SEMESTER])</td>
									<td>Rp. ".number_format($tarif['BESAR_BIAYA'],0,',','.')."</td>
									<td>$tarif[NM_BANK]</td>
									<td>$tarif[NAMA_BANK_VIA]</td>
									<td>$tarif[TGL_BAYAR]</td>
									<td>$tarif[NO_TRANSAKSI]</td>
								</tr>
							</table>";

		$tampil_data = $tampil_data . "<table width='80%'>
								<tr>
									<th>Semester</th>
									<th>Besar Biaya</th>
									<th>Nama Bank</th>
									<th>Via Bank</th>
									<th>Tanggal Bayar</th>
									<th>No Transaksi</th>
								</tr>
								<tr>
									<td>$tarif[THN_AKADEMIK_SEMESTER] ($tarif[NM_SEMESTER])</td>
									<td>Rp. ".number_format($tarif['BESAR_BIAYA'],0,',','.')."</td>
									<td>$tarif[NM_BANK]</td>
									<td>$tarif[NAMA_BANK_VIA]</td>
									<td>$tarif[TGL_BAYAR]</td>
									<td>$tarif[NO_TRANSAKSI]</td>
								</tr>
							</table>";
		
	$smarty->assign('tampil_data', $tampil_data);
	$smarty->display('keuangan-tagihan-wisuda.tpl');
}else {
	$smarty->display('session-expired.tpl');
}
*/
$smarty->assign('informasi',alert_success("Halaman sedang masa perbaikan"));
$smarty->display('keuangan-tagihan-wisuda.tpl');