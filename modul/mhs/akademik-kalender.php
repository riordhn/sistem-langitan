<?php
// start load
$times = explode(" ", microtime());
$start = $times[1] + $times[0];
//==============================================================================

require('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
// Yudi Sulistya, 27-07-2012 (untuk menampilkan kalendar akademik fakultas)
// Updated 12-02-2013


$mhs_nim = $user->USERNAME;
$queri = "
select c.id_fakultas
from mahasiswa a, program_studi b, fakultas c
where a.nim_mhs='".$mhs_nim."' and a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas
";
$result = $db->Query($queri)or die("Fakultas?");
while($r = $db->FetchArray()) {
	$id_fakultas = $r[0];
}

	$tampil_data = '
	<br>
	<table width="850" id="myTable" class="tablesorter">
	<thead>
	<tr>
		<th width="550">Kegiatan</th>
		<th width="150">Tanggal Mulai</th>
		<th width="150">Tanggal Selesai</th>
	</tr>
	</thead>
	<tbody>
	';
/*
	$kueri = "
	select c.nm_kegiatan, a.tgl_mulai_jks, a.tgl_selesai_jks
	from jadwal_kegiatan_semester a, semester b, kegiatan c
	where a.id_semester=b.id_semester and a.id_kegiatan=c.id_kegiatan and b.status_aktif_semester='True' 
	order by c.id_kegiatan
	";
*/

	// KEGIATAN PER FAKULTAS

	/*$kueri = "
	SELECT KG.NM_KEGIATAN, TO_CHAR(JSF.TGL_MULAI_JSF, 'DD MON YYYY'), TO_CHAR(JSF.TGL_SELESAI_JSF, 'DD MON YYYY')
	FROM JADWAL_SEMESTER_FAKULTAS JSF
	LEFT JOIN KEGIATAN KG ON JSF.ID_KEGIATAN=KG.ID_KEGIATAN
	LEFT JOIN SEMESTER SMT ON JSF.ID_SEMESTER=SMT.ID_SEMESTER
	WHERE SMT.STATUS_AKTIF_SEMESTER='True' and JSF.ID_FAKULTAS='$id_fakultas'
	";*/


	//KEGIATAN SECARA UMUM

	$kueri = "
	SELECT KG.NM_KEGIATAN, TO_CHAR(JKS.TGL_MULAI_JKS, 'DD MON YYYY'), TO_CHAR(JKS.TGL_SELESAI_JKS, 'DD MON YYYY')
	FROM JADWAL_KEGIATAN_SEMESTER JKS
	LEFT JOIN KEGIATAN KG ON JKS.ID_KEGIATAN=KG.ID_KEGIATAN
	LEFT JOIN SEMESTER SMT ON JKS.ID_SEMESTER=SMT.ID_SEMESTER
	WHERE SMT.STATUS_AKTIF_SEMESTER='True' AND KG.ID_PERGURUAN_TINGGI = {$id_pt_user}
	ORDER BY JKS.TGL_MULAI_JKS DESC
	";	


	$result = $db->Query($kueri)or die("salah kueri : 35");
	$row="";$i=0;
	while($r = $db->FetchArray()) {
	$i++;
		$tampil_data .= '
		<tr>
			<td>'.$r[0].'</td>
			<td align="center">'.$r[1].'</td>
			<td align="center">'.$r[2].'</td>
		</tr>
		';
	}

	$tampil_data .= '
		</tbody>
	</table>
	';

	/*
	$tampil_data .= '
	</table>
	<input type=button name="roster" value="Roster Akademik" onclick="window.open(\".$base_url."'modul/dashboard/roster/index.php\',\'baru2\');">
	';
	*/

	$smarty->assign('tampil_data', $tampil_data);

	$smarty->display('akademik-kalender.tpl');
}else {
	$smarty->display('session-expired.tpl');
}