<?php

include 'config.php';
include '../keuangan/class/history.class.php';

$history = new history($db);
$id_tagihan_mhs=get('id_tagihan_mhs');
$cari = $db->QuerySingle("SELECT NIM_MHS FROM MAHASISWA WHERE ID_MHS IN (SELECT ID_MHS FROM TAGIHAN_MHS WHERE ID_TAGIHAN_MHS='{$id_tagihan_mhs}')");

$data_tagihan_biaya = $history->load_riwayat_pembayaran_biaya($id_tagihan_mhs);
foreach ($data_tagihan_biaya as $d) {
    $arr = [
        'SEMESTER' => $d['NM_SEMESTER'] . ' ' . $d['TAHUN_AJARAN'],
        'NAMA_BIAYA' => $d['NM_BIAYA'],
        'BESAR_TAGIHAN' => $d['BESAR_BIAYA'],
        'BESAR_TERBAYAR' => $d['TOTAL_TERBAYAR'],
        'IS_LUNAS' =>  $d['BESAR_BIAYA']==$d['TOTAL_TERBAYAR']&&$d['TOTAL_TERBAYAR']>0?1:0,
    ];
    $data_tagihan_pembayaran[] = $arr;
}
$smarty->assign('cari', $cari);
$smarty->assign('id_pt', $id_pt);
$smarty->assign('jenis', 'mhs');
$smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim($cari), getenv('ID_PT')));
$smarty->assign('data_riwayat_pembayaran', $history->load_riwayat_transaksi_pembayaran($id_tagihan_mhs));
$smarty->assign('data_tagihan_pembayaran', $data_tagihan_pembayaran);
$smarty->display('keuangan-riwayat-transaksi.tpl');
?>
