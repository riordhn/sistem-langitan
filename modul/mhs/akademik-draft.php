<?php
require('config.php');
include 'class/eva.class.php';
include 'class/upload.class.php';
$eva = new eva($db, $user->ID_PENGGUNA);
$upload = new upload($db, $user->ID_PENGGUNA);
// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
} 

// Set sementara oleh Fathoni : (29/12/2015)
// Ada pesan salah kueri 6
/*echo '<div class="center_title_bar">Draft Ijazah</div><p>Fitur sedang tidak aktif.</p>'; exit();*/



	// ambil nim & password
	$mhs_pass=""; $mhs_nim="";
	$kueri = "select b.nim_mhs, a.password_hash, a.se1, b.id_mhs, c.id_jenjang from pengguna a, mahasiswa b, program_studi c where a.id_pengguna=b.id_pengguna and a.id_pengguna='".$user->ID_PENGGUNA."' and c.id_program_studi = b.id_program_studi";
	$result = $db->Query($kueri)or die("salah kueri 6 : ");
	while($r = $db->FetchRow()) {
		$mhs_pass = $r[2];
		$mhs_nim = $r[0];
		$id_mhs = $r[3];
		$id_jenjang = $r[4];
	}
 if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	
	$error = "";
	
	$kueri = "SELECT COUNT(*) FROM PENGAJUAN_WISUDA WHERE ID_MHS = '$id_mhs'";
	$result = $db->Query($kueri)or die("salah kueri 2 : ");
	$r = $db->FetchRow();
	if($r[0] == 1){
	
	//evaluasi wisudawan
	if($id_jenjang != 2 and $id_jenjang != 3){
		/*$kueri = "select count(*) from hasil_ev_wisuda where nim = '$mhs_nim'";
		$result = $db->Query($kueri)or die("salah kueri 7 : ");
		$r = $db->FetchRow();
		if($r[0] == 0){
			$error  = $error . "</br><b>-  Harap Mangisi Evaluasi Wiusuda</b></br>";
		}*/
		
		if (!$eva->CekPengisianEvaluasiWisuda($id_mhs)) {
            $error = $error . "</br><b>-  Harap Mengisi Evaluasi Wisuda</b></br>";
        }
	
		
	}
	
	//if (!$upload->CheckStatusUpload($id_mhs, $id_jenjang)) {
   //         $error = $error . '<b>-  Upload File Skripsi, Tesis, Tugas Akhir atau Desertasi belum dilakukan</b></br>';
    //} cc
		
	//biodata
	if($id_jenjang=='2' or $id_jenjang=='3') {
		$kueri = "SELECT COUNT(*) AS MHS FROM MAHASISWA WHERE NIM_MHS = '$mhs_nim'
				AND MOBILE_MHS IS NOT NULL AND NM_AYAH_MHS IS NOT NULL
				AND PENDIDIKAN_AYAH_MHS IS NOT NULL AND PEKERJAAN_AYAH_MHS IS NOT NULL AND ALAMAT_AYAH_MHS IS NOT NULL
				AND NM_IBU_MHS IS NOT NULL AND PENDIDIKAN_IBU_MHS IS NOT NULL AND PEKERJAAN_IBU_MHS IS NOT NULL
				AND ALAMAT_IBU_MHS IS NOT NULL AND PENGHASILAN_ORTU_MHS IS NOT NULL
				AND LAHIR_KOTA_MHS IS NOT NULL AND LAHIR_PROP_MHS IS NOT NULL AND ASAL_KOTA_MHS IS NOT NULL
				AND ASAL_PROV_MHS IS NOT NULL 
				AND ALAMAT_AYAH_MHS_KOTA IS NOT NULL AND ALAMAT_AYAH_MHS_PROV IS NOT NULL 
				AND ALAMAT_IBU_MHS_KOTA IS NOT NULL AND ALAMAT_IBU_MHS_PROV IS NOT NULL AND ALAMAT_ASAL_MHS IS NOT NULL
				AND ALAMAT_ASAL_MHS_KOTA IS NOT NULL AND ALAMAT_ASAL_MHS_PROV IS NOT NULL";
	}else if($id_jenjang=='9'){
		$kueri = "SELECT COUNT(*) AS MHS FROM MAHASISWA WHERE NIM_MHS = '$mhs_nim'
				AND MOBILE_MHS IS NOT NULL AND NM_AYAH_MHS IS NOT NULL
				AND PENDIDIKAN_AYAH_MHS IS NOT NULL AND PEKERJAAN_AYAH_MHS IS NOT NULL AND ALAMAT_AYAH_MHS IS NOT NULL
				AND NM_IBU_MHS IS NOT NULL AND PENDIDIKAN_IBU_MHS IS NOT NULL AND PEKERJAAN_IBU_MHS IS NOT NULL
				AND ALAMAT_IBU_MHS IS NOT NULL AND PENGHASILAN_ORTU_MHS IS NOT NULL AND NEM_PELAJARAN_MHS IS NOT NULL
				AND LAHIR_KOTA_MHS IS NOT NULL AND LAHIR_PROP_MHS IS NOT NULL AND ASAL_KOTA_MHS IS NOT NULL
				AND ASAL_PROV_MHS IS NOT NULL 
				AND ALAMAT_AYAH_MHS_KOTA IS NOT NULL AND ALAMAT_AYAH_MHS_PROV IS NOT NULL 
				AND ALAMAT_IBU_MHS_KOTA IS NOT NULL AND ALAMAT_IBU_MHS_PROV IS NOT NULL AND ALAMAT_ASAL_MHS IS NOT NULL
				AND ALAMAT_ASAL_MHS_KOTA IS NOT NULL AND ALAMAT_ASAL_MHS_PROV IS NOT NULL";
	}else{
		$kueri = "SELECT COUNT(*) AS MHS FROM MAHASISWA WHERE NIM_MHS = '$mhs_nim'
				AND MOBILE_MHS IS NOT NULL AND ID_SEKOLAH_ASAL_MHS IS NOT NULL AND THN_LULUS_MHS IS NOT NULL
				AND NILAI_SKHUN_MHS IS NOT NULL AND CITA_CITA_MHS IS NOT NULL AND NM_AYAH_MHS IS NOT NULL
				AND PENDIDIKAN_AYAH_MHS IS NOT NULL AND PEKERJAAN_AYAH_MHS IS NOT NULL AND ALAMAT_AYAH_MHS IS NOT NULL
				AND NM_IBU_MHS IS NOT NULL AND PENDIDIKAN_IBU_MHS IS NOT NULL AND PEKERJAAN_IBU_MHS IS NOT NULL
				AND ALAMAT_IBU_MHS IS NOT NULL AND PENGHASILAN_ORTU_MHS IS NOT NULL AND NEM_PELAJARAN_MHS IS NOT NULL
				AND LAHIR_KOTA_MHS IS NOT NULL AND LAHIR_PROP_MHS IS NOT NULL AND ASAL_KOTA_MHS IS NOT NULL
				AND ASAL_PROV_MHS IS NOT NULL 
				AND ALAMAT_AYAH_MHS_KOTA IS NOT NULL AND ALAMAT_AYAH_MHS_PROV IS NOT NULL 
				AND ALAMAT_IBU_MHS_KOTA IS NOT NULL AND ALAMAT_IBU_MHS_PROV IS NOT NULL AND ALAMAT_ASAL_MHS IS NOT NULL
				AND ALAMAT_ASAL_MHS_KOTA IS NOT NULL AND ALAMAT_ASAL_MHS_PROV IS NOT NULL";
	}
	
	$result = $db->Query($kueri)or die("salah kueri 7 : ");
	$r = $db->FetchRow();
	
	if($r[0] <= 0){
		$error  = $error . '<b>-  Menu biodata harap dilengkapi</b></br>';
	}
	
	}else{
		$error  = $error . '<b>-  Pengajuan Yudisium Harap Hubungi Akademik Fakultas</b></br>';
	}
	
	if($error == ""){
		
/*		if(isset($_POST['tgl_lahir'])){
			$db->Query("UPDATE PENGAJUAN_WISUDA SET LAHIR_IJAZAH = '$_POST[tgl_lahir]' WHERE ID_MHS = '$id_mhs'");
			$tampil_data = '
				<br>
				Berhasil di Simpan
				';
		}
		
		$kueri = "SELECT * FROM PENGAJUAN_WISUDA WHERE ID_MHS = '$id_mhs'";
		$result = $db->Query($kueri)or die("salah kueri 2 : ");
		$r = $db->FetchArray();
		$smarty->assign('tgl_lahir', $r['LAHIR_IJAZAH']);*/
		$smarty->assign('berhasil', 'berhasil');
		

	}else{
		$tampil_data = "</br>" . $error;
	}

	$smarty->assign('tampil_data', $tampil_data);
	$smarty->assign('nim', $mhs_nim);

	$smarty->display('akademik-draft.tpl');
}else {
	$smarty->display('session-expired.tpl');
}
?>
