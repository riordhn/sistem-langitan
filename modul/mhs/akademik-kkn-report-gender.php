<?php
	include_once 'config.php';
	include_once 'akademik-kkn-model.php';

	// security sementara terhadap no auth access ~sugenk.
	if ($user->Role() != AUCC_ROLE_MAHASISWA){
		header("location: /logout.php");
		exit();
	}
	if(isset($_GET['gender']) && isset($_GET['kelompok'])){
	
		$gender = (int)$_GET['gender'];
		$kelompok = (int)$_GET['kelompok'];
		
		$report = new KknModel();
		echo $report->getQuotaGender($gender, $kelompok);
	
	}
	
?>