<?php

require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA) {
    header("location: /logout.php");
    exit();
}

include 'class/eva.class.php';
include 'function-tampil-informasi.php';

//echo $_SERVER['REMOTE_ADDR'];

$eva = new eva($db, $user->ID_PENGGUNA);
$id_evaluasi_bimbingan=7;

//Semester Pengisian
$id_semester_isi = $eva->GetSemesterAktifEvaluasi();
$id_semester_aktif = $eva->GetSemesterAktif();

$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER='{$id_semester_isi}'");
$isi = $db->FetchAssoc();
$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER='{$id_semester_aktif}'");
$setelah = $db->FetchAssoc();

//Data Mahasiswa
$db->Query("
        SELECT M.*,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI ,PS.ID_FAKULTAS
        FROM MAHASISWA M
        JOIN PENGGUNA P ON M.ID_PENGGUNA=P.ID_PENGGUNA
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
        WHERE M.ID_PENGGUNA='{$user->ID_PENGGUNA}'
    ");
$data_mhs = $db->FetchAssoc();

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {

    if (!empty($_POST)) {
        if (post('mode') == 'simpan') {
            //Simpan Evaluasi
            for ($i = 1; $i <= post('jumlah_aspek'); $i++) {
                $eva->SaveEvaluasiBimbinganSkripsi($id_evaluasi_bimbingan, post('kel_aspek' . $i), post('aspek' . $i), $data_mhs['ID_MHS'], $data_mhs['ID_PROGRAM_STUDI']
                        , $data_mhs['ID_FAKULTAS'], post('nilai' . $i));
            }
            $smarty->assign('alert', alert_success("Anda telah berhasil mengisi Evaluasi Bimbingan Skripsi.Terima kasih atas partisipasinya..."));
        }
    }
    // Jika Sudah Melakukan Pengisian Evaluasi 
    else if ($eva->CekPengisianEvaluasiBimbinganSkripsi($data_mhs['ID_MHS'])) {
        $smarty->assign('alert', alert_success("Anda telah berhasil mengisi Evaluasi Bimbingan Skripsi.Terima kasih atas partisipasinya..."));
    } else {
        $status_buka_adm = $db->QuerySingle("SELECT STATUS_BUKA FROM EVALUASI_INSTRUMEN WHERE ID_EVAL_INSTRUMEN=$id_evaluasi_bimbingan");
        if ($status_buka_adm == 1) {
            $smarty->assign('data_aspek', $eva->LoadEvaluasiAspek($id_evaluasi_bimbingan));
        } else {
            $pengumuman = $db->QuerySingle("SELECT PENGUMUMAN FROM EVALUASI_INSTRUMEN GROUP BY PENGUMUMAN");
            if($pengumuman==''){
                $smarty->assign('alert', alert_error("Mohon Maaf proses pengisian evaluasi sudah tertutup. Terima Kasih"));
            }else{
                $smarty->assign('alert', alert_error($pengumuman));    
            }
        }
    }
    $smarty->display('evskripsi.tpl');
} else {
    $smarty->display('session-expired.tpl');
}
