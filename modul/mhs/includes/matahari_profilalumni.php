<?php
require '../../../config.php';

	$kembalian="";
	$awal = mktime();

	$db = new MyOracle();

	//tes koneksi
	if ($db->conn == false){
		echo OCIError($db->conn)."Not Connected!";
		exit;
	} else {
		// echo "<br>success connecting to my oracle databases";
	} 
	//mengambil koneksi dari class
	$conn = $db->conn;

if (!$conn){
    $e = oci_error(); // For oci_connect errors do not pass a handle
	//die('<pre>Koneksi ke DATABASE tidak dapat dilakukan Gan!, karena:</br><>'.$e['message'].'</pre>');
	//die('database tidak konek');
	$kembalian = "ERROR;database tidak konek";
}



if(strlen($kembalian)>0) {
	// stop
}else if(strlen($kembalian)==0) {
	$isi_xml = '<?xml version="1.0"?>
<catalog>';

	$kueri = "select id_fakultas, nm_fakultas from aucc.fakultas order by id_fakultas";
	$stmt = oci_parse ($conn, $kueri);
	oci_execute ($stmt);
	while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
		$query = "
		select * from (
			select a.id_pengajuan_wisuda, b.nim_mhs, c.nm_pengguna, c.email_pengguna, 
			to_char(a.tgl_sk_yudisium,'YYYYMMDD'), d.nm_program_studi, e.nm_jenjang, a.sks_total, a.ipk, a.judul_ta, g.thn_akademik_semester, g.nm_semester
			from aucc.pengajuan_wisuda a, aucc.mahasiswa b, aucc.pengguna c, aucc.program_studi d, aucc.jenjang e, aucc.periode_wisuda f, aucc.semester g
			where a.id_mhs=b.id_mhs and b.id_pengguna=c.id_pengguna and a.id_periode_wisuda=f.id_periode_wisuda and f.id_semester=g.id_semester
			and b.id_program_studi=d.id_program_studi and d.id_jenjang=e.id_jenjang 
			and d.id_fakultas='".$r[0]."' and a.ipk is not null and a.ipk<=4
			order by a.ipk desc
		) where rownum<2
		";
		$stmt2 = oci_parse ($conn, $query);
		oci_execute ($stmt2);
		while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
			$isi_xml .= '
			<book id="'.$r2[0].'">
			<periode>'.$r2[10].' - '.$r2[11].'</periode>
			<nama>'.$r2[2].'</nama>
			<nim>'.$r2[1].'</nim>
			<email>'.$r2[3].'</email>
			<tgllulus>'.$r2[4].'</tgllulus>
			<prodi>'.$r2[5].'</prodi>
			<jenjang>'.$r2[6].'</jenjang>
			<sks>'.$r2[7].'</sks>
			<ipk>'.$r2[8].'</ipk>
			<judulta>'.$r2[9].'</judulta>
			</book>
			';
		}
	}

	$isi_xml .= '</catalog>';

	$kembalian = $isi_xml;
}

echo $kembalian;

?>