<?php

$oci_user = 'aucc_akademik'; // username
$oci_pass = 'akadem1k'; // password
//$oci_host = '210.57.212.67'; // hostname
$oci_host = '10.0.110.100'; // hostname
$oci_port = '1521'; // listener port
$oci_sidc = 'aucc.localdomain'; // sid name

$oci_tnsc = '
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = '.$oci_host.')(PORT = '.$oci_port.'))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = '.$oci_sidc.')
    )
  )';


@$conn = oci_connect($oci_user, $oci_pass, $oci_tnsc);
if (!$conn){
    $e = oci_error(); // For oci_connect errors do not pass a handle
	die('database tidak konek');
}

$id_semaktif='5';

$muncul2 = '
<table cellpadding=3 cellspacing=0 border=1>
<tr>
	<td>No</td>
	<td>Nim</td>
	<td>spp</td>
	<td>praktikum</td>
	<td>regis</td>
	<td>sp3</td>
	<td>pkl</td>
	<td>asuransi</td>
	<td>denda</td>
	<td>matrikulasi</td>
	<td>kkn</td>
	<td>wisuda</td>
	<td>pembinaan</td>
	<td>tgl_bayar</td>
	<td>bank</td>
</tr>
';
	$query = "select a.id_mhs, to_char(TO_DATE(b.tgl_bayar,'DD-month-YY'),'YYYY-MM-DD'), a.nim_mhs, b.id_bank
	from aucc.mahasiswa a, aucc.pembayaran b
	where a.id_mhs=b.id_mhs 
	-- and substr(a.nim_mhs,3,2) in ('92','91','90','89','88','87','86','85','84','83','82','81')
	-- and substr(a.nim_mhs,3,2) in ('95')
	-- and substr(a.nim_mhs,3,2) in ('96')
	-- and substr(a.nim_mhs,3,2) in ('97')
	-- and substr(a.nim_mhs,3,2) in ('98')
	-- and substr(a.nim_mhs,3,2) in ('99')
	-- and substr(a.nim_mhs,3,2) in ('00')
	-- and substr(a.nim_mhs,3,2) in ('01')
	-- and substr(a.nim_mhs,3,2) in ('02')
	-- and substr(a.nim_mhs,3,2) in ('03')
	-- and substr(a.nim_mhs,3,2) in ('04')
	-- and substr(a.nim_mhs,3,2) in ('05')
	-- and substr(a.nim_mhs,3,2) in ('06')
	-- and substr(a.nim_mhs,3,2) in ('07')
	-- and substr(a.nim_mhs,3,2) in ('08')
	-- and substr(a.nim_mhs,3,2) in ('09')
	-- and substr(a.nim_mhs,3,2) in ('10')
	and substr(a.nim_mhs,3,2) in ('11')
	group by a.id_mhs,b.tgl_bayar, a.nim_mhs, b.id_bank
	order by a.nim_mhs, b.tgl_bayar";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	$hit=0; $bayar_sudah=0; $bayar_belum=0;
	while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
		$hit++;
		$id_mhs = $r[0];
		$tgl_bayar = $r[1];
		$nim = $r[2];
		$bank = $r[3];

		if(strlen($tgl_bayar)==0) {
			$param_tgl = " and a.tgl_bayar is null ";
		}else{
			$param_tgl = " and to_char(TO_DATE(a.tgl_bayar,'DD-month-YY'),'YYYY-MM-DD')='".$tgl_bayar."' ";
		}

		//echo $hit." - ".$tgl_bayar."<br>";

		// bank
		$query2 = "select nm_bank from aucc.bank where id_bank='".$bank."'";
		$stmt2 = oci_parse ($conn, $query2);
		oci_execute ($stmt2);
		$nm_bank="";
		while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
			$nm_bank = $r2[0];
		}

		$denda = 0;
		// spp
		$query2 = "select a.besar_biaya, a.denda_biaya
		from aucc.pembayaran a, AUCC.detail_biaya b, AUCC.biaya c
		where a.id_detail_biaya = b.id_detail_biaya and b.id_biaya=c.id_biaya 
		and a.id_mhs='".$id_mhs."' and a.id_semester='5' and c.nm_biaya='SOP' ".$param_tgl." ";
		//echo $query2."<br>";
		$stmt2 = oci_parse ($conn, $query2);
		oci_execute ($stmt2);
		$spp='0';
		while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
			$spp = $r2[0];
			$denda += $r2[1];
		}
		// praktikum
		$query2 = "select a.besar_biaya, a.denda_biaya
		from aucc.pembayaran a, AUCC.detail_biaya b, AUCC.biaya c
		where a.id_detail_biaya = b.id_detail_biaya and b.id_biaya=c.id_biaya 
		and a.id_mhs='".$id_mhs."' and a.id_semester='5' and c.nm_biaya='Praktikum' ".$param_tgl." ";
		$stmt2 = oci_parse ($conn, $query2);
		oci_execute ($stmt2);
		$praktikum='0';
		while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
			$praktikum = $r2[0];
			$denda += $r2[1];
		}
		// regis
		$query2 = "select a.besar_biaya, a.denda_biaya
		from aucc.pembayaran a, AUCC.detail_biaya b, AUCC.biaya c
		where a.id_detail_biaya = b.id_detail_biaya and b.id_biaya=c.id_biaya 
		and a.id_mhs='".$id_mhs."' and a.id_semester='5' and c.nm_biaya='Registrasi' ".$param_tgl." ";
		$stmt2 = oci_parse ($conn, $query2);
		oci_execute ($stmt2);
		$regis='0';
		while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
			$regis = $r2[0];
			$denda += $r2[1];
		}
		// sp3
		$query2 = "select a.besar_biaya, a.denda_biaya
		from aucc.pembayaran a, AUCC.detail_biaya b, AUCC.biaya c
		where a.id_detail_biaya = b.id_detail_biaya and b.id_biaya=c.id_biaya 
		and a.id_mhs='".$id_mhs."' and a.id_semester='5' and c.nm_biaya='SP3' ".$param_tgl." ";
		$stmt2 = oci_parse ($conn, $query2);
		oci_execute ($stmt2);
		$sp3='0';
		while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
			$sp3 = $r2[0];
			$denda += $r2[1];
		}
		// pkl
		$query2 = "select a.besar_biaya, a.denda_biaya
		from aucc.pembayaran a, AUCC.detail_biaya b, AUCC.biaya c
		where a.id_detail_biaya = b.id_detail_biaya and b.id_biaya=c.id_biaya 
		and a.id_mhs='".$id_mhs."' and a.id_semester='5' and c.nm_biaya='Praktik Kerja Lapangan' ".$param_tgl." ";
		$stmt2 = oci_parse ($conn, $query2);
		oci_execute ($stmt2);
		$pkl='0';
		while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
			$pkl = $r2[0];
			$denda += $r2[1];
		}
		// asuransi
		$query2 = "select a.besar_biaya, a.denda_biaya
		from aucc.pembayaran a, AUCC.detail_biaya b, AUCC.biaya c
		where a.id_detail_biaya = b.id_detail_biaya and b.id_biaya=c.id_biaya 
		and a.id_mhs='".$id_mhs."' and a.id_semester='5' and c.nm_biaya='Asuransi kesehatan' ".$param_tgl." ";
		$stmt2 = oci_parse ($conn, $query2);
		oci_execute ($stmt2);
		$asuransi='0';
		while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
			$asuransi = $r2[0];
			$denda += $r2[1];
		}
		// matrikulasi
		$query2 = "select a.besar_biaya, a.denda_biaya
		from aucc.pembayaran a, AUCC.detail_biaya b, AUCC.biaya c
		where a.id_detail_biaya = b.id_detail_biaya and b.id_biaya=c.id_biaya 
		and a.id_mhs='".$id_mhs."' and a.id_semester='5' and c.nm_biaya='Matrik' ".$param_tgl." ";
		$stmt2 = oci_parse ($conn, $query2);
		oci_execute ($stmt2);
		$matrikulasi='0';
		while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
			$matrikulasi = $r2[0];
			$denda += $r2[1];
		}
		// kkn
		$query2 = "select a.besar_biaya, a.denda_biaya
		from aucc.pembayaran a, AUCC.detail_biaya b, AUCC.biaya c
		where a.id_detail_biaya = b.id_detail_biaya and b.id_biaya=c.id_biaya 
		and a.id_mhs='".$id_mhs."' and a.id_semester='5' and c.nm_biaya='KKN' ".$param_tgl." ";
		$stmt2 = oci_parse ($conn, $query2);
		oci_execute ($stmt2);
		$kkn='0';
		while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
			$kkn = $r2[0];
			$denda += $r2[1];
		}
		// wisuda
		$query2 = "select a.besar_biaya, a.denda_biaya
		from aucc.pembayaran a, AUCC.detail_biaya b, AUCC.biaya c
		where a.id_detail_biaya = b.id_detail_biaya and b.id_biaya=c.id_biaya 
		and a.id_mhs='".$id_mhs."' and a.id_semester='5' and c.nm_biaya='Wisuda' ".$param_tgl." ";
		$stmt2 = oci_parse ($conn, $query2);
		oci_execute ($stmt2);
		$wisuda='0';
		while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
			$wisuda = $r2[0];
			$denda += $r2[1];
		}
		// pembinaan
		$query2 = "select a.besar_biaya, a.denda_biaya
		from aucc.pembayaran a, AUCC.detail_biaya b, AUCC.biaya c
		where a.id_detail_biaya = b.id_detail_biaya and b.id_biaya=c.id_biaya 
		and a.id_mhs='".$id_mhs."' and a.id_semester='5' and c.nm_biaya='Biaya pemb. jati diri' ".$param_tgl." ";
		$stmt2 = oci_parse ($conn, $query2);
		oci_execute ($stmt2);
		$pembinaan='0';
		while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
			$pembinaan = $r2[0];
			$denda += $r2[1];
		}

		$muncul2 .= '
		<tr>
			<td>'.$hit.'</td>
			<td>'.$nim.'</td>
			<td>'.$spp.'</td>
			<td>'.$praktikum.'</td>
			<td>'.$regis.'</td>
			<td>'.$sp3.'</td>
			<td>'.$pkl.'</td>
			<td>'.$asuransi.'</td>
			<td>'.$denda.'</td>
			<td>'.$matrikulasi.'</td>
			<td>'.$kkn.'</td>
			<td>'.$wisuda.'</td>
			<td>'.$pembinaan.'</td>
			<td>'.$tgl_bayar.'</td>
			<td>'.$nm_bank.'</td>
		</tr>
		';
	}
$muncul2 .= '
</table>
';

	echo $muncul2;
?>