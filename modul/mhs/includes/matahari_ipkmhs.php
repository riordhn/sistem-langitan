<?php
require '../../../config.php';

$kembalian="";
$awal = mktime();

	$db = new MyOracle();

	//tes koneksi
	if ($db->conn == false){
		echo OCIError($db->conn)."Not Connected!";
		exit;
	} else {
		// echo "<br>success connecting to my oracle databases";
	} 
	//mengambil koneksi dari class
	$conn = $db->conn;

if (!$conn){
    $e = oci_error(); // For oci_connect errors do not pass a handle
	//die('<pre>Koneksi ke DATABASE tidak dapat dilakukan Gan!, karena:</br><>'.$e['message'].'</pre>');
	//die('database tidak konek');
	$kembalian = "ERROR;database tidak konek";
}


if(strlen($kembalian)>0) {
	// stop
}else if(strlen($kembalian)==0) {
	$isi_xml = '<?xml version="1.0"?>
<catalog>';

	$query = "
	select * from (
		select a.id_pengajuan_wisuda, b.nim_mhs, c.nm_pengguna, a.ipk, g.thn_akademik_semester, g.nm_semester, e.nm_fakultas
		from aucc.pengajuan_wisuda a, aucc.mahasiswa b, aucc.pengguna c, aucc.program_studi d, aucc.fakultas e, aucc.periode_wisuda f, aucc.semester g
		where a.id_mhs=b.id_mhs and b.id_pengguna=c.id_pengguna and b.id_program_studi=d.id_program_studi and d.id_fakultas=e.id_fakultas
		and a.id_periode_wisuda=f.id_periode_wisuda and f.id_semester=g.id_semester and f.status_aktif='1' and a.ipk is not null and a.ipk<=4
		order by a.ipk desc
	) where rownum<11
	";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
		$isi_xml .= '
		<book id="'.$r[0].'">
		<periode>'.$r[4].' - '.$r[5].'</periode>
		<nim>'.$r[1].'</nim>
		<nama>'.$r[2].'</nama>
		<fakul>'.$r[6].'</fakul>
		<link></link>
		<ipk>'.$r[3].'</ipk>
		</book>
		';
	}
	$isi_xml .= '</catalog>';

	$kembalian = $isi_xml;
}

echo $kembalian;

?>