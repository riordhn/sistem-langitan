<?php
class ImageUploader{
	private $max_height;
	private $max_width;
	private $upload_dir;
	
	private $file_name;
	private $file_size;
	private $file_tmp;
	private $file_type;
	private $image_name;

	function __construct($max_size, $max_width, $max_height, $upload_dir)
	{
		$this->max_size = $max_size;
		$this->max_height = $max_height;
		$this->max_width = $max_width;
		$this->upload_dir = $upload_dir;
	}

	function setImage($name)
	{
		$this->file_name = $_FILES[$name]['name'];
		$this->file_size = $_FILES[$name]['size'];
		$this->file_tmp = $_FILES[$name]['tmp_name'];
		$this->file_type = $_FILES[$name]['type'];
	}

	function setImageName($name)
	{
		$this->image_name = $name;
	}

	function deleteExisting()
	{
		$jpg =  $this->upload_dir.''.$this->image_name.'.jpg';
		if(file_exists($jpg)) unlink($jpg);
		
		$jpeg =  $this->upload_dir.''.$this->image_name.'.jpeg';
		if(file_exists($jpeg)) unlink($jpeg);		
			
		$gif =  $this->upload_dir.''.$this->image_name.'.gif';
		if(file_exists($gif)) unlink($gif); 		
			
		$png =  $this->upload_dir.''.$this->image_name.'.png';
		if(file_exists($png)) unlink($png);		
	}
	
	function upload()
	{
		$ext = strrchr($this->file_name, '.');
		$name = $this->upload_dir.''.$this->image_name.'.JPG';
		if(!move_uploaded_file($this->file_tmp, $name)){
			echo $_FILES[$this->file_name]['error'];
			return false;
		}
		else
			return true;
	}

	function upload_baru()
	{
		$ext = strrchr($this->file_name, '.');
		$name = $this->upload_dir.''.$this->image_name.'.jpg';
		if(!move_uploaded_file($this->file_tmp, $name)){
			echo $_FILES[$this->file_name]['error'];
			return false;
		}
		else
			return true;
	}
	
	function checkSize()
	{
		if($this->file_size > ($this->max_size*1024))
			return false;
		else
			return true;
	}
	

	function checkHeight()
	{
		$file = getimagesize($this->file_tmp);		
		if($file[1] > $this->max_height)
			return false;
		else
			return true;
	}
	
	function checkWidth()
	{
		$file = getimagesize($this->file_tmp);		
		if($file[0] > $this->max_height)
			return false;
		else
			return true;
	}
	
	function checkExt()
	{		
		if (($this->file_type != 'image/jpg') && ($this->file_type != 'image/jpeg'))
			return false;
		else
			return true;	
	}
}
?>