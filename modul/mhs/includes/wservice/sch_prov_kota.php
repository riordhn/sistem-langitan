<?php
require '../../../../config.php';
require '../ceking4.php';

	$db = new MyOracle();

	//tes koneksi
	if ($db->conn == false){
		echo OCIError($db->conn)."Not Connected!";
		exit;
	} else {
		// echo "<br>success connecting to my oracle databases";
	} 
	//mengambil koneksi dari class
	$conn = $db->conn;



	if(isset($_GET["ambil_data"]) ) {
		if($_GET["ambil_data"] == "propinsi") {
			$query = "select id_provinsi,nm_provinsi from aucc.PROVINSI where id_negara=114 order by nm_provinsi";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt); $hasil = array(); $urut=0;
			while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
				$hasil[$urut]["prov_id"] = $r[0];
				$hasil[$urut]["prov_nama"] = $r[1];
				$urut++;
			}
			echo json_encode($hasil);
		}
		if($_GET["ambil_data"] == "kota" and !empty($_GET["prov"]) ) {
			$query = "select id_kota,nm_kota from aucc.KOTA where id_provinsi=".AngkaSaja($_GET["prov"])." order by nm_kota";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt); $hasil = array(); $urut=0;
			while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
				$hasil[$urut]["kota_id"] = $r[0];
				$hasil[$urut]["kota_nama"] = $r[1];
				$urut++;
			}
			echo json_encode($hasil);
		}
	}

?>