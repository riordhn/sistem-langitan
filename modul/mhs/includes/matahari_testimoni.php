<?php
require '../../../config.php';

$kembalian="";
$awal = mktime();

	$db = new MyOracle();

	//tes koneksi
	if ($db->conn == false){
		echo OCIError($db->conn)."Not Connected!";
		exit;
	} else {
		// echo "<br>success connecting to my oracle databases";
	} 
	//mengambil koneksi dari class
	$conn = $db->conn;

if (!$conn){
    $e = oci_error(); // For oci_connect errors do not pass a handle
	//die('<pre>Koneksi ke DATABASE tidak dapat dilakukan Gan!, karena:</br><>'.$e['message'].'</pre>');
	//die('database tidak konek');
	$kembalian = "ERROR;database tidak konek";
}


if(strlen($kembalian)>0) {
	// stop
}else if(strlen($kembalian)==0) {
	$isi_xml = '<?xml version="1.0"?>
<catalog>';

	$query = "
	select * from (
		select a.id_blog, b.nm_pengguna, a.judul, a.resume, a.link
		from aucc.blog a, aucc.pengguna b
		where a.id_pengguna=b.id_pengguna and a.is_approve='1'
		order by a.id_blog desc
	) where rownum<11
	";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
		$isi_xml .= '
		<book id="'.$r[0].'">
		<author>'.$r[1].'</author>
		<title>'.$r[2].'</title>
		<description>'.$r[3].'</description>
		<link>'.$r[4].'</link>
		</book>
		';
	}
	$isi_xml .= '</catalog>';

	$kembalian = $isi_xml;
}

echo $kembalian;

?>