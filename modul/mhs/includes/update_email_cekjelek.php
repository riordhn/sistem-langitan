<?php

function buat_email ($nama, $fakul, $angkatan, $tambahan){
	$nama = strtolower(trim($nama));
	$nama = str_replace("  ", " ", $nama);
	$nama = str_replace("-", "", $nama);
	$nama = str_replace("'", "", $nama);
	$nama = str_replace(" s.hum", " ", $nama);
	$nama = str_replace(" s.kom", " ", $nama);
	$nama = str_replace(" st", " ", $nama);
	$nama = str_replace(" s.s", " ", $nama);
	$nama = str_replace(" s.ip", " ", $nama);
	$nama = str_replace("prof", " ", $nama);
	$nama = str_replace(" drs", " ", $nama);
	$nama = str_replace("dr.", " ", $nama);
	$nama = str_replace("dra", " ", $nama);
	$nama = str_replace("drg", " ", $nama);
	$nama = str_replace(" ir", " ", $nama);
	$nama = str_replace(" m.eng", " ", $nama);
	$nama = str_replace(" sh", " ", $nama);
	$nama = str_replace(" s.e", " ", $nama);
	$nama = str_replace(" s.si", " ", $nama);
	$nama = str_replace(" s.t", " ", $nama);
	$nama = str_replace(" dr", " ", $nama);
	$nama = str_replace(" (purna ba", " ", $nama);
	$nama = str_replace(" (alm)", " ", $nama);
	$nama = str_replace(" (k)", " ", $nama);
	$nama = str_replace(" ms", " ", $nama);
	$nama = str_replace(" m.t", " ", $nama);
	$nama = str_replace(" mm", " ", $nama);
	$nama = str_replace("se.,mm", " ", $nama);
	$nama = str_replace(" m.sc", " ", $nama);
	$nama = str_replace(" m.hum", " ", $nama);
	$nama = str_replace(" m.si", " ", $nama);
	$nama = str_replace(" s.pd", " ", $nama);
	$nama = str_replace("sp.bm", " ", $nama);
	$nama = str_replace(" dtm&h", " ", $nama);
	$nama = str_replace(" sp.a", " ", $nama);

	$nama = str_replace(".", " ", $nama);
	$nama = str_replace(",", " ", $nama);

	$nama = str_replace("`","",$nama);
	$nama = str_replace("(","",$nama);
	$nama = str_replace(")","",$nama);
	$nama = str_replace("/","",$nama);

	switch ($fakul) {
		case "1" : $fakul = "-".$angkatan."@fk.unair.ac.id"; break;
		case "2" : $fakul = "-".$angkatan."@fkg.unair.ac.id"; break;
		case "3" : $fakul = "-".$angkatan."@fh.unair.ac.id"; break;
		case "4" : $fakul = "-".$angkatan."@feb.unair.ac.id"; break;
		case "5" : $fakul = "-".$angkatan."@ff.unair.ac.id"; break;
		case "6" : $fakul = "-".$angkatan."@fkh.unair.ac.id"; break;
		case "7" : $fakul = "-".$angkatan."@fisip.unair.ac.id"; break;
		case "8" : $fakul = "-".$angkatan."@fst.unair.ac.id"; break;
		case "9" : $fakul = "-".$angkatan."@pasca.unair.ac.id"; break; 
		case "10" : $fakul = "-".$angkatan."@fkm.unair.ac.id"; break;
		case "11" : $fakul = "-".$angkatan."@psikologi.unair.ac.id"; break;
		case "12" : $fakul = "-".$angkatan."@fib.unair.ac.id"; break;
		case "13" : $fakul = "-".$angkatan."@fkp.unair.ac.id"; break;
		case "14" : $fakul = "-".$angkatan."@fpk.unair.ac.id"; break;
		default : $fakul = ""; break;
	}

	$tmp_nama2 = explode(" ",$nama);

	// array yg kosong, dihilangkan
	$tmp_nama = array();
	for($d=0; $d<count($tmp_nama2); $d++) {
		if(strlen($tmp_nama2[$d])==0) {
		}else{
			array_push($tmp_nama, $tmp_nama2[$d]);
		}
	}
	$mail = "";

	$ada = 1;
	for($k=0; $k<count($tmp_nama); $k++) {
		if(strlen(trim($tmp_nama[$k]))>0 and $ada<=3) {
			$mail .= ".".trim($tmp_nama[$k]);
			if(strlen(trim($tmp_nama[$k]))>1) {
				$ada++;
			}
		}
	}
	$mail = substr($mail,1).$tambahan.$fakul;

	return $mail;
}

$oci_user = 'aucc_akademik'; // username
$oci_pass = 'akadem1k'; // password
//$oci_host = '210.57.212.67'; // hostname
$oci_host = '10.0.110.100'; // hostname
$oci_port = '1521'; // listener port
$oci_sidc = 'aucc.localdomain'; // sid name

$oci_tnsc = '
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = '.$oci_host.')(PORT = '.$oci_port.'))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = '.$oci_sidc.')
    )
  )';


@$conn = oci_connect($oci_user, $oci_pass, $oci_tnsc);
if (!$conn){
    $e = oci_error(); // For oci_connect errors do not pass a handle
	die('database tidak konek');
}



	echo '
	<table cellpadding=5 cellspacing=0>
	<tr>
		<td>No</td>
		<td>id pengguna</td>
		<td>nama</td>
		<td>email</td>
		<td>email baru</td>
	</tr>
	';
	$kueri = "
	select a.email_pengguna, b.thn_angkatan_mhs, a.id_pengguna, a.nm_pengguna, c.id_fakultas
	from aucc.pengguna a, aucc.mahasiswa b, aucc.program_studi c, aucc.status_pengguna d
	where a.id_pengguna=b.id_pengguna and b.id_program_studi=c.id_program_studi and b.status_akademik_mhs=d.id_status_pengguna
	and d.status_aktif='1'
	";
	$stmt = oci_parse ($conn, $kueri)or die("salah kueri 4");
	oci_execute ($stmt); $urut=0; $email_jelek=0; $email_kosong=0;
	while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
		$email = $r[0];
		$tahun = substr($r[1],-2);
		$idpengguna = $r[2];
		$nmpengguna = $r[3];
		$fakultas = $r[4];

		if(strlen($email)==0) {
			$email_kosong++;

				$mailbaru = buat_email($nmpengguna, $fakultas, $tahun,'');
				// cek apakah email sudah ada
				$kueri2 = "select count(*) from aucc.pengguna where email_pengguna='".$mailbaru."'";
				$stmt2 = oci_parse ($conn, $kueri2)or die("salah kueri 5");
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					$mailada = $r2[0];
				}
				if($mailada > 0) {
					$mailbaru = buat_email($nmpengguna, $fakultas, $tahun, $idpengguna);
				}
				// updatekan
				//$kueri3 = "update aucc.pengguna set email_pengguna='".$mailbaru."' where id_pengguna='".$idpengguna."'";
				//$stmt3 = oci_parse ($conn, $kueri3)or die("salah kueri 6");
				//oci_execute ($stmt3);
		}else{
			switch ($fakultas) {
			case '1' :
				$bahan = "-".$tahun."@fk.unair.ac.id"; break;
			case '2' :
				$bahan = "-".$tahun."@fkg.unair.ac.id"; break;
			case '3' :
				$bahan = "-".$tahun."@fh.unair.ac.id"; break;
			case '4' :
				$bahan = "-".$tahun."@feb.unair.ac.id"; break;
			case '5' :
				$bahan = "-".$tahun."@ff.unair.ac.id"; break;
			case '6' :
				$bahan = "-".$tahun."@fkh.unair.ac.id"; break;
			case '7' :
				$bahan = "-".$tahun."@fisip.unair.ac.id"; break;
			case '8' :
				$bahan = "-".$tahun."@fst.unair.ac.id"; break;
			case '9' :
				$bahan = "-".$tahun."@pasca.unair.ac.id"; break;
			case '10' :
				$bahan = "-".$tahun."@fkm.unair.ac.id"; break;
			case '11' :
				$bahan = "-".$tahun."@psikologi.unair.ac.id"; break;
			case '12' :
				$bahan = "-".$tahun."@fib.unair.ac.id"; break;
			case '13' :
				$bahan = "-".$tahun."@fkp.unair.ac.id"; break;
			case '14' :
				$bahan = "-".$tahun."@fpk.unair.ac.id"; break;
			
			}
			$email2 = str_replace($bahan,"",$email);
			$tmp = explode("-",$email2);
			$ya=0; $tidak=0;
			for($k=0; $k<count($tmp); $k++) {
				if(strlen($tmp[$k])<=2) {
					// lanjut
					$ya++;
				}else{
					// bukan
					$tidak++;
				}
			}
			if($ya==count($tmp)) {
				//echo $idpengguna." # ".$nmpengguna." # ".$email."<br>";
				$email_jelek++;
				$urut++;
				
				$mailbaru = buat_email($nmpengguna, $fakultas, $tahun,'');
				// cek apakah email sudah ada
				$kueri2 = "select count(*) from aucc.pengguna where email_pengguna='".$mailbaru."'";
				$stmt2 = oci_parse ($conn, $kueri2)or die("salah kueri 5");
				oci_execute ($stmt2);
				while ($r2 = oci_fetch_array ($stmt2, OCI_BOTH)) {
					$mailada = $r2[0];
				}
				if($mailada > 0) {
					$mailbaru = buat_email($nmpengguna, $fakultas, $tahun, $idpengguna);
				}
				echo '
				<tr>
					<td>'.$urut.'</td>
					<td>'.$idpengguna.'</td>
					<td>'.$nmpengguna.'</td>
					<td>'.$email.'</td>
					<td>'.$mailbaru.'</td>
				</tr>
				';
				// updatekan
				//$kueri3 = "update aucc.pengguna set email_pengguna='".$mailbaru."' where id_pengguna='".$idpengguna."'";
				//$stmt3 = oci_parse ($conn, $kueri3)or die("salah kueri 6");
				//oci_execute ($stmt3);
			}
		}
	}
	echo '</table><br><br>';
	echo 'Email Kosong : '.$email_kosong.'<br>';
	echo 'Email Jelek : '.$email_jelek.'<br>';

?>