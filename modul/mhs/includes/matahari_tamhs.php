<?php
require '../../../config.php';

$kembalian="";
$awal = mktime();

	$db = new MyOracle();

	//tes koneksi
	if ($db->conn == false){
		echo OCIError($db->conn)."Not Connected!";
		exit;
	} else {
		// echo "<br>success connecting to my oracle databases";
	} 
	//mengambil koneksi dari class
	$conn = $db->conn;

if (!$conn){
    $e = oci_error(); // For oci_connect errors do not pass a handle
	//die('<pre>Koneksi ke DATABASE tidak dapat dilakukan Gan!, karena:</br><>'.$e['message'].'</pre>');
	//die('database tidak konek');
	$kembalian = "ERROR;database tidak konek";
}


if(strlen($kembalian)>0) {
	// stop
}else if(strlen($kembalian)==0) {
	$isi_xml = '<?xml version="1.0"?>
<catalog>';

	$query = "
	select * from (
		select a.id_pengajuan_wisuda, b.nim_mhs, c.nm_pengguna, a.judul_ta, to_char(a.tgl_lulus_pengajuan,'YYYYMMDD'), e.nm_fakultas
		from aucc.pengajuan_wisuda a, aucc.mahasiswa b, aucc.pengguna c, aucc.program_studi d, aucc.fakultas e
		where a.id_mhs=b.id_mhs and b.id_pengguna=c.id_pengguna and a.tgl_lulus_pengajuan is not null and a.judul_ta is not null
		and b.id_program_studi=d.id_program_studi and d.id_fakultas=e.id_fakultas
		order by to_char(a.tgl_lulus_pengajuan,'YYYYMMDD') desc
	) where rownum<11
	";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
		$isi_xml .= '
		<book id="'.$r[0].'">
		<author>['.$r[1].'] '.$r[2].'</author>
		<title>'.$r[3].'</title>
		<fakul>'.$r[5].'</fakul>
		<bidang></bidang>
		<link></link>
		<unit>Cybercampus</unit>
		<publish_date>'.$r[4].'</publish_date>
		</book>
		';
	}
	$isi_xml .= '</catalog>';

	$kembalian = $isi_xml;
}

echo $kembalian;

?>