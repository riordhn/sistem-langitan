<?php
require '../../../config.php';

	$db = new MyOracle();

	//tes koneksi
	if ($db->conn == false){
		echo OCIError($db->conn)."Not Connected!";
		exit;
	} else {
		// echo "<br>success connecting to my oracle databases";
	} 
	//mengambil koneksi dari class
	$conn = $db->conn;

date_default_timezone_set('Asia/Jakarta');      // Timezone set ke Asia/Jakarta


if($_POST["judul"] and $_POST["pengumuman"] and $_POST["pengumuman_resume"] and $_POST["ok"] and $_POST["idedit"]) {
	if($_POST["aktif"]=='Y') { $isaktif = '1'; }else if($_POST["aktif"]=='T') { $isaktif = '0'; }
	if($_POST["splash"]=='Y') { $issplash = '1'; }else if($_POST["splash"]=='T') { $issplash = '0'; }

	// simpan edit
	$query = "update aucc.news set judul='".addslashes($_POST["judul"])."', isi='".addslashes($_POST["pengumuman"])."', resume='".addslashes($_POST["pengumuman_resume"])."', is_active='".$isaktif."', is_popup='".$issplash."' where id_news='".addslashes($_POST["idedit"])."'";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	echo '
	<script type="text/javascript">
	location.href="input_pengumuman.php";
	</script>
	';
}else if($_POST["judul"] and $_POST["pengumuman"] and $_POST["pengumuman_resume"] and $_POST["ok"]) {
	if($_POST["aktif"]=='Y') { $isaktif = '1'; }else if($_POST["aktif"]=='T') { $isaktif = '0'; }
	if($_POST["splash"]=='Y') { $issplash = '1'; }else if($_POST["splash"]=='T') { $issplash = '0'; }

	// update popup
	//$query = "update aucc.news set is_popup='0'";
	//$stmt = oci_parse ($conn, $query);
	//oci_execute ($stmt);

	// input baru
	$query = "insert into aucc.news (id_pengguna, judul, isi, resume, tgl, is_active, is_approve, is_popup) values ('25646', '".addslashes($_POST["judul"])."', '".addslashes($_POST["pengumuman"])."', '".addslashes($_POST["pengumuman_resume"])."', TO_DATE('".date("Ymd")."', 'YYYYMMDD'), '".$isaktif."', '1', '".$issplash."')";
	//echo $query;
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
}

if($_GET["edit"]) {
	$query = "select id_news,judul,isi,resume,is_active,is_popup from aucc.news where id_pengguna='25646' and is_approve='1' and id_news='".addslashes($_GET["edit"])."'";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
		$idnews = $r[0];
		$judul = $r[1];
		$isi = $r[2];
		$isi_resume = $r[3];
		$isaktif = $r[4];
		$issplash = $r[5];
	}
}else{
	$idnews = '0';
	$judul = '';
	$isi = '';
	$isi_resume = '';
	$isaktif = '';
	$issplash = '';
}

echo '
<form name=form1 method=post action="">
<table>
<tr>
	<td>Judul</td>
	<td>:</td>
	<td><input type=text name="judul" value="'.$judul.'"></td>
</tr>
<tr>
	<td>Resume Pengumuman</td>
	<td>:</td>
	<td>
	<textarea name="pengumuman_resume" rows="5" cols="40">'.$isi_resume.'</textarea>
	</td>
</tr>
<tr>
	<td>Pengumuman Lengkap</td>
	<td>:</td>
	<td>
	<textarea name="pengumuman" rows="5" cols="40">'.$isi.'</textarea>
	</td>
</tr>
<tr>
	<td>Is Aktif</td>
	<td>:</td>
	<td>
	<select name="aktif">
		<option value="Y" '; if($isaktif=='1'){ echo 'selected'; } echo '>Y</option>
		<option value="T" '; if($isaktif=='0'){ echo 'selected'; } echo '>T</option>
	</select>
	</td>
</tr>
<tr>
	<td>Splash News</td>
	<td>:</td>
	<td>
	<select name="splash">
		<option value="Y" '; if($issplash=='1'){ echo 'selected'; } echo '>Y</option>
		<option value="T" '; if($issplash=='0'){ echo 'selected'; } echo '>T</option>
	</select>
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>
	';
	if($_GET["edit"]) {
		echo '
		<input type=hidden name="idedit" value="'.$idnews.'">
		<input type=submit name="ok" value="Ubah">
		';
	}else{
		echo '<input type=submit name="ok" value="Simpan">';
	}
	echo '
	</td>
</tr>
</table>
</form>
<br><br>
<table cellpadding=5 cellspacing=0 border=1>
<tr>
	<td><b>Urut</b></td>
	<td><b>Judul</b></td>
	<td><b>Resume</b></td>
	<td><b>Pengumuman</b></td>
	<td><b>Aktif</b></td>
	<td><b>Is Splash</b></td>
	<td>&nbsp;</td>
</tr>
';
$query = "select id_news,judul,resume,isi,is_active, is_popup from aucc.news where id_pengguna='25646' and is_approve='1' order by id_news";
$stmt = oci_parse ($conn, $query);
oci_execute ($stmt);
$no=0;
while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
	$no++;
	if($r[4]=='1') { $aktif='Y'; }else if($r[4]=='0') { $aktif='T'; }
	echo '
	<tr>
		<td>'.$no.'</td>
		<td>'.$r[1].'</td>
		<td>'.$r[2].'</td>
		<td>'.$r[3].'</td>
		<td>'.$aktif.'</td>
		<td>'.$r[5].'</td>
		<td><a href="?edit='.$r[0].'">Edit</a></td>
	</tr>
	';
}
echo '
</table>
';

?>