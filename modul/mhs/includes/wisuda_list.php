<?php
require '../../../config.php';

// include all files needed
		require_once('bank/excel/OLEwriter.php');
		require_once('bank/excel/BIFFwriter.php');
		require_once('bank/excel/Worksheet.php');
		require_once('bank/excel/Workbook.php');

	//create 'header' function. if called, this will tell the browser that the file returned is an excel document
		function HeaderingExcel($filename) {
		  header("Content-type: application/vnd.ms-excel");
		  header("Content-Disposition: attachment; filename=$filename" );
		  header("Expires: 0");
		  header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		  header("Pragma: public");
		}

	$db = new MyOracle();

	//tes koneksi
	if ($db->conn == false){
		echo OCIError($db->conn)."Not Connected!";
		exit;
	} else {
		// echo "<br>success connecting to my oracle databases";
	} 
	//mengambil koneksi dari class
	$conn = $db->conn;

date_default_timezone_set('Asia/Jakarta');      // Timezone set ke Asia/Jakarta

if(!isset($_POST["periode"]) ) {
	echo '
	<form name=form1 method=post action="">
	<table>
	<tr>
		<td>Periode Wisuda</td>
		<td>:</td>
		<td>
		<select name="periode">
		';
		/*
		$query = "
		select a.id_periode_wisuda, b.tahun_ajaran, b.nm_semester, c.nm_jenjang
		from aucc.periode_wisuda a, aucc.semester b, aucc.jenjang c 
		where a.id_semester=b.id_semester and a.id_jenjang=c.id_jenjang
		order by b.tahun_ajaran, b.nm_semester, c.nm_jenjang
		";
		*/
		$query = "select id_tarif_wisuda,nm_tarif_wisuda from aucc.tarif_wisuda order by id_tarif_wisuda";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
			echo '<option value="'.$r[0].'" '.(($_POST["periode"]==$r[0])? 'selected':'').'>'.$r[1].'</option>';
		}
		echo '
		</select>
		<input type=submit name="ok" value="Tampil">
		</td>
	</tr>
	</table>
	</form>
	';

}else if(isset($_POST["periode"]) ) {
	// HTTP headers
		HeaderingExcel('list_mhs_wisuda.xls'); //call the function above

	// Creating a workbook instance
		$workbook = new Workbook("-");

	// woksheet 1
		$worksheet1 =& $workbook->add_worksheet('Report');

		$worksheet1->set_zoom(100); //75% zoom
		$worksheet1->set_portrait();
		$worksheet1->set_paper(9); //set A4
		$worksheet1->hide_gridlines();  //hide gridlines

		$worksheet1->print_area(0, 0,40,8);

		$worksheet1->set_column(0, 0, 10);
		$worksheet1->set_column(0, 1, 10);
		$worksheet1->set_column(0, 2, 10);
		$worksheet1->set_column(0, 3, 10);
		$worksheet1->set_column(0, 4, 10);
		$worksheet1->set_column(0, 5, 10);
		$worksheet1->set_column(0, 6, 10);
		$worksheet1->set_column(0, 7, 10);
		$worksheet1->set_column(0, 8, 10);
		$worksheet1->set_column(0, 9, 10);
		$worksheet1->set_column(0, 10, 10);
		$worksheet1->set_column(0, 11, 10);
		$worksheet1->set_column(0, 12, 10);
		$worksheet1->set_column(0, 13, 10);
		$worksheet1->set_column(0, 14, 10);
		$worksheet1->set_column(0, 15, 10);
		$worksheet1->set_column(0, 16, 10);
		$worksheet1->set_column(0, 17, 10);
	  
		$worksheet1->write_string(0,0, "URUT");
		$worksheet1->write_string(0,1, "GELAR DEPAN");
		$worksheet1->write_string(0,2, "NAMA");
		$worksheet1->write_string(0,3, "GELAR BELAKANG");
		$worksheet1->write_string(0,4, "KELAMIN");
		$worksheet1->write_string(0,5, "NIM");
		$worksheet1->write_string(0,6, "TTL");
		$worksheet1->write_string(0,7, "JENJANG");
		$worksheet1->write_string(0,8, "FAKULTAS");
		$worksheet1->write_string(0,9, "PRODI");
		$worksheet1->write_string(0,10, "TGL LULUS");
		$worksheet1->write_string(0,11, "ORTU");
		$worksheet1->write_string(0,12, "ALAMAT");
		$worksheet1->write_string(0,13, "EMAIL");
		$worksheet1->write_string(0,14, "NO IJAZAH");
		$worksheet1->write_string(0,15, "KODE FAKULTAS");
		$worksheet1->write_string(0,16, "IPK");
		$worksheet1->write_string(0,17, "TOEFL");

	/*
		echo '
		<table border=1 cellpadding=5 cellspacing=0>
		<tr>
			<td>Urut</td>
			<td>Gelar depan</td>
			<td>Nama</td>
			<td>Gelar belakang</td>
			<td>Kelamin</td>
			<td>Nim</td>
			<td>TTL</td>
			<td>Jenjang</td>
			<td>Fakultas</td>
			<td>Prodi</td>
			<td>Tgl Lulus</td>
			<td>Ortu</td>
			<td>Alamat</td>
			<td>Email</td>
			<td>No Ijazah</td>
			<td>Kode Fakultas</td>
		</tr>
		';
	*/
	$query = "
		SELECT pg.gelar_depan as gelar_dpn, pg.nm_pengguna as nama, (case when pg.gelar_belakang is null then '' else ', ' || pg.gelar_belakang end) as gelar_blk, (case when pg.kelamin_pengguna = '1' then 'Laki-Laki' else 'Perempuan' end)as kelamin, mh.nim_mhs as nim, pw.lahir_ijazah as ttl, jj.nm_jenjang as jenjang, fk.nm_fakultas as fakultas, ps.nm_program_studi as prodi, to_char(pw.tgl_lulus_pengajuan,'dd-mm-yyyy') as tgl_lulus, mh.nm_ayah_mhs as ortu, mh.alamat_mhs as alamat, pg.email_pengguna as email, pw.no_ijasah, fk.id_fakultas as kode_fakultas, pw.ipk, pw.elpt
		FROM aucc.pembayaran_wisuda bw join aucc.mahasiswa mh on mh.id_mhs=bw.id_mhs join aucc.pengguna pg on pg.id_pengguna=mh.id_pengguna join aucc.program_studi ps on ps.id_program_studi=mh.id_program_studi join aucc.fakultas fk on fk.id_fakultas=ps.id_fakultas join aucc.jenjang jj on jj.id_jenjang=ps.id_jenjang join aucc.periode_wisuda ww on ww.id_periode_wisuda=bw.id_periode_wisuda left join aucc.pengajuan_wisuda pw on pw.id_mhs=bw.id_mhs where bw.absensi_wisuda=1 and pw.elpt!=0 and ww.id_tarif_wisuda='".sprintf('%d', $_POST["periode"])."' order by ps.id_fakultas,ps.id_jenjang,ps.id_program_studi,mh.nim_mhs,bw.tgl_bayar
	";
	//echo $query;
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt); $urut=0; $baris=0;
	while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
		$urut++; $baris++;
		/*
		echo '
		<tr>
			<td>'.$urut.'</td>
			<td>'.$r[0].'</td>
			<td>'.$r[1].'</td>
			<td>'.$r[2].'</td>
			<td>'.$r[3].'</td>
			<td>'.$r[4].'</td>
			<td>'.$r[5].'</td>
			<td>'.$r[6].'</td>
			<td>'.$r[7].'</td>
			<td>'.$r[8].'</td>
			<td>'.$r[9].'</td>
			<td>'.$r[10].'</td>
			<td>'.$r[11].'</td>
			<td>'.$r[12].'</td>
			<td>'.$r[13].'</td>
			<td>'.$r[14].'</td>
		</tr>
		';
		*/
		$worksheet1->write_string($baris,0, $urut);
		$worksheet1->write_string($baris,1, $r[0]);
		$worksheet1->write_string($baris,2, $r[1]);
		$worksheet1->write_string($baris,3, $r[2]);
		$worksheet1->write_string($baris,4, $r[3]);
		$worksheet1->write_string($baris,5, $r[4]);
		$worksheet1->write_string($baris,6, $r[5]);
		$worksheet1->write_string($baris,7, $r[6]);
		$worksheet1->write_string($baris,8, $r[7]);
		$worksheet1->write_string($baris,9, $r[8]);
		$worksheet1->write_string($baris,10, $r[9]);
		$worksheet1->write_string($baris,11, $r[10]);
		$worksheet1->write_string($baris,12, $r[11]);
		$worksheet1->write_string($baris,13, $r[12]);
		$worksheet1->write_string($baris,14, $r[13]);
		$worksheet1->write_string($baris,15, $r[14]);
		$worksheet1->write_string($baris,16, $r[15]);
		$worksheet1->write_string($baris,17, $r[16]);
	}
	//echo '</table>';
	$workbook->close();
}

?>