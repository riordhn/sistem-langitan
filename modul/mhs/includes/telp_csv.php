<?php
require '../../../config.php';

	$db = new MyOracle();

	//tes koneksi
	if ($db->conn == false){
		echo OCIError($db->conn)."Not Connected!";
		exit;
	} else {
		// echo "<br>success connecting to my oracle databases";
	} 
	//mengambil koneksi dari class
	$conn = $db->conn;

date_default_timezone_set('Asia/Jakarta');      // Timezone set ke Asia/Jakarta

if(!isset($_POST["ok"]) ) {
	echo '
	<form name=form1 method=post action="">
	<select name="fakul">
		<option value="0">-----</option>
	';
	$query = "select id_fakultas, nm_fakultas from aucc.fakultas order by id_fakultas";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt); $urut=0; $baris=0;
	while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
		echo '<option value="'.$r[0].'" '.(($_POST[""]==$r[0])? 'selected':'').'>'.$r[1].'</option>';
	}
	echo '
	</select>
	<input type=submit name="ok" value="Ambil Data">
	</form>
	';

}else if(isset($_POST["ok"]) ) {
	if(!empty($_POST["fakul"]) ) {
		$param = " and d.id_fakultas='".sprintf('%d',$_POST["fakul"])."' ";
	}else{
		$param = '';
	}
	//$isicsv = 'Name,Given Name,Additional Name,Family Name,Yomi Name,Given Name Yomi,Additional Name Yomi,Family Name Yomi,Name Prefix,Name Suffix,Initials,Nickname,Short Name,Maiden Name,Birthday,Gender,Location,Billing Information,Directory Server,Mileage,Occupation,Hobby,Sensitivity,Priority,Subject,Notes,Group Membership,E-mail 1 - Type,E-mail 1 - Value,Phone 1 - Type,Phone 1 - Value';
	$isicsv = 'Name,Given Name,Additional Name,Family Name,Yomi Name,Given Name Yomi,Additional Name Yomi,Family Name Yomi,Name Prefix,Name Suffix,Initials,Nickname,Short Name,Maiden Name,Birthday,Gender,Location,Billing Information,Directory Server,Mileage,Occupation,Hobby,Sensitivity,Priority,Subject,Notes,Group Membership,E-mail 1 - Type,E-mail 1 - Value,E-mail 2 - Type,E-mail 2 - Value,E-mail 3 - Type,E-mail 3 - Value,E-mail 4 - Type,E-mail 4 - Value,IM 1 - Type,IM 1 - Service,IM 1 - Value,Phone 1 - Type,Phone 1 - Value,Phone 2 - Type,Phone 2 - Value,Phone 3 - Type,Phone 3 - Value,Phone 4 - Type,Phone 4 - Value,Address 1 - Type,Address 1 - Formatted,Address 1 - Street,Address 1 - City,Address 1 - PO Box,Address 1 - Region,Address 1 - Postal Code,Address 1 - Country,Address 1 - Extended Address,Organization 1 - Type,Organization 1 - Name,Organization 1 - Yomi Name,Organization 1 - Title,Organization 1 - Department,Organization 1 - Symbol,Organization 1 - Location,Organization 1 - Job Description,Website 1 - Type,Website 1 - Value,Custom Field 1 - Type,Custom Field 1 - Value';

	// select d.nm_fakultas, a.thn_angkatan_mhs, a.nim_mhs, b.nm_pengguna, a.mobile_mhs
	$query = "
		select b.nm_pengguna, b.email_pengguna, a.mobile_mhs
		from aucc.mahasiswa a, aucc.pengguna b, aucc.program_studi c, aucc.fakultas d
		where a.id_pengguna=b.id_pengguna and a.id_program_studi=c.id_program_studi and c.id_fakultas=d.id_fakultas
		and a.status_akademik_mhs in (1,2,3) and a.mobile_mhs is not null and a.mobile_mhs!='-' ".$param."
		order by d.id_fakultas, a.nim_mhs
	";
	//echo $query;
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt); $urut=0; $baris=0;
	while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
		$tmp = explode(" ", $r[0]);
		if(count($tmp) >= 3) {
			$depan = $tmp[0]; $tengah = $tmp[1]; $belakang = $tmp[2];
		}else if(count($tmp) == 2) {
			$depan = $tmp[0]; $tengah = $tmp[1]; $belakang = $tmp[1];
		}else if(count($tmp) == 1) {
			$depan = $tmp[0]; $tengah = $tmp[0]; $belakang = $tmp[0];
		}
		$telp = str_replace(',','',$r[2]); $telp = str_replace('-','',$telp);
		$isicsv .= "\n".addslashes($r[0]).','.addslashes($depan).','.addslashes($tengah).','.addslashes($belakang).',,,,,,,,,,,,,,,,,,,,,,,,* ,'.$r[1].',Mobile,'.$telp;

		$isicsv .= "\n".addslashes($r[0]).','.addslashes($depan).','.addslashes($tengah).','.addslashes($belakang).',,,,,,,,,,,,,,,,,,,,,,,*,'.$r[1].',,,,,,,,,,Mobile,'.$telp.',,,,,,,,,,,,,,,,,,,,,,,,,,,';
	}

	$filename = 'telp_csv.csv';
	$handle = fopen($filename, 'w');

	if (fwrite($handle, $isicsv) === FALSE) {
		echo "Gagal !!!";
		exit;
	}else{
		//echo "berhasil";
		echo '
		<script type="text/javascript">
		location.href="'.$filename.'";
		</script>
		';
	}
	fclose($handle);
}

?>