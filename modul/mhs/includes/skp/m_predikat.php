<?php
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMPUSAT"){
	header("location: logout.php");
}else{

	require "templ.php";
	require "util/konek.php";
	require("util/ceking3.php");
	require("util/keamanan.php");


	// ********** MENU KIRI **********
		require "menu_kiri.php";
	// *******************************

	$isi = '';

	if($_GET["edit"]){
		$query = "select id_krp_predikat, nm_krp_predikat, id_jenjang, nilai_min, nilai_max from aucc.krp_predikat where id_krp_predikat='".AngkaSaja(base64_decode($_GET["edit"]))."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$nm_predikat = $r[1]; $jenjang = $r[2];  $nil_min = $r[3];  $nil_max = $r[4];

		$tombol = '
		<input type=hidden name=paramid value="'.$_GET["edit"].'">
		<input type="submit" name="ubah" value="Update">
		<input type="button" name="batal" value="Batal" onclick="location.href=\'m_predikat.php\';">
		';
	}else{
		$kode = "";  $nama = "";
		$tombol = '<input type=submit name=ok value="Tambahkan">';
	}
	$isi .= '
	<div id="welcome" class="post">
		<h2 class="title">Master Predikat</h2>
		<div class="content">
			<TABLE>
			<form name="form2" method="post" action="m_predikat.php">
			<TR>
				<TD>Jenjang</TD>
				<TD>:</TD>
				<TD>
				<select name="jenjang">
					<option value="1" '; if($jenjang=='1'){ $isi .= 'selected'; } $isi .= '>S1</option>
					<option value="5" '; if($jenjang=='5'){ $isi .= 'selected'; } $isi .= '>D3</option>
				</select>
				</TD>
			</TR>
			<TR>
				<TD>Predikat</TD>
				<TD>:</TD>
				<TD><input type=text name="predikat" value="'.$nm_predikat.'"></TD>
			</TR>
			<TR>
				<TD>Rentang Nilai</TD>
				<TD>:</TD>
				<TD>
				<input type=text name="nil_min" value="'.$nil_min.'" size="5"> -
				<input type=text name="nil_max" value="'.$nil_max.'" size="5">
				</TD>
			</TR>
			<TR>
				<TD>&nbsp;</TD>
				<TD>&nbsp;</TD>
				<TD>'.$tombol.'</TD>
			</TR>
			</form>
			</TABLE>
	';
	if($_POST["ok"] and $_POST["jenjang"] and $_POST["predikat"] and isset($_POST["nil_min"]) and $_POST["nil_max"]){
		// cek apakah ada nama sama
		$query = "select count(*) from aucc.krp_predikat where id_jenjang='".AngkaSaja($_POST["jenjang"])."' and nm_krp_predikat='".semua_boleh_kecuali_saja($_POST["predikat"])."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		if($r[0] > 0) {
			$isi .= '<FONT COLOR="red">Nama predikat ganda</FONT>';
		}else{
			$query = "insert into aucc.krp_predikat (id_jenjang, nm_krp_predikat, nilai_min, nilai_max) values (".AngkaSaja($_POST["jenjang"]).", '".semua_boleh_kecuali_saja($_POST["predikat"])."', ".AngkaSaja($_POST["nil_min"]).", ".AngkaSaja($_POST["nil_max"]).") ";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			if(ocirowcount($stmt)>0) {
				$isi .= '<FONT COLOR="blue">Data berhasil ditambahkan</FONT>';
			}else{
				$isi .= '<FONT COLOR="red">Data gagal ditambahkan</FONT>';
			}
		}
	}else if($_POST["ubah"] and $_POST["jenjang"] and $_POST["predikat"] and isset($_POST["nil_min"]) and $_POST["nil_max"] and $_POST["paramid"]){
		// cek apakah ada nama sama
		$query = "select count(*) from aucc.krp_predikat where id_jenjang='".AngkaSaja($_POST["jenjang"])."' and nm_krp_predikat='".semua_boleh_kecuali_saja($_POST["predikat"])."' and id_krp_predikat!='".AngkaSaja(base64_decode($_POST["paramid"]))."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		if($r[0] > 0) {
			$isi .= '<FONT COLOR="red">Nama predikat ganda</FONT>';
		}else{
			$query = "update aucc.krp_predikat set id_jenjang='".AngkaSaja($_POST["jenjang"])."', nm_krp_predikat='".semua_boleh_kecuali_saja($_POST["predikat"])."', nilai_min=".AngkaSaja($_POST["nil_min"]).", nilai_max=".AngkaSaja($_POST["nil_max"])." where id_krp_predikat='".AngkaSaja(base64_decode($_POST["paramid"]))."' ";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			if(ocirowcount($stmt)>0) {
				$isi .= '<FONT COLOR="blue">Data berhasil diubah</FONT>';
			}else{
				$isi .= '<FONT COLOR="red">Data gagal diubah</FONT>';
			}
		}
	}
	$isi .= '
			<br><hr><br>
	';
	if($_GET["hps"]){
		$query = "delete from aucc.krp_predikat where id_krp_predikat='".AngkaSaja(base64_decode($_GET["hps"]))."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		if(ocirowcount($stmt)>0) {
			$isi .= '<FONT COLOR="blue">Data berhasil dihapus</FONT>';
		}else{
			$isi .= '<FONT COLOR="red">Data gagal dihapus</FONT>';
		}
	}
	$isi .= '
			<TABLE width=100% align=center>
			<TR>
				<TD align=center class="judul" width=25%>&nbsp;</TD>
				<TD align=center class="judul">Jenjang</TD>
				<TD align=center class="judul">Predikat</TD>
				<TD align=center class="judul">Rentang Nilai</TD>
			</TR>
	';
		$query = "select a.id_krp_predikat, a.nm_krp_predikat, b.nm_jenjang, a.nilai_min, a.nilai_max from aucc.krp_predikat a, aucc.jenjang b where a.id_jenjang=b.id_jenjang order by b.nm_jenjang,a.nilai_min";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt); $hit=0;
		while($r = oci_fetch_array ($stmt, OCI_BOTH)){
			$hit++;
			if($hit%2==0){
				$gaya = "isian2";
			}else{
				$gaya = "isian3";
			}
			$isi .= '
			<TR>
				<TD align=center class="'.$gaya.'">
				[<A HREF="#?" onclick="if(confirm(\'Yakin Hapus ?\')!=0){ location.href=\'m_predikat.php?hps='.base64_encode($r[0]).'\'; }">hapus</A>]
				[<A HREF="m_predikat.php?edit='.base64_encode($r[0]).'">edit</A>]
				</TD>
				<TD align="center" class="'.$gaya.'">'.$r[2].'</TD>
				<TD class="'.$gaya.'">'.$r[1].'</TD>
				<TD align="center" class="'.$gaya.'">'.$r[3].' - '.$r[4].'</TD>
			</TR>
			';
		}
		
	$isi .= '
			</TABLE>
		</div>
	</div>
	';

	require("template_setting.php");
}

?>