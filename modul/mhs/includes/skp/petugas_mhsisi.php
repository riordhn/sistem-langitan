<?php
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMFAKUL" and $_SESSION["HAK"]!="KMPUSAT" ){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking3.php");
require("util/keamanan.php");


// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************

if($_SESSION["HAK"]=="KMFAKUL") {
	$query = "select b.id_fakultas from aucc.pegawai a, aucc.unit_kerja b where a.id_unit_kerja=b.id_unit_kerja and a.nip_pegawai='".$_SESSION["KODE"]."'";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	$kd_fakul = $r[0];
}else if($_SESSION["HAK"]=="KMPUSAT") {
	$kd_fakul = '0';
}


$isi = '
	<div id="welcome" class="post">
		<h2 class="title">List Mhs Sudah Mengisi SKP</h2>
		<TABLE>
		<form name=form4 method=post action="petugas_mhsisi.php">
		<TR>
			<TD>Prodi</TD>
			<TD>:</TD>
			<TD>
			<select name="prodi">
				<option value="0">Semua</option>
				';
				if($kd_fakul!='0') {
					$query = "select a.id_program_studi, b.nm_jenjang, a.nm_program_studi from aucc.program_studi a, aucc.jenjang b where a.id_jenjang=b.id_jenjang and a.id_fakultas='".$kd_fakul."' order by b.nm_jenjang, a.nm_program_studi";
				}else if($kd_fakul=='0') {
					$query = "select a.id_program_studi, b.nm_jenjang, a.nm_program_studi from aucc.program_studi a, aucc.jenjang b where a.id_jenjang=b.id_jenjang order by b.nm_jenjang, a.nm_program_studi";
				}
				$stmt = oci_parse ($conn, $query);
				oci_execute ($stmt);
				while($r = oci_fetch_array ($stmt, OCI_BOTH)){
					$isi .= '<option value="'.$r[0].'" '; if($_POST["prodi"]==$r[0] or $_GET["prodi"]==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[1].' '.$r[2].'</option>';
				}
				$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD>Urutkan</TD>
			<TD>:</TD>
			<TD>
			<select name="urut">
				<option value="nim" '; if($_POST["urut"]=='nim'){ $isi .= 'selected'; } $isi .= '>NIM</option>
				<option value="point" '; if($_POST["urut"]=='point'){ $isi .= 'selected'; } $isi .= '>Point</option>
			</select>
			</TD>
		</TR>
		<TR>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD><input type=submit name=tam value="Tampil"></TD>
		</TR>
		</form>
		</TABLE><br>
		';
		$param = "";
		if(!empty($_POST["prodi"])) {
			$query = "select count(distinct a.id_mhs) from aucc.krp_khp a, aucc.mahasiswa b where a.id_mhs=b.id_mhs and b.id_program_studi='".AngkaSaja($_POST["prodi"])."' ";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$banyak = $r[0];
			$param .= " and c.id_program_studi='".AngkaSaja($_POST["prodi"])."' ";
		}else if(!empty($_GET["prodi"])) {
			$query = "select count(distinct a.id_mhs) from aucc.krp_khp a, aucc.mahasiswa b where a.id_mhs=b.id_mhs and b.id_program_studi='".AngkaSaja($_GET["prodi"])."' ";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$banyak = $r[0];
			$param .= " and c.id_program_studi='".AngkaSaja($_GET["prodi"])."' ";
		}else{
			if($kd_fakul!='0') {
				$query = "select count(distinct a.id_mhs) from aucc.krp_khp a, aucc.mahasiswa b, aucc.program_studi c where a.id_mhs=b.id_mhs and b.id_program_studi=c.id_program_studi and c.id_fakultas='".AngkaSaja($kd_fakul)."' ";
				$param .= " and c.id_fakultas='".AngkaSaja($kd_fakul)."' ";
			}else if($kd_fakul=='0') {
				$query = "select count(distinct a.id_mhs) from aucc.krp_khp a, aucc.mahasiswa b, aucc.program_studi c where a.id_mhs=b.id_mhs and b.id_program_studi=c.id_program_studi";
				$param .= "";
			}
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$banyak = $r[0];
		}
		$isi .= '
		<TABLE>
		<TR>
			<TD>Ada <FONT SIZE="4" COLOR="blue">'.$banyak.'</FONT> Mahasiswa yang sudah mengisi SKP</TD>
		</TR>
		</TABLE><br>
		<TABLE border=0>
		<TR>
			<TD class="judul" align=center><B>No</B></TD>
			<TD class="judul" align=center><B>Prodi</B></TD>
			<TD class="judul" align=center><B>Nim</B></TD>
			<TD class="judul" align=center><B>Nama</B></TD>
			<TD class="judul" align=center><B>Point</B></TD>
		</TR>
		';
		/*
		if($_GET["hal"]) {
			$halaman = sprintf("%d", $_GET["hal"]);
			$batas = ' limit '.(20*($halaman-1)).',20 ';
			$hit=(20*($halaman-1));
		}else{
			$hit=0;
			$batas = ' limit 0,20 ';
		}
		*/
		if($_POST["urut"]=="nim" or $_GET["urut"]=="nim") {
			$diurutkan = "order by e.nm_jenjang, c.nm_program_studi, b.nim_mhs";
			$diurutkan2 = "&urut=nim";
		}else if($_POST["urut"]=="point" or $_GET["urut"]=="point") {
			$diurutkan = "order by sum(a.skor_krp_khp)";
			$diurutkan2 = "&urut=point";
		}

		/*
		$kueri = "select a.nim,b.nama,sum(skor) as jum,c.prodi_nama,d.nm_jenjang from tr_krp_khp a, tb_mhs b, kd_prodi_nama c, kd_jenja d where a.nim=b.nim and b.kode_b=c.kode_b and c.kd_jenjang=d.kd_jenjang  ".$param_prodi."
		group by a.nim
		".$diurutkan."
		".$batas."";
		*/
		
		$query = "select b.nim_mhs, d.nm_pengguna, e.nm_jenjang, c.nm_program_studi, sum(a.skor_krp_khp)
		from aucc.krp_khp a, aucc.mahasiswa b, aucc.program_studi c, aucc.pengguna d, aucc.jenjang e
		where a.id_mhs=b.id_mhs and b.id_program_studi=c.id_program_studi and b.id_pengguna=d.id_pengguna and c.id_jenjang=e.id_jenjang
		".$param."
		group by b.nim_mhs, d.nm_pengguna, e.nm_jenjang, c.nm_program_studi
		".$diurutkan."
		";
		//echo $query;
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt); $hit=0;
		while($r = oci_fetch_array ($stmt, OCI_BOTH)){
			$hit++;
			if($hit%2==0){
				$gaya = "isian2";
			}else{
				$gaya = "isian3";
			}
			$isi .= '
			<TR>
				<TD class="'.$gaya.'">'.$hit.'</TD>
				<TD class="'.$gaya.'">'.$r[2].' - '.$r[3].'</TD>
				<TD class="'.$gaya.'"><A HREF="petugas_mhsisi_detil.php?mhs='.$r[0].'">'.$r[0].'</A></TD>
				<TD class="'.$gaya.'">'.$r[1].'</TD>
				<TD class="'.$gaya.'" align=right>'.$r[4].'</TD>
			</TR>
			';
		}
		/*
		$bagian = ceil($banyak/20);
		$isi .= '
		<TR>
			<TD colspan=4 align=center>
		';
		if($_POST["prodi"]) {
			$param_link = "&prodi=".$_POST["prodi"]."";
		}else if($_GET["prodi"]) {
			$param_link = "&prodi=".$_GET["prodi"]."";
		}else{
			$param_link = "";
		}

		$paging = "";
		for($w=1; $w<=$bagian; $w++) {
			$paging .= ' - <A HREF="petugas_mhsisi.php?hal='.$w.$param_link.$diurutkan2.'">'.$w.'</A>';
		}

		$isi .= '
			'.substr($paging,3).'
			</TD>
		</TR>
		';
		*/
		$isi .= '
		</TABLE>
';
$isi .= '
	</div>
';

	require("template_setting.php");

}
?>