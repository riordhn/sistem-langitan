<?php
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMFAKUL" and $_SESSION["HAK"]!="KMPUSAT"){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking3.php");
require("util/keamanan.php");


// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************


if(empty($_GET["mhs"])) {
	echo '
	<SCRIPT LANGUAGE="JavaScript">
	location.href="petugas_mhsisi_detil.php";
	</SCRIPT>
	';
}
$nimmhs = AngkaHurufSaja($_GET["mhs"]);

$query = "select a.id_mhs,b.nm_pengguna from aucc.mahasiswa a, aucc.pengguna b where a.id_pengguna=b.id_pengguna and a.nim_mhs='".$nimmhs."' ";
$stmt = oci_parse ($conn, $query);
oci_execute ($stmt);
$r = oci_fetch_array ($stmt, OCI_BOTH);
$idmhs = $r[0];
$namanya = $r[1];


$isi = '
	<div id="welcome" class="post">
		<h2 class="title">List Detail Mhs "'.$namanya.'"</h2>
		<TABLE>
		<TR>
			<TD class="judul" align=center><B>Tahun Akad</B></TD>
			<TD class="judul" align=center><B>Nama Kegiatan</B></TD>
			<TD class="judul" align=center><B>Waktu</B></TD>
			<TD class="judul" align=center><B>Penyelenggara</B></TD>
			<TD class="judul" align=center><B>Skor</B></TD>
		</TR>
		';
		$query = "select b.thn_akademik_semester, b.nm_semester, a.nm_krp_khp, a.waktu_krp_khp, a.penyelenggara_krp_khp, a.skor_krp_khp from aucc.krp_khp a, aucc.semester b where a.id_semester=b.id_semester and a.id_mhs='".$idmhs."' order by b.thn_akademik_semester, b.nm_semester, a.nm_krp_khp";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt); $hit=0; $tot=0;
		while($r = oci_fetch_array ($stmt, OCI_BOTH)) {
			$hit++;
			if($hit%2==0){
				$gaya = "isian2";
			}else{
				$gaya = "isian3";
			}
			$tot += $r[5];
			$isi .= '
			<TR>
				<TD class="'.$gaya.'">'.$r[0].' '.$r[1].'</TD>
				<TD class="'.$gaya.'">'.$r[2].'</TD>
				<TD class="'.$gaya.'">'.$r[3].'</TD>
				<TD class="'.$gaya.'">'.$r[4].'</TD>
				<TD class="'.$gaya.'" align=right>'.$r[5].'</TD>
			</TR>
			';
		}
		
		$isi .= '
		<TR>
			<TD bgcolor="#FFFF99" align=center colspan=3><B>&nbsp;</B></TD>
			<TD bgcolor="#FFFF99" align=center><B>Total</B></TD>
			<TD bgcolor="#FFFF99" align=center>'.$tot.'</TD>
		</TR>
		</TABLE>
';
$isi .= '
	</div>
';

	require("template_setting.php");

}
?>