<?php
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMFAKUL" and $_SESSION["HAK"]!="KMPUSAT"){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking3.php");
require("util/keamanan.php");


// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************


$isi = '';

$isi .= '
	<div id="welcome" class="post">
		<h2 class="title">Cetak Portofolio Mahasiswa</h2>
		<TABLE>
		<form name="form2" method="post" action="petugas_porto.php">
		<TR>
			<TD class="formulir">NIM</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
			<input type=text name="nimmhs" value="'.$_POST["nimmhs"].'">
			</TD>
		</TR>
		<TR>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir"><input type=submit name="ok" value="Tampil"></TD>
		</TR>
		</form>
		</TABLE>
	';
	if($_POST["ok"] and $_POST["nimmhs"]){
		$isi .= '<hr>';
		$query = "select nm_pengguna from aucc.pengguna where username='".AngkaHurufSaja($_POST["nimmhs"])."'";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$nama = $r[0];
		
		$isi .= '
		<TABLE cellspacing=5>
		<TR>
			<TD>Nim</TD>
			<TD>:</TD>
			<TD>'.AngkaHurufSaja($_POST["nimmhs"]).'</TD>
		</TR>
		<TR>
			<TD>Nama</TD>
			<TD>:</TD>
			<TD>'.$nama.'</TD>
		</TR>
		</TABLE>
		<TABLE width=100%>
		<tr>
			<td colspan=3 align=right>[<A HREF="petugas_porto_cetak.php?nim='.base64_encode($_POST["nimmhs"]).'" target="_BLANK">Cetak</A>]</td>
		</tr>
		<TR>
			<TD class="judul" align=center>Tahun</TD>
			<TD class="judul" align=center>Semester</TD>
			<TD class="judul" align=center>Kegiatan</TD>
		</TR>
		';
		$query = "select c.tahun_ajaran, c.nm_semester, a.nm_krp_khp from aucc.krp_khp a, aucc.mahasiswa b, aucc.semester c where a.id_mhs=b.id_mhs and a.id_semester=c.id_semester and b.nim_mhs='".AngkaHurufSaja($_POST["nimmhs"])."' order by c.tahun_ajaran, c.nm_semester, a.nm_krp_khp ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt); $no=0;
		while($r = oci_fetch_array ($stmt, OCI_BOTH)) {
			$no++;
			if($no%2==0){
				$gaya = "isian2";
			}else{
				$gaya = "isian3";
			}
			$isi .= '
			<TR>
				<TD class="'.$gaya.'" align="center">'.$r[0].'</TD>
				<TD class="'.$gaya.'" align="center">'.$r[1].'</TD>
				<TD class="'.$gaya.'">'.$r[2].'</TD>
			</TR>
			';
		}
		$isi .= '
		</table>
		<br>
		';
	}
	$isi .= '
	</div>
';

	require("template_setting.php");
}

?>