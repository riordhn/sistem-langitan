<?php
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMPUSAT" and $_SESSION["HAK"]!="KMFAKUL"){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking3.php");
require("util/keamanan.php");

// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************

$isi = '';

if($_GET["edit"]){
	$kueri = "
	select a.id_kegiatan_2, a.nm_krp_khp, a.waktu_krp_khp, a.penyelenggara_krp_khp, a.id_bukti_fisik, a.id_semester, a.id_mhs,
	c.nm_kegiatan_1, b.id_tingkat, b.id_jabatan_prestasi, b.id_dasar_nilai, d.nm_kelompok_kegiatan
	from aucc.krp_khp a, aucc.kegiatan_2 b, aucc.kegiatan_1 c, aucc.kelompok_kegiatan d
	where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_kegiatan_1=c.id_kegiatan_1 and c.id_kelompok_kegiatan=d.id_kelompok_kegiatan and a.id_krp_khp='".AngkaSaja(base64_decode($_GET["edit"]))."' ";
	
	$stmt = oci_parse ($conn, $kueri);
	oci_execute ($stmt)or die("salah 1");
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	$kode_kegiatan_2 = $r[0];
	$keg_nama = $r[1];
	$keg_waktu = $r[2];
	$keg_penyelenggara = $r[3];
	$keg_bukti = $r[4];
	$thakad = $r[5];
	$idmhs = $r[6];
	$kk = $r[11];
	$jk = $r[7];
	$tingkat_id = $r[8];
	$jabatan_id = $r[9];

	$tingkat_nama="";
	$kueri = "select nm_tingkat from aucc.tingkat where id_tingkat='".$tingkat_id."'  ";
	$stmt = oci_parse ($conn, $kueri);
	oci_execute ($stmt)or die("salah 2");
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	$tingkat_nama = $r[0];

	$jabatan_nama="";
	$kueri = "select nm_jabatan_prestasi from aucc.jabatan_prestasi where id_jabatan_prestasi='".$jabatan_id."'  ";
	$stmt = oci_parse ($conn, $kueri);
	oci_execute ($stmt)or die("salah 2");
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	$jabatan_nama = $r[0];

	$kueri = "select a.nm_pengguna, b.nim_mhs from aucc.pengguna a, aucc.mahasiswa b where a.id_pengguna=b.id_pengguna and b.id_mhs='".$idmhs."' ";
	$stmt = oci_parse ($conn, $kueri);
	oci_execute ($stmt)or die("salah 2");
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	$nama_mhs = $r[0];
	$nim = $r[1];
	
}else if($_POST["ok"] and $_POST["hid"]){
	$kueri = "select b.nim_mhs from aucc.krp_khp a, aucc.mahasiswa b where a.id_mhs=b.id_mhs and a.id_krp_khp='".AngkaSaja(base64_decode($_POST["hid"]))."'";
	$stmt = oci_parse ($conn, $kueri);
	oci_execute ($stmt)or die("salah 2");
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	$nim = $r[0];
}

$isi .= '
<div id="welcome" class="post">
	<h2 class="title">TKM Mhs "<I>'.$nama_mhs.'</I>"</h2>
	<div class="content">
		<TABLE>
		<form name="form2" method="post" action="petugas_revisi_tkk_2_edit.php">
		<TR>
				<TD class="formulir">Tahun</TD>
				<TD class="formulir">:</TD>
				<TD class="formulir">
				<select name="tahun">
					';
					$query = "select thn_akademik_semester, nm_semester, id_semester from aucc.semester where nm_semester in ('Ganjil','Genap') order by thn_akademik_semester desc, nm_semester desc";
					$stmt = oci_parse ($conn, $query);
					oci_execute ($stmt);
					while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
						$isi .= '<option value="'.$r[2].'" '; if($thakad==$r[2]){ $isi .= 'selected'; } $isi .= '>'.$r[0].' - '.$r[1].'</option>';
					}				
					$isi .= '
				</select>
				</TD>
			</TR>
		<TR>
			<TD class="formulir">Kelompok Kegiatan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">'.$kk.'</TD>
		</TR>
		<TR>
			<TD class="formulir">Jenis Kegiatan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">'.$jk.'</TD>
		</TR>
		<TR>
			<TD class="formulir">Tingkat</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">'.$tingkat_nama.'</TD>
		</TR>
		<TR>
			<TD class="formulir">Prestasi/Partisipasi/Jabatan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">'.$jabatan_nama.'</TD>
		</TR>
		<TR>
			<TD class="formulir">Nama Kegiatan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir"><input type=text name=kegiatan_nama value="'.$keg_nama.'" size="60"></TD>
		</TR>
		<TR>
			<TD class="formulir">Waktu Pelaksanaan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir"><input type=text name=kegiatan_waktu value="'.$keg_waktu.'" size="10"></TD>
		</TR>
		<TR>
			<TD class="formulir">Penyelenggara</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir"><input type=text name=kegiatan_penyelenggara value="'.$keg_penyelenggara.'" size="50"></TD>
		</TR>
		<TR>
			<TD class="formulir">Bukti Fisik</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
				<select name="bukti">
				<option value="0">-----</option>
				';
				$kueri = "select id_bukti_fisik, nm_bukti_fisik from bukti_fisik order by nm_bukti_fisik";
				$stmt = oci_parse ($conn, $kueri);
				oci_execute ($stmt); $tingkat_ada=false;
				while($r = oci_fetch_array ($stmt, OCI_BOTH)){
					$isi .= '<option value="'.$r[0].'" '; if($keg_bukti==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
				}
			
				$isi .= '
				</select> Nomor : <input type=text name="no_bukti_fisik" value="'.$no_bukti.'" size="30">
			</TD>
		</TR>
		<TR>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir">
			<input type=hidden name=hid value="'.$_GET["edit"].'">
			<input type=submit name=ok value="Update">
			<input type=button name=balik value="Kembali" onclick="location.href=\'petugas_revisi_tkk_2.php?param='.base64_encode($nim).'\';">
			</TD>
		</TR>
		</form>
		</TABLE>
';
if($_POST["ok"] and $_POST["kegiatan_nama"] and $_POST["kegiatan_penyelenggara"] and $_POST["kegiatan_waktu"] and $_POST["hid"] and $_POST["tahun"]){
	// ambil id_mhs
	$query = "select id_pengguna from aucc.pengguna where username='".$_SESSION["KODE"]."'";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	$id_pengguna = $r[0];

	$kueri = "Update aucc.krp_khp set id_semester='".AngkaSaja($_POST["tahun"])."', nm_krp_khp='".semua_boleh_kecuali_saja($_POST["kegiatan_nama"])."', waktu_krp_khp='".semua_boleh_kecuali_saja($_POST["kegiatan_waktu"])."', penyelenggara_krp_khp='".semua_boleh_kecuali_saja($_POST["kegiatan_penyelenggara"])."', id_bukti_fisik='".AngkaSaja($_POST["bukti"])."', tgl_insert=TO_DATE('".date("YmdHis")."', 'YYYYMMDDHH24MISS'), ID_PENGGUNA=".$id_pengguna." where id_krp_khp='".AngkaSaja(base64_decode($_POST["hid"]))."'";
	$stmt = oci_parse ($conn, $kueri);
	oci_execute ($stmt); $tingkat_ada=false;
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	if(ocirowcount($stmt)>0) {
		$isi .= '<FONT COLOR="blue">Data berhasil di-Update</FONT>';
	}else{
		$isi .= '<FONT COLOR="red">Data gagal di-Update</FONT>';
	}
}
$isi .= '
		<br><hr><br>
		Catatan :<br>
		Yang bisa di-edit adalah data <font color="blue"><I>"tahun", "semester", "nama kegiatan", "waktu pelaksanaan", "penyelenggara", "bukti fisik"</I></font>
';	
$isi .= '
	</div>
</div>
';

	require("template_setting.php");

}
?>