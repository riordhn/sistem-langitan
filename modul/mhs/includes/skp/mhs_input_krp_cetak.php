<?
require("util/ceking3.php");
require("util/keamanan.php");
?>
<BODY>
<?
if($_GET["param"]) {
	// cek input
	$lanjut=true;

	if($lanjut==true) {
		require("util/konek.php");
		$tmp_p = explode("&",base64_decode($_GET["param"]));
		$nim = AngkaHurufSaja($tmp_p[0]); $thakad = AngkaSaja($tmp_p[1]);
		if(!empty($tmp_p[2]) ) {
			$judulnya = HurufSaja($tmp_p[2]);
		}else{
			$judulnya = '';
		}

		$query = "select tahun_ajaran, nm_semester from aucc.semester where id_semester='".$thakad."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$tahun = $r[0]; $semester = $r[1];

		$query = "select e.nm_pengguna, c.nm_fakultas, d.nm_jenjang, b.nm_program_studi, a.id_mhs from aucc.mahasiswa a, aucc.program_studi b, aucc.fakultas c, aucc.jenjang d, aucc.pengguna e where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and b.id_jenjang=d.id_jenjang and a.id_pengguna=e.id_pengguna and a.nim_mhs='".$nim."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$nama = $r[0];
		$prodi = $r[3];
		$fakultas = $r[1];
		$jenjang = $r[2];
		$idmhsnya = $r[4];
		//$wali_nip = $r[4];
		//$wali_nama = $r[5];
		
		echo '
		<div align=center><font size=4><B>UNIVERSITAS AIRLANGGA<br>Fakultas '.$fakultas.'<br>'.(($judulnya=='KHP')?'Kartu Hasil Prestasi (KHP)':'Kartu Rencana Prestasi (KRP)').'</B></font></div><br>
		<TABLE width=98% cellpadding=2 cellspacing=0 align=center>
		<TR>
			<TD width=14% valign=top>Nama</TD>
			<TD width=1% valign=top>:</TD>
			<TD width=35% valign=top>'.$nama.'</TD>
			<TD width=15%>Pro.Studi</TD>
			<TD width=1%>:</TD>
			<TD width=34%>'.$jenjang.'-'.$prodi.'</TD>
		</TR>
		<TR>
			<TD>NIM</TD>
			<TD>:</TD>
			<TD>'.$nim.'</TD>
			<TD>Semester</TD>
			<TD>:</TD>
			<TD>'.$tahun.' '.$semester.'</TD>
		</TR>
		</TABLE>
		';
		echo '<br>
		<TABLE width=98% cellpadding=3 cellspacing=0 border=1 align=center>
		<TR>
			<TD align=center width=3%><B>No</B></TD>
			<TD align=center><B>Nama Kegiatan</B></TD>
			<TD align=center width=6%><B>Nilai skp</B></TD>
			<TD align=center width=15%><B>Perkiraan Waktu Pelaksanaan</B></TD>
			<TD align=center width=15%><B>Penyelenggara</B></TD>
		</TR>
		<TR>
			<TD align=center>(1)</TD>
			<TD align=center>(2)</TD>
			<TD align=center>(3)</TD>
			<TD align=center>(4)</TD>
			<TD align=center>(5)</TD>
		</TR>
		';
		$query = "select id_kelompok_kegiatan,nm_kelompok_kegiatan from aucc.KELOMPOK_KEGIATAN order by id_kelompok_kegiatan ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$no=0; $jum_skp_tot=0;
		while($r = oci_fetch_array ($stmt, OCI_BOTH)){
			echo '
			<TR>
				<TD colspan=5 STYLE="background:#E0E0E0"><B>'.$r[1].'</B></TD>
			</TR>
			';
			$query2 = "select a.nm_krp_khp, a.penyelenggara_krp_khp, a.waktu_krp_khp, a.skor_krp_khp from aucc.krp_khp a, aucc.kegiatan_2 b, aucc.kegiatan_1 c where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_kegiatan_1=c.id_kegiatan_1 and a.id_mhs='".$idmhsnya."' and a.id_semester='".$thakad."' and c.id_kelompok_kegiatan='".$r[0]."' ";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2); $jum_skp=0;
			while($r2 = oci_fetch_array ($stmt2, OCI_BOTH)){
				$no++;
				echo '
				<TR>
					<TD>'.$no.'</TD>
					<TD>'.$r2[0].'</TD>
					<TD align=center>'.$r2[3].'</TD>
					<TD>'.$r2[2].'</TD>
					<TD>'.$r2[1].'</TD>
				</TR>
				';
				$jum_skp += $r2[3];
			}
			$jum_skp_tot += $jum_skp;
			echo '
			<TR>
				<TD>&nbsp;</TD>
				<TD>&nbsp;</TD>
				<TD>&nbsp;</TD>
				<TD>&nbsp;</TD>
				<TD>&nbsp;</TD>
			</TR>
			<TR>
				<TD STYLE="background:#E0E0E0">&nbsp;</TD>
				<TD align=right><I>Jumlah</I></TD>
				<TD align=center><I>'.$jum_skp.'</I></TD>
				<TD colspan=2 STYLE="background:#E0E0E0">&nbsp;</TD>
			</TR>
			<TR>
				<TD colspan=5>&nbsp;</TD>
			</TR>
			';
		}
		
		echo '
		<TR>
			<TD STYLE="background:#E0E0E0">&nbsp;</TD>
			<TD align=right><I>Total SKP</I></TD>
			<TD align=center><I>'.$jum_skp_tot.'</I></TD>
			<TD colspan=2 STYLE="background:#E0E0E0">&nbsp;</TD>
		</TR>
		</TABLE><br>
		';
		echo '
		<TABLE width=90% cellpadding=2 cellspacing=0 align=center>
		<TR>
			<TD width=50%>Menyetujui,</TD>
			<TD width=50%>Surabaya, '.date("d-m-Y").'</TD>
		</TR>
		<TR>
			<TD>Dosen Wali</TD>
			<TD>&nbsp;</TD>
		</TR>
		<TR>
			<TD colspan=2><BR><BR></TD>
		</TR>
		<TR>
			<TD>.........................................</TD>
			<TD>..........................................</TD>
		</TR>
		<TR>
			<TD>NIP :.................................</TD>
			<TD>NIM :.................................</TD>
		</TR>
		</TABLE>
		';
	}
}
?>
</BODY>
</HTML>
