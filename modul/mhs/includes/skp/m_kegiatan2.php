<?php
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMPUSAT"){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking3.php");
require("util/keamanan.php");


// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************

$isi = '';

if($_GET["edit"]){
	$query = "select a.id_kegiatan_2, b.id_kelompok_kegiatan, b.id_kegiatan_1, a.id_tingkat, a.id_jabatan_prestasi, a.id_dasar_nilai, a.bobot_kegiatan_2 from aucc.kegiatan_2 a, aucc.kegiatan_1 b where a.id_kegiatan_1=b.id_kegiatan_1 and a.id_kegiatan_2='".AngkaSaja(base64_decode($_GET["edit"]))."' ";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	$r = oci_fetch_array ($stmt, OCI_BOTH);
		$kode_idkelompok = $r[1];
		$kode_idkegiatan = $r[2];
		$kode_idtingkat = $r[3];
		$kode_idjabpres = $r[4];
		$kode_iddasarnilai = $r[5];
		$kode_bobot = $r[6];

	$tombol = '
	<input type=hidden name=paramid value="'.$_GET["edit"].'">
	<input type=submit name=ubah value="Update">
	<input type="Button" name="batal" value="Batal" onclick="location.href=\'m_kegiatan_2.php\';">
	';
}else if($_POST["ok"]){
	$kode_idkelompok = $_POST["kode_kk"];
	$kode_idkegiatan = $_POST["kode_jk"];
	$kode_idtingkat = $_POST["kode_tingkat"];
	$kode_idjabpres = $_POST["kode_jabatan"];
	$kode_iddasarnilai = $_POST["kode_penilaian"];
	$kode_bobot = "0";
	$tombol = '<input type=submit name=ok value="Tambahkan">';
}else{
	$kode_idkelompok = "0";
	$kode_idkegiatan = "0";
	$kode_idtingkat = "00";
	$kode_idjabpres = "00";
	$kode_iddasarnilai = "00";
	$kode_bobot = "0";
	$tombol = '<input type=submit name=ok value="Tambahkan"><input type=button name=tampil value="Cetak" onclick="location.href=\'list_jeniskegiatan.php\';">';
}
$isi .= '
<div id="welcome" class="post">
	<h2 class="title">Master Kegiatan</h2>
	<div class="content">
		<TABLE>
		<form name="form2" method="post" action="m_kegiatan2.php">
		<TR>
			<TD>Kelompok Kegiatan</TD>
			<TD>:</TD>
			<TD>
			<select name="kode_kk" onchange="submit()">
			<option value="0">-----</option>
				';
				$query = "select id_kelompok_kegiatan, nm_kelompok_kegiatan from aucc.kelompok_kegiatan order by id_kelompok_kegiatan asc";
				$stmt = oci_parse ($conn, $query);
				oci_execute ($stmt);
				while($r = oci_fetch_array ($stmt, OCI_BOTH)){
					$isi .= '<option value="'.$r[0].'" '; if($kode_idkelompok==$r[0] or $_POST["kode_kk"]==$r[0] ){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
				}
				$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD>Jenis Kegiatan</TD>
			<TD>:</TD>
			<TD>
			<select name="kode_jk">
			<option value="0">-----</option>
			';
			if($_GET["edit"]){
				$tmp_kelompok = $kode_idkelompok;
			}else if($_POST["kode_kk"]){
				$tmp_kelompok = $_POST["kode_kk"];
			}
			$query = "select id_kegiatan_1, nm_kegiatan_1 from aucc.kegiatan_1 where id_kelompok_kegiatan='".AngkaSaja($tmp_kelompok)."' order by id_kegiatan_1 asc";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			while($r = oci_fetch_array ($stmt, OCI_BOTH)){
				$isi .= '<option value="'.$r[0].'" '; if($kode_idkegiatan==$r[0] or $_POST["kode_jk"]==$r[0] ) { $isi .= 'selected'; } $isi .= '>'.substr($r[1],0,70).'</option>';
			}
			$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD>Tingkat</TD>
			<TD>:</TD>
			<TD>
			<select name="kode_tingkat">
			<option value="0">-----</option>
			';
			$query = "select id_tingkat, nm_tingkat from aucc.tingkat order by id_tingkat asc";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			while($r = oci_fetch_array ($stmt, OCI_BOTH)){
				$isi .= '<option value="'.$r[0].'" '; if($kode_idtingkat==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
			}
			$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD>Prestasi/Partisipasi/Jabatan</TD>
			<TD>:</TD>
			<TD>
			<select name="kode_jabatan">
			<option value="0">-----</option>
			';
			$query = "select id_jabatan_prestasi, nm_jabatan_prestasi from aucc.jabatan_prestasi order by id_jabatan_prestasi asc";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			while($r = oci_fetch_array ($stmt, OCI_BOTH)){
				$isi .= '<option value="'.$r[0].'" '; if($kode_idjabpres==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
			}
			$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD>Dasar Penilaian</TD>
			<TD>:</TD>
			<TD>
			<select name="kode_penilaian">
			<option value="0">-----</option>
			';
			$query = "select id_dasar_nilai, dasar_penilaian from aucc.dasar_nilai order by id_dasar_nilai asc";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			while($r = oci_fetch_array ($stmt, OCI_BOTH)){
				$isi .= '<option value="'.$r[0].'" '; if($kode_iddasarnilai==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
			}
			$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD>Bobot</TD>
			<TD>:</TD>
			<TD><input type=text name="bobot" value="'.$kode_bobot.'"></TD>
		</TR>
		<TR>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD>'.$tombol.'</TD>
		</TR>
		</form>
		</TABLE>
';
if($_POST["ok"] and $_POST["bobot"] and $_POST["kode_jk"]  and $_POST["kode_kk"]){
	// cek apakah ada nama sama
	$query = "select count(*) from aucc.kegiatan_2 where id_kegiatan_1='".AngkaSaja($_POST["kode_jk"])."' and id_tingkat='".AngkaSaja($_POST["kode_tingkat"])."' and id_jabatan_prestasi='".AngkaSaja($_POST["kode_jabatan"])."' and id_dasar_nilai='".AngkaSaja($_POST["kode_penilaian"])."'";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	if($r[0] > 0) {
		$isi .= '<FONT COLOR="red">Proses Gagal. Data Ganda !!!</FONT>';
	}else{
		$query = "insert into aucc.kegiatan_2 (id_kegiatan_1, id_tingkat, id_jabatan_prestasi, id_dasar_nilai, bobot_kegiatan_2) values (".AngkaSaja($_POST["kode_jk"]).", ".AngkaSaja($_POST["kode_tingkat"]).", ".AngkaSaja($_POST["kode_jabatan"]).", ".AngkaSaja($_POST["kode_penilaian"]).", ".AngkaSaja($_POST["bobot"]).") ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		if(ocirowcount($stmt)>0) {
			$isi .= '<FONT COLOR="blue">Data berhasil ditambahkan</FONT>';
		}else{
			$isi .= '<FONT COLOR="red">Data gagal ditambahkan</FONT>';
		}
	}
}else if($_POST["ubah"] and $_POST["bobot"] and $_POST["kode_jk"]  and $_POST["kode_kk"] and $_POST["paramid"]){
	// cek apakah ada nama sama
	$query = "select count(*) from aucc.kegiatan_2 where id_kegiatan_1='".AngkaSaja($_POST["kode_jk"])."' and id_tingkat='".AngkaSaja($_POST["kode_tingkat"])."' and id_jabatan_prestasi='".AngkaSaja($_POST["kode_jabatan"])."' and id_dasar_nilai='".AngkaSaja($_POST["kode_penilaian"])."' and id_kegiatan_2!='".AngkaSaja(base64_decode($_POST["paramid"]))."'";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	if($r[0] > 0) {
		$isi .= '<FONT COLOR="red">Proses Gagal. Data Ganda !!!</FONT>';
	}else{
		$query = "update aucc.kegiatan_2 set id_kegiatan_1='".AngkaSaja($_POST["kode_jk"])."', id_tingkat='".AngkaSaja($_POST["kode_tingkat"])."', id_jabatan_prestasi='".AngkaSaja($_POST["kode_jabatan"])."', id_dasar_nilai='".AngkaSaja($_POST["kode_penilaian"])."' where id_kegiatan_2='".AngkaSaja(base64_decode($_POST["paramid"]))."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		if(ocirowcount($stmt)>0) {
			$isi .= '<FONT COLOR="blue">Data berhasil diubah</FONT>';
		}else{
			$isi .= '<FONT COLOR="red">Data gagal diubah</FONT>';
		}
	}
}
$isi .= '
		<br><hr><br>
';
if($_GET["hps"]){
	$query = "delete from aucc.kegiatan_2 where id_kegiatan_2='".AngkaSaja(base64_decode($_GET["hps"]))."' ";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	if(ocirowcount($stmt)>0) {
		$isi .= '<FONT COLOR="blue">Data berhasil dihapus</FONT>';
	}else{
		$isi .= '<FONT COLOR="red">Data gagal dihapus</FONT>';
	}
}
$isi .= '
		<TABLE width=100% align=center>
		<TR>
			<TD align=center class="judul" width=25%><A HREF="m_kegiatan_list.php" target="_BLANK">&nbsp;</A></TD>
			<TD align=center class="judul">KELOMPOK</TD>
			<TD align=center class="judul">JENIS KEGIATAN</TD>
			<TD align=center class="judul">TINGKAT</TD>
			<TD align=center class="judul">JABATAN/ PRESTASI</TD>
			<TD align=center class="judul">DASAR NILAI</TD>
			<TD align=center class="judul">BOBOT</TD>
		</TR>
';
	$query = "select c.id_kegiatan_2, a.nm_kelompok_kegiatan, b.nm_kegiatan_1, c.id_tingkat, c.id_jabatan_prestasi, d.dasar_penilaian, c.bobot_kegiatan_2
	from aucc.kelompok_kegiatan a, aucc.kegiatan_1 b, aucc.kegiatan_2 c, aucc.dasar_nilai d
	where a.id_kelompok_kegiatan=b.id_kelompok_kegiatan and b.id_kegiatan_1=c.id_kegiatan_1 and c.id_dasar_nilai=d.id_dasar_nilai
	order by a.id_kelompok_kegiatan, b.id_kegiatan_1, c.id_kegiatan_2";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt); $hit=0;
	while($r = oci_fetch_array ($stmt, OCI_BOTH)){
		$hit++;
		if($hit%2==0){
			$gaya = "isian2";
		}else{
			$gaya = "isian3";
		}
		$tingkat = "";
		$query2 = "select nm_tingkat from aucc.tingkat where id_tingkat='".$r[3]."'";
		$stmt2 = oci_parse ($conn, $query2);
		oci_execute ($stmt2);
		$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
		$tingkat = $r2[0];

		$jabatan_prestasi = "";
		$query2 = "select nm_jabatan_prestasi from aucc.jabatan_prestasi where id_jabatan_prestasi='".$r[4]."'";
		$stmt2 = oci_parse ($conn, $query2);
		oci_execute ($stmt2);
		$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
		$jabatan_prestasi = $r2[0];
		
		$isi .= '
		<TR>
			<TD align=center class="'.$gaya.'">
			[<A HREF="#?" onclick="if(confirm(\'Yakin Hapus ?\')!=0){ location.href=\'m_kegiatan2.php?hps='.base64_encode($r[0]).'\'; }">hapus</A>]
			[<A HREF="m_kegiatan2.php?edit='.base64_encode($r[0]).'">edit</A>]
			</TD>
			<TD align=center class="'.$gaya.'">'.$r[1].'</TD>
			<TD align=center class="'.$gaya.'">'.$r[2].'</TD>
			<TD align=center class="'.$gaya.'">'.$tingkat.'</TD>
			<TD align=center class="'.$gaya.'">'.$jabatan_prestasi.'</TD>
			<TD align=center class="'.$gaya.'">'.$r[5].'</TD>
			<TD align=center class="'.$gaya.'">'.$r[6].'</TD>
		</TR>
		';
	}
	
$isi .= '
		</TABLE>
		<br>
		<U>Catatan</U> : <br>yang bisa di-update adalah data <FONT COLOR="blue">"Prestasi/Partisipasi/Jabatan", "Tingkat", "Dasar Penilaian", "Nama Kegiatan", dan "Bobot"</FONT>
	</div>
</div>
';


	require("template_setting.php");

}
?>