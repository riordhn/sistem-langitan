<?php
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMFAKUL"){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking3.php");
require("util/keamanan.php");


// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************

$query = "select b.id_fakultas from aucc.pegawai a, aucc.unit_kerja b where a.id_unit_kerja=b.id_unit_kerja and a.nip_pegawai='".$_SESSION["KODE"]."'";
$stmt = oci_parse ($conn, $query);
oci_execute ($stmt);
$r = oci_fetch_array ($stmt, OCI_BOTH);
$fakulta = $r[0];

$isi = '
	<div id="welcome" class="post">
		<h2 class="title">List Validasi SKP</h2>
		<TABLE>
		<form name="form2" method="post" action="petugas_listval.php">
		<TR>
			<TD class="formulir">Th Akad</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
			<select name="thakad">
				';
				$query = "select thn_akademik_semester, nm_semester, id_semester from aucc.semester where nm_semester in ('Ganjil','Genap') order by thn_akademik_semester desc, nm_semester desc";
				$stmt = oci_parse ($conn, $query);
				oci_execute ($stmt);
				while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
					$isi .= '<option value="'.$r[2].'" '; if($_POST["thakad"]==$r[2]){ $isi .= 'selected'; } $isi .= '>'.$r[0].' - '.$r[1].'</option>';
				}				
				$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir"><input type=submit name="ok" value="Tampil"></TD>
		</TR>
		</form>
		</TABLE>
		';
		if($_POST["ok"] and $_POST["thakad"] ) {
			$thakad = AngkaSaja($_POST["thakad"]);

			$query = "select tahun_ajaran, nm_semester from aucc.semester where id_semester='".$thakad."' ";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$tahun = $r[0]; $semester = $r[1];

			$isi .= '
			<hr>
			<font size=3>List Validasi SKP Tahun '.$tahun.' semester '.$semester.'</font>
			<font size=3>List <U><font size="4" color="blue">Belum</font></U> validasi</font>
			<TABLE width=80%>
			<TR>
				<TD align=center class="judul"><b>No</b></TD>
				<TD align=center class="judul"><b>Prodi</b></TD>
				<TD align=center class="judul"><b>NIM</b></TD>
				<TD align=center class="judul"><b>Nama</b></TD>
			</TR>
			';
			$query = "select d.nm_jenjang, c.nm_program_studi, b.nim_mhs, e.nm_pengguna
			from aucc.krp_khp a, aucc.mahasiswa b, aucc.program_studi c, aucc.jenjang d, aucc.pengguna e
			where a.id_mhs=b.id_mhs and b.id_program_studi=c.id_program_studi and c.id_jenjang=d.id_jenjang and b.id_pengguna=e.id_pengguna
			and a.id_semester='".$thakad."' and c.id_fakultas='".$fakulta."' and a.skor_krp_khp='0'
			group by d.nm_jenjang, c.nm_program_studi, b.nim_mhs, e.nm_pengguna
			order by d.nm_jenjang, c.nm_program_studi, b.nim_mhs";
			//echo $query;
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt); $hit=0;
			while($r = oci_fetch_array ($stmt, OCI_BOTH)){
				$hit++;
				if($hit%2==0){
					$gaya = "isian2";
				}else{
					$gaya = "isian3";
				}
				$isi .= '
				<TR>
					<TD class="'.$gaya.'">'.$hit.'</TD>
					<TD class="'.$gaya.'">'.$r[0].' - '.$r[1].'</TD>
					<TD class="'.$gaya.'">'.$r[2].'</TD>
					<TD class="'.$gaya.'">'.$r[3].'</TD>
				</TR>
				';
			}
			
			$isi .= '
			</TABLE><br><br>
			<font size=3>List <U><font size="4" color="blue">Sudah</font></U> validasi</font>
			<TABLE width=80%>
			<TR>
				<TD align=center class="judul"><b>No</b></TD>
				<TD align=center class="judul"><b>Prodi</b></TD>
				<TD align=center class="judul"><b>NIM</b></TD>
				<TD align=center class="judul"><b>Nama</b></TD>
			</TR>
			';
			$query = "select d.nm_jenjang, c.nm_program_studi, b.nim_mhs, e.nm_pengguna
			from aucc.krp_khp a, aucc.mahasiswa b, aucc.program_studi c, aucc.jenjang d, aucc.pengguna e
			where a.id_mhs=b.id_mhs and b.id_program_studi=c.id_program_studi and c.id_jenjang=d.id_jenjang and b.id_pengguna=e.id_pengguna
			and a.id_semester='".$thakad."' and c.id_fakultas='".$fakulta."' and a.skor_krp_khp!='0'
			group by d.nm_jenjang, c.nm_program_studi, b.nim_mhs, e.nm_pengguna
			order by d.nm_jenjang, c.nm_program_studi, b.nim_mhs";
			//echo $query;
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt); $hit=0;
			while($r = oci_fetch_array ($stmt, OCI_BOTH)){
				$hit++;
				if($hit%2==0){
					$gaya = "isian2";
				}else{
					$gaya = "isian3";
				}
				$isi .= '
				<TR>
					<TD class="'.$gaya.'">'.$hit.'</TD>
					<TD class="'.$gaya.'">'.$r[0].' - '.$r[1].'</TD>
					<TD class="'.$gaya.'">'.$r[2].'</TD>
					<TD class="'.$gaya.'">'.$r[3].'</TD>
				</TR>
				';
			}

			$isi .= '
			</TABLE>
';
		}
$isi .= '
	</div>
';

	require("template_setting.php");
}

?>