<?
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else{

	require "templ.php";
	require "util/konek.php";
	require("util/ceking2.php");
	require("util/keamanan.php");


	// ********** MENU KIRI **********
		require "menu_kiri.php";
	// *******************************



	$isi = '
		<div id="welcome" class="post">
				<h2 class="title">Home</h2>
				<div class="content">
					<p>
					Alur Aplikasi SKP : 
					<OL>
						<LI>Pada awal semester aktif, mahasiswa mengisi rencana kegiatan yang akan dilakukan pada semester aktif tersebut melalui menu input KRP.</li>
						<LI>Setelah mengisi KRP, mahasiswa dapat melakukan kegiatan akademik (perkuliahan, merealisasikan rencana kegiatan pada KRP, dll)</li>
						<LI>Pada akhir semester aktif, mahasiswa melakukan edit pada data KRP-nya. Mungkin ada rencana kegiatan yang tidak bisa direalisasikan pada semester aktif. Atau mungkin ada kegiatan tambahan dimasukkan ketika mengisi KRP pada awal semester aktif.</li>
						<LI>Dosen wali menyerahkan dokumen KRP mahasiswa yang sudah diacc berserta bukti fisiknya kepada bagian Kemahasiswaan Fakultas.</li>
						<LI>Setelah mendapat dokumen KRP mahasiswa yang sudah diacc berserta bukti fisiknya, kemudian operator Kemahasiswaan Fakultas melakukan edit data KRP yang sudah diinput oleh mahasiswa untuk menjadi data KHP diaplikasi SKP.</li>
						<LI>Setelah proses KHP selesai dilakukan oleh operator kemahasiswaan fakultas, maka data KHP mahasiswa bisa ditampilkan. Data KHP ini bisa dilihat oleh mahasiswa sendiri, atau oleh bagian kemahasiswaan.</li>
						<LI>Proses selesai.</li>
					</OL>
					</p>
				</div>
			</div>
	';



	require("template_setting.php");
}

?>