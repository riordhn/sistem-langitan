<?php
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMPUSAT" and $_SESSION["HAK"]!="KMFAKUL"){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking3.php");
require("util/keamanan.php");


// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************

$isi = '';

$isi .= '
<div id="welcome" class="post">
	<h2 class="title">Pencarian Data Prestasi/Kegiatan Mahasiswa</h2>
	<div class="content">
		<TABLE>
		<form name="form2" method="post" action="petugas_pencarian.php">
		<TR>
			<TD>Kelompok Kegiatan</TD>
			<TD>:</TD>
			<TD>
			<select name="kode_kk" onchange="submit()">
			<option value="0">-----</option>
				';
				$query = "select id_kelompok_kegiatan, nm_kelompok_kegiatan from aucc.kelompok_kegiatan order by id_kelompok_kegiatan";
				$stmt = oci_parse ($conn, $query);
				oci_execute ($stmt); $kk_ada = false;
				while($r = oci_fetch_array ($stmt, OCI_BOTH)){
					$kk_ada = true;
					$isi .= '<option value="'.$r[0].'" '; if(AngkaSaja($_POST["kode_kk"])==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
				}
				$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD>Jenis Kegiatan</TD>
			<TD>:</TD>
			<TD>
			<select name="kode_jk" onchange="submit()">
			<option value="0">-----</option>
				';
				if(isset($_POST["kode_kk"])) {
					$kk = AngkaSaja($_POST["kode_kk"]);
				}else{
					$kk = "0";
				}
				$query = "select id_kegiatan_1, nm_kegiatan_1 from aucc.kegiatan_1 where id_kelompok_kegiatan='".$kk."' order by id_kegiatan_1 ";
				$stmt = oci_parse ($conn, $query);
				oci_execute ($stmt); $jk_ada = false;
				while($r = oci_fetch_array ($stmt, OCI_BOTH)){
					$jk_ada = true;
					$isi .= '<option value="'.$r[0].'" '; if(AngkaSaja($_POST["kode_jk"])==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
				}
				$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD>Tingkat</TD>
			<TD>:</TD>
			<TD>
			<select name="kode_tingkat">
			<option value="0">-----</option>
				';
				if(isset($_POST["kode_jk"])) {
					$jk = AngkaSaja($_POST["kode_jk"]);
				}else{
					$jk = "0";
				}
				$kueri = "select distinct a.id_tingkat,b.nm_tingkat from kegiatan_2 a, tingkat b where a.id_tingkat=b.id_tingkat and a.id_kegiatan_1='".$jk."' order by b.nm_tingkat ";
				$stmt = oci_parse ($conn, $kueri);
				oci_execute ($stmt); $tingkat_ada=false;
				while($r = oci_fetch_array ($stmt, OCI_BOTH)){
					$tingkat_ada = true;
					$isi .= '<option value="'.$r[0].'" '; if(AngkaSaja($_POST["kode_tingkat"])==$r[0] and $lepas==false){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
				}
			
				$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD>Prestasi/Partisipasi/Jabatan</TD>
			<TD>:</TD>
			<TD>
			<select name="kode_jabatan">
			<option value="0">-----</option>
				';
				if(isset($_POST["kode_jk"]) ) {
					$jk = AngkaSaja($_POST["kode_jk"]);
				}else{
					$jk = "0";
				}
				$kueri = "select distinct b.id_jabatan_prestasi, b.nm_jabatan_prestasi from kegiatan_2 a, jabatan_prestasi b where a.id_jabatan_prestasi=b.id_jabatan_prestasi and a.id_kegiatan_1='".$jk."' order by b.nm_jabatan_prestasi ";
				$stmt = oci_parse ($conn, $kueri);
				oci_execute ($stmt); $tingkat_ada=false;
				while($r = oci_fetch_array ($stmt, OCI_BOTH)){
					$isi .= '<option value="'.$r[0].'" '; if(AngkaSaja($_POST["kode_jabatan"])==$r[0] and $lepas==false){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
				}
			
				$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD>Nama Kegiatan</TD>
			<TD>:</TD>
			<TD><input type=text name="nmkeg" value="'.$_POST["nmkeg"].'"></TD>
		</TR>
		<TR>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD><input type=submit name=ok value="Tampil"></TD>
		</TR>
		</form>
		</TABLE>
';
if($_POST["ok"]) {
	$isi .= '<br><hr><br>';
	$param = ""; $satu='0'; $dua='0'; $tiga='0'; $empat='0'; $lima='0'; $enam='0';

	if(!empty($_POST["kode_kk"])) {
		$param .= " and e.id_kelompok_kegiatan = '".AngkaSaja($_POST["kode_kk"])."' ";
		$satu = AngkaSaja($_POST["kode_kk"]);
	}
	if(!empty($_POST["kode_jk"])) {
		$param .= " and e.id_kegiatan_1 = '".AngkaSaja($_POST["kode_jk"])."' ";
		$dua = AngkaSaja($_POST["kode_jk"]);
	}
	if(!empty($_POST["kode_tingkat"])) {
		$param .= " and d.id_tingkat = '".AngkaSaja($_POST["kode_tingkat"])."' ";
		$tiga = AngkaSaja($_POST["kode_tingkat"]);
	}
	if(!empty($_POST["kode_jabatan"])) {
		$param .= " and d.id_jabatan_prestasi = '".AngkaSaja($_POST["kode_jabatan"])."' ";
		$empat = AngkaSaja($_POST["kode_jabatan"]);
	}
	if(!empty($_POST["nmkeg"])) {
		$param .= " and a.nm_krp_khp like '%".semua_boleh_kecuali_saja($_POST["nmkeg"])."%' ";
		$lima = semua_boleh_kecuali_saja($_POST["nmkeg"]);
	}

	if($_SESSION["HAK"]=="KMFAKUL") {
		$query = "select b.id_fakultas from aucc.pegawai a, aucc.unit_kerja b where a.id_unit_kerja=b.id_unit_kerja and a.nip_pegawai='".$_SESSION["KODE"]."'";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$kd_fakul = $r[0];

		$param .= " and f.id_fakultas='".$kd_fakul."' ";
		$enam = $kd_fakul;
	}

	if(strlen($param) == 0) { // biar kalau tidak ada yang diisi, maka tidak tampil semua
		$param = " and 1=2 ";
	}

	$bungkus = base64_encode($satu.";".$dua.";".$tiga.";".$empat.";".str_replace(';','',$lima).";".$enam);

	$isi .= '
	<a href="petugas_pencarian_2.php?param='.$bungkus.'" target="_BLANK">Report Lengkap</a>
	<TABLE align=center width=100%>
	<TR>
		<TD align=center class="judul">TAHUN SEMES</TD>
		<TD align=center class="judul">NIM</TD>
		<TD align=center class="judul">KEGIATAN</TD>
		<TD align=center class="judul">WAKTU</TD>
		<TD align=center class="judul">PENYELENGGARA</TD>
	</TR>
	';
	
	$kueri = "
		select b.thn_akademik_semester, b.nm_semester, c.nim_mhs, a.nm_krp_khp, a.waktu_krp_khp, a.penyelenggara_krp_khp
		from aucc.krp_khp a, aucc.semester b, aucc.mahasiswa c, aucc.kegiatan_2 d, aucc.kegiatan_1 e, aucc.program_studi f
		where a.id_semester=b.id_semester and a.id_mhs=c.id_mhs and a.id_kegiatan_2=d.id_kegiatan_2 and d.id_kegiatan_1=e.id_kegiatan_1 and c.id_program_studi=f.id_program_studi
		".$param."
	";
	$stmt = oci_parse ($conn, $kueri);
	oci_execute ($stmt); $hit=0;
	while($r = oci_fetch_array ($stmt, OCI_BOTH)){
		$hit++;
		if($hit%2==0){
			$gaya = "isian2";
		}else{
			$gaya = "isian3";
		}
		$isi .= '
		<TR>
			<TD align=center class="'.$gaya.'">'.$r[0].' '.$r[1].'</TD>
			<TD align=center class="'.$gaya.'">'.$r[3].'</TD>
			<TD align=center class="'.$gaya.'">'.$r[2].'</TD>
			<TD align=center class="'.$gaya.'">'.$r[4].'</TD>
			<TD align=center class="'.$gaya.'">'.$r[5].'</TD>
		</TR>
		';
	}
	$isi .= '
	</TABLE>
	';

}
$isi .= '
	</div>
</div>
';

	require("template_setting.php");
}

?>