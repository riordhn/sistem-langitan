<?php
function AngkaSaja ($param){
	$pan = strlen($param);
	$hasil = "";
	for($x=0; $x<$pan; $x++) {
		$tmp_char = substr($param,$x,1);
		if($tmp_char=='1' or $tmp_char=='2' or $tmp_char=='3' or $tmp_char=='4' or $tmp_char=='5' or $tmp_char=='6' or $tmp_char=='7' or $tmp_char=='8' or $tmp_char=='9' or $tmp_char=='0' ) {
			$hasil .= substr($param,$x,1);
		}else{
			// lewat
		}
	}
	return $hasil;
}

function HurufSaja ($param){
	$pan = strlen($param);
	$hasil = "";
	for($x=0; $x<$pan; $x++) {
		$tmp_char = strtoupper(substr($param,$x,1));
		if($tmp_char=='A' or $tmp_char=='B' or $tmp_char=='C' or $tmp_char=='D' or $tmp_char=='E' or $tmp_char=='F' or $tmp_char=='G' or $tmp_char=='H' or $tmp_char=='I' or $tmp_char=='J' or $tmp_char=='K' or $tmp_char=='L' or $tmp_char=='M' or $tmp_char=='N' or $tmp_char=='O' or $tmp_char=='P' or $tmp_char=='Q' or $tmp_char=='R' or $tmp_char=='S' or $tmp_char=='T' or $tmp_char=='U' or $tmp_char=='V' or $tmp_char=='W' or $tmp_char=='X' or $tmp_char=='Y' or $tmp_char=='Z' ) {
			$hasil .= substr($param,$x,1);
		}else{
			// lewat
		}
	}
	return $hasil;
}

function AngkaHurufSaja ($param){
	$pan = strlen($param);
	$hasil = "";
	for($x=0; $x<$pan; $x++) {
		$tmp_char = strtoupper(substr($param,$x,1));
		if($tmp_char=='1' or $tmp_char=='2' or $tmp_char=='3' or $tmp_char=='4' or $tmp_char=='5' or $tmp_char=='6' or $tmp_char=='7' or $tmp_char=='8' or $tmp_char=='9' or $tmp_char=='0' or $tmp_char=='A' or $tmp_char=='B' or $tmp_char=='C' or $tmp_char=='D' or $tmp_char=='E' or $tmp_char=='F' or $tmp_char=='G' or $tmp_char=='H' or $tmp_char=='I' or $tmp_char=='J' or $tmp_char=='K' or $tmp_char=='L' or $tmp_char=='M' or $tmp_char=='N' or $tmp_char=='O' or $tmp_char=='P' or $tmp_char=='Q' or $tmp_char=='R' or $tmp_char=='S' or $tmp_char=='T' or $tmp_char=='U' or $tmp_char=='V' or $tmp_char=='W' or $tmp_char=='X' or $tmp_char=='Y' or $tmp_char=='Z' ) {
			$hasil .= substr($param,$x,1);
		}else{
			// lewat
		}
	}
	return $hasil;
}

function semua_boleh_kecuali_saja ($tmp_param){
	$hasil = $tmp_param;
	$hasil = str_replace('--','',$hasil);
	$hasil = str_replace(';','',$hasil);
	$hasil = str_replace('=','',$hasil);
	$hasil = str_replace('|','',$hasil);
	
	return $hasil;
}

function IDSaja ($param){
	$pan = strlen($param);
	$hasil = "";
	for($x=0; $x<$pan; $x++) {
		$tmp_char = strtoupper(substr($param,$x,1));
		if($tmp_char=='1' or $tmp_char=='2' or $tmp_char=='3' or $tmp_char=='4' or $tmp_char=='5' or $tmp_char=='6' or $tmp_char=='7' or $tmp_char=='8' or $tmp_char=='9' or $tmp_char=='0' or $tmp_char=='A' or $tmp_char=='B' or $tmp_char=='C' or $tmp_char=='D' or $tmp_char=='E' or $tmp_char=='F' or $tmp_char=='G' or $tmp_char=='H' or $tmp_char=='I' or $tmp_char=='J' or $tmp_char=='K' or $tmp_char=='L' or $tmp_char=='M' or $tmp_char=='N' or $tmp_char=='O' or $tmp_char=='P' or $tmp_char=='Q' or $tmp_char=='R' or $tmp_char=='S' or $tmp_char=='T' or $tmp_char=='U' or $tmp_char=='V' or $tmp_char=='W' or $tmp_char=='X' or $tmp_char=='Y' or $tmp_char=='Z' or $tmp_char=='.' or $tmp_char=='_' or $tmp_char=='@' ) {
			$hasil .= substr($param,$x,1);
		}else{
			// lewat
		}
	}
	return $hasil;
}

function DomainSaja ($param){
	$pan = strlen($param);
	$hasil = "";
	for($x=0; $x<$pan; $x++) {
		$tmp_char = strtoupper(substr($param,$x,1));
		if($tmp_char=='1' or $tmp_char=='2' or $tmp_char=='3' or $tmp_char=='4' or $tmp_char=='5' or $tmp_char=='6' or $tmp_char=='7' or $tmp_char=='8' or $tmp_char=='9' or $tmp_char=='0' or $tmp_char=='A' or $tmp_char=='B' or $tmp_char=='C' or $tmp_char=='D' or $tmp_char=='E' or $tmp_char=='F' or $tmp_char=='G' or $tmp_char=='H' or $tmp_char=='I' or $tmp_char=='J' or $tmp_char=='K' or $tmp_char=='L' or $tmp_char=='M' or $tmp_char=='N' or $tmp_char=='O' or $tmp_char=='P' or $tmp_char=='Q' or $tmp_char=='R' or $tmp_char=='S' or $tmp_char=='T' or $tmp_char=='U' or $tmp_char=='V' or $tmp_char=='W' or $tmp_char=='X' or $tmp_char=='Y' or $tmp_char=='Z' or $tmp_char=='_' ) {
			$hasil .= substr($param,$x,1);
		}else{
			// lewat
		}
	}
	return $hasil;
}

function PencarianSaja ($param){
	$pan = strlen($param);
	$hasil = "";
	for($x=0; $x<$pan; $x++) {
		$tmp_char = strtoupper(substr($param,$x,1));
		if($tmp_char=='1' or $tmp_char=='2' or $tmp_char=='3' or $tmp_char=='4' or $tmp_char=='5' or $tmp_char=='6' or $tmp_char=='7' or $tmp_char=='8' or $tmp_char=='9' or $tmp_char=='0' or $tmp_char=='A' or $tmp_char=='B' or $tmp_char=='C' or $tmp_char=='D' or $tmp_char=='E' or $tmp_char=='F' or $tmp_char=='G' or $tmp_char=='H' or $tmp_char=='I' or $tmp_char=='J' or $tmp_char=='K' or $tmp_char=='L' or $tmp_char=='M' or $tmp_char=='N' or $tmp_char=='O' or $tmp_char=='P' or $tmp_char=='Q' or $tmp_char=='R' or $tmp_char=='S' or $tmp_char=='T' or $tmp_char=='U' or $tmp_char=='V' or $tmp_char=='W' or $tmp_char=='X' or $tmp_char=='Y' or $tmp_char=='Z' or $tmp_char==' ' or $tmp_char=='.' ) {
			$hasil .= substr($param,$x,1);
		}else{
			// lewat
		}
	}
	return $hasil;
}

function NamaSaja ($param){
	$pan = strlen($param);
	$hasil = "";
	for($x=0; $x<$pan; $x++) {
		$tmp_char = strtoupper(substr($param,$x,1));
		if($tmp_char=='1' or $tmp_char=='2' or $tmp_char=='3' or $tmp_char=='4' or $tmp_char=='5' or $tmp_char=='6' or $tmp_char=='7' or $tmp_char=='8' or $tmp_char=='9' or $tmp_char=='0' or $tmp_char=='A' or $tmp_char=='B' or $tmp_char=='C' or $tmp_char=='D' or $tmp_char=='E' or $tmp_char=='F' or $tmp_char=='G' or $tmp_char=='H' or $tmp_char=='I' or $tmp_char=='J' or $tmp_char=='K' or $tmp_char=='L' or $tmp_char=='M' or $tmp_char=='N' or $tmp_char=='O' or $tmp_char=='P' or $tmp_char=='Q' or $tmp_char=='R' or $tmp_char=='S' or $tmp_char=='T' or $tmp_char=='U' or $tmp_char=='V' or $tmp_char=='W' or $tmp_char=='X' or $tmp_char=='Y' or $tmp_char=='Z' or $tmp_char==' ' or $tmp_char=='.' or $tmp_char==',' ) {
			$hasil .= substr($param,$x,1);
		}else{
			// lewat
		}
	}
	return $hasil;
}

function LinkpartnerSaja ($param){
	$pan = strlen($param);
	$hasil = "";
	for($x=0; $x<$pan; $x++) {
		$tmp_char = strtoupper(substr($param,$x,1));
		if($tmp_char=='1' or $tmp_char=='2' or $tmp_char=='3' or $tmp_char=='4' or $tmp_char=='5' or $tmp_char=='6' or $tmp_char=='7' or $tmp_char=='8' or $tmp_char=='9' or $tmp_char=='0' or $tmp_char=='A' or $tmp_char=='B' or $tmp_char=='C' or $tmp_char=='D' or $tmp_char=='E' or $tmp_char=='F' or $tmp_char=='G' or $tmp_char=='H' or $tmp_char=='I' or $tmp_char=='J' or $tmp_char=='K' or $tmp_char=='L' or $tmp_char=='M' or $tmp_char=='N' or $tmp_char=='O' or $tmp_char=='P' or $tmp_char=='Q' or $tmp_char=='R' or $tmp_char=='S' or $tmp_char=='T' or $tmp_char=='U' or $tmp_char=='V' or $tmp_char=='W' or $tmp_char=='X' or $tmp_char=='Y' or $tmp_char=='Z' or $tmp_char=='_' ) {
			$hasil .= substr($param,$x,1);
		}else{
			// lewat
		}
	}
	return $hasil;
}




?>