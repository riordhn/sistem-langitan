<?
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="MHS"){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking2.php");
require("util/keamanan.php");


// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************

$isi = '';
// ambil th akad aktif
$kueri = "select tahun,semester,aktif from tb_thakad where aktif='1'";
$q = mysql_query($kueri);
if(mysql_num_rows($q)>1){
	$isi .= 'Database tahun akademik bermasalah. Harap hubungi DSI ()';
}else if(mysql_num_rows($q)==0){
	$isi .= 'Database tahun akademik bermasalah. Harap hubungi DSI ()';
}else if(mysql_num_rows($q)==1){
	$r = mysql_fetch_row($q);
	$tahun = $r[0];
	$semester = $r[1];
	if($semester=='1'){
		$thakad = "Ganjil";
	}else if($semester=='2'){
		$thakad = "Genap";
	}
}

if($_GET["edit"]){
	$kueri = "select kode_kegiatan_2,nama_kegiatan,waktu,penyelenggara,id_bukti_fisik from tr_krp_khp where row_id='".base64_decode($_GET["edit"])."' ";
	$q = mysql_query($kueri);
	while($r = mysql_fetch_row($q)){
		$kode_kegiatan_2 = $r[0];
		$keg_nama = $r[1];
		$keg_waktu = $r[2];
		$keg_penyelenggara = $r[3];
		$keg_bukti = $r[4];
	}
	// ambil kk
	$kueri = "select kelompok from tb_kelompok_kegiatan where row_id='".substr($kode_kegiatan_2,0,2)."' ";
	$q = mysql_query($kueri);
	while($r = mysql_fetch_row($q)){
		$kk = $r[0];
	}
	// ambil jk
	$kueri = "select nama_kegiatan from tb_kegiatan where kode_kegiatan='".substr($kode_kegiatan_2,0,4)."' ";
	$q = mysql_query($koneksi,$kueri);
	while($r = mysql_fetch_row($q)){
		$jk = $r[0];
	}
	// ambil tingkat
	$kueri = "select tingkat from tb_tingkat where row_id='".substr($kode_kegiatan_2,4,2)."' ";
	$q = mysql_query($kueri);
	while($r = mysql_fetch_row($q)){
		$tingkat = $r[0];
	}
	// ambil jabatan
	$kueri = "select jabatan_prestasi from tb_jabatan_prestasi where row_id='".substr($kode_kegiatan_2,6,2)."' ";
	$q = mysql_query($kueri);
	while($r = mysql_fetch_row($q)){
		$jabatan = $r[0];
	}
	
}

$isi .= '
<div id="welcome" class="post">
	<h2 class="title">KRP Tahun Akademik '.$tahun.' Semester '.$thakad.'</h2>
	<div class="content">
		<TABLE>
		<form name="form2" method="post" action="mhs_input_krp_edit.php">
		<TR>
			<TD class="formulir">Kelompok Kegiatan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">'.$kk.'</TD>
		</TR>
		<TR>
			<TD class="formulir">Jenis Kegiatan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">'.$jk.'</TD>
		</TR>
		<TR>
			<TD class="formulir">Tingkat</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">'.$tingkat.'</TD>
		</TR>
		<TR>
			<TD class="formulir">Prestasi/Partisipasi/Jabatan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">'.$jabatan.'</TD>
		</TR>
		<TR>
			<TD class="formulir">Nama Kegiatan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir"><input type=text name=kegiatan_nama value="'.$keg_nama.'"></TD>
		</TR>
		<TR>
			<TD class="formulir">Waktu Pelaksanaan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir"><input type=text name=kegiatan_waktu value="'.$keg_waktu.'"></TD>
		</TR>
		<TR>
			<TD class="formulir">Penyelenggara</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir"><input type=text name=kegiatan_penyelenggara value="'.$keg_penyelenggara.'"></TD>
		</TR>
		<TR>
			<TD class="formulir">Bukti Fisik</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
			<select name="bukti">
			<option value="0">-----</option>
';
		$kueri = "select row_id,jenis_bukti_fisik from tb_jenis_bukti_fisik order by row_id ";
		$q = mysql_query($kueri);
		while($r = mysql_fetch_row($q)){
			$isi .= '<option value="'.$r[0].'" '; if($keg_bukti==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
		}
		
$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir">
			<input type=hidden name=hid value="'.$_GET["edit"].'">
			<input type=submit name=ok value="Update">
			<input type=button name=balik value="Kembali" onclick="location.href=\'mhs_input_krp.php\';">
			</TD>
		</TR>
		</form>
		</TABLE>
';
if($_POST["ok"] and $_POST["kegiatan_nama"] and $_POST["kegiatan_penyelenggara"] and $_POST["kegiatan_waktu"] and $_POST["hid"]){
	// cek sudah ada
	$kueri = "select count(*) from tr_krp_khp where tahun='".$tahun."' and semester='".$semester."' and nama_kegiatan='".$_POST["kegiatan_nama"]."' and row_id!='".base64_decode($_POST["hid"])."' ";
	$q = mysql_query($kueri);
	$r = mysql_fetch_row($q);
	if($r[0]>0){
		$isi .= '<FONT COLOR="red">Proses Gagal. Data Ganda</FONT>';
	}else{
		$kueri2 = "update tr_krp_khp set nama_kegiatan='".$_POST["kegiatan_nama"]."', waktu='".$_POST["kegiatan_waktu"]."', penyelenggara='".$_POST["kegiatan_penyelenggara"]."', id_bukti_fisik='".$_POST["bukti"]."' where row_id='".base64_decode($_POST["hid"])."' ";
		$q = mysql_query($kueri2);
		if(mysql_affected_rows()>0){
			$isi .= '<FONT COLOR="blue">Data berhasil di-Update</FONT>';
		}else{
			$isi .= '<FONT COLOR="red">Data gagal di-Update</FONT>';
		}
	}
}
$isi .= '
		<br><hr><br>
		Catatan :<br>
		Yang bisa di-edit adalah data <font color="blue"><I>"nama kegiatan", "waktu pelaksanaan", "penyelenggara", "bukti fisik"</I></font>
';	
$isi .= '
	</div>
</div>
';

}
?>