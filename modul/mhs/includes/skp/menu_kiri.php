<?
$javascript="";
$menu='';

if(session_is_registered("KODE")) {
	$identitas="";
	if($_SESSION["HAK"] == "KMPUSAT"){
		$query = "select d.nm_pengguna from aucc.pegawai a, aucc.unit_kerja b, aucc.pengguna d where a.id_unit_kerja=b.id_unit_kerja and a.id_pengguna=d.id_pengguna and a.nip_pegawai='".$_SESSION["KODE"]."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$nama = $r[0];
		$fakul = "Rektorat";
	}else if($_SESSION["HAK"] == "KMFAKUL"){
		$query = "select d.nm_pengguna, c.nm_fakultas from aucc.pegawai a, aucc.unit_kerja b, aucc.fakultas c, aucc.pengguna d where a.id_unit_kerja=b.id_unit_kerja and b.id_fakultas=c.id_fakultas and a.id_pengguna=d.id_pengguna and a.nip_pegawai='".$_SESSION["KODE"]."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$nama = $r[0];
		$fakul = $r[1];
	}else if($_SESSION["HAK"] == "MHS"){
		$query = "select e.nm_pengguna, c.nm_fakultas, d.nm_jenjang, b.nm_program_studi from aucc.mahasiswa a, aucc.program_studi b, aucc.fakultas c, aucc.jenjang d, aucc.pengguna e where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and b.id_jenjang=d.id_jenjang and a.id_pengguna=e.id_pengguna and a.nim_mhs='".$_SESSION["KODE"]."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$nama = $r[0];
		$fakul = $r[1];
		$jenjang = $r[2];
		$prodi = $r[3];
	}

	$tambahan="";
	if($_SESSION["HAK"] == "MHS"){
		$tambahan = '
			<TR>
				<TD valign=top>Prodi</TD>
				<TD valign=top>:</TD>
				<TD>'.$jenjang.'-'.$prodi.'</TD>
			</TR>
		';
	}
	$identitas .= '
		<div id="login" class="boxed">
		<h2 class="title">Identitas</h2>
		<div class="content">
			<TABLE>
			<TR>
				<TD>ID</TD>
				<TD valign=top>:</TD>
				<TD>'.$_SESSION["KODE"].'</TD>
			</TR>
			<TR>
				<TD valign=top>Nama</TD>
				<TD valign=top>:</TD>
				<TD>'.$nama.'</TD>
			</TR>
			<TR>
				<TD valign=top>Fakultas</TD>
				<TD valign=top>:</TD>
				<TD>'.$fakul.'</TD>
			</TR>
			'.$tambahan.'
			<TR>
				<TD valign=top>Hak Akses</TD>
				<TD valign=top>:</TD>
				<TD>'.$_SESSION["HAK"].'</TD>
			</TR>
			</TABLE>
		</div>
		</div>
		<div id="updates" class="boxed">
			<h2 class="title">&nbsp;</h2>
			<div class="content">
				&nbsp;
			</div>
		</div>
	';

	if($_SESSION["HAK"]=="KMPUSAT"){
		$menu .= '
			<div id="menu">
			<ul>
				<li><a href="home.php" title="">Home</a></li>
				<li><a href="#" title=""><b><i>:: SIS. KREDIT PRESTASI :::</i></b></a></li>
				<li><a href="m_kelompok_kegiatan.php" title="">Master Kelompok Kegiatan</a></li>
				<li><a href="m_kegiatan.php" title="">Master Jenis Kegiatan</a></li>
				<li><a href="m_kegiatan2.php" title="">Master Kegiatan</a></li>
				<li><a href="m_tingkat.php" title="">Master Tingkat</a></li>
				<li><a href="m_dasar_penilaian.php" title="">Master Dasar Penilaian</a></li>
				<li><a href="m_jabatan_prestasi.php" title="">Master Prestasi/Partisipasi/Jabatan</a></li>
				<li><a href="m_jenis_buktifisik.php" title="">Master Jenis Bukti Fisik</a></li>
				<li><a href="m_predikat.php" title="">Master Predikat</a></li>
				<li><a href="petugas_lihat_krp.php" title="">Lihat KRP</a></li>
				<li><a href="petugas_lihat_khp.php" title="">Lihat KHP</a></li>
				<li><a href="petugas_lihat_tkk.php" title="">Lihat TKM</a></li>
				<li><a href="petugas_revisi_tkk.php" title="">Revisi TKM</a></li>
				<li><a href="petugas_pencarian.php" title="">Pencarian Data Prestasi</a></li>
				<li><a href="petugas_porto.php" title="">Cetak Protofolio</a></li>
				<li><a href="petugas_mhsisi.php" title="">Mhs Sudah Isi</a></li>
				<li><a href="logout.php" title="">Log Out</a></li>
			</ul>
			</div>
		';
		$menu .= $identitas;
	}else if($_SESSION["HAK"]=="KMFAKUL"){
		$menu .= '
			<div id="menu">
			<ul>
				<li><a href="home.php" title="">Home</a></li>
				<li><a href="#" title=""><b><i>:: SIS. KREDIT PRESTASI :::</i></b></a></li>
				<li><a href="petugas_proses_khp.php" title="">Proses Validasi KHP</a></li>
				<li><a href="petugas_lihat_krp.php" title="">Lihat KRP</a></li>
				<li><a href="petugas_listval.php" title="">List Validasi</a></li>
				<li><a href="petugas_mhsbelumval.php" title="">Mhs Belum Validasi</a></li>
				<li><a href="petugas_lihat_khp.php" title="">Lihat KHP</a></li>
				<li><a href="petugas_lihat_tkk.php" title="">Lihat TKM</a></li>
				<li><a href="petugas_revisi_tkk.php" title="">Input/Revisi TKM</a></li>
				<li><a href="petugas_pencarian.php" title="">Pencarian Data Prestasi</a></li>
				<li><a href="petugas_pejabat.php" title="">Update Pejabat</a></li>
				<li><a href="petugas_mhsisi.php" title="">Mhs Sudah Isi</a></li>
				<li><a href="petugas_laporan.php" title="">Laporan</a></li>
				<li><a href="logout.php" title="">Log Out</a></li>
			</ul>
			</div>
		';
		$menu .= $identitas;
	}else if($_SESSION["HAK"]=="MHS") {
		$menu .= '
			<div id="menu">
			<ul>
				<li><a href="home.php" title="">Home</a></li>
				<li><a href="mhs_input_krp.php" title="">Input KRP</a></li>
				<li><a href="mhs_lihat_khp.php" title="">Lihat KHP</a></li>
				<li><a href="mhs_lihat_tkk.php" title="">Lihat TKM</a></li>
				<li><a href="logout.php" title="">Log Out</a></li>
			</ul>
			</div>
		';
		$menu .= $identitas;
	}

}else{
	$menu .= "
		<div id='login' class='boxed'>
		<h2 class='title'>Login</h2>
		<div class='content'>
			<form id='form1' method='post' action='index_login.php'>
				<fieldset>
				<legend>Sign-In</legend>
				<label for='inputtext1'>Username:</label>
				<input id='inputtext1' type='text' name='userid' value='' />
				<label for='inputtext2'>Password:</label>
				<input id='inputtext2' type='password' name='userpass' value='' />
				<input id='inputsubmit1' type='submit' name='ok' value='Sign In' />
				</fieldset>
			</form>
		</div>
		</div>
		";

	/*
	$q = mysql_query("select * from informasi order by kategori");
	while ($d = mysql_fetch_array($q)) {
	$menu .= "<li><a href='informasi.php?cl=informasi&id=".$d[row_id]."' title='Informasi SKP & Beasiswa'>".$d[judul_menu]."</a></li>";
	}
	$menu .= "<li class='active'><a href='#' title=''>Informasi SKP & Beasiswa</a></li>
			<li><a href='informasi.skp.php' title=''>Point SKP Mahasiswa</a></li>
			<li><a href='informasi.beasiswa.php' title=''>Penawaran Beasiswa</a></li>
			<li><a href='penerima.beasiswa.php' title=''>Penerima Beasiswa</a></li>
			</ul>
			</div>";

	$qq = mysql_query("select * from pengumuman order by row_id desc");
	$dd = mysql_fetch_array($qq);

	$menu .="<div id='updates' class='boxed'>
			<h2 class='title'>PENGUMUMAN</h2>
			<div class='content'>
				<ul>
					<li>
						<h3>".substr($dd[tanggal],8,2)."-".substr($dd[tanggal],5,2)."-".substr($dd[tanggal],0,4)."</h3>
						<p><a href='informasi.php?cl=pengumuman&id=".$dd[row_id]."' title='Pengumuman SKP & Beasiswa'>".$dd[judul]."&#8230;</a></p>
					</li>
				</ul>
			</div>
		</div>";
	*/
}



?>