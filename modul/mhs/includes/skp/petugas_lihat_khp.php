<?php
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMFAKUL" and $_SESSION["HAK"]!="KMPUSAT"){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking3.php");
require("util/keamanan.php");

// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************


$isi = '';
$isi .= '
	<div id="welcome" class="post">
		<h2 class="title">Lihat KHP</h2>
		<TABLE>
		<form name="form2" method="post" action="petugas_lihat_khp.php">
		<TR>
			<TD class="formulir">NIM</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
			<input type=text name="nimmhs" value="'.$_POST["nimmhs"].'">
			</TD>
		</TR>
		<TR>
			<TD class="formulir">Tahun</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
			<select name="tahun">
				';
				$query = "select thn_akademik_semester, nm_semester, id_semester from aucc.semester where nm_semester in ('Ganjil','Genap') order by thn_akademik_semester desc, nm_semester desc";
				$stmt = oci_parse ($conn, $query);
				oci_execute ($stmt);
				while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
					$isi .= '<option value="'.$r[2].'" '; if($_POST["tahun"]==$r[2]){ $isi .= 'selected'; } $isi .= '>'.$r[0].' - '.$r[1].'</option>';
				}				
				$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir"><input type=submit name="ok" value="Tampil"></TD>
		</TR>
		</form>
		</TABLE>
	';
	if($_POST["ok"] and $_POST["nimmhs"]) {
		$nimnyamhs = AngkaHurufSaja($_POST["nimmhs"]);
		$thakad = AngkaSaja($_POST["tahun"]);

		// cek apakah mhs ada pada fakul tertentu
		if($_SESSION["HAK"]=="KMFAKUL") {
			$kd_fakul = "0";
			$query = "select b.id_fakultas from aucc.pegawai a, aucc.unit_kerja b where a.id_unit_kerja=b.id_unit_kerja and a.nip_pegawai='".$_SESSION["KODE"]."'";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$kd_fakul = $r[0];

			$query = "select b.id_fakultas, c.nm_pengguna, a.id_mhs from aucc.mahasiswa a, aucc.program_studi b, aucc.pengguna c where a.id_program_studi=b.id_program_studi and a.id_pengguna=c.id_pengguna and b.id_fakultas='".$kd_fakul."' and a.nim_mhs='".$nimnyamhs."'";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$kd_fakul2 = $r[0];
			$namanyamhs = $r[1];
			$idmhsnya = $r[2];

			if($kd_fakul==$kd_fakul2) {
				$boleh=true;
			}else{
				$boleh=false;
				$isi .= 'Anda tidak punya hak pada Mahasiswa tersebut !!!';
			}
		}else if($_SESSION["HAK"]=="KMPUSAT") {
			$boleh=true;
			$query = "select b.id_fakultas, c.nm_pengguna, a.id_mhs from aucc.mahasiswa a, aucc.program_studi b, aucc.pengguna c where a.id_program_studi=b.id_program_studi and a.id_pengguna=c.id_pengguna and a.nim_mhs='".$nimnyamhs."'";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$kd_fakul2 = $r[0];
			$namanyamhs = $r[1];
			$idmhsnya = $r[2];
		}else{
			$boleh=true;
		}
		
		if($boleh) {
			$query = "select tahun_ajaran, nm_semester from aucc.semester where id_semester='".$thakad."' ";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$tahun = $r[0]; $semester = $r[1];

			$isi .= '<br><hr><br>';
			
			//$param = "nim=123&tahun=11&semes=11";
			$param = $nimnyamhs."&".$thakad.'&KHP';

			$isi .= '
			<div align=center><font size="5">Kartu Hasil Prestasi</font><br />
			 '.$nimnyamhs.' / '.$namanyamhs.' <br />
			Tahun : '.$tahun.' / Semester : '.$semester.'<br />
			 [<A HREF="mhs_input_krp_cetak.php?param='.base64_encode($param).'" target="_BLANK">cetak KHP</A>]</div>
			<TABLE width=100%>
			<TR>
				<TD align=center class="judul">NAMA KEGIATAN</TD>
				<TD align=center class="judul">WAKTU</TD>
				<TD align=center class="judul">PENYELENGGARA</TD>
				<TD align=center class="judul">BUKTI FISIK</TD>
				<TD align=center class="judul">SKOR</TD>
			</TR>
			';
			$query = "select id_krp_khp, nm_krp_khp, waktu_krp_khp, penyelenggara_krp_khp, id_bukti_fisik, skor_krp_khp from aucc.krp_khp where id_semester='".$thakad."' and id_mhs='".$idmhsnya."' and skor_krp_khp!='0' order by id_krp_khp";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt); $no=0;
			while($r = oci_fetch_array ($stmt, OCI_BOTH)){
				$no++;
				if($no%2==0){
					$gaya = "isian2";
				}else{
					$gaya = "isian3";
				}
				if($r[4]!='0'){
					$query2 = "select nm_bukti_fisik from aucc.bukti_fisik where id_bukti_fisik = '".$r[4]."' ";
					$stmt2 = oci_parse ($conn, $query2);
					oci_execute ($stmt2);
					$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
					$bukti = $r2[0];
				}else{
					$bukti = '-';
				}
				$isi .= '
				<TR>
					<TD class="'.$gaya.'">'.$r[1].'</TD>
					<TD class="'.$gaya.'">'.$r[2].'</TD>
					<TD class="'.$gaya.'">'.$r[3].'</TD>
					<TD class="'.$gaya.'">'.$bukti.'</TD>
					<TD align=center class="'.$gaya.'">'.$r[5].'</TD>
				</TR>
				';
			}

			$isi .= '
			</TABLE>
			';
		}
	}
	$isi .= '
	</div>
';


	require("template_setting.php");
}

?>