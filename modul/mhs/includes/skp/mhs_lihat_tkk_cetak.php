<?php
require("util/ceking3.php");
require("util/keamanan.php");
?>
<BODY>
<?php
if($_GET["param"]){
	// cek input
	$lanjut=true;

	if($lanjut==true) {
		require("util/konek.php");
		$nim = AngkaHurufSaja(base64_decode($_GET["param"]));
		
		$query = "select e.nm_pengguna, b.nm_program_studi, c.nm_fakultas, d.nm_jenjang, c.id_fakultas, d.id_jenjang from aucc.mahasiswa a, aucc.program_studi b, aucc.fakultas c, aucc.jenjang d, aucc.pengguna e where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and b.id_jenjang=d.id_jenjang and a.id_pengguna=e.id_pengguna and a.nim_mhs='".$nim."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$jum_generate=0; $email_sama=0;
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$nama = $r[0];
		$prodi = $r[1];
		$fakultas = $r[2];
		$jenjang = $r[3];
		$kd_fakul = $r[4];
		$jenjang_kode = $r[5];
		
		echo '
		<div align=center><IMG SRC="images/logo_unair.jpg" WIDTH="90"><br><br><font size=4><B>UNIVERSITAS AIRLANGGA<br>FAKULTAS '.strtoupper($fakultas).'<br>TRANSKRIP KEGIATAN MAHASISWA (TKM)</B></font></div><br>
		<TABLE cellpadding=3 width=98% align=center>
		<TR>
			<TD width=15%>Nama</TD>
			<TD width=1%>:</TD>
			<TD>'.$nama.'</TD>
		</TR>
		<TR>
			<TD>NIM</TD>
			<TD>:</TD>
			<TD>'.$nim.'</TD>
		</TR>
		<TR>
			<TD>Prog.Studi</TD>
			<TD>:</TD>
			<TD>'.$jenjang.'-'.$prodi.'</TD>
		</TR>
		
		</TABLE><br>
		';
		echo '
		<TABLE width=98% cellpadding=3 cellspacing=0 border=1 align=center>
		<TR>
			<TD width=3% align=center><B>No</B></TD>
			<TD width=87% align=center><B>Kriteria Kegiatan</B></TD>
			<TD width=10% align=center><B>Nilai skp</B></TD>
		</TR>
		<TR>
			<TD align=center>A</TD>
			<TD>Wajib Universitas</TD>
			<TD>&nbsp;</TD>
		</TR>
		';
		$query = "select id_kegiatan_1,nm_kegiatan_1 from aucc.kegiatan_1 where id_kelompok_kegiatan='1' order by id_kegiatan_1";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$no=0; $nilai_skp_tot=0;
		while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
			$no++;

			$query2 = "select sum(b.skor_krp_khp) from aucc.kegiatan_2 a, aucc.krp_khp b, aucc.mahasiswa c where a.id_kegiatan_2=b.id_kegiatan_2 and a.id_kegiatan_1='".$r[0]."' and b.id_mhs=c.id_mhs and c.nim_mhs='".$nim."' ";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			if($r2[0]==""){ $nilai_skp = "0"; }else{ $nilai_skp = $r2[0]; }

			$nilai_skp_tot += $nilai_skp;
			echo '
			<TR>
				<TD align=center>&nbsp;</TD>
				<TD>'.$no.'. '.$r[1].'</TD>
				<TD align=center>'.$nilai_skp.'</TD>
			</TR>
			';
		}
		
		echo '
		<TR>
			<TD align=center>B</TD>
			<TD>Pilihan</TD>
			<TD>&nbsp;</TD>
		</TR>
		';
		$query = "select id_kelompok_kegiatan,nm_kelompok_kegiatan from aucc.kelompok_kegiatan where id_kelompok_kegiatan!='1' order by id_kelompok_kegiatan";
		//$query = "select id_kegiatan_1,nm_kegiatan_1 from aucc.kegiatan_1 where id_kelompok_kegiatan!='1' order by id_kegiatan_1";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$no=0;
		while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
			$no++;

			$query2 = "select sum(b.skor_krp_khp) from aucc.kegiatan_2 a, aucc.krp_khp b, aucc.mahasiswa c, aucc.kegiatan_1 d where a.id_kegiatan_2=b.id_kegiatan_2 and a.id_kegiatan_1=d.id_kegiatan_1 and d.id_kelompok_kegiatan='".$r[0]."' and b.id_mhs=c.id_mhs and c.nim_mhs='".$nim."' ";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);
			$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
			if($r2[0]==""){ $nilai_skp = "0"; }else{ $nilai_skp = $r2[0]; }

			$nilai_skp_tot += $nilai_skp;
			echo '
			<TR>
				<TD align=center>&nbsp;</TD>
				<TD>'.$no.'. '.$r[1].'</TD>
				<TD align=center>'.$nilai_skp.'</TD>
			</TR>
			';
		}

		$kueri = "select nm_krp_predikat from aucc.krp_predikat where id_jenjang='".$jenjang_kode."' and nilai_min<=".$nilai_skp_tot." and nilai_max>=".$nilai_skp_tot." ";
		$stmt2 = oci_parse ($conn, $kueri);
		oci_execute ($stmt2);
		$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
		$predikatnya = $r2[0];
		
		/*
		if ($nilai_skp_tot>=250) {
			$predikat = "Sangat Baik";
			} else if ($nilai_skp_tot<=250 and $nilai_skp_tot>=201) {
			$predikat = "Baik";
			} else if ($nilai_skp_tot<=200 and $nilai_skp_tot>=100) {
			$predikat = "Cukup";
			} else {
			$predikat = "tidak Baik";
			}
		*/

		echo '
		<TR>
			<TD>&nbsp;</TD>
			<TD align=right>Jumlah Perolehan skp</TD>
			<TD align=center>'.$nilai_skp_tot.'</TD>
		</TR>
		<TR>
			<TD>&nbsp;</TD>
			<TD align=right>Predikat</TD>
			<TD align=center>'.$predikatnya.'</TD>
		</TR>
		</TABLE><br>
		';
		echo '
		<TABLE width=98% cellpadding=3 cellspacing=0 align=center>
		<TR>
			<TD width=60% valign=top>
			<font size="2">Catatan :</font><br>
			<B><font size="2">Predikat SKP S1:</font></B>
			<TABLE>
				';
				$kueri = "select nm_krp_predikat, nilai_min, nilai_max from aucc.krp_predikat where id_jenjang='1' order by nilai_max desc";
				$stmt2 = oci_parse ($conn, $kueri);
				oci_execute ($stmt2);
				while($r2 = oci_fetch_array ($stmt2, OCI_BOTH)){
					if($r2[2] == 10000) {
						echo '
							<TR>
								<TD><font size="2">'.$r2[0].'</font></TD>
								<TD>:</TD>
								<TD><font size="2"> > '.$r2[1].' skp</font></TD>
							</TR>
						';
					}else{
						echo '
							<TR>
								<TD><font size="2">'.$r2[0].'</font></TD>
								<TD>:</TD>
								<TD><font size="2"> > '.$r2[1].' - '.$r2[2].' skp</font></TD>
							</TR>
						';
					}
				}
				echo '
			</TABLE><BR>
			<B><font size="2">Predikat SKP D3:</font></B>
			<TABLE>
				';
				$kueri = "select nm_krp_predikat, nilai_min, nilai_max from aucc.krp_predikat where id_jenjang='5' order by nilai_max desc";
				$stmt2 = oci_parse ($conn, $kueri);
				oci_execute ($stmt2);
				while($r2 = oci_fetch_array ($stmt2, OCI_BOTH)){
					if($r2[2] == 10000) {
						echo '
							<TR>
								<TD><font size="2">'.$r2[0].'</font></TD>
								<TD>:</TD>
								<TD><font size="2"> > '.$r2[1].' skp</font></TD>
							</TR>
						';
					}else{
						echo '
							<TR>
								<TD><font size="2">'.$r2[0].'</font></TD>
								<TD>:</TD>
								<TD><font size="2"> > '.$r2[1].' - '.$r2[2].' skp</font></TD>
							</TR>
						';
					}
				}
				echo '
			</TABLE>
			</TD>
			<TD width=40% valign=top>
			<br><br>
			<font size=2>
			Surabaya, '.date("d-m-Y").'<br>
			A.n. Dekan,<br>
			';
			//$query = "select b.username, b.nm_asli from aucc.fakultas a, aucc.pengguna b where a.id_wadek1=b.id_pengguna and a.id_fakultas='".$kd_fakul."'";
			$query = "select pejabat_nip, pejabat, jabatan from aucc.skp_pejabat where id_fakultas='".$kd_fakul."' ";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$jab = $r[2];
			$pej = $r[1];
			$pej_nip = $r[0];
			
			echo '
			'.$jab.'<br>
			<br><br><br><br>
			'.$pej.'<br>
			NIP : '.$pej_nip.'
			</font>
			</TD>
		</TR>
		</TABLE>
		';
	}
}
?>
</BODY>
</HTML>
