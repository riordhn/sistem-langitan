<?php
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMPUSAT"){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking3.php");
require("util/keamanan.php");


// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************

$isi = '';

if($_GET["edit"]){
	$query = "select id_kelompok_kegiatan, nm_kelompok_kegiatan from aucc.kelompok_kegiatan where id_kelompok_kegiatan='".AngkaSaja(base64_decode($_GET["edit"]))."' ";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	$kode = $r[0];  $nama = $r[1];

	$tombol = '
	<input type=hidden name=paramid value="'.$_GET["edit"].'">
	<input type=submit name=ubah value="Update">
	<input type=button name=ubah value="Batal" onclick="location.href=\'m_kelompok_kegiatan.php\';">
	';
}else{
	$kode = "";  $nama = "";
	$tombol = '<input type=submit name=ok value="Tambahkan">';
}
$isi .= '
<div id="welcome" class="post">
	<h2 class="title">Master Kelompok Kegiatan</h2>
	<div class="content">
		<TABLE>
		<form name="form2" method="post" action="m_kelompok_kegiatan.php">
		<TR>
			<TD>Kode</TD>
			<TD>:</TD>
			<TD><input type=text name=kode_kk value="'.$kode.'"></TD>
		</TR>
		<TR>
			<TD>Kelompok Kegiatan</TD>
			<TD>:</TD>
			<TD><input type=text name=nama_kk value="'.$nama.'"></TD>
		</TR>
		<TR>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD>'.$tombol.'</TD>
		</TR>
		</form>
		</TABLE>
';
if($_POST["ok"] and $_POST["kode_kk"] and $_POST["nama_kk"]){
	// cek apakah ada nama sama
	$query = "select count(*) from aucc.kelompok_kegiatan where nm_kelompok_kegiatan='".semua_boleh_kecuali_saja($_POST["nama_kk"])."'";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	if($r[0] > 0) {
		$isi .= '<FONT COLOR="red">Nama kelompok kegiatan ganda</FONT>';
	}else{
		$query = "insert into aucc.kelompok_kegiatan (nm_kelompok_kegiatan) values ('".semua_boleh_kecuali_saja($_POST["nama_kk"])."')";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		if(ocirowcount($stmt)>0) {
			$isi .= '<FONT COLOR="blue">Data berhasil ditambahkan</FONT>';
		}else{
			$isi .= '<FONT COLOR="red">Data gagal ditambahkan</FONT>';
		}
	}
}else if($_POST["ubah"] and $_POST["kode_kk"] and $_POST["nama_kk"] and $_POST["paramid"]){
	// cek apakah ada nama sama
	$query = "select count(*) from aucc.kelompok_kegiatan where nm_kelompok_kegiatan='".semua_boleh_kecuali_saja($_POST["nama_kk"])."' and id_kelompok_kegiatan!='".AngkaSaja(base64_decode($_POST["paramid"]))."' ";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	if($r[0] > 0) {
		$isi .= '<FONT COLOR="red">Nama kelompok kegiatan ganda</FONT>';
	}else{
		$query = "update aucc.kelompok_kegiatan set nm_kelompok_kegiatan='".semua_boleh_kecuali_saja($_POST["nama_kk"])."' where id_kelompok_kegiatan='".AngkaSaja(base64_decode($_POST["paramid"]))."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		if(ocirowcount($stmt)>0) {
			$isi .= '<FONT COLOR="blue">Data berhasil diubah</FONT>';
		}else{
			$isi .= '<FONT COLOR="red">Data gagal diubah</FONT>';
		}
	}
}
$isi .= '
		<br><hr><br>
';
if($_GET["hps"]){
	$query = "delete from aucc.kelompok_kegiatan where id_kelompok_kegiatan='".AngkaSaja(base64_decode($_GET["hps"]))."' ";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	if(ocirowcount($stmt)>0) {
		$isi .= '<FONT COLOR="blue">Data berhasil dihapus</FONT>';
	}else{
		$isi .= '<FONT COLOR="red">Data gagal dihapus</FONT>';
	}
}
$isi .= '
		<TABLE width=100% align=center>
		<TR>
			<TD align=center class="judul" width=25%>&nbsp;</TD>
			<TD align=center class="judul">KODE</TD>
			<TD align=center class="judul">KELOMPOK KEGIATAN</TD>
		</TR>
';
	$query = "select id_kelompok_kegiatan, nm_kelompok_kegiatan from aucc.kelompok_kegiatan order by id_kelompok_kegiatan asc";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt); $hit=0;
	while($r = oci_fetch_array ($stmt, OCI_BOTH)){
		$hit++;
		if($hit%2==0){
			$gaya = "isian2";
		}else{
			$gaya = "isian3";
		}
		$isi .= '
		<TR>
			<TD align=center class="'.$gaya.'">
			[<A HREF="#?" onclick="if(confirm(\'Yakin Hapus ?\')!=0){ location.href=\'m_kelompok_kegiatan.php?hps='.base64_encode($r[0]).'\'; }">hapus</A>]
			[<A HREF="m_kelompok_kegiatan.php?edit='.base64_encode($r[0]).'">edit</A>]
			</TD>
			<TD align=center class="'.$gaya.'">'.$r[0].'</TD>
			<TD class="'.$gaya.'">'.$r[1].'</TD>
		</TR>
		';
	}
	
$isi .= '
		</TABLE>
	</div>
</div>
';


	require("template_setting.php");
}
?>