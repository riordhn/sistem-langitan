<?
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="MHS"){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking2.php");
require("util/keamanan.php");


// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************

$isi = '';
// ambil th akad aktif
$kueri = "select tahun,semester,aktif from tb_thakad where aktif='1'";
$q = mysql_query($kueri);
if(mysql_num_rows($q)>1){
	$isi .= 'Database tahun akademik bermasalah. Harap hubungi DSI ()';
}else if(mysql_num_rows($q)==0){
	$isi .= 'Database tahun akademik bermasalah. Harap hubungi DSI ()';
}else if(mysql_num_rows($q)==1){
	$r = mysql_fetch_row($q);
	$tahun = $r[0];
	$semester = $r[1];
	if($semester=='1'){
		$thakad = "Ganjil";
	}else if($semester=='2'){
		$thakad = "Genap";
	}
}

$isi .= '
<div id="welcome" class="post">
	<h2 class="title">KRP Tahun Akademik <U>'.$tahun.'</U> Semester <U>'.$thakad.'</U></h2>
	<div class="content">
		<TABLE>
		<form name="form2" method="post" action="mhs_input_krp.php">
		<TR>
			<TD class="formulir">Kelompok Kegiatan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
			<select name="kk" onchange="submit()">
			<option value="0">-----</option>
';
		$kueri = "select row_id,kelompok from tb_kelompok_kegiatan order by row_id ";
		$q = mysql_query($kueri);
		$kk_ada = false;
		while($r = mysql_fetch_row($q)){
			$kk_ada = true;
			$isi .= '<option value="'.$r[0].'" '; if($_POST["kk"]==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
		}
		
$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD class="formulir"><a href="list_jeniskegiatan.php" target="_BLANK">Jenis Kegiatan</a></TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
			<select name="jk" onchange="submit()">
			<option value="0">-----</option>
';
		if(isset($_POST["kk"]) and $kk_ada==true) {
			$kk = $_POST["kk"];
		}else{
			$kk = "00";
		}
		$kueri = "select kode_kegiatan,nama_kegiatan from tb_kegiatan where id_kelompok='".$kk."' order by row_id ";
		$q = mysql_query($kueri);
		$jk_ada = false;
		while($r = mysql_fetch_row($q)){
			$jk_ada = true;
			$isi .= '<option value="'.$r[0].'" '; if($_POST["jk"]==$r[0]){ $isi .= 'selected'; } $isi .= '>'.substr($r[1],0,70).'</option>';
		}
		
$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD class="formulir">Tingkat</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
			<select name="tingkat" onchange="submit()">
			<option value="0">-----</option>
';
		if(isset($_POST["jk"]) and $jk_ada == true) {
			$jk = $_POST["jk"];
		}else{
			$jk = "00";
		}
		//$kueri = "select row_id,tingkat from tb_tingkat order by row_id ";
		$kueri = "select b.row_id,b.tingkat
		from tb_kegiatan_2 a, tb_tingkat b
		where a.id_tingkat=b.row_id and a.id_kelompok='".$kk."' and a.id_kegiatan='".substr($jk,2,2)."'
		group by b.row_id,b.tingkat
		order by b.tingkat";
		//echo $kueri;
		$q = mysql_query($kueri);
		$tingkat_ada = false;
		while($r = mysql_fetch_row($q)){
			$tingkat_ada = true;
			$isi .= '<option value="'.$r[0].'" '; if($_POST["tingkat"]==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
		}
		
$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD class="formulir">Prestasi/Partisipasi/Jabatan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
			<select name="jabatan" onchange="submit()">
			<option value="0">-----</option>
';
		if(isset($_POST["tingkat"]) and $tingkat_ada == true) {
			$tingkat = $_POST["tingkat"];
		}else{
			$tingkat = "00";
		}
		//$kueri = "select row_id,jabatan_prestasi from tb_jabatan_prestasi order by row_id ";
		$kueri = "select b.row_id,b.jabatan_prestasi
		from tb_kegiatan_2 a, tb_jabatan_prestasi b
		where a.id_jab_pres=b.row_id and a.id_kelompok='".$kk."' and a.id_kegiatan='".substr($jk,2,2)."' and a.id_tingkat='".$tingkat."'
		group by b.row_id,b.jabatan_prestasi
		order by b.jabatan_prestasi";
		//echo $kueri;
		$q = mysql_query($kueri);
		while($r = mysql_fetch_row($q)){
			$isi .= '<option value="'.$r[0].'" '; if($_POST["jabatan"]==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
		}
		
$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD class="formulir">Nama Kegiatan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir"><input type=text name=kegiatan_nama value="'.$_POST["kegiatan_nama"].'"></TD>
		</TR>
		<TR>
			<TD class="formulir">Waktu Pelaksanaan</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir"><input type=text name=kegiatan_waktu value="'.$_POST["kegiatan_waktu"].'"></TD>
		</TR>
		<TR>
			<TD class="formulir">Penyelenggara</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir"><input type=text name=kegiatan_penyelenggara value="'.$_POST["kegiatan_penyelenggara"].'"></TD>
		</TR>
		<TR>
			<TD class="formulir">Bukti Fisik</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
			<select name="bukti">
			<option value="0">-----</option>
';
		if(isset($_POST["jabatan"])) {
			$jabatan = $_POST["jabatan"];
		}else{
			$jabatan = "00";
		}
		$kueri = "select row_id,jenis_bukti_fisik from tb_jenis_bukti_fisik order by row_id ";
		/*
		$kueri = "select b.row_id,b.jenis_bukti_fisik
		from tb_kegiatan_2 a, tb_jenis_bukti_fisik b
		where a.id_dasar_nilai=b.row_id and a.id_kelompok='".$kk."' and a.id_kegiatan='".substr($jk,2,2)."' and a.id_tingkat='".$tingkat."' and id_jab_pres='".$jabatan."'
		group by b.row_id,b.jenis_bukti_fisik
		order by b.jenis_bukti_fisik";
		*/
		//echo $kueri;
		$q = mysql_query($kueri);
		while($r = mysql_fetch_row($q)){
			$isi .= '<option value="'.$r[0].'" '; if($_POST["bukti"]==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
		}
		
$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir"><input type=submit name=ok value="Tambahkan"></TD>
		</TR>
		</form>
		</TABLE>
';
if($_POST["ok"] and $_POST["kegiatan_nama"] and $_POST["kegiatan_penyelenggara"] and $_POST["kegiatan_waktu"] and $_POST["bukti"]) {
	// cek input
	$lanjut = true;
	if(blackLisT($_POST["kegiatan_nama"])==false or blackLisT($_POST["kegiatan_penyelenggara"])==false or blackLisT($_POST["kegiatan_waktu"])==false or harusAngka($_POST["bukti"])==false ) {
		$lanjut=false;
	}

	if($lanjut==true) {
		if($_POST["tingkat"]){
			$tmp_tingkat = $_POST["tingkat"]; 
		}else{ 
			$tmp_tingkat =  "00";
		}
		if($_POST["jabatan"]){
			$tmp_jabatan = $_POST["jabatan"]; 
		}else{ 
			$tmp_jabatan =  "00";
		}
		$kode_kegiatan_lagi = $_POST["jk"].$tmp_tingkat.$tmp_jabatan;
		// cek validasi jenis kegiatan
		$kueri = "select kode_kegiatan_2 from tb_kegiatan_2 where kode_kegiatan_2='".$kode_kegiatan_lagi."' ";
		$q = mysql_query($kueri);
		if(mysql_num_rows($q)==0){
			$isi .= '<FONT COLOR="red">Proses Gagal. Jenis kegiatan tidak valid. <a href="list_jeniskegiatan.php" target="_BLANK">Lihat Master Jenis Kegiatan</a></FONT>';
		}else if(mysql_num_rows($q)>1){
			$isi .= '<FONT COLOR="red">Proses Gagal. Database tidak valid. Hubungi DSI ()</FONT>';
		}else if(mysql_num_rows($q)==1){
			// cek sudah ada
			//$q = mysql_query("select count(*) from tr_krp_khp where tahun='".$tahun."' and semester='".$semester."' and nama_kegiatan='".$_POST["kegiatan_nama"]."'");
			//$r = mysql_fetch_row($q);
			//if($r[0]>0){
			//	$isi .= '<FONT COLOR="red">Proses Gagal. Kegiatan sudah ada</FONT>';
			//}else{
				$kueri2 = "insert into tr_krp_khp (tahun,semester,kode_kegiatan_2,nama_kegiatan,waktu,penyelenggara,nim,id_bukti_fisik) values ('".$tahun."','".$semester."','".$kode_kegiatan_lagi."','".$_POST["kegiatan_nama"]."','".$_POST["kegiatan_waktu"]."','".$_POST["kegiatan_penyelenggara"]."','".$_SESSION["KODE"]."', '".$_POST["bukti"]."') ";
				$q = mysql_query($kueri2);
				if(mysql_affected_rows()>0){
					$isi .= '<FONT COLOR="blue">Data berhasil ditambahkan</FONT>';
				}else{
					$isi .= '<FONT COLOR="red">Data gagal ditambahkan</FONT>';
				}
			//}
		}
	}
}
$isi .= '
		<br><hr><br>
';
if($_GET["hps"]){
	$dihapus = sprintf("%d",base64_decode($_GET["hps"]));
	$kueri = "delete from tr_krp_khp where row_id='".$dihapus."'";
	$q = mysql_query($kueri);
	if(mysql_affected_rows()>0){
		$isi .= '<FONT COLOR="blue">Data berhasil dihapus</FONT>';
	}else{
		$isi .= '<FONT COLOR="red">Data gagal dihapus</FONT>';
	}
}
	//$param = "nim=123&tahun=11&semes=11";
	$param = $_SESSION["KODE"]."&".$tahun."&".$semester;
$isi .= '
		<A HREF="mhs_input_krp_cetak.php?param='.base64_encode($param).'" target="_BLANK">Cetak KRP</A>
		<TABLE width=100% align=center>
		<TR>
			<TD align=center class="judul" width=15%>&nbsp;</TD>
			<TD align=center class="judul">KODE KEGIATAN</TD>
			<TD align=center class="judul">NAMA KEGIATAN</TD>
			<TD align=center class="judul">WAKTU PELAKSANAAN</TD>
			<TD align=center class="judul">PENYELENGGARA</TD>
			<TD align=center class="judul">BUKTI</TD>
		</TR>
';
	$kueri = "select row_id,tahun,semester,kode_kegiatan_2,nama_kegiatan,waktu,penyelenggara,id_bukti_fisik from tr_krp_khp where tahun='".$tahun."' and semester='".$semester."' and nim='".$_SESSION["KODE"]."' order by row_id ";
	$q = mysql_query($kueri);
	$hit=0;
	while($r = mysql_fetch_row($q)){
		$hit++;
		if($hit%2==0){
			$gaya = "isian2";
		}else{
			$gaya = "isian3";
		}
		if($r[7]!='0'){
			$kueri2 = "select jenis_bukti_fisik from tb_jenis_bukti_fisik where row_id='".$r[7]."' ";
			$q2 = mysql_query($kueri2);
			while($r2 = mysql_fetch_row($q2)){
				$bukti = $r2[0];
			}
		}else{
			$bukti = '-';
		}
		
		$isi .= '
		<TR>
			<TD align=center class="'.$gaya.'">
			[<A HREF="#?" onclick="if(confirm(\'Yakin Hapus ?\')!=0){ location.href=\'mhs_input_krp.php?hps='.base64_encode($r[0]).'\'; }">hapus</A>]
			[<A HREF="mhs_input_krp_edit.php?edit='.base64_encode($r[0]).'">edit</A>]
			</TD>
			<TD align=center class="'.$gaya.'">'.$r[3].'</TD>
			<TD class="'.$gaya.'">'.$r[4].'</TD>
			<TD class="'.$gaya.'">'.$r[5].'</TD>
			<TD class="'.$gaya.'">'.$r[6].'</TD>
			<TD align=center class="'.$gaya.'">'.$bukti.'</TD>
		</TR>
		';
	}
	
$isi .= '
		</TABLE>
	</div>
</div>
';


	require("template_setting.php");

}
?>