<?
require("util/ceking2.php");
require("util/keamanan.php");
?>
<HTML>
<HEAD>
<TITLE> List Master Jenis Kegiatan </TITLE>
</HEAD>
<BODY>
<?
require "util/konek.php";
echo '
<TABLE cellpadding=5 cellspacing=0 border=1>
<tr>
	<td align="center"><b>No</b></td>
	<td align="center"><b>Id Kegiatan</b></td>
	<td align="center"><b>Kelompok Kegiatan</b></td>
	<td align="center"><b>Nama Kegiatan</b></td>
	<td align="center"><b>Tingkat</b></td>
	<td align="center"><b>Prestasi/Partisipasi/Jabatan</b></td>
	<td align="center"><b>Dasar Penilaian</b></td>
</tr>
';

$query = "select thn_akademik_semester, nm_semester, id_semester from aucc.semester where nm_semester in ('Ganjil','Genap') order by thn_akademik_semester desc, nm_semester desc";
$stmt = oci_parse ($conn, $query);
oci_execute ($stmt);
$r = oci_fetch_array ($stmt, OCI_BOTH);

$kueri = "
select a.id_kegiatan_2, c.nm_kelompok_kegiatan, b.nm_kegiatan_1, a.id_tingkat, a.id_jabatan_prestasi, a.id_dasar_nilai
from kegiatan_2 a, kegiatan_1 b, kelompok_kegiatan c
where a.id_kegiatan_1=b.id_kegiatan_1 and b.id_kelompok_kegiatan=c.id_kelompok_kegiatan
order by b.id_kelompok_kegiatan,b.id_kegiatan_1,a.id_kegiatan_2
";
$stmt = oci_parse ($conn, $kueri);
oci_execute ($stmt); $hit=0;
while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
	$hit++;

	// ambil tingkat
	$nm_tingkat="";
	$kueri2 = "select nm_tingkat from tingkat where id_tingkat='".$r[3]."' ";
	$stmt2 = oci_parse ($conn, $kueri2);
	oci_execute ($stmt2);
	$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
	$nm_tingkat = $r2[0];
	if(strlen($nm_tingkat)==0) { $nm_tingkat="&nbsp;"; }

	// ambil jabatan_prestasi
	$nm_jabatan_prestasi="";
	$kueri2 = "select nm_jabatan_prestasi from jabatan_prestasi where id_jabatan_prestasi='".$r[4]."' ";
	$stmt2 = oci_parse ($conn, $kueri2);
	oci_execute ($stmt2);
	$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
	$nm_jabatan_prestasi = $r2[0];
	if(strlen($nm_jabatan_prestasi)==0) { $nm_jabatan_prestasi="&nbsp;"; }

	// ambil dasar nilai
	$nm_dasar_nilai="";
	$kueri2 = "select dasar_penilaian from dasar_nilai where id_dasar_nilai='".$r[5]."' ";
	$stmt2 = oci_parse ($conn, $kueri2);
	oci_execute ($stmt2);
	$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
	$nm_dasar_nilai = $r2[0];
	if(strlen($nm_dasar_nilai)==0) { $nm_dasar_nilai="&nbsp;"; }

	echo '
	<tr>
		<td>'.$hit.'</td>
		<td>'.$r[0].'</td>
		<td>'.$r[1].'</td>
		<td>'.$r[2].'</td>
		<td>'.$nm_tingkat.'</td>
		<td>'.$nm_jabatan_prestasi.'</td>
		<td>'.$nm_dasar_nilai.'</td>
	</tr>
	';
}
echo '
</TABLE>
';
?>
</BODY>
</HTML>
