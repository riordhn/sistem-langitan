<?php
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMFAKUL" and $_SESSION["HAK"]!="KMPUSAT"){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking3.php");
require("util/keamanan.php");


// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************


$isi = '';

$isi .= '
	<div id="welcome" class="post">
		<h2 class="title">Lihat TKM</h2>
		<TABLE>
		<form name="form2" method="post" action="petugas_lihat_tkk.php">
		<TR>
			<TD class="formulir">NIM</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
			<input type=text name="nimmhs" value="'.$_POST["nimmhs"].'">
			</TD>
		</TR>
		<TR>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir"><input type=submit name="ok" value="Tampil"></TD>
		</TR>
		</form>
		</TABLE>
	';
	if($_POST["ok"] and $_POST["nimmhs"]){
		$nimnyamhs = AngkaHurufSaja($_POST["nimmhs"]);

		// cek apakah mhs ada pada fakul tertentu
		if($_SESSION["HAK"]=="KMFAKUL") {
			$kd_fakul = "0";
			$query = "select b.id_fakultas from aucc.pegawai a, aucc.unit_kerja b where a.id_unit_kerja=b.id_unit_kerja and a.nip_pegawai='".$_SESSION["KODE"]."'";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$kd_fakul = $r[0];

			$query = "select b.id_fakultas, c.nm_pengguna, a.id_mhs from aucc.mahasiswa a, aucc.program_studi b, aucc.pengguna c where a.id_program_studi=b.id_program_studi and a.id_pengguna=c.id_pengguna and b.id_fakultas='".$kd_fakul."' and a.nim_mhs='".$nimnyamhs."'";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$kd_fakul2 = $r[0];
			$namanyamhs = $r[1];
			$idmhsnya = $r[2];

			if($kd_fakul==$kd_fakul2) {
				$boleh=true;
			}else{
				$boleh=false;
				$isi .= 'Anda tidak punya hak pada Mahasiswa tersebut !!!';
			}
		}else if($_SESSION["HAK"]=="KMPUSAT") {
			$boleh=true;
			$query = "select b.id_fakultas, c.nm_pengguna, a.id_mhs from aucc.mahasiswa a, aucc.program_studi b, aucc.pengguna c where a.id_program_studi=b.id_program_studi and a.id_pengguna=c.id_pengguna and a.nim_mhs='".$nimnyamhs."'";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$kd_fakul2 = $r[0];
			$namanyamhs = $r[1];
			$idmhsnya = $r[2];
		}else{
			$boleh=true;
		}

		if($boleh) {
			$query = "select sum(skor_krp_khp) from aucc.krp_khp where id_mhs='".$idmhsnya."' ";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			($r[0]=="")? $skor_total="0" : $skor_total=$r[0];

			$isi .= '
			<br><hr><br>
			';
				//$param = "nim=123";
				$param = $nimnyamhs;


			$isi .= '
				<div align=center><font size="5">Transkrip Kegiatan Mahasiswa</font><br />
				 '.$nimnyamhs.' / '.$namanyamhs.'<br />Skor Total : <b>'.$skor_total.'</b><br />
				 [<A HREF="mhs_lihat_tkk_cetak.php?param='.base64_encode($param).'" target="_BLANK">cetak TKM</A>]</div>
				<TABLE width=100%>
				<TR>
					<TD align=center class="judul">SEMESTER</TD>
					<TD align=center class="judul">NAMA KEGIATAN</TD>
					<TD align=center class="judul">SKOR</TD>
				</TR>
			';
			$query = "select a.id_krp_khp, a.nm_krp_khp, a.skor_krp_khp, b.thn_akademik_semester, b.nm_semester from aucc.krp_khp a, aucc.semester b where a.id_semester=b.id_semester and a.skor_krp_khp!='0' and a.id_mhs='".$idmhsnya."' order by b.thn_akademik_semester, b.nm_semester, a.nm_krp_khp";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt); $no=0;
			while($r = oci_fetch_array ($stmt, OCI_BOTH)){
				$no++;
				if($no%2==0){
					$gaya = "isian2";
				}else{
					$gaya = "isian3";
				}
				($r[4]=="1")? $semes="Ganjil" : $semes="Genap";
				$isi .= '
				<TR>
					<TD align=center class="'.$gaya.'">'.$r[3].' - '.$semes.'</TD>
					<TD class="'.$gaya.'">'.$r[1].'</TD>
					<TD align=center class="'.$gaya.'">'.$r[2].'</TD>
				</TR>
				';
			}				
			$isi .= '
			</TABLE>
			';
		}
	}
	$isi .= '
	</div>
';

	require("template_setting.php");
}

?>