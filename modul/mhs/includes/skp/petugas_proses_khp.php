<?
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMFAKUL"){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking3.php");
require("util/keamanan.php");


// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************

// ********** MENU KIRI **********
	$javascript = '
	<script type="text/javascript">
		function dicentang_semua(){
			
		}
	</script>
	';
// *******************************


$isi = '';
/*
// ambil th akad aktif
$kueri = "select tahun,semester,aktif from tb_thakad where aktif='1'";
$q = mysql_query($kueri);
if(mysql_num_rows($q)>1){
	$isi .= 'Database tahun akademik bermasalah. Harap hubungi DSI ()';
}else if(mysql_num_rows($q)==0){
	$isi .= 'Database tahun akademik bermasalah. Harap hubungi DSI ()';
}else if(mysql_num_rows($q)==1){
	$r = mysql_fetch_row($q);
	$tahun = $r[0];
	$semester = $r[1];
	if($semester=='1'){
		$thakad = "Ganjil";
	}else if($semester=='2'){
		$thakad = "Genap";
	}
}
*/

$isi .= '
	<div id="welcome" class="post">
		<h2 class="title">Proses Validasi KHP</h2>
		<TABLE>
		<form name="form2" method="post" action="petugas_proses_khp.php">
		<TR>
			<TD class="formulir">Th Akad</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
			<select name="thakad">
				';
				$query = "select thn_akademik_semester, nm_semester, id_semester from aucc.semester where nm_semester in ('Ganjil','Genap') order by thn_akademik_semester desc, nm_semester desc";
				$stmt = oci_parse ($conn, $query);
				oci_execute ($stmt);
				while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
					$isi .= '<option value="'.$r[2].'" '; if($_POST["thakad"]==$r[2]){ $isi .= 'selected'; } $isi .= '>'.$r[0].' - '.$r[1].'</option>';
				}				
				$isi .= '
			</select>
			</TD>
		</TR>
		<TR>
			<TD class="formulir">NIM</TD>
			<TD class="formulir">:</TD>
			<TD class="formulir">
			<input type=text name="nimmhs" value="'.$_POST["nimmhs"].'">
			</TD>
		</TR>
		<TR>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir">&nbsp;</TD>
			<TD class="formulir"><input type=submit name="ok" value="Tampil"></TD>
		</TR>
		</form>
		</TABLE>
	';
	if($_GET["hps"]){
		$query = "delete from krp_khp where id_krp_khp='".AngkaSaja($_GET["hps"])."'";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);

		if(ocirowcount($stmt)>0) {
			$isi .= '<FONT COLOR="blue">Data berhasil dihapus</FONT>';
		}else{
			$isi .= '<FONT COLOR="red">Data gagal dihapus</FONT>';
		}
	}
	if( ($_POST["ok"] and $_POST["nimmhs"]) or ($_GET["hps"] and $_GET["jkwp"]) ) {
			$thakad = AngkaSaja($_POST["thakad"]);

			if($_POST["nimmhs"]){
				$nimnyamhs = strtoupper(AngkaHurufSaja($_POST["nimmhs"]));
			}else if($_GET["jkwp"]){
				$nimnyamhs = strtoupper(AngkaHurufSaja(base64_decode($_GET["jkwp"])));
			}
		// cek apakah mhs ada pada fakul tertentu
		if($_SESSION["HAK"]=="KMFAKUL") {
			$kd_fakul = "0";
			$query = "select b.id_fakultas from aucc.pegawai a, aucc.unit_kerja b where a.id_unit_kerja=b.id_unit_kerja and a.nip_pegawai='".$_SESSION["KODE"]."'";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$kd_fakul = $r[0];

			$query = "select b.id_fakultas, c.nm_pengguna from aucc.mahasiswa a, aucc.program_studi b, aucc.pengguna c where a.id_program_studi=b.id_program_studi and a.id_pengguna=c.id_pengguna and b.id_fakultas='".$kd_fakul."' and a.nim_mhs='".$nimnyamhs."'";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			$r = oci_fetch_array ($stmt, OCI_BOTH);
			$kd_fakul2 = $r[0];
			$namanyamhs = $r[1];

			if($kd_fakul==$kd_fakul2) {
				$boleh=true;
			}else{
				$boleh=false;
				$isi .= 'Anda tidak punya hak pada Mahasiswa tersebut !!!';
			}
		}else{
			$boleh=true;
		}
		if($boleh) {
			//$param = "nim=123&tahun=11&semes=11";
			$param = $nimnyamhs."&".$thakad;

			$isi .= '<br><hr><br>
			<A HREF="mhs_input_krp_cetak.php?param='.base64_encode($param).'" target="_BLANK">Cetak KRP</A>
			';
			$isi .= '
				<div align=center><h3>List Kegiatan Mahasiswa<br>NIM : '.$nimnyamhs.'<br>Nama : '.$namanyamhs.'</h3></div>
				<TABLE width=100%>
				<form name="form3" method="post" action="petugas_proses_khp.php">
				<TR>
					<TD align=center class="judul"><input type=submit name="dihapus" value="Hapus"></TD>
					<TD align=center class="judul">Urut</TD>
					<TD align=center class="judul">NAMA KEGIATAN</TD>
					<TD align=center class="judul">TGL KEGIATAN</TD>
					<TD align=center class="judul">PENYELENGGARA</TD>
					<TD align=center class="judul">SKOR</TD>
				</TR>
			';

			//$kueri = "select row_id,kode_kegiatan_2,nama_kegiatan,penyelenggara,skor,nim,waktu from tr_krp_khp where tahun='".$tahun."' and semester='".$semester."' and nim='".$nimnyamhs."' order by kode_kegiatan_2";

			$query = "select a.id_krp_khp, a.nm_krp_khp, a.waktu_krp_khp, a.penyelenggara_krp_khp, a.skor_krp_khp, c.bobot_kegiatan_2 from aucc.krp_khp a, aucc.mahasiswa b, aucc.kegiatan_2 c where a.id_mhs=b.id_mhs and a.id_kegiatan_2=c.id_kegiatan_2 and a.id_semester='".$thakad."' and b.nim_mhs='".$nimnyamhs."'";
			//echo $query;
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt); $no=0;
			while($r = oci_fetch_array ($stmt, OCI_BOTH)){
				$no++;
				if($no%2==0){
					$gaya = "isian2";
				}else{
					$gaya = "isian3";
				}
				// bila skor=0, maka diambilkan dari tabel tb_kegiatan_2
				if($r[4]!="0"){
					$tmp_skor = $r[4];
					$edit_skor = " disabled";
				}else{
					$tmp_skor = $r[5];
					$edit_skor = " ";
				}
				
				$isi .= '
					<TR>
						<TD align=center class="'.$gaya.'">
						<input type="checkbox" name="centang_hapus[]" value="'.$r[0].'">
						[<A HREF="#?" onclick="if(confirm(\'Yakin Hapus ?\')!=0){ location.href=\'petugas_proses_khp.php?hps='.$r[0].'&jkwp='.base64_encode($r[5]).'\'; }">hapus</A>]
						</TD>
						<TD class="'.$gaya.'">'.$no.'</TD>
						<TD align=left class="'.$gaya.'">'.$r[1].'</TD>
						<TD class="'.$gaya.'">'.$r[2].'</TD>
						<TD class="'.$gaya.'">'.$r[3].'</TD>
						<TD align=center class="'.$gaya.'">
						<input type=text name="skornya'.$r[0].'" value="'.$tmp_skor.'" size=2 '.$edit_skor.'>
						</TD>
					</TR>
				';
			}
			$isi .= '
				<TR>
					<TD colspan=6 align=center STYLE=" padding:3px; ">
					<input type=hidden name=tahun value="'.$thakad.'">
					<input type=hidden name=nim value="'.base64_encode($nimnyamhs).'">
					<input type=submit name=lanjut value="Simpan">
					</TD>
				</TR>
			';
			
			$isi .= '
				</form>
				</TABLE>
			';
		}
	}else if($_POST["lanjut"] and $_POST["nim"] and $_POST["tahun"]){
		$thakad = AngkaSaja($_POST["tahun"]);
		$nimnyamhs = AngkaHurufSaja(base64_decode($_POST["nim"]));

		$query = "select a.id_krp_khp, a.nm_krp_khp, a.waktu_krp_khp, a.penyelenggara_krp_khp, a.skor_krp_khp, c.bobot_kegiatan_2 from aucc.krp_khp a, aucc.mahasiswa b, aucc.kegiatan_2 c where a.id_mhs=b.id_mhs and a.id_kegiatan_2=c.id_kegiatan_2 and a.id_semester='".$thakad."' and b.nim_mhs='".$nimnyamhs."'";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		while($r = oci_fetch_array ($stmt, OCI_BOTH)){
			if(isset($_POST["skornya".$r[0]]) ) {
				$query2 = "update aucc.krp_khp set skor_krp_khp='".AngkaSaja($_POST["skornya".$r[0]])."' where id_krp_khp='".$r[0]."' ";
				$stmt2 = oci_parse ($conn, $query2);
				oci_execute ($stmt2);
			}
		}
		$isi .= '<br><hr><br>
		<font color=blue>Proses berhasil</font>';
		
	}else if($_POST["dihapus"] and $_POST["nim"] and $_POST["tahun"]){
		$nimnyamhs = AngkaHurufSaja(base64_decode($_POST["nim"]));

		$query = "select id_mhs from aucc.mahasiswa where nim_mhs='".$nimnyamhs."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$id_mhs = $r[0];

		$dihapus = $_POST["centang_hapus"];
		$yangdihapus = '';
		if( count($dihapus) > 0) {
			for($k=0; $k<count($dihapus); $k++) {
				$yangdihapus .= ','.$dihapus[$k];
			}
			$yangdihapus = substr($yangdihapus,1);
			//echo $yangdihapus;

			$query2 = "delete from aucc.krp_khp where id_mhs='".$id_mhs."' and id_krp_khp in (".$yangdihapus.") ";
			$stmt2 = oci_parse ($conn, $query2);
			oci_execute ($stmt2);

			if(ocirowcount($stmt)>0) {
				$isi .= '<FONT COLOR="blue">Data berhasil dihapus</FONT>';
			}else{
				$isi .= '<FONT COLOR="red">Data gagal dihapus</FONT>';
			}
		}
	}
	$isi .= '
	</div>
';


	require("template_setting.php");
}

?>