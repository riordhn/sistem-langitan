<?php
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMPUSAT"){
	header("location: logout.php");
}else{

	require "templ.php";
	require "util/konek.php";
	require("util/ceking3.php");
	require("util/keamanan.php");


	// ********** MENU KIRI **********
		require "menu_kiri.php";
	// *******************************

	$isi = '';

	if($_GET["edit"]){
		$query = "select id_bukti_fisik, nm_bukti_fisik from aucc.bukti_fisik where id_bukti_fisik='".AngkaSaja(base64_decode($_GET["edit"]))."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$kode = $r[0]; $nama = $r[1];

		$tombol = '
		<input type=hidden name=paramid value="'.$_GET["edit"].'">
		<input type="submit" name="ubah" value="Update">
		<input type="button" name="batal" value="Batal" onclick="location.href=\'m_jenis_buktifisik.php\';">
		';
	}else{
		$kode = "";  $nama = "";
		$tombol = '<input type=submit name=ok value="Tambahkan">';
	}
	$isi .= '
	<div id="welcome" class="post">
		<h2 class="title">Master Jenis Bukti Fisik</h2>
		<div class="content">
			<TABLE>
			<form name="form2" method="post" action="m_jenis_buktifisik.php">
			<TR>
				<TD>Kode</TD>
				<TD>:</TD>
				<TD><input type=text name=kode_bukti value="'.$kode.'"></TD>
			</TR>
			<TR>
				<TD>Jenis Bukti Fisik</TD>
				<TD>:</TD>
				<TD><input type=text name=nama_bukti value="'.$nama.'"></TD>
			</TR>
			<TR>
				<TD>&nbsp;</TD>
				<TD>&nbsp;</TD>
				<TD>'.$tombol.'</TD>
			</TR>
			</form>
			</TABLE>
	';
	if($_POST["ok"] and $_POST["kode_bukti"] and $_POST["nama_bukti"]){
		// cek apakah ada nama sama
		$query = "select count(*) from aucc.bukti_fisik where nm_bukti_fisik='".semua_boleh_kecuali_saja($_POST["nama_bukti"])."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		if($r[0] > 0) {
			$isi .= '<FONT COLOR="red">Nama bukti fisik ganda</FONT>';
		}else{
			$query = "insert into aucc.bukti_fisik (nm_bukti_fisik) values ('".semua_boleh_kecuali_saja($_POST["nama_bukti"])."') ";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			if(ocirowcount($stmt)>0) {
				$isi .= '<FONT COLOR="blue">Data berhasil ditambahkan</FONT>';
			}else{
				$isi .= '<FONT COLOR="red">Data gagal ditambahkan</FONT>';
			}
		}
	}else if($_POST["ubah"] and $_POST["kode_bukti"] and $_POST["nama_bukti"] and $_POST["paramid"]){
		// cek apakah ada nama sama
		$query = "select count(*) from aucc.bukti_fisik where nm_bukti_fisik='".semua_boleh_kecuali_saja($_POST["nama_bukti"])."' and id_bukti_fisik!='".AngkaSaja(base64_decode($_POST["paramid"]))."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		if($r[0] > 0) {
			$isi .= '<FONT COLOR="red">Nama bukti fisik ganda</FONT>';
		}else{
			$query = "update aucc.bukti_fisik set nm_bukti_fisik='".semua_boleh_kecuali_saja($_POST["nama_bukti"])."' where id_bukti_fisik='".AngkaSaja(base64_decode($_POST["paramid"]))."' ";
			$stmt = oci_parse ($conn, $query);
			oci_execute ($stmt);
			if(ocirowcount($stmt)>0) {
				$isi .= '<FONT COLOR="blue">Data berhasil diubah</FONT>';
			}else{
				$isi .= '<FONT COLOR="red">Data gagal diubah</FONT>';
			}
		}
	}
	$isi .= '
			<br><hr><br>
	';
	if($_GET["hps"]){
		$query = "delete from aucc.bukti_fisik where id_bukti_fisik='".AngkaSaja(base64_decode($_GET["hps"]))."' ";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		if(ocirowcount($stmt)>0) {
			$isi .= '<FONT COLOR="blue">Data berhasil dihapus</FONT>';
		}else{
			$isi .= '<FONT COLOR="red">Data gagal dihapus</FONT>';
		}
	}
	$isi .= '
			<TABLE width=100% align=center>
			<TR>
				<TD align=center class="judul" width=25%>&nbsp;</TD>
				<TD align=center class="judul">KODE</TD>
				<TD align=center class="judul">JENIS BUKTI FISIK</TD>
			</TR>
	';
		$query = "select id_bukti_fisik, nm_bukti_fisik from aucc.bukti_fisik order by id_bukti_fisik asc";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt); $hit=0;
		while($r = oci_fetch_array ($stmt, OCI_BOTH)){
			$hit++;
			if($hit%2==0){
				$gaya = "isian2";
			}else{
				$gaya = "isian3";
			}
			$isi .= '
			<TR>
				<TD align=center class="'.$gaya.'">
				[<A HREF="#?" onclick="if(confirm(\'Yakin Hapus ?\')!=0){ location.href=\'m_jenis_buktifisik.php?hps='.base64_encode($r[0]).'\'; }">hapus</A>]
				[<A HREF="m_jenis_buktifisik.php?edit='.base64_encode($r[0]).'">edit</A>]
				</TD>
				<TD align=center class="'.$gaya.'">'.$r[0].'</TD>
				<TD class="'.$gaya.'">'.$r[1].'</TD>
			</TR>
			';
		}
		
	$isi .= '
			</TABLE>
		</div>
	</div>
	';

	require("template_setting.php");
}

?>