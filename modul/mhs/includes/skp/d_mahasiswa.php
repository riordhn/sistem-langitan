<?
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMFAKUL" and $_SESSION["HAK"]!="KMPUSAT"){
	header("location: logout.php");
}else{

	require "templ.php";
	require "util/konek.php";
	require("util/ceking2.php");
	require("util/keamanan.php");


	// ********** MENU KIRI **********
		require "menu_kiri.php";
	// *******************************

	$isi = '';
	$isi .= '
	<div id="welcome" class="post">
		<h2 class="title">Data Mahasiswa</h2>
		<div class="content">
			<TABLE>
			<form name="form2" method="post" action="d_mahasiswa.php">
			<TR>
				<TD>Fakultas</TD>
				<TD>:</TD>
				<TD>
				<select name="fakultas" onchange="submit()">
					<option value="0">-----</option>
			';
			if($_SESSION["HAK"]=="KMFAKUL") {
				$kueri = "select fakul from tb_user where kode='".$_SESSION["KODE"]."' ";
				$q = mysql_query($kueri);
				while($r = mysql_fetch_row($q)) {
					$kd_fakul = $r[0];
				}
				$param = " where kd_fakulta='".$kd_fakul."' ";
			}else{
				$param = "";
			}
				$kueri = "select kd_fakulta,nm_fakulta from kd_fakul ".$param." order by kd_fakulta";
				$q = mysql_query($kueri);
				while($r = mysql_fetch_row($q)) {
					$isi .= '<option value="'.$r[0].'" '; if($_POST["fakultas"]==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
				}
			$isi .= '
				</select>
				</TD>
			</TR>
			<TR>
				<TD>Prodi</TD>
				<TD>:</TD>
				<TD>
				<select name="prodi">
					<option value="0">-----</option>
			';
				//$kueri = "select a.id_prodi,a.nm_prodi,b.nm_jenjang from tbkode_prodi a, kd_jenja b where a.kd_jenjang=b.kd_jenjang and a.kd_fakulta='".$_POST["fakultas"]."' and b.kd_jenjang in ('C','E') order by b.kd_jenjang";
				//$kueri = "call d_mahasiswa_3('".$_POST["fakultas"]."')";

				$kueri = "select a.kode_b,a.prodi_nama,b.nm_jenjang from kd_prodi_nama a, kd_jenja b where a.kd_jenjang=b.kd_jenjang and a.kd_fakulta='".$_POST["fakultas"]."' and b.kd_jenjang in ('C','E') order by b.kd_jenjang";
				$q = mysql_query($kueri);
				while($r = mysql_fetch_row($q)) {
					$isi .= '<option value="'.$r[0].'" '; if($_POST["prodi"]==$r[0]){ $isi .= 'selected'; } $isi .= '>'.$r[2].'-'.$r[1].'</option>';
				}
			$isi .= '
				</select>
				</TD>
			</TR>
			<TR>
				<TD>Th Angkatan</TD>
				<TD>:</TD>
				<TD>
				<select name="thangkat">
					<option value="0">Semua</option>
			';
				for($s="2000"; $s<=date("Y"); $s++) {
					$isi .= '<option value="'.$s.'"'; if($_POST["thangkat"]==$s){ $isi .= 'selected'; } $isi .= '>'.$s.'</option>';
				}
			$isi .= '
				</select>
				</TD>
			</TR>
			<TR>
				<TD>&nbsp;</TD>
				<TD>&nbsp;</TD>
				<TD>
				<input type=submit name=ok value="Tampil">
			';
			if($_SESSION["HAK"] == "KMPUSAT"){
				/*
				$isi .= '
					<input type=button name="sinkron" value="Sinkronisasi" onclick="location.href=\'d_mahasiswa_sinkron.php\';">
				';
				*/
			}
			$isi .= '
				</TD>
			</TR>
			</form>
			</TABLE>
	';
	$isi .= '
			<br><hr><br>
	';
	$param = "";
	$param_limit = " limit 0,10";
	if($_POST["thangkat"]) {
		$param_thangkat = " and substr(a.nim,3,2)='".substr($_POST["thangkat"],2,2)."' ";
		$param_limit = "";
		$thangkat_ket = $_POST["thangkat"];
	}else{
		$param_thangkat = " ";
		$thangkat_ket = "semua angkatan";
	}
	if($_POST["prodi"]) {
		$kueri = "select b.nm_jenjang,a.prodi_nama from kd_prodi_nama a, kd_jenja b where a.kd_jenjang=b.kd_jenjang and a.kode_b='".$_POST["prodi"]."' ";
		$q = mysql_query($kueri);
		while($r = mysql_fetch_row($q)) {
			$prodi_ket = "prodi ".$r[0]." - ".$r[1];
		}
	}else{
		$prodi_ket = "semua prodi";
	}
	$isi .= '
			<font size=4>
			List Mahasiswa Pada '.$prodi_ket.'<br>
			Th Angkatan : '.$thangkat_ket.'
			</font>
			<TABLE width=100% align=center>
			<TR>
				<TD align=center class="judul">NO</TD>
				<TD align=center class="judul">FAKULTAS</TD>
				<TD align=center class="judul">JENJANG</TD>
				<TD align=center class="judul">PRODI</TD>
				<TD align=center class="judul">NIM</TD>
				<TD align=center class="judul">NAMA</TD>
			</TR>
	';
	if($_POST["fakultas"] and $_POST["prodi"]){
		$kueri = "
		select d.nm_fakulta, e.nm_jenjang, c.prodi_nama, a.nim, a.nama
		from tb_mhs a, kd_prodi_nama c, kd_fakul d, kd_jenja e
		where a.kode_b=c.kode_b and c.kd_fakulta=d.kd_fakulta and c.kd_jenjang=e.kd_jenjang and c.kd_fakulta='".$_POST["fakultas"]."' and c.kode_b='".$_POST["prodi"]."' and e.kd_jenjang in ('C','E') ".$param_thangkat."
		order by c.kd_fakulta,c.kd_jenjang,c.prodi_nama
		";
	}else if($_POST["fakultas"]){
		$kueri = "
		select d.nm_fakulta, e.nm_jenjang, c.prodi_nama, a.nim, a.nama
		from tb_mhs a, kd_prodi_nama c, kd_fakul d, kd_jenja e
		where a.kode_b=c.kode_b and c.kd_fakulta=d.kd_fakulta and c.kd_jenjang=e.kd_jenjang and c.kd_fakulta='".$_POST["fakultas"]."' and e.kd_jenjang in ('C','E') ".$param_thangkat."
		order by c.kd_fakulta,c.kd_jenjang,c.prodi_nama
		";
	}else if($_POST["prodi"]){
		$kueri = "
		select d.nm_fakulta, e.nm_jenjang, c.prodi_nama, a.nim, a.nama
		from tb_mhs a, kd_prodi_nama c, kd_fakul d, kd_jenja e
		where a.kode_b=c.kode_b and c.kd_fakulta=d.kd_fakulta and c.kd_jenjang=e.kd_jenjang and c.kode_b='".$_POST["prodi"]."' and e.kd_jenjang in ('C','E') ".$param_thangkat."
		order by c.kd_fakulta,c.kd_jenjang,c.prodi_nama
		";
		//$kueri = "call d_mahasiswa_5('".$_POST["prodi"]."')";
	}else{
		if($_SESSION["HAK"]=="KMFAKUL") {
			$kueri = "select fakul from tb_user where kode='".$_SESSION["KODE"]."' ";
			$q = mysql_query($kueri);
			while($r = mysql_fetch_row($q)) {
				$kd_fakul = $r[0];
			}
			$param2 = " and d.kd_fakulta='".$kd_fakul."' ";
		}else{
			$param2= "";
		}

		$kueri = "
		select d.nm_fakulta, e.nm_jenjang, c.prodi_nama, a.nim, a.nama
		from tb_mhs a, kd_prodi_nama c, kd_fakul d, kd_jenja e
		where a.kode_b=c.kode_b and c.kd_fakulta=d.kd_fakulta and c.kd_jenjang=e.kd_jenjang and e.kd_jenjang in ('C','E') ".$param2."  ".$param_thangkat."
		order by c.kd_fakulta,c.kd_jenjang,c.prodi_nama ".$param_limit."
		";
		//$kueri = "call d_mahasiswa_6()";
	}
		//echo $kueri;
		$q = mysql_query($kueri);
		$hit=0;
		while($r = mysql_fetch_row($q)) {
			$hit++;
			if($hit%2==0){
				$gaya = "isian2";
			}else{
				$gaya = "isian3";
			}
			$isi .= '
			<TR>
				<TD align=center class="'.$gaya.'">'.$hit.'</TD>
				<TD align=center class="'.$gaya.'">'.$r[0].'</TD>
				<TD align=center class="'.$gaya.'">'.$r[1].'</TD>
				<TD class="'.$gaya.'">'.$r[2].'</TD>
				<TD class="'.$gaya.'">'.$r[3].'</TD>
				<TD class="'.$gaya.'">'.$r[4].'</TD>
			</TR>
			';
		}
	$isi .= '
			</TABLE>
			Catatan :<br>
			* Bila data mahasiswa tidak ada, hubungi Direktorat Kemahasiswaan atau DSI
		</div>
	</div>
	';


	require("template_setting.php");

}
?>