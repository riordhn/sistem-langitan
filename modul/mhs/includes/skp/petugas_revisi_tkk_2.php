<?php
session_start();
if(!session_is_registered("KODE") or !session_is_registered("HAK")){
	header("location: logout.php");
}else if($_SESSION["HAK"]!="KMPUSAT" and $_SESSION["HAK"]!="KMFAKUL"){
	header("location: logout.php");
}else{

require "templ.php";
require "util/konek.php";
require("util/ceking3.php");
require("util/keamanan.php");


// ********** MENU KIRI **********
	require "menu_kiri.php";
// *******************************

$isi = '';
// ambil identitas mhs
if($_POST["nimmhs"]){
	$nimnya=AngkaHurufSaja($_POST["nimmhs"]);
}else if($_POST["nim"]){
	$nimnya=AngkaHurufSaja(base64_decode($_POST["nim"]));
}else if($_GET["getnim"]){
	$nimnya=AngkaHurufSaja(base64_decode($_GET["getnim"]));
}else if($_GET["param"]){
	$nimnya=AngkaHurufSaja(base64_decode($_GET["param"]));
}

// cek apakah mhs ada pada fakul tertentu
if($_SESSION["HAK"]=="KMFAKUL") {
	$kd_fakul = "0";
	$query = "select b.id_fakultas from aucc.pegawai a, aucc.unit_kerja b where a.id_unit_kerja=b.id_unit_kerja and a.nip_pegawai='".$_SESSION["KODE"]."'";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	$kd_fakul = $r[0];

	$query = "select b.id_fakultas, c.nm_pengguna, a.id_mhs from aucc.mahasiswa a, aucc.program_studi b, aucc.pengguna c where a.id_program_studi=b.id_program_studi and a.id_pengguna=c.id_pengguna and b.id_fakultas='".$kd_fakul."' and a.nim_mhs='".$nimnya."'";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	$kd_fakul2 = $r[0];
	$namanyamhs = $r[1];
	$idmhsnya = $r[2];

	if($kd_fakul==$kd_fakul2) {
		$boleh=true;
	}else{
		$boleh=false;
		$isi .= 'Anda tidak punya hak pada Mahasiswa tersebut !!!';
	}
}else if($_SESSION["HAK"]=="KMPUSAT") {
	$boleh=true;
	$query = "select b.id_fakultas, c.nm_pengguna, a.id_mhs from aucc.mahasiswa a, aucc.program_studi b, aucc.pengguna c where a.id_program_studi=b.id_program_studi and a.id_pengguna=c.id_pengguna and a.nim_mhs='".$nimnya."'";
	$stmt = oci_parse ($conn, $query);
	oci_execute ($stmt);
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	$kd_fakul2 = $r[0];
	$namanyamhs = $r[1];
	$idmhsnya = $r[2];
}else{
	$boleh=true;
}

$lepas = false;
if($_POST["ok"] and $_POST["kegiatan_nama"] and $_POST["kegiatan_penyelenggara"] and $_POST["kegiatan_waktu"] and $_POST["kk"] and $_POST["jk"] and $_POST["tahun"] and $_POST["nim"]) {
	$param_prestasi = "";
	if(!empty($_POST["jabatan"])) {
		$param_prestasi = " and id_jabatan_prestasi='".AngkaSaja($_POST["jabatan"])."' ";
	}else{
		$param_prestasi = " and id_jabatan_prestasi is null ";
	}

	$param_tingkat = "";
	if(!empty($_POST["tingkat"])) {
		$param_tingkat = " and id_tingkat='".AngkaSaja($_POST["tingkat"])."' ";
	}else{
		$param_tingkat = " and id_tingkat is null ";
	}
	
	// ambil id_kegiatan2, dan skor
	$id_kegiatan2="";
	$kueri = "select id_kegiatan_2,bobot_kegiatan_2 from aucc.kegiatan_2 where id_kegiatan_1='".AngkaSaja($_POST["jk"])."' ".$param_tingkat." ".$param_prestasi." ";
	$stmt = oci_parse ($conn, $kueri);
	oci_execute ($stmt);
	$r = oci_fetch_array ($stmt, OCI_BOTH);
	$id_kegiatan2 = $r[0];
	$skor_master = $r[1];

	if(strlen($id_kegiatan2)>0) {
		// ambil id_semes
		$id_semes = AngkaSaja($_POST["tahun"]);

		// ambil id_mhs
		$kueri = "select id_mhs from aucc.mahasiswa where nim_mhs ='".AngkaHurufSaja(base64_decode($_POST["nim"]))."'";
		$stmt = oci_parse ($conn, $kueri);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$id_mhs = $r[0];

		$query = "select id_pengguna from aucc.pengguna where username='".$_SESSION["KODE"]."'";
		$stmt = oci_parse ($conn, $query);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$id_pengguna = $r[0];

		$kueri = "INSERT INTO aucc.krp_khp (ID_SEMESTER, ID_KEGIATAN_2, ID_BUKTI_FISIK, ID_MHS, PENYELENGGARA_KRP_KHP, NM_KRP_KHP, SKOR_KRP_KHP, WAKTU_KRP_KHP, TGL_INSERT, ID_PENGGUNA ) VALUES (".$id_semes.", ".$id_kegiatan2.", ".AngkaSaja($_POST["bukti"]).", ".$id_mhs.", '".semua_boleh_kecuali_saja($_POST["kegiatan_penyelenggara"])."', '".semua_boleh_kecuali_saja($_POST["kegiatan_nama"])."',".$skor_master.", '".semua_boleh_kecuali_saja($_POST["kegiatan_waktu"])."', TO_DATE('".date("YmdHis")."', 'YYYYMMDDHH24MISS'), ".$id_pengguna." )";
		//echo $kueri;
		$stmt = oci_parse ($conn, $kueri);
		oci_execute ($stmt)or die(print_r(oci_error($stmt)));
		if(ocirowcount($stmt)>0) {
			echo '
				<SCRIPT LANGUAGE="JavaScript">
				alert("Berhasil input ....");
				</SCRIPT>
			';
		}else{
			echo '
				<SCRIPT LANGUAGE="JavaScript">
				alert("Gagal input ....");
				</SCRIPT>
			';
		}
	}else{
		echo '<FONT COLOR="red">Proses Gagal. Jenis kegiatan tidak valid. <a href="list_jeniskegiatan.php" target="_BLANK">Lihat Master Jenis Kegiatan</a></FONT>';
	}
}

if($boleh) {

	$isi .= '
	<div id="welcome" class="post">
		<h2 class="title">Revisi Transkrip Kegiatan Mahasiswa</h2>
		<p><b>'.$nimnya.' / '.$namanyamhs.'</b></p>
		<div class="content">
			<TABLE>
			<form name="form2" method="post" action="petugas_revisi_tkk_2.php">
			<TR>
				<TD class="formulir">Tahun</TD>
				<TD class="formulir">:</TD>
				<TD class="formulir">
				<select name="tahun">
					';
					$query = "select thn_akademik_semester, nm_semester, id_semester from aucc.semester where nm_semester in ('Ganjil','Genap') order by thn_akademik_semester desc, nm_semester desc";
					$stmt = oci_parse ($conn, $query);
					oci_execute ($stmt);
					while ($r = oci_fetch_array ($stmt, OCI_BOTH)) {
						$isi .= '<option value="'.$r[2].'" '; if(AngkaSaja($_POST["tahun"])==$r[2]){ $isi .= 'selected'; } $isi .= '>'.$r[0].' - '.$r[1].'</option>';
					}				
					$isi .= '
				</select>
				</TD>
			</TR>
			<TR>
				<TD class="formulir">Kelompok Kegiatan</TD>
				<TD class="formulir">:</TD>
				<TD class="formulir">
				<select name="kk" onchange="submit()">
				<option value="0">-----</option>
				';
				$query = "select id_kelompok_kegiatan, nm_kelompok_kegiatan from aucc.kelompok_kegiatan order by id_kelompok_kegiatan";
				$stmt = oci_parse ($conn, $query);
				oci_execute ($stmt); $kk_ada = false;
				while($r = oci_fetch_array ($stmt, OCI_BOTH)){
					$kk_ada = true;
					$isi .= '<option value="'.$r[0].'" '; if(AngkaSaja($_POST["kk"])==$r[0] and $lepas==false){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
				}
				$isi .= '
				</select>
				</TD>
			</TR>
			<TR>
				<TD class="formulir"><a href="list_jeniskegiatan.php" target="_BLANK">Jenis Kegiatan</a></TD>
				<TD class="formulir">:</TD>
				<TD class="formulir">
				<select name="jk" onchange="submit()">
				<option value="0">-----</option>
				';
				if(isset($_POST["kk"]) and $kk_ada==true) {
					$kk = AngkaSaja($_POST["kk"]);
				}else{
					$kk = "0";
				}
				$query = "select id_kegiatan_1, nm_kegiatan_1 from aucc.kegiatan_1 where id_kelompok_kegiatan='".$kk."' order by id_kegiatan_1 ";
				$stmt = oci_parse ($conn, $query);
				oci_execute ($stmt); $jk_ada = false;
				while($r = oci_fetch_array ($stmt, OCI_BOTH)){
					$jk_ada = true;
					$isi .= '<option value="'.$r[0].'" '; if(AngkaSaja($_POST["jk"])==$r[0] and $lepas==false){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
				}
				$isi .= '
				</select>
				</TD>
			</TR>
			<TR>
				<TD class="formulir">Tingkat</TD>
				<TD class="formulir">:</TD>
				<TD class="formulir">
				<select name="tingkat">
				<option value="0">-----</option>
				';
				if(isset($_POST["jk"]) and $jk_ada==true) {
					$jk = AngkaSaja($_POST["jk"]);
				}else{
					$jk = "0";
				}
				$kueri = "select distinct a.id_tingkat,b.nm_tingkat from kegiatan_2 a, tingkat b where a.id_tingkat=b.id_tingkat and a.id_kegiatan_1='".$jk."' order by b.nm_tingkat ";
				$stmt = oci_parse ($conn, $kueri);
				oci_execute ($stmt); $tingkat_ada=false;
				while($r = oci_fetch_array ($stmt, OCI_BOTH)){
					$tingkat_ada = true;
					$isi .= '<option value="'.$r[0].'" '; if(AngkaSaja($_POST["tingkat"])==$r[0] and $lepas==false){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
				}
			
				$isi .= '
				</select>
				</TD>
			</TR>
			<TR>
				<TD class="formulir">Prestasi/Partisipasi/Jabatan</TD>
				<TD class="formulir">:</TD>
				<TD class="formulir">
				<select name="jabatan">
					<option value="0">-----</option>
				';
				if(isset($_POST["jk"]) and $jk_ada==true) {
					$jk = AngkaSaja($_POST["jk"]);
				}else{
					$jk = "0";
				}
				$kueri = "select distinct b.id_jabatan_prestasi, b.nm_jabatan_prestasi from kegiatan_2 a, jabatan_prestasi b where a.id_jabatan_prestasi=b.id_jabatan_prestasi and a.id_kegiatan_1='".$jk."' order by b.nm_jabatan_prestasi ";
				$stmt = oci_parse ($conn, $kueri);
				oci_execute ($stmt); $tingkat_ada=false;
				while($r = oci_fetch_array ($stmt, OCI_BOTH)){
					$isi .= '<option value="'.$r[0].'" '; if(AngkaSaja($_POST["jabatan"])==$r[0] and $lepas==false){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
				}
			
				$isi .= '
				</select>
				</TD>
			</TR>
			<TR>
				<TD class="formulir">Nama Kegiatan</TD>
				<TD class="formulir">:</TD>
				<TD class="formulir"><input type=text name=kegiatan_nama value="'; if($lepas==true){ $isi .= ''; }else{ $isi.= $_POST["kegiatan_nama"]; } $isi .= '" size="60"></TD>
			</TR>
			<TR>
				<TD class="formulir">Waktu Pelaksanaan</TD>
				<TD class="formulir">:</TD>
				<TD class="formulir"><input type=text name=kegiatan_waktu value="'; if($lepas==true){ $isi .= ''; }else{ $isi.= $_POST["kegiatan_waktu"]; } $isi .= '" size="40"></TD>
			</TR>
			<TR>
				<TD class="formulir">Penyelenggara</TD>
				<TD class="formulir">:</TD>
				<TD class="formulir"><input type=text name=kegiatan_penyelenggara value="'; if($lepas==true){ $isi .= ''; }else{ $isi.= $_POST["kegiatan_penyelenggara"]; } $isi .= '" size="50"></TD>
			</TR>
			<TR>
				<TD class="formulir">Bukti Fisik</TD>
				<TD class="formulir">:</TD>
				<TD class="formulir">
				<select name="bukti">
				<option value="0">-----</option>
				';
				$kueri = "select id_bukti_fisik, nm_bukti_fisik from bukti_fisik order by nm_bukti_fisik";
				$stmt = oci_parse ($conn, $kueri);
				oci_execute ($stmt); $tingkat_ada=false;
				while($r = oci_fetch_array ($stmt, OCI_BOTH)){
					$isi .= '<option value="'.$r[0].'" '; if($_POST["bukti"]==$r[0] and $lepas==false){ $isi .= 'selected'; } $isi .= '>'.$r[1].'</option>';
				}
			
				$isi .= '
				</select>
				Nomor : <input type=text name="no_bukti_fisik" size="30"></TD>
			</TR>
			<TR>
				<TD class="formulir">&nbsp;</TD>
				<TD class="formulir">&nbsp;</TD>
				<TD class="formulir">
				<input type=hidden name="nim" value="'.base64_encode($nimnya).'">
				<input type=submit name=ok value="Simpan">
				</TD>
			</TR>
			</form>
			</TABLE><br>
			<FONT COLOR="blue">*) Yang bisa diedit adalah data nama kegiatan,waktu pelaksanaan, penyelenggara, dan bukti fisik</FONT>
	';
	$isi .= '
			<br><hr><br>
	';
	if($_GET["hps"] and $_GET["getnim"]){
		$kueri = "select id_mhs from aucc.mahasiswa where nim_mhs='".AngkaHurufSaja(base64_decode($_GET["getnim"]))."' ";
		$stmt = oci_parse ($conn, $kueri);
		oci_execute ($stmt);
		$r = oci_fetch_array ($stmt, OCI_BOTH);
		$id_mhs = $r[0];

		$kueri = "delete from aucc.krp_khp where id_krp_khp='".AngkaSaja($_GET["hps"])."' and id_mhs='".$id_mhs."'";
		$stmt = oci_parse ($conn, $kueri);
		oci_execute ($stmt);
		if(ocirowcount($stmt)>0) {
			$isi .= '<FONT COLOR="blue">Data berhasil dihapus</FONT>';
		}else{
			$isi .= '<FONT COLOR="red">Data gagal dihapus</FONT>';
		}
	}
		//$param = "nim=123&tahun=11&semes=11";
		$param = $nimnya;
	$isi .= '<p align=ceter><b>'.$nimnya.' / '.$namanyamhs.'</b><br />
			<A HREF="mhs_lihat_tkk_cetak.php?param='.base64_encode($param).'" target="_BLANK">Cetak TKM</A></p>
			<TABLE width=100% align=center>
			<TR>
				<TD align=center class="judul" width=15%>&nbsp;</TD>
				<TD align=center class="judul">TAHUN AKADEMIK</TD>
				<TD align=center class="judul">NAMA KEGIATAN</TD>
				<TD align=center class="judul">WAKTU PELAKSANAAN</TD>
				<TD align=center class="judul">PENYELENGGARA</TD>
				<TD align=center class="judul">BUKTI</TD>
				<TD align=center class="judul">SKOR</TD>
			</TR>
	';
		$kueri = "select a.id_krp_khp, a.nm_krp_khp, a.penyelenggara_krp_khp, a.waktu_krp_khp, b.thn_akademik_semester, b.nm_semester, a.id_bukti_fisik, a.skor_krp_khp, c.nim_mhs from aucc.krp_khp a, aucc.semester b, aucc.mahasiswa c where a.id_semester=b.id_semester and a.id_mhs=c.id_mhs and a.id_mhs='".$idmhsnya."' order by b.thn_akademik_semester, b.nm_semester,a.nm_krp_khp";
		$stmt = oci_parse ($conn, $kueri);
		oci_execute ($stmt); $hit=0;
		while($r = oci_fetch_array ($stmt, OCI_BOTH)){
			$hit++;
			if($hit%2==0){
				$gaya = "isian2";
			}else{
				$gaya = "isian3";
			}
			if($r[6]=='0' or strlen($r[6])=='0'){
				$bukti = '-';
			}else{
				$kueri2 = "select nm_bukti_fisik from aucc.bukti_fisik where id_bukti_fisik='".$r[6]."' ";
				$stmt2 = oci_parse ($conn, $kueri2);
				oci_execute ($stmt2);
				$r2 = oci_fetch_array ($stmt2, OCI_BOTH);
				$bukti = $r2[0];
			}
			
			$isi .= '
			<TR>
				<TD align=center class="'.$gaya.'">
				[<A HREF="#?" onclick="if(confirm(\'Yakin Hapus ?\')!=0){ location.href=\'petugas_revisi_tkk_2.php?hps='.$r[0].'&getnim='.base64_encode($r[8]).'\'; }">hapus</A>]
				[<A HREF="petugas_revisi_tkk_2_edit.php?edit='.base64_encode($r[0]).'">edit</A>]
				</TD>
				<TD align=center class="'.$gaya.'">'.$r[4].' '.$r[5].'</TD>
				<TD align=left class="'.$gaya.'">'.$r[1].'</TD>
				<TD class="'.$gaya.'">'.$r[3].'</TD>
				<TD class="'.$gaya.'">'.$r[2].'</TD>
				<TD class="'.$gaya.'">'.$bukti.'</TD>
				<TD align=center class="'.$gaya.'">'.$r[7].'</TD>
			</TR>
			';
		}
		
	$isi .= '
			</TABLE>
		</div>
	</div>
	';
}

	require("template_setting.php");
}

?>