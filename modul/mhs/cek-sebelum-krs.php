<?php

require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

include 'class/eva.class.php';
include 'function-tampil-informasi.php';

$eva = new eva($db, $user->ID_PENGGUNA);

//Semester Aktif
$semester_aktif = $eva->GetSemesterSebelum();

//Data Mahasiswa
$db->Query("
        SELECT M.*,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI ,PS.ID_FAKULTAS
        FROM MAHASISWA M
        JOIN PENGGUNA P ON M.ID_PENGGUNA=P.ID_PENGGUNA
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
        WHERE M.ID_PENGGUNA='{$user->ID_PENGGUNA}'
    ");
$data_mhs = $db->FetchAssoc();

//Cek Pembayaran
$cek_pembayaran = $eva->CekPembayaran($data_mhs['ID_MHS'], $semester_aktif);

// Load Matakuliah Mahasiswa
$data_mata_kuliah = $eva->LoadKelasMahasiswa($semester_aktif);

//Data Dosen Wali
$db->Query("
        SELECT D.ID_DOSEN,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI
        FROM AUCC.DOSEN_WALI DW
        JOIN AUCC.DOSEN D ON DW.ID_DOSEN=D.ID_DOSEN
        JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
        JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
        WHERE DW.ID_MHS='{$data_mhs['ID_MHS']}' AND DW.STATUS_DOSEN_WALI=1
    ");
$data_dosen_wali = $db->FetchAssoc();



$check_message = '';

// Cek Pembayaran
if ($cek_pembayaran) {
    $check_message = alert_error("Anda belum melakukan Pembayaran...") . '<br/>';
}

// Cek Pengisian Evaluasi perkuliahan
if (!$eva->CekPengisianSemuaEvaluasiKuliah($data_mhs['ID_MHS'], $semester_aktif, count($data_mata_kuliah))) {
    $check_message .= alert_error("Anda belum mengisi Evaluasi Perkuliahan secara benar/lengkap...") . '<br/>';
}

// Cek Pengisian Evaluasi Perwalian

if (!$eva->CekPengisianEvaluasiWali($data_mhs['ID_MHS'], $data_dosen_wali['ID_DOSEN'], $semester_aktif)) {
    $check_message .= alert_error("Anda belum mengisi Evaluasi Perwalian secara benar/lengkap...") . '<br/>';
}

// Cek Pengisian Evaluasi Administrasi Fakultas
if (!$eva->CekPengisianEvaluasiAdmFak($data_mhs['ID_MHS'], $semester_aktif)) {
    $check_message .= alert_error("Anda belum mengisi Evaluasi Administrasi Fakultas secara benar/lengkap...") . '<br/>';
}

// Jika Mahasiswa SUdah Belajar 1 tahun
$tahun_sekarang = date('Y');
if (($data_mhs['THN_ANGKATAN_MHS'] == $tahun_sekarang - 1) && (!$eva->CekPengisianEvaluasiMaba($data_mhs['ID_MHS']))) {
    $check_message .= alert_error("Anda belum mengisi Evaluasi Evaluasi Mahasiswa Baru secara benar/lengkap...") . '<br/>';
}

// Jika terdapat kekurangan tampilkan Pesan dan tidak dilanjutkan
if ($check_message != '') {
    die('<div class="center_title_bar">Status Pengisian KRS</div><br/>'.$check_message);
}
?>
