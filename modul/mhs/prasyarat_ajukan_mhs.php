<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}


if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	// ambil semester_aktif
	$fakultas="";
	$kueri = "
	select c.id_fakultas
	from mahasiswa b, program_studi c
	where b.id_program_studi=c.id_program_studi and b.id_pengguna='".$user->ID_PENGGUNA."'
	";
	$result = $db->Query($kueri)or die ("salah kueri : ");
	while($r = $db->FetchRow()) {
		$fakultas = $r[0];
	}

	$jkueri_pass = '
	<script type="text/javascript">
		function tampildatadua(){
			$.ajax({
				type:"POST",
				url:"proses/_prasyarat_ajukan_simpan.php",
				data: "aksi=tampil",
				success: function(data){
					$("#password-update").html(data);
				}
			});
		}
		tampildatadua();

		function simpan(a,b){
			$("#password-update").fadeIn(2000);
			$("#password-update").html("<img src=\'loading.gif\'>");
			$.ajax({
				type:"POST",
				url:"proses/_prasyarat_ajukan_simpan.php",
				data: "aksi=simpan&ma_tidaklolos="+a+"&ma_tidaklolos_nilai="+b+"",
				success: function(data){
					alert(data);
					tampildatadua();
				}
			});
		}
		function hapus(c){
			$("#password-update").fadeIn(2000);
			$("#password-update").html("<img src=\'loading.gif\'>");
			$.ajax({
				type:"POST",
				url:"proses/_prasyarat_ajukan_simpan.php",
				data: "aksi=hapus&dihapus="+c+"",
				success: function(data){
					alert(data);
					tampildatadua();
				}
			});
		}
	</script>
	';
	if($fakultas!='4') {
		$jkueri_pass="";
	}

	$smarty->assign('jkueri_pass', $jkueri_pass);

	$smarty->display('biodata-prasyaratajukan.tpl');

}else {
	$smarty->display('session-expired.tpl');
}

?>
