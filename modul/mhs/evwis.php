<?php

include 'config.php';
include 'class/eva.class.php';
include 'function-tampil-informasi.php';

$eva = new eva($db, $user->ID_PENGGUNA);

//Data Mahasiswa
$db->Query("
    SELECT M.*,P.SE1,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI ,PS.ID_FAKULTAS
    FROM MAHASISWA M
    JOIN PENGGUNA P ON M.ID_PENGGUNA=P.ID_PENGGUNA
    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
    WHERE M.ID_PENGGUNA='{$user->ID_PENGGUNA}'
");
$data_mhs = $db->FetchAssoc();

//Periode Wisuda
$periode_wisuda = $eva->GetPeriodeWisudaAktif();

// Cek Yudisium
$cek_yudisium = $db->QuerySingle("SELECT COUNT(*) FROM AUCC.MAHASISWA WHERE ID_MHS='{$data_mhs['ID_MHS']}' AND STATUS_AKADEMIK_MHS=4");

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {

    if (isset($_POST)) {
        if (post('mode') == 'simpan') {
            //Simpan Evaluasi Perwalian
            for ($i = 1; $i <= post('jumlah_aspek'); $i++) {
                $eva->SaveEvaluasiWisuda(5, post('kel_aspek' . $i), post('aspek' . $i), $data_mhs['ID_MHS'], $data_mhs['ID_PROGRAM_STUDI']
                        , $data_mhs['ID_FAKULTAS'], $periode_wisuda, post('nilai' . $i));
            }
        }
    }

    // Jika Sudah Melakukan Pengisian Evaluasi 
    if ($eva->CekPengisianEvaluasiWisuda($data_mhs['ID_MHS'])) {
        $smarty->assign('alert', alert_success("Anda telah berhasil mengisi Evaluasi Wisuda, Terima kasih atas partisipasinya, Semoga Sukses..."));
    } else if ($cek_yudisium == 0) {
        $smarty->assign('alert', alert_error("Anda Belum Yudisium, hubungi akademik fakultas..."));
    } else {
        $smarty->assign('data_aspek', $eva->LoadEvaluasiAspek(5));
    }
    $smarty->display('evwis.tpl');
} else {
    $smarty->display('session-expired.tpl');
}
?>
