<?php
require('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

	if ($user->ID_FAKULTAS == 11) // Psikologi
	{
		echo '
		<script>
		location.href="#akademik-ujian!akademik-jadwal-psi.php";
		</script>';
		exit;
	}

// INI PENYAKITANYA !!!	
/*$db2 = new MyOracle();*/

// ambil nim & password
	$mhs_pass=""; $mhs_nim=""; $angkatan=""; $status=""; $id_mhs="";
	$kueri = "select b.nim_mhs,b.thn_angkatan_mhs, b.status, a.se1,b.id_mhs from pengguna a, mahasiswa b where a.id_pengguna=b.id_pengguna and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 6 : ");
	while($r = $db->FetchRow()) {
		$mhs_pass = $r[3];
		$mhs_nim = $r[0];
		$angkatan=$r[1];
		$status=$r[2];
		$id_mhs=$r[4];
	}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
// Yudi Sulistya, 02 Aug 2013 (tablesorter)
echo '
<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[0,0],[2,0]], widgets: ["zebra"]
		}
	);
}
);
</script>
';

	// ambil prodi
	$id_prodi="";
	$kueri = "select a.ID_PROGRAM_STUDI,b.id_fakultas from mahasiswa a, program_studi b where a.id_program_studi=b.id_program_studi and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : 26");
	while($r = $db->FetchRow()) {
		$id_prodi = $r[0];
		$id_fakul = $r[1];
	}
	
	// ambil semester_aktif
	$sem_aktif=""; $nm_semester = "";
	$kueri = "select id_semester, nm_semester,tahun_ajaran from semester where STATUS_AKTIF_SEMESTER='True' and id_perguruan_tinggi = {$id_pt_user} order by id_semester desc";
	$result = $db->Query($kueri)or die ("salah kueri : 34");
	while($r = $db->FetchRow()) {
		$sem_aktif = $r[0];
		$nm_semester = $r[1]." ".$r[2];
	}

	$tampil_data = '
	<p><center><h2>Jadwal Kuliah Semester '.$nm_semester.'</h2><a onclick="window.open(\'./proses/e_pembelajaran/e_pembelajaran.php\', \'e-Pembelajaran\');">....</a></center></p>
	<table width="850" id="myTable" class="tablesorter">
	<thead>
		<tr>
			<th align="center">Mata Ajar</th>
			<th align="center">Sks</th>
			<th align="center">Kelas</th>
			<th align="center">Jadwal</th>
			<th align="center">PJMA</th>
			<th align="center">Ruang</th>
			<th align="center">Petugas</th>
		</tr>
	</thead>
	<tbody>
	';
	/*
	$kueri = "
	select e.id_mata_kuliah,b.id_jadwal_hari,a.id_ruangan,c.id_kelas_mk
	from jadwal_kelas a, jadwal_kuliah b, kelas_mk c, semester d, kurikulum_mk e
	where a.id_jadwal_kuliah=b.id_jadwal_kuliah and a.id_kelas_mk=c.id_kelas_mk and c.id_kurikulum_mk=e.id_kurikulum_mk
	and c.id_semester=d.id_semester and d.status_aktif_semester='True' and c.ID_PROGRAM_STUDI='".$prodi."'
	group by e.id_mata_kuliah,b.id_jadwal_hari,a.id_ruangan,c.id_kelas_mk
	";
	*/

	// nama_mk sks kelas jadwal(hari+jam) JMMA ruangan petugas
	$kueri = "";
	/*if($id_fakul=='3') {
		// khusus FHUKUM = umum
		$kueri = "
		select a.kd_mata_kuliah, a.nm_mata_kuliah, d.kredit_semester, d.id_mata_kuliah, d.id_kurikulum_mk, c.no_kelas_mk, c.id_kelas_mk,
		c.terisi_kelas_mk, c.kapasitas_kelas_mk
		from mata_kuliah a, krs_prodi b, kelas_mk c, kurikulum_mk d, jadwal_kelas e
		where c.id_kurikulum_mk=d.id_kurikulum_mk and a.id_mata_kuliah=d.id_mata_kuliah and c.id_kelas_mk=b.id_kelas_mk and c.id_kelas_mk=e.id_kelas_mk
		and b.id_semester='".$sem_aktif."' and b.id_program_studi='".$id_prodi."'
		";
	}else if($id_fakul=='4') {
		// khusus FE
		if ($status=='AJ'){
			$kueri = "
			select a.kd_mata_kuliah, a.nm_mata_kuliah, d.kredit_semester, d.id_mata_kuliah, d.id_kurikulum_mk, c.no_kelas_mk, c.id_kelas_mk,
			c.terisi_kelas_mk, c.kapasitas_kelas_mk
			from mata_kuliah a, krs_prodi b, kelas_mk c, kurikulum_mk d, jadwal_kelas e
			where c.id_kurikulum_mk=d.id_kurikulum_mk and a.id_mata_kuliah=d.id_mata_kuliah and c.id_kelas_mk=b.id_kelas_mk and c.id_kelas_mk=e.id_kelas_mk
			and b.id_semester='".$sem_aktif."' and b.id_program_studi='".$id_prodi."'
			and (c.status='".$status."' or b.status='".$status."')";
		} else {
			$kueri = "
			select a.kd_mata_kuliah, a.nm_mata_kuliah, d.kredit_semester, d.id_mata_kuliah, d.id_kurikulum_mk, c.no_kelas_mk, c.id_kelas_mk,
			c.terisi_kelas_mk, c.kapasitas_kelas_mk
			from mata_kuliah a, krs_prodi b, kelas_mk c, kurikulum_mk d, jadwal_kelas e
			where c.id_kurikulum_mk=d.id_kurikulum_mk and a.id_mata_kuliah=d.id_mata_kuliah and c.id_kelas_mk=b.id_kelas_mk and c.id_kelas_mk=e.id_kelas_mk
			and b.id_semester='".$sem_aktif."' and b.id_program_studi='".$id_prodi."'
			-- and a.kd_mata_kuliah in (select kd_mata_kuliah from kurikulum_feb where id_program_studi='".$id_prodi."' and tahun='".$angkatan."' and status='".$status."')
			and (c.status='".$status."' or b.status='".$status."')";

		}

	}else if($id_fakul=='7') {
		$kueri = "
		select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nama_kelas,
		nm_jadwal_hari,jam_mulai||':'||menit_mulai||' - '||jam_selesai||':'||menit_selesai,
		nm_ruangan,gelar_depan||' '||nm_pengguna||', '||gelar_belakang
		from pengambilan_mk
		left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
		left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
		left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
		left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
		left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
		left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
		left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
		left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
		left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1
		left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
		left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
		where id_mhs='{$id_mhs}' and pengambilan_mk.id_semester='{$sem_aktif}' and status_apv_pengambilan_mk=1
		";
		$result = $db->Query($kueri);

		while($r = $db->FetchRow()) {

		$tampil_data .= '
		<tr>
			<td><span>'.$r[0].'<br>'.$r[1].'</span></td>
			<td align="center">'.$r[2].'</td>
			<td align="center">'.$r[3].'</td>
			<td align="center">'.$r[4].' - '.$r[5].'</td>
			<td align="center">'.$r[7].'</td>
			<td align="center">'.$r[6].'</td>
			<td align="center"></td>
		</tr>
		';

		}
		
		$tampil_data .= '
		</tbody>
		</table>
		';
		
		$smarty->assign('tampil_data', $tampil_data);
		$smarty->display('akademik-jadwal.tpl');
		exit;

	}

	else if($id_fakul=='8'){
		$kueri = "
SELECT 
h.nm_mata_kuliah,
g.kredit_semester,
i.nmkelas,
B.NM_RUANGAN,
C.NM_JADWAL_HARI,
D.NM_JADWAL_JAM NM_JADWAL_JAM_AWAL,
E.NM_JADWAL_JAM NM_JADWAL_JAM_AKHIR,
l.nm_asli
FROM JADWAL_KELAS A
LEFT JOIN RUANGAN B ON (A.ID_RUANGAN=B.ID_RUANGAN)
LEFT JOIN JADWAL_HARI C ON (A.ID_JADWAL_HARI=C.ID_JADWAL_HARI)
LEFT JOIN JADWAL_JAM D ON (A.ID_JADWAL_JAM_MULAI=D.ID_JADWAL_JAM)
LEFT JOIN JADWAL_JAM E ON (A.ID_JADWAL_JAM_SELESAI=E.ID_JADWAL_JAM)
left join kelas_mk f on f.id_kelas_mk=a.id_kelas_mk
LEFT JOIN kurikulum_mk G on g.id_kurikulum_mk=f.id_kurikulum_mk
left join mata_kuliah h on h.id_mata_kuliah=g.id_mata_kuliah
left join fst_kelas i on i.kdkelas=f.tipe_kelas_mk
left join (select * from pengampu_mk where pjmk_pengampu_mk='1') j on j.id_kelas_mk=a.id_kelas_mk
left join dosen k on k.id_dosen=j.id_dosen
left join pengguna l on l.id_pengguna=k.id_pengguna
WHERE A.ID_KELAS_MK in 
(select id_kelas_mk from pengambilan_mk where id_mhs='{$id_mhs}' and id_semester='{$sem_aktif}')
		";
$result = $db->Query($kueri);
$warna="";$i=0;
while($r = $db->FetchRow()) {
$i++;
$ruangan_penjaga="";
$pjmk=$r[7];
		$tampil_data .= '
		<tr bgcolor='.$warna.'>
			<td><span>'.$r[0].'</span></td>
			<td align="center">'.$r[1].'</td>
			<td align="center">'.$r[2].'</td>
			<td align="center">'.$r[4].'['.$r[5].'-'.$r[6].']</td>
			<td align="center">'.$pjmk.'</td>
			<td align="center">'.$r[3].'</td>
			<td align="center">'.$ruangan_penjaga.'</td>
		</tr>
		';

}
		$tampil_data .= '
		</tbody>
		</table>
		';
		$smarty->assign('tampil_data', $tampil_data);
		$smarty->display('akademik-jadwal.tpl');
		exit;
	}
	else{*/
		// umum
		// perubahan kueri ke pengambilan_mk dan id_mhs biar lebih jitu
		// FIKRIE (28-08-2016)
		/*$kueri = "
		select a.kd_mata_kuliah, a.nm_mata_kuliah, d.kredit_semester, d.id_mata_kuliah, d.id_kurikulum_mk, c.no_kelas_mk, c.id_kelas_mk,
		c.terisi_kelas_mk, c.kapasitas_kelas_mk, e.nama_kelas
		from mata_kuliah a, krs_prodi b, kelas_mk c, kurikulum_mk d, jadwal_kelas e, pengambilan_mk f, nama_kelas e
		where c.id_kurikulum_mk=d.id_kurikulum_mk and a.id_mata_kuliah=d.id_mata_kuliah and c.id_kelas_mk=b.id_kelas_mk and c.id_kelas_mk=e.id_kelas_mk 
			and f.id_kurikulum_mk = d.id_kurikulum_mk and f.id_kelas_mk = c.id_kelas_mk
			and e.id_nama_kelas = c.no_kelas_mk
		and b.id_semester='".$sem_aktif."' and b.id_program_studi='".$id_prodi."' and f.id_mhs = '".$id_mhs."'
		";*/

	$data_set = $db->QueryToArray("select a.kd_mata_kuliah, a.nm_mata_kuliah, d.kredit_semester, d.id_mata_kuliah, d.id_kurikulum_mk, c.no_kelas_mk, c.id_kelas_mk,
		c.terisi_kelas_mk, c.kapasitas_kelas_mk, e.nama_kelas
		from mata_kuliah a, krs_prodi b, kelas_mk c, kurikulum_mk d, jadwal_kelas e, pengambilan_mk f, nama_kelas e
		where c.id_kurikulum_mk=d.id_kurikulum_mk and a.id_mata_kuliah=d.id_mata_kuliah and c.id_kelas_mk=b.id_kelas_mk and c.id_kelas_mk=e.id_kelas_mk 
			and f.id_kurikulum_mk = d.id_kurikulum_mk and f.id_kelas_mk = c.id_kelas_mk
			and e.id_nama_kelas = c.no_kelas_mk
		and b.id_semester='".$sem_aktif."' and b.id_program_studi='".$id_prodi."' and f.id_mhs = '".$id_mhs."'");

	foreach ($data_set as $r) {
		$kd_mata_kuliah = $r['KD_MATA_KULIAH'];
		$nm_mata_kuliah = $r['NM_MATA_KULIAH'];
		$kredit_semester = $r['KREDIT_SEMESTER'];
		$id_kelas_mk = $r['ID_KELAS_MK'];
		$nm_kelas = $r['NAMA_KELAS'];

		// AMBIL JADWAL
		$jamnya = ""; $harinya = ""; $ruangannya = "";
		$result2 = $db->Query("select a.nm_jadwal_jam,b.id_jadwal_hari,b.id_jadwal_jam, b.id_ruangan from aucc.jadwal_jam a, aucc.jadwal_kelas b where a.id_jadwal_jam=b.id_jadwal_jam and b.id_kelas_mk='".$id_kelas_mk."'")or die("salah kueri 151 : ");
		while($r2 = $db->FetchRow()) {
			if($r2[1]=='1') {
				$harinya = "Minggu";
			}else if($r2[1]=='2') {
				$harinya = "Senin";
			}else if($r2[1]=='3') {
				$harinya = "Selasa";
			}else if($r2[1]=='4') {
				$harinya = "Rabu";
			}else if($r2[1]=='5') {
				$harinya = "Kamis";
			}else if($r2[1]=='6') {
				$harinya = "Jumat";
			}else if($r2[1]=='7') {
				$harinya = "Sabtu";
			}
			$jamnya = $r2[0];
			$ruangannya = $r2[3];
		}

		$ruangan=""; $ruangan_penjaga="";
		// ambil ruangan
		$kueri2 = "select nm_ruangan, id_penanggung_jawab from ruangan where id_ruangan='".$ruangannya."'";
		$result2 = $db->Query($kueri2)or die("salah kueri 175 ");
		while($r2 = $db->FetchRow()) {
			$ruangan = $r2[0];
			$ruangan_penjaga = $r2[1];
		}

		// ambil pjmk
		$pjmk="";
		$kueri2 = "select c.nm_pengguna from pengampu_mk a, dosen b, pengguna c where a.id_dosen=b.id_dosen and b.id_pengguna=c.id_pengguna and a.id_kelas_mk='".$id_kelas_mk."'";
		$result2 = $db->Query($kueri2)or die("salah kueri 184 ");
		while($r2 = $db->FetchRow()) {
			$pjmk = $r2[0];
		}

		$tampil_data .= '
		<tr>
			<td><span>'.$kd_mata_kuliah.'<br>'.$nm_mata_kuliah.'</span></td>
			<td align="center">'.$kredit_semester.'</td>
			<td align="center">'.$nm_kelas.'</td>
			<td align="center">'.$harinya.' - '.$jamnya.'</td>
			<td align="center">'.$pjmk.'</td>
			<td align="center">'.$ruangan.'</td>
			<td align="center">'.$ruangan_penjaga.'</td>
		</tr>
		';

	}		

	
	$tampil_data .= '
	</tbody>
	</table>
	';

	$smarty->assign('tampil_data', $tampil_data);

	$smarty->display('akademik-jadwal.tpl');
	exit;

}else {
	$smarty->display('session-expired.tpl');
}
?>
