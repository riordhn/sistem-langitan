<?php
include 'config.php';
include '../keuangan/class/history.class.php';
require 'function-tampil-informasi-no-conf.php';
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}
$history = new history($db);
$nim_mhs = $db->QuerySingle("SELECT NIM_MHS FROM MAHASISWA WHERE ID_PENGGUNA ='{$user->ID_PENGGUNA}'");

$id_mhs = $db->QuerySingle("SELECT ID_MHS FROM MAHASISWA WHERE ID_PENGGUNA ='{$user->ID_PENGGUNA}'");

$smarty->assign('info', alert_info("<b>Perhatian</b>! Data pembayaran sebelum <b>Bulan November Tahun 2019</b>
sedang dalam proses migrasi.<hr> Akibatnya akan terjadi  perbedaan antara data pembayaran 
yang resmi dengan yang tertera di Sistem.<br/>
<b>Silahkan Hubungi Bagian Keuangan untuk info selengkapnya</b>.
"));
$data_va_mhs=$history->load_data_va($nim_mhs);
$new_data_va_mhs =[];
foreach($data_va_mhs as $data){
    if(date('Y-m-d')<date('Y-m-d',strtotime($data['TGL_EXPIRED']))){
        array_push($new_data_va_mhs,$data);
    }
}
$smarty->assign('nim_mhs', $nim_mhs);
$smarty->assign('data_va_mhs', $new_data_va_mhs);

$smarty->assign('data_pembayaran', $history->load_riwayat_pembayaran_mhs($nim_mhs));

$link = 'proses/cetak_kartu_keuangan.php?x=';
$smarty->assign('cetak_kartu_link', $link);

$smarty->display('keuangan-riwayat-new.tpl');
