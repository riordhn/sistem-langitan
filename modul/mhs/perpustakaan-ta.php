<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	$jkueri_buku = '
		<script type="text/javascript">
			function tampil(f){
				$.ajax({
					type:"POST",
					url:"proses/_perpustakaan-ta-tampil.php",
					data: $("#"+f).serialize(),
					success: function(data){
						$("#tampil_buku").html(data);
					}
				});
			};
			$("#frmcaribuku").submit(function(){
				$.ajax({
					type:"POST",
					url:"proses/_perpustakaan-ta-tampil.php",
					data: $("#"+f).serialize(),
					success: function(data){
						$("#tampil_buku").html(data);
					}
				});
				return false;
			});
		</script>
		';
	$smarty->assign('jkueri_buku', $jkueri_buku);


	$buku_cari = '
	<form name="frmcaribuku" method=post action="perpustakaan-ta.php" id="frmcaribuku">
	<table>
	<tr>
		<td>Perpustakaan</td>
		<td>:</td>
		<td>
		<select name="perpus">
			<option value="all">Semua</option>
			<option value="pusat">Pusat</option>
			<option value="fk">FK</option>
		</select>
		</td>
	</tr>
	<tr>
		<td>Judul Skripsi</td>
		<td>:</td>
		<td><input type=text name="judul" size="50"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>
		<input type=hidden name="aksi" value="tampil">
		<input type=button name="ok" value="Tampil" onclick="tampil(\'frmcaribuku\')">
		</td>
	</tr>
	</table>
	</form>
	';

	$smarty->assign('buku_cari', $buku_cari);

	$smarty->display('perpustakaan-ta.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>
