<?php

include 'config.php';

// security sementara terhadap no auth access ~sugenk.


include 'function-tampil-informasi.php';
include 'class/upload.class.php';

$upload = new upload($db, $user->ID_PENGGUNA);


//if ($_SERVER['REMOTE_ADDR'] != '210.57.212.66') {
//    die('<div class="center_title_bar">Upload File Skripsi</div><br/>' . alert_error("Mohon maaf  , masih dalam proses perbaikan.Terima kasih"));
//}


// Data Mahasiswa
$db->Query("
        SELECT M.*,PS.NM_PROGRAM_STUDI,J.ID_JENJANG,J.NM_JENJANG,P.NM_PENGGUNA
        FROM MAHASISWA M
        JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
        JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
        WHERE M.ID_PENGGUNA='{$user->ID_PENGGUNA}'
    ");
$data_mhs = $db->FetchAssoc();
//$upload->SettingConfigUpload();
//
//if ($user->ID_PENGGUNA == 52737) {
//    $upload->ResetUpload(52737);
//}

if (isset($_GET)) {
    if (get('mode') == 'frame') {
        if (isset($_POST)) {
            if (post('mode') == 'upload' && $user->ID_PENGGUNA != '') {
                $db->Query("SELECT * FROM UPLOAD_SERVER WHERE STATUS_AKTIF=1");
                $server = $db->FetchAssoc();
                $db->Query("SELECT * FROM UPLOAD_FOLDER WHERE ID_UPLOAD_SERVER='{$server['ID_UPLOAD_SERVER']}' AND KEDALAMAN=0 AND AKSES=1");
                $direktori = $db->FetchAssoc();
                $new_folder = '../..' . $direktori['PATH_UPLOAD_FOLDER'] . '/' . $direktori['NAMA_UPLOAD_FOLDER'] . '/' . $data_mhs['NIM_MHS'];
			
                if (is_dir($new_folder) && count(glob($new_folder)) == 0) {
                    // Hapus Folder Yang sudah terbuat
                    rmdir($new_folder);
                    $cek_folder_db = $db->QuerySingle("SELECT COUNT(*) FROM UPLOAD_FOLDER WHERE NAMA_UPLOAD_FOLDER LIKE '%{$data_mhs['NIM_MHS']}%'");
                    // Hapus Folder Database
                    if ($cek_folder_db > 0) {
                        $db->Query("DELETE FROM UPLOAD_FOLDER WHERE NAMA_UPLOAD_FOLDER LIKE '%{$data_mhs['NIM_MHS']}%' ");
                    }
                } else if (!is_dir($new_folder)) {
                    // Buat Folder Baru
                    mkdir($new_folder);
                    chmod($new_folder, 0777);
                    // Input Folder Database
                    $db->Query("
                        INSERT INTO UPLOAD_FOLDER 
                            (ID_UPLOAD_SERVER,ID_PARENT_FOLDER,NAMA_UPLOAD_FOLDER,PATH_UPLOAD_FOLDER,KEDALAMAN,AKSES)
                        VALUES
                            ('{$server['ID_UPLOAD_SERVER']}','{$direktori['ID_UPLOAD_FOLDER']}','{$data_mhs['NIM_MHS']}','','1','1')
                        ");
                }

                $allowed_exts = array("pdf");
                $allowed_type = array('application/pdf');
                $error_upload = '';
                for ($i = 1; $i <= post('jumlah_file'); $i++) {
                    //Properties File
                    $extension = end(explode(".", $_FILES["file" . $i]["name"]));
                    $type = $_FILES["file" . $i]["type"];
                    $name = $_FILES["file" . $i]["name"];
                    $tmp_name = $_FILES["file" . $i]["tmp_name"];
                    $size = $_FILES["file" . $i]["size"];
                    $error = $_FILES["file" . $i]["error"];
                    // Format Nama File 
                    $file_name = strtolower(post('nama' . $i) . '_' . $data_mhs['NM_JENJANG'] . '.' . $extension);
                    $deskripsi = post('des' . $i);
                    $id_bag_upload = post('id_bag' . $i);
                    if ($id_bag_upload != '') {
                        //Cek Extension dan Type
                        if (in_array($extension, $allowed_exts) && in_array($type, $allowed_type)) {
                            if ($error > 0) {
                                $error_upload .= alert_error("Error file :" . $error);
                            } else {
                               // if (file_exists($new_folder . '/' . $file_name)) {
                               //     $error_upload .= alert_error($file_name . " Sudah Ada. ");
                               //} else {
                                    // Simpan data fisik
                                    move_uploaded_file($tmp_name, "$new_folder/$file_name");
                                    // Menyimpan data file secara database
                                    $id_folder = $upload->GetIdFolderFile($data_mhs['NIM_MHS']);
                                    $upload->InsertFileDatabase($id_folder, $file_name, $deskripsi);
                                    $id_file = $upload->GetLastIdFile();
                                    $upload->InsertStatusUploadFile($data_mhs['ID_MHS'], $id_bag_upload, $id_file);
                               // }
                            }
                        } else {
                            $error_upload .= alert_error("Format File {$name} tidak sesuai");
                        }
                    }
                }
                $smarty->assign('error_upload', $error_upload);
            }
        }
    }
}




// Cek Upload File
if ($upload->CheckStatusUpload($data_mhs['ID_MHS'], $data_mhs['ID_JENJANG'])) {
    $smarty->assign('status_isi', alert_success("Terima Kasih anda telah berhasil upload semua file, lanjutkan ke tahap berikutnya. Semoga Sukses"));
}
$cek_wisuda = $upload->CheckStatusWisuda($data_mhs['ID_MHS'], $data_mhs['ID_JENJANG']);
$smarty->assign('cek_wisuda', $cek_wisuda);
$smarty->assign('info_1', alert_success("Tugas Akhir Perlu di pecah beberapa bagian, Sesuai dengan jenjang {$data_mhs['NM_JENJANG']} "));
$smarty->assign('info_2', alert_success("Tipe Format File Upload Tugas Akhir harus menggunakan PDF"));
$smarty->assign('data_mhs', $data_mhs);
$smarty->assign('data_bagian', $upload->LoadBagTaMhs($data_mhs['ID_MHS'], $data_mhs['ID_JENJANG']));
$smarty->display('upload-skripsi.tpl');
?>