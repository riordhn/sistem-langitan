<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

$db2 = new MyOracle();

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {

// Yudi Sulistya, 02 Aug 2013 (tablesorter)
echo '
<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]]
		}
	);
}
);
</script>
';

	// ambil nama di tabel pengguna
	$mhs_nama = "";
	$kueri = "select NM_PENGGUNA from pengguna where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ");
	while($r = $db->FetchRow()) {
		$mhs_nama = $r[0];
	}

	// ambil id_mhs
	$id_mhs=""; $mhs_nim="";
	$kueri = "
	select a.id_mhs, a.NIM_MHS, c.nm_fakultas, b.nm_program_studi, d.nm_jenjang, a.TGL_LULUS_MHS, a.TGL_TERDAFTAR_MHS, a.no_ijazah, a.id_program_studi
	from mahasiswa a, program_studi b, fakultas c, jenjang d
	where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and b.id_jenjang=d.id_jenjang and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
		$mhs_nim = $r[1];
		$mhs_fakul = $r[2];
		$mhs_prodi = $r[3];
		$mhs_jenjang = $r[4];
		$mhs_tgl_lulus = $r[5];
		$mhs_tgl_terdaftar = $r[6];
		$mhs_noijazah = $r[7];
		$mhs_prodi_id = $r[8];
	}

	$jum_sks=0; $jum_bobot=0; $ipk=0;

	$tot = "
	select e.tahun_ajaran, e.nm_semester, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf asc,e.thn_akademik_semester desc,e.nm_semester desc) rangking,count(*) over(partition by c.nm_mata_kuliah) terulang,c.status_praktikum,a.status_hapus
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1'
	order by e.tahun_ajaran desc, e.nm_semester desc, c.kd_mata_kuliah
	";

	$result = $db->Query($tot)or die("salah kueri1 : ");
	while($r = $db->FetchRow()) {
			$semester = $r[0].' '.$r[1];
		
			// ambil bobot nilai huruf
			$bobot=0;
			$kueri2 = "select NILAI_STANDAR_NILAI from standar_nilai where NM_STANDAR_NILAI='".trim($r[5])."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ");
			while($r2 = $db2->FetchRow()) {
				$bobot = $r2[0];
			}
			if($r[5]<'E' and $r[5]!='' and $r[5]!='-' and $r[7]==1 and $r[10]==0 ){
				$jum_sks += $r[4];
				$jum_bobot += ($bobot*$r[4]);
			}
		}

	if($jum_sks==0) {
		$ipk='0.00';
	}else{
		$ipk = number_format(($jum_bobot/$jum_sks),2);
	}
	
	$isi_transkrip = '
		<table cellspacing="0" cellpadding="0" border="0" width="100%" class="tb_frame">
		<tr>
			<td align=center><img src='.$base_url.'img/akademik_images/logounair.gif" width="100" height="100" border="0" alt=""></td>
			<td>
			UNIVERSITAS AIRLANGGA<BR>
			FAKULTAS '.strtoupper($mhs_fakul).'<BR>
			Kampus C Mulyorejo Surabaya 60115<br>
			Telp. (031) 5914042, 5914043, 5912564 Fax (031) 5981841<br>
			Website : http://www.unair.ac.id; e-mail:rektor@unair.ac.id
			</td>
		</tr>
		</table>
		<table cellspacing="0" cellpadding="0" border="0" width="100%" class="tb_frame">
		<tbody>
		<tr>
			<td>Nama</td>
			<td><strong>: '.$mhs_nama.'</strong></td>
			<td>&nbsp;</td>
			<td>Nomor Ijazah</td>
			<td><strong>: '.$mhs_noijazah.'</strong></td>
		</tr>
		<tr>
			<td>NIM</td>
			<td><strong>: '.$mhs_nim.'</strong></td>
			<td>&nbsp;</td>
			<td>Jumlah SKS</td>
			<td><strong>: '.$jum_sks.'</strong></td>
		 </tr>
		<tr>
			<td>Program Studi</td>
			<td><strong>: '.$mhs_jenjang.' - '.$mhs_prodi.'</strong></td>
			<td>&nbsp;</td>
			<td>IP Kumulatif</td>
			<td><strong>: '.$ipk.'</strong></td>
		</tr>
		<tr>
			<td width="32%">Tanggal Terdaftar Pertama Kali</td>
			<td width="25%"><strong>: '.$mhs_tgl_terdaftar.'</strong></td>
			<td width="7%">&nbsp;</td>
			<td width="16%">Tanggal Lulus</td>
			<td width="20%"><strong>: '.$mhs_tgl_lulus.'</strong></td>
		</tr>
		</tbody>
		</table>
		<br>

		<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable" class="tablesorter">
		<thead>
		<tr>
			<th width="80px">Semester</th>
			<th>Kode MA</th>
			<th>Nama Mata Ajar</th>
			<th>SKS</th>
			<th>Nilai</th>
			<th>Bobot</th>
		</tr>
		</thead>
		<tbody>
	';
	$jum_sks=0; $jum_bobot=0; $ipk=0;

	$kueri = "
	select e.tahun_ajaran, e.nm_semester, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf asc,e.thn_akademik_semester desc,e.nm_semester desc) rangking,count(*) over(partition by c.nm_mata_kuliah) terulang,c.status_praktikum,a.status_hapus
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1'
	";

	$result = $db->Query($kueri)or die("salah kueri1 : ");
	while($r = $db->FetchRow()) {
		if($r[6]=='1') { // filter FLAGNILAI
			$semester = $r[0].' '.$r[1];
		
			// ambil bobot nilai huruf
			$bobot=0;
			$kueri2 = "select NILAI_STANDAR_NILAI from standar_nilai where NM_STANDAR_NILAI='".trim($r[5])."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ");
			while($r2 = $db2->FetchRow()) {
				$bobot = $r2[0];
			}
			if($r[7]==1 and $r[8]>1)$warna='#00FF00';
			else {
				if($r[7]>1)$warna='#FF0000';
				else {
					if($r[5]=='-')$warna='yellow';
					$warna='#FFFFFF';
				}
			}
			if(($r[5]=='E' or $r[5]=='') and ($r[9]>1) ){
				$warna='#cccccc';
			}
			if($r[10]=='1' ){
				$warna='#772244';
			}			
			$isi_transkrip .= '
				<tr bgcolor="'.$warna.'">
					<td>'.$semester.'</td>
					<td>'.$r[2].'</td>
					<td>'.$r[3].'</td>
					<td align="center">'.$r[4].'</td>
					<td align="center">'.$r[5].'</td>
					<td align="center">'.number_format(($bobot*$r[4]),2).'</td>
				</tr>
			';
			if($r[5]<'E' and $r[5]!='' and $r[5]!='-' and $r[7]==1 and $r[10]==0 ){
				$jum_sks += $r[4];
				$jum_bobot += ($bobot*$r[4]);
			}
		}else{
			$semester = $r[0].' '.$r[1];
			$isi_transkrip .= '
				<tr bgcolor="yellow">
					<td> '.$semester.'</td>
					<td>'.$r[2].'</td>
					<td>'.$r[3].'</td>
					<td align="center">'.$r[4].'</td>
					<td align="center">-</td>
					<td align="center">-</td>
				</tr>
			';
		}
	}

	if($jum_sks==0) {
		$ipk='0.00';
	}else{
		$ipk = number_format(($jum_bobot/$jum_sks),2);
	}
	
	$isi_transkrip .= '
		</tbody>
		<tr>
			<td rowspan="3" colspan="2"></td>
			<td><div align="left">Jumlah SKS dan Bobot</div></td>
			<td align="center">'.$jum_sks.'</td>
			<td align="center">&nbsp;</td>
			<td align="center">'.number_format($jum_bobot,2).'</td>
		</tr>
		<tr>
			<td>IP Kumulatif</td>
			<td colspan="3"><div align="center">'.$ipk.'</div></td>
		</tr>
		<tr>
			<td>Predikat Kelulusan</td>
			<td colspan="3"><div align="center">&nbsp;</div></td>
		</tr>
		</table>
		Keterangan: <br>
		<span>Putih</span> : Normal, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif<br>
		<span style="background-color:green">Hijau</span> : Ulangan, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif<br>
		<span style="background-color:yellow">Kuning</span> : Nilai belum dikeluarkan, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif<br>
		<span style="background-color:red">Merah</span> : Sudah diulang, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif<br>
		<span style="background-color:#cccccc">Abu-abu</span> : PKL,KKN,TA,PROPOSAL,SEMINAR,SKRIPSI,THESIS yang belum masuk nilainya. Tidak masuk perhitungan IPK dan sks kumulatif.<br>
		<span style="background-color:#772244">Ungu</span> : MK Pilihan yang dihapus oleh akademik atas permintaan mahasiswa. Tidak masuk perhitungan IPK dan sks kumulatif.<br>
	';
	//<input type=button name="dicetak" value="Cetak" onclick="window.open(\'proses/_akademik-transkrip_cetak.php\',\'baru2\');">

	$smarty->assign('isitranskrip', $isi_transkrip);

	$smarty->display('akademik-transkrip.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>
