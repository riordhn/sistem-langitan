<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

	$id_pengguna = $user->ID_PENGGUNA;
	
	$id_status = array();
	$id_pengguna_status = array();
	$status = array();
	$tgl_status = array();
	$nm_pengguna_status = array();
	$username_status = array();
	
	$id_status_balas = array();
	$id_pengguna_status_balas = array();
	$status_balas = array();
	$tgl_status_balas = array();
	$nm_pengguna_status_balas = array();
	$username_status_balas = array();
	
	$like = array();
	$comment = array();
	$id = $_GET['id'];
	
	$jml_status = 0;
	
	$db->Query("select sn.id_sn_status, sn.id_pengguna, sn.sn_status, sn.sn_status_tgl, p.nm_pengguna, p.username from sn_status sn
				left join pengguna p on p.id_pengguna = sn.id_pengguna
				where sn.id_sn_status = '" . $id . "'
	");
	
	$i = 0;
	while ($row = $db->FetchRow()){ 
		$id_status[$i] = $row[0];
		$id_pengguna_status[$i] = $row[1];
		$status[$i] = $row[2];
		$tgl_status[$i] = $row[3];
		$nm_pengguna_status[$i] = $row[4];
		$username_status[$i] = $row[5];
		$jml_status++;
		$i++;
	}
	
	$db->Query("select sn.id_sn_comment, sn.id_pengguna, sn.sn_status, sn.sn_status_tgl, p.nm_pengguna, p.username from sn_status sn
				left join pengguna p on p.id_pengguna = sn.id_pengguna
				where sn.id_sn_comment <> 0 order by sn.id_sn_status
	");
	
	$i = 0;
	while ($row = $db->FetchRow()){ 
		$id_status_balas[$i] = $row[0];
		$id_pengguna_status_balas[$i] = $row[1];
		$status_balas[$i] = $row[2];
		$tgl_status_balas[$i] = $row[3];
		$nm_pengguna_status_balas[$i] = $row[4];
		$username_status_balas[$i] = $row[5];
		$jml_status_balas++;
		$i++;
	}
	
	$smarty->assign('id_status', $id_status);
	$smarty->assign('id_pengguna_status', $id_pengguna_status);
	$smarty->assign('status', $status);
	$smarty->assign('tgl_status', $tgl_status);
	$smarty->assign('nm_pengguna_status', $nm_pengguna_status);
	$smarty->assign('username_status', $username_status);
	$smarty->assign('jml_status', $jml_status);
	
	$smarty->assign('id_status_balas', $id_status_balas);
	$smarty->assign('id_pengguna_status_balas', $id_pengguna_status_balas);
	$smarty->assign('status_balas', $status_balas);
	$smarty->assign('tgl_status_balas', $tgl_status_balas);
	$smarty->assign('nm_pengguna_status_balas', $nm_pengguna_status_balas);
	$smarty->assign('username_status_balas', $username_status_balas);
	$smarty->assign('jml_status_balas', $jml_status_balas);

	$smarty->assign('id_pengguna', $id_pengguna);
	$smarty->display('sn_comments.tpl');
?>