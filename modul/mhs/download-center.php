<?php

require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

include 'function-tampil-informasi.php';
include 'class/download.class.php';

$download = new download($db, $user->ID_PENGGUNA);
/*
  STATUS DOWNLOAD
  0=REQUEST
  1=APPROVE

  STATUS FILE
  0=REQUEST
  1=AVAILABLE
  2=NOT FOUND
 */

if (isset($_POST)) {
    if (post('mode') == 'add') {
        $download->AddRequestDownload(post('nama'), post('url'), post('jenis'));
    } else if (post('mode') == 'update') {
        $download->UpdateRequestDownload(post('id_download'), post('nama'), post('url'), post('jenis'));
    } else if (post('mode') == 'delete') {
        $download->DeleteRequestDownload(post('id_download'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'update' or get('mode') == 'delete') {
        $smarty->assign('download', $download->GetRequestDownload(get('id_download')));
    }
}

	$isi .= '<br><br>
	<iframe src="http://210.57.208.29/modul/mhs/proses/e_pembelajaran/e_pembelajaran.php" width="100%" height="600" frameborder=0>
	<p>Your browser does not support iframes.</p>
	</iframe> 
	';


$smarty->assign('data_request', $download->LoadRequestDownload());
$smarty->assign('jkueri_pembelajaran', $isi);
$smarty->display('download-center.tpl');
?>

