<?php
require 'config.php';

if ($user->Role() != AUCC_ROLE_MAHASISWA) {
    header("location: /logout.php");
    exit();
}

require 'class/eva.class.php';
require 'class/process.class.php';
require 'function-tampil-informasi-no-conf.php';

$eva = new eva($db, $user->ID_PENGGUNA);
$process = new process($db, $user->ID_PENGGUNA, $user->ID_PERGURUAN_TINGGI);

$check_message  = '';

$id_semester_aktif      = $eva->GetSemesterAktif();
$id_semester_isi_eval   = $eva->GetSemesterAktifEvaluasi();

$status_krs = '0';

$db->Query("SELECT to_char(tgl_mulai_jsf,'YYYYMMDD') mulai,to_char(tgl_selesai_jsf,'YYYYMMDD') selesai 
    from JADWAL_SEMESTER_FAKULTAS
    where ((id_kegiatan=56) or (id_kegiatan=41)) and 
        id_fakultas={$user->ID_FAKULTAS} and 
        id_semester={$id_semester_aktif}");

while ($r = $db->FetchRow()) {
    $tgl_mulai_jsf = $r[0];
    $tgl_selesai_jsf = $r[1];
    
    $tgl_sekarang = date("Ymd");

    if ($tgl_sekarang < $tgl_mulai_jsf) {
        $status_krs = '0';
    } elseif ($tgl_sekarang > $tgl_selesai_jsf) {
        $status_krs = '2';
    }
}

$r = $eva->Biodata($user->ID_PENGGUNA);
$id_mhs = $r[0];
$status_cekal = $r[6];
$mhs_ang = $r[10];
$angkatan = $r[10];
$status_krs_prodi = $r[11];
$jenjang = $r[12];

if (($jenjang == 2 or $jenjang == 3) and ($status_krs_prodi == 1)) {
    $status_krs = '1';
}

$_SESSION['STATUS_KRS'] = $status_krs;

// PEMERIKSAAN PEMBAYARAN KEUANGAN KHUSUS UNU LAMPUNG - NAMBI
if ($user->ID_PERGURUAN_TINGGI == PT_UNU_LAMPUNG) {
    $status_keuangan = $process->CekStatusKeuanganMhs($id_mhs, $id_semester_aktif);
    if (!$status_keuangan['status']) {
        $check_message .= alert_info($status_keuangan['message']);
        die('<div class="center_title_bar">Status Pengisian KRS</div>' . $check_message);
    }
}

if ($status_cekal == 1) {
    $check_message .= alert_error("Anda belum bisa KRS, karena status anda adalah cekal, hubungi bagian pendidikan rektorat") . '<br/>';
}

// Jika terdapat kekurangan tampilkan Pesan dan tidak dilanjutkan
if ($check_message != '') {
    die('<div class="center_title_bar">Status Pengisian KRS</div><br/>' . $check_message);
}

$status_krs = 1;

### DIRECT TO THIS SETTING ~ RIESKHA
// *****************
// *** INPUT KRS ***
// *****************
$jkueri_penawaranmk = '
        <script type="text/javascript">
            function tampildatasatu(){
                $.ajax({
                    type:"POST",
                    url:"proses/_akademik-krs_ditambah.php",
                    data: "aksi=tampil",
                    success: function(data){
                        $("#krs_ditambah").html(data);
                    }
                });
            }
            tampildatasatu();

            function krstambah_kirim(f, id_kur_mk){
                $.ajax({
                    type:"POST",
                    url:"proses/_akademik-krs_ditambah.php",
                    data: "aksi=input&kelas="+f+"&id_kur_mk="+id_kur_mk+"&sid=' . session_id() . '",
                    success: function(data){
                        alert(data);
                        tampildatasatu();
                        tampildatadua();
                        tampildatatiga();
                    }
                });
            }
        </script>
        ';
$smarty->assign('jkueri_penawaranmk', $jkueri_penawaranmk);

// ****************************
// *** HAPUS KRS / TERAMBIL ***
// ****************************
$jkueri_mkterambil = '
        <script type="text/javascript">
            var htmlLoading="<center style=\'padding:20px\'>Waiting for data...</center>";
            function tampildatadua(){
                $.ajax({
                    type:"POST",
                    url:"proses/_akademik-krs_hapus.php",
                    data: "aksi=tampil",
                    beforeSend: function() {          
                        $("#krs_dihapus").html(
                            htmlLoading
                        );
                    },
                    success: function(data){
                        $("#krs_dihapus").html(data);
                    }
                });
            }
            tampildatadua();

            function krshapus_kirim(f){
                $.ajax({
                    type:"POST",
                    url:"proses/_akademik-krs_hapus.php",
                    data: "aksi=hapus&pengambilan_mk="+f,
                    success: function(data){
                        alert(data);
                        tampildatasatu();
                        tampildatadua();
                        tampildatatiga();
                    }
                });
            }
        </script>
        ';
$smarty->assign('jkueri_mkterambil', $jkueri_mkterambil);

// ********************
// *** Respon dosen ***
// ********************
$jkueri_respondosen = '
        <script type="text/javascript">
            function tampildatatiga(){
                $.ajax({
                    type:"POST",
                    url:"proses/_akademik-krs_dilihat.php",
                    data: "aksi=tampil",
                    success: function(data){
                        $("#krs_ditampil").html(data);
                    }
                });
            }
            tampildatatiga();
        </script>
        ';
$smarty->assign('jkueri_respondosen', $jkueri_respondosen);

$smarty->assign('session_id', session_id());
$smarty->display('akademik-krs.tpl');
