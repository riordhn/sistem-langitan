<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

$db = new MyOracle();
$db2 = new MyOracle();
$db3 = new MyOracle();
$db4 = new MyOracle();

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
// ambil id_mhs
	$id_mhs="";
	$kueri = "select id_mhs from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : id_mhs");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

// Yudi Sulistya, 14-10-2012

	// ambil semester_aktif
	$sem_aktif="";
	$kueri1 = "select id_semester from semester where status_aktif_semester='True'";
	$result1 = $db->Query($kueri1)or die ("salah kueri : sem_aktif");
	while($r1 = $db->FetchRow()) {
		$sem_aktif = $r1[0];
	}
	
	// Jadwal UTS
	$jad_uts = "
	select umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai,umk.jam_selesai,mk.kd_mata_kuliah,upper(mk.nm_mata_kuliah) as nm_mata_kuliah,rg.nm_ruangan
	from ujian_mk_peserta pst
	left join ujian_mk umk on umk.id_ujian_mk=pst.id_ujian_mk
	left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
	left join ruangan rg on jad.id_ruangan=rg.id_ruangan
	left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
	left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
	left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
	where umk.id_kegiatan=44 and pst.id_mhs=$id_mhs and umk.id_semester=$sem_aktif
	";
	$result = $db2->Query($jad_uts)or die("salah kueri : jad_uts");
	while($r = $db2->FetchRow()) {
		$uts .= '
		<tr>
			<td>'.$r[4].'</td>
			<td>'.$r[5].'</td>
			<td>'.$r[0].'</td>
			<td align="center">'.$r[1].'</td>
			<td align="center">'.$r[2].' - '.$r[3].'</td>
			<td align="center">'.$r[6].'</td>
		</tr>
		';
	}

	$smarty->assign('UTS', $uts);

	// Jadwal UAS
	$jad_uas = "
	select umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai,umk.jam_selesai,mk.kd_mata_kuliah,upper(mk.nm_mata_kuliah) as nm_mata_kuliah,rg.nm_ruangan
	from ujian_mk_peserta pst
	left join ujian_mk umk on umk.id_ujian_mk=pst.id_ujian_mk
	left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
	left join ruangan rg on jad.id_ruangan=rg.id_ruangan
	left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
	left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
	left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
	where umk.id_kegiatan=47 and pst.id_mhs=$id_mhs and umk.id_semester=$sem_aktif
	";
	$result = $db3->Query($jad_uas)or die("salah kueri : jad_uas");
	while($r = $db3->FetchRow()) {
		$uas .= '
		<tr>
			<td>'.$r[4].'</td>
			<td>'.$r[5].'</td>
			<td>'.$r[0].'</td>
			<td align="center">'.$r[1].'</td>
			<td align="center">'.$r[2].' - '.$r[3].'</td>
			<td align="center">'.$r[6].'</td>
		</tr>
		';
	}
	
	$smarty->assign('UAS', $uas);
	
	// Yudi Sulistya, 13-01-2014
	// Jadwal TA/SKRIPSI/THESIS/DESERTASI
	$jad_ta = "
	select a.nm_ujian_mk,b.judul,a.tgl_ujian,a.jam_mulai,a.jam_selesai,a.nm_ruangan,a.tim from
	(select pst.id_mhs,kmk.id_kelas_mk,umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,
	wm_concat('<li>'||trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang)) as tim
	from ujian_mk umk
	left join kelas_mk kmk on kmk.id_kelas_mk = umk.id_kelas_mk
	left join jadwal_ujian_mk jad on jad.id_ujian_mk = umk.id_ujian_mk
	left join ruangan rg on rg.id_ruangan = jad.id_ruangan
	left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk = tim.id_jadwal_ujian_mk
	left join ujian_mk_peserta pst on umk.id_ujian_mk = pst.id_ujian_mk
	left join pengguna pgg on pgg.id_pengguna = tim.id_pengguna
	where umk.id_kegiatan = 71 and umk.id_semester = $sem_aktif and pst.id_mhs = $id_mhs
	group by pst.id_mhs,kmk.id_kelas_mk,umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan) a
	join
	(select a.id_mhs, upper(a.judul_tugas_akhir) as judul, e.id_kelas_mk
	from tugas_akhir a 
	left join tipe_ta b on a.id_tipe_ta = b.id_tipe_ta
	left join mahasiswa c on a.id_mhs = c.id_mhs
	left join program_studi g on c.id_program_studi = g.id_program_studi
	left join kelas_mk e on g.id_program_studi = e.id_program_studi
	left join kurikulum_mk f on e.id_kurikulum_mk = f.id_kurikulum_mk
	where a.status = 1 and f.status_mkta = 1 and a.id_mhs = $id_mhs
	and e.id_semester = $sem_aktif) b
	on a.id_mhs=b.id_mhs and a.id_kelas_mk=b.id_kelas_mk
	";
	$result = $db4->Query($jad_ta)or die("salah kueri : jad_ta");
	while($r = $db4->FetchRow()) {
		$ta .= '
		<tr>
			<td>'.$r[0].'</td>
			<td>'.$r[1].'</td>
			<td align="center">'.$r[2].'</td>
			<td align="center">'.$r[3].' - '.$r[4].'</td>
			<td align="center">'.$r[5].'</td>
			<td><ul>'.$r[6].'</li></ul></td>
		</tr>
		';
	}
	
	$smarty->assign('TA', $ta);
	
	if ($user->ID_FAKULTAS == 11) // Psikologi
	{
	// Jadwal All
	$jad_all = "
	select distinct umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai,umk.jam_selesai,mk.kd_mata_kuliah,upper(mk.nm_mata_kuliah) as nm_mata_kuliah,rg.nm_ruangan,
	case when id_kegiatan=44 then 'UTS' else 'UAS' end as jenis
	from ujian_mk_peserta pst
	left join ujian_mk umk on umk.id_ujian_mk=pst.id_ujian_mk
	left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
	left join ruangan rg on jad.id_ruangan=rg.id_ruangan
	left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
	left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
	left join mata_kuliah mk on kur.id_mata_kuliah=mk.id_mata_kuliah
	where umk.id_semester=$sem_aktif and umk.id_fakultas=11 and (umk.id_kegiatan=44 or umk.id_kegiatan=47) 
	";
	$result2 = $db2->Query($jad_all)or die("salah kueri : jad_all");
	while($r2 = $db2->FetchRow()) {
		$all .= '
		<tr>
			<td>'.$r2[4].'</td>
			<td>'.$r2[5].'</td>
			<td>'.$r2[0].'</td>
			<td align="center">'.$r2[1].'</td>
			<td align="center">'.$r2[2].' - '.$r2[3].'</td>
			<td align="center">'.$r2[6].'</td>
			<td align="center">'.$r2[7].'</td>
		</tr>
		';
	}
	
	$smarty->assign('ALL', $all);
	}
	
	if ($user->ID_FAKULTAS == 11) // Psikologi
	{
	$smarty->display('akademik-ujian-psi.tpl');
	} else {
	$smarty->display('akademik-ujian.tpl');
	}

}else {
	$smarty->display('session-expired.tpl');
}
?>
