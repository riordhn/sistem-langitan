<?php
require 'config.php';

require_once '../keuangan/class/utility.class.php';
require_once '../keuangan/class/history.class.php';
require_once '../keuangan/class/list_data.class.php';

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$history = new history($db);
$list = new list_data($db);
$utility = new utility($db);

$biodata = $history->get_data_mhs(get('cari'), $id_pt_user);
$transaksi = $history->load_pembayaran_by_no_kuitansi(get('no_kuitansi'));
$data_transaksi = $history->load_pembayaran_all_biaya_by_no_kuitansi(get('no_kuitansi'));

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('Kwitansi Keuangan');
$pdf->SetSubject('Kwitansi Keuangan');
$var = [];

$tglcetak = $history->tanggalInd(date('Y-m-d'));
$kode_transaksi = $_GET['no_transaksi'];
$no_kuitansi = $_GET['no_kuitansi'];
$nama = $biodata['NM_PENGGUNA'];
$nim = $biodata['NIM_MHS'];
$fakultas = $biodata['NM_FAKULTAS'];
$prodi = $biodata['NM_JENJANG'] . ' ' . $biodata['NM_PROGRAM_STUDI'];
$status = $biodata['NM_STATUS_PENGGUNA'];
$jalur = $biodata['NM_JALUR'];
$tgl_bayar = $history->tanggalInd(date("Y-m-d", strtotime($transaksi[0]['TGL_BAYAR'])));
$semester = $transaksi[0]['SEMESTER'];
$total_bayar = 0;

if ($is_tunai == 1) {
  $is_tunai = "Tunai";
} else {
  $is_tunai = "Transfer Tgl. " . $tgl_transfer;
}

$id_pt = $id_pt_user;

$nm_pengguna = $user->NM_PENGGUNA;


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(8, 5, 8);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 9);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 8);

// add a page
$pdf->AddPage('P', 'A5');
/*$pdf->AddPage('L', 'F4');*/

// draw jpeg image
//$pdf->Image('../includes/logo_unair.png', 75, 25, 150, '', '', '', '', false, 72);
//$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
// set the starting point for the page content
$pdf->setPageMark();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
	  <td width="15%" align="left" valign="middle"><img src="../../img/akademik_images/logo-' . $nama_singkat . '.gif" width="30" border="0"></td>
    <td width="85%" align="left" valign="middle"><font size="14"><b>' . strtoupper($nama_pt) . '</b></font><br><font size="8">' . strtoupper($alamat) . ' Telp. ' . $telp_pt . '</font></td>
  </tr>
  <tr>
    <td colspan="2" align="center"  style="border-top:1px solid black;border-bottom:1px solid black;"><br/><font size="10"><b>KWITANSI PEMBAYARAN</b></font><br><font><b>NOMOR KUITANSI : ' . $no_kuitansi . '<br/>SEMESTER : ' . $semester . '</b></font></td>
  </tr>
</table>
<br/>
  ';
  $html .= '
  <br/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="15%"><font>Nama</font></td>
	<td width="60%"><font> : ' . strtoupper($nama) . '</font></td>
  </tr>
  <tr>
    <td width="15%"><font>NIM</font></td>
    <td width="45%"><font> : ' . strtoupper($nim) . '</font></td>
    <td width="15%"><font>Jalur</font></td>
    <td width="25%"><font> : ' . $jalur . '</font></td>
  
  </tr>
  <tr>
    <td width="15%"><font>Prodi</font></td>
    <td width="45%"><font> : ' . $prodi . '</font></td>
    <td width="15%"><font>Fakultas</font></td>
    <td width="25%"><font> : ' . ucwords(strtolower($fakultas)) . '</font></td>
  </tr>
  <tr>
    <td width="15%"><font>Tgl.Bayar</font></td>
    <td width="45%"><font> : ' . $tgl_bayar . '</font></td>    
    <td width="15%"><font>Status</font></td>
    <td width="25%"><font> : ' . ucwords(strtolower($status)) . '</font></td>
  </tr>
  <tr>
  	<td colspan="2"><br>&nbsp;</td>
  </tr>
</table>

<table width="100%" border="1" cellpadding="2">

	<tr>
		 <th align="center" width="5%">No</th>
		 <th  align="center" width="70%">Nama Biaya</th>
		 <th  colspan="2" align="center" width="27%">Jumlah</th> 			 
    </tr>
    ';
$no = 1;
foreach ($data_transaksi as $d) {
  if ($d['BESAR_PEMBAYARAN'] > 0) {
    $html .= ' <tr>
    <th align="center" width="5%">' . $no++ . '</th>
    <th align="left" width="70%">' . $d['NM_BIAYA'] . '</th>
    <th align="left" width="6%">Rp</th>
    <th align="right" width="21%">' . number_format($d['BESAR_PEMBAYARAN']) . '</th>			 			 
   </tr>';
    $total_bayar += !empty($d['BESAR_PEMBAYARAN']) ? $d['BESAR_PEMBAYARAN'] : 0;
  }
}
$terbilang = $history->getTerbilang($total_bayar);
$html .= '
    <tr>
		 <th align="left" colspan="2"><b>Total dibayar</b></th>
		 <th align="left" width="6%">Rp</th>
		 <th align="right" width="21%"><b>' . number_format($total_bayar) . '</b></th>	 			 
    </tr>

    <tr>
		 <th align="left" colspan="4"><b><i>Terbilang :  &nbsp; ' . $terbilang . ' rupiah</i></b></th>	 			 
    </tr>

</table>
';

$html .= '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2"><br>&nbsp;</td>
  </tr>  
  <tr>
    <td colspan="2"><font><b>Catatan</b></font></td>
  </tr>
  <tr>
    <td colspan="3"><font>*) Dokumen ini hanya bisa sebagai digunakan untuk Bukti Pembayaran</font></td>
  </tr>
  <tr>
    <td colspan="2"><font>*) Untuk kepentingan akademik silahkan  cetak kuitansi resmi dari Bag.Keuangan</font></td>	
  </tr>
</table>
<br><br>
<b><i>Waktu cetak :' . date('d/m/y H:i:s') . '</i><b>
';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('Kwitansi Keuangan ' . strtoupper($kode_transaksi) . '.pdf', 'I');
