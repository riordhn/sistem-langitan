<?php
require_once('config.php');


$id_pt = $id_pt_user;


// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {

	$kueri = "select id_semester, thn_akademik_semester, nm_semester from semester where NM_SEMESTER in ('Ganjil','Genap') and id_perguruan_tinggi = '{$id_pt}' order by THN_AKADEMIK_SEMESTER desc, NM_SEMESTER desc";
	$result = $db->Query($kueri)or die("salah kueri : ");
	while($r = $db->FetchRow()) {
		$semester[$r[0]] = $r[1]." ".$r[2];
	}

	$smarty->assign('semes_isi', $semester );

	$jkueri_lihat = '
	<script type="text/javascript">
		function km_khp (f){
			$.ajax({
				type:"POST",
				url:"proses/_kemahasiswaan-khp_lihat.php",
				data: $("#"+f).serialize(),
				success: function(data){
					$("#km-khp-tampil").html(data);
				}
			});
		}
	</script>
	';
	$smarty->assign('jkueri_lihat', $jkueri_lihat);

	$smarty->display('kemahasiswaan-khp.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>
