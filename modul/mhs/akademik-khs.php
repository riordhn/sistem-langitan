<?php
require 'config.php';

$akd = new Akademik($db); // referensi ke /includes/Akademik.class.php

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) 
{
	$smarty->assign('semester_set', $akd->get_semester_mhs($user->ID_PENGGUNA));
	$smarty->display('akademik-khs.tpl');
}
else 
{
	$smarty->display('session-expired.tpl');
}