<?php
define('NOFILTER',1);
require_once('config.php');
include 'class/eva.class.php';

$eva = new eva($db, $user->ID_PENGGUNA);

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA) {
    header("location: /logout.php");
    exit();
}

// Set sementara oleh Fathoni : (29/12/2015)
// Ada pesan salah kueri 6
//echo '<div class="center_title_bar">Pengajuan Wisuda</div><p>Fitur sedang tidak aktif.</p>'; exit();



include 'class/upload.class.php';

$upload = new upload($db, $user->ID_PENGGUNA);

//require('config-mysql.php');
// ambil nim & password
$mhs_pass = "";
$mhs_nim = "";
$id_prodi = "";
$kueri = "select b.nim_mhs, a.password_hash, a.se1, b.id_mhs, b.id_program_studi from pengguna a, mahasiswa b where a.id_pengguna=b.id_pengguna and a.id_pengguna='" . $user->ID_PENGGUNA . "'";
$result = $db->Query($kueri) or die("salah kueri 6 : ");
while ($r = $db->FetchRow()) {
    $mhs_pass = $r[2];
    $mhs_nim = $r[0];
    $id_mhs = $r[3];
    $id_prodi = $r[4];
}
// ambil jenjang
$id_jenjang = "";
$kueri = "select id_jenjang from program_studi where id_program_studi='" . $id_prodi . "'";
$result = $db->Query($kueri) or die("salah kueri 6 : ");
while ($r = $db->FetchRow()) {
    $id_jenjang = $r[0];
}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {

    $error = "";

    $kueri = "SELECT COUNT(*) FROM PENGAJUAN_WISUDA WHERE ID_MHS = '$id_mhs'";
    $result = $db->Query($kueri) or die("salah kueri 2 : ");
    $r = $db->FetchRow();
    if ($r[0] == 1) {


        // biodata
        if ($id_jenjang == '2' or $id_jenjang == '3' or $id_jenjang == '10' or $id_jenjang == '9') {
            $kueri = "SELECT COUNT(*) AS MHS 
					FROM MAHASISWA 
					JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
					WHERE NIM_MHS = '$mhs_nim' AND PENGGUNA.ID_PERGURUAN_TINGGI = '{$id_pt_user}'
					AND MOBILE_MHS IS NOT NULL AND NM_AYAH_MHS IS NOT NULL
					AND PENDIDIKAN_AYAH_MHS IS NOT NULL AND PEKERJAAN_AYAH_MHS IS NOT NULL AND ALAMAT_AYAH_MHS IS NOT NULL
					AND NM_IBU_MHS IS NOT NULL AND PENDIDIKAN_IBU_MHS IS NOT NULL AND PEKERJAAN_IBU_MHS IS NOT NULL
					AND ALAMAT_IBU_MHS IS NOT NULL AND PENGHASILAN_AYAH_MHS IS NOT NULL AND PENGHASILAN_IBU_MHS IS NOT NULL
					AND LAHIR_KOTA_MHS IS NOT NULL AND LAHIR_PROP_MHS IS NOT NULL AND ASAL_KOTA_MHS IS NOT NULL
					AND ASAL_PROV_MHS IS NOT NULL 
					AND ALAMAT_AYAH_MHS_KOTA IS NOT NULL AND ALAMAT_AYAH_MHS_PROV IS NOT NULL 
					AND ALAMAT_IBU_MHS_KOTA IS NOT NULL AND ALAMAT_IBU_MHS_PROV IS NOT NULL AND ALAMAT_ASAL_MHS IS NOT NULL
					AND ALAMAT_ASAL_MHS_KOTA IS NOT NULL AND ALAMAT_ASAL_MHS_PROV IS NOT NULL
					AND KELAMIN_PENGGUNA IS NOT NULL AND ID_AGAMA IS NOT NULL AND TGL_LAHIR_PENGGUNA IS NOT NULL";
        } else {
            $kueri = "SELECT COUNT(*) AS MHS FROM MAHASISWA 
					JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
					WHERE NIM_MHS = '$mhs_nim' AND PENGGUNA.ID_PERGURUAN_TINGGI = '{$id_pt_user}'
					AND MOBILE_MHS IS NOT NULL AND ID_SEKOLAH_ASAL_MHS IS NOT NULL AND THN_LULUS_MHS IS NOT NULL
					AND NILAI_SKHUN_MHS IS NOT NULL AND CITA_CITA_MHS IS NOT NULL AND NM_AYAH_MHS IS NOT NULL
					AND PENDIDIKAN_AYAH_MHS IS NOT NULL AND PEKERJAAN_AYAH_MHS IS NOT NULL AND ALAMAT_AYAH_MHS IS NOT NULL
					AND NM_IBU_MHS IS NOT NULL AND PENDIDIKAN_IBU_MHS IS NOT NULL AND PEKERJAAN_IBU_MHS IS NOT NULL
					AND ALAMAT_IBU_MHS IS NOT NULL AND PENGHASILAN_AYAH_MHS IS NOT NULL AND PENGHASILAN_IBU_MHS IS NOT NULL 
          AND NEM_PELAJARAN_MHS IS NOT NULL
					AND LAHIR_KOTA_MHS IS NOT NULL AND LAHIR_PROP_MHS IS NOT NULL AND ASAL_KOTA_MHS IS NOT NULL
					AND ASAL_PROV_MHS IS NOT NULL 
					AND ALAMAT_AYAH_MHS_KOTA IS NOT NULL AND ALAMAT_AYAH_MHS_PROV IS NOT NULL 
					AND ALAMAT_IBU_MHS_KOTA IS NOT NULL AND ALAMAT_IBU_MHS_PROV IS NOT NULL AND ALAMAT_ASAL_MHS IS NOT NULL
					AND ALAMAT_ASAL_MHS_KOTA IS NOT NULL AND ALAMAT_ASAL_MHS_PROV IS NOT NULL
					AND KELAMIN_PENGGUNA IS NOT NULL AND ID_AGAMA IS NOT NULL AND TGL_LAHIR_PENGGUNA IS NOT NULL";
        }

        $result = $db->Query($kueri) or die("salah kueri 7 : ");
        $r = $db->FetchRow();

        if ($r[0] <= 0) {
            $error = $error . '<b>-  Menu biodata harap dilengkapi</b></br>';
        }
		
  		$db->Query("SELECT PERIODE_WISUDA.BESAR_BIAYA, ID_PERIODE_WISUDA, ID_TARIF_WISUDA, TO_CHAR(TGL_BAYAR_SELESAI, 'DD-MM-YYYY') AS TGL_BAYAR_SELESAI
  									FROM PERIODE_WISUDA
  									LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
  									LEFT JOIN MAHASISWA ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
  									WHERE MAHASISWA.ID_MHS = '$id_mhs' AND STATUS_AKTIF = 1");
          $tarif = $db->FetchArray();				 
  		 
  		 $db->Query("SELECT ID_PERIODE_WISUDA
  									FROM PENGAJUAN_WISUDA WHERE ID_MHS = '$id_mhs'");
  		 $wisuda_mhs = $db->FetchArray();
  		 		 	
  			
  			if($wisuda_mhs['ID_PERIODE_WISUDA'] != $tarif['ID_PERIODE_WISUDA'] or strtotime($tarif['TGL_BAYAR_SELESAI']) < strtotime("now")){
  		 		$error = $error . '<b>- Bukan periode wisuda aktif, harap hubungi Akademik Fakultas untuk pindah periode wisuda</b></br>';
  		 	}		 		
								
    } else {
        $error = $error . '<b>-  Anda Belum Yudisium, Harap Hubungi Akademik Fakultas</b></br>';
    }


    if ($error == "") {
        $kueri = "SELECT count(*) as jml FROM PENGAJUAN_WISUDA WHERE ID_MHS = '$id_mhs'";
        $result = $db->Query($kueri) or die("salah kueri 213 : ");
        $w = $db->FetchRow();

        $pesan = "";
        /*if ($_POST['judul'] <> '' and $_POST['abstrak'] <> '' and $_POST['judul_en'] <> '' 
          and $_POST['abstrak_en'] <> '') 
        {*/
        if ($_POST['judul'] <> '') 
        {
            if ($w[0] > 0) {

                //ABSTRAK DAN JUDUL
                $JUDUL_TA = str_replace("'", "''", $_POST['judul']);
                $ABSTRAK_TA = str_replace("'", "''", $_POST['abstrak']);
                $ABSTRAK_TA_EN = str_replace("'", "''", $_POST['abstrak_en']);
        				$JUDUL_TA_EN = str_replace("'", "''", $_POST['judul_en']);
                $BULAN_AWAL_BIMBINGAN = $_POST['bulan_awal_bimbingan'];
                $BULAN_AKHIR_BIMBINGAN = $_POST['bulan_akhir_bimbingan'];

        				/*$db->Query("SELECT JUDUL_TA, JUDUL_TA_EN FROM PENGAJUAN_WISUDA
        									WHERE ID_MHS = '$id_mhs'");
                        $cek_judul = $db->FetchArray();

        					$set = " ";
        				if($cek_judul['JUDUL_TA'] == ''){
                        	$set .= " , JUDUL_TA = '$JUDUL_TA'";
        				}
        				
        				if($cek_judul['JUDUL_TA_EN'] == ''){
                        	$set .= " , JUDUL_TA_EN = '$JUDUL_TA_EN'";
        				}		*/		
			         

              $db->Query("SELECT PERIODE_WISUDA.BESAR_BIAYA, ID_PERIODE_WISUDA, PERIODE_WISUDA.ID_TARIF_WISUDA, TO_CHAR(TGL_BAYAR_SELESAI, 'DD-MM-YYYY') AS TGL_BAYAR_SELESAI, NM_TARIF_WISUDA
                    FROM PERIODE_WISUDA
                    JOIN TARIF_WISUDA ON TARIF_WISUDA.ID_TARIF_WISUDA = PERIODE_WISUDA.ID_TARIF_WISUDA
                    LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
                    LEFT JOIN MAHASISWA ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
                    WHERE MAHASISWA.ID_MHS = '$id_mhs' AND STATUS_AKTIF = 1");
              $tarif = $db->FetchArray(); 

      				$nm_periode_w = $tarif['NM_TARIF_WISUDA'];
      				
      				$db->Query("UPDATE PENGAJUAN_WISUDA SET JUDUL_TA='{$JUDUL_TA}', JUDUL_TA_EN='{$JUDUL_TA_EN}', ABSTRAK_TA_EN = '$ABSTRAK_TA_EN', ABSTRAK_TA_CLOB = '$ABSTRAK_TA', 
                  BULAN_AWAL_BIMBINGAN = to_date('$BULAN_AWAL_BIMBINGAN', 'DD-MM-YYYY'), 
                  BULAN_AKHIR_BIMBINGAN = to_date('$BULAN_AKHIR_BIMBINGAN', 'DD-MM-YYYY')
      							WHERE ID_MHS = '$id_mhs'");	
              $pesan = '<script>alert("SELAMAT...\n- Anda sudah bisa mengikuti '.$nm_periode_w.'\n")</script>';
			
			     }
           
      /*} elseif ($_POST['judul'] <> '' and ($_POST['abstrak'] == ''  or $_POST['abstrak_en'] == '')) {
          $pesan = "<br>Abstrak Harus di isi";
      } elseif ($_POST['judul'] == '' and $_POST['abstrak'] <> '') {
          $pesan = "<br>Judul Harus di isi";
      }*/
      } 
      elseif ($_POST['judul'] == '') {
          $pesan = "<br>Judul Harus di isi";
      }


        $kueri = "SELECT JUDUL_TA, ABSTRAK_TA_CLOB, ABSTRAK_TA_EN, JUDUL_TA_EN, ID_PENGAJUAN_WISUDA, BULAN_AWAL_BIMBINGAN, BULAN_AKHIR_BIMBINGAN FROM PENGAJUAN_WISUDA WHERE ID_MHS = '$id_mhs'";
        $result = $db->Query($kueri) or die("salah kueri 288 : ");
        $w = $db->FetchRow();
        if ($w[4] <> '') {
           $judul = $w[0];
           $abstrak = $w[1];
           $abstrak_en = $w[2];
			     $judul_en = $w[3];
           
           if(!empty($w[5])){
            $bulan_awal_bimbingan = date_format(date_create($w[5]), 'd-m-Y');
           }
           else{
            $bulan_awal_bimbingan = '';
           }

           if(!empty($w[6])){
            $bulan_akhir_bimbingan = date_format(date_create($w[6]), 'd-m-Y');
           }
           else{
            $bulan_akhir_bimbingan = '';
           }

        }

        $link = 'proses/_akademik-wisuda_cetak.php';

        if(!empty($judul) and !empty($bulan_awal_bimbingan) and !empty($bulan_akhir_bimbingan)){

          $tampil_data = $pesan . '
  	<br>
  	<br />
    <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
          <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
              <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                      Tanggal Bulan Bimbingan dan Judul Skripsi/TA Harus Diisi.</p>
          </div>
    </div>
  		<b><i>* Isi Abstrak tidak boleh lebih dari 3800 karakter</i></b>
  	<form name="lengkap" id="lengkap" action="akademik-wisuda.php" method="post">
  	<table width="100%">
  	<tr>
  		<td>Judul Skripsi/TA</td>
  		<td> : </td>
  		<td><textarea cols="50" rows="3" name="judul" id="judul">' . $judul . '</textarea></td>
  	</tr>
  	<tr>
  		<td>Abstrak Skripsi/TA</td>
  		<td> : </td>
  		<td><textarea cols="100" rows="20" name="abstrak" onKeyDown="limitText(this.form.abstrak,this.form.countdown,3800);" 
  onKeyUp="limitText(this.form.abstrak,this.form.countdown,3800);">' . $abstrak . '</textarea>
  		<br>
  		<br>
  		<font size="1">(Maksimal karakter: 3800)<br>
  			Sisa Karakter <input readonly type="text" name="countdown" size="4" value="3800"></font>
  		</form>
  		<br>
  		<br>
  		</td>
  	</tr>
  	<tr>
  		<td>Judul Skripsi/TA English</td>
  		<td> : </td>
  		<td><textarea cols="50" rows="3" name="judul_en">' . $judul_en . '</textarea></td>
  	</tr>
  	<tr>
  		<td>Abstrak Skripsi/TA English</td>
  		<td> : </td>
  		<td><textarea cols="100" rows="20" name="abstrak_en" onKeyDown="limitText(this.form.abstrak_en,this.form.countdown_en,3800);" 
  onKeyUp="limitText(this.form.abstrak_en,this.form.countdown_en,3800);">' . $abstrak_en . '</textarea>
  		<br>
  		<br>
  		<font size="1">(Maksimal karakter: 3800)<br>
  			Sisa Karakter <input readonly type="text" name="countdown_en" size="4" value="3800"></font>
  		</form>
  		</td>
  	</tr>
    <tr>
        <td>Bulan Awal Bimbingan</td>
        <td> : </td>
        <td>
            <input type="text" id="bulan_awal_bimbingan" class="datepicker"  name="bulan_awal_bimbingan" value="' . $bulan_awal_bimbingan . '" />        
        </td>
    </tr>
    <tr>
        <td>Bulan Akhir Bimbingan</td>
        <td> : </td>
        <td>
            <input type="text" id="bulan_akhir_bimbingan" class="datepicker"  name="bulan_akhir_bimbingan" value="' . $bulan_akhir_bimbingan . '" />       
        </td>
    </tr>
  	<tr>
  		<td colspan="2"></td>
  		<td class="center">
          <input type="submit" value="Simpan">
          <input type=button value="Cetak Biodata" onclick="window.open(\'' . $link . '\');">
      </td>
  	</tr>
  	</table>
  	</form>
  	';
      }
      else{
          $tampil_data = $pesan . '
      <br>
      <br />
      <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
            <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                        Tanggal Bulan Bimbingan dan Judul Skripsi/TA Harus Diisi.</p>
            </div>
      </div>
        <b><i>* Isi Abstrak tidak boleh lebih dari 3800 karakter</i></b>
      <form name="lengkap" id="lengkap" action="akademik-wisuda.php" method="post">
      <table width="100%">
      <tr>
        <td>Judul Skripsi/TA</td>
        <td> : </td>
        <td><textarea cols="50" rows="3" name="judul" id="judul">' . $judul . '</textarea></td>
      </tr>
      <tr>
        <td>Abstrak Skripsi/TA</td>
        <td> : </td>
        <td><textarea cols="100" rows="20" name="abstrak" onKeyDown="limitText(this.form.abstrak,this.form.countdown,3800);" 
    onKeyUp="limitText(this.form.abstrak,this.form.countdown,3800);">' . $abstrak . '</textarea>
        <br>
        <br>
        <font size="1">(Maksimal karakter: 3800)<br>
          Sisa Karakter <input readonly type="text" name="countdown" size="4" value="3800"></font>
        </form>
        <br>
        <br>
        </td>
      </tr>
      <tr>
        <td>Judul Skripsi/TA English</td>
        <td> : </td>
        <td><textarea cols="50" rows="3" name="judul_en">' . $judul_en . '</textarea></td>
      </tr>
      <tr>
        <td>Abstrak Skripsi/TA English</td>
        <td> : </td>
        <td><textarea cols="100" rows="20" name="abstrak_en" onKeyDown="limitText(this.form.abstrak_en,this.form.countdown_en,3800);" 
    onKeyUp="limitText(this.form.abstrak_en,this.form.countdown_en,3800);">' . $abstrak_en . '</textarea>
        <br>
        <br>
        <font size="1">(Maksimal karakter: 3800)<br>
          Sisa Karakter <input readonly type="text" name="countdown_en" size="4" value="3800"></font>
        </form>
        </td>
      </tr>
      <tr>
          <td>Bulan Awal Bimbingan</td>
          <td> : </td>
          <td>
              <input type="text" id="bulan_awal_bimbingan" class="datepicker"  name="bulan_awal_bimbingan" value="' . $bulan_awal_bimbingan . '" />        
          </td>
      </tr>
      <tr>
          <td>Bulan Akhir Bimbingan</td>
          <td> : </td>
          <td>
              <input type="text" id="bulan_akhir_bimbingan" class="datepicker"  name="bulan_akhir_bimbingan" value="' . $bulan_akhir_bimbingan . '" />       
          </td>
      </tr>
      <tr>
        <td colspan="2"></td>
        <td class="center">
            <input type="submit" value="Simpan">
        </td>
      </tr>
      </table>
      </form>
      ';
      }
    
    } 
    else {
        $tampil_data = "</br>" . $error;
    }

    $smarty->assign('tampil_data', $tampil_data);

    $smarty->display('akademik-wisuda.tpl');
} else {
    $smarty->display('session-expired.tpl');
}
?>
