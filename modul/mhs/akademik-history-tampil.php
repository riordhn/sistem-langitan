<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        var myTable = $("#myTable");

        /* Detektor biar tidak error */
        if (myTable.length > 0) {
            $("#myTable").tablesorter({
                sortList: [[2, 0]], widgets: ["zebra"]
            });
        }

    });
</script>

<?php

require '../../../config.php';

if ($_POST["aksi"] == "tampil" and $_POST["semes"])
{
	$id_mhs		= $user->MAHASISWA->ID_MHS;
	$prodi		= $user->MAHASISWA->ID_PROGRAM_STUDI;
	$nim_mhs	= $user->MAHASISWA->NIM;
	$fakul		= $user->MAHASISWA->ID_FAKULTAS;

	// id semester dari post
	$idsemes	= sprintf("%d", $_POST["semes"]);
	
	// Ambil row Semester
	$sql = "SELECT thn_akademik_semester,nm_semester,tipe_semester from semester where id_semester='" . $idsemes . "' ";
	$sem = "";
	$tahun = "";
	$result = $db->Query($sql) or die("salah kueri : ".__LINE__);


	// status utk menampilkan detail nilai
		//	khusus umaha, dan yg >= 2015 Genap
	/* FIKRIE */
	$status_sem = 0;

	// status utk menampilkan KHS hanya yg sudah di approve keuangan
		//	khusus umaha, dan yg >= 2016 Genap
	/* FIKRIE */
	$status_sem_khs = 0;
	
	while ($r = $db->FetchRow())
	{
		$tahun = $r[0] . "/" . ($r[0] + 1);
		$sem = $r[1];
		$tmp_thn_akad = $r[0];
		$tmp_sem = $r[1];
		$sem_sp = $r[2];

		// mencari status semester utk menampilkan detai nilai (khusus umaha)
		if($id_pt_user == 1){
			if($r[0] == 2015){
				if($r[1] == 'Genap'){
					$status_sem = 1;
				}
			}
			else if ($r[0] > 2015){
				$status_sem = 1;
			}
		}

		// mencari status utk khs di sisi keuangan (khusus umaha non-fikes)
		if($id_pt_user == 1 and $fakul != 1){
			if($r[0] == 2016){
				if($r[1] == 'Genap'){
					$status_sem_khs = 1;
				}
			}
			else if ($r[0] > 2016){
				$status_sem_khs = 1;
			}
		}
	}

	if($status_sem_khs == 1){
		$db->Query("SELECT IS_BOLEH_CETAK 
									FROM KEUANGAN_KHS 
									WHERE ID_SEMESTER='{$idsemes}' 
									AND ID_MHS = '{$id_mhs}'");
		$is_boleh_cetak = $db->FetchAssoc();

		if($is_boleh_cetak['IS_BOLEH_CETAK'] == 1){
			$status_sem_khs = 0;
		}
	}

	if($status_sem_khs == 0){
		$semester = $sem . " " . $tahun . " ";
		
		$judulfk = "Hasil Studi Semester " . $semester;

		$depan = time();
		$belakang = strrev(time());

		$link = 'proses/_akademik-khs_cetak.php?nim=' . $nim_mhs . '&id=' . $idsemes;

		// ini skrip aslinya sementara ditutup cetak KHS Ganjil 2017 ada perbaikan
		/*$isi = '
		<h2>' . $judulfk . '</h2>
		
		<a style="padding:5px" class="ui-button ui-state-default ui-corner-all disable-ajax" onclick="window.open(\'' . $link . '\');"><b>Cetak</b></a>
		<br />
		<table width="850" id="myTable" class="tablesorter">
		<thead>
		<tr>
			<th>Mata Kuliah</th>
			<th>SKS</th>
			<th>Nilai</th>
		</tr>
		</thead>
		<tbody>
		';*/

		
		$isi = '
		<h2>' . $judulfk . '</h2>
		<a style="padding:5px" class="ui-button ui-state-default ui-corner-all disable-ajax" onclick="window.open(\'' . $link . '\');"><b>Cetak</b></a>
		
		
		<br />
		<table width="850" id="myTable" class="tablesorter">
		<thead>
		<tr>
			<th>Mata Kuliah</th>
			<th>SKS</th>
			<th>Nilai</th>
		</tr>
		</thead>
		<tbody>
		';
		

		/* ditaruh setelah 	<h2>' . $judulfk . '</h2>

		<a style="padding:5px" class="ui-button ui-state-default ui-corner-all disable-ajax" onclick="window.open(\'' . $link . '\');"><b>Cetak</b></a>*/
		
		// Semester Pendek
		if ($sem_sp == 'SP')
		{
			$sql = 
				"SELECT 
					kd_mata_kuliah AS kode,
					upper(nm_mata_kuliah) AS nama,
					nama_kelas,
					tipe_semester,
					kredit_semester AS sks,
					CASE WHEN flagnilai = 1 THEN nilai_huruf ELSE '-' END AS nilai,
					bobot,
					bobot_total,
					id_pengambilan_mk
				FROM (
					SELECT 
						pengambilan_mk.id_mhs,
						mata_kuliah.kd_mata_kuliah,
						mata_kuliah.nm_mata_kuliah,
						nama_kelas,
						tipe_semester,
						kurikulum_mk.kredit_semester,

						/* Nilai Huruf, cek status Tugas Akhir */
						CASE 
						  WHEN kurikulum_mk.status_mkta = '1' AND (nilai_huruf = 'E' OR nilai_huruf IS NULL) /* AND id_fakultas = 7 */ THEN 'T'
						  WHEN nilai_huruf IS NULL THEN 'E' 
						  ELSE nilai_huruf
						END AS nilai_huruf,

						/* Bobot = Standar nilai */
						CASE
						  WHEN standar_nilai.nilai_standar_nilai IS NULL THEN 0
						  ELSE standar_nilai.nilai_standar_nilai
						END AS bobot,

						/* Bobot total = Standar Nilai x SKS */
						COALESCE(standar_nilai.nilai_standar_nilai * kurikulum_mk.kredit_semester, 0) AS bobot_total,

						/* Ranking group by id_mhs-kode mata kuliah */
						row_number() OVER(PARTITION BY pengambilan_mk.id_mhs, kd_mata_kuliah ORDER BY nilai_huruf) AS rangking,

						pengambilan_mk.id_pengambilan_mk,
						pengambilan_mk.flagnilai
					FROM pengambilan_mk
					JOIN mahasiswa			ON pengambilan_mk.id_mhs = mahasiswa.id_mhs
					JOIN program_studi		ON mahasiswa.id_program_studi = program_studi.id_program_studi
					JOIN semester			ON pengambilan_mk.id_semester = semester.id_semester
					LEFT JOIN kelas_mk		ON pengambilan_mk.id_kelas_mk = kelas_mk.id_kelas_mk
					LEFT JOIN nama_kelas	ON kelas_mk.no_kelas_mk = nama_kelas.id_nama_kelas
					LEFT JOIN kurikulum_mk	ON pengambilan_mk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk
					LEFT JOIN mata_kuliah	ON kurikulum_mk.id_mata_kuliah = mata_kuliah.id_mata_kuliah
					LEFT JOIN standar_nilai	ON pengambilan_mk.nilai_huruf = standar_nilai.nm_standar_nilai
					WHERE 
						group_semester || thn_akademik_semester IN (SELECT group_semester || thn_akademik_semester FROM semester WHERE id_semester = '" . $idsemes . "') AND 
						tipe_semester = 'SP' AND
						status_apv_pengambilan_mk = 1 AND
						pengambilan_mk.status_hapus = 0 AND
						pengambilan_mk.status_pengambilan_mk != 0 AND
						mahasiswa.id_mhs = '" . $id_mhs . "')
				WHERE rangking = 1";
		}
		else  // Semester Reguler
		{
			// Catatan :
			// - Nilai huruf sebelumnya jika NULL langsung dikonversi ke E
			// - Nilai huruf sekarang jika NULL tampil Kosong (*K)
			
			$sql = 
				"SELECT 
					kd_mata_kuliah AS kode,
					upper(nm_mata_kuliah) AS nama,
					nama_kelas,
					tipe_semester,
					kredit_semester AS sks,
					/* Status Belum Tampil */
					CASE WHEN flagnilai = 1 THEN nilai_huruf ELSE '*BT' END AS nilai,
					bobot,
					bobot_total,
					id_pengambilan_mk
				FROM (
					SELECT 
						pengambilan_mk.id_mhs,
						mata_kuliah.kd_mata_kuliah,
						mata_kuliah.nm_mata_kuliah,
						nama_kelas,
						tipe_semester,
						kurikulum_mk.kredit_semester,

						CASE 
						  WHEN pengambilan_mk.nilai_huruf IS NULL THEN '*K' 
						  ELSE pengambilan_mk.nilai_huruf
						END AS nilai_huruf,

						/* Bobot */
						CASE
						  WHEN standar_nilai.nilai_standar_nilai IS NULL THEN 0
						  ELSE standar_nilai.nilai_standar_nilai
						END AS bobot,

						/* Bobot Total = Standar Nilai x SKS */
						COALESCE(standar_nilai.nilai_standar_nilai * kurikulum_mk.kredit_semester, 0) AS bobot_total,

						/* Ranking berdasar kode */
						row_number() OVER(PARTITION BY pengambilan_mk.id_mhs, kd_mata_kuliah ORDER BY nilai_huruf) AS rangking,

						pengambilan_mk.id_pengambilan_mk,
						pengambilan_mk.flagnilai
					FROM pengambilan_mk
					JOIN mahasiswa			ON pengambilan_mk.id_mhs = mahasiswa.id_mhs
					JOIN semester			ON pengambilan_mk.id_semester = semester.id_semester
					JOIN program_studi		ON mahasiswa.id_program_studi = program_studi.id_program_studi
					LEFT JOIN kelas_mk		ON pengambilan_mk.id_kelas_mk = kelas_mk.id_kelas_mk
					LEFT JOIN nama_kelas	ON kelas_mk.no_kelas_mk = nama_kelas.id_nama_kelas
					LEFT JOIN kurikulum_mk	ON pengambilan_mk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk
					LEFT JOIN mata_kuliah	ON kurikulum_mk.id_mata_kuliah = mata_kuliah.id_mata_kuliah
					LEFT JOIN standar_nilai	ON pengambilan_mk.nilai_huruf = standar_nilai.nm_standar_nilai
					WHERE
						group_semester||thn_akademik_semester IN (SELECT group_semester||thn_akademik_semester FROM semester WHERE id_semester = '" . $idsemes . "') AND
						tipe_semester IN ('UP','REG') AND
						status_apv_pengambilan_mk = 1 AND 
						pengambilan_mk.status_hapus = 0 AND 
						pengambilan_mk.status_pengambilan_mk != 0 AND
						mahasiswa.id_mhs = '" . $id_mhs . "'
				)
				WHERE rangking = 1";
		}
		
		$result = $db->Query($sql) or die("salah kueri : ".__LINE__);
		
		while ($r = $db->FetchRow())
		{
			$isi .= '
					<tr>
						<td><span>' . $r[0] . ' -- ' . $r[1] . '</span></td>
						<td align="center">' . $r[4] . '</td>';
			
			// status flagnilai "-" atau belum tampil
			if ($r[5] == '-')
			{
				$isi .= '<td align="center">BT</td>';
			}
			else
			{
				// Link untuk menampilkan detail khs, khusus umaha
				if($status_sem == 1){
					$isi .= '<td align="center"><a style="text-decoration:underline;cursor:pointer;" onclick="window.open(\'proses/_akademik-khs-tampil_det.php?mk=' . $r[8] . '\',\'baru2\');">' . trim($r[5]) . '</a></td>';
				}
				else{

					// Set Italic untuk Nilai *K dan *BT
					if ($r[5] == '*K' || $r[5] == '*BT') {
						$isi .= '<td align="center"><i>' . trim($r[5]) . '</i></td>';
					}
					else {
						$isi .= '<td align="center">' . trim($r[5]) . '</td>';
					}
					
				}
			}

			$isi .= '
				</tr>';
		}

		$isi .= '</tbody></table>';
	}
	elseif($status_sem_khs == 1){
		$semester = $sem . " " . $tahun . " ";
		
		$judulfk = "Hasil Studi Semester " . $semester . " Belum Bisa Dicetak, Silakan Hubungi Bagian Keuangan";
		
		$isi = '
		<h2>' . $judulfk . '</h2>';	
	}
}

echo $isi;