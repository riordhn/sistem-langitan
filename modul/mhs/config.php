<?php
require '../../config.php';

$is_bidikmisi = 0;

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

foreach ($user->MODULs as &$modul)
{
	$nm_modul    = &$modul['NM_MODUL'];
	$akses_modul = &$modul['AKSES'];

	if($is_bidikmisi=='1') {
		if ($nm_modul == 'bidikmisi')     { $akses_modul = 1; }
	}
	
	foreach ($modul['MENUs'] as &$menu)
	{
		$nm_menu = &$menu['NM_MENU'];
		$akses_menu = &$menu['AKSES'];
		
	}
}