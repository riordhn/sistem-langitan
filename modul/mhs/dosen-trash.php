<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	$jkueri_trash = '
	<script type="text/javascript">
		function tampildata(){
			$.ajax({
				type:"POST",
				url:"proses/_dosen-trash_hapus.php",
				data: "aksi=tampil",
				success: function(data){
					$("#trash_hapus").html(data);
				}
			});
		}
		tampildata();

		function hapus_kirim(f){
			$.ajax({
				type:"POST",
				url:"proses/_dosen-trash_hapus.php",
				data: $("#"+f).serialize(),
				success: function(data){
					alert(data);
					tampildata();
				}
			});
		}
		function hapustunggal_kirim(f){
			$.ajax({
				type:"POST",
				url:"proses/_dosen-trash_hapus.php",
				data: "aksi=hapustunggal&ygdihapus="+f,
				success: function(data){
					alert(data);
					tampildata();
				}
			});
		}
		function bacapesan(f){
			$.ajax({
				type:"POST",
				url:"proses/_dosen-trash_hapus.php",
				data: "aksi=bacapesan&ygdibaca="+f,
				success: function(data){
					$("#trash_detail").html(data);
				}
			});
		}
	</script>
	';
	$smarty->assign('jkueri_trash', $jkueri_trash);

	$smarty->display('dosen-trash.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>
