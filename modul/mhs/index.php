<?php
require_once('config.php');
include ('../sumberdaya/includes/encrypt.php');

$nama_singkat_pt = $nama_singkat;

$depan = time();
$belakang = strrev(time());

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}
else if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA)
{

	// force password to ISO27001:2009
	if($_SESSION['standar_iso']=='no'){
		header("Location: ../../login.php?mode=ltp-null");
		exit();
	}

	// Blok khusus feb, fisip, fst
	if (!in_array($user->MAHASISWA->ID_FAKULTAS, array(4, 7)))
	{
		//echo "Mohon maaf, anda sedang dalam antrian. Terima kasih. <br/><a href=\"/logout.php\">Logout</a>";
		//exit();
	}
	
	// Pembatasan khusus mahasiswa user max 500 active
	if ($user->ID_ROLE == 3 && $user->MAHASISWA->NIM != '080916062')
	{
		$limit = time() - 300;
		$db->Query("select count(*) as jumlah from session_pengguna where id_role = 3 and waktu_session > {$limit}");
		$row = $db->FetchAssoc();
		
		/*
		if ($row['JUMLAH'] > 750)
		{
			echo "<html><head><meta http-equiv=\"REFRESH\" content=\"2;url=".$base_url."logout.php\"></head><body>";
			echo "Mohon maaf, anda sedang dalam antrian. Terima kasih. <br/><a href=\"/logout.php\">Logout</a>";
			echo "</body></html>";
			exit();
		}
		*/

	}
	
	// ambil nama mahasiswa
	$namamahasiswa = $user->MAHASISWA->NAMA;
	$nimmahasiswa = $user->MAHASISWA->NIM;
	$noujian = $user->MAHASISWA->NO_UJIAN;
	$fotomahasiswa = "../../foto_mhs/{$nama_singkat_pt}/".$nimmahasiswa.".jpg";
	## jika extension .jpg dalam huruf kapital
	if (file_exists($fotomahasiswa) == 0) 
	{
		$fotomahasiswa = "../../foto_mhs/{$nama_singkat_pt}/".$nimmahasiswa.".JPG";
	}

	if (file_exists($fotomahasiswa) == 0) 
	{
		$fotomahasiswa = "../../foto_mhs/{$nama_singkat_pt}/".$noujian.".JPG";
	}

	if (file_exists($fotomahasiswa) == 0) 
	{
		$fotomahasiswa = "../../foto_mhs/image-not-found.png";
	}

    $to = $namamahasiswa;
    $file = $nimmahasiswa;

    $img = 'proses/upload_photo.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&yth=' . $to . '&file=' . $file . '&' . $belakang . '=' . $depan . $belakang) . '';
    $smarty->assign('IMG', $img);

	
	/** disable, untuk meringankan db, sudah dipindah ke session $user
	$kueri = "select a.nm_pengguna, b.nim_mhs, c.no_ujian
				from pengguna a 
				left join mahasiswa b on a.id_pengguna = b.id_pengguna
				left join calon_mahasiswa_baru c on c.id_c_mhs = b.id_c_mhs
				where a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri) or die("salah kueri 6 : ");
	
	while($r = $db->FetchRow()) {
		$namamahasiswa = $r[0];
		$nimmahasiswa = $r[1];
		$noujian = $r[2];
		$fotomahasiswa = "../../foto_mhs/".$nimmahasiswa.".JPG";
		
		if (file_exists($fotomahasiswa)==0) {
		$fotomahasiswa = "../../foto_mhs/".$noujian.".JPG";
		
			if (file_exists($fotomahasiswa)==0) {
				$fotomahasiswa = "../../foto_mhs/".$noujian.".jpg";
			}
		}
	}
	*/

	$peringatan = "";
	// cek apakah bidik misi
		/*
		$kueri2 = "select b.status_bidik_misi_baru
		from aucc.mahasiswa a, aucc.calon_mahasiswa_data b
		where a.id_c_mhs=b.id_c_mhs and a.id_pengguna='".$user->ID_PENGGUNA."' 
		";
		*/
		

		$id_jenjang = '';
		$kueri = $db->Query("select b.id_jenjang from aucc.mahasiswa a, aucc.program_studi b where a.id_program_studi=b.id_program_studi and a.id_pengguna='".$user->ID_PENGGUNA."' ");
		$r = $db->FetchAssoc();
		while($r = $db->FetchRow()) {
			$id_jenjang = $r[0];
		}

		// cek data di biodata
		$mhs_nama = ''; $mhs_kelamin = ''; $mhs_agama = ''; $mhs_lahir_tgl = ''; $mhs_fb = ''; $mhs_tw = '';
		$kueri = "select nm_pengguna, kelamin_pengguna, id_agama, to_char(tgl_lahir_pengguna,'DD-MM-YYYY'), email_pengguna, email_alternate, id_fb, id_twitter from pengguna where id_pengguna='".$user->ID_PENGGUNA."'";
		$result = $db->Query($kueri)or die("salah kueri 10 : ");
		while($r = $db->FetchArray()) {
			$mhs_nama = $r[0];
			$mhs_kelamin = $r[1];
			$mhs_agama = $r[2];
			$mhs_lahir_tgl = $r[3];
			$mhs_fb = $r[6];
			$mhs_tw = $r[7];
		}
		if(strlen(trim($mhs_kelamin))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data jenis kelamin</li>'; }
		if(strlen(trim($mhs_agama))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data agama</li>'; }
		if(strlen(trim($mhs_lahir_tgl))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data tanggal lahir</li>'; }
		/*if(strlen(trim($mhs_fb))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data FB</li>'; }
		if(strlen(trim($mhs_tw))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data Twitter</li>'; }*/
		
		
		$mhs_lahir_prop = "";$mhs_lahir_kota = ""; $mhs_alamat = ""; $mhs_hp = ""; $mhs_thn_lulus = ""; $mhs_nem_nilai = ""; $mhs_nem_pelajaran = ""; $mhs_cita = ""; $ayah_nama = ""; $ayah_dikakhir = ""; $ayah_kerja = ""; $ayah_alamat = ""; $ibu_nama = ""; $ibu_dikakhir = ""; $ibu_kerja = ""; $ibu_alamat = ""; $penghasilan = ""; $mhs_prodi2 = ""; $mhs_alamat_prop = ""; $mhs_alamat_kota = ""; $mhs_alamat_prop_ayah = ""; $mhs_alamat_kota_ayah = ""; $mhs_alamat_prop_ibu = ""; $mhs_alamat_kota_ibu = ""; $mhs_alamat_asli = ""; $mhs_alamat_prop_asli = ""; $mhs_alamat_kota_asli = ""; $mhs_kebangsaan = ""; $telp_ayah = ""; $telp_ibu = ""; $tgl_lahir_ayah = ""; $tgl_lahir_ibu = ""; $disabilitas_ayah = ""; $disabilitas_ibu = ""; $penghasilan_ayah = ""; $penghasilan_ibu = "";

		$kueri = "select nim_mhs, lahir_prop_mhs, lahir_kota_mhs, alamat_mhs, mobile_mhs, id_sekolah_asal_mhs, thn_lulus_mhs, nilai_skhun_mhs, nem_pelajaran_mhs, cita_cita_mhs, nm_ayah_mhs, pendidikan_ayah_mhs, pekerjaan_ayah_mhs, alamat_ayah_mhs, nm_ibu_mhs, pendidikan_ibu_mhs, pekerjaan_ibu_mhs, alamat_ibu_mhs, penghasilan_ortu_mhs, id_program_studi, ASAL_KOTA_MHS, ASAL_PROV_MHS, 
		ALAMAT_AYAH_MHS_PROV,ALAMAT_AYAH_MHS_KOTA, ALAMAT_IBU_MHS_PROV,ALAMAT_IBU_MHS_KOTA, 
		ALAMAT_ASAL_MHS,ALAMAT_ASAL_MHS_PROV,ALAMAT_ASAL_MHS_KOTA, ID_KEBANGSAAN, STATUS_MHS_ASING,
		TELP_AYAH_MHS, TELP_IBU_MHS, TGL_LAHIR_AYAH_MHS, TGL_LAHIR_IBU_MHS, ID_DISABILITAS_AYAH_MHS, ID_DISABILITAS_IBU_MHS, PENGHASILAN_AYAH_MHS, PENGHASILAN_IBU_MHS,
		id_c_mhs, id_mhs
		from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'";
		$result = $db->Query($kueri)or die("salah kueri 42 : ");
		while($r = $db->FetchRow()) {
			$mhs_lahir_prop = $r[1];
			$mhs_lahir_kota = $r[2];
			$mhs_alamat = $r[3];
			$mhs_hp = $r[4];
			$mhs_sma = $r[5];
			$mhs_thn_lulus = $r[6];
			$mhs_nem_nilai = $r[7];
			$mhs_nem_pelajaran = $r[8];
			$mhs_cita = $r[9];
			$ayah_nama = $r[10];
			$ayah_dikakhir = $r[11];
			$ayah_kerja = $r[12];
			$ayah_alamat = $r[13];
			$ibu_nama = $r[14];
			$ibu_dikakhir = $r[15];
			$ibu_kerja = $r[16];
			$ibu_alamat = $r[17];
			$penghasilan = $r[18];
			$mhs_prodi2 = $r[19];
			$mhs_alamat_kota = $r[20];
			$mhs_alamat_prop = $r[21];
			$mhs_alamat_prop_ayah = $r[22];
			$mhs_alamat_kota_ayah = $r[23];
			$mhs_alamat_prop_ibu = $r[24];
			$mhs_alamat_kota_ibu = $r[25];
			$mhs_alamat_asli = $r[26];
			$mhs_alamat_prop_asli = $r[27];
			$mhs_alamat_kota_asli = $r[28];
			$mhs_kebangsaan = $r[29];
			$telp_ayah = $r[31];
			$telp_ibu = $r[32];
			$tgl_lahir_ayah = $r[33];
			$tgl_lahir_ibu = $r[34];
			$disabilitas_ayah = $r[35];
			$disabilitas_ibu = $r[36];
			$penghasilan_ayah = $r[37];
			$penghasilan_ibu = $r[38];

			$id_mhs = $r[40];
		}

		if(strlen(trim($mhs_lahir_prop))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data propinsi tempat lahir</li>'; }
		if(strlen(trim($mhs_lahir_kota))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data kota tempat lahir</li>'; }
		if(strlen(trim($mhs_alamat))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data alamat di surabaya</li>'; }
		if(strlen(trim($mhs_hp))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data no HP</li>'; }
		if($id_jenjang=='1' or $id_jenjang=='5') {
			if(strlen(trim($mhs_sma))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data sekolah sma</li>'; }
			if(strlen(trim($mhs_thn_lulus))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data tahun lulus sma</li>'; }
			if(strlen(trim($mhs_nem_nilai))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data nilai NEM</li>'; }
			if(strlen(trim($mhs_nem_pelajaran))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data jumlah pelajaran sma</li>'; }
			if(strlen(trim($mhs_cita))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data cita-cita</li>'; }
		}
		if(strlen(trim($ayah_nama))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data nama ayah</li>'; }
		if(strlen(trim($ayah_dikakhir))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data pendidikan ayah</li>'; }
		if(strlen(trim($ayah_kerja))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data pekerjaan ayah</li>'; }
		if(strlen(trim($ayah_alamat))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data alamat tinggal ayah / telp</li>'; }
		if(strlen(trim($mhs_alamat_prop_ayah))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data propinsi alamat tinggal ayah</li>'; }
		if(strlen(trim($mhs_alamat_kota_ayah))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data kota alamat tinggal ayah</li>'; }
		if(strlen(trim($ibu_nama))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data nama ibu</li>'; }
		if(strlen(trim($ibu_dikakhir))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data pendidikan ibu</li>'; }
		if(strlen(trim($ibu_kerja))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data pekerjaan ibu</li>'; }
		if(strlen(trim($ibu_alamat))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data alamat tinggal ibu / telp</li>'; }
		if(strlen(trim($mhs_alamat_prop_ibu))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data propinsi alamat tinggal ibu</li>'; }
		if(strlen(trim($mhs_alamat_kota_ibu))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data kota alamat tinggal ibu</li>'; }

		if(strlen(trim($telp_ayah))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data telp ayah</li>'; }
		if(strlen(trim($telp_ibu))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data telp ibu</li>'; }
		if(strlen(trim($penghasilan_ayah))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data penghasilan ayah</li>'; }
		if(strlen(trim($penghasilan_ibu))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data penghasilan ibu</li>'; }

		/*if(strlen(trim($penghasilan))==0) { $peringatan .= '<li>[Biodata] Belum mengisi data penghasilan orang tua</li>'; }*/

		// beri peringatan bila belum isi SKP
		$jum_skp=0;
		$kueri = $db->Query("select count(*) from aucc.krp_khp where id_mhs='".$id_mhs."'");
		//$result = $db->Query($kueri)or die("salah kueri 15 : ");
		while($r = $db->FetchArray()) {
			$jum_skp = $r[0];
		}
		/*if($id_jenjang=='1' or $id_jenjang=='5') {
			if($jum_skp == 0) { $peringatan .= '<li>[SKP] Belum mengisi data SKP</li>'; }
		}*/
		
		/*
	
	if(strlen($peringatan)>0) {
		$dialoge = '
		<script type="text/javascript">
			$(document).ready(function(){
				// create dialog element
				var lupaPasswordDialog = document.createElement("div");

				$(lupaPasswordDialog).html(\'<iframe width="600" height="300" frameborder="0" scrolling="no" src="notifikasi.php"></iframe>\');

				// Display dialog
				$(lupaPasswordDialog).dialog({
					title: "Notifikasi",
					width: 640,
					height: 300,
					resizable: false,
					close: function() {
						lupaPasswordDialog.parentNode.removeChild(lupaPasswordDialog);
					}
				});
			});
		</script>
		';
	}else{
		$dialoge = '';
	}
	$smarty->assign('dialoge', $dialoge);
	*/
	$smarty->assign('namamahasiswa', $namamahasiswa);
	$smarty->assign('fotomahasiswa', $fotomahasiswa);
	
	// domain email
	$smarty->assign('email_domain', $user->EMAIL_DOMAIN);
	
	if($user->ID_PENGGUNA=='50455') {
		//print_r($user->MODULs);
	}


	// ambil semester_aktif
	$sem_aktif=""; $tmp_thn_akad=""; $tmp_sem="";
	$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where STATUS_AKTIF_SEMESTER='True' order by id_semester desc";
	$result = $db->Query($kueri)or die ("salah kueri : ");
	while($r = $db->FetchRow()) {
		$sem_aktif = $r[0];
		$tmp_thn_akad = $r[1];
		$tmp_sem = $r[2];
	}
	
	$kemarin_thn=""; $kemarin_sem="";
	if($tmp_sem=="Ganjil") {
		$kemarin_thn = $tmp_thn_akad-1;
		$kemarin_sem = "Genap";
	}else if($tmp_sem=="Genap") {
		$kemarin_thn = $tmp_thn_akad;
		$kemarin_sem = "Ganjil";
	}
	// ambil semester_kemarin
		$kemarin_idsem="";
		$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		while($r = $db->FetchRow()) {
			$kemarin_idsem = $r[0];
		}

		// apakah semester tsb cuti ?
		$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		$r = $db->FetchRow();
		if($r[0]>0) { // maka cuti ke-1
			// ambil semester sebelum cuti
			$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$sem_aktif_tahun = $r[1];
				$sem_aktif_semes = $r[2];
			}
			if($sem_aktif_semes=="Ganjil") {
				$kemarin_thn = $sem_aktif_tahun-1;
				$kemarin_sem = "Genap";
			}else if($sem_aktif_semes=="Genap") {
				$kemarin_thn = $sem_aktif_tahun;
				$kemarin_sem = "Ganjil";
			}
			$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$kemarin_idsem = $r[0];
			}
			// apakah semester tsb cuti ?
			$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			$r = $db->FetchRow();
			if($r[0]>0) { // maka cuti ke-2
				// ambil semester sebelum cuti
				$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$sem_aktif_tahun = $r[1];
					$sem_aktif_semes = $r[2];
				}
				if($sem_aktif_semes=="Ganjil") {
					$kemarin_thn = $sem_aktif_tahun-1;
					$kemarin_sem = "Genap";
				}else if($sem_aktif_semes=="Genap") {
					$kemarin_thn = $sem_aktif_tahun;
					$kemarin_sem = "Ganjil";
				}
				$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$kemarin_idsem = $r[0];
				}
			}
		}

	//include 'class/akademik.class.php';
	//$akademik = new akademik($db, $user->ID_PENGGUNA);
	//$getIPS = $akademik->getIPS($id_mhs, $kemarin_idsem);
	//$getIPK = $akademik->getIPK($id_mhs);
	/*$_SESSION['ipk'] = $user->MAHASISWA->IPK;
	$_SESSION['ips'] = $user->MAHASISWA->IPS;
	$_SESSION['sks_sem'] = $user->MAHASISWA->SKS_SEMESTER;
	$_SESSION['id_semester_kemarin'] = $user->SEMESTER->ID_SEMESTER_LALU;
	$_SESSION['id_semester_sekarang'] = $user->SEMESTER->ID_SEMESTER_AKTIF;
	$_SESSION['total_sks_kumulatif'] = $user->MAHASISWA->SKS_TOTAL;*/
	// data menu dari $user
    $smarty->assign('modul_set', $user->MODULs);
	$smarty->display('index.tpl');

}else {
	echo '<script>alert("Anda Belum Login");window.location="../../logout.php"</script>';}
?>