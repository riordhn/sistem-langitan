<?php
include 'config.php';
include 'function-tampil-informasi.php';
// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA) {
	header("location: /logout.php");
	exit();
}
$id_pt = $id_pt_user;
$kode_bulan_sekarang = date('m');
$tahun_sekarang = date('Y');
// Semester Bayar Bulan
$db->Query("SELECT * FROM SEMESTER_BAYAR_BULAN WHERE KODE_BULAN='{$kode_bulan_sekarang}'");
$semester_bayar_bulan = $db->FetchAssoc();
// Semester Bayar
$db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='{$semester_bayar_bulan['NM_SEMESTER']}' AND THN_AKADEMIK_SEMESTER='{$tahun_sekarang}'  AND ID_PERGURUAN_TINGGI='{$id_pt}'");
$semester_bayar = $db->FetchAssoc();
if (!empty($semester_bayar)) {
	$id_mhs = $db->QuerySingle("SELECT ID_MHS FROM MAHASISWA WHERE ID_PENGGUNA ='{$user->ID_PENGGUNA}'");
	$db->Query("
		SELECT T.* 
		FROM TAGIHAN_MHS TM
		JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
		JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=T.ID_DETAIL_BIAYA
		WHERE DB.ID_BIAYA=" . ID_BIAYA_SPP . "
		AND  TM.ID_SEMESTER='{$semester_bayar['ID_SEMESTER']}'
		AND ID_MHS='{$id_mhs}'
	");
	$tagihan = $db->FetchAssoc();
	$tagihan_perbulan = $tagihan['BESAR_BIAYA'] / 6;
	if (!empty($tagihan)) {
		$pembayaran_mahasiswa_bulanan = $db->QueryToArray("
		SELECT SBB.KODE_BULAN,SBB.NM_BULAN,COALESCE(PPEM.PEMBAYARAN,0) PEMBAYARAN_BULANAN
		FROM SEMESTER_BAYAR_BULAN SBB
		LEFT JOIN ( 
			SELECT PP.KODE_BULAN,SUM(PP.BESAR_PEMBAYARAN) PEMBAYARAN
			FROM TAGIHAN_MHS TM 
			JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS=TM.ID_TAGIHAN_MHS
			JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=T.ID_DETAIL_BIAYA
			JOIN PEMBAYARAN PEM ON T.ID_TAGIHAN=PEM.ID_TAGIHAN
			JOIN PEMBAYARAN_PERBULAN PP ON PP.ID_TAGIHAN=PEM.ID_TAGIHAN AND PP.ID_PEMBAYARAN=PEM.ID_PEMBAYARAN
			WHERE TM.ID_MHS='{$id_mhs}' AND PEM.ID_STATUS_PEMBAYARAN='1'
			AND TM.ID_SEMESTER='{$semester_bayar['ID_SEMESTER']}'
			AND DB.ID_BIAYA=" . ID_BIAYA_SPP . "
			GROUP BY PP.KODE_BULAN
		) PPEM ON PPEM.KODE_BULAN=SBB.KODE_BULAN
		WHERE SBB.NM_SEMESTER='{$semester_bayar_bulan['NM_SEMESTER']}'
		");
		$smarty->assign('tagihan_perbulan', $tagihan_perbulan);
		$smarty->assign('semester_bayar', $semester_bayar);
		$smarty->assign('pembayaran_bulanan', $pembayaran_mahasiswa_bulanan);
	} else {
		$smarty->assign('informasi', alert_error("Tagihan pembayaran belum dibuat"));
	}
} else {
	$smarty->assign('informasi', alert_error("Semester bayar tidak ditemukan."));
}

$smarty->display('keuangan-tagihan-spp.tpl');
