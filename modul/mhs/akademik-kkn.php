<?php

include 'config.php';
include 'function-tampil-informasi.php';

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA) {
    header("location: /logout.php");
    exit();
}

$db->Query("SELECT * FROM KKN_ANGKATAN WHERE STATUS_AKTIF='1'");
$kkn_aktif = $db->FetchAssoc();
$db->Query("
    SELECT * 
    FROM KKN_KELOMPOK_MHS 
    WHERE ID_KKN_ANGKATAN='{$kkn_aktif['ID_KKN_ANGKATAN']}'
    AND ID_MHS IN (
        SELECT ID_MHS
        FROM MAHASISWA
        WHERE ID_PENGGUNA='{$user->ID_PENGGUNA}'
    )");
$peserta = $db->FetchAssoc();


	if(isset($_POST['no_rek']) and $_POST['no_rek'] != ''){
	
	$nama =  str_replace("'", "''", $_POST['nama']);

	$db->Query("
    UPDATE MAHASISWA SET NO_REKENING = '$_POST[no_rek]', BANK_REKENING = '$_POST[bank]', ATAS_NAMA_REKENING = '$nama' WHERE ID_PENGGUNA='{$user->ID_PENGGUNA}'
    ");
	}

	$data_mhs = $db->FetchAssoc();
	$db->Query("
    SELECT * FROM MAHASISWA JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
    WHERE MAHASISWA.ID_PENGGUNA='{$user->ID_PENGGUNA}'
    ");
    $data_mhs = $db->FetchAssoc();


if ($peserta != '') {
    $block_proses = alert_success("Mohon maaf, karena untuk penentuan daerah masih menunggu ijin resmi dari kabupaten/Kota yang ditempati KKN dan sampai saat ini masih belum mendapatkan ijin.");
    $block_proses .=alert_success("Maka dari itu pembagian yang sempat muncul di cyber belum valid dan dinyatakan Batal, untuk pembagian Kelompok yang sebenarnya menununggu kabar berikutnya");
    $block_proses .=alert_success("Untuk pengarahan Tanggal 18 Mei di tunda pada tanggal 2 Juni 2013");
    $block_proses .=alert_success("Pengumuman Resmi dapat  dilihat pada Website <a href='http://www.lppm.unair.ac.id' class='disable-ajax' target='_blank'>LPPM Unair</a>");
    $kegiatan_kkn = $db->QueryToArray("SELECT * FROM KKN_KEGIATAN WHERE ID_KKN_ANGKATAN='{$kkn_aktif['ID_KKN_ANGKATAN']}' AND TIPE=1 ORDER BY TGL_POST DESC");
    $db->Query("
    SELECT KK.*,KEL.NM_KELURAHAN,KEC.NM_KECAMATAN,KOT.NM_KOTA,P.NM_PENGGUNA DPL
    FROM KKN_KELOMPOK KK
    JOIN KELURAHAN KEL ON KK.ID_KELURAHAN=KEL.ID_KELURAHAN
    JOIN KECAMATAN KEC ON KEL.ID_KECAMATAN=KEC.ID_KECAMATAN
    JOIN KOTA KOT ON KOT.ID_KOTA=KEC.ID_KOTA
    LEFT JOIN KKN_DPL KD ON KD.ID_KKN_KELOMPOK = KK.ID_KKN_KELOMPOK
    LEFT JOIN DOSEN D ON D.ID_DOSEN=KD.ID_DOSEN
    LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
    WHERE KK.ID_KKN_KELOMPOK='{$peserta['ID_KKN_KELOMPOK']}'
    ");
    $kelompok_kkn = $db->FetchAssoc();


    if ($kelompok_kkn['STATUS_PUBLISH'] == '1') {
        $data_kelompok = $db->QueryToArray("
            SELECT KKM.*,M.*,P.KELAMIN_PENGGUNA,P.NM_PENGGUNA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.NM_FAKULTAS
            FROM KKN_KELOMPOK_MHS KKM
            JOIN MAHASISWA M ON M.ID_MHS=KKM.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            WHERE KKM.ID_KKN_KELOMPOK='{$peserta['ID_KKN_KELOMPOK']}'
            ORDER BY F.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
            ");
        $smarty->assign('id_mhs', $peserta['ID_MHS']);
        $smarty->assign("kelompok_kkn", $kelompok_kkn);
        $smarty->assign('data_kelompok', $data_kelompok);
    } else {
        $smarty->assign('error_kelompok', "Mohon maaf, Data Kelompok KKN belum dipublikasikan. Terima Kasih");
    }
    $smarty->assign("angkatan", $kkn_aktif);
    $smarty->assign("data_kegiatan", $kegiatan_kkn);
    //$smarty->assign('error',$block_proses);
} else {
    $smarty->assign('error', alert_error("Mohon maaf, Anda tidak termasuk dalam peserta KKN pada angkatan {$kkn_aktif['NAMA_ANGKATAN']}"));
}
$smarty->assign("data_mhs", $data_mhs);
$smarty->display('akademik-kkn.tpl');
?>
