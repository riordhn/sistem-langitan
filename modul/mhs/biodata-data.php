<?php
require_once('config.php');
if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
    $smarty->display('biodata-data.tpl');
} else {
    $smarty->display('session-expired.tpl');
}
