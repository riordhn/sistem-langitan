<?php
require_once('config.php');
require 'function-tampil-informasi-no-conf.php';
require 'class/process.class.php';

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA) {
    header("location: /logout.php");
    exit();
}

// Yudi Sulistya, 02 Aug 2013 (tablesorter)
echo '
<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
$("#myTable").tablesorter(
    {
    sortList: [[0,0]]
    }
);
}
);
</script>
';

$id_fakul_user  = $user->MAHASISWA->ID_FAKULTAS;
$id_pt_user     = $user->ID_PERGURUAN_TINGGI;

// ambil nama di tabel pengguna
$mhs_nama = "";
$kueri = "select NM_PENGGUNA from pengguna where id_pengguna='" . $user->ID_PENGGUNA . "'";
$result = $db->Query($kueri) or die("salah kueri " . __LINE__);
while ($r = $db->FetchRow()) {
    $mhs_nama = $r[0];
}

// ambil id_mhs
$id_mhs = "";
$mhs_nim = "";
$kueri =
    "SELECT a.id_mhs, a.NIM_MHS, 'FAKULTAS '||c.nm_fakultas as nm_fakultas, 
    b.nm_program_studi, d.nm_jenjang, a.TGL_LULUS_MHS, a.TGL_TERDAFTAR_MHS, a.no_ijazah, a.id_program_studi
    from mahasiswa a, program_studi b, fakultas c, jenjang d
    where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and b.id_jenjang=d.id_jenjang and a.id_pengguna='" . $user->ID_PENGGUNA . "'";
$result = $db->Query($kueri) or die("salah kueri " . __LINE__);
while ($r = $db->FetchRow()) {
    $id_mhs = $r[0];
    $mhs_nim = $r[1];
    $mhs_fakul = $r[2];
    $mhs_prodi = $r[3];
    $mhs_jenjang = $r[4];
    $mhs_tgl_lulus = $r[5];
    $mhs_tgl_terdaftar = $r[6];
    $mhs_noijazah = $r[7];
    $mhs_prodi_id = $r[8];
}

$jum_sks = 0;
$jum_bobot = 0;
$ipk = 0;

// Cari Jumlah SKS dan IPK terbaru
// FIKRIE (31-08-2018)
$sql = "SELECT pmk.ID_SEMESTER, s.FD_ID_SMT, s.THN_AKADEMIK_SEMESTER, s.NM_SEMESTER
    FROM PENGAMBILAN_MK pmk
    JOIN SEMESTER s ON s.ID_SEMESTER = pmk.ID_SEMESTER
    JOIN MAHASISWA m ON m.ID_MHS = pmk.ID_MHS
    JOIN PENGGUNA p ON p.ID_PENGGUNA = m.ID_PENGGUNA
    WHERE m.ID_MHS = '{$id_mhs}'
    ORDER BY s.THN_AKADEMIK_SEMESTER DESC
    FETCH first 1 ROW ONLY";

$result2 = $db->Query($sql) or die("salah query " . __LINE__);
while ($r2 = $db->FetchRow()) {
    $fd_id_smt = $r2[1];
}

$sql1 = "SELECT id_mhs,
        SUM(sks) AS total_sks, 
        round(SUM(nilai_mutu) / SUM(sks), 2) AS ipk 
    FROM (
        SELECT
            mhs.id_mhs,
            /* Kode MK utk grouping total mutu */
            COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
            /* SKS */
            COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
            /* Nilai Mutu = SKS * Bobot */
            COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu,
            /* Urutan Nilai Terbaik */
            row_number() OVER (PARTITION BY mhs.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY sn.nilai_standar_nilai DESC) urut
        FROM pengambilan_mk pmk
        JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
        JOIN semester S ON S.id_semester = pmk.id_semester
        -- Via Kelas
        LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
        LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
        -- Via Kurikulum
        LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
        LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
        -- Penyetaraan
        LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
        LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
        -- nilai bobot
        JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
        WHERE 
            pmk.status_apv_pengambilan_mk = 1 AND 
            mhs.id_mhs = '{$id_mhs}' AND 
            S.fd_id_smt <= '{$fd_id_smt}'
    )
    WHERE urut = 1
    GROUP BY id_mhs";

$result3 = $db->Query($sql1) or die("salah query " . __LINE__);
while ($r3 = $db->FetchRow()) {
    $jum_sks = $r3[1];
    $ipk = $r3[2];
}

$isi_transkrip = '
    <table cellspacing="0" cellpadding="0" border="0" width="100%" class="tb_frame">
    <tr>
        <td align=center><img src="' . $base_url . '../../img/akademik_images/logo-' . $nama_singkat . '.gif" width="100" height="100" border="0" alt=""></td>
        <td>
        <!-- aa -->
        ' . strtoupper($nama_pt) . '<BR>
        ' . strtoupper($mhs_fakul) . '<BR>
        ' . $alamat . '<br>
        Telp. ' . $telp_pt . ' Fax ' . $fax_pt . '<br>
        Website : http://www.' . $nama_singkat . '.ac.id; e-mail:rektor@' . $nama_singkat . '.ac.id
        </td>
    </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="0" width="100%" class="tb_frame">
    <tbody>
    <tr>
        <td>Nama</td>
        <td><strong>: ' . $mhs_nama . '</strong></td>
        <td>&nbsp;</td>
        <td>Nomor Ijazah</td>
        <td><strong>: ' . $mhs_noijazah . '</strong></td>
    </tr>
    <tr>
        <td>NIM</td>
        <td><strong>: ' . $mhs_nim . '</strong></td>
        <td>&nbsp;</td>
        <td>Jumlah SKS</td>
        <td><strong>: ' . $jum_sks . '</strong></td>
        </tr>
    <tr>
        <td>Program Studi</td>
        <td><strong>: ' . $mhs_jenjang . ' - ' . $mhs_prodi . '</strong></td>
        <td>&nbsp;</td>
        <td>IP Komulatif</td>
        <td><strong>: ' . $ipk . '</strong></td>
    </tr>
    <tr>
        <td width="32%">Tanggal Terdaftar Pertama Kali</td>
        <td width="25%"><strong>: ' . $mhs_tgl_terdaftar . '</strong></td>
        <td width="7%">&nbsp;</td>
        <td width="16%">Tanggal Lulus</td>
        <td width="20%"><strong>: ' . $mhs_tgl_lulus . '</strong></td>
    </tr>
    </tbody>
    </table>
    <br>

    <table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable" class="tablesorter">
    <thead>
    <tr>
        <th width="80px">Semester</th>
        <th>Kode MA</th>
        <th>Nama Mata Ajar</th>
        <th>SKS</th>
        <th>Nilai</th>
        <th>Bobot</th>
    </tr>
    </thead>
    <tbody>
';
$jum_sks = 0;
$jum_bobot = 0;
$ipk = 0;

// query di tata 22-01-2017
// query ini DEPRECATED, akan dipindah seperti model feeder
$kueri =
    "SELECT 
        s.tahun_ajaran, s.nm_semester, 
        COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
        COALESCE(mk1.nm_mata_kuliah, mk2.nm_mata_kuliah) AS nm_mata_kuliah, 
        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS kredit_semester,
        pmk.nilai_huruf, pmk.flagnilai,
        row_number() OVER(PARTITION BY pmk.id_mhs,COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah)
            ORDER BY pmk.nilai_huruf ASC,s.thn_akademik_semester DESC,s.nm_semester DESC) rangking,
        count(*) OVER(PARTITION BY COALESCE(mk1.nm_mata_kuliah, mk2.nm_mata_kuliah)) terulang,
        COALESCE(mk1.status_praktikum, mk2.status_praktikum) AS status_praktikum,
        pmk.status_hapus, s.group_semester, s.thn_akademik_semester
    FROM pengambilan_mk pmk
    JOIN semester s ON s.id_semester = pmk.id_semester
    -- Via Kelas
    LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
    LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
    -- Via Kurikulum
    LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
    LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
    -- Penyetaraan
    LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
    LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0

    WHERE pmk.id_mhs = {$id_mhs} AND pmk.status_apv_pengambilan_mk = 1
    ORDER BY s.thn_akademik_semester, s.nm_semester, 4";

// khusus non-fikes umaha
if ($id_pt_user == 1 && $id_fakul_user != 1) {

    $kueri =
        "SELECT 
            s.tahun_ajaran, s.nm_semester, 
            COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
            COALESCE(mk1.nm_mata_kuliah, mk2.nm_mata_kuliah) AS nm_mata_kuliah, 
            COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS kredit_semester,
            pmk.nilai_huruf, pmk.flagnilai,
            row_number() OVER(PARTITION BY pmk.id_mhs,COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) 
                ORDER BY pmk.nilai_huruf ASC,s.thn_akademik_semester DESC,s.nm_semester DESC) rangking,
            count(*) OVER(PARTITION BY COALESCE(mk1.nm_mata_kuliah, mk2.nm_mata_kuliah)) terulang,
            COALESCE(mk1.status_praktikum, mk2.status_praktikum) AS status_praktikum,
            pmk.status_hapus, s.group_semester, kk.is_boleh_cetak, pmk.id_semester, sn.nilai_standar_nilai, s.thn_akademik_semester
        FROM pengambilan_mk pmk
        JOIN semester s ON s.id_semester = pmk.id_semester
        LEFT JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf 
        -- Via Kelas
        LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
        LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
        -- Via Kurikulum
        LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
        LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
        -- Penyetaraan
        LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
        LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0

        LEFT JOIN keuangan_khs kk ON kk.id_semester = s.id_semester and kk.id_mhs = pmk.id_mhs
        WHERE pmk.id_mhs = {$id_mhs} AND pmk.status_apv_pengambilan_mk = 1
        ORDER BY s.thn_akademik_semester, s.nm_semester, 4";
}

$result = $db->Query($kueri) or die("salah kueri " . __LINE__);
while ($r = $db->FetchRow()) {
    $semester = $r[0] . ' ' . $r[11] . ' - ' . $r[1];
    if ($r[6] == '1') { // filter FLAGNILAI (Ditampilkan atau Tidak)
        // ambil bobot nilai huruf
        $bobot = $r[14];

        // hijau
        if ($r[7] == 1 and $r[8] > 1) {
            $warna = '#00FF00';
        } else {
            // merah
            if ($r[7] > 1) {
                $warna = '#FF0000';
            } else {
                // kuning
                if ($r[5] == '-') {
                    $warna = 'yellow';
                }
                // putih
                $warna = '#FFFFFF';
            }
        }
        if (($r[5] == 'E' or $r[5] == '') and ($r[9] > 1)) {
            // abu-abu
            $warna = '#cccccc';
        }
        if ($r[10] == '1') {
            // ungu
            $warna = '#772244';
        }


        $isi_transkrip .= '
                <tr bgcolor="' . $warna . '">
                    <td>' . $semester . '</td>
                    <td>' . $r[2] . '</td>
                    <td>' . $r[3] . '</td>
                    <td align="center">' . $r[4] . '</td>
                    <td align="center">' . $r[5] . '</td>
                    <td align="center">' . ($bobot * $r[4]) . '</td>
                </tr>
            ';
        if ($r[5] <= 'E' and $r[5] != '' and $r[5] != '-' and $r[7] == 1 and $r[10] == 0) {
            $jum_sks += $r[4];
            $jum_bobot += ($bobot * $r[4]);
        }
    } else {
        // ambil bobot nilai huruf
        $bobot = $r[14];

        if ($r[10] == '1') {
            // ungu
            $warna = '#772244';
        } else {
            if ($r[5] <= 'E' && !empty($r[5])) {
                // biru muda (tambahan FIKRIE) (31-08-2018)
                $warna = '#00E9FF';
            } else {
                // kuning
                $warna = 'yellow';
            }
        }
        $isi_transkrip .= '
                <tr bgcolor="' . $warna . '">
                    <td> ' . $semester . '</td>
                    <td>' . $r[2] . '</td>
                    <td>' . $r[3] . '</td>
                    <td align="center">' . $r[4] . '</td>
                    <td align="center">' . $r[5] . '</td>
                    <td align="center">' . ($bobot * $r[4]) . '</td>
                </tr>
            ';

        // nilai belum ditampilkan sudah masuk perhitungan ipk dan sks kumulatif
        if ($r[5] <= 'E' and $r[5] != '' and $r[5] != '-' and $r[7] == 1 and $r[10] == 0) {
            $jum_sks += $r[4];
            $jum_bobot += ($bobot * $r[4]);
        }
    }
}

if ($jum_sks == 0) {
    $ipk = '0.00';
} else {
    $ipk = number_format(($jum_bobot / $jum_sks), 2);
}

$isi_transkrip .= '
    </tbody>
    <tr>
        <td rowspan="3" colspan="2"></td>
        <td><div align="left">Jumlah SKS dan Bobot</div></td>
        <td align="center">' . $jum_sks . '</td>
        <td align="center">&nbsp;</td>
        <td align="center">' . $jum_bobot . '</td>
    </tr>
    <tr>
        <td>IP komulatif</td>
        <td colspan="3"><div align="center">' . $ipk . '</div></td>
    </tr>
    <tr>
        <td>Predikat Kelulusan</td>
        <td colspan="3"><div align="center">&nbsp;</div></td>
    </tr>
    </table>
    Keterangan: <br>
    <span>Putih</span> : Normal, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif<br>
    <span style="background-color:green">Hijau</span> : Ulangan, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif<br>
    <span style="background-color:yellow">Kuning</span> : Nilai belum dikeluarkan, Belum ada nilai, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif<br>
    <span style="background-color:#00e9ff">Biru Muda</span> : Nilai belum dikeluarkan, Sudah ada nilai, masuk transkrip dan masuk perhitungan IP kumulatif dan sks kumulatif<br>
    <span style="background-color:red">Merah</span> : Sudah diulang, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif<br>
    <span style="background-color:#cccccc">Abu-abu</span> : PKL,KKN,TA,PROPOSAL,SEMINAR,SKRIPSI,THESIS yang belum masuk nilainya. Tidak masuk perhitungan IPK dan sks kumulatif.<br>
    <span style="background-color:#772244">Ungu</span> : MK Pilihan yang dihapus oleh akademik atas permintaan mahasiswa. Tidak masuk perhitungan IPK dan sks kumulatif.<br>
';
//<input type=button name="dicetak" value="Cetak" onclick="window.open(\'proses/_akademik-transkrip_cetak.php\',\'baru2\');">

/*** TAMBAHAN CEK EVALUASI (NAMBI) ***/
$process = new process($db, $user->ID_PENGGUNA, $user->ID_PERGURUAN_TINGGI);
$proses_evaluasi = $process->CekEvaluasi($id_mhs);
$smarty->assign('message_evaluasi', $proses_evaluasi['message']);
$smarty->assign('isitranskrip', $isi_transkrip);

$smarty->display('akademik-transkrip.tpl');
