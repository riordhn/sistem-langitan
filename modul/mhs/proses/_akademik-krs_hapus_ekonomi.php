<?php
require('../../../config.php');
require('../includes/ceking2.php');

$db2 = new MyOracle();

$id_mhs = $user->MAHASISWA->ID_MHS;
$id_prodi = $user->MAHASISWA->ID_PROGRAM_STUDI;
$id_jenjang = $user->MAHASISWA->ID_JENJANG;
$sem_aktif = $user->SEMESTER->ID_SEMESTER_AKTIF;
$sem_lalu = $user->SEMESTER->ID_SEMESTER_LALU;
$sks_total = $user->MAHASISWA->SKS_TOTAL;
$ipk_mhs = $user->MAHASISWA->IPK;
$ips_mhs = $user->MAHASISWA->IPS;
$ips_bawah = $user->MAHASISWA->SKS_SEMESTER;
$status_krs = $_SESSION['STATUS_KRS'];

if($_POST["aksi"]=="tampil") {
	$isi = '
		<form name="frmkrshapus" id="frmkrshapus">
		<table>
		<tr>
			<th>No.</th>
			<th>KODE MTA</th>
			<th>NAMA MATA AJAR</th>
			<th>SKS MTA</font></th>
			<th>KELAS</th>
			<th>JADWAL</th>
			<th>STATUS</th>
			<th>AKSI</th>
		</tr>
	';

	$kueri = "
	select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester,d.id_pengambilan_mk,c.no_kelas_mk, d.status_apv_pengambilan_mk, d.STATUS_PENGAMBILAN_MK, c.id_kelas_mk
	from mata_kuliah a, kelas_mk c, pengambilan_mk d, kurikulum_mk e
	where c.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and c.id_kelas_mk=d.id_kelas_mk	and d.id_semester='".$sem_aktif."' and d.id_mhs='".$id_mhs."'
	order by a.kd_mata_kuliah
	";
	
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	$hit=0;
	while($r = $db->FetchRow()) {
		$hit++;
		$nm_kelas = "";
		$result2 = $db2->Query("select nama_kelas from nama_kelas where id_nama_kelas='".$r[4]."'")or die("salah kueri 42 : ".$db2->Error());
		while($r2 = $db2->FetchRow()) {
			$nm_kelas = $r2[0];
		}
		// AMBIL JADWAL
		$jadwalnya = "";
		$result2 = $db2->Query("select a.nm_jadwal_jam,b.id_jadwal_hari from aucc.jadwal_jam a, aucc.jadwal_kelas b where a.id_jadwal_jam=b.id_jadwal_jam and b.id_kelas_mk='".$r[7]."'")or die("salah kueri 63 : ");
		while($r2 = $db2->FetchRow()) {
			if($r2[1]=='1') {
				$harinya = "Minggu";
			}else if($r2[1]=='2') {
				$harinya = "Senin";
			}else if($r2[1]=='3') {
				$harinya = "Selasa";
			}else if($r2[1]=='4') {
				$harinya = "Rabu";
			}else if($r2[1]=='5') {
				$harinya = "Kamis";
			}else if($r2[1]=='6') {
				$harinya = "Jumat";
			}else if($r2[1]=='7') {
				$harinya = "Sabtu";
			}
			$jadwalnya .= "<br>".$harinya." ".$r2[0];
		}
		$jadwalnya = substr($jadwalnya,4);

		if($status_krs=='1') {
			if($r[5]=="1") {
				$boleh_hapus = "";
			}else if($r[5]=="0") {
				$boleh_hapus = '<input type="button" name="simpan" value="Hapus" onclick="if(confirm(\'Yakin hapus ?\')!=0){ krshapus_kirim('.$r[3].') }" >';
			}
		}else{
			$boleh_hapus = "";
		}
		if($r[6]=="3") {
			$stat_mk = "MTA tidak disetujui";
		}else{
			if($r[5]=="1") {
				$stat_mk = "Sudah disetujui dosen";
			}else if($r[5]=="0") {
				$stat_mk = 'Belum disetujui dosen';
			}
		}
		$isi .= '
			<tr>
				<td>'.$hit.'</td>
				<td>'.$r[0].'</td>
				<td>'.$r[1].'</td>
				<td>'.$r[2].'</td>
				<td>'.$nm_kelas.'</td>
				<td>'.$jadwalnya.'</td>
				<td>'.$stat_mk.'</td>
				<td align=center>'.$boleh_hapus.'</td>
			</tr>
		';
	}
	// ambil pesan dosen
	$kueri = "
	select isi_pesan
	from pesan
	where tipe_pesan='wali' and id_penerima='".$user->ID_PENGGUNA."'
	";
	$pesan_dosen = "";
	$result = $db->Query($kueri)or die("salah kueri 91");
	while($r = $db->FetchRow()) {
		$pesan_dosen = $r[0];
	}
	$isi .= '
	<tr>
		<td colspan="8"><b>Pesan Dosen :</b><br>'.$pesan_dosen.'</td>
	</tr>
	</table>
	</form>
	';
	echo $isi;

}else if($_POST["aksi"]=="hapus" and $_POST["pengambilan_mk"]) {
	$lanjut = true;
	if(harusAngka($_POST["pengambilan_mk"])) {
		$id_pengambilan_mk = $_POST["pengambilan_mk"];
	}else{
		$pesan .= "Illegal Character\n";
		$id_pengambilan_mk = '0';
		$lanjut = false;
	}


	// ambil id_kelas
	$id_kelas="";
	$kueri = "select id_kelas_mk from pengambilan_mk where id_pengambilan_mk='".$id_pengambilan_mk."'";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$id_kelas = $r[0];
	}
	
	if($status_krs=='1') {
		$kueri = "delete from pengambilan_mk where status_apv_pengambilan_mk='0' and id_pengambilan_mk='".$id_pengambilan_mk."' and id_mhs = {$id_mhs}";
		$result =  $db->Query($kueri)or die("salah kueri 73 ");

		if ($result)
		{
				echo 'hapus berhasil';		
		}else{
			echo 'hapus gagal';
		}
	}
}

?>