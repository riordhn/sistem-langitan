<?php
require('../../../config.php');
require('../includes/ceking2.php');
require('../includes/ceking3.php');

if($_POST["aksi"]=="tampil") {
	$db2 = new MyOracle();
	$bulan = array('01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'November', '12'=>'Desember' );
	$isi = '
	<div>
		Data kegiatan bulan : <b>'.$bulan[date("m")].' '.date("Y").'</b>
		<table cellpadding=5 cellspacing=0 border=1>
		<tr>
			<td align=center><b>Urut</b></td>
			<td align=center><b>Jenis</b></td>
			<td align=center><b>Kegiatan</b></td>
			<td align=center><b>Jumlah dalam 1 bulan</b></td>
			<td align=center>&nbsp;</td>
		</tr>
		';
		$kueri = "select a.id_bidikmisi_kegiatan, to_char(a.tgl_posting,'DD-MM-YYYY'), a.jenis_kegiatan, a.kegiatan, a.jumlah_kegiatan
		from aucc.bidikmisi_kegiatan a, aucc.mahasiswa b 
		where a.id_mhs=b.id_mhs and b.id_pengguna='".$user->ID_PENGGUNA."' and to_char(a.tgl_posting, 'MMYYYY')='".date("mY")."'
		order by a.tgl_posting asc";
		$result = $db->Query($kueri)or die("salah kueri 69 : ");
		$urut=0;
		while($r = $db->FetchRow()) {
			$urut++;
			
			$isi .= '
			<tr id="tampil_'.$r[0].'">
				<td>'.$urut.'</td>
				<td>'.$r[2].'</td>
				<td>'.$r[3].'</td>
				<td align=center>'.$r[4].' x</td>
				<td align=center>[<a onclick="edit_data(\''.$r[0].'\')" style="color:blue;">Edit</a>] [<a onclick="if(confirm(\'Yakin Hapus ?\')!=0){ hapus_data('.$r[0].'); }" style="color:blue;" >Hapus</a>]</td>
			</tr>
			<tr id="edit_'.$r[0].'" style="display:none;">
				<td>'.$urut.'</td>
				<td>
				<select name="jenis" id="jenis_'.$r[0].'">
					<option value="INTERNAL UA" '; if($r[2]=='INTERNAL UA'){ $isi .= 'selected'; } $isi .= '>Internal UA</option>
					<option value="EKSTERNAL UA" '; if($r[2]=='EKSTERNAL UA'){ $isi .= 'selected'; } $isi .= '>Eksternal UA</option>
				</select>
				</td>
				<td><input type="text" name="kegiatan_'.$r[0].'" value="'.$r[3].'" id="kegiatan_'.$r[0].'"></td>
				<td align="center"><input type="text" name="jumlah_'.$r[0].'" value="'.$r[4].'" id="jumlah_'.$r[0].'" size="5"></td>
				<td align=center>
					<input type=button name="tambah_baru" value="Simpan" onclick="edit_simpan(\''.$r[0].'\')">
					<input type=button name="balik" value="Batal" onclick="edit_batal(\''.$r[0].'\')">
				</td>
			</tr>
			';
		}
		$isi .= '
		<tr id="baru">
			<td>&nbsp;</td>
			<td>
				<select name="jenis" id="baru_jenis">
					<option value="INTERNAL UA">Internal UA</option>
					<option value="EKSTERNAL UA">Eksternal UA</option>
				</select>
			</td>
			<td><input type=text name="baru_kegiatan" id="baru_kegiatan"></td>
			<td align="center"><input type="text" name="baru_jumlah" id="baru_jumlah" size="5"></td>
			<td><input type=button name="tambah_baru" value="Tambah" onclick="tambah_data()"></td>
		</tr>
		</table>
		</div>
	';

	echo $isi;
}
else if($_POST["aksi"]=="simpanbaru") {
	$jenis = semua_boleh_kecuali2($_POST["jenis"]);
	$jumlah = Angka($_POST["jumlah"]);
	$kegiatan = semua_boleh_kecuali2($_POST["kegiatan"]);

	// ambil idmhs
	$id_mhs = "";
	$kueri = "select a.id_mhs from aucc.mahasiswa a, aucc.pengguna b where a.id_pengguna=b.id_pengguna and b.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 425 : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

	$kueri = "insert into aucc.bidikmisi_kegiatan (jenis_kegiatan, kegiatan, tgl_posting, id_mhs, jumlah_kegiatan) values ('".$jenis."', '".$kegiatan."', to_date('".date("d-m-Y")."', 'DD-MM-YYYY'), ".$id_mhs.", ".$jumlah.") ";
	$result =  $db->Query($kueri)or die("...");
	if($result) {
		echo 'Input berhasil';
	}else{
		echo 'Update gagal';
	}
}
else if($_POST["aksi"]=="simpanedit") {
	$jenis = semua_boleh_kecuali2($_POST["jenis"]);
	$jumlah = Angka($_POST["jumlah"]);
	$kegiatan = semua_boleh_kecuali2($_POST["kegiatan"]);
	$diedit = Angka($_POST["diedit"]);

	// ambil idmhs
	$id_mhs = "";
	$kueri = "select a.id_mhs from aucc.mahasiswa a, aucc.pengguna b where a.id_pengguna=b.id_pengguna and b.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 425 : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

	$kueri = "update aucc.bidikmisi_kegiatan set jenis_kegiatan='".$jenis."', kegiatan='".$kegiatan."', jumlah_kegiatan=".$jumlah." where id_bidikmisi_kegiatan='".$diedit."' and id_mhs='".$id_mhs."'";
	$result =  $db->Query($kueri)or die($kueri);
	if($result) {
		echo 'Update berhasil';
	}else{
		echo 'Update gagal';
	}
}
else if($_POST["aksi"]=="hapus") {
	$dihapus = Angka($_POST["dihapus"]);

	// ambil idmhs
	$id_mhs = "";
	$kueri = "select a.id_mhs from aucc.mahasiswa a, aucc.pengguna b where a.id_pengguna=b.id_pengguna and b.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 425 : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

	$kueri = "delete aucc.bidikmisi_kegiatan where id_bidikmisi_kegiatan='".$dihapus."' and id_mhs='".$id_mhs."'";
	$result =  $db->Query($kueri)or die($kueri);
	if($result) {
		echo 'Hapus berhasil';
	}else{
		echo 'Hapus gagal';
	}
}


?>