<?php

class info_buku {

    public $database;

    function __construct($database) {
        $this->database = $database;
    }

    function condition_tempat($tempat) {
        if ($tempat != '') {
            $query = "and tk.kode_tempat like '%{$tempat}%'";
        } else {
            $query = "";
        }
        return $query;
    }

    function contition_kriteria($kriteria, $kunci) {
        if ($kriteria == 'judul') {
            $query = "and p.judul like '%{$kunci}%'";
        } else if ($kriteria == 'pengarang') {
            $query = "and p.entri_utama like '%{$kunci}%'";
        } else if ($kriteria == 'subjek') {
            $query = "and p.subjek like '%{$kunci}%'";
        }
        return $query;
    }

    function get_pencarian_buku($kode_register) {
        $query = mysql_query("
            select p.*,tk.keterangan from pengolahan p
            inner join tempat_koleksi tk on p.kode_tempat = tk.kode_tempat
            where p.kode_register='{$kode_register}'");
        return mysql_fetch_assoc($query);
    }

    function load_pencarian_buku($kriteria, $kunci, $tempat) {
        $data_hasil_pencarian = array();
        $condition_tempat = $this->condition_tempat($tempat);
        $condition_kriteria = $this->contition_kriteria($kriteria, $kunci);
        $query = mysql_query("
            select p.*,tk.keterangan from pengolahan p
            inner join tempat_koleksi tk on p.kode_tempat = tk.kode_tempat
            where tk.keterangan!='' and tk.keterangan!='---' and p.kode_jenis='01' {$condition_kriteria} {$condition_tempat}
            group by p.ddc1,p.ddc2,p.kode_register
            order by p.judul");
        while ($temp = mysql_fetch_array($query)) {
            array_push($data_hasil_pencarian, $temp);
        }

        return $data_hasil_pencarian;
    }

    function load_list_journal() {
        $data_list_journal = array();
        $query = mysql_query("
            select tk.kode_tempat,tk.keterangan,count(distinct(p.kode_register)) as jumlah from pengolahan p
            inner join tempat_koleksi tk on p.kode_tempat = tk.kode_tempat
            where tk.keterangan!='' and tk.keterangan!='---' and p.ddc1 is not null and p.ddc2 is not null and kode_jenis='02'
            group by tk.kode_tempat,tk.keterangan");
        while ($temp = mysql_fetch_array($query)) {
            array_push($data_list_journal, $temp);
        }

        return $data_list_journal;
    }
    
    function load_data_journal($tempat){
        $data_journal = array();
        $condition_tempat = $this->condition_tempat($tempat);
        $query = mysql_query("
            select p.*,tk.keterangan from pengolahan p
            inner join tempat_koleksi tk on p.kode_tempat = tk.kode_tempat
            where tk.keterangan!='' and tk.keterangan!='---' and p.kode_jenis='02' and p.ddc1 is not null and p.ddc2 is not null {$condition_tempat}
            group by p.kode_register
            order by p.judul");
        while ($temp = mysql_fetch_array($query)) {
            array_push($data_journal, $temp);
        }

        return $data_journal;
    }

}

?>
