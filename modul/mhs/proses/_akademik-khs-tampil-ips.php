<?php
include('../../../config.php');

if ($user->IsLogged())
{
	# query diganti seperti penghitungan IPS di halaman _akademik-khs-tampil.php by putra rieskha
	/**
	$ips_set = $db->QueryToArray(
		"SELECT 
			a.id_semester, s.thn_akademik_semester, s.nm_semester,
			ROUND( NVL(SUM(C.KREDIT_SEMESTER*(SELECT nilai_standar_nilai from standar_nilai sn where sn.nm_standar_nilai = a.nilai_huruf)) / sum(C.KREDIT_SEMESTER), 0), 2) IPS
		FROM PENGAMBILAN_MK A
		LEFT JOIN KELAS_MK B ON (A.ID_KELAS_MK=B.ID_KELAS_MK)
		LEFT JOIN KURIKULUM_MK C ON (B.ID_KURIKULUM_MK=C.ID_KURIKULUM_MK)
		LEFT JOIN MATA_KULIAH D ON (D.ID_MATA_KULIAH=C.ID_MATA_KULIAH)
		join semester s on s.id_semester = a.id_semester
		WHERE A.ID_MHS={$user->ID_MHS} AND A.FLAGNILAI=1 AND NOT (d.status_praktikum=2 and a.nilai_huruf='E')
		group by a.id_semester, s.thn_akademik_semester, s.nm_semester
		order by s.thn_akademik_semester asc, s.nm_semester asc");
	*/
	
	/*$sql = "SELECT ID_SEMESTER,THN_AKADEMIK_SEMESTER,NM_SEMESTER,
			case when sum(kredit_semester) = 0 then 0
				 else round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips
			from 
			(
			select kredit_semester,bobot,id_semester,THN_AKADEMIK_SEMESTER,NM_SEMESTER
			from(
			select pengambilan_mk.ID_SEMESTER,pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,semester.THN_AKADEMIK_SEMESTER,SEMESTER.NM_SEMESTER,
			case when kurikulum_mk.status_mkta = '1' 
			and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas in (7,12) then 0 else kurikulum_mk.kredit_semester end as kredit_semester, 
			case when kurikulum_mk.status_mkta = '1' 
			and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
			when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
			case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
			coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
			nilai_angka,a.besar_nilai_mk as UTS,b.besar_nilai_mk as UAS,
			row_number() over(partition by pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,pengambilan_mk.id_kelas_mk order by nilai_huruf) rangking
			from PENGAMBILAN_MK
			left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
			left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
			left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
			left join semester on pengambilan_mk.id_semester=semester.id_semester
			left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
			left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
			left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
			left join nilai_mk a on pengambilan_mk.id_pengambilan_mk=a.id_pengambilan_mk and a.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UTS')
			left join nilai_mk b on pengambilan_mk.id_pengambilan_mk=b.id_pengambilan_mk and b.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UAS')
			where  tipe_semester in ('UP','REG','RD') and status_apv_pengambilan_mk = 1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0 and flagnilai='1'
			and mahasiswa.id_mhs= '".$user->ID_MHS."') 
			where rangking = 1
			) group by id_semester,THN_AKADEMIK_SEMESTER,nm_semester
			order by THN_AKADEMIK_SEMESTER asc,nm_semester asc ";*/

	$sql = "SELECT id_mhs, thn_akademik_semester, nm_semester, SUM(sks) AS total_sks, round(SUM(nilai_mutu) / sum(sks), 2) as ips from (
			    SELECT
			        mhs.id_mhs,
			        s.thn_akademik_semester,
			        s.nm_semester,
			        /* Kode MK utk grouping total mutu */
			        COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
			        /* SKS */
			        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
			        /* Nilai Mutu = SKS * Bobot */
			        COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu
			    FROM pengambilan_mk pmk
			    JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
			    JOIN semester S ON S.id_semester = pmk.id_semester
			    -- Via Kelas
			    LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
			    LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
			    -- Via Kurikulum
			    LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
			    LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
			    -- nilai bobot
			    JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
			    WHERE 
			        pmk.status_apv_pengambilan_mk = 1 AND 
			        mhs.id_mhs = '{$user->ID_MHS}'
			)
			group by id_mhs, thn_akademik_semester, nm_semester
			order by thn_akademik_semester asc,nm_semester asc";

	$ips_set = $db->QueryToArray($sql);

	$rows = array();
	foreach ($ips_set as $ips)
	{
		array_push($rows, "['{$ips['THN_AKADEMIK_SEMESTER']} {$ips['NM_SEMESTER']}', {$ips['IPS']}]");
	}
	$json_rows = "[".implode(",", $rows)."]";
}
?>

<!--
You are free to copy and use this sample in accordance with the terms of the
Apache license (http://www.apache.org/licenses/LICENSE-2.0.html)
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<script type="text/javascript" src="//www.google.com/jsapi"></script>
	<script type="text/javascript">
	  google.load('visualization', '1.0', {packages: ['corechart']});
	</script>
	<script type="text/javascript">
	 function placeMarker(dataTable) {
        var cli = this.getChartLayoutInterface();
        var chartArea = cli.getChartAreaBoundingBox();
        // "Zombies" is element #5.
        document.querySelector('.overlay-marker').style.top = Math.floor(cli.getYLocation(dataTable.getValue(5, 1))) - 50 + "px";
        document.querySelector('.overlay-marker').style.left = Math.floor(cli.getXLocation(5)) - 10 + "px";
      };

	  function drawVisualization() {
		// Create and populate the data table.
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'x');
		data.addColumn('number', 'IPS');
		data.addRows(<?php echo $json_rows; ?>);
	   


		// Create and draw the visualization.
		var chart = new google.visualization.LineChart(document.getElementById('visualization')).
			draw(data, {curveType: "function",
						width: 600, height: 400,
						vAxis: {viewWindow: {min:0, max:4}},
						title: 'Grafik IPS',
						pointSize: 15,
          				pointShape: { type: 'circle', sides: 7, dent: 0.3 },
          				colors: ['#003D00']
						}
				);
		
	  }
	  

	  google.setOnLoadCallback(drawVisualization);
	</script>
  </head>
  <body style="font-family: Arial;border: 0 none; margin: 0px; padding: 0px;">
	<div id="visualization" style="width: 600px; height: 400px;"></div>
   
  </body>
</html>