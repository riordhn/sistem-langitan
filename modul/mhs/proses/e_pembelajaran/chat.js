/* 
Created by: Kenrick Beckett

Name: Chat Engine
*/

var instanse = false;
var state;
var mes;
var file;

function Chat () {
    this.update = updateChat;
    this.send = sendChat;
	this.getState = getStateOfChat;
}

//gets the state of the chat
function getStateOfChat(){
	if(!instanse){
		 instanse = true;
		 $.ajax({
			   type: "POST",
			   url: "process.php",
			   data: {  
			   			'function': 'getState',
						'file': file
						},
			   dataType: "json",
			
			   success: function(data){
				   //state = data.state;
				   //alert(state);
				   state = 0;
				   instanse = false;
			   },
			});
	}	 
}

//Updates the chat
function updateChat(){
	 if(!instanse){
		 instanse = true;
	     $.ajax({
			   type: "POST",
			   url: "process.php",
			   data: {  
			   			'function': 'update',
						'state': state,
						'file': file
						},
			   dataType: "json",
			   success: function(data){
				   if(data.text){
						$('#chat-area').html("");
						for (var i = 0; i < data.text.length; i++) {
                            $('#chat-area').append($("<div style=\"padding-top:0px; padding-bottom:0px;\">"+ data.text[i] +"</div>"));
                        }
						$("#chat-area").each( function() 
						{
						   // certain browsers have a bug such that scrollHeight is too small
						   // when content does not fill the client area of the element
						   var scrollHeight = Math.max(this.scrollHeight, this.clientHeight);
						   this.scrollTop = scrollHeight - this.clientHeight;
						});
				   }
				  //document.getElementById('chat-area').scrollTop = document.getElementById('chat-area').scrollHeight;
				   instanse = false;
				   state = data.state;
			   },
			});
	 }
	 else {
		 setTimeout(updateChat, 1500);
	 }
}

//send the message
function sendChat(message, nickname)
{       
    updateChat();
     $.ajax({
		   type: "POST",
		   url: "process.php",
		   data: {  
		   			'function': 'send',
					'message': message,
					'nickname': nickname,
					'file': file
				 },
		   dataType: "json",
		   success: function(data){
				updateChat();
		   },
		});
}
