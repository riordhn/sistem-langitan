<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}

	/**
	 * Khusus Psikologi Unair, tidak digunakan di langitan
	if ($user->ID_FAKULTAS == 11)
	{
		echo '
		<script>
		location.href="#akademik-transkrip!akademik-transkrip-psi.php";
		</script>';
		exit;
	}
	*/

$db2 = new MyOracle();

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {

// Yudi Sulistya, 02 Aug 2013 (tablesorter)
echo '
<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]]
		}
	);
}
);
</script>
';

	$id_fakul_user	= $user->MAHASISWA->ID_FAKULTAS;
	$id_pt_user 	= $user->ID_PERGURUAN_TINGGI;

	// ambil nama di tabel pengguna
	$mhs_nama = "";
	 $kueri = "select NM_PENGGUNA from pengguna where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ");
	while($r = $db->FetchRow()) {
		$mhs_nama = $r[0];
	}

	// ambil id_mhs
	$id_mhs=""; $mhs_nim="";
	$kueri = "
	select a.id_mhs, a.NIM_MHS, 
	case when c.id_fakultas=9 then 'PROGRAM '||c.nm_fakultas else 'FAKULTAS '||c.nm_fakultas end, 
	b.nm_program_studi, d.nm_jenjang, a.TGL_LULUS_MHS, a.TGL_TERDAFTAR_MHS, a.no_ijazah, a.id_program_studi
	from mahasiswa a, program_studi b, fakultas c, jenjang d
	where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and b.id_jenjang=d.id_jenjang and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
		$mhs_nim = $r[1];
		$mhs_fakul = $r[2];
		$mhs_prodi = $r[3];
		$mhs_jenjang = $r[4];
		$mhs_tgl_lulus = $r[5];
		$mhs_tgl_terdaftar = $r[6];
		$mhs_noijazah = $r[7];
		$mhs_prodi_id = $r[8];
	}

	$jum_sks=0; $jum_bobot=0; $ipk=0;
	// data lama 
//farid revisi d.kredit_semester->c.kredit_semester
	/*
	echo  $kueri = "
	select a.id_semester,c.kd_mata_kuliah,c.nm_mata_kuliah,c.kredit_semester,a.nilai_huruf, a.flagnilai
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1'
	order by a.id_semester,c.kd_mata_kuliah
	";
	// pakai kueri ini untuk mengambil nilai terbaik
		/*
		select c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,Min(a.nilai_huruf)
		from aucc.pengambilan_mk a, aucc.mata_kuliah c, aucc.kurikulum_mk d
		where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah 
		and a.id_mhs='10383' and d.id_program_studi='66'
		group by c.kd_mata_kuliah, c.nm_mata_kuliah, d.kredit_semester
		order by c.kd_mata_kuliah
		*/
	$result = $db->Query($kueri)or die("salah kueri : ");
	while($r = $db->FetchRow()) {
		if($r[5]=='1') { // fileter FLAGNILAI
			// ambil bobot nilai huruf
			$bobot=0;
			$kueri2 = "select NILAI_STANDAR_NILAI from standar_nilai where NM_STANDAR_NILAI='".$r[4]."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ");
			while($r2 = $db2->FetchRow()) {
				$bobot = $r2[0];
			}
			$jum_sks += $r[3];
			$jum_bobot += ($bobot*$r[3]);
		}
	}
	/*
	// data baru
/*
		$kueri = "
		select a.id_semester,c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf
		from pengambilan_mk a, kelas_mk b, mata_kuliah c, kurikulum_mk d
		where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1' and (a.STATUS_PENGAMBILAN_MK!='4' or a.STATUS_PENGAMBILAN_MK is null) and a.nilai_huruf is not null
		order by a.id_semester,c.kd_mata_kuliah
		";
		$result = $db->Query($kueri)or die("salah kueri : ");
		while($r = $db->FetchRow()) {
			// ambil bobot nilai huruf
			$bobot=0;
			$kueri2 = "select NILAI_STANDAR_NILAI from standar_nilai where NM_STANDAR_NILAI='".$r[4]."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ");
			while($r2 = $db2->FetchRow()) {
				$bobot = $r2[0];
			}
			$jum_sks += $r[3];
			$jum_bobot += ($bobot*$r[3]);
		}

	if($jum_sks==0) {
		$ipk='0.00';
	}else{
		$ipk = number_format(($jum_bobot/$jum_sks),2);
	}
*/
// ipk versi lukman, ngulang diambil nilai terbaik.
// line 130 d.kredit -> e.kredit
	$sql="select a.id_mhs,sum(a.kredit_semester) skstotal,
round(sum(a.kredit_semester*(case a.nilai_huruf 
when 'A' then 4 
when 'AB' then 3.5 
when 'B' then 3
when 'BC' then 2.5
when 'C' then 2
when 'D' then 1
end))/sum(a.kredit_semester),2) IPK
from
(
select a.id_mhs,e.nm_mata_kuliah,e.kredit_semester kredit_semester,a.nilai_huruf from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null and a.flagnilai=1 
and a.status_hapus=0 and a.flagnilai='1'
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where rangking=1 and id_mhs='{$id_mhs}'
) a
left join mahasiswa b on a.id_mhs=b.id_mhs
left join program_studi f on b.id_program_studi=f.id_program_studi
where a.id_mhs='{$id_mhs}'
group by a.id_mhs order by a.id_mhs";

// ngulang -> nilai terakhir yg berlaku, dimatikan dulu, UNU tidak ada yg menggunakan nilai terakhir
/**
if($user->ID_FAKULTAS==10){
$sql="select a.id_mhs,sum(a.kredit_semester) skstotal,
round(sum(a.kredit_semester*(case a.nilai_huruf 
when 'A' then 4 
when 'AB' then 3.5 
when 'B' then 3
when 'BC' then 2.5
when 'C' then 2
when 'D' then 1
end))/sum(a.kredit_semester),2) IPK
from
(
select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester desc,sm.nm_semester desc) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join semester sm on sm.id_semester=a.id_semester
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.flagnilai=1 
and a.id_semester is not null and a.status_hapus=0 and a.flagnilai='1'
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where rangking=1 and id_mhs='{$id_mhs}'
) a
left join mahasiswa b on a.id_mhs=b.id_mhs
left join program_studi f on b.id_program_studi=f.id_program_studi
where a.id_mhs='{$id_mhs}'
group by a.id_mhs order by a.id_mhs";

}
*/

$result2 = $db2->Query($sql);
			while($r2 = $db2->FetchRow()) {
				$jum_sks = $r2[1];
				$ipk = $r2[2];
			}

			// Tambahan
			/*$judulfk = "Histori Nilai  ";

			$depan = time();
			$belakang = strrev(time());

			$link = 'proses/_akademik-history-cetak2.php?nim=' . $nim_mhs . '&id='; //. $idsemes;
			

			$isi = '
		<h2>' . $judulfk . '</h2>
		<a style="padding:5px" class="ui-button ui-state-default ui-corner-all disable-ajax" onclick="window.open(\'' . $link . '\');"><b>Cetak</b></a>
		
		
		<br />
		<table width="850" id="myTable" class="tablesorter">
		<thead>
		<tr>
			<th>Mata Kuliah</th>
			<th>SKS</th>
			<th>Nilai</th>
		</tr>
		</thead>
		<tbody>
		';*/

		// Akhir Dari Tambahan Histori Nilai


	$isi_transkrip = '
		<table cellspacing="0" cellpadding="0" border="0" width="100%" class="tb_frame">
		<tr>
			<td align=center><img src="'.$base_url.'../../img/akademik_images/logo-'.$nama_singkat.'.gif" width="100" height="100" border="0" alt=""></td>
			<td>
			<!-- aa -->
			'.strtoupper($nama_pt).'<BR>
			'.strtoupper($mhs_fakul).'<BR>
			'.$alamat.'<br>
			Telp. '.$telp_pt.' Fax '.$fax_pt.'<br>
			Website : http://www.'.$nama_singkat.'.ac.id; e-mail:rektor@'.$nama_singkat.'.ac.id
			</td>
		</tr>
		</table>
		<table cellspacing="0" cellpadding="0" border="0" width="100%" class="tb_frame">
		<tbody>
		<tr>
			<td>Nama</td>
			<td><strong>: '.$mhs_nama.'</strong></td>
			<td>&nbsp;</td>
			<td>Nomor Ijazah</td>
			<td><strong>: '.$mhs_noijazah.'</strong></td>
		</tr>
		<tr>
			<td>NIM</td>
			<td><strong>: '.$mhs_nim.'</strong></td>
			<td>&nbsp;</td>
			<td>Jumlah SKS</td>
			<td><strong>: '.$jum_sks.'</strong></td>
		 </tr>
		<tr>
			<td>Program Studi</td>
			<td><strong>: '.$mhs_jenjang.' - '.$mhs_prodi.'</strong></td>
			<td>&nbsp;</td>
			<td>IP Komulatif</td>
			<td><strong>: '.$ipk.'</strong></td>
		</tr>
		<tr>
			<td width="32%">Tanggal Terdaftar Pertama Kali</td>
			<td width="25%"><strong>: '.$mhs_tgl_terdaftar.'</strong></td>
			<td width="7%">&nbsp;</td>
			<td width="16%">Tanggal Lulus</td>
			<td width="20%"><strong>: '.$mhs_tgl_lulus.'</strong></td>
		</tr>
		<a style="padding:5px" class="ui-button ui-state-default ui-corner-all disable-ajax" onclick="window.open(\'' . $link . '\');"><b>Cetak</b></a>
		
		
		<br />
		</tbody>
		</table>
		<br>

		



		<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable" class="tablesorter">
		<thead>
		<tr>
			<th width="80px">Semester</th>
			<th>Kode MA</th>
			<th>Nama Mata Ajar</th>
			<th>SKS</th>
			<th>Nilai</th>
			<th>Bobot</th>
		</tr>
		</thead>
		<tbody>
	';
	$jum_sks=0; $jum_bobot=0; $ipk=0;
	// query sebelum di tata
	$kueri = "SELECT e.tahun_ajaran, e.nm_semester, c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf asc,e.thn_akademik_semester desc,e.nm_semester desc) rangking,count(*) over(partition by c.nm_mata_kuliah) terulang,c.status_praktikum,a.status_hapus, e.group_semester
	from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e
	where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1'
	";

	// query di tata 22-01-2017
	// query ini DEPRECATED, akan dipindah seperti model feeder
	$kueri = 
		"SELECT 
			s.tahun_ajaran, s.nm_semester, mk.kd_mata_kuliah, mk.nm_mata_kuliah, kmk.kredit_semester, pmk.nilai_huruf, pmk.flagnilai,
			row_number() OVER(PARTITION BY pmk.id_mhs,mk.nm_mata_kuliah ORDER BY pmk.nilai_huruf ASC,s.thn_akademik_semester DESC,s.nm_semester DESC) rangking,
			count(*) OVER(PARTITION BY mk.nm_mata_kuliah) terulang,
			mk.status_praktikum, pmk.status_hapus, s.group_semester
		FROM pengambilan_mk pmk
		JOIN semester s ON s.id_semester = pmk.id_semester
		JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
		JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
		WHERE pmk.id_mhs = {$id_mhs} AND pmk.status_apv_pengambilan_mk = 1";

	// khusus non-fikes umaha
	if($id_pt_user == 1 && $id_fakul_user != 1){
		// query di tata 22-01-2017
		// query ini DEPRECATED, akan dipindah seperti model feeder
		$kueri = 
			"SELECT 
				s.tahun_ajaran, s.nm_semester, mk.kd_mata_kuliah, mk.nm_mata_kuliah, kmk.kredit_semester, pmk.nilai_huruf, pmk.flagnilai,
				row_number() OVER(PARTITION BY pmk.id_mhs,mk.nm_mata_kuliah ORDER BY pmk.nilai_huruf ASC,s.thn_akademik_semester DESC,s.nm_semester DESC) rangking,
				count(*) OVER(PARTITION BY mk.nm_mata_kuliah) terulang,
				mk.status_praktikum, pmk.status_hapus, s.group_semester, kk.is_boleh_cetak, pmk.id_semester
			FROM pengambilan_mk pmk
			JOIN semester s ON s.id_semester = pmk.id_semester
			JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
			LEFT JOIN keuangan_khs kk ON kk.id_semester = s.id_semester and kk.id_mhs = pmk.id_mhs
			WHERE pmk.id_mhs = {$id_mhs} AND pmk.status_apv_pengambilan_mk = 1";
	}

	// Jika UNINUS maka cara baca query di bedakan
	if ($user->ID_PERGURUAN_TINGGI == 11 || $user->ID_PERGURUAN_TINGGI == 10)
	{
		$kueri =
			"SELECT 
				s.tahun_ajaran, s.nm_semester, mk.kd_mata_kuliah, mk.nm_mata_kuliah, kls.kredit_semester, pmk.nilai_huruf, pmk.flagnilai,
				row_number() OVER(PARTITION BY pmk.id_mhs,mk.nm_mata_kuliah ORDER BY pmk.nilai_huruf ASC,s.thn_akademik_semester DESC,s.nm_semester DESC) rangking,
				count(*) OVER(PARTITION BY mk.nm_mata_kuliah) terulang,
				mk.status_praktikum, pmk.status_hapus, s.group_semester
			FROM pengambilan_mk pmk
			JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
			JOIN semester s ON s.id_semester = kls.id_semester
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kls.id_mata_kuliah
			WHERE pmk.id_mhs = {$id_mhs} AND pmk.status_apv_pengambilan_mk = 1";
	}

	$result = $db->Query($kueri)or die("salah kueri1 : ");
	while($r = $db->FetchRow()) {
		$semester = $r[0].' '.$r[11].' - '.$r[1];
		if($r[6]=='1') { // filter FLAGNILAI
		
			// ambil bobot nilai huruf
			$bobot=0;
			$kueri2 = "select NILAI_STANDAR_NILAI from standar_nilai where NM_STANDAR_NILAI='".trim($r[5])."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ");
			while($r2 = $db2->FetchRow()) {
				$bobot = $r2[0];
			}
			if($r[7]==1 and $r[8]>1)$warna='#00FF00';
			else {
				if($r[7]>1)$warna='#FF0000';
				else {
					if($r[5]=='-')$warna='yellow';
					$warna='#FFFFFF';
				}
			}
			if(($r[5]=='E' or $r[5]=='') and ($r[9]>1) ){
				$warna='#cccccc';
			}
			if($r[10]=='1' ){
				$warna='#772244';
			}

			// status khusus no-fikes umaha (pembatasan keuangan)
			// 0 = boleh tampil; 1 = tidak boleh tampil;
			// FIKRIE (15-08-2017)
			$status_sem_khs = '0';
			if($id_pt_user == 1 and $fakul != 1){
				if($r[0] == 2016){
					if($r[1] == 'Genap'){
						$status_sem_khs = 1;
					}
				}
				else if ($r[0] > 2016){
					$status_sem_khs = 1;
				}
			}

			if($status_sem_khs == 1 and $r[12] == 1){
				$status_sem_khs = 0;
			}	

			if($status_sem_khs == 0){
				$isi_transkrip .= '
					<tr bgcolor="'.$warna.'">
						<td>'.$semester.'</td>
						<td>'.$r[2].'</td>
						<td>'.$r[3].'</td>
						<td align="center">'.$r[4].'</td>
						<td align="center">'.$r[5].'</td>
						<td align="center">'.($bobot*$r[4]).'</td>
					</tr>
				';
				if($r[5]<'E' and $r[5]!='' and $r[5]!='-' and $r[7]==1 and $r[10]==0 ){
					$jum_sks += $r[4];
					$jum_bobot += ($bobot*$r[4]);
				}
			}
		}else{
			// status khusus no-fikes umaha (pembatasan keuangan)
			// 0 = boleh tampil; 1 = tidak boleh tampil;
			// FIKRIE (15-08-2017)
			$status_sem_khs = '0';
			if($id_pt_user == 1 and $fakul != 1){
				if($r[0] == 2016){
					if($r[1] == 'Genap'){
						$status_sem_khs = 1;
					}
				}
				else if ($r[0] > 2016){
					$status_sem_khs = 1;
				}
			}

			if($status_sem_khs == 1 and $r[12] == 1){
				$status_sem_khs = 0;
			}	

			if($status_sem_khs == 0){
				if($r[10]=='1' ){
					$warna='#772244';
				}else{
					$warna='yellow';
				}
				$isi_transkrip .= '
					<tr bgcolor="'.$warna.'">
						<td> '.$semester.'</td>
						<td>'.$r[2].'</td>
						<td>'.$r[3].'</td>
						<td align="center">'.$r[4].'</td>
						<td align="center">-</td>
						<td align="center">-</td>
					</tr>
				';
			}
		}
	}

	/*
	// data baru
		$kueri = "
		select a.id_semester,c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf
		from pengambilan_mk a, kelas_mk b, mata_kuliah c, kurikulum_mk d
		where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_mhs='".$id_mhs."' and a.STATUS_APV_PENGAMBILAN_MK='1' and a.STATUS_PENGAMBILAN_MK!='4' and a.nilai_huruf is not null
		order by a.id_semester,c.kd_mata_kuliah
		";
		$result = $db->Query($kueri)or die("salah kueri : ");
		while($r = $db->FetchRow()) {
			// ambil nama semes
			$kueri2 = "select tahun_ajaran,nm_semester from semester where id_semester='".$r[0]."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ");
			while($r2 = $db2->FetchRow()) {
				$semester = $r2[0].' '.$r2[1];
			}
			// ambil bobot nilai huruf
			$bobot=0;
			$kueri2 = "select NILAI_STANDAR_NILAI from standar_nilai where NM_STANDAR_NILAI='".$r[4]."'";
			$result2 = $db2->Query($kueri2)or die("salah kueri : ");
			while($r2 = $db2->FetchRow()) {
				$bobot = $r2[0];
			}
			$isi_transkrip .= '
				<tr>
					<td>'.$semester.'</td>
					<td>'.$r[1].'</td>
					<td>'.$r[2].'</td>
					<td align="center">'.$r[3].'</td>
					<td align="center">'.$r[4].'</td>
					<td align="center">'.($bobot*$r[3]).'</td>
				</tr>
			';
			$jum_sks += $r[3];
			$jum_bobot += ($bobot*$r[3]);
		}
	*/
	if($jum_sks==0) {
		$ipk='0.00';
	}else{
		$ipk = number_format(($jum_bobot/$jum_sks),2);
	}
	
	$isi_transkrip .= '
		</tbody>
		<tr>
			<td rowspan="3" colspan="2"></td>
			<td><div align="left">Jumlah SKS dan Bobot</div></td>
			<td align="center">'.$jum_sks.'</td>
			<td align="center">&nbsp;</td>
			<td align="center">'.$jum_bobot.'</td>
		</tr>
		<tr>
			<td>IP komulatif</td>
			<td colspan="3"><div align="center">'.$ipk.'</div></td>
		</tr>
		<tr>
			<td>Predikat Kelulusan</td>
			<td colspan="3"><div align="center">&nbsp;</div></td>
		</tr>
		</table>
		Keterangan: <br>
		<span>Putih</span> : Normal, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif<br>
		<span style="background-color:green">Hijau</span> : Ulangan, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif<br>
		<span style="background-color:yellow">Kuning</span> : Nilai belum dikeluarkan, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif<br>
		<span style="background-color:red">Merah</span> : Sudah diulang, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif<br>
		<span style="background-color:#cccccc">Abu-abu</span> : PKL,KKN,TA,PROPOSAL,SEMINAR,SKRIPSI,THESIS yang belum masuk nilainya. Tidak masuk perhitungan IPK dan sks kumulatif.<br>
		<span style="background-color:#772244">Ungu</span> : MK Pilihan yang dihapus oleh akademik atas permintaan mahasiswa. Tidak masuk perhitungan IPK dan sks kumulatif.<br>
	';
	//<input type=button name="dicetak" value="Cetak" onclick="window.open(\'proses/_akademik-transkrip_cetak.php\',\'baru2\');">

	$smarty->assign('isitranskrip', $isi_transkrip);

	$smarty->display('akademik-transkrip.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>
