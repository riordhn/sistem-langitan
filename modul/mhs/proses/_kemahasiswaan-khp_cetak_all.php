<?php
require('../../../config.php');

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$title = strtoupper($nama_pt);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Langitan '.$title);
$pdf->SetTitle('Cetak SKKK - '.$title);
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

$logo = "../../../img/akademik_images/logo-".$nama_singkat.".gif";
$logo_size = "25";


// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content
$html = '<table width="100%" align="center" cellpadding="0">
<tr>
	<td>
	<img src="'.$logo.'" width="70" height="70"><br><BR>
	<B>'.$title.'<BR>
';
	// ambil Fakultas
	$kueri = "select c.nm_fakultas from mahasiswa a, program_studi b, fakultas c where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : maintenance");
	while($r = $db->FetchRow()) {
		$fakultas = $r[0];
	}
	// ambil nama mhs
	$kueri = "select nm_pengguna from pengguna where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : maintenance");
	while($r = $db->FetchRow()) {
		$nm_pengguna = $r[0];
	}
	// ambil nim mhs
	$kueri = "select a.nim_mhs, b.nm_program_studi, a.id_mhs from mahasiswa a, program_studi b where a.id_program_studi=b.id_program_studi and a.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri : maintenance");
	while($r = $db->FetchRow()) {
		$nim = $r[0];
		$prodi = $r[1];
		$id_mhs = $r[2];
	}
$html .= '
	FAKULTAS '.strtoupper($fakultas).'<BR>
	SATUAN KREDIT KEGIATAN KEMAHASISWAAN (SKKK)</B>
	</td>
</tr>
</table>
<br>
<table width="120%" cellpadding="0" style="font-size:8x;">
<tr>
	<td width="10%">Nama</td>
	<td width="1%">:</td>
	<td width="39%">'.$nm_pengguna.'</td>
	<td width="10%">Prog.Studi</td>
	<td width="1%">:</td>
	<td width="39%">'.$prodi.'</td>
</tr>
<tr>
	<td>NIM</td>
	<td>:</td>
	<td>'.$nim.'</td>
</tr>	
</table>
<br>
<table width="120%" border="1" cellpadding="2" cellspacing="0" style="font-size:8x;">
<tr>
	<td width="3%" align="center"><b>No</b></td>
	<td align="center"><b>Nama Kegiatan</b></td>
	<td align="center"><b>Kelompok Kegiatan</b></td>
	<td align="center"><b>Penyelenggara</b></td>
	<td align="center"><b>Semester-Tahun</b></td>
	<td align="center"><b>Nilai SKP</b></td>
	<td align="center"><b>Bukti Fisik</b></td>
</tr>
<tr>
	<td align="center">(1)</td>
	<td align="center">(2)</td>
	<td align="center">(3)</td>
	<td align="center">(4)</td>
	<td align="center">(5)</td>
	<td align="center">(6)</td>
</tr>
';
$hit=0;
$kueria = "
select b.nm_krp_khp, b.penyelenggara_krp_khp, e.tahun_ajaran || ' ' || e.nm_semester, b.skor_krp_khp, c.nm_bukti_fisik, f.nm_kelompok_kegiatan
from kegiatan_2 a, krp_khp b, bukti_fisik c, kegiatan_1 d, semester e, kelompok_kegiatan f
where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_bukti_fisik=c.id_bukti_fisik and a.id_kegiatan_1=d.id_kegiatan_1 and b.id_semester=e.id_semester and d.id_kelompok_kegiatan=f.id_kelompok_kegiatan and b.id_mhs='".$id_mhs."'
order by e.thn_akademik_semester
";
$result = $db->Query($kueria)or die("salah kueri : ".$kueria);
	while($r = $db->FetchRow()) {
	$hit++;
	$html .= '
	<tr>
		<td>'.$hit.'</td>
		<td>'.$r[0].'</td>
		<td>'.$r[5].'</td>
		<td>'.$r[1].'</td>
		<td>'.$r[2].'</td>
		<td>'.$r[3].'</td>
		<td>'.$r[4].'</td>
	</tr>
	';
}
$html .= '
</table>
<table width="98%" border="0" cellpadding="2" cellspacing="0" style="font-size:6x;">
<tr>
	<td>
	Catatan : <br>
	- Kolom (6) diisi SK/Sertifikat/Daftar Hadir, dll dan (Nomor Bukti Fisik yang dilampirkan)<br>
	- Baris pada tabel dapat ditambah apabila diperlukan.
	</td>
</tr>
</table>
<br>
<table width="120%" border="0" cellpadding="2" cellspacing="0" style="font-size:8x;">
<tr>
	<td>
		Menyetujui,<br>
		Dosen Wali<br>
		<br><br><br>
		.............................<br>
		NIP : ....................
	</td>
	<td>
		Surabaya, '.date("d-m-Y").'<br>
		<br>
		<br><br><br>
		.............................<br>
		NIM : ....................
	</td>
</tr>
</table>
';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('skkk-'.$nim.'.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+
