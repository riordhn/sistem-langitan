<?php
require('../../../config.php');
require('../includes/ceking2.php');

if ($_POST["aksi"] == "tampil") {
	$isi = '
		<form name="frmkrshapus" id="frmkrshapus">
		<table class="table table-striped">
		<tr>
			<th>No.</th>
			<th>KODE MTA</th>
			<th>NAMA MATA AJAR</th>
			<th>SKS MTA</th>
			<th>KELAS</th>
			<th>STATUS</th>
			<th class="text-center">AKSI</th>
		</tr>
	';

	// ambil id_mhs
	$id_mhs = "";
	$id_mhs = $db->QuerySingle("select id_mhs from mahasiswa where id_pengguna='" . $user->ID_PENGGUNA . "'") or die("salah kueri baris " . __LINE__);


	// ambil semester_aktif
	$sem_aktif = "";
	$sem_aktif = $db->QuerySingle("select id_semester from semester where STATUS_AKTIF_SEMESTER='True' and id_perguruan_tinggi = '{$id_pt_user}' order by id_semester desc");

	$status_krs = $_SESSION['STATUS_KRS'];

	if (empty($id_mhs)) {
		$f = fopen("log/log_id_mhs.txt", "a");
		ob_start();
		echo "------------------------ perbaikan ------------\n";
		$user = new User($db);
		var_dump($user);
		echo "------------------------ perbaikan ------------\n";

		$kueri = "select id_mhs,id_program_studi from mahasiswa where id_pengguna='" . $user->ID_PENGGUNA . "'";
		$result = $db->Query($kueri) or die("salah kueri baris " . __LINE__);

		while ($r = $db->FetchRow()) {
			$id_mhs = $r[0];
		}

		echo "---------------------------------------------------\n\n";

		$s = ob_get_contents();
		fwrite($f, $s);
		fclose($f);
		ob_end_clean();
	}

	$kueri = "
	select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester,d.id_pengambilan_mk,
	c.no_kelas_mk, d.status_apv_pengambilan_mk, d.STATUS_PENGAMBILAN_MK, c.id_kelas_mk
	from pengambilan_mk d 
	join kelas_mk c on c.id_kelas_mk=d.id_kelas_mk
	JOIN kurikulum_mk e on e.id_kurikulum_mk=c.id_kurikulum_mk 
	JOIN mata_kuliah a on a.id_mata_kuliah=e.id_mata_kuliah 
	and d.id_semester='" . $sem_aktif . "' 
	and d.id_mhs='" . $id_mhs . "'
	order by a.nm_mata_kuliah
	";

	$result_mk = $db->QueryToArray($kueri);
	$hit = 0;
	foreach ($result_mk as $r) {
		$hit++;
		$nm_kelas = $db->QuerySingle("select nama_kelas from nama_kelas where id_nama_kelas='" . $r['NO_KELAS_MK'] . "'") or die("salah kueri baris " . __LINE__);

		if ($status_krs == '1') {
			if ($r['STATUS_APV_PENGAMBILAN_MK'] == "1") {
				$boleh_hapus = "";
			} elseif ($r['STATUS_APV_PENGAMBILAN_MK'] == "0") {
				$boleh_hapus = '<input type="button" name="simpan" value="Hapus" onclick="if(confirm(\'Yakin hapus ?\')!=0){ krshapus_kirim(' . $r['ID_PENGAMBILAN_MK'] . ') }" >';
			}
		} else {
			$boleh_hapus = "";
		}

		if ($r['STATUS_PENGAMBILAN_MK'] == "3") {
			$stat_mk = "MTA tidak disetujui";
		} else {
			if ($r['STATUS_APV_PENGAMBILAN_MK'] == "1") {
				$stat_mk = "Sudah disetujui dosen";
			} elseif ($r['STATUS_APV_PENGAMBILAN_MK'] == "0") {
				$stat_mk = 'Belum disetujui dosen';
			}
		}
		// AMBIL JADWAL
		$jadwalnya = "";
		$query_jadwal = "select a.nm_jadwal_jam,b.id_jadwal_hari,b.id_jadwal_jam from aucc.jadwal_jam a, aucc.jadwal_kelas b where a.id_jadwal_jam=b.id_jadwal_jam and b.id_kelas_mk='" . $r['ID_KELAS_MK'] . "'";
		$db->Query($query_jadwal) or die("salah kueri baris " . __LINE__);
		$arr_jadwalnya = $db->FetchAssoc();
		if ($arr_jadwalnya['ID_JADWAL_HARI'] == '1') {
			$harinya = "Minggu";
		} elseif ($arr_jadwalnya['ID_JADWAL_HARI'] == '2') {
			$harinya = "Senin";
		} elseif ($arr_jadwalnya['ID_JADWAL_HARI'] == '3') {
			$harinya = "Selasa";
		} elseif ($arr_jadwalnya['ID_JADWAL_HARI'] == '4') {
			$harinya = "Rabu";
		} elseif ($arr_jadwalnya['ID_JADWAL_HARI'] == '5') {
			$harinya = "Kamis";
		} elseif ($arr_jadwalnya['ID_JADWAL_HARI'] == '6') {
			$harinya = "Jumat";
		} elseif ($arr_jadwalnya['ID_JADWAL_HARI'] == '7') {
			$harinya = "Sabtu";
		}
		$jadwalnya .= "<br>" . $harinya . " " . $arr_jadwalnya['NM_JADWAL_JAM'];
		$jadwalnya = substr($jadwalnya, 4);
		$isi .= '
			<tr>
				<td>' . $hit . '</td>
				<td>' . $r['KD_MATA_KULIAH'] . '</td>
				<td>' . $r['NM_MATA_KULIAH'] . ' <hr/>Jadwal <b>'.$jadwalnya.'</b></td>
				<td>' . $r['KREDIT_SEMESTER'] . '</td>
				<td>' . $nm_kelas . '</td>
				<td>' . $stat_mk . '</td>
				<td align=center style="width:100px">' . $boleh_hapus . '</td>
			</tr>
		';
	}

	// ambil pesan dosen
	$kueri_pesan = "
	select isi_pesan
	from pesan
	where tipe_pesan='KRS' and id_penerima='" . $user->ID_PENGGUNA . "'
	";
	$pesan_dosen = "";
	$pesan_dosen = $db->QuerySingle($kueri_pesan);
	if (!empty($pesan_dosen)) {
		$pesan_dosen = $pesan_dosen;
	}
	$isi .= '
	<tr>
		<td colspan="7">Pesan Dosen :<br>' . $pesan_dosen . '</td>
	</tr>
	</table>
	</form>
	';
	echo $isi;
} elseif ($_POST["aksi"] == "hapus" and $_POST["pengambilan_mk"]) {
	$lanjut = true;
	if (harusAngka($_POST["pengambilan_mk"])) {
		$id_pengambilan_mk = $_POST["pengambilan_mk"];
	} else {
		$pesan .= "Illegal Character\n";
		$id_pengambilan_mk = '0';
		$lanjut = false;
	}

	// ambil id_mhs -- tambahan filter
	$id_mhs = "";
	$result = $db->Query("select id_mhs from mahasiswa where id_pengguna='" . $user->ID_PENGGUNA . "'") or die("salah kueri baris " . __LINE__);
	while ($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

	// ambil semester_aktif
	$sem_aktif = "";
	$kueri = "select id_semester from semester where STATUS_AKTIF_SEMESTER='True' and id_perguruan_tinggi = '{$id_pt_user}' order by id_semester desc";
	$result = $db->Query($kueri) or die("salah kueri baris " . __LINE__);
	while ($r = $db->FetchRow()) {
		$sem_aktif = $r[0];
	}

	$status_krs = $_SESSION['STATUS_KRS'];

	// ambil id_kelas
	$id_kelas = "";
	$kueri = "select id_kelas_mk from pengambilan_mk where id_pengambilan_mk='" . $id_pengambilan_mk . "'";
	$result = $db->Query($kueri) or die("salah kueri baris " . __LINE__);
	while ($r = $db->FetchRow()) {
		$id_kelas = $r[0];
	}

	if ($status_krs == '1') {

		// Delete nilai mk
		$kueri = "delete from nilai_mk where id_pengambilan_mk = '" . $id_pengambilan_mk . "'";
		$result = $db->Query($kueri) or die("salah kueri baris " . __LINE__);

		// Delete pengambilan mk
		$kueri = "delete from pengambilan_mk where status_apv_pengambilan_mk='0' and id_pengambilan_mk='" . $id_pengambilan_mk . "' and id_mhs = {$id_mhs}";
		$result =  $db->Query($kueri) or die("salah kueri baris " . __LINE__);

		if ($result) {
			//$kueri = "update kelas_mk set terisi_kelas_mk=(terisi_kelas_mk-1) where terisi_kelas_mk>=0 and id_kelas_mk='".$id_kelas."' ";
			//$result =  $db->Query($kueri)or die("salah kueri 73 ");

			//if($db->NumRows() >0) {
			echo 'hapus berhasil';
			//}
		} else {
			echo 'hapus gagal';
		}
	}
}
