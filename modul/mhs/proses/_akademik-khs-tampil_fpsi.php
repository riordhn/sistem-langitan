<?php
require('../../../config.php');

// Yudi Sulistya, 01 Aug 2013 (tablesorter)
echo '
<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[2,0]], widgets: ["zebra"]
		}
	);
}
);
</script>
';

if($_POST["aksi"]=="tampil" and $_POST["semes"]) {
	// ambil id_mhs
	$id_mhs=""; $prodi=""; $nim_mhs="";
	$result = $db->Query("select id_mhs,id_program_studi,nim_mhs from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'")or die("salah kueri : 1");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
		$prodi = $r[1];
		$nim_mhs = $r[2];
	}
	// ambil fakultas
	$fakul = "";
	$result = $db->Query("select id_fakultas from program_studi where id_program_studi='".$prodi."'")or die("salah kueri : 1a");
	while($r = $db->FetchRow()) {
		$fakul = $r[0];
                
	}
$idsemes = sprintf("%d",$_POST["semes"]);
	
	// ambil semester
	$kueri2 = "select thn_akademik_semester,nm_semester,tipe_semester from semester where id_semester='".$idsemes."' ";
	$sem=""; $tahun="";
	$result = $db->Query($kueri2)or die("salah kueri : 2");
	while($r = $db->FetchRow()) {
		$tahun = $r[0]."/".($r[0]+1);
		$sem = $r[1];
		$tmp_thn_akad = $r[0];
		$tmp_sem = $r[1];
		$sem_sp = $r[2];
	}
	$semester = $sem." ".$tahun." ";

	$judulfk = "Hasil Studi Semester ".$semester;

	$isi = '
	<p><h2>'.$judulfk.'</h2></p>
	<table width="850" id="myTable" class="tablesorter">
	<thead>
	<tr>
		<th>Mata Kuliah</th>
		<th>SKS</th>
		<th>Nilai</th>
	</tr>
	</thead>
	<tbody>
	';

		if ($sem_sp == 'SP') {
		$kueri2 = "
select kd_mata_kuliah as kode,upper(nm_mata_kuliah) as nama,nama_kelas,tipe_semester,
kredit_semester as sks,case when flagnilai=1 then nilai_huruf else '-' end as nilai,bobot,bobot_total, id_pengambilan_mk
from(
select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
KURIKULUM_MK.kredit_semester,case when kurikulum_mk.status_mkta='1' 
and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
row_number() over(partition by pengambilan_mk.id_mhs,kd_mata_kuliah order by nilai_huruf) rangking, pengambilan_mk.id_pengambilan_mk,
pengambilan_mk.flagnilai
from PENGAMBILAN_MK
left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
where  group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_semester='".$idsemes."')
and tipe_semester='SP'
and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0
and mahasiswa.id_mhs='".$id_mhs."') 
where rangking=1";		
		} else {
		$kueri2 = "
select kd_mata_kuliah as kode,upper(nm_mata_kuliah) as nama,nama_kelas,tipe_semester,
kredit_semester as sks,case when flagnilai=1 then nilai_huruf else '-' end as nilai,bobot,bobot_total, id_pengambilan_mk
from(
select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
KURIKULUM_MK.kredit_semester,case when kurikulum_mk.status_mkta='1' 
and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
row_number() over(partition by pengambilan_mk.id_mhs,kd_mata_kuliah order by nilai_huruf) rangking, pengambilan_mk.id_pengambilan_mk,
pengambilan_mk.flagnilai
from PENGAMBILAN_MK
left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
where  group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_semester='".$idsemes."')
and tipe_semester in ('UP','REG','RD')
and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0
and mahasiswa.id_mhs='".$id_mhs."') 
where rangking=1";
		}
	$result = $db->Query($kueri2)or die("salah kueri : 4");
	while($r = $db->FetchRow()) {
	$isi .= '
			<tr>
				<td><span>'.$r[0].' -- '.$r[1].'</span></td>
				<td align="center">'.$r[4].'</td>';
	if ($r[5] == '-') {
		$isi .= '<td align="center">'.trim($r[5]).'</td>';
	} else {
		$isi .= '<td align="center"><a style="text-decoration:underline;cursor:pointer;" onclick="window.open(\'proses/_akademik-khs-tampil_det.php?mk='.$r[8].'\',\'baru2\');">'.trim($r[5]).'</a></td>';
	}
		$isi .= '
			<tr>';
	}

$db->Query("select sum(kredit_semester) as sks_sem, sum((bobot*kredit_semester)) as bobot_total, 
case when sum(kredit_semester)=0 then 0 else round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips
from 
(
select kredit_semester,bobot
from(
select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
KURIKULUM_MK.kredit_semester,case when kurikulum_mk.status_mkta='1' 
and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
nilai_angka,a.besar_nilai_mk as UTS,b.besar_nilai_mk as UAS,
row_number() over(partition by pengambilan_mk.id_mhs,kd_mata_kuliah order by nilai_huruf) rangking
from PENGAMBILAN_MK
left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
left join semester on pengambilan_mk.id_semester=semester.id_semester
left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
left join nilai_mk a on pengambilan_mk.id_pengambilan_mk=a.id_pengambilan_mk and a.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UTS')
left join nilai_mk b on pengambilan_mk.id_pengambilan_mk=b.id_pengambilan_mk and b.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UAS')
where  group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_semester='".$idsemes."')
and tipe_semester in ('UP','REG','RD')
and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0 and flagnilai='1'
and mahasiswa.id_mhs='".$id_mhs."') 
where rangking=1
)");
$rw = $db->FetchAssoc();
$ips = $rw['IPS'];
$ips_bawah = $rw['SKS_SEM'];

//perhitungan sks total dan ipk versi lukman
$sql="select a.id_mhs,sum(a.kredit_semester) skstotal,
round(sum(a.kredit_semester*(case a.nilai_huruf 
when 'A' then 4 
when 'AB' then 3.5 
when 'B' then 3
when 'BC' then 2.5
when 'C' then 2
when 'D' then 1
end))/sum(a.kredit_semester),2) IPK
from
(
select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null 
and a.id_semester is not null and a.flagnilai=1 and a.status_hapus=0 and a.flagnilai='1'
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where rangking=1 and id_mhs='{$id_mhs}'
) a
left join mahasiswa b on a.id_mhs=b.id_mhs
left join program_studi f on b.id_program_studi=f.id_program_studi
where a.id_mhs='{$id_mhs}'
group by a.id_mhs order by a.id_mhs";
$result2 = $db->Query($sql);
			while($r2 = $db->FetchRow()) {
				$ipk_skstot = $r2[1];
				$ipk = $r2[2];
			}
	// ambil sks sems depan
	$ipk_sksdepan="0.00";

	$kueri2 = "select max(sks_maksimal) as sks_maksimal from beban_sks where id_program_studi = ".$prodi." and ipk_minimum <= ".$ips."";
	$result = $db->Query($kueri2)or die("salah kueri : 5");
	while($r = $db->FetchRow()) {
		$ipk_sksdepan = $r[0];
	}
	if($ipk_sksdepan>24) {
		$ipk_sksdepan=24;
	}

	if ($ips == null) {
	$isi .= '
	</tbody>
	</table>';
	} else {
	$isi .= '
	</tbody>
	</table>
	IPS : '.$ips."<br/> Sks Semester: $ips_bawah".'<br> IPK : '.$ipk.'<br> Sks Total : '.$ipk_skstot.'<br> Jatah Sks : '.$ipk_sksdepan.'
	';
	}

	echo $isi; 
}
?>
