<?php
require('../../../config.php');
require('../includes/ceking3.php');
$db = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Surat Pernyataan Auto Debet');
$pdf->SetSubject('Surat Pernyataan Auto Debet');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";


$nim = Angka($_REQUEST['nim']);
//$nim = $_REQUEST['nim'];
//echo $nim;

$kueri="
select
	a.nm_pengguna, d.nm_jenjang, e.nm_fakultas, c.nm_program_studi, b.no_rekening_debet, b.id_mhs, b.id_semester_masuk, b.alamat_mhs, b.mobile_mhs
from 
	aucc.pengguna a, aucc.mahasiswa b, aucc.program_studi c, aucc.jenjang d, aucc.fakultas e
where
	a.id_pengguna=b.id_pengguna and b.id_program_studi=c.id_program_studi and c.id_jenjang=d.id_jenjang and c.id_fakultas=e.id_fakultas
	and b.nim_mhs='".$nim."'
";
$result = $db->Query($kueri)or die("salah kueri 0 ");
while($r = $db->FetchRow()) {
	$mhs_nama = $r[0];
	$mhs_jenjang = $r[1];
	$mhs_fakul = $r[2];
	$mhs_prodi = $r[3];
	$mhs_rekening = (($r[4]=='')?'XXX':$r[4]);
	$mhs_idmhs = $r[5];
	$mhs_semes = $r[6];
	$mhs_alamat = $r[7];
	$mhs_mobile = $r[8];
}

$kueri="
select
	sum(besar_biaya)
from 
	aucc.pembayaran a
where
	a.id_mhs='".$mhs_idmhs."' and id_semester='".$mhs_semes."' and id_detail_biaya in (select ID_DETAIL_BIAYA from AUCC.DETAIL_BIAYA where ID_BIAYA in (47, 110, 134))
";
$mhs_biaya = '0';
$result = $db->Query($kueri)or die("salah kueri 1 ");
while($r = $db->FetchRow()) {
	$mhs_biaya = $r[0];
}

switch ($nim) {
	case "081311333098" :
		$mhs_biaya=4000000; break;
	case "141311133082" :
		$mhs_biaya=4000000; break;
	case "021311133009" :
		$mhs_biaya=1250000; break;
}


$kueri="
select
	b.nm_bank
from 
	aucc.mahasiswa a, aucc.bank b
where
	a.id_bank=b.id_bank
	and a.nim_mhs='".$nim."'
";
$mhs_bank = 'XXX';
$result = $db->Query($kueri)or die("salah kueri 1 ");
while($r = $db->FetchRow()) {
	$mhs_bank = $r[0];
}


$content = "";
$pdf->setPrintHeader(false);
/*
// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
*/
// set default monospaced font
//$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default footer
//$pdf->setPrintFooter(false);

//set auto page breaks
//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------
//$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 12);

// add a page
$pdf->AddPage('P', 'A4');

// draw jpeg image
//$pdf->Image('../includes/draft.png', 30, 70, 140, '40', '', '', '', false, 72);

// set the starting point for the page content
//$pdf->setPageMark();

if($mhs_bank == 'BNI 6257') {
$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><u><b>PERNYATAAN SURAT KUASA PENDEBETAN REKENING</b></u></td>
  </tr>
</table>
<br><br>
Yang bertanda tangan di bawah ini kami :<br>
<table cellpadding="0" cellspacing="0" border="0" width="70%">
<tr>
	<td width="29%">Nama</td>
	<td width="1%">:</td>
	<td width="70%"> '.$mhs_nama.'</td>
</tr>
<tr>
	<td>No. KTP / SIM</td>
	<td>:</td>
	<td></td>
</tr>
<tr>
	<td>Alamat</td>
	<td>:</td>
	<td> '.$mhs_alamat.'</td>
</tr>
<tr>
	<td>No. Telepon / HP</td>
	<td>:</td>
	<td> '.$mhs_mobile.'</td>
</tr>
<tr>
	<td>Nim</td>
	<td>:</td>
	<td> '.$nim.'</td>
</tr>
<tr>
	<td>Program Studi</td>
	<td>:</td>
	<td> '.$mhs_jenjang.' '.$mhs_prodi.'</td>
</tr>
<tr>
	<td>Fakultas</td>
	<td>:</td>
	<td> '.$mhs_fakul.'</td>
</tr>
</table>
<br>
Untuk selanjutnya disebut :
<div align="center"><b>.....................................................PEMBERI KUASA.....................................................</b></div>
Dengan ini memberi kuasa kepada : <br><br>
Pemimpin PT. Bank Negara Indonesia (Persero) Tbk. Kantor Layanan Universitas Airlangga Surabaya.
<div align="center">............................................................................................................................................</div>
Untuk selanjutnya disebut : 
<div align="center"><b>.....................................................PENERIMA KUASA.....................................................<br>KHUSUS</b></div>
<p>
Mewakili serta bertindak untuk dan atas nama Pemberi Kuasa untuk melakukan tindakan berupa :<br>
Mendebet rekening Taplus Mahasiswa nomor rekening : '.$mhs_rekening.' atas nama Pemberi Kuasa di PT. Bank Negara Indonesia (Persero) Tbk. Kantor Layanan Universitas Airlangga Surabaya dalam rangka pembayaran Uang Kuliah Tunggal (UKT) dan biaya-biaya yang terkait perkuliahan di Universitas Airlangga Surabaya.<br>
Apabila saldo rekening Pemberi Kuasa kurang dari jumlah total kewajiban sebagaimana ketentuan, maka Penerima Kuasa dibebaskan dari kewajiban akibat gagalnya proses pendebetan rekening tersebut. Segala akibat yang timbul berkenaan dengan kegagalan pendebetan tersebut menjadi tanggung jawab Pemberi Kuasa sepenuhnya. 
</p>

<p>
Surat kuasa ini berlaku terhitung sejak ditandatangani oleh Pemberi Kuasa dan akan berakhir dengan sendirinya apabila terjadi pencabutan atas Surat Kuasa ini yang akan diberitahukan secara tertulis kepaada PT. Bank Negara Indonesia (Persero) Tbk. Kantor Layanan Universitas Airlangga Surabaya dan atau Pemberi Kuasa telah menyelesaikan pendidikan di Universitas Airlangga Surabaya.
</p>

<p>
Demikian surat kuasa ini saya berikan dengan hak substitusi untuk dapat dipergunakan sebagaimana mestinya.
</p>

<table cellpadding="0" cellspacing="0" border="0" width="90%">
<tr>
	<td width="30%" valign="top" align="center">
		<br><br>PENERIMA KUASA<br>
		<br><br><br><br>
		'.$mhs_bank.'<br>
	</td>
	<td width="40%" valign="top" align="center">
		&nbsp;
	</td>
	<td width="30%" valign="top" align="center">
		Surabaya '.date("d-m-Y").'<br>
		PEMBERI KUASA<br>
		<br><br>Materei Rp. 6.000<br><br>
		'.$mhs_nama.'<br>
	</td>
</tr>
</table>
';
}

else{
$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><h1>PERNYATAAN SURAT KUASA</h1></td>
  </tr>
</table>
<br><br>
Yang bertanda tangan di bawah ini kami :<br>
<table cellpadding="0" cellspacing="0" border="0" width="70%">
<tr>
	<td width="29%">Nama</td>
	<td width="1%">:</td>
	<td width="70%"> '.$mhs_nama.'</td>
</tr>
<tr>
	<td>Nim</td>
	<td>:</td>
	<td> '.$nim.'</td>
</tr>
<tr>
	<td>Program Studi</td>
	<td>:</td>
	<td> '.$mhs_jenjang.' '.$mhs_prodi.'</td>
</tr>
<tr>
	<td>Fakultas</td>
	<td>:</td>
	<td> '.$mhs_fakul.'</td>
</tr>
</table>

<p>
Dengan ini memberikan kuasa kepada bank <b>'.$mhs_bank.'</b> untuk melakukan pendebetan/pemindahbukuan setiap semester pada tanggal sesuai dengan jadual pembayaran yang ditetapkan Universitas Airlangga dari rekening kami atas nama <b>'.$mhs_nama.'</b> dengan no rekening <u>'.$mhs_rekening.'</u> pada bank <b>'.$mhs_bank.'</b> untuk biaya SOP dan Asuransi Kesehatan kami sebesar <b>Rp. '.number_format($mhs_biaya,0,',','.').'.</b>
</p>

<p>
Pernyataan surat kuasa ini berlaku sejak tanggal kami menyatakan setuju untuk memberikan kuasa kepada bank sampai kami dinyatakan lulus oleh Universitas Airlangga
</p>

<p>
Demikian pernyataan surat kuasa ini dibuat dengan sebenarnya dan dapat dijadikan sebagai surat kuasa dengan kekuatan hukum sebagaimana mestinya.
</p>

<table cellpadding="0" cellspacing="0" border="0" width="90%">
<tr>
	<td width="30%" valign="top" align="center">
		Surabaya '.date("d-m-Y").'<br>
		Pemberi Kuasa<br>
		<br><br>Materei Rp. 6.000<br><br>
		'.$mhs_nama.'<br>
	</td>
	<td width="40%" valign="top" align="center">
		&nbsp;
	</td>
	<td width="30%" valign="top" align="center">
		<br><br>Penerima Kuasa<br>
		<br><br><br><br>
		'.$mhs_bank.'<br>
	</td>
</tr>
</table>

<p>
Catatan:<br>
Mohon diserahkan ke bank yang menerbitkan KTM (Cabang UNAIR) paling lambat tanggal 18 juli 2014
</p>
';
}

// output the HTML content
$pdf->writeHTML($html);
$pdf->Output();

?>