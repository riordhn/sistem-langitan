<?php
require '../../../config.php';
require '../class/process.class.php';

$id_pt = $id_pt_user;

if ($_POST["aksi"] !== "tampil") {
    exit();
}

$isi = '
    <table class="table table-striped">
    <tr>
        <th>No.</th>
        <th>KODE MTA</th>
        <th>NAMA MATA AJAR</th>
        <th>SKS MTA</font></th>
        <th>KELAS</th>
        <th>STATUS</th>
    </tr>
    ';

$hit = 0;
$sks_terambil = 0;
$result = $db->Query("SELECT mk.KD_MATA_KULIAH, mk.NM_MATA_KULIAH, kur.KREDIT_SEMESTER, pmk.ID_PENGAMBILAN_MK,
    kls.NO_KELAS_MK, pmk.STATUS_APV_PENGAMBILAN_MK, nk.NAMA_KELAS
    FROM PENGAMBILAN_MK pmk
    JOIN KELAS_MK kls ON kls.ID_KELAS_MK = pmk.ID_KELAS_MK
    JOIN KURIKULUM_MK kur ON kur.ID_KURIKULUM_MK = kls.ID_KURIKULUM_MK
    JOIN MATA_KULIAH mk ON mk.ID_MATA_KULIAH = kur.ID_MATA_KULIAH
    LEFT JOIN NAMA_KELAS nk ON nk.ID_NAMA_KELAS = kls.NO_KELAS_MK
    WHERE pmk.ID_SEMESTER = {$user->SEMESTER->ID_SEMESTER_AKTIF} AND pmk.ID_MHS = {$user->MAHASISWA->ID_MHS}
    ORDER BY mk.NM_MATA_KULIAH ASC");
while ($r = $db->FetchRow()) {
    $hit++;
    $isi .= '
            <tr>
                <td>' . $hit . '</td>
                <td>' . $r[0] . '</td>
                <td>' . $r[1] . '</td>
                <td>' . $r[2] . '</td>
                <td>' . $r[6] . '</td>
                <td>' . (($r[5] == '1') ? 'Disetujui' : 'Belum Disetujui') . '</td>
            </tr>
        ';
    $sks_terambil += $r[2];
}

// Ambil Informasi Mahasiswa Status
$db->Query("SELECT ms.* FROM MAHASISWA_STATUS ms
JOIN SEMESTER s ON s.ID_SEMESTER = ms.ID_SEMESTER
WHERE ms.ID_MHS = {$user->MAHASISWA->ID_MHS} AND ms.ID_SEMESTER = {$user->SEMESTER->ID_SEMESTER_LALU}
ORDER BY s.THN_AKADEMIK_SEMESTER DESC, s.NM_SEMESTER DESC");

$mahasiswa_status_sem_lalu = $db->FetchAssoc();

if ($mahasiswa_status_sem_lalu != null) {
    $ipk_mhs = $mahasiswa_status_sem_lalu['IPK'];
    $ips_mhs = $mahasiswa_status_sem_lalu['IPS'];
} else {
    $ipk_mhs = 0;
    $ips_mhs = 0;
}

$sks_maks = $db->QuerySingle("SELECT MAX(SKS_MAKSIMAL) SKS_MAKSIMAL FROM BEBAN_SKS bs 
    WHERE ID_PROGRAM_STUDI = {$user->MAHASISWA->ID_PROGRAM_STUDI} AND IPK_MINIMUM <= {$ipk_mhs}");

if ($sks_maks == null) {
    $sks_maks = 24;
}

// status utk menampilkan KHS hanya yg sudah di approve keuangan
// khusus umaha, dan yg >= 2016 Genap
/* FIKRIE */
$status_sem_khs = 0;
// tambahan nambi
$process = new process($db, $user->ID_PENGGUNA, $user->ID_PERGURUAN_TINGGI);
$id_semester_aktif_keu = $process->GetSemesterAktif();

// Untuk FIKES UMAHA, langsung boleh cetak
if ($user->ID_PERGURUAN_TINGGI == PT_UMAHA and $user->MAHASISWA->ID_FAKULTAS == 1) {
    $status_sem_khs = 1;
} elseif (
    $user->ID_PERGURUAN_TINGGI == PT_UNU_LAMPUNG || 
    $user->ID_PERGURUAN_TINGGI == PT_UNU_KALBAR ||
    $user->ID_PERGURUAN_TINGGI == PT_UNINUS) {
    $status_sem_khs = 1;
} else {
    $db->Query("SELECT IS_BOLEH_CETAK FROM KEUANGAN_KHS 
        WHERE ID_SEMESTER='{$user->SEMESTER->ID_SEMESTER_LALU}' 
        AND ID_MHS = '{$user->MAHASISWA->ID_MHS}'");

    $is_boleh_cetak = $db->FetchAssoc();

    if ($process->CekPembayaran($user->MAHASISWA->ID_MHS, $id_semester_aktif_keu, false)) {
        $status_sem_khs = 1;
    }
}
    

if ($status_sem_khs == 0) {
    $isi .= '
        <tr>
            <td colspan="6">
            <b>IPK : ' . $ipk_mhs . ' <br/>
            <b>IPS : ' . $ips_mhs . ' <br/>
            <!-- <b>MAX SKS : ' . $sks_maks . ' <br/> -->
            TERAMBIL : ' . $sks_terambil . ' <br />
             
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                * Kartu Rencana Studi Belum Bisa Dicetak, Silakan Hubungi Bagian Keuangan<br />
            </td>
        </tr>
        </table>
        ';
} elseif ($status_sem_khs == 1) {
    $isi .= '
        <tr>
            <td colspan="6">
            <b>IPK : ' . $ipk_mhs . ' <br/>
            <b>IPS : ' . $ips_mhs . ' <br/>
            <!-- <b>MAX SKS : ' . $sks_maks . ' <br/> -->
            TERAMBIL : ' . $sks_terambil . ' <br />
             
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                <!-- * Mata kuliah yang bisa di cetak hanya yang sudah di setujui oleh  dosen wali<br /> -->
                <input type=button name="cetak5" class="btn btn-sm btn-primary" value="Cetak" onclick="window.open(\'proses/_akademik-krs_cetak.php\',\'baru2\');">
            </td>
        </tr>
        </table>
        ';
}

echo $isi;
