<?php
require '../../../config.php';
require '../function-tampil-informasi-no-conf.php';
require '../class/eva.class.php';
require '../class/process.class.php';

$process = new process($db, $user->ID_PENGGUNA, $user->ID_PERGURUAN_TINGGI);

$id_semester = (int)post('semes', 0);

$id_mhs		= $user->MAHASISWA->ID_MHS;
$prodi		= $user->MAHASISWA->ID_PROGRAM_STUDI;
$nim_mhs	= $user->MAHASISWA->NIM;
$isi = '';

// status utk menampilkan detail nilai
//	khusus umaha, dan yg >= 2015 Genap
$boleh_lihat_detail = 0;

// status utk menampilkan KHS hanya yg sudah di approve keuangan
//	khusus umaha, dan yg >= 2016 Genap
$perlu_cek_pembayaran = 0;
$boleh_lihat_khs = 0;

// Ambil row Semester
$sql = "SELECT thn_akademik_semester, nm_semester, tipe_semester, tahun_ajaran from semester where id_semester = '{$id_semester}'";
$db->Query($sql);
$semester = $db->FetchAssoc();

// mencari status semester utk menampilkan detai nilai (khusus umaha)
if ($id_pt_user == PT_UMAHA) {
    // Mulai Genap 2015 diijinkan lihat detail
    if ($semester['THN_AKADEMIK_SEMESTER'] == 2015) {
        if ($semester['NM_SEMESTER'] == 'Genap') {
            $boleh_lihat_detail = 1;
        }
    } else if ($semester['THN_AKADEMIK_SEMESTER'] > 2015) {
        $boleh_lihat_detail = 1;
    }
} else { // selain UMAHA, boleh lihat detail semua
    $boleh_lihat_detail = 1;
}

// mencari status utk khs di sisi keuangan (khusus umaha non-fikes)
if ($id_pt_user == PT_UMAHA and $user->MAHASISWA->ID_FAKULTAS != 1) {
    // Mulai Genap 2016 perlu dicek pembayaran
    if ($semester['THN_AKADEMIK_SEMESTER'] == 2016) {
        if ($semester['NM_SEMESTER'] == 'Genap') {
            $perlu_cek_pembayaran = 1;
        }
    } else if ($semester['THN_AKADEMIK_SEMESTER'] > 2016) {
        $perlu_cek_pembayaran = 1;
    }
}

/*** TAMBAHAN CEK EVALUASI (NAMBI) ***/
$eva = new eva($db, $user->ID_PENGGUNA);
$proses_evaluasi = $eva->CekEvaluasiSemester($id_semester, $id_mhs, $user->ID_PERGURUAN_TINGGI);
$ada_pembayaran = $process->CekPembayaran($id_mhs, $id_semester, true);

if ($perlu_cek_pembayaran) {
    if ($ada_pembayaran) {
        $boleh_lihat_khs = 1;
    } else {
        $boleh_lihat_khs = 0;
    }
} else {
    $boleh_lihat_khs = 1;
}

// cek apabila SP boleh
// *FIKRIE 18-08-2020
if ($semester['TIPE_SEMESTER'] == 'SP') {
    $boleh_lihat_detail = 1;
    $boleh_lihat_khs = 1;
}

// TAMBAHAN apabila SP boleh lihat 
if ($boleh_lihat_khs && $proses_evaluasi['status_isi'] == 1) {
    // Catatan :
    // - Nilai huruf jika NULL tampil Kosong (*K)

    $sql =
        "SELECT 
            pengambilan_mk.id_mhs,
            /* Kode MK */
            COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
            /* Nama MK */
            COALESCE(mk1.nm_mata_kuliah, mk2.nm_mata_kuliah) AS nm_mata_kuliah,
            /* SKS */
            COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS kredit_semester,
            nama_kelas,
            tipe_semester,
            pengambilan_mk.nilai_huruf,
            pengambilan_mk.id_pengambilan_mk,
            pengambilan_mk.flagnilai
        FROM pengambilan_mk
        JOIN mahasiswa			ON pengambilan_mk.id_mhs = mahasiswa.id_mhs
        JOIN semester			ON pengambilan_mk.id_semester = semester.id_semester
        JOIN program_studi		ON mahasiswa.id_program_studi = program_studi.id_program_studi
        -- Via Kelas
        LEFT JOIN kelas_mk		ON pengambilan_mk.id_kelas_mk = kelas_mk.id_kelas_mk
        LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kelas_mk.id_mata_kuliah
        LEFT JOIN nama_kelas	ON kelas_mk.no_kelas_mk = nama_kelas.id_nama_kelas
        -- Via Kurikulum
        LEFT JOIN kurikulum_mk	ON pengambilan_mk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk
        LEFT JOIN mata_kuliah mk2 ON kurikulum_mk.id_mata_kuliah = mk2.id_mata_kuliah
        -- Bobot
        LEFT JOIN standar_nilai	ON pengambilan_mk.nilai_huruf = standar_nilai.nm_standar_nilai
        WHERE
            pengambilan_mk.ID_SEMESTER = {$id_semester} AND
            status_apv_pengambilan_mk = 1 AND 
            pengambilan_mk.status_hapus = 0 AND 
            pengambilan_mk.status_pengambilan_mk != 0 AND
            mahasiswa.id_mhs = '{$id_mhs}'
        ORDER BY 3 ASC";
        
    $pengambilan_mk_set = $db->QueryToArray($sql);
    $smarty->assign('pengambilan_mk_set', $pengambilan_mk_set);
    
    $link = 'proses/_akademik-khs_cetak.php?nim=' . $nim_mhs . '&id=' . $id_semester;
    $smarty->assign('cetak_khs_link', $link);
}


// echo $isi;
$smarty->assign('semester', $semester);
$smarty->assign('boleh_lihat_khs', $boleh_lihat_khs);
$smarty->assign('boleh_lihat_detail', $boleh_lihat_detail);
$smarty->assign('proses_evaluasi', $proses_evaluasi);
$smarty->display('../gui/proses/akademik-khs-tampil.tpl');