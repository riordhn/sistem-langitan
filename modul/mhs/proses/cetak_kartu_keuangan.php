<?php
require('../../../config.php');
$db2 = new MyOracle();

$id_pt = $id_pt_user;

 $kota_pt = $PT->kota_pt; #untuk informasi kota PTNU by putra rieskha
require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

if ($user->Role() != AUCC_ROLE_MAHASISWA) {
	echo "anda tidak berhal mengakses halaman ini";
	exit();
}

$id_tagihan = $_GET['x'];

$nama_kartu="";
$kueri = "SELECT b.nm_biaya, s.thn_akademik_semester, s.nm_semester, s.tahun_ajaran
			FROM tagihan t 
			JOIN detail_biaya db ON db.id_detail_biaya = t.id_detail_biaya
			JOIN biaya_kuliah bk ON bk.id_biaya_kuliah = db.id_biaya_kuliah
			JOIN semester s ON s.id_semester = bk.id_semester
			JOIN biaya b ON b.id_biaya = db.id_biaya
			WHERE t.id_tagihan='".$id_tagihan."'";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$nama_kartu 			= $r[0];
	$thn_akademik_semester 	= $r[1];
	$nm_semester 			= $r[2]; 
	$tahun_ajaran 			= $r[3];
}

$mhs_nama="";
$kueri = "select nm_pengguna from pengguna where id_pengguna='".$user->ID_PENGGUNA."'";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$mhs_nama = $r[0];
}

// ambil id_mhs
$id_mhs=""; $mhs_nim=""; $mhs_fakul=""; $mhs_prodi=""; $mhs_jenjang=""; $id_prodi=""; $id_jenjang="";  $id_fakultas="";
$kueri = 
"SELECT a.id_mhs, a.NIM_MHS, 'FAKULTAS '||c.nm_fakultas
, b.nm_program_studi, d.nm_jenjang, a.id_program_studi, b.id_fakultas, b.id_jenjang,a.thn_angkatan_mhs,
c.alamat_fakultas, c.kodepos_fakultas, c.telpon_fakultas, c.faksimili_fakultas, c.website_fakultas, c.email_fakultas
from mahasiswa a, program_studi b, fakultas c, jenjang d
where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and b.id_jenjang=d.id_jenjang and a.id_pengguna='".$user->ID_PENGGUNA."'";
$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
while($r = $db->FetchRow()) {
	$id_mhs 		= $r[0];
	$mhs_nim 		= $r[1];
	$mhs_fakul 		= $r[2];
	$mhs_prodi 		= $r[3];
	$mhs_jenjang	= $r[4];
	$id_prodi 		= $r[5];
	$id_fakultas 	= $r[6];
	$id_jenjang 	= $r[7];
	$angkatan_mhs 	= $r[8];
	$alm_fak 		= $r[9];
	$pos_fak 		= $r[10];
	$tel_fak 		= $r[11];
	$fax_fak 		= $r[12];
	$web_fak 		= $r[13];
	$eml_fak 		= $r[14];
}

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'F4', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('Kartu '.$nama_kartu.' Mahasiswa');
$pdf->SetSubject('Kartu '.$nama_kartu.' Mahasiswa');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logo-".$nama_singkat.".gif";
//$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "20";
$title = strtoupper($PT->nama_pt);

$content = strtoupper($mhs_fakul)."\n".strtoupper($mhs_jenjang)." - ".strtoupper($mhs_prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:8x;">
	  <tr>
	    <td colspan="7" align="center"><p><strong>KARTU '.strtoupper($nama_kartu).' <br>TAHUN AJARAN '.$tahun_ajaran.' SEMESTER '.strtoupper($nm_semester).'</strong></p>
	    </td>
	  </tr>
	  <tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	  </tr>
	  <tr>
	           <td width="12%"><strong>Nama</strong></td>
	           <td width="1%"><strong>: </strong></td>
	           <td width="33%"><strong>'.$mhs_nama.'</strong></td>
	  </tr>
	 <tr>
	   <td><strong>NIM</strong></td>
	   <td><strong>: </strong></td>
	   <td><strong>'.$mhs_nim.'</strong></td>
	 </tr>
	 <tr>
	   <td><strong>Program Studi</strong></td>
	   <td><strong>: </strong></td>
	   <td><strong>'.$mhs_jenjang.' '.strtoupper($mhs_prodi).'</strong></td>
	 </tr>
	 <tr>
	   <td><strong>Fakultas</strong></td>
	   <td><strong>: </strong></td>
	   <td><strong>'.strtoupper($mhs_fakul).'</strong></td>
	 </tr>
</table>
<p>&nbsp;</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:8x;">
	  <tr>
	    <td colspan="7" align="center"><p><strong>TELAH MELAKUKAN PEMBAYARAN '.strtoupper($nama_kartu).' <br>TAHUN AJARAN '.$tahun_ajaran.' SEMESTER '.strtoupper($nm_semester).'</strong></p>
	    </td>
	  </tr>
	  <tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	  </tr>
	  <tr>
	    <td colspan="7" align="left"><p><strong>Link Verifikasi :<br>https://langitan.umaha.ac.id/verifikasi-kartu.php?x='.$id_tagihan.'</strong></p>
	    </td>
	  </tr>
</table>
';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// *******************************************************************
// HTML TIPS & TRICKS
// *******************************************************************

// REMOVE CELL PADDING
//
// $pdf->SetCellPadding(0);
// 
// This is used to remove any additional vertical space inside a 
// single cell of text.

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// REMOVE TAG TOP AND BOTTOM MARGINS
//
// $tagvs = array('p' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)));
// $pdf->setHtmlVSpace($tagvs);
// 
// Since the CSS margin command is not yet implemented on TCPDF, you
// need to set the spacing of block tags using the following method.

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// SET LINE HEIGHT
//
// $pdf->setCellHeightRatio(1.25);
// 
// You can use the following method to fine tune the line height
// (the number is a percentage relative to font height).

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// CHANGE THE PIXEL CONVERSION RATIO
//
// $pdf->setImageScale(0.47);
// 
// This is used to adjust the conversion ratio between pixels and 
// document units. Increase the value to get smaller objects.
// Since you are using pixel unit, this method is important to set the
// right zoom factor.
// 
// Suppose that you want to print a web page larger 1024 pixels to 
// fill all the available page width.
// An A4 page is larger 210mm equivalent to 8.268 inches, if you 
// subtract 13mm (0.512") of margins for each side, the remaining 
// space is 184mm (7.244 inches).
// The default resolution for a PDF document is 300 DPI (dots per 
// inch), so you have 7.244 * 300 = 2173.2 dots (this is the maximum 
// number of points you can print at 300 DPI for the given width).
// The conversion ratio is approximatively 1024 / 2173.2 = 0.47 px/dots
// If the web page is larger 1280 pixels, on the same A4 page the 
// conversion ratio to use is 1280 / 2173.2 = 0.59 pixels/dots

// *******************************************************************

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('KARTU-'.$nama_kartu.'('.strtoupper($mhs_nim).').pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+
