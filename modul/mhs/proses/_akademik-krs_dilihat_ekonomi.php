<?php
require('../../../config.php');
$db2 = new MyOracle();

$id_mhs = $user->MAHASISWA->ID_MHS;
$id_prodi = $user->MAHASISWA->ID_PROGRAM_STUDI;
$id_jenjang = $user->MAHASISWA->ID_JENJANG;
$sem_aktif = $user->SEMESTER->ID_SEMESTER_AKTIF;
$sem_lalu = $user->SEMESTER->ID_SEMESTER_LALU;
$sks_total = $user->MAHASISWA->SKS_TOTAL;
$ipk_mhs = $user->MAHASISWA->IPK;
$ips_mhs = $user->MAHASISWA->IPS;
$ips_bawah = $user->MAHASISWA->SKS_SEMESTER;
	
if($_POST["aksi"]=="tampil") {
	
	$isi = '
	<table>
	<tr>
		<th>No.</th>
		<th>KODE MTA</th>
		<th>NAMA MATA AJAR</th>
		<th>SKS MTA</font></th>
		<th>KELAS</th>
		<th>STATUS</th>
	</tr>
	';

	$kueri = "
	select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester,d.id_pengambilan_mk,c.no_kelas_mk,d.STATUS_APV_PENGAMBILAN_MK
	from mata_kuliah a, kelas_mk c, pengambilan_mk d, kurikulum_mk e
	where c.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and c.id_kelas_mk=d.id_kelas_mk
	and d.id_semester='".$sem_aktif."' and d.id_mhs='".$id_mhs."' and d.STATUS_APV_PENGAMBILAN_MK='1'
	order by a.kd_mata_kuliah
	";
	$hit=0;
	$result = $db->Query($kueri)or die("salah kueri 31 ");
	while($r = $db->FetchRow()) {
		$hit++;
		$nm_kelas = "";
		$result2 = $db2->Query("select nama_kelas from nama_kelas where id_nama_kelas='".$r[4]."'")or die("salah kueri 42 : ".$db2->Error());
		while($r2 = $db2->FetchRow()) {
			$nm_kelas = $r2[0];
		}

		if($r[5]=='1') {
			$status = "Approved";
		}else{
			$status = "Not Approved";
		}
		$isi .= '
			<tr>
				<td>'.$hit.'</td>
				<td>'.$r[0].'</td>
				<td>'.$r[1].'</td>
				<td>'.$r[2].'</td>
				<td>'.$nm_kelas.'</td>
				<td>'.$status.'</td>
			</tr>
		';
	}

	// ambil sks terambil
	$sks_terambil=0;
	$kueri = "
	select sum(d.kredit_semester)
	from pengambilan_mk a, kelas_mk b, mata_kuliah c, kurikulum_mk d
	where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah
	and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' and a.STATUS_APV_PENGAMBILAN_MK ='1'
	";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$sks_terambil = $r[0];
	}
	if(strlen($sks_terambil)==0) { $sks_terambil = '0'; }
	
	
	$bobot["A"] = 4; $bobot["AB"] = 3.5; $bobot["B"] = 3; $bobot["BC"] = 2.5; $bobot["C"] = 2; $bobot["D"] = 1; $bobot["E"] = 0;
	
	
	if($ips_mhs == ''){
		$ips_mhs = 0;
	}
	
	// ambil sks sems depan
		$kueri2 = "select max(sks_maksimal) as sks_maksimal from beban_sks where id_program_studi = ".$id_prodi." and ipk_minimum <= ".$ips_mhs."";
		$result = $db->Query($kueri2)or die("salah kueri : 131");
		while($r = $db->FetchRow()) {
			$sks_maks = $r[0];
		}
		if($id_jenjang=='1') {
			if(strlen($sks_maks)==0) { 
				$sks_maks = '0'; 
			}else{
				$sks_maks=$sks_maks+1; 
			}
		}else if($id_jenjang=='5') {
			if(strlen($sks_maks)==0) { 
				$sks_maks = '0'; 
			}else{
				$sks_maks=$sks_maks+2;
			}
		}
		if($sks_maks>24) {
			$sks_maks=24;
		}
		
		

	$isi .= '
	<tr>
		<td colspan="6">
		<b>IPK : '.$ipk_mhs.' <br/>
		<b>IPS : '.$ips_mhs.' <br/>
		<b>MAX SKS : '.$sks_maks.' <br/>
		TERAMBIL : '.$sks_terambil.' <br> 
		</td>
	</tr>
	<tr>
		<td colspan="6" align="center">
			<input type=button name="cetak5" value="Cetak" onclick="window.open(\'proses/_akademik-krs_cetak.php\',\'baru2\');">
		</td>
	</tr>
	</table>
	';
	echo $isi;

}

?>