<?php
require('../../../config.php');
$db2 = new MyOracle();

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('KRS');
$pdf->SetSubject('KRS Mahasiswa');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "20";
$title = "UNIVERSITAS AIRLANGGA";

$mhs_nama="";
$kueri = "select nm_pengguna from pengguna where id_pengguna='".$user->ID_PENGGUNA."'";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$mhs_nama = $r[0];
}

// ambil id_mhs
$kueri = "
select mhs.nim_mhs, upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs, f.nm_fakultas, 
f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas, f.id_fakultas,
mhs.id_program_studi,mhs.id_mhs
from mahasiswa mhs 
left join pengguna p on mhs.id_pengguna=p.id_pengguna 
left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
left join fakultas f on ps.id_fakultas=f.id_fakultas
left join jenjang j on ps.id_jenjang=j.id_jenjang
where mhs.id_pengguna='".$user->ID_PENGGUNA."'";
$result1 = $db->Query($kueri)or die("salah kueri : ".$kueri);
while($r1 = $db->FetchRow()) {
	$nim_mhs = $r1[0];
	$nm_mhs = $r1[1];
	$jenjang = $r1[2];
	$prodi = $r1[3];
	$status = $r1[4];
	$fak = $r1[5];
	$alm_fak = $r1[6];
	$pos_fak = $r1[7];
	$tel_fak = $r1[8];
	$fax_fak = $r1[9];
	$web_fak = $r1[10];
	$eml_fak = $r1[11];
	$kd_fak = $r1[12];
	$id_prodi = $r1[13];
	$id_mhs=$r1[14];
}

// ambil semester_aktif
$sem_aktif=""; $sem_aktif_tahun=""; $sem_aktif_semes="";
$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where status_aktif_semester='True' order by id_semester desc";
$result = $db->Query($kueri)or die ("salah kueri : ".$kueri);
while($r = $db->FetchRow()) {
	$sem_aktif = $r[0];
	$sem_aktif_tahun = $r[1];
	$sem_aktif_semes = $r[2];
}

// header krs
$thnsmt="select 'KARTU RENCANA STUDI TAHUN AJARAN '||tahun_ajaran||' SEMESTER '||upper(nm_semester) as nm_smt from semester where id_semester='".$sem_aktif."'";
$result = $db->Query($thnsmt)or die("salah kueri 1 ");
while($r = $db->FetchRow()) {
	$nm_smt = $r[0];
}

// ambil sks terambil
$sks_terambil=0;
$kueri = "
select sum(d.kredit_semester)
from pengambilan_mk a, kelas_mk b, mata_kuliah c, kurikulum_mk d
where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah
and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' and a.status_apv_pengambilan_mk='1'
";
$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
while($r = $db->FetchRow()) {
	$sks_terambil = $r[0];
}
if(strlen($sks_terambil)==0) { $sks_terambil = '0'; }

	$kemarin_thn=""; $kemarin_sem="";
	if($sem_aktif_semes=="Ganjil") {
		$kemarin_thn = $sem_aktif_tahun-1;
		$kemarin_sem = "Genap";
	}else if($sem_aktif_semes=="Genap") {
		$kemarin_thn = $sem_aktif_tahun;
		$kemarin_sem = "Ganjil";
	}
	
	// ambil semester_kemarin
		$kemarin_idsem="";
		$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		while($r = $db->FetchRow()) {
			$kemarin_idsem = $r[0];
		}

		// apakah semester tsb cuti ?
		$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		$r = $db->FetchRow();
		if($r[0]>0) { // maka cuti ke-1
			// ambil semester sebelum cuti
			$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$sem_aktif_tahun = $r[1];
				$sem_aktif_semes = $r[2];
			}
			if($sem_aktif_semes=="Ganjil") {
				$kemarin_thn = $sem_aktif_tahun-1;
				$kemarin_sem = "Genap";
			}else if($sem_aktif_semes=="Genap") {
				$kemarin_thn = $sem_aktif_tahun;
				$kemarin_sem = "Ganjil";
			}
			$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$kemarin_idsem = $r[0];
			}
			// apakah semester tsb cuti ?
			$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			$r = $db->FetchRow();
			if($r[0]>0) { // maka cuti ke-2
				// ambil semester sebelum cuti
				$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$sem_aktif_tahun = $r[1];
					$sem_aktif_semes = $r[2];
				}
				if($sem_aktif_semes=="Ganjil") {
					$kemarin_thn = $sem_aktif_tahun-1;
					$kemarin_sem = "Genap";
				}else if($sem_aktif_semes=="Genap") {
					$kemarin_thn = $sem_aktif_tahun;
					$kemarin_sem = "Ganjil";
				}
				$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$kemarin_idsem = $r[0];
				}
			}
		}

	// cek semester maba
	$sem_maba="";
	$kueri = "select thn_akademik_semester from semester where status_aktif_semester='True' and nm_semester='Ganjil'";
	$result = $db->Query($kueri)or die ("salah kueri : semester maba");
	while($r = $db->FetchRow()) {
		$sem_maba = $r[0];
	}
	
	// cek maba
	$agk_maba="";
	$kueri = "select thn_angkatan_mhs from mahasiswa where id_mhs='".$id_mhs."'";
	$result = $db->Query($kueri)or die ("salah kueri : agk maba");
	while($r = $db->FetchRow()) {
		$agk_maba = $r[0];
	}
	
	// ambil sks_maks
	$sks_maks = 0; $sks_total = 0; $ipk_mhs = 0; $ips_mhs = 0;
	$ips_atas='0'; $ips_bawah='0';
	$bobot["A"] = 4; $bobot["AB"] = 3.5; $bobot["B"] = 3; $bobot["BC"] = 2.5; $bobot["C"] = 2; $bobot["D"] = 1; $bobot["E"] = 0;
	$kueri2 = "
		select kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nilai_huruf,status_mkta,tipe_semester
		from(
		select pengambilan_mk.id_mhs,mata_kuliah.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
		kurikulum_mk.kredit_semester,case when kurikulum_mk.status_mkta>0
		and (nilai_huruf='E' or nilai_huruf is null) then 'E' else nilai_huruf end as nilai_huruf,
		case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
		coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
		nilai_angka,a.besar_nilai_mk as UTS,b.besar_nilai_mk as UAS,kurikulum_mk.status_mkta,
		row_number() over(partition by pengambilan_mk.id_mhs,kd_mata_kuliah order by nilai_huruf) rangking
		from pengambilan_mk
		left join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
		left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
		left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
		left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
		left join semester on pengambilan_mk.id_semester=semester.id_semester
		left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
		left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
		left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
		left join nilai_mk a on pengambilan_mk.id_pengambilan_mk=a.id_pengambilan_mk and a.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UTS')
		left join nilai_mk b on pengambilan_mk.id_pengambilan_mk=b.id_pengambilan_mk and b.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UAS')
		where group_semester||thn_akademik_semester in
		(select group_semester||thn_akademik_semester from semester where id_semester='".$kemarin_idsem."')
		and tipe_semester in ('UP','REG','RD')
		and status_apv_pengambilan_mk=1 and pengambilan_mk.status_hapus=0 and pengambilan_mk.status_pengambilan_mk !=0
		and mahasiswa.id_mhs='".$id_mhs."')
		where rangking=1
	";
	$result = $db->Query($kueri2)or die("salah kueri : 4");
	while($r = $db->FetchRow()) {
		if($r[4]>0) {
			if($r[3]=="E") {
				// tidak dihitung
			}else{
				$ips_bawah += $r[2];
				$ips_atas += ($bobot[$r[3]]*$r[2]);
			}
		}else{
			$ips_bawah += $r[2];
			$ips_atas += ($bobot[$r[3]]*$r[2]);
		}
	}
	if($ips_bawah>0) {
		$ips_mhs = number_format(($ips_atas/$ips_bawah),2);
	}else{
		$ips_mhs = '0.00';
	}
	// ambil sks sems depan
		$kueri2 = "select max(sks_maksimal) as sks_maksimal from beban_sks where id_program_studi = ".$id_prodi." and ipk_minimum <= ".$ips_mhs."";
		$result = $db->Query($kueri2)or die("salah kueri : 131");
		while($r = $db->FetchRow()) {
			$sks_maks = $r[0];
		}

		if($sks_maks>24) {
			$sks_maks=24;
		}

		if($agk_maba == $sem_maba){
			$sks_maks=20;
		}
		
		//perhitungan sks total dan ipk versi lukman
		$sql="select a.id_mhs,sum(a.kredit_semester) skstotal,
		round(sum(a.kredit_semester*(case a.nilai_huruf 
		when 'A' then 4 
		when 'AB' then 3.5 
		when 'B' then 3
		when 'BC' then 2.5
		when 'C' then 2
		when 'D' then 1
		end))/sum(a.kredit_semester),2) IPK
		from
		(
		select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
		select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
		from pengambilan_mk a 
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null and a.flagnilai=1
		) a
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where rangking=1 and id_mhs='{$id_mhs}'
		) a
		left join mahasiswa b on a.id_mhs=b.id_mhs
		left join program_studi f on b.id_program_studi=f.id_program_studi
		where a.id_mhs='{$id_mhs}'
		group by a.id_mhs order by a.id_mhs";
		$result2 = $db->Query($sql);
		while($r2 = $db->FetchRow()) {
			$sks_total = $r2[1];
			$ipk_mhs = number_format($r2[2],2);
		}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default footer
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('P', 'A4');

// draw jpeg image
$pdf->Image('../includes/logo_unair.png', 50, 50, 100, '', '', '', '', false, 72);

// set the starting point for the page content
$pdf->setPageMark();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="7">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="7" align="center"><b><u>'.$nm_smt.'</u></b></td>
  </tr>
  <tr>
    <td colspan="7">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
           <td width="15%">Nama</td>
           <td width="1%">:</td>
           <td width="38%">'.strtoupper($nm_mhs).'</td>
           <td width="15%">&nbsp;</td>
           <td width="15%">IPK</td>
           <td width="1%">:</td>
           <td width="15%">'.$ipk_mhs.'</td>
  </tr>
  <tr>
           <td>NIM</td>
           <td>:</td>
           <td>'.$nim_mhs.'</td>
           <td>&nbsp;</td>
           <td>IPS</td>
           <td>:</td>
           <td>'.$ips_mhs.'</td>
  </tr>
  <tr>
           <td>Program Studi</td>
           <td>:</td>
           <td>'.$jenjang.' '.$prodi.'</td>
           <td>&nbsp;</td>
           <td>SKS Maks</td>
           <td>:</td>
           <td>'.$sks_maks.'</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="7">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="7">
<table cellspacing="0" cellpadding="0" border="1" width="100%">
  <thead>
    <tr bgcolor="#000000" style="color:#FFF;">
      <td width="15%" align="center" bgcolor="#333333"><font color="#FFFFFF">Kode MA</font></td>
      <td width="55%" align="center" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Ajar</font></td>
      <td width="15%" align="center" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
      <td width="15%" align="center" bgcolor="#333333"><font color="#FFFFFF">Kelas</font></td>
    </tr>
  </thead>
  <tbody>
';

$kueri = "
select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester,d.id_pengambilan_mk,c.no_kelas_mk,d.status_apv_pengambilan_mk
from mata_kuliah a, kelas_mk c, pengambilan_mk d, kurikulum_mk e
where c.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and c.id_kelas_mk=d.id_kelas_mk
and d.id_semester='".$sem_aktif."' and d.id_mhs='".$id_mhs."' and d.status_apv_pengambilan_mk='1'
order by a.kd_mata_kuliah
";
$hit=0; $tot_sks=0;
$result = $db->Query($kueri)or die("salah kueri 31 ");
while($r = $db->FetchRow()) {
	$hit++;
	$tot_sks += $r[2];
	$nm_kelas = "";
	$result2 = $db2->Query("select nama_kelas from nama_kelas where id_nama_kelas='".$r[4]."'")or die("salah kueri 42 ");
	while($r2 = $db2->FetchRow()) {
		$nm_kelas = $r2[0];
	}
	$html .= '
		<tr>
			<td>&nbsp;&nbsp;'.$r[0].'</td>
			<td>&nbsp;&nbsp;'.$r[1].'</td>
			<td align="center" >'.$r[2].'</td>
			<td align="center" >'.$nm_kelas.'</td>
		</tr>
	';
}
$html .= '
	<tr>
		<td colspan="2" align="center">Total SKS</td>
		<td align="center">'.$tot_sks.'</td>
		<td align="center"></td>
	</tr>
  </tbody>
</table>
		</td>
	</tr>
  <tr>
    <td colspan="7">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="7">PENGATURAN KELAS MASIH DIMUNGKINKAN DILAKUKAN OLEH AKADEMIK, MOHON DICEK KEMBALI SETELAH MASA KRS BERAKHIR.</td>
  </tr>
  <tr>
    <td colspan="7">&nbsp;</td>
  </tr>
</table>
';
// ambil dosen wali
$kueri = "select case when p.gelar_belakang is not null then 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen, d.nip_dosen
from pengguna p
left join dosen d on p.id_pengguna=d.id_pengguna 
left join dosen_wali dw on d.id_dosen=dw.id_dosen
where dw.id_mhs='".$id_mhs."' and dw.status_dosen_wali=1
";
$result = $db->Query($kueri)or die ("salah kueri 48 ");
while($r = $db->FetchRow()) {
	$wali_nip = $r[1];
	$wali_nama = $r[0];
}

$bulan['01'] = "Januari"; $bulan['02'] = "Februari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "November";  $bulan['12'] = "Desember";
$html .= '
<table border="0" width="100%">
  <tr valign="top">
    <td width="50%">&nbsp;</td>
    <td width="50%" rowspan="3">
	  <p align="center">Surabaya, '.date("d").' '.$bulan[date("m")].' '.date("Y").'<br/>Dosen Wali,</p>
      <p align="center">&nbsp;</p>
      <p align="center">&nbsp;</p>
      <p align="center">'.$wali_nama.'<br/>NIP. '.$wali_nip.'</p>
	</td>
  </tr>
</table>

';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();
$pdf->Output('KRS.pdf', 'I');
