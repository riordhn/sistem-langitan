<?php
require('../../../config.php');

$id_pt = $id_pt_user;
$kota_pt = $PT->kota_pt;

if ($user->Role() != AUCC_ROLE_MAHASISWA) {
	echo "anda tidak berhal mengakses halaman ini";
	exit();
}

# untuk info perguruan tinggi by putra rieskha
#$id_pt = $user->ID_PERGURUAN_TINGGI;
#$kota_pt = $user->KOTA_PERGURUAN_TINGGI;
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('langitan');
$pdf->SetTitle('BIODATA');
$pdf->SetSubject('BIODATA');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logo-".$nama_singkat.".gif";
$logo_size = "25";
$title = strtoupper($nama_pt);

// query umum di PENGGUNA
	$mhs_nama = ''; $mhs_kelamin = ''; $mhs_agama = ''; $mhs_lahir_tgl = ''; $mhs_lahir_bln = ''; $mhs_lahir_thn = ''; $mhs_mail = ''; $mhs_maillain = ''; $mhs_blog = ''; $mhs_bloglain = ''; $mhs_url_sosmed = '';
	$kueri = "select nm_pengguna, kelamin_pengguna, id_agama, to_char(tgl_lahir_pengguna,'DD-MM-YYYY'), email_pengguna, email_alternate, id_fb, id_twitter, blog_pengguna, blog_alternate, url_sosmed from pengguna where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 10 : ");
	while($r = $db->FetchArray()) {
		$mhs_nama = $r[0];
		$mhs_kelamin = $r[1];
		$mhs_agama = $r[2];
		$mhs_lahir_tgl = substr($r[3],0,2);
		$mhs_lahir_bln = substr($r[3],3,2);
		$mhs_lahir_thn = substr($r[3],6,4);
		$mhs_mail = $r[4];
		//$mhs_mailpass = $r[5];
		$mhs_maillain = $r[5];
		/*$mhs_fb = $r[6];
		$mhs_tw = $r[7];*/
		$mhs_blog = $r[8];
		$mhs_bloglain = $r[9];
		$mhs_url_sosmed = $r[10];
	}

// query banyak hal di MAHASISWA
		$mhs_nim = ""; $mhs_lahir_prop = ""; $mhs_lahir_kota = ""; $mhs_alamat = ""; $mhs_hp = ""; $mhs_sma = ""; $mhs_thn_lulus = ""; $mhs_nem_nilai = ""; $mhs_nem_pelajaran = ""; $mhs_cita = ""; $ayah_nama = ""; $ayah_dikakhir = ""; $ayah_kerja = ""; $ayah_alamat = ""; $ibu_nama = ""; $ibu_dikakhir = ""; $ibu_kerja = ""; $ibu_alamat = ""; $penghasilan = ""; $mhs_prodi2 = ""; $mhs_alamat_prop = ""; $mhs_alamat_kota = ""; $mhs_alamat_prop_ayah = ""; $mhs_alamat_kota_ayah = ""; $mhs_alamat_prop_ibu = ""; $mhs_alamat_kota_ibu = ""; $mhs_alamat_asli = ""; $mhs_alamat_prop_asli = ""; $mhs_alamat_kota_asli = ""; $mhs_kebangsaan = "";

		//tambahan FIKRIE
		$ayah_hp = ""; $ibu_hp = ""; $ayah_lahir_tgl = ""; $ayah_lahir_bln = ""; $ayah_lahir_thn = ""; $ibu_lahir_tgl = ""; $ibu_lahir_bln = ""; $ibu_lahir_thn = ""; $ayah_penghasilan = ""; $ibu_penghasilan = ""; $ayah_disabilitas = ""; $ibu_disabilitas = "";

		$wali_nama = ""; $wali_dikakhir = ""; $wali_kerja = ""; $wali_alamat = ""; $mhs_alamat_prop_wali = ""; $mhs_alamat_kota_wali = ""; $wali_hp = ""; $wali_lahir_tgl = ""; $wali_lahir_bln = ""; $wali_lahir_thn = ""; $wali_penghasilan = ""; $wali_disabilitas = "";


	$kueri = "select nim_mhs, lahir_prop_mhs, lahir_kota_mhs, alamat_mhs, mobile_mhs, id_sekolah_asal_mhs, thn_lulus_mhs, nilai_skhun_mhs, nem_pelajaran_mhs, cita_cita_mhs, nm_ayah_mhs, pendidikan_ayah_mhs, pekerjaan_ayah_mhs, alamat_ayah_mhs, nm_ibu_mhs, pendidikan_ibu_mhs, pekerjaan_ibu_mhs, alamat_ibu_mhs, penghasilan_ortu_mhs, id_program_studi, ASAL_KOTA_MHS, ASAL_PROV_MHS, 
	ALAMAT_AYAH_MHS_PROV,ALAMAT_AYAH_MHS_KOTA, ALAMAT_IBU_MHS_PROV,ALAMAT_IBU_MHS_KOTA, 
	ALAMAT_ASAL_MHS,ALAMAT_ASAL_MHS_PROV,ALAMAT_ASAL_MHS_KOTA, ID_KEBANGSAAN, STATUS_MHS_ASING, id_c_mhs, no_rekening_debet, id_bank,
	TELP_AYAH_MHS, TELP_IBU_MHS, to_char(TGL_LAHIR_AYAH_MHS,'DD-MM-YYYY'), to_char(TGL_LAHIR_IBU_MHS,'DD-MM-YYYY'),
	PENGHASILAN_AYAH_MHS, PENGHASILAN_IBU_MHS, ID_DISABILITAS_AYAH_MHS, ID_DISABILITAS_IBU_MHS,
	NM_WALI_MHS, PENDIDIKAN_WALI_MHS, PEKERJAAN_WALI_MHS, ALAMAT_WALI_MHS, ALAMAT_WALI_MHS_PROV,ALAMAT_WALI_MHS_KOTA,
	TELP_WALI_MHS, to_char(TGL_LAHIR_WALI_MHS,'DD-MM-YYYY'), PENGHASILAN_WALI_MHS, ID_DISABILITAS_WALI_MHS
	from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 42 : ");
	while($r = $db->FetchRow()) {
		$mhs_nim = $r[0];
		$mhs_lahir_prop = $r[1];
		$mhs_lahir_kota = $r[2];
		$mhs_alamat = $r[3];
		$mhs_hp = $r[4];
		$mhs_sma = $r[5];
		$mhs_thn_lulus = $r[6];
		$mhs_nem_nilai = $r[7];
		$mhs_nem_pelajaran = $r[8];
		$mhs_cita = $r[9];
		$ayah_nama = $r[10];
		$ayah_dikakhir = $r[11];
		$ayah_kerja = $r[12];
		$ayah_alamat = $r[13];
		$ibu_nama = $r[14];
		$ibu_dikakhir = $r[15];
		$ibu_kerja = $r[16];
		$ibu_alamat = $r[17];
		$penghasilan = $r[18];
		$mhs_prodi2 = $r[19];
		$mhs_alamat_kota = $r[20];
		$mhs_alamat_prop = $r[21];
		$mhs_alamat_prop_ayah = $r[22];
		$mhs_alamat_kota_ayah = $r[23];
		$mhs_alamat_prop_ibu = $r[24];
		$mhs_alamat_kota_ibu = $r[25];
		$mhs_alamat_asli = $r[26];
		$mhs_alamat_prop_asli = $r[27];
		$mhs_alamat_kota_asli = $r[28];
		$mhs_kebangsaan = $r[29];
		$status_mhs_asing = $r[30];
		$id_c_mhs = $r[31];
		$autodebet = $r[32];
		$idbank = $r[33];
		//tambahan FIKRIE
		$ayah_hp = $r[34];
		$ibu_hp = $r[35];
		$ayah_lahir_tgl = substr($r[36],0,2);
		$ayah_lahir_bln = substr($r[36],3,2);
		$ayah_lahir_thn = substr($r[36],6,4);
		$ibu_lahir_tgl = substr($r[37],0,2);
		$ibu_lahir_bln = substr($r[37],3,2);
		$ibu_lahir_thn = substr($r[37],6,4);
		$ayah_penghasilan = $r[38];
		$ibu_penghasilan = $r[39];
		$ayah_disabilitas = $r[40];
		$ibu_disabilitas = $r[41];

		$wali_nama = $r[42];
		$wali_dikakhir = $r[43];
		$wali_kerja = $r[44];
		$wali_alamat = $r[45];
		$mhs_alamat_prop_wali = $r[46];
		$mhs_alamat_kota_wali = $r[47];
		$wali_hp = $r[48];
		$wali_lahir_tgl = substr($r[49],0,2);
		$wali_lahir_bln = substr($r[49],3,2);
		$wali_lahir_thn = substr($r[49],6,4);
		$wali_penghasilan = $r[50];
		$wali_disabilitas = $r[51];
	}

// query panggil dosen wali
$nm_doli1="select case when p.gelar_belakang is not null then 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen, d.nip_dosen
from pengguna p
left join dosen d on p.id_pengguna=d.id_pengguna 
left join dosen_wali dw on d.id_dosen=dw.id_dosen
left join mahasiswa m on dw.id_mhs=m.id_mhs 
where m.nim_mhs='".$mhs_nim."' and dw.status_dosen_wali=1 and rownum=1
order by dw.id_dosen_wali desc";
$result7 = $db->Query($nm_doli1)or die("salah kueri 8 ");
while($r7 = $db->FetchRow()) {
	$nm_doli2 = $r7[0];
	$nip_doli2 = $r7[1];
}

// query kebutuhan join MAHASISWA
if($mhs_kelamin=='1'){$mhs_kelamin = 'Laki-laki';}elseif($mhs_kelamin=='2'){$mhs_kelamin = 'Perempuan';}

$mhs_lahir_kota_nama='';
$kueri = "select id_kota,nm_kota from kota where id_kota='".$mhs_lahir_kota."' order by nm_kota, tipe_dati2";
$result = $db->Query($kueri)or die("salah kueri 162 : ");
while($r = $db->FetchRow()) {
	$mhs_lahir_kota_nama = $r[1];
}

$mhs_alamat_prop_nama='';
$kueri = "select id_provinsi,nm_provinsi from provinsi where id_provinsi='".$mhs_alamat_prop."'";
$result = $db->Query($kueri)or die("salah kueri 162 : ");
while($r = $db->FetchRow()) {
	$mhs_alamat_prop_nama = $r[1];
}

$mhs_alamat_kota_nama='';
$kueri = "select id_kota,tipe_dati2||' '||nm_kota from kota where id_kota='".$mhs_alamat_kota."' order by nm_kota, tipe_dati2";
$result = $db->Query($kueri)or die("salah kueri 162 : ");
while($r = $db->FetchRow()) {
	$mhs_alamat_kota_nama = $r[1];
}

$mhs_alamat_prop_asli_nama='';
$kueri = "select id_provinsi,nm_provinsi from provinsi where id_provinsi='".$mhs_alamat_prop_asli."'";
$result = $db->Query($kueri)or die("salah kueri 162 : ");
while($r = $db->FetchRow()) {
	$mhs_alamat_prop_asli_nama = $r[1];
}

$mhs_alamat_kota_asli_nama='';
$kueri = "select id_kota,tipe_dati2||' '||nm_kota from kota where id_kota='".$mhs_alamat_kota_asli."' order by nm_kota, tipe_dati2";
$result = $db->Query($kueri)or die("salah kueri 162 : ");
while($r = $db->FetchRow()) {
	$mhs_alamat_kota_asli_nama = $r[1];
}

$kueri = "select id_agama,nm_agama from agama where id_agama='".$mhs_agama."' ";
$result = $db->Query($kueri)or die("salah kueri 162 : ");
while($r = $db->FetchRow()) {
	$mhs_agama = $r[1];
}

$kueri = "select id_sekolah,nm_sekolah from sekolah where id_sekolah='".$mhs_sma."' ";
$result = $db->Query($kueri)or die("salah kueri 162 : ");
while($r = $db->FetchRow()) {
	$mhs_sma = $r[1];
}


// query kebutuhan join pendidikan ortu
$kueri = "select id_pendidikan_akhir, nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir='".$ayah_dikakhir."' ";
$result = $db->Query($kueri)or die("salah kueri 162 : ");
while($r = $db->FetchRow()) {
	$ayah_dikakhir = $r[1];
}

$kueri = "select id_pendidikan_akhir, nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir='".$ibu_dikakhir."' ";
$result = $db->Query($kueri)or die("salah kueri 162 : ");
while($r = $db->FetchRow()) {
	$ibu_dikakhir = $r[1];
}

$kueri = "select id_pendidikan_akhir, nama_pendidikan_akhir from pendidikan_akhir where id_pendidikan_akhir='".$wali_dikakhir."' ";
$result = $db->Query($kueri)or die("salah kueri 162 : ");
while($r = $db->FetchRow()) {
	$wali_dikakhir = $r[1];
}

// query kebutuhan join pekerjaan ortu
$kueri = "select id_pekerjaan, nm_pekerjaan from pekerjaan where id_pekerjaan='".$ayah_kerja."' ";
$result = $db->Query($kueri)or die("salah kueri 162 : ");
while($r = $db->FetchRow()) {
	$ayah_kerja = $r[1];
}

$kueri = "select id_pekerjaan, nm_pekerjaan from pekerjaan where id_pekerjaan='".$ibu_kerja."' ";
$result = $db->Query($kueri)or die("salah kueri 162 : ");
while($r = $db->FetchRow()) {
	$ibu_kerja = $r[1];
}

$kueri = "select id_pekerjaan, nm_pekerjaan from pekerjaan where id_pekerjaan='".$wali_kerja."' ";
$result = $db->Query($kueri)or die("salah kueri 162 : ");
while($r = $db->FetchRow()) {
	$wali_kerja = $r[1];
}

// query kebutuhan convert bulan lahir
if($mhs_lahir_bln=='01'){ $mhs_lahir_bln = 'Januari'; }
elseif($mhs_lahir_bln=='02'){ $mhs_lahir_bln = 'Februari'; }
elseif($mhs_lahir_bln=='03'){ $mhs_lahir_bln = 'Maret'; }
elseif($mhs_lahir_bln=='04'){ $mhs_lahir_bln = 'April'; }
elseif($mhs_lahir_bln=='05'){ $mhs_lahir_bln = 'Mei'; }
elseif($mhs_lahir_bln=='06'){ $mhs_lahir_bln = 'Juni'; }
elseif($mhs_lahir_bln=='07'){ $mhs_lahir_bln = 'Juli'; }
elseif($mhs_lahir_bln=='08'){ $mhs_lahir_bln = 'Agustus'; }
elseif($mhs_lahir_bln=='09'){ $mhs_lahir_bln = 'September'; }
elseif($mhs_lahir_bln=='10'){ $mhs_lahir_bln = 'Oktober'; }
elseif($mhs_lahir_bln=='11'){ $mhs_lahir_bln = 'November'; }
elseif($mhs_lahir_bln=='12'){ $mhs_lahir_bln = 'Desember'; }

if($ayah_lahir_bln=='01'){ $ayah_lahir_bln = 'Januari'; }
elseif($ayah_lahir_bln=='02'){ $ayah_lahir_bln = 'Februari'; }
elseif($ayah_lahir_bln=='03'){ $ayah_lahir_bln = 'Maret'; }
elseif($ayah_lahir_bln=='04'){ $ayah_lahir_bln = 'April'; }
elseif($ayah_lahir_bln=='05'){ $ayah_lahir_bln = 'Mei'; }
elseif($ayah_lahir_bln=='06'){ $ayah_lahir_bln = 'Juni'; }
elseif($ayah_lahir_bln=='07'){ $ayah_lahir_bln = 'Juli'; }
elseif($ayah_lahir_bln=='08'){ $ayah_lahir_bln = 'Agustus'; }
elseif($ayah_lahir_bln=='09'){ $ayah_lahir_bln = 'September'; }
elseif($ayah_lahir_bln=='10'){ $ayah_lahir_bln = 'Oktober'; }
elseif($ayah_lahir_bln=='11'){ $ayah_lahir_bln = 'November'; }
elseif($ayah_lahir_bln=='12'){ $ayah_lahir_bln = 'Desember'; }

if($ibu_lahir_bln=='01'){ $ibu_lahir_bln = 'Januari'; }
elseif($ibu_lahir_bln=='02'){ $ibu_lahir_bln = 'Februari'; }
elseif($ibu_lahir_bln=='03'){ $ibu_lahir_bln = 'Maret'; }
elseif($ibu_lahir_bln=='04'){ $ibu_lahir_bln = 'April'; }
elseif($ibu_lahir_bln=='05'){ $ibu_lahir_bln = 'Mei'; }
elseif($ibu_lahir_bln=='06'){ $ibu_lahir_bln = 'Juni'; }
elseif($ibu_lahir_bln=='07'){ $ibu_lahir_bln = 'Juli'; }
elseif($ibu_lahir_bln=='08'){ $ibu_lahir_bln = 'Agustus'; }
elseif($ibu_lahir_bln=='09'){ $ibu_lahir_bln = 'September'; }
elseif($ibu_lahir_bln=='10'){ $ibu_lahir_bln = 'Oktober'; }
elseif($ibu_lahir_bln=='11'){ $ibu_lahir_bln = 'November'; }
elseif($ibu_lahir_bln=='12'){ $ibu_lahir_bln = 'Desember'; }

if($wali_lahir_bln=='01'){ $wali_lahir_bln = 'Januari'; }
elseif($wali_lahir_bln=='02'){ $wali_lahir_bln = 'Februari'; }
elseif($wali_lahir_bln=='03'){ $wali_lahir_bln = 'Maret'; }
elseif($wali_lahir_bln=='04'){ $wali_lahir_bln = 'April'; }
elseif($wali_lahir_bln=='05'){ $wali_lahir_bln = 'Mei'; }
elseif($wali_lahir_bln=='06'){ $wali_lahir_bln = 'Juni'; }
elseif($wali_lahir_bln=='07'){ $wali_lahir_bln = 'Juli'; }
elseif($wali_lahir_bln=='08'){ $wali_lahir_bln = 'Agustus'; }
elseif($wali_lahir_bln=='09'){ $wali_lahir_bln = 'September'; }
elseif($wali_lahir_bln=='10'){ $wali_lahir_bln = 'Oktober'; }
elseif($wali_lahir_bln=='11'){ $wali_lahir_bln = 'November'; }
elseif($wali_lahir_bln=='12'){ $wali_lahir_bln = 'Desember'; }

// query kebutuhan khusus diluar 2 kueri besar di atas
	$kueri = "select j.nm_jenjang || ' ' || ps.nm_program_studi as nm_prodi, m.thn_angkatan_mhs, 
				m.tgl_terdaftar_mhs, nk.nama_kelas, m.nik_mhs, m.nm_jenis_tinggal, m.kewarganegaraan,
				m.nisn, m.no_ijazah, m.nomor_kps, sb.nm_sumber_biaya, m.status_kerja, 
				m.nik_ayah_mhs, m.nik_ibu_mhs, m.nik_wali_mhs, jt.nm_jenis_transportasi
			from pengguna p 
			join mahasiswa m on m.id_pengguna = p.id_pengguna
			join program_studi ps on ps.id_program_studi = m.id_program_studi
			join jenjang j on j.id_jenjang = ps.id_jenjang
			left join mahasiswa_kelas mkel on mkel.id_mhs = m.id_mhs
			left join nama_kelas nk on nk.id_nama_kelas = mkel.id_nama_kelas
			left join sumber_biaya sb on sb.id_sumber_biaya = m.sumber_biaya
			left join jenis_transportasi jt on jt.id_jenis_transportasi = m.id_jenis_transportasi
			where p.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 10 : ");
	while($r = $db->FetchArray()) {
		$mhs_prodi = $r[0];
		$mhs_angkatan = $r[1];
		$tgl_terdaftar = $r[2];
		$kelompok_kelas = $r[3];
		$nik_mhs = $r[4];
		$nm_jenis_tinggal = $r[5];
		$kewarganegaraan = $r[6];
		$nisn_mhs = $r[7];
		$no_ijazah_mhs = $r[8];
		$nomor_kps = $r[9];
		$sumber_biaya_mhs = $r[10];
		$status_kerja_mhs = $r[11];
		$ayah_nik = $r[12];
		$ibu_nik = $r[13];
		$wali_nik = $r[14];
		$nm_jenis_transportasi = $r[15];
	}

// query kebutuhan kewarganegaraan
if($kewarganegaraan=='1'){ $kewarganegaraan = 'WNI'; }
elseif($kewarganegaraan=='2'){ $kewarganegaraan = 'WNA'; }
elseif($kewarganegaraan=='3'){ $kewarganegaraan = 'WNI Keturunan'; }

// query kebutuhan status kerja
if($status_kerja_mhs=='0'){ $status_kerja_mhs = 'Belum Bekerja'; }
elseif($status_kerja_mhs=='1'){ $status_kerja_mhs = 'Sudah Bekerja'; }

// query kebutuhan identifikasi KPS (Kartu Sosial)
if(empty($nomor_kps) || $nomor_kps == '0' || $nomor_kps == '-'){
	$mhs_kps = 'Tidak';
}
else{
	$mhs_kps = 'Ya, No. KPS : '.$nomor_kps;
}

$tahun_sekarang = date('Y');
$bulan_sekarang = date('m');
$tgl_sekarang = date('d');
$jam_sekarang = date('H:i:s');

if($bulan_sekarang=='01'){ $bulan_sekarang = 'Januari'; }
elseif($bulan_sekarang=='02'){ $bulan_sekarang = 'Februari'; }
elseif($bulan_sekarang=='03'){ $bulan_sekarang = 'Maret'; }
elseif($bulan_sekarang=='04'){ $bulan_sekarang = 'April'; }
elseif($bulan_sekarang=='05'){ $bulan_sekarang = 'Mei'; }
elseif($bulan_sekarang=='06'){ $bulan_sekarang = 'Juni'; }
elseif($bulan_sekarang=='07'){ $bulan_sekarang = 'Juli'; }
elseif($bulan_sekarang=='08'){ $bulan_sekarang = 'Agustus'; }
elseif($bulan_sekarang=='09'){ $bulan_sekarang = 'September'; }
elseif($bulan_sekarang=='10'){ $bulan_sekarang = 'Oktober'; }
elseif($bulan_sekarang=='11'){ $bulan_sekarang = 'November'; }
elseif($bulan_sekarang=='12'){ $bulan_sekarang = 'Desember'; }

$biomhs="select mhs.nim_mhs, upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs, 
			 case when f.id_fakultas=9 then 'PROGRAM '||f.nm_fakultas else 'FAKULTAS '||f.nm_fakultas end, 
			f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas, f.id_fakultas,
			mhs.id_program_studi,mhs.id_mhs
			from mahasiswa mhs 
			left join pengguna p on mhs.id_pengguna=p.id_pengguna and p.id_perguruan_tinggi = '{$id_pt}' 
			left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			where mhs.nim_mhs='".$mhs_nim."'";
$result1 = $db->Query($biomhs)or die("salah kueri 2 ");
while($r1 = $db->FetchRow()) {
	$nim_mhs = $r1[0];
	$nm_mhs = $r1[1];
	$jenjang = $r1[2];
	$prodi = $r1[3];
	$status = $r1[4];
	$fak = $r1[5];
	$alm_fak = $r1[6];
	$pos_fak = $r1[7];
	$tel_fak = $r1[8];
	$fax_fak = $r1[9];
	$web_fak = $r1[10];
	$eml_fak = $r1[11];
	$kd_fak = $r1[12];
	$kd_prodi = $r1[13];
	$id_mhs=$r1[14];
}

	$noujian = $user->MAHASISWA->NO_UJIAN;
	$fotomahasiswa = "../../../foto_mhs/{$nama_singkat}/".$mhs_nim.".jpg";
	## jika extension .jpg dalam huruf kapital
	if (file_exists($fotomahasiswa) == 0) 
	{
		$fotomahasiswa = "../../../foto_mhs/{$nama_singkat}/".$mhs_nim.".JPG";
	}

	if (file_exists($fotomahasiswa) == 0) 
	{
		$fotomahasiswa = "../../../foto_mhs/{$nama_singkat}/".$noujian.".JPG";
	}

	if (file_exists($fotomahasiswa) == 0) 
	{
		$fotomahasiswa = "../../../foto_mhs/{$nama_singkat}/foto-tidak-ditemukan.png";
	}


$content = strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default footer
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('P', 'A4');

// draw jpeg image
//$pdf->Image('../includes/draft.png', 30, 70, 140, '40', '', '', '', false, 72);

// set the starting point for the page content
$pdf->setPageMark();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="center"><span style="font-size: large;"><b>BIODATA MAHASISWA</b></span></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="left"><b>Data Diri Mahasiswa</b></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td width="26%">&nbsp;&nbsp;NIM / NPM</td>
	<td width="2%" align="center">:</td>
	<td width="52%">'.$mhs_nim.'</td>
	<td rowspan="6" width="20%"><img src="'.$fotomahasiswa.'" width="120" height="150"></td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Program Studi</td>
	<td width="2%" align="center">:</td>
	<td width="52%">'.$mhs_prodi.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Angkatan</td>
	<td width="2%" align="center">:</td>
	<td width="52%">'.$mhs_angkatan.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Tanggal Terdaftar</td>
	<td width="2%" align="center">:</td>
	<td width="52%">'.$tgl_terdaftar.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Dosen Wali</td>
	<td width="2%" align="center">:</td>
	<td width="52%">'.$nm_doli2.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Kelompok Kelas</td>
	<td width="2%" align="center">:</td>
	<td width="52%">'.$kelompok_kelas.'</td>
  </tr>
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td width="26%">&nbsp;&nbsp;Nama</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_nama.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Jenis Kelamin</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_kelamin.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Tempat Tanggal Lahir</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_lahir_kota_nama.', '.$mhs_lahir_tgl.' '.$mhs_lahir_bln.' '.$mhs_lahir_thn.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;NIK (KTP)</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$nik_mhs.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Alamat Tinggal Sekarang</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_alamat.', '.$mhs_alamat_kota_nama.' - '.$mhs_alamat_prop_nama.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Jenis Tinggal</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$nm_jenis_tinggal.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Jenis Transportasi</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$nm_jenis_transportasi.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Alamat Asli (KTP)</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_alamat_asli.', '.$mhs_alamat_kota_asli_nama.' - '.$mhs_alamat_prop_asli_nama.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Kewarganegaraan</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$kewarganegaraan.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Agama</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_agama.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;NISN</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$nisn_mhs.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Asal Sekolah</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_sma.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Tahun Lulus SMA</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_thn_lulus.' - No Ijazah : '.$no_ijazah_mhs.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;No Handphone 1</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_hp.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Email Mahasiswa</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_mail.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Email Pribadi</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_maillain.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Blog Mahasiswa</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_blog.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Blog Pribadi</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_bloglain.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;FB / Google+ / Lainnya</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_url_sosmed.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Penerima KPS ?</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$mhs_kps.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Sumber Biaya Kuliah</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$sumber_biaya_mhs.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Status Kerja</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$status_kerja_mhs.'</td>
  </tr>
</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// Next page
$pdf->AddPage();

$html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="center"><span style="font-size: large;"><b>BIODATA ORANG TUA</b></span></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="left"><b>Data Ayah</b></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td width="26%">&nbsp;&nbsp;NIK (KTP)</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$ayah_nik.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Nama</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$ayah_nama.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Tanggal Lahir</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$ayah_lahir_tgl.' '.$ayah_lahir_bln.' '.$ayah_lahir_thn.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Pendidikan</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$ayah_dikakhir.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Pekerjaan</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$ayah_kerja.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Penghasilan</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$ayah_penghasilan.'</td>
  </tr>
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="left"><b>Data Ibu</b></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td width="26%">&nbsp;&nbsp;NIK (KTP)</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$ibu_nik.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Nama</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$ibu_nama.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Tanggal Lahir</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$ibu_lahir_tgl.' '.$ibu_lahir_bln.' '.$ibu_lahir_thn.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Pendidikan</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$ibu_dikakhir.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Pekerjaan</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$ibu_kerja.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Penghasilan</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$ibu_penghasilan.'</td>
  </tr>
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" align="left"><b>Data Wali (Jika Ada)</b></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td width="26%">&nbsp;&nbsp;NIK (KTP)</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$wali_nik.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Nama</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$wali_nama.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Tanggal Lahir</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$wali_lahir_tgl.' '.$wali_lahir_bln.' '.$wali_lahir_thn.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Pendidikan</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$wali_dikakhir.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Pekerjaan</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$wali_kerja.'</td>
  </tr>
  <tr>
    <td width="26%">&nbsp;&nbsp;Penghasilan</td>
	<td width="2%" align="center">:</td>
	<td width="72%">'.$wali_penghasilan.'</td>
  </tr>
</table>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="left"><i>Waktu cetak PDF '.$tgl_sekarang.' '.$bulan_sekarang.' '.$tahun_sekarang.' '.$jam_sekarang.'</i></td>
  </tr>
</table>
';


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('BIODATA-'.strtoupper($mhs_nim).'.pdf', 'I');