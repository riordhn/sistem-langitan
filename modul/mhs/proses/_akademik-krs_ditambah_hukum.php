<?php
require('../../../config.php');
require('../includes/ceking2.php');


$db2 = new MyOracle();
$db3 = new MyOracle();


$id_prodi = $user->MAHASISWA->ID_PROGRAM_STUDI;
$id_jenjang = $user->MAHASISWA->ID_JENJANG;
$id_fakultas = $user->MAHASISWA->ID_FAKULTAS;
$ips_mhs = $user->MAHASISWA->IPS;
$ips_bawah = $user->MAHASISWA->SKS_SEMESTER;
$sks_total = $user->MAHASISWA->SKS_TOTAL;
$ipk_mhs = $user->MAHASISWA->IPK;

$sem_aktif = $user->SEMESTER->ID_SEMESTER_AKTIF;
$sem_lalu = $user->SEMESTER->ID_SEMESTER_LALU;
$sem_aktif_semes = $user->SEMESTER->NAMA_SEMESTER_AKTIF;
$sem_aktif_tahun = $user->SEMESTER->TAHUN_AKADEMIK_SEMESTER_AKTIF;


$id_mhs = $user->MAHASISWA->ID_MHS;
$nim_mhs = $user->MAHASISWA->NIM_MHS;
$mhs_status = $user->MAHASISWA->STATUS;

if($_POST["aksi"]=="tampil") {
	
// Yudi Sulistya, 01 Aug 2013 (tablesorter)
echo '
<link rel="stylesheet" type="text/css" href="includes/sortable/themes/brown/style.css" />
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0],[3,0]], widgets: ["zebra"]
		}
	);
}
);
</script>
';


	$bobot["A"] = 4; $bobot["AB"] = 3.5; $bobot["B"] = 3; $bobot["BC"] = 2.5; $bobot["C"] = 2; $bobot["D"] = 1; $bobot["E"] = 0;

	if($ips_mhs == ''){
		$ips_mhs = 0;
	}


	// ambil sks sems depan
		$kueri2 = "select max(sks_maksimal) as sks_maksimal from beban_sks where id_program_studi = ".$id_prodi." and ipk_minimum <= ".$ips_mhs."";
		//echo $kueri2;
		$result = $db->Query($kueri2)or die("salah kueri : 5");
		while($r = $db->FetchRow()) {
			$sks_maks = $r[0];
		}
		$sks_maks = $sks_maks+1;
		if($sks_maks>24) {
			$sks_maks=24;
		}

	
	$isi = '
	<form name="frmkrstambah" id="frmkrstambah">
	<table id="myTable" class="tablesorter">
	<thead>
	<tr>
		<th>KODE MTK</th>
		<th>NAMA MATA AJAR</th>
		<th>SKS MTA</th>
		<th>KELAS</th>
		<th>KAPASITAS</th>
		<th>TERISI</th>
		<th>JADWAL</th>
		<th>AKSI</th>
	</tr>
	</thead>
	<tbody>
	';
	// mk yg sudah diambil tidak dimunculkan
	$kueri = "select b.id_kurikulum_mk from pengambilan_mk a, kelas_mk b where a.id_kelas_mk=b.id_kelas_mk and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' ";
	$id_kurikulum_sudahada = "";
	$result = $db->Query($kueri)or die("salah kueri 58 : ");
	while($r = $db->FetchRow()) {
		$id_kurikulum_sudahada .= ",".$r[0];
	}
	$id_kurikulum_sudahada = substr($id_kurikulum_sudahada, 1);
	if(strlen($id_kurikulum_sudahada) == 0){
		$id_kurikulum_sudahada = "0";
	}

	// bila nilai B, maka tidak boleh diambil lagi
	$mk_tidak_boleh = "";
	$kueri = "
	select d.kd_mata_kuliah
	from pengambilan_mk a, kelas_mk b, kurikulum_mk c, mata_kuliah d
	where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=c.id_kurikulum_mk and c.id_mata_kuliah=d.id_mata_kuliah 
	and a.id_mhs='".$id_mhs."' and a.nilai_huruf='B'
	";
	$result = $db->Query($kueri)or die("salah kueri 75 : ");
	while($r = $db->FetchRow()) {
		$mk_tidak_boleh .= ",'".$r[0]."'";
	}

	// bila sudah kadaluarsa, maka tidak boleh diambil lagi, KECUALI YANG NILAINYA E MASIH BOLEH
	// definisi kadaluarsa, dilihat dari pertama dia ambil
	if($sem_aktif_semes == 'Genap') {
		$kadaluarsa = ($sem_aktif_tahun-2)."Genap";
	}else if($sem_aktif_semes == 'Ganjil') {
		$kadaluarsa = ($sem_aktif_tahun-2)."Ganjil";
	}
	$kueri = "
	select d.kd_mata_kuliah, e.thn_akademik_semester, e.nm_semester
	from pengambilan_mk a, kurikulum_mk c, mata_kuliah d, semester e
	where a.id_kurikulum_mk=c.id_kurikulum_mk and c.id_mata_kuliah=d.id_mata_kuliah and a.id_semester=e.id_semester
	and a.id_mhs='".$id_mhs."' and e.id_semester!='".$sem_aktif."' and concat(e.thn_akademik_semester, e.nm_semester)<='".$kadaluarsa."'
	and a.nilai_huruf not in ('E','K')
	order by e.thn_akademik_semester asc, e.nm_semester asc
	";
	$result = $db->Query($kueri)or die("salah kueri 75 : ");
	while($r = $db->FetchRow()) {
		$mk_tidak_boleh .= ",'".$r[0]."'";
	}


	// bila sks total mhs < 110, maka tidak boleh ambil skripsi dan kkn
	if($sks_total < 110) {
		$mk_tidak_boleh .= ",'SKH499','KKH101'";
	}

	if(strlen($mk_tidak_boleh)>0) {
		$mk_tidak_boleh = " and a.kd_mata_kuliah not in (".substr($mk_tidak_boleh,1).") ";
	}else if(strlen($mk_tidak_boleh)==0) {
		$mk_tidak_boleh .= "";
	}

	
	$kueri = "
	select a.kd_mata_kuliah, a.nm_mata_kuliah, d.kredit_semester, d.id_mata_kuliah, d.id_kurikulum_mk, c.no_kelas_mk, c.id_kelas_mk,
	c.terisi_kelas_mk, (case when b.quota = 0 or b.quota is null then c.kapasitas_kelas_mk else b.quota end) as kapasitas_kelas_mk
	from mata_kuliah a, krs_prodi b, kelas_mk c, kurikulum_mk d
	where c.id_kurikulum_mk=d.id_kurikulum_mk and a.id_mata_kuliah=d.id_mata_kuliah and c.id_kelas_mk=b.id_kelas_mk
	and b.id_semester='".$sem_aktif."' and b.id_program_studi='".$id_prodi."' and (c.kapasitas_kelas_mk is not null or c.kapasitas_kelas_mk!=0)
	and d.id_kurikulum_mk not in (".$id_kurikulum_sudahada.") ".$mk_tidak_boleh."
	order by a.kd_mata_kuliah, c.no_kelas_mk
	";
	$hit=0;
	$result = $db->Query($kueri)or die("salah kueri 25 : ".$db->Error());
	while($r = $db->FetchRow()) {
		$hit++;
		$nm_kelas = "";
		$result2 = $db2->Query("select nama_kelas from nama_kelas where id_nama_kelas='".$r[5]."'")or die("salah kueri 42 : ");
		while($r2 = $db2->FetchRow()) {
			$nm_kelas = $r2[0];
		}
		$kelas_terisi = "0";
		$result2 = $db2->Query("select count(*) from pengambilan_mk 
								left join mahasiswa on mahasiswa.id_mhs = pengambilan_mk.id_mhs
								where pengambilan_mk.id_kelas_mk='".$r[6]."' and mahasiswa.id_program_studi = '".$id_prodi."'")or die("salah kueri 42 : ");
		while($r2 = $db2->FetchRow()) {
			$kelas_terisi = $r2[0];
		}
		// AMBIL JADWAL
		$jadwalnya = "";
		$result2 = $db2->Query("select a.nm_jadwal_jam,b.id_jadwal_hari,b.id_jadwal_jam from aucc.jadwal_jam a, aucc.jadwal_kelas b where a.id_jadwal_jam=b.id_jadwal_jam and b.id_kelas_mk='".$r[6]."'")or die("salah kueri 121 : ");
		while($r2 = $db2->FetchRow()) {
			if($r2[1]=='1') {
				$harinya = "Minggu";
			}else if($r2[1]=='2') {
				$harinya = "Senin";
			}else if($r2[1]=='3') {
				$harinya = "Selasa";
			}else if($r2[1]=='4') {
				$harinya = "Rabu";
			}else if($r2[1]=='5') {
				$harinya = "Kamis";
			}else if($r2[1]=='6') {
				$harinya = "Jumat";
			}else if($r2[1]=='7') {
				$harinya = "Sabtu";
			}
			$jadwalnya .= "<br>".$harinya." ".$r2[0];
		}
		$jadwalnya = substr($jadwalnya,4);

		$isi .= '
			<tr>
				<td>'.$r[0].'</td>
				<td>'.$r[1].'</td>
				<td>'.$r[2].'</td>
				<td>'.$nm_kelas.'</td>
				<td>'.$r[8].'</td>
				<td>'.$kelas_terisi.'</td>
				<td>'.$jadwalnya.'</td>
				<td>
				';
				if($_SESSION['STATUS_KRS'] == 1){
				if($kelas_terisi >= $r[8] ){
					$isi .= '';
				}else{
					$isi .= '<input type="button" name="simpan" value="Ambil" onclick="krstambah_kirim('.$r[6].', '.$r[4].')" >';
				}
				}
				$isi .= '
				</td>
			</tr>
		';
	}
	
	// ambil sks terambil
	$sks_terambil=0;
	$kueri = "
	select sum(d.kredit_semester)
	from pengambilan_mk a, kelas_mk b, mata_kuliah c, kurikulum_mk d
	where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=b.id_semester
	and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' and a.status_pengambilan_mk!='4'
	";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$sks_terambil = $r[0];
	}
	if(strlen($sks_terambil)==0) { $sks_terambil = '0'; }

	$sks_sisa = $sks_maks-$sks_terambil;
	
	$isi .= '
	</tbody>
	<tr>
		<td colspan="9">
		<b>IPK : '.$ipk_mhs.' <br/>
		<b>IPS : '.$ips_mhs.' <br/>
		<b>MAX SKS : '.$sks_maks.' <br/>
		TERAMBIL : '.$sks_terambil.' <br> 
		SISA : '.$sks_sisa.'</b>
		</td>
	</tr>
	</table>
	</form>
	';
	echo $isi;
	
}else if($_POST["aksi"]=="input" and $_POST["kelas"] and $_POST["sid"] == session_id()) {
	$lanjut = true; $pesan = "";
	if(harusAngka($_POST["kelas"])) {
		$id_kelas = $_POST["kelas"];
	}else{
		$pesan .= "Illegal Character\n";
		$id_kelas = '0';
		$lanjut = false;
	}
		
	// ambil id_kurikulum
	$id_kurikulum_mk=""; $id_mata_kuliah="";
	$kueri = "
	select id_kurikulum_mk,id_mata_kuliah from kurikulum_mk where id_kurikulum_mk = '".$_REQUEST['id_kur_mk']."'
	";
	$result = $db->Query($kueri)or die("salah kueri 140");
	while($r = $db->FetchRow()) {
		$id_kurikulum_mk = $r[0];
		$id_mata_kuliah = $r[1];
	}


	// bila nilai B, maka tidak boleh diambil lagi
	$mk_tidak_boleh = "";
	$kueri = "
	select d.kd_mata_kuliah
	from pengambilan_mk a, kelas_mk b, kurikulum_mk c, mata_kuliah d
	where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=c.id_kurikulum_mk and c.id_mata_kuliah=d.id_mata_kuliah 
	and a.id_mhs='".$id_mhs."' and a.nilai_huruf='B'
	";
	$result = $db->Query($kueri)or die("salah kueri 75 : ");
	while($r = $db->FetchRow()) {
		$mk_tidak_boleh .= ",'".$r[0]."'";
	}

	// bila sudah kadaluarsa, maka tidak boleh diambil lagi, KECUALI YANG NILAINYA E MASIH BOLEH
	// definisi kadaluarsa, dilihat dari pertama dia ambil
	if($sem_aktif_semes == 'Genap') {
		$kadaluarsa = ($sem_aktif_tahun-2)."Genap";
	}else if($sem_aktif_semes == 'Ganjil') {
		$kadaluarsa = ($sem_aktif_tahun-2)."Ganjil";
	}
	$kueri = "
	select d.kd_mata_kuliah, e.thn_akademik_semester, e.nm_semester
	from pengambilan_mk a, kurikulum_mk c, mata_kuliah d, semester e
	where a.id_kurikulum_mk=c.id_kurikulum_mk and c.id_mata_kuliah=d.id_mata_kuliah and a.id_semester=e.id_semester
	and a.id_mhs='".$id_mhs."' and e.id_semester!='".$sem_aktif."' and concat(e.thn_akademik_semester, e.nm_semester)<='".$kadaluarsa."'
	and a.nilai_huruf not in ('E','K')
	order by e.thn_akademik_semester asc, e.nm_semester asc
	";
	$result = $db->Query($kueri)or die("salah kueri 75 : ");
	while($r = $db->FetchRow()) {
		$mk_tidak_boleh .= ",'".$r[0]."'";
	}


	// bila sks total mhs < 110, maka tidak boleh ambil skripsi dan kkn

	if(strlen($mk_tidak_boleh)>0) {
		$mk_tidak_boleh = " and a.kd_mata_kuliah not in (".substr($mk_tidak_boleh,1).") ";
	}else if(strlen($mk_tidak_boleh)==0) {
		$mk_tidak_boleh .= "";
	}

	$mk_ditawarkan = false;
	$kueri = "
	select a.kd_mata_kuliah, a.nm_mata_kuliah, d.kredit_semester, d.id_mata_kuliah, d.id_kurikulum_mk, c.no_kelas_mk, c.id_kelas_mk,
	c.terisi_kelas_mk, (case when b.quota = 0 or b.quota is null then c.kapasitas_kelas_mk else b.quota end) as kapasitas_kelas_mk
	from mata_kuliah a, krs_prodi b, kelas_mk c, kurikulum_mk d
	where c.id_kurikulum_mk=d.id_kurikulum_mk and a.id_mata_kuliah=d.id_mata_kuliah and c.id_kelas_mk=b.id_kelas_mk
	and b.id_semester='".$sem_aktif."' and b.id_program_studi='".$id_prodi."'
	".$mk_tidak_boleh."
	order by a.kd_mata_kuliah, c.no_kelas_mk
	";
	$result = $db->Query($kueri)or die("salah kueri 25 : ".$db->Error());
	while($r = $db->FetchRow()) {
		$mk_ditawarkan = true;
	}
	if($mk_ditawarkan == false) {
		$lanjut = false;
		$pesan .= "Mta tidak ditawarkan\n";
	}

	// ambil prasyaratnya
	$lolos_prasyarat=true; $kode_mk_prasyarat="";
	$kueri = "select distinct id_prasyarat_mk from prasyarat_mk where id_kurikulum_mk='".$id_kurikulum_mk."' ";
	$result = $db->Query($kueri)or die("salah kueri 164");
	while($r = $db->FetchRow()) {
		$lolos_prasyarat=true;
		$id_prasyarat = $r[0];
		
		$kueri2 = "select group_prasyarat_mk.id_kurikulum_mk, case when (min_nilai_huruf='D') then 1 else
		           0 end as min_nilai_huruf,kd_mata_kuliah
		           from group_prasyarat_mk
               		   left join kurikulum_mk on group_prasyarat_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
               		   left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
                           where id_prasyarat_mk='".$id_prasyarat."' "; 

		$result2 = $db2->Query($kueri2)or die("salah kueri 169");
		while($r2 = $db2->FetchRow()) {
			$ambil_kuri = $r2[0];
			$ambil_nilai = $r2[1];
			$ambil_kode = $r2[2];	

			$kueri3 = "select count(*) from 
      					(select case when (b.nilai_huruf='A') then 4 else
      					case when (b.nilai_huruf='AB') then 3.5 else
      					case when (b.nilai_huruf='B') then 3 else
      					case when (b.nilai_huruf='BC') then 2.5 else
      					case when (b.nilai_huruf='C') then 2 else
      					case when (b.nilai_huruf='CD') then 1.5 else
      					case when (b.nilai_huruf='D') then 1 else 
					case when (b.nilai_huruf is null) then 0 else 0
      					end end end end end end end end as nilai_huruf
					from kurikulum_mk a, pengambilan_mk b
					where a.id_kurikulum_mk=b.id_kurikulum_mk 
					and a.id_kurikulum_mk in (select id_kurikulum_mk from kurikulum_mk where id_mata_kuliah in
          				(select id_mata_kuliah from mata_kuliah where kd_mata_kuliah='".$ambil_kode."')) 
					and b.id_mhs='".$id_mhs."' and (b.id_semester!='".$sem_aktif."' or id_semester is null)) 
					where nilai_huruf >= '".$ambil_nilai."'";

			$result3 = $db3->Query($kueri3)or die("salah kueri 315");
			$r3 = $db3->FetchRow();
			if($r3[0] > 0) {
				// ada
			}else if($r3[0] == 0) {
				// ambil kode mtk
				$kueri3 = "select b.kd_mata_kuliah
				from kurikulum_mk a, mata_kuliah b
				where a.id_mata_kuliah=b.id_mata_kuliah and a.id_kurikulum_mk='".$ambil_kuri."'";
				$result3 = $db3->Query($kueri3)or die("salah kueri 325");
				while($r3 = $db3->FetchRow()) {
					$kode_mk_prasyarat = $r3[0];
				}
				$lolos_prasyarat = false;
				//break;
			}
		}
		if($lolos_prasyarat==true) {
			break;
		}
	}
	if($lolos_prasyarat==false) {
		$lanjut = false;
		$pesan .= "Tidak lolos pada prasyarat ".$kode_mk_prasyarat."\n";
	}
	//-----------------
	
	if($lanjut){
		// cek apakah sudah diambil
		$kueri = "
		select count(*) from pengambilan_mk a, kelas_mk b 
		where a.id_kelas_mk=b.id_kelas_mk and a.id_semester='".$sem_aktif."' and b.id_kelas_mk='".$id_kelas."' and id_mhs='".$id_mhs."'";
		$result = $db->Query($kueri)or die("salah kueri 287 ");
		while($r = $db->FetchRow()) {
			$sudah_ambil = $r[0];
		}
		if($sudah_ambil > 0){
			$lanjut = false;
			$pesan .= "Mata Ajar sudah diambil\n";
		}
	}
	
	// ambil sks terambil
	$sks_terambil=0;
	$kueri = "
	select sum(d.kredit_semester)
	from pengambilan_mk a, kelas_mk b, mata_kuliah c, kurikulum_mk d
	where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=d.id_kurikulum_mk and c.id_mata_kuliah=d.id_mata_kuliah and a.id_semester=b.id_semester
	and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' and a.status_pengambilan_mk!='4'
	";
	$result = $db->Query($kueri)or die("salah kueri 176");
	$r = $db->FetchRow();
	$sks_terambil = $r[0];
	if(strlen($sks_terambil)==0 ) {
		$sks_terambil = '0';
	}

	
	$bobot["A"] = 4; $bobot["AB"] = 3.5; $bobot["B"] = 3; $bobot["BC"] = 2.5; $bobot["C"] = 2; $bobot["D"] = 1; $bobot["E"] = 0;
	
	if($ips_mhs == ''){
		$ips_mhs = 0;
	}


	// ambil sks sems depan
		$kueri2 = "select max(sks_maksimal) as sks_maksimal from beban_sks where id_program_studi = ".$id_prodi." and ipk_minimum <= ".$ips_mhs."";
		$result = $db->Query($kueri2)or die("salah kueri : 5");
		while($r = $db->FetchRow()) {
			$sks_maks = $r[0];
		}
		$sks_maks = $sks_maks+1;
		if($sks_maks>24) {
			$sks_maks=24;
		}
	
	$sks_ditambahkan=0;
	$kueri = "
	select b.kredit_semester from kelas_mk a, kurikulum_mk b, mata_kuliah c
	where a.id_kurikulum_mk=b.id_kurikulum_mk and b.id_mata_kuliah=c.id_mata_kuliah and a.id_kelas_mk='".$id_kelas."'
	";
	$result = $db->Query($kueri)or die("salah kueri 194 ");
	$r = $db->FetchRow();
	$sks_ditambahkan = $r[0];
	if(strlen($sks_ditambahkan)==0 ) {
		$sks_ditambahkan = '0';
	}
	
	if($lanjut and ($sks_terambil+$sks_ditambahkan)>$sks_maks ) {
		$lanjut = false;
		$pesan .= "Sks diambil melebihi jatah maksimal\n";
	}

	// cek jadwal tabrakan
	$kueri = "select id_jadwal_hari,id_jadwal_jam from jadwal_kelas where id_kelas_mk='".$id_kelas."' ";
	$result = $db->Query($kueri)or die("salah kueri 206");
	while($r = $db->FetchRow()) {
		$jadwal_hari = $r[0];
		$jadwal_jam = $r[1];
	}
	$kueri = "select e.kd_mata_kuliah
	from pengambilan_mk a, jadwal_kelas b, kelas_mk c, kurikulum_mk d, mata_kuliah e
	where a.id_kelas_mk=b.id_kelas_mk and a.id_kelas_mk=c.id_kelas_mk and c.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=e.id_mata_kuliah and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' and b.id_jadwal_hari='".$jadwal_hari."' and b.id_jadwal_jam='".$jadwal_jam."' ";
	$result = $db->Query($kueri)or die("salah kueri 206");
	while($r = $db->FetchRow()) {
		$lanjut = false;
		$pesan .= "Jadwal tabrakan dengan kode mata ajar ".$r[0]."\n";
	}

	
	if($lanjut) {
	
		// ambil sudah diulang berapa kali
		$kueri = "select count(*)
		from pengambilan_mk a, kurikulum_mk b, mata_kuliah c
		where a.id_kurikulum_mk=b.id_kurikulum_mk and b.id_mata_kuliah=c.id_mata_kuliah 
		and a.id_mhs='".$id_mhs."' and b.id_program_studi='".$id_prodi."' and c.id_mata_kuliah='".$id_mata_kuliah."'
		";
		$jum_ulang=0;
		$result = $db->Query($kueri)or die("salah kueri 416");
		while($r = $db->FetchRow()) {
			$jum_ulang = $r[0];
		}
		
		$kueri = "
		INSERT INTO PENGAMBILAN_MK (ID_MHS, ID_SEMESTER, STATUS_PENGAMBILAN_MK, ID_KELAS_MK, STATUS_APV_PENGAMBILAN_MK, STATUS_ULANGKE, ID_KURIKULUM_MK )  
		(
		select '".$id_mhs."', '".$sem_aktif."', '1', '".$id_kelas."', '0', '".$jum_ulang."', '".$id_kurikulum_mk."'
		from kelas_mk where id_kelas_mk='".$id_kelas."' and kapasitas_kelas_mk>
		  (
		  select count(*) from pengambilan_mk where id_kelas_mk='".$id_kelas."'
		  )
		)
		";
		$result = $db->Query($kueri)or die("Salah kueri 234");
		
		if($result) {
			echo 'Proses berhasil';
		}else{
			echo 'Kelas sudah penuh';
		}
		
	}else{
		echo $pesan;
	}
	
}else{
	echo "???";
}

?>