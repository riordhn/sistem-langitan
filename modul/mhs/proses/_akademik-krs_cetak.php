<?php
require('../../../config.php');
$db2 = new MyOracle();

$id_pt = $id_pt_user;

 $kota_pt = $PT->kota_pt; #untuk informasi kota PTNU by putra rieskha
require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

if ($user->Role() != AUCC_ROLE_MAHASISWA) {
	echo "anda tidak berhal mengakses halaman ini";
	exit();
}

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'F4', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('KRS');
$pdf->SetSubject('KRS Mahasiswa');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logo-".$nama_singkat.".gif";
//$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "20";
$title = strtoupper($PT->nama_pt);

$mhs_nama="";
$kueri = "select nm_pengguna from pengguna where id_pengguna='".$user->ID_PENGGUNA."'";
$result = $db->Query($kueri)or die("salah kueri 25 ");
while($r = $db->FetchRow()) {
	$mhs_nama = $r[0];
}

// ambil id_mhs
$id_mhs=""; $mhs_nim=""; $mhs_fakul=""; $mhs_prodi=""; $mhs_jenjang=""; $id_prodi=""; $id_jenjang="";  $id_fakultas="";
$kueri = 
"SELECT a.id_mhs, a.NIM_MHS, 'FAKULTAS '||c.nm_fakultas
, b.nm_program_studi, d.nm_jenjang, a.id_program_studi, b.id_fakultas, b.id_jenjang,a.thn_angkatan_mhs
from mahasiswa a, program_studi b, fakultas c, jenjang d
where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and b.id_jenjang=d.id_jenjang and a.id_pengguna='".$user->ID_PENGGUNA."'";
$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
while($r = $db->FetchRow()) {
	$id_mhs = $r[0];
	$mhs_nim = $r[1];
	$mhs_fakul = $r[2];
	$mhs_prodi = $r[3];
	$mhs_jenjang = $r[4];
	$id_prodi = $r[5];
	$id_fakultas = $r[6];
	$id_jenjang = $r[7];
	$angkatan_mhs = $r[8];
}

// ambil semester_aktif
$sem_aktif=""; $sem_aktif_tahun=""; $sem_aktif_semes="";
$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where STATUS_AKTIF_SEMESTER='True' and id_perguruan_tinggi = '{$id_pt}' order by id_semester desc";
$result = $db->Query($kueri)or die ("salah kueri : ".$kueri);
while($r = $db->FetchRow()) {
	$sem_aktif = $r[0];
	$sem_aktif_tahun = $r[1];
	$sem_aktif_semes = $r[2];
}

$thnsmt="select 'KRS AKADEMIK TAHUN AJARAN '||tahun_ajaran||' SEMESTER '||upper(nm_semester) as nm_smt from semester where id_semester='".$sem_aktif."'";
$result = $db->Query($thnsmt)or die("salah kueri 1 ");
while($r = $db->FetchRow()) {
	$nm_smt = $r[0];
}

// ambil sks terambil
$sks_terambil=0;
$kueri = "
select sum(d.kredit_semester)
from pengambilan_mk a, kelas_mk b, mata_kuliah c, kurikulum_mk d
where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah
and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' -- and a.STATUS_APV_PENGAMBILAN_MK ='1'
";
$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
while($r = $db->FetchRow()) {
	$sks_terambil = $r[0];
}
if(strlen($sks_terambil)==0) { $sks_terambil = '0'; }

	$kemarin_thn=""; $kemarin_sem="";
	if($sem_aktif_semes=="Ganjil") {
		$kemarin_thn = $sem_aktif_tahun-1;
		$kemarin_sem = "Genap";
	}else if($sem_aktif_semes=="Genap") {
		$kemarin_thn = $sem_aktif_tahun;
		$kemarin_sem = "Ganjil";
	}
	
	// ambil semester_kemarin
		$kemarin_idsem="";
		$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."' and id_perguruan_tinggi = '".$id_pt."'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		while($r = $db->FetchRow()) {
			$kemarin_idsem = $r[0];
		}

		// apakah semester tsb cuti ?
		$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		$r = $db->FetchRow();
		if($r[0]>0) { // maka cuti ke-1
			// ambil semester sebelum cuti
			$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$sem_aktif_tahun = $r[1];
				$sem_aktif_semes = $r[2];
			}
			if($sem_aktif_semes=="Ganjil") {
				$kemarin_thn = $sem_aktif_tahun-1;
				$kemarin_sem = "Genap";
			}else if($sem_aktif_semes=="Genap") {
				$kemarin_thn = $sem_aktif_tahun;
				$kemarin_sem = "Ganjil";
			}
			$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$kemarin_idsem = $r[0];
			}
			// apakah semester tsb cuti ?
			$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			$r = $db->FetchRow();
			if($r[0]>0) { // maka cuti ke-2
				// ambil semester sebelum cuti
				$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$sem_aktif_tahun = $r[1];
					$sem_aktif_semes = $r[2];
				}
				if($sem_aktif_semes=="Ganjil") {
					$kemarin_thn = $sem_aktif_tahun-1;
					$kemarin_sem = "Genap";
				}else if($sem_aktif_semes=="Genap") {
					$kemarin_thn = $sem_aktif_tahun;
					$kemarin_sem = "Ganjil";
				}
				$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$kemarin_idsem = $r[0];
				}
			}
		}
	
	// ambil sks_maks
	$sks_maks = 0; $sks_total = 0; $ipk_mhs = 0; $ips_mhs = 0;
	$ips_atas='0'; $ips_bawah='0';
	$bobot["A"] = 4; $bobot["AB"] = 3.5; $bobot["B"] = 3; $bobot["BC"] = 2.5; $bobot["C"] = 2; $bobot["D"] = 1; $bobot["E"] = 0;
	/*
	$kueri2 = "
		select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester, d.nilai_huruf
		from mata_kuliah a, semester b, pengambilan_mk d, kurikulum_mk e
		where d.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and d.id_semester=b.id_semester
		and b.id_semester='".$kemarin_idsem."' and d.id_mhs='".$id_mhs."' and d.nilai_huruf is not null
	";
	*/
	/*
		$kueri2 = "
		select kd_mata_kuliah,nm_mata_kuliah,kredit_semester,nilai_huruf,
		tipe_semester
		from(
		select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
		case when kurikulum_mk.status_mkta='1' 
		and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas in (7,12) then 0 else kurikulum_mk.kredit_semester end as kredit_semester,
		case when kurikulum_mk.status_mkta='1' 
		and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
		when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
		case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
		coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
		nilai_angka,a.besar_nilai_mk as UTS,b.besar_nilai_mk as UAS,
		row_number() over(partition by pengambilan_mk.id_mhs,kd_mata_kuliah order by nilai_huruf) rangking
		from PENGAMBILAN_MK
		left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
		left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
		left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
		left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
		left join semester on pengambilan_mk.id_semester=semester.id_semester
		left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
		left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
		left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
		left join nilai_mk a on pengambilan_mk.id_pengambilan_mk=a.id_pengambilan_mk and a.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UTS')
		left join nilai_mk b on pengambilan_mk.id_pengambilan_mk=b.id_pengambilan_mk and b.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UAS')
		where  group_semester||thn_akademik_semester in
		(select group_semester||thn_akademik_semester from semester where id_semester='".$kemarin_idsem."')
		and tipe_semester in ('UP','REG','RD')
		and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0
		and mahasiswa.id_mhs='".$id_mhs."') 
		where rangking=1";
	$result = $db->Query($kueri2)or die("salah kueri : 4");
	while($r = $db->FetchRow()) {
		if(strtoupper($r[1])=='KKN' or strtoupper($r[1])=='KULIAH KERJA NYATA' or strtoupper($r[1])=='SKRIPSI' or strtoupper($r[1])=='TUGAS AKHIR' or strtoupper($r[1])=='PRAKTEK KERJA LAPANGAN' or strtoupper($r[1])=='PKL' or strtoupper($r[1])=='PKL (MAGANG PRAKTEK KERJA LAPANGAN)' or strtoupper($r[1])=='PRAKTEK KERJA LAPANGAN (PKL)' or strtoupper($r[1])=='RESIDENSI DAN PRAKTEK KERJA LAPANGAN (PKL)') {
			if($r[3]=="E") {
				// tidak dihitung
			}else{
				$ips_bawah += $r[2];
				$ips_atas += ($bobot[$r[3]]*$r[2]);
			}
		}else{
			$ips_bawah += $r[2];
			$ips_atas += ($bobot[$r[3]]*$r[2]);
		}
	}
	if($ips_bawah>0) {
		$ips_mhs = number_format(($ips_atas/$ips_bawah),2);
	}else{
		$ips_mhs = '0.00';
	}
	*/

	/*$db->Query("select sum(kredit_semester) as sks_sem, sum((bobot*kredit_semester)) as bobot_total, 
	case when sum(kredit_semester)=0 then 0 else round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips
	from 
	(
	select kredit_semester,bobot
	from(
	select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
	case when kurikulum_mk.status_mkta='1' 
	and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas in (7,12) then 0 else kurikulum_mk.kredit_semester end as kredit_semester, 
	case when kurikulum_mk.status_mkta='1' 
	and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
	when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
	case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
	coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
	nilai_angka,a.besar_nilai_mk as UTS,b.besar_nilai_mk as UAS,
	row_number() over(partition by pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,pengambilan_mk.id_kelas_mk order by nilai_huruf) rangking
	from PENGAMBILAN_MK
	left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
	left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
	left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
	left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
	left join semester on pengambilan_mk.id_semester=semester.id_semester
	left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
	left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
	left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
	left join nilai_mk a on pengambilan_mk.id_pengambilan_mk=a.id_pengambilan_mk and a.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UTS')
	left join nilai_mk b on pengambilan_mk.id_pengambilan_mk=b.id_pengambilan_mk and b.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UAS')
	where  group_semester||thn_akademik_semester in
	(select group_semester||thn_akademik_semester from semester where id_semester='".$kemarin_idsem."')
	and tipe_semester in ('UP','REG','RD')
	and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0 and flagnilai='1'
	and mahasiswa.id_mhs='".$id_mhs."') 
	where rangking=1
	)");*/

	$db->Query("SELECT SUM(sks) AS sks_sem, SUM((bobot*sks)) as bobot_total, round(SUM(nilai_mutu) / sum(sks), 2) as ips from (
					    SELECT
					        mhs.id_mhs,
					        /* Kode MK utk grouping total mutu */
					        COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
					        /* SKS */
					        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
					        /* Bobot */
					        sn.nilai_standar_nilai AS bobot,
					        /* Nilai Mutu = SKS * Bobot */
					        COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu
					    FROM pengambilan_mk pmk
					    JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
					    JOIN semester S ON S.id_semester = pmk.id_semester
					    -- Via Kelas
					    LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
					    LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
					    -- Via Kurikulum
					    LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
					    LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
					    -- nilai bobot
					    JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
					    WHERE 
					        pmk.status_apv_pengambilan_mk = 1 AND 
					        mhs.id_mhs = '".$id_mhs."' AND 
					        S.id_semester = '".$kemarin_idsem."'
					)
					group by id_mhs");

	$rw = $db->FetchAssoc();
	$ips_mhs = (($rw['IPS']=='')? '0':$rw['IPS'] );
	$ips_bawah = $rw['SKS_SEM'];


	// ambil sks sems depan
		$kueri2 = "select max(sks_maksimal) as sks_maksimal from beban_sks where id_program_studi = ".$id_prodi." and ipk_minimum <= ".$ips_mhs."";
		$result = $db->Query($kueri2)or die("salah kueri : 131");
		while($r = $db->FetchRow()) {
			$sks_maks = $r[0];
		}
		/*if($id_fakultas=='2') {
			// ambil tingkat semester
			$thn_masuk="0";
			if(substr($mhs_nim,2,2) < 50) {
				$thn_masuk = "20".substr($mhs_nim,2,2);
			}else if(substr($mhs_nim,2,2) > 50) {
				$thn_masuk = "19".substr($mhs_nim,2,2);
			}
			$tingkat_semester='0';
			if(strtoupper($sem_aktif_semes)=="GANJIL") {
				$tingkat_semester = (($sem_aktif_tahun-$thn_masuk)*2)+1;
			}else if(strtoupper($sem_aktif_semes)=="GENAP") {
				$tingkat_semester = (($sem_aktif_tahun-$thn_masuk)*2)+2;
			}

			if($tingkat_semester<=4) {
				$sks_maks = '22';
			}
		}else if($id_fakultas=='3') {
			$sks_maks = $sks_maks+1;
		}else if($id_fakultas=='4') {
			if($id_jenjang=='1') {
				if(strlen($sks_maks)==0) { 
					$sks_maks = '0'; 
				}else{
					$sks_maks=$sks_maks+1; 
				}
			}else if($id_jenjang=='5') {
				if(strlen($sks_maks)==0) { 
					$sks_maks = '0'; 
				}else{
					$sks_maks=$sks_maks+2;
				}
			}
		}else if($id_fakultas=='5') {
			$sks_maks = $sks_maks+2;
		}*/
		if($sks_maks>24) {
			$sks_maks=24;
		}


/*
	//perhitungan sks total dan ipk versi lukman
		$sql="select a.id_mhs,sum(a.kredit_semester) skstotal,
		round(sum(a.kredit_semester*(case a.nilai_huruf 
		when 'A' then 4 
		when 'AB' then 3.5 
		when 'B' then 3
		when 'BC' then 2.5
		when 'C' then 2
		when 'D' then 1
		end))/sum(a.kredit_semester),2) IPK
		from
		(
			select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
				select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
				from pengambilan_mk a 
				left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
				left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
				where a.nilai_huruf<'E' and a.nilai_huruf is not null
			) a
			left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
			left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
			where rangking=1 and id_mhs='{$id_mhs}'
		) a
		left join mahasiswa b on a.id_mhs=b.id_mhs
		left join program_studi f on b.id_program_studi=f.id_program_studi
		where a.id_mhs='{$id_mhs}'
		group by a.id_mhs order by a.id_mhs";
*/
		//perhitungan sks total dan ipk versi lukman
		/*$sql="select a.id_mhs,sum(a.kredit_semester) skstotal,
		round(sum(a.kredit_semester*(case a.nilai_huruf 
		when 'A' then 4 
		when 'AB' then 3.5 
		when 'B' then 3
		when 'BC' then 2.5
		when 'C' then 2
		when 'D' then 1
		end))/sum(a.kredit_semester),2) IPK
		from
		(
		select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
		select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
		from pengambilan_mk a 
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null and a.flagnilai=1
		) a
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where rangking=1 and id_mhs='{$id_mhs}'
		) a
		left join mahasiswa b on a.id_mhs=b.id_mhs
		left join program_studi f on b.id_program_studi=f.id_program_studi
		where a.id_mhs='{$id_mhs}'
		group by a.id_mhs order by a.id_mhs";*/

		$fd_id_smt="";
		$kueri = "select fd_id_smt from semester where id_semester = '".$kemarin_idsem."'";
		$result = $db->Query($kueri)or die ("salah kueri : ".$kueri);
		while($r = $db->FetchRow()) {
			$fd_id_smt = $r[0];
		}

		$sql = "SELECT id_mhs,
                        SUM(sks) AS total_sks, 
                        round(SUM(nilai_mutu) / SUM(sks), 2) AS ipk 
                    FROM (
                        SELECT
                            mhs.id_mhs,
                            /* Kode MK utk grouping total mutu */
                            COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
                            /* SKS */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
                            /* Nilai Mutu = SKS * Bobot */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu,
                            /* Urutan Nilai Terbaik */
                            row_number() OVER (PARTITION BY mhs.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY sn.nilai_standar_nilai DESC) urut
                        FROM pengambilan_mk pmk
                        JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
                        JOIN semester S ON S.id_semester = pmk.id_semester
                        -- Via Kelas
                        LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
                        LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
                        -- Via Kurikulum
                        LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
                        LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
                        -- Penyetaraan
                        LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
                        LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
                        -- nilai bobot
                        JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
                        WHERE 
                            pmk.status_apv_pengambilan_mk = 1 AND 
                            mhs.id_mhs = '".$id_mhs."' AND 
                            S.fd_id_smt <= '".$fd_id_smt."'
                    )
                    GROUP BY id_mhs";

		$result2 = $db->Query($sql);
		while($r2 = $db->FetchRow()) {
			$sks_total = $r2[1];
			$ipk_mhs = $r2[2];
		}

		$biomhs="select mhs.nim_mhs, upper(p.nm_pengguna) as nm_pengguna, j.nm_jenjang, ps.nm_program_studi, mhs.status_akademik_mhs, 
					 case when f.id_fakultas=9 then 'PROGRAM '||f.nm_fakultas else 'FAKULTAS '||f.nm_fakultas end, 
					f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas, f.id_fakultas,
					mhs.id_program_studi,mhs.id_mhs
					from mahasiswa mhs 
					left join pengguna p on mhs.id_pengguna=p.id_pengguna and p.id_perguruan_tinggi = '{$id_pt}' 
					left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
					left join fakultas f on ps.id_fakultas=f.id_fakultas
					left join jenjang j on ps.id_jenjang=j.id_jenjang
					where mhs.nim_mhs='".$mhs_nim."'";
		$result1 = $db->Query($biomhs)or die("salah kueri 2 ");
		while($r1 = $db->FetchRow()) {
			//$nim_mhs = $r1[0];
			//$nm_mhs = $r1[1];
			$jenjang = $r1[2];
			$prodi = $r1[3];
			//$status = $r1[4];
			$fak = $r1[5];
			$alm_fak = $r1[6];
			$pos_fak = $r1[7];
			$tel_fak = $r1[8];
			$fax_fak = $r1[9];
			$web_fak = $r1[10];
			$eml_fak = $r1[11];
			//$kd_fak = $r1[12];
			//$kd_prodi = $r1[13];
			//$id_mhs=$r1[14];
		}


$content = strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage();

/* NOTE:
 * *********************************************************
 * You can load external XHTML using :
 *
 * $html = file_get_contents('/path/to/your/file.html');
 *
 * External CSS files will be automatically loaded.
 * Sometimes you need to fix the path of the external CSS.
 * *********************************************************
 */

// define some HTML content with style
//$html = file_get_contents('gui/transkrip-cetak.tpl');

$html = '
<p>&nbsp;</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:8x;">
  <tr>
    <td colspan="7" align="center"><p><strong>'.$nm_smt.'</strong></p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
           <td width="12%">Nama</td>
           <td width="1%"><strong>: </strong></td>
           <td width="33%"><strong>'.$mhs_nama.'</strong></td>
           <td width="18%">&nbsp;</td>
           <td width="15%">IPK</td>
           <td width="1%"><strong>:</strong></td>
           <td width="20%"><strong>'.$ipk_mhs.'</strong></td>
  </tr>
         <tr>
           <td>NIM</td>
           <td><strong>: </strong></td>
           <td><strong>'.$mhs_nim.'</strong></td>
           <td>&nbsp;</td>
           <td>IPS</td>
           <td><strong>: </strong></td>
           <td><strong>'.$ips_mhs.'</strong></td>
         </tr>
         <tr>
           <td>Program Studi</td>
           <td><strong>: </strong></td>
           <td><strong>'.$mhs_jenjang.' '.$mhs_prodi.'</strong></td>
           <td>&nbsp;</td>
           <td>Sks Maks</td>
           <td><strong>: </strong></td>';
	if ($angkatan_mhs==$sem_aktif_tahun and $sem_aktif_semes=='Ganjil')
	{$html .= '<td><strong>PAKET MABA</strong></td>';} else
	{$html .= '<td><strong>'.$sks_maks.'</strong></td>';}
         $html .= '</tr>
</table>
<p>&nbsp;</p>
<table border="0" width="100%">
<tr>
<td width="100%">
<table cellspacing="0" cellpadding="3" border="1" width="100%" style="font-size:25px;">
  <thead>
    <tr bgcolor="#000000" style="color:#FFF;">
      <td width="20%" align="center" bgcolor="#333333"><font color="#FFFFFF">Kode MA</font></td>
      <td width="42%" align="center" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Ajar</font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
      <td width="19%" align="center" bgcolor="#333333"><font color="#FFFFFF">Kelas</font></td>
    </tr>
  </thead>
  <tbody>
';
/*
$kueri = "
select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester,d.id_pengambilan_mk,c.no_kelas_mk,d.STATUS_APV_PENGAMBILAN_MK
from mata_kuliah a, kelas_mk c, pengambilan_mk d, kurikulum_mk e
where c.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and c.id_semester=d.id_semester and c.id_kelas_mk=d.id_kelas_mk
and c.id_semester='".$sem_aktif."' and d.id_mhs='".$id_mhs."' and d.STATUS_APV_PENGAMBILAN_MK='1'
order by a.kd_mata_kuliah
";
*/
$kueri = "
select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester,d.id_pengambilan_mk,c.no_kelas_mk,d.STATUS_APV_PENGAMBILAN_MK
from mata_kuliah a, kelas_mk c, pengambilan_mk d, kurikulum_mk e
where c.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and c.id_kelas_mk=d.id_kelas_mk
and d.id_semester='".$sem_aktif."' and d.id_mhs='".$id_mhs."' -- and d.STATUS_APV_PENGAMBILAN_MK='1'
order by a.kd_mata_kuliah
";
$hit=0; $tot_sks=0;
$result = $db->Query($kueri)or die("salah kueri 31 ");
while($r = $db->FetchRow()) {
	$hit++;
	$tot_sks += $r[2];
	$nm_kelas = "";
	$result2 = $db2->Query("select nama_kelas from nama_kelas where id_nama_kelas='".$r[4]."'")or die("salah kueri 42 ");
	while($r2 = $db2->FetchRow()) {
		$nm_kelas = $r2[0];
	}
	$html .= '
		<tr>
			<td width="20%">'.$r[0].'</td>
			<td width="42%">'.$r[1].'</td>
			<td width="19%" align="center" >'.$r[2].'</td>
			<td width="19%" align="center" >'.$nm_kelas.'</td>
		</tr>
	';
}
$html .= '
	<tr>
		<td colspan="2" align="center">Total SKS</td>
		<td width="19%" align="center">'.$tot_sks.'</td>
		<td width="19%" align="center"></td>
	</tr>
  </tbody>
</table>
</td>
</tr>
</table>
<p>&nbsp;</p>
';
// ambil dosen wali
$sem_aktif="";
/*
$kueri = "select b.nip_dosen,c.nm_pengguna from dosen_wali a, dosen b, pengguna c
where a.id_dosen=b.id_dosen and b.id_pengguna=c.id_pengguna and a.id_mhs='".$id_mhs."'
";
*/
/*$kueri = "
select b.nip_dosen,c.gelar_depan || ' ' || c.nm_pengguna || ', ' || c.gelar_belakang as nm_pengguna
from dosen_wali a, dosen b, pengguna c, semester d
where a.id_dosen=b.id_dosen and b.id_pengguna=c.id_pengguna and a.id_semester=d.id_semester and a.id_mhs='".$id_mhs."' and a.status_dosen_wali=1
order by d.thn_akademik_semester desc, d.nm_semester desc
";
$result = $db->Query($kueri)or die ("salah kueri 48 ");
//while($r = $db->FetchRow()) {
//	$wali_nip = $r[0];
//	$wali_nama = $r[1];
//}
$r = $db->FetchRow();
$wali_nip = $r[0];
$wali_nama = $r[1];*/
#$kota_pt = 'Lampung Timur';


// mencari BAAK request UNULAMPUNG
/*$kueri = "
	select b.nip_pegawai,a.gelar_depan || ' ' || a.nm_pengguna || ', ' || a.gelar_belakang as nm_pengguna
	from pengguna a, pegawai b, fakultas c
	where a.id_pengguna=b.id_pengguna and a.id_pengguna=c.id_kabag_akademik and c.id_fakultas='".$id_fakultas."'
	";
$result = $db->Query($kueri)or die ("salah kueri 48 ");

$r = $db->FetchRow();
$baak_nip = $r[0];
$baak_nama = $r[1];*/

/*$kueri = "
	select nvl(b.nidn_dosen,b.nip_dosen) nip,a.gelar_depan || ' ' || a.nm_pengguna || ', ' || a.gelar_belakang as nm_pengguna
	from pengguna a, dosen b, fakultas c
	where a.id_pengguna=b.id_pengguna and a.id_pengguna=c.id_kabag_akademik and c.id_fakultas='".$id_fakultas."'
	";
$result = $db->Query($kueri)or die ("salah kueri 48 ");

$r = $db->FetchRow();
$baak_nip = $r[0];
$baak_nama = $r[1];*/



$ttd_khs = 1;

// Request UNU Lampung BAAK
if ($id_pt == 5) {
	$ttd_khs = 4;
}


if($ttd_khs == 1){
	$dosen_wali = "select b.nip_dosen,c.gelar_depan || ' ' || c.nm_pengguna || ', ' || c.gelar_belakang as nm_pengguna
					from dosen_wali a, dosen b, pengguna c, semester d
					where a.id_dosen=b.id_dosen and b.id_pengguna=c.id_pengguna 
					and a.id_semester=d.id_semester and a.id_mhs='".$id_mhs."' and a.status_dosen_wali=1
					order by d.thn_akademik_semester desc, d.nm_semester desc
					";
	$result = $db->Query($dosen_wali)or die ("salah kueri 48 ");

	$r = $db->FetchRow();
	$jabatan_ttd = "Dosen Wali";
	$nip_ttd = 'NIP ' . $r[0];
	$nama_ttd = $r[1];
}
elseif($ttd_khs == 2){
	$kaprodi="select d.nip_dosen, case when p.gelar_belakang is not null then 
				trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
				trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen
				from program_studi ps, pengguna p, dosen d
				where d.id_dosen=ps.id_kaprodi and ps.id_program_studi=".$kd_prodi." and p.id_pengguna=d.id_pengguna";
	$resultkaprodi = $db->Query($kaprodi)or die("salah kueri kaprodi ");

	$r = $db->FetchRow();
	$jabatan_ttd = "Kaprodi";
	$nip_ttd = 'NIP ' . $r[0];
	$nama_ttd = $r[1];
}
elseif($ttd_khs == 3){
	/*$dekan="select d.nip_dosen ,case when p.gelar_belakang is not null then 
				trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
				trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen, 
				case when f.id_fakultas=9 then 'Direktur' else 'Dekan' end as dekan,
				case when f.id_fakultas=9 then 'Wakil Direktur 1' else 'Wakil Dekan 1' end as wadek
				from fakultas f, pengguna p, dosen d
				where p.id_pengguna=f.id_wadek1 and f.id_fakultas=".$kd_fak." and p.id_pengguna=d.id_pengguna";
	$resultdekan = $db->Query($dekan)or die("salah kueri dekan ");*/

	$dekan="select d.nip_dosen ,case when p.gelar_belakang is not null then 
				trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
				trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen
				from fakultas f, pengguna p, dosen d
				where p.id_pengguna=f.id_wadek1 and f.id_fakultas=".$kd_fak." and p.id_pengguna=d.id_pengguna";
	$resultdekan = $db->Query($dekan)or die("salah kueri dekan ");
	
	$r = $db->FetchRow();
	$jabatan_ttd = "Wakil Dekan 1";
	$nip_ttd = 'NIP ' . $r[0];
	$nama_ttd = $r[1];
}
elseif($ttd_khs == 4){
	$baak = "select nvl(b.nidn_dosen,b.nip_dosen) nip,
				a.gelar_depan || ' ' || a.nm_pengguna || ', ' || a.gelar_belakang as nm_pengguna
				from pengguna a, dosen b, fakultas c
				where a.id_pengguna=b.id_pengguna and a.id_pengguna=c.id_kabag_akademik and c.id_fakultas='".$id_fakultas."'
				";
	$result = $db->Query($baak)or die ("salah kueri 48 ");

	$r = $db->FetchRow();
	$jabatan_ttd = "Kabag. Akademik";
	if($id_pt == 5){
		$jabatan_ttd = 'Kepala Biro Administrasi Akademik dan <br/>Kemahasiswaan';
        $nip_ttd = 'NIK 02102617';
        $nama_ttd = 'RUKIN SUDARWANTO';
	} else {
        $jabatan_ttd = 'Kepala Biro Administrasi Akademik dan <br/>Kemahasiswaan';
        $nip_ttd = 'NIP ' . $r[0];
        $nama_ttd = $r[1];
    }
}
elseif($ttd_khs == 5){
	// Pejabat yang tanda tangan KHS, masih manual, perlu di databasekan.
	$jabatan_ttd		= 'Kepala Biro Administrasi Akademik,<br/>Kemahasiswaan dan Sistem Informasi';
	$nip_ttd			= '';
	$nama_ttd			= 'Dr. Nur Syamsiah, S.Pd, M.Pd';
}



$bulan['01'] = "Januari"; $bulan['02'] = "Februari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "Juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "November";  $bulan['12'] = "Desember";

$html .= '
		<table border="0" width="100%" style="font-size:30px;">
		  <tr valign="top">
		    <td width="50%">&nbsp;</td>
		    <td width="50%" rowspan="3"><p align="center">'.$kota_pt.', '.date("d").' '.$bulan[date("m")].' '.date("Y").'</p>
		      <p align="center">'.$jabatan_ttd.',</p>
		      <p align="center">&nbsp;</p>
		      <p align="center">&nbsp;</p>
		      <p align="center">'.$nama_ttd.'<br/>'.$nip_ttd.'</p>
		      <div align="center"></div>      <div align="center"></div>
		    </td>
		  </tr>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
		</table>

		';

// REQUEST UNULAMPUNG
// FIKRIE (30-09-2016)
/*if($id_pt == 5){

	$html .= '
		<table border="0" width="100%" style="font-size:30px;">
		  <tr valign="top">
		    <td width="50%">&nbsp;</td>
		    <td width="50%" rowspan="3"><p align="center">'.$kota_pt.', '.date("d").' '.$bulan[date("m")].' '.date("Y").'</p>
		      <p align="center">Biro Administrasi Akademik dan Sistem Informasi</p>
		      <p align="center">&nbsp;</p>
		      <p align="center">&nbsp;</p>
		      <p align="center">'.$baak_nama.'</p>
		      <p align="center">NIP. '.$baak_nip.'</p>
		      <div align="center"></div>      <div align="center"></div>
		    </td>
		  </tr>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
		</table>

		';
}
else{

	// jika sudah punya dosen wali
	// FIKRIE (28-08-2016)
	if(!empty($wali_nama)){
		$html .= '
		<table border="0" width="100%" style="font-size:30px;">
		  <tr valign="top">
		    <td width="50%">&nbsp;</td>
		    <td width="50%" rowspan="3"><p align="center">'.$kota_pt.', '.date("d").' '.$bulan[date("m")].' '.date("Y").'</p>
		      <p align="center">Dosen Wali,</p>
		      <p align="center">&nbsp;</p>
		      <p align="center">&nbsp;</p>
		      <p align="center">'.$wali_nama.'</p>
		      <p align="center">NIP. '.$wali_nip.'</p>
		      <div align="center"></div>      <div align="center"></div>
		    </td>
		  </tr>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
		</table>

		';
	}
	else{
		$html .= '
		<table border="0" width="100%" style="font-size:30px;">
		  <tr valign="top">
		    <td width="50%">&nbsp;</td>
		    <td width="50%" rowspan="3"><p align="center">'.$kota_pt.', '.date("d").' '.$bulan[date("m")].' '.date("Y").'</p>
		      <p align="center">Dosen Wali,</p>
		      <p align="center">&nbsp;</p>
		      <p align="center">&nbsp;</p>
		      <p align="center">________________________</p>
		      <div align="center"></div>      <div align="center"></div>
		    </td>
		  </tr>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
		</table>

		';
	}
}*/

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// *******************************************************************
// HTML TIPS & TRICKS
// *******************************************************************

// REMOVE CELL PADDING
//
// $pdf->SetCellPadding(0);
// 
// This is used to remove any additional vertical space inside a 
// single cell of text.

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// REMOVE TAG TOP AND BOTTOM MARGINS
//
// $tagvs = array('p' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)));
// $pdf->setHtmlVSpace($tagvs);
// 
// Since the CSS margin command is not yet implemented on TCPDF, you
// need to set the spacing of block tags using the following method.

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// SET LINE HEIGHT
//
// $pdf->setCellHeightRatio(1.25);
// 
// You can use the following method to fine tune the line height
// (the number is a percentage relative to font height).

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// CHANGE THE PIXEL CONVERSION RATIO
//
// $pdf->setImageScale(0.47);
// 
// This is used to adjust the conversion ratio between pixels and 
// document units. Increase the value to get smaller objects.
// Since you are using pixel unit, this method is important to set the
// right zoom factor.
// 
// Suppose that you want to print a web page larger 1024 pixels to 
// fill all the available page width.
// An A4 page is larger 210mm equivalent to 8.268 inches, if you 
// subtract 13mm (0.512") of margins for each side, the remaining 
// space is 184mm (7.244 inches).
// The default resolution for a PDF document is 300 DPI (dots per 
// inch), so you have 7.244 * 300 = 2173.2 dots (this is the maximum 
// number of points you can print at 300 DPI for the given width).
// The conversion ratio is approximatively 1024 / 2173.2 = 0.47 px/dots
// If the web page is larger 1280 pixels, on the same A4 page the 
// conversion ratio to use is 1280 / 2173.2 = 0.59 pixels/dots

// *******************************************************************

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('KRS-'.strtoupper($mhs_nim).'.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+
