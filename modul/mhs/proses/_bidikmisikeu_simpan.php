<?php
require('../../../config.php');
require('../includes/ceking2.php');
require('../includes/ceking3.php');

if($_POST["aksi"]=="tampil") {
	$db2 = new MyOracle();
	$bulan = array('01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'November', '12'=>'Desember' );
	$isi = '
	<div>
		Data keuangan bulan : <b>'.$bulan[date("m")].' '.date("Y").'</b>
		<table cellpadding=5 cellspacing=0 border=1>
		<tr>
			<td align=center><b>Urut</b></td>
			<td align=center><b>Tanggal</b></td>
			<td align=center><b>Pos</b></td>
			<td align=center><b>Nominal</b></td>
			<td align=center><b>Ket</b></td>
			<td align=center>&nbsp;</td>
		</tr>
		';
		$kueri = "select a.id_bidikmisi_keu, b.jenis_transaksi, b.pos_transaksi, a.nominal, a.keterangan,
		b.id_bidikmisi_keu_pos, to_char(a.tgl_posting,'DD-MM-YYYY')
		from aucc.bidikmisi_keu a, aucc.bidikmisi_keu_pos b, aucc.mahasiswa c 
		where a.id_bidikmisi_keu_pos=b.id_bidikmisi_keu_pos and a.id_mhs=c.id_mhs and c.id_pengguna='".$user->ID_PENGGUNA."' and to_char(a.tgl_posting, 'MMYYYY')='".date("mY")."'
		order by a.tgl_posting asc";
		$result = $db->Query($kueri)or die("salah kueri 69 : ");
		$urut=0;
		while($r = $db->FetchRow()) {
			$urut++;
			
			$isi .= '
			<tr id="tampil_'.$r[0].'">
				<td>'.$urut.'</td>
				<td align=center>'.$r[6].'</td>
				<td>'.$r[1].' - '.$r[2].'</td>
				<td align=right>Rp. '.number_format($r[3],0,',','.').',-</td>
				<td align=center>'.$r[4].'</td>
				<td align=center>[<a onclick="edit_data(\''.$r[0].'\')" style="color:blue;">Edit</a>] [<a onclick="if(confirm(\'Yakin Hapus ?\')!=0){ hapus_data('.$r[0].'); }" style="color:blue;" >Hapus</a>]</td>
			</tr>
			<tr id="edit_'.$r[0].'" style="display:none;">
				<td>'.$urut.'</td>
				<td><input type="text" name="tgl_'.$r[0].'" value="'.$r[6].'" id="tgl_'.$r[0].'"></td>
				<td>
				<select name="pos" id="pos_'.$r[0].'">
				';
				$kueri2 = "select id_bidikmisi_keu_pos, jenis_transaksi, pos_transaksi from bidikmisi_keu_pos order by pos_transaksi";
				$result2 = $db2->Query($kueri2)or die("salah kueri 76 : ");
				while($r2 = $db2->FetchRow()) {
					$isi .= '<option value="'.$r2[0].'" '; if($r[5]==$r2[0]){ $isi .= 'selected'; } $isi .= '>'.$r2[1].' - '.$r2[2].'</option>';
				}
				$isi .= '
				</select>
				</td>
				<td><input type="text" name="nominal_'.$r[0].'" value="'.$r[3].'" id="nominal_'.$r[0].'"></td>
				<td><input type="text" name="ket_'.$r[0].'" value="'.$r[4].'" id="ket_'.$r[0].'"></td>
				<td align=center>
					<input type=button name="tambah_baru" value="Simpan" onclick="edit_simpan(\''.$r[0].'\')">
					<input type=button name="balik" value="Batal" onclick="edit_batal(\''.$r[0].'\')">
				</td>
			</tr>
			';
		}
		$isi .= '
		<tr id="baru">
			<td>&nbsp;</td>
			<td><input type="text" name="baru_tgl" value="'.date("d-m-Y").'" id="baru_tgl"></td>
			<td>
				<select name="pos" id="baru_pos">
					';
					$kueri2 = "select id_bidikmisi_keu_pos, jenis_transaksi, pos_transaksi from bidikmisi_keu_pos order by pos_transaksi desc";
					$result2 = $db2->Query($kueri2)or die("salah kueri 76 : ");
					while($r2 = $db2->FetchRow()) {
						$isi .= '<option value="'.$r2[0].'">'.$r2[1].' - '.$r2[2].'</option>';
					}
					$isi .= '
				</select>
			</td>
			<td><input type=text name="baru_nominal" id="baru_nominal"></td>
			<td><input type=text name="baru_ket" id="baru_ket"></td>
			<td><input type=button name="tambah_baru" value="Tambah" onclick="tambah_data()"></td>
		</tr>
		</table>
		</div>
	';

	echo $isi;
}
else if($_POST["aksi"]=="simpanbaru") {
	$id_pos = Angka($_POST["pos"]);
	$tanggal = semua_boleh_kecuali2($_POST["tgl"]);
	$nominal = Angka($_POST["nominal"]);
	$ket = semua_boleh_kecuali2($_POST["ket"]);

	// ambil idmhs
	$id_mhs = "";
	$kueri = "select a.id_mhs from aucc.mahasiswa a, aucc.pengguna b where a.id_pengguna=b.id_pengguna and b.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 425 : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

	$kueri = "insert into aucc.bidikmisi_keu (id_bidikmisi_keu_pos, nominal, keterangan, tgl_posting, id_mhs) values (".$id_pos.", ".$nominal.", '".$ket."', to_date('".$tanggal."', 'DD-MM-YYYY'), ".$id_mhs.") ";
	$result =  $db->Query($kueri)or die("...");
	if($result) {
		echo 'Input berhasil';
	}else{
		echo 'Update gagal';
	}
}
else if($_POST["aksi"]=="simpanedit") {
	$id_pos = Angka($_POST["pos"]);
	$tanggal = semua_boleh_kecuali2($_POST["tgl"]);
	$nominal = Angka($_POST["nominal"]);
	$ket = semua_boleh_kecuali2($_POST["ket"]);
	$diedit = Angka($_POST["diedit"]);

	// ambil idmhs
	$id_mhs = "";
	$kueri = "select a.id_mhs from aucc.mahasiswa a, aucc.pengguna b where a.id_pengguna=b.id_pengguna and b.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 425 : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

	$kueri = "update aucc.bidikmisi_keu set id_bidikmisi_keu_pos=".$id_pos.", nominal=".$nominal.", keterangan='".$ket."', tgl_posting=to_date('".$tanggal."', 'DD-MM-YYYY') where id_bidikmisi_keu='".$diedit."' and id_mhs='".$id_mhs."'";
	$result =  $db->Query($kueri)or die($kueri);
	if($result) {
		echo 'Update berhasil';
	}else{
		echo 'Update gagal';
	}
}
else if($_POST["aksi"]=="hapus") {
	$dihapus = Angka($_POST["dihapus"]);

	// ambil idmhs
	$id_mhs = "";
	$kueri = "select a.id_mhs from aucc.mahasiswa a, aucc.pengguna b where a.id_pengguna=b.id_pengguna and b.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 425 : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

	$kueri = "delete aucc.bidikmisi_keu where id_bidikmisi_keu='".$dihapus."' and id_mhs='".$id_mhs."'";
	$result =  $db->Query($kueri)or die($kueri);
	if($result) {
		echo 'Hapus berhasil';
	}else{
		echo 'Hapus gagal';
	}
}


?>