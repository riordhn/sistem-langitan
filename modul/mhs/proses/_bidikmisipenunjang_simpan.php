<?php
require('../../../config.php');
require('../includes/ceking2.php');
require('../includes/ceking3.php');

if($_POST["aksi"]=="tampil") {
	$isi = '';
	$db2 = new MyOracle();
	$bulan = array('01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'November', '12'=>'Desember' );

	// ambil idmhs
	$id_mhs = "";
	$kueri = "select a.id_mhs from aucc.mahasiswa a, aucc.pengguna b where a.id_pengguna=b.id_pengguna and b.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 425 : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

	$isi .= '
	Data penunjang bulan : <b>'.$bulan[date("m")].' '.date("Y").'</b>
	<form>
	<table style="border:solid 0px;">
	';
	$kueri = "select id_bidikmisi_penunjang_tanya, pertanyaan from aucc.bidikmisi_penunjang_tanya order by id_bidikmisi_penunjang_tanya";
	$result = $db->Query($kueri)or die("salah kueri 425 : ");
	$urut=0;
	while($r = $db->FetchRow()) {
		$urut++;
		$isi .= '
		<tr>
			<td style="border:solid 0px;">'.$urut.'. </td>
			<td style="border:solid 0px;">
				'.$r[1].'
				<ul style="list-style-type:none; padding-left:3px;">
					';
					// ambil jawaban mhs, yang paling baru
					//$kueri2 = "select id_bidikmisi_penunjang_jawab, keterangan from aucc.bidikmisi_pj_mhs where id_mhs='".$id_mhs."' and id_bidikmisi_penunjang_tanya='".$r[0]."' and to_char(tgl_posting,'MMYYYY')='".date("mY")."' ";
					$kueri2 = "select id_bidikmisi_penunjang_jawab, keterangan from aucc.bidikmisi_pj_mhs where id_mhs='".$id_mhs."' and id_bidikmisi_penunjang_tanya='".$r[0]."' and rownum=1 order by tgl_posting desc";
					$result2 = $db2->Query($kueri2)or die("salah kueri 428 : ");
					$r2 = $db2->FetchRow();
					$jawabnya = $r2[0]; $keterangannya = $r2[1];

					$kueri2 = "select id_bidikmisi_penunjang_jawab, jawaban, ada_keterangan from aucc.bidikmisi_penunjang_jawab where id_bidikmisi_penunjang_tanya='".$r[0]."' order by id_bidikmisi_penunjang_jawab ";
					$result2 = $db2->Query($kueri2)or die("salah kueri 426 : ");
					while($r2 = $db2->FetchRow()) {
						if($jawabnya == $r2[0]) {
							$dipilih = ' checked';
							$nilai_ket = $keterangannya;
						}else{
							$dipilih = ' ';
							$nilai_ket = '';
						}
						$isi .= '<li style="padding:3px;">
							<input type="radio" name="jawaban_'.$r[0].'" value="'.$r2[0].'" '.$dipilih.'>&nbsp;'.$r2[1].'
						';
						if($r2[2]=='1') {
							$isi .= '<input type=text name="ket_'.$r[0].'_'.$r2[0].'" value="'.$nilai_ket.'"> *beri keterangan';
						}
						$isi .= '
						</li>';
					}
					$isi .= '
				</ul>
			</td>
		</tr>
		';
	}
	$isi .= '
		<tr>
			<td style="border:solid 0px;" colspan=2><input type="button" name="ok" value="Simpan" onclick="simpan_data()"></td>
		</tr>
	</table>
	</form>
	';

	echo $isi;
}

else if($_POST["aksi"]=="simpandata") {
	$db2 = new MyOracle();
	// ambil idmhs
	$id_mhs = "";
	$kueri = "select a.id_mhs from aucc.mahasiswa a, aucc.pengguna b where a.id_pengguna=b.id_pengguna and b.id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die("salah kueri 425 : ");
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
	}

	//print_r($_POST);
	$kueri = "select id_bidikmisi_penunjang_tanya from aucc.bidikmisi_penunjang_tanya order by id_bidikmisi_penunjang_tanya";
	$result = $db->Query($kueri)or die("salah kueri 425 : ");
	while($r = $db->FetchRow()) {
		// cek apakah jawaban untuk bulan ini sudah ada
		$kueri2 = "select count(*) from aucc.bidikmisi_pj_mhs where id_mhs='".$id_mhs."' and id_bidikmisi_penunjang_tanya='".$r[0]."' and to_char(tgl_posting,'MMYYYY')='".date("mY")."' ";
		$result2 = $db2->Query($kueri2)or die("salah kueri 426 : ");
		$r2 = $db2->FetchRow();
		$jawab_ada = $r2[0];
		
		if(!empty($_POST["jawaban_".$r[0]]) ) {
			$menjawab = Angka($_POST["jawaban_".$r[0]]);
			if(!empty($_POST["ket_".$r[0]."_".$menjawab]) ) {
				$keterangan = semua_boleh_kecuali2($_POST["ket_".$r[0]."_".$menjawab]);
			}else{
				$keterangan = '';
			}
		}else{
			$menjawab = 'null';
			$keterangan = '';
		}
		
		if($jawab_ada == 0) { // insert
			$result2 = $db2->Query("insert into aucc.bidikmisi_pj_mhs (id_mhs, id_bidikmisi_penunjang_tanya, id_bidikmisi_penunjang_jawab, keterangan, tgl_posting) values (".$id_mhs.", ".$r[0].", ".$menjawab.", '".$keterangan."', to_date('".date("d-m-Y")."', 'DD-MM-YYYY') ) ")or die("salah kueri 429 : ");
			$r2 = $db2->FetchRow();
		}else{ // update
			$result2 = $db2->Query("update aucc.bidikmisi_pj_mhs set id_bidikmisi_penunjang_jawab=".$menjawab.", keterangan='".$keterangan."', tgl_posting = to_date('".date("d-m-Y")."', 'DD-MM-YYYY') where id_mhs='".$id_mhs."' and id_bidikmisi_penunjang_tanya='".$r[0]."' and to_char(tgl_posting,'MMYYYY')='".date("mY")."'")or die("salah kueri 430 : ");
			$r2 = $db2->FetchRow();
		}
	}
	echo "Data berhasil disimpan";
}


?>