<?php
require('../../../config.php');
require('../includes/ceking3.php');

// ambil Fakultas
$kueri = "select c.nm_fakultas from mahasiswa a, program_studi b, fakultas c where a.id_program_studi=b.id_program_studi and b.id_fakultas=c.id_fakultas and a.id_pengguna='".$user->ID_PENGGUNA."'";
$result = $db->Query($kueri)or die("salah kueri : maintenance");
while($r = $db->FetchRow()) {
	$fakultas = $r[0];
}
// ambil nama mhs
$kueri = "select nm_pengguna from pengguna where id_pengguna='".$user->ID_PENGGUNA."'";
$result = $db->Query($kueri)or die("salah kueri : maintenance");
while($r = $db->FetchRow()) {
	$nm_pengguna = $r[0];
}
// ambil nim mhs
$kueri = "select a.nim_mhs, c.nm_jenjang || ' - ' || b.nm_program_studi, a.id_mhs from mahasiswa a, program_studi b, jenjang c where a.id_program_studi=b.id_program_studi and b.id_jenjang=c.id_jenjang and a.id_pengguna='".$user->ID_PENGGUNA."'";
$result = $db->Query($kueri)or die("salah kueri : maintenance");
while($r = $db->FetchRow()) {
	$nim = $r[0];
	$prodi = $r[1];
	$id_mhs = $r[2];
}


// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=skkk-".$nim.".xls");

?>

<table border="1" width="100%">
	<?php
	echo '
		<tr>
			<td colspan="8" align="center"><h2>Satuan Kredit Kegiatan Kemahasiswaan</h2></td>
		</tr>
		<tr>
			<td>Nama</td>
			<td align="center">:</td>
			<td colspan="6">'.$nm_pengguna.'</td>
		</tr>
		<tr>
			<td>NIM</td>
			<td align="center">:</td>
			<td colspan="6" align="left">'.$nim.'</td>
		</tr>
		<tr>
			<td>Prodi</td>
			<td align="center">:</td>
			<td colspan="6">'.$prodi.'</td>
		</tr>
		<tr>
			<td>Fakultas</td>
			<td align="center">:</td>
			<td colspan="6">'.$fakultas.'</td>
		</tr>
		';
	?>
	
	<tr>
		<th>NO.</th>
		<th colspan="2">NAMA KEGIATAN</th>
		<th>KELOMPOK KEGIATAN</th>
		<th>PENYELENGGARA</th>
		<th>SEMESTER - TAHUN</th>
		<th>NILAI SKP</th>
		<th>BUKTI FISIK</th>
	</tr>
	<?php
	$kueri = "
		select b.nm_krp_khp, b.penyelenggara_krp_khp, e.tahun_ajaran || ' ' || e.nm_semester, b.skor_krp_khp, c.nm_bukti_fisik, f.nm_kelompok_kegiatan
		from kegiatan_2 a, krp_khp b, bukti_fisik c, kegiatan_1 d, semester e, kelompok_kegiatan f
		where a.id_kegiatan_2=b.id_kegiatan_2 and b.id_bukti_fisik=c.id_bukti_fisik and a.id_kegiatan_1=d.id_kegiatan_1 and b.id_semester=e.id_semester and d.id_kelompok_kegiatan=f.id_kelompok_kegiatan and b.id_mhs='".$id_mhs."'
		order by e.thn_akademik_semester
		";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);

	$no = 1;
	while($r = $db->FetchRow()) {
		echo '
			<tr>
				<td align="center" valign="center">'.$no.'</td>
				<td colspan="2">'.$r[0].'</td>
				<td>'.$r[5].'</td>
				<td>'.$r[1].'</td>
				<td>'.$r[2].'</td>
				<td align="center">'.$r[3].'</td>
				<td>'.$r[4].'</td>
			</tr>
		';
		$no++;
	}
	?>
</table>