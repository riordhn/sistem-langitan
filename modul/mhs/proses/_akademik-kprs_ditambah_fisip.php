<?php
require('../../../config.php');
require('../includes/ceking2.php');

$db2 = new MyOracle();
$db3 = new MyOracle();

if($_POST["aksi"]=="tampil") {
	echo "";
	// ambil id_mhs
	$id_mhs=""; $id_prodi=""; $status="";
	$kueri = "select id_mhs,id_program_studi,status from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die ("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
		$id_prodi = $r[1];
		$status = $r[2];
	}
	
	// ambil semester_aktif
	$sem_aktif=""; $tmp_thn_akad=""; $tmp_sem="";
	$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where STATUS_AKTIF_SEMESTER='True' order by id_semester desc";
	$result = $db->Query($kueri)or die ("salah kueri : ");
	while($r = $db->FetchRow()) {
		$sem_aktif = $r[0];
		$tmp_thn_akad = $r[1];
		$tmp_sem = $r[2];
	}
	$kemarin_thn=""; $kemarin_sem="";
	if($tmp_sem=="Ganjil") {
		$kemarin_thn = $tmp_thn_akad-1;
		$kemarin_sem = "Genap";
	}else if($tmp_sem=="Genap") {
		$kemarin_thn = $tmp_thn_akad;
		$kemarin_sem = "Ganjil";
	}
	// ambil semester_kemarin
		$kemarin_idsem="";
		$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		while($r = $db->FetchRow()) {
			$kemarin_idsem = $r[0];
		}

		// apakah semester tsb cuti ?
		$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		$r = $db->FetchRow();
		if($r[0]>0) { // maka cuti ke-1
			// ambil semester sebelum cuti
			$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$sem_aktif_tahun = $r[1];
				$sem_aktif_semes = $r[2];
			}
			if($sem_aktif_semes=="Ganjil") {
				$kemarin_thn = $sem_aktif_tahun-1;
				$kemarin_sem = "Genap";
			}else if($sem_aktif_semes=="Genap") {
				$kemarin_thn = $sem_aktif_tahun;
				$kemarin_sem = "Ganjil";
			}
			$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$kemarin_idsem = $r[0];
			}
			// apakah semester tsb cuti ?
			$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			$r = $db->FetchRow();
			if($r[0]>0) { // maka cuti ke-2
				// ambil semester sebelum cuti
				$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$sem_aktif_tahun = $r[1];
					$sem_aktif_semes = $r[2];
				}
				if($sem_aktif_semes=="Ganjil") {
					$kemarin_thn = $sem_aktif_tahun-1;
					$kemarin_sem = "Genap";
				}else if($sem_aktif_semes=="Genap") {
					$kemarin_thn = $sem_aktif_tahun;
					$kemarin_sem = "Ganjil";
				}
				$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$kemarin_idsem = $r[0];
				}
			}
		}

	// ambil sks_maks
	$sks_maks = 0; $sks_total = 0; $ipk_mhs = 0; $ips_mhs = 0;
	$ips_atas='0'; $ips_bawah='0';
	$bobot["A"] = 4; $bobot["AB"] = 3.5; $bobot["B"] = 3; $bobot["BC"] = 2.5; $bobot["C"] = 2; $bobot["D"] = 1; $bobot["E"] = 0;
	$kueri2 = "
		select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester, d.nilai_huruf
		from mata_kuliah a, semester b, pengambilan_mk d, kurikulum_mk e
		where d.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and d.id_semester=b.id_semester
		and b.id_semester='".$kemarin_idsem."' and d.id_mhs='".$id_mhs."' and d.nilai_huruf is not null
		and d.status_hapus=0 and d.status_apv_pengambilan_mk=1 and d.status_pengambilan_mk !=0
	";
	$result = $db->Query($kueri2)or die("salah kueri : 4");
	while($r = $db->FetchRow()) {
		if(strtoupper($r[1])=='KKN' or strtoupper($r[1])=='KULIAH KERJA NYATA' or strtoupper($r[1])=='SKRIPSI' or strtoupper($r[1])=='TUGAS AKHIR' or strtoupper($r[1])=='PRAKTEK KERJA LAPANGAN' or strtoupper($r[1])=='PKL' or strtoupper($r[1])=='PKL (MAGANG PRAKTEK KERJA LAPANGAN)' or strtoupper($r[1])=='PRAKTEK KERJA LAPANGAN (PKL)' or strtoupper($r[1])=='RESIDENSI DAN PRAKTEK KERJA LAPANGAN (PKL)') {
			if($r[3]=="E") {
				// tidak dihitung
			}else{
				$ips_bawah += $r[2];
				$ips_atas += ($bobot[$r[3]]*$r[2]);
			}
		}else{
			$ips_bawah += $r[2];
			$ips_atas += ($bobot[$r[3]]*$r[2]);
		}
	}
	if($ips_bawah>0) {
		$ips_mhs = number_format(($ips_atas/$ips_bawah),2);
	}else{
		$ips_mhs = '0.00';
	}
	// ambil sks sems depan
		$kueri2 = "select max(sks_maksimal) as sks_maksimal from beban_sks where id_program_studi = ".$id_prodi." and ipk_minimum <= ".$ips_mhs."";
		$result = $db->Query($kueri2)or die("salah kueri : 5");
		while($r = $db->FetchRow()) {
			$sks_maks = $r[0];
		}

	//perhitungan sks total dan ipk versi lukman
		$sql="select a.id_mhs,sum(a.kredit_semester) skstotal,
		round(sum(a.kredit_semester*(case a.nilai_huruf 
		when 'A' then 4 
		when 'AB' then 3.5 
		when 'B' then 3
		when 'BC' then 2.5
		when 'C' then 2
		when 'D' then 1
		end))/sum(a.kredit_semester),2) IPK
		from
		(
		select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
		select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
		from pengambilan_mk a 
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null and a.flagnilai=1
		and a.status_hapus=0 and a.status_apv_pengambilan_mk=1 and a.status_pengambilan_mk !=0
		) a
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where rangking=1 and id_mhs='{$id_mhs}'
		) a
		left join mahasiswa b on a.id_mhs=b.id_mhs
		left join program_studi f on b.id_program_studi=f.id_program_studi
		where a.id_mhs='{$id_mhs}'
		group by a.id_mhs order by a.id_mhs";
		$result2 = $db->Query($sql);
		while($r2 = $db->FetchRow()) {
			$sks_total = $r2[1];
			$ipk_mhs = $r2[2];
		}

	
	// ambil prasyaratnya
		/*
		$lolos_prasyarat=true; $kode_mk_prasyarat="";
		$kueri = "select a.id_prasyarat_mk from prasyarat_mk a, kurikulum_mk b where a.id_kurikulum=b.id_kurikulum and b.id_kelas_mk='".$id_kelas."' ";
		echo $kueri;
		$result = $db->Query($kueri)or die("salah kueri 164");
		while($r = $db->FetchRow()) {
			$lolos_prasyarat=true;
			$id_prasyarat = $r[0];

			$kueri2 = "select id_kurikulum, nilai from group_prasyarat_mk where id_prasyarat_mk='".$id_prasyarat."' ";
			$result2 = $db2->Query($kueri2)or die("salah kueri 169");
			while($r2 = $db2->FetchRow()) {
				$ambil_kuri = $r2[0];
				$ambil_nilai = $r2[1];

				$kueri3 = "select count(*) from kurikulum_mk a, pengambilan_mk b where a.id_kelas_mk=b.id_kelas_mk and a.id_kurikulum='".$ambil_kuri."' and b.id_mhs='".$id_mhs."' ";
				$result3 = $db3->Query($kueri3)or die("salah kueri 177");
				$r3 = $db3->FetchRow();
				if($r3[0] > 0) {
					// ada
				}else if($r3[0] == 0) {
					// ambil kode mtk
					$kueri3 = "select b.kd_mata_kuliah from kurikulum_mk a, mata_kuliah b where a.id_mata_kuliah=b.id_mata_kuliah and a.id_kurikulum_mk='".$ambil_kuri."' ";
					$result3 = $db3->Query($kueri3)or die("salah kueri 185");
					while($r3 = $db3->FetchRow()) {
						$kode_mk_prasyarat = $r3[0];
					}
					$lolos_prasyarat = false;
					break;
				}
			}
			if($lolos_prasyarat==true) {
				break;
			}
		}
		if($lolos_prasyarat==false) {
			$lanjut = false;
			$pesan .= "Tidak lolos pada prasyarat ".$kode_mk_prasyarat."\n";
		}else if($lolos_prasyarat==true) {
			$lanjut = true;
		}
	*/
	
	$isi = '
	<form name="frmkrstambah" id="frmkrstambah">
	<table>
	<tr>
		<th>No.</th>
		<th>KODE MTK</th>
		<th>NAMA MATA AJAR</th>
		<th>SKS MTA</th>
		<th>KELAS</th>
		<th>KAPASITAS</th>
		<th>TERISI</th>
		<th>AKSI</th>
	</tr>
	';
	// mk yg sudah diambil tidak dimunculkan
	//$kueri = "select b.id_kurikulum_mk from pengambilan_mk a, kelas_mk b where a.id_kelas_mk=b.id_kelas_mk and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' and a.STATUS_PENGAMBILAN_MK not in ('3') ";
	$kueri = "select b.id_kurikulum_mk from pengambilan_mk a, kelas_mk b where a.id_kelas_mk=b.id_kelas_mk and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' ";
	$id_kurikulum_sudahada = "";
	$result = $db->Query($kueri)or die("salah kueri 250 : ");
	while($r = $db->FetchRow()) {
		$id_kurikulum_sudahada .= ",".$r[0];
	}
	$id_kurikulum_sudahada = substr($id_kurikulum_sudahada, 1);
	if(strlen($id_kurikulum_sudahada) == 0){
		$id_kurikulum_sudahada = "0";
	}
	/*
	if ($sks_total  < 15) $kelkode="substr(a.kd_mata_kuliah,4,1)='1'"; 
	if ($sks_total  >= 15 and $sks_total  <=48) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2')";
	if ($sks_total  >= 49 and $sks_total  <=95) $kelkode="(substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3')";
	if ($sks_total  >= 96 and $sks_total  <=109) $kelkode="(substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')"; 
	if ($sks_total  >109) $kelkode="substr(a.kd_mata_kuliah,4,1)='4'";
	if ($id_prodi ==89 or $id_prodi==90) $kelkode="substr(a.kd_mata_kuliah,4,1)='0'";
	if ($id_prodi ==98 or $id_prodi==99 or $id_prodi==100 or $id_prodi==101 or $id_prodi==102 or $id_prodi==103 or $id_prodi==104 or $id_prodi==150) $kelkode="substr(a.kd_mata_kuliah,4,1)='6'";
	*/

		
	if ($sks_total  < 15) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1')"; 
	if ($sks_total  >= 15 and $sks_total  <=48) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2')";
	if ($sks_total  >= 49 and $sks_total  <=95) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3')";
	if ($sks_total  >= 96 and $sks_total  <=109) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')"; 
	if ($sks_total  >109) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')";
	if ($id_prodi ==89 or $id_prodi==90) $kelkode="substr(a.kd_mata_kuliah,4,1)='0'";
	if ($id_prodi ==98 or $id_prodi==99 or $id_prodi==100 or $id_prodi==101 or $id_prodi==102 or $id_prodi==103 or $id_prodi==104 or $id_prodi==150) $kelkode="substr(a.kd_mata_kuliah,4,1)='6'";
	if ($status=='AJ') $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')";
	
	/*

	if ($sks_total  < 15) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1')"; 
	if ($sks_total  >= 15 and $sks_total  <=48) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2')";
	if ($sks_total  >= 49 and $sks_total  <=95) $kelkode="(substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3')";

	//if ($sks_total  >= 96 and $sks_total  <=109) $kelkode="(substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')"; 
	//if ($sks_total  >109) $kelkode="(substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')";
	if ($sks_total  >= 96 ) $kelkode="(substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4' or substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='1')"; 
	
	//if ($sks_total  >109) $kelkode="substr(a.kd_mata_kuliah,4,1)='4'";
	if ($sks_total  >= 49 and $sks_total  <=95 and ($sks_total+$sks_maks)>=110) $kelkode="(substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3' or a.kd_mata_kuliah='KNS401')";
	if ($id_prodi ==89 or $id_prodi==90) $kelkode="substr(a.kd_mata_kuliah,4,1)='0'";
	if ($id_prodi ==98 or $id_prodi==99 or $id_prodi==100 or $id_prodi==101 or $id_prodi==102 or $id_prodi==103 or $id_prodi==104 or $id_prodi==150) $kelkode="substr(a.kd_mata_kuliah,4,1)='6'";
	if ($id_prodi ==124) $kelkode="substr(a.kd_mata_kuliah,1,3) in ('IIS','UMU')";
	if ($status=='AJ') $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')";

	*/

	$kueri = "
	select a.kd_mata_kuliah, a.nm_mata_kuliah, d.kredit_semester, d.id_mata_kuliah, d.id_kurikulum_mk, c.no_kelas_mk, c.id_kelas_mk,
	c.terisi_kelas_mk, (case when b.quota = 0 or b.quota is null then c.kapasitas_kelas_mk else b.quota end) as kapasitas_kelas_mk
	from mata_kuliah a, krs_prodi b, kelas_mk c, kurikulum_mk d
	where c.id_kurikulum_mk=d.id_kurikulum_mk and a.id_mata_kuliah=d.id_mata_kuliah and c.id_kelas_mk=b.id_kelas_mk 
	and b.id_semester='".$sem_aktif."' and b.id_program_studi='$id_prodi' and ".$kelkode."
	and d.id_kurikulum_mk not in (".$id_kurikulum_sudahada.")
	order by a.kd_mata_kuliah, c.no_kelas_mk";
	//echo $kueri;
	//echo "aaa";
	$hit=0;
	$result = $db->Query($kueri)or die("salah kueri 252 : ".$sks_total);
	while($r = $db->FetchRow()) {
		$hit++;
		$nm_kelas = "";
		$result2 = $db2->Query("select nama_kelas from nama_kelas where id_nama_kelas='".$r[5]."'")or die("salah kueri 42 : ");
		while($r2 = $db2->FetchRow()) {
			$nm_kelas = $r2[0];
		}
		$kelas_terisi = "0";
		$result2 = $db2->Query("select count(*) from pengambilan_mk where id_kelas_mk='".$r[6]."'")or die("salah kueri 42 : ");
		while($r2 = $db2->FetchRow()) {
			$kelas_terisi = $r2[0];
		}
		$isi .= '
			<tr>
				<td>'.$hit.'</td>
				<td>'.$r[0].'</td>
				<td>'.$r[1].'</td>
				<td>'.$r[2].'</td>
				<td>'.$nm_kelas.'</td>
				<td>'.$r[8].'</td>
				<td>'.$kelas_terisi.'</td>
				<td>
				';
				if($r[7] >= $r[8] ){
					$isi .= '';
				}else{
					$isi .= '<input type="button" name="simpan" value="Ambil" onclick="krstambah_kirim('.$r[6].')" >';
				}
				$isi .= '
				</td>
			</tr>
		';
	}
	
	// ambil sks terambil
	$sks_terambil=0;
	$kueri = "
	select sum(d.kredit_semester)
	from pengambilan_mk a, kelas_mk b, mata_kuliah c, kurikulum_mk d
	where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=b.id_semester
	and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' and a.status_pengambilan_mk!='4'
	";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$sks_terambil = $r[0];
	}
	if(strlen($sks_terambil)==0) { $sks_terambil = '0'; }

	$sks_sisa = $sks_maks-$sks_terambil;
	
	$isi .= '
	<tr>
		<td colspan="8">
		<b>IPK : '.$ipk_mhs.' <br/>
		<b>IPS : '.$ips_mhs.' <br/>
		<b>MAX SKS : '.$sks_maks.' <br/>
		TERAMBIL : '.$sks_terambil.' <br> 
		SISA : '.$sks_sisa.'</b>
		</td>
	</tr>
	</table>
	</form>
	';
	echo $isi;
	
}else if($_POST["aksi"]=="input" and $_POST["kelas"] and $_POST["sid"] == session_id()) {
	$id_kelas = $_POST["kelas"]; $lanjut = true; $pesan = "";
	if(harusAngka($_POST["kelas"])) {
		$id_kelas = $_POST["kelas"];
	}else{
		$pesan .= "Illegal Character\n";
		$id_kelas = '0';
		$lanjut = false;
	}
	
	// ambil id_mhs
	$id_mhs=""; $id_prodi=""; $status="";
	$kueri = "select id_mhs,id_program_studi,status from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'";
	$result = $db->Query($kueri)or die ("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
		$id_prodi = $r[1];
		$status = $r[2];
	}

	// ambil id_kurikulum
	$id_kurikulum_mk=""; $id_mata_kuliah="";
	//$kueri = "select b.id_kurikulum_mk, b.id_mata_kuliah from kelas_mk a, kurikulum_mk b where a.id_kurikulum_mk=b.id_kurikulum_mk and a.id_kelas_mk='".$id_kelas."' and b.id_program_studi='".$id_prodi."'";
	$kueri = "
	select id_kurikulum_mk,id_mata_kuliah from kurikulum_mk kmk1
	where kmk1.id_program_studi='".$id_prodi."' and kmk1.id_mata_kuliah in
	(select id_mata_kuliah from mata_kuliah where kd_mata_kuliah in
	(select c.kd_mata_kuliah from kelas_mk a, kurikulum_mk b, mata_kuliah c
	where a.id_kurikulum_mk=b.id_kurikulum_mk and a.id_kelas_mk='".$id_kelas."' and c.id_mata_kuliah=b.id_mata_kuliah))
	";
	
	$result = $db->Query($kueri)or die("salah kueri 140");
	while($r = $db->FetchRow()) {
		$id_kurikulum_mk = $r[0];
		$id_mata_kuliah = $r[1];
	}

	// ambil sem_aktif
	$sem_aktif=""; $tmp_thn_akad=""; $tmp_sem="";
	$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where STATUS_AKTIF_SEMESTER='True' order by id_semester desc";
	$result = $db->Query($kueri)or die ("salah kueri : ");
	while($r = $db->FetchRow()) {
		$sem_aktif = $r[0];
		$tmp_thn_akad = $r[1];
		$tmp_sem = $r[2];
	}

	// cek apakah sudah ada di tabel pengambilan_mk_kprs
	$tempe_ada="0";
	$kueri = "select count(*) from pengambilan_mk_kprs where id_semester='".$sem_aktif."' and id_mhs='".$id_mhs."' ";
	$result = $db->Query($kueri)or die("salah kueri 266");
	while($r = $db->FetchRow()) {
		$tempe_ada = $r[0];
	}
	$krs_ada="0";
	$kueri = "select count(*) from pengambilan_mk where id_semester='".$sem_aktif."' and id_mhs='".$id_mhs."' and STATUS_APV_PENGAMBILAN_MK='1' ";
	$result = $db->Query($kueri)or die("salah kueri 266");
	while($r = $db->FetchRow()) {
		$krs_ada = $r[0];
	}
	if($krs_ada=='0') {
		// bila krs kosong, atau belum disetujui, maka tidak di insert ke tabel pengambilan_mk_kprs
	}else if($tempe_ada=='0') {
		// yg di insert adalah yg sudah approve saja
		$kueri = "select id_pengambilan_mk,id_mhs,id_kelas_mk,id_semester,STATUS_PENGAMBILAN_MK,STATUS_APV_PENGAMBILAN_MK from pengambilan_mk where id_semester='".$sem_aktif."' and id_mhs='".$id_mhs."' and STATUS_APV_PENGAMBILAN_MK='1' ";
		$result = $db->Query($kueri)or die("salah kueri 274");
		while($r = $db->FetchRow()) {
			$kueri2 = "insert into pengambilan_mk_kprs (id_mhs,id_kelas_mk,id_semester,STATUS_PENGAMBILAN_MK,STATUS_APV_PENGAMBILAN_MK) values ('".$id_mhs."','".$r[2]."','".$sem_aktif."','".$r[4]."','".$r[5]."')";
			$result2 = $db2->Query($kueri2)or die("Salah kueri 277");
			//FISIP HARUS IJIN DOLI UNTUK KPRS
			$kueri3 = "update pengambilan_mk set STATUS_APV_PENGAMBILAN_MK='1' where id_pengambilan_mk='".$r[0]."' and id_mhs='".$id_mhs."'";
			$result3 = $db3->Query($kueri3)or die("Salah kueri 280");
		}
	}

	$kemarin_thn=""; $kemarin_sem="";
	if($tmp_sem=="Ganjil") {
		$kemarin_thn = $tmp_thn_akad-1;
		$kemarin_sem = "Genap";
	}else if($tmp_sem=="Genap") {
		$kemarin_thn = $tmp_thn_akad;
		$kemarin_sem = "Ganjil";
	}
	// ambil semester_kemarin
		$kemarin_idsem="";
		$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		while($r = $db->FetchRow()) {
			$kemarin_idsem = $r[0];
		}

		// apakah semester tsb cuti ?
		$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		$r = $db->FetchRow();
		if($r[0]>0) { // maka cuti ke-1
			// ambil semester sebelum cuti
			$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$sem_aktif_tahun = $r[1];
				$sem_aktif_semes = $r[2];
			}
			if($sem_aktif_semes=="Ganjil") {
				$kemarin_thn = $sem_aktif_tahun-1;
				$kemarin_sem = "Genap";
			}else if($sem_aktif_semes=="Genap") {
				$kemarin_thn = $sem_aktif_tahun;
				$kemarin_sem = "Ganjil";
			}
			$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$kemarin_idsem = $r[0];
			}
			// apakah semester tsb cuti ?
			$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			$r = $db->FetchRow();
			if($r[0]>0) { // maka cuti ke-2
				// ambil semester sebelum cuti
				$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$sem_aktif_tahun = $r[1];
					$sem_aktif_semes = $r[2];
				}
				if($sem_aktif_semes=="Ganjil") {
					$kemarin_thn = $sem_aktif_tahun-1;
					$kemarin_sem = "Genap";
				}else if($sem_aktif_semes=="Genap") {
					$kemarin_thn = $sem_aktif_tahun;
					$kemarin_sem = "Ganjil";
				}
				$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$kemarin_idsem = $r[0];
				}
			}
		}

	// ambil sks_maks
	$sks_maks = 0; $sks_total = 0; $ipk_mhs = 0; $ips_mhs = 0;
	$ips_atas='0'; $ips_bawah='0';
	$bobot["A"] = 4; $bobot["AB"] = 3.5; $bobot["B"] = 3; $bobot["BC"] = 2.5; $bobot["C"] = 2; $bobot["D"] = 1; $bobot["E"] = 0;
	$kueri2 = "
		select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester, d.nilai_huruf
		from mata_kuliah a, semester b, pengambilan_mk d, kurikulum_mk e
		where d.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and d.id_semester=b.id_semester
		and b.id_semester='".$kemarin_idsem."' and d.id_mhs='".$id_mhs."' and d.nilai_huruf is not null
		and d.status_hapus=0 and d.status_apv_pengambilan_mk=1 and d.status_pengambilan_mk !=0
	";
	$result = $db->Query($kueri2)or die("salah kueri : 4");
	while($r = $db->FetchRow()) {
		if(strtoupper($r[1])=='KKN' or strtoupper($r[1])=='KULIAH KERJA NYATA' or strtoupper($r[1])=='SKRIPSI' or strtoupper($r[1])=='TUGAS AKHIR' or strtoupper($r[1])=='PRAKTEK KERJA LAPANGAN' or strtoupper($r[1])=='PKL' or strtoupper($r[1])=='PKL (MAGANG PRAKTEK KERJA LAPANGAN)' or strtoupper($r[1])=='PRAKTEK KERJA LAPANGAN (PKL)' or strtoupper($r[1])=='RESIDENSI DAN PRAKTEK KERJA LAPANGAN (PKL)') {
			if($r[3]=="E") {
				// tidak dihitung
			}else{
				$ips_bawah += $r[2];
				$ips_atas += ($bobot[$r[3]]*$r[2]);
			}
		}else{
			$ips_bawah += $r[2];
			$ips_atas += ($bobot[$r[3]]*$r[2]);
		}
	}
	if($ips_bawah>0) {
		$ips_mhs = number_format(($ips_atas/$ips_bawah),2);
	}else{
		$ips_mhs = '0.00';
	}
	// ambil sks sems depan
		$kueri2 = "select max(sks_maksimal) as sks_maksimal from beban_sks where id_program_studi = ".$id_prodi." and ipk_minimum <= ".$ips_mhs."";
		$result = $db->Query($kueri2)or die("salah kueri : 5");
		while($r = $db->FetchRow()) {
			$sks_maks = $r[0];
		}
	//perhitungan sks total dan ipk versi lukman
		$sql="select a.id_mhs,sum(a.kredit_semester) skstotal,
		round(sum(a.kredit_semester*(case a.nilai_huruf 
		when 'A' then 4 
		when 'AB' then 3.5 
		when 'B' then 3
		when 'BC' then 2.5
		when 'C' then 2
		when 'D' then 1
		end))/sum(a.kredit_semester),2) IPK
		from
		(
			select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
				select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
				from pengambilan_mk a 
				left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
				left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
				where a.nilai_huruf<'E' and a.nilai_huruf is not null
				and a.status_hapus=0 and a.status_apv_pengambilan_mk=1 and a.status_pengambilan_mk !=0
			) a
			left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
			left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
			where rangking=1 and id_mhs='{$id_mhs}'
		) a
		left join mahasiswa b on a.id_mhs=b.id_mhs
		left join program_studi f on b.id_program_studi=f.id_program_studi
		where a.id_mhs='{$id_mhs}'
		group by a.id_mhs order by a.id_mhs";
		$result2 = $db->Query($sql);
		while($r2 = $db->FetchRow()) {
			$sks_total = $r2[1];
			$ipk_mhs = $r2[2];
		}

	/*
	if ($sks_total  < 15) $kelkode="substr(a.kd_mata_kuliah,4,1)='1'"; 
	if ($sks_total  >= 15 and $sks_total  <=48) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2')";
	if ($sks_total  >= 49 and $sks_total  <=95) $kelkode="(substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3')";
	if ($sks_total  >= 96 and $sks_total  <=109) $kelkode="(substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')"; 
	if ($sks_total  >109) $kelkode="substr(a.kd_mata_kuliah,4,1)='4'";
	if ($id_prodi ==89 or $id_prodi==90) $kelkode="substr(a.kd_mata_kuliah,4,1)='0'";
	if ($id_prodi ==98 or $id_prodi==99 or $id_prodi==100 or $id_prodi==101 or $id_prodi==102 or $id_prodi==103 or $id_prodi==104 or $id_prodi==150) $kelkode="substr(a.kd_mata_kuliah,4,1)='6'";
	*/
	

	if ($sks_total  < 15) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1')"; 
	if ($sks_total  >= 15 and $sks_total  <=48) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2')";
	if ($sks_total  >= 49 and $sks_total  <=95) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3')";
	if ($sks_total  >= 96 and $sks_total  <=109) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')"; 
	if ($sks_total  >109) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')";
	if ($id_prodi ==89 or $id_prodi==90) $kelkode="substr(a.kd_mata_kuliah,4,1)='0'";
	if ($id_prodi ==98 or $id_prodi==99 or $id_prodi==100 or $id_prodi==101 or $id_prodi==102 or $id_prodi==103 or $id_prodi==104 or $id_prodi==150) $kelkode="substr(a.kd_mata_kuliah,4,1)='6'";
	if ($status=='AJ') $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')";

	/*
	if ($sks_total  < 15) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1')"; 
	if ($sks_total  >= 15 and $sks_total  <=48) $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2')";
	if ($sks_total  >= 49 and $sks_total  <=95) $kelkode="(substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3')";
	
	//if ($sks_total  >= 96 and $sks_total  <=109) $kelkode="(substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')"; 
	//if ($sks_total  >109) $kelkode="(substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')";
	
	if ($sks_total  >= 96 ) $kelkode="(substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4' or substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='1')"; 

	//if ($sks_total  >109) $kelkode="substr(a.kd_mata_kuliah,4,1)='4'";
	if ($sks_total  >= 49 and $sks_total  <=95 and ($sks_total+$sks_maks)>=110) $kelkode="(substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3' or a.kd_mata_kuliah='KNS401')";
	if ($id_prodi ==89 or $id_prodi==90) $kelkode="substr(a.kd_mata_kuliah,4,1)='0'";
	if ($id_prodi ==98 or $id_prodi==99 or $id_prodi==100 or $id_prodi==101 or $id_prodi==102 or $id_prodi==103 or $id_prodi==104 or $id_prodi==150) $kelkode="substr(a.kd_mata_kuliah,4,1)='6'";
	if ($id_prodi ==124) $kelkode="substr(a.kd_mata_kuliah,1,3) in ('IIS','UMU')";
	if ($status=='AJ') $kelkode="(substr(a.kd_mata_kuliah,4,1)='1' or substr(a.kd_mata_kuliah,4,1)='2' or substr(a.kd_mata_kuliah,4,1)='3' or substr(a.kd_mata_kuliah,4,1)='4')";

	*/

	$kueri = "
	select a.kd_mata_kuliah, a.nm_mata_kuliah, d.kredit_semester, d.id_mata_kuliah, d.id_kurikulum_mk, c.no_kelas_mk, c.id_kelas_mk,
	c.terisi_kelas_mk, (case when b.quota = 0 or b.quota is null then c.kapasitas_kelas_mk else b.quota end) as kapasitas_kelas_mk
	from mata_kuliah a, krs_prodi b, kelas_mk c, kurikulum_mk d
	where c.id_kurikulum_mk=d.id_kurikulum_mk and a.id_mata_kuliah=d.id_mata_kuliah and c.id_kelas_mk=b.id_kelas_mk
	and b.id_semester='".$sem_aktif."' and b.id_program_studi='$id_prodi' and ".$kelkode."
	order by a.kd_mata_kuliah, c.no_kelas_mk";

	//echo $kueri;


	//$result = $db->Query($kueri)or die("salah kueri 25 : ".$sem_aktif.'-'.$sks_total.'-'.$id_prodi);
	$result = $db->Query($kueri)or die("salah kueri 25 : ");
	while($r = $db->FetchRow()) {
		$mk_ditawarkan = true;
	}
	if($mk_ditawarkan == false) {
		$lanjut = false;
		$pesan .= "Mta tidak ditawarkan\n";
	}

	// ambil prasyaratnya
	$lolos_prasyarat=true; $kode_mk_prasyarat="";
	//$kueri = "select a.id_prasyarat_mk from prasyarat_mk a, kurikulum_mk b where a.id_kurikulum=b.id_kurikulum and b.id_kelas_mk='".$id_kelas."' ";
	$kueri = "select id_prasyarat_mk from prasyarat_mk where id_kurikulum_mk='".$id_kurikulum_mk."' ";
	
	$result = $db->Query($kueri)or die("salah kueri 164");
	while($r = $db->FetchRow()) {
		$lolos_prasyarat=true;
		$id_prasyarat = $r[0];

		$kueri2 = "select id_kurikulum_mk, min_nilai_huruf from group_prasyarat_mk where id_prasyarat_mk='".$id_prasyarat."' ";
		$result2 = $db2->Query($kueri2)or die("salah kueri 169");
		while($r2 = $db2->FetchRow()) {
			$ambil_kuri = $r2[0];
			$ambil_nilai = $r2[1];

			$kueri3 = "select count(*) from kurikulum_mk a, pengambilan_mk b, kelas_mk c where a.id_kurikulum_mk=c.id_kurikulum_mk and c.id_kelas_mk=b.id_kelas_mk and a.id_kurikulum_mk='".$ambil_kuri."' and b.id_mhs='".$id_mhs."' and b.id_semester!='".$sem_aktif."' ";
			$result3 = $db3->Query($kueri3)or die("salah kueri 177");
			$r3 = $db3->FetchRow();
			if($r3[0] > 0) {
				// ada
			}else if($r3[0] == 0) {
				// ambil kode mtk
				$kueri3 = "select b.kd_mata_kuliah from kurikulum_mk a, mata_kuliah b where a.id_mata_kuliah=b.id_mata_kuliah and a.id_kurikulum_mk='".$ambil_kuri."' ";
				$result3 = $db3->Query($kueri3)or die("salah kueri 185");
				while($r3 = $db3->FetchRow()) {
					$kode_mk_prasyarat = $r3[0];
				}
				$lolos_prasyarat = false;
				break;
			}
		}
		if($lolos_prasyarat==true) {
			break;
		}
	}
	$lolos_prasyarat=true;
	if($lolos_prasyarat==false) {
		$lanjut = false;
		$pesan .= "Tidak lolos pada prasyarat ".$kode_mk_prasyarat."\n";
	}
	
	if($lanjut){
		// cek apakah sudah diambil
		$kueri = "
		select count(*) from pengambilan_mk a, kelas_mk b 
		where a.id_kelas_mk=b.id_kelas_mk and a.id_semester='".$sem_aktif."' and b.id_kelas_mk='".$id_kelas."' and id_mhs='".$id_mhs."'";
		$result = $db->Query($kueri)or die("salah kueri 287 ");
		while($r = $db->FetchRow()) {
			$sudah_ambil = $r[0];
		}
		if($sudah_ambil > 0){
			$lanjut = false;
			$pesan .= "Mata Ajar sudah diambil\n";
		}
	}
	
	// ambil sks terambil
	$sks_terambil=0;
	$kueri = "
	select sum(d.kredit_semester)
	from pengambilan_mk a, kelas_mk b, mata_kuliah c, kurikulum_mk d
	where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=d.id_kurikulum_mk and c.id_mata_kuliah=d.id_mata_kuliah and a.id_semester=b.id_semester
	and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' and a.status_pengambilan_mk!='4'
	";
	$result = $db->Query($kueri)or die("salah kueri 176");
	$r = $db->FetchRow();
	$sks_terambil = $r[0];
	if(strlen($sks_terambil)==0 ) {
		$sks_terambil = '0';
	}

	// ambil sks_maks
	/*
		$sks_maks = 0;
		$kueri = "
		select sks_max from mhs_status where id_mhs='".$id_mhs."' and id_semester='".$sem_aktif."' order by id_mhs_status desc
		";
		$result = $db->Query($kueri)or die("salah kueri 185");
		$r = $db->FetchRow();
		$sks_maks = $r[0];
		if(strlen($sks_maks)==0 ) {
			$sks_maks = '0';
		}
	*/
	
	$sks_ditambahkan=0;
	$kueri = "
	select b.kredit_semester from kelas_mk a, kurikulum_mk b, mata_kuliah c
	where a.id_kurikulum_mk=b.id_kurikulum_mk and b.id_mata_kuliah=c.id_mata_kuliah and a.id_kelas_mk='".$id_kelas."'
	";
	$result = $db->Query($kueri)or die("salah kueri 194 ");
	$r = $db->FetchRow();
	$sks_ditambahkan = $r[0];
	if(strlen($sks_ditambahkan)==0 ) {
		$sks_ditambahkan = '0';
	}
	
	if($lanjut and ($sks_terambil+$sks_ditambahkan)>$sks_maks ) {
		$lanjut = false;
		$pesan .= "Sks diambil melebihi jatah maksimal\n";
	}

	// cek jadwal tabrakan
	$kueri = "select id_jadwal_hari,id_jadwal_jam from jadwal_kelas where id_kelas_mk='".$id_kelas."' ";
	$result = $db->Query($kueri)or die("salah kueri 206");
	while($r = $db->FetchRow()) {
		$jadwal_hari = $r[0];
		$jadwal_jam = $r[1];
	}
	$kueri = "select e.kd_mata_kuliah
	from pengambilan_mk a, jadwal_kelas b, kelas_mk c, kurikulum_mk d, mata_kuliah e
	where a.id_kelas_mk=b.id_kelas_mk and a.id_kelas_mk=c.id_kelas_mk and c.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=e.id_mata_kuliah and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' and b.id_jadwal_hari='".$jadwal_hari."' and b.id_jadwal_jam='".$jadwal_jam."' ";
	$result = $db->Query($kueri)or die("salah kueri 206");
	while($r = $db->FetchRow()) {
		$lanjut = false;
		$pesan .= "Jadwal tabrakan dengan kode mata ajar ".$r[0]."\n";
	}

	
	/*
	if($lanjut) {
		$result = 0;
		$kueri = "update kelas_mk set terisi_kelas_mk=(terisi_kelas_mk+1) where id_kelas_mk='".$id_kelas."' and (terisi_kelas_mk+2) <= kapasitas_kelas_mk";
		$result = $db->Query($kueri)or die("Salah kueri 334");
		echo "sss".$result;
		
		if($result) {
			// insert
			$kueri = "INSERT INTO PENGAMBILAN_MK (ID_MHS, ID_SEMESTER, STATUS_PENGAMBILAN_MK, ID_KELAS_MK, STATUS_APV_PENGAMBILAN_MK ) VALUES ('".$id_mhs."', '".$sem_aktif."', '1', '".$id_kelas."', '0')";
			$result2 =  $db->Query($kueri)or die("salah kueri 338: ");
			if($result2) {
				echo 'Proses berhasil';
			}else{
				echo 'Proses gagal';
			}
		}else{
			echo 'Kelas sudah penuh';
		}
	}else{
		echo $pesan;
	}
	*/
	
	if($lanjut) {
		/*
		$kueri = "
		INSERT INTO PENGAMBILAN_MK (ID_MHS, ID_SEMESTER, STATUS_PENGAMBILAN_MK, ID_KELAS_MK, STATUS_APV_PENGAMBILAN_MK )  
		(select '".$id_mhs."', '".$sem_aktif."', '1', '".$id_kelas."', '0'
		from kelas_mk where (terisi_kelas_mk+2) <= kapasitas_kelas_mk and id_kelas_mk='".$id_kelas."'
		)
		";
		$result = $db->Query($kueri)or die("Salah kueri 234");
		
		if($result) {
			// insert
			$kueri = "update kelas_mk set terisi_kelas_mk=(terisi_kelas_mk+1) where id_kelas_mk='".$id_kelas."' and (terisi_kelas_mk+2) <= kapasitas_kelas_mk";
			$result2 =  $db->Query($kueri)or die("salah kueri 239 ");
			if($result2) {
				echo 'Proses fisip berhasil';
			}else{
				echo 'Proses gagal';
			}
		}else{
			echo 'Kelas sudah penuh';
		}
		*/

		// ambil sudah diulang berapa kali
		$kueri = "select count(*)
		from pengambilan_mk a, kurikulum_mk b, mata_kuliah c
		where a.id_kurikulum_mk=b.id_kurikulum_mk and b.id_mata_kuliah=c.id_mata_kuliah 
		and a.id_mhs='".$id_mhs."' and b.id_program_studi='".$id_prodi."' and c.id_mata_kuliah='".$id_mata_kuliah."'
		";
		$jum_ulang=0;
		$result = $db->Query($kueri)or die("salah kueri 416");
		while($r = $db->FetchRow()) {
			$jum_ulang = $r[0];
		}
		
		$kueri = "
		INSERT INTO PENGAMBILAN_MK (ID_MHS, ID_SEMESTER, STATUS_PENGAMBILAN_MK, ID_KELAS_MK, STATUS_APV_PENGAMBILAN_MK, STATUS_ULANGKE, ID_KURIKULUM_MK )  
		(
		select '".$id_mhs."', '".$sem_aktif."', '2', '".$id_kelas."', '0', '".$jum_ulang."', '".$id_kurikulum_mk."'
		from kelas_mk where id_kelas_mk='".$id_kelas."' and kapasitas_kelas_mk>
		  (
		  select count(*) from pengambilan_mk where id_kelas_mk='".$id_kelas."'
		  )
		)
		";
		$result = $db->Query($kueri)or die("Salah kueri 234");
		
		if($result) {
			echo 'Proses berhasil';
		}else{
			echo 'Kelas sudah penuh';
		}
		
	}else{
		echo $pesan;
	}
	
}else{
	echo "???";
}





?>