<?php
require('../../../config.php');
$db2 = new MyOracle();

if($_POST["aksi"]=="tampil") {
	
	$isi = '
	<table>
	<tr>
		<th>No.</th>
		<th>KODE MTA</th>
		<th>NAMA MATA AJAR</th>
		<th>SKS MTA</font></th>
		<th>KELAS</th>
		<th>STATUS</th>
	</tr>
	';
	// ambil semester_aktif
	$sem_aktif=""; $sem_aktif_tahun=""; $sem_aktif_semes="";
	$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where STATUS_AKTIF_SEMESTER='True' order by id_semester desc";
	$result = $db->Query($kueri)or die ("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$sem_aktif = $r[0];
		$sem_aktif_tahun = $r[1];
		$sem_aktif_semes = $r[2];
	}

	// ambil id_mhs
	$id_mhs=""; $id_prodi="";
	$result = $db->Query("select id_mhs,id_program_studi from mahasiswa where id_pengguna='".$user->ID_PENGGUNA."'")or die("salah kueri 18 : ".$db->Error());
	while($r = $db->FetchRow()) {
		$id_mhs = $r[0];
		$id_prodi = $r[1];
	}

	$kueri = "
	select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester,d.id_pengambilan_mk,c.no_kelas_mk,d.STATUS_APV_PENGAMBILAN_MK
	from mata_kuliah a, kelas_mk c, pengambilan_mk d, kurikulum_mk e
	where c.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and c.id_semester=d.id_semester and c.id_kelas_mk=d.id_kelas_mk
	and c.id_semester='".$sem_aktif."' and d.id_mhs='".$id_mhs."' and d.STATUS_APV_PENGAMBILAN_MK='1'
	order by a.kd_mata_kuliah
	";
	$hit=0;
	$result = $db->Query($kueri)or die("salah kueri 31 ");
	while($r = $db->FetchRow()) {
		$hit++;
		$nm_kelas = "";
		$result2 = $db2->Query("select nama_kelas from nama_kelas where id_nama_kelas='".$r[4]."'")or die("salah kueri 42 : ".$db2->Error());
		while($r2 = $db2->FetchRow()) {
			$nm_kelas = $r2[0];
		}

		if($r[5]=='1') {
			$status = "Approved";
		}else{
			$status = "Not Approved";
		}
		$isi .= '
			<tr>
				<td>'.$hit.'</td>
				<td>'.$r[0].'</td>
				<td>'.$r[1].'</td>
				<td>'.$r[2].'</td>
				<td>'.$nm_kelas.'</td>
				<td>'.$status.'</td>
			</tr>
		';
	}

	// ambil sks terambil
	$sks_terambil=0;
	$kueri = "
	select sum(d.kredit_semester)
	from pengambilan_mk a, kelas_mk b, mata_kuliah c, kurikulum_mk d
	where a.id_kelas_mk=b.id_kelas_mk and b.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=b.id_semester
	and a.id_mhs='".$id_mhs."' and a.id_semester='".$sem_aktif."' and a.STATUS_APV_PENGAMBILAN_MK ='1'
	";
	$result = $db->Query($kueri)or die("salah kueri : ".$kueri);
	while($r = $db->FetchRow()) {
		$sks_terambil = $r[0];
	}
	if(strlen($sks_terambil)==0) { $sks_terambil = '0'; }

	$kemarin_thn=""; $kemarin_sem="";
	if($sem_aktif_semes=="Ganjil") {
		$kemarin_thn = $sem_aktif_tahun-1;
		$kemarin_sem = "Genap";
	}else if($sem_aktif_semes=="Genap") {
		$kemarin_thn = $sem_aktif_tahun;
		$kemarin_sem = "Ganjil";
	}
	
	// ambil semester_kemarin
		$kemarin_idsem="";
		$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		while($r = $db->FetchRow()) {
			$kemarin_idsem = $r[0];
		}

		// apakah semester tsb cuti ?
		$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
		$result = $db->Query($kueri)or die ("salah kueri : ");
		$r = $db->FetchRow();
		if($r[0]>0) { // maka cuti ke-1
			// ambil semester sebelum cuti
			$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$sem_aktif_tahun = $r[1];
				$sem_aktif_semes = $r[2];
			}
			if($sem_aktif_semes=="Ganjil") {
				$kemarin_thn = $sem_aktif_tahun-1;
				$kemarin_sem = "Genap";
			}else if($sem_aktif_semes=="Genap") {
				$kemarin_thn = $sem_aktif_tahun;
				$kemarin_sem = "Ganjil";
			}
			$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			while($r = $db->FetchRow()) {
				$kemarin_idsem = $r[0];
			}
			// apakah semester tsb cuti ?
			$kueri = "select count(*) from admisi where id_mhs='".$id_mhs."' and id_semester='".$kemarin_idsem."' and status_akd_mhs='2'";
			$result = $db->Query($kueri)or die ("salah kueri : ");
			$r = $db->FetchRow();
			if($r[0]>0) { // maka cuti ke-2
				// ambil semester sebelum cuti
				$kueri = "select id_semester,thn_akademik_semester,nm_semester from semester where id_semester='".$kemarin_idsem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$sem_aktif_tahun = $r[1];
					$sem_aktif_semes = $r[2];
				}
				if($sem_aktif_semes=="Ganjil") {
					$kemarin_thn = $sem_aktif_tahun-1;
					$kemarin_sem = "Genap";
				}else if($sem_aktif_semes=="Genap") {
					$kemarin_thn = $sem_aktif_tahun;
					$kemarin_sem = "Ganjil";
				}
				$kueri = "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."'";
				$result = $db->Query($kueri)or die ("salah kueri : ");
				while($r = $db->FetchRow()) {
					$kemarin_idsem = $r[0];
				}
			}
		}
	
	// ambil sks_maks
	$sks_maks = 0; $sks_total = 0; $ipk_mhs = 0; $ips_mhs = 0;
	$ips_atas='0'; $ips_bawah='0';
	$bobot["A"] = 4; $bobot["AB"] = 3.5; $bobot["B"] = 3; $bobot["BC"] = 2.5; $bobot["C"] = 2; $bobot["D"] = 1; $bobot["E"] = 0;
	/*
		$kueri2 = "
			select a.kd_mata_kuliah,a.nm_mata_kuliah,e.kredit_semester, d.nilai_huruf
			from mata_kuliah a, semester b, pengambilan_mk d, kurikulum_mk e
			where d.id_kurikulum_mk=e.id_kurikulum_mk and a.id_mata_kuliah=e.id_mata_kuliah and d.id_semester=b.id_semester
			and b.id_semester='".$kemarin_idsem."' and d.id_mhs='".$id_mhs."' and d.nilai_huruf is not null
		";
		$result = $db->Query($kueri2)or die("salah kueri : 4");
		while($r = $db->FetchRow()) {
			if(strtoupper($r[1])=='KKN' or strtoupper($r[1])=='KULIAH KERJA NYATA' or strtoupper($r[1])=='SKRIPSI' or strtoupper($r[1])=='TUGAS AKHIR' or strtoupper($r[1])=='PRAKTEK KERJA LAPANGAN' or strtoupper($r[1])=='PKL' or strtoupper($r[1])=='PKL (MAGANG PRAKTEK KERJA LAPANGAN)' or strtoupper($r[1])=='PRAKTEK KERJA LAPANGAN (PKL)' or strtoupper($r[1])=='RESIDENSI DAN PRAKTEK KERJA LAPANGAN (PKL)') {
				if($r[3]=="E") {
					// tidak dihitung
				}else{
					$ips_bawah += $r[2];
					$ips_atas += ($bobot[$r[3]]*$r[2]);
				}
			}else{
				$ips_bawah += $r[2];
				$ips_atas += ($bobot[$r[3]]*$r[2]);
			}
		}
		if($ips_bawah>0) {
			$ips_mhs = number_format(($ips_atas/$ips_bawah),2);
		}else{
			$ips_mhs = '0.00';
		}
	*/

	$db->Query("select sum(kredit_semester) as sks_sem, sum((bobot*kredit_semester)) as bobot_total, 
	case when sum(kredit_semester)=0 then 0 else round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips
	from 
	(
	select kredit_semester,bobot
	from(
	select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
	case when kurikulum_mk.status_mkta='1' 
	and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas in (7,12) then 0 else kurikulum_mk.kredit_semester end as kredit_semester, 
	case when kurikulum_mk.status_mkta='1' 
	and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
	when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
	case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
	coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
	nilai_angka,a.besar_nilai_mk as UTS,b.besar_nilai_mk as UAS,
	row_number() over(partition by pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,pengambilan_mk.id_kelas_mk order by nilai_huruf) rangking
	from PENGAMBILAN_MK
	left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
	left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
	left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
	left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
	left join semester on pengambilan_mk.id_semester=semester.id_semester
	left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
	left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
	left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
	left join nilai_mk a on pengambilan_mk.id_pengambilan_mk=a.id_pengambilan_mk and a.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UTS')
	left join nilai_mk b on pengambilan_mk.id_pengambilan_mk=b.id_pengambilan_mk and b.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UAS')
	where  group_semester||thn_akademik_semester in
	(select group_semester||thn_akademik_semester from semester where id_semester='".$kemarin_idsem."')
	and tipe_semester in ('UP','REG','RD')
	and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0 and flagnilai='1'
	and mahasiswa.id_mhs='".$id_mhs."') 
	where rangking=1
	)");
	$rw = $db->FetchAssoc();
	$ips_mhs = $rw['IPS'];
	$ips_bawah = $rw['SKS_SEM'];

	// ambil sks sems depan
		$kueri2 = "select max(sks_maksimal) as sks_maksimal from beban_sks where id_program_studi = ".$id_prodi." and ipk_minimum <= ".$ips_mhs."";
		$result = $db->Query($kueri2)or die("salah kueri : 131");
		while($r = $db->FetchRow()) {
			$sks_maks = $r[0];
		}
		$sks_maks = $sks_maks+1;
		if($sks_maks>24) {
			$sks_maks=24;
		}

	//perhitungan sks total dan ipk versi lukman
		$sql="select a.id_mhs,sum(a.kredit_semester) skstotal,
		round(sum(a.kredit_semester*(case a.nilai_huruf 
		when 'A' then 4 
		when 'AB' then 3.5 
		when 'B' then 3
		when 'BC' then 2.5
		when 'C' then 2
		when 'D' then 1
		end))/sum(a.kredit_semester),2) IPK
		from
		(
		select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
		select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
		from pengambilan_mk a 
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null and a.flagnilai=1
		) a
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where rangking=1 and id_mhs='{$id_mhs}'
		) a
		left join mahasiswa b on a.id_mhs=b.id_mhs
		left join program_studi f on b.id_program_studi=f.id_program_studi
		where a.id_mhs='{$id_mhs}'
		group by a.id_mhs order by a.id_mhs";
		$result2 = $db->Query($sql);
		while($r2 = $db->FetchRow()) {
			$sks_total = $r2[1];
			$ipk_mhs = $r2[2];
		}

	$isi .= '
	<tr>
		<td colspan="6">
		<b>IPK : '.$ipk_mhs.' <br/>
		<b>IPS : '.$ips_mhs.' <br/>
		<b>MAX SKS : '.$sks_maks.' <br/>
		TERAMBIL : '.$sks_terambil.' <br> 
		</td>
	</tr>

	<tr>
		<td colspan="6" align="center">
			* Mata kuliah yang bisa di cetak hanya yang sudah di setujui oleh  dosen wali<br />
			<input type=button name="cetak5" value="Cetak" onclick="window.open(\'proses/_akademik-krs_cetak.php\',\'baru2\');">
		</td>
	</tr>
	</table>
	';
	echo $isi;

}

?>