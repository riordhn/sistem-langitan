<?php

class process
{

    public $db;
    private $id_pengguna;
    private $id_pt;

    function __construct($db, $id_pengguna, $id_pt)
    {
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
        $this->id_pt = $id_pt;
    }

    function GetSemesterAktif()
    {
        return $this->db->QuerySingle("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'");
    }

    function GetSemesterAktifEvaluasi()
    {
        return $this->db->QuerySingle("SELECT ID_SEMESTER FROM EVALUASI_SEMESTER_ISI WHERE STATUS_AKTIF='1' AND ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'");
    }

    function GetSemesterSebelumSemester($id_semester)
    {
        $this->db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER='$id_semester'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil' AND ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'");
            $semester_kemarin = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1) AND ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'");
            $semester_kemarin = $this->db->FetchAssoc();
        }
        return $semester_kemarin['ID_SEMESTER'];
    }

    function GetSemesterSebelum()
    {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil' AND ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'");
            $semester_kemarin = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1) AND ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'");
            $semester_kemarin = $this->db->FetchAssoc();
        }
        return $semester_kemarin['ID_SEMESTER'];
    }
    function GetSemesterSetelah()
    {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Ganjil' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'+1) AND ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'");
            $semester_setelah = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND ID_PERGURUAN_TINGGI='" . getenv('ID_PT') . "'");
            $semester_setelah = $this->db->FetchAssoc();
        }
        return $semester_setelah;
    }

    function CekPembayaran($id_mhs, $semester, $is_end_semester = true)
    {
        $ikm_terbayar = false;
        $her_terbayar = false;
        $spp_terbayar = false;

        $query_detail_mhs = "
        SELECT M.NIM_MHS,M.THN_ANGKATAN_MHS,M.ID_PROGRAM_STUDI
        FROM MAHASISWA M
        WHERE M.ID_MHS='{$id_mhs}'";
        $this->db->Query($query_detail_mhs);
        $detail_mhs = $this->db->FetchAssoc();

        // JIKA ANGKATAN BARU BYPASS
        if ($detail_mhs['THN_ANGKATAN_MHS'] == date('Y')) {
            return true;
        }

        $query_non_spp = "
        SELECT T.ID_TAGIHAN, DB.ID_BIAYA, CASE WHEN T.BESAR_BIAYA = SUM(PEM.BESAR_PEMBAYARAN) THEN 1 ELSE 0 END TERBAYAR
        FROM TAGIHAN_MHS TM
        LEFT JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS = TM.ID_TAGIHAN_MHS
        LEFT JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = T.ID_DETAIL_BIAYA
        LEFT JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN = T.ID_TAGIHAN
        WHERE DB.ID_BIAYA IN (187, 188) AND TM.ID_MHS = '{$id_mhs}' AND TM.ID_SEMESTER = '{$semester}'
        GROUP BY T.ID_TAGIHAN, DB.ID_BIAYA, T.BESAR_BIAYA";
        $this->db->Query($query_non_spp);

        while ($pembayaran_non_spp = $this->db->FetchAssoc()) {
            if ($pembayaran_non_spp['ID_BIAYA'] == 187 && $pembayaran_non_spp['TERBAYAR'] == 1) {
                $her_terbayar = true;
            }
            if ($pembayaran_non_spp['ID_BIAYA'] == 188 && $pembayaran_non_spp['TERBAYAR'] == 1) {
                $ikm_terbayar = true;
            }
        }

        $id_semester_kemarin = $this->GetSemesterSebelum();

        // PEMBAYARAN SPP
        $query_spp = "
        SELECT T.ID_TAGIHAN, DB.ID_BIAYA, T.BESAR_BIAYA, SUM(PEM.BESAR_PEMBAYARAN) BESAR_PEMBAYARAN
        FROM TAGIHAN_MHS TM
        LEFT JOIN TAGIHAN T ON T.ID_TAGIHAN_MHS = TM.ID_TAGIHAN_MHS
        LEFT JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = T.ID_DETAIL_BIAYA
        LEFT JOIN PEMBAYARAN PEM ON PEM.ID_TAGIHAN = T.ID_TAGIHAN
        WHERE DB.ID_BIAYA IN (185) AND TM.ID_MHS = '{$id_mhs}' AND TM.ID_SEMESTER = '{$id_semester_kemarin}'
        GROUP BY T.ID_TAGIHAN, DB.ID_BIAYA, T.BESAR_BIAYA";
        $this->db->Query($query_spp);

        while ($pembayaran_spp = $this->db->FetchAssoc()) {
            if ($pembayaran_spp['BESAR_BIAYA'] > 0) {
                if ($pembayaran_spp['BESAR_PEMBAYARAN'] > 0) {
                    $spp_terbayar = true;
                }
            } elseif ($pembayaran_spp['BESAR_BIAYA'] == 0) {
                $spp_terbayar = true;
            }
        }

        // Jika pengecekan KHS
        if ($is_end_semester) {
            if ($spp_terbayar) {
                return true;
            }
        } else { // pengecekan KRS
            if ($spp_terbayar && $her_terbayar && $ikm_terbayar) {
                return true;
            }
        }

        return false;
    }

    function GetPeriodeWisudaAktif()
    {
        return $this->db->QuerySingle("
                SELECT ID_TARIF_WISUDA FROM TARIF_WISUDA 
                WHERE ID_TARIF_WISUDA IN (
                  SELECT ID_TARIF_WISUDA 
                  FROM PERIODE_WISUDA 
                  WHERE STATUS_AKTIF=1
                )
            ");
    }

    function LoadKelasMahasiswa($semester)
    {
        $fakultas = $this->db->QuerySingle("SELECT ID_FAKULTAS FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM MAHASISWA WHERE ID_PENGGUNA='{$this->id_pengguna}')");
        if ($fakultas == 1) {
            $query = "
                SELECT 
                    KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH,K2.NAMA_KELAS,MK.KD_MATA_KULIAH,P.NM_PENGGUNA,MK.STATUS_PRAKTIKUM,D.ID_DOSEN,
                    (SELECT COUNT(ID_EVAL_HASIL) FROM EVALUASI_HASIL WHERE ID_MHS=M.ID_MHS AND ID_SEMESTER='{$semester}' AND ID_KELAS_MK=PMK.ID_KELAS_MK AND ID_DOSEN=D.ID_DOSEN) STATUS_ISI
                FROM PENGAMBILAN_MK PMK
                JOIN MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
                JOIN PENGAMPU_MK PENGMK ON PMK.ID_KELAS_MK=PENGMK.ID_KELAS_MK
                JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
                JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
                JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
                LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
                JOIN SEMESTER S ON PMK.ID_SEMESTER = S.ID_SEMESTER
                JOIN DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
                JOIN PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
                WHERE M.ID_PENGGUNA='{$this->id_pengguna}' AND S.ID_SEMESTER='{$semester}'
                ORDER BY NM_MATA_KULIAH,K2.NAMA_KELAS
                ";
        } else {
            $query = "
                SELECT 
                    KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH, K2.NAMA_KELAS,MK.KD_MATA_KULIAH,P.NM_PENGGUNA,0 STATUS_PRAKTIKUM,KUMK.STATUS_MKTA,D.ID_DOSEN,
                    (SELECT COUNT(ID_EVAL_HASIL) FROM EVALUASI_HASIL WHERE ID_MHS=M.ID_MHS AND ID_SEMESTER='{$semester}' AND ID_KELAS_MK=PMK.ID_KELAS_MK AND ID_DOSEN=D.ID_DOSEN) STATUS_ISI
                FROM PENGAMBILAN_MK PMK
                JOIN MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
                JOIN PENGAMPU_MK PENGMK ON PMK.ID_KELAS_MK=PENGMK.ID_KELAS_MK
                JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
                JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
                JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
                LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
                JOIN SEMESTER S ON PMK.ID_SEMESTER = S.ID_SEMESTER
                JOIN DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
                JOIN PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
                WHERE M.ID_PENGGUNA='{$this->id_pengguna}' AND S.ID_SEMESTER='{$semester}' AND KUMK.STATUS_MKTA IN (0,5,6)
                ORDER BY NM_MATA_KULIAH,K2.NAMA_KELAS";
        }
        return $this->db->QueryToArray($query);
    }

    function LoadEvaluasiAspek($id_instrumen)
    {
        $data_instrumen = array();
        $data_kelompok_aspek = $this->db->QueryToArray("SELECT * FROM EVALUASI_KELOMPOK_ASPEK WHERE ID_EVAL_INSTRUMEN='{$id_instrumen}' ORDER BY ID_EVAL_KELOMPOK_ASPEK");
        $index_kelompok = 0;
        foreach ($data_kelompok_aspek as $dk) {
            $data_aspek = $this->db->QueryToArray("SELECT * FROM EVALUASI_ASPEK WHERE ID_EVAL_KELOMPOK_ASPEK='{$dk['ID_EVAL_KELOMPOK_ASPEK']}' AND STATUS_AKTIF=0 ORDER BY URUTAN,ID_EVAL_ASPEK");
            array_push($data_instrumen, array_merge($dk, array('DATA_ASPEK' => $data_aspek)));
            $index_aspek = 0;
            foreach ($data_aspek as $da) {
                array_push($data_instrumen[$index_kelompok]['DATA_ASPEK'][$index_aspek], $this->db->QueryToArray("SELECT * FROM EVALUASI_NILAI WHERE ID_KELOMPOK_NILAI='{$da['ID_KELOMPOK_NILAI']}' ORDER BY URUTAN"));
                $index_aspek++;
            }
            $index_kelompok++;
        }
        return $data_instrumen;
    }

    function CekPengisianEvaluasiKuliah($instrumen, $kelas, $id_dosen, $id_mhs, $semester)
    {
        //echo "SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN='{$instrumen}' AND ID_KELAS_MK='{$kelas}' AND ID_DOSEN='{$id_dosen}' AND ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester}'";
        return $this->db->QuerySingle("SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN IN (1,2) AND ID_KELAS_MK='{$kelas}' AND ID_DOSEN='{$id_dosen}' AND ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester}'");
    }

    function CekPengisianSemuaEvaluasiKuliah($id_mhs, $semester, $total_mk)
    {
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM
            (
                SELECT ID_KELAS_MK,ID_DOSEN,ID_MHS,ID_SEMESTER FROM EVALUASI_HASIL 
                WHERE ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester}' AND (ID_EVAL_INSTRUMEN=1 OR ID_EVAL_INSTRUMEN=2)
                GROUP BY ID_KELAS_MK,ID_DOSEN,ID_MHS,ID_SEMESTER
            )");
        return ($jumlah_pengisian >= $total_mk && $total_mk != 0) || (($jumlah_pengisian == 0 || $jumlah_pengisian >= 0) && $total_mk == 0);
    }

    function CekPengisianEvaluasiWali($id_mhs, $id_dosen, $semester)
    {
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=3 AND ID_MHS='{$id_mhs}' AND ID_DOSEN='{$id_dosen}' AND ID_SEMESTER='{$semester}'
        ");
        return ($jumlah_pengisian > 0);
    }

    function CekPengisianEvaluasiAdmFak($id_mhs, $semester)
    {
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=6 AND ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester}'
        ");
        return ($jumlah_pengisian > 0);
    }

    function CekPengisianEvaluasiMaba($id_mhs)
    {
        //echo "SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='{$id_mhs}'";
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='{$id_mhs}'
        ");
        return ($jumlah_pengisian > 0);
    }

    function CekPengisianEvaluasiWisuda($id_mhs)
    {
        //echo "SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='{$id_mhs}'";
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=5 AND ID_MHS='{$id_mhs}'
        ");
        return ($jumlah_pengisian > 0);
    }

    function CekPengisianEvaluasiBimbinganSkripsi($id_mhs)
    {
        //echo "SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='{$id_mhs}'";
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=7 AND ID_MHS='{$id_mhs}'
        ");
        return ($jumlah_pengisian > 0);
    }

    public function CekEvaluasi($id_mhs)
    {
        /*** PROSES CEK EVALUASI (NAMBI) ***/
        $status_isi_evaluasi = 1;
        $message_evaluasi = '';
        $semester_isi = $this->GetSemesterAktifEvaluasi();
        $this->db->Query("
                SELECT * FROM SEMESTER WHERE ID_SEMESTER='{$semester_isi}'
            ");
        $semester_isi_row = $this->db->FetchAssoc();
        // Jika perguruan tinggi umaha (id=1)
        if ($this->id_pt == 1) {
            // Jika akademik diatas 2019 cek evaluasi keatas
            if (intval(($semester_isi_row['THN_AKADEMIK_SEMESTER']) >= 2018 && $semester_isi_row['NM_SEMESTER'] == 'Genap') || ($semester_isi_row['THN_AKADEMIK_SEMESTER'] >= 2019)) {
                // Load Matakuliah Mahasiswa
                $data_mata_kuliah = $this->LoadKelasMahasiswa($semester_isi);
                // Cek Pengisian Evaluasi perkuliahan
                if (!$this->CekPengisianSemuaEvaluasiKuliah($id_mhs, $semester_isi, count($data_mata_kuliah))) {
                    $status_isi_evaluasi = 0;
                    $link = "Klik <a style='text-decoration:none;font-weight:bold;padding:1px 5px;color:orange' href='/modul/mhs/#ev!evkul.php' class='ui-widget-content ui-state-highlight ui-corner-all disable-ajax'>disini</a>";
                    $message_evaluasi .= alert_info("Anda belum mengisi Evaluasi Perkuliahan secara benar/lengkap..." . $link);
                }
                //Data Dosen Wali
                $this->db->Query("
                SELECT D.ID_DOSEN,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI
                FROM DOSEN_WALI DW
                JOIN DOSEN D ON DW.ID_DOSEN=D.ID_DOSEN
                JOIN PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
                WHERE DW.ID_MHS=$id_mhs AND DW.STATUS_DOSEN_WALI=1
                ");
                $data_dosen_wali = $this->db->FetchAssoc();
                // Cek Pengisian Evaluasi Perwalian
                if (!$this->CekPengisianEvaluasiWali($id_mhs, $data_dosen_wali['ID_DOSEN'], $semester_isi)) {
                    $status_isi_evaluasi = 0;
                    $link = "Klik <a style='text-decoration:none;font-weight:bold;padding:1px 5px;color:orange' href='/modul/mhs/#ev!evwali.php' class='ui-widget-content ui-state-highlight ui-corner-all disable-ajax'>disini</a>";
                    $message_evaluasi .= alert_info("Anda belum mengisi Evaluasi Perwalian secara benar/lengkap..." . $link);
                }
                // Evaluasi bimbingan skripsi
                $id_jenjang_mhs = $this->db->QuerySingle("SELECT ID_JENJANG FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM MAHASISWA WHERE ID_MHS='{$id_mhs}'");
                if ($id_jenjang_mhs == 1 || $id_jenjang_mhs == 4) {
                    $batas_semester_skripsi = 8;
                } else {
                    $batas_semester_skripsi = 6;
                }
                $this->db->Query("
                select count(distinct(id_semester)) semester from pengambilan_mk where id_mhs='{$id_mhs}'
                ");
                $jumlah_khs = $this->db->FetchAssoc();
                // Untuk semester 7 keatas
                if ($jumlah_khs['SEMESTER'] >= $batas_semester_skripsi) {
                    if (!$this->CekPengisianEvaluasiBimbinganSkripsi($id_mhs)) {
                        $status_isi_evaluasi = 0;
                        $link = "Klik <a style='text-decoration:none;font-weight:bold;padding:1px 5px;color:orange' href='/modul/mhs/#ev!evskripsi.php' class='ui-widget-content ui-state-highlight ui-corner-all disable-ajax'>disini</a>";
                        $message_evaluasi .= alert_info("Anda belum mengisi Evaluasi Bimbingan skripsi secara benar/lengkap..." . $link);
                    }
                }

                // Evaluasi administrasi fakultas
                if (!$this->CekPengisianEvaluasiAdmFak($id_mhs, $semester_isi)) {
                    $status_isi_evaluasi = 0;
                    $link = "Klik <a style='text-decoration:none;font-weight:bold;padding:1px 5px;color:orange' href='/modul/mhs/#ev!evaf.php' class='ui-widget-content ui-state-highlight ui-corner-all disable-ajax'>disini</a>";
                    $message_evaluasi .= alert_info("Anda belum mengisi Evaluasi Administrasi Fakultas secara benar/lengkap..." . $link);
                }
            }
        }
        return [
            'status' => $status_isi_evaluasi,
            'message' => $message_evaluasi
        ];
    }

    public function CekPengisianBiodata($id_mhs)
    {
        $message_biodata = "";

        $id_jenjang = '';
        $kueri = $this->db->Query("select b.id_jenjang from aucc.mahasiswa a, aucc.program_studi b where a.id_program_studi=b.id_program_studi and a.id_pengguna='" . $this->id_pengguna . "' ");
        $r = $this->db->FetchAssoc();
        while ($r = $this->db->FetchRow()) {
            $id_jenjang = $r[0];
        }

        // cek data di biodata
        $mhs_nama = '';
        $mhs_kelamin = '';
        $mhs_agama = '';
        $mhs_lahir_tgl = '';
        $mhs_fb = '';
        $mhs_tw = '';
        $kueri = "select nm_pengguna, kelamin_pengguna, id_agama, to_char(tgl_lahir_pengguna,'DD-MM-YYYY'), email_pengguna, email_alternate, id_fb, id_twitter from pengguna where id_pengguna='" . $this->id_pengguna . "'";
        $result = $this->db->Query($kueri) or die("salah kueri 10 : ");
        while ($r = $this->db->FetchArray()) {
            $mhs_nama = $r[0];
            $mhs_kelamin = $r[1];
            $mhs_agama = $r[2];
            $mhs_lahir_tgl = $r[3];
            $mhs_fb = $r[6];
            $mhs_tw = $r[7];
        }
        if (strlen(trim($mhs_kelamin)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data jenis kelamin</li>');
        }
        if (strlen(trim($mhs_agama)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data agama</li>');
        }
        if (strlen(trim($mhs_lahir_tgl)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data tanggal lahir</li>');
        }
        /*if(strlen(trim($mhs_fb))==0) { $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data FB</li>'); }
		if(strlen(trim($mhs_tw))==0) { $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data Twitter</li>'); }*/


        $mhs_lahir_prop = "";
        $mhs_lahir_kota = "";
        $mhs_alamat = "";
        $mhs_hp = "";
        $mhs_thn_lulus = "";
        $mhs_nem_nilai = "";
        $mhs_nem_pelajaran = "";
        $mhs_cita = "";
        $ayah_nama = "";
        $ayah_dikakhir = "";
        $ayah_kerja = "";
        $ayah_alamat = "";
        $ibu_nama = "";
        $ibu_dikakhir = "";
        $ibu_kerja = "";
        $ibu_alamat = "";
        $penghasilan = "";
        $mhs_prodi2 = "";
        $mhs_alamat_prop = "";
        $mhs_alamat_kota = "";
        $mhs_alamat_prop_ayah = "";
        $mhs_alamat_kota_ayah = "";
        $mhs_alamat_prop_ibu = "";
        $mhs_alamat_kota_ibu = "";
        $mhs_alamat_asli = "";
        $mhs_alamat_prop_asli = "";
        $mhs_alamat_kota_asli = "";
        $mhs_kebangsaan = "";
        $telp_ayah = "";
        $telp_ibu = "";
        $tgl_lahir_ayah = "";
        $tgl_lahir_ibu = "";
        $disabilitas_ayah = "";
        $disabilitas_ibu = "";
        $penghasilan_ayah = "";
        $penghasilan_ibu = "";

        $kueri = "select nim_mhs, lahir_prop_mhs, lahir_kota_mhs, alamat_mhs, mobile_mhs, id_sekolah_asal_mhs, thn_lulus_mhs, nilai_skhun_mhs, nem_pelajaran_mhs, cita_cita_mhs, nm_ayah_mhs, pendidikan_ayah_mhs, pekerjaan_ayah_mhs, alamat_ayah_mhs, nm_ibu_mhs, pendidikan_ibu_mhs, pekerjaan_ibu_mhs, alamat_ibu_mhs, penghasilan_ortu_mhs, id_program_studi, ASAL_KOTA_MHS, ASAL_PROV_MHS, 
		ALAMAT_AYAH_MHS_PROV,ALAMAT_AYAH_MHS_KOTA, ALAMAT_IBU_MHS_PROV,ALAMAT_IBU_MHS_KOTA, 
		ALAMAT_ASAL_MHS,ALAMAT_ASAL_MHS_PROV,ALAMAT_ASAL_MHS_KOTA, ID_KEBANGSAAN, STATUS_MHS_ASING,
		TELP_AYAH_MHS, TELP_IBU_MHS, TGL_LAHIR_AYAH_MHS, TGL_LAHIR_IBU_MHS, ID_DISABILITAS_AYAH_MHS, ID_DISABILITAS_IBU_MHS, PENGHASILAN_AYAH_MHS, PENGHASILAN_IBU_MHS,
		id_c_mhs, id_mhs
		from mahasiswa where id_pengguna='" . $this->id_pengguna . "'";
        $result = $this->db->Query($kueri) or die("salah kueri 42 : ");
        while ($r = $this->db->FetchRow()) {
            $mhs_lahir_prop = $r[1];
            $mhs_lahir_kota = $r[2];
            $mhs_alamat = $r[3];
            $mhs_hp = $r[4];
            $mhs_sma = $r[5];
            $mhs_thn_lulus = $r[6];
            $mhs_nem_nilai = $r[7];
            $mhs_nem_pelajaran = $r[8];
            $mhs_cita = $r[9];
            $ayah_nama = $r[10];
            $ayah_dikakhir = $r[11];
            $ayah_kerja = $r[12];
            $ayah_alamat = $r[13];
            $ibu_nama = $r[14];
            $ibu_dikakhir = $r[15];
            $ibu_kerja = $r[16];
            $ibu_alamat = $r[17];
            $penghasilan = $r[18];
            $mhs_prodi2 = $r[19];
            $mhs_alamat_kota = $r[20];
            $mhs_alamat_prop = $r[21];
            $mhs_alamat_prop_ayah = $r[22];
            $mhs_alamat_kota_ayah = $r[23];
            $mhs_alamat_prop_ibu = $r[24];
            $mhs_alamat_kota_ibu = $r[25];
            $mhs_alamat_asli = $r[26];
            $mhs_alamat_prop_asli = $r[27];
            $mhs_alamat_kota_asli = $r[28];
            $mhs_kebangsaan = $r[29];
            $telp_ayah = $r[31];
            $telp_ibu = $r[32];
            $tgl_lahir_ayah = $r[33];
            $tgl_lahir_ibu = $r[34];
            $disabilitas_ayah = $r[35];
            $disabilitas_ibu = $r[36];
            $penghasilan_ayah = $r[37];
            $penghasilan_ibu = $r[38];

            $id_mhs = $r[40];
        }

        if (strlen(trim($mhs_lahir_prop)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data propinsi tempat lahir</li>');
        }
        if (strlen(trim($mhs_lahir_kota)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data kota tempat lahir</li>');
        }
        if (strlen(trim($mhs_alamat)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data alamat domisili</li>');
        }
        if (strlen(trim($mhs_hp)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data no HP</li>');
        }
        if ($id_jenjang == '1' or $id_jenjang == '5') {
            if (strlen(trim($mhs_sma)) == 0) {
                $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data sekolah sma</li>');
            }
            if (strlen(trim($mhs_thn_lulus)) == 0) {
                $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data tahun lulus sma</li>');
            }
            if (strlen(trim($mhs_nem_nilai)) == 0) {
                $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data nilai NEM</li>');
            }
            if (strlen(trim($mhs_nem_pelajaran)) == 0) {
                $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data jumlah pelajaran sma</li>');
            }
            if (strlen(trim($mhs_cita)) == 0) {
                $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data cita-cita</li>');
            }
        }
        if (strlen(trim($ayah_nama)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data nama ayah</li>');
        }
        if (strlen(trim($ayah_dikakhir)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data pendidikan ayah</li>');
        }
        if (strlen(trim($ayah_kerja)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data pekerjaan ayah</li>');
        }
        if (strlen(trim($ayah_alamat)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data alamat tinggal ayah / telp</li>');
        }
        if (strlen(trim($mhs_alamat_prop_ayah)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data propinsi alamat tinggal ayah</li>');
        }
        if (strlen(trim($mhs_alamat_kota_ayah)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data kota alamat tinggal ayah</li>');
        }
        if (strlen(trim($ibu_nama)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data nama ibu</li>');
        }
        if (strlen(trim($ibu_dikakhir)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data pendidikan ibu</li>');
        }
        if (strlen(trim($ibu_kerja)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data pekerjaan ibu</li>');
        }
        if (strlen(trim($ibu_alamat)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data alamat tinggal ibu / telp</li>');
        }
        if (strlen(trim($mhs_alamat_prop_ibu)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data propinsi alamat tinggal ibu</li>');
        }
        if (strlen(trim($mhs_alamat_kota_ibu)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data kota alamat tinggal ibu</li>');
        }

        if (strlen(trim($telp_ayah)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data telp ayah</li>');
        }
        if (strlen(trim($telp_ibu)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data telp ibu</li>');
        }
        if (strlen(trim($penghasilan_ayah)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data penghasilan ayah</li>');
        }
        if (strlen(trim($penghasilan_ibu)) == 0) {
            $message_biodata .= alert_info('<li style="list-style: none; font-size: 0.8em;"> Belum mengisi data penghasilan ibu</li>');
        }
        return [
            'status' => $message_biodata == '' ? 1 : 0,
            'message' => $message_biodata
        ];
    }

    function CekStatusKeuanganMhs($id_mhs, $id_semester_aktif)
    {
        $status = true;
        $message = '';
        $tagihan = $this->db->QuerySingle("
        SELECT COUNT(*) FROM TAGIHAN WHERE ID_TAGIHAN_MHS IN (SELECT ID_TAGIHAN_MHS FROM TAGIHAN_MHS WHERE ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$id_semester_aktif}')
        ");

        $pembayaran = $this->db->QuerySingle("
        SELECT
            COUNT(*) 
        FROM
            PEMBAYARAN 
        WHERE
            ID_TAGIHAN IN (
        SELECT
            TAG.ID_TAGIHAN 
        FROM
            TAGIHAN TAG
            JOIN TAGIHAN_MHS TM ON TM.ID_TAGIHAN_MHS = TAG.ID_TAGIHAN_MHS 
        WHERE
            TM.ID_MHS = '{$id_mhs}' 
            AND TM.ID_SEMESTER = '{$id_semester_aktif}')
        ");
        if ($tagihan == 0) {
            $status = false;
            $message = 'Maaf harap hubungi bagian keuangan. Terima Kasih';
        }
        if ($tagihan > 0 && $pembayaran == 0) {
            $status = false;
            $message = 'Anda memiliki tagihan, harap melakukan menghubungi bagian keuangan. Terima Kasih';
        }
        return [
            'status' => $status,
            'message' => $message
        ];
    }
}
