<?php

class Akademik
{

	public $db;
	private $id_pengguna;

	function __construct($db, $id_pengguna)
	{
		$this->db = $db;
		$this->id_pengguna = $id_pengguna;
	}

	function GetSemesterMhs()
	{
		return $this->db->QueryToArray(
			"select a.id_semester,a.tahun_ajaran,a.nm_semester,a.group_semester
			from semester a, pengambilan_mk b, mahasiswa c
			where a.id_semester=b.id_semester and b.id_mhs=c.id_mhs and c.id_pengguna='" . $this->id_pengguna . "'
			group by a.id_semester,a.tahun_ajaran,a.nm_semester,a.group_semester
			order by a.tahun_ajaran desc, a.nm_semester desc");
	}

	private function getSemesterAktif()
	{
		$this->db->Query("select id_semester,thn_akademik_semester,nm_semester from semester where STATUS_AKTIF_SEMESTER='True'");
		return $this->db->FetchAssoc();
	}

	function getIPS($id_mhs, $id_semester_lalu)
	{

		$this->db->Query(
			"select sum(kredit_semester) as sks_sem, sum((bobot*kredit_semester)) as bobot_total, 
			case when sum(kredit_semester)=0 then 0 else round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips
			from 
			(
			select kredit_semester,bobot
			from(
			select pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,mata_kuliah.nm_mata_kuliah,nama_kelas,tipe_semester,
			case when kurikulum_mk.status_mkta='1' 
			and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas in (7,12) then 0 else kurikulum_mk.kredit_semester end as kredit_semester, 
			case when kurikulum_mk.status_mkta='1' 
			and (nilai_huruf='E' or nilai_huruf is null) and id_fakultas=7 then 'T' 
			when nilai_huruf is null then 'E' else nilai_huruf end as nilai_huruf,
			case when standar_nilai.nilai_standar_nilai is null then 0 else standar_nilai.nilai_standar_nilai end as bobot,
			coalesce(standar_nilai.nilai_standar_nilai*kurikulum_mk.kredit_semester,0) as bobot_total,
			nilai_angka,a.besar_nilai_mk as UTS,b.besar_nilai_mk as UAS,
			row_number() over(partition by pengambilan_mk.id_mhs,MATA_KULIAH.kd_mata_kuliah,pengambilan_mk.id_kelas_mk order by nilai_huruf) rangking
			from PENGAMBILAN_MK
			left join kelas_mk on PENGAMBILAN_MK.id_kelas_mk=KELAS_MK.id_kelas_mk
			left join NAMA_KELAS on KELAS_MK.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk
			left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
			left join semester on pengambilan_mk.id_semester=semester.id_semester
			left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
			left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
			left join standar_nilai on pengambilan_mk.nilai_huruf=standar_nilai.nm_standar_nilai
			left join nilai_mk a on pengambilan_mk.id_pengambilan_mk=a.id_pengambilan_mk and a.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UTS')
			left join nilai_mk b on pengambilan_mk.id_pengambilan_mk=b.id_pengambilan_mk and b.id_komponen_mk in (select id_komponen_mk from komponen_mk where nm_komponen_mk='UAS')
			where  group_semester||thn_akademik_semester in
			(select group_semester||thn_akademik_semester from semester where id_semester='" . $id_semester_lalu . "')
			and tipe_semester in ('UP','REG','RD')
			and status_apv_pengambilan_mk=1 and PENGAMBILAN_MK.status_hapus=0 and PENGAMBILAN_MK.status_pengambilan_mk !=0 and flagnilai='1'
			and mahasiswa.id_mhs='" . $id_mhs . "') 
			where rangking=1
			)");

		return $this->db->FetchAssoc();
	}

	function getIPK($id_mhs)
	{

		$this->db->Query("select a.id_mhs,sum(a.kredit_semester) skstotal,
		round(sum(a.kredit_semester*(case a.nilai_huruf 
		when 'A' then 4 
		when 'AB' then 3.5 
		when 'B' then 3
		when 'BC' then 2.5
		when 'C' then 2
		when 'D' then 1
		end))/sum(a.kredit_semester),2) IPK
		from
		(
		select a.id_mhs,e.nm_mata_kuliah,nvl(e.kredit_semester,d.kredit_semester) kredit_semester,a.nilai_huruf from (
		select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
		from pengambilan_mk a 
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null and a.flagnilai=1
		) a
		left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
		left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
		where rangking=1 and id_mhs='{$id_mhs}'
		) a
		left join mahasiswa b on a.id_mhs=b.id_mhs
		left join program_studi f on b.id_program_studi=f.id_program_studi
		where a.id_mhs='{$id_mhs}'
		group by a.id_mhs order by a.id_mhs");

		return $this->db->FetchAssoc();
	}

}
