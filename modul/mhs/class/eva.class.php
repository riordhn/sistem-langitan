<?php

class eva
{

    public $db;
    private $id_pengguna;

    function __construct($db, $id_pengguna)
    {
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
    }

    public function CekEvaluasiSemester($id_semester,$id_mhs, $id_perguruan_tinggi)
    {
        /*** PROSES CEK EVALUASI (NAMBI) ***/
        $status_isi_evaluasi = 1;
        $message_evaluasi = '';
        $semester_isi = $id_semester;
        $this->db->Query("
                SELECT * FROM SEMESTER WHERE ID_SEMESTER='{$id_semester}'
            ");
        $semester_isi_row = $this->db->FetchAssoc();
        // Jika perguruan tinggi umaha (id=1)
        if ($id_perguruan_tinggi == 1) {
            // Jika akademik diatas 2019 cek evaluasi keatas 
            // Dan  bukan TIPE SP *TAMBAHAN FIKRIE 18-08-2020
            if (($semester_isi_row['TIPE_SEMESTER'] != 'SP') && (intval(($semester_isi_row['THN_AKADEMIK_SEMESTER']) >= 2018 && $semester_isi_row['NM_SEMESTER'] == 'Genap') || ($semester_isi_row['THN_AKADEMIK_SEMESTER'] >= 2019))) {

                // Load Matakuliah Mahasiswa
                $data_mata_kuliah = $this->LoadKelasMahasiswa($semester_isi);
                // Cek Pengisian Evaluasi perkuliahan
                if (!$this->CekPengisianSemuaEvaluasiKuliah($id_mhs, $semester_isi, count($data_mata_kuliah))) {
                    $status_isi_evaluasi = 0;
                    $link = "Klik <a style='text-decoration:none;font-weight:bold;padding:1px 5px;color:orange' href='/modul/mhs/#ev!evkul.php' class='ui-widget-content ui-state-highlight ui-corner-all disable-ajax'>disini</a>";
                    $message_evaluasi .= alert_info("Anda belum mengisi Evaluasi Perkuliahan secara benar/lengkap..." . $link);
                }
                //Data Dosen Wali
                $this->db->Query("
                SELECT D.ID_DOSEN,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI
                FROM DOSEN_WALI DW
                JOIN DOSEN D ON DW.ID_DOSEN=D.ID_DOSEN
                JOIN PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
                WHERE DW.ID_MHS=$id_mhs AND DW.STATUS_DOSEN_WALI=1
                ");
                $data_dosen_wali = $this->db->FetchAssoc();
                // Cek Pengisian Evaluasi Perwalian
                if (!$this->CekPengisianEvaluasiWali($id_mhs, $data_dosen_wali['ID_DOSEN'], $semester_isi)) {
                    $status_isi_evaluasi = 0;
                    $link = "Klik <a style='text-decoration:none;font-weight:bold;padding:1px 5px;color:orange' href='/modul/mhs/#ev!evwali.php' class='ui-widget-content ui-state-highlight ui-corner-all disable-ajax'>disini</a>";
                    $message_evaluasi .= alert_info("Anda belum mengisi Evaluasi Perwalian secara benar/lengkap..." . $link);
                }
                // Evaluasi bimbingan skripsi
                $id_jenjang_mhs = $this->db->QuerySingle("SELECT ID_JENJANG FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM MAHASISWA WHERE ID_MHS='{$id_mhs}'");
                if ($id_jenjang_mhs == 1 || $id_jenjang_mhs == 4) {
                    $batas_semester_skripsi = 8;
                } else {
                    $batas_semester_skripsi = 6;
                }
                $this->db->Query("
                select count(distinct(id_semester)) semester from pengambilan_mk where id_mhs='{$id_mhs}'
                ");
                $jumlah_khs = $this->db->FetchAssoc();
                // Untuk semester 7 keatas
                if ($jumlah_khs['SEMESTER'] >= $batas_semester_skripsi) {
                    if (!$this->CekPengisianEvaluasiBimbinganSkripsi($id_mhs)) {
                        $status_isi_evaluasi = 0;
                        $link = "Klik <a style='text-decoration:none;font-weight:bold;padding:1px 5px;color:orange' href='/modul/mhs/#ev!evskripsi.php' class='ui-widget-content ui-state-highlight ui-corner-all disable-ajax'>disini</a>";
                        $message_evaluasi .= alert_info("Anda belum mengisi Evaluasi Bimbingan skripsi secara benar/lengkap..." . $link);
                    }
                }

                // Evaluasi administrasi fakultas
                if (!$this->CekPengisianEvaluasiAdmFak($id_mhs, $semester_isi)) {
                    $status_isi_evaluasi = 0;
                    $link = "Klik <a style='text-decoration:none;font-weight:bold;padding:1px 5px;color:orange' href='/modul/mhs/#ev!evaf.php' class='ui-widget-content ui-state-highlight ui-corner-all disable-ajax'>disini</a>";
                    $message_evaluasi .= alert_info("Anda belum mengisi Evaluasi Administrasi Fakultas secara benar/lengkap..." . $link);
                }
            }
        }
        return [
            'status_isi' => $status_isi_evaluasi,
            'message' => $message_evaluasi
        ];
    }

    function GetSemesterAktif()
    {
        return $this->db->QuerySingle("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI = " . getenv('ID_PT'));
    }

    function GetSemesterAktifEvaluasi()
    {
        return $this->db->QuerySingle(
            "SELECT ES.ID_SEMESTER FROM EVALUASI_SEMESTER_ISI ES
            JOIN SEMESTER S ON S.ID_SEMESTER = ES.ID_SEMESTER
            WHERE ES.STATUS_AKTIF='1' AND S.ID_PERGURUAN_TINGGI = " . getenv('ID_PT')
        );
    }


    function Biodata($id_pengguna)
    {

        $this->db->Query("
			SELECT M.id_mhs,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI ,PS.ID_FAKULTAS,
				NM_STATUS_PENGGUNA,STATUS_AKADEMIK_MHS, M.STATUS_CEKAL,M.ID_C_MHS,
				M.id_program_studi,M.nim_mhs,M.THN_ANGKATAN_MHS,PS.status_krs,ps.id_jenjang
				FROM MAHASISWA M
				JOIN PENGGUNA P ON M.ID_PENGGUNA=P.ID_PENGGUNA
				JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
				JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
				WHERE M.ID_PENGGUNA='$id_pengguna'");
        $biodata = $this->db->FetchRow();

        return $biodata;
    }

    function CekBiodataPasca($mhs_nim)
    {
        $this->db->Query("
			SELECT COUNT(*) AS MHS 
					FROM MAHASISWA 
					JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
					WHERE NIM_MHS = '$mhs_nim'
					AND MOBILE_MHS IS NOT NULL AND NM_AYAH_MHS IS NOT NULL
					AND PENDIDIKAN_AYAH_MHS IS NOT NULL AND PEKERJAAN_AYAH_MHS IS NOT NULL AND ALAMAT_AYAH_MHS IS NOT NULL
					AND NM_IBU_MHS IS NOT NULL AND PENDIDIKAN_IBU_MHS IS NOT NULL AND PEKERJAAN_IBU_MHS IS NOT NULL
					AND ALAMAT_IBU_MHS IS NOT NULL AND PENGHASILAN_ORTU_MHS IS NOT NULL
					AND LAHIR_KOTA_MHS IS NOT NULL AND LAHIR_PROP_MHS IS NOT NULL AND ASAL_KOTA_MHS IS NOT NULL
					AND ASAL_PROV_MHS IS NOT NULL 
					AND ALAMAT_AYAH_MHS_KOTA IS NOT NULL AND ALAMAT_AYAH_MHS_PROV IS NOT NULL 
					AND ALAMAT_IBU_MHS_KOTA IS NOT NULL AND ALAMAT_IBU_MHS_PROV IS NOT NULL AND ALAMAT_ASAL_MHS IS NOT NULL
					AND ALAMAT_ASAL_MHS_KOTA IS NOT NULL AND ALAMAT_ASAL_MHS_PROV IS NOT NULL
					AND KELAMIN_PENGGUNA IS NOT NULL AND ID_AGAMA IS NOT NULL AND TGL_LAHIR_PENGGUNA IS NOT NULL");
        $biodata = $this->db->FetchAssoc();

        return $biodata;
    }


    function CekBiodataReguler($mhs_nim)
    {
        $this->db->Query("
			SELECT COUNT(*) AS MHS FROM MAHASISWA 
					JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
					WHERE NIM_MHS = '$mhs_nim'
					AND MOBILE_MHS IS NOT NULL AND ID_SEKOLAH_ASAL_MHS IS NOT NULL AND THN_LULUS_MHS IS NOT NULL
					AND NILAI_SKHUN_MHS IS NOT NULL AND CITA_CITA_MHS IS NOT NULL AND NM_AYAH_MHS IS NOT NULL
					AND PENDIDIKAN_AYAH_MHS IS NOT NULL AND PEKERJAAN_AYAH_MHS IS NOT NULL AND ALAMAT_AYAH_MHS IS NOT NULL
					AND NM_IBU_MHS IS NOT NULL AND PENDIDIKAN_IBU_MHS IS NOT NULL AND PEKERJAAN_IBU_MHS IS NOT NULL
					AND ALAMAT_IBU_MHS IS NOT NULL AND PENGHASILAN_ORTU_MHS IS NOT NULL AND NEM_PELAJARAN_MHS IS NOT NULL
					AND LAHIR_KOTA_MHS IS NOT NULL AND LAHIR_PROP_MHS IS NOT NULL AND ASAL_KOTA_MHS IS NOT NULL
					AND ASAL_PROV_MHS IS NOT NULL 
					AND ALAMAT_AYAH_MHS_KOTA IS NOT NULL AND ALAMAT_AYAH_MHS_PROV IS NOT NULL 
					AND ALAMAT_IBU_MHS_KOTA IS NOT NULL AND ALAMAT_IBU_MHS_PROV IS NOT NULL AND ALAMAT_ASAL_MHS IS NOT NULL
					AND ALAMAT_ASAL_MHS_KOTA IS NOT NULL AND ALAMAT_ASAL_MHS_PROV IS NOT NULL
					AND KELAMIN_PENGGUNA IS NOT NULL AND ID_AGAMA IS NOT NULL AND TGL_LAHIR_PENGGUNA IS NOT NULL");
        return $this->db->FetchAssoc();
    }

    function GetSemesterSebelumSemester($id_semester)
    {
        $this->db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER='$id_semester'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil'");
            $semester_kemarin = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1)");
            $semester_kemarin = $this->db->FetchAssoc();
        }
        return $semester_kemarin['ID_SEMESTER'];
    }

    function GetSemesterSebelum()
    {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil'");
            $semester_kemarin = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1)");
            $semester_kemarin = $this->db->FetchAssoc();
        }
        return $semester_kemarin['ID_SEMESTER'];
    }
    function GetSemesterSetelah()
    {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Ganjil' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'+1)");
            $semester_setelah = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}'");
            $semester_setelah = $this->db->FetchAssoc();
        }
        return $semester_setelah;
    }

    function CekPembayaran($id_mhs, $semester_aktif)
    {
        $tagihan = $this->db->QuerySingle("SELECT COUNT(*) FROM PEMBAYARAN WHERE ID_SEMESTER='{$semester_aktif}' AND ID_MHS='{$id_mhs}' AND ID_STATUS_PEMBAYARAN=2");
        return ($tagihan > 0);
    }

    function GetPeriodeWisudaAktif()
    {
        return $this->db->QuerySingle("
                SELECT ID_TARIF_WISUDA FROM TARIF_WISUDA 
                WHERE ID_TARIF_WISUDA IN (
                  SELECT ID_TARIF_WISUDA 
                  FROM PERIODE_WISUDA 
                  WHERE STATUS_AKTIF=1
                )
            ");
    }

    function LoadKelasMahasiswa($semester)
    {
//        $fakultas = $this->db->QuerySingle("SELECT ID_FAKULTAS FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM MAHASISWA WHERE ID_PENGGUNA='{$this->id_pengguna}')");
//        if ($fakultas == 1) {
//            $query = "
//                SELECT 
//                    KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH,K2.NAMA_KELAS,MK.KD_MATA_KULIAH,P.NM_PENGGUNA,MK.STATUS_PRAKTIKUM,D.ID_DOSEN,
//                    (SELECT COUNT(ID_EVAL_HASIL) FROM EVALUASI_HASIL WHERE ID_MHS=M.ID_MHS AND ID_SEMESTER='{$semester}' AND ID_KELAS_MK=PMK.ID_KELAS_MK AND ID_DOSEN=D.ID_DOSEN) STATUS_ISI
//                FROM PENGAMBILAN_MK PMK
//                JOIN MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
//                JOIN PENGAMPU_MK PENGMK ON PMK.ID_KELAS_MK=PENGMK.ID_KELAS_MK
//                JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
//                JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
//                JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
//                LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
//                JOIN SEMESTER S ON PMK.ID_SEMESTER = S.ID_SEMESTER
//                JOIN DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
//                JOIN PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
//                WHERE M.ID_PENGGUNA='{$this->id_pengguna}' AND S.ID_SEMESTER='{$semester}'
//                ORDER BY NM_MATA_KULIAH,K2.NAMA_KELAS
//                ";
//        } else {
            $query = "
                SELECT 
                    KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH, K2.NAMA_KELAS,MK.KD_MATA_KULIAH,P.NM_PENGGUNA,0 STATUS_PRAKTIKUM,KUMK.STATUS_MKTA,D.ID_DOSEN,
                    (SELECT COUNT(ID_EVAL_HASIL) FROM EVALUASI_HASIL WHERE ID_MHS=M.ID_MHS AND ID_SEMESTER='{$semester}' AND ID_KELAS_MK=PMK.ID_KELAS_MK AND ID_DOSEN=D.ID_DOSEN) STATUS_ISI
                FROM PENGAMBILAN_MK PMK
                JOIN MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
                JOIN PENGAMPU_MK PENGMK ON PMK.ID_KELAS_MK=PENGMK.ID_KELAS_MK
                JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
                JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
                JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
                LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
                JOIN SEMESTER S ON PMK.ID_SEMESTER = S.ID_SEMESTER
                JOIN DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
                JOIN PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
                WHERE M.ID_PENGGUNA='{$this->id_pengguna}' AND S.ID_SEMESTER='{$semester}' AND KUMK.STATUS_MKTA IN (0,5,6)
                ORDER BY NM_MATA_KULIAH,K2.NAMA_KELAS";
//        }
        return $this->db->QueryToArray($query);
    }

    function LoadEvaluasiAspek($id_instrumen)
    {
        $data_instrumen = array();
        $data_kelompok_aspek = $this->db->QueryToArray("SELECT * FROM EVALUASI_KELOMPOK_ASPEK WHERE ID_EVAL_INSTRUMEN='{$id_instrumen}' ORDER BY ID_EVAL_KELOMPOK_ASPEK");
        $index_kelompok = 0;
        foreach ($data_kelompok_aspek as $dk) {
            $data_aspek = $this->db->QueryToArray("SELECT * FROM EVALUASI_ASPEK WHERE ID_EVAL_KELOMPOK_ASPEK='{$dk['ID_EVAL_KELOMPOK_ASPEK']}' AND STATUS_AKTIF=0 ORDER BY URUTAN,ID_EVAL_ASPEK");
            array_push($data_instrumen, array_merge($dk, array('DATA_ASPEK' => $data_aspek)));
            $index_aspek = 0;
            foreach ($data_aspek as $da) {
                array_push($data_instrumen[$index_kelompok]['DATA_ASPEK'][$index_aspek], $this->db->QueryToArray("SELECT * FROM EVALUASI_NILAI WHERE ID_KELOMPOK_NILAI='{$da['ID_KELOMPOK_NILAI']}' ORDER BY URUTAN"));
                $index_aspek++;
            }
            $index_kelompok++;
        }
        return $data_instrumen;
    }

    function SaveEvaluasiKuliah($instrumen, $kel_aspek, $aspek, $id_mhs, $id_dosen, $prodi, $fakultas, $kelas, $semester, $nilai)
    {
        $this->db->Query("
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_MHS,ID_DOSEN,ID_PROGRAM_STUDI,ID_FAKULTAS,ID_KELAS_MK,ID_SEMESTER,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_mhs}','{$id_dosen}','{$prodi}','{$fakultas}','{$kelas}','{$semester}','{$nilai}')");
    }

    function CekPengisianEvaluasiKuliah($instrumen, $kelas, $id_dosen, $id_mhs, $semester)
    {
        //echo "SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN='{$instrumen}' AND ID_KELAS_MK='{$kelas}' AND ID_DOSEN='{$id_dosen}' AND ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester}'";
        return $this->db->QuerySingle("SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN IN (1,2) AND ID_KELAS_MK='{$kelas}' AND ID_DOSEN='{$id_dosen}' AND ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester}'");
    }

    function CekPengisianSemuaEvaluasiKuliah($id_mhs, $semester, $total_mk)
    {
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM
            (
                SELECT ID_KELAS_MK,ID_DOSEN,ID_MHS,ID_SEMESTER FROM EVALUASI_HASIL 
                WHERE ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester}' AND (ID_EVAL_INSTRUMEN=1 OR ID_EVAL_INSTRUMEN=2)
                GROUP BY ID_KELAS_MK,ID_DOSEN,ID_MHS,ID_SEMESTER
            )");
        return ($jumlah_pengisian >= $total_mk && $total_mk != 0) || (($jumlah_pengisian == 0 || $jumlah_pengisian >= 0) && $total_mk == 0);
    }

    function CekPengisianEvaluasiWali($id_mhs, $id_dosen, $semester)
    {
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=3 AND ID_MHS='{$id_mhs}' AND ID_DOSEN='{$id_dosen}' AND ID_SEMESTER='{$semester}'
        ");
        return ($jumlah_pengisian > 0);
    }

    function SaveEvaluasiWali($instrumen, $kel_aspek, $aspek, $id_mhs, $id_dosen, $prodi, $fakultas, $semester, $nilai)
    {
        $this->db->Query("
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_MHS,ID_DOSEN,ID_PROGRAM_STUDI,ID_FAKULTAS,ID_SEMESTER,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_mhs}','{$id_dosen}','{$prodi}','{$fakultas}','{$semester}','{$nilai}')");
    }

    function CekPengisianEvaluasiAdmFak($id_mhs, $semester)
    {
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=6 AND ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester}'
        ");
        return ($jumlah_pengisian > 0);
    }

    function SaveEvaluasiAdmFak($instrumen, $kel_aspek, $aspek, $id_mhs, $prodi, $fakultas, $semester, $nilai)
    {
        $this->db->Query("
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_MHS,ID_PROGRAM_STUDI,ID_FAKULTAS,ID_SEMESTER,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_mhs}','{$prodi}','{$fakultas}','{$semester}','{$nilai}')");
    }

    function CekPengisianEvaluasiMaba($id_mhs)
    {
        //echo "SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='{$id_mhs}'";
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='{$id_mhs}'
        ");
        return ($jumlah_pengisian > 0);
    }

    function SaveEvaluasiMaba($instrumen, $kel_aspek, $aspek, $id_mhs, $prodi, $fakultas, $semester, $nilai)
    {
        $this->db->Query("
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_MHS,ID_PROGRAM_STUDI,ID_FAKULTAS,ID_SEMESTER,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_mhs}','{$prodi}','{$fakultas}','{$semester}','{$nilai}')");
    }

    function CekPengisianEvaluasiWisuda($id_mhs)
    {
        //echo "SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='{$id_mhs}'";
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=5 AND ID_MHS='{$id_mhs}'
        ");
        return ($jumlah_pengisian > 0);
    }

    function SaveEvaluasiWisuda($instrumen, $kel_aspek, $aspek, $id_mhs, $prodi, $fakultas, $periode, $nilai)
    {
        $this->db->Query("
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_MHS,ID_PROGRAM_STUDI,ID_FAKULTAS,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_mhs}','{$prodi}','{$fakultas}','{$nilai}')");
    }

    function CekPengisianEvaluasiBimbinganSkripsi($id_mhs)
    {
        //echo "SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_MHS='{$id_mhs}'";
        $jumlah_pengisian = $this->db->QuerySingle("
            SELECT COUNT(*) FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=7 AND ID_MHS='{$id_mhs}'
        ");
        return ($jumlah_pengisian > 0);
    }

    function SaveEvaluasiBimbinganSkripsi($instrumen, $kel_aspek, $aspek, $id_mhs, $prodi, $fakultas, $nilai)
    {
        $this->db->Query("
            INSERT INTO EVALUASI_HASIL
                (ID_EVAL_INSTRUMEN,ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_ASPEK,ID_MHS,ID_PROGRAM_STUDI,ID_FAKULTAS,NILAI_EVAL)
            VALUES
                ('{$instrumen}','{$kel_aspek}','{$aspek}','{$id_mhs}','{$prodi}','{$fakultas}','{$nilai}')");
    }
}
