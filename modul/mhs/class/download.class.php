<?php

class download {

    public $db;
    public $id_pengguna;

    function __construct($db, $id_pengguna) {
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
    }

    function LoadRequestDownload() {
        return $this->db->QueryToArray("SELECT * FROM AUCC.DOWNLOAD WHERE ID_REQUESTBY='{$this->id_pengguna}'");
    }

    function AddRequestDownload($nama, $url, $jenis) {
        $id_download = $this->db->QuerySingle("SELECT ID_DOWNLOAD FROM DOWNLOAD ORDER BY ID_DOWNLOAD DESC") + 1;
        $this->db->Query("
            INSERT INTO DOWNLOAD
                (ID_DOWNLOAD,ID_REQUESTBY,TGL_REQUEST,NAMA_FILE,DOWNLOAD_URL,JENIS_FILE,STATUS_DOWNLOAD,STATUS_FILE)
            VALUES
                ('{$id_download}','{$this->id_pengguna}',SYSDATE,'{$nama}','{$url}','{$jenis}','0','0')
            ");
    }

    function GetRequestDownload($id_download) {
        $this->db->Query("SELECT * FROM AUCC.DOWNLOAD WHERE ID_DOWNLOAD='{$id_download}'");
        return $this->db->FetchAssoc();
    }

    function UpdateRequestDownload($id_download, $nama, $url, $jenis) {
        $this->db->Query("
            UPDATE DOWNLOAD
                SET
                    TGL_REQUEST=SYSDATE,
                    KETERANGAN_APPROVE='',
                    STATUS_FILE=0,
                    NAMA_FILE='{$nama}',
                    DOWNLOAD_URL='{$url}',
                    JENIS_FILE='{$jenis}'
            WHERE ID_DOWNLOAD='{$id_download}'                    
            ");
    }

    function DeleteRequestDownload($id_download) {
        $this->db->Query("
            DELETE FROM AUCC.DOWNLOAD WHERE ID_DOWNLOAD='{$id_download}'
            ");
    }

}

?>
