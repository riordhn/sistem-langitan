<?php

include 'function-tampil-informasi.php';
include_once 'eva.class.php';

class upload {

    public $db;
    private $id_pengguna;

    function __construct($db, $id_pengguna) {
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
    }

    function CheckStatusWisuda($id_mhs, $jenjang) {
        $eva = new eva($this->db, $this->id_pengguna);
        $nim_mhs = $this->db->QuerySingle("SELECT NIM_MHS FROM MAHASISWA WHERE ID_MHS='{$id_mhs}'");
        $error = '';
        $cek_pengajuan = $this->db->QuerySingle("SELECT COUNT(*) FROM PENGAJUAN_WISUDA WHERE ID_MHS = '{$id_mhs}'");
        if ($cek_pengajuan == 1) {
            $cek_ijazah = $this->db->QuerySingle("SELECT COUNT(*) FROM PENGAJUAN_WISUDA WHERE ID_MHS = '{$id_mhs}' AND NO_IJASAH IS NULL");
            if ($cek_ijazah == 1) {
                $error .=alert_error(" No Ijasah dan Tanggal lulus belum di entry, harap hubungi akademik fakultas<br/>");
            }

            //evaluasi wisudawan
            if (!$eva->CekPengisianEvaluasiWisuda($id_mhs)) {
                $error .= alert_error("Harap Mangisi Evaluasi Wisuda<br/>");
            }

            // biodata
            if ($jenjang == '2' or $jenjang == '3' or $jenjang == '10' or $jenjang == '9') {
                $q_bio = "SELECT COUNT(*) AS MHS FROM MAHASISWA WHERE NIM_MHS = '$nim_mhs'
                        AND MOBILE_MHS IS NOT NULL AND NM_AYAH_MHS IS NOT NULL
                        AND PENDIDIKAN_AYAH_MHS IS NOT NULL AND PEKERJAAN_AYAH_MHS IS NOT NULL AND ALAMAT_AYAH_MHS IS NOT NULL
                        AND NM_IBU_MHS IS NOT NULL AND PENDIDIKAN_IBU_MHS IS NOT NULL AND PEKERJAAN_IBU_MHS IS NOT NULL
                        AND ALAMAT_IBU_MHS IS NOT NULL AND PENGHASILAN_ORTU_MHS IS NOT NULL
                        AND LAHIR_KOTA_MHS IS NOT NULL AND LAHIR_PROP_MHS IS NOT NULL AND ASAL_KOTA_MHS IS NOT NULL
                        AND ASAL_PROV_MHS IS NOT NULL 
                        AND ALAMAT_AYAH_MHS_KOTA IS NOT NULL AND ALAMAT_AYAH_MHS_PROV IS NOT NULL 
                        AND ALAMAT_IBU_MHS_KOTA IS NOT NULL AND ALAMAT_IBU_MHS_PROV IS NOT NULL AND ALAMAT_ASAL_MHS IS NOT NULL
                        AND ALAMAT_ASAL_MHS_KOTA IS NOT NULL AND ALAMAT_ASAL_MHS_PROV IS NOT NULL";
            } else {
                $q_bio = "SELECT COUNT(*) AS MHS FROM MAHASISWA WHERE NIM_MHS = '$nim_mhs'
                        AND MOBILE_MHS IS NOT NULL AND ID_SEKOLAH_ASAL_MHS IS NOT NULL AND THN_LULUS_MHS IS NOT NULL
                        AND NILAI_SKHUN_MHS IS NOT NULL AND CITA_CITA_MHS IS NOT NULL AND NM_AYAH_MHS IS NOT NULL
                        AND PENDIDIKAN_AYAH_MHS IS NOT NULL AND PEKERJAAN_AYAH_MHS IS NOT NULL AND ALAMAT_AYAH_MHS IS NOT NULL
                        AND NM_IBU_MHS IS NOT NULL AND PENDIDIKAN_IBU_MHS IS NOT NULL AND PEKERJAAN_IBU_MHS IS NOT NULL
                        AND ALAMAT_IBU_MHS IS NOT NULL AND PENGHASILAN_ORTU_MHS IS NOT NULL AND NEM_PELAJARAN_MHS IS NOT NULL
                        AND LAHIR_KOTA_MHS IS NOT NULL AND LAHIR_PROP_MHS IS NOT NULL AND ASAL_KOTA_MHS IS NOT NULL
                        AND ASAL_PROV_MHS IS NOT NULL 
                        AND ALAMAT_AYAH_MHS_KOTA IS NOT NULL AND ALAMAT_AYAH_MHS_PROV IS NOT NULL 
                        AND ALAMAT_IBU_MHS_KOTA IS NOT NULL AND ALAMAT_IBU_MHS_PROV IS NOT NULL AND ALAMAT_ASAL_MHS IS NOT NULL
                        AND ALAMAT_ASAL_MHS_KOTA IS NOT NULL AND ALAMAT_ASAL_MHS_PROV IS NOT NULL";
            }

            $cek_biodata = $this->db->QuerySingle($q_bio);
            if ($cek_biodata <= 0) {
                $error .= alert_error('Menu biodata harap dilengkapi<br/>');
            }
        } else {
            $error .= alert_error('Anda Belum Yudisium, Harap Hubungi Akademik Fakultas<br/>');
        }
        return $error;
    }

    function LoadBagTaMhs($id_mhs, $jenjang) {
        return $this->db->QueryToArray("
            SELECT UTA.*,UST.SUDAH
            FROM UPLOAD_TUGAS_AKHIR UTA
            LEFT JOIN 
            (
              SELECT ID_UPLOAD_TUGAS_AKHIR,COUNT(ID_UPLOAD_STATUS_TA) SUDAH
              FROM UPLOAD_STATUS_TA
              WHERE ID_MHS='{$id_mhs}'
              GROUP BY ID_UPLOAD_TUGAS_AKHIR
            ) UST ON UST.ID_UPLOAD_TUGAS_AKHIR=UTA.ID_UPLOAD_TUGAS_AKHIR
            WHERE ID_JENJANG='{$jenjang}' ORDER BY URUTAN");
    }

    function CheckStatusUpload($id_mhs, $jenjang) {
        $jumlah_bag = $this->db->QuerySingle("SELECT COUNT(*) FROM UPLOAD_TUGAS_AKHIR WHERE ID_JENJANG='{$jenjang}'");
        $jumlah_upload = $this->db->QuerySingle("SELECT COUNT(*) FROM UPLOAD_STATUS_TA WHERE ID_MHS='{$id_mhs}'");
        return ($jumlah_bag == $jumlah_upload||$jumlah_bag <= $jumlah_upload);
    }

    function GetIdFolderFile($nim) {
        return $this->db->QuerySingle("SELECT ID_UPLOAD_FOLDER FROM UPLOAD_FOLDER WHERE NAMA_UPLOAD_FOLDER LIKE '%{$nim}%'");
    }

    function InsertFileDatabase($id_folder, $nama, $deskripsi) {
        $this->db->Query("
            INSERT INTO UPLOAD_FILE
                (ID_UPLOAD_FOLDER,ID_PENGGUNA,IP_UPLOAD_FILE,NAMA_UPLOAD_FILE,ID_JENIS_FILE,AKSES,DESKRIPSI,TGL_UPLOAD)
            VALUES
                ('{$id_folder}','{$this->id_pengguna}','{$_SERVER['REMOTE_ADDR']}','{$nama}','1','1','{$deskripsi}',SYSDATE)
            ");
    }

    function GetLastIdFile() {
        return $this->db->QuerySingle("SELECT ID_UPLOAD_FILE FROM UPLOAD_FILE WHERE ID_PENGGUNA='{$this->id_pengguna}' ORDER BY ID_UPLOAD_FILE DESC");
    }

    function InsertStatusUploadFile($id_mhs, $id_ta, $id_file) {
        $this->db->Query("
            INSERT INTO UPLOAD_STATUS_TA
                (ID_MHS,ID_UPLOAD_TUGAS_AKHIR,STATUS_UPLOAD,ID_UPLOAD_FILE)
            VALUES
                ('{$id_mhs}','{$id_ta}','1','{$id_file}')
            ");
    }

    function SettingConfigUpload() {
        $this->db->Query("UPDATE UPLOAD_FOLDER SET ID_PARENT_FOLDER=1866 WHERE ID_PARENT_FOLDER=1");
        //$this->db->Query("DELETE FROM UPLOAD_FOLDER WHERE ID_UPLOAD_FOLDER='1867'");
        //$this->db->Query("INSERT INTO UPLOAD_FOLDER (ID_UPLOAD_SERVER,NAMA_UPLOAD_FOLDER,PATH_UPLOAD_FOLDER,KEDALAMAN,AKSES) VALUES ('2','digilib','/files','0','1')");
    }

    function ResetUpload($id) {

        $this->db->Query("SELECT * FROM MAHASISWA WHERE ID_PENGGUNA='{$id}'");
        $mhs = $this->db->FetchAssoc();
        $this->db->Query("DELETE FROM UPLOAD_STATUS_TA WHERE ID_MHS='{$mhs['ID_MHS']}'");
        $this->db->Query("DELETE FROM UPLOAD_FILE WHERE ID_PENGGUNA='{$mhs['ID_PENGGUNA']}'");
        $this->db->Query("DELETE FROM AUCC.UPLOAD_FOLDER WHERE NAMA_UPLOAD_FOLDER LIKE '%{$mhs['NIM_MHS']}%'");

        // Delete Gagal Upload
        /*
          $this->db->Query("
          DELETE FROM AUCC.UPLOAD_STATUS_TA WHERE ID_MHS IN (
          SELECT ID_MHS FROM AUCC.UPLOAD_STATUS_TA
          HAVING COUNT(ID_UPLOAD_STATUS_TA)=1
          GROUP BY ID_MHS
          )
          ");
          $this->db->Query("
          DELETE FROM AUCC.UPLOAD_FILE WHERE ID_PENGGUNA IN (
          SELECT ID_PENGGUNA FROM AUCC.MAHASISWA WHERE ID_MHS IN
          (
          SELECT ID_MHS FROM AUCC.UPLOAD_STATUS_TA
          HAVING COUNT(ID_UPLOAD_STATUS_TA)=1
          GROUP BY ID_MHS
          )
          )
          ");

          $this->db->Query("
          DELETE FROM AUCC.UPLOAD_FOLDER WHERE NAMA_UPLOAD_FOLDER IN (
          SELECT NIM_MHS FROM AUCC.MAHASISWA WHERE ID_MHS IN
          (
          SELECT ID_MHS FROM AUCC.UPLOAD_STATUS_TA
          HAVING COUNT(ID_UPLOAD_STATUS_TA)=1
          GROUP BY ID_MHS
          )
          )
          ");
         */
    }

}

?>
