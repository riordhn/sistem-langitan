<?php

function alert_error($text) {
    return "<div id='alert'  class='alert alert-danger my-2' role='alert'>
                {$text}
            </div>";
}

function alert_success($text) {
    return "<div id='alert'  class='alert alert-success my-2' role='alert'>
                {$text}
            </div>";
}

function alert_info($text) {
    return "<div id='alert'  class='alert alert-secondary my-2' role='alert'>
                {$text}
            </div>";
}

?>
