<?php
require_once('config.php');
require 'function-tampil-informasi-no-conf.php';
require 'class/process.class.php';

$process = new process($db, $user->ID_PENGGUNA, $user->ID_PERGURUAN_TINGGI);
$id_mhs = $db->QuerySingle("SELECT ID_MHS FROM MAHASISWA WHERE ID_PENGGUNA ='{$user->ID_PENGGUNA}'");
$notif_evaluasi = $process->CekEvaluasi($id_mhs);
$notif_biodata = $process->CekPengisianBiodata($id_mhs);

echo json_encode([
    'status' => 1,
    'evaluasi' => $notif_evaluasi,
    'biodata' => $notif_biodata,
]);
