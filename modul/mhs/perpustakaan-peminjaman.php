<?php
require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_MAHASISWA){
	header("location: /logout.php");
    exit();
}
if ($user->IsLogged() && $user->Role() == AUCC_ROLE_MAHASISWA) {
	$tampil_data = '
	<br>
	<table width="500" border="none">
	<tr>
		<td colspan="6" align="center"><h2>Buku yg belum anda kembalikan</h2></td>
	</tr>
	<tr>
		<th align=center>No</th>
		<th align=center>Judul Buku</th>
		<th align=center>Tanggal Pinjam</th>
	</tr>
	</table>
	';

	$smarty->assign('tampil_data', $tampil_data);

	$smarty->display('perpustakaan-peminjaman.tpl');

}else {
	$smarty->display('session-expired.tpl');
}
?>
