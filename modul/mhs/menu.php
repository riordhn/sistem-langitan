<?php
include('config.php');

//mendapatkan lokasi halaman
$location = explode('-', $_GET['location']);

//meload struktur menu dari modul user (session)
foreach ($user->MODULs as $m)
{
    if ($m['NM_MODUL'] == $location[0])
    {
        $smarty->assign('nm_modul', $m['NM_MODUL']);
        $smarty->assign('menu_set', $m['MENUs']);
    }
}

$smarty->display('menu.tpl');