<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';
include '../keuangan/class/laporan.class.php';
include '../ppmb/class/Penerimaan.class.php';
include '../keuangan/class/pendaftaran.class.php';


$list = new list_data($db);
$laporan = new laporan($db);
$penerimaan = new Penerimaan($db);

if (isset($_GET)) {
    if (get('mode') == 'laporan') {
        $smarty->assign('data_penghasilan', $laporan->load_report_penghasilan_ortu(get('fakultas'), get('penerimaan')));
    }
}

$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
$smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->display('laporan/penghasilan-ortu.tpl');
?>
