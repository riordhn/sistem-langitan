<?php
include 'config.php';
include 'class/list_data.class.php';

$list = new list_data($db);

$fakultas = get('fakultas');
$prodi = get('prodi');
$tgl_awal = get('tgl_awal');
$tgl_akhir = get('tgl_akhir');
$q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI={$prodi}" : "";

$data_excel = $db->QueryToArray("
            SELECT P.ID_PENGGUNA,P.GELAR_DEPAN,P.NM_PENGGUNA,P.GELAR_BELAKANG,D.ID_DOSEN,D.NIP_DOSEN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,PM.TOTAL_SKS,PJ.TOTAL_JAM
            FROM AUCC.DOSEN D
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN (
                SELECT PM.ID_DOSEN,SUM(KUMK.KREDIT_SEMESTER) TOTAL_SKS
                FROM AUCC.PRESENSI_MKDOS PM
                JOIN AUCC.PRESENSI_KELAS PK ON PK.ID_PRESENSI_KELAS =PM.ID_PRESENSI_KELAS AND PM.KEHADIRAN=1
                JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK=PK.ID_KELAS_MK
                JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=KMK.ID_PROGRAM_STUDI
                WHERE TO_CHAR(PK.TGL_PRESENSI_KELAS,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}' AND PS.ID_FAKULTAS={$fakultas} {$q_prodi}
                GROUP BY PM.ID_DOSEN
            ) PM ON PM.ID_DOSEN=D.ID_DOSEN
            JOIN (
                SELECT PM.ID_DOSEN,SUM(
                    (
                        TO_DATE(
                          TO_CHAR(PK.TGL_PRESENSI_KELAS,'DD.MM.YYYY'
                          )||':'||REPLACE(PK.WAKTU_SELESAI,'.',':'
                          ),'DD-MM-YYYY HH24:MI'
                      )-
                        TO_DATE(
                          TO_CHAR(PK.TGL_PRESENSI_KELAS,'DD.MM.YYYY'
                          )||':'||REPLACE(PK.WAKTU_MULAI,'.',':'
                          ),'DD-MM-YYYY HH24:MI'
                      ))*24
                ) TOTAL_JAM
                FROM AUCC.PRESENSI_MKDOS PM
                JOIN AUCC.PRESENSI_KELAS PK ON PK.ID_PRESENSI_KELAS =PM.ID_PRESENSI_KELAS AND PM.KEHADIRAN=1
                JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK=PK.ID_KELAS_MK
                JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=KMK.ID_PROGRAM_STUDI
                WHERE TO_CHAR(PK.TGL_PRESENSI_KELAS,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}' AND PS.ID_FAKULTAS={$fakultas} {$q_prodi}
                GROUP BY PM.ID_DOSEN
            ) PJ ON PJ.ID_DOSEN=D.ID_DOSEN
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,P.NM_PENGGUNA
            ");
if (get('fakultas') != '' || get('prodi') != '') {
    $fakultas = $list->get_fakultas(get('fakultas'));
    $prodi = $list->get_prodi(get('prodi'));
}
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
    }
    td{
        text-align: left;
    }
</style>
<table width='950' border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="6" class="header_text">
                <?php
                echo "Rekapitulasi Mengajar Dosen<br/> Dari Tanggal {$tgl_awal} Sampai {$tgl_akhir} ";
                if ($fakultas != '') {
                    echo 'Fakultas ' . $fakultas['NM_FAKULTAS'] . '<br/>';
                }
                if ($prodi != '') {
                    echo 'Program Studi (' . $prodi['NM_JENJANG'] . ') ' . $prodi['NM_PROGRAM_STUDI'] . '<br/>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td class="header_text">NO</td>
            <td class="header_text">NIP DOSEN</td>
            <td class="header_text">NAMA</td>
            <td class="header_text">PROGRAM STUDI</td>
            <td class="header_text">TOTAL SKS</td>
            <td class="header_text">TOTAL JAM MENGAJAR</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data_excel as $data) {
            echo "<tr>
                        <td>{$no}</td>
                        <td>'{$data['NIP_DOSEN']}</td>
                        <td>{$data['GELAR_DEPAN']} {$data['NM_PENGGUNA']} {$data['GELAR_BELAKANG']}</td>
                        <td>{$data['NM_JENJANG']} {$data['NM_PROGRAM_STUDI']}</td>
                        <td>{$data['TOTAL_SKS']}</td>
                        <td>{$data['TOTAL_JAM']}</td>
                </tr>";
            $no++;
        }
        ?>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "rekap-mengajar(" . date('d-m-Y_h:i:s') . ')';
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
