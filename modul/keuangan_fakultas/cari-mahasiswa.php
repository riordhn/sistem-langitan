<?php

include 'config.php';
include '../keuangan/class/utility.class.php';

$utility = new utility($db);

if (isset($_GET['x'])) {
    $smarty->assign('data_mahasiswa', $utility->get_hasil_cari_mahasiswa(get('x')));
}
$smarty->display("utility/cari-mahasiswa.tpl");
?>
