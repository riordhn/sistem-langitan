<?php

include 'config.php';
include 'class/honorarium.class.php';
include '../keuangan/class/honor.class.php';
include '../keuangan/class/list_data.class.php';

$list = new list_data($db);
$honor = new honor($db);
$honorarium = new honorarium($db);

$smarty->assign('data_honorarium', $honorarium->load_honorarium());
$smarty->assign('data_golongan', $list->load_golongan());
$smarty->assign('data_prodi', $list->load_list_prodi($user->ID_FAKULTAS));
$smarty->assign('data_jenis_honor', $honor->load_jenis_honor());
$smarty->assign('data_pendidikan', $list->load_pendidikan_akhir());

if (isset($_GET['mode'])) {
    if (get('mode') == 'add') {
        $smarty->display('honor/add-tarif-honor.tpl');
    } else if (get('mode') == 'edit') {
        $smarty->assign('data_tarif_honor', $honorarium->get_honorarium(get('id')));
        $smarty->display('honor/edit-tarif-honor.tpl');
    }
} else {
    if (isset($_POST)) {
        if (post('mode') == 'add') {
            $honorarium->add_honorarium(post('jenis_honor'), post('prodi'), post('jenis_kelas'), post('status_kelas'), post('golongan'), post('pendidikan'), post('tarif'));
            $smarty->assign('alert', alert_success('Data Berhasil Dimasukkan'));
        } else if (post('mode') == 'edit') {
            $honorarium->edit_honorarium(post('id'), post('jenis_honor'), post('prodi'), post('jenis_kelas'), post('status_kelas'), post('golongan'), post('pendidikan'), post('tarif'));
            $smarty->assign('alert', alert_success('Data Berhasil Di Ubah'));
        }
    }
    $smarty->assign('data_tarif_honor', $honorarium->load_honorarium($user->ID_FAKULTAS));
    $smarty->display('honor/tarif-honor.tpl');
}
?>
