<?php

include 'config.php';
include 'class/master.class.php';
include '../keuangan/class/list_data.class.php';

$master = new master($db);
$list = new list_data($db);

if (isset($_POST)) {
    if (post('mode') == 'add') {
        $master->add_periode_bayar_sp(post('fakultas'), post('semester'), post('jenjang'), post('tgl_awal'), post('tgl_akhir'), post('keterangan'));
    } else if (post('mode') == 'edit') {
        $master->update_periode_bayar_sp(post('id'), post('fakultas'), post('semester'), post('jenjang'), post('tgl_awal'), post('tgl_akhir'), post('keterangan'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit') {
        $smarty->assign('periode', $master->get_periode_bayar_sp(get('id')));
    }
    $smarty->assign('data_semester', $list->load_list_semester_sp());
    $smarty->assign('data_fakultas', $list->load_list_fakultas());
    $smarty->assign('data_jenjang', $list->load_list_jenjang());
    $smarty->assign('id_fakultas', $user->ID_FAKULTAS);
}
$smarty->assign('data_periode', $master->load_periode_bayar_sp($user->ID_FAKULTAS));
$smarty->display("master/periode-sp.tpl");
?>
