<?php

include 'config.php';
include '../keuangan/class/honor.class.php';
$honor = new honor($db);
$smarty->assign('data_jenis_honor', $honor->load_jenis_honor());
$smarty->assign('alert', alert_success('Untuk Menambahkan/Mengubah master Jenis Honor silahkan menghubungi Direktorat Keuangan'));
$smarty->display('honor/jenis-honor.tpl');
?>
