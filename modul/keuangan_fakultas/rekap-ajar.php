<?php

include 'config.php';
include '../keuangan/class/laporan.class.php';
include '../keuangan/class/list_data.class.php';
include '../keuangan/class/paging.class.php';

$laporan = new laporan($db);
$list = new list_data($db);

if (isset($_GET)) {
    $fakultas = get('fakultas');
    $prodi = get('prodi');
    $tgl_awal = get('tgl_awal');
    $tgl_akhir = get('tgl_akhir');
    $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI={$prodi}" : "";
    if (get('mode') == 'tampil') {
        $data_rekap = $db->QueryToArray("
            SELECT P.ID_PENGGUNA,P.GELAR_DEPAN,P.NM_PENGGUNA,P.GELAR_BELAKANG,D.ID_DOSEN,D.NIP_DOSEN,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,PM.TOTAL_SKS,PJ.TOTAL_JAM
            FROM AUCC.DOSEN D
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN (
                SELECT PM.ID_DOSEN,SUM(KUMK.KREDIT_SEMESTER) TOTAL_SKS
                FROM AUCC.PRESENSI_MKDOS PM
                JOIN AUCC.PRESENSI_KELAS PK ON PK.ID_PRESENSI_KELAS =PM.ID_PRESENSI_KELAS AND PM.KEHADIRAN=1
                JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK=PK.ID_KELAS_MK
                JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=KMK.ID_PROGRAM_STUDI
                WHERE TO_CHAR(PK.TGL_PRESENSI_KELAS,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}' AND PS.ID_FAKULTAS={$fakultas} {$q_prodi}
                GROUP BY PM.ID_DOSEN
            ) PM ON PM.ID_DOSEN=D.ID_DOSEN
            JOIN (
                SELECT PM.ID_DOSEN,SUM(
                    (
                        TO_DATE(
                          TO_CHAR(PK.TGL_PRESENSI_KELAS,'DD.MM.YYYY'
                          )||':'||REPLACE(PK.WAKTU_SELESAI,'.',':'
                          ),'DD-MM-YYYY HH24:MI'
                      )-
                        TO_DATE(
                          TO_CHAR(PK.TGL_PRESENSI_KELAS,'DD.MM.YYYY'
                          )||':'||REPLACE(PK.WAKTU_MULAI,'.',':'
                          ),'DD-MM-YYYY HH24:MI'
                      ))*24
                ) TOTAL_JAM
                FROM AUCC.PRESENSI_MKDOS PM
                JOIN AUCC.PRESENSI_KELAS PK ON PK.ID_PRESENSI_KELAS =PM.ID_PRESENSI_KELAS AND PM.KEHADIRAN=1
                JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK=PK.ID_KELAS_MK
                JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=KMK.ID_PROGRAM_STUDI
                WHERE TO_CHAR(PK.TGL_PRESENSI_KELAS,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}' AND PS.ID_FAKULTAS={$fakultas} {$q_prodi}
                GROUP BY PM.ID_DOSEN
            ) PJ ON PJ.ID_DOSEN=D.ID_DOSEN
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,P.NM_PENGGUNA
            ");
        $smarty->assign('data_rekap', $data_rekap);
    } else if (get('mode') == 'detail') {
        $id = get('id');
        $data_detail = $db->QueryToArray("
               SELECT PK.ID_KELAS_MK,PK.TGL_PRESENSI_KELAS,PK.WAKTU_MULAI,PK.WAKTU_SELESAI,KUMK.KREDIT_SEMESTER,
               (
                    TO_DATE(
                      TO_CHAR(PK.TGL_PRESENSI_KELAS,'DD.MM.YYYY'
                      )||':'||REPLACE(PK.WAKTU_SELESAI,'.',':'
                      ),'DD-MM-YYYY HH24:MI'
                  )-
                    TO_DATE(
                      TO_CHAR(PK.TGL_PRESENSI_KELAS,'DD.MM.YYYY'
                      )||':'||REPLACE(PK.WAKTU_MULAI,'.',':'
                      ),'DD-MM-YYYY HH24:MI'
                  ))*24 JAM_MENGAJAR,MK.NM_MATA_KULIAH,
               MK.KD_MATA_KULIAH,NVL(NK.NAMA_KELAS,KMK.NM_KELAS_MK) NM_KELAS, PS.NM_PROGRAM_STUDI,J.NM_JENJANG
                FROM AUCC.PRESENSI_MKDOS PM
                JOIN AUCC.PRESENSI_KELAS PK ON PM.ID_PRESENSI_KELAS=PK.ID_PRESENSI_KELAS
                JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK=PK.ID_KELAS_MK
                JOIN AUCC.NAMA_KELAS NK ON NK.ID_NAMA_KELAS = KMK.NO_KELAS_MK
                JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
                JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH=KUMK.ID_MATA_KULIAH
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=KMK.ID_PROGRAM_STUDI
                JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                WHERE TO_CHAR(PK.TGL_PRESENSI_KELAS,'YYYY-MM-DD') BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}' AND PM.ID_DOSEN='{$id}' AND PS.ID_FAKULTAS={$fakultas} {$q_prodi}
                ORDER BY NM_MATA_KULIAH,NM_KELAS,TGL_PRESENSI_KELAS DESC
            ");
        $smarty->assign('data_detail', $data_detail);
    }
}

$smarty->assign('info1', alert_success("Harap Mengisi Tarif Honorarium pada menu Tarif Honorarium ", "90"));
$smarty->assign('tgl_awal', get('tgl_awal'));
$smarty->assign('tgl_akhir', get('tgl_akhir'));
$smarty->assign('fakultas', get('fakultas'));
$smarty->assign('prodi', get('prodi'));
$smarty->assign('id_fakultas', $user->ID_FAKULTAS);
$smarty->assign('data_prodi', $list->load_list_prodi($user->ID_FAKULTAS));
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_jalur', $list->load_list_jalur());
$smarty->display('honor/rekap-ajar.tpl');
?>
