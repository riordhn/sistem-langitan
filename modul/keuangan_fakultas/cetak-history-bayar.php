<?php
include ('config.php');
include ('../../tcpdf/config/lang/ind.php');
include ('../../tcpdf/tcpdf.php');
include ('class/history.class.php');
include ('class/list_data.class.php');

$history = new history($db);
$list = new list_data($db);
$semester = $list->get_semester(get('semester'));

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .header { font-size: 20pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 14pt; font-family: serif; margin-top: 0px ;text-align:center; }
    td { font-size: 10pt; }
    td.center { font-weight:bold;text-align:center }
    th { background-color:black;color:white;font-size: 9pt;text-align:center; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="10%" align="right"><img src="../../img/maba/logo_unair.gif" width="80px" height="80px"/></td>
        <td width="80%" align="center">
            <span class="header">UNIVERSITAS AIRLANGGA<br/></span><span class="address">DIREKTORAT KEUANGAN</span><br/>BUKTI PEMBAYARAN 
        </td>
    </tr>
</table>
<hr/>
<p></p>
<table width="50%" cellpadding="3" border="0.5">
                <tr>
                    <th colspan="2">Detail Biodata</th>
                </tr>
                {nomer_header}
                <tr>
                    <td>Nama</td>
                    <td>{nama}</td>
                </tr>
                <tr>
                    <td>Fakultas</td>
                    <td>{fakultas}</td>
                </tr>
                <tr>
                    <td>Program Studi</td>
                    <td>{prodi}</td>
                </tr>
                <tr>
                    <td>Jenjang</td>
                    <td>{jenjang}</td>
                </tr>
</table>
<p></p>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center">
            <span class="address">DETAIL PEMBAYARAN {semester}</span>
        </td>
    </tr>
</table>
<p></p>
<table width="100%" cellpadding="2" border="0.5">
        
        <tr>
            <th width="25px">No</th>
            {all_header}
            {besar_biaya_header}
            <th>Nama Bank</th>
            <th>Via Bank</th>
            <th>Tanggal Bayar</th>
            <th>No Transaksi</th>
            <th>Keterangan</th>
        </tr>
    {data_pembayaran}
        
</table>
<p></p>
<table width="100%" border="0">
    <tr>
        <td width="100%" class="center">
            <span class="address">TOTAL PEMBAYARAN {total_pembayaran}</span>
        </td>
    </tr>
</table>
<p></p>
Data Ini Di Cetak Pada Tanggal {tanggal}
EOF;


$index = 1;
$data_pembayaran = '';

if (get('mode') == 'all') {
    if (get('jenis') == 'mhs') {
        $biodata = $history->get_data_mhs(get('cari'));
        $nomer_header = '<tr><td>NIM</td><td>' . $biodata['NIM_MHS'] . '</td></tr>';
        $all_header = '<th width="110px">Semester</th>';
        $besar_biaya_header = '<th width="80px">Besar Biaya</th>';
        foreach ($history->load_history_bayar_mhs(get('cari')) as $data) {
            $data_pembayaran .= '<tr>
            <td>' . $index++ . '</td>
            <td>' . $data['NM_SEMESTER'] . ' ( ' . $data['TAHUN_AJARAN'] . ' )' . '</td>
            <td>Rp. ' . number_format($data['JUMLAH_PEMBAYARAN']) . '</td>
            <td>' . $data['NM_BANK'] . '</td>
            <td>' . $data['NAMA_BANK_VIA'] . '</td>
            <td>' . $data['TGL_BAYAR'] . '</td>
            <td>' . $data['NO_TRANSAKSI'] . '</td>
            <td>' . $data['KETERANGAN'] . '</td> 
    </tr>';
            $total_pembayaran+=$data['JUMLAH_PEMBAYARAN'];
        }
    } else {
        $biodata = $history->get_data_cmhs(get('cari'));
        $nomer_header = '<tr><td>Nomer Ujian</td><td>' . $biodata['NO_UJIAN'] . '</td></tr>';
        $all_header = '<th width="110px">Semester</th>';
        $besar_biaya_header = '<th width="80px">Besar Biaya</th>';
        foreach ($history->load_history_bayar_cmhs(get('cari')) as $data) {
            $data_pembayaran .= '<tr>
            <td>' . $index++ . '</td>
            <td>' . $data['NM_SEMESTER'] . ' ( ' . $data['TAHUN_AJARAN'] . ' )' . '</td>
            <td>Rp. ' . number_format($data['JUMLAH_PEMBAYARAN']) . '</td>
            <td>' . $data['NM_BANK'] . '</td>
            <td>' . $data['NAMA_BANK_VIA'] . '</td>
            <td>' . $data['TGL_BAYAR'] . '</td>
            <td>' . $data['NO_TRANSAKSI'] . '</td>
            <td>' . $data['KETERANGAN'] . '</td> 
    </tr>';
            $total_pembayaran+=$data['JUMLAH_PEMBAYARAN'];
        }
    }
} else {
    if (get('jenis') == 'mhs') {
        $biodata = $history->get_data_mhs(get('cari'));
        $nomer_header = '<tr><td>NIM</td><td>' . $biodata['NIM_MHS'] . '</td></tr>';
        $all_header = '<th>Nama Biaya</th>';
        $besar_biaya_header = '<th width="80px">Besar Biaya</th><th width="80px">Denda Biaya</th>';
        foreach ($history->get_history_bayar_mhs(get('cari'), get('semester'), get('no_transaksi'), get('tgl_bayar')) as $data) {
            $data_pembayaran .= '<tr>
            <td>' . $index++ . '</td>
            <td>' . $data['NM_BIAYA'] . '</td>
            <td>Rp. ' . number_format($data['BESAR_BIAYA']) . '</td>
            <td>' . $data['DENDA_BIAYA'] . '</td>
            <td>' . $data['NM_BANK'] . '</td>
            <td>' . $data['NAMA_BANK_VIA'] . '</td>
            <td>' . $data['TGL_BAYAR'] . '</td>
            <td>' . $data['NO_TRANSAKSI'] . '</td>
            <td>' . $data['KETERANGAN'] . '</td> 
    </tr>';
            $total_pembayaran+=$data['BESAR_BIAYA'] + $data['DENDA_BIAYA'];
        }
    } else {
        $biodata = $history->get_data_cmhs(get('cari'));
        $nomer_header = '<tr><td>Nomer Ujian</td><td>' . $biodata['NO_UJIAN'] . '</td></tr>';
        $all_header = '<th>Nama Biaya</th>';
        $besar_biaya_header = '<th width="80px">Besar Biaya</th>';
        foreach ($history->get_history_bayar_cmhs(get('cari'), get('semester'), get('no_transaksi'), get('tgl_bayar')) as $data) {
            $data_pembayaran .= '<tr>
            <td>' . $index++ . '</td>
            <td>' . $data['NM_BIAYA'] . '</td>
            <td>Rp. ' . number_format($data['BESAR_BIAYA']) . '</td>
            <td>' . $data['NM_BANK'] . '</td>
            <td>' . $data['NAMA_BANK_VIA'] . '</td>
            <td>' . $data['TGL_BAYAR'] . '</td>
            <td>' . $data['NO_TRANSAKSI'] . '</td>
            <td>' . $data['KETERANGAN'] . '</td>        
    </tr>';
            $total_pembayaran+=$data['BESAR_BIAYA'];
        }
    }
}
$nama = $biodata['NM_PENGGUNA'] != NULL ? $biodata['NM_PENGGUNA'] : $biodata['NM_C_MHS'];
$html = str_replace('{nama}', $nama, $html);
$html = str_replace('{fakultas}', $biodata['NM_FAKULTAS'], $html);
$html = str_replace('{prodi}', $biodata['NM_PROGRAM_STUDI'], $html);
$html = str_replace('{jenjang}', $biodata['NM_JENJANG'], $html);
$html = str_replace('{nomer_header}', $nomer_header, $html);
$html = str_replace('{besar_biaya_header}', $besar_biaya_header, $html);
$html = str_replace('{all_header}', $all_header, $html);
$html = str_replace('{data_pembayaran}', $data_pembayaran, $html);
$html = str_replace('{tanggal}', date('d F Y  H:i:s'), $html);
if (get('mode') == '')
    $html = str_replace('{semester}', 'SEMESTER ' . $semester['NM_SEMESTER'] . ' ( ' . $semester['TAHUN_AJARAN'] . ' )', $html);
else
    $html = str_replace('{semester}', '', $html);
$html = str_replace('{total_pembayaran}', 'Rp.' . number_format($total_pembayaran), $html);

$pdf->writeHTML($html);

$pdf->Output();
?>
