<?php

include 'config.php';
include 'class/history.class.php';

$history = new history($db);

$smarty->assign('data_pembayaran', $history->load_history_bayar_mhs(trim(get('cari'))));
$smarty->assign('data_detail_mahasiswa', $history->get_data_mhs(trim(get('cari'))));
$smarty->display('laporan/load-history-bayar-piutang.tpl');
?>
