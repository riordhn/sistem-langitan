<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';
include '../keuangan/class/pendapatan.class.php';
$list = new list_data($db);
$pendapatan = new pendapatan($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $smarty->assign('data_pendapatan_mala', $pendapatan->load_pendapatan_spp_mala(get('fakultas'), get('semester')));
        $smarty->assign('data_pendapatan_terima_muka_mala', $pendapatan->load_pendapatan_terima_muka_spp_mala(get('fakultas'), get('semester')));
        $smarty->assign('data_pendapatan_maba', $pendapatan->load_pendapatan_spp_maba(get('fakultas'), get('semester')));
        $smarty->assign('data_pendapatan_terima_muka_maba', $pendapatan->load_pendapatan_terima_muka_spp_maba(get('fakultas'), get('semester')));
    } else if (get('mode') == 'detail') {
        $smarty->assign('data_biaya', $list->load_biaya());
        $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
        $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        $smarty->assign('data_semester_one', $list->get_semester(get('semester')));
        //echo '<pre>';var_dump($pendapatan->load_detail_pendapatan_spp_mala(get('fakultas'), get('prodi'), get('semester')));echo '</pre>';
        $smarty->assign('data_pendapatan_spp_mala', $pendapatan->load_detail_pendapatan_spp_mala(get('fakultas'), get('prodi'), get('semester')));
    } else if (get('mode') == 'detail_terima_muka') {
        $smarty->assign('data_biaya', $list->load_biaya());
        $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
        $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
        $smarty->assign('data_semester_one', $list->get_semester(get('semester')));
        if (get('mhs') == 'lama')
            $smarty->assign('data_pendapatan_terima_muka', $pendapatan->load_detail_pendapatan_terima_muka_spp_mala(get('fakultas'), get('prodi'), get('semester')));
        else
            $smarty->assign('data_pendapatan_terima_muka', $pendapatan->load_detail_pendapatan_terima_muka_spp_maba(get('fakultas'), get('prodi'), get('semester')));
    }
}
$smarty->assign('id_fakultas',$user->ID_FAKULTAS);
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->display('pendapatan/pendapatan-spp.tpl');
?>
