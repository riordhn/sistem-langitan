<?php

include 'config.php';
include '../keuangan/class/honor.class.php';
$load_honor = new honor($db);
$smarty->assign('data_pajak_honor', $load_honor->load_pajak_honor());
$smarty->assign('alert', alert_success('Untuk Menambahkan/Mengubah master Pajak Honor silahkan menghubungi Direktorat Keuangan'));
$smarty->display('honor/pajak-honor.tpl');
?>
