<?php

include 'config.php';
include '../keuangan/class/master.class.php';
include '../keuangan/class/list_data.class.php';

$biaya_sks = new master($db);
$list = new list_data($db);

$mode = get('mode', 'view');

if ($request_method == 'POST') {
    if (post('mode') == 'add') {
        $biaya_sks->add_biaya_sks(post('semester'), post('prodi'), post('besar_biaya'), post('keterangan'), post('jalur'));
    } else if (post('mode') == 'edit') {
        $biaya_sks->edit_biaya_sks(post('semester'), post('prodi'), post('besar_biaya'), post('keterangan'), post('id_biaya_sks'), post('jalur'));
    } else if (post('mode') == 'delete') {
        $id=post('id_biaya_sks');
        $db->Query("DELETE FROM BIAYA_SP WHERE ID_BIAYA_SP='{$id}'");
    }
}
if ($mode == 'detail' || $mode == 'edit' || $mode == 'upload' || $mode == 'delete') {
    $smarty->assign('data_biaya_sks', $biaya_sks->get_biaya_sks(get('id_biaya_sks')));
} else {
    $smarty->assign('data_biaya_sks', $biaya_sks->load_biaya_sks($user->ID_FAKULTAS));
}
$smarty->assign('id_fakultas', $user->ID_FAKULTAS);
$smarty->assign('data_jalur', $list->load_list_jalur());
$smarty->assign('semester', get('semester'));
$smarty->assign('data_semester', $list->load_list_semester_sp());
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_prodi', $list->load_list_prodi($user->ID_FAKULTAS));
$smarty->display("master/{$mode}-biaya-sks.tpl");
?>