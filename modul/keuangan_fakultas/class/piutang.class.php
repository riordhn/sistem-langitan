<?php

class piutang {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    

    function load_list_semester() {
        return $this->db->QueryToArray("
            SELECT * FROM SEMESTER 
            WHERE  (NM_SEMESTER = 'Ganjil' or NM_SEMESTER = 'Genap') AND THN_AKADEMIK_SEMESTER BETWEEN '2000' 
            AND (SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True')
            AND ID_SEMESTER <> (SELECT ID_SEMESTER FROM SEMESTER WHERE NM_SEMESTER = 'Genap' AND THN_AKADEMIK_SEMESTER = 
            (SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True'))
            ORDER BY TAHUN_AJARAN,NM_SEMESTER");
    }

    function load_list_status() {
        return $this->db->QueryToArray("SELECT * FROM STATUS_PENGGUNA WHERE ID_ROLE=3");
    }

    function load_mahasiswa_piutang($id_fakultas, $id_program_studi, $status) {
        if ($id_program_studi != '') {
            $query = " AND PS.ID_PROGRAM_STUDI = '{$id_program_studi}'";
        } else if ($id_fakultas != '') {
            $query = "AND PS.ID_FAKULTAS = '{$id_fakultas}'";
        } else {
            $query = "";
        }
        if ($status != '') {
            $query_status = "M.STATUS_AKADEMIK_MHS ='{$status}'";
        } else {
            $query_status = "M.STATUS_AKADEMIK_MHS IS NOT NULL";
        }
        return $this->db->QueryToArray("
            SELECT M.ID_MHS, M.NIM_MHS, P.NM_PENGGUNA, SP.NM_STATUS_PENGGUNA, F.NM_FAKULTAS,PS. NM_PROGRAM_STUDI, J.NM_JENJANG,M.STATUS_AKADEMIK_MHS
            FROM MAHASISWA M
            LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE {$query_status}
            {$query}
            ");
    }

    function load_mahasiswa_piutang_excel($id_fakultas, $id_program_studi, $status) {
        if ($id_program_studi != '') {
            $query = " AND PS.ID_PROGRAM_STUDI = '{$id_program_studi}'";
        } else if ($id_fakultas != '') {
            $query = "AND PS.ID_FAKULTAS = '{$id_fakultas}'";
        } else {
            $query = "";
        }
        if ($status != '') {
            $query_status = "M.STATUS_AKADEMIK_MHS ='{$status}'";
        } else {
            $query_status = "M.STATUS_AKADEMIK_MHS IS NOT NULL";
        }
        return $this->db->QueryToArray("
            SELECT M.ID_MHS, M.NIM_MHS, P.NM_PENGGUNA, SP.NM_STATUS_PENGGUNA, F.NM_FAKULTAS,PS. NM_PROGRAM_STUDI, J.NM_JENJANG,M.STATUS_AKADEMIK_MHS
            FROM MAHASISWA M
            LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
            LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE {$query_status}
            {$query}");
    }

    

    function get_data_piutang($nim) {
       return $this->db->QueryToArray("SELECT S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,PEM.ID_MHS,SUM(PEM.TOTAL_BIAYA) TOTAL_BIAYA FROM 
        (SELECT ID_SEMESTER,NM_SEMESTER,TAHUN_AJARAN FROM SEMESTER
        WHERE  (NM_SEMESTER = 'Ganjil' or NM_SEMESTER = 'Genap') AND THN_AKADEMIK_SEMESTER BETWEEN '2000' 
        AND (SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True')
        AND ID_SEMESTER <> (SELECT ID_SEMESTER FROM SEMESTER WHERE NM_SEMESTER = 'Genap' AND THN_AKADEMIK_SEMESTER = 
        (SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True'))) S
        LEFT JOIN 
        (SELECT PEM.ID_SEMESTER,PEM.ID_MHS,(CASE
            WHEN (PEM.TGL_BAYAR IS NOT NULL) THEN SUM(PEM.BESAR_BIAYA)
            WHEN (PEM.TGL_BAYAR IS NULL) THEN SUM(PEM.BESAR_BIAYA)*-1
        END) 
        TOTAL_BIAYA FROM PEMBAYARAN PEM
        JOIN MAHASISWA M ON M.ID_MHS=PEM.ID_MHS
        WHERE M.NIM_MHS='{$nim}'
        GROUP BY PEM.ID_SEMESTER,PEM.ID_MHS,PEM.TGL_BAYAR) PEM
        ON S.ID_SEMESTER= PEM.ID_SEMESTER 
        GROUP BY S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,PEM.ID_MHS
        ORDER BY S.TAHUN_AJARAN,S.NM_SEMESTER");
    }

    function load_list_fakultas() {
        return $this->db->QueryToArray("SELECT ID_FAKULTAS,NM_FAKULTAS FROM FAKULTAS");
    }

    function load_list_prodi($id_fakultas, $id_jenjang = null) {
        if ($id_jenjang != '') {
            $q_jenjang = "PS.ID_JENJANG = '{$id_jenjang}'";
        } else {
            $q_jenjang = "";
        }
        if ($id_fakultas != '') {
            $q_fakultas = "PS.ID_FAKULTAS = '{$id_fakultas}'";
        } else {
            $q_fakultas = "";
        }
        if ($id_jenjang != '' && $id_fakultas != '') {
            $q_operator = "AND";
        } else {
            $q_operator = "";
        }

        return $this->db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            FROM PROGRAM_STUDI PS
            JOIN JENJANG J ON PS.ID_JENJANG=J.ID_JENJANG
            WHERE {$q_fakultas} {$q_operator} {$q_jenjang}");
    }

    function load_data_puitang($id_fakultas, $id_program_studi, $status, $excel = NULL) {
        $data_piutang = array();
        if (empty($excel)) {
            foreach ($this->load_mahasiswa_piutang($id_fakultas, $id_program_studi, $status) as $data) {
                array_push($data_piutang, array(
                    'ID_MHS' => $data['ID_MHS'],
                    'NIM_MHS' => $data['NIM_MHS'],
                    'PRODI' => '( ' . $data['NM_JENJANG'] . ' ) ' . $data['NM_PROGRAM_STUDI'],
                    'NM_PENGGUNA' => $data['NM_PENGGUNA'],
                    'ID_STATUS' => $data['STATUS_AKADEMIK_MHS'],
                    'STATUS' => $data['NM_STATUS_PENGGUNA'],
                    'DATA_PIUTANG' => $this->get_data_piutang($data['NIM_MHS'])
                ));
            }
        }else{
            foreach ($this->load_mahasiswa_piutang_excel($id_fakultas, $id_program_studi, $status) as $data) {
                array_push($data_piutang, array(
                    'ID_MHS' => $data['ID_MHS'],
                    'NIM_MHS' => $data['NIM_MHS'],
                    'PRODI' => '( ' . $data['NM_JENJANG'] . ' ) ' . $data['NM_PROGRAM_STUDI'],
                    'NM_PENGGUNA' => $data['NM_PENGGUNA'],
                    'ID_STATUS' => $data['STATUS_AKADEMIK_MHS'],
                    'STATUS' => $data['NM_STATUS_PENGGUNA'],
                    'DATA_PIUTANG' => $this->get_data_piutang($data['NIM_MHS'])
                ));
            }
        }
        return $data_piutang;
    }

}

?>
