<?php

class upload {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    function get_mahasiswa($nim) {
        $this->db->Query("SELECT P.NM_PENGGUNA,M.* FROM MAHASISWA M JOIN PENGGUNA P ON P.ID_PENGGUNA= M.ID_PENGGUNA WHERE NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_mahasiswa($nim) {
        return $this->db->QueryToArray("SELECT * FROM MAHASISWA WHERE NIM_MHS='{$nim}'");
    }

    function cek_detail_biaya($nim_mhs, $semester, $id_biaya, $tgl_bayar, $id_bank) {
        $sudah_dimasukkan = $this->db->QueryToArray("
            SELECT M.ID_MHS FROM MAHASISWA M
            JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
            WHERE PEM.ID_SEMESTER = '{$semester}' AND M.NIM_MHS='{$nim_mhs}' AND DB.ID_BIAYA ='{$id_biaya}'
            AND PEM.TGL_BAYAR =UPPER('{$tgl_bayar}') AND PEM.ID_BANK ='{$id_bank}'");
        $sudah_bayar = $this->db->QueryToArray("
            SELECT M.ID_MHS FROM MAHASISWA M
            JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
            WHERE PEM.ID_SEMESTER = '{$semester}' AND M.NIM_MHS='{$nim_mhs}' AND DB.ID_BIAYA ='{$id_biaya}'
            AND PEM.TGL_BAYAR IS NOT NULL AND PEM.ID_BANK IS NOT NULL");
        $diloloskan = $this->db->QueryToArray("SELECT M.ID_MHS FROM MAHASISWA M
            JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
            WHERE PEM.ID_SEMESTER = '{$semester}' AND M.NIM_MHS='{$nim_mhs}' AND DB.ID_BIAYA ='{$id_biaya}'
            AND (PEM.TGL_BAYAR IS NOT NULL OR PEM.TGL_BAYAR IS NULL) AND PEM.ID_BANK IS NULL");
        if ($sudah_dimasukkan != null) {
            $status_bayar = 4;
        } else if ($sudah_bayar != null && $diloloskan == null && $sudah_dimasukkan == null) {
            $status_bayar = 1;
        } else if ($sudah_bayar == null && $diloloskan != null) {
            $status_bayar = 2;
        } else {
            $status_bayar = 0;
        }
        return $status_bayar;
    }

    function get_id_detail_biaya($nim, $id_biaya) {
        $this->db->Query("SELECT DB.ID_DETAIL_BIAYA FROM MAHASISWA M
            JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
            WHERE M.NIM_MHS='{$nim}' AND DB.ID_BIAYA ='{$id_biaya}'
            ");
        $data_biaya = $this->db->FetchAssoc();
        if (empty($data_biaya)) {
            $this->db->Query("SELECT DB.ID_DETAIL_BIAYA FROM DETAIL_BIAYA DB
            WHERE DB.ID_BIAYA ='{$id_biaya}'
            ");
            $data_biaya = $this->db->FetchAssoc();
        }
        return $data_biaya['ID_DETAIL_BIAYA'];
    }

    function insert($id_mhs, $id_semester, $id_detail_biaya, $id_bank, $id_bank_via, $tgl_bayar, $keterangan, $besar_biaya) {
        $this->db->Query("
            INSERT INTO PEMBAYARAN
                (ID_MHS,ID_SEMESTER,ID_DETAIL_BIAYA,ID_BANK,ID_BANK_VIA,TGL_BAYAR,KETERANGAN,BESAR_BIAYA,IS_TAGIH)
            VALUES
                ('{$id_mhs}','{$id_semester}','{$id_detail_biaya}','{$id_bank}','{$id_bank_via}','{$tgl_bayar}','{$keterangan}','{$besar_biaya}','T')");
    }

    function update($id_bank, $id_bank_via, $tgl_bayar, $keterangan, $besar_biaya, $nim_mhs, $semester, $id_biaya) {
        $this->db->Query("
            UPDATE PEMBAYARAN SET
                ID_BANK='{$id_bank}',
                ID_BANK_VIA='{$id_bank_via}',
                TGL_BAYAR='{$tgl_bayar}',
                BESAR_BIAYA='{$besar_biaya}',
                KETERANGAN='{$keterangan}',
                IS_TAGIH='T'
            WHERE ID_PEMBAYARAN IN (
                SELECT PEM.ID_PEMBAYARAN FROM MAHASISWA M
                JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
                JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA = PEM.ID_DETAIL_BIAYA
                WHERE PEM.ID_SEMESTER = '{$semester}' AND M.NIM_MHS='{$nim_mhs}' AND DB.ID_BIAYA ='{$id_biaya}'
                AND (PEM.TGL_BAYAR IS NOT NULL OR PEM.TGL_BAYAR IS NULL) AND PEM.ID_BANK IS NULL
            )");
    }

}

?>
