<?php

class utility {

    public $db;
    public $id_semester_sebelum;

    function __construct($db) {
        $this->db = $db;
        $this->id_semester_sebelum = $this->get_semester_kemarin();
    }

    function get_semester_kemarin() {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil'");
            $semester_kemarin = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1)");
            $semester_kemarin = $this->db->FetchAssoc();
        }
        return $semester_kemarin['ID_SEMESTER'];
    }

    function get_hasil_cari_mahasiswa($var) {
        $var_trim = trim($var);
        return $this->db->QueryToArray("
            SELECT P.NM_PENGGUNA,M.NIM_MHS,CMB.NO_UJIAN,F.NM_FAKULTAS,PS.NM_PROGRAM_STUDI FROM PENGGUNA P
            JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN CALON_MAHASISWA_BARU CMB ON M.ID_C_MHS = CMB.ID_C_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
            WHERE M.NIM_MHS LIKE '%{$var_trim}%' OR UPPER(P.NM_PENGGUNA) LIKE UPPER('%{$var}%') OR CMB.NO_UJIAN LIKE '%{$var_trim}%'
            ");
    }

    function detail_pembayaran($nim) {
        return $this->db->QueryToArray("
        SELECT * FROM 
            (SELECT M.ID_MHS AS ID1,M.NIM_LAMA,P.NM_PENGGUNA,NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,M.NIM_MHS,PEM.*,B.NM_BIAYA,J.NM_JENJANG,M.STATUS_CEKAL,JAL.NM_JALUR,S.THN_AKADEMIK_SEMESTER
            ,KB.NM_KELOMPOK_BIAYA,M.NIM_BANK,BNK.NM_BANK,BNK.ID_BANK AS ID_BNK,BNKV.ID_BANK_VIA AS ID_BNKV,BNKV.NAMA_BANK_VIA,S2.NM_SEMESTER,S2.TAHUN_AJARAN
            FROM PENGGUNA P
            LEFT JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN PROGRAM_STUDI PR ON M.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            LEFT JOIN (SELECT * FROM JALUR_MAHASISWA WHERE ROWNUM<=1 ORDER BY ID_JALUR_MAHASISWA DESC) JM ON JM.ID_MHS=M.ID_MHS
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR = JM.ID_JALUR
            LEFT JOIN SEMESTER S ON S.ID_SEMESTER = JM.ID_SEMESTER
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS
            LEFT JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
            LEFT JOIN SEMESTER S2 ON S2.ID_SEMESTER = PEM.ID_SEMESTER
            LEFT JOIN BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            LEFT JOIN BANK_VIA BNKV ON BNKV.ID_BANK_VIA = PEM.ID_BANK_VIA
            LEFT JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            LEFT JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
            ) X1
        LEFT JOIN
            (SELECT M.ID_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_SPP_TAGIH,SUM(PEM.DENDA_BIAYA) AS TOTAL_DENDA_BIAYA FROM PEMBAYARAN PEM
            LEFT JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
            WHERE PEM.TGL_BAYAR IS NULL AND PEM.IS_TAGIH ='Y'
            GROUP BY M.ID_MHS
            ) X2 
        ON X1.ID1 = X2.ID2
        LEFT JOIN
            (SELECT M.ID_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_SPP,SUM(PEM.DENDA_BIAYA) AS TOTAL_DENDA FROM PEMBAYARAN PEM
            LEFT JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
            GROUP BY M.ID_MHS
            ) X3 
        ON X1.ID1 = X3.ID2
        WHERE X1.NIM_MHS ='{$nim}'
        ORDER BY X1.NM_SEMESTER DESC,X1.TAHUN_AJARAN DESC,X1.ID_PEMBAYARAN");
    }

    function detail_pembayaran_cmhs($no_ujian) {
        $calon_mahasiswa = $this->db->QueryToArray("SELECT * FROM CALON_MAHASISWA WHERE NO_UJIAN ='{$no_ujian}'") == NULL ? "_BARU" : "";
        return $this->db->QueryToArray("
        SELECT * FROM 
            (SELECT CM.ID_C_MHS AS ID1,CM.NM_C_MHS,CM.NO_UJIAN,J.NM_JENJANG,PR.NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,PEM.*,B.NM_BIAYA,
            BNK.NM_BANK,BNK.ID_BANK AS ID_BNK,BNKV.ID_BANK_VIA AS ID_BNKV,BNKV.NAMA_BANK_VIA
            FROM CALON_MAHASISWA{$calon_mahasiswa} CM
            LEFT JOIN PROGRAM_STUDI PR ON CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS
            LEFT JOIN PEMBAYARAN_CMHS PEM ON CM.ID_C_MHS = PEM.ID_C_MHS
            LEFT JOIN SEMESTER S2 ON S2.ID_SEMESTER = PEM.ID_SEMESTER
            LEFT JOIN BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            LEFT JOIN BANK_VIA BNKV ON BNKV.ID_BANK_VIA = PEM.ID_BANK_VIA
            LEFT JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            LEFT JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
            ) X1
        LEFT JOIN
            (SELECT CM.ID_C_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_BIAYA_TAGIH
            FROM PEMBAYARAN_CMHS PEM
            LEFT JOIN CALON_MAHASISWA{$calon_mahasiswa} CM ON PEM.ID_C_MHS = CM.ID_C_MHS
            WHERE PEM.TGL_BAYAR IS NULL
            GROUP BY CM.ID_C_MHS
            ) X2 
        ON X1.ID1 = X2.ID2
        LEFT JOIN
            (SELECT CM.ID_C_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_BIAYA
            FROM PEMBAYARAN_CMHS PEM
            LEFT JOIN CALON_MAHASISWA{$calon_mahasiswa} CM ON PEM.ID_C_MHS = CM.ID_C_MHS
            GROUP BY CM.ID_C_MHS
            ) X3
        ON X1.ID1 = X3.ID2
        WHERE X1.NO_UJIAN ='{$no_ujian}'
        ORDER BY X1.ID_PEMBAYARAN_CMHS");
    }

    function get_detail_pembayaran_mahasiswa($nim) {
        return $this->db->QueryToArray("SELECT B.NM_BIAYA,P.* FROM PEMBAYARAN P
        JOIN DETAIL_BIAYA DB ON P.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
        JOIN MAHASISWA M ON P.ID_MHS = M.ID_MHS
        WHERE NIM_MHS ='{$nim}'
        ");
    }

    function get_detail_pembayaran($id_pembayaran) {
        $this->db->Query("SELECT B.NM_BIAYA,P.* FROM PEMBAYARAN P
        JOIN DETAIL_BIAYA DB ON P.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
        JOIN MAHASISWA M ON P.ID_MHS = M.ID_MHS
        WHERE P.ID_PEMBAYARAN ='{$id_pembayaran}'
        ");

        return $this->db->FetchAssoc();
    }

    function get_detail_pengguna($username) {
        $this->db->Query("
            SELECT * FROM PENGGUNA P
            LEFT JOIN ROLE R ON R.ID_ROLE = P.ID_ROLE
            WHERE P.USERNAME='{$username}'
        ");

        return $this->db->FetchAssoc();
    }

    function reset_password_pengguna($username, $pass) {
        $this->db->Query("UPDATE PENGGUNA SET SE1 = '{$pass}' WHERE USERNAME='{$username}'");
    }

    function cek_data($nim) {
        $this->db->Query("
        SELECT M.NIM_MHS,P.NM_PENGGUNA,J.ID_JENJANG,J.NM_JENJANG,S.ID_SEMESTER,S.THN_AKADEMIK_SEMESTER,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.ID_JALUR,J.NM_JALUR,
        KB.ID_KELOMPOK_BIAYA,KB.NM_KELOMPOK_BIAYA 
            FROM PENGGUNA P
            LEFT JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA 
            LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
            LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
            LEFT JOIN SEMESTER S ON JM.ID_SEMESTER = S.ID_SEMESTER 
            LEFT JOIN JALUR J ON JM.ID_JALUR = J.ID_JALUR
        WHERE M.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_pengguna($nim) {
        $this->db->Query("SELECT * FROM PENGGUNA WHERE USERNAME='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_mahasiswa($nim) {
        $this->db->Query("SELECT * FROM MAHASISWA WHERE NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_jalur($nim) {
        $this->db->Query("SELECT * FROM JALUR_MAHASISWA WHERE NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function insert_pengguna_mahasiswa($nama, $nim) {
        $this->db->Query("INSERT INTO PENGGUNA 
                (USERNAME,PASSWORD_PENGGUNA,NM_PENGGUNA,ID_ROLE,JOIN_TABLE) 
                VALUES 
                ('{$nim}','{$nim}','{$nama}',3,3)");
    }

    function insert_mahasiswa($id_pengguna, $nim, $program_studi, $kelompok_biaya) {
        $nim_bank = substr($nim, 0, 9);
        $this->db->Query("INSERT INTO MAHASISWA 
                (ID_PENGGUNA,NIM_MHS,ID_PROGRAM_STUDI,ID_KELOMPOK_BIAYA,NIM_BANK,STATUS_CEKAL) 
                VALUES 
                ('{$id_pengguna}','{$nim}','{$program_studi}','{$kelompok_biaya}','{$nim_bank}','0')");
    }

    function insert_jalur_mahasiswa($id_mhs, $program_studi, $jalur, $semester, $nim_mhs) {
        $this->db->Query("INSERT INTO JALUR_MAHASISWA 
                (ID_MHS,ID_PROGRAM_STUDI,ID_JALUR,ID_SEMESTER,NIM_MHS)
                VALUES
                ('{$id_mhs}','{$program_studi}','{$jalur}','{$semester}','{$nim_mhs}')");
    }

    function update_pengguna($nama, $nim) {
        $this->db->Query("UPDATE PENGGUNA SET NM_PENGGUNA ='{$nama}' WHERE USERNAME='{$nim}'");
    }

    function update_mahasiswa($id_kelompok_biaya, $nim, $id_program_studi) {
        $nim_bank = substr($nim, 0, 9);
        $this->db->Query("UPDATE MAHASISWA SET ID_KELOMPOK_BIAYA='{$id_kelompok_biaya}',NIM_BANK='{$nim_bank}',ID_PROGRAM_STUDI='{$id_program_studi}',STATUS_CEKAL=0 WHERE NIM_MHS='{$nim}'");
    }

    function update_jalur_mahasiswa($id_mhs, $program_studi, $jalur, $semester, $nim) {
        $this->db->Query("UPDATE JALUR_MAHASISWA SET ID_MHS = '{$id_mhs}', ID_PROGRAM_STUDI='{$program_studi}',ID_JALUR='{$jalur}',ID_SEMESTER='{$semester}' WHERE NIM_MHS='{$nim}'");
    }

    function get_jenjang() {
        return $this->db->QueryToArray("SELECT * FROM JENJANG ORDER BY NM_JENJANG,ID_JENJANG");
    }

    function get_semester() {
        return $this->db->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER = 'Ganjil' ORDER BY ID_SEMESTER");
    }

    function get_program_studi() {
        return $this->db->QueryToArray("SELECT * FROM PROGRAM_STUDI ORDER BY NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }

    function get_program_studi_by_jenjang($id_jenjang) {
        return $this->db->QueryToArray("SELECT * FROM PROGRAM_STUDI WHERE ID_JENJANG={$id_jenjang} ORDER BY NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }

    function get_jalur() {
        return $this->db->QueryToArray("SELECT * FROM JALUR ORDER BY NM_JALUR,ID_JALUR");
    }

    function get_kelompok() {
        return $this->db->QueryToArray("SELECT * FROM KELOMPOK_BIAYA ORDER BY NM_KELOMPOK_BIAYA,ID_KELOMPOK_BIAYA");
    }

    function get_bank() {
        return $this->db->QueryToArray("SELECT * FROM BANK ORDER BY NM_BANK,ID_BANK");
    }

    function get_via_bank() {
        return $this->db->QueryToArray("SELECT * FROM BANK_VIA ORDER BY NAMA_BANK_VIA,ID_BANK_VIA");
    }

    function get_biaya_kuliah_mahasiswa() {
        return $this->db->QueryToArray("
            SELECT S.THN_AKADEMIK_SEMESTER,S.NM_SEMESTER,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,BK.BESAR_BIAYA_KULIAH FROM BIAYA_KULIAH BK
            JOIN SEMESTER S ON BK.ID_SEMESTER = S.ID_SEMESTER
            JOIN PROGRAM_STUDI PS ON BK.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = BK.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR = BK.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON BK.ID_KELOMPOK_BIAYA = KB.ID_KELOMPOK_BIAYA
            ORDER BY PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,S.THN_AKADEMIK_SEMESTER
            ");
    }

    function get_semester_all() {
        return $this->db->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Ganjil' OR NM_SEMESTER='Genap' ORDER BY THN_AKADEMIK_SEMESTER DESC");
    }

    function cek_biaya_kuliah_mhs($nim) {
        $this->db->Query("
          SELECT T.NIM_MHS,T.ID_MHS, BK.ID_BIAYA_KULIAH,BK.BESAR_BIAYA_KULIAH FROM BIAYA_KULIAH BK
          RIGHT JOIN (
              SELECT M.NIM_MHS, M.ID_MHS, JM.ID_SEMESTER, M.ID_PROGRAM_STUDI, M.ID_KELOMPOK_BIAYA, JM.ID_JALUR FROM MAHASISWA M
              JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
              WHERE JM.ID_SEMESTER IS NOT NULL) T ON T.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
          WHERE
          BK.ID_KELOMPOK_BIAYA = T.ID_KELOMPOK_BIAYA AND BK.ID_SEMESTER = T.ID_SEMESTER AND BK.ID_JALUR = T.ID_JALUR
          AND T.ID_MHS NOT IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND T.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_pembayaran($nim) {
        $this->db->Query("
        SELECT M.NIM_MHS,BKM.ID_MHS, 5 AS ID_SEMESTER, DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, 'Y' AS IS_TAGIH FROM BIAYA_KULIAH_MHS BKM
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
            LEFT JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
            WHERE
	M.ID_MHS IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND
	M.ID_MHS NOT IN (SELECT DISTINCT ID_MHS FROM PEMBAYARAN) AND M.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function generate_pembayaran($nim) {
        $this->db->Query("
        INSERT INTO PEMBAYARAN (ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, BESAR_BIAYA, IS_TAGIH)
            SELECT BKM.ID_MHS, 5 AS ID_SEMESTER, DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, 'Y' AS IS_TAGIH FROM BIAYA_KULIAH_MHS BKM
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
            LEFT JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
            WHERE
        M.ID_MHS IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND
        M.ID_MHS NOT IN (SELECT DISTINCT ID_MHS FROM PEMBAYARAN) AND
        M.NIM_MHS ='{$nim}'");
    }

    function generate_biaya_kuliah($nim) {
        $this->db->Query("
        INSERT INTO BIAYA_KULIAH_MHS (ID_MHS, ID_BIAYA_KULIAH)
          SELECT T.ID_MHS, BK.ID_BIAYA_KULIAH FROM BIAYA_KULIAH BK
          RIGHT JOIN (
            SELECT M.NIM_MHS, M.ID_MHS, JM.ID_SEMESTER, M.ID_PROGRAM_STUDI, M.ID_KELOMPOK_BIAYA, JM.ID_JALUR FROM MAHASISWA M
            JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
            WHERE JM.ID_SEMESTER IS NOT NULL) T ON T.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
          WHERE
            BK.ID_KELOMPOK_BIAYA = T.ID_KELOMPOK_BIAYA AND BK.ID_SEMESTER = T.ID_SEMESTER AND BK.ID_JALUR = T.ID_JALUR
            AND T.ID_MHS NOT IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND
            T.NIM_MHS ='{$nim}'");
    }

    function load_sejarah_status_pembayaran($nim) {
        return $this->db->QueryToArray("
        SELECT SSP.ID_SEJARAH_STATUS_BAYAR,M.NIM_MHS,P.NM_PENGGUNA NM_MHS,S.NM_SEMESTER,S.TAHUN_AJARAN,SP.NAMA_STATUS,
                SSP.TGL_MULAI_BAYAR,SSP.TGL_JATUH_TEMPO,SSP.KETERANGAN,PU.NM_PENGGUNA NM_PENGUPDATE,SSP.IS_AKTIF
        FROM SEJARAH_STATUS_BAYAR SSP
        JOIN MAHASISWA M ON M.ID_MHS = SSP.ID_MHS
        JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
        JOIN SEMESTER S ON S.ID_SEMESTER = SSP.ID_SEMESTER
        JOIN STATUS_PEMBAYARAN SP ON SP.ID_STATUS_PEMBAYARAN= SSP.ID_STATUS_PEMBAYARAN
        JOIN PENGGUNA PU ON PU.ID_PENGGUNA = SSP.ID_PENGGUNA
        WHERE M.NIM_MHS='{$nim}'");
    }

    function insert_sejarah_status_pembayaran($id_mhs, $semester, $status, $tgl_mulai, $tgl_tempo, $keterangan, $id_update, $aktif) {
        $this->db->Query("
        INSERT INTO SEJARAH_STATUS_BAYAR
            (ID_MHS,ID_SEMESTER,ID_STATUS_PEMBAYARAN,TGL_MULAI_BAYAR,TGL_JATUH_TEMPO,KETERANGAN,ID_PENGGUNA,IS_AKTIF)
        VALUES
            ('{$id_mhs}','{$semester}','{$status}','{$tgl_mulai}','{$tgl_tempo}','{$keterangan}','{$id_update}','{$aktif}')");
        $this->db->Query("UPDATE PEMBAYARAN SET ID_STATUS_PEMBAYARAN='{$status}' WHERE ID_SEMESTER='{$semester}' AND ID_MHS='{$id_mhs}'");
    }

    function aktifkan_sejarah_status_pembayaran($id_sejarah_status) {
        $this->db->Query("SELECT * FROM SEJARAH_STATUS_BAYAR WHERE ID_SEJARAH_STATUS_BAYAR='{$id_sejarah_status}'");
        $data = $this->db->FetchAssoc();
        $aktif = $data['IS_AKTIF'] == '1' ? '0' : '1';
        $this->db->Query("UPDATE SEJARAH_STATUS_BAYAR SET IS_AKTIF='{$aktif}' WHERE ID_SEJARAH_STATUS_BAYAR='{$id_sejarah_status}'");
    }

    function hapus_sejarah_status_pembayaran($id_status) {
        $this->db->Query("DELETE FROM SEJARAH_STATUS_BAYAR WHERE ID_SEJARAH_STATUS_BAYAR='{$id_status}'");
    }

    // UNTUK SEMESTER PENDEK
    function load_mahasiswa_sp($fakultas,$semester) {
        $mahasiswa_sp = array();
        $this->db->Open();
        $row = $this->db->QueryToArray("
        SELECT M.ID_MHS,M.NIM_MHS,UPPER(P.NM_PENGGUNA) AS NM_PENGGUNA,M.THN_ANGKATAN_MHS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,
                PMK.STATUS_APPROVE,PMK.STATUS_PENGAMBILAN,M.ID_PROGRAM_STUDI
            FROM AUCC.MAHASISWA M
            JOIN AUCC.PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN AUCC.STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA=M.STATUS_AKADEMIK_MHS
           JOIN (
              SELECT ID_MHS,ID_SEMESTER,
                AVG(STATUS_APV_PENGAMBILAN_MK) STATUS_APPROVE,
                AVG(STATUS_PENGAMBILAN_MK) STATUS_PENGAMBILAN
                FROM AUCC.PENGAMBILAN_MK
                WHERE ID_KELAS_MK IS NOT NULL
              GROUP BY ID_MHS,ID_SEMESTER
            ) PMK ON PMK.ID_MHS = M.ID_MHS
        WHERE SP.STATUS_AKTIF=1 AND PMK.ID_SEMESTER=126 AND PS.ID_FAKULTAS=4
        ORDER BY THN_ANGKATAN_MHS,NM_PENGGUNA");
        foreach ($row as $temp) {
            if ($temp['WALI_KE'] == '1') {
                $mhs_status = $this->get_mhs_status($temp['ID_MHS']);
                $ips_mhs = $this->get_ips_mhs($temp['ID_MHS'], $this->id_semester_sebelum);
                array_push($mahasiswa_sp, array(
                    'ID_MHS' => $temp['ID_MHS'],
                    'NIM' => $temp['NIM_MHS'],
                    'NAMA' => $temp['NM_PENGGUNA'],
                    'IPS' => $ips_mhs,
                    'IPK' => $mhs_status['IPK'],
                    'TOTAL_SKS' => $mhs_status['SKSTOTAL'],
                    'ANGKATAN' => $temp['THN_ANGKATAN_MHS'],
                    'STATUS' => $temp['STATUS_PENGAMBILAN'],
                    'STATUS_APV' => $temp['STATUS_APPROVE']
                ));
            }
        }
        return $mahasiswa_sp;
    }

    function get_mhs_status($id_mhs) {
        $this->db->Query("SELECT PS.ID_FAKULTAS FROM MAHASISWA M JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI WHERE M.ID_MHS='{$id_mhs}'");
        $id_fakultas_mhs = $this->db->FetchAssoc();
        // ngulang -> nilai terakhir yg berlaku
        if ($id_fakultas_mhs['ID_FAKULTAS'] == 10) {
            $sql = "select a.id_mhs,sum(a.kredit_semester) skstotal,
                    round(sum(a.kredit_semester*(case a.nilai_huruf 
                    when 'A' then 4 
                    when 'AB' then 3.5 
                    when 'B' then 3
                    when 'BC' then 2.5
                    when 'C' then 2
                    when 'D' then 1
                    end))/sum(a.kredit_semester),2) IPK
                    from
                    (
                    select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
                    select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester desc,sm.nm_semester desc) rangking
                    from pengambilan_mk a 
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    left join semester sm on sm.id_semester=a.id_semester
                    where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.flagnilai=1 and a.id_semester is not null
                    ) a
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    where rangking=1 and id_mhs='{$id_mhs}'
                    ) a
                    left join mahasiswa b on a.id_mhs=b.id_mhs
                    left join program_studi f on b.id_program_studi=f.id_program_studi
                    where a.id_mhs='{$id_mhs}'
                    group by a.id_mhs order by a.id_mhs";
            $this->db->Query($sql);
            $mhs_status = $this->db->FetchAssoc();
        } else {
            // ipk versi lukman, ngulang diambil nilai terbaik.
            $sql = "select a.id_mhs,sum(a.kredit_semester) skstotal,
                    round(sum(a.kredit_semester*(case a.nilai_huruf 
                    when 'A' then 4 
                    when 'AB' then 3.5 
                    when 'B' then 3
                    when 'BC' then 2.5
                    when 'C' then 2
                    when 'D' then 1
                    end))/sum(a.kredit_semester),2) IPK
                    from
                    (
                    select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
                    select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
                    from pengambilan_mk a 
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null 
                    ) a
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    where rangking=1 and id_mhs='{$id_mhs}'
                    ) a
                    left join mahasiswa b on a.id_mhs=b.id_mhs
                    left join program_studi f on b.id_program_studi=f.id_program_studi
                    where a.id_mhs='{$id_mhs}'
                    group by a.id_mhs order by a.id_mhs";
            $this->db->Query($sql);
            $mhs_status = $this->db->FetchAssoc();
        }
        return $mhs_status;
    }

    function get_ips_mhs($id_mhs, $id_semester) {
        $this->db->Query("
            select id_mhs,case when sum(bobot*kredit_semester)=0 then 0 else 
            round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
            case when id_fakultas=7 then sum(sksreal) else sum(kredit_semester) end as sks_sem 
            from 
            (select id_mhs,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
            (select a.id_mhs,ps.id_fakultas,a.id_kurikulum_mk, 
            case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
            and d.status_mkta in (1,2) then 0
            else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
            case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
            case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
            from pengambilan_mk a
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
            left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
            left join mahasiswa m on a.id_mhs=m.id_mhs
            left join program_studi ps on m.id_program_studi=ps.id_program_studi
            left join semester s on a.id_semester=s.id_semester
            where group_semester||thn_akademik_semester in
            (select group_semester||thn_akademik_semester from semester where id_Semester='" . $id_semester . "')
            and tipe_semester in ('UP','REG','RD') 
            and a.status_apv_pengambilan_mk='1' and m.id_mhs='" . $id_mhs . "' and a.status_hapus=0 and a.flagnilai='1'
            and a.status_pengambilan_mk !=0
            )
            group by id_mhs, id_kurikulum_mk, id_fakultas, kredit_semester, sksreal
            )
            group by id_mhs,id_fakultas
            ");
        $data = $this->db->FetchAssoc();
        return $data['IPS'];
    }

}

?>
