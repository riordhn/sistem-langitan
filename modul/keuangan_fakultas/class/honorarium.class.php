<?php

class honorarium {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    //fungsi Honorarium
    function load_honorarium($fakultas) {
        return $this->db->QueryToArray("
            SELECT H.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JH.NM_JENIS_HONOR,G.NM_GOLONGAN,G.NM_PANGKAT,PA.NAMA_PENDIDIKAN_AKHIR
            FROM HONORARIUM H
            JOIN JENIS_HONOR JH ON JH.ID_JENIS_HONOR=H.ID_JENIS_HONOR
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=H.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN GOLONGAN G ON G.ID_GOLONGAN=H.ID_GOLONGAN
            JOIN PENDIDIKAN_AKHIR PA ON PA.ID_PENDIDIKAN_AKHIR=H.ID_PENDIDIKAN_AKHIR
            WHERE PS.ID_FAKULTAS='{$fakultas}'
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JH.NM_JENIS_HONOR
            ");
    }

    function get_honorarium($id) {
        $this->db->Query("
            SELECT H.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JH.NM_JENIS_HONOR,G.NM_GOLONGAN,G.NM_PANGKAT,PA.NAMA_PENDIDIKAN_AKHIR
            FROM HONORARIUM H
            JOIN JENIS_HONOR JH ON JH.ID_JENIS_HONOR=H.ID_JENIS_HONOR
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=H.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            JOIN GOLONGAN G ON G.ID_GOLONGAN=H.ID_GOLONGAN
            JOIN PENDIDIKAN_AKHIR PA ON PA.ID_PENDIDIKAN_AKHIR=H.ID_PENDIDIKAN_AKHIR
            WHERE H.ID_HONORARIUM='{$id}'");
        return $this->db->FetchAssoc();
    }

    function add_honorarium($jenis_honor, $prodi, $jenis_kelas, $status_kelas, $golongan, $pendidikan, $tarif) {
        $this->db->Query("
            INSERT INTO HONORARIUM
                (ID_JENIS_HONOR,ID_PROGRAM_STUDI,JENIS_KELAS,STATUS_KELAS,ID_GOLONGAN,ID_PENDIDIKAN_AKHIR,BESAR_TARIF)
            VALUES
                ('{$jenis_honor}','{$prodi}','{$jenis_kelas}','{$status_kelas}','{$golongan}','{$pendidikan}','{$tarif}')");
    }

    function edit_honorarium($id, $jenis_honor, $prodi, $jenis_kelas, $status_kelas, $golongan, $pendidikan, $tarif) {
        $this->db->Query("
            UPDATE HONORARIUM
                SET
                    ID_JENIS_HONOR='{$jenis_honor}',
                    ID_PROGRAM_STUDI='{$prodi}',
                    JENIS_KELAS='{$jenis_kelas}',
                    STATUS_KELAS='{$status_kelas}',
                    ID_GOLONGAN='{$golongan}',
                    ID_PENDIDIKAN_AKHIR='{$pendidikan}',
                    BESAR_TARIF='{$tarif}'
            WHERE ID_HONORARIUM='{$id}'");
    }

}

?>
