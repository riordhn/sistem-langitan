<div class="center_title_bar">Master Biaya SKS</div>
<form id="form_add" method="post" action="biaya-sks.php">
    <table class="ui-widget" style="width: 60%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Edit Biaya SKS</th>
        </tr>
        <tr class="ui-widget-content">
            <td style="width: 20%">Fakultas</td>
            <td>
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        {if $id_fakultas==$data.ID_FAKULTAS}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>
                <select name="prodi" id="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$data_biaya_sks.ID_PROGRAM_STUDI}selected="true"{/if}>( {$data.NM_JENJANG} ) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester</td>
            <td>
                <select name="semester" id="semester">
                    <option value="">Semua</option>
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$data_biaya_sks.ID_SEMESTER}selected="true"{/if}>{$data.NM_SEMESTER} {$data.GROUP_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jalur</td>
            <td>
                <select name="jalur" id="jalur" class="required">
                    <option value="">Pilih</option>
                    {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $data.ID_JALUR==$data_biaya_sks.ID_JALUR}selected="true"{/if}>{$data.NM_JALUR}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Besar Biaya SKS</td>
            <td>
                RP. <input name="besar_biaya" class="required number" value="{$data_biaya_sks.BESAR_BIAYA_SP}" />
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Keterangan Biaya SKS</td>
            <td>
                <textarea name="keterangan" >{$data_biaya_sks.KETERANGAN_BIAYA_SP}</textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="hidden" name="id_biaya_sks" value="{$data_biaya_sks.ID_BIAYA_SP}"/>
                <input type="hidden" name="mode" value="edit"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Update"/>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="biaya-sks.php">Cancel</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#form_add').validate();
        $('#fakultas').change(function(){
            $.ajax({
                    type:'post',
                    url:'getProdi.php',
                    data:'id_fakultas='+$('#fakultas').val(),
                    success:function(data){
                            $('#prodi').html(data);
                    }                    
            })
        });
    </script>
{/literal}