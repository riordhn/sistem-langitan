<div class="center_title_bar">Master Biaya SKS</div>
<form id="form_add" method="post" action="biaya-sks.php">
    <table class="ui-widget" style="width: 60%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Tambah Biaya SKS</th>
        </tr>
        <tr class="ui-widget-content">
            <td style="width: 20%">Fakultas</td>
            <td>
                <select name="fakultas" id="fakultas">
                    {foreach $data_fakultas as $data}
                        {if $id_fakultas==$data.ID_FAKULTAS}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>
                <select name="prodi" id="prodi" class="required">
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$prodi}selected="true"{/if}>( {$data.NM_JENJANG} ) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester</td>
            <td>
                <select name="semester" id="semester" class="required">
                    <option value="">Semua</option>
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$semester}selected="true"{/if}>{$data.NM_SEMESTER} {$data.GROUP_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jalur</td>
            <td>
                <select name="jalur" id="jalur" class="required">
                    <option value="">Pilih</option>
                    {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $data.ID_JALUR==$jalur}selected="true"{/if}>{$data.NM_JALUR}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Besar Biaya SKS</td>
            <td>
                RP. <input name="besar_biaya" class="required number" />
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Keterangan Biaya SKS</td>
            <td>
                <textarea name="keterangan"></textarea>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="add"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Tambah"/>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="biaya-sks.php">Cancel</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#form_add').validate();
        $('#fakultas').change(function(){
            $.ajax({
                    type:'post',
                    url:'getProdi.php',
                    data:'id_fakultas='+$('#fakultas').val(),
                    success:function(data){
                            $('#prodi').html(data);
                    }                    
            })
        });
    </script>
{/literal}