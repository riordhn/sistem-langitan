<div class="center_title_bar">Master Biaya SKS</div>
<a class="ui-widget ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;margin-left:15px;" href="biaya-sks.php?mode=add">Tambah</a>
<table class="ui-widget" style="width: 80%">
    <tr class="ui-widget-header">
        <th colspan="8" class="header-coloumn" style="text-align: center;">Master Biaya SKS</th>
    </tr>
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Fakultas</th>
        <th>Program Studi</th>
        <th>Semester</th>
        <th>Jalur</th>
        <th>Besar Biaya SKS</th>
        <th>Keterangan Biaa SKS</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_biaya_sks as $data}
        <tr class="ui-widget-content">
            <td>{$data@index+1}</td>
            <td>{$data.NM_FAKULTAS}</td>
            <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
            <td>{$data.NM_SEMESTER} {$data.GROUP_SEMESTER} ({$data.TAHUN_AJARAN})</td>
            <td>{$data.NM_JALUR}</td>
            <td>{number_format($data.BESAR_BIAYA_SP)}</td>
            <td>{$data.KETERANGAN_BIAYA_SP}</td>
            <td style="width: 120px" class="center">
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="biaya-sks.php?mode=edit&id_biaya_sks={$data.ID_BIAYA_SP}">Edit</a>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="biaya-sks.php?mode=delete&id_biaya_sks={$data.ID_BIAYA_SP}">Delete</a>
            </td>
        </tr>
    {/foreach}
</table>	

