<div class="center_title_bar">Master Biaya SKS</div>
<form id="form_add" method="post" action="biaya-sks.php">
    <table class="ui-widget" style="width: 60%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Delete Biaya SKS</th>
        </tr>
        <tr class="ui-widget-content">
            <td style="width: 20%">Fakultas</td>
            <td>
                {foreach $data_fakultas as $data}
                    {if $data.ID_FAKULTAS==$id_fakultas}
                        {$data.NM_FAKULTAS}
                    {/if}
                {/foreach}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>
                {foreach $data_prodi as $data}
                    {if $data.ID_PROGRAM_STUDI==$data_biaya_sks.ID_PROGRAM_STUDI}
                        ( {$data.NM_JENJANG} ) {$data.NM_PROGRAM_STUDI}
                    {/if}
                {/foreach}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester</td>
            <td>
                {foreach $data_semester as $data}
                    {if $data.ID_SEMESTER==$data_biaya_sks.ID_SEMESTER}{$data.NM_SEMESTER} {$data.GROUP_SEMESTER} ( {$data.TAHUN_AJARAN} ){/if}
                {/foreach}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jalur</td>
            <td>
                {foreach $data_jalur as $data}
                    {if $data.ID_JALUR==$data_biaya_sks.ID_JALUR}
                        {$data.NM_JALUR}
                    {/if}
                {/foreach}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Besar Biaya SKS</td>
            <td>
                RP. {$data_biaya_sks.BESAR_BIAYA_SP}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Keterangan Biaya SKS</td>
            <td>
                {$data_biaya_sks.KETERANGAN_BIAYA_SP}
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <p>Apakah anda yakin menghapus data ini?</p>
                <input type="hidden" name="id_biaya_sks" value="{$data_biaya_sks.ID_BIAYA_SP}"/>
                <input type="hidden" name="mode" value="delete"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Hapus"/>
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="biaya-sks.php">Cancel</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#form_add').validate();
        $('#fakultas').change(function() {
            $.ajax({
                type: 'post',
                url: 'getProdi.php',
                data: 'id_fakultas=' + $('#fakultas').val(),
                success: function(data) {
                    $('#prodi').html(data);
                }
            })
        });
    </script>
{/literal}