{if $smarty.get.mode=='add'}
    <form id="form_add" method="post" action="periode-sp.php">
        <table class="ui-widget" style="width: 90%">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn" style="text-align: center;">Tambah Periode Semester Pendek</th>
            </tr>
            <tr class="ui-widget-content">
                <td>Fakultas</td>
                <td>
                    <select name="fakultas">
                        <option class="required" value="">Pilih Fakultas</option>
                        {foreach $data_fakultas as $f}
                            {if $f.ID_FAKULTAS==$id_fakultas}
                                <option value="{$f.ID_FAKULTAS}">{$f.NM_FAKULTAS}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Semester</td>
                <td>
                    <select name="semester">
                        {foreach $data_semester as $s}
                            <option value="{$s.ID_SEMESTER}">{$s.NM_SEMESTER} {$s.GROUP_SEMESTER} {$s.TAHUN_AJARAN}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Jenjang</td>
                <td>
                    <select name="jenjang">
                        {foreach $data_jenjang as $j}
                            <option value="{$j.ID_JENJANG}">{$j.NM_JENJANG}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Tanggal Awal</td>
                <td>
                    <input type="text" style="text-transform: uppercase" name="tgl_awal" class="required datepicker" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Tanggal Akhir</td>
                <td>
                    <input type="text" style="text-transform: uppercase" name="tgl_akhir" class="required datepicker" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Keterangan</td>
                <td>
                    <textarea name="keterangan"></textarea>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td colspan="2" class="center">
                    <input type="hidden" name="mode" value="add"/>
                    <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Tambah"/>
                </td>
            </tr>
        </table>
    </form>
    {literal}
        <script type="text/javascript">
                $('#form_add').validate();            
                $('#form_add').submit(function(){
                    if($('#form_add').valid()){
                        $('#dialog-add').dialog('close');
                    }
                    else{
                        return false;
                    }
                });
                $('.datepicker').datepicker({ dateFormat: "dd-M-y" });
        </script>
    {/literal}
{else if $smarty.get.mode=='edit'}
    <form id="form_edit" method="post" action="periode-sp.php">
        <table class="ui-widget" style="width: 90%">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn" style="text-align: center;">Tambah Periode Semester Pendek</th>
            </tr>
            <tr class="ui-widget-content">
                <td>Fakultas</td>
                <td>
                    <select name="fakultas">
                        <option class="required" value="">Pilih Fakultas</option>
                        {foreach $data_fakultas as $f}
                            {if $f.ID_FAKULTAS==$id_fakultas}
                                <option value="{$f.ID_FAKULTAS}" {if $periode.ID_FAKULTAS==$f.ID_FAKULTAS}selected="true"{/if}>{$f.NM_FAKULTAS}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Semester</td>
                <td>
                    <select name="semester">
                        {foreach $data_semester as $s}
                            <option value="{$s.ID_SEMESTER}"{if $periode.ID_SEMESTER==$s.ID_SEMESTER}selected="true"{/if}>{$s.NM_SEMESTER} {$s.GROUP_SEMESTER} {$s.TAHUN_AJARAN}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Jenjang</td>
                <td>
                    <select name="jenjang">
                        {foreach $data_jenjang as $j}
                            <option value="{$j.ID_JENJANG}" {if $periode.ID_JENJANG==$j.ID_JENJANG}selected="true"{/if}>{$j.NM_JENJANG}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Tanggal Awal</td>
                <td>
                    <input type="text" style="text-transform: uppercase" name="tgl_awal" value="{$periode.TGL_AWAL_PERIODE_BAYAR}" class="required datepicker" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Tanggal Akhir</td>
                <td>
                    <input type="text" style="text-transform: uppercase" name="tgl_akhir" value="{$periode.TGL_AKHIR_PERIODE_BAYAR}" class="required datepicker" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Keterangan</td>
                <td>
                    <textarea name="keterangan">{$periode.KET_PERIODE_BAYAR}</textarea>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td colspan="2" class="center">
                    <input type="hidden" name="mode" value="edit"/>
                    <input type="hidden" name="id" value="{$periode.ID_PERIODE_BAYAR_SP}"/>
                    <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Update"/>
                </td>
            </tr>
        </table>
    </form>
    {literal}
        <script type="text/javascript">
                $('#form_edit').validate();            
                $('#form_edit').submit(function(){
                    if($('#form_edit').valid()){
                        $('#dialog-edit').dialog('close');
                    }
                    else{
                        return false;
                    }
                });
                $('.datepicker').datepicker({ dateFormat: "dd-M-y" });
        </script>
    {/literal}
{else}
    <div class="center_title_bar">Periode Bayar Semester Pendek</div>
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="8" class="header-coloumn" style="text-align: center;">Master Tarif Honor</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>Fakultas</th>
            <th>Semester</th>
            <th>Jenjang</th>
            <th>Tanggal Awal</th>
            <th>Tanggal Akhir</th>
            <th>Keterangan</th>
            <th>Operasi</th>
        </tr>
        {foreach $data_periode as $dp}
            <tr class="ui-widget-content">
                <td class="center">{$dp@index+1}</td>
                <td class="center">{$dp.NM_FAKULTAS}</td>
                <td>{$dp.NM_SEMESTER} {$dp.GROUP_SEMESTER} {$dp.TAHUN_AJARAN}</td>
                <td>{$dp.NM_JENJANG}</td>
                <td>{$dp.TGL_AWAL_PERIODE_BAYAR}</td>
                <td>{$dp.TGL_AKHIR_PERIODE_BAYAR}</td>
                <td>{$dp.KET_PERIODE_BAYAR}</td>
                <td>
                    <span class="ui-widget ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;margin-left:15px;" 
                          onclick="$('#dialog-edit').dialog('open').load('periode-sp.php?mode=edit&id={$dp.ID_PERIODE_BAYAR_SP}')">Edit</span>
                </td>
            </tr>
        {foreachelse}
            <tr class="ui-widget-content">
                <th colspan="8" class="data-kosong" style="text-align: center;">Data Kosong</th>
            </tr>
        {/foreach}
        <tr class="ui-widget-content">
            <td colspan="8" class="center">
                <span class="ui-widget ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;margin-left:15px;margin-top:5px;" 
                      onclick="$('#dialog-add').dialog('open').load('periode-sp.php?mode=add')">Tambah</span>
            </td>
        </tr>
    </table>	
    <div id="dialog-add" title="Tambah Periode Bayar"></div>
    <div id="dialog-edit" title="Edit Periode Bayar"></div>
{/if}
{literal}
    <script type="text/javascript">
        $('#dialog-add').dialog({
            width:'40%',
            modal: true,
            resizable:false,
            autoOpen:false
        });
        $('#dialog-edit').dialog({
            width:'40%',
            modal: true,
            resizable:false,
            autoOpen:false
        });
    </script>
{/literal}
