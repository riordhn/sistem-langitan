<div class="center_title_bar">Pembayaran Semester Pendek Mahasiswa</div>
<form method="get" id="form" action="tagihan-sp.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Input Form</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester Pendek</td>
            <td>
                <select name="semester">
                    <option>Pilih</option>
                    {foreach $semester_sp as $s}
                        <option value="{$s.ID_SEMESTER}">{$s.NM_SEMESTER} {$s.GROUP_SEMESTER} {$s.TAHUN_AJARAN}</option>
                    {/foreach}
                </select>
            </td>
            <td>
                <input type="hidden" name="mode" value="tampil"/>
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" value="Tamplikan"/>
            </td>
        </tr>
    </table>
</form>