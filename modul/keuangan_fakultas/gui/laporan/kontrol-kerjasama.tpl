{literal}
    <style>
        .ui-widget-content a{
            text-decoration: none;
            color: brown;
        }
        .ui-widget-content a:hover{
            color: #f09a14;
        }
    </style>
{/literal}
<div class="center_title_bar">Laporan Kontrol Kerjasama</div> 
</div>
<form method="get" id="report_form" action="kontrol-kerjasama.php">
    <table class="ui-widget" style="width:90%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td width="15%">Program Studi</td>
            <td>
                <select name="prodi" id="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$smarty.get.prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenis Beasiswa</td>
            <td>
                <select name="beasiswa">
                    {foreach $data_kerjasama as $data}
                        <option value="{$data['ID_BEASISWA']}" {if $data['ID_BEASISWA']==$smarty.get.beasiswa}selected="true"{/if}>{$data['NM_BEASISWA']} {$data['PENYELENGGARA_BEASISWA']} {$data['NM_GROUP_BEASISWA']}</option>
                    {/foreach}
                </select>
            </td>
            <td></td>
            <td></td>
        </tr>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="4">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
                <span class="ui-button ui-corner-all ui-state-hover" onclick="window.open('upload-kerjasama.php','_blank')"  style="padding:5px;cursor:pointer;">Input Data Kerjasama</span>
            </td>
        </tr>

    </table>
</form>

{if isset($data_mahasiswa_kerjasama)}
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="9">LAPORAN KERJASAMA MAHASISWA</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Prodi</th>
            <th>Jalur</th>
            <th>Jenis Beasiswa</th>
            <th>Semester Mulai</th>
            <th>Semester Selesai</th>
            <th>Keterangan</th>
        </tr>
        {foreach $data_mahasiswa_kerjasama as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td><a href="history-bayar.php?cari={$data.NIM_MHS}">{$data.NIM_MHS}</a></td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                <td class="center">{$data.NM_JALUR}</td>
                <td class="center">{$data.NM_BEASISWA} Dari {$data.PENYELENGGARA_BEASISWA} Tahun {$data.PERIODE_PEMBERIAN_BEASISWA}</td>
                <td class="center">{$data.SEMESTER_MULAI}</td>
                <td class="center">{$data.SEMESTER_SELESAI}</td>
                <td class="center">{$data.KETERANGAN}</td>
            </tr>
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="9" class="center"><span style="color: red">Data Kosong</span></td>
            </tr>
        {/foreach}
        <tr class="ui-widget-content">
            <td class="link center" colspan="9">
<!--                <span class="link_button ui-corner-all" onclick="window.location.href='excel-kontrol-status.php?{$smarty.server.QUERY_STRING}'" style="padding:5px;cursor:pointer;">Excel</span>-->
            </td>	
        </tr>
    </table>
{/if}

{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}