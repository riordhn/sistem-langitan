{if empty($data_report_fakultas)&&$smarty.get.mode!='fakultas'}
    <div class="center_title_bar">Laporan Pengembalian Pembayaran Mahasiswa</div> 
    <div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                Tanggal Harus Diisi.</p>
        </div>
    </div>
    <form method="get" id="report_form" action="laporan-pengembalian.php">
        <table class="ui-widget" style="width:90%">
            <tr class="ui-widget-header">
                <th colspan="4" class="header-coloumn">Parameter</th>
            </tr>
            <tr class="ui-widget-content">
                <td width="15%">Tanggal Awal</td>
                <td width="15%"><input type="text" id="tgl_awal" class="datepicker"  name="tgl_awal" {if isset($tgl_awal)} value="{$tgl_awal}"{/if} /></td>
                <td width="15%">Fakultas</td>
                <td width="55%">
                    <select name="fakultas" id="fakultas">
                        <option value="">Semua</option>
                        {foreach $data_fakultas as $data}
                            {if $id_fakultas_user==$data.ID_FAKULTAS}
                                <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Tanggal Akhir</td>
                <td><input type="text" id="tgl_akhir" class="datepicker" class="required" name="tgl_akhir" {if isset($tgl_awal)} value="{$tgl_akhir}"{/if}/></td>
                <td>Program Studi</td>
                <td>
                    <select name="prodi" id="prodi">
                        <option value="">Semua</option>
                        {foreach $data_prodi as $data}
                            <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            </tr>
            <tr class="center ui-widget-content">
                <td colspan="4">
                    <input type="hidden" name="mode" value="bank" />
                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
                </td>
            </tr>

        </table>
    </form>
{/if}
{if isset($data_report_bank)}
    <table class="ui-widget" style="width:90%">
        <tr class="ui-widget-header">
            <th colspan="5">
                Rekapitulasi Pengembalian Pembayaran Mahasiswa
                {if $fakultas!=''}
                    <br/>
                    Fakultas {$data_fakultas_one.NM_FAKULTAS}
                {/if}
                {if $prodi!=''}
                    <br/>
                    Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
                {/if}
                <br/>
                Pada Tanggal {$tgl_awal} sampai {$tgl_akhir}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NAMA BANK</th>
            <th>JUMLAH MAHASISWA</th>
            <th>JUMLAH PEMBAYARAN</th>
            <th>OPERASI</th>
        </tr>
        {foreach $data_report_bank as $data}
            <tr class="center ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.NM_BANK}</td>
                <td>{$data.JUMLAH_MHS}</td>
                <td>{number_format($data.PENGEMBALIAN)}</td>
                <td>
                    {$total_mhs=$total_mhs+$data.JUMLAH_MHS}
                    {$total_pengembalian=$total_pengembalian+$data.PENGEMBALIAN}
                    <a class="ui-button ui-corner-all ui-state-hover" href="laporan-pengembalian.php?{$smarty.server.QUERY_STRING|replace:'bank':'detail'}&bank={$data.ID_BANK}" style="padding:5px;cursor:pointer;">Detail</a>
                    {if $prodi==''}
                        <span class="ui-button ui-corner-all ui-state-hover" onclick="window.open('laporan-pengembalian.php?{$smarty.server.QUERY_STRING|replace:'bank':'fakultas'}&bank={$data.ID_BANK}', '_blank')"  style="padding:5px;cursor:pointer;">Fakultas</span>
                    {/if}
                </td>
            </tr>
        {/foreach}
        <tr class="center ui-widget-content">
            <td class="total" colspan="2">Jumlah</td>
            <td class="total" >{$total_mhs}</td>
            <td class="total" >{number_format($total_pembayaran)}</td>
            <td class="total" >
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="pembayaran-mhs.php?mode=detail&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}">Detail</a>
                {if $prodi==''}
                    <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" onclick="window.open('laporan-pengembalian.php?mode=fakultas&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}', '_blank')">Fakultas</span>
                {/if}
            </td>
        </tr>
    </table>
{else if isset($data_report_detail)}
    <table class="ui-widget-content" style="width:90%">
        <tr class="ui-widget-header">
            <th colspan="13">
                Rekapitulasi Pengembalian Pembayaran Mahasiswa
                {if $fakultas!=''}
                    <br/>
                    Fakultas {$data_fakultas_one.NM_FAKULTAS}
                {/if}
                {if $prodi!=''}
                    <br/>
                    Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
                {/if}
                <br/>
                Pada Tanggal {$tgl_awal} sampai {$tgl_akhir}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Prodi</th>
            <th>Status</th>
            <th>Semester Pembayaran</th>
            <th>Besar Pembayaran</th>
            <th>Semester Pengembalian</th>
            <th>Besar Pengembalian</th>
            <th>Tgl Pengembalian</th>
            <th>Bank</th>
            <th>Keterangan</th>
            <th>Detail</th>
        </tr>
        {foreach $data_report_detail as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                <td>{$data.NM_STATUS_PENGGUNA}</td>
                <td>{$data.NM_SEMESTER_BAYAR} ({$data.TAHUN_AJARAN_BAYAR})</td>
                <td>{number_format($data.BESAR_PEMBAYARAN)}</td>
                <td>{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
                <td>{number_format($data.PENGEMBALIAN)}</td>
                <td>{$data.TGL_PENGEMBALIAN}</td>
                <td>{$data.NM_BANK}</td>
                <td>{$data.KETERANGAN}</td>
                <td class="center">
                    <a href="history-bayar.php?cari={$data.NIM_MHS}"class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Detail</a>
                </td>
            </tr>
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="13" class="center"><span style="color: red">Data Kosong</span></td>
            </tr>
        {/foreach}
        <!--
        <tr class="center ui-widget-content">
            <td class="link center" colspan="9">
                <span class="link_button ui-corner-all" onclick="window.location.href='excel-pembayaran-mhs-detail.php?tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&bank={$bank}'" style="padding:5px;cursor:pointer;">Excel</span>
            </td>	
        </tr>
        -->
    </table>
    <span class="ui-button ui-corner-all ui-state-hover" onclick="history.back(-1)" style="padding:5px;cursor:pointer;margin:10px;">Kembali</span>
{else if isset($data_report_fakultas)}
    {literal}
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
        <link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
        <style>           
            .ui-widget-content a{
                text-decoration: none;
            }
            .ui-widget-content a:hover{
                color: #f09a14;
            }
        </style>
    {/literal}
    <table class="ui-widget" style="width:250%">
        <tr class="ui-widget-header">
            <th colspan="{$count_data_biaya+5}">
                Rekapitulasi Pengembalian Pembayaran Mahasiswa
                {if $fakultas!=''}
                    <br/>
                    Fakultas {$data_fakultas_one.NM_FAKULTAS}
                {/if}
                {if $prodi!=''}
                    <br/>
                    Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
                    <br/>
                    Jalur {$data_jalur_one.NM_JALUR}
                {/if}
                <br/>
                Pada Tanggal {$tgl_awal} sampai {$tgl_akhir}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>
                {if $prodi!=''}
                    Nama
                {else if $fakultas!=''}
                    Program Studi
                {else}
                    Fakultas
                {/if}
            </th>
            {if $prodi!=''}
                <th>NIM</th>
                {/if}
            <th>
                {if $prodi!=''}
                    Program Studi
                {else}
                    Jumlah MHS
                {/if}
                {foreach $data_biaya as $data}
                <th>{$data.NM_BIAYA}</th>
                {/foreach}
            <th>Total Pengembalian</th>
        </tr>
        {foreach $data_report_fakultas as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>
                    {if $prodi!=''}
                        {$data.NM_PENGGUNA}
                    {else if $fakultas!=''}
                        <a href="laporan-pengembalian.php?mode=fakultas&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$data.ID_PROGRAM_STUDI}&bank={$bank}">
                            ({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}
                        </a>
                    {else}
                        <a href="laporan-pengembalian.php?mode=fakultas&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$data.ID_FAKULTAS}&prodi={$prodi}&bank={$bank}">
                            {$data.NM_FAKULTAS}
                        </a>
                    {/if}    
                </td>
                {if $prodi!=''}
                    <td>{$data.NIM_MHS}</td>
                {/if}
                <td class="center">
                    {if $prodi!=''}
                        {$data.NM_PROGRAM_STUDI}
                    {else}    
                        {$data.JUMLAH_MHS}
                        {$total_mhs=$total_mhs+$data.JUMLAH_MHS}
                    {/if}
                </td>
                {$total_pembayaran_right=0}
                {foreach $data.DATA_PENGEMBALIAN as $peng}
                    <td class="center">
                        {number_format($peng.PENGEMBALIAN)}
                        {$total_pembayaran_right=$total_pembayaran_right+$peng.PENGEMBALIAN}
                        {$total_pembayaran_bottom[$peng@index]=$total_pembayaran_bottom[$peng@index]+$peng.PENGEMBALIAN}
                    </td>
                {/foreach}
                <td class="center total">{number_format($total_pembayaran_right)}</td>
            </tr>
        {/foreach}
        <tr class="center ui-widget-content">
            <td class="total" colspan="{if $prodi!=''}4{else}2{/if}">Jumlah</td>
            {if $prodi==''}
                <td class="total">{$total_mhs}</td>
            {/if}
            {foreach $data.DATA_PENGEMBALIAN as $peng}
                <td class="total">
                    {number_format($total_pembayaran_bottom[$peng@index])}
                    {$total=$total+$total_pembayaran_bottom[$peng@index]}
                </td>
            {/foreach}
            <td class="total">{number_format($total)}</td>
        </tr>
        <tr class="center ui-widget-content">
            <td class="total" colspan="{$count_data_biaya+5}">
                <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
                <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="window.close()">Close</span>
            </td>	
        </tr>
        <!--
        <tr class="center ui-widget-content">
            <td class="link center" colspan="{$count_data_biaya+5}">
                <span class="link_button ui-corner-all" onclick="window.location.href='excel-pembayaran-mhs-fakultas.php?tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&bank={$bank}&jalur={$jalur}'" style="padding:5px;cursor:pointer;">Excel</span>
            </td>	
        </tr>
        -->
    </table>
{/if}

{literal}
    <script>
        $('#report_form').submit(function() {
            if ($('#tgl_awal').val() == '' || $('#tgl_akhir').val() == '') {
                $('#alert').fadeIn();
                return false;
            }
        });
        $('#report_form').validate();
        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true,
            changeYear: true});
        $('#fakultas').change(function() {
            $.ajax({
                type: 'post',
                url: 'getProdi.php',
                data: 'id_fakultas=' + $(this).val(),
                success: function(data) {
                    $('#prodi').html(data);
                }
            })
        });
    </script>
{/literal}