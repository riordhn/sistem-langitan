<div class="center_title_bar">Laporan Penghasilan Orang Tua Mahasiswa Baru</div>
<form method="get" id="report_form" action="penghasilan-ortu.php">
    <table class="ui-widget" style="width:80%;">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Penerimaan</td>
            <td>
                <select name="penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                {if ($p2.ID_JENJANG==1||$p2.ID_JENJANG==5)}
                                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                                {/if}
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="6">
                <input type="hidden" name="mode" value="laporan" />
                <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_penghasilan)}
    <table class="ui-widget-content" style="width: 98%">
        <tr class="ui-widget-header">
            <th colspan="10" class="header-coloumn">Laporan Penghasilan Mahasiswa Baru</th>
        </tr>
        <tr class="ui-widget-header">
            <th rowspan="2">NO</th>
            <th style="width: 90px" rowspan="2">NO UJIAN/NIM</th>
            <th rowspan="2">NAMA</th>
            <th rowspan="2">PRODI</th>
            <th rowspan="2">JALUR</th>
            <th colspan="2">INSTANSI</th>
            <th colspan="2">PENGHASILAN</th>
            <th rowspan="2">TOTAL PENGHASILAN</th>
        </tr>
        <tr class="ui-widget-header">
            <th>INSTANSI AYAH</th>
            <th>INSTANSI IBU</th>
            <th>PENGHASILAN AYAH</th>
            <th>PENGHASILAN IBU</th>
        </tr>
        {foreach $data_penghasilan as $d}
            <tr>
                <td>{$d@index+1}</td>
                <td class="center">{$d.NO_UJIAN}<br/>{$d.NIM_MHS}</td>
                <td>{$d.NM_C_MHS}</td>
                <td>{$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI}</td>
                <td>{$d.NM_JALUR}</td>
                <td>{$d.INSTANSI_AYAH|upper} {if $d.JABATAN_AYAH!=''&&$d.JABATAN_AYAH!='-'}( {$d.JABATAN_AYAH|upper} ){/if}</td>
                <td>{$d.INSTANSI_IBU|upper} {if $d.JABATAN_IBU!=''&&$d.JABATAN_IBU!='-'}( {$d.JABATAN_IBU|upper} ){/if}</td>
                <td>{number_format($d.GAJI_AYAH+$d.TUNJANGAN_KELUARGA_AYAH+$d.TUNJANGAN_JABATAN_AYAH+$d.TUNJANGAN_SERTIFIKASI_AYAH+$d.TUNJANGAN_KEHORMATAN_AYAH+$d.RENUMERASI_AYAH+$d.TUNJANGAN_LAIN_AYAH)}</td>
                <td>{number_format($d.GAJI_IBU+$d.TUNJANGAN_KELUARGA_IBU+$d.TUNJANGAN_JABATAN_IBU+$d.TUNJANGAN_SERTIFIKASI_IBU+$d.TUNJANGAN_KEHORMATAN_IBU+$d.RENUMERASI_IBU+$d.TUNJANGAN_LAIN_IBU)}</td>
                <td>{number_format($d.TOTAL_PENDAPATAN_ORTU)}</td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="10" class="data-kosong">Data Kosong</td>
            </tr>
        {/foreach}
    </table>
{/if}