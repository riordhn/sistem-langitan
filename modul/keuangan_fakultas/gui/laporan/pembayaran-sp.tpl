<div class="center_title_bar">Laporan Pembayaran Semester Pendek</div> 
<div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
    <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
            Tanggal Harus Diisi.</p>
    </div>
</div>
<form method="get" id="report_form" action="pembayaran-sp.php">
    <table class="ui-widget" style="width:90%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Tanggal Awal</td>
            <td width="15%"><input type="text" id="tgl_awal" class="datepicker"  name="tgl_awal" {if isset($tgl_awal)} value="{$tgl_awal}"{/if} /></td>
            <td width="15%">Fakultas</td>
            <td width="55%">
                <select name="fakultas" id="fakultas">
                    {foreach $data_fakultas as $data}
                        {if $data.ID_FAKULTAS==$id_fakultas}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Tanggal Akhir</td>
            <td><input type="text" id="tgl_akhir" class="datepicker" class="required" name="tgl_akhir" {if isset($tgl_awal)} value="{$tgl_akhir}"{/if}/></td>
            <td>Program Studi</td>
            <td>
                <select name="prodi" id="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="4">
                <input type="hidden" name="mode" value="bank" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if $smarty.get.mode=='bank'}
    <table class="ui-widget" style="width:90%">
        <tr class="ui-widget-header">
            <th colspan="5">
                Rekapitulasi Pembayaran Semester Pendek Per Bank
                {if $fakultas!=''}
                    <br/>
                    Fakultas {$data_fakultas_one.NM_FAKULTAS}
                {/if}
                {if $prodi!=''}
                    <br/>
                    Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
                {/if}
                <br/>
                Pada Tanggal {$tgl_awal} sampai {$tgl_akhir}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NAMA BANK</th>
            <th>JUMLAH MAHASISWA</th>
            <th>JUMLAH PEMBAYARAN</th>
            <th>OPERASI</th>
        </tr>
        {foreach $data_report_bank as $data}
            <tr class="center ui-widget-content">
                <td>{$data@index+1}</td>
                <td>{$data.NM_BANK}</td>
                <td>{$data.JUMLAH_MHS}</td>
                <td>{number_format($data.PEMBAYARAN)}</td>
                <td>
                    {$total_mhs=$total_mhs+$data.JUMLAH_MHS}
                    {$total_pembayaran=$total_pembayaran+$data.PEMBAYARAN}
                    <a class="ui-button ui-corner-all ui-state-hover" href="pembayaran-sp.php?{$smarty.server.QUERY_STRING|replace:'bank':'detail'}&bank={$data.ID_BANK}" style="padding:5px;cursor:pointer;">Detail</a>
                </td>
            </tr>
        {/foreach}
        <tr class="center ui-widget-content">
            <td class="total" colspan="2">Jumlah</td>
            <td class="total" >{$total_mhs}</td>
            <td class="total" >{number_format($total_pembayaran)}</td>
            <td class="total" >
                <a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="pembayaran-mhs.php?mode=detail&tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}">Detail</a>
            </td>
        </tr>
    </table>
{else if $smarty.get.mode=='detail'}
    <table class="ui-widget-content" style="width:98%">
        <tr class="ui-widget-header">
            <th colspan="13">
                Rekapitulasi Pembayaran Semester Pendek
                {if $fakultas!=''}
                    <br/>
                    Fakultas {$data_fakultas_one.NM_FAKULTAS}
                {/if}
                {if $prodi!=''}
                    <br/>
                    Program Studi ({$data_prodi_one.NM_JENJANG}) {$data_prodi_one.NM_PROGRAM_STUDI}
                {/if}
                <br/>
                Pada Tanggal {$tgl_awal} sampai {$tgl_akhir}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Prodi</th>
            <th>Jalur</th>
            <th>Status</th>
            <th>Pembayaran</th>
            <th>Jumlah SKS</th>
            <th>Semester</th>
            <th>Tanggal Bayar</th>
            <th>Bank</th>
            <th>Keterangan</th>
            <th>Detail</th>
        </tr>
        {foreach $data_report_detail as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                <td>{$data.NM_JALUR}</td>
                <td>{$data.NM_STATUS_PENGGUNA}</td>
                <td>{number_format($data.BESAR_BIAYA)}</td>
                <td>{$data.JUMLAH_SKS}</td>
                <td>{$data.NM_SEMESTER}<br/> ({$data.GROUP_SEMESTER} {$data.TAHUN_AJARAN})</td>
                <td>{$data.TGL_BAYAR}</td>
                <td>{$data.NM_BANK} ({$data.NAMA_BANK_VIA})</td>
                <td>{$data.KETERANGAN}</td>
                <td class="center">
<!--                    <a href="history-bayar.php?cari={$data.NIM_MHS}"class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;">Detail</a>-->
                </td>
            </tr>
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="13" class="center"><span style="color: red">Data Kosong</span></td>
            </tr>
        {/foreach}
        <!--
        <tr class="center ui-widget-content">
            <td class="link center" colspan="9">
                <span class="link_button ui-corner-all" onclick="window.location.href='excel-pembayaran-mhs-detail.php?tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&bank={$bank}'" style="padding:5px;cursor:pointer;">Excel</span>
            </td>	
        </tr>
        -->
    </table>
    <span class="ui-button ui-corner-all ui-state-hover" onclick="history.back(-1)" style="padding:5px;cursor:pointer;margin:10px;">Kembali</span>

{/if}

{literal}
    <script>
            $('#report_form').submit(function() {
                    if($('#tgl_awal').val()==''||$('#tgl_akhir').val()==''){
                            $('#alert').fadeIn();
                    return false;
                    }  
            });
            $('#report_form').validate();
            $( ".datepicker" ).datepicker({dateFormat:'yy-mm-dd',changeMonth: true,
                        changeYear: true});
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}