<div class="center_title_bar">Laporan Status Pembayaran Mahasiswa</div> 
</div>
<form method="get" id="report_form" action="kontrol-status.php">
    <table class="ui-widget" style="width:90%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    {foreach $data_fakultas as $data}
                        {if $data.ID_FAKULTAS==$id_fakultas}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
            <td width="15%">Program Studi</td>
            <td>
                <select name="prodi" id="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$smarty.get.prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester</td>
            <td>
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$smarty.get.semester}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
            <td>Status</td>
            <td>
                <select name="status">
                    {foreach $data_status as $data}
                        <option value="{$data.ID_STATUS_PEMBAYARAN}" {if $smarty.get.status==$data.ID_STATUS_PEMBAYARAN} selected="true"{/if}>{$data.NAMA_STATUS}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        </tr>
        <tr class="ui-widget-content">
            <td>Jalur</td>
            <td>
                <select name="jalur">
                    <option value="">Semua</option>
                    {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}">{$data.NM_JALUR}</option>
                    {/foreach}
                </select>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="4">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>

{if isset($data_status_bayar)}
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="10">LAPORAN STATUS PEMBAYARAN MAHASISWA</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Prodi</th>
            <th>Jalur</th>
            <th>Biaya</th>
            <th>Status</th>
            <th>Tgl Jatuh Tempo</th>
            <th>Ket. Pembayaran</th>
            <th>Ket. Status</th>
        </tr>
        {foreach $data_status_bayar as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td><a href="history-bayar.php?cari={$data.NIM_MHS}">{$data.NIM_MHS}</a></td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                <td>{$data.NM_JALUR}</td>
                <td>{number_format($data.TOTAL_BIAYA)}</td>
                <td><a href="status-bayar.php?cari={$data.NIM_MHS}">{$data.NAMA_STATUS}</a></td>
                <td width="70px">{$data.TGL_JATUH_TEMPO}</td>
                <td>{$data.KETERANGAN_PEMBAYARAN}</td>
                <td>{$data.KETERANGAN_STATUS}</td>
            </tr>
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="10" class="center"><span style="color: red">Data Kosong</span></td>
            </tr>
        {/foreach}
    </table>
{/if}

{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}