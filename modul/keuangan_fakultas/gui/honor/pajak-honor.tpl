<div class="center_title_bar">Master Pajak Honor</div>
{$alert}
<table class="ui-widget" style="width: 40%">
    <tr class="ui-widget-header">
        <th colspan="4" class="header-coloumn" style="text-align: center;">Master Pajak Honor</th>
    </tr>
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Besar Pajak</th>
        <th>Besar Pajak NPWP</th>
        <th>Golongan dan Pangkat</th>
    </tr>
    {foreach $data_pajak_honor as $data}
        <tr class="ui-widget-content">
            <td class="center">{$data@index+1}</td>
            <td class="center">{$data.BESAR_PAJAK} %</td>
            <td class="center">{$data.BESAR_PAJAK_NPWP} %</td>
            <td>{$data.NM_GOLONGAN} {$data.NM_PANGKAT}</td>
        </tr>
    {foreachelse}
        <tr class="ui-widget-content">
            <th colspan="4" class="data-kosong" style="text-align: center;">Data Kosong</th>
        </tr>
    {/foreach}
</table>
