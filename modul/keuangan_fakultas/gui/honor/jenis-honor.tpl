<div class="center_title_bar">Master Jenis Honor</div>
{$alert}
<table class="ui-widget" style="width: 40%">
    <tr class="ui-widget-header">
        <th colspan="3" class="header-coloumn" style="text-align: center;">Master Jenis Honor</th>
    </tr>
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Nama Jenis Honor</th>
        <th>Tipe Jenis Honor</th>
    </tr>
    {foreach $data_jenis_honor as $data}
        <tr class="ui-widget-content">
            <td class="center">{$data@index+1}</td>
            <td class="center">{$data.NM_JENIS_HONOR}</td>
            <td>
                {if $data.TIPE_HONOR=='1'}
                    Dosen
                {elseif $data.TIPE_HONOR=='2'}
                    Karyawan
                {/if}
            </td>
        </tr>
    {foreachelse}
        <tr class="ui-widget-content">
            <th colspan="3" class="data-kosong" style="text-align: center;">Data Kosong</th>
        </tr>
    {/foreach}
</table>	
