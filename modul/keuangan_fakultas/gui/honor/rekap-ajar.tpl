<div class="center_title_bar">Rekapitulasi Mengajar Dosen</div> 
{$info1}

<form method="get" id="report_form" action="rekap-ajar.php">
    <table class="ui-widget" style="width:90%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Tanggal Awal</td>
            <td width="10%"><input type="text" id="tgl_awal" class="datepicker required"  name="tgl_awal" {if isset($tgl_awal)} value="{$tgl_awal}"{/if} /></td>
            <td width="15%">Fakultas</td>
            <td width="60%">
                <select name="fakultas" id="fakultas">
                    {foreach $data_fakultas as $data}
                        {if $data.ID_FAKULTAS==$id_fakultas}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Tanggal Akhir</td>
            <td><input type="text" id="tgl_akhir" class="datepicker required" name="tgl_akhir" {if isset($tgl_awal)} value="{$tgl_akhir}"{/if}/></td>
            <td>Program Studi</td>
            <td>
                <select name="prodi" id="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="4">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>
    </table>
</form>
{if isset($data_rekap)}
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="7">REKAPITULASI MENGAJAR</th>
        </tr>
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>NIP DOSEN</th>
            <th>NAMA</th>
            <th>PROGRAM STUDI</th>
            <th>TOTAL SKS MENGAJAR</th>
            <th>TOTAL JAM MENGAJAR</th>
            <th>DETAIL</th>
        </tr>
        {foreach $data_rekap as $d}
            <tr class="ui-widget-content">
                <td>{$d@index+1}</td>
                <td>{$d.NIP_DOSEN}</td>
                <td>{$d.GELAR_DEPAN} {$d.NM_PENGGUNA} {$d.GELAR_BELAKANG}</td>
                <td>{$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI}</td>
                <td class="center">{$d.TOTAL_SKS}</td>
                <td class="center">{round($d.TOTAL_JAM,2)} Jam</td>
                <td class="center">
                    <a href="rekap-ajar.php?{$smarty.server.QUERY_STRING|replace:'tampil':'detail'}&id={$d.ID_DOSEN}" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;">Detail</a>
                </td>
            </tr>
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="7" class="data-kosong center">Data Tidak Ditemukan</td>
            </tr>
        {/foreach}
        <tr>
            <td class="center link" colspan="7">
                <span class="link_button ui-corner-all" onclick="window.location.href='excel-rekap-ajar.php?{$smarty.server.QUERY_STRING}'" style="padding:5px;cursor:pointer;">Excel</span>
            </td>
        </tr>
    </table>
{elseif isset($data_detail)}
    <table class="ui-widget-content" style="width: 90%">
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="8">DETAIL MENGAJAR DOSEN</th>
        </tr>
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>MATA KULIAH</th>
            <th>KELAS</th>
            <th>PRODI KELAS</th>
            <th>TANGGAL PRESENSI</th>
            <th>WAKTU</th>
            <th>KREDIT SEMESTER</th>
            <th>JAM MENGAJAR</th>
        </tr>
        {$total_sks=0}
        {$total_jam=0}
        {foreach $data_detail as $d}
            <tr>
                <td>{$d@index+1}</td>
                <td>{$d.NM_MATA_KULIAH} ( {$d.KD_MATA_KULIAH}  )</td>
                <td class="center">{$d.NM_KELAS}</td>
                <td>{$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI}</td>
                <td class="center">{$d.TGL_PRESENSI_KELAS}</td>
                <td class="center">{$d.WAKTU_MULAI} -  {$d.WAKTU_SELESAI}</td>
                <td class="center">{$d.KREDIT_SEMESTER}</td>
                <td class="center">{round($d.JAM_MENGAJAR,2)}</td>
            </tr>
            {$total_sks=$total_sks+$d.KREDIT_SEMESTER}
            {$total_jam=$total_jam+$d.JAM_MENGAJAR}
        {foreachelse}
            <tr>
                <td colspan="8" class="data-kosong center">Data Tidak Ditemukan</td>
            </tr>
        {/foreach}
        <tr class="total">
            <td colspan="6"></td>
            <td class="center">TOTAL SKS : {round($total_sks,2)}</td>
            <td class="center">TOTAL JAM : {round($total_jam,2)}</td>
        </tr>
    </table>
    <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
{/if}
{literal}
    <script>
        $('#report_form').validate();
        $( ".datepicker" ).datepicker({dateFormat:'yy-mm-dd',changeMonth: true,
                    changeYear: true});
        $('#fakultas').change(function(){
                $.ajax({
                        type:'post',
                        url:'getProdi.php',
                        data:'id_fakultas='+$(this).val(),
                        success:function(data){
                                $('#prodi').html(data);
                        }                    
                })
        });
    </script>
{/literal}