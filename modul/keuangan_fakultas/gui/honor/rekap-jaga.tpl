<div class="center_title_bar">Rekapitulasi Jaga Ujian</div> 
{if !empty($smarty.get.jenis)}
    {if $smarty.get.jenis=='utsuas'}

        {literal}
            <script type="text/javascript">
                $('#tabs').tabs();
                $('#report_form').validate();
                $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true,
                    changeYear: true});
                $('#fakultas_utsuas').change(function() {
                    $.ajax({
                        type: 'post',
                        url: 'getProdi.php',
                        data: 'id_fakultas=' + $(this).val(),
                        success: function(data) {
                            $('#prodi_utsuas').html(data);
                        }
                    })
                });
            </script>
        {/literal}
    {else $smarty.get.jenis=='sidang'}

        {literal}
            <script type="text/javascript">
                $('#tabs').tabs({
                    selected: 1
                });
                $('#report_form').validate();
                $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true,
                    changeYear: true});
                $('#fakultas_sidang').change(function() {
                    $.ajax({
                        type: 'post',
                        url: 'getProdi.php',
                        data: 'id_fakultas=' + $(this).val(),
                        success: function(data) {
                            $('#prodi_sidang').html(data);
                        }
                    })
                });
            </script>
        {/literal}
    {/if}
{else}
    {literal}
        <script type="text/javascript">
            $('#tabs').tabs();
            $('#report_form').validate();
            $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true,
                changeYear: true});
            $('#fakultas_sidang').change(function() {
                $.ajax({
                    type: 'post',
                    url: 'getProdi.php',
                    data: 'id_fakultas=' + $(this).val(),
                    success: function(data) {
                        $('#prodi_sidang').html(data);
                    }
                })
            });
        </script>
    {/literal}
{/if}
<div id="tabs" style="position: inherit">
    <ul>
        <li><a class="disable-ajax" href="#uts-uas">UTS/UAS</a></li>
        <li><a class="disable-ajax"href="#sidang">TA/Skripsi/Thesis/Desertasi</a></li>
    </ul>
    <div id="uts-uas">
        <form method="get" id="report_form" action="rekap-jaga.php">
            <table class="ui-widget" style="width:90%">
                <tr class="ui-widget-header">
                    <th colspan="4" class="header-coloumn">Parameter Rekap Jaga Ujian UTS/UAS</th>
                </tr>
                <tr class="ui-widget-content">
                    <td width="15%">Tanggal Awal</td>
                    <td width="10%"><input type="text" id="tgl_awal_utsuas" class="datepicker required"  name="tgl_awal" {if isset($tgl_awal)} value="{$tgl_awal}"{/if} /></td>
                    <td width="15%">Fakultas</td>
                    <td width="60%">
                        <select name="fakultas" id="fakultas_utsuas">
                            {foreach $data_fakultas as $data}
                                {if $data.ID_FAKULTAS==$id_fakultas}
                                    <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Tanggal Akhir</td>
                    <td><input type="text" id="tgl_akhir_utsuas" class="datepicker required" name="tgl_akhir" {if isset($tgl_awal)} value="{$tgl_akhir}"{/if}/></td>
                    <td>Program Studi</td>
                    <td>
                        <select name="prodi" id="prodi_utsuas">
                            <option value="">Semua</option>
                            {foreach $data_prodi as $data}
                                <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                </tr>
                <tr class="center ui-widget-content">
                    <td colspan="4">
                        <input type="hidden" name="jenis" value="utsuas" />
                        <input type="hidden" name="mode" value="tampil" />
                        <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
                    </td>
                </tr>
            </table>
        </form>
        {if isset($data_rekap_utsuas)}
            <table class="ui-widget" style="width: 90%">
                <tr class="ui-widget-header">
                    <th class="header-coloumn" colspan="6">REKAPITULASI JAGA UJIAN UTS/UAS</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>NO</th>
                    <th>NIP</th>
                    <th>NAMA PENGAWAS</th>
                    <th>PRODI</th>
                    <th>TOTAL JAGA</th>
                    <th>DETAIL</th>
                </tr>
                {foreach $data_rekap_utsuas as $d}
                    <tr class="ui-widget-content">
                        <td>{$d@index+1}</td>
                        <td>{$d.USERNAME}</td>
                        <td>{$d.PENGAWAS}</td>
                        <td>{$d.PRODI}</td>
                        <td class="center">{$d.TOTAL}</td>
                        <td class="center">
                            <a href="rekap-jaga.php?{$smarty.server.QUERY_STRING|replace:'tampil':'detail'}&id={$d.ID_PENGGUNA}&prodos={$d.ID_PROGRAM_STUDI}&jmlmenit={$d.JML_MENIT}" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;">Detail</a>
                        </td>
                    </tr>
                {foreachelse}
                    <tr class="ui-widget-content">
                        <td colspan="6" class="data-kosong center">Data Tidak Ditemukan</td>
                    </tr>
                {/foreach}
                <tr>
                    <td class="center link" colspan="6">
                        <span class="link_button ui-corner-all" onclick="window.location.href = 'excel-rekap-jaga.php?{$smarty.server.QUERY_STRING}'" style="padding:5px;cursor:pointer;">Excel</span>
                    </td>
                </tr>
            </table>
        {elseif isset($data_detail_utsuas)}
            <table class="ui-widget-content" style="width: 90%">
                <tr class="ui-widget-header">
                    <th class="header-coloumn" colspan="8">DETAIL PENGAWAS</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>NO</th>
                    <th>NIP</th>
                    <th>NAMA PENGAWAS</th>
                    <th>PRODI</th>
                    <th>JENIS</th>
                    <th>MATA KULIAH</th>
                    <th>WAKTU</th>
                    <th>TOTAL MENIT</th>
                </tr>
                {$total_jam=0}
                {foreach $data_detail_utsuas as $d}
                    <tr>
                        <td>{$d@index+1}</td>
                        <td>{$d.USERNAME}</td>
                        <td>{$d.PENGAWAS}</td>
                        <td class="center">{$d.PRODI}</td>
                        <td class="center">{$d.JENIS}</td>
                        <td>{$d.MK}</td>
                        <td class="center">TANGGAL {$d.MULAI} <br/> {$d.JAM_MULAI} s/d {$d.JAM_SELESAI}</td>
                        <td class="center">{$d.JML_MENIT}</td>
                    </tr>
                    {$total_jam=$total_jam+$d.JML_MENIT}
                {foreachelse}
                    <tr>
                        <td colspan="8" class="data-kosong center">Data Tidak Ditemukan</td>
                    </tr>
                {/foreach}
                <tr class="total">
                    <td colspan="7"></td>
                    <td class="center">TOTAL JAM : {round($total_jam,2)}</td>
                </tr>
            </table>
            <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
        {/if}
    </div>
    <div id="sidang">
        <form method="get" id="report_form" action="rekap-jaga.php">
            <table class="ui-widget" style="width:90%">
                <tr class="ui-widget-header">
                    <th colspan="4" class="header-coloumn">Parameter Rekap Jaga Ujian TA/Skripsi/Thesis/Desertasi</th>
                </tr>
                <tr class="ui-widget-content">
                    <td width="15%">Tanggal Awal</td>
                    <td width="10%"><input type="text" id="tgl_awal_sidang" class="datepicker required"  name="tgl_awal" {if isset($tgl_awal)} value="{$tgl_awal}"{/if} /></td>
                    <td width="15%">Fakultas</td>
                    <td width="60%">
                        <select name="fakultas" id="fakultas_sidang">
                            {foreach $data_fakultas as $data}
                                {if $data.ID_FAKULTAS==$id_fakultas}
                                    <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr class="ui-widget-content">
                    <td>Tanggal Akhir</td>
                    <td><input type="text" id="tgl_akhir_sidang" class="datepicker required" name="tgl_akhir" {if isset($tgl_awal)} value="{$tgl_akhir}"{/if}/></td>
                    <td>Program Studi</td>
                    <td>
                        <select name="prodi" id="prodi_sidang">
                            <option value="">Semua</option>
                            {foreach $data_prodi as $data}
                                <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                </tr>
                <tr class="center ui-widget-content">
                    <td colspan="4">
                        <input type="hidden" name="jenis" value="sidang" />
                        <input type="hidden" name="mode" value="tampil" />
                        <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
                    </td>
                </tr>
            </table>
        </form>
        {if isset($data_rekap_sidang)}
            <table class="ui-widget" style="width: 90%">
                <tr class="ui-widget-header">
                    <th class="header-coloumn" colspan="6">REKAPITULASI JAGA UJIAN TA/Skripsi/Thesis/Desertasi</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>NO</th>
                    <th>NIP</th>
                    <th>NAMA PENGAWAS</th>
                    <th>PRODI</th>
                    <th>TOTAL JAGA</th>
                    <th>DETAIL</th>
                </tr>
                {foreach $data_rekap_sidang as $d}
                    <tr class="ui-widget-content">
                        <td>{$d@index+1}</td>
                        <td>{$d.USERNAME}</td>
                        <td>{$d.PENGAWAS}</td>
                        <td>{$d.PRODI}</td>
                        <td class="center">{$d.TOTAL}</td>
                        <td class="center">
                            <a href="rekap-jaga.php?{$smarty.server.QUERY_STRING|replace:'tampil':'detail'}&id={$d.ID_PENGGUNA}&prodos={$d.ID_PROGRAM_STUDI}&jmlmenit={$d.JML_MENIT}" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;">Detail</a>
                        </td>
                    </tr>
                {foreachelse}
                    <tr class="ui-widget-content">
                        <td colspan="6" class="data-kosong center">Data Tidak Ditemukan</td>
                    </tr>
                {/foreach}
                <tr>
                    <td class="center link" colspan="6">
                        <span class="link_button ui-corner-all" onclick="window.location.href = 'excel-rekap-jaga.php?{$smarty.server.QUERY_STRING}'" style="padding:5px;cursor:pointer;">Excel</span>
                    </td>
                </tr>
            </table>
        {elseif isset($data_detail_sidang)}
            <table class="ui-widget-content" style="width: 90%">
                <tr class="ui-widget-header">
                    <th class="header-coloumn" colspan="8">DETAIL PENGAWAS</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>NO</th>
                    <th>NIP</th>
                    <th>NAMA PENGAWAS</th>
                    <th>PRODI</th>
                    <th>JENIS</th>
                    <th>MATA KULIAH</th>
                    <th>WAKTU</th>
                    <th>TOTAL MENIT</th>
                </tr>
                {$total_jam=0}
                {foreach $data_detail_sidang as $d}
                    <tr>
                        <td>{$d@index+1}</td>
                        <td>{$d.USERNAME}</td>
                        <td>{$d.PENGAWAS}</td>
                        <td class="center">{$d.PRODI}</td>
                        <td class="center">{$d.JENIS}</td>
                        <td>{$d.MK}</td>
                        <td class="center">TANGGAL {$d.MULAI} <br/> {$d.JAM_MULAI} s/d {$d.JAM_SELESAI}</td>
                        <td class="center">{$d.JML_MENIT}</td>
                    </tr>
                    {$total_jam=$total_jam+$d.JML_MENIT}
                {foreachelse}
                    <tr>
                        <td colspan="8" class="data-kosong center">Data Tidak Ditemukan</td>
                    </tr>
                {/foreach}
                <tr class="total">
                    <td colspan="7"></td>
                    <td class="center">TOTAL JAM : {round($total_jam,2)}</td>
                </tr>
            </table>
            <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
        {/if}
    </div>
</div>
