<div class="center_title_bar">Master Tarif Honor</div>
{$alert}
<table class="ui-widget" style="width: 90%">
    <tr class="ui-widget-header">
        <th colspan="9" class="header-coloumn" style="text-align: center;">Master Tarif Honor</th>
    </tr>
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Nama Jenis Honor</th>
        <th>Program Studi</th>
        <th>Jenis Kelas</th>
        <th>Status Kelas</th>
        <th>Golongan dan Pangkat</th>
        <th>Pendidikan AKhir</th>
        <th>Besar Tarif</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_tarif_honor as $data}
        <tr class="ui-widget-content">
            <td class="center">{$data@index+1}</td>
            <td class="center">{$data.NM_JENIS_HONOR}</td>
            <td>{$data.NM_JENJANG} {$data.NM_PROGRAM_STUDI}</td>
            <td>
                {if $data.JENIS_KELAS=='EC'}
                    EC
                {else if $data.JENIS_KELAS=='R'}
                    Reguler
                {else if $data.JENIS_KELAS=='AJ'}
                    Alih Jenis
                {else}
                    International
                {/if}
            </td>
            <td>
                {if $data.STATUS_KELAS=='S'}
                    Siang
                {else}
                    Malam
                {/if}
            </td>
            <td>{$data.NM_GOLONGAN} {$data.NM_PANGKAT}</td>
            <td>{$data.NAMA_PENDIDIKAN_AKHIR}</td>
            <td>{number_format($data.BESAR_TARIF)}</td>
            <td>
                <span class="ui-widget ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;margin-left:15px;" 
                      onclick="$('#dialog-edit').dialog('open').load('tarif-honor.php?mode=edit&id={$data.ID_HONORARIUM}')">Edit</span>
            </td>
        </tr>
    {foreachelse}
        <tr class="ui-widget-content">
            <th colspan="9" class="data-kosong" style="text-align: center;">Data Kosong</th>
        </tr>
    {/foreach}
    <tr class="ui-widget-content">
        <td colspan="9" class="center">
            <span class="ui-widget ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;margin-left:15px;margin-top:5px;" 
                  onclick="$('#dialog-add').dialog('open').load('tarif-honor.php?mode=add')">Tambah</span>
        </td>
    </tr>
</table>	
<div id="dialog-add" title="Tambah Master Tarif Honorarium"></div>
<div id="dialog-edit" title="Edit Master Tarif Honorarium"></div>
{literal}
    <script type="text/javascript">
        $('#dialog-add').dialog({
            width:'40%',
            modal: true,
            resizable:false,
            autoOpen:false
        });
        $('#dialog-edit').dialog({
            width:'40%',
            modal: true,
            resizable:false,
            autoOpen:false
        });
    </script>
{/literal}
