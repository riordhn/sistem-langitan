<form id="form_edit" method="post" action="tarif-honor.php">
    <table class="ui-widget" style="width: 90%">
        <table class="ui-widget" style="width: 90%">
            <tr class="ui-widget-header">
                <th colspan="2" class="header-coloumn" style="text-align: center;">Edit Jenis Honor</th>
            </tr>
            <tr class="ui-widget-content">
                <td>Nama Jenis Honor</td>
                <td>
                    <select name="jenis_honor">
                        {foreach $data_jenis_honor as $data}
                            {if $data.TIPE_HONOR==1}
                                <option value="{$data.ID_JENIS_HONOR}" {if $data_tarif_honor.ID_JENIS_HONOR==$data.ID_JENIS_HONOR}selected="true"{/if}>{$data.NM_JENIS_HONOR}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Program Studi</td>
                <td>
                    <select name="prodi">
                        {foreach $data_prodi as $data}
                            <option value="{$data.ID_PROGRAM_STUDI}" {if $data_tarif_honor.ID_PROGRAM_STUDI==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_JENJANG} {$data.NM_PROGRAM_STUDI}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Jenis Kelas</td>
                <td>
                    <select name="jenis_kelas">
                        <option value="EC" {if $data_tarif_honor.JENIS_KELAS=='EC'}selected="true"{/if}>EC</option>
                        <option value="R" {if $data_tarif_honor.JENIS_KELAS=='R'}selected="true"{/if}>Reguler</option>
                        <option value="AJ" {if $data_tarif_honor.JENIS_KELAS=='AJ'}selected="true"{/if}>Alih Jenis</option>
                        <option value="I" {if $data_tarif_honor.JENIS_KELAS=='I'}selected="true"{/if}>International</option>
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Status Kelas</td>
                <td>
                    <select name="status_kelas">
                        <option value="S" {if $data_tarif_honor.STATUS_KELAS=='S'}selected="true"{/if}>Siang</option>
                        <option value="M" {if $data_tarif_honor.STATUS_KELAS=='M'}selected="true"{/if}>Malam</option>
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Golongan dan Pangkat</td>
                <td>
                    <select name="golongan">
                        {foreach $data_golongan as $data}
                            <option value="{$data.ID_GOLONGAN}" {if $data_tarif_honor.ID_GOLONGAN==$data.ID_GOLONGAN}selected="true"{/if}>{$data.NM_GOLONGAN} {$data.NM_PANGKAT}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Pendidikan</td>
                <td>
                    <select name="pendidikan">
                        {foreach $data_pendidikan as $data}
                            <option value="{$data.ID_PENDIDIKAN_AKHIR}" {if $data_tarif_honor.ID_PENDIDIKAN_AKHIR==$data.ID_PENDIDIKAN_AKHIR}selected="true"{/if}>{$data.NAMA_PENDIDIKAN_AKHIR}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td>Besar Tarif</td>
                <td>
                    <input type="text" name="tarif" class="required number" value="{$data_tarif_honor.BESAR_TARIF}" />
                </td>
            </tr>
            <tr class="ui-widget-content">
                <td colspan="2" class="center">
                    <input type="hidden" name="mode" value="edit"/>
                    <input type="hidden" name="id" value="{$data_tarif_honor.ID_HONORARIUM}"/>
                    <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Edit"/>
                </td>
            </tr>
        </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_edit').validate();            
            $('#form_edit').submit(function(){
                if($('#form_edit').valid()){
                    $('#dialog-edit').dialog('close');
                }
                else{
                    return false;
                }
            });
    </script>
{/literal}