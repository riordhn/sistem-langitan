<form id="form_add" method="post" action="tarif-honor.php">
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn" style="text-align: center;">Tambah Jenis Honor</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Nama Jenis Honor</td>
            <td>
                <select name="jenis_honor">
                    {foreach $data_jenis_honor as $data}
                        {if $data.TIPE_HONOR==1}
                            <option value="{$data.ID_JENIS_HONOR}">{$data.NM_JENIS_HONOR}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Program Studi</td>
            <td>
                <select name="prodi">
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}">{$data.NM_JENJANG} {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenis Kelas</td>
            <td>
                <select name="jenis_kelas">
                    <option value="EC">EC</option>
                    <option value="R">Reguler</option>
                    <option value="AJ">ALIH JENIS</option>
                    <option value="I">International</option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Status Kelas</td>
            <td>
                <select name="status_kelas">
                    <option value="S">Siang</option>
                    <option value="M">Malam</option>
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Golongan dan Pangkat</td>
            <td>
                <select name="golongan">
                    {foreach $data_golongan as $data}
                        <option value="{$data.ID_GOLONGAN}">{$data.NM_GOLONGAN} {$data.NM_PANGKAT}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Pendidikan</td>
            <td>
                <select name="pendidikan">
                    {foreach $data_pendidikan as $data}
                        <option value="{$data.ID_PENDIDIKAN_AKHIR}">{$data.NAMA_PENDIDIKAN_AKHIR}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Besar Tarif</td>
            <td>
                <input type="text" name="tarif" class="required number" />
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="add"/>
                <input class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" type="submit" value="Tambah"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
            $('#form_add').validate();            
            $('#form_add').submit(function(){
                if($('#form_add').valid()){
                    $('#dialog-add').dialog('close');
                }
                else{
                    return false;
                }
            });
    </script>
{/literal}