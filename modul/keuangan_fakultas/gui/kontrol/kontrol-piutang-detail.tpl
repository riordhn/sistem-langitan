<link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
<link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
<script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
<script language="javascript" src="../../js/jquery.validate.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
<style>
    .center{
        text-align: center;
    }
</style>
<form method="get" id="report_form" action="kontrol-piutang-detail.php">
    <table class="ui-widget" style="width:90%;">
        <tr class="ui-widget-header">
            <th colspan="6" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    {foreach $data_fakultas as $data}
                        {if $data.ID_FAKULTAS==$id_fakultas}
                           <option value="{$data.NM_FAKULTAS}" {if $data.NM_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
            <td>Tanggal Cut Off</td>
            <td>
                <select name="tgl">
                    {foreach $data_cut_off as $t}
                        <option value="{$t['CUT_OFF']}" {if $t.CUT_OFF==$smarty.get.tgl} selected="true" {/if}>{$t.CUT_OFF}</option>
                    {/foreach}
                </select>
            </td><td>Status Piutang</td>
            <td>
                <select name="status">
                    <option value="1" {if $smarty.get.status==1}selected="true" {/if}>Piutang</option>
                    <option value="0" {if $smarty.get.status==0&&$smarty.get.status!=''}selected="true" {/if}>Bukan</option>
                </select>
            </td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="6">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_piutang)}
    <table class="ui-widget" style="width:100%;">
        <tr class="ui-widget-header">
            <th colspan="
                {if $smarty.get.prodi !=''||$smarty.get.mhs!=''}
                    {count($data_biaya)+7}
                {else} 
                    {count($data_biaya)+4}
                {/if}">

                Rekapitulasi Detail Piutang  Mahasiswa
                {if $smarty.get.fakultas!=''}
                    <br/>
                    Fakultas {$smarty.get.fakultas}
                {/if}
                {if $smarty.get.prodi!=''||$smarty.get.mhs!=''}
                    <br/>
                    Program Studi ({$smarty.get.jenjang}) {$smarty.get.prodi}
                {/if}
                <br/>
                Pada Tanggal Cut OFF {$smarty.get.tgl}
            </th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>
                {if $smarty.get.prodi!=''||$smarty.get.mhs!=''}
                    Nama
                {else if $smarty.get.fakultas!=''}
                    Program Studi
                {else}
                    Fakultas
                {/if}
            </th>
            {if $smarty.get.prodi!=''||$smarty.get.mhs!=''}
                <th>NIM</th>
                <th>Status</th>
                <th>Banyak Tagihan Per Semester</th>
            {/if}
            <th>
                {if $smarty.get.prodi!=''||$smarty.get.mhs!=''}
                    Program Studi
                {else}
                    Jumlah MHS
                {/if}
            </th>
            {foreach $data_biaya as $data}
                <th>{$data.BIAYA}</th>
            {/foreach}
            <th>Total Piutang</th>
        </tr>
        {foreach $data_piutang as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td>
                    {if $smarty.get.prodi!=''||$smarty.get.mhs!=''}
                        {$data.NAMA}
                    {else if $smarty.get.fakultas!=''}
                        <a href="kontrol-piutang-detail.php?mode=tampil&fakultas={$smarty.get.fakultas}&jenjang={$data.NM_JENJANG}&prodi={$data.NM_PROGRAM_STUDI}&tgl={$smarty.get.tgl}&status={$smarty.get.status}">
                            ({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}
                        </a>
                    {else}
                        <a href="kontrol-piutang-detail.php?mode=tampil&fakultas={$data.NM_FAKULTAS}&tgl={$smarty.get.tgl}&status={$smarty.get.status}">
                            {$data.NM_FAKULTAS}
                        </a>
                        <br/>
                        <a href="kontrol-piutang-detail.php?mode=tampil&fakultas={$data.NM_FAKULTAS}&mhs=show&tgl={$smarty.get.tgl}&status={$smarty.get.status}">
                            Tampil Mhs
                        </a>
                    {/if}    
                </td>
                {if $smarty.get.prodi!=''||$smarty.get.mhs!=''}
                    <td>{$data.NIM}</td>
                    <td>{$data.STATUS}</td>
                    <td class="center">{$data.BANYAK_TAGIHAN}</td>
                {/if}
                <td class="center">
                    {if $smarty.get.prodi!=''||$smarty.get.mhs!=''}
                        {$data.JENJANG} {$data.PRODI}
                    {else}    
                        {$data.JUMLAH_MHS}
                        {$total_mhs=$total_mhs+$data.JUMLAH_MHS}
                    {/if}
                </td>
                {$total_pembayaran_right=0}
                {$total_denda_right=0}
                {foreach $data.PIUTANG as $p}
                    <td class="center">
                        {number_format($p.BESAR_BIAYA)}
                        {$total_pembayaran_right=$total_pembayaran_right+$p.BESAR_BIAYA}
                        {$total_pembayaran_bottom[$p@index]=$total_pembayaran_bottom[$p@index]+$p.BESAR_BIAYA}
                    </td>
                {/foreach}
                <td class="center total">{number_format($total_pembayaran_right)}</td>
            </tr>
        {/foreach}
        <tr class="center ui-widget-content">
            <td class="total" colspan="{if $smarty.get.prodi!=''||$smarty.get.mhs!=''}6{else}2{/if}">Jumlah</td>
            {if ($smarty.get.prodi==''||$smarty.get.fakultas=='')&&$smarty.get.mhs==''}
                <td class="total">{$total_mhs}</td>
            {/if}
            {foreach $data.PIUTANG as $p}
                <td class="total">
                    {number_format($total_pembayaran_bottom[$p@index])}
                    {$total=$total+$total_pembayaran_bottom[$p@index]}
                </td>
            {/foreach}
            <td class="total">{number_format($total+$total_denda)}</td>
        </tr>
        <tr class="center ui-widget-content">
            <td class="total" colspan="{if $smarty.get.prodi !=''||$smarty.get.mhs!=''}{count($data_biaya)+7}{else} {count($data_biaya)+4}{/if}">
                <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="history.back(-1)">Kembali</span>
                <span class="ui-button ui-corner-all ui-state-hover" style="margin-top:10px;padding:5px;cursor:pointer;" onclick="window.close()">Close</span>
            </td>	
        </tr>
        <!--        <tr class="center ui-widget-content">
                    <td class="link center" colspan="{if $fakultas !=''}{$count_data_biaya+6}{else} {$count_data_biaya+5}{/if}">
                        <span class="link_button ui-corner-all" onclick="window.location.href='excel-pembayaran-mhs-fakultas.php?tgl_awal={$tgl_awal}&tgl_akhir={$tgl_akhir}&fakultas={$fakultas}&prodi={$prodi}&bank={$bank}&jalur={$jalur}'" style="padding:5px;cursor:pointer;">Excel</span>
                    </td>	
                </tr>-->
    </table>
{/if}
{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}