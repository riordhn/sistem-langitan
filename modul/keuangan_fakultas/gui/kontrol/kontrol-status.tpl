{literal}
    <style>
        .ui-widget-content a{
            text-decoration: none;
            color: brown;
        }
        .ui-widget-content a:hover{
            color: #f09a14;
        }
    </style>
{/literal}
<div class="center_title_bar">Laporan Status Pembayaran Mahasiswa</div> 
</div>
<form method="get" id="report_form" action="kontrol-status.php">
    <table class="ui-widget" style="width:90%">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    {foreach $data_fakultas as $data}
                        {if $data.ID_FAKULTAS==$id_fakultas}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
            <td width="15%">Program Studi</td>
            <td>
                <select name="prodi" id="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$smarty.get.prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Semester</td>
            <td>
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$smarty.get.semester}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
            <td>Status Bayar</td>
            <td>
                <select name="status">
                    {foreach $data_status as $data}
                        <option value="{$data.ID_STATUS_PEMBAYARAN}" {if $smarty.get.status==$data.ID_STATUS_PEMBAYARAN} selected="true"{/if}>{$data.NAMA_STATUS}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        </tr>
        <tr class="ui-widget-content">
            <td>Jalur</td>
            <td>
                <select name="jalur">
                    <option value="">Semua</option>
                    {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}">{$data.NM_JALUR}</option>
                    {/foreach}
                </select>
            </td>
            <td>Angkatan Mahasiswa</td>
            <td>
                <select name="angkatan">
                    <option value="">Semua</option>
                    {foreach $data_angkatan as $data}
                        <option value="{$data.ANGKATAN}" {if $data.ANGKATAN==$smarty.get.angkatan}selected="true"{/if}>{$data.ANGKATAN}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Jenjang</td>
            <td>
                <select name="jenjang">
                    <option value="">Semua</option>
                    {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $data.ID_JENJANG==$smarty.get.jenjang}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}
                </select>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="4">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>

{if isset($data_status_bayar)}
    <table class="ui-widget" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="11">LAPORAN STATUS PEMBAYARAN MAHASISWA</th>
        </tr>
        <tr class="ui-widget-header">
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Prodi</th>
            <th>Jalur</th>
            <th>Status Mahasiswa</th>
            <th>Biaya</th>
            <th>Status Bayar</th>
            <th>Tgl Jatuh Tempo</th>
            <th>Ket. Pembayaran</th>
            <th>Ket. Status</th>
        </tr>
        {foreach $data_status_bayar as $data}
            <tr class="ui-widget-content">
                <td>{$data@index+1}</td>
                <td><a href="history-bayar.php?cari={$data.NIM_MHS}">{$data.NIM_MHS}</a></td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                <td class="center">{$data.NM_JALUR}</td>
                <td class="center">{$data.NM_STATUS_PENGGUNA}</td>
                <td class="center">{number_format($data.TOTAL_BIAYA)}</td>
                <td class="center"><a href="status-bayar.php?cari={$data.NIM_MHS}">{$data.NAMA_STATUS}</a></td>
                <td width="70px">{$data.TGL_JATUH_TEMPO}</td>
                <td class="center">{$data.KETERANGAN_PEMBAYARAN}</td>
                <td class="center">{$data.KETERANGAN_STATUS}</td>
            </tr>
        {foreachelse}
            <tr class="ui-widget-content">
                <td colspan="11" class="center"><span style="color: red">Data Kosong</span></td>
            </tr>
        {/foreach}
        <tr class="ui-widget-content">
            <td class="link center" colspan="11">
                <span class="link_button ui-corner-all" onclick="window.location.href='excel-kontrol-status.php?{$smarty.server.QUERY_STRING}'" style="padding:5px;cursor:pointer;">Excel</span>
            </td>	
        </tr>
    </table>
{/if}

{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}