<div class="center_title_bar">Laporan Kontrol Mahasiswa Piutang</div> 
<form method="get" id="report_form" action="kontrol-piutang.php">
    <table class="ui-widget" style="width:90%;">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td width="15%">Fakultas</td>
            <td width="25%">
                <select name="fakultas" id="fakultas">
                    {foreach $data_fakultas as $data}
                        {if $data.ID_FAKULTAS==$id_fakultas}
                            <option value="{$data.NM_FAKULTAS}" {if $data.NM_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
            <td width="15%">Program Studi</td>
            <td>
                <select name="prodi" id="prodi">
                    <option value="">Semua</option>
                    {foreach $data_prodi as $data}
                        <option value="{$data.VALUE}" {if {$data.VALUE}==$smarty.get.prodi}selected="true"{/if}>{$data.PRODI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="ui-widget-content">
            <td>Status Piutang</td>
            <td>
                <select name="status">
                    <option value="1" {if $smarty.get.status==1}selected="true"{/if}>Piutang</option>
                    <option value="0" {if $smarty.get.status==0}selected="true"{/if}>Bukan</option>
                </select>
            </td>
            <td>Tanggal Cut Off</td>
            <td>
                <select name="tgl">
                    {foreach $data_cut_off as $t}
                        <option value="{$t['CUT_OFF']}" {if $t.CUT_OFF==$smarty.get.tgl} selected="true" {/if}>{$t.CUT_OFF}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr class="center ui-widget-content">
            <td colspan="6">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" name="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
                <span class="ui-button ui-corner-all ui-state-hover" onclick="window.open('kontrol-piutang-detail.php','_blank')"  style="padding:5px;cursor:pointer;">Lihat Tampilan Detail</span>
            </td>
        </tr>

    </table>
</form>
{if isset($data_kontrol_piutang)}
    <form method="post" action="kontrol-piutang.php?{$smarty.server.QUERY_STRING}">
        <table class="ui-widget-content" style="width: 98%">
            <tr class="ui-widget-header">
                <th colspan="12" class="header-coloumn">Daftar Mahasiswa Piutang</th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Progrm Studi</th>
                <th>Fakultas</th>
                <th>Status Piutang</th>
                <th>Tanggal Cut Off</th>
                <th>Status</th>
                <th>Banyak Tagihan</th>
                <th>Besar Piutang</th>
                <th>Keterangan</th>
                <th>Perubahan</th>
            </tr>
            {foreach $data_kontrol_piutang as $data}
                <tr>
                    <td>{$data@index+1}</td>
                    <td>{$data.NIM}</td>
                    <td>{$data.NAMA}</td>
                    <td>({$data.JENJANG}) {$data.PRODI}</td>
                    <td>{$data.FAKULTAS|upper}</td>
                    <td>{if $data.STATUS_PIUTANG==1} Piutang {else} Bukan {/if}</td>
                    <td>{$data.TGL_CUT_OFF}</td>
                    <td>{$data.STATUS}</td>
                    <td class="center">{$data.BANYAK_TAGIHAN}</td>
                    <th>{number_format($data.BESAR_PIUTANG)}</th>
                    <td>{$data.KETERANGAN}</td>
                    <td>
                        <input type="hidden" name="id{$data@index+1}" value="{$data.ID_RIWAYAT_PIUTANG}"/>
                        <select disabled="true" name="status{$data@index+1}">
                            <option value="1" {if $data.STATUS_PIUTANG==1} selected="true" {/if}>Ya</option>
                            <option value="0" {if $data.STATUS_PIUTANG==0} selected="true" {/if}>Bukan</option>
                        </select>
                    </td>
                </tr>
            {foreachelse}
                <tr class="ui-widget-content">
                    <td colspan="12" class="center"><span style="color: red">Data Kosong</span></td>
                </tr>                
            {/foreach}
        </table>
    </form>
{/if}
{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}