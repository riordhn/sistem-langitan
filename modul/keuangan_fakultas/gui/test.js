$('#select_mata_kuliah').change(function(){
    if($('#select_mata_kuliah').val()!=''){
        $.ajax({
            url : '/modul/dosen/buka-nilai.php',
            type : 'post',
            data : 'mode=load_kelas&id_mata_kuliah='+$('#select_mata_kuliah').val(),
            beforeSend : function(){
                $('#table_pembukaan').html('<div style="width: 100%;" align="center"><img src="/js/loading.gif" /></div>');
            },
            success :function(data){
                $('#content').html(data);
            }
        })
    }
});
$('.status_buka').click(function(){
    var c =confirm("Apakah anda yakin membuka Kelas Ini ?");
    if(c==true){    
        $.ajax({
            type:'post',
            url:'/modul/dosen/buka-nilai.php',
            data:'mode=buka_kelas&id_kelas_mk='+$(this).val(),
            success:function(data){
                $('#content').html(data);
            }    
        });
    }else{
        $(this).attr('checked', false)
    }
});