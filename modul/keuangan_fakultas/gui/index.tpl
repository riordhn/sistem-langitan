<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="SHORTCUT ICON" href="../../img/keuangan/iconunair.ico"/>
        <link rel="stylesheet" type="text/css" href="css/themes/blue/style.css" media="print, projection, screen" />
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-keu.css" />
        <link rel="stylesheet" type="text/css" href="../../css/keuangan-style.css" />
        <link rel="stylesheet" type="text/css" href="../../css/keuangan-nav-style.css" />

        <script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
        <script language="javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        <script language="javascript" src="../../js/jquery.validate.js"></script>
        <script language="javascript" src="../../js/jquery-input-format.js"></script>
        <script language="javascript" src="../../js/keuangan-nav.js"></script>
        <script type="text/javascript" src="js/jquery.tablesorter.js"></script>
        
        <script type="text/javascript">
            var defaultRel = 'history-bayar';
            var defaultPage = 'history-bayar.php';
            id_fakultas = null;
            id_role = 10;
        </script>
        <script language="javascript" src="../../js/cybercampus.ajax-1.0.js"></script>
        <title>Keuangan - Universitas Airlangga</title>
        <style>
            .ui-widget-content a{
                text-decoration: none;
                color: brown;
            }
            .ui-widget-content a:hover{
                color: #f09a14;
            }
        </style>
    </head>
    <body>
        <div id="main_container">
            <div id="header">
                <div id="judul_header"></div>
                <div id="keterangan_fakultas" style="color: goldenrod;font-family:Trebuchet MS;font-size:11pt ;  top: 10px;left: 808px;position: absolute">
                    <h2>Fakultas {$data_fakultas_one.NM_FAKULTAS}</h2>
                </div>            
                <div id="menu_tab">
                    <ul class="topnav">
                        {foreach $struktur_menu as $m}
                            {if $m.AKSES == 1}
                                <li><a href="#{$m.NM_MODUL}!{$m.PAGE}">{$m.TITLE}</a>
                                    {if $m.SUBMENU!=NULL}
                                        <ul class="subnav">
                                            {foreach $m.SUBMENU as $sm}
                                                <li><a href="#{$sm.NM_MENU}!{$sm.PAGE}">{$sm.TITLE}</a></li>
                                                {/foreach}
                                        </ul>
                                    {/if}
                                </li>
                            {/if}
                        {/foreach}
                        <li>
                            <label style="cursor: pointer" onclick="window.location.href = '../../logout.php'">Log Out</label>
                        </li>
                        <li>
                            <label style="color: goldenrod">User Login : {$user|strtolower|capitalize}</label>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="center">
                <div id="center_content"></div>
            </div>
            <div id="footer">
                <div id="left_footer">
                    <img src="../../../img/keuangan/footer_logo.jpg" width="170" height="37" />
                </div>
                <div id="center_footer">
                    Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU <br />oleh <a target="_blank" href="http://unair.ac.id" class="disable-ajax">Universitas Airlangga</a>
                </div>
                <div id="right_footer">
                    <a href="#">home</a>
                    <a href="#">about</a>
                    <a href="#">sitemap</a>
                    <a href="#">rss</a>
                    <a href="#">contact us</a>
                </div>
            </div>
        </div>
    </body>
</html>