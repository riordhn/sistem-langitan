<div class="center_title_bar">Ganti Password</div> 

{if $result}
<div id="alert" class="ui-widget" style="margin-left: 15px; ">
	<div class="ui-state-highlight ui-corner-all" style="padding: 5px;width:50%;"> 
		<p>{$result}.</p>
	</div>
</div>
{/if}

<script type="text/javascript">
function GantiRole()
    {
        if (confirm('Apakah Anda yakin akan ganti role ?') == true)
        {
            var id_role = $('select[name="id_role"]').val();
            var id_pengguna = $('input[name="id_pengguna"]').val();
            var path = (id_fakultas == 8) ? '../' : '';
            
            $.ajax({
                type: 'POST',
                url: path + 'ubah-password.php',
                data: 'mode=change-role&id_pengguna='+id_pengguna+'&id_role='+id_role,
                success: function(data) {
                    if (data == 1)
                    {
                        alert('Anda sudah ganti role. Akun anda akan di logout.');
                        window.location = path + '../../logout.php';
                    }
                    else
                    {
                        alert('Gagal ganti role');
                    }
                }
            });
        }
    }
</script>

<form action="ubah-password.php" method="post">
<input type="hidden" name="mode" value="change-password" />
<table class="ui-widget">
	<tr class="ui-widget-header">
		<th colspan="2" class="header-coloumn">Ganti Password Form</th>
	</tr>
	<tr class="ui-widget-content">
		<td>Password lama</td>
		<td><input type="password" name="password_lama"/></td>
	</tr>
	<tr class="ui-widget-content">
		<td>Password baru</td>
		<td><input type="password" name="password_baru"/></td>
	</tr>
	<tr class="ui-widget-content">
		<td>Password baru (ulangi)</td>
		<td><input type="password" name="password_baru2"/></td>
	</tr>
	<tr class="ui-widget-content">
		<td colspan="2" class="center"><input type="submit" value="Simpan" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;"></td>
	</tr>
</table>
</form>

<table>
    <tr class="ui-widget-header">
        <th class="header-coloumn" colspan="2">Multi Role</th>
    </tr>
    <tr class="ui-widget-content">
        <td><span>Role</span></td>
        <td>
            <input type="hidden" name="id_pengguna" value="{$id_pengguna}" />
            <select name="id_role">
            {foreach $role_set as $r}
                <option value="{$r.ID_ROLE}" {if $r.ID_ROLE == $id_role}selected="selected"{/if}>{$r.NM_ROLE}</option>
            {/foreach}
            </select>
            <button onclick="GantiRole(); return false;" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;">Ganti Role</button>
        </td>
    </tr>
</table>