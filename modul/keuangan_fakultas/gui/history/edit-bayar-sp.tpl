<div class="center_title_bar">Pembayaran Semester Pendek Mahasiswa</div>
<form method="get" id="form" action="edit-bayar-sp.php">
    <table class="ui-widget">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Input Form</th>
        </tr>
        <tr class="ui-widget-content">
            <td>NIM Mahasiswa</td>
            <td><input id="cari" type="text" name="nim" value="{if isset($smarty.get.nim)}{$smarty.get.nim}{/if}"/></td>
            <td>
                <input type="hidden" name="mode" value="cari"/>
                <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" value="Tamplikan"/>
                {if $smarty.get.khusus!=''}
                    <a class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;" href="generate-bayar-sp.php">Generate</a>
                {/if}
            </td>
        </tr>
    </table>
</form>
{if $smarty.get.mode=='cari'}
    {if $data_mhs!=''}
        <table class="ui-widget-content">
            <caption>Data Mahasiswa</caption>
            <tr class="ui-widget-header">
                <th>Kolom</th>
                <th>Data</th>
            </tr>
            <tr>
                <td>Nama</td>
                <td>{$data_mhs.NM_PENGGUNA}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>{$data_mhs.NIM_MHS}</td>
            </tr>
            <tr>
                <td>Program Studi</td>
                <td>{$data_mhs.NM_JENJANG} {$data_mhs.NM_PROGRAM_STUDI}</td>
            </tr>
            <tr>
                <td>Jalur</td>
                <td>{$data_mhs.NM_JALUR}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>{$data_mhs.NM_STATUS_PENGGUNA}</td>
            </tr>
        </table>
        {if $data_pembayaran!=''}
            <table class="ui-widget-content" style="width: 60%">
                <caption>Mata Kuliah SP</caption>
                <tr class="ui-widget-header">
                    <th>NO</th>
                    <th>KODE</th>
                    <th>MATA KULIAH</th>
                    <th>SKS</th>
                    <th>Semester</th>
                    <th>STATUS</th>
                </tr>
                {foreach $data_mk_sp as $mksp}
                    <tr>
                        <td>{$mksp@index+1}</td>
                        <td>{$mksp.KODE_MK}</td>
                        <td>{$mksp.NAMA_MK}</td>
                        <td>{$mksp.SKS}</td>
                        <td>{$mksp.NM_SEMESTER} {$mksp.GROUP_SEMESTER} {$mksp.TAHUN_AJARAN}</td>
                        <td>
                            {if $mksp.STATUS_APV==1}
                                Sudah Approve
                            {else}
                                Belum Approve
                            {/if}
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="5" class="data-kosong center">Data Kosong</td>
                    </tr>
                {/foreach}
            </table>
            <table class="ui-widget-content" style="width: 60%">
                <caption>Pembayaran SP</caption>
                <tr class="ui-widget-header">
                    <th>NO</th>
                    <th>TANGGAL BAYAR</th>
                    <th>BANK</th>
                    <th>BANK VIA</th>
                    <th>JUMLAH PEMBAYARAN</th>
                    <th>JUMLAH SKS</th>
                    <th>Status</th>
                </tr>
                {foreach $data_pembayaran as $pem}
                    <tr>
                        <td>{$pem@index+1}</td>
                        <td>{$pem.TGL_BAYAR}</td>
                        <td>{$pem.NM_BANK}</td>
                        <td>{$pem.NAMA_BANK_VIA}</td>
                        <td>
                            {if $pem.TGL_BAYAR=='' and $pem.ID_BANK==''}
                                <form method="post" action="edit-bayar-sp.php?{$smarty.server.QUERY_STRING}">
                                    <input type="text" name="biaya" size="9" value="{$pem.BESAR_BIAYA}" />
                                    <input type="hidden" name="id_pembayaran" value="{$pem.ID_PEMBAYARAN_SP}"/>
                                    <input type="hidden" name="mode" value="update"/>
                                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:4px;cursor:pointer;margin:2px;font-size: 12px" value="Update"/>
                                </form>
                            {else}
                                {$pem.BESAR_BIAYA}
                            {/if}
                        </td>
                        <td>{$pem.JUMLAH_SKS}</td>
                        <td>
                            {if $pem.ID_STATUS_PEMBAYARAN==1}
                                Sudah Bayar
                            {else $pem.ID_STATUS_PEMBAYARAN==2}
                                Belum Bayar
                            {/if}
                        </td>
                    </tr>

                {foreachelse}
                    <tr>
                        <td colspan="7" class="data-kosong center">Data Kosong</td>
                    </tr>
                {/foreach}
            </table>
        {else}
            {$status}
        {/if}
    {else}
        {$status}
    {/if}
{/if}