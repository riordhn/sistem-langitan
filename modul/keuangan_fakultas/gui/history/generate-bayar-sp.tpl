<div class="center_title_bar">Auto Approve+Auto Generate Pembayaran Mahasiswa SP</div>
<form method="get" id="generate_form" action="generate-bayar-sp.php">
    <table class="ui-widget" style="width:30%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Parameter</th>
        </tr>
        <tr class="ui-widget-content">
            <td>Pilih Semester SP</td>
            <td>
                <select name="semester" onchange="$('#generate_form').submit()">
                    <option value="">Pilih Semester</option>
                    {foreach $data_semester_sp as $s}
                        <option value="{$s.ID_SEMESTER}" {if $s.ID_SEMESTER==$smarty.get.semester}selected="true"{/if}>{$s.NM_SEMESTER} {$s.GROUP_SEMESTER} {$s.TAHUN_AJARAN}</option>
                    {/foreach}
                </select>
                <input type="hidden" name="mode" value="view"/>
            </td>
        </tr>
    </table>
</form>
{if !empty($smarty.get.semester)}
    <form method="post" action="generate-bayar-sp.php?{$smarty.server.QUERY_STRING}">
        <table class="ui-widget" style="width: 98%">
            <tr class="ui-widget-header">
                <th colspan="10">DATA MAHASISWA SP</th>
            </tr>
            <tr class="ui-widget-header">
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Prodi</th>
                <th>Jalur</th>
                <th>Status</th>
                <th>Pembayaran Masuk</th>
                <th>Pembayaran Seharusnya</th>
                <th class="center">
                    Generate<br/>
                    <input type="checkbox" id="check_all_generate"/>
                </th>
            </tr>
            {foreach $data_mhs_sp as $data}
                <tr class="ui-widget-content">
                    <td>{$data@index+1}</td>
                    <td>{$data.NIM_MHS}</td>
                    <td>{$data.NM_PENGGUNA}</td>
                    <td>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</td>
                    <td>{$data.NM_JALUR}</td>
                    <td>{$data.STATUS}</td>
                    <td width="250px">
                        <ul>
                            {foreach $data.PEMBAYARAN as $p} 
                                <li>
                                    JUMLAH BAYAR : {number_format($p.BESAR_BIAYA)} <br/>
                                    TGL BAYAR : {$p.TGL_BAYAR} - {$p.NO_TRANSAKSI}<br/>
                                    BANK : {$p.NM_BANK} - {$p.NAMA_BANK_VIA}<br/>
                                </li>
                            {foreachelse}
                                <li style="color: red">Pembayaran Kosong</li>
                                {/foreach}
                        </ul>
                    </td>
                    <td width="150px">
                        Sudah Terbayar :<b>{number_format($data.TOTAL_SUDAH_BAYAR)}</b></br>
                        Harus Dibayar :<b>{number_format($data.TOTAL_BIAYA)}</b><br/>
                        SKS : <b>{$data.SKS} </b><br/>
                        Biaya/SKS : <b>{number_format($data.BESAR_BIAYA_SP)}</b>
                    </td>
                    <td class="center">
                        {if $data.TOTAL_BIAYA==0}
                            <span style="color: red">Master Biaya Kosong</span>
                        {else if ($data.TOTAL_SUDAH_BAYAR==$data.TOTAL_BIAYA&&$data.TOTAL_BIAYA>0)||($data.TOTAL_SUDAH_BAYAR>$data.TOTAL_BIAYA&&$data.TOTAL_BIAYA>0)}
                            <span style="color: green">Sudah Lunas</span>
                        {else}            
                            <input type="hidden" name="master_biaya{$data@index+1}" value="{$data.BESAR_BIAYA_SP}"/>
                            <input type="hidden" name="sks{$data@index+1}" value="{$data.SKS}"/>
                            <input class="generate_biaya" type="checkbox" name="id_mhs{$data@index+1}" value="{$data.ID_MHS}"/>
                        {/if}
                    </td>
                </tr>
            {foreachelse}
                <tr class="ui-widget-content">
                    <td colspan="10" class="center"><span style="color: red">Data Kosong</span></td>
                </tr>
            {/foreach}
            <tr>
                <td class="total center" colspan="10">
                    <input type="hidden" name="mode" value="generate" />
                    <input type="hidden" name="jumlah_data" value="{count($data_mhs_sp)}" />
                    <input type="submit" class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" value="Generate"/>
                </td>
            </tr>
        </table>
    </form>
{/if}
{literal}
    <script>
        $('#check_all_generate').click(function() {
            if ($(this).is(':checked')) {
                $('.generate_biaya').attr('checked', true);
            } else {
                $('.generate_biaya').attr('checked', false);
            }
        });
    </script>
{/literal}