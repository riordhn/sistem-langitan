<?php
include 'config.php';
include 'class/list_data.class.php';

$list = new list_data($db);

$fakultas = get('fakultas');
    $prodi = get('prodi');
    $prodos = get('prodos');
    $jmlmenit = get('jmlmenit');
    $tgl_awal = get('tgl_awal');
    $tgl_akhir = get('tgl_akhir');
    $jenis = get('jenis');
    $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI={$prodi}" : "";
    $q_jenis =  $jenis== 'utsuas' ? "AND UMK.ID_KEGIATAN!='71'" : "AND UMK.ID_KEGIATAN='71'";

$data_excel = $db->QueryToArray("
            SELECT ID_PENGGUNA,USERNAME, PENGAWAS,PRODI,ID_PROGRAM_STUDI, COUNT(JML_MENIT)||' X '||JML_MENIT AS TOTAL,JML_MENIT FROM 
                (
                SELECT X.*, FLOOR(((((((X.SELESAI-X.MULAI)*24*60*60)/3600)*3600)/60)))||' MENIT' AS JML_MENIT FROM
                (SELECT TIM.ID_PENGGUNA,PGG.USERNAME, CASE WHEN (PGG.GELAR_BELAKANG IS NULL OR PGG.GELAR_BELAKANG = '') THEN TRIM(PGG.GELAR_DEPAN||' '||UPPER(NM_PENGGUNA))
                ELSE TRIM(PGG.GELAR_DEPAN||' '||UPPER(NM_PENGGUNA)||', '||PGG.GELAR_BELAKANG) END AS PENGAWAS, CASE WHEN UMK.ID_KEGIATAN=44 THEN 'UTS' ELSE 'UAS' END AS JENIS,
                UPPER(MK.NM_MATA_KULIAH)||' ('||KUR.KREDIT_SEMESTER||' SKS)' AS MK, JJG.NM_JENJANG||'-'||NVL(PS.NM_SINGKAT_PRODI,PS.NM_PROGRAM_STUDI) AS PRODI,PS.ID_PROGRAM_STUDI,
                TO_DATE(TO_CHAR(UMK.TGL_UJIAN, 'YYYY-MM-DD')||' '||SUBSTR(UMK.JAM_MULAI,1,2)||':'||SUBSTR(UMK.JAM_MULAI,-2,2), 'YYYY-MM-DD HH24:MI:SS') AS MULAI,
                TO_DATE(TO_CHAR(UMK.TGL_UJIAN, 'YYYY-MM-DD')||' '||SUBSTR(UMK.JAM_SELESAI,1,2)||':'||SUBSTR(UMK.JAM_SELESAI,-2,2), 'YYYY-MM-DD HH24:MI:SS') AS SELESAI
                FROM AUCC.UJIAN_MK UMK
                LEFT JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK=UMK.ID_KELAS_MK
                LEFT JOIN AUCC.KURIKULUM_MK KUR ON KUR.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
                LEFT JOIN AUCC.MATA_KULIAH MK ON KUR.ID_MATA_KULIAH=MK.ID_MATA_KULIAH
                LEFT JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=KMK.ID_PROGRAM_STUDI
                LEFT JOIN AUCC.JENJANG JJG ON JJG.ID_JENJANG=PS.ID_JENJANG
                LEFT JOIN AUCC.JADWAL_UJIAN_MK JAD ON JAD.ID_UJIAN_MK=UMK.ID_UJIAN_MK
                LEFT JOIN AUCC.TIM_PENGAWAS_UJIAN TIM ON JAD.ID_JADWAL_UJIAN_MK=TIM.ID_JADWAL_UJIAN_MK
                RIGHT JOIN AUCC.PENGGUNA PGG ON PGG.ID_PENGGUNA=TIM.ID_PENGGUNA
                WHERE UMK.ID_FAKULTAS='{$fakultas}' {$q_prodi} AND UMK.JAM_MULAI IS NOT NULL AND UMK.JAM_SELESAI IS NOT NULL
                {$q_jenis} 
                AND UMK.TGL_UJIAN BETWEEN TO_DATE('{$tgl_awal}','YYYY-MM-DD') AND TO_DATE('{$tgl_akhir}','YYYY-MM-DD')) X
                GROUP BY ID_PENGGUNA,USERNAME, PENGAWAS, JENIS, MK, PRODI, MULAI, SELESAI,ID_PROGRAM_STUDI
                ORDER BY PENGAWAS, MULAI, PRODI
                )
                GROUP BY ID_PENGGUNA,USERNAME, PENGAWAS, JML_MENIT,PRODI,ID_PROGRAM_STUDI
                ORDER BY PRODI,PENGAWAS
            ");
if (get('fakultas') != '' || get('prodi') != '') {
    $fakultas = $list->get_fakultas(get('fakultas'));
    $prodi = $list->get_prodi(get('prodi'));
}
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
    }
    td{
        text-align: left;
    }
</style>
<table width='950' border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="5" class="header_text">
                <?php
                echo "Rekapitulasi Jaga Ujian<br/> Dari Tanggal {$tgl_awal} Sampai {$tgl_akhir} ";
                if ($fakultas != '') {
                    echo 'Fakultas ' . $fakultas['NM_FAKULTAS'] . '<br/>';
                }
                if ($prodi != '') {
                    echo 'Program Studi (' . $prodi['NM_JENJANG'] . ') ' . $prodi['NM_PROGRAM_STUDI'] . '<br/>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td class="header_text">NO</td>
            <td class="header_text">NIP</td>
            <td class="header_text">NAMA</td>
            <td class="header_text">PRODI</td>
            <td class="header_text">TOTAL JAM</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data_excel as $data) {
            echo "<tr>
                        <td>{$no}</td>
                        <td>&nbsp;{$data['USERNAME']}&nbsp;</td>
                        <td>{$data['PENGAWAS']}</td>
                        <td>{$data['PRODI']}</td>
                        <td>{$data['TOTAL']}</td>
                </tr>";
            $no++;
        }
        ?>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "rekap-jaga-ujian(" . date('d-m-Y_h:i:s') . ')';
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
