<?php
include 'config.php';
include 'class/list_data.class.php';
include 'class/piutang.class.php';
$piutang = new piutang($db);
$data_excel = $piutang->load_data_puitang(get('fakultas'), get('prodi'), get('status'),1);
$data_semester = $piutang->load_list_semester();
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
        text-transform:uppercase;
    }
    td{
        text-align: left;
        border: 1px solid black;
    }
    .center{
        text-align: center;
    }
</style>
<table>
    <thead>
        <tr>
            <td colspan="<?php echo count($data_semester) + 6; ?>" class="header_text">
                LAPORAN PIUTANG MAHASISWA
            </td>
        </tr>
        <tr>
            <td class="header_text">NO</td>
            <td class="header_text">NIM</td>
            <td class="header_text">NAMA</td>
            <td class="header_text">PROGRAM STUDI</td>
            <td class="header_text">STATUS</td>
            <?php
            foreach ($data_semester as $data) {
                echo '<td class="header_text">' . $data['NM_SEMESTER'] . ' <br/> ( ' . $data['TAHUN_AJARAN'] . ' )';
            }
            ?>
            <td class="header_text">Piutang Mahasiswa</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data_excel as $data) {
            $show_piutang = '';
            $show_piutang = "<tr>
				<td class='center'>{$no}</td>
				<td class='center'>'{$data['NIM_MHS']}</td>
				<td class='center'>{$data['NM_PENGGUNA']}</td>
				<td class='center'>{$data['PRODI']}</td>
                                <td class='center'>{$data['STATUS']}</td>";
            $piutang_mhs = 0;
            foreach ($data['DATA_PIUTANG'] as $data_piutang) {
                $data_bayar = $data_piutang['PIUTANG_MHS'];
                if ($data_bayar != 'Kosong' && $data_bayar != '0') {
                    if ($data_bayar < 0) {
                        $piutang_mhs +=($data_bayar * -1);
                        $pembayaran = '<font style="color:red">'.($data_bayar*-1).'</font>';
                    } else if (strpos($data_bayar, '*'))
                        $pembayaran = $data_bayar;
                    else
                        $pembayaran = $data_bayar;
                }else if ($data_bayar == '0' && $data_bayar == '') {
                    $pembayaran = 'History Belum Masuk/Data Pembayaran Belum Di Generate';
                } else {
                    if ($data_bayar = 'Kosong')
                        $pembayaran = '-';
                }
                $show_piutang .= '<td class="center">' . $pembayaran . '</td>';
            }

            $show_piutang .='<td class="center">' . $piutang_mhs . '</td>';
            $show_piutang .= "</tr>";
            echo $show_piutang;
            $no++;
            $total_piutang += $piutang_mhs;
        }
        ?>
        <tr>
            <td bgcolor='#CCFF99' style="text-align: center" colspan='<?php echo count($data_semester) + 5; ?>'><b>TOTAL PIUTANG</b></td>
            <td bgcolor='#CCFF99' style="text-align: center"><?php echo $total_piutang; ?></td>
        </tr>
    </tbody>
</table>
<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "piutang-mahasiswa__" . date('d-m-Y');
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
