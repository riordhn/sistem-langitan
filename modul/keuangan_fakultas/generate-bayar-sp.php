<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';

$list = new list_data($db);
$fakultas_user = $user->ID_FAKULTAS;

if (get('mode') == 'view') {
    $semester = get('semester');
    if (!empty($_POST)) {
       if (post('mode') == 'generate') {
            for ($i = 1; $i <= post('jumlah_data'); $i++) {
                $id_mhs = post('id_mhs' . $i);
                $biaya_sks = post('master_biaya' . $i);
                $sks = post('sks' . $i);
                $besar_biaya_sp =$biaya_sks*$sks;
                if ($id_mhs != '') {
                    $db->QuerySingle("UPDATE PENGAMBILAN_MK SET STATUS_APV_PENGAMBILAN_MK=1 WHERE ID_SEMESTER='{$semester}' AND ID_MHS='{$id_mhs}'");
                    $cek_pembayaran = $db->QuerySingle("SELECT COUNT(*) FROM PEMBAYARAN_SP WHERE ID_SEMESTER='{$semester}' AND ID_MHS='{$id_mhs}'");
                    // Jika Sudah Ada di Pembayaran
                    if ($cek_pembayaran > 0) {
                        // Jika Sudah Terbayar
                        $sudah_bayar = $db->QuerySingle("SELECT COUNT(*) FROM PEMBAYARAN_SP WHERE ID_SEMESTER='{$semester}' AND ID_MHS='{$id_mhs}' AND TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL");
                        if ($sudah_bayar == 0) {
                            $db->Query("UPDATE PEMBAYARAN_SP SET JUMLAH_SKS='{$sks}',BESAR_BIAYA='{$besar_biaya_sp}' WHERE ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester}'");
                            $db->Query("UPDATE MAHASISWA SET JENIS_TAGIHAN='SPENDEK' WHERE ID_MHS='{$id_mhs}'");
                        } else {
                            $db->Query("SELECT SUM(JUMLAH_SKS) JUMLAH_SKS FROM PEMBAYARAN_SP WHERE ID_SEMESTER='{$semester}' AND ID_MHS='{$id_mhs}' AND TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL");
                            $data_pembayaran = $db->FetchAssoc();
                            $db->Query("SELECT SUM(JUMLAH_SKS) JUMLAH_SKS FROM PEMBAYARAN_SP WHERE ID_SEMESTER='{$semester}' AND ID_MHS='{$id_mhs}' AND TGL_BAYAR IS NULL AND ID_BANK IS NULL");
                            $data_tagihan = $db->FetchAssoc();
                            // Jika sudah bayar namun ada mata kuliah sp yang masih belum terbayar
                            if ($data_pembayaran['JUMLAH_SKS'] < $sks && ($data_tagihan['JUMLAH_SKS'] + $data_pembayaran['JUMLAH_SKS']) != $sks) {
                                $sks_belum_terbayar = $sks - $data_pembayaran['JUMLAH_SKS'];
                                $besar_biaya_sp = $biaya_sks * $sks_belum_terbayar;
                                $db->Query("
                                INSERT INTO PEMBAYARAN_SP
                                    (ID_MHS,ID_SEMESTER,JUMLAH_SKS,BESAR_BIAYA,IS_TAGIH,ID_STATUS_PEMBAYARAN,KETERANGAN,IS_TARIK)
                                VALUES
                                    ('{$id_mhs}','{$semester}','{$sks_belum_terbayar}','{$besar_biaya_sp}','Y','2','SEMESTER PENDEK',0)
                                ");
                                $db->Query("UPDATE MAHASISWA SET JENIS_TAGIHAN='SPENDEK' WHERE ID_MHS='{$id_mhs}'");
                            }
                        }
                    }// Jika Belum ada
                    else {
                        $db->Query("
                        INSERT INTO PEMBAYARAN_SP
                            (ID_MHS,ID_SEMESTER,JUMLAH_SKS,BESAR_BIAYA,IS_TAGIH,ID_STATUS_PEMBAYARAN,KETERANGAN,IS_TARIK)
                        VALUES
                            ('{$id_mhs}','{$semester}','{$sks}','{$besar_biaya_sp}','Y','2','SEMESTER PENDEK',0)
                        ");
                        $db->Query("UPDATE MAHASISWA SET JENIS_TAGIHAN='SPENDEK' WHERE ID_MHS='{$id_mhs}'");
                    }
                }
            }
        }
    }
    $data_mhs_sp = array();
    $mahasiswa_sp = $db->QueryToArray("
    SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.ID_JALUR,JAL.NM_JALUR,SP.NM_STATUS_PENGGUNA STATUS,PSPS.TOTAL_SUDAH_BAYAR,
    BSP.BESAR_BIAYA_SP,SUM(KUMK.KREDIT_SEMESTER) SKS,(SUM(KUMK.KREDIT_SEMESTER)*BSP.BESAR_BIAYA_SP) TOTAL_BIAYA
    FROM AUCC.PENGAMBILAN_MK PMK
    JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK=PMK.ID_KELAS_MK
    JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
    JOIN AUCC.MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
    JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
    JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
    JOIN AUCC.JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND JM.ID_JALUR_AKTIF=1
    JOIN AUCC.JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
    JOIN AUCC.STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA=M.STATUS_AKADEMIK_MHS
    JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
    LEFT JOIN AUCC.BIAYA_SP BSP ON BSP.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI AND BSP.ID_JALUR=JAL.ID_JALUR AND BSP.ID_SEMESTER='{$semester}'
    LEFT JOIN
    (
        SELECT ID_MHS,SUM(BESAR_BIAYA) TOTAL_SUDAH_BAYAR
        FROM PEMBAYARAN_SP
        WHERE TGL_BAYAR IS NOT NULL AND ID_BANK IS NOT NULL
        AND ID_SEMESTER='{$semester}'
        GROUP BY ID_MHS
    ) PSPS ON PSPS.ID_MHS=M.ID_MHS
    WHERE PMK.ID_SEMESTER='{$semester}' AND PS.ID_FAKULTAS='{$fakultas_user}'
    GROUP BY M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,JAL.ID_JALUR,JAL.NM_JALUR,BSP.BESAR_BIAYA_SP,SP.NM_STATUS_PENGGUNA,PSPS.TOTAL_SUDAH_BAYAR
    ");
    foreach ($mahasiswa_sp as $m) {
        $pembayaran = $db->QueryToArray("
            SELECT PEM.*,B.NM_BANK,BV.NAMA_BANK_VIA 
            FROM PEMBAYARAN_SP PEM
            LEFT JOIN BANK B ON PEM.ID_BANK=B.ID_BANK
            LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA=PEM.ID_BANK_VIA
            WHERE PEM.ID_MHS='{$m['ID_MHS']}' AND PEM.ID_SEMESTER='{$semester}'
            ");
        array_push($data_mhs_sp, array_merge($m, array('PEMBAYARAN' => $pembayaran)));
    }
    $smarty->assign('data_mhs_sp', $data_mhs_sp);
}
$smarty->assign('id_fakultas', $user->ID_FAKULTAS);
$smarty->assign('error',alert_error("Akses ditolak",90));
$smarty->assign('data_semester_sp', $list->load_list_semester_sp());
$smarty->display('utility/generate-bayar-sp.tpl');

