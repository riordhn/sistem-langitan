<?php

include 'config.php';
include '../keuangan/class/laporan.class.php';
include '../keuangan/class/list_data.class.php';
include '../keuangan/class/paging.class.php';

$laporan = new laporan($db);
$list = new list_data($db);

if (isset($_GET)) {
    $fakultas = get('fakultas');
    $prodi = get('prodi');
    $prodos = get('prodos');
    $jmlmenit = get('jmlmenit');
    $tgl_awal = get('tgl_awal');
    $tgl_akhir = get('tgl_akhir');
    $jenis = get('jenis');
    $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI={$prodi}" : "";
    $q_jenis =  $jenis== 'utsuas' ? "AND UMK.ID_KEGIATAN!='71'" : "AND UMK.ID_KEGIATAN='71'";
    if (get('mode') == 'tampil') {
        $data_rekap = $db->QueryToArray("
            SELECT ID_PENGGUNA,USERNAME, PENGAWAS,PRODI,ID_PROGRAM_STUDI, COUNT(JML_MENIT)||' X '||JML_MENIT AS TOTAL,JML_MENIT FROM 
                (
                SELECT X.*, FLOOR(((((((X.SELESAI-X.MULAI)*24*60*60)/3600)*3600)/60)))||' MENIT' AS JML_MENIT FROM
                (SELECT TIM.ID_PENGGUNA,PGG.USERNAME, CASE WHEN (PGG.GELAR_BELAKANG IS NULL OR PGG.GELAR_BELAKANG = '') THEN TRIM(PGG.GELAR_DEPAN||' '||UPPER(NM_PENGGUNA))
                ELSE TRIM(PGG.GELAR_DEPAN||' '||UPPER(NM_PENGGUNA)||', '||PGG.GELAR_BELAKANG) END AS PENGAWAS, CASE WHEN UMK.ID_KEGIATAN=44 THEN 'UTS' ELSE 'UAS' END AS JENIS,
                UPPER(MK.NM_MATA_KULIAH)||' ('||KUR.KREDIT_SEMESTER||' SKS)' AS MK, JJG.NM_JENJANG||'-'||NVL(PS.NM_SINGKAT_PRODI,PS.NM_PROGRAM_STUDI) AS PRODI,PS.ID_PROGRAM_STUDI,
                TO_DATE(TO_CHAR(UMK.TGL_UJIAN, 'YYYY-MM-DD')||' '||SUBSTR(UMK.JAM_MULAI,1,2)||':'||SUBSTR(UMK.JAM_MULAI,-2,2), 'YYYY-MM-DD HH24:MI:SS') AS MULAI,
                TO_DATE(TO_CHAR(UMK.TGL_UJIAN, 'YYYY-MM-DD')||' '||SUBSTR(UMK.JAM_SELESAI,1,2)||':'||SUBSTR(UMK.JAM_SELESAI,-2,2), 'YYYY-MM-DD HH24:MI:SS') AS SELESAI
                FROM AUCC.UJIAN_MK UMK
                LEFT JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK=UMK.ID_KELAS_MK
                LEFT JOIN AUCC.KURIKULUM_MK KUR ON KUR.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
                LEFT JOIN AUCC.MATA_KULIAH MK ON KUR.ID_MATA_KULIAH=MK.ID_MATA_KULIAH
                LEFT JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=KMK.ID_PROGRAM_STUDI
                LEFT JOIN AUCC.JENJANG JJG ON JJG.ID_JENJANG=PS.ID_JENJANG
                LEFT JOIN AUCC.JADWAL_UJIAN_MK JAD ON JAD.ID_UJIAN_MK=UMK.ID_UJIAN_MK
                LEFT JOIN AUCC.TIM_PENGAWAS_UJIAN TIM ON JAD.ID_JADWAL_UJIAN_MK=TIM.ID_JADWAL_UJIAN_MK
                RIGHT JOIN AUCC.PENGGUNA PGG ON PGG.ID_PENGGUNA=TIM.ID_PENGGUNA
                WHERE UMK.ID_FAKULTAS='{$fakultas}' {$q_prodi} AND UMK.JAM_MULAI IS NOT NULL AND UMK.JAM_SELESAI IS NOT NULL
                {$q_jenis} 
                AND UMK.TGL_UJIAN BETWEEN TO_DATE('{$tgl_awal}','YYYY-MM-DD') AND TO_DATE('{$tgl_akhir}','YYYY-MM-DD')) X
                GROUP BY ID_PENGGUNA,USERNAME, PENGAWAS, JENIS, MK, PRODI, MULAI, SELESAI,ID_PROGRAM_STUDI
                ORDER BY PENGAWAS, MULAI, PRODI
                )
                GROUP BY ID_PENGGUNA,USERNAME, PENGAWAS, JML_MENIT,PRODI,ID_PROGRAM_STUDI
                ORDER BY PRODI,PENGAWAS
            ");

        $smarty->assign(($jenis == 'utsuas' ? 'data_rekap_utsuas' : 'data_rekap_sidang'), $data_rekap);
    } else if (get('mode') == 'detail') {
        $id = get('id');
        
        $data_detail = $db->QueryToArray("
              SELECT X.*, FLOOR(((((((X.SELESAI-X.MULAI)*24*60*60)/3600)*3600)/60)))||' MENIT' AS JML_MENIT FROM
            (SELECT UMK.JAM_MULAI,UMK.JAM_SELESAI,PGG.USERNAME, CASE WHEN (PGG.GELAR_BELAKANG IS NULL OR PGG.GELAR_BELAKANG = '') THEN TRIM(PGG.GELAR_DEPAN||' '||UPPER(NM_PENGGUNA))
            ELSE TRIM(PGG.GELAR_DEPAN||' '||UPPER(NM_PENGGUNA)||', '||PGG.GELAR_BELAKANG) END AS PENGAWAS, CASE WHEN UMK.ID_KEGIATAN=44 THEN 'UTS' WHEN UMK.ID_KEGIATAN=71 THEN 'SIDANG' ELSE 'UAS' END AS JENIS,
            UPPER(MK.NM_MATA_KULIAH)||' ('||KUR.KREDIT_SEMESTER||' SKS)' AS MK, JJG.NM_JENJANG||'-'||NVL(PS.NM_SINGKAT_PRODI,PS.NM_PROGRAM_STUDI) AS PRODI,PS.ID_PROGRAM_STUDI,
            TO_DATE(TO_CHAR(UMK.TGL_UJIAN, 'YYYY-MM-DD')||' '||SUBSTR(UMK.JAM_MULAI,1,2)||':'||SUBSTR(UMK.JAM_MULAI,-2,2), 'YYYY-MM-DD HH24:MI:SS') AS MULAI,
            TO_DATE(TO_CHAR(UMK.TGL_UJIAN, 'YYYY-MM-DD')||' '||SUBSTR(UMK.JAM_SELESAI,1,2)||':'||SUBSTR(UMK.JAM_SELESAI,-2,2), 'YYYY-MM-DD HH24:MI:SS') AS SELESAI
            FROM AUCC.UJIAN_MK UMK
            LEFT JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK=UMK.ID_KELAS_MK
            LEFT JOIN AUCC.KURIKULUM_MK KUR ON KUR.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
            LEFT JOIN AUCC.MATA_KULIAH MK ON KUR.ID_MATA_KULIAH=MK.ID_MATA_KULIAH
            LEFT JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=KMK.ID_PROGRAM_STUDI
            LEFT JOIN AUCC.JENJANG JJG ON JJG.ID_JENJANG=PS.ID_JENJANG
            LEFT JOIN AUCC.JADWAL_UJIAN_MK JAD ON JAD.ID_UJIAN_MK=UMK.ID_UJIAN_MK
            LEFT JOIN AUCC.TIM_PENGAWAS_UJIAN TIM ON JAD.ID_JADWAL_UJIAN_MK=TIM.ID_JADWAL_UJIAN_MK
            RIGHT JOIN AUCC.PENGGUNA PGG ON PGG.ID_PENGGUNA=TIM.ID_PENGGUNA
            WHERE UMK.ID_FAKULTAS='{$fakultas}' AND PS.ID_PROGRAM_STUDI='{$prodos}' AND UMK.JAM_MULAI IS NOT NULL AND UMK.JAM_SELESAI IS NOT NULL AND TIM.ID_PENGGUNA='{$id}'
            {$q_jenis} 
            AND UMK.TGL_UJIAN BETWEEN TO_DATE('{$tgl_awal}','YYYY-MM-DD') AND TO_DATE('{$tgl_akhir}','YYYY-MM-DD')) X
            GROUP BY USERNAME,JAM_MULAI,JAM_SELESAI, PENGAWAS, JENIS, MK, PRODI, MULAI, SELESAI,ID_PROGRAM_STUDI
            HAVING FLOOR(((((((X.SELESAI-X.MULAI)*24*60*60)/3600)*3600)/60)))||' MENIT'='{$jmlmenit}'
            ORDER BY PRODI,PENGAWAS, MULAI
            ");
        $smarty->assign(($jenis == 'utsuas' ? 'data_detail_utsuas' : 'data_detail_sidang'), $data_detail);
    }
}

$smarty->assign('info1', alert_success("Harap Mengisi Tarif Honorarium pada menu Tarif Honorarium ", "90"));
$smarty->assign('tgl_awal', get('tgl_awal'));
$smarty->assign('tgl_akhir', get('tgl_akhir'));
$smarty->assign('fakultas', get('fakultas'));
$smarty->assign('prodi', get('prodi'));
$smarty->assign('id_fakultas', $user->ID_FAKULTAS);
$smarty->assign('data_prodi', $list->load_list_prodi($user->ID_FAKULTAS));
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_jalur', $list->load_list_jalur());
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->display('honor/rekap-jaga.tpl');
?>
