<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';
include '../keuangan/class/piutang.class.php';
$list = new list_data($db);
$piutang = new piutang($db);


if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        if (isset($_POST)) {
            if (post('mode') == 'ubah') {
                for ($i = 1; $i <= post('jumlah_data'); $i++) {
                    $id=post('id'.$i);
                    $status=post('status'.$i);
                    $db->Query("UPDATE RIWAYAT_PIUTANG SET STATUS_PIUTANG='{$status}' WHERE ID_RIWAYAT_PIUTANG='{$id}'");
                }
            }
        }
        $smarty->assign('data_kontrol_piutang', $piutang->load_data_kontrol_piutang(get('fakultas'), get('prodi'), get('status'), get('tgl')));
    }
}
$smarty->assign('id_fakultas',$user->ID_FAKULTAS);
$smarty->assign('data_prodi', $piutang->load_prodi_piutang());
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_status', $piutang->load_list_status());
$smarty->assign('data_cut_off', $list->load_tgl_cut_off_piutang());
$smarty->assign('data_status_piutang', $list->load_status_piutang());
$smarty->display('kontrol/kontrol-piutang.tpl');
?>
