<?php

include 'config.php';
include '../keuangan/funtion-tampil-informasi.php';

if (isset($_GET)) {
    if (get('mode') == 'cari') {
        $nim = addslashes(get('nim'));
        // Cari Data Mahasiswa
        $db->Query("
            SELECT M.ID_MHS,P.NM_PENGGUNA,M.NIM_MHS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,SP.NM_STATUS_PENGGUNA
            FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND JM.ID_JALUR_AKTIF=1
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA=M.STATUS_AKADEMIK_MHS
            WHERE M.NIM_MHS='{$nim}'");
        $data_mahasiswa = $db->FetchAssoc();
        if ($data_mahasiswa != '') {
            // Update
            if (isset($_POST)) {
                if (post('mode') == 'update') {
                    $biaya = post('biaya');
                    $id_pembayaran = post('id_pembayaran');
                    $db->Query("UPDATE PEMBAYARAN_SP SET BESAR_BIAYA='{$biaya}' WHERE ID_PEMBAYARAN_SP='{$id_pembayaran}'");
                }
            }
            // Cari Mata Kuliah SP
            $data_mk_sp = $db->QueryToArray("
                SELECT PMK.ID_PENGAMBILAN_MK AS ID_PENGAMBILAN_MK, MK.KD_MATA_KULIAH AS KODE_MK,MK.NM_MATA_KULIAH AS NAMA_MK,
                    KUMK.KREDIT_SEMESTER AS SKS,PMK.STATUS_APV_PENGAMBILAN_MK AS STATUS_APV,PMK.STATUS_PENGAMBILAN_MK AS STATUS,S.NM_SEMESTER,S.GROUP_SEMESTER,S.TAHUN_AJARAN
                    FROM PENGAMBILAN_MK PMK
                    LEFT JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
                    LEFT JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PMK.ID_KELAS_MK
                    LEFT JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
                    LEFT JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
                    LEFT JOIN SEMESTER S ON PMK.ID_SEMESTER =  S.ID_SEMESTER
                WHERE M.ID_MHS = '{$data_mahasiswa['ID_MHS']}' AND S.STATUS_AKTIF_SP='True' AND PMK.STATUS_PENGAMBILAN_MK !=0
                GROUP BY PMK.ID_PENGAMBILAN_MK,MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,KUMK.KREDIT_SEMESTER,PMK.STATUS_APV_PENGAMBILAN_MK
                ,PMK.STATUS_PENGAMBILAN_MK,S.NM_SEMESTER,S.GROUP_SEMESTER,S.TAHUN_AJARAN
                ORDER BY MK.NM_MATA_KULIAH");
            // Cari tagiha SP
            $data_pembayaran_sp = $db->QueryToArray("
                SELECT PSP.*,B.NM_BANK,BV.NAMA_BANK_VIA
                FROM PEMBAYARAN_SP PSP
                LEFT JOIN BANK B ON B.ID_BANK=PSP.ID_BANK
                LEFT JOIN BANK_VIA BV ON BV.ID_BANK_VIA=PSP.ID_BANK_VIA
                WHERE PSP.ID_MHS='{$data_mahasiswa['ID_MHS']}'");
            if ($data_pembayaran_sp != '') {
                $smarty->assign('data_mhs', $data_mahasiswa);
                $smarty->assign('data_mk_sp', $data_mk_sp);
                $smarty->assign('data_pembayaran', $data_pembayaran_sp);
            } else {
                $status = alert_error("Pembayaran Semester pendek tidak ditemukan...");
            }
        } else {
            $status.=alert_error("Data Mahasiswa tidak ditemukan...");
        }
        $smarty->assign('status', $status);
    }
}

$smarty->display('history/edit-bayar-sp.tpl');
?>
