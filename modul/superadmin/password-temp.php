<?php
include('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_SUPERADMIN){
	header("location: /logout.php");
    exit();
}

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
    if (post('mode') == 'reset')
    {
        $id_pengguna = post('id_pengguna');
        $username = post('username');
        $pass = post('pass');
        
        $password_temp = sha1("1");
        $result = $db->Query("update pengguna set password_hash = '{$password_temp}', password_hash_temp = '{$pass}' where id_pengguna = {$id_pengguna}");

        $smarty->assign('edited', $result ? "Password berhasil direset" : "Password gagal direset");
    }

    if (post('mode') == 'kembali')
    {
        $id_pengguna = post('id_pengguna');
        $username = post('username');
        $pass_temp = post('pass_temp');
        
        //$password_hash = sha1($username);
        $result = $db->Query("update pengguna set password_hash = '{$pass_temp}', password_hash_temp = null where id_pengguna = {$id_pengguna}");

        $smarty->assign('edited', $result ? "Password berhasil dikembalikan" : "Password gagal direset");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $q = strtoupper(get('q', ''));

        //$password_temp = sha1("1");
        //$smarty->assign('password_temp', $password_temp);
        
        if (strlen(trim($q)) >= 3)
        {
            $pengguna_set = $db->QueryToArray("
                select p.id_pengguna, p.username, p.password_hash, p.password_hash_temp, upper(p.nm_pengguna) as nm_pengguna, p.join_table, r.nm_role, pt.nama_singkat
                from pengguna p
                join role r on r.id_role = p.id_role
                left join role_pengguna rp on rp.id_pengguna = p.id_pengguna
                left join perguruan_tinggi pt on p.id_perguruan_tinggi = pt.id_perguruan_tinggi
                where p.id_role not in (6, 28) and (upper(p.nm_pengguna) like '%{$q}%' or p.username like '%{$q}%' or upper(r.nm_role) like '%{$q}%')
                group by p.id_pengguna, p.username, p.password_hash, p.password_hash_temp, p.nm_pengguna, p.join_table, r.nm_role, pt.nama_singkat
                order by p.nm_pengguna");
            $smarty->assign('pengguna_set', $pengguna_set);
        }
    }
    
    if ($mode == 'reset' or $mode == 'kembali')
    {
        $id_pengguna = get('id_pengguna');
        
        $pengguna = $db->QueryToArray("
            select id_pengguna, username, nm_pengguna, password_hash, password_hash_temp
			from pengguna
            where id_pengguna = {$id_pengguna}");
        $smarty->assign('pengguna', $pengguna[0]);
    }
    
}

$smarty->display("password-temp/{$mode}.tpl");
?>
