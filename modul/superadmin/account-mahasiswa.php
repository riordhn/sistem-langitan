<?php
include 'config.php';
include 'class/Prodi.php';
include 'class/Fakultas.php';

$id_pt = $id_pt_user;

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_SUPERADMIN){
	header("location: /logout.php");
    exit();
}

$mode = get('mode', 'list');

$Fakultas = new Fakultas($db,$id_pt);
$Prodi = new Prodi($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'ubahPassword')
    {
        $id_pengguna = post('id_pengguna');
        $nim_mhs = post('nim_mhs');

        $password_hash = sha1($nim_mhs);

        $result = $db->Query("update pengguna set password_hash = '{$password_hash}', password_must_change = '1' where id_pengguna = {$id_pengguna}");

        $smarty->assign('edited', $result ? "Password berhasil direset" : "Password gagal direset");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'list')
    {
    	$id_fakultas 	= (int)get('id_fakultas', '');
		$id_prodi 		= (int)get('id_prodi', '');

        $smarty->assign('fakultas_set', $Fakultas->GetList());

        if ($id_fakultas != '')
		{
        	$smarty->assign('prodi_set', $Prodi->GetList($id_fakultas));
    	}
        if ($id_fakultas != '' && $id_prodi != '')
        {
            $mhs_set = $db->QueryToArray("select m.id_mhs, m.nim_mhs, m.id_pengguna, p.nm_pengguna, ps.nm_program_studi, j.nm_jenjang_sngkat
                                             from mahasiswa m, pengguna p, program_studi ps, jenjang j
                                             WHERE m.id_pengguna = p.id_pengguna and m.id_program_studi = ps.id_program_studi
                                                    and ps.id_jenjang = j.id_jenjang and m.id_program_studi = {$id_prodi} ORDER BY ID_MHS");

            $smarty->assignByRef('mhs_set', $mhs_set);
        }
    }
    if($mode == 'edit'){
        $id_mhs       = (int)get('id_mhs', '');

        $mhs = $db->QueryToArray("select m.id_mhs, m.nim_mhs, m.id_pengguna, p.nm_pengguna, m.id_program_studi, ps.id_fakultas, ps.nm_program_studi, j.nm_jenjang_sngkat
                                             from mahasiswa m, pengguna p, program_studi ps, jenjang j
                                             WHERE m.id_pengguna = p.id_pengguna and m.id_program_studi = ps.id_program_studi
                                                    and ps.id_jenjang = j.id_jenjang and m.id_mhs = {$id_mhs}");
        $smarty->assign('mhs', $mhs[0]);

    }
}

$smarty->display("account/mahasiswa/{$mode}.tpl");
?>