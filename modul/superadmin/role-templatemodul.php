<?php
include 'config.php';

$mode = get('mode', 'view');

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	if ($mode == 'view')
	{
		$id_role 	= (int)get('id_role', '');

		$role_set = $db->QueryToArray("SELECT R.* FROM AUCC.ROLE R ORDER BY R.NM_ROLE");
		$smarty->assignByRef('role_set', $role_set);

		if ($id_role != '')
		{
			// Mengambil row ROLE
			$db->Query("SELECT * FROM AUCC.ROLE WHERE ID_ROLE = {$id_role}");
			$role = $db->FetchAssoc();

			// Mengambil list Modul
			$modul_set = $db->QueryToArray("SELECT M1.* FROM MODUL M1 WHERE M1.ID_ROLE = {$id_role} ORDER BY M1.URUTAN");
			$smarty->assignByRef('modul_set', $modul_set);

			// Mengambil list Template
			$template_role_set = $db->QueryToArray("SELECT * FROM TEMPLATE_ROLE WHERE ID_ROLE = {$id_role} ORDER BY NM_TEMPLATE");
			$smarty->assignByRef('template_role_set', $template_role_set);

			// Mengambil Data Template_modul
			$template_modul_set = $db->QueryToArray(
				"SELECT * FROM TEMPLATE_MODUL WHERE ID_TEMPLATE_ROLE IN (
  					SELECT ID_TEMPLATE_ROLE FROM TEMPLATE_ROLE WHERE ID_ROLE = {$id_role})");
			$smarty->assignByRef('template_modul_set', $template_modul_set);
		}
	}

	if ($mode == 'set-template-modul')
	{
		$action				= get('action');
		$id_template_role	= get('id-t-role');
		$id_modul			= get('id_modul');
		$akses				= get('akses');

		exit();
	}
}

$smarty->display("role/template_modul/{$mode}.tpl");