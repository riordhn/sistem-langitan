<?php
include('config.php');

$smarty->assign('state','');
$sql = "SELECT count (KELAS_MK.ID_KELAS_MK)as jumlah_kelas,ps.id_program_studi,ps.NM_PROGRAM_STUDI from kelas_mk 
			join PROGRAM_STUDI ps on ps.ID_PROGRAM_STUDI = KELAS_MK.ID_PROGRAM_STUDI 
				where KELAS_MK.ID_SEMESTER = 200 
					group by KELAS_MK.id_program_Studi,ps.id_program_studi,PS.NM_PROGRAM_STUDI
						order by PS.NM_PROGRAM_STUDI";

$parsingData = $db->QueryToArray($sql);

foreach ($parsingData as $key => $value) {
	# code...
	
	$id_program_studi = $value['ID_PROGRAM_STUDI'];
	$sql_jml_peserta = "SELECT count(*) as jumlah_peserta from pengambilan_mk
						join  kelas_mk kls on KLS.id_kelas_mk = PENGAMBILAN_MK.id_kelas_mk
							where kls.id_semester = 200 and id_program_studi = '{$id_program_studi}'
								group by id_program_studi";
	$t = $db->QueryToArray($sql_jml_peserta);

	$sql_nilai_masuk = "SELECT count(*) AS JML_NILAI_MASUK from PENGAMBILAN_MK 
						JOIN KELAS_MK ON KELAS_MK.ID_KELAS_MK = PENGAMBILAN_MK.ID_KELAS_MK 
						WHERE KELAS_MK.ID_SEMESTER = 200 and ID_PROGRAM_STUDI = '{$id_program_studi}'
						AND NILAI_ANGKA IS NOT NULL";
	$u = $db->QueryToArray($sql_nilai_masuk);

	$sql_nilai_blm_masuk = "SELECT count(*) AS JML_NILAI_BLM_MASUK from PENGAMBILAN_MK 
							JOIN KELAS_MK ON KELAS_MK.ID_KELAS_MK = PENGAMBILAN_MK.ID_KELAS_MK 
							WHERE KELAS_MK.ID_SEMESTER = 200 and ID_PROGRAM_STUDI = '{$id_program_studi}'
							AND NILAI_ANGKA IS NULL";
	$v = $db->QueryToArray($sql_nilai_blm_masuk);

	$parsingData[$key]['JUMLAH_PESERTA'] = $t[0]['JUMLAH_PESERTA'];
	$parsingData[$key]['JML_NILAI_MASUK'] = $u[0]['JML_NILAI_MASUK'];
	$parsingData[$key]['JML_NILAI_BLM_MASUK'] = $v[0]['JML_NILAI_BLM_MASUK'];
}

	

if($_GET['detail'] != ''){
	$id_program_studi = $_GET['detail'];
	$sqls = "SELECT NAMA_KELAS,KELAS_MK.ID_KELAS_MK AS ID_KELAS_MK,NM_MATA_KULIAH,KURIKULUM_MK.TINGKAT_SEMESTER 
				AS TINGKAT_SEMESTER ,KURIKULUM_MK.KREDIT_SEMESTER AS KREDIT_SEMESTER,NM_PENGGUNA 
				FROM KELAS_MK 
				JOIN PENGAMPU_MK ON PENGAMPU_MK.ID_KELAS_MK = KELAS_MK.ID_KELAS_MK
				JOIN NAMA_KELAS ON NAMA_KELAS.ID_NAMA_KELAS = KELAS_MK.NO_KELAS_MK
				JOIN JADWAL_KELAS ON JADWAL_KELAS.ID_KELAS_MK = KELAS_MK.ID_KELAS_MK
				JOIN KURIKULUM_MK ON KURIKULUM_MK.ID_KURIKULUM_MK = KELAS_MK.ID_KURIKULUM_MK
				JOIN MATA_KULIAH ON MATA_KULIAH.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH
				JOIN DOSEN ON DOSEN.ID_DOSEN = PENGAMPU_MK.ID_DOSEN
				JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA =  DOSEN.ID_PENGGUNA
				where KELAS_MK.ID_SEMESTER = '200' AND KELAS_MK.ID_PROGRAM_STUDI = '{$id_program_studi}'
				ORDER BY NM_MATA_KULIAH,NAMA_KELAS,NM_PENGGUNA
				";
	$parsingDatas = $db->QueryToArray($sqls);

	foreach ($parsingDatas as $key => $value) {
		$id_kelas_mk = $value['ID_KELAS_MK'];
		$sql_get_peserta = "SELECT count(*) AS JML FROM pengambilan_mk WHERE id_kelas_mk = '{$id_kelas_mk}'";
		$sql_nilai_masuk = "SELECT count(*) AS JML from pengambilan_mk WHERE id_kelas_mk = '{$id_kelas_mk}'
								AND (NILAI_ANGKA IS NOT NULL OR NILAI_ANGKA <> 0)";
		$sql_nilai_belum_masuk = "SELECT count(*) AS JML from pengambilan_mk WHERE id_kelas_mk = '{$id_kelas_mk}'
								AND NILAI_ANGKA IS  NULL";
		$u = $db->QueryToArray($sql_get_peserta);
		$v = $db->QueryToArray($sql_nilai_masuk);
		$w = $db->QueryToArray($sql_nilai_belum_masuk);
		$parsingDatas[$key]['JUMLAH_PESERTA'] = $u[0]['JML'];
		$parsingDatas[$key]['JUMLAH_NILAI_MASUK'] = $v[0]['JML'];
		$parsingDatas[$key]['JUMLAH_NILAI_BLM_MASUK'] = $w[0]['JML'];
	}

	$smarty->assign('state','detail');
	$smarty->assign('data_detail', $parsingDatas);
}


	
$smarty->assign('data', $parsingData);
$smarty->display('laporan.tpl');

?>
