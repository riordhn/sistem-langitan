<?php
include 'config.php';

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_menu = post('id_menu');
        $nm_menu = post('nm_menu');
        $title = post('title');
        $page = post('page');
        $urutan = post('urutan');
        $akses = post('akses');
        
        $result = $db->Query("update menu set nm_menu = '{$nm_menu}', title = '{$title}', page = '{$page}',
        						urutan = '{$urutan}', akses = '{$akses}' where id_menu = '{$id_menu}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }
    if (post('mode') == 'add')
    {
        $id_modul = post('id_modul');
        $nm_menu = post('nm_menu');
        $title = post('title');
        $page = post('page');
        $urutan = post('urutan');
        $akses = post('akses');
        
        $result = $db->Query("insert into menu (id_modul, nm_menu, title, page, urutan, akses) 
        						values ('{$id_modul}','{$nm_menu}','{$title}','{$page}','{$urutan}','{$akses}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$id_role 	= (int)get('id_role', '');
		$id_modul 	= (int)get('id_modul', '');

		$role_set = $db->QueryToArray("SELECT R.* FROM AUCC.ROLE R ORDER BY R.NM_ROLE");
		$smarty->assignByRef('role_set', $role_set);

		if ($id_role != '')
		{
			// Mengambil row ROLE
			$db->Query("SELECT * FROM AUCC.ROLE WHERE ID_ROLE = {$id_role}");
			$role = $db->FetchAssoc();

			// Mengambil List Modul
			$modul_set = $db->QueryToArray("SELECT M1.* FROM MODUL M1 WHERE M1.ID_ROLE = {$id_role} ORDER BY M1.URUTAN");
			$smarty->assignByRef('modul_set', $modul_set);
		}

		if ($id_role != '' && $id_modul != '')
		{
			$menu_set = $db->QueryToArray("SELECT * FROM MENU WHERE ID_MODUL = {$id_modul} ORDER BY URUTAN");

			// Mengecek eksistensi file
			foreach ($menu_set as &$menu)
			{
				if (file_exists($html_root . $role['PATH'] . $menu['PAGE']))
					$menu['FILE_EXISTS'] = 1;
				else
					$menu['FILE_EXISTS'] = 0;
			}

			$smarty->assignByRef('menu_set', $menu_set);
			$smarty->assign('id_role', $id_role);
		}
	}
	else if($mode == 'edit')
	{
		$id_modul 	= (int)get('id_modul', '');
		$id_menu 	= (int)get('id_menu', '');

		$menu = $db->QueryToArray("
            select id_menu, nm_menu, title, page, urutan, akses 
            from menu 
            where id_menu = {$id_menu}");
        $smarty->assign('menu', $menu[0]);
	}
	else if($mode == 'add')
	{
		$id_role 	= (int)get('id_role', '');
		$id_modul 	= (int)get('id_modul', '');

		$role_set = $db->QueryToArray("SELECT R.* FROM AUCC.ROLE R ORDER BY R.NM_ROLE");
		$smarty->assignByRef('role_set', $role_set);

		$role = $db->QueryToArray("SELECT R.* FROM AUCC.ROLE R WHERE ID_ROLE = {$id_role}");
		$smarty->assign('role', $role[0]);

        $modul_set = $db->QueryToArray("
            select id_modul, nm_modul, title, urutan 
            from modul
            order by nm_modul");
        $smarty->assignByRef('modul_set', $modul_set);

        $modul = $db->QueryToArray("
            select id_modul, nm_modul, title, urutan 
            from modul
            where id_modul = {$id_modul}");
        $smarty->assign('modul', $modul[0]);
	}
}

$smarty->display("role/menu/{$mode}.tpl");