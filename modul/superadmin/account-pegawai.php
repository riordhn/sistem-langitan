<?php
include 'config.php';
include 'class/Account.class.php';
include 'class/Role.class.php';

$id_pt = $id_pt_user;

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_SUPERADMIN){
	header("location: /logout.php");
    exit();
}

$mode = get('mode', 'list');
//echo "aa ".$id_pt;
$Account = new Account($db,$id_pt);
$Role = new Role($db,$id_pt);

if ($request_method == 'GET')
{
    if ($mode == 'list')
    {
        $smarty->assign('pegawai_set', $Account->GetListPegawai(get('id_role')));
        $smarty->assign('role_set', $Role->GetListRolePegawai());
    }
}

$smarty->display("account/pegawai/{$mode}.tpl");
?>
