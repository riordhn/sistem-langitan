<?php
include('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_SUPERADMIN){
	header("location: /logout.php");
    exit();
}

$location = explode("-", $_GET['location']);

foreach ($user->MODULs as $modul)
{
    if ($modul['NM_MODUL'] === $location[0])
    {
        $smarty->assign('modul', $modul);
        
        foreach ($modul['MENUs'] as $menu)
        {
			if (isset($location[1]))
			{
				if ($menu['NM_MENU'] === $location[1])
				{
					$smarty->assign('menu', $menu);
				}
			}
			else
			{
				$smarty->assign('menu', NULL);
			}
        }
        
    }
}

$smarty->display('breadcrumbs.tpl');
?>