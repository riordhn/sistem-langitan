<?php
include 'config.php';
include 'class/Account.class.php';
include 'class/Fakultas.php';

$id_pt = $id_pt_user;

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_SUPERADMIN){
	header("location: /logout.php");
    exit();
}

$mode = get('mode', 'list');

$Fakultas = new Fakultas($db,$id_pt);
$Account = new Account($db,$id_pt);

if ($request_method == 'GET')
{
    if ($mode == 'list')
    {
        $smarty->assign('fakultas_set', $Fakultas->GetList());
        $smarty->assign('dosen_set', $Account->GetListDosen(get('id_fakultas','')));
    }
}

$smarty->display("account/dosen/{$mode}.tpl");
?>