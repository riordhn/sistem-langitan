<?php
include 'config.php';
include 'class/Account.class.php';

$id_pt = $id_pt_user;

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_SUPERADMIN){
	header("location: /logout.php");
    exit();
}

$mode = get('mode', 'list');
$Account = new Account($db,$id_pt);

if ($request_method == 'GET')
{
    if ($mode == 'list')
    {
        $smarty->assign('pengguna_set', $Account->GetListUsernameProblem());
    }
}

$smarty->display("account/username/{$mode}.tpl");
?>
