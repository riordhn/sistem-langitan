<?php
include 'config.php';
$mode = get('mode', 'view');
$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
	$db->Query("select pengguna_seq.nextval from dual");
	$row = $db->FetchAssoc();
	$id_pengguna = $row['NEXTVAL'];

	$id_role = post('id_jenis_pengguna') == '1' ? '22' : '4';

	$success = true;
	$db->BeginTransaction();

	$db->Parse("
		insert into pengguna (id_pengguna, join_table, username, se1, nm_pengguna, tgl_lahir_pengguna, gelar_depan, gelar_belakang, email_pengguna, id_role, password_hash, password_encrypted, id_perguruan_tinggi)
		values (:id_pengguna,:join_table,:username,:se1,:nm_pengguna,to_date(:tgl_lahir_pengguna,'DD/MM/YYYY'),:gelar_depan,:gelar_belakang,:email_pengguna,:id_role,:password_hash,:password_encrypted,:id_perguruan_tinggi)");
	$db->BindByName(':id_pengguna', $id_pengguna);
	$db->BindByName(':join_table', post('id_jenis_pengguna'));
	$db->BindByName(':username', post('username'));
	$db->BindByName(':se1', $user->Encrypt(post('password')));
	$db->BindByName(':nm_pengguna', post('nm_pengguna'));
	$db->BindByName(':tgl_lahir_pengguna', post('tgl_lahir_pengguna'));
	$db->BindByName(':gelar_depan', post('gelar_depan'));
	$db->BindByName(':gelar_belakang', post('gelar_belakang'));
	$db->BindByName(':email_pengguna', post('email_pengguna'));
	$db->BindByName(':id_role', $id_role);
	$db->BindByName(':password_hash', sha1(post('password')));
	$db->BindByName(':password_encrypted', $user->PublicKeyEncrypt(post('password')));
	$db->BindByName(':id_perguruan_tinggi', $id_pt);

	if ($db->Execute() == false)
	{
		$success = false;
		$message = "Gagal Insert pengguna";
		$db->Rollback();
	}

	// Multi Role insert
	if ($success)
	{
		if ($db->Query("insert into role_pengguna (id_pengguna, id_role) values ({$id_pengguna}, {$id_role})") == false)
		{
			$success = false;
			$message = "Gagal insert role: " . $db->Error;
			$db->Rollback();
		}
	}

	if ($success)
	{
		// Pegawai
		if (post('id_jenis_pengguna') == '1')
		{
			$db->Parse("
				insert into pegawai (id_pengguna, nip_pegawai, id_golongan, id_jabatan_pegawai, id_unit_kerja, alamat_pegawai, telp_pegawai)
				values (:id_pengguna,:nip_pegawai,:id_golongan,:id_jabatan_pegawai,:id_unit_kerja,:alamat_pegawai,:telp_pegawai)");
			$db->BindByName(':id_pengguna', $id_pengguna);
			$db->BindByName(':nip_pegawai', post('username'));
			$db->BindByName(':id_golongan', post('id_golongan_pegawai'));
			$db->BindByName(':id_jabatan_pegawai', post('id_jabatan_pegawai'));
			$db->BindByName(':id_unit_kerja', post('id_unit_kerja'));
			$db->BindByName(':alamat_pegawai', post('alamat_pegawai'));
			$db->BindByName(':telp_pegawai', post('telp_pegawai'));
			if ($db->Execute() == false)
			{
				$success = false;
				$message = "Gagal insert pegawai";
				$db->Rollback();
			}
		}

		// Dosen
		if (post('id_jenis_pengguna') == '2')
		{
			// Data dosen
			$db->Parse("
				insert into dosen (id_pengguna, nidn_dosen, id_program_studi, id_golongan, alamat_rumah_dosen, tlp_dosen, mobile_dosen)
				values (:id_pengguna,:nidn_dosen,:id_program_studi,:id_golongan,:alamat_rumah_dosen,:tlp_dosen,:mobile_dosen)");
			$db->BindByName(':id_pengguna', $id_pengguna);
			$db->BindByName(':nidn_dosen', post('nidn_dosen'));
			$db->BindByName(':id_program_studi', post('id_program_studi'));
			$db->BindByName(':id_golongan', post('id_golongan_dosen'));
			$db->BindByName(':alamat_rumah_dosen', post('alamat_rumah_dosen'));
			$db->BindByName(':tlp_dosen', post('tlp_dosen'));
			$db->BindByName(':mobile_dosen', post('mobile_dosen'));
			if ($db->Execute() == false)
			{
				$success = false;
				$message = "Gagal insert dosen";
				$db->Rollback();
			}
		}
	}

	if ($success)
	{
		$success = $db->Commit();
	}

	$smarty->assign('success', $success ? "1" : "0");
	$smarty->assign('message', $message);
	$mode = 'result';
}

if ($request_method == 'GET')
{
	$smarty->assign('program_studi_set', $db->QueryToArray(
		"SELECT ps.id_program_studi, j.nm_jenjang || ' ' || ps.nm_program_studi as nm_program_studi 
		from program_studi ps
		join jenjang j on j.id_jenjang = ps.id_jenjang
		join fakultas f on f.id_fakultas = ps.id_fakultas
		where f.id_perguruan_tinggi = '{$id_pt}''
		order by j.id_jenjang, nm_program_studi"));

	$smarty->assign('golongan_set', $db->QueryToArray("SELECT * from golongan order by nm_golongan"));

	$smarty->assign('jabatan_pegawai_set', $db->QueryToArray("SELECT * from jabatan_pegawai order by nm_jabatan_pegawai"));

	$smarty->assign('unit_kerja_set', $db->QueryToArray("SELECT * from unit_kerja WHERE id_perguruan_tinggi = {$id_pt} order by nm_unit_kerja"));
}

$smarty->display("account/add/{$mode}.tpl");
?>