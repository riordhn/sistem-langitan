<?php
include('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_SUPERADMIN){
	header("location: /logout.php");
    exit();
}

	// force password to ISO27001:2009
	if($_SESSION['standar_iso']=='no'){
		header("Location: /login.php?mode=ltp-null");
		exit();
	}

// data menu dari $user
$smarty->assign('modul_set', $user->MODULs);

$smarty->display("index.tpl");
?>