<?php
include('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_SUPERADMIN){
	header("location: /logout.php");
    exit();
}

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'add-modul')
    {
        $id_role = post('id_role');
        
        $max_urutan = $db->QuerySingle("select max(urutan) from modul where id_role = {$id_role}") + 1;
        $db->Parse("insert into modul (id_role, nm_modul, title, page, urutan, akses) values (:id_role,:nm_modul,:title,:page,:urutan,:akses)");
        $db->BindByName(':id_role', $id_role);
        $db->BindByName(':nm_modul', post('nm_modul'));
        $db->BindByName(':title', post('title'));
        $db->BindByName(':page', post('page'));
        $db->BindByName(':urutan', $max_urutan);
        $db->BindByName(':akses', post('akses'));
        $result = $db->Execute();
        
        $smarty->assign('added', $result ? "Data berhasil ditambahkan" : "Data gagal ditambahkan");
    }
    
	if (post('mode') == 'move-up-modul')
	{
		$id_modul = post('id_modul');
		
		$modul_set = $db->QueryToArray("select id_modul, id_role, urutan from modul where id_modul = {$id_modul}");
        $modul = $modul_set[0];
        
        $result = $db->Query("update modul set urutan = urutan + 1 where id_role = {$modul['ID_ROLE']} and urutan = ({$modul['URUTAN']} - 1)");
        $result = $db->Query("update modul set urutan = urutan - 1 where id_modul = {$modul['ID_MODUL']}");
        
        if ($result)
            echo "1";
        else
            echo "0";
		
		exit();
	
    }
    
    if (post('mode') == 'move-down-modul')
	{
		$id_modul = post('id_modul');
		
		$modul_set = $db->QueryToArray("select id_modul, id_role, urutan from modul where id_modul = {$id_modul}");
        $modul = $modul_set[0];
        
        $result = $db->Query("update modul set urutan = urutan - 1 where id_role = {$modul['ID_ROLE']} and urutan = ({$modul['URUTAN']} + 1)");
        $result = $db->Query("update modul set urutan = urutan + 1 where id_modul = {$modul['ID_MODUL']}");
        
        if ($result)
            echo "1";
        else
            echo "0";
		
		exit();
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $smarty->assign('role_set', $db->QueryToArray("select r.* from aucc.role r order by id_role"));
    }
    
    if ($mode == 'add-menu')   
    {
        $id_role = get('id_role');
        $role = $db->QueryToArray("select id_role, nm_role from role where id_role = {$id_role}");
        
        $smarty->assign('role', $role[0]);
    }
    
    if ($mode == 'edit-menu')
    {
        $id_role = get('id_role');
        $role = $db->QueryToArray("select * from role where id_role = {$id_role}");
        $smarty->assign('role', $role[0]);
        
        $modul_set = $db->QueryToArray("select * from modul where id_role = {$id_role} order by urutan");
        $smarty->assign('modul_set', $modul_set);
    }
    
    if ($mode == 'edit-submenu')
    {
        $id_modul = get('id_modul');
        $modul = $db->QueryToArray("
            select r.nm_role, m.title, r.id_role, m.id_modul from modul m
            join role r on r.id_role = m.id_role
            where id_modul = {$id_modul}");
        $smarty->assign('modul', $modul[0]);
        
        $menu_set = $db->QueryToArray("select * from menu where id_modul = {$id_modul} order by urutan");
        $smarty->assign('menu_set', $menu_set);
    }
}

$smarty->display("cp/role/{$mode}.tpl");
?>
