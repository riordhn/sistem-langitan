<?php

include 'config.php';
include 'class/Mahasiswa.class.php';
$mahasiswa = new Mahasiswa($db);
if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $fakultas = get('fakultas');
        $q_fakultas = $fakultas == '' ? "" : "AND F.ID_FAKULTAS='{$fakultas}'";
        $query = "
            SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,J.ID_JENJANG,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.NM_FAKULTAS,COUNT(PMK.ID_PENGAMBILAN_MK) JUMLAH_BEDA,
            SUM(
                CASE
                WHEN ID_PENGAMBILAN_MK IN (SELECT ID_PENGAMBILAN_MK FROM AUCC.LOG_KONVERSI_NILAI WHERE ID_PENGAMBILAN_MK IS NOT NULL)
                THEN 1
                ELSE 0
                END
            ) JUMLAH_LOG_KONVERSI,
            SUM(
                CASE
                WHEN ID_PENGAMBILAN_MK IN (SELECT ID_PENGAMBILAN_MK FROM AUCC.LOG_UPDATE_NILAI WHERE ID_PENGAMBILAN_MK IS NOT NULL)
                THEN 1
                ELSE 0
                END
            ) JUMLAH_LOG_UPDATE
            FROM AUCC.PENGAMBILAN_MK PMK
            JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = PMK.ID_KELAS_MK
            JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
            JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
            JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=PMK.ID_SEMESTER
            JOIN AUCC.MAHASISWA M ON M.ID_MHS=PMK.ID_MHS  
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN AUCC.FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            LEFT JOIN AUCC.PERATURAN_NILAI PN ON PMK.NILAI_ANGKA BETWEEN PN.NILAI_MIN_PERATURAN_NILAI AND PN.NILAI_MAX_PERATURAN_NILAI
            LEFT JOIN AUCC.STANDAR_NILAI SN ON SN.ID_STANDAR_NILAI=PN.ID_STANDAR_NILAI
            WHERE SN.NM_STANDAR_NILAI !=PMK.NILAI_HURUF {$q_fakultas}
            GROUP BY M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,J.ID_JENJANG,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,F.NM_FAKULTAS
            ORDER BY J.ID_JENJANG,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,M.NIM_MHS,M.ID_MHS,P.NM_PENGGUNA
            ";
        $data_mhs_nilai_abnormal = $db->QueryToArray($query);
        foreach($data_mhs_nilai_abnormal as $d){
            
        }
        $smarty->assign('data_mhs_nilai_abnormal', $data_mhs_nilai_abnormal);
    } else if (get('mode') == 'mk-abnormal') {
        $id_mhs = get('mhs');
        $data_biodata = $mahasiswa->get_biodata_mahasiswa($id_mhs);
        if (file_exists("../../foto_mhs/{$data_biodata['NIM_MHS']}.JPG"))
            $smarty->assign('foto', $base_url."foto_mhs/{$data_biodata['NIM_MHS']}.JPG");
        else if (file_exists("../../foto_mhs/{$data_biodata['NIM_MHS']}.jpg"))
            $smarty->assign('foto', "/modulx/images/{$data_biodata['NIM_MHS']}.jpg");

        $query = "
            SELECT PMK.ID_MHS,MK.KD_MATA_KULIAH,S.NM_SEMESTER,S.TAHUN_AJARAN,MK.NM_MATA_KULIAH,PMK.NILAI_HURUF,PMK.NILAI_ANGKA,SN.NM_STANDAR_NILAI,S.ID_SEMESTER
            FROM AUCC.PENGAMBILAN_MK PMK
            JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = PMK.ID_KELAS_MK
            JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
            JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
            JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=PMK.ID_SEMESTER
            JOIN AUCC.MAHASISWA M ON M.ID_MHS=PMK.ID_MHS  
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN AUCC.FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            LEFT JOIN AUCC.PERATURAN_NILAI PN ON PMK.NILAI_ANGKA BETWEEN PN.NILAI_MIN_PERATURAN_NILAI AND PN.NILAI_MAX_PERATURAN_NILAI
            LEFT JOIN AUCC.STANDAR_NILAI SN ON SN.ID_STANDAR_NILAI=PN.ID_STANDAR_NILAI
            WHERE SN.NM_STANDAR_NILAI !=PMK.NILAI_HURUF AND M.ID_MHS='{$id_mhs}'
            ORDER BY MK.NM_MATA_KULIAH
            ";
        $data_mk_nilai_abnormal = $db->QueryToArray($query);
        $smarty->assign('data_mk_nilai_abnormal', $data_mk_nilai_abnormal);
        $smarty->assign('data_biodata', $data_biodata);
        $smarty->assign('data_mhs_status', $mahasiswa->load_mhs_status($id_mhs));
    }
}

$smarty->assign('data_fakultas', $db->QueryToArray("SELECT * FROM AUCC.FAKULTAS ORDER BY ID_FAKULTAS"));
$smarty->display("controlling/control-nilai-abnormal.tpl");
?>
