<?php
include 'config.php';

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_modul = post('id_modul');
        $nm_modul = post('nm_modul');
        $title = post('title');
        $page = post('page');
        $urutan = post('urutan');
        $akses = post('akses');
        
        $result = $db->Query("update modul set nm_modul = '{$nm_modul}', title = '{$title}', page = '{$page}',
        						urutan = '{$urutan}', akses = '{$akses}' where id_modul = '{$id_modul}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }
    if (post('mode') == 'add')
    {
        $id_role = post('id_role');
        $nm_modul = post('nm_modul');
        $title = post('title');
        $page = post('page');
        $urutan = post('urutan');
        $akses = post('akses');
        
        $result = $db->Query("insert into modul (id_role, nm_modul, title, page, urutan, akses) 
        						values ('{$id_role}','{$nm_modul}','{$title}','{$page}','{$urutan}','{$akses}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$id_role 	= (int)get('id_role', '');

		$role_set = $db->QueryToArray("SELECT R.* FROM AUCC.ROLE R ORDER BY R.NM_ROLE");
		$smarty->assignByRef('role_set', $role_set);

		if ($id_role != '')
		{
			// Mengambil row ROLE
			$db->Query("SELECT * FROM AUCC.ROLE WHERE ID_ROLE = {$id_role}");
			$role = $db->FetchAssoc();

			// Mengambil List Modul
			$modul_set = $db->QueryToArray(
				"SELECT M1.*,
					(SELECT COUNT(M2.ID_MENU) FROM MENU M2 WHERE M2.ID_MODUL = M1.ID_MODUL) AS JUMLAH_MENU
				FROM MODUL M1
				WHERE M1.ID_ROLE = {$id_role} ORDER BY M1.URUTAN");


			// Mengecek eksistensi file
			foreach ($modul_set as &$modul)
			{
				if (file_exists($html_root . $role['PATH'] . $modul['PAGE']))
					$modul['FILE_EXISTS'] = 1;
				else
					$modul['FILE_EXISTS'] = 0;
			}

			$smarty->assignByRef('modul_set', $modul_set);
		}
	}
	else if ($mode == 'edit' )
	{
		$id_role 	= (int)get('id_role', '');
		$id_modul 	= (int)get('id_modul', '');

		$modul = $db->QueryToArray("
            select id_modul, nm_modul, title, page, urutan, akses 
            from modul 
            where id_modul = {$id_modul}");
        $smarty->assign('modul', $modul[0]);
	}
	else if($mode == 'add')
	{
		$id_role 	= (int)get('id_role', '');

		$role_set = $db->QueryToArray("SELECT R.* FROM AUCC.ROLE R ORDER BY R.NM_ROLE");
		$smarty->assignByRef('role_set', $role_set);

		$role = $db->QueryToArray("SELECT R.* FROM AUCC.ROLE R WHERE ID_ROLE = {$id_role}");
		$smarty->assign('role', $role[0]);
	}
}

$smarty->display("role/modul/{$mode}.tpl");