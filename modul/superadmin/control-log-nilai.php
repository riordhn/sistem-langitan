<?php

include 'config.php';
include 'class/Mahasiswa.class.php';
$mahasiswa = new Mahasiswa($db);
if (isset($_GET)) {
    if (get('mode') == 'cari') {
        $cari = get('cari');
        $query = "
            SELECT M.*,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS,SP.NM_STATUS_PENGGUNA
            FROM PENGGUNA P
            JOIN MAHASISWA M ON M.ID_PENGGUNA=P.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA=M.STATUS_AKADEMIK_MHS
            WHERE UPPER(P.NM_PENGGUNA) LIKE UPPER('%{$cari}%') OR M.NIM_MHS LIKE '%{$cari}%'
            ORDER BY PS.ID_FAKULTAS,PS.NM_PROGRAM_STUDI,M.NIM_MHS
            ";
        $data_cari = $db->QueryToArray($query);
        $smarty->assign('data_cari', $data_cari);
    } else if (get('mode') == 'detail') {
        $id = get('mhs');
        
        $data_biodata = $mahasiswa->get_biodata_mahasiswa($id);
        if (file_exists("../../foto_mhs/{$data_biodata['NIM_MHS']}.JPG"))
            $smarty->assign('foto', $base_url."foto_mhs/{$data_biodata['NIM_MHS']}.JPG");
        else if (file_exists("../../foto_mhs/{$data_biodata['NIM_MHS']}.jpg"))
            $smarty->assign('foto', "/modulx/images/{$data_biodata['NIM_MHS']}.jpg");

        $smarty->assign('data_biodata', $data_biodata);
        $smarty->assign('data_mhs_status', $mahasiswa->load_mhs_status($id));
        $smarty->assign('data_history_nilai', $mahasiswa->load_transkrip($id));
    }else if (get('mode') == 'log-nilai') {
        $mhs = get('mhs');
        $mk = get('mk');
        $semester = get('sem');
        
        $query_log_nilai_dosen ="
        SELECT MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,S.NM_SEMESTER,S.TAHUN_AJARAN,KOMK.NM_KOMPONEN_MK,KOMK.PERSENTASE_KOMPONEN_MK PERSEN,NML.NILAI_ASAL,
          NML.NILAI_BARU,PA.NM_PENGGUNA DOSEN_LAMA,DA.NIP_DOSEN NIP_LAMA,PB.NM_PENGGUNA DOSEN_BARU,DB.NIP_DOSEN NIP_BARU,TO_CHAR(NML.WAKTU, 'yyyy/mm/dd HH24:MI:SS') WAKTU
        FROM AUCC.NILAI_MK_LOG NML 
        JOIN AUCC.NILAI_MK NMK ON NML.ID_NILAI_MK=NMK.ID_NILAI_MK
        JOIN AUCC.KOMPONEN_MK KOMK ON KOMK.ID_KOMPONEN_MK = NMK.ID_KOMPONEN_MK
        JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK= KOMK.ID_KELAS_MK
        JOIN AUCC.SEMESTER S ON S.ID_SEMESTER = KMK.ID_SEMESTER
        JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
        JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH=KUMK.ID_MATA_KULIAH
        LEFT JOIN AUCC.DOSEN DA ON DA.ID_DOSEN = NML.ID_DOSEN_LAMA
        LEFT JOIN AUCC.PENGGUNA PA ON PA.ID_PENGGUNA=DA.ID_PENGGUNA
        LEFT JOIN AUCC.DOSEN DB ON DB.ID_DOSEN = NML.ID_DOSEN_BARU
        LEFT JOIN AUCC.PENGGUNA PB ON PB.ID_PENGGUNA=DB.ID_PENGGUNA
        WHERE NMK.ID_MHS='{$mhs}' AND MK.KD_MATA_KULIAH='{$mk}' AND S.ID_SEMESTER='{$semester}'";
        $data_log_nilai_dosen = $db->QueryToArray($query_log_nilai_dosen);
        
        $query_log_granted_ubah_nilai ="
        SELECT MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,S.NM_SEMESTER,S.TAHUN_AJARAN,SP.NM_SEMESTER NM_SEMESTER_UBAH,SP.TAHUN_AJARAN TAHUN_AJARAN_UBAH
        ,TO_CHAR(LGN.TGL_GRANTED, 'yyyy/mm/dd HH24:MI:SS') WAKTU_PERUBAHAN,LGN.IP_ADDRESS,LGN.ALASAN,PU.NM_PENGGUNA NM_PENGUBAH
        FROM AUCC.LOG_GRANTED_NILAI LGN
        JOIN AUCC.PENGAMBILAN_MK PMK ON PMK.ID_PENGAMBILAN_MK=LGN.ID_PENGAMBILAN_MK
        JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK= PMK.ID_KELAS_MK
        JOIN AUCC.SEMESTER S ON S.ID_SEMESTER = KMK.ID_SEMESTER
        JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
        JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH=KUMK.ID_MATA_KULIAH
        JOIN AUCC.PENGGUNA PU ON PU.ID_PENGGUNA=LGN.ID_PENGGUNA
        JOIN AUCC.SEMESTER SP ON SP.ID_SEMESTER=LGN.ID_SEMESTER
        WHERE LGN.ID_MHS='{$mhs}' AND MK.KD_MATA_KULIAH='{$mk}' AND S.ID_SEMESTER='{$semester}'";
        $data_log_granted_ubah_nilai = $db->QueryToArray($query_log_granted_ubah_nilai);
        
        $query_log_ubah_nilai ="
        SELECT MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,S.NM_SEMESTER,S.TAHUN_AJARAN,LUN.NILAI_HURUF_LAMA,LUN.NILAI_HURUF_BARU
        ,TO_CHAR(LUN.TGL_UPDATE, 'yyyy/mm/dd HH24:MI:SS') WAKTU_PERUBAHAN,LUN.IP_ADDRESS,LUN.REFERENSI,PU.NM_PENGGUNA NM_PENGUBAH,PMK.KETERANGAN
        FROM AUCC.LOG_UPDATE_NILAI LUN
        JOIN AUCC.PENGAMBILAN_MK PMK ON PMK.ID_PENGAMBILAN_MK=LUN.ID_PENGAMBILAN_MK
        JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK= PMK.ID_KELAS_MK
        JOIN AUCC.SEMESTER S ON S.ID_SEMESTER = KMK.ID_SEMESTER
        JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
        JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH=KUMK.ID_MATA_KULIAH
        JOIN AUCC.PENGGUNA PU ON PU.ID_PENGGUNA=LUN.ID_PENGGUNA
        WHERE PMK.ID_MHS='{$mhs}' AND MK.KD_MATA_KULIAH='{$mk}' AND S.ID_SEMESTER='{$semester}'
        ORDER BY WAKTU_PERUBAHAN";
        $data_log_ubah_nilai = $db->QueryToArray($query_log_ubah_nilai);
        
        $query_log_konversi_nilai ="
        SELECT MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,S.NM_SEMESTER,S.TAHUN_AJARAN,K1.NM_KURIKULUM KURIKULUM_LAMA,K1.THN_KURIKULUM THN_LAMA,K2.NM_KURIKULUM KURIKULUM_BARU,K2.THN_KURIKULUM THN_BARU
        ,TO_CHAR(LKN.TANGGAL, 'yyyy/mm/dd HH24:MI:SS') WAKTU_PERUBAHAN,LKN.IP_ADDRESS,LKN.NILAI_HURUF_LAMA,LKN.NILAI_HURUF_BARU,PU.NM_PENGGUNA NM_PENGUBAH,PMK.KETERANGAN
        FROM AUCC.LOG_KONVERSI_NILAI LKN
        JOIN AUCC.PENGAMBILAN_MK PMK ON PMK.ID_PENGAMBILAN_MK=LKN.ID_PENGAMBILAN_MK
        JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK= PMK.ID_KELAS_MK
        JOIN AUCC.SEMESTER S ON S.ID_SEMESTER = KMK.ID_SEMESTER
        JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
        JOIN AUCC.KURIKULUM_MK KMK1 ON KMK1.ID_KURIKULUM_MK=LKN.ID_KURIKULUM_MK_LAMA
        JOIN AUCC.KURIKULUM_PRODI KP1 ON KP1.ID_KURIKULUM_PRODI=KMK1.ID_KURIKULUM_PRODI
        JOIN AUCC.KURIKULUM K1 ON K1.ID_KURIKULUM=KP1.ID_KURIKULUM
        JOIN AUCC.KURIKULUM_MK KMK2 ON KMK2.ID_KURIKULUM_MK=LKN.ID_KURIKULUM_MK_BARU
        JOIN AUCC.KURIKULUM_PRODI KP2 ON KP2.ID_KURIKULUM_PRODI=KMK2.ID_KURIKULUM_PRODI
        JOIN AUCC.KURIKULUM K2 ON K2.ID_KURIKULUM=KP2.ID_KURIKULUM
        JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH=KUMK.ID_MATA_KULIAH
        JOIN AUCC.PENGGUNA PU ON PU.ID_PENGGUNA=LKN.ID_PENGGUNA
        WHERE PMK.ID_MHS='{$mhs}' AND MK.KD_MATA_KULIAH='{$mk}' AND S.ID_SEMESTER='{$semester}'
        ORDER BY WAKTU_PERUBAHAN";
        $data_log_konversi_nilai = $db->QueryToArray($query_log_konversi_nilai);
        
        
        $query_log_granted_transfer_nilai ="
        SELECT S.NM_SEMESTER,S.TAHUN_AJARAN,TO_CHAR(LGT.TANGGAL, 'yyyy/mm/dd HH24:MI:SS') WAKTU_PERUBAHAN,LGT.*,PU.NM_PENGGUNA NM_PENGUBAH
        FROM AUCC.LOG_GRANTED_TRANSFER LGT
        JOIN AUCC.SEMESTER S ON S.ID_SEMESTER = LGT.SEMESTER_PROSES
        JOIN AUCC.PENGGUNA PU ON PU.ID_PENGGUNA=LGT.ID_PENGGUNA
        WHERE LGT.ID_MHS='{$mhs}' AND S.ID_SEMESTER='{$semester}'";
        $data_log_granted_transfer_nilai = $db->QueryToArray($query_log_granted_transfer_nilai);
        
        $query_log_transfer_nilai ="
        SELECT MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,S.NM_SEMESTER,S.TAHUN_AJARAN,LTN.NILAI
        ,TO_CHAR(LTN.TANGGAL_INSERT, 'yyyy/mm/dd HH24:MI:SS') WAKTU_PERUBAHAN,LTN.IP_ADDRESS,PU.NM_PENGGUNA NM_PENGUBAH,PMK.KETERANGAN
        FROM AUCC.LOG_TRANSFER_NILAI LTN
        JOIN AUCC.PENGAMBILAN_MK PMK ON PMK.ID_PENGAMBILAN_MK=LTN.ID_PENGAMBILAN_MK
        JOIN AUCC.SEMESTER S ON S.ID_SEMESTER = LTN.ID_SEMESTER
        JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = LTN.ID_KURIKULUM_MK
        JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH=KUMK.ID_MATA_KULIAH
        JOIN AUCC.PENGGUNA PU ON PU.ID_PENGGUNA=LTN.ID_PENGGUNA
        WHERE PMK.ID_MHS='{$mhs}' AND MK.KD_MATA_KULIAH='{$mk}' AND S.ID_SEMESTER='{$semester}'
        ORDER BY WAKTU_PERUBAHAN";
        $data_log_transfer_nilai = $db->QueryToArray($query_log_transfer_nilai);
        
        $smarty->assign('data_log_nilai_dosen', $data_log_nilai_dosen);
        $smarty->assign('data_log_granted_ubah_nilai', $data_log_granted_ubah_nilai);
        $smarty->assign('data_log_ubah_nilai', $data_log_ubah_nilai);
        $smarty->assign('data_log_konversi_nilai', $data_log_konversi_nilai);
        $smarty->assign('data_log_granted_transfer_nilai', $data_log_granted_transfer_nilai);
        $smarty->assign('data_log_transfer_nilai', $data_log_transfer_nilai);
        
    }
}

$smarty->display("controlling/control-log-nilai.tpl");
?>
