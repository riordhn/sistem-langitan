<?php
include 'config.php';
include 'class/Account.class.php';

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_SUPERADMIN){
	header("location: /logout.php");
    exit();
}

$mode = get('mode', 'view');

if ($request_method == 'GET')
{
    if ($mode == 'view')
    {
        
    }
}

$smarty->display("account/list/{$mode}.tpl");
?>
