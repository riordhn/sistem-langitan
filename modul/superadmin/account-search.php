<?php

include('config.php');
include('sendsms.php');
// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_SUPERADMIN){
	header("location: /logout.php");
    exit();
}

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {

        $id_pengguna = post('id_pengguna');
        $id_role = post('id_role');
        $id_join_table = post('id_join_table');
        $id_program_studi = post('id_program_studi');
        $id_unit_kerja = post('id_unit_kerja');

        
        //pegawai
        if($id_join_table == 1){
            if($id_unit_kerja== ''){
                $result = $db->Query("update pengguna set id_role = {$id_role} where id_pengguna = {$id_pengguna}");
            }
            else{
                $result = $db->Query("update pengguna set id_role = {$id_role} where id_pengguna = {$id_pengguna}");
                $result = $db->Query("update pegawai set id_unit_kerja = {$id_unit_kerja} where id_pengguna = {$id_pengguna}"); 
            }   
        }
        //dosen
        else if($id_join_table == 2){
            if($id_program_studi== ''){
                $result = $db->Query("update pengguna set id_role = {$id_role} where id_pengguna = {$id_pengguna}");
            }
            else{
                $result = $db->Query("update pengguna set id_role = {$id_role} where id_pengguna = {$id_pengguna}");
                $result = $db->Query("update dosen set id_program_studi = {$id_program_studi} where id_pengguna = {$id_pengguna}");
            }
        }
        else
        {
            $result = $db->Query("update pengguna set id_role = {$id_role} where id_pengguna = {$id_pengguna}");
        }

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }

    if (post('mode') == 'ubahPassword')
    {
        $id_pengguna = post('id_pengguna');
        $username = post('username');

        #reset via manual ~ Putra Rieskha
        if(post('via_reset') == 'manual'){
            $password_hash = sha1($username);
            $result = $db->Query("update pengguna set password_hash = '{$password_hash}', password_must_change = '1', password_hash_temp = null where id_pengguna = {$id_pengguna}");
            $smarty->assign('edited', $result ? "Password berhasil direset" : "Password gagal direset");
        }

        #reset via sms ~ Putra Rieskha
        if(post('via_reset') == 'sms'){
            $result = sendSms($id_pengguna);
            if($result['result'] == 1) $msg = "Password berhasil direset ~ ".$result['msg'];
            else $msg = "Password gagal direset ~ ".$result['msg'];
            $smarty->assign('edited', $msg);
        }

    }
        
    
    if (post('mode') == 'add-role')
    {
        $id_pengguna = post('id_pengguna');
        $id_roles = post('id_roles');
        
        foreach ($id_roles as $id_role)
        {
            $db->Query("insert into role_pengguna (id_pengguna, id_role) values ({$id_pengguna}, $id_role)");
        }
    }
    
    if (post('mode') == 'delete-role')
    {
        $id_role_pengguna = post('id_role_pengguna');
        
        $result = $db->Query("delete from role_pengguna where id_role_pengguna = {$id_role_pengguna}");
        
        echo $result ? "1" : "0";
        
        exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $q = strtoupper(get('q', ''));
        
        if (strlen(trim($q)) >= 3)
        {
            $pengguna_set = $db->QueryToArray("
                select p.id_pengguna, p.username, upper(p.nm_pengguna) as nm_pengguna, p.join_table, r.nm_role, count(rp.id_role) jumlah_role
                from pengguna p
                join role r on r.id_role = p.id_role
                left join role_pengguna rp on rp.id_pengguna = p.id_pengguna
                where p.id_role not in (6) and (upper(p.nm_pengguna) like '%{$q}%' or upper(p.username) like '{$q}%') and p.id_perguruan_tinggi = {$id_pt}
                group by p.id_pengguna, p.username, p.nm_pengguna, p.join_table, r.nm_role
                order by p.nm_pengguna");
            $smarty->assign('pengguna_set', $pengguna_set);
        }
    }
    
    if ($mode == 'edit')
    {
        $id_pengguna = get('id_pengguna');
        
        $pengguna = $db->QueryToArray("
            select p.id_pengguna, p.username, p.nm_pengguna, pw.id_unit_kerja, pw.id_unit_kerja_sd, uk.nm_unit_kerja, email_pengguna, p.join_table, p.id_role, se1, sp.id_session,
				d.nip_dosen, d.id_program_studi, d.id_program_studi_sd, ps.nm_program_studi prodi_dosen, f.nm_fakultas fakultas_dosen
			from pengguna p
            left join pegawai pw on pw.id_pengguna = p.id_pengguna
            left join unit_kerja uk on uk.id_unit_kerja = pw.id_unit_kerja
            left join session_pengguna sp on sp.id_pengguna = p.id_pengguna
			left join dosen d on d.id_pengguna = p.id_pengguna
			left join program_studi ps on ps.id_program_studi = d.id_program_studi
			left join fakultas f on f.id_fakultas = ps.id_fakultas
            where p.id_pengguna = {$id_pengguna}");
        $smarty->assign('pengguna', $pengguna[0]);
        
        $unit_kerja_set = $db->QueryToArray("
            select uk.id_unit_kerja, uk.nm_unit_kerja, ps.nm_program_studi, j.nm_jenjang 
            from unit_kerja uk
            left join program_studi ps on ps.id_program_studi = uk.id_program_studi
            left join jenjang j on j.id_jenjang = ps.id_jenjang
            where id_perguruan_tinggi = {$id_pt}");
        $smarty->assign('unit_kerja_set', $unit_kerja_set);

        $unit_kerja_sd = $db->QueryToArray("
            select uk.id_unit_kerja, uk.nm_unit_kerja 
            from unit_kerja uk, pegawai p, pengguna pn
            where p.id_pengguna = pn.id_pengguna and p.id_pengguna = {$id_pengguna} and p.id_unit_kerja_sd = uk.id_unit_kerja");
        $smarty->assign('unit_kerja_sd', $unit_kerja_sd[0]);

        $program_studi_set = $db->QueryToArray("
            select ps.id_program_studi, ps.nm_program_studi, j.nm_jenjang 
            from program_studi ps, jenjang j
            where ps.id_jenjang = j.id_jenjang and ps.id_fakultas IN (select id_fakultas from fakultas where id_perguruan_tinggi = {$id_pt})");
        $smarty->assign('program_studi_set', $program_studi_set);

        $program_studi_sd = $db->QueryToArray("
            select ps.id_program_studi, ps.nm_program_studi 
            from program_studi ps, dosen d, pengguna pn
            where d.id_pengguna = pn.id_pengguna and d.id_pengguna = {$id_pengguna} and d.id_program_studi_sd = ps.id_program_studi");
        $smarty->assign('program_studi_sd', isset($program_studi_sd[0]) ? $program_studi_sd[0] : 0);

        $role_pengguna_set = $db->QueryToArray("
            select rp.id_role_pengguna, rp.id_role, tr.nm_template, r.nm_role from role_pengguna rp
            join role r on r.id_role = rp.id_role
            left join template_role tr on tr.id_template_role = rp.id_template_role
            where rp.id_pengguna = {$id_pengguna}
			order by r.nm_role");
        $smarty->assign('role_pengguna_set', $role_pengguna_set);
    }
    
    if ($mode == 'add-role')
    {
        $id_pengguna = get('id_pengguna');
        
        $pengguna = $db->QueryToArray("select p.id_pengguna, p.nm_pengguna, p.join_table from pengguna p where p.id_pengguna = {$id_pengguna}");
        $smarty->assign('pengguna', $pengguna[0]);

        //$role_disable = $db->QueryToArray("select * from aucc.role 
                                            //where id_role in (17,35,41,37,9,24,26,19,34,25,39,16,29,44,32,21,30,36,31,47,43) order by id_role");
        //$smarty->assign('role_disable', $x = array(35,41,37,9,24,26,19,34,25,39,16,29,44,32,21,30,36,31,47,43));
        // membuka role LPPM
        $smarty->assign('role_disable', $x = array(35,41,37,9,24,26,19,34,16,29,44,32,21,30,36,31,47,43));

        if($pengguna[0]['JOIN_TABLE'] == '3'){
            $role_set = $db->QueryToArray("select * from aucc.role where id_role not in (select id_role from aucc.role_pengguna where id_pengguna = {$id_pengguna}) and id_role in (3,28) order by nm_role");
            $smarty->assign('role_set', $role_set);
        }
        else{
            $role_set = $db->QueryToArray("select * from aucc.role where id_role not in (select id_role from aucc.role_pengguna where id_pengguna = {$id_pengguna}) and id_role not in (3,6,28) order by nm_role");
            $smarty->assign('role_set', $role_set);
        }
    }
}

$smarty->display("account/search/{$mode}.tpl");