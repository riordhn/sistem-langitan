<?php
include 'config.php';

$mode = get('mode', 'view');

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	if ($mode == 'view')
	{
		$id_role 	= (int)get('id_role', '');

		$role_set = $db->QueryToArray("SELECT R.* FROM AUCC.ROLE R ORDER BY R.NM_ROLE");
		$smarty->assignByRef('role_set', $role_set);

		if ($id_role != '')
		{
			// Mengambil row ROLE
			$db->Query("SELECT * FROM AUCC.ROLE WHERE ID_ROLE = {$id_role}");
			$role = $db->FetchAssoc();

			// Mengambil list Template
			$template_set = $db->QueryToArray(
				"SELECT TR.*,
					(SELECT COUNT(ID_ROLE_PENGGUNA) FROM ROLE_PENGGUNA RP WHERE RP.ID_TEMPLATE_ROLE = TR.ID_TEMPLATE_ROLE) AS JUMLAH_USER
				FROM TEMPLATE_ROLE TR
				WHERE ID_ROLE = {$id_role} ORDER BY NM_TEMPLATE");
			$smarty->assignByRef('template_set', $template_set);
		}
	}
}

$smarty->display("role/template/{$mode}.tpl");