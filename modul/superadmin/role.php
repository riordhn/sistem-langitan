<?php
include 'config.php';

$mode = get('mode', 'view');

/*
if (function_exists('posix_getpwuid'))
{
	echo "ADA"; exit();
}
*/

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	if ($mode == 'view')
	{
		// Mengambil list Role
		$role_set = $db->QueryToArray(
			"SELECT R.*,
				(SELECT COUNT(ID_MODUL) FROM MODUL M WHERE M.ID_ROLE = R.ID_ROLE) AS JUMLAH_MODUL,
				(SELECT COUNT(ID_MENU) FROM MODUL M1 JOIN MENU M2 ON M2.ID_MODUL = M1.ID_MODUL WHERE M1.ID_ROLE = R.ID_ROLE) AS JUMLAH_MENU,
				(SELECT COUNT(ID_PENGGUNA) FROM PENGGUNA P WHERE P.ID_ROLE = R.ID_ROLE) AS JUMLAH_USER,
				(SELECT COUNT(ID_TEMPLATE_ROLE) FROM TEMPLATE_ROLE TR WHERE TR.ID_ROLE = R.ID_ROLE) AS JUMLAH_TEMPLATE
			FROM AUCC.ROLE R
			ORDER BY R.NM_ROLE");

		// Mendeteksi Permision path dan owner
		foreach ($role_set as &$role)
		{
			// Get Exist Folder
			if (file_exists($html_root . $role['PATH']))
			{
				$role['FILE_EXISTS'] = 1;

				// Get Permission
				$role['PERMISSION'] = substr(sprintf('%o', fileperms($html_root . $role['PATH'])), -4);

				// Get File Owner
				$uid = fileowner($html_root . $role['PATH']);
				exec("grep {$uid} /etc/passwd | cut -d: -f1", $o, $r);
				if ($r == 0)
					$role['FILE_OWNER'] = trim($o[0]);
				else
					$role['FILE_OWNER'] = $uid;

				// clear variabel $o
				unset($o);
			}
			else
			{
				$role['FILE_EXISTS'] = 0;
				$role['PERMISSION'] = '';
				$role['FILE_OWNER'] = '';
			}


		}


		$smarty->assignByRef('role_set', $role_set);
	}
}

$smarty->display("role/{$mode}.tpl");