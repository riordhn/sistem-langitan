<?php
/**
 * Description of Account Class
 *
 * @author Fathoni
 */
class Account
{
    private $db;
    private $id_pt;
    
    function __construct(MyOracle $database, $id_pt = null)
    {
        $this->db = $database;
        $this->id_pt = $id_pt;
    }
    
    function GetListPegawai($id_role = '')
    {
        //echo "abc ".$this->id_pt;
        if ($id_role != '')
        {
            $where_role = "and r.id_role = ".$id_role;
        
            if ($id_role == 'all')
            {
                $where_role = '';
            }

            return $this->db->QueryToArray("
                select p.id_pengguna, p.username, p.nm_pengguna, r.id_role, r.nm_role, uk.nm_unit_kerja
                from pengguna p
                join pegawai peg on peg.id_pengguna = p.id_pengguna
                left join role r on r.id_role = p.id_role
                left join unit_kerja uk on uk.id_unit_kerja = peg.id_unit_kerja
                where p.id_perguruan_tinggi = {$this->id_pt} and p.id_role not in (3, 4, 6, 28)
                {$where_role}
                order by nm_pengguna");
        }
        
        return null;
    }
    
    function GetListDosen($id_fakultas = '')
    {
        if ($id_fakultas != '')
        {
            $where_fakultas = "where p.id_perguruan_tinggi = {$this->id_pt} and ps.id_fakultas = {$id_fakultas}";
            
            if ($id_fakultas == 'all')
            {
                $where_fakultas = "where p.id_perguruan_tinggi = {$this->id_pt}";
            }
            
            return $this->db->QueryToArray("
                select p.id_pengguna, p.username, p.nm_pengguna, j.nm_jenjang||' '||ps.nm_program_studi as nm_program_studi, r.nm_role
                from pengguna p
                join dosen d on d.id_pengguna = p.id_pengguna
                join program_studi ps on ps.id_program_studi = d.id_program_studi
                join jenjang j on j.id_jenjang = ps.id_jenjang
                left join role r on r.id_role = p.id_role
                $where_fakultas");
        }
        
        return null;
    }
    
    function GetListNoRole()
    {
        return $this->db->QueryToArray("select p.id_pengguna, p.username, p.nm_pengguna from pengguna p where p.id_perguruan_tinggi = {$this->id_pt} and id_role is null");
    }
    
    function GetListDoubleJoin()
    {
        return $this->db->QueryToArray("
            select * from (
              select id_pengguna, username, nm_pengguna, nm_role,
                (select count(id_dosen) from dosen where dosen.id_pengguna = pengguna.id_pengguna) jumlah_dosen,
                (select count(id_pegawai) from pegawai where pegawai.id_pengguna = pengguna.id_pengguna) jumlah_pegawai
              from pengguna
              left join role r on r.id_role = pengguna.id_role
              where pengguna.id_perguruan_tinggi = {$this->id_pt})
            where jumlah_dosen > 1 or jumlah_pegawai > 1 or (jumlah_dosen = 1 and jumlah_pegawai = 1)");
    }
    
    function GetListNoJoin()
    {
        return $this->db->QueryToArray("
            select * from (
              select id_pengguna, username, nm_pengguna, nm_role,
                (select count(id_dosen) from dosen where dosen.id_pengguna = pengguna.id_pengguna) jumlah_dosen,
                (select count(id_pegawai) from pegawai where pegawai.id_pengguna = pengguna.id_pengguna) jumlah_pegawai,
                (select count(id_mhs) from mahasiswa where mahasiswa.id_pengguna = pengguna.id_pengguna) jumlah_mahasiswa
              from pengguna
              left join role r on r.id_role = pengguna.id_role
              where pengguna.id_perguruan_tinggi = {$this->id_pt} and r.id_role <> 6)
            where jumlah_dosen = 0 and jumlah_pegawai = 0 and jumlah_mahasiswa = 0");
    }
    
    function GetListNoName()
    {
        return $this->db->QueryToArray("
            select p.id_pengguna, p.username, p.nm_pengguna, r.nm_role
            from pengguna p
            left join role r on r.id_role = p.id_role
            where p.id_perguruan_tinggi = {$this->id_pt} and p.nm_pengguna is null and p.id_role <> 6");
    }
    
    function GetListUsernameProblem()
    {
        return $this->db->QueryToArray("
            select p.id_pengguna, p.username, p.nm_pengguna, r.nm_role
            from pengguna p
            left join role r on r.id_role = p.id_role
            where p.id_perguruan_tinggi = {$this->id_pt} and length(trim(translate(p.username, ' +-.0123456789', ' '))) > 2");
    }
}
?>
