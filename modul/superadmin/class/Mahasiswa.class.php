<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mahasiswa
 *
 * @author NAMBISEMBILU
 */
class Mahasiswa {

    //put your code here

    private $db;

    function __construct($db) {
        $this->db = $db;
    }

    function get_biodata_mahasiswa($id_mhs) {
        $this->db->Query("select NIM_MHS from MAHASISWA where id_mhs='" . $id_mhs . "'");
        $tmp_nim = $this->db->FetchAssoc();
        $mhs_nim = $tmp_nim['NIM_MHS'];

        $DBhost = "10.0.110.1";
        $DBuser = "lihat_blogmhs";
        $DBpass = "lihat_blogmhs";
        $DBName = "dbwebmhs";
        mysql_connect($DBhost, $DBuser, $DBpass) ;
        mysql_select_db("$DBName");

        $q = mysql_query("select subdomainMhs from tb_mahasiswa where nim='" . $mhs_nim . "'");
        while ($r = mysql_fetch_row($q)) {
            $mhs_blog = $r[0] . '.web.unair.ac.id';
        }

        $this->db->Query("
            SELECT P.NM_PENGGUNA,M.NIM_MHS,M.THN_ANGKATAN_MHS,J.NM_JENJANG,F.NM_FAKULTAS,PS.NM_PROGRAM_STUDI,
                NVL(K.NM_KOTA,P.TEMPAT_LAHIR) TEMPAT_LAHIR,M.MOBILE_MHS,P.KELAMIN_PENGGUNA,P.EMAIL_PENGGUNA, '" . $mhs_blog . "' as BLOG_PENGGUNA,
                M.ALAMAT_MHS,M.ALAMAT_AYAH_MHS,P.FOTO_PENGGUNA,P.TGL_LAHIR_PENGGUNA,M.PENGHASILAN_ORTU_MHS,M.NM_AYAH_MHS
            FROM PENGGUNA P 
            LEFT JOIN KOTA K ON K.ID_KOTA LIKE ''||P.TEMPAT_LAHIR||''
            LEFT JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN CALON_MAHASISWA_BARU CM ON CM.ID_C_MHS = M.ID_C_MHS       
            LEFT JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
            LEFT JOIN FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
            WHERE M.ID_MHS='{$id_mhs}'
            ");
        return $this->db->FetchAssoc();
    }

    function get_ips_mhs($id_mhs, $id_semester) {
        $this->db->Query("
            select id_mhs,case when sum(bobot*kredit_semester)=0 then 0 else 
            round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
            case when id_fakultas=7 then sum(sksreal) else sum(kredit_semester) end as sks_sem 
            from 
            (select id_mhs,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
            (select a.id_mhs,ps.id_fakultas,a.id_kurikulum_mk, 
            case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
            and d.status_mkta in (1,2) then 0
            else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
            case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
            case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
            from pengambilan_mk a
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
            left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
            left join mahasiswa m on a.id_mhs=m.id_mhs
            left join program_studi ps on m.id_program_studi=ps.id_program_studi
            left join semester s on a.id_semester=s.id_semester
            where group_semester||thn_akademik_semester in
            (select group_semester||thn_akademik_semester from semester where id_Semester='" . $id_semester . "')
            and tipe_semester in ('UP','REG','RD') 
            and a.status_apv_pengambilan_mk='1' and m.id_mhs='" . $id_mhs . "' and a.status_hapus=0 and a.flagnilai='1'
            and a.status_pengambilan_mk !=0
            )
            group by id_mhs, id_kurikulum_mk, id_fakultas, kredit_semester, sksreal
            )
            group by id_mhs,id_fakultas
            ");
        $data = $this->db->FetchAssoc();
        return $data['IPS'];
    }

    function load_mhs_status($id_mhs) {
        $ips = $this->get_ips_mhs($id_mhs, $this->id_semester_sebelum);
        $mhs_status = $this->get_mhs_status($id_mhs);
        $this->db->Query("
        SELECT P.NM_PENGGUNA,M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,PS.NM_PROGRAM_STUDI,'{$ips}' AS IPS,'{$mhs_status['IPK']}' AS IPK,'{$mhs_status['SKSTOTAL']}' AS TOTAL_SKS,J.NM_JENJANG FROM MAHASISWA M
            LEFT JOIN PENGGUNA P ON M.ID_PENGGUNA  = P.ID_PENGGUNA
            LEFT JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        WHERE M.ID_MHS ='{$id_mhs}'");
        return $this->db->FetchAssoc();
    }

    function get_mhs_status($id_mhs) {
        $this->db->Query("SELECT PS.ID_FAKULTAS FROM MAHASISWA M JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI WHERE M.ID_MHS='{$id_mhs}'");
        $id_fakultas_mhs = $this->db->FetchAssoc();
        // ngulang -> nilai terakhir yg berlaku
        if ($id_fakultas_mhs['ID_FAKULTAS'] == 10) {
            $sql = "select a.id_mhs,sum(a.kredit_semester) skstotal,
                    round(sum(a.kredit_semester*(case a.nilai_huruf 
                    when 'A' then 4 
                    when 'AB' then 3.5 
                    when 'B' then 3
                    when 'BC' then 2.5
                    when 'C' then 2
                    when 'D' then 1
                    end))/sum(a.kredit_semester),2) IPK
                    from
                    (
                    select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
                    select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester desc,sm.nm_semester desc) rangking
                    from pengambilan_mk a 
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    left join semester sm on sm.id_semester=a.id_semester
                    where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.flagnilai=1 and a.id_semester is not null
                    ) a
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    where rangking=1 and id_mhs='{$id_mhs}'
                    ) a
                    left join mahasiswa b on a.id_mhs=b.id_mhs
                    left join program_studi f on b.id_program_studi=f.id_program_studi
                    where a.id_mhs='{$id_mhs}'
                    group by a.id_mhs order by a.id_mhs";
            $this->db->Query($sql);
            $mhs_status = $this->db->FetchAssoc();
        } else {
            // ipk versi lukman, ngulang diambil nilai terbaik.
            $sql = "select a.id_mhs,sum(a.kredit_semester) skstotal,
                    round(sum(a.kredit_semester*(case a.nilai_huruf 
                    when 'A' then 4 
                    when 'AB' then 3.5 
                    when 'B' then 3
                    when 'BC' then 2.5
                    when 'C' then 2
                    when 'D' then 1
                    end))/sum(a.kredit_semester),2) IPK
                    from
                    (
                    select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
                    select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
                    from pengambilan_mk a 
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null 
                    ) a
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    where rangking=1 and id_mhs='{$id_mhs}'
                    ) a
                    left join mahasiswa b on a.id_mhs=b.id_mhs
                    left join program_studi f on b.id_program_studi=f.id_program_studi
                    where a.id_mhs='{$id_mhs}'
                    group by a.id_mhs order by a.id_mhs";
            $this->db->Query($sql);
            $mhs_status = $this->db->FetchAssoc();
        }
        return $mhs_status;
    }
    
    function load_transkrip($id_mhs) {
        return $this->db->QueryToArray("
        SELECT S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,MK.KD_MATA_KULIAH,MK.NM_MATA_KULIAH,KUMK.KREDIT_SEMESTER,PMK.NILAI_HURUF,PMK.FLAGNILAI
            ,COUNT(*) OVER(PARTITION BY MK.NM_MATA_KULIAH) STATUS_ULANG,
        ROW_NUMBER() OVER(PARTITION BY PMK.ID_MHS,MK.NM_MATA_KULIAH ORDER BY PMK.NILAI_HURUF) ULANG_KE 
        FROM PENGAMBILAN_MK PMK
        LEFT JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = PMK.ID_KURIKULUM_MK
        LEFT JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
        JOIN SEMESTER S ON S.ID_SEMESTER= PMK.ID_SEMESTER
        WHERE PMK.ID_MHS='{$id_mhs}' AND PMK.STATUS_APV_PENGAMBILAN_MK='1'
        ORDER BY S.TAHUN_AJARAN DESC,S.NM_SEMESTER DESC,MK.NM_MATA_KULIAH
        ");
    }

}
