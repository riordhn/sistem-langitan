<?php
//include '../../config.php';
//include('../../includes/PerguruanTinggi.class.php');
/**
 * Description of Fakultas
 *
 * @author Fathoni
 */
class Fakultas
{
    private $db;
    private $id_pt;
    
    function __construct(MyOracle $db, $id_pt)
    {
        $this->db = $db;
        $this->id_pt = $id_pt;
    }
    
    function GetList()
    {
        //echo $id_pt = getID();
        return $this->db->QueryToArray("
            select f.id_fakultas, nm_fakultas,
                (select count(ps.id_program_studi) from program_studi ps
                 where ps.id_fakultas = f.id_fakultas) as jumlah
            from fakultas f
            where f.id_perguruan_tinggi = {$this->id_pt}
            order by id_fakultas");
    }
}

?>
