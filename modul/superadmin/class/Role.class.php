<?php
/**
 * Description of Role
 *
 * @author Fathoni
 */
class Role
{
    private $db;
    private $id_pt;
    
    function __construct(MyOracle $database, $id_pt = null)
    {
        $this->db = $database;
        $this->id_pt = $id_pt;
    }
    
    function GetListRolePegawai()
    {
        //echo "role ".$this->id_pt;
        return $this->db->QueryToArray("
            select role.*,
                (select count(p.id_pengguna) from pengguna p
                 join pegawai peg on peg.id_pengguna = p.id_pengguna
                 where p.id_perguruan_tinggi = {$this->id_pt} and p.id_role = role.id_role) as jumlah
            from role
            where role.id_role not in (3, 4, 6, 28)
            order by nm_role");
    }
}
?>
