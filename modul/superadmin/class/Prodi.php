<?php

/**
 * Description of Prodi
 *
 * @author Fathoni
 */
class Prodi
{
    private $db;
    
    function __construct(MyOracle $db)
    {
        $this->db = $db;
    }
    
    function GetList($id_fakultas)
    {
        return $this->db->QueryToArray("
            select ps.id_program_studi, ps.nm_program_studi, j.nm_jenjang_sngkat,
            (select count(m.id_mhs) from mahasiswa m
                 where m.id_program_studi = ps.id_program_studi) as jumlah
            from program_studi ps, jenjang j
            where j.id_jenjang = ps.id_jenjang and ps.id_fakultas = {$id_fakultas}");
    }
}

?>
