<?php

include('../../config.php');

if ($user->Role() != AUCC_ROLE_SUPERADMIN) {
	header('Location: /logout.php');
	
} else {

	foreach ($user->MODULs as &$modul)
	{
		$nm_modul    = &$modul['NM_MODUL'];
		$akses_modul = &$modul['AKSES'];
		
		
		// modul role khusus umaha
		if ($user->ID_PERGURUAN_TINGGI == 1)
		{
			if ($nm_modul == 'role')   { $akses_modul = 1; }
		}
	}
	
}

// Settingan path html root
$html_root = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR;