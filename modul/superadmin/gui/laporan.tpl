<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Super Administrator - {$nama_pt}</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<link rel="shorcut icon" href="{$base_url}img/icon.ico" />
	</head>

	<body>
		
		<h1>MASUK HALAMAN LAPORAN</h1>
        {if $state eq 'detail'}
        		<table class="table table-striped table-bordered">
				<tr>
					<td>NO</td>
					<td>NAMA KELAS</td>
					<td>MATA KULIAH</td>
					<td>TINGKAT SEMESTER</td>
					<td>DOSEN PENGAMPU</td>
					<td>JUMLAH PESERTA</td>
					<td>JML NILAI MASUK</td>
					<td>JML NILAI BLM MASUK</td>
				</tr>
				{foreach $data_detail as $datas}
	             <tr>   
	                <td>{$datas@index + 1}</td>
	                <td>{$datas.NAMA_KELAS}</td>
	                <td>{$datas.NM_MATA_KULIAH}</td>
	                <td>{$datas.TINGKAT_SEMESTER}</td>
	                <td>{$datas.NM_PENGGUNA}</td>
	                <td>{$datas.JUMLAH_PESERTA}</td>
	                <td>{$datas.JUMLAH_NILAI_MASUK}</td>
	                <td>{$datas.JUMLAH_NILAI_BLM_MASUK}</td>
	                
                 </tr>
	        	{/foreach}
	        
			</table>
		{else}
			
			<table class="table table-striped table-bordered">
			<tr>
				<td>NO</td>
				<td>PROGRAM STUDI</td>
				<td>JUMLAH KELAS</td>
				<td>JUMLAH PESERTA</td>
				<td>JML NILAI MASUK</td>
				<td>JML NILAI BLM MASUK</td>
			</tr>
			{foreach $data as $datas}
                <tr>
                    <td class="center">{$datas@index + 1}</td>
                    <td><a href="?detail={$datas.ID_PROGRAM_STUDI}">{$datas.NM_PROGRAM_STUDI}</a></td>
                    <td>{$datas.JUMLAH_KELAS}</td>
                    <td>{$datas.JUMLAH_PESERTA}</td>
                    <td>{$datas.JML_NILAI_MASUK}</td>
                    <td>{$datas.JML_NILAI_BLM_MASUK}</td>
                </tr>
	        {/foreach}
	        </table>
        {/if}

	</body>
	
</html>