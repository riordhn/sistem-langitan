<div class="center_title_bar">Pencarian Account</div>

<form action="password-temp.php" method="get">
    <table>
        <tr>
            <td>Nama / Username / Nama Role : </td>
            <td><input type="search" name="q" value="{$smarty.get.q}" /></td>
        </tr>
    </table>
</form>

{if !empty($pengguna_set)}
<table>
    <tr>
        <th>No</th>
        <th>Username</th>
        <th>Nama</th>
        <th>Tipe Account</th>
        <th>Default Role</th>
        <th>Perguruan Tinggi</th>
        <th>Action</th>
        
    </tr>
    {foreach $pengguna_set as $p}
    <tr {if $p@index is not div by 2}class="row1"{/if}>
        <td class="center">{$p@index + 1}</td>
        <td>{$p.USERNAME}</td>
        <td>{$p.NM_PENGGUNA}</td>
        <td class="center">{if $p.JOIN_TABLE == 1}Pegawai{else if $p.JOIN_TABLE == 2}Dosen{else if $p.JOIN_TABLE == 3}Mahasiswa{/if}</td>
        <td class="center">{$p.NM_ROLE}</td>
        <td class="center">{$p.NAMA_SINGKAT}</td>
        <td class="center">
        {if $p.PASSWORD_HASH eq '356a192b7913b04c54574d18c28d46e6395428ab' }
            <a href="password-temp.php?mode=kembali&id_pengguna={$p.ID_PENGGUNA}&q={$smarty.get.q}">Recovery</a>
        {else}
            <a href="password-temp.php?mode=reset&id_pengguna={$p.ID_PENGGUNA}&q={$smarty.get.q}">Reset</a>
        {/if}
        </td>
        
    </tr>
    {/foreach}
</table>
{/if}