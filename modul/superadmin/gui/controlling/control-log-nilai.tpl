<div class="center_title_bar">Log Perubahan Penilaian</div>
{if $smarty.get.mode=='' or $smarty.get.mode=='cari'}
    <form method="get" action="control-log-nilai.php">
        <table>
            <tr>
                <th colspan="2" class="center"> Pencarian</th>
            </tr>
            <tr>
                <td>Cari Nama/NIM</td>
                <td>
                    <input type="text" size="30" name="cari" value="{$smarty.get.cari}" />
                    <input type="submit" value="Cari" class="button" />
                    <input type="hidden" name="mode" value="cari"/>
                </td>
            </tr>
        </table>
    </form>
{/if}
{if isset($smarty.get.mode)}
    {if isset($data_biodata)}
        <a class="button disable-ajax" onclick="history.back(-1)">Kembali</a>
        <p></p>
        <table style="width: 100%">
            <tr>
                <th colspan="2">DETAIL BIODATA MAHASISWA</th>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <img src="{$foto}" width="180" height="230"/>
                </td>
            </tr>
            <tr>
                <td>NAMA</td>
                <td>{$data_biodata.NM_PENGGUNA}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>{$data_biodata.NIM_MHS}</td>
            </tr>
            <tr>
                <td>ANGKATAN</td>
                <td>{$data_biodata.THN_ANGKATAN_MHS}</td>
            </tr>
            <tr>
                <td>JENJANG</td>
                <td>{$data_biodata.NM_JENJANG}</td>
            </tr>
            <tr>
                <td>FAKULTAS</td>
                <td>{$data_biodata.NM_FAKULTAS|upper}</td>
            </tr>
            <tr>
                <td>PROGRAM STUDI</td>
                <td>{$data_biodata.NM_PROGRAM_STUDI}</td>
            </tr>
            <tr>
                <td>TEMPAT/TANGGAL LAHIR</td>
                <td> {$data_biodata.TEMPAT_LAHIR} / {$data_biodata.TGL_LAHIR_PENGGUNA|date_format:"%d %B %Y"}</td>
            </tr>
            <tr>
                <td>KONTAK</td>
                <td>{$data_biodata.MOBILE_MHS}</td>
            </tr>
            <tr>
                <td>KELAMIN</td>
                <td>
                    {if $data_biodata.KELAMIN_PENGGUNA==1}
                        Laki-laki
                    {else}
                        Perempuan
                    {/if}
                </td>
            </tr>
            <tr>
                <td>EMAIL</td>
                <td>{$data_biodata.EMAIL_PENGGUNA}</td>
            </tr>
            <tr>
                <td>BLOG MAHASISWA</td>
                <td>{$data_biodata.BLOG_PENGGUNA}</td>
            </tr>
            <tr>
                <td>ALAMAT MAHASISWA</td>
                <td>{$data_biodata.ALAMAT_MHS}</td>
            </tr>
            <tr>
                <td>NAMA AYAH</td>
                <td>{$data_biodata.NM_AYAH_MHS}</td>
            </tr>
            <tr>
                <td>ALAMAT AYAH</td>
                <td>{$data_biodata.ALAMAT_AYAH_MHS}</td>
            </tr>
            <tr>
                <td>PENGHASILAN ORTU</td>
                <td>{number_format($data_biodata.PENGHASILAN_ORTU_MHS)}</td>
            </tr>
        </table>
        <br/><hr style="width:90%"/><br/>
        <table style="width: 100%;">
            <tr>
                <th colspan="6">DETAIL AKADEMIK MAHASISWA</th>
            </tr>
            <tr>
                <td>NAMA</td>
                <td style="width: 230px;">: {$data_mhs_status.NM_PENGGUNA}</td>
                <td>IPS</td>
                <td>: {$data_mhs_status.IPS}</td>
                <td>TOTAL SKS</td>
                <td>: {$data_mhs_status.TOTAL_SKS}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>: {$data_mhs_status.NIM_MHS}</td>
                <td>IPK</td>
                <td>: {$data_mhs_status.IPK}</td>
                <td>ANGKATAN</td>
                <td>: {$data_mhs_status.THN_ANGKATAN_MHS}</td>           
            </tr>
            <tr>
                <td>PROGRAM STUDI</td>
                <td colspan="5">: ({$data_mhs_status.NM_JENJANG}) {$data_mhs_status.NM_PROGRAM_STUDI}</td>
            </tr>
        </table>
        <br/><hr style="width:90%"/><br/>
        <table  style="width:100% ">
            <tr>
                <th colspan="7">HISTORY NILAI MAHASISWA</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>SEMESTER</th>
                <th>KODE AJAR</th>
                <th>NAMA MATA KULIAH</th>
                <th>SKS</th>
                <th>NILAI</th>
                <th>LOG</th>
            </tr>
            {foreach $data_history_nilai as $data}
                <tr 
                    {if $data.STATUS_ULANG>1&&$data.ULANG_KE>1}
                        style="background-color: #ffcccc"
                    {elseif $data.STATUS_ULANG>1&&$data.ULANG_KE==1}
                        style="background-color: #8f8"
                    {else}

                    {/if}
                    >
                    <td>{$data@index+1}</td>
                    <td>{$data.NM_SEMESTER} ({$data.TAHUN_AJARAN})</td>
                    <td>{$data.KD_MATA_KULIAH}</td>
                    <td>{$data.NM_MATA_KULIAH}</td>
                    <td>{$data.KREDIT_SEMESTER}</td>
                    <td>
                        {if $data.NILAI_HURUF==''}
                            Nilai Kosong
                        {else}
                            {$data.NILAI_HURUF}
                        {/if}
                    </td>
                    <td>
                        <a href="control-log-nilai.php?mode=log-nilai&mhs={$smarty.get.mhs}&mk={$data.KD_MATA_KULIAH}&sem={$data.ID_SEMESTER}">Log</a>
                    </td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="7">
                    <p style="font-weight: bold">Keterangan : </p>
                    <ol>
                        <li>
                            <p>Warna <span class="ui-corner-all" style="background-color: #ffcccc;padding: 3px">Merah</span> : Sudah diulang, tidak masuk transkrip dan tidak masuk perhitungan IP kumulatif dan sks kumulatif</p>
                        </li>
                        <li>
                            <p>Warna <span class="ui-corner-all" style="background-color: #8f8;padding: 3px">Hijau</span> : Ulangan, masuk transkrip dan perhitungan IP kumulatif dan sks kumulatif</p>
                        </li>
                    </ol>
                </td>
            </tr>
        </table>
        <br/><hr style="width:90%"/><br/>
    {else if isset($data_log_nilai)||isset($data_log_granted_ubah_nilai)}
        <a class="button disable-ajax" onclick="history.back(-1)">Kembali</a>
        <p></p>
        <table style="width: 98%">
            <tr>
                <th colspan="8">LOG PERUBAHAN NILAI DARI DOSEN</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>MATA KULIAH</th>
                <th>SEMESTER</th>
                <th>KOMPONEN</th>
                <th>PERUBAHAN NILAI</th>
                <th>PERUBAH SEBELUM</th>
                <th>PERUBAH TERBARU</th>
                <th>WAKTU</th>
            </tr>
            {foreach $data_log_nilai_dosen as $l}
                <tr>
                    <td>{$l@index+1}</td>
                    <td>{$l.KD_MATA_KULIAH} {$l.NM_MATA_KULIAH}</td>
                    <td>{$l.NM_SEMESTER} {$l.TAHUN_AJARAN}</td>
                    <td class="center">{$l.NM_KOMPONEN_MK} <br/>({$l.PERSEN}%)</td>
                    <td>{$l.NILAI_ASAL} <span style="color: green;font-weight: bold;font-family: fantasy;font-size: 1.2em">&#8594;</span> {$l.NILAI_BARU}</td>
                    <td class="center">{$l.DOSEN_LAMA} <br/>{$l.NIP_LAMA}</td>
                    <td class="center">{$l.DOSEN_BARU} <br/>{$l.NIP_BARU}</td>
                    <td>{$l.WAKTU}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="9" class="center" style="color: red">Data Kosong</td>
                </tr>
            {/foreach}
        </table>

        <table style="width: 98%;display: none">
            <tr>
                <th colspan="8">LOG GRANTED PERUBAHAN NILAI DARI WADEK</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>MATA KULIAH</th>
                <th>SEMESTER PENGAMBILAN</th>
                <th>SEMESTER PERUBAHAN</th>
                <th>NAMA PENGUBAH</th>
                <th>IP ADDRESS</th>
                <th>WAKTU</th>
                <th>ALASAN PERUBAHAN</th>
            </tr>
            {foreach $data_log_granted_ubah_nilai as $l}
                <tr>
                    <td>{$l@index+1}</td>
                    <td>{$l.KD_MATA_KULIAH} {$l.NM_MATA_KULIAH}</td>
                    <td>{$l.NM_SEMESTER} {$l.TAHUN_AJARAN}</td>
                    <td>{$l.NM_SEMESTER_UBAH} {$l.TAHUN_AJARAN_UBAH}</td>
                    <td>{$l.NM_PENGUBAH} </td>
                    <td class="center">{$l.IP_ADDRESS}</td>
                    <td class="center">{$l.WAKTU_PERUBAHAN}</td>
                    <td>{$l.ALASAN}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="8" class="center" style="color: red">Data Kosong</td>
                </tr>
            {/foreach}
        </table>

        <table style="width: 98%">
            <tr>
                <th colspan="8">LOG PERUBAHAN NILAI DARI AKADEMIK</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>MATA KULIAH</th>
                <th>SEMESTER PENGAMBILAN</th>
                <th>PERUBAHAN NILAI</th>
                <th>NAMA PENGUBAH</th>
                <th>IP ADDRESS</th>
                <th>WAKTU</th>
                <th>KETERANGAN</th>
            </tr>
            {$no=1}
            {foreach $data_log_ubah_nilai as $l}
                <tr>
                    <td>{$l@index+1}</td>
                    <td>{$l.KD_MATA_KULIAH} {$l.NM_MATA_KULIAH}</td>
                    <td>{$l.NM_SEMESTER} {$l.TAHUN_AJARAN}</td>
                    <td>{if $l.NILAI_HURUF_LAMA==''}Kosong{else}{$l.NILAI_HURUF_LAMA}{/if} <span style="color: green;font-weight: bold;font-family: fantasy;font-size: 1.2em">&#8594;</span> {$l.NILAI_HURUF_BARU}</td>
                    <td>{$l.NM_PENGUBAH} </td>
                    <td class="center">{$l.IP_ADDRESS}</td>
                    <td class="center">{$l.WAKTU_PERUBAHAN}</td>
                    <td><button onclick="$('#referensi-perubahan-nilai{$no}').toggle()">Show/Hide</button><span style="display: none" id="referensi-perubahan-nilai{$no}">{$l.REFERENSI}</span></td>
                </tr>
                {$no=$no+1}
            {/foreach}
            {foreach $data_log_konversi_nilai as $l}
                <tr>
                    <td>{$l@index+1}</td>
                    <td>{$l.KD_MATA_KULIAH} {$l.NM_MATA_KULIAH}</td>
                    <td>{$l.NM_SEMESTER} {$l.TAHUN_AJARAN}</td>
                    <td>{$l.NILAI_HURUF_LAMA} <span style="color: green;font-weight: bold;font-family: fantasy;font-size: 1.2em">&#8594;</span> {$l.NILAI_HURUF_BARU}</td>
                    <td>{$l.NM_PENGUBAH} </td>
                    <td class="center">{$l.IP_ADDRESS}</td>
                    <td class="center">{$l.WAKTU_PERUBAHAN}</td>
                    <td><button onclick="$('#referensi-perubahan-nilai{$no}').toggle()">Show/Hide</button><span style="display: none" id="referensi-perubahan-nilai{$no}">{$l.KETERANGAN}</span></td>
                </tr>
                {$no=$no+1}
            {/foreach}
            {if count($data_log_ubah_nilai)==0 && count($data_log_konversi_nilai)==0}
                <tr>
                    <td colspan="8" class="center" style="color: red">Data Kosong</td>
                </tr>
            {/if}
        </table>
        
        
        <table style="width: 98%;display: none">
            <tr>
                <th colspan="7">LOG GRANTED TRANSFER NILAI DARI WADEK</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>SEMESTER PERUBAHAN</th>
                <th>TOTAL MATA KULIAH</th>
                <th>NAMA PENGUBAH</th>
                <th>IP ADDRESS</th>
                <th>WAKTU</th>
                <th>ALASAN PERUBAHAN</th>
            </tr>
            {foreach $data_log_granted_transfer_nilai as $l}
                <tr>
                    <td>{$l@index+1}</td>
                    <td>{$l.NM_SEMESTER} {$l.TAHUN_AJARAN}</td>
                    <td>{$l.TOTAL_MATA_KULIAH}</td>
                    <td>{$l.NM_PENGUBAH} </td>
                    <td class="center">{$l.IP_ADDRESS}</td>
                    <td class="center">{$l.WAKTU_PERUBAHAN}</td>
                    <td>{$l.ALASAN}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="7" class="center" style="color: red">Data Kosong</td>
                </tr>
            {/foreach}
        </table>
        
        
        <table style="width: 98%">
            <tr>
                <th colspan="8">LOG TRANSFER NILAI DARI AKADEMIK</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>MATA KULIAH</th>
                <th>SEMESTER PENGAMBILAN</th>
                <th>NAMA PENGUBAH</th>
                <th>NILAI DIRUBAH</th>
                <th>IP ADDRESS</th>
                <th>WAKTU</th>
                <th>KETERANGAN</th>
            </tr>
            {foreach $data_log_transfer_nilai as $l}
                <tr>
                    <td>{$l@index+1}</td>
                    <td>{$l.KD_MATA_KULIAH} {$l.NM_MATA_KULIAH}</td>
                    <td>{$l.NM_SEMESTER} {$l.TAHUN_AJARAN}</td>
                    <td>{$l.NM_PENGUBAH} </td>
                    <td>{$l.NILAI} </td>
                    <td class="center">{$l.IP_ADDRESS}</td>
                    <td class="center">{$l.WAKTU_PERUBAHAN}</td>
                    <td><button onclick="$('#referensi-transfer-nilai{$l@index+1}').toggle();">Show/Hide</button><span style="display: none" id="referensi-transfer-nilai{$l@index+1}">{$l.KETERANGAN}</span></td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="8" class="center" style="color: red">Data Kosong</td>
                </tr>
            {/foreach}
        </table>


    {else if isset($data_cari)}
        <table style="width: 98%">
            <tr>
                <th>NO</th>
                <th>NAMA</th>
                <th>NIM</th>
                <th>PROGRAM STUDI</th>
                <th>FAKULTAS</th>
                <th>STATUS</th>
                <th>DETAIL</th>
            </tr>
            {foreach $data_cari as $d}
                <tr>
                    <td>{$d@index+1}</td>
                    <td>{$d.NM_PENGGUNA}</td>
                    <td>{$d.NIM_MHS}</td>
                    <td>{$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI}</td>
                    <td>{$d.NM_FAKULTAS}</td>
                    <td>{$d.NM_STATUS_PENGGUNA}</td>
                    <td>
                        <a class="button" href="control-log-nilai.php?mode=detail&mhs={$d.ID_MHS}">DETAIL</a>
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="7" class="data-kosong">Data Tidak Ditemukan</td>
                </tr>
            {/foreach}
        </table>
    {/if}
{/if}