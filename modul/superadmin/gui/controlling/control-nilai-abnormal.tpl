<div class="center_title_bar">Data Nilai Abnormal</div>
{if $smarty.get.mode=='' or $smarty.get.mode=='tampil'}
    <form method="get" action="control-nilai-abnormal.php">
        <table>
            <tr>
                <th colspan="2" class="center">INPUT </th>
            </tr>
            <tr>
                <td>Pilih Fakultas</td>
                <td>
                    <select name="fakultas">
                        <option>Semua</option>
                        {foreach $data_fakultas as $f}
                            <option value="{$f.ID_FAKULTAS}" {if $f.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$f.NM_FAKULTAS}</option>
                        {/foreach}
                    </select>
                    <input type="submit" value="Tampil" class="button" />
                    <input type="hidden" name="mode" value="tampil"/>
                </td>
            </tr>
        </table>
    </form>
{/if}
{if isset($data_mhs_nilai_abnormal)}
    <br/><hr style="width:90%"/><br/>
    <table  style="width:100% ">
        <tr>
            <th colspan="6">DATA MAHASISWA DENGAN NILAI ABNORMAL </th>
        </tr>
        <tr>
            <th>NO</th>
            <th>NIM MHS</th>
            <th>NAMA</th>
            <th>PRODI</th>
            <th>JUMLAH MATA AJAR</th>
            <th>DETAIL</th>
        </tr>
        {$no=1}
        {foreach $data_mhs_nilai_abnormal as $l}
            {if $l.JUMLAH_LOG_KONVERSI>0||$l.JUMLAH_LOG_UPDATE>0}
            <tr>
                <td>{$no}</td>
                <td>{$l.NIM_MHS}</td>
                <td>{$l.NM_PENGGUNA}</td>
                <td>{$l.NM_JENJANG} {$l.NM_PROGRAM_STUDI}<br/> Fak.{$l.NM_FAKULTAS}</td>
                <td>{$l.JUMLAH_BEDA}</td>
                <td>
                    <a href="control-nilai-abnormal.php?mode=mk-abnormal&mhs={$l.ID_MHS}">Detail</a>
                </td>
            </tr>
            {$no=$no+1}
            {/if}
        {foreachelse}
            <tr>
                <td colspan="6" class="center" style="color: red">Data Kosong</td>
            </tr>
        {/foreach}
    </table>
{/if}
{if isset($data_mk_nilai_abnormal)}
    <a class="button disable-ajax" onclick="history.back(-1)">Kembali</a>
    <p></p>
    <table style="width: 100%">
        <tr>
            <th colspan="2">DETAIL BIODATA MAHASISWA</th>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <img src="{$foto}" width="180" height="230"/>
            </td>
        </tr>
        <tr>
            <td>NAMA</td>
            <td>{$data_biodata.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>{$data_biodata.NIM_MHS}</td>
        </tr>
        <tr>
            <td>ANGKATAN</td>
            <td>{$data_biodata.THN_ANGKATAN_MHS}</td>
        </tr>
        <tr>
            <td>JENJANG</td>
            <td>{$data_biodata.NM_JENJANG}</td>
        </tr>
        <tr>
            <td>FAKULTAS</td>
            <td>{$data_biodata.NM_FAKULTAS|upper}</td>
        </tr>
        <tr>
            <td>PROGRAM STUDI</td>
            <td>{$data_biodata.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td>TEMPAT/TANGGAL LAHIR</td>
            <td> {$data_biodata.TEMPAT_LAHIR} / {$data_biodata.TGL_LAHIR_PENGGUNA|date_format:"%d %B %Y"}</td>
        </tr>
        <tr>
            <td>KONTAK</td>
            <td>{$data_biodata.MOBILE_MHS}</td>
        </tr>
        <tr>
            <td>KELAMIN</td>
            <td>
                {if $data_biodata.KELAMIN_PENGGUNA==1}
                    Laki-laki
                {else}
                    Perempuan
                {/if}
            </td>
        </tr>
        <tr>
            <td>EMAIL</td>
            <td>{$data_biodata.EMAIL_PENGGUNA}</td>
        </tr>
        <tr>
            <td>BLOG MAHASISWA</td>
            <td>{$data_biodata.BLOG_PENGGUNA}</td>
        </tr>
        <tr>
            <td>ALAMAT MAHASISWA</td>
            <td>{$data_biodata.ALAMAT_MHS}</td>
        </tr>
        <tr>
            <td>NAMA AYAH</td>
            <td>{$data_biodata.NM_AYAH_MHS}</td>
        </tr>
        <tr>
            <td>ALAMAT AYAH</td>
            <td>{$data_biodata.ALAMAT_AYAH_MHS}</td>
        </tr>
        <tr>
            <td>PENGHASILAN ORTU</td>
            <td>{number_format($data_biodata.PENGHASILAN_ORTU_MHS)}</td>
        </tr>
    </table>
    <br/><hr style="width:90%"/><br/>
    <table style="width: 100%;">
        <tr>
            <th colspan="6">DETAIL AKADEMIK MAHASISWA</th>
        </tr>
        <tr>
            <td>NAMA</td>
            <td style="width: 230px;">: {$data_mhs_status.NM_PENGGUNA}</td>
            <td>IPS</td>
            <td>: {$data_mhs_status.IPS}</td>
            <td>TOTAL SKS</td>
            <td>: {$data_mhs_status.TOTAL_SKS}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>: {$data_mhs_status.NIM_MHS}</td>
            <td>IPK</td>
            <td>: {$data_mhs_status.IPK}</td>
            <td>ANGKATAN</td>
            <td>: {$data_mhs_status.THN_ANGKATAN_MHS}</td>           
        </tr>
        <tr>
            <td>PROGRAM STUDI</td>
            <td colspan="5">: ({$data_mhs_status.NM_JENJANG}) {$data_mhs_status.NM_PROGRAM_STUDI}</td>
        </tr>
    </table>
    <br/><hr style="width:90%"/><br/>
    <table  style="width:100% ">
        <tr>
            <th colspan="5">DATA NILAI ABNORMAL MAHASISWA</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>MATA AJAR</th>
            <th>NILAI SEKARANG</th>
            <th>NILAI SEHARUSNYA</th>
            <th>LOG</th>
        </tr>
        {foreach $data_mk_nilai_abnormal as $l}
            <tr>
                <td>{$l@index+1}</td>
                <td>({$l.KD_MATA_KULIAH}) {$l.NM_MATA_KULIAH} <br/>{$l.NM_SEMESTER} {$l.TAHUN_AJARAN}</td>
                <td class="center">{$l.NILAI_ANGKA} ({$l.NILAI_HURUF})</td>
                <td class="center">{$l.NM_STANDAR_NILAI}</td>
                <td>
                    <a href="control-log-nilai.php?mode=log-nilai&mhs={$l.ID_MHS}&mk={$l.KD_MATA_KULIAH}&sem={$l.ID_SEMESTER}">Log</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="5" class="center" style="color: red">Data Kosong</td>
            </tr>
        {/foreach}
    </table>
{/if}