<div class="center_title_bar">Template Role</div>

<form action="role-template.php" method="get">
	<table>
		<tbody>
			<tr>
				<td>
					<label>Role</label>
					<select name="id_role" onchange="javascript: $(this).submit();">
						<option value="">-- Pilih Role --</option>
						{foreach $role_set as $role}
							<option value="{$role.ID_ROLE}" {if isset($smarty.get.id_role)}{if $smarty.get.id_role == $role.ID_ROLE}selected{/if}{/if}>{$role.NM_ROLE}</option>
						{/foreach}
					</select>
					{if !empty($smarty.get.id_role)}
						<a href="role-templatemodul.php?id_role={$smarty.get.id_role}">Template Modul</a>
					{/if}
				</td>
			</tr>
		</tbody>
	</table>
</form>

{if isset($template_set)}
	<table>
		<thead>
			<tr>
				<th>Nama Template</th>
				<th>Deskripsi</th>
				<th>User Aktif</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $template_set as $template}
				<tr>
					<td>{$template.NM_TEMPLATE}</td>
					<td>{$template.DESKRIPSI}</td>
					<td class="center">{$template.JUMLAH_USER}</td>
					<td>
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="4" class="center">
					<a href="role-template.php?mode=add&id_role={$smarty.get.id_role}">Tambah Template</a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}