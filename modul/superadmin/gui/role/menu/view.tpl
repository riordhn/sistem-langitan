<div class="center_title_bar">Menu</div>

<form action="role-menu.php" method="get">
	<table>
		<tbody>
			<tr>
				<td>
					<label>Role</label>
					<select name="id_role" onchange="javascript: $(this).submit();">
						<option value="">-- Pilih Role --</option>
						{foreach $role_set as $role}
							<option value="{$role.ID_ROLE}" {if isset($smarty.get.id_role)}{if $smarty.get.id_role == $role.ID_ROLE}selected{/if}{/if}>{$role.NM_ROLE}</option>
						{/foreach}
					</select>
					<label>Modul</label>
					<select name="id_modul" onchange="javascript: $(this).submit();">
						<option value="">-- Pilih Modul --</option>
						{foreach $modul_set as $modul}
							<option value="{$modul.ID_MODUL}" {if isset($smarty.get.id_modul)}{if $smarty.get.id_modul == $modul.ID_MODUL}selected{/if}{/if}>{$modul.URUTAN}-{$modul.TITLE}</option>
						{/foreach}
					</select>
				</td>
			</tr>
		</tbody>
	</table>
</form>

{if isset($menu_set)}
	<table>
		<thead>
			<tr>
				<th>Kode Menu</th>
				<th>Nama Menu</th>
				<th>Halaman</th>
				<th>Urutan</th>
				<th>Default Akses</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $menu_set as $menu}
				<tr>
					<td><strong>{$menu.NM_MENU}</strong></td>
					<td>{$menu.TITLE}</td>
					<td>{if $menu.FILE_EXISTS == 0}<i style="color: red">{$menu.PAGE}</i>{else}{$menu.PAGE}{/if}</td>
					<td class="center">{$menu.URUTAN}</td>
					<td class="center">{if $menu.AKSES == 1}YA{else}-{/if}</td>
					<td class="center">
						<a href="role-menu.php?mode=edit&id_role={$id_role}&id_modul={$menu.ID_MODUL}&id_menu={$menu.ID_MENU}">Edit</a>
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="6" class="center">
					<a href="role-menu.php?mode=add&id_role={$id_role}&id_modul={$smarty.get.id_modul}">Tambah Menu</a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}