<div class="center_title_bar">Modul</div>

<form action="role-modul.php" method="get">
	<table>
		<tbody>
			<tr>
				<td>
					<label>Role</label>
					<select name="id_role" onchange="javascript: $(this).submit();">
						<option value="">-- Pilih Role --</option>
						{foreach $role_set as $role}
							<option value="{$role.ID_ROLE}" {if isset($smarty.get.id_role)}{if $smarty.get.id_role == $role.ID_ROLE}selected{/if}{/if}>{$role.NM_ROLE}</option>
						{/foreach}
					</select>
				</td>
			</tr>
		</tbody>
	</table>
</form>

{if isset($modul_set)}
	<table>
		<thead>
			<tr>
				<th>Kode Modul</th>
				<th>Nama Modul</th>
				<th>Halaman</th>
				<th>Urutan</th>
				<th>Default Akses</th>
				<th>Jumlah Menu</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $modul_set as $modul}
				<tr>
					<td><strong>{$modul.NM_MODUL}</strong></td>
					<td>{$modul.TITLE}</td>
					<td>{if $modul.FILE_EXISTS == 0}<i style="color: red">{$modul.PAGE}</i>{else}{$modul.PAGE}{/if}</td>
					<td class="center">{$modul.URUTAN}</td>
					<td class="center">{if $modul.AKSES == 1}YA{else}-{/if}</td>
					<td class="center">
						<a href="role-menu.php?id_role={$modul.ID_ROLE}&id_modul={$modul.ID_MODUL}">{$modul.JUMLAH_MENU}</a>
					</td>
					<td class="center">
						<a href="role-modul.php?mode=edit&id_role={$modul.ID_ROLE}&id_modul={$modul.ID_MODUL}">Edit</a>
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="7" class="center">
					<a href="role-modul.php?mode=add&id_role={$smarty.get.id_role}">Tambah Modul</a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}