<div class="center_title_bar">Template Modul</div>

<form action="role-templatemodul.php" method="get">
	<table>
		<tbody>
			<tr>
				<td>
					<label>Role</label>
					<select name="id_role" onchange="javascript: $(this).submit();">
						<option value="">-- Pilih Role --</option>
						{foreach $role_set as $role}
							<option value="{$role.ID_ROLE}" {if isset($smarty.get.id_role)}{if $smarty.get.id_role == $role.ID_ROLE}selected{/if}{/if}>{$role.NM_ROLE}</option>
						{/foreach}
					</select>
				</td>
			</tr>
		</tbody>
	</table>
</form>

{if isset($modul_set)}

	<table>
		<thead>
			<tr>
				<th>Template</th>
				{foreach $modul_set as $modul}
					<th>{$modul.TITLE}</th>
				{/foreach}
			</tr>
		</thead>
		<tbody>
			<!-- Baris 1 untuk yg Default -->
			<tr class="row1">
				<td><i>Default</i></td>
				{foreach $modul_set as $modul}
					<td class="center">
						{if $modul.AKSES == 1}
							<img src="../../img/superadmin/navigation/ok-green.png" />
						{else}
							<img src="../../img/superadmin/navigation/del-red.png" />
						{/if}
					</td>
				{/foreach}
			</tr>
			{foreach $template_role_set as $template_role}
				<tr>
					<td><strong>{$template_role.NM_TEMPLATE}-{$template_role.ID_TEMPLATE_ROLE}</strong></td>
					{foreach $modul_set as $modul}
						<td class="center">
							{$exist = 0}
							{foreach $template_modul_set as $template_modul}

								{if $template_modul.ID_TEMPLATE_ROLE == $template_role.ID_TEMPLATE_ROLE and $template_modul.ID_MODUL == $modul.ID_MODUL}
									{if $template_modul.AKSES == 1}
										<span style="cursor: pointer" title="Klik untuk disable" data-action="disable" data-id-t-role="{$template_role.ID_TEMPLATE_ROLE}" data-id-modul="{$modul.ID_MODUL}" data-akses="0">
									 		<img src="../../img/superadmin/navigation/ok-green.png" />
									 	</span>
									{else}
										<span style="cursor: pointer" title="Klik untuk enable" data-action="enable" data-id-t-role="{$template_role.ID_TEMPLATE_ROLE}" data-id-modul="{$modul.ID_MODUL}" data-akses="1">
											<img src="../../img/superadmin/navigation/del-red.png" />
										</span>
									{/if}
									{$exist = 1}
								{/if}

							{/foreach}
							{if $exist == 0}
								<span style="cursor: pointer" title="Override dari Default" data-action="enable" data-id-t-role="{$template_role.ID_TEMPLATE_ROLE}" data-id-modul="{$modul.ID_MODUL}" data-akses="1">--</span>
							{/if}
						</td>
					{/foreach}
				</tr>
			{/foreach}
		</tbody>
	</table>

{/if}

<script>
	$(document).ready(function(){
		$('span').on('click', function(){
			
			var action		= $(this).data('action');
			var id_t_role	= $(this).data('id-t-role');
			var id_modul	= $(this).data('id-modul');
			var akses		= $(this).data('akses');

			$.ajax({
				url: 'role-templatemodul.php?mode=set-template-modul&action='+action
			}).done(function(resultText) {
				alert('Ajax Sukses: ' + resultText);
			}).fail(function() {
				alert('Terjadi kesalahan saat ajax');
			});

		});
	});
		

</script>