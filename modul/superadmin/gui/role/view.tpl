<div class="center_title_bar">Role</div>

<table>
	<thead>
		<tr>
			<th>Role</th>
			<th>Path</th>
			<th>Perm | Owner</th>
			<th>Modul</th>
			<th>Menu</th>
			<th>Template</th>
			<th>User Aktif</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		{foreach $role_set as $role}
		<tr {if $role@index is not div by 2}class="row1"{/if}>
			<td>{$role.NM_ROLE}</td>
			<td>{if $role.FILE_EXISTS == 0}<i style="color: red">{$role.PATH}</i>{else}{$role.PATH}{/if}</td>
			<td>{if $role.FILE_EXISTS == 1}{$role.PERMISSION} | {$role.FILE_OWNER}{/if}</td>
			<td class="center"><a href="role-modul.php?id_role={$role.ID_ROLE}">{$role.JUMLAH_MODUL}</a></td>
			<td class="center">{$role.JUMLAH_MENU}</td>
			<td class="center">
				<a href="role-template.php?id_role={$role.ID_ROLE}">{$role.JUMLAH_TEMPLATE}</a>
			</td>
			<td class="center">{$role.JUMLAH_USER}</td>
			<td class="center">

			</td>
		</tr>
		{/foreach}
	</tbody>
</table>