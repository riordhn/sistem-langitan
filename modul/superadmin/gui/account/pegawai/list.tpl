<div class="center_title_bar">Account Pegawai</div>

<form action="account-pegawai.php" method="get" id="filter">
    <table>
        <tr>
            <th colspan="4">Filter</th>
        </tr>
        <tr>
            <td>Role</td>
            <td>
                <select name="id_role" onchange="$('#filter').submit();">
                    <option value="">-- Pilih Role --</option>
                    <option value="all">[SEMUA]</option>
                {foreach $role_set as $r}
                    <option value="{$r.ID_ROLE}" {if $r.ID_ROLE == $smarty.get.id_role}selected="selected"{/if}>{$r.NM_ROLE} ({$r.JUMLAH})</option>
                {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>

<table>
    <tr>
        <th>No</th>
        <th>NIP</th>
        <th>Nama</th>
        <th>Role</th>
        <th>Unit Kerja</th>
    </tr>
    {foreach $pegawai_set as $p}
    <tr {if $p@index is not div by 2}class="row1"{/if}>
        <td class="center">{$p@index + 1}</td>
        <td>{$p.USERNAME}</td>
        <td>{$p.NM_PENGGUNA}</td>
        <td>{$p.NM_ROLE}</td>
        <td>{$p.NM_UNIT_KERJA}</td>
    </tr>
    {/foreach}
</table>