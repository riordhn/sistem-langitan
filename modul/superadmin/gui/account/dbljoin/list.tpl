<div class="center_title_bar">Account Yang Terjoin Double</div>

<table>
    <tr>
        <th>No</th>
        <th>Username</th>
        <th>Nama</th>
        <th>Role</th>
        <th>Dosen</th>
        <th>Pegawai</th>
    </tr>
    {foreach $pengguna_set as $p}
    <tr {if $p@index is not div by 2}class="row1"{/if}>
        <td class="center">{$p@index + 1}</td>
        <td>{$p.USERNAME}</td>
        <td>{$p.NM_PENGGUNA}</td>
        <td>{$p.NM_ROLE}</td>
        <td class="center">{$p.JUMLAH_DOSEN}</td>
        <td class="center">{$p.JUMLAH_PEGAWAI}</td>
    </tr>
    {/foreach}
</table>