<div class="center_title_bar">Account Dosen</div>

<form action="account-dosen.php" method="get" id="filter">
    <table>
        <tr>
            <th colspan="4">Filter</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select name="id_fakultas" onchange="$('#filter').submit();">
                    <option value="">-- Pilih Fakultas --</option>
                {foreach $fakultas_set as $f}
                    <option value="{$f.ID_FAKULTAS}" {if $f.ID_FAKULTAS == $smarty.get.id_fakultas}selected="selected"{/if}>{$f.NM_FAKULTAS} ({$f.JUMLAH})</option>
                {/foreach}
                </select>
            </td>
            <td>Role</td>
            <td>
                <select name="role">
                    <option value="">-- Pilih Role --</option>
                    <option value="dosen">Dosen</option>
                    <option value="non-dosen">Non-Dosen</option>
                </select>
            </td>
        </tr>
    </table>
</form>

<table>
    <tr>
        <th>No</th>
        <th>Username</th>
        <th>Nama</th>
        <th>Program Studi</th>
        <th>Role</th>
    </tr>
    {foreach $dosen_set as $d}
    <tr {if $d@index is not div by 2}class="row1"{/if}>
        <td class="center">{$d@index + 1}</td>
        <td>{$d.USERNAME}</td>
        <td>{$d.NM_PENGGUNA}</td>
        <td>{$d.NM_PROGRAM_STUDI}</td>
        <td>{$d.NM_ROLE}</td>
    </tr>
    {/foreach}
</table>