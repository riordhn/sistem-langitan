<div class="center_title_bar">Account Mahasiswa</div>

<form action="account-mahasiswa.php" method="get" id="filter">
    <table>
        <tr>
            <th colspan="4">Filter</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select name="id_fakultas" onchange="javascript: $(this).submit();">
                    <option value="">-- Pilih Fakultas --</option>
                {foreach $fakultas_set as $f}
                    <option value="{$f.ID_FAKULTAS}" {if $f.ID_FAKULTAS == $smarty.get.id_fakultas}selected="selected"{/if}>{$f.NM_FAKULTAS} ({$f.JUMLAH})</option>
                {/foreach}
                </select>
            </td>
            <td>Program Studi</td>
            <td>
                <select name="id_prodi" onchange="javascript: $(this).submit();">
                    <option value="">-- Pilih Program Studi --</option>
                {foreach $prodi_set as $p}
                    <option value="{$p.ID_PROGRAM_STUDI}" {if $p.ID_PROGRAM_STUDI == $smarty.get.id_prodi}selected="selected"{/if}>{$p.NM_JENJANG_SNGKAT} - {$p.NM_PROGRAM_STUDI} ({$p.JUMLAH})</option>
                {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>

{if isset($mhs_set)}
<form action="account-mahasiswa.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="edit" />
    <table>
        <tr>
            <th>No</th>
            <th>NIM Mahasiswa</th>
            <th>Nama Mahasiswa</th>
            <th>Program Studi</th>
            <th>Reset Password</th>
        </tr>
        <tbody>
            {foreach $mhs_set as $mhs}
                <tr {if $mhs@index is not div by 2}class="row1"{/if}>
                    <td class="center">{$mhs@index + 1}</td>
                    <td>{$mhs.NIM_MHS}</td>
                    <td>{$mhs.NM_PENGGUNA}</td>
                    <td>{$mhs.NM_JENJANG_SNGKAT} - {$mhs.NM_PROGRAM_STUDI}</td>
                    <td class="center">
                            <a href="account-mahasiswa.php?mode=edit&id_mhs={$mhs.ID_MHS}">Reset</a>
                            <!--<input id="id_pengguna" type="hidden" name="id_pengguna" value="{$mhs.ID_PENGGUNA}" />
                            <input id="nim_mhs" type="hidden" name="nim_mhs" value="{$mhs.NIM_MHS}" />
                            <input type="submit" value="Reset" />-->
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</form>
{/if}