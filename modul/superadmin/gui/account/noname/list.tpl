<div class="center_title_bar">Account Tidak Ada Nama</div>

<table>
    <tr>
        <th>No</th>
        <th>Username</th>
        <th>Nama</th>
        <th>Role</th>
    </tr>
    {foreach $pengguna_set as $p}
    <tr {if $p@index is not div by 2}class="row1"{/if}>
        <td class="center">{$p@index + 1}</td>
        <td>{$p.USERNAME}</td>
        <td>{$p.NM_PENGGUNA}</td>
        <td>{$p.NM_ROLE}</td>
    </tr>
    {/foreach}
</table>