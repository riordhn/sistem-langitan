<div class="center_title_bar">Pencarian Account</div>

<form action="account-search.php" method="get">
    <table>
        <tr>
            <td>Nama / Username : </td>
            <td><input type="search" name="q" value="{if isset($smarty.get.q)}{$smarty.get.q}{/if}" /></td>
        </tr>
    </table>
</form>

{if !empty($pengguna_set)}
<table>
    <tr>
        <th>No</th>
        <th>ID</th>
        <th>Username</th>
        <th>Nama</th>
        <th>Tipe Account</th>
        <th>Default Role</th>
        <th>Multi Role</th>
        <th>Action</th>
        
    </tr>
    {foreach $pengguna_set as $p}
    <tr {if $p@index is not div by 2}class="row1"{/if}>
        <td class="center">{$p@index + 1}</td>
        <td>{$p.ID_PENGGUNA}</td>
        <td>{$p.USERNAME}</td>
        <td>{$p.NM_PENGGUNA}</td>
        <td class="center">{if $p.JOIN_TABLE == 1}Pegawai{else if $p.JOIN_TABLE == 2}Dosen{else if $p.JOIN_TABLE == 3}Mahasiswa{/if}</td>
        <td class="center">{$p.NM_ROLE}</td>
        <td class="center">{if $p.JUMLAH_ROLE > 1}{$p.JUMLAH_ROLE}{else}-{/if}</td>
        <td class="center">
            <a href="account-search.php?mode=edit&id_pengguna={$p.ID_PENGGUNA}&q={$smarty.get.q}">Edit</a>
        </td>
        
    </tr>
    {/foreach}
</table>
{/if}