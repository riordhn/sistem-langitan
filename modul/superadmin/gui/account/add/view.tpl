<div class="center_title_bar">Tambah Pengguna</div>

<form action="account-add.php" method="post" style="display: none">
<input type="hidden" name="mode" value="add" />
<table>
    <tr>
        <th colspan="2">Informasi Pengguna</th>
    </tr>
    <tr>
        <td>Jenis Pengguna</td>
        <td>
            <label><input type="radio" name="id_jenis_pengguna" value="1"/> Pegawai</label>
            <label><input type="radio" name="id_jenis_pengguna" value="2" /> Dosen</label>
            <label for="id_jenis_pengguna" class="error" style="display: none">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>NIP / NIK</td>
        <td><input type="text" name="username" ></td>
    </tr>
    <tr>
        <td>Password</td>
        <td><input type="text" name="password" ></td>
    </tr>
    <tr>
        <td>Nama</td>
        <td><input type="text" name="nm_pengguna" ></td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>
            <label><input type="radio" name="jenis_kelamin" value="1" /> Laki-Laki</label>
            <label><input type="radio" name="jenis_kelamin" value="2" /> Perempuan</label>
            <label for="jenis_kelamin" class="error" style="display: none">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>Tanggal Lahir</td>
        <td><input type="text" name="tgl_lahir_pengguna"></td>
    </tr>
    <tr>
        <td>Gelar Depan</td>
        <td><input type="text" name="gelar_depan" /></td>
    </tr>
    <tr>
        <td>Gelar Belakang</td>
        <td><input type="text" name="gelar_belakang" /></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><input type="email" name="email_pengguna" /></td>
    </tr>
    <tr>
        <th colspan="2">Dosen</th>
    </tr>
    <tr>
        <td>NIDN</td>
        <td><input type="text" name="nidn_dosen" /></td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>
            <select name="id_program_studi">
                <option value="">--</option>
            {foreach $program_studi_set as $ps}<option value="{$ps.ID_PROGRAM_STUDI}">{$ps.NM_PROGRAM_STUDI}</option>{/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Golongan</td>
        <td>
            <select name="id_golongan_dosen">
                <option value="">--</option>
            {foreach $golongan_set as $g}<option value="{$g.ID_GOLONGAN}">{$g.NM_GOLONGAN}</option>{/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Jabatan</td>
        <td>
            <select name="id_jabatan_dosen">
                <option value="">--</option>
            {foreach $jabatan_pegawai_set as $jp}<option value="{$jp.ID_JABATAN_PEGAWAI}">{$jp.NM_JABATAN_PEGAWAI}</option>{/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Alamat Rumah</td>
        <td><input type="text" name="alamat_rumah_dosen" /></td>
    </tr>
    <tr>
        <td>Telp</td>
        <td><input type="text" name="tlp_dosen" /></td>
    </tr>
    <tr>
        <td>Mobile</td>
        <td><input type="text" name="mobile_dosen" /></td>
    </tr>
    <tr>
        <th colspan="2">Pegawai</th>
    </tr>
    <tr>
        <td>Golongan</td>
        <td>
            <select name="id_golongan_pegawai">
                <option value="">--</option>
            {foreach $golongan_set as $g}<option value="{$g.ID_GOLONGAN}">{$g.NM_GOLONGAN}</option>{/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Jabatan</td>
        <td>
            <select name="id_jabatan_pegawai">
                <option value="">--</option>
            {foreach $jabatan_pegawai_set as $jp}<option value="{$jp.ID_JABATAN_PEGAWAI}">{$jp.NM_JABATAN_PEGAWAI}</option>{/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Unit Kerja</td>
        <td>
            <select name="id_unit_kerja">
                <option value="">--</option>
            {foreach $unit_kerja_set as $uk}<option value="{$uk.ID_UNIT_KERJA}">{$uk.NM_UNIT_KERJA}</option>{/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Alamat Rumah</td>
        <td><input type="text" name="alamat_pegawai" /></td>
    </tr>
    <tr>
        <td>Telp</td>
        <td><input type="text" name="telp_pegawai" /></td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Tambahkan" />
        </td>
    </tr>
</table>
</form>

<script type="text/javascript">
$(document).ready(function() {

    

    $('form').validate({
        rules: {
            id_jenis_pengguna: { required: true },
            username: { required: true },
            password: { required: true },
            nm_pengguna: { required: true },
            jenis_kelamin: { required: true }
        }
    });
    
    $('form').show();
    
});
</script>