<?php
	include 'config.php';
	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}

	$id_pt = $id_pt_user;

	if(isset($_POST['act'])){ // POST METHOD

		$action = $_POST['act'];

		if($action=='simpan'){
			
			$id_dosen = $_POST['id_dosen'];
			$db->Query("SELECT ka.id_semester, ka.id_kkn_angkatan 
												FROM kkn_angkatan ka 
												JOIN semester s ON s.id_semester = ka.id_semester 
												WHERE ka.status_aktif = '1' AND s.id_perguruan_tinggi = '{$id_pt}'");
			$kkn_angkatan = $db->FetchAssoc();
			
			$id_semester = $kkn_angkatan['ID_SEMESTER'];
			$id_kkn_angkatan = $kkn_angkatan['ID_KKN_ANGKATAN'];

			if(isset($_POST['id_kelompok'])){
				$id_kelompok = $_POST['id_kelompok'];

				$db->Query("INSERT INTO kkn_dpl (id_kkn_angkatan, id_dosen, id_kkn_kelompok, id_semester) values ('$id_kkn_angkatan', '$id_dosen', '$id_kelompok', '$id_semester')");
			}
		}

		else if($action=='delete'){
			$id_dosen = $_POST['id_dosen'];
			$id_dpl = $_POST['id_dpl'];
			$db->Query("DELETE FROM kkn_dpl WHERE id_kkn_dpl = '$id_dpl'");
		}

	}else if(isset($_GET['act'])){ // GET METHOD
		$action = $_GET['act'];

		if($action=='status'){ // IF action == status / get status dosen korbing

			
			$id_dosen = $_GET['id_dosen'];

			$db->Query("select dpl.id_kkn_dpl, kel.nama_kelompok from kkn_dpl dpl
			left join kkn_kelompok kel on kel.id_kkn_kelompok = dpl.id_kkn_kelompok
			where dpl.id_dosen = '$id_dosen'
			");
			
			$a = 0;
			$jml_kelompok = 0;
			$kelompok = array();
			$id_dpl = array();

			while ($row = $db->FetchRow()){
				$id_dpl[$a] = $row[0];
				$kelompok[$a] = $row[1];
				$jml_kelompok++;
				$a++;
			}
			
			$smarty->assign('jml_kelompok', $jml_kelompok);
			$smarty->assign('id_dosen', $id_dosen);
			$smarty->assign('id_dpl', $id_dpl);
			$smarty->assign('kelompok', $kelompok);
			$smarty->display('get-status-dpl.tpl');
			

		}

		else if($action=='prov'){ // IF action == provinsi
			$sec = 0;
			if(isset($_GET['id_dosen'])){
				$id_dosen = $_GET['id_dosen'];
				$sec++;
			}
			if(isset($_GET['i'])){
				$i = $_GET['i'];
				$sec++;
			}

			if($sec==2){

				$id_provinsi = array();
				$nm_provinsi = array();

				$db->Query("SELECT id_provinsi, nm_provinsi FROM provinsi WHERE id_negara = '114' ORDER BY nm_provinsi");
				$jml_provinsi = 0;
				$a = 0;
				while ($row = $db->FetchRow()){ 
					$id_provinsi[$a] = $row[0];
					$nm_provinsi[$a] = $row[1];
					$a++;
					$jml_provinsi++;
				}

				$smarty->assign('jml_provinsi', $jml_provinsi);
				$smarty->assign('id_provinsi', $id_provinsi);
				$smarty->assign('nm_provinsi', $nm_provinsi);

				$smarty->assign('i', $i);
				$smarty->assign('id_dosen', $id_dosen);
				$smarty->display('get-provinsi-dpl.tpl');
			}
		}

		else if($action=='kab'){ // IF action == kab/kabupaten
			$sec = 0;
			if(isset($_GET['id_dosen'])){
				$id_dosen = $_GET['id_dosen'];
				$sec++;
			}
			if(isset($_GET['i'])){
				$i = $_GET['i'];
				$sec++;
			}
			if(isset($_GET['id_prov'])){
				$id_prov = $_GET['id_prov'];
				$sec++;
			}


			if($sec==3){

				$id_kabupaten = array();
				$nm_kabupaten = array();

				$db->Query("select id_kota, kota from (
							SELECT id_kota, (tipe_dati2||' '||nm_kota) as kota FROM kota WHERE id_provinsi = '$id_prov')
						    order by kota");
				$jml_kabupaten = 0;
				$a = 0;
				while ($row = $db->FetchRow()){ 
					$id_kabupaten[$a] = $row[0];
					$nm_kabupaten[$a] = $row[1];
					$a++;
					$jml_kabupaten++;
				}

				$smarty->assign('jml_kabupaten', $jml_kabupaten);
				$smarty->assign('id_kabupaten', $id_kabupaten);
				$smarty->assign('nm_kabupaten', $nm_kabupaten);

				$smarty->assign('i', $i);
				$smarty->assign('id_dosen', $id_dosen);
				$smarty->display('get-kabupaten-dpl.tpl');
			}
		}

		else if($action=='kec'){ // IF action == kecamatan
			$sec = 0;
			if(isset($_GET['id_dosen'])){
				$id_dosen = $_GET['id_dosen'];
				$sec++;
			}
			if(isset($_GET['i'])){
				$i = $_GET['i'];
				$sec++;
			}
			if(isset($_GET['id_kab'])){
				$id_kab = $_GET['id_kab'];
				$sec++;
			}


			if($sec==3){

				$id_kecamatan = array();
				$nm_kecamatan = array();

				$db->Query("SELECT id_kecamatan, nm_kecamatan FROM kecamatan WHERE id_kota = '$id_kab' ORDER BY nm_kecamatan");
				$jml_kabupaten = 0;
				$a = 0;
				while ($row = $db->FetchRow()){ 
					$id_kecamatan[$a] = $row[0];
					$nm_kecamatan[$a] = $row[1];
					$a++;
					$jml_kecamatan++;
				}

				$smarty->assign('jml_kecamatan', $jml_kecamatan);
				$smarty->assign('id_kecamatan', $id_kecamatan);
				$smarty->assign('nm_kecamatan', $nm_kecamatan);

				$smarty->assign('i', $i);
				$smarty->assign('id_dosen', $id_dosen);
				$smarty->display('get-kecamatan-dpl.tpl');
			}
		}

		else if($action=='kel'){ // IF action == kelompok
			$sec = 0;
			if(isset($_GET['id_dosen'])){
				$id_dosen = $_GET['id_dosen'];
				$sec++;
			}
			if(isset($_GET['i'])){
				$i = $_GET['i'];
				$sec++;
			}
			if(isset($_GET['id_kec'])){
				$id_kec = $_GET['id_kec'];
				$sec++;
			}


			if($sec==3){

				$id_kelompok = array();
				$nm_kelompok = array();

				$db->Query("select 
								kec.id_kecamatan, 
								kec.nm_kecamatan, 
								lur.id_kelurahan, 
								lur.nm_kelurahan, 
								kel.id_kkn_kelompok,						
								kel.nama_kelompok,
								kel.jenis_kkn
							from kecamatan kec
							left join kelurahan lur on lur.id_kecamatan = kec.id_kecamatan
							left join kkn_kelompok kel on kel.id_kelurahan = lur.id_kelurahan
							where kec.id_kecamatan = '$id_kec'
							order by kel.nama_kelompok
				");

				$jml_kelompok = 0;
				$a = 0;
				while ($row = $db->FetchRow()){ 
					$id_kelompok[$a] = $row[4];
					$nm_kelompok[$a] = $row[5];
					$jenis_kkn[$a] = $row[6];
					if($jenis_kkn[$a] == '1'){
						$jenis_kkn[$a] = 'REGULER';
					}else if($jenis_kkn[$a] == '2'){
						$jenis_kkn[$a] = 'TEMATIK';
					}
					$a++;
					$jml_kelompok++;
				}

				$smarty->assign('jml_kelompok', $jml_kelompok);
				$smarty->assign('id_kelompok', $id_kelompok);
				$smarty->assign('nm_kelompok', $nm_kelompok);
				$smarty->assign('jenis_kkn', $jenis_kkn);

				$smarty->assign('i', $i);
				$smarty->assign('id_dosen', $id_dosen);
				$smarty->display('get-kelompok-dpl.tpl');
			}
		}
	}
?>