<?php

$id_pt = $id_pt_user;

class kegiatan {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    function LoadKKNAngkatan() {
        return $this->db->QueryToArray("
            SELECT KA.*,S.TAHUN_AJARAN,S.NM_SEMESTER
            FROM KKN_ANGKATAN KA
            JOIN SEMESTER S ON S.ID_SEMESTER=KA.ID_SEMESTER
            WHERE S.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ORDER BY TAHUN_AJARAN, NM_SEMESTER DESC
            ");
    }

    function LoadKegiatanKKN($angkatan) {
        return $this->db->QueryToArray("
            SELECT * FROM KKN_KEGIATAN
            WHERE ID_KKN_ANGKATAN='{$angkatan}'
            ORDER BY TGL_POST DESC");
    }

    function GetKegiatanKKN($id) {
        $this->db->Query("SELECT * FROM KKN_KEGIATAN WHERE ID_KKN_KEGIATAN='{$id}'");
        return $this->db->FetchAssoc();
    }

    function TambahKegiatanKKN($post) {
        $query = "
            INSERT INTO AUCC.KKN_KEGIATAN
                (TIPE,ISI,TGL_AWAL,TGL_AKHIR,ID_KKN_ANGKATAN)
            VALUES
                ('{$post['tipe']}','{$post['isi']}','{$post['tgl_awal']}','{$post['tgl_akhir']}','{$post['angkatan']}')";
        $this->db->Query($query);
    }

    function UpdateKegiatanKKN($post) {
        $query = "
            UPDATE KKN_KEGIATAN
                SET 
                    TIPE='{$post['tipe']}',
                    ISI='{$post['isi']}',
                    TGL_AWAL='{$post['tgl_awal']}',
                    TGL_AKHIR='{$post['tgl_akhir']}'
            WHERE ID_KKN_KEGIATAN='{$post['id']}'
            ";
        $this->db->Query($query);
    }

    function DeleteKegiatanKKN($post) {
        $this->db->Query("DELETE FROM KKN_KEGIATAN WHERE ID_KKN_KEGIATAN='{$post['id']}'");
    }

}

?>
