<?php

$id_pt = $id_pt_user;

class nilai {

    public $db;
    public $id_pengguna;

    function __construct($db, $id_pengguna) {
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
    }

    function load_angkatan_kkn() {
        return $this->db->QueryToArray("
            SELECT KA.*,S.NM_SEMESTER,S.TAHUN_AJARAN 
            FROM KKN_ANGKATAN KA
            JOIN SEMESTER S ON S.ID_SEMESTER=KA.ID_SEMESTER
            WHERE S.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ORDER BY THN_AKADEMIK_SEMESTER DESC,NM_SEMESTER
            ");
    }

    function load_kelompok_kkn($id_angkatan) {
        return $this->db->QueryToArray("
            SELECT KK.*, P.NM_PENGGUNA,P.USERNAME,KEL.NM_KELURAHAN,KEC.NM_KECAMATAN,KOT.NM_KOTA,
            (SELECT COUNT(DISTINCT(ID_MHS)) FROM AUCC.KKN_KELOMPOK_MHS WHERE ID_KKN_KELOMPOK=KK.ID_KKN_KELOMPOK) TERISI
            FROM AUCC.KKN_KELOMPOK KK
            JOIN AUCC.KELURAHAN KEL ON KK.ID_KELURAHAN=KEL.ID_KELURAHAN
            JOIN AUCC.KECAMATAN KEC ON KEL.ID_KECAMATAN=KEC.ID_KECAMATAN
            JOIN AUCC.KOTA KOT ON KOT.ID_KOTA=KEC.ID_KOTA
            LEFT JOIN AUCC.KKN_DPL KD ON KD.ID_KKN_KELOMPOK = KK.ID_KKN_KELOMPOK
            LEFT JOIN AUCC.DOSEN D ON D.ID_DOSEN=KD.ID_DOSEN
            LEFT JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
            WHERE KK.ID_KKN_ANGKATAN='{$id_angkatan}'
            ORDER BY KK.JENIS_KKN,KOT.NM_KOTA,KEC.NM_KECAMATAN,KK.NAMA_KELOMPOK
            ");
    }

    function load_mahasiswa_kelompok($id_kkn) {
        return $this->db->QueryToArray("
            SELECT KKM.*,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS,JAL.NM_JALUR,
            NVL(PMKG.ID_PENGAMBILAN_MK,KKM.ID_PENGAMBILAN_MK) ID_PENGAMBILAN_MK,NVL(PMKG.NILAI_HURUF,PMK.NILAI_HURUF) NILAI_HURUF,NVL(PMKG.NILAI_ANGKA,PMK.NILAI_ANGKA) NILAI_ANGKA
            FROM AUCC.KKN_KELOMPOK_MHS KKM
            JOIN AUCC.KKN_KELOMPOK KK ON KK.ID_KKN_KELOMPOK=KKM.ID_KKN_KELOMPOK
            JOIN AUCC.MAHASISWA M ON M.ID_MHS=KKM.ID_MHS
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN AUCC.FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            JOIN AUCC.JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND JM.IS_JALUR_AKTIF=1
            JOIN AUCC.JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
            LEFT JOIN AUCC.PENGAMBILAN_MK PMK ON PMK.ID_PENGAMBILAN_MK=KKM.ID_PENGAMBILAN_MK
            LEFT JOIN (
              SELECT PMK.ID_SEMESTER,PMK.ID_MHS,PMK.ID_PENGAMBILAN_MK,PMK.NILAI_HURUF,PMK.NILAI_ANGKA
              FROM AUCC.PENGAMBILAN_MK PMK
              LEFT JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK=PMK.ID_KELAS_MK
              LEFT JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
              WHERE KUMK.STATUS_MKTA = 2 
              AND PMK.STATUS_APV_PENGAMBILAN_MK = 1                
            ) PMKG ON PMKG.ID_MHS=KKM.ID_MHS AND PMKG.ID_SEMESTER=KK.ID_SEMESTER
            WHERE KKM.ID_KKN_KELOMPOK='{$id_kkn}' AND KKM.TERJUN=1
            ");
    }

    function load_rekap_penilaian_kkn($angk, $kel) {
        if ($kel != '') {
            $data_mahasiswa = $this->load_mahasiswa_kelompok($kel);
        } else {
            $data_mahasiswa = array();
            foreach ($this->load_kelompok_kkn($angk) as $k) {
                array_push($data_mahasiswa, array_merge(array('KELOMPOK' => $k, 'DATA_MHS' => $this->load_mahasiswa_kelompok($k['ID_KKN_KELOMPOK']))));
            }
        }
        return $data_mahasiswa;
    }
    
    // Penilaian
    
    function get_nilai_huruf_akhir($nilai) {
        $nilai_huruf_akhir = '';
        $query = "SELECT SN.NM_STANDAR_NILAI,PN.NILAI_MAX_PERATURAN_NILAI AS NILAI_MAX,PN.NILAI_MIN_PERATURAN_NILAI AS NILAI_MIN 
			FROM PERATURAN_NILAI PN
			JOIN STANDAR_NILAI SN ON PN.ID_STANDAR_NILAI = SN.ID_STANDAR_NILAI
            WHERE PN.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'";

        // Cari Standar Nilai Untuk Nilai Angka
        foreach ($this->db->QueryToArray($query) as $data) {
            if ($nilai <= $data['NILAI_MAX'] && $nilai >= $data['NILAI_MIN']) {
                $nilai_huruf_akhir = $data['NM_STANDAR_NILAI'];
            }
        }
        return $nilai_huruf_akhir;
    }

}

?>
