<?php
/**
 * Class Proposal Penelitian
 *
 * @author Fathoni
 */
class ProposalPenelitian
{
	// variabel database
    private $db;

	// konstanta
    const UPLOAD_FOR_ADD = 2;
    const UPLOAD_FOR_EDIT = 3;
    const PAIR_FOR_ADD = 2;
    const PAIR_FOR_EDIT = 3;

    function __construct(MyOracle $db)
    {
		$this->db = $db;
    }
    
    function CariPeneliti($q)
    {
        $q = strtoupper($q);
        
        return $this->db->QueryToArray("
            select d.id_dosen, p.username, p.nm_pengguna, f.nm_fakultas from pengguna p
            join dosen d on d.id_pengguna = p.id_pengguna
            join program_studi ps on ps.id_program_studi = d.id_program_studi
            join fakultas f on f.id_fakultas = ps.id_fakultas
            where (upper(p.nm_pengguna) like '%{$q}%' or p.username like '%{$q}%')
            order by f.id_fakultas");
    }
    
    function GetDosen($id_dosen)
    {
        $this->db->Query("
            select d.id_dosen, p.username, p.nm_pengguna, f.nm_fakultas from pengguna p
            join dosen d on d.id_pengguna = p.id_pengguna
            join program_studi ps on ps.id_program_studi = d.id_program_studi
            join fakultas f on f.id_fakultas = ps.id_fakultas
            where d.id_dosen = {$id_dosen}");
        return $this->db->FetchAssoc();
    }


    function GetListJenisPenelitian()
    {
        return $this->db->QueryToArray("select * from penelitian_jenis order by nama_jenis");
    }
    
    function GetListSKIM($id_penelitian_jenis = '')
    {
		if ($id_penelitian_jenis != '')
			return $this->db->QueryToArray("select * from penelitian_skim where id_penelitian_jenis = {$id_penelitian_jenis} order by kode_skim");
		else
			return $this->db->QueryToArray("select * from penelitian_skim order by kode_skim");
    }
    
    function GetListBidangPenelitian($id_penelitian_jenis)
    {
        return $this->db->QueryToArray("select * from penelitian_bidang where id_penelitian_jenis = {$id_penelitian_jenis} order by nama_bidang, nama_tema");
    }
    
    function GetKetuaLPPM()
    {
        $this->db->Query("
            select * from lembaga
            join pengguna on pengguna.id_pengguna = lembaga.id_ketua
            where singkatan_lembaga = 'LPPM'");
        return $this->db->FetchAssoc();
    }
    
    function GetDekan($id_dosen)
    {
        $this->db->Query("
            select p.id_pengguna as id_dekan, p.nm_pengguna as nm_dekan, p.username as nip_dekan
            from fakultas f
            join pengguna p on p.id_pengguna = f.id_dekan
            join program_studi ps on ps.id_fakultas = f.id_fakultas
            where ps.id_program_studi in (select id_program_studi from dosen where id_dosen = {$id_dosen})");
        return $this->db->FetchAssoc();
    }
    
    function GetListGolongan()
    {
        return $this->db->QueryToArray("select id_golongan, nm_golongan||' - '||nm_pangkat as nm_golongan from golongan order by nm_golongan");
    }
    
    function UploadFileProposal(&$post, &$file)
    {
		// Lokasi tujuan
        $destination = "../../files/upload/dosen/proposal/{$post['token']}.pdf";
		
		// Jika sudah ada di remove dulu
		if (file_exists($destination))
			unlink($destination);
        
		// Pindah hasil upload-an
        if (move_uploaded_file($file['tmp_name'], $destination))
        {
			// delete record sebelumnya jika melakukan reupload
			$this->db->Query("delete from penelitian_file where md5 = '{$post['token']}'");
			
			// update ke db
			$this->db->Query("insert into penelitian_file (md5, nm_asli, waktu_upload) values ('{$post['token']}','{$file['name']}', ".time().")");
			return true;
        }

        return false;
    }
	
	function DeleteFileProposal(&$post)
	{
		return $this->db->Query("update penelitian set md5_file = null, nama_file = null where id_penelitian = {$post['id_penelitian']}");
	}
    
    function Add(&$post)
    {
		$this->db->BeginTransaction();
		
		// Insert tabel penelitian
        $this->db->Parse("
            insert into penelitian (
                id_penelitian, id_peneliti, judul, id_penelitian_jenis, id_penelitian_skim, id_penelitian_bidang, penelitian_bidang_lain,
				lokasi, tahun, jangka_waktu, jangka_waktu_ke,
                kota, tgl_input,
                id_dekan, id_ketua_lppm,
                is_dari_lppm, is_proposal
            ) values (
                :id_penelitian, :id_peneliti, :judul, :id_penelitian_jenis, :id_penelitian_skim, :id_penelitian_bidang, :penelitian_bidang_lain,
				:lokasi, :tahun, :jangka_waktu, :jangka_waktu_ke,
                :kota, to_date(:tgl_input,'YYYYMMDD'),
                :id_dekan, :id_ketua_lppm,
                :is_dari_lppm, :is_proposal
            )");
        
        $this->db->BindByName(':id_penelitian',			$post['id_penelitian']);
        $this->db->BindByName(':id_peneliti',			$post['id_dosen']);
        $this->db->BindByName(':judul',					$post['judul']);
        $this->db->BindByName(':id_penelitian_jenis',	$post['id_penelitian_jenis']);
        $this->db->BindByName(':id_penelitian_skim',	$post['id_penelitian_skim']);
        $this->db->BindByName(':id_penelitian_bidang',	$post['id_penelitian_bidang']);
        $this->db->BindByName(':penelitian_bidang_lain',$post['penelitian_bidang_lain']);
        $this->db->BindByName(':lokasi',                $post['lokasi']);
		$this->db->BindByName(':tahun',					$post['tahun']);
        $this->db->BindByName(':jangka_waktu',			$post['jangka_waktu']);
        $this->db->BindByName(':jangka_waktu_ke',		$post['jangka_waktu_ke']);      
        $this->db->BindByName(':kota',					$post['kota']);
        $this->db->BindByName(':tgl_input',				$tgl = $post['tgl_input_Year'] . $post['tgl_input_Month'] . $post['tgl_input_Day']);
        $this->db->BindByName(':id_dekan',              $post['id_dekan']);
        $this->db->BindByName(':id_ketua_lppm',			$post['id_ketua_lppm']);
		$this->db->BindByName(':is_dari_lppm',			$post['is_dari_lppm']);
		$this->db->BindByName(':is_proposal',			$post['is_proposal']);
        $result = $this->db->Execute();
		
		if (!$result) { $this->db->Rollback(); echo __LINE__ .": Gagal insert penelitian."; exit(); }
		
		
		// Insert data penelitian mitra
		$this->db->Parse("
			insert into penelitian_mitra (
				id_penelitian,
				nama_mitra, nip_mitra, golongan_mitra, jabatan_fungsional_mitra, jabatan_struktural_mitra, pt_mitra, 
				fakultas_mitra, prodi_mitra, alamat_kantor_mitra, telp_kantor_mitra, fax_kantor_mitra, alamat_mitra, 
				telp_mitra, email_mitra)
			values (
				:id_penelitian,
				:nama_mitra, :nip_mitra, :golongan_mitra, :jabatan_fungsional_mitra, :jabatan_struktural_mitra, :pt_mitra, 
				:fakultas_mitra, :prodi_mitra, :alamat_kantor_mitra, :telp_kantor_mitra, :fax_kantor_mitra, :alamat_mitra, 
				:telp_mitra, :email_mitra)");
		$this->db->BindByName(':id_penelitian',				$post['id_penelitian']);
		$this->db->BindByName(':nama_mitra',				$post['nama_mitra']);
		$this->db->BindByName(':nip_mitra',					$post['nip_mitra']);
		$this->db->BindByName(':golongan_mitra',			$post['golongan_mitra']);
		$this->db->BindByName(':jabatan_fungsional_mitra',	$post['jabatan_fungsional_mitra']);
		$this->db->BindByName(':jabatan_struktural_mitra',	$post['jabatan_struktural_mitra']);
		$this->db->BindByName(':pt_mitra',					$post['pt_mitra']);
		$this->db->BindByName(':fakultas_mitra',			$post['fakultas_mitra']);
		$this->db->BindByName(':prodi_mitra',				$post['prodi_mitra']);
		$this->db->BindByName(':alamat_kantor_mitra',		$post['alamat_kantor_mitra']);
		$this->db->BindByName(':telp_kantor_mitra',			$post['telp_kantor_mitra']);
		$this->db->BindByName(':fax_kantor_mitra',			$post['fax_kantor_mitra']);
		$this->db->BindByName(':alamat_mitra',				$post['alamat_mitra']);
		$this->db->BindByName(':telp_mitra',				$post['telp_mitra']);
		$this->db->BindByName(':email_mitra',				$post['email_mitra']);
		$result = $this->db->Execute();
		
		if (!$result) { $this->db->Rollback(); echo __LINE__ .": Gagal insert penelitian 2."; exit(); }
		
		
		// Insert anggota penelitian
		for ($i = 0; $i < count($post['nip_anggota']); $i++)
		{
			$this->db->Parse("
				insert into penelitian_anggota (id_penelitian, nip_anggota, nama_anggota, gelar_depan, gelar_belakang, urutan)
				values (:id_penelitian, :nip_anggota, :nama_anggota, :gelar_depan, :gelar_belakang, :urutan)");
			$this->db->BindByName(':id_penelitian',		$post['id_penelitian']);
			$this->db->BindByName(':nip_anggota',		$post['nip_anggota'][$i]);
			$this->db->BindByName(':nama_anggota',		$post['nama_anggota'][$i]);
			$this->db->BindByName(':gelar_depan',		$post['gelar_depan_anggota'][$i]);
			$this->db->BindByName(':gelar_belakang',	$post['gelar_belakang_anggota'][$i]);
			$this->db->BindByName(':urutan',			$urutan = ($i + 1));
			$result = $this->db->Execute();
			
			if (!$result) { $this->db->Rollback(); echo __LINE__ .": Gagal insert anggota penelitian. {$i}"; exit(); }
		}
		
		
		// Insert keuangan 
		for ($i = 0; $i < count($post['besar_biaya_dikti']); $i++)
		{
			$this->db->Parse("
				insert into penelitian_biaya (id_penelitian, tahun_ke, besar_biaya_dikti, besar_biaya_lain, besar_biaya_pt)
				values (:id_penelitian, :tahun_ke, :besar_biaya_dikti, :besar_biaya_lain, :besar_biaya_pt)");
			$this->db->BindByName(':id_penelitian',		$post['id_penelitian']);
			$this->db->BindByName(':tahun_ke',			$tahun = ($i + 1));
			$this->db->BindByName(':besar_biaya_dikti',	$post['besar_biaya_dikti'][$i]);
			$this->db->BindByName(':besar_biaya_lain',	$post['besar_biaya_lain'][$i]);
			$this->db->BindByName(':besar_biaya_pt',	$post['besar_biaya_pt'][$i]);
			$result = $this->db->Execute();
			
			if (!$result) { $this->db->Rollback(); echo __LINE__ .": Gagal insert biaya penelitian. {$i}"; exit(); }
		}
		
		// Cek apakah ada file yg terupload saat ditambah
		$this->db->Query("select nm_asli from penelitian_file where md5 = '{$post['token']}'");
		$row = $this->db->FetchAssoc();
		
		// Jika file ada
		if ($row)
		{
			// Update file di tabel penelitian
			$result = $this->db->Query("
				update penelitian set md5_file = '{$post['token']}', nama_file = '{$row['NM_ASLI']}'
				where id_penelitian = {$post['id_penelitian']}");
				
			if (!$result) { $this->db->Rollback(); echo __LINE__ .": Gagal update file."; exit(); }
		}
		
		return $this->db->Commit();
		
		// Update data mitra

    }
    
    function Edit($post)
    {
		//  Start transaksi
		$this->db->BeginTransaction();
		
		// Update tabel penelitian
        $this->db->Parse("
			update penelitian set
				judul = :judul,
				id_penelitian_jenis = :id_penelitian_jenis,
				id_penelitian_skim = :id_penelitian_skim,
				id_penelitian_bidang = :id_penelitian_bidang,
				penelitian_bidang_lain = :penelitian_bidang_lain,
				nama_institusi = :nama_institusi,
				lokasi = :lokasi,
				tahun = :tahun,			
				jangka_waktu = :jangka_waktu,
				jangka_waktu_ke = :jangka_waktu_ke,
				kota = :kota,
				tgl_input = to_date(:tgl_input,'YYYYMMDD')
			where id_penelitian = :id_penelitian");
		$this->db->BindByName(':judul',					$post['judul']);
		$this->db->BindByName(':id_penelitian_jenis',	$post['id_penelitian_jenis']);
		$this->db->BindByName(':id_penelitian_skim',	$post['id_penelitian_skim']);
		$this->db->BindByName(':id_penelitian_bidang',	$post['id_penelitian_bidang']);
		$this->db->BindByName(':penelitian_bidang_lain',$post['penelitian_bidang_lain']);
		$this->db->BindByName(':nama_institusi',		$post['nama_institusi']);
		$this->db->BindByName(':lokasi',				$post['lokasi']);
		$this->db->BindByName(':tahun',					$post['tahun']);
		$this->db->BindByName(':jangka_waktu',			$post['jangka_waktu']);
		$this->db->BindByName(':jangka_waktu_ke',		$post['jangka_waktu_ke']);
		$this->db->BindByName(':kota',					$post['kota']);
		$this->db->BindByName(':tgl_input',				$tgl_input = $post['tgl_input_Year'] . $post['tgl_input_Month'] . str_pad($post['tgl_input_Day'], 2, "0", STR_PAD_LEFT));
		$this->db->BindByName(':id_penelitian',         $post['id_penelitian']);
		$result = $this->db->Execute();
		
		// update tabel mitra
		$this->db->Parse("
			update penelitian_mitra set
				nama_mitra = :nama_mitra, nip_mitra = :nip_mitra, golongan_mitra = :golongan_mitra,
				jabatan_fungsional_mitra = :jabatan_fungsional_mitra, jabatan_struktural_mitra = :jabatan_struktural_mitra,
				pt_mitra = :pt_mitra, fakultas_mitra = :fakultas_mitra, prodi_mitra = :prodi_mitra,
				alamat_kantor_mitra = :alamat_kantor_mitra, telp_kantor_mitra = :telp_kantor_mitra, fax_kantor_mitra = :fax_kantor_mitra,
				alamat_mitra = :alamat_mitra, telp_mitra = :telp_mitra, email_mitra = :email_mitra
			where id_penelitian = :id_penelitian");
		$this->db->BindByName(':nama_mitra',				$post['nama_mitra']);
		$this->db->BindByName(':nip_mitra',					$post['nip_mitra']);
		$this->db->BindByName(':golongan_mitra',			$post['golongan_mitra']);
		$this->db->BindByName(':jabatan_fungsional_mitra',	$post['jabatan_fungsional_mitra']);
		$this->db->BindByName(':jabatan_struktural_mitra',	$post['jabatan_struktural_mitra']);
		$this->db->BindByName(':pt_mitra',					$post['pt_mitra']);
		$this->db->BindByName(':fakultas_mitra',			$post['fakultas_mitra']);
		$this->db->BindByName(':prodi_mitra',				$post['prodi_mitra']);
		$this->db->BindByName(':alamat_kantor_mitra',		$post['alamat_kantor_mitra']);
		$this->db->BindByName(':telp_kantor_mitra',			$post['telp_kantor_mitra']);
		$this->db->BindByName(':fax_kantor_mitra',			$post['fax_kantor_mitra']);
		$this->db->BindByName(':alamat_mitra',				$post['alamat_mitra']);
		$this->db->BindByName(':telp_mitra',				$post['telp_mitra']);
		$this->db->BindByName(':email_mitra',				$post['email_mitra']);
		$this->db->BindByName(':id_penelitian',				$post['id_penelitian']);
		$result = $this->db->Execute();
		
		
		// update tabel anggota
		for ($i = 0; $i < count($post['id_anggota']); $i++)
		{
			$this->db->Parse("
				update penelitian_anggota set
					nip_anggota = :nip_anggota, nama_anggota = :nama_anggota, gelar_depan = :gelar_depan, gelar_belakang = :gelar_belakang
				where id_anggota = :id_anggota");
			$this->db->BindByName(':nip_anggota',	$post['nip_anggota'][$i]);
			$this->db->BindByName(':nama_anggota',	$post['nama_anggota'][$i]);
			$this->db->BindByName(':gelar_depan',	$post['gelar_depan_anggota'][$i]);
			$this->db->BindByName(':gelar_belakang',$post['gelar_belakang_anggota'][$i]);
			$this->db->BindByName(':id_anggota',	$post['id_anggota'][$i]);
			$this->db->Execute();
		}
		
		// update tabel biaya
		for ($i = 0; $i < count($post['id_biaya']); $i++)
		{
			$this->db->Parse("
				update penelitian_biaya set
					besar_biaya_dikti = :besar_biaya_dikti, besar_biaya_lain = :besar_biaya_lain, besar_biaya_pt = :besar_biaya_pt
				where id_biaya = :id_biaya");
			$this->db->BindByName(':besar_biaya_dikti',	$post['besar_biaya_dikti'][$i]);
			$this->db->BindByName(':besar_biaya_lain',	$post['besar_biaya_lain'][$i]);
			$this->db->BindByName(':besar_biaya_pt',	$post['besar_biaya_pt'][$i]);
			$this->db->BindByName(':id_biaya',			$post['id_biaya'][$i]);
			$this->db->Execute();
		}
	
		// update file if exist
		if (file_exists("/var/www/html/files/upload/dosen/proposal/{$post['token']}.pdf"))
		{
			// ambil nama asli file
			$nama_file = $this->db->QuerySingle("select nm_asli from penelitian_file where md5 = '{$post['token']}'");

			// update kolom penelitian
			$this->db->Query("update penelitian set md5_file = '{$post['token']}', nama_file = '{$nama_file}' where id_penelitian = {$post['id_penelitian']}");
		}
		
		$result = $this->db->Commit();

		return $result;
    }
    
    function GetProposal($id)
    {
		// Mendapatkan data proposal
        $this->db->Query("
			select
				peneliti.*,
				md5_file, nama_file, judul, id_penelitian_jenis, id_penelitian_skim, nama_institusi, id_penelitian_bidang, penelitian_bidang_lain,
				lokasi, tahun, jangka_waktu, jangka_waktu_ke,
				kota, to_char(tgl_input,'YYYY-MM-DD') as tgl_input,
				mitra.*
			from penelitian
			join (
				select
					d.id_dosen as id_peneliti,
					p.nm_pengguna as nama_peneliti,
					p.username as nip_peneliti,
					f.nm_fakultas as asal_peneliti,
					dek.username as nip_dekan,
					dek.nm_pengguna as nama_dekan
				from dosen d
				join pengguna p on p.id_pengguna = d.id_pengguna
				join program_studi ps on ps.id_program_studi = d.id_program_studi
				join fakultas f on f.id_fakultas = ps.id_fakultas
				left join pengguna dek on dek.id_pengguna = f.id_dekan) peneliti on peneliti.id_peneliti = penelitian.id_peneliti
			join penelitian_mitra mitra on mitra.id_penelitian = penelitian.id_penelitian
			where is_proposal = 1 and penelitian.id_penelitian = {$id}");
        $proposal = $this->db->FetchAssoc();
		
		// anggota penelitian
		$proposal['anggota'] = $this->db->QueryToArray("
			select * from penelitian_anggota where id_penelitian = {$proposal['ID_PENELITIAN']} order by urutan");
			
		// biaya penelitian
		$proposal['biaya'] = $this->db->QueryToArray("
			select * from penelitian_biaya where id_penelitian = {$proposal['ID_PENELITIAN']} order by tahun_ke");
		
		return $proposal;
    }
	
	function Delete($post)
	{
		$this->db->BeginTransaction();
		$this->db->Query("delete from penelitian_anggota where id_penelitian = {$post['id_penelitian']}");
		$this->db->Query("delete from penelitian_biaya where id_penelitian = {$post['id_penelitian']}");
		$this->db->Query("delete from penelitian_mitra where id_penelitian = {$post['id_penelitian']}");
		$this->db->Query("delete from penelitian where id_penelitian = {$post['id_penelitian']}");
		return $this->db->Commit();
	}
    
    function GetListProposalForLPPM()
    {
        return $this->db->QueryToArray("
			select p.id_penelitian, p.judul, pengguna.nm_pengguna as nama_peneliti, tahun
			from penelitian p
			join dosen d on d.id_dosen = p.id_peneliti
			join pengguna on pengguna.id_pengguna = d.id_pengguna
			where p.is_proposal = 1
			order by tahun desc, id_penelitian desc");
    }
	
	function GetListProposalForDosen($id_dosen)
	{
		return $this->db->QueryToArray("
			select id_penelitian, judul, nama_jenis, tahun
			from penelitian
			join penelitian_jenis on penelitian_jenis.id_penelitian_jenis = penelitian.id_penelitian_jenis
			where id_peneliti = {$id_dosen}
			order by tahun desc, id_penelitian desc");
	}
    
    /**
     * Mendapatkan ID Penelitian baru
     * @return int
     */
    function GetNewID()
    {
        $this->db->Query("select PENELITIAN_SEQ.nextval from dual");
        $row = $this->db->FetchRow();
        return $row[0];
    }
    
    /**
     * Mendapatkan report proposal per fakultas
     * @return Array
     */
    function GetReportProposalByFakultas()
    {
        return $this->db->QueryToArray("
			select tahun, id_fakultas, count(id_penelitian) jumlah from penelitian p
			join dosen on DOSEN.id_dosen = p.ID_PENELITI
			join program_studi on PROGRAM_STUDI.id_program_studi = dosen.id_program_studi
			where p.is_proposal = 1
			group by tahun, id_fakultas");
    }
	
	function GetDistinctTahun()
	{
		return $this->db->QueryToArray("select distinct tahun from penelitian order by tahun desc");
	}
	
	function GetListFakultas()
	{
		return $this->db->QueryToArray("select id_fakultas, singkatan_fakultas, nm_fakultas from fakultas order by id_fakultas");
	}
	
	function GetReportProposalDataPerSkimFakultas()
	{
		return $this->db->QueryToArray("
			select id_penelitian_skim, id_fakultas, tahun, count(id_penelitian) jumlah from penelitian
			join dosen on dosen.id_dosen = penelitian.id_peneliti
			join program_studi on program_studi.id_program_studi = dosen.id_program_studi
			group by id_penelitian_skim, id_fakultas, tahun");
	}
    
    function GetReportProposalByTahun()
    {
        return $this->db->QueryToArray("
            SELECT tahun, COUNT(id_penelitian) AS jumlah
            FROM penelitian
            WHERE is_proposal = 1
            GROUP BY tahun");
    }
    
    function GetReportProposalBySKIM()
    {
        return $this->db->QueryToArray("
            SELECT ps.id_penelitian_skim, kode_skim, nama_skim, nvl(jumlah,0) as jumlah FROM penelitian_skim ps
            LEFT JOIN (
                SELECT id_penelitian_skim, count(id_penelitian) as jumlah FROM penelitian
                WHERE is_proposal = 1
                group by id_penelitian_skim) p on p.id_penelitian_skim = ps.id_penelitian_skim");
    }
    
    function GetReportProposalByJabatanFungsional()
    {
        return $this->db->QueryToArray("
            SELECT * FROM jabatan_fungsional jf
            RIGHT JOIN (
              SELECT d.id_jabatan_fungsional, count(p.id_penelitian) AS jumlah FROM penelitian p
              JOIN dosen d ON d.id_dosen = p.id_peneliti
              WHERE p.is_proposal = 1
              GROUP BY d.id_jabatan_fungsional) p on p.id_jabatan_fungsional = jf.id_jabatan_fungsional");
    }
	
	function GetPeneliti($id_dosen)
	{
		$this->db->Query("
			select
				d.id_dosen as id_peneliti,
				p.nm_pengguna as nama_peneliti,
				p.username as nip_peneliti,
				f.nm_fakultas as asal_peneliti,
				dek.username as nip_dekan,
				dek.nm_pengguna as nama_dekan
			from dosen d
			join pengguna p on p.id_pengguna = d.id_pengguna
			join program_studi ps on ps.id_program_studi = d.id_program_studi
			join fakultas f on f.id_fakultas = ps.id_fakultas
			left join pengguna dek on dek.id_pengguna = f.id_dekan
			where d.id_dosen = {$id_dosen}");
		return $this->db->FetchAssoc();
	}
}

?>
