<?php

$id_pt = $id_pt_user;

class ploting {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    function GetSemesterAktif() {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'");
        return $this->db->FetchAssoc();
    }
    
    function GetSemesterKKN(){
        $this->db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER IN (SELECT ka.ID_SEMESTER FROM KKN_ANGKATAN ka JOIN SEMESTER s ON s.ID_SEMESTER = ka.ID_SEMESTER WHERE ka.STATUS_AKTIF=1 AND s.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."')");
        return $this->db->FetchAssoc();
    }
    
    function GetAngkatanAktif(){
        $this->db->Query("SELECT * FROM KKN_ANGKATAN WHERE STATUS_AKTIF='1' AND ID_SEMESTER IN (SELECT ID_SEMESTER FROM SEMESTER WHERE ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."' AND STATUS_AKTIF_SEMESTER = 'True')");
        return $this->db->FetchAssoc();
    }

    function LoadEstimasiFakultas($jenis) {
        return $this->db->QueryToArray("
            select nm_fakultas, D.* from AUCC.fakultas F
            LEFT JOIN (
                SELECT F.ID_FAKULTAS, COUNT(KKM.ID_MHS) AS JML_PESERTA
                FROM AUCC.FAKULTAS F 
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_FAKULTAS = F.ID_FAKULTAS
                JOIN AUCC.MAHASISWA M ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
                JOIN AUCC.KKN_KELOMPOK_MHS KKM ON KKM.ID_MHS = M.ID_MHS
                JOIN AUCC.KKN_ANGKATAN KA ON KA.ID_KKN_ANGKATAN=KKM.ID_KKN_ANGKATAN
                WHERE KKM.ID_KKN_KELOMPOK IS NULL AND KKM.JENIS_KKN='{$jenis}' 
                    AND KA.STATUS_AKTIF='1'
                    AND F.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
                group by F.id_fakultas, nm_fakultas
            ) D ON D.ID_FAKULTAS = F.ID_FAKULTAS
            where F.id_perguruan_tinggi = '".getenv('ID_PT')."'
            order by F.id_fakultas
            ");
    }

    function LoadEstimasiGender($jenis) {
        return $this->db->QueryToArray("
            SELECT P.KELAMIN_PENGGUNA, COUNT(KKM.ID_MHS) AS JML_PESERTA
            FROM AUCC.PENGGUNA P 
            JOIN AUCC.MAHASISWA M ON M.ID_PENGGUNA = P.ID_PENGGUNA
            JOIN AUCC.KKN_KELOMPOK_MHS KKM ON KKM.ID_MHS = M.ID_MHS
            JOIN AUCC.KKN_ANGKATAN KA ON KA.ID_KKN_ANGKATAN=KKM.ID_KKN_ANGKATAN
            WHERE KKM.ID_KKN_KELOMPOK IS NULL AND KKM.JENIS_KKN='{$jenis}' 
                AND KA.STATUS_AKTIF='1'
                AND P.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            GROUP BY P.KELAMIN_PENGGUNA
            ");
    }

    function GetTotalKKN($jenis) {
        return $this->db->QuerySingle("
            SELECT COUNT(KKM.ID_MHS)
            FROM AUCC.MAHASISWA M
            JOIN AUCC.KKN_KELOMPOK_MHS KKM ON KKM.ID_MHS = M.ID_MHS
            JOIN AUCC.KKN_ANGKATAN KA ON KA.ID_KKN_ANGKATAN=KKM.ID_KKN_ANGKATAN
            JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=KA.ID_SEMESTER
            WHERE KKM.ID_KKN_KELOMPOK IS NULL AND KKM.JENIS_KKN='{$jenis}' 
                AND KA.STATUS_AKTIF='1'
                AND S.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ");
    }

    function GetQuotaKelompok($jenis) {
        return $this->db->QuerySingle("
            SELECT SUM(QUOTA-TERISI) FROM
            (
                SELECT KKEL.*,KEL.NM_KELURAHAN,
                (SELECT COUNT(DISTINCT(ID_MHS)) FROM AUCC.KKN_KELOMPOK_MHS WHERE ID_KKN_KELOMPOK=KKEL.ID_KKN_KELOMPOK) TERISI
                FROM AUCC.KKN_KELOMPOK KKEL
                JOIN AUCC.KELURAHAN KEL ON KKEL.ID_KELURAHAN=KEL.ID_KELURAHAN
                JOIN AUCC.KKN_ANGKATAN KA ON KA.ID_KKN_ANGKATAN=KKEL.ID_KKN_ANGKATAN
                JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=KA.ID_SEMESTER
                WHERE KA.STATUS_AKTIF='1' 
                    AND KKEL.JENIS_KKN='{$jenis}'
                    AND S.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
                ORDER BY KKEL.JENIS_KKN,KEL.ID_KECAMATAN,KEL.NM_KELURAHAN
            )
            ");
    }

    function GetSudahDapatKelompok($jenis) {
        return $this->db->QuerySingle("
            SELECT COUNT(KKM.ID_MHS)
            FROM AUCC.MAHASISWA M
            JOIN AUCC.KKN_KELOMPOK_MHS KKM ON KKM.ID_MHS = M.ID_MHS
            JOIN AUCC.KKN_ANGKATAN KA ON KA.ID_KKN_ANGKATAN=KKM.ID_KKN_ANGKATAN
            JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=KA.ID_SEMESTER
            WHERE KKM.ID_KKN_KELOMPOK IS NOT NULL AND KKM.JENIS_KKN='{$jenis}' 
                AND KA.STATUS_AKTIF='1'
                AND S.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ");
    }
    
    function GetBelumDapatKelompok($jenis){
        return $this->db->QuerySingle("
            SELECT COUNT(KKM.ID_MHS)
            FROM AUCC.MAHASISWA M
            JOIN AUCC.KKN_KELOMPOK_MHS KKM ON KKM.ID_MHS = M.ID_MHS
            JOIN AUCC.KKN_ANGKATAN KA ON KA.ID_KKN_ANGKATAN=KKM.ID_KKN_ANGKATAN
            JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=KA.ID_SEMESTER
            WHERE KKM.ID_KKN_KELOMPOK IS NULL AND KKM.JENIS_KKN='{$jenis}' 
                AND KA.STATUS_AKTIF='1'
                AND S.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ");
    }

    function GetKelurahanKelompok($id) {
        $this->db->Query("
            SELECT * FROM KKN_KELOMPOK KK
            JOIN KELURAHAN KEL ON KK.ID_KELURAHAN=KEL.ID_KELURAHAN
            WHERE KK.ID_KKN_KELOMPOK='{$id}'
            ");
        return $this->db->FetchAssoc();
    }

    function GetDPLKelompok($kel) {
        $semester = $this->GetSemesterKKN();
        $this->db->Query("
            SELECT P.*,D.* FROM KKN_DPL KD
            JOIN DOSEN D ON D.ID_DOSEN=KD.ID_DOSEN
            JOIN PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
            WHERE KD.ID_KKN_KELOMPOK='{$kel}' AND KD.ID_SEMESTER='{$semester['ID_SEMESTER']}'
            ");
        return $this->db->FetchAssoc();
    }

    function GetKorcamKelompok($kec) {
        $semester = $this->GetSemesterKKN();
        $this->db->Query("
            SELECT P.*,D.* FROM KKN_KORBING KB
            JOIN DOSEN D ON D.ID_DOSEN=KB.ID_DOSEN
            JOIN PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
            WHERE KB.ID_KECAMATAN='{$kec}' AND KB.ID_SEMESTER='{$semester['ID_SEMESTER']}'
            ");
        return $this->db->FetchAssoc();
    }

    function GetKorkabKelompok($kab) {
        $semester = $this->GetSemesterKKN();
        $this->db->Query("
            SELECT P.*,D.* FROM KKN_KORKAB KK
            JOIN DOSEN D ON D.ID_DOSEN=KK.ID_DOSEN
            JOIN PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
            WHERE KK.ID_KABUPATEN='{$kab}' AND KK.ID_SEMESTER='{$semester['ID_SEMESTER']}'
            ");
        return $this->db->FetchAssoc();
    }

    function LoadKknKelompok($jenis) {
        return $this->db->QueryToArray("
            SELECT KKEL.*,KEL.NM_KELURAHAN,
            (SELECT COUNT(DISTINCT(ID_MHS)) FROM AUCC.KKN_KELOMPOK_MHS WHERE ID_KKN_KELOMPOK=KKEL.ID_KKN_KELOMPOK) TERISI
            FROM AUCC.KKN_KELOMPOK KKEL
            JOIN AUCC.KELURAHAN KEL ON KKEL.ID_KELURAHAN=KEL.ID_KELURAHAN
            JOIN AUCC.KKN_ANGKATAN KA ON KA.ID_KKN_ANGKATAN=KKEL.ID_KKN_ANGKATAN
            JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=KA.ID_SEMESTER
            WHERE KA.STATUS_AKTIF='1' 
                AND KKEL.JENIS_KKN='{$jenis}'
                AND S.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ORDER BY KKEL.JENIS_KKN,KEL.ID_KECAMATAN,KEL.NM_KELURAHAN
            ");
    }

    function LoadKKNKelompokMhs($id) {
        return $this->db->QueryToArray("
            SELECT M.ID_MHS,P.NM_PENGGUNA,M.NIM_MHS,M.THN_ANGKATAN_MHS,PS.NM_PROGRAM_STUDI,F.NM_FAKULTAS,P.KELAMIN_PENGGUNA,J.NM_JENJANG
                FROM AUCC.FAKULTAS F 
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_FAKULTAS = F.ID_FAKULTAS
                JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                JOIN AUCC.MAHASISWA M ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
                JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
                JOIN AUCC.KKN_KELOMPOK_MHS KKM ON KKM.ID_MHS = M.ID_MHS
                WHERE KKM.ID_KKN_KELOMPOK='{$id}'
                ORDER BY P.KELAMIN_PENGGUNA,F.ID_FAKULTAS,PS.NM_PROGRAM_STUDI
            ");
    }

    function shuffle_assoc($list) {
        if (!is_array($list))
            return $list;

        $keys = array_keys($list);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key)
            $random[$key] = $list[$key];

        return $random;
    }

    function LoadKKNKelompokPeserta($jenis) {
        $query = "SELECT KKEL.*,KEL.NM_KELURAHAN,KEL.ID_KECAMATAN,KEC.NM_KECAMATAN,KOT.ID_KOTA,KOT.NM_KOTA,
            (SELECT COUNT(DISTINCT(ID_MHS)) FROM AUCC.KKN_KELOMPOK_MHS WHERE ID_KKN_KELOMPOK=KKEL.ID_KKN_KELOMPOK) TERISI
            FROM AUCC.KKN_KELOMPOK KKEL
            JOIN AUCC.KELURAHAN KEL ON KKEL.ID_KELURAHAN=KEL.ID_KELURAHAN
            JOIN AUCC.KECAMATAN KEC ON KEC.ID_KECAMATAN=KEL.ID_KECAMATAN
            JOIN AUCC.KOTA KOT ON KOT.ID_KOTA=KEC.ID_KOTA
            JOIN AUCC.KKN_ANGKATAN KA ON KA.ID_KKN_ANGKATAN=KKEL.ID_KKN_ANGKATAN
            JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=KA.ID_SEMESTER
            WHERE KA.STATUS_AKTIF='1' 
                AND KKEL.JENIS_KKN='{$jenis}'
                AND S.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ORDER BY KKEL.JENIS_KKN,KEL.ID_KECAMATAN,KEL.NM_KELURAHAN";
        $data_kelompok = $this->db->QueryToArray($query);
        $arr_hasil = array();
        foreach ($data_kelompok as $dk) {
            $dpl = $this->GetDPLKelompok($dk['ID_KKN_KELOMPOK']);
            $korcam = $this->GetKorcamKelompok($dk['ID_KECAMATAN']);
            $korkab = $this->GetKorkabKelompok($dk['ID_KABUPATEN']);
            array_push($arr_hasil, array_merge($dk, array(
                        'DPL' => $dpl['NM_PENGGUNA']." (".$dpl['MOBILE_DOSEN'].")",
                        'KORCAM' => $korcam['NM_PENGGUNA']." (".$korcam['MOBILE_DOSEN'].")",
                        'KORKAB' => $korkab['NM_PENGGUNA']." (".$korkab['MOBILE_DOSEN'].")",
                        'MHS_KKN' => $this->LoadKKNKelompokMhs($dk['ID_KKN_KELOMPOK'])
                    )));
        }
        return $arr_hasil;
    }
    
    function ResetPlottingMhs($jenis){
        $this->db->Query("
        UPDATE KKN_KELOMPOK_MHS SET ID_KKN_KELOMPOK='' WHERE ID_KKN_KELOMPOK_MHS IN (
            SELECT ID_KKN_KELOMPOK_MHS 
            FROM KKN_KELOMPOK_MHS KKM
            JOIN KKN_ANGKATAN KA ON KA.ID_KKN_ANGKATAN=KKM.ID_KKN_ANGKATAN
            JOIN SEMESTER S ON S.ID_SEMESTER=KA.ID_SEMESTER
            WHERE KA.STATUS_AKTIF='1'
            AND KKM.JENIS_KKN='{$jenis}'
            AND KKM.ID_KKN_KELOMPOK IS NOT NULL
            AND S.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
        )
            ");
    }

    function PlottingMhsKKN($jenis) {
        $total = $this->GetTotalKKN($jenis);
        $data_kelompok = $this->shuffle_assoc($this->LoadKknKelompok($jenis));
        foreach ($data_kelompok as $k) {
            $query_quota = "
                    SELECT KKEL.*,
                    (SELECT COUNT(DISTINCT(ID_MHS)) FROM AUCC.KKN_KELOMPOK_MHS WHERE ID_KKN_KELOMPOK=KKEL.ID_KKN_KELOMPOK) TERISI
                    FROM AUCC.KKN_KELOMPOK KKEL
                    WHERE KKEL.ID_KKN_KELOMPOK='{$k['ID_KKN_KELOMPOK']}'
                    ";
            $this->db->Query($query_quota);
            $kel = $this->db->FetchAssoc();
            if ($kel['TERISI'] < $kel['QUOTA']||$kel['TERISI'] < round($kel['QUOTA']*0.9)) {
                foreach ($this->LoadEstimasiGender($jenis) as $g) {
                    $qg = round($g['JML_PESERTA'] / $total * $k['QUOTA']);
                    foreach ($this->LoadEstimasiFakultas($jenis) as $f) {
                        $qf = round($f['JML_PESERTA'] / $total * $k['QUOTA']);
                        if ($qf == 0) {
                            $qf = 1;
                        }
                        $query_cek_f = "SELECT COUNT(M.ID_MHS)
                                FROM KKN_KELOMPOK_MHS KKM
                                JOIN MAHASISWA M ON M.ID_MHS=KKM.ID_MHS
                                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                                JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
                                WHERE PS.ID_FAKULTAS='{$f['ID_FAKULTAS']}' 
                                AND KKM.ID_KKN_KELOMPOK='{$k['ID_KKN_KELOMPOK']}'";
                        $ef = $this->db->QuerySingle($query_cek_f);
                        $query_cek_g = "SELECT COUNT(M.ID_MHS)
                                FROM KKN_KELOMPOK_MHS KKM
                                JOIN MAHASISWA M ON M.ID_MHS=KKM.ID_MHS
                                JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
                                WHERE P.KELAMIN_PENGGUNA='{$g['KELAMIN_PENGGUNA']}' 
                                AND KKM.ID_KKN_KELOMPOK='{$k['ID_KKN_KELOMPOK']}'";
                        $eg = $this->db->QuerySingle($query_cek_g);
                        if ($ef <= $qf && $eg < $qg) {
                            $arr_prod = $this->db->QueryToArray("
                                    SELECT ID_PROGRAM_STUDI 
                                    FROM PROGRAM_STUDI 
                                    WHERE ID_FAKULTAS='{$f['ID_FAKULTAS']}' AND ID_PROGRAM_STUDI IN
                                    (
                                        SELECT PS.ID_PROGRAM_STUDI
                                        FROM KKN_KELOMPOK_MHS KKM
                                        JOIN KKN_ANGKATAN KA ON KA.ID_KKN_ANGKATAN=KKM.ID_KKN_ANGKATAN
                                        JOIN MAHASISWA M ON M.ID_MHS=KKM.ID_MHS
                                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                                        WHERE KKM.ID_KKN_KELOMPOK IS NULL AND KKM.JENIS_KKN='{$jenis}' AND KA.STATUS_AKTIF='1'
                                    )");
                            $random_prod = rand(0, count($arr_prod) - 1);
                            $query_ambil = "
                                SELECT M.ID_MHS
                                FROM KKN_KELOMPOK_MHS KKM
                                JOIN MAHASISWA M ON M.ID_MHS=KKM.ID_MHS
                                JOIN KKN_ANGKATAN KA ON KA.ID_KKN_ANGKATAN=KKM.ID_KKN_ANGKATAN
                                JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                                JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
                                WHERE PS.ID_FAKULTAS='{$f['ID_FAKULTAS']}' 
                                AND PS.ID_PROGRAM_STUDI='{$arr_prod[$random_prod]['ID_PROGRAM_STUDI']}' 
                                AND P.KELAMIN_PENGGUNA='{$g['KELAMIN_PENGGUNA']}'
                                AND KKM.ID_KKN_KELOMPOK IS NULL 
                                AND KKM.JENIS_KKN='{$jenis}' AND KA.STATUS_AKTIF='1'
                                AND ROWNUM=1";
                            $ran = $this->db->QuerySingle($query_ambil);
                            if ($ran != '') {
                                $query_update = "
                                    UPDATE KKN_KELOMPOK_MHS SET ID_KKN_KELOMPOK='{$k['ID_KKN_KELOMPOK']}'
                                    WHERE ID_MHS='{$ran}'
                                    ";
                                $this->db->Query($query_update);
                            }
                        }
                    }
                }
            }
        }
    }

}

?>
