<?php

require_once('config.php');

// security sementara terhadap no auth access ~sugenk.
if ($user->Role() != AUCC_ROLE_LPPM){
	header("location: /logout.php");
    exit();
}	

	/*
	Created By Sani Iman Pribadi
	*/

	class KknModel{

		public $db;
		public $user;
		public $jml_data = "";

		public $page = "";
		public $jml_kelompok = 0;
		public $id_kelompok = array();
		public $jenis = array();
		public $kelompok = array();
		public $kelurahan = array();
		public $quota = array();
		public $peserta = array();
		public $sisa = array();
		public $jml_pria = array();
		public $jml_wanita = array();
		public $ada = "";

		public $id_mhs_status = "";
		public $id_dpl_status = "";
		public $dpl_status = "";
		public $id_korbing_status = "";
		public $korbing_status = "";
		public $id_korkab_status = "";
		public $korkab_status = "";
		public $id_kelurahan_status = "";
		public $kelurahan_status = "";
		public $id_kecamatan_status = "";
		public $kecamatan_status = "";
		public $id_kota_status = "";
		public $kota_status = "";
		public $provinsi_status = "";
		public $kelompok_status = "";
		public $username_status = "";
		public $foto_status = "";
		public $is_clicked = "";
		
		// Detail
		
		public $id_mhs_detail = array();
		public $id_dpl_detail = "";
		public $dpl_detail = "";

		
		public $id_korbing_detail = "";
		public $korbing_detail = "";
		public $id_korkab_detail = "";
		public $korkab_detail = "";
		public $id_kelurahan_detail = "";
		public $kelurahan_detail = "";
		public $id_kecamatan_detail = "";
		
		public $kecamatan_detail = "";
		public $id_kota_detail = "";
		public $kota_detail = "";
		public $provinsi_detail = "";
		
		public $kelompok_detail = "";
		public $username_detail = array();
		public $foto_detail = array();
		public $prodi_detail = array();
		public $nama_detail = array();
		
		
		function __construct() {
				$this->db = new MyOracle();
				//$this->db->Open();
		}
		
		public function getCountGender($gender){
		
			return $this->db->QuerySingle("
					select jumlah from (
					select kelamin_pengguna as id_kelamin, count(kelamin_pengguna) as jumlah from (                
					select pmk.id_mhs, p.nm_pengguna, mk.nm_mata_kuliah, ps.nm_program_studi, p.kelamin_pengguna, pmk.id_pengambilan_mk, mhs.nim_mhs  from pengambilan_mk pmk 
										left join kurikulum_mk kmk on kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
										left join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
										left join kelas_mk kls on kls.id_kelas_mk = pmk.id_kelas_mk
										left join mahasiswa mhs on mhs.id_mhs = pmk.id_mhs
										left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
										left join pengguna p on p.id_pengguna = mhs.id_pengguna
										where pmk.id_semester = (select id_semester from semester where status_aktif_semester = 'True') and (mk.nm_mata_kuliah like ('%KKN%') or upper(mk.nm_mata_kuliah) like '%KERJA NYATA%')
										and p.kelamin_pengguna = '$gender'
							  order by p.kelamin_pengguna, pmk.id_mhs)
					group by kelamin_pengguna)
			");
		
		}
		
		public function getSumGender(){
			return $this->db->QuerySingle("
					select sum(jumlah) from (
					select kelamin_pengguna as id_kelamin, count(kelamin_pengguna) as jumlah from (                
					select pmk.id_mhs, p.nm_pengguna, mk.nm_mata_kuliah, ps.nm_program_studi, p.kelamin_pengguna, pmk.id_pengambilan_mk, mhs.nim_mhs  from pengambilan_mk pmk 
										left join kurikulum_mk kmk on kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
										left join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
										left join kelas_mk kls on kls.id_kelas_mk = pmk.id_kelas_mk
										left join mahasiswa mhs on mhs.id_mhs = pmk.id_mhs
										left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
										left join pengguna p on p.id_pengguna = mhs.id_pengguna
										where pmk.id_semester = (select id_semester from semester where status_aktif_semester = 'True') and (mk.nm_mata_kuliah like ('%KKN%') or upper(mk.nm_mata_kuliah) like '%KERJA NYATA%')
							  order by p.kelamin_pengguna, pmk.id_mhs)
					group by kelamin_pengguna)
			");
		}
		
		
		public function getPersenGender($gender){
			$total = $this->getSumGender();
			$countGender = $this->getCountGender($gender);
			$fnumber = $countGender/$total;
			$persen = $fnumber;
			return $persen;
		}
		
		public function getGender($id_pengguna){
		
			return $this->db->QuerySingle("select kelamin_pengguna from pengguna where id_pengguna = '$id_pengguna'");
		
		}
		
		public function getQuotaKelompok($id_kelompok){
			return $this->db->QuerySingle("select quota from kkn_kelompok where id_kkn_kelompok = '$id_kelompok'");
		}
		
		public function getQuotaGender($gender, $id_kelompok){
			
			
			$quota = $this->getQuotaKelompok($id_kelompok);
			$persen = $this->getPersenGender(1);
			$quotaGender = $persen * $quota;
			$quotaGender = round($quotaGender);
			
			if($gender == '1'){
				return ($quotaGender);
			}else{
				$quotaPerempuan = $quota - $quotaGender;
				return $quotaPerempuan;
			}
		}
		
		public function getMemberKelompok($gender, $id_kelompok){
			return $this->db->QuerySingle("select count(*) from kkn_kelompok_mhs a
											left join mahasiswa b on b.id_mhs = a.id_mhs
											left join pengguna c on c.id_pengguna = b.id_pengguna
											where c.kelamin_pengguna = '$gender' and a.id_kkn_kelompok = '$id_kelompok' and a.id_kkn_kelompok is not null
											");
		}
		
		public function getInfoClicked($id_mhs){
			return $this->db->QuerySingle("select is_clicked from kkn_kelompok_mhs where id_mhs = '$id_mhs'");
		}
		
		public function getIdMhs($id_pengguna){
			
			return $this->db->QuerySingle("select id_mhs from mahasiswa where id_pengguna = '$id_pengguna'");
		
		}
		
		public function updateKelompok($id_pengguna, $id_kelompok){
			
			$id_mhs = $this->getIdMhs($id_pengguna);
			$gender = $this->getGender($id_pengguna);
			//echo $this->getQuotaGender($gender, $id_kelompok);
			//echo ".";
			//echo $this->getMemberKelompok($gender, $id_kelompok);
			//echo $quota;
			//$get
			//$isClicked = $this->getInfoClicked($id_mhs);
			//if($this->getMemberKelompok($gender, $id_kelompok) < $this->getQuotaGender($gender, $id_kelompok)){
				$this->db->Query("update kkn_kelompok_mhs set id_kkn_kelompok = '$id_kelompok' where id_mhs = '$id_mhs'");
				echo 'Berhasil mengupdate kelompok KKN';
				//$this->db->Query("update kkn_kelompok_mhs set id_kkn_kelompok = '$id_kelompok', is_clicked = (select is_clicked+1 from kkn_kelompok_mhs where id_mhs = '$id_mhs' ) where id_mhs = '$id_mhs'");
				
			//}else{
			//	echo 'Quota sudah penuh, silakan untuk menambah quota lagi';
			//}
		}

		public function requestData($view, $id){

			if($view=='index'){
				
			}

			else if($view=='report'){
				
				$this->db->Query("select a.id_kkn_kelompok as id_kelompok, a.id_kelurahan, c.kelompok, b.nm_kelurahan as kelurahan, a.QUOTA as quota, c.peserta, a.QUOTA - c.peserta as sisa, a.jenis_kkn from kkn_kelompok a
									left join kelurahan b on b.id_kelurahan = a.id_kelurahan
									left join (
									SELECT id_kelompok, id_kelurahan, kelurahan, kelompok, peserta FROM (
																	  SELECT count(kmhs.id_kkn_kelompok) as peserta, kmhs.id_kkn_kelompok as id_kelompok, kel.nama_kelompok as kelompok, lur.id_kelurahan as id_kelurahan, lur.nm_kelurahan as kelurahan FROM kkn_kelompok_mhs kmhs
																	  left join kkn_kelompok kel on kel.id_kkn_kelompok = kmhs.id_kkn_kelompok
																	  left join kelurahan lur on lur.id_kelurahan = kel.id_kelurahan
																	  WHERE kmhs.id_kkn_kelompok is not null 
																	  GROUP BY kmhs.id_kkn_kelompok, kel.nama_kelompok, lur.id_kelurahan, lur.nm_kelurahan)
													  ) c on c.id_kelompok = a.id_kkn_kelompok
								where a.jenis_kkn = '$id' and a.status_publish = 1
				");
				
				$a = 0;
				while ($row = $this->db->FetchRow()){
					$this->id_kelompok[$a] = $row[0];
					$this->kelurahan[$a] = $row[3];
					$this->kelompok[$a] = $row[2];
					$this->quota[$a] = $row[4];
					$this->peserta[$a] = $row[5];
					$this->sisa[$a] = $row[6];
					$this->jenis[$a] = $row[7];
					//$this->jml_pria[$a] = $this->getQuotaGender('1', $this->id_kelompok[$a]);
					//$this->jml_wanita[$a] = $this->getQuotaGender('2', $row[0]);
					$this->jml_kelompok++;
					$a++;
				}

				$this->db->Close();
			}

			else if($view=='status'){
				
				//$this->db = new MyOracle();
				$this->user = $_SESSION['user']['ID_PENGGUNA'];

				$this->db->Query("SELECT id_pengguna, nm_pengguna, username, foto_pengguna FROM pengguna WHERE id_pengguna = '$this->user' 
				");
				while ($row = $this->db->FetchRow()){ 
					$this->foto_status = $row[3];
					$this->username_status = $row[2];
					$this->foto_status = $this->foto_status . '/' . $this->username_status . '.JPG';			
				}

				$this->db->Query("select kmhs.id_mhs, 
							dpl.id_dosen as ID_DPL,
							dospl.nama as DPL,
							kbing.id_dosen id_korbing, 
							dosbing.nama as korbing,
							kkab.id_dosen as id_korkab, 
							doskab.nama as korkab,
							lur.id_kelurahan, 
							lur.nm_kelurahan,
							kec.id_kecamatan, 
							kec.nm_kecamatan,
							kot.id_kota,
							kot.nm_kota, 
							prov.nm_provinsi, 
							kel.nama_kelompok, --15 
							kmhs.is_clicked, --16
							p.foto_pengguna, --17
							p.username --18
							from kkn_kelompok_mhs kmhs
							left join mahasiswa mhs on mhs.id_mhs = kmhs.id_mhs
							left join pengguna p on p.id_pengguna = mhs.id_pengguna
							left join kkn_kelompok kel on kel.id_kkn_kelompok = kmhs.id_kkn_kelompok
							left join kelurahan lur on lur.id_kelurahan = kel.id_kelurahan
							left join kkn_dpl dpl on dpl.id_kkn_kelompok = kel.id_kkn_kelompok
							left join kecamatan kec on kec.id_kecamatan = lur.id_kecamatan
							left join kkn_korbing kbing on kbing.id_kecamatan = kec.id_kecamatan
							left join kota kot on kot.id_kota = kec.id_kota
							left join kkn_korkab kkab on kkab.id_kabupaten = kot.id_kota
							left join provinsi prov on prov.id_provinsi = kot.id_provinsi
							left join (
										  select dos.id_pengguna as id, p.username as username, p.nm_pengguna||' [ '||ps.nm_program_studi||' ]' as nama, j.nm_jenjang as jenjang, p.foto_pengguna as foto, ps.nm_program_studi as prodi from dosen dos
												  join pengguna p on p.id_pengguna = dos.id_pengguna
												  join program_studi ps on ps.id_program_studi = dos.id_program_studi
												  join jenjang j on j.id_jenjang = ps.id_jenjang
												  order by p.nm_pengguna
							) dospl on (dospl.id = dpl.id_dosen)
							left join (
										  select dos.id_pengguna as id, p.username as username, p.nm_pengguna||' [ '||ps.nm_program_studi||' ]' as nama, j.nm_jenjang as jenjang, p.foto_pengguna as foto, ps.nm_program_studi as prodi from dosen dos
												  join pengguna p on p.id_pengguna = dos.id_pengguna
												  join program_studi ps on ps.id_program_studi = dos.id_program_studi
												  join jenjang j on j.id_jenjang = ps.id_jenjang
												  order by p.nm_pengguna
							) dosbing on dosbing.id = kbing.id_dosen
							left join (
										  select dos.id_pengguna as id, p.username as username, p.nm_pengguna||' [ '||ps.nm_program_studi||' ]' as nama, j.nm_jenjang as jenjang, p.foto_pengguna as foto, ps.nm_program_studi as prodi from dosen dos
												  join pengguna p on p.id_pengguna = dos.id_pengguna
												  join program_studi ps on ps.id_program_studi = dos.id_program_studi
												  join jenjang j on j.id_jenjang = ps.id_jenjang
												  order by p.nm_pengguna
							) doskab on doskab.id = kkab.id_dosen 
							where p.id_pengguna = '$this->user'
				");
				
				$this->ada = 0;
				while ($row = $this->db->FetchRow()){ 
					$this->id_mhs_status = $row[0];
					$this->id_dpl_status = $row[1];
					$this->dpl_status = $row[2];
					$this->id_korbing_status = $row[3];
					$this->korbing_status = $row[4];
					$this->id_korkab_status = $row[5];
					$this->korkab_status = $row[6];
					$this->id_kelurahan_status = $row[7];
					$this->kelurahan_status = $row[8];
					$this->id_kecamatan_status = $row[9];
					$this->kecamatan_status = $row[10];
					$this->id_kota_status = $row[11];
					$this->kota_status = $row[12];
					$this->provinsi_status = $row[13];
					$this->kelompok_status = $row[14];
					$this->is_clicked = $row[15];
					$this->ada++;
				}

				$this->db->Close();
			}

			// Detail
			else if($view=='detail'){
				
				$id = (int)$id;

				$this->db->Query("select kmhs.id_mhs, 
							dpl.id_dosen as ID_DPL,
							dospl.nama as DPL,
							kbing.id_dosen id_korbing, 
							dosbing.nama as korbing,
							kkab.id_dosen as id_korkab, 
							doskab.nama as korkab,
							lur.id_kelurahan, 
							lur.nm_kelurahan,
							kec.id_kecamatan, 
							kec.nm_kecamatan,
							kot.id_kota,
							kot.nm_kota, 
							prov.nm_provinsi,
							kel.id_kkn_kelompok,
							kel.nama_kelompok, 
							kmhs.is_clicked, 
							p.foto_pengguna, 
							p.username, 
							'S1 '||ps.nm_program_studi,
							p.nm_pengguna
							from kkn_kelompok_mhs kmhs
							left join mahasiswa mhs on mhs.id_mhs = kmhs.id_mhs
							left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
							left join pengguna p on p.id_pengguna = mhs.id_pengguna
							left join kkn_kelompok kel on kel.id_kkn_kelompok = kmhs.id_kkn_kelompok
							left join kelurahan lur on lur.id_kelurahan = kel.id_kelurahan
							left join kkn_dpl dpl on dpl.id_kkn_kelompok = kel.id_kkn_kelompok
							
							left join kecamatan kec on kec.id_kecamatan = lur.id_kecamatan
							left join kkn_korbing kbing on kbing.id_kecamatan = kec.id_kecamatan
							left join kota kot on kot.id_kota = kec.id_kota
							left join kkn_korkab kkab on kkab.id_kabupaten = kot.id_kota
							left join provinsi prov on prov.id_provinsi = kot.id_provinsi
							left join (
										  select dos.id_dosen as id, p.username as username, p.nm_pengguna||' [ '||ps.nm_program_studi||' ]' as nama, j.nm_jenjang as jenjang, p.foto_pengguna as foto, ps.nm_program_studi as prodi from dosen dos
												  join pengguna p on p.id_pengguna = dos.id_pengguna
												  join program_studi ps on ps.id_program_studi = dos.id_program_studi
												  join jenjang j on j.id_jenjang = ps.id_jenjang
												  order by p.nm_pengguna
							) dospl on (dospl.id = dpl.id_dosen)
							left join (
										  select dos.id_dosen as id, p.username as username, p.nm_pengguna||' [ '||ps.nm_program_studi||' ]' as nama, j.nm_jenjang as jenjang, p.foto_pengguna as foto, ps.nm_program_studi as prodi from dosen dos
												  join pengguna p on p.id_pengguna = dos.id_pengguna
												  join program_studi ps on ps.id_program_studi = dos.id_program_studi
												  join jenjang j on j.id_jenjang = ps.id_jenjang
												  order by p.nm_pengguna
							) dosbing on dosbing.id = kbing.id_dosen
							left join (
										  select dos.id_dosen as id, p.username as username, p.nm_pengguna||' [ '||ps.nm_program_studi||' ]' as nama, j.nm_jenjang as jenjang, p.foto_pengguna as foto, ps.nm_program_studi as prodi from dosen dos
												  join pengguna p on p.id_pengguna = dos.id_pengguna
												  join program_studi ps on ps.id_program_studi = dos.id_program_studi
												  join jenjang j on j.id_jenjang = ps.id_jenjang
												  order by p.nm_pengguna
							) doskab on doskab.id = kkab.id_dosen
							-- where dpl.id_dosen is not null and kel.id_kkn_kelompok = '$id'
							where kel.id_kkn_kelompok = '$id' and kel.status_publish = 1
				");
				
				$i = 0;
				$this->jml_data = 0;
				while ($row = $this->db->FetchRow()){ 
					$this->id_mhs_detail[$i] = $row[0];
					$this->id_dpl_detail = $row[1];
					$this->dpl_detail = $row[2];
					$this->id_korbing_detail = $row[3];
					$this->korbing_detail = $row[4];
					$this->id_korkab_detail = $row[5];
					$this->korkab_detail = $row[6];
					$this->id_kelurahan_detail = $row[7];
					$this->kelurahan_detail = $row[8];
					$this->id_kecamatan_detail = $row[9];
					$this->kecamatan_detail = $row[10];
					$this->id_kota_detail = $row[11];
					$this->kota_detail = $row[12];
					$this->provinsi_detail = $row[13];
					$this->kelompok_detail = $row[15];
					$this->foto_detail[$i] = $row[17];
					$this->username_detail[$i] = $row[18];
					$this->prodi_detail[$i] = $row[19];
					$this->nama_detail[$i] =  $row[20];
					$this->foto_detail[$i] = $this->foto_detail[$i] . '/' . $this->username_detail[$i] . '.JPG';
					$i++;
					$this->jml_data++;
				}

				$this->db->Close();
				
			}

			
		}

	}