<?php
	include 'config.php';

    $id_pt = $id_pt_user;

	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}

	$get_dosen= "";
	
	$query = array();
	
	
	if (isset($_POST['cari'])){  
		$get_dosen = $_POST['cari'];  
	
	
		$jml_data = 0;
		foreach ($get_dosen as $value) {
		  $query[$jml_data] = $value;
		  $jml_data++;
		}
		
		$j = 0;
		$jml_dosen = 0;
		for($i=0; $i<$jml_data; $i++){
			$db->Query("
				select id, nama, foto, prodi, username  from (
				  select dos.id_dosen as id, p.username as username, p.nm_pengguna||' [ '||ps.nm_program_studi||' ]' as nama, j.nm_jenjang as jenjang, p.foto_pengguna as foto, ps.nm_program_studi as prodi from dosen dos
						  join pengguna p on p.id_pengguna = dos.id_pengguna
						  join program_studi ps on ps.id_program_studi = dos.id_program_studi
						  join jenjang j on j.id_jenjang = ps.id_jenjang
						  where p.id_perguruan_tinggi = {$id_pt}
						  order by p.nm_pengguna)
				where nama like '$query[$i]'
			");

			while ($row = $db->FetchRow()){ 
				$id[$j] = $row[0];
				$nama[$j] = $row[1];
				$foto[$j] = $row[2];
				$prodi[$j] = $row[3];
				$foto[$i] = $foto[$i] . '/' . $row[4] . '.JPG';
				$jml_dosen++;
				$j++;
			}
		}
	}

	else{
			$db->Query("select id, nama, foto, prodi, username  from (
			  select dos.id_dosen as id, p.username as username, p.nm_pengguna||' [ '||ps.nm_program_studi||' ]' as nama, j.nm_jenjang as jenjang, p.foto_pengguna as foto, ps.nm_program_studi as prodi from dosen dos
					  join pengguna p on p.id_pengguna = dos.id_pengguna
					  join program_studi ps on ps.id_program_studi = dos.id_program_studi
					  join jenjang j on j.id_jenjang = ps.id_jenjang
					  where p.id_perguruan_tinggi = {$id_pt}
					  order by p.nm_pengguna)
			  where id in (select dpl.id_dosen from kkn_dpl dpl where dpl.id_semester = (select ka.id_semester from kkn_angkatan ka join semester s on s.id_semester = ka.id_semester where ka.status_aktif='1' and s.id_perguruan_tinggi = {$id_pt}) group by dpl.id_dosen) or id in(select korbing.id_dosen from kkn_korbing korbing where korbing.id_semester = (select ka.id_semester from kkn_angkatan ka join semester s on s.id_semester = ka.id_semester where ka.status_aktif='1' and s.id_perguruan_tinggi = {$id_pt}) group by korbing.id_dosen) or id in(select korkab.id_dosen from kkn_korkab korkab where korkab.id_semester = (select ka.id_semester from kkn_angkatan ka join semester s on s.id_semester = ka.id_semester where ka.status_aktif='1' and s.id_perguruan_tinggi = {$id_pt}) group by korkab.id_dosen)
			  -- where id in (select dpl.id_dosen from kkn_dpl dpl group by dpl.id_dosen) or id in(select korbing.id_dosen from kkn_korbing korbing  group by korbing.id_dosen) or id in(select korkab.id_dosen from kkn_korkab korkab group by korkab.id_dosen)
			");
			
			$j = 0;
			$jml_dosen = 0;
			while ($row = $db->FetchRow()){ 
				$id[$j] = $row[0];
				$nama[$j] = $row[1];
				$foto[$j] = $row[2];
				$prodi[$j] = $row[3];
				$foto[$j] = $foto[$j] . '/' . $row[4] . '.JPG';
				$jml_dosen++;
				$j++;
			}
	}

	$smarty->assign('id', $id);
	$smarty->assign('nama', $nama);
	$smarty->assign('foto', $foto);
	$smarty->assign('prodi', $prodi);
	$smarty->assign('jml_dosen', $jml_dosen);
	$smarty->display('get-kkn-dosen.tpl');
?>