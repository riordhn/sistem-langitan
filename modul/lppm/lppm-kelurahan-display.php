<?php
	include 'config.php';

	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}

	
	if(isset($_GET['id_kota'])){
		$id_kota = $_GET['id_kota'];
		$id_kelurahan = array();
		$nm_kelurahan = array();
		
		$db->Query("select id_kelurahan, nm_kelurahan from kelurahan where id_kecamatan = '$id_kota'");
		
		$i = 0;
		$jml_kelurahan = 0;
		while ($row = $db->FetchRow()){ 
			$id_kelurahan[$i] = $row[0];
			$nm_kelurahan[$i] = $row[1];
			$jml_kelurahan++;
			$i++;
		}
		
		$smarty->assign('id_kelurahan', $id_kelurahan);
		$smarty->assign('nm_kelurahan', $nm_kelurahan);
		$smarty->assign('jml_kelurahan', $jml_kelurahan);
		$smarty->display('lppm-kelurahan-display.tpl');
	}
?>