<?php
	include 'config.php';
	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}

	if(isset($_GET['prodi']) && isset($_GET['semester'])){
		
		$prodi = $_GET['prodi'];
		$id_semester = $_GET['semester'];
		$id_mhs = array();
		$nm_mhs = array();
		$nim = array();
		$nm_prodi = array();
		$kelamin = array();
		$jml_laki = 0;
		$jml_perempuan = 0;
		
		$db->Query("select pmk.id_mhs, p.nm_pengguna, mk.nm_mata_kuliah, ps.nm_program_studi, p.kelamin_pengguna, pmk.id_pengambilan_mk, mhs.nim_mhs  from pengambilan_mk pmk 
					left join kurikulum_mk kmk on kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
					left join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
					left join kelas_mk kls on kls.id_kelas_mk = pmk.id_kelas_mk
					left join mahasiswa mhs on mhs.id_mhs = pmk.id_mhs
					left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
					left join pengguna p on p.id_pengguna = mhs.id_pengguna
					where pmk.id_semester = '$id_semester' and mhs.id_program_studi = '$prodi' and kmk.status_mkta = 2 and pmk.STATUS_APV_PENGAMBILAN_MK = 1 and (pmk.nilai_huruf is null  or ((pmk.nilai_huruf = 'E' or pmk.nilai_angka = 0)  and pmk.flagnilai=0)) -- OR pmk.nilai_huruf = 'E' OR pmk.nilai_angka = 0)
					order by p.kelamin_pengguna, pmk.id_mhs");
		/*
		$db->Query("select pmk.id_mhs, p.nm_pengguna, mk.nm_mata_kuliah, ps.nm_program_studi, p.kelamin_pengguna, pmk.id_pengambilan_mk, mhs.nim_mhs  from pengambilan_mk pmk 
					left join kurikulum_mk kmk on kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
					left join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
					left join kelas_mk kls on kls.id_kelas_mk = pmk.id_kelas_mk
					left join mahasiswa mhs on mhs.id_mhs = pmk.id_mhs
					left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
					left join pengguna p on p.id_pengguna = mhs.id_pengguna
					where pmk.id_semester = '$id_semester' and mhs.id_program_studi = '$prodi' and (mk.nm_mata_kuliah like ('%KKN%') or upper(mk.nm_mata_kuliah) like '%KERJA NYATA%')
					order by p.kelamin_pengguna, pmk.id_mhs
		");
		*/

		$i = 0;
		$jml_data = 0;
		while ($row = $db->FetchRow()){ 
			$id_mhs[$i] = $row[0];
			$nm_mhs[$i] = $row[1];
			$nm_prodi[$i] = $row[3];
			$kelamin[$i] = $row[4];
			$nim[$i] = $row[6];
			$jml_data++;
			$i++;
		}
		
		$smarty->assign('semester', $id_semester);
		$smarty->assign('jml_laki', $jml_laki);
		$smarty->assign('jml_perempuan', $jml_perempuan);
		$smarty->assign('jml_data', $jml_data);
		$smarty->assign('id_mhs', $id_mhs);
		$smarty->assign('nm_mhs', $nm_mhs);
		$smarty->assign('nim', $nim);
		$smarty->assign('nm_prodi', $nm_prodi);
		$smarty->assign('kelamin', $kelamin);
		$smarty->assign('prodi', $prodi);
		$smarty->display('lppm-kkn-calon-peserta-display.tpl');
	}
?>