<?php
		include 'config.php';

	$id_pt = $id_pt_user;

	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}
		
		
		

		
		$id_fakultas = array();
		$nm_fakultas = array();
		
		$db->Query("select id_fakultas, nm_fakultas from fakultas where id_perguruan_tinggi = '{$id_pt}' order by id_fakultas");

		$i = 0;
		$jml_fak = 0;
		while ($row = $db->FetchRow()){ 
			$id_fakultas[$i] = $row[0];
			$nm_fakultas[$i] = $row[1];
			$jml_fak++;
			$i++;
		}
		
		$smarty->assign('jml_fak', $jml_fak);
		$smarty->assign('id_fakultas', $id_fakultas);
		$smarty->assign('nm_fakultas', $nm_fakultas);
	
		$fakultas = $_GET['fakultas'];
		$nm_prodi = array();
		$jml_peserta = array();
		
		$db->Query("select ps.nm_program_studi, count(pmk.id_mhs) as JML_PESERTA  from pengambilan_mk pmk 
					left join kurikulum_mk kmk on kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
					left join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
					left join kelas_mk kls on kls.id_kelas_mk = pmk.id_kelas_mk
					left join program_studi ps on ps.id_program_studi = kls.id_program_studi
					left join mahasiswa mhs on mhs.id_mhs = pmk.id_mhs
					left join pengguna p on p.id_pengguna = mhs.id_pengguna
					where pmk.id_semester = (select id_semester from semester where status_aktif_semester = 'True' and id_perguruan_tinggi = '{$id_pt}') and ps.id_fakultas = '$fakultas' and (mk.nm_mata_kuliah like ('%KKN%') or upper(mk.nm_mata_kuliah) like '%KERJA NYATA%') group by ps.nm_program_studi");

		$i = 0;
		$jml_data = 0;
		while ($row = $db->FetchRow()){ 
			$nm_prodi[$i] = $row[0];
			$jml_peserta[$i] = $row[1];
			$jml_data++;
			$i++;
		}
		
		$kelompok = $db->QueryToArray("
				select a.id_mhs, f.nm_pengguna as nama, e.nm_jenjang||' '||c.nm_program_studi as prodi, d.nm_fakultas as fakultas, g.nama_kelompok, g.jenis_kkn from kkn_kelompok_mhs a
				left join kkn_kelompok g on g.id_kkn_kelompok = a.id_kkn_kelompok
				left join mahasiswa b on b.id_mhs = a.id_mhs
				left join pengguna f on f.id_pengguna = b.id_pengguna
				left join program_studi c on c.id_program_studi = b.id_program_studi
				left join fakultas d on d.id_fakultas = c.id_fakultas
				left join jenjang e on e.id_jenjang = c.id_jenjang
				where d.id_fakultas = '$fakultas'-- and a.id_kkn_kelompok is not null
				order by a.id_kkn_kelompok
		");
		
		$smarty->assign('kelompok', $kelompok);
		$smarty->assign('jml_data', $jml_data);
		$smarty->assign('nm_prodi', $nm_prodi);
		$smarty->assign('jml_peserta', $jml_peserta);
		$smarty->display('lppm-kkn-info-peserta.tpl');
?>