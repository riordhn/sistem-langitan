<?php
include 'config.php';
include 'class/ProposalPenelitian.class.php';

$mode = get('mode', 'list');
$ProposalPenelitian = new ProposalPenelitian($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'upload-file')
    {
		$ProposalPenelitian->UploadFileProposal($_POST, $_FILES['file']);
		$smarty->assign('lokasi_file', "../../files/upload/dosen/proposal/" . $_POST['token'] . ".pdf");
		$smarty->assign('nama_file', $_FILES['file']['name']);
    }
	
	if (post('mode') == 'delete-file')
	{
		$result = $ProposalPenelitian->DeleteFileProposal($_POST);
		echo $result; 
		exit();
	}
    
    if (post('mode') == 'add')
    {	
		$result = $ProposalPenelitian->Add($_POST);
		$smarty->assign('result', $result);
		$mode = 'add-result';
    }
    
    if (post('mode') == 'edit')
    {
		$result = $ProposalPenelitian->Edit($_POST);
		$smarty->assign('result', $result);
		$smarty->assign('id_penelitian', post('id_penelitian'));

		$mode = 'edit-result';
    }
}

if ($request_method == 'GET')
{
    if ($mode == 'list')
    {
        $proposal_penelitian_set = $ProposalPenelitian->GetListProposalForLPPM();
        $smarty->assign('proposal_set', $proposal_penelitian_set);
    }
    
    if ($mode == 'add')
    {
        $new_id = $ProposalPenelitian->GetNewID();
        
        $smarty->assign('token', md5($new_id . time()));
        $smarty->assign('id_penelitian', $new_id);
        $smarty->assign('penelitian_jenis_set', $ProposalPenelitian->GetListJenisPenelitian());
        $smarty->assign('lppm', $ProposalPenelitian->GetKetuaLPPM());
        $smarty->assign('golongan_set', $ProposalPenelitian->GetListGolongan());
    }
    
    if ($mode == 'edit')
    {
        $id_penelitian = get('id_penelitian');
        $proposal = $ProposalPenelitian->GetProposal($id_penelitian);
        
        $smarty->assign('token', md5($id_penelitian . time()));
        $smarty->assign('proposal', $proposal);
        $smarty->assign('penelitian_jenis_set', $ProposalPenelitian->GetListJenisPenelitian());
        $smarty->assign('penelitian_skim_set', $ProposalPenelitian->GetListSKIM($proposal['ID_PENELITIAN_JENIS']));
        
        $penelitian_bidang_set = $ProposalPenelitian->GetListBidangPenelitian($proposal['ID_PENELITIAN_JENIS']);
        $smarty->assign('penelitian_bidang_set', $penelitian_bidang_set);
        
        // Mengecek apakah pilihan bidang / tema pada yang lainnya
        foreach ($penelitian_bidang_set as $bidang)
            if ($bidang['LAINNYA'] == 1 && $bidang['ID_PENELITIAN_BIDANG'] == $proposal['ID_PENELITIAN_BIDANG'])
                $smarty->assign('bidang_lain', true); 
            
        $smarty->assign('lppm', $ProposalPenelitian->GetKetuaLPPM());
    }
    
    if ($mode == 'search-dosen')
    {
        $term = get('term');
        
        $data_set = $db->QueryToArray("
            select * from (
				select d.id_dosen, p.username, p.nm_pengguna, nm_fakultas, dek.nm_pengguna as nm_dekan, dek.username as nip_dekan from pengguna p
				join dosen d on d.id_pengguna = p.id_pengguna
				join program_studi ps on ps.id_program_studi = d.id_program_studi
				join fakultas f on f.id_fakultas = ps.id_fakultas
				join pengguna dek on dek.id_pengguna = f.id_dekan
				where upper(p.nm_pengguna) like upper('{$term}%') or upper(p.nm_pengguna) like upper('% {$term}%')
				order by length(p.username) desc, p.nm_pengguna
            ) where rownum <= 10");
            
        $result = array();
        foreach ($data_set as $row)
            array_push($result, array(
                'value'     => $row['ID_DOSEN'],
                'label'     => $row['USERNAME'] . " : " . $row['NM_PENGGUNA'] . " (" . $row['NM_FAKULTAS'] . ")",
                'fakultas'  => 'Fakultas ' . $row['NM_FAKULTAS'],
                'nama'      => $row['NM_PENGGUNA'],
                'nip'       => $row['USERNAME'],
                'nm_dekan'  => $row['NM_DEKAN'],
                'nip_dekan'  => $row['NIP_DEKAN']
            ));
        
        echo json_encode($result);
        
        exit();
    }
    
    if ($mode == 'view')
    {
        $smarty->assign('penelitian', $ProposalPenelitian->GetProposal(get('id_penelitian')));
    }
    
    if ($mode == 'get-penelitian-skim')
    {   
        $penelitian_skim_set = $ProposalPenelitian->GetListSKIM(get('id_penelitian_jenis'));
        
        if (count($penelitian_skim_set) > 0)
        {
            foreach ($penelitian_skim_set as $ps)
            {
                if ($ps['KODE_SKIM'] != '')
                    echo "<option value=\"{$ps['ID_PENELITIAN_SKIM']}\">[{$ps['KODE_SKIM']}] - {$ps['NAMA_SKIM']}</option>";
                else
                    echo "<option value=\"{$ps['ID_PENELITIAN_SKIM']}\">{$ps['NAMA_SKIM']}</option>";
            }
        }
        else
        {
            echo "0";
        }
            
        exit();
    }
    
    if ($mode == 'get-penelitian-bidang')
    {       
        $penelitian_bidang_set = $ProposalPenelitian->GetListBidangPenelitian(get('id_penelitian_jenis'));
        
        foreach ($penelitian_bidang_set as $pb)
            echo "<option value=\"{$pb['ID_PENELITIAN_BIDANG']}\">{$pb['NAMA_BIDANG']} \ {$pb['NAMA_TEMA']}</option>";
        
        exit();
    }
}

$smarty->display("proposal/proposal/{$mode}.tpl");
?>
