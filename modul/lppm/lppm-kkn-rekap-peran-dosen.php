<?php
	if (!($_SERVER[ "HTTP_X_REQUESTED_WITH" ]) && !($_SERVER[ "HTTP_X_REQUESTED_WITH" ] ==="XMLHttpRequest" )) {
		exit();
	}
	include 'config.php';
	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}
	$smarty->display('lppm-kkn-rekap-peran-dosen.tpl');

?>