<?php
	include 'config.php';


$id_pt = $id_pt_user;

if ($user->Role() != AUCC_ROLE_LPPM){
	header("location: /logout.php");
    exit();
}	
	$id_kkn_angkatan = array();
	$kode_angkatan = array();
	$nama_angkatan = array();
	$id_semester = array();
	$nm_semester = array();
	$akd_semester = array();
	
	$db->Query("SELECT akt.id_kkn_angkatan, akt.kode_angkatan, akt.nama_angkatan, akt.id_semester, smt.nm_semester, smt.tahun_ajaran,akt.status_aktif FROM kkn_angkatan akt
				LEFT JOIN semester smt on smt.id_semester = akt.id_semester
				WHERE smt.id_perguruan_tinggi = '{$id_pt}'
				ORDER BY id_semester
	");
	$i = 0;
	$jml_angkatan = 0;
	while ($row = $db->FetchRow()){ 
		$id_kkn_angkatan[$i] = $row[0];
		$kode_angkatan[$i] = $row[1];
		$nama_angkatan[$i] = $row[2];
		$id_semester[$i] = $row[3];
		$nm_semester[$i] = $row[4];
		$akd_semester[$i] = $row[5];
                $status_aktif[$i] = $row[6];
		$jml_angkatan++;
		$i++;
	}
	
	$smarty->assign('jml_angkatan', $jml_angkatan);
	$smarty->assign('id_kkn_angkatan', $id_kkn_angkatan);
	$smarty->assign('kode_angkatan', $kode_angkatan);
	$smarty->assign('nama_angkatan', $nama_angkatan);
	$smarty->assign('id_semester', $id_semester);
	$smarty->assign('nm_semester', $nm_semester);
	$smarty->assign('akd_semester', $akd_semester);
        $smarty->assign('status_aktif', $status_aktif);
	$smarty->display('lppm-kkn-angkatan-display.tpl');
?>