<?php
include 'config.php';
include 'class/ProposalPenelitian.class.php';

$mode = get('mode', 'search');
$ProposalPenelitian = new ProposalPenelitian($db);

if ($request_method == 'POST')
{   
    if (post('mode') == 'upload-file')
    {
        if (post('for') == 'add')
        {
            $result = $ProposalPenelitian->UploadFileProposal($_POST, $_FILES['file'], ProposalPenelitian::UPLOAD_FOR_ADD);
            $smarty->assign('result', $result ? $_FILES['file']['name'] : "Gagal upload file, silahkan refresh halaman (F5).");
        }
        
        if (post('for') == 'edit')
        {
            
        }
    }
    
    if (post('mode') == 'add')
    {
        $result = $ProposalPenelitian->Add(post('id_dosen'), $_POST);
        $smarty->assign('result', $result ? "Proposal berhasil disimpan" : "Proposal gagal disimpan");
    }
}

if ($request_method == 'GET')
{
    if ($mode == 'search')
    {
        if (get('q') != '')
        {
            $smarty->assign('dosen_set', $ProposalPenelitian->CariPeneliti(get('q')));
        }
    }
    
    if ($mode == 'add')
    {
        $smarty->assign('dosen', $ProposalPenelitian->GetDosen(get('id_dosen')));
        $smarty->assign('penelitian_jenis_set', $ProposalPenelitian->GetListJenisPenelitian());
        $smarty->assign('dekan', $ProposalPenelitian->GetDekan(get('id_dosen')));
        $smarty->assign('lppm', $ProposalPenelitian->GetKetuaLPPM());
        $smarty->assign('golongan_set', $ProposalPenelitian->GetListGolongan());
    }
    
    if ($mode == 'get-penelitian-skim')
    {   
        $penelitian_skim_set = $ProposalPenelitian->GetListSKIM(get('id_penelitian_jenis'));
        
        if (count($penelitian_skim_set) > 0)
        {
            foreach ($penelitian_skim_set as $ps)
            {
                if ($ps['KODE_SKIM'] != '')
                    echo "<option value=\"{$ps['ID_PENELITIAN_SKIM']}\">[{$ps['KODE_SKIM']}] - {$ps['NAMA_SKIM']}</option>";
                else
                    echo "<option value=\"{$ps['ID_PENELITIAN_SKIM']}\">{$ps['NAMA_SKIM']}</option>";
            }
        }
        else
        {
            echo "0";
        }
            
        exit();
    }
    
    if ($mode == 'get-penelitian-bidang')
    {       
        $penelitian_bidang_set = $ProposalPenelitian->GetListBidangPenelitian(get('id_penelitian_jenis'));
        
        foreach ($penelitian_bidang_set as $pb)
            echo "<option value=\"{$pb['ID_PENELITIAN_BIDANG']}\">{$pb['NAMA_TEMA']}</option>";
        
        exit();
    }
}

$smarty->display("proposal/add/{$mode}.tpl");
?>
