<?php
include 'config.php';
include 'class/ProposalPenelitian.class.php';

$ProposalPenelitian = new ProposalPenelitian($db);

$tahun_set = $ProposalPenelitian->GetDistinctTahun();

// ------------------------- SAMPAI SINI ----------------------------------

$smarty->assign('tahun_set', $ProposalPenelitian->GetDistinctTahun());
$smarty->assign('fakultas_set', $ProposalPenelitian->GetListFakultas());
$smarty->assign('skim_set', $ProposalPenelitian->GetListSKIM());
$smarty->assign('data_set', $ProposalPenelitian->GetReportProposalDataPerSkimFakultas());

$smarty->display("report-proposal/skim.tpl");
?>