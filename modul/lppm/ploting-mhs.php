<?php

/** @var $smarty SmartyBC */
include 'config.php';
include 'class/ploting.class.php';

$p = new ploting($db);
//$db->Query("DELETE FROM KKN_KELOMPOK_MHS");
//$db->Query("UPDATE KKN_KELOMPOK_MHS SET ID_KKN_KELOMPOK=NULL WHERE ID_KKN_KELOMPOK IS NOT NULL AND JENIS_KKN=1");
if (get('mode') == 'detail') {
    $smarty->assign('kelompok', $p->GetKelurahanKelompok(get('kel')));
    $smarty->assign('mhs_kelompok', $p->LoadKKNKelompokMhs(get('kel')));
} else if (get('mode') == 'ploting') {
    if (isset($_POST)) {
        if (post('mode') == 'plot') {
            $p->PlottingMhsKKN(get('jenis'));
        } else if (post('mode') == 'reset') {
            $p->ResetPlottingMhs(get('jenis'));
        }
    }
    $smarty->assign('quota_kel', $p->GetQuotaKelompok(get('jenis')));
    $smarty->assign('kel_kkn', $p->LoadKknKelompok(get('jenis')));
    $smarty->assign('e_gender', $p->LoadEstimasiGender(get('jenis')));
    $smarty->assign('e_fakultas', $p->LoadEstimasiFakultas(get('jenis')));
    $smarty->assign('total_sudah', $p->GetSudahDapatKelompok(get('jenis')));
    $smarty->assign('total_belum', $p->GetBelumDapatKelompok(get('jenis')));
    $smarty->assign('total_kkn', $p->GetTotalKKN(get('jenis')));
    $smarty->assign('angkatan', $p->GetAngkatanAktif());
} else {
    $smarty->assign('angkatan', $p->GetAngkatanAktif());
}
$smarty->display("ploting-mhs.tpl");
?>
