<?php

include 'config.php';
include 'class/kegiatan.class.php';

$keg = new kegiatan($db);

if (isset($_GET)) {
    if (get('angkatan') != '') {
        if (isset($_POST)) {
            if (post('mode') == 'tambah') {
                $keg->TambahKegiatanKKN($_POST);
            } else if (post('mode') == 'edit') {
                $keg->UpdateKegiatanKKN($_POST);
            } else if (post('mode') == 'delete') {
                $keg->DeleteKegiatanKKN($_POST);
            }
        }
        if (get('mode') == 'edit' || get('mode') == 'delete') {
            $smarty->assign('keg', $keg->GetKegiatanKKN(get('id')));
        }
        $smarty->assign('data_kegiatan', $keg->LoadKegiatanKKN(get('angkatan')));
    }
}
$smarty->assign('kkn_angkatan', $keg->LoadKKNAngkatan());
$smarty->display("kegiatan-kkn.tpl");
?>
