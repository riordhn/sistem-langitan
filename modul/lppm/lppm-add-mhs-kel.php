<?php
	include 'config.php';

	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}

	if(isset($_GET['prodi'])){
		
		$prodi = $_GET['prodi'];
		$id_mhs = array();
		$nm_mhs = array();
		$nm_prodi = array();
		$kelamin = array();
		$id_kkn_kel_mhs = array();
		$foto = array();
		$username = array();
		$jml_laki = 0;
		$jml_perempuan = 0;
		
		
		$db->Query("select kmhs.id_mhs, p.nm_pengguna, ps.nm_program_studi, p.kelamin_pengguna, kmhs.id_kkn_kelompok_mhs, p.foto_pengguna, p.username from kkn_kelompok_mhs kmhs
					left join mahasiswa mhs on mhs.id_mhs = kmhs.id_mhs
					left join program_studi ps on ps.id_program_studi = mhs.id_program_studi
					left join pengguna p on p.id_pengguna = mhs.id_pengguna
					where mhs.id_program_studi = '$prodi'
					order by mhs.nim_mhs, p.kelamin_pengguna, kmhs.id_mhs
		");

		$i = 0;
		$jml_data = 0;
		while ($row = $db->FetchRow()){ 
			$id_mhs[$i] = $row[0];
			$nm_mhs[$i] = $row[1];
			$nm_prodi[$i] = $row[2];
			$kelamin[$i] = $row[3];
			$id_kkn_kel_mhs[$i] = $row[4];
			$foto[$i] = $row[5];
			$username[$i] = $row[6];
			$foto[$i] = $foto[$i] . '/' . $username[$i] . '.JPG';
			$jml_data++;
			$i++;
		}
		
		$id_kkn_kelompok = array();
		$nm_kkn_kelompok = array();
		
		$db->Query("select kel.id_kkn_kelompok, kel.nama_kelompok from kkn_kelompok kel
					left join semester smt on smt.id_semester = kel.id_semester
					where smt.status_aktif_semester ='True'
					order by kel.id_kkn_kelompok
		");

		
		$i = 0;
		$jml_kelompok = 0;
		while ($row = $db->FetchRow()){ 
			$id_kkn_kelompok[$i] = $row[0];
			$nm_kkn_kelompok[$i] = $row[1];
			$jml_kelompok++;
			$i++;
		}

		$smarty->assign('jml_kelompok', $jml_kelompok);
		$smarty->assign('id_kkn_kelompok', $id_kkn_kelompok);
		$smarty->assign('nm_kkn_kelompok', $nm_kkn_kelompok);
		
		$smarty->assign('username', $username);
		$smarty->assign('jml_laki', $jml_laki);
		$smarty->assign('jml_perempuan', $jml_perempuan);
		$smarty->assign('jml_data', $jml_data);
		$smarty->assign('id_mhs', $id_mhs);
		$smarty->assign('nm_mhs', $nm_mhs);
		$smarty->assign('nm_prodi', $nm_prodi);
		$smarty->assign('kelamin', $kelamin);
		$smarty->assign('id_kkn_kel_mhs', $id_kkn_kel_mhs);
		$smarty->assign('foto', $foto);
		$smarty->display('lppm-add-mhs-kel.tpl');
	}
?>