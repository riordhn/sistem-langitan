<?php

	include 'config.php';

	$id_pt = $id_pt_user;

	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}
	if(isset($_POST['jml_kelompok'])){
		$jml_kelompok = $_POST['jml_kelompok'];
		$id_kecamatan = $_POST['id_kecamatan'];
		
		//unset($_SESSION['jml_kelompok']);
		//$_SESSION['jml_kelompok'] = $jml_kelompok;
	
	
		$id_kelurahan = array();
		$nm_kelurahan = array();
		
		$db->Query("select id_kelurahan, nm_kelurahan from kelurahan where id_kecamatan = '$id_kecamatan'");
		
		$i = 0;
		$jml_desa = 0;
		while ($row = $db->FetchRow()){ 
			$id_kelurahan[$i] = $row[0];
			$nm_kelurahan[$i] = $row[1];
			$jml_kelurahan++;
			$i++;
		}
		
		$db->Query("select id_semester 
						from semester 
						where status_aktif_semester = 'True' and id_perguruan_tinggi = '{$id_pt}'");

		while ($row = $db->FetchRow()){ 
			$id_semester = $row[0];
		}
		
		$smarty->assign('id_semester', $id_semester);
		$smarty->assign('id_kelurahan', $id_kelurahan);
		$smarty->assign('nm_kelurahan', $nm_kelurahan);
		$smarty->assign('jml_kelurahan', $jml_kelurahan);
		$smarty->assign('jml_kelompok', $jml_kelompok);
		$smarty->display('lppm-add-kkn-kelompok.tpl');
	}
	else if(isset($_GET['act'])){
		$action = $_GET['act'];

		if($action=='prov'){
				$id_provinsi = array();
				$nm_provinsi = array();

				$db->Query("SELECT id_provinsi, nm_provinsi FROM provinsi WHERE id_negara = '114' ORDER BY nm_provinsi");
				$jml_provinsi = 0;
				$a = 0;
				while ($row = $db->FetchRow()){ 
					$id_provinsi[$a] = $row[0];
					$nm_provinsi[$a] = $row[1];
					$a++;
					$jml_provinsi++;
				}

				$smarty->assign('jml_provinsi', $jml_provinsi);
				$smarty->assign('id_provinsi', $id_provinsi);
				$smarty->assign('nm_provinsi', $nm_provinsi);
				$smarty->display('get-provinsi-kelompok-kkn.tpl');
		}
		else if($action=='kab'){
			if(isset($_GET['id_prov'])){
				$id_prov = $_GET['id_prov'];
				$sec++;
			}


			$id_kabupaten = array();
			$nm_kabupaten = array();

			$db->Query("select id_kota, kota from (
							SELECT id_kota, (tipe_dati2||' '||nm_kota) as kota FROM kota WHERE id_provinsi = '$id_prov')
						    order by kota");
			$jml_kabupaten = 0;
			$a = 0;
			while ($row = $db->FetchRow()){ 
				$id_kabupaten[$a] = $row[0];
				$nm_kabupaten[$a] = $row[1];
				$a++;
				$jml_kabupaten++;
			}

			$smarty->assign('jml_kabupaten', $jml_kabupaten);
			$smarty->assign('id_kabupaten', $id_kabupaten);
			$smarty->assign('nm_kabupaten', $nm_kabupaten);

			$smarty->display('get-kabupaten-kelompok-kkn.tpl');
		}
		else if($action=='kec'){
			if(isset($_GET['id_kab'])){
				$id_kab = $_GET['id_kab'];
				$sec++;
			}


			$id_kecamatan = array();
			$nm_kecamatan = array();

			$db->Query("SELECT id_kecamatan, nm_kecamatan FROM kecamatan WHERE id_kota = '$id_kab' ORDER BY nm_kecamatan");
			$jml_kabupaten = 0;
			$a = 0;
			while ($row = $db->FetchRow()){ 
				$id_kecamatan[$a] = $row[0];
				$nm_kecamatan[$a] = $row[1];
				$a++;
				$jml_kecamatan++;
			}

			$smarty->assign('jml_kecamatan', $jml_kecamatan);
			$smarty->assign('id_kecamatan', $id_kecamatan);
			$smarty->assign('nm_kecamatan', $nm_kecamatan);

			$smarty->display('get-kecamatan-kelompok-kkn.tpl');
		}
	}
?>