<?php
	include 'config.php';


$id_pt = $id_pt_user;


if ($user->Role() != AUCC_ROLE_LPPM){
	header("location: /logout.php");
    exit();
}
	$id_kkn_kelompok = array();
	$nm_kkn_kelompok = array();
	$id_kelurahan = array();
	$nm_kelurahan = array();
	$quota = array();
	$jenis_kkn = array();
	
	$db->Query("select kel.id_kkn_kelompok, kec.id_kecamatan, kel.id_kelurahan, kel.nama_kelompok, des.nm_kelurahan, kel.quota, kel.jenis_kkn, kec.nm_kecamatan 
                                from kkn_kelompok kel
				left join kelurahan des on des.id_kelurahan = kel.id_kelurahan
				left join kecamatan kec on kec.id_kecamatan = des.id_kecamatan
				left join kkn_angkatan ka on kel.id_kkn_angkatan = ka.id_kkn_angkatan
				where ka.status_aktif ='1' and kel.id_semester in (select id_semester from semester where status_aktif_semester = 'True' and id_perguruan_tinggi = '{$id_pt}')
				order by kel.id_kkn_kelompok
	");

	
	$i = 0;
	$jml_data = 0;
	while ($row = $db->FetchRow()){ 
		$id_kkn_kelompok[$i] = $row[0];
		$id_kecamatan[$i] = $row[1];
		$id_kelurahan[$i] = $row[2];
		$nm_kkn_kelompok[$i] = htmlentities($row[3]);
		$nm_kelurahan[$i] = $row[4];
		$quota[$i] = $row[5];
		$jenis_kkn[$i] = $row[6];
		$nm_kecamatan[$i] = $row[7];
		$jml_data++;
		$i++;
	}

	$smarty->assign('jml_data', $jml_data);
	$smarty->assign('id_kkn_kelompok', $id_kkn_kelompok);
	$smarty->assign('id_kecamatan', $id_kecamatan);
	$smarty->assign('nm_kkn_kelompok', $nm_kkn_kelompok);
	$smarty->assign('id_kelurahan', $id_kelurahan);
	$smarty->assign('nm_kelurahan', $nm_kelurahan);
	$smarty->assign('quota', $quota);
	$smarty->assign('jenis_kkn', $jenis_kkn);
	$smarty->assign('nm_kecamatan', $nm_kecamatan);
	$smarty->display('lppm-kkn-kelompok-display.tpl');
?>