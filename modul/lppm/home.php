<div class="center_title_bar">LPPM Home</div>
<div id="lppm-home">
<?php
	
	$options = array(
		CURLOPT_URL				=> 'http://lppmua.unair.ac.id',
		CURLOPT_RETURNTRANSFER	=> TRUE,
		CURLOPT_HEADER			=> FALSE
	);
	
	// Eksekusi url
	$ch = curl_init();
	curl_setopt_array($ch, $options);
	$curl_response = curl_exec($ch);
	curl_close($ch);
	
	// left tag
	$left_tag = "<!-- Main Column -->";
	$right_tag = "<!-- / Main Column -->";
	$left_position = strpos($curl_response, $left_tag) + strlen($left_tag) + 1;
	$right_position = strpos($curl_response, $right_tag);
	$html_lppm = substr($curl_response, $left_position, $right_position - $left_position);
	$html_lppm = str_replace("<a ", "<a class=\"disable-ajax\" target=\"_blank\"", $html_lppm);
	echo $html_lppm;
	
?>
</div>