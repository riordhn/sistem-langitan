<?php
	include 'config.php';
	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}

	$sec = 0;
	if(isset($_GET['id_dosen'])){
		$id_dosen = $_GET['id_dosen'];
		$sec++;
	}
	if(isset($_GET['i'])){
		$i = $_GET['i'];
		$sec++;
	}

	if($sec==2){

		$id_provinsi = array();
		$nm_provinsi = array();

		$db->Query("SELECT id_provinsi, nm_provinsi FROM provinsi WHERE id_negara = '114' ORDER BY id_provinsi");
		$jml_provinsi = 0;
		$a = 0;
		while ($row = $db->FetchRow()){ 
			$id_provinsi[$a] = $row[0];
			$nm_provinsi[$a] = $row[1];
			$a++;
			$jml_provinsi++;
		}

		$smarty->assign('jml_provinsi', $jml_provinsi);
		$smarty->assign('id_provinsi', $id_provinsi);
		$smarty->assign('nm_provinsi', $nm_provinsi);

		$smarty->assign('i', $i);
		$smarty->assign('id_dosen', $id_dosen);
		$smarty->display('get-provinsi.tpl');
	}
?>