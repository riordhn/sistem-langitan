<?php
	include 'config.php';
	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}
	
	if(isset($_GET['id_propinsi'])){
		$id_propinsi = $_GET['id_propinsi'];
		$id_kota = array();
		$nm_kota = array();
		
		$db->Query("SELECT id_kota, nm_kota FROM kota WHERE id_provinsi = '$id_propinsi' order by id_kota");
		$jml_kota = 0;
		$i = 0;
		while ($row = $db->FetchRow()){ 
			$id_kota[$i] = $row[0];
			$nm_kota[$i] = $row[1];
			$i++;
			$jml_kota++;
		}
		
		$smarty->assign('jml_kota', $jml_kota);
		$smarty->assign('id_kota', $id_kota);
		$smarty->assign('nm_kota', $nm_kota);
		$smarty->display('get-kota.tpl');
	}
?>