<?php
	include 'config.php';
	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}

	$sec = 0;
	if(isset($_GET['id_dosen'])){
		$id_dosen = $_GET['id_dosen'];
		$sec++;
	}
	if(isset($_GET['i'])){
		$i = $_GET['i'];
		$sec++;
	}
	if(isset($_GET['id_prov'])){
		$id_prov = $_GET['id_prov'];
		$sec++;
	}


	if($sec==3){

		$id_kabupaten = array();
		$nm_kabupaten = array();

		$db->Query("select id_kota, kota from (
							SELECT id_kota, (tipe_dati2||' '||nm_kota) as kota FROM kota WHERE id_provinsi = '$id_prov')
						    order by kota");
		$jml_kabupaten = 0;
		$a = 0;
		while ($row = $db->FetchRow()){ 
			$id_kabupaten[$a] = $row[0];
			$nm_kabupaten[$a] = $row[1];
			$a++;
			$jml_kabupaten++;
		}

		$smarty->assign('jml_kabupaten', $jml_kabupaten);
		$smarty->assign('id_kabupaten', $id_kabupaten);
		$smarty->assign('nm_kabupaten', $nm_kabupaten);

		$smarty->assign('i', $i);
		$smarty->assign('id_dosen', $id_dosen);
		$smarty->display('get-kabupaten-korkab.tpl');
	}
	
?>