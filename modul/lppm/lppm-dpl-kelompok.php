<?php
	include 'config.php';

	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}
	
	$fakultas = $_GET['fakultas'];
	$id_prodi = array();
	$nm_prodi = array();
	$nm_dosen = array();
	
	$db->Query("select dpl.id_dosen, p.nm_pengguna, ps.nm_program_studi from kkn_dpl dpl
				left join dosen d on d.id_dosen = dpl.id_dosen
				left join program_studi ps on ps.id_program_studi = d.id_program_studi
				left join pengguna p on p.id_pengguna = d.id_pengguna
				order by dpl.id_kkn_dpl
	");

	$i = 0;
	$jml_data = 0;
	while ($row = $db->FetchRow()){ 
		$id_prodi[$i] = $row[0];
		$nm_dosen[$i] = $row[1];
		$nm_prodi[$i] = $row[2];
		$jml_data++;
		$i++;
	}
	
	$smarty->assign('jml_data', $jml_data);
	$smarty->assign('id_prodi', $id_prodi);
	$smarty->assign('nm_prodi', $nm_prodi);
	$smarty->assign('nm_dosen', $nm_dosen);
	$smarty->display('lppm-dpl-display.tpl');
?>