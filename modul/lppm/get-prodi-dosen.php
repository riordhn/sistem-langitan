<?php
	include 'config.php';
	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}
	
	if(isset($_GET['fakultas'])){
		
		$fakultas = $_GET['fakultas'];
		$id_prodi = array();
		$nm_prodi = array();
		$jenjang = array();
		
		$db->Query("select ps.id_program_studi, ps.nm_program_studi, j.nm_jenjang from program_studi ps
					left join jenjang j on j.id_jenjang = ps.id_jenjang
					where ps.id_fakultas = '$fakultas' and ps.id_jenjang = '1' order by id_program_studi");

		$i = 0;
		$jml_prodi = 0;
		while ($row = $db->FetchRow()){ 
			$id_prodi[$i] = $row[0];
			$nm_prodi[$i] = $row[1];
			$jenjang[$i] = $row[2];
			$jml_prodi++;
			$i++;
		}
		
		$smarty->assign('jml_prodi', $jml_prodi);
		$smarty->assign('id_prodi', $id_prodi);
		$smarty->assign('nm_prodi', $nm_prodi);
		$smarty->assign('jenjang', $jenjang);
		$smarty->display('get-prodi-dosen.tpl');
	}
?>