<?php
	include 'config.php';
	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}
	
	$id_propinsi = array();
	$nm_propinsi = array();
	
	$db->Query("SELECT id_provinsi, nm_provinsi FROM provinsi WHERE id_negara = '114' order by id_provinsi");
	$jml_propinsi = 0;
	$i = 0;
	while ($row = $db->FetchRow()){ 
		$id_propinsi[$i] = $row[0];
		$nm_propinsi[$i] = $row[1];
		$i++;
		$jml_propinsi++;
	}
	
	$smarty->assign('jml_propinsi', $jml_propinsi);
	$smarty->assign('id_propinsi', $id_propinsi);
	$smarty->assign('nm_propinsi', $nm_propinsi);
	$smarty->display('get-propinsi.tpl');
?>