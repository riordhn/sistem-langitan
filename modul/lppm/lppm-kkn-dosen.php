<?php
    include 'config.php';

    $id_pt = $id_pt_user;

    if ($user->Role() != AUCC_ROLE_LPPM) {
        header("location: /logout.php");
        exit();
    }

    $id_fakultas = array();
    $nm_fakultas = array();

    $db->Query("select id_fakultas, nm_fakultas from fakultas where id_perguruan_tinggi = '{$id_pt}' order by id_fakultas");

    $i = 0;
    $jml_data = 0;
    while ($row = $db->FetchRow()) {
        $id_fakultas[$i] = $row[0];
        $nm_fakultas[$i] = $row[1];
        $jml_data++;
        $i++;
    }

    $db->Query("select dos.id_pengguna, p.username, p.nm_pengguna, ps.nm_program_studi, j.nm_jenjang from dosen dos
				left join pengguna p on p.id_pengguna = dos.id_pengguna
				left join program_studi ps on ps.id_program_studi = dos.id_program_studi
				left join jenjang j on j.id_jenjang = ps.id_jenjang
                where p.id_perguruan_tinggi = {$id_pt}
				-- where p.id_role=4
				order by p.nm_pengguna
	");

    $id_pengguna = array();
    $username = array();
    $nm_pengguna = array();
    $prodi = array();
    $jenjang = array();
    $label = array();
    $jml_dosen = 0;

    $i = 0;
    while ($row = $db->FetchRow()) {
        $id_pengguna[$i] = $row[0];
        $username[$i] = $row[1];

        $prodi[$i] = $row[3];
        $jenjang[$i] = $row[4];

        $label[$i] = $nm_pengguna[$i] . ' [ ' . $jenjang[$i] . ' ]' . ' ' . $prodi[$i];
        $nm_pengguna[$i] = '"' . $row[2] . ' [ ' . $prodi[$i] . ' ]' . '"' . ',';
        $jml_dosen++;
        $i++;
    }

    $smarty->assign('jml_dosen', $jml_dosen);
    $smarty->assign('nm_pengguna', $nm_pengguna);
    $smarty->assign('label', $label);

    $smarty->assign('jml_data', $jml_data);
    $smarty->assign('id_fakultas', $id_fakultas);
    $smarty->assign('nm_fakultas', $nm_fakultas);


    $smarty->display('lppm-kkn-dosen.tpl');
?>

<script>
    $(function() {

        //var availableTags = {for $i=0 to $jml_dosen-1}{$nm_pengguna}]{/for};
        var availableTags = [
<?php for ($i = 0; $i < $jml_dosen - 1; $i++) {
        echo $nm_pengguna[$i];
    } ?>
        ];
        $('#pesan').autosize();
        $('#cari').tagit({tagSource: availableTags, select: true, triggerKeys: ['enter', 'comma', 'tab']});

    });
</script>