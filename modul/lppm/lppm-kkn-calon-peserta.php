<?php

include 'config.php';

$id_pt = $id_pt_user;

$db->Query("SELECT * FROM KKN_ANGKATAN ka JOIN SEMESTER s ON s.ID_SEMESTER = ka.ID_SEMESTER WHERE ka.STATUS_AKTIF='1' AND s.ID_PERGURUAN_TINGGI = '{$id_pt}'");
$angkatan = $db->FetchAssoc();

if (get('mode') != '') {
    if (get('mode') == 'view') {
        $fakultas = get('fakultas');
        $semester = get('semester');
        if (isset($_POST)) {
            $jenis = post('jenis');
            if (post('mode') == 'proses') {
                for ($i = 1; $i <= post('jumlah'); $i++) {
                    $mhs = post('mhs' . $i);
                    $pmk = post('pmk' . $i);
                    if (post('peserta' . $i) != '') {
                        $query = "
                            INSERT INTO KKN_KELOMPOK_MHS 
                                (ID_MHS,JENIS_KKN,TERJUN,ID_PENGAMBILAN_MK,ID_KKN_ANGKATAN)
                            VALUES
                                ('{$mhs}','{$jenis}','1','{$pmk}','{$angkatan['ID_KKN_ANGKATAN']}')";
                        $db->Query($query);
                    } else if (post('delete-peserta' . $i) != '') {
                        $kkm = post('kkm' . $i);
                        $query = "DELETE FROM KKN_KELOMPOK_MHS WHERE ID_KKN_KELOMPOK_MHS='{$kkm}'";
                        $db->Query($query);
                    }
                }
            }
        }
        $query_mhs = "
            SELECT M.ID_MHS,P.NM_PENGGUNA,M.NIM_MHS,M.THN_ANGKATAN_MHS,M.STATUS,PS.NM_PROGRAM_STUDI
            ,J.NM_JENJANG,JAL.NM_JALUR,PMK.NILAI_HURUF,PMK.NILAI_ANGKA,P.KELAMIN_PENGGUNA,PMK.ID_PENGAMBILAN_MK,
            KKM.ID_KKN_KELOMPOK_MHS,
            (
                CASE 
                WHEN KKM.JENIS_KKN=1 THEN 'Reguler'
                WHEN KKM.JENIS_KKN=2 THEN 'Tematik'
                WHEN KKM.JENIS_KKN=3 THEN 'Magang/Khusus'
                ELSE ''
                END 
            ) JENIS_KKN_MHS, KA.NAMA_ANGKATAN,KK.NAMA_KELOMPOK
            FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND JM.IS_JALUR_AKTIF='1'
            JOIN JALUR JAL ON JM.ID_JALUR=JAL.ID_JALUR
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG= PS.ID_JENJANG
            JOIN PENGAMBILAN_MK PMK ON PMK.ID_MHS=M.ID_MHS
            JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=PMK.ID_KURIKULUM_MK
            JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH=KUMK.ID_MATA_KULIAH
            LEFT JOIN KKN_KELOMPOK_MHS KKM ON KKM.ID_MHS=M.ID_MHS
            LEFT JOIN KKN_KELOMPOK KK ON KK.ID_KKN_KELOMPOK=KKM.ID_KKN_KELOMPOK
            LEFT JOIN KKN_ANGKATAN KA ON KA.ID_KKN_ANGKATAN = KKM.ID_KKN_ANGKATAN
            WHERE (KUMK.STATUS_MKTA= 2 OR (MK.NM_MATA_KULIAH LIKE ('%KKN%') OR UPPER(MK.NM_MATA_KULIAH) LIKE '%KERJA NYATA%'))
            AND PMK.STATUS_APV_PENGAMBILAN_MK = 1
            AND PMK.ID_SEMESTER='{$semester}'
            AND PS.ID_FAKULTAS='{$fakultas}'
            ORDER BY M.NIM_MHS,M.STATUS,JAL.NM_JALUR,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
            -- OR pmk.nilai_huruf = 'E' OR pmk.nilai_angka = 0)
            ";
        $mahasiswa = $db->QueryToArray($query_mhs);
        $query_gender = "
            SELECT PS.ID_FAKULTAS,
            SUM(
                CASE
                WHEN P.KELAMIN_PENGGUNA=1 THEN 1
                ELSE 0
                END
            ) LAKI,
            SUM(
                CASE
                WHEN P.KELAMIN_PENGGUNA=2 THEN 1
                ELSE 0
                END
            ) WANITA
            FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG= PS.ID_JENJANG
            JOIN PENGAMBILAN_MK PMK ON PMK.ID_MHS=M.ID_MHS
            JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=PMK.ID_KURIKULUM_MK
            WHERE KUMK.STATUS_MKTA= 2 AND PMK.STATUS_APV_PENGAMBILAN_MK = 1 
            AND PMK.ID_SEMESTER='{$semester}'
            AND PS.ID_FAKULTAS='{$fakultas}'
            GROUP BY PS.ID_FAKULTAS
            ";
        $db->Query($query_gender);
        $gender = $db->FetchAssoc();
        $smarty->assign('data_gender', $gender);
        $smarty->assign('data_mhs', $mahasiswa);
    }
}

$query_semester = "
    SELECT * FROM SEMESTER 
    WHERE TIPE_SEMESTER='REG' 
    AND THN_AKADEMIK_SEMESTER BETWEEN TO_CHAR(sysdate, 'YYYY')-5
    AND TO_CHAR(sysdate, 'YYYY')+3
    AND ID_PERGURUAN_TINGGI = '{$id_pt}'
    ORDER BY TAHUN_AJARAN DESC, NM_SEMESTER DESC";
$semester = $db->QueryToArray($query_semester);
$query_fakultas = "SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$id_pt}' ORDER BY ID_FAKULTAS";
$fakultas = $db->QueryToArray($query_fakultas);


$smarty->assign('data_fakultas', $fakultas);
$smarty->assign('data_semester', $semester);
$smarty->display("kkn/calon-peserta.tpl");
?>
