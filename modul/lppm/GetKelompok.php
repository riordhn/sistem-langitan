<?php

include 'config.php';
include 'class/ploting.class.php';

$p = new ploting($db);
$kel_lama = get('kel_lama');
$kel_baru = get('kel_baru');
$jk_lama = get('jk_lama');
$mhs_lama = get('mhs_lama');
$cari = get('cari');
$data_kelompok = $p->LoadKKNKelompokMhs($kel_baru);
echo "
    <form method='post' action='pindah-lokasi.php?mode=cari&cari={$cari}'>
    <table style='width:100%'>
        <tr>
            <th colspan='7'>KELOMPOK KKN YANG DI PILIH</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>NAMA</th>
            <th>NIM</th>
            <th>JENIS KELAMIN</th>
            <th>PRODI</th>
            <th>FAKULTAS</th>
            <th>OPERASI</th>
        </tr>
    ";
$no = 1;
foreach ($data_kelompok as $k) {
    $jk = $k['KELAMIN_PENGGUNA'] == 1 ? "Laki-Laki" : "Perempuan";
    $op = $k['KELAMIN_PENGGUNA'] == $jk_lama ? "<input type='radio' name='mhs_baru' value='{$k['ID_MHS']}'/>" : "";
    echo "
        <tr>
            <td>{$no}</td>
            <td>{$k['NM_PENGGUNA']}</td>
            <td>{$k['NIM_MHS']}</td>
            <td>{$jk}</td>
            <td>{$k['NM_JENJANG']} {$k['NM_PROGRAM_STUDI']}</td>
            <td>{$k['NM_FAKULTAS']}</td>
            <td style='text-align:center'>{$op}</td>
        </tr>
        ";
    $no++;
}
echo "
    <tr>
        <td colspan='7' style='text-align:center'>
            <input type='hidden' name='mode' value='ubah' />
            <input type='hidden' name='mhs_lama' value='{$mhs_lama}' />
            <input type='hidden' name='kel_lama' value='{$kel_lama}' />
            <input type='hidden' name='kel_baru' value='{$kel_baru}' />
            <input type='submit' value='Pilih' style='padding: 5px' class='ui-button ui-corner-all ui-state-default' />
        </td>
    </tr>
    </table>
    </form>";
?>
