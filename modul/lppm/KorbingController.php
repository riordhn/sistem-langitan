<?php
	include 'config.php';
	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}
	
	if(isset($_POST['act'])){ // POST METHOD

		$action = $_POST['act'];

		if($action=='simpan'){
			
			$id_dosen = (int)$_POST['id_dosen'];
			$id_semester = $db->QuerySingle("SELECT id_semester FROM kkn_angkatan WHERE status_aktif = '1'");

			if(isset($_POST['id_kecamatan'])){
				//$id_kelurahan = (int)$_POST['id_kelurahan'];
				$id_kecamatan = (int)$_POST['id_kecamatan'];
				
				$db->Query("SELECT id_kkn_korbing FROM kkn_korbing WHERE id_dosen = '$id_dosen'");
				$is_korbing = 0;
				while ($row = $db->FetchRow()){ 
					$is_korbing++;
				}

				if($is_korbing<2){
					//$db->Query("INSERT INTO kkn_korbing (id_dosen, id_kelurahan, id_semester) values ('$id_dosen', '$id_kelurahan', '$id_semester')");
					$db->Query("INSERT INTO kkn_korbing (id_dosen, id_kecamatan, id_semester) values ('$id_dosen', '$id_kecamatan', '$id_semester')");
					echo "status:success";
				}else{
					echo $is_korbing;
				}
			}
		}

		else if($action=='delete'){
			$id_dosen = $_POST['id_dosen'];
			$id_korbing = $_POST['id_korbing'];
			$db->Query("DELETE FROM kkn_korbing WHERE id_kkn_korbing = '$id_korbing'");
		}

	}else if(isset($_GET['act'])){ // GET METHOD
		$action = $_GET['act'];

		if($action=='status'){ // IF action == status / get status dosen korbing
			$id_dosen = $_GET['id_dosen'];
			/*
			$db->Query("SELECT korbing.id_kkn_korbing, korbing.id_dosen, kel.id_kelurahan, kel.nm_kelurahan FROM kkn_korbing korbing
						LEFT JOIN kelurahan kel on kel.id_kelurahan = korbing.id_kelurahan
						WHERE korbing.id_dosen = '$id_dosen'
			");
			*/
			$db->Query("SELECT korbing.id_kkn_korbing, kec.nm_kecamatan FROM kkn_korbing korbing
						LEFT JOIN kecamatan kec on kec.id_kecamatan = korbing.id_kecamatan
						WHERE korbing.id_dosen = '$id_dosen'
			");
			
			$a = 0;
			$jml_kelurahan = 0;
			$kelurahan = array();
			$id_korbing = array();

			while ($row = $db->FetchRow()){
				$id_korbing[$a] = $row[0];
				$kelurahan[$a] = $row[1];
				$jml_kelurahan++;
				$a++;
			}
			
			$smarty->assign('jml_kelurahan', $jml_kelurahan);
			$smarty->assign('id_dosen', $id_dosen);
			$smarty->assign('id_korbing', $id_korbing);
			$smarty->assign('kelurahan', $kelurahan);
			$smarty->display('get-status-korbing.tpl');

		}

		else if($action=='prov'){
			$sec = 0;
			if(isset($_GET['id_dosen'])){
				$id_dosen = $_GET['id_dosen'];
				$sec++;
			}
			if(isset($_GET['i'])){
				$i = $_GET['i'];
				$sec++;
			}

			if($sec==2){

				$id_provinsi = array();
				$nm_provinsi = array();

				$db->Query("SELECT id_provinsi, nm_provinsi FROM provinsi WHERE id_negara = '114' ORDER BY nm_provinsi");
				$jml_provinsi = 0;
				$a = 0;
				while ($row = $db->FetchRow()){ 
					$id_provinsi[$a] = $row[0];
					$nm_provinsi[$a] = $row[1];
					$a++;
					$jml_provinsi++;
				}

				$smarty->assign('jml_provinsi', $jml_provinsi);
				$smarty->assign('id_provinsi', $id_provinsi);
				$smarty->assign('nm_provinsi', $nm_provinsi);

				$smarty->assign('i', $i);
				$smarty->assign('id_dosen', $id_dosen);
				$smarty->display('get-provinsi-korbing.tpl');
			}
		}

		else if($action=='kab'){ // IF action == kab/kabupaten
			$sec = 0;
			if(isset($_GET['id_dosen'])){
				$id_dosen = $_GET['id_dosen'];
				$sec++;
			}
			if(isset($_GET['i'])){
				$i = $_GET['i'];
				$sec++;
			}
			if(isset($_GET['id_prov'])){
				$id_prov = $_GET['id_prov'];
				$sec++;
			}


			if($sec==3){

				$id_kabupaten = array();
				$nm_kabupaten = array();

				$db->Query("select id_kota, kota from (
							SELECT id_kota, (tipe_dati2||' '||nm_kota) as kota FROM kota WHERE id_provinsi = '$id_prov')
						    order by kota
				");
				$jml_kabupaten = 0;
				$a = 0;
				while ($row = $db->FetchRow()){ 
					$id_kabupaten[$a] = $row[0];
					$nm_kabupaten[$a] = $row[1];
					$a++;
					$jml_kabupaten++;
				}

				$smarty->assign('jml_kabupaten', $jml_kabupaten);
				$smarty->assign('id_kabupaten', $id_kabupaten);
				$smarty->assign('nm_kabupaten', $nm_kabupaten);

				$smarty->assign('i', $i);
				$smarty->assign('id_dosen', $id_dosen);
				$smarty->display('get-kabupaten-korbing.tpl');
			}
		}

		else if($action=='kec'){ // IF action == kecamatan
			$sec = 0;
			if(isset($_GET['id_dosen'])){
				$id_dosen = $_GET['id_dosen'];
				$sec++;
			}
			if(isset($_GET['i'])){
				$i = $_GET['i'];
				$sec++;
			}
			if(isset($_GET['id_kab'])){
				$id_kab = $_GET['id_kab'];
				$sec++;
			}


			if($sec==3){

				$id_kecamatan = array();
				$nm_kecamatan = array();

				$db->Query("SELECT id_kecamatan, nm_kecamatan FROM kecamatan WHERE id_kota = '$id_kab' ORDER BY nm_kecamatan");
				$jml_kabupaten = 0;
				$a = 0;
				while ($row = $db->FetchRow()){ 
					$id_kecamatan[$a] = $row[0];
					$nm_kecamatan[$a] = $row[1];
					$a++;
					$jml_kecamatan++;
				}

				$smarty->assign('jml_kecamatan', $jml_kecamatan);
				$smarty->assign('id_kecamatan', $id_kecamatan);
				$smarty->assign('nm_kecamatan', $nm_kecamatan);

				$smarty->assign('i', $i);
				$smarty->assign('id_dosen', $id_dosen);
				$smarty->display('get-kecamatan-korbing.tpl');
			}
		}

		else if($action=='kel'){ // IF action == kab/kabupaten
			$sec = 0;
			if(isset($_GET['id_dosen'])){
				$id_dosen = $_GET['id_dosen'];
				$sec++;
			}
			if(isset($_GET['i'])){
				$i = $_GET['i'];
				$sec++;
			}
			if(isset($_GET['id_kec'])){
				$id_kec = $_GET['id_kec'];
				$sec++;
			}


			if($sec==3){

				$id_kelurahan = array();
				$nm_kelurahan = array();

				$db->Query("SELECT id_kelurahan, nm_kelurahan FROM kelurahan WHERE id_kecamatan = '$id_kec' ORDER BY id_kelurahan");
				$jml_kabupaten = 0;
				$a = 0;
				while ($row = $db->FetchRow()){ 
					$id_kelurahan[$a] = $row[0];
					$nm_kelurahan[$a] = $row[1];
					$a++;
					$jml_kelurahan++;
				}

				$smarty->assign('jml_kelurahan', $jml_kelurahan);
				$smarty->assign('id_kelurahan', $id_kelurahan);
				$smarty->assign('nm_kelurahan', $nm_kelurahan);

				$smarty->assign('i', $i);
				$smarty->assign('id_dosen', $id_dosen);
				$smarty->display('get-kelurahan-korbing.tpl');
			}
		}

	}
?>