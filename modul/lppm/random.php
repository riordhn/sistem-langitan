<?php
	include 'config.php';
	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}
	
	$data = $db->QueryToArray("
		SELECT
			A.id_mhs,
			c.id_fakultas
		FROM
			mahasiswa A
		LEFT JOIN program_studi b ON b.ID_PROGRAM_STUDI = A .ID_PROGRAM_STUDI
		LEFT JOIN fakultas c ON c.id_fakultas = b.id_fakultas
		ORDER BY
			c.id_fakultas
	");
	
	print json_encode($data);
	
?>