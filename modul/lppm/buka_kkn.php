<?php
	include 'config.php';

	$id_pt = $id_pt_user;
	
	if(isset($_GET['confirm'])){
		
		$id_kelompok = (int)$_GET['kel'];
		
		$kelompok = $db->QueryToArray("select kel.id_kkn_kelompok, kec.id_kecamatan, kel.id_kelurahan, kel.nama_kelompok, des.nm_kelurahan, kel.quota, kel.jenis_kkn, kel.status_publish from kkn_kelompok kel
					left join kelurahan des on des.id_kelurahan = kel.id_kelurahan
					left join kecamatan kec on kec.id_kecamatan = des.id_kecamatan
					left join semester smt on smt.id_semester = kel.id_semester
					where smt.status_aktif_semester ='True' and smt.id_perguruan_tinggi = '{$id_pt}' and kel.id_kkn_kelompok = '$id_kelompok'");
		
		$smarty->assign('id_kelompok', $id_kelompok);
		$smarty->assign('kelompok', $kelompok);
		$smarty->display('buka_kkn_confirm.tpl');
	}
	else if(isset($_GET['unpublish'])){
		$id_kelompok = (int)$_GET['kel'];
		
		$kelompok = $db->QueryToArray("select kel.id_kkn_kelompok, kec.id_kecamatan, kel.id_kelurahan, kel.nama_kelompok, des.nm_kelurahan, kel.quota, kel.jenis_kkn, kel.status_publish from kkn_kelompok kel
					left join kelurahan des on des.id_kelurahan = kel.id_kelurahan
					left join kecamatan kec on kec.id_kecamatan = des.id_kecamatan
					left join semester smt on smt.id_semester = kel.id_semester
					where smt.status_aktif_semester ='True' and smt.id_perguruan_tinggi = '{$id_pt}' and kel.id_kkn_kelompok = '$id_kelompok'");
		
		$smarty->assign('id_kelompok', $id_kelompok);
		$smarty->assign('kelompok', $kelompok);
		$smarty->display('buka_kkn_unpublish.tpl');
	}
	else{
	
		if(isset($_POST['kelompok'])){
			$id_kelompok = (int)$_POST['kelompok'];
			$publish = (int)$_POST['publish'];
			$db->Query("update kkn_kelompok set status_publish = '$publish' where id_kkn_kelompok = '$id_kelompok'");
			
		}
		
		$kelompok = $db->QueryToArray("select kel.id_kkn_kelompok, kec.id_kecamatan, kel.id_kelurahan, kel.nama_kelompok, des.nm_kelurahan, kel.quota, kel.jenis_kkn, kel.status_publish from kkn_kelompok kel
					left join kelurahan des on des.id_kelurahan = kel.id_kelurahan
					left join kecamatan kec on kec.id_kecamatan = des.id_kecamatan
					left join semester smt on smt.id_semester = kel.id_semester
					where smt.status_aktif_semester ='True' and smt.id_perguruan_tinggi = '{$id_pt}' and kel.jenis_kkn = 1
					order by kel.id_kkn_kelompok");
		
		
		$smarty->assign('kelompok', $kelompok);
		$smarty->display('buka_kkn.tpl');
	}
?>