<?php
include 'config.php';

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

if ($user->Role() != AUCC_ROLE_LPPM){
    header("location: /logout.php");
    exit();
}

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_kkn_angkatan = post('id_kkn_angkatan');
        $kode_angkatan = post('kode_angkatan');
        $nama_angkatan = post('nama_angkatan');       
        
        $result = $db->Query("update kkn_angkatan set kode_angkatan = '{$kode_angkatan}', 
                                nama_angkatan = '{$nama_angkatan}' 
        						where id_kkn_angkatan = '{$id_kkn_angkatan}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }
    if (post('mode') == 'add')
    {
        $kode_angkatan = post('kode_angkatan');
        $nama_angkatan = post('nama_angkatan'); 
        $id_semester = post('id_semester');    
        
        $result = $db->Query("insert into kkn_angkatan (id_semester, kode_angkatan, nama_angkatan, status_aktif) 
        						values ('{$id_semester}','{$kode_angkatan}','{$nama_angkatan}','0')");

        $result1 = $db->Query("update kkn_angkatan set status_aktif = '0' where id_semester in (select id_semester from semester where status_aktif_semester = 'False' and id_perguruan_tinggi = '{$id_pt}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }
    if (post('mode') == 'delete')
    {
        $id_kkn_angkatan = post('id_kkn_angkatan');   
        
        $result = $db->Query("update kkn_angkatan set status_aktif = '0' 
                                where id_kkn_angkatan = '{$id_kkn_angkatan}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }
    if (post('mode') == 'aktifkan')
    {
        $id_kkn_angkatan = post('id_kkn_angkatan'); 

        $db->Query("SELECT COUNT(*) AS JML 
                        FROM KKN_ANGKATAN ka
                        JOIN SEMESTER s ON s.ID_SEMESTER = ka.ID_SEMESTER
                        WHERE ka.STATUS_AKTIF = '1' 
                            AND s.STATUS_AKTIF_SEMESTER = 'True' 
                            AND s.ID_PERGURUAN_TINGGI = '{$id_pt}'
                            AND ka.ID_KKN_ANGKATAN <> '{$id_kkn_angkatan}'");
        $jml = $db->FetchAssoc();

        $jml_angkatan = $jml['JML'];           
        
        if($jml_angkatan > 0){
            $smarty->assign('edited', "Terdapat Angkatan KKN Aktif Yang Lain!");
        }
        else{
            $result = $db->Query("update kkn_angkatan set status_aktif = '1' 
                                where id_kkn_angkatan = '{$id_kkn_angkatan}'");

            $smarty->assign('edited', $result ? "Data berhasil diaktifkan" : "Data gagal diaktifkan");
        }
        
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$kkn_angkatan_set = $db->QueryToArray("select id_kkn_angkatan, kode_angkatan, nama_angkatan,ka.status_aktif,ka.id_semester,
            s.tahun_ajaran || ' (' || s.nm_semester || ')' AS NM_SEMESTER, 
            (SELECT COUNT(*) FROM KKN_KELOMPOK WHERE ID_KKN_ANGKATAN = ka.ID_KKN_ANGKATAN) AS RELASI
            from kkn_angkatan ka
            join semester s on s.id_semester = ka.id_semester
            where ka.id_semester in (select id_semester from semester where status_aktif_semester = 'True' and id_perguruan_tinggi = '{$id_pt}')
            order by kode_angkatan");
		$smarty->assignByRef('kkn_angkatan_set', $kkn_angkatan_set);
	}
	else if ($mode == 'edit' or $mode == 'delete_aktif')
	{
		$id_kkn_angkatan 	= (int)get('id_kkn_angkatan', '');

        // get deleted khusus mode delete_aktif
        $deleted    = (int)get('deleted', '');
        if(!empty($deleted)){
            $smarty->assign('deleted', $deleted);
        }

		$kkn_angkatan = $db->QueryToArray("
            select * 
            from kkn_angkatan 
            where id_kkn_angkatan = {$id_kkn_angkatan}");
        $smarty->assign('kkn_angkatan', $kkn_angkatan[0]);

        /*$db->Query("SELECT TYPE_UNIT_KERJA FROM UNIT_KERJA WHERE ID_PERGURUAN_TINGGI = '{$id_pt}'");
        $type_unit_kerja = $db->FetchAssoc();

        $isi_type = $type_unit_kerja['TYPE_UNIT_KERJA'];

        $smarty->assign('isi_type', $isi_type);*/

        $db->Query("SELECT ID_SEMESTER, TAHUN_AJARAN || ' (' || NM_SEMESTER || ')' AS NM_SEMESTER 
                        FROM semester 
                        WHERE status_aktif_semester = 'True' and id_perguruan_tinggi = '{$id_pt}'");
        $semester = $db->FetchAssoc();
        $smarty->assign('id_semester', $semester['ID_SEMESTER']);
        $smarty->assign('nm_semester', $semester['NM_SEMESTER']);

	}
	else if($mode == 'add')
	{
		$db->Query("SELECT ID_SEMESTER, TAHUN_AJARAN || ' (' || NM_SEMESTER || ')' AS NM_SEMESTER 
                        FROM semester 
                        WHERE status_aktif_semester = 'True' and id_perguruan_tinggi = '{$id_pt}'");
        $semester = $db->FetchAssoc();
        $smarty->assign('id_semester', $semester['ID_SEMESTER']);
        $smarty->assign('nm_semester', $semester['NM_SEMESTER']);
	}
}

$smarty->display("kkn-angkatan/{$mode}.tpl");