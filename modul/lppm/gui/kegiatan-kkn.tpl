<div class="center_title_bar">KEGIATAN, PENGUMUMAN DAN BERITA KKN</div>
<form action="kegiatan-kkn.php" method="get" id="form">
    <table>
        <tr>
            <td>Pilih Angkatan : </td>
            <td>
                <select name="angkatan" onchange="$('#form').submit()">
                    <option>Pilih</option>
                    {foreach $kkn_angkatan as $ka}
                        <option value="{$ka.ID_KKN_ANGKATAN}" {if $smarty.get.angkatan==$ka.ID_KKN_ANGKATAN}selected="true"{/if}>{$ka.NAMA_ANGKATAN} {$ka.NM_SEMESTER} {$ka.TAHUN_AJARAN}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>
{if $smarty.get.angkatan}
    {if $smarty.get.mode=='tambah'}
        <form action="kegiatan-kkn.php?angkatan={$smarty.get.angkatan}" method="post">
            <table style="width: 90%">
                <tr>
                    <th colspan="2">FORM TAMBAH KEGIATAN
                        {foreach $kkn_angkatan as $ka}
                            {if $smarty.get.angkatan==$ka.ID_KKN_ANGKATAN}
                                {$ka.NAMA_ANGKATAN} {$ka.NM_SEMESTER} {$ka.TAHUN_AJARAN}
                            {/if}
                        {/foreach}
                    </th>
                </tr>
                <tr>
                    <td>Tipe Kegiatan</td>
                    <td>
                        <select name="tipe">
                            <option value="1">Pengumuman untuk Mahasiswa</option>
                            <option value="2">Kegiatan dengan batas periode</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Isi</td>
                    <td>
                        <textarea cols="180" rows="30" maxlength="1000" class="editor" name="isi"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>Periode</td>
                    <td>
                        Awal :<input type="text" name="tgl_awal" class="datepick" size="10" /> &Tab;
                        Akhir :<input type="text" name="tgl_akhir" class="datepick" size="10" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="center">
                        <input type="hidden" name="mode" value="tambah"/>
                        <input type="hidden" name="angkatan" value="{$smarty.get.angkatan}"/>
                        <input type="submit" value="Tambah" style="padding: 5px" class="ui-button ui-corner-all ui-state-default"/>
                        <a style="padding: 5px" class="ui-button ui-corner-all ui-state-default" href="kegiatan-kkn.php?angkatan={$smarty.get.angkatan}">Kembali</a>
                    </td>
                </tr>
            </table>
        </form>
        {literal}
            <script>
           $(document).ready(function() {

                    $('.editor').ckeditor(function(e){
                                    delete CKEDITOR.instances[$(e).attr('name')];
                            },{
                                    toolbar:
                                            [
                                                    ['TextColor','Style','Font','FontSize','Bold','Italic', 'JustifyLeft','JustifyCenter','JustifyRight', 'JustifyBlock', 'Underline','Strike','-','NumberedList','BulletedList', 'Outdent','Indent','-']
                                            ],
                                    skin: 'office2003'
                            }
                    ); 

            });
            $('.datepick').datepicker({dateFormat:'dd-M-y'});
            </script>
        {/literal}
    {else if $smarty.get.mode=='edit'}
        <form action="kegiatan-kkn.php?angkatan={$smarty.get.angkatan}" method="post">
            <table style="width: 90%">
                <tr>
                    <th colspan="2">FORM EDIT KEGIATAN
                        {foreach $kkn_angkatan as $ka}
                            {if $smarty.get.angkatan==$ka.ID_KKN_ANGKATAN}
                                {$ka.NAMA_ANGKATAN} {$ka.NM_SEMESTER} {$ka.TAHUN_AJARAN}
                            {/if}
                        {/foreach}
                    </th>
                </tr>
                <tr>
                    <td>Tipe Kegiatan</td>
                    <td>
                        <select name="tipe">
                            <option value="1" {if $keg.TIPE==1}selected="true"{/if}>Pengumuman untuk Mahasiswa</option>
                            <option value="2" {if $keg.TIPE==2}selected="true"{/if}>Kegiatan dengan batas periode</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Isi</td>
                    <td>
                        <textarea cols="180" rows="30" maxlength="1000" class="editor" name="isi">{$keg.ISI}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Periode</td>
                    <td>
                        Awal :<input type="text" name="tgl_awal" class="datepick" size="10" value="{$keg.TGL_AWAL}" /> &Tab;
                        Akhir :<input type="text" name="tgl_akhir" class="datepick" size="10" value="{$keg.TGL_AKHIR}" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="center">
                        <input type="hidden" name="mode" value="edit"/>
                        <input type="hidden" name="id" value="{$keg.ID_KKN_KEGIATAN}"/>
                        <input type="submit" value="Update" style="padding: 5px" class="ui-button ui-corner-all ui-state-default"/>
                        <a style="padding: 5px" class="ui-button ui-corner-all ui-state-default" href="kegiatan-kkn.php?angkatan={$smarty.get.angkatan}">Kembali</a>
                    </td>
                </tr>
            </table>
        </form>
        {literal}
            <script>
           $(document).ready(function() {

                    $('.editor').ckeditor(function(e){
                                    delete CKEDITOR.instances[$(e).attr('name')];
                            },{
                                    toolbar:
                                            [
                                                    ['TextColor','Style','Font','FontSize','Bold','Italic', 'JustifyLeft','JustifyCenter','JustifyRight', 'JustifyBlock', 'Underline','Strike','-','NumberedList','BulletedList', 'Outdent','Indent','-']
                                            ],
                                    skin: 'office2003'
                            }
                    ); 

            });
            $('.datepick').datepicker({dateFormat:'dd-M-y'});
            </script>
        {/literal}
    {else if $smarty.get.mode=='delete'}
        <form action="kegiatan-kkn.php?angkatan={$smarty.get.angkatan}" method="post">
            <table style="width: 90%">
                <tr>
                    <th colspan="2">FORM EDIT KEGIATAN
                        {foreach $kkn_angkatan as $ka}
                            {if $smarty.get.angkatan==$ka.ID_KKN_ANGKATAN}
                                {$ka.NAMA_ANGKATAN} {$ka.NM_SEMESTER} {$ka.TAHUN_AJARAN}
                            {/if}
                        {/foreach}
                    </th>
                </tr>
                <tr>
                    <td>Tipe Kegiatan</td>
                    <td>
                        {if $keg.TIPE==1}
                            Pengumuman untuk Mahasiswa
                        {else}
                            Kegiatan dengan batas periode
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td>Isi</td>
                    <td>
                        <div style="padding: 10px">{$keg.ISI}</div>
                    </td>
                </tr>
                <tr>
                    <td>Periode</td>
                    <td>
                        Awal :{$keg.TGL_AWAL}
                        Akhir :{$keg.TGL_AKHIR}
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="center">
                        <input type="hidden" name="mode" value="delete"/>
                        <input type="hidden" name="id" value="{$keg.ID_KKN_KEGIATAN}"/>
                        Apakah anda ingin menghapus ini ?<br/>
                        <input type="submit" value="Ya" style="padding: 5px" class="ui-button ui-corner-all ui-state-default"/>
                        <a style="padding: 5px" class="ui-button ui-corner-all ui-state-default" href="kegiatan-kkn.php?angkatan={$smarty.get.angkatan}">Kembali</a>
                    </td>
                </tr>
            </table>
        </form>
    {else}
        <table style="width: 98%">
            <tr>
                <th>NO</th>
                <th>TIPE</th>
                <th>ISI</th>
                <th>PERIODE</th>
                <th>OPERASI</th>
            </tr>
            {foreach $data_kegiatan as $dk}
                <tr>
                    <td>{$dk@index+1}</td>
                    <td>{if $dk.TIPE==1}Pengumuman Ke Mahasiswa{else}Kegiatan Dengan Periode{/if}</td>
                    <td>
                        <div style="padding: 0px 20px">{$dk.ISI}</div>
                        <span style="text-align: right;font-size: 0.7em;">Date Post :{$dk.TGL_POST}</span>
                    </td>
                    <td>Awal : {$dk.TGL_AWAL} <br/> Akhir :{$dk.TGL_AWAL}</td>
                    <td>
                        <a style="padding: 5px" class="ui-button ui-corner-all ui-state-default" href="kegiatan-kkn.php?angkatan={$smarty.get.angkatan}&mode=edit&id={$dk.ID_KKN_KEGIATAN}">Edit</a><br/><br/>
                        <a style="padding: 5px" class="ui-button ui-corner-all ui-state-default" href="kegiatan-kkn.php?angkatan={$smarty.get.angkatan}&mode=delete&id={$dk.ID_KKN_KEGIATAN}">Delete</a>
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="5" style="color: red" class="center">Data Kosong</td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="5" class="center">
                    <a style="padding: 5px" class="ui-button ui-corner-all ui-state-default" href="kegiatan-kkn.php?angkatan={$smarty.get.angkatan}&mode=tambah">Buat Baru</a>
                </td>
            </tr>
        </table>
    {/if}
{/if}