<script language="javascript">
    $(document).ready(function() {
        $("#btn-add").button({
            text: true,
            icons: {
                primary: "ui-icon-plus"
            }
        });

        $("#btn-cancel").button({
            text: true,
            icons: {
                primary: "ui-icon-arrowreturnthick-1-s"
            }
        });

        $("#btn-save").button({
            text: true,
            icons: {
                primary: "ui-icon-disk"
            }
        });

        $("#dialog:ui-dialog").dialog("destroy");

        $('#display-area').load('lppm-kkn-angkatan-display.php');

        $('#btn-add').click(function() {
            //$('#frmAddNumbangkatan').submit();
            //$('#frmAddAngkatan').submit();


        })

        $('#btn-cancel').click(function() {
            $('#add-skim-space').hide();

        })

        $('#frmAddAngkatan').submit(function() {

            var id_semester = $('#id_semester').val();
            var kode_angkatan = $('#kode_angkatan').val();
            var nama_angkatan = $('#nama_angkatan').val();
            var dataString = 'id_semester=' + id_semester + '&kode_angkatan=' + kode_angkatan + '&nama_angkatan=' + nama_angkatan;
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: dataString,
                success: function(data) {
                    $('#add-angkatan-area').remove();
                    $('#display-area').load('lppm-kkn-angkatan-display.php');
                }
            })
            return false;
        });

    });
</script>


<div id="page">
    <div class="center_title_bar">
        PENAMAAN ANGKATAN KKN
    </div>


    <div id="add-angkatan-area">
        <span style="font-style:italic;" class="ui-state-highlight">
            &nbsp;Note: Nama angkatan untuk semester ini masih kosong. Silakan mengisi nama angkatan dibawah ini.&nbsp;
        </span>
        <br /><br />
        <form id="frmAddAngkatan" name="frmAddAngkatan" action="./../../modul/lppm/controller/lppm-kkn-angkatan-insert.php" method="post">
            <table class="ui-widget" style="">
                <tr>
                    <td>
                        Kode Angkatan
                    </td>
                    <td>
                        <input type="text" name="kode_angkatan" id="kode_angkatan" style="width:100px;" />
                        <input type="hidden" name="id_semester" id="id_semester" value="{$id_semester}" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Nama Angkatan
                    </td>
                    <td>
                        <input type="text" name="nama_angkatan" id="nama_angkatan" style="width:300px;" />

                    </td>
                </tr>
            </table>
            <button id="btn-save">Simpan</button>
        </form>
    </div>



    <div>
        &nbsp;
    </div>

    <div id="add-area">
    </div>

    <div id="display-area">
    </div>
</div>