<script>
	$(document).ready(function(){

		function ImgError(source){
			source.src = "{$base_url}images/noimages.jpg";
			source.onerror = "";
			return true;
		}
	
		$('#frmMhsKl .checkall').click(function(event) {
				 $('#frmMhsKl input[type="checkbox"]').attr('checked', 'checked');
				 $('#frmMhsKl .uncheckall').removeAttr("checked");
		});
		
		$('#frmMhsKl .uncheckall').click(function(event) {
		   $('#frmMhsKl input[type="checkbox"]').removeAttr("checked");
		   $('#frmMhsKl .uncheckall').attr('checked', 'checked');
		});
		
		$('#frmMhsKl .listcheck').click(function(event) {
		   $('#frmMhsKl .uncheckall').removeAttr("checked");
		   $('#frmMhsKl .checkall').removeAttr("checked");
		});
			
		$( "#btn-next" ).button({
			text: true,
			icons: {
				primary: "ui-icon-arrow-1-e"
			}
		});
		
		{for $i=0 to $jml_data-1}
		
			$( "#btn-update{$i}" ).button({
				text: true,
				icons: {
					primary: "ui-icon-plus"
				}
			});
			
			$('#btn-update{$i}').click(function(){
				var id_kkn_kel_mhs = document.getElementById('id_kkn_kel_mhs{$i}').value;
				var kelompok = document.getElementById('kelompok{$i}').value;
				var mhs = document.getElementById('id_mhs{$i}').value;
				
				var dataString{$i} = 'kelompok='+kelompok+'&id_kkn_kel_mhs='+id_kkn_kel_mhs;
				$.ajax({
					type: 'GET',
					url: 'lppm-mhs-kelompok-update.php',
					data: dataString{$i},
					success: function(data) {	
						//$('#div_kelompok{$i}').html(data);
						
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"newestOnTop": false,
							"progressBar": false,
							"positionClass": "toast-top-right",
							"preventDuplicates": false,
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "0",
							"extendedTimeOut": "0",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						};
						toastr.success(data);
						$('#div_kelompok{$i}').load('lppm-mhs-kelompok-update.php?id_kkn_kel_mhs='+{$id_kkn_kel_mhs[$i]});
						
					}
				})
				return false;
				
				//$('#div_kelompok{$i}').load('lppm-mhs-kelompok-update.php?kelompok='+kelompok+'&id_kkn_kel_mhs='+id_kkn_kel_mhs);
			});
		
		{/for}
	});
</script>
<form id="frmMhsKl" name="frmMhsKl" action="" method="post">
	<!--<span title="Langkah #1 adalah memilih mahasiswa yang akan dimasukkan kedalam kelompok tertentu">Langkah #1 </span><br /><br />-->
	{for $i=0 to $jml_data-1}
		{if $kelamin[$i]=='1'}
			{$jml_laki=$jml_laki+1}
		{else}
			{$jml_perempuan=$jml_perempuan+1}
		{/if}
	{/for}

	<span style="font-style:italic;">Informasi jumlah peserta menurut jenis kelamin</span>
	<table class="ui-widget" style="width:300px;">
		<tr>
			<td class="ui-widget-header">Kelamin </td> <td class="ui-widget-header">Jumlah</td>
		</tr>
		<tr>
			<td class="ui-widget-content">Laki-laki : </td> <td>{$jml_laki}</td>
		</tr>
		<tr>
			<td class="ui-widget-content">Perempuan : </td> <td>{$jml_perempuan}</td>
		</tr>
		<tr>
			<td class="ui-widget-content">TOTAL : </td> <td>{$jml_perempuan+$jml_laki}</td>
		</tr>
	</table>

	<span style="font-style:italic;">Detail data mahasiswa peserta KKN berdasarkan prodi</span>
	<table class="ui-widget" style="width:100%">
		<tr class="ui-widget-header">
			<!--
			<td style="width:30px;">
				&nbsp;
			</td>
			-->
			<td style="width:20px;">
				#
			</td>
			<td>
				NAMA MAHASISWA
			</td>
			<td>
				JENIS KELAMIN
			</td>
			<td>
				KELOMPOK
			</td>
		</tr>
		
		{for $i=0 to $jml_data-1}
		<tr>
			<!--
			<td class="ui-widget"> 
				<input type="checkbox" value="{$id_mhs[$i]}" name="mhs[]" id="listcheck" class="listcheck"/>
			</td>
			-->
			<td class="ui-widget-content">
				{$i+1}
			</td>
			<td class="ui-widget-content">
				<div style="float:left;margin:5px;">{$nm_mhs[$i]}<br />{$username[$i]}</div>
				<div style="clear:both;">
				</div>
				<input type="hidden" value="{$id_mhs[$i]}" name="id_mhs{$i}" id="id_mhs{$i}"/>
				<input type="hidden" value="{$id_kkn_kel_mhs[$i]}" name="id_kkn_kel_mhs{$i}" id="id_kkn_kel_mhs{$i}"/>
			</td>
			<td class="ui-widget">
				{if $kelamin[$i]=='1'}{$jml_laki==$jml_laki+1}
					LAKI - LAKI
				{else}
					{$jml_perempuan==$jml_perempuan+1}
					PEREMPUAN
				{/if}
			</td>
			<td>
				<table class="ui-widget" style="width:100%;">
					<tr>
						<td style="width:100%;" class="ui-widget-content">
						<select id="kelompok{$i}" name="kelompok{$i}" style="width:100%;">
							{for $j=0 to $jml_kelompok-1}
								<option value="{$id_kkn_kelompok[$j]}">{$nm_kkn_kelompok[$j]}</option>
							{/for}
						</select>
						</td>
						<td class="ui-widget-content">
						<button id="btn-update{$i}">update</button>
						</td>
					</tr>
				</table>
			</form>
			<div id="div_kelompok{$i}">
				<script>
					$('#div_kelompok{$i}').load('lppm-mhs-kelompok-update.php?id_kkn_kel_mhs='+{$id_kkn_kel_mhs[$i]});
				</script>
			</div>
			</td>
		</tr>
		{/for}
	</table>
	<!--
	<table>
		<tr>
			<td><label for="checkall" class="checkall">Check all</label></td><td><input id="checkall2" type="checkbox" name="checkall2" class="checkall" value="0"  /></td>
			<td><label for="checkall" class="uncheckall">Uncheck all</label></td><td> <input type="checkbox"  name="uncheck-all" id="uncheckall" class="uncheckall"/></td>
		</tr>
	</table>
	<button id="btn-next">Ke-langkah #2</button>
	-->
</form>