<div class="center_title_bar">
	REPORT KKN PER FAKULTAS
</div>

{if $fakultas!=""}
	<select id="fakultas" name="fakultas">
		<option value="0">
			: : PILIH FAKULTAS
		</option>
		{foreach $fakultas as $data}
		<option value="{$data.ID_FAKULTAS}">
			{$data.NM_FAKULTAS}
		</option>
		{/foreach}
	</select>
{/if}

<br />
<br />
<div id="report-table" style="width:750px;">
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#fakultas").change(function(){
			var fakultas = $("#fakultas").val()
			$("#report-table").load("kkn-report-fakultas.php?fakultas="+fakultas);
		});
	});
</script>