<form id="formTerjun" name="formTerjun" action="kkn-penerjunan.php" method="post" >
<table class="table">
	<th>
		NO
	</th>
	<th>
		NIM
	</th>
	<th>
		NAMA
	</th>
	<th>
		KELOMPOK
	</th>
	<th>
		KELURAHAN
	</th>
	<th>
		PRODI
	</th>
	<th>
		FAKULTAS
	</th>
	<th>
		STATUS TERJUN
	</th>
	{foreach $sebaran as $data}
	<input type="hidden" name="id_mhs" id="id_mhs" value="{$data.ID_MHS}" />
	<tr>
		<td>
			{$data@index + 1}
		</td>
		<td>
			{$data.NIM_MHS}
		</td>
		<td>
			{$data.NM_PENGGUNA}
		</td>
		<td>
			{$data.NAMA_KELOMPOK}
		</td>
		<td>
			{$data.NM_KELURAHAN}
		</td>
		<td>
			{$data.NM_PROGRAM_STUDI}
		</td>
		<td>
			{$data.NM_FAKULTAS}
		</td>
		<td>
			<select id="status" name="status">
				<option value="{$data.TERJUN}" selected>
					{if $data.TERJUN == "1"}
					TERJUN
					{else}
					BATAL
					{/if}
				</option>
					{if $data.TERJUN=="1"}
					<option value="0">
						BATAL
					</option>
					{else}
					<option value="1">
						TERJUN
					</option>
					{/if}
			</select>
		</td>
	</tr>
	{/foreach}
</table>
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$("#status").change(function(){
			$("#formTerjun").submit();
		});
		
		$("#formTerjun").submit(function(){
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
					alert("Data berhasil diupdate");
				}
			})
			return false;
		});
	});
</script>