<div class="center_title_bar">
	PENERJUNAN KKN
</div>

<div class="content">
	<form id="formCariMhs" name="formCariMhs" action="kkn-penerjunan.php">
		NIM : <input type="text" name="nim" id="nim" />
		<input type="submit" value="Cari" class="button" />
	</form>
	<br /><br />
	<div id="result" class="result">
	</div>

</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#formCariMhs").submit(function(){
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
					$("#result").html(data);
				}
			})
			return false;
		});
	});
</script>