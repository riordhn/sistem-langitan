<div class="center_title_bar">PENETAPAN PESERTA KKN</div>
<form id="calon-peserta" action="lppm-kkn-calon-peserta.php" method="get">
    <table>
        <tr>
            <td>Semester KRS Mahasiswa</td>
            <td>
                <select name="semester">
                    <option value="">Pilih</option>
                    {foreach $data_semester as $s}
                        <option {if $s.ID_SEMESTER==$smarty.get.semester} selected="true" {/if}value="{$s.ID_SEMESTER}">{$s.NM_SEMESTER} , {$s.TAHUN_AJARAN}{if $s.STATUS_AKTIF_SEMESTER=='True'} (Aktif) {/if}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select name="fakultas">
                    <option value="">Pilih</option>
                    {foreach $data_fakultas as $f}
                        <option {if $f.ID_FAKULTAS==$smarty.get.fakultas} selected="true" {/if}value="{$f.ID_FAKULTAS}">{$f.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="view"/>
                <input class="ui-button ui-corner-all ui-state-default" style="margin: 10px;color: green" type="submit" value="Tampilkan"/>
            </td>
        </tr>
    </table>
</form>
{if $data_mhs!=''}
    <form action="lppm-kkn-calon-peserta.php{$smarty.get.QUERY_STRING}" method="post">
        <table style="width: 40%">
            <thead>
                <tr>
                    <th>KOLOM</th>
                    <th>JUMLAH</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>LAKI LAKI</td>
                    <td class="center">{$data_gender.LAKI}</td>
                </tr>
                <tr>
                    <td>PEREMPUAN</td>
                    <td class="center">{$data_gender.WANITA}</td>
                </tr>
                <tr>
                    <td style="font-weight: bold">JUMLAH</td>
                    <td class="center" style="font-weight: bold">{$data_gender.WANITA+$data_gender.LAKI}</td>
                </tr>
            </tbody>
        </table>
    </form>
    {literal}
        <script type="text/javascript">
            $('#peserta').click(function() {
                if ($(this).is(':checked')) {
                    $('.peserta-check').attr('checked', true);
                } else {
                    $('.peserta-check').attr('checked', false);
                }
            });
            $('#delete-peserta').click(function() {
                if ($(this).is(':checked')) {
                    $('.delete-peserta-check').attr('checked', true);
                } else {
                    $('.delete-peserta-check').attr('checked', false);
                }
            });
        </script>
    {/literal}
    <form action="lppm-kkn-calon-peserta.php?{$smarty.server.QUERY_STRING}" method="post">
        <table style="width: 98%">        
            {if count($data_mhs)>0}
                <caption style="margin:10px 0px;padding: 8px" class="center ui-state-highlight ui-corner-all">
                    <b>KETENTUAN PROSES</b>
                    <ol style="text-align: left;margin: 10px">
                        <li>Bila Mahasiswa tersebut sudah menjadi peserta tetapi <i>tidak punya kelompok </i>maka data akan terhapus</li>
                        <li>Bila Mahasiswa tersebut belum menjadi peserta maka data akan terdaftar sebagai peserta</li>
                        <li>Data yang di proses hanyalah yang tercentang (<input type="checkbox" checked="true" disabled="true"/>)</li>
                    </ol>
                    <p style="width: 100%">
                        <i style="color: green">Klik Disini untuk mencentang semua yang belum jadi calon peserta dan akan di jadikan calon peserta</i>
                        <input type="checkbox" id="peserta" />
                        <br/>
                        <br/>
                        <i style="color: green" >Klik Disini untuk mencentang semua peserta yang belum punya kelompok /sudah punya kelompok(tetapi belum di nilai)  selanjutnya data akan di hapus</i>
                        <input type="checkbox" id="delete-peserta" />
                        <br/>
                        <br/>
                    </p>
                    <i>Pilih Jenis KKN Calon Peserta</i><br/><br/>
                    <select name="jenis">
                        <option value="1">Reguler</option>
                        <option value="2">Tematik</option>
                        <option value="3">Magang Khusus</option>
                    </select>
                    <br/>
                    <input class="ui-button ui-corner-all ui-state-default" style="margin: 10px;color: green" type="submit" value="Proses"/>
                    <input type="hidden" name="jumlah" value="{count($data_mhs)}"/>
                    <input type="hidden" name="mode" value="proses"/>
                </caption>
            {/if}            
            <thead>
                <tr>
                    <th>NO</th>
                    <th>NIM</th>
                    <th>NAMA</th>
                    <th>PROGRAM STUDI</th>
                    <th>JALUR</th>
                    <th>NILAI</th>
                    <th>STATUS PESERTA</th>
                </tr>
            </thead>
            <tbody style="font-size: 0.9em">
                {foreach $data_mhs as $d}
                    <tr>
                        <td>{$d@index+1}</td>
                        <td>{$d.NIM_MHS}</td>
                        <td>
                            {$d.NM_PENGGUNA}<br/>
                            {if $d.KELAMIN_PENGGUNA==1}
                                <i>Laki-Laki</i>
                            {else}
                                <i>Perempuan</i>
                            {/if}
                        </td>
                        <td>{$d.NM_JENJANG} {$d.NM_PROGRAM_STUDI}</td>
                        <td class="center">
                            {* 
                            {if $d.STATUS=='R'}
                                Reguler
                            {elseif $d.STATUS=='AJ'}
                                Alih Jenjang
                            {else}
                                International
                            {/if}
                            *}
                            <!-- <br/> -->
                            {$d.NM_JALUR}
                        </td>
                        <td class="center">
                            {number_format($d.NILAI_ANGKA)}, 
                            {if $d.NILAI_HURUF==''}
                                Kosong
                            {else}
                                {$d.NILAI_HURUF}
                            {/if}
                        </td>
                        <td class="center">
                            {if $d.JENIS_KKN_MHS!=''}
                                Terdaftar Peserta <br/>
                                <span style="color: red">
                                    {$d.NAMA_ANGKATAN} ({$d.JENIS_KKN_MHS})<br/>
                                    {if $d.NAMA_KELOMPOK!=''&&$d.NILAI_HURUF!=''}
                                        <i style="color: green"> {$d.NAMA_KELOMPOK}</i>
                                    {else if $d.NAMA_KELOMPOK!=''&&($d.NILAI_HURUF==''||$d.NILAI_HURUF=='0')}
                                        <i style="color: green"> {$d.NAMA_KELOMPOK}</i><br/>
                                        <label style="font-weight: bold;color: #049cdb" for="delete-peserta{$d@index+1}">Hapus Peserta</label>
                                        <input type="checkbox" name="delete-peserta{$d@index+1}" class="delete-peserta-check"/>
                                        <input type="hidden" name="kkm{$d@index+1}" value="{$d.ID_KKN_KELOMPOK_MHS}"/>
                                    {else}
                                        <i style="color: #049cdb">Belum Ada Kelompok</i>
                                        <input type="checkbox" name="delete-peserta{$d@index+1}" class="delete-peserta-check"/>
                                        <input type="hidden" name="kkm{$d@index+1}" value="{$d.ID_KKN_KELOMPOK_MHS}"/>
                                    {/if}
                                </span>
                            {else}
                                <span style="color: green">Belum Ikut</span>
                                <input type="checkbox" name="peserta{$d@index+1}" class="peserta-check"/>
                            {/if}
                            <input type="hidden" name="mhs{$d@index+1}" value="{$d.ID_MHS}"/>
                            <input type="hidden" name="pmk{$d@index+1}" value="{$d.ID_PENGAMBILAN_MK}"/>
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="7" class="error center">Data Kosong</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </form>
{/if}