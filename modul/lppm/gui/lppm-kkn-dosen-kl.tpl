<script>
	function ImgError(source){
		source.src = "{$base_url}images/noimages.jpg";
		source.onerror = "";
		return true;
	}
	 
	$(document).ready(function(){
	
		{for $i=0 to $jml_data-1}
		
		$( "#btn-add{$i}" ).button({
			text: true,
			icons: {
				primary: "ui-icon-plus"
			}
		});
		
		$('#btn-add{$i}').click(function(){
			var id_kkn_dpl = document.getElementById('id_kkn_dpl{$i}').value;
			var kelompok = document.getElementById('kelompok{$i}').value;
			var dosen = document.getElementById('id_dosen{$i}').value;
			$('#div_kelompok{$i}').load('lppm-dpl-kelompok-insert.php?kelompok='+kelompok+'&id_kkn_dpl='+id_kkn_dpl);
		})
		
		{/for}

	});
</script>

<div class="center_title_bar">
	KELOMPOK DOSEN PEMBIMBING LAPANGAN
</div>
<table class="ui-widget" style="width:100%; text-align:top;" valign="top">
	<tr class="ui-widget-header">
		<td>
			NO
		</td>
		<td>
			NAMA DOSEN
		</td>
		<td>
			PRODI
		</td>
		<td>
			KELOMPOK
		</td>
	</tr>
	
	{for $i=0 to $jml_data-1}
	<form id="frmSimpanKelompok{$i}" name="frmSimpanKelompok{$i}" method="post">
	<tr valign="top">
		<td class="ui-widget-content" valign="top">
			{$i+1}
		</td>
		<td  valign="top" style="text-align:top; width:300px;" class="ui-widget-content">
			<img src="{$foto[$i]}" width="50px" height="60px" onerror="ImgError(this);"/>
			{$nm_dosen[$i]}
			<input type="hidden" value="{$id_dosen[$i]}" name="id_dosen{$i}" id="id_dosen{$i}"/>
			<input type="hidden" value="{$id_kkn_dpl[$i]}" name="id_kkn_dpl{$i}" id="id_kkn_dpl{$i}"/>
		</td>
		<td class="ui-widget"> 
			{$nm_prodi[$i]}
		</td>
		<td class="ui-widget">

				<table class="ui-widget" style="width:100%;">
					<tr>
						<td style="width:100%;" class="ui-widget-content">
						<select id="kelompok{$i}" name="kelompok{$i}" style="width:100%;">
							{for $j=0 to $jml_kelompok-1}
								<option value="{$id_kkn_kelompok[$j]}">{$nm_kkn_kelompok[$j]}</option>
							{/for}
						</select>
						</td>
						<td class="ui-widget-content">
						<button id="btn-add{$i}">Tambah</button>
						</td>
					</tr>
				</table>
			</form>
			<div id="div_kelompok{$i}">
				<script>
					$('#div_kelompok{$i}').load('lppm-dpl-kelompok-insert.php?id_kkn_dpl='+{$id_kkn_dpl[$i]});
				</script>
			</div>
		</td>
	</tr>
	{/for}
</table>