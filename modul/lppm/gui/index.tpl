<html>
    <head>
        <title>Lembaga Penelitian dan Pengabdian Kepada Masyarakat (LPPM) - {$nama_pt}</title>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <link rel="stylesheet" type="text/css" href="/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="/css/text.css" />
        <link rel="stylesheet" type="text/css" href="/css/lppm.css" />
        <link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-smoothness.css" />
        <link rel="stylesheet" href="/assets/plugins/jquery-toast/toastr.min.css">
        <!-- <link rel="stylesheet" type="text/css" href="/css/tagit/tagit-simple-blue.css"/>DISABLE by FATHONI, theme kacau -->
        <link rel="shortcut icon" href="/img/icon.ico" />
        
     
		<style>
			.center{
				text-align: center
			}
		</style>
    </head>
    <body>
        <table class="clear-margin-bottom">
            <colgroup>
                <col />
                <col class="main-width"/>
                <col />
            </colgroup>
            <thead>
                <tr>
                    <td class="header-left"></td>
                    <td class="header-center" style="background-image: url('../../img/header/lppm-{$nama_singkat}.png')"></td>
                    <td class="header-right"></td>
                </tr>
                <tr>
                    <td class="tab-left"></td>
                    <td class="tab-center">
                        <ul>
                        <!-- Untuk Menampilkan Menu Utama  -->
                        {foreach $modul_set as $m}
                            {if $m.AKSES == 1}
                                <li><a href="#{$m.NM_MODUL}!{$m.PAGE}" class="nav">{$m.TITLE}</a></li>
                                <li class="divider"></li>
                            {/if}
                        {/foreach}
                            <li><a class="disable-ajax" href="../../logout.php">Logout</a></li>
                        </ul>
                    </td>
                    <td class="tab-right"></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="body-left">&nbsp;</td>
                    <td class="body-center">
                        <table class="content-table">
                            <colgroup>
                                <col />
                                <col />
                            </colgroup>
                            <tr>
                                <td colspan="2" id="breadcrumbs" class="breadcrumbs" ></td>
                            </tr>
                            <tr>
                                <td id="menu" class="menu"></td>
                                <td id="content" class="content">Loading data...</td>
                            </tr>
                        </table>
                    </td>
                    <td class="body-right">&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td class="foot-left">&nbsp;</td>
                    <td class="foot-center">
                        <div class="footer-nav">
                            <a href="">Home</a> | <a href="">About</a> | <a href="">Sitemap</a> | <a href="">RSS</a> | <a href="">Contact Us</a>
                        </div>
                        <div class="footer">Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU <br />oleh <a target="_blank" href="http://unair.ac.id" class="disable-ajax">Universitas Airlangga</a></div>
                    </td>
                    <td class="foot-right">&nbsp;</td>
                </tr>
            </tfoot>
        </table>
        <script src="//code.jquery.com/jquery-1.5.1.min.js" integrity="sha256-dkuenzrThqqlzerpNoNTmU3mHAvt4IfI9+NXnLRD3js=" crossorigin="anonymous"></script>
        <script src="//code.jquery.com/ui/1.8.16/jquery-ui.min.js" integrity="sha256-ctmDClJZfVNK6PR+q7Ne7yDTQxgKLgZBe3qpeE/I5Aw=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="/js/jquery.validate.min.js"></script>
		<!-- Edit in place plugin -->

		<!-- -->
        <script type="text/javascript" src="/js/additional-methods.min.js"></script>
        <script type="text/javascript" src="/js/jquery.price_format.1.4.js"></script>
		<script type="text/javascript" src="../../api/plugins/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="../../api/plugins/ckeditor/adapters/jquery.js"></script>
        <script type="text/javascript" src="/modul/lppm/js/tagit.js"></script>
        <script type="text/javascript" src="/js/jquery.autosize.js"></script>
        <script type="text/javascript">var defaultRel = 'kkn'; var defaultPage = 'lppm-kkn-angkatan.php';</script>
        <script type="text/javascript" src="/js/cybercampus.ajax-1.0.js"></script>
        <script type="text/javascript" src="/assets/plugins/jquery-toast/toastr.min.js"></script>
    </body>
</html>