<div class="center_title_bar">Proposal &gt; Tambah proposal (Pilih Peneliti)</div>

<form action="proposal-add.php" method="get" id="find-form">
<table>
    <tr>
        <th colspan="2">Pencarian Peneliti</th>
    </tr>
    <tr>
        <td>Nama / NIP</td>
        <td>
            <input type="search" name="q" value="{$smarty.get.q}"/>
            <input type="submit" value="Cari" />
        </td>
    </tr>
</table>
</form>

{if !empty($dosen_set)}
{$token = md5(time())}
<table>
    <tr>
        <th>No</th>
        <th>NIP/NIK</th>
        <th>Nama</th>
        <th>Fakultas</th>
        <th>Aksi</th>
    </tr>
    {foreach $dosen_set as $d}
    <tr {if $d@index is not div by 2}class="row1"{/if}>
        <td class="center">{$d@index + 1}</td>
        <td>{$d.USERNAME}</td>
        <td>{$d.NM_PENGGUNA}</td>
        <td>{$d.NM_FAKULTAS}</Td>
        <td>
            <a href="proposal-add.php?mode=add&id_dosen={$d.ID_DOSEN}&token={$token}">Pilih</a>
        </td>
    </tr>
    {/foreach}
</table>
{/if}