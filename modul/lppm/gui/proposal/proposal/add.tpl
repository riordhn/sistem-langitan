<div class="center_title_bar">Proposal &gt; Tambah Proposal</div>

<p><a href="proposal.php">Kembali</a></p>

<form action="proposal.php" method="post" id="form">
    <input type="hidden" name="mode" value="add" />
    <input type="hidden" name="id_penelitian" value="{$id_penelitian}" />
    <input type="hidden" name="id_dosen" value="" />
	<input type="hidden" name="is_dari_lppm" value="1" />
	<input type="hidden" name="is_proposal" value="1" />
	<input type="hidden" name="token" value="{$token}" />
    <table style="width: 100%">
        <tr>
            <th colspan="2">Info Peneliti</th>
        </tr>
        <tr>
            <td>Nama Peneliti</td>
            <td>
                <input type="text" name="peneliti" size="80"/>
                <label for="id_dosen" class="error" style="display: none">Peneliti harus ditentukan</label>
            </td>
        </tr>
        <tr>
            <td>NIP Peneliti</td>
            <td id="nip_peneliti"></td>
        </tr>
        <tr>
            <td>Asal Peneliti</td>
            <td id="asal_peneliti"></td>
        </tr>
        <tr>
            <th colspan="2">Proposal Penelitian</th>
        </tr>
        <tr>
            <td>File Proposal</td>
            <td>
                <iframe src="proposal.php?mode=upload-file&token={$token}&id_penelitian={$id_penelitian}" height="30px" width="350px" scrolling="no" style="border: 0px solid #000;overflow-y: hidden"></iframe>
            </td>
        </tr>
        <tr>
            <td>Judul</td>
            <td><textarea cols="75" rows="3" name="judul"></textarea></td>
        </tr>
        <tr>
            <td>Jenis Penelitian</td>
            <td>
                <select name="id_penelitian_jenis" id="penelitian-jenis">
                    <option value="">--</option>
                {foreach $penelitian_jenis_set as $pj}
                    <option value="{$pj.ID_PENELITIAN_JENIS}">{$pj.NAMA_JENIS}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr id="row-kerjasama" style="display: none">
            <td>Nama Institusi (Kerjasama)</td>
            <td><input type="text" name="nama_institusi" size="50" maxlength="100" /></td>
        </tr>
        <tr id="row-skim" style="display: none">
            <td>SKIM (Dikti)</td>
            <td>
                <select name="id_penelitian_skim" id="penelitian-skim">
                    <option value="">--</option>
                {foreach $penelitian_skim_set as $ps}
                    <option value="{$ps.ID_PENELITIAN_SKIM}">[{$ps.KODE_SKIM}] {$ps.NAMA_SKIM}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Bidang / Tema Penelitian</td>
            <td>
                <select name="id_penelitian_bidang" id="penelitian-bidang" style="display: block">
                {foreach $penelitian_bidang_set as $pb}
                    <option value="{$pb.ID_PENELITIAN_BIDANG}">{$pb.NAMA_BIDANG} / {$pb.NAMA_TEMA}</option>
                {/foreach}
                </select>
                Lainnya : <input type="text" name="penelitian_bidang_lain" size="75" maxlength="100" value="" />
            </td>
        </tr>
        <tr>
            <th colspan="2">Deskripsi</th>
        </tr>
        <tr>
            <td style="width: 20%">Lokasi Penelitian</td>
            <td><input type="text" name="lokasi" maxlength="50" size="30" value="" /></td>
        </tr>
		<tr>
            <td>Tahun</td>
            <td><input type="text" name="tahun" maxlength="5" size="4" value=""/></td>
        </tr>
        <tr>
            <td>Jangka Waktu Penelitian</td>
            <td><input type="text" name="jangka_waktu" maxlength="5" size="4" value=""/> Tahun</td>
        </tr>
        <tr>
            <td>Penelitian Tahun Ke</td>
            <td><input type="text" name="jangka_waktu_ke" maxlength="5" size="4" value=""/></td>
        </tr>
        <tr>
            <td>Anggota Penelitian</td>
            <td>
                <table class="sub-table">
					<tbody>
						<tr>
							<th>No</th>
							<th>NIP</th>
							<th>Nama</th>
							<th>Gelar Depan</th>
							<th>Gelar Belakang</th>
						</tr>
						{for $iAnggota=1 to 6}
						<tr>
							<td class="center">{$iAnggota}</td>
							<td><input type="text" name="nip_anggota[]" /></td>
							<td><input type="text" name="nama_anggota[]" /></td>
							<td><input type="text" name="gelar_depan_anggota[]" size="6"/></td>
							<td><input type="text" name="gelar_belakang_anggota[]" size="6"/></td>
						</tr>
						{/for}
					</tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>Pembiayaan</td>
            <td>
				<table class="sub-table">
					<tbody>
						<tr>
							<th>Tahun Ke</th>
							<th>Biaya dari DIKTI</th>
							<th>Biaya Lain</th>
							<th>Biaya dari PT</th>
						</tr>
						{for $iBiaya=1 to 5}
						<tr>
							<td class="center">{$iBiaya}</td>
							<td><input type="text" name="besar_biaya_dikti[]" size="15"/></td>
							<td><input type="text" name="besar_biaya_lain[]" size="15"/></td>
							<td><input type="text" name="besar_biaya_pt[]" size="15"/></td>
						</tr>
						{/for}
					</tbody>
				</table>
				- Silahkan memasukkan angka saja
			</td>
        </tr>
		<tr>
			<td>Mitra</td>
			<td>
				<table>
					<tr>
						<th colspan="2">Ketua Tim Peneliti Mitra (TPM)</th>
					</tr>
					<tr>
						<td>Nama</td>
						<td><input type="text" name="nama_mitra" maxlength="100" size="30" /></td>
					</tr>
					<tr>
						<td>NIP</td>
						<td><input type="text" name="nip_mitra" maxlength="20" /></td>
					</tr>
					<tr>
						<td>Golongan</td>
						<td><input type="text" name="golongan_mitra" maxlength="100" size="30" /></td>
					</tr>
					<tr>
						<td>Jabatan Fungsional</td>
						<td><input type="text" name="jabatan_fungsional_mitra" size="30" maxlength="50" /></td>
					<tr>
					</tr>
						<td>Jabatan Struktural</td>
						<td><input type="text" name="jabatan_struktural_mitra" size="30" maxlength="50" /></td>
					</tr>
					<tr>
						<td>Perguruan Tinggi</td>
						<td><input type="text" name="pt_mitra" size="50" maxlength="50" /></td>
					</tr>
					<tr>
						<td>Fakultas</td>
						<td><input type="text" name="fakultas_mitra" size="30" maxlength="50"/></td>
					</tr>
					<tr>
						<td>Jurusan</td>
						<td><input type="text" name="prodi_mitra" size="30" maxlength="50"/></td>
					</tr>
					<tr>
						<td>Alamat Kantor</td>
						<td>
							<textarea name="alamat_kantor_mitra" rows="2" cols="50" style="width: auto;"></textarea><br/>
							Telp : <input type="text" name="telp_kantor_mitra" size="20" maxlength="32"/>
							Fax : <input type="text" name="fax_kantor_mitra" size="20" maxlength="32"/>
						</td>
					</tr>
					<tr>
						<td>Alamat Rumah</td>
						<td>
							<textarea name="alamat_mitra" rows="2" cols="50" style="width: auto;" maxlength="256"></textarea><br/>
						</td>
					</tr>
					<tr>
						<td>Telepon Rumah</td>
						<td><input type="text" name="telp_mitra" size="20"/></td>
					</tr>
					<tr>
						<td>Email</td>
						<td><input type="text" name="email_mitra" size="30" /></td>
					</tr>
				</table>
			</td>
		</tr>
        <tr>
            <th colspan="2">Dekan</th>
        </tr>
        <tr>
            <td>NIP Dekan</td>
            <td id="nip_dekan"></td>
        </tr>
        <tr>
            <td>Nama Dekan</td>
            <td id="nm_dekan"></td>
        </tr>
        <tr>
            <th colspan="2">Ketua LPPM</th>
        </tr>
        <tr>
            <td>NIP Ketua LPPM</td>
            <td>{$lppm.USERNAME}</td>
        </tr>
        <tr>
            <td>Nama Ketua LPPM</td>
            <td>{$lppm.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <th colspan="2">Data Pengisian</th>
        </tr>
        <tr>
            <td>Kota</td>
            <td><input type="text" name="kota" value="" /></td>
        </tr>
        <tr>
            <td>Tanggal Pengisian</td>
            <td>
                {html_select_date prefix="tgl_input_" start_year="-50" field_order="DMY"}
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <a href="proposal-add.php">Batal</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>

<div id="dialog"></div>

<script type="text/javascript">
$(document).ready(function() {

    // Fungsi autocomplete search Peneliti / Dosen
    $('input[name=peneliti]').autocomplete({
        source: 'proposal.php?mode=search-dosen',
        search: function(event, ui) {
            $('#asal_peneliti, #nip_peneliti, #nip_dekan, #nm_dekan').html('');
            $('input[name=id_dosen]').val('');
        },
        select: function(event, ui) {
            event.preventDefault();
            $(this).val(ui.item.nama);
            $('input[name=id_dosen]').val(ui.item.value);
            $('#asal_peneliti').html(ui.item.fakultas);
            $('#nip_peneliti').html(ui.item.nip);
            $('#nip_dekan').html(ui.item.nip_dekan);
            $('#nm_dekan').html(ui.item.nm_dekan);
            return false;
        }
    });

    // Jenis penelitian diganti
    $('#penelitian-jenis').change(function() {
        
        // Khusus kerjasama
        if (this.value == 3)
            $('#row-kerjasama').show();
        else
            $('#row-kerjasama').hide();
        
        // SKIM Penelitian
        $.ajax({
            type: 'GET',
            url: 'proposal.php',
            data: 'mode=get-penelitian-skim&id_penelitian_jenis='+this.value,
            beforeSend: function() { $('#penelitian-skim').html(''); },
            success: function(data) {
                if (data != '0')
                {
                    $('#row-skim').show();    
                    $('#penelitian-skim').html(data);
                }
                else
                {
                    $('#row-skim').hide();
                }
            }
        });

        // Bidang / Tema Penelitian sesuai jenis
        $.ajax({
            type: 'GET',
            url: 'proposal.php',
            data: 'mode=get-penelitian-bidang&id_penelitian_jenis='+this.value,
            beforeSend: function() { $('#penelitian-bidang').html('<option>--</option>'); },
            success: function(data) { $('#penelitian-bidang').html(data); $('#penelitian-bidang').change(); }
        });
    });
    
    // Bidang penelitian / tema
    $('#penelitian-bidang').change(function() {
        if ($('#penelitian-bidang :selected').text() == 'Lainnya')
            $('#penelitian-bidang-lain').show();
        else
            $('#penelitian-bidang-lain').hide();
    });
    
    // Validasi form
    $('#form').validate({
        rules: {
            id_dosen:               { required: true },
            judul:                  { required: true },
            id_penelitian_jenis:    { required: true },
            id_penelitian_bidang:   { required: true },
            lokasi:                 { required: true },
			tahun:					{ required: true, number: true },
            jangka_waktu:           { required: true, number: true },
            jangka_waktu_ke:        { required: true, number: true },
            kota:                   { required: true }
        },
        messages: {
            judul:                  { required: 'Judul penelitian harus di isi' },
            id_penelitian_jenis:    { required: 'Jenis penelitian harus di isi' },
            id_penelitian_bidang:   { required: 'Bidang penelitian harus di isi' },
            lokasi:                 { required: 'Lokasi penelitian harus di isi' },
			tahun:					{ required: 'Tahun penelitian harus di isi', number: 'Format isian harus angka' },
            jangka_waktu:           { required: 'Jangka waktu penelitian harus di isi', number: 'Format isian harus angka' },
            jangka_waktu_ke:        { required: 'Jangka waktu ke penelitian harus di isi', number: 'Format isian harus angka' },
            kota:                   { required: 'Kota pengisian form' }
        },
        ignore: '.ignore',
        errorPlacement: function(error, element) { error.appendTo(element.parent()); }
    });
});
</script>