<div class="center_title_bar">Daftar Proposal Penelitian</div>

<p><a href="proposal.php?mode=add">Tambah Proposal</a></p>

<table>
    <tr>
        <th>No</th>
        <th>Judul</th>
        <th>Peneliti</th>
        <th>Tahun</th>
        <th style="width: 70px"></th>
    </tr>
    {foreach $proposal_set as $p}
    <tr {if $p@index is not div by 2}class="row1"{/if}>
        <td class="center">{$p@index + 1}</td>
        <td>{htmlentities($p.JUDUL)}</td>
        <td>{$p.NAMA_PENELITI}</td>
        <td>{$p.TAHUN}</td>
        <td>
			<a href="proposal.php?mode=edit&id_penelitian={$p.ID_PENELITIAN}">Edit</a>
		</td>
    </tr>
    {/foreach}
</table>