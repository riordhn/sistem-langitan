<div class="center_title_bar">Proposal &gt; Edit Proposal</div>

{if $result == true}
<div class="ui-widget">
    <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
        <p style="margin: 0.7em"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
            Data Proposal penelitian berhasil perbarui</p>
    </div>
</div>
{else}
<div class="ui-widget">
    <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
            Data Proposal Penelitian gagal diperbarui.</p>
    </div>
</div>
{/if}

<a href="proposal.php?mode=edit&id_penelitian={$id_penelitian}">Kembali Ke Edit</a>
<br/>
<a href="proposal.php">Kembali Ke List</a>