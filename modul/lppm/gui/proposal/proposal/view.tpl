<div class="center_title_bar">Proposal Penelitian &gt; Lihat Proposal</div>

<p><a href="proposal.php">Kembali</a></p>
<table>
    <tr>
        <th colspan="2">Info Peneliti</th>
    </tr>
    <tr>
        <td>Nama Peneliti</td>
        <td>{$penelitian.NAMA_PENELITI}</td>
    </tr>
    <tr>
        <td>NIP Peneliti</td>
        <td>{$penelitian.NIP_PENELITI}</td>
    </tr>
    <tr>
        <td>Asal Peneliti</td>
        <td>{$penelitian.ASAL_PENELITI}</td>
    </tr>
    <tr>
        <th colspan="2">Proposal Penelitian</th>
    </tr>
    <tr>
        <td>File Proposal</td>
        <td></td>
    </tr>
    <tr>
        <td>Judul</td>
        <td>{$penelitian.JUDUL}</td>
    </tr>
    <tr>
        <td>Jenis Penelitian</td>
        <td>{$penelitian.JENIS_PENELITIAN}</td>
    </tr>
    <tr id="row-kerjasama" style="display: none">
        <td>Nama Institusi (Kerjasama)</td>
        <td></td>
    </tr>
    <tr id="row-skim" style="display: none">
        <td>SKIM (Dikti)</td>
        <td></td>
    </tr>
    <tr>
        <td>Bidang / Tema Penelitian</td>
        <td>{$penelitian.BIDANG_PENELITIAN}</td>
    </tr>
    <tr>
        <th colspan="2">Deskripsi</th>
    </tr>
    <tr>
        <td style="width: 20%">Lokasi Penelitian</td>
        <td>{$penelitian.LOKASI}</td>
    </tr>
    <tr>
        <td>Jangka Waktu Penelitian</td>
        <td>{$penelitian.JANGKA_WAKTU} Tahun</td>
    </tr>
    <tr>
        <td>Penelitian Tahun Ke</td>
        <td>{$penelitian.JANGKA_WAKTU_KE}</td>
    </tr>
    <tr>
        <td>Anggota Penelitian</td>
        <td style="font-size: 90%">
            <!--
            <table>
                <tr>
                    <th>NIP</th>
                    <th>Nama</th>
                    <th>Gelar Depan</th>
                    <th>Gelar Belakang</th>
                    <th>Aksi</th>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="nip_anggota" />
                    </td>
                    <td>
                        <input type="text" name="nama_anggota" />
                    </td>
                    <td>
                        <input type="text" name="gdepan_anggota" size="6"/>
                    </td>
                    <td>
                        <input type="text" name="gbelakang_anggota" size="6"/>
                    </td>
                    <td>
                        <button id="tambah-anggota-button">Tambah</button>
                    </td>
                </tr>
            </table>
            -->
        </td>
    </tr>
    <tr>
        <td>Pembiayaan</td>
        <td>Rp 0</td>
    </tr>
    <tr>
        <th colspan="2">Ketua Tim Peneliti Mitra (TPM)</th>
    </tr>
    <tr>
        <td>Nama</td>
        <td><input type="text" name="tpm_nama" maxlength="100" size="30" /></td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>
            <select name="tpm_jenis_kelamin">
                <option value="L">Laki-Laki</option>
                <option value="P">Perempuan</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>NIP</td>
        <td><input type="text" name="tpm_nip" maxlength="20" /></td>
    </tr>
    <tr>
        <td>Golongan</td>
        <td>
            <select name="tpm_id_golongan">
                <option value="">--</option>
            {foreach $golongan_set as $g}
                <option value="{$g.ID_GOLONGAN}">{$g.NM_GOLONGAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Jabatan Fungsional</td>
        <td><input type="text" name="tpm_jabatan_fungsional" size="30" maxlength="50" /></td>
    <tr>
    </tr>
        <td>Jabatan Struktural</td>
        <td><input type="text" name="tpm_jabatan_struktural" size="30" maxlength="50" /></td>
    </tr>
    <tr>
        <td>Perguruan Tinggi</td>
        <td><input type="text" name="tpm_pt" size="50" maxlength="50" /></td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td><input type="text" name="tpm_fakultas" size="30" maxlength="50"/></td>
    </tr>
    <tr>
        <td>Jurusan</td>
        <td><input type="text" name="tpm_prodi" size="30" maxlength="50"/></td>
    </tr>
    <tr>
        <td>Alamat Kantor</td>
        <td>
            <textarea name="tpm_alamat_kantor" rows="2" cols="50" style="width: auto;"></textarea><br/>
            Telp : <input type="text" name="tpm_telp_kantor" size="20" maxlength="32"/>
            Fax : <input type="text" name="tpm_fax_kantor" size="20" maxlength="32"/>
        </td>
    </tr>
    <tr>
        <td>Alamat Rumah</td>
        <td>
            <textarea name="tpm_alamat_rumah" rows="2" cols="50" style="width: auto;" maxlength="256"></textarea><br/>
        </td>
    </tr>
    <tr>
        <td>Telepon Rumah</td>
        <td><input type="text" name="tpm_telp_rumah" size="20"/></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><input type="text" name="tpm_email" size="30" /></td>
    </tr>
    <tr>
        <th colspan="2">Dekan</th>
    </tr>
    <tr>
        <td>NIP Dekan</td>
        <td id="nip_dekan">{$penelitian.NIP_DEKAN}</td>
    </tr>
    <tr>
        <td>Nama Dekan</td>
        <td id="nm_dekan">{$penelitian.NAMA_DEKAN}</td>
    </tr>
    <tr>
        <th colspan="2">Ketua LPPM</th>
    </tr>
    <tr>
        <td>NIP Ketua LPPM</td>
        <td>{$penelitian.NIP_KETUA_LPPM}</td>
    </tr>
    <tr>
        <td>Nama Ketua LPPM</td>
        <td>{$penelitian.NAMA_KETUA_LPPM}</td>
    </tr>
    <tr>
        <th colspan="2">Data Pengisian</th>
    </tr>
    <tr>
        <td>Kota</td>
        <td>{$penelitian.KOTA}</td>
    </tr>
    <tr>
        <td>Tanggal Pengisian</td>
        <td>{$penelitian.TGL_PENELITIAN|date_format:'%d %B %Y'}</td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <a href="proposal.php">Kembali</a>
        </td>
    </tr>
</table>