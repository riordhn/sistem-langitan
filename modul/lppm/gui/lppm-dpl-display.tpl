<script>
	function ImgError(source){
		source.src = "{$base_url}images/noimages.jpg";
		source.onerror = "";
		return true;
	}
	$(document).ready(function(){
	
		{for $i=0 to $jml_data-1}
			$( "#btn-del{$id_dosen[$i]}" ).button({
				text: false,
				icons: {
					primary: "ui-icon-close"
				}
			});
			
			$('#btn-del{$id_dosen[$i]}').click(function(){
				//var id_kkn_dpl = document.getElementById('id_kkn_dpl{$i}').value;
				//var kelompok = document.getElementById('kelompok{$i}').value;
				var id_dosen = document.getElementById('id_dosen{$i}').value;
				//$('#div_kelompok{$i}').load('lppm-dpl-kelompok-insert.php?kelompok='+kelompok+'&id_kkn_dpl='+id_kkn_dpl);
				//var id = $("#id_kelompok_dpl{$id[$i]}").val();
				//alert("" + id + "");
				$("#dpl{$i}").load('lppm-dpl-delete.php?id_dosen='+id_dosen);
				$("#dpl{$i}").remove();
			})
		
		{/for}

	});
</script>
<span style="font-style:italic;">Daftar Dosen Pembimbing Lapangan</span>
<table class="ui-widget" style="width:100%; text-align:top;" valign="top">
	<tr class="ui-widget-header">
		<td>
			#
		</td>
		<td>
			NAMA DOSEN
		</td>
		<td>
			PRODI
		</td>
		<td style="width:40px;">
			ACTION
		</td>
	</tr>
	
	{for $i=0 to $jml_data-1}
	<tr id="dpl{$i}">
		<td class="ui-widget-content">
			{$i+1}
		</td>
		<td class="ui-widget-content">
			<img src="{$foto[$i]}" width="30px" height="40px" onerror="ImgError(this);"/>
			{$nm_dosen[$i]}
			<input type="hidden" value="{$id_dosen[$i]}" id="id_dosen{$i}" />
		</td>
		<td class="ui-widget-content"> 
			{$nm_prodi[$i]}
		</td>
		<td>
			<div id="btn-del{$id_dosen[$i]}" style="width:30px; height:20px;">Hapus</div>
		</td>
	</tr>
	{/for}
</table>