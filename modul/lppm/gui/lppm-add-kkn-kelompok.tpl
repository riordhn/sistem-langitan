<div id="add-kkn-kelompok">
	<script language="javascript">
		$(document).ready(function() {
			$( "#btn-add" ).button({
				text: true
			});
			
			$( "#btn-cancel" ).button({
				text: true
			});
			
			$( "#btn-save" ).button({
				text: true
			});
			
			$('#btn-save').click(function(){	
				$('#frmSimpanKKN').submit();
			})
			
			$('#btn-cancel').click(function(){	
				
				$('#add-kkn-kelompok').remove();
				$('#display-area').show();
				$('#display-area').load('lppm-kkn-kelompok-display.php');

		
			})
			
			$('#frmSimpanKKN').submit(function() {
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: $(this).serialize(),
							success: function(data) {	
								$('#add-kkn-kelompok').remove();
								$('#display-area').show();
								$('#display-area').load('lppm-kkn-kelompok-display.php');
							
							}
						})
						return false;
			});
			
			{for $i=0 to $jml_kelompok-1}
				
				$( "#btn-delete{$i}" ).button({
					text: false,
					icons: {
						primary: "ui-icon-close"
					}
				});
				
				$('#btn-delete{$i}').click(function(){	
					$('#kelompok{$i}').remove();
				})
			{/for}
			
			$( "#dialog:ui-dialog" ).dialog( "destroy" );	

		});
	</script>

	<form id="frmSimpanKKN" name="frmSimpanKKN" action="lppm-kkn-kelompok-simpan.php" method="post">
	<input type="hidden" name="id_semester" id="id_semester" value="{$id_semester}"/>
	<table class="ui-widget" width="100%" style="width:100%;">
		<tr class="ui-widget-header">
			
			<td>
				#
			</td>
			
			<td>
				NAMA
			</td>
			<td>
				KELURAHAN
			</td>
			<td>
				QUOTA
			</td>
			<td>
				JENIS KKN
			</td>
			<td style="width:20px;">
				
			</td>
		</tr>
		
		{for $i=0 to $jml_kelompok-1}
		

			<tr class="ui-widget-content" id="kelompok{$i}">
				
				<td>
					{$i+1}
				</td>
				
				<td>
					<input type="text" name="nama-kelompok-kkn[]" id="nama-kelompok-kkn{$i}" style="width:100%;" />
				</td>

				<td>
					<select id="kelurahan{$i}" name="kelurahan[]" style="width:100%;">
						{for $j=0 to $jml_kelurahan-1}
						<option value="{$id_kelurahan[$j]}">{$nm_kelurahan[$j]}</option>
						{/for}
					</select>
				</td>
				<td>
					<input type="text" name="quota[]" style="width:100%" />
				</td>
				<td>
					<select name="jenis_kkn[]" style="width:100%;">
						<option value="1">
							Reguler
						</option>
						<option value="2">
							Tematik
						</option>
					</select>
				</td>
				<td style="width:20px;">
					<button style="width:20px;" id="btn-delete{$i}">hapus</button>
				</td>
			</tr>
		{/for}
	</table>
	</form>
	<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;background:#ededed;padding-bottom:10px; padding-left:5px; padding-right:5px; padding-top:10px; margin-right:5px;  border-color:#ccc;border-bottom-width:1px;border-bottom-style:solid;">
		 <button style="float:right;" id="btn-cancel">batal</button><button id="btn-save" style="float:right;">simpan</button> &nbsp;
		<div style="clear:both;">
		</div>
	</div>
</div>