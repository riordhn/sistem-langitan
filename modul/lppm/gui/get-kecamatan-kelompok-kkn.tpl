<script>
	$(document).ready(function(){
		$( "#btn-add" ).button({
			text: true
		});
		
		$( "#btn-cancel" ).button({
			text: true
		});
		
		$( "#btn-save" ).button({
			text: true
		});
		
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		//$('#display-area').load('lppm-kkn-kelompok-display.php');
		
		$('#btn-add').click(function(){		
		
			$('#frmAddNumbKelompok').submit();

		})

		$('#btn-cancel').click(function(){		
			$('#add-skim-space').hide();

		})
		
		$('#btn-save').click(function(){		
			$('#formAddSkim').submit();
		})

		$('#frmAddNumbKelompok').submit(function() {

						var jml_kel = $('#input-jml-kelompok').val();
						var id_kecamatan = $('#id_kecamatan').val();
						var dataString = 'jml_kelompok=' + jml_kel+'&id_kecamatan='+id_kecamatan;
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: $(this).serialize(),
							success: function(data) {	
								$('#add-area').html(data);	
						}
					})
					return false;
		});

		$('#kec-kelompok-kkn').change(function(){
			$('input').removeAttr('disabled');
			var id_kec = $('#kec-kelompok-kkn').val();
			$('#get-kelompok-kkn').load('KelompokKknController.php?id_kec='+id_kec+'&act=kel');
		});

	});
</script>

<form id="frmAddNumbKelompok" name="frmAddNumbKelompok" action="KelompokKknController.php" method="post">
<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;background:#ededed;padding-bottom:10px; padding-left:5px; padding-right:5px; padding-top:10px; margin-right:5px;  border-color:#ccc;border-bottom-width:1px;border-bottom-style:solid;">
	<span style="float:left;">Kecamatan :</span> 
	<select id="kec-kelompok-kkn" name="id_kecamatan" id="id_kecamatan" style="float:right; width:200px;">
		<option value="0">Pilih Kecamatan</option>
		{for $a = 0 to $jml_kecamatan-1}
		<option value="{$id_kecamatan[$a]}">{$nm_kecamatan[$a]}</option>
		{/for}
	</select>
	<div style="clear:both;">
	</div>
</div>

<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;background:#ededed;padding-bottom:10px; padding-left:5px; padding-right:5px; padding-top:10px; margin-right:5px;  border-color:#ccc;border-bottom-width:1px;border-bottom-style:solid;">
	<span style="float:left;">Jumlah Kelompok :</span>
		<input style="float:right; width:200px;" type="text" id="jml_kelompok" name="jml_kelompok" disabled/>
	<div style="clear:both;">
	</div>
</div>
<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;background:#ededed;padding-bottom:10px; padding-left:5px; padding-right:5px; padding-top:10px; margin-right:5px;  border-color:#ccc;border-bottom-width:1px;border-bottom-style:solid;">
	<button id="btn-add" style="float:right;">tambah</button> 
	<div style="clear:both;">
	</div>
</div>
</form>