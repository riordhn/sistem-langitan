<script language="javascript">
	$(document).ready(function() {
		$( "#btn-add" ).button({
			text: true
		});
		
		$( "#btn-cancel" ).button({
			text: true
		});
		
		$( "#btn-save" ).button({
			text: true
		});
		
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		$('#display-area').load('lppm-kkn-kelompok-display.php');
		
		$('#btn-add').click(function(){		
		
			$('#frmAddNumbKelompok').submit();

		})

		$('#btn-cancel').click(function(){		
			$('#add-skim-space').hide();

		})
		
		$('#btn-save').click(function(){		
			$('#formAddSkim').submit();
		})

		$('#frmAddNumbKelompok').submit(function() {

						var jml_kel = $('#input-jml-kelompok').val();
						var dataString = 'jml_kelompok=' + jml_kel;
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: dataString,
							success: function(data) {	
								$('#add-area').html(data);
								//$('#display-area').hide();	
						}
					})
					return false;
		});		

		$('#get-provinsi-kelompok-kkn').load('KelompokKknController.php?act=prov');
		$('#get-kabupaten-kelompok-kkn').load('KelompokKknController.php?act=kab');
		$('#get-kecamatan-kelompok-kkn').load('KelompokKknController.php?act=kec');

	});
</script>


<div id="page">
	<div class="center_title_bar">
		KELOMPOK KKN
	</div>
	<div style="width:760px;">
		<div style="float:left; width:700px;">
			<div style="width:700px;border-color:#CCC;border-left-width:1px;border-left-style:dotted;padding-left:10px;">
				<div></div>
				<div style="float:left;width:50%;margin-right:5px;">
					<form id="frmAddKelompok" name="frmAddKelompok" method="POST">
					<div id="korkab-area{$id[$i]}">
						<input type="hidden" id="" name="id" value="">
						<input type="hidden" id="" name="i" value="">
						<div id="get-provinsi-kelompok-kkn">
						</div>
						<div id="get-kabupaten-kelompok-kkn">
						</div>
						<div id="get-kecamatan-kelompok-kkn">
						</div>
					</div>
					</form>
				</div>

				<div style="clear:both;">
					&nbsp;
				</div>
			</div>
		</div>
	</div>
	
	<div style="clear:both;">
	</div>
	<br />
	<div style="border-color:#EAEAEA;border-bottom-width:1px;border-bottom-style:solid;">
	</div>
	
	<div>
		&nbsp;
	</div>
	
	<div id="add-area">
	</div>
	
	<div id="notif">
	</div>
	
	<div id="display-area">
	</div>
</div>