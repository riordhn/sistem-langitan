<script>
	$(document).ready(function() {
		//$('#display-area').load('lppm-mhs-kel-display.php');
		$('#fakultas').change(function(){
			var fakultas = document.getElementById('fakultas').value;
			var semester = document.getElementById('semester').value;
			
			$('#prodi_area').load('get-prodi-calon-peserta.php?fakultas='+fakultas+'&semester='+semester);
		})
	});
</script>
<div id="page">
	<div class="center_title_bar">
		PENENTUAN CALON PESERTA KKN
	</div>
	<span style="font-style:italic;">Note: Pilih fakultas untuk memulai</span>
	<br /><br />
	<div id="add-number-area">
		<table class="ui-widget">
			<tr>
				<td>
					Pilih Semester
				</td>
				<td>
					<form>
						<select id="semester" name="semester" style="width:100%;">
							{for $i=0 to $jml_data_semester-1}
							<option value="{$id_semester[$i]}">{$nm_semester[$i]}</option>
							{/for}
						</select>
					</form>
				</td>
			</tr>
			<tr>
				<td>
					Pilih Fakultas
				</td>
				<td>
					<form>
						<select id="fakultas" name="kelurahan" style="width:100%;">
							{for $i=0 to $jml_data-1}
							<option value="{$id_fakultas[$i]}">{$nm_fakultas[$i]}</option>
							{/for}
						</select>
					</form>
				</td>
			</tr>
			<tr>
				<td>
					Pilih Prodi
				</td>
				<td>
					<div id="prodi_area">
					</div>
				</td>
			</tr>
		</table>
	</div>
	
	<div>
		&nbsp;
	</div>
	
	<div id="add-area">
	</div>
	
	<div id="notif">
	</div>
	
	<div id="display-area">
	</div>
</div>