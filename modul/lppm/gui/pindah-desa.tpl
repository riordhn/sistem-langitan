<div class="center_title_bar">PEMINDAHAN PERKELOMPOK KKN KE KELOMPOK LAIN</div>
<form id="kontrol" action="pindah-desa.php" method="get">
    <table style="width: 50%">
        <tr>
            <td style="width: 10%">Angkatan</td>
            <td style="width: 20%">
                <select id="angk" name="angkatan">
                    <option value="">Pilih Angkatan</option>
                    {foreach $data_angkatan as $da}
                        <option {if $smarty.get.angkatan==$da.ID_KKN_ANGKATAN}selected="true"{/if} value="{$da.ID_KKN_ANGKATAN}">{$da.NAMA_ANGKATAN}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="center">
                <input type="hidden" name="mode" value="tampil"/>
                <input type="submit" value="Tampilkan" style="padding: 5px" class="ui-button ui-corner-all ui-state-default"/>
            </td>
        </tr>
    </table>
</form>
{if $smarty.get.mode=='tampil'}
    <table style="width: 99%">
        <tr>
            <th colspan="8" class="center">KELOMPOK KKN {$angkatan.NAMA_ANGKATAN} </th>
        </tr>
        <tr>
            <th>NO</th>
            <th>NAMA</th>
            <th>LOKASI</th>
            <th>JENIS KKN</th>
            <th>QUOTA</th>
            <th>TERISI</th>
            <th>STATUS</th>
            <th>AKSI</th>
        </tr>
        {foreach $data_kelompok as $k}
            <tr>
                <td>{$k@index+1}</td>
                <td>{$k.NAMA_KELOMPOK}</td>
                <td>Kel : {$k.NM_KELURAHAN}<br/>
                    Kec : {$k.NM_KECAMATAN}<br/>
                    Kota : {$k.NM_KOTA}
                </td>
                <td>{if $k.JENIS_KKN==1}Reguler {else if $k.JENIS_KKN==2} Tematik {else if $k.JENIS_KKN==3}Magang Khusus{/if}</td>
                <td>{$k.QUOTA}</td>
                <td>{$k.TERISI}</td>
                <td class="center">
                    {if $k.TERISI>=$k.QUOTA}
                        <b style="color: red;font-family: Trebuchet MS"><i>Sudah Penuh</i></b>
                    {else if $k.TERISI<$k.QUOTA&&$k.TERISI!=0}
                        <b style="color: orange;font-family: Trebuchet MS"><i>Belum Penuh</i></b>
                    {else if $k.TERISI<$k.QUOTA&&$k.TERISI==0}
                        <b style="color: green;font-family: Trebuchet MS"><i>Kosong</i></b>
                    {/if}
                </td>
                <td>
                    <a style="padding: 5px" onclick="$('#kelompok{$k.ID_KKN_KELOMPOK}').toggle();" class="disable-ajax ui-button ui-corner-all ui-state-default" >UBAH</a>
                </td>
            </tr>
            <tr id="kelompok{$k.ID_KKN_KELOMPOK}" style="display: none">
                <td colspan="8" class="center">
                    <form action="pindah-desa.php?{$smarty.server.QUERY_STRING}"  method="post">
                        Pindah Kelompok Ini Ke Kelompok<br/>
                        <select name="id_kelompok_baru" style="font-size: 0.85em">
                            {foreach $data_kelompok as $kel}
                                {if $kel.TERISI>=$kel.QUOTA}
                                    <option  style="background-color: #ff9999"  {if $kel.ID_KKN_KELOMPOK==$k.ID_KKN_KELOMPOK}selected="true"{/if} value="{$kel.ID_KKN_KELOMPOK}">Kelompok : {$kel.NAMA_KELOMPOK}, Kel. : {$kel.NM_KELURAHAN}, Kec. : {$kel.NM_KECAMATAN}, Kota : {$kel.NM_KOTA}, Status : Penuh({$kel.QUOTA}=>{$kel.TERISI})</option>
                                {else if $kel.TERISI<$kel.QUOTA&&$kel.TERISI!=0}
                                    <option  style="background-color: #ffcc66" {if $kel.ID_KKN_KELOMPOK==$k.ID_KKN_KELOMPOK}selected="true"{/if} value="{$kel.ID_KKN_KELOMPOK}">Kelompok : {$kel.NAMA_KELOMPOK}, Kel. : {$kel.NM_KELURAHAN}, Kec. : {$kel.NM_KECAMATAN}, Kota : {$kel.NM_KOTA}, Status : Belum Penuh({$kel.QUOTA}=>{$kel.TERISI})</option>
                                {else if $kel.TERISI<$kel.QUOTA&&$kel.TERISI==0}
                                    <option  style="background-color: #99ff99" {if $kel.ID_KKN_KELOMPOK==$k.ID_KKN_KELOMPOK}selected="true"{/if} value="{$kel.ID_KKN_KELOMPOK}">Kelompok : {$kel.NAMA_KELOMPOK}, Kel. : {$kel.NM_KELURAHAN}, Kec. : {$kel.NM_KECAMATAN}, Kota : {$kel.NM_KOTA}, Status : Kosong({$kel.QUOTA}=>{$kel.TERISI})</option>
                                {/if}                                    
                            {/foreach}
                        </select>
                        <input type="hidden" name="id_kelompok_lama" value="{$k.ID_KKN_KELOMPOK}"/>
                        <input type="hidden" name="mode" value="ubah-kelompok"/>
                        <input type="submit" value="Simpan" class="ui-button ui-corner-all ui-state-default" style="padding: 5px"/>
                    </form>
                </td> 
            </tr>
        {/foreach}
    </table>
{/if}
{literal}
    <script type="text/javascript">
        $('#angk').change(function() {
            $.ajax({
                url: 'GetKelompokPerAngkatan.php',
                type: 'get',
                data: 'angkatan=' + $(this).val(),
                success: function(data) {
                    $('#kel').html(data);
                }
            });
        });
    </script>
{/literal}