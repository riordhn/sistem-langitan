<div class="center_title_bar">Rekap Proposal Penelitian per SKIM</div>

{foreach $tahun_set as $t}
<table>
	<caption>Tahun {$t.TAHUN}</caption>
    <thead>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">SKIM</th>
            <th colspan="{count($fakultas_set)}">Fakultas</th>
            <th rowspan="2">Jumlah</th>
        </tr>
		<tr>
			{foreach $fakultas_set as $f}
				<th title="{$f.NM_FAKULTAS}">{$f.SINGKATAN_FAKULTAS}</th>
			{/foreach}
		</tr>
    </thead>
    <tbody>
		{foreach $skim_set as $s}
			<tr>
				<td>{$s@index + 1}</td>
				<td>{$s.KODE_SKIM}<br/><span style="font-size: 0.9em">{$s.NAMA_SKIM}</span></td>
				{foreach $fakultas_set as $f}
				<td class="center">
					{foreach $data_set as $d}
						{if $d.TAHUN == $t.TAHUN and $d.ID_PENELITIAN_SKIM == $s.ID_PENELITIAN_SKIM and $d.ID_FAKULTAS == $f.ID_FAKULTAS}
							{$d.JUMLAH}
						{/if}
					{/foreach}
				</td>
				{/foreach}
				<td></td>
			</tr>
		{/foreach}
		<tr>
			<td colspan="2">Jumlah</td>
			{foreach $fakultas_set as $f}
				<td></td>
			{/foreach}
			<td></td>
		</tr>
    </tbody>
</table>
{/foreach}