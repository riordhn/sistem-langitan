<div class="center_title_bar">Rekap Proposal Penelitian per Tahun</div>

<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Tahun</th>
            <th>Jumlah Proposal</th>
        </tr>
    </thead>
    <tbody>
        {foreach $data_set as $data}
        <tr>
            <td>{$data@index + 1}</td>
            <td>{$data.TAHUN}</td>
            <td class="center">{$data.JUMLAH}</td>
        </tr>
        {/foreach}
    </tbody>
</table>