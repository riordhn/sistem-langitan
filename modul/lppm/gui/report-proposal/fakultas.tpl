<div class="center_title_bar">Rekap Proposal Penelitian per Fakultas</div>

<table>
    <thead>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Tahun</th>
			<th colspan="{count($fakultas_set)}">Fakultas</th>
			<th rowspan="2">Jumlah</th>
        </tr>
		<tr>
			{foreach $fakultas_set as $f}
				<th title="{$f.NM_FAKULTAS}">{$f.SINGKATAN_FAKULTAS}</th>
			{/foreach}
		</tr>
    </thead>
    <tbody>
		{foreach $tahun_set as $t}
			<tr>
				<td>{$t@index + 1}</td>
				<td>{$t.TAHUN}</td>
				{foreach $fakultas_set as $f}
					<td>
						{foreach $data_set as $d}
							{if $d.TAHUN == $t.TAHUN and $f.ID_FAKULTAS == $d.ID_FAKULTAS}{$d.JUMLAH}{break}{/if}
						{/foreach}
					</td>
				{/foreach}
				<td>{$t.JUMLAH}</td>
			</tr>
		{/foreach}
		<tr>
			<td colspan="2">Jumlah</td>
			{foreach $fakultas_set as $f}
				<td>{$f.JUMLAH}</td>
			{/foreach}
			<td></td>
		</tr>
    </tbody>
</table>