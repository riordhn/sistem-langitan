<div class="center_title_bar">
	KONFIRMASI PUBLIKASI KKN
</div>

<div>
	<form id="formBukaKkn" name="formBukaKkn" action="buka_kkn.php" method="post">
		<input type="hidden" value="{$id_kelompok}" name="kelompok" />
		<input type="hidden" value="1" name="publish" />
		<table>
			<th>
				NAMA
			</th>
			<th>
				KELURAHAN			
			</th>
			<th>
				QUOTA
			</th>
			<th>
				JENIS KKN
			</th>
			<th>
				STATUS
			</th>
			
			<tbody>
				{foreach $kelompok as $data}
				<tr>
					<td>
						{$data.NAMA_KELOMPOK}
					</td>
					<td>
						{$data.NM_KELURAHAN}
					</td>
					<td>
						{$data.QUOTA}
					</td>
					<td>
						
						{if $data.JENIS_KKN==1}
							REGULER
						{else}
							TEMATIK
						{/if}
					</td>
					<td>
						{if $data.STATUS_PUBLISH == 1}
							DIPUBLIKASI
						{else}
							TIDAK DIPUBLIKASI
						{/if}
					</td>
				<tr>
				{/foreach}
			</tbody>
		</table>
		
		<input class="ui-button ui-corner-all ui-state-default" type="submit" value="PUBLIKASIKAN" />
	</form>
</div>