<div class="center_title_bar">Tambah Angkatan KKN</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="lppm-kkn-angkatan.php" method="post">
<input type="hidden" name="mode" value="add" />
<input type="hidden" name="id_semester" value="{$id_semester}" />
<!--<input type="hidden" name="id_role" value="{$smarty.get.id_role}" />
<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="2">Detail Angkatan KKN</th>
    </tr>
    <tr>
        <td>Kode Angkatan</td>
        <td><input class="form-control" type="text" name="kode_angkatan" value="" size="25" />
        </td>
    </tr>
    <tr>
        <td>Semester</td>
        <td>{$nm_semester}</td>
    </tr>
    <tr>
        <td>Nama Angkatan</td>
        <td><input class="form-control" type="text" name="nama_angkatan" value="" size="75" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

<a href="lppm-kkn-angkatan.php">Kembali</a>