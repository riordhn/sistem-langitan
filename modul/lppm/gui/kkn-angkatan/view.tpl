<div class="center_title_bar">PENAMAAN ANGKATAN KKN</div>

{if isset($kkn_angkatan_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Kode</th>
				<th>Semester</th>
				<th>Nama Angkatan</th>
				<th>Status Aktif</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $kkn_angkatan_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td><strong>{$uk.KODE_ANGKATAN}</strong></td>
					<td>{$uk.NM_SEMESTER}</td>
					<td>{$uk.NAMA_ANGKATAN}</td>
					{if $uk.STATUS_AKTIF == 0}
						<td>Tidak Aktif</td>
						<td class="center">
							<a href="lppm-kkn-angkatan.php?mode=delete_aktif&deleted=0&id_kkn_angkatan={$uk.ID_KKN_ANGKATAN}"><b style="color: red">Aktifkan</b></a>
						</td>
					{elseif $uk.STATUS_AKTIF == 1}
						<td>Aktif</td>
						<td class="center">
							<a href="lppm-kkn-angkatan.php?mode=edit&id_kkn_angkatan={$uk.ID_KKN_ANGKATAN}"><b style="color: blue">Edit</b></a>
						{if $uk.RELASI == 0}
							<a href="lppm-kkn-angkatan.php?mode=delete_aktif&deleted=1&id_kkn_angkatan={$uk.ID_KKN_ANGKATAN}"><b style="color: red">Hapus</b></a>
						{/if}

						</td>

					{/if}
				</tr>
			{/foreach}
			<tr>
				<td colspan="6" class="center">
					<a href="lppm-kkn-angkatan.php?mode=add"><b style="color: red">Tambah Angkatan KKN</b></a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}