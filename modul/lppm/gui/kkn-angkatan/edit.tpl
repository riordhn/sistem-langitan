<div class="center_title_bar">ID Angkatan KKN : {$kkn_angkatan.ID_KKN_ANGKATAN}</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="lppm-kkn-angkatan.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_kkn_angkatan" value="{$kkn_angkatan.ID_KKN_ANGKATAN}" />
<!--<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="2">Detail Angkatan KKN</th>
    </tr>
    <tr>
        <td>ID</td>
        <td>{$kkn_angkatan.ID_KKN_ANGKATAN}</td>
    </tr>
    <tr>
        <td>Kode Angkatan</td>
        <td><input class="form-control" type="text" name="kode_angkatan" value="{$kkn_angkatan.KODE_ANGKATAN}" size="25" />
        </td>
    </tr>
    <tr>
        <td>Semester</td>
        <td>{$nm_semester}</td>
    </tr>
    <tr>
        <td>Nama Angkatan</td>
        <td><input class="form-control" type="text" name="nama_angkatan" value="{$kkn_angkatan.NAMA_ANGKATAN}" size="75" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

<a href="lppm-kkn-angkatan.php">Kembali</a>