<script>
	
	$(document).ready(function(){
		$( "#btnSimpanKorkab{$id_dosen}" ).button({
			text: true
		});
		$( "#btnBatalKorkab{$id_dosen}" ).button({
			text: true
		});

		$('#btnSimpanKorkab{$id_dosen}').click(function(){	
			
			$('#frmSimpanKorkab').submit();
		});
		
		$('#btnBatalKorkab{$id_dosen}').click(function(){	
			$('#get-status-korkab{$id_dosen}').show();
			$('#get-provinsi-korkab{$id_dosen}').hide();
			$('#get-kabupaten-korkab{$id_dosen}').hide();

		});
		
		$('#frmSimpanKorkab').submit(function() {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {	
					$('#get-status-korkab{$id_dosen}').show();
					$('#get-provinsi-korkab{$id_dosen}').hide();
					$('#get-kabupaten-korkab{$id_dosen}').hide();
					$('#get-status-korkab{$id_dosen}').load('KorkabController.php?act=status&id_dosen='+'{$id_dosen}');
				}
			})
			return false;
		});
	});
</script>

<form id="frmSimpanKorkab" name="frmSimpanKorkab" action="KorkabController.php" method="POST">
<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;background:#ededed;padding-bottom:10px; padding-left:5px; padding-right:5px; padding-top:10px; margin-right:5px;  border-color:#ccc;border-bottom-width:1px;border-bottom-style:solid;">
	<span style="float:left;">Kabupaten/Kota :</span> 
	<input type="hidden" value="simpan" name="act" id="act" />
	<input type="hidden" value="kabupaten" name="src" id="src" />
	<input type="hidden" value="{$id_dosen}" name="id_dosen" id="id_dosen" />
	<select id="kab-korkab{$id_dosen}{$i}" name="id_kabupaten" style="float:right; width:100px;">
		{for $a = 0 to $jml_kabupaten-1}
		<option value="{$id_kabupaten[$a]}">{$nm_kabupaten[$a]}</option>
		{/for}
	</select>
	<div style="clear:both;">
	</div>
</div>
<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;margin-right:5px;padding-top:10px;padding-bottom:10px;background:#ededed;">
	<div id="btnBatalKorkab{$id_dosen}" style="float:right;margin-left:10px;margin-right:10px;background:#FFF;color:#000;width:65px;">
		Batal
	</div>
	<div id="btnSimpanKorkab{$id_dosen}" style="float:right;">
		Simpan
	</div>
	<div style="clear:both;">
	</div>

</div>
</form>