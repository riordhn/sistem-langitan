<script>
	$(document).ready(function() {
		$('#kota').load('get-kota.php');
		$('#propinsi').change(function(){		
			var id_propinsi = $('#propinsi').val();
			$('#kota').load('get-kota.php?id_propinsi='+id_propinsi);
		})
		
		$( "#btn-add" ).button({
			text: true,
			icons: {
				primary: "ui-icon-plus"
			}
		});
	});
</script>
<table class="ui-widget" style="width:700px;">
	<tr>
		<td>Pilih Propinsi</td>
		<td>
			<form>
				<select id="propinsi" style="width:100%;">
					{for $i = 0 to $jml_propinsi-1}
					<option value="{$id_propinsi[$i]}">{$nm_propinsi[$i]}</option>
					{/for}
				</select>
			</form>
		</td>
	</tr>
	<tr>
		<td>Pilih Kota</td>
		<td>
			<div id="kota">
			</div>
		</td>
	</tr>
	<tr>
		<td>Input Nama Kelurahan</td>
		<td>
			<input type="text" style="width:75%;" id="input_kelurahan" />
			<button id="btn-add">tambah</button>
		</td>
	</tr>
</table>