<script>
	$(document).ready(function() {
		$( "#btn-add" ).button({
			text: true,
			icons: {
				primary: "ui-icon-plus"
			}
		});
		
		$( "#btn-cancel" ).button({
			text: true,
			icons: {
				primary: "ui-icon-arrowreturnthick-1-s"
			}
		});
		
		$( "#btn-save" ).button({
			text: true,
			icons: {
				primary: "ui-icon-check"
			}
		});
		
		$('#btn-save').click(function(){	
			$('#frmSimpanDPL').submit();
		})
		
		$('#frmSimpanDPL').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {	
							//$('#add-dpl').remove();
							//$('#display-area').show();
							$('#display-area').load('lppm-dpl-display.php');
						
						}
					})
					return false;
		});
	});
</script>
<form id="frmSimpanDPL" name="frmSimpanDPL" action="lppm-dpl-simpan.php" method="post">
	<table class="ui-widget" style="width:100%">
		<tr class="ui-widget-header">
			<td>
				
			</td>
			<td>
				#
			</td>
			<td>
				NAMA DOSEN
			</td>
			<td>
				STATUS
			</td>
		</tr>
		
		{for $i=0 to $jml_data-1}
		<tr>
			<td class="ui-widget-content"> 
				<input type="checkbox" value="{$id_dosen[$i]}" name="dosen[]" id="dosen{$i}"/>
			</td>
			<td class="ui-widget-content">
				{$i+1}
			</td>
			<td class="ui-widget-content">
				{$nm_dosen[$i]}
			</td>
			<td class="ui-widget-content"> 
				<div id="status{$i}">
					<script>
							$.get('get-status-calon-dpl.php?id_dosen='+{$id_dosen[$i]}, function(data){
								//$('#status{$i}').html(data);
								if(data=='DPL'){
									$('#status{$i}').html('<span style="color:blue;font-weight:normal;">' + data + '</span>');
									$('#dosen{$i}').remove();
									//$('#listcheck{$i}').attr('checked', 'checked');
									//$('#listcheck{$i}').attr("disabled", true);
								}
								else{
									$('#status{$i}').html('<span style="color:red;font-weight:normal;">' + data + '</span>');
								}
							});
					</script>
				</div>
			</td>
		</tr>
		{/for}
	</table>
	<button id="btn-save">Simpan</button>
</form>
