<script>
	$(document).ready(function() {
		//$('#kota').load('get-kota.php');
		$('#get-kota').change(function(){		
			var id_kota = $('#get-kota').val();
			$('#display').load('lppm-kelurahan-display.php?id_kota='+id_kota);
		})
		
		$( "#btn-add" ).button({
			text: true,
			icons: {
				primary: "ui-icon-plus"
			}
		});
	});
</script>
<form>
	<select id="get-kota">
		{for $i = 0 to $jml_kota-1}
		<option value="{$id_kota[$i]}">{$nm_kota[$i]}</option>
		{/for}
	</select>
</form>