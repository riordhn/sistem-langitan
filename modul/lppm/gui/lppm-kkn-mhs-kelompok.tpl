<script>
	$(document).ready(function() {
	
		$( "#btn-cari" ).button({
			text: true
		});
		
		$('#btn-cari').click(function(){
			var nim = document.getElementById('nim').value;
			$('#display-area').load('MhsKelompokController.php?nim='+nim+'&act=cari');
		})
		
		//$('#display-area').load('lppm-mhs-kel-display.php');
		$('#fakultas').change(function(){
			var fakultas = document.getElementById('fakultas').value;
			$('#prodi_area').load('get-prodi-mhs.php?fakultas='+fakultas);
		})
	});
</script>
<div id="page">
	<div class="center_title_bar">
		PENENTUAN KELOMPOK KKN MAHASISWA
		<button id="btn-cari" style="float:right;height:25px;">Cari</button>
		<input type="text" name="nim" id="nim" style="float:right;" title="Mencari Mahasiswa Berdasarkan NIM" />
		<div style="clear:both;">
		</div>
	</div>
	<span style="font-style:italic;">Note: Tampilkan Peserta KKN Berdasarkan Fakultas</span>
	<div id="add-number-area">

		
		<table class="ui-widget">
			<tr>
				<td>
					Pilih Fakultas
				</td>
				<td>
					<form>
						<select id="fakultas" name="kelurahan" style="width:100%;">
							{for $i=0 to $jml_data-1}
							<option value="{$id_fakultas[$i]}">{$nm_fakultas[$i]}</option>
							{/for}
						</select>
					</form>
				</td>
			</tr>
			<tr>
				<td>
					Pilih Prodi
				</td>
				<td>
					<div id="prodi_area">
					</div>
				</td>
			</tr>
		</table>
	</div>
	
	<div>
		&nbsp;
	</div>
	
	<div id="add-area">
	</div>
	
	<div id="display-area">
	</div>
</div>