	<style>
	#toolbar {
		padding: 10px 4px;
	}
	</style>
	<script>
	$(function() {
		$( "#beginning" ).button({
			text: false,
			icons: {
				primary: "ui-icon-seek-start"
			}
		});
		$( "#rewind" ).button({
			text: false,
			icons: {
				primary: "ui-icon-seek-prev"
			}
		});
		$( "#play" ).button({
			text: false,
			icons: {
				primary: "ui-icon-play"
			}
		})
		.click(function() {
			var options;
			if ( $( this ).text() === "play" ) {
				options = {
					label: "pause",
					icons: {
						primary: "ui-icon-pause"
					}
				};
			} else {
				options = {
					label: "play",
					icons: {
						primary: "ui-icon-play"
					}
				};
			}
			$( this ).button( "option", options );
		});
		$( "#stop" ).button({
			text: false,
			icons: {
				primary: "ui-icon-stop"
			}
		})
		.click(function() {
			$( "#play" ).button( "option", {
				label: "play",
				icons: {
					primary: "ui-icon-play"
				}
			});
		});
		$( "#forward" ).button({
			text: false,
			icons: {
				primary: "ui-icon-seek-next"
			}
		});
		$( "#end" ).button({
			text: false,
			icons: {
				primary: "ui-icon-seek-end"
			}
		});
		$( "#checkall" ).button({
			text: false,
			icons: {
				primary: "ui-icon-check"
			}
		});
		$( "#uncheckall" ).button({
			text: false,
			icons: {
				primary: "ui-icon-arrowreturnthick-1-n"
			}
		});
		$( "#shuffle" ).button();
		$( "#repeat" ).buttonset();
	});
	</script>




<script>
	$(document).ready(function(){
	
		$('#frmMhsKl .checkall').click(function(event) {
				 $('#btn-save').show();
				 $('#frmMhsKl input[type="checkbox"]').attr('checked', 'checked');
				 $('#frmMhsKl .uncheckall').removeAttr("checked");
		});
		
		$('#frmMhsKl .uncheckall').click(function(event) {
			   //$('#btn-save').hide();
			   $('#frmMhsKl input[type="checkbox"]').removeAttr("checked");
			   $('#frmMhsKl .uncheckall').attr('checked', 'checked');
		});
		
		$('#frmMhsKl .listcheck').click(function(event) {
			$('#btn-save').show();
		   $('#frmMhsKl .uncheckall').removeAttr("checked");
		   $('#frmMhsKl .checkall').removeAttr("checked");
		});
			
		$( "#btn-save" ).button({
			text: true
		});
		
		$( "#btn-calon" ).button({
			text: true
		});
		
		{for $i=0 to $jml_data-1}
			$('#checkbox_yes{$i}').click(function(){	
					$('#status{$i}').replaceWith('<div id="status{$i}"><img src="../../api/images/gif/facebook-loading.gif" /></div>');
					var id_mhs{$i} = $('#id_mhs{$i}').val();
					var dataString{$i} = 'id_mhs[]='+id_mhs{$i};
					$.ajax({
						type: 'POST',
						url: 'controller/lppm-calon-peserta-delete.php',
						data: dataString{$i},
						success: function(data) {	
							//$('#result').html(data);
							
							//$('#display-area').load('lppm-kkn-calon-peserta-display.php?prodi='+{$prodi});
							/*
							$('#status{$i}').load('get-status-kkn-mhs.php?id_mhs='+id_mhs{$i});
							$('#status{$i}').html('<span style="color:red;font-weight:normal;">' + data + '</span>');
							$('#checkbox_no{$i}').show();
							$('#checkbox_yes{$i}').hide();
							*/
							$('#status{$i}').load('get-status-kkn-mhs.php?id_mhs='+id_mhs{$i}, function(result){
								//$('#status{$i}').html(data);
								if(result=='PESERTA'){
									$('#status{$i}').html('<span style="color:blue;font-weight:normal;">' + result + '</span>');
									$('#checkbox_yes{$i}').show();
									$('#checkbox_no{$i}').hide();
									//$('#listcheck{$i}').remove();
									//$('#listcheck{$i}').attr('checked', 'checked');
									//$('#listcheck{$i}').attr("disabled", true);
								}
								else{
									$('#status{$i}').html('<span style="color:red;font-weight:normal;">' + result + '</span>');
									$('#checkbox_yes{$i}').hide();
									$('#checkbox_no{$i}').show();
								}
							})
							
						}
					})
					return false;
			});
			
			$('#checkbox_no{$i}').click(function(){	
					$('#status{$i}').replaceWith('<div id="status{$i}"><img src="../../api/images/gif/facebook-loading.gif" /></div>');					
					var id_mhs{$i} = $('#id_mhs{$i}').val();
					var dataString{$i} = 'id_mhs[]='+id_mhs{$i};
					$.ajax({
						type: 'POST',
						url: 'lppm-calon-peserta-insert.php',
						data: dataString{$i},
						success: function(data) {	
							//$('#result').html(data);
							
							//$('#display-area').load('lppm-kkn-calon-peserta-display.php?prodi='+{$prodi});
							/*
							$('#status{$i}').load('get-status-kkn-mhs.php?id_mhs='+id_mhs{$i});
							$('#status{$i}').html('<span style="color:red;font-weight:normal;">' + data + '</span>');
							$('#checkbox_no{$i}').show();
							$('#checkbox_yes{$i}').hide();
							*/
							$('#status{$i}').load('get-status-kkn-mhs.php?id_mhs='+id_mhs{$i}, function(result){
								//$('#status{$i}').html(data);
								if(result=='PESERTA'){
									$('#status{$i}').html('<span style="color:blue;font-weight:normal;">' + result + '</span>');
									$('#checkbox_yes{$i}').show();
									$('#checkbox_no{$i}').hide();
									//$('#listcheck{$i}').remove();
									//$('#listcheck{$i}').attr('checked', 'checked');
									//$('#listcheck{$i}').attr("disabled", true);
								}
								else{
									$('#status{$i}').html('<span style="color:red;font-weight:normal;">' + result + '</span>');
									$('#checkbox_yes{$i}').hide();
									$('#checkbox_no{$i}').show();
								}
							})
							
						}
					})
					return false;
			});
		{/for}
		
		$('#btn-save').click(function(){	
			$('#frmMhsKl').submit();
		})
		
		$('#btn-calon').click(function(){	
					$.ajax({
						type: 'POST',
						url: 'controller/lppm-calon-peserta-delete.php',
						data: $('#frmMhsKl').serialize(),
						success: function(data) {	
							$('#result').html(data);
							$('#display-area').load('lppm-kkn-calon-peserta-display.php?prodi='+{$prodi}+'&semester={$semester}');
							
						}
					})
					return false;
		})
		
		
		$('#frmMhsKl').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {	
							$('#result').html(data);
							$('#display-area').load('lppm-kkn-calon-peserta-display.php?prodi='+{$prodi}+'&semester={$semester}');
							
						}
					})
					return false;
		});
	});
</script>
<form id="frmMhsKl" name="frmMhsKl" action="lppm-calon-peserta-insert.php" method="post">
<div class="demo">

<span id="toolbar" class="ui-widget-header ui-corner-all">
	<!--<input id="checkall" type="checkbox" name="checkall2" class="checkall"  /><label for="checkall" class="checkall">Check all</label>	
	<input type="checkbox"  name="uncheck-all" id="uncheckall" class="uncheckall"/><label for="uncheckall" class="uncheckall">Uncheck all</label>-->
	<button id="btn-save" style="">Tetapkan Semua Sebagai <u title="PESERTA">Peserta</u> KKN</button>
	<button id="btn-calon" style="">Tetapkan Semua Sebagai <u title="CALON PESERTA">Calon Peserta</u> KKN</button>
</span>

</div><!-- End demo -->
<br /><br />

	{for $i=0 to $jml_data-1}
		{if $kelamin[$i]=='1'}
			{$jml_laki=$jml_laki+1}
		{else}
			{$jml_perempuan=$jml_perempuan+1}
		{/if}
	{/for}
	
	<div style="width:250px;float:right;">
		<div style="font-style:italic;padding:5px;" class="ui-state-highlight">Informasi jumlah peserta menurut jenis kelamin</div>
		<br />
		<table class="ui-widget" style="width:250px;">
			<tr>
				<td class="ui-widget-header">Kelamin </td> <td class="ui-widget-header">Jumlah</td>
			</tr>
			<tr>
				<td class="ui-widget-content">Laki-laki : </td> <td>{$jml_laki}</td>
			</tr>
			<tr>
				<td class="ui-widget-content">Perempuan : </td> <td>{$jml_perempuan}</td>
			</tr>
			<tr>
				<td class="ui-widget-content">TOTAL : </td> <td>{$jml_perempuan+$jml_laki}</td>
			</tr>
		</table>
	</div>

	<div style="width:500px;float:left;">
		<span style="font-style:italic;padding:5px;" class="ui-state-highlight">Detail data mahasiswa peserta KKN berdasarkan prodi</span>
		<br /><br />
		<table class="ui-widget" style="width:500px;">
			<tr class="ui-widget-header">
				<td style="width:30px;">
					&nbsp;
				</td>
				<td style="width:20px;">
					#
				</td>
				<td>
					NAMA MAHASISWA
				</td>
				<td>
					NIM
				</td>
				<td>
					JENIS KELAMIN
				</td>
				<td>
					STATUS
				</td>
			</tr>
			
			{for $i=0 to $jml_data-1}
			<tr>
				<td class="ui-widget" align="center"> 
					<center>
					<input type="hidden" value="{$id_mhs[$i]}" name="id_mhs[]" id="id_mhs{$i}"/>
					<div id="checkbox_no{$i}" style="display:none;">
						<img src="../../api/images/checkbox_no.png" />
					</div>
					<div id="checkbox_yes{$i}" style="display:none;">
						<img src="../../api/images/checkbox_yes.png" />
					</div>
					</center>
					<!--
					<input type="checkbox" value="{$id_mhs[$i]}" name="id_mhs[]" id="listcheck{$i}" class="listcheck"/>
					-->
				</td>
				<td class="ui-widget-content">
					{$i+1}
				</td>
				<td class="ui-widget-content">
					{$nm_mhs[$i]}
				</td>
				<td class="ui-widget-content">
					{$nim[$i]}
				</td>
				<td class="ui-widget">
					{if $kelamin[$i]=='1'}{$jml_laki==$jml_laki+1}
						LAKI - LAKI
					{else}
						{$jml_perempuan==$jml_perempuan+1}
						PEREMPUAN
					{/if}
				</td>
				<td class="ui-widget-content">
					<div id="status{$i}">
						<script>
							//$('#status{$i}').load('get-status-kkn-mhs.php?id_mhs='+{$id_mhs[$i]});
							
							$.get('get-status-kkn-mhs.php?id_mhs='+{$id_mhs[$i]}, function(data){
								//$('#status{$i}').html(data);
								if(data=='PESERTA'){
									$('#status{$i}').html('<span style="color:blue;font-weight:normal;">' + data + '</span>');
									$('#checkbox_yes{$i}').show();
									$('#checkbox_no{$i}').hide();
									//$('#listcheck{$i}').remove();
									//$('#listcheck{$i}').attr('checked', 'checked');
									//$('#listcheck{$i}').attr("disabled", true);
								}
								else{
									$('#status{$i}').html('<span style="color:red;font-weight:normal;">' + data + '</span>');
									$('#checkbox_yes{$i}').hide();
									$('#checkbox_no{$i}').show();
								}
							});
						</script>
					</div>
				</td>
			</tr>
			{/for}
		</table>

		<span id="result">
		</span>
	</div>
</form>