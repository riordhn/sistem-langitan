<script>
	$(document).ready(function(){
		$('#kab-kelompok-kkn').change(function(){
			var id_kab = $('#kab-kelompok-kkn').val();
			$('#get-kecamatan-kelompok-kkn{$id_dosen}').load('KelompokKknController.php?id_kab='+id_kab+'&act=kec');
		});
	});
</script>

<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;background:#ededed;padding-bottom:10px; padding-left:5px; padding-right:5px; padding-top:10px; margin-right:5px;  border-color:#ccc;border-bottom-width:1px;border-bottom-style:solid;">
	<span style="float:left;">Kabupaten/Kota :</span> 
	<input type="hidden" value="{$id_dosen}" name="id_dosen" id="id_dosen" />
	<select id="kab-kelompok-kkn" name="id_kabupaten" style="float:right; width:200px;">
		<option value="0">Pilih Kabupaten</option>
		{for $a = 0 to $jml_kabupaten-1}
		<option value="{$id_kabupaten[$a]}">{$nm_kabupaten[$a]}</option>
		{/for}
	</select>
	<div style="clear:both;">
	</div>
</div>