<div class="center_title_bar">PLOTING MAHASISWA KKN</div>
{if $smarty.get.mode!='detail'}
    <span class="ui-widget ui-button ui-state-focus" style="padding: 10px;margin:10px 0px ;width: 98%;">KKN ANGKATAN {$angkatan.NAMA_ANGKATAN}</span>
    <form id="pilihJenis" method="get" action="ploting-mhs.php" style="margin: 10px 0px">
        <label for="jenis">Pilih Jenis KKN</label>
        <select name="jenis" onchange="$('#pilihJenis').submit()">
            <option value="">Pilih</option>
            <option value="1" {if $smarty.get.jenis==1}selected="true"{/if}>Reguler</option>
            <option value="2" {if $smarty.get.jenis==2}selected="true"{/if}>Tematik</option>
            <option value="3" {if $smarty.get.jenis==3}selected="true"{/if}>Magang Khusus</option>
        </select>
        <input type="hidden" name="mode" value="ploting"/>
    </form>
{/if}
{if $smarty.get.mode=='ploting'}
    {if $total_kkn==0}
        <i style="color: red">Mohon Maaf Daftar Peserta KKN Kosong</i>
    {/if}
    <table style="width: 60%">
        <tr>
            <th colspan="4" class="center"> PESERTA KKN ANGKATAN {$angkatan.NAMA_ANGKATAN} </th>
        </tr>
        <tr>
            <th>NO</th>
            <th>FAKULTAS</th>
            <th>JUMLAH MHS</th>
            <th>ESTIMASI FAK <br/>(% Tiap Kelompok)</th>
        </tr>
        {foreach $e_fakultas as $ef}
            <tr>
                <td>{$ef@index+1}</td>
                <td>{$ef.NM_FAKULTAS}</td>
                <td>{number_format($ef.JML_PESERTA)}</td>
                <td>
                {if $total_kkn > 0}
                    {if $ef.JML_PESERTA==''}
                        {round((0/$total_kkn*100),3)} % <br/>
                    {else}
                        {round(($ef.JML_PESERTA/$total_kkn*100),3)} % 
                    {/if}
                {else}
                    Belum Ada Mahasiswa Mengambil KKN Pada Semester Ini
                {/if}
                </td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="2" class="center">TOTAL</td>
            <td colspan="2" class="center">
                TOTAL SEMUA : {$total_sudah+$total_belum}<br/>
                SUDAH DAPAT KELOMPOK :{$total_sudah}<br/>
                BELUM DAPAT KELOMPOK :{$total_belum}<br/>
                QUOTA KELOMPOK KOSONG : {$quota_kel}
            </td>
        </tr>
    </table>
    <table style="width: 60%">
        <tr>
            <th>NO</th>
            <th>JENIS KELAMIN</th>
            <th>JUMLAH MHS</th>
            <th>ESTIMASI GENDER <br/>(% Tiap Kelompok)</th>
        </tr>
        {foreach $e_gender as $eg}
            <tr>
                <td>{$eg@index+1}</td>
                <td>{if $eg.KELAMIN_PENGGUNA==1}Laki-laki{else if $eg.KELAMIN_PENGGUNA==2}Perempuan{/if}</td>
                <td>{number_format($eg.JML_PESERTA)}</td>
                <td>
                    {if $ef.JML_PESERTA==''}
                        {round((0/($total_kkn)*100),3)} % <br/>
                    {else}
                        {round(($eg.JML_PESERTA/$total_kkn*100),3)} % 
                    {/if}
                </td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="2" class="center">TOTAL</td>
            <td colspan="2" class="center">
                TOTAL SEMUA : {$total_sudah+$total_belum}<br/>
                SUDAH DAPAT KELOMPOK :{$total_sudah}<br/>
                BELUM DAPAT KELOMPOK :{$total_belum}<br/>
                QUOTA KELOMPOK KOSONG : {$quota_kel}
            </td>
        </tr>
    </table>
    <table style="width: 98%">
        <tr>
            <th colspan="8" class="center">KELOMPOK KKN {$angkatan.NAMA_ANGKATAN} ( {if $smarty.get.jenis==1}Reguler{else if $smarty.get.jenis==2}Tematik{else}Magang/Khusus{/if})</th>
        </tr>
        <tr>
            <td colspan="8" class="center">
                <form method="post" action="ploting-mhs.php?{$smarty.server.QUERY_STRING}">
                    <input type="hidden" name="mode" value="plot"/>
                    <i style="color: green">Tekan Tombol Plot sampai Quota Kelompok Terpenuhi</i><br/><br/>
                    <input class="ui-button ui-corner-all ui-state-default" style="color: green" type="submit" value="PLOTING"/><br/><br/>
                    <a style="padding: 5px;color: #0088cc" class="disable-ajax ui-button ui-corner-all ui-state-default" target="_blank" href="excel-ploting-mhs.php?jenis={$smarty.get.jenis}">DOWNLOAD EXCEL </a>
                </form>
                <form method="post" action="ploting-mhs.php?{$smarty.server.QUERY_STRING}" style="margin: 10px">
                    <input type="hidden" name="mode" value="reset"/>
                    <input class="ui-button ui-corner-all ui-state-default" style="color: red" type="submit" value="RESET PLOTING"/>
                </form>
            </td>
        </tr>
        <tr>
            <th>NO</th>
            <th>NAMA</th>
            <th>KELURAHAN</th>
            <th>JENIS KKN</th>
            <th>QUOTA</th>
            <th>TERISI</th>
            <th>STATUS</th>
            <th>DETAIL</th>
        </tr>
        {foreach $kel_kkn as $k}
            <tr>
                <td>{$k@index+1}</td>
                <td>{$k.NAMA_KELOMPOK}</td>
                <td>{$k.NM_KELURAHAN}</td>
                <td>{if $k.JENIS_KKN==1}Reguler {else} Tematik{/if}</td>
                <td>{$k.QUOTA}</td>
                <td>{$k.TERISI}</td>
                <td class="center">
                    {if $k.TERISI>=$k.QUOTA}
                        <b style="color: red;font-family: Trebuchet MS"><i>Sudah Penuh</i></b>
                    {else if $k.TERISI<$k.QUOTA&&$k.TERISI!=0}
                        <b style="color: orange;font-family: Trebuchet MS"><i>Belum Penuh</i></b>
                    {else if $k.TERISI<$k.QUOTA&&$k.TERISI==0}
                        <b style="color: green;font-family: Trebuchet MS"><i>Kosong</i></b>
                    {/if}
                </td>
                <td>
                    <a style="padding: 5px" class="ui-button ui-corner-all ui-state-default" href="ploting-mhs.php?mode=detail&kel={$k.ID_KKN_KELOMPOK}">DETAIL</a>
                </td>
            </tr>
        {/foreach}
    </table>

{else if $smarty.get.mode=='detail'}
    <table style="width: 90%">
        <tr>
            <th colspan="6">DAFTAR MAHASISWA KKN {$kelompok.NAMA_KELOMPOK} , KELURAHAN {$kelompok.NM_KELURAHAN}</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>NAMA</th>
            <th>NIM</th>
            <th>GENDER</th>
            <th>PRODI</th>
            <th>FAKULTAS</th>
        </tr>
        {foreach $mhs_kelompok as $m}
            <tr>
                <td>{$m@index+1}</td>
                <td>{$m.NM_PENGGUNA}</td>
                <td>{$m.NIM_MHS}</td>
                <td>{if $m.KELAMIN_PENGGUNA==1}Laki-Laki {else if $m.KELAMIN_PENGGUNA==2} Perempuan{/if}</td>
                <td>{$m.NM_PROGRAM_STUDI}</td>
                <td>{$m.NM_FAKULTAS}</td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="6" class="center">Data Kosong</td>
            </tr>
        {/foreach}
    </table>
    <a style="padding: 5px" class="disable-ajax ui-button ui-corner-all ui-state-default" onclick="history.back()">Kembali</a>
{/if}

