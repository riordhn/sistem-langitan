{if $kelompok>0}
<script>
	$(document).ready(function(){
		{for $i=0 to $jml_kelompok-1}
			$("#btnDelDplKelompok{$id_dosen}{$i}").mouseover(function(){
			  $(this).css('cursor', 'pointer');
			  $(this).css('text-decoration', 'underline');
			}).mouseleave(function(){
			  $(this).css('cursor', 'none');
			  $(this).css('text-decoration', 'none');
			});
			$("#btnDelDplKelompok{$id_dosen}{$i}").mouseover(function(){
			  $(this).css('cursor', 'pointer');
			  $(this).css('text-decoration', 'underline');
			}).mouseleave(function(){
			  $(this).css('cursor', 'none');
			  $(this).css('text-decoration', 'none');
			});

			//
			$("#btnDelDplKelompok{$id_dosen}{$i}").click(function(){
				$('#frmDelKelompokDpl{$id_dosen}{$i}').submit();
			});

			$('#frmDelKelompokDpl{$id_dosen}{$i}').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {	
						$('#get-status-dpl{$id_dosen}').load('DplController.php?act=status&id_dosen='+'{$id_dosen}');
					}
				})
				return false;
			});
		{/for}
	});

</script>
{for $i=0 to $jml_kelompok-1}
<form id="frmDelKelompokDpl{$id_dosen}{$i}" action="DplController.php" method="POST">
<div class="ui-button-text" style="font-size:11px;font-weight:bold;background:#ededed;padding:10px;margin-right:5px;border-bottom-color:#ccc;border-bottom-style:solid;border-bottom-width:1px;box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;">
	<input type="hidden" value="{$id_dosen}" name="id_dosen" id="id_dosen" />
	<input type="hidden" value="delete" name="act" id="act" />
	<input type="hidden" value="{$id_dpl[$i]}" name="id_dpl" id="id_dpl" />
	<div style="float:left;" title="Sebagai Koordinator Pembimbing {$kelompok[$i]}">
	{$kelompok[$i]}
	</div>
	<div id="btnDelDplKelompok{$id_dosen}{$i}" style="float:right;" title="Hapus">
	x
	</div>
	<div style="clear:both;">
	</div>
</div>
</form>
{/for}
{/if}