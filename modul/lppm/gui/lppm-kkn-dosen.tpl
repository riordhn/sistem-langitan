{literal}
	<link rel="stylesheet" type="text/css" href="../../css/tagit-simple-blue.css"/>	
	<!-- <link href="../../css/demo.css" rel="stylesheet" type="text/css"/>-->
    <script src="../../js/tagit.js"></script>
<script>
	$(function() {
	
		$("#btnTampil").button({
			text: true
		});
		
		$('#btnTampil').click(function() {
		
			$('#formTampil').submit();
					
		});

		$("#btnTampilSemua").button({
			text: true
		});
		
		$('#btnTampilSemua').click(function() {
		
			$('#container').load('get-kkn-dosen.php');
					
		});
		
		$('#formTampil').submit(function() {
					$.ajax({
						type: 'POST',
						url: $(this).attr('action'),
						data: $(this).serialize(),
						success: function(data) {
							$('#container').html(data);											
						}
					})
					
					return false;
					
		});

		$('#container').load('get-kkn-dosen.php');

	});
</script>

{/literal}
<div class="center_title_bar">
	PENGELOLA KKN
</div>

<div id="dialog-message" title="Pemberitahuan" style="display:none;">
	<p>
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
		Anda belum mengisi pesan.
	</p>
	<p>
		Silakan mengisi pesan yang ingin anda sampaikan dengan ketentuan jumlah Max. karakter adalah 160.
	</p>
</div>
<div id="wrap">
    <div class="box">

        <div class="box">
            
					<form id="formTampil" name="formTampil" action="get-kkn-dosen.php" method="post">
						<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;background:#ededed;padding-bottom:10px; padding-top:10px; padding-left:5px; padding-right:5px; margin-right:5px; border-color:#ccc;border-bottom-width:1px;border-bottom-style:solid;">
						<div class="note">Tulis Nama Dosen : </div>
						<ul id="cari" name="cari[]" class="cari"></ul>
						</div>
						<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;background:#ededed;padding-bottom:10px; padding-top:10px; padding-left:5px; padding-right:5px; margin-right:5px; border-color:#ccc;border-bottom-width:1px;border-bottom-style:solid;">

						<div id="btnTampilSemua" title="Tampilkan Semua Dosen" style="background:#fff;color:#000;float:right;margin-left:5px;">Tampilkan</div>
						<div id="btnTampil" title="Pencarian" style="float:right;">Tambahkan</div>
						<div style="clear:both;">
						</div>
						</div>
					</form>
			<div>
				<div style="margin-bottom:10px;width:625px;">
				</div>
			</div>
        </div>
		
    </div>
</div>

<div id="container">
</div>