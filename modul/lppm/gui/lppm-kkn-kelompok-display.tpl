<style>
input{
	width: 100%;
}
</style>
{if $jml_data>0}
<script language="javascript">
	$(document).ready(function() {
		
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		{for $i=0 to $jml_data-1}
			$( "#btn-edit{$i}" ).button({
				text: false,
				icons: {
					primary: "ui-icon-pencil"
				}
			});
			
			$( "#btn-update{$i}" ).button({
				text: true,
				icons: {
					primary: "ui-icon-check"
				}
			});
			
			$( "#btn-delete{$i}" ).button({
				text: false,
				icons: {
					primary: "ui-icon-close"
				}
			});
			
			$('#btn-edit{$i}').click(function(){	
				var id_kecamatan = document.getElementById('id_kecamatan{$i}').value;
				
				var id = document.getElementById('id{$i}').value;
				
				
				//load kelurahan by id_kecamatan
				$('#id_kelurahan{$i}').load('lppm-get-kelurahan.php?id_kecamatan='+id_kecamatan+'&id='+id);
				
				//load jenis kkn
				$("#jenis_kkn{$i}").load("get-jenis-kkn.php?id="+id);
				
				$('#btn-edit{$i}').hide();
				$('#btn-delete{$i}').hide();
				$('#div_nm_kkn_kelompok{$i}').hide();
				$('#div_kelurahan{$i}').hide();
				$('#quota{$i}').hide();
				
				$('#btn-update{$i}').show();
				$('#ed_nm_kkn_kelompok{$i}').show();
				$('#ed_kelurahan{$i}').show();
				$('#quota_edit{$i}').show();
				
			});
			
			$("#jenis_kkn{$i}").change(function(){
				var jenis_kkn = $("#jenisKkn{$i}").val();
				$("#jenis_kkn_edit{$i}").val(jenis_kkn);
			});
			
			$('#btn-update{$i}').click(function(){	
				$('#formUpdate{$i}').submit();
			});
			

			$('#id_kelurahan{$i}').change(function(){
				var id_kelurahan = document.getElementById('selectKelurahan{$i}').value;
				//alert(id_kelurahan);
				$('#div_kelurahan{$i}').load('lppm-get-kelurahan.php?id_kelurahan='+id_kelurahan);
			});
	
			
			$('#btn-delete{$i}').click(function(){	
				$( "#dialog-confirm{$i}" ).dialog({
					resizable: false,
					height:175,
					modal: true,
					buttons: {
						"Delete items": function() {
							$( this ).dialog( "close" );
							$('#formDelete{$i}').submit();
						},
						Cancel: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			});
			
			
			$('#frmGetKelurahan{$i}').submit(function() {
						var id_kelurahan = document.getElementById('id_kelurahan{$i}').value;
						var dataString = 'id_kelurahan='+ id_kelurahan;
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: dataString,
							success: function(data) {	
								
								$('#div_kelurahan{$i}').load('lppm-get-kelurahan.php');
							
							}
						})
						return false;
			});
			
			
			
			$('#formUpdate{$i}').submit(function() {
						var id_kelompok = $('#id_kelompok{$i}').val();
						var id_kelurahan = $('#selectKelurahan{$i}').val();
						var quota = $("#quota_input{$i}").val();
						var jenis_kkn = $("#jenis_kkn_edit{$i}").val();
						var nama_kelompok = $('#edit_nm_kkn_kelompok{$i}').val();
						var dataString = 'id_kelompok='+ id_kelompok + '&nama_kelompok=' + nama_kelompok + '&id_kelurahan=' + id_kelurahan + '&quota=' + quota + '&jenis_kkn=' + jenis_kkn;  
						
						$.ajax({
							url : $(this).attr('action'),
							data : dataString,
							type : 'post',
							success: function(data) {	
								
								$('#btn-edit{$i}').show();
								$('#btn-delete{$i}').show();
								$('#div_nm_kkn_kelompok{$i}').show();
								$('#div_kelurahan{$i}').show();
								$("#quota{$i}").show();
								
								$('#btn-update{$i}').hide();
								$('#ed_nm_kkn_kelompok{$i}').hide();
								$('#ed_kelurahan{$i}').hide();
								$("#quota_edit{$i}").hide();
									
								
								var nama_kelompok_div = document.getElementById('div_nm_kkn_kelompok{$i}');
								nama_kelompok_div.innerHTML = document.getElementById('edit_nm_kkn_kelompok{$i}').value;
								
								$("#quota{$i}").load("get-quota-kelompok.php?id="+id_kelompok);
								$("#jenis_kkn{$i}").load('get-jenis-kkn.php?view='+id_kelompok);
							
							}
						})
						return false;
						
			});
			
			
			$('#formDelete{$i}').submit(function() {
						var id_kelompok = document.getElementById('id_kelompok{$i}').value;
						var dataString = 'id_kelompok='+ id_kelompok;
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: dataString,
							success: function(data) {								

								if(data=='sukses'){
								
									
									
									$('#notif').append('<div class="ui-state-highlight" style="padding:5px;">Berhasil menghapus kelompok</div><br />');
									$('#display-area').load('lppm-kkn-kelompok-display.php');
									//$('#notif').fade('slow', function() {
										// Animation complete.
									//});
								}
								else{
									$('#notif').append('<div class="ui-state-highlight" style="padding:5px;">Gagal menghapus kelompok, karena ada dosen / mahasiswa yang menggunakan kelompok tersebut</div><br />');
								}
								
							
							}
						})
						return false;
			});
			
		{/for}

		$('#btn-cancel').click(function(){		
			$('#add-skim-space').hide();

		})
		
		$('#btn-save').click(function(){		
			$('#formAddSkim').submit();
		})	

	});
</script>

<table class="ui-widget" width="100%" style="width:100%;">
	<tr class="ui-widget-header">
		<td>
			NO
		</td>
		<td>
			NAMA
		</td>
		<td>
			KELURAHAN			
		</td>
		<td>
			KECAMATAN			
		</td>
		<td>
			QUOTA
		</td>
		<td>
			JENIS KKN
		</td>
		<td style="width:80px;">
			ACTION
		</td>
	</tr>
	
	{for $i=0 to $jml_data-1}
	<div id="dialog-confirm{$i}" title="Hapus Data Skim Penelitian" style="display:none;">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
	</div>
	<form id="formDelete{$i}" name="formDelete{$i}" action="lppm-kkn-kelompok-delete.php" method="post">
		<input type="hidden" name="id_kelompok" id="id_kelompok{$i}" value="{$id_kkn_kelompok[$i]}" />
	</form>
	
	<form id="formUpdate{$i}" name="formUpdate{$i}" action="lppm-kkn-kelompok-update.php" method="POST">
	
	<tr>
		<td>
			{$i+1}
		</td>
		<td>
			<div id="div_nm_kkn_kelompok{$i}">
				{$nm_kkn_kelompok[$i]}
			</div>
			<div style="display:none;" id="ed_nm_kkn_kelompok{$i}">
				<input type="text" name="nama_kelompok" id="edit_nm_kkn_kelompok{$i}"  value="{$nm_kkn_kelompok[$i]}" />
				<input type="hidden" name="id_kelompok{$i}" id="id_kelompok{$i}" value="{$id_kkn_kelompok[$i]}" />
			</div>
		</td>
		<td>
			<div id="div_kelurahan{$i}">
				{$nm_kelurahan[$i]}
			</div>
			<div style="display:none;" id="ed_kelurahan{$i}">
				<input type="hidden" name="id_kecamatan{$i}" id="id_kecamatan{$i}" value="{$id_kecamatan[$i]}" />
				<input type="hidden" name="id{$i}" id="id{$i}" value="{$i}" />
				<form id="frmGetKelurahan" action="lppm-get-kelurahan.php" method="post">
					<div id="id_kelurahan{$i}" style="width:100%;">
					</div>
				</form>
			</div>
		</td>
		<td>
			<div id="div_kecamatan{$i}">
				{$nm_kecamatan[$i]}
			</div>
		</td>
		<td>
			<div style="width:50px;" id="quota{$i}">{$quota[$i]}</div>
			<div id="quota_edit{$i}" style="display:none;">
				<input style="width:50px;" type="text" value="{$quota[$i]}" id="quota_input{$i}" name="quota_input{$i}" />
			</div>
		</td>
		<td>
			<div id="jenis_kkn{$i}">
				{if $jenis_kkn[$i] == "1"}
				Reguler
				{else}
				Tematik
				{/if}
			</div>
			<div >
				<input type="hidden" name="jenis_kkn_edit{$i}" id="jenis_kkn_edit{$i}" value="{$jenis_kkn[$i]}"/>
			</div>
		</td>
		<td style="width:25px;">
			<div id="btn-edit{$i}">edit</div>
			<div id="btn-delete{$i}">hapus</div>
			<div style="display:none;" id="btn-update{$i}">update</div>
		</td>
	</tr>
	</form>
	{/for}
</table>
{/if}