<script>
	
	$(document).ready(function(){
		$( "#btnSimpanDpl{$id_dosen}" ).button({
			text: true
		});
		$( "#btnBatalDpl{$id_dosen}" ).button({
			text: true
		});

		$('#btnSimpanDpl{$id_dosen}').click(function(){	
			
			$('#frmSimpanDpl{$id_dosen}').submit();
		});
		
		$('#btnBatalDpl{$id_dosen}').click(function(){	
			$('#get-status-dpl{$id_dosen}').show();
			$('#get-provinsi-dpl{$id_dosen}').hide();
			$('#get-kabupaten-dpl{$id_dosen}').hide();
			$('#get-kecamatan-dpl{$id_dosen}').hide();
			$('#get-kelompok-dpl{$id_dosen}').hide();

		});
		
		$('#frmSimpanDpl{$id_dosen}').submit(function() {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {	
					$('#get-status-dpl{$id_dosen}').show();
					$('#get-provinsi-dpl{$id_dosen}').hide();
					$('#get-kabupaten-dpl{$id_dosen}').hide();
					$('#get-kecamatan-dpl{$id_dosen}').hide();
					$('#get-kelompok-dpl{$id_dosen}').hide();
					$('#get-status-dpl{$id_dosen}').load('DplController.php?act=status&id_dosen='+'{$id_dosen}');
				}
			})
			return false;
		});
	});
</script>

<form id="frmSimpanDpl{$id_dosen}" name="frmSimpanDpl{$id_dosen}" action="DplController.php" method="POST">
<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;background:#ededed;padding-bottom:10px; padding-left:5px; padding-right:5px; padding-top:10px; margin-right:5px;  border-color:#ccc;border-bottom-width:1px;border-bottom-style:solid;">
	<span style="float:left;">Kelompok :</span> 
	<input type="hidden" value="simpan" name="act" id="act" />
	<input type="hidden" value="kelompok" name="src" id="src" />
	<input type="hidden" value="{$id_dosen}" name="id_dosen" id="id_dosen" />
	{if $jml_kelompok>0}
	<select id="kab-dpl{$id_dosen}{$i}" name="id_kelompok" style="float:right; width:100px;">
		{for $a = 0 to $jml_kelompok-1}
		<option value="{$id_kelompok[$a]}">{$nm_kelompok[$a]} / <b>{$jenis_kkn[$a]} </b></option>
		{/for}
	</select>
	{else}
	<select id="kab-dpl{$id_dosen}{$i}" name="id_kelompok" style="float:right; width:100px;" disabled>
		{for $a = 0 to $jml_kelompok-1}
		<option value="{$id_kelompok[$a]}">{$nm_kelompok[$a]} / <b>{$jenis_kkn[$a]}</b></option>
		{/for}
	</select>
	{/if}
	<div style="clear:both;">
	</div>
</div>
<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;margin-right:5px;padding-top:10px;padding-bottom:10px;background:#ededed;">
	<div id="btnBatalDpl{$id_dosen}" style="float:right;margin-left:10px;margin-right:10px;background:#FFF;color:#000;width:65px;">
		Batal
	</div>
	<div id="btnSimpanDpl{$id_dosen}" style="float:right;">
		Tambahkan
	</div>
	<div style="clear:both;">
	</div>

</div>
</form>