<div id="page">
	<div class="center_title_bar">
		INFORMASI JUMLAH PERSERTA KKN PER PRODI
	</div>
	<script>
		$(document).ready(function() {
			//$('#display-area').load('lppm-mhs-kel-display.php');
			$('#fakultas').change(function(){
				var fakultas = document.getElementById('fakultas').value;
				$('#page').load('lppm-kkn-info-peserta.php?fakultas='+fakultas);
			})
		});
	</script>
	
	<table class="ui-widget">
		<tr>
			<td>
				Pilih Fakultas
			</td>
			<td>
				<form>
					<select id="fakultas" name="kelurahan" style="width:100%;">
						<option value="0" selected>
							PILIH FAKULTAS
						</option>
						{for $i=0 to $jml_fak-1}
						<option value="{$id_fakultas[$i]}">{$nm_fakultas[$i]}</option>
						{/for}
					</select>
				</form>
			</td>
		</tr>
	</table>
	<table class="ui-widget" style="width:100%">
		<tr class="ui-widget-header">
			<td>
				NO
			</td>
			<td>
				NAMA PRODI
			</td>
			<td>
				JUMLAH PESERTA KKN
			</td>
		</tr>
		
		{for $i=0 to $jml_data-1}
		<tr>
			<td class="ui-widget-content">
				{$i+1}
			</td>
			<td class="ui-widget-content">
				{$nm_prodi[$i]}
			</td>
			<td class="ui-widget-content"> 
				{$jml_peserta[$i]}
			</td>
		</tr>
		{/for}
	</table>
	
	<table style="width:100%;">
		<th>
			#
		</th>
		<th>
			NAMA
		</th>
		<th>
			PRODI
		</th>
		<th>
			FAKULTAS
		</th>
		<th>
			KELOMPOK
		</th>
		<th>
			JENIS KKN
		</th>
		{foreach $kelompok as $data}
		<tr>
			<td>
				{$data@index + 1}
			</td>
			<td>
				{$data.NAMA}
			</td>
			<td>
				{$data.PRODI}
			</td>
			<td>
				{$data.FAKULTAS}
			</td>
			<td>
				{$data.NAMA_KELOMPOK}
			</td>
			<td>
				{if $data.JENIS_KKN == 1}
					<font color="black">REGULER</font>
				{else if $data.JENIS_KKN ==2}
					<font color="blue">TEMATIK</font>
				{else}
					<font color="red">BELUM</font>
				{/if}
			</td>
		</tr>
	
		{/foreach}
	</table>
</div>