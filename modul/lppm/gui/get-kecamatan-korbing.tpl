<script>
	$(document).ready(function(){
		/*
		$('#kec-korbing{$id_dosen}{$i}').change(function(){
			var id_kec = $('#kec-korbing{$id_dosen}{$i}').val();
			$('#get-kelurahan-korbing{$id_dosen}').load('KorbingController.php?id_dosen='+'{$id_dosen}'+'&i='+'{$i}'+'&id_kec='+id_kec+'&act=kel');
		});
		*/
		
		$( "#btnSimpanKorbing{$id_dosen}" ).button({
			text: true
		});
		$( "#btnBatalKorbing{$id_dosen}" ).button({
			text: true
		});

		$('#btnSimpanKorbing{$id_dosen}').click(function(){	
			
			$('#frmSimpanKorbing{$id_dosen}').submit();
		});
		
		$('#btnBatalKorbing{$id_dosen}').click(function(){	
			$('#get-status-korbing{$id_dosen}').show();
			$('#get-provinsi-korbing{$id_dosen}').hide();
			$('#get-kabupaten-korbing{$id_dosen}').hide();
			$('#get-kecamatan-korbing{$id_dosen}').hide();
			$('#get-kelurahan-korbing{$id_dosen}').hide();

		});
		
		$('#frmSimpanKorbing{$id_dosen}').submit(function() {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {	
					$('#get-status-korbing{$id_dosen}').show();
					$('#get-provinsi-korbing{$id_dosen}').hide();
					$('#get-kabupaten-korbing{$id_dosen}').hide();
					$('#get-kecamatan-korbing{$id_dosen}').hide();
					$('#get-kelurahan-korbing{$id_dosen}').hide();
					$('#get-status-korbing{$id_dosen}').load('KorbingController.php?act=status&id_dosen='+'{$id_dosen}');
				}
			})
			return false;
		});
		
	});
</script>

<form id="frmSimpanKorbing{$id_dosen}" name="frmSimpanKorbing{$id_dosen}" action="KorbingController.php" method="POST">
<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;background:#ededed;padding-bottom:10px; padding-left:5px; padding-right:5px; padding-top:10px; margin-right:5px;  border-color:#ccc;border-bottom-width:1px;border-bottom-style:solid;">
	<span style="float:left;">Kecamatan :</span> 
	<input type="hidden" value="simpan" name="act" id="act" />
	<input type="hidden" value="{$id_dosen}" name="id_dosen" id="id_dosen" />
	<select id="kec-korbing{$id_dosen}{$i}" name="id_kecamatan" style="float:right; width:100px;">
		<option value="0">Pilih Kecamatan</option>
		{for $a = 0 to $jml_kecamatan-1}
		<option value="{$id_kecamatan[$a]}">{$nm_kecamatan[$a]}</option>
		{/for}
	</select>
	<div style="clear:both;">
	</div>
</div>

<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;margin-right:5px;padding-top:10px;padding-bottom:10px;background:#ededed;">
	<div id="btnBatalKorbing{$id_dosen}" style="float:right;margin-left:10px;margin-right:10px;background:#FFF;color:#000;width:65px;">
		Batal
	</div>
	<div id="btnSimpanKorbing{$id_dosen}" style="float:right;">
		Tambahkan
	</div>
	<div style="clear:both;">
	</div>

</div>
</form>