<script>
	$(document).ready(function() {
		
		$('#prodi').change(function(){	
			var prodi = document.getElementById('prodi').value;
			var semester = document.getElementById('semester').value;
			
			$('#display-area').load('lppm-kkn-calon-peserta-display.php?prodi='+prodi+'&semester='+semester);
		})
	});
</script>
		
<form>
	<select id="prodi" name="prodi" style="width:100%;">
		<option selected>--PROGRAM STUDI--</option>
		{for $i=0 to $jml_prodi-1}
		<option value="{$id_prodi[$i]}">[ {$jenjang[$i]} ] {$nm_prodi[$i]}</option>
		{/for}
	</select>
</form>
