<div class="center_title_bar">KONTROL PENILAIAN KKN DARI DPL</div>
<form id="kontrol" action="kontrol-nilai-kkn.php" method="get">
    <table style="width: 99%">
        <tr>
            <td style="width: 10%">Angkatan</td>
            <td style="width: 20%">
                <select id="angk" name="angkatan">
                    <option value="">Pilih Angkatan</option>
                    {foreach $data_angkatan as $da}
                        <option {if $smarty.get.angkatan==$da.ID_KKN_ANGKATAN}selected="true"{/if} value="{$da.ID_KKN_ANGKATAN}">{$da.NAMA_ANGKATAN}</option>
                    {/foreach}
                </select>
            </td>
            <td>Kelompok KKN</td>
            <td>
                <select id="kel" onchange="$('#kontrol').submit()" name="kelompok">
                    <option value="">Semua</option>
                    {foreach $data_kelompok as $dk}
                        {if $dk.NM_PENGGUNA!=''}
                            <option {if $smarty.get.kelompok==$dk.ID_KKN_KELOMPOK}selected="true"{/if} value="{$dk.ID_KKN_KELOMPOK}">{if $dk.JENIS_KKN==1}Reguler{else}Tematik{/if} {$dk.NAMA_KELOMPOK} ({$dk.NM_PENGGUNA})</option>
                        {else}
                            <option {if $smarty.get.kelompok==$dk.ID_KKN_KELOMPOK}selected="true"{/if} style="color: red" value="{$dk.ID_KKN_KELOMPOK}">{if $dk.JENIS_KKN==1}Reguler{else}Tematik{/if} {$dk.NAMA_KELOMPOK} DPL Tidak Ada</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="center">
                <input type="hidden" name="mode" value="tampil"/>
                <input type="submit" value="Tampilkan" style="padding: 5px" class="ui-button ui-corner-all ui-state-default"/>
            </td>
        </tr>
    </table>
</form>
{if $data_rekap!=''}
    {if $smarty.get.kelompok!=''}
        <form action="kontrol-nilai-kkn.php?{$smarty.server.QUERY_STRING}" method="post">
            <table class="ui-widget-content" style="width: 98%">
                <tr class="ui-widget-header">
                    <th class="center" colspan="8" class="header-coloumn">Mahasiswa KKN Kelompok</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>NO</th>
                    <th>NIM</th>
                    <th>NAMA</th>
                    <th>PROGRAM STUDI</th>
                    <th>FAKULTAS</th>
                    <th>JALUR</th>
                    <th>NILAI ANGKA KKN</th>
                    <th>NILAI HURUF KKN</th>
                </tr>
                {$index_nilai=1}
                {foreach $data_rekap as $m}
                    <tr>
                        <td>{$m@index+1}</td>
                        <td>{$m.NIM_MHS}</td>
                        <td>{$m.NM_PENGGUNA}</td>
                        <td>{$m.NM_JENJANG} {$m.NM_PROGRAM_STUDI}</td>
                        <td>{$m.NM_FAKULTAS}</td>
                        <td>{$m.NM_JALUR}</td>
                        <td class="center">
                            {if $m.ID_PENGAMBILAN_MK!=''}
                                <input type="text" name="nilai{$index_nilai}"  size="4" maxlength="3" value="{$m.NILAI_ANGKA}"/>
                                <input type="hidden" name="pengambilan{$index_nilai}" value="{$m.ID_PENGAMBILAN_MK}"/>
                                {$index_nilai=$index_nilai+1}
                            {else}
                                <span class="kosong" style="font-size: 0.8em">KRS KKN Kosong</span>
                            {/if}
                        </td>
                        <td class="center">{$m.NILAI_HURUF}</td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="8" class="kosong">Data Masih Kosong</td>
                    </tr>
                {/foreach}
                <tr>
                    <td colspan="8" class="center">
                        <input type="hidden" name="mode" value="ubah-nilai"/>
                        <input type="hidden" name="jumlah_nilai" value="{$index_nilai}"/>
                        <div style="padding:5px;" class="ui-button ui-state-default ui-corner-all"><a class='disable-ajax' href='export-nilai-kkn.php?angkatan={$smarty.get.angkatan}&kelompok={$smarty.get.kelompok}&mode=export'>Export Excel</a></div>
                        <input type="submit" style="padding:5px" value="Ubah Nilai" class="ui-button ui-state-default ui-corner-all"/>
                    </td>
                </tr>
            </table>
        </form>
    {else}
        {foreach $data_rekap as $r}
            <span class="ui-state-highlight" style="padding: 15px;display: block;margin-top: 10px;margin-bottom: 10px;font-style: italic;font-weight: bold;font-size: 1.2em">
                Pembina {$r.KELOMPOK.NM_PENGGUNA} ({$r.KELOMPOK.USERNAME})<br/>
                Nama Kelompok {$r.KELOMPOK.NAMA_KELOMPOK} ({if $r.KELOMPOK.JENIS_KKN==1}Reguler{else}Tematik{/if}) 
            </span>
            <table class="ui-widget-content" style="width: 98%">
                <tr class="ui-widget-header">
                    <th colspan="8" class="header-coloumn">Mahasiswa KKN Kelompok</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>NO</th>
                    <th>NIM</th>
                    <th>NAMA</th>
                    <th>PROGRAM STUDI</th>
                    <th>FAKULTAS</th>
                    <th>JALUR</th>
                    <th>NILAI ANGKA KKN</th>
                    <th>NILAI HURUF KKN</th>
                </tr>
                {foreach $r.DATA_MHS as $m}
                    <tr>
                        <td>{$m@index+1}</td>
                        <td>{$m.NIM_MHS}</td>
                        <td>{$m.NM_PENGGUNA}</td>
                        <td>{$m.NM_JENJANG} {$m.NM_PROGRAM_STUDI}</td>
                        <td>{$m.NM_FAKULTAS}</td>
                        <td>{$m.NM_JALUR}</td>
                        <td class="center">{$m.NILAI_ANGKA}</td>
                        <td class="center">{$m.NILAI_HURUF}</td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="8" class="kosong">Data Masih Kosong</td>
                    </tr>
                {/foreach}
            </table>
        {/foreach}
    {/if}
{/if}
{literal}
    <script type="text/javascript">
                    $('#angk').change(function() {
                        $.ajax({
                            url: 'GetKelompokPerAngkatan.php',
                            type: 'get',
                            data: 'angkatan=' + $(this).val(),
                            success: function(data) {
                                $('#kel').html(data);
                            }
                        });
                    });
    </script>
{/literal}
