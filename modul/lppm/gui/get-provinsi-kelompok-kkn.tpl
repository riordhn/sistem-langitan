<script>
	$(document).ready(function(){
			$("#prov-kelompok-kkn").change(function(){
				var id_prov = $('#prov-kelompok-kkn').val();
				$('#get-kabupaten-kelompok-kkn{$id_dosen}').load('KelompokKknController.php?id_prov='+id_prov+'&act=kab');
			});
	});

</script>

<div style="box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;background:#ededed;padding-bottom:10px; padding-top:10px; padding-left:5px; padding-right:5px; margin-right:5px; border-color:#ccc;border-bottom-width:1px;border-bottom-style:solid;">
	<span style="float:left; ">Provinsi :</span> 
	<select id="prov-kelompok-kkn" style="float:right; width:200px;">
		<option value="0">Pilih Provinsi</option>
		{for $a = 0 to $jml_provinsi-1}
		<option value="{$id_provinsi[$a]}">{$nm_provinsi[$a]}</option>
		{/for}
	</select>
	<div style="clear:both;">
	</div>
</div>