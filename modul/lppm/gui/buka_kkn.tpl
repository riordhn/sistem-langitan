<div class="center_title_bar">
	PUBLIKASI KKN
</div>

<div>
	<form id="formBukaKkn" name="formBukaKkn" action="buka_kkn.php" method="post">
		<table>
			<!--
			<th>
				#
			</th>
			-->
			<th>
				NAMA
			</th>
			<th>
				KELURAHAN			
			</th>
			<th>
				QUOTA
			</th>
			<th>
				JENIS KKN
			</th>
			<th>
				STATUS
			</th>
			<th style="width:80px;">
				ACTION
			</th>
			<tbody>
				{foreach $kelompok as $data}
				<tr>
					<!--
					<td>
						
					</td>
					-->
					<td>
						{$data.NAMA_KELOMPOK}
					</td>
					<td>
						{$data.NM_KELURAHAN}
					</td>
					<td>
						{$data.QUOTA}
					</td>
					<td>
						
						{if $data.JENIS_KKN==1}
							REGULER
						{else}
							TEMATIK
						{/if}
					</td>
					<td>
						{if $data.STATUS_PUBLISH == 1}
							<font color="green"><b>PUBLISH</b></font>
						{else}
							<font color="red"><b>UNPUBLISH</b></font>
						{/if}
					</td>
					<td>					
						<a style="padding:5px;" class="ui-button ui-corner-all ui-state-default" href="ploting-mhs.php?mode=detail&kel={$data.ID_KKN_KELOMPOK}">LIHAT</a>
						{if $data.STATUS_PUBLISH == 1}
							<a style="padding:5px;" class="ui-button ui-corner-all ui-state-default" href="buka_kkn.php?kel={$data.ID_KKN_KELOMPOK}&unpublish">UNPUBLISH</a>
						{else}
							<a style="padding:5px;" class="ui-button ui-corner-all ui-state-default" href="buka_kkn.php?kel={$data.ID_KKN_KELOMPOK}&confirm">PUBLISH</a>
						{/if}						
						
					</td>
				<tr>
				{/foreach}
			</tbody>
		</table>
	</form>
</div>