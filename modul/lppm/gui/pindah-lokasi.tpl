<div class="center_title_bar">PINDAH LOKASI KKN</div>
<form method="get" action="pindah-lokasi.php">
    <table>
        <tr>
            <td>Cari NIM</td>
            <td><input type="text" name="cari"/></td>
            <td>
                <input type="hidden" name="mode" value="cari"/>
                <input type="submit" value="Cari" style="padding: 5px" class="ui-button ui-corner-all ui-state-default"/>
            </td>
        </tr> 
    </table>
</form>
{if $smarty.get.mode=='cari'}
    {if $mhs!=''}
        <table>
            <tr>
                <th colspan="2">Data Biodata</th>
            </tr>
            <tr>
                <td>NAMA</td>
                <td>: {$mhs.NM_PENGGUNA}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>: {$mhs.NIM_MHS}</td>
            </tr>
            <tr>
                <td>JENIS KELAMIN</td>
                <td>: {if $mhs.KELAMIN_PENGGUNA==1}Laki-laki{else}Perempuan{/if}
                    <input type="hidden" id="jk_lama" value="{$mhs.KELAMIN_PENGGUNA}"/>
                    <input type="hidden" id="cari" value="{$mhs.NIM_MHS}"/>
                    <input type="hidden" id="kel_lama" value="{$mhs.ID_KKN_KELOMPOK}"/>
                    <input type="hidden" id="mhs_lama" value="{$mhs.ID_MHS}"/>
                </td>
            </tr>
            <tr>
                <td>PRODI</td>
                <td>: {$mhs.NM_JENJANG} {$mhs.NM_PROGRAM_STUDI}</td>
            </tr>
            <tr>
                <td>KELOMPOK ASLI</td>
                <td>: {$mhs.NAMA_KELOMPOK}&nbsp;&nbsp;(kel.{$mhs.NM_KELURAHAN})</td>
            </tr>
            <tr>
                <td>PINDAH KELOMPOK</td>
                <td>
                    <select id="kel_p" name="kel_p">
                        {foreach $data_kelompok as $k}
                            <option value="{$k.ID_KKN_KELOMPOK}" {if $k.ID_KKN_KELOMPOK==$mhs.ID_KKN_KELOMPOK}selected="true"{/if}>{$k.NAMA_KELOMPOK} (Kel.{$k.NM_KELURAHAN})</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <th colspan="6">KELOMPOK KKN MAHASISWA SEKARANG</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>NIM</th>
                <th>NAMA</th>
                <th>JENIS KELAMIN</th>
                <th>PRODI</th>
                <th>FAKULTAS</th>
            </tr>
            {foreach $kel_mhs as $m}
                <tr>
                    <td>{$m@index+1}</td>
                    <td>{$m.NIM_MHS}</td>
                    <td>{$m.NM_PENGGUNA}</td>
                    <td>{if $m.KELAMIN_PENGGUNA==1}Laki-laki{else}Perempuan{/if}</td>
                    <td>{$m.NM_JENJANG} {$m.NM_PROGRAM_STUDI}</td>
                    <td>{$m.NM_FAKULTAS}</td>
                </tr>
            {/foreach}
        </table>
        <div id="load_kel">
        </div>

        {literal}
            <script type="text/javascript">
                $('#kel_p').change(function(){
                    $.ajax({
                        url : 'GetKelompok.php',
                        type : 'get',
                        data : 'kel_baru='+$(this).val()+'&jk_lama='+$('#jk_lama').val()+'&mhs_lama='+$('#mhs_lama').val()+'&kel_lama='+$('#kel_lama').val()+'&cari='+$('#cari').val(),
                        success :function(data){
                            $('#load_kel').html(data);
                            $('html, body').animate({ scrollTop: $('#load_kel').offset().top }, 'slow');
                        }
                    });
                });
            </script>
        {/literal}
    {else}
        <span style="color: red"><i>Data Mahasiswa tidak ditemukan / Belum Punya Kelompok</i></span>
    {/if}
{/if}