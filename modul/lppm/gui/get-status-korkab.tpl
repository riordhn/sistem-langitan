{if $is_kabupaten>0}
<script>
	$(document).ready(function(){
			$("#btnDelKorkabKabupaten{$id_dosen}").mouseover(function(){
			  $(this).css('cursor', 'pointer');
			  $(this).css('text-decoration', 'underline');
			}).mouseleave(function(){
			  $(this).css('cursor', 'none');
			  $(this).css('text-decoration', 'none');
			});
			$("#btnDelKorkabKabupaten{$id_dosen}").mouseover(function(){
			  $(this).css('cursor', 'pointer');
			  $(this).css('text-decoration', 'underline');
			}).mouseleave(function(){
			  $(this).css('cursor', 'none');
			  $(this).css('text-decoration', 'none');
			});
			$("#btnDelKorkabKabupaten{$id_dosen}").click(function(){
				$('#frmDelKorkabKabupaten{$id_dosen}{$i}').submit();
			});

			$('#frmDelKorkabKabupaten{$id_dosen}{$i}').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {	
						$('#get-status-korkab{$id_dosen}').load('KorkabController.php?act=status&id_dosen='+'{$id_dosen}');
					}
				})
				return false;
			});
	});

</script>
<form id="frmDelKorkabKabupaten{$id_dosen}{$i}" action="KorkabController.php" method="POST">
<div style="font-size:11px;font-weight:bold;background:#ededed;padding:10px;margin-right:5px;box-shadow: 0 0 8px rgba(0, 0, 0, 0.15), 0 1px 0 rgba(255,255,255,0.8) inset;border-bottom-color:#ccc;border-bottom-style:solid;border-bottom-width:1px;">
	<input type="hidden" value="{$id_dosen}" name="id_dosen" id="id_dosen" />
	<input type="hidden" value="delete" name="act" id="act" />
	<input type="hidden" value="{$id_korkab}" name="id_korkab" id="id_korkab" />

	<div style="float:left;" title="Sebagai Koordinator Kabupaten {$kabupaten}">
	{$kabupaten}
	</div>
	<div id="btnDelKorkabKabupaten{$id_dosen}" style="float:right;" title="Hapus">
	x
	</div>
	<div style="clear:both;">
	</div>
</div>
{/if}