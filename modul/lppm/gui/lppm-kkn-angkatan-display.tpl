<script language="javascript">
    $(document).ready(function() {

    {for $i=0 to $jml_angkatan-1}
        $("#btn-add").button({
            text: true,
            icons: {
                primary: "ui-icon-plus"
            }
        });

        $("#btn-cancel").button({
            text: true,
            icons: {
                primary: "ui-icon-arrowreturnthick-1-s"
            }
        });

        $("#btn-save").button({
            text: true,
            icons: {
                primary: "ui-icon-check"
            }
        });

        $("#btn-update{$i}").button({
            text: true,
            icons: {
                primary: "ui-icon-check"
            }
        });

        $("#btn-edit{$i}").button({
            text: false,
            icons: {
                primary: "ui-icon-pencil"
            }
        });

        $("#btn-delete{$i}").button({
            text: false,
            icons: {
                primary: "ui-icon-close"
            }
        });

        $('#btn-edit{$i}').click(function() {
            $('#div_nama_angkatan{$i}').hide();
            $('#div_kode_angkatan{$i}').hide();
            $('#div_angkatan_aktif{$i}').hide();


            $('#edit_nama_angkatan{$i}').show();
            $('#edit_kode_angkatan{$i}').show();
            $('#edit_angkatan_aktif{$i}').show();

            //Button Event
            $('#btn-update{$i}').show();
            $('#btn-delete{$i}').hide();
            $('#btn-edit{$i}').hide();
            return false;
        })

        $('#btn-delete{$i}').click(function() {
            var id_kkn_angkatan = $('#id_kkn_angkatan{$i}').val();
            $('#angkatan{$i}').load('controller/lppm-kkn-angkatan-delete.php?id_kkn_angkatan=' + id_kkn_angkatan);
            $('#angkatan{$i}').remove();
        })

        $('#btn-update{$i}').click(function() {
            $('#frmUpdateAngkatan{$i}').submit();
        })

        $("#dialog:ui-dialog").dialog("destroy");


        $('#btn-add').click(function() {
            $('#frmAddNumbKelompok').submit();


        })

        $('#btn-cancel').click(function() {
            $('#add-skim-space').hide();

        })

        $('#btn-save').click(function() {
            $('#formAddSkim').submit();
        })


        $('#frmUpdateAngkatan{$i}').submit(function() {
            var id_kkn_angkatan = $('#id_kkn_angkatan{$i}').val();
            var kode_angkatan = $('#edit_kode_angkatan{$i}').val();
            var nama_angkatan = $('#edit_nama_angkatan{$i}').val();
            var angkatan_aktif = $('#edit_angkatan_aktif{$i}').val();
            var dataString = 'kode_angkatan=' + kode_angkatan + '&nama_angkatan=' + nama_angkatan + '&angkatan_aktif=' + angkatan_aktif + '&id_kkn_angkatan=' + id_kkn_angkatan;
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: dataString,
                success: function(data) {
                    $('#div_nama_angkatan{$i}').show();
                    $('#div_kode_angkatan{$i}').show();
                    $('#div_angkatan_aktif{$i}').show();

                    $('#edit_nama_angkatan{$i}').hide();
                    $('#edit_kode_angkatan{$i}').hide();
                    $('#edit_angkatan_aktif{$i}').hide();

                    //Button Event
                    $('#btn-update{$i}').hide();
                    $('#btn-delete{$i}').show();
                    $('#btn-edit{$i}').show();

                    var kode_angkatan = $('#edit_kode_angkatan{$i}').val();
                    var nama_angkatan = $('#edit_nama_angkatan{$i}').val();
                    var angkatan_aktif = $('#edit_angkatan_aktif{$i}').val();
                    if (angkatan_aktif==1) {
                        $('#div_angkatan_aktif{$i}').html('Aktif');
                    }
                    else {
                        $('#div_angkatan_aktif{$i}').html('Tidak Aktif');
                    }

                    $('#div_nama_angkatan{$i}').html('' + nama_angkatan + '');
                    $('#div_kode_angkatan{$i}').html('' + kode_angkatan + '');
                    
                }
            })
            return false;
        });

    {/for}

    });
</script>
<table class="ui-widget" style="width:100%;">
    <tr class="ui-widget-header">
        <td style="width:20px;">#</td><td>KODE</td><td>SEMESTER</td><td>NAMA ANGKATAN</td><td>STATUS AKTIF</td><td style="width:80px;">ACTION</td>
    </tr>
    {for $i=0 to $jml_angkatan-1}
        <form id="frmUpdateAngkatan{$i}" name="frmUpdateAngkatan{$i}" action="controller/lppm-kkn-angkatan-update.php" method="post">
            <tr id="angkatan{$i}">
                <td>{$i+1}</td>
                <td>
                    <div id="div_kode_angkatan{$i}">
                        {$kode_angkatan[$i]}
                    </div>
                    <input type="hidden" value="{$id_kkn_angkatan[$i]}" id="id_kkn_angkatan{$i}" />
                    <input style="width:100%; display:none;" type="text" value="{$kode_angkatan[$i]}" name="edit_kode_angkatan" id="edit_kode_angkatan{$i}" />
                </td>
                <td>
                    {$nm_semester[$i]} - {$akd_semester[$i]}
                </td>
                <td>
                    <div id="div_nama_angkatan{$i}">{$nama_angkatan[$i]}</div>
                    <input style="width:100%; display:none;" type="text" value="{$nama_angkatan[$i]}" name="edit_nama_angkatan" id="edit_nama_angkatan{$i}" />
                </td>
                <td>
                    <div id="div_angkatan_aktif{$i}">{if $status_aktif[$i]==1}Aktif{else}Tidak Aktif{/if}</div>
                    <select style="width:100%; display:none;" name="edit_angkatan_aktif" id="edit_angkatan_aktif{$i}" >
                        <option value="1"{if $status_aktif[$i]==1}selected="true"{/if}>Aktif</option>
                        <option value="0"{if $status_aktif[$i]==0}selected="true"{/if}>Tidak Aktif</option>
                    </select>
                </td>
                <td style="width:80px;">
                    <button id="btn-edit{$i}">edit</button>
                    <button id="btn-delete{$i}">delete</button>
                    <button id="btn-update{$i}" style="display:none;">update</button>
                </td>
            </tr>
        </form>
    {/for}
</table>