<?php
include 'config.php';
include 'class/ProposalPenelitian.class.php';

$ProposalPenelitian = new ProposalPenelitian($db);

// hitung total matrix data per fakultas
$fakultas_set = $ProposalPenelitian->GetListFakultas();
$tahun_set = $ProposalPenelitian->GetDistinctTahun();
$data_set = $ProposalPenelitian->GetReportProposalByFakultas();

foreach ($tahun_set as &$tahun)
{
	foreach ($fakultas_set as &$fakultas)
	{
		foreach ($data_set as $data)
		{
			if ($data['TAHUN'] == $tahun['TAHUN'] && $data['ID_FAKULTAS'] == $fakultas['ID_FAKULTAS'])
			{
				$tahun['JUMLAH'] += $data['JUMLAH'];
				$fakultas['JUMLAH'] += $data['JUMLAH'];
				break;
			}
		}
	}
}


$smarty->assign('fakultas_set', $fakultas_set);
$smarty->assign('tahun_set', $tahun_set);
$smarty->assign('data_set', $data_set);

$smarty->display("report-proposal/fakultas.tpl");
?>