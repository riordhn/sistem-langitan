<?php

    include 'config.php';
    include 'class/nilai.class.php';

    $n = new nilai($db, $user->ID_PENGGUNA);

    if (isset($_GET)) {

        if (get('mode') == 'export') {
            $data_rekap = $n->load_rekap_penilaian_kkn(get('angkatan'), get('kelompok'));
           ob_start();
    

?>
<table border='1' align='left' cellpadding='1' cellspacing='1' class='keuangan'>
                <tr class="ui-widget-header">
                    <th colspan="8" class="header-coloumn">Mahasiswa KKN Kelompok</th>
                </tr>
                <tr class="ui-widget-header">
                    <th>NO</th>
                    <th>NIM</th>
                    <th>NAMA</th>
                    <th>PROGRAM STUDI</th>
                    <th>FAKULTAS</th>
                    <th>JALUR</th>
                    <th>NILAI ANGKA KKN</th>
                    <th>NILAI HURUF KKN</th>
                </tr>
                
                <?php $i=0; foreach($data_rekap as $m): ?>
                    <tr>
                        <td><?php echo $i+1 ?></td>
                        <td><?php echo $m['NIM_MHS']; ?></td>
                        <td><?php echo $m['NM_PENGGUNA']; ?></td>
                        <td><?php echo $m['NM_JENJANG'] . $m['NM_PROGRAM_STUDI']; ?></td>
                        <td><?php echo $m['NM_FAKULTAS']; ?></td>
                        <td><?php echo $m['NM_JALUR']; ?></td>
                        <td class="center"><?php echo $m['NILAI_ANGKA']; ?></td>
                        <td class="center"><?php echo $m['NILAI_HURUF']; ?></td>
                    </tr>
                <?php $i++; endforeach; ?>
            </table>

<?php
        }
    }
?>

<?php
$nm_file = "Daftar_Nilai_Mahasiswa_KKN_"."(" . date('d-m-Y') . ')';
header("Content-disposition: attachment; filename={$nm_file}.xls");
header("Content-type: application/vnd.ms-excel");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>