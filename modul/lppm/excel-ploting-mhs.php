<?php
include 'config.php';
include 'class/ploting.class.php';

$p = new ploting($db);

$semester = $p->GetSemesterAktif();
$angkatan = $p->GetAngkatanAktif();
$jenis_kkn = get('jenis') == 1 ? "REGULER" : "TEMATIK";
$data_excel = $p->LoadKKNKelompokPeserta(get('jenis'));
//echo "<pre>";
//var_dump($data_excel);
//echo "</pre>";
//die('');
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #B24405;
        text-transform:capitalize;
    }
    td{
        text-align: left;
    }
    .bold{
        font-weight: bold;
        font-size: 1.2em;
    }
</style>
<table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="5" class="header_text">
                <?php
                echo strtoupper("Daftar Mahasiswa Kuliah Kerja Nyata (KKN) {$jenis_kkn} Angkatan {$angkatan['NAMA_ANGKATAN']} <br/>");
                ?>
            </td>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($data_excel as $data) {
            ?>
            <tr>
                <td class="bold" style="text-align: left" colspan="5">
                    Koordinator Kabupaten/Kota :  <?= $data['KORKAB'] ?>
                    <br/>Koordinator Kecamatan : <?= $data['KORCAM'] ?>
                    <br/>Dosen Pembina Lapangan : <?= $data['DPL'] ?>
                    <br/>Kelompok : <?= $data['NAMA_KELOMPOK'] ?>
                    <br/>Kelurahan : <?= $data['NM_KELURAHAN'] ?> , Kecamatan : <?= $data['NM_KECAMATAN'] ?> , Kota <?= $data['NM_KOTA'] ?>
                </td>
            </tr>
            <tr>
                <td class="bold">NO</td>
                <td class="bold">NIM</td>
                <td class="bold">NAMA</td>
                <td class="bold">PRODI/FAKULTAS</td>
                <td class="bold">KELOMPOK</td>
            </tr>

            <?php
            $no = 1;
            foreach ($data['MHS_KKN'] as $m) {
                $fakultas = strtoupper($m['NM_FAKULTAS']);
                echo "<tr>
                        <td>{$no}</td>
                        <td>'{$m['NIM_MHS']}</td>
                        <td>{$m['NM_PENGGUNA']}</td>
                        <td>{$m['NM_JENJANG']} {$m['NM_PROGRAM_STUDI']} <br/>{$fakultas}</td>
                        <td>{$data['NAMA_KELOMPOK']}</td>
                    </tr>";
                $no++;
            }
            ?>
            <tr>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td colspan="5"></td>
            </tr>
        </tbody>
        <?php
    }
    ?>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "Daftar_Mahasiswa_KKN_".$jenis_kkn."(" . date('d-m-Y') . ')';
header("Content-disposition: attachment; filename={$nm_file}.xls");
header("Content-type: application/vnd.ms-excel");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
