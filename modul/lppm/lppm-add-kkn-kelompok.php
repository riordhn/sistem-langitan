<?php
	include 'config.php';
	
	if ($user->Role() != AUCC_ROLE_LPPM){
		header("location: /logout.php");
		exit();
	}

	if(isset($_POST['jml_kelompok'])){
		$jml_kelompok = $_POST['jml_kelompok'];
		unset($_SESSION['jml_kelompok']);
		$_SESSION['jml_kelompok'] = $jml_kelompok;
	}
	
	$id_kelurahan = array();
	$nm_kelurahan = array();
	
	$db->Query("select id_kelurahan, nm_kelurahan from kelurahan");
	
	$i = 0;
	$jml_desa = 0;
	while ($row = $db->FetchRow()){ 
		$id_kelurahan[$i] = $row[0];
		$nm_kelurahan[$i] = $row[1];
		$jml_kelurahan++;
		$i++;
	}
	
	$db->Query("select id_semester from semester where status_aktif_semester = 'True'");

	while ($row = $db->FetchRow()){ 
		$id_semester = $row[0];
	}
	
	$smarty->assign('id_semester', $id_semester);
	$smarty->assign('id_kelurahan', $id_kelurahan);
	$smarty->assign('nm_kelurahan', $nm_kelurahan);
	$smarty->assign('jml_kelurahan', $jml_kelurahan);
	$smarty->assign('jml_kelompok', $_SESSION['jml_kelompok']);
	$smarty->display('lppm-add-kkn-kelompok.tpl');
?>