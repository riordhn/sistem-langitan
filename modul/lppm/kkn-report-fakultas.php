<?php
	include '../../config.php';

	$id_pt = $id_pt_user;
	
		if(isset($_GET['fakultas'])){
			$fakultas = (int)$_GET['fakultas'];
			$sebaran = $db->QueryToArray("SELECT
											doskorkab.nm_pengguna AS korkab,
											doskorbing.nm_pengguna AS korbing,
											dosdpl.nm_pengguna AS dpl,
											b.nim_mhs,
											P .nm_pengguna,
											E .nama_kelompok,
											f.nm_kelurahan,
											c.nm_program_studi,
											D .nm_fakultas
										FROM
											kkn_kelompok_mhs A
										LEFT JOIN mahasiswa b ON b.id_mhs = A .id_mhs
										LEFT JOIN pengguna P ON P .id_pengguna = b.id_pengguna
										LEFT JOIN program_studi c ON c.id_program_studi = b.id_program_studi
										LEFT JOIN fakultas D ON D .id_fakultas = c.id_fakultas
										LEFT JOIN kkn_kelompok E ON E .id_kkn_kelompok = A .id_kkn_kelompok
										LEFT JOIN kelurahan f ON f.id_kelurahan = E .id_kelurahan
										LEFT JOIN semester G ON G .id_semester = E .id_semester
										LEFT JOIN kkn_dpl H ON H .id_kkn_kelompok = A .id_kkn_kelompok
										LEFT JOIN dosen dpl ON dpl.id_dosen = H .id_dosen
										LEFT JOIN pengguna dosdpl ON dosdpl.id_pengguna = dpl.id_pengguna
										LEFT JOIN kecamatan i ON i.id_kecamatan = f.id_kecamatan
										LEFT JOIN kota j ON j.id_kota = i.id_kota
										LEFT JOIN kkn_korkab K ON K .id_kabupaten = j.id_kota
										LEFT JOIN dosen korkab ON korkab.id_dosen = K .id_dosen
										LEFT JOIN pengguna doskorkab ON doskorkab.id_pengguna = korkab.id_pengguna
										LEFT JOIN kkn_korbing l ON l.id_kecamatan = f.id_kecamatan
										LEFT JOIN dosen korbing ON korbing.id_dosen = l.id_dosen
										LEFT JOIN pengguna doskorbing ON doskorbing.id_pengguna = korbing.id_pengguna
										WHERE
											D .id_fakultas = '$fakultas'
										AND G .status_aktif_semester = 'True'
										ORDER BY
											E .id_kkn_kelompok,
											c.id_program_studi");
		
			$smarty->assign('sebaran', $sebaran);
			$smarty->assign('getfak', $fakultas);
			$smarty->display('kkn/kkn-report-fakultas-ajax.tpl');
		}
		else{
			$fakultas = $db->QueryToArray("SELECT id_fakultas, nm_fakultas 
											FROM fakultas 
											WHERE id_perguruan_tinggi = '{$id_pt}' ORDER BY id_fakultas");
			$smarty->assign('fakultas', $fakultas);
			$smarty->display('kkn/kkn-report-fakultas.tpl');
		}
		
	
?>