<?php

include 'config.php';
include 'class/nilai.class.php';


$n = new nilai($db, $user->ID_PENGGUNA);

$angkatan = get('angkatan');

$data_kelompok = $n->load_kelompok_kkn($angkatan);

echo "<option value=''>Semua</option>";
foreach ($data_kelompok as $d) {
    $jenis = $d['JENIS_KKN'] == 1 ? 'Reguler' : 'Tematik';
    if ($d['NM_PENGGUNA'] != '') {
        echo "
        <option value='{$d['ID_KKN_KELOMPOK']}'>{$jenis} {$d['NAMA_KELOMPOK']} ( {$d['NM_PENGGUNA']} )</option>
        ";
    } else {
        echo "
        <option style='color:red' value='{$d['ID_KKN_KELOMPOK']}'>{$jenis} {$d['NAMA_KELOMPOK']} DPL Tidak Ada</option>
        ";
    }
}
?>
