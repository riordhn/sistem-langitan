<?php

include 'config.php';
include 'class/nilai.class.php';

$n = new nilai($db, $user->ID_PENGGUNA);
    
if (isset($_GET)) {
 
    if (get('mode') == 'tampil' || get('mode') == 'export') {
        if (isset($_POST)) {
            if (post('mode') == 'ubah-nilai') {
                for ($i = 1; $i < post('jumlah_nilai'); $i++) {
                    $id_pengambilan = post('pengambilan' . $i);
                    $nilai_angka = post('nilai' . $i);
                    $nilai_huruf = $n->get_nilai_huruf_akhir($nilai_angka);
                    if ($id_pengambilan != '' && $nilai_angka != '') {
                        $db->Query("UPDATE PENGAMBILAN_MK SET NILAI_HURUF='{$nilai_huruf}', NILAI_ANGKA='{$nilai_angka}',FLAGNILAI=1 WHERE ID_PENGAMBILAN_MK='{$id_pengambilan}'");
                    }
                }
            }
        }
        $smarty->assign('data_kelompok', $n->load_kelompok_kkn(get('angkatan')));
        $smarty->assign('data_rekap', $n->load_rekap_penilaian_kkn(get('angkatan'), get('kelompok')));
    }
}

$smarty->assign('data_angkatan', $n->load_angkatan_kkn());
$smarty->display("kontrol-nilai-kkn.tpl");
?>
