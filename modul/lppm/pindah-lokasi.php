<?php

include 'config.php';
include 'class/ploting.class.php';

$id_pt = $id_pt_user;

$p = new ploting($db);

if (isset($_GET)) {
    if (get('mode') == 'cari') {
        $cari= get('cari');
        if(isset($_POST)){
            if(post('mode')=='ubah'){
                $mhs_lama=post("mhs_lama");
                $mhs_baru=post("mhs_baru");
                $kel_lama=post("kel_lama");
                $kel_baru=post("kel_baru");
                $db->Query("UPDATE KKN_KELOMPOK_MHS SET ID_KKN_KELOMPOK='{$kel_baru}' WHERE ID_MHS='{$mhs_lama}'");
                $db->Query("UPDATE KKN_KELOMPOK_MHS SET ID_KKN_KELOMPOK='{$kel_lama}' WHERE ID_MHS='{$mhs_baru}'");
            }
        }
        $db->Query("
            SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI, J.NM_JENJANG,KK.*,KEL.NM_KELURAHAN,P.KELAMIN_PENGGUNA
            FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN KKN_KELOMPOK_MHS KKM ON KKM.ID_MHS=M.ID_MHS
            JOIN KKN_KELOMPOK KK ON KK.ID_KKN_KELOMPOK=KKM.ID_KKN_KELOMPOK
            JOIN KELURAHAN KEL ON KEL.ID_KELURAHAN=KK.ID_KELURAHAN
            WHERE M.NIM_MHS LIKE '%{$cari}%' AND P.ID_PERGURUAN_TINGGI = '{$id_pt}'
            ");
        $mhs = $db->FetchAssoc();
        $data_kelompok = $db->QueryToArray("
            SELECT KK.*, KEL.NM_KELURAHAN
            FROM KKN_KELOMPOK KK
            JOIN KELURAHAN KEL ON KEL.ID_KELURAHAN=KK.ID_KELURAHAN
            WHERE KK.ID_KKN_ANGKATAN='{$mhs['ID_KKN_ANGKATAN']}' AND KK.JENIS_KKN='{$mhs['JENIS_KKN']}' 
            ORDER BY KEL.ID_KECAMATAN,KEL.NM_KELURAHAN,NAMA_KELOMPOK");
            
        $smarty->assign('data_kelompok', $data_kelompok);
        $smarty->assign('kel_mhs',$p->LoadKKNKelompokMhs($mhs['ID_KKN_KELOMPOK']));
        $smarty->assign("mhs", $mhs);
    }
}

$smarty->display("pindah-lokasi.tpl");
?>
