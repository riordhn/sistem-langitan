<?php

include 'config.php';
include 'class/nilai.class.php';

$n = new nilai($db, $user->ID_PENGGUNA);

if (isset($_GET)) {

    if (get('mode') == 'tampil') {
        $id_angkatan = get('angkatan');
        if (isset($_POST)) {
            if (post('mode') == 'ubah-kelompok') {
                $kelompok_lama = post('id_kelompok_lama');
                $kelompok_baru = post('id_kelompok_baru');
                $query_ubah = "UPDATE KKN_KELOMPOK_MHS SET ID_KKN_KELOMPOK='{$kelompok_baru}' WHERE ID_KKN_KELOMPOK='{$kelompok_lama}'";
                $db->Query($query_ubah);
            }
        }
        $db->Query("SELECT * FROM KKN_ANGKATAN WHERE ID_KKN_ANGKATAN='{$id_angkatan}'");
        $angkatan = $db->FetchAssoc();
        $smarty->assign('angkatan', $angkatan);
        $smarty->assign('data_kelompok', $n->load_kelompok_kkn($id_angkatan));
    }
}

$smarty->assign('data_angkatan', $n->load_angkatan_kkn());
$smarty->display("pindah-desa.tpl");
?>
