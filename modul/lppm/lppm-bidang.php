<?php
	include 'config.php';
	
	$id_skim = array();
	$kode_skim = array();
	$nama_skim = array();
	
	$db->Query("select id_penelitian_skim, kode_skim, nama_skim from penelitian_skim order by id_penelitian_skim");
	$i = 0;
	$jml_data_skim = 0;
	while ($row = $db->FetchRow()){ 
		$id_skim[$i] = $row[0];
		$kode_skim[$i] = $row[1];
		$nama_skim[$i] = $row[2];
		$jml_data_skim++;
		$i++;
	}

	$id_bidang = array();
	$bidang_nama = array();
	$skim_id_skim = array();
	$skim_kode = array();
	$skim_nama = array();
	
	$db->Query("select bidang.id_penelitian_bidang, bidang.nama_bidang, skim.kode_skim, skim.nama_skim, skim.id_penelitian_skim from penelitian_bidang bidang
				left join penelitian_skim skim on skim.id_penelitian_skim = bidang.id_penelitian_skim
				order by bidang.id_penelitian_bidang");

	
	$i = 0;
	$jml_data = 0;
	while ($row = $db->FetchRow()){
		$id_bidang[$i] = $row[0];
		$bidang_nama[$i] = $row[1];
		$skim_kode[$i] = $row[2];
		$skim_nama[$i] = $row[3];
		$skim_id_skim[$i] = $row[4];
		$jml_data++;
		$i++;
	}
	
	
	$smarty->assign('jml_data', $jml_data);
	
	$smarty->assign('id_bidang', $id_bidang);
	$smarty->assign('bidang_nama', $bidang_nama);
	$smarty->assign('skim_kode', $skim_kode);
	$smarty->assign('skim_nama', $skim_nama);
	$smarty->assign('skim_id_skim', $skim_id_skim);
	
	$smarty->assign('jml_data_skim', $jml_data_skim);
	$smarty->assign('id_skim', $id_skim);
	$smarty->assign('kode_skim', $kode_skim);
	$smarty->assign('nama_skim', $nama_skim);

	$smarty->display('lppm-bidang.tpl');
?>