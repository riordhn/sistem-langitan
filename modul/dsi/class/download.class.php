<?php

class download {

    public $db;
    public $id_pengguna;

    function __construct($db, $id_pengguna) {
        $this->db = $db;
        $this->id_pengguna = $id_pengguna;
    }

    function LoadDownloadRequest() {
        return $this->db->QueryToArray("
            SELECT D.*,P1.NM_PENGGUNA NM_REQ,P2.NM_PENGGUNA NM_APP
            FROM DOWNLOAD D
            JOIN PENGGUNA P1 ON P1.ID_PENGGUNA=D.ID_REQUESTBY
            LEFT JOIN PENGGUNA P2 ON P2.ID_PENGGUNA=D.ID_APPROVEBY
            ORDER BY D.TGL_REQUEST
            ");
    }
    
    function GetDownloadRequest($id_download){
        $this->db->Query("SELECT D.*,P1.NM_PENGGUNA NM_REQ,P2.NM_PENGGUNA NM_APP
            FROM DOWNLOAD D
            JOIN PENGGUNA P1 ON P1.ID_PENGGUNA=D.ID_REQUESTBY
            LEFT JOIN PENGGUNA P2 ON P2.ID_PENGGUNA=D.ID_APPROVEBY
            WHERE D.ID_DOWNLOAD='{$id_download}'");
        return $this->db->FetchAssoc();
    }

    function ApproveRequest($status_file, $id_download, $keterangan) {
        if ($status_file == 1) {
            $this->db->Query("
            UPDATE DOWNLOAD
                SET
                    ID_APPROVEBY='{$this->id_pengguna}',
                    TGL_READY=SYSDATE,
                    STATUS_DOWNLOAD=1,
                    STATUS_FILE=1,
                    KETERANGAN_APPROVE='{$keterangan}'
            WHERE ID_DOWNLOAD='{$id_download}'
            ");
        } else {
            $this->db->Query("
            UPDATE DOWNLOAD
                SET
                    STATUS_FILE=2,
                    KETERANGAN_APPROVE='{$keterangan}'
            WHERE ID_DOWNLOAD='{$id_download}'
            ");
        }
    }
    

}

?>
