<?php
include 'config.php';

if ($user->IsLogged())
{    

	// force password to ISO27001:2009
	if($_SESSION['standar_iso']=='no'){
		header("Location: ../../login.php?mode=ltp-null");
		exit();
	}

	// data menu dari $user
    $smarty->assign('modul_set', $user->MODULs);

    $smarty->display('index.tpl');
	
}
else
{
    echo "<script type=\"text/javascript\">alert('Anda harus login terlebih dahulu'); window.location = 'http://{$_SERVER['HTTP_HOST']}';</script>";
}
?>