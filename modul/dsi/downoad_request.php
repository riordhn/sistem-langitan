<?php

include 'config.php';
include 'class/download.class.php';

$download = new download($db, $user->ID_PENGGUNA);

if (isset($_POST)) {
    if(post('mode')=='apv'){
        $download->ApproveRequest(post('status_file'), post('id_download'), post('keterangan'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'apv') {
        $smarty->assign('download', $download->GetDownloadRequest(get('id_download')));
    }
}

$smarty->assign('data_request', $download->LoadDownloadRequest());
$smarty->display('download_request.tpl');
?>