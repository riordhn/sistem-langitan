<?php

include 'config.php';
include 'function-tampil-informasi.php';

if (substr($_SERVER['REMOTE_ADDR'], 0, 6) != '210.57') {
    $smarty->assign('status',  alert_error("Link Ini Hanya Bisa diakses dari jaringan di Universitas Airlangga",60));
}

$smarty->display('download-center.tpl');
?>

