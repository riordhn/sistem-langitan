<div class="center_title_bar">Data Request Download</div>
{if $smarty.get.mode==''}
    <table class="ui-widget-content">
        <tr>
            <th>NO</th>
            <th>TANGGAL REQUEST</th>
            <th>TANGGAL APPROVE</th>
            <th>NAMA REQUEST</th>
            <th>NAMA FILE</th>
            <th>JENIS FILE</th>
            <th>STATUS FILE</th>
            <th>STATUS APPROVE</th>
            <th>KETERANGAN APPROVE</th>
            <th>OPERASI</th>
        </tr>
        {foreach $data_request as $d}
            <tr>
                <td>{$d@index+1}</td>
                <td>{$d.TGL_REQUEST}</td>
                <td style="text-align: center">
                    {if $d.ID_APPROVEBY!=''}
                        {$d.TGL_READY}
                    {else}
                        -
                    {/if}
                </td>
                <td>{$d.NM_REQ}</td>
                <td>{$d.NAMA_FILE}</td>
                <td>
                    {$d.JENIS_FILE}
                </td>
                <td>
                    {if $d.STATUS_FILE==0}
                        Sedang Request
                    {else if $d.STATUS_FILE==1}
                        Sudah Di Download
                    {else}
                        <span style="color: red">
                            File/link Tidak ada
                        </span>
                    {/if}
                </td>
                <td style="text-align: center;">
                    {if $d.ID_APPROVEBY!=''}
                        <span style="color: green">Sudah </span><br/><span style="font-size: 0.8em;font-style: italic">{$d.NM_APP}</span>
                    {else}
                        <span style="color: red">Belum</span>
                    {/if}
                </td>
                <td style="text-align: center">
                    {if $d.KETERANGAN_APPROVE!=''}
                        {$d.KETERANGAN_APPROVE}
                    {else}
                        -
                    {/if}
                </td>
                <td style="width: 120px;text-align: center">
                    {if $d.ID_APPROVEBY==''&&$d.STATUS_FILE==0}
                        <a class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer" href="downoad_request.php?mode=apv&id_download={$d.ID_DOWNLOAD}">Approve</a>
                    {else}
                        -
                    {/if}
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="10" style="text-align: center;color: red">Data Kosong</td>
            </tr>
        {/foreach}
    </table>
{else if $smarty.get.mode=='apv'}
    <form action="downoad_request.php" method="post">
        <table class="ui-widget-content">
            <tr>
                <th>FIELD</th>
                <th>DATA</th>
            </tr>
            <tr>
                <td>NAMA REQUEST</td>
                <td>{$download.NM_REQ}</td>
            </tr>
            <tr>
                <td>NAMA FILE</td>
                <td>{$download.NAMA_FILE}</td>
            </tr>
            <tr>
                <td>DOWNLOAD LINK</td>
                <td>
                    <a class="disable-ajax ui-button ui-corner-all ui-state-hover" style="padding: 3px;cursor: pointer" target="_blank" href="{$download.DOWNLOAD_URL}">Link Download</a>
                </td>
            </tr>
            <tr>
                <td>STATUS FILE</td>
                <td>
                    <select name="status_file" onchange="if($(this).val()=='1') $('#status_apv').val('Sudah Approve'); else $('#status_apv').val('Belum/Tidak Approve');">
                        <option value="1">File Ada/Bisa Di Download</option>
                        <option value="2">Link Broken/File Tidak Ada</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>KETERANGAN APPROVE</td>
                <td>
                    <textarea class="required" name="keterangan" style="width: 96%;height: 100px;resize: none;margin: 8px"></textarea><br/>
                    <p style="font-style: italic;font-size: 0.9em">
                        <span style="color: green">Jika Status File File Ada/Bisa Di Download</span> : Isi Keterangan dengan letak file pada Halaman Data Sharing<br/>
                        <span style="color: red">Jika Status File File Link Broken/File Tidak Ada</span> : Isi Keterangan dengan bahwa file request tidak ada
                    </p>
                </td>
            </tr>
            <tr>
                <td>STATUS APPROVE</td>
                <td>
                    <input type="hidden" name="id_download" value="{$smarty.get.id_download}"/>
                    <input type="hidden" name="mode" value="apv"/>
                    <input id="status_apv" type="text" name="status_apv" readonly="true" value="Sudah Approve"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;margin: 5px;">
                    <input class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer" type="submit" value="Simpan"/>
                </td>
            </tr>
        </table>
    </form>
    <a class="ui-button ui-corner-all ui-state-hover" style="padding: 5px;cursor: pointer" href="downoad_request.php">Kembali</span>
    {/if}
{literal}
    <script>
        $('form').validate();
    </script>
{/literal}