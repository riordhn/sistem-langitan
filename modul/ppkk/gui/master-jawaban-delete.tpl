<div class="center_title_bar">Master Jawaban-Hapus</div>
<form action="master-jawaban.php?mode=view_jawaban&soal={$smarty.get.soal}&form={$smarty.get.form}" method="post">
    <table style="width: 90%">
        <caption> SOAL : {$soal.NOMER} {$soal.SOAL}</caption>
        <tr>
            <th colspan="2">Hapus Jawaban</th>
        </tr>
        <tr>
            <td>Urutan Jawaban</td>
            <td>
                {$jawaban.URUTAN}
            </td>
        </tr>
        <tr>
            <td>Tipe Jawaban</td>
            <td>
                {if $jawaban.TIPE_JAWABAN==1}
                    Pilihan 
                {else if $jawaban.TIPE_JAWABAN==2}
                    Piihan Banyak
                {else if $jawaban.TIPE_JAWABAN==3}
                    Isian
                {/if}
                </select>
            </td>
        </tr>
        <tr>
            <td>Tipe Data</td>
            <td>
                {if $jawaban.TIPE_DATA==''}
                    Kosongi
                {else if $jawaban.TIPE_DATA==1}
                    HURUF
                {else if $jawaban.TIPE_DATA==2}
                    ANGKA
                {/if}
                </select>
            </td>
        </tr>
        <tr>
            <td>Max Lenght</td>
            <td>
                {$jawaban.MAX_LENGTH}
            </td>
        </tr>
        <tr>
            <td>Group Jawaban</td>
            <td>
                {$jawaban.GROUP_JAWABAN}
            </td>
        </tr>
        <tr>
            <td>Skor Jawaban</td>
            <td>
                {$jawaban.SKOR_JAWABAN}
            </td>
        </tr>
        <tr>
            <td>Jawaban</td>
            <td>
                {$jawaban.JAWABAN}
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" name="id_jawaban" value="{$smarty.get.jawaban}" />
                <input type="hidden" name="mode" value="delete"/>
                Apakah anda yakin akan menghapus data ini ?<br/>
                <a class="button" href="master-jawaban.php?mode=view_jawaban&soal={$smarty.get.soal}&form={$smarty.get.form}">Batal</a>
                <input class="button" type="submit" value="Hapus"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('form').validate();
    </script>
{/literal}