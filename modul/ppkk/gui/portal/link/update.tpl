<div class="center_title_bar">PORTAL LINK</div>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<th >NO.</th>
		<th>LINK</th>
		<th>LABEL</th>
	</tr>
	{foreach $link as $data}
	<tr>
		<td>
			{$p@index + 1}
		</td>
		<td>
			<a href="portal-link.php?id={$data.ID_PORTAL_LINK}&action=edit">{$data.LINK}</a>
		</td>
		<td>{$data.LABEL}</td>
	</tr>
	{/foreach}
</table>