<div class="center_title_bar">PORTAL SAMBUTAN REKTOR</div>

<div class="ui-widget-content sui-widget-content">
	{foreach $news as $data}
	<div id="news_title">
		<span style="padding:5px;">
			<strong>
			<a href="portal-sambutan.php?id={$data.ID_NEWS}">{$data.JUDUL}</a>
			</strong>
		</span>
		<span style="padding:5px;">
			<strong>
			<a href="portal-sambutan.php?id={$data.ID_NEWS}&mode=edit">Edit</a>
			</strong>
		</span>
	<div>
	{if $id==''}
	<div id="news_content" style="padding:5px;">
		<p align="justify" style="font-size:1em;">
		{$data.ISI}
		<!--<a class="more" href="portal-sambutan.php?id={$data.ID_NEWS}" > ... Read more &rsaquo;&rsaquo; </a></p>-->
		</p>
	</div>
	{else}
		{if $mode!=''}
		<div  style="padding:5px;">
			<form action="portal-sambutan.php" method="post" >
				<input type="hidden" id="id_sambutan" name="id_sambutan" value="{$data.ID_NEWS}"/>
				<textarea id="sambutan_content" name="sambutan_content" class="editor" align="justify" style="font-size:1em;">
				{$data.ISI}
				</textarea>
				<input type="submit" value="Update" />
			</form>
		</div>
		{else}
		<div  style="padding:5px;">
			<p align="justify" style="font-size:1em;">
			{$data.ISI}
			</p>
		</div>
		{/if}
	{/if}	
	<br />
	{/foreach}
</div>

<script>
$(document).ready(function() {

	$('.editor').ckeditor(function(e){
			delete CKEDITOR.instances[$(e).attr('name')];
		},{
			toolbar:
				[
					['Bold','Italic', 'JustifyLeft','JustifyCenter','JustifyRight', 'JustifyBlock', 'Underline','Strike','-','NumberedList','BulletedList', 'Outdent','Indent','-','Link']
				],
			skin: 'office2003'
		}
	); 
	
});
</script>