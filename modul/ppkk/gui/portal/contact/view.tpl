<script>
	
$(document).ready(function() {

	$('.editor').ckeditor(function(e){
			delete CKEDITOR.instances[$(e).attr('name')];
		},{
			toolbar:
				[
					['Bold','Italic', 'JustifyLeft','JustifyCenter','JustifyRight', 'JustifyBlock', 'Underline','Strike','-','NumberedList','BulletedList', 'Outdent','Indent','-','Link']
				],
			skin: 'office2003'
		}
	); 
	
});
</script>
<div class="center_title_bar">PORTAL CONTACT</div>

<div class="ui-widget-content sui-widget-content">
	{foreach $contact as $data}
	<div id="contact_title">
		<span style="padding:5px;">
			<strong>
			Aksi : <a href="portal-contact.php?id={$data.ID_CYBERCAMPUS}">{$data.TITLE}</a>
			</strong>
		</span>
		<span style="padding:5px;">
			<strong>
			<a href="portal-contact.php?id={$data.ID_CYBERCAMPUS}&mode=edit">Edit</a>
			</strong>
		</span>
	<div>
	{if $id==''}
	<div id="contact_content" style="padding:5px;">
		<p align="justify" style="font-size:1em;">
		{$data.CONTACT}
		</p>
	</div>
	{else}
		{if $mode!=''}
		<div  style="padding:5px;">
			<form action="portal-contact-update.php" method="post">
			
			<textarea class="editor" id="contact_edit" name="contact_edit" align="justify" style="font-size:1em;">
			{$data.CONTACT}
			</textarea>
			<input type="submit" value="Update" />
			</form>
		</div>
		{else}
		<div  style="padding:5px;">
			<p align="justify" style="font-size:1em;">
			{$data.ISI}
			</p>
		</div>
		{/if}
	{/if}	
	<br />
	{/foreach}
</div>

