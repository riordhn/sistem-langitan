<div class="center_title_bar">Master Kategori Soal-Hapus</div>
<form action="master-kategori-soal.php?form={$smarty.get.form}" method="post">
    <table style="width: 70%">
        <tr>
            <th colspan="2">Hapus Kategori Soal</th>
        </tr>
        <tr>
            <td>Kode Kategori</td>
            <td>
                {$kategori_soal.KODE_KATEGORI}
            </td>
        </tr>
        <tr>
            <td>Nama Kategori Soal</td>
            <td>
                {$kategori_soal.NM_KATEGORI}
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                Apakah Anda yakin untuk menghapus item ini?<br/>
                <input type="hidden" name="id_kategori" value="{$kategori_soal.ID_KATEGORI_SOAL}"/>
                <input type="hidden" name="mode" value="delete"/>
                <a class="button" href="master-kategori-soal.php?form={$smarty.get.form}">Batal</a>
                <input class="button" type="submit" value="Hapus"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('form').validate();
    </script>
{/literal}