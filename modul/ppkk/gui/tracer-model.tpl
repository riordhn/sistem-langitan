<div class="center_title_bar">Tracer Model</div>

<table style="width: 40%">
    <tr>
        <th colspan="2" class="center">TRACER MODEL</th>
    </tr>
    <tr>
        <td>Setting Full Trace</td>
        <td class="center">
            <input type="checkbox" id="model" {if $model.FULL_TRACE==1}checked="true"{/if}value="{$model.ID_TRACER_MODEL}"/>
            <br/>
            {if $model.FULL_TRACE==1}
                <span style="font-size: 0.9em;font-style: italic">Hilangkan Centang Untuk Non Full Trace</span>
            {else}
                <span style="font-size: 0.9em;font-style: italic">Centang Untuk Full Trace</span>
            {/if}
        </td>
    </tr>
</table>
{literal}
    <script>
        $('#model').click(function(){
            $.ajax({
                    type:'post',
                    url:'tracer-model.php',
                    data:'mode=ubah&id='+$(this).val(),
                    success:function(data){
                            $('#content').html(data);
                    }                    
            })
            });
    </script>
{/literal}