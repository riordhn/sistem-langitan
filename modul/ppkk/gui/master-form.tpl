<div class="center_title_bar">Master Form</div>
{if $smarty.get.mode==''}
    <table style="width: 98%">
        <tr>
            <th>NO</th>
            <th>NAMA FORM</th>
            <th>TIPE FORM</th>
            <th>FAKULTAS</th>
            <th style="width: 240px">OPERASI</th>
        </tr>
        {foreach $data_form as $f}
            <tr>
                <td>{$f@index+1}</td>
                <td>{$f.NM_FORM}</td>
                <td>
                    {if $f.TIPE_FORM==1}
                        Alumni
                    {/if}
                </td>
                <td>
                    {if $f.NM_FAKULTAS==''}
                        Semua
                    {else}
                        {$f.NM_FAKULTAS}
                    {/if}
                </td>
                <td class="center">
                    <a class="button" href="master-form.php?mode=edit&f={$f.ID_FORM}">Edit</a>
                    <a class="button" href="master-form.php?mode=delete&f={$f.ID_FORM}">Delete</a>
                    <br/>
                    <a class="button" href="master-form.php?mode=template&f={$f.ID_FORM}">View Template</a>

                </td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="5" class="center"><a class="button" href="master-form.php?mode=add">Tambah</a></td>
        </tr>
    </table>
{else if $smarty.get.mode=='add'}
    <div class="center_title_bar">Master Form-Tambah</div>
    <form action="master-form.php" method="post">
        <table style="width: 70%">
            <tr>
                <th colspan="2">Tambah Form</th>
            </tr>
            <tr>
                <td>Nama Form</td>
                <td>
                    <input type="text" class="required" size="30" name="nama">
                </td>
            </tr>
            <tr>
                <td>Tipe Form</td>
                <td>
                    <select name="tipe">
                        <option value="1">Alumni</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Untuk Fakultas</td>
                <td>
                    <select name="fakultas">
                        <option value="">Semua</option>
                        {foreach $data_fakultas as $f}
                            <option value="{$f.ID_FAKULTAS}">{$f.NM_FAKULTAS}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <input type="hidden" name="mode" value="tambah"/>
                    <a class="button" href="master-form.php">Batal</a>
                    <input class="button" type="submit" value="Tambah"/>
                </td>
            </tr>
        </table>
    </form>
    {literal}
        <script type="text/javascript">
            $('form').validate();
        </script>
    {/literal}
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Master Form-Edit</div>
    <form action="master-form.php" method="post">
        <table style="width: 70%">
            <tr>
                <th colspan="2">Edit Form</th>
            </tr>
            <tr>
                <td>Nama Form</td>
                <td>
                    <input type="text" class="required" size="30" value="{$form.NM_FORM}" name="nama">
                </td>
            </tr>
            <tr>
                <td>Tipe Form</td>
                <td>
                    <select name="tipe">
                        <option value="1">Alumni</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Untuk Fakultas</td>
                <td>
                    <select name="fakultas">
                        <option value="" {if $form.ID_FAKULTAS==''}selected="true"{/if}>Semua</option>
                        {foreach $data_fakultas as $f}
                            <option value="{$f.ID_FAKULTAS}" {if $f.ID_FAKULTAS==$form.ID_FAKULTAS}selected="true"{/if}>{$f.NM_FAKULTAS}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <input type="hidden" name="id_form" value="{$form.ID_FORM}"/>
                    <input type="hidden" name="mode" value="edit"/>
                    <a class="button" href="master-form.php">Batal</a>
                    <input class="button" type="submit" value="Update"/>
                </td>
            </tr>
        </table>
    </form>
    {literal}
        <script type="text/javascript">
            $('form').validate();
        </script>
    {/literal}
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">Master Form-Hapus</div>
    <form action="master-form.php" method="post">
        <table style="width: 70%">
            <tr>
                <th colspan="2">Hapus Form</th>
            </tr>
            <tr>
                <td>Nama Form</td>
                <td>
                    {$form.NM_FORM}
                </td>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    Apakah Anda yakin untuk menghapus item ini?<br/>
                    <input type="hidden" name="id_form" value="{$form.ID_FORM}"/>
                    <input type="hidden" name="mode" value="delete"/>
                    <a class="button" href="master-form.php">Batal</a>
                    <input class="button" type="submit" value="Hapus"/>
                </td>
            </tr>
        </table>
    </form>
    {literal}
        <script type="text/javascript">
            $('form').validate();
        </script>
    {/literal}
{else if $smarty.get.mode=='template'}
    {literal}
        <style>
            .title{
                background-color: #ccccff;
                font-weight: bold;
                font-size: large;
                border-top: 2px solid black;
            }
            .ket_soal{
                font-weight: normal;
                font-size: smaller;
            }
            .ket_jawaban{
                font-size: 10px;
            }
            .info_jawaban{
                text-transform: uppercase;
                font-weight: bold;
            }
        </style>
    {/literal}
    <div class="center" style="border: 1px #ccccff solid;width: 98%;padding: 10px;margin-bottom: 10px">
        <a class="button" href="master-form.php">Kembali</a><br/>
        <a class="disable-ajax button" style="background-color: #ffffff;color: red;border: 1px solid darkgrey" target="_blank" href="template-form-pdf.php?f={$smarty.get.f}">Cetak Template</a>
    </div>
    {foreach $data_template as $t}
        <table class="ui-widget" style="width: 100%;font-family: Arial">
            <tr>
                <th class="center" style="font-size: 1.2em">{$t.KODE_KATEGORI}. {$t.NM_KATEGORI}</th>
            </tr>
            {foreach $t.DATA_SOAL as $s}
                <tr class="ui-widget-header">
                    <th style="padding: 8px">{$s.NOMER} {$s.SOAL}</th>
                </tr>
                <!--HARCODE DATA-->
                {if $s.TIPE_SOAL==1}
                    <!--UNTUK TIPE SOAL CATEGORICAL VARIABEL-->
                    {foreach $s.DATA_JAWABAN as $j}
                        <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                            <td>
                                <input type="radio" name="{$s.NOMER}" value="{$j.ID_JAWABAN}" />{$j.JAWABAN}
                                {if $j.TIPE_JAWABAN==2}
                                    <input type="text" name="{$s.NOMER}ket" size="{$j.MAX_LENGTH}" maxlength="{$j.MAX_LENGTH}" />
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                {else if $s.TIPE_SOAL==2}
                    <!--UNTUK TIPE SOAL MULTIPLE DICHOTOMOUS--> 
                    {foreach $s.DATA_JAWABAN as $j}
                        <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                            <td>
                                <input type="checkbox" name="{$s.NOMER}{$j.URUTAN}" value="{$j.ID_JAWABAN}"/>&nbsp;&nbsp;&nbsp;{$j.JAWABAN}
                                {if $j.TIPE_JAWABAN==2}
                                    <input type="text" name="{$s.NOMER}{$j.URUTAN}ket" size="{$j.MAX_LENGTH}" maxlength="{$j.MAX_LENGTH}" />
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                {else if $s.TIPE_SOAL==3}
                    <!--UNTUK TIPE SOAL TEXT VARIABEL--> 
                    {foreach $s.DATA_JAWABAN as $j}
                        <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                            <td>
                                <input type="text" size="10" name="{$s.NOMER}{$j.URUTAN}"  />
                                <input type="hidden" name="{$s.NOMER}{$j.URUTAN}jaw" value="{$j.ID_JAWABAN}" />
                                {$j.JAWABAN}
                                {if $j.TIPE_JAWABAN==2}
                                    <input type="text" name="{$s.NOMER}{$j.URUTAN}ket" size="{$j.MAX_LENGTH}" maxlength="{$j.MAX_LENGTH}" />
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                {else if $s.TIPE_SOAL==4}
                    <!--UNTUK TIPE SOAL LONG TEXT--> 
                    {foreach $s.DATA_JAWABAN as $j}
                        <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                            <td>
                                <input type="text" size="50"  name="{$s.NOMER}{$j.URUTAN}"  />
                                <input type="hidden" name="{$s.NOMER}{$j.URUTAN}jaw" value="{$j.ID_JAWABAN}" />
                                {$j.JAWABAN}
                                {if $j.TIPE_JAWABAN==2}
                                    <input type="text" name="{$s.NOMER}{$j.URUTAN}ket" size="{$j.MAX_LENGTH}" maxlength="{$j.MAX_LENGTH}" />
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                {else if $s.TIPE_SOAL==5}
                    <!--UNTUK TIPE SOAL METRIC VARIABEL--> 
                    {foreach $s.DATA_JAWABAN as $j}
                        <tr {if $j.URUTAN%2==0}class="row1"{/if}>
                            <td>
                                <input type="text" size="5" maxlength="5" name="{$s.NOMER}{$j.URUTAN}"  />
                                <input type="hidden" name="{$s.NOMER}{$j.URUTAN}jaw" value="{$j.ID_JAWABAN}" />
                                {$j.JAWABAN}
                                {if $j.TIPE_JAWABAN==2}
                                    <input type="text" name="{$s.NOMER}{$j.URUTAN}ket" size="{$j.MAX_LENGTH}" maxlength="{$j.MAX_LENGTH}" />
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                {else if $s.TIPE_SOAL==6}
                    <!--UNTUK TIPE SOAL ORDINAL VARIABEL--> 
                    <tr class="row1">
                        <td>{$s.KETERANGAN_SOAL}</td>
                    </tr>
                    <tr>
                        <td>
                            {$group_jawaban=0}
                            {foreach $s.DATA_JAWABAN as $j}
                                {if $group_jawaban!=$j.GROUP_JAWABAN}
                                    {if $group_jawaban!=0}
                                        <br/><br/>
                                    {/if}
                                    <span class="info_jawaban">{$j.JAWABAN}</span><hr style="margin: 4px 0px"/>
                                    {$group_jawaban=$j.GROUP_JAWABAN}
                                {/if}
                                {$j.SKOR_JAWABAN}<input type="radio" name="{$s.NOMER}g{$j.GROUP_JAWABAN}" value="{$j.ID_JAWABAN}" />
                            {/foreach}
                            <br/>
                        </td>
                    </tr>
                {/if}
            {/foreach}
        </table>
    {/foreach}
{/if}