<div class="center_title_bar">BROADCAST INFORMASI KE ALUMNI</div>
<!--
<form name="formCariAlumni" id="formCariAlumni" action="broadcast-mail.php" method="get">
<table>
	<tr>
		<td>
			Input NIM/NAMA : 
		</td>
		<td>
			<input type="hidden" name="id" id="id" value="1" />
			<input type="text" name="user" id="user" />
		</td>
		<td>
			
			<input type="submit" name="submit" id="submit" value="CARI"/>
		</td>
	</tr>
</table>
</form>
-->
<table>
<tr>
<td>
BROADCAST KE : 
</td>
<td>
<form name="formSrc" id="formSrc" action="broadcast-info.php" method="get">
<select name="src" id="src">
	<option selected>
		PILIH SALAH SATU
	</option>
	<option value="1">
		PER ALUMNI
	</option>
	<option value="2">
		ALUMNI PER ANGKATAN
	</option>
	<option value="3">
		ALUMNI PER PERIODE LULUS
	</option>
	<option value="4">
		MAHASISWA AKTIF
	</option>
	<option value="5">
		MAHASISWA PER FAKULTAS
	</option>
</select>
</form>
</td>
</tr>
</table>
<hr>

<form id="formInfo" id="formInfo" method="post" action="broadcast-info-send.php" class="ui-widget">
	<label style="float:left;padding-right:10px;"><strong>KEPADA  </strong></label>
	<div style="float:left;" id="div_tujuan">
	<input type="text" name="tujuan" id="tujuan" style="width:335px;float:left;"  disabled="disabled"/>
	</div>
	<br /><br />

	<label style="float:left;padding-right:5px;"><strong>PERIHAL  </strong></label>
	<select id="jenis" name="jenis">
		<option value="1">
			INFO UMUM
		</option>
		<option value="2">
			INFO LOWONGAN
		</option>
	</select>
	
	<br /><br />
	
	<label style="float:left;padding-right:5px;"><strong>JUDUL &nbsp;&nbsp;</strong></label>
	<input type="text" name="subject" id="subject" style="width:335px;float:left;"  disabled="disabled"/>
	
	<br /><br />
	
	<textarea name="pengumuman" id="pengumuman" style="width:530px; height:200px;" disabled="disabled">
	</textarea>
	<br /><br />
	<input type="button" style="float:left;" disabled="disabled" name="btnSubmit" id="btnSubmit" value="Buat Pengumuman" />
</form>

<script>
	$(document).ready(function() {
	
		$( '#btnSubmit' ).button({
			text: true,
			icons: {
				primary: "ui-icon-plus"
			}
		});
		
		$('#src').change(function(){
			var src = $('#src').val();
			$('#btnSubmit').removeAttr('disabled');
			$('#tujuan').removeAttr('disabled');
			$('#subject').removeAttr('disabled');
			$('#pengumuman').removeAttr('disabled');
			$('#div_tujuan').load('broadcast-info.php?src='+src);
			//$("#formSrc").submit();
		});
		
		$('#btnSubmit').click(function(){
			$("#formInfo").submit();
		});
		
		
		$("#formInfo").submit( function() {
			var post_data = $(this).serialize();
			var form_action = $(this).attr("action");
			var form_method = $(this).attr("method");
			$.ajax( {
				type :form_method,
				url :form_action,
				cache :false,
				data :post_data,
				success : function() {
					//$('#div_tujuan').load('broadcast-info.php?src='+post_data);
					alert("Informasi telah dikirim");
					window.location.href = ('#broadcast-broadcast_info!broadcast-info.php');
				},
				error : function() {
					alert("Gagal Mengirim Informasi");
					window.location.href = ('#broadcast-broadcast_info!broadcast-info.php');
				}
			});
			return false;
		});
		
		
		$("#formSrc").submit( function() {
			var post_data = $(this).serialize();
			var form_action = $(this).attr("action");
			var form_method = $(this).attr("method");
			$.ajax( {
				type :form_method,
				url :form_action,
				cache :false,
				data :post_data,
				success : function() {
					$('#div_tujuan').load('broadcast-info.php?src='+post_data);
				},
				error : function() {
					//alert("Data gagal dimasukkan.");
				}
			});
			return false;
		});
	});
</script>