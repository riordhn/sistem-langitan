<div class="center_title_bar">Master Kategori Soal</div>
<form id="form" action="master-kategori-soal.php" method="get">
    <table style="width: 50%">
        <tr>
            <td>Pilih Form</td>
            <td>
                <select name="form" onchange="$('#form').submit()">
                    <option value="">Pilih Form</option>
                    {foreach $data_form as $f}
                        <option value="{$f.ID_FORM}" {if $smarty.get.form==$f.ID_FORM}selected="true"{/if}>{$f.NM_FORM} {if $f.ID_FAKULTAS!=''} ({$f.NM_FAKULTAS}){/if}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>
{if $smarty.get.form!=''}
    <table style="width: 98%">
        <tr>
            <th>NO</th>
            <th>KODE KATEGORI</th>
            <th>KATEGORI SOAL</th>
            <th style="width: 120px">OPERASI</th>
        </tr>
        {foreach $data_kategori_soal as $kat}
            <tr>
                <td>{$kat@index+1}</td>
                <td>{$kat.KODE_KATEGORI}</td>
                <td>{$kat.NM_KATEGORI}</td>
                <td class="center">
                    <a class="button" href="master-kategori-soal.php?mode=edit&kategori={$kat.ID_KATEGORI_SOAL}&form={$smarty.get.form}">Edit</a>
                    <a class="button" href="master-kategori-soal.php?mode=delete&kategori={$kat.ID_KATEGORI_SOAL}&form={$smarty.get.form}">Delete</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="4" class="kosong">Data Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="4" class="center"><a class="button" href="master-kategori-soal.php?mode=tambah&form={$smarty.get.form}">Tambah</a></td>
        </tr>
    </table>
{/if}