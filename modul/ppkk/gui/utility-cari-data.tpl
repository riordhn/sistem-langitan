<div class="center_title_bar">UTILITY CARI DATA</div>
<form name="formCariData" id="formCariData" action="utility-cari-data.php" method="get">
    <table>
        <tr>
            <td>
                Input NIM/NAMA: 
            </td>
            <td>
                <input type="text" name="cari" value="{$smarty.get.cari}"  />
            </td>
            <td>
                <input type="submit" name="submit" id="submit" class="button" value="Cari"/>
            </td>
        </tr>
    </table>
</form>
{if $data_alumni!=''}
    <table style="width: 98%;font-size: 0.95em">
        <tr>
            <th>NO</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>PRODI/FAKULTAS</th>
            <th>ANGKATAN</th>
            <th>TANGGAL LULUS</th>
            <th>PERIODE LULUS</th>
            <th>MOBILE</th>
            <th>EMAIL UNAIR</th>
            <th>EMAIL LAIN</th>
        </tr>
        {foreach $data_alumni as $det}
            <tr>
                <td>{$det@index+1}</td>
                <td>{$det.NIM_MHS}</td>
                <td>{$det.NM_PENGGUNA}</td>
                <td>{$det.NM_JENJANG} {$det.NM_PROGRAM_STUDI}<br/><br/>{$det.NM_FAKULTAS|upper}</td>
                <td>{$det.THN_ANGKATAN_MHS}</td>
                <td>{$det.TGL_LULUS}</td>
                <td>{$det.NM_TARIF_WISUDA}</td>
                <td>{$det.MOBILE_MHS}</td>
                <td>{$det.EMAIL_PENGGUNA}</td>
                <td>{$det.EMAIL_ALTERNATE}</td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="10" class="kosong">Data Tidak Ditemukan</td>
            </tr>
        {/foreach}
    </table>
{/if}