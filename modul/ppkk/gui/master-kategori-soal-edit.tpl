<div class="center_title_bar">Master Kategori Soal-Edit</div>
<form action="master-kategori-soal.php?form={$smarty.get.form}" method="post">
    <table style="width: 70%">
        <tr>
            <th colspan="2">Edit Kategori Soal</th>
        </tr>
        <tr>
            <td>Kode Kategori</td>
            <td>
                <input type="text" class="required" size="3" maxlength="3" name="kode" value="{$kategori_soal.KODE_KATEGORI}">
            </td>
        </tr>
        <tr>
            <td>Nama Kategori Soal</td>
            <td>
                <textarea name="nama" cols="25" rows="4" class="required">{$kategori_soal.NM_KATEGORI}</textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" name="id_kategori" value="{$kategori_soal.ID_KATEGORI_SOAL}"/>
                <input type="hidden" name="mode" value="edit"/>
                <a class="button" href="master-kategori-soal.php?form={$smarty.get.form}">Batal</a>
                <input class="button" type="submit" value="Update"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('form').validate();
    </script>
{/literal}