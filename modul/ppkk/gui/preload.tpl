<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0044)http://yensdesign.com/tutorials/popupjquery/ -->
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>yensdesign.com - How to create a stuning and smooth popup in jQuery</title>
	<link rel="stylesheet" type="text/css" href="/css/maba.css" />
	<link rel="stylesheet" href="css/popup.css" type="text/css" media="screen">
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
	<script src="js/popup.js" type="text/javascript"></script>
	<script async="" src="js/cloudflare.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#popupContactClose').click(function() {
				
						//Getting the variable's value from a link 
				var loginBox = '#login-box';

				//Fade in the Popup
				$('#login-box').fadeIn(300);
				
				//Set the center alignment padding + border see css style
				var popMargTop = ($(loginBox).height() + 24) / 2; 
				var popMargLeft = ($(loginBox).width() + 24) / 2; 
				
				$(loginBox).css({ 
					'margin-top' : -popMargTop,
					'margin-left' : -popMargLeft
				});
				
				// Add the mask to body
				//$('body').append('<div id="mask"></div>');
				$('#mask').fadeIn(300);
				
				return false;
			});
			
			$('#buttonSubmit').click(function(){
				$('#loginForm').submit();
			});
			
			/*
			$('#loginForm').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(data) {	
					}
				})
				return false;
			});
			*/
			
			// When clicking on the button close or the mask layer the popup closed
			$('a.close, #mask').live('click', function() { 
			  $('#mask , .login-popup').fadeOut(300 , function() {
				$('#mask').remove();  
			  }); 
			  return false;
			});
		});
	</script>
</head>
<body onload="centerPopup();loadPopup();">
	<!--
	<center>
		<a href="http://www.yensdesign.com/"><img src="./yendesign_files/logo.jpg" alt="Go to yensdesign.com"></a>
		<div id="button"><input type="submit" value="Press me please!"></div>
	</center>
	-->
	<div id="popupContact">
		<a id="popupContactClose">x</a>
		<h1><center>Tracer Study - Universitas Airlangga</center></h1>
		<p id="contactArea">
		<img style="float:left; padding-right:10px;" src="http://4.bp.blogspot.com/_19j87EQervM/SfPGw3AfH6I/AAAAAAAADtk/cwFW4V6btDU/s400/Fasichul+Lisan.jpg" width="120" height="150"/>
		<p style="padding:15px;margin:15px;">
			Sebanyak 25 mahasiswa yang tergabung dalam enam kelompok Program Kreativitas Mahasiswa (PKM) yang akan berlaga di Pekan Ilmiah Mahasiswa Nasional (PIMNAS) ke-25 di Yogyakarta tanggal 9-12 Juli 2012, dilepas oleh Rektor Universitas Airlangga, Prof. Fasich, di Hall lantai I Rektorat Unair (6/7).

			Dalam sambutannya, Rektor Unair mengatakan bahwa ia bangga kepada kontingen yang berhasil lolos di ajang PIMNAS. "Saya bangga kalian bisa mempersiapkan PKM hingga lolos ke PIMNAS," katanya. Ia juga berpesan untuk tetap yakin dan berlaga dengan jujur.

			Walaupun secara pribadi ia merasa tidak puas dengan jumlah kontingen Unair di Pimnas yang sedikit, Ia tetap memberikan pesan dengan nada optimis. "Yang kecil bisa mengalahkan yang besar berkat ijin Allah," kata Prof. Fasich.

			Tahun ini Unair hanya mengirim kontingen di PIMNAS sebanyak 25 orang yang tergabung dalam enam tim. Ada enam bidang PKM yang dilombakan di ajang PIMNAS yakni PKM Karsa Cipta (PKMKC), Kewirausahaan (PKMK), Penelitian (PKMP), Gagasan Tertulis (PKMGT), Pengabdian Masyarakat (PKMM) dan Teknologi (PKMT). Tiap bidang diwakili oleh satu kelompok PKM dari Kontingen Unair.

			Dr. rer. Nat. Ganden Supriyanto. M,Sc., Koordinator Kontingen PIMNAS Unair yang juga anggota Tim Pendamping Kemahasiswaan Universitas Airlanga (TPKUA) menyatakan hal senada dengan apa yang diungkapkan Prof. Fasich. "Sebelumnya, sejak September lalu kami telah mengirimkan 734 judul proposal ke Jakarta dan hasilnya, sebanyak 293 proposal didanai oleh Dikti. Namun, setelah Tahap Monitoring dan Evaluasi (Monef) Dikti yang diumumkan, hanya enam finalis yang lolos," ucapnya.

			Meski demikian, Ganden melontarkan pernyataan bernada optimis. "Bila dari enam kelompok itu mendapat lima atau semuanya memperoleh emas, ada peluang keluar sebagai juara umum. Oleh karena itu, dari enam itu harus dioptimalkan," terang Ganden. Menurutnya, meski perguruan tinggi lain mengirimkan banyak kontingen, perolehan medali belum tentu mudah didapat. "Kita bersikap realistis tapi kita tidak boleh menyerah," tegasnya.

			Sebelumnya, selama enam hari kontingen PIMNAS Unair diberi pembekalan dan pembinaan. "Pembekalan total ada enam hari, selama dua periode. Namun di luar jadwal itu, kami tetap membuka klinik 24 jam sehari untuk membekali mereka," ungkap Ganden. Pembekalan yang diberikan meliputi teknik pembuatan poster ilmiah dan materi presentasi, cara presentasi serta trik menjawab pertanyaan juri PIMNAS.

			Selain dukungan dari TPKUA, Pihak BEM Keluarga Mahasiswa Unair (BEM KM) dan mahasiswa yang tahun lalu lolos di ajang PIMNAS XXIV di Makassar juga memberikan dukungan kepada kontingen PIMNAS Unair. "Saya mengapresiasi mereka karena mau membantu teman-taman PKM yang bakal berlaga. Teman-teman BEM nanti akan mengrimkan sekitar 90 orang mahasiswa Unair untuk menjadi tim supporter," tutur Ganden.

			Menurutnya, siapapun komponen di Unair harus bisa bersama-sama membangun Unair di bidang penalaran. "Saya yakin itu mudah, karena mereka pasti punya basis ilmu dan semangat yang sama," tegasnya.
		
		</p>
	</div>
	<div id="backgroundPopup"></div>
	
    <div id="login-box" class="wrap-box">
        <div class="login-box">
            <form action="index.php" method="post">
            <input type="hidden" name="mode" value="login">
            <table>
                <tr>
                    <td class="right-align"><strong>No Ujian</strong></td>
                    <td>
                        <input name="no_ujian" type="text" maxlength="12" style="width: 110px">
                    </td>
                </tr>
                <tr>
                    <td class="right-align"><strong>Kode Voucher</strong></td>
                    <td>
                        <input name="kode_voucher" type="text" maxlength="16" style="width: 110px" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button onclick="window.location = '/index.php'; return false;">Kembali</button>
                        <input type="submit" value="Login" />
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>

	<!--
	<div id="login-box" class="login-popup">
			<a href="#" class="close"><img src="close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
			  <form method="post" class="signin" action="login.php" id="loginForm" name="loginForm">
					<fieldset class="textbox">
					<label class="username">
					<span>Username or email</span>
					<input id="username" name="username" value="" type="text" autocomplete="on" placeholder="Username">
					</label>
					<label class="password">
					<span>Password</span>
					<input id="password" name="password" value="" type="password" placeholder="Password">
					</label>
					<button class="submit button" id="buttonSubmit" type="button">Sign in</button>
					<p>
					<a class="forgot" href="#">Forgot your password?</a>
					</p>        
					</fieldset>
			  </form>
	</div>
	-->
	
</body></html>