<div class="center_title_bar">Master Soal-Edit</div>
<form action="master-soal.php?form={$smarty.get.form}" method="post">
    <table style="width: 90%">
        <caption>KATEGORI SOAL : {$kategori.KODE_KATEGORI} {$kategori.NM_KATEGORI}</caption>
        <tr>
            <th colspan="2">Edit Soal</th>
        </tr>
        <tr>
            <td>Nomer Soal</td>
            <td>
                <input type="text" class="required" size="4" maxlength="4" name="nomer" value="{$soal.NOMER}">
            </td>
        </tr>
        <tr>
            <td>Tipe Soal</td>
            <td>
                <select name="tipe">
                    {foreach $jenis_soal as $js}
                        <option value="{$js.ID_JENIS_SOAL}" {if $soal.TIPE_SOAL==$js.ID_JENIS_SOAL}selected="true"{/if}>{$js.NAMA_JENIS}</option>
                    {/foreach}
                </select>
                <br/>
                <br/>
                <p style="font-size: 0.9em">
                    * KETERANGAN<br/>
                    {foreach $jenis_soal as $js}
                        {$js@index+1}. {$js.NAMA_JENIS} -> {$js.KETERANGAN}<br/>
                    {/foreach}
                </p>
            </td>
        </tr>
        <tr>
            <td>Soal</td>
            <td>
                <textarea name="soal" cols="25" rows="4" class="required">{$soal.SOAL}</textarea>
            </td>
        </tr>
        <tr>
            <td>Keterangan Soal</td>
            <td>
                <textarea name="keterangan" cols="25" rows="4" >{$soal.KETERANGAN_SOAL}</textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" name="id_soal" value="{$smarty.get.soal}" />
                <input type="hidden" name="id_kategori" value="{$smarty.get.kat}" />
                <input type="hidden" name="mode" value="edit"/>
                <a class="button" href="master-soal.php?form={$smarty.get.form}">Batal</a>
                <input class="button" type="submit" value="Update"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('form').validate();
    </script>
{/literal}