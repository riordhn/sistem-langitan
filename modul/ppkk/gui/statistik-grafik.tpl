<div class="center_title_bar">Grafik Pengisian</div>
<a class="button disable-ajax" style="cursor: pointer" onclick="history.back()">Kembali</a>
<p></p>
{if base64_decode($smarty.get.tipe)!=6}
    <div style="width: 100%;text-align: center">
        <b>Soal : {$soal.NOMER} {$soal.SOAL}</b>
        <div id="chartContainer" style="width: 100%;margin: auto;text-align: center">Loading</div>          
        <script type="text/javascript" src="../../fusionchart/FusionCharts.js"></script>
        <script type="text/javascript">
            var myChart = new FusionCharts( "../../swf/Charts/Column3D.swf", 
            "myChartId", "900", "700", "0", "1" );
            myChart.setXMLUrl("grafik-data.php?soal={base64_decode($smarty.get.soal)}&tipe={base64_decode($smarty.get.tipe)}");
            myChart.render("chartContainer");
        </script>
    </div>
{else}
    
    <div style="width: 100%;text-align: center">
        <b>Soal : {$soal.NOMER} {$soal.SOAL}</b>
        <div id="chartContainer" style="width: 100%;margin: auto;text-align: center">Loading</div>          
        <script type="text/javascript" src="../../fusionchart/FusionCharts.js"></script>
        <script type="text/javascript">
            var myChart = new FusionCharts( "../../swf/Charts/MSColumn3D.swf", 
            "myChartId", "950", "720", "0", "1" );
            myChart.setXMLUrl("grafik-data.php?soal={base64_decode($smarty.get.soal)}&tipe={base64_decode($smarty.get.tipe)}");
            myChart.render("chartContainer");
        </script>
    </div>
{/if}