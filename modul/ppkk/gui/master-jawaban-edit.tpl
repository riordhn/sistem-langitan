<div class="center_title_bar">Master Jawaban-Edit</div>
<form action="master-jawaban.php?mode=view_jawaban&soal={$smarty.get.soal}&form={$smarty.get.form}" method="post">
    <table style="width: 90%">
        <caption> SOAL : {$soal.NOMER} {$soal.SOAL}</caption>
        <tr>
            <th colspan="2">Edit Jawaban</th>
        </tr>
        <tr>
            <td>Urutan Jawaban</td>
            <td>
                <input type="text" class="required" size="3" maxlength="3" name="urutan" value="{$jawaban.URUTAN}">
            </td>
        </tr>
        <tr>
            <td>Tipe Jawaban</td>
            <td>
                <select name="tipe_jawaban">
                    <option value="1" {if $jawaban.TIPE_JAWABAN==1}selected="true"{/if}>Biasa</option>
                    <option value="2" {if $jawaban.TIPE_JAWABAN==2}selected="true"{/if}>Tambah Isian</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Tipe Data</td>
            <td>
                <select name="tipe_data">
                    <option value="" {if $jawaban.TIPE_DATA==''}selected="true"{/if}>Kosongi</option>
                    <option value="1" {if $jawaban.TIPE_DATA==1}selected="true"{/if}>Teks</option>
                    <option value="2" {if $jawaban.TIPE_DATA==2}selected="true"{/if}>Angka</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Max Lenght</td>
            <td>
                <input name="max" type="text" size="3" maxlength="3" value="{$jawaban.MAX_LENGTH}"/>
            </td>
        </tr>
        <tr>
            <td>Group Jawaban</td>
            <td>
                <input name="group" type="text" size="3" maxlength="3" value="{$jawaban.GROUP_JAWABAN}" />
            </td>
        </tr>
        <tr>
            <td>Skor Jawaban</td>
            <td>
                <input name="skor" type="text" size="3" maxlength="3" value="{$jawaban.SKOR_JAWABAN}" />
            </td>
        </tr>
        <tr>
            <td>Jawaban</td>
            <td>
                <textarea name="jawaban" cols="25" rows="4" class="required">{$jawaban.JAWABAN}</textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" name="id_jawaban" value="{$smarty.get.jawaban}" />
                <input type="hidden" name="mode" value="edit"/>
                <a class="button" href="master-jawaban.php?mode=view_jawaban&soal={$smarty.get.soal}&form={$smarty.get.form}">Batal</a>
                <input class="button" type="submit" value="Update"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('form').validate();
    </script>
{/literal}