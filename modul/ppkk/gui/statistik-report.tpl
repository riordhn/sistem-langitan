<div class="center_title_bar">Report Statistik</div>

<span style="font-size:18px;">Kuisioner {$nomor} &nbsp; {$soal}</span>
<hr>

<table class="ui-widget" >
	<tr class="center_title_bar">
		<td class="ui-widget-header">
			Pertanyaan
		</td>
		<td class="ui-widget-header">
			Jumlah
		</td>
	</tr>
	
	{for $i=0 to $data-1}
	<tr class="ui-widget-content">
		<td>
			{$pertanyaan[$i]}
		</td>
		<td>
			<a href="statistik-detail-report.php?id={$id_jawaban[$i]}&ket={base64_encode($pertanyaan[$i])|replace:'=':''}">
			{$jumlah[$i]}
			</a>
		</td>
	</tr>
	{/for}
</table>