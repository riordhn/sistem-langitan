<div class="center_title_bar">Master Soal-Hapus</div>
<form action="master-soal.php?form={$smarty.get.form}" method="post">
    <table style="width: 90%">
        <caption>KATEGORI SOAL : {$kategori.KODE_KATEGORI} {$kategori.NM_KATEGORI}</caption>
        <tr>
            <th colspan="2">Hapus Soal</th>
        </tr>
        <tr>
            <td>Nomer Soal</td>
            <td>
                {$soal.NOMER}
            </td>
        </tr>
        <tr>
            <td>Tipe Soal</td>
            <td>
                {foreach $jenis_soal as $js}
                    {if $soal.TIPE_SOAL==$js.ID_JENIS_SOAL}
                        {$js.NAMA_JENIS}
                    {/if}
                {/foreach}
            </td>
        </tr>
        <tr>
            <td>Soal</td>
            <td>
                {$soal.SOAL}
            </td>
        </tr>
        <tr>
            <td>Keterangan Soal</td>
            <td>
                {$soal.KETERANGAN_SOAL}
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" name="id_soal" value="{$smarty.get.soal}" />
                <input type="hidden" name="id_kategori" value="{$smarty.get.kat}" />
                <input type="hidden" name="mode" value="delete"/>
                Apakah Anda yakin menghapus data ini?<br/>
                <a class="button" href="master-soal.php?form={$smarty.get.form}">Batal</a>
                <input class="button" type="submit" value="Hapus"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('form').validate();
    </script>
{/literal}