<div class="center_title_bar">Master Jawaban-Tambah</div>
<form action="master-jawaban.php?mode=view_jawaban&soal={$smarty.get.soal}&form={$smarty.get.form}" method="post">
    <table style="width: 90%">
        <caption> SOAL : {$soal.NOMER} {$soal.SOAL}</caption>
        <tr>
            <th colspan="2">Tambah Jawaban</th>
        </tr>
        <tr>
            <td>Urutan Jawaban</td>
            <td>
                <input type="text" class="required" size="3" maxlength="3" name="urutan">
            </td>
        </tr>
        <tr>
            <td>Tipe Jawaban</td>
            <td>
                <select name="tipe_jawaban">
                    <option value="1">Biasa</option>
                    <option value="2">Tambah Isian</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Tipe Data</td>
            <td>
                <select name="tipe_data">
                    <option value="">Kosongi</option>
                    <option value="1">Teks</option>
                    <option value="2">Angka</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Max Lenght</td>
            <td>
                <input name="max" type="text" size="3" maxlength="3"/>
            </td>
        </tr>
        <tr>
            <td>Group Jawaban</td>
            <td>
                <input name="group" type="text" size="3" maxlength="3"/>
            </td>
        </tr>
        <tr>
            <td>Skor Skor</td>
            <td>
                <input name="skor" type="text" size="3" maxlength="3"/>
            </td>
        </tr>
        <tr>
            <td>Jawaban</td>
            <td>
                <textarea name="jawaban" cols="25" rows="4" class="required"></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" name="id_soal" value="{$smarty.get.soal}" />
                <input type="hidden" name="mode" value="tambah"/>
                <a class="button" href="master-jawaban.php?mode=view_jawaban&soal={$smarty.get.soal}&form={$smarty.get.form}">Batal</a>
                <input class="button" type="submit" value="Tambah"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('form').validate();
    </script>
{/literal}