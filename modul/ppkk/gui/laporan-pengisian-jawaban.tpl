<div class="center_title_bar">LAPORAN PENGISIAN JAWABAN</div>
<form id="form" action="laporan-pengisian-jawaban.php" method="get">
    <table style="width: 50%">
        <tr>
            <td>Pilih Form</td>
            <td>
                <select name="form" onchange="$('#form').submit()">
                    <option value="">Pilih Form</option>
                    {foreach $data_form as $f}
                        <option value="{$f.ID_FORM}" {if $smarty.get.form==$f.ID_FORM}selected="true"{/if}>{$f.NM_FORM} {if $f.ID_FAKULTAS!=''} ({$f.NM_FAKULTAS}){/if}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>
{if $smarty.get.form!=''}
    {if $smarty.get.mode=='detail'}
        <table style="width: 70%">
            <caption>{$soal.NOMER} {$soal.SOAL}</caption>
            {if $smarty.get.tipe==1||$smarty.get.tipe==2}
                <tr>
                    <th>NO</th>
                    <th>Jawaban</th>
                    <th>Keterangan</th>
                    <th>Jumlah</th>
                </tr>
                {foreach $data_detail as $det}
                    <tr>
                        <td>{$det@index+1}</td>
                        <td>{$det.JAWABAN}</td>
                        <td>{$det.KETERANGAN}</td>
                        <td>{$det.JUMLAH}</td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="4" class="kosong">Data Kosong</td>
                    </tr>
                {/foreach}
            {else if $smarty.get.tipe==3||$smarty.get.tipe==4||$smarty.get.tipe==5}
                <tr>
                    <th>NO</th>
                    <th>Jawaban</th>
                    <th>Isian</th>
                    <th>Jumlah</th>
                </tr>
                {foreach $data_detail as $det}
                    <tr>
                        <td>{$det@index+1}</td>
                        <td>{$det.JAWABAN}</td>
                        <td>{$det.ISIAN}</td>
                        <td>{$det.JUMLAH}</td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="4" class="kosong">Data Kosong</td>
                    </tr>
                {/foreach}
            {else if $smarty.get.tipe==6}
                <tr>
                    <th>NO</th>
                    <th>Jawaban</th>
                    <th>Skor Jawaban</th>
                    <th>Jumlah</th>
                </tr>
                {foreach $data_detail as $det}
                    <tr>
                        <td>{$det@index+1}</td>
                        <td>{$det.JAWABAN}</td>
                        <td>{$det.SKOR_JAWABAN}</td>
                        <td>{$det.JUMLAH}</td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="4" class="kosong">Data Kosong</td>
                    </tr>
                {/foreach}
            {/if}
        </table>        
        <a class="button" href="laporan-pengisian-jawaban.php?form={$smarty.get.form}">Kembali</a>
    {else}
        <a class="button disable-ajax" style="background-color: green" href="excel-pengisian-jawaban.php?{$smarty.server.QUERY_STRING}">Download Excel</a>
        <p></p>
        {foreach $data_soal as $data}
            <table style="width: 98%">
                <caption>Kategori Soal : {$data.KODE_KATEGORI} {$data.NM_KATEGORI}</caption>
                <tr>
                    <th>NO</th>
                    <th>NOMER SOAL</th>
                    <th>SOAL</th>
                    <th style="width: 120px">JUMLAH</th>
                </tr>
                {foreach $data.DATA_SOAL as $soal}
                    <tr>
                        <td>{$soal@index+1}</td>
                        <td>{$soal.NOMER}</td>
                        <td>{$soal.SOAL}</td>
                        <td class="center">
                            <a href="laporan-pengisian-jawaban.php?mode=detail&soal={$soal.ID_SOAL}&tipe={$soal.TIPE_SOAL}&form={$smarty.get.form}">{$soal.JUMLAH}</a>
                        </td>
                    </tr>
                {/foreach}
            </table>
        {/foreach}        
    {/if}    
{/if}