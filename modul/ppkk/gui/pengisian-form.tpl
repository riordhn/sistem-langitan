<div class="center_title_bar">Laporan Pengisian Form Tracer Studi</div>

{if $smarty.get.mode=='pengisi'}
    <a class="button" href="pengisian-form.php?form={$smarty.get.form}">Kembali</a>
    <a class="button disable-ajax" style="background-color: green" href="excel-pengisian-form.php?{$smarty.server.QUERY_STRING}">Download Excel</a>

    <p></p>
    <table style="width: 90%">
        <tr>
            <th>NO</th>
            <th>NAMA</th>
            <th>NIM MAHASISWA</th>
            <th>EMAIL</th>
            <th>CONTACT</th>
            <th>PROGRAM STUDI KELULUSAN</th>
            <th>FAKULTAS KELULUSAN</th>
            <th>ANGKATAN</th>
            <th>TAHUN LULUS</th>
        </tr>
        {foreach $data_pengisi as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.EMAIL_ALTERNATE}</td>
                <td>{$data.MOBILE_MHS}</td>
                <td>{$data.NM_JENJANG} {$data.NM_PROGRAM_STUDI}</td>
                <td>{$data.NM_FAKULTAS}</td>
                <td>{$data.THN_ANGKATAN_MHS}</td>
                <td>{$data.TAHUN_LULUS}</td>
            </tr>
        {/foreach}
    </table>
{else}
    <form id="form" action="pengisian-form.php" method="get">
        <table style="width: 90%">
            <tr>
                <td>Pilih Form</td>
                <td>
                    <select name="form" >
                        <option value="">Pilih Form</option>
                        {foreach $data_form as $f}
                            <option value="{$f.ID_FORM}" {if $smarty.get.form==$f.ID_FORM}selected="true"{/if}>{$f.NM_FORM} {if $f.ID_FAKULTAS!=''} ({$f.NM_FAKULTAS}){/if}</option>
                        {/foreach}
                    </select>
                </td>
                <td>Pilih Fakultas</td>
                <td>
                    <select name="fakultas">
                        <option value="">Semua</option>
                        {foreach $data_fakultas as $d}
                            <option value="{$d.ID_FAKULTAS}" {if $smarty.get.fakultas==$d.ID_FAKULTAS}selected="true"{/if}>{$d.NM_FAKULTAS}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <input type="submit" value="Tampilkan" class="button"/>
                </td>
            </tr>
        </table>
    </form>
    {if $smarty.get.form!=''}
        <a class="button disable-ajax" style="background-color: green" href="excel-pengisian-form.php?{$smarty.server.QUERY_STRING}">Download Excel</a>
        <a class="button disable-ajax" style="background-color: green" href="excel-pengisian-mahasiswa.php?fakultas={$smarty.get.fakultas}">Download Pengisian Per Mahasiswa</a>
        <p></p>
        {foreach $data_laporan_pengisian as $data}
            <table style="width: 90%">
                <caption>{if $smarty.get.fakultas!=''}PROGRAM STUDI: {$data.NM_JENJANG} {$data.NM_PROGRAM_STUDI}{else}FAKULTAS :{$data.NM_FAKULTAS}{/if}</caption>
                <tr>
                    <th>NO</th>
                    <th>KATEGORI SOAL</th>
                    <th>JUMLAH Pengisi</th>
                </tr>
                {foreach $data.HASIL as $h}
                    <tr>
                        <td>{$h@index+1}</td>
                        <td>{$h.KODE_KATEGORI} {$h.NM_KATEGORI}</td>
                        <td class="center">
                            {if $h.JUMLAH_PENGISI!=0}
                                <a href="pengisian-form.php?mode=pengisi&kat={$h.ID_KATEGORI_SOAL}&form={$smarty.get.form}&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}">{$h.JUMLAH_PENGISI}</a>
                            {else}
                                {$h.JUMLAH_PENGISI}
                            {/if}
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="3" class="kosong">Data Pengisi Kosong</td>
                    </tr>
                {/foreach}
            </table>
        {/foreach}
    {/if}
{/if}