<div class="center_title_bar">PORTAL NEWS</div>
<a class="button" href="portal-news.php?mode=add" >Tambah</a>
<br />
<br />
<div class="ui-widget-content sui-widget-content">
	{foreach $news as $data}
	<div id="news_title">
		<span style="padding:5px;">
			<strong>
			Judul : <a href="portal-news.php?id={$data.ID_NEWS}">{$data.JUDUL}</a>
			</strong>
		</span>
		&nbsp; | &nbsp;
		<strong>Aksi : </strong> 
		<span style="padding:5px;">
			<strong>
			<a href="portal-news.php?id={$data.ID_NEWS}&mode=edit">Edit</a>
			</strong>
		</span>
		<span style="padding:5px;">
			<strong>
			<a href="portal-news.php?id={$data.ID_NEWS}&mode=delete">Delete</a>
			</strong>
		</span>
	<div>
	{if $id==''}
	<div id="news_content" style="padding:5px;">
		<p align="justify" style="font-size:1em;">
		{$data.ISI}
		<a class="more" href="portal-news.php?id={$data.ID_NEWS}" > ... Read more &rsaquo;&rsaquo; </a></p> 
		</p>
	</div>
	{else}
		{if $mode!=''}
		<div  style="padding:5px;">
			<form action="portal-news.php" method="post">
				<input type="hidden" id="id_news" name="id_news" value="{$data.ID_NEWS}"/>
				<textarea id="news_content" name="news_content" class="editor" align="justify" style="font-size:1em;">
				
				{$data.ISI}
				</textarea>
				<input type="submit" value="Update" />
			</form>
		</div>
		{else}
		<div  style="padding:5px;">
			<p align="justify" style="font-size:1em;">
			{$data.ISI}
			</p>
		</div>
		{/if}
	{/if}	
	<br />
	{/foreach}
</div>

<script>
	
$(document).ready(function() {

	$('.editor').ckeditor(function(e){
			delete CKEDITOR.instances[$(e).attr('name')];
		},{
			toolbar:
				[
					['Bold','Italic', 'JustifyLeft','JustifyCenter','JustifyRight', 'JustifyBlock', 'Underline','Strike','-','NumberedList','BulletedList', 'Outdent','Indent','-','Link']
				],
			skin: 'office2003'
		}
	); 
	
});
</script>