<div class="center_title_bar">Master Jawaban Soal- Jawaban</div>
<a class="button" href="master-jawaban.php?form={$smarty.get.form}">Kembali</a>
<p></p>
<table style="width: 99%">
    <caption>{$soal.NOMER} {$soal.SOAL}</caption>
    <tr>
        <th>NO</th>
        <th>URUTAN JAWABAN</th>
        <th>JAWABAN</th>
        <th>TIPE JAWABAN</th>
        <th>TIPE DATA (Untuk Jawaban Isian)</th>
        <th>PANJANG JAWABAN (Untuk Jawaban Isian)</th>
        <th>SKOR JAWABAN</th>
        <th>GROUP JAWABAN</th>
        <th class="center" style="width: 150px">OPERASI</th>
    </tr>
    {foreach $data_jawaban as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data.URUTAN}</td>
            <td>{$data.JAWABAN}</td>
            <td>
                {if $data.TIPE_JAWABAN==1}
                    Biasa
                {else if $data.TIPE_JAWABAN==2}
                    Tambah Isian
                {/if}
            </td>
            <td>
                {if $data.TIPE_DATA==1}
                    Teks
                {else if $data.TIPE_DATA==2}
                    Angka
                {/if}
            </td>
            <td>{$data.MAX_LENGTH}</td>
            <td>{$data.SKOR_JAWABAN}</td>
            <td>{$data.GROUP_JAWABAN}</td>
            <td>
                <a class="button" href="master-jawaban.php?mode=edit&jawaban={$data.ID_JAWABAN}&soal={$data.ID_SOAL}&form={$smarty.get.form}">Edit</a>
                <a class="button" href="master-jawaban.php?mode=delete&jawaban={$data.ID_JAWABAN}&soal={$data.ID_SOAL}&form={$smarty.get.form}">Delete</a>
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="9" class="kosong">Data Jawaban Kosong</td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="9" class="center">
            <a class="button" href="master-jawaban.php?mode=tambah&soal={$smarty.get.soal}&form={$smarty.get.form}">Tambah</a>
        </td>
    </tr>
</table>