<div class="center_title_bar">Master Soal</div>
<form id="form" action="master-soal.php" method="get">
    <table style="width: 50%">
        <tr>
            <td>Pilih Form</td>
            <td>
                <select name="form" onchange="$('#form').submit()">
                    <option value="">Pilih Form</option>
                    {foreach $data_form as $f}
                        <option value="{$f.ID_FORM}" {if $smarty.get.form==$f.ID_FORM}selected="true"{/if}>{$f.NM_FORM} {if $f.ID_FAKULTAS!=''} ({$f.NM_FAKULTAS}){/if}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
    </table>
</form>
{if $smarty.get.form!=''}
    {foreach $data_soal as $data}
        <table style="width: 98%">
            <caption>Kategori Soal : {$data.KODE_KATEGORI} {$data.NM_KATEGORI}</caption>
            <tr>
                <th>NO</th>
                <th>NOMER SOAL</th>
                <th>SOAL</th>
                <th>JENIS SOAL</th>
                <th>KETERANGAN SOAL</th>
                <th style="width: 120px">OPERASI</th>
            </tr>
            {foreach $data.DATA_SOAL as $soal}
                <tr>
                    <td>{$soal@index+1}</td>
                    <td>{$soal.NOMER}</td>
                    <td>{$soal.SOAL}</td>
                    <td>{$soal.NAMA_JENIS}</td>
                    <td>{$soal.KETERANGAN_SOAL}</td>
                    <td class="center">
                        <a class="button" href="master-soal.php?mode=edit&soal={$soal.ID_SOAL}&kat={$soal.ID_KATEGORI_SOAL}&form={$smarty.get.form}">Edit</a>
                        <a class="button" href="master-soal.php?mode=delete&soal={$soal.ID_SOAL}&kat={$soal.ID_KATEGORI_SOAL}&form={$smarty.get.form}">Delete</a>
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="6" class="kosong">Data Soal Kosong</td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="6" class="center"><a class="button" href="master-soal.php?mode=tambah&kat={$data.ID_KATEGORI_SOAL}&form={$smarty.get.form}">Tambah</a></td>
            </tr>
        </table>
    {foreachelse}
        <span class="kosong">Data Kategori Kosong</span>
    {/foreach}
{/if}