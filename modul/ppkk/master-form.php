<?php

include 'config.php';

include 'class/master.class.php';

$master = new master($db);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $master->tambah_form(post('nama'), post('fakultas'));
    } else if (post('mode') == 'edit') {
        $master->edit_form(post('id_form'), post('nama'), post('fakultas'));
    } else if (post('mode') == 'delete') {
        $master->delete_form(post('id_form'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit') {
        $smarty->assign('form', $master->get_form(get('f')));
    } else if (get('mode') == 'delete') {
        $smarty->assign('form', $master->get_form(get('f')));
    } else if (get('mode') == 'template') {
        $smarty->assign('data_template', $master->load_template_form(get('f')));
    }
    $smarty->assign('data_fakultas', $db->QueryToArray("SELECT * FROM FAKULTAS ORDER BY ID_FAKULTAS"));
}

$smarty->assign("data_form", $master->load_form());
$smarty->display("master-form.tpl");
?>
