<?php
include 'config.php';
include 'class/laporan.class.php';
include '../keuangan/class/list_data.class.php';

$lap = new laporan($db);
$list = new list_data($db);

if (isset($_GET)) {
    if (get('form') != '') {
        if (get('mode') == 'pengisi') {
            $data_excel = $lap->load_data_pengisi(get('kat'), get('fakultas'), get('prodi'));
        } else {
            $data_excel = $lap->load_laporan_pengisian_form(get('fakultas'), get('form'));
        }
    }
}

if (get('fakultas') != '' || get('prodi') != '') {
    $fakultas = $list->get_fakultas(get('fakultas'));
    $prodi = $list->get_prodi(get('prodi'));
}
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1f4c63;
        text-transform:capitalize;
    }
    td{
        text-align: left;
    }
    caption{
        font-weight: bold;
        font-size: 1.2em;
    }
</style>
<?php
if ($_GET['mode'] == 'pengisi') {
    ?>
    <table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
        <thead>
            <tr>
                <td colspan="9" class="header_text">
                    <?php
                    echo strtoupper('LAPORAN PENGISIAN FORM TRACER STUDI<br/>');
                    if ($fakultas != '') {
                        echo strtoupper('Fakultas ' . $fakultas['NM_FAKULTAS'] . '<br/>');
                    }
                    if ($prodi != '') {
                        echo strtoupper('Program Studi (' . $prodi['NM_JENJANG'] . ') ' . $prodi['NM_PROGRAM_STUDI'] . '<br/>');
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td class="header_text">NO</td>
                <td class="header_text">NAMA</td>
                <td class="header_text">NIM MAHASISWA</td>
                <td class="header_text">EMAIL</td>
                <td class="header_text">CONTACT</td>
                <td class="header_text">PRODI LULUS</td>
                <td class="header_text">FAKUTAS LULUS</td>
                <td class="header_text">ANGKATAN</td>
                <td class="header_text">TAHUN LULUS</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach ($data_excel as $data) {
                $fakultas = strtoupper($data['NM_FAKULTAS']);
                echo "<tr>
                    <td>{$no}</td>
                    <td>{$data['NM_PENGGUNA']}</td>
                    <td>'{$data['NIM_MHS']}</td>
                    <td>{$data['EMAIL_ALTERNATE']}</td>
                    <td>{$data['MOBILE_MHS']}</td>
                    <td>{$data['NM_JENJANG']} {$data['NM_PROGRAM_STUDI']} <br/>{$fakultas}</td>
                    <td>{$data['NM_FAKULTAS']}</td>
                    <td>{$data['THN_ANGKATAN_MHS']}</td>
                    <td>{$data['TAHUN_LULUS']}</td>
                </tr>";
                $no++;
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    foreach ($data_excel as $data) {
        ?>
        <table  border='1' align='left' cellpadding='0' cellspacing='0'>
            <caption>
                <?php
                if ($_GET['fakultas'] != '')
                    echo "PROGRAM STUDI: {$data['NM_JENJANG']} {$data['NM_PROGRAM_STUDI']}";
                else
                    echo "FAKULTAS : {$data['NM_FAKULTAS']}";
                ?>
            </caption>
            <tr>
                <th class="header_text">NO</th>
                <th class="header_text">KATEGORI SOAL</th>
                <th class="header_text">JUMLAH Pengisi</th>
            </tr>
            <?php
            $no = 1;
            foreach ($data['HASIL'] as $h) {
                echo "<tr>
                        <td>{$no}</td>
                        <td>{$h['KODE_KATEGORI']} {$h['NM_KATEGORI']}</td>
                        <td class='center'>{$h['JUMLAH_PENGISI']}</td>
                    </tr>";
                $no++;
            }
            ?>
        </table>
        <?php
    }
}
?>



<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "Form-Pengisian-Mahasiswa-" . $fakultas['NM_FAKULTAS'] . "(" . date('d-m-Y') . ')';
header("Content-disposition: attachment; filename={$nm_file}.xls");
header("Content-type: application/vnd.ms-excel");
header("Content-Transfer-Encoding: binary");
ob_flush();

?>
