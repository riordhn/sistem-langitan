<?php
	include 'config.php';
	
	include 'class/news.class.php';
	
	$news = new news($db);

	if(isset($_GET['id'])&& isset($_GET['mode'])){
		$id = get('id');
		$mode = get('mode');
		if($mode=='edit'){
			$smarty->assign(array(
							   'id'=>$id, 
							   'mode'=>$mode,
							   'news'=>$news->GetNewsById($id)
						   )
			);
			
			$smarty->display('portal-news.tpl');
		}
		else if($mode=='delete'){
			$news->DeleteNewsById($id);
			$smarty->assign('news', $news->GetLatestNews());
			$smarty->display('portal-news.tpl');
		}	
		
	}
	
	else if(isset($_GET['id'])){
		$id = get('id');
		$smarty->assign(array(
						   'id'=>$id, 
						   'news'=>$news->GetNewsById($id)
					   )
		);
		$smarty->display('portal-news.tpl');
	}
	else if(isset($_GET['mode'])){
		$mode = get('mode');
		if($mode=='add'){
			$smarty->display('portal/news/add.tpl');
		}else if($mode=='view'){
			$smarty->assign('news', $news->GetLatestNews());
			$smarty->display('portal-news.tpl');
		}
	}
	
	else{
		$smarty->assign('news', $news->GetLatestNews());
		$smarty->display('portal-news.tpl');
	}
	
	if(isset($_POST['news_content']) && isset($_POST['id_news'])){
		$isi = post('news_content');
		$id = post('id_news');
		$news->UpdateNewsById($id, $isi);
		echo "<script>window.location = '#portal-portal-news!portal-news.php?id={$id}'; </script>";
	}else if(isset($_POST['news_add']) && isset($_POST['news_title'])){
		$judul = post('news_title');
		$isi = post('news_add');
		$news->InsertNews($judul, $isi);
		echo "<script>window.location = '#portal-portal-news!portal-news.php?mode=view'; </script>";
	}
	
	
?>