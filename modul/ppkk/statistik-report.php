<?php
	include '../../config.php';
	
	if(isset($_GET['nomor']) && isset($_GET['soal'])){
		
		$nomor = base64_decode($_GET['nomor']);
		$soal = base64_decode($_GET['soal']);
		
		$db->Query("
			select count(*), j.id_jawaban, j.jawaban from kuisioner_jawaban j
			left join kuisioner_soal s on s.id_soal = j.id_soal
			left join kuisioner_hasil_jawaban hj on hj.id_jawaban = j.id_jawaban
			left join kuisioner_hasil_soal hs on hs.id_hasil_soal = hj.id_hasil_soal
			left join kuisioner_hasil_form hf on hf.id_hasil_form = hs.id_hasil_form
			where s.nomer = '$nomor' and hf.id_pengguna is not null group by j.id_jawaban, j.jawaban
		");
		
		$i = 0;
		$data = 0;
		while ($row = $db->FetchRow()){
			
			$jumlah[$i] = $row[0];
			$id_jawaban[$i] = $row[1];
			$pertanyaan[$i] = $row[2];
			$data++;
			$i++;
			
		}
		
		$smarty->assign('id_jawaban', $id_jawaban);
		$smarty->assign('pertanyaan', $pertanyaan);
		$smarty->assign('jumlah', $jumlah);
		$smarty->assign('data', $data);
		$smarty->assign('nomor', ($nomor));
		$smarty->assign('soal', ($soal));
		$smarty->display('statistik-report.tpl');
	}
	else if(isset($_GET['id'])){
		
		
		$smarty->display('statistik-detail-report.tpl');
	}
?>