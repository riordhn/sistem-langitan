<?php

class laporan {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    // Fungsi Include
    function load_kategori_soal($form) {
        $f_form = $form != '' ? "WHERE ID_FORM='{$form}'" : "";
        return $this->db->QueryToArray("SELECT * FROM KUISIONER_KATEGORI_SOAL {$f_form}");
    }

    // Laporan Pengisian Form

    function load_row_pengisian_form($fakultas) {
        if ($fakultas != '') {
            $query = "
                SELECT PS.*,J.NM_JENJANG 
                FROM PROGRAM_STUDI PS
                JOIN JENJANG J ON PS.ID_JENJANG=J.ID_JENJANG
                WHERE PS.ID_FAKULTAS='{$fakultas}'
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI
                ";
        } else {
            $query = "SELECT * FROM FAKULTAS ORDER BY ID_FAKULTAS";
        }
        return $this->db->QueryToArray($query);
    }

    function load_laporan_pengisian_form($fakultas, $form) {
        $arr_result = array();
        foreach ($this->load_row_pengisian_form($fakultas) as $row) {
            if ($fakultas != '') {
                $filter = "AND PS.ID_PROGRAM_STUDI='{$row['ID_PROGRAM_STUDI']}'";
            } else {
                $filter = "AND PS.ID_FAKULTAS='{$row['ID_FAKULTAS']}'";
            }
            $hasil = $this->db->QueryToArray("
            SELECT KKS.ID_KATEGORI_SOAL,KKS.KODE_KATEGORI,KKS.NM_KATEGORI,COUNT(DISTINCT(KHF.ID_PENGGUNA)) JUMLAH_PENGISI
            FROM KUISIONER_KATEGORI_SOAL KKS
            LEFT JOIN KUISIONER_SOAL KS ON KS.ID_KATEGORI_SOAL = KKS.ID_KATEGORI_SOAL
            LEFT JOIN KUISIONER_HASIL_SOAL KHS ON KHS.ID_SOAL = KS.ID_SOAL
            LEFT JOIN KUISIONER_HASIL_FORM KHF ON KHS.ID_HASIL_FORM = KHF.ID_HASIL_FORM
            LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA=KHF.ID_PENGGUNA
            LEFT JOIN MAHASISWA M ON M.ID_PENGGUNA=P.ID_PENGGUNA
            LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            WHERE KS.ID_FORM='{$form}' {$filter}
            GROUP BY KKS.ID_KATEGORI_SOAL,KKS.KODE_KATEGORI,KKS.NM_KATEGORI");
            array_push($arr_result, array_merge($row, array('HASIL' => $hasil)));
        }
        return $arr_result;
    }

    function load_data_pengisi($kategori_soal, $fakultas, $prodi) {
        $q_fakultas = $fakultas != "" ? "AND PS.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != "" ? "AND PS.ID_PROGRAM_STUDI='{$prodi}'" : "";
        return $this->db->QueryToArray("
            SELECT P.ID_PENGGUNA,M.ID_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS,M.NIM_MHS,M.THN_ANGKATAN_MHS,AD.TAHUN_LULUS,P.EMAIL_ALTERNATE,M.MOBILE_MHS
            FROM KUISIONER_HASIL_FORM KHF 
            JOIN PENGGUNA P ON P.ID_PENGGUNA = KHF.ID_PENGGUNA
            JOIN MAHASISWA M ON M.ID_PENGGUNA = P.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            JOIN JENJANG J ON J.ID_JENJANG =PS.ID_JENJANG
            JOIN KUISIONER_HASIL_SOAL KHS ON KHS.ID_HASIL_FORM = KHF.ID_HASIL_FORM
            JOIN KUISIONER_SOAL KS ON KS.ID_SOAL=KHS.ID_SOAL
            LEFT JOIN (
                SELECT AD.ID_MHS,S.THN_AKADEMIK_SEMESTER TAHUN_LULUS
                FROM ADMISI AD
                JOIN SEMESTER S ON S.ID_SEMESTER=AD.ID_SEMESTER
                WHERE AD.STATUS_AKD_MHS=4
            ) AD ON AD.ID_MHS = M.ID_MHS
            WHERE KS.ID_KATEGORI_SOAL='{$kategori_soal}' {$q_fakultas} {$q_prodi}
            GROUP BY P.ID_PENGGUNA,M.ID_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS,M.NIM_MHS,M.THN_ANGKATAN_MHS,AD.TAHUN_LULUS,P.EMAIL_ALTERNATE,M.MOBILE_MHS");
    }

    // Rekap Kelulusan

    function load_rekap_kelulusan($fakultas, $tahun) {
        if ($fakultas != '') {
            $query = "
                    SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,L.JUMLAH_LULUS 
                    FROM PROGRAM_STUDI PS
                    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                    JOIN
                    (
                        SELECT PS.ID_PROGRAM_STUDI,COUNT(M.ID_MHS) JUMLAH_LULUS 
                        FROM ADMISI AD
                        JOIN MAHASISWA M ON AD.ID_MHS=M.ID_MHS
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                        WHERE AD.STATUS_AKD_MHS=4 AND TO_CHAR(AD.TGL_LULUS,'YYYY')='{$tahun}'
                        GROUP BY PS.ID_PROGRAM_STUDI
                    ) L ON L.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                    WHERE PS.ID_FAKULTAS='{$fakultas}' 
                    ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI";
        } else {
            $query = "SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,L.JUMLAH_LULUS 
                    FROM FAKULTAS F
                    LEFT JOIN
                    (
                        SELECT PS.ID_FAKULTAS,COUNT(M.ID_MHS) JUMLAH_LULUS 
                        FROM ADMISI AD
                        JOIN MAHASISWA M ON AD.ID_MHS=M.ID_MHS
                        JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
                        WHERE AD.STATUS_AKD_MHS=4 AND TO_CHAR(AD.TGL_LULUS,'YYYY')='{$tahun}'
                        GROUP BY PS.ID_FAKULTAS
                    ) L ON L.ID_FAKULTAS=F.ID_FAKULTAS
                    ORDER BY F.ID_FAKULTAS";
        }
        return $this->db->QueryToArray($query);
    }

    function load_detail_kelulusan($fakultas, $prodi, $tahun) {
        $q_fakultas = $fakultas != '' ? "AND PS.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != '' ? "AND PS.ID_PROGRAM_STUDI='{$prodi}'" : "";
        return $this->db->QueryToArray("
            SELECT M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,P.NM_PENGGUNA,P.EMAIL_PENGGUNA,TW.NM_TARIF_WISUDA,
                P.EMAIL_ALTERNATE,M.MOBILE_MHS,AD.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS
            FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN ADMISI AD ON AD.ID_MHS=M.ID_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            LEFT JOIN PEMBAYARAN_WISUDA PW ON PW.ID_MHS=M.ID_MHS AND PW.ABSENSI_WISUDA=1
            LEFT JOIN PERIODE_WISUDA PEW ON PEW.ID_PERIODE_WISUDA=PW.ID_PERIODE_WISUDA
            LEFT JOIN TARIF_WISUDA TW ON TW.ID_TARIF_WISUDA=PEW.ID_TARIF_WISUDA
            WHERE AD.STATUS_AKD_MHS=4 AND TO_CHAR(AD.TGL_LULUS,'YYYY')='{$tahun}' {$q_fakultas} {$q_prodi}
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI,M.NIM_MHS");
    }

    // Laporan Pengisian Jawaban
    function load_laporan_pengisian_jawaban($form) {
        $data_hasil = array();
        $data_soal = $this->load_kategori_soal($form);
        foreach ($data_soal as $kat) {
            array_push($data_hasil, array_merge($kat, array('DATA_SOAL' => $this->db->QueryToArray("
			SELECT KS.*,KJS.NAMA_JENIS,
                            (SELECT COUNT(*)
                            FROM KUISIONER_HASIL_FORM KHF
                            LEFT JOIN KUISIONER_HASIL_SOAL KHS ON KHS.ID_HASIL_FORM=KHF.ID_HASIL_FORM
                            WHERE KHS.ID_SOAL=KS.ID_SOAL
                            ) JUMLAH
                            FROM KUISIONER_SOAL KS
                            JOIN KUISIONER_JENIS_SOAL KJS ON KJS.ID_JENIS_SOAL=KS.TIPE_SOAL
                            WHERE KS.ID_KATEGORI_SOAL='{$kat['ID_KATEGORI_SOAL']}'
                            ORDER BY KS.ID_SOAL
			")
                            )
                    )
            );
        }
        return $data_hasil;
    }

    function load_detail_pengisian_jawaban($soal, $tipe) {
        if ($tipe == 1 || $tipe == 2) {
            $query = "
            SELECT KJ.ID_JAWABAN,KJ.JAWABAN,KHJ.KETERANGAN,COUNT(KHJ.ID_HASIL_JAWABAN) JUMLAH
            FROM KUISIONER_HASIL_SOAL KHS 
            JOIN KUISIONER_SOAL KS ON KHS.ID_SOAL=KS.ID_SOAL
            JOIN KUISIONER_HASIL_JAWABAN KHJ ON KHJ.ID_HASIL_SOAL = KHS.ID_HASIL_SOAL
            JOIN KUISIONER_JAWABAN KJ ON KJ.ID_JAWABAN = KHJ.ID_JAWABAN
            WHERE KHS.ID_SOAL='{$soal}'
            GROUP BY KJ.ID_JAWABAN,KJ.JAWABAN,KHJ.KETERANGAN
            ORDER BY JAWABAN";
        } else if ($tipe == 3 || $tipe == 4 || $tipe == 5) {
            $query = "
            SELECT KHJ.ID_JAWABAN,KJ.JAWABAN,KHJ.ISIAN,COUNT(KHJ.ID_HASIL_JAWABAN) JUMLAH
            FROM KUISIONER_HASIL_SOAL KHS 
            JOIN KUISIONER_SOAL KS ON KHS.ID_SOAL=KS.ID_SOAL
            JOIN KUISIONER_HASIL_JAWABAN KHJ ON KHJ.ID_HASIL_SOAL = KHS.ID_HASIL_SOAL
            JOIN KUISIONER_JAWABAN KJ ON KJ.ID_JAWABAN = KHJ.ID_JAWABAN
            WHERE KHS.ID_SOAL='{$soal}' AND KHJ.ISIAN IS NOT NULL
            GROUP BY KHJ.ID_JAWABAN,KJ.JAWABAN,KHJ.ISIAN
            ORDER BY JAWABAN,ISIAN";
        } else if ($tipe == 6) {
            $query = "
            SELECT KHJ.ID_JAWABAN,KJ.JAWABAN,KJ.SKOR_JAWABAN,COUNT(KHJ.ID_HASIL_JAWABAN) JUMLAH
            FROM KUISIONER_HASIL_SOAL KHS 
            JOIN KUISIONER_SOAL KS ON KHS.ID_SOAL=KS.ID_SOAL
            JOIN KUISIONER_HASIL_JAWABAN KHJ ON KHJ.ID_HASIL_SOAL = KHS.ID_HASIL_SOAL
            JOIN KUISIONER_JAWABAN KJ ON KJ.ID_JAWABAN = KHJ.ID_JAWABAN
            WHERE KHS.ID_SOAL='{$soal}'
            GROUP BY KHJ.ID_JAWABAN,KJ.JAWABAN,KJ.SKOR_JAWABAN
            ORDER BY JAWABAN,SKOR_JAWABAN";
        }
        return $this->db->QueryToArray($query);
    }

    // Statistik Grafik

    function load_detail_statistik($soal, $tipe) {
        if ($tipe == 1 || $tipe == 2) {
            $query = "
            SELECT (KJ.JAWABAN||' ('||KHJ.KETERANGAN||')') JAWABAN,COUNT(KHJ.ID_HASIL_JAWABAN) JUMLAH
            FROM KUISIONER_HASIL_SOAL KHS 
            JOIN KUISIONER_SOAL KS ON KHS.ID_SOAL=KS.ID_SOAL
            JOIN KUISIONER_HASIL_JAWABAN KHJ ON KHJ.ID_HASIL_SOAL = KHS.ID_HASIL_SOAL
            JOIN KUISIONER_JAWABAN KJ ON KJ.ID_JAWABAN = KHJ.ID_JAWABAN
            WHERE KHS.ID_SOAL='{$soal}'
            GROUP BY KJ.JAWABAN,KHJ.KETERANGAN
            ORDER BY JAWABAN";
        } else if ($tipe == 3 || $tipe == 4 || $tipe == 5) {
            $query = "
            SELECT (KJ.JAWABAN||' '||NVL(KHJ.ISIAN,KHJ.KETERANGAN)) JAWABAN,COUNT(KHJ.ID_HASIL_JAWABAN) JUMLAH
            FROM KUISIONER_HASIL_SOAL KHS 
            JOIN KUISIONER_SOAL KS ON KHS.ID_SOAL=KS.ID_SOAL
            JOIN KUISIONER_HASIL_JAWABAN KHJ ON KHJ.ID_HASIL_SOAL = KHS.ID_HASIL_SOAL
            JOIN KUISIONER_JAWABAN KJ ON KJ.ID_JAWABAN = KHJ.ID_JAWABAN
            WHERE KHS.ID_SOAL='{$soal}' AND KHJ.ISIAN IS NOT NULL
            GROUP BY KJ.JAWABAN,KHJ.ISIAN,KHJ.KETERANGAN
            ORDER BY JAWABAN,ISIAN";
        }
        return $this->db->QueryToArray($query);
    }

    // Excel Pengisian Mahasiswa

    function load_row_mahasiswa($fakultas) {
        $q_fakultas = $fakultas != '' ? "WHERE F.ID_FAKULTAS='{$fakultas}'" : "";
        return $this->db->QueryToArray("
            SELECT M.ID_PENGGUNA,P.NM_PENGGUNA,M.NIM_MHS,M.THN_ANGKATAN_MHS,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,F.NM_FAKULTAS
            FROM AUCC.KUISIONER_HASIL_FORM KHF
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=KHF.ID_PENGGUNA
            JOIN AUCC.MAHASISWA M ON M.ID_PENGGUNA=P.ID_PENGGUNA
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN AUCC.FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            {$q_fakultas}
            ORDER BY PS.ID_FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI
            ");
    }

    function load_row_soal() {
        return $this->db->QueryToArray("
            SELECT KS.ID_SOAL,KS.TIPE_SOAL,KS.NOMER,KS.SOAL
            FROM AUCC.KUISIONER_SOAL KS
            ORDER BY KS.ID_KATEGORI_SOAL,KS.ID_SOAL
            ");
    }

    function load_hasil_jawaban_mahasiswa($fakultas) {
        $data_hasil = array();
        $data_mhs = $this->load_row_mahasiswa($fakultas);
        foreach ($data_mhs as $m) {
            $data_soal = array();
            foreach ($this->load_row_soal() as $s) {
                $jawaban = $this->db->QueryToArray("
                    SELECT KHF.ID_PENGGUNA,KS.ID_KATEGORI_SOAL,KS.ID_SOAL,
                    (
                      CASE 
                        WHEN KS.TIPE_SOAL=1 OR KS.TIPE_SOAL=2        
                        THEN (KHJ.KETERANGAN||' '||KJ.JAWABAN)
                        WHEN KS.TIPE_SOAL=3 OR KS.TIPE_SOAL=4  OR KS.TIPE_SOAL=5       
                        THEN (KJ.JAWABAN||' : '||KHJ.ISIAN)
                        WHEN KS.TIPE_SOAL=6
                        THEN (KJ.JAWABAN||' : Skor='||KJ.SKOR_JAWABAN)
                      END
                    ) JAWABAN
                    FROM AUCC.KUISIONER_HASIL_FORM KHF
                    JOIN AUCC.KUISIONER_HASIL_SOAL KHS ON KHF.ID_HASIL_FORM=KHS.ID_HASIL_FORM
                    JOIN AUCC.KUISIONER_SOAL KS ON KS.ID_SOAL=KHS.ID_SOAL
                    JOIN AUCC.KUISIONER_HASIL_JAWABAN KHJ ON KHJ.ID_HASIL_SOAL=KHS.ID_HASIL_SOAL
                    JOIN AUCC.KUISIONER_JAWABAN KJ ON KJ.ID_JAWABAN=KHJ.ID_JAWABAN
                    WHERE KHF.ID_PENGGUNA='{$m['ID_PENGGUNA']}' AND KHS.ID_SOAL='{$s['ID_SOAL']}'");
                array_push($data_soal, array('JAWABAN' => $jawaban));
            }
            array_push($data_hasil, array_merge($m, array('SOAL_JAWABAN' => $data_soal)));
        }
        return $data_hasil;
    }

}

?>
