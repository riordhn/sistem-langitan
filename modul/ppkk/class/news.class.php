<?php
/**
 * Description of Programmer
 *
 * @author User
 */
class news
{
    private $db;
    
    function __construct(MyOracle $db)
    {
        $this->db = $db;
    }
    
    function GetLatestNews()
    {
        return $this->db->QueryToArray("
		select * from news where id_portal = 2 and id_news <> 53 order by id_news desc");
    }
	
    function GetAllNews()
    {
        return $this->db->QueryToArray("
		select * from news where id_portal = 2 and id_news <> 53 order by id_news desc ");
    }

    function GetNewsById($id)
    {
        return $this->db->QueryToArray("
		select * from news where id_news = '$id'");
    }
	
	function UpdateNewsById($id, $isi){
		$this->db->Query("update news set isi = '$isi' where id_news = '$id'");
	}

	function InsertNews($judul, $isi){
		$this->db->Query("insert into news (id_portal, judul, isi, tgl) values ('2', '$judul', '$isi', sysdate)");
	}
	function DeleteNewsById($id){
		$this->db->Query("delete from news where id_news = '$id'");
	}
}