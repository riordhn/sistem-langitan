<?php

class master {

    private $db;

    function __construct($db) {
        $this->db = $db;
    }

    // menu form
    function load_form() {
        return $this->db->QueryToArray("
            SELECT KF.*,F.NM_FAKULTAS
            FROM KUISIONER_FORM KF
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS=KF.ID_FAKULTAS
            ");
    }

    function get_form($id) {
        $this->db->Query("
            SELECT KF.*,F.NM_FAKULTAS
            FROM KUISIONER_FORM KF
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS=KF.ID_FAKULTAS
            WHERE KF.ID_FORM='{$id}'
            ");
        return $this->db->FetchAssoc();
    }

    function tambah_form($nama, $fakultas) {
        $this->db->Query("INSERT INTO KUISIONER_FORM (NM_FORM,TIPE_FORM,ID_FAKULTAS) VALUES ('{$nama}','1','{$fakultas}')");
    }

    function edit_form($id, $nama, $fakultas) {
        $this->db->Query("
            UPDATE KUISIONER_FORM
                SET
                    NM_FORM='{$nama}',
                    ID_FAKULTAS='{$fakultas}'
            WHERE ID_FORM='{$id}'
            ");
    }

    function delete_form($id) {
        $this->db->Query("DELETE KUISIONER_FORM WHERE ID_FORM='{$id}'");
    }
    
    // Tampilkan template

    function load_template_form($form) {
        $data_hasil = array();
        $data_kategori = $this->db->QueryToArray("SELECT * FROM KUISIONER_KATEGORI_SOAL WHERE ID_FORM='{$form}' ORDER BY NM_KATEGORI");
        foreach ($data_kategori as $dk) {
            array_push($data_hasil, array_merge($dk, array('DATA_SOAL' => $this->load_template_soal($dk['ID_KATEGORI_SOAL']))));
        }
        return $data_hasil;
    }

    function load_template_soal($kategori_soal) {
        $data_hasil = array();
        $data_soal = $this->db->QueryToArray("SELECT * FROM KUISIONER_SOAL WHERE ID_KATEGORI_SOAL='{$kategori_soal}' ORDER BY ID_SOAL");
        foreach ($data_soal as $data) {
            array_push($data_hasil, array_merge($data, array(
                        'JUMLAH_JAWABAN' => count($this->load_data_jawaban($data['ID_SOAL'])),
                        'DATA_JAWABAN' => $this->load_data_jawaban($data['ID_SOAL'])
                            )
                    )
            );
        }
        return $data_hasil;
    }
    
    function load_data_jawaban($soal) {
        return $this->db->QueryToArray("SELECT * FROM KUISIONER_JAWABAN WHERE ID_SOAL='{$soal}' ORDER BY ID_JAWABAN");
    }

    //menu kategori soal

    function load_kategori_soal($form) {
        $f_form = $form != '' ? "WHERE ID_FORM='{$form}'" : "";
        return $this->db->QueryToArray("SELECT * FROM KUISIONER_KATEGORI_SOAL {$f_form}");
    }

    function get_kategori_soal($id) {
        $this->db->Query("SELECT * FROM KUISIONER_KATEGORI_SOAL WHERE ID_KATEGORI_SOAL='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_kategori_soal($kode, $nama, $form) {
        $this->db->Query("
            INSERT INTO KUISIONER_KATEGORI_SOAL
                (KODE_KATEGORI,NM_KATEGORI,ID_FORM)
            VALUES
                ('{$kode}','{$nama}','{$form}')");
    }

    function update_kategori_soal($id, $kode, $nama) {
        $this->db->Query("
            UPDATE KUISIONER_KATEGORI_SOAL
            SET
                KODE_KATEGORI='{$kode}',
                NM_KATEGORI='{$nama}'
            WHERE ID_KATEGORI_SOAL='{$id}'");
    }

    function delete_kategori_soal($id) {
        $this->db->Query("DELETE FROM KUISIONER_KATEGORI_SOAL WHERE ID_KATEGORI_SOAL='{$id}'");
    }

    //menu soal

    function load_jenis_soal() {
        return $this->db->QueryToArray("SELECT * FROM KUISIONER_JENIS_SOAL ORDER BY ID_JENIS_SOAL");
    }

    function load_soal($form) {
        $data_hasil = array();
        $data_soal = $this->load_kategori_soal($form);
        foreach ($data_soal as $kat) {
            array_push($data_hasil, array_merge($kat, array('DATA_SOAL' => $this->db->QueryToArray("
                            SELECT KS.*,KJS.NAMA_JENIS
                            FROM KUISIONER_SOAL KS
                            JOIN KUISIONER_JENIS_SOAL KJS ON KJS.ID_JENIS_SOAL=KS.TIPE_SOAL
                            WHERE KS.ID_KATEGORI_SOAL='{$kat['ID_KATEGORI_SOAL']}'
                            ORDER BY KS.ID_SOAL")
                            )
                    )
            );
        }
        return $data_hasil;
    }

    function get_soal($id) {
        $this->db->Query("
            SELECT KS.*,KKS.KODE_KATEGORI,KKS.NM_KATEGORI FROM KUISIONER_SOAL KS
            JOIN KUISIONER_KATEGORI_SOAL KKS ON KKS.ID_KATEGORI_SOAL=KS.ID_KATEGORI_SOAL
            WHERE KS.ID_SOAL='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_soal($nomer, $kategori, $tipe, $soal, $keterangan) {
        $this->db->Query("
            INSERT INTO KUISIONER_SOAL
                (ID_FORM,NOMER,ID_KATEGORI_SOAL,TIPE_SOAL,SOAL,KETERANGAN_SOAL)
            VALUES
                (1,'{$nomer}','{$kategori}','{$tipe}','{$soal}','{$keterangan}')
        ");
    }

    function update_soal($id, $nomer, $kategori, $tipe, $soal, $keterangan) {
        $this->db->Query("
            UPDATE KUISIONER_SOAL
            SET
                NOMER='{$nomer}',
                ID_KATEGORI_SOAL='{$kategori}',
                TIPE_SOAL='{$tipe}',
                SOAL='{$soal}',
                KETERANGAN_SOAL='{$keterangan}'
            WHERE ID_SOAL='{$id}'");
    }

    function delete_soal($id) {
        $this->db->Query("DELETE FROM KUISIONER_SOAL WHERE ID_SOAL='{$id}'");
    }

    //menu jawaban

    function load_jawaban($soal) {
        return $this->db->QueryToArray("SELECT * FROM KUISIONER_JAWABAN WHERE ID_SOAL='{$soal}' ORDER BY TO_NUMBER(URUTAN)");
    }

    function get_jawaban($id) {
        $this->db->Query("SELECT * FROM KUISIONER_JAWABAN WHERE ID_JAWABAN='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_jawaban($soal, $jawaban, $tipe_jawaban, $group, $skor, $urutan, $max, $tipe_data) {
        $this->db->Query("
            INSERT INTO KUISIONER_JAWABAN
                (ID_SOAL,JAWABAN,TIPE_JAWABAN,GROUP_JAWABAN,SKOR_JAWABAN,URUTAN,MAX_LENGTH,TIPE_DATA)
            VALUES
                ('{$soal}','{$jawaban}','{$tipe_jawaban}','{$group}','{$skor}','{$urutan}','{$max}','{$tipe_data}')");
    }

    function edit_jawaban($id, $jawaban, $tipe_jawaban, $group, $skor, $urutan, $max, $tipe_data) {
        $this->db->Query("
            UPDATE KUISIONER_JAWABAN
            SET
                JAWABAN='{$jawaban}',
                TIPE_JAWABAN='{$tipe_jawaban}',
                GROUP_JAWABAN='{$group}',
                SKOR_JAWABAN='{$skor}',
                URUTAN='{$urutan}',
                MAX_LENGTH='{$max}',
                TIPE_DATA='{$tipe_data}'
            WHERE ID_JAWABAN='{$id}'");
    }

    function delete_jawaban($id) {
        $this->db->Query("DELETE KUISIONER_JAWABAN WHERE ID_JAWABAN='{$id}'");
    }

}

?>
