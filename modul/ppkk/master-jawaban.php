<?php

include 'config.php';

include 'class/master.class.php';

$master = new master($db);

$mode = get('mode', 'view');

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $master->tambah_jawaban(post('id_soal'), post('jawaban'), post('tipe_jawaban'), post('group'), post('skor'), post('urutan'), post('max'), post('tipe_data'));
    } else if (post('mode') == 'edit') {
        $master->edit_jawaban(post('id_jawaban'), post('jawaban'), post('tipe_jawaban'), post('group'), post('skor'), post('urutan'), post('max'), post('tipe_data'));
    } else if (post('mode') == 'delete') {
        $master->delete_jawaban(post('id_jawaban'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'view_jawaban') {
        $smarty->assign('soal',$master->get_soal(get('soal')));
        $smarty->assign('data_jawaban', $master->load_jawaban(get('soal')));
    } else if (get('mode') == 'tambah') {
        $smarty->assign('soal', $master->get_soal(get('soal')));
    } elseif (get('mode') == 'edit') {
        $smarty->assign('jawaban', $master->get_jawaban(get('jawaban')));
        $smarty->assign('soal', $master->get_soal(get('soal')));
    } else if (get('mode') == 'delete') {
        $smarty->assign('jawaban', $master->get_jawaban(get('jawaban')));
        $smarty->assign('soal', $master->get_soal(get('soal')));
    }
}

if (get('form') != '') {    
    $smarty->assign('data_soal', $master->load_soal(get('form')));
}
$smarty->assign('data_form', $master->load_form());
$smarty->display("master-jawaban-{$mode}.tpl");
?>
