<?php

include 'config.php';
include 'class/laporan.class.php';
include 'class/master.class.php';

$lap = new laporan($db);
$master = new master($db);
$soal = $master->get_soal(get('soal'));

if (get('tipe') != 6) {
    $data_jawaban = $lap->load_detail_statistik(get('soal'), get('tipe'));
    echo "<chart caption='Grafik Jawaban Pengisian Soal' labelDisplay='ROTATE' slantLabels='1' showValues='0' xAxisName='Variabel' yAxisName='Jumlah' showValues='0' decimals='0' formatNumberScale='0' palette='4'>";
    foreach ($data_jawaban as $j) {
        echo "<set label='{$j['JAWABAN']}' value='{$j['JUMLAH']}' />";
    }
    echo "</chart>";
} else {
    $group_jawaban = $db->QueryToArray("SELECT JAWABAN FROM KUISIONER_JAWABAN WHERE ID_SOAL='" . get('soal') . "' GROUP BY JAWABAN ORDER BY JAWABAN");
    $skor_jawaban = $db->QueryToArray("SELECT SKOR_JAWABAN FROM KUISIONER_JAWABAN WHERE ID_SOAL='" . get('soal') . "' GROUP BY SKOR_JAWABAN ORDER BY SKOR_JAWABAN");
    echo "<chart caption='Grafik Jawaban Pengisian Soal' labelDisplay='ROTATE' slantLabels='1' showValues='0' yAxisName='Jumlah' palette='2' animation='1' formatNumberScale='0' numDivLines='4' legendPosition='BOTTOM'>";
    echo "<categories>";
    foreach ($group_jawaban as $gj) {
        echo "<category label='{$gj['JAWABAN']}' />";
    }
    echo "</categories>";
    foreach ($skor_jawaban as $sk) {
        echo "<dataset seriesName='Skor {$sk['SKOR_JAWABAN']}'>";
        foreach ($group_jawaban as $gj) {
            $jumlah = $db->QuerySingle("
                SELECT COUNT(*) JUMLAH
                FROM AUCC.KUISIONER_HASIL_JAWABAN KHJ
                JOIN AUCC.KUISIONER_JAWABAN KJ ON KJ.ID_JAWABAN = KHJ.ID_JAWABAN
                WHERE KJ.JAWABAN='{$gj['JAWABAN']}' AND KJ.SKOR_JAWABAN='{$sk['SKOR_JAWABAN']}'");
            echo "<set value='{$jumlah}' />";
        }
        echo '</dataset>';
    }
    echo '<styles>

            <definition>

                <style type="font" name="CaptionFont" color="666666" size="15" />

                <style type="font" name="SubCaptionFont" bold="0" />

            </definition>

            <application>

                <apply toObject="caption" styles="CaptionFont" />

                <apply toObject="SubCaption" styles="SubCaptionFont" />

            </application>

        </styles>';
    echo '</chart>';
}
?>
