<?php
	include 'config.php';
	include 'class/news.class.php';
	
	$news = new news($db);
	if(isset($_GET['id'])){
		$id = get('id');
		$smarty->assign(array(
						   'id'=>$id, 
						   'news'=>$news->GetNewsById($id)
					   )
		);
	}
	else if(isset($_GET['id'])&& isset($_GET['mode'])){
		$mode = get('mode');
		if($mode=='edit'){
			include_once "../../api/ckeditor/ckeditor.php";

			// Create a class instance.
			$CKEditor = new CKEditor();

			// Path to the CKEditor directory.
			$CKEditor->basePath = '../../api/ckeditor/';

			// Replace a textarea element with an id (or name) of "textarea_id".
			$CKEditor->replace("news_content");
			
			$smarty->assign(array(
							   'id'=>$id, 
							   'mode'=>$mode,
							   'news'=>$news->GetNewsById($id)
						   )
			);
		}
	}else{
		$smarty->assign('news', $news->GetLatestNews());
	}
	
	$smarty->display('broadcast-news.tpl');
?>