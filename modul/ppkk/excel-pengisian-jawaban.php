<?php
include 'config.php';
include 'class/laporan.class.php';
include '../keuangan/class/list_data.class.php';

$lap = new laporan($db);
$list = new list_data($db);

if (isset($_GET)) {
    if (get('form') != '') {
        if (get('mode') == 'detail') {
            $smarty->assign('soal', $master->get_soal(get('soal')));
            $data_excel = $lap->load_detail_pengisian_jawaban(get('soal'), get('tipe'));
        }
        $data_excel = $lap->load_laporan_pengisian_jawaban(get('form'));
    }
}

if (get('fakultas') != '' || get('prodi') != '') {
    $fakultas = $list->get_fakultas(get('fakultas'));
    $prodi = $list->get_prodi(get('prodi'));
}
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1f4c63;
        text-transform:capitalize;
    }
    .center{
        text-align: center;
    }
    td{
        text-align: left;
    }
    caption{
        font-weight: bold;
        font-size: 1.2em;
    }
</style>
<?php
if ($_GET['mode'] == 'detail') {
    if ($_Get['tipe'] == 1 || $_Get['tipe'] == 2) {
        
    }
} else {
    foreach ($data_excel as $data) {
        ?>
        <table  border='1' align='left' style="display: block" cellpadding='0' cellspacing='0'>
            <caption>
                <?php
                echo "KATEGORI SOAL: {$data['KODE_KATEOGRI']} {$data['NM_KATEGORI']}";
                ?>
            </caption>
            <?php
            $no = 1;
            foreach ($data['DATA_SOAL'] as $h) {
                echo "<tr>
                        <th class='header_text' colspan='3'>{$no}. {$h['NOMER']} {$h['SOAL']}<br/> JUMLAH PENGISI:{$h['JUMLAH']}</th>
                    </tr>";
                $data_jawaban = $lap->load_detail_pengisian_jawaban($h['ID_SOAL'], $h['TIPE_SOAL']);
                if ($h['TIPE_SOAL'] == 1 || $h['TIPE_SOAL'] == 2) {
                    echo '<tr>
                            <th class="header_text">JAWABAN</th>
                            <th class="header_text">KETERANGAN</th>
                            <th class="header_text">JUMLAH</th>
                        </tr>';
                } else if ($h['TIPE_SOAL'] == 3 || $h['TIPE_SOAL'] == 4 || $h['TIPE_SOAL'] == 5) {
                    echo '<tr>
                            <th class="header_text">JAWABAN</th>
                            <th class="header_text">ISIAN</th>
                            <th class="header_text">JUMLAH</th>
                        </tr>';
                } else if ($h['TIPE_SOAL'] == 6) {
                    echo '<tr>
                            <th class="header_text">JAWABAN</th>
                            <th class="header_text">SKOR JAWABAN</th>
                            <th class="header_text">JUMLAH</th>
                        </tr>';
                }
                ?>
                <?php
                foreach ($data_jawaban as $j) {
                    if ($h['TIPE_SOAL'] == 1 || $h['TIPE_SOAL'] == 2) {
                        echo "<tr>
                            <td>{$j['JAWABAN']}</td>
                            <td>{$j['KETERANGAN']}</td>
                            <td class='center'>{$j['JUMLAH']}</td>
                        </tr>";
                    } else if ($h['TIPE_SOAL'] == 3 || $h['TIPE_SOAL'] == 4 || $h['TIPE_SOAL'] == 5) {
                        echo "<tr>
                            <td>{$j['JAWABAN']}</td>
                            <td>{$j['ISIAN']}</td>
                            <td class='center'>{$j['JUMLAH']}</td>
                        </tr>";
                    } else if ($h['TIPE_SOAL'] == 6) {
                        echo "<tr>
                            <td>{$j['JAWABAN']}</td>
                            <td>{$j['SKOR_JAWABAN']}</td>
                            <td class='center'>{$j['JUMLAH']}</td>
                        </tr>";
                    }
                }
                $no++;
            }
            ?>
        </table>
        <br/>
        <br/>
        <br/>
        <?php
    }
}
?>



<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "Data-Pengisian-Jawaban-(" . date('d-m-Y') . ')';
header("Content-disposition: attachment; filename={$nm_file}.xls");
header("Content-type: application/vnd.ms-excel");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
