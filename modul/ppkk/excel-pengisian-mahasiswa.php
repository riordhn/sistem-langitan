<?php
include '../../config.php';
include 'class/laporan.class.php';

$lap = new laporan($db);
$fakultas = $_GET['fakultas'];

$data_excel = $lap->load_hasil_jawaban_mahasiswa($fakultas);
//echo "<pre>";
//var_dump($data_excel);
//echo "</pre>";
//die();
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1f4c63;
        text-transform:capitalize;
    }
    .center{
        text-align: center;
    }
    td{
        text-align: left;
    }
    caption{
        font-weight: bold;
        font-size: 1.2em;
    }
</style>
<table  border='1' align='left' style="display: block" cellpadding='0' cellspacing='0'>
    <caption>
        <?php
        echo "PENGISIAN TRACER STUDI MAHASISWA";
        ?>
    </caption>
    <?php
    echo "<tr>
            <th class='header_text'>NO</th>
            <th class='header_text'>NAMA</th>
            <th class='header_text'>NIM</th>
            <th class='header_text'>ANGKATAN</th>
            <th class='header_text'>PROGRAM STUDI</th>
            <th class='header_text'>FAKULTAS</th>
            ";
    foreach ($lap->load_row_soal() as $s) {
        echo "
            <th class='header_text'>{$s['NOMER']} {$s['SOAL']}</th>
        ";
    }
    echo "</tr>";
    $no = 1;
    foreach ($data_excel as $d) {
        echo "<tr>";
        echo "
                <td>{$no}</td>
                <td>{$d['NM_PENGGUNA']}</td>
                <td>{$d['NIM_MHS']}</td>
                <td>{$d['THN_ANGKATAN_MHS']}</td>
                <td>{$d['NM_JENJANG']} {$d['NM_PROGRAM_STUDI']}</td>
                <td>{$d['NM_FAKULTAS']}</td>
            ";
        foreach ($d['SOAL_JAWABAN'] as $sj) {
            echo "<td>";
            foreach ($sj['JAWABAN'] as $j) {
                echo $j['JAWABAN'] . "<br/>";
            }
            echo "</td>";
        }
        echo "</tr>";
        $no++;
    }
    ?>

</table>




<?php


echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "Data-Pengisian-Mahasiswa-" . $fakultas['NM_FAKULTAS'] . "(" . date('d-m-Y') . ')';
header("Content-disposition: attachment; filename={$nm_file}.xls");
header("Content-type: application/vnd.ms-excel");
header("Content-Transfer-Encoding: binary");
ob_flush();

?>
