<?php

include 'config.php';

include 'class/master.class.php';

$master = new master($db);

$mode = get('mode', 'view');

if (get('form') != '') {
    if (isset($_POST)) {
        if (post('mode') == 'tambah') {
            $master->tambah_soal(post('nomer'), post('id_kategori'), post('tipe'), post('soal'), post('keterangan'));
        } else if (post('mode') == 'edit') {
            $master->update_soal(post('id_soal'), post('nomer'), post('id_kategori'), post('tipe'), post('soal'), post('keterangan'));
        } else if (post('mode') == 'delete') {
            $master->delete_soal(post('id_soal'));
        }
    }

    if (get('mode') != '') {
        if (get('mode') == 'tambah') {
            $smarty->assign('kategori', $master->get_kategori_soal(get('kat')));
        } elseif (get('mode') == 'edit') {
            $smarty->assign('kategori', $master->get_kategori_soal(get('kat')));
            $smarty->assign('soal', $master->get_soal(get('soal')));
        } else if (get('mode') == 'delete') {
            $smarty->assign('kategori', $master->get_kategori_soal(get('kat')));
            $smarty->assign('soal', $master->get_soal(get('soal')));
        }
        $smarty->assign('jenis_soal', $master->load_jenis_soal());
    }

    $smarty->assign('data_soal', $master->load_soal(get('form')));
}
$smarty->assign('data_form', $master->load_form());
$smarty->display("master-soal-{$mode}.tpl");
?>
