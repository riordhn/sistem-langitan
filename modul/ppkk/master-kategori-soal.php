<?php

include 'config.php';

include 'class/master.class.php';

$master = new master($db);

$mode = get('mode', 'view');

if (get('form') != '') {
    if (isset($_POST)) {
        if (post('mode') == 'tambah') {
            $master->tambah_kategori_soal(post('kode'), post('nama'), get('form'));
        } else if (post('mode') == 'edit') {
            $master->update_kategori_soal(post('id_kategori'), post('kode'), post('nama'));
        } else if (post('mode') == 'delete') {
            $master->delete_kategori_soal(post('id_kategori'));
        }
    }

    if (get('mode') != '') {
        if (get('mode') == 'edit') {
            $smarty->assign('kategori_soal', $master->get_kategori_soal(get('kategori')));
        } else if (get('mode') == 'delete') {
            $smarty->assign('kategori_soal', $master->get_kategori_soal(get('kategori')));
        }
    }
    
    $smarty->assign("data_kategori_soal", $master->load_kategori_soal(get('form')));
}
$smarty->assign('data_form', $master->load_form());

$smarty->display("master-kategori-soal-{$mode}.tpl");
?>
