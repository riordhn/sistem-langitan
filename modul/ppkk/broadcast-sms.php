<?php
	include '../../config.php';
	
	if(isset($_GET['id'])){
		$id = $_GET['id'];
		
		if($id==1){
			if(isset($_GET['user'])){
				$user = $_GET['user'];
				
				$db->Query("
					select p.username, p.nm_pengguna, p.foto_pengguna, m.mobile_mhs from pengguna p
					left join mahasiswa m on m.id_pengguna = p.id_pengguna
					where p.username like '$user' or p.nm_pengguna like UPPER('%$user%')
					--and p.id_role = 3
				");
				
				while ($row = $db->FetchRow()){
					$username = $row[0];
					$nama = $row[1];
					$foto = $row[2];
					$mobile = $row[3];
					$foto = $foto . '/' . $username . '.JPG';
				}
				
				$smarty->assign('mobile', $mobile);
				$smarty->assign('username', $username);
				$smarty->assign('nama', $nama);
				$smarty->assign('foto', $foto);
				$smarty->assign('user', $user);
			}
			$smarty->display('broadcast-sms-per-alumni.tpl');
		}
		else if($id==2){
			$smarty->display('broadcast-sms-all-alumni.tpl');
		}
		else if($id==3){
			$db->Query("select distinct thn_angkatan_mhs, 'ALUMNI ANGKATAN '||thn_angkatan_mhs from mahasiswa where thn_lulus_mhs is not null order by thn_angkatan_mhs desc");
			$i = 0;
			$jml_angkatan = 0;
			while($row = $db->FetchRow()){
				$angkatan[$i] = $row[0];
				$nama_angkatan[$i] = $row[1];
				$i++;
				$jml_angkatan++;
			}
			
			$smarty->assign('angkatan', $angkatan);
			$smarty->assign('nama_angkatan', $nama_angkatan);
			$smarty->assign('jml_angkatan', $jml_angkatan);
			$smarty->display('broadcast-sms-per-angkatan.tpl');
			
		}
		else if($id==4){
			$db->Query("SELECT DISTINCT id_semester,
						  periode
						FROM
						  (SELECT b.id_semester AS id_semester,
							b.tahun_ajaran
							||' - '
							||b.nm_semester AS periode
						  FROM admisi a
						  LEFT JOIN semester b
						  ON b.id_semester     = a.id_semester
						  WHERE (b.nm_semester = 'Ganjil'
						  OR b.nm_semester     = 'Genap')
						  ORDER BY b.tahun_ajaran DESC
						  )
			");
			
			$i = 0;
			$jml_periode = 0;
			while($row = $db->FetchRow()){

				$periode[$i] = $row[0];
				$nama_periode[$i] = $row[1];
				$i++;
				$jml_periode++;
			}

			$smarty->assign('periode', $periode);
			$smarty->assign('nama_periode', $nama_periode);
			$smarty->assign('jml_periode', $jml_periode);
			$smarty->display('broadcast-sms-per-periode-lulus.tpl');
		}
	}
	else{
		$smarty->display('broadcast-sms.tpl');
	}
?>