<?php

include 'config.php';
include 'class/laporan.class.php';
include 'class/master.class.php';

$lap = new laporan($db);
$master = new master($db);

if (isset($_GET)) {
    if (get('form') != '') {
        if (get('mode') == 'detail') {
            $smarty->assign('soal', $master->get_soal(get('soal')));
            $smarty->assign('data_detail', $lap->load_detail_pengisian_jawaban(get('soal'), get('tipe')));
        }
        $smarty->assign('data_soal', $lap->load_laporan_pengisian_jawaban(get('form')));
    }
}
$smarty->assign('data_form', $master->load_form());
$smarty->display('laporan-pengisian-jawaban.tpl');
?>