<?php
include 'config.php';
include '../../tcpdf/config/lang/ind.php';
include '../../tcpdf/tcpdf.php';
include 'class/master.class.php';

$master = new master($db);

$data_pdf = $master->load_template_form(get('f'));

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    .header { font-size: 20pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 16pt; font-family: serif; margin-top: 0px ;text-align:center; }
    th.kategori { background-color : black; color: white; font-size: 13pt; text-align: left; font-weight: bold; }
    td.soal { background-color :#e2e2e2; color:black; font-size: 11pt; text-align:left; font-weight:bold;border-top:1px solid black; }
    td { font-size:9pt}
    i { font-size: 8pt;}
    </style>
<table width="100%" border="0">
    <tr>
        <td width="8%" align="right"><img src="../../img/maba/logo_unair.gif" width="80px" height="80px"/></td>
        <td width="85%" align="center">
            <span class="header">Pusat Pembinaan Karir dan Kewirausahaan Universitas Airlangga</span><br/><br/>
            <span class="address">FORM TRACER STUDI</span>
        </td>
    </tr>
</table>
<hr/>
<p></p>
{soal}

EOF;

$soal = '';

foreach ($data_pdf as $dp) {
    $soal .='
            <table width="100%" cellspacing="0" cellpadding="4" border="0">
                <tr>
                    <th class="kategori">' . $dp['KODE_KATEGORI'] . ' ' . $dp['NM_KATEGORI'] . '</th>
                </tr>
                <tr>
                    <td></td>
                </tr>
            ';
    foreach ($dp['DATA_SOAL'] as $s) {
        $soal.='
            <tr>
                <td class="soal">' . $s['NOMER'] . ' ' . $s['SOAL'] . '</td>
            </tr>            
            ';
        if ($s['TIPE_SOAL'] == 1) {
            foreach ($s['DATA_JAWABAN'] as $j) {
                $row_class = $j['URUTAN'] % 2 == 0 ? 'class="row"' : 'class="none"';
                $tambah_isian = $j['TIPE_JAWABAN'] == 1 ? '' : '............................(<i>Tuliskan</i>)';
                $soal.='
                    <tr ' . $row_class . '>
                        <td>
                            <table width="100%" cellpadding="0" border="0">
                                <tr>
                                    <td width="2%" border="1"></td>
                                    <td width="95%" >  ' . $j['JAWABAN'] . '' . $tambah_isian . '</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    ';
            }
        } else if ($s['TIPE_SOAL'] == 2) {
            foreach ($s['DATA_JAWABAN'] as $j) {
                $row_class = $j['URUTAN'] % 2 == 0 ? 'class="row"' : 'class="none"';
                $tambah_isian = $j['TIPE_JAWABAN'] == 1 ? '' : '............................(<i>Tuliskan</i>)';
                $soal.='
                    <tr ' . $row_class . '>
                        <td>
                            <table width="100%" cellpadding="0" border="0">
                                <tr>
                                    <td width="2%" border="1"></td>
                                    <td width="95%" >  ' . $j['JAWABAN'] . '' . $tambah_isian . '</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    ';
            }
        } else if ($s['TIPE_SOAL'] == 3) {
            foreach ($s['DATA_JAWABAN'] as $j) {
                $row_class = $j['URUTAN'] % 2 == 0 ? 'class="row"' : 'class="none"';
                $soal.='
                    <tr ' . $row_class . '>
                        <td>
                            <table width="100%" cellpadding="0" border="0">
                                <tr>
                                    <td>  ' . $j['JAWABAN'] . ' ................................................</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    ';
            }
        } else if ($s['TIPE_SOAL'] == 4) {
            foreach ($s['DATA_JAWABAN'] as $j) {
                $row_class = $j['URUTAN'] % 2 == 0 ? 'class="row"' : 'class="none"';
                $soal.='
                    <tr ' . $row_class . '>
                        <td>
                            <table cellpadding="0" border="0">
                                <tr>
                                    <td>  ' . $j['JAWABAN'] . ' : ..................................................................................</td>                         
                                </tr>
                            </table>
                        </td>
                    </tr>
                    ';
            }
        } else if ($s['TIPE_SOAL'] == 5) {
            foreach ($s['DATA_JAWABAN'] as $j) {
                $row_class = $j['URUTAN'] % 2 == 0 ? 'class="row"' : 'class="none"';
                $soal.='
                    <tr ' . $row_class . '>
                        <td>
                            <table cellpadding="2" border="0">
                                <tr>
                                    
                    ';
                if ($j['TIPE_JAWABAN'] == 1) {
                    $max_length = $j['MAX_LENGTH'] != '' ? $j['MAX_LENGTH'] : 3;
                    for ($i = 1; $i <= $max_length; $i++) {
                        $soal.='<td width="15" border="1"></td>';
                    }
                    $soal.='
                            <td width="500" >  ' . $j['JAWABAN'] . '</td>
                            </tr>
                        </table>
                        </td>
                    </tr>';
                } else {
                    $max_length = 3;
                    for ($i = 1; $i <= $max_length; $i++) {
                        $soal.='<td width="15" border="1"></td>';
                    }
                    $soal.='
                            <td width="600">  ' . $j['JAWABAN'] . ' ..................... (Tuliskan)</td>
                            </tr>
                        </table>
                        </td>
                    </tr>';
                }
            }
        } else if ($s['TIPE_SOAL'] == 6) {
            $soal.='
                <tr>
                    <td>' . $s['KETERANGAN_SOAL'] . ' (<i>Lingkari Skor Jawaban</i>)</td>
                </tr>
                <tr>
                    <td>
                ';
            $group_jawaban = 0;
            foreach ($s['DATA_JAWABAN'] as $j) {
                if ($j['GROUP_JAWABAN'] != $group_jawaban) {
                    if ($group_jawaban != '0') {
                        $soal.='<br/>';
                    }
                    $soal.='<br/><b>' . $j['JAWABAN'] . '</b><br/><br/>';
                    $group_jawaban = $j['GROUP_JAWABAN'];
                }
                $soal.=$j['SKOR_JAWABAN'] . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            $soal.=' </td>
                    </tr>';
        }
    }
    $soal .='</table>
        <p></p>';
}

$html = str_replace('{soal}', $soal, $html);
//echo $html;
$pdf->writeHTML($html);
$pdf->Output();
?>
