<?php
	include '../../config.php';
	
	if(isset($_GET['src'])){
		$src = $_GET['src'];
		if($src=='angkatan'){
			$id = $_GET['id'];
			$db->Query("
				SELECT c.email_pengguna
				FROM admisi a
				LEFT JOIN mahasiswa b
				ON b.id_mhs = a.id_mhs
				LEFT JOIN pengguna c
				ON c.id_pengguna = b.id_pengguna
				LEFT JOIN semester d
				ON d.id_semester       = a.id_semester
				WHERE a.tgl_lulus     IS NOT NULL
				AND b.thn_angkatan_mhs = '$id'
				AND c.email_pengguna  IS NOT NULL
				-- AND c.username = '080810374F'
				AND c.email_pengguna is not null
				UNION ALL(
				SELECT c.email_alternate
				FROM admisi a
				LEFT JOIN mahasiswa b
				ON b.id_mhs = a.id_mhs
				LEFT JOIN pengguna c
				ON c.id_pengguna = b.id_pengguna
				LEFT JOIN semester d
				ON d.id_semester       = a.id_semester
				WHERE a.tgl_lulus     IS NOT NULL
				AND b.thn_angkatan_mhs = '$id'
				AND c.email_pengguna  IS NOT NULL
				-- AND c.username = '080810374F'
				AND c.email_alternate is not null
				)
			");
			
			echo "<select multiple='multiple' name='email[]' id='email' style='min-height:10px;width:100%;'>";
			while($row = $db->FetchRow()){
				echo "<option value='$row[0]' selected>" . $row[0] . "</option>";
			}
			echo "</select>";
		}
		else if($src=='all'){
			$db->Query("
				SELECT c.email_pengguna
				FROM admisi a
				LEFT JOIN mahasiswa b
				ON b.id_mhs = a.id_mhs
				LEFT JOIN pengguna c
				ON c.id_pengguna = b.id_pengguna
				LEFT JOIN semester d
				ON d.id_semester       = a.id_semester
				WHERE a.tgl_lulus     IS NOT NULL
				--AND b.thn_angkatan_mhs = '2008'
				AND c.email_pengguna  IS NOT NULL
				-- AND c.username = '080810374F'
				AND c.email_pengguna is not null
				UNION ALL(
				SELECT c.email_alternate
				FROM admisi a
				LEFT JOIN mahasiswa b
				ON b.id_mhs = a.id_mhs
				LEFT JOIN pengguna c
				ON c.id_pengguna = b.id_pengguna
				LEFT JOIN semester d
				ON d.id_semester       = a.id_semester
				WHERE a.tgl_lulus     IS NOT NULL
				--AND b.thn_angkatan_mhs = '2008'
				AND c.email_pengguna  IS NOT NULL
				-- AND c.username = '080810374F'
				AND c.email_alternate is not null
				)
			");
			
			echo "<select multiple='multiple' name='email[]' id='email' style='min-height:10px;width:100%;'>";
			while($row = $db->FetchRow()){
				echo "<option value='$row[0]' selected>" . $row[0] . "</option>";
			}
			echo "</select>";
		}
		else if($src=='periode'){
			$id = $_GET['id'];
			$db->Query("
				SELECT c.email_pengguna
				FROM admisi a
				LEFT JOIN mahasiswa b
				ON b.id_mhs = a.id_mhs
				LEFT JOIN pengguna c
				ON c.id_pengguna = b.id_pengguna
				LEFT JOIN semester d
				ON d.id_semester       = a.id_semester
				WHERE a.tgl_lulus     IS NOT NULL
				AND d.id_semester = '$id'
				AND c.email_pengguna  IS NOT NULL
				-- AND c.username = '080810374F'
				AND c.email_pengguna is not null
				UNION ALL(
				SELECT c.email_alternate
				FROM admisi a
				LEFT JOIN mahasiswa b
				ON b.id_mhs = a.id_mhs
				LEFT JOIN pengguna c
				ON c.id_pengguna = b.id_pengguna
				LEFT JOIN semester d
				ON d.id_semester       = a.id_semester
				WHERE a.tgl_lulus     IS NOT NULL
				AND d.id_semester = '$id'
				AND c.email_pengguna  IS NOT NULL
				-- AND c.username = '080810374F'
				AND c.email_alternate is not null
				)
			");
			
			echo "<select multiple='multiple' name='email[]' id='email' style='min-height:10px;width:100%;'>";
			while($row = $db->FetchRow()){
				echo "<option value='$row[0]' selected>" . $row[0] . "</option>";
			}
			echo "</select>";
		}
	}
?>