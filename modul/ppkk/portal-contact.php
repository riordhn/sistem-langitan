<?php
	include 'config.php';
	include 'class/portal.contact.class.php';
	
	$contact = new contact($db);

	if(isset($_GET['id'])&& isset($_GET['mode'])){
		$id = get('id');
		$mode = get('mode');
		if($mode=='edit'){
			$smarty->assign(array(
							   'id'=>$id, 
							   'mode'=>$mode,
							   'contact'=>$contact->GetContactById($id)
						   )
			);
		}

	}
	
	
	else if(isset($_GET['id'])){
		$id = get('id');
		$smarty->assign(array(
						   'id'=>$id, 
						   'contact'=>$contact->GetContactById($id)
					   )
		);
		
	}
	
	else{
		$smarty->assign('contact', $contact->GetLatestContact());
	}
	
	if(isset($_POST['contact_content']) && isset($_POST['id_contact'])){
		$isi = post('contact_content');
		$id = post('id_contact');
		$contact->UpdateContactById($id, $isi);
		$smarty->assign('contact', $contact->GetLatestContact());
	}
	
	$smarty->display('portal/contact/view.tpl');
	
?>