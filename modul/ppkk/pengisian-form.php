<?php

include 'config.php';
include 'class/laporan.class.php';
include 'class/master.class.php';

$master = new master($db);
$laporan = new laporan($db);

if (isset($_GET)) {
    if (get('form') != '') {
        if (get('mode') == 'pengisi') {
            $smarty->assign('data_pengisi', $laporan->load_data_pengisi(get('kat'), get('fakultas'), get('prodi')));
        } else {
            $smarty->assign('data_laporan_pengisian', $laporan->load_laporan_pengisian_form(get('fakultas'), get('form')));
        }
    }
}

$smarty->assign('data_fakultas', $db->QueryToArray("SELECT * FROM FAKULTAS ORDER BY ID_FAKULTAS"));
$smarty->assign('data_form', $master->load_form());
$smarty->display('pengisian-form.tpl');
?>
