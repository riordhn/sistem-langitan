<?php
	include 'config.php';
	
	include 'class/portal.sambutan.class.php';
	
	$news = new news($db);

	if(isset($_GET['id'])&& isset($_GET['mode'])){
		$id = get('id');
		$mode = get('mode');
		if($mode=='edit'){
			$smarty->assign(array(
							   'id'=>$id, 
							   'mode'=>$mode,
							   'news'=>$news->GetNewsById($id)
						   )
			);
		}
	}
	
	
	else if(isset($_GET['id'])){
		$id = get('id');
		$smarty->assign(array(
						   'id'=>$id, 
						   'news'=>$news->GetNewsById($id)
					   )
		);
	}
	
	else{
		$smarty->assign('news', $news->GetLatestNews());
	}
	
	if(isset($_POST['sambutan_content']) && isset($_POST['id_sambutan'])){
		$isi = post('sambutan_content');
		$id = post('id_sambutan');
		$news->UpdateNewsById($id, $isi);
		$smarty->assign('news', $news->GetLatestNews());
	}
	
	$smarty->display('portal/sambutan/view.tpl');
?>