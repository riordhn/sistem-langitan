<?PHP
include('config.php');
/*

$lulusan_31 = $db->QueryToArray("SELECT THN_SEMESTER AS THN_AKADEMIK_SMT,ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, STATUS AS STATUS_MHS,   
		IPK1_JML, IPK2_JML, IPK3_JML, IPK4_JML, IPK5_JML, TOTAL_LULUSAN, RATA
		-- ROUND((RATA1_IPK+RATA2_IPK+RATA3_IPK+RATA4_IPK+RATA5_IPK)/ ( (CASE WHEN IPK1_JML > 0 THEN 1 ELSE 0 END) + (CASE WHEN IPK2_JML > 0 THEN 1 ELSE 0 END) + (CASE WHEN IPK3_JML > 0 THEN 1 ELSE 0 END) + (CASE WHEN IPK4_JML > 0 THEN 1 ELSE 0 END) + (CASE WHEN IPK5_JML > 0 THEN 1 ELSE 0 END) ), 2) AS RATA
		FROM (
		SELECT A.*, 
		(CASE WHEN IPK1_JML IS NULL THEN 0 ELSE IPK1_JML END) AS IPK1_JML, 
		(CASE WHEN IPK2_JML IS NULL THEN 0 ELSE IPK2_JML END) AS IPK2_JML, 
		(CASE WHEN IPK3_JML IS NULL THEN 0 ELSE IPK3_JML END) AS IPK3_JML, 
		(CASE WHEN IPK4_JML IS NULL THEN 0 ELSE IPK4_JML END) AS IPK4_JML, 
		(CASE WHEN IPK5_JML IS NULL THEN 0 ELSE IPK5_JML END) AS IPK5_JML,
		(CASE WHEN RATA1_IPK IS NULL THEN 0 ELSE RATA1_IPK END) AS RATA1_IPK, 
		(CASE WHEN RATA2_IPK IS NULL THEN 0 ELSE RATA2_IPK END) AS RATA2_IPK, 
		(CASE WHEN RATA3_IPK IS NULL THEN 0 ELSE RATA3_IPK END) AS RATA3_IPK, 
		(CASE WHEN RATA4_IPK IS NULL THEN 0 ELSE RATA4_IPK END) AS RATA4_IPK, 
		(CASE WHEN RATA5_IPK IS NULL THEN 0 ELSE RATA5_IPK END) AS RATA5_IPK,  RATA_IPK_TOTAL  AS RATA
		FROM (
		select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS TOTAL_LULUSAN
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1 and tgl_lulus is not null
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						
		where  to_char(TGL_LULUS, 'YYYY') >= '2007'
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
		) A 
		LEFT JOIN (
		select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK1_JML, AVG(IPK) AS RATA1_IPK
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
							from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
							c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
							row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
							count(*) over(partition by c.nm_mata_kuliah) terulang
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
							) x
							where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
							group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 2 and ipk < 2.5
		where tgl_lulus is not null and program_studi.id_jenjang in (1, 5)
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
		
		UNION 
		
		select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK1_JML, AVG(IPK) AS RATA1_IPK
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
							from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
							c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
							row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
							count(*) over(partition by c.nm_mata_kuliah) terulang
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
							) x
							where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
							group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 2.75 and ipk < 3
		where tgl_lulus is not null and program_studi.id_jenjang in (2, 3)
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
		) B ON B.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND B.THN_SEMESTER = A.THN_SEMESTER AND  B.TAHUN_LULUS = A.TAHUN_LULUS AND  B.STATUS = A.STATUS AND B.KELAMIN_PENGGUNA = A.KELAMIN_PENGGUNA


		LEFT JOIN (
		select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK2_JML, AVG(IPK) AS RATA2_IPK
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
							from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
							c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
							row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
							count(*) over(partition by c.nm_mata_kuliah) terulang
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
							) x
							where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
							group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 2.5 and ipk < 2.75
		where tgl_lulus is not null and program_studi.id_jenjang in (1, 5)
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
		
		UNION
		
		select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK2_JML, AVG(IPK) AS RATA2_IPK
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
							from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
							c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
							row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
							count(*) over(partition by c.nm_mata_kuliah) terulang
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
							) x
							where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
							group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 3 and ipk < 3.5
		where tgl_lulus is not null and program_studi.id_jenjang in (2, 3)
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
		) C ON C.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND C.THN_SEMESTER = A.THN_SEMESTER AND  C.TAHUN_LULUS = A.TAHUN_LULUS AND C.STATUS = A.STATUS AND C.KELAMIN_PENGGUNA = A.KELAMIN_PENGGUNA

	LEFT JOIN (
		select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK3_JML, AVG(IPK) AS RATA3_IPK
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
							from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
							c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
							row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
							count(*) over(partition by c.nm_mata_kuliah) terulang
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
							) x
							where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
							group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 2.75 and ipk < 3
		where tgl_lulus is not null and program_studi.id_jenjang in (1, 5)
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
		
		UNION
		
		select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK3_JML, AVG(IPK) AS RATA3_IPK
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
							from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
							c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
							row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
							count(*) over(partition by c.nm_mata_kuliah) terulang
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
							) x
							where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
							group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 3.5 and ipk < 3.75
		where tgl_lulus is not null and program_studi.id_jenjang in (2, 3)
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
		) D ON D.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND D.THN_SEMESTER = A.THN_SEMESTER AND  D.TAHUN_LULUS = A.TAHUN_LULUS AND D.STATUS = A.STATUS AND D.KELAMIN_PENGGUNA = A.KELAMIN_PENGGUNA


		LEFT JOIN (
		select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK4_JML, AVG(IPK) AS RATA4_IPK
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
							from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
							c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
							row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
							count(*) over(partition by c.nm_mata_kuliah) terulang
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
							) x
							where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
							group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 3 and ipk < 3.5
		where tgl_lulus is not null and program_studi.id_jenjang in (1, 5)
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
		) E ON E.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND E.THN_SEMESTER = A.THN_SEMESTER AND E.TAHUN_LULUS = A.TAHUN_LULUS AND E.STATUS = A.STATUS AND E.KELAMIN_PENGGUNA = A.KELAMIN_PENGGUNA


		LEFT JOIN (
		select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK5_JML, AVG(IPK) AS RATA5_IPK
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
							from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
							c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
							row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
							count(*) over(partition by c.nm_mata_kuliah) terulang
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
							) x
							where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
							group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 3.5
		where tgl_lulus is not null and program_studi.id_jenjang in (1, 5)
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
		
		UNION
		
		select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK5_JML, AVG(IPK) AS RATA5_IPK
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
							from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
							c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
							row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
							count(*) over(partition by c.nm_mata_kuliah) terulang
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
							) x
							where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
							group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 3.75
		where tgl_lulus is not null and program_studi.id_jenjang in (2, 3)
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
		) F ON F.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND F.THN_SEMESTER = A.THN_SEMESTER AND F.TAHUN_LULUS = A.TAHUN_LULUS AND F.STATUS = A.STATUS AND F.KELAMIN_PENGGUNA = A.KELAMIN_PENGGUNA
		
		
		LEFT JOIN (
		select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, AVG(IPK) AS RATA_IPK_TOTAL
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
							from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
							c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
							row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
							count(*) over(partition by c.nm_mata_kuliah) terulang
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
							) x
							where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
							group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS
		where tgl_lulus is not null
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
		) G ON G.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND G.THN_SEMESTER = A.THN_SEMESTER AND G.TAHUN_LULUS = A.TAHUN_LULUS AND G.STATUS = A.STATUS AND G.KELAMIN_PENGGUNA = A.KELAMIN_PENGGUNA

		) 
");

//
//$db_wh->Query("INSERT INTO WH_SE_LULUSAN_IPK_LOG (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, IPK1_JML, IPK2_JML, IPK3_JML, IPK4_JML, IPK5_JML, IPK1_PERSEN, IPK2_PERSEN, IPK3_PERSEN, IPK4_PERSEN, IPK5_PERSEN, TOTAL_KELAMIN, RATA, THN_AKADEMIK_LULUS, STATUS_MHS, JENIS_KELAMIN, TGL_UPDATE)
//SELECT THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, IPK1_JML, IPK2_JML, IPK3_JML, IPK4_JML, IPK5_JML, IPK1_PERSEN, IPK2_PERSEN, IPK3_PERSEN, IPK4_PERSEN, IPK5_PERSEN, TOTAL_KELAMIN, RATA, THN_AKADEMIK_LULUS, STATUS_MHS, JENIS_KELAMIN, SYSDATE AS TGL_UPDATE FROM WH_SE_LULUSAN_IPK");

$db_wh->Query("DELETE WH_SE_LULUSAN_IPK");

foreach($lulusan_31 as $data){
	
	if($data['TOTAL_LULUSAN'] > ($data['IPK1_JML'] + $data['IPK2_JML'] + $data['IPK3_JML'] + $data['IPK4_JML'] + $data['IPK5_JML'])){
		$data['IPK1_JML'] = $data['IPK1_JML'] + ($data['TOTAL_LULUSAN'] - ($data['IPK1_JML'] + $data['IPK2_JML'] + $data['IPK3_JML'] + $data['IPK4_JML'] + $data['IPK5_JML']));
	}
	

$db_wh->Query("INSERT INTO WH_SE_LULUSAN_IPK (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, IPK1_JML, IPK2_JML, IPK3_JML, IPK4_JML, IPK5_JML, STATUS_MHS, TGL_UPDATE, RATA)
VALUES
('$data[THN_AKADEMIK_SMT]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[IPK1_JML]', '$data[IPK2_JML]', '$data[IPK3_JML]', '$data[IPK4_JML]', '$data[IPK5_JML]', '$data[STATUS_MHS]', '$data[TGL_UPDATE]', '$data[RATA]')");

}
*/



/*

$lulusan_35 = $db->QueryToArray("
select ID_PROGRAM_STUDI, ID_FAKULTAS, id_jenjang, STATUS, THN_AKADEMIK_LULUS, THN_AKADEMIK_SMT, kelamin_pengguna, 
(lama_studi + (case when semester_studi = 1 then 1 when semester_studi = 0 then 0.5 else 0 end)) as LAMA_STUDI
from (
select PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, JENJANG.id_jenjang, STATUS, 
TO_CHAR(TGL_LULUS, 'YYYY') as THN_AKADEMIK_LULUS, b.THN_AKADEMIK_SEMESTER || (CASE WHEN b.NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_AKADEMIK_SMT, 
(b.THN_AKADEMIK_SEMESTER - a.THN_AKADEMIK_SEMESTER - (case when cuti is null then 0 else cuti end)) as lama_studi, 
(CASE WHEN b.NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) - (CASE WHEN a.NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) as semester_studi
, kelamin_pengguna
from mahasiswa 
join semester a on a.id_semester = mahasiswa.id_semester_masuk
join pengguna on PENGGUNA.id_pengguna = MAHASISWA.id_pengguna
join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi 
join fakultas on fakultas.id_fakultas = program_studi.id_fakultas 
join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs 
join jenjang on jenjang.id_jenjang = program_studi.id_jenjang 
join admisi on ADMISI.ID_MHS = MAHASISWA.ID_MHS and status_akd_mhs = '4' and status_apv = 1 AND TGL_LULUS IS NOT NULL
JOIN SEMESTER b ON b.ID_SEMESTER = ADMISI.ID_SEMESTER

left join (
	select id_mhs, count(*) as cuti from ADMISI where status_apv = 1 and status_akd_mhs = 2
group by id_mhs 
) x on x.id_mhs = MAHASISWA.id_mhs
where to_char(TGL_LULUS, 'YYYY') >= '2007'
)
");


//$db_wh->Query("INSERT INTO WH_SE_LULUSAN_MASA_ST_LOG (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, TOTAL_LULUSAN, MASA_STUDI, STATUS_MHS, TGL_UPDATE, THN_AKADEMIK_LULUS, JENIS_KELAMIN)
//SELECT THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, TOTAL_LULUSAN, MASA_STUDI, STATUS_MHS, SYSDATE AS TGL_UPDATE, THN_AKADEMIK_LULUS, JENIS_KELAMIN 
//FROM WH_SE_LULUSAN_MASA_ST");

$db_wh->Query("DELETE WH_SE_LULUSAN_MASA_ST");

foreach($lulusan_35 as $data){

$db_wh->Query("INSERT INTO WH_SE_LULUSAN_MASA_ST (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, MASA_STUDI, STATUS_MHS, TGL_UPDATE, THN_AKADEMIK_LULUS, JENIS_KELAMIN)
VALUES
('$data[THN_AKADEMIK_SMT]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[LAMA_STUDI]', '$data[STATUS]', SYSDATE, '$data[THN_AKADEMIK_LULUS]', '$data[KELAMIN_PENGGUNA]')");

}*/




/*
$lulusan_36 = $db->QueryToArray("SELECT THN_SEMESTER AS THN_AKADEMIK_SMT,ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, STATUS AS STATUS_MHS, TOTAL_LULUSAN, 
CASE WHEN TOTAL_LULUSAN != (LAMA_SKRIPSI1_JML+LAMA_SKRIPSI2_JML+LAMA_SKRIPSI3_JML) THEN (LAMA_SKRIPSI1_JML + TOTAL_LULUSAN - (LAMA_SKRIPSI1_JML+LAMA_SKRIPSI2_JML+LAMA_SKRIPSI3_JML)) ELSE LAMA_SKRIPSI1_JML END AS LAMA_SKRIPSI1_JML,
LAMA_SKRIPSI2_JML, LAMA_SKRIPSI3_JML, TAHUN_LULUS AS THN_AKADEMIK_LULUS
FROM 
(
SELECT A.*, 
		(CASE WHEN LAMA_SKRIPSI1_JML IS NULL AND LAMA_SKRIPSI2_JML IS NULL AND LAMA_SKRIPSI3_JML IS NULL THEN TOTAL_LULUSAN WHEN LAMA_SKRIPSI1_JML IS NULL THEN 0 ELSE LAMA_SKRIPSI1_JML END) AS LAMA_SKRIPSI1_JML,
		(CASE WHEN LAMA_SKRIPSI2_JML IS NULL THEN 0 ELSE LAMA_SKRIPSI2_JML END) AS LAMA_SKRIPSI2_JML, 
		(CASE WHEN LAMA_SKRIPSI3_JML IS NULL THEN 0 ELSE LAMA_SKRIPSI3_JML END) AS LAMA_SKRIPSI3_JML
FROM (
select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, COUNT(*) AS TOTAL_LULUSAN
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1 and tgl_lulus is not null
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER				
		where  to_char(TGL_LULUS, 'YYYY') >= '2007'
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS
) A
LEFT JOIN (

	select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, COUNT(*) AS LAMA_SKRIPSI1_JML
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1 and tgl_lulus is not null
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select a.id_mhs, g.nim_mhs, count(*) as lama_skripsi
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4 and d.status_mkta = 1 and a.flagnilai = 1
							group by a.id_mhs, g.nim_mhs
							) x on x.id_mhs = MAHASISWA.ID_MHS and lama_skripsi <= 1 AND PROGRAM_STUDI.ID_JENJANG IN (1, 5)
		where  to_char(TGL_LULUS, 'YYYY') >= '2007'
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS

UNION 

	select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, COUNT(*) AS LAMA_SKRIPSI1_JML
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1 and tgl_lulus is not null
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select a.id_mhs, g.nim_mhs, count(*) as lama_skripsi
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4 and d.status_mkta = 1 and a.flagnilai = 1
							group by a.id_mhs, g.nim_mhs
							) x on x.id_mhs = MAHASISWA.ID_MHS and lama_skripsi <= 2 AND PROGRAM_STUDI.ID_JENJANG = 2
		where  to_char(TGL_LULUS, 'YYYY') >= '2007'
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS


UNION 

	select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, COUNT(*) AS LAMA_SKRIPSI1_JML
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1 and tgl_lulus is not null
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select a.id_mhs, g.nim_mhs, count(*) as lama_skripsi
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4 and d.status_mkta = 1 and a.flagnilai = 1
							group by a.id_mhs, g.nim_mhs
							) x on x.id_mhs = MAHASISWA.ID_MHS and lama_skripsi <= 3 AND PROGRAM_STUDI.ID_JENJANG = 3
		where  to_char(TGL_LULUS, 'YYYY') >= '2007'
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS
) B ON B.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND B.THN_SEMESTER = A.THN_SEMESTER AND  B.TAHUN_LULUS = A.TAHUN_LULUS AND  B.STATUS = A.STATUS
LEFT JOIN (

	select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, COUNT(*) AS LAMA_SKRIPSI2_JML
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1 and tgl_lulus is not null
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select a.id_mhs, g.nim_mhs, count(*) as lama_skripsi
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4 and d.status_mkta = 1 and a.flagnilai = 1
							group by a.id_mhs, g.nim_mhs
							) x on x.id_mhs = MAHASISWA.ID_MHS and lama_skripsi > 1 AND lama_skripsi <= 2 AND PROGRAM_STUDI.ID_JENJANG IN (1, 5)
		where  to_char(TGL_LULUS, 'YYYY') >= '2007'
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS

UNION 

	select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, COUNT(*) AS LAMA_SKRIPSI2_JML
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1 and tgl_lulus is not null
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select a.id_mhs, g.nim_mhs, count(*) as lama_skripsi
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4 and d.status_mkta = 1 and a.flagnilai = 1
							group by a.id_mhs, g.nim_mhs
							) x on x.id_mhs = MAHASISWA.ID_MHS and lama_skripsi > 2 AND lama_skripsi <= 3 AND PROGRAM_STUDI.ID_JENJANG = 2
		where  to_char(TGL_LULUS, 'YYYY') >= '2007'
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS


UNION 

	select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, COUNT(*) AS LAMA_SKRIPSI2_JML
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1 and tgl_lulus is not null
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select a.id_mhs, g.nim_mhs, count(*) as lama_skripsi
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4 and d.status_mkta = 1 and a.flagnilai = 1
							group by a.id_mhs, g.nim_mhs
							) x on x.id_mhs = MAHASISWA.ID_MHS and lama_skripsi > 3 AND lama_skripsi <= 4 AND PROGRAM_STUDI.ID_JENJANG = 3
		where  to_char(TGL_LULUS, 'YYYY') >= '2007'
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS
) C ON C.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND C.THN_SEMESTER = A.THN_SEMESTER AND  C.TAHUN_LULUS = A.TAHUN_LULUS AND C.STATUS = A.STATUS
LEFT JOIN (

	select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, COUNT(*) AS LAMA_SKRIPSI3_JML
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1 and tgl_lulus is not null
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select a.id_mhs, g.nim_mhs, count(*) as lama_skripsi
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4 and d.status_mkta = 1 and a.flagnilai = 1
							group by a.id_mhs, g.nim_mhs
							) x on x.id_mhs = MAHASISWA.ID_MHS and lama_skripsi > 2 AND PROGRAM_STUDI.ID_JENJANG IN (1, 5)
		where  to_char(TGL_LULUS, 'YYYY') >= '2007'
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS

UNION 

	select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, COUNT(*) AS LAMA_SKRIPSI3_JML
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1 and tgl_lulus is not null
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select a.id_mhs, g.nim_mhs, count(*) as lama_skripsi
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4 and d.status_mkta = 1 and a.flagnilai = 1
							group by a.id_mhs, g.nim_mhs
							) x on x.id_mhs = MAHASISWA.ID_MHS and lama_skripsi > 3 AND PROGRAM_STUDI.ID_JENJANG = 2
		where  to_char(TGL_LULUS, 'YYYY') >= '2007'
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS


UNION 

	select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, COUNT(*) AS LAMA_SKRIPSI3_JML
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1 and tgl_lulus is not null
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						join (
							select a.id_mhs, g.nim_mhs, count(*) as lama_skripsi
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4 and d.status_mkta = 1 and a.flagnilai = 1
							group by a.id_mhs, g.nim_mhs
							) x on x.id_mhs = MAHASISWA.ID_MHS and lama_skripsi > 4 AND PROGRAM_STUDI.ID_JENJANG = 3
		where  to_char(TGL_LULUS, 'YYYY') >= '2007'
		GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS

) D ON D.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND D.THN_SEMESTER = A.THN_SEMESTER AND  D.TAHUN_LULUS = A.TAHUN_LULUS AND D.STATUS = A.STATUS
)
");


$db_wh->Query("INSERT INTO WH_SE_LULUSAN_MASA_ST_LOG (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, TOTAL_LULUSAN, MASA_STUDI, STATUS_MHS, TGL_UPDATE, THN_AKADEMIK_LULUS, JENIS_KELAMIN)
SELECT THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, TOTAL_LULUSAN, MASA_STUDI, STATUS_MHS, SYSDATE AS TGL_UPDATE, THN_AKADEMIK_LULUS, JENIS_KELAMIN 
FROM WH_SE_LULUSAN_MASA_ST");


$db_wh->Query("DELETE WH_SE_LULUSAN_SKRIPSI");

foreach($lulusan_36 as $data){

$db_wh->Query("INSERT INTO WH_SE_LULUSAN_SKRIPSI (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, TOTAL_LULUSAN, LAMA_SKRIPSI1_JML, LAMA_SKRIPSI2_JML, LAMA_SKRIPSI3_JML, STATUS_MHS, TGL_UPDATE, THN_AKADEMIK_LULUS)
VALUES
('$data[THN_AKADEMIK_SMT]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[TOTAL_LULUSAN]', '$data[LAMA_SKRIPSI1_JML]', '$data[LAMA_SKRIPSI2_JML]', '$data[LAMA_SKRIPSI3_JML]', '$data[STATUS_MHS]', SYSDATE, '$data[THN_AKADEMIK_LULUS]')");

}

*/

/*
$lulusan_31 = $db->QueryToArray("
select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as THN_AKADEMIK_LULUS,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, NIM_MHS, ipk, sks
		from mahasiswa
						join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						left join (
							select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
							from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
							c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
							row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
							count(*) over(partition by c.nm_mata_kuliah) terulang
								from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
							) x
							where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
							group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS
		where tgl_lulus is not null and to_char(TGL_LULUS, 'YYYY') >= '2007'
");



$db_wh->Query("DELETE WH_SE_LULUSAN_IPK_MHS");

foreach($lulusan_31 as $data){

$db_wh->Query("INSERT INTO WH_SE_LULUSAN_IPK_MHS (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, STATUS_MHS, THN_AKADEMIK_LULUS, JENIS_KELAMIN, TGL_UPDATE, NIM_MHS, IPK, SKS)
VALUES
('$data[THN_SEMESTER]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[STATUS]', '$data[THN_AKADEMIK_LULUS]',  '$data[KELAMIN_PENGGUNA]', SYSDATE, '$data[NIM_MHS]', '$data[IPK]', '$data[SKS]')");

}*/

/*
//lulusan_31
$db_wh->Query("DELETE WH_SE_LULUSAN_IPK_MHS");
$db_wh->Query("INSERT INTO WH_SE_LULUSAN_IPK_MHS (THN_AKADEMIK_SMT, THN_AKADEMIK_LULUS, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, STATUS_MHS, JENIS_KELAMIN, NIM_MHS, IPK, SKS, TGL_UPDATE)
select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as THN_AKADEMIK_LULUS,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, NIM_MHS, ipk, sks, SYSDATE
		from AUCC.mahasiswa@AUCCRO
						join AUCC.pengguna@AUCCRO on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join AUCC.admisi@AUCCRO on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
						join AUCC.program_studi@AUCCRO on program_studi.id_program_studi = mahasiswa.id_program_studi
						join AUCC.semester@AUCCRO on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						left join (
							select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
							from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
							c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
							row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
							count(*) over(partition by c.nm_mata_kuliah) terulang
								from AUCC.pengambilan_mk@AUCCRO a, AUCC.mata_kuliah@AUCCRO c, AUCC.kurikulum_mk@AUCCRO d, AUCC.semester@AUCCRO e, 
								AUCC.standar_nilai@AUCCRO f, AUCC.mahasiswa@AUCCRO g, AUCC.program_studi@AUCCRO h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
							) x
							where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
							group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS
		where tgl_lulus is not null and to_char(TGL_LULUS, 'YYYY') >= '2007'");*/

echo "Berhasil Disimpan";


?>