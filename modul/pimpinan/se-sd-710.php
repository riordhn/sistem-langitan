<?php
include('config.php');

if($_SESSION['ID_PRODI'] != ''){
	$where = "AND z.ID_PROGRAM_STUDI = '".$_SESSION['ID_PRODI']."'";
	$header = $_SESSION['JENJANG'] . ' - '. $_SESSION['PRODI'];

}elseif($_SESSION['ID_FAK'] != ''){
	$where = "AND z.ID_FAKULTAS = '".$_SESSION['ID_FAK']."'";
	$header = 'FAKULTAS '.strtoupper($_SESSION['NM_FAK']);

}else{
	$where = "";
	$header = strtoupper('Universitas Airlangga');
}

if($_SESSION['ID_PRODI'] != ''){
$thn_akademik = (date("Y") - 2).'/'.(date("Y") - 1);
$tampil .= '
<table class="ui-widget">
	<tr style="background:navy;color:#fff;padding:5px;">
		<td colspan="20"><div class="center_title_bar">Rincian Beban Dosen Tetap</div></td>
	</tr>
	<tr class="ui-widget-header" style="padding:5px;">
		<td colspan="20"><div style="padding:10px;">'.$header.'</div></td>
	</tr>
</table>
<h2>SEMESTER GASAL '.$thn_akademik.'</h2>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<th rowspan="3" style="vertical-align: middle;"><center>Nama Dosen</center></th>
			<th colspan="18"><center>Beban Dosen (SKS) Pada Kegiatan</center></th>
			<th rowspan="3" style="vertical-align: middle;"><center>Total Beban</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th colspan="5"><center>Kuliah</center></th>
			<th colspan="5"><center>Praktikum</center></th>
			<th colspan="5"><center>Pembimbingan</center></th>
			<th rowspan="2"><center>Penelitian</center></th>
			<th rowspan="2"><center>Pengmas</center></th>
			<th rowspan="2"><center>Manajemen<br>&amp;<br>Administrasi</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th><center>D3</center></th>
			<th><center>S1</center></th>
			<th><center>S2</center></th>
			<th><center>S3</center></th>
			<th><center>Prodi Lain</center></th>
			<th><center>D3</center></th>
			<th><center>S1</center></th>
			<th><center>S2</center></th>
			<th><center>S3</center></th>
			<th><center>Prodi Lain</center></th>
			<th><center>D3</center></th>
			<th><center>S1</center></th>
			<th><center>S2</center></th>
			<th><center>S3</center></th>
			<th><center>Dosen Wali</center></th>
		</tr>
';
$dt1 = $db_wh->QueryToArray("
select * from WH_BEBAN_DOSEN where ID_PROGRAM_STUDI = '".$_SESSION['ID_PRODI']."'
and substr(TAHUN_SEMESTER,1,4) = extract(year from sysdate) - 1
and substr(TAHUN_SEMESTER,-1) = 1 and (STATUS_DOSEN <> 'LB' or STATUS_DOSEN <> '' or STATUS_DOSEN is null)
");
foreach($dt1 as $data1){
$tampil .= '
	<tr class="ui-widget-content">
		<td>'.$data1['NM_LENGKAP_PENGGUNA'].'</td>
		<td><center>'.$data1['D3_KULIAH'].'</center></td>
		<td><center>'.$data1['S1_KULIAH'].'</center></td>
		<td><center>'.$data1['S2_KULIAH'].'</center></td>
		<td><center>'.$data1['S3_KULIAH'].'</center></td>
		<td><center>'.$data1['PRODI_LAIN_KULIAH'].'</center></td>
		<td><center>'.$data1['D3_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['S1_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['S2_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['S3_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['PRODI_LAIN_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['D3_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['S1_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['S2_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['S3_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['DOSEN_WALI'].'</center></td>
		<td><center>'.$data1['PENELITIAN'].'</center></td>
		<td><center>'.$data1['PENGMAS'].'</center></td>
		<td><center>'.$data1['MAN_ADM'].'</center></td>
		<td><center>'.$data1['TOTAL_BEBAN'].'</center></td>
	</tr>';
}

$tampil .= '
</table>
<h2>SEMESTER GENAP '.$thn_akademik.'</h2>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<th rowspan="3" style="vertical-align: middle;"><center>Nama Dosen</center></th>
			<th colspan="18"><center>Beban Dosen (SKS) Pada Kegiatan</center></th>
			<th rowspan="3" style="vertical-align: middle;"><center>Total Beban</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th colspan="5"><center>Kuliah</center></th>
			<th colspan="5"><center>Praktikum</center></th>
			<th colspan="5"><center>Pembimbingan</center></th>
			<th rowspan="2"><center>Penelitian</center></th>
			<th rowspan="2"><center>Pengmas</center></th>
			<th rowspan="2"><center>Manajemen<br>&amp;<br>Administrasi</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th><center>D3</center></th>
			<th><center>S1</center></th>
			<th><center>S2</center></th>
			<th><center>S3</center></th>
			<th><center>Prodi Lain</center></th>
			<th><center>D3</center></th>
			<th><center>S1</center></th>
			<th><center>S2</center></th>
			<th><center>S3</center></th>
			<th><center>Prodi Lain</center></th>
			<th><center>D3</center></th>
			<th><center>S1</center></th>
			<th><center>S2</center></th>
			<th><center>S3</center></th>
			<th><center>Dosen Wali</center></th>
		</tr>
';
$dt1 = $db_wh->QueryToArray("
select * from WH_BEBAN_DOSEN where ID_PROGRAM_STUDI = '".$_SESSION['ID_PRODI']."'
and substr(TAHUN_SEMESTER,1,4) = extract(year from sysdate) - 1
and substr(TAHUN_SEMESTER,-1) = 2 and (STATUS_DOSEN <> 'LB' or STATUS_DOSEN <> '' or STATUS_DOSEN is null)
");
foreach($dt1 as $data1){
$tampil .= '
	<tr class="ui-widget-content">
		<td>'.$data1['NM_LENGKAP_PENGGUNA'].'</td>
		<td><center>'.$data1['D3_KULIAH'].'</center></td>
		<td><center>'.$data1['S1_KULIAH'].'</center></td>
		<td><center>'.$data1['S2_KULIAH'].'</center></td>
		<td><center>'.$data1['S3_KULIAH'].'</center></td>
		<td><center>'.$data1['PRODI_LAIN_KULIAH'].'</center></td>
		<td><center>'.$data1['D3_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['S1_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['S2_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['S3_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['PRODI_LAIN_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['D3_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['S1_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['S2_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['S3_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['DOSEN_WALI'].'</center></td>
		<td><center>'.$data1['PENELITIAN'].'</center></td>
		<td><center>'.$data1['PENGMAS'].'</center></td>
		<td><center>'.$data1['MAN_ADM'].'</center></td>
		<td><center>'.$data1['TOTAL_BEBAN'].'</center></td>
	</tr>';
}
$tampil .= '
</table>
';
}
elseif($_SESSION['ID_FAK'] != ''){
$thn_akademik = (date("Y") - 2).'/'.(date("Y") - 1);
$tampil .= '
<table class="ui-widget">
	<tr style="background:navy;color:#fff;padding:5px;">
		<td colspan="20"><div class="center_title_bar">Rincian Beban Dosen Tetap</div></td>
	</tr>
	<tr class="ui-widget-header" style="padding:5px;">
		<td colspan="20"><div style="padding:10px;">'.$header.'</div></td>
	</tr>
</table>
<h2>SEMESTER GASAL '.$thn_akademik.'</h2>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<th rowspan="3" style="vertical-align: middle;"><center>Nama Dosen</center></th>
			<th colspan="18"><center>Beban Dosen (SKS) Pada Kegiatan</center></th>
			<th rowspan="3" style="vertical-align: middle;"><center>Total Beban</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th colspan="5"><center>Kuliah</center></th>
			<th colspan="5"><center>Praktikum</center></th>
			<th colspan="5"><center>Pembimbingan</center></th>
			<th rowspan="2"><center>Penelitian</center></th>
			<th rowspan="2"><center>Pengmas</center></th>
			<th rowspan="2"><center>Manajemen<br>&amp;<br>Administrasi</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th><center>D3</center></th>
			<th><center>S1</center></th>
			<th><center>S2</center></th>
			<th><center>S3</center></th>
			<th><center>Prodi Lain</center></th>
			<th><center>D3</center></th>
			<th><center>S1</center></th>
			<th><center>S2</center></th>
			<th><center>S3</center></th>
			<th><center>Prodi Lain</center></th>
			<th><center>D3</center></th>
			<th><center>S1</center></th>
			<th><center>S2</center></th>
			<th><center>S3</center></th>
			<th><center>Dosen Wali</center></th>
		</tr>
';
$dt1 = $db_wh->QueryToArray("
select * from WH_BEBAN_DOSEN where ID_FAKULTAS = '".$_SESSION['ID_FAK']."' 
and substr(TAHUN_SEMESTER,1,4) = extract(year from sysdate) - 1
and substr(TAHUN_SEMESTER,-1) = 1 and (STATUS_DOSEN <> 'LB' or STATUS_DOSEN <> '' or STATUS_DOSEN is null)
");
foreach($dt1 as $data1){
$tampil .= '
	<tr class="ui-widget-content">
		<td>'.$data1['NM_LENGKAP_PENGGUNA'].'</td>
		<td><center>'.$data1['D3_KULIAH'].'</center></td>
		<td><center>'.$data1['S1_KULIAH'].'</center></td>
		<td><center>'.$data1['S2_KULIAH'].'</center></td>
		<td><center>'.$data1['S3_KULIAH'].'</center></td>
		<td><center>'.$data1['PRODI_LAIN_KULIAH'].'</center></td>
		<td><center>'.$data1['D3_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['S1_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['S2_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['S3_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['PRODI_LAIN_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['D3_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['S1_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['S2_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['S3_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['DOSEN_WALI'].'</center></td>
		<td><center>'.$data1['PENELITIAN'].'</center></td>
		<td><center>'.$data1['PENGMAS'].'</center></td>
		<td><center>'.$data1['MAN_ADM'].'</center></td>
		<td><center>'.$data1['TOTAL_BEBAN'].'</center></td>
	</tr>';
}

$tampil .= '
</table>
<h2>SEMESTER GENAP '.$thn_akademik.'</h2>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<th rowspan="3" style="vertical-align: middle;"><center>Nama Dosen</center></th>
			<th colspan="18"><center>Beban Dosen (SKS) Pada Kegiatan</center></th>
			<th rowspan="3" style="vertical-align: middle;"><center>Total Beban</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th colspan="5"><center>Kuliah</center></th>
			<th colspan="5"><center>Praktikum</center></th>
			<th colspan="5"><center>Pembimbingan</center></th>
			<th rowspan="2"><center>Penelitian</center></th>
			<th rowspan="2"><center>Pengmas</center></th>
			<th rowspan="2"><center>Manajemen<br>&amp;<br>Administrasi</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th><center>D3</center></th>
			<th><center>S1</center></th>
			<th><center>S2</center></th>
			<th><center>S3</center></th>
			<th><center>Prodi Lain</center></th>
			<th><center>D3</center></th>
			<th><center>S1</center></th>
			<th><center>S2</center></th>
			<th><center>S3</center></th>
			<th><center>Prodi Lain</center></th>
			<th><center>D3</center></th>
			<th><center>S1</center></th>
			<th><center>S2</center></th>
			<th><center>S3</center></th>
			<th><center>Dosen Wali</center></th>
		</tr>
';
$dt1 = $db_wh->QueryToArray("
select * from WH_BEBAN_DOSEN where ID_FAKULTAS = '".$_SESSION['ID_FAK']."' 
and substr(TAHUN_SEMESTER,1,4) = extract(year from sysdate) - 1
and substr(TAHUN_SEMESTER,-1) = 2 and (STATUS_DOSEN <> 'LB' or STATUS_DOSEN <> '' or STATUS_DOSEN is null)
");
foreach($dt1 as $data1){
$tampil .= '
	<tr class="ui-widget-content">
		<td>'.$data1['NM_LENGKAP_PENGGUNA'].'</td>
		<td><center>'.$data1['D3_KULIAH'].'</center></td>
		<td><center>'.$data1['S1_KULIAH'].'</center></td>
		<td><center>'.$data1['S2_KULIAH'].'</center></td>
		<td><center>'.$data1['S3_KULIAH'].'</center></td>
		<td><center>'.$data1['PRODI_LAIN_KULIAH'].'</center></td>
		<td><center>'.$data1['D3_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['S1_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['S2_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['S3_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['PRODI_LAIN_PRAKTIKUM'].'</center></td>
		<td><center>'.$data1['D3_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['S1_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['S2_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['S3_BIMBINGAN'].'</center></td>
		<td><center>'.$data1['DOSEN_WALI'].'</center></td>
		<td><center>'.$data1['PENELITIAN'].'</center></td>
		<td><center>'.$data1['PENGMAS'].'</center></td>
		<td><center>'.$data1['MAN_ADM'].'</center></td>
		<td><center>'.$data1['TOTAL_BEBAN'].'</center></td>
	</tr>';
}
$tampil .= '
</table>
';
}else{
$tampil .= '
<table class="ui-widget">
	<tr style="background:navy;color:#fff;padding:5px;">
		<td colspan="7"><div class="center_title_bar">Rekapitulasi Beban Dosen</div></td>
	</tr>
	<tr class="ui-widget-header" style="padding:5px;">
		<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
	</tr>
</table>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<th rowspan="2" style="vertical-align: middle;"><center>Fakultas/Prodi</center></th>
			<th colspan="3"><center>Beban Kerja Rata-rata (SKS)</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th width="90"><center>'.(date("Y")-3).'</center></th>
			<th width="90"><center>'.(date("Y")-2).'</center></th>
			<th width="90"><center>'.(date("Y")-1).'</center></th>
		</tr>
';
$dt = $db_wh->QueryToArray("
select x.ID_FAKULTAS, x.ID_PROGRAM_STUDI, fak.SINGKATAN_FAKULTAS, jjg.NM_JENJANG, pro.NM_PROGRAM_STUDI, z.TOTAL as z, y.TOTAL as Y, x.TOTAL as X
from
(
(
select substr(TAHUN_SEMESTER,1,4) as TAHUN, ID_FAKULTAS, ID_PROGRAM_STUDI, round(avg(TOTAL_BEBAN),2) as total from WH_BEBAN_DOSEN
where substr(TAHUN_SEMESTER,1,4) = extract(year from sysdate) - 1
group by substr(TAHUN_SEMESTER,1,4), ID_FAKULTAS, ID_PROGRAM_STUDI
) x
left join
(
select substr(TAHUN_SEMESTER,1,4) as TAHUN, ID_FAKULTAS, ID_PROGRAM_STUDI, round(avg(TOTAL_BEBAN),2) as total from WH_BEBAN_DOSEN
where substr(TAHUN_SEMESTER,1,4) = extract(year from sysdate) - 2
group by substr(TAHUN_SEMESTER,1,4), ID_FAKULTAS, ID_PROGRAM_STUDI
) y on x.ID_FAKULTAS = y.ID_FAKULTAS and x.ID_PROGRAM_STUDI = y.ID_PROGRAM_STUDI
left join
(
select substr(TAHUN_SEMESTER,1,4) as TAHUN, ID_FAKULTAS, ID_PROGRAM_STUDI, round(avg(TOTAL_BEBAN),2) as total from WH_BEBAN_DOSEN
where substr(TAHUN_SEMESTER,1,4) = extract(year from sysdate) - 3
group by substr(TAHUN_SEMESTER,1,4), ID_FAKULTAS, ID_PROGRAM_STUDI
) z on x.ID_FAKULTAS = z.ID_FAKULTAS and x.ID_PROGRAM_STUDI = z.ID_PROGRAM_STUDI
)
left join fakultas fak on x.ID_FAKULTAS = fak.ID_FAKULTAS
left join program_studi pro on x.ID_PROGRAM_STUDI = pro.ID_PROGRAM_STUDI
left join jenjang jjg on jjg.ID_JENJANG = pro.ID_JENJANG
order by x.ID_FAKULTAS, x.ID_PROGRAM_STUDI
");
foreach($dt as $data){
$tampil .= '
	<tr class="ui-widget-content">
		<td>'.$data['SINGKATAN_FAKULTAS'].' / '.$data['NM_JENJANG'].'-'.$data['NM_PROGRAM_STUDI'].'</td>
		<td><center>'.$data['Z'].'</center></td>
		<td><center>'.$data['Y'].'</center></td>
		<td><center>'.$data['X'].'</center></td>
	</tr>';
}
$avg=$db_wh->QueryToArray("
select round(avg(z.TOTAL),2) as avg_z, round(avg(y.TOTAL),2) as avg_y, round(avg(x.TOTAL),2) as avg_x
FROM
(
(
select substr(TAHUN_SEMESTER,1,4) as TAHUN, ID_FAKULTAS, ID_PROGRAM_STUDI, round(avg(TOTAL_BEBAN),2) as total from WH_BEBAN_DOSEN
where substr(TAHUN_SEMESTER,1,4) = extract(year from sysdate) - 1
group by substr(TAHUN_SEMESTER,1,4), ID_FAKULTAS, ID_PROGRAM_STUDI
) x
left join
(
select substr(TAHUN_SEMESTER,1,4) as TAHUN, ID_FAKULTAS, ID_PROGRAM_STUDI, round(avg(TOTAL_BEBAN),2) as total from WH_BEBAN_DOSEN
where substr(TAHUN_SEMESTER,1,4) = extract(year from sysdate) - 2
group by substr(TAHUN_SEMESTER,1,4), ID_FAKULTAS, ID_PROGRAM_STUDI
) y on x.ID_FAKULTAS = y.ID_FAKULTAS and x.ID_PROGRAM_STUDI = y.ID_PROGRAM_STUDI
left join
(
select substr(TAHUN_SEMESTER,1,4) as TAHUN, ID_FAKULTAS, ID_PROGRAM_STUDI, round(avg(TOTAL_BEBAN),2) as total from WH_BEBAN_DOSEN
where substr(TAHUN_SEMESTER,1,4) = extract(year from sysdate) - 3
group by substr(TAHUN_SEMESTER,1,4), ID_FAKULTAS, ID_PROGRAM_STUDI
) z on x.ID_FAKULTAS = z.ID_FAKULTAS and x.ID_PROGRAM_STUDI = z.ID_PROGRAM_STUDI
)
order by x.ID_FAKULTAS, x.ID_PROGRAM_STUDI
");
foreach($avg as $data_avg){
$tampil .= '
	<tr class="ui-widget-header">
		<td style="text-align:right;">UNAIR</td>
		<td><center>'.$data_avg['AVG_Z'].'</center></td>
		<td><center>'.$data_avg['AVG_Y'].'</center></td>
		<td><center>'.$data_avg['AVG_X'].'</center></td>
	</tr>';
}
$tampil .= '
</table>
';
}

$smarty->assign('tampil', $tampil);
$smarty->display('se-sd-710.tpl');