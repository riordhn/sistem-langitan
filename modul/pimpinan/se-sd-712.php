<?php
include('config.php');

if($_SESSION['ID_PRODI'] != ''){
	$where = "AND z.ID_PROGRAM_STUDI = '".$_SESSION['ID_PRODI']."'";
	$header = $_SESSION['JENJANG'] . ' - '. $_SESSION['PRODI'];

}elseif($_SESSION['ID_FAK'] != ''){
	$where = "AND z.ID_FAKULTAS = '".$_SESSION['ID_FAK']."'";
	$header = 'FAKULTAS '.strtoupper($_SESSION['NM_FAK']);

}else{
	$where = "";
	$header = strtoupper('Universitas Airlangga');
}

if($_SESSION['ID_PRODI'] != ''){
$tampil .= '
<table class="ui-widget">
	<tr style="background:navy;color:#fff;padding:5px;">
		<td colspan="7"><div class="center_title_bar">Jumlah Tenaga Kependidikan Honorer </div></td>
	</tr>
	<tr class="ui-widget-header" style="padding:5px;">
		<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
	</tr>
</table>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<th rowspan="2" style="vertical-align: middle;"><center>Pendidikan Terakhir</center></th>
			<th colspan="5"><center>Kelompok Umur (tahun)</center></th>
			<th rowspan="2" style="vertical-align: middle;"><center>Jumlah</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th width="90"><center>&#60; 31</center></th>
			<th width="90"><center>31 &#8211; 40</center></th>
			<th width="90"><center>41 &#8211; 50</center></th>
			<th width="90"><center>51 &#8211; 55</center></th>
			<th width="90"><center>&#62; 55</center></th>
		</tr>
';
$dt = $db_wh->QueryToArray("
select z.PENDIDIKAN_TERAKHIR, z.URUT,
sum(y.A) as A,
sum(y.B) as B,
sum(y.C) as C,
sum(y.D) as D,
sum(y.E) as E,
sum(y.F) as F
from
(
(
select distinct PENDIDIKAN_TERAKHIR,
case when PENDIDIKAN_TERAKHIR = 'SD' then 'A'
when PENDIDIKAN_TERAKHIR = 'SMP' then 'B'
when PENDIDIKAN_TERAKHIR = 'SMA' then 'C'
else PENDIDIKAN_TERAKHIR end as URUT
from WH_PEGAWAI where PENDIDIKAN_TERAKHIR is not null order by URUT
) z
left join
(
select PENDIDIKAN_TERAKHIR,
case when USIA_PENGGUNA is null then 1 else 0 end as A,
case when USIA_PENGGUNA between 0 and 30 then 1 else 0 end as B,
case when USIA_PENGGUNA between 31 and 40 then 1 else 0 end as C,
case when USIA_PENGGUNA between 41 and 50 then 1 else 0 end as D,
case when USIA_PENGGUNA between 51 and 55 then 1 else 0 end as E,
case when USIA_PENGGUNA > 55 then 1 else 0 end as F
from WH_PEGAWAI where PENDIDIKAN_TERAKHIR is not null and USIA_PENGGUNA is not null and STATUS_PEGAWAI = 'KONTRAK'
and ID_PROGRAM_STUDI = '".$_SESSION['ID_PRODI']."'
) y on z.PENDIDIKAN_TERAKHIR = y.PENDIDIKAN_TERAKHIR
)
group by z.PENDIDIKAN_TERAKHIR, z.URUT
order by z.URUT
");
foreach($dt as $data){
$jumlah = $data['B'] + $data['C'] + $data['D'] + $data['E'] + $data['F'];
$tampil .= '
	<tr class="ui-widget-content">
		<td>'.$data['PENDIDIKAN_TERAKHIR'].'</td>
		<td><center>'.$data['B'].'</center></td>
		<td><center>'.$data['C'].'</center></td>
		<td><center>'.$data['D'].'</center></td>
		<td><center>'.$data['E'].'</center></td>
		<td><center>'.$data['F'].'</center></td>
		<td><center>'.$jumlah.'</center></td>
	</tr>';
$total_b += $data['B'];
$total_c += $data['C'];
$total_d += $data['D'];
$total_e += $data['E'];
$total_f += $data['F'];
$total += $jumlah;
}

$tampil .= '
	<tr class="ui-widget-header">
		<td style="text-align:right;">TOTAL UNAIR</td>
		<td><center>'.$total_b.'</center></td>
		<td><center>'.$total_c.'</center></td>
		<td><center>'.$total_d.'</center></td>
		<td><center>'.$total_e.'</center></td>
		<td><center>'.$total_f.'</center></td>
		<td><center>'.$total.'</center></td>
	</tr>
</table>
';
}
elseif($_SESSION['ID_FAK'] != ''){
$tampil .= '
<table class="ui-widget">
	<tr style="background:navy;color:#fff;padding:5px;">
		<td colspan="7"><div class="center_title_bar">Jumlah Tenaga Kependidikan Honorer </div></td>
	</tr>
	<tr class="ui-widget-header" style="padding:5px;">
		<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
	</tr>
</table>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<th rowspan="2" style="vertical-align: middle;"><center>Pendidikan Terakhir</center></th>
			<th colspan="5"><center>Kelompok Umur (tahun)</center></th>
			<th rowspan="2" style="vertical-align: middle;"><center>Jumlah</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th width="90"><center>&#60; 31</center></th>
			<th width="90"><center>31 &#8211; 40</center></th>
			<th width="90"><center>41 &#8211; 50</center></th>
			<th width="90"><center>51 &#8211; 55</center></th>
			<th width="90"><center>&#62; 55</center></th>
		</tr>
';
$dt = $db_wh->QueryToArray("
select z.PENDIDIKAN_TERAKHIR, z.URUT,
sum(y.A) as A,
sum(y.B) as B,
sum(y.C) as C,
sum(y.D) as D,
sum(y.E) as E,
sum(y.F) as F
from
(
(
select distinct PENDIDIKAN_TERAKHIR,
case when PENDIDIKAN_TERAKHIR = 'SD' then 'A'
when PENDIDIKAN_TERAKHIR = 'SMP' then 'B'
when PENDIDIKAN_TERAKHIR = 'SMA' then 'C'
else PENDIDIKAN_TERAKHIR end as URUT
from WH_PEGAWAI where PENDIDIKAN_TERAKHIR is not null order by URUT
) z
left join
(
select PENDIDIKAN_TERAKHIR,
case when USIA_PENGGUNA is null then 1 else 0 end as A,
case when USIA_PENGGUNA between 0 and 30 then 1 else 0 end as B,
case when USIA_PENGGUNA between 31 and 40 then 1 else 0 end as C,
case when USIA_PENGGUNA between 41 and 50 then 1 else 0 end as D,
case when USIA_PENGGUNA between 51 and 55 then 1 else 0 end as E,
case when USIA_PENGGUNA > 55 then 1 else 0 end as F
from WH_PEGAWAI where PENDIDIKAN_TERAKHIR is not null and USIA_PENGGUNA is not null and STATUS_PEGAWAI = 'KONTRAK'
and ID_FAKULTAS = '".$_SESSION['ID_FAK']."'
) y on z.PENDIDIKAN_TERAKHIR = y.PENDIDIKAN_TERAKHIR
)
group by z.PENDIDIKAN_TERAKHIR, z.URUT
order by z.URUT
");
foreach($dt as $data){
$jumlah = $data['B'] + $data['C'] + $data['D'] + $data['E'] + $data['F'];
$tampil .= '
	<tr class="ui-widget-content">
		<td>'.$data['PENDIDIKAN_TERAKHIR'].'</td>
		<td><center>'.$data['B'].'</center></td>
		<td><center>'.$data['C'].'</center></td>
		<td><center>'.$data['D'].'</center></td>
		<td><center>'.$data['E'].'</center></td>
		<td><center>'.$data['F'].'</center></td>
		<td><center>'.$jumlah.'</center></td>
	</tr>';
$total_b += $data['B'];
$total_c += $data['C'];
$total_d += $data['D'];
$total_e += $data['E'];
$total_f += $data['F'];
$total += $jumlah;
}

$tampil .= '
	<tr class="ui-widget-header">
		<td style="text-align:right;">TOTAL UNAIR</td>
		<td><center>'.$total_b.'</center></td>
		<td><center>'.$total_c.'</center></td>
		<td><center>'.$total_d.'</center></td>
		<td><center>'.$total_e.'</center></td>
		<td><center>'.$total_f.'</center></td>
		<td><center>'.$total.'</center></td>
	</tr>
</table>
';
}else{
$tampil .= '
<table class="ui-widget">
	<tr style="background:navy;color:#fff;padding:5px;">
		<td colspan="7"><div class="center_title_bar">Jumlah Tenaga Kependidikan Honorer </div></td>
	</tr>
	<tr class="ui-widget-header" style="padding:5px;">
		<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
	</tr>
</table>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<th rowspan="2" style="vertical-align: middle;"><center>Pendidikan Terakhir</center></th>
			<th colspan="5"><center>Kelompok Umur (tahun)</center></th>
			<th rowspan="2" style="vertical-align: middle;"><center>Jumlah</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th width="90"><center>&#60; 31</center></th>
			<th width="90"><center>31 &#8211; 40</center></th>
			<th width="90"><center>41 &#8211; 50</center></th>
			<th width="90"><center>51 &#8211; 55</center></th>
			<th width="90"><center>&#62; 55</center></th>
		</tr>
';
$dt = $db_wh->QueryToArray("
select z.PENDIDIKAN_TERAKHIR, z.URUT,
sum(y.A) as A,
sum(y.B) as B,
sum(y.C) as C,
sum(y.D) as D,
sum(y.E) as E,
sum(y.F) as F
from
(
(
select distinct PENDIDIKAN_TERAKHIR,
case when PENDIDIKAN_TERAKHIR = 'SD' then 'A'
when PENDIDIKAN_TERAKHIR = 'SMP' then 'B'
when PENDIDIKAN_TERAKHIR = 'SMA' then 'C'
else PENDIDIKAN_TERAKHIR end as URUT
from WH_PEGAWAI where PENDIDIKAN_TERAKHIR is not null order by URUT
) z
left join
(
select PENDIDIKAN_TERAKHIR,
case when USIA_PENGGUNA is null then 1 else 0 end as A,
case when USIA_PENGGUNA between 0 and 30 then 1 else 0 end as B,
case when USIA_PENGGUNA between 31 and 40 then 1 else 0 end as C,
case when USIA_PENGGUNA between 41 and 50 then 1 else 0 end as D,
case when USIA_PENGGUNA between 51 and 55 then 1 else 0 end as E,
case when USIA_PENGGUNA > 55 then 1 else 0 end as F
from WH_PEGAWAI where PENDIDIKAN_TERAKHIR is not null and USIA_PENGGUNA is not null and STATUS_PEGAWAI = 'KONTRAK'
) y on z.PENDIDIKAN_TERAKHIR = y.PENDIDIKAN_TERAKHIR
)
group by z.PENDIDIKAN_TERAKHIR, z.URUT
order by z.URUT
");
foreach($dt as $data){
$jumlah = $data['B'] + $data['C'] + $data['D'] + $data['E'] + $data['F'];
$tampil .= '
	<tr class="ui-widget-content">
		<td>'.$data['PENDIDIKAN_TERAKHIR'].'</td>
		<td><center>'.$data['B'].'</center></td>
		<td><center>'.$data['C'].'</center></td>
		<td><center>'.$data['D'].'</center></td>
		<td><center>'.$data['E'].'</center></td>
		<td><center>'.$data['F'].'</center></td>
		<td><center>'.$jumlah.'</center></td>
	</tr>';
$total_b += $data['B'];
$total_c += $data['C'];
$total_d += $data['D'];
$total_e += $data['E'];
$total_f += $data['F'];
$total += $jumlah;
}

$tampil .= '
	<tr class="ui-widget-header">
		<td style="text-align:right;">TOTAL UNAIR</td>
		<td><center>'.$total_b.'</center></td>
		<td><center>'.$total_c.'</center></td>
		<td><center>'.$total_d.'</center></td>
		<td><center>'.$total_e.'</center></td>
		<td><center>'.$total_f.'</center></td>
		<td><center>'.$total.'</center></td>
	</tr>
</table>
';
}

$smarty->assign('tampil', $tampil);
$smarty->display('se-sd-712.tpl');