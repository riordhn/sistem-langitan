SELECT THN_SEMESTER AS THN_AKADEMIK_SEMESTER, TAHUN_LULUS AS THN_AKADEMIK_LULUS, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, STATUS AS STATUS_MHS, KELAMIN_PENGGUNA AS JENIS_KELAMIN, 
IPK1_JML, IPK2_JML, IPK3_JML, IPK4_JML, IPK5_JML,
 ROUND((IPK1_JML / (IPK1_JML+IPK2_JML+IPK3_JML+IPK4_JML+IPK5_JML) * 100), 2) AS IPK1_PERSEN,
 ROUND((IPK2_JML / (IPK1_JML+IPK2_JML+IPK3_JML+IPK4_JML+IPK5_JML) * 100), 2) AS IPK2_PERSEN,
 ROUND((IPK3_JML / (IPK1_JML+IPK2_JML+IPK3_JML+IPK4_JML+IPK5_JML) * 100), 2) AS IPK3_PERSEN,
 ROUND((IPK4_JML / (IPK1_JML+IPK2_JML+IPK3_JML+IPK4_JML+IPK5_JML) * 100), 2) AS IPK4_PERSEN,
 ROUND((IPK5_JML / (IPK1_JML+IPK2_JML+IPK3_JML+IPK4_JML+IPK5_JML) * 100), 2) AS IPK5_PERSEN,
(IPK1_JML+IPK2_JML+IPK3_JML+IPK4_JML+IPK5_JML) AS TOTAL_KELAMIN, 
ROUND((RATA1_IPK+RATA2_IPK+RATA3_IPK+RATA4_IPK+RATA5_IPK)/ ((CASE WHEN IPK1_JML = 0 THEN 0 ELSE 1 END) + (CASE WHEN IPK2_JML = 0 THEN 0 ELSE 1 END) +
(CASE WHEN IPK3_JML = 0 THEN 0 ELSE 1 END) + (CASE WHEN IPK4_JML = 0 THEN 0 ELSE 1 END) + (CASE WHEN IPK5_JML = 0 THEN 0 ELSE 1 END)), 2) AS RATA,
SYSDATE AS TGL_UPDATE
FROM (
SELECT A.*, (CASE WHEN IPK1_JML IS NULL THEN 0 ELSE IPK1_JML END) AS IPK1_JML, (CASE WHEN IPK2_JML IS NULL THEN 0 ELSE IPK2_JML END) AS IPK2_JML, 
(CASE WHEN IPK3_JML IS NULL THEN 0 ELSE IPK3_JML END) AS IPK3_JML, (CASE WHEN IPK4_JML IS NULL THEN 0 ELSE IPK4_JML END) AS IPK4_JML, 
(CASE WHEN IPK5_JML IS NULL THEN 0 ELSE IPK5_JML END) AS IPK5_JML, (CASE WHEN ROUND(RATA1_IPK, 2) IS NULL THEN 0 ELSE ROUND(RATA1_IPK, 2) END) AS RATA1_IPK, 
(CASE WHEN ROUND(RATA2_IPK, 2) IS NULL THEN 0 ELSE ROUND(RATA2_IPK, 2) END) AS RATA2_IPK, (CASE WHEN ROUND(RATA3_IPK, 2) IS NULL THEN 0 ELSE ROUND(RATA3_IPK, 2) END) AS RATA3_IPK, 
(CASE WHEN ROUND(RATA4_IPK, 2) IS NULL THEN 0 ELSE ROUND(RATA4_IPK, 2) END) AS RATA4_IPK, (CASE WHEN ROUND(RATA5_IPK, 2) IS NULL THEN 0 ELSE ROUND(RATA5_IPK, 2) END) AS RATA5_IPK
FROM (
select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
from mahasiswa
				join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
				join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
				join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
				join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
				join (
					select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
					from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
					c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
					row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
					count(*) over(partition by c.nm_mata_kuliah) terulang
						from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
						where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
						and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
						and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
					) x
					where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
					group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 2
where tgl_lulus is not null
GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
) A 
LEFT JOIN (
select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK1_JML, AVG(IPK) AS RATA1_IPK
from mahasiswa
				join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
				join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
				join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
				join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
				join (
					select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
					from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
					c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
					row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
					count(*) over(partition by c.nm_mata_kuliah) terulang
						from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
						where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
						and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
						and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
					) x
					where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
					group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 2 and ipk < 2.5
where tgl_lulus is not null
GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
) B ON B.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND B.THN_SEMESTER = A.THN_SEMESTER AND  B.TAHUN_LULUS = A.TAHUN_LULUS AND  B.STATUS = A.STATUS AND B.KELAMIN_PENGGUNA = A.KELAMIN_PENGGUNA

LEFT JOIN (
select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK2_JML, AVG(IPK) AS RATA2_IPK
from mahasiswa
				join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
				join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
				join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
				join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
				join (
					select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
					from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
					c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
					row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
					count(*) over(partition by c.nm_mata_kuliah) terulang
						from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
						where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
						and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
						and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
					) x
					where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
					group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 2.5 and ipk < 2.75
where tgl_lulus is not null
GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
) C ON C.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND C.THN_SEMESTER = A.THN_SEMESTER AND  C.TAHUN_LULUS = A.TAHUN_LULUS AND C.STATUS = A.STATUS AND C.KELAMIN_PENGGUNA = A.KELAMIN_PENGGUNA

LEFT JOIN (
select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK3_JML, AVG(IPK) AS RATA3_IPK
from mahasiswa
				join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
				join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
				join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
				join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
				join (
					select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
					from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
					c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
					row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
					count(*) over(partition by c.nm_mata_kuliah) terulang
						from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
						where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
						and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
						and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
					) x
					where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
					group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 2.75 and ipk < 3
where tgl_lulus is not null
GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
) D ON D.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND D.THN_SEMESTER = A.THN_SEMESTER AND  D.TAHUN_LULUS = A.TAHUN_LULUS AND D.STATUS = A.STATUS AND D.KELAMIN_PENGGUNA = A.KELAMIN_PENGGUNA


LEFT JOIN (
select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK4_JML, AVG(IPK) AS RATA4_IPK
from mahasiswa
				join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
				join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
				join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
				join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
				join (
					select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
					from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
					c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
					row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
					count(*) over(partition by c.nm_mata_kuliah) terulang
						from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
						where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
						and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
						and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
					) x
					where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
					group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 3 and ipk < 3.5
where tgl_lulus is not null
GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
) E ON E.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND E.THN_SEMESTER = A.THN_SEMESTER AND E.TAHUN_LULUS = A.TAHUN_LULUS AND E.STATUS = A.STATUS AND E.KELAMIN_PENGGUNA = A.KELAMIN_PENGGUNA


LEFT JOIN (
select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as tahun_lulus,
PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, COUNT(*) AS IPK5_JML, AVG(IPK) AS RATA5_IPK
from mahasiswa
				join pengguna on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
				join admisi on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
				join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
				join semester on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
				join (
					select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
					from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
					c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
					row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
					count(*) over(partition by c.nm_mata_kuliah) terulang
						from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
						where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
						and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
						and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
					) x
					where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
					group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS and ipk >= 3.5
where tgl_lulus is not null
GROUP BY THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END), to_char(tgl_lulus, 'yyyy'),
PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA
) F ON F.ID_PROGRAM_STUDI = A.ID_PROGRAM_STUDI AND F.THN_SEMESTER = A.THN_SEMESTER AND F.TAHUN_LULUS = A.TAHUN_LULUS AND F.STATUS = A.STATUS AND F.KELAMIN_PENGGUNA = A.KELAMIN_PENGGUNA
) 

