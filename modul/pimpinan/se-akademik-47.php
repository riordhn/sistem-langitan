<?PHP
include('config.php');

$smarty->assign('tahun',$_SESSION['TAHUN']);

if($_SESSION['JENJANG'] == 'S1'){
	$judul = 'Skripsi';
	$program = 'Sarjana Reguler';
}elseif($_SESSION['JENJANG'] == 'S2'){
	$judul = 'Tesis';	
	$program = 'Magister';
}elseif($_SESSION['JENJANG'] == 'S3'){
	$judul = 'Desertasi';
	$program = 'Doktor';
}else{
	$judul = 'Tugas Akhir';
	$program = 'Diploma';
}
$smarty->assign('judul', $judul);
$smarty->assign('program', $program);


if ($_SESSION['STATUS']==1 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('fakultas', $_SESSION['NM_FAK']);

$skripsi_R = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
										sum(ttl_skripsi) as skripsi
										from wh_mhs_skripsi
										where  id_jenjang=1 and status='R'
										and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
										group by substr(tahun,1,4)
										order by tahun asc");
$smarty->assign('skripsi_R', $skripsi_R);
									
$skripsi_AJ = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
										sum(ttl_skripsi) as skripsi
										from wh_mhs_skripsi
										where  id_jenjang=1 and status='AJ'
										and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
										group by substr(tahun,1,4)
										order by tahun asc");
$smarty->assign('skripsi_AJ', $skripsi_AJ);

$skripsi_S2 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
										sum(ttl_skripsi) as skripsi
										from wh_mhs_skripsi
										where  id_jenjang=2
										and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
										group by substr(tahun,1,4)
										order by tahun asc");
$smarty->assign('skripsi_S2', $skripsi_S2);

$skripsi_S3 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
										sum(ttl_skripsi) as skripsi
										from wh_mhs_skripsi
										where  id_jenjang=3
										and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
										group by substr(tahun,1,4)
										order by tahun asc");
$smarty->assign('skripsi_S3', $skripsi_S3);

$skripsi_D3 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
										sum(ttl_skripsi) as skripsi
										from wh_mhs_skripsi
										where  id_jenjang=5
										and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
										group by substr(tahun,1,4)
										order by tahun asc");
$smarty->assign('skripsi_D3', $skripsi_D3);


$skripsi_PROFESI = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
										sum(ttl_skripsi) as skripsi
										from wh_mhs_skripsi
										where  id_jenjang=9
										and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
										group by substr(tahun,1,4)
										order by tahun asc");
$smarty->assign('skripsi_PROFESI', $skripsi_PROFESI);

$smarty->display("se-akademik-47-universitas.tpl");
}

if ($_SESSION['STATUS']==2 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('fakultas', $_SESSION['NM_FAK']);

$skripsi_R = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
										sum(ttl_skripsi) as skripsi
										from wh_mhs_skripsi
										where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and status='R'
										and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
										group by substr(tahun,1,4)
										order by tahun asc");
$smarty->assign('skripsi_R', $skripsi_R);
									
$skripsi_AJ = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
										sum(ttl_skripsi) as skripsi
										from wh_mhs_skripsi
										where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and status='AJ'
										and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
										group by substr(tahun,1,4)
										order by tahun asc");
$smarty->assign('skripsi_AJ', $skripsi_AJ);

$skripsi_S2 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
										sum(ttl_skripsi) as skripsi
										from wh_mhs_skripsi
										where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2
										and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
										group by substr(tahun,1,4)
										order by tahun asc");
$smarty->assign('skripsi_S2', $skripsi_S2);

$skripsi_S3 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
										sum(ttl_skripsi) as skripsi
										from wh_mhs_skripsi
										where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3
										and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
										group by substr(tahun,1,4)
										order by tahun asc");
$smarty->assign('skripsi_S3', $skripsi_S3);

$skripsi_D3 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
										sum(ttl_skripsi) as skripsi
										from wh_mhs_skripsi
										where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5
										and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
										group by substr(tahun,1,4)
										order by tahun asc");
$smarty->assign('skripsi_D3', $skripsi_D3);


$skripsi_PROFESI = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
										sum(ttl_skripsi) as skripsi
										from wh_mhs_skripsi
										where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9
										and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
										group by substr(tahun,1,4)
										order by tahun asc");
$smarty->assign('skripsi_PROFESI', $skripsi_PROFESI);

$smarty->display("se-akademik-47-fakultas.tpl");
}

if ($_SESSION['STATUS']==3 ) {
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('jenjang', $_SESSION['JENJANG']);
$smarty->assign('prodi', $_SESSION['PRODI']);

$skripsi_R = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
									sum(ttl_skripsi) as skripsi
									from wh_mhs_skripsi
									where id_program_studi='$_SESSION[ID_PRODI]' and status='R'
									and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
									group by substr(tahun,1,4)
									order by tahun asc");
$smarty->assign('skripsi_R', $skripsi_R);
									
$skripsi_AJ = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,
									sum(ttl_skripsi) as skripsi
									from wh_mhs_skripsi
									where id_program_studi='$_SESSION[ID_PRODI]' and status='AJ'
									and substr(tahun,5,1)='2' and substr(tahun,1,4) <='$_SESSION[TAHUN]' and substr(tahun,1,4) >= '$_SESSION[TAHUN]'-2
									group by substr(tahun,1,4)
									order by tahun asc");
$smarty->assign('skripsi_AJ', $skripsi_AJ);
$smarty->display("se-akademik-47-prodi.tpl");
}

?>