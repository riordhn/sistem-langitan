<?php
//Yudi Sulistya, 28-04-2012
include 'config.php';
require_once ('ociFunction.php');

$kd_fak=getData("select id_fakultas, nm_fakultas from fakultas order by id_fakultas");
$smarty->assign('KD_FAK', $kd_fak);

if ($request_method == 'GET' or $request_method == 'POST'){

$display = get('id','');

$fak=getData("select 'FAKULTAS '||upper(fak.nm_fakultas) as fakultas
from fakultas fak
where fak.id_fakultas=$display");
$smarty->assign('FAK', $fak);

$a = getData("
select * from 
(
(
select fsg.urut, fsg.jabatan, S1, S2, S3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as S3, sum(S2) as S2, sum(S1) as S1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) <= 30) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
)
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
)
union 
(
select urut, jabatan , sum(S1) as S1, sum(S2) as S2, sum(S3) as S3
from
(
select 5 as urut, '&Sigma;' as jabatan, 
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) <= 30) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
)
group by id_pengguna
)
)
)
)
order by urut
");
$smarty->assign('A', $a);

$b = getData("
select * from 
(
(
select fsg.urut, fsg.jabatan, S1, S2, S3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as S3, sum(S2) as S2, sum(S1) as S1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
)
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
)
union 
(
select urut, jabatan , sum(S1) as S1, sum(S2) as S2, sum(S3) as S3
from
(
select 5 as urut, '&Sigma;' as jabatan, 
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
)
group by id_pengguna
)
)
)
)
order by urut
");
$smarty->assign('B', $b);

$c = getData("
select * from 
(
(
select fsg.urut, fsg.jabatan, S1, S2, S3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as S3, sum(S2) as S2, sum(S1) as S1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
)
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
)
union 
(
select urut, jabatan , sum(S1) as S1, sum(S2) as S2, sum(S3) as S3
from
(
select 5 as urut, '&Sigma;' as jabatan, 
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
)
group by id_pengguna
)
)
)
)
order by urut
");
$smarty->assign('C', $c);

$d = getData("
select * from 
(
(
select fsg.urut, fsg.jabatan, S1, S2, S3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S1) as S1, sum(S2) as S2, sum(S3) as S3
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
)
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
)
union 
(
select urut, jabatan , sum(S3) as S3, sum(S2) as S2, sum(S1) as S1
from
(
select 5 as urut, '&Sigma;' as jabatan, 
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
)
group by id_pengguna
)
)
)
)
order by urut
");
$smarty->assign('D', $d);

$e = getData("
select * from 
(
(
select fsg.urut, fsg.jabatan, S1, S2, S3 from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as S3, sum(S2) as S2, sum(S1) as S1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
)
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
)
union 
(
select urut, jabatan, sum(S1) as S1, sum(S2) as S2, sum(S3) as S3
from
(
select 5 as urut, '&Sigma;' as jabatan, 
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
)
group by id_pengguna
)
)
)
)
order by urut
");
$smarty->assign('E', $e);

$jml=getData("
select * from 
(
(
select fsg.urut, fsg.jabatan, sum(S1+S2+S3) as jml from
(
select distinct
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 0 else b.id_jabatan_fungsional end as urut,
case when (b.id_jabatan_fungsional is null or b.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a left join dosen b on a.id_jabatan_fungsional=b.id_jabatan_fungsional
) fsg
left join
(
select jabatan, sum(S3) as S3, sum(S2) as S2, sum(S1) as S1
from
(
select case when (dsn.id_jabatan_fungsional is null or dsn.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else jab.nm_jabatan_fungsional end as jabatan,
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where pgg.tgl_lahir_pengguna is not null and 
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
)
group by id_pengguna
)
)
group by jabatan
) mat on fsg.jabatan=mat.jabatan
group by fsg.urut, fsg.jabatan
)
union 
(
select urut, jabatan, sum(S1+S2+S3) as jml from
(
select 5 as urut, '&Sigma;' as jabatan, sum(S3) as S3, sum(S2) as S2, sum(S1) as S1
from
(
select 
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where pgg.tgl_lahir_pengguna is not null and 
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
)
group by id_pengguna
)
)
)
group by urut, jabatan
)
)
order by urut
");
$smarty->assign('JML', $jml);

$ttl=getvar("
select sum(S1+S2+S3) as total from
(
select 
case when pda.nama_pendidikan_akhir='S3' then 1 else 0 end as S3,
case when pda.nama_pendidikan_akhir='S2' then 1 else 0 end as S2,
case when pda.nama_pendidikan_akhir='S1' then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where pgg.tgl_lahir_pengguna is not null and 
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and dsn.status_dosen in ('PNS', 'KONTRAK')
)
group by id_pengguna
)
)
");
$smarty->assign('TTL', $ttl['TOTAL']);

}

$smarty->display('se-sumberdaya-71a.tpl');
?>