<?PHP
include('config.php');

$smarty->assign('tahun',$_SESSION['TAHUN']);


if ($_SESSION['STATUS']==1 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);

$elpt_R = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 450 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml4,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml6,
								count(id_mhs) as jml10, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where  status='R' and id_jenjang=1
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]' and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs");
$smarty->assign('elpt_R', $elpt_R);

$elpt_AJ = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 450 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml4,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml6,
								count(id_mhs) as jml10, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where  status='AJ' and id_jenjang=1
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]' and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs");
$smarty->assign('elpt_AJ', $elpt_AJ);

$elpt_PROFESI = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 450 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml4,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml6,
								count(id_mhs) as jml10, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where  id_jenjang=9
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]' and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs
								");
$smarty->assign('elpt_PROFESI', $elpt_PROFESI);

$elpt_D3 = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 425 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 450 and elpt_score > 425 then 1 else 0 end) as jml4,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml6,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml8,
								count(id_mhs) as jml12 , round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where  id_jenjang=5
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]' and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs
								");
$smarty->assign('elpt_D3', $elpt_D3);

$elpt_S2 = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 450 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml4,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml6,
								count(id_mhs) as jml10, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where  id_jenjang=2
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]' and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs
								");
$smarty->assign('elpt_S2', $elpt_S2);

$elpt_S3 = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 450 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml4,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml6,
								count(id_mhs) as jml10, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where  id_jenjang=3
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]' and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs
								");
$smarty->assign('elpt_S3', $elpt_S3);

$smarty->display("se-akademik-43-universitas.tpl");
}

if ($_SESSION['STATUS']==2 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('fakultas', $_SESSION['NM_FAK']);

$elpt_R = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 450 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml4,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml6,
								count(id_mhs) as jml10, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where id_fakultas='$_SESSION[ID_FAK]' and status='R' and id_jenjang=1
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]' and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs");
$smarty->assign('elpt_R', $elpt_R);

$elpt_AJ = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 450 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml4,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml6,
								count(id_mhs) as jml10, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where id_fakultas='$_SESSION[ID_FAK]' and status='AJ' and id_jenjang=1
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]' and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs");
$smarty->assign('elpt_AJ', $elpt_AJ);

$elpt_PROFESI = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 450 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml4,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml6,
								count(id_mhs) as jml10, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]' and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs
								");
$smarty->assign('elpt_PROFESI', $elpt_PROFESI);

$elpt_D3 = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 425 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 450 and elpt_score > 425 then 1 else 0 end) as jml4,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml6,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml8,
								count(id_mhs) as jml12, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]' and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs
								");
$smarty->assign('elpt_D3', $elpt_D3);

$elpt_S2 = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 450 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml4,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml6,
								count(id_mhs) as jml10, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]' and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs
								");
$smarty->assign('elpt_S2', $elpt_S2);

$elpt_S3 = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 450 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml4,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml6,
								count(id_mhs) as jml10, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]' and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs
								");
$smarty->assign('elpt_S3', $elpt_S3);

$smarty->display("se-akademik-43-fakultas.tpl");
}

if ($_SESSION['STATUS']==3 ) {
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('jenjang', $_SESSION['JENJANG']);
$smarty->assign('prodi', $_SESSION['PRODI']);

if($_SESSION['JENJANG'] == 'D3'){
	
$elpt_d3 = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 425 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 450 and elpt_score > 425 then 1 else 0 end) as jml4,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml6,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml8,
								count(id_mhs) as jml12, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where id_program_studi='$_SESSION[ID_PRODI]' and status='R'
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]'  and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs");
$smarty->assign('elpt_d3', $elpt_d3);

}else{
$elpt_R = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 450 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml4,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml6,
								count(id_mhs) as jml10, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where id_program_studi='$_SESSION[ID_PRODI]' and status='R'
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]'  and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs");
$smarty->assign('elpt_R', $elpt_R);


$elpt_AJ = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as angkatan,
								sum(case when elpt_score <= 450 and elpt_score >= 0 then 1 else 0 end) as jml2,
								sum(case when elpt_score <= 500 and elpt_score > 450 then 1 else 0 end) as jml4,
								sum(case when elpt_score > 500 then 1 else 0 end) as jml6,
								count(id_mhs) as jml10, round(avg(elpt_score), 2) as rata_elpt
								from wh_elpt
								where id_program_studi='$_SESSION[ID_PRODI]' and status='AJ'
								and thn_angkatan_mhs <= '$_SESSION[TAHUN]'  and thn_angkatan_mhs >= '$_SESSION[TAHUN]'-2
								group by thn_angkatan_mhs
								order by thn_angkatan_mhs");
$smarty->assign('elpt_AJ', $elpt_AJ);
}


									
$smarty->display("se-akademik-43-prodi.tpl");
}

?>