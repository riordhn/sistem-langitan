<?php
//Yudi Sulistya, 28-04-2012
include 'config.php';
require_once ('ociFunction.php');

$kd_fak=getData("select id_fakultas, nm_fakultas from fakultas order by id_fakultas");
$smarty->assign('KD_FAK', $kd_fak);

if ($request_method == 'GET' or $request_method == 'POST'){

$display = get('id','');

$fak=getData("select 'FAKULTAS '||upper(fak.nm_fakultas) as fakultas
from fakultas fak
where fak.id_fakultas=$display");
$smarty->assign('FAK', $fak);

$data=getData("
select thn1.jabatan, jml5, jml4, jml3, jml2, jml1
from
(
select jab.id_jabatan_fungsional as urutan, jab.nm_jabatan_fungsional as jabatan, jml1
from jabatan_fungsional jab
left join 
(
select a.urut, count(a.urut) as jml1
from 
(
select distinct max(id_jabatan_fungsional) as urut, id_pengguna
from
( 
select sjf.id_pengguna, sjf.id_jabatan_fungsional
from sejarah_jabatan_fungsional sjf
where sjf.id_pengguna in
(
select distinct id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
and sjf.id_jabatan_fungsional<5 and sjf.tmt_sej_jab_fungsional is not null
and to_char(sjf.tmt_sej_jab_fungsional,'YYYY')<=to_char(sysdate,'YYYY')
)
group by id_pengguna
) a
group by a.urut
) b on jab.id_jabatan_fungsional=b.urut
where jab.id_jabatan_fungsional<5
) thn1 
left join
(
select jab.id_jabatan_fungsional as urutan, jab.nm_jabatan_fungsional as jabatan, jml2
from jabatan_fungsional jab
left join 
(
select a.urut, count(a.urut) as jml2
from 
(
select distinct max(id_jabatan_fungsional) as urut, id_pengguna
from
( 
select sjf.id_pengguna, sjf.id_jabatan_fungsional
from sejarah_jabatan_fungsional sjf
where sjf.id_pengguna in
(
select distinct id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
and sjf.id_jabatan_fungsional<5 and sjf.tmt_sej_jab_fungsional is not null
and to_char(sjf.tmt_sej_jab_fungsional,'YYYY')<=to_char(sysdate,'YYYY')-1
)
group by id_pengguna
) a
group by a.urut
) b on jab.id_jabatan_fungsional=b.urut
where jab.id_jabatan_fungsional<5
) thn2 on thn1.urutan=thn2.urutan
left join 
(
select jab.id_jabatan_fungsional as urutan, jab.nm_jabatan_fungsional as jabatan, jml3
from jabatan_fungsional jab
left join 
(
select a.urut, count(a.urut) as jml3
from 
(
select distinct max(id_jabatan_fungsional) as urut, id_pengguna
from
( 
select sjf.id_pengguna, sjf.id_jabatan_fungsional
from sejarah_jabatan_fungsional sjf
where sjf.id_pengguna in
(
select distinct id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
and sjf.id_jabatan_fungsional<5 and sjf.tmt_sej_jab_fungsional is not null
and to_char(sjf.tmt_sej_jab_fungsional,'YYYY')<=to_char(sysdate,'YYYY')-2
)
group by id_pengguna
) a
group by a.urut
) b on jab.id_jabatan_fungsional=b.urut
where jab.id_jabatan_fungsional<5
) thn3 on thn1.urutan=thn3.urutan
left join 
(
select jab.id_jabatan_fungsional as urutan, jab.nm_jabatan_fungsional as jabatan, jml4
from jabatan_fungsional jab
left join 
(
select a.urut, count(a.urut) as jml4
from 
(
select distinct max(id_jabatan_fungsional) as urut, id_pengguna
from
( 
select sjf.id_pengguna, sjf.id_jabatan_fungsional
from sejarah_jabatan_fungsional sjf
where sjf.id_pengguna in
(
select distinct id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
and sjf.id_jabatan_fungsional<5 and sjf.tmt_sej_jab_fungsional is not null
and to_char(sjf.tmt_sej_jab_fungsional,'YYYY')<=to_char(sysdate,'YYYY')-3
)
group by id_pengguna
) a
group by a.urut
) b on jab.id_jabatan_fungsional=b.urut
where jab.id_jabatan_fungsional<5
) thn4 on thn1.urutan=thn4.urutan
left join 
(
select jab.id_jabatan_fungsional as urutan, jab.nm_jabatan_fungsional as jabatan, jml5
from jabatan_fungsional jab
left join 
(
select a.urut, count(a.urut) as jml5
from 
(
select distinct max(id_jabatan_fungsional) as urut, id_pengguna
from
( 
select sjf.id_pengguna, sjf.id_jabatan_fungsional
from sejarah_jabatan_fungsional sjf
where sjf.id_pengguna in
(
select distinct id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
and sjf.id_jabatan_fungsional<5 and sjf.tmt_sej_jab_fungsional is not null
and to_char(sjf.tmt_sej_jab_fungsional,'YYYY')<=to_char(sysdate,'YYYY')-4
)
group by id_pengguna
) a
group by a.urut
) b on jab.id_jabatan_fungsional=b.urut
where jab.id_jabatan_fungsional<5
) thn5 on thn1.urutan=thn5.urutan
order by thn1.urutan
");

$smarty->assign('DATA', $data);
}

$smarty->display('se-sumberdaya-72.tpl');
?>