<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="4">
			<div class="center_title_bar">Rekapitulasi Kepuasan Pengalaman Belajar Mhs Tahun Pertama(Program D3)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="4"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table> 

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td><strong><center>Jenis Layanan</center></strong></td>
    <td colspan="3"><strong><center>Indeks Kepuasan (IK)</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td>&nbsp;</td>
    <td><strong><center>{$tahun-2}</center></strong></td>
    <td><strong><center>{$tahun-1}</center></strong></td>
    <td><strong><center>{$tahun}</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
  </tr>
  
  {$jml_eh_rata_a = 0}
  {$jml_eh_rata_b = 0}
  {$jml_eh_rata_c = 0}
  {$jml_row_eh_rata_a = 0}
  {$jml_row_eh_rata_b = 0}
  {$jml_row_eh_rata_c = 0}
 
  {foreach $ik412a_D3 as $data}
  <tr class="ui-widget-content">
    <td width="300">{$data.LAYANAN}</td>
    <td><center>{foreach $ik412_D3 as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun-2)}
					{$data1.IK}
					{$jml_eh_rata_a=$jml_eh_rata_a+ $data1.IK}
					{$jml_row_eh_rata_a = $jml_row_eh_rata_a + 1}
				{/if}
				{/foreach}
	</center></td>
    <td><center>{foreach $ik412_D3 as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun-1)}
					{$data1.IK}
					{$jml_eh_rata_b=$jml_eh_rata_b+ $data1.IK}
					{$jml_row_eh_rata_b = $jml_row_eh_rata_b + 1}
				{/if}
				{/foreach}
	</center></td>
    <td><center>{foreach $ik412_D3 as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun)}
					{$data1.IK}
					{$jml_eh_rata_c=$jml_eh_rata_c+ $data1.IK}
					{$jml_row_eh_rata_c = $jml_row_eh_rata_c + 1}
				{/if}
				{/foreach}
	</center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-header">
    <td><center>Rerata</center></td>
    <td><center>{round($jml_eh_rata_a/$jml_row_eh_rata_a,2)}</center></td>
    <td><center>{round($jml_eh_rata_b/$jml_row_eh_rata_b,2)}</center></td>
    <td><center>{round($jml_eh_rata_c/$jml_row_eh_rata_c, 2)}</center></td>
  </tr>
</table>


<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="4">
			<div class="center_title_bar">Rekapitulasi Kepuasan Pengalaman Belajar Mhs Tahun Pertama(Program Sarjana Reguler)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="4"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td><strong><center>Jenis Layanan</center></strong></td>
    <td colspan="3"><strong><center>Indeks Kepuasan (IK)</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td>&nbsp;</td>
    <td><strong><center>{$tahun-2}</center></strong></td>
    <td><strong><center>{$tahun-1}</center></strong></td>
    <td><strong><center>{$tahun}</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
  </tr>
  
  {$jml_eh_rata_a = 0}
  {$jml_eh_rata_b = 0}
  {$jml_eh_rata_c = 0}
  {$jml_row_eh_rata_a = 0}
  {$jml_row_eh_rata_b = 0}
  {$jml_row_eh_rata_c = 0}
 
  {foreach $ik412a_R as $data}
  <tr class="ui-widget-content">
    <td width="300">{$data.LAYANAN}</td>
    <td><center>{foreach $ik412_R as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun-2)}
					{$data1.IK}
					{$jml_eh_rata_a=$jml_eh_rata_a+ $data1.IK}
					{$jml_row_eh_rata_a = $jml_row_eh_rata_a + 1}
				{/if}
				{/foreach}
	</center></td>
    <td><center>{foreach $ik412_R as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun-1)}
					{$data1.IK}
					{$jml_eh_rata_b=$jml_eh_rata_b+ $data1.IK}
					{$jml_row_eh_rata_b = $jml_row_eh_rata_b + 1}
				{/if}
				{/foreach}
	</center></td>
    <td><center>{foreach $ik412_R as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun)}
					{$data1.IK}
					{$jml_eh_rata_c=$jml_eh_rata_c+ $data1.IK}
					{$jml_row_eh_rata_c = $jml_row_eh_rata_c + 1}
				{/if}
				{/foreach}
	</center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-header">
    <td><center>Rerata</center></td>
    <td><center>{round($jml_eh_rata_a/$jml_row_eh_rata_a,2)}</center></td>
    <td><center>{round($jml_eh_rata_b/$jml_row_eh_rata_b,2)}</center></td>
    <td><center>{round($jml_eh_rata_c/$jml_row_eh_rata_c, 2)}</center></td>
  </tr>
</table>


<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="4">
			<div class="center_title_bar">Rekapitulasi Kepuasan Pengalaman Belajar Mhs Tahun Pertama(Program PROFESI)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="4"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td><strong><center>Jenis Layanan</center></strong></td>
    <td colspan="3"><strong><center>Indeks Kepuasan (IK)</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td>&nbsp;</td>
    <td><strong><center>{$tahun-2}</center></strong></td>
    <td><strong><center>{$tahun-1}</center></strong></td>
    <td><strong><center>{$tahun}</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
  </tr>
  
  {$jml_eh_rata_a = 0}
  {$jml_eh_rata_b = 0}
  {$jml_eh_rata_c = 0}
  {$jml_row_eh_rata_a = 0}
  {$jml_row_eh_rata_b = 0}
  {$jml_row_eh_rata_c = 0}
 
  {foreach $ik412a_PROFESI as $data}
  <tr class="ui-widget-content">
    <td width="300">{$data.LAYANAN}</td>
    <td><center>{foreach $ik412_PROFESI as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun-2)}
					{$data1.IK}
					{$jml_eh_rata_a=$jml_eh_rata_a+ $data1.IK}
					{$jml_row_eh_rata_a = $jml_row_eh_rata_a + 1}
				{/if}
				{/foreach}
	</center></td>
    <td><center>{foreach $ik412_PROFESI as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun-1)}
					{$data1.IK}
					{$jml_eh_rata_b=$jml_eh_rata_b+ $data1.IK}
					{$jml_row_eh_rata_b = $jml_row_eh_rata_b + 1}
				{/if}
				{/foreach}
	</center></td>
    <td><center>{foreach $ik412_PROFESI as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun)}
					{$data1.IK}
					{$jml_eh_rata_c=$jml_eh_rata_c+ $data1.IK}
					{$jml_row_eh_rata_c = $jml_row_eh_rata_c + 1}
				{/if}
				{/foreach}
	</center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-header">
    <td><center>Rerata</center></td>
    <td><center>{round($jml_eh_rata_a/$jml_row_eh_rata_a,2)}</center></td>
    <td><center>{round($jml_eh_rata_b/$jml_row_eh_rata_b,2)}</center></td>
    <td><center>{round($jml_eh_rata_c/$jml_row_eh_rata_c, 2)}</center></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="4">
			<div class="center_title_bar">Rekapitulasi Kepuasan Pengalaman Belajar Mhs Tahun Pertama(Program S2)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="4"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">

    <td><strong><center>Jenis Layanan</center></strong></td>
    <td colspan="3"><strong><center>Indeks Kepuasan (IK)</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td>&nbsp;</td>
    <td><strong><center>{$tahun-2}</center></strong></td>
    <td><strong><center>{$tahun-1}</center></strong></td>
    <td><strong><center>{$tahun}</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
  </tr>
  
  {$jml_eh_rata_a = 0}
  {$jml_eh_rata_b = 0}
  {$jml_eh_rata_c = 0}
  {$jml_row_eh_rata_a = 0}
  {$jml_row_eh_rata_b = 0}
  {$jml_row_eh_rata_c = 0}
 
  {foreach $ik412a_S2 as $data}
  <tr class="ui-widget-content">
    <td width="300">{$data.LAYANAN}</td>
    <td><center>{foreach $ik412_S2 as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun-2)}
					{$data1.IK}
					{$jml_eh_rata_a=$jml_eh_rata_a+ $data1.IK}
					{$jml_row_eh_rata_a = $jml_row_eh_rata_a + 1}
				{/if}
				{/foreach}
	</center></td>
    <td><center>{foreach $ik412_S2 as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun-1)}
					{$data1.IK}
					{$jml_eh_rata_b=$jml_eh_rata_b+ $data1.IK}
					{$jml_row_eh_rata_b = $jml_row_eh_rata_b + 1}
				{/if}
				{/foreach}
	</center></td>
    <td><center>{foreach $ik412_S2 as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun)}
					{$data1.IK}
					{$jml_eh_rata_c=$jml_eh_rata_c+ $data1.IK}
					{$jml_row_eh_rata_c = $jml_row_eh_rata_c + 1}
				{/if}
				{/foreach}
	</center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-header">
    <td><center>Rerata</center></td>
    <td><center>{round($jml_eh_rata_a/$jml_row_eh_rata_a,2)}</center></td>
    <td><center>{round($jml_eh_rata_b/$jml_row_eh_rata_b,2)}</center></td>
    <td><center>{round($jml_eh_rata_c/$jml_row_eh_rata_c, 2)}</center></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="4">
			<div class="center_title_bar">Rekapitulasi Kepuasan Pengalaman Belajar Mhs Tahun Pertama(Program S3)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="4"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td><strong><center>Jenis Layanan</center></strong></td>
    <td colspan="3"><strong><center>Indeks Kepuasan (IK)</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td>&nbsp;</td>
    <td><strong><center>{$tahun-2}</center></strong></td>
    <td><strong><center>{$tahun-1}</center></strong></td>
    <td><strong><center>{$tahun}</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
  </tr>
  
  {$jml_eh_rata_a = 0}
  {$jml_eh_rata_b = 0}
  {$jml_eh_rata_c = 0}
  {$jml_row_eh_rata_a = 0}
  {$jml_row_eh_rata_b = 0}
  {$jml_row_eh_rata_c = 0}
 
  {foreach $ik412a_S3 as $data}
  <tr class="ui-widget-content">
    <td width="300">{$data.LAYANAN}</td>
    <td><center>{foreach $ik412_S3 as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun-2)}
					{$data1.IK}
					{$jml_eh_rata_a=$jml_eh_rata_a+ $data1.IK}
					{$jml_row_eh_rata_a = $jml_row_eh_rata_a + 1}
				{/if}
				{/foreach}
	</center></td>
    <td><center>{foreach $ik412_S3 as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun-1)}
					{$data1.IK}
					{$jml_eh_rata_b=$jml_eh_rata_b+ $data1.IK}
					{$jml_row_eh_rata_b = $jml_row_eh_rata_b + 1}
				{/if}
				{/foreach}
	</center></td>
    <td><center>{foreach $ik412_S3 as $data1}
				{if ($data.LAYANAN==$data1.LAYANAN) and ($data1.TAHUN==$tahun)}
					{$data1.IK}
					{$jml_eh_rata_c=$jml_eh_rata_c+ $data1.IK}
					{$jml_row_eh_rata_c = $jml_row_eh_rata_c + 1}
				{/if}
				{/foreach}
	</center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-header">
    <td><center>Rerata</center></td>
    <td><center>{round($jml_eh_rata_a/$jml_row_eh_rata_a,2)}</center></td>
    <td><center>{round($jml_eh_rata_b/$jml_row_eh_rata_b,2)}</center></td>
    <td><center>{round($jml_eh_rata_c/$jml_row_eh_rata_c, 2)}</center></td>
  </tr>
</table>
