{* Yudi Sulistya, 29-04-2012 *}
<div class="center_title_bar">Profil Dosen Prodi yang berasal dari Departemen/Fakultas berdasarkan Umur, Tingkat Pendidikan dan Jabatan Fungsional{if $smarty.get.id == '%'}<li>UNIVERSITAS AIRLANGGA</li>{else}{foreach item="list" from=$FAK}<li>{$list.FAKULTAS}</li>{/foreach}{/if}</div>
<form action="se-sumberdaya-71a.php" method="get">
<p>Pilih Fakultas : 
	<select name="id" id="id">
	<option value="">----------</option>
	<option value='%' style="font-weight: bold; color: #fff; background-color: #013e68;">LIHAT SEMUA</option>
	{foreach item="fak" from=$KD_FAK}
	{html_options values=$fak.ID_FAKULTAS output=$fak.NM_FAKULTAS}
	{/foreach}
	</select>
	<input type="submit" name="View" value="View">
</p>
</form>
{if $smarty.get.id == ''}
{else}
	<table width="850" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th rowspan="3" width="90" style="vertical-align: middle;"><center>Jabatan<br>Fungsional</center></th>
			<th colspan="15" width="720"><center>Kelompok Umur (tahun)</center></th>
			<th colspan="2" width="40"><center>Jumlah</center></th>
		</tr>
		<tr>
			<th width="90" colspan="3"><center>&#60; 31</center></th>
			<th width="90" colspan="3"><center>31 &#8211; 40</center></th>
			<th width="90" colspan="3"><center>41 &#8211; 50</center></th>
			<th width="90" colspan="3"><center>51 &#8211; 60</center></th>
			<th width="90" colspan="3"><center>&#62; 60</center></th>
			<th width="90" rowspan="2" style="vertical-align: middle;"><center>&Sigma;</center></th>
			<th width="90" rowspan="2" style="vertical-align: middle;"><center>&#37;</center></th>
		</tr>
		<tr>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
		</tr>
		</thead>
		<tbody>
		<tr>
		<td colspan="4">
		<table>
			{foreach item="a" from=$A}
		<tr>
			<td width="90" height="90" style="vertical-align: middle;"><center>{$a.JABATAN}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $a.S1 < 1}-{else}{$a.S1}{/if}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $a.S2 < 1}-{else}{$a.S2}{/if}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $a.S3 < 1}-{else}{$a.S3}{/if}</center></td>
		</tr>
			{/foreach}
		<tr>
			<td width="30" height="90" style="vertical-align: middle;"><center>&#37;</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$a.S1 y=$TTL format="%.2f"}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$a.S2 y=$TTL format="%.2f"}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$a.S3 y=$TTL format="%.2f"}</center></td>
		</tr>
		</table>
		</td>
		<td colspan="3">
		<table>
			{foreach item="b" from=$B}
		<tr>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $b.S1 < 1}-{else}{$b.S1}{/if}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $b.S2 < 1}-{else}{$b.S2}{/if}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $b.S3 < 1}-{else}{$b.S3}{/if}</center></td>
		</tr>
			{/foreach}
		<tr>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$b.S1 y=$TTL format="%.2f"}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$b.S2 y=$TTL format="%.2f"}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$b.S3 y=$TTL format="%.2f"}</center></td>
		</tr>
		</table>
		</td>
		<td colspan="3">
		<table>
			{foreach item="c" from=$C}
		<tr>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $c.S1 < 1}-{else}{$c.S1}{/if}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $c.S2 < 1}-{else}{$c.S2}{/if}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $c.S3 < 1}-{else}{$c.S3}{/if}</center></td>
		</tr>
			{/foreach}
		<tr>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$c.S1 y=$TTL format="%.2f"}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$c.S2 y=$TTL format="%.2f"}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$c.S3 y=$TTL format="%.2f"}</center></td>
		</tr>
		</table>
		</td>
		<td colspan="3">
		<table>
			{foreach item="d" from=$D}
		<tr>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $d.S1 < 1}-{else}{$d.S1}{/if}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $d.S2 < 1}-{else}{$d.S2}{/if}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $d.S3 < 1}-{else}{$d.S3}{/if}</center></td>
		</tr>
			{/foreach}
		<tr>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$d.S1 y=$TTL format="%.2f"}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$d.S2 y=$TTL format="%.2f"}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$d.S3 y=$TTL format="%.2f"}</center></td>
		</tr>
		</table>
		</td>
		<td colspan="3">
		<table>
			{foreach item="e" from=$E}
		<tr>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $e.S1 < 1}-{else}{$e.S1}{/if}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $e.S2 < 1}-{else}{$e.S2}{/if}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{if $e.S3 < 1}-{else}{$e.S3}{/if}</center></td>
		</tr>
			{/foreach}
		<tr>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$e.S1 y=$TTL format="%.2f"}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$e.S2 y=$TTL format="%.2f"}</center></td>
			<td width="30" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$e.S3 y=$TTL format="%.2f"}</center></td>
		</tr>
		</table>
		</td>
		<td colspan="2" width="180">
		<table>
			{foreach item="jml" from=$JML}
		<tr>
			<td width="90" height="90" style="vertical-align: middle;"><center>{$jml.JML}</center></td>
			{if $jml.JABATAN == "&Sigma;"}
			<td bgcolor="#013e68" width="90" height="90" style="vertical-align: middle;"><center></center></td>
			{else}
			<td width="90" height="90" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$jml.JML y=$TTL format="%.2f"}</center></td>
			{/if}
		</tr>
			{/foreach}
		<tr>
			<td bgcolor="#013e68" width="90" height="90" style="vertical-align: middle;"><center></center></td>
			<td width="90" height="90" style="vertical-align: middle;"><center>100</center></td>
		</tr>
		</table>
		</td>
		</tr>
		</tbody>
	</table>
{/if}