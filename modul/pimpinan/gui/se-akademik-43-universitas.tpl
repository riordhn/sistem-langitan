<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Nilai English Language Proficiency Test(ELPT) Mahasiswa Baru (Program D3)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>X &lt;= 425</center></td>
        <td colspan="2"><center>425 &lt; IPK &lt;= 450</center></td>
        <td colspan="2"><center>450 &lt; IPK &lt;= 500</center></td>
        <td colspan="2"><center>X &gt; 500</center></td>
        <td colspan="2"><center>Sampel</center></td>
        <td rowspan="2"><center>Jumlah Per Angkatan</center></td>
        <td rowspan="2"><center>Rerata</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_8 = 0}
    {$jml_10 = 0}
    {$jml_12 = 0}
    {$jml_101 = 0}

    {$rata_2 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
    {$rata_12 = 0}
    {$row = 0}
    {foreach $elpt_d3 as $data}
		
		{$jml_2 = $jml_2 + $data.JML2 }
		{$jml_4 = $jml_4 + $data.JML4 }
		{$jml_6 = $jml_6 + $data.JML6 }
        {$jml_8 = $jml_8 + $data.JML8 }
        {$jml_10 = $data.JML2 + $data.JML4 + $data.JML6 + $data.JML8}
        {$jml_12 = $jml_12 + $data.JML12}
        {$jml_101 = $jml_101 + $jml_10}
        
        <tr>
            <td><center>{$data.ANGKATAN}</center></td>
            <td><center>{$data.JML2}</center></td>
            <td><center>{($data.JML2/$jml_10*100)|round:2}</center></td>
            <td><center>{$data.JML4}</center></td>
            <td><center>{($data.JML4/$jml_10*100)|round:2}</center></td>
            <td><center>{$data.JML6}</center></td>
            <td><center>{($data.JML6/$jml_12*100)|round:2}</center></td>
            <td><center>{$data.JML8}</center></td>
            <td><center>{($jml_8/$jml_10*100)|round:2}</center></td>
            <td><center>{$jml_10}</center></td> 
            <td><center>{($jml_10/$data.JML12*100)|round:2}</center></td>
            <td><center>{$data.JML12}</center></td>            
            <td><center></center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_101}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>


	</tr>
    
    {$jml_total = $jml_2 + $jml_4 + $jml_6 + $jml_8}
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_2/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_4/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_6/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_8/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_101/$jml_total)*100)|round:2}</center></td>
      <td bgcolor="#333333"><center></center></td>
      <td><center></center></td>
	</tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Nilai English Language Proficiency Test(ELPT) Mahasiswa Baru (Program Reguler)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>X &lt;= 450</center></td>
        <td colspan="2"><center>450 &lt; IPK &lt;= 500</center></td>
        <td colspan="2"><center>X &gt; 500</center></td>
        <td colspan="2"><center>Sampel</center></td>
        <td rowspan="2"><center>Jumlah Per Angkatan</center></td>
        <td rowspan="2"><center>Rerata</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_81 = 0}
    {$jml_10 = 0}

    {$rata_2 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
    {$row = 0}
    {foreach $elpt_R as $data}
		{$jml_8 = 0}
		{$jml_8 = $data.JML2 + $data.JML4 + $data.JML6}
		{$jml_81 = $jml_81 + $jml_8 }
		{$jml_2 = $jml_2 + $data.JML2 }
		{$jml_4 = $jml_4 + $data.JML4 }
		{$jml_6 = $jml_6 + $data.JML6 }
		{$jml_10 = $jml_10 + $data.JML10 }
        <tr>
            <td><center>{$data.ANGKATAN}</center></td>
            <td><center>{$data.JML2}</center></td>
            <td><center>{($data.JML2/$jml_8*100)|round:2}</center></td>
            <td><center>{$data.JML4}</center></td>
            <td><center>{($data.JML4/$jml_8*100)|round:2}</center></td>
            <td><center>{$data.JML6}</center></td>
            <td><center>{($data.JML6/$jml_8*100)|round:2}</center></td>
            <td><center>{$jml_8}</center></td>
            <td><center>{($jml_8/$data.JML10*100)|round:2}</center></td>
            <td><center>{$data.JML10}</center></td>
            <td><center></center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_81}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>


	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_2/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_4/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_6/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_8/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_10/$jml_total)*100)|round:2}</center></td>
	</tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Nilai English Language Proficiency Test(ELPT) Mahasiswa Baru (Program Alih Jenis)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>X &lt;= 450</center></td>
        <td colspan="2"><center>450 &lt; IPK &lt;= 500</center></td>
        <td colspan="2"><center>X &gt; 500</center></td>
        <td colspan="2"><center>Sampel</center></td>
        <td rowspan="2"><center>Jumlah Per Angkatan</center></td>
        <td rowspan="2"><center>Rerata</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_81 = 0}
    {$jml_10 = 0}

    {$rata_2 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
    {$row = 0}
    {foreach $elpt_AJ as $data}
		{$jml_8 = 0}
		{$jml_8 = $data.JML2 + $data.JML4 + $data.JML6}
		{$jml_81 = $jml_81 + $jml_8 }
		{$jml_2 = $jml_2 + $data.JML2 }
		{$jml_4 = $jml_4 + $data.JML4 }
		{$jml_6 = $jml_6 + $data.JML6 }
		{$jml_10 = $jml_10 + $data.JML10 }
        <tr>
            <td><center>{$data.ANGKATAN}</center></td>
            <td><center>{$data.JML2}</center></td>
            <td><center>{($data.JML2/$jml_8*100)|round:2}</center></td>
            <td><center>{$data.JML4}</center></td>
            <td><center>{($data.JML4/$jml_8*100)|round:2}</center></td>
            <td><center>{$data.JML6}</center></td>
            <td><center>{($data.JML6/$jml_8*100)|round:2}</center></td>
            <td><center>{$jml_8}</center></td>
            <td><center>{($jml_8/$data.JML10*100)|round:2}</center></td>
            <td><center>{$data.JML10}</center></td>
            <td><center></center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_81}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>


	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_2/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_4/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_6/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_8/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_10/$jml_total)*100)|round:2}</center></td>
	</tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Nilai English Language Proficiency Test(ELPT) Mahasiswa Baru (Program PROFESI)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>X &lt;= 450</center></td>
        <td colspan="2"><center>450 &lt; IPK &lt;= 500</center></td>
        <td colspan="2"><center>X &gt; 500</center></td>
        <td colspan="2"><center>Sampel</center></td>
        <td rowspan="2"><center>Jumlah Per Angkatan</center></td>
        <td rowspan="2"><center>Rerata</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_81 = 0}
    {$jml_10 = 0}

    {$rata_2 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
    {$row = 0}
    {foreach $elpt_PROFESI as $data}
		{$jml_8 = 0}
		{$jml_8 = $data.JML2 + $data.JML4 + $data.JML6}
		{$jml_81 = $jml_81 + $jml_8 }
		{$jml_2 = $jml_2 + $data.JML2 }
		{$jml_4 = $jml_4 + $data.JML4 }
		{$jml_6 = $jml_6 + $data.JML6 }
		{$jml_10 = $jml_10 + $data.JML10 }
        <tr>
            <td><center>{$data.ANGKATAN}</center></td>
            <td><center>{$data.JML2}</center></td>
            <td><center>{($data.JML2/$jml_8*100)|round:2}</center></td>
            <td><center>{$data.JML4}</center></td>
            <td><center>{($data.JML4/$jml_8*100)|round:2}</center></td>
            <td><center>{$data.JML6}</center></td>
            <td><center>{($data.JML6/$jml_8*100)|round:2}</center></td>
            <td><center>{$jml_8}</center></td>
            <td><center>{($jml_8/$data.JML10*100)|round:2}</center></td>
            <td><center>{$data.JML10}</center></td>
            <td><center></center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_81}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>


	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_2/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_4/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_6/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_8/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_10/$jml_total)*100)|round:2}</center></td>
	</tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Nilai English Language Proficiency Test(ELPT) Mahasiswa Baru (Program S2)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>X &lt;= 450</center></td>
        <td colspan="2"><center>450 &lt; IPK &lt;= 500</center></td>
        <td colspan="2"><center>X &gt; 500</center></td>
        <td colspan="2"><center>Sampel</center></td>
        <td rowspan="2"><center>Jumlah Per Angkatan</center></td>
        <td rowspan="2"><center>Rerata</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_81 = 0}
    {$jml_10 = 0}

    {$rata_2 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
    {$row = 0}
    {foreach $elpt_S2 as $data}
		{$jml_8 = 0}
		{$jml_8 = $data.JML2 + $data.JML4 + $data.JML6}
		{$jml_81 = $jml_81 + $jml_8 }
		{$jml_2 = $jml_2 + $data.JML2 }
		{$jml_4 = $jml_4 + $data.JML4 }
		{$jml_6 = $jml_6 + $data.JML6 }
		{$jml_10 = $jml_10 + $data.JML10 }
        <tr>
            <td><center>{$data.ANGKATAN}</center></td>
            <td><center>{$data.JML2}</center></td>
            <td><center>{($data.JML2/$jml_8*100)|round:2}</center></td>
            <td><center>{$data.JML4}</center></td>
            <td><center>{($data.JML4/$jml_8*100)|round:2}</center></td>
            <td><center>{$data.JML6}</center></td>
            <td><center>{($data.JML6/$jml_8*100)|round:2}</center></td>
            <td><center>{$jml_8}</center></td>
            <td><center>{($jml_8/$data.JML10*100)|round:2}</center></td>
            <td><center>{$data.JML10}</center></td>
            <td><center></center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_81}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>


	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_2/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_4/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_6/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_8/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_10/$jml_total)*100)|round:2}</center></td>
	</tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Nilai English Language Proficiency Test(ELPT) Mahasiswa Baru (Program S3)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>X &lt;= 450</center></td>
        <td colspan="2"><center>450 &lt; IPK &lt;= 500</center></td>
        <td colspan="2"><center>X &gt; 500</center></td>
        <td colspan="2"><center>Sampel</center></td>
        <td rowspan="2"><center>Jumlah Per Angkatan</center></td>
        <td rowspan="2"><center>Rerata</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_81 = 0}
    {$jml_10 = 0}

    {$rata_2 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
    {$row = 0}
    {foreach $elpt_S3 as $data}
		{$jml_8 = 0}
		{$jml_8 = $data.JML2 + $data.JML4 + $data.JML6}
		{$jml_81 = $jml_81 + $jml_8 }
		{$jml_2 = $jml_2 + $data.JML2 }
		{$jml_4 = $jml_4 + $data.JML4 }
		{$jml_6 = $jml_6 + $data.JML6 }
		{$jml_10 = $jml_10 + $data.JML10 }
        <tr>
            <td><center>{$data.ANGKATAN}</center></td>
            <td><center>{$data.JML2}</center></td>
            <td><center>{($data.JML2/$jml_8*100)|round:2}</center></td>
            <td><center>{$data.JML4}</center></td>
            <td><center>{($data.JML4/$jml_8*100)|round:2}</center></td>
            <td><center>{$data.JML6}</center></td>
            <td><center>{($data.JML6/$jml_8*100)|round:2}</center></td>
            <td><center>{$jml_8}</center></td>
            <td><center>{($jml_8/$data.JML10*100)|round:2}</center></td>
            <td><center>{$data.JML10}</center></td>
            <td><center></center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_81}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>


	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_2/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_4/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_6/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_8/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_10/$jml_total)*100)|round:2}</center></td>
	</tr>
</table>


