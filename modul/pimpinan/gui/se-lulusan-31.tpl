{if $jenjang == 'S1'}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan IPK (Program Sarjana)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Akademik Lulus</center></td>
        <td colspan="2"><center>2&lt;=IPK&lt;2,5</center></td>
        <td colspan="2"><center>2,5&lt;=IPK&lt;2,75</center></td>
        <td colspan="2"><center>2,75&lt;=IPK&lt;3,0</center></td>
        <td colspan="2"><center>3,0&lt;=IPK&lt;3,5</center></td>
        <td colspan="2"><center>IPK>=3,5</center></td>
        <td rowspan="2"><center>Total Lulusan</center></td>
        <td rowspan="2"><center>Rerata IPK</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
	{$rata_ipk = 0}
    {foreach $lulusan_r as $data}
    	{$jml = $data.IPK1_JML + $data.IPK2_JML + $data.IPK3_JML + $data.IPK4_JML + $data.IPK5_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.IPK1_JML}
        {$jml_6 = $jml_6 + $data.IPK2_JML}
        {$jml_8 = $jml_8 + $data.IPK3_JML}
        {$jml_10 = $jml_10 + $data.IPK4_JML}
		{$jml_12 = $jml_12 + $data.IPK5_JML}
        {$rata_2 = $rata_2 + ($data.IPK1_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_6 = $rata_6 + ($data.IPK2_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_8 = $rata_8 + ($data.IPK3_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_10 = $rata_10 + ($data.IPK4_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_12 = $rata_12 + ($data.IPK5_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_ipk = $rata_ipk + $data.RATA_IPK}
        {$row = $row+1}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{(int)$data.IPK1_JML}</center></td>
            <td><center>{($data.IPK1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK2_JML}</center></td>
            <td><center>{($data.IPK2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK3_JML}</center></td>
            <td><center>{($data.IPK3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK4_JML}</center></td>
            <td><center>{($data.IPK4_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK5_JML}</center></td>
            <td><center>{($data.IPK5_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.TOTAL_LULUSAN}</center></td>
            <td>
			<center>
				{$data.RATA_IPK|round:2}
			</center>
			</td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_10/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_12/$row)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_ipk/$row)|round:2}</center></td>
	</tr>
</table>
<br />
<br />

{if count($lulusan_aj) > 0}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan IPK (Program Alih Jenis dari D3 ke S1)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Akademik Lulus</center></td>
        <td colspan="2"><center>2&lt;=IPK&lt;2,5</center></td>
        <td colspan="2"><center>2,5&lt;=IPK&lt;2,75</center></td>
        <td colspan="2"><center>2,75&lt;=IPK&lt;3,0</center></td>
        <td colspan="2"><center>3,0&lt;=IPK&lt;3,5</center></td>
        <td colspan="2"><center>IPK>=3,5</center></td>
        <td rowspan="2"><center>Total Lulusan</center></td>
        <td rowspan="2"><center>Rerata IPK</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
	{$rata_ipk = 0}
    {foreach $lulusan_aj as $data}
    	{$jml = $data.IPK1_JML + $data.IPK2_JML + $data.IPK3_JML + $data.IPK4_JML + $data.IPK5_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.IPK1_JML}
        {$jml_6 = $jml_6 + $data.IPK2_JML}
        {$jml_8 = $jml_8 + $data.IPK3_JML}
        {$jml_10 = $jml_10 + $data.IPK4_JML}
		{$jml_12 = $jml_12 + $data.IPK5_JML}
       {$rata_2 = $rata_2 + ($data.IPK1_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_6 = $rata_6 + ($data.IPK2_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_8 = $rata_8 + ($data.IPK3_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_10 = $rata_10 + ($data.IPK4_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_12 = $rata_12 + ($data.IPK5_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_ipk = $rata_ipk + $data.RATA_IPK}
        {$row = $row+1}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{(int)$data.IPK1_JML}</center></td>
            <td><center>{($data.IPK1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK2_JML}</center></td>
            <td><center>{($data.IPK2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK3_JML}</center></td>
            <td><center>{($data.IPK3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK4_JML}</center></td>
            <td><center>{($data.IPK4_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK5_JML}</center></td>
            <td><center>{($data.IPK5_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.TOTAL_LULUSAN}</center></td>
            <td>
			<center>
				{$data.RATA_IPK|round:2}
			</center>
			</td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_10/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_12/$row)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_ipk/$row)|round:2}</center></td>
	</tr>
</table>
{/if}
<br />
<br />
{/if}


{if $jenjang == 'D3'}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan IPK (Program D3)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Akademik Lulus</center></td>
        <td colspan="2"><center>2&lt;=IPK&lt;2,5</center></td>
        <td colspan="2"><center>2,5&lt;=IPK&lt;2,75</center></td>
        <td colspan="2"><center>2,75&lt;=IPK&lt;3,0</center></td>
        <td colspan="2"><center>3,0&lt;=IPK&lt;3,5</center></td>
        <td colspan="2"><center>IPK>=3,5</center></td>
        <td rowspan="2"><center>Total Lulusan</center></td>
        <td rowspan="2"><center>Rerata IPK</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
	{$rata_ipk = 0}
    {foreach $lulusan_d3 as $data}
    	{$jml = $data.IPK1_JML + $data.IPK2_JML + $data.IPK3_JML + $data.IPK4_JML + $data.IPK5_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.IPK1_JML}
        {$jml_6 = $jml_6 + $data.IPK2_JML}
        {$jml_8 = $jml_8 + $data.IPK3_JML}
        {$jml_10 = $jml_10 + $data.IPK4_JML}
		{$jml_12 = $jml_12 + $data.IPK5_JML}
        {$rata_2 = $rata_2 + ($data.IPK1_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_6 = $rata_6 + ($data.IPK2_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_8 = $rata_8 + ($data.IPK3_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_10 = $rata_10 + ($data.IPK4_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_12 = $rata_12 + ($data.IPK5_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_ipk = $rata_ipk + $data.RATA_IPK}
        {$row = $row+1}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{(int)$data.IPK1_JML}</center></td>
            <td><center>{($data.IPK1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK2_JML}</center></td>
            <td><center>{($data.IPK2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK3_JML}</center></td>
            <td><center>{($data.IPK3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK4_JML}</center></td>
            <td><center>{($data.IPK4_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK5_JML}</center></td>
            <td><center>{($data.IPK5_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.TOTAL_LULUSAN}</center></td>
            <td>
			<center>
				{$data.RATA_IPK|round:2}
			</center>
			</td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_10/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_12/$row)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_ipk/$row)|round:2}</center></td>
	</tr>
</table>


<br />
<br />
{/if}


{if $jenjang == 'S2'}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan IPK (Program S2)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Akademik Lulus</center></td>
        <td colspan="2"><center>2,75&lt;=IPK&lt;3,0</center></td>
        <td colspan="2"><center>3,0&lt;=IPK&lt;3,5</center></td>
        <td colspan="2"><center>3,5&lt;=IPK&lt;3,75</center></td>
        <td colspan="2"><center>IPK>=3,75</center></td>
        <td rowspan="2"><center>Total Lulusan</center></td>
        <td rowspan="2"><center>Rerata IPK</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
	{$rata_ipk = 0}
    {foreach $lulusan_s2 as $data}
    	{$jml = $data.IPK1_JML + $data.IPK2_JML + $data.IPK3_JML + $data.IPK4_JML + $data.IPK5_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.IPK1_JML}
        {$jml_6 = $jml_6 + $data.IPK2_JML}
        {$jml_8 = $jml_8 + $data.IPK3_JML}
		{$jml_12 = $jml_12 + $data.IPK5_JML}
        {$rata_2 = $rata_2 + ($data.IPK1_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_6 = $rata_6 + ($data.IPK2_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_8 = $rata_8 + ($data.IPK3_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_12 = $rata_12 + ($data.IPK5_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_ipk = $rata_ipk + $data.RATA_IPK}
        {$row = $row+1}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{(int)$data.IPK1_JML}</center></td>
            <td><center>{($data.IPK1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK2_JML}</center></td>
            <td><center>{($data.IPK2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK3_JML}</center></td>
            <td><center>{($data.IPK3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK5_JML}</center></td>
            <td><center>{($data.IPK5_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.TOTAL_LULUSAN}</center></td>
            <td>
			<center>
				{$data.RATA_IPK|round:2}
			</center>
			</td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_12/$row)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_ipk/$row)|round:2}</center></td>
	</tr>
</table>


<br />
<br />

{/if}

{if $jenjang == 'S3'}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan IPK (Program S3)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Akademik Lulus</center></td>
        <td colspan="2"><center>2,75&lt;=IPK&lt;3,0</center></td>
        <td colspan="2"><center>3,0&lt;=IPK&lt;3,5</center></td>
        <td colspan="2"><center>3,5&lt;=IPK&lt;3,75</center></td>
        <td colspan="2"><center>IPK>=3,75</center></td>
        <td rowspan="2"><center>Total Lulusan</center></td>
        <td rowspan="2"><center>Rerata IPK</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
	{$rata_ipk = 0}
    {foreach $lulusan_s3 as $data}
    	{$jml = $data.IPK1_JML + $data.IPK2_JML + $data.IPK3_JML + $data.IPK4_JML + $data.IPK5_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.IPK1_JML}
        {$jml_6 = $jml_6 + $data.IPK2_JML}
        {$jml_8 = $jml_8 + $data.IPK3_JML}
		{$jml_12 = $jml_12 + $data.IPK5_JML}
        {$rata_2 = $rata_2 + ($data.IPK1_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_6 = $rata_6 + ($data.IPK2_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_8 = $rata_8 + ($data.IPK3_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_12 = $rata_12 + ($data.IPK5_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_ipk = $rata_ipk + $data.RATA_IPK}
        {$row = $row+1}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{(int)$data.IPK1_JML}</center></td>
            <td><center>{($data.IPK1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK2_JML}</center></td>
            <td><center>{($data.IPK2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK3_JML}</center></td>
            <td><center>{($data.IPK3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK5_JML}</center></td>
            <td><center>{($data.IPK5_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.TOTAL_LULUSAN}</center></td>
            <td>
			<center>
				{$data.RATA_IPK|round:2}
			</center>
			</td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_12/$row)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_ipk/$row)|round:2}</center></td>
	</tr>
</table>

{/if}











{if $jenjang == ''}

{if count($lulusan_r) > 0}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan IPK (Program Sarjana)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Akademik Lulus</center></td>
        <td colspan="2"><center>2&lt;=IPK&lt;2,5</center></td>
        <td colspan="2"><center>2,5&lt;=IPK&lt;2,75</center></td>
        <td colspan="2"><center>2,75&lt;=IPK&lt;3,0</center></td>
        <td colspan="2"><center>3,0&lt;=IPK&lt;3,5</center></td>
        <td colspan="2"><center>IPK>=3,5</center></td>
        <td rowspan="2"><center>Total Lulusan</center></td>
        <td rowspan="2"><center>Rerata IPK</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
	{$rata_ipk = 0}
    {foreach $lulusan_r as $data}
    	{$jml = $data.IPK1_JML + $data.IPK2_JML + $data.IPK3_JML + $data.IPK4_JML + $data.IPK5_JML}
        {$jml_total = $jml_total + $data.TOTAL_LULUSAN}
        {$jml_2 = $jml_2 + $data.IPK1_JML}
        {$jml_6 = $jml_6 + $data.IPK2_JML}
        {$jml_8 = $jml_8 + $data.IPK3_JML}
        {$jml_10 = $jml_10 + $data.IPK4_JML}
		{$jml_12 = $jml_12 + $data.IPK5_JML}
        {$rata_2 = $rata_2 + ($data.IPK1_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_6 = $rata_6 + ($data.IPK2_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_8 = $rata_8 + ($data.IPK3_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_10 = $rata_10 + ($data.IPK4_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_12 = $rata_12 + ($data.IPK5_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_ipk = $rata_ipk + $data.RATA_IPK}
        {$row = $row+1}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{(int)$data.IPK1_JML}</center></td>
            <td><center>{($data.IPK1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK2_JML}</center></td>
            <td><center>{($data.IPK2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK3_JML}</center></td>
            <td><center>{($data.IPK3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK4_JML}</center></td>
            <td><center>{($data.IPK4_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK5_JML}</center></td>
            <td><center>{($data.IPK5_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.TOTAL_LULUSAN}</center></td>
            <td>
			<center>
				{$data.RATA_IPK|round:2}
			</center>
			</td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_10/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_12/$row)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_ipk/$row)|round:2}</center></td>
	</tr>
</table>

{/if}

<br />
<br />

{if count($lulusan_aj) > 0}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan IPK (Program Alih Jenis dari D3 ke S1)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Akademik Lulus</center></td>
        <td colspan="2"><center>2&lt;=IPK&lt;2,5</center></td>
        <td colspan="2"><center>2,5&lt;=IPK&lt;2,75</center></td>
        <td colspan="2"><center>2,75&lt;=IPK&lt;3,0</center></td>
        <td colspan="2"><center>3,0&lt;=IPK&lt;3,5</center></td>
        <td colspan="2"><center>IPK>=3,5</center></td>
        <td rowspan="2"><center>Total Lulusan</center></td>
        <td rowspan="2"><center>Rerata IPK</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
	{$rata_ipk = 0}
    {foreach $lulusan_aj as $data}
    	{$jml = $data.IPK1_JML + $data.IPK2_JML + $data.IPK3_JML + $data.IPK4_JML + $data.IPK5_JML}
        {$jml_total = $jml_total + $data.TOTAL_LULUSAN}
        {$jml_2 = $jml_2 + $data.IPK1_JML}
        {$jml_6 = $jml_6 + $data.IPK2_JML}
        {$jml_8 = $jml_8 + $data.IPK3_JML}
        {$jml_10 = $jml_10 + $data.IPK4_JML}
		{$jml_12 = $jml_12 + $data.IPK5_JML}
       {$rata_2 = $rata_2 + ($data.IPK1_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_6 = $rata_6 + ($data.IPK2_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_8 = $rata_8 + ($data.IPK3_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_10 = $rata_10 + ($data.IPK4_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_12 = $rata_12 + ($data.IPK5_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_ipk = $rata_ipk + $data.RATA_IPK}
        {$row = $row+1}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{(int)$data.IPK1_JML}</center></td>
            <td><center>{($data.IPK1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK2_JML}</center></td>
            <td><center>{($data.IPK2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK3_JML}</center></td>
            <td><center>{($data.IPK3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK4_JML}</center></td>
            <td><center>{($data.IPK4_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK5_JML}</center></td>
            <td><center>{($data.IPK5_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.TOTAL_LULUSAN}</center></td>
            <td>
			<center>
				{$data.RATA_IPK|round:2}
			</center>
			</td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_10/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_12/$row)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_ipk/$row)|round:2}</center></td>
	</tr>
</table>

{/if}
<br />
<br />


{if count($lulusan_d3) > 0}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan IPK (Program D3)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Akademik Lulus</center></td>
        <td colspan="2"><center>2&lt;=IPK&lt;2,5</center></td>
        <td colspan="2"><center>2,5&lt;=IPK&lt;2,75</center></td>
        <td colspan="2"><center>2,75&lt;=IPK&lt;3,0</center></td>
        <td colspan="2"><center>3,0&lt;=IPK&lt;3,5</center></td>
        <td colspan="2"><center>IPK>=3,5</center></td>
        <td rowspan="2"><center>Total Lulusan</center></td>
        <td rowspan="2"><center>Rerata IPK</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
	{$rata_ipk = 0}
    {foreach $lulusan_d3 as $data}
    	{$jml = $data.IPK1_JML + $data.IPK2_JML + $data.IPK3_JML + $data.IPK4_JML + $data.IPK5_JML}
        {$jml_total = $jml_total + $data.TOTAL_LULUSAN}
        {$jml_2 = $jml_2 + $data.IPK1_JML}
        {$jml_6 = $jml_6 + $data.IPK2_JML}
        {$jml_8 = $jml_8 + $data.IPK3_JML}
        {$jml_10 = $jml_10 + $data.IPK4_JML}
		{$jml_12 = $jml_12 + $data.IPK5_JML}
        {$rata_2 = $rata_2 + ($data.IPK1_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_6 = $rata_6 + ($data.IPK2_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_8 = $rata_8 + ($data.IPK3_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_10 = $rata_10 + ($data.IPK4_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_12 = $rata_12 + ($data.IPK5_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_ipk = $rata_ipk + $data.RATA_IPK}
        {$row = $row+1}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{(int)$data.IPK1_JML}</center></td>
            <td><center>{($data.IPK1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK2_JML}</center></td>
            <td><center>{($data.IPK2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK3_JML}</center></td>
            <td><center>{($data.IPK3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK4_JML}</center></td>
            <td><center>{($data.IPK4_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK5_JML}</center></td>
            <td><center>{($data.IPK5_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.TOTAL_LULUSAN}</center></td>
            <td>
			<center>
				{$data.RATA_IPK|round:2}
			</center>
			</td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_10/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_12/$row)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_ipk/$row)|round:2}</center></td>
	</tr>
</table>
{/if}

<br />
<br />

{if count($lulusan_s2) > 0}

	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan IPK (Program S2)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Akademik Lulus</center></td>
        <td colspan="2"><center>2,75&lt;=IPK&lt;3,0</center></td>
        <td colspan="2"><center>3,0&lt;=IPK&lt;3,5</center></td>
        <td colspan="2"><center>3,5&lt;=IPK&lt;3,75</center></td>
        <td colspan="2"><center>IPK>=3,75</center></td>
        <td rowspan="2"><center>Total Lulusan</center></td>
        <td rowspan="2"><center>Rerata IPK</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
	{$rata_ipk = 0}
    {foreach $lulusan_s2 as $data}
    	{$jml = $data.IPK1_JML + $data.IPK2_JML + $data.IPK3_JML + $data.IPK4_JML + $data.IPK5_JML}
        {$jml_total = $jml_total + $data.TOTAL_LULUSAN}
        {$jml_2 = $jml_2 + $data.IPK1_JML}
        {$jml_6 = $jml_6 + $data.IPK2_JML}
        {$jml_8 = $jml_8 + $data.IPK3_JML}
        {$jml_10 = $jml_10 + $data.IPK4_JML}
		{$jml_12 = $jml_12 + $data.IPK5_JML}
        {$rata_2 = $rata_2 + ($data.IPK1_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_6 = $rata_6 + ($data.IPK2_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_8 = $rata_8 + ($data.IPK3_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_12 = $rata_12 + ($data.IPK5_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_ipk = $rata_ipk + $data.RATA_IPK}
        {$row = $row+1}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{(int)$data.IPK1_JML}</center></td>
            <td><center>{($data.IPK1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK2_JML}</center></td>
            <td><center>{($data.IPK2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK3_JML}</center></td>
            <td><center>{($data.IPK3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK5_JML}</center></td>
            <td><center>{($data.IPK5_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.TOTAL_LULUSAN}</center></td>
            <td>
			<center>
				{$data.RATA_IPK|round:2}
			</center>
			</td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_12/$row)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_ipk/$row)|round:2}</center></td>
	</tr>
</table>
{/if}

<br />
<br />


{if count($lulusan_s3) > 0}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan IPK (Program S3)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Akademik Lulus</center></td>
        <td colspan="2"><center>2,75&lt;=IPK&lt;3,0</center></td>
        <td colspan="2"><center>3,0&lt;=IPK&lt;3,5</center></td>
        <td colspan="2"><center>3,5&lt;=IPK&lt;3,75</center></td>
        <td colspan="2"><center>IPK>=3,75</center></td>
        <td rowspan="2"><center>Total Lulusan</center></td>
        <td rowspan="2"><center>Rerata IPK</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
	{$rata_ipk = 0}
    {foreach $lulusan_s3 as $data}
    	{$jml = $data.IPK1_JML + $data.IPK2_JML + $data.IPK3_JML + $data.IPK4_JML + $data.IPK5_JML}
        {$jml_total = $jml_total + $data.TOTAL_LULUSAN}
        {$jml_2 = $jml_2 + $data.IPK1_JML}
        {$jml_6 = $jml_6 + $data.IPK2_JML}
        {$jml_8 = $jml_8 + $data.IPK3_JML}
        {$jml_10 = $jml_10 + $data.IPK4_JML}
		{$jml_12 = $jml_12 + $data.IPK5_JML}
        {$rata_2 = $rata_2 + ($data.IPK1_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_6 = $rata_6 + ($data.IPK2_JML/$data.TOTAL_LULUSAN*100)}
        {$rata_8 = $rata_8 + ($data.IPK3_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_12 = $rata_12 + ($data.IPK5_JML/$data.TOTAL_LULUSAN*100)}
		{$rata_ipk = $rata_ipk + $data.RATA_IPK}
        {$row = $row+1}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{(int)$data.IPK1_JML}</center></td>
            <td><center>{($data.IPK1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK2_JML}</center></td>
            <td><center>{($data.IPK2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK3_JML}</center></td>
            <td><center>{($data.IPK3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{(int)$data.IPK5_JML}</center></td>
            <td><center>{($data.IPK5_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.TOTAL_LULUSAN}</center></td>
            <td>
			<center>
				{$data.RATA_IPK|round:2}
			</center>
			</td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_12/$row)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_ipk/$row)|round:2}</center></td>
	</tr>
</table>
{/if}

{/if}