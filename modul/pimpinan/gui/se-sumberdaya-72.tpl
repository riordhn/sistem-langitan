{* Yudi Sulistya, 29-04-2012 *}
<div class="center_title_bar">Profil Perkembangan Dosen Prodi dalam Departemen/Fakultas berdasarkan Jabatan Fungsional dalam Lima Tahun Terakhir{if $smarty.get.id == '%'}<li>UNIVERSITAS AIRLANGGA</li>{else}{foreach item="list" from=$FAK}<li>{$list.FAKULTAS}</li>{/foreach}{/if}</div>
<form action="se-sumberdaya-72.php" method="get">
<p>Pilih Fakultas : 
	<select name="id" id="id">
	<option value="">----------</option>
	<option value='%' style="font-weight: bold; color: #fff; background-color: #013e68;">LIHAT SEMUA</option>
	{foreach item="fak" from=$KD_FAK}
	{html_options values=$fak.ID_FAKULTAS output=$fak.NM_FAKULTAS}
	{/foreach}
	</select>
	<input type="submit" name="View" value="View">
</p>
</form>
{if $smarty.get.id == ''}
{else}
	<table width="850" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th rowspan="3" width="90" style="vertical-align: middle;"><center>Jabatan<br>Fungsional</center></th>
		</tr>
		<tr>
			<th width="152" colspan="2"><center>{$smarty.now|date_format:"%Y"-4}</center></th>
			<th width="152" colspan="2"><center>{$smarty.now|date_format:"%Y"-3}</center></th>
			<th width="152" colspan="2"><center>{$smarty.now|date_format:"%Y"-2}</center></th>
			<th width="152" colspan="2"><center>{$smarty.now|date_format:"%Y"-1}</center></th>
			<th width="152" colspan="2"><center>{$smarty.now|date_format:"%Y"}</center></th>
		</tr>
		<tr>
			<th width="76"><center>&Sigma;</center></th>
			<th width="76"><center>&#37;</center></th>
			<th width="76"><center>&Sigma;</center></th>
			<th width="76"><center>&#37;</center></th>
			<th width="76"><center>&Sigma;</center></th>
			<th width="76"><center>&#37;</center></th>
			<th width="76"><center>&Sigma;</center></th>
			<th width="76"><center>&#37;</center></th>
			<th width="76"><center>&Sigma;</center></th>
			<th width="76"><center>&#37;</center></th>
		</tr>
		</thead>
		<tbody>
			{assign var="j1" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$j1 y=$data.JML1 assign="j1"}
			{/foreach}

			{assign var="j2" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$j2 y=$data.JML2 assign="j2"}
			{/foreach}

			{assign var="j3" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$j3 y=$data.JML3 assign="j3"}
			{/foreach}

			{assign var="j4" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$j4 y=$data.JML4 assign="j4"}
			{/foreach}

			{assign var="j5" value=0}
			{foreach item="data" from=$DATA}
			{math equation="x+y" x=$j5 y=$data.JML5 assign="j5"}
			{/foreach}
			
			{foreach item="data" from=$DATA}
		<tr>
			<td width="90" style="vertical-align: middle;"><center>{$data.JABATAN}</center></td>
			<td width="76" style="vertical-align: middle;"><center>{if $data.JML5 < 1}-{else}{$data.JML5}{/if}</center></td>
			<td width="76" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$data.JML5 y=$j5 format="%.2f"}</center></td>
			<td width="76" style="vertical-align: middle;"><center>{if $data.JML4 < 1}-{else}{$data.JML4}{/if}</center></td>
			<td width="76" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$data.JML4 y=$j4 format="%.2f"}</center></td>
			<td width="76" style="vertical-align: middle;"><center>{if $data.JML3 < 1}-{else}{$data.JML3}{/if}</center></td>
			<td width="76" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$data.JML3 y=$j3 format="%.2f"}</center></td>
			<td width="76" style="vertical-align: middle;"><center>{if $data.JML2 < 1}-{else}{$data.JML2}{/if}</center></td>
			<td width="76" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$data.JML2 y=$j2 format="%.2f"}</center></td>
			<td width="76" style="vertical-align: middle;"><center>{if $data.JML1 < 1}-{else}{$data.JML1}{/if}</center></td>
			<td width="76" style="vertical-align: middle;"><center>{math equation="((x / y) * 100)" x=$data.JML1 y=$j1 format="%.2f"}</center></td>
		</tr>
			{/foreach}
		<tr>
			<td width="90" style="vertical-align: middle;"><center>&Sigma;</center></td>
			<td width="76" style="vertical-align: middle;"><center>{$j5}</center></td>
			<td bgcolor="#013e68" width="76" style="vertical-align: middle;"><center></center></td>
			<td width="76" style="vertical-align: middle;"><center>{$j4}</center></td>
			<td bgcolor="#013e68" width="76" style="vertical-align: middle;"><center></center></td>
			<td width="76" style="vertical-align: middle;"><center>{$j3}</center></td>
			<td bgcolor="#013e68" width="76" style="vertical-align: middle;"><center></center></td>
			<td width="76" style="vertical-align: middle;"><center>{$j2}</center></td>
			<td bgcolor="#013e68" width="76" style="vertical-align: middle;"><center></center></td>
			<td width="76" style="vertical-align: middle;"><center>{$j1}</center></td>
			<td bgcolor="#013e68" width="76" style="vertical-align: middle;"><center></center></td>
		</tr>
		</tbody>
	</table>
{/if}