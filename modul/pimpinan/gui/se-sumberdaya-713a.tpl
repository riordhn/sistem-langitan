{* Yudi Sulistya, 29-04-2012 *}
<div class="center_title_bar">Profil Tenaga Kependidikan Tetap berdasarkan Umur dan Tingkat Pendidikan Tahun {'Y'|date}{if $smarty.get.id == '%'}<li>UNIVERSITAS AIRLANGGA</li>{else}{foreach item="list" from=$UK}<li>{$list.UNITKERJA}</li>{/foreach}{/if}</div>
<form action="se-sumberdaya-713a.php" method="get">
<p>Pilih Unit Kerja : 
	<select name="id" id="id">
	<option value="">----------</option>
	<option value='%' style="font-weight: bold; color: #fff; background-color: #013e68;">LIHAT SEMUA</option>
	{foreach item="uk" from=$KD_UK}
	{html_options values=$uk.ID_UNIT_KERJA output=$uk.UNITKERJA}
	{/foreach}
	</select>
	<input type="submit" name="View" value="View">
</p>
</form>
{if $smarty.get.id == ''}
{else}
<p align="right">
<input type=button name="cetak" value="Ekspor ke Excel" onclick="window.open('proses/se-sumberdaya-713a.php?id={$smarty.get.id}','baru2');">
</p>
	<table width="850" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
			<th rowspan="2" width="90" style="vertical-align: middle;"><center>Pendidikan<br>Terakhir</center></th>
			<th colspan="5" width="720"><center>Kelompok Umur (tahun)</center></th>
			<th colspan="2" width="40"><center>Jumlah</center></th>
		<tr>
			<th width="90"><center>&#60; 31</center></th>
			<th width="90"><center>31 &#8211; 40</center></th>
			<th width="90"><center>41 &#8211; 50</center></th>
			<th width="90"><center>51 &#8211; 55</center></th>
			<th width="90"><center>&#62; 55</center></th>
			<th width="90"><center>&Sigma;</center></th>
			<th width="90"><center>&#37;</center></th>
		</tr>
		</thead>
		<tbody>
			{foreach item="gol" from=$GOL}
		<tr>
			<td><center>{$gol.NAMA_PENDIDIKAN_AKHIR}</center></td>
			<td><center>{if $gol.II == 0}-{else}{$gol.II}{/if}</center></td>
			<td><center>{if $gol.III == 0}-{else}{$gol.III}{/if}</center></td>
			<td><center>{if $gol.IV == 0}-{else}{$gol.IV}{/if}</center></td>
			<td><center>{if $gol.V == 0}-{else}{$gol.V}{/if}</center></td>
			<td><center>{if $gol.VI == 0}-{else}{$gol.VI}{/if}</center></td>
			<td><center>{if $gol.TOTAL == 0}-{else}{$gol.TOTAL}{/if}</center></td>
			<td><center>{math equation="((x / y) * 100)" x=$gol.TOTAL y=$TTL format="%.2f"}</center></td>
		</tr>
			{foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
			{/foreach}
			{foreach item="jml" from=$JML}
		<tr>
			<td><center>&Sigma;</center></td>
			<td><center>{if $jml.II == 0}-{else}{$jml.II}{/if}</center></td>
			<td><center>{if $jml.III == 0}-{else}{$jml.III}{/if}</center></td>
			<td><center>{if $jml.IV == 0}-{else}{$jml.IV}{/if}</center></td>
			<td><center>{if $jml.V == 0}-{else}{$jml.V}{/if}</center></td>
			<td><center>{if $jml.VI == 0}-{else}{$jml.VI}{/if}</center></td>
			<td><center>{if $jml.TOTAL == 0}-{else}{$jml.TOTAL}{/if}</center></td>
			<td bgcolor="#013e68"><center></center></td>
		</tr>
			{/foreach}
			{foreach item="psn" from=$PSN}
		<tr>
			<td><center>&#37;</center></th>
			<td><center>{$psn.II|string_format:"%.2f"}</center></td>
			<td><center>{$psn.III|string_format:"%.2f"}</center></td>
			<td><center>{$psn.IV|string_format:"%.2f"}</center></td>
			<td><center>{$psn.V|string_format:"%.2f"}</center></td>
			<td><center>{$psn.VI|string_format:"%.2f"}</center></td>
			<td bgcolor="#013e68"><center></center></td>
			<td><center>{$psn.PERSEN}</center></td>
		</tr>
			{/foreach}
		</tbody>
	</table>
{/if}
