<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="16">
			<div class="center_title_bar">Profil Penyelenggaraan Proses Pendidikan Tahun Akademik {$tahun}/{$tahun+1}(Program Diploma)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="16"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>


<table width="100%" border="1" class="ui-widget" style="font-size:10px;">
  <tr class="ui-widget-header">
    <td rowspan="2" align="center"><center>Nama Mata Kuliah &amp; Praktikum</center></td>
    <td rowspan="2" align="center"><center>% Kehadiran Dosen di kelas</center></td>
    <td colspan="7" align="center"><center>Jumlah</center></td>
    <td colspan="7" align="center"><center>% Distribusi Nilai</center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center>SAP</center></td>
    <td align="center"><center>Bahan Ajar/Diklat</center></td>
    <td align="center"><center>Petunjuk Praktikum</center></td>
    <td align="center"><center>Buku Teks</center></td>
    <td align="center"><center>Peserta Kuliah</center></td>
    <td align="center"><center>Kelas Paralel</center></td>
    <td align="center"><center>Peserta Ulang</center></td>
    <td align="center"><center>A</center></td>
    <td align="center"><center>AB</center></td>
    <td align="center"><center>B</center></td>
    <td align="center"><center>BC</center></td>
    <td align="center"><center>C</center></td>
    <td align="center"><center>D</center></td>
    <td align="center"><center>E</center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center>1</center></td>
    <td align="center"><center>2</center></td>
    <td align="center"><center>3</center></td>
    <td align="center"><center>4</center></td>
    <td align="center"><center>5</center></td>
    <td align="center"><center>6</center></td>
    <td align="center"><center>7</center></td>
    <td align="center"><center>8</center></td>
    <td align="center"><center>9</center></td>
    <td align="center"><center>10</center></td>
    <td align="center"><center>11</center></td>
    <td align="center"><center>12</center></td>
    <td align="center"><center>13</center></td>
    <td align="center"><center>14</center></td>
    <td align="center"><center>15</center></td>
    <td align="center"><center>16</center></td>
  </tr>
 {$jml_row_persenhadir = 0}
 {$jml_persenhadir = 0}
 {$jml_peserta_total = 0}
 {$jml_ulang = 0}
 {$jml_paralel = 0}
 {$jml_dosen_hadir=0}
 {$jml_tm=0}
 
 {$jml_row = 0}
 
 {$jml_nila = 0}
 {$jml_nilab = 0}
 {$jml_nilb = 0}
 {$jml_nilbc = 0}
 {$jml_nilc = 0}
 {$jml_nild = 0}
 {$jml_nile = 0}
 
 <tr>
    <td colspan="16" align="center" >SEMESTER GANJIL</td>
 </tr> 
 {foreach $pendidikan_D3 as $data}
 {$jml_nila = $jml_nila+$data.NILA}
 {$jml_nilab = $jml_nilab+$data.NILAB}
 {$jml_nilb = $jml_nilb+$data.NILB}
 {$jml_nilbc = $jml_nilbc+$data.NILBC}
 {$jml_nilc = $jml_nilc+$data.NILC}
 {$jml_nild = $jml_nild+$data.NILD}
 {$jml_nile = $jml_nile+$data.NILE}
 
 {$jml_row = $jml_row + 1}
 {if $data.POSISI==1}
 {$jml_peserta_total = $jml_peserta_total+$data.PST}
 {$jml_dosen_hadir=$jml_dosen_hadir+$data.DOSEN_HADIR}
 {$jml_tm=$jml_tm+$data.JUMLAH_PERTEMUAN_KELAS_MK}
 {$jml_ulang = $jml_ulang+$data.PSTULANG}
 {$jml_paralel = $jml_paralel+$data.KLS_PARALEL}

 
 <tr class="ui-widget-content">
    <td><left>{$data.NM_MATA_KULIAH}</left></td>
    <td><center>{round($data.DOSEN_HADIR/$data.JUMLAH_PERTEMUAN_KELAS_MK*100,2)}</center></td>
    <td><center>{$data.SAP}</center></td>
    <td><center>{$data.BAHAN_AJAR_DIKTAT}</center></td>
	<td><center>{$data.PETUNJUK_PRAKTIKUM}</center></td>
    <td><center>{$data.BUKU_TEKS}</center></td>
    <td><center>{$data.PST}</center></td>
    <td><center>{$data.KLS_PARALEL}</center></td>
    <td><center>{$data.PSTULANG}</center></td>
    <td><center>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
  </tr>
  {/if}
  {/foreach}
  
 <tr>
    <td colspan="16" align="center">SEMESTER GENAP</td>
 </tr> 
 {foreach $pendidikan_D3 as $data}
 {if $data.POSISI==2}
 {$jml_peserta_total = $jml_peserta_total+$data.PST}
 {$jml_dosen_hadir=$jml_dosen_hadir+$data.DOSEN_HADIR}
 {$jml_tm=$jml_tm+$data.JUMLAH_PERTEMUAN_KELAS_MK}
 {$jml_ulang = $jml_ulang+$data.PSTULANG}
 {$jml_paralel = $jml_paralel+$data.KLS_PARALEL}


 <tr class="ui-widget-content">
    <td><left>{$data.NM_MATA_KULIAH}</left></td>
    <td><center>{round($data.DOSEN_HADIR/$data.JUMLAH_PERTEMUAN_KELAS_MK*100,2)}</center></td>
    <td><center>{$data.SAP}</center></td>
    <td><center>{$data.BAHAN_AJAR_DIKTAT}</center></td>
	<td><center>{$data.PETUNJUK_PRAKTIKUM}</center></td>
    <td><center>{$data.BUKU_TEKS}</center></td>
    <td><center>{$data.PST}</center></td>
    <td><center>{$data.KLS_PARALEL}</center></td>
    <td><center>{$data.PSTULANG}</center></td>
    <td>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
  </tr>
  {/if}
  {/foreach}
  
  <tr>
    <td class="ui-widget-header">Jumlah</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center> - </center></td>
    <td><center> - </center></td>
    <td><center> - </center></td>
    <td><center> - </center></td>
    <td><center> {$jml_peserta_total} </center></td>
    <td><center> {$jml_paralel} </center></td>
    <td><center> {$jml_ulang} </center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td class="ui-widget-header">Rerata</td>
    <td class="ui-widget-content"><center>{round($jml_dosen_hadir/$jml_tm*100, 2)}</center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center>{round($jml_nila/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilab/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilb/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilbc/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilc/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nild/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nile/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>

  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="16">
			<div class="center_title_bar">Profil Penyelenggaraan Proses Pendidikan Tahun Akademik {$tahun}/{$tahun+1}(Program Reguler)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="16"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>


<table width="100%" border="1" class="ui-widget" style="font-size:10px;">
  <tr class="ui-widget-header">
    <td rowspan="2" align="center"><center>Nama Mata Kuliah &amp; Praktikum</center></td>
    <td rowspan="2" align="center"><center>% Kehadiran Dosen di kelas</center></td>
    <td colspan="7" align="center"><center>Jumlah</center></td>
    <td colspan="7" align="center"><center>% Distribusi Nilai</center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center>SAP</center></td>
    <td align="center"><center>Bahan Ajar/Diklat</center></td>
    <td align="center"><center>Petunjuk Praktikum</center></td>
    <td align="center"><center>Buku Teks</center></td>
    <td align="center"><center>Peserta Kuliah</center></td>
    <td align="center"><center>Kelas Paralel</center></td>
    <td align="center"><center>Peserta Ulang</center></td>
    <td align="center"><center>A</center></td>
    <td align="center"><center>AB</center></td>
    <td align="center"><center>B</center></td>
    <td align="center"><center>BC</center></td>
    <td align="center"><center>C</center></td>
    <td align="center"><center>D</center></td>
    <td align="center"><center>E</center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center>1</center></td>
    <td align="center"><center>2</center></td>
    <td align="center"><center>3</center></td>
    <td align="center"><center>4</center></td>
    <td align="center"><center>5</center></td>
    <td align="center"><center>6</center></td>
    <td align="center"><center>7</center></td>
    <td align="center"><center>8</center></td>
    <td align="center"><center>9</center></td>
    <td align="center"><center>10</center></td>
    <td align="center"><center>11</center></td>
    <td align="center"><center>12</center></td>
    <td align="center"><center>13</center></td>
    <td align="center"><center>14</center></td>
    <td align="center"><center>15</center></td>
    <td align="center"><center>16</center></td>
  </tr>
 {$jml_row_persenhadir = 0}
 {$jml_persenhadir = 0}
 {$jml_peserta_total = 0}
 {$jml_ulang = 0}
 {$jml_paralel = 0}
 {$jml_dosen_hadir=0}
 {$jml_tm=0}
 
 {$jml_row = 0}
 
 {$jml_nila = 0}
 {$jml_nilab = 0}
 {$jml_nilb = 0}
 {$jml_nilbc = 0}
 {$jml_nilc = 0}
 {$jml_nild = 0}
 {$jml_nile = 0}
 
 <tr>
    <td colspan="16" align="center" >SEMESTER GANJIL</td>
 </tr> 
 {foreach $pendidikan_R as $data}
 {$jml_nila = $jml_nila+$data.NILA}
 {$jml_nilab = $jml_nilab+$data.NILAB}
 {$jml_nilb = $jml_nilb+$data.NILB}
 {$jml_nilbc = $jml_nilbc+$data.NILBC}
 {$jml_nilc = $jml_nilc+$data.NILC}
 {$jml_nild = $jml_nild+$data.NILD}
 {$jml_nile = $jml_nile+$data.NILE}
 
 {$jml_row = $jml_row + 1}
 {if $data.POSISI==1}
 {$jml_peserta_total = $jml_peserta_total+$data.PST}
 {$jml_dosen_hadir=$jml_dosen_hadir+$data.DOSEN_HADIR}
 {$jml_tm=$jml_tm+$data.JUMLAH_PERTEMUAN_KELAS_MK}
 {$jml_ulang = $jml_ulang+$data.PSTULANG}
 {$jml_paralel = $jml_paralel+$data.KLS_PARALEL}

 
 <tr class="ui-widget-content">
    <td><left>{$data.NM_MATA_KULIAH}</left></td>
    <td><center>{round($data.DOSEN_HADIR/$data.JUMLAH_PERTEMUAN_KELAS_MK*100,2)}</center></td>
    <td><center>{$data.SAP}</center></td>
    <td><center>{$data.BAHAN_AJAR_DIKTAT}</center></td>
	<td><center>{$data.PETUNJUK_PRAKTIKUM}</center></td>
    <td><center>{$data.BUKU_TEKS}</center></td>
    <td><center>{$data.PST}</center></td>
    <td><center>{$data.KLS_PARALEL}</center></td>
    <td><center>{$data.PSTULANG}</center></td>
    <td><center>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
  </tr>
  {/if}
  {/foreach}
  
 <tr>
    <td colspan="16" align="center">SEMESTER GENAP</td>
 </tr> 
 {foreach $pendidikan_R as $data}
 {if $data.POSISI==2}
 {$jml_peserta_total = $jml_peserta_total+$data.PST}
 {$jml_dosen_hadir=$jml_dosen_hadir+$data.DOSEN_HADIR}
 {$jml_tm=$jml_tm+$data.JUMLAH_PERTEMUAN_KELAS_MK}
 {$jml_ulang = $jml_ulang+$data.PSTULANG}
 {$jml_paralel = $jml_paralel+$data.KLS_PARALEL}


 <tr class="ui-widget-content">
    <td><left>{$data.NM_MATA_KULIAH}</left></td>
    <td><center>{round($data.DOSEN_HADIR/$data.JUMLAH_PERTEMUAN_KELAS_MK*100,2)}</center></td>
    <td><center>{$data.SAP}</center></td>
    <td><center>{$data.BAHAN_AJAR_DIKTAT}</center></td>
	<td><center>{$data.PETUNJUK_PRAKTIKUM}</center></td>
    <td><center>{$data.BUKU_TEKS}</center></td>
    <td><center>{$data.PST}</center></td>
    <td><center>{$data.KLS_PARALEL}</center></td>
    <td><center>{$data.PSTULANG}</center></td>
    <td>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
  </tr>
  {/if}
  {/foreach}
  
  <tr>
    <td class="ui-widget-header">Jumlah</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center> - </center></td>
    <td><center> - </center></td>
    <td><center> - </center></td>
    <td><center> - </center></td>
    <td><center> {$jml_peserta_total} </center></td>
    <td><center> {$jml_paralel} </center></td>
    <td><center> {$jml_ulang} </center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td class="ui-widget-header">Rerata</td>
    <td class="ui-widget-content"><center>{round($jml_dosen_hadir/$jml_tm*100, 2)}</center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center>{round($jml_nila/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilab/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilb/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilbc/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilc/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nild/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nile/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>

  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="16">
			<div class="center_title_bar">Profil Penyelenggaraan Proses Pendidikan Tahun Akademik {$tahun}/{$tahun+1}(Program Alih Jenis)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="16"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>


<table width="100%" border="1" class="ui-widget" style="font-size:10px;">
  <tr class="ui-widget-header">
    <td rowspan="2" align="center"><center>Nama Mata Kuliah &amp; Praktikum</center></td>
    <td rowspan="2" align="center"><center>% Kehadiran Dosen di kelas</center></td>
    <td colspan="7" align="center"><center>Jumlah</center></td>
    <td colspan="7" align="center"><center>% Distribusi Nilai</center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center>SAP</center></td>
    <td align="center"><center>Bahan Ajar/Diklat</center></td>
    <td align="center"><center>Petunjuk Praktikum</center></td>
    <td align="center"><center>Buku Teks</center></td>
    <td align="center"><center>Peserta Kuliah</center></td>
    <td align="center"><center>Kelas Paralel</center></td>
    <td align="center"><center>Peserta Ulang</center></td>
    <td align="center"><center>A</center></td>
    <td align="center"><center>AB</center></td>
    <td align="center"><center>B</center></td>
    <td align="center"><center>BC</center></td>
    <td align="center"><center>C</center></td>
    <td align="center"><center>D</center></td>
    <td align="center"><center>E</center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center>1</center></td>
    <td align="center"><center>2</center></td>
    <td align="center"><center>3</center></td>
    <td align="center"><center>4</center></td>
    <td align="center"><center>5</center></td>
    <td align="center"><center>6</center></td>
    <td align="center"><center>7</center></td>
    <td align="center"><center>8</center></td>
    <td align="center"><center>9</center></td>
    <td align="center"><center>10</center></td>
    <td align="center"><center>11</center></td>
    <td align="center"><center>12</center></td>
    <td align="center"><center>13</center></td>
    <td align="center"><center>14</center></td>
    <td align="center"><center>15</center></td>
    <td align="center"><center>16</center></td>
  </tr>
 {$jml_row_persenhadir = 0}
 {$jml_persenhadir = 0}
 {$jml_peserta_total = 0}
 {$jml_ulang = 0}
 {$jml_paralel = 0}
 {$jml_dosen_hadir=0}
 {$jml_tm=0}
 
 {$jml_row = 0}
 
 {$jml_nila = 0}
 {$jml_nilab = 0}
 {$jml_nilb = 0}
 {$jml_nilbc = 0}
 {$jml_nilc = 0}
 {$jml_nild = 0}
 {$jml_nile = 0}
 
 <tr>
    <td colspan="16" align="center" >SEMESTER GANJIL</td>
 </tr> 
 {foreach $pendidikan_AJ as $data}
 {$jml_nila = $jml_nila+$data.NILA}
 {$jml_nilab = $jml_nilab+$data.NILAB}
 {$jml_nilb = $jml_nilb+$data.NILB}
 {$jml_nilbc = $jml_nilbc+$data.NILBC}
 {$jml_nilc = $jml_nilc+$data.NILC}
 {$jml_nild = $jml_nild+$data.NILD}
 {$jml_nile = $jml_nile+$data.NILE}
 
 {$jml_row = $jml_row + 1}
 {if $data.POSISI==1}
 {$jml_peserta_total = $jml_peserta_total+$data.PST}
 {$jml_dosen_hadir=$jml_dosen_hadir+$data.DOSEN_HADIR}
 {$jml_tm=$jml_tm+$data.JUMLAH_PERTEMUAN_KELAS_MK}
 {$jml_ulang = $jml_ulang+$data.PSTULANG}
 {$jml_paralel = $jml_paralel+$data.KLS_PARALEL}

 
 <tr class="ui-widget-content">
    <td><left>{$data.NM_MATA_KULIAH}</left></td>
    <td><center>{round($data.DOSEN_HADIR/$data.JUMLAH_PERTEMUAN_KELAS_MK*100,2)}</center></td>
    <td><center>{$data.SAP}</center></td>
    <td><center>{$data.BAHAN_AJAR_DIKTAT}</center></td>
	<td><center>{$data.PETUNJUK_PRAKTIKUM}</center></td>
    <td><center>{$data.BUKU_TEKS}</center></td>
    <td><center>{$data.PST}</center></td>
    <td><center>{$data.KLS_PARALEL}</center></td>
    <td><center>{$data.PSTULANG}</center></td>
    <td><center>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
  </tr>
  {/if}
  {/foreach}
  
 <tr>
    <td colspan="16" align="center">SEMESTER GENAP</td>
 </tr> 
 {foreach $pendidikan_AJ as $data}
 {if $data.POSISI==2}
 {$jml_peserta_total = $jml_peserta_total+$data.PST}
 {$jml_dosen_hadir=$jml_dosen_hadir+$data.DOSEN_HADIR}
 {$jml_tm=$jml_tm+$data.JUMLAH_PERTEMUAN_KELAS_MK}
 {$jml_ulang = $jml_ulang+$data.PSTULANG}
 {$jml_paralel = $jml_paralel+$data.KLS_PARALEL}


 <tr class="ui-widget-content">
    <td><left>{$data.NM_MATA_KULIAH}</left></td>
    <td><center>{round($data.DOSEN_HADIR/$data.JUMLAH_PERTEMUAN_KELAS_MK*100,2)}</center></td>
    <td><center>{$data.SAP}</center></td>
    <td><center>{$data.BAHAN_AJAR_DIKTAT}</center></td>
	<td><center>{$data.PETUNJUK_PRAKTIKUM}</center></td>
    <td><center>{$data.BUKU_TEKS}</center></td>
    <td><center>{$data.PST}</center></td>
    <td><center>{$data.KLS_PARALEL}</center></td>
    <td><center>{$data.PSTULANG}</center></td>
    <td>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
  </tr>
  {/if}
  {/foreach}
  
  <tr>
    <td class="ui-widget-header">Jumlah</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center> - </center></td>
    <td><center> - </center></td>
    <td><center> - </center></td>
    <td><center> - </center></td>
    <td><center> {$jml_peserta_total} </center></td>
    <td><center> {$jml_paralel} </center></td>
    <td><center> {$jml_ulang} </center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td class="ui-widget-header">Rerata</td>
    <td class="ui-widget-content"><center>{round($jml_dosen_hadir/$jml_tm*100, 2)}</center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center>{round($jml_nila/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilab/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilb/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilbc/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilc/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nild/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nile/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="16">
			<div class="center_title_bar">Profil Penyelenggaraan Proses Pendidikan Tahun Akademik {$tahun}/{$tahun+1}(Program S2)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="16"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>


<table width="100%" border="1" class="ui-widget" style="font-size:10px;">
  <tr class="ui-widget-header">
    <td rowspan="2" align="center"><center>Nama Mata Kuliah &amp; Praktikum</center></td>
    <td rowspan="2" align="center"><center>% Kehadiran Dosen di kelas</center></td>
    <td colspan="7" align="center"><center>Jumlah</center></td>
    <td colspan="7" align="center"><center>% Distribusi Nilai</center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center>Bahan Ajar/Diklat</center></td>
    <td align="center"><center>Buku Teks</center></td>
    <td align="center"><center>Peserta Kuliah</center></td>
    <td align="center"><center>Kelas Paralel</center></td>
    <td align="center"><center>Peserta Ulang</center></td>
    <td align="center"><center>A</center></td>
    <td align="center"><center>AB</center></td>
    <td align="center"><center>B</center></td>
    <td align="center"><center>BC</center></td>
    <td align="center"><center>C</center></td>
    <td align="center"><center>D</center></td>
    <td align="center"><center>E</center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center>1</center></td>
    <td align="center"><center>2</center></td>
    <td align="center"><center>3</center></td>
    <td align="center"><center>4</center></td>
    <td align="center"><center>5</center></td>
    <td align="center"><center>6</center></td>
    <td align="center"><center>7</center></td>
    <td align="center"><center>8</center></td>
    <td align="center"><center>9</center></td>
    <td align="center"><center>10</center></td>
    <td align="center"><center>11</center></td>
    <td align="center"><center>12</center></td>
    <td align="center"><center>13</center></td>
    <td align="center"><center>14</center></td>
  </tr>
 {$jml_row_persenhadir = 0}
 {$jml_persenhadir = 0}
 {$jml_peserta_total = 0}
 {$jml_ulang = 0}
 {$jml_paralel = 0}
 {$jml_dosen_hadir=0}
 {$jml_tm=0}
 
 {$jml_row = 0}
 
 {$jml_nila = 0}
 {$jml_nilab = 0}
 {$jml_nilb = 0}
 {$jml_nilbc = 0}
 {$jml_nilc = 0}
 {$jml_nild = 0}
 {$jml_nile = 0}
 
 <tr>
    <td colspan="16" align="center" >SEMESTER GANJIL</td>
 </tr> 
 {foreach $pendidikan_S2 as $data}
 {$jml_nila = $jml_nila+$data.NILA}
 {$jml_nilab = $jml_nilab+$data.NILAB}
 {$jml_nilb = $jml_nilb+$data.NILB}
 {$jml_nilbc = $jml_nilbc+$data.NILBC}
 {$jml_nilc = $jml_nilc+$data.NILC}
 {$jml_nild = $jml_nild+$data.NILD}
 {$jml_nile = $jml_nile+$data.NILE}
 
 {$jml_row = $jml_row + 1}
 {if $data.POSISI==1}
 {$jml_peserta_total = $jml_peserta_total+$data.PST}
 {$jml_dosen_hadir=$jml_dosen_hadir+$data.DOSEN_HADIR}
 {$jml_tm=$jml_tm+$data.JUMLAH_PERTEMUAN_KELAS_MK}
 {$jml_ulang = $jml_ulang+$data.PSTULANG}
 {$jml_paralel = $jml_paralel+$data.KLS_PARALEL}

 
 <tr class="ui-widget-content">
    <td><left>{$data.NM_MATA_KULIAH}</left></td>
    <td><center>{round($data.DOSEN_HADIR/$data.JUMLAH_PERTEMUAN_KELAS_MK*100,2)}</center></td>
    <td><center>{$data.BAHAN_AJAR_DIKTAT}</center></td>
    <td><center>{$data.BUKU_TEKS}</center></td>
    <td><center>{$data.PST}</center></td>
    <td><center>{$data.KLS_PARALEL}</center></td>
    <td><center>{$data.PSTULANG}</center></td>
    <td><center>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
  </tr>
  {/if}
  {/foreach}
  
 <tr>
    <td colspan="16" align="center">SEMESTER GENAP</td>
 </tr> 
 {foreach $pendidikan_S2 as $data}
 {if $data.POSISI==2}
 {$jml_peserta_total = $jml_peserta_total+$data.PST}
 {$jml_dosen_hadir=$jml_dosen_hadir+$data.DOSEN_HADIR}
 {$jml_tm=$jml_tm+$data.JUMLAH_PERTEMUAN_KELAS_MK}
 {$jml_ulang = $jml_ulang+$data.PSTULANG}
 {$jml_paralel = $jml_paralel+$data.KLS_PARALEL}


 <tr class="ui-widget-content">
    <td><left>{$data.NM_MATA_KULIAH}</left></td>
    <td><center>{round($data.DOSEN_HADIR/$data.JUMLAH_PERTEMUAN_KELAS_MK*100,2)}</center></td>
    <td><center>{$data.BAHAN_AJAR_DIKTAT}</center></td>
    <td><center>{$data.BUKU_TEKS}</center></td>
    <td><center>{$data.PST}</center></td>
    <td><center>{$data.KLS_PARALEL}</center></td>
    <td><center>{$data.PSTULANG}</center></td>
    <td>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
  </tr>
  {/if}
  {/foreach}
  
  <tr>
    <td class="ui-widget-header">Jumlah</td>
    <td bgcolor="#000000">&nbsp;</td>    <td><center> - </center></td>
    <td><center> - </center></td>
    <td><center> {$jml_peserta_total} </center></td>
    <td><center> {$jml_paralel} </center></td>
    <td><center> {$jml_ulang} </center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td class="ui-widget-header">Rerata</td>
    <td class="ui-widget-content"><center>{round($jml_dosen_hadir/$jml_tm*100, 2)}</center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center>{round($jml_nila/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilab/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilb/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilbc/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilc/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nild/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nile/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>

  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="16">
			<div class="center_title_bar">Profil Penyelenggaraan Proses Pendidikan Tahun Akademik {$tahun}/{$tahun+1}(Program S3)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="16"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>


<table width="100%" border="1" class="ui-widget" style="font-size:10px;">
  <tr class="ui-widget-header">
    <td rowspan="2" align="center"><center>Nama Mata Kuliah &amp; Praktikum</center></td>
    <td rowspan="2" align="center"><center>% Kehadiran Dosen di kelas</center></td>
    <td colspan="7" align="center"><center>Jumlah</center></td>
    <td colspan="7" align="center"><center>% Distribusi Nilai</center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center>Bahan Ajar/Diklat</center></td>
    <td align="center"><center>Buku Teks</center></td>
    <td align="center"><center>Peserta Kuliah</center></td>
    <td align="center"><center>Kelas Paralel</center></td>
    <td align="center"><center>Peserta Ulang</center></td>
    <td align="center"><center>A</center></td>
    <td align="center"><center>AB</center></td>
    <td align="center"><center>B</center></td>
    <td align="center"><center>BC</center></td>
    <td align="center"><center>C</center></td>
    <td align="center"><center>D</center></td>
    <td align="center"><center>E</center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center>1</center></td>
    <td align="center"><center>2</center></td>
    <td align="center"><center>3</center></td>
    <td align="center"><center>4</center></td>
    <td align="center"><center>5</center></td>
    <td align="center"><center>6</center></td>
    <td align="center"><center>7</center></td>
    <td align="center"><center>8</center></td>
    <td align="center"><center>9</center></td>
    <td align="center"><center>10</center></td>
    <td align="center"><center>11</center></td>
    <td align="center"><center>12</center></td>
    <td align="center"><center>13</center></td>
    <td align="center"><center>14</center></td>
  </tr>
 {$jml_row_persenhadir = 0}
 {$jml_persenhadir = 0}
 {$jml_peserta_total = 0}
 {$jml_ulang = 0}
 {$jml_paralel = 0}
 {$jml_dosen_hadir=0}
 {$jml_tm=0}
 
 {$jml_row = 0}
 
 {$jml_nila = 0}
 {$jml_nilab = 0}
 {$jml_nilb = 0}
 {$jml_nilbc = 0}
 {$jml_nilc = 0}
 {$jml_nild = 0}
 {$jml_nile = 0}
 
 <tr>
    <td colspan="16" align="center" >SEMESTER GANJIL</td>
 </tr> 
 {foreach $pendidikan_S3 as $data}
 {$jml_nila = $jml_nila+$data.NILA}
 {$jml_nilab = $jml_nilab+$data.NILAB}
 {$jml_nilb = $jml_nilb+$data.NILB}
 {$jml_nilbc = $jml_nilbc+$data.NILBC}
 {$jml_nilc = $jml_nilc+$data.NILC}
 {$jml_nild = $jml_nild+$data.NILD}
 {$jml_nile = $jml_nile+$data.NILE}
 
 {$jml_row = $jml_row + 1}
 {if $data.POSISI==1}
 {$jml_peserta_total = $jml_peserta_total+$data.PST}
 {$jml_dosen_hadir=$jml_dosen_hadir+$data.DOSEN_HADIR}
 {$jml_tm=$jml_tm+$data.JUMLAH_PERTEMUAN_KELAS_MK}
 {$jml_ulang = $jml_ulang+$data.PSTULANG}
 {$jml_paralel = $jml_paralel+$data.KLS_PARALEL}

 
 <tr class="ui-widget-content">
    <td><left>{$data.NM_MATA_KULIAH}</left></td>
    <td><center>{round($data.DOSEN_HADIR/$data.JUMLAH_PERTEMUAN_KELAS_MK*100,2)}</center></td>
    <td><center>{$data.BAHAN_AJAR_DIKTAT}</center></td>
    <td><center>{$data.BUKU_TEKS}</center></td>
    <td><center>{$data.PST}</center></td>
    <td><center>{$data.KLS_PARALEL}</center></td>
    <td><center>{$data.PSTULANG}</center></td>
    <td><center>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
  </tr>
  {/if}
  {/foreach}
  
 <tr>
    <td colspan="16" align="center">SEMESTER GENAP</td>
 </tr> 
 {foreach $pendidikan_S3 as $data}
 {if $data.POSISI==2}
 {$jml_peserta_total = $jml_peserta_total+$data.PST}
 {$jml_dosen_hadir=$jml_dosen_hadir+$data.DOSEN_HADIR}
 {$jml_tm=$jml_tm+$data.JUMLAH_PERTEMUAN_KELAS_MK}
 {$jml_ulang = $jml_ulang+$data.PSTULANG}
 {$jml_paralel = $jml_paralel+$data.KLS_PARALEL}


 <tr class="ui-widget-content">
    <td><left>{$data.NM_MATA_KULIAH}</left></td>
    <td><center>{round($data.DOSEN_HADIR/$data.JUMLAH_PERTEMUAN_KELAS_MK*100,2)}</center></td>
    <td><center>{$data.BAHAN_AJAR_DIKTAT}</center></td>
    <td><center>{$data.BUKU_TEKS}</center></td>
    <td><center>{$data.PST}</center></td>
    <td><center>{$data.KLS_PARALEL}</center></td>
    <td><center>{$data.PSTULANG}</center></td>
    <td>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
    <td>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</td>
  </tr>
  {/if}
  {/foreach}
  
  <tr>
    <td class="ui-widget-header">Jumlah</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center> - </center></td>
    <td><center> - </center></td>
    <td><center> {$jml_peserta_total} </center></td>
    <td><center> {$jml_paralel} </center></td>
    <td><center> {$jml_ulang} </center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td class="ui-widget-header">Rerata</td>
    <td class="ui-widget-content"><center>{round($jml_dosen_hadir/$jml_tm*100, 2)}</center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center>{round($jml_nila/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilab/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilb/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilbc/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nilc/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nild/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>
    <td><center>{round($jml_nile/($jml_nila+$jml_nilab+$jml_nilb+$jml_nilbc+$jml_nilc+$jml_nild+$jml_nile)*100,2)}</center></td>

  </tr>
</table>