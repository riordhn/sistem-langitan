{if $jenjang == 'S1'}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Masa Studi (Program Sarjana)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="8"><center>Masa Studi (Tahun)</center></td>
        <td rowspan="3"><center>Jumlah Lulusan</center></td>
        <td rowspan="3"><center>Rerata Masa Studi (Tahun)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 4</center></td>
        <td colspan="2"><center>4 &lt; X &lt;= 4,5</center></td>
        <td colspan="2"><center>4,5 &lt; X &lt;= 5,0</center></td>
        <td colspan="2"><center>X > 5,0</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$row = 0}
    {$rata_2 = 0}
    
    {foreach $lulusan_r as $data}
        {$jml_total = $jml_total + $data.MASA_STUDI1_PERSEN}
        {$jml_2 = $jml_2 + $data.MASA_STUDI1_JML}
        {$jml_4 = $jml_4 + $data.MASA_STUDI2_JML}
        {$jml_6 = $jml_6 + $data.MASA_STUDI3_JML}
        {$jml_8 = $jml_8 + $data.MASA_STUDI4_JML}
        {$rata_2 = $rata_2 + $data.MASA_STUDI1_PERSEN}
        {$row = $row+1}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.MASA_STUDI1_JML}</center></td>
            <td><center>{round($data.MASA_STUDI1_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI2_JML}</center></td>
            <td><center>{round($data.MASA_STUDI2_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI3_JML}</center></td>
            <td><center>{round($data.MASA_STUDI3_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI4_JML}</center></td>
            <td><center>{round($data.MASA_STUDI4_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI1_PERSEN}</center></td>
            <td><center>{$data.RATA_MASA_STUDI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($jml_8/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center></center></td>
	</tr>
</table>
<br />
<br />


{if count($lulusan_aj) > 0}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Masa Studi (Program Alih Jenis dari D3 ke S1)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="8"><center>Masa Studi (Tahun)</center></td>
        <td rowspan="3"><center>Jumlah Lulusan</center></td>
        <td rowspan="3"><center>Rerata Masa Studi (Tahun)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 4</center></td>
        <td colspan="2"><center>4 &lt; X &lt;= 4,5</center></td>
        <td colspan="2"><center>4,5 &lt; X &lt;= 5,0</center></td>
        <td colspan="2"><center>X > 5,0</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$row = 0}
    {$rata_2 = 0}
    
    {foreach $lulusan_aj as $data}
        {$jml_total = $jml_total + $data.MASA_STUDI1_PERSEN}
        {$jml_2 = $jml_2 + $data.MASA_STUDI1_JML}
        {$jml_4 = $jml_4 + $data.MASA_STUDI2_JML}
        {$jml_6 = $jml_6 + $data.MASA_STUDI3_JML}
        {$jml_8 = $jml_8 + $data.MASA_STUDI4_JML}
        {$rata_2 = $rata_2 + $data.MASA_STUDI1_PERSEN}
        {$row = $row+1}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.MASA_STUDI1_JML}</center></td>
            <td><center>{round($data.MASA_STUDI1_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI2_JML}</center></td>
            <td><center>{round($data.MASA_STUDI2_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI3_JML}</center></td>
            <td><center>{round($data.MASA_STUDI3_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI4_JML}</center></td>
            <td><center>{round($data.MASA_STUDI4_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI1_PERSEN}</center></td>
            <td><center>{$data.RATA_MASA_STUDI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($jml_8/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center></center></td>
	</tr>
</table>
{/if}
<br />
<br />
{/if}


{if $jenjang == 'D3'}

	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Masa Studi (Program D3)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="8"><center>Masa Studi (Tahun)</center></td>
        <td rowspan="3"><center>Jumlah Lulusan</center></td>
        <td rowspan="3"><center>Rerata Masa Studi (Tahun)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 3</center></td>
        <td colspan="2"><center>3 &lt; X &lt;= 3,5</center></td>
        <td colspan="2"><center>3,5 &lt; X &lt;= 4,0</center></td>
        <td colspan="2"><center>X > 4,0</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$row = 0}
    {$rata_2 = 0}
    
    {foreach $lulusan_d3 as $data}
        {$jml_total = $jml_total + $data.MASA_STUDI1_PERSEN}
        {$jml_2 = $jml_2 + $data.MASA_STUDI1_JML}
        {$jml_4 = $jml_4 + $data.MASA_STUDI2_JML}
        {$jml_6 = $jml_6 + $data.MASA_STUDI3_JML}
        {$jml_8 = $jml_8 + $data.MASA_STUDI4_JML}
        {$rata_2 = $rata_2 + $data.MASA_STUDI1_PERSEN}
        {$row = $row+1}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.MASA_STUDI1_JML}</center></td>
            <td><center>{round($data.MASA_STUDI1_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI2_JML}</center></td>
            <td><center>{round($data.MASA_STUDI2_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI3_JML}</center></td>
            <td><center>{round($data.MASA_STUDI3_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI4_JML}</center></td>
            <td><center>{round($data.MASA_STUDI4_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI1_PERSEN}</center></td>
            <td><center>{$data.RATA_MASA_STUDI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($jml_8/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center></center></td>
	</tr>
</table>
<br />
<br />
{/if}


{if $jenjang == 'S2'}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Masa Studi (Program S2)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="8"><center>Masa Studi (Tahun)</center></td>
        <td rowspan="3"><center>Jumlah Lulusan</center></td>
        <td rowspan="3"><center>Rerata Masa Studi (Tahun)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 2</center></td>
        <td colspan="2"><center>2 &lt; X &lt;= 2,5</center></td>
        <td colspan="2"><center>2,5 &lt; X &lt;= 3,0</center></td>
        <td colspan="2"><center>X > 3,0</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$row = 0}
    {$rata_2 = 0}
    
    {foreach $lulusan_s2 as $data}
        {$jml_total = $jml_total + $data.MASA_STUDI1_PERSEN}
        {$jml_2 = $jml_2 + $data.MASA_STUDI1_JML}
        {$jml_4 = $jml_4 + $data.MASA_STUDI2_JML}
        {$jml_6 = $jml_6 + $data.MASA_STUDI3_JML}
        {$jml_8 = $jml_8 + $data.MASA_STUDI4_JML}
        {$rata_2 = $rata_2 + $data.MASA_STUDI1_PERSEN}
        {$row = $row+1}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.MASA_STUDI1_JML}</center></td>
            <td><center>{round($data.MASA_STUDI1_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI2_JML}</center></td>
            <td><center>{round($data.MASA_STUDI2_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI3_JML}</center></td>
            <td><center>{round($data.MASA_STUDI3_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI4_JML}</center></td>
            <td><center>{round($data.MASA_STUDI4_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI1_PERSEN}</center></td>
            <td><center>{$data.RATA_MASA_STUDI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($jml_8/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center></center></td>
	</tr>
</table>
<br />
<br />

{/if}


{if $jenjang == 'S3'}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Masa Studi (Program S3)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="8"><center>Masa Studi (Tahun)</center></td>
        <td rowspan="3"><center>Jumlah Lulusan</center></td>
        <td rowspan="3"><center>Rerata Masa Studi (Tahun)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 3</center></td>
        <td colspan="2"><center>3 &lt; X &lt;= 3,5</center></td>
        <td colspan="2"><center>3,5 &lt; X &lt;= 4,0</center></td>
        <td colspan="2"><center>X > 4,0</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$row = 0}
    {$rata_2 = 0}
    
    {foreach $lulusan_s3 as $data}
        {$jml_total = $jml_total + $data.MASA_STUDI1_PERSEN}
        {$jml_2 = $jml_2 + $data.MASA_STUDI1_JML}
        {$jml_4 = $jml_4 + $data.MASA_STUDI2_JML}
        {$jml_6 = $jml_6 + $data.MASA_STUDI3_JML}
        {$jml_8 = $jml_8 + $data.MASA_STUDI4_JML}
        {$rata_2 = $rata_2 + $data.MASA_STUDI1_PERSEN}
        {$row = $row+1}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.MASA_STUDI1_JML}</center></td>
            <td><center>{round($data.MASA_STUDI1_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI2_JML}</center></td>
            <td><center>{round($data.MASA_STUDI2_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI3_JML}</center></td>
            <td><center>{round($data.MASA_STUDI3_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI4_JML}</center></td>
            <td><center>{round($data.MASA_STUDI4_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI1_PERSEN}</center></td>
            <td><center>{$data.RATA_MASA_STUDI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($jml_8/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center></center></td>
	</tr>
</table>
{/if}







{if $jenjang == ''}

{if count($lulusan_r) > 0}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Masa Studi (Program Sarjana)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="8"><center>Masa Studi (Tahun)</center></td>
        <td rowspan="3"><center>Jumlah Lulusan</center></td>
        <td rowspan="3"><center>Rerata Masa Studi (Tahun)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 4</center></td>
        <td colspan="2"><center>4 &lt; X &lt;= 4,5</center></td>
        <td colspan="2"><center>4,5 &lt; X &lt;= 5,0</center></td>
        <td colspan="2"><center>X > 5,0</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$row = 0}
    {$rata_2 = 0}
    
    {foreach $lulusan_r as $data}
        {$jml_total = $jml_total + $data.MASA_STUDI1_PERSEN}
        {$jml_2 = $jml_2 + $data.MASA_STUDI1_JML}
        {$jml_4 = $jml_4 + $data.MASA_STUDI2_JML}
        {$jml_6 = $jml_6 + $data.MASA_STUDI3_JML}
        {$jml_8 = $jml_8 + $data.MASA_STUDI4_JML}
        {$rata_2 = $rata_2 + $data.MASA_STUDI1_PERSEN}
        {$row = $row+1}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.MASA_STUDI1_JML}</center></td>
            <td><center>{round($data.MASA_STUDI1_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI2_JML}</center></td>
            <td><center>{round($data.MASA_STUDI2_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI3_JML}</center></td>
            <td><center>{round($data.MASA_STUDI3_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI4_JML}</center></td>
            <td><center>{round($data.MASA_STUDI4_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI1_PERSEN}</center></td>
            <td><center>{$data.RATA_MASA_STUDI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($jml_8/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center></center></td>
	</tr>
</table>
{/if}
<br />
<br />


{if count($lulusan_aj) > 0}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Masa Studi (Program Alih Jenis dari D3 ke S1)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="8"><center>Masa Studi (Tahun)</center></td>
        <td rowspan="3"><center>Jumlah Lulusan</center></td>
        <td rowspan="3"><center>Rerata Masa Studi (Tahun)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 4</center></td>
        <td colspan="2"><center>4 &lt; X &lt;= 4,5</center></td>
        <td colspan="2"><center>4,5 &lt; X &lt;= 5,0</center></td>
        <td colspan="2"><center>X > 5,0</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$row = 0}
    {$rata_2 = 0}
    
    {foreach $lulusan_aj as $data}
        {$jml_total = $jml_total + $data.MASA_STUDI1_PERSEN}
        {$jml_2 = $jml_2 + $data.MASA_STUDI1_JML}
        {$jml_4 = $jml_4 + $data.MASA_STUDI2_JML}
        {$jml_6 = $jml_6 + $data.MASA_STUDI3_JML}
        {$jml_8 = $jml_8 + $data.MASA_STUDI4_JML}
        {$rata_2 = $rata_2 + $data.MASA_STUDI1_PERSEN}
        {$row = $row+1}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.MASA_STUDI1_JML}</center></td>
            <td><center>{round($data.MASA_STUDI1_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI2_JML}</center></td>
            <td><center>{round($data.MASA_STUDI2_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI3_JML}</center></td>
            <td><center>{round($data.MASA_STUDI3_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI4_JML}</center></td>
            <td><center>{round($data.MASA_STUDI4_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI1_PERSEN}</center></td>
            <td><center>{$data.RATA_MASA_STUDI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($jml_8/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center></center></td>
	</tr>
</table>

{/if}
<br />
<br />


{if count($lulusan_d3) > 0}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Masa Studi (Program D3)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="8"><center>Masa Studi (Tahun)</center></td>
        <td rowspan="3"><center>Jumlah Lulusan</center></td>
        <td rowspan="3"><center>Rerata Masa Studi (Tahun)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 4</center></td>
        <td colspan="2"><center>4 &lt; X &lt;= 4,5</center></td>
        <td colspan="2"><center>4,5 &lt; X &lt;= 5,0</center></td>
        <td colspan="2"><center>X > 5,0</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$row = 0}
    {$rata_2 = 0}
    
    {foreach $lulusan_d3 as $data}
        {$jml_total = $jml_total + $data.MASA_STUDI1_PERSEN}
        {$jml_2 = $jml_2 + $data.MASA_STUDI1_JML}
        {$jml_4 = $jml_4 + $data.MASA_STUDI2_JML}
        {$jml_6 = $jml_6 + $data.MASA_STUDI3_JML}
        {$jml_8 = $jml_8 + $data.MASA_STUDI4_JML}
        {$rata_2 = $rata_2 + $data.MASA_STUDI1_PERSEN}
        {$row = $row+1}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.MASA_STUDI1_JML}</center></td>
            <td><center>{round($data.MASA_STUDI1_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI2_JML}</center></td>
            <td><center>{round($data.MASA_STUDI2_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI3_JML}</center></td>
            <td><center>{round($data.MASA_STUDI3_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI4_JML}</center></td>
            <td><center>{round($data.MASA_STUDI4_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI1_PERSEN}</center></td>
            <td><center>{$data.RATA_MASA_STUDI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($jml_8/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center></center></td>
	</tr>
</table>
{/if}
<br />
<br />


{if count($lulusan_s2) > 0}

	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Masa Studi (Program S2)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="8"><center>Masa Studi (Tahun)</center></td>
        <td rowspan="3"><center>Jumlah Lulusan</center></td>
        <td rowspan="3"><center>Rerata Masa Studi (Tahun)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 2</center></td>
        <td colspan="2"><center>2 &lt; X &lt;= 2,5</center></td>
        <td colspan="2"><center>2,5 &lt; X &lt;= 3,0</center></td>
        <td colspan="2"><center>X > 3,0</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$row = 0}
    {$rata_2 = 0}
    
    {foreach $lulusan_s2 as $data}
        {$jml_total = $jml_total + $data.MASA_STUDI1_PERSEN}
        {$jml_2 = $jml_2 + $data.MASA_STUDI1_JML}
        {$jml_4 = $jml_4 + $data.MASA_STUDI2_JML}
        {$jml_6 = $jml_6 + $data.MASA_STUDI3_JML}
        {$jml_8 = $jml_8 + $data.MASA_STUDI4_JML}
        {$rata_2 = $rata_2 + $data.MASA_STUDI1_PERSEN}
        {$row = $row+1}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.MASA_STUDI1_JML}</center></td>
            <td><center>{round($data.MASA_STUDI1_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI2_JML}</center></td>
            <td><center>{round($data.MASA_STUDI2_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI3_JML}</center></td>
            <td><center>{round($data.MASA_STUDI3_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI4_JML}</center></td>
            <td><center>{round($data.MASA_STUDI4_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI1_PERSEN}</center></td>
            <td><center>{$data.RATA_MASA_STUDI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($jml_8/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center></center></td>
	</tr>
</table>
{/if}
<br />
<br />


{if count($lulusan_s3) > 0}
	<table class="ui-widget">
		<tr style="background:#669;color:#fff;padding:5px;">
			<td colspan="11">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Masa Studi (Program S3)</div>
			</td>
		</tr>
        <tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="8"><center>Masa Studi (Tahun)</center></td>
        <td rowspan="3"><center>Jumlah Lulusan</center></td>
        <td rowspan="3"><center>Rerata Masa Studi (Tahun)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 3</center></td>
        <td colspan="2"><center>3 &lt; X &lt;= 3,5</center></td>
        <td colspan="2"><center>3,5 &lt; X &lt;= 4,0</center></td>
        <td colspan="2"><center>X > 4,0</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$row = 0}
    {$rata_2 = 0}
    
    {foreach $lulusan_s3 as $data}
        {$jml_total = $jml_total + $data.MASA_STUDI1_PERSEN}
        {$jml_2 = $jml_2 + $data.MASA_STUDI1_JML}
        {$jml_4 = $jml_4 + $data.MASA_STUDI2_JML}
        {$jml_6 = $jml_6 + $data.MASA_STUDI3_JML}
        {$jml_8 = $jml_8 + $data.MASA_STUDI4_JML}
        {$rata_2 = $rata_2 + $data.MASA_STUDI1_PERSEN}
        {$row = $row+1}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.MASA_STUDI1_JML}</center></td>
            <td><center>{round($data.MASA_STUDI1_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI2_JML}</center></td>
            <td><center>{round($data.MASA_STUDI2_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI3_JML}</center></td>
            <td><center>{round($data.MASA_STUDI3_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI4_JML}</center></td>
            <td><center>{round($data.MASA_STUDI4_JML/$data.MASA_STUDI1_PERSEN*100, 2)}</center></td>
            <td><center>{$data.MASA_STUDI1_PERSEN}</center></td>
            <td><center>{$data.RATA_MASA_STUDI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($jml_8/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center></center></td>
	</tr>
</table>

{/if}
{/if}