{* Yudi Sulistya, 29-04-2012 *}
<div class="center_title_bar">Profil Dosen Prodi dalam Departemen/Fakultas Menurut Kompetensi{if $smarty.get.id == '%'}<li>UNIVERSITAS AIRLANGGA</li>{else}{foreach item="list" from=$FAK}<li>{$list.FAKULTAS}</li>{/foreach}{/if}</div>
<form action="se-sumberdaya-78.php" method="get">
<p>Pilih Fakultas : 
	<select name="id" id="id">
	<option value="">----------</option>
	<option value='%' style="font-weight: bold; color: #fff; background-color: #013e68;">LIHAT SEMUA</option>
	{foreach item="fak" from=$KD_FAK}
	{html_options values=$fak.ID_FAKULTAS output=$fak.NM_FAKULTAS}
	{/foreach}
	</select>
	<input type="submit" name="View" value="View">
</p>
</form>
{if $smarty.get.id == ''}
{else}
{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		}
	);
}
);
</script>
{/literal}
<p align="right">
<input type=button name="cetak" value="Ekspor ke Excel" onclick="window.open('proses/se-sumberdaya-78.php?id={$smarty.get.id}','baru2');">
</p>
	<table id="myTable" width="850" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
        <tr>
			<th width="700"><center>Kelompok Bidang Keahlian</center></th>
			<th width="75"><center>Jumlah</center></th>
			<th width="75"><center>Persentase</center></th>
		</tr>
		</thead>
		<tbody>
			{foreach item="kpt" from=$KPT}
		<tr>
			<td>{$kpt.KOMPETENSI}</td>
			<td><center>{$kpt.JML}</center></td>
			<td><center>{math equation="((x / y) * 100)" x=$kpt.JML y=$TTL format="%.2f"}</center></td>
		</tr>
			{foreachelse}
		<tr>
			<td colspan="3"><em>Data tidak ditemukan</em></td>
		</tr>
			{/foreach}
		</tbody>
		<tr>
			<td><center><strong>Total</strong></center></td>
			<td><center><strong>{$TTL}<strong></center></td>
			<td><center><strong>{math equation="((x / y) * 100)" x=$TTL y=$TTL format="%.2f"}<strong></center></td>
		</tr>
	</table>
{/if}