<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="4">
			<div class="center_title_bar">Proses Data Akademik (Semua Tabel 4) </div>
			</td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td width="50%"><strong><center>Uraian</center></strong></td>
    <td width="40%" colspan="2" ><strong><center>Aksi</center></strong></td>
  </tr>
  

  <tr>
    <td><strong><left>41-Status Akademik</left></strong></td>
	<td colspan="2"><strong><center>
		{$41genap}
	{if $41genap == ''}
		<input name="Button" style="background-color: red" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=status_akademik&th={$tahun}&st=1';disableButtons()";return true" value="Proses {$tahun}" /> 
	{else}
		<input name="Button" style="background-color: green" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=status_akademik&th={$41genapth}&st=2';disableButtons()";return true" value="Re-Proses {$tahun}" /> 
	{/if}	
	</center></strong></td>
  </tr>
  <tr>
    <td><strong><left>42-IPK Mhs Aktif</left></strong></td>
    <td><strong><center>
	{$42ganjil}
	{if $42ganjil == ''}
		<input name="Button" style="background-color: red" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=ips_ipk&th={$tahun}1&st=1';disableButtons()";return true" value="Proses {$tahun}-Ganjil" /> 
	{else}
		<input name="Button" style="background-color: green" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=ips_ipk&th={$tahun}1&st=2';disableButtons()";return true" value="Re-Proses {$tahun}-Ganjil" /> 
	{/if}	
	</center></strong></td>
	<td><strong><center>
	{$42genap}
	{if $42genap == ''}
		<input name="Button" style="background-color: red" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=ips_ipk&th={$tahun}2&st=1';disableButtons()";return true" value="Proses {$tahun}-Genap" /> 
	{else}
		<input name="Button" style="background-color: green" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=ips_ipk&th={$tahun}2&st=2';disableButtons()";return true" value="Re-Proses {$tahun}-Genap" /> 
	{/if}	
	</center></strong></td>
  </tr>
  <tr>
    <td><strong><left>43-Nilai ELPT Maba</left></strong></td>
    <td colspan="2"><strong><center>
	{$43elpt}
	{if $43elpt == ''}
		<input name="Button" style="background-color: red" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=elptmaba&th={$tahun}&st=1';disableButtons()";return true" value="Proses {$tahun}" /> 
	{else}
		<input name="Button" style="background-color: green" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=elptmaba&th={$tahun}&st=2';disableButtons()";return true" value="Re-Proses {$tahun}" /> 
	{/if}	
	</center></strong></td>
  </tr>
    <tr>
    <td><strong><left>45-Proses Pendidikan</left></strong></td>
    <td><strong><center>
	{$45ganjil}
	{if $45ganjil == ''}
		<input name="Button" style="background-color: red" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=distr_nil&th={$tahun}1&st=1';disableButtons()";return true" value="Proses {$tahun}-Ganjil" /> 
	{else}
		<input name="Button" style="background-color: green" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=distr_nil&th={$tahun}1&st=2';disableButtons()";return true" value="Re-Proses {$tahun}-Ganjil" /> 
	{/if}	
	</center></strong></td>
	<td><strong><center>
	{$45genap}
	{if $45genap == ''}
		<input name="Button" style="background-color: red" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=distr_nil&th={$tahun}2&st=1';disableButtons()";return true" value="Proses {$tahun}-Genap" /> 
	{else}
		<input name="Button" style="background-color: green" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=distr_nil&th={$tahun}2&st=2';disableButtons()";return true" value="Re-Proses {$tahun}-Genap" /> 
	{/if}	
	</center></strong></td>
  </tr>
  <tr>
    <td><strong><left>48-Indeks Kepuasan</left></strong></td>
    <td><strong><center>
	{$48ganjil}
	{if $48ganjil == ''}
		<input name="Button" style="background-color: red" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=ik&th={$tahun}1&st=1';disableButtons()";return true" value="Proses {$tahun}-Ganjil" /> 
	{else}
		<input name="Button" style="background-color: green" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=ik&th={$tahun}1&st=2';disableButtons()";return true" value="Re-Proses {$tahun}-Ganjil" /> 
	{/if}	
	</center></strong></td>
	<td><strong><center>
	{$48genap}
	{if $48genap == ''}
		<input name="Button" style="background-color: red" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=ik&th={$tahun}2&st=1';disableButtons()";return true" value="Proses {$tahun}-Genap" /> 
	{else}
		<input name="Button" style="background-color: green" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=ik&th={$tahun}2&st=2';disableButtons()";return true" value="Re-Proses {$tahun}-Genap" /> 
	{/if}	
	</center></strong></td>
  </tr>
  <tr>
    <td><strong><left>410-Eval Kinerja Staff</left></strong></td>
    <td><strong><center>
	{$410ganjil}
	{if $410ganjil == ''}
		<input name="Button" style="background-color: red" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=ev_staff&th={$tahun}1&st=1';disableButtons()";return true" value="Proses {$tahun}-Ganjil" /> 
	{else}
		<input name="Button" style="background-color: green" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=ev_staff&th={$tahun}1&st=2';disableButtons()";return true" value="Re-Proses {$tahun}-Ganjil" /> 
	{/if}	
	</center></strong></td>
	<td><strong><center>
	{$410genap}
	{if $410genap == ''}
		<input name="Button" style="background-color: red" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=ev_staff&th={$tahun}2&st=1';disableButtons()";return true" value="Proses {$tahun}-Genap" /> 
	{else}
		<input name="Button" style="background-color: green" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=ev_staff&th={$tahun}2&st=2';disableButtons()";return true" value="Re-Proses {$tahun}-Genap" /> 
	{/if}	
	</center></strong></td>
  </tr>
    <tr>
    <td><strong><left>411-Eval Maba</left></strong></td>
    <td><strong><center>
	{$411ganjil}
	{if $411ganjil == ''}
		<input name="Button" style="background-color: red" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=eva_maba&th={$tahun}1&st=1';disableButtons()";return true" value="Proses {$tahun}-Ganjil" /> 
	{else}
		<input name="Button" style="background-color: green" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=eva_maba&th={$tahun}1&st=2';disableButtons()";return true" value="Re-Proses {$tahun}-Ganjil" /> 
	{/if}	
	</center></strong></td>
	<td><strong><center>
	{$411genap}
	{if $411genap == ''}
		<input name="Button" style="background-color: red" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=eva_maba&th={$tahun}2&st=1';disableButtons()";return true" value="Proses {$tahun}-Genap" /> 
	{else}
		<input name="Button" style="background-color: green" type="button" class="button" onClick="location.href='#se!se-data_4.php?action=eva_maba&th={$tahun}2&st=2';disableButtons()";return true" value="Re-Proses {$tahun}-Genap" /> 
	{/if}	
	</center></strong></td>
  </tr>
</table>
