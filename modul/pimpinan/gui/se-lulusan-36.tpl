{* Choirul Hidayat, 29-04-2012 *}
{if $jenjang == 'S1'}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Lama Penyelesaian Skripsi (Program Sarjana)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="6"><center>Lama Penyelesaian Skripsi (Semester)</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata<br/>(Bulan)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 1</center></td>
        <td colspan="2"><center>1 &lt; X &lt;= 2</center></td>
        <td colspan="2"><center>X > 2</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_9 = 0}
	{$jml_row = 0}
    {foreach $lulusan_r as $data}
        {$jml = $data.LAMA_SKRIPSI1_JML + $data.LAMA_SKRIPSI2_JML + $data.LAMA_SKRIPSI3_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.LAMA_SKRIPSI1_JML}
        {$jml_4 = $jml_4 + $data.LAMA_SKRIPSI2_JML}
        {$jml_6 = $jml_6 + $data.LAMA_SKRIPSI3_JML}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.LAMA_SKRIPSI1_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI2_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI3_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center></center></td>
			{$jml_row = $jml_row + 1}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_9/$jml_row}</center></td>
	</tr>
</table>

<br />
<br />

{if count($lulusan_aj) > 0}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Lama Penyelesaian Skripsi (Program Alih Jenis dari D3 ke S1)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="6"><center>Lama Penyelesaian Skripsi (Semester)</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata<br/>(Bulan)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 1</center></td>
        <td colspan="2"><center>1 &lt; X &lt;= 2</center></td>
        <td colspan="2"><center>X > 2</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_9 = 0}
	{$jml_row = 0}
    {foreach $lulusan_aj as $data}
        {$jml = $data.LAMA_SKRIPSI1_JML + $data.LAMA_SKRIPSI2_JML + $data.LAMA_SKRIPSI3_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.LAMA_SKRIPSI1_JML}
        {$jml_4 = $jml_4 + $data.LAMA_SKRIPSI2_JML}
        {$jml_6 = $jml_6 + $data.LAMA_SKRIPSI3_JML}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.LAMA_SKRIPSI1_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI2_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI3_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center></center></td>
			{$jml_row = $jml_row + 1}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_9/$jml_row}</center></td>
	</tr>
</table>
{/if}
{/if}


{if $jenjang == 'D3'}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Lama Penyelesaian Tugas Akhir (Program Diploma 3)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="6"><center>Lama Penyelesaian Tugas Akhir (Semester)</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata<br/>(Bulan)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 1</center></td>
        <td colspan="2"><center>1 &lt; X &lt;= 2</center></td>
        <td colspan="2"><center>X > 2</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_9 = 0}
	{$jml_row = 0}
    {foreach $lulusan_d3 as $data}
        {$jml = $data.LAMA_SKRIPSI1_JML + $data.LAMA_SKRIPSI2_JML + $data.LAMA_SKRIPSI3_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.LAMA_SKRIPSI1_JML}
        {$jml_4 = $jml_4 + $data.LAMA_SKRIPSI2_JML}
        {$jml_6 = $jml_6 + $data.LAMA_SKRIPSI3_JML}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.LAMA_SKRIPSI1_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI2_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI3_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center></center></td>
			{$jml_row = $jml_row + 1}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_9/$jml_row}</center></td>
	</tr>
</table>

<br />

{/if}


{* Choirul Hidayat, 29-04-2012 *}
{if $jenjang == ''}

{if count($lulusan_r) > 0}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Lama Penyelesaian Skripsi (Program Sarjana)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="6"><center>Lama Penyelesaian Skripsi (Semester)</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata<br/>(Bulan)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 1</center></td>
        <td colspan="2"><center>1 &lt; X &lt;= 2</center></td>
        <td colspan="2"><center>X > 2</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_9 = 0}
	{$jml_row = 0}
    {foreach $lulusan_r as $data}
        {$jml = $data.LAMA_SKRIPSI1_JML + $data.LAMA_SKRIPSI2_JML + $data.LAMA_SKRIPSI3_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.LAMA_SKRIPSI1_JML}
        {$jml_4 = $jml_4 + $data.LAMA_SKRIPSI2_JML}
        {$jml_6 = $jml_6 + $data.LAMA_SKRIPSI3_JML}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.LAMA_SKRIPSI1_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI2_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI3_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center></center></td>
			{$jml_row = $jml_row + 1}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_9/$jml_row}</center></td>
	</tr>
</table>
{/if}
<br />
<br />


{if count($lulusan_aj) > 0}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Lama Penyelesaian Skripsi (Program Alih Jenis dari D3 ke S1)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="6"><center>Lama Penyelesaian Skripsi (Semester)</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata<br/>(Bulan)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 1</center></td>
        <td colspan="2"><center>1 &lt; X &lt;= 2</center></td>
        <td colspan="2"><center>X > 2</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_9 = 0}
	{$jml_row = 0}
    {foreach $lulusan_aj as $data}
        {$jml = $data.LAMA_SKRIPSI1_JML + $data.LAMA_SKRIPSI2_JML + $data.LAMA_SKRIPSI3_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.LAMA_SKRIPSI1_JML}
        {$jml_4 = $jml_4 + $data.LAMA_SKRIPSI2_JML}
        {$jml_6 = $jml_6 + $data.LAMA_SKRIPSI3_JML}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.LAMA_SKRIPSI1_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI2_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI3_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center></center></td>
			{$jml_row = $jml_row + 1}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_9/$jml_row}</center></td>
	</tr>
</table>
{/if}


<br />
<br />

{if count($lulusan_s2) > 0}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Lama Penulisan Tesis</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="6"><center>Lama Penyelesaian Tesis (Semester)</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata<br/>(Bulan)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 2</center></td>
        <td colspan="2"><center>2 &lt; X &lt;= 3</center></td>
        <td colspan="2"><center>X > 3</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_9 = 0}
	{$jml_row = 0}
    {foreach $lulusan_s2 as $data}
        {$jml = $data.LAMA_SKRIPSI1_JML + $data.LAMA_SKRIPSI2_JML + $data.LAMA_SKRIPSI3_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.LAMA_SKRIPSI1_JML}
        {$jml_4 = $jml_4 + $data.LAMA_SKRIPSI2_JML}
        {$jml_6 = $jml_6 + $data.LAMA_SKRIPSI3_JML}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.LAMA_SKRIPSI1_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI2_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI3_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center></center></td>
			{$jml_row = $jml_row + 1}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_9/$jml_row}</center></td>
	</tr>
</table>

{/if}

<br />
<br />


{if count($lulusan_s3) > 0}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Profil Lulusan Berdasarkan Tahun Akademik Lulusan dan Lama Penulisan Desertasi</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="6"><center>Lama Penyelesaian Disertasi (Semester)</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata<br/>(Bulan)</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt;= 3</center></td>
        <td colspan="2"><center>3 &lt; X &lt;= 4</center></td>
        <td colspan="2"><center>X > 4</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_9 = 0}
	{$jml_row = 0}
    {foreach $lulusan_s3 as $data}
        {$jml = $data.LAMA_SKRIPSI1_JML + $data.LAMA_SKRIPSI2_JML + $data.LAMA_SKRIPSI3_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.LAMA_SKRIPSI1_JML}
        {$jml_4 = $jml_4 + $data.LAMA_SKRIPSI2_JML}
        {$jml_6 = $jml_6 + $data.LAMA_SKRIPSI3_JML}

        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.LAMA_SKRIPSI1_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI2_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.LAMA_SKRIPSI3_JML}</center></td>
            <td><center>{($data.LAMA_SKRIPSI3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center></center></td>
			{$jml_row = $jml_row + 1}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_9/$jml_row}</center></td>
	</tr>
</table>
{/if}
{/if}