<table class="ui-widget"> 
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="8">
			<div class="center_title_bar">Profil Distribusi Nilai Kuliah dan Praktikum per Tahun Akademik (Program Diploma)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="8"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>
<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td rowspan="2" align="center"><center><strong>Tahun Akademik</strong></center></td>
    <td colspan="7" align="center"><center><strong>% Distribusi Rata - rata Nilai Kuliah dan Praktikum</strong></center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center><strong>A</strong></center></td>
    <td align="center"><center><strong>AB</strong></center></td>
    <td align="center"><center><strong>B</strong></center></td>
    <td align="center"><center><strong>BC</strong></center></td>
    <td align="center"><center><strong>C</strong></center></td>
    <td align="center"><center><strong>D</strong></center></td>
    <td align="center"><center><strong>E</strong></center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center><strong>1</strong></center></td>
    <td align="center"><center><strong>2</strong></center></td>
    <td align="center"><center><strong>3</strong></center></td>
    <td align="center"><center><strong>4</strong></center></td>
    <td align="center"><center><strong>5</strong></center></td>
    <td align="center"><center><strong>6</strong></center></td>
    <td align="center"><center><strong>7</strong></center></td>
    <td align="center"><center><strong>8</strong></center></td>
  </tr>

	{$jml_persena = 0}
	{$jml_persenab = 0}
	{$jml_persenb = 0}
	{$jml_persenbc = 0}
	{$jml_persenc = 0}
	{$jml_persend = 0}
	{$jml_persene = 0}
	
 {foreach $disnil_D3 as $data}
  {$jml_persena=$jml_persena+$data.NILA}
  {$jml_persenab=$jml_persenab+$data.NILAB}
  {$jml_persenb=$jml_persenb+$data.NILB}
  {$jml_persenbc=$jml_persenbc+$data.NILBC}
  {$jml_persenc=$jml_persenc+$data.NILC}
  {$jml_persend=$jml_persend+$data.NILD}
  {$jml_persene=$jml_persene+$data.NILE}
  
  <tr class="ui-widget-content">
    <td><center>{$data.TAHUN}</center></td>
    <td><center>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-content">
    <td align="center"><center><strong>Rerata</strong></center></td>
    <td><center>{round($jml_persena/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenab/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenb/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenbc/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenc/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persend/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persene/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
  </tr>
</table>


<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="8">
			<div class="center_title_bar">Profil Distribusi Nilai Kuliah dan Praktikum per Tahun Akademik (Program Reguler)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="8"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>
<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td rowspan="2" align="center"><center><strong>Tahun Akademik</strong></center></td>
    <td colspan="7" align="center"><center><strong>% Distribusi Rata - rata Nilai Kuliah dan Praktikum</strong></center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center><strong>A</strong></center></td>
    <td align="center"><center><strong>AB</strong></center></td>
    <td align="center"><center><strong>B</strong></center></td>
    <td align="center"><center><strong>BC</strong></center></td>
    <td align="center"><center><strong>C</strong></center></td>
    <td align="center"><center><strong>D</strong></center></td>
    <td align="center"><center><strong>E</strong></center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center><strong>1</strong></center></td>
    <td align="center"><center><strong>2</strong></center></td>
    <td align="center"><center><strong>3</strong></center></td>
    <td align="center"><center><strong>4</strong></center></td>
    <td align="center"><center><strong>5</strong></center></td>
    <td align="center"><center><strong>6</strong></center></td>
    <td align="center"><center><strong>7</strong></center></td>
    <td align="center"><center><strong>8</strong></center></td>
  </tr>

	{$jml_persena = 0}
	{$jml_persenab = 0}
	{$jml_persenb = 0}
	{$jml_persenbc = 0}
	{$jml_persenc = 0}
	{$jml_persend = 0}
	{$jml_persene = 0}
	
 {foreach $disnil_R as $data}
  {$jml_persena=$jml_persena+$data.NILA}
  {$jml_persenab=$jml_persenab+$data.NILAB}
  {$jml_persenb=$jml_persenb+$data.NILB}
  {$jml_persenbc=$jml_persenbc+$data.NILBC}
  {$jml_persenc=$jml_persenc+$data.NILC}
  {$jml_persend=$jml_persend+$data.NILD}
  {$jml_persene=$jml_persene+$data.NILE}
  
  <tr class="ui-widget-content">
    <td><center>{$data.TAHUN}</center></td>
    <td><center>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-content">
    <td align="center"><center><strong>Rerata</strong></center></td>
    <td><center>{round($jml_persena/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenab/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenb/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenbc/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenc/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persend/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persene/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="8">
			<div class="center_title_bar">Profil Distribusi Nilai Kuliah dan Praktikum per Tahun Akademik (Program Alih Jenis)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="8"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td rowspan="2" align="center"><center><strong>Tahun Akademik</strong></center></td>
    <td colspan="7" align="center"><center><strong>% Distribusi Rata - rata Nilai Kuliah dan Praktikum</strong></center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center><strong>A</strong></center></td>
    <td align="center"><center><strong>AB</strong></center></td>
    <td align="center"><center><strong>B</strong></center></td>
    <td align="center"><center><strong>BC</strong></center></td>
    <td align="center"><center><strong>C</strong></center></td>
    <td align="center"><center><strong>D</strong></center></td>
    <td align="center"><center><strong>E</strong></center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center><strong>1</strong></center></td>
    <td align="center"><center><strong>2</strong></center></td>
    <td align="center"><center><strong>3</strong></center></td>
    <td align="center"><center><strong>4</strong></center></td>
    <td align="center"><center><strong>5</strong></center></td>
    <td align="center"><center><strong>6</strong></center></td>
    <td align="center"><center><strong>7</strong></center></td>
    <td align="center"><center><strong>8</strong></center></td>
  </tr>

	{$jml_persena = 0}
	{$jml_persenab = 0}
	{$jml_persenb = 0}
	{$jml_persenbc = 0}
	{$jml_persenc = 0}
	{$jml_persend = 0}
	{$jml_persene = 0}
	
 {foreach $disnil_AJ as $data}
  {$jml_persena=$jml_persena+$data.NILA}
  {$jml_persenab=$jml_persenab+$data.NILAB}
  {$jml_persenb=$jml_persenb+$data.NILB}
  {$jml_persenbc=$jml_persenbc+$data.NILBC}
  {$jml_persenc=$jml_persenc+$data.NILC}
  {$jml_persend=$jml_persend+$data.NILD}
  {$jml_persene=$jml_persene+$data.NILE}
  
  <tr class="ui-widget-content">
    <td><center>{$data.TAHUN}</center></td>
    <td><center>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-content">
    <td align="center"><center><strong>Rerata</strong></center></td>
    <td><center>{round($jml_persena/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenab/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenb/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenbc/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenc/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persend/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persene/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="8">
			<div class="center_title_bar">Profil Distribusi Nilai Kuliah dan Praktikum per Tahun Akademik (Program Profesi)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="8"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>
<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td rowspan="2" align="center"><center><strong>Tahun Akademik</strong></center></td>
    <td colspan="7" align="center"><center><strong>% Distribusi Rata - rata Nilai Kuliah dan Praktikum</strong></center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center><strong>A</strong></center></td>
    <td align="center"><center><strong>AB</strong></center></td>
    <td align="center"><center><strong>B</strong></center></td>
    <td align="center"><center><strong>BC</strong></center></td>
    <td align="center"><center><strong>C</strong></center></td>
    <td align="center"><center><strong>D</strong></center></td>
    <td align="center"><center><strong>E</strong></center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center><strong>1</strong></center></td>
    <td align="center"><center><strong>2</strong></center></td>
    <td align="center"><center><strong>3</strong></center></td>
    <td align="center"><center><strong>4</strong></center></td>
    <td align="center"><center><strong>5</strong></center></td>
    <td align="center"><center><strong>6</strong></center></td>
    <td align="center"><center><strong>7</strong></center></td>
    <td align="center"><center><strong>8</strong></center></td>
  </tr>

	{$jml_persena = 0}
	{$jml_persenab = 0}
	{$jml_persenb = 0}
	{$jml_persenbc = 0}
	{$jml_persenc = 0}
	{$jml_persend = 0}
	{$jml_persene = 0}
	
 {foreach $disnil_PROFESI as $data}
  {$jml_persena=$jml_persena+$data.NILA}
  {$jml_persenab=$jml_persenab+$data.NILAB}
  {$jml_persenb=$jml_persenb+$data.NILB}
  {$jml_persenbc=$jml_persenbc+$data.NILBC}
  {$jml_persenc=$jml_persenc+$data.NILC}
  {$jml_persend=$jml_persend+$data.NILD}
  {$jml_persene=$jml_persene+$data.NILE}
  
  <tr class="ui-widget-content">
    <td><center>{$data.TAHUN}</center></td>
    <td><center>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-content">
    <td align="center"><center><strong>Rerata</strong></center></td>
    <td><center>{round($jml_persena/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenab/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenb/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenbc/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenc/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persend/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persene/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="8">
			<div class="center_title_bar">Profil Distribusi Nilai Kuliah dan Praktikum per Tahun Akademik (Program S2)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="8"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>
<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td rowspan="2" align="center"><center><strong>Tahun Akademik</strong></center></td>
    <td colspan="7" align="center"><center><strong>% Distribusi Rata - rata Nilai Kuliah dan Praktikum</strong></center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center><strong>A</strong></center></td>
    <td align="center"><center><strong>AB</strong></center></td>
    <td align="center"><center><strong>B</strong></center></td>
    <td align="center"><center><strong>BC</strong></center></td>
    <td align="center"><center><strong>C</strong></center></td>
    <td align="center"><center><strong>D</strong></center></td>
    <td align="center"><center><strong>E</strong></center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center><strong>1</strong></center></td>
    <td align="center"><center><strong>2</strong></center></td>
    <td align="center"><center><strong>3</strong></center></td>
    <td align="center"><center><strong>4</strong></center></td>
    <td align="center"><center><strong>5</strong></center></td>
    <td align="center"><center><strong>6</strong></center></td>
    <td align="center"><center><strong>7</strong></center></td>
    <td align="center"><center><strong>8</strong></center></td>
  </tr>

	{$jml_persena = 0}
	{$jml_persenab = 0}
	{$jml_persenb = 0}
	{$jml_persenbc = 0}
	{$jml_persenc = 0}
	{$jml_persend = 0}
	{$jml_persene = 0}
	
 {foreach $disnil_S2 as $data}
  {$jml_persena=$jml_persena+$data.NILA}
  {$jml_persenab=$jml_persenab+$data.NILAB}
  {$jml_persenb=$jml_persenb+$data.NILB}
  {$jml_persenbc=$jml_persenbc+$data.NILBC}
  {$jml_persenc=$jml_persenc+$data.NILC}
  {$jml_persend=$jml_persend+$data.NILD}
  {$jml_persene=$jml_persene+$data.NILE}
  
  <tr class="ui-widget-content">
    <td><center>{$data.TAHUN}</center></td>
    <td><center>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-content">
    <td align="center"><center><strong>Rerata</strong></center></td>
    <td><center>{round($jml_persena/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenab/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenb/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenbc/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenc/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persend/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persene/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="8">
			<div class="center_title_bar">Profil Distribusi Nilai Kuliah dan Praktikum per Tahun Akademik (Program S3)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="8"><div style="padding:10px;">Universitas</div></td>
		</tr>
</table>
<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td rowspan="2" align="center"><center><strong>Tahun Akademik</strong></center></td>
    <td colspan="7" align="center"><center><strong>% Distribusi Rata - rata Nilai Kuliah dan Praktikum</strong></center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center><strong>A</strong></center></td>
    <td align="center"><center><strong>AB</strong></center></td>
    <td align="center"><center><strong>B</strong></center></td>
    <td align="center"><center><strong>BC</strong></center></td>
    <td align="center"><center><strong>C</strong></center></td>
    <td align="center"><center><strong>D</strong></center></td>
    <td align="center"><center><strong>E</strong></center></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center"><center><strong>1</strong></center></td>
    <td align="center"><center><strong>2</strong></center></td>
    <td align="center"><center><strong>3</strong></center></td>
    <td align="center"><center><strong>4</strong></center></td>
    <td align="center"><center><strong>5</strong></center></td>
    <td align="center"><center><strong>6</strong></center></td>
    <td align="center"><center><strong>7</strong></center></td>
    <td align="center"><center><strong>8</strong></center></td>
  </tr>

	{$jml_persena = 0}
	{$jml_persenab = 0}
	{$jml_persenb = 0}
	{$jml_persenbc = 0}
	{$jml_persenc = 0}
	{$jml_persend = 0}
	{$jml_persene = 0}
	
 {foreach $disnil_S3 as $data}
  {$jml_persena=$jml_persena+$data.NILA}
  {$jml_persenab=$jml_persenab+$data.NILAB}
  {$jml_persenb=$jml_persenb+$data.NILB}
  {$jml_persenbc=$jml_persenbc+$data.NILBC}
  {$jml_persenc=$jml_persenc+$data.NILC}
  {$jml_persend=$jml_persend+$data.NILD}
  {$jml_persene=$jml_persene+$data.NILE}
  
  <tr class="ui-widget-content">
    <td><center>{$data.TAHUN}</center></td>
    <td><center>{round($data.NILA/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILAB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILB/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILBC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILC/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILD/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
    <td><center>{round($data.NILE/($data.NILA+$data.NILAB+$data.NILB+$data.NILBC+$data.NILC+$data.NILD+$data.NILE)*100,2)}</center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-content">
    <td align="center"><center><strong>Rerata</strong></center></td>
    <td><center>{round($jml_persena/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenab/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenb/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenbc/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persenc/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persend/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
    <td><center>{round($jml_persene/($jml_persena+$jml_persenab+$jml_persenb+$jml_persenbc+$jml_persenc+$jml_persend+$jml_persene)*100, 2)}</center></td>
  </tr>
</table>


