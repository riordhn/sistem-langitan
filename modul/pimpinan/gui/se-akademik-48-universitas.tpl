<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Rekapitulasi Evaluasi Kinerja Dosen/Mata Kuliah dan Indeks Kepuasan (IK) Terhadap Proses Pembelajaran(Program D3)</div></div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;"></div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget" style="font-size:11px;">
  <tr class="ui-widget-header">
    <td rowspan="2"><strong><center>Dosen/Mata Kuliah</center></strong></td>
     <td colspan="2"><strong><center>{$tahun-2}/{$tahun-1}</center></strong></td>
    <td colspan="2"><strong><center>{$tahun-1}/{$tahun}</center></strong></td>
    <td colspan="2"><strong><center>{$tahun}/{$tahun+1}</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
    <td><strong><center>5</center></strong></td>
    <td><strong><center>6</center></strong></td>
    <td><strong><center>7</center></strong></td>
  </tr>
  {$jml_2 = 0}
  {$ttl_2 = 0}
  {$jml_3 = 0}
  {$ttl_3 = 0}
  {$jml_4 = 0}
  {$ttl_4 = 0}
  {$jml_5 = 0}
  {$ttl_5 = 0}
  {$jml_6 = 0}
  {$ttl_6 = 0}
  {$jml_7 = 0}
  {$ttl_7 = 0}
  
  {foreach $ikn_D3 as $data}
  <tr class="ui-widget-content">
    <td><strong><left> {$data.NM_MATA_KULIAH} </left></strong></td>
	<td><strong><center>	{foreach $ik_D3 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-2) }
								{$data1.KUL}
								 {$jml_2=$jml_2+1}
								 {$ttl_2=$ttl_2+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
	<td><strong><center>	{foreach $ik_D3 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-2) }
								{$data1.PRK}
								 {$jml_3=$jml_3+1}
								 {$ttl_3=$ttl_3+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
	<td><strong><center>	{foreach $ik_D3 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-1) }
								{$data1.KUL}
								 {$jml_4=$jml_4+1}
								 {$ttl_4=$ttl_4+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>

	<td><strong><center>	{foreach $ik_D3 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-1) }
								{$data1.PRK}
								 {$jml_5=$jml_5+1}
								 {$ttl_5=$ttl_5+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
		<td><strong><center>	{foreach $ik_D3 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun) }
								{$data1.KUL}
								 {$jml_6=$jml_6+1}
								 {$ttl_6=$ttl_6+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>

	<td><strong><center>	{foreach $ik_D3 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun) }
								{$data1.PRK}
								 {$jml_7=$jml_7+1}
								 {$ttl_7=$ttl_7+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-header">
    <td><strong><center>RERATA</center></strong></td>
    <td><strong><center> {round($ttl_2/$jml_2, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_3/$jml_3, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_4/$jml_4, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_5/$jml_5, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_6/$jml_6, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_7/$jml_7, 2)} </center></strong></td>
  </tr>
</table>


<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Rekapitulasi Evaluasi Kinerja Dosen/Mata Kuliah dan Indeks Kepuasan (IK) Terhadap Proses Pembelajaran(Program Sarjana Reguler)</div></div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">  </div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget" style="font-size:11px;">
  <tr class="ui-widget-header">
    <td rowspan="2"><strong><center>Dosen/Mata Kuliah</center></strong></td>
      <td colspan="2"><strong><center>{$tahun-2}/{$tahun-1}</center></strong></td>
    <td colspan="2"><strong><center>{$tahun-1}/{$tahun}</center></strong></td>
    <td colspan="2"><strong><center>{$tahun}/{$tahun+1}</center></strong></td>
</tr>
  <tr class="ui-widget-header">
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
    <td><strong><center>5</center></strong></td>
    <td><strong><center>6</center></strong></td>
    <td><strong><center>7</center></strong></td>
  </tr>
  {$jml_2 = 0}
  {$ttl_2 = 0}
  {$jml_3 = 0}
  {$ttl_3 = 0}
  {$jml_4 = 0}
  {$ttl_4 = 0}
  {$jml_5 = 0}
  {$ttl_5 = 0}
  {$jml_6 = 0}
  {$ttl_6 = 0}
  {$jml_7 = 0}
  {$ttl_7 = 0}
  
  {foreach $ikn_R as $data}
  <tr class="ui-widget-content">
    <td><strong><left> {$data.NM_MATA_KULIAH} </left></strong></td>
	<td><strong><center>	{foreach $ik_R as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-2) }
								{$data1.KUL}
								 {$jml_2=$jml_2+1}
								 {$ttl_2=$ttl_2+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
	<td><strong><center>	{foreach $ik_R as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-2) }
								{$data1.PRK}
								 {$jml_3=$jml_3+1}
								 {$ttl_3=$ttl_3+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
	<td><strong><center>	{foreach $ik_R as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-1) }
								{$data1.KUL}
								 {$jml_4=$jml_4+1}
								 {$ttl_4=$ttl_4+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>

	<td><strong><center>	{foreach $ik_R as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-1) }
								{$data1.PRK}
								 {$jml_5=$jml_5+1}
								 {$ttl_5=$ttl_5+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
		<td><strong><center>	{foreach $ik_R as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun) }
								{$data1.KUL}
								 {$jml_6=$jml_6+1}
								 {$ttl_6=$ttl_6+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>

	<td><strong><center>	{foreach $ik_R as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun) }
								{$data1.PRK}
								 {$jml_7=$jml_7+1}
								 {$ttl_7=$ttl_7+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-header">
    <td><strong><center>RERATA</center></strong></td>
    <td><strong><center> {round($ttl_2/$jml_2, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_3/$jml_3, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_4/$jml_4, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_5/$jml_5, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_6/$jml_6, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_7/$jml_7, 2)} </center></strong></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Rekapitulasi Evaluasi Kinerja Dosen/Mata Kuliah dan Indeks Kepuasan (IK) Terhadap Proses Pembelajaran(Alih Jenis)</div></div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">  </div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget" style="font-size:11px;">
  <tr class="ui-widget-header">
    <td rowspan="2"><strong><center>Dosen/Mata Kuliah</center></strong></td>
    <td colspan="2"><strong><center>{$tahun-2}/{$tahun-1}</center></strong></td>
    <td colspan="2"><strong><center>{$tahun-1}/{$tahun}</center></strong></td>
    <td colspan="2"><strong><center>{$tahun}/{$tahun+1}</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
    <td><strong><center>5</center></strong></td>
    <td><strong><center>6</center></strong></td>
    <td><strong><center>7</center></strong></td>
  </tr>
  {$jml_2 = 0}
  {$ttl_2 = 0}
  {$jml_3 = 0}
  {$ttl_3 = 0}
  {$jml_4 = 0}
  {$ttl_4 = 0}
  {$jml_5 = 0}
  {$ttl_5 = 0}
  {$jml_6 = 0}
  {$ttl_6 = 0}
  {$jml_7 = 0}
  {$ttl_7 = 0}
  
  {foreach $ikn_AJ as $data}
  <tr class="ui-widget-content">
    <td><strong><left> {$data.NM_MATA_KULIAH} </left></strong></td>
	<td><strong><center>	{foreach $ik_AJ as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-2) }
								{$data1.KUL}
								 {$jml_2=$jml_2+1}
								 {$ttl_2=$ttl_2+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
	<td><strong><center>	{foreach $ik_AJ as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-2) }
								{$data1.PRK}
								 {$jml_3=$jml_3+1}
								 {$ttl_3=$ttl_3+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
	<td><strong><center>	{foreach $ik_AJ as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-1) }
								{$data1.KUL}
								 {$jml_4=$jml_4+1}
								 {$ttl_4=$ttl_4+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>

	<td><strong><center>	{foreach $ik_AJ as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-1) }
								{$data1.PRK}
								 {$jml_5=$jml_5+1}
								 {$ttl_5=$ttl_5+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
		<td><strong><center>	{foreach $ik_AJ as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun) }
								{$data1.KUL}
								 {$jml_6=$jml_6+1}
								 {$ttl_6=$ttl_6+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>

	<td><strong><center>	{foreach $ik_AJ as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun) }
								{$data1.PRK}
								 {$jml_7=$jml_7+1}
								 {$ttl_7=$ttl_7+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-header">
    <td><strong><center>RERATA</center></strong></td>
    <td><strong><center> {round($ttl_2/$jml_2, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_3/$jml_3, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_4/$jml_4, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_5/$jml_5, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_6/$jml_6, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_7/$jml_7, 2)} </center></strong></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Rekapitulasi Evaluasi Kinerja Dosen/Mata Kuliah dan Indeks Kepuasan (IK) Terhadap Proses Pembelajaran(Program Profesi)</div></div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">  </div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget" style="font-size:11px;">
  <tr class="ui-widget-header">
    <td rowspan="2"><strong><center>Dosen/Mata Kuliah</center></strong></td>
    <td colspan="2"><strong><center>{$tahun-2}/{$tahun-1}</center></strong></td>
    <td colspan="2"><strong><center>{$tahun-1}/{$tahun}</center></strong></td>
    <td colspan="2"><strong><center>{$tahun}/{$tahun+1}</center></strong></td>
</tr>
  <tr class="ui-widget-header">
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
    <td><strong><center>5</center></strong></td>
    <td><strong><center>6</center></strong></td>
    <td><strong><center>7</center></strong></td>
  </tr>
  {$jml_2 = 0}
  {$ttl_2 = 0}
  {$jml_3 = 0}
  {$ttl_3 = 0}
  {$jml_4 = 0}
  {$ttl_4 = 0}
  {$jml_5 = 0}
  {$ttl_5 = 0}
  {$jml_6 = 0}
  {$ttl_6 = 0}
  {$jml_7 = 0}
  {$ttl_7 = 0}
  
  {foreach $ikn_PROFESI as $data}
  <tr class="ui-widget-content">
    <td><strong><left> {$data.NM_MATA_KULIAH} </left></strong></td>
	<td><strong><center>	{foreach $ik_PROFESI as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-2) }
								{$data1.KUL}
								 {$jml_2=$jml_2+1}
								 {$ttl_2=$ttl_2+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
	<td><strong><center>	{foreach $ik_PROFESI as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-2) }
								{$data1.PRK}
								 {$jml_3=$jml_3+1}
								 {$ttl_3=$ttl_3+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
	<td><strong><center>	{foreach $ik_PROFESI as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-1) }
								{$data1.KUL}
								 {$jml_4=$jml_4+1}
								 {$ttl_4=$ttl_4+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>

	<td><strong><center>	{foreach $ik_PROFESI as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-1) }
								{$data1.PRK}
								 {$jml_5=$jml_5+1}
								 {$ttl_5=$ttl_5+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
		<td><strong><center>	{foreach $ik_PROFESI as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun) }
								{$data1.KUL}
								 {$jml_6=$jml_6+1}
								 {$ttl_6=$ttl_6+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>

	<td><strong><center>	{foreach $ik_PROFESI as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun) }
								{$data1.PRK}
								 {$jml_7=$jml_7+1}
								 {$ttl_7=$ttl_7+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-header">
    <td><strong><center>RERATA</center></strong></td>
    <td><strong><center> {round($ttl_2/$jml_2, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_3/$jml_3, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_4/$jml_4, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_5/$jml_5, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_6/$jml_6, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_7/$jml_7, 2)} </center></strong></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Rekapitulasi Evaluasi Kinerja Dosen/Mata Kuliah dan Indeks Kepuasan (IK) Terhadap Proses Pembelajaran(Program S2)</div></div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">  </div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget" style="font-size:11px;">
  <tr class="ui-widget-header">
    <td rowspan="2"><strong><center>Dosen/Mata Kuliah</center></strong></td>
    <td colspan="2"><strong><center>{$tahun-2}/{$tahun-1}</center></strong></td>
    <td colspan="2"><strong><center>{$tahun-1}/{$tahun}</center></strong></td>
    <td colspan="2"><strong><center>{$tahun}/{$tahun+1}</center></strong></td>
</tr>
  <tr class="ui-widget-header">
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
    <td><strong><center>5</center></strong></td>
    <td><strong><center>6</center></strong></td>
    <td><strong><center>7</center></strong></td>
  </tr>
  {$jml_2 = 0}
  {$ttl_2 = 0}
  {$jml_3 = 0}
  {$ttl_3 = 0}
  {$jml_4 = 0}
  {$ttl_4 = 0}
  {$jml_5 = 0}
  {$ttl_5 = 0}
  {$jml_6 = 0}
  {$ttl_6 = 0}
  {$jml_7 = 0}
  {$ttl_7 = 0}
  
  {foreach $ikn_S2 as $data}
  <tr class="ui-widget-content">
    <td><strong><left> {$data.NM_MATA_KULIAH} </left></strong></td>
	<td><strong><center>	{foreach $ik_S2 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-2) }
								{$data1.KUL}
								 {$jml_2=$jml_2+1}
								 {$ttl_2=$ttl_2+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
	<td><strong><center>	{foreach $ik_S2 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-2) }
								{$data1.PRK}
								 {$jml_3=$jml_3+1}
								 {$ttl_3=$ttl_3+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
	<td><strong><center>	{foreach $ik_S2 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-1) }
								{$data1.KUL}
								 {$jml_4=$jml_4+1}
								 {$ttl_4=$ttl_4+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>

	<td><strong><center>	{foreach $ik_S2 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-1) }
								{$data1.PRK}
								 {$jml_5=$jml_5+1}
								 {$ttl_5=$ttl_5+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
		<td><strong><center>	{foreach $ik_S2 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun) }
								{$data1.KUL}
								 {$jml_6=$jml_6+1}
								 {$ttl_6=$ttl_6+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>

	<td><strong><center>	{foreach $ik_S2 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun) }
								{$data1.PRK}
								 {$jml_7=$jml_7+1}
								 {$ttl_7=$ttl_7+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-header">
    <td><strong><center>RERATA</center></strong></td>
    <td><strong><center> {round($ttl_2/$jml_2, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_3/$jml_3, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_4/$jml_4, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_5/$jml_5, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_6/$jml_6, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_7/$jml_7, 2)} </center></strong></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Rekapitulasi Evaluasi Kinerja Dosen/Mata Kuliah dan Indeks Kepuasan (IK) Terhadap Proses Pembelajaran(Program S3)</div></div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">  </div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget" style="font-size:11px;">
  <tr class="ui-widget-header">
    <td rowspan="2"><strong><center>Dosen/Mata Kuliah</center></strong></td>
    <td colspan="2"><strong><center>{$tahun-2}/{$tahun-1}</center></strong></td>
    <td colspan="2"><strong><center>{$tahun-1}/{$tahun}</center></strong></td>
    <td colspan="2"><strong><center>{$tahun}/{$tahun+1}</center></strong></td>
 </tr>
  <tr class="ui-widget-header">
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
    <td><strong><center>IK KULIAH</center></strong></td>
    <td><strong><center>IK PRAKTIKUM</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
    <td><strong><center>5</center></strong></td>
    <td><strong><center>6</center></strong></td>
    <td><strong><center>7</center></strong></td>
  </tr>
  {$jml_2 = 0}
  {$ttl_2 = 0}
  {$jml_3 = 0}
  {$ttl_3 = 0}
  {$jml_4 = 0}
  {$ttl_4 = 0}
  {$jml_5 = 0}
  {$ttl_5 = 0}
  {$jml_6 = 0}
  {$ttl_6 = 0}
  {$jml_7 = 0}
  {$ttl_7 = 0}
  
  {foreach $ikn_S3 as $data}
  <tr class="ui-widget-content">
    <td><strong><left> {$data.NM_MATA_KULIAH} </left></strong></td>
	<td><strong><center>	{foreach $ik_S3 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-2) }
								{$data1.KUL}
								 {$jml_2=$jml_2+1}
								 {$ttl_2=$ttl_2+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
	<td><strong><center>	{foreach $ik_S3 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-2) }
								{$data1.PRK}
								 {$jml_3=$jml_3+1}
								 {$ttl_3=$ttl_3+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
	<td><strong><center>	{foreach $ik_S3 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-1) }
								{$data1.KUL}
								 {$jml_4=$jml_4+1}
								 {$ttl_4=$ttl_4+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>

	<td><strong><center>	{foreach $ik_S3 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun-1) }
								{$data1.PRK}
								 {$jml_5=$jml_5+1}
								 {$ttl_5=$ttl_5+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
		<td><strong><center>	{foreach $ik_S3 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun) }
								{$data1.KUL}
								 {$jml_6=$jml_6+1}
								 {$ttl_6=$ttl_6+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>

	<td><strong><center>	{foreach $ik_S3 as $data1}
								{if ($data.NM_MATA_KULIAH==$data1.NM_MATA_KULIAH) and ($data1.TAHUN==$tahun) }
								{$data1.PRK}
								 {$jml_7=$jml_7+1}
								 {$ttl_7=$ttl_7+$data1.KUL}
								{/if}
							{/foreach}
	</center></strong></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-header">
    <td><strong><center>RERATA</center></strong></td>
    <td><strong><center> {round($ttl_2/$jml_2, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_3/$jml_3, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_4/$jml_4, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_5/$jml_5, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_6/$jml_6, 2)} </center></strong></td>
    <td><strong><center> {round($ttl_7/$jml_7, 2)} </center></strong></td>
  </tr>
</table>
