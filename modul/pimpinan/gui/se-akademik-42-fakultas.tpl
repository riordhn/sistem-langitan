
{if count($ipk_D3) > 0}
<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Profil IPK Mahasiswa Aktif Pada Tahun Akademik {$tahun}/{$tahun+1} (Program Diploma)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Fakultas {$fakultas}</div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>IPK &lt; 2</center></td>
        <td colspan="2"><center>2 &lt;= IPK &lt; 2,5</center></td>
        <td colspan="2"><center>2,5 &lt;= IPK &lt; 2,75</center></td>
        <td colspan="2"><center>2,75 &lt;= IPK &lt; 3,0</center></td>
        <td colspan="2"><center>3,0 &lt;= IPK &lt; 3,5</center></td>
        <td colspan="2"><center>IPK &gt;= 3,5</center></td>
        <td rowspan="2"><center>Jumlah Per Angkatan</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
        <td><center>14</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
    {foreach $ipk_D3 as $data}
        {$jml = $data.JUMLAH_2 + $data.JUMLAH_4 + $data.JUMLAH_6 + $data.JUMLAH_8 + $data.JUMLAH_10 + $data.JUMLAH_12}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.JUMLAH_2}
        {$jml_4 = $jml_4 + $data.JUMLAH_4}
        {$jml_6 = $jml_6 + $data.JUMLAH_6}
        {$jml_8 = $jml_8 + $data.JUMLAH_8}
        {$jml_10 = $jml_10 + $data.JUMLAH_10}
		{$jml_12 = $jml_12 + $data.JUMLAH_12}
        {$rata_2 = $rata_2 + ($data.JUMLAH_2/$jml*100)}
        {$rata_4 = $rata_4 + ($data.JUMLAH_4/$jml*100)}
        {$rata_6 = $rata_6 + ($data.JUMLAH_6/$jml*100)}
        {$rata_8 = $rata_8 + ($data.JUMLAH_8/$jml*100)}
        {$rata_10 = $rata_10 + ($data.JUMLAH_10/$jml*100)}
		{$rata_12 = $rata_12 + ($data.JUMLAH_12/$jml*100)}
        {$row = $row+1}
        <tr>
            <td><center>{$data.TAHUN_ANGKATAN}</center></td>
            <td><center>{$data.JUMLAH_2}</center></td>
            <td><center>{($data.JUMLAH_2/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_4}</center></td>
            <td><center>{($data.JUMLAH_4/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_6}</center></td>
            <td><center>{($data.JUMLAH_6/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_8}</center></td>
            <td><center>{($data.JUMLAH_8/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_10}</center></td>
            <td><center>{($data.JUMLAH_10/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_12}</center></td>
            <td><center>{($data.JUMLAH_12/$jml*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_2/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_4/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_6/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_8/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_10/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_12/$jml_total)*100)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
</table>
{/if}


{if count($ipk_R) > 0}
<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Profil IPK Mahasiswa Aktif Pada Tahun Akademik {$tahun}/{$tahun+1} (Program S1-Reguler)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$fakultas}</div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>IPK &lt; 2</center></td>
        <td colspan="2"><center>2 &lt;= IPK &lt; 2,5</center></td>
        <td colspan="2"><center>2,5 &lt;= IPK &lt; 2,75</center></td>
        <td colspan="2"><center>2,75 &lt;= IPK &lt; 3,0</center></td>
        <td colspan="2"><center>3,0 &lt;= IPK &lt; 3,5</center></td>
        <td colspan="2"><center>IPK &gt;= 3,5</center></td>
        <td rowspan="2"><center>Jumlah Per Angkatan</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
        <td><center>14</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
    {foreach $ipk_R as $data}
        {$jml = $data.JUMLAH_2 + $data.JUMLAH_4 + $data.JUMLAH_6 + $data.JUMLAH_8 + $data.JUMLAH_10 + $data.JUMLAH_12}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.JUMLAH_2}
        {$jml_4 = $jml_4 + $data.JUMLAH_4}
        {$jml_6 = $jml_6 + $data.JUMLAH_6}
        {$jml_8 = $jml_8 + $data.JUMLAH_8}
        {$jml_10 = $jml_10 + $data.JUMLAH_10}
		{$jml_12 = $jml_12 + $data.JUMLAH_12}
        {$rata_2 = $rata_2 + ($data.JUMLAH_2/$jml*100)}
        {$rata_4 = $rata_4 + ($data.JUMLAH_4/$jml*100)}
        {$rata_6 = $rata_6 + ($data.JUMLAH_6/$jml*100)}
        {$rata_8 = $rata_8 + ($data.JUMLAH_8/$jml*100)}
        {$rata_10 = $rata_10 + ($data.JUMLAH_10/$jml*100)}
		{$rata_12 = $rata_12 + ($data.JUMLAH_12/$jml*100)}
        {$row = $row+1}
        <tr>
            <td><center>{$data.TAHUN_ANGKATAN}</center></td>
            <td><center>{$data.JUMLAH_2}</center></td>
            <td><center>{($data.JUMLAH_2/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_4}</center></td>
            <td><center>{($data.JUMLAH_4/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_6}</center></td>
            <td><center>{($data.JUMLAH_6/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_8}</center></td>
            <td><center>{($data.JUMLAH_8/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_10}</center></td>
            <td><center>{($data.JUMLAH_10/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_12}</center></td>
            <td><center>{($data.JUMLAH_12/$jml*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_2/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_4/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_6/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_8/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_10/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_12/$jml_total)*100)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
</table>
{/if}


{if count($ipk_AJ) > 0}
<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Profil IPK Mahasiswa Aktif Pada Tahun Akademik {$tahun}/{$tahun+1} (Program S1-Alih Jenis)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Fakultas {$fakultas}</div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>IPK &lt; 2</center></td>
        <td colspan="2"><center>2 &lt;= IPK &lt; 2,5</center></td>
        <td colspan="2"><center>2,5 &lt;= IPK &lt; 2,75</center></td>
        <td colspan="2"><center>2,75 &lt;= IPK &lt; 3,0</center></td>
        <td colspan="2"><center>3,0 &lt;= IPK &lt; 3,5</center></td>
        <td colspan="2"><center>IPK &gt;= 3,5</center></td>
        <td rowspan="2"><center>Jumlah Per Angkatan</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
        <td><center>14</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
    {foreach $ipk_AJ as $data}
        {$jml = $data.JUMLAH_2 + $data.JUMLAH_4 + $data.JUMLAH_6 + $data.JUMLAH_8 + $data.JUMLAH_10 + $data.JUMLAH_12}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.JUMLAH_2}
        {$jml_4 = $jml_4 + $data.JUMLAH_4}
        {$jml_6 = $jml_6 + $data.JUMLAH_6}
        {$jml_8 = $jml_8 + $data.JUMLAH_8}
        {$jml_10 = $jml_10 + $data.JUMLAH_10}
		{$jml_12 = $jml_12 + $data.JUMLAH_12}
        {$rata_2 = $rata_2 + ($data.JUMLAH_2/$jml*100)}
        {$rata_4 = $rata_4 + ($data.JUMLAH_4/$jml*100)}
        {$rata_6 = $rata_6 + ($data.JUMLAH_6/$jml*100)}
        {$rata_8 = $rata_8 + ($data.JUMLAH_8/$jml*100)}
        {$rata_10 = $rata_10 + ($data.JUMLAH_10/$jml*100)}
		{$rata_12 = $rata_12 + ($data.JUMLAH_12/$jml*100)}
        {$row = $row+1}
        <tr>
            <td><center>{$data.TAHUN_ANGKATAN}</center></td>
            <td><center>{$data.JUMLAH_2}</center></td>
            <td><center>{($data.JUMLAH_2/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_4}</center></td>
            <td><center>{($data.JUMLAH_4/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_6}</center></td>
            <td><center>{($data.JUMLAH_6/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_8}</center></td>
            <td><center>{($data.JUMLAH_8/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_10}</center></td>
            <td><center>{($data.JUMLAH_10/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_12}</center></td>
            <td><center>{($data.JUMLAH_12/$jml*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_2/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_4/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_6/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_8/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_10/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_12/$jml_total)*100)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
</table>

{/if}


{if count($ipk_S2) > 0}
<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Profil IPK Mahasiswa Aktif Pada Tahun Akademik {$tahun}/{$tahun+1} (Program S2)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Fakultas {$fakultas}</div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>IPK &lt; 2,75</center></td>
        <td colspan="2"><center>2,75 &lt;= IPK &lt; 3</center></td>
        <td colspan="2"><center>3 &lt;= IPK &lt; 3,5</center></td>
        <td colspan="2"><center>3,5 &lt;= IPK &lt; 3,75</center></td>
        <td colspan="2"><center>IPK &gt;= 3,75</center></td>
        <td rowspan="2"><center>Jumlah Per Angkatan</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
    {foreach $ipk_S2 as $data}
        {$jml = $data.JUMLAH_2 + $data.JUMLAH_4 + $data.JUMLAH_6 + $data.JUMLAH_8 + $data.JUMLAH_10}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.JUMLAH_2}
        {$jml_4 = $jml_4 + $data.JUMLAH_4}
        {$jml_6 = $jml_6 + $data.JUMLAH_6}
        {$jml_8 = $jml_8 + $data.JUMLAH_8}
        {$jml_10 = $jml_10 + $data.JUMLAH_10}
		{$jml_12 = $jml_12 + $data.JUMLAH_12}
        {$rata_2 = $rata_2 + ($data.JUMLAH_2/$jml*100)}
        {$rata_4 = $rata_4 + ($data.JUMLAH_4/$jml*100)}
        {$rata_6 = $rata_6 + ($data.JUMLAH_6/$jml*100)}
        {$rata_8 = $rata_8 + ($data.JUMLAH_8/$jml*100)}
        {$rata_10 = $rata_10 + ($data.JUMLAH_10/$jml*100)}
		{$rata_12 = $rata_12 + ($data.JUMLAH_12/$jml*100)}
        {$row = $row+1}
        <tr>
            <td><center>{$data.TAHUN_ANGKATAN}</center></td>
            <td><center>{$data.JUMLAH_2}</center></td>
            <td><center>{($data.JUMLAH_2/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_4}</center></td>
            <td><center>{($data.JUMLAH_4/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_6}</center></td>
            <td><center>{($data.JUMLAH_6/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_8}</center></td>
            <td><center>{($data.JUMLAH_8/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_10}</center></td>
            <td><center>{($data.JUMLAH_10/$jml*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_2/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_4/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_6/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_8/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_10/$jml_total)*100)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
</table>
{/if}



{if count($ipk_S3) > 0}
<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Profil IPK Mahasiswa Aktif Pada Tahun Akademik {$tahun}/{$tahun+1} (Program S3)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Fakultas {$fakultas}</div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>IPK &lt; 2,75</center></td>
        <td colspan="2"><center>2,75 &lt;= IPK &lt; 3</center></td>
        <td colspan="2"><center>3 &lt;= IPK &lt; 3,5</center></td>
        <td colspan="2"><center>3,5 &lt;= IPK &lt; 3,75</center></td>
        <td colspan="2"><center>IPK &gt;= 3,75</center></td>
        <td rowspan="2"><center>Jumlah Per Angkatan</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
    {foreach $ipk_S3 as $data}
        {$jml = $data.JUMLAH_2 + $data.JUMLAH_4 + $data.JUMLAH_6 + $data.JUMLAH_8 + $data.JUMLAH_10}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.JUMLAH_2}
        {$jml_4 = $jml_4 + $data.JUMLAH_4}
        {$jml_6 = $jml_6 + $data.JUMLAH_6}
        {$jml_8 = $jml_8 + $data.JUMLAH_8}
        {$jml_10 = $jml_10 + $data.JUMLAH_10}
		{$jml_12 = $jml_12 + $data.JUMLAH_12}
        {$rata_2 = $rata_2 + ($data.JUMLAH_2/$jml*100)}
        {$rata_4 = $rata_4 + ($data.JUMLAH_4/$jml*100)}
        {$rata_6 = $rata_6 + ($data.JUMLAH_6/$jml*100)}
        {$rata_8 = $rata_8 + ($data.JUMLAH_8/$jml*100)}
        {$rata_10 = $rata_10 + ($data.JUMLAH_10/$jml*100)}
		{$rata_12 = $rata_12 + ($data.JUMLAH_12/$jml*100)}
        {$row = $row+1}
        <tr>
            <td><center>{$data.TAHUN_ANGKATAN}</center></td>
            <td><center>{$data.JUMLAH_2}</center></td>
            <td><center>{($data.JUMLAH_2/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_4}</center></td>
            <td><center>{($data.JUMLAH_4/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_6}</center></td>
            <td><center>{($data.JUMLAH_6/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_8}</center></td>
            <td><center>{($data.JUMLAH_8/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_10}</center></td>
            <td><center>{($data.JUMLAH_10/$jml*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_2/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_4/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_6/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_8/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_10/$jml_total)*100)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
</table>
{/if}

{if count($ipk_PROFESI) > 0}
<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Profil IPK Mahasiswa Aktif Pada Tahun Akademik {$tahun}/{$tahun+1} (Program Profesi)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Fakultas {$fakultas}</div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>IPK &lt; 2</center></td>
        <td colspan="2"><center>2 &lt;= IPK &lt; 2,5</center></td>
        <td colspan="2"><center>2,5 &lt;= IPK &lt; 2,75</center></td>
        <td colspan="2"><center>2,75 &lt;= IPK &lt; 3,0</center></td>
        <td colspan="2"><center>3,0 &lt;= IPK &lt; 3,5</center></td>
        <td colspan="2"><center>IPK &gt;= 3,5</center></td>
        <td rowspan="2"><center>Jumlah Per Angkatan</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
        <td><center>14</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
	{$jml_12 = 0}
    {$rata_2 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
	{$rata_12 = 0}
    {$row = 0}
    {foreach $ipk_PROFESI as $data}
        {$jml = $data.JUMLAH_2 + $data.JUMLAH_4 + $data.JUMLAH_6 + $data.JUMLAH_8 + $data.JUMLAH_10 + $data.JUMLAH_12}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.JUMLAH_2}
        {$jml_4 = $jml_4 + $data.JUMLAH_4}
        {$jml_6 = $jml_6 + $data.JUMLAH_6}
        {$jml_8 = $jml_8 + $data.JUMLAH_8}
        {$jml_10 = $jml_10 + $data.JUMLAH_10}
		{$jml_12 = $jml_12 + $data.JUMLAH_12}
        {$rata_2 = $rata_2 + ($data.JUMLAH_2/$jml*100)}
        {$rata_4 = $rata_4 + ($data.JUMLAH_4/$jml*100)}
        {$rata_6 = $rata_6 + ($data.JUMLAH_6/$jml*100)}
        {$rata_8 = $rata_8 + ($data.JUMLAH_8/$jml*100)}
        {$rata_10 = $rata_10 + ($data.JUMLAH_10/$jml*100)}
		{$rata_12 = $rata_12 + ($data.JUMLAH_12/$jml*100)}
        {$row = $row+1}
        <tr>
            <td><center>{$data.TAHUN_ANGKATAN}</center></td>
            <td><center>{$data.JUMLAH_2}</center></td>
            <td><center>{($data.JUMLAH_2/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_4}</center></td>
            <td><center>{($data.JUMLAH_4/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_6}</center></td>
            <td><center>{($data.JUMLAH_6/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_8}</center></td>
            <td><center>{($data.JUMLAH_8/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_10}</center></td>
            <td><center>{($data.JUMLAH_10/$jml*100)|round:2}</center></td>
            <td><center>{$data.JUMLAH_12}</center></td>
            <td><center>{($data.JUMLAH_12/$jml*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_12}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_2/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_4/$jml_total)*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{(($jml_6/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_8/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_10/$jml_total)*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{(($jml_12/$jml_total)*100)|round:2}</center></td>
      	<td bgcolor="#333333"><center></center></td>
	</tr>
</table>

{/if}
