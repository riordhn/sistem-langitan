<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="4">
			<div class="center_title_bar">Jumlah Mahasiswa Skripsi Terlibat Penelitian Dosen per Tahun (Program Diploma 3)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="4"><div style="padding:10px;">Fakultas {$fakultas}</div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td><strong><center>Tahun Akademik</center></strong></td>
    <td><strong><center>Jumlah Mahasiswa Skripsi Terlibat Penelitian Dosen</center></strong></td>
    <td><strong><center>Jumlah Mahasiswa Skripsi</center></strong></td>
    <td><strong><center>Persentase</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
  </tr>
  
  {$jml_row_skripsi = 0}
  {$jml_skripsi = 0}
  
  {foreach $skripsi_D3 as $data}
  {$jml_skripsi = $jml_skripsi + $data.SKRIPSI}
  <tr class="ui-widget-content">
    <td><center>{$data.TAHUN}</center></td>
    <td><center> - </center></td>
    <td><center>{$data.SKRIPSI}</center></td>
    <td><center> - </center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-content">
    <td><strong><center>Jumlah</center></strong></td>
    <td><center> 0 </center></td>
    <td><center>{$jml_skripsi}</center></td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr class="ui-widget-content">
    <td><center><strong>Rerata</strong></center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center> - </center></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="4">
			<div class="center_title_bar">Jumlah Mahasiswa Skripsi Terlibat Penelitian Dosen per Tahun (Program Sarjana Reguler)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="4"><div style="padding:10px;">Fakultas {$fakultas}</div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td><strong><center>Tahun Akademik</center></strong></td>
    <td><strong><center>Jumlah Mahasiswa Skripsi Terlibat Penelitian Dosen</center></strong></td>
    <td><strong><center>Jumlah Mahasiswa Skripsi</center></strong></td>
    <td><strong><center>Persentase</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
  </tr>
  
  {$jml_row_skripsi = 0}
  {$jml_skripsi = 0}
  
  {foreach $skripsi_R as $data}
  {$jml_skripsi = $jml_skripsi + $data.SKRIPSI}
  <tr class="ui-widget-content">
    <td><center>{$data.TAHUN}</center></td>
    <td><center> - </center></td>
    <td><center>{$data.SKRIPSI}</center></td>
    <td><center> - </center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-content">
    <td><strong><center>Jumlah</center></strong></td>
    <td><center> 0 </center></td>
    <td><center>{$jml_skripsi}</center></td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr class="ui-widget-content">
    <td><center><strong>Rerata</strong></center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center> - </center></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="4">
			<div class="center_title_bar">Jumlah Mahasiswa Skripsi Terlibat Penelitian Dosen per Tahun (Program Alih Jenis)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="4"><div style="padding:10px;">Fakultas {$fakultas}</div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td><strong><center>Tahun Akademik</center></strong></td>
    <td><strong><center>Jumlah Mahasiswa Skripsi Terlibat Penelitian Dosen</center></strong></td>
    <td><strong><center>Jumlah Mahasiswa Skripsi</center></strong></td>
    <td><strong><center>Persentase</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
  </tr>
  
  {$jml_row_skripsi = 0}
  {$jml_skripsi = 0}
  
  {foreach $skripsi_AJ as $data}
  {$jml_skripsi = $jml_skripsi + $data.SKRIPSI}
  <tr class="ui-widget-content">
    <td><center>{$data.TAHUN}</center></td>
    <td><center> - </center></td>
    <td><center>{$data.SKRIPSI}</center></td>
    <td><center> - </center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-content">
    <td><strong><center>Jumlah</center></strong></td>
    <td><center> 0 </center></td>
    <td><center>{$jml_skripsi}</center></td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr class="ui-widget-content">
    <td><center><strong>Rerata</strong></center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center> - </center></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="4">
			<div class="center_title_bar">Jumlah Mahasiswa Skripsi Terlibat Penelitian Dosen per Tahun (Program Profesi)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="4"><div style="padding:10px;">Fakultas {$fakultas}</div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td><strong><center>Tahun Akademik</center></strong></td>
    <td><strong><center>Jumlah Mahasiswa Skripsi Terlibat Penelitian Dosen</center></strong></td>
    <td><strong><center>Jumlah Mahasiswa Skripsi</center></strong></td>
    <td><strong><center>Persentase</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
  </tr>
  
  {$jml_row_skripsi = 0}
  {$jml_skripsi = 0}
  
  {foreach $skripsi_PROFESI as $data}
  {$jml_skripsi = $jml_skripsi + $data.SKRIPSI}
  <tr class="ui-widget-content">
    <td><center>{$data.TAHUN}</center></td>
    <td><center> - </center></td>
    <td><center>{$data.SKRIPSI}</center></td>
    <td><center> - </center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-content">
    <td><strong><center>Jumlah</center></strong></td>
    <td><center> 0 </center></td>
    <td><center>{$jml_skripsi}</center></td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr class="ui-widget-content">
    <td><center><strong>Rerata</strong></center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center> - </center></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="4">
			<div class="center_title_bar">Jumlah Mahasiswa Tesis Terintegrasi Penelitian Pembimbing per Tahun (Program S2)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="4"><div style="padding:10px;">Fakultas {$fakultas}</div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td><strong><center>Tahun Akademik</center></strong></td>
    <td><strong><center>Jumlah Mahasiswa Tesis Terintegrasi Penelitian Pembimbing</center></strong></td>
    <td><strong><center>Jumlah Mahasiswa Tesis</center></strong></td>
    <td><strong><center>Persentase</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
  </tr>
  
  {$jml_row_skripsi = 0}
  {$jml_skripsi = 0}
  
  {foreach $skripsi_S2 as $data}
  {$jml_skripsi = $jml_skripsi + $data.SKRIPSI}
  <tr class="ui-widget-content">
    <td><center>{$data.TAHUN}</center></td>
    <td><center> - </center></td>
    <td><center>{$data.SKRIPSI}</center></td>
    <td><center> - </center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-content">
    <td><strong><center>Jumlah</center></strong></td>
    <td><center> 0 </center></td>
    <td><center>{$jml_skripsi}</center></td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr class="ui-widget-content">
    <td><center><strong>Rerata</strong></center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center> - </center></td>
  </tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="4">
			<div class="center_title_bar">Jumlah Mahasiswa Disertasi Terintegrasi Penelitian Pembimbing per Tahun (Program S3)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="4"><div style="padding:10px;">Fakultas {$fakultas}</div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td><strong><center>Tahun Akademik</center></strong></td>
    <td><strong><center>Jumlah Mahasiswa Disertasi Terintegrasi Penelitian Pembimbing</center></strong></td>
    <td><strong><center>Jumlah Mahasiswa Disertasi</center></strong></td>
    <td><strong><center>Persentase</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td><strong><center>1</center></strong></td>
    <td><strong><center>2</center></strong></td>
    <td><strong><center>3</center></strong></td>
    <td><strong><center>4</center></strong></td>
  </tr>
  
  {$jml_row_skripsi = 0}
  {$jml_skripsi = 0}
  
  {foreach $skripsi_S3 as $data}
  {$jml_skripsi = $jml_skripsi + $data.SKRIPSI}
  <tr class="ui-widget-content">
    <td><center>{$data.TAHUN}</center></td>
    <td><center> - </center></td>
    <td><center>{$data.SKRIPSI}</center></td>
    <td><center> - </center></td>
  </tr>
  {/foreach}
  <tr class="ui-widget-content">
    <td><strong><center>Jumlah</center></strong></td>
    <td><center> 0 </center></td>
    <td><center>{$jml_skripsi}</center></td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr class="ui-widget-content">
    <td><center><strong>Rerata</strong></center></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><center> - </center></td>
  </tr>
</table>