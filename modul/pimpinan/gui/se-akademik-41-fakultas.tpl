{if $status==2}
<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Profil Mahasiswa per Tahun Angkatan Berdasarkan Status Akademik Tahun {$tahun}/{$tahun+1} (D3-Reguler)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Fakultas {$fakultas} </div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>Krs</center></td>
		<td colspan="2"><center>Nokrs</center></td>
        <td colspan="2"><center>Cuti Akademik</center></td>
        <td colspan="2"><center>Mengundurkan Diri</center></td>
        <td colspan="2"><center>Mahasiswa DO</center></td>
        <td colspan="2"><center>Lulus</center></td>
        <td rowspan="2"><center>Jumlah Mahasiswa Per Angkatan</center></td>
        <td rowspan="2"><center>Jumlah Mahasiswa Asing</center></td>
        <td rowspan="2"><center>Sedang Skripsi</center></td>
	</tr>
	
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
		<td><center>%</center></td>
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
	
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
        <td><center>14</center></td>
		<td><center>15</center></td>
        <td><center>16</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
	{$jml_3 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
    {$rata_2 = 0}
	{$rata_3 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
    {$row = 0}
    {$jml_mhs_asing = 0}
    {$jml_mhs_skripsi = 0}

    {foreach $akademik_D3 as $data}
        {$jml = $data.KRS + $data.NOKRS + $data.CUTI + $data.UNDUR + $data.DO + $data.LULUS}
        {$jml_total = $jml_total + $jml}
        {$jml_mhs_asing = $jml_mhs_asing + $data.JUMLAH_13}
        {$jml_mhs_skripsi = $jml_mhs_skripsi + $data.SKRIPSI}
        {$jml_2 = $jml_2 + $data.KRS}
		{$jml_3 = $jml_3 + $data.NOKRS}
        {$jml_4 = $jml_4 + $data.CUTI}
        {$jml_6 = $jml_6 + $data.UNDUR}
        {$jml_8 = $jml_8 + $data.DO}
        {$jml_10 = $jml_10 + $data.LULUS}
        {$rata_2 = $rata_2 + ($data.KRS/$jml*100)}
		{$rata_3 = $rata_3 + ($data.NOKRS/$jml*100)}
        {$rata_4 = $rata_4 + ($data.CUTI/$jml*100)}
        {$rata_6 = $rata_6 + ($data.UNDUR/$jml*100)}
        {$rata_8 = $rata_8 + ($data.DO/$jml*100)}
        {$rata_10 = $rata_10 + ($data.LULUS/$jml*100)}
        {$row = $row+1}
        <tr>
            <td><center>{$data.TAHUN_ANGKATAN}</center></td>
            <td><center>{$data.KRS}</center></td>
            <td><center>{($data.KRS/$jml*100)|round:2}</center></td>
			<td><center>{$data.NOKRS}</center></td>
			<td><center>{($data.NOKRS/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=C&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.CUTI}</a></center></td>
            <td><center>{($data.CUTI/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=U&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.UNDUR}</a></center></td>
            <td><center>{($data.UNDUR/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=DO&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.DO}</a></center></td>
            <td><center>{($data.DO/$jml*100)|round:2}</center></td>
            <td><center>{$data.LULUS}</center></td>
            <td><center>{($data.LULUS/$jml*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.JUMLAH_13}</center></td>
            <td><center>{$data.SKRIPSI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
		<td><center>{$jml_3}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td  bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td><center>{$jml_mhs_asing}</center></td>
      <td><center>{$jml_mhs_skripsi}</center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
		<td><center>{($rata_3/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_4/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_10/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
        <td bgcolor="#333333"><center></center></td>
      <td bgcolor="#333333"><center></center></td>
	</tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Profil Mahasiswa per Tahun Angkatan Berdasarkan Status Akademik Tahun {$tahun}/{$tahun+1} (S1-Reguler)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Fakultas {$fakultas}  </div></td>
		</tr>
</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>Krs</center></td>
		<td colspan="2"><center>Nokrs</center></td>
        <td colspan="2"><center>Cuti Akademik</center></td>
        <td colspan="2"><center>Mengundurkan Diri</center></td>
        <td colspan="2"><center>Mahasiswa DO</center></td>
        <td colspan="2"><center>Lulus</center></td>
        <td rowspan="2"><center>Jumlah Mahasiswa Per Angkatan</center></td>
        <td rowspan="2"><center>Jumlah Mahasiswa Asing</center></td>
        <td rowspan="2"><center>Sedang Skripsi</center></td>
	</tr>
	
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
		<td><center>%</center></td>
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
	
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
        <td><center>14</center></td>
		<td><center>15</center></td>
        <td><center>16</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
	{$jml_3 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
    {$rata_2 = 0}
	{$rata_3 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
    {$row = 0}
    {$jml_mhs_asing = 0}
    {$jml_mhs_skripsi = 0}

    {foreach $akademik_R as $data}
        {$jml = $data.KRS + $data.NOKRS + $data.CUTI + $data.UNDUR + $data.DO + $data.LULUS}
        {$jml_total = $jml_total + $jml}
        {$jml_mhs_asing = $jml_mhs_asing + $data.JUMLAH_13}
        {$jml_mhs_skripsi = $jml_mhs_skripsi + $data.SKRIPSI}
        {$jml_2 = $jml_2 + $data.KRS}
		{$jml_3 = $jml_3 + $data.NOKRS}
        {$jml_4 = $jml_4 + $data.CUTI}
        {$jml_6 = $jml_6 + $data.UNDUR}
        {$jml_8 = $jml_8 + $data.DO}
        {$jml_10 = $jml_10 + $data.LULUS}
        {$rata_2 = $rata_2 + ($data.KRS/$jml*100)}
		{$rata_3 = $rata_3 + ($data.NOKRS/$jml*100)}
        {$rata_4 = $rata_4 + ($data.CUTI/$jml*100)}
        {$rata_6 = $rata_6 + ($data.UNDUR/$jml*100)}
        {$rata_8 = $rata_8 + ($data.DO/$jml*100)}
        {$rata_10 = $rata_10 + ($data.LULUS/$jml*100)}
        {$row = $row+1}
        <tr>
            <td><center>{$data.TAHUN_ANGKATAN}</center></td>
            <td><center>{$data.KRS}</center></td>
            <td><center>{($data.KRS/$jml*100)|round:2}</center></td>
			<td><center>{$data.NOKRS}</center></td>
			<td><center>{($data.NOKRS/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=C&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.CUTI}</a></center></td>
            <td><center>{($data.CUTI/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=U&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.UNDUR}</a></center></td>
            <td><center>{($data.UNDUR/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=DO&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.DO}</a></center></td>
            <td><center>{($data.DO/$jml*100)|round:2}</center></td>
            <td><center>{$data.LULUS}</center></td>
            <td><center>{($data.LULUS/$jml*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.JUMLAH_13}</center></td>
            <td><center>{$data.SKRIPSI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
		<td><center>{$jml_3}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td  bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td><center>{$jml_mhs_asing}</center></td>
      <td><center>{$jml_mhs_skripsi}</center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
		<td><center>{($rata_3/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_4/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_10/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
        <td bgcolor="#333333"><center></center></td>
      <td bgcolor="#333333"><center></center></td>
	</tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Profil Mahasiswa per Tahun Angkatan Berdasarkan Status Akademik Tahun {$tahun}/{$tahun+1} (Alih Jenis)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Fakultas {$fakultas}  </div></td>
		</tr>
</table>

<table class="ui-widget">
<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>Krs</center></td>
		<td colspan="2"><center>Nokrs</center></td>
        <td colspan="2"><center>Cuti Akademik</center></td>
        <td colspan="2"><center>Mengundurkan Diri</center></td>
        <td colspan="2"><center>Mahasiswa DO</center></td>
        <td colspan="2"><center>Lulus</center></td>
        <td rowspan="2"><center>Jumlah Mahasiswa Per Angkatan</center></td>
        <td rowspan="2"><center>Jumlah Mahasiswa Asing</center></td>
        <td rowspan="2"><center>Sedang Skripsi</center></td>
	</tr>
	
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
		<td><center>%</center></td>
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
	
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
        <td><center>14</center></td>
		<td><center>15</center></td>
        <td><center>16</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
	{$jml_3 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
    {$rata_2 = 0}
	{$rata_3 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
    {$row = 0}
    {$jml_mhs_asing = 0}
    {$jml_mhs_skripsi = 0}

    {foreach $akademik_AJ as $data}
        {$jml = $data.KRS + $data.NOKRS + $data.CUTI + $data.UNDUR + $data.DO + $data.LULUS}
        {$jml_total = $jml_total + $jml}
        {$jml_mhs_asing = $jml_mhs_asing + $data.JUMLAH_13}
        {$jml_mhs_skripsi = $jml_mhs_skripsi + $data.SKRIPSI}
        {$jml_2 = $jml_2 + $data.KRS}
		{$jml_3 = $jml_3 + $data.NOKRS}
        {$jml_4 = $jml_4 + $data.CUTI}
        {$jml_6 = $jml_6 + $data.UNDUR}
        {$jml_8 = $jml_8 + $data.DO}
        {$jml_10 = $jml_10 + $data.LULUS}
        {$rata_2 = $rata_2 + ($data.KRS/$jml*100)}
		{$rata_3 = $rata_3 + ($data.NOKRS/$jml*100)}
        {$rata_4 = $rata_4 + ($data.CUTI/$jml*100)}
        {$rata_6 = $rata_6 + ($data.UNDUR/$jml*100)}
        {$rata_8 = $rata_8 + ($data.DO/$jml*100)}
        {$rata_10 = $rata_10 + ($data.LULUS/$jml*100)}
        {$row = $row+1}
        <tr>
            <td><center>{$data.TAHUN_ANGKATAN}</center></td>
            <td><center>{$data.KRS}</center></td>
            <td><center>{($data.KRS/$jml*100)|round:2}</center></td>
			<td><center>{$data.NOKRS}</center></td>
			<td><center>{($data.NOKRS/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=C&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.CUTI}</a></center></td>
            <td><center>{($data.CUTI/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=U&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.UNDUR}</a></center></td>
            <td><center>{($data.UNDUR/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=DO&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.DO}</a></center></td>
            <td><center>{($data.DO/$jml*100)|round:2}</center></td>
            <td><center>{$data.LULUS}</center></td>
            <td><center>{($data.LULUS/$jml*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.JUMLAH_13}</center></td>
            <td><center>{$data.SKRIPSI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
		<td><center>{$jml_3}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td  bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td><center>{$jml_mhs_asing}</center></td>
      <td><center>{$jml_mhs_skripsi}</center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
		<td><center>{($rata_3/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_4/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_10/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
        <td bgcolor="#333333"><center></center></td>
      <td bgcolor="#333333"><center></center></td>
	</tr>
</table>
<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Profil Mahasiswa per Tahun Angkatan Berdasarkan Status Akademik Tahun {$tahun}/{$tahun+1} (S2-Reguler)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Fakultas {$fakultas}  </div></td>
		</tr>
</table>

<table class="ui-widget">
<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>Krs</center></td>
		<td colspan="2"><center>Nokrs</center></td>
        <td colspan="2"><center>Cuti Akademik</center></td>
        <td colspan="2"><center>Mengundurkan Diri</center></td>
        <td colspan="2"><center>Mahasiswa DO</center></td>
        <td colspan="2"><center>Lulus</center></td>
        <td rowspan="2"><center>Jumlah Mahasiswa Per Angkatan</center></td>
        <td rowspan="2"><center>Jumlah Mahasiswa Asing</center></td>
        <td rowspan="2"><center>Sedang Skripsi</center></td>
	</tr>
	
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
		<td><center>%</center></td>
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
	
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
        <td><center>14</center></td>
		<td><center>15</center></td>
        <td><center>16</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
	{$jml_3 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
    {$rata_2 = 0}
	{$rata_3 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
    {$row = 0}
    {$jml_mhs_asing = 0}
    {$jml_mhs_skripsi = 0}

    {foreach $akademik_S2 as $data}
        {$jml = $data.KRS + $data.NOKRS + $data.CUTI + $data.UNDUR + $data.DO + $data.LULUS}
        {$jml_total = $jml_total + $jml}
        {$jml_mhs_asing = $jml_mhs_asing + $data.JUMLAH_13}
        {$jml_mhs_skripsi = $jml_mhs_skripsi + $data.SKRIPSI}
        {$jml_2 = $jml_2 + $data.KRS}
		{$jml_3 = $jml_3 + $data.NOKRS}
        {$jml_4 = $jml_4 + $data.CUTI}
        {$jml_6 = $jml_6 + $data.UNDUR}
        {$jml_8 = $jml_8 + $data.DO}
        {$jml_10 = $jml_10 + $data.LULUS}
        {$rata_2 = $rata_2 + ($data.KRS/$jml*100)}
		{$rata_3 = $rata_3 + ($data.NOKRS/$jml*100)}
        {$rata_4 = $rata_4 + ($data.CUTI/$jml*100)}
        {$rata_6 = $rata_6 + ($data.UNDUR/$jml*100)}
        {$rata_8 = $rata_8 + ($data.DO/$jml*100)}
        {$rata_10 = $rata_10 + ($data.LULUS/$jml*100)}
        {$row = $row+1}
        <tr>
            <td><center>{$data.TAHUN_ANGKATAN}</center></td>
            <td><center>{$data.KRS}</center></td>
            <td><center>{($data.KRS/$jml*100)|round:2}</center></td>
			<td><center>{$data.NOKRS}</center></td>
			<td><center>{($data.NOKRS/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=C&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.CUTI}</a></center></td>
            <td><center>{($data.CUTI/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=U&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.UNDUR}</a></center></td>
            <td><center>{($data.UNDUR/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=DO&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.DO}</a></center></td>
            <td><center>{($data.DO/$jml*100)|round:2}</center></td>
            <td><center>{$data.LULUS}</center></td>
            <td><center>{($data.LULUS/$jml*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.JUMLAH_13}</center></td>
            <td><center>{$data.SKRIPSI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
		<td><center>{$jml_3}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td  bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td><center>{$jml_mhs_asing}</center></td>
      <td><center>{$jml_mhs_skripsi}</center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
		<td><center>{($rata_3/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_4/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_10/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
        <td bgcolor="#333333"><center></center></td>
      <td bgcolor="#333333"><center></center></td>
	</tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Profil Mahasiswa per Tahun Angkatan Berdasarkan Status Akademik Tahun {$tahun}/{$tahun+1} (S3-Reguler)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Fakultas {$fakultas}  </div></td>
		</tr>
</table>

<table class="ui-widget">
<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>Krs</center></td>
		<td colspan="2"><center>Nokrs</center></td>
        <td colspan="2"><center>Cuti Akademik</center></td>
        <td colspan="2"><center>Mengundurkan Diri</center></td>
        <td colspan="2"><center>Mahasiswa DO</center></td>
        <td colspan="2"><center>Lulus</center></td>
        <td rowspan="2"><center>Jumlah Mahasiswa Per Angkatan</center></td>
        <td rowspan="2"><center>Jumlah Mahasiswa Asing</center></td>
        <td rowspan="2"><center>Sedang Skripsi</center></td>
	</tr>
	
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
		<td><center>%</center></td>
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
	
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
        <td><center>14</center></td>
		<td><center>15</center></td>
        <td><center>16</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
	{$jml_3 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
    {$rata_2 = 0}
	{$rata_3 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
    {$row = 0}
    {$jml_mhs_asing = 0}
    {$jml_mhs_skripsi = 0}

    {foreach $akademik_S3 as $data}
        {$jml = $data.KRS + $data.NOKRS + $data.CUTI + $data.UNDUR + $data.DO + $data.LULUS}
        {$jml_total = $jml_total + $jml}
        {$jml_mhs_asing = $jml_mhs_asing + $data.JUMLAH_13}
        {$jml_mhs_skripsi = $jml_mhs_skripsi + $data.SKRIPSI}
        {$jml_2 = $jml_2 + $data.KRS}
		{$jml_3 = $jml_3 + $data.NOKRS}
        {$jml_4 = $jml_4 + $data.CUTI}
        {$jml_6 = $jml_6 + $data.UNDUR}
        {$jml_8 = $jml_8 + $data.DO}
        {$jml_10 = $jml_10 + $data.LULUS}
        {$rata_2 = $rata_2 + ($data.KRS/$jml*100)}
		{$rata_3 = $rata_3 + ($data.NOKRS/$jml*100)}
        {$rata_4 = $rata_4 + ($data.CUTI/$jml*100)}
        {$rata_6 = $rata_6 + ($data.UNDUR/$jml*100)}
        {$rata_8 = $rata_8 + ($data.DO/$jml*100)}
        {$rata_10 = $rata_10 + ($data.LULUS/$jml*100)}
        {$row = $row+1}
        <tr>
            <td><center>{$data.TAHUN_ANGKATAN}</center></td>
            <td><center>{$data.KRS}</center></td>
            <td><center>{($data.KRS/$jml*100)|round:2}</center></td>
			<td><center>{$data.NOKRS}</center></td>
			<td><center>{($data.NOKRS/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=C&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.CUTI}</a></center></td>
            <td><center>{($data.CUTI/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=U&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.UNDUR}</a></center></td>
            <td><center>{($data.UNDUR/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=DO&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.DO}</a></center></td>
            <td><center>{($data.DO/$jml*100)|round:2}</center></td>
            <td><center>{$data.LULUS}</center></td>
            <td><center>{($data.LULUS/$jml*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.JUMLAH_13}</center></td>
            <td><center>{$data.SKRIPSI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
		<td><center>{$jml_3}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td  bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td><center>{$jml_mhs_asing}</center></td>
      <td><center>{$jml_mhs_skripsi}</center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
		<td><center>{($rata_3/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_4/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_10/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
        <td bgcolor="#333333"><center></center></td>
      <td bgcolor="#333333"><center></center></td>
	</tr>
</table>

<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Profil Mahasiswa per Tahun Angkatan Berdasarkan Status Akademik Tahun {$tahun}/{$tahun+1} (Profesi)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">Fakultas {$fakultas}  </div></td>
		</tr>
</table>

<table class="ui-widget">
<tr class="ui-widget-header">
		<td rowspan="2"><center>Tahun Angkatan</center></td>
        <td colspan="2"><center>Krs</center></td>
		<td colspan="2"><center>Nokrs</center></td>
        <td colspan="2"><center>Cuti Akademik</center></td>
        <td colspan="2"><center>Mengundurkan Diri</center></td>
        <td colspan="2"><center>Mahasiswa DO</center></td>
        <td colspan="2"><center>Lulus</center></td>
        <td rowspan="2"><center>Jumlah Mahasiswa Per Angkatan</center></td>
        <td rowspan="2"><center>Jumlah Mahasiswa Asing</center></td>
        <td rowspan="2"><center>Sedang Skripsi</center></td>
	</tr>
	
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
		<td><center>%</center></td>
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
	
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
        <td><center>10</center></td>
        <td><center>11</center></td>
        <td><center>12</center></td>
        <td><center>13</center></td>
        <td><center>14</center></td>
		<td><center>15</center></td>
        <td><center>16</center></td>
	</tr>
    
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
	{$jml_3 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
    {$jml_8 = 0}
    {$jml_10 = 0}
    {$rata_2 = 0}
	{$rata_3 = 0}
    {$rata_4 = 0}
    {$rata_6 = 0}
    {$rata_8 = 0}
    {$rata_10 = 0}
    {$row = 0}
    {$jml_mhs_asing = 0}
    {$jml_mhs_skripsi = 0}

    {foreach $akademik_PROFESI as $data}
        {$jml = $data.KRS + $data.NOKRS + $data.CUTI + $data.UNDUR + $data.DO + $data.LULUS}
        {$jml_total = $jml_total + $jml}
        {$jml_mhs_asing = $jml_mhs_asing + $data.JUMLAH_13}
        {$jml_mhs_skripsi = $jml_mhs_skripsi + $data.SKRIPSI}
        {$jml_2 = $jml_2 + $data.KRS}
		{$jml_3 = $jml_3 + $data.NOKRS}
        {$jml_4 = $jml_4 + $data.CUTI}
        {$jml_6 = $jml_6 + $data.UNDUR}
        {$jml_8 = $jml_8 + $data.DO}
        {$jml_10 = $jml_10 + $data.LULUS}
        {$rata_2 = $rata_2 + ($data.KRS/$jml*100)}
		{$rata_3 = $rata_3 + ($data.NOKRS/$jml*100)}
        {$rata_4 = $rata_4 + ($data.CUTI/$jml*100)}
        {$rata_6 = $rata_6 + ($data.UNDUR/$jml*100)}
        {$rata_8 = $rata_8 + ($data.DO/$jml*100)}
        {$rata_10 = $rata_10 + ($data.LULUS/$jml*100)}
        {$row = $row+1}
        <tr>
            <td><center>{$data.TAHUN_ANGKATAN}</center></td>
            <td><center>{$data.KRS}</center></td>
            <td><center>{($data.KRS/$jml*100)|round:2}</center></td>
			<td><center>{$data.NOKRS}</center></td>
			<td><center>{($data.NOKRS/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=C&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.CUTI}</a></center></td>
            <td><center>{($data.CUTI/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=U&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.UNDUR}</a></center></td>
            <td><center>{($data.UNDUR/$jml*100)|round:2}</center></td>
            <td><center><a href="se-akademik-41-detail.php?st=DO&jalur=R&angkatan={$data.TAHUN_ANGKATAN}">{$data.DO}</a></center></td>
            <td><center>{($data.DO/$jml*100)|round:2}</center></td>
            <td><center>{$data.LULUS}</center></td>
            <td><center>{($data.LULUS/$jml*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.JUMLAH_13}</center></td>
            <td><center>{$data.SKRIPSI}</center></td>
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
		<td><center>{$jml_3}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_8}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_10}</center></td>
      	<td  bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td><center>{$jml_mhs_asing}</center></td>
      <td><center>{$jml_mhs_skripsi}</center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_2/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
		<td><center>{($rata_3/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_4/$row)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($rata_6/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_8/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_10/$row)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
        <td bgcolor="#333333"><center></center></td>
      <td bgcolor="#333333"><center></center></td>
	</tr>
</table>
{/if}