<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>    	
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-custom-pim.css" />
        <link rel="stylesheet" type="text/css" href="../../css/pimpinan-style.css" />
        <link rel="stylesheet" type="text/css" href="../../css/pimpinan-nav-style.css" />
        <link rel="SHORTCUT ICON" href="../../img/keuangan/iconunair.ico"/>
        <script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
        <script language="javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        <script language="javascript" src="../../js/jquery.validate.js"></script>
        <script language="javascript" src="../../js/jquery-input-format.js"></script>
        <script type="text/javascript">var defaultRel = 'se'; var defaultPage = 'home.php'; id_role = 12;</script>
        <script language="javascript" src="../../js/cybercampus.ajax-1.0.js"></script>
        <title>Self Evaluation - {$nama_pt}</title>
        <style>
            .ui-widget-content a{
                text-decoration: none;
                color: brown;
            }
            .ui-widget-content a:hover{
                color: #f09a14;
            }
        </style>        
        
    </head>
    <body>
        <div id="main_container">
            <div id="header">
                <div id="judul_header">
                  <p><br />
                    <h1 style="color:#FFF;position:relative;left:650px;">{$header}</h1>
                  </p>
                </div>

				<div id="menu_tab">
                	{$menu}
				</div>
				
			<div id="center">
			<div id="content">
            </div>
                
            </div>
            <div id="footer">
                <div id="left_footer">
                    <img src="../../../img/keuangan/footer_logo.jpg" width="170" height="37" />
                </div>
                <div id="center_footer">
                    Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU <br />oleh <a target="_blank" href="http://unair.ac.id" class="disable-ajax">Universitas Airlangga</a>
                </div>
                <div id="right_footer">
                    <a href="#">home</a>
                    <a href="#">about</a>
                    <a href="#">sitemap</a>
                    <a href="#">rss</a>
                    <a href="#">contact us</a>
                </div>
            </div>
        </div>
    </body>
</html>
