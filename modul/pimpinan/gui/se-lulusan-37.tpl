{* Choirul Hidayat, 29-04-2012 *}
{if $jenjang == 'S1'}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Nilai English Language Proficiency Test (ELPT) Lulusan (Program Sarjana)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>

<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="6"><center>Skor ELPT</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt; 450</center></td>
        <td colspan="2"><center>450 &lt;= X &lt;= 500</center></td>
        <td colspan="2"><center>X > 500</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$rata_total = 0}
	{$jml_row = 0}
    {foreach $lulusan_r as $data}
        {$jml = $data.ELPT1_JML + $data.ELPT2_JML + $data.ELPT3_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.ELPT1_JML}
        {$jml_4 = $jml_4 + $data.ELPT2_JML}
        {$jml_6 = $jml_6 + $data.ELPT3_JML}
		{$rata_total = $rata_total + $data.RATA_ELPT}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.ELPT1_JML}</center></td>
            <td><center>{($data.ELPT1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT2_JML}</center></td>
            <td><center>{($data.ELPT2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT3_JML}</center></td>
            <td><center>{($data.ELPT3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.RATA_ELPT}</center></td> 
			{if $data.RATA_ELPT != ''}
			{$jml_row = $jml_row + 1}
			{/if}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_total/$jml_row)|round:2}</center></td>
	</tr>
</table>


<br />
<br />

{if count($lulusan_aj) > 0}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Nilai English Language Proficiency Test (ELPT) Lulusan (Program Alih Jenis dari D3 ke S1)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="6"><center>Skor ELPT</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt; 450</center></td>
        <td colspan="2"><center>450 &lt;= X &lt;= 500</center></td>
        <td colspan="2"><center>X > 500</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$rata_total = 0}
	{$jml_row = 0}
    {foreach $lulusan_aj as $data}
        {$jml = $data.ELPT1_JML + $data.ELPT2_JML + $data.ELPT3_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.ELPT1_JML}
        {$jml_4 = $jml_4 + $data.ELPT2_JML}
        {$jml_6 = $jml_6 + $data.ELPT3_JML}
		{$rata_total = $rata_total + $data.RATA_ELPT}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.ELPT1_JML}</center></td>
            <td><center>{($data.ELPT1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT2_JML}</center></td>
            <td><center>{($data.ELPT2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT3_JML}</center></td>
            <td><center>{($data.ELPT3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.RATA_ELPT}</center></td> 
			{if $data.RATA_ELPT != ''}
			{$jml_row = $jml_row + 1}
			{/if}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_total/$jml_row)|round:2}</center></td>
	</tr>
</table>


{/if}
{/if}


{if $jenjang == 'D3'}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Nilai English Language Proficiency Test (ELPT) Lulusan (Program Diploma 3)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>



<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="8"><center>Skor ELPT</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt; 425</center></td>
        <td colspan="2"><center>425 &lt;= X &lt;= 450</center></td>
		<td colspan="2"><center>450 &lt;= X &lt;= 500</center></td>
        <td colspan="2"><center>X > 500</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
		<td><center>9</center></td>
        <td><center>10</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_8 = 0}
	{$rata_total = 0}
	{$jml_row = 0}
    {foreach $lulusan_d3 as $data}
        {$jml = $data.ELPT1_JML + $data.ELPT2_JML + $data.ELPT3_JML + $data.ELPT4_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.ELPT1_JML}
        {$jml_4 = $jml_4 + $data.ELPT2_JML}
        {$jml_6 = $jml_6 + $data.ELPT3_JML}
		{$jml_8 = $jml_8 + $data.ELPT4_JML}
		{$rata_total = $rata_total + $data.RATA_ELPT}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.ELPT1_JML}</center></td>
            <td><center>{($data.ELPT1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT2_JML}</center></td>
            <td><center>{($data.ELPT2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT3_JML}</center></td>
            <td><center>{($data.ELPT3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
			<td><center>{$data.ELPT4_JML}</center></td>
            <td><center>{($data.ELPT4_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.RATA_ELPT}</center></td> 
			{if $data.RATA_ELPT != ''}
			{$jml_row = $jml_row + 1}
			{/if}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_8}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
	  	<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_8/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_total/$jml_row)|round:2}</center></td>
	</tr>
</table>

<br />

{/if}


{* Choirul Hidayat, 29-04-2012 *}
{if $jenjang == ''}

{if count($lulusan_d3) > 0}
<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Nilai English Language Proficiency Test (ELPT) Lulusan (Program Diploma 3)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>



<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="8"><center>Skor ELPT</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt; 425</center></td>
        <td colspan="2"><center>425 &lt;= X &lt;= 450</center></td>
		<td colspan="2"><center>450 &lt;= X &lt;= 500</center></td>
        <td colspan="2"><center>X > 500</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
		<td><center>9</center></td>
        <td><center>10</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$jml_8 = 0}
	{$rata_total = 0}
	{$jml_row = 0}
    {foreach $lulusan_d3 as $data}
        {$jml = $data.ELPT1_JML + $data.ELPT2_JML + $data.ELPT3_JML + $data.ELPT4_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.ELPT1_JML}
        {$jml_4 = $jml_4 + $data.ELPT2_JML}
        {$jml_6 = $jml_6 + $data.ELPT3_JML}
		{$jml_8 = $jml_8 + $data.ELPT4_JML}
		{$rata_total = $rata_total + $data.RATA_ELPT}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.ELPT1_JML}</center></td>
            <td><center>{($data.ELPT1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT2_JML}</center></td>
            <td><center>{($data.ELPT2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT3_JML}</center></td>
            <td><center>{($data.ELPT3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
			<td><center>{$data.ELPT4_JML}</center></td>
            <td><center>{($data.ELPT4_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.RATA_ELPT}</center></td> 
			{if $data.RATA_ELPT != ''}
			{$jml_row = $jml_row + 1}
			{/if}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_8}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
	  	<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_8/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_total/$jml_row)|round:2}</center></td>
	</tr>
</table>

<br /><br />
{/if}

{if count($lulusan_r) > 0}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Nilai English Language Proficiency Test (ELPT) Lulusan (Program Sarjana)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="6"><center>Skor ELPT</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt; 450</center></td>
        <td colspan="2"><center>450 &lt;= X &lt;= 500</center></td>
        <td colspan="2"><center>X > 500</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$rata_total = 0}
	{$jml_row = 0}
    {foreach $lulusan_r as $data}
        {$jml = $data.ELPT1_JML + $data.ELPT2_JML + $data.ELPT3_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.ELPT1_JML}
        {$jml_4 = $jml_4 + $data.ELPT2_JML}
        {$jml_6 = $jml_6 + $data.ELPT3_JML}
		{$rata_total = $rata_total + $data.RATA_ELPT}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.ELPT1_JML}</center></td>
            <td><center>{($data.ELPT1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT2_JML}</center></td>
            <td><center>{($data.ELPT2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT3_JML}</center></td>
            <td><center>{($data.ELPT3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.RATA_ELPT}</center></td> 
			{if $data.RATA_ELPT != ''}
			{$jml_row = $jml_row + 1}
			{/if}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_total/$jml_row)|round:2}</center></td>
	</tr>
</table>
{/if}
<br />
<br />


{if count($lulusan_aj) > 0}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Nilai English Language Proficiency Test (ELPT) Lulusan (Program Alih Jenis dari D3 ke S1)</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>



<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="6"><center>Skor ELPT</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>X &lt; 450</center></td>
        <td colspan="2"><center>450 &lt;= X &lt;= 500</center></td>
        <td colspan="2"><center>X > 500</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
        <td><center>8</center></td>
        <td><center>9</center></td>
	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$rata_total = 0}
	{$jml_row = 0}
    {foreach $lulusan_aj as $data}
        {$jml = $data.ELPT1_JML + $data.ELPT2_JML + $data.ELPT3_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.ELPT1_JML}
        {$jml_4 = $jml_4 + $data.ELPT2_JML}
        {$jml_6 = $jml_6 + $data.ELPT3_JML}
		{$rata_total = $rata_total + $data.RATA_ELPT}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.ELPT1_JML}</center></td>
            <td><center>{($data.ELPT1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT2_JML}</center></td>
            <td><center>{($data.ELPT2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT3_JML}</center></td>
            <td><center>{($data.ELPT3_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.RATA_ELPT}</center></td> 
			{if $data.RATA_ELPT != ''}
			{$jml_row = $jml_row + 1}
			{/if}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_6}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_6/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_total/$jml_row)|round:2}</center></td>
	</tr>
</table>
{/if}


<br />
<br />

{if count($lulusan_s2) > 0}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Nilai English Language Proficiency Test (ELPT) Lulusan S2</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>



<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="4"><center>Skor ELPT</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>450 &lt;= X &lt;= 500</center></td>
        <td colspan="2"><center>X > 500</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$rata_total = 0}
	{$jml_row = 0}
    {foreach $lulusan_s2 as $data}
        {$jml = $data.ELPT1_JML + $data.ELPT2_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.ELPT1_JML}
        {$jml_4 = $jml_4 + $data.ELPT2_JML}
		{$rata_total = $rata_total + $data.RATA_ELPT}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.ELPT1_JML}</center></td>
            <td><center>{($data.ELPT1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT2_JML}</center></td>
            <td><center>{($data.ELPT2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.RATA_ELPT}</center></td> 
			{if $data.RATA_ELPT != ''}
			{$jml_row = $jml_row + 1}
			{/if}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_total/$jml_row)|round:2}</center></td>
	</tr>
</table>

{/if}

<br />
<br />


{if count($lulusan_s3) > 0}
	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="9">
			<div class="center_title_bar">Nilai English Language Proficiency Test (ELPT) Lulusan S3</div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="9"><div style="padding:10px;">{$header}</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td rowspan="3"><center>Tahun Akademik Lulus</center></td>
        <td colspan="4"><center>Skor ELPT</center></td>
        <td rowspan="3"><center>Total Lulusan</center></td>
        <td rowspan="3"><center>Rerata</center></td>
	</tr>
	<tr class="ui-widget-header">
        <td colspan="2"><center>450 &lt;= X &lt;= 500</center></td>
        <td colspan="2"><center>X > 500</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
        <td><center>%</center></td>
        <td><center>Jumlah</center></td>
        <td><center>%</center></td>
	</tr>
    <tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>	</tr>
    {$jml = 0}
    {$jml_total = 0}
    {$jml_2 = 0}
    {$jml_4 = 0}
    {$jml_6 = 0}
	{$rata_total = 0}
	{$jml_row = 0}
    {foreach $lulusan_s3 as $data}
        {$jml = $data.ELPT1_JML + $data.ELPT2_JML}
        {$jml_total = $jml_total + $jml}
        {$jml_2 = $jml_2 + $data.ELPT1_JML}
        {$jml_4 = $jml_4 + $data.ELPT2_JML}
		{$rata_total = $rata_total + $data.RATA_ELPT}
        <tr>
            <td><center>{$data.THN_AKADEMIK_LULUS}/{$data.THN_AKADEMIK_LULUS+1}</center></td>
            <td><center>{$data.ELPT1_JML}</center></td>
            <td><center>{($data.ELPT1_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$data.ELPT2_JML}</center></td>
            <td><center>{($data.ELPT2_JML/$data.TOTAL_LULUSAN*100)|round:2}</center></td>
            <td><center>{$jml}</center></td>
            <td><center>{$data.RATA_ELPT}</center></td> 
			{if $data.RATA_ELPT != ''}
			{$jml_row = $jml_row + 1}
			{/if}
        </tr>
    {/foreach}
    
    <tr class="ui-widget-header">
		<td><center>Jumlah</center></td>
	  <td><center>{$jml_2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{$jml_4}</center></td>
		<td bgcolor="#333333"><center></center></td>
      <td><center>{$jml_total}</center></td>
        <td bgcolor="#333333"><center></center></td>
	</tr>
    <tr class="ui-widget-header">
	  <td><center>Rerata</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_2/$jml_total*100)|round:2}</center></td>
		<td bgcolor="#333333"><center></center></td>
	  <td><center>{($jml_4/$jml_total*100)|round:2}</center></td>
        <td bgcolor="#333333"><center></center></td>
      <td><center>{($rata_total/$jml_row)|round:2}</center></td>
	</tr>
</table>
{/if}
{/if}