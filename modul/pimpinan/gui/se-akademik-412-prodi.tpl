<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Data Angka Efisiensi Edukasi (AEE) Program Reguler 3 Tahun Terakhir</div></div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$jenjang} - {$prodi} </div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td align="center" ><strong><center>Tahun Akademik</center></strong></td> 
    <td align="center" ><strong><center>Total Mahasiswa Aktif ( Program Reguler )</center></strong></td>
    <td align="center" ><strong><center>Total Lulusan ( Program Reguler )</center></strong></td>
    <td align="center" ><strong><center>AEE(%)</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center" ><strong><center>1</center></strong></td>
    <td align="center" ><strong><center>2</center></strong></td>
    <td align="center" ><strong><center>3</center></strong></td>
    <td align="center" ><strong><center>4</center></strong></td>
  </tr>
  
  {$jml2=0}
  {$jml3=0}
  {$jml4=0}
  {$ttl4=0}
  
  {for $foo=({$tahun}-3) to $tahun}
   {foreach $aee_R{$foo} as $data}
		{$jml2=$jml2+$data.AKTIF}
		{$jml3=$jml3+$data.LULUS}
		{$jml4=$jml4+$data.AEE}
		{$ttl4=$ttl4+1}
  <tr>
    <td align="center"><strong><center>{$data.TAHUN_AKADEMIK}</center></strong></td>
    <td align="center">{$data.AKTIF}</td>
    <td align="center">{$data.LULUS}</td>
    <td align="center">{$data.AEE}</td>
  </tr>
    {/foreach}
  {/for}

  <tr>
    <td align="center"><strong><center>Jumlah</strong></td>
    <td align="center">{$jml2}</td>
    <td align="center">{$jml3}</td>
    <td align="center" bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><strong><center>Rerata</center></strong></td>
    <td align="center" bgcolor="#000000">&nbsp;</td>
    <td align="center" bgcolor="#000000">&nbsp;</td>
    <td align="center">{round($jml4/$ttl4,2)}</td>
  </tr>
</table>

{if count($aee_AJ) > 0}
<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="14">
			<div class="center_title_bar">Data Angka Efisiensi Edukasi (AEE) Program Alih Jenis 3 Tahun Terakhir</div></div>
			</td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="14"><div style="padding:10px;">{$jenjang} - {$prodi} </div></td>
		</tr>
</table>

<table width="100%" border="1" class="ui-widget">
  <tr class="ui-widget-header">
    <td align="center" ><strong><center>Tahun Akademik</center></strong></td>
    <td align="center" ><strong><center>Total Mahasiswa Aktif ( Alih Jenis )</center></strong></td>
    <td align="center" ><strong><center>Total Lulusan ( Alih Jenis )</center></strong></td>
    <td align="center" ><strong><center>AEE(%)</center></strong></td>
  </tr>
  <tr class="ui-widget-header">
    <td align="center" ><strong><center>1</center></strong></td>
    <td align="center" ><strong><center>2</center></strong></td>
    <td align="center" ><strong><center>3</center></strong></td>
    <td align="center" ><strong><center>4</center></strong></td>
  </tr>
  
  {$jml2=0}
  {$jml3=0}
  {$jml4=0}
  {$ttl4=0}
  
  {for $foo=({$tahun}-3) to $tahun}
   {foreach $aee_AJ{$foo} as $data}
		{$jml2=$jml2+$data.AKTIF}
		{$jml3=$jml3+$data.LULUS}
		{$jml4=$jml4+$data.AEE}
		{$ttl4=$ttl4+1}
  <tr>
    <td align="center"><strong><center>{$data.TAHUN_AKADEMIK}</center></strong></td>
    <td align="center">{$data.AKTIF}</td>
    <td align="center">{$data.LULUS}</td>
    <td align="center">{$data.AEE}</td>
  </tr>
    {/foreach}
  {/for}


  <tr>
    <td align="center"><strong><center>Jumlah</strong></td>
    <td align="center">{$jml2}</td>
    <td align="center">{$jml3}</td>
    <td align="center" bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><strong><center>Rerata</center></strong></td>
    <td align="center" bgcolor="#000000">&nbsp;</td>
    <td align="center" bgcolor="#000000">&nbsp;</td>
    <td align="center">{round($jml4/$ttl4,2)}</td>
  </tr>
</table>
{/if}