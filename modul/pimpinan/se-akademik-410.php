<?PHP
include('config.php');

$smarty->assign('tahun',$_SESSION['TAHUN']);

if ($_SESSION['STATUS']==1 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);

$ik411a_R = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where  id_jenjang=1 and jalur='R'
								order by layanan");
								
								
$smarty->assign('ik411a_R', $ik411a_R);

$ik411_R = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where  id_jenjang=1 and jalur='R'
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_R', $ik411_R);

$ik411a_AJ = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where  id_jenjang=1 and jalur='AJ'
								order by layanan");
								
								
$smarty->assign('ik411a_AJ', $ik411a_AJ);

$ik411_AJ = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where  id_jenjang=1 and jalur='AJ'
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_AJ', $ik411_AJ);

$ik411a_D3 = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where  id_jenjang=5
								order by layanan");
								
								
$smarty->assign('ik411a_D3', $ik411a_D3);

$ik411_D3 = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where  id_jenjang=5
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_D3', $ik411_D3);

$ik411a_S2 = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where  id_jenjang=2
								order by layanan");
								
								
$smarty->assign('ik411a_S2', $ik411a_S2);

$ik411_S2 = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where  id_jenjang=2
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_S2', $ik411_S2);

$ik411a_S3 = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where  id_jenjang=3
								order by layanan");
								
								
$smarty->assign('ik411a_S3', $ik411a_S3);

$ik411_S3 = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where  id_jenjang=3
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_S3', $ik411_S3);

$ik411a_PROFESI = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where  id_jenjang=9
								order by layanan");
								
								
$smarty->assign('ik411a_PROFESI', $ik411a_PROFESI);

$ik411_PROFESI = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where  id_jenjang=9
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_PROFESI', $ik411_PROFESI);


$smarty->display("se-akademik-410-fakultas.tpl");
}


if ($_SESSION['STATUS']==2 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('fakultas', $_SESSION['NM_FAK']);

$ik411a_R = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and jalur='R'
								order by layanan");
								
								
$smarty->assign('ik411a_R', $ik411a_R);

$ik411_R = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and jalur='R'
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_R', $ik411_R);

$ik411a_AJ = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and jalur='AJ'
								order by layanan");
								
								
$smarty->assign('ik411a_AJ', $ik411a_AJ);

$ik411_AJ = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and jalur='AJ'
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_AJ', $ik411_AJ);

$ik411a_D3 = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5
								order by layanan");
								
								
$smarty->assign('ik411a_D3', $ik411a_D3);

$ik411_D3 = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_D3', $ik411_D3);

$ik411a_S2 = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2
								order by layanan");
								
								
$smarty->assign('ik411a_S2', $ik411a_S2);

$ik411_S2 = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_S2', $ik411_S2);

$ik411a_S3 = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3
								order by layanan");
								
								
$smarty->assign('ik411a_S3', $ik411a_S3);

$ik411_S3 = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_S3', $ik411_S3);

$ik411a_PROFESI = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9
								order by layanan");
								
								
$smarty->assign('ik411a_PROFESI', $ik411a_PROFESI);

$ik411_PROFESI = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_PROFESI', $ik411_PROFESI);


$smarty->display("se-akademik-410-fakultas.tpl");
}

if ($_SESSION['STATUS']==3 ) {
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('jenjang', $_SESSION['JENJANG']);
$smarty->assign('prodi', $_SESSION['PRODI']);

$ik411a_R = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where id_program_studi='$_SESSION[ID_PRODI]' and jalur='R'
								order by layanan");
								
								
$smarty->assign('ik411a_R', $ik411a_R);

$ik411_R = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where id_program_studi='$_SESSION[ID_PRODI]' and jalur='R'
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_R', $ik411_R);

$ik411a_AJ = $db_wh->QueryToArray("select distinct layanan
								from wh_411
								where id_program_studi='$_SESSION[ID_PRODI]' and jalur='AJ'
								order by layanan");
								
								
$smarty->assign('ik411a_AJ', $ik411a_AJ);

$ik411_AJ = $db_wh->QueryToArray("select layanan,substr(semester,1,4) as tahun,
								round(avg(ik),2) as ik 
								from wh_411
								where id_program_studi='$_SESSION[ID_PRODI]' and jalur='AJ'
								group by layanan,substr(semester,1,4)
								order by substr(semester,1,4),layanan");
$smarty->assign('ik411_AJ', $ik411_AJ);

$smarty->display("se-akademik-410-prodi.tpl");
}

?>