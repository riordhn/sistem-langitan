<?PHP
include('config.php');

$smarty->assign('tahun',$_SESSION['TAHUN']);
$angkatan=$_GET['angkatan'];
$st=$_GET['st'];
$jalur=$_GET['jalur'];
$id_fakultas=$_SESSION['ID_FAK'];

if ($st=='DO')
{
$op='6,20';
}
if ($st=='U')
{
$op='5';
}
if ($st=='C')
{
$op='2';
}



//Detail Status Akademik
if ($_SESSION['STATUS']==3)
{
$detail = $db->QueryToArray("select nim_mhs,nm_pengguna,prodi,status_akademik_mhs,
								wm_concat(semester) as semester from(
								select nim_mhs,nm_pengguna,nm_status_pengguna as status_akademik_mhs,
								nm_jenjang||'-'||nm_program_studi as prodi,
								thn_akademik_semester||' - '||nm_semester||' SK:'||admisi.no_sk as semester from mahasiswa
								join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
								join admisi on mahasiswa.id_mhs=admisi.id_mhs
								join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
								join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
								join semester on admisi.id_semester=semester.id_semester
								join status_pengguna on mahasiswa.status_akademik_mhs=status_pengguna.id_status_pengguna
								where thn_angkatan_mhs=substr('$angkatan',1,4) and program_studi.id_program_studi='$_SESSION[ID_PRODI]' and status='$jalur'
								and status_akademik_mhs in ($op) and status_akademik_mhs !=1
								order by program_studi.id_program_studi,nim_mhs,thn_akademik_semester,nm_semester)
								group by nim_mhs,nm_pengguna,status_akademik_mhs,prodi");
						
$smarty->assign('detail', $detail);

}
if ($_SESSION['STATUS']==2)
{
$detail = $db->QueryToArray("select nim_mhs,nm_pengguna,prodi,status_akademik_mhs,
								wm_concat(semester) as semester from(
								select nim_mhs,nm_pengguna,nm_status_pengguna as status_akademik_mhs,
								nm_jenjang||'-'||nm_program_studi as prodi,
								thn_akademik_semester||' - '||nm_semester||' SK:'||admisi.no_sk as semester from mahasiswa
								join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
								join admisi on mahasiswa.id_mhs=admisi.id_mhs
								join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
								join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
								join semester on admisi.id_semester=semester.id_semester
								join status_pengguna on mahasiswa.status_akademik_mhs=status_pengguna.id_status_pengguna
								where thn_angkatan_mhs=substr('$angkatan',1,4) and program_studi.id_fakultas=$id_fakultas and status='$jalur'
								and status_akademik_mhs in ($op) and status_akademik_mhs !=1
								order by program_studi.id_program_studi,nim_mhs,thn_akademik_semester,nm_semester)
								group by nim_mhs,nm_pengguna,status_akademik_mhs,prodi");

							
$smarty->assign('detail', $detail);

}

$smarty->display("se-akademik-41-detail.tpl");

?> 