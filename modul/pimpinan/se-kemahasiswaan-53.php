<?php
	include 'config.php';
	
if($_SESSION['ID_PRODI'] != ''){
	$where = "AND ID_PROGRAM_STUDI = '".$_SESSION['ID_PRODI']."'";
	$header = $_SESSION['JENJANG'] . ' - '. $_SESSION['PRODI'];
	
}elseif($_SESSION['ID_FAK'] != ''){
	$where = "AND ID_FAKULTAS = '".$_SESSION['ID_FAK']."'";
	$header = 'FAKULTAS '.strtoupper($_SESSION['NM_FAK']);

}else{
	$where = "";
	$header = strtoupper('Universitas Airlangga');
}

/*
$mhs_53 = $db->QueryToArray("
select pro.nm_provinsi as NM_PROVINSI, kt.nm_kota as nm_kota, cmhs.id_jalur, ps.id_program_studi, ps.id_fakultas, ps.id_jenjang, 
thn_akademik_semester AS THN_AKADEMIK_SMT, thn_angkatan_mhs AS THN_ANGKATAN, mhs.status AS STATUS_MHS, sysdate as tgl_update, MHS.nim_mhs,
coalesce(total_pendapatan_ortu, penghasilan_ortu_mhs, 0) as PENGHASILAN_ORTU
from calon_mahasiswa_baru cmhs
join program_studi ps on ps.id_program_studi = cmhs.id_program_studi
join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan  
join semester sm on sm.id_semester = pen.id_semester
join mahasiswa mhs on mhs.id_c_mhs = cmhs.id_c_mhs
left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
left join sekolah skl on skl.id_sekolah = cms.id_sekolah_asal
left join kota kt on kt.id_kota = skl.id_kota
left join provinsi pro on pro.id_provinsi = kt.id_provinsi
join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmhs.id_c_mhs
");

$db_wh->Query("DELETE WH_SE_MHS_ASAL");

foreach($mhs_53 as $data){

$db_wh->Query("INSERT INTO WH_SE_MHS_ASAL (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, THN_ANGKATAN, PENGHASILAN_ORTU, STATUS_MHS, ID_JALUR, TGL_UPDATE, NM_PROVINSI, NM_KOTA, NIM_MHS)
VALUES
('$data[THN_AKADEMIK_SMT]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[THN_ANGKATAN]', '$data[PENGHASILAN_ORTU]', '$data[STATUS_MHS]', '$data[ID_JALUR]', '$data[TGL_UPDATE]', '$data[NM_PROVINSI]', '$data[NM_KOTA]', '$data[NIM_MHS]')");

}
*/

$jalur_wh = $db_wh->QueryToArray("SELECT ID_JALUR FROM WH_SE_MHS_ASAL WHERE ID_JALUR IS NOT NULL $where GROUP BY ID_JALUR");
$i=0;
foreach($jalur_wh as $data){
	$i++;
	$id_jalur .= $data['ID_JALUR'];
	if (count($jalur_wh) != $i)
		$id_jalur .= ', ';
		
}

$jalur = $db->QueryToArray("SELECT * FROM JALUR WHERE ID_JALUR IN (5, 1, 3) ORDER BY NM_JALUR");


foreach($jalur as $data){
		
$mhs_r = $db_wh->QueryToArray("
SELECT 
A.NM_KOTA, A.NM_PROVINSI, PENGHASILAN1_JML, PENGHASILAN2_JML, PENGHASILAN3_JML, PENGHASILAN4_JML, PENGHASILAN5_JML, PENGHASILAN6_JML
FROM (
SELECT NM_PROVINSI, NM_KOTA
FROM WH_SE_MHS_ASAL
WHERE THN_AKADEMIK_SMT = '".$_SESSION['TAHUN']."' AND ID_JALUR = '".$data['ID_JALUR']."' $where 
GROUP BY NM_PROVINSI, NM_KOTA
) A
LEFT JOIN (
SELECT COUNT(PENGHASILAN_ORTU) AS PENGHASILAN1_JML, NM_PROVINSI, NM_KOTA
FROM WH_SE_MHS_ASAL
WHERE THN_AKADEMIK_SMT = '".$_SESSION['TAHUN']."' AND ID_JALUR = '".$data['ID_JALUR']."' $where  AND PENGHASILAN_ORTU < '1000000'
GROUP BY NM_PROVINSI, NM_KOTA
) B ON B.NM_PROVINSI = A.NM_PROVINSI AND B.NM_KOTA = A.NM_KOTA
LEFT JOIN (
SELECT COUNT(PENGHASILAN_ORTU) AS PENGHASILAN2_JML, NM_PROVINSI, NM_KOTA
FROM WH_SE_MHS_ASAL
WHERE THN_AKADEMIK_SMT = '".$_SESSION['TAHUN']."' AND ID_JALUR = '".$data['ID_JALUR']."' $where  AND PENGHASILAN_ORTU >= '1000000' AND PENGHASILAN_ORTU < '2500000'
GROUP BY NM_PROVINSI, NM_KOTA
) C ON C.NM_PROVINSI = A.NM_PROVINSI AND C.NM_KOTA = A.NM_KOTA
LEFT JOIN (
SELECT COUNT(PENGHASILAN_ORTU) AS PENGHASILAN3_JML, NM_PROVINSI, NM_KOTA
FROM WH_SE_MHS_ASAL
WHERE THN_AKADEMIK_SMT = '".$_SESSION['TAHUN']."' AND ID_JALUR = '".$data['ID_JALUR']."' $where  AND PENGHASILAN_ORTU >= '2500000' AND PENGHASILAN_ORTU < '5000000'
GROUP BY NM_PROVINSI, NM_KOTA
) D ON D.NM_PROVINSI = A.NM_PROVINSI AND D.NM_KOTA = A.NM_KOTA
LEFT JOIN (
SELECT COUNT(PENGHASILAN_ORTU) AS PENGHASILAN4_JML, NM_PROVINSI, NM_KOTA
FROM WH_SE_MHS_ASAL
WHERE THN_AKADEMIK_SMT = '".$_SESSION['TAHUN']."' AND ID_JALUR = '".$data['ID_JALUR']."' $where  AND PENGHASILAN_ORTU >= '5000000' AND PENGHASILAN_ORTU < '7500000'
GROUP BY NM_PROVINSI, NM_KOTA
) E ON E.NM_PROVINSI = A.NM_PROVINSI AND E.NM_KOTA = A.NM_KOTA
LEFT JOIN (
SELECT COUNT(PENGHASILAN_ORTU) AS PENGHASILAN5_JML, NM_PROVINSI, NM_KOTA
FROM WH_SE_MHS_ASAL
WHERE THN_AKADEMIK_SMT = '".$_SESSION['TAHUN']."' AND ID_JALUR = '".$data['ID_JALUR']."' $where  AND PENGHASILAN_ORTU >= '7500000' AND PENGHASILAN_ORTU < '10000000'
GROUP BY NM_PROVINSI, NM_KOTA
) F ON F.NM_PROVINSI = A.NM_PROVINSI AND F.NM_KOTA = A.NM_KOTA
LEFT JOIN (
SELECT COUNT(PENGHASILAN_ORTU) AS PENGHASILAN6_JML, NM_PROVINSI, NM_KOTA
FROM WH_SE_MHS_ASAL
WHERE THN_AKADEMIK_SMT = '".$_SESSION['TAHUN']."' AND ID_JALUR = '".$data['ID_JALUR']."' $where  AND PENGHASILAN_ORTU >= '10000000'
GROUP BY NM_PROVINSI, NM_KOTA
) G ON G.NM_PROVINSI = A.NM_PROVINSI AND G.NM_KOTA = A.NM_KOTA
ORDER BY A.NM_PROVINSI, A.NM_KOTA ASC");						
							

if(count($mhs_r) > 0){
$tampil .= '

	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="7"><div class="center_title_bar">Profil Mahasiwa Baru (Jalur '.$data['NM_JALUR'].') Angkatan '.$_SESSION['TAHUN'].'/'.($_SESSION['TAHUN']+1).' Berdasarkan Kota/Kabupaten Asal Mahasiswa dan Penghasilan Orang Tua per Bulan</div></td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
		</tr>
	</table>



<table class="ui-widget" style="width:100%">
  <tr class="ui-widget-header">
    <td rowspan="3" colspan="2">Asal SMTA Mahasiswa Baru (Kota/Kabupaten)</td>
    <td colspan="12">Penghasilan Orang Tua per Bulan (dalam ribu Rp)</td>
  </tr>
  <tr class="ui-widget-header">
    <td colspan="2">X &lt; 1000</td>
    <td colspan="2">1000 =&lt; X &lt; 2500</td>
    <td colspan="2">2500 =&lt; X &lt; 5000</td>
    <td colspan="2">5000 =&lt; X 7500</td>
	<td colspan="2">7500 =&lt; X 10000</td>
    <td colspan="2">X &gt; 10000</td>
  </tr>
';



 $tampil .= '
    <tr class="ui-widget-header">
    <td>Jumlah</td>
    <td>%</td>
    <td>Jumlah</td>
    <td>%</td>
    <td>Jumlah</td>
    <td>%</td>
    <td>Jumlah</td>
    <td>%</td>
    <td>Jumlah</td>
    <td>%</td>
	<td>Jumlah</td>
    <td>%</td>
  </tr>
  <tr class="ui-widget-header">
    <td colspan="2">1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
    <td>9</td>
    <td>10</td>
    <td>11</td>
	<td>12</td>
    <td>13</td>
  </tr>
';
	
	$persen1 = 0;
	$persen2 = 0;
	$persen3 = 0;
	$persen4 = 0;
	$persen5 = 0;
	$persen6 = 0;
	$jumlah = 0;
	$row = 0;
	
	$jml1 = 0;
	
	
	
	
	foreach($mhs_r as $value){	
	$tampil .= '  <tr>
	<td>'.$value['NM_PROVINSI'].'</td>
    <td>'.$value['NM_KOTA'].'</td>
    <td align="center">'.$value['PENGHASILAN1_JML'].'</td>
    <td align="center">'.round($value['PENGHASILAN1_JML']/($value['PENGHASILAN1_JML']+$value['PENGHASILAN2_JML']+$value['PENGHASILAN3_JML']+$value['PENGHASILAN4_JML']+$value['PENGHASILAN5_JML']+$value['PENGHASILAN6_JML'])*100, 2).'</td>
    <td align="center">'.$value['PENGHASILAN2_JML'].'</td>
    <td align="center">'.round($value['PENGHASILAN2_JML']/($value['PENGHASILAN1_JML']+$value['PENGHASILAN2_JML']+$value['PENGHASILAN3_JML']+$value['PENGHASILAN4_JML']+$value['PENGHASILAN5_JML']+$value['PENGHASILAN6_JML'])*100, 2).'</td>
    <td align="center">'.$value['PENGHASILAN3_JML'].'</td>
    <td align="center">'.round($value['PENGHASILAN3_JML']/($value['PENGHASILAN1_JML']+$value['PENGHASILAN2_JML']+$value['PENGHASILAN3_JML']+$value['PENGHASILAN4_JML']+$value['PENGHASILAN5_JML']+$value['PENGHASILAN6_JML'])*100, 2).'</td>
    <td align="center">'.$value['PENGHASILAN4_JML'].'</td>
    <td align="center">'.round($value['PENGHASILAN4_JML']/($value['PENGHASILAN1_JML']+$value['PENGHASILAN2_JML']+$value['PENGHASILAN3_JML']+$value['PENGHASILAN4_JML']+$value['PENGHASILAN5_JML']+$value['PENGHASILAN6_JML'])*100, 2).'</td>
    <td align="center">'.$value['PENGHASILAN5_JML'].'</td>
    <td align="center">'.round($value['PENGHASILAN5_JML']/($value['PENGHASILAN1_JML']+$value['PENGHASILAN2_JML']+$value['PENGHASILAN3_JML']+$value['PENGHASILAN4_JML']+$value['PENGHASILAN5_JML']+$value['PENGHASILAN6_JML'])*100, 2).'</td>
	<td align="center">'.$value['PENGHASILAN6_JML'].'</td>
    <td align="center">'.round($value['PENGHASILAN6_JML']/($value['PENGHASILAN1_JML']+$value['PENGHASILAN2_JML']+$value['PENGHASILAN3_JML']+$value['PENGHASILAN4_JML']+$value['PENGHASILAN5_JML']+$value['PENGHASILAN6_JML'])*100, 2).'</td>
  </tr>';
  
  $row++;
  $persen1 = $persen1 + round($value['PENGHASILAN1_JML']/($value['PENGHASILAN1_JML']+$value['PENGHASILAN2_JML']+$value['PENGHASILAN3_JML']+$value['PENGHASILAN4_JML']+$value['PENGHASILAN5_JML']+$value['PENGHASILAN6_JML'])*100, 2);
  $persen2 = $persen2 + round($value['PENGHASILAN2_JML']/($value['PENGHASILAN1_JML']+$value['PENGHASILAN2_JML']+$value['PENGHASILAN3_JML']+$value['PENGHASILAN4_JML']+$value['PENGHASILAN5_JML']+$value['PENGHASILAN6_JML'])*100, 2);
  $persen3 = $persen3 + round($value['PENGHASILAN3_JML']/($value['PENGHASILAN1_JML']+$value['PENGHASILAN2_JML']+$value['PENGHASILAN3_JML']+$value['PENGHASILAN4_JML']+$value['PENGHASILAN5_JML']+$value['PENGHASILAN6_JML'])*100, 2);
  $persen4 = $persen4 + round($value['PENGHASILAN4_JML']/($value['PENGHASILAN1_JML']+$value['PENGHASILAN2_JML']+$value['PENGHASILAN3_JML']+$value['PENGHASILAN4_JML']+$value['PENGHASILAN5_JML']+$value['PENGHASILAN6_JML'])*100, 2);
  $persen5 = $persen5 + round($value['PENGHASILAN5_JML']/($value['PENGHASILAN1_JML']+$value['PENGHASILAN2_JML']+$value['PENGHASILAN3_JML']+$value['PENGHASILAN4_JML']+$value['PENGHASILAN5_JML']+$value['PENGHASILAN6_JML'])*100, 2);
  $persen6 = $persen6 + round($value['PENGHASILAN6_JML']/($value['PENGHASILAN1_JML']+$value['PENGHASILAN2_JML']+$value['PENGHASILAN3_JML']+$value['PENGHASILAN4_JML']+$value['PENGHASILAN5_JML']+$value['PENGHASILAN6_JML'])*100, 2);


  $jml1 = $jml1 + $value['PENGHASILAN1_JML'];
  $jml2 = $jml2 + $value['PENGHASILAN2_JML'];
  $jml3 = $jml3 + $value['PENGHASILAN3_JML'];
  $jml4 = $jml4 + $value['PENGHASILAN4_JML'];
  $jml5 = $jml5 + $value['PENGHASILAN5_JML'];
  $jml6 = $jml6 + $value['PENGHASILAN6_JML'];

  	}
	$tampil .= '
      <tr>
    <td colspan="2">Jumlah Mhs Baru</td>
    <td align="center">'.$jml1.'</td>
    <td align="center">'.round($persen1/$row,2).'</td>
    <td align="center">'.$jml2.'</td>
    <td align="center">'.round($persen2/$row,2).'</td>
    <td align="center">'.$jml3.'</td>
    <td align="center">'.round($persen3/$row,2).'</td>
    <td align="center">'.$jml4.'</td>
    <td align="center">'.round($persen4/$row,2).'</td>
    <td align="center">'.$jml5.'</td>
    <td align="center">'.round($persen5/$row,2).'</td>
	<td align="center">'.$jml6.'</td>
    <td align="center">'.round($persen6/$row,2).'</td>
  </tr>
</table><br />
';	
}

}

$smarty->assign('tampil', $tampil);	
$smarty->display('se-kemahasiswaan-53.tpl');
?>