<?PHP
include('config.php');
$smarty->assign('tahun',$_SESSION['TAHUN']);


if ($_SESSION['STATUS']==1 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('fakultas', $_SESSION['NM_FAK']);

$akademik_R = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and id_jenjang=1
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and id_jenjang=1 and (status_mhs_aktif=1 or krs=1))
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_R', $akademik_R);
									
$akademik_AJ = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='AJ' and id_jenjang=1
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='AJ' and id_jenjang=1 and (status_mhs_aktif=1 or krs=1))
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_AJ', $akademik_AJ);

$akademik_S2 = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_jenjang=2
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_jenjang=2 and (status_mhs_aktif=1 or krs=1))
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_S2', $akademik_S2);

$akademik_S3 = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_jenjang=3 and (status_mhs_aktif=1 or krs=1))
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_S3', $akademik_S3);

$akademik_D3 = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_jenjang=5
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_jenjang=5 and (status_mhs_aktif=1 or krs=1))
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_D3', $akademik_D3);


$akademik_PROFESI = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_jenjang=9
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_jenjang=9 and (status_mhs_aktif=1 or krs=1))
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_PROFESI', $akademik_PROFESI);

$smarty->display("se-akademik-41-universitas.tpl");
}


if ($_SESSION['STATUS']==2 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('fakultas', $_SESSION['NM_FAK']);

$akademik_R = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and (status_mhs_aktif=1 or krs=1))
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_R', $akademik_R);
									
$akademik_AJ = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='AJ' and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='AJ' and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and (status_mhs_aktif=1 or krs=1))
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_AJ', $akademik_AJ);

$akademik_S2 = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2 and (status_mhs_aktif=1 or krs=1))
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_S2', $akademik_S2);

$akademik_S3 = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3 and (status_mhs_aktif=1 or krs=1))
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_S3', $akademik_S3);

$akademik_D3 = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5 and (status_mhs_aktif=1 or krs=1))
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_D3', $akademik_D3);


$akademik_PROFESI = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9 and (status_mhs_aktif=1 or krs=1))
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_PROFESI', $akademik_PROFESI);

$smarty->display("se-akademik-41-fakultas.tpl");
}

if ($_SESSION['STATUS']==3 ) {
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('jenjang', $_SESSION['JENJANG']);
$smarty->assign('prodi', $_SESSION['PRODI']);

$akademik_R = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and id_program_studi='$_SESSION[ID_PRODI]'
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_program_studi='$_SESSION[ID_PRODI]' and status='R' and (status_mhs_aktif=1 or krs=1) )
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_R', $akademik_R);
									
$akademik_AJ = $db_wh->QueryToArray("select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan, 
									sum(status_mhs_aktif) as aktif,
									sum(krs) as krs,
									sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)as nokrs,
									sum(status_mhs_cuti_akd) as cuti,
									sum(status_mhs_undur_diri) as undur,
									sum(status_mhs_do) as do,
									sum(status_mhs_lulus) as lulus,
									sum(status_mhs_skripsi) as skripsi
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='AJ' and id_program_studi='$_SESSION[ID_PRODI]'
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_program_studi='$_SESSION[ID_PRODI]' and status='AJ' and (status_mhs_aktif=1 or krs=1))
									group by thn_angkatan_mhs
									order by thn_angkatan_mhs desc");
$smarty->assign('akademik_AJ', $akademik_AJ);
$smarty->display("se-akademik-41-prodi.tpl");
}

?>