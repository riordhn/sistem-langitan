<?php
include('config.php');

if($_SESSION['ID_PRODI'] != ''){
	$where = "AND z.ID_PROGRAM_STUDI = '".$_SESSION['ID_PRODI']."'";
	$header = $_SESSION['JENJANG'] . ' - '. $_SESSION['PRODI'];

}elseif($_SESSION['ID_FAK'] != ''){
	$where = "AND z.ID_FAKULTAS = '".$_SESSION['ID_FAK']."'";
	$header = 'FAKULTAS '.strtoupper($_SESSION['NM_FAK']);

}else{
	$where = "";
	$header = strtoupper('Universitas Airlangga');
}

if($_SESSION['ID_PRODI'] != ''){
$tampil .= '
<table class="ui-widget">
	<tr style="background:navy;color:#fff;padding:5px;">
		<td colspan="7"><div class="center_title_bar">Profil Dosen Tidak Tetap dari Luar Universitas Airlangga Berdasarkan Umur, Tingkat Pendidikan, dan Jabatan Fungsional</div></td>
	</tr>
	<tr class="ui-widget-header" style="padding:5px;">
		<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
	</tr>
</table>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<th rowspan="3" style="vertical-align: middle;"><center>Jabatan Fungsional</center></th>
			<th colspan="15"><center>Kelompok Umur (tahun)</center></th>
			<th rowspan="3" style="vertical-align: middle;"><center>Jumlah</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th width="90" colspan="3"><center>&#60; 31</center></th>
			<th width="90" colspan="3"><center>31 &#8211; 40</center></th>
			<th width="90" colspan="3"><center>41 &#8211; 50</center></th>
			<th width="90" colspan="3"><center>51 &#8211; 60</center></th>
			<th width="90" colspan="3"><center>&#62; 60</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
		</tr>
';
$dt = $db_wh->QueryToArray("
select z.URUT, z.ID_PROGRAM_STUDI, z.NM_JABATAN_FUNGSIONAL, 
coalesce(z.a1,0) as a1, coalesce(z.a2,0) as a2, coalesce(z.a3,0) as a3,
coalesce(y.b1,0) as b1, coalesce(y.b2,0) as b2, coalesce(y.b3,0) as b3,
coalesce(x.c1,0) as c1, coalesce(x.c2,0) as c2, coalesce(x.c3,0) as c3,
coalesce(w.d1,0) as d1, coalesce(w.d2,0) as d2, coalesce(w.d3,0) as d3,
coalesce(v.e1,0) as e1, coalesce(v.e2,0) as e2, coalesce(v.e3,0) as e3
from 
(
select fsg.URUT, fsg.ID_PROGRAM_STUDI, fsg.NM_JABATAN_FUNGSIONAL, a1, a2, a3 from
(
(
select distinct ID_PROGRAM_STUDI,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_PROGRAM_STUDI, sum(S3) as a3, sum(S2) as a2, sum(S1) as a1
from
(
select dsn.ID_PROGRAM_STUDI, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where dsn.USIA_PENGGUNA < 31 and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_PROGRAM_STUDI
) a on fsg.URUT = a.URUT and fsg.ID_PROGRAM_STUDI = a.ID_PROGRAM_STUDI
)
) z
left join
(
select fsg.URUT, fsg.ID_PROGRAM_STUDI, fsg.NM_JABATAN_FUNGSIONAL, b1, b2, b3 from
(
(
select distinct ID_PROGRAM_STUDI,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_PROGRAM_STUDI, sum(S3) as b3, sum(S2) as b2, sum(S1) as b1
from
(
select dsn.ID_PROGRAM_STUDI, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where (dsn.USIA_PENGGUNA between 31 and 40) and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_PROGRAM_STUDI
) b on fsg.URUT = b.URUT and fsg.ID_PROGRAM_STUDI = b.ID_PROGRAM_STUDI
)
) y on z.URUT = y.URUT and z.ID_PROGRAM_STUDI = y.ID_PROGRAM_STUDI
left join
(
select fsg.URUT, fsg.ID_PROGRAM_STUDI, fsg.NM_JABATAN_FUNGSIONAL, c1, c2, c3 from
(
(
select distinct ID_PROGRAM_STUDI,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_PROGRAM_STUDI, sum(S3) as c3, sum(S2) as c2, sum(S1) as c1
from
(
select dsn.ID_PROGRAM_STUDI, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where (dsn.USIA_PENGGUNA between 41 and 50) and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_PROGRAM_STUDI
) b on fsg.URUT = b.URUT and fsg.ID_PROGRAM_STUDI = b.ID_PROGRAM_STUDI
)
) x on z.URUT = x.URUT and z.ID_PROGRAM_STUDI = x.ID_PROGRAM_STUDI
left join
(
select fsg.URUT, fsg.ID_PROGRAM_STUDI, fsg.NM_JABATAN_FUNGSIONAL, d1, d2, d3 from
(
(
select distinct ID_PROGRAM_STUDI,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_PROGRAM_STUDI, sum(S3) as d3, sum(S2) as d2, sum(S1) as d1
from
(
select dsn.ID_PROGRAM_STUDI, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where (dsn.USIA_PENGGUNA between 51 and 60) and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_PROGRAM_STUDI
) b on fsg.URUT = b.URUT and fsg.ID_PROGRAM_STUDI = b.ID_PROGRAM_STUDI
)
) w on z.URUT = w.URUT and z.ID_PROGRAM_STUDI = w.ID_PROGRAM_STUDI
left join
(
select fsg.URUT, fsg.ID_PROGRAM_STUDI, fsg.NM_JABATAN_FUNGSIONAL, e1, e2, e3 from
(
(
select distinct ID_PROGRAM_STUDI,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_PROGRAM_STUDI, sum(S3) as e3, sum(S2) as e2, sum(S1) as e1
from
(
select dsn.ID_PROGRAM_STUDI, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where dsn.USIA_PENGGUNA > 60 and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_PROGRAM_STUDI
) b on fsg.URUT = b.URUT and fsg.ID_PROGRAM_STUDI = b.ID_PROGRAM_STUDI
)
) v on z.URUT = v.URUT and z.ID_PROGRAM_STUDI = v.ID_PROGRAM_STUDI
where z.ID_PROGRAM_STUDI = '".$_SESSION['ID_PRODI']."'
order by z.ID_PROGRAM_STUDI, z.URUT
");

foreach($dt as $data){
$jumlah = 
$data['A1'] + $data['A2'] + $data['A3'] +
$data['B1'] + $data['B2'] + $data['B3'] +
$data['C1'] + $data['C2'] + $data['C3'] +
$data['D1'] + $data['D2'] + $data['D3'] +
$data['E1'] + $data['E2'] + $data['E3']
;
$tampil .= '
	<tr class="ui-widget-content">
		<td>'.$data['NM_JABATAN_FUNGSIONAL'].'</td>
		<td><center>'.$data['A1'].'</center></td>
		<td><center>'.$data['A2'].'</center></td>
		<td><center>'.$data['A3'].'</center></td>
		<td><center>'.$data['B1'].'</center></td>
		<td><center>'.$data['B2'].'</center></td>
		<td><center>'.$data['B3'].'</center></td>
		<td><center>'.$data['C1'].'</center></td>
		<td><center>'.$data['C2'].'</center></td>
		<td><center>'.$data['C3'].'</center></td>
		<td><center>'.$data['D1'].'</center></td>
		<td><center>'.$data['D2'].'</center></td>
		<td><center>'.$data['D3'].'</center></td>
		<td><center>'.$data['E1'].'</center></td>
		<td><center>'.$data['E2'].'</center></td>
		<td><center>'.$data['E3'].'</center></td>
		<td><center>'.$jumlah.'</center></td>
	</tr>';
$last = $data['ID_PROGRAM_STUDI'];
$total_a1 += $data['A1'];
$total_a2 += $data['A2'];
$total_a3 += $data['A3'];
$total_b1 += $data['B1'];
$total_b2 += $data['B2'];
$total_b3 += $data['B3'];
$total_c1 += $data['C1'];
$total_c2 += $data['C2'];
$total_c3 += $data['C3'];
$total_d1 += $data['D1'];
$total_d2 += $data['D2'];
$total_d3 += $data['D3'];
$total_e1 += $data['E1'];
$total_e2 += $data['E2'];
$total_e3 += $data['E3'];
$total += $jumlah;
}

$tampil .= '
	<tr class="ui-widget-header">
		<td style="text-align:right;">TOTAL</td>
		<td><center>'.$total_a1.'</center></td>
		<td><center>'.$total_a2.'</center></td>
		<td><center>'.$total_a3.'</center></td>
		<td><center>'.$total_b1.'</center></td>
		<td><center>'.$total_b2.'</center></td>
		<td><center>'.$total_b3.'</center></td>
		<td><center>'.$total_c1.'</center></td>
		<td><center>'.$total_c2.'</center></td>
		<td><center>'.$total_c3.'</center></td>
		<td><center>'.$total_d1.'</center></td>
		<td><center>'.$total_d2.'</center></td>
		<td><center>'.$total_d3.'</center></td>
		<td><center>'.$total_e1.'</center></td>
		<td><center>'.$total_e2.'</center></td>
		<td><center>'.$total_e3.'</center></td>
		<td><center>'.$total.'</center></td>
	</tr>
</table>
';
}
elseif($_SESSION['ID_FAK'] != ''){
$tampil .= '
<table class="ui-widget">
	<tr style="background:navy;color:#fff;padding:5px;">
		<td colspan="7"><div class="center_title_bar">Profil Dosen Tidak Tetap dari Luar Universitas Airlangga Berdasarkan Umur, Tingkat Pendidikan, dan Jabatan Fungsional</div></td>
	</tr>
	<tr class="ui-widget-header" style="padding:5px;">
		<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
	</tr>
</table>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<th rowspan="3" style="vertical-align: middle;"><center>Jabatan Fungsional</center></th>
			<th colspan="15"><center>Kelompok Umur (tahun)</center></th>
			<th rowspan="3" style="vertical-align: middle;"><center>Jumlah</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th width="90" colspan="3"><center>&#60; 31</center></th>
			<th width="90" colspan="3"><center>31 &#8211; 40</center></th>
			<th width="90" colspan="3"><center>41 &#8211; 50</center></th>
			<th width="90" colspan="3"><center>51 &#8211; 60</center></th>
			<th width="90" colspan="3"><center>&#62; 60</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
		</tr>
';
$dt = $db_wh->QueryToArray("
select z.URUT, z.ID_FAKULTAS, fak.SINGKATAN_FAKULTAS, z.NM_JABATAN_FUNGSIONAL, 
coalesce(z.a1,0) as a1, coalesce(z.a2,0) as a2, coalesce(z.a3,0) as a3,
coalesce(y.b1,0) as b1, coalesce(y.b2,0) as b2, coalesce(y.b3,0) as b3,
coalesce(x.c1,0) as c1, coalesce(x.c2,0) as c2, coalesce(x.c3,0) as c3,
coalesce(w.d1,0) as d1, coalesce(w.d2,0) as d2, coalesce(w.d3,0) as d3,
coalesce(v.e1,0) as e1, coalesce(v.e2,0) as e2, coalesce(v.e3,0) as e3
from 
(
select fsg.URUT, fsg.ID_FAKULTAS, fsg.NM_JABATAN_FUNGSIONAL, a1, a2, a3 from
(
(
select distinct ID_FAKULTAS,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_FAKULTAS, sum(S3) as a3, sum(S2) as a2, sum(S1) as a1
from
(
select dsn.ID_FAKULTAS, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where dsn.USIA_PENGGUNA < 31 and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_FAKULTAS
) a on fsg.URUT = a.URUT and fsg.ID_FAKULTAS = a.ID_FAKULTAS
)
) z
left join
(
select fsg.URUT, fsg.ID_FAKULTAS, fsg.NM_JABATAN_FUNGSIONAL, b1, b2, b3 from
(
(
select distinct ID_FAKULTAS,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_FAKULTAS, sum(S3) as b3, sum(S2) as b2, sum(S1) as b1
from
(
select dsn.ID_FAKULTAS, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where (dsn.USIA_PENGGUNA between 31 and 40) and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_FAKULTAS
) b on fsg.URUT = b.URUT and fsg.ID_FAKULTAS = b.ID_FAKULTAS
)
) y on z.URUT = y.URUT and z.ID_FAKULTAS = y.ID_FAKULTAS
left join
(
select fsg.URUT, fsg.ID_FAKULTAS, fsg.NM_JABATAN_FUNGSIONAL, c1, c2, c3 from
(
(
select distinct ID_FAKULTAS,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_FAKULTAS, sum(S3) as c3, sum(S2) as c2, sum(S1) as c1
from
(
select dsn.ID_FAKULTAS, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where (dsn.USIA_PENGGUNA between 41 and 50) and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_FAKULTAS
) b on fsg.URUT = b.URUT and fsg.ID_FAKULTAS = b.ID_FAKULTAS
)
) x on z.URUT = x.URUT and z.ID_FAKULTAS = x.ID_FAKULTAS
left join
(
select fsg.URUT, fsg.ID_FAKULTAS, fsg.NM_JABATAN_FUNGSIONAL, d1, d2, d3 from
(
(
select distinct ID_FAKULTAS,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_FAKULTAS, sum(S3) as d3, sum(S2) as d2, sum(S1) as d1
from
(
select dsn.ID_FAKULTAS, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where (dsn.USIA_PENGGUNA between 51 and 60) and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_FAKULTAS
) b on fsg.URUT = b.URUT and fsg.ID_FAKULTAS = b.ID_FAKULTAS
)
) w on z.URUT = w.URUT and z.ID_FAKULTAS = w.ID_FAKULTAS
left join
(
select fsg.URUT, fsg.ID_FAKULTAS, fsg.NM_JABATAN_FUNGSIONAL, e1, e2, e3 from
(
(
select distinct ID_FAKULTAS,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_FAKULTAS, sum(S3) as e3, sum(S2) as e2, sum(S1) as e1
from
(
select dsn.ID_FAKULTAS, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where dsn.USIA_PENGGUNA > 60 and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_FAKULTAS
) b on fsg.URUT = b.URUT and fsg.ID_FAKULTAS = b.ID_FAKULTAS
)
) v on z.URUT = v.URUT and z.ID_FAKULTAS = v.ID_FAKULTAS
join FAKULTAS fak on z.ID_FAKULTAS = fak.ID_FAKULTAS
where z.ID_FAKULTAS = '".$_SESSION['ID_FAK']."'
order by z.ID_FAKULTAS, z.URUT
");

foreach($dt as $data){
$jumlah = 
$data['A1'] + $data['A2'] + $data['A3'] +
$data['B1'] + $data['B2'] + $data['B3'] +
$data['C1'] + $data['C2'] + $data['C3'] +
$data['D1'] + $data['D2'] + $data['D3'] +
$data['E1'] + $data['E2'] + $data['E3']
;
$tampil .= '
	<tr class="ui-widget-content">
		<td>'.$data['NM_JABATAN_FUNGSIONAL'].'</td>
		<td><center>'.$data['A1'].'</center></td>
		<td><center>'.$data['A2'].'</center></td>
		<td><center>'.$data['A3'].'</center></td>
		<td><center>'.$data['B1'].'</center></td>
		<td><center>'.$data['B2'].'</center></td>
		<td><center>'.$data['B3'].'</center></td>
		<td><center>'.$data['C1'].'</center></td>
		<td><center>'.$data['C2'].'</center></td>
		<td><center>'.$data['C3'].'</center></td>
		<td><center>'.$data['D1'].'</center></td>
		<td><center>'.$data['D2'].'</center></td>
		<td><center>'.$data['D3'].'</center></td>
		<td><center>'.$data['E1'].'</center></td>
		<td><center>'.$data['E2'].'</center></td>
		<td><center>'.$data['E3'].'</center></td>
		<td><center>'.$jumlah.'</center></td>
	</tr>';
$last = $data['ID_FAKULTAS'];
$total_a1 += $data['A1'];
$total_a2 += $data['A2'];
$total_a3 += $data['A3'];
$total_b1 += $data['B1'];
$total_b2 += $data['B2'];
$total_b3 += $data['B3'];
$total_c1 += $data['C1'];
$total_c2 += $data['C2'];
$total_c3 += $data['C3'];
$total_d1 += $data['D1'];
$total_d2 += $data['D2'];
$total_d3 += $data['D3'];
$total_e1 += $data['E1'];
$total_e2 += $data['E2'];
$total_e3 += $data['E3'];
$total += $jumlah;
}

$tampil .= '
	<tr class="ui-widget-header">
		<td style="text-align:right;">TOTAL</td>
		<td><center>'.$total_a1.'</center></td>
		<td><center>'.$total_a2.'</center></td>
		<td><center>'.$total_a3.'</center></td>
		<td><center>'.$total_b1.'</center></td>
		<td><center>'.$total_b2.'</center></td>
		<td><center>'.$total_b3.'</center></td>
		<td><center>'.$total_c1.'</center></td>
		<td><center>'.$total_c2.'</center></td>
		<td><center>'.$total_c3.'</center></td>
		<td><center>'.$total_d1.'</center></td>
		<td><center>'.$total_d2.'</center></td>
		<td><center>'.$total_d3.'</center></td>
		<td><center>'.$total_e1.'</center></td>
		<td><center>'.$total_e2.'</center></td>
		<td><center>'.$total_e3.'</center></td>
		<td><center>'.$total.'</center></td>
	</tr>
</table>
';
}else{
$tampil .= '
<table class="ui-widget">
	<tr style="background:navy;color:#fff;padding:5px;">
		<td colspan="7"><div class="center_title_bar">Profil Dosen Tidak Tetap dari Luar Universitas Airlangga Berdasarkan Umur, Tingkat Pendidikan, dan Jabatan Fungsional</div></td>
	</tr>
	<tr class="ui-widget-header" style="padding:5px;">
		<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
	</tr>
</table>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<th rowspan="3" style="vertical-align: middle;"><center>Fakultas</center></th>
			<th rowspan="3" style="vertical-align: middle;"><center>Jabatan Fungsional</center></th>
			<th colspan="15"><center>Kelompok Umur (tahun)</center></th>
			<th rowspan="3" style="vertical-align: middle;"><center>Jumlah</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th width="90" colspan="3"><center>&#60; 31</center></th>
			<th width="90" colspan="3"><center>31 &#8211; 40</center></th>
			<th width="90" colspan="3"><center>41 &#8211; 50</center></th>
			<th width="90" colspan="3"><center>51 &#8211; 60</center></th>
			<th width="90" colspan="3"><center>&#62; 60</center></th>
		</tr>
		<tr class="ui-widget-header">
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
			<th width="30"><center>S1</center></th>
			<th width="30"><center>S2</center></th>
			<th width="30"><center>S3</center></th>
		</tr>
';
$dt = $db_wh->QueryToArray("
select z.URUT, z.ID_FAKULTAS, fak.SINGKATAN_FAKULTAS, z.NM_JABATAN_FUNGSIONAL, 
coalesce(z.a1,0) as a1, coalesce(z.a2,0) as a2, coalesce(z.a3,0) as a3,
coalesce(y.b1,0) as b1, coalesce(y.b2,0) as b2, coalesce(y.b3,0) as b3,
coalesce(x.c1,0) as c1, coalesce(x.c2,0) as c2, coalesce(x.c3,0) as c3,
coalesce(w.d1,0) as d1, coalesce(w.d2,0) as d2, coalesce(w.d3,0) as d3,
coalesce(v.e1,0) as e1, coalesce(v.e2,0) as e2, coalesce(v.e3,0) as e3
from 
(
select fsg.URUT, fsg.ID_FAKULTAS, fsg.NM_JABATAN_FUNGSIONAL, a1, a2, a3 from
(
(
select distinct ID_FAKULTAS,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_FAKULTAS, sum(S3) as a3, sum(S2) as a2, sum(S1) as a1
from
(
select dsn.ID_FAKULTAS, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where dsn.USIA_PENGGUNA < 31 and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_FAKULTAS
) a on fsg.URUT = a.URUT and fsg.ID_FAKULTAS = a.ID_FAKULTAS
)
) z
left join
(
select fsg.URUT, fsg.ID_FAKULTAS, fsg.NM_JABATAN_FUNGSIONAL, b1, b2, b3 from
(
(
select distinct ID_FAKULTAS,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_FAKULTAS, sum(S3) as b3, sum(S2) as b2, sum(S1) as b1
from
(
select dsn.ID_FAKULTAS, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where (dsn.USIA_PENGGUNA between 31 and 40) and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_FAKULTAS
) b on fsg.URUT = b.URUT and fsg.ID_FAKULTAS = b.ID_FAKULTAS
)
) y on z.URUT = y.URUT and z.ID_FAKULTAS = y.ID_FAKULTAS
left join
(
select fsg.URUT, fsg.ID_FAKULTAS, fsg.NM_JABATAN_FUNGSIONAL, c1, c2, c3 from
(
(
select distinct ID_FAKULTAS,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_FAKULTAS, sum(S3) as c3, sum(S2) as c2, sum(S1) as c1
from
(
select dsn.ID_FAKULTAS, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where (dsn.USIA_PENGGUNA between 41 and 50) and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_FAKULTAS
) b on fsg.URUT = b.URUT and fsg.ID_FAKULTAS = b.ID_FAKULTAS
)
) x on z.URUT = x.URUT and z.ID_FAKULTAS = x.ID_FAKULTAS
left join
(
select fsg.URUT, fsg.ID_FAKULTAS, fsg.NM_JABATAN_FUNGSIONAL, d1, d2, d3 from
(
(
select distinct ID_FAKULTAS,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_FAKULTAS, sum(S3) as d3, sum(S2) as d2, sum(S1) as d1
from
(
select dsn.ID_FAKULTAS, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where (dsn.USIA_PENGGUNA between 51 and 60) and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_FAKULTAS
) b on fsg.URUT = b.URUT and fsg.ID_FAKULTAS = b.ID_FAKULTAS
)
) w on z.URUT = w.URUT and z.ID_FAKULTAS = w.ID_FAKULTAS
left join
(
select fsg.URUT, fsg.ID_FAKULTAS, fsg.NM_JABATAN_FUNGSIONAL, e1, e2, e3 from
(
(
select distinct ID_FAKULTAS,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A'
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else NM_JABATAN_FUNGSIONAL end as URUT,
case when (NM_JABATAN_FUNGSIONAL is null or NM_JABATAN_FUNGSIONAL like '%PNS%') then 'BELUM MEMILIKI JABATAN FUNGSIONAL' 
else NM_JABATAN_FUNGSIONAL end as NM_JABATAN_FUNGSIONAL
from WH_DOSEN
) fsg
left join
(
select URUT, ID_FAKULTAS, sum(S3) as e3, sum(S2) as e2, sum(S1) as e1
from
(
select dsn.ID_FAKULTAS, 
case when dsn.PENDIDIKAN_TERAKHIR = 'S3' then 1 else 0 end as S3,
case when dsn.PENDIDIKAN_TERAKHIR = 'S2' then 1 else 0 end as S2,
case when dsn.PENDIDIKAN_TERAKHIR = 'S1' then 1 else 0 end as S1,
case when (dsn.NM_JABATAN_FUNGSIONAL is null or dsn.NM_JABATAN_FUNGSIONAL like '%PNS%') then 'A' 
when NM_JABATAN_FUNGSIONAL = 'GURU BESAR' then 'Z'
else dsn.NM_JABATAN_FUNGSIONAL end as URUT
from WH_DOSEN dsn
where dsn.USIA_PENGGUNA > 60 and dsn.USIA_PENGGUNA is not null and dsn.STATUS_DOSEN = 'LB'
)
group by URUT, ID_FAKULTAS
) b on fsg.URUT = b.URUT and fsg.ID_FAKULTAS = b.ID_FAKULTAS
)
) v on z.URUT = v.URUT and z.ID_FAKULTAS = v.ID_FAKULTAS
join FAKULTAS fak on z.ID_FAKULTAS = fak.ID_FAKULTAS
order by z.ID_FAKULTAS, z.URUT
");
foreach($dt as $data){
$jumlah = 
$data['A1'] + $data['A2'] + $data['A3'] +
$data['B1'] + $data['B2'] + $data['B3'] +
$data['C1'] + $data['C2'] + $data['C3'] +
$data['D1'] + $data['D2'] + $data['D3'] +
$data['E1'] + $data['E2'] + $data['E3']
;
$tampil .= '
	<tr class="ui-widget-content">';
$now = $data['ID_FAKULTAS'];
if($last!=$now){
$tampil .= '
		<td><center>'.$data['SINGKATAN_FAKULTAS'].'</center></td>
		<td>'.$data['NM_JABATAN_FUNGSIONAL'].'</td>
		<td><center>'.$data['A1'].'</center></td>
		<td><center>'.$data['A2'].'</center></td>
		<td><center>'.$data['A3'].'</center></td>
		<td><center>'.$data['B1'].'</center></td>
		<td><center>'.$data['B2'].'</center></td>
		<td><center>'.$data['B3'].'</center></td>
		<td><center>'.$data['C1'].'</center></td>
		<td><center>'.$data['C2'].'</center></td>
		<td><center>'.$data['C3'].'</center></td>
		<td><center>'.$data['D1'].'</center></td>
		<td><center>'.$data['D2'].'</center></td>
		<td><center>'.$data['D3'].'</center></td>
		<td><center>'.$data['E1'].'</center></td>
		<td><center>'.$data['E2'].'</center></td>
		<td><center>'.$data['E3'].'</center></td>
		<td><center>'.$jumlah.'</center></td>
	</tr>';
}else{
$tampil .= '
		<td><center></center></td>
		<td>'.$data['NM_JABATAN_FUNGSIONAL'].'</td>
		<td><center>'.$data['A1'].'</center></td>
		<td><center>'.$data['A2'].'</center></td>
		<td><center>'.$data['A3'].'</center></td>
		<td><center>'.$data['B1'].'</center></td>
		<td><center>'.$data['B2'].'</center></td>
		<td><center>'.$data['B3'].'</center></td>
		<td><center>'.$data['C1'].'</center></td>
		<td><center>'.$data['C2'].'</center></td>
		<td><center>'.$data['C3'].'</center></td>
		<td><center>'.$data['D1'].'</center></td>
		<td><center>'.$data['D2'].'</center></td>
		<td><center>'.$data['D3'].'</center></td>
		<td><center>'.$data['E1'].'</center></td>
		<td><center>'.$data['E2'].'</center></td>
		<td><center>'.$data['E3'].'</center></td>
		<td><center>'.$jumlah.'</center></td>
	</tr>';
}
$last = $data['ID_FAKULTAS'];
$total_a1 += $data['A1'];
$total_a2 += $data['A2'];
$total_a3 += $data['A3'];
$total_b1 += $data['B1'];
$total_b2 += $data['B2'];
$total_b3 += $data['B3'];
$total_c1 += $data['C1'];
$total_c2 += $data['C2'];
$total_c3 += $data['C3'];
$total_d1 += $data['D1'];
$total_d2 += $data['D2'];
$total_d3 += $data['D3'];
$total_e1 += $data['E1'];
$total_e2 += $data['E2'];
$total_e3 += $data['E3'];
$total += $jumlah;
}

$tampil .= '
	<tr class="ui-widget-header">
		<td colspan="2" style="text-align:right;">TOTAL UNAIR</td>
		<td><center>'.$total_a1.'</center></td>
		<td><center>'.$total_a2.'</center></td>
		<td><center>'.$total_a3.'</center></td>
		<td><center>'.$total_b1.'</center></td>
		<td><center>'.$total_b2.'</center></td>
		<td><center>'.$total_b3.'</center></td>
		<td><center>'.$total_c1.'</center></td>
		<td><center>'.$total_c2.'</center></td>
		<td><center>'.$total_c3.'</center></td>
		<td><center>'.$total_d1.'</center></td>
		<td><center>'.$total_d2.'</center></td>
		<td><center>'.$total_d3.'</center></td>
		<td><center>'.$total_e1.'</center></td>
		<td><center>'.$total_e2.'</center></td>
		<td><center>'.$total_e3.'</center></td>
		<td><center>'.$total.'</center></td>
	</tr>
</table>
';
}

$smarty->assign('tampil', $tampil);
$smarty->display('se-sd-71b.tpl');