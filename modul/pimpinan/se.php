<?PHP
include('config.php');

$db->Query("SELECT USERNAME, ID_KAPRODI, A.ID_DEKAN, B.ID_WADEK1, C.ID_WADEK2, D.ID_WADEK3, A.ID_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI
FROM PENGGUNA 
LEFT JOIN DOSEN ON DOSEN.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA
LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_KAPRODI = DOSEN.ID_DOSEN
LEFT JOIN FAKULTAS A ON A.ID_DEKAN = PENGGUNA.ID_PENGGUNA
LEFT JOIN FAKULTAS B ON B.ID_DEKAN = PENGGUNA.ID_PENGGUNA
LEFT JOIN FAKULTAS C ON C.ID_DEKAN = PENGGUNA.ID_PENGGUNA
LEFT JOIN FAKULTAS D ON D.ID_DEKAN = PENGGUNA.ID_PENGGUNA
WHERE DOSEN.ID_PENGGUNA = '{$user->ID_PENGGUNA}'");
$akses=$db->FetchAssoc();

	$_SESSION['TAHUN'] = date("Y") -1;
	
	// By Pass thn 2013 dulu --> Fathoni Umaha
	$_SESSION['TAHUN'] = 2013;

	if(isset($_REQUEST['id_fakultas']) or $akses['ID_DEKAN'] != '' or $akses['ID_WADEK1'] != '' or $akses['ID_WADEK2'] != '' or $akses['ID_WADEK3'] != ''){
		
		$_SESSION['ID_FAK'] = $_GET['id_fakultas'];
		
		$_SESSION['STATUS'] = 2;			
		$_SESSION['ID_PRODI'] = '';
		$_SESSION['JENJANG'] = '';
		
		
		$db->Query("select nm_fakultas from fakultas
					where id_fakultas='$_SESSION[ID_FAK]'");
		$data=$db->FetchAssoc();
		$_SESSION['NM_FAK'] = $data['NM_FAKULTAS'];
		$_SESSION['HEADER'] = $data['NM_FAKULTAS'];
		
	}elseif(isset($_REQUEST['id_prodi']) or $akses['ID_KAPRODI'] != ''){
		$_SESSION['ID_FAK'] = '';
		$_SESSION['ID_PRODI'] = $_REQUEST['id_prodi'];
		$_SESSION['STATUS'] = 3;
		$db->Query("select nm_jenjang,nm_program_studi from program_studi
								join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
								where id_program_studi='$_SESSION[ID_PRODI]'");
		$data=$db->FetchAssoc();
		$_SESSION['JENJANG'] = $data['NM_JENJANG'];
		$_SESSION['PRODI'] = $data['NM_PROGRAM_STUDI'];
		$_SESSION['HEADER'] = $data['NM_PROGRAM_STUDI'];
		
	}else{
		$_SESSION['ID_FAK'] = '';	
		$_SESSION['ID_PRODI'] = '';	
		$_SESSION['STATUS'] = 1;
		$_SESSION['JENJANG'] = '';
		$_SESSION['HEADER'] = 'UNIVERSITAS MAARIF HASYIM LATIF';
	}

header('Location: index.php');
$smarty->display("se.tpl");
?>