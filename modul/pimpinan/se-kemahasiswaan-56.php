<?php
	include 'config.php';
	include 'models/se-kemahasiswaan-56-model.php';
	//include 'models/se-kemahasiswaan-52-model.php';
	//unset($_SESSION['jenjang']);
	//$_SESSION['jenjang'] = 'Diploma';
	class Se_Kemahasiswaan_56_Controller{
	
		public $model = array();
		
		public function DataUniversitas(){
			$this->model = new Se_Kemahasiswaan_56_Model();
			$this->model->requestDataUniversitas();
		}
		
		public function DataFakultas($id_fakultas){
			$this->model = new Se_Kemahasiswaan_56_Model();
			$this->model->requestDataFakultas($id_fakultas);
		}
		
		public function DataProdi($id_prodi){
			$this->model = new Se_Kemahasiswaan_56_Model();
			$this->model->requestDataProdi($id_prodi);
		}
		
	}
	
	$kmhs = new Se_Kemahasiswaan_56_Controller();
	
	if(isset($_GET['f']) || isset($_GET['p'])){
		if(($_GET['f']!="")){
			$smarty->assign('f', ($_GET['f']));
			$id_fakultas = $_GET['f'];
			$kmhs->DataFakultas($id_fakultas);
		}
		else if(($_GET['p']!="")){
			if($_SESSION['aksesRole']=='GPM'){
				include 'controllers/url-encrypt-controller.php';
				$enc = new urlEncrypt;
				$smarty->assign('n', $enc->decrypt($_GET['n']));
				$smarty->assign('title', $enc->decrypt($_GET['n']));
				$smarty->assign('p', $enc->decrypt($_GET['p']));
				$id_prodi = $enc->decrypt($_GET['p']);				
			}
			else{
				$smarty->assign('n', ($_GET['n']));
				$smarty->assign('title', ($_GET['n']));
				$smarty->assign('p', ($_GET['p']));
				$id_prodi = $_GET['p'];
			}
			$kmhs->DataProdi($id_prodi);
		}
		else{
			$kmhs->DataUniversitas();
		}
	}
	else{
		$kmhs->DataUniversitas();
	}
	
	if(isset($_GET['download'])){
		$n = str_replace(" ", "-", $_GET['n']);
		echo '
		<script type="text/javascript" src="../../js/jquery-1.5.1.js"></script>
		<script type="text/javascript" src="../../js/jquery-ui-pimpinan.min.js"></script>
		<style>
		body{width:600px;font-size:12px;font-family:sans-serif;margin:0px;padding:0px;}
		.center_title_bar {
			background: navy;
			color: #fff;
			font-size: 14px;
			font-weight: bold;
			padding: 10px;
			text-transform: uppercase;
		}
		table{
			width:600px;
			font-size: 12px;
		}
		table caption { 
			padding: 2px; 
			font-size: 12px;
			font-weight: bold;
		}
		table tr td {
			border: 1px solid #969BA5;
			padding: 3px;
			font-size: 12px;
			/*vertical-align: top;*/
		}
		.ui-widget { font-family: Lucida Grande, Lucida Sans, Arial, sans-serif; font-size: 1.1em; }
		.ui-widget .ui-widget { font-size: 1em; }
		.ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button { font-family: Lucida Grande, Lucida Sans, Arial, sans-serif; font-size: 1em; }
		.ui-widget-content { border: 1px solid #dddddd; background: #f2f5f7 url(images/ui-bg_highlight-hard_100_f2f5f7_1x100.png) 50% top repeat-x; color: #362b36; }
		.ui-widget-content a { color: #362b36; }
		.ui-widget-header { border: 1px solid #aed0ea; background: #deedf7 url(images/ui-bg_highlight-soft_100_deedf7_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;}
		.ui-widget-header a { color: #222222; }
		</style>';
		
		$date = date("Y-m-d");
		$filename ="kemahasiswaan-tabel-56-{$date}.xls";
		header('Content-type: application/ms-excel');
		header('Content-Disposition: attachment; filename='.$filename);
		
	}
	
	
	$smarty->assign('download', $_GET['download']);
	//$smarty->assign('title', ($_GET['n']));
	$smarty->assign('f', ($_GET['f']));
	$smarty->assign('p', ($_GET['p']));
	//$smarty->assign('n', ($_GET['n']));
	$smarty->assign('t', ($_GET['t']));

	
	$smarty->assign('jml_data', $kmhs->model->jml_data);
	$smarty->assign('jml_maba_A', $kmhs->model->jml_maba_A);
	$smarty->assign('jml_maba_B', $kmhs->model->jml_maba_B);
	$smarty->assign('jml_maba_C', $kmhs->model->jml_maba_C);

	$smarty->assign('persen_A', $kmhs->model->persen_A);
	$smarty->assign('persen_B', $kmhs->model->persen_B);
	$smarty->assign('persen_C', $kmhs->model->persen_C);

	$smarty->assign('tot_A', $kmhs->model->tot_A);
	$smarty->assign('tot_B', $kmhs->model->tot_B);
	$smarty->assign('tot_C', $kmhs->model->tot_C);

	$smarty->assign('tot_persen_A', $kmhs->model->tot_persen_A);
	$smarty->assign('tot_persen_B', $kmhs->model->tot_persen_B);
	$smarty->assign('tot_persen_C', $kmhs->model->tot_persen_C);

	$smarty->assign('provinsi', $kmhs->model->provinsi);
	
	$smarty->display('se-kemahasiswaan-56.tpl');
?>