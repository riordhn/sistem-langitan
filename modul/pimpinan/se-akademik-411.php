<?PHP
include('config.php');

$smarty->assign('tahun',$_SESSION['TAHUN']);


if ($_SESSION['STATUS']==1 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);


$ik412a_R = $db_wh->QueryToArray("select distinct nama_kelompok as layanan
								from wh_eva_maba
								order by layanan");
$smarty->assign('ik412a_R', $ik412a_R);

$ik412_R = $db_wh->QueryToArray("select nama_kelompok as layanan,substr(tahun,1,4) as tahun,
								round(avg(rata),2) as ik 
								from wh_eva_maba
								where id_jenjang=1
								group by nama_kelompok,substr(tahun,1,4)
								order by substr(tahun,1,4),nama_kelompok");
$smarty->assign('ik412_R', $ik412_R);

$ik412a_D3 = $db_wh->QueryToArray("select distinct nama_kelompok as layanan
								from wh_eva_maba
								order by layanan");
$smarty->assign('ik412a_D3', $ik412a_D3);

$ik412_D3 = $db_wh->QueryToArray("select nama_kelompok as layanan,substr(tahun,1,4) as tahun,
								round(avg(rata),2) as ik 
								from wh_eva_maba
								where id_jenjang=5
								group by nama_kelompok,substr(tahun,1,4)
								order by substr(tahun,1,4),nama_kelompok");
$smarty->assign('ik412_D3', $ik412_D3);

$ik412a_S2 = $db_wh->QueryToArray("select distinct nama_kelompok as layanan
								from wh_eva_maba
								order by layanan");
$smarty->assign('ik412a_S2', $ik412a_S2);

$ik412_S2 = $db_wh->QueryToArray("select nama_kelompok as layanan,substr(tahun,1,4) as tahun,
								round(avg(rata),2) as ik 
								from wh_eva_maba
								where id_jenjang=2
								group by nama_kelompok,substr(tahun,1,4)
								order by substr(tahun,1,4),nama_kelompok");
$smarty->assign('ik412_S2', $ik412_S2);

$ik412a_S3 = $db_wh->QueryToArray("select distinct nama_kelompok as layanan
								from wh_eva_maba
								order by layanan");
$smarty->assign('ik412a_S3', $ik412a_S3);

$ik412_S3 = $db_wh->QueryToArray("select nama_kelompok as layanan,substr(tahun,1,4) as tahun,
								round(avg(rata),2) as ik 
								from wh_eva_maba
								where id_jenjang=3
								group by nama_kelompok,substr(tahun,1,4)
								order by substr(tahun,1,4),nama_kelompok");
$smarty->assign('ik412_S3', $ik412_S3);

$ik412a_PROFESI = $db_wh->QueryToArray("select distinct nama_kelompok as layanan
								from wh_eva_maba
								order by layanan");
$smarty->assign('ik412a_PROFESI', $ik412a_PROFESI);

$ik412_PROFESI = $db_wh->QueryToArray("select nama_kelompok as layanan,substr(tahun,1,4) as tahun,
								round(avg(rata),2) as ik 
								from wh_eva_maba
								where id_jenjang=9
								group by nama_kelompok,substr(tahun,1,4)
								order by substr(tahun,1,4),nama_kelompok");
$smarty->assign('ik412_PROFESI', $ik412_PROFESI);


$smarty->display("se-akademik-411-universitas.tpl");
}


if ($_SESSION['STATUS']==2 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('fakultas', $_SESSION['NM_FAK']);

$ik412a_R = $db_wh->QueryToArray("select distinct nama_kelompok as layanan
								from wh_eva_maba
								order by layanan");
$smarty->assign('ik412a_R', $ik412a_R);

$ik412_R = $db_wh->QueryToArray("select nama_kelompok as layanan,substr(tahun,1,4) as tahun,
								round(avg(rata),2) as ik 
								from wh_eva_maba
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1
								group by nama_kelompok,substr(tahun,1,4)
								order by substr(tahun,1,4),nama_kelompok");
$smarty->assign('ik412_R', $ik412_R);

$ik412a_D3 = $db_wh->QueryToArray("select distinct nama_kelompok as layanan
								from wh_eva_maba
								order by layanan");
$smarty->assign('ik412a_D3', $ik412a_D3);

$ik412_D3 = $db_wh->QueryToArray("select nama_kelompok as layanan,substr(tahun,1,4) as tahun,
								round(avg(rata),2) as ik 
								from wh_eva_maba
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5
								group by nama_kelompok,substr(tahun,1,4)
								order by substr(tahun,1,4),nama_kelompok");
$smarty->assign('ik412_D3', $ik412_D3);

$ik412a_S2 = $db_wh->QueryToArray("select distinct nama_kelompok as layanan
								from wh_eva_maba
								order by layanan");
$smarty->assign('ik412a_S2', $ik412a_S2);

$ik412_S2 = $db_wh->QueryToArray("select nama_kelompok as layanan,substr(tahun,1,4) as tahun,
								round(avg(rata),2) as ik 
								from wh_eva_maba
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2
								group by nama_kelompok,substr(tahun,1,4)
								order by substr(tahun,1,4),nama_kelompok");
$smarty->assign('ik412_S2', $ik412_S2);

$ik412a_S3 = $db_wh->QueryToArray("select distinct nama_kelompok as layanan
								from wh_eva_maba
								order by layanan");
$smarty->assign('ik412a_S3', $ik412a_S3);

$ik412_S3 = $db_wh->QueryToArray("select nama_kelompok as layanan,substr(tahun,1,4) as tahun,
								round(avg(rata),2) as ik 
								from wh_eva_maba
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3
								group by nama_kelompok,substr(tahun,1,4)
								order by substr(tahun,1,4),nama_kelompok");
$smarty->assign('ik412_S3', $ik412_S3);

$ik412a_PROFESI = $db_wh->QueryToArray("select distinct nama_kelompok as layanan
								from wh_eva_maba
								order by layanan");
$smarty->assign('ik412a_PROFESI', $ik412a_PROFESI);

$ik412_PROFESI = $db_wh->QueryToArray("select nama_kelompok as layanan,substr(tahun,1,4) as tahun,
								round(avg(rata),2) as ik 
								from wh_eva_maba
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9
								group by nama_kelompok,substr(tahun,1,4)
								order by substr(tahun,1,4),nama_kelompok");
$smarty->assign('ik412_PROFESI', $ik412_PROFESI);


$smarty->display("se-akademik-411-fakultas.tpl");
}

if ($_SESSION['STATUS']==3 ) {
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('jenjang', $_SESSION['JENJANG']);
$smarty->assign('prodi', $_SESSION['PRODI']);

$ik412a_R = $db_wh->QueryToArray("select distinct nama_kelompok as layanan
								from wh_eva_maba
								where id_program_studi='$_SESSION[ID_PRODI]' 
								order by layanan");
$smarty->assign('ik412a_R', $ik412a_R);

$ik412_R = $db_wh->QueryToArray("select nama_kelompok as layanan,substr(tahun,1,4) as tahun,
								round(avg(rata),2) as ik 
								from wh_eva_maba
								where id_program_studi='$_SESSION[ID_PRODI]'
								group by nama_kelompok,substr(tahun,1,4)
								order by substr(tahun,1,4),nama_kelompok");
							

$smarty->assign('ik412_R', $ik412_R);

$smarty->display("se-akademik-411-prodi.tpl");
}

?>