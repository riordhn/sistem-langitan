<?php
//Yudi Sulistya, 28-04-2012
include 'config.php';
require_once ('ociFunction.php');

$kd_fak=getData("select id_fakultas, nm_fakultas from fakultas order by id_fakultas");
$smarty->assign('KD_FAK', $kd_fak);

if ($request_method == 'GET' or $request_method == 'POST'){

$display = get('id','');

$fak=getData("select 'FAKULTAS '||upper(fak.nm_fakultas) as fakultas
from fakultas fak
where fak.id_fakultas=$display");
$smarty->assign('FAK', $fak);

$data = getData("
select a.urut, a.jabatan, 
coalesce(a.a1,0) as a1, coalesce(a.a2,0) as a2, coalesce(a.a3,0) as a3,
coalesce(b.b1,0) as b1, coalesce(b.b2,0) as b2, coalesce(b.b3,0) as b3,
coalesce(c.c1,0) as c1, coalesce(c.c2,0) as c2, coalesce(c.c3,0) as c3,
coalesce(d.d1,0) as d1, coalesce(d.d2,0) as d2, coalesce(d.d3,0) as d3,
coalesce(e.e1,0) as e1, coalesce(e.e2,0) as e2, coalesce(e.e3,0) as e3
from 
(
select fsg.urut, fsg.jabatan, a1, a2, a3 from
(
select distinct
case when (a.id_jabatan_fungsional>4) then 0 else a.id_jabatan_fungsional end as urut,
case when (a.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a
) fsg
left join
(
select id_jabatan_fungsional, sum(S3) as a3, sum(S2) as a2, sum(S1) as a1
from
(
select dsn.id_pengguna,
case when pda.id_pendidikan_akhir=10 then 1 else 0 end as S3,
case when pda.id_pendidikan_akhir in (1,7,8,9) then 1 else 0 end as S2,
case when pda.id_pendidikan_akhir=2 then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) < 31) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
group by id_pengguna
) and dsn.status_dosen = 'PNS'
) x left join
(select distinct max(id_jabatan_fungsional) as id_jabatan_fungsional, id_pengguna
from
( 
select sjf.id_pengguna, sjf.id_jabatan_fungsional
from sejarah_jabatan_fungsional sjf
where sjf.id_pengguna in
(
select distinct id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
and sjf.tmt_sej_jab_fungsional is not null
and to_char(sjf.tmt_sej_jab_fungsional,'YYYY')<=to_char(sysdate,'YYYY')
)
group by id_pengguna
) y on x.id_pengguna=y.id_pengguna
group by id_jabatan_fungsional
) mat on fsg.urut=mat.id_jabatan_fungsional
) a
left join 
(
select fsg.urut, fsg.jabatan, b1, b2, b3 from
(
select distinct
case when (a.id_jabatan_fungsional>4) then 0 else a.id_jabatan_fungsional end as urut,
case when (a.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a
) fsg
left join
(
select id_jabatan_fungsional, sum(S3) as b3, sum(S2) as b2, sum(S1) as b1
from
(
select dsn.id_pengguna,
case when pda.id_pendidikan_akhir=10 then 1 else 0 end as S3,
case when pda.id_pendidikan_akhir in (1,7,8,9) then 1 else 0 end as S2,
case when pda.id_pendidikan_akhir=2 then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
group by id_pengguna
) and dsn.status_dosen = 'PNS'
) x left join
(select distinct max(id_jabatan_fungsional) as id_jabatan_fungsional, id_pengguna
from
( 
select sjf.id_pengguna, sjf.id_jabatan_fungsional
from sejarah_jabatan_fungsional sjf
where sjf.id_pengguna in
(
select distinct id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
and sjf.tmt_sej_jab_fungsional is not null
and to_char(sjf.tmt_sej_jab_fungsional,'YYYY')<=to_char(sysdate,'YYYY')
)
group by id_pengguna
) y on x.id_pengguna=y.id_pengguna
group by id_jabatan_fungsional
) mat on fsg.urut=mat.id_jabatan_fungsional
) b on a.urut=b.urut
left join 
(
select fsg.urut, fsg.jabatan, c1, c2, c3 from
(
select distinct
case when (a.id_jabatan_fungsional>4) then 0 else a.id_jabatan_fungsional end as urut,
case when (a.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a
) fsg
left join
(
select id_jabatan_fungsional, sum(S3) as c3, sum(S2) as c2, sum(S1) as c1
from
(
select dsn.id_pengguna,
case when pda.id_pendidikan_akhir=10 then 1 else 0 end as S3,
case when pda.id_pendidikan_akhir in (1,7,8,9) then 1 else 0 end as S2,
case when pda.id_pendidikan_akhir=2 then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
group by id_pengguna
) and dsn.status_dosen = 'PNS'
) x left join
(select distinct max(id_jabatan_fungsional) as id_jabatan_fungsional, id_pengguna
from
( 
select sjf.id_pengguna, sjf.id_jabatan_fungsional
from sejarah_jabatan_fungsional sjf
where sjf.id_pengguna in
(
select distinct id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
and sjf.tmt_sej_jab_fungsional is not null
and to_char(sjf.tmt_sej_jab_fungsional,'YYYY')<=to_char(sysdate,'YYYY')
)
group by id_pengguna
) y on x.id_pengguna=y.id_pengguna
group by id_jabatan_fungsional
) mat on fsg.urut=mat.id_jabatan_fungsional
) c on a.urut=c.urut
left join 
(
select fsg.urut, fsg.jabatan, d1, d2, d3 from
(
select distinct
case when (a.id_jabatan_fungsional>4) then 0 else a.id_jabatan_fungsional end as urut,
case when (a.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a
) fsg
left join
(
select id_jabatan_fungsional, sum(S3) as d3, sum(S2) as d2, sum(S1) as d1
from
(
select dsn.id_pengguna,
case when pda.id_pendidikan_akhir=10 then 1 else 0 end as S3,
case when pda.id_pendidikan_akhir in (1,7,8,9) then 1 else 0 end as S2,
case when pda.id_pendidikan_akhir=2 then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 60) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
group by id_pengguna
) and dsn.status_dosen = 'PNS'
) x left join
(select distinct max(id_jabatan_fungsional) as id_jabatan_fungsional, id_pengguna
from
( 
select sjf.id_pengguna, sjf.id_jabatan_fungsional
from sejarah_jabatan_fungsional sjf
where sjf.id_pengguna in
(
select distinct id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
and sjf.tmt_sej_jab_fungsional is not null
and to_char(sjf.tmt_sej_jab_fungsional,'YYYY')<=to_char(sysdate,'YYYY')
)
group by id_pengguna
) y on x.id_pengguna=y.id_pengguna
group by id_jabatan_fungsional
) mat on fsg.urut=mat.id_jabatan_fungsional
) d on a.urut=d.urut
left join 
(
select fsg.urut, fsg.jabatan, e1, e2, e3 from
(
select distinct
case when (a.id_jabatan_fungsional>4) then 0 else a.id_jabatan_fungsional end as urut,
case when (a.id_jabatan_fungsional>4) then 'Belum memiliki jabatan fungsional' else a.nm_jabatan_fungsional end as jabatan
from jabatan_fungsional a
) fsg
left join
(
select id_jabatan_fungsional, sum(S3) as e3, sum(S2) as e2, sum(S1) as e1
from
(
select dsn.id_pengguna,
case when pda.id_pendidikan_akhir=10 then 1 else 0 end as S3,
case when pda.id_pendidikan_akhir in (1,7,8,9) then 1 else 0 end as S2,
case when pda.id_pendidikan_akhir=2 then 1 else 0 end as S1
from sejarah_pendidikan pdd
left join pengguna pgg on pgg.id_pengguna=pdd.id_pengguna
left join dosen dsn on dsn.id_pengguna=pdd.id_pengguna
left join jabatan_fungsional jab on jab.id_jabatan_fungsional=dsn.id_jabatan_fungsional
left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
where (trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 60) and
pdd.id_pengguna||pdd.tahun_lulus_pendidikan in
(
select distinct id_pengguna||max(tahun_lulus_pendidikan) from sejarah_pendidikan where id_pengguna in
(
select id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
group by id_pengguna
) and dsn.status_dosen = 'PNS'
) x left join
(select distinct max(id_jabatan_fungsional) as id_jabatan_fungsional, id_pengguna
from
( 
select sjf.id_pengguna, sjf.id_jabatan_fungsional
from sejarah_jabatan_fungsional sjf
where sjf.id_pengguna in
(
select distinct id_pengguna from dosen where id_program_studi in
(select id_program_studi from program_studi where id_fakultas like '$display')
and id_status_pengguna in
(select id_status_pengguna from status_pengguna where id_role=4 and status_aktif=1)
and status_dosen = 'PNS'
)
and sjf.tmt_sej_jab_fungsional is not null
and to_char(sjf.tmt_sej_jab_fungsional,'YYYY')<=to_char(sysdate,'YYYY')
)
group by id_pengguna
) y on x.id_pengguna=y.id_pengguna
group by id_jabatan_fungsional
) mat on fsg.urut=mat.id_jabatan_fungsional
) e on a.urut=e.urut
order by a.urut
");

$smarty->assign('DATA', $data);
}

$smarty->display('se-sumberdaya-71a.tpl');
?>