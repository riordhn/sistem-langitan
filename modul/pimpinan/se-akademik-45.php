<?PHP
include('config.php');

$smarty->assign('tahun',$_SESSION['TAHUN']);


if ($_SESSION['STATUS']==1 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);


$pendidikan_R = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]' and status='R'  and id_jenjang=1
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");

$smarty->assign('pendidikan_R', $pendidikan_R);
									
$pendidikan_AJ = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]' and status='AJ'  and id_jenjang=1
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");
$smarty->assign('pendidikan_AJ', $pendidikan_AJ);

$pendidikan_S2 = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]'  and id_jenjang=2
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");
$smarty->assign('pendidikan_S2', $pendidikan_S2);

$pendidikan_S3 = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]'  and id_jenjang=3
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");
$smarty->assign('pendidikan_S3', $pendidikan_S3);

$pendidikan_D3 = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile  
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]'  and id_jenjang=5
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");
$smarty->assign('pendidikan_D3', $pendidikan_D3);


$pendidikan_PROFESI = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]'  and id_jenjang=9
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");
$smarty->assign('pendidikan_PROFESI', $pendidikan_PROFESI);

$smarty->display("se-akademik-45-universitas.tpl");
}

if ($_SESSION['STATUS']==2 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('fakultas', $_SESSION['NM_FAK']);

$pendidikan_R = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]' and status='R' and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");

$smarty->assign('pendidikan_R', $pendidikan_R);
									
$pendidikan_AJ = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]' and status='AJ' and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");
$smarty->assign('pendidikan_AJ', $pendidikan_AJ);

$pendidikan_S2 = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]' and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");
$smarty->assign('pendidikan_S2', $pendidikan_S2);

$pendidikan_S3 = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]' and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");
$smarty->assign('pendidikan_S3', $pendidikan_S3);

$pendidikan_D3 = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile  
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]' and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");
$smarty->assign('pendidikan_D3', $pendidikan_D3);


$pendidikan_PROFESI = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]' and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");
$smarty->assign('pendidikan_PROFESI', $pendidikan_PROFESI);

$smarty->display("se-akademik-45-fakultas.tpl");
}

if ($_SESSION['STATUS']==3 ) {
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('jenjang', $_SESSION['JENJANG']);
$smarty->assign('prodi', $_SESSION['PRODI']);

$pendidikan_R = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]' and status='R' and id_program_studi='$_SESSION[ID_PRODI]'
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");
$smarty->assign('pendidikan_R', $pendidikan_R);
									
$pendidikan_AJ = $db_wh->QueryToArray("select substr(tahun,5,1) as posisi,nm_mata_kuliah,
									sum(pst) as pst,sum(pstulang)as pstulang,sum(kls_paralel)as kls_paralel,sum(dosen_hadir)as dosen_hadir,
									sum(case when jumlah_pertemuan_kelas_mk=0 then 1 else jumlah_pertemuan_kelas_mk end) as jumlah_pertemuan_kelas_mk,
									sum(nila)as nila,sum(nilab)as nilab,sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
									from wh_distribusi_nilai
									where substr(tahun,1,4)='$_SESSION[TAHUN]' and status='AJ' and id_program_studi='$_SESSION[ID_PRODI]'
									group by tahun,nm_mata_kuliah
									order by tahun,nm_mata_kuliah");
$smarty->assign('pendidikan_AJ', $pendidikan_AJ);
$smarty->display("se-akademik-45-prodi.tpl");
}

?>