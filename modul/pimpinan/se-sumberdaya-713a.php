<?php
//Yudi Sulistya, 28-04-2012
include 'config.php';
require_once ('ociFunction.php');

$unit_kerja=getData("select id_unit_kerja, upper(nm_unit_kerja)||' ('||upper(type_unit_kerja)||')' as unitkerja,
case when type_unit_kerja='REKTORAT' then 0 
when type_unit_kerja='LEMBAGA' then 1 
when type_unit_kerja='FAKULTAS' then 2 end as urut0,
case when id_fakultas is null then 0 else id_fakultas end as urut1
from unit_kerja where type_unit_kerja in ('REKTORAT', 'LEMBAGA', 'FAKULTAS')
order by urut0, urut1, unitkerja");
$smarty->assign('KD_UK', $unit_kerja);

if ($request_method == 'GET' or $request_method == 'POST')
{
		$display = get('id','');

		$uk=getData("select upper(nm_unit_kerja) as unitkerja
		from unit_kerja
		where id_unit_kerja=$display");
		$smarty->assign('UK', $uk);

		$ttl=getvar("select sum(A+B+C+D+E+F) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 55 then 1 else 0 end as E,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 55 then 1 else 0 end as F
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna in
						(
						select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where (id_unit_kerja like '$display'
						or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja like '$display')
						or uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja like '$display'))
						) and pdd.status_akhir=1
						group by id_pengguna
						)
						)");
		$smarty->assign('TTL', $ttl['TOTAL']);

		$gol=getData("select urut, nama_pendidikan_akhir, sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(F) as VI, sum(A+B+C+D+E+F) as total
						from
						(
						select case when pda.id_pendidikan_akhir=2 then 11 
						when pda.id_pendidikan_akhir=1 then 12
						when pda.id_pendidikan_akhir=10 then 13  
						when pda.id_pendidikan_akhir=9 then 10 
						when pda.id_pendidikan_akhir=8 then 9
						when pda.id_pendidikan_akhir=7 then 8   
						when pda.id_pendidikan_akhir=3 then 7  
						when pda.id_pendidikan_akhir=4 then 6 
						when pda.id_pendidikan_akhir=5 then 5 
						when pda.id_pendidikan_akhir=6 then 4 
						when pda.id_pendidikan_akhir=13 then 3 
						when pda.id_pendidikan_akhir=12 then 2 
						when pda.id_pendidikan_akhir=11 then 1 
						else 0 end as urut, pda.nama_pendidikan_akhir,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 55 then 1 else 0 end as E,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 55 then 1 else 0 end as F
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna in
						(
						select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where (id_unit_kerja like '$display'
						or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja like '$display')
						or uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja like '$display'))
						) and pdd.status_akhir=1
						group by id_pengguna
						)
						)
						group by urut, nama_pendidikan_akhir
						order by urut");
		$smarty->assign('GOL', $gol);

		$jml=getData("select sum(A) as I, sum(B) as II, sum(C) as III, sum(D) as IV, sum(E) as V, sum(F) as VI, sum(A+B+C+D+E+F) as total
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 55 then 1 else 0 end as E,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 55 then 1 else 0 end as F
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna in
						(
						select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where (id_unit_kerja like '$display'
						or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja like '$display')
						or uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja like '$display'))
						) and pdd.status_akhir=1
						group by id_pengguna
						)
						)");
		$smarty->assign('JML', $jml);

		$psn=getData("select case when sum(A)=0 then 0 else round((sum(A)/sum(A+B+C+D+E+F)*100),2) end as I,
						case when sum(B)=0 then 0 else round((sum(B)/sum(A+B+C+D+E+F)*100),2) end as II,
						case when sum(C)=0 then 0 else round((sum(C)/sum(A+B+C+D+E+F)*100),2) end as III,
						case when sum(D)=0 then 0 else round((sum(D)/sum(A+B+C+D+E+F)*100),2) end as IV,
						case when sum(E)=0 then 0 else round((sum(E)/sum(A+B+C+D+E+F)*100),2) end as V,
						case when sum(F)=0 then 0 else round((sum(F)/sum(A+B+C+D+E+F)*100),2) end as VI,
						round((sum(A+B+C+D+E+F)/sum(A+B+C+D+E+F)*100),2) as persen
						from
						(
						select 
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) is null then 1 else 0 end as A,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 0 and 30 then 1 else 0 end as B,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 31 and 40 then 1 else 0 end as C,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 41 and 50 then 1 else 0 end as D,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) between 51 and 55 then 1 else 0 end as E,
						case when trunc(months_between(sysdate,pgg.tgl_lahir_pengguna)/12) > 55 then 1 else 0 end as F
						from pegawai peg
						left join unit_kerja uk on uk.id_unit_kerja=peg.id_unit_kerja
						left join sejarah_pendidikan pdd on peg.id_pengguna=pdd.id_pengguna
						left join pendidikan_akhir pda on pda.id_pendidikan_akhir=pdd.id_pendidikan_akhir
						left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
						left join golongan gol on gol.id_golongan=peg.id_golongan
						where pdd.id_pengguna in
						(
						select distinct id_pengguna from sejarah_pendidikan where id_pengguna in
						(
						select id_pengguna from pegawai where (id_unit_kerja like '$display'
						or uk.id_program_studi in (select id_program_studi from unit_kerja where id_unit_kerja like '$display')
						or uk.id_fakultas in (select id_fakultas from unit_kerja where id_unit_kerja like '$display'))
						) and pdd.status_akhir=1
						group by id_pengguna
						)
						)");
		$smarty->assign('PSN', $psn);
}

$smarty->display('se-sumberdaya-713a.tpl');
?>