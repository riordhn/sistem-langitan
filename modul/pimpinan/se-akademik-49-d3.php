<?PHP
include('config.php');

$smarty->assign('tahun',$_SESSION['TAHUN']);


if ($_SESSION['STATUS']==1 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);

$ikn_D3 = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where  id_jenjang=5 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_D3', $ikn_D3);

$ik_D3 = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where  id_jenjang=5 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_D3', $ik_D3);

$smarty->display("se-akademik-49-d3-universitas.tpl");
}


if ($_SESSION['STATUS']==2 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('fakultas', $_SESSION['NM_FAK']);

$ikn_D3 = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_D3', $ikn_D3);

$ik_D3 = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_D3', $ik_D3);


$smarty->display("se-akademik-49-d3-fakultas.tpl");
}

if ($_SESSION['STATUS']==3 ) {
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('jenjang', $_SESSION['JENJANG']);
$smarty->assign('prodi', $_SESSION['PRODI']);

$ikn_R = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where id_program_studi='$_SESSION[ID_PRODI]' and jalur='R'
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_R', $ikn_R);

$ik_R = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah,NULL)),2) as kul,round(avg(coalesce(ik_praktikum,NULL)),2) as prk
								from wh_ik
								where id_program_studi='$_SESSION[ID_PRODI]' and jalur='R'
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_R', $ik_R);

$smarty->display("se-akademik-49-d3-prodi.tpl");
}

?>