<?php
include('config.php');

if($_SESSION['ID_PRODI'] != ''){
	$where = "AND ID_PROGRAM_STUDI = '".$_SESSION['ID_PRODI']."'";
	$header = $_SESSION['JENJANG'] . ' - '. $_SESSION['PRODI'];
	
}elseif($_SESSION['ID_FAK'] != ''){
	$where = "AND ID_FAKULTAS = '".$_SESSION['ID_FAK']."'";
	$header = 'FAKULTAS '.strtoupper($_SESSION['NM_FAK']);

}else{
	$where = "";
	$header = strtoupper('Universitas Airlangga');
}

if($_SESSION['ID_PRODI'] != ''){
	/*
$mhs_51 = $db->QueryToArray("select pem.tahun as THN_AKADEMIK_SMT, pem.id_program_studi, pem.id_fakultas, pem.id_jenjang, pem.id_jalur ,pem.peminat, dit.diterima, round(dit.diterima/pem.peminat*100, 2) as keketatan, daftar_ulang as MENDAFTAR_KEMBALI, SYSDATE AS TGL_UPDATE, rata_nilai, nilai_pmdk
from (
		select count(*) as peminat, tahun, id_program_studi, id_fakultas, id_jenjang, id_jalur 
		from (
		select thn_akademik_semester as tahun, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur, 
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_1
		union all 
		select thn_akademik_semester as tahun, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur, 
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_2
		union all 
		select thn_akademik_semester as tahun, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur, 
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_3
		union all 
		select thn_akademik_semester as tahun, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur, 
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_4
		)
		group by tahun, id_program_studi, id_fakultas, id_jenjang, id_jalur
) pem 
left join (
		select count(cmhs.id_program_studi) as diterima, semester.thn_akademik_semester as tahun, cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur
		from calon_mahasiswa_baru cmhs
		join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
		join semester on semester.id_semester = pen.id_semester
		join program_studi p on p.id_program_studi = cmhs.id_program_studi								
		group by thn_akademik_semester, cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur
) dit on pem.tahun = dit.tahun and pem.id_program_studi = dit.id_program_studi and pem.id_fakultas = dit.id_fakultas and pem.id_jenjang = dit.id_jenjang and pem.id_jalur = dit.id_jalur
left join (
	select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun , cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur,  round(avg(mahasiswa.nilai_skhun_mhs), 2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
	from calon_mahasiswa_baru cmhs
	join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
	join semester on semester.id_semester = pen.id_semester		
	join program_studi p on p.id_program_studi = cmhs.id_program_studi
	left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
	where p.id_jenjang in (1, 5) and mahasiswa.status IN ('R', 'I') and cmhs.nim_mhs is not null
	group by thn_akademik_semester, cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur
	
	union
	
	select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun , cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur,  round(avg(cms.ip_s1), 2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
	from calon_mahasiswa_baru cmhs
	join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
	join semester on semester.id_semester = pen.id_semester		
	join program_studi p on p.id_program_studi = cmhs.id_program_studi
	left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
	where ((p.id_jenjang in (2, 9) and mahasiswa.status = 'R') or (p.id_jenjang in (1) and mahasiswa.status = 'AJ'))  and cmhs.nim_mhs is not null
	group by thn_akademik_semester, cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur
	
	union
	
	select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun , cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur,  round(avg(cms.ip_s2), 2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
	from calon_mahasiswa_baru cmhs
	join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
	join semester on semester.id_semester = pen.id_semester		
	join program_studi p on p.id_program_studi = cmhs.id_program_studi
	left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
	where (p.id_jenjang in (3) and mahasiswa.status = 'R') and cmhs.nim_mhs is not null
	group by thn_akademik_semester, cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur
) daf on pem.tahun = daf.tahun and pem.id_program_studi = daf.id_program_studi and pem.id_fakultas = daf.id_fakultas and pem.id_jenjang = daf.id_jenjang and pem.id_jalur = daf.id_jalur
");

$db_wh->Query("DELETE WH_SE_MHS_THN_MASUK");

foreach($mhs_51 as $data){

$db_wh->Query("INSERT INTO WH_SE_MHS_THN_MASUK (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, PEMINAT, DITERIMA, KEKETATAN, MENDAFTAR_KEMBALI, ID_JALUR, TGL_UPDATE, RATA_UN, RATA)
VALUES
('$data[THN_AKADEMIK_SMT]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[PEMINAT]', '$data[DITERIMA]', '$data[KEKETATAN]', '$data[MENDAFTAR_KEMBALI]', '$data[ID_JALUR]', '$data[TGL_UPDATE]', '$data[RATA_NILAI]', '$data[NILAI_PMDK]')");

}
*/

$jalur_wh = $db_wh->QueryToArray("SELECT ID_JALUR FROM WH_SE_MHS_THN_MASUK WHERE ID_JALUR IS NOT NULL and THN_AKADEMIK_SMT <= '".$_SESSION['TAHUN']."' $where GROUP BY ID_JALUR");
$i=0;
foreach($jalur_wh as $data){
	$i++;
	$id_jalur .= $data['ID_JALUR'];
	if (count($jalur_wh) != $i)
		$id_jalur .= ', ';
		
}


$jalur = $db->QueryToArray("SELECT * FROM JALUR WHERE ID_JALUR IN (".$id_jalur.") ORDER BY NM_JALUR");

foreach($jalur as $data){
/*$mhs_r = $db_wh->QueryToArray("SELECT *
									FROM WH_SE_MHS_THN_MASUK
									WHERE ID_JALUR = '".$data['ID_JALUR']."' and THN_AKADEMIK_SMT <= '".$_SESSION['TAHUN']."' $where
									ORDER BY THN_AKADEMIK_SMT ASC");*/

$mhs_r = $db_wh->QueryToArray("SELECT THN_AKADEMIK_SEMESTER, C.* FROM 
(SELECT THN_AKADEMIK_SEMESTER FROM AUCC.semester@AUCCRO A WHERE THN_AKADEMIK_SEMESTER >= '2011' GROUP BY THN_AKADEMIK_SEMESTER) B
LEFT JOIN (
SELECT *
FROM WH_SE_MHS_THN_MASUK
WHERE ID_JALUR = '".$data['ID_JALUR']."' and THN_AKADEMIK_SMT <= '".$_SESSION['TAHUN']."' $where
) C ON C.THN_AKADEMIK_SMT = B.THN_AKADEMIK_SEMESTER		
WHERE THN_AKADEMIK_SEMESTER <= '".$_SESSION['TAHUN']."'
ORDER BY C.THN_AKADEMIK_SMT ASC");									

$tampil .= '

	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="7"><div class="center_title_bar">Mahasiswa Baru (Jalur '.$data['NM_JALUR'].') Berdasarkan Tahun Masuk</div></td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td><center>Tahun Angkatan</center></td>
		<td><center>Peminat</center></td>
		<td><center>Diterima</center></td>
		<td><center>% Keketatan persaingan</center></td>
		<td><center>Mendaftar Kembali</center></td>';
		if($data['ID_JALUR'] == 1){
		$tampil .= '<td><center>Rerata Nilai SNMPTN</center></td>';
		}else{
		$tampil .= '<td><center>Rerata Nilai PMDK</center></td>';
		}
		if($mhs_r[0]['ID_JENJANG'] == 2 or $mhs_r[0]['ID_JENJANG'] == 3 or $mhs_r[0]['ID_JENJANG'] == 9){
		$tampil .= '<td><center>Rerata IPK pada jenjang sebelumnya</center></td>';
		}else{
		$tampil .= '<td><center>Rerata Nilai UN</center></td>';
		}
	$tampil .= '</tr>
	<tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
	</tr>';
	
	$jumlah_peminat = 0;
	$jumlah_diterima = 0;
	$jumlah_daftar_ulang = 0;
	$rerata_persaingan = 0;
	$rerata_uan = 0;
	$rerata_pmdk = 0;
	$x = 0;
	
	foreach($mhs_r as $value){
	$tampil .= '<tr class="ui-widget-content">
		<td><center>'.$value['THN_AKADEMIK_SEMESTER'].'/'.($value['THN_AKADEMIK_SEMESTER']+1).'</center></td>
		<td><center>'.$value['PEMINAT'].'</center></td>
		<td><center>'.$value['DITERIMA'].'</center></td>
		<td><center>'.$value['KEKETATAN'].'</center></td>
		<td><center>'.$value['MENDAFTAR_KEMBALI'].'</center></td>';
		if($data['ID_JALUR'] == 1){	
		$tampil .= '<td><center>-</center></td>';
		}else{
		$tampil .= '<td><center>'.$value['RATA'].'</center></td>';
		}
		$tampil .= '<td><center>-</center></td>
	</tr>';
	$jumlah_peminat = $jumlah_peminat + $value['PEMINAT'];
	$jumlah_diterima = $jumlah_diterima + $value['DITERIMA'];
	$jumlah_daftar_ulang = $jumlah_daftar_ulang + $value['MENDAFTAR_KEMBALI'];
	$rerata_persaingan = $rerata_persaingan + $value['KEKETATAN'];
	$rerata_uan = $rerata_uan + $value['RATA_UN'];
	if($data['ID_JALUR'] != 1){	
	$rerata_pmdk = $rerata_pmdk + $value['RATA'];
	}
	if($value['KEKETATAN'] != ''){
		$x++;
	}
	
	}
	$tampil .= '<tr class="ui-widget-content">
		<td><center>Jumlah</center></td>
		<td><center>'.$jumlah_peminat.'</center></td>
		<td><center>'.$jumlah_diterima.'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.$jumlah_daftar_ulang.'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td style="background:#000;">&nbsp;</td>
	</tr>
	<tr class="ui-widget-content">
		<td><center>Rerata</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.round($rerata_persaingan/$x, 2).'</center></td>
		<td style="background:#000;">&nbsp;</td>';
		if($rerata_pmdk > 0){	
			$tampil .= '<td><center>'.round($rerata_pmdk/count($mhs_r), 2).'</center></td>';
		}else{
			$tampil .= '<td><center>-</center></td>';
		}
		
		$tampil .= '<td><center>-</center></td>
	</tr>
</table><br />
';									

}


}
elseif($_SESSION['ID_FAK'] != ''){

/*
$mhs_51 = $db->QueryToArray("select pem.tahun as THN_AKADEMIK_SMT, pem.id_fakultas, pem.id_jalur ,pem.peminat, dit.diterima, round(dit.diterima/pem.peminat*100, 2) as keketatan,
daftar_ulang as MENDAFTAR_KEMBALI, round(rata_nilai, 2) as NILAI_RATA, pem.id_jenjang, SYSDATE AS TGL_UPDATE , nilai_pmdk
from (
								select count(*) as peminat, tahun, id_fakultas, id_jalur, id_jenjang
								from (
								select thn_akademik_semester as tahun, p.id_fakultas, cmhs.id_jalur, p.id_jenjang, 
								cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
																	from calon_mahasiswa_baru cmhs
																	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
																	join semester on semester.id_semester = pen.id_semester										
																	join program_studi p on p.id_program_studi = cmhs.id_pilihan_1
								union 
								select thn_akademik_semester as tahun, p.id_fakultas, cmhs.id_jalur, p.id_jenjang,
								cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
																	from calon_mahasiswa_baru cmhs
																	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
																	join semester on semester.id_semester = pen.id_semester										
																	join program_studi p on p.id_program_studi = cmhs.id_pilihan_2
								union 
								select thn_akademik_semester as tahun, p.id_fakultas, cmhs.id_jalur, p.id_jenjang,
								cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
																	from calon_mahasiswa_baru cmhs
																	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
																	join semester on semester.id_semester = pen.id_semester										
																	join program_studi p on p.id_program_studi = cmhs.id_pilihan_3
								union 
								select thn_akademik_semester as tahun, p.id_fakultas, cmhs.id_jalur, p.id_jenjang,
								cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
																	from calon_mahasiswa_baru cmhs
																	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
																	join semester on semester.id_semester = pen.id_semester										
																	join program_studi p on p.id_program_studi = cmhs.id_pilihan_4
								)
								group by tahun, id_fakultas, id_jalur, id_jenjang
						) pem 
						left join (
								select count(cmhs.id_program_studi) as diterima, semester.thn_akademik_semester as tahun, p.id_fakultas,pen.id_jalur, p.id_jenjang
								from calon_mahasiswa_baru cmhs
								join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
								join semester on semester.id_semester = pen.id_semester
								join program_studi p on p.id_program_studi = cmhs.id_program_studi	
								group by thn_akademik_semester,  p.id_fakultas, pen.id_jalur, p.id_jenjang
						) dit on pem.tahun = dit.tahun and pem.id_fakultas = dit.id_fakultas and pem.id_jalur = dit.id_jalur and pem.id_jenjang = dit.id_jenjang
						left join (
							select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun , p.id_fakultas, p.id_jenjang, pen.id_jalur, avg(mahasiswa.nilai_skhun_mhs) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
							from calon_mahasiswa_baru cmhs
							join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
							join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
							join semester on semester.id_semester = pen.id_semester		
							join program_studi p on p.id_program_studi = cmhs.id_program_studi
							left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
							where p.id_jenjang in (1, 5) and mahasiswa.status IN ('R', 'I') and cmhs.nim_mhs is not null
							group by thn_akademik_semester, p.id_fakultas, pen.id_jalur, p.id_jenjang

							union 

							select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun , p.id_fakultas, p.id_jenjang, pen.id_jalur, avg(cms.ip_s1) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
							from calon_mahasiswa_baru cmhs
							join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
							join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
							join semester on semester.id_semester = pen.id_semester		
							join program_studi p on p.id_program_studi = cmhs.id_program_studi
							left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
							where ((p.id_jenjang in (2, 9) and mahasiswa.status = 'R') or (p.id_jenjang in (1) and mahasiswa.status = 'AJ'))  and cmhs.nim_mhs is not null
							group by thn_akademik_semester, p.id_fakultas, pen.id_jalur, p.id_jenjang

							union
							
							select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun , p.id_fakultas, p.id_jenjang, pen.id_jalur, avg(cms.ip_s2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
							from calon_mahasiswa_baru cmhs
							join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
							join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
							join semester on semester.id_semester = pen.id_semester		
							join program_studi p on p.id_program_studi = cmhs.id_program_studi
							left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
							where (p.id_jenjang in (3) and mahasiswa.status = 'R') and cmhs.nim_mhs is not null
							group by thn_akademik_semester, p.id_fakultas, pen.id_jalur, p.id_jenjang
						) daf on pem.tahun = daf.tahun  and pem.id_fakultas = daf.id_fakultas  and pem.id_jalur = daf.id_jalur and pem.id_jenjang = daf.id_jenjang

"); 

$db_wh->Query("DELETE WH_SE_MHS_THN_MASUK_FAK");

foreach($mhs_51 as $data){

$db_wh->Query("INSERT INTO WH_SE_MHS_THN_MASUK_FAK (THN_AKADEMIK_SMT, ID_FAKULTAS, PEMINAT, DITERIMA, KEKETATAN, MENDAFTAR_KEMBALI, ID_JALUR, ID_JENJANG, TGL_UPDATE, RATA_UN, RATA)
VALUES
('$data[THN_AKADEMIK_SMT]', '$data[ID_FAKULTAS]','$data[PEMINAT]', '$data[DITERIMA]', '$data[KEKETATAN]', '$data[MENDAFTAR_KEMBALI]', '$data[ID_JALUR]', '$data[ID_JENJANG]', '$data[TGL_UPDATE]', '$data[NILAI_RATA]', '$data[NILAI_PMDK]')");

}
*/
$jalur_wh = $db_wh->QueryToArray("SELECT ID_JALUR FROM WH_SE_MHS_THN_MASUK_FAK WHERE ID_JALUR IS NOT NULL $where GROUP BY ID_JALUR");
$i=0;
foreach($jalur_wh as $data){
	$i++;
	$id_jalur .= $data['ID_JALUR'];
	if (count($jalur_wh) != $i)
		$id_jalur .= ', ';
		
}


$jalur = $db->QueryToArray("SELECT * FROM JALUR WHERE ID_JALUR IN  (5, 1, 3, 23, 4, 27) ORDER BY NM_JALUR");

foreach($jalur as $data){
$mhs_r = $db_wh->QueryToArray("SELECT *
									FROM WH_SE_MHS_THN_MASUK_FAK
									WHERE ID_JALUR = '".$data['ID_JALUR']."' and THN_AKADEMIK_SMT <= '".$_SESSION['TAHUN']."' $where
									ORDER BY THN_AKADEMIK_SMT ASC");
									
if(count($mhs_r) > 0){
$tampil .= '

	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="7"><div class="center_title_bar">Mahasiswa Baru (Jalur '.$data['NM_JALUR'].') Berdasarkan Tahun Masuk</div></td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td><center>Tahun Angkatan</center></td>
		<td><center>Peminat</center></td>
		<td><center>Diterima</center></td>
		<td><center>% Keketatan persaingan</center></td>
		<td><center>Mendaftar Kembali</center></td>';
		if($data['ID_JALUR'] == 1){
		$tampil .= '<td><center>Rerata Nilai SNMPTN</center></td>';
		}else{
		$tampil .= '<td><center>Rerata Nilai PMDK</center></td>';
		}
		if($mhs_r[0]['ID_JENJANG'] == 2 or $mhs_r[0]['ID_JENJANG'] == 3 or $mhs_r[0]['ID_JENJANG'] == 9){
		$tampil .= '<td><center>Rerata IPK pada jenjang sebelumnya</center></td>';
		}else{
		$tampil .= '<td><center>Rerata Nilai UN</center></td>';
		}
	$tampil .='</tr>
	<tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
	</tr>';
	
	$jumlah_peminat = 0;
	$jumlah_diterima = 0;
	$jumlah_daftar_ulang = 0;
	$rerata_persaingan = 0;
	$rerata_uan = 0;
	$rerata_pmdk = 0;
	$x = 0;
	
	foreach($mhs_r as $value){
	$tampil .= '<tr class="ui-widget-content">
		<td><center>'.$value['THN_AKADEMIK_SMT'].'/'.($value['THN_AKADEMIK_SMT']+1).'</center></td>
		<td><center>'.$value['PEMINAT'].'</center></td>
		<td><center>'.$value['DITERIMA'].'</center></td>
		<td><center>'.$value['KEKETATAN'].'</center></td>
		<td><center>'.$value['MENDAFTAR_KEMBALI'].'</center></td>';
		if($data['ID_JALUR'] == 1){	
		$tampil .= '<td><center>-</center></td>';
		}else{
		$tampil .= '<td><center>'.$value['RATA'].'</center></td>';
		}
		$tampil .= '<td><center>-</center></td>
	</tr>';
	$jumlah_peminat = $jumlah_peminat + $value['PEMINAT'];
	$jumlah_diterima = $jumlah_diterima + $value['DITERIMA'];
	$jumlah_daftar_ulang = $jumlah_daftar_ulang + $value['MENDAFTAR_KEMBALI'];
	$rerata_persaingan = $rerata_persaingan + $value['KEKETATAN'];
	$rerata_uan = $rerata_uan + $value['RATA_UN'];
	if($data['ID_JALUR'] != 1){	
	$rerata_pmdk = $rerata_pmdk + $value['RATA'];
	}
	if($value['KEKETATAN'] != ''){
		$x++;
	}
	
	}
	$tampil .= '<tr class="ui-widget-content">
		<td><center>Jumlah</center></td>
		<td><center>'.$jumlah_peminat.'</center></td>
		<td><center>'.$jumlah_diterima.'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.$jumlah_daftar_ulang.'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td style="background:#000;">&nbsp;</td>
	</tr>
	<tr class="ui-widget-content">
		<td><center>Rerata</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.round($rerata_persaingan/$x, 2).'</center></td>
		<td style="background:#000;">&nbsp;</td>';
		if($rerata_pmdk > 0){	
			$tampil .= '<td><center>'.round($rerata_pmdk/count($mhs_r), 2).'</center></td>';
		}else{
			$tampil .= '<td><center>-</center></td>';
		}
		$tampil .= '<td><center>-</center></td>
	</tr>
</table><br />
';			
}
}




//PMDK DAN SNMPTN
$mhs_r = $db_wh->QueryToArray("SELECT THN_AKADEMIK_SMT,
	SUM (PEMINAT) AS PEMINAT,
	SUM (DITERIMA) AS DITERIMA,
	round(SUM (DITERIMA)/SUM (PEMINAT)*100, 2) AS KEKETATAN,
	SUM (MENDAFTAR_KEMBALI) AS MENDAFTAR_KEMBALI,
	SUM (RATA_UN) AS RATA_UN
									FROM WH_SE_MHS_THN_MASUK_FAK
									WHERE ID_JALUR IN (1, 3) and THN_AKADEMIK_SMT <= '".$_SESSION['TAHUN']."' $where
									GROUP BY THN_AKADEMIK_SMT
									ORDER BY THN_AKADEMIK_SMT ASC");
									

$tampil .= '

	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="7"><div class="center_title_bar">Mahasiswa Baru (Jalur SNMPTN dan PMDK) Berdasarkan Tahun Masuk</div></td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td><center>Tahun Angkatan</center></td>
		<td><center>Peminat</center></td>
		<td><center>Diterima</center></td>
		<td><center>% Keketatan persaingan</center></td>
		<td><center>Mendaftar Kembali</center></td>
		<td><center>Rerata Nilai UN</center></td>
	</tr>
	<tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
	</tr>';
	
	$jumlah_peminat = 0;
	$jumlah_diterima = 0;
	$jumlah_daftar_ulang = 0;
	$rerata_persaingan = 0;
	$rerata_uan = 0;
	$x = 0;
	
	
	foreach($mhs_r as $value){
	$tampil .= '<tr class="ui-widget-content">
		<td><center>'.$value['THN_AKADEMIK_SMT'].'/'.($value['THN_AKADEMIK_SMT']+1).'</center></td>
		<td><center>'.$value['PEMINAT'].'</center></td>
		<td><center>'.$value['DITERIMA'].'</center></td>
		<td><center>'.$value['KEKETATAN'].'</center></td>
		<td><center>'.$value['MENDAFTAR_KEMBALI'].'</center></td>
		<td><center>-</center></td>
	</tr>';
	$jumlah_peminat = $jumlah_peminat + $value['PEMINAT'];
	$jumlah_diterima = $jumlah_diterima + $value['DITERIMA'];
	$jumlah_daftar_ulang = $jumlah_daftar_ulang + $value['MENDAFTAR_KEMBALI'];
	$rerata_persaingan = $rerata_persaingan + $value['KEKETATAN'];
	$rerata_uan = $rerata_uan + $value['RATA_UN'];
	if($value['KEKETATAN'] != ''){
		$x++;
	}
	
	}
	$tampil .= '<tr class="ui-widget-content">
		<td><center>Jumlah</center></td>
		<td><center>'.$jumlah_peminat.'</center></td>
		<td><center>'.$jumlah_diterima.'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.$jumlah_daftar_ulang.'</center></td>
		<td style="background:#000;">&nbsp;</td>
	</tr>
	<tr class="ui-widget-content">
		<td><center>Rerata</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.round($rerata_persaingan/$x, 2).'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>-</center></td>
	</tr>
</table><br />
';									


//S2
$mhs_r = $db_wh->QueryToArray("
SELECT THN_AKADEMIK_SMT,
	SUM (PEMINAT) AS PEMINAT,
	SUM (DITERIMA) AS DITERIMA,
	round(SUM (DITERIMA)/SUM (PEMINAT)*100, 2) AS KEKETATAN,
	SUM (MENDAFTAR_KEMBALI) AS MENDAFTAR_KEMBALI,
	SUM (RATA_UN) AS RATA_UN
									FROM WH_SE_MHS_THN_MASUK_FAK
									WHERE ID_JENJANG IN (2) and THN_AKADEMIK_SMT <= '".$_SESSION['TAHUN']."' $where
									GROUP BY THN_AKADEMIK_SMT
									ORDER BY THN_AKADEMIK_SMT ASC");
									

$tampil .= '

	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="7"><div class="center_title_bar">Mahasiswa Baru S2 Berdasarkan Tahun Masuk</div></td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td><center>Tahun Angkatan</center></td>
		<td><center>Peminat</center></td>
		<td><center>Diterima</center></td>
		<td><center>% Keketatan persaingan</center></td>
		<td><center>Mendaftar Kembali</center></td>
		<td><center>Rerata IPK pada jenjang sebelumnya</center></td>
	</tr>
	<tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
	</tr>';
	
	$jumlah_peminat = 0;
	$jumlah_diterima = 0;
	$jumlah_daftar_ulang = 0;
	$rerata_persaingan = 0;
	$rerata_uan = 0;
	$x = 0;
	
	
	foreach($mhs_r as $value){
	$tampil .= '<tr class="ui-widget-content">
		<td><center>'.$value['THN_AKADEMIK_SMT'].'/'.($value['THN_AKADEMIK_SMT']+1).'</center></td>
		<td><center>'.$value['PEMINAT'].'</center></td>
		<td><center>'.$value['DITERIMA'].'</center></td>
		<td><center>'.$value['KEKETATAN'].'</center></td>
		<td><center>'.$value['MENDAFTAR_KEMBALI'].'</center></td>
		<td><center>-</center></td>
	</tr>';
	$jumlah_peminat = $jumlah_peminat + $value['PEMINAT'];
	$jumlah_diterima = $jumlah_diterima + $value['DITERIMA'];
	$jumlah_daftar_ulang = $jumlah_daftar_ulang + $value['MENDAFTAR_KEMBALI'];
	$rerata_persaingan = $rerata_persaingan + $value['KEKETATAN'];
	$rerata_uan = $rerata_uan + $value['RATA_UN'];
	if($value['KEKETATAN'] != ''){
		$x++;
	}
	
	}
	$tampil .= '<tr class="ui-widget-content">
		<td><center>Jumlah</center></td>
		<td><center>'.$jumlah_peminat.'</center></td>
		<td><center>'.$jumlah_diterima.'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.$jumlah_daftar_ulang.'</center></td>
		<td style="background:#000;">&nbsp;</td>
	</tr>
	<tr class="ui-widget-content">
		<td><center>Rerata</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.round($rerata_persaingan/$x, 2).'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>-</center></td>
	</tr>
</table><br />
';	



//S3
$mhs_r = $db_wh->QueryToArray("SELECT THN_AKADEMIK_SMT,
	SUM (PEMINAT) AS PEMINAT,
	SUM (DITERIMA) AS DITERIMA,
	round(SUM (DITERIMA)/SUM (PEMINAT)*100, 2) AS KEKETATAN,
	SUM (MENDAFTAR_KEMBALI) AS MENDAFTAR_KEMBALI,
	SUM (RATA_UN) AS RATA_UN
									FROM WH_SE_MHS_THN_MASUK_FAK
									WHERE ID_JENJANG IN (3) and THN_AKADEMIK_SMT <= '".$_SESSION['TAHUN']."' $where
									GROUP BY THN_AKADEMIK_SMT
									ORDER BY THN_AKADEMIK_SMT ASC");
									

$tampil .= '

	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="7"><div class="center_title_bar">Mahasiswa Baru S3 Berdasarkan Tahun Masuk</div></td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td><center>Tahun Angkatan</center></td>
		<td><center>Peminat</center></td>
		<td><center>Diterima</center></td>
		<td><center>% Keketatan persaingan</center></td>
		<td><center>Mendaftar Kembali</center></td>
		<td><center>Rerata IPK pada jenjang sebelumnya</center></td>
	</tr>
	<tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
	</tr>';
	
	$jumlah_peminat = 0;
	$jumlah_diterima = 0;
	$jumlah_daftar_ulang = 0;
	$rerata_persaingan = 0;
	$rerata_uan = 0;
	$x = 0;
	
	
	foreach($mhs_r as $value){
	$tampil .= '<tr class="ui-widget-content">
		<td><center>'.$value['THN_AKADEMIK_SMT'].'/'.($value['THN_AKADEMIK_SMT']+1).'</center></td>
		<td><center>'.$value['PEMINAT'].'</center></td>
		<td><center>'.$value['DITERIMA'].'</center></td>
		<td><center>'.$value['KEKETATAN'].'</center></td>
		<td><center>'.$value['MENDAFTAR_KEMBALI'].'</center></td>
		<td><center>-</center></td>
	</tr>';
	$jumlah_peminat = $jumlah_peminat + $value['PEMINAT'];
	$jumlah_diterima = $jumlah_diterima + $value['DITERIMA'];
	$jumlah_daftar_ulang = $jumlah_daftar_ulang + $value['MENDAFTAR_KEMBALI'];
	$rerata_persaingan = $rerata_persaingan + $value['KEKETATAN'];
	$rerata_uan = $rerata_uan + $value['RATA_UN'];
	if($value['KEKETATAN'] != ''){
		$x++;
	}

	
	}
	$tampil .= '<tr class="ui-widget-content">
		<td><center>Jumlah</center></td>
		<td><center>'.$jumlah_peminat.'</center></td>
		<td><center>'.$jumlah_diterima.'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.$jumlah_daftar_ulang.'</center></td>
		<td style="background:#000;">&nbsp;</td>
	</tr>
	<tr class="ui-widget-content">
		<td><center>Rerata</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.round($rerata_persaingan/$x, 2).'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>-</center></td>
	</tr>
</table><br />
';	

}else{


/*
$mhs_51 = $db->QueryToArray("select pem.tahun as THN_AKADEMIK_SMT, pem.id_jalur ,pem.peminat, dit.diterima, round(dit.diterima/pem.peminat*100, 2) as keketatan, 
daftar_ulang as MENDAFTAR_KEMBALI, SYSDATE AS TGL_UPDATE, pem.id_jenjang, rata_nilai, nilai_pmdk
from (
								select count(*) as peminat, tahun, id_jalur , id_jenjang
								from (
								select thn_akademik_semester as tahun, cmhs.id_jalur, 
								cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, p.id_jenjang
																	from calon_mahasiswa_baru cmhs
																	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
																	join semester on semester.id_semester = pen.id_semester										
																	join program_studi p on p.id_program_studi = cmhs.id_pilihan_1								
								)
								group by tahun, id_jalur, id_jenjang
						) pem 
						left join (
								select count(cmhs.id_program_studi) as diterima, semester.thn_akademik_semester as tahun,pen.id_jalur, p.id_jenjang
								from calon_mahasiswa_baru cmhs
								join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
								join semester on semester.id_semester = pen.id_semester
								join program_studi p on p.id_program_studi = cmhs.id_program_studi								
								group by thn_akademik_semester, pen.id_jalur, p.id_jenjang
						) dit on pem.tahun = dit.tahun and pem.id_jalur = dit.id_jalur and pem.id_jenjang = dit.id_jenjang
						left join (
							
select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun, pen.id_jalur, p.id_jenjang, round(avg(mahasiswa.nilai_skhun_mhs), 2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
							from calon_mahasiswa_baru cmhs
							join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
							join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
							join semester on semester.id_semester = pen.id_semester		
							join program_studi p on p.id_program_studi = cmhs.id_program_studi
							left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
							where p.id_jenjang in (1, 5) and mahasiswa.status IN ('R', 'I') and cmhs.nim_mhs is not null
							group by thn_akademik_semester, pen.id_jalur, p.id_jenjang

union 

							select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun, pen.id_jalur, p.id_jenjang, round(avg(cms.ip_s1), 2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
							from calon_mahasiswa_baru cmhs
							join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
							join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
							join semester on semester.id_semester = pen.id_semester		
							join program_studi p on p.id_program_studi = cmhs.id_program_studi
							left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
							where ((p.id_jenjang in (2, 9) and mahasiswa.status = 'R') or (p.id_jenjang in (1) and mahasiswa.status = 'AJ')) and cmhs.nim_mhs is not null
							group by thn_akademik_semester, pen.id_jalur, p.id_jenjang
							
							union 

							select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun, pen.id_jalur, p.id_jenjang, round(avg(cms.ip_s2), 2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
							from calon_mahasiswa_baru cmhs
							join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
							join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
							join semester on semester.id_semester = pen.id_semester		
							join program_studi p on p.id_program_studi = cmhs.id_program_studi
							left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
							where (p.id_jenjang in (3) and mahasiswa.status = 'R') and cmhs.nim_mhs is not null
							group by thn_akademik_semester, pen.id_jalur, p.id_jenjang
						) daf on pem.tahun = daf.tahun and pem.id_jalur = daf.id_jalur and pem.id_jenjang = daf.id_jenjang
");

$db_wh->Query("DELETE WH_SE_MHS_THN_MASUK_UNIV");

foreach($mhs_51 as $data){

$db_wh->Query("INSERT INTO WH_SE_MHS_THN_MASUK_UNIV (THN_AKADEMIK_SMT, PEMINAT, DITERIMA, KEKETATAN, MENDAFTAR_KEMBALI, ID_JALUR, ID_JENJANG, TGL_UPDATE, RATA_UN, RATA)
VALUES
('$data[THN_AKADEMIK_SMT]', '$data[PEMINAT]', '$data[DITERIMA]', '$data[KEKETATAN]', '$data[MENDAFTAR_KEMBALI]', '$data[ID_JALUR]', '$data[ID_JENJANG]', '$data[TGL_UPDATE]', '$data[RATA_NILAI]', '$data[NILAI_PMDK]')");

}*/

$jalur_wh = $db_wh->QueryToArray("SELECT ID_JALUR FROM WH_SE_MHS_THN_MASUK_UNIV WHERE ID_JALUR IS NOT NULL $where GROUP BY ID_JALUR");
$i=0;
foreach($jalur_wh as $data){
	$i++;
	$id_jalur .= $data['ID_JALUR'];
	if (count($jalur_wh) != $i)
		$id_jalur .= ', ';
		
}


$jalur = $db->QueryToArray("SELECT * FROM JALUR WHERE ID_JALUR IN (5, 1, 3, 23, 4, 27) ORDER BY NM_JALUR");

foreach($jalur as $data){
$mhs_r = $db_wh->QueryToArray("SELECT *
									FROM WH_SE_MHS_THN_MASUK_UNIV
									WHERE ID_JALUR = '".$data['ID_JALUR']."' and THN_AKADEMIK_SMT <= '".$_SESSION['TAHUN']."' $where
									ORDER BY THN_AKADEMIK_SMT ASC");
									

$tampil .= '

	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="7"><div class="center_title_bar">Mahasiswa Baru (Jalur '.$data['NM_JALUR'].') Berdasarkan Tahun Masuk</div></td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td><center>Tahun Angkatan</center></td>
		<td><center>Peminat</center></td>
		<td><center>Diterima</center></td>
		<td><center>% Keketatan persaingan</center></td>
		<td><center>Mendaftar Kembali</center></td>
		';
		if($data['ID_JALUR'] == 1){
		$tampil .= '<td><center>Rerata Nilai SNMPTN</center></td>';
		}else{
		$tampil .= '<td><center>Rerata Nilai PMDK</center></td>';
		}
		if($data['ID_JALUR'] == 23 or $data['ID_JALUR'] == 4){
		$tampil .= '<td><center>Rerata IPK pada jenjang sebelumnya</center></td>';
		}else{
		$tampil .= '<td><center>Rerata Nilai UN</center></td>';
		}
		
$tampil .= '
	</tr>
	<tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
		<td><center>7</center></td>
	</tr>';
	
	$jumlah_peminat = 0;
	$jumlah_diterima = 0;
	$jumlah_daftar_ulang = 0;
	$rerata_persaingan = 0;
	$rerata_uan = 0;
	$rerata_pmdk = 0;
	$x = 0;
	
	
	foreach($mhs_r as $value){
		
	$tampil .= '<tr class="ui-widget-content">
		<td><center>'.$value['THN_AKADEMIK_SMT'].'/'.($value['THN_AKADEMIK_SMT']+1).'</center></td>
		<td><center>'.$value['PEMINAT'].'</center></td>
		<td><center>'.$value['DITERIMA'].'</center></td>
		<td><center>'.$value['KEKETATAN'].'</center></td>
		<td><center>'.$value['MENDAFTAR_KEMBALI'].'</center></td>';
		if($data['ID_JALUR'] == 1){	
		$tampil .= '<td><center>-</center></td>';
		}else{
		$tampil .= '<td><center>'.$value['RATA'].'</center></td>';
		}
		$tampil .= '<td><center>-</center></td>
	</tr>';
	$jumlah_peminat = $jumlah_peminat + $value['PEMINAT'];
	$jumlah_diterima = $jumlah_diterima + $value['DITERIMA'];
	$jumlah_daftar_ulang = $jumlah_daftar_ulang + $value['MENDAFTAR_KEMBALI'];
	$rerata_persaingan = $rerata_persaingan + $value['KEKETATAN'];
	$rerata_uan = $rerata_uan + $value['RATA_UN'];
	if($data['ID_JALUR'] != 1){	
	$rerata_pmdk = $rerata_pmdk + $value['RATA'];
	}
	if($value['KEKETATAN'] != ''){
		$x++;
	}
	
	}
	$tampil .= '<tr class="ui-widget-content">
		<td><center>Jumlah</center></td>
		<td><center>'.$jumlah_peminat.'</center></td>
		<td><center>'.$jumlah_diterima.'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.$jumlah_daftar_ulang.'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td style="background:#000;">&nbsp;</td>
	</tr>
	<tr class="ui-widget-content">
		<td><center>Rerata</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.round($rerata_persaingan/$x, 2).'</center></td>
		<td style="background:#000;">&nbsp;</td>';
		if($rerata_pmdk > 0){	
			$tampil .= '<td><center>'.round($rerata_pmdk/count($mhs_r), 2).'</center></td>';
		}else{
			$tampil .= '<td><center>-</center></td>';
		}
		
		$tampil .= '<td><center>-</center></td>
	</tr>
</table><br />
';									

}


//PMDK DAN SNMPTN
$mhs_r = $db_wh->QueryToArray("SELECT THN_AKADEMIK_SMT, SUM(PEMINAT) AS PEMINAT, SUM(DITERIMA) AS DITERIMA, 
									round(SUM (DITERIMA)/SUM (PEMINAT)*100, 2)  AS KEKETATAN, SUM(MENDAFTAR_KEMBALI) AS MENDAFTAR_KEMBALI, SUM(RATA_UN) AS RATA_UN
									FROM WH_SE_MHS_THN_MASUK_UNIV
									WHERE ID_JALUR IN (1, 3) and THN_AKADEMIK_SMT <= '".$_SESSION['TAHUN']."' $where
									GROUP BY THN_AKADEMIK_SMT
									ORDER BY THN_AKADEMIK_SMT ASC");
									

$tampil .= '

	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="7"><div class="center_title_bar">Mahasiswa Baru (Jalur SNMPTN dan PMDK) Berdasarkan Tahun Masuk</div></td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td><center>Tahun Angkatan</center></td>
		<td><center>Peminat</center></td>
		<td><center>Diterima</center></td>
		<td><center>% Keketatan persaingan</center></td>
		<td><center>Mendaftar Kembali</center></td>
		<td><center>Rerata Nilai UN</center></td>
	</tr>
	<tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
	</tr>';
	
	$jumlah_peminat = 0;
	$jumlah_diterima = 0;
	$jumlah_daftar_ulang = 0;
	$rerata_persaingan = 0;
	$rerata_uan = 0;
	$x = 0;
	
	
	foreach($mhs_r as $value){
	$tampil .= '<tr class="ui-widget-content">
		<td><center>'.$value['THN_AKADEMIK_SMT'].'/'.($value['THN_AKADEMIK_SMT']+1).'</center></td>
		<td><center>'.$value['PEMINAT'].'</center></td>
		<td><center>'.$value['DITERIMA'].'</center></td>
		<td><center>'.$value['KEKETATAN'].'</center></td>
		<td><center>'.$value['MENDAFTAR_KEMBALI'].'</center></td>
		<td><center>-</center></td>
	</tr>';
	$jumlah_peminat = $jumlah_peminat + $value['PEMINAT'];
	$jumlah_diterima = $jumlah_diterima + $value['DITERIMA'];
	$jumlah_daftar_ulang = $jumlah_daftar_ulang + $value['MENDAFTAR_KEMBALI'];
	$rerata_persaingan = $rerata_persaingan + $value['KEKETATAN'];
	$rerata_uan = $rerata_uan + $value['RATA_UN'];
	if($value['KEKETATAN'] != ''){
		$x++;
	}
	
	}
	$tampil .= '<tr class="ui-widget-content">
		<td><center>Jumlah</center></td>
		<td><center>'.$jumlah_peminat.'</center></td>
		<td><center>'.$jumlah_diterima.'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.$jumlah_daftar_ulang.'</center></td>
		<td style="background:#000;">&nbsp;</td>
	</tr>
	<tr class="ui-widget-content">
		<td><center>Rerata</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.round($rerata_persaingan/$x, 2).'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>-</center></td>
	</tr>
</table><br />
';									



//S2
$mhs_r = $db_wh->QueryToArray("SELECT THN_AKADEMIK_SMT, SUM(PEMINAT) AS PEMINAT, SUM(DITERIMA) AS DITERIMA, 
									round(SUM (DITERIMA)/SUM (PEMINAT)*100, 2)  AS KEKETATAN, SUM(MENDAFTAR_KEMBALI) AS MENDAFTAR_KEMBALI, SUM(RATA_UN) AS RATA_UN
									FROM WH_SE_MHS_THN_MASUK_UNIV
									WHERE ID_JENJANG IN (2) and THN_AKADEMIK_SMT <= '".$_SESSION['TAHUN']."' $where
									GROUP BY THN_AKADEMIK_SMT
									ORDER BY THN_AKADEMIK_SMT ASC");
									

$tampil .= '

	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="7"><div class="center_title_bar">Mahasiswa Baru S2 Berdasarkan Tahun Masuk</div></td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td><center>Tahun Angkatan</center></td>
		<td><center>Peminat</center></td>
		<td><center>Diterima</center></td>
		<td><center>% Keketatan persaingan</center></td>
		<td><center>Mendaftar Kembali</center></td>
		<td><center>Rerata IPK pada jenjang sebelumnya</center></td>
	</tr>
	<tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
	</tr>';
	
	$jumlah_peminat = 0;
	$jumlah_diterima = 0;
	$jumlah_daftar_ulang = 0;
	$rerata_persaingan = 0;
	$rerata_uan = 0;
	$x = 0;
	
	
	foreach($mhs_r as $value){
	$tampil .= '<tr class="ui-widget-content">
		<td><center>'.$value['THN_AKADEMIK_SMT'].'/'.($value['THN_AKADEMIK_SMT']+1).'</center></td>
		<td><center>'.$value['PEMINAT'].'</center></td>
		<td><center>'.$value['DITERIMA'].'</center></td>
		<td><center>'.$value['KEKETATAN'].'</center></td>
		<td><center>'.$value['MENDAFTAR_KEMBALI'].'</center></td>
		<td><center>-</center></td>
	</tr>';
	$jumlah_peminat = $jumlah_peminat + $value['PEMINAT'];
	$jumlah_diterima = $jumlah_diterima + $value['DITERIMA'];
	$jumlah_daftar_ulang = $jumlah_daftar_ulang + $value['MENDAFTAR_KEMBALI'];
	$rerata_persaingan = $rerata_persaingan + $value['KEKETATAN'];
	$rerata_uan = $rerata_uan + $value['RATA_UN'];
	if($value['KEKETATAN'] != ''){
		$x++;
	}
	
	}
	$tampil .= '<tr class="ui-widget-content">
		<td><center>Jumlah</center></td>
		<td><center>'.$jumlah_peminat.'</center></td>
		<td><center>'.$jumlah_diterima.'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.$jumlah_daftar_ulang.'</center></td>
		<td style="background:#000;">&nbsp;</td>
	</tr>
	<tr class="ui-widget-content">
		<td><center>Rerata</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.round($rerata_persaingan/$x, 2).'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>-</center></td>
	</tr>
</table><br />
';	



//S3
$mhs_r = $db_wh->QueryToArray("SELECT THN_AKADEMIK_SMT, SUM(PEMINAT) AS PEMINAT, SUM(DITERIMA) AS DITERIMA, 
									round(SUM (DITERIMA)/SUM (PEMINAT)*100, 2)  AS KEKETATAN, SUM(MENDAFTAR_KEMBALI) AS MENDAFTAR_KEMBALI, SUM(RATA_UN) AS RATA_UN
									FROM WH_SE_MHS_THN_MASUK_UNIV
									WHERE ID_JENJANG IN (3) and THN_AKADEMIK_SMT <= '".$_SESSION['TAHUN']."' $where
									GROUP BY THN_AKADEMIK_SMT
									ORDER BY THN_AKADEMIK_SMT ASC");
									

$tampil .= '

	<table class="ui-widget">
		<tr style="background:navy;color:#fff;padding:5px;">
			<td colspan="7"><div class="center_title_bar">Mahasiswa Baru S3 Berdasarkan Tahun Masuk</div></td>
		</tr>
		<tr class="ui-widget-header" style="padding:5px;">
			<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
		</tr>
	</table>


<table class="ui-widget">
	<tr class="ui-widget-header">
		<td><center>Tahun Angkatan</center></td>
		<td><center>Peminat</center></td>
		<td><center>Diterima</center></td>
		<td><center>% Keketatan persaingan</center></td>
		<td><center>Mendaftar Kembali</center></td>
		<td><center>Rerata IPK pada jenjang sebelumnya</center></td>
	</tr>
	<tr class="ui-widget-header">
		<td><center>1</center></td>
		<td><center>2</center></td>
		<td><center>3</center></td>
		<td><center>4</center></td>
		<td><center>5</center></td>
		<td><center>6</center></td>
	</tr>';
	
	$jumlah_peminat = 0;
	$jumlah_diterima = 0;
	$jumlah_daftar_ulang = 0;
	$rerata_persaingan = 0;
	$rerata_uan = 0;
	$x = 0;
	
	
	foreach($mhs_r as $value){
	$tampil .= '<tr class="ui-widget-content">
		<td><center>'.$value['THN_AKADEMIK_SMT'].'/'.($value['THN_AKADEMIK_SMT']+1).'</center></td>
		<td><center>'.$value['PEMINAT'].'</center></td>
		<td><center>'.$value['DITERIMA'].'</center></td>
		<td><center>'.$value['KEKETATAN'].'</center></td>
		<td><center>'.$value['MENDAFTAR_KEMBALI'].'</center></td>
		<td><center>-</center></td>
	</tr>';
	$jumlah_peminat = $jumlah_peminat + $value['PEMINAT'];
	$jumlah_diterima = $jumlah_diterima + $value['DITERIMA'];
	$jumlah_daftar_ulang = $jumlah_daftar_ulang + $value['MENDAFTAR_KEMBALI'];
	$rerata_persaingan = $rerata_persaingan + $value['KEKETATAN'];
	$rerata_uan = $rerata_uan + $value['RATA_UN'];
	if($value['KEKETATAN'] != ''){
		$x++;
	}
	
	}
	$tampil .= '<tr class="ui-widget-content">
		<td><center>Jumlah</center></td>
		<td><center>'.$jumlah_peminat.'</center></td>
		<td><center>'.$jumlah_diterima.'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.$jumlah_daftar_ulang.'</center></td>
		<td style="background:#000;">&nbsp;</td>
	</tr>
	<tr class="ui-widget-content">
		<td><center>Rerata</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>'.round($rerata_persaingan/$x, 2).'</center></td>
		<td style="background:#000;">&nbsp;</td>
		<td><center>-</center></td>
	</tr>
</table><br />
';	

}


$smarty->assign('tampil', $tampil);	
	$smarty->display('se-kemahasiswaan-51.tpl');