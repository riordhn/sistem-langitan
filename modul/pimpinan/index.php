<?PHP
include('config.php');

$db->Query("SELECT USERNAME, ID_KAPRODI, A.ID_DEKAN, B.ID_WADEK1, C.ID_WADEK2, D.ID_WADEK3, A.ID_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_JENJANG
FROM PENGGUNA 
LEFT JOIN DOSEN ON DOSEN.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA
LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_KAPRODI = DOSEN.ID_DOSEN
LEFT JOIN FAKULTAS A ON A.ID_DEKAN = PENGGUNA.ID_PENGGUNA
LEFT JOIN FAKULTAS B ON B.ID_DEKAN = PENGGUNA.ID_PENGGUNA
LEFT JOIN FAKULTAS C ON C.ID_DEKAN = PENGGUNA.ID_PENGGUNA
LEFT JOIN FAKULTAS D ON D.ID_DEKAN = PENGGUNA.ID_PENGGUNA
WHERE DOSEN.ID_PENGGUNA = '{$user->ID_PENGGUNA}'");
$akses=$db->FetchAssoc();

$where2="";
if($akses['ID_DEKAN'] != '' or $akses['ID_WADEK1'] != '' or $akses['ID_WADEK2'] != '' or $akses['ID_WADEK3'] != ''){
	$where = "WHERE ID_FAKULTAS = '".$_SESSION['ID_FAK']."' FAKULTAS.ID_PERGURUAN_TINGGI = 1";
	
}elseif($akses['ID_KAPRODI'] != '' ){
	$where = "WHERE ID_FAKULTAS IN (SELECT ID_FAKULTAS FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI = '".$akses['ID_PROGRAM_STUDI']."')";
	$where2 = "AND ID_PROGRAM_STUDI = '".$akses['ID_PROGRAM_STUDI']."'";
	
}else{
	$where = "WHERE FAKULTAS.ID_PERGURUAN_TINGGI = 1";
}


$menu = '<nav>
	<ul>		
		<li><a href="se.php" class="disable-ajax">Universitas</a></li>
		<li><a href="#">Fakultas</a>
			<ul>';
				$fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS $where ORDER BY ID_FAKULTAS ASC");
				foreach($fakultas as $fak){
				
				$menu .= '<li><a href="se.php?id_fakultas='.$fak['ID_FAKULTAS'].'" class="disable-ajax">'.$fak['NM_FAKULTAS'].'</a></li>';
				}
			$menu .= '</ul>
		</li>
		<li><a href="#">Prodi</a>
			<ul>';
				$fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS $where ORDER BY ID_FAKULTAS ASC");
				foreach($fakultas as $fak){
				
				$menu .= '<li><a href="se.php?id_fakultas='.$fak['ID_FAKULTAS'].'" class="disable-ajax">'.$fak['NM_FAKULTAS'].'</a>
					<ul>';
					
				$prodi = $db->QueryToArray("SELECT * FROM PROGRAM_STUDI 
											JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
											WHERE STATUS_AKTIF_PRODI = 1 AND ID_FAKULTAS = '$fak[ID_FAKULTAS]' $where2
											ORDER BY ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI ASC");
				foreach($prodi as $prod){
				$menu .= '<li><a href="se.php?id_prodi='.$prod['ID_PROGRAM_STUDI'].'" class="disable-ajax">'.$prod['NM_JENJANG']. ' - '.$prod['NM_PROGRAM_STUDI'].'</a></li>';
				}
				
				$menu .= '</ul>
				</li>';
				}
			$menu .= '</ul>
		</li>
		<li><a href="#">Lulusan</a>
			<ul>';
				if($_SESSION['JENJANG'] == 'S2' or $_SESSION['JENJANG'] == 'S3'){
					$menu .= '<li><a href="#se!se-lulusan-31.php">31-Tahun Lulusan dan IPK</a></li>
								<li><a href="#se!se-lulusan-34-s2.php">34-Tahun Lulusan dan Lama Penyelesaian Tesis/Desertasi</a></li>
								<li><a href="#se!se-lulusan-35-s2.php">35-Nilai English Language Proficiency Test (ELPT) Lulusan</a></li>';
				}else{
					$menu .= '<li><a href="#se!se-lulusan-31.php">31-Tahun Lulusan dan IPK</a></li>
								<li><a href="#se!se-lulusan-35.php">35-Tahun Lulusan dan Masa Studi</a></li>
								<li><a href="#se!se-lulusan-36.php">36-Tahun Lulusan dan Lama Penyelesaian Skripsi/Tugas Akhir</a></li>
								<li><a href="#se!se-lulusan-37.php">37-Nilai English Language Proficiency Test (ELPT) Lulusan</a></li>';
				}
				
			$menu .= '</ul>
		</li>
		<li><a href="#">Akademik</a>
			<ul>';
				if($_SESSION['JENJANG'] == 'D3'){
					$menu .= '<li><a href="#se!se-akademik-41.php">41-Status Akademik</a></li>
								<li><a href="#se!se-akademik-42.php">42-IPK Mhs Aktif</a></li>
								<li><a href="#se!se-akademik-43.php">43-Nilai ELPT Maba</a></li>
								<li><a href="#se!se-akademik-45.php">45-Proses Pendidikan</a></li>
								<li><a href="#se!se-akademik-46.php">46-Dist Nilai Kuliah/Prakt</a></li>
								<li><a href="#se!se-akademik-49-d3.php">49-Indeks Kepuasan</a></li>
								<li><a href="#se!se-akademik-410.php">410-Eval Kinerja Staf</a></li>
								<li><a href="#se!se-akademik-411.php">411-Eval Layanan - Thn(satu)</a></li>
								<li><a href="#se!se-akademik-412.php">412-AEE</a></li>';
				}elseif($_SESSION['JENJANG'] == 'S2' or $_SESSION['JENJANG'] == 'S3'){
					$menu .= '<li><a href="#se!se-akademik-41.php">41-Status Akademik</a></li>
								<li><a href="#se!se-akademik-42.php">42-IPK Mhs Aktif</a></li>
								<li><a href="#se!se-akademik-43.php">43-Nilai ELPT Maba</a></li>
								<li><a href="#se!se-akademik-45.php">45-Proses Pendidikan</a></li>
								<li><a href="#se!se-akademik-46.php">46-Dist Nilai Kuliah/Prakt</a></li>
								<li><a href="#se!se-akademik-49-s2.php">49-AEE</a></li>
								<li><a href="#se!se-akademik-410.php">410-Eval Kinerja Staf</a></li>
								<li><a href="#se!se-akademik-411.php">411-Eval Layanan - Thn(satu)</a></li>';
				}else{
					$menu .= '<li><a href="#se!se-akademik-41.php">41-Status Akademik</a></li>
								<li><a href="#se!se-akademik-42.php">42-IPK Mhs Aktif</a></li>
								<li><a href="#se!se-akademik-43.php">43-Nilai ELPT Maba</a></li>
								<li><a href="#se!se-akademik-45.php">45-Proses Pendidikan</a></li>
								<li><a href="#se!se-akademik-46.php">46-Dist Nilai Kuliah/Prakt</a></li>
								<li><a href="#se!se-akademik-48.php">48-Indeks Kepuasan</a></li>
								<li><a href="#se!se-akademik-410.php">410-Eval Kinerja Staf</a></li>
								<li><a href="#se!se-akademik-411.php">411-Eval Layanan - Thn(satu)</a></li>
								<li><a href="#se!se-akademik-412.php">412-AEE</a></li>';
				}
			$menu .= '</ul>
		</li>
		
		<li><a href="#">Kemahasiswaan</a>
			<ul>';
				if($_SESSION['JENJANG'] == 'S3' or $_SESSION['JENJANG'] == 'S2'){
					$menu .= '<li><a href="#se!se-kemahasiswaan-51.php">51-Tahun Masuk</a></li>
								<li><a href="#se!se-kemahasiswaan-52-s2.php">52-S2 dan S3 Tahun Masuk dan Institusi Asal</a></li>
								<li><a href="#se!se-kemahasiswaan-53-s2.php">53-S2 dan S3 Asal Universitas</a></li>			
								<li><a href="#se!se-kemahasiswaan-54-s2.php">54-S2 dan S3 Asal Domisili</a></li>';
				}elseif($_SESSION['JENJANG'] == 'S1' or $_SESSION['JENJANG'] == 'D3'){
					$menu .= '<li><a href="#se!se-kemahasiswaan-51.php">51-Tahun Masuk</a></li>
								<li><a href="#se!se-kemahasiswaan-52.php">52-Tahun Masuk dan Propinsi Asal</a></li>
								<li><a href="#se!se-kemahasiswaan-53.php">53-Kota/Kabupaten dan Penghasilan Orang Tua</a></li>	
								<li><a href="#se!se-kemahasiswaan-54.php">54-Pilihan Mahasiswa Baru Berdasarkan Tahun Masuk</a></li>
								<li><a href="#se!se-kemahasiswaan-55.php">55-Peminat Program Studi Berdasarkan Tahun Masuk dan Provinsi Asal SMTA</a></li>';
				}else{
					$menu .= '<li><a href="#se!se-kemahasiswaan-51.php">51-Tahun Masuk</a></li>
								<li><a href="#se!se-kemahasiswaan-52.php">52-Tahun Masuk dan Propinsi Asal</a></li>
								<li><a href="#se!se-kemahasiswaan-52-s2.php">52-S2 dan S3 Tahun Masuk dan Institusi Asal</a></li>
								<li><a href="#se!se-kemahasiswaan-53.php">53-Kota/Kabupaten dan Penghasilan Orang Tua</a></li>	
								<li><a href="#se!se-kemahasiswaan-53-s2.php">53-S2 dan S3 Asal Universitas</a></li>			
								<li><a href="#se!se-kemahasiswaan-54.php">54-Pilihan Mahasiswa Baru Berdasarkan Tahun Masuk</a></li>
								<li><a href="#se!se-kemahasiswaan-54-s2.php">54-S2 dan S3 Asal Domisili</a></li>
								<li><a href="#se!se-kemahasiswaan-55.php">55-Peminat Program Studi Berdasarkan Tahun Masuk dan Provinsi Asal SMTA</a></li>';
				}
				
				
				$menu .= '				
			</ul>
		</li>		
		<li><a href="#">Sumber Daya</a>
			<ul>
				<li><a href="#se!se-sd-71a.php">Dosen Tetap PNS (umur, tingkat pendidikan,& Jabatan Fungsional)</a></li>
				<li><a href="#se!se-sd-71b.php">Dosen Tidak Tetap Luar Unair (umur, tingkat pendidikan,& Jabatan Fungsional)</a></li>
				<li><a href="#se!se-sd-76.php">Daftar Dosen Tetap yang Mengikuti Pendidikan Tidak Bergelar (Magang, Pelatihan, Pencangkokan, dsb)</a></li>
				<li><a href="#se!se-sd-710.php">Rekapitulasi Beban Dosen</a></li>
				<li><a href="#se!se-sd-711.php">Jumlah Tenaga Kependidikan PNS</a></li>
				<li><a href="#se!se-sd-712.php">Jumlah Tenaga Kependidikan Honorer</a></li>
			</ul>
		</li>


		<li><a href="account.php">Account</a></li>
		<li><a href="#">Proses Data</a>
			<ul>
				<li><a href="#se!se-data_4.php">Akademik (Tabel 4)</a></li>
				<li><a href="#se!se-data_5">Kemahasiswaan</a></li>
				<li><a href="#se!se-data_3.php">Lulusan</a></li>
				<li><a href="#">SDM</a></li>
			</ul>
		</li>

		<li><a class="disable-ajax" href="../../logout.php">Logout</a></li>
	</ul>
</nav>';
$smarty->assign('header', $_SESSION['HEADER']);
$smarty->assign('menu', $menu);
$smarty->display("index.tpl");
?>