<?PHP
include('config.php');

$smarty->assign('tahun',$_SESSION['TAHUN']);

if ($_SESSION['STATUS']==1 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);

$ipk_R = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]')
								and status='R' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0))
								and id_jenjang=1)s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') )s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc");

$smarty->assign('ipk_R', $ipk_R);
									
$ipk_AJ = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='AJ' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0))
								and id_jenjang=1)s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') )s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc");
$smarty->assign('ipk_AJ', $ipk_AJ);

$ipk_S2 = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0))
								and id_jenjang=2)s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') )s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc");
$smarty->assign('ipk_S2', $ipk_S2);

$ipk_S3 = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0))
								and id_jenjang=3)s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') )s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc");
$smarty->assign('ipk_S3', $ipk_S3);

$ipk_D3 = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0))
								and id_jenjang=5)s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') )s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc");
$smarty->assign('ipk_D3', $ipk_D3);


$ipk_PROFESI = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0))
								and id_jenjang=9)s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') )s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc
								");
$smarty->assign('ipk_PROFESI', $ipk_PROFESI);

$smarty->display("se-akademik-42-universitas.tpl");
}


if ($_SESSION['STATUS']==2 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('fakultas', $_SESSION['NM_FAK']);

$ipk_R = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]')
								and status='R' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0))
								and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1)s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]')s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc");

$smarty->assign('ipk_R', $ipk_R);
									
$ipk_AJ = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='AJ' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0))
								and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1)s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]')s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc");
$smarty->assign('ipk_AJ', $ipk_AJ);

$ipk_S2 = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0))
								and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2)s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]')s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc");
$smarty->assign('ipk_S2', $ipk_S2);

$ipk_S3 = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0))
								and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3)s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]')s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc");
$smarty->assign('ipk_S3', $ipk_S3);

$ipk_D3 = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0))
								and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5)s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]')s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc");
$smarty->assign('ipk_D3', $ipk_D3);


$ipk_PROFESI = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0))
								and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9)s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_fakultas='$_SESSION[ID_FAK]')s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc
								");
$smarty->assign('ipk_PROFESI', $ipk_PROFESI);

$smarty->display("se-akademik-42-fakultas.tpl");
}

if ($_SESSION['STATUS']==3 ) {
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('jenjang', $_SESSION['JENJANG']);
$smarty->assign('prodi', $_SESSION['PRODI']);

if ($_SESSION['JENJANG']=='S2' or $_SESSION['JENJANG']=='S3')
{
$ipk_R = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2.75 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3.0 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 3.0 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 3.5 and ipk_mhs < 3.75 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3.75  then 1 else 0 end) as jumlah_10
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and id_program_studi='$_SESSION[ID_PRODI]' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0)))s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_program_studi='$_SESSION[ID_PRODI]')s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc
								");

$smarty->assign('ipk_R', $ipk_R);

} else {
$ipk_R = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='R' and id_program_studi='$_SESSION[ID_PRODI]' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0)))s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_program_studi='$_SESSION[ID_PRODI]')s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc
								");
$smarty->assign('ipk_R', $ipk_R);
									
$ipk_AJ = $db_wh->QueryToArray("select tahun_angkatan,
								sum(case when (ipk_mhs <2 or ipk_mhs is null) then 1 else 0 end) as jumlah_2, 
								sum(case when ipk_mhs >= 2 and ipk_mhs < 2.5 then 1 else 0 end) as jumlah_4,
								sum(case when ipk_mhs >= 2.5 and ipk_mhs < 2.75 then 1 else 0 end) as jumlah_6,
								sum(case when ipk_mhs >= 2.75 and ipk_mhs < 3 then 1 else 0 end) as jumlah_8,
								sum(case when ipk_mhs >= 3 and ipk_mhs < 3.5 then 1 else 0 end) as jumlah_10,
								sum(case when ipk_mhs >= 3.5 then 1 else 0 end) as jumlah_12
								from (
								select thn_angkatan_mhs||'/'||(thn_angkatan_mhs+1) as tahun_angkatan,id_mhs 
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and status='AJ' and id_program_studi='$_SESSION[ID_PRODI]' and (krs=1 or status_mhs_cuti_akd=1 or (status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0)))s1
								left join (
								select id_mhs,ipk_mhs from wh_ipk_ips
								where tahun in (select max(tahun) from wh_ipk_ips 
								where substr(tahun,1,4)='$_SESSION[TAHUN]') and id_program_studi='$_SESSION[ID_PRODI]')s2 on s1.id_mhs=s2.id_mhs
								group by tahun_angkatan       
								order by tahun_angkatan desc
								");
$smarty->assign('ipk_AJ', $ipk_AJ);
}
$smarty->display("se-akademik-42-prodi.tpl");

}

?>