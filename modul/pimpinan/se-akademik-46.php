<?PHP
include('config.php');

$smarty->assign('tahun',$_SESSION['TAHUN']);

if ($_SESSION['STATUS']==1 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);

$disnil_R = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
										sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
										from wh_distribusi_nilai
										where  id_jenjang=1 and status='R'
										group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
										order by tahun asc");
$smarty->assign('disnil_R', $disnil_R);
									
$disnil_AJ = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
										sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
										from wh_distribusi_nilai
										where id_jenjang=1 and status='AJ'
										group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
										order by tahun asc");
$smarty->assign('disnil_AJ', $disnil_AJ);

$disnil_S2 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
										sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
										from wh_distribusi_nilai
										where id_jenjang=2
										group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
										order by tahun asc");
$smarty->assign('disnil_S2', $disnil_S2);

$disnil_S3 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
										sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
										from wh_distribusi_nilai
										where id_jenjang=3
										group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
										order by tahun asc");
$smarty->assign('disnil_S3', $disnil_S3);

$disnil_D3 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
										sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
										from wh_distribusi_nilai
										where id_jenjang=5
										group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
										order by tahun asc");
$smarty->assign('disnil_D3', $disnil_D3);


$disnil_PROFESI = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
										sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
										from wh_distribusi_nilai
										where id_jenjang=9
										group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
										order by tahun asc");
$smarty->assign('disnil_PROFESI', $disnil_PROFESI);

$smarty->display("se-akademik-46-universitas.tpl");
}

if ($_SESSION['STATUS']==2 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('fakultas', $_SESSION['NM_FAK']);

$disnil_R = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
										sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
										from wh_distribusi_nilai
										where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and status='R'
										group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
										order by tahun asc");
$smarty->assign('disnil_R', $disnil_R);
									
$disnil_AJ = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
										sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
										from wh_distribusi_nilai
										where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and status='AJ'
										group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
										order by tahun asc");
$smarty->assign('disnil_AJ', $disnil_AJ);

$disnil_S2 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
										sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
										from wh_distribusi_nilai
										where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2
										group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
										order by tahun asc");
$smarty->assign('disnil_S2', $disnil_S2);

$disnil_S3 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
										sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
										from wh_distribusi_nilai
										where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3
										group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
										order by tahun asc");
$smarty->assign('disnil_S3', $disnil_S3);

$disnil_D3 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
										sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
										from wh_distribusi_nilai
										where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5
										group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
										order by tahun asc");
$smarty->assign('disnil_D3', $disnil_D3);


$disnil_PROFESI = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
										sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
										from wh_distribusi_nilai
										where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9
										group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
										order by tahun asc");
$smarty->assign('disnil_PROFESI', $disnil_PROFESI);

$smarty->display("se-akademik-46-fakultas.tpl");
}

if ($_SESSION['STATUS']==3 ) {
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('jenjang', $_SESSION['JENJANG']);
$smarty->assign('prodi', $_SESSION['PRODI']);

$disnil_R = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
								sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
								from wh_distribusi_nilai
								where status='R' and id_program_studi='$_SESSION[ID_PRODI]'
								group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
								order by tahun asc");
$smarty->assign('disnil_R', $disnil_R);
									
$disnil_AJ = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun,sum(nila)as nila,sum(nilab)as nilab,
								sum(nilb)as nilb,sum(nilbc)as nilbc,sum(nilc) as nilc,sum(nild)as nild,sum(nile)as nile 
								from wh_distribusi_nilai
								where status='AJ' and id_program_studi='$_SESSION[ID_PRODI]'
								group by substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1)
								order by tahun asc");
$smarty->assign('disnil_AJ', $disnil_AJ);
$smarty->display("se-akademik-46-prodi.tpl");
}

?>