<?PHP
include('config.php');

$smarty->assign('tahun',$_SESSION['TAHUN']);

if ($_SESSION['STATUS']==1 )
{
//Baca Tgl rekam Proses

$db_wh->Query("select distinct tahun,to_char(tgl_update,'DD-MM-YYYY') as tgl_update  from WH_STATUS_AKADEMIK 
			where substr(tahun,1,4)='$_SESSION[TAHUN]' and rownum=1
			order by tahun desc");
$wh_41_genap=$db_wh->FetchAssoc();
$smarty->assign('41genap',$wh_41_genap['TGL_UPDATE']);
$smarty->assign('41genapth',$wh_41_genap['TAHUN']);

$db_wh->Query("select distinct tahun,to_char(tgl_update,'DD-MM-YYYY') as tgl_update  from WH_IPK_IPS 
			where tahun='$_SESSION[TAHUN]1'");
$wh_42_ganjil=$db_wh->FetchAssoc();
$smarty->assign('42ganjil',$wh_42_ganjil['TGL_UPDATE']);
$db_wh->Query("select distinct tahun,to_char(tgl_update,'DD-MM-YYYY') as tgl_update  from WH_IPK_IPS
			where tahun='$_SESSION[TAHUN]2'");
$wh_42_genap=$db_wh->FetchAssoc();
$smarty->assign('42genap',$wh_42_genap['TGL_UPDATE']);

$db_wh->Query("select distinct to_char(tgl_update,'DD-MM-YYYY') as tgl_update from WH_ELPT 
				where THN_ANGKATAN_MHS=$_SESSION[TAHUN]");
$wh_43_elptmaba=$db_wh->FetchAssoc();
$smarty->assign('43elpt',$wh_43_elptmaba['TGL_UPDATE']);

$db_wh->Query("select distinct tahun,to_char(tgl_update,'DD-MM-YYYY') as tgl_update  from WH_DISTRIBUSI_NILAI 
			where tahun='$_SESSION[TAHUN]1'");
$wh_45_ganjil=$db_wh->FetchAssoc();
$smarty->assign('45ganjil',$wh_45_ganjil['TGL_UPDATE']);
$db_wh->Query("select distinct tahun,to_char(tgl_update,'DD-MM-YYYY') as tgl_update  from WH_DISTRIBUSI_NILAI
			where tahun='$_SESSION[TAHUN]2'");
$wh_45_genap=$db_wh->FetchAssoc();
$smarty->assign('45genap',$wh_45_genap['TGL_UPDATE']);

$db_wh->Query("select distinct tahun,to_char(tgl_update,'DD-MM-YYYY') as tgl_update  from WH_IK 
			where tahun='$_SESSION[TAHUN]1'");
$wh_48_ganjil=$db_wh->FetchAssoc();
$smarty->assign('48ganjil',$wh_48_ganjil['TGL_UPDATE']);
$db_wh->Query("select distinct tahun,to_char(tgl_update,'DD-MM-YYYY') as tgl_update  from WH_IK
			where tahun='$_SESSION[TAHUN]2'");
$wh_48_genap=$db_wh->FetchAssoc();
$smarty->assign('48genap',$wh_48_genap['TGL_UPDATE']);

$db_wh->Query("select distinct semester,to_char(tgl_update,'DD-MM-YYYY') as tgl_update  from WH_411 
			where semester='$_SESSION[TAHUN]1'");
$wh_410_ganjil=$db_wh->FetchAssoc();
$smarty->assign('410ganjil',$wh_410_ganjil['TGL_UPDATE']);
$db_wh->Query("select distinct semester,to_char(tgl_update,'DD-MM-YYYY') as tgl_update  from WH_411
			where semester='$_SESSION[TAHUN]2'");
$wh_410_genap=$db_wh->FetchAssoc();
$smarty->assign('410genap',$wh_410_genap['TGL_UPDATE']);

$db_wh->Query("select distinct tahun,to_char(tgl_update,'DD-MM-YYYY') as tgl_update  from WH_EVA_MABA 
			where tahun='$_SESSION[TAHUN]1'");
$wh_411_ganjil=$db_wh->FetchAssoc();
$smarty->assign('411ganjil',$wh_411_ganjil['TGL_UPDATE']);
$db_wh->Query("select distinct tahun,to_char(tgl_update,'DD-MM-YYYY') as tgl_update  from WH_EVA_MABA
			where tahun='$_SESSION[TAHUN]2'");
$wh_411_genap=$db_wh->FetchAssoc();
$smarty->assign('411genap',$wh_411_genap['TGL_UPDATE']);

//proses status_akademik
/*

if ($_GET['action']=='status_akademik' )
{
	$tahun=$_GET['th'];
	$st=$_GET['st'];
	
	if ($st=='2')
	{
		//move data
		$db_wh->Query("insert into wh_status_akademik_log
					   (tahun,id_fakultas,id_program_studi,id_jenjang,id_mhs,nim_mhs,thn_angkatan_mhs,status,
						status_mhs_skripsi,status_mhs_aktif,status_mhs_cuti_akd,status_mhs_undur_diri,
						status_mhs_do,status_mhs_lulus,krs,tgl_update)
						select tahun,id_fakultas,id_program_studi,id_jenjang,id_mhs,nim_mhs,thn_angkatan_mhs,status,
						status_mhs_skripsi,status_mhs_aktif,status_mhs_cuti_akd,status_mhs_undur_diri,
						status_mhs_do,status_mhs_lulus,krs,tgl_update from wh_status_akademik
						where tahun=$tahun
						");
		//delete data
		$db_wh->Query("delete from wh_status_akademik where tahun=$tahun
						");
	}
		//proses insert baru
		$db->Query("select id_semester from semester where status_aktif_semester='True''");
		$smt=$db_wh->FetchAssoc();
		
		$wh_status_akademik = $db->QueryToArray("select tahun,id_fakultas,id_program_studi,id_jenjang,s1.id_mhs,nim_mhs,thn_angkatan_mhs,status,status_mhs_skripsi,
					case when (coalesce(status_akd1,status_akd) = 1 or status_krs=1 )then 1 else 0 end  as status_mhs_aktif,
					case when coalesce(status_akd1,status_akd) = 2 then 1 else 0 end  as status_mhs_cuti_akd,
					case when coalesce(status_akd1,status_akd) = 5 then 1 else 0 end  as status_mhs_undur_diri,
					case when coalesce(status_akd1,status_akd) = 6 or coalesce(status_akd1,status_akd) = 20 then 1 else 0 end  as status_mhs_do,
					case when coalesce(status_akd1,status_akd) = 4 then 1 else 0 end  as status_mhs_lulus from 
					(SELECT distinct mahasiswa.id_mhs,mahasiswa.nim_mhs,mahasiswa.id_program_studi,
					program_studi.id_fakultas,program_studi.id_jenjang,mahasiswa.status,
					case when semester.id_semester is not null then 1 else 0 end as status_krs,
					case when nm_semester='Ganjil' then coalesce(thn_akademik_semester,$tahun)||1 else coalesce(thn_akademik_semester,$tahun)||2 end as tahun,
					mahasiswa.thn_angkatan_mhs,status_akademik_mhs as status_akd,
					case when id_kurikulum_mk in 
					(select id_kurikulum_mk from kurikulum_mk where status_mkta in (1)) then 1 else 0 end as status_mhs_skripsi
					FROM MAHASISWA
					LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
					LEFT JOIN JALUR_MAHASISWA ON MAHASISWA.ID_MHS = JALUR_MAHASISWA.ID_MHS
					left join pengambilan_mk on mahasiswa.id_mhs=pengambilan_mk.id_mhs and pengambilan_mk.id_semester='$smt[ID_SEMESTER]'
					left join semester on pengambilan_mk.id_semester=semester.id_semester and semester.id_semester='$smt[ID_SEMESTER]'
					WHERE mahasiswa.thn_angkatan_mhs<=$tahun)s1
					left join
					(select distinct id_mhs,id_semester,status_akd_mhs as status_akd1 from admisi
					where status_akd_mhs in (2,4,5,6,20) and status_apv=1 and id_semester='$smt[ID_SEMESTER]')s2 on s1.id_mhs=s2.id_mhs
					order by tahun,id_fakultas,id_program_studi,id_jenjang,id_mhs");
	


}

*/











$smarty->display("se-data_4.tpl");


} else
{
	?>
	<script>alert("Maaf.. Anda Tidak Berhak Melakukan Proses Data")</script>	
	<?php
}

?> 