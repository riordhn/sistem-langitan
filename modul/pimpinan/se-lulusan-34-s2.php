<?PHP
include('config.php');

if($_SESSION['ID_PRODI'] != ''){
	$where = "AND ID_PROGRAM_STUDI = '".$_SESSION['ID_PRODI']."'";
	$header = $_SESSION['JENJANG'] . ' - '. $_SESSION['PRODI'];
	
}elseif($_SESSION['ID_FAK'] != ''){
	$where = "AND ID_FAKULTAS = '".$_SESSION['ID_FAK']."'";
	$header = 'FAKULTAS '.strtoupper($_SESSION['NM_FAK']);

}else{
	$where = "";
	$header = strtoupper('Universitas Airlangga');
}


$thn = date("Y") - 3;
$thn_bawah = date("Y") -1;
$lulusan_s2 = $db_wh->QueryToArray("SELECT SUBSTR(THN_AKADEMIK_SMT, 0, 4) AS THN_AKADEMIK_LULUS, 
									SUM(LAMA_SKRIPSI1_JML) AS LAMA_SKRIPSI1_JML, SUM(LAMA_SKRIPSI2_JML) AS LAMA_SKRIPSI2_JML, SUM(LAMA_SKRIPSI3_JML) AS LAMA_SKRIPSI3_JML,  
									(SUM(LAMA_SKRIPSI1_JML) + SUM(LAMA_SKRIPSI2_JML) + SUM(LAMA_SKRIPSI3_JML)) AS TOTAL_LULUSAN
									FROM WH_SE_LULUSAN_SKRIPSI
									WHERE SUBSTR(THN_AKADEMIK_SMT, 0, 4)  >= '$thn' AND SUBSTR(THN_AKADEMIK_SMT, 0, 4)  <= '$thn_bawah'
									 AND STATUS_MHS = 'R' AND ID_JENJANG = 2 $where
									GROUP BY SUBSTR(THN_AKADEMIK_SMT, 0, 4)
									ORDER BY SUBSTR(THN_AKADEMIK_SMT, 0, 4) ASC");
$smarty->assign('lulusan_s2', $lulusan_s2);	


$lulusan_s3 = $db_wh->QueryToArray("SELECT SUBSTR(THN_AKADEMIK_SMT, 0, 4) AS THN_AKADEMIK_LULUS, 
									SUM(LAMA_SKRIPSI1_JML) AS LAMA_SKRIPSI1_JML, SUM(LAMA_SKRIPSI2_JML) AS LAMA_SKRIPSI2_JML, SUM(LAMA_SKRIPSI3_JML) AS LAMA_SKRIPSI3_JML,  
									(SUM(LAMA_SKRIPSI1_JML) + SUM(LAMA_SKRIPSI2_JML) + SUM(LAMA_SKRIPSI3_JML)) AS TOTAL_LULUSAN
									FROM WH_SE_LULUSAN_SKRIPSI
									WHERE SUBSTR(THN_AKADEMIK_SMT, 0, 4)  >= '$thn' AND SUBSTR(THN_AKADEMIK_SMT, 0, 4)  <= '$thn_bawah' 
									AND STATUS_MHS = 'R' AND ID_JENJANG = 3 $where
									GROUP BY SUBSTR(THN_AKADEMIK_SMT, 0, 4)
									ORDER BY SUBSTR(THN_AKADEMIK_SMT, 0, 4) ASC");
$smarty->assign('lulusan_s3', $lulusan_s3);	

			
$smarty->assign('header', $header);		
$smarty->assign('jenjang', $_SESSION['JENJANG']);	
$smarty->display("se-lulusan-34-s2.tpl");
?>