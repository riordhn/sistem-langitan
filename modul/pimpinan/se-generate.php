<?PHP
include('config.php');

// Masa Periode : Ganjil 2014
$id_perguruan_tinggi	= 1;
$thn_akademik_semester 	= "2013";
$nm_semester 			= "Genap";
$smt_string 			= "20132";

$thn_akademik_semester_2 = "2013";
$nm_semester_2 			= "Ganjil";
$smt_string_2 			= "20131";

// Clear Data First
$db_wh->Query("DELETE FROM wh_status_akademik");
$db_wh->Query("DELETE FROM wh_ipk_ips");
$db_wh->Query("DELETE FROM wh_elpt");

//wh_status_akademik 4.1
$db_wh->Query(
			"INSERT INTO wh_status_akademik
			(TAHUN,
			ID_FAKULTAS,
			ID_PROGRAM_STUDI,
			ID_JENJANG,
			ID_MHS,
			NIM_MHS,
			THN_ANGKATAN_MHS,
			STATUS,
			STATUS_MHS_SKRIPSI,
			STATUS_MHS_AKTIF,
			STATUS_MHS_CUTI_AKD,
			STATUS_MHS_UNDUR_DIRI,
			STATUS_MHS_DO,
			STATUS_MHS_LULUS,
			TGL_UPDATE,
			KRS)
 SELECT
	TAHUN,
	ID_FAKULTAS,
	ID_PROGRAM_STUDI,
	ID_JENJANG,
	ID_MHS,
	NIM_MHS,
	THN_ANGKATAN_MHS,
	STATUS,
	skripsi AS STATUS_MHS_SKRIPSI,
	STATUS_MHS_AKTIF,
	STATUS_MHS_CUTI_AKD,
	STATUS_MHS_UNDUR_DIRI,
	STATUS_MHS_DO,
	STATUS_MHS_LULUS,
	SYSDATE AS TGL_UPDATE,
	KRS
FROM
	(
		SELECT
			s1.id_mhs,
			nim_mhs,
			thn_angkatan_mhs,
			status,
			status_akd,
			status_akd1,
			tahun,
			id_program_studi,
			id_fakultas,
			id_jenjang,
			krs,
			skripsi,
			CASE
		WHEN (
			COALESCE (status_akd1, status_akd) = 1
			OR krs = 1
		) THEN
			1
		ELSE
			0
		END AS status_mhs_aktif,
		CASE
	WHEN COALESCE (status_akd1, status_akd) = 2 THEN
		1
	ELSE
		0
	END AS status_mhs_cuti_akd,
	CASE
WHEN COALESCE (status_akd1, status_akd) = 5 THEN
	1
ELSE
	0
END AS status_mhs_undur_diri,
 CASE
WHEN COALESCE (status_akd1, status_akd) = 6
OR COALESCE (status_akd1, status_akd) = 20 THEN
	1
ELSE
	0
END AS status_mhs_do,
 CASE
WHEN (
	COALESCE (status_akd1, status_akd) = 4
	AND krs = 0
) THEN
	1
ELSE
	0
END AS status_mhs_lulus
FROM
	(
		SELECT
			mahasiswa.id_mhs,
			mahasiswa.nim_mhs,
			thn_angkatan_mhs,
			status,
			status_akademik_mhs AS status_akd,
			'{$thn_akademik_semester}' AS tahun,
			mahasiswa.id_program_studi,
			program_studi.id_fakultas,
			program_studi.id_jenjang,
			CASE
		WHEN mahasiswa.id_mhs IN (
			SELECT
				id_mhs
			FROM
				AUCC.pengambilan_mk
			WHERE
				id_semester IN (
					SELECT
						id_semester
					FROM
						AUCC.semester
					WHERE
						thn_akademik_semester = '{$thn_akademik_semester}'
					AND nm_semester = '{$nm_semester}'
				)
		) THEN
			1
		ELSE
			0
		END AS krs,
		CASE
	WHEN mahasiswa.id_mhs IN (
		SELECT
			id_mhs
		FROM
			AUCC.pengambilan_mk
		WHERE
			id_semester IN (
				SELECT
					id_semester
				FROM
					AUCC.semester
				WHERE
					thn_akademik_semester = '{$thn_akademik_semester}'
				AND nm_semester = '{$nm_semester}'
			)
		AND id_kurikulum_mk IN (
			SELECT
				id_kurikulum_mk
			FROM
				AUCC.kurikulum_mk
			WHERE
				status_mkta IN (1)
		)
	) THEN
		1
	ELSE
		0
	END AS skripsi
	FROM
		AUCC.mahasiswa
	JOIN AUCC.PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
	JOIN AUCC.FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS AND FAKULTAS.ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}'
	WHERE
		thn_angkatan_mhs <= '2014' 
	) s1
LEFT JOIN (
	SELECT
		id_mhs,
		id_semester,
		smt,
		status_akd1,
		rangking
	FROM
		(
			SELECT
				id_mhs,
				id_semester,
				smt,
				status_akd1,
				ROW_NUMBER () OVER (
					PARTITION BY id_mhs
					ORDER BY
						smt DESC
				) rangking
			FROM
				(
					SELECT
						id_mhs,
						admisi.id_semester,
						CASE
					WHEN group_semester = 'Ganjil' THEN
						thn_akademik_semester || 1
					ELSE
						thn_akademik_semester || 2
					END AS smt,
					status_akd_mhs AS status_akd1
				FROM
					AUCC.admisi
				JOIN AUCC.semester ON admisi.id_semester = semester.id_semester
				WHERE
					status_akd_mhs IN (2, 4, 5, 6, 20)
				AND status_apv = 1
				)
			WHERE
				smt <= '{$smt_string}'
		)
	WHERE
		rangking = 1
) s2 ON s1.id_mhs = s2.id_mhs
ORDER BY
	tahun,
	id_fakultas,
	id_program_studi,
	nim_mhs
	)
		");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

//wh_ipk_ips 4.2
$db_wh->Query(
	"INSERT INTO wh_ipk_ips 
	(id_mhs,
	nim_mhs,
	NM_PENGGUNA,
	status,
	thn_angkatan_mhs,
	id_program_studi,
	tahun,
	id_fakultas,	
	sks_sem,
	ips,
	SKS_TOTAL_MHS,
	IPK_MHS,
	id_jenjang,
	TGL_UPDATE)
SELECT
	s1.id_mhs,
	s1.nim_mhs,
	UPPER (s1.nm_pengguna) AS NM_PENGGUNA,
	s1.status,
thn_angkatan_mhs,
id_program_studi,
tahun,
id_fakultas,	
	COALESCE (sks_sem, 0) AS sks_sem,
	ROUND (COALESCE(ips, 0), 2) AS ips,
	COALESCE (SKS_TOTAL_MHS, 0) AS SKS_TOTAL_MHS,
	ROUND (COALESCE(IPK_MHS, 0), 2) AS IPK_MHS,
	s1.id_jenjang,
SYSDATE AS TGL_UPDATE
FROM
	(
		SELECT
			id_mhs,
			nim_mhs,
			nm_pengguna,
			status,
			id_jenjang,
			thn_angkatan_mhs,
			id_program_studi,
			tahun,
			id_fakultas,
			CASE
		WHEN SUM (bobot * kredit_semester) = 0 THEN
			0
		ELSE
			ROUND (
				COALESCE (
					SUM (bobot * kredit_semester) / SUM (kredit_semester),
					0
				),
				2
			)
		END AS ips,
		CASE
	WHEN id_fakultas = 7 THEN
		SUM (sksreal)
	ELSE
		SUM (kredit_semester)
	END AS sks_sem
	FROM
		(
			SELECT
				id_mhs,
				nim_mhs,
				nm_pengguna,
				id_kurikulum_mk,
				id_fakultas,
				kredit_semester,
				id_jenjang,
				thn_angkatan_mhs,
				id_program_studi,
				tahun,
				status,
				sksreal,
				MIN (nilai_huruf) AS nilai,
				MAX (bobot) AS bobot
			FROM
				(
					SELECT
						A .id_mhs,
						M .nim_mhs,
						pg.nm_pengguna,
						ps.id_fakultas,
						A .id_kurikulum_mk,
						M .status,
						ps.id_jenjang,
						CASE
					WHEN (
						A .nilai_huruf = 'E'
						OR A .nilai_huruf IS NULL
					)
					AND D .status_mkta IN (1, 2) THEN
						0
					ELSE
						D .kredit_semester
					END AS kredit_semester,
					D .kredit_semester AS sksreal,
					CASE
				WHEN A .nilai_huruf IS NULL THEN
					'E'
				ELSE
					A .nilai_huruf
				END AS nilai_huruf,
				CASE
			WHEN E .nilai_standar_nilai IS NULL THEN
				0
			ELSE
				E .nilai_standar_nilai
			END AS bobot,
			M .thn_angkatan_mhs,
			M .id_program_studi,
			CASE
		WHEN group_semester = 'Ganjil' THEN
			thn_akademik_semester || 1
		ELSE
			thn_akademik_semester || 2
		END AS tahun
		FROM
			AUCC.pengambilan_mk A
		JOIN AUCC.kurikulum_mk D ON A .id_kurikulum_mk = D .id_kurikulum_mk
		JOIN AUCC.standar_nilai E ON A .nilai_huruf = E .nm_standar_nilai
		JOIN AUCC.mahasiswa M ON A .id_mhs = M .id_mhs
		JOIN AUCC.pengguna pg ON M .id_pengguna = pg.id_pengguna
		JOIN AUCC.program_studi ps ON M .id_program_studi = ps.id_program_studi
		JOIN AUCC.semester s ON A .id_semester = s.id_semester
		WHERE
			group_semester || thn_akademik_semester IN 
			('Ganjil2013','Genap2013')
		AND tipe_semester IN ('UP', 'REG', 'RD', 'SK')
		AND A .status_apv_pengambilan_mk = '1'
		AND A .status_hapus = 0
		AND A .status_pengambilan_mk != 0
				)
			GROUP BY
				id_mhs,
				nim_mhs,
				nm_pengguna,
				id_kurikulum_mk,
				id_fakultas,
				kredit_semester,
				thn_angkatan_mhs,
				id_program_studi,
				tahun,
				sksreal,
				status,
				id_jenjang
		)
	GROUP BY
		id_mhs,
		nim_mhs,
		nm_pengguna,
		thn_angkatan_mhs,
		id_program_studi,
		id_fakultas,
		tahun,
		status,
		id_jenjang
	) s1

LEFT JOIN (
	SELECT
		id_mhs,
		COALESCE (SUM(kredit_semester), 0) AS SKS_TOTAL_MHS,
		ROUND (
			(
				COALESCE (
					(
						SUM (
							(
								kredit_semester * nilai_standar_nilai
							)
						) / SUM (kredit_semester)
					),
					0
				)
			),
			2
		) AS IPK_MHS
	FROM
		(
			SELECT
				A .id_mhs,
				c.kd_mata_kuliah,
				D .kredit_semester,
				A .nilai_huruf,
				E .nilai_standar_nilai,
				CASE
			WHEN group_semester = 'Ganjil' THEN
				thn_akademik_semester || 1
			ELSE
				thn_akademik_semester || 2
			END AS tahun,
			ROW_NUMBER () OVER (
				PARTITION BY A .id_mhs,
				c.nm_mata_kuliah
			ORDER BY
				nilai_huruf
			) rangking
		FROM
			AUCC.pengambilan_mk A
		JOIN AUCC.kurikulum_mk D ON A .id_kurikulum_mk = D .id_kurikulum_mk
		JOIN AUCC.mata_kuliah c ON D .id_mata_kuliah = c.id_mata_kuliah
		JOIN AUCC.standar_nilai E ON A .nilai_huruf = E .nm_standar_nilai
		JOIN AUCC.semester smt ON A .id_semester = smt.id_semester
		JOIN AUCC.mahasiswa M ON A .id_mhs = M .id_mhs
		WHERE
			A .status_apv_pengambilan_mk = 1
		AND (
			A .nilai_huruf <> 'E'
			OR A .nilai_huruf <> NULL
			OR A .nilai_huruf <> ''
		)
		AND D .kredit_semester <> 0
		AND A .status_hapus = 0
		AND A .status_pengambilan_mk != 0
		)
	WHERE
		tahun <= '{$smt_string}'
	AND rangking = 1
	GROUP BY
		id_mhs
) s2 ON s1.id_mhs = s2.id_mhs");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

//wh_elpt 4.3
$db_wh->Query(
	"INSERT INTO wh_elpt (NIM_MHS, ID_MHS, ID_JENJANG, ID_PROGRAM_STUDI, id_fakultas, status, thn_angkatan_mhs,elpt_score,overall )
		select nim_mhs,mahasiswa.id_mhs,program_studi.id_jenjang,program_studi.id_program_studi,
		program_studi.id_fakultas,status,thn_angkatan_mhs,elpt_score,overall 
		from AUCC.mahasiswa
		join AUCC.program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
		left join AUCC.calon_mahasiswa_data on mahasiswa.id_c_mhs=calon_mahasiswa_data.id_c_mhs
		left join AUCC.toefl on mahasiswa.id_mhs=toefl.id_mhs
		order by id_fakultas,id_jenjang,id_program_studi,nim_mhs");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

//wh_distribusi_nilai 4.5
$db_wh->Query("
	insert into wh_distribusi_nilai 
SELECT
	id_fakultas,
	id_program_studi,
	id_jenjang,
	tahun,
	kd_mata_kuliah,
	nm_mata_kuliah,
	status,
	SUM (pst) AS pst,
	SUM (pstulang) AS pstulang,
	CASE
WHEN status_mkta IN (1, 2, 3, 4) THEN
	100
ELSE
	SUM (dosen_hadir)
END AS dosen_hadir,
 CASE
WHEN status_mkta IN (1, 2, 3, 4) THEN
	100
ELSE
	SUM (jumlah_pertemuan_kelas_mk)
END AS jumlah_pertemuan_kelas_mk,
 COUNT (id_kelas_mk) AS kls_paralel,
 SUM (nila) AS nila,
 SUM (nilab) AS nilab,
 SUM (nilb) AS nilb,
 SUM (nilbc) AS nilbc,
 SUM (nilc) AS nilc,
 SUM (nild) AS nild,
 SUM (nile) AS nile
FROM
	(
		SELECT
			id_fakultas,
			id_program_studi,
			id_jenjang,
			s1.id_kelas_mk,
			id_kurikulum_mk,
			status,
			kd_mata_kuliah,
			nm_mata_kuliah,
			tahun,
			pst,
			pstulang,
			CASE
		WHEN jumlah_pertemuan_kelas_mk < s2.dosen_hadir THEN
			s2.dosen_hadir
		ELSE
			jumlah_pertemuan_kelas_mk
		END AS jumlah_pertemuan_kelaS_mk,
		COALESCE (s2.dosen_hadir, 0) AS dosen_hadir,
		status_mkta,
		nila,
		nilab,
		nilb,
		nilbc,
		nilc,
		nild,
		nile
	FROM
		(
			SELECT
				ps.id_fakultas,
				ps.id_program_studi,
				ps.id_jenjang,
				kmk.id_kelas_mk,
				kmk.id_kurikulum_mk,
				M .status,
				kk.status_mkta,
				kd_mata_kuliah,
				nm_mata_kuliah,
				COALESCE (
					kmk.jumlah_pertemuan_kelas_mk,
					0
				) AS jumlah_pertemuan_kelas_mk,
				CASE
			WHEN s.nm_semester = 'Ganjil' THEN
				s.thn_akademik_semester || 1
			ELSE
				s.thn_akademik_semester || 2
			END AS tahun,
			COUNT (pmk.id_mhs) AS pst,
			SUM (
				CASE
				WHEN 
					(
						select count(*) from AUCC.pengambilan_mk@AUCCRO a 
						LEFT JOIN AUCC.SEMESTER@AUCCRO b ON b.ID_SEMESTER = A.ID_SEMESTER 
						where a.id_mhs = pmk.id_mhs and a.id_kurikulum_mk = pmk.id_kurikulum_mk and b.thn_akademik_semester != '2014'
						AND b.nm_semester != 'Ganjil'
					) > 0 THEN
					1
				ELSE
					0
				END
			) AS pstulang,
			SUM (
				CASE
				WHEN nilai_huruf = 'A' THEN
					1
				ELSE
					0
				END
			) AS nila,
			SUM (
				CASE
				WHEN nilai_huruf = 'AB' THEN
					1
				ELSE
					0
				END
			) AS nilab,
			SUM (
				CASE
				WHEN nilai_huruf = 'B' THEN
					1
				ELSE
					0
				END
			) AS nilb,
			SUM (
				CASE
				WHEN nilai_huruf = 'BC' THEN
					1
				ELSE
					0
				END
			) AS nilbc,
			SUM (
				CASE
				WHEN nilai_huruf = 'C' THEN
					1
				ELSE
					0
				END
			) AS nilc,
			SUM (
				CASE
				WHEN nilai_huruf = 'D' THEN
					1
				ELSE
					0
				END
			) AS nild,
			SUM (
				CASE
				WHEN nilai_huruf = 'E'
				OR nilai_huruf = ''
				OR nilai_huruf IS NULL THEN
					1
				ELSE
					0
				END
			) AS nile
		FROM
			AUCC.kelas_mk@AUCCRO kmk
		JOIN AUCC.pengambilan_mk@AUCCRO pmk ON kmk.id_kelas_mk = pmk.id_kelas_mk
		JOIN AUCC.semester@AUCCRO s ON pmk.id_semester = s.id_semester
		JOIN AUCC.mahasiswa@AUCCRO M ON pmk.id_mhs = M .id_mhs
		JOIN AUCC.program_studi@AUCCRO ps ON M .id_program_studi = ps.id_program_studi
		JOIN AUCC.kurikulum_mk@AUCCRO kk ON kmk.id_kurikulum_mk = kk.id_kurikulum_mk
		JOIN AUCC.mata_kuliah@AUCCRO mk ON kk.id_mata_kuliah = mk.id_mata_kuliah
		WHERE
			thn_akademik_semester = '2014'
		AND nm_semester = 'Ganjil'
		AND pmk.status_apv_pengambilan_mk = 1
		GROUP BY
			ps.id_fakultas,
			ps.id_program_studi,
			ps.id_jenjang,
			kmk.id_kelas_mk,
			kmk.id_kurikulum_mk,
			M .status,
			nm_semester,
			thn_akademik_semester,
			kd_mata_kuliah,
			nm_mata_kuliah,
			kmk.jumlah_pertemuan_kelas_mk,
			kk.status_mkta
		) s1
	LEFT JOIN (
		SELECT
			id_kelas_mk,
			COUNT (*) AS dosen_hadir
		FROM
			AUCC.presensi_kelas@AUCCRO
		WHERE
			id_kelas_mk IN (
				SELECT
					id_kelas_mk
				FROM
					AUCC.kelas_mk@AUCCRO
				LEFT JOIN AUCC.SEMESTER@AUCCRO ON SEMESTER.ID_SEMESTER = KELAS_MK.ID_SEMESTER
				WHERE
					thn_akademik_semester = '2014'
					AND nm_semester = 'Ganjil'
			)
		GROUP BY
			id_kelas_mk
	) s2 ON s1.id_kelas_mk = s2.id_kelas_mk
	)
WHERE
	pst >= 1
GROUP BY
	id_fakultas,
	id_program_studi,
	id_jenjang,
	id_kurikulum_mk,
	status,
	kd_mata_kuliah,
	nm_mata_kuliah,
	tahun,
	status_mkta
ORDER BY
	id_fakultas,
	id_program_studi,
	id_jenjang,
	nm_mata_kuliah;
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

//wh_ik 4.8
$db_wh->Query("
		INSERT INTO WH_IK 
		(
		ID_FAKULTAS,
		ID_PROGRAM_STUDI,
		ID_JENJANG,
		JALUR,
		NM_MATA_KULIAH,
		status_mkta,
		tahun,
		IK_KULIAH,
		IK_PRAKTIKUM
		)
SELECT
	PS.ID_FAKULTAS,
	PS.ID_PROGRAM_STUDI,
	PS.ID_JENJANG,
	M .STATUS AS JALUR,
	MK.NM_MATA_KULIAH,
	KUMK.status_mkta,
	CASE
WHEN s.group_semester = 'Ganjil' THEN
	thn_akademik_semester || 1
ELSE
	thn_akademik_semester || 2
END AS tahun,
 ROUND (
	AVG (
		CASE
		WHEN EH.NILAI_EVAL BETWEEN '1'
		AND '4'
		AND EH.ID_EVAL_ASPEK IN (
			SELECT
				ID_EVAL_ASPEK
			FROM
				AUCC.EVALUASI_ASPEK@AUCCRO
			WHERE
				ID_EVAL_INSTRUMEN = 1
			AND TIPE_ASPEK = 1
		) THEN
			EH.NILAI_EVAL
		END
	),
	2
) IK_KULIAH,
 ROUND (
	AVG (
		CASE
		WHEN EH.NILAI_EVAL BETWEEN '1'
		AND '4'
		AND EH.ID_EVAL_ASPEK IN (
			SELECT
				ID_EVAL_ASPEK
			FROM
				AUCC.EVALUASI_ASPEK@AUCCRO
			WHERE
				ID_EVAL_INSTRUMEN = 2
			AND TIPE_ASPEK = 1
		) THEN
			EH.NILAI_EVAL
		END
	),
	2
) IK_PRAKTIKUM
FROM
	AUCC.EVALUASI_HASIL@AUCCRO EH
JOIN AUCC.DOSEN@AUCCRO D ON D .ID_DOSEN = EH.ID_DOSEN
JOIN AUCC.PENGGUNA@AUCCRO P ON P .ID_PENGGUNA = D .ID_PENGGUNA
JOIN AUCC.KELAS_MK@AUCCRO KMK ON KMK.ID_KELAS_MK = EH.ID_KELAS_MK
JOIN AUCC.KURIKULUM_MK@AUCCRO KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
JOIN AUCC.MATA_KULIAH@AUCCRO MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
JOIN AUCC.MAHASISWA@AUCCRO M ON EH.ID_MHS = M .ID_MHS
JOIN AUCC.PROGRAM_STUDI@AUCCRO PS ON PS.ID_PROGRAM_STUDI = EH.ID_PROGRAM_STUDI
JOIN AUCC.JENJANG@AUCCRO J ON J.ID_JENJANG = PS.ID_JENJANG
JOIN AUCC.FAKULTAS@AUCCRO F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
JOIN AUCC.SEMESTER@AUCCRO S ON S.ID_SEMESTER = EH.ID_SEMESTER
GROUP BY
	MK.NM_MATA_KULIAH,
	PS.ID_FAKULTAS,
	PS.ID_PROGRAM_STUDI,
	PS.ID_JENJANG,
	M .STATUS,
	s.group_semester,
	s.thn_akademik_semester,
	KUMK.status_mkta
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

//wh_411 4.10
$db_wh->Query("
	INSERT INTO wh_411 
	(
		LAYANAN,
		ID_JENJANG,
		ID_PROGRAM_STUDI,
		ID_FAKULTAS,
		PROGRAM_STUDI,
		JENJANG,
		FAKULTAS,
		STATUS JALUR,
		SEMESTER,
		IK
	) 
	SELECT
	EKA.NAMA_KELOMPOK LAYANAN,
	PS.ID_JENJANG,
	PS.ID_PROGRAM_STUDI,
	PS.ID_FAKULTAS,
	PS.NM_PROGRAM_STUDI PROGRAM_STUDI,
	J.NM_JENJANG JENJANG,
	F.NM_FAKULTAS FAKULTAS,
	M .STATUS JALUR,
	CASE
WHEN S.GROUP_SEMESTER = 'Ganjil' THEN
	S.THN_AKADEMIK_SEMESTER || 1
ELSE
	S.THN_AKADEMIK_SEMESTER || 2
END AS SEMESTER,
 ROUND (
	AVG (
		CASE
		WHEN EH.NILAI_EVAL BETWEEN '1'
		AND '4'
		AND EH.ID_EVAL_ASPEK IN (
			SELECT
				ID_EVAL_ASPEK
			FROM
				AUCC.EVALUASI_ASPEK@AUCCRO
			WHERE
				ID_EVAL_INSTRUMEN = 6
			AND TIPE_ASPEK = 1
		) THEN
			EH.NILAI_EVAL
		END
	),
	2
) IK
FROM
	AUCC.EVALUASI_HASIL@AUCCRO EH
JOIN AUCC.EVALUASI_ASPEK@AUCCRO EA ON EH.ID_EVAL_ASPEK = EA.ID_EVAL_ASPEK
JOIN AUCC.EVALUASI_KELOMPOK_ASPEK@AUCCRO EKA ON EKA.ID_EVAL_KELOMPOK_ASPEK = EH.ID_EVAL_KELOMPOK_ASPEK
JOIN AUCC.PROGRAM_STUDI@AUCCRO PS ON PS.ID_PROGRAM_STUDI = EH.ID_PROGRAM_STUDI
JOIN AUCC.JENJANG@AUCCRO J ON J.ID_JENJANG = PS.ID_JENJANG
JOIN AUCC.FAKULTAS@AUCCRO F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
JOIN AUCC.MAHASISWA@AUCCRO M ON M .ID_MHS = EH.ID_MHS
JOIN AUCC.SEMESTER@AUCCRO S ON S.ID_SEMESTER = EH.ID_SEMESTER
WHERE
	EH.ID_EVAL_INSTRUMEN = 6
GROUP BY
	EKA.NAMA_KELOMPOK,
	PS.ID_JENJANG,
	PS.ID_PROGRAM_STUDI,
	PS.ID_FAKULTAS,
	PS.NM_PROGRAM_STUDI,
	J.NM_JENJANG,
	F.NM_FAKULTAS,
	M .STATUS,
	S.GROUP_SEMESTER,
	S.THN_AKADEMIK_SEMESTER
ORDER BY
	F.NM_FAKULTAS,
	PS.NM_PROGRAM_STUDI,
	J.NM_JENJANG;
	
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

//wh_eva_maba 4.11
$db_wh->Query("
	INSERT INTO wh_eva_maba
	(
		id_fakultas,
		id_program_studi,
		tahun,
		nama_kelompok,
		RATA, 
		ID_JENJANG
	)
SELECT
	id_fakultas,
	id_program_studi,
	tahun,
	nama_kelompok,
	RATA, 
	ID_JENJANG
FROM
	(
		SELECT			
		program_studi.id_fakultas,
		program_studi.id_program_studi,
		program_studi.id_jenjang,
			CASE
		WHEN nm_semester = 'Ganjil' THEN
			thn_akademik_semester || '1'
		ELSE
			thn_akademik_semester || '2'
		END AS tahun,
		ID_EVAL_KELOMPOK_ASPEK,
		ROUND (
			AVG (
				CASE
				WHEN NILAI_EVAL BETWEEN '1'
				AND '4' THEN
					NILAI_EVAL
				END
			),
			2
		) RATA
	FROM
		AUCC.EVALUASI_HASIL@AUCCRO EV
	JOIN AUCC.semester@AUCCRO ON EV.id_semester = semester.id_semester
	JOIN AUCC.program_studi@AUCCRO ON EV.id_program_studi = program_studi.id_program_studi
	WHERE
		ID_EVAL_INSTRUMEN = 4
	AND ID_EVAL_ASPEK IN (
		SELECT
			ID_EVAL_ASPEK
		FROM
			AUCC.EVALUASI_ASPEK@AUCCRO
		WHERE
			TIPE_ASPEK = 1
		AND ID_EVAL_INSTRUMEN = 4
	)
	GROUP BY
		ID_EVAL_KELOMPOK_ASPEK,
		program_studi.id_fakultas,
		program_studi.id_program_studi,
		program_studi.id_jenjang,
		nm_semester,
		thn_akademik_semester
	) s1
LEFT JOIN (
	SELECT
		EKS.ID_EVAL_KELOMPOK_ASPEK,
		EKS.NAMA_KELOMPOK
	FROM
		AUCC.EVALUASI_KELOMPOK_ASPEK@AUCCRO EKS
	JOIN AUCC.EVALUASI_ASPEK@AUCCRO EA ON EA.ID_EVAL_KELOMPOK_ASPEK = EKS.ID_EVAL_KELOMPOK_ASPEK
	AND EA.TIPE_ASPEK = 1
	WHERE
		EKS.ID_EVAL_INSTRUMEN = 4
	GROUP BY
		EKS.ID_EVAL_KELOMPOK_ASPEK,
		EKS.NAMA_KELOMPOK
) s2 ON s1.ID_EVAL_KELOMPOK_ASPEK = s2.ID_EVAL_KELOMPOK_ASPEK

");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

//WH_SE_LULUSAN_IPK_MHS 3.1
$db_wh->Query("
	INSERT INTO WH_SE_LULUSAN_IPK_MHS (THN_AKADEMIK_SMT, THN_AKADEMIK_LULUS, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, STATUS_MHS, JENIS_KELAMIN, NIM_MHS, IPK, SKS, TGL_UPDATE)
select THN_AKADEMIK_SEMESTER || (CASE WHEN NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_SEMESTER, to_char(tgl_lulus, 'yyyy') as THN_AKADEMIK_LULUS,
		PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_JENJANG, MAHASISWA.STATUS, KELAMIN_PENGGUNA, NIM_MHS, ipk, sks, SYSDATE
		from AUCC.mahasiswa@AUCCRO
						join AUCC.pengguna@AUCCRO on pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
						join AUCC.admisi@AUCCRO on admisi.id_mhs = mahasiswa.id_mhs and status_akd_mhs = 4 and status_apv = 1
						join AUCC.program_studi@AUCCRO on program_studi.id_program_studi = mahasiswa.id_program_studi
						join AUCC.semester@AUCCRO on SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
						left join (
							select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
							from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
							c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
							row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
							count(*) over(partition by c.nm_mata_kuliah) terulang
								from AUCC.pengambilan_mk@AUCCRO a, AUCC.mata_kuliah@AUCCRO c, AUCC.kurikulum_mk@AUCCRO d, AUCC.semester@AUCCRO e, 
								AUCC.standar_nilai@AUCCRO f, AUCC.mahasiswa@AUCCRO g, AUCC.program_studi@AUCCRO h
								where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
								and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs  and a.status_hapus=0
								and h.id_program_studi = g.id_program_studi and status_akademik_mhs = 4
							) x
							where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
							group by id_mhs) x on x.id_mhs = MAHASISWA.ID_MHS
		where tgl_lulus is not null and to_char(TGL_LULUS, 'YYYY') >= '2007'
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

//WH_SE_LULUSAN_MASA_ST 3.5
$db_wh->Query("
	INSERT INTO WH_SE_LULUSAN_MASA_ST (ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, STATUS_MHS, THN_AKADEMIK_LULUS, THN_AKADEMIK_SMT, JENIS_KELAMIN, MASA_STUDI, TGL_UPDATE)
SELECT
	ID_PROGRAM_STUDI,
	ID_FAKULTAS,
	id_jenjang,
	STATUS,
	THN_AKADEMIK_LULUS,
	THN_AKADEMIK_SMT,
	kelamin_pengguna,
	(
		lama_studi + (
			CASE
			WHEN semester_studi = 1 THEN
				1
			WHEN semester_studi = 0 THEN
				0.5
			ELSE
				0
			END
		)
	) AS LAMA_STUDI,
	SYSDATE
FROM
	(
		SELECT
			PROGRAM_STUDI.ID_PROGRAM_STUDI,
			PROGRAM_STUDI.ID_FAKULTAS,
			JENJANG.id_jenjang,
			STATUS,
			TO_CHAR (TGL_LULUS, 'YYYY') AS THN_AKADEMIK_LULUS,
			b.THN_AKADEMIK_SEMESTER || (
				CASE
				WHEN b.NM_SEMESTER = 'Ganjil' THEN
					1
				ELSE
					2
				END
			) AS THN_AKADEMIK_SMT,
			(
				b.THN_AKADEMIK_SEMESTER - A .THN_AKADEMIK_SEMESTER - (
					CASE
					WHEN cuti IS NULL THEN
						0
					ELSE
						cuti
					END
				)
			) AS lama_studi,
			(
				CASE
				WHEN b.NM_SEMESTER = 'Ganjil' THEN
					1
				ELSE
					2
				END
			) - (
				CASE
				WHEN A .NM_SEMESTER = 'Ganjil' THEN
					1
				ELSE
					2
				END
			) AS semester_studi,
			kelamin_pengguna
		FROM
			AUCC.mahasiswa@AUCCRO
		JOIN AUCC.semester@AUCCRO A ON A .id_semester = mahasiswa.id_semester_masuk
		JOIN AUCC.pengguna@AUCCRO ON PENGGUNA.id_pengguna = MAHASISWA.id_pengguna
		JOIN AUCC.program_studi@AUCCRO ON program_studi.id_program_studi = mahasiswa.id_program_studi
		JOIN AUCC.fakultas@AUCCRO ON fakultas.id_fakultas = program_studi.id_fakultas
		JOIN AUCC.status_pengguna@AUCCRO ON status_pengguna.id_status_pengguna = status_akademik_mhs
		JOIN AUCC.jenjang@AUCCRO ON jenjang.id_jenjang = program_studi.id_jenjang
		JOIN AUCC.admisi@AUCCRO ON ADMISI.ID_MHS = MAHASISWA.ID_MHS
		AND status_akd_mhs = '4'
		AND status_apv = 1
		AND TGL_LULUS IS NOT NULL
		JOIN AUCC.SEMESTER@AUCCRO b ON b.ID_SEMESTER = ADMISI.ID_SEMESTER
		LEFT JOIN (
			SELECT
				id_mhs,
				COUNT (*) AS cuti
			FROM
				AUCC.ADMISI@AUCCRO
			WHERE
				status_apv = 1
			AND status_akd_mhs = 2
			GROUP BY
				id_mhs
		) x ON x.id_mhs = MAHASISWA.id_mhs
		WHERE
			TO_CHAR (TGL_LULUS, 'YYYY') >= '2007'
	)
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

//WH_SE_LULUSAN_SKRIPSI 3.6
$db_wh->Query("
INSERT INTO WH_SE_LULUSAN_SKRIPSI 
(THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, STATUS_MHS, LAMA_SKRIPSI1_JML, LAMA_SKRIPSI2_JML, LAMA_SKRIPSI3_JML, THN_AKADEMIK_LULUS)
SELECT
	THN_SEMESTER AS THN_AKADEMIK_SMT,
	ID_PROGRAM_STUDI,
	ID_FAKULTAS,
	ID_JENJANG,
	STATUS AS STATUS_MHS,
	TOTAL_LULUSAN,
	CASE
WHEN TOTAL_LULUSAN != (
	LAMA_SKRIPSI1_JML + LAMA_SKRIPSI2_JML + LAMA_SKRIPSI3_JML
) THEN
	(
		LAMA_SKRIPSI1_JML + TOTAL_LULUSAN - (
			LAMA_SKRIPSI1_JML + LAMA_SKRIPSI2_JML + LAMA_SKRIPSI3_JML
		)
	)
ELSE
	LAMA_SKRIPSI1_JML
END AS LAMA_SKRIPSI1_JML,
 LAMA_SKRIPSI2_JML,
 LAMA_SKRIPSI3_JML,
 TAHUN_LULUS AS THN_AKADEMIK_LULUS
FROM
	(
		SELECT
			A .*, (
				CASE
				WHEN LAMA_SKRIPSI1_JML IS NULL
				AND LAMA_SKRIPSI2_JML IS NULL
				AND LAMA_SKRIPSI3_JML IS NULL THEN
					TOTAL_LULUSAN
				WHEN LAMA_SKRIPSI1_JML IS NULL THEN
					0
				ELSE
					LAMA_SKRIPSI1_JML
				END
			) AS LAMA_SKRIPSI1_JML,
			(
				CASE
				WHEN LAMA_SKRIPSI2_JML IS NULL THEN
					0
				ELSE
					LAMA_SKRIPSI2_JML
				END
			) AS LAMA_SKRIPSI2_JML,
			(
				CASE
				WHEN LAMA_SKRIPSI3_JML IS NULL THEN
					0
				ELSE
					LAMA_SKRIPSI3_JML
				END
			) AS LAMA_SKRIPSI3_JML
		FROM
			(
				SELECT
					THN_AKADEMIK_SEMESTER || (
						CASE
						WHEN NM_SEMESTER = 'Ganjil' THEN
							1
						ELSE
							2
						END
					) AS THN_SEMESTER,
					TO_CHAR (tgl_lulus, 'yyyy') AS tahun_lulus,
					PROGRAM_STUDI.ID_PROGRAM_STUDI,
					PROGRAM_STUDI.ID_FAKULTAS,
					PROGRAM_STUDI.ID_JENJANG,
					MAHASISWA.STATUS,
					COUNT (*) AS TOTAL_LULUSAN
				FROM
					AUCC.mahasiswa@AUCCRO
				JOIN AUCC.pengguna@AUCCRO ON pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
				JOIN AUCC.admisi@AUCCRO ON admisi.id_mhs = mahasiswa.id_mhs
				AND status_akd_mhs = 4
				AND status_apv = 1
				AND tgl_lulus IS NOT NULL
				JOIN AUCC.program_studi@AUCCRO ON program_studi.id_program_studi = mahasiswa.id_program_studi
				JOIN AUCC.semester@AUCCRO ON SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
				WHERE
					TO_CHAR (TGL_LULUS, 'YYYY') >= '2007'
				GROUP BY
					THN_AKADEMIK_SEMESTER || (
						CASE
						WHEN NM_SEMESTER = 'Ganjil' THEN
							1
						ELSE
							2
						END
					),
					TO_CHAR (tgl_lulus, 'yyyy'),
					PROGRAM_STUDI.ID_PROGRAM_STUDI,
					PROGRAM_STUDI.ID_FAKULTAS,
					PROGRAM_STUDI.ID_JENJANG,
					MAHASISWA.STATUS
			) A
		LEFT JOIN (
			SELECT
				THN_AKADEMIK_SEMESTER || (
					CASE
					WHEN NM_SEMESTER = 'Ganjil' THEN
						1
					ELSE
						2
					END
				) AS THN_SEMESTER,
				TO_CHAR (tgl_lulus, 'yyyy') AS tahun_lulus,
				PROGRAM_STUDI.ID_PROGRAM_STUDI,
				PROGRAM_STUDI.ID_FAKULTAS,
				PROGRAM_STUDI.ID_JENJANG,
				MAHASISWA.STATUS,
				COUNT (*) AS LAMA_SKRIPSI1_JML
			FROM
					AUCC.mahasiswa@AUCCRO
				JOIN AUCC.pengguna@AUCCRO ON pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
				JOIN AUCC.admisi@AUCCRO ON admisi.id_mhs = mahasiswa.id_mhs
				AND status_akd_mhs = 4
				AND status_apv = 1
				AND tgl_lulus IS NOT NULL
				JOIN AUCC.program_studi@AUCCRO ON program_studi.id_program_studi = mahasiswa.id_program_studi
				JOIN AUCC.semester@AUCCRO ON SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
			JOIN (
				SELECT
					A .id_mhs,
					G .nim_mhs,
					COUNT (*) AS lama_skripsi
				FROM
					AUCC.pengambilan_mk@AUCCRO A,
					AUCC.mata_kuliah@AUCCRO c,
					AUCC.kurikulum_mk@AUCCRO D,
					AUCC.semester@AUCCRO E,
					AUCC.standar_nilai@AUCCRO f,
					AUCC.mahasiswa@AUCCRO G,
					AUCC.program_studi@AUCCRO H
				WHERE
					A .id_kurikulum_mk = D .id_kurikulum_mk
				AND D .id_mata_kuliah = c.id_mata_kuliah
				AND A .id_semester = E .id_semester
				AND A .STATUS_APV_PENGAMBILAN_MK = '1'
				AND nm_standar_nilai = nilai_huruf
				AND G .id_mhs = A .id_mhs
				AND A .status_hapus = 0
				AND H .id_program_studi = G .id_program_studi
				AND status_akademik_mhs = 4
				AND D .status_mkta = 1
				AND A .flagnilai = 1
				GROUP BY
					A .id_mhs,
					G .nim_mhs
			) x ON x.id_mhs = MAHASISWA.ID_MHS
			AND lama_skripsi <= 1
			AND PROGRAM_STUDI.ID_JENJANG IN (1, 5)
			WHERE
				TO_CHAR (TGL_LULUS, 'YYYY') >= '2007'
			GROUP BY
				THN_AKADEMIK_SEMESTER || (
					CASE
					WHEN NM_SEMESTER = 'Ganjil' THEN
						1
					ELSE
						2
					END
				),
				TO_CHAR (tgl_lulus, 'yyyy'),
				PROGRAM_STUDI.ID_PROGRAM_STUDI,
				PROGRAM_STUDI.ID_FAKULTAS,
				PROGRAM_STUDI.ID_JENJANG,
				MAHASISWA.STATUS
			UNION
				SELECT
					THN_AKADEMIK_SEMESTER || (
						CASE
						WHEN NM_SEMESTER = 'Ganjil' THEN
							1
						ELSE
							2
						END
					) AS THN_SEMESTER,
					TO_CHAR (tgl_lulus, 'yyyy') AS tahun_lulus,
					PROGRAM_STUDI.ID_PROGRAM_STUDI,
					PROGRAM_STUDI.ID_FAKULTAS,
					PROGRAM_STUDI.ID_JENJANG,
					MAHASISWA.STATUS,
					COUNT (*) AS LAMA_SKRIPSI1_JML
				FROM
					AUCC.mahasiswa@AUCCRO
				JOIN AUCC.pengguna@AUCCRO ON pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
				JOIN AUCC.admisi@AUCCRO ON admisi.id_mhs = mahasiswa.id_mhs
				AND status_akd_mhs = 4
				AND status_apv = 1
				AND tgl_lulus IS NOT NULL
				JOIN AUCC.program_studi@AUCCRO ON program_studi.id_program_studi = mahasiswa.id_program_studi
				JOIN AUCC.semester@AUCCRO ON SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
				JOIN (
					SELECT
						A .id_mhs,
						G .nim_mhs,
						COUNT (*) AS lama_skripsi
					FROM
						AUCC.pengambilan_mk@AUCCRO A,
						AUCC.mata_kuliah@AUCCRO c,
						AUCC.kurikulum_mk@AUCCRO D,
						AUCC.semester@AUCCRO E,
						AUCC.standar_nilai@AUCCRO f,
						AUCC.mahasiswa@AUCCRO G,
						AUCC.program_studi@AUCCRO H
					WHERE
						A .id_kurikulum_mk = D .id_kurikulum_mk
					AND D .id_mata_kuliah = c.id_mata_kuliah
					AND A .id_semester = E .id_semester
					AND A .STATUS_APV_PENGAMBILAN_MK = '1'
					AND nm_standar_nilai = nilai_huruf
					AND G .id_mhs = A .id_mhs
					AND A .status_hapus = 0
					AND H .id_program_studi = G .id_program_studi
					AND status_akademik_mhs = 4
					AND D .status_mkta = 1
					AND A .flagnilai = 1
					GROUP BY
						A .id_mhs,
						G .nim_mhs
				) x ON x.id_mhs = MAHASISWA.ID_MHS
				AND lama_skripsi <= 2
				AND PROGRAM_STUDI.ID_JENJANG = 2
				WHERE
					TO_CHAR (TGL_LULUS, 'YYYY') >= '2007'
				GROUP BY
					THN_AKADEMIK_SEMESTER || (
						CASE
						WHEN NM_SEMESTER = 'Ganjil' THEN
							1
						ELSE
							2
						END
					),
					TO_CHAR (tgl_lulus, 'yyyy'),
					PROGRAM_STUDI.ID_PROGRAM_STUDI,
					PROGRAM_STUDI.ID_FAKULTAS,
					PROGRAM_STUDI.ID_JENJANG,
					MAHASISWA.STATUS
				UNION
					SELECT
						THN_AKADEMIK_SEMESTER || (
							CASE
							WHEN NM_SEMESTER = 'Ganjil' THEN
								1
							ELSE
								2
							END
						) AS THN_SEMESTER,
						TO_CHAR (tgl_lulus, 'yyyy') AS tahun_lulus,
						PROGRAM_STUDI.ID_PROGRAM_STUDI,
						PROGRAM_STUDI.ID_FAKULTAS,
						PROGRAM_STUDI.ID_JENJANG,
						MAHASISWA.STATUS,
						COUNT (*) AS LAMA_SKRIPSI1_JML
					FROM
					AUCC.mahasiswa@AUCCRO
					JOIN AUCC.pengguna@AUCCRO ON pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
					JOIN AUCC.admisi@AUCCRO ON admisi.id_mhs = mahasiswa.id_mhs
					AND status_akd_mhs = 4
					AND status_apv = 1
					AND tgl_lulus IS NOT NULL
					JOIN AUCC.program_studi@AUCCRO ON program_studi.id_program_studi = mahasiswa.id_program_studi
					JOIN AUCC.semester@AUCCRO ON SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
					JOIN (
						SELECT
							A .id_mhs,
							G .nim_mhs,
							COUNT (*) AS lama_skripsi
						FROM
							AUCC.pengambilan_mk@AUCCRO A,
							AUCC.mata_kuliah@AUCCRO c,
							AUCC.kurikulum_mk@AUCCRO D,
							AUCC.semester@AUCCRO E,
							AUCC.standar_nilai@AUCCRO f,
							AUCC.mahasiswa@AUCCRO G,
							AUCC.program_studi@AUCCRO H
						WHERE
							A .id_kurikulum_mk = D .id_kurikulum_mk
						AND D .id_mata_kuliah = c.id_mata_kuliah
						AND A .id_semester = E .id_semester
						AND A .STATUS_APV_PENGAMBILAN_MK = '1'
						AND nm_standar_nilai = nilai_huruf
						AND G .id_mhs = A .id_mhs
						AND A .status_hapus = 0
						AND H .id_program_studi = G .id_program_studi
						AND status_akademik_mhs = 4
						AND D .status_mkta = 1
						AND A .flagnilai = 1
						GROUP BY
							A .id_mhs,
							G .nim_mhs
					) x ON x.id_mhs = MAHASISWA.ID_MHS
					AND lama_skripsi <= 3
					AND PROGRAM_STUDI.ID_JENJANG = 3
					WHERE
						TO_CHAR (TGL_LULUS, 'YYYY') >= '2007'
					GROUP BY
						THN_AKADEMIK_SEMESTER || (
							CASE
							WHEN NM_SEMESTER = 'Ganjil' THEN
								1
							ELSE
								2
							END
						),
						TO_CHAR (tgl_lulus, 'yyyy'),
						PROGRAM_STUDI.ID_PROGRAM_STUDI,
						PROGRAM_STUDI.ID_FAKULTAS,
						PROGRAM_STUDI.ID_JENJANG,
						MAHASISWA.STATUS
		) B ON B.ID_PROGRAM_STUDI = A .ID_PROGRAM_STUDI
		AND B.THN_SEMESTER = A .THN_SEMESTER
		AND B.TAHUN_LULUS = A .TAHUN_LULUS
		AND B.STATUS = A .STATUS
		LEFT JOIN (
			SELECT
				THN_AKADEMIK_SEMESTER || (
					CASE
					WHEN NM_SEMESTER = 'Ganjil' THEN
						1
					ELSE
						2
					END
				) AS THN_SEMESTER,
				TO_CHAR (tgl_lulus, 'yyyy') AS tahun_lulus,
				PROGRAM_STUDI.ID_PROGRAM_STUDI,
				PROGRAM_STUDI.ID_FAKULTAS,
				PROGRAM_STUDI.ID_JENJANG,
				MAHASISWA.STATUS,
				COUNT (*) AS LAMA_SKRIPSI2_JML
			FROM
					AUCC.mahasiswa@AUCCRO
			JOIN AUCC.pengguna@AUCCRO ON pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
			JOIN AUCC.admisi@AUCCRO ON admisi.id_mhs = mahasiswa.id_mhs
			AND status_akd_mhs = 4
			AND status_apv = 1
			AND tgl_lulus IS NOT NULL
			JOIN AUCC.program_studi@AUCCRO ON program_studi.id_program_studi = mahasiswa.id_program_studi
			JOIN AUCC.semester@AUCCRO ON SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
			JOIN (
				SELECT
					A .id_mhs,
					G .nim_mhs,
					COUNT (*) AS lama_skripsi
				FROM
					AUCC.pengambilan_mk@AUCCRO A,
					AUCC.mata_kuliah@AUCCRO c,
					AUCC.kurikulum_mk@AUCCRO D,
					AUCC.semester@AUCCRO E,
					AUCC.standar_nilai@AUCCRO f,
					AUCC.mahasiswa@AUCCRO G,
					AUCC.program_studi@AUCCRO H
				WHERE
					A .id_kurikulum_mk = D .id_kurikulum_mk
				AND D .id_mata_kuliah = c.id_mata_kuliah
				AND A .id_semester = E .id_semester
				AND A .STATUS_APV_PENGAMBILAN_MK = '1'
				AND nm_standar_nilai = nilai_huruf
				AND G .id_mhs = A .id_mhs
				AND A .status_hapus = 0
				AND H .id_program_studi = G .id_program_studi
				AND status_akademik_mhs = 4
				AND D .status_mkta = 1
				AND A .flagnilai = 1
				GROUP BY
					A .id_mhs,
					G .nim_mhs
			) x ON x.id_mhs = MAHASISWA.ID_MHS
			AND lama_skripsi > 1
			AND lama_skripsi <= 2
			AND PROGRAM_STUDI.ID_JENJANG IN (1, 5)
			WHERE
				TO_CHAR (TGL_LULUS, 'YYYY') >= '2007'
			GROUP BY
				THN_AKADEMIK_SEMESTER || (
					CASE
					WHEN NM_SEMESTER = 'Ganjil' THEN
						1
					ELSE
						2
					END
				),
				TO_CHAR (tgl_lulus, 'yyyy'),
				PROGRAM_STUDI.ID_PROGRAM_STUDI,
				PROGRAM_STUDI.ID_FAKULTAS,
				PROGRAM_STUDI.ID_JENJANG,
				MAHASISWA.STATUS
			UNION
				SELECT
					THN_AKADEMIK_SEMESTER || (
						CASE
						WHEN NM_SEMESTER = 'Ganjil' THEN
							1
						ELSE
							2
						END
					) AS THN_SEMESTER,
					TO_CHAR (tgl_lulus, 'yyyy') AS tahun_lulus,
					PROGRAM_STUDI.ID_PROGRAM_STUDI,
					PROGRAM_STUDI.ID_FAKULTAS,
					PROGRAM_STUDI.ID_JENJANG,
					MAHASISWA.STATUS,
					COUNT (*) AS LAMA_SKRIPSI2_JML
				FROM
					AUCC.mahasiswa@AUCCRO
				JOIN AUCC.pengguna@AUCCRO ON pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
				JOIN AUCC.admisi@AUCCRO ON admisi.id_mhs = mahasiswa.id_mhs
				AND status_akd_mhs = 4
				AND status_apv = 1
				AND tgl_lulus IS NOT NULL
				JOIN AUCC.program_studi@AUCCRO ON program_studi.id_program_studi = mahasiswa.id_program_studi
				JOIN AUCC.semester@AUCCRO ON SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
				JOIN (
					SELECT
						A .id_mhs,
						G .nim_mhs,
						COUNT (*) AS lama_skripsi
					FROM
						AUCC.pengambilan_mk@AUCCRO A,
						AUCC.mata_kuliah@AUCCRO c,
						AUCC.kurikulum_mk@AUCCRO D,
						AUCC.semester@AUCCRO E,
						AUCC.standar_nilai@AUCCRO f,
						AUCC.mahasiswa@AUCCRO G,
						AUCC.program_studi@AUCCRO H
					WHERE
						A .id_kurikulum_mk = D .id_kurikulum_mk
					AND D .id_mata_kuliah = c.id_mata_kuliah
					AND A .id_semester = E .id_semester
					AND A .STATUS_APV_PENGAMBILAN_MK = '1'
					AND nm_standar_nilai = nilai_huruf
					AND G .id_mhs = A .id_mhs
					AND A .status_hapus = 0
					AND H .id_program_studi = G .id_program_studi
					AND status_akademik_mhs = 4
					AND D .status_mkta = 1
					AND A .flagnilai = 1
					GROUP BY
						A .id_mhs,
						G .nim_mhs
				) x ON x.id_mhs = MAHASISWA.ID_MHS
				AND lama_skripsi > 2
				AND lama_skripsi <= 3
				AND PROGRAM_STUDI.ID_JENJANG = 2
				WHERE
					TO_CHAR (TGL_LULUS, 'YYYY') >= '2007'
				GROUP BY
					THN_AKADEMIK_SEMESTER || (
						CASE
						WHEN NM_SEMESTER = 'Ganjil' THEN
							1
						ELSE
							2
						END
					),
					TO_CHAR (tgl_lulus, 'yyyy'),
					PROGRAM_STUDI.ID_PROGRAM_STUDI,
					PROGRAM_STUDI.ID_FAKULTAS,
					PROGRAM_STUDI.ID_JENJANG,
					MAHASISWA.STATUS
				UNION
					SELECT
						THN_AKADEMIK_SEMESTER || (
							CASE
							WHEN NM_SEMESTER = 'Ganjil' THEN
								1
							ELSE
								2
							END
						) AS THN_SEMESTER,
						TO_CHAR (tgl_lulus, 'yyyy') AS tahun_lulus,
						PROGRAM_STUDI.ID_PROGRAM_STUDI,
						PROGRAM_STUDI.ID_FAKULTAS,
						PROGRAM_STUDI.ID_JENJANG,
						MAHASISWA.STATUS,
						COUNT (*) AS LAMA_SKRIPSI2_JML
					FROM
					AUCC.mahasiswa@AUCCRO
					JOIN AUCC.pengguna@AUCCRO ON pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
					JOIN AUCC.admisi@AUCCRO ON admisi.id_mhs = mahasiswa.id_mhs
					AND status_akd_mhs = 4
					AND status_apv = 1
					AND tgl_lulus IS NOT NULL
					JOIN AUCC.program_studi@AUCCRO ON program_studi.id_program_studi = mahasiswa.id_program_studi
					JOIN AUCC.semester@AUCCRO ON SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
					JOIN (
						SELECT
							A .id_mhs,
							G .nim_mhs,
							COUNT (*) AS lama_skripsi
						FROM
							AUCC.pengambilan_mk@AUCCRO A,
							AUCC.mata_kuliah@AUCCRO c,
							AUCC.kurikulum_mk@AUCCRO D,
							AUCC.semester@AUCCRO E,
							AUCC.standar_nilai@AUCCRO f,
							AUCC.mahasiswa@AUCCRO G,
							AUCC.program_studi@AUCCRO H
						WHERE
							A .id_kurikulum_mk = D .id_kurikulum_mk
						AND D .id_mata_kuliah = c.id_mata_kuliah
						AND A .id_semester = E .id_semester
						AND A .STATUS_APV_PENGAMBILAN_MK = '1'
						AND nm_standar_nilai = nilai_huruf
						AND G .id_mhs = A .id_mhs
						AND A .status_hapus = 0
						AND H .id_program_studi = G .id_program_studi
						AND status_akademik_mhs = 4
						AND D .status_mkta = 1
						AND A .flagnilai = 1
						GROUP BY
							A .id_mhs,
							G .nim_mhs
					) x ON x.id_mhs = MAHASISWA.ID_MHS
					AND lama_skripsi > 3
					AND lama_skripsi <= 4
					AND PROGRAM_STUDI.ID_JENJANG = 3
					WHERE
						TO_CHAR (TGL_LULUS, 'YYYY') >= '2007'
					GROUP BY
						THN_AKADEMIK_SEMESTER || (
							CASE
							WHEN NM_SEMESTER = 'Ganjil' THEN
								1
							ELSE
								2
							END
						),
						TO_CHAR (tgl_lulus, 'yyyy'),
						PROGRAM_STUDI.ID_PROGRAM_STUDI,
						PROGRAM_STUDI.ID_FAKULTAS,
						PROGRAM_STUDI.ID_JENJANG,
						MAHASISWA.STATUS
		) C ON C.ID_PROGRAM_STUDI = A .ID_PROGRAM_STUDI
		AND C.THN_SEMESTER = A .THN_SEMESTER
		AND C.TAHUN_LULUS = A .TAHUN_LULUS
		AND C.STATUS = A .STATUS
		LEFT JOIN (
			SELECT
				THN_AKADEMIK_SEMESTER || (
					CASE
					WHEN NM_SEMESTER = 'Ganjil' THEN
						1
					ELSE
						2
					END
				) AS THN_SEMESTER,
				TO_CHAR (tgl_lulus, 'yyyy') AS tahun_lulus,
				PROGRAM_STUDI.ID_PROGRAM_STUDI,
				PROGRAM_STUDI.ID_FAKULTAS,
				PROGRAM_STUDI.ID_JENJANG,
				MAHASISWA.STATUS,
				COUNT (*) AS LAMA_SKRIPSI3_JML
				FROM
					AUCC.mahasiswa@AUCCRO
				JOIN AUCC.pengguna@AUCCRO ON pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
				JOIN AUCC.admisi@AUCCRO ON admisi.id_mhs = mahasiswa.id_mhs
				AND status_akd_mhs = 4
				AND status_apv = 1
				AND tgl_lulus IS NOT NULL
				JOIN AUCC.program_studi@AUCCRO ON program_studi.id_program_studi = mahasiswa.id_program_studi
				JOIN AUCC.semester@AUCCRO ON SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
			JOIN (
				SELECT
					A .id_mhs,
					G .nim_mhs,
					COUNT (*) AS lama_skripsi
				FROM
					AUCC.pengambilan_mk@AUCCRO A,
					AUCC.mata_kuliah@AUCCRO c,
					AUCC.kurikulum_mk@AUCCRO D,
					AUCC.semester@AUCCRO E,
					AUCC.standar_nilai@AUCCRO f,
					AUCC.mahasiswa@AUCCRO G,
					AUCC.program_studi@AUCCRO H
				WHERE
					A .id_kurikulum_mk = D .id_kurikulum_mk
				AND D .id_mata_kuliah = c.id_mata_kuliah
				AND A .id_semester = E .id_semester
				AND A .STATUS_APV_PENGAMBILAN_MK = '1'
				AND nm_standar_nilai = nilai_huruf
				AND G .id_mhs = A .id_mhs
				AND A .status_hapus = 0
				AND H .id_program_studi = G .id_program_studi
				AND status_akademik_mhs = 4
				AND D .status_mkta = 1
				AND A .flagnilai = 1
				GROUP BY
					A .id_mhs,
					G .nim_mhs
			) x ON x.id_mhs = MAHASISWA.ID_MHS
			AND lama_skripsi > 2
			AND PROGRAM_STUDI.ID_JENJANG IN (1, 5)
			WHERE
				TO_CHAR (TGL_LULUS, 'YYYY') >= '2007'
			GROUP BY
				THN_AKADEMIK_SEMESTER || (
					CASE
					WHEN NM_SEMESTER = 'Ganjil' THEN
						1
					ELSE
						2
					END
				),
				TO_CHAR (tgl_lulus, 'yyyy'),
				PROGRAM_STUDI.ID_PROGRAM_STUDI,
				PROGRAM_STUDI.ID_FAKULTAS,
				PROGRAM_STUDI.ID_JENJANG,
				MAHASISWA.STATUS
			UNION
				SELECT
					THN_AKADEMIK_SEMESTER || (
						CASE
						WHEN NM_SEMESTER = 'Ganjil' THEN
							1
						ELSE
							2
						END
					) AS THN_SEMESTER,
					TO_CHAR (tgl_lulus, 'yyyy') AS tahun_lulus,
					PROGRAM_STUDI.ID_PROGRAM_STUDI,
					PROGRAM_STUDI.ID_FAKULTAS,
					PROGRAM_STUDI.ID_JENJANG,
					MAHASISWA.STATUS,
					COUNT (*) AS LAMA_SKRIPSI3_JML
				FROM
					AUCC.mahasiswa@AUCCRO
				JOIN AUCC.pengguna@AUCCRO ON pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
				JOIN AUCC.admisi@AUCCRO ON admisi.id_mhs = mahasiswa.id_mhs
				AND status_akd_mhs = 4
				AND status_apv = 1
				AND tgl_lulus IS NOT NULL
				JOIN AUCC.program_studi@AUCCRO ON program_studi.id_program_studi = mahasiswa.id_program_studi
				JOIN AUCC.semester@AUCCRO ON SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
				JOIN (
					SELECT
						A .id_mhs,
						G .nim_mhs,
						COUNT (*) AS lama_skripsi
					FROM
						AUCC.pengambilan_mk@AUCCRO A,
						AUCC.mata_kuliah@AUCCRO c,
						AUCC.kurikulum_mk@AUCCRO D,
						AUCC.semester@AUCCRO E,
						AUCC.standar_nilai@AUCCRO f,
						AUCC.mahasiswa@AUCCRO G,
						AUCC.program_studi@AUCCRO H
					WHERE
						A .id_kurikulum_mk = D .id_kurikulum_mk
					AND D .id_mata_kuliah = c.id_mata_kuliah
					AND A .id_semester = E .id_semester
					AND A .STATUS_APV_PENGAMBILAN_MK = '1'
					AND nm_standar_nilai = nilai_huruf
					AND G .id_mhs = A .id_mhs
					AND A .status_hapus = 0
					AND H .id_program_studi = G .id_program_studi
					AND status_akademik_mhs = 4
					AND D .status_mkta = 1
					AND A .flagnilai = 1
					GROUP BY
						A .id_mhs,
						G .nim_mhs
				) x ON x.id_mhs = MAHASISWA.ID_MHS
				AND lama_skripsi > 3
				AND PROGRAM_STUDI.ID_JENJANG = 2
				WHERE
					TO_CHAR (TGL_LULUS, 'YYYY') >= '2007'
				GROUP BY
					THN_AKADEMIK_SEMESTER || (
						CASE
						WHEN NM_SEMESTER = 'Ganjil' THEN
							1
						ELSE
							2
						END
					),
					TO_CHAR (tgl_lulus, 'yyyy'),
					PROGRAM_STUDI.ID_PROGRAM_STUDI,
					PROGRAM_STUDI.ID_FAKULTAS,
					PROGRAM_STUDI.ID_JENJANG,
					MAHASISWA.STATUS
				UNION
					SELECT
						THN_AKADEMIK_SEMESTER || (
							CASE
							WHEN NM_SEMESTER = 'Ganjil' THEN
								1
							ELSE
								2
							END
						) AS THN_SEMESTER,
						TO_CHAR (tgl_lulus, 'yyyy') AS tahun_lulus,
						PROGRAM_STUDI.ID_PROGRAM_STUDI,
						PROGRAM_STUDI.ID_FAKULTAS,
						PROGRAM_STUDI.ID_JENJANG,
						MAHASISWA.STATUS,
						COUNT (*) AS LAMA_SKRIPSI3_JML
					FROM
					AUCC.mahasiswa@AUCCRO
					JOIN AUCC.pengguna@AUCCRO ON pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
					JOIN AUCC.admisi@AUCCRO ON admisi.id_mhs = mahasiswa.id_mhs
					AND status_akd_mhs = 4
					AND status_apv = 1
					AND tgl_lulus IS NOT NULL
					JOIN AUCC.program_studi@AUCCRO ON program_studi.id_program_studi = mahasiswa.id_program_studi
					JOIN AUCC.semester@AUCCRO ON SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
					JOIN (
						SELECT
							A .id_mhs,
							G .nim_mhs,
							COUNT (*) AS lama_skripsi
						FROM
							AUCC.pengambilan_mk@AUCCRO A,
							AUCC.mata_kuliah@AUCCRO c,
							AUCC.kurikulum_mk@AUCCRO D,
							AUCC.semester@AUCCRO E,
							AUCC.standar_nilai@AUCCRO f,
							AUCC.mahasiswa@AUCCRO G,
							AUCC.program_studi@AUCCRO H
						WHERE
							A .id_kurikulum_mk = D .id_kurikulum_mk
						AND D .id_mata_kuliah = c.id_mata_kuliah
						AND A .id_semester = E .id_semester
						AND A .STATUS_APV_PENGAMBILAN_MK = '1'
						AND nm_standar_nilai = nilai_huruf
						AND G .id_mhs = A .id_mhs
						AND A .status_hapus = 0
						AND H .id_program_studi = G .id_program_studi
						AND status_akademik_mhs = 4
						AND D .status_mkta = 1
						AND A .flagnilai = 1
						GROUP BY
							A .id_mhs,
							G .nim_mhs
					) x ON x.id_mhs = MAHASISWA.ID_MHS
					AND lama_skripsi > 4
					AND PROGRAM_STUDI.ID_JENJANG = 3
					WHERE
						TO_CHAR (TGL_LULUS, 'YYYY') >= '2007'
					GROUP BY
						THN_AKADEMIK_SEMESTER || (
							CASE
							WHEN NM_SEMESTER = 'Ganjil' THEN
								1
							ELSE
								2
							END
						),
						TO_CHAR (tgl_lulus, 'yyyy'),
						PROGRAM_STUDI.ID_PROGRAM_STUDI,
						PROGRAM_STUDI.ID_FAKULTAS,
						PROGRAM_STUDI.ID_JENJANG,
						MAHASISWA.STATUS
		) D ON D .ID_PROGRAM_STUDI = A .ID_PROGRAM_STUDI
		AND D .THN_SEMESTER = A .THN_SEMESTER
		AND D .TAHUN_LULUS = A .TAHUN_LULUS
		AND D .STATUS = A .STATUS
	)
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

//WH_SE_LULUSAN_ELPT 3.7
$db_wh->Query("
	INSERT INTO WH_SE_LULUSAN_ELPT (
	THN_AKADEMIK_SMT, 
	THN_AKADEMIK_LULUS,
	ID_PROGRAM_STUDI,
	ID_FAKULTAS,
	ID_JENJANG,
	STATUS_MHS,
	KELAMIN_PENGGUNA,
	NIM_MHS,
	ELPT
	) 
SELECT
	THN_AKADEMIK_SEMESTER || (
		CASE
		WHEN NM_SEMESTER = 'Ganjil' THEN
			1
		ELSE
			2
		END
	) AS THN_AKADEMIK_SMT,
	TO_CHAR (tgl_lulus, 'yyyy') AS THN_AKADEMIK_LULUS,
	PROGRAM_STUDI.ID_PROGRAM_STUDI,
	PROGRAM_STUDI.ID_FAKULTAS,
	PROGRAM_STUDI.ID_JENJANG,
	MAHASISWA.STATUS AS STATUS_MHS,
	KELAMIN_PENGGUNA,
	NIM_MHS,
	pengajuan_wisuda.ELPT
FROM
	AUCC.mahasiswa@AUCCRO
JOIN AUCC.pengguna@AUCCRO ON pengguna.id_pengguna = MAHASISWA.ID_PENGGUNA
JOIN AUCC.admisi@AUCCRO ON admisi.id_mhs = mahasiswa.id_mhs
AND status_akd_mhs = 4
AND status_apv = 1
JOIN AUCC.program_studi@AUCCRO ON program_studi.id_program_studi = mahasiswa.id_program_studi
JOIN AUCC.semester@AUCCRO ON SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
LEFT JOIN AUCC.pengajuan_wisuda@AUCCRO ON pengajuan_wisuda.id_mhs = mahasiswa.id_mhs
WHERE
	tgl_lulus IS NOT NULL
AND TO_CHAR (TGL_LULUS, 'YYYY') >= '2007'
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

//5.1

$mhs_51 = $db->QueryToArray("select pem.tahun as THN_AKADEMIK_SMT, pem.id_program_studi, pem.id_fakultas, pem.id_jenjang, pem.id_jalur ,pem.peminat, dit.diterima, round(dit.diterima/pem.peminat*100, 2) as keketatan, daftar_ulang as MENDAFTAR_KEMBALI, SYSDATE AS TGL_UPDATE, rata_nilai, nilai_pmdk
from (
		select count(*) as peminat, tahun, id_program_studi, id_fakultas, id_jenjang, id_jalur 
		from (
		select thn_akademik_semester as tahun, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur, 
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_1
		union all 
		select thn_akademik_semester as tahun, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur, 
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_2
		union all 
		select thn_akademik_semester as tahun, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur, 
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_3
		union all 
		select thn_akademik_semester as tahun, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur, 
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_4
		)
		group by tahun, id_program_studi, id_fakultas, id_jenjang, id_jalur
) pem 
left join (
		select count(cmhs.id_program_studi) as diterima, semester.thn_akademik_semester as tahun, cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur
		from calon_mahasiswa_baru cmhs
		join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
		join semester on semester.id_semester = pen.id_semester
		join program_studi p on p.id_program_studi = cmhs.id_program_studi								
		group by thn_akademik_semester, cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur
) dit on pem.tahun = dit.tahun and pem.id_program_studi = dit.id_program_studi and pem.id_fakultas = dit.id_fakultas and pem.id_jenjang = dit.id_jenjang and pem.id_jalur = dit.id_jalur
left join (
	select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun , cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur,  round(avg(mahasiswa.nilai_skhun_mhs), 2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
	from calon_mahasiswa_baru cmhs
	join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
	join semester on semester.id_semester = pen.id_semester		
	join program_studi p on p.id_program_studi = cmhs.id_program_studi
	left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
	where p.id_jenjang in (1, 5) and mahasiswa.status IN ('R', 'I') and cmhs.nim_mhs is not null
	group by thn_akademik_semester, cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur
	
	union
	
	select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun , cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur,  round(avg(cms.ip_s1), 2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
	from calon_mahasiswa_baru cmhs
	join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
	join semester on semester.id_semester = pen.id_semester		
	join program_studi p on p.id_program_studi = cmhs.id_program_studi
	left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
	where ((p.id_jenjang in (2, 9) and mahasiswa.status = 'R') or (p.id_jenjang in (1) and mahasiswa.status = 'AJ'))  and cmhs.nim_mhs is not null
	group by thn_akademik_semester, cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur
	
	union
	
	select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun , cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur,  round(avg(cms.ip_s2), 2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
	from calon_mahasiswa_baru cmhs
	join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
	join semester on semester.id_semester = pen.id_semester		
	join program_studi p on p.id_program_studi = cmhs.id_program_studi
	left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
	where (p.id_jenjang in (3) and mahasiswa.status = 'R') and cmhs.nim_mhs is not null
	group by thn_akademik_semester, cmhs.id_program_studi, p.id_fakultas, p.id_jenjang, pen.id_jalur
) daf on pem.tahun = daf.tahun and pem.id_program_studi = daf.id_program_studi and pem.id_fakultas = daf.id_fakultas and pem.id_jenjang = daf.id_jenjang and pem.id_jalur = daf.id_jalur
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

foreach($mhs_51 as $data){

	$db_wh->Query("INSERT INTO WH_SE_MHS_THN_MASUK (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, PEMINAT, DITERIMA, KEKETATAN, MENDAFTAR_KEMBALI, ID_JALUR, TGL_UPDATE, RATA_UN, RATA)
	VALUES
	('$data[THN_AKADEMIK_SMT]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[PEMINAT]', '$data[DITERIMA]', '$data[KEKETATAN]', '$data[MENDAFTAR_KEMBALI]', '$data[ID_JALUR]', '$data[TGL_UPDATE]', '$data[RATA_NILAI]', '$data[NILAI_PMDK]')");

	if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

}



$mhs_51_2 = $db->QueryToArray("select pem.tahun as THN_AKADEMIK_SMT, pem.id_fakultas, pem.id_jalur ,pem.peminat, dit.diterima, round(dit.diterima/pem.peminat*100, 2) as keketatan,
daftar_ulang as MENDAFTAR_KEMBALI, round(rata_nilai, 2) as NILAI_RATA, pem.id_jenjang, SYSDATE AS TGL_UPDATE , nilai_pmdk
from (
								select count(*) as peminat, tahun, id_fakultas, id_jalur, id_jenjang
								from (
								select thn_akademik_semester as tahun, p.id_fakultas, cmhs.id_jalur, p.id_jenjang, 
								cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
																	from calon_mahasiswa_baru cmhs
																	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
																	join semester on semester.id_semester = pen.id_semester										
																	join program_studi p on p.id_program_studi = cmhs.id_pilihan_1
								union 
								select thn_akademik_semester as tahun, p.id_fakultas, cmhs.id_jalur, p.id_jenjang,
								cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
																	from calon_mahasiswa_baru cmhs
																	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
																	join semester on semester.id_semester = pen.id_semester										
																	join program_studi p on p.id_program_studi = cmhs.id_pilihan_2
								union 
								select thn_akademik_semester as tahun, p.id_fakultas, cmhs.id_jalur, p.id_jenjang,
								cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
																	from calon_mahasiswa_baru cmhs
																	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
																	join semester on semester.id_semester = pen.id_semester										
																	join program_studi p on p.id_program_studi = cmhs.id_pilihan_3
								union 
								select thn_akademik_semester as tahun, p.id_fakultas, cmhs.id_jalur, p.id_jenjang,
								cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, cmhs.id_c_mhs
																	from calon_mahasiswa_baru cmhs
																	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
																	join semester on semester.id_semester = pen.id_semester										
																	join program_studi p on p.id_program_studi = cmhs.id_pilihan_4
								)
								group by tahun, id_fakultas, id_jalur, id_jenjang
						) pem 
						left join (
								select count(cmhs.id_program_studi) as diterima, semester.thn_akademik_semester as tahun, p.id_fakultas,pen.id_jalur, p.id_jenjang
								from calon_mahasiswa_baru cmhs
								join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
								join semester on semester.id_semester = pen.id_semester
								join program_studi p on p.id_program_studi = cmhs.id_program_studi	
								group by thn_akademik_semester,  p.id_fakultas, pen.id_jalur, p.id_jenjang
						) dit on pem.tahun = dit.tahun and pem.id_fakultas = dit.id_fakultas and pem.id_jalur = dit.id_jalur and pem.id_jenjang = dit.id_jenjang
						left join (
							select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun , p.id_fakultas, p.id_jenjang, pen.id_jalur, avg(mahasiswa.nilai_skhun_mhs) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
							from calon_mahasiswa_baru cmhs
							join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
							join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
							join semester on semester.id_semester = pen.id_semester		
							join program_studi p on p.id_program_studi = cmhs.id_program_studi
							left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
							where p.id_jenjang in (1, 5) and mahasiswa.status IN ('R', 'I') and cmhs.nim_mhs is not null
							group by thn_akademik_semester, p.id_fakultas, pen.id_jalur, p.id_jenjang

							union 

							select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun , p.id_fakultas, p.id_jenjang, pen.id_jalur, avg(cms.ip_s1) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
							from calon_mahasiswa_baru cmhs
							join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
							join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
							join semester on semester.id_semester = pen.id_semester		
							join program_studi p on p.id_program_studi = cmhs.id_program_studi
							left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
							where ((p.id_jenjang in (2, 9) and mahasiswa.status = 'R') or (p.id_jenjang in (1) and mahasiswa.status = 'AJ'))  and cmhs.nim_mhs is not null
							group by thn_akademik_semester, p.id_fakultas, pen.id_jalur, p.id_jenjang

							union
							
							select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun , p.id_fakultas, p.id_jenjang, pen.id_jalur, avg(cms.ip_s2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
							from calon_mahasiswa_baru cmhs
							join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
							join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
							join semester on semester.id_semester = pen.id_semester		
							join program_studi p on p.id_program_studi = cmhs.id_program_studi
							left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
							where (p.id_jenjang in (3) and mahasiswa.status = 'R') and cmhs.nim_mhs is not null
							group by thn_akademik_semester, p.id_fakultas, pen.id_jalur, p.id_jenjang
						) daf on pem.tahun = daf.tahun  and pem.id_fakultas = daf.id_fakultas  and pem.id_jalur = daf.id_jalur and pem.id_jenjang = daf.id_jenjang

"); 

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

foreach($mhs_51_2 as $data){

	$db_wh->Query("INSERT INTO WH_SE_MHS_THN_MASUK_FAK (THN_AKADEMIK_SMT, ID_FAKULTAS, PEMINAT, DITERIMA, KEKETATAN, MENDAFTAR_KEMBALI, ID_JALUR, ID_JENJANG, TGL_UPDATE, RATA_UN, RATA)
	VALUES
	('$data[THN_AKADEMIK_SMT]', '$data[ID_FAKULTAS]','$data[PEMINAT]', '$data[DITERIMA]', '$data[KEKETATAN]', '$data[MENDAFTAR_KEMBALI]', '$data[ID_JALUR]', '$data[ID_JENJANG]', '$data[TGL_UPDATE]', '$data[NILAI_RATA]', '$data[NILAI_PMDK]')");

	if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }
}




$mhs_51_3 = $db->QueryToArray("select pem.tahun as THN_AKADEMIK_SMT, pem.id_jalur ,pem.peminat, dit.diterima, round(dit.diterima/pem.peminat*100, 2) as keketatan, 
daftar_ulang as MENDAFTAR_KEMBALI, SYSDATE AS TGL_UPDATE, pem.id_jenjang, rata_nilai, nilai_pmdk
from (
								select count(*) as peminat, tahun, id_jalur , id_jenjang
								from (
								select thn_akademik_semester as tahun, cmhs.id_jalur, 
								cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, p.id_jenjang
																	from calon_mahasiswa_baru cmhs
																	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
																	join semester on semester.id_semester = pen.id_semester										
																	join program_studi p on p.id_program_studi = cmhs.id_pilihan_1								
								)
								group by tahun, id_jalur, id_jenjang
						) pem 
						left join (
								select count(cmhs.id_program_studi) as diterima, semester.thn_akademik_semester as tahun,pen.id_jalur, p.id_jenjang
								from calon_mahasiswa_baru cmhs
								join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
								join semester on semester.id_semester = pen.id_semester
								join program_studi p on p.id_program_studi = cmhs.id_program_studi								
								group by thn_akademik_semester, pen.id_jalur, p.id_jenjang
						) dit on pem.tahun = dit.tahun and pem.id_jalur = dit.id_jalur and pem.id_jenjang = dit.id_jenjang
						left join (
							
select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun, pen.id_jalur, p.id_jenjang, round(avg(mahasiswa.nilai_skhun_mhs), 2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
							from calon_mahasiswa_baru cmhs
							join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
							join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
							join semester on semester.id_semester = pen.id_semester		
							join program_studi p on p.id_program_studi = cmhs.id_program_studi
							left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
							where p.id_jenjang in (1, 5) and mahasiswa.status IN ('R', 'I') and cmhs.nim_mhs is not null
							group by thn_akademik_semester, pen.id_jalur, p.id_jenjang

union 

							select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun, pen.id_jalur, p.id_jenjang, round(avg(cms.ip_s1), 2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
							from calon_mahasiswa_baru cmhs
							join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
							join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
							join semester on semester.id_semester = pen.id_semester		
							join program_studi p on p.id_program_studi = cmhs.id_program_studi
							left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
							where ((p.id_jenjang in (2, 9) and mahasiswa.status = 'R') or (p.id_jenjang in (1) and mahasiswa.status = 'AJ')) and cmhs.nim_mhs is not null
							group by thn_akademik_semester, pen.id_jalur, p.id_jenjang
							
							union 

							select count(cmhs.id_c_mhs) as daftar_ulang, thn_akademik_semester as tahun, pen.id_jalur, p.id_jenjang, round(avg(cms.ip_s2), 2) as rata_nilai, round(avg(coalesce((n.nilai_prestasi + n.nilai_tpa), np.total_nilai)) , 2) as nilai_pmdk
							from calon_mahasiswa_baru cmhs
							join mahasiswa on mahasiswa.id_c_mhs = cmhs.id_c_mhs
							join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
							join semester on semester.id_semester = pen.id_semester		
							join program_studi p on p.id_program_studi = cmhs.id_program_studi
							left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
  left join nilai_cmhs n on n.id_c_mhs = cmhs.id_c_mhs and n.jurusan_sekolah = p.jurusan_sekolah
	left join nilai_cmhs_pasca np on np.id_c_mhs = cmhs.id_c_mhs
							where (p.id_jenjang in (3) and mahasiswa.status = 'R') and cmhs.nim_mhs is not null
							group by thn_akademik_semester, pen.id_jalur, p.id_jenjang
						) daf on pem.tahun = daf.tahun and pem.id_jalur = daf.id_jalur and pem.id_jenjang = daf.id_jenjang
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

foreach($mhs_51_3 as $data){

	$db_wh->Query("INSERT INTO WH_SE_MHS_THN_MASUK_UNIV (THN_AKADEMIK_SMT, PEMINAT, DITERIMA, KEKETATAN, MENDAFTAR_KEMBALI, ID_JALUR, ID_JENJANG, TGL_UPDATE, RATA_UN, RATA)
	VALUES
	('$data[THN_AKADEMIK_SMT]', '$data[PEMINAT]', '$data[DITERIMA]', '$data[KEKETATAN]', '$data[MENDAFTAR_KEMBALI]', '$data[ID_JALUR]', '$data[ID_JENJANG]', '$data[TGL_UPDATE]', '$data[RATA_NILAI]', '$data[NILAI_PMDK]')");

	if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }
}


//5.2

$mhs_52 = $db->QueryToArray("
select count(*) as THN_ANGKATAN_JML, pro.nm_provinsi as NM_PROVINSI, cmhs.id_jalur, ps.id_program_studi, ps.id_fakultas, ps.id_jenjang, 
thn_akademik_semester AS THN_AKADEMIK_SMT, thn_angkatan_mhs AS THN_ANGKATAN, mhs.status AS STATUS_MHS, sysdate as tgl_update
from calon_mahasiswa_baru cmhs
join program_studi ps on ps.id_program_studi = cmhs.id_program_studi
join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan  
join semester sm on sm.id_semester = pen.id_semester
join mahasiswa mhs on mhs.id_c_mhs = cmhs.id_c_mhs
left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
left join sekolah skl on skl.id_sekolah = cms.id_sekolah_asal
left join kota kt on kt.id_kota = skl.id_kota
left join provinsi pro on pro.id_provinsi = kt.id_provinsi
group by pro.nm_provinsi, cmhs.id_jalur, ps.id_program_studi, ps.id_fakultas, ps.id_jenjang, thn_akademik_semester, thn_angkatan_mhs, mhs.status
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

foreach($mhs_52 as $data){

	$db_wh->Query("INSERT INTO WH_SE_MHS_PROVINSI (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, THN_ANGKATAN, THN_ANGKATAN_JML, STATUS_MHS, ID_JALUR, TGL_UPDATE, NM_PROVINSI)
	VALUES
	('$data[THN_AKADEMIK_SMT]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[THN_ANGKATAN]', '$data[THN_ANGKATAN_JML]', '$data[STATUS_MHS]', '$data[ID_JALUR]', '$data[TGL_UPDATE]', '$data[NM_PROVINSI]')");

	if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

}



$mhs_52_2 = $db->QueryToArray("
select count(*) as THN_ANGKATAN_JML, cms.PEKERJAAN, cmhs.id_jalur, ps.id_program_studi, ps.id_fakultas, ps.id_jenjang, 
thn_akademik_semester AS THN_AKADEMIK_SMT, thn_angkatan_mhs AS THN_ANGKATAN, mhs.status AS STATUS_MHS, sysdate as tgl_update
from calon_mahasiswa_baru cmhs
join program_studi ps on ps.id_program_studi = cmhs.id_program_studi
join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan  
join semester sm on sm.id_semester = pen.id_semester
join mahasiswa mhs on mhs.id_c_mhs = cmhs.id_c_mhs
left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
group by cms.PEKERJAAN, cmhs.id_jalur, ps.id_program_studi, ps.id_fakultas, ps.id_jenjang, thn_akademik_semester, thn_angkatan_mhs, mhs.status
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

foreach($mhs_52_2 as $data){

	$db_wh->Query("INSERT INTO WH_SE_MHS_INSTITUSI (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, THN_ANGKATAN, THN_ANGKATAN_JML, STATUS_MHS, ID_JALUR, TGL_UPDATE, NM_INSTITUSI)
	VALUES
	('$data[THN_AKADEMIK_SMT]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[THN_ANGKATAN]', '$data[THN_ANGKATAN_JML]', '$data[STATUS_MHS]', '$data[ID_JALUR]', '$data[TGL_UPDATE]', '$data[PEKERJAAN]')");

	if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }
}



//5.3
$mhs_53 = $db->QueryToArray("
select pro.nm_provinsi as NM_PROVINSI, kt.nm_kota as nm_kota, cmhs.id_jalur, ps.id_program_studi, ps.id_fakultas, ps.id_jenjang, 
thn_akademik_semester AS THN_AKADEMIK_SMT, thn_angkatan_mhs AS THN_ANGKATAN, mhs.status AS STATUS_MHS, sysdate as tgl_update, MHS.nim_mhs,
coalesce(total_pendapatan_ortu, penghasilan_ortu_mhs, 0) as PENGHASILAN_ORTU
from calon_mahasiswa_baru cmhs
join program_studi ps on ps.id_program_studi = cmhs.id_program_studi
join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan  
join semester sm on sm.id_semester = pen.id_semester
join mahasiswa mhs on mhs.id_c_mhs = cmhs.id_c_mhs
left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
left join sekolah skl on skl.id_sekolah = cms.id_sekolah_asal
left join kota kt on kt.id_kota = skl.id_kota
left join provinsi pro on pro.id_provinsi = kt.id_provinsi
join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmhs.id_c_mhs
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

foreach($mhs_53 as $data){

	$db_wh->Query("INSERT INTO WH_SE_MHS_ASAL (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, THN_ANGKATAN, PENGHASILAN_ORTU, STATUS_MHS, ID_JALUR, TGL_UPDATE, NM_PROVINSI, NM_KOTA, NIM_MHS)
	VALUES
	('$data[THN_AKADEMIK_SMT]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[THN_ANGKATAN]', '$data[PENGHASILAN_ORTU]', '$data[STATUS_MHS]', '$data[ID_JALUR]', '$data[TGL_UPDATE]', '$data[NM_PROVINSI]', '$data[NM_KOTA]', '$data[NIM_MHS]')");

	if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }
}


$mhs_53_2 = $db->QueryToArray("
select count(*) as THN_ANGKATAN_JML, cms.PTN_S1 as NM_UNIVERSITAS, cmhs.id_jalur, ps.id_program_studi, ps.id_fakultas, ps.id_jenjang, 
thn_akademik_semester AS THN_AKADEMIK_SMT, thn_angkatan_mhs AS THN_ANGKATAN, mhs.status AS STATUS_MHS, sysdate as tgl_update
from calon_mahasiswa_baru cmhs
join program_studi ps on ps.id_program_studi = cmhs.id_program_studi
join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan  
join semester sm on sm.id_semester = pen.id_semester
join mahasiswa mhs on mhs.id_c_mhs = cmhs.id_c_mhs
left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
group by cms.PTN_S1, cmhs.id_jalur, ps.id_program_studi, ps.id_fakultas, ps.id_jenjang, thn_akademik_semester, thn_angkatan_mhs, mhs.status

UNION

select count(*) as THN_ANGKATAN_JML, cms.PTN_S2 as NM_UNIVERSITAS, cmhs.id_jalur, ps.id_program_studi, ps.id_fakultas, ps.id_jenjang, 
thn_akademik_semester AS THN_AKADEMIK_SMT, thn_angkatan_mhs AS THN_ANGKATAN, mhs.status AS STATUS_MHS, sysdate as tgl_update
from calon_mahasiswa_baru cmhs
join program_studi ps on ps.id_program_studi = cmhs.id_program_studi
join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan  
join semester sm on sm.id_semester = pen.id_semester
join mahasiswa mhs on mhs.id_c_mhs = cmhs.id_c_mhs
left join calon_mahasiswa_pasca cms on cms.id_c_mhs = cmhs.id_c_mhs
group by cms.PTN_S2, cmhs.id_jalur, ps.id_program_studi, ps.id_fakultas, ps.id_jenjang, thn_akademik_semester, thn_angkatan_mhs, mhs.status
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

foreach($mhs_53_2 as $data){

	$db_wh->Query("INSERT INTO WH_SE_MHS_UNIVERSITAS (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, THN_ANGKATAN, THN_ANGKATAN_JML, STATUS_MHS, ID_JALUR, TGL_UPDATE, NM_UNIVERSITAS)
	VALUES
	('$data[THN_AKADEMIK_SMT]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[THN_ANGKATAN]', '$data[THN_ANGKATAN_JML]', '$data[STATUS_MHS]', '$data[ID_JALUR]', '$data[TGL_UPDATE]', '$data[NM_UNIVERSITAS]')");

	if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }
}

//5.4
$mhs_54 = $db->QueryToArray("
 select a.*, (case when pilihan_2 is null then 0 else pilihan_2 end) as pilihan_2, (case when pilihan_3 is null then 0 else pilihan_3 end) as pilihan_3 from (
select count(*) as pilihan_1, thn_akademik_semester, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur
					from calon_mahasiswa_baru cmhs
				  join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan 
					join semester s on s.id_semester = pen.id_semester
				  join mahasiswa mhs on mhs.id_c_mhs = cmhs.id_c_mhs
					join program_studi p on p.id_program_studi = cmhs.id_program_studi
				  where cmhs.id_pilihan_1 is not null  
					group by thn_akademik_semester, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur				  
) a
left join (
 select count(*) as pilihan_2, thn_akademik_semester, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur
					from calon_mahasiswa_baru cmhs
				  join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan 
					join semester s on s.id_semester = pen.id_semester
				  join mahasiswa mhs on mhs.id_c_mhs = cmhs.id_c_mhs
					join program_studi p on p.id_program_studi = cmhs.id_program_studi
				  where cmhs.id_pilihan_2 is not null  
					group by thn_akademik_semester, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur				 
) b on a.thn_akademik_semester = b.thn_akademik_semester and a.id_program_studi = b.id_program_studi and a.id_fakultas = b.id_fakultas and a.id_jenjang = b.id_jenjang and a.id_jalur = b.id_jalur
left join (
 select count(*) as pilihan_3, thn_akademik_semester, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur
					from calon_mahasiswa_baru cmhs
				  join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan 
					join semester s on s.id_semester = pen.id_semester
				  join mahasiswa mhs on mhs.id_c_mhs = cmhs.id_c_mhs
					join program_studi p on p.id_program_studi = cmhs.id_program_studi
				  where cmhs.id_pilihan_3 is not null  
					group by thn_akademik_semester, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur				 
) c on a.thn_akademik_semester = c.thn_akademik_semester and a.id_program_studi = c.id_program_studi and a.id_fakultas = c.id_fakultas and a.id_jenjang = c.id_jenjang and a.id_jalur = c.id_jalur
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

foreach($mhs_54 as $data){

	$db_wh->Query("INSERT INTO WH_SE_MHS_PILIHAN (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, ID_JALUR, TGL_UPDATE, PILIHAN_PERTAMA, PILIHAN_KEDUA, PILIHAN_KETIGA)
	VALUES
	('$data[THN_AKADEMIK_SEMESTER]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[ID_JALUR]', '$data[TGL_UPDATE]', '$data[PILIHAN_1]','$data[PILIHAN_2]','$data[PILIHAN_3]')");

	if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }
}

$mhs_54_2 = $db->QueryToArray("
select count(*) as THN_ANGKATAN_JML,  NM_KOTA, cmhs.id_jalur, ps.id_program_studi, ps.id_fakultas, ps.id_jenjang, 
thn_akademik_semester AS THN_AKADEMIK_SMT, thn_angkatan_mhs AS THN_ANGKATAN, mhs.status AS STATUS_MHS, sysdate as tgl_update
from calon_mahasiswa_baru cmhs
join program_studi ps on ps.id_program_studi = cmhs.id_program_studi
join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan  
join semester sm on sm.id_semester = pen.id_semester
join mahasiswa mhs on mhs.id_c_mhs = cmhs.id_c_mhs
join kota on KOTA.id_kota = cmhs.id_kota
group by NM_KOTA, cmhs.id_jalur, ps.id_program_studi, ps.id_fakultas, ps.id_jenjang, thn_akademik_semester, thn_angkatan_mhs, mhs.status

");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

foreach($mhs_54_2 as $data){

	$db_wh->Query("INSERT INTO WH_SE_MHS_DOMISILI (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, THN_ANGKATAN, THN_ANGKATAN_JML, STATUS_MHS, ID_JALUR, TGL_UPDATE, NM_DOMISILI)
	VALUES
	('$data[THN_AKADEMIK_SMT]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[THN_ANGKATAN]', '$data[THN_ANGKATAN_JML]', '$data[STATUS_MHS]', '$data[ID_JALUR]', '$data[TGL_UPDATE]', '$data[NM_KOTA]')");

	if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }
}



//5.5
$mhs_55 = $db->QueryToArray("select count(*) as THN_ANGKATAN_JML, tahun AS THN_AKADEMIK_SMT, id_program_studi, id_fakultas, id_jenjang, id_jalur, nm_provinsi, SYSDATE AS TGL_UPDATE, NM_PENERIMAAN
		from (
		select thn_akademik_semester as tahun, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur, pro.nm_provinsi,
		coalesce(cmhs.id_pilihan_1, cmhs.id_program_studi) as id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, NM_PENERIMAAN
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											left join program_studi p on p.id_program_studi = cmhs.id_pilihan_1 or p.id_program_studi = cmhs.id_program_studi
											left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
										  left join sekolah skl on skl.id_sekolah = cms.id_sekolah_asal
										  left join kota kt on kt.id_kota = skl.id_kota
										  left join provinsi pro on pro.id_provinsi = kt.id_provinsi
		union all 
		select thn_akademik_semester as tahun, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur, pro.nm_provinsi,
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, NM_PENERIMAAN
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_2
											left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
										  left join sekolah skl on skl.id_sekolah = cms.id_sekolah_asal
										  left join kota kt on kt.id_kota = skl.id_kota
										  left join provinsi pro on pro.id_provinsi = kt.id_provinsi
		union all 
		select thn_akademik_semester as tahun, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur, pro.nm_provinsi,
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, NM_PENERIMAAN
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_3
											left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
										  left join sekolah skl on skl.id_sekolah = cms.id_sekolah_asal
										  left join kota kt on kt.id_kota = skl.id_kota
										  left join provinsi pro on pro.id_provinsi = kt.id_provinsi
		union all 
		select thn_akademik_semester as tahun, p.id_program_studi, p.id_fakultas, p.id_jenjang, cmhs.id_jalur, pro.nm_provinsi,
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, NM_PENERIMAAN
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_4
											left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
										  left join sekolah skl on skl.id_sekolah = cms.id_sekolah_asal
										  left join kota kt on kt.id_kota = skl.id_kota
										  left join provinsi pro on pro.id_provinsi = kt.id_provinsi
		)
		group by tahun, id_program_studi, id_fakultas, id_jenjang, id_jalur, nm_provinsi, NM_PENERIMAAN
");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

foreach($mhs_55 as $data){

	$db_wh->Query("INSERT INTO WH_SE_MHS_PROVINSI_PEMINAT (THN_AKADEMIK_SMT, ID_PROGRAM_STUDI, ID_FAKULTAS, ID_JENJANG, NM_PROVINSI, THN_ANGKATAN_JML, ID_JALUR, TGL_UPDATE, NM_PENERIMAAN)
	VALUES
	('$data[THN_AKADEMIK_SMT]', '$data[ID_PROGRAM_STUDI]', '$data[ID_FAKULTAS]', '$data[ID_JENJANG]', '$data[NM_PROVINSI]', '$data[THN_ANGKATAN_JML]', '$data[ID_JALUR]', '$data[TGL_UPDATE]', '$data[NM_PENERIMAAN]')");

	if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }
}


$mhs_55_2 = $db->QueryToArray("select count(*) as THN_ANGKATAN_JML, tahun AS THN_AKADEMIK_SMT, id_fakultas, id_jalur, nm_provinsi, SYSDATE AS TGL_UPDATE, NM_PENERIMAAN
		from (
		select thn_akademik_semester as tahun, p.id_fakultas, cmhs.id_jalur, pro.nm_provinsi,
		coalesce(cmhs.id_pilihan_1, cmhs.id_program_studi) as id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, NM_PENERIMAAN
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											left join program_studi p on p.id_program_studi = cmhs.id_pilihan_1 or p.id_program_studi = cmhs.id_program_studi
											left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
										  left join sekolah skl on skl.id_sekolah = cms.id_sekolah_asal
										  left join kota kt on kt.id_kota = skl.id_kota
										  left join provinsi pro on pro.id_provinsi = kt.id_provinsi
		union 
		select thn_akademik_semester as tahun, p.id_fakultas, cmhs.id_jalur, pro.nm_provinsi,
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, NM_PENERIMAAN
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_2
											left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
										  left join sekolah skl on skl.id_sekolah = cms.id_sekolah_asal
										  left join kota kt on kt.id_kota = skl.id_kota
										  left join provinsi pro on pro.id_provinsi = kt.id_provinsi
		union 
		select thn_akademik_semester as tahun, p.id_fakultas, cmhs.id_jalur, pro.nm_provinsi,
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, NM_PENERIMAAN
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_3
											left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
										  left join sekolah skl on skl.id_sekolah = cms.id_sekolah_asal
										  left join kota kt on kt.id_kota = skl.id_kota
										  left join provinsi pro on pro.id_provinsi = kt.id_provinsi
		union
		select thn_akademik_semester as tahun, p.id_fakultas, cmhs.id_jalur, pro.nm_provinsi,
		cmhs.id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, NM_PENERIMAAN
											from calon_mahasiswa_baru cmhs
											join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
											join semester on semester.id_semester = pen.id_semester										
											join program_studi p on p.id_program_studi = cmhs.id_pilihan_4
											left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
										  left join sekolah skl on skl.id_sekolah = cms.id_sekolah_asal
										  left join kota kt on kt.id_kota = skl.id_kota
										  left join provinsi pro on pro.id_provinsi = kt.id_provinsi
		)
		group by tahun, id_fakultas, id_jalur, nm_provinsi, NM_PENERIMAAN

");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }

foreach($mhs_55_2 as $data){

	$db_wh->Query("INSERT INTO WH_SE_MHS_PROVINSI_PEMINAT_FAK (THN_AKADEMIK_SMT, ID_FAKULTAS, NM_PROVINSI, THN_ANGKATAN_JML, ID_JALUR, TGL_UPDATE, NM_PENERIMAAN)
	VALUES
	('$data[THN_AKADEMIK_SMT]', '$data[ID_FAKULTAS]', '$data[NM_PROVINSI]', '$data[THN_ANGKATAN_JML]', '$data[ID_JALUR]', '$data[TGL_UPDATE]', '$data[NM_PENERIMAAN]')");

	if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }
}



$mhs_55_3 = $db->QueryToArray("select count(*) as THN_ANGKATAN_JML, tahun AS THN_AKADEMIK_SMT, id_jalur, SYSDATE AS TGL_UPDATE, nm_provinsi, NM_PENERIMAAN
								from (
								select thn_akademik_semester as tahun, cmhs.id_jalur, pro.nm_provinsi, 
								coalesce(cmhs.id_pilihan_1, cmhs.id_program_studi) as id_pilihan_1, cmhs.id_pilihan_2, cmhs.id_pilihan_3, cmhs.id_pilihan_4, NM_PENERIMAAN
																	from calon_mahasiswa_baru cmhs
																	join penerimaan pen on pen.id_penerimaan = cmhs.id_penerimaan
																	join semester on semester.id_semester = pen.id_semester										
																	left join program_studi p on p.id_program_studi = cmhs.id_pilihan_1		
																	left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmhs.id_c_mhs
																	left join sekolah skl on skl.id_sekolah = cms.id_sekolah_asal
																  left join kota kt on kt.id_kota = skl.id_kota
																  left join provinsi pro on pro.id_provinsi = kt.id_provinsi						
								)
								group by tahun, id_jalur, nm_provinsi, NM_PENERIMAAN

");

if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }


foreach($mhs_55_3 as $data){

	$db_wh->Query("INSERT INTO WH_SE_MHS_PROVINSI_PEMINAT_UNI (THN_AKADEMIK_SMT, NM_PROVINSI, THN_ANGKATAN_JML, ID_JALUR, TGL_UPDATE, NM_PENERIMAAN)
	VALUES
	('$data[THN_AKADEMIK_SMT]', '$data[NM_PROVINSI]', '$data[THN_ANGKATAN_JML]', '$data[ID_JALUR]', '$data[TGL_UPDATE]', '$data[NM_PENERIMAAN]')");

	if ($db_wh->Error != '') { echo "Baris ". __LINE__ .": ". $db_wh->Error; exit(); }
}




?>