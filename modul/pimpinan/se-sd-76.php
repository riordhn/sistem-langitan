<?php
include('config.php');

if($_SESSION['ID_PRODI'] != ''){
	$where = "AND z.ID_PROGRAM_STUDI = '".$_SESSION['ID_PRODI']."'";
	$header = $_SESSION['JENJANG'] . ' - '. $_SESSION['PRODI'];

}elseif($_SESSION['ID_FAK'] != ''){
	$where = "AND z.ID_FAKULTAS = '".$_SESSION['ID_FAK']."'";
	$header = 'FAKULTAS '.strtoupper($_SESSION['NM_FAK']);

}else{
	$where = "";
	$header = strtoupper('Universitas Airlangga');
}

if($_SESSION['ID_PRODI'] != ''){
$tampil .= '
<table class="ui-widget">
	<tr style="background:navy;color:#fff;padding:5px;">
		<td colspan="7"><div class="center_title_bar">Daftar Dosen Tetap yang Mengikuti Pendidikan Tidak Bergelar (Magang, Pelatihan, Pencangkokan, dsb)</div></td>
	</tr>
	<tr class="ui-widget-header" style="padding:5px;">
		<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
	</tr>
</table>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<th><center>Nama</center></th>
			<th><center>Topik Pendidikan Tidak Bergelar</center></th>
			<th><center>Tempat</center></th>
			<th><center>Waktu Pelaksanaan</center></th>
			<th><center>Durasi Pelaksanaan</center></th>
		</tr>
';
$dt = $db_wh->QueryToArray("
select z.NM_LENGKAP_PENGGUNA, z.NM_PELATIHAN, z.TEMPAT, z.WAKTU, z.DURASI
from WH_PELATIHAN_DOSEN z
where z.ID_PROGRAM_STUDI = '".$_SESSION['ID_PRODI']."'
order by z.ID_FAKULTAS, z.ID_JENJANG, z.ID_PROGRAM_STUDI, z.NM_LENGKAP_PENGGUNA, z.TAHUN DESC
");

foreach($dt as $data){
$tampil .= '
	<tr class="ui-widget-content">
		<td>'.$data['NM_LENGKAP_PENGGUNA'].'</td>
		<td>'.$data['NM_PELATIHAN'].'</td>
		<td>'.$data['TEMPAT'].'</td>
		<td>'.$data['WAKTU'].'</td>
		<td>'.$data['DURASI'].'</td>
	</tr>';
}

$tampil .= '
</table>
';
}
elseif($_SESSION['ID_FAK'] != ''){
$tampil .= '
<table class="ui-widget">
	<tr style="background:navy;color:#fff;padding:5px;">
		<td colspan="7"><div class="center_title_bar">Daftar Dosen Tetap yang Mengikuti Pendidikan Tidak Bergelar (Magang, Pelatihan, Pencangkokan, dsb)</div></td>
	</tr>
	<tr class="ui-widget-header" style="padding:5px;">
		<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
	</tr>
</table>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<th><center>Nama</center></th>
			<th><center>Topik Pendidikan Tidak Bergelar</center></th>
			<th><center>Tempat</center></th>
			<th><center>Waktu Pelaksanaan</center></th>
			<th><center>Durasi Pelaksanaan</center></th>
		</tr>
';
$dt = $db_wh->QueryToArray("
select z.NM_LENGKAP_PENGGUNA, z.NM_PELATIHAN, z.TEMPAT, z.WAKTU, z.DURASI
from WH_PELATIHAN_DOSEN z
where z.ID_FAKULTAS = '".$_SESSION['ID_FAK']."'
order by z.ID_FAKULTAS, z.ID_JENJANG, z.ID_PROGRAM_STUDI, z.NM_LENGKAP_PENGGUNA, z.TAHUN DESC
");

foreach($dt as $data){
$tampil .= '
	<tr class="ui-widget-content">
		<td>'.$data['NM_LENGKAP_PENGGUNA'].'</td>
		<td>'.$data['NM_PELATIHAN'].'</td>
		<td>'.$data['TEMPAT'].'</td>
		<td>'.$data['WAKTU'].'</td>
		<td>'.$data['DURASI'].'</td>
	</tr>';
}

$tampil .= '
</table>
';
}else{
$tampil .= '
<table class="ui-widget">
	<tr style="background:navy;color:#fff;padding:5px;">
		<td colspan="7"><div class="center_title_bar">Daftar Dosen Tetap yang Mengikuti Pendidikan Tidak Bergelar (Magang, Pelatihan, Pencangkokan, dsb)</div></td>
	</tr>
	<tr class="ui-widget-header" style="padding:5px;">
		<td colspan="7"><div style="padding:10px;">'.$header.'</div></td>
	</tr>
</table>
<table class="ui-widget">
		<tr class="ui-widget-header">
			<td><center>Fakultas</center></td>
			<th><center>Nama</center></th>
			<th><center>Topik Pendidikan Tidak Bergelar</center></th>
			<th><center>Tempat</center></th>
			<th><center>Waktu Pelaksanaan</center></th>
			<th><center>Durasi Pelaksanaan</center></th>
		</tr>
';
$dt = $db_wh->QueryToArray("
select z.ID_FAKULTAS, z.NM_LENGKAP_PENGGUNA, z.NM_PELATIHAN, z.TEMPAT, z.WAKTU, z.DURASI, fak.SINGKATAN_FAKULTAS
from WH_PELATIHAN_DOSEN z
join FAKULTAS fak on z.ID_FAKULTAS = fak.ID_FAKULTAS
order by z.ID_FAKULTAS, z.ID_JENJANG, z.ID_PROGRAM_STUDI, z.NM_LENGKAP_PENGGUNA, z.TAHUN DESC
");
foreach($dt as $data){
$tampil .= '
	<tr class="ui-widget-content">
		<td><center>'.$data['SINGKATAN_FAKULTAS'].'</center></td>
		<td>'.$data['NM_LENGKAP_PENGGUNA'].'</td>
		<td>'.$data['NM_PELATIHAN'].'</td>
		<td>'.$data['TEMPAT'].'</td>
		<td>'.$data['WAKTU'].'</td>
		<td>'.$data['DURASI'].'</td>
	</tr>';
}

$tampil .= '
</table>
';
}

$smarty->assign('tampil', $tampil);
$smarty->display('se-sd-76.tpl');