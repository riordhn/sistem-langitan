<?PHP
include('config.php');

$smarty->assign('tahun',$_SESSION['TAHUN']);

 
if ($_SESSION['STATUS']==1 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);


for($i=$_SESSION[TAHUN];$i>($_SESSION[TAHUN]-3);$i--)
{
$aee_R = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
									sum(status_mhs_aktif)+sum(status_mhs_cuti_akd) as aktif,
									sum(status_mhs_lulus) as lulus,
									round(sum(status_mhs_lulus)/(sum(status_mhs_aktif)+sum(status_mhs_cuti_akd))*100,2) as aee
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and  status='R' and id_jenjang=1
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and  status='R' and id_jenjang=1 and status_mhs_aktif=1)
									group by substr(tahun,1,4)");
$smarty->assign('aee_R'.$i, $aee_R);

$aee_AJ = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
									sum(status_mhs_aktif)+sum(status_mhs_cuti_akd) as aktif,
									sum(status_mhs_lulus) as lulus,
									round(sum(status_mhs_lulus)/(sum(status_mhs_aktif)+sum(status_mhs_cuti_akd))*100,2) as aee
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and  status='AJ' and id_jenjang=1
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and  status='AJ' and id_jenjang=1 and status_mhs_aktif=1)
									group by substr(tahun,1,4)");
$smarty->assign('aee_AJ'.$i, $aee_AJ);

$aee_S2 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
									sum(status_mhs_aktif)+sum(status_mhs_cuti_akd) as aktif,
									sum(status_mhs_lulus) as lulus,
									round(sum(status_mhs_lulus)/(sum(status_mhs_aktif)+sum(status_mhs_cuti_akd))*100,2) as aee
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and  id_jenjang=2
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and  id_jenjang=2 and status_mhs_aktif=1)
									group by substr(tahun,1,4)");
$smarty->assign('aee_S2'.$i, $aee_S2);

$aee_S3 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
									sum(status_mhs_aktif)+sum(status_mhs_cuti_akd) as aktif,
									sum(status_mhs_lulus) as lulus,
									round(sum(status_mhs_lulus)/(sum(status_mhs_aktif)+sum(status_mhs_cuti_akd))*100,2) as aee
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and  id_jenjang=3
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and  id_jenjang=3 and status_mhs_aktif=1)
									group by substr(tahun,1,4)");
$smarty->assign('aee_S3'.$i, $aee_S3);

$aee_D3 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
									sum(status_mhs_aktif)+sum(status_mhs_cuti_akd) as aktif,
									sum(status_mhs_lulus) as lulus,
									round(sum(status_mhs_lulus)/(sum(status_mhs_aktif)+sum(status_mhs_cuti_akd))*100,2) as aee
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and  id_jenjang=5
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and  id_jenjang=5 and status_mhs_aktif=1)
									group by substr(tahun,1,4)");
$smarty->assign('aee_D3'.$i, $aee_D3);

$aee_PROFESI = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
									sum(status_mhs_aktif)+sum(status_mhs_cuti_akd) as aktif,
									sum(status_mhs_lulus) as lulus,
									round(sum(status_mhs_lulus)/(sum(status_mhs_aktif)+sum(status_mhs_cuti_akd))*100,2) as aee
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and  id_jenjang=9
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and  id_jenjang=9 and status_mhs_aktif=1)
									group by substr(tahun,1,4)");
$smarty->assign('aee_PROFESI'.$i, $aee_PROFESI);
}
$smarty->display("se-akademik-412-universitas.tpl");
}


if ($_SESSION['STATUS']==2 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('fakultas', $_SESSION['NM_FAK']);

for($i=$_SESSION[TAHUN];$i>($_SESSION[TAHUN]-3);$i--)
{
$aee_R = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
									sum(status_mhs_aktif)+sum(status_mhs_cuti_akd) as aktif,
									sum(status_mhs_lulus) as lulus,
									round(sum(status_mhs_lulus)/(sum(status_mhs_aktif)+sum(status_mhs_cuti_akd))*100,2) as aee
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and id_fakultas='$_SESSION[ID_FAK]' and status='R' and id_jenjang=1
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and id_fakultas='$_SESSION[ID_FAK]' and status='R' and id_jenjang=1 and status_mhs_aktif=1)
									group by substr(tahun,1,4)");
$smarty->assign('aee_R'.$i, $aee_R);

$aee_AJ = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
									sum(status_mhs_aktif)+sum(status_mhs_cuti_akd) as aktif,
									sum(status_mhs_lulus) as lulus,
									round(sum(status_mhs_lulus)/(sum(status_mhs_aktif)+sum(status_mhs_cuti_akd))*100,2) as aee
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and id_fakultas='$_SESSION[ID_FAK]' and status='AJ' and id_jenjang=1
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and id_fakultas='$_SESSION[ID_FAK]' and status='AJ' and id_jenjang=1 and status_mhs_aktif=1)
									group by substr(tahun,1,4)");
$smarty->assign('aee_AJ'.$i, $aee_AJ);

$aee_S2 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
									sum(status_mhs_aktif)+sum(status_mhs_cuti_akd) as aktif,
									sum(status_mhs_lulus) as lulus,
									round(sum(status_mhs_lulus)/(sum(status_mhs_aktif)+sum(status_mhs_cuti_akd))*100,2) as aee
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2 and status_mhs_aktif=1)
									group by substr(tahun,1,4)");
$smarty->assign('aee_S2'.$i, $aee_S2);

$aee_S3 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
									sum(status_mhs_aktif)+sum(status_mhs_cuti_akd) as aktif,
									sum(status_mhs_lulus) as lulus,
									round(sum(status_mhs_lulus)/(sum(status_mhs_aktif)+sum(status_mhs_cuti_akd))*100,2) as aee
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3 and status_mhs_aktif=1)
									group by substr(tahun,1,4)");
$smarty->assign('aee_S3'.$i, $aee_S3);

$aee_D3 = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
									sum(status_mhs_aktif)+sum(status_mhs_cuti_akd) as aktif,
									sum(status_mhs_lulus) as lulus,
									round(sum(status_mhs_lulus)/(sum(status_mhs_aktif)+sum(status_mhs_cuti_akd))*100,2) as aee
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5 and status_mhs_aktif=1)
									group by substr(tahun,1,4)");
$smarty->assign('aee_D3'.$i, $aee_D3);

$aee_PROFESI = $db_wh->QueryToArray("select substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
									sum(status_mhs_aktif)+sum(status_mhs_cuti_akd) as aktif,
									sum(status_mhs_lulus) as lulus,
									round(sum(status_mhs_lulus)/(sum(status_mhs_aktif)+sum(status_mhs_cuti_akd))*100,2) as aee
									from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9
									and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
									where tahun in (select max(tahun) from wh_status_akademik 
								    where substr(tahun,1,4)='$i') and id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9 and status_mhs_aktif=1)
									group by substr(tahun,1,4)");
$smarty->assign('aee_PROFESI'.$i, $aee_PROFESI);
}
$smarty->display("se-akademik-412-fakultas.tpl");
}

if ($_SESSION['STATUS']==3 ) {
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('jenjang', $_SESSION['JENJANG']);
$smarty->assign('prodi', $_SESSION['PRODI']);

for($i=$_SESSION[TAHUN];$i>($_SESSION[TAHUN]-3);$i--)
{

$aee_R = $db_wh->QueryToArray("select s1.id_program_studi,tahun_akademik,aktif,lulus,round(lulus/aktif,2)*100 as aee from (
								select id_program_studi,substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
								sum(krs)+	sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)+sum(status_mhs_cuti_akd) as aktif
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$i') and status='R' and id_program_studi='$_SESSION[ID_PRODI]'
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$i') and id_program_studi='$_SESSION[ID_PRODI]' and status='R' and  (status_mhs_aktif=1 or krs=1))
								group by id_program_studi, substr(tahun,1,4))s1
								left join
								(select substr(thn_akademik_smt,1,4),id_program_studi, 
								count(*) as lulus
								from WH_SE_LULUSAN_IPK_MHS
								where substr(thn_akademik_smt,1,4)='$i' and status_mhs='R' and id_program_studi='$_SESSION[ID_PRODI]'
								group by substr(thn_akademik_smt,1,4), id_program_studi)s2 on s1.id_program_studi=s2.id_program_studi");
		
$smarty->assign('aee_R'.$i, $aee_R);

$aee_AJ = $db_wh->QueryToArray("select s1.id_program_studi,tahun_akademik,aktif,lulus,round(lulus/aktif,2)*100 as aee from (
								select id_program_studi,substr(tahun,1,4)||'/'||(substr(tahun,1,4)+1) as tahun_akademik, 
								sum(krs)+	sum(case when status_mhs_cuti_akd=0 and status_mhs_aktif=1 and krs=0 then 1 else 0 end)+sum(status_mhs_cuti_akd) as aktif
								from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$i') and status='AJ' and id_program_studi='$_SESSION[ID_PRODI]'
								and thn_angkatan_mhs >= (select min(thn_angkatan_mhs) from wh_status_akademik
								where tahun in (select max(tahun) from wh_status_akademik 
								where substr(tahun,1,4)='$i') and id_program_studi='$_SESSION[ID_PRODI]' and status='AJ' and  (status_mhs_aktif=1 or krs=1))
								group by id_program_studi, substr(tahun,1,4))s1
								left join
								(select substr(thn_akademik_smt,1,4),id_program_studi, 
								count(*) as lulus
								from WH_SE_LULUSAN_IPK_MHS
								where substr(thn_akademik_smt,1,4)='$i' and status_mhs='AJ' and id_program_studi='$_SESSION[ID_PRODI]'
								group by substr(thn_akademik_smt,1,4), id_program_studi)s2 on s1.id_program_studi=s2.id_program_studi");
$smarty->assign('aee_AJ'.$i, $aee_AJ);
}
$smarty->display("se-akademik-412-prodi.tpl");
}

?>