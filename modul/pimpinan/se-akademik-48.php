<?PHP
include('config.php');

$smarty->assign('tahun',$_SESSION['TAHUN']);


if ($_SESSION['STATUS']==1 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);


$ikn_R = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where  id_jenjang=1 and jalur='R' 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_R', $ikn_R);

$ik_R = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where  id_jenjang=1 and jalur='R'
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_R', $ik_R);

$ikn_AJ = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where  id_jenjang=1 and jalur='AJ'
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_AJ', $ikn_AJ);

$ik_AJ = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where  id_jenjang=1 and jalur='AJ'
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_AJ', $ik_AJ);

$ikn_S2 = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where  id_jenjang=2 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_S2', $ikn_S2);

$ik_S2 = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where  id_jenjang=2 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_S2', $ik_S2);

$ikn_S3 = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where  id_jenjang=3 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_S3', $ikn_S3);

$ik_S3 = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where  id_jenjang=3 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_S3', $ik_S3);

$ikn_D3 = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where  id_jenjang=5 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_D3', $ikn_D3);

$ik_D3 = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where  id_jenjang=5 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_D3', $ik_D3);

$ikn_PROFESI = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where  id_jenjang=9 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_PROFESI', $ikn_PROFESI);

$ik_PROFESI = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where  id_jenjang=9 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_PROFESI', $ik_PROFESI);


$smarty->display("se-akademik-48-universitas.tpl");
}


if ($_SESSION['STATUS']==2 )
{
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('fakultas', $_SESSION['NM_FAK']);

$ikn_R = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and jalur='R' 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_R', $ikn_R);

$ik_R = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and jalur='R'
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_R', $ik_R);

$ikn_AJ = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and jalur='AJ'
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_AJ', $ikn_AJ);

$ik_AJ = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=1 and jalur='AJ'
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_AJ', $ik_AJ);

$ikn_S2 = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_S2', $ikn_S2);

$ik_S2 = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=2 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_S2', $ik_S2);

$ikn_S3 = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_S3', $ikn_S3);

$ik_S3 = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=3 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_S3', $ik_S3);

$ikn_D3 = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_D3', $ikn_D3);

$ik_D3 = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=5 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_D3', $ik_D3);

$ikn_PROFESI = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_PROFESI', $ikn_PROFESI);

$ik_PROFESI = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where id_fakultas='$_SESSION[ID_FAK]' and id_jenjang=9 
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_PROFESI', $ik_PROFESI);


$smarty->display("se-akademik-48-fakultas.tpl");
}

if ($_SESSION['STATUS']==3 ) {
//parameter info
$smarty->assign('status', $_SESSION['STATUS']);
$smarty->assign('jenjang', $_SESSION['JENJANG']);
$smarty->assign('prodi', $_SESSION['PRODI']);

$ikn_R = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where id_program_studi='$_SESSION[ID_PRODI]' and jalur='R'
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_R', $ikn_R);

$ik_R = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah,NULL)),2) as kul,round(avg(coalesce(ik_praktikum,NULL)),2) as prk
								from wh_ik
								where id_program_studi='$_SESSION[ID_PRODI]' and jalur='R'
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_R', $ik_R);

$ikn_AJ = $db_wh->QueryToArray("select distinct nm_mata_kuliah from wh_ik
								where id_program_studi='$_SESSION[ID_PRODI]' and jalur='AJ'
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ikn_AJ', $ikn_AJ);

$ik_AJ = $db_wh->QueryToArray("select nm_mata_kuliah,substr(tahun,1,4) as tahun,
								round(avg(coalesce(ik_kuliah, NULL)),2) as kul,round(avg(coalesce(ik_praktikum, NULL)),2) as prk
								from wh_ik
								where id_program_studi='$_SESSION[ID_PRODI]' and jalur='AJ'
								group by nm_mata_kuliah,substr(tahun,1,4)
								order by nm_mata_kuliah");
$smarty->assign('ik_AJ', $ik_AJ);

$smarty->display("se-akademik-48-prodi.tpl");
}

?>