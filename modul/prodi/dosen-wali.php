<?php
/*
update by Yudi Sulistya on 29/12/2011
*/

error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

//$smarty->clearAllAssign();

//if (!isFindData("jadwal_kelas","id_kelas_mk=162")) {echo "a";}


$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');
$smarty->assign('disp5','none');

$id_pt = $id_pt_user;

$kdprodi=$user->ID_PROGRAM_STUDI;
$kdfak=$user->ID_FAKULTAS;
$smarty->assign('kdfak',$kdfak);
//$kdfak=getvar("select id_fakultas from program_studi where id_program_studi=$kdprodi");

$db->Query("select id_departemen from program_studi where id_program_studi=$kdprodi");
$kddep = $db->FetchAssoc();

//Untuk Fakultas Psikologi
/*if($kdfak==11){
	echo '
		<script>
		location.href="#aktivitas-dosenw!dosen-wali-psikologi.php?action=tampil";
		</script>
	';

//Fakultas Lain
} else {*/

$db->Query("select angkatan from
(select distinct thn_angkatan_mhs as angkatan from mahasiswa
where id_program_studi=$kdprodi and status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
order by thn_angkatan_mhs)
where rownum=1");
$default = $db->FetchAssoc();

$db->Query("select * from semester where status_aktif_semester='True' and id_perguruan_tinggi = '{$id_pt}'");
$smtaktif = $db->FetchAssoc();
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$angkatan=getData("select distinct thn_angkatan_mhs as angkatan from mahasiswa
where id_program_studi=$kdprodi and status_akademik_mhs<4 order by angkatan");
$smarty->assign('ANG', $angkatan);
$agk = isSet($_GET['angk']) ? $_GET['angk'] : $default['ANGKATAN'];


/*if ($kdfak == 7)
{
$dosen=getData("select id_dosen,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nama_dosen from dosen
left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
left join pengguna b on dosen.id_pengguna=b.id_pengguna
where id_departemen='$kddep[ID_DEPARTEMEN]' order by nm_pengguna");
}elseif ($kdprodi==124) {
$dosen=getData("select id_dosen,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nama_dosen from dosen
left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
left join pengguna b on dosen.id_pengguna=b.id_pengguna
where program_studi.id_fakultas=$kdfak order by nm_pengguna");
}elseif ($kdfak == 9) {
$dosen=getData("select dosen.id_dosen,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nama_dosen from program_studi
join dosen on program_studi.id_kaprodi=dosen.id_dosen
join pengguna on dosen.id_pengguna=pengguna.id_pengguna
where id_fakultas=$kdfak order by nm_pengguna");
} else {*/
$dosen=getData("SELECT id_dosen,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nama_dosen 
	from dosen
	left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
	left join pengguna b on dosen.id_pengguna=b.id_pengguna
	where program_studi.id_fakultas=$kdfak /*and dosen.status_dosen = 'PNS'*/ order by nm_pengguna");
/*}*/


$smarty->assign('DOSEN', $dosen);

$jaf2=getData("SELECT dw.id_dosen, p.username as nip_dosen, p.nm_pengguna, p.gelar_depan, p.gelar_belakang, count(dw.id_mhs) as TTL
FROM dosen_wali dw
JOIN mahasiswa m ON m.id_mhs = dw.id_mhs
JOIN dosen d ON d.id_dosen = dw.id_dosen
JOIN pengguna p ON p.id_pengguna = d.id_pengguna
WHERE 
    status_dosen_wali = 1 AND 
    m.id_program_studi = {$kdprodi} AND 
    m.status_akademik_mhs IN (SELECT id_status_pengguna FROM status_pengguna WHERE id_role = 3 AND status_aktif = 1)
GROUP BY dw.id_dosen, p.username, p.nm_pengguna, p.gelar_depan, p.gelar_belakang");
$smarty->assign('SEBDOLI',$jaf2);


//}


/*
$angkatan_nowali=getData("select distinct '20'||substr(nim_mhs,3,2) as angkatan from mahasiswa
where id_program_studi=$kdprodi and id_mhs not in (select id_mhs from dosen_wali)
order by angkatan");
$smarty->assign('ANG_NOWALI', $angkatan_nowali);
*/

$jaf1=getData("select mahasiswa.id_mhs,nim_mhs,upper(nm_pengguna) as nm_pengguna,'0' as tanda,thn_angkatan_mhs, nk.nama_kelas from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join mahasiswa_kelas mk on mk.id_mhs = mahasiswa.id_mhs and mk.is_aktif = '1'
left join nama_kelas nk on nk.id_nama_kelas = mk.id_nama_kelas
where id_program_studi=$kdprodi and mahasiswa.id_mhs not in (select id_mhs from dosen_wali where status_dosen_wali = 1)
and status_akademik_mhs in
(select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
order by nim_mhs
");
		$smarty->assign('NOWALI', $jaf1);



$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';
//echo $status;

switch($status) {

case 'savewali':

		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','none');

		$id_dosen = $_POST['dosen'];
		$counter = $_POST['counter1'];

		for ($i=1; $i<=$counter; $i++)
		{
		if ($_POST['mhs'.$i]<>'' || $_POST['mhs'.$i]<>null) {
					gantidata("insert into dosen_wali(id_mhs,id_semester,id_dosen,status_dosen_wali)
							  values ('".$_POST['mhs'.$i]."','$smtaktif[ID_SEMESTER]',$id_dosen,1)");
		}
		}

		$jaf1=getData("select mahasiswa.id_mhs,nim_mhs,upper(nm_pengguna) as nm_pengguna,'0' as tanda,thn_angkatan_mhs, nk.nama_kelas from mahasiswa
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						left join mahasiswa_kelas mk on mk.id_mhs = mahasiswa.id_mhs and mk.is_aktif = '1'
						left join nama_kelas nk on nk.id_nama_kelas = mk.id_nama_kelas
						where id_program_studi=$kdprodi and mahasiswa.id_mhs not in (select id_mhs from dosen_wali where status_dosen_wali = 1)
						and status_akademik_mhs in
						(select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
						order by nim_mhs
						");
		$smarty->assign('NOWALI', $jaf1);

		break;

case 'update':
		
		$thn_angkatan_mhs = (isset($_GET['angk']) && is_numeric($_GET['angk'])) ? $_GET['angk'] : 0;
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','none');

		$id_mhs=$_POST['id_mhs'];
		//$id_dosen=$_POST['dosen[$id_mhs]'];
		$id_dosen=$_POST['dosen'];
		//echo $id_mhs;
		//echo $id_dosen;

		//UpdateData("dosen_wali","id_mhs,id_dosen,id_semester,status_dosen_wali","$id_mhs,$id_dosen,".$smtaktif['ID_SEMESTER'].",1");
		UpdateData("update dosen_wali set status_dosen_wali=0 where id_mhs='$id_mhs' and status_dosen_wali=1");
		//echo "update dosen_wali set status_dosen_wali=0 where id_mhs='$id_mhs' and status_dosen_wali=1";
		
		UpdateData("insert into dosen_wali (id_mhs,id_dosen,id_semester,status_dosen_wali) values ($id_mhs,$id_dosen,".$smtaktif['ID_SEMESTER'].",1)");
		//echo "insert into dosen_wali (id_mhs,id_dosen,id_semester,status_dosen_wali) values ($id_mhs,$id_dosen,".$smtaktif['ID_SEMESTER'].",1)";
				
		$jaf=getData("select mahasiswa.id_mhs,nim_mhs,upper(pengguna.nm_pengguna) as mhs,
			trim(b.gelar_depan||' '||upper(b.nm_pengguna)||', '||b.gelar_belakang) as doli,thn_angkatan_mhs, nk.nama_kelas from mahasiswa
		left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
		left join dosen_wali on dosen_wali.id_mhs=mahasiswa.id_mhs
		left join dosen on dosen_wali.id_dosen=dosen.id_dosen
		left join pengguna b on dosen.id_pengguna=b.id_pengguna
		left join mahasiswa_kelas mk on mk.id_mhs = mahasiswa.id_mhs and mk.is_aktif = '1'
		left join nama_kelas nk on nk.id_nama_kelas = mk.id_nama_kelas
		where mahasiswa.id_program_studi=$kdprodi and
		mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
		and thn_angkatan_mhs = '{$thn_angkatan_mhs}'");

		#FILTER DIBAWAH UTK SEMENTARA DIMATIKAN KARENA BERHUBUNGAN DGN FORMAT NIM ~ RIESKHA
		#and substr(nim_mhs,3,2)=substr($agk,3,2)")

		$smarty->assign('WALI', $jaf);

		break;

case 'searchdosen':
		$namacari=$_POST['namadosen'];
		$id_mk=$_GET['id_klsmk'];
		$stdos=$_GET['status'];
		$pjmk=$_GET['pjmk'];
		$id_pengampu=$_GET['id_pengampu'];
		$status1=$_GET['status1'];

		if ($status1==ganti) {
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','block');} else {
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
		$smarty->assign('disp4','none');}

		//echo $id_mk;
		//echo $stdos;
		//echo $pjmk;
		//echo $id_pengampu;
		//echo $status1;


		if ($namacari !='') {
		$hasil=getData("select id_dosen,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna from dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
where nm_pengguna like '%$namacari%'");}
		$smarty->assign('DOSEN',$hasil);
		$smarty->assign('IDKEL',$id_mk);
		$smarty->assign('ST_PJMK',$pjmk);
		$smarty->assign('ID_PENGAMPU',$id_pengampu);
		$smarty->assign('STATUS1',$status1);

		break;

case 'viewupdate':
		 // pilih
		 $smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
		$smarty->assign('disp4','none');
		$id_mhs= $_GET['id_mhs'];

$jaf=getData("select id_dosen_wali,	mahasiswa.id_mhs,nim_mhs,upper(pengguna.nm_pengguna) as mhs,
trim(b.gelar_depan||' '||upper(b.nm_pengguna)||', '||b.gelar_belakang) as doli from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join dosen_wali on dosen_wali.id_mhs=mahasiswa.id_mhs
left join dosen on dosen_wali.id_dosen=dosen.id_dosen
left join pengguna b on dosen.id_pengguna=b.id_pengguna
where mahasiswa.id_program_studi=$kdprodi and mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
and mahasiswa.id_mhs=$id_mhs
and dosen_wali.status_dosen_wali='1'");
		$smarty->assign('DOLI', $jaf);

        break;

case 'del':
		 // pilih


        break;


case 'tampil':
		//echo "aa";
		//echo $smtaktif['ID_SEMESTER'];
		$smt1= $_POST['smt'];
		 if ($smt1 == '') { 
		 	$smt1=$smtaktif['ID_SEMESTER'];
		 }
		//echo $smt1;

		if ($smt1!='') {
		$smarty->assign('SMT',$smt);
		//$smarty->assign('KUR', $id_kur);

		//$agk = isSet($_GET['angk']) ? $_GET['angk'] : $default['ANGKATAN'];
		//echo $agk;
		$thn_angkatan_mhs = (isset($_GET['angk']) && is_numeric($_GET['angk'])) ? $_GET['angk'] : 0;
		$jaf=getData("SELECT id_dosen_wali,mahasiswa.id_mhs,nim_mhs,nip_dosen,thn_angkatan_mhs,
						upper(pengguna.nm_pengguna) as mhs,trim(b.gelar_depan||' '||upper(b.nm_pengguna)||', '||b.gelar_belakang) as doli, nk.nama_kelas
						from mahasiswa
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						left join dosen_wali on dosen_wali.id_mhs=mahasiswa.id_mhs
						left join dosen on dosen_wali.id_dosen=dosen.id_dosen
						left join pengguna b on dosen.id_pengguna=b.id_pengguna
						left join mahasiswa_kelas mk on mk.id_mhs = mahasiswa.id_mhs and mk.is_aktif = '1'
						left join nama_kelas nk on nk.id_nama_kelas = mk.id_nama_kelas
						where mahasiswa.id_program_studi=$kdprodi and mahasiswa.id_mhs in (select id_mhs from mahasiswa
						where status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1))						
						and dosen_wali.status_dosen_wali='1' and thn_angkatan_mhs = '{$thn_angkatan_mhs}'");
						# FILTER DIBAWAH UTK SEMENTARA DIMATIKAN KARENA BERHUBUNGAN DGN FORMAT NIM ~ RIESKHA
						#and substr(nim_mhs,3,2)=substr($agk,3,2)
		}
		$smarty->assign('WALI', $jaf);

        break;

case 'detailwali':

		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','block');

		$id_dosen=$_GET['dosen'];

		$db->Query("select nip_dosen, trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as dosen
					from dosen left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					where dosen.id_dosen=$id_dosen");
		$dat = $db->FetchAssoc();

		$smarty->assign('NIPDSN', $dat['NIP_DOSEN']);
		$smarty->assign('NMDSN', $dat['DOSEN']);

		$det=getData("select nim_mhs,upper(pengguna.nm_pengguna) as mhs, upper(nm_jenjang)||' - '||upper(nm_singkat_prodi) as prodi
					from mahasiswa
					left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
					left join dosen_wali on dosen_wali.id_mhs=mahasiswa.id_mhs
					left join program_studi on program_studi.id_program_studi=mahasiswa.id_program_studi
					left join jenjang on jenjang.id_jenjang=program_studi.id_jenjang
					where dosen_wali.id_dosen=$id_dosen and mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
					and dosen_wali.status_dosen_wali='1'
					order by nim_mhs");
		$smarty->assign('DET', $det);

		break;
}

$smarty->display('dosen-wali.tpl');

/*}*/
?>
