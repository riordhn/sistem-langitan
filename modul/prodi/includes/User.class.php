<?php
class user
{
    public $id_pengguna = '';
    public $nama_pengguna = '';
    public $id_role = 0;
    public $nama_role = '';
    
    /**
     * Konstruktor dari kelas user
     */
    function __construct()
    {
        session_start();
	
        
        // Cek kondisi login
        if (isset($_SESSION['login']))
        {
            $this->id_pengguna = $_SESSION['login']['ID_PENGGUNA'];
            $this->nama_pengguna = $_SESSION['login']['NAMA_PENGGUNA'];
            $this->id_role = $_SESSION['login']['ID_ROLE'];
            $this->nama_role = $_SESSION['login']['NAMA_ROLE'];
        }
        else
        {
            // Alihkan ke halaman utama portal
            header('Location: ../../');
            exit();
        }
    }
}
?>
