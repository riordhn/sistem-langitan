<?php
if ($_SERVER['REMOTE_ADDR'] != '162.158.167.192') { echo 'dalam perbaikan '.$_SERVER['REMOTE_ADDR']; exit(); }

require ('common.php');
require_once ('ociFunction.php');

error_reporting (E_ALL & ~E_NOTICE);

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');
$smarty->assign('disp5','none');

$kdprodi= $user->ID_PROGRAM_STUDI;
$smarty->assign('kdprod',$kdprodi);

$kdfak= $user->ID_FAKULTAS;
$smarty->assign('kdf',$kdfak);

//Khusus Fakultas Psikologi
if($kdfak == 11)
{

	echo '
		<script>
		location.href="#aktivitas-uma!usulan-mk-psikologi.php?action=tampil";
		</script>
	';

//Fakultas Lain
} 
else 
{

$idjen = getvar("SELECT id_jenjang from program_studi where id_program_studi=$kdprodi");
$smarty->assign('jen',$idjen['ID_JENJANG']);

$smtaktif = getvar("SELECT id_semester from semester where status_aktif_semester='True' AND id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$smt1 = isset($_REQUEST['smt']) ? $_REQUEST['smt'] : $smtaktif['ID_SEMESTER'];
$smarty->assign('SMT',$smt1);

$idkur = getvar(
	"SELECT id_kurikulum_prodi from kurikulum_prodi
	left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
	where kurikulum_prodi.id_program_studi=$kdprodi and status_aktif='1'");

$kur = isSet($_REQUEST['kur']) ? $_REQUEST['kur'] : $idkur['ID_KURIKULUM_PRODI'];
$smarty->assign('kuraktif', $kur);

//ALL FAKULTAS
$smt = getData(
	"SELECT id_semester, tahun_ajaran||' - '||nm_semester as smt 
	from semester 
	where thn_akademik_semester in (
		select * from (
			select distinct thn_akademik_semester
			from semester where thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc
		)
		where rownum<=3
	) and nm_semester in ('Ganjil', 'Genap') and id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}'
	order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_ST', $smt);

$kelas = getData(
	"SELECT id_nama_kelas, nama_kelas FROM nama_kelas
	where (id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI} or id_perguruan_tinggi is null)
	order by nama_kelas");
$smarty->assign('NM_KELAS', $kelas);

$datakur = getData(
	"SELECT id_kurikulum_prodi,nm_kurikulum||'-'||tahun_kurikulum as nama
	from kurikulum_prodi
	left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
	where kurikulum_prodi.id_program_studi=$kdprodi and kurikulum_prodi.status_aktif = 1 order by tahun_kurikulum desc");
$smarty->assign('T_KUR', $datakur);

$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';

//echo $status; 
//echo $smt1;
switch($status) 
{
	case 'add':

		$id_mk	= $_POST['id_mk'];
		$kur	= $_POST['kur'];  // id_kurikulum_prodi
		//echo $smt1;
		//echo $id_mk;
		//echo $kdprodi;

		// Modifikasi Selected Tab
		$_REQUEST['action'] = 'penawaran';
		
		if ($kdprodi == 228)
		{
			tambahdata("kelas_mk","id_kurikulum_mk,id_semester,id_program_studi,status","'$id_mk','$smt1','$kdprodi','MKWU'");
		} 
		else
		{
			tambahdata("kelas_mk","id_kurikulum_mk,id_semester,id_program_studi","'$id_mk','$smt1','$kdprodi'");
		}
		

		$tawar = getData(
			"SELECT distinct id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_tatap_muka,kredit_praktikum,
			kmk.kredit_semester, nm_status_mk,nm_kelompok_mk, tahun_kurikulum,
			tingkat_semester, kmk.id_kurikulum_prodi from kurikulum_mk kmk left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join status_mk on kmk.id_status_mk=status_mk.id_status_mk
			left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk
			left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
			where 
				kmk.id_program_studi=$kdprodi and 
				id_kurikulum_mk not in (select id_kurikulum_mk from kelas_mk where id_program_studi=$kdprodi and id_semester=$smt1) and
				kmk.id_kurikulum_prodi = {$kur}");
		$smarty->assign('TAWAR', $tawar);
		
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','none');
		
        break;


	case 'tambahkelas':

		$kap_kelas	= $_POST['kap_kelas'];
		$kelas_mk	= $_POST['kelas_mk'];
		$ren_kul	= $_POST['ren_kul'];
		$id_kur_mk	= $_POST['id_kur_mk'];
		$id_hari	= $_POST['hari'];
		$id_jam		= $_POST['jam'];
		//$smt1		= $_POST['smtb'];
		$idruang	= $_POST['ruangan'];

		if ($kdprodi == 228)
		{
			tambahdata("kelas_mk","id_kurikulum_mk,id_semester,id_program_studi,no_kelas_mk,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,status","'$id_kur_mk','$smt1','$kdprodi','$kelas_mk','$kap_kelas','$ren_kul','MKWU'");
		} 
		else
		{
			tambahdata("kelas_mk","id_kurikulum_mk,id_semester,id_program_studi,no_kelas_mk,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk","'$id_kur_mk','$smt1','$kdprodi','$kelas_mk','$kap_kelas','$ren_kul'");
		}
				
		$id_kelas = getvar("SELECT id_kelas_mk from kelas_mk where id_kurikulum_mk=$id_kur_mk and id_semester=$smt1 and id_program_studi=$kdprodi and no_kelas_mk=$kelas_mk");
		$id_kelas_mk = $id_kelas['ID_KELAS_MK'];				
			
		
		if ($kdprodi == 228)
		{
			
			//khusus FST
			$id_jadwal_jam_mulai=getvar("SELECT * from jadwal_jam where id_fakultas='8' 
					and jam_mulai = (select jam_mulai from jadwal_jam where id_jadwal_jam = '$id_jam')
					and menit_mulai = (select menit_mulai from jadwal_jam where id_jadwal_jam = '$id_jam')");
			$id_jadwal_jam_mulai=$id_jadwal_jam_mulai['ID_JADWAL_JAM'];
			
			$id_jadwal_jam_selesai=getvar("SELECT * from jadwal_jam where id_fakultas='8' 
					and jam_selesai = (select jam_selesai from jadwal_jam where id_jadwal_jam = '$id_jam')
					and menit_selesai = (select menit_selesai from jadwal_jam where id_jadwal_jam = '$id_jam')");
			$id_jadwal_jam_selesai=$id_jadwal_jam_selesai['ID_JADWAL_JAM'];
		
			tambahdata("jadwal_kelas","id_kelas_mk,id_jadwal_hari,id_jadwal_jam,id_ruangan,id_jadwal_jam_mulai,id_jadwal_jam_selesai","'$id_kelas_mk','$id_hari','$id_jam','$idruang','$id_jadwal_jam_mulai','$id_jadwal_jam_selesai'");
		} 
		else
		{
			tambahdata("jadwal_kelas","id_kelas_mk,id_jadwal_hari,id_jadwal_jam,id_ruangan","'$id_kelas_mk','$id_hari','$id_jam','$idruang'");
		}
		
		/**
		$jaf = getData(
			"select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
				kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester,
				jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
				jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
				ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'belum terisi' end as status_pjmk,
				no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,tahun_kurikulum 
			from kelas_mk
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
			left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
			left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
			left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
			where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_semester=$smt1");

		$smarty->assign('T_MK', $jaf);
		*/
        break;


	case 'tambahhari':

		$kap_kelas		= $_POST['kap_kelas'];
		$id_kelas_mk	= $_POST['id_kelas_mk'];
		$ren_kul		= $_POST['ren_kul'];
		$id_kur_mk		= $_POST['id_kur_mk'];
		$id_hari		= $_POST['hari'];
		$id_jam			= $_POST['jam'];
		//$smt1			= $_POST['smtb'];
		$idruang		= $_POST['ruangan'];
		
		if ($kdprodi == 228)
		{
			
			//khusus FST
			$id_jadwal_jam_mulai = getvar("SELECT * from jadwal_jam where id_fakultas='8' 
					and jam_mulai = (select jam_mulai from jadwal_jam where id_jadwal_jam = '$id_jam')
					and menit_mulai = (select menit_mulai from jadwal_jam where id_jadwal_jam = '$id_jam')");
			$id_jadwal_jam_mulai=$id_jadwal_jam_mulai['ID_JADWAL_JAM'];
			
			$id_jadwal_jam_selesai = getvar("SELECT * from jadwal_jam where id_fakultas='8' 
					and jam_selesai = (select jam_selesai from jadwal_jam where id_jadwal_jam = '$id_jam')
					and menit_selesai = (select menit_selesai from jadwal_jam where id_jadwal_jam = '$id_jam')");
			$id_jadwal_jam_selesai=$id_jadwal_jam_selesai['ID_JADWAL_JAM'];
				
			tambahdata("jadwal_kelas","id_kelas_mk,id_jadwal_hari,id_jadwal_jam,id_ruangan,id_jadwal_jam_mulai,id_jadwal_jam_selesai","'$id_kelas_mk','$id_hari','$id_jam','$idruang','$id_jadwal_jam_mulai','$id_jadwal_jam_selesai'");
		} 
		else
		{
			tambahdata("jadwal_kelas","id_kelas_mk,id_jadwal_hari,id_jadwal_jam,id_ruangan","'$id_kelas_mk','$id_hari','$id_jam','$idruang'");
		}
		
		/*
		$jaf=getData(
			"select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
			kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester,
			jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
			jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
			ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'belum terisi' end as status_pjmk,
			no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,tahun_kurikulum from kelas_mk
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
			left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
			left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
			left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
			where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_semester=$smt1");
		$smarty->assign('T_MK', $jaf);
		*/
		
        break;


	case 'del':

		$id_klsmk = $_GET['id_klsmk'];
		
		deleteData("delete from komponen_mk where id_kelas_mk=$id_klsmk");
		deleteData("delete from jadwal_kelas where id_kelas_mk=$id_klsmk");
		deleteData("delete from pengampu_mk where id_kelas_mk=$id_klsmk");
		deleteData("delete from kelas_mk where id_kelas_mk=$id_klsmk");
		//echo "delete from kelas_mk where id_kelas_mk=$id_klsmk";
        break;

	case 'updateview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','none');

		$id_klsmk = $_GET['id_klsmk'];

		$ruang = getData(
			"SELECT id_ruangan,nm_ruangan||' ('||kapasitas_ruangan||')' as ruang 
			from ruangan
			join gedung on gedung.id_gedung = ruangan.id_gedung and gedung.id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}
			left join gedung_kuliah on gedung.id_gedung = gedung_kuliah.id_gedung
			where gedung.id_fakultas = '{$kdfak}' order by nm_ruangan");

		$smarty->assign('T_RUANG', $ruang);

		$hari = getData("SELECT id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);

		$jam = getData("SELECT id_jadwal_jam,nm_jadwal_jam from jadwal_jam where id_fakultas='".$kdfak."' order by jam_mulai||menit_mulai, jam_selesai||menit_selesai");
		$smarty->assign('T_JAM', $jam);


		$jaf=getData(
			"SELECT distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
				kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,
				jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,kelas_mk.id_semester,
				ruangan.id_ruangan,nm_ruangan,no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,tahun_kurikulum from kelas_mk
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
			left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
			left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
			where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_kelas_mk=$id_klsmk and rownum=1");
		$smarty->assign('TJAF', $jaf);
        break;


	case 'adkelview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','block');
		$smarty->assign('disp5','none');

		$id_klsmk = $_GET['id_klsmk'];

		if ($kdprodi == 228)
		{
			$ruang = getData(
				"SELECT id_ruangan,nm_ruangan||' ('||kapasitas_ruangan||')' as ruang from ruangan
				left join gedung on gedung.id_gedung=ruangan.id_gedung
				left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
				where gedung.id_gedung in(63,70) order by nm_ruangan");
		} 
		else
		{
			$ruang = getData(
				"SELECT id_ruangan,nm_ruangan||' ('||kapasitas_ruangan||')' as ruang from ruangan
				left join gedung on gedung.id_gedung=ruangan.id_gedung
				left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
				where gedung.id_fakultas='".$kdfak."' order by nm_ruangan");
		}

		$smarty->assign('T_RUANG', $ruang);

		$hari = getData("SELECT id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);

		$jam = getData("SELECT id_jadwal_jam,nm_jadwal_jam from jadwal_jam where id_fakultas='".$kdfak."' order by jam_mulai||menit_mulai, jam_selesai||menit_selesai");
		$smarty->assign('T_JAM', $jam);


		$jaf=getData(
			"SELECT distinct kelas_mk.id_kurikulum_mk, kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
				kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,
				jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,kelas_mk.id_semester,
				ruangan.id_ruangan,nm_ruangan,no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,tahun_kurikulum from kelas_mk
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
			left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
			left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
			where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_kelas_mk=$id_klsmk and rownum=1");
		$smarty->assign('TJAF1', $jaf);

        break;


	case 'adhariview':
		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','block');
		
		$id_klsmk = $_GET['id_klsmk'];

		if ($kdprodi == 228)
		{
			$ruang = getData(
				"SELECT id_ruangan,nm_ruangan||' ('||kapasitas_ruangan||')' as ruang from ruangan
				left join gedung on gedung.id_gedung=ruangan.id_gedung
				left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
				where gedung.id_gedung in(63,70) order by nm_ruangan");
		} 
		else
		{
			$ruang = getData(
				"SELECT id_ruangan,nm_ruangan||' ('||kapasitas_ruangan||')' as ruang from ruangan
				left join gedung on gedung.id_gedung=ruangan.id_gedung
				left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
				where gedung.id_fakultas='".$kdfak."' order by nm_ruangan");
		}

		$smarty->assign('T_RUANG', $ruang);

		$hari = getData("SELECT id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
		$smarty->assign('T_HARI', $hari);

		$jam = getData("SELECT id_jadwal_jam,nm_jadwal_jam from jadwal_jam where id_fakultas='".$kdfak."'");
		$smarty->assign('T_JAM', $jam);


		$jaf = getData(
			"SELECT distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
				kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,
				jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,kelas_mk.id_semester,
				ruangan.id_ruangan,nm_ruangan,no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,
				tahun_kurikulum
			from kelas_mk
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
			left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
			left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
			where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_kelas_mk=$id_klsmk and rownum=1");
		$smarty->assign('TJAF2', $jaf);
        break;

	case 'penawaran':

		$smt = $_GET['smt'];
		$kur = $_GET['kur'];

		if(!isset($kdprodi) || empty($kdprodi)) $kdprodi = 0;
		if(!isset($kur) || empty($kur)) $kur = 0;
		
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','none');
				

		if ($_SERVER['REQUEST_METHOD']  == 'POST')
		{
			$kdprodi	= $_POST['id_prodi'];
			$kur		= $_POST['kur'];
			$smt1		= $_POST['smt'];
		}

		/**
		$tawar = getData(
			"SELECT id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_tatap_muka,kredit_praktikum,
				kmk.kredit_semester, nm_status_mk,nm_kelompok_mk, tahun_kurikulum,kmk.keterangan_kur_mk,
				tingkat_semester, kmk.id_kurikulum_prodi
			from kurikulum_mk kmk
			left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join status_mk on kmk.id_status_mk=status_mk.id_status_mk
			left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk
			left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
			where kmk.id_program_studi='{$kdprodi}' and kurikulum_prodi.id_kurikulum_prodi='{$kur}' 
			and id_kurikulum_mk not in (select id_kurikulum_mk from kelas_mk where id_program_studi='{$kdprodi}' and id_semester='{$smt1}')");
		*/

		$tawar = getData(
			"SELECT 
			    kmk.id_kurikulum_mk, mk.kd_mata_kuliah, mk.nm_mata_kuliah, kmk.kredit_semester, kmk.tingkat_semester, kur.nm_kurikulum, kur.thn_kurikulum,
			    status.nm_status_mk
			FROM kurikulum_mk kmk
			JOIN kurikulum kur ON kur.id_kurikulum = kmk.id_kurikulum
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
			LEFT JOIN status_mk status ON status.id_status_mk = kmk.id_status_mk
			WHERE 
			    kur.id_program_studi = '{$kdprodi}' AND kur.status_aktif = 1 AND
			    kmk.id_kurikulum_mk not in (select id_kurikulum_mk from kelas_mk where id_program_studi = '{$kdprodi}' and id_semester = '{$smt1}')
			ORDER BY nm_mata_kuliah");

		$smarty->assign('id_smt',$smt1);
		$smarty->assign('TAWAR', $tawar);
		
		break;


	case 'update1':

		$kap_kelas		= $_POST['kap_kelas'];
		$kelas_mk		= $_POST['kelas_mk'];
		$ren_kul		= $_POST['ren_kul'];
		$id_kelas_mk	= $_POST['id_kelas_mk'];
		$id_hari		= $_POST['hari'];
		$id_jam			= $_POST['jam'];
		//$smt1			= $_POST['smtb'];
		$idruang		= $_POST['ruangan'];

		//if (!isFindData("jadwal_kelas","id_kelas_mk=$id_kelas_mk")) {tambahdata("jadwal_kelas","id_kelas_mk","$id_kelas_mk");}
		UpdateData("UPDATE kelas_mk set kapasitas_kelas_mk=$kap_kelas, jumlah_pertemuan_kelas_mk=$ren_kul,no_kelas_mk='$kelas_mk' where id_kelas_mk=$id_kelas_mk");

		//khusus FST
		$id_jadwal_jam_mulai=getvar("SELECT * from jadwal_jam where id_fakultas='8' 
				and jam_mulai = (select jam_mulai from jadwal_jam where id_jadwal_jam = '$id_jam')
				and menit_mulai = (select menit_mulai from jadwal_jam where id_jadwal_jam = '$id_jam')");
		$id_jadwal_jam_mulai=$id_jadwal_jam_mulai['ID_JADWAL_JAM'];
		
		$id_jadwal_jam_selesai=getvar("SELECT * from jadwal_jam where id_fakultas='8' 
				and jam_selesai = (select jam_selesai from jadwal_jam where id_jadwal_jam = '$id_jam')
				and menit_selesai = (select menit_selesai from jadwal_jam where id_jadwal_jam = '$id_jam')");
		$id_jadwal_jam_selesai=$id_jadwal_jam_selesai['ID_JADWAL_JAM'];
				
		
		if ( ! isFindData("jadwal_kelas","id_kelas_mk=$id_kelas_mk")) 
		{
			tambahdata("jadwal_kelas","id_kelas_mk","$id_kelas_mk");
			
			if ($kdprodi == 228)
			{
				UpdateData("UPDATE jadwal_kelas set id_jadwal_jam=$id_jam, id_jadwal_hari=$id_hari,id_ruangan='$idruang', id_jadwal_jam_mulai='$id_jadwal_jam_mulai',id_jadwal_jam_selesai = '$id_jadwal_jam_selesai'  where id_kelas_mk=$id_kelas_mk");
			}
			else
			{
				UpdateData("UPDATE jadwal_kelas set id_jadwal_jam=$id_jam, id_jadwal_hari=$id_hari,id_ruangan='$idruang' where id_kelas_mk=$id_kelas_mk");
			}
		}
		else 
		{
			if ($kdprodi == 228)
			{
				UpdateData("UPDATE jadwal_kelas set id_jadwal_jam=$id_jam, id_jadwal_hari=$id_hari,id_ruangan='$idruang', id_jadwal_jam_mulai='$id_jadwal_jam_mulai',id_jadwal_jam_selesai = '$id_jadwal_jam_selesai' where id_kelas_mk=$id_kelas_mk");
			}
			else
			{
				UpdateData("UPDATE jadwal_kelas set id_jadwal_jam=$id_jam, id_jadwal_hari=$id_hari,id_ruangan='$idruang' where id_kelas_mk=$id_kelas_mk");
			}
		}

		echo '<script>location.href="#aktivitas-uma!usulan-mk.php?smt='.$smt1.'&action=tampil";</script>'; exit();
		break;
}

if ($kdfak == 6)
{
	$jaf = getData(
		"SELECT distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
			kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester,
			jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, a.nm_jadwal_jam,
			a.jam_mulai||':'||a.menit_mulai||'-'||b.jam_selesai||':'||b.menit_selesai as jam,b.nm_jadwal_jam,
			ruangan.id_ruangan,nm_ruangan,
			case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
			no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,
			tahun_kurikulum
		from kelas_mk
		left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
		left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
		left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
		left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
		left join semester on kelas_mk.id_semester=semester.id_semester
		left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
		left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
		left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
		left join jadwal_jam a on jadwal_kelas.id_jadwal_jam=a.id_jadwal_jam
		left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
		left join jadwal_jam b on a.kdjam+KURIKULUM_MK.kredit_semester-1=b.kdjam and b.id_fakultas=$kdfak
		where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_semester=$smt1");

}
else 
{
	$jaf = getData(
		"SELECT distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
			kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester,
			coalesce(jadwal_kelas.id_jenis_pertemuan,0) as id_jenis_pertemuan,
			jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
			jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
			ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
			no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,
			kurikulum_mk.tingkat_semester
		from kelas_mk
		left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
		left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
		left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
		left join semester on kelas_mk.id_semester=semester.id_semester
		left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
		left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
		left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
		left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
		left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
		where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_semester='{$smt1}' ORDER BY nm_mata_kuliah");
}

$smarty->assign('T_MK', $jaf);

$smarty->display('usulan-mk.tpl');
}