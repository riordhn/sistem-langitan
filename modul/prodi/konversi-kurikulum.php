<?php
/*
YAH 06/06/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

//print_r($user);

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');

$kdprodi= $user->ID_PROGRAM_STUDI; 
$thkur='';

$datakur=getData("select id_kurikulum,nm_kurikulum||'-'||thn_kurikulum as nama  from kurikulum
					where id_program_studi=$kdprodi");

$smarty->assign('T_KUR', $datakur);


$mkbaru=getData("select a1.id_kurikulum_mk,a2.kd_mata_kuliah ||'/'||a2.nm_mata_kuliah||'/'||
a1.kredit_semester||'/'|| a3.thn_kurikulum as mkbaru
from kurikulum_mk a1
left join mata_kuliah a2 on a1.id_mata_kuliah=a2.id_mata_kuliah
left join kurikulum a3 on a1.id_kurikulum =a3.id_kurikulum
where a1.id_program_studi=$kdprodi order by a3.thn_kurikulum desc");
		$smarty->assign('MKBARU', $mkbaru);
		
		$mklama=getData("select a1.id_kurikulum_mk,a2.kd_mata_kuliah ||'/'||a2.nm_mata_kuliah||'/'||
a1.kredit_semester||'/'|| a3.thn_kurikulum as mklama
from kurikulum_mk a1
left join mata_kuliah a2 on a1.id_mata_kuliah=a2.id_mata_kuliah
left join kurikulum a3 on a1.id_kurikulum=a3.id_kurikulum
where a1.id_program_studi=$kdprodi order by a3.thn_kurikulum desc");
		$smarty->assign('MKLAMA', $mklama);
//echo $_POST['kdprodi'];
//echo $_POST['thkur'];
//echo "aa";

$status1 = isset($_REQUEST['kirim']) ? $_REQUEST['kirim'] : 'tampil';
//echo $status;

switch($status1) {
    case 'updateview':
		$id_konv= $_GET['id_kon'];
		
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');

        $konv=getData("select kk.id_kurikulum_mk_baru,a2.kd_mata_kuliah as kodebaru,a2.nm_mata_kuliah as namabaru,id_kurikulum_mk_lama,
a1.kredit_semester as sksbaru, a3.thn_kurikulum as thbaru,b2.kd_mata_kuliah as kodelama,
b2.nm_mata_kuliah as namalama,b1.kredit_semester as skslama,b3.thn_kurikulum as thlama,kk.id_konversi_kurikulum
from konversi_kurikulum kk
left join kurikulum_mk a1 on kk.id_kurikulum_mk_baru=a1.id_kurikulum_mk
left join mata_kuliah a2 on a1.id_mata_kuliah=a2.id_mata_kuliah
left join kurikulum a3 on a1.id_kurikulum=a3.id_kurikulum
left join kurikulum_mk b1 on kk.id_kurikulum_mk_lama=b1.id_kurikulum_mk
left join mata_kuliah b2 on b1.id_mata_kuliah=b2.id_mata_kuliah
left join kurikulum b3 on b1.id_kurikulum=b3.id_kurikulum
where kk.id_konversi_kurikulum=$id_konv");
		$smarty->assign('T_KONV1', $konv);
        $smarty->display('konversi-kurikulum.tpl'); 
        break;   
   
    case 'tampil':
        // viewing kurikulum
		//echo $kdfak;
		$thkur = isSet($_POST['thkur']) ? $_POST['thkur'] : '0';
		if ($thkur !='') {
		$konv=getData("select kk.id_kurikulum_mk_baru,a2.kd_mata_kuliah as kodebaru,a2.nm_mata_kuliah as namabaru,id_kurikulum_mk_lama,
a1.kredit_semester as sksbaru, a3.thn_kurikulum as thbaru,b2.kd_mata_kuliah as kodelama,
b2.nm_mata_kuliah as namalama,b1.kredit_semester as skslama,b3.thn_kurikulum as thlama,kk.id_konversi_kurikulum
from konversi_kurikulum kk
left join kurikulum_mk a1 on kk.id_kurikulum_mk_baru=a1.id_kurikulum_mk
left join mata_kuliah a2 on a1.id_mata_kuliah=a2.id_mata_kuliah
left join kurikulum a3 on a1.id_kurikulum=a3.id_kurikulum
left join kurikulum_mk b1 on kk.id_kurikulum_mk_lama=b1.id_kurikulum_mk
left join mata_kuliah b2 on b1.id_mata_kuliah=b2.id_mata_kuliah
left join kurikulum b3 on b1.id_kurikulum=b3.id_kurikulum
where a1.id_program_studi=$kdprodi and a1.id_kurikulum=$thkur");
		$smarty->assign('T_KONV', $konv);}
        $smarty->display('konversi-kurikulum.tpl');     
        break;
	case 'add':
        // viewing kurikulum
		//echo $kdfak;
		$mklama= $_POST['mklama'];
		$mkbaru= $_POST['mkbaru'];

		$thkur = getvar("select a3.id_kurikulum from kurikulum_mk a1 
left join mata_kuliah a2 on a1.id_mata_kuliah=a2.id_mata_kuliah 
left join kurikulum a3 on a1.id_kurikulum=a3.id_kurikulum
where id_program_studi=$kdprodi and id_kurikulum_mk=$mkbaru");
		
		
		if ($mklama != '' && $mkbaru !='') {
		tambahdata("konversi_kurikulum","id_kurikulum_mk_baru,id_kurikulum_mk_lama","'$mklama','$mkbaru'"); }
		
		if ($thkur['ID_KURIKULUM'] !='') {
		$konv=getData("select kk.id_kurikulum_mk_baru,a2.kd_mata_kuliah as kodebaru,a2.nm_mata_kuliah as namabaru,id_kurikulum_mk_lama,
a1.kredit_semester as sksbaru, a3.thn_kurikulum as thbaru,b2.kd_mata_kuliah as kodelama,
b2.nm_mata_kuliah as namalama,b1.kredit_semester as skslama,b3.thn_kurikulum as thlama,kk.id_konversi_kurikulum
from konversi_kurikulum kk
left join kurikulum_mk a1 on kk.id_kurikulum_mk_baru=a1.id_kurikulum_mk
left join mata_kuliah a2 on a1.id_mata_kuliah=a2.id_mata_kuliah
left join kurikulum a3 on a1.id_kurikulum=a3.id_kurikulum
left join kurikulum_mk b1 on kk.id_kurikulum_mk_lama=b1.id_kurikulum_mk
left join mata_kuliah b2 on b1.id_mata_kuliah=b2.id_mata_kuliah
left join kurikulum b3 on b1.id_kurikulum=b3.id_kurikulum
where a1.id_program_studi=$kdprodi and a1.id_kurikulum='".$thkur['ID_KURIKULUM']."' ");
		$smarty->assign('T_KONV', $konv);}
        $smarty->display('konversi-kurikulum.tpl');     
        break;
		
	case 'del':
        // viewing kurikulum
		//echo $kdfak;
		$id_konv= $_GET['id_kon'];
		$mkbaru= $_GET['mk'];

		$thkur = getvar("select a3.id_kurikulum from kurikulum_mk a1 
left join mata_kuliah a2 on a1.id_mata_kuliah=a2.id_mata_kuliah 
left join kurikulum a3 on a1.id_kurikulum=a3.id_kurikulum
where id_program_studi=$kdprodi and a2.kd_mata_kuliah='$mkbaru'");
		
		deleteData("delete from konversi_kurikulum where id_konversi_kurikulum=$id_konv");
					
		if ($thkur['ID_KURIKULUM'] !='') {
		$konv=getData("select kk.id_kurikulum_mk_baru,a2.kd_mata_kuliah as kodebaru,a2.nm_mata_kuliah as namabaru,id_kurikulum_mk_lama,
a1.kredit_semester as sksbaru, a3.thn_kurikulum as thbaru,b2.kd_mata_kuliah as kodelama,
b2.nm_mata_kuliah as namalama,b1.kredit_semester as skslama,b3.thn_kurikulum as thlama,kk.id_konversi_kurikulum
from konversi_kurikulum kk
left join kurikulum_mk a1 on kk.id_kurikulum_mk_baru=a1.id_kurikulum_mk
left join mata_kuliah a2 on a1.id_mata_kuliah=a2.id_mata_kuliah
left join kurikulum a3 on a1.id_kurikulum=a3.id_kurikulum
left join kurikulum_mk b1 on kk.id_kurikulum_mk_lama=b1.id_kurikulum_mk
left join mata_kuliah b2 on b1.id_mata_kuliah=b2.id_mata_kuliah
left join kurikulum b3 on b1.id_kurikulum=b3.id_kurikulum
where a1.id_program_studi=$kdprodi and a1.id_kurikulum='".$thkur['ID_KURIKULUM']."' ");
		$smarty->assign('T_KONV', $konv);}
        $smarty->display('konversi-kurikulum.tpl');     
        break;
		
	case 'ganti':
        // viewing kurikulum
		//echo $kdfak;
		$id_konv= $_POST['idkonv'];
		$mkbaru= $_POST['mkbaru'];
		$mklama= $_POST['mklama'];

		$thkur = getvar("select a3.id_kurikulum from konversi_kurikulum c
left join kurikulum_mk a1 on c.id_kurikulum_mk_baru=a1.id_kurikulum_mk
left join mata_kuliah a2 on a1.id_mata_kuliah=a2.id_mata_kuliah 
left join kurikulum a3 on a1.id_kurikulum=a3.id_kurikulum
where a1.id_program_studi=$kdprodi and a1.id_kurikulum_mk='$mkbaru'");
		
		if ($id_konv != '' && $mkbaru !='' && $mklama != ''){
		gantidata("update konversi_kurikulum set id_kurikulum_mk_baru='$mkbaru', id_kurikulum_mk_lama='$mklama' where id_konversi_kurikulum=$id_konv");}
					
		if ($thkur['ID_KURIKULUM'] !='') {
		$konv=getData("select kk.id_kurikulum_mk_baru,a2.kd_mata_kuliah as kodebaru,a2.nm_mata_kuliah as namabaru,id_kurikulum_mk_lama,
a1.kredit_semester as sksbaru, a3.thn_kurikulum as thbaru,b2.kd_mata_kuliah as kodelama,
b2.nm_mata_kuliah as namalama,b1.kredit_semester as skslama,b3.thn_kurikulum as thlama,kk.id_konversi_kurikulum
from konversi_kurikulum kk
left join kurikulum_mk a1 on kk.id_kurikulum_mk_baru=a1.id_kurikulum_mk
left join mata_kuliah a2 on a1.id_mata_kuliah=a2.id_mata_kuliah
left join kurikulum a3 on a1.id_kurikulum=a3.id_kurikulum
left join kurikulum_mk b1 on kk.id_kurikulum_mk_lama=b1.id_kurikulum_mk
left join mata_kuliah b2 on b1.id_mata_kuliah=b2.id_mata_kuliah
left join kurikulum b3 on b1.id_kurikulum=b3.id_kurikulum
where a1.id_program_studi=$kdprodi and a1.id_kurikulum='".$thkur['ID_KURIKULUM']."' ");
		$smarty->assign('T_KONV', $konv);}
        $smarty->display('konversi-kurikulum.tpl');     
        break;
	
}

?>
