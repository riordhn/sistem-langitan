<?php
include '../../config.php';

$akademik = new Akademik($db);

$mode = empty($_GET['mode']) ? 'list' : $_GET['mode'];

if ($mode == 'list')
{
	$semester_aktif	= $PT->get_semester_aktif();

	$id_semester	= empty($_GET['id_semester']) ? $semester_aktif['ID_SEMESTER'] : (int)$_GET['id_semester'];

	$semester_set	= $PT->list_semester();
	$kelas_set		= $akademik->list_kelas_per_prodi($user->ID_PROGRAM_STUDI, $id_semester);

	// Assignment ke template
	$smarty->assign('semester_set', $semester_set);
	$smarty->assign('id_semester_terpilih', $id_semester);
	$smarty->assign('kelas_set', $kelas_set);
}

if ($mode == 'detail')
{
	$id_kelas_mk	= empty($_GET['id_kelas_mk']) ? 0 : (int)$_GET['id_kelas_mk'];
	$id_semester	= empty($_GET['id_semester']) ? 0 : (int)$_GET['id_semester'];

	$kelas = $akademik->get_kelas($id_kelas_mk);
	$mahasiswa_set = $akademik->list_mahasiswa_of_kelas($id_kelas_mk);

	$smarty->assign('kelas', $kelas);
	$smarty->assign('data_set', $mahasiswa_set);
}

$smarty->assign('mode', $mode);

$smarty->display('monitoring/krs-kelas.tpl');