<?php
require('common.php');
require_once ('ociFunction.php');
$kd_fak= $user->ID_FAKULTAS;
$kd_prodi=$user->ID_PROGRAM_STUDI;
$id_pengguna= $user->ID_PENGGUNA; 


/*
echo '<script>alert("Maaf, Fitur Ini di non aktifkan.. Hubungi Data Controler")</script>';
*/

if ($kd_fak==1)
{
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");

if (($_GET['action'])=='viewproses') {
	$smarty->assign('disp1','none');
	$smarty->assign('disp2','block');

	$id_pengambilan_mk =$_GET['id_pmk'];
	$id_log =$_GET['id_log'];
	$smarty->assign('log',$id_log);
	
	$datamk=getvar("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nilai_huruf,id_mhs,
					pengambilan_mk.id_semester,id_pengambilan_mk,pengambilan_mk.id_kurikulum_mk,
					thn_akademik_semester||'/'||group_semester||'/'||nm_semester as smt
					from pengambilan_mk
					left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join semester on pengambilan_mk.id_semester=semester.id_semester
					where id_pengambilan_mk=$id_pengambilan_mk");
	$smarty->assign('datamk', $datamk);
}

if (($_GET['action'])=='delete') {

	$id_pengambilan_mk =$_GET['id_pmk'];
	$ip = $_SERVER['REMOTE_ADDR'];
	
	InsertData("insert into log_delete_peng_mk
				select * from pengambilan_mk where id_pengambilan_mk=$id_pengambilan_mk") or die('gagal 1');
	
	$cek=getvar("select count(*) as ada from log_delete_peng_mk where id_pengambilan_mk=$id_pengambilan_mk");
	if ($cek['ADA']>0)
	{
	UpdateData("update log_delete_peng_mk set id_pengguna='$id_pengguna',ip_address='$ip'
				where id_pengambilan_mk=$id_pengambilan_mk");
	UpdateData("delete from pengambilan_mk where id_pengambilan_mk=$id_pengambilan_mk");
	}

}

if(($_POST['action'])=='proses'){
	
	if(strtoupper($_POST['nilai']) != 'A' and strtoupper($_POST['nilai']) != 'AB' and strtoupper($_POST['nilai']) != 'B' and strtoupper($_POST['nilai']) != 'BC' and strtoupper($_POST['nilai']) != 'C' and strtoupper($_POST['nilai']) != 'D' and strtoupper($_POST['nilai']) != 'E'){
	echo '<script>alert("Masukkan Nilai Huruf [A/AB/B/BC/C/D/E]")</script>';
	}else{
	$ip = $_SERVER['REMOTE_ADDR'];
	$id_pengambilan_mk =$_POST['id_pengambilan_mk'];
	$id_log =$_POST['log'];
	
	
	//log perubahan nilai
	InsertData("insert into log_update_nilai (id_pengguna,referensi,id_pengambilan_mk,ip_address,tgl_update,nilai_huruf_lama,nilai_huruf_baru)
		select '$id_pengguna','UPDATE NILAI KHUSUS',id_pengambilan_mk,'$ip', sysdate, nilai_huruf, upper('$_POST[nilai]')
		from pengambilan_mk where id_pengambilan_mk=$id_pengambilan_mk") or die('gagal 1');
	
	//cek proses
	$datacek=getvar("select count(*) as ada from log_update_nilai
					where id_pengguna=$id_pengguna and id_pengambilan_mk=$id_pengambilan_mk
					and nilai_huruf_baru=upper('$_POST[nilai]')");

	if ($datacek[ADA]>0)
	{
	//nilai di ubah
	UpdateData("update pengambilan_mk set nilai_huruf=upper('$_POST[nilai]'), STATUS_APV_PENGAMBILAN_MK = 1, FLAGNILAI = 1
 where id_pengambilan_mk=$id_pengambilan_mk")
	//closed log_granted_nilai
	or die ('<script>alert("Proses Gagal.. Mhn diCek..")</script>') ;
	echo '<script>alert("Proses Edit Nilai Telah Berhasil")</script>';
	} else
	{
	echo '<script>alert("Proses Edit Nilai GAGAL")</script>';
	}
	}
}

$nim =$_REQUEST['nim'];
$nama=getvar("select nm_pengguna from pengguna where username='$nim'");

$smarty->assign('nama', $nama['NM_PENGGUNA']);
$smarty->assign('nim', $nim);

$nilai=getData("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nilai_huruf,
					pengambilan_mk.id_pengambilan_mk,mahasiswa.id_program_studi,pengambilan_mk.id_kurikulum_mk,
					thn_akademik_semester||'/'||group_semester||'/'||nm_semester as smt,1 as status
					from pengambilan_mk
					left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join semester on pengambilan_mk.id_semester=semester.id_semester
					left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
					where nim_mhs='$nim' 
					and mahasiswa.id_program_studi=$kd_prodi and pengambilan_mk.id_semester !='".$smtaktif['ID_SEMESTER']."'
					order by thn_akademik_semester,group_semester,kd_mata_kuliah");


					
	
$smarty->assign('T_NILAI', $nilai);

$smarty->display('edit-nilai-fk.tpl');
}
else
{
	echo '<script>alert("Maaf, Ini Hanya untuk Fakultas Kedokteran")</script>';
}

?>
