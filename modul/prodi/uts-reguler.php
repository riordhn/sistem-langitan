<?php
//include 'config.php';
require('common.php');
require_once ('ociFunction.php');

$mode = get('mode', 'view');


$id_pt = $id_pt_user;


// Semester Aktif
$db->Query("select * from semester where status_aktif_semester='True' and id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}");
$semester = $db->FetchAssoc();
$smarty->assign('semester', $semester);

$id_smt = $semester['ID_SEMESTER'];
$smarty->assign('SMT', $id_smt);

$kegiatanUTS = getvar("select id_kegiatan from kegiatan where id_perguruan_tinggi = '{$id_pt}' and kode_kegiatan = 'UTS'");
$id_kegiatan = $kegiatanUTS['ID_KEGIATAN'];
$smarty->assign('KEG', $id_kegiatan);

$kdfak=$user->ID_FAKULTAS;
$id_prodi_user=$user->ID_PROGRAM_STUDI;

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if (post('mode') == 'add')
	{
		/*print_r(post('id'));
		exit();*/

		
		//$id_kelas_mk = "WHERE IN (".implode(',',post('id_kelas_mk')).")";
		// echo $id_kelas_mk;

		$id_gabungan_set = post('id_gabungan');

		foreach ($id_gabungan_set as $id_gabungan) {
			# code...
			$id_kelas_jadwal = explode("-", $id_gabungan);

			$id_kelas_mk = $id_kelas_jadwal[0];
			$id_jadwal_kelas = $id_kelas_jadwal[1];

			//$id_kelas_mk = $_GET['kls'];

			$jadwal = getvar("
				select kelas_mk.id_kelas_mk,jam_mulai||':'||menit_mulai as jam_mulai, jam_selesai||':'||menit_selesai as jam_selesai
				from kelas_mk
				left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
				left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
				where kelas_mk.id_kelas_mk=$id_kelas_mk and rownum=1
				");
			$jam_mulai = $jadwal['JAM_MULAI'];
			$jam_selesai = $jadwal['JAM_SELESAI'];

			$cek = getvar("select count(*) as ada from ujian_mk where id_kelas_mk=$id_kelas_mk and id_kegiatan=(select id_kegiatan from kegiatan where id_perguruan_tinggi = '{$id_pt}' and kode_kegiatan = 'UTS')");
			$ada = $cek['ADA'];

			if ($ada == 0)
			{
				$id_ujian = getvar("
					select ujian_mk_seq.nextval as id_ujian_mk
					from dual");
				
				$id_ujian_mk = $id_ujian['ID_UJIAN_MK'];

				tambahdata("ujian_mk", "id_ujian_mk,id_kegiatan,id_kelas_mk,jam_mulai,jam_selesai,id_fakultas,id_semester", "$id_ujian_mk,(select id_kegiatan from kegiatan where id_perguruan_tinggi = '{$id_pt}' and kode_kegiatan = 'UTS'),$id_kelas_mk,'$jam_mulai','$jam_selesai',$kdfak,$id_smt");

				//$id_jadwal_kelas = $_GET['jad'];
				$ruang = getvar("select id_ruangan from jadwal_kelas where id_jadwal_kelas=$id_jadwal_kelas and rownum=1");
				$id_ruangan = $ruang['ID_RUANGAN'];

				tambahdata("jadwal_ujian_mk", "id_ujian_mk,id_ruangan", "$id_ujian_mk,$id_ruangan");
			}


		}

	}
	
	if (post('mode') == 'plot-otomatis')
	{
		$id_ujian_mk = post('id_ujian_mk');

		$db->Query("select id_kelas_mk from ujian_mk umk where id_ujian_mk='{$id_ujian_mk}'");
		$kelas_mk = $db->FetchAssoc();
		$id_kelas_mk = $kelas_mk['ID_KELAS_MK'];

		$kpst = "select rg.kapasitas_ujian from jadwal_ujian_mk jad left join ruangan rg on rg.id_ruangan=jad.id_ruangan where jad.id_ujian_mk=$id_ujian_mk";

		$result = $db->Query($kpst) or die("salah kueri kpst ");

		while ($r = $db->FetchRow())
		{
			$limit = $r[0];
			
		}

		$add_pst = $db->QueryToArray("
			select pmk.id_mhs from pengambilan_mk pmk 
					left join mahasiswa mhs on mhs.id_mhs=pmk.id_mhs
					left join kelas_mk kmk on kmk.id_kelas_mk=pmk.id_kelas_mk
					left join nama_kelas kls on kmk.no_kelas_mk=kls.id_nama_kelas
					where pmk.id_semester=$id_smt and pmk.id_kelas_mk=$id_kelas_mk and pmk.status_apv_pengambilan_mk = '1'
					--and rownum<=$limit
					order by kls.nama_kelas, mhs.nim_mhs
					");
		foreach ($add_pst as $r) {
			$id_mhs = $r['ID_MHS'];

			tambahdata("ujian_mk_peserta", "id_ujian_mk,id_mhs", "$id_ujian_mk,$id_mhs");
		}

		/*$result = $db->Query($add_pst) or die("salah kueri proses add_pst ");
		while ($r = $db->FetchRow())
		{
			$id_mhs = $r[0];

			tambahdata("ujian_mk_peserta", "id_ujian_mk,id_mhs", "$id_ujian_mk,$id_mhs");
		}*/
	}
	
	if (post('mode') == 'reset-peserta')
	{
		$id_ujian_mk = post('id_ujian_mk');

		$db->Query("delete from ujian_mk_peserta where id_ujian_mk = '{$id_ujian_mk}'");
	}
	
	if (post('mode') == 'add-ruang')
	{
		$id_kelas_mk = post('id_kelas_mk');
		$id_ujian_mk = post('id_ujian_mk');

		$jadwal = getvar("select nm_ujian_mk,tgl_ujian,jam_mulai,jam_selesai from ujian_mk where id_ujian_mk=$id_ujian_mk");

		$nm_ujian_mk = $jadwal['NM_UJIAN_MK'];
		$tgl_ujian = $jadwal['TGL_UJIAN'];
		$jam_mulai = $jadwal['JAM_MULAI'];
		$jam_selesai = $jadwal['JAM_SELESAI'];

		tambahdata("ujian_mk", "id_kegiatan,id_kelas_mk,nm_ujian_mk,tgl_ujian,jam_mulai,jam_selesai,id_fakultas,id_semester", "(select id_kegiatan from kegiatan where id_perguruan_tinggi = '{$id_pt}' and kode_kegiatan = 'UTS'),$id_kelas_mk,'$nm_ujian_mk','$tgl_ujian','$jam_mulai','$jam_selesai',$kdfak,$id_smt");	
	}
	
	if (post('mode') == 'delete')
	{
		$id_ujian_mk = $_POST['id_ujian_mk'];

		$del = getvar("select id_jadwal_ujian_mk from jadwal_ujian_mk where id_ujian_mk=$id_ujian_mk");
		$id_jadwal_ujian_mk = $del['ID_JADWAL_UJIAN_MK'];

		deleteData("delete from ujian_mk_peserta where id_ujian_mk=$id_ujian_mk");
		deleteData("delete from tim_pengawas_ujian where id_jadwal_ujian_mk=$id_jadwal_ujian_mk");
		deleteData("delete from jadwal_ujian_mk where id_ujian_mk=$id_ujian_mk");
		deleteData("delete from ujian_mk where id_ujian_mk=$id_ujian_mk");
	}

	if (post('mode') == 'update')
	{
		$id_ujian_mk = $_POST['id_ujian_mk'];
		$id_jadwal_ujian_mk = $_POST['id_jadwal_ujian_mk'];
		$nm_ujian_mk = $_POST['nm_ujian_mk'];
		$tgl_ujian = "to_date('".$_POST['tgl_ujian']."', 'DD-MM-YYYY')";
		$mulai = $_POST['jam_mulai'];
		$selesai = $_POST['jam_selesai'];
		$id_ruangan = $_POST['id_ruangan'];

		$jam_mulai = substr($mulai, 0, 2) . ':' . substr($mulai, -2, 2);
		$jam_selesai = substr($selesai, 0, 2) . ':' . substr($selesai, -2, 2);

		$cek = getvar("select count(*) as ada from jadwal_ujian_mk where id_ujian_mk=$id_ujian_mk");
		$ada = $cek['ADA'];

		if ($ada == 0)
		{
			gantidata("update ujian_mk set nm_ujian_mk=upper('" . $nm_ujian_mk . "'), tgl_ujian=$tgl_ujian, jam_mulai='$jam_mulai', jam_selesai='$jam_selesai' where id_ujian_mk=$id_ujian_mk");
			tambahdata("jadwal_ujian_mk", "id_ujian_mk,id_ruangan", "$id_ujian_mk,$id_ruangan");
		}
		else
		{
			gantidata("update ujian_mk set nm_ujian_mk=upper('" . $nm_ujian_mk . "'), tgl_ujian=$tgl_ujian, jam_mulai='$jam_mulai', jam_selesai='$jam_selesai' where id_ujian_mk=$id_ujian_mk");
			gantidata("update jadwal_ujian_mk set id_ruangan=$id_ruangan where id_jadwal_ujian_mk=$id_jadwal_ujian_mk");
		}
	}
}

if ($mode == 'view')
{

	// Query Lama
	$old_sql = "SELECT * from
			(
			(select umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,mk.kredit_semester,umk.nm_ujian_mk,to_char(umk.tgl_ujian,'DD-MM-YYYY') as tgl_ujian,
			umk.jam_mulai||' - '||umk.jam_selesai as jam,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang||'-'||ps.nm_singkat_prodi as prodi,
			listagg(case when tim.status='1' then trim('<font color=\"blue\"><b>'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</b></font>')
			else trim('<font color=\"black\">'||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font>') end, '<li>') within group (order by nm_pengguna) as pengawas,
			nmk.nama_kelas
			from ujian_mk umk
			left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
			left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
			--left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
			left join mata_kuliah mk on kmk.id_mata_kuliah=mk.id_mata_kuliah
			left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
			left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
			left join jadwal_ujian_mk jad on jad.id_ujian_mk=umk.id_ujian_mk
			left join ruangan rg on rg.id_ruangan=jad.id_ruangan
			left join tim_pengawas_ujian tim on jad.id_jadwal_ujian_mk=tim.id_jadwal_ujian_mk
			left join pengguna pgg on pgg.id_pengguna=tim.id_pengguna
			where umk.id_kegiatan=(select id_kegiatan from kegiatan where id_perguruan_tinggi = '{$id_pt}' and kode_kegiatan = 'UTS') and ps.id_program_studi='{$id_prodi_user}' and umk.id_semester=$id_smt
			group by umk.id_kelas_mk,umk.id_ujian_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,mk.kredit_semester,umk.nm_ujian_mk,umk.tgl_ujian,
			umk.jam_mulai,umk.jam_selesai,rg.nm_ruangan,rg.kapasitas_ujian,jjg.nm_jenjang,ps.nm_singkat_prodi,nmk.nama_kelas) a
			left join
			(select id_ujian_mk, count(*) as peserta from ujian_mk_peserta group by id_ujian_mk) b
			on a.id_ujian_mk=b.id_ujian_mk
			)";
	
	// Query Baru : 13-Okt-2018
	$sql = 
		"SELECT 
			umk.ID_UJIAN_MK, umk.ID_KELAS_MK, kls.ID_KURIKULUM_MK, 
			mk.KD_MATA_KULIAH, mk.NM_MATA_KULIAH, mk.KREDIT_SEMESTER, kur.TINGKAT_SEMESTER, nk.NAMA_KELAS,
			umk.NM_UJIAN_MK, to_char(umk.TGL_UJIAN, 'YYYY-MM-DD') tgl_ujian, 
			umk.JAM_MULAI||' - '||umk.JAM_SELESAI AS jam, 
			r.NM_RUANGAN, r.KAPASITAS_UJIAN,
			'' AS pengawas, -- Perlu diganti jadi link saja
			(SELECT count(*) FROM UJIAN_MK_PESERTA pst WHERE pst.ID_UJIAN_MK = umk.ID_UJIAN_MK) AS peserta
		FROM UJIAN_MK umk
		JOIN KEGIATAN k ON k.ID_KEGIATAN = umk.ID_KEGIATAN
		JOIN KELAS_MK kls ON kls.id_kelas_mk = umk.id_kelas_mk
		JOIN KURIKULUM_MK kur ON kur.ID_KURIKULUM_MK = kls.ID_KURIKULUM_MK
		JOIN MATA_KULIAH mk ON mk.ID_MATA_KULIAH = kur.ID_MATA_KULIAH
		JOIN NAMA_KELAS nk ON nk.ID_NAMA_KELAS = kls.NO_KELAS_MK
		LEFT JOIN JADWAL_UJIAN_MK jdwl ON jdwl.ID_UJIAN_MK = umk.ID_UJIAN_MK
		LEFT JOIN RUANGAN r ON r.ID_RUANGAN = jdwl.ID_RUANGAN
		WHERE
			k.ID_PERGURUAN_TINGGI = {$id_pt} 
			AND k.KODE_KEGIATAN = 'UTS'
			AND	kls.ID_PROGRAM_STUDI = {$id_prodi_user} 
			AND kls.ID_SEMESTER = {$id_smt}
		ORDER BY mk.NM_MATA_KULIAH";

	$data_set = getData($sql);
	
	$smarty->assign('data_set', $data_set);
}
else if ($mode == 'add')
{	
	$data_set = getData("SELECT distinct jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,mata_kuliah.kredit_semester,
			nama_kelas,kapasitas_kelas_mk,nm_ruangan,kapasitas_ujian,nm_jadwal_hari,
			sum(case when status_apv_pengambilan_mk=1 then 1 else 0 end) as peserta,nm_jenjang||'-'||nm_singkat_prodi as prodi
			from kelas_mk
			left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and pengambilan_mk.id_semester=$id_smt
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			--left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join mata_kuliah on kelas_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
			left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
			left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
			where program_studi.id_program_studi='{$id_prodi_user}' and kelas_mk.id_semester=$id_smt 
			--and kurikulum_mk.status_mkta not in (1,2) 
			and mata_kuliah.id_jenis_mk not in ('S')
			and kelas_mk.id_kelas_mk not in (select id_kelas_mk from ujian_mk where id_kegiatan=(select id_kegiatan from kegiatan where id_perguruan_tinggi = '{$id_pt}' and kode_kegiatan = 'UTS') and program_studi.id_program_studi='{$id_prodi_user}' and id_semester=$id_smt)
			group by jadwal_kelas.id_jadwal_kelas,kelas_mk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah, mata_kuliah.kredit_semester, nama_kelas, kapasitas_kelas_mk,
			nm_jadwal_hari, nm_singkat_prodi, nm_jenjang, nm_ruangan, kapasitas_ujian");
	
	$smarty->assign('data_set', $data_set);
}
else if ($mode == 'update')
{
	$id_ujian_mk = get('id_ujian_mk');

	$ujian_mk = getData("
		select umk.id_ujian_mk,jad.id_jadwal_ujian_mk,umk.id_kelas_mk,umk.nm_ujian_mk,umk.tgl_ujian,umk.jam_mulai,umk.jam_selesai,jad.id_ruangan,
		mk.kd_mata_kuliah||' - '||mk.nm_mata_kuliah as nm_mata_kuliah,nmk.nama_kelas,nm_jenjang||'-'||nm_singkat_prodi as prodi, rg.nm_ruangan, 
		--kur.tingkat_semester,
		rg.kapasitas_ujian
		from ujian_mk umk
		left join jadwal_ujian_mk jad on umk.id_ujian_mk=jad.id_ujian_mk
		left join ruangan rg on jad.id_ruangan=rg.id_ruangan
		left join kelas_mk kmk on kmk.id_kelas_mk=umk.id_kelas_mk
		left join nama_kelas nmk on nmk.id_nama_kelas=kmk.no_kelas_mk
		--left join kurikulum_mk kur on kur.id_kurikulum_mk=kmk.id_kurikulum_mk
		left join mata_kuliah mk on kmk.id_mata_kuliah=mk.id_mata_kuliah
		left join program_studi ps on ps.id_program_studi=kmk.id_program_studi
		left join jenjang jjg on jjg.id_jenjang=ps.id_jenjang
		where umk.id_ujian_mk=$id_ujian_mk
		");
	$smarty->assign('UMK', $ujian_mk);

	$ruang = getData("
		select id_ruangan,nm_ruangan||' - '||kapasitas_ujian as ruang from ruangan
		left join gedung on gedung.id_gedung=ruangan.id_gedung
		left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
		where gedung.id_fakultas=$kdfak and ruangan.kapasitas_ujian > 0 order by nm_ruangan
		");
	$smarty->assign('T_RUANG', $ruang);

}

$smarty->display("uts-reguler/{$mode}.tpl");