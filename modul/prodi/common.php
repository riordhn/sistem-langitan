<?php
date_default_timezone_set('Asia/Jakarta');
include 'config.php';

// Root Path
$root_path = isset($root_path) ? $root_path : '';

// Smarty Initialize
$smarty = new Smarty();
$smarty->caching = Smarty::CACHING_OFF;
$smarty->template_dir = $root_path . 'gui';
$smarty->compile_dir = $root_path . 'gui_c';
