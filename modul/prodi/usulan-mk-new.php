<?php
include 'config.php';

$akademik = new Akademik($db);

$action = get('action', 'tampil');

$semester_aktif = $PT->get_semester_aktif();
$smarty->assign('id_semester_aktif', $semester_aktif['ID_SEMESTER']);

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$action = post('action');
	
	if ($action == 'add-kelas')
	{
		$id_kurikulum_mk	= (int)post('id_kurikulum_mk');
		$id_semester		= (int)post('id_semester');
		$id_nama_kelas		= (int)post('id_nama_kelas');
		$kapasitas			= (int)post('kapasitas_kelas_mk');
		$pertemuan			= (int)post('jumlah_pertemuan_kelas_mk');
		
		$id_jadwal_hari		= post('id_jadwal_hari');
		$id_jadwal_jam		= post('id_jadwal_jam');
		$id_ruangan			= post('id_ruangan');
		
		$id_dosen			= post('id_dosen');
		
		// Meng-NULL-kan jika tidak di isi
		$bahasan			= empty(post('bahasan')) ? 'NULL' : "'".str_replace("'", "''", post('bahasan'))."'";
		$tgl_mulai			= empty(post('tgl_mulai')) ? 'NULL' : "to_date('".post('tgl_mulai')."', 'DD-MM-YYYY')";
		$tgl_akhir			= empty(post('tgl_akhir')) ? 'NULL' : "to_date('".post('tgl_akhir')."', 'DD-MM-YYYY')";
		
		// START TRANSACATION
		$db->BeginTransaction();
		
		// Ambil id_mata_kuliah dan id_prodi
		$db->Query(
			"SELECT kmk.id_mata_kuliah, k.id_program_studi FROM kurikulum_mk kmk
			join kurikulum k on k.id_kurikulum = kmk.id_kurikulum where id_kurikulum_mk = {$id_kurikulum_mk}");
		$kmk = $db->FetchAssoc();

		// cek kembar utk ID_MATA_KULIAH, ID_SEMESTER, NO_KELAS_MK (ID_NAMA_KELAS)
		$db->Query(
			"SELECT COUNT(*) AS CEK
			FROM KELAS_MK
			WHERE ID_MATA_KULIAH || ID_SEMESTER || NO_KELAS_MK = '{$kmk['ID_MATA_KULIAH']}' || '{$id_semester}' || '{$id_nama_kelas}' ");
		$cekKembar = $db->FetchAssoc();

		if($cekKembar['CEK'] > 0){
			$result = FALSE;
		}
		else{

			// Ambil ID_KELAS_MK sebagi primari
			$db->Query("SELECT kelas_mk_seq.nextval FROM dual");
			$seq = $db->FetchAssoc();
			$id_kelas_mk = $seq['NEXTVAL'];
			
			// INSERT KELAS_MK
			$result = $db->Query(
				"INSERT INTO kelas_mk (id_kelas_mk, id_program_studi, id_semester, id_kurikulum_mk, id_mata_kuliah, no_kelas_mk, kapasitas_kelas_mk, jumlah_pertemuan_kelas_mk, bahasan, tgl_mulai, tgl_akhir, created_by)
				VALUES ({$id_kelas_mk}, {$kmk['ID_PROGRAM_STUDI']}, {$id_semester}, {$id_kurikulum_mk}, {$kmk['ID_MATA_KULIAH']}, {$id_nama_kelas}, {$kapasitas}, {$pertemuan}, {$bahasan}, {$tgl_mulai}, {$tgl_akhir}, {$user->ID_PENGGUNA})");

		}
			
		if ($result == FALSE)
		{
			$db->Rollback();
			$smarty->assign('message', "Gagal menambahkan kelas (kombinasi semester, matkul, dan nama kelas sudah ada). Periksa kembali isian.");
			$smarty->display('usulan-mk/result.tpl');
			exit();
		}
		// 3 field
		for ($i = 0; $i < 3; $i++)
		{
			// INSERT JADWAL_KELAS (Jika ada hari)
			if ( ! empty($id_jadwal_hari[$i]))
			{
				$jadwal_jam	= empty($id_jadwal_jam[$i]) ? 'NULL' : $id_jadwal_jam[$i];
				$ruangan	= empty($id_ruangan[$i]) ? 'NULL' : $id_ruangan[$i];
				
				$db->Query("INSERT INTO jadwal_kelas (id_kelas_mk, id_jadwal_hari, id_jadwal_jam, id_ruangan) VALUES ({$id_kelas_mk}, {$id_jadwal_hari[$i]}, {$jadwal_jam}, {$ruangan})");
			}
			
			// Jika ada dosen pengampu / dosen tim baru insert
			if ( ! empty($id_dosen[$i]))
			{
				// Untuk urutan pertama merupakan PJMK (1), selebihnya Anggota (2)
				$pjmk_pengampu_mk = ($i == 0) ? '1' : '2'; 
				
				$db->Query("INSERT INTO pengampu_mk (id_kelas_mk, id_dosen, pjmk_pengampu_mk, created_by) VALUES ({$id_kelas_mk}, {$id_dosen[$i]}, {$pjmk_pengampu_mk}, {$user->ID_PENGGUNA})");
			}
		}
		
		// Commit
		$db->Commit();
		
		$smarty->assign('message', "Berhasil menambahkan kelas");
		$smarty->assign('smt', $id_semester);
		$smarty->display('usulan-mk/result.tpl');
		exit();
	}
	
	if ($action == 'update')
	{
		$id_kelas_mk		= (int)post('id_kelas_mk');
		$id_semester		= (int)post('id_semester');
		$id_nama_kelas		= (int)post('id_nama_kelas');
		$kapasitas			= (int)post('kapasitas_kelas_mk');
		$pertemuan			= (int)post('jumlah_pertemuan_kelas_mk');
		
		$id_jadwal_hari		= post('id_jadwal_hari');
		$id_jadwal_jam		= post('id_jadwal_jam');
		$id_ruangan			= post('id_ruangan');
		
		$id_dosen			= post('id_dosen');
		
		// Meng-NULL-kan jika tidak di isi
		$bahasan			= empty(post('bahasan')) ? 'NULL' : "'".str_replace("'", "''", post('bahasan'))."'";
		$tgl_mulai			= empty(post('tgl_mulai')) ? 'NULL' : "to_date('".post('tgl_mulai')."', 'DD-MM-YYYY')";
		$tgl_akhir			= empty(post('tgl_akhir')) ? 'NULL' : "to_date('".post('tgl_akhir')."', 'DD-MM-YYYY')";
		
		// Data Lama untuk membedakan yg INSERT / UPDATE
		$id_jadwal_kelas_old	= post('id_jadwal_kelas_old');
		$id_jadwal_hari_old		= post('id_jadwal_hari_old');
		$id_jadwal_jam_old		= post('id_jadwal_jam_old');
		$id_ruangan_old			= post('id_ruangan_old');
		
		$id_pengampu_mk_old		= post('id_pengampu_mk_old');
		$id_dosen_old			= post('id_dosen_old');

		$status_seri			= post('status_seri');
		$tingkat_semester_seri	= empty(post('tingkat_semester_seri')) ? 'NULL' : "'".post('tingkat_semester_seri')."'";
		
		
		$db->BeginTransaction();
		
		// Update Kelas
		$db->Query("UPDATE kelas_MK SET 
			no_kelas_mk = {$id_nama_kelas}, kapasitas_kelas_mk = {$kapasitas}, jumlah_pertemuan_kelas_mk = {$pertemuan},
			bahasan = {$bahasan}, tgl_mulai = {$tgl_mulai}, tgl_akhir = {$tgl_akhir}, status_seri = {$status_seri}, tingkat_semester_seri = {$tingkat_semester_seri},
			updated_on = sysdate, updated_by = {$user->ID_PENGGUNA} WHERE id_kelas_mk = {$id_kelas_mk}");
			
		// Sampai tiga input
		for ($i = 0; $i < 3; $i++)
		{
			// Jika jadwal hari ada isian
			if ( ! empty($id_jadwal_hari[$i]))
			{
				// Jika jadwal_kelas sudah ada -> UPDATE
				if ( ! empty($id_jadwal_kelas_old[$i]))
				{
					$id_jadwal_jam[$i]	= empty($id_jadwal_jam[$i]) ? 'NULL' : $id_jadwal_jam[$i];
					$id_ruangan[$i]		= empty($id_ruangan[$i]) ? 'NULL' : $id_ruangan[$i];

					$result = $db->Query("UPDATE jadwal_kelas SET id_jadwal_hari = {$id_jadwal_hari[$i]}, id_jadwal_jam = {$id_jadwal_jam[$i]}, id_ruangan = {$id_ruangan[$i]} WHERE id_jadwal_kelas = {$id_jadwal_kelas_old[$i]}");
				}
				// Jika belum ada jadwal_kelas -> INSERT
				else
				{
					$result = $db->Query("INSERT INTO jadwal_kelas (id_kelas_mk, id_jadwal_hari, id_jadwal_jam, id_ruangan) VALUES ({$id_kelas_mk}, {$id_jadwal_hari[$i]}, {$id_jadwal_jam[$i]}, {$id_ruangan[$i]})");
				}
			}
			else
			{
				// Jika jadwal_kelas ada -> DELETE
				if ( ! empty($id_jadwal_kelas_old[$i]))
				{
					$result = $db->Query("DELETE FROM jadwal_kelas WHERE id_jadwal_kelas = {$id_jadwal_kelas_old[$i]}");
				}
			}
			
			// Jika dosen pengampu ada isian
			if ( ! empty($id_dosen[$i]))
			{
				// Untuk urutan pertama merupakan PJMK (1), selebihnya Anggota (2)
				$pjmk_pengampu_mk = ($i == 0) ? '1' : '2';  
				
				// Jika sudah ada pengampu mk -> UPDATE
				if ( ! empty($id_pengampu_mk_old[$i]))
				{
					$db->Query("UPDATE pengampu_mk SET id_dosen = {$id_dosen[$i]}, updated_on = sysdate, updated_by = {$user->ID_PENGGUNA} WHERE id_pengampu_mk = {$id_pengampu_mk_old[$i]}");
				}
				// Jika belum ada pengampu mk -> INSERT
				else
				{
					$db->Query("INSERT INTO pengampu_mk (id_kelas_mk, id_dosen, pjmk_pengampu_mk, created_by) VALUES ({$id_kelas_mk}, {$id_dosen[$i]}, {$pjmk_pengampu_mk}, {$user->ID_PENGGUNA})");
				}
			}
			// Jika tidak ada isian pengampu
			else
			{
				// Jika sudah ada pengampu mk -> DELETE
				if ( ! empty($id_pengampu_mk_old[$i]))
				{
					$db->Query("DELETE FROM pengampu_mk WHERE id_pengampu_mk = {$id_pengampu_mk_old[$i]}");
				}
			}
		}
			
		$db->Commit();
		
		$smarty->assign('message', "Berhasil mengupdate kelas");
		$smarty->assign('smt', $id_semester);
		$smarty->display('usulan-mk/result.tpl');
		
		exit();
	}
}

if ($action == 'add-kelas')
{
	$id_kurikulum_mk = get('kmk', '');
	
	// Jika mata kuliah belum di pilih, tampilkan list mata kuliah berdasarkan kurikulum yang aktif
	if (empty($id_kurikulum_mk))
	{
		// Ambil daftar mata kuliah
		$mata_kuliah_set = $db->QueryToArray(
			"SELECT
				kmk.id_kurikulum_mk,
				mk.kd_mata_kuliah, mk.nm_mata_kuliah, kmk.kredit_semester, kmk.tingkat_semester, k.nm_kurikulum
			FROM kurikulum_mk kmk
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
			JOIN kurikulum k ON k.id_kurikulum = kmk.id_kurikulum
			WHERE kmk.id_program_studi = '{$user->ID_PROGRAM_STUDI}' AND k.status_aktif = 1
			ORDER BY mk.nm_mata_kuliah");
			
		$smarty->assign('mata_kuliah_set', $mata_kuliah_set);
	}
	
	// Jika mata kuliah sudah di pilih, tampilkan editor tambah kelas
	if ( ! empty($id_kurikulum_mk))
	{
		$id_semester = get('smt', '');
	}
}

if ($action == 'update')
{
	$id_kelas_mk = (int)get('kls');
	
	// Ambil info kelas
	$db->Query("SELECT kls.*, to_char(kls.tgl_mulai, 'DD-MM-YYYY') tgl_mulai_dmy, to_char(kls.tgl_akhir, 'DD-MM-YYYY') tgl_akhir_dmy FROM kelas_mk kls WHERE id_kelas_mk = {$id_kelas_mk}");
	$kelas = $db->FetchAssoc();
	
	$id_kurikulum_mk = $kelas['ID_KURIKULUM_MK'];
	$id_semester = $kelas['ID_SEMESTER'];
	
	// Ambil data jadwal_kelas
	$jadwal_set = $db->QueryToArray("SELECT jdw.* FROM kelas_mk kls JOIN jadwal_kelas jdw ON jdw.id_kelas_mk = kls.id_kelas_mk WHERE kls.id_kelas_mk = {$id_kelas_mk} ORDER BY jdw.id_jadwal_hari");
	
	// Ambil data dosen pengampu
	$pengampu_set = $db->QueryToArray("SELECT pmk.* FROM kelas_mk kls JOIN pengampu_mk pmk ON pmk.id_kelas_mk = kls.id_kelas_mk WHERE kls.id_kelas_mk = {$id_kelas_mk} ORDER BY pjmk_pengampu_mk DESC");
	
	$smarty->assign('kelas', $kelas);
	$smarty->assign('jadwal_set', $jadwal_set);
	$smarty->assign('pengampu_set', $pengampu_set);
}

// Data-data master editor
if (($action == 'add-kelas' && ! empty($id_kurikulum_mk)) || $action == 'update')
{
	// Ambil mata kuliah
		$db->Query(
			"SELECT kmk.id_kurikulum_mk, mk.kd_mata_kuliah, mk.nm_mata_kuliah, mk.kredit_semester, kmk.tingkat_semester, k.nm_kurikulum, kmk.status_mkta
			FROM kurikulum_mk kmk
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
			JOIN kurikulum k ON k.id_kurikulum = kmk.id_kurikulum
			WHERE kmk.id_kurikulum_mk = '{$id_kurikulum_mk}'
			ORDER BY mk.nm_mata_kuliah");
		$mata_kuliah = $db->FetchAssoc();
		
		// Ambil semester
		$db->Query("SELECT id_semester, tahun_ajaran, nm_semester FROM semester WHERE id_semester = '{$id_semester}'");
		$semester = $db->FetchAssoc();
		
		// Ambil nama kelas
		$nama_kelas_set = $db->QueryToArray(
			"SELECT id_nama_kelas, nama_kelas FROM nama_kelas
			where (id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI} or id_perguruan_tinggi is null)
			order by nama_kelas");
			
		// Ambil nama hari
		$hari_set = $db->QueryToArray("SELECT id_jadwal_hari,nm_jadwal_hari from jadwal_hari order by 1");
		
		// Ambil Jam
		$jam_set = $db->QueryToArray("SELECT id_jadwal_jam,nm_jadwal_jam from jadwal_jam where id_fakultas='{$user->ID_FAKULTAS}' order by jam_mulai, menit_mulai, jam_selesai, menit_selesai");
		
		// Ambil Ruangan
		$ruangan_set =$db->QueryToArray(
			"SELECT id_ruangan,nm_ruangan||' ('||kapasitas_ruangan||')' as nm_ruangan 
			from ruangan
			join gedung on gedung.id_gedung = ruangan.id_gedung and gedung.id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}
			left join gedung_kuliah on gedung.id_gedung = gedung_kuliah.id_gedung
			where gedung.id_fakultas = '{$user->ID_FAKULTAS}' order by nm_ruangan");
			
		// Daftar dosen seluruh PT
		$dosen_set = $db->QueryToArray(
			"SELECT d.id_dosen, p.nm_pengguna||nvl2(p.gelar_belakang,', '||p.gelar_belakang,NULL)||' ('||d.nip_dosen||')' AS nm_pengguna
			FROM dosen d
			JOIN pengguna p ON p.id_pengguna = d.id_pengguna
			WHERE p.id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}'
			order by nm_pengguna");
		
		$smarty->assign('mata_kuliah', $mata_kuliah);
		$smarty->assign('semester', $semester);
		$smarty->assign('nama_kelas_set', $nama_kelas_set);
		$smarty->assign('hari_set', $hari_set);
		$smarty->assign('jam_set', $jam_set);
		$smarty->assign('ruangan_set', $ruangan_set);
		$smarty->assign('dosen_set', $dosen_set);
}

if ($action == 'tampil')
{
	$id_semester = (! empty($_GET['smt'])) ? $_GET['smt'] : $semester_aktif['ID_SEMESTER'];

	/**
	// Semester 3 tahun terakhir
	$semester_set = $db->QueryToArray(
		"SELECT id_semester, tahun_ajaran||' '||nm_semester||decode(status_aktif_semester, 'True', ' (Aktif)') as nm_semester FROM semester
		WHERE id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' and thn_akademik_semester >= extract(year from sysdate) - 3
		order by thn_akademik_semester desc, nm_semester desc");
	
	// Daftar Kelas
	$kelas_set = $db->QueryToArray(
		"SELECT
			kls.id_kelas_mk,
			mk.kd_mata_kuliah, mk.nm_mata_kuliah, nk.nama_kelas, mk.kredit_semester, 
			kmk.tingkat_semester,
			kls.kapasitas_kelas_mk AS kapasitas,
			(select count(*) from pengambilan_mk pmk where pmk.id_kelas_mk = kls.id_kelas_mk) as isi,
			kmk.status_mkta,
			(select count(*) from pengampu_mk pmk where pmk.id_kelas_mk = kls.id_kelas_mk and pjmk_pengampu_mk = 1) as jml_dosen,
			(select count(*) from jadwal_kelas jdw where jdw.id_kelas_mk = kls.id_kelas_mk) as jml_jadwal,
			(select count(*) from jadwal_kelas jdw join ruangan r on r.id_ruangan = jdw.id_ruangan where jdw.id_kelas_mk = kls.id_kelas_mk) as jml_ruangan
		FROM kelas_mk kls
		JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = kls.id_kurikulum_mk
		JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
		LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kls.no_kelas_mk
		WHERE 
			kls.id_program_studi = '{$user->ID_PROGRAM_STUDI}' AND
			kls.id_semester = '{$id_semester}'
		ORDER BY mk.nm_mata_kuliah");
	
	// Jadwal kuliah tiap kelas
	$jadwal_set = $db->QueryToArray(
		"SELECT
			kls.id_kelas_mk,
			hari.nm_jadwal_hari||', '||jam.jam_mulai||':'||jam.menit_mulai||' - '||jam.jam_selesai||':'||jam.menit_selesai as jadwal, nm_ruangan
		FROM kelas_mk kls
		JOIN jadwal_kelas jdw ON jdw.id_kelas_mk = kls.id_kelas_mk
		LEFT JOIN jadwal_hari hari ON hari.id_jadwal_hari = jdw.id_jadwal_hari
		LEFT JOIN jadwal_jam jam ON jam.id_jadwal_jam = jdw.id_jadwal_jam
		LEFT JOIN ruangan r ON r.id_ruangan = jdw.id_ruangan
		WHERE 
			kls.id_program_studi = '{$user->ID_PROGRAM_STUDI}' AND
			kls.id_semester = '{$id_semester}'
		ORDER BY hari.id_jadwal_hari, jam.jam_mulai");
			
	// Dosen pengajar / tim pengajar
	$dosen_set = $db->QueryToArray(
		"SELECT
			kls.id_kelas_mk,
			p.nm_pengguna||nvl2(p.gelar_belakang,', '||p.gelar_belakang,NULL) AS nm_dosen,
			pmk.pjmk_pengampu_mk
		FROM kelas_mk kls
		JOIN pengampu_mk pmk ON pmk.id_kelas_mk = kls.id_kelas_mk
		JOIN dosen d ON d.id_dosen = pmk.id_dosen
		JOIN pengguna p ON p.id_pengguna = d.id_pengguna
		WHERE 
			kls.id_program_studi = '{$user->ID_PROGRAM_STUDI}' AND
			kls.id_semester = '{$id_semester}'
		ORDER BY pjmk_pengampu_mk DESC");
	*/

	$semester_set	= $PT->list_semester_with_sp();
	$kelas_set		= $akademik->list_kelas_per_prodi($user->ID_PROGRAM_STUDI, $id_semester);
	$jadwal_set		= $akademik->list_jadwal_kelas_per_prodi($user->ID_PROGRAM_STUDI, $id_semester);
	$dosen_set		= $akademik->list_dosen_kelas_per_prodi($user->ID_PROGRAM_STUDI, $id_semester);

	// Assignment ke template
	$smarty->assign('semester_set', $semester_set);
	$smarty->assign('id_semester_terpilih', $id_semester);
	$smarty->assign('kelas_set', $kelas_set);
	$smarty->assign('jadwal_set', $jadwal_set);
	$smarty->assign('dosen_set', $dosen_set);
}

if ($action == 'delete')
{
	$id_kelas_mk = (int)get('kls');
	
	$db->Query("SELECT id_semester FROM kelas_mk WHERE id_kelas_mk = {$id_kelas_mk}");
	$kelas = $db->FetchAssoc();
	$id_semester = $kelas['ID_SEMESTER'];
	
	// Query delete-nya di join dulu dengan perguruan tinggi & program_studi biar aman
	$db->Query(
		"DELETE from kelas_mk where id_kelas_mk in (
		SELECT id_kelas_mk FROM kelas_mk kls
		JOIN semester s ON s.id_semester = kls.id_semester
		where s.id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI} and kls.id_program_studi = {$user->ID_PROGRAM_STUDI} and kls.id_kelas_mk = {$id_kelas_mk})");
		
	$smarty->assign('message', "Berhasil menghapus kelas");
	$smarty->assign('smt', $id_semester);
	$smarty->display('usulan-mk/result.tpl');
	exit();
}

$smarty->display("usulan-mk/{$action}.tpl");