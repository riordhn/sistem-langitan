<?php

// ------------------------------------------------------
// Modifikasi by Fathoni > di arahkan ke class $db global
// ------------------------------------------------------
// 
// Reference
$conn = $db->conn;

$db_common = $db;

function getData($sSql)
{
    global $db_common;
    return $db_common->QueryToArray($sSql);
}

function isFindData($table, $findfield)
{
    global $db_common;

    $sSql = "select * from " . $table . " where " . $findfield;

    $db_common->Query($sSql);

    $row = $db_common->FetchRow();
    if ($row)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function tambahdata($table, $field, $value)
{
    global $db_common;

    $sSql = "insert into " . $table . "(" . $field . ") values (" . $value . ")";
    $db_common->Query($sSql);
}

function InsertData($sSql)
{
    global $db_common;
    return $db_common->Query($sSql);
}

function UpdateData($sSql)
{
    global $db_common;
    return $db_common->Query($sSql);
}

function gantidata($sSql)
{
    global $db_common;
    return $db_common->Query($sSql);
}

function getvar($sSql)
{
    global $db_common;
    $db_common->Query($sSql);
    return $db_common->FetchAssoc();
}

function deleteData($sSql)
{
    global $db_common;
    return $db_common->Query($sSql);
}
