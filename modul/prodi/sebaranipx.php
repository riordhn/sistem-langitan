<?php
include "../config.php";
$id_fakultas=$_GET['idfak'];
$sem_aktif=21;
$sem_lama=5;
$db = new MyOracle();
$osql="
select K.*,nvl(K.sksd,0) sksdx from
(select
SP.NM_STATUS_PENGGUNA STATUS,J.id_jenjang,S.id_program_studi,J.nm_jenjang,S.nm_program_studi,P.username,P.nm_pengguna,A.totalsks,round(B.bobot/A.totalsks,2) ipk,C.totalsks sksx,round(D.bobot/C.totalsks,2) ips,round((E.totalsksd/A.totalsks)*100,1) sksd
from 
(
select id_pengguna,nm_pengguna,username from pengguna  where id_role=3 and id_pengguna in 
(select id_pengguna from 
(select id_pengguna,id_program_studi from mahasiswa where id_mhs not in 
(select distinct id_mhs from pembayaran where id_semester={$sem_aktif} and id_status_pembayaran !=2)) m 
left join program_studi p on (m.id_program_studi=p.id_program_studi) where p.id_fakultas={$id_fakultas})
) P
left join 
(
select * from mahasiswa where id_mhs not in
(select distinct id_mhs from pembayaran where id_semester={$sem_aktif} and id_status_pembayaran !=2)
)
 M on (P.id_pengguna=M.id_pengguna)
left join program_studi S on (S.id_program_studi=M.id_program_studi)
left join jenjang J on (J.id_jenjang=S.id_jenjang)
LEFT JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA=M.STATUS_AKADEMIK_MHS
left join 
(
select distinct aaa.id_mhs,sum(kredit_semester) over(partition by aaa.id_mhs) totalsks
from(
select a.id_mhs,a.id_pengambilan_mk,e.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from 
(
select * from pengambilan_mk 
where id_mhs not in(
select distinct id_mhs from pembayaran where id_semester={$sem_aktif} and id_status_pembayaran !=2
)
)
a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join program_studi p on p.id_program_studi=d.id_program_studi
where a.nilai_huruf<'E' and a.nilai_huruf is not null  and  p.id_fakultas={$id_fakultas}
) aaa
where rangking=1 
) A on (A.id_mhs=M.id_mhs)
left join 
(
select distinct aaa.id_mhs,sum((aaa.kredit_semester*(case aaa.nilai_huruf 
when 'A' then 4 
when 'AB' then 3.5 
when 'B' then 3
when 'BC' then 2.5
when 'C' then 2
when 'D' then 1
end)) ) over(partition by aaa.id_mhs) bobot

from(
select a.id_mhs,a.id_pengambilan_mk,e.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from 
(
select * from pengambilan_mk 
where id_mhs not in(
select distinct id_mhs from pembayaran where id_semester={$sem_aktif} and id_status_pembayaran !=2
)
)
 a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join program_studi p on p.id_program_studi=d.id_program_studi
where a.nilai_huruf<'E' and a.nilai_huruf is not null  and  p.id_fakultas={$id_fakultas}) aaa
where rangking=1
) B on (B.id_mhs=M.id_mhs)
left join 
(
select distinct aaa.id_mhs,sum(kredit_semester) over(partition by aaa.id_mhs) totalsks
from(
select a.id_mhs,a.id_pengambilan_mk,e.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from 
(
select * from pengambilan_mk 
where id_semester='$sem_lama' and id_mhs not in(
select distinct id_mhs from pembayaran where id_semester={$sem_aktif} and id_status_pembayaran !=2
)
)
a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join program_studi p on p.id_program_studi=d.id_program_studi
where a.nilai_huruf is not null AND NOT (e.status_praktikum=2 and a.nilai_huruf='E')  and  p.id_fakultas={$id_fakultas}
) aaa
where rangking=1 
) C on (C.id_mhs=M.id_mhs)
left join 
(
select distinct aaa.id_mhs,sum((aaa.kredit_semester*(case aaa.nilai_huruf 
when 'A' then 4 
when 'AB' then 3.5 
when 'B' then 3
when 'BC' then 2.5
when 'C' then 2
when 'D' then 1
end)) ) over(partition by aaa.id_mhs) bobot

from(
select a.id_mhs,a.id_pengambilan_mk,e.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from 
(
select * from pengambilan_mk 
where id_semester='$sem_lama' and  id_mhs not in(
select distinct id_mhs from pembayaran where id_semester={$sem_aktif} and id_status_pembayaran !=2
)
)
 a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join program_studi p on p.id_program_studi=d.id_program_studi
where a.nilai_huruf is not null AND NOT (e.status_praktikum=2 and a.nilai_huruf='E')  and  p.id_fakultas={$id_fakultas}) aaa
where rangking=1
) D on (D.id_mhs=M.id_mhs)

left join 
(
select distinct aaa.id_mhs,sum(kredit_semester) over(partition by aaa.id_mhs) totalsksd
from(
select a.id_mhs,a.id_pengambilan_mk,e.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join program_studi p on p.id_program_studi=d.id_program_studi
where a.nilai_huruf<'E' and a.nilai_huruf is not null  and  p.id_fakultas={$id_fakultas}) aaa
where rangking=1 and nilai_huruf='D'
) E on (E.id_mhs=M.id_mhs)


) K
where ipk is not null
order by id_jenjang,id_program_studi,status,totalsks desc,ipk asc
";
//echo $osql;
$result1 = $db->Query($osql);
echo "<h3>DAFTAR IPK,SKS KUMULATIF,IPS DAN SKS SEMESTER MAHASISWA (TIDAK BAYAR SPP)</h3>";
echo "<table border=1>";
echo "<tr bgcolor='#AAAAAA'>";
echo "<th>No.</th><th>JENJANG</th><th>PROGRAM STUDI</th><th>NIM</th><th>NAMA</th><th>STATUS</th><th> IPK </th><th>SKS KUMULATIF </th><th>IPS</th><th>SKS Semester</th><th>PROSEN SKS NILAI D</th>";
echo "</tr>";$i=1;
while($r1 = $db->FetchAssoc()) {
if($i%2==0)
echo "<tr bgcolor='#CCCCCC'>";
else
echo "<tr bgcolor='#FFFFFF'>";
echo "<td>$i</td><td>{$r1['NM_JENJANG']}</td><td>{$r1['NM_PROGRAM_STUDI']}</td><td>{$r1['USERNAME']}</td><td>{$r1['NM_PENGGUNA']}</td><td>{$r1['STATUS']}</td><td>{$r1['IPK']}</td><td>{$r1['TOTALSKS']}</td><td>{$r1['IPS']}</td><td>{$r1['SKSX']}</td><td>{$r1['SKSDX']}</td>";
echo "</tr>";
$i++;
}
echo "</table>";
?>