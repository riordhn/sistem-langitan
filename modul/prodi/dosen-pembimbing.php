<?php
/*
Yudi Sulistya on 27/09/2012
*/

error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$kdprodi=$user->ID_PROGRAM_STUDI; 
$kdfak=$user->ID_FAKULTAS;

$default=getvar("select angkatan from
(select distinct thn_angkatan_mhs as angkatan from mahasiswa 
where id_program_studi=$kdprodi and status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
order by thn_angkatan_mhs)
where rownum=1");

$smtaktif=getvar("select * from semester where status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where thn_akademik_semester in (
select * from (select distinct thn_akademik_semester
from semester where thn_akademik_semester<=EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc)
where rownum<=10) and nm_semester in ('Ganjil', 'Genap')
order by thn_akademik_semester desc, nm_semester desc");
$smarty->assign('T_ST', $smt);

$angkatan=getData("select distinct thn_angkatan_mhs as angkatan from mahasiswa
where id_program_studi=$kdprodi and status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
order by angkatan");
$smarty->assign('ANG', $angkatan);
$agk = isSet($_GET['angk']) ? $_GET['angk'] : $default['ANGKATAN'];

$dosen=getData("select id_dosen,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nama_dosen from dosen
left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
left join pengguna b on dosen.id_pengguna=b.id_pengguna
where program_studi.id_fakultas='$kdfak' order by nm_pengguna");
$smarty->assign('DOSEN', $dosen);

$jaf2=getData("select nip_dosen, trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna, count(distinct id_mhs) as ttl from dosen
left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
left join dosen_wali on dosen.id_dosen=dosen_wali.id_dosen
where id_departemen='$kddep[ID_DEPARTEMEN]' and id_mhs in (select id_mhs from mahasiswa where status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1))
and status_dosen_wali=1
group by nip_dosen, trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang)
");
$smarty->assign('SEBDOLI',$jaf2);


$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';

switch($status) {

case 'savewali':
		
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
	
		$id_dosen=$_POST['dosen'];
		$counter=$_POST['counter1'];
	
		for ($i=1; $i<=$counter; $i++)
		{
		if ($_POST['mhs'.$i]<>'' || $_POST['mhs'.$i]<>null) {				
					gantidata("insert into dosen_wali(id_mhs,id_semester,id_dosen,status_dosen_wali)
							  values ('".$_POST['mhs'.$i]."','$smtaktif[ID_SEMESTER]',$id_dosen,1)");
				  	//echo "insert into dosen_wali(id_mhs,id_semester,id_dosen,status_dosen_wali)
							  //values ('".$_POST['mhs'.$i]."','$smtaktif[ID_SEMESTER]',$id_dosen,1)";	
		} 
		}
	
		$jaf1=getData("select id_mhs,nim_mhs,upper(nm_pengguna) as nm_pengguna,'0' as tanda from mahasiswa 
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna 
						where id_program_studi=$kdprodi and id_mhs not in (select id_mhs from dosen_wali) 
						and status_akademik_mhs in 
						(select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
						order by nim_mhs
						");
		$smarty->assign('NOWALI', $jaf1);
		
		break;
		
case 'update':
		
		$smarty->assign('disp1','block');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$id_mhs=$_POST['id_mhs'];
		//$id_dosen=$_POST['dosen[$id_mhs]'];
		$id_dosen=$_POST['dosen'];
		//echo $id_mhs;
		//echo $id_dosen;
		
		//UpdateData("dosen_wali","id_mhs,id_dosen,id_semester,status_dosen_wali","$id_mhs,$id_dosen,".$smtaktif['ID_SEMESTER'].",1");
		UpdateData("update dosen_wali set status_dosen_wali=0 where id_mhs='$id_mhs' and status_dosen_wali=1");
		tambahdata("dosen_wali","id_mhs,id_dosen,id_semester,status_dosen_wali","$id_mhs,$id_dosen,".$smtaktif['ID_SEMESTER'].",1");

$jaf=getData("select mahasiswa.id_mhs,nim_mhs,upper(pengguna.nm_pengguna) as mhs,b.nm_pengguna as doli from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join dosen_wali on dosen_wali.id_mhs=mahasiswa.id_mhs
left join dosen on dosen_wali.id_dosen=dosen.id_dosen
left join pengguna b on dosen.id_pengguna=b.id_pengguna
where mahasiswa.id_program_studi=$kdprodi and 
mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
and substr(nim_mhs,3,2)=substr($agk,3,2)");
		$smarty->assign('WALI', $jaf);
		
		break;
		
case 'searchdosen':
		$namacari=$_POST['namadosen'];
		$id_mk=$_GET['id_klsmk'];
		$stdos=$_GET['status'];
		$pjmk=$_GET['pjmk'];
		$id_pengampu=$_GET['id_pengampu'];
		$status1=$_GET['status1'];
		
		if ($status1==ganti) {
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','block');} else {
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
		$smarty->assign('disp4','none');}
		
	
		
		if ($namacari !='') {
		$hasil=getData("select id_dosen,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna from dosen
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
where nm_pengguna like '%$namacari%'");}
		$smarty->assign('DOSEN',$hasil);
		$smarty->assign('IDKEL',$id_mk);
		$smarty->assign('ST_PJMK',$pjmk);
		$smarty->assign('ID_PENGAMPU',$id_pengampu);
		$smarty->assign('STATUS1',$status1);

		break;

case 'viewupdate':
		 // pilih
		 $smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
		$smarty->assign('disp4','none');
		$id_mhs= $_GET['id_mhs'];
		 		
$jaf=getData("select id_dosen_wali,	mahasiswa.id_mhs,nim_mhs,upper(pengguna.nm_pengguna) as mhs,
trim(b.gelar_depan||' '||upper(b.nm_pengguna)||', '||b.gelar_belakang) as doli from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join dosen_wali on dosen_wali.id_mhs=mahasiswa.id_mhs
left join dosen on dosen_wali.id_dosen=dosen.id_dosen
left join pengguna b on dosen.id_pengguna=b.id_pengguna
where mahasiswa.id_program_studi=$kdprodi and mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
and mahasiswa.id_mhs=$id_mhs
and dosen_wali.status_dosen_wali='1'");
		$smarty->assign('DOLI', $jaf);

        break;   
		
case 'del':
		 // pilih
		

        break; 
		
		
case 'tampil':
		//echo "aa";
		//echo $smtaktif['ID_SEMESTER'];
		$smt1= $_POST['smt'];
		 if ($smt1=='') { $smt1=$smtaktif['ID_SEMESTER'];}
		//echo $smt1;
		
		if ($smt1!='') {
		$smarty->assign('SMT',$smt);
		//$smarty->assign('KUR', $id_kur);
		
		//$agk = isSet($_GET['angk']) ? $_GET['angk'] : $default['ANGKATAN'];
		//echo $agk;
		
		$jaf=getData("select id_dosen_wali,mahasiswa.id_mhs,nim_mhs,
						upper(pengguna.nm_pengguna) as mhs,trim(b.gelar_depan||' '||upper(b.nm_pengguna)||', '||b.gelar_belakang) as doli 
						from mahasiswa
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						left join dosen_wali on dosen_wali.id_mhs=mahasiswa.id_mhs
						left join dosen on dosen_wali.id_dosen=dosen.id_dosen
						left join pengguna b on dosen.id_pengguna=b.id_pengguna
						where mahasiswa.id_program_studi=$kdprodi and mahasiswa.id_mhs in (select id_mhs from mahasiswa 
						where status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1))						and substr(nim_mhs,3,2)=substr($agk,3,2)
						and dosen_wali.status_dosen_wali='1'");

}
		$smarty->assign('WALI', $jaf);

        break; 
			 
}



$smarty->display('dosen-wali.tpl');
?>
