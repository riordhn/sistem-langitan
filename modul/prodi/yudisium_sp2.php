<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi= $user->ID_PROGRAM_STUDI;
//$kdprodi=7;

$kdfak=$user->ID_FAKULTAS;
//$kdfak=1;

$id_pengguna=$user->ID_PENGGUNA;

$smtaktif=getvar("select id_semester,thn_akademik_semester from semester where status_aktif_semester='True'");
$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);
$smt=getData("select id_semester,thn_akademik_semester||'-'||nm_semester as SMT 
			 from semester where nm_semester in ('Ganjil','Genap') and 
			 thn_akademik_semester >='$smtaktif[THN_AKADEMIK_SEMESTER]'-4");
$smarty->assign('T_ST',$smt);		 


if ($kdfak==1 ) 
{

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');

if ($_GET['action']=='detail')
{

$nim_mhs=$_GET['nim_mhs'];
$smarty->assign('nim_mhs',$nim_mhs);
$datamk=getData("select s1.kd_mata_kuliah,s1.nm_mata_kuliah,s1.kredit_semester,s2.id_pengambilan_mk,s2.nilai_huruf,s2.tahun from 
				(select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester from kurikulum_mk 
				left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
				where kurikulum_mk.id_program_studi=$kdprodi and id_kurikulum_prodi=220 and status_paket='sp2'
				order by tingkat_semester,kd_mata_kuliah)s1 
				left join (
				select * from
				(select a.id_mhs, a.id_pengambilan_mk,c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
				thn_akademik_semester||'-'||group_semester||'-'||nm_semester as tahun,
				row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
				from pengambilan_mk a
				join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
				join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
				join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
				join semester smt on a.id_semester=smt.id_semester
				join mahasiswa m on a.id_mhs=m.id_mhs
				where a.status_apv_pengambilan_mk = 1 and d.status_paket='sp2'
				and m.id_program_studi=$kdprodi and a.status_hapus=0 
				and m.nim_mhs='$nim_mhs'
				and a.status_pengambilan_mk !=0)
				where rangking=1)s2 on s1.kd_mata_kuliah=s2.kd_mata_kuliah");
				
$smarty->assign('datamk',$datamk);
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','block');

}
if ($_POST['action']=='ganti')
{

	$counter=$_POST['hitnilai'];

	

	for ($i=1; $i<=$counter; $i++) {
		$cek=getvar("select nilai_huruf from pengambilan_mk where id_pengambilan_mk='".$_POST['pmk'.$i]."'" );
			if ($cek[NILAI_HURUF]<>$_POST['nilai'.$i] and $_POST['pmk'.$i]<>'' ) {
				$ip = $_SERVER['REMOTE_ADDR'];
				
				InsertData("insert into log_update_nilai (id_pengguna,referensi,id_pengambilan_mk,ip_address,tgl_update,nilai_huruf_lama,nilai_huruf_baru)
							select '$id_pengguna','UPDATE NILAI YUDISIUM',id_pengambilan_mk,'$ip', sysdate, nilai_huruf, upper('".$_POST['nilai'.$i]."')
							from pengambilan_mk where id_pengambilan_mk='".$_POST['pmk'.$i]."'") or die('gagal 1');
				/*
				echo "insert into log_update_nilai (id_pengguna,referensi,id_pengambilan_mk,ip_address,tgl_update,nilai_huruf_lama,nilai_huruf_baru)
							select '$id_pengguna','UPDATE NILAI YUDISIUM',id_pengambilan_mk,'$ip', sysdate, nilai_huruf, upper('".$_POST['nilai'.$i]."')
							from pengambilan_mk where id_pengambilan_mk='".$_POST['pmk'.$i]."'";
				*/
				gantidata("update pengambilan_mk set nilai_huruf='".$_POST['nilai'.$i]."' where id_pengambilan_mk='".$_POST['pmk'.$i]."'");
				//echo "update pengambilan_mk set nilai_huruf='".$_POST['nilai'.$i]."' where id_pengambilan_mk='".$_POST['pmk'.$i]."'";
			}
	}
	
	
$smarty->assign('disp1','none');
$smarty->assign('disp2','block');
$smarty->assign('disp3','none');

}

if ($_POST['action']=='proses')
{

	$counter=$_POST['hitungyds'];
	$smt=$_POST['smt'];
	$tgl=$_POST['yds'];
	

	for ($i=1; $i<=$counter; $i++) {
		if ($_POST['sp2'.$i]<>'' || $_POST['sp2'.$i]<>null) {

					gantidata("insert into status_supro (id_mhs,id_semester,nama_supro,status_supro,tgl_yds_supro,id_pengguna)
							  values ('".$_POST['sp2'.$i]."',$smt,'SP2',1,'$tgl',$id_pengguna)");
				 // 	echo "insert into status_supro (id_mhs,id_semester,nama_supro,status_supro,tgl_yds_supro)
				//			  values ('".$_POST['sp1'.$i]."',$smt,'SP2',1,'$tgl')";	
		} 
		
		
	}
	echo '<script>alert("Proses Isi Yudisium SP-2.. Sudah SELESAI...")</script>';
}

if ($_GET['action']=='del')
{
$id_status_supro=$_GET['id'];
gantidata("delete from status_supro where id_status_supro=$id_status_supro");
//echo "delete from status_supro where id_status_supro=$id_status_supro";

}

$datasupro=getData("select id_status_supro,id_mhs,nim_mhs,nm_pengguna,nama_supro||'- LULUS' as nama_supro,tgl_yds_supro from
					(select a.id_status_supro,a.id_mhs,nim_mhs,upper(nm_pengguna) as nm_pengguna,nama_supro,tgl_yds_supro,
					row_number() over(partition by a.id_mhs,status_supro order by nama_supro desc) rangking
					from status_supro a
					join mahasiswa m on a.id_mhs=m.id_mhs
					join pengguna p on m.id_pengguna=p.id_pengguna
					where status_supro=1)
					where rangking=1 and nama_supro='SP2'
					  and id_mhs in (select id_mhs from mahasiswa 
					  where status_akademik_mhs in 
					  (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
					  and id_program_studi=$kdprodi)
					order by nim_mhs");
$smarty->assign('datasupro',$datasupro);
/*
$datasupro2=getData("select id_status_supro,id_mhs,nim_mhs,nm_pengguna,'SP-2' as status_supro,'' as tgl_yds from
					(select a.id_status_supro,a.id_mhs,nim_mhs,upper(nm_pengguna) as nm_pengguna,nama_supro,tgl_yds_supro,
					row_number() over(partition by a.id_mhs,status_supro order by nama_supro desc) rangking
					from status_supro a
					join mahasiswa m on a.id_mhs=m.id_mhs
					join pengguna p on m.id_pengguna=p.id_pengguna
					where status_supro=1)
					where rangking=1 and nama_supro='SP1'
					  and id_mhs in (select id_mhs from mahasiswa 
					  where status_akademik_mhs in 
					  (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
					  and id_program_studi=$kdprodi)
					order by nim_mhs");
*/
$datasupro2=getData("select s1.id_mhs,s1.nim_mhs,s1.nm_pengguna,s2.sks_total_mhs,s2.ipk_mhs,s2.ttlnilD,s2.ttlnilE,
					s1.status_supro from
					(select id_status_supro,id_mhs,nim_mhs,nm_pengguna,'SP-2' as status_supro,'' as tgl_yds from
					(select a.id_status_supro,a.id_mhs,nim_mhs,upper(nm_pengguna) as nm_pengguna,nama_supro,tgl_yds_supro,
					row_number() over(partition by a.id_mhs,status_supro order by nama_supro desc) rangking
					from status_supro a
					join mahasiswa m on a.id_mhs=m.id_mhs
					join pengguna p on m.id_pengguna=p.id_pengguna
					where status_supro=1)
					where rangking=1 and nama_supro='SP1'
					  and id_mhs in (select id_mhs from mahasiswa 
					  where status_akademik_mhs in 
					  (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
					  and id_program_studi=$kdprodi)
					order by nim_mhs)s1
					left join
					(select id_mhs, sum(kredit_semester) as SKS_TOTAL_MHS,round(sum((kredit_semester * nilai_standar_nilai)),2) as bobot, 
					round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS,
					sum(case when nilai_huruf='D' then 1 else 0 end) as ttlnilD,
					sum(case when nilai_huruf='E' then 1 else 0 end) as ttlnilE
					from 
					(select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
					case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
					row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
					from pengambilan_mk a
					join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
					join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
					join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
					join semester smt on a.id_semester=smt.id_semester
					join mahasiswa m on a.id_mhs=m.id_mhs
					where a.status_apv_pengambilan_mk = 1 and d.status_paket='sp2'
					and m.id_program_studi=$kdprodi and a.status_hapus=0 
					and a.status_pengambilan_mk !=0)
					where rangking=1
					group by id_mhs)s2 on s1.id_mhs=s2.id_mhs
					order by nim_mhs");


$smarty->assign('datasupro1',$datasupro2);

$smarty->display('yudisium_sp2.tpl');
}
else
{
 echo '<script>alert("Maaf... Anda tidak Berhak di Menu Ini!!!")</script>';
}
?>
