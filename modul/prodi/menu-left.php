<?php
require('common.php');

$selected_tab = request_var('tab', '');

$xml = new SimpleXMLElement(load_file('menu.xml'));

foreach ($xml->tab as $tab)
{
    // Mencari tab yang dipilih
    if ($tab->id == $selected_tab)
    {
    	$data = array();
        
    	foreach ($tab->menu as $menu)
        {
        	array_push($data, array(
                'title' => $menu->title,
                'tab'   => $tab->id,
                'menu'	=> $menu->id
        	));
        }
        
        $smarty->assign('menus', $data);
        
        break;
    }
}

$smarty->display('menu-left.tpl');
?>