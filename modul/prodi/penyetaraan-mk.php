<?php
include 'config.php';

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

$id_pengguna = $user->ID_PENGGUNA;
$id_prodi = $user->ID_PROGRAM_STUDI;

if ($request_method == 'POST')
{

    if (post('mode') == 'save-mk-setara')
    {
        $id_mk_penyetaraan = $_POST['id_mk_penyetaraan'];
        $counter = $_POST['counter'];

        for ($i=1; $i<=$counter; $i++)
        {
            if ($_POST['mk'.$i]<>'' || $_POST['mk'.$i]<>null) {
                $result = $db->Query("insert into mk_setara(id_mata_kuliah,id_mk_penyetaraan,created_by)
                                  values ('".$_POST['mk'.$i]."','$id_mk_penyetaraan','$id_pengguna')");
            }
        }

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }
    if (post('mode') == 'delete-mk')
    {
        $id_mk_setara = post('id_mk_setara');
        
        $result = $db->Query("update mk_setara set is_deleted = 1
                                where id_mk_setara = '{$id_mk_setara}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }




    if (post('mode') == 'edit')
    {
        $id_mk_penyetaraan = post('id_mk_penyetaraan');
        $nm_mk_penyetaraan = post('nm_mk_penyetaraan');
        
        $result = $db->Query("update mk_penyetaraan set nm_mk_penyetaraan = '{$nm_mk_penyetaraan}'
        						where id_mk_penyetaraan = '{$id_mk_penyetaraan}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }
    if (post('mode') == 'add')
    {
        //$id_unit_kerja = post('id_unit_kerja');
        $nm_mk_penyetaraan = post('nm_mk_penyetaraan');
        
        $result = $db->Query("insert into mk_penyetaraan (nm_mk_penyetaraan, id_program_studi) 
        						values ('{$nm_mk_penyetaraan}', '{$id_prodi}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }
    if (post('mode') == 'delete')
    {
        $id_mk_penyetaraan = post('id_mk_penyetaraan');
        
        $result = $db->Query("update mk_penyetaraan set is_deleted = 1
                                where id_mk_penyetaraan = '{$id_mk_penyetaraan}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$mk_penyetaraan_set = $db->QueryToArray("SELECT MK_PENYETARAAN.*, 
                                                            (SELECT COUNT(ID_MK_PENYETARAAN) FROM MK_SETARA ms WHERE ms.ID_MK_PENYETARAAN = MK_PENYETARAAN.ID_MK_PENYETARAAN AND ms.IS_DELETED = 0) AS JML_JOIN 
                                                        FROM MK_PENYETARAAN WHERE ID_PROGRAM_STUDI = '{$id_prodi}' AND IS_DELETED = 0");
		$smarty->assignByRef('mk_penyetaraan_set', $mk_penyetaraan_set);
	}
	else if ($mode == 'add-mk' or $mode == 'delete')
	{
		$id_mk_penyetaraan 	= (int)get('id_mk_penyetaraan', '');

		$mk_penyetaraan = $db->QueryToArray("
            select * 
            from mk_penyetaraan 
            where id_mk_penyetaraan = {$id_mk_penyetaraan}");
        $smarty->assign('mk_penyetaraan', $mk_penyetaraan[0]);

        $matkul_set = $db->QueryToArray("SELECT id_mata_kuliah,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,'0' as tanda, kredit_tatap_muka, kredit_praktikum, kredit_prak_lapangan, kredit_simulasi,nm_jenjang_ijasah,
                                sub_rumpun_ilmu.id_rumpun_ilmu,nm_rumpun_ilmu,
                                kd_sub_rumpun_ilmu,nm_sub_rumpun_ilmu,SUB_RUMPUN_ILMU.ID_SUB_RUMPUN_ILMU,jenjang.nm_jenjang as nm_strata_mk,
                                (SELECT COUNT(*) FROM MATA_KULIAH mk WHERE mk.KD_MATA_KULIAH = MATA_KULIAH.KD_MATA_KULIAH AND mk.ID_PROGRAM_STUDI = '$id_prodi') AS RELASI_MATKUL,
                                (SELECT COUNT(*) FROM KURIKULUM_MK WHERE KURIKULUM_MK.ID_MATA_KULIAH = mata_kuliah.ID_MATA_KULIAH) AS RELASI,
                                fd_id_mk AS RELASI_FEEDER, jmk.nm_jenis_mk, kmk.nm_kelompok_mk
                                from mata_kuliah
                                 join program_studi ps on ps.id_program_studi = mata_kuliah.id_program_studi
                                 join jenjang on jenjang.id_jenjang = ps.id_jenjang 
                                 left join jenis_mk jmk on jmk.id_jenis_mk = mata_kuliah.id_jenis_mk
                                 left join kelompok_mk kmk on kmk.id_kelompok_mk = mata_kuliah.id_kelompok_mk
                                 left join SUB_RUMPUN_ILMU on MATA_KULIAH.ID_SUB_RUMPUN_ILMU=SUB_RUMPUN_ILMU.ID_SUB_RUMPUN_ILMU
                                 left join rumpun_ilmu on sub_rumpun_ilmu.id_rumpun_ilmu=rumpun_ilmu.id_rumpun_ilmu
                                where mata_kuliah.id_program_studi = '{$id_prodi}' and mata_kuliah.id_mata_kuliah NOT IN (SELECT ID_MATA_KULIAH FROM MK_SETARA WHERE ID_MK_PENYETARAAN = '{$id_mk_penyetaraan}' AND IS_DELETED = 0) ORDER BY nm_mata_kuliah");
        $smarty->assignByRef('MATKULSET', $matkul_set);

	}
	else if($mode == 'edit-delete-mk')
	{
        $id_mk_penyetaraan = (int)get('id_mk_penyetaraan', '');

		$mk_setara_set = $db->QueryToArray("SELECT ms.*, mk.KD_MATA_KULIAH, mk.NM_MATA_KULIAH, p.NM_PENGGUNA, p1.NM_PENGGUNA AS PENGGUNA_UPDATE
                                                        FROM MK_SETARA ms
                                                        JOIN MATA_KULIAH mk ON mk.ID_MATA_KULIAH = ms.ID_MATA_KULIAH
                                                        JOIN PENGGUNA p ON p.ID_PENGGUNA = ms.CREATED_BY
                                                        LEFT JOIN PENGGUNA p1 ON p1.ID_PENGGUNA = ms.UPDATED_BY
                                                        WHERE ms.ID_MK_PENYETARAAN = '{$id_mk_penyetaraan}' AND IS_DELETED = 0");
        $smarty->assignByRef('mk_setara_set', $mk_setara_set);

        $mk_penyetaraan = $db->QueryToArray("
            select * 
            from mk_penyetaraan 
            where id_mk_penyetaraan = {$id_mk_penyetaraan}");
        $smarty->assign('mk_penyetaraan', $mk_penyetaraan[0]);
	}
    else if($mode == 'delete-mk')
    {
        $id_mk_setara = (int)get('id_mk_setara', '');

        $mk_setara = $db->QueryToArray("SELECT ms.*, mp.NM_MK_PENYETARAAN, mk.KD_MATA_KULIAH, mk.NM_MATA_KULIAH, p.NM_PENGGUNA, p1.NM_PENGGUNA AS PENGGUNA_UPDATE
                                                        FROM MK_SETARA ms
                                                        JOIN MK_PENYETARAAN mp ON mp.ID_MK_PENYETARAAN = ms.ID_MK_PENYETARAAN
                                                        JOIN MATA_KULIAH mk ON mk.ID_MATA_KULIAH = ms.ID_MATA_KULIAH
                                                        JOIN PENGGUNA p ON p.ID_PENGGUNA = ms.CREATED_BY
                                                        LEFT JOIN PENGGUNA p1 ON p1.ID_PENGGUNA = ms.UPDATED_BY
                                                        WHERE ms.ID_MK_SETARA = '{$id_mk_setara}' AND ms.IS_DELETED = 0");
        $smarty->assignByRef('mk_setara', $mk_setara[0]);
    }

}

$smarty->display("penyetaraan-mk/{$mode}.tpl");