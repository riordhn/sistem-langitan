<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi= $user->ID_PROGRAM_STUDI;
$kdfak=$user->ID_FAKULTAS;
//tes

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True'");
$smtpil = isset($_REQUEST['smt']) ? $_REQUEST['smt'] : $smtaktif['ID_SEMESTER'];
$smarty->assign('id_smt',$smtpil);

$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester order by thn_akademik_semester desc,nm_semester desc");
$smarty->assign('T_ST', $smt);


$status=$_GET['action'];

if ($status=='update')

{
	$smarty->assign('disp1','none');
	$smarty->assign('disp2','block');
	
	
	  
	$id_mhs=$_GET['id_mhs'];
	$kode=$_GET['kode'];
	$smt=$_GET['smt'];

	$upd_mkpd=getData("select nim_mhs,nm_pengguna,kd_mata_kuliah,judul_mkpd,
				PENGAMBILAN_MK.id_semester,thn_akademik_semester,nm_semester,pengambilan_mk.id_mhs 
				from PENGAMBILAN_MK
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
				left join pengguna on MAHASISWA.id_pengguna=PENGGUNA.id_pengguna
				left join kurikulum_mk on PENGAMBILAN_MK.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk 
				left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
				left join DETAIL_MKPD on PENGAMBILAN_MK.id_mhs=DETAIL_MKPD.id_mhs and MATA_KULIAH.kd_mata_kuliah=DETAIL_MKPD.tipe
				left join semester on PENGAMBILAN_MK.id_semester=SEMESTER.id_semester
				where mahasiswa.id_program_studi=$kdprodi and KURIKULUM_MK.status_mkta=10 and DETAIL_MKPD.status=1
				and pengambilan_mk.id_mhs=$id_mhs and MATA_KULIAH.kd_mata_kuliah='$kode'");
	
	
	$smarty->assign('T_UPD_MKPD', $upd_mkpd);	

}

if ($smtpil==0)
{
$mkpd=getData("select nim_mhs,nm_pengguna,kd_mata_kuliah,judul_mkpd,
				PENGAMBILAN_MK.id_semester,thn_akademik_semester,nm_semester,pengambilan_mk.id_mhs 
				from PENGAMBILAN_MK
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
				left join pengguna on MAHASISWA.id_pengguna=PENGGUNA.id_pengguna
				left join kurikulum_mk on PENGAMBILAN_MK.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk 
				left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
				left join DETAIL_MKPD on PENGAMBILAN_MK.id_mhs=DETAIL_MKPD.id_mhs and MATA_KULIAH.kd_mata_kuliah=DETAIL_MKPD.tipe
				left join semester on PENGAMBILAN_MK.id_semester=SEMESTER.id_semester
				where mahasiswa.id_program_studi=$kdprodi and KURIKULUM_MK.status_mkta=10 and DETAIL_MKPD.status=1
				order by nim_mhs,kd_mata_kuliah");
} else
{
$mkpd=getData("select nim_mhs,nm_pengguna,kd_mata_kuliah,judul_mkpd,
				PENGAMBILAN_MK.id_semester,thn_akademik_semester,nm_semester,pengambilan_mk.id_mhs 
				from PENGAMBILAN_MK
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
				left join pengguna on MAHASISWA.id_pengguna=PENGGUNA.id_pengguna
				left join kurikulum_mk on PENGAMBILAN_MK.id_kurikulum_mk=KURIKULUM_MK.id_kurikulum_mk 
				left join mata_kuliah on KURIKULUM_MK.id_mata_kuliah=MATA_KULIAH.id_mata_kuliah
				left join DETAIL_MKPD on PENGAMBILAN_MK.id_mhs=DETAIL_MKPD.id_mhs and MATA_KULIAH.kd_mata_kuliah=DETAIL_MKPD.tipe
				left join semester on PENGAMBILAN_MK.id_semester=SEMESTER.id_semester
				where mahasiswa.id_program_studi=$kdprodi and KURIKULUM_MK.status_mkta=10 and DETAIL_MKPD.status=1
				and pengambilan_mk.id_semester=$smtpil
				order by nim_mhs,kd_mata_kuliah");
}
$smarty->assign('T_MKPD', $mkpd);				
$smarty->display('mkpd.tpl');
?>
