<?php
require 'common.php';
require_once 'ociFunction.php';

$kdprodi = $user->ID_PROGRAM_STUDI;
$kdfak = $user->ID_FAKULTAS;

$smarty->assign('disp1', 'block');
$smarty->assign('disp2', 'none');


$db->Query("SELECT id_semester from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND status_aktif_semester='True'");
$smtaktif = $db->FetchAssoc();

$smtpil = isset($_REQUEST['smt']) ? $_REQUEST['smt'] : $smtaktif['ID_SEMESTER'];

$smarty->assign('id_smt', $smtpil);

$db->Query("SELECT id_kurikulum 
	from kurikulum
	where id_program_studi=$kdprodi");
$idkur = $db->FetchAssoc();
$smarty->assign('idkur', $idkur['ID_KURIKULUM']);

$smt = getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' order by thn_akademik_semester desc,nm_semester desc");

$smarty->assign('T_ST', $smt);

$datakur = getData(
	"SELECT id_kurikulum,nm_kurikulum||'-'||thn_kurikulum as nama 
	from kurikulum
	where id_program_studi=$kdprodi order by thn_kurikulum desc");
$smarty->assign('T_KUR', $datakur);

$status = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'tampil';

switch ($status)
{
	case 'add':
		// pilih
		$smarty->assign('disp1', 'none');
		$smarty->assign('disp2', 'block');

		$id_mk = $_GET['id_mk'];

		tambahdata("krs_prodi", "id_kelas_mk,id_semester,id_program_studi", "'$id_mk','$smtpil','$kdprodi'");

		break;

	case 'del':
		// pilih
		$id_krs_prodi = $_GET['id_krs_prodi'];

		deleteData("DELETE from krs_prodi where id_krs_prodi='$id_krs_prodi'");

		$jaf = getData(
			"SELECT distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
			kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester,
			coalesce(jadwal_kelas.id_jenis_pertemuan,0) as id_jenis_pertemuan,id_krs_prodi,
			jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
			jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
			ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
			no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,
			thn_kurikulum,nm_jenjang||'-'||nm_program_studi as prodiasal
			from KRS_PRODI
			left join kelas_mk on KRS_PRODI.id_kelas_mk=KELAS_MK.id_kelas_mk and KRS_PRODI.id_semester=KELAS_MK.id_semester 
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join kurikulum on kurikulum_mk.id_kurikulum=kurikulum.id_kurikulum
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
			left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
			left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
			left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
			left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
			left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
			where krs_prodi.id_program_studi=$kdprodi and krs_prodi.id_semester='{$smtpil}'");

		$smarty->assign('T_MK', $jaf);

		break;

	case 'penawaran':

		$id_kur = $_POST['thkur'];

		$smarty->assign('disp1', 'none');
		$smarty->assign('disp2', 'block');

		break;

	case 'tampil':
		$id_kur1 = isset($_POST['thkur']) ? $_POST['thkur'] : '0';

		if (!isset($kdprodi) || empty($kdprodi))
			$kdprodi = 0;
		
		if (!isset($smtpil) || empty($smtpil))
			$smtpil = 0;
		
		if (!isset($kdfak) || empty($kdfak))
			$kdfak = 0;
		
		$jaf = getData(
			"SELECT distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
			kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester,
			coalesce(jadwal_kelas.id_jenis_pertemuan,0) as id_jenis_pertemuan,id_krs_prodi,
			jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
			jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
			ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
			no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,
			thn_kurikulum,nm_jenjang||'-'||nm_program_studi as prodiasal
			from KRS_PRODI
			left join kelas_mk on KRS_PRODI.id_kelas_mk=KELAS_MK.id_kelas_mk and KRS_PRODI.id_semester=KELAS_MK.id_semester 
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join kurikulum on kurikulum_mk.id_kurikulum=kurikulum.id_kurikulum
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
			left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
			left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
			left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
			left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
			left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
			where krs_prodi.id_program_studi=$kdprodi and krs_prodi.id_semester='{$smtpil}'");
		

		$smarty->assign('T_MK', $jaf);
		break;
}

$tawar = getData(
	"SELECT kelas_mk.id_kelas_mk, kelas_mk.id_kurikulum_mk, kd_mata_kuliah, nm_mata_kuliah, 
	kurikulum_mk.kredit_semester, nama_kelas, program_studi.id_program_studi, 
	nm_jenjang||'-'||nm_program_studi as prodiasal 
	from kelas_mk
	left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
	left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
	left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
	left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
	left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
	where 
		kelas_mk.id_semester = $smtpil and 
		kelas_mk.id_program_studi = $kdprodi and 
		mata_kuliah.kd_mata_kuliah in (
			select kd_mata_kuliah from kurikulum_mk
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			where kurikulum_mk.id_program_studi = $kdprodi
		) and 
		id_kelas_mk not in (
			select id_kelas_mk from krs_prodi where id_program_studi = $kdprodi and id_semester = $smtpil
		)");

$smarty->assign('TAWAR', $tawar);
$smarty->display('krs-tawar.tpl');