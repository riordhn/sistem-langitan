<?php
include '../../config.php';

$akademik = new Akademik($db);

$mode = empty($_GET['mode']) ? 'list' : $_GET['mode'];

if ($mode == 'list')
{
	$angkatan_mhs_set = $akademik->get_angkatan_mhs($user->ID_PROGRAM_STUDI);
	$thn_angkatan_mhs = (int)$_GET['thn_angkatan_mhs'];
	if(empty($thn_angkatan_mhs)){
		$mhs_set = $akademik->mhs_nilai_kosong($user->ID_PROGRAM_STUDI);
	}
	else{
		$mhs_set = $akademik->mhs_nilai_kosong($user->ID_PROGRAM_STUDI, $thn_angkatan_mhs);
	}

	// Assignment ke template
	$smarty->assign('angkatan_mhs_set', $angkatan_mhs_set);
	$smarty->assign('thn_angkatan_mhs', $thn_angkatan_mhs);
	$smarty->assign('mhs_set', $mhs_set);
}

if($mode == 'detail-mhs'){
	$id_mhs = (int)$_GET['id_mhs'];

	$data_mhs = $akademik->get_data_mhs($id_mhs);
	$matkul_set = $akademik->get_data_nilai_kosong($id_mhs);

	// Assignment ke template
	$smarty->assign('data_mhs', $data_mhs);	
	$smarty->assign('matkul_set', $matkul_set);
}

$smarty->assign('mode', $mode);

$smarty->display('monitoring/mhs-nilai-kosong.tpl');