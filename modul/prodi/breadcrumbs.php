<?php
include('config.php');

$location = explode("-", $_GET['location']);

foreach ($user->MODULs as $modul) {
    if ($modul['NM_MODUL'] == $location[0]) {
        $smarty->assign('modul', $modul);
        
        foreach ($modul['MENUs'] as $menu) {
            if (isset($location[1])) {
                if ($menu['NM_MENU'] == $location[1]) {
                    $smarty->assign('menu', $menu);
                }
            }
            else {
                $smarty->assign('menu', $modul['MENUs'][0]);
            }
        }
    }
}

$smarty->display('breadcrumbs.tpl');
?>
<script>
var lukman=0;
$.get('/isloggedin.php', function(data) {
  if(data=='1'){
        lukman=1;
    }
    else{
        alert('Maaf, Sesi login anda telah habis. Silahkan melakukan login ulang. Terimakasih.');
        location.href="/logout.php";
    }
});
</script>
