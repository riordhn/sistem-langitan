<?php
include '../../config.php';

$akademik = new Akademik($db);

$semester_aktif	= $PT->get_semester_aktif();
$id_semester	= empty($_GET['id_semester']) ? $semester_aktif['ID_SEMESTER'] : (int)$_GET['id_semester'];
$semester_set	= $PT->list_semester();
$kelas_set		= $akademik->list_kelas_per_prodi($user->ID_PROGRAM_STUDI, $id_semester);

// Assignment ke template
$smarty->assign('semester_set', $semester_set);
$smarty->assign('id_semester_terpilih', $id_semester);
$smarty->assign('kelas_set', $kelas_set);

$smarty->display('monitoring/nilai.tpl');