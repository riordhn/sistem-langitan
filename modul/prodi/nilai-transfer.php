<?php
require('common.php');
require_once ('ociFunction.php');
$kd_fak= $user->ID_FAKULTAS;
$kd_prodi= $user->ID_PROGRAM_STUDI;
$id_pengguna= $user->ID_PENGGUNA; 

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pt = $id_pt_user;

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = '{$id_pt}'");



if (($_GET['action'])=='viewproses') {
	$smarty->assign('disp1','none');
	$smarty->assign('disp2','block');

	//$id_log =$_GET['id_log'];
	$id_mhs =$_GET['id_mhs'];
	$id_semester =$_GET['smt'];

	/*$datamhs=getvar("select nim_mhs,nm_pengguna,nm_jenjang||'-'||nm_program_studi as prodi,
						total_mata_kuliah,thn_akademik_semester||'-'||nm_semester as smt,m.id_program_studi,
						m.id_mhs,id_log_granted_transfer,sm.id_semester
						from log_granted_transfer lgt
						join mahasiswa m on lgt.id_mhs=m.id_mhs
						join pengguna p on m.id_pengguna=p.id_pengguna
						join program_studi ps on m.id_program_studi=ps.id_program_studi
						join jenjang j on ps.id_jenjang=j.id_jenjang
						join semester sm on lgt.semester_proses=sm.id_semester
						where id_log_granted_transfer=$id_log and lgt.id_mhs=$id_mhs and lgt.status=1");*/
	
	$datamhs=getvar("select nim_mhs,nm_pengguna,nm_jenjang||'-'||nm_program_studi as prodi,
						thn_akademik_semester||'-'||nm_semester as smt,m.id_program_studi,
						m.id_mhs,sm.id_semester,
	(select count(*) 
			from pengambilan_mk pmk 
			where pmk.id_mhs = m.id_mhs and pmk.id_semester = a.id_semester and pmk.status_transfer = 1) as TOTAL_MATKUL,
	(select coalesce(sum(kredit_semester),0) 
			from kurikulum_mk 
			join pengambilan_mk pmk on pmk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk
			where pmk.id_mhs = m.id_mhs and pmk.id_semester = a.id_semester and pmk.status_transfer = 1) as TOTAL_SKS
						from mahasiswa m
						join pengguna p on m.id_pengguna=p.id_pengguna
						join program_studi ps on m.id_program_studi=ps.id_program_studi
						join jenjang j on ps.id_jenjang=j.id_jenjang
						join admisi a on a.id_mhs = m.id_mhs and a.id_jalur is not null
						join semester sm on sm.id_semester=a.id_semester
						where m.id_mhs=$id_mhs");

	$smarty->assign('datamhs', $datamhs);

	$kurikulum_set=getData("SELECT id_kurikulum,nm_kurikulum||' ('||thn_kurikulum||')' as nama from kurikulum
								where /* status_aktif = 1 and */ id_program_studi='$datamhs[ID_PROGRAM_STUDI]'");
						
	$smarty->assign('kurikulum_set', $kurikulum_set);

	$id_kurikulum 	= (int)post('id_kurikulum', '');

	// Jika kurikulum di pilih, tampil entrian nilai
	if($id_kurikulum!=''){
		/*$kd_konversi=getData("select kd_mata_kuliah, nm_mata_kuliah, id_kurikulum_mk,kurikulum_mk.kredit_semester,coalesce(tahun_kurikulum,thn_kurikulum) as tahun
							 from kurikulum_mk
							join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi 
							join kurikulum on kurikulum.id_kurikulum = kurikulum_prodi.id_kurikulum
							join mata_kuliah on mata_kuliah.id_mata_kuliah = kurikulum_mk.id_mata_kuliah
							where kurikulum_mk.id_program_studi='$datamhs[ID_PROGRAM_STUDI]'
							order by tahun_kurikulum,kd_mata_kuliah asc");
		$smarty->assign('kd_konversi', $kd_konversi);*/
		$kd_konversi=getData("select kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.id_kurikulum_mk,kurikulum_mk.kredit_semester,thn_kurikulum as tahun, 
			pmk.nilai_huruf
							 from kurikulum_mk
							join kurikulum on kurikulum.id_kurikulum = kurikulum_mk.id_kurikulum
							join mata_kuliah on mata_kuliah.id_mata_kuliah = kurikulum_mk.id_mata_kuliah
							left join pengambilan_mk pmk on pmk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk and pmk.id_mhs = '$datamhs[ID_MHS]' and pmk.id_kelas_mk is null
							left join admisi on admisi.id_mhs = pmk.id_mhs and admisi.id_jalur is not null and admisi.id_semester = pmk.id_semester
							where kurikulum.id_kurikulum='{$id_kurikulum}'
							order by nm_mata_kuliah asc");

		$smarty->assign('kd_konversi', $kd_konversi);
	}
	else // jika belum dipilih, tampil nilai yg sudah terentri order by nama
	{
		$nilai_konversi = getData(
			"SELECT 
				pmk.id_pengambilan_mk, 
				mk.kd_mata_kuliah, mk.nm_mata_kuliah, 
				coalesce(kmk.kredit_semester, mk.kredit_semester) as kredit_semester, pmk.nilai_huruf
			FROM pengambilan_mk pmk
			JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
			WHERE 
				pmk.status_transfer = 1 AND
				pmk.id_mhs = {$datamhs['ID_MHS']}
			ORDER BY nm_mata_kuliah");
		$smarty->assign('nilai_konversi', $nilai_konversi);
	}
						
}


if(($_POST['action'])=='proses'){

	$counter=$_POST['counter'];
	$id_mhs=$_POST['id_mhs'];
	$smt_diakui=$_POST['semester_diakui'];
	/*$log_granted=$_POST['log_granted'];*/
	
	/*$ip = $_SERVER['REMOTE_ADDR'];*/
	
	for ($i=1; $i<=$counter; $i++)
	{
		$id_kurikulum=$_POST['kd_konversi'.$i];
		$nilai=strtoupper($_POST['nilai'.$i]);
				
		$pengambilan=getvar("SELECT id_pengambilan_mk, nilai_huruf 
								from pengambilan_mk 
								where id_mhs = '{$id_mhs}' and id_kurikulum_mk = '{$id_kurikulum}' and id_semester = '{$smt_diakui}' and status_transfer = '1'");
		
		if(!empty($_POST['nilai'.$i])){
			if($nilai!= 'A' and $nilai  != 'AB' and $nilai  != 'B' and $nilai  != 'BC' and $nilai  != 'C' and $nilai  != 'D' and $nilai  != 'E'){
				echo '<script>alert("Masukkan Nilai Huruf [A/AB/B/BC/C/D/E]")</script>';
			}else{

				if(empty($pengambilan['NILAI_HURUF'])){
					InsertData("insert into pengambilan_mk (id_mhs, id_semester,id_kurikulum_mk,nilai_huruf,status_apv_pengambilan_mk,status_pengambilan_mk,flagnilai,keterangan,status_transfer)
					values ($id_mhs,$smt_diakui,$id_kurikulum,'$nilai',1,9,'1','INSERT NILAI','1')");
				}
				else{
					UpdateData("update pengambilan_mk set nilai_huruf = '$nilai' where id_pengambilan_mk = '$pengambilan[ID_PENGAMBILAN_MK]'");
				}
				
				/*InsertData("insert into log_transfer_nilai(id_pengambilan_mk,id_kurikulum_mk,id_mhs,nilai,id_semester,id_pengguna,id_log_granted_transfer,ip_address,tanggal_insert)
				select id_pengambilan_mk,id_kurikulum_mk,id_mhs,nilai_huruf,id_semester,'$id_pengguna','$log_granted','$ip',sysdate 
				from pengambilan_mk where id_mhs=$id_mhs and id_semester=$smt_diakui and id_kurikulum_mk=$id_kurikulum and nilai_huruf='$nilai' and keterangan='INSERT NILAI'");*/
			/*
			echo "insert into pengambilan_mk (id_mhs, id_semester,id_kurikulum_mk,nilai_huruf,status_apv_pengambilan_mk,status_pengambilan_mk,flagnilai,keterangan)
				values ($id_mhs,$smt_diakui,$id_kurikulum,'$nilai',1,9,'1','INSERT NILAI')";
				
			echo "insert into log_transfer_nilai(id_pengambilan_mk,id_kurikulum_mk,id_mhs,nilai,id_semester,id_pengguna,id_log_granted_transfer,ip_address,tanggal_insert)
			select id_pengambilan_mk,id_kurikulum_mk,id_mhs,nilai_huruf,id_semester,'$id_pengguna','$log_granted','$ip',sysdate 
			from pengambilan_mk where id_mhs=$id_mhs and id_semester=$smt_diakui and id_kurikulum_mk=$id_kurikulum and nilai_huruf='$nilai' and keterangan='INSERT NILAI'";
			*/
			}
		}
		else{

			if(!empty($pengambilan['ID_PENGAMBILAN_MK'])){
					deleteData("DELETE FROM PENGAMBILAN_MK WHERE ID_PENGAMBILAN_MK = '$pengambilan[ID_PENGAMBILAN_MK]'");
			}

		}
	}

	$nilai_konversi = getData(
			"SELECT 
				pmk.id_pengambilan_mk, 
				mk.kd_mata_kuliah, mk.nm_mata_kuliah, 
				coalesce(kmk.kredit_semester, mk.kredit_semester) as kredit_semester, pmk.nilai_huruf
			FROM pengambilan_mk pmk
			JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
			WHERE 
				pmk.status_transfer = 1 AND
				pmk.id_mhs = {$datamhs['ID_MHS']}
			ORDER BY nm_mata_kuliah");
	$smarty->assign('nilai_konversi', $nilai_konversi);
	
	/*UpdateData("update log_granted_transfer set status=0 where id_log_granted_transfer=$log_granted");*/
}


$nim =$_REQUEST['nim'];
$smarty->assign('nim', $nim);

/*$detail_transfer=getData("select nim_mhs,nm_pengguna,nm_jenjang||'-'||nm_program_studi as prodi,
						total_mata_kuliah,lgt.status,coalesce(s1.ttlinsert,0) as  ttlinsert,
						lgt.id_log_granted_transfer,lgt.id_mhs,lgt.semester_proses
						from log_granted_transfer lgt
						join mahasiswa m on lgt.id_mhs=m.id_mhs
						join pengguna p on m.id_pengguna=p.id_pengguna
						join program_studi ps on m.id_program_studi=ps.id_program_studi
						join jenjang j on ps.id_jenjang=j.id_jenjang
						left join 
						(select id_log_granted_transfer,count(*) as ttlinsert from log_transfer_nilai
						where id_mhs in (select id_mhs from program_studi where id_fakultas=$kd_fak) 
						group by id_log_granted_transfer) s1 on lgt.id_log_granted_transfer=s1.id_log_granted_transfer
						where ps.id_fakultas=$kd_fak
						order by tanggal");*/

$detail_transfer=getData("select m.id_mhs,nim_mhs,nm_pengguna,nm_jenjang||'-'||nm_program_studi as prodi, jl.nm_jalur, a.id_semester as semester_proses,
	(select count(*) 
			from pengambilan_mk pmk 
			where pmk.id_mhs = m.id_mhs and pmk.id_semester = a.id_semester and pmk.status_transfer = 1) as TOTAL_MATKUL,
	(select coalesce(sum(kredit_semester),0) 
			from kurikulum_mk 
			join pengambilan_mk pmk on pmk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk
			where pmk.id_mhs = m.id_mhs and pmk.id_semester = a.id_semester and pmk.status_transfer = 1) as TOTAL_SKS
						from mahasiswa m
						join pengguna p on m.id_pengguna=p.id_pengguna
						join program_studi ps on m.id_program_studi=ps.id_program_studi
						join jenjang j on ps.id_jenjang=j.id_jenjang
						join admisi a on a.id_mhs = m.id_mhs and a.id_jalur is not null
						join jalur jl on jl.id_jalur = a.id_jalur and jl.kode_jalur in ('PINDAHAN','LINTAS_JALUR','ALIH_JENJANG')
						where ps.id_program_studi=$kd_prodi
						order by nim_mhs");
	
$smarty->assign('detail_transfer', $detail_transfer);


$smarty->display('nilai-transfer.tpl');