<?php
include 'config.php';

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

$id_prodi = $user->ID_PROGRAM_STUDI;

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_mk_penyetaraan = post('id_mk_penyetaraan');
        $nm_mk_penyetaraan = post('nm_mk_penyetaraan');
        
        $result = $db->Query("update mk_penyetaraan set nm_mk_penyetaraan = '{$nm_mk_penyetaraan}'
        						where id_mk_penyetaraan = '{$id_mk_penyetaraan}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }
    if (post('mode') == 'add')
    {
        //$id_unit_kerja = post('id_unit_kerja');
        $nm_mk_penyetaraan = post('nm_mk_penyetaraan');
        
        $result = $db->Query("insert into mk_penyetaraan (nm_mk_penyetaraan, id_program_studi) 
        						values ('{$nm_mk_penyetaraan}', '{$id_prodi}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }
    if (post('mode') == 'delete')
    {
        $id_mk_penyetaraan = post('id_mk_penyetaraan');
        
        $result = $db->Query("update mk_penyetaraan set is_deleted = 1
                                where id_mk_penyetaraan = '{$id_mk_penyetaraan}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$mk_penyetaraan_set = $db->QueryToArray("SELECT MK_PENYETARAAN.*, 
                                                            (SELECT SUM(ID_MK_PENYETARAAN) FROM MK_SETARA ms WHERE ms.ID_MK_PENYETARAAN = MK_PENYETARAAN.ID_MK_PENYETARAAN) AS JML_JOIN 
                                                        FROM MK_PENYETARAAN WHERE ID_PROGRAM_STUDI = '{$id_prodi}' AND IS_DELETED = 0");
		$smarty->assignByRef('mk_penyetaraan_set', $mk_penyetaraan_set);
	}
	else if ($mode == 'edit' or $mode == 'delete')
	{
		$id_mk_penyetaraan 	= (int)get('id_mk_penyetaraan', '');

		$mk_penyetaraan = $db->QueryToArray("
            select * 
            from mk_penyetaraan 
            where id_mk_penyetaraan = {$id_mk_penyetaraan}");
        $smarty->assign('mk_penyetaraan', $mk_penyetaraan[0]);

	}
	else if($mode == 'add')
	{
		
	}
}

$smarty->display("penyetaraan-mk/master/{$mode}.tpl");