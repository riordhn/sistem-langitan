<?php
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1', 'block');
$smarty->assign('disp2', 'none');
$smarty->assign('disp3', 'none');

$id_pengguna = $user->ID_PENGGUNA;
$kdfak = $user->ID_FAKULTAS;
$kdprodi = $user->ID_PROGRAM_STUDI;
$smarty->assign('prodi', $kdprodi);

$db->Query("SELECT id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}");
$smtaktif = $db->FetchAssoc();
$smtpil = isset($_GET['smt']) ? $_GET['smt'] : $smtaktif['ID_SEMESTER'];
$smarty->assign('id_smt', $smtpil);


$smt = getData("SELECT id_semester, tahun_ajaran||' - '||nm_semester as smt from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' and tipe_semester IN ('REG', 'SP') order by thn_akademik_semester desc,nm_semester desc");
$smarty->assign('T_ST', $smt);

$status = isset($_GET['action']) ? $_GET['action'] : 'tampil';
//echo $status;

switch ($status)
{
	case 'kapasitas':
		// pilih
		$smarty->assign('disp1', 'none');
		$smarty->assign('disp2', 'block');
		$smarty->assign('disp3', 'none');

		/*if ($kdprodi == 228)
		{
			$id_krs_prodi = $_GET['id_krs_prodi'];

			$jaf1 = getData("select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
						nama_kelas,quota as kapasitas_kelas_mk,id_krs_prodi
						from krs_prodi 
						join kelas_mk on krs_prodi.id_kelaS_mk=kelaS_mk.id_kelas_mk
						left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
						left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
						where id_krs_prodi=$id_krs_prodi");
		}
		else
		{*/
			$id_mk = $_GET['id_kelas_mk'];

			$jaf1 = getData("select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
						nama_kelas,kapasitas_kelas_mk
						from kelas_mk
						left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
						left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
						where id_kelas_mk=$id_mk");
		/*}*/
		$smarty->assign('TJAF1', $jaf1);
		break;

	case 'detail':
		// pilih
		$smarty->assign('disp1', 'none');
		$smarty->assign('disp2', 'none');
		$smarty->assign('disp3', 'block');


		$id_mk = $_GET['id_kelas_mk'];

		$info = getData(
			"select kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas,
			trim(listagg(''||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||' '||(case when pengampu_mk.pjmk_pengampu_mk=1 then '(PJMA)' when pengampu_mk.pjmk_pengampu_mk=0 then '(ANGGOTA)' else '' end),'<li>') within group (order by nm_pengguna)) as tim
			from kelas_mk 
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
			left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
			left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
			left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
			where kelas_mk.id_semester='" . $smtpil . "' and kelas_mk.id_kelas_mk=$id_mk
			group by kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas");
		$smarty->assign('T_INFO', $info);

		$detail = getData(
			"select distinct pengambilan_mk.id_mhs,nim_mhs,nm_pengguna,nm_jenjang || ' - ' || nm_program_studi as nm_program_studi,
			case when status_apv_pengambilan_mk=1 then 'Disetujui' else 'Blm Disetujui' end as status
			from pengambilan_mk
			left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
			left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
			left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
			left join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			where id_kelas_mk=$id_mk
			order by nim_mhs");
		$smarty->assign('T_DETAIL', $detail);
		break;


	case 'add':
		// pilih
		$smarty->assign('disp1', 'blok');
		$smarty->assign('disp2', 'none');
		$smarty->assign('disp3', 'none');


		$id_mk = $_POST['id_kelas_mk'];
		$kap_kelas = $_POST['kap_kelas'];

		/*if ($kdprodi == 228)
		{
			$id_krs_prodi = $_POST['id_krs_prodi'];
			UpdateData("update krs_prodi set quota=$kap_kelas where id_krs_prodi=$id_krs_prodi");
		}
		else
		{*/
			UpdateData("update kelas_mk set kapasitas_kelas_mk=$kap_kelas where id_kelas_mk=$id_mk");
		/*}*/

		if ($kdfak == 6.5) //tidak dipakai
		{
			$jaf = getData(
				"select kelas_mk.id_kelas_mk, kelas_mk.id_semester, kelas_mk.id_kurikulum_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester,
				nama_kelas.nama_kelas, kelas_mk.kapasitas_kelas_mk, sum(case when pengambilan_mk.status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi, kelas_mk.status,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam,
				a.jam_mulai||':'||a.menit_mulai||'-'||b.jam_selesai||':'||b.menit_selesai as jam
				from kelas_mk
				left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
				left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
				left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
				left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
				left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
				left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
				left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
				left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
				left join jadwal_jam a on jadwal_kelas.id_jadwal_jam=a.id_jadwal_jam
				left join jadwal_jam b on a.kdjam+KURIKULUM_MK.kredit_semester-1=b.kdjam and b.id_fakultas=$kdfak
				where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_semester='" . $smtpil . "'
				group by kelas_mk.id_kelas_mk, kelas_mk.id_semester, kelas_mk.id_kurikulum_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester,
				nama_kelas.nama_kelas, kelas_mk.kapasitas_kelas_mk, kelas_mk.status, jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam,
				a.jam_mulai||':'||a.menit_mulai||'-'||b.jam_selesai||':'||b.menit_selesai");
		}
		else
		{

			$jaf = getData(
				"select kelas_mk.id_kelas_mk, kelas_mk.id_semester, kelas_mk.id_kurikulum_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester,
				nama_kelas.nama_kelas, kelas_mk.kapasitas_kelas_mk, sum(case when pengambilan_mk.status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi, jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam,
				jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam
				from kelas_mk
				left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
				left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
				left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
				left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
				left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
				left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
				left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
				left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
				left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
				where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_semester='" . $smtpil . "'
				group by kelas_mk.id_kelas_mk, kelas_mk.id_semester, kelas_mk.id_kurikulum_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester,
				nama_kelas.nama_kelas, kelas_mk.kapasitas_kelas_mk, jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam,
				jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai
				order by nama_kelas.nama_kelas");
		}
		$smarty->assign('T_MK', $jaf);

		/*if ($kdprodi == 228)
		{
			$jafpengikut = getData(
				"select id_krs_prodi,kelas_mk.id_kelas_mk,
				nm_jenjang||'-'||nm_program_studi as pengikut,quota,
				sum(case when pengambilan_mk.status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi1
				from  krs_prodi
				left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk
				left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
				left join program_studi on krs_prodi.id_program_studi=program_studi.id_program_studi
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_semester='" . $smtpil . "'
				GROUP BY id_krs_prodi,kelas_mk.id_kelas_mk,nm_jenjang,nm_program_studi,quota
				order by nm_jenjang,nm_program_studi");
			$smarty->assign('T_MKPENGIKUT', $jafpengikut);
		}*/
		break;


	case 'tampil':

		if ($kdfak == 0.5)  // aslinya 6, diabaikan
		{
			$jaf = getData(
				"select kelas_mk.id_kelas_mk, kelas_mk.id_semester, kelas_mk.id_kurikulum_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester,
				nama_kelas.nama_kelas, kelas_mk.kapasitas_kelas_mk, sum(case when pengambilan_mk.status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi, kelas_mk.status,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam,
				a.jam_mulai||':'||a.menit_mulai||'-'||b.jam_selesai||':'||b.menit_selesai as jam
				from kelas_mk
				left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
				left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
				left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
				left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
				left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
				left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
				left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
				left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
				left join jadwal_jam a on jadwal_kelas.id_jadwal_jam=a.id_jadwal_jam
				left join jadwal_jam b on a.kdjam+KURIKULUM_MK.kredit_semester-1=b.kdjam and b.id_fakultas=$kdfak
				where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_semester='" . $smtpil . "'
				group by kelas_mk.id_kelas_mk, kelas_mk.id_semester, kelas_mk.id_kurikulum_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester,
				nama_kelas.nama_kelas, kelas_mk.kapasitas_kelas_mk, kelas_mk.status, jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam,
				a.jam_mulai||':'||a.menit_mulai||'-'||b.jam_selesai||':'||b.menit_selesai");
		}
		else
		{

			$jaf = getData("select kelas_mk.id_kelas_mk, kelas_mk.id_semester, kelas_mk.id_kurikulum_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester,
					nama_kelas.nama_kelas, kelas_mk.kapasitas_kelas_mk, sum(case when pengambilan_mk.status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi,jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam,
					jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam
					from kelas_mk
					left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
					left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
					left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
					left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
					left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
					left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
					left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
					where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_semester='" . $smtpil . "'
					group by kelas_mk.id_kelas_mk, kelas_mk.id_semester, kelas_mk.id_kurikulum_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester,
					nama_kelas.nama_kelas, kelas_mk.kapasitas_kelas_mk, jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam,
					jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai
					order by jadwal_kelas.id_jadwal_hari, jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai, nama_kelas.nama_kelas
					");
		}
		
		$smarty->assign('T_MK', $jaf);

		/*if ($kdprodi == 228)
		{
			$jafpengikut = getData(
				"select id_krs_prodi,kelas_mk.id_kelas_mk,
				nm_jenjang||'-'||nm_program_studi as pengikut,quota,
				(case when kls_terisi1 is not null then kls_terisi1 else 0 end) as kls_terisi1
				from  krs_prodi
				left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk
				left join program_studi on krs_prodi.id_program_studi=program_studi.id_program_studi
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				left join (
					select id_program_studi, id_kelas_mk, sum(case when pengambilan_mk.status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi1
					from pengambilan_mk 
					join mahasiswa on mahasiswa.id_mhs = pengambilan_mk.id_mhs
					where id_semester = " . $smtpil . "
					group by id_program_studi, id_kelas_mk
				) a on a.id_program_studi = program_studi.id_program_studi and a.id_kelas_mk = kelas_mk.id_kelas_mk
				where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_semester='" . $smtpil . "'
				GROUP BY id_krs_prodi,kelas_mk.id_kelas_mk,nm_jenjang,nm_program_studi,quota,kls_terisi1
				order by nm_jenjang,nm_program_studi");
			$smarty->assign('T_MKPENGIKUT', $jafpengikut);
		}*/

		break;
}

$smarty->display('monitor-krs.tpl');
