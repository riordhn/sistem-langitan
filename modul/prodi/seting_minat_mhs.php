<?php
/*
Yudi Sulistya 14/09/2012
*/

error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');


$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$kdprodi=$user->ID_PROGRAM_STUDI;
$kdfak=$user->ID_FAKULTAS;
$kddep=getvar("select id_departemen from program_studi where id_program_studi=$kdprodi");

$default=getvar("select angkatan from
(select distinct thn_angkatan_mhs as angkatan from mahasiswa
where id_program_studi=$kdprodi and status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
order by thn_angkatan_mhs)
where rownum=1");

$minat=getData("select id_prodi_minat, nm_prodi_minat from prodi_minat where id_program_studi=$kdprodi order by nm_prodi_minat");
$smarty->assign('T_MINAT', $minat);

$angkatan=getData("select distinct thn_angkatan_mhs as angkatan from mahasiswa
where id_program_studi=$kdprodi and status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
order by thn_angkatan_mhs");
$smarty->assign('ANG', $angkatan);
$agk = isSet($_GET['angk']) ? $_GET['angk'] : $default['ANGKATAN'];

$jaf2=getData("select nm_prodi_minat, count(distinct id_mhs) as ttl from prodi_minat
left join mahasiswa on prodi_minat.id_prodi_minat=mahasiswa.id_prodi_minat
where mahasiswa.id_program_studi=$kdprodi and id_mhs in
(select id_mhs from mahasiswa where status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1))
group by nm_prodi_minat
order by nm_prodi_minat
");
$smarty->assign('SEBPEMINATAN',$jaf2);

$jaf1=getData("select id_mhs,nim_mhs,upper(nm_pengguna) as nm_pengguna,'0' as tanda from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
where id_program_studi=$kdprodi and (id_prodi_minat is null or id_prodi_minat = '')
and status_akademik_mhs in
(select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
order by nim_mhs
");
$smarty->assign('NO_MINAT', $jaf1);

$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';


switch($status) {
case 'add':

$smarty->assign('disp1','none');
$smarty->assign('disp2','block');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$id_minat=$_POST['id_minat'];
$counter=$_POST['counter1'];

for ($i=1; $i<=$counter; $i++)
{
	if ($_POST['mhs'.$i]<>'' || $_POST['mhs'.$i]<>null) {
		UpdateData("update mahasiswa set id_prodi_minat=$id_minat where id_mhs='".$_POST['mhs'.$i]."'");
	}
}

$jaf1=getData("select id_mhs,nim_mhs,upper(nm_pengguna) as nm_pengguna,'0' as tanda from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
where id_program_studi=$kdprodi and (id_prodi_minat is null or id_prodi_minat = '')
and status_akademik_mhs in
(select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
order by nim_mhs
");
$smarty->assign('NO_MINAT', $jaf1);

break;

case 'update':

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');
$id_mhs=$_POST['id_mhs'];
$id_minat=$_POST['id_minat'];

UpdateData("update mahasiswa set id_prodi_minat=$id_minat where id_mhs=$id_mhs");

$jaf=getData("select mahasiswa.id_mhs,nim_mhs,upper(pengguna.nm_pengguna) as mhs,nm_prodi_minat as peminatan from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join prodi_minat on prodi_minat.id_prodi_minat=mahasiswa.id_prodi_minat
where mahasiswa.id_program_studi=$kdprodi and
mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
and mahasiswa.ID_MHS=$id_mhs");
$smarty->assign('MINAT1', $jaf);

break;

case 'viewupdate':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','block');
$smarty->assign('disp4','none');
$id_mhs= $_GET['id_mhs'];

$jaf=getData("select mahasiswa.id_mhs,nim_mhs,upper(pengguna.nm_pengguna) as mhs,nm_prodi_minat as peminatan from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join prodi_minat on prodi_minat.id_prodi_minat=mahasiswa.id_prodi_minat
where mahasiswa.id_program_studi=$kdprodi and
mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
and mahasiswa.ID_MHS=$id_mhs");
$smarty->assign('MINAT', $jaf);

break;

case 'tampil':
$jaf=getData("select mahasiswa.id_mhs,nim_mhs,upper(pengguna.nm_pengguna) as mhs,nm_prodi_minat as peminatan from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join prodi_minat on prodi_minat.id_prodi_minat=mahasiswa.id_prodi_minat
where mahasiswa.id_program_studi=$kdprodi and mahasiswa.thn_angkatan_mhs=$agk and
mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
and mahasiswa.thn_angkatan_mhs=$agk");
$smarty->assign('MINAT1', $jaf);

break;

}


if(isset($_REQUEST['id_mhs']) and $_REQUEST['id_mhs'] != ''){
	$where = "and mahasiswa.id_mhs = '$_REQUEST[id_mhs]'";
}else{
	$where = "and mahasiswa.thn_angkatan_mhs=$agk";
}


$jaf=getData("select mahasiswa.id_mhs,nim_mhs,upper(pengguna.nm_pengguna) as mhs,nm_prodi_minat as peminatan from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join prodi_minat on prodi_minat.id_prodi_minat=mahasiswa.id_prodi_minat
where mahasiswa.id_program_studi=$kdprodi and
mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
$where");
$smarty->assign('MINAT1', $jaf);
$smarty->display('seting_minat_mhs.tpl');
?>
