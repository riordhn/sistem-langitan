<?php
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');


$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');


$kdprodi= $user->ID_PROGRAM_STUDI; 

$id_thkur=getvar("SELECT id_kurikulum from kurikulum where id_program_studi=$kdprodi and status_aktif=1");
$smarty->assign('IDKURPRODI',$id_thkur['ID_KURIKULUM']);

$id_minat=getvar("SELECT id_prodi_minat from prodi_minat where id_program_studi=$kdprodi and rownum <=1");
$smarty->assign('IDPRODIMINAT',$id_minat['ID_PRODI_MINAT']);

$id_status_mk=getvar("SELECT * from status_mk order by nm_status_mk");
$smarty->assign('IDSTATUS_MK',$id_status_mk['ID_STATUS_MK']);

$datakur=getData(
	"SELECT id_kurikulum,nm_kurikulum||' ('||thn_kurikulum||')' AS nama FROM kurikulum
	where status_aktif = 1 and id_program_studi=$kdprodi");
$smarty->assign('T_KUR', $datakur);

$dataminat=getData("SELECT * from prodi_minat where id_program_studi=$kdprodi");
$smarty->assign('T_MINAT', $dataminat);

$datastatus_mk=getData("SELECT * from status_mk order by nm_status_mk");
$smarty->assign('T_STATUS_MK', $datastatus_mk);

$status = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'view';
//echo $status;

switch ($status)
{
	 case 'view':
		 //echo $id_thkur['ID_KURIKULUM_PRODI'];
		//$thkur= $_POST['thkur'];
		$thkur = isSet($_POST['thkur']) ? $_POST['thkur'] :$id_thkur['ID_KURIKULUM'];
		break;
		
	case 'del':
		 // pilih
		
		$idkmk = $_GET['id_kmk'];
		//echo $idkmk;
		deleteData("delete from group_prasyarat_mk where id_prasyarat_mk in (select id_prasyarat_mk from prasyarat_mk where id_kurikulum_mk=$idkmk)");
		deleteData("delete from prasyarat_mk where id_kurikulum_mk=$idkmk");
		break;
	
	case 'update1':
		 // pilih
		$id_km = $_POST['id_km'];		
		$status_mk = $_POST['status_mk'];
		
		UpdateData("update kurikulum_minat set id_status_mk='$status_mk' where id_kurikulum_minat=$id_km");
		break;
   
	case 'viewup':
		//$thkur= $_POST['thkur'];
		$idkm= $_GET['id_km'];
		if (trim($idkm) != '') {
		
		$smarty->assign('IDKM',$idkm);
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block'); 
		$smarty->assign('disp3','none');}

		//$datamp=getData("select * from prodi_minat where id_program_studi=$kdprodi");
		//$smarty->assign('MP_PRODI', $datamp);

		//$datakelmk=getData("select * from kelompok_mk");
		//$smarty->assign('KEL_MK', $datakelmk);

		$datamk=getData("select * from status_mk");
		$smarty->assign('ST_MK', $datamk);
		
		$mk=getData(
			"SELECT kmm.id_kurikulum_minat,kmk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_tatap_muka,
			kredit_praktikum,kmk.kredit_semester,kmm.id_status_mk,nm_status_mk,nm_kelompok_mk,kurikulum.thn_kurikulum,
			tingkat_semester from kurikulum_minat kmm
			left join kurikulum_mk kmk on kmm.id_kurikulum_mk=kmk.id_kurikulum_mk
			left join prodi_minat pm on kmm.id_prodi_minat=pm.id_prodi_minat
			left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
			left join status_mk on kmm.id_status_mk=status_mk.id_status_mk 
			left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk 
			left join kurikulum on kmk.id_kurikulum=kurikulum.id_kurikulum 
			where id_kurikulum_minat=$idkm");
		
		$smarty->assign('T_MK1', $mk);
	break;
	
	case 'setting':
		
		$id_kmk=$_GET['id_kmk'];
		$id_kp=$_GET['id_kp'];
		
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
		
		$smarty->assign('id_kmk',$id_kmk);
		$smarty->assign('id_kp',$id_kp);


		$mk=getData(
			"SELECT kmk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kmk.kredit_semester,nm_kelompok_mk,
			tingkat_semester,kurikulum.thn_kurikulum from kurikulum_mk kmk 
			left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
			left join status_mk on kmk.id_status_mk=status_mk.id_status_mk 
			left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk 
			left join kurikulum on kmk.id_kurikulum=kurikulum.id_kurikulum 
			where kmk.id_kurikulum_mk=$id_kmk");



		$smarty->assign('INSERT_MK', $mk);
		
		$mkpilih=getData(
			"SELECT kmk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kmk.kredit_semester,nm_kelompok_mk,
			tingkat_semester,kurikulum.thn_kurikulum from kurikulum_mk kmk 
			left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
			left join status_mk on kmk.id_status_mk=status_mk.id_status_mk 
			left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk 
			left join kurikulum on kmk.id_kurikulum=kurikulum.id_kurikulum 
			where kmk.id_program_studi=$kdprodi and kmk.id_kurikulum=$id_kp 
			order by tingkat_semester, nm_kelompok_mk, kd_mata_kuliah");
		$smarty->assign('MK_PILIH', $mkpilih);  
	break;
	
	case 'insert':
		$pilih=$_POST['pilih'];
		$id_kmk=$_POST['id_kmk'];
		$counter=$_POST['counter'];
	
		/*if ($pilih==1)
		{
			gantidata("insert into prasyarat_mk(id_kurikulum_mk,flag_group) values ($id_kmk,1)");
			//  echo "insert into prasyarat_mk(id_kurikulum_mk,flag_group) values ($id_kmk,1)";
			$id_pmk=getvar("select id_prasyarat_mk from prasyarat_mk where id_kurikulum_mk=$id_kmk and flag_group=1");
		}
		else
		{
			$counter1=getvar("select count(id_kurikulum_mk) as ttl from prasyarat_mk where id_kurikulum_mk=$id_kmk");

			$jumlah = $counter1['TTL']; 

			gantidata("insert into prasyarat_mk(id_kurikulum_mk,flag_group) values ($id_kmk,$jumlah+1)");
			//   echo "insert into prasyarat_mk(id_kurikulum_mk,flag_group) values ($id_kmk,$counter1[TTL]+1)";
			$id_pmk=getvar("select id_prasyarat_mk from prasyarat_mk where id_kurikulum_mk=$id_kmk and flag_group=$jumlah+1");
		}*/

		gantidata("insert into prasyarat_mk(id_kurikulum_mk,flag_group) values ($id_kmk,$pilih)");

		$id_pmk=getvar("select id_prasyarat_mk from prasyarat_mk where id_kurikulum_mk=$id_kmk and flag_group=$pilih");
		
	
		for ($i=1; $i<=$counter; $i++)
		{
			//echo $mhs['ID_MHS'];
			if ($_POST['pilihmk'.$i] <> '' || $_POST['pilihmk' . $i] <> null) 
			{
			   gantidata("insert into group_prasyarat_mk (id_prasyarat_mk,id_kurikulum_mk,min_nilai_huruf) values ($id_pmk[ID_PRASYARAT_MK],'".$_POST['pilihmk'.$i]."','D')");
			 // echo "insert into group_prasyarat_mk (id_prasyarat_mk,id_kurikulum_mk,min_nilai_huruf) values ($id_pmk[ID_PRASYARAT_MK],'".$_POST['pilihmk'.$i]."','D')";
			   //echo "update pengambilan_mk set status_cekal=0 where id_kelas_mk=$idkelas and id_mhs='".$_POST['mhs'.$i]."' and id_semester='$smtaktif[ID_SEMESTER]'";
			   //gantidata("update pengambilan_mk set id_kelas_mk=$kelas_mk_tujuan where id_pengambilan_mk='".$_POST['pil'.$i]."'");
			} 
		}
	
	break;


}

$thkur = isSet($_POST['thkur']) ? $_POST['thkur'] :$id_thkur['ID_KURIKULUM'];
$id_minat = isSet($_POST['minat']) ? $_POST['minat'] :$id_minat['ID_PRODI_MINAT'];

$smarty->assign('IDKP', $thkur);
$smarty->assign('IDM', $id_minat);

/** Old Query -- dimatikan oleh Fathoni - 20/11/2017
$mk = getData("SELECT id_kurikulum_mk,id_kurikulum_prodi,kd_mata_kuliah,nm_mata_kuliah,kredit_semester, 
case when wm_concat (group1) is null then '-' else wm_concat (group1) end as g1,
case when wm_concat (group2) is null then '-' else wm_concat (group2) end as g2,
case when wm_concat (group3) is null then '-' else wm_concat (group3) end as g3,
case when wm_concat (group4) is null then '-' else wm_concat (group4) end as g4
from (
select kmk.id_kurikulum_mk,kmk.id_kurikulum_prodi,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kmk.kredit_semester,
decode(flag_group,1,wm_concat(mk1.kd_mata_kuliah)) group1,
decode(flag_group,2,wm_concat(mk1.kd_mata_kuliah)) group2,
decode(flag_group,3,wm_concat(mk1.kd_mata_kuliah)) group3,
decode(flag_group,4,wm_concat(mk1.kd_mata_kuliah)) group4
from kurikulum_mk kmk
left join mata_kuliah mk on kmk.id_mata_kuliah=mk.id_mata_kuliah
left join prasyarat_mk on kmk.id_kurikulum_mk=prasyarat_mk.id_kurikulum_mk
left join group_prasyarat_mk on prasyarat_mk.id_prasyarat_mk=group_prasyarat_mk.id_prasyarat_mk
left join kurikulum_mk kmk1 on group_prasyarat_mk.id_kurikulum_mk=kmk1.id_kurikulum_mk
left join mata_kuliah mk1 on kmk1.id_mata_kuliah=mk1.id_mata_kuliah
where kmk.id_program_studi=$kdprodi and kmk.id_kurikulum_prodi=$thkur
group by kmk.id_kurikulum_mk,kmk.id_kurikulum_prodi,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kmk.kredit_semester,flag_group)
group by id_kurikulum_mk,id_kurikulum_prodi,kd_mata_kuliah,nm_mata_kuliah,kredit_semester
order by id_kurikulum_mk");
*/

$mk = getData(
	"SELECT 
		id_kurikulum_mk, kd_mata_kuliah, nm_mata_kuliah, kredit_semester, id_kurikulum,
		case when wm_concat (group1) is null then '-' else wm_concat (group1) end as g1,
		case when wm_concat (group2) is null then '-' else wm_concat (group2) end as g2,
		case when wm_concat (group3) is null then '-' else wm_concat (group3) end as g3,
		case when wm_concat (group4) is null then '-' else wm_concat (group4) end as g4,
		case when wm_concat (group5) is null then '-' else wm_concat (group5) end as g5
	from (
		select 
			kmk.id_kurikulum_mk, mk.kd_mata_kuliah, mk.nm_mata_kuliah, kmk.kredit_semester, kmk.id_kurikulum,
			decode(flag_group,1,wm_concat(mk1.kd_mata_kuliah)) group1,
			decode(flag_group,2,wm_concat(mk1.kd_mata_kuliah)) group2,
			decode(flag_group,3,wm_concat(mk1.kd_mata_kuliah)) group3,
			decode(flag_group,4,wm_concat(mk1.kd_mata_kuliah)) group4,
			decode(flag_group,5,wm_concat(mk1.kd_mata_kuliah)) group5
		from kurikulum_mk kmk
		left join mata_kuliah mk on kmk.id_mata_kuliah=mk.id_mata_kuliah
		left join prasyarat_mk on kmk.id_kurikulum_mk=prasyarat_mk.id_kurikulum_mk
		left join group_prasyarat_mk on prasyarat_mk.id_prasyarat_mk=group_prasyarat_mk.id_prasyarat_mk
		left join kurikulum_mk kmk1 on group_prasyarat_mk.id_kurikulum_mk=kmk1.id_kurikulum_mk
		left join mata_kuliah mk1 on kmk1.id_mata_kuliah=mk1.id_mata_kuliah
		where kmk.id_program_studi = {$kdprodi} 
		group by kmk.id_kurikulum_mk,mk.kd_mata_kuliah,mk.nm_mata_kuliah,kmk.kredit_semester,kmk.id_kurikulum,flag_group
	)
	group by id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,id_kurikulum
	order by id_kurikulum_mk");


$smarty->assign('T_MK', $mk);
$smarty->display('prasyaratmk.tpl');  

#echo '<script>alert("Maaf... fitur ini dimatikan, sedang ada cleansing TABEL KURIKULUM")</script>';