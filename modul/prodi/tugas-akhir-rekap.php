<?php

error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi= $user->ID_PROGRAM_STUDI; 
$kdfak= $user->ID_FAKULTAS;

$jenis_pembimbing=getData("select * from jenis_pembimbing order by id_jenis_pembimbing");
$smarty->assign('jenis_pembimbing', $jenis_pembimbing);

if(isset($_GET['id'])){
// Yudi Sulistya, 23-05-2013 (penambahan filter status akademik mahasiswa aktif = 1 serta selain magang/pkl != 4)
$detail=getData("
select trim(a.gelar_depan||' '||upper(a.nm_pengguna)||', '|| a.gelar_belakang) as nm_dosen, b.nm_pengguna as nm_mhs, nim_mhs, nm_jenis_pembimbing, judul_tugas_akhir, nm_tipe_ta
from pembimbing_ta
left join dosen on dosen.id_dosen = pembimbing_ta.id_dosen
left join pengguna a on a.id_pengguna = dosen.id_pengguna
left join mahasiswa on mahasiswa.id_mhs = pembimbing_ta.id_mhs	
left join pengguna b on b.id_pengguna = mahasiswa.id_pengguna		
left join jenis_pembimbing on jenis_pembimbing.id_jenis_pembimbing = pembimbing_ta.id_jenis_pembimbing  
left join tugas_akhir on tugas_akhir.id_mhs = mahasiswa.id_mhs
left join tipe_ta on tipe_ta.id_tipe_ta = tugas_akhir.id_tipe_ta
where pembimbing_ta.status_dosen = 1 and mahasiswa.id_program_studi = '$kdprodi' and pembimbing_ta.id_dosen = '$_GET[id]'
and tugas_akhir.status = 1 and mahasiswa.status_akademik_mhs = 1 and tugas_akhir.id_tipe_ta != 4	 
and pembimbing_ta.id_jenis_pembimbing = '$_GET[jenis]'
order by nim_mhs
");			
$smarty->assign('detail',$detail);	
}else{
// Yudi Sulistya, 23-05-2013 (penambahan filter status akademik mahasiswa aktif = 1 serta selain magang/pkl != 4)
$pembimbing=getData("
select nm_pengguna, id_dosen, 
max(DECODE(id_jenis_pembimbing, 1, anak_bimbing, NULL)) as Pembimbing_1,
max(DECODE(id_jenis_pembimbing, 2, anak_bimbing, NULL)) as Pembimbing_2,
max(DECODE(id_jenis_pembimbing, 3, anak_bimbing, NULL)) as Pembimbing_3,
max(DECODE(id_jenis_pembimbing, 4, anak_bimbing, NULL)) as Pembimbing_4,
max(DECODE(id_jenis_pembimbing, 5, anak_bimbing, NULL)) as Pembimbing_5
from 
(
	select trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna, 
	pembimbing_ta.id_dosen, pembimbing_ta.id_jenis_pembimbing, COUNT(*) AS ANAK_BIMBING
	from pembimbing_ta
	left join tugas_akhir on tugas_akhir.id_mhs = pembimbing_ta.id_mhs
	left join dosen on dosen.id_dosen = pembimbing_ta.id_dosen
	left join pengguna on pengguna.id_pengguna = dosen.id_pengguna
	left join mahasiswa on mahasiswa.id_mhs = pembimbing_ta.id_mhs
	where pembimbing_ta.status_dosen = 1 and mahasiswa.id_program_studi = '$kdprodi' and mahasiswa.status_akademik_mhs = 1 and tugas_akhir.id_tipe_ta != 4
	and tugas_akhir.status = 1
	group by trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang), pembimbing_ta.id_dosen, pembimbing_ta.id_jenis_pembimbing
)
group by nm_pengguna, id_dosen
order by nm_pengguna
");						
$smarty->assign('pembimbing',$pembimbing);	
}

if ($kdfak == 11) {
	$smarty->display('tugas-akhir-rekap-psikologi.tpl');
} else {
	$smarty->display('tugas-akhir-rekap.tpl');
}

?>