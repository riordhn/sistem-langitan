<?php

$id_pt = $id_pt_user;
$id_pengguna_input = $user->ID_PENGGUNA;

class penilaian
{

	public $db;
	public $id_dosen;

	function __construct($db, $id_dosen)
	{
		$this->db = $db;
		$this->id_dosen = $id_dosen;
	}

	// Fungsi Untuk Penilian Umum

	function cek_penilaian_dosen_login($id_kelas_mk)
	{
		return $this->db->QuerySingle("SELECT COUNT(*) FROM PENGAMPU_MK WHERE ID_DOSEN='{$this->id_dosen}' AND ID_KELAS_MK='{$id_kelas_mk}'");
	}

	function get_semester_kelas_mk($id_kelas_mk)
	{
		$this->db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER IN
			(SELECT ID_SEMESTER FROM KELAS_MK WHERE ID_KELAS_MK='{$id_kelas_mk}') AND ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}'");
		return $this->db->FetchAssoc();
	}

	function get_pjmk_status($id_kelas_mk)
	{
		return $this->db->QuerySingle("SELECT PJMK_PENGAMPU_MK STATUS FROM PENGAMPU_MK WHERE ID_DOSEN='{$this->id_dosen}' AND ID_KELAS_MK='{$id_kelas_mk}'");
	}

	function get_id_fakultas_kelas_mk($id_kelas_mk)
	{
		$this->db->Query("SELECT ID_FAKULTAS FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM KELAS_MK WHERE ID_KELAS_MK='{$id_kelas_mk}')");
		$kelas_mk = $this->db->FetchAssoc();
		return $kelas_mk['ID_FAKULTAS'];
	}

	function load_kelas_mata_kuliah()
	{
		//echo "SELECT * FROM SEMESTER WHERE (NM_SEMESTER='Ganjil' OR NM_SEMESTER='Genap') AND THN_AKADEMIK_SEMESTER>=2011 AND ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}' ORDER BY TAHUN_AJARAN DESC,NM_SEMESTER DESC";
		$data_kelas_mk = array();
		foreach ($this->load_semester_kelas_mk() as $semester)
		{
			array_push($data_kelas_mk, array(
				'NM_SEMESTER' => $semester['NM_SEMESTER'],
				'TAHUN_AJARAN' => $semester['TAHUN_AJARAN'],
				'DATA_KELAS_MK' => $this->db->QueryToArray(
					"SELECT 
					KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH, K2.NAMA_KELAS AS NM_KELAS,
					--NVL(K1.NMKELAS,K2.NAMA_KELAS) NM_KELAS,
					MK.KD_MATA_KULIAH,J.NM_JENJANG,INITCAP(PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI,MK.KD_MATA_KULIAH,
					S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,PENGMK.PJMK_PENGAMPU_MK,KUMK.STATUS_MKTA,MK.STATUS_PRAKTIKUM
					FROM PENGAMPU_MK PENGMK
					JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
					JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
					JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
					--LEFT JOIN FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
					LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
					JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = KMK.ID_PROGRAM_STUDI --OR K1.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
					JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
					LEFT JOIN SEMESTER S ON KMK.ID_SEMESTER = S.ID_SEMESTER
					JOIN DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
					WHERE D.ID_DOSEN='{$this->id_dosen}' AND S.ID_SEMESTER='{$semester['ID_SEMESTER']}' AND KUMK.STATUS_MKTA NOT IN (1,2)
					GROUP BY KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH,
					--K1.NMKELAS,
					K2.NAMA_KELAS,MK.KD_MATA_KULIAH,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,MK.KD_MATA_KULIAH,
					S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,PENGMK.PJMK_PENGAMPU_MK,KUMK.STATUS_MKTA,MK.STATUS_PRAKTIKUM
					ORDER BY NM_MATA_KULIAH,NM_KELAS")
			));
		}

		return $data_kelas_mk;
	}

	function load_kelas_mata_kuliah_khusus_baru()
	{
		//echo "SELECT * FROM SEMESTER WHERE (NM_SEMESTER='Ganjil' OR NM_SEMESTER='Genap') AND THN_AKADEMIK_SEMESTER>=2011 AND ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}' ORDER BY TAHUN_AJARAN DESC,NM_SEMESTER DESC";
		$data_kelas_mk = array();
		foreach ($this->load_semester_kelas_mk() as $semester)
		{
			array_push($data_kelas_mk, array(
				'NM_SEMESTER' => $semester['NM_SEMESTER'],
				'TAHUN_AJARAN' => $semester['TAHUN_AJARAN'],
				'DATA_KELAS_MK' => $this->db->QueryToArray(
					"SELECT DISTINCT KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH, K2.NAMA_KELAS AS NM_KELAS,
						MK.KD_MATA_KULIAH,J.NM_JENJANG,INITCAP(PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI,MK.KD_MATA_KULIAH,
						S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,
						JP.NM_JENIS_PEMBIMBING,
						KKMK.STATUS_MKTA,MK.STATUS_PRAKTIKUM
					FROM KELAS_MK kmk
					JOIN PENGAMBILAN_MK pmk ON pmk.ID_KELAS_MK = kmk.ID_KELAS_MK
					LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
					JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = KMK.ID_PROGRAM_STUDI
					JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
					LEFT JOIN SEMESTER S ON KMK.ID_SEMESTER = S.ID_SEMESTER
					JOIN KURIKULUM_MK kkmk ON kmk.ID_KURIKULUM_MK = kkmk.ID_KURIKULUM_MK
					JOIN MATA_KULIAH mk ON mk.ID_MATA_KULIAH = kkmk.ID_MATA_KULIAH
					JOIN TUGAS_AKHIR ta ON ta.ID_MHS = pmk.ID_MHS AND ta.STATUS = 1 AND ta.ID_TIPE_TA != 4
					JOIN PEMBIMBING_TA pta ON pta.ID_MHS = pmk.ID_MHS AND pta.STATUS_DOSEN = 1
					JOIN JENIS_PEMBIMBING jp ON jp.ID_JENIS_PEMBIMBING = pta.ID_JENIS_PEMBIMBING
					JOIN DOSEN d ON d.ID_DOSEN = pta.ID_DOSEN
					JOIN PENGGUNA p ON p.ID_PENGGUNA = d.ID_PENGGUNA
					WHERE kmk.ID_SEMESTER = '{$semester['ID_SEMESTER']}' AND STATUS_MKTA = 1 
						AND d.ID_DOSEN = '{$this->id_dosen}' AND jp.ID_JENIS_PEMBIMBING = 1
					ORDER BY NM_MATA_KULIAH,NM_KELAS")
			));
		}

		return $data_kelas_mk;
	}

	function load_kelas_mata_kuliah_kkn()
	{
		//echo "SELECT * FROM SEMESTER WHERE (NM_SEMESTER='Ganjil' OR NM_SEMESTER='Genap') AND THN_AKADEMIK_SEMESTER>=2011 AND ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}' ORDER BY TAHUN_AJARAN DESC,NM_SEMESTER DESC";
		$data_kelas_mk = array();
		foreach ($this->load_semester_kelas_mk() as $semester)
		{
			array_push($data_kelas_mk, array(
				'NM_SEMESTER' => $semester['NM_SEMESTER'],
				'TAHUN_AJARAN' => $semester['TAHUN_AJARAN'],
				'DATA_KELAS_MK' => $this->db->QueryToArray(
					"SELECT DISTINCT KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH, K2.NAMA_KELAS AS NM_KELAS,
						MK.KD_MATA_KULIAH,J.NM_JENJANG,INITCAP(PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI,MK.KD_MATA_KULIAH,
						S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,
						'DPL' AS NM_JENIS_PENILAI,
						KKMK.STATUS_MKTA,MK.STATUS_PRAKTIKUM
					FROM KELAS_MK kmk
					JOIN PENGAMBILAN_MK pmk ON pmk.ID_KELAS_MK = kmk.ID_KELAS_MK
					LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
					JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = KMK.ID_PROGRAM_STUDI
					JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
					LEFT JOIN SEMESTER S ON KMK.ID_SEMESTER = S.ID_SEMESTER
					JOIN KURIKULUM_MK kkmk ON kmk.ID_KURIKULUM_MK = kkmk.ID_KURIKULUM_MK
					JOIN MATA_KULIAH mk ON mk.ID_MATA_KULIAH = kkmk.ID_MATA_KULIAH
					JOIN KKN_KELOMPOK_MHS kkm ON kkm.ID_PENGAMBILAN_MK = pmk.ID_PENGAMBILAN_MK
					JOIN KKN_KELOMPOK kk ON kk.ID_KKN_KELOMPOK = kkm.ID_KKN_KELOMPOK
					JOIN KKN_DPL kd ON kd.ID_KKN_KELOMPOK = kk.ID_KKN_KELOMPOK
					JOIN DOSEN d ON d.ID_DOSEN = kd.ID_DOSEN
					JOIN PENGGUNA p ON p.ID_PENGGUNA = d.ID_PENGGUNA
					WHERE kmk.ID_SEMESTER = '{$semester['ID_SEMESTER']}' 
						--AND STATUS_MKTA = 1 
						AND d.ID_DOSEN = '{$this->id_dosen}' 
						--AND jp.ID_JENIS_PEMBIMBING = 1
					ORDER BY NM_MATA_KULIAH,NM_KELAS")
			));
		}

		return $data_kelas_mk;
	}  

	function get_pembimbing_status($id_kelas_mk)
	{
		return $this->db->QuerySingle("SELECT DISTINCT ID_JENIS_PEMBIMBING
										FROM KELAS_MK kmk
										JOIN PENGAMBILAN_MK pmk ON pmk.ID_KELAS_MK = kmk.ID_KELAS_MK
										JOIN PEMBIMBING_TA pta ON pta.ID_MHS = pmk.ID_MHS AND pta.STATUS_DOSEN = 1
										WHERE pta.ID_DOSEN='{$this->id_dosen}' AND kmk.ID_KELAS_MK='{$id_kelas_mk}'");
	}

	function load_data_mahasiswa_skripsi($id_kelas_mk)
	{
		$data_mahasiswa_kelas = array();
		$data_mahasiswa = $this->db->QueryToArray("
		SELECT 
			M.ID_MHS,PMK.ID_PENGAMBILAN_MK,M.NIM_MHS,P.NM_PENGGUNA,PMK.NILAI_HURUF,PMK.NILAI_ANGKA, PMK.STATUS_CEKAL,PMK.FLAGNILAI,PMK.STATUS_CEKAL_UTS 
		FROM PENGAMBILAN_MK PMK
		LEFT JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
		LEFT JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
		LEFT JOIN SEMESTER S ON S.ID_SEMESTER = PMK.ID_SEMESTER 
		JOIN PEMBIMBING_TA pta ON pta.ID_MHS = pmk.ID_MHS AND pta.STATUS_DOSEN = 1
		WHERE PMK.ID_KELAS_MK = '{$id_kelas_mk}' AND S.TIPE_SEMESTER='REG' AND PMK.STATUS_APV_PENGAMBILAN_MK=1 AND pta.ID_JENIS_PEMBIMBING = 1
			AND pta.ID_DOSEN = '{$this->id_dosen}'
		ORDER BY M.THN_ANGKATAN_MHS,M.NIM_MHS");
		foreach ($data_mahasiswa as $data)
		{
			array_push($data_mahasiswa_kelas, array(
				'id_pengambilan' => $data['ID_PENGAMBILAN_MK'],
				'nim' => $data['NIM_MHS'],
				'nama' => $data['NM_PENGGUNA'],
				'flagnilai' => $data['FLAGNILAI'],
				'status_cekal' => $data['STATUS_CEKAL'],
				'status_cekal_uts' => $data['STATUS_CEKAL_UTS'],
				'data_nilai' => $this->load_data_nilai($data['ID_MHS'], $data['ID_PENGAMBILAN_MK']),
				'nilai_angka_akhir' => $data['NILAI_ANGKA'],
				'nilai_huruf_akhir' => $data['NILAI_HURUF']
			));
		}
		return $data_mahasiswa_kelas;
	}

	function benahi_tampilan_nilai_krs_akad($id_kelas_mk, $id_semester)
	{
		// CEK KOMPONEN MK YANG TIDAK TERGENERATE
		$mhs_komponen_kurang = $this->db->QueryToArray("
			SELECT PMK.ID_MHS,PMK.ID_PENGAMBILAN_MK,KMK.ID_KELAS_MK,COUNT(DISTINCT(NMK.ID_KOMPONEN_MK)) JUMLAH_KOMPONEN
			FROM AUCC.PENGAMBILAN_MK PMK
			JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = PMK.ID_KELAS_MK
			JOIN AUCC.KOMPONEN_MK KOMK ON KOMK.ID_KELAS_MK = KMK.ID_KELAS_MK
			LEFT JOIN AUCC.NILAI_MK NMK ON PMK.ID_PENGAMBILAN_MK=NMK.ID_PENGAMBILAN_MK AND NMK.ID_MHS=PMK.ID_MHS
			WHERE KMK.ID_KELAS_MK='{$id_kelas_mk}' AND PMK.ID_SEMESTER='{$id_semester}'
			GROUP BY PMK.ID_MHS,PMK.ID_PENGAMBILAN_MK,KMK.ID_KELAS_MK
			HAVING COUNT(DISTINCT(NMK.ID_KOMPONEN_MK))<(SELECT COUNT(*) FROM AUCC.KOMPONEN_MK WHERE ID_KELAS_MK='{$id_kelas_mk}')
			");
		// JIKA ADA MAHASISWA YANG DITEMUKAN KURANG KOMPONENYA
		if (count($mhs_komponen_kurang) > 0)
		{
			foreach ($mhs_komponen_kurang as $m)
			{
				$this->db->Query("
					INSERT INTO AUCC.NILAI_MK (ID_MHS,ID_PENGAMBILAN_MK,ID_KOMPONEN_MK)
					SELECT '{$m['ID_MHS']}' ID_MHS,'{$m['ID_PENGAMBILAN_MK']}' ID_PENGAMBILAN_MK,ID_KOMPONEN_MK 
					FROM AUCC.KOMPONEN_MK 
					WHERE ID_KELAS_MK='{$id_kelas_mk}'
					AND ID_KOMPONEN_MK NOT IN (
					  SELECT ID_KOMPONEN_MK
					  FROM AUCC.NILAI_MK
					  WHERE ID_MHS='{$m['ID_MHS']}' AND ID_PENGAMBILAN_MK='{$m['ID_PENGAMBILAN_MK']}'
					)
					");
			}
		}
	}

	function load_data_pengajar_kelas_mk($id_kelas_mk)
	{
		return $this->db->QueryToArray("
			SELECT PENGMK.PJMK_PENGAMPU_MK,P.NM_PENGGUNA
				FROM PENGAMPU_MK PENGMK
			JOIN DOSEN D ON D.ID_DOSEN=PENGMK.ID_DOSEN
			JOIN PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
			WHERE PENGMK.ID_KELAS_MK='{$id_kelas_mk}'
			ORDER BY PENGMK.PJMK_PENGAMPU_MK DESC");
	}

	function load_semester_kelas_mk()
	{

		return $this->db->QueryToArray("SELECT * FROM SEMESTER WHERE (NM_SEMESTER='Ganjil' OR NM_SEMESTER='Genap') AND THN_AKADEMIK_SEMESTER>=2011 AND ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}' ORDER BY TAHUN_AJARAN DESC,NM_SEMESTER DESC");
	}

	function get_semester_aktif()
	{
		return $this->db->QuerySingle("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}'");
	}

	function get_kelas_mk($id_kelas_mk)
	{
		$this->db->Query("SELECT 
			  KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH, K2.NAMA_KELAS AS NM_KELAS,
			  --NVL(K1.NMKELAS,K2.NAMA_KELAS) NM_KELAS,
			  INITCAP(PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI,KMK.IS_DIBUKA,KMK.ID_SEMESTER,MK.KD_MATA_KULIAH
			FROM KELAS_MK KMK
			--JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
			JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KMK.ID_MATA_KULIAH
			--LEFT JOIN FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
			LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
						JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = KMK.ID_PROGRAM_STUDI --OR K1.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
			LEFT JOIN SEMESTER S ON KMK.ID_SEMESTER = S.ID_SEMESTER
			WHERE KMK.ID_KELAS_MK='{$id_kelas_mk}'
			ORDER BY NM_MATA_KULIAH,NM_KELAS");
		return $this->db->FetchAssoc();
	}

	function load_komponen_mk($id_kelas_mk)
	{
		$data_komponen_akses = $this->db->QueryToArray("
			SELECT * FROM KOMPONEN_MK WHERE ID_KOMPONEN_MK IN (SELECT ID_KOMPONEN_MK FROM AKSES_KOMPONEN_MK WHERE ID_DOSEN='{$this->id_dosen}') AND ID_KELAS_MK ='{$id_kelas_mk}' ORDER BY URUTAN_KOMPONEN_MK");
		if ($this->get_pjmk_status($id_kelas_mk) == 1 || $data_komponen_akses == null)
		{
			$data_komponen = $this->db->QueryToArray("SELECT * FROM KOMPONEN_MK WHERE ID_KELAS_MK ='{$id_kelas_mk}' ORDER BY URUTAN_KOMPONEN_MK");
		}
		else
		{
			$data_komponen = $data_komponen_akses;
		}
		return $data_komponen;
	}

	function load_data_mahasiswa($id_kelas_mk)
	{
		$data_mahasiswa_kelas = array();
		$data_mahasiswa = $this->db->QueryToArray("
		SELECT 
			M.ID_MHS,PMK.ID_PENGAMBILAN_MK,M.NIM_MHS,P.NM_PENGGUNA,PMK.NILAI_HURUF,PMK.NILAI_ANGKA, PMK.STATUS_CEKAL,PMK.FLAGNILAI,PMK.STATUS_CEKAL_UTS 
		FROM PENGAMBILAN_MK PMK
		LEFT JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
		LEFT JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
		LEFT JOIN SEMESTER S ON S.ID_SEMESTER = PMK.ID_SEMESTER 
		WHERE PMK.ID_KELAS_MK = '{$id_kelas_mk}' AND S.TIPE_SEMESTER='REG' AND PMK.STATUS_APV_PENGAMBILAN_MK=1
		ORDER BY M.THN_ANGKATAN_MHS,M.NIM_MHS");
		foreach ($data_mahasiswa as $data)
		{
			array_push($data_mahasiswa_kelas, array(
				'id_pengambilan' => $data['ID_PENGAMBILAN_MK'],
				'nim' => $data['NIM_MHS'],
				'nama' => $data['NM_PENGGUNA'],
				'flagnilai' => $data['FLAGNILAI'],
				'status_cekal' => $data['STATUS_CEKAL'],
				'status_cekal_uts' => $data['STATUS_CEKAL_UTS'],
				'data_nilai' => $this->load_data_nilai($data['ID_MHS'], $data['ID_PENGAMBILAN_MK']),
				'nilai_angka_akhir' => $data['NILAI_ANGKA'],
				'nilai_huruf_akhir' => $data['NILAI_HURUF']
			));
		}
		return $data_mahasiswa_kelas;
	}

	function load_data_mahasiswa_reg_sp($id_kelas_mk)
	{
		$data_mahasiswa_kelas = array();
		$data_mahasiswa = $this->db->QueryToArray("
		SELECT 
			M.ID_MHS,PMK.ID_PENGAMBILAN_MK,M.NIM_MHS,P.NM_PENGGUNA,PMK.NILAI_HURUF,PMK.NILAI_ANGKA, PMK.STATUS_CEKAL,PMK.FLAGNILAI,PMK.STATUS_CEKAL_UTS 
		FROM PENGAMBILAN_MK PMK
		LEFT JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
		LEFT JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
		LEFT JOIN SEMESTER S ON S.ID_SEMESTER = PMK.ID_SEMESTER 
		WHERE PMK.ID_KELAS_MK = '{$id_kelas_mk}' AND S.TIPE_SEMESTER IN ('REG','SP') AND PMK.STATUS_APV_PENGAMBILAN_MK=1
		ORDER BY M.THN_ANGKATAN_MHS,M.NIM_MHS");
		foreach ($data_mahasiswa as $data)
		{
			array_push($data_mahasiswa_kelas, array(
				'id_pengambilan' => $data['ID_PENGAMBILAN_MK'],
				'nim' => $data['NIM_MHS'],
				'nama' => $data['NM_PENGGUNA'],
				'flagnilai' => $data['FLAGNILAI'],
				'status_cekal' => $data['STATUS_CEKAL'],
				'status_cekal_uts' => $data['STATUS_CEKAL_UTS'],
				'data_nilai' => $this->load_data_nilai($data['ID_MHS'], $data['ID_PENGAMBILAN_MK']),
				'nilai_angka_akhir' => $data['NILAI_ANGKA'],
				'nilai_huruf_akhir' => $data['NILAI_HURUF']
			));
		}
		return $data_mahasiswa_kelas;
	}

	function load_dosen_mk($id_kelas_mk)
	{
		return $this->db->QueryToArray("
			SELECT P.NM_PENGGUNA,D.ID_DOSEN,KMK.ID_SEMESTER FROM PENGAMPU_MK PENGMK
			JOIN DOSEN D ON PENGMK.ID_DOSEN = D.ID_DOSEN
			JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
			JOIN KELAS_MK KMK ON PENGMK.ID_KELAS_MK = KMK.ID_KELAS_MK
			WHERE PENGMK.ID_KELAS_MK='{$id_kelas_mk}'");
	}

	function load_data_nilai($id_mhs, $id_pengambilan_mk)
	{
		$data_pengambilan = $this->get_pengambilan_mk($id_pengambilan_mk);
		$id_kelas_mk = $data_pengambilan['ID_KELAS_MK'];
		$data_nilai_akses = $this->db->QueryToArray("
			SELECT * FROM NILAI_MK NMK 
			JOIN KOMPONEN_MK KOMK ON KOMK.ID_KOMPONEN_MK = NMK.ID_KOMPONEN_MK
			WHERE NMK.ID_PENGAMBILAN_MK ='{$id_pengambilan_mk}' AND NMK.ID_MHS='{$id_mhs}'
			AND KOMK.ID_KOMPONEN_MK IN (SELECT ID_KOMPONEN_MK FROM AKSES_KOMPONEN_MK WHERE ID_DOSEN ='{$this->id_dosen}')
			ORDER BY KOMK.URUTAN_KOMPONEN_MK");
		if ($this->get_pjmk_status($id_kelas_mk) == 1 || $data_nilai_akses == null)
		{
			$data_nilai = $this->db->QueryToArray("
				SELECT * FROM NILAI_MK NMK 
				JOIN KOMPONEN_MK KOMK ON KOMK.ID_KOMPONEN_MK = NMK.ID_KOMPONEN_MK
				WHERE NMK.ID_PENGAMBILAN_MK ='{$id_pengambilan_mk}' AND NMK.ID_MHS='{$id_mhs}'
				ORDER BY KOMK.URUTAN_KOMPONEN_MK");
		}
		else
		{
			$data_nilai = $data_nilai_akses;
		}
		return $data_nilai;
	}

	function get_id_nilai_mk($nim, $id_kelas_mk)
	{
		$data_komponen_akses = $this->db->QueryToArray("
			SELECT * 
			FROM KOMPONEN_MK 
			WHERE ID_KOMPONEN_MK IN (
				SELECT ID_KOMPONEN_MK 
				FROM AKSES_KOMPONEN_MK 
				WHERE ID_DOSEN='{$this->id_dosen}'
			) 
			AND ID_KELAS_MK ='{$id_kelas_mk}' 
			ORDER BY URUTAN_KOMPONEN_MK");
		if ($this->get_pjmk_status($id_kelas_mk) == 1 || $data_komponen_akses == null)
		{
			$data_id_nilai_mk = $this->db->QueryToArray("
			SELECT ID_NILAI_MK 
				FROM NILAI_MK NMK
			JOIN KOMPONEN_MK KOMK ON KOMK.ID_KOMPONEN_MK = NMK.ID_KOMPONEN_MK
			JOIN MAHASISWA M ON M.ID_MHS=NMK.ID_MHS
			WHERE KOMK.ID_KELAS_MK ={$id_kelas_mk} AND M.NIM_MHS='{$nim}'
			ORDER BY KOMK.URUTAN_KOMPONEN_MK");
		}
		else
		{
			$data_id_nilai_mk = $this->db->QueryToArray("
			SELECT ID_NILAI_MK 
				FROM NILAI_MK NMK
			JOIN KOMPONEN_MK KOMK ON KOMK.ID_KOMPONEN_MK = NMK.ID_KOMPONEN_MK
			JOIN MAHASISWA M ON M.ID_MHS=NMK.ID_MHS
			WHERE KOMK.ID_KELAS_MK ={$id_kelas_mk} 
			AND M.NIM_MHS='{$nim}'
			AND KOMK.ID_KOMPONEN_MK IN (
				SELECT ID_KOMPONEN_MK 
				FROM AKSES_KOMPONEN_MK 
				WHERE ID_DOSEN='{$this->id_dosen}'
			)    
			ORDER BY KOMK.URUTAN_KOMPONEN_MK");
		}
		return $data_id_nilai_mk;
	}

	function get_id_nilai_mk_uas($nim, $id_kelas_mk)
	{
		$data_komponen_akses = $this->db->QueryToArray("
			SELECT * 
			FROM KOMPONEN_MK 
			WHERE ID_KOMPONEN_MK IN (
				SELECT ID_KOMPONEN_MK 
				FROM AKSES_KOMPONEN_MK 
				WHERE ID_DOSEN='{$this->id_dosen}'
			) 
			AND ID_KELAS_MK ='{$id_kelas_mk}' AND NM_KOMPONEN_MK = 'UAS'
			ORDER BY URUTAN_KOMPONEN_MK");
		if ($this->get_pjmk_status($id_kelas_mk) == 1 || $data_komponen_akses == null)
		{
			$data_id_nilai_mk = $this->db->QueryToArray("
			SELECT ID_NILAI_MK 
				FROM NILAI_MK NMK
			JOIN KOMPONEN_MK KOMK ON KOMK.ID_KOMPONEN_MK = NMK.ID_KOMPONEN_MK
			JOIN MAHASISWA M ON M.ID_MHS=NMK.ID_MHS
			WHERE KOMK.ID_KELAS_MK ={$id_kelas_mk} AND M.NIM_MHS='{$nim}' AND KOMK.NM_KOMPONEN_MK = 'UAS'
			ORDER BY KOMK.URUTAN_KOMPONEN_MK");
		}
		else
		{
			$data_id_nilai_mk = $this->db->QueryToArray("
			SELECT ID_NILAI_MK 
				FROM NILAI_MK NMK
			JOIN KOMPONEN_MK KOMK ON KOMK.ID_KOMPONEN_MK = NMK.ID_KOMPONEN_MK
			JOIN MAHASISWA M ON M.ID_MHS=NMK.ID_MHS
			WHERE KOMK.ID_KELAS_MK ={$id_kelas_mk} 
			AND M.NIM_MHS='{$nim}'
			AND KOMK.NM_KOMPONEN_MK = 'UAS'
			AND KOMK.ID_KOMPONEN_MK IN (
				SELECT ID_KOMPONEN_MK 
				FROM AKSES_KOMPONEN_MK 
				WHERE ID_DOSEN='{$this->id_dosen}'
			)    
			ORDER BY KOMK.URUTAN_KOMPONEN_MK");
		}
		return $data_id_nilai_mk;
	}

	function cek_cekal_mhs($nim, $id_kelas_mk)
	{
		$this->db->Query("SELECT PMK.STATUS_CEKAL FROM PENGAMBILAN_MK PMK
		JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
		WHERE PMK.ID_KELAS_MK={$id_kelas_mk} AND M.NIM_MHS='{$nim}'");
		$status_cekal = $this->db->FetchAssoc();
		return $status_cekal['STATUS_CEKAL'];
	}

	function update_nilai_excel($nilai, $id_nilai)
	{
		$this->db->Query("UPDATE NILAI_MK SET BESAR_NILAI_MK='{$nilai}',UPDATED_BY='{$this->id_dosen}' WHERE ID_NILAI_MK='{$id_nilai}' ");
	}

	function update_nilai($nilai, $id_nilai)
	{
		if(trim($nilai) == ''){
			$this->db->Query("UPDATE NILAI_MK SET BESAR_NILAI_MK = NULL,UPDATED_BY='{$this->id_dosen}' WHERE ID_NILAI_MK='{$id_nilai}' ");
		}
		else{
			$this->db->Query("UPDATE NILAI_MK SET BESAR_NILAI_MK='{$nilai}',UPDATED_BY='{$this->id_dosen}' WHERE ID_NILAI_MK='{$id_nilai}' ");
		}
	}

	function update_nilai_up($nilai, $id_nilai)
	{
		$this->db->Query("UPDATE NILAI_KOMPONEN_UP SET BESAR_NILAI='{$nilai}' WHERE ID_NILAI_KOMPONEN_UP='{$id_nilai}' ");
	}

	function update_nilai_khusus($nilai, $id_pengambilan_mk)
	{
		$this->db->Query("UPDATE PENGAMBILAN_MK SET NILAI_ANGKA='{$nilai}' WHERE ID_PENGAMBILAN_MK='{$id_pengambilan_mk}'");
	}

	function status_tampilkan_nilai_mhs($id_kelas_mk, $id_semester)
	{
		return $this->db->QuerySingle("SELECT AVG(FLAGNILAI) FROM PENGAMBILAN_MK WHERE ID_KELAS_MK='{$id_kelas_mk}' AND ID_SEMESTER='{$id_semester}'");
	}

	function tampilkan_nilai_mhs($id_kelas_mk, $id_semester)
	{
		$this->db->Query("UPDATE PENGAMBILAN_MK SET FLAGNILAI=1 WHERE ID_KELAS_MK='{$id_kelas_mk}' AND ID_SEMESTER='{$id_semester}'");
	}

	function reset_tampilkan_nilai_mhs($id_kelas_mk, $id_semester)
	{
		$this->db->Query("UPDATE PENGAMBILAN_MK SET FLAGNILAI=0 WHERE ID_KELAS_MK='{$id_kelas_mk}' AND ID_SEMESTER='{$id_semester}'");
	}

	function get_pengambilan_mk($id_pengambilan_mk)
	{
		$this->db->Query("SELECT * FROM PENGAMBILAN_MK WHERE ID_PENGAMBILAN_MK='{$id_pengambilan_mk}'");
		return $this->db->FetchAssoc();
	}

	function proses_nilai_akhir_sp($id_kelas_mk)
	{
		$data_mahasiswa = $this->db->QueryToArray("
		SELECT 
			M.ID_MHS,PMK.ID_PENGAMBILAN_MK,M.NIM_MHS,P.NM_PENGGUNA,PMK.NILAI_HURUF,PMK.NILAI_ANGKA, PMK.STATUS_CEKAL,PMK.FLAGNILAI,PMK.STATUS_CEKAL_UTS 
		FROM PENGAMBILAN_MK PMK
		LEFT JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
		LEFT JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
		LEFT JOIN SEMESTER S ON S.ID_SEMESTER = PMK.ID_SEMESTER 
		WHERE PMK.ID_KELAS_MK = '{$id_kelas_mk}' AND S.TIPE_SEMESTER='SP'
		ORDER BY M.THN_ANGKATAN_MHS,M.NIM_MHS");
		foreach ($data_mahasiswa as $mhs)
		{
			$nilai_angka = $this->get_nilai_angka_akhir($mhs['ID_MHS'], $id_kelas_mk, $mhs['ID_PENGAMBILAN_MK']);
			$this->get_nilai_huruf_akhir($nilai_angka, $mhs['ID_PENGAMBILAN_MK']);
		}
	}

	function proses_nilai_akhir_up($id_kelas_mk)
	{
		$data_mahasiswa = $this->db->QueryToArray("
		SELECT 
			M.ID_MHS,PMK.ID_PENGAMBILAN_MK,M.NIM_MHS,P.NM_PENGGUNA,PMK.NILAI_HURUF,PMK.NILAI_ANGKA, PMK.STATUS_CEKAL,PMK.FLAGNILAI,PMK.STATUS_CEKAL_UTS 
		FROM PENGAMBILAN_MK PMK
		JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
		JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
		JOIN SEMESTER S ON S.ID_SEMESTER = PMK.ID_SEMESTER 
		WHERE PMK.ID_KELAS_MK = '{$id_kelas_mk}' AND S.TIPE_SEMESTER='UP'
		ORDER BY M.THN_ANGKATAN_MHS,M.NIM_MHS");
		foreach ($data_mahasiswa as $mhs)
		{
			$nilai_angka = $this->get_nilai_angka_akhir($mhs['ID_MHS'], $id_kelas_mk, $mhs['ID_PENGAMBILAN_MK']);
			$this->get_nilai_huruf_akhir($nilai_angka, $mhs['ID_PENGAMBILAN_MK']);
		}
	}

	function proses_nilai_akhir($id_kelas_mk)
	{
		$data_mahasiswa = $this->db->QueryToArray("
		SELECT 
			M.ID_MHS,PMK.ID_PENGAMBILAN_MK,M.NIM_MHS,P.NM_PENGGUNA,PMK.NILAI_HURUF,PMK.NILAI_ANGKA, PMK.STATUS_CEKAL,PMK.FLAGNILAI,PMK.STATUS_CEKAL_UTS 
		FROM PENGAMBILAN_MK PMK
		JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
		JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
		JOIN SEMESTER S ON S.ID_SEMESTER = PMK.ID_SEMESTER 
		WHERE PMK.ID_KELAS_MK = '{$id_kelas_mk}' AND S.TIPE_SEMESTER IN ('REG','SP') AND PMK.STATUS_APV_PENGAMBILAN_MK=1");
		foreach ($data_mahasiswa as $mhs)
		{
			$nilai_angka = $this->get_nilai_angka_akhir($mhs['ID_MHS'], $id_kelas_mk, $mhs['ID_PENGAMBILAN_MK']);
			$this->get_nilai_huruf_akhir($nilai_angka, $mhs['ID_PENGAMBILAN_MK']);
		}
	}

	function proses_status_mhs($id_kelas_mk)
	{
		$data_mahasiswa = $this->db->QueryToArray("
		SELECT 
			M.ID_MHS,PMK.ID_PENGAMBILAN_MK,PMK.ID_SEMESTER,M.NIM_MHS,P.NM_PENGGUNA,PMK.NILAI_HURUF,PMK.NILAI_ANGKA, PMK.STATUS_CEKAL,PMK.FLAGNILAI,PMK.STATUS_CEKAL_UTS 
		FROM PENGAMBILAN_MK PMK
		JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
		JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
		JOIN SEMESTER S ON S.ID_SEMESTER = PMK.ID_SEMESTER 
		WHERE PMK.ID_KELAS_MK = '{$id_kelas_mk}' AND S.TIPE_SEMESTER='REG' AND PMK.STATUS_APV_PENGAMBILAN_MK=1");
		foreach ($data_mahasiswa as $mhs)
		{
			$this->update_status_mhs($mhs['ID_MHS'], $mhs['ID_SEMESTER']);
		}
	}

	function update_status_mhs($id_mhs, $id_semester){

            $this->db->Query("SELECT * FROM MAHASISWA_STATUS WHERE ID_MHS='{$id_mhs}' AND ID_SEMESTER = '{$id_semester}'");
            $mhs_status = $this->db->FetchAssoc();
            $id_mhs_status = $mhs_status['ID_MHS_STATUS'];
            $ips_lama = $mhs_status['IPS'];

            $this->db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '{$id_semester}'");
            $smt = $this->db->FetchAssoc();
            $fd_id_smt = $smt['FD_ID_SMT'];

            // query get ipk
            $this->db->Query("SELECT id_mhs,
                                    SUM(sks) AS total_sks, 
                                    round(SUM(nilai_mutu) / SUM(sks), 2) AS ipk 
                                FROM (
                                    SELECT
                                        mhs.id_mhs,
                                        /* Kode MK utk grouping total mutu */
                                        COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
                                        /* SKS */
                                        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
                                        /* Nilai Mutu = SKS * Bobot */
                                        COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu,
                                        /* Urutan Nilai Terbaik */
                                        row_number() OVER (PARTITION BY mhs.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY sn.nilai_standar_nilai DESC) urut
                                    FROM pengambilan_mk pmk
                                    JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
                                    JOIN semester S ON S.id_semester = pmk.id_semester
                                    -- Via Kelas
                                    LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
                                    LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
                                    -- Via Kurikulum
                                    LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
                                    LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
                                    -- Penyetaraan
                                    LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
                                    LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
                                    -- nilai bobot
                                    JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
                                    WHERE 
                                        pmk.status_apv_pengambilan_mk = 1 AND 
                                        mhs.id_mhs = '{$id_mhs}' AND 
                                        S.fd_id_smt <= '{$fd_id_smt}'
                                )
                                GROUP BY id_mhs");
            $data_ipk = $this->db->FetchAssoc();
            $ipk = $data_ipk['IPK'];
            $total_sks = $data_ipk['TOTAL_SKS'];

            // query get ips
            $this->db->Query("SELECT id_mhs, SUM(sks) AS total_sks, round(SUM(nilai_mutu) / sum(sks), 2) as ips from (
                        SELECT
                            mhs.id_mhs,
                            /* Kode MK utk grouping total mutu */
                            COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
                            /* SKS */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
                            /* Nilai Mutu = SKS * Bobot */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu
                        FROM pengambilan_mk pmk
                        JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
                        JOIN semester S ON S.id_semester = pmk.id_semester
                        -- Via Kelas
                        LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
                        LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
                        -- Via Kurikulum
                        LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
                        LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
                        -- nilai bobot
                        JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
                        WHERE 
                            pmk.status_apv_pengambilan_mk = 1 AND 
                            mhs.id_mhs = '{$id_mhs}' AND 
                            S.id_semester = '{$id_semester}'
                    )
                    group by id_mhs");
            $data_ips = $this->db->FetchAssoc();
            $ips = $data_ips['IPS'];
            $total_sks_semester = $data_ips['TOTAL_SKS'];

            if($id_mhs_status == ''){
                $this->db->Query("INSERT INTO MAHASISWA_STATUS (ID_MHS, ID_SEMESTER, ID_STATUS_PENGGUNA, IPS, IPK, SKS_TOTAL, SKS_SEMESTER, CREATED_BY)
                                    VALUES ('{$id_mhs}', '{$id_semester}', '1', '{$ips}', '{$ipk}', '{$total_sks}', '{$total_sks_semester}', '{$this->id_pengguna}'");
            }
            else{
            	if($ips_lama != $ips){
                	$this->db->Query("UPDATE MAHASISWA_STATUS SET ID_STATUS_PENGGUNA = 1, IPS = '{$ips}', IPK = '{$ipk}', SKS_TOTAL = '{$total_sks}', SKS_SEMESTER = '{$total_sks_semester}', UPDATED_ON = SYSDATE, UPDATED_BY = '{$this->id_pengguna}' WHERE ID_MHS_STATUS = '{$id_mhs_status}'");
            	}
            }
        
    }

	function get_komponen_mk($id_kelas_mk)
	{
		$ada = $this->db->QuerySingle("
			SELECT COUNT(*) AS ADA
			FROM KOMPONEN_MK
			WHERE ID_KELAS_MK='{$id_kelas_mk}'");

		return $ada;
	}

	function get_nilai_angka_akhir($id_mhs, $id_kelas_mk, $id_pengambilan_mk)
	{
		$data_pengambilan = $this->get_pengambilan_mk($id_pengambilan_mk);
		$nilai_angka_sebelumnya = $data_pengambilan['NILAI_ANGKA'];
		$nilai_angka_akhir = '';
		$cek_semester_up = $this->db->QuerySingle("SELECT COUNT(*) FROM SEMESTER WHERE ID_SEMESTER='{$data_pengambilan['ID_SEMESTER']}' AND TIPE_SEMESTER='UP' AND ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}'");
		// Hitung Nilai Angka Berdasarkan Komponen
		if ($cek_semester_up > 0)
		{
			// Jika Semester Ambil Nilai Perkomponen UP
			$nilai = $this->db->QuerySingle("
			SELECT SUM(NMK.BESAR_NILAI*(KOMK.PROSENTASE_KOMPONEN/100))
			FROM NILAI_KOMPONEN_UP NMK
			JOIN KOMPONEN_UP KOMK ON NMK.ID_KOMPONEN_UP = KOMK.ID_KOMPONEN_UP
			JOIN PENGAMBILAN_MK PMK ON PMK.ID_PENGAMBILAN_MK = NMK.ID_PENGAMBILAN_MK
			WHERE PMK.ID_KELAS_MK='{$id_kelas_mk}' AND PMK.ID_MHS='{$id_mhs}' AND PMK.STATUS_APV_PENGAMBILAN_MK = 1");
		}
		else
		{
			$nilai = $this->db->QuerySingle("
			SELECT SUM(NMK.BESAR_NILAI_MK*(KOMK.PERSENTASE_KOMPONEN_MK/100))
			FROM NILAI_MK NMK
			JOIN KOMPONEN_MK KOMK ON NMK.ID_KOMPONEN_MK = KOMK.ID_KOMPONEN_MK
			JOIN PENGAMBILAN_MK PMK ON PMK.ID_PENGAMBILAN_MK = NMK.ID_PENGAMBILAN_MK
			WHERE PMK.ID_KELAS_MK='{$id_kelas_mk}' AND PMK.ID_MHS='{$id_mhs}' AND PMK.STATUS_APV_PENGAMBILAN_MK = 1");
		}
		// Cek nilai angka apa tidak kosong
		if ($nilai != '')
		{
			$nilai_angka_akhir = str_replace(',', '.', round($nilai, 2));
		}
		else
		{
			$nilai_angka_akhir = '';
		}
		// Update Nilai Angka Pengambilan MK
		if ($nilai_angka_akhir != $nilai_angka_sebelumnya)
		{
			if ($nilai_angka_akhir == '')
			{
				$this->db->Query("UPDATE PENGAMBILAN_MK SET NILAI_ANGKA=NULL WHERE ID_PENGAMBILAN_MK='{$id_pengambilan_mk}'");
			}
			else
			{ 
				if($nilai_angka_sebelumnya > 0){
					$this->db->Query("UPDATE PENGAMBILAN_MK SET NILAI_ANGKA='{$nilai_angka_akhir}' WHERE ID_PENGAMBILAN_MK='{$id_pengambilan_mk}'");
				}
				else{
					$this->db->Query("UPDATE PENGAMBILAN_MK SET NILAI_ANGKA='{$nilai_angka_akhir}', ID_PENGGUNA_INPUT ='{$GLOBALS[id_pengguna_input]}' WHERE ID_PENGAMBILAN_MK='{$id_pengambilan_mk}'");
				}
			}
		}
		return $nilai_angka_akhir;
	}

	function get_nilai_huruf_akhir($nilai, $id_pengambilan_mk)
	{
		$data_status = $this->get_status_ulang($id_pengambilan_mk);
		$data_pengambilan = $this->get_pengambilan_mk($id_pengambilan_mk);
		$nilai_huruf_sebelumnya = $data_pengambilan['NILAI_HURUF'];
		$nilai_huruf_akhir = '';

		$query = "SELECT NM_STANDAR_NILAI, NILAI_STANDAR_NILAI, NILAI_MIN_PERATURAN_NILAI AS NILAI_MIN, 
					NILAI_MAX_PERATURAN_NILAI AS NILAI_MAX, ID_PERATURAN_NILAI
					FROM PERATURAN_NILAI
					JOIN PERATURAN ON PERATURAN.ID_PERATURAN = PERATURAN_NILAI.ID_PERATURAN
					JOIN STANDAR_NILAI ON STANDAR_NILAI.ID_STANDAR_NILAI = PERATURAN_NILAI.ID_STANDAR_NILAI
					WHERE PERATURAN_NILAI.ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}'";

		// Cari Standar Nilai Untuk Nilai Angka
		foreach ($this->db->QueryToArray($query) as $data)
		{
			if ($nilai <= $data['NILAI_MAX'] && $nilai >= $data['NILAI_MIN'])
			{
				$nilai_huruf_akhir = $data['NM_STANDAR_NILAI'];
			}
		}
		/*
		  #Dimatikan karena tidak ada pengecualian seperti di UNAIR ~ Putra Rieskha
		  //khusus mata kuliah mengulang fakultas FEB
		  if ($data_status['ID_FAKULTAS'] == 4 && $data_status['STATUS_ULANG'] == 'U') {
		  if ($nilai_huruf_akhir == 'A' || $nilai_huruf_akhir == 'AB') {
		  $nilai_huruf_akhir = 'B';
		  }
		  }

		  // Jika Semester SP
		  $cek_semester_sp = $this->db->QuerySingle("SELECT COUNT(*) FROM SEMESTER WHERE ID_SEMESTER='{$data_pengambilan['ID_SEMESTER']}' AND TIPE_SEMESTER='SP' AND ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}'");
		  if ($cek_semester_sp > 0 && $data_status['ID_FAKULTAS'] != 4) {
		  if ($nilai_huruf_akhir == 'A' || $nilai_huruf_akhir == 'AB') {
		  $nilai_huruf_akhir = 'B';
		  }
		  }

		  // Jika Semester UP
		  $cek_semester_up = $this->db->QuerySingle("SELECT COUNT(*) FROM SEMESTER WHERE ID_SEMESTER='{$data_pengambilan['ID_SEMESTER']}' AND TIPE_SEMESTER='UP' AND ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}'");
		  if ($cek_semester_up > 0) {
		  if ($nilai_huruf_akhir == 'A' || $nilai_huruf_akhir == 'AB') {
		  $nilai_huruf_akhir = 'B';
		  }
		  }

		  // Pengecualian FKG Cekal Mahasiswa= Nilai E
		  if ($data_status['ID_FAKULTAS'] == 2 && $data_pengambilan['STATUS_CEKAL'] == '0' && $cek_semester_up == 0) {
		  if ($nilai_huruf_akhir != 'E') {
		  $nilai_huruf_akhir = 'E';
		  }
		  }
		 */
		//jika nilai angka Kosong
		if ($nilai == '')
		{
			$nilai_huruf_akhir = '';
		}
		// Update Nilai Angka Pengambilan MK

		if ($nilai_huruf_sebelumnya != $nilai_huruf_akhir)
		{
			if ($nilai_huruf_akhir == '')
			{
				$this->db->Query("UPDATE PENGAMBILAN_MK SET NILAI_HURUF=NULL WHERE ID_PENGAMBILAN_MK='{$id_pengambilan_mk}'");
			}
			else
			{
				$this->db->Query("UPDATE PENGAMBILAN_MK SET NILAI_HURUF='{$nilai_huruf_akhir}' WHERE ID_PENGAMBILAN_MK='{$id_pengambilan_mk}'");
			}
		}
		return $nilai_huruf_akhir;
	}

	function get_status_ulang($id_pengambilan_mk)
	{
		$this->db->Query("
		SELECT PMK.*,PS.ID_FAKULTAS FROM PENGAMBILAN_MK PMK
		JOIN MAHASISWA M ON M.ID_MHS=PMK.ID_MHS
		JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
		WHERE PMK.ID_PENGAMBILAN_MK='{$id_pengambilan_mk}'");
		return $this->db->FetchAssoc();
	}

	//Fungsi Akses Komponen MK

	function load_akses_komponen($id_kelas_mk)
	{
		return $this->db->QueryToArray("
			SELECT AKMK.*,P.NM_PENGGUNA,KOMK.NM_KOMPONEN_MK FROM AKSES_KOMPONEN_MK AKMK
			JOIN DOSEN D ON AKMK.ID_DOSEN = D.ID_DOSEN
			JOIN PENGGUNA P ON D.ID_PENGGUNA  = P.ID_PENGGUNA
			JOIN KOMPONEN_MK KOMK ON KOMK.ID_KOMPONEN_MK = AKMK.ID_KOMPONEN_MK
			WHERE AKMK.ID_KELAS_MK='{$id_kelas_mk}'");
	}

	function tambah_akses_komponen($dosen, $kelas, $komponen_mk, $semester)
	{
		$this->db->Query("INSERT INTO AKSES_KOMPONEN_MK (ID_DOSEN,ID_KELAS_MK,ID_KOMPONEN_MK,ID_SEMESTER) 
			VALUES
				('{$dosen}','{$kelas}','{$komponen_mk}','{$semester}')");
	}

	function delete_akses_komponen($id_akses)
	{
		$this->db->Query("DELETE FROM AKSES_KOMPONEN_MK WHERE ID_AKSES_KOMPONEN_MK ='{$id_akses}'");
	}

	//Fungsi Pembukaan Kelas

	function load_mata_kuliah_wadek1($id_fakultas)
	{
		return $this->db->QueryToArray("
		SELECT MK.ID_MATA_KULIAH,('('||J.NM_JENJANG||') '||PS.NM_PROGRAM_STUDI) PRODI,MK.NM_MATA_KULIAH,MK.KD_MATA_KULIAH
		,SUM(PMK.NILAI_ANGKA) AS STATUS_NILAI,COUNT(PMK.ID_MHS) JUMLAH_MHS,J.NM_JENJANG
		FROM MATA_KULIAH MK
		JOIN KURIKULUM_MK KUMK ON KUMK.ID_MATA_KULIAH = MK.ID_MATA_KULIAH
		JOIN KELAS_MK KMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
		JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = KMK.ID_PROGRAM_STUDI AND PS.ID_FAKULTAS='{$id_fakultas}'
		JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
		JOIN PENGAMBILAN_MK PMK ON PMK.ID_KELAS_MK = KMK.ID_KELAS_MK
		GROUP BY MK.ID_MATA_KULIAH,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,MK.NM_MATA_KULIAH,MK.KD_MATA_KULIAH,J.NM_JENJANG
		ORDER BY MK.NM_MATA_KULIAH,PRODI");
	}

	function load_kelas_pembukaan($id_mata_kuliah)
	{
		return $this->db->QueryToArray("
		SELECT 
			KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH,NVL(K1.NMKELAS,K2.NAMA_KELAS) NM_KELAS,J.NM_JENJANG,INITCAP(PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI,KMK.IS_DIBUKA,
			S.NM_SEMESTER,S.TAHUN_AJARAN
		FROM KELAS_MK KMK
		JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
		JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
		LEFT JOIN FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
		LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
		JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = KMK.ID_PROGRAM_STUDI OR K1.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
		JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
		LEFT JOIN SEMESTER S ON KMK.ID_SEMESTER = S.ID_SEMESTER
		WHERE MK.ID_MATA_KULIAH='{$id_mata_kuliah}'
		ORDER BY NM_MATA_KULIAH,NM_KELAS,S.TAHUN_AJARAN DESC,S.NM_SEMESTER DESC");
	}

	function pembukaan_kelas($id_kelas_mk)
	{
		$data_kelas_mk = $this->get_kelas_mk($id_kelas_mk);
		if ($data_kelas_mk['IS_DIBUKA'] == 1)
		{
			$this->db->Query("UPDATE KELAS_MK SET IS_DIBUKA=0 WHERE ID_KELAS_MK='{$id_kelas_mk}'");
		}
		else
		{
			$this->db->Query("UPDATE KELAS_MK SET IS_DIBUKA=1 WHERE ID_KELAS_MK='{$id_kelas_mk}'");
		}
	}

	function load_semester_pembukaan_fakultas($fakultas)
	{
		return $this->db->QueryToArray("
			SELECT S.ID_SEMESTER,(CASE WHEN TIPE_SEMESTER='SP' THEN S.NM_SEMESTER||', '||S.GROUP_SEMESTER ELSE S.NM_SEMESTER END) NM_SEMESTER,S.TAHUN_AJARAN,S.STATUS_AKTIF_SEMESTER,S.THN_AKADEMIK_SEMESTER,
				AVG(KMK.IS_DIBUKA) STATUS_BUKA
			FROM AUCC.SEMESTER S
			JOIN AUCC.KELAS_MK KMK ON S.ID_SEMESTER=KMK.ID_SEMESTER
			WHERE (TIPE_SEMESTER='REG' OR TIPE_SEMESTER='SP')
			AND THN_AKADEMIK_SEMESTER 
			BETWEEN TO_CHAR(SYSDATE,'YYYY')-2 AND TO_CHAR(SYSDATE,'YYYY')
			AND KMK.ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM AUCC.PROGRAM_STUDI WHERE ID_FAKULTAS='{$fakultas}')
			GROUP BY S.TIPE_SEMESTER,S.GROUP_SEMESTER,S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,S.STATUS_AKTIF_SEMESTER,S.THN_AKADEMIK_SEMESTER
			ORDER BY THN_AKADEMIK_SEMESTER DESC,NM_SEMESTER DESC
			");
	}

	function load_semester_penilaian($fakultas)
	{
		return $this->db->QueryToArray("
			SELECT S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,S.STATUS_AKTIF_SEMESTER,S.THN_AKADEMIK_SEMESTER,
				AVG(KMK.IS_DIBUKA) STATUS_BUKA
			FROM SEMESTER S
			JOIN KELAS_MK KMK ON S.ID_SEMESTER=KMK.ID_SEMESTER
			WHERE TIPE_SEMESTER='REG' 
			AND THN_AKADEMIK_SEMESTER 
			BETWEEN TO_CHAR(SYSDATE,'YYYY')-1 AND TO_CHAR(SYSDATE,'YYYY')
			AND KMK.ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM PROGRAM_STUDI WHERE ID_FAKULTAS='{$fakultas}')
			GROUP BY S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,S.STATUS_AKTIF_SEMESTER,S.THN_AKADEMIK_SEMESTER
			ORDER BY THN_AKADEMIK_SEMESTER DESC,NM_SEMESTER DESC
			");
	}

	function pembukaan_semester($id_semester, $fakultas)
	{
		$status_buka = $this->db->QuerySingle("
			SELECT 
				AVG(KMK.IS_DIBUKA) STATUS_BUKA
			FROM SEMESTER S
			JOIN KELAS_MK KMK ON S.ID_SEMESTER=KMK.ID_SEMESTER
			WHERE S.ID_SEMESTER='{$id_semester}'
			AND KMK.ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM PROGRAM_STUDI WHERE ID_FAKULTAS='{$fakultas}')
			");
		if (($status_buka < 1 && $status_buka > 0) || $status_buka == 1)
		{
			// Tutup Semua
			$this->db->Query("UPDATE KELAS_MK SET IS_DIBUKA=0 WHERE ID_SEMESTER='{$id_semester}' AND IS_DIBUKA=1 AND ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM PROGRAM_STUDI WHERE ID_FAKULTAS='{$fakultas}')");
		}
		else
		{
			// Buka Semua
			$this->db->Query("UPDATE KELAS_MK SET IS_DIBUKA=1 WHERE ID_SEMESTER='{$id_semester}' AND IS_DIBUKA=0 AND ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM PROGRAM_STUDI WHERE ID_FAKULTAS='{$fakultas}')");
		}
	}

	// Fungsi untuk penilaian Khusus (Remidi, Ujian Perbaikan)
	function benahi_tampilan_nilai_krs_akad_up($id_kelas_mk, $id_semester)
	{
		// Cek Edit KRS Akademik
		$jumlah_krs_akad = $this->db->QuerySingle("
		SELECT COUNT(*)
		FROM AUCC.PENGAMBILAN_MK PMK
		JOIN AUCC.KOMPONEN_UP KOMK ON KOMK.ID_KELAS_MK = PMK.ID_KELAS_MK
		WHERE PMK.ID_KELAS_MK='{$id_kelas_mk}' AND PMK.ID_SEMESTER='{$id_semester}'
		AND PMK.ID_PENGAMBILAN_MK NOT IN (
		SELECT ID_PENGAMBILAN_MK
		FROM AUCC.NILAI_KOMPONEN_UP WHERE ID_PENGAMBILAN_MK IN 
			(
				SELECT ID_PENGAMBILAN_MK FROM AUCC.PENGAMBILAN_MK WHERE ID_KELAS_MK='{$id_kelas_mk}' AND ID_SEMESTER='{$id_semester}'
			)
		)");
		if ($jumlah_krs_akad > 0)
		{
			// Benahi Tampilan Nilai
			$this->db->Query("
			INSERT INTO AUCC.NILAI_KOMPONEN_UP (ID_PENGAMBILAN_MK,ID_KOMPONEN_UP,BESAR_NILAI)
			SELECT PMK.ID_PENGAMBILAN_MK,KOMK.ID_KOMPONEN_UP,'0' BESAR_NILAI
			FROM AUCC.PENGAMBILAN_MK PMK
			JOIN AUCC.KOMPONEN_UP KOMK ON KOMK.ID_KELAS_MK = PMK.ID_KELAS_MK
			WHERE PMK.ID_KELAS_MK='{$id_kelas_mk}' AND PMK.ID_SEMESTER='{$id_semester}'
			AND PMK.ID_PENGAMBILAN_MK NOT IN (
			SELECT ID_PENGAMBILAN_MK
			FROM AUCC.NILAI_KOMPONEN_UP WHERE ID_PENGAMBILAN_MK IN 
				(
					SELECT ID_PENGAMBILAN_MK FROM AUCC.PENGAMBILAN_MK WHERE ID_KELAS_MK='{$id_kelas_mk}' AND ID_SEMESTER='{$id_semester}'
				)
			)");
		}
	}

	function get_semester_kelas_khusus($id_semester)
	{
		$this->db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER='{$id_semester}'");
		return $this->db->FetchAssoc();
	}

	function load_data_mahasiswa_khusus($id_kelas_mk)
	{
		$var = explode('_', $id_kelas_mk);
		$id_kelas = $var[0];
		$id_semester = $var[1];

		// Ambil Semester
		$this->db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER='{$id_semester}'");
		$semester = $this->db->FetchAssoc();

		// Cek apakah Mata Kuliah Tersebiut Punya Komponen Nilai dan Dari Semester SP
		if ($semester['TIPE_SEMESTER'] == 'SP')
		{
			$komponen_mk = $this->db->QueryToArray("SELECT * FROM KOMPONEN_MK WHERE ID_KELAS_MK='{$id_kelas}'");
		}
		// Cek apakah Mata Kuliah Tersebiut Punya Komponen Nilai dan Dari Semester UP
		if ($semester['TIPE_SEMESTER'] == 'UP')
		{
			$komponen_up = $this->db->QueryToArray("SELECT * FROM KOMPONEN_UP WHERE ID_KELAS_MK='{$id_kelas}'");
		}
		$data_mahasiswa_kelas = array();
		// Data Mahasiswa
		$data_mahasiswa = $this->db->QueryToArray("
			SELECT M.ID_MHS,PMK.ID_PENGAMBILAN_MK,M.NIM_MHS,P.NM_PENGGUNA,PMK.NILAI_HURUF,PMK.NILAI_ANGKA, PMK.STATUS_CEKAL,PMK.FLAGNILAI FROM PENGAMBILAN_MK PMK
			LEFT JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
			LEFT JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
			LEFT JOIN SEMESTER S ON S.ID_SEMESTER = PMK.ID_SEMESTER
			WHERE PMK.ID_KELAS_MK = '{$id_kelas}' AND PMK.ID_SEMESTER='{$id_semester}' AND PMK.STATUS_HAPUS!=1
			ORDER BY M.THN_ANGKATAN_MHS,M.NIM_MHS");
		// Komponen Mk SP di set kosong
		if (count($komponen_mk) == 0 && count($komponen_up) == 0)
		{
			foreach ($data_mahasiswa as $data)
			{
				array_push($data_mahasiswa_kelas, array(
					'nim' => $data['NIM_MHS'],
					'nama' => $data['NM_PENGGUNA'],
					'flagnilai' => $data['FLAGNILAI'],
					'id_pengambilan_mk' => $data['ID_PENGAMBILAN_MK'],
					'status_cekal' => $data['STATUS_CEKAL'],
					'nilai_angka_akhir' => $data['NILAI_ANGKA'],
					'nilai_huruf_akhir' => $this->get_nilai_huruf_akhir($data['NILAI_ANGKA'], $data['ID_PENGAMBILAN_MK'])
				));
			}
		}
		else if (count($komponen_mk) > 0)
		{
			foreach ($data_mahasiswa as $data)
			{
				array_push($data_mahasiswa_kelas, array(
					'nim' => $data['NIM_MHS'],
					'nama' => $data['NM_PENGGUNA'],
					'flagnilai' => $data['FLAGNILAI'],
					'id_pengambilan_mk' => $data['ID_PENGAMBILAN_MK'],
					'status_cekal' => $data['STATUS_CEKAL'],
					'data_nilai' => $this->load_data_nilai($data['ID_MHS'], $data['ID_PENGAMBILAN_MK']),
					'nilai_angka_akhir' => $data['NILAI_ANGKA'],
					'nilai_huruf_akhir' => $data['NILAI_HURUF']
				));
			}
		}
		else if (count($komponen_up) > 0)
		{
			foreach ($data_mahasiswa as $data)
			{
				array_push($data_mahasiswa_kelas, array(
					'nim' => $data['NIM_MHS'],
					'nama' => $data['NM_PENGGUNA'],
					'flagnilai' => $data['FLAGNILAI'],
					'id_pengambilan_mk' => $data['ID_PENGAMBILAN_MK'],
					'status_cekal' => $data['STATUS_CEKAL'],
					'data_nilai' => $this->load_data_nilai_up($data['ID_PENGAMBILAN_MK']),
					'nilai_angka_akhir' => $data['NILAI_ANGKA'],
					'nilai_huruf_akhir' => $data['NILAI_HURUF']
				));
			}
		}
		return $data_mahasiswa_kelas;
	}

	function load_data_nilai_up($id_pengambilan_mk)
	{
		$data_nilai = $this->db->QueryToArray("
			SELECT * FROM NILAI_KOMPONEN_UP NMK 
			JOIN KOMPONEN_UP KOMK ON KOMK.ID_KOMPONEN_UP = NMK.ID_KOMPONEN_UP
			WHERE NMK.ID_PENGAMBILAN_MK ='{$id_pengambilan_mk}'
			ORDER BY KOMK.NM_KOMPONEN_UP");
		return $data_nilai;
	}

	function load_kelas_mata_kuliah_khusus()
	{
		$data_kelas_mk = array();
		foreach ($this->load_semester_kelas_khusus() as $semester)
		{
			array_push($data_kelas_mk, array(
				'NM_SEMESTER' => $semester['NM_SEMESTER'],
				'TAHUN_AJARAN' => $semester['TAHUN_AJARAN'],
				'GROUP_SEMESTER' => $semester['GROUP_SEMESTER'],
				'DATA_KELAS_MK' => $this->db->QueryToArray("                    
					SELECT 
						KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH,NVL(K1.NMKELAS,K2.NAMA_KELAS) NM_KELAS,MK.KD_MATA_KULIAH,J.NM_JENJANG,INITCAP(PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI,MK.KD_MATA_KULIAH,
						S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,S.GROUP_SEMESTER
					FROM PENGAMPU_MK PENGMK
					JOIN PENGAMBILAN_MK PMK ON PMK.ID_KELAS_MK=PENGMK.ID_KELAS_MK
					JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PMK.ID_KELAS_MK
					JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
					JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
					LEFT JOIN FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
					LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
					JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = KMK.ID_PROGRAM_STUDI OR K1.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
					JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
					LEFT JOIN SEMESTER S ON PMK.ID_SEMESTER = S.ID_SEMESTER
					JOIN DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
					WHERE D.ID_DOSEN='{$this->id_dosen}' AND S.ID_SEMESTER='{$semester['ID_SEMESTER']}'
					GROUP BY KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH,K1.NMKELAS,K2.NAMA_KELAS,MK.KD_MATA_KULIAH,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,MK.KD_MATA_KULIAH,
						S.ID_SEMESTER,S.NM_SEMESTER,S.TAHUN_AJARAN,S.GROUP_SEMESTER
					ORDER BY NM_MATA_KULIAH,NM_KELAS
		")
			));
		}
		return $data_kelas_mk;
	}

	function load_semester_kelas_khusus()
	{
		return $this->db->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Ganjil' AND NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER>=2011 AND ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}' ORDER BY TAHUN_AJARAN DESC,NM_SEMESTER DESC");
	}

	//fungsi setting penilaian khusus (Remidi , Ujian Perbaikan)


	function load_kelas_mata_kuliah_pjma()
	{
		$data_kelas_mk = array();
		foreach ($this->load_semester_kelas_mk() as $semester)
		{
			array_push($data_kelas_mk, array(
				'NM_SEMESTER' => $semester['NM_SEMESTER'],
				'TAHUN_AJARAN' => $semester['TAHUN_AJARAN'],
				'DATA_KELAS_MK' => $this->db->QueryToArray("
				SELECT 
					KMK.ID_KELAS_MK,MK.NM_MATA_KULIAH,NVL(K1.NMKELAS,K2.NAMA_KELAS) NM_KELAS,MK.KD_MATA_KULIAH,J.NM_JENJANG,INITCAP(PS.NM_PROGRAM_STUDI) NM_PROGRAM_STUDI,MK.KD_MATA_KULIAH,
					S.NM_SEMESTER,S.TAHUN_AJARAN
				FROM PENGAMPU_MK PENGMK
				JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK = PENGMK.ID_KELAS_MK
				JOIN KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
				JOIN MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
				LEFT JOIN FST_KELAS K1 ON K1.KDKELAS = KMK.TIPE_KELAS_MK
				LEFT JOIN NAMA_KELAS K2 ON K2.ID_NAMA_KELAS = KMK.NO_KELAS_MK
				JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = KMK.ID_PROGRAM_STUDI OR K1.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
				JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
				LEFT JOIN SEMESTER S ON KMK.ID_SEMESTER = S.ID_SEMESTER
				JOIN DOSEN D ON D.ID_DOSEN = PENGMK.ID_DOSEN
				WHERE D.ID_DOSEN='{$this->id_dosen}' AND S.ID_SEMESTER='{$semester['ID_SEMESTER']}' AND PENGMK.PJMK_PENGAMPU_MK=1
				ORDER BY NM_MATA_KULIAH,NM_KELAS
		")
			));
		}
		return $data_kelas_mk;
	}

	function load_data_mahasiswa_setting_penilaian($id_kelas_mk)
	{
		$semester_aktif = $this->get_semester_aktif();
		$data_mahasiswa_kelas = array();
		$data_mahasiswa = $this->db->QueryToArray("
		SELECT 
			M.ID_MHS,PMK.ID_PENGAMBILAN_MK,M.NIM_MHS,P.NM_PENGGUNA,PMK.NILAI_HURUF,PMK.NILAI_ANGKA, PMK.STATUS_CEKAL,PMK.FLAGNILAI,PMK.STATUS_CEKAL_UTS
		FROM PENGAMBILAN_MK PMK
		LEFT JOIN MAHASISWA M ON PMK.ID_MHS = M.ID_MHS
		LEFT JOIN PENGGUNA P ON M.ID_PENGGUNA = P.ID_PENGGUNA
		LEFT JOIN SEMESTER S ON S.ID_SEMESTER = PMK.ID_SEMESTER
		WHERE PMK.ID_KELAS_MK = '{$id_kelas_mk}' AND (PMK.ID_SEMESTER='{$semester_aktif}' OR PMK.ID_KELAS_MK IN (SELECT ID_KELAS_MK FROM KELAS_MK WHERE IS_DIBUKA=1)) AND S.TIPE_SEMESTER='REG'
		ORDER BY M.THN_ANGKATAN_MHS,M.NIM_MHS");
		foreach ($data_mahasiswa as $data)
		{
			array_push($data_mahasiswa_kelas, array(
				'id_mhs' => $data['ID_MHS'],
				'nim' => $data['NIM_MHS'],
				'nama' => $data['NM_PENGGUNA'],
				'nilai' => $data['NILAI_ANGKA'] . " ( {$data['NILAI_HURUF']} )",
				'status_remidi' => $this->cek_kelas_remidi($data['ID_MHS'], $id_kelas_mk),
				'status_up' => $this->cek_kelas_ujian_perbaikan($data['ID_MHS'], $id_kelas_mk)
			));
		}
		return $data_mahasiswa_kelas;
	}

	function get_semester_remidi($id_kelas_mk)
	{
		$semester_kelas = $this->get_semester_kelas_mk($id_kelas_mk);
		$semester_remidi = $this->db->QuerySingle("
			SELECT ID_SEMESTER FROM SEMESTER WHERE UPPER(NM_SEMESTER) LIKE UPPER('%REMIDI%') AND TAHUN_AJARAN=
			(SELECT TAHUN_AJARAN FROM SEMESTER WHERE ID_SEMESTER='{$semester_kelas['ID_SEMESTER']}') AND GROUP_SEMESTER=
			(SELECT GROUP_SEMESTER FROM SEMESTER WHERE ID_SEMESTER='{$semester_kelas['ID_SEMESTER']}')");
		return $semester_remidi;
	}

	function get_semester_up($id_kelas_mk)
	{
		$semester_kelas = $this->get_semester_kelas_mk($id_kelas_mk);
		$semester_up = $this->db->QuerySingle("
			SELECT ID_SEMESTER FROM SEMESTER WHERE UPPER(NM_SEMESTER) LIKE UPPER('%UJIAN_PERBAIKAN%') AND TAHUN_AJARAN=
			(SELECT TAHUN_AJARAN FROM SEMESTER WHERE ID_SEMESTER='{$semester_kelas['ID_SEMESTER']}') AND GROUP_SEMESTER=
			(SELECT GROUP_SEMESTER FROM SEMESTER WHERE ID_SEMESTER='{$semester_kelas['ID_SEMESTER']}')");
		if ($semester_up == '')
		{
			$this->db->Query("UPDATE SEMESTER SET STATUS_AKTIF_UP='False' WHERE STATUS_AKTIF_UP='True'");
			$this->db->Query(""
				. "INSERT INTO SEMESTER "
				. "(NM_SEMESTER,THN_AKADEMIK_SEMESTER,STATUS_AKTIF_SEMESTER,TAHUN_AJARAN,GROUP_SEMESTER,TIPE_SEMESTER,STATUS_AKTIF_UP,STATUS_AKTIF_SP,STATUS_AKTIF_PEMBAYARAN,STATUS_AKTIF_PEMBAYARAN_SP)"
				. "VALUES"
				. "('Ujian Perbaikan','{$semester_kelas['THN_AKADEMIK_SEMESTER']}','False','{$semester_kelas['TAHUN_AJARAN']}','{$semester_kelas['GROUP_SEMESTER']}','UP','True','False','False','False')");
			$semester_up = $this->db->QuerySingle("
			SELECT ID_SEMESTER FROM SEMESTER WHERE UPPER(NM_SEMESTER) LIKE UPPER('%UJIAN_PERBAIKAN%') AND TAHUN_AJARAN=
			(SELECT TAHUN_AJARAN FROM SEMESTER WHERE ID_SEMESTER='{$semester_kelas['ID_SEMESTER']}') AND GROUP_SEMESTER=
			(SELECT GROUP_SEMESTER FROM SEMESTER WHERE ID_SEMESTER='{$semester_kelas['ID_SEMESTER']}')");
		}
		return $semester_up;
	}

	function cek_kelas_remidi($id_mhs, $id_kelas_mk)
	{
		$semester_remidi = $this->get_semester_remidi($id_kelas_mk);
		$remidi = $this->db->QuerySingle("
			SELECT COUNT(*) FROM PENGAMBILAN_MK WHERE ID_KELAS_MK='{$id_kelas_mk}' AND ID_MHS='{$id_mhs}' AND ID_SEMESTER ='{$semester_remidi}'");
		return $remidi;
	}

	function cek_kelas_ujian_perbaikan($id_mhs, $id_kelas_mk)
	{
		$semester_up = $this->get_semester_up($id_kelas_mk);
		$up = $this->db->QuerySingle("
			SELECT COUNT(*) FROM PENGAMBILAN_MK WHERE ID_KELAS_MK='{$id_kelas_mk}' AND ID_MHS='{$id_mhs}' AND ID_SEMESTER ='{$semester_up}'");
		return $up;
	}

	function insert_pengambilan_remidi($id_mhs, $id_kelas_mk)
	{
		$semester_kelas = $this->get_semester_kelas_mk($id_kelas_mk);
		$semester_remidi = $this->get_semester_remidi($id_kelas_mk);
		if ($this->cek_kelas_remidi($id_mhs, $id_kelas_mk) > 0)
		{
			$this->db->Query("
				DELETE FROM PENGAMBILAN_MK WHERE ID_KELAS_MK='{$id_kelas_mk}' AND ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester_remidi}'");
		}
		else
		{
			$this->db->Query("
				INSERT INTO PENGAMBILAN_MK (ID_KELAS_MK,ID_MHS,ID_SEMESTER,STATUS_APV_PENGAMBILAN_MK,STATUS_PENGAMBILAN_MK,STATUS_TAMPIL,STATUS_CEKAL,ID_KURIKULUM_MK)
				SELECT PMK.ID_KELAS_MK,PMK.ID_MHS,{$semester_remidi} AS ID_SEMESTER,PMK.STATUS_APV_PENGAMBILAN_MK,PMK.STATUS_PENGAMBILAN_MK,PMK.STATUS_TAMPIL,1 STATUS_CEKAL
					,KMK.ID_KURIKULUM_MK
				FROM PENGAMBILAN_MK PMK
				JOIN KELAS_MK KMK ON PMK.ID_KELAS_MK=KMK.ID_KELAS_MK
				WHERE PMK.ID_KELAS_MK='{$id_kelas_mk}' AND PMK.ID_MHS='{$id_mhs}' AND PMK. ID_SEMESTER='{$semester_kelas['ID_SEMESTER']}'");
		}
	}

	function insert_pengambilan_up($id_mhs, $id_kelas_mk)
	{
		$semester_kelas = $this->get_semester_kelas_mk($id_kelas_mk);
		$semester_up = $this->get_semester_up($id_kelas_mk);
		if ($this->cek_kelas_ujian_perbaikan($id_mhs, $id_kelas_mk) > 0)
		{
			$this->db->Query("
				DELETE FROM PENGAMBILAN_MK WHERE ID_KELAS_MK='{$id_kelas_mk}' AND ID_MHS='{$id_mhs}' AND ID_SEMESTER='{$semester_up}'");
		}
		else
		{
			$this->db->Query("
				INSERT INTO PENGAMBILAN_MK (ID_KELAS_MK,ID_MHS,ID_SEMESTER,STATUS_APV_PENGAMBILAN_MK,STATUS_PENGAMBILAN_MK,STATUS_TAMPIL,STATUS_CEKAL,ID_KURIKULUM_MK)
				SELECT PMK.ID_KELAS_MK,PMK.ID_MHS,{$semester_up} AS ID_SEMESTER,PMK.STATUS_APV_PENGAMBILAN_MK,PMK.STATUS_PENGAMBILAN_MK,PMK.STATUS_TAMPIL,1 STATUS_CEKAL,
					KMK.ID_KURIKULUM_MK
				FROM PENGAMBILAN_MK PMK
				JOIN KELAS_MK KMK ON KMK.ID_KELAS_MK=PMK.ID_KELAS_MK
				WHERE PMK.ID_KELAS_MK='{$id_kelas_mk}' AND PMK.ID_MHS='{$id_mhs}' AND PMK.ID_SEMESTER='{$semester_kelas['ID_SEMESTER']}'");
		}
	}

	// Fungsi Cetak Nilai

	function load_distribusi_nilai($id_kelas_mk)
	{
		return $this->db->QueryToArray("
			SELECT NILAI_HURUF,
			COUNT(ID_PENGAMBILAN_MK)/(SELECT COUNT(ID_PENGAMBILAN_MK) FROM PENGAMBILAN_MK WHERE ID_KELAS_MK='{$id_kelas_mk}')*100 DISTRIBUSI_NILAI
			FROM PENGAMBILAN_MK 
			WHERE ID_KELAS_MK='{$id_kelas_mk}'
			GROUP BY NILAI_HURUF
			ORDER BY NILAI_HURUF
			");
	}

	// Fngsi Monitoring Nilai
	function update_status_publish($pengguna, $semester, $fakultas, $status, $jenjang)
	{
		$status_lain = $status == '1' ? "0" : "1";
		$this->db->Query("INSERT INTO LOG_PUBLISH_NILAI (ID_PENGGUNA,ID_FAKULTAS,ID_SEMESTER,ID_JENJANG,STATUS_PUBLISH) VALUES ('{$pengguna}','{$fakultas}','{$semester}','{$jenjang}','{$status}')");
		$this->db->Query("
			UPDATE AUCC.PENGAMBILAN_MK SET FLAGNILAI='{$status}' WHERE ID_SEMESTER='{$semester}' AND FLAGNILAI='{$status_lain}' AND ID_KELAS_MK IN (
			SELECT KMK.ID_KELAS_MK
			FROM AUCC.PENGAMBILAN_MK PMK
			JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK=PMK.ID_KELAS_MK
			JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=KMK.ID_PROGRAM_STUDI
			WHERE PS.ID_FAKULTAS='{$fakultas}' AND PS.ID_JENJANG='{$jenjang}'
			GROUP BY KMK.ID_KELAS_MK
			)
			");
	}

	function update_status_publish_d3($semester, $fakultas, $status, $jenjang)
	{
		$this->db->Query("
			UPDATE AUCC.PENGAMBILAN_MK SET FLAGNILAI='{$status}' WHERE ID_SEMESTER='{$semester}' AND FLAGNILAI='1' 
			AND ID_KELAS_MK IN (
				SELECT KMK.ID_KELAS_MK
				FROM AUCC.PENGAMBILAN_MK PMK
				JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK=PMK.ID_KELAS_MK
				JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=KMK.ID_PROGRAM_STUDI
				WHERE PS.ID_FAKULTAS='{$fakultas}' AND PS.ID_JENJANG='{$jenjang}'
				GROUP BY KMK.ID_KELAS_MK
			)
			AND ID_MHS IN (
				SELECT M.ID_MHS FROM AUCC.MAHASISWA M
				JOIN (
				SELECT ROUND(SUM(KREDIT_SEMESTER*NILAI_STANDAR_NILAI)/SUM(KREDIT_SEMESTER), 2) AS IPK, SUM(KREDIT_SEMESTER) AS SKS, ID_MHS
					FROM (SELECT A.ID_MHS,NILAI_STANDAR_NILAI, E.TAHUN_AJARAN, E.NM_SEMESTER, 
					C.KD_MATA_KULIAH,C.NM_MATA_KULIAH,D.KREDIT_SEMESTER,A.NILAI_HURUF, A.FLAGNILAI, 
					ROW_NUMBER() OVER(PARTITION BY A.ID_MHS,C.NM_MATA_KULIAH ORDER BY A.NILAI_HURUF) RANGKING,
					COUNT(*) OVER(PARTITION BY C.NM_MATA_KULIAH) TERULANG
							FROM AUCC.PENGAMBILAN_MK A, AUCC.MATA_KULIAH C, AUCC.KURIKULUM_MK D, AUCC.SEMESTER E, AUCC.STANDAR_NILAI F, AUCC.MAHASISWA G, AUCC.PROGRAM_STUDI H
							WHERE A.ID_KURIKULUM_MK=D.ID_KURIKULUM_MK AND D.ID_MATA_KULIAH=C.ID_MATA_KULIAH AND A.ID_SEMESTER=E.ID_SEMESTER
							AND A.STATUS_APV_PENGAMBILAN_MK='1' AND NM_STANDAR_NILAI = NILAI_HURUF AND G.ID_MHS = A.ID_MHS
							AND H.ID_PROGRAM_STUDI = G.ID_PROGRAM_STUDI AND H.ID_FAKULTAS='{$fakultas}' AND H.ID_JENJANG =5
					) X
					WHERE RANGKING = 1 AND FLAGNILAI = 1 AND NILAI_HURUF < 'E' AND NILAI_HURUF IS NOT NULL AND NILAI_HURUF != '-'
					GROUP BY ID_MHS
				) STS ON STS.ID_MHS=M.ID_MHS
				WHERE STS.SKS>100 AND M.STATUS_AKADEMIK_MHS=1
			)
			");
	}

	function load_publish($semester, $fakultas)
	{
		$arr_hasil = array();
		$jenjang = $this->load_jenjang_publish($semester, $fakultas);
		foreach ($jenjang as $j)
		{
			$this->db->Query("
			SELECT ID_JENJANG,SUM(CASE WHEN STATUS=1 THEN 1 ELSE 0 END) PUBLISH,SUM(CASE WHEN STATUS=0 THEN 1 ELSE 0 END) BLM_PUBLISH 
			FROM 
			(
				SELECT PS.ID_JENJANG,PMK.ID_KELAS_MK
				,AVG(FLAGNILAI) STATUS 
				FROM AUCC.PENGAMBILAN_MK PMK
				JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK=PMK.ID_KELAS_MK
				JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=KMK.ID_PROGRAM_STUDI
				WHERE PMK.ID_SEMESTER='{$semester}' AND PS.ID_FAKULTAS='{$fakultas}' AND PS.ID_JENJANG='{$j['ID_JENJANG']}'
				GROUP BY PS.ID_JENJANG,PMK.ID_KELAS_MK
			)
			GROUP BY ID_JENJANG
			");
			$arr_publish = $this->db->FetchAssoc();
			array_push($arr_hasil, array_merge($j, array(
				'DATA_PUBLISH' => $arr_publish)));
		}
		return $arr_hasil;
	}

	function load_mahasiswa_mau_yudisium_d3($fakultas)
	{
		// Mahasiswa D3 SKS Total > 100
		$mhs = $this->db->QueryToArray("
			SELECT M.* FROM AUCC.MAHASISWA M
			JOIN (
			SELECT ROUND(SUM(KREDIT_SEMESTER*NILAI_STANDAR_NILAI)/SUM(KREDIT_SEMESTER), 2) AS IPK, SUM(KREDIT_SEMESTER) AS SKS, ID_MHS
				FROM (SELECT A.ID_MHS,NILAI_STANDAR_NILAI, E.TAHUN_AJARAN, E.NM_SEMESTER, 
				C.KD_MATA_KULIAH,C.NM_MATA_KULIAH,D.KREDIT_SEMESTER,A.NILAI_HURUF, A.FLAGNILAI, 
				ROW_NUMBER() OVER(PARTITION BY A.ID_MHS,C.NM_MATA_KULIAH ORDER BY A.NILAI_HURUF) RANGKING,
				COUNT(*) OVER(PARTITION BY C.NM_MATA_KULIAH) TERULANG
						FROM AUCC.PENGAMBILAN_MK A, AUCC.MATA_KULIAH C, AUCC.KURIKULUM_MK D, AUCC.SEMESTER E, AUCC.STANDAR_NILAI F, AUCC.MAHASISWA G, AUCC.PROGRAM_STUDI H
						WHERE A.ID_KURIKULUM_MK=D.ID_KURIKULUM_MK AND D.ID_MATA_KULIAH=C.ID_MATA_KULIAH AND A.ID_SEMESTER=E.ID_SEMESTER
						AND A.STATUS_APV_PENGAMBILAN_MK='1' AND NM_STANDAR_NILAI = NILAI_HURUF AND G.ID_MHS = A.ID_MHS
						AND H.ID_PROGRAM_STUDI = G.ID_PROGRAM_STUDI AND H.ID_FAKULTAS='{$fakultas}' AND H.ID_JENJANG =5
				) X
				WHERE RANGKING = 1 AND FLAGNILAI = 1 AND NILAI_HURUF < 'E' AND NILAI_HURUF IS NOT NULL AND NILAI_HURUF != '-'
				GROUP BY ID_MHS
			) STS ON STS.ID_MHS=M.ID_MHS
			WHERE STS.SKS>100 AND M.STATUS_AKADEMIK_MHS=1
			");
		return $mhs;
	}

	function load_jenjang_publish($semester, $fakultas)
	{
		return $this->db->QueryToArray("
			SELECT * FROM JENJANG
			WHERE ID_JENJANG IN 
			(
				SELECT PS.ID_JENJANG
				FROM KELAS_MK KMK
				JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=KMK.ID_PROGRAM_STUDI
				WHERE PS.ID_FAKULTAS='{$fakultas}' AND KMK.ID_SEMESTER='{$semester}'
				GROUP BY PS.ID_JENJANG
			)
			ORDER BY NM_JENJANG
			");
	}

	function load_rekap_nilai($id_fakultas, $status_nilai, $semester)
	{
		// Disable pengecualian Fakultas - 14 April 2016
		// $id_fakultas = 0.5;
			
		// Nilai Masuk
		if ($status_nilai == 1)
		{
			// Fakultas FST-UNAIR
			/*
			if ($id_fakultas == 8)
			{
				$query = "
					select kd_mata_kuliah,nm_mata_kuliah,kelas_mk.id_kelas_mk,kurikulum_mk.kredit_semester,nmkelas nama_kelas,gelar_depan||nm_pengguna||', '||gelar_belakang as pjma,dosen.mobile_dosen,
					count(id_mhs) as pst,sum(case when nilai_angka is not null and nilai_angka >0 then 1 else 0 end) as nilmasuk,
					case when(sum(case when pengambilan_mk.flagnilai='1' then 1 else 0 end)=count(id_mhs)) then 'PUBLISH' else 'BELUM' end as terpublish
					from kelas_mk
					left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1
					left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
					left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
					left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
					left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join fst_kelas on kelas_mk.tipe_kelas_mk=fst_kelas.kdkelas
					where kelas_mk.id_semester='{$semester}' and program_studi.id_fakultas='{$id_fakultas}' and status_apv_pengambilan_mk=1
					group by kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester,kelas_mk.id_kelas_mk, nmkelas, gelar_depan||nm_pengguna||', '||gelar_belakang,mobile_dosen
					having sum(case when nilai_angka is not null and nilai_angka >0 then 1 else 0 end)>=(count(id_mhs)/2) and count(id_mhs)<>0";
			}
			else
			{
			 */
			
				$query = "
					select kd_mata_kuliah,nm_mata_kuliah,kelas_mk.id_kelas_mk,kurikulum_mk.kredit_semester,nama_kelas,gelar_depan||nm_pengguna||', '||gelar_belakang as pjma,dosen.mobile_dosen,
					count(id_mhs) as pst,sum(case when nilai_angka is not null and nilai_angka >0 then 1 else 0 end) as nilmasuk,
					case when(sum(case when pengambilan_mk.flagnilai='1' then 1 else 0 end)=count(id_mhs)) then 'PUBLISH' else 'BELUM' end as terpublish
					from kelas_mk
					left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1
					left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
					left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
					left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
					left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
					where kelas_mk.id_semester='{$semester}' and program_studi.id_fakultas='{$id_fakultas}' and status_apv_pengambilan_mk=1
					group by kd_mata_kuliah, nm_mata_kuliah,kelas_mk.id_kelas_mk, kurikulum_mk.kredit_semester, nama_kelas, gelar_depan||nm_pengguna||', '||gelar_belakang,mobile_dosen
					having sum(case when nilai_angka is not null and nilai_angka >0 then 1 else 0 end)>=(count(id_mhs)/2) and count(id_mhs)<>0
					order by kd_mata_kuliah";
			/*
			}
			 */
		}
		//Nilai Belum Masuk
		else
		{
			/*
			if ($id_fakultas == 8)
			{
				$query = "
					select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nmkelas nama_kelas,gelar_depan||nm_pengguna||', '||gelar_belakang as pjma,dosen.mobile_dosen,
					count(id_mhs) as pst,sum(case when nilai_angka is null or nilai_angka <=0 then 1 else 0 end) as nilmasuk,
					case when(sum(case when pengambilan_mk.flagnilai='1' then 1 else 0 end)=count(id_mhs)) then 'PUBLISH' else 'BELUM' end as terpublish
					from kelas_mk
					left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1
					left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
					left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
					left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
					left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join fst_kelas on kelas_mk.tipe_kelas_mk=fst_kelas.kdkelas
					where kelas_mk.id_semester='{$semester}' and program_studi.id_fakultas={$id_fakultas} and status_apv_pengambilan_mk=1
					group by kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, nmkelas, gelar_depan||nm_pengguna||', '||gelar_belakang,mobile_dosen
					having sum(case when nilai_angka is null or nilai_angka <=0 then 1 else 0 end)>=(count(id_mhs)/2) and count(id_mhs)<>0";
			}
			else
			{
			 */
				$query = "
					select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nama_kelas,gelar_depan||nm_pengguna||', '||gelar_belakang as pjma,dosen.mobile_dosen,
					count(id_mhs) as pst,sum(case when nilai_angka is null or nilai_angka <=0 then 1 else 0 end) as nilmasuk,
					case when(sum(case when pengambilan_mk.flagnilai='1' then 1 else 0 end)=count(id_mhs)) then 'PUBLISH' else 'BELUM' end as terpublish
					from kelas_mk
					left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk and pjmk_pengampu_mk=1
					left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
					left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
					left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
					left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
					left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
					where kelas_mk.id_semester='{$semester}' and program_studi.id_fakultas='{$id_fakultas}' and status_apv_pengambilan_mk=1
					group by kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas, gelar_depan||nm_pengguna||', '||gelar_belakang,mobile_dosen
					having sum(case when nilai_angka is null or nilai_angka <=0 then 1 else 0 end)>=(count(id_mhs)/2) and count(id_mhs)<>0
					order by kd_mata_kuliah";
			/*
			}
			 */
		}
		
		return $this->db->QueryToArray($query);
	}

}