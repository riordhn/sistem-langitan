<?php
require('../../../config.php');
$db = new MyOracle();

$id_pt = $id_pt_user;

require_once('../../../tcpdf/config/lang/eng.php');
require_once('../../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Detail Perwalian');
$pdf->SetSubject('Detail Perwalian');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logo-".$nama_singkat.".gif";
$logo_size = "25";
$title = strtoupper($nama_pt);
/*$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";*/

$id_dosen = $_GET['cetak'];
$id_pengguna=$user->ID_PENGGUNA;
$kdfak=$user->ID_FAKULTAS;
$kdprodi=$user->ID_PROGRAM_STUDI;

$smtaktif = "select id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = '{$id_pt}'";
$result = $db->Query($smtaktif)or die("salah kueri smtaktif ");
while($r = $db->FetchRow()) {
	$kdsmt = $r[0];
}

$kueri = "select f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas,
            j.nm_jenjang, ps.nm_program_studi 
            from program_studi ps
            join jenjang j on j.id_jenjang = ps.id_jenjang
            join fakultas f on f.id_fakultas = ps.id_fakultas
            where ps.id_program_studi=$kdprodi";
$result = $db->Query($kueri)or die("salah kueri 22 ");
while($r = $db->FetchRow()) {
	$fak = $r[0];
	$alm_fak = $r[1];
	$pos_fak = $r[2];
	$tel_fak = $r[3];
	$fax_fak = $r[4];
	$web_fak = $r[5];
	$eml_fak = $r[6];
  $jenjang = $r[7];
  $prodi = $r[8];
}

$kueri1 = "select tahun_ajaran, nm_semester from semester where id_semester=$kdsmt";
$result = $db->Query($kueri1)or die("salah kueri 26 ");
while($r = $db->FetchRow()) {
	$thn = $r[0];
	$smt = $r[1];
}

$dat = "select dsn.nip_dosen, trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)||', '||pgg.gelar_belakang) as dosen,
upper(gol.nm_golongan) as golongan, upper(gol.nm_pangkat) as pangkat
from dosen dsn
left join pengguna pgg on dsn.id_pengguna=pgg.id_pengguna
left join golongan gol on gol.id_golongan=dsn.id_golongan
where dsn.id_dosen=$id_dosen";
$result = $db->Query($dat)or die("salah kueri dat ");
while($r = $db->FetchRow()) {
	$nip_dsn = $r[0];
	$nm_dsn = $r[1];
	$nm_gol = $r[2];
	$nm_pkt = $r[3];
}
					
$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($jenjang)." - ".strtoupper($prodi)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 40, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 14);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('P', 'A4');

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="4" align="center"><b><u>DAFTAR PERWALIAN SEMESTER '.strtoupper($smt).' '.$thn.'</u></b></td>
  </tr>
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2" width="20%"><b>NIP/NIK</b></td>
    <td colspan="2" width="80%"><b>:&nbsp;&nbsp;&nbsp;'.$nip_dsn.'</b></td>
  </tr>
  <tr>
    <td colspan="2"><b>NAMA DOSEN</b></td>
    <td colspan="2"><b>:&nbsp;&nbsp;&nbsp;'.$nm_dsn.'</b></td>
  </tr>
  <tr>
    <td colspan="2"><b>GOLONGAN</b></td>
    <td colspan="2"><b>:&nbsp;&nbsp;&nbsp;'.$nm_gol.'</b></td>
  </tr>
  <tr>
    <td colspan="2"><b>PANGKAT</b></td>
    <td colspan="2"><b>:&nbsp;&nbsp;&nbsp;'.$nm_pkt.'</b></td>
  </tr>
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
</table>
<table width="100%" cellspacing="0" cellpadding="5">
<thead>
  <tr style="background-color:#373737; color:#fff;">
      <td border="1" width="5%" align="center"><strong>NO.</strong></td>
	  <td border="1" width="15%" align="center"><strong>NIM</strong></td>
      <td border="1" width="40%" align="center"><strong>NAMA MAHASISWA</strong></td>
	  <td border="1" width="40%" align="center"><strong>PROGRAM STUDI</strong></td>
    </tr>
</thead>';

$nomor=1;
$kueri = "select nim_mhs,upper(pengguna.nm_pengguna) as mhs, upper(nm_jenjang)||' - '||upper(nm_program_studi) as prodi
from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join dosen_wali on dosen_wali.id_mhs=mahasiswa.id_mhs
left join program_studi on program_studi.id_program_studi=mahasiswa.id_program_studi
left join jenjang on jenjang.id_jenjang=program_studi.id_jenjang
where dosen_wali.id_dosen=$id_dosen and mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
and dosen_wali.status_dosen_wali='1'
order by nim_mhs";
$result = $db->Query($kueri)or die("salah kueri 27 ");
$i=0;
while($r = $db->FetchRow()) {
	$nim_mhs = $r[0];
	$nama_mhs = $r[1];
	$prodi = $r[2];
$i++;   
if (($i % 2)==0) $bg='lightgrey';
else $bg='white';
$html .= '
    <tr nobr="true" bgcolor="'.$bg.'" style="color:#000;">
      <td border="1" width="5%" align="center">'.$nomor.'</td>
	  <td border="1" width="15%" align="center">'.$nim_mhs.'</td>
      <td border="1" width="40%" align="left">'.$nama_mhs.'</td>
	  <td border="1" width="40%" align="center">'.$prodi.'</td>
    </tr>';
$nomor++;
}
$html .= '</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('DAFTAR PERWALIAN '.$nip_dsn.'.pdf', 'I');

?>