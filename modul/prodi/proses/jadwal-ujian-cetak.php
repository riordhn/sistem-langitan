<?php
require('../../../config.php');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().' - '.date("Y-m-d H:i:s", time()), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('langitan');
$pdf->SetTitle('Daftar Pengawas Ujian');
$pdf->SetSubject('Daftar Pengawas Ujian');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

//parameter header
$logo = "../../img/akademik_images/logo-".$nama_singkat.".gif";
$logo_size = "25";
$title = strtoupper($nama_pt);

$kdfak = $user->ID_FAKULTAS;
$id_pt = $id_pt_user;
$ujian = $_GET['cetak'];
$id_smt = $_GET['smt'];

$thn_smt = "select nm_semester, tahun_ajaran from semester where id_semester=$id_smt";
$result = $db->Query($thn_smt)or die("salah kueri thn_smt ");
while($r = $db->FetchRow()) {
	$smt = $r[0];
	$thn = $r[1];
}

// memanggil id_kegiatan UTS tiap PTNU
$kegiatanUTS = "select id_kegiatan from kegiatan where id_perguruan_tinggi = '{$id_pt}' and kode_kegiatan = 'UTS'";
$result = $db->Query($kegiatanUTS)or die("salah kueri keg_uts ");
while($r = $db->FetchRow()) {
  $keg_uts = $r[0];
}

$fak = "select a.nm_jenjang, b.nm_program_studi, f.nm_fakultas, f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas
			from jenjang a, program_studi b, fakultas f where a.id_jenjang=b.id_jenjang and f.id_fakultas=b.id_fakultas and b.id_fakultas=$kdfak";
$result = $db->Query($fak)or die("salah kueri fak ");
while($r = $db->FetchRow()) {
	$jenjang = $r[0];
	$prodi = $r[1];
	$fak = $r[2];
	$alm_fak = $r[3];
	$pos_fak = $r[4];
	$tel_fak = $r[5];
	$fax_fak = $r[6];
	$web_fak = $r[7];
	$eml_fak = $r[8];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(5, 35, 5);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 15);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage('L', 'A4');

if ($ujian == $keg_uts) {
$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="5" align="center"><b><u>DAFTAR PENGAWAS UJIAN TENGAH SEMESTER '.strtoupper($smt).' '.$thn.'</u></b></td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
</table>';
} else {
$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="5" align="center"><b><u>DAFTAR PENGAWAS UJIAN AKHIR SEMESTER '.strtoupper($smt).' '.$thn.'</u></b></td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
</table>';
}

$html .= '
<table width="100%" cellspacing="0" cellpadding="5" border="1">
<thead>
  <tr bgcolor="black" style="color:white;">
      <td border="1" width="5%" align="center"><strong>NO.</strong></td>
	  <td border="1" width="10%" align="center"><strong>KODE MK</strong></td>
      <td border="1" width="15%" align="center"><strong>MATA KULIAH</strong></td>
	  <td border="1" width="5%" align="center"><strong>KLS</strong></td>
      <td border="1" width="10%" align="center"><strong>PRODI</strong></td>
      <td border="1" width="10%" align="center"><strong>JENIS</strong></td>
      <td border="1" width="10%" align="center"><strong>RUANG</strong></td>
      <td border="1" width="10%" align="center"><strong>WAKTU</strong></td>
      <td border="1" width="5%" align="center"><strong>PST</strong></td>
      <td border="1" width="20%" align="center"><strong>PENGAWAS</strong></td>
    </tr>
</thead>';

$nomor=1;
$list = 
"SELECT
*
FROM
( (
SELECT
  umk.id_kelas_mk,
  umk.id_ujian_mk,
  mk.kd_mata_kuliah,
  mk.nm_mata_kuliah,
  kur.kredit_semester,
  umk.nm_ujian_mk,
  TO_CHAR(umk.tgl_ujian, 'DD-MM-YYYY') AS tgl_ujian,
  umk.jam_mulai || ' - ' || umk.jam_selesai AS jam,
  rg.nm_ruangan,
  rg.kapasitas_ujian,
  jjg.nm_jenjang || '-' || ps.nm_singkat_prodi AS prodi,
  LISTAGG(gelar_depan || ' ' || UPPER(nm_pengguna)|| ', ' || gelar_belakang) WITHIN GROUP (
  ORDER BY nm_pengguna) AS tim,
  nmk.nama_kelas,
  TO_CHAR(umk.tgl_ujian, 'YYYYMMDD') AS urut
FROM ujian_mk umk
LEFT JOIN kelas_mk kmk ON kmk.id_kelas_mk = umk.id_kelas_mk
LEFT JOIN nama_kelas nmk ON nmk.id_nama_kelas = kmk.no_kelas_mk
LEFT JOIN kurikulum_mk kur ON kur.id_kurikulum_mk = kmk.id_kurikulum_mk
LEFT JOIN mata_kuliah mk ON kur.id_mata_kuliah = mk.id_mata_kuliah
LEFT JOIN program_studi ps ON ps.id_program_studi = kmk.id_program_studi
LEFT JOIN jenjang jjg ON jjg.id_jenjang = ps.id_jenjang
LEFT JOIN jadwal_ujian_mk jad ON jad.id_ujian_mk = umk.id_ujian_mk
LEFT JOIN ruangan rg ON rg.id_ruangan = jad.id_ruangan
LEFT JOIN tim_pengawas_ujian tim ON jad.id_jadwal_ujian_mk = tim.id_jadwal_ujian_mk
LEFT JOIN pengguna pgg ON pgg.id_pengguna = tim.id_pengguna
WHERE
  umk.id_kegiatan = '{$ujian}'
  AND umk.id_fakultas = '{$kdfak}'
  AND umk.id_semester = '{$id_smt}'
GROUP BY
  umk.id_kelas_mk,
  umk.id_ujian_mk,
  mk.kd_mata_kuliah,
  mk.nm_mata_kuliah,
  kur.kredit_semester,
  umk.nm_ujian_mk,
  umk.tgl_ujian,
  umk.jam_mulai,
  umk.jam_selesai,
  rg.nm_ruangan,
  rg.kapasitas_ujian,
  jjg.nm_jenjang,
  ps.nm_singkat_prodi,
  nmk.nama_kelas) a
LEFT JOIN (
  SELECT id_ujian_mk, COUNT(*) AS kls_terisi FROM ujian_mk_peserta GROUP BY id_ujian_mk
) b ON
a.id_ujian_mk = b.id_ujian_mk )
ORDER BY
urut,
jam,
nm_mata_kuliah,
nama_kelas";

$result = $db->Query($list)or die("salah queri list");
$i=0;
while($r = $db->FetchRow()) {
	$kode_mk = $r[2];
	$nama_mk = $r[3];
	$kelas_mk = $r[12];
	$prodi_mk = $r[10];
	$ruang_mk = $r[8];
	$pst_mk = $r[15];
	$tgl_umk = $r[6];
	$jam_umk = $r[7];
	$tim_umk = $r[11];
	$jenis_umk = $r[5];
$i++;   
if (($i % 2)==0) $bgcolor='lightgrey';
else $bgcolor='white';
$html .= '
    <tr bgcolor="'.$bgcolor.'" style="color:black;">
      <td border="1" width="5%" align="center" valign="middle">'.$nomor.'</td>
	  <td border="1" width="10%" align="center" valign="middle">'.$kode_mk.'</td>
      <td border="1" width="15%" align="left" valign="middle">'.strtoupper($nama_mk).'</td>
      <td border="1" width="5%" align="center" valign="middle">'.strtoupper($kelas_mk).'</td>
      <td border="1" width="10%" align="center" valign="middle">'.strtoupper($prodi_mk).'</td>
      <td border="1" width="10%" align="center" valign="middle">'.strtoupper($jenis_umk).'</td>
      <td border="1" width="10%" align="center" valign="middle">'.strtoupper($ruang_mk).'</td>
      <td border="1" width="10%" align="center" valign="middle">'.$tgl_umk.'<br/>'.$jam_umk.'</td>
      <td border="1" width="5%" align="center" valign="middle">'.$pst_mk.'</td>
      <td border="1" width="20%" align="left" valign="middle">'.$tim_umk.'</td>
    </tr>';
$nomor++;
}
$html .= '
</table>';


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('DAFTAR PENGAWAS UJIAN.pdf', 'I');