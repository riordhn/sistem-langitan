<?php
require('../../../config.php');
$db = new MyOracle();

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=rekap-doli.xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

echo '
<table border="1" cellspacing="0" cellpadding="0">
           <tr>
             <td bgcolor="lightgrey" align="center"><b>NIM Mahasiswa</b></td>
             <td bgcolor="lightgrey" align="center"><b>NIP Dosen</b></td>
           </tr>';

$kueri="select distinct mahasiswa.nim_mhs,dosen.nip_dosen
		from dosen_wali
		left join mahasiswa on mahasiswa.id_mhs = dosen_wali.id_mhs
		left join dosen on dosen.id_dosen = dosen_wali.id_dosen
		where mahasiswa.id_mhs in (select id_mhs from mahasiswa
		where id_program_studi in (select id_program_studi from program_studi where id_fakultas = 11) and
		status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1))						
		and dosen_wali.status_dosen_wali='1'";
$result = $db->Query($kueri)or die("salah kueri 01 ");
$i=1;
while($r = $db->FetchRow()) {
echo '
           <tr>
             <td>&nbsp;'.$r[0].'</td>
             <td>&nbsp;'.$r[1].'</td>
           </tr>';
$i++;
}
echo '
</table>';

?>