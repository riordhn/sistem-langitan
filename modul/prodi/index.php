<?php
include 'config.php';
include 'ociFunction.php';

$id_prodi = $user->ID_PROGRAM_STUDI;

if ($user->IsLogged() && $user->Role() == AUCC_ROLE_PRODI && !empty($user->ID_PROGRAM_STUDI))
{
	
    $dataprodi = getData(
        "SELECT nm_jenjang||'-'||nm_program_studi as nm_prodi from program_studi 
        left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
        where id_program_studi=$id_prodi");

    $smarty->assign('dataprodi',$dataprodi);
    
    $smarty->assign('modul_set', $user->MODULs);

    $smarty->display('index.tpl');
    
}
else
{
    echo '<p>"Maaf..., Anda Belum Bisa Login, Silahkan Hub Admin Sistem Langitan untuk mengecek Homebase Prodi Anda "</p>';
    echo '<a href="/logout.php">Logout</a>';
    echo '<iframe src="/includes/changepassword.php" width="500px" height="400px" style="border: none"></iframe>';
}