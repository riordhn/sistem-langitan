<?php
/*
Yudi Sulistya on 23-05-2013
*/

error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$kdprodi = $user->ID_PROGRAM_STUDI; 
$kdfak = $user->ID_FAKULTAS;

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');

$smtaktif=getvar("select id_semester,thn_akademik_semester from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND status_aktif_semester='True'");
$smt = isSet($_REQUEST['smt']) ? $_REQUEST['smt'] : $smtaktif['ID_SEMESTER'];
$smarty->assign('SMT',$smt);

$smt1=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester 
			  where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' AND nm_semester in ('Ganjil','Genap') and  thn_akademik_semester > ('$smtaktif[THN_AKADEMIK_SEMESTER]' - 2) 
			  order by thn_akademik_semester desc,nm_semester desc");
$smarty->assign('T_ST', $smt1);

$tipe_ta=getData("select * from tipe_ta where id_tipe_ta = 4 order by id_tipe_ta");
$smarty->assign('tipe_ta', $tipe_ta);

if ($_GET['action']=='edit'){
		
		// Yudi Sulistya, 23-05-2013 (penambahan filter tipe TA)
		$id_mhs=$_GET['id_mhs'];				
		$id_tipe_ta=$_GET['tipe'];

		if ($id_tipe_ta == '') {
		$ta=getData("select m.id_mhs,m.nim_mhs,p.nm_pengguna as mhs
						from mahasiswa m
						join pengguna p on m.id_pengguna=p.id_pengguna  
						where m.id_mhs=$id_mhs");		
		} else {
		$ta=getData("select m.id_mhs,m.nim_mhs,p.nm_pengguna as mhs,
						tugas_akhir.id_tugas_akhir,judul_tugas_akhir,deskripsi_tugas_akhir
						from mahasiswa m
						join pengguna p on m.id_pengguna=p.id_pengguna 
						left join tugas_akhir on m.id_mhs=tugas_akhir.id_mhs and tugas_akhir.status=1 
						where m.id_mhs=$id_mhs and tugas_akhir.id_tipe_ta=$id_tipe_ta");
		}
		
		$smarty->assign('ta',$ta);

		if ($id_tipe_ta == '') {
		$jenis=getData("select trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna, jenis_pembimbing.*, pembimbing_ta.*
						from jenis_pembimbing 
						left join pembimbing_ta on pembimbing_ta.id_jenis_pembimbing = jenis_pembimbing.id_jenis_pembimbing and pembimbing_ta.id_mhs = $id_mhs and pembimbing_ta.status_dosen = 1
						left join dosen on dosen.id_dosen = pembimbing_ta.id_dosen
						left join pengguna on pengguna.id_pengguna = dosen.id_pengguna
						where pembimbing_ta.id_tugas_akhir is not null
						order by jenis_pembimbing.id_jenis_pembimbing");		
		} else {
		$jenis=getData("select trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna, jenis_pembimbing.*, pembimbing_ta.*
						from jenis_pembimbing 
						left join pembimbing_ta on pembimbing_ta.id_jenis_pembimbing = jenis_pembimbing.id_jenis_pembimbing and pembimbing_ta.id_mhs = $id_mhs and pembimbing_ta.status_dosen = 1
						left join tugas_akhir on tugas_akhir.id_tugas_akhir = pembimbing_ta.id_tugas_akhir and tugas_akhir.id_tipe_ta = $id_tipe_ta and tugas_akhir.status = 1
						left join dosen on dosen.id_dosen = pembimbing_ta.id_dosen
						left join pengguna on pengguna.id_pengguna = dosen.id_pengguna
						order by jenis_pembimbing.id_jenis_pembimbing");
		}
		
		$smarty->assign('jenis',$jenis);
		
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
}

if ($_GET['action']=='searchdosen'){
		
		$ta=$_GET['ta'];
		$status1=$_GET['status1'];
		
		$namacari=$_POST['namadosen'];
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');

			
		if ($namacari !='') {
		// Added by Yudi Sulistya on Dec 02, 2011
		$upper = strtoupper($namacari);
		$lower = strtolower($namacari);
		$proper = ucwords($namacari);
		$hasil=getData("select id_dosen,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna, foto_pengguna , upper(nm_program_studi) as nm_program_studi
		from dosen 
		left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
		left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
		where (nm_pengguna like '%$upper%' or nm_pengguna like '%$lower%' or nm_pengguna like '%$proper%') and dosen.id_status_pengguna=22
		order by trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang)");}
		$smarty->assign('DOSEN',$hasil);
		$smarty->assign('ST_ta',$ta);
		
}

if ($_GET['action']=='addview'){

		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		
		$id_mhs=$_GET['id_mhs'];
		$id_dosen=$_GET['id_dosen'];
		$id_jenis=$_GET['jenis'];
		$id_tugas_akhir=$_GET['ta'];

		$cek=getvar("select count(*) as cek from pembimbing_ta where id_mhs = '$id_mhs' and id_jenis_pembimbing='$id_jenis' and id_tugas_akhir='$id_tugas_akhir'");
		if($cek['CEK'] == 0){
			InsertData("insert into pembimbing_ta (id_mhs,id_semester,id_dosen,id_jenis_pembimbing,status_dosen,id_tugas_akhir)
						values ('$id_mhs', '$smt', '$id_dosen', '$id_jenis', '1', '$id_tugas_akhir')");
		}else{
			UpdateData("update pembimbing_ta set status_dosen = 0 where id_mhs = '$id_mhs' and id_jenis_pembimbing='$id_jenis' and id_tugas_akhir='$id_tugas_akhir'");
			InsertData("insert into pembimbing_ta (id_mhs,id_semester,id_dosen,id_jenis_pembimbing,status_dosen,id_tugas_akhir)
						values ('$id_mhs', '$smt', '$id_dosen', '$id_jenis', '1', '$id_tugas_akhir')");
		}

		$jenis=getData("select trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna, jenis_pembimbing.*, pembimbing_ta.*
						from jenis_pembimbing 
						left join pembimbing_ta on pembimbing_ta.id_jenis_pembimbing = jenis_pembimbing.id_jenis_pembimbing and pembimbing_ta.id_mhs = $id_mhs and pembimbing_ta.status_dosen = 1
						left join tugas_akhir on tugas_akhir.id_tugas_akhir = pembimbing_ta.id_tugas_akhir and tugas_akhir.id_tipe_ta = $id_tipe_ta and tugas_akhir.status = 1
						left join dosen on dosen.id_dosen = pembimbing_ta.id_dosen
						left join pengguna on pengguna.id_pengguna = dosen.id_pengguna
						order by jenis_pembimbing.id_jenis_pembimbing");						
		$smarty->assign('jenis',$jenis);

		$ta=getData("select m.id_mhs,m.nim_mhs,p.nm_pengguna as mhs,
						tugas_akhir.id_tugas_akhir,judul_tugas_akhir,deskripsi_tugas_akhir
						from mahasiswa m
						join pengguna p on m.id_pengguna=p.id_pengguna 
						left join tugas_akhir on m.id_mhs=tugas_akhir.id_mhs and tugas_akhir.status=1 
						where m.id_mhs=$id_mhs and tugas_akhir.id_tipe_ta = 4 and tugas_akhir.id_tugas_akhir = '$id_tugas_akhir'");
						  
		$smarty->assign('ta',$ta);
} 

// Added by Yudi Sulistya on Dec 28, 2011
if ($_GET['action']=='hapusdosen'){

		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');

		$id_mhs=$_GET['id_mhs'];
		$id_dosen=$_GET['id_dosen'];
		$id_jenis=$_GET['jenis'];
		$id_pembimbing_ta=$_GET['pembimbing'];
		
		//hapus dosen pengampu
		//UpdateData("update pembimbing_ta set status_dosen = 0 where id_mhs = '$id_mhs' and id_jenis_pembimbing='$id_jenis'");
		UpdateData("update pembimbing_ta set status_dosen = 0 where id_pembimbing_ta = '$id_pembimbing_ta'");
		
		$ta=getData("select m.id_mhs,m.nim_mhs,p.nm_pengguna as mhs,
						tugas_akhir.id_tugas_akhir,judul_tugas_akhir,deskripsi_tugas_akhir
						from mahasiswa m
						join pengguna p on m.id_pengguna=p.id_pengguna 
						left join tugas_akhir on m.id_mhs=tugas_akhir.id_mhs and tugas_akhir.status=1 
						where m.id_mhs=$id_mhs and tugas_akhir.id_tipe_ta = 4 and tugas_akhir.id_tugas_akhir = '$id_tugas_akhir'");
						  
		$smarty->assign('ta',$ta);


		$jenis=getData("select trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna, jenis_pembimbing.*, pembimbing_ta.*
						from jenis_pembimbing 
						left join pembimbing_ta on pembimbing_ta.id_jenis_pembimbing = jenis_pembimbing.id_jenis_pembimbing and pembimbing_ta.id_mhs = $id_mhs and pembimbing_ta.status_dosen = 1
						left join tugas_akhir on tugas_akhir.id_tugas_akhir = pembimbing_ta.id_tugas_akhir and tugas_akhir.id_tipe_ta = $id_tipe_ta and tugas_akhir.status = 1
						left join dosen on dosen.id_dosen = pembimbing_ta.id_dosen
						left join pengguna on pengguna.id_pengguna = dosen.id_pengguna
						order by jenis_pembimbing.id_jenis_pembimbing");
						  
		$smarty->assign('jenis',$jenis);

}	


if ($_GET['action']=='simpanjudul'){

		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');

		$id_mhs=$_GET['id_mhs'];
		$judul=$_POST['judul'];
		$des=$_POST['des'];
		$tipe_ta=$_POST['tipe_ta'];
		$id_tugas_akhir=$_POST['ta'];
		
		$cek=getvar("select count(*) as cek from tugas_akhir where id_mhs = '$id_mhs' and id_tipe_ta = 4");
		if($cek['CEK'] == 0){
			InsertData("insert into tugas_akhir (judul_tugas_akhir,deskripsi_tugas_akhir,id_semester,id_tipe_ta,id_mhs,status)
						values ('$judul', '$des', '$smt', '$tipe_ta', '$id_mhs', '1')");
		}elseif($judul != '' && $id_tugas_akhir != ''){
			UpdateData("update tugas_akhir set status = 0 where id_tugas_akhir = '$id_tugas_akhir'");
			UpdateData("update pembimbing_ta set status_dosen = 0 where id_tugas_akhir = '$id_tugas_akhir'");
			InsertData("insert into tugas_akhir (judul_tugas_akhir,deskripsi_tugas_akhir,id_semester,id_tipe_ta,id_mhs,status)
						values ('$judul', '$des', '$smt', '$tipe_ta', '$id_mhs', '1')");
		}

		$ta=getData("select m.id_mhs,m.nim_mhs,p.nm_pengguna as mhs,
						tugas_akhir.id_tugas_akhir,judul_tugas_akhir,deskripsi_tugas_akhir
						from mahasiswa m
						join pengguna p on m.id_pengguna=p.id_pengguna 
						left join tugas_akhir on m.id_mhs=tugas_akhir.id_mhs and tugas_akhir.status=1 
						where m.id_mhs=$id_mhs and tugas_akhir.id_tipe_ta = 4 and tugas_akhir.id_tugas_akhir = '$id_tugas_akhir'");
						  
		$smarty->assign('ta',$ta);


		$jenis=getData("select trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna, jenis_pembimbing.*, pembimbing_ta.*
						from jenis_pembimbing 
						left join pembimbing_ta on pembimbing_ta.id_jenis_pembimbing = jenis_pembimbing.id_jenis_pembimbing and pembimbing_ta.id_mhs = $id_mhs and pembimbing_ta.status_dosen = 1
						left join tugas_akhir on tugas_akhir.id_tugas_akhir = pembimbing_ta.id_tugas_akhir and tugas_akhir.id_tipe_ta = $id_tipe_ta and tugas_akhir.status = 1
						left join dosen on dosen.id_dosen = pembimbing_ta.id_dosen
						left join pengguna on pengguna.id_pengguna = dosen.id_pengguna
						order by jenis_pembimbing.id_jenis_pembimbing");
						  
		$smarty->assign('jenis',$jenis);

}

$mhs_skripsi=getData("select id_mhs, nim_mhs, mhs, judul_tugas_akhir, id_tipe_ta, nm_mata_kuliah,
max(DECODE(id_jenis_pembimbing, 1, dosen, NULL)) as Pembimbing_1,
max(DECODE(id_jenis_pembimbing, 2, dosen, NULL)) as Pembimbing_2,
max(DECODE(id_jenis_pembimbing, 3, dosen, NULL)) as Pembimbing_3,
max(DECODE(id_jenis_pembimbing, 4, dosen, NULL)) as Pembimbing_4,
max(DECODE(id_jenis_pembimbing, 5, dosen, NULL)) as Pembimbing_5,
max(DECODE(id_jenis_pembimbing, 1, nm_jenis_pembimbing, NULL)) as jenis_Pembimbing_1,
max(DECODE(id_jenis_pembimbing, 2, nm_jenis_pembimbing, NULL)) as jenis_Pembimbing_2,
max(DECODE(id_jenis_pembimbing, 3, nm_jenis_pembimbing, NULL)) as jenis_Pembimbing_3,
max(DECODE(id_jenis_pembimbing, 4, nm_jenis_pembimbing, NULL)) as jenis_Pembimbing_4,
max(DECODE(id_jenis_pembimbing, 5, nm_jenis_pembimbing, NULL)) as jenis_Pembimbing_5
from (
select jenis_pembimbing.id_jenis_pembimbing, pengambilan_mk.id_mhs, nim_mhs, p1.nm_pengguna as mhs, tugas_akhir.judul_tugas_akhir, tugas_akhir.id_tipe_ta,
pg.gelar_depan || ' ' || pg.nm_pengguna || ', ' || pg.gelar_belakang as dosen,nm_jenis_pembimbing, replace(mata_kuliah.nm_mata_kuliah, '&', 'dan') as nm_mata_kuliah
from pengambilan_mk
join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
join pengguna p1 on mahasiswa.id_pengguna=p1.id_pengguna
join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk
join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join tugas_akhir on pengambilan_mk.id_mhs=tugas_akhir.id_mhs and tugas_akhir.status=1 and tugas_akhir.id_tipe_ta = 4
left join pembimbing_ta on tugas_akhir.id_tugas_akhir=pembimbing_ta.id_tugas_akhir and pembimbing_ta.status_dosen=1
left join jenis_pembimbing on jenis_pembimbing.id_jenis_pembimbing = pembimbing_ta.id_jenis_pembimbing
left join dosen on pembimbing_ta.id_dosen=dosen.id_dosen
left join pengguna pg on dosen.id_pengguna=pg.id_pengguna 
where pengambilan_mk.id_semester=$smt and status_mkta=3 and mahasiswa.id_program_studi=$kdprodi
)
group by id_mhs, nim_mhs, mhs, judul_tugas_akhir, id_tipe_ta, nm_mata_kuliah
order by nim_mhs");

$smarty->assign('mhs_skripsi',$mhs_skripsi);

if ($kdfak == 11) {
	$smarty->display('magang-pkl-psikologi.tpl');
} else {
	$smarty->display('magang-pkl.tpl');
}
?>