<?php
/*
Yudi Sulistya 14/09/2012
*/

error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');


$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$kdprodi=$user->ID_PROGRAM_STUDI;
$kdfak=$user->ID_FAKULTAS;
$kddep=getvar("select id_departemen from program_studi where id_program_studi=$kdprodi");



$default=getvar("select angkatan from
(select distinct thn_angkatan_mhs as angkatan from mahasiswa
where id_program_studi=$kdprodi and status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
order by thn_angkatan_mhs)
where rownum=1");



$kur=getData("select id_kurikulum_prodi, nm_kurikulum||' - '||thn_kurikulum as kurikulum
from kurikulum
left join kurikulum_prodi on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
where kurikulum_prodi.id_program_studi=$kdprodi
and id_kurikulum_prodi in (select distinct id_kurikulum_prodi from kurikulum_mk where id_program_studi=$kdprodi) order by kurikulum_prodi.tahun_kurikulum desc");
$smarty->assign('T_KUR', $kur);

$angkatan=getData("select distinct thn_angkatan_mhs as angkatan from mahasiswa
where id_program_studi=$kdprodi and status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
order by thn_angkatan_mhs");
$smarty->assign('ANG', $angkatan);
if(empty($default['ANGKATAN'])) $default['ANGKATAN'] = 1945; ## UNTUK MENGHASILKAN QUERY NO RESULT ~ RIESKHA 
$agk = isset($_GET['angk']) ? $_GET['angk'] : $default['ANGKATAN'];


$jaf2=getData("select kurikulum,count(id_mhs) as ttl from 
(select nm_kurikulum||' - '||thn_kurikulum as kurikulum,id_kurikulum_prodi
from kurikulum
left join kurikulum_prodi on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
where kurikulum_prodi.id_program_studi=$kdprodi)s1
left join 
(select id_mhs,id_kurikulum_prodi from mahasiswa
where mahasiswa.id_program_studi=$kdprodi
and status_akademik_mhs in (select id_status_pengguna from status_pengguna 
where id_role=3 and status_aktif=1))s2 on s1.id_kurikulum_prodi=s2.id_kurikulum_prodi 
group by kurikulum
order by kurikulum
");
$smarty->assign('SEBKUR',$jaf2);


$jaf1=getData("select id_mhs,nim_mhs,upper(nm_pengguna) as nm_pengguna,'0' as tanda from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
where id_program_studi=$kdprodi and (id_kurikulum_prodi is null or id_kurikulum_prodi = '')
and status_akademik_mhs in
(select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
order by nim_mhs
");
$smarty->assign('NO_KUR', $jaf1);

$status = isset($_REQUEST['action'])? $_REQUEST['action'] : 'tampil';



switch($status) {
case 'add':

$smarty->assign('disp1','none');
$smarty->assign('disp2','block');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$id_kur=$_POST['id_kur'];
$counter=$_POST['counter1'];

for ($i=1; $i<=$counter; $i++)
{
	if ($_POST['mhs'.$i] <> '' || $_POST['mhs'.$i] <> null) {
		if(isset($id_kur) && !empty($id_kur))UpdateData("update mahasiswa set id_kurikulum_prodi=$id_kur where id_mhs='".$_POST['mhs'.$i]."'");
	}
}

$jaf1=getData("select id_mhs,nim_mhs,upper(nm_pengguna) as nm_pengguna,'0' as tanda from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
where id_program_studi=$kdprodi and (id_kurikulum_prodi is null or id_kurikulum_prodi = '')
and status_akademik_mhs in
(select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
order by nim_mhs
");
$smarty->assign('NO_KUR', $jaf1);

break;

case 'update':

#echo empty($id_kur); exit();
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');

$id_mhs = $_POST['id_mhs'];
$id_kur = $_POST['id_kur'];

UpdateData("update mahasiswa set id_kurikulum_prodi=$id_kur where id_mhs=$id_mhs");

$jaf=getData("select mahasiswa.id_mhs,nim_mhs,upper(pengguna.nm_pengguna) as mhs,nm_kurikulum||' - '||thn_kurikulum as kurikulum from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join kurikulum_prodi on kurikulum_prodi.id_kurikulum_prodi=mahasiswa.id_kurikulum_prodi
left join kurikulum on kurikulum.id_kurikulum=kurikulum_prodi.id_kurikulum
where mahasiswa.id_program_studi=$kdprodi and
mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
and mahasiswa.thn_angkatan_mhs=$agk");
$smarty->assign('KUR1', $jaf);

break;

case 'viewupdate':
$smarty->assign('disp1','none');
$smarty->assign('disp2','none');
$smarty->assign('disp3','block');
$smarty->assign('disp4','none');
$id_mhs= $_GET['id_mhs'];

$jaf=getData("select mahasiswa.id_mhs,nim_mhs,upper(pengguna.nm_pengguna) as mhs,nm_kurikulum||' - '||thn_kurikulum as kurikulum from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join kurikulum_prodi on kurikulum_prodi.id_kurikulum_prodi=mahasiswa.id_kurikulum_prodi
left join kurikulum on kurikulum.id_kurikulum=kurikulum_prodi.id_kurikulum
where mahasiswa.id_program_studi=$kdprodi and
mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
and mahasiswa.thn_angkatan_mhs='{$agk}' order by nm_pengguna");
$smarty->assign('KUR', $jaf);

break;

case 'tampil':
$jaf=getData("select mahasiswa.id_mhs,nim_mhs,upper(pengguna.nm_pengguna) as mhs,nm_kurikulum||' - '||thn_kurikulum as kurikulum from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join kurikulum_prodi on kurikulum_prodi.id_kurikulum_prodi=mahasiswa.id_kurikulum_prodi
left join kurikulum on kurikulum.id_kurikulum=kurikulum_prodi.id_kurikulum
where mahasiswa.id_program_studi=$kdprodi and mahasiswa.thn_angkatan_mhs='{$agk}' and
mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
and mahasiswa.thn_angkatan_mhs='{$agk}' order by nm_pengguna");
$smarty->assign('KUR1', $jaf);

break;

}

$jaf=getData("select mahasiswa.id_mhs,nim_mhs,upper(pengguna.nm_pengguna) as mhs,nm_kurikulum||' - '||thn_kurikulum as kurikulum from mahasiswa
left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
left join kurikulum_prodi on kurikulum_prodi.id_kurikulum_prodi=mahasiswa.id_kurikulum_prodi
left join kurikulum on kurikulum.id_kurikulum=kurikulum_prodi.id_kurikulum
where mahasiswa.id_program_studi='{$kdprodi}' and mahasiswa.thn_angkatan_mhs='{$agk}' and
mahasiswa.status_akademik_mhs in (select id_status_pengguna from status_pengguna where id_role=3 and status_aktif=1)
and mahasiswa.thn_angkatan_mhs='{$agk}'");
$smarty->assign('KUR1', $jaf);
$smarty->display('seting_kur_mhs.tpl');
?>
