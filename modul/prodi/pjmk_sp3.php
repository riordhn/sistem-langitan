<?php
/*
update by Yudi Sulistya on 29/12/2011
*/

error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');
$smarty->assign('disp4','none');
$smarty->assign('disp5','none');

$kdprodi= $user->ID_PROGRAM_STUDI; 

$kdfak=$user->ID_FAKULTAS;

$smtaktif=getvar("select * from semester where status_aktif_semester='True'");
//$smarty->assign('smtaktif',$smtaktif['ID_SEMESTER']);

$smt1 = isSet($_POST['thn_ajaran']) ? $_POST['thn_ajaran'] : $smtaktif['ID_SEMESTER'];
$smarty->assign('smtaktif',$smt1);
$smt=getData("select id_semester, tahun_ajaran||' - '||nm_semester as smt from semester order by thn_akademik_semester desc,nm_semester desc");
$smarty->assign('T_ST', $smt);

if ($_GET['action']=='searchdosen'){

		$id_mk=$_GET['id_klsmk'];
		$pjmk=$_GET['pjmk'];
		$id_pengampu=$_GET['id_pengampu'];
		$status1=$_GET['status1'];
			
		if ($status1=='ganti') {
		$namacari=$_POST['namadosen1'];
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','block');
		$smarty->assign('disp5','none');
		$smarty->assign('STATUS1',$status1);
		} else {
		$namacari=$_POST['namadosen'];
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');
		$smarty->assign('disp4','none');}
		$smarty->assign('disp5','none');
		
		if ($namacari !='') {
		// Added by Yudi Sulistya on Dec 02, 2011
		$upper = strtoupper($namacari);
		$lower = strtolower($namacari);
		$proper = ucwords($namacari);
		$hasil=getData("select id_dosen,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna, foto_pengguna , upper(nm_program_studi) as nm_program_studi
		from dosen 
		left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
		left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
		where (nm_pengguna like '%$upper%' or nm_pengguna like '%$lower%' or nm_pengguna like '%$proper%') and dosen.id_status_pengguna=22
		order by trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang)");}
		$smarty->assign('DOSEN',$hasil);
		$smarty->assign('IDKEL',$id_mk);
		$smarty->assign('ST_PJMK',$pjmk);
		$smarty->assign('ID_PENGAMPU',$id_pengampu);
		

}
if ($_GET['action']=='addview'){

		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','none');

		$id_mk=$_GET['id_klsmk'];
		$smarty->assign('IDKEL',$id_mk);
		$st=$_GET['st'];
		$pjma=$_GET['pjma'];
		$smarty->assign('PJ',$pjma);
		
		//insert dosen pengampu
		$pil=$_GET['pil'];
		//$kelasmk=$_GET['kelasmk'];
		$id_dosen=$_GET['id_dosen'];
		
		if ($pil==1) {
			if (!isFindData("pengampu_mk","pjmk_pengampu_mk=1 and id_kelas_mk=$id_mk")) {
			tambahdata("pengampu_mk","id_kelas_mk,id_dosen,pjmk_pengampu_mk","$id_mk,$id_dosen,1");
			$st=1;
			} 
			}
		if ($pil==2) {
			if (!isFindData("pengampu_mk","pjmk_pengampu_mk=0 and id_kelas_mk=$id_mk and id_dosen=$id_dosen")) {
			tambahdata("pengampu_mk","id_kelas_mk,id_dosen,pjmk_pengampu_mk","$id_mk,$id_dosen,0");
			$st=1;
			} 
			}
		//update dosen pengampu
		$id_pengampu=$_GET['id_pengampu'];
		$status1=$_GET['status1'];
		if ($status1=='ganti'){
			UpdateData("update pengampu_mk set id_dosen=$id_dosen where id_pengampu_mk=$id_pengampu");
			$st=1;
			}	
		
				
		$jaf=getData("select distinct kelas_mk.id_kelas_mk,KD_MATA_KULIAH,nm_mata_kuliah,kurikulum_mk.KREDIT_SEMESTER,nama_kelas,kelas_mk.id_kurikulum_mk
from kelas_mk 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join semester on kelas_mk.id_semester=semester.id_semester 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_program_studi in (select id_program_studi from program_studi where id_fakultas=$kdfak) and kelas_mk.id_kelas_mk=$id_mk");
		$smarty->assign('T_PJMK', $jaf);
		
		$pengampu=getData("select pengampu_mk.id_dosen,pjmk_pengampu_mk,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna,id_pengampu_mk from pengampu_mk
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where id_kelas_mk=$id_mk order by pjmk_pengampu_mk desc ");
		$smarty->assign('PGP', $pengampu);
		$smarty->assign('ST', $st);
		
		//$smarty->display('usulan-mk.tpl');
} 
// Added by Yudi Sulistya on Dec 28, 2011
if ($_GET['action']=='hapusdosen'){

		 // pilih
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','none');

		$id_mk=$_GET['id_klsmk'];
		$smarty->assign('IDKEL',$id_mk);
		$st=$_GET['st'];
		
		//hapus dosen pengampu
		$id_pengampu=$_GET['id_pengampu'];
		$status2=$_GET['status2'];
		if ($status2=='hapus'){
			UpdateData("delete from pengampu_mk where id_pengampu_mk=$id_pengampu");
			$st=1;
			}	
		
		echo '<script> $("#supro_3-pjmasp3").load("pjmk_sp3.php"); </script>';
}			
if ($_GET['action']=='del'){	

		 // pilih
		$id_klsmk= $_GET['id_klsmk'];
				
		deleteData("delete from pengampu_mk where id_kelas_mk='$id_klsmk'");
	
		//if ($smt!=''){
		$jaf=getData("select kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas,kelas_mk.id_kurikulum_mk,
case when wm_concat(pengguna.nm_pengguna) is not null then '1' else '0' end as id_status,
trim('<li> '||wm_concat(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||' '||(case when pengampu_mk.pjmk_pengampu_mk=1 then '(PJMA)' when pengampu_mk.pjmk_pengampu_mk=0 then '(ANGGOTA)' else '' end))) as tim,
case when sum(pengampu_mk.pjmk_pengampu_mk)=1 then 'ada' else 'kosong' end as ada,kurikulum_mk.tingkat_semester as smt
from kelas_mk 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_semester=$smt1 
and kurikulum_mk.status_paket='sp3'
group by kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, 
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, kelas_mk.id_kurikulum_mk");
		$smarty->assign('PJMK', $jaf);
		//$smarty->display('usulan-mk.tpl');

}
		
		

		//echo "aa";
		//echo $smtaktif['ID_SEMESTER'];
		$smt1 = isSet($_POST['thn_ajaran']) ? $_POST['thn_ajaran'] : $smtaktif['ID_SEMESTER'];
		//$smt1= $_POST['smt'];
		// if ($smt1=='') { $smt1=$smtaktif['ID_SEMESTER'];}
		//echo $smt1;
		
		if ($smt1!='') {
		$smarty->assign('SMT',$smt1);
		//$smarty->assign('KUR', $id_kur);
		
		$jaf=getData("select kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas,kelas_mk.id_kurikulum_mk,
case when wm_concat(pengguna.nm_pengguna) is not null then '1' else '0' end as id_status,
trim('<li> '||wm_concat(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||' '||(case when pengampu_mk.pjmk_pengampu_mk=1 then '(PJMA)' when pengampu_mk.pjmk_pengampu_mk=0 then '(ANGGOTA)' else '' end))) as tim,
case when sum(pengampu_mk.pjmk_pengampu_mk)=1 then 'ada' else 'kosong' end as ada,kurikulum_mk.tingkat_semester as smt
from kelas_mk 
left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where kelas_mk.id_program_studi=$kdprodi and kelas_mk.id_semester=$smt1 
and kurikulum_mk.status_paket='sp3'
group by kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah, 
kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, kelas_mk.id_kurikulum_mk,kurikulum_mk.tingkat_semester
");


}

		$smarty->assign('PJMK', $jaf);
		/*
		$pengampu=getData("select case when pjmk_pengampu_mk=1 then nm_pengguna||'(PJMA)' else nm_pengguna||'(Anggota)' end as dosen_pgp
from pengampu_mk
left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
where id_kelas_mk=$id_mk");
		$smarty->assign('PGP1', $pengampu);
		*/
if ($_GET['action']=='copy'){	

		 // pilih
		$id_klsmk= $_GET['id_klsmk'];
		$id_kurmk= $_GET['id_kurmk'];
		
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','none');
		$smarty->assign('disp4','none');
		$smarty->assign('disp5','block');
		
		$smarty->assign('id_klsmk1',$id_klsmk);
		
		$getkur=getData("select kelas_mk.id_kelas_mk,kelas_mk.id_kelas_mk,kd_mata_kuliah||'-'||nm_mata_kuliah||'-'||nama_kelas as nama from kelas_mk
						left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
						where kelas_mk.id_kurikulum_mk=$id_kurmk and kelas_mk.id_kelas_mk!=$id_klsmk");
		
		$smarty->assign('T_CP', $getkur);
		//$smarty->display('usulan-mk.tpl');
}
if ($_POST['action']=='prosescp'){	

		//echo "copy";
		$kelasto=$_POST['kelas_mk'];
		$kelasas=$_POST['cp'];
		
		//echo $kelasto;
		//echo "to";
		//echo $kelasas;
		//proses copy
		InsertData("insert into pengampu_mk(id_kelas_mk,id_dosen,pjmk_pengampu_mk) select '".$kelasto."',id_dosen,pjmk_pengampu_mk from pengampu_mk where id_kelas_mk=$kelasas");
		//echo "insert into pengampu_mk(id_kelas_mk,id_dosen,pjmk_pengampu_mk) select '".$kelasto."',id_dosen,pjmk_pengampu_mk from kelas_mk where id_kelas_mk=$kelasas";			
		
		//$smarty->display('usulan-mk.tpl');

}

$smarty->display('pjmk_sp3.tpl');

?>
