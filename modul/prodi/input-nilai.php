<?php
// ====== FITUR INPUT NILAI OLEH STAFF ======
// ================== FIKRIE ==================
// =============== 23-07-2018 =================

include 'config.php';
include 'proses/komponen_nilai.class.php';
include 'proses/penilaian.class.php';

$mode = get('mode', 'view');

$kd_fak= $user->ID_FAKULTAS;
$kd_prodi= $user->ID_PROGRAM_STUDI;
$id_pengguna= $user->ID_PENGGUNA; 

$id_pt = $id_pt_user;

$komponen_nilai = new komponen_nilai($db);

$id_semester_aktif = $db->QuerySingle("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI = {$id_pt}");

$id_semester_sp_aktif = $db->QuerySingle("SELECT id_semester FROM semester WHERE status_aktif_sp='True' AND id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}");

if ($request_method == 'POST') {
	if(post('mode') == 'add-komponen'){
		$nama = post('nama') != '' ? post('nama') : post('nama_utama');
        $komponen_nilai->insert_komponen($nama, post('persentase'), post('urutan'), post('id_kelas_mk'));
        $smarty->assign('id_kelas_mk', post('id_kelas_mk'));
        $smarty->assign('id_dosen', post('id_dosen'));
	} 
	else if (post('mode') == 'edit-komponen') {
        $komponen_nilai->update_komponen(post('id_komponen'), post('urutan'), post('nama'), post('persentase'));
        $smarty->assign('id_kelas_mk', post('id_kelas_mk'));
        $smarty->assign('id_dosen', post('id_dosen'));
    } 
    else if (post('mode') == 'delete-komponen') {
        $komponen_nilai->delete_komponen(post('id_komponen'));
        $smarty->assign('id_kelas_mk', post('id_kelas_mk'));
        $smarty->assign('id_dosen', post('id_dosen'));
    } 

}

if ($_SERVER['REQUEST_METHOD'] == 'GET' || $_SERVER['REQUEST_METHOD'] == 'POST')
{
	$smarty->assign('id_pengguna', $id_pengguna);

	// khusus DSI UMAHA
	$id_unit_kerja_sd = $db->QuerySingle("SELECT ID_UNIT_KERJA_SD 
											FROM PEGAWAI 
											JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = PEGAWAI.ID_PENGGUNA
											WHERE PEGAWAI.ID_PENGGUNA = {$id_pengguna}  AND PENGGUNA.ID_PERGURUAN_TINGGI = 1");
	$smarty->assign('id_unit_kerja_sd', $id_unit_kerja_sd);

	$semester_set = $db->QueryToArray(
			"SELECT * FROM SEMESTER WHERE ID_PERGURUAN_TINGGI = '{$id_pt}' 
				AND THN_AKADEMIK_SEMESTER > ((SELECT EXTRACT(YEAR FROM SYSDATE) FROM DUAL) - 6) AND TIPE_SEMESTER IN ('REG','SP')
							ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
		
	$smarty->assign('semester_set', $semester_set);	

	$panjang_semester = count($semester_set);
	$i = 0;
	$semester_id = "";
	foreach ($semester_set as $semester) {
		if($i == $panjang_semester - 1){
			$semester_id .= $semester['ID_SEMESTER'];
		}
		else{
			$semester_id .= $semester['ID_SEMESTER'].",";
		}
		$i++;
	}

	if ($id_unit_kerja_sd == 30){

		$dosen_set = $db->QueryToArray(
				"SELECT DISTINCT pmk.ID_DOSEN, d.NIP_DOSEN, p.GELAR_DEPAN || '' || p.NM_PENGGUNA || ', ' || p.GELAR_BELAKANG AS NM_PENGGUNA
					FROM PENGAMPU_MK pmk
					JOIN DOSEN d ON d.ID_DOSEN = pmk.ID_DOSEN
					JOIN PENGGUNA p ON p.ID_PENGGUNA = d.ID_PENGGUNA
					JOIN KELAS_MK kmk ON kmk.ID_KELAS_MK = pmk.ID_KELAS_MK
					WHERE kmk.ID_PROGRAM_STUDI = '{$kd_prodi}' AND kmk.ID_SEMESTER IN ({$semester_id})
					ORDER BY NM_PENGGUNA");
	}
	else{
		$dosen_set = $db->QueryToArray(
				"SELECT DISTINCT pmk.ID_DOSEN, d.NIP_DOSEN, p.GELAR_DEPAN || '' || p.NM_PENGGUNA || ', ' || p.GELAR_BELAKANG AS NM_PENGGUNA
					FROM PENGAMPU_MK pmk
					JOIN DOSEN d ON d.ID_DOSEN = pmk.ID_DOSEN
					JOIN PENGGUNA p ON p.ID_PENGGUNA = d.ID_PENGGUNA
					JOIN KELAS_MK kmk ON kmk.ID_KELAS_MK = pmk.ID_KELAS_MK
					WHERE kmk.ID_PROGRAM_STUDI = '{$kd_prodi}' AND kmk.ID_SEMESTER IN ('{$id_semester_aktif}','{$id_semester_sp_aktif}')
					ORDER BY NM_PENGGUNA");
	}
		
	$smarty->assign('dosen_set', $dosen_set);

	$semester_aktif_set = $db->QueryToArray(
			"SELECT * FROM SEMESTER WHERE (STATUS_AKTIF_SEMESTER = 'True' OR STATUS_AKTIF_SP = 'True') AND ID_PERGURUAN_TINGGI = '{$id_pt}'");
		
	$smarty->assign('semester_aktif_set', $semester_aktif_set);

	if ($mode == 'view')
	{
		$id_dosen = get('id_dosen');
		$id_semester = get('id_semester');

		if(!empty($id_dosen)){
			$matkul_set = $db->QueryToArray(
				"SELECT kmk.ID_KELAS_MK, kmk.ID_SEMESTER, mk.KD_MATA_KULIAH, mk.NM_MATA_KULIAH, nk.NAMA_KELAS, pmk.PJMK_PENGAMPU_MK, pmk.ID_DOSEN,
					(SELECT COUNT (*) FROM PENGAMBILAN_MK pmk WHERE pmk.ID_KELAS_MK = kmk.ID_KELAS_MK AND pmk.STATUS_APV_PENGAMBILAN_MK = 1) AS JML_MHS 
					FROM KELAS_MK kmk
					JOIN PENGAMPU_MK pmk ON pmk.ID_KELAS_MK = kmk.ID_KELAS_MK
					JOIN MATA_KULIAH mk ON mk.ID_MATA_KULIAH = kmk.ID_MATA_KULIAH
					JOIN NAMA_KELAS nk ON nk.ID_NAMA_KELAS = kmk.NO_KELAS_MK
					WHERE pmk.ID_DOSEN = '{$id_dosen}' AND kmk.ID_SEMESTER = '{$id_semester}' AND kmk.ID_PROGRAM_STUDI = '{$kd_prodi}'
					ORDER BY mk.NM_MATA_KULIAH, nk.NAMA_KELAS");

			$smarty->assign('matkul_set', $matkul_set);		
		}
		
	}

	if ($mode == 'pilih-menu' or $mode == 'komponen' or $mode == 'add-komponen' or $mode == 'edit-komponen' or $mode == 'delete-komponen'){
		$id_dosen = (int)get('id_dosen', 0);
		$id_kelas_mk = (int)get('id_kelas_mk', 0);
		$id_semester = (int)get('id_semester', 0);

		$smarty->assign('id_dosen', $id_dosen);	
		$smarty->assign('id_kelas_mk', $id_kelas_mk);	
		$smarty->assign('id_semester', $id_semester);	

		$persentase = $db->QuerySingle("SELECT NVL(SUM(PERSENTASE_KOMPONEN_MK), 0) AS PERSENTASE 
											FROM KOMPONEN_MK 
											WHERE ID_KELAS_MK = '{$id_kelas_mk}'");
		$smarty->assign('persentase', $persentase);	

		$nm_matkul = $db->QuerySingle("SELECT KD_MATA_KULIAH || ' - ' || NM_MATA_KULIAH AS NM_MATA_KULIAH 
											FROM KELAS_MK kmk
											JOIN MATA_KULIAH mk ON mk.ID_MATA_KULIAH = kmk.ID_MATA_KULIAH 
											WHERE kmk.ID_KELAS_MK = '{$id_kelas_mk}' AND kmk.ID_SEMESTER = '{$id_semester}'");
		$smarty->assign('nm_matkul', $nm_matkul);	

		$pengampu = $db->QuerySingle("SELECT pmk.PJMK_PENGAMPU_MK 
											FROM KELAS_MK kmk
											JOIN PENGAMPU_MK pmk ON pmk.ID_KELAS_MK = kmk.ID_KELAS_MK
											WHERE kmk.ID_KELAS_MK = '{$id_kelas_mk}' AND pmk.ID_DOSEN = '{$id_dosen}'");
		$smarty->assign('pengampu', $pengampu);	

		// kebutuhan mode komponen
		$komponen_set = $db->QueryToArray(
			"SELECT * FROM KOMPONEN_MK 
				WHERE ID_KELAS_MK = '{$id_kelas_mk}' ORDER BY URUTAN_KOMPONEN_MK");
		$smarty->assign('komponen_set', $komponen_set);

		// kebutuhan mode edit dan delete komponen
		$smarty->assign('id_komponen', get('id_komponen'));
		$smarty->assign('data_komponen_mk_by_id', $komponen_nilai->load_komponen_mk_by_id(get('id_komponen')));
	}

	if($mode == 'input'){
		$id_dosen = (int)get('id_dosen', 0);
		$id_kelas_mk = (int)get('id_kelas_mk', 0);

		$smarty->assign('id_dosen', $id_dosen);	

		$penilaian = new penilaian($db, $id_dosen);

        $detail_mk = $penilaian->get_kelas_mk($id_kelas_mk);
        // Cek User Dosen Sebagai Pengampu selain itu di block
        if ($penilaian->cek_penilaian_dosen_login($id_kelas_mk) == 0) {
            echo '<div class="center_title_bar">Input Nilai</div>';
            die(alert_error("Access denied.", 90));
        }
        // Cek Edit KRS Akademik dan Benahi Tampilan Nilai
        $penilaian->benahi_tampilan_nilai_krs_akad($id_kelas_mk, $detail_mk['ID_SEMESTER']);

        if (isset($_POST)) {
            if (post('mode') == 'save_nilai') {

                if ($id_dosen != '') {
                    for ($i = 1; $i < post('total_nilai'); $i++) {
                        $penilaian->update_nilai(post('nilai' . $i), post('id_nilai' . $i));
                    }
                    $penilaian->proses_nilai_akhir($id_kelas_mk);

                    // update ips,ipk,dll
                    $penilaian->proses_status_mhs($id_kelas_mk);
                } else {
                    die('UPDATE GAGAL RELOAD KEMBALI HALAMAN INI');
                }
            } else if (post('mode') == 'tambah_akses') {
                $penilaian->tambah_akses_komponen(post('id_dosen'), $id_kelas_mk, post('id_komponen_mk'), post('id_semester'));
            } else if (post('mode') == 'delete_akses') {
                $penilaian->delete_akses_komponen(post('id_akses'));
            } else if (post('mode') == 'tampilkan_permhs') {
                $id = post('id_pengambilan');
                $db->Query("UPDATE PENGAMBILAN_MK SET FLAGNILAI=1 WHERE ID_PENGAMBILAN_MK='{$id}'");
            } else if (post('mode') == 'tidak_tampilkan_permhs') {
                $id = post('id_pengambilan');
                $db->Query("UPDATE PENGAMBILAN_MK SET FLAGNILAI=0 WHERE ID_PENGAMBILAN_MK='{$id}'");
            } else if (post('mode') == 'tampilkan') {
                if (post('status_tampil') == 0) {
                    $penilaian->tampilkan_nilai_mhs($id_kelas_mk, $detail_mk['ID_SEMESTER']);
                } else {
                    $penilaian->reset_tampilkan_nilai_mhs($id_kelas_mk, $detail_mk['ID_SEMESTER']);
                }
            }
        }
        $smarty->assign('data_mahasiswa', $penilaian->load_data_mahasiswa_reg_sp($id_kelas_mk));
        $smarty->assign('data_pengajar_mk', $penilaian->load_data_pengajar_kelas_mk($id_kelas_mk));
        $smarty->assign('data_kelas_mk_detail', $detail_mk);
        $smarty->assign('id_fakultas_kelas', $penilaian->get_id_fakultas_kelas_mk($id_kelas_mk));
        $smarty->assign('pjmk_status', $penilaian->get_pjmk_status($id_kelas_mk));
        $smarty->assign('data_komponen_mk', $penilaian->load_komponen_mk($id_kelas_mk));
        $smarty->assign('count_data_komponen_mk', count($penilaian->load_komponen_mk($id_kelas_mk)));
        $smarty->assign('id_kelas_mk', $id_kelas_mk);
        $smarty->assign('data_dosen_mk', $penilaian->load_dosen_mk($id_kelas_mk));
        $smarty->assign('data_akses_dosen_mk', $penilaian->load_akses_komponen($id_kelas_mk));
        $smarty->assign('status_tampil', $penilaian->status_tampilkan_nilai_mhs($id_kelas_mk, $detail_mk['ID_SEMESTER']));
	}

}

$smarty->display("input-nilai/{$mode}.tpl");