<div class="center_title_bar">Status KRS per Kelas</div>

{if $mode == 'list'}

<table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 5px">
	<tr>
		<td>
			<form action="monitoring-krs-kelas.php" method="get">
				Semester :
					<select name="id_semester">
					{foreach $semester_set as $semester}
						<option value="{$semester.ID_SEMESTER}" {if $semester.ID_SEMESTER == $id_semester_terpilih}selected{/if}
						>{$semester.TAHUN_AJARAN} {$semester.NM_SEMESTER}{if $semester.STATUS_AKTIF_SEMESTER == 'True'} (Aktif){/if}</option>
					{/foreach}
				</select>
				<input type="submit" name="View" value="View">
			</form>
		</td>
	</tr>
</table>

<table id="table" class="tablesorter">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode</th>
			<th>Mata Kuliah</th>
			<th>SKS</th>
			<th>Kelas</th>
			<th>KRS</th>
			<th>Disetujui</th>
			<th class="noheader"></th>
		</tr>
	</thead>
	<tbody>
		{foreach $kelas_set as $kelas}
			<tr>
				<td class="center">{$kelas@index + 1}</td>
				<td>{$kelas.KD_MATA_KULIAH}</td>
				<td>{$kelas.NM_MATA_KULIAH}</td>
				<td class="center">{$kelas.SKS}</td>
				<td class="center">{$kelas.NAMA_KELAS}</td>
				<td class="center">{$kelas.KRS}</td>
				<td class="center">{$kelas.KRS_APPROVE}</td>
				<td>
					<a href="monitoring-krs-kelas.php?mode=detail&id_kelas_mk={$kelas.ID_KELAS_MK}&id_semester={$id_semester_terpilih}">Detail</a>
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>

{/if}

{if $mode == 'detail'}

<a href="monitoring-krs-kelas.php?id_semester={$smarty.get.id_semester}">Kembali</a>

<table>
	<tbody>
		<tr>
			<td>Kelas</td>
			<td>{$kelas.NAMA_KELAS}</td>
		</tr>
		<tr>
			<td>Kode Mata Kuliah</td>
			<td>{$kelas.KD_MATA_KULIAH}</td>
		</tr>
		<tr>
			<td>Mata Kuliah</td>
			<td>{$kelas.NM_MATA_KULIAH}</td>
		</tr>
		<tr>
			<td>SKS</td>
			<td>{$kelas.KREDIT_SEMESTER}</td>
		</tr>
	</tbody>
</table>

<table>
	<thead>
		<tr>
			<th>No</th>
			<th>NIM</th>
			<th>Nama</th>
			<th>Dosen Wali</th>
			<th>Status KRS</th>
		</tr>
	</thead>
	<tbody>
		{foreach $data_set as $data}
			<tr>
				<td>{$data@index + 1}</td>
				<td>{$data.NIM_MHS}</td>
				<td>{$data.NM_PENGGUNA}</td>
				<td>{$data.NM_DOSEN_WALI}</td>
				<td class="center">{if $data.STATUS_APV_PENGAMBILAN_MK == 1}Disetujui{else}Belum disetujui{/if}</td>
			</tr>
		{/foreach}
	</tbody>
</table>

{/if}