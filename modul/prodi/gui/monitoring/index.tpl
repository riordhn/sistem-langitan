<div class="center_title_bar">Monitoring Perkuliahan</div>
<table border="0" cellspacing="1" cellpadding="1">
	<tr>
		<td width="178" bgcolor="#FFCC33"><div align="center">Menu</div></td>
		<td width="434" bgcolor="#FFCC33"><div align="center">Deskripsi</div></td>
	</tr>
	<tr>
		<td>Status KRS Mahasiswa</td>
		<td>Untuk mengetahui status KRS mahasiswa yang sudah / yang belum</td>
	</tr>
	<tr>
		<td>Status KRS per Kelas</td>
		<td>Untuk mengetahui status KRS per kelas</td>
	</tr>
	<tr>
		<td>Status Entri Nilai</td>
		<td>Untuk mengetahui Nilai yang dimasukkan oleh tiap dosen  </td>
	</tr>
	<tr>
		<td>Mahasiswa Akhir</td>
		<td>Untuk mengetahui Mahasiswa di semester akhir</td>
	</tr>
</table>