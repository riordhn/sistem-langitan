<div class="center_title_bar">Status KRS Mahasiswa</div>

{if $mode == 'list'}
<table class="tb_frame">
	<tr>
		<td>
			<form action="monitoring-krs.php" method="get">
				Semester :
				<select name="id_semester">
					{foreach $semester_set as $semester}
						<option value="{$semester.ID_SEMESTER}"
						{if $smarty.get.id_semester == ''}
							{if $semester.ID_SEMESTER == $semester_aktif.ID_SEMESTER}selected{/if}
						{else}
							{if $semester.ID_SEMESTER == $smarty.get.id_semester}selected{/if}
						{/if}
						>{$semester.TAHUN_AJARAN} {$semester.NM_SEMESTER}{if $semester.STATUS_AKTIF_SEMESTER == 'True'} (Aktif){/if}</option>
					{/foreach}
				</select>
				<input type="submit" value="View" />
			</form>
		</td>
	</tr>
</table>

<table>
	<thead>
		<tr>
			<th>No</th>
			<th>NIM</th>
			<th>Nama</th>
			<th>Status MHS</th>
			<th>Dosen Wali</th>
			<th>Status KRS</th>
			<th>Mata Kuliah</th>
			<th>Mata Kuliah Disetujui</th>
			<th>SKS</th>
			<th>SKS Disetujui</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach $data_set as $data}
			<tr 
				{* Rule *}
				{if $data.JUMLAH_SKS > 24}
					style="background-color: pink"
				{else if $data.JUMLAH_MK == 0}
					style="background-color: cyan"
				{/if}>
				<td>{$data@index + 1}</td>
				<td>{$data.NIM_MHS}</td>
				<td>{$data.NM_PENGGUNA}</td>
				<td>{strtoupper($data.NM_STATUS_PENGGUNA)}</td>
				<td>{$data.NM_DOSEN_WALI}</td>
				<td class="center">
					{if $data.JUMLAH_MK == 0}
						Belum KRS
					{else if $data.JUMLAH_MK > 0}
						{if $data.JUMLAH_MK_APPROVE == 0}
							Belum Approve
						{else if $data.JUMLAH_MK_APPROVE <= $data.JUMLAH_MK}
							KRS
						{/if}
					{/if}
				</td>
				<td class="center">{$data.JUMLAH_MK}</td>
				<td class="center">{$data.JUMLAH_MK_APPROVE}</td>
				<td class="center">{$data.JUMLAH_SKS}</td>
				<td class="center">{$data.JUMLAH_SKS_APPROVE}</td>
				<td>
					<a href="monitoring-krs.php?mode=detail&id_mhs={$data.ID_MHS}&id_semester={if $smarty.get.id_semester == ''}{$semester_aktif.ID_SEMESTER}{else}{$smarty.get.id_semester}{/if}">Detail</a>
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>
{/if}

{if $mode == 'detail'}

	<a href="monitoring-krs.php?id_semester={$smarty.get.id_semester}">Kembali</a>

	<table>
		<thead>
			<tr>
				<th>No</th>
				<th>Kode</th>
				<th>Mata Kuliah</th>
				<th>SKS</th>
				<th>Kelas</th>
				<th>Status KRS</th>
			</tr>
		</thead>
		<tbody>
			{foreach $data_set as $data}
				<tr>
					<td class="center">{$data@index + 1}</td>
					<td>{$data.KD_MATA_KULIAH}</td>
					<td>{$data.NM_MATA_KULIAH}</td>
					<td class="center">{$data.SKS}</td>
					<td>{$data.NAMA_KELAS}</td>
					<td>{if $data.STATUS_APV_PENGAMBILAN_MK == 1}Disetujui{else}Belum disetujui{/if}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>

{/if}