<div class="center_title_bar">Status Nilai Kosong Mahasiswa</div>

{if $mode == 'list'}

<table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 5px">
	<tr>
		<td>
			<form action="monitoring-mhs-nilai-kosong.php" method="get">
				Angkatan Mahasiswa :
					<select name="thn_angkatan_mhs">
					<option value="" >-- Pilih Angkatan --</option>
					{foreach $angkatan_mhs_set as $angkatan}
						<option value="{$angkatan.THN_ANGKATAN_MHS}" {if $angkatan.THN_ANGKATAN_MHS == $thn_angkatan_mhs}selected{/if}
						>{$angkatan.THN_ANGKATAN_MHS}</option>
					{/foreach}
				</select>
				<input type="submit" name="View" value="View">
			</form>
		</td>
	</tr>
</table>

<table>
	<thead>
		<tr>
			<th>No</th>
			<th>NIM</th>
			<th>Nama MHS</th>
			<th>Jumlah Nilai Kosong</th>
			<th class="noheader"></th>
		</tr>
	</thead>
	<tbody>
		{foreach $mhs_set as $mhs}
			<tr>
				<td class="center">{$mhs@index + 1}</td>
				<td>{$mhs.NIM_MHS}</td>
				<td>{$mhs.NM_PENGGUNA}</td>
				<td class="noheader">{$mhs.JML_NILAI_KOSONG}</td>
				<td class="center"><a href="monitoring-mhs-nilai-kosong.php?mode=detail-mhs&id_mhs={$mhs.ID_MHS}&thn_angkatan_mhs={$thn_angkatan_mhs}">Detail</a></td>
			</tr>
		{/foreach}
	</tbody>
</table>

{/if}

{if $mode == 'detail-mhs'}

<a href="monitoring-mhs-nilai-kosong.php?thn_angkatan_mhs={$smarty.get.thn_angkatan_mhs}"> << Kembali</a>

<table>
	<tbody>
		<tr>
			<td>NIM Mahasiswa</td>
			<td>{$data_mhs.NIM_MHS}</td>
		</tr>
		<tr>
			<td>Nama Mahasiswa</td>
			<td>{$data_mhs.NM_PENGGUNA}</td>
		</tr>
		<tr>
			<td>Angkatan Mahasiswa</td>
			<td>{$data_mhs.THN_ANGKATAN_MHS}</td>
		</tr>
		<tr>
			<td>Kelas Mahasiswa</td>
			<td>{$data_mhs.NAMA_KELAS}</td>
		</tr>
	</tbody>
</table>

<table>
	<thead>
		<tr>
			<th>No</th>
			<th>Tahun Ajaran</th>
			<th>Kode Matkul</th>
			<th>Nama Matkul</th>
			<th>SKS</th>
			<th>Nilai Angka</th>
			<th>Nilai Huruf</th>
		</tr>
	</thead>
	<tbody>
		{foreach $matkul_set as $matkul}
			<tr>
				<td class="center">{$matkul@index + 1}</td>
				<td>{$matkul.TAHUN_AJARAN} {$matkul.NM_SEMESTER}</td>
				<td>{$matkul.KD_MATA_KULIAH}</td>
				<td>{$matkul.NM_MATA_KULIAH}</td>
				<td>{$matkul.SKS}</td>
				<td>{if $matkul.NILAI_ANGKA == ''} - {else}{$matkul.NILAI_ANGKA}{/if}</td>
				<td>{if $matkul.NILAI_HURUF == ''} - {else}{$matkul.NILAI_HURUF}{/if}</td>
			</tr>
		{/foreach}
	</tbody>
</table>


{/if}