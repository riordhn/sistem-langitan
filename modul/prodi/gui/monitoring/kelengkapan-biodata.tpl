<div class="center_title_bar">Kelengkapan Biodata Mahasiswa</div>

{if $mode == 'list'}

<table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 5px">
	<tr>
		<td>
			<form action="monitoring-kelengkapan-biodata.php" method="get">
				Angkatan Mahasiswa :
					<select name="thn_angkatan_mhs">
					<option value="" >-- Pilih Angkatan --</option>
					{foreach $angkatan_mhs_set as $angkatan}
						<option value="{$angkatan.THN_ANGKATAN_MHS}" {if $angkatan.THN_ANGKATAN_MHS == $thn_angkatan_mhs}selected{/if}
						>{$angkatan.THN_ANGKATAN_MHS}</option>
					{/foreach}
				</select>
				<input type="submit" name="View" value="View">
			</form>
		</td>
	</tr>
</table>

	{if isset ($mhs_set)}

	<table>
		<thead>
			<tr>
				<th>No</th>
				<th>NIM</th>
				<th>Nama MHS</th>
				<th>Angkatan</th>
				<th>Prodi</th>
				<th>Status MHS</th>
				<th>Email</th>
				<th>No. HP</th>
				<th>Kelengkapan Biodata</th>
				<th class="noheader"></th>
			</tr>
		</thead>
		<tbody>
			{foreach $mhs_set as $mhs}
				<tr>
					<td class="center">{$mhs@index + 1}</td>
					<td>{$mhs.NIM_MHS}</td>
					<td>{$mhs.NM_PENGGUNA}</td>
					<td>{$mhs.THN_ANGKATAN_MHS}</td>
					<td>{$mhs.NM_PROGRAM_STUDI}</td>
					<td>{$mhs.NM_STATUS_PENGGUNA}</td>
					<td>{$mhs.EMAIL_ALTERNATE}</td>
					<td>{$mhs.MOBILE_MHS}</td>
					<td>{if $mhs.CEK_KELENGKAPAN == 0}Tidak{else}Ya{/if}</td>
					<td class="center">
					<!-- <a href="monitoring-mhs-nilai-kosong.php?mode=detail-mhs&id_mhs={$mhs.ID_MHS}&thn_angkatan_mhs={$thn_angkatan_mhs}">Detail</a> -->
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>

	{/if}

{/if}

{if $mode == 'detail-mhs'}

<a href="monitoring-mhs-nilai-kosong.php?thn_angkatan_mhs={$smarty.get.thn_angkatan_mhs}"> << Kembali</a>

<table>
	<tbody>
		<tr>
			<td>NIM Mahasiswa</td>
			<td>{$data_mhs.NIM_MHS}</td>
		</tr>
		<tr>
			<td>Nama Mahasiswa</td>
			<td>{$data_mhs.NM_PENGGUNA}</td>
		</tr>
		<tr>
			<td>Angkatan Mahasiswa</td>
			<td>{$data_mhs.THN_ANGKATAN_MHS}</td>
		</tr>
		<tr>
			<td>Kelas Mahasiswa</td>
			<td>{$data_mhs.NAMA_KELAS}</td>
		</tr>
	</tbody>
</table>

<table>
	<thead>
		<tr>
			<th>No</th>
			<th>Tahun Ajaran</th>
			<th>Kode Matkul</th>
			<th>Nama Matkul</th>
			<th>SKS</th>
			<th>Nilai Angka</th>
			<th>Nilai Huruf</th>
		</tr>
	</thead>
	<tbody>
		{foreach $matkul_set as $matkul}
			<tr>
				<td class="center">{$matkul@index + 1}</td>
				<td>{$matkul.TAHUN_AJARAN} {$matkul.NM_SEMESTER}</td>
				<td>{$matkul.KD_MATA_KULIAH}</td>
				<td>{$matkul.NM_MATA_KULIAH}</td>
				<td>{$matkul.SKS}</td>
				<td>{if $matkul.NILAI_ANGKA == ''} - {else}{$matkul.NILAI_ANGKA}{/if}</td>
				<td>{if $matkul.NILAI_HURUF == ''} - {else}{$matkul.NILAI_HURUF}{/if}</td>
			</tr>
		{/foreach}
	</tbody>
</table>


{/if}