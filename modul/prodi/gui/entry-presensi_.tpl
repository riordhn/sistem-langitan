<div class="center_title_bar">Detail Presensi Mata Ajar </div>  
		<div id="tabs">
        <div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
        <div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Materi-Dos</div>
		<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Presensi Mhs</div>
		<div id="tab4" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('4');">Edit Materi-Dos</div>
    	</div>
        <div class="tab_bdr"></div>
		<div class="panel" id="panel1" style="display: {$disp1} ">
				<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
				{foreach item="dtkelas" from=$DT_KELAS}
                  <tr>
                    <td>Kode Mata Ajar</td>
                    <td>:</td>
					<td>{$dtkelas.KD_MATA_KULIAH}</td>
                  </tr>
				  <tr>
                    <td>Nama Mata Ajar</td>
                    <td>:</td>
					<td>{$dtkelas.NM_MATA_KULIAH}</td>
                  </tr>
				  <tr>
                    <td>SKS</td>
                    <td>:</td>
					<td>{$dtkelas.KREDIT_SEMESTER}</td>
                  </tr>
				  <tr>
                    <td>Kelas</td>
                    <td>:</td>
					<td>{$dtkelas.NAMA_KELAS}</td>
                  </tr>
				  <tr>
                    <td>Ruang</td>
                    <td>:</td>
					<td>{$dtkelas.NM_RUANGAN}</td>
                  </tr>
				 {/foreach}
                  
            
				</table>
				
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#000">
			     <td width="4%"><font color="#FFFFFF">Kuliah ke</font></td>
				 <td width="8%"><font color="#FFFFFF">Tgl</font></td>
				 <td width="8%"><font color="#FFFFFF">Jam</font></td>
				 <td width="25%"><font color="#FFFFFF">Materi</font></td>
				 <td width="26%"><font color="#FFFFFF">Dosen</font></td>
				 <td width="4%"><font color="#FFFFFF">MHS Msk</font></td>
				 <td width="4%"><font color="#FFFFFF">Absen</font></td>
				 <td width="6%"><font color="#FFFFFF">Presensi</font></td>
				 <td width="6%"><font color="#FFFFFF">Aksi</font></td>
				</tr>
					{foreach name=test item="presensi" from=$DATA_PRESENSI}
				<tr>
			     <td >{$smarty.foreach.test.iteration}</td>
				 <td ><a href="entry-presensi.php?action=edit&id_presensi={$presensi.IDPRESENSI}&idkelas={$idkelas}&ed=1">{$presensi.TGL_PRESENSI_KELAS}</a></td>
				 <td >{$presensi.JAM}</td>
				 <td >{$presensi.ISI_MATERI_MK}</td>
				 <td ><ol>{$presensi.NMDOS}</ol></td>
				 <td >{$presensi.HADIR}</td>
				 <td >{$presensi.ABSEN}</td>
				 {if $presensi.HADIR==0}
				 <td ><a href="entry-presensi.php?action=presensi&id_presensi={$presensi.IDPRESENSI}&idkelas={$idkelas}&id=1" onclick="displayPanel('3')">Entry</a></td>
				 {else}
				 <td ><a href="entry-presensi.php?action=presensi&id_presensi={$presensi.IDPRESENSI}&idkelas={$idkelas}&id=2" onclick="displayPanel('3')">Edit</a></td>
				 {/if}
				  <td ><a href="entry-presensi.php?action=hapus&id_presensi={$presensi.IDPRESENSI}&idkelas={$idkelas}&id_materi={$presensi.ID_MATERI_MK}">Hapus</a></td>
				</tr>
			     {foreachelse}
        			<tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
        			{/foreach}
				</table>		 
		</div>
		
<div class="panel" id="panel2" style="display: {$disp2}">

<form action="entry-presensi.php" method="post" >
<input type="hidden" name="action" value="add" >
<input type="hidden" name="idkelas" value={$idkelas} >
	<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="20%">Tgl Kuliah</td>
                    <td width="1%">:</td>
					<td width="79%"><input type="Text" name="calawal" value="Isikan Tanggal" id="calawal" maxlength="25" size="25">
					<input type="button" value="" style="background-image:url(includes/cal/cal.gif)
; background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calawal','ddmmmyyyy');" /></td>
                  </tr>
				  <tr>
                    <td>Jam Mulai Kuliah</td>
                    <td>:</td>
					<td><input type="text" name="jam_mulai"> Contoh: 07:00</td>
                  </tr>
				  <tr>
                    <td>Jam Selesai Kuliah</td>
                    <td>:</td>
					<td><input type="text" name="jam_selesai"> Contoh: 10:00</td>
                  </tr>
				  {foreach name=nomer item="dosen" from=$T_DOSEN}
				  <tr>
                    <td>Dosen Pengajar-{$smarty.foreach.nomer.iteration}</td>
                    <td>:</td>
					<td><input name="dosen{$smarty.foreach.nomer.iteration}" type="checkbox" value="{$dosen.ID_DOSEN}" />{$dosen.NAMA}</td>
			
                  </tr>
				  {/foreach}
				  <input type="hidden" name="counter1" value="{$smarty.foreach.nomer.iteration}" >
				  <tr>
                    <td>Materi</td>
                    <td>:</td>
					<td><textarea name="materi" cols="60"></textarea></td>
                  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="simpan" value="Simpan"></td>
					</tr>
        </table>		

</form>

</div>
		
<div class="panel" id="panel3" style="display: {$disp3}">
<form action="entry-presensi.php" method="post" >
<input type="hidden" name="action" value="masuk" >
<input type="hidden" name="id_presensi" value={$id_presensi} >
<input type="hidden" name="idkelas" value={$idkelas} >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
			   <tr bgcolor="#000">
			     <td width="4%"><font color="#FFFFFF">No</font></td>
				 <td width="11%"><font color="#FFFFFF">NIM</font></td>
				 <td width="20%"><font color="#FFFFFF">Nama</font></td>
				 <td width="4%"><font color="#FFFFFF">Status</font></td>
			  </tr>
			  {foreach name=presen item="pst" from=$PESERTA}
			   <tr>
			     <td >{$smarty.foreach.presen.iteration}</td>
				 <td >{$pst.NIM_MHS}</td>
				 <td >{$pst.NM_PENGGUNA}</td>
				 <td><input name="mhs{$smarty.foreach.presen.iteration}" type="checkbox" value="{$pst.ID_MHS}" {$pst.TANDA}   /></td>
				 
			   </tr>
			     {foreachelse}
        			<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
        			{/foreach}
				<input type="hidden" name="counter" value="{$smarty.foreach.presen.iteration}" >
				
  </table>
		{if $stt==1}
		<input type="hidden" name="pilihan" value="1" >
		<tr>	
		<td><div align="right"><input type="submit" name="proses" value="Proses"></div></td>
		</tr>
		{else}
		<input type="hidden" name="pilihan" value="2" >
		<tr>	
		<td><div align="right"><input type="submit" name="proses" value="Update"></div></td>
		</tr>
		{/if}
</form>	  
</div>
<div class="panel" id="panel4" style="display: {$disp4}">
<form action="entry-presensi.php" method="post" >
<input type="hidden" name="action" value="update" >
<input type="hidden" name="idkelas" value={$idkelas} >
<input type="hidden" name="id_presensi" value={$id_presensi} >
	<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
				 {foreach item="list" from=$DT_EDIT}
                  <tr>
                    <td width="20%">Tgl Kuliah</td>
                    <td width="1%">:</td>
					<td width="79%"><input type="Text" name="calawal1" value="{$list.TGL_PRESENSI_KELAS}" id="calawal1" maxlength="25" size="25">
					<input type="button" value="" style="background-image:url(includes/cal/cal.gif)
; background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calawal1','ddmmmyyyy');" /></td>
                  </tr>
				  <tr>
                    <td>Jam Mulai Kuliah</td>
                    <td>:</td>
					<td><input type="text" name="jam_mulai" value="{$list.WAKTU_MULAI}"> Contoh: 07:00</td>
                  </tr>
				  <tr>
                    <td>Jam Selesai Kuliah</td>
                    <td>:</td>
					<td><input type="text" name="jam_selesai" value="{$list.WAKTU_SELESAI}"> Contoh: 10:00</td>
                  </tr>
				  
				  <tr>
                    <td>Materi</td>
                    <td>:</td>
					<td><textarea name="materi" cols="60">{$list.ISI_MATERI_MK}</textarea></td>
                  </tr>
				  {/foreach}
				  {foreach name=nomer item="dosen" from=$DOS_MASUK}
				  <tr>
                    <td>Dosen Pengajar-{$smarty.foreach.nomer.iteration}</td>
                    <td>:</td>
					<td><input name="dosen{$smarty.foreach.nomer.iteration}" type="checkbox" value="{$dosen.ID_DOSEN}" {$dosen.TANDA} />{$dosen.NAMA}</td>
			
                  </tr>
				  {/foreach}
				  <input type="hidden" name="counter1" value="{$smarty.foreach.nomer.iteration}" >
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="update" value="Update"></td>
					</tr>
        </table>		

</form>
</div>
<script>$('form').validate();</script>      
