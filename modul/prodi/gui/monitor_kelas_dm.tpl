{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable").tablesorter(
		{
		sortList: [[1,0], [2,0]],
		headers: {
      7: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">MONITORING KAPASITAS KELAS DM</div>  
<div id="panel1" style="display: {$disp1} ">
<p> </p>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Kode</th>
			<th>Nama Mata Ajar</th>
			<th>SKS</th>
			<th>KLS</th>
			<th>STS</th>
			<th>Kpst</th>
			<th>Terisi</th>
			<th class="noheader">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=test item="list" from=$T_MK}
		{if $list.KAPASITAS_KELAS_MK == $list.KLS_TERISI}
		<tr bgcolor="#ff828e">
		{elseif $list.KAPASITAS_KELAS_MK - $list.KLS_TERISI <= 5}
		<tr bgcolor="#fdff5a">
		{else}
		<tr>
		{/if}
		<td>{$list.KD_MATA_KULIAH}</td>
		<td>{$list.NM_MATA_KULIAH}</td>
		<td><center>{$list.KREDIT_SEMESTER}</center></td>
		<td><center>{$list.NAMA_KELAS}</center></td>
		<td><center>{$list.STATUS}</center></td>
		<td><center>{$list.KAPASITAS_KELAS_MK}</center></td>
		<td><center><a href="monitor_kelas_dm.php?action=detail&id_kelas_mk={$list.ID_KELAS_MK}">{$list.KLS_TERISI}</a></center></td>
		<td><a href="monitor_kelas_dm.php?action=kapasitas&id_kelas_mk={$list.ID_KELAS_MK}">Ubah Kapasitas</a></td>
		</tr>
		{foreachelse}
		<tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>
</div>
		
<div id="panel2" style="display: {$disp2}">
<form action="monitor_kelas_dm.php" method="post">
<input type="hidden" name="action" value="add" >
<table class="tablesorter" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<th colspan="3">Tambah Kapasitas Kelas</th>
	</tr>
	{foreach item="ubah" from=$TJAF1}
	<input type="hidden" name="id_kelas_mk" value="{$ubah.ID_KELAS_MK}" />
	<tr>
		<td>Kode Mata Ajar</td>
		<td>:</td>
		<td>{$ubah.KD_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar</td>
		<td>:</td>
		<td>{$ubah.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Kelas</td>
		<td>:</td>
		<td>{$ubah.NAMA_KELAS}</td>
	</tr>					
	<tr bgcolor="yellow">
		<td>KAPASITAS KELAS</td>
		<td>:</td>
		<td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$ubah.KAPASITAS_KELAS_MK}"/></td>
	</tr>
	{/foreach}
</table>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript:history.go(-1);">&nbsp;&nbsp;&nbsp;<input type="submit" name="Simpan1" value="Simpan"></p>
</form>			    
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable1").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
      7: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div id="panel3" style="display: {$disp3} ">
<p> </p>
<table>
	{foreach item="info" from=$T_INFO}
	<tr>
		<td width="29%">Kode Mata Ajar / SKS / Kelas</td>
		<td width="2%">:</td>
		<td width="69%">{$info.KD_MATA_KULIAH} / {$info.KREDIT_SEMESTER} / {$info.NAMA_KELAS}</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar</td>
		<td>:</td>
		<td width="69%" >{$info.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Tim Pengajar</td>
		<td>:</td>
		<td><br><ol>{$info.TIM}</li></ol></td>
	</tr>
		{/foreach}
</table>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript:history.go(-1);"></p>
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="15%">NIM</th>
			<th width="35%">Nama Mahasiswa</th>
			<th width="35%">Prodi</th>
			<th width="15%">Status</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=test item="list" from=$T_DETAIL}
			{if $list.STATUS == "Blm Disetujui"}
		<tr bgcolor="#f0939b">
			{else}
		<tr>
			{/if}
			<td>{$list.NIM_MHS}</td>
			<td>{$list.NM_PENGGUNA}</td>
			<td>{$list.NM_PROGRAM_STUDI}</td>
			<td>{$list.STATUS}</td>
		</tr>
		{foreachelse}
		<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript:history.go(-1);"></p>
</div>

{literal}
 <script>$('form').validate();</script>
{/literal}