{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
    if ($("#myTable").length > 0) {

        $("#myTable").tablesorter({
            sortList: [[1,0], [6,0]],
            headers: {
                8: { sorter: false }
            }
        });
    }

    if ($("#myTable1").length > 0) {

        $("#myTable1").tablesorter({
            sortList: [[1,0]],
            headers: {
                6: { sorter: false },
                4: { sorter: false }
            }
        });
    }
    

    //$.ajaxSetup();

    $("#smt").change(function(){

            var jqXHR =  $.ajax({
                    async: false,
                    statusCode : { 404 : function() { console.log('Not found');}},
                    url : 'ajax-operation.php?action=getkurikulumpenawaran',
                    type : 'post',
                    dataType : 'json',
                    data: $("#form-tambah-penawaran").serialize(),
                    success : function(data){
                            //console.log(data);

                    }
            }).done(function(data){
                    //console.log(data);
                    $("#kur").html(data.dinamis);
            });
            return jqXHR;
            //console.log(jqXHR.responseText);
    });
});

</script>
{/literal}

<div id="usulan-mk">
<div class="center_title_bar">Usulan Mata Ajar</div>
	<div id="tabs">
  <div id="tab1" {if $smarty.request.action == 'tampil'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('1');"><a href="usulan-mk.php?action=tampil&smt={$SMT}&kur={$kuraktif}" style="color:#000000;text-decoration:none;cursor:pointer">Rincian </a></div>
  <div id="tab2" {if $smarty.request.action == 'penawaran'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');"><a href="usulan-mk.php?action=penawaran&smt={$SMT}&kur={$kuraktif}" style="color:#000000;text-decoration:none;cursor:pointer">Penawaran</a></div>
	<div id="tab3" {if $smarty.get.action == 'updateview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>
	<div id="tab4" {if $smarty.get.action == 'adkelview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('4');">Tambah Kelas</div>
	<div id="tab5" {if $smarty.get.action == 'adhariview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('5');">Tambah Hari</div>
 </div>

<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
<form action="usulan-mk.php" method="get">
<input type="hidden" name="action" value="tampil" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		Tahun Akademik :
			<select name="smt">
				<option value=''>-- PILIH THN AKD --</option>
				{foreach item="smt" from=$T_ST}
				{html_options values=$smt.ID_SEMESTER output=$smt.SMT selected=$SMT}
				{/foreach}
			</select>
		<input type="submit" name="View" value="View">
		</td>
	</tr>
</table>
</form>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0" style="width: 100%">
	<thead>
		<tr>
			<th>Kode</th>
			<th>Nama Mata Ajar</th>
			<th>Kelas</th> 
			<th>SKS</th>
			<th>Tkt Smt</th>
			<th>Ruang</th>
			<th>Hari</th>
			<th>Jam</th>
			<th class="noheader">Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$T_MK}
	{if $list.NM_JADWAL_HARI == "" || $list.JAM == ""}
		<tr bgcolor="#FFC0CB">
	{else}
		<tr>
	{/if}
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.NAMA_KELAS}</center></td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td><center>{$list.TINGKAT_SEMESTER}</center></td>
			<td><center>{$list.NM_RUANGAN}</center></td>
			<td><center>{$list.NM_JADWAL_HARI}</center></td>
			<td><center>{$list.NM_JADWAL_JAM}</center></td>
			<td>
			{if $kdf==7 and ($jen ==1 or $jen ==5) and $kdprod !=90}
			<a href="usulan-mk.php?action=del&id_klsmk={$list.ID_KELAS_MK}&smt={$SMT}">Del</a>
			&nbsp;|&nbsp;
			<a href="usulan-mk.php?action=adkelview&id_klsmk={$list.ID_KELAS_MK}&smt={$SMT}">Add Kls</a>
			<a href="usulan-mk.php?action=updateview&id_klsmk={$list.ID_KELAS_MK}&smt={$SMT}">Upd</a>
			{else}
			<a href="usulan-mk.php?action=updateview&id_klsmk={$list.ID_KELAS_MK}&smt={$SMT}">Update</a>
			&nbsp;|&nbsp;
			<a href="usulan-mk.php?action=adkelview&id_klsmk={$list.ID_KELAS_MK}&smt={$SMT}">Add Kelas</a>
			&nbsp;|&nbsp;
			<a href="usulan-mk.php?action=adhariview&id_klsmk={$list.ID_KELAS_MK}&smt={$SMT}">Add Hari</a>
			&nbsp;|&nbsp;
			<a href="usulan-mk.php?action=del&id_klsmk={$list.ID_KELAS_MK}&smt={$SMT}" style="color: gray">Delete</a>
			{/if}
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="usulan-mk.php" method="post" id="form-tambah-penawaran">
<input type="hidden" name="action" value="penawaran" >
<input type="hidden" name="id_prodi" id="id_prodi" value="{$kdprod}" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		Tahun Akademik :
			<select name="smt" id="smt">
				<option value=''>-- PILIH THN AKD --</option>
				{foreach item="smt" from=$T_ST}
				{html_options values=$smt.ID_SEMESTER output=$smt.SMT selected=$SMT}
				{/foreach}
			</select>
		Tahun Kurikulum :
			<select name="kur" id="kur">
				<option value=''>-- THN KUR --</option>
				{foreach item="thkur" from=$T_KUR}
				{html_options values=$thkur.ID_KURIKULUM_PRODI output=$thkur.NAMA selected=$kuraktif}
				{/foreach}
			</select>
		<input type="submit" name="View" value="View">
		</td>
	</tr>
</table>
</form>
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="10%">Kode</th>
			<th width="35%">Nama Mata Ajar</th>
			<th width="10%">SKS</th>
			<th width="10%">Tkt Smt</th>
			<th class="noheader" width="20%">Tahun Kurikulum</th>
			<th width="10%">Status</th>
			<th class="noheader" width="15%">Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$TAWAR}
		<tr>
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td><center>{$list.TINGKAT_SEMESTER}</center></td>
			<td><center>{$list.NM_KURIKULUM} ({$list.THN_KURIKULUM})</center></td>
			<td><center>{$list.NM_STATUS_MK}</center></td>
			<!-- <td><center><a href="usulan-mk.php?action=add&id_mk={$list.ID_KURIKULUM_MK}&smt={$SMT}">Ditawarkan</a></center></td> -->
			<td>
			<form action="usulan-mk.php" method="post">
				<input type="hidden" name="action" value="add">
				<input type="hidden" name="id_mk" value="{$list.ID_KURIKULUM_MK}">
				<input type="hidden" name="smt" value="{$SMT}">
				<input type="hidden" name="kur" value="{$list.ID_KURIKULUM_PRODI}">
				<input type="submit" name="Ditawarkan" value="Ditawarkan">
			</form>
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
<p> </p>
<form action="usulan-mk.php" method="post">
<input type="hidden" name="action" value="update1" >
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	<tr bgcolor="#265226"><td colspan="3"><strong><font color="#fff"> Lengkapi Kelas </font></strong></td></tr>
	{foreach item="ubah" from=$TJAF}
	<input type="hidden" name="id_kelas_mk" value="{$ubah.ID_KELAS_MK}" />
	<input type="hidden" name="smt" value="{$SMT}" />
	<tr>
		<td width="29%">Kode Mata Ajar</td>
		<td width="2%"><center>:</center></td>
		<td width="69%">{$ubah.KD_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar</td>
		<td><center>:</center></td>
		<td>{$ubah.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Kelas</td>
		<td><center>:</center></td>
		<td>
			<select name="kelas_mk" id="kelas_mk">
				{foreach $NM_KELAS as $kls}
				{html_options values=$kls.ID_NAMA_KELAS output=$kls.NAMA_KELAS selected=$ubah.NO_KELAS_MK}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Kapasitas Kelas</td>
		<td><center>:</center></td>
		<td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$ubah.KAPASITAS_KELAS_MK}"/> [Wajib diisi]</td>
	</tr>
	<tr>
		<td>Rencana Perkuliahan (Tatap Muka)</td>
		<td><center>:</center></td>
		<td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$ubah.JUMLAH_PERTEMUAN_KELAS_MK}"/> [Wajib diisi]</td>
	</tr>
	<tr>
		<td>Hari</td>
		<td><center>:</center></td>
		<td>
			<select name="hari" id="hari">
			{foreach item="hari" from=$T_HARI}
			{html_options values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI selected=$ubah.ID_JADWAL_HARI}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Jam</td>
		<td><center>:</center></td>
			<td>
			<select name="jam" id="jam">
			{foreach item="jam" from=$T_JAM}
			{html_options values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$ubah.ID_JADWAL_JAM}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Ruangan</td>
		<td><center>:</center></td>
		<td>
			<select name="ruangan" id="ruangan">
			<option value="">-- Belum Ditentukan --</option>
			{foreach item="ruangan" from=$T_RUANG}
			{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG selected=$ubah.ID_RUANGAN}
			{/foreach}
			</select>
		</td>
	</tr>
	{/foreach}
</table>
{if $ubah.KD_MATA_KULIAH == ""}
<p><em>Data tidak ditemukan</em></p>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript: displayPanel('1');"></p>
{else}
<p><input type="submit" name="Simpan1" value="Simpan"></p>
{/if}
</form>
</div>

<div class="panel" id="panel4" style="display: {$disp4}">
<p> </p>
<form action="usulan-mk.php" method="post">
<input type="hidden" name="action" value="tambahkelas" >
<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr bgcolor="#265226"><td colspan="3"><strong><font color="#fff"> Tambah Kelas </font></strong></td></tr>
	{foreach item="tambahkelas" from=$TJAF1}
	<input type="hidden" name="id_kur_mk" value="{$tambahkelas.ID_KURIKULUM_MK}" />
	<input type="hidden" name="smt" value="{$SMT}" />
	{if $kdf==7}
	<tr>
		<td width="29%">Kode Mata Ajar</td>
		<td width="2%"><center>:</center></td>
		<td width="69%">{$tambahkelas.KD_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar</td>
		<td><center>:</center></td>
		<td>{$tambahkelas.NM_MATA_KULIAH}</td>
	</tr>
	<tr bgcolor="yellow">
		<td>Kelas MK [A/B/C/D]</td>
		<td><center>:</center></td>
		<td>
			<select name="kelas_mk" id="kelas_mk">
				<option value=''>------</option>
				{foreach item="namakls" from=$NM_KELAS}
				{html_options values=$namakls.ID_NAMA_KELAS output=$namakls.NAMA_KELAS}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>KAPASITAS KELAS</td>
		<td><center>:</center></td>
		<td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambahkelas.KAPASITAS_KELAS_MK}"/></td>
	</tr>
	<tr>
		<td>Rencana Perkuliahan</td>
		<td><center>:</center></td>
		<td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambahkelas.JUMLAH_PERTEMUAN_KELAS_MK}"/></td>
	</tr>
	{else}
	<tr>
		<td width="29%">Kode Mata Ajar</td>
		<td width="2%"><center>:</center></td>
		<td width="69%">{$tambahkelas.KD_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar</td>
		<td><center>:</center></td>
		<td>{$tambahkelas.NM_MATA_KULIAH}</td>
	</tr>
	<tr bgcolor="yellow">
		<td>Kelas MK [A/B/C/D]</td>
		<td><center>:</center></td>
		<td>
			<select name="kelas_mk" id="kelas_mk">
				<option value=''>------</option>
				{foreach item="namakls" from=$NM_KELAS}
				{html_options values=$namakls.ID_NAMA_KELAS output=$namakls.NAMA_KELAS}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>KAPASITAS KELAS</td>
		<td><center>:</center></td>
		<td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambahkelas.KAPASITAS_KELAS_MK}"/></td>
	</tr>
	<tr>
		<td>Rencana Perkuliahan</td>
		<td><center>:</center></td>
		<td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambahkelas.JUMLAH_PERTEMUAN_KELAS_MK}"/></td>
	</tr>
	<tr bgcolor="yellow">
		<td>Hari</td>
		<td><center>:</center></td>
		<td>
			<select name="hari" id="hari">
			{foreach item="hari" from=$T_HARI}
			{html_options values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI selected=$tambahkelas.ID_JADWAL_HARI}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr bgcolor="yellow">
		<td>Jam</td>
		<td><center>:</center></td>
		<td>
			<select name="jam" id="jam">
			{foreach item="jam" from=$T_JAM}
			{html_options values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$tambahkelas.ID_JADWAL_JAM}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr bgcolor="yellow">
		<td>Ruangan</td>
		<td><center>:</center></td>
		<td>
			<select name="ruangan" id="ruangan">
				<option value=''>------</option>
				{foreach item="ruangan" from=$T_RUANG}
				{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG}
				{/foreach}
			</select>
		</td>
	</tr>
	{/if}
	{/foreach}
</table>
{if $tambahkelas.KD_MATA_KULIAH == ""}
<p><em>Data tidak ditemukan</em></p>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript: displayPanel('1');"></p>
{else}
<p><input type="button" name="kembali" value="Kembali" onClick="window.location.href='#aktivitas-uma!usulan-mk.php';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Simpan1" value="Tambah Kelas"></p>
{/if}
</form>
</div>

<div class="panel" id="panel5" style="display: {$disp5}">
<p> </p>
<form action="usulan-mk.php" method="post">
<input type="hidden" name="action" value="tambahhari" >
<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr bgcolor="#265226"><td colspan="3"><strong><font color="#fff"> Tambah Hari </font></strong></td></tr>
	{foreach item="tambahhari" from=$TJAF2}
	<input type="hidden" name="id_kelas_mk" value="{$tambahhari.ID_KELAS_MK}" />
	<input type="hidden" name="smt" value="{$SMT}" />
	<tr>
		<td width="29%">Kode Mata Ajar</td>
		<td width="2%"><center>:</center></td>
		<td width="69%">{$tambahhari.KD_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar</td>
		<td><center>:</center></td>
		<td>{$tambahhari.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Kelas MK [A/B/C/D]</td>
		<td><center>:</center></td>
		<td>
			<select name="kelas_mk" id="kelas_mk">
			{foreach item="namakls" from=$NM_KELAS}
			{html_options values=$namakls.ID_NAMA_KELAS output=$namakls.NAMA_KELAS selected=$tambahhari.ID_NAMA_KELAS}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>KAPASITAS KELAS</td>
		<td><center>:</center></td>
		<td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambahhari.KAPASITAS_KELAS_MK}"/></td>
	</tr>
	<tr>
		<td>Rencana Perkuliahan</td>
		<td><center>:</center></td>
		<td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambahhari.JUMLAH_PERTEMUAN_KELAS_MK}"/></td>
	</tr>
	<tr bgcolor="yellow">
		<td>Hari</td>
		<td><center>:</center></td>
		<td>
			<select name="hari" id="hari">
				<option value=''>------</option>
				{foreach item="hari" from=$T_HARI}
				{html_options values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Jam</td>
		<td><center>:</center></td>
		<td>
			<select name="jam" id="jam">
			{foreach item="jam" from=$T_JAM}
			{html_options values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$tambahhari.ID_JADWAL_JAM}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Ruangan</td>
		<td><center>:</center></td>
		<td>
			<select name="ruangan" id="ruangan">
			{foreach item="ruangan" from=$T_RUANG}
			{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG selected=$tambahhari.ID_RUANGAN}
			{/foreach}
			</select>
		</td>
	</tr>
	{/foreach}
</table>
{if $tambahhari.KD_MATA_KULIAH == ""}
<p><em>Data tidak ditemukan</em></p>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript: displayPanel('1');"></p>
{else}
<p><input type="button" name="kembali" value="Kembali" onClick="window.location.href='#aktivitas-uma!usulan-mk.php';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Simpan2" value="Tambah Hari"></p>
{/if}
</form>
</div>
</div>