<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script type="text/javascript" src="jquery-1.2.1.pack.js"></script>
{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
      8: { sorter: false },
      9: { sorter: false }
		}
		}
	);
}
);

function lookup(inputString) {
if(inputString.length == 0) {
// Hide the suggestion box.
$('#suggestions').hide();
} else {
$.post("gp.php", {queryString: ""+inputString+""}, function(data){
if(data.length >0) {
$('#suggestions').show();
$('#autoSuggestionsList').html(data);
}
});
}
} // lookup

function fill1(thisValue) {
$('#kode_matkul').val(thisValue);
setTimeout("$('#suggestions').hide();", 200);
}
function fill2(thisValue) {
$('#nama_matkul').val(thisValue);
setTimeout("$('#suggestions').hide();", 200);
}
function fill3(thisValue) {
$('#nama_en_matkul').val(thisValue);
setTimeout("$('#suggestions').hide();", 200);
}
</script>
{/literal}

<div class="center_title_bar">Daftar Mata Ajar </div>
	<div id="tabs">
		<div id="tab1" {if $smarty.get.action == ''}class="tab_sel"{else}class="tab"{/if} align="center" onclick="javascript: displayPanel('1');">Rincian</div>
		<div id="tab2" {if $smarty.get.action == 'viewup'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Update</div>
		<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Insert MA</div>
  	</div>
<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<form action="daftarmk-psikologi.php" method="post">
<input type="hidden" name="action" value="view" />
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
      <tr>
       <td>
			 	Tahun Kurikulum :
			  <select name="thkur">
	 		  {foreach item="kur" from=$T_KUR}
			  {html_options values=$kur.ID_KURIKULUM_PRODI output=$kur.NAMA selected=$IDKURPRODI}
	 		  {/foreach}
			  </select>
			  <input type="submit" name="View" value="View">
		   </td>
      </tr>
</table>
</form>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="10%">Kode</th>
			<th width="30%">Nama Mata Ajar</th>
			<th width="6%">SKS TM</th>
			<th width="6%">SKS PRAK</th>
			<th width="6%">SKS TTL</th>
			<th width="6%">MA Blok</th>
			<th width="6%">KLP MA</th>
			<th width="6%">SMT</th>
			<th class="noheader" width="6%">Thn Kur</th>
			<th class="noheader" width="18%">Aksi</th>
		</tr>
	</thead>
	<tbody>
      {foreach name=tmk item="list" from=$T_MK}
		  <tr>
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.KREDIT_TATAP_MUKA}</center></td>
			<td><center>{$list.KREDIT_PRAKTIKUM}</center></td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td><center>{$list.STATUS_PAKET}</center></td>
			<td><center>{$list.NM_KELOMPOK_MK}</center></td>
			<td><center>{$list.TINGKAT_SEMESTER}</center></td>
			<td><center>{$list.TAHUN_KURIKULUM}</center></td>
			<td><center><a href="daftarmk-psikologi.php?action=viewup&id_mk={$list.ID_KURIKULUM_MK}" onclick="displayPanel('3')">Update</a><br><a href="daftarmk-psikologi.php?action=del&id_mk={$list.ID_KURIKULUM_MK}">Delete</a></center></td>
		</tr>
		  {foreachelse}
			<tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
		  {/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="daftarmk-psikologi.php" method="post" >
<input type="hidden" name="action" value="update1" />
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	{foreach item="ganti" from=$T_MK1}
	<input type="hidden" name="id_mk" value="{$ganti.ID_KURIKULUM_MK}" />
	<tr>
	 <td width="23%">Kode Mata Ajar</td>
	 <td width="2%"><center>:</center></td>
	 <td width="75%">{$ganti.KD_MATA_KULIAH}</td>
	</tr>
	<tr>
	 <td>Nama Mata Ajar</td>
	 <td><center>:</center></td>
	 <td>{$ganti.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
	 <td>Nama Mata Ajar (English)</td>
	 <td><center>:</center></td>
	 <td><input name="en" id="en" type="text" value="{$ganti.NM_MATA_KULIAH_EN}" style="width:500px" /></td>
	</tr>
	<tr>
	 <td>SKS TM</td>
	 <td><center>:</center></td>
	 <td>
	 <input name="kredit_tm" id="kredit_tm" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_TATAP_MUKA}"/>
	 &nbsp;<font color="grey">Tanda koma diganti dengan titik. (1,5 --> 1.5)</font>
	 </td>
	</tr>
	<tr>
	 <td>SKS PRAKT</td>
	 <td><center>:</center></td>
	 <td>
	 <input name="kredit_prk" id="kredit_prk" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_PRAKTIKUM}"/>
	 &nbsp;<font color="grey">Tanda koma diganti dengan titik. (1,5 --> 1.5)</font></td>
	</tr>
	<tr>
	 <td>SKS TTL</td>
	 <td><center>:</center></td>
	 <td>
	 <input name="kredit_matkul" id="kredit_matkul" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_SEMESTER}"/>
	 &nbsp;<font color="grey">Tanda koma diganti dengan titik. (1,5 --> 1.5)</font>
	 </td>
	</tr>
	<tr>
	 <td>Tingkat Semester</td>
	 <td><center>:</center></td>
	 <td>
		<select name="tingkat_smt" id="tingkat_smt">
			<option value="{$ganti.TINGKAT_SEMESTER}">{$ganti.TINGKAT_SEMESTER}</option>
			<option value="1">SEMESTER I</option>
			<option value="2">SEMESTER II</option>
			<option value="3">SEMESTER III</option>
			<option value="4">SEMESTER IV</option>
			<option value="5">SEMESTER V</option>
			<option value="6">SEMESTER VI</option>
			<option value="7">SEMESTER VII</option>
			<option value="8">SEMESTER VIII</option>
		 </select>
	 </td>
	</tr>
	<tr>
		<td>Kelompok Mata Ajar</td>
		<td><center>:</center></td>
		<td>
		 <select name="kelompok_mk" id="kelompok_mk">
			<option value="{$ganti.ID_KELOMPOK_MK}">{$ganti.NM_KELOMPOK_MK}</option>
			{foreach item="list" from=$KEL_MK}
			{html_options values=$list.ID_KELOMPOK_MK output=$list.NM_KELOMPOK_MK}
			{/foreach}
		 </select>
		</td>
	</tr>
	<tr>
		<td>Tahun Kurikulum</td>
		<td><center>:</center></td>
		<td>
		 <select name="thkur">
			<option value='{$ganti.ID_KURIKULUM_PRODI}'>{$ganti.NAMA}</option>
			{foreach item="list" from=$T_KUR1}
			{html_options values=$list.ID_KURIKULUM_PRODI output=$list.NAMA}
			{/foreach}
		 </select>
		</td>
	</tr>
	<tr>
	 <td>MA (KKN, Skripsi, TA)</td>
	 <td><center>:</center></td>
	 <td>
		<select name="status_mkta">
			<option value="{$ganti.STATUS_MKTA}">{$ganti.MKTA}</option>
			<option value="0">-----</option>
			<option value="1">Skripsi/TA/Thesis</option>
			<option value="2">KKN</option>
			<option value="3">Magang/PKL</option>
			<option value="4">Keasistenan</option>
			<option value="5">Modul/Skill/Micro</option>
			<option value="6">Praktikum</option>
		</select>
	 </td>
	</tr>
	<tr>
	 <td>MA Agama</td>
	 <td><center>:</center></td>
	 <td>
		<select name="status_mk_agama">
		{if $ganti.STATUS_MK_AGAMA == null}
			<option value="">-----</option>
			{foreach item="list" from=$T_AGM}
			{html_options values=$list.ID_AGAMA output=$list.NM_AGAMA}
			{/foreach}
		{else}
			<option value="">-----</option>
			{foreach item="list" from=$T_AGM}
			{html_options values=$list.ID_AGAMA output=$list.NM_AGAMA selected=$ganti.STATUS_MK_AGAMA}
			{/foreach}
		{/if}
		</select>
	 </td>
	</tr>
	<tr>
	 <td>MA Sistem Blok</td>
	 <td><center>:</center></td>
	 <td>
		<select name="status_paket">
		{if $ganti.STATUS_PAKET == null}
			<option value="">-----</option>
			<option value="Ya">Ya</option>
			<option value="Tidak">Tidak</option>		
		{else}
			<option value="{$ganti.STATUS_PAKET}">{$ganti.STATUS_PAKET}</option>
			<option value="Ya">Ya</option>
			<option value="Tidak">Tidak</option>
		{/if}
		</select>
	 </td>
	</tr>
	{/foreach}
</table>
<p><input type="submit" name="simpan" value="Simpan"></p>
</form>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable1").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
      3: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel3" style="display: {$disp3}">
<p> </p>
<form action="daftarmk-psikologi.php" method="post">
<input type="hidden" name="action" value="insertmk" />
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>Tahun Kurikulum</td>
		<td><center>:</center></td>
		<td colspan="2">
			<select name="thkur1">
		  {foreach item="kur" from=$T_KUR}
		  {html_options values=$kur.ID_KURIKULUM_PRODI output=$kur.NAMA selected=$IDKURPRODI}
		  {/foreach}
		  </select>
		</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar</td>
		<td><center>:</center></td>
		<td><input name="nama_mk" id="nama_mk" type="text" size="40" maxlength="40" /></td>
		<td><input type="submit" name="View" value="Cari Mata Ajaran"></td>
	</tr>
</table>
</form>
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
    <tr>
	 <th width="15%">Kode</th>
	 <th width="35%">Nama Mata Ajar</th>
	 <th width="35%">Nama Mata Ajar (English)</th>
	 <th class="noheader" width="15%">Aksi</th>
	</tr>
	</thead>
	<tbody>
	{foreach name="test" item="list" from=$INSERT_MK}
	<tr>
	 <td>{$list.KD_MATA_KULIAH}</td>
	 <td>{$list.NM_MATA_KULIAH}</td>
	 <td>{$list.NM_MATA_KULIAH_EN}</td>
	 <td><center><a href="daftarmk-psikologi.php?action=add&id_mk={$list.ID_MATA_KULIAH}&tkur={$ID_KP}">Pilih</a></center></td>
	</tr>
	{foreachelse}
	<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

 <script>$('form').validate();</script>