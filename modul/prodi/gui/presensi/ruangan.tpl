<div class="center_title_bar">Presensi Kelas - {$ruangan->NM_RUANGAN}</div>

<form action="presensi-kelas.php" method="get">
<input type="hidden" name="mode" value="ruangan" />
<input type="hidden" name="id_ruangan" value="{$ruangan->ID_RUANGAN}" />
<table>
    <tr>
        <td>Tanggal</td>
        <td>
            {html_select_date prefix="tgl_" start_year=-1 field_order="DMY"}
            <input type="submit" value="Tampilkan" />
        </td>
    </tr>
</table>
</form>

<table style="width: 75%">
    <tr>
        <th style="width: 30px">No</th>
        <th>Jam</th>
        <th>Kelas</th>
        <th>Dosen</th>
        <th></th>
    </tr>
    {for $i=0 to $ruangan->PRESENSI_KELASs->Count()-1}
    {$presensi_kelas=$ruangan->PRESENSI_KELASs->Get($i)}
    <tr>
        <td class="center">{$i+1}</td>
        <td>
            {$presensi_kelas->WAKTU_MULAI} - {$presensi_kelas->WAKTU_SELESAI}
        </td>
        <td>
            {$presensi_kelas->KELAS_MK->MATA_KULIAH->NM_MATA_KULIAH}
        </td>
        <td>
            {$presensi_kelas->DOSEN->PENGGUNA->NM_PENGGUNA}
        </td>
        <td>
            <a href="presensi-kelas.php?mode=kelas&id_presensi_kelas={$presensi_kelas->ID_PRESENSI_KELAS}">Lihat Kelas</a>
        </td>
    </tr>
    {/for}
</table>

<pre>{*var_dump($ruangan)*}</pre>

<button href="presensi-kelas.php">Kembali</button>