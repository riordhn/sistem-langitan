<div class="center_title_bar">Presensi Kelas</div>

<table>
    <tr>
        <th>No</th>
        <th>Ruangan</th>
        <th></th>
    </tr>
    {for $i=0 to $ruangan_set->Count()-1}
    {$ruangan=$ruangan_set->Get($i)}
    <tr>
        <td class="center">{$i+1}</td>
        <td>{$ruangan->NM_RUANGAN}</td>
        <td>
            <a href="presensi-kelas.php?mode=ruangan&id_ruangan={$ruangan->ID_RUANGAN}">View</a>
        </td>
    </tr>
    {/for}
</table>