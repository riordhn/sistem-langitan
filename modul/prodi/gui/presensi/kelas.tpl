<div class="center_title_bar">Presensi Kelas - {$presensi_kelas->RUANGAN->NM_RUANGAN}</div>

<table>
    <tr>
        <th>No</th>
        <th>Nama Mahasiswa</th>
        <th>Kehadiran</th>
    </tr>
    {for $i=0 to $presensi_kelas->PRESENSI_MKMHSs->Count()-1}
    {$presensi_mkmhs=$presensi_kelas->PRESENSI_MKMHSs->Get($i)}
    <tr>
        <td class="center">{$i+1}</td>
        <td>{$presensi_mkmhs->MAHASISWA->PENGGUNA->NM_PENGGUNA}</td>
        <td>{$presensi_mkmhs->KEHADIRAN}</td>
    </tr>
    {/for}
</table>

<button href="presensi-kelas.php?mode=ruangan&id_ruangan={$presensi_kelas->ID_RUANGAN}">Kembali</button>