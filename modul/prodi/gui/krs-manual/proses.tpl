<div class="center_title_bar">KRS Manual</div>

{if $gagal}
    <div id='alert' class='ui-widget' style='margin-left:10px;'>
        <div class='ui-state-error ui-corner-all' style='padding: 50px;width:88%;'> 
            <p style='font-size:50px;'><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;margin-bottom:1em;'></span> 
                !!! Mahasiswa Tidak Diperbolehkan Mengambil <b>Lebih Dari 24 SKS Dalam Satu Semester</b>. Terima Kasih !!!</p>
        </div>
    </div>
{elseif $result}
	{if $approve == '0'}
    	<h3>Berhasil melakukan KRS manual {$jumlah_mhs} Mahasiswa untuk {$jumlah_kelas} kelas kuliah (belum approve)</h3>
    {else}
    	<h3>Berhasil melakukan KRS manual {$jumlah_mhs} Mahasiswa untuk {$jumlah_kelas} kelas kuliah (sudah approve)</h3>
	{/if}
{else}
    <h3>Gagal melakukan KRS manual</h3>
{/if}