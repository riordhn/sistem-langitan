<div class="center_title_bar">KRS Manual</div>

<form action="krs-manual.php" method="get">
    <table>
        <tbody>
            <tr>
                <td>Semester :
                    <select name="id_semester">
                        {foreach $semester_set as $s}
                            <option value="{$s.ID_SEMESTER}" 
                                    {if empty($smarty.get.id_semester)}
                                        {if $s.STATUS_AKTIF_SEMESTER == 'True'}selected{/if}
                                    {else}
                                        {if $s.ID_SEMESTER == $smarty.get.id_semester}selected{/if}
                                    {/if}
                                    >{$s.NM_SEMESTER} {$s.TAHUN_AJARAN} {if $s.STATUS_AKTIF_SEMESTER == 'True'}(AKTIF REG){/if} {if $s.STATUS_AKTIF_SP == 'True'} (AKTIF SP){/if}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    Angkatan :
                    <select name="thn_angkatan_mhs">
                        <option value="all">Semua Angkatan</option>
                        {foreach $thn_angkatan_set as $thn}
                            <option value="{$thn.THN_ANGKATAN_MHS}" {if !empty($smarty.get.thn_angkatan_mhs)}{if $thn.THN_ANGKATAN_MHS == $smarty.get.thn_angkatan_mhs}selected{/if}{/if}>{$thn.THN_ANGKATAN_MHS}</option>
                        {/foreach}
                    </select>
                </td>
                <td>
                    <input type="submit" value="Lihat"/>
                </td>
            </tr>
        </tbody>
    </table>
</form>
                    
<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Program Studi</th>
            <th>Mahasiswa</th>
            <th>Telah KRS</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        {foreach $prodi_set as $prodi}
            <tr>
                <td class="center">{$prodi@index + 1}</td>
                <td>{$prodi.NM_PROGRAM_STUDI}</td>
                <td class="center">{$prodi.JUMLAH_MHS}</td>
                <td class="center">{$prodi.JUMLAH_KRS}</td>
                <td>
                    <form action="krs-manual.php" method="get">
                        <input type="hidden" name="mode" value="pilih-mk" />
                        <input type="hidden" name="id_program_studi" value="{$prodi.ID_PROGRAM_STUDI}" />
                        <input type="hidden" name="id_semester" value="{$id_semester}" />
                        <button>Manual KRS</button>
                    </form>
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>