<div class="center_title_bar"> Info Pengisian Master Aktifitas (DM) </div>

<p>Langkah-langkah mengisi Master DM meliputi :</p>
<table width="660" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="38" bgcolor="#FFCC33"><div align="center">No</div></td>
    <td width="178" bgcolor="#FFCC33"><div align="center">Modul</div></td>
    <td width="434" bgcolor="#FFCC33"><div align="center">Uraian</div></td>
  </tr>
  <tr>
    <td>1</td>
    <td>Usulan Mata Ajar </td>
    <td>Difungsikan untuk menawarkan Mata Ajar yang akan diselenggarakan dalam satu semester, meliputi Mata Ajar dan Jadwal </td>
  </tr>
  <tr>
    <td>2</td>
    <td>PJMA</td>
    <td>Difungsikan untuk menentukan Pengampu Mata Ajar dan Team Teaching </td>
  </tr>
  <tr>
    <td>3</td>
    <td> Dosen Wali </td>
    <td>Difungsikan untuk menentukan perwalian Mahasiswa dan Manajemen Perwalian </td>
  </tr>
</table>


<p>{literal}
  <script>$('form').validate();</script>
{/literal}</p>
