<div class="center_title_bar">Ujian UAS Reguler - {$semester.TAHUN_AJARAN} {$semester.NM_SEMESTER} - Update Jadwal</div>

<a href="uas-reguler.php">Kembali ke daftar Ujian</a>

<form action="uas-reguler.php" method="post">
	<input type="hidden" name="mode" value="update" />
	<table>
		<thead>
			<tr>
				<th colspan="2">Informasi Jadwal Ujian</th>
			</tr>
		</thead>
		<tbody>
		{foreach item="data" from=$UMK}
			<input type="hidden" name="id_ujian_mk" value="{$data.ID_UJIAN_MK}" />
			<input type="hidden" name="id_jadwal_ujian_mk" value="{$data.ID_JADWAL_UJIAN_MK}" />
			<tr>
				<td>Mata Kuliah</td>
				<td>{$data.NM_MATA_KULIAH}</td>
			</tr>
			<tr>
				<td>Kelas</td>
				<td>{$data.NAMA_KELAS}</td>
			</tr>
			<tr>
				<td>Tingkat Semester</td>
				<td>{$data.TINGKAT_SEMESTER}</td>
			</tr>
			<tr>
				<td>Jenis Ujian</td>
				<td>
					<input type="text" name="nm_ujian_mk" value="{$data.NM_UJIAN_MK}" />
				</td>
			</tr>
			<tr>
				<td>Tanggal Ujian</td>
				<td>
					<input type="text" name="tgl_ujian" id="tgl_ujian" value="{$data.TGL_UJIAN}"/>
				</td>
			</tr>
			<tr>
				<td>Jam Mulai</td>
				<td>
					<input type="text" name="jam_mulai" value="{$data.JAM_MULAI}"/>
				</td>
			</tr>
			<tr>
				<td>Jam Selesai</td>
				<td>
					<input type="text" name="jam_selesai" value="{$data.JAM_SELESAI}"/>
				</td>
			</tr>
			<tr>
				<td>Ruangan - Kapasitas Ujian</td>
				<td>
					<select name="id_ruangan">
						{foreach item="ruangan" from=$T_RUANG}
							{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG selected=$data.ID_RUANGAN}
						{/foreach}
					</select>
				</td>
			</tr>
		{/foreach}
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<button onclick="window.location.href = '#ujian-ujianreguas!uas-reguler.php'; return false;">Batal</button>
					<input type="submit" value="Simpan" />
				</td>
			</tr>
		</tfoot>
	</table>
</form>

{literal}
<script language="text/javascript">
		$("#tgl_ujian").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
</script>
{/literal}