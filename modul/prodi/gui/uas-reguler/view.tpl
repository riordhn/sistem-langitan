<style type="text/css">
	input[type="submit"].link { 
		background: none; border: none; color: #008000;
		text-decoration: underline; cursor: pointer;
		padding: 0; font-size: 13px; text-align: left;
	}
	input[type="submit"].link:hover {
		color: #00aa00;
	}
	input[type="submit"].danger { color: red; }
</style>

<div class="center_title_bar">Ujian UAS Reguler - {$semester.TAHUN_AJARAN} {$semester.NM_SEMESTER}</div>

<a href="uas-reguler.php?mode=add">Tambah Jadwal UAS</a>
<input type="button" name="cetak" value="Cetak Jadwal UAS" onClick="window.open('proses/jadwal-ujian-cetak.php?cetak={$KEG}&smt={$SMT}','baru');">

<table>
	<thead>
		<tr>
			<th>Kode</th>
			<th>Mata Kuliah</th>
			<th>SKS</th>
			<th title="Kelompok Kelas">Kls</th>
			<th>Smt</th>
			<th>Jenis</th>
			<th>Tanggal</th>
			<th>Jam</th>
			<th>Ruang</th>
			<th title="Kapasitas">Kpt</th>
			<th title="Peserta">Pst</th>
			<th>Pengawas</th>
			<th colspan="2"></th>
		</tr>
	</thead>
	<tbody>
		{foreach $data_set as $data}
			<tr>
				<td>{$data.KD_MATA_KULIAH}</td>
				<td>{$data.NM_MATA_KULIAH}</td>
				<td class="center">{$data.KREDIT_SEMESTER}</td>
				<td>{$data.NAMA_KELAS}</td>
				<td class="center">{$data.TINGKAT_SEMESTER}</td>
				<td>{$data.NM_UJIAN_MK}</td>
				<td>{$data.TGL_UJIAN|date_format:"%A,%d-%b-%y"}</td>
				<td class="center">{$data.JAM}</td>
				<td class="center">{$data.NM_RUANGAN}</td>
				<td class="center">{$data.KAPASITAS_UJIAN}</td>
				<td class="center">
					{if $data.PESERTA > 0}
						{$data.PESERTA}
					{else}
						<form action="uas-reguler.php" method="post" style="display: inline">
							<input type="hidden" name="mode" value="plot-otomatis" />
							<input type="hidden" name="id_ujian_mk" value="{$data.ID_UJIAN_MK}" />
							<input type="submit" class="link" value="Plot Otomatis" />
						</form><br/>
						<a href="uas-reguler.php?mode=add-peserta&id_ujian_mk={$data.ID_UJIAN_MK}">Plot Manual</a>
					{/if}
				</td>
				<td>
					{$data.PENGAWAS}
				</td>
				<td>
					<ul>
						<li><a href="uas-reguler.php?mode=update&id_ujian_mk={$data.ID_UJIAN_MK}" title="Mengupdate info jadawl ujian">Update</a></li>
						{if $data.PESERTA > 0}
						<li>
							<form action="uas-reguler.php" method="post" style="display: inline">
								<input type="hidden" name="mode" value="reset-peserta" />
								<input type="hidden" name="id_ujian_mk" value="{$data.ID_UJIAN_MK}" />
								<input type="submit" class="link" value="Reset Peserta" onclick="return confirm('Anda akan menghapus semua mahasiswa dr jadwal ?');"/>
							</form>
						</li>
						{/if}
					</ul>
				</td>
				<td>
					<ul>
						<li>
							<form action="uas-reguler.php" method="post" style="display: inline">
								<input type="hidden" name="mode" value="add-ruang" />
								<input type="hidden" name="id_ujian_mk" value="{$data.ID_UJIAN_MK}" />
								<input type="hidden" name="id_kelas_mk" value="{$data.ID_KELAS_MK}" />
								<input type="submit" class="link" value="Tambah Ruang" />
							</form>
						</li>
						<li>
							<form action="uas-reguler.php" method="post" style="display: inline">
								<input type="hidden" name="mode" value="delete" />
								<input type="hidden" name="id_ujian_mk" value="{$data.ID_UJIAN_MK}" />
								<input type="submit" class="link danger" value="Hapus" onclick="return confirm('Anda akan menghapus jadwal ujian ini ?');"/>
							</form>
						</li>
					</ul>
				</td>
			</tr>
		{foreachelse}
			<tr>
				<td colspan="12">Jadwal UAS belum di plotting</td>
			</tr>
		{/foreach}
	</tbody>
</table>