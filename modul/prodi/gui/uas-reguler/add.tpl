<div class="center_title_bar">Ujian UAS Reguler - {$semester.TAHUN_AJARAN} {$semester.NM_SEMESTER} - Tambah Jadwal UAS</div>

<a href="uas-reguler.php">Kembali ke daftar Ujian</a>

<form method="post" action="uas-reguler.php">
	<input type="hidden" name="mode" value="add" />
	<table>
		<thead>
			<tr>
				<th></th>
				<th>Kode</th>
				<th>Mata Kuliah</td>
				<th>SKS</th>
				<th title="Kelompok Kelas">Kls</th>
				<th title="Tingkat Semester">Smt</th>
				<th>Hari</th>
				<th>Ruang</th>
				<th>Kapasitas</th>
				<th>Peserta</th>
			</tr>
		</thead>
		<tbody>
			{foreach $data_set as $data}
				<tr>
					<td><input type="checkbox" name="id_gabungan[]" value="{$data.ID_KELAS_MK}-{$data.ID_JADWAL_KELAS}" /></td>
					<td>{$data.KD_MATA_KULIAH}</td>
					<td>{$data.NM_MATA_KULIAH}</td>
					<td class="center">{$data.KREDIT_SEMESTER}</td>
					<td class="center">{$data.NAMA_KELAS}</td>
					<td class="center"></td>
					<td class="center">{$data.NM_JADWAL_HARI}</td>
					<td class="center">{$data.NM_RUANGAN}</td>
					<td class="center">{$data.KAPASITAS_UJIAN}</td>
					<td class="center">{$data.PESERTA}</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="10">Data tidak ada</td>
				</tr>
			{/foreach}
		</tbody>
		<tfoot>
			<tr>
				<td colspan="10">
					<input type="submit" value="Tambahkan" />
				</td>
			</tr>
		</tfoot>
	</table>
</form>