{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable").tablesorter(
		{
		sortList: [[1,0], [3,0]],
		headers: {
     5: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="center_title_bar">Yudisium Supro I</div>
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Proses Yudisium</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1} ">

<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="5%">No</th>
			<th width="10%">Nim</th>
			<th width="30%">Nama Mhs</th>
			<th width="10%">Status</th>
			<th width="10%">Keterangan</th>
			<th class="noheader" width="12%">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=test item="list" from=$T_MK}
		<tr>
			<td></td>
			<td></td>
			<td></center></td>
			<td><center></center></td>
			<td></td>
			<td><center><a href="#">Proses</a></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
</table>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable1").tablesorter(
		{
		sortList: [[1,0], [5,0]],
		headers: {
      7: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="panel" id="panel2" style="display: {$disp2}">

<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="10%">Nim</th>
			<th width="20%">Nama Mhs</th>
			<th width="7%">Kode</th>
			<th width="30%">Nama Mata Ajar</th>
			<th width="5%">SKS</th>
			<th width="10%">Smt</th>
			<th width="5%">Nilai</th>
			<th class="noheader" width="12%">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=test item="list" from=$TAWAR}
		<tr>
			<td></td>
			<td></td>
			<td><center></center></td>
			<td><center></center></td>
			<td></td>
			<td></td>
			<td></td>
			<td><center><a href="#">Lulus</a></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>
</div>
