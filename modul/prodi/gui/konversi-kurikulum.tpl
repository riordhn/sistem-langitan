<div class="center_title_bar">Konversi Kelulusan </div>

<div id="tabs">
		<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
		<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
		<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>

   	</div>

		
   <div class="tab_bdr"></div>

   <div class="panel" id="panel1" style="display: {$disp1}"> 
   <form action="konversi-kurikulum.php" method="post">
 <input type="hidden" name="kirim" value="tampil" > 
<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Tahun Kurikulum : 
                <select name="thkur">
    		   <option value=''>-- THN KUR --</option>
	 		   {foreach item="kur" from=$T_KUR}
    		   {html_options  values=$kur.ID_KURIKULUM output=$kur.NAMA}
	 		   {/foreach}
			   </select>
			   <input type="submit" name="View" value="View">
		     </td> 
           </tr>
</table>
</form>	  	   
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr class="left_menu">
              <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Thn Kur</font></td>
              <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Kode MK </font></td>
              <td width="20%" bgcolor="#333333"><font color="#FFFFFF">Nama MK </font> </td>
              <td width="5%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
              <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Thn Kur </font></td>
              <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Kode MK </font></td>
              <td width="20%" bgcolor="#333333"><font color="#FFFFFF">Nama MK </font></td>
              <td width="5%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
			  <td width="15%" bgcolor="#333333"><font color="#FFFFFF">AKSI</font></td>
            </tr>
			
			  {foreach item="konv" from=$T_KONV}
            <tr>
              <td >{$konv.THBARU}</td>
              <td >{$konv.KODEBARU}</td>
              <td >{$konv.NAMABARU}</td>
              <td >{$konv.SKSBARU}</td>
              <td >{$konv.THLAMA}</td>
              <td >{$konv.KODELAMA}</td>
              <td >{$konv.NAMALAMA}</td>
              <td >{$konv.SKSLAMA}</td>
			  <td ><a href="konversi-kurikulum.php?kirim=updateview&id_kon={$konv.ID_KONVERSI_KURIKULUM}" onclick="displayPanel('3')">Update</a> | <a href="konversi-kurikulum.php?kirim=del&id_kon={$konv.ID_KONVERSI_KURIKULUM}&mk={$konv.KODEBARU}">Delete</a></td>
 </td>
            </tr>
			{foreachelse}
        <tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
</table>
  </div>
  
  <div class="panel" id="panel2" style="display: {$disp2}">
  <form action="konversi-kurikulum.php" method="post" >
				  <input type="hidden" name="kirim" value="add" >
				  <table class="tb_frame" width="80%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
					  <td width="20%">MATA KULIAH BARU</td>
					  <td width="2%">:</td>
					  <td width="78%">
                        <select name="mkbaru" id="mk_baru" size="1">
                    <option value="">-- PILIH MK BARU --</option>
                        {foreach item="mkbaru" from=$MKBARU}
    		   			{html_options  values=$mkbaru.ID_KURIKULUM_MK output=$mkbaru.MKBARU}
	 		   			{/foreach}
                         </select>                    
					</tr>
					<tr>
					  <td width="20%">MATA KULIAH LAMA</td>
					  <td width="2%">:</td>
					  <td width="78%">
                        <select name="mklama" id="mk_lama" size="1" >
                    <option value="">-- PILIH MK LAMA --</option>
                        {foreach item="mklama" from=$MKLAMA}
    		   			{html_options  values=$mklama.ID_KURIKULUM_MK output=$mklama.MKLAMA}
	 		   			{/foreach}
                         </select>                    
					</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="ttambah" value="Simpan" id="ttambah"></td>
					</tr>
		  		  </table>
			</form>	
  </div>
  
  <div class="panel" id="panel3" style="display: {$disp3}">
  <form action="konversi-kurikulum.php" method="post" >
				  <input type="hidden" name="kirim" value="ganti" >
				  <table class="tb_frame" width="80%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
					{foreach item="konv" from=$T_KONV1}
						<input type="hidden" name="idkonv" value="{$konv.ID_KONVERSI_KURIKULUM}" >
					  <td width="20%">MATA KULIAH BARU</td>
					  <td width="2%">:</td>
					  <td width="78%">
                        <select name="mkbaru" id="mk_baru" size="1">
                        {foreach item="mkbaru" from=$MKBARU}
    		   			{html_options  values=$mkbaru.ID_KURIKULUM_MK output=$mkbaru.MKBARU selected=$konv.ID_KURIKULUM_MK_BARU}
	 		   			{/foreach}
                         </select>                    
					</tr>
					<tr>
					  <td width="20%">MATA KULIAH LAMA</td>
					  <td width="2%">:</td>
					  <td width="78%">
                        <select name="mklama" id="mk_lama" size="1" >
                        {foreach item="mklama" from=$MKLAMA}
    		   			{html_options  values=$mklama.ID_KURIKULUM_MK output=$mklama.MKLAMA selected=$konv.ID_KURIKULUM_MK_LAMA}
	 		   			{/foreach}
                         </select>                    
					</tr>
					{/foreach}
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="Update" value="Update" id="Update"></td>
					</tr>
		  		  </table>
			</form>	
  </div>
<script>$('form').validate();</script>
        