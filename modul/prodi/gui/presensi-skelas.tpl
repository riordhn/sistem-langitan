{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">

/*$(document).ready(function($){
	$('#dosensearch').autocomplete({source:'dosen-autocomple.php', minLength:2});
	});*/
	
$(document).ready(function($)
{
  $("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
      3: { sorter: false }
		}
		}
	);
	
});

	var i = $('input type="text"').size() + 2;

	$('#addButton').click(function() {


		 $('#tableText tr:last').after('<tr><td>Mahasiswa ' + i + '</td>' +
		  '<td>:</td>' +
		  '<td>' +
          ' <select name="select"> ' +
                  '<option value"">-- Mahasiswa --</option>' +
				  '    {foreach $dosen_pengguna as $dosen}'+
               	  '			<option value="{$dosen.id_dosen}">{$dosen.nama_dosen}</option>' +
             	  '	   {/foreach}'+
          ' </select>' +
          '</td></tr>').
			animate({ opacity: "show" }, "slow");
		i++;
	});


	function ubah(ID_MHS){
		$.ajax({
		type: "POST",
        url: "presensi-skelas.php",
        data: "action=savewali&dosen="+$("#id_dosen"+ID_MHS).val()+"&id_mhs="+$("#id_mhs"+ID_MHS).val(),
        cache: false,
        success: function(data){
			confirm("Yakin Untuk Menyimpan?");
			$('#content').html(data);
        }
        });
	}
	function update(ID_MHS){
		$.ajax({
		type: "POST",
        url: "presensi-skelas.php",
        data: "action=update&dosen="+$("#id_dosen1").val()+"&id_mhs="+$("#id_mhs"+ID_MHS).val(),
        cache: false,
        success: function(data){
			confirm("Yakin Untuk Menyimpan?");
			$('#content').html(data);
        }
        });
	}
	function cariDosen(){
		$.ajax({
		type: "POST",
        url: "dosen-cari.php",
        data: "dosen="+$("#caridosen").val(),
        cache: false,
        success: function(data){
			$('#contentCariDosen').html(data);
        }
        });
	}
	
</script>
{/literal}

<div class="center_title_bar">Penetapan Kelas Mahasiswa </div>

<div id="tabs">
	<div id="tab1" {if $smarty.request.action == 'savewali'}class="tab"{else if $smarty.get.action == 'tampil' or $smarty.get.action == ''}class="tab_sel"{else}class="tab"{/if} align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" {if $smarty.request.action == 'savewali'}class="tab_sel"{else}class="tab"{/if} align="center" onclick="javascript: displayPanel('2');">Mhs Tanpa Kelas</div>
	<div id="tab3" {if $smarty.get.action == 'viewupdate'}class="tab_sel"{else}class="tab"{/if}  align="center" onclick="javascript: displayPanel('3');">Update</div>
	<div id="tab4" {if $smarty.get.action == ''}class="tab"{else}class="tab"{/if}  align="center" onclick="javascript: displayPanel('4');">Dist. Kelas</div>
	<div id="tab5" {if $smarty.get.action == 'detailwali'}class="tab_sel"{else}class="tab"{/if}  align="center" onclick="javascript: displayPanel('5');">Detil Kelas</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table>
	<tr bgcolor="#efefef">
{foreach item="agk" from=$ANG}
		<td><a href="presensi-skelas.php?action=tampil&angk={$agk.ANGKATAN}"><strong>{$agk.ANGKATAN}</strong></a></td>
{/foreach}
	</tr>
</table>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			
			<th>NIM</th>
			<th>Nama Mahasiswa</th>
			<th>Kelas</th>
			<th class="noheader">Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="wali" from=$WALI}
		<tr>
			
			<td>{$wali.NIM_MHS}</td>
			<td>{$wali.MHS}</td>
			<td>{$wali.DOLI}</td>
			<td><center><a href="presensi-skelas.php?action=viewupdate&id_mhs={$wali.ID_MHS}">Update</a></center></td>
		</tr>
	{foreachelse}
		<tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>


<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="presensi-skelas.php" method="post" >
<!--
<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>Pilih Dosen Wali :</td>
			<td>            	
                <input type="text" name="caridosen" id="caridosen" onkeydown="cariDosen()" />  
                <input type="button" value="Cari Dosen"  onclick="cariDosen()" />    
                         
			</td>
		</tr>
</table>
<p id="contentCariDosen"></p>
-->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>Pilih Kelas :</td>
			<td>
				<select name="dosen" id="id_dosen">
					<option value="">-- Pilih Kelas --</option>
					{foreach item="kelas" from=$KELAS}
					{html_options values=$kelas.ID_NAMA_KELAS output=$kelas.NAMA_KELAS}
					{/foreach}
				</select>
			</td>
			<!-- <td><input type="submit" name="PROSES" value="PROSES"></td> -->
		</tr>
</table>

<input type="hidden" name="action" value="savewali" >

<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="5%" bgcolor="#FFCC33">ANGKATAN</td>
			<td width="10%" bgcolor="#FFCC33">NIM</td>
			<td width="25%" bgcolor="#FFCC33">Nama Mahasiswa</td>
			<td width="25%" bgcolor="#FFCC33">Penerimaan</td>
			<td width="5%" bgcolor="#FFCC33">Pilih</td>
		</tr>

		{foreach name=nokelas item="mhs_nokelas" from=$NOKELAS}
		<tr>
			<td>{$mhs_nokelas.THN_ANGKATAN_MHS}</td>
			<td>{$mhs_nokelas.NIM_MHS}</td>
			<td>{$mhs_nokelas.NM_PENGGUNA}</td>
			<td>{$mhs_nokelas.NM_PENERIMAAN}</td>
			<td><center><input name="mhs{$smarty.foreach.nokelas.iteration}" type="checkbox" value="{$mhs_nokelas.ID_MHS}" {$mhs_nokelas.TANDA} /></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
		<input type="hidden" name="counter1" value="{$smarty.foreach.nokelas.iteration}" >
				<tr>
				<td colspan="4"></td><td><input type="submit" name="PROSES" value="PROSES"></td>
				</tr>
</table>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
</p> </p>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>NIM</th>
			<th>Nama Mahasiswa</th>
			<th>Nama Kelas</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=nokls item="mhs_kls" from=$KLS}
	<input type="hidden" value="{$mhs_kls.ID_MHS}" name="mhs" id="id_mhs{$mhs_kls.ID_MHS}" />
		<tr>
			<td>{$mhs_kls.NIM_MHS}</td>
			<td>{$mhs_kls.MHS}</td>
			<td>
				<select name="dosen" id="id_dosen1" onChange="update({$mhs_kls.ID_MHS})">
					<option value="">-- Pilih Kelas --</option>
					{foreach item="kelas" from=$KELAS}
					{html_options values=$kelas.ID_NAMA_KELAS output=$kelas.NAMA_KELAS}
					{/foreach}
				</select>
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>




<div class="panel" id="panel4" style="display: {$disp4}">
<p> </p>
<table id="myTable2" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Angkatan</th>
			<th>Nama Kelas</th>
			<th>Total Mahasiswa</th>
			<!-- <th>Aksi</th> -->
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="seb" from=$SEBKELAS}
		<tr>
			<td>{$seb.THN_ANGKATAN_MHS}</td>
			<td>{$seb.NAMA_KELAS}</td>
			<td><center><a href="presensi-skelas.php?action=detailwali&dosen={$seb.ID_NAMA_KELAS}&angkatan={$seb.THN_ANGKATAN_MHS}">{$seb.TTL}</a></center></td>
			<!-- <td><center><input type="button" name="cetak" value="Cetak" onclick="window.open('proses/cetak-doli.php?cetak={$seb.ID_DOSEN}','baru');"></center></td> -->
		</tr>
	{foreachelse}
		<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel5" style="display: {$disp5}">
<p> </p>
<p><strong>Daftar Mahasiswa Untuk Kelas {$NMDSN} Angkatan {$ANGKATAN}</strong></p>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>NIM</th>
			<th>Nama Mahasiswa</th>
			<th>Program Studi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="det" from=$DET}
		<tr>
			<td>{$det.NIM_MHS}</td>
			<td>{$det.MHS}</td>
			<td>{$det.PRODI}</td>
		</tr>
	{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>




{literal}
 <script>$('form').validate();</script>
{/literal}

