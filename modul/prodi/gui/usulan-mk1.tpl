<div id="usulan-mk">
   <div class="center_title_bar">Usulan / Penawaran Mata Ajar</div>  
       
        <div id="tabs">
        <div id="tab1" {if $smarty.get.action == ''}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('1');"><a href="usulan-mk.php" style="color:#000000;text-decoration:none;cursor:pointer">Rincian </a></div>
        <div id="tab2" {if $smarty.get.action == 'penawaran'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');"><a href="usulan-mk.php?action=penawaran" style="color:#000000;text-decoration:none;cursor:pointer">Penawaran</a></div>
		<div id="tab3" {if $smarty.get.action == 'updateview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>
		<div id="tab4" {if $smarty.get.action == 'adkelview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('4');">Tambah Kelas</div>
		<div id="tab5" {if $smarty.get.action == 'adhariview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('5');">Tambah Hari</div>
    	</div>
        <div class="tab_bdr"></div>
		
<div class="panel" id="panel1" style="display: {$disp1} ">
 <form action="usulan-mk.php" method="post">
		 <input type="hidden" name="action" value="tampil" >  
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Tahun Akademik : 
                <select name="smt">
    		   <option value=''>-- PILIH THN AKD --</option>
	 		   {foreach item="smt" from=$T_ST}
    		   {html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$smtaktif}
	 		   {/foreach}
			   </select>
		     </td> 
          
			 <td> <input type="submit" name="View" value="View"> </td>
           </tr>
		    
</table>
</form>	    
			<table width="90%" border="0" cellspacing="0" cellpadding="0">
           <tr class="left_menu" align="center">
             <td width="4%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
             <td width="5%" bgcolor="#333333"><font color="#FFFFFF">Kode</font></td>
             <td width="33%" bgcolor="#333333"><font color="#FFFFFF">Nama  Mata Ajar</font> </td>
             <td width="5%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
			 <td width="5%" bgcolor="#333333"><font color="#FFFFFF">KLS</font></td>
             <td width="7%" bgcolor="#333333"><font color="#FFFFFF">Hari</font></td>
			 <td width="7%" bgcolor="#333333"><font color="#FFFFFF">Jam</font></td>
			 <td width="12%" bgcolor="#333333"><font color="#FFFFFF">Ruangan</font></td>
             <td width="30%" bgcolor="#333333"><font color="#FFFFFF">Aksi</font></td>
           </tr>
           {foreach name=test item="list" from=$T_MK}
		   {if $list.NM_JADWAL_HARI == "" || $list.JAM == ""}
		   <tr bgcolor="yellow">
		   {else}
		   <tr>
		   {/if}
             <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td>{$list.KREDIT_SEMESTER}</td>
			 <td>{$list.NAMA_KELAS}</td>
             <td>{$list.NM_JADWAL_HARI}</td>
			 <td>{$list.JAM}</td>
			 <td>{$list.NM_RUANGAN}</td>
             <td align="center">
			 {if $kdf==7}
			 <a href="usulan-mk.php?action=del&id_klsmk={$list.ID_KELAS_MK}">Del</a><br>
			 <a href="usulan-mk.php?action=adkelview&id_klsmk={$list.ID_KELAS_MK}&smt={$list.ID_SEMESTER}">Add Kls</a>
			 {else}
			 <a href="usulan-mk.php?action=updateview&id_klsmk={$list.ID_KELAS_MK}">Upd</a>
			 &nbsp;|&nbsp;
			 <a href="usulan-mk.php?action=del&id_klsmk={$list.ID_KELAS_MK}">Del</a><br>
			 <a href="usulan-mk.php?action=adkelview&id_klsmk={$list.ID_KELAS_MK}&smt={$list.ID_SEMESTER}">Add Kls</a>
			 &nbsp;|&nbsp;
			 <a href="usulan-mk.php?action=adhariview&id_klsmk={$list.ID_KELAS_MK}&smt={$list.ID_SEMESTER}">Add Hari</a>
			 {/if}
             </td>
           </tr>
		   {foreachelse}
        <tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
   	 </table>
	 <br>
</div>
		
<div class="panel" id="panel2" style="display: {$disp2}">
<form action="usulan-mk.php" method="post" >
		 <input type="hidden" name="action" value="penawaran" >  
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td>
			 	Tahun Akademik : 
                <select name="smt">
    		   <option value=''>-- PILIH THN AKD --</option>
	 		   {foreach item="smt" from=$T_ST}
    		   {html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$smtaktif}
	 		   {/foreach}
			   </select>
		     </td> 
          
             <td>
			 	Tahun Kurikulum : 
                <select name="thkur">
    		   <option value=''>-- THN KUR --</option>
	 		   {foreach item="kur" from=$T_KUR}
    		   {html_options  values=$kur.ID_KURIKULUM_PRODI output=$kur.NAMA selected=$idkur}
	 		   {/foreach}
			   </select>
		     </td> 
			 <td> <input type="submit" name="View" value="View"> </td>
           </tr>
		    
</table>
</form>	    
			<table width="90%" border="0" cellspacing="0" cellpadding="0">
           <tr class="left_menu" align="center">
             <td width="4%" bgcolor="#333333"><font color="#FFFFFF">No</font></td>
             <td width="20%" bgcolor="#333333"><font color="#FFFFFF">Kode MK </font></td>
             <td width="33%" bgcolor="#333333"><font color="#FFFFFF">Nama  Mata Ajar</font> </td>
             <td width="10%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
             <td width="12%" bgcolor="#333333"><font color="#FFFFFF"> Tahun Kur </font></td>
			 <td width="12%" bgcolor="#333333"><font color="#FFFFFF"> Status</font></td>
             <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Aksi</font></td>
           </tr>
           <tbody id="tableBody">
		  
           {foreach name=test item="list" from=$TAWAR}
		   <tr>
             <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td>{$list.KREDIT_SEMESTER}</td>
             <td>{$list.TAHUN_KURIKULUM}</td>
			 <td>{$list.KETERANGAN}</td>
             <td align="center"><a href="usulan-mk.php?action=add&id_mk={$list.ID_KURIKULUM_MK}">Di Tawarkan</a>
             </td>
           </tr>
		   {foreachelse}
        <tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
   	 </table>
	 <br>
        </div>
		
<div class="panel" id="panel3" style="display: {$disp3}">
		
		<form action="usulan-mk.php" method="post">
			 <input type="hidden" name="action" value="update1" >
			  <table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
				  <tr>
				 
					  <td colspan="3"><strong> Lengkapi Kelas </strong></td>
				    </tr>
					{foreach item="ubah" from=$TJAF1}
					<input type="hidden" name="id_kelas_mk" value="{$ubah.ID_KELAS_MK}" />
				  	<input type="hidden" name="smtb" value="{$ubah.ID_SEMESTER}" />
					<tr>
					  <td width="29%" >Kode Mata Ajar</td>
					  <td width="2%" >:</td>
					  <td width="69%" >{$ubah.KD_MATA_KULIAH}</td>
					</tr>
					<tr>
					  <td>Nama Mata Ajar</td>
					  <td>:</td>
					  <td>{$ubah.NM_MATA_KULIAH}</td>
					</tr>					
					<tr>
					  <td>Kelas MK [A/B/C/D]</td>
					  <td>:</td>
					  <td><select name="kelas_mk" id="kelas_mk">
                            {foreach item="namakls" from=$NM_KELAS}
    		   				{html_options  values=$namakls.ID_NAMA_KELAS output=$namakls.NAMA_KELAS selected=$ubah.NO_KELAS_MK}
	 		   				{/foreach}
                          </select>
					  </td>
				    </tr>
					<tr>
					  <td>KAPASITAS KELAS</td>
					  <td>:</td>
					  <td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$ubah.KAPASITAS_KELAS_MK}"/> [WAJIB DI-ISI]</td>
				    </tr>
					<tr>
					  <td>Rencana Perkuliahan (Tatap Muka)</td>
					  <td>:</td>
					  <td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$ubah.JUMLAH_PERTEMUAN_KELAS_MK}"/> [WAJIB DI-ISI]</td>
				    </tr>
					
					<tr>
					  <td>Hari</td>
					  <td>:</td>
					  <td>
                      	<select name="hari" id="hari">
                            {foreach item="hari" from=$T_HARI}
    		   				{html_options  values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI selected=$ubah.ID_JADWAL_HARI}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					<tr>
					  <td>Jam</td>
					  <td>:</td>
					  <td>
                      	<select name="jam" id="jam">
                            {foreach item="jam" from=$T_JAM}
    		   				{html_options  values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$ubah.ID_JADWAL_JAM}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					<tr>
					  <td>Ruangan</td>
					  <td>:</td>
					  <td>
                      	<select name="ruangan" id="ruangan">
                            {foreach item="ruangan" from=$T_RUANG}
    		   				{html_options  values=$ruangan.ID_RUANGAN output=$ruangan.RUANG selected=$ubah.ID_RUANGAN}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					
					<tr>
					{/foreach}

                  <p>
				  
					<input type="submit" name="Simpan1" value="Simpan">
				
                </p></td>
            </tr>
          </table>
 </form>			    
        </div>
		
		<div class="panel" id="panel4" style="display: {$disp4}">
		
		<form action="usulan-mk.php" method="post">
			 <input type="hidden" name="action" value="tambahkelas" >
			  <table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
				  <tr>
				 
					  <td colspan="3"><strong> Tambah Kelas </strong></td>
				    </tr>
					{foreach item="tambah" from=$TJAF1}
					<input type="hidden" name="id_kur_mk" value="{$tambah.ID_KURIKULUM_MK}" />
				  	<input type="hidden" name="smtb" value="{$tambah.ID_SEMESTER}" />
					<tr>
					  <td width="29%" >Kode Mata Ajar</td>
					  <td width="2%" >:</td>
					  <td width="69%" >{$tambah.KD_MATA_KULIAH}</td>
					</tr>
					<tr>
					  <td>Nama Mata Ajar</td>
					  <td>:</td>
					  <td>{$tambah.NM_MATA_KULIAH}</td>
					</tr>					
					<tr bgcolor="yellow">
					  <td>Kelas MK [A/B/C/D]</td>
					  <td>:</td>
					  <td><select name="kelas_mk" id="kelas_mk">
							<option value=''>------</option>
                            {foreach item="namakls" from=$NM_KELAS}
    		   				{html_options  values=$namakls.ID_NAMA_KELAS output=$namakls.NAMA_KELAS}
	 		   				{/foreach}
                          </select>
</td>
				    </tr>
					<tr>
					  <td>KAPASITAS KELAS</td>
					  <td>:</td>
					  <td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambah.KAPASITAS_KELAS_MK}"/></td>
				    </tr>
					<tr>
					  <td>Rencana Perkuliahan</td>
					  <td>:</td>
					  <td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambah.JUMLAH_PERTEMUAN_KELAS_MK}"/></td>
				    </tr>
					
					<tr>
					  <td>Hari</td>
					  <td>:</td>
					  <td>
                      	<select name="hari" id="hari">
                            {foreach item="hari" from=$T_HARI}
    		   				{html_options  values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI selected=$ubah.ID_JADWAL_HARI}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					<tr>
					  <td>Jam</td>
					  <td>:</td>
					  <td>
                      	<select name="jam" id="jam">
                            {foreach item="jam" from=$T_JAM}
    		   				{html_options  values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$ubah.ID_JADWAL_JAM}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					<tr bgcolor="yellow">
					  <td>Ruangan</td>
					  <td>:</td>
					  <td>
                      	<select name="ruangan" id="ruangan">
							<option value=''>------</option>
                            {foreach item="ruangan" from=$T_RUANG}
    		   				{html_options  values=$ruangan.ID_RUANGAN output=$ruangan.RUANG}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					
					<tr>
					{/foreach}

                  <p>
				  
					<input type="submit" name="Simpan1" value="Tambah Kelas">
				
                </p></td>
            </tr>
          </table>
 </form>			    
        </div>

		<div class="panel" id="panel5" style="display: {$disp5}">
		
		<form action="usulan-mk.php" method="post">
			 <input type="hidden" name="action" value="tambahhari" >
			  <table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
				  <tr>
				 
					  <td colspan="3"><strong> Tambah Hari </strong></td>
				    </tr>
					{foreach item="tambah" from=$TJAF2}
					<input type="hidden" name="id_kelas_mk" value="{$tambah.ID_KELAS_MK}" />
				  	<input type="hidden" name="smtb" value="{$tambah.ID_SEMESTER}" />
					<tr>
					  <td width="29%" >Kode Mata Ajar</td>
					  <td width="2%" >:</td>
					  <td width="69%" >{$tambah.KD_MATA_KULIAH}</td>
					</tr>
					<tr>
					  <td>Nama Mata Ajar</td>
					  <td>:</td>
					  <td>{$tambah.NM_MATA_KULIAH}</td>
					</tr>					
					<tr>
					  <td>Kelas MK [A/B/C/D]</td>
					  <td>:</td>
					  <td><select name="kelas_mk" id="kelas_mk">
                            {foreach item="namakls" from=$NM_KELAS}
    		   				{html_options  values=$namakls.ID_NAMA_KELAS output=$namakls.NAMA_KELAS}
	 		   				{/foreach}
                          </select>
</td>
				    </tr>
					<tr>
					  <td>KAPASITAS KELAS</td>
					  <td>:</td>
					  <td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambah.KAPASITAS_KELAS_MK}"/></td>
				    </tr>
					<tr>
					  <td>Rencana Perkuliahan</td>
					  <td>:</td>
					  <td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambah.JUMLAH_PERTEMUAN_KELAS_MK}"/></td>
				    </tr>
					
					<tr bgcolor="yellow">
					  <td>Hari</td>
					  <td>:</td>
					  <td>
                      	<select name="hari" id="hari">
							<option value=''>------</option>
                            {foreach item="hari" from=$T_HARI}
    		   				{html_options  values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI selected=$ubah.ID_JADWAL_HARI}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					<tr>
					  <td>Jam</td>
					  <td>:</td>
					  <td>
                      	<select name="jam" id="jam">
                            {foreach item="jam" from=$T_JAM}
    		   				{html_options  values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$ubah.ID_JADWAL_JAM}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					<tr>
					  <td>Ruangan</td>
					  <td>:</td>
					  <td>
                      	<select name="ruangan" id="ruangan">
                            {foreach item="ruangan" from=$T_RUANG}
    		   				{html_options  values=$ruangan.ID_RUANGAN output=$ruangan.RUANG selected=$ubah.ID_RUANGAN}
	 		   				{/foreach}
                          </select>
                      </td>
				    </tr>
					
					<tr>
					{/foreach}

                  <p>
				  
					<input type="submit" name="Simpan2" value="Tambah Hari">
				
                </p></td>
            </tr>
          </table>
 </form>			    
	      <br>
        </div>
</div>