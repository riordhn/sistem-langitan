<div class="center_title_bar"> Info Pengisian Master Akademik (Prodi) </div>

<p>Langkah-langkah mengisi Master Akademik meliputi :</p>
<table width="660" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="38" bgcolor="#FFCC33"><div align="center">No</div></td>
    <td width="178" bgcolor="#FFCC33"><div align="center">Modul</div></td>
    <td width="434" bgcolor="#FFCC33"><div align="center">Uraian</div></td>
  </tr>
  <tr>
    <td>1</td>
    <td>Kurikulum</td>
    <td>Master kurikulum untuk pembuatan kurikulum prodi</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Setup Kurikulum Prodi</td>
    <td>Memilih nama kurikulum dan mengisi kelengkapannya spt SK, Tahun dan lainnya yang berlaku di Prodi Tersebut </td>
  </tr>
  <tr>
    <td>3</td>
    <td> Daftar Mata Ajar</td>
    <td>Difungsikan untuk mengisi mata ajar dalam kurikulum tertentu. Semua Kurikulum aktif yang dipakai harus diisi Mata Ajar-nya </td>
  </tr>
  <tr>
    <td>4</td>
    <td>Konversi Kurikulum </td>
    <td>Difungsikan untuk mengisi data transformasi Mata Ajar antar Kurikulum </td>
  </tr>
  <tr>
    <td>5</td>
    <td>Minat Prodi </td>
    <td>Difungsikan untuk mengisi Peminatan yang ada dala suatu Prodi </td>
  </tr>
  <tr>
    <td>6</td>
    <td>Paket Prodi </td>
    <td>Fungsi ini khusus untuk D3, FK dan S2 yang menawarkan sistem perkuliahan secara khusus. </td>
  </tr>
</table>


<p>{literal}
  <script>$('form').validate();</script>
{/literal}</p>
