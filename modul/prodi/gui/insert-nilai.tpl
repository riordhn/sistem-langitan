
<div class="center_title_bar">Proses Insert Nilai [Internal]</div> 

<p> </p>
<form action="insert-nilai.php" method="get">
        <table>
            <tr>
                <td>NIM Mahasiswa</td>
                <td><input type="text" name="s"/></td>
                <td><input type="submit" value="Cari" /></td>
            </tr>
        </table>
</form>
<!-- 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
			 <td width="2%" bgcolor="#FFCC33"><center>No</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>NIM</center></td>
			 <td width="20%" bgcolor="#FFCC33"><center>Nama Mhs</center></td>
			 <td width="15%" bgcolor="#FFCC33"><center>Prodi</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>Total MK Diakui</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>Total SKS Diakui</center></td>
			 <td width="5%" bgcolor="#FFCC33"><center>AKSI</center></td>
		  </tr>
		  {foreach name=test item="list" from=$detail_transfer}
			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.NIM_MHS}</td>
			 <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.PRODI}</td>
             <td>{$list.TOTAL_MATKUL}</td>
			 <td>{$list.TOTAL_SKS}</td>
			 <td height="0" align="center"><input name="Button" type="button" class="button" onClick="location.href='#aktivitas-transfernil!nilai-transfer.php?action=viewproses&id_mhs={$list.ID_MHS}&smt={$list.SEMESTER_PROSES}';disableButtons()" onMouseOver="window.status='Click untuk Insert';return true" onMouseOut="window.status='Edit Nilai'" value="INSERT NILAI" /> </td> 
			</tr>
		  {/foreach}
</table> -->

{if isset($datamhs)}
<p> </p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>NIM</td>
      <td>:</td>
      <td>{$datamhs.NIM_MHS}</td>
    </tr>
    <tr>
      <td>Nama </td>
      <td>:</td>
      <td>{$datamhs.NM_PENGGUNA}</td>
    </tr>
    <tr>
      <td>Program Studi </td>
      <td>:</td>
      <td>{$datamhs.NM_PROGRAM_STUDI}</td>
    </tr>
    <tr>
      <td>Angkatan </td>
      <td>:</td>
      <td>{$datamhs.THN_ANGKATAN_MHS}</td>
    </tr>
    <tr>
      <td>Status Kelas</td>
      <td>:</td>
      <td>{$datamhs.NAMA_KELAS}</td>
    </tr>

</table>

        <form action="insert-nilai.php?s={$datamhs.NIM_MHS}" method="post">
            <table>
                <tbody>
                    <tr>
                        <td>
                            <label><strong>Kurikulum</strong></label>
                        </td>
                        <td>
                            <label><strong>:</strong></label>
                        </td>
                        <td>
                            <select name="id_kurikulum" onchange="javascript: $(this).submit();">
                                <option value="">-- Pilih Kurikulum --</option>
                                {foreach $kurikulum_set as $kur}
                                    <option value="{$kur.ID_KURIKULUM}" {if isset($smarty.post.id_kurikulum)}{if $smarty.post.id_kurikulum == $kur.ID_KURIKULUM}selected{/if}{/if}>{$kur.NAMA}</option>
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
{/if}

{if isset($matkul_set)}
<h3>*) Input Mata Kuliah, Semester, dan Nilai Huruf</h3>
{else if isset($nilai_konversi)}

    <h3>Daftar nilai diakui</h3>
    <table>
        <thead>
            <tr>
                <th></th>
                <th>Semester</th>
                <th>Kode Mata Kuliah</th>
                <th>Nama Mata Kuliah</th>
                <th>SKS</th>
                <th>Nilai</th>
            </tr>
        </thead>
        <tbody>
            {foreach $nilai_konversi as $nk}
                <tr>
                    <td>{$nk@index + 1}</td>
                    <td>{$nk.NM_SEMESTER}</td>
                    <td>{$nk.KD_MATA_KULIAH}</td>
                    <td>{$nk.NM_MATA_KULIAH}</td>
                    <td class="center">{$nk.KREDIT_SEMESTER}</td>
                    <td class="center">{$nk.NILAI_HURUF}</td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="5">Nilai konversi belum dimasukkan</td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    
{/if}

<form action="insert-nilai.php?s={$smarty.get.s}" method="post" name="id_konversi_nilai" id="id_konversi_nilai">
<input type="hidden" name="action" value="proses" >
<!-- <input type="hidden" name="counter" value="{$datamhs.TOTAL_MATKUL}" > -->
<input type="hidden" name="id_mhs" value="{$datamhs.ID_MHS}" >
<input type="hidden" name="id_kurikulum" value="{$id_kurikulum_post}" >
<!-- <input type="hidden" name="log_granted" value="{$datamhs.ID_LOG_GRANTED_TRANSFER}" > -->

    {if isset($matkul_set)}
    <table>
        <tbody>
             <tr>
                <td>
                    <label><strong>Mata Kuliah</strong></label>
                </td>
                <td>
                    <label><strong>:</strong></label>
                </td>
                <td>
                    <select name="id_kurikulum_mk">
                        <option value="">-- Pilih Matkul --</option>
                        {foreach $matkul_set as $mk}
                             <option value="{$mk.ID_KURIKULUM_MK}">{$mk.TAHUN} - {$mk.KD_MATA_KULIAH} - {$mk.NM_MATA_KULIAH} - {$mk.KREDIT_SEMESTER} Sks</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <label><strong>Semester</strong></label>
                </td>
                <td>
                    <label><strong>:</strong></label>
                </td>
                <td>
                    <select name="id_semester">
                        <option value="">-- Pilih Semester --</option>
                        {foreach $semester_set as $sem}
                             <option value="{$sem.ID_SEMESTER}">{$sem.NM_SEMESTER}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <label><strong>Nilai Huruf</strong></label>
                </td>
                <td>
                    <label><strong>:</strong></label>
                </td>
                <td>
                    <input type="text" name="nilai" maxlength="2" size="2" />
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align:center">
                    <input type="submit" name="Submit" value="Proses">
                </td>
            </tr>
        </tbody>
    </table>
    {/if}
</form>


{if !empty($matkul_tampil)}
    <h3>Daftar Matkul Pengakuan Nilai Internal</h3>
	<table>   
            <thead>
                <tr>
                    <th>Semester</th>
                    <th>Mata Kuliah</th>
                    <th>Nilai Huruf</th>
                </tr>
            </thead> 
        {foreach $matkul_tampil as $data}
            <tbody>
                <tr>
                    <td>
                        {$data.NM_SEMESTER}
                    </td>
                    <td>{$data.TAHUN} - {$data.KD_MATA_KULIAH} - {$data.NM_MATA_KULIAH} - {$data.KREDIT_SEMESTER} Sks</td>
                    <td class="center">
                        {$data.NILAI_HURUF}
                    </td>
                </tr>
            </tbody>
        {/foreach}
    </table>
{/if}


	
{literal}
    <script>
			$("#id_konversi_nilai").validate();
    </script>
{/literal}
    