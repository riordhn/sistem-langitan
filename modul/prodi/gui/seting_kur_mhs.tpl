{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
      3: { sorter: false }
		}
		}
	);
}
);

	var i = $('input type="text"').size() + 2;

	$('#addButton').click(function() {


		 $('#tableText tr:last').after('<tr><td>Mahasiswa ' + i + '</td>' +
		  '<td>:</td>' +
		  '<td>' +
          ' <select name="select"> ' +
                  '<option value"">-- Mahasiswa --</option>' +
				  '    {foreach $dosen_pengguna as $dosen}'+
               	  '			<option value="{$dosen.id_dosen}">{$dosen.nama_dosen}</option>' +
             	  '	   {/foreach}'+
          ' </select>' +
          '</td></tr>').
			animate({ opacity: "show" }, "slow");
		i++;
	});


	function update(ID_MHS){
		$.ajax({
		type: "POST",
        url: "seting_kur_mhs.php",
        data: "action=update&id_kur="+$("#id_kur_edit"+ID_MHS).val()+"&id_mhs="+$("#id_mhs"+ID_MHS).val(),
        cache: false,
        success: function(data){
			confirm("Yakin Untuk Menyimpan?");
			$('#content').html(data);
        }
        });
	}
</script>
{/literal}

<div class="center_title_bar">Seting Kurikulum Mahasiswa</div>
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" align="center" onclick="javascript: displayPanel('2');">No Kurikulum</div>
	<div id="tab3" class="tab" align="center" onclick="javascript: displayPanel('3');">Update</div>
	<div id="tab4" class="tab" align="center" onclick="javascript: displayPanel('4');">Dist. Kurikulum</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table>
	<tr bgcolor="#efefef">
{foreach item="agk" from=$ANG}
		<td><a href="seting_kur_mhs.php?action=tampil&angk={$agk.ANGKATAN}"><strong>{$agk.ANGKATAN}</strong></a></td>
{/foreach}
	</tr>
</table>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Nim</th>
			<th>Nama Mahasiswa</th>
			<th>Kurikulum Mhs</th>
			<th class="noheader">Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="kur" from=$KUR1}
		<tr>
			<td>{$kur.NIM_MHS}</td>
			<td>{$kur.MHS}</td>
			<td>{$kur.KURIKULUM}</td>
			<td><center><a href="seting_kur_mhs.php?action=viewupdate&id_mhs={$kur.ID_MHS}">Update</a></center></td>
		</tr>
	{foreachelse}
		<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable1").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
      3: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="seting_kur_mhs.php" method="post" >
<input type="hidden" name="action" value="add" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>Pilih Kurikulum :</td>
			<td>
				<select name="id_kur" id="id_kur">
					<option value="">-- Pilih Kurikulum --</option>
					{foreach item="kur" from=$T_KUR}
					{html_options values=$kur.ID_KURIKULUM_PRODI output=$kur.KURIKULUM}
					{/foreach}
				</select>
			</td>
		</tr>
</table>
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Nim</th>
			<th>Nama Mahasiswa</th>
			<th>Pilih</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=nokur item="mhs_nokur" from=$NO_KUR}
		<tr>
			<td>{$mhs_nokur.NIM_MHS}</td>
			<td>{$mhs_nokur.NM_PENGGUNA}</td>
			<td><center><input name="mhs{$smarty.foreach.nokur.iteration}" type="checkbox" value="{$mhs_nokur.ID_MHS}" {$mhs_nokur.TANDA} /></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
		<input type="hidden" name="counter1" value="{$smarty.foreach.nokur.iteration}" >
		</tbody>
		<tr>
			<td colspan="2"></td>
			<td><center><input type="submit" name="PROSES" value="PROSES"></center></td>
		</tr>
</table>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
</p> </p>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Nim</th>
			<th>Nama Mahasiswa</th>
			<th>Kurikulum Mhs</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=nokur item="mhs_kur" from=$KUR}
	<input type="hidden" value="{$mhs_kur.ID_MHS}" name="mhs" id="id_mhs{$mhs_kur.ID_MHS}" />
		<tr>
			<td>{$mhs_kur.NIM_MHS}</td>
			<td>{$mhs_kur.MHS}</td>
			<td>
				<select name="id_kur_edit" id="id_kur_edit{$mhs_kur.ID_MHS}" onChange="update({$mhs_kur.ID_MHS})">
					<option value="">-- Pilih Kurikulum --</option>
					{foreach item="kur" from=$T_KUR}
					{html_options values=$kur.ID_KURIKULUM_PRODI output=$kur.KURIKULUM}
					{/foreach}
				</select>
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable2").tablesorter(
		{
		sortList: [[0,0]],
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel4" style="display: {$disp4}">
<p> </p>
<table id="myTable2" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Kurikulum</th>
			<th>Total Mahasiswa</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="seb" from=$SEBKUR}
		<tr>
			<td>{$seb.KURIKULUM}</td>
			<td><center>{$seb.TTL}</center></td>
		</tr>
	{foreachelse}
		<tr><td colspan="2"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

{literal}
 <script>$('form').validate();</script>
{/literal}

