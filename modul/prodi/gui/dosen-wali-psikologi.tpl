{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
      3: { sorter: false }
		}
		}
	);
}
);

	var i = $('input type="text"').size() + 2;

	$('#addButton').click(function() {


		 $('#tableText tr:last').after('<tr><td>Mahasiswa ' + i + '</td>' +
		  '<td>:</td>' +
		  '<td>' +
          ' <select name="select"> ' +
                  '<option value"">-- Mahasiswa --</option>' +
				  '    {foreach $dosen_pengguna as $dosen}'+
               	  '			<option value="{$dosen.id_dosen}">{$dosen.nama_dosen}</option>' +
             	  '	   {/foreach}'+
          ' </select>' +
          '</td></tr>').
			animate({ opacity: "show" }, "slow");
		i++;
	});


	function ubah(ID_MHS){
		$.ajax({
		type: "POST",
        url: "dosen-wali-psikologi.php",
        data: "action=savewali&dosen="+$("#id_dosen"+ID_MHS).val()+"&id_mhs="+$("#id_mhs"+ID_MHS).val(),
        cache: false,
        success: function(data){
			confirm("Yakin Untuk Menyimpan?");
			$('#content').html(data);
        }
        });
	}
	function update(ID_MHS){
		$.ajax({
		type: "POST",
        url: "dosen-wali-psikologi.php",
        data: "action=update&dosen="+$("#id_dosen").val()+"&id_mhs="+$("#id_mhs"+ID_MHS).val(),
        cache: false,
        success: function(data){
			confirm("Yakin Untuk Menyimpan?");
			$('#content').html(data);
        }
        });
	}
</script>
{/literal}

<div class="center_title_bar">Penetapan Dosen Wali </div>
<form action="dosen-wali-psikologi.php" method="post">
<input type="hidden" name="action" value="tampil" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		Tahun Ajaran : 
			<select name="thn_ajaran" id="thn_ajaran">
				<option value="">-- Tahun Ajaran --</option>
				{foreach item="smt" from=$T_ST}
				{html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$smtaktif}
				{/foreach}
			</select>
			<input type="submit" name="View" value="View">
		</td>
	</tr>
</table>
</form>

<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" align="center" onclick="javascript: displayPanel('2');">Mhs No Wali</div>
	<div id="tab3" class="tab" align="center" onclick="javascript: displayPanel('3');">Update</div>
	<div id="tab4" class="tab" align="center" onclick="javascript: displayPanel('4');">Dist. Perwalian</div>
	<div id="tab5" class="tab" align="center" onclick="javascript: displayPanel('5');">Detil Perwalian</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table>
	<tr bgcolor="#efefef">
{foreach item="agk" from=$ANG}
		<td><a href="dosen-wali-psikologi.php?action=tampil&angk={$agk.ANGKATAN}"><strong>{$agk.ANGKATAN}</strong></a></td>
{/foreach}
	</tr>
</table>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>NIM</th>
			<th>Nama Mahasiswa</th>
			<th>Dosen Wali</th>
			<th class="noheader">Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="wali" from=$WALI}
		<tr>
			<td>{$wali.NIM_MHS}</td>
			<td>{$wali.MHS}</td>
			<td>{$wali.DOLI}</td>
			<td><center><a href="dosen-wali-psikologi.php?action=viewupdate&id_mhs={$wali.ID_MHS}">Update</a></center></td>
		</tr>
	{foreachelse}
		<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>


<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="dosen-wali-psikologi.php" method="post" >
<input type="hidden" name="action" value="savewali" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>Pilih Dosen Wali :</td>
			<td>
				<select name="dosen" id="id_dosen">
					<option value="">-- Pilih Dosen --</option>
					{foreach item="dosen" from=$DOSEN}
					{html_options values=$dosen.ID_DOSEN output=$dosen.NAMA_DOSEN}
					{/foreach}
				</select>
			</td>
		</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="10%" bgcolor="#FFCC33">NIM</td>
			<td width="30%" bgcolor="#FFCC33">Nama Mahasiswa</td>
			<td width="5%" bgcolor="#FFCC33">Pilih</td>
		</tr>

		{foreach name=nowali item="mhs_nowali" from=$NOWALI}
		<tr>
			<td>{$mhs_nowali.NIM_MHS}</td>
			<td>{$mhs_nowali.NM_PENGGUNA}</td>
			<td><center><input name="mhs{$smarty.foreach.nowali.iteration}" type="checkbox" value="{$mhs_nowali.ID_MHS}" {$mhs_nowali.TANDA} /></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
		<input type="hidden" name="counter1" value="{$smarty.foreach.nowali.iteration}" >
				<tr>
				<td colspan="2"></td><td><input type="submit" name="PROSES" value="PROSES"></td>
				</tr>
</table>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
</p> </p>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>NIM</th>
			<th>Nama Mahasiswa</th>
			<th>Dosen Wali</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=nowali item="mhs_doli" from=$DOLI}
	<input type="hidden" value="{$mhs_doli.ID_MHS}" name="mhs" id="id_mhs{$mhs_doli.ID_MHS}" />
		<tr>
			<td>{$mhs_doli.NIM_MHS}</td>
			<td>{$mhs_doli.MHS}</td>
			<td>
				<select name="dosen" id="id_dosen" onChange="update({$mhs_doli.ID_MHS})">
					<option value="">-- Pilih Dosen --</option>
					{foreach item="dosen" from=$DOSEN}
					{html_options values=$dosen.ID_DOSEN output=$dosen.NAMA_DOSEN}
					{/foreach}
				</select>
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable2").tablesorter(
		{
		sortList: [[2,1]],
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel4" style="display: {$disp4}">
<p> </p>
<p><input type=button name="cetak" value="Ekspor Semua ke MS. Excel" onclick="window.open('proses/cetak-doli-psikologi.php','baru2');"></p>
<table id="myTable2" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>NIP/NIK</th>
			<th>Nama Dosen</th>
			<th>Total Mahasiswa</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="seb" from=$SEBDOLI}
		<tr>
			<td>{$seb.NIP_DOSEN}</td>
			<td>{$seb.NM_PENGGUNA}</td>
			<td><center><a href="dosen-wali-psikologi.php?action=detailwali&dosen={$seb.ID_DOSEN}">{$seb.TTL}</a></center></td>
			<td><center><input type="button" name="cetak" value="Cetak" onclick="window.open('proses/cetak-doli.php?cetak={$seb.ID_DOSEN}','baru');"></center></td>
		</tr>
	{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel5" style="display: {$disp5}">
<p> </p>
<p><strong>Daftar Anak Wali Dosen {$NMDSN} ({$NIPDSN})</strong></p>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>NIM</th>
			<th>Nama Mahasiswa</th>
			<th>Program Studi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="det" from=$DET}
		<tr>
			<td>{$det.NIM_MHS}</td>
			<td>{$det.MHS}</td>
			<td>{$det.PRODI}</td>
		</tr>
	{foreachelse}
		<tr><td colspan="2"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

{literal}
 <script>$('form').validate();</script>
{/literal}

