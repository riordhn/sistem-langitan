<div class="center_title_bar">REKAP BIMBINGAN TUGAS AKHIR/SKRIPSI/TESIS/DESERTASI</div>

{if isset($smarty.get.id)}
<table>
	<tr>
    	<td>Dosen</td>
        <td>:</td>
        <td>{$detail[0]['NM_DOSEN']}</td>
    </tr>
    <tr>
    	<td></td>
        <td>:</td>
        <td>{$detail[0]['NM_JENIS_PEMBIMBING']}</td>
    </tr>
</table>

<table>
	<tr>
    	<th>No</th>
        <th>NIM</th>
        <th>Nama</th>
        <th>Tipe</th>
        <th>Judul</th>
    </tr>
    {$no = 1}
    {foreach $detail as $data}
    <tr>
    	<td><center>{$no++}</center></td>
        <td>{$data.NIM_MHS}</td>
        <td>{$data.NM_MHS}</td>
        <td>{$data.NM_TIPE_TA}</td>
        <td>{$data.JUDUL_TUGAS_AKHIR|upper}</td>
    </tr>
    {/foreach}
</table>

{else}
<table>
	<tr>
    	<th rowspan="2" style="text-align:center">No</th>
    	<th rowspan="2" style="text-align:center">Dosen Pembimbing</th>
        <th colspan="5" style="text-align:center">Jumlah Mhs</th>        
    </tr>
    <tr>
    	{foreach $jenis_pembimbing as $data}
        <th>{$data.NM_JENIS_PEMBIMBING}</th>
        {/foreach}
    </tr>
    {$no = 1}
    {foreach $pembimbing as $data}
    <tr>
    	<td><center>{$no++}</center></td>
        <td>{$data.NM_PENGGUNA}</td>
        <td style="text-align:center"><a href="tugas-akhir-rekap.php?id={$data.ID_DOSEN}&jenis=1">{$data.PEMBIMBING_1}</a></td>
        <td style="text-align:center">{$data.PEMBIMBING_2}</td>
        <td style="text-align:center">{$data.PEMBIMBING_3}</td>
        <td style="text-align:center">{$data.PEMBIMBING_4}</td>
        <td style="text-align:center">{$data.PEMBIMBING_5}</td>
    </tr>
    {/foreach}
</table>
{/if}