
<div class="center_title_bar">Penawaran/Distribusi MKDU</div>
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">MKU</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
<form action="krs-tawar.php" method="post">
<input type="hidden" name="action" value="tampil" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
  <tr>
	 <td>
		Tahun Akademik :
		<select name="smt">
		<option value=''>-- PILIH THN AKD --</option>
		{foreach item="smt" from=$T_ST}
		{html_options values=$smt.ID_SEMESTER output=$smt.SMT selected=$id_smt}
		{/foreach}
		</select>
		<input type="submit" name="View" value="View">
	</td>
  </tr>
</table>
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
			<td width="5%" bgcolor="#333333"><font color="#FFFFFF">Kode</font></td>
			<td width="33%" bgcolor="#333333"><font color="#FFFFFF">Nama Mata Ajar</font></td>
			<td width="5%" bgcolor="#333333"><font color="#FFFFFF">SKS</font></td>
			<td width="5%" bgcolor="#333333"><font color="#FFFFFF">KLS</font></td>
			<td width="8%" bgcolor="#333333"><font color="#FFFFFF">Hari</font></td>
			<td width="12%" bgcolor="#333333"><font color="#FFFFFF">Jam</font></td>
			<td width="20%" bgcolor="#333333"><font color="#FFFFFF">Kapasitas</font></td>
			<td width="12%" bgcolor="#333333"><font color="#FFFFFF">Aksi</font></td>
	</tr>

		{foreach name=test item="list" from=$T_MK}
		<tr>
			<td><a href="krs-tawar.php?id_kelas_mk={$list.ID_KELAS_MK}&smt={$id_smt}">{$list.KD_MATA_KULIAH}</a></td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td><center>{$list.NAMA_KELAS}</center></td>
			<td>{$list.NM_JADWAL_HARI}</td>
			<td><center>{$list.JAM}</center></td>
			<td>{$list.KAPASITAS_KELAS_MK}</td>
			<td></td>
		</tr>
			{foreach name=test1 item="list1" from=$T_PENGIKUT}
			{if $list.ID_KELAS_MK==$list1.ID_KELAS_MK }
			<tr>
				<td></td>
				<td colspan="5" >{$list1.PENGIKUT}</td>
				<td>{$list1.QUOTA}</td>
				<td><center><a href="krs-tawar.php?action=del&id_krs_prodi={$list1.ID_KRS_PRODI}">Hapus</a></center></td>
			</tr>
			{/if}
			{/foreach}
		{foreachelse}
		<tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
</table>
</div>

{if isset($smarty.request.id_kelas_mk)}
<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="krs-tawar.php" method="post" >
<input type="hidden" name="action" value="add" >
<input type="hidden" name="mku" value="{$TAWAR['ID_KELAS_MK']}" >

<table>
	<tr>
    	<td>Mata Ajar</td>
        <td>:</td>
        <td>{$TAWAR['KELAS']}</td>
    </tr>
    <tr>
    	<td>Kelas / Ruangan</td>
        <td>:</td>
        <td>{$TAWAR['NAMA_KELAS']} / {$TAWAR['NM_RUANGAN']}</td>
    </tr>
    <tr>
    	<td>Hari / Jam</td>
        <td>:</td>
        <td>{$TAWAR['NM_JADWAL_HARI']} / {$TAWAR['JAM']}</td>
    </tr>
    <tr>
    	<td>Kapasitas</td>
        <td>:</td>
        <td>{$TAWAR['KAPASITAS_KELAS_MK']}</td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="3%" bgcolor="#FFCC33">No</td>
	<td width="40%" bgcolor="#FFCC33">Prodi Pengikut</td>
	<td width="20%" bgcolor="#FFCC33">Quota</td>
	</tr>
	{foreach name=test1 item="tawar" from=$T_PRODI}
	<tr>
	<td>{$smarty.foreach.test1.iteration}</td>
	<td>{$tawar.PENGIKUT}</td>
	<td><input name="quota{$smarty.foreach.test1.iteration}" type="text" value="0" size="7" /></td>
	<input type="hidden" name="prodi{$smarty.foreach.test1.iteration}" value="{$tawar.ID_PROGRAM_STUDI}" >
	</tr>
	{/foreach}
	<input type="hidden" name="counter" value="{$smarty.foreach.test1.iteration}" >
	<input type="hidden" name="smt" value="{$id_smt}">
	<tr><td colspan="3"></td></tr>
	<tr><td colspan="3"></td></tr>
	<tr>
	<td colspan="3"><center><input type="submit" name="PROSES" value="           PROSES         "></center></td>
	</tr>

</table>
</form>
</div>
{/if}
