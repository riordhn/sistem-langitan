<div class="center_title_bar">Status Entri Nilai</div>

<table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 5px">
	<tr>
		<td>
			<form action="mhs-nilai-kosong.php" method="get">
				Semester :
					<select name="thn_angkatan_mhs">
					{foreach $angkatan_mhs_set as $angkatan}
						<option value="{$angkatan.THN_ANGKATAN_MHS}" {if $angkatan.THN_ANGKATAN_MHS == $thn_angkatan_mhs}selected{/if}
						>{$angkatan.THN_ANGKATAN_MHS}</option>
					{/foreach}
				</select>
				<input type="submit" name="View" value="View">
			</form>
		</td>
	</tr>
</table>

<table>
	<thead>
		<tr>
			<th>No</th>
			<th>NIM</th>
			<th>Nama MHS</th>
			<th>Jumlah Nilai Kosong</th>
			<th class="noheader"></th>
		</tr>
	</thead>
	<tbody>
		{foreach $mhs_set as $mhs}
			<tr>
				<td class="center">{$mhs@index + 1}</td>
				<td>{$mhs.NIM_MHS}</td>
				<td>{$mhs.NM_PENGGUNA}</td>
				<td class="noheader">{$mhs.JML_NILAI_KOSONG}</td>
				<td class="center"><a href="mhs-nilai-kosong.php?mode=detail-mhs&id_mhs={$mhs.ID_MHS}">Detail</a></td>
			</tr>
		{/foreach}
	</tbody>
</table>