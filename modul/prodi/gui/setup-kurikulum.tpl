{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[4,1]],
		headers: {
            5: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">Master Kurikulum (Program Studi)</div>
	<div id="tabs">
		<div id="tab1" {if $smarty.get.action == '' || $smarty.get.action == 'tampil'}class="tab_sel"{else}class="tab"{/if} align="center" onclick="javascript: displayPanel('1');">Rincian</div>
		{* <div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div> *}
		<div id="tab2" {if $smarty.get.action == 'aktivasi'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Aktivasi</div>
   	</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Nama Kurikulum</th>
			<th>Status</th>
			<th>No. SK. Kurikulum</th>
			<th>Tahun Dibuat</th>
			<th>Semester Mulai Berlaku</th>
			<th>Masa Berlaku</th>
			<th class="noheader">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach item="list" from=$T_KUR}
		<tr>
			<td>{$list.NM_KURIKULUM}</td>
			<td>{$list.STATUS_AKTIF}</td>
			<td>{$list.NOMOR_SK_KURIKULUM}</td>
			<td>{$list.THN_KURIKULUM}</td>
			<td><center>{$list.THN_AKADEMIK_SEMESTER} {$list.NM_SEMESTER}</center></td>
			<td>{$list.BERLAKU_MULAI} s/d {$list.BERLAKU_SAMPAI}</td>
			<td><center><a href="setup-kurikulum.php?action=aktivasi&id_kurikulum={$list.ID_KURIKULUM}" onclick="displayPanel('2')">Aktivasi</a> <!--| <a href="setup-kurikulum.php?action=del&id_kurikulum={$list.ID_KURIKULUM_PRODI}">Delete</a>--> </center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>
</div>

{*
<div class="panel" id="panel2" style="display:{$disp2} ">
<p> </p>
<form action="setup-kurikulum.php" method="post" >
<input type="hidden" name="action" value="add" >
	<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>Nama Kurikulum</td>
		<td><center>:</center></td>
		<td>
			<select name="namakur">
				<option value=''>-- NAMA KUR --</option>
				{foreach item="kur" from=$NM_KUR}
				{html_options values=$kur.ID_KURIKULUM output=$kur.NM_KURIKULUM}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Status Kurikulum</td>
		<td><center>:</center></td>
		<td>
			<select name="status_kur" id="status_kur">
				<option value="">-- Status --</option>
				<option value="1">Aktif</option>
				<option value="0">Non Aktif</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Semester Mulai Berlaku</td>
		<td><center>:</center></td>
		<td>
			<select name="id_semester">
				{foreach $semester_set as $s}
				<option value="{$s.ID_SEMESTER}">{$s.THN_AKADEMIK_SEMESTER} {$s.NM_SEMESTER}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Berlaku Mulai</td>
		<td><center>:</center></td>
		<td><input type="Text" name="calawal" value="" id="calawal" maxlength="25" size="25"><input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calawal','ddmmmyyyy');" /></td>
	</tr>
	<tr>
		<td>Berlaku Sampai</td>
		<td><center>:</center></td>
		<td><input type="Text" name="calakhir" value="" id="calakhir" maxlength="25" size="25"><input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calakhir','ddmmmyyyy');" /></td>
	</tr>
	<tr>
		<td>No. SK Kurikulum</td>
		<td><center>:</center></td>
		<td><input type="text" name="sk_kur" id="nama_kurikulum3" size="40"/> Tulis SK Rektor-nya</td>
	</tr>
	</table>
<p><input type="submit" name="ttambah" value="Simpan" id="ttambah"></p>
</form>
</div>
*}

<div class="panel" id="panel2" style="display: {$disp2} ">
<p> </p>
<form action="setup-kurikulum.php?action=update" method="post" >
	<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	{foreach item="datakur" from=$T_KUR}
	{if $datakur.ID_KURIKULUM==$id_kur}
	<input type="hidden" name="id_kur" value="{$id_kur}" >
		<tr>
			<td>Nama Kurikulum</td>
			<td><center>:</center></td>
			<td>{$datakur.NM_KURIKULUM}
				<!-- <select name="namakur">
				{foreach item="kur" from=$NM_KUR}
				{html_options  values=$kur.ID_KURIKULUM output=$kur.NM_KURIKULUM selected=$datakur.ID_KURIKULUM}
				{/foreach}
				</select> -->
			</td>
		</tr>
		<tr>
			<td>Status Kurikulum</td>
			<td><center>:</center></td>
			<td>
				<select name="status_kur" id="status_kur">
				{html_options options=$myOptions selected=$datakur.STATUS}
				</select>
			</td>
		</tr>
		{*
		<tr>
		<td>Semester Mulai Berlaku</td>
		<td><center>:</center></td>
		<td>
		<select name="id_semester" >
			<option value="">-- BELUM DIPILIH --</option>
			{foreach $semester_set as $s}
				<option value="{$s.ID_SEMESTER}" {if $s.ID_SEMESTER == $datakur.ID_SEMESTER}selected{/if}>{$s.THN_AKADEMIK_SEMESTER} {$s.NM_SEMESTER}</option>
			{/foreach}
		</select>
		</tr>
		<tr>
			<td>Berlaku Mulai</td>
			<td><center>:</center></td>
			<td><input type="Text" name="calawal1" value="{$datakur.BERLAKU_MULAI}" id="calawal1" maxlength="25" size="25"><input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calawal1','ddmmmyyyy');" /></td>
		</tr>
		<tr>
			<td>Berlaku Sampai</td>
			<td><center>:</center></td>
			<td><input type="Text" name="calakhir1" value="{$datakur.BERLAKU_SAMPAI}" id="calakhir1" maxlength="25" size="25"><input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calakhir1','ddmmmyyyy');" /></td>
		</tr>
		<tr>
			<td>No. SK Kurikulum</td>
			<td><center>:</center></td>
			<td><input type="text" name="sk_kur1" value="{$datakur.NOMOR_SK_KURIKULUM}" id="nama_kurikulum3" size="40"/> Tulis SK Rektor-nya</td>
		</tr> *}
		{/if}
		{/foreach}
	</table>
<p><input type="submit" name="tambah" value="Update"></p>
</form>
</div>
<script>$('form').validate();</script>

