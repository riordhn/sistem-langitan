{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            2: { sorter: false },
            3: { sorter: false },
            4: { sorter: false },
            5: { sorter: false },
            6: { sorter: false },
            7: { sorter: false },
            8: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
  
<div class="center_title_bar">Master Mata Kuliah</div>
{literal}
  <style>
	input, select{
	height:25px;}
  </style>
  <script>
		$('.chosen-select').chosen();
  </script>
{/literal}

	<div id="tabs">
		<div id="tab1" {if $smarty.get.action == ''}class="tab_sel"{else}class="tab"{/if}  style="margin-left:1px;" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
		<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Input</div>
		<div id="tab3" {if $smarty.get.action == 'update'}class="tab_sel"{else}class="tab"{/if}  style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>
   	</div>
    <div class="tab_bdr"></div>
	
<div class="panel" id="panel1" style="display: {$disp1}">
    
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
  <thead>
  	<tr>
  		<th>Kode</th>
  		<th width="35%">Nama Mata Ajar</th>
  		<th>SKS</th>
  		<th title="SKS Tatap Muka">SKS<br/>TM</th>
  		<th title="SKS Praktikum">SKS<br/>Prak.</th>
  		<th title="SKS Praktikum Lapangan">SKS<br/>Prak.Lap.</th>
  		<th title="SKS Simulasi">SKS<br/>Sim.</th>
  		<th>Jenis MK</th>
  		<th>Kelompok MK</th>
  		<th>Status<br/>Sync</th>
  		<th>Aksi</th>
  	</tr>
	</thead>
	<tbody>
  {foreach name=test item="list" from=$T_RINCIAN}
	  
	  <tr {if $list.KREDIT_SEMESTER == '' or $list.KREDIT_SEMESTER == '0'}bgcolor="#FE2E2E"{/if}{if $list.RELASI_MATKUL > 1}bgcolor="#f6ff01"{/if}>
		<td>{$list.KD_MATA_KULIAH}</td>
		<td>{$list.NM_MATA_KULIAH}</td>
		<td>{$list.KREDIT_SEMESTER}</td>
		<td>{$list.KREDIT_TATAP_MUKA}</td>
		<td>{$list.KREDIT_PRAKTIKUM}</td>
		<td>{$list.KREDIT_PRAK_LAPANGAN}</td>
		<td>{$list.KREDIT_SIMULASI}</td>
		<td>{$list.NM_JENIS_MK}</td>
		<td>{$list.NM_KELOMPOK_MK}</td>
		<td class="center">{if $list.RELASI_FEEDER == ''}-{else}Ya{/if}</td>
	    <td ><a href="insert-mk.php?action=update&id_mata_kuliah={$list.ID_MATA_KULIAH}">Update</a> 
	    	{if $list.RELASI == 0} | 
				 <a href="insert-mk.php?action=del&id_mata_kuliah={$list.ID_MATA_KULIAH}">Delete</a></td>
			 {/if}
	  </tr>
     {foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
	</tbody>
</table>
<br>
</div>
		
    <div class="panel" id="panel2" style="display:{$disp2} ">
	  			 <form action="insert-mk.php" method="post" >
				  <input type="hidden" name="action" value="add" >
				  <table class="tb_frame" width="80%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>Kode Mata Kuliah</td>
                      <td>:</td>
                      <td><input type="text" name="kd_mata_kuliah" id="kd_mata_kuliah" size="40" required /></td>
                    </tr>
					<tr>
                      <td>Nama Mata Kuliah</td>
                      <td>:</td>
                      <td><input type="text" name="nm_mata_ajar" id="nm_mata_ajar" size="40" required/></td>
                    </tr>
                    <tr>
                      <td>Program Studi</td>
                      <td>:</td>
                      <td>
                      	<select  name="program_studi" >
                      		{foreach $T_DAFTAR_PRODI as $prodi}
			                    <option value="{$prodi.ID_PROGRAM_STUDI}">{$prodi.NM_JENJANG} - {$prodi.NM_PROGRAM_STUDI}</option>
			                {/foreach}
							</select>   
                      </td>
                    </tr>
                    <tr>
                      <td>Jenis Mata Kuliah</td>
                      <td>:</td>
                      <td>
                      	{foreach $T_JENIS_MK as $jenis_mk}
                      	  <input type="radio" name="jenis_mk" value="{$jenis_mk.ID_JENIS_MK}" {if $jenis_mk@index == 0} checked {/if}> {$jenis_mk.NM_JENIS_MK}
						 {/foreach}
                      </td>
                    </tr>
                    <tr>
                      <td>Kelompok Mata Kuliah</td>
                      <td>:</td>
                      <td>
                      	<select  name="kelompok_mk" >
                      		{foreach $T_KELOMPOK_MK as $kel_mk}
			                    <option value="{$kel_mk.ID_KELOMPOK_MK}">{$kel_mk.KET_KELOMPOK_MK}</option>
			                {/foreach}
						</select>   
                      </td>
                    </tr>
                    <tr>
                      <td>SKS Mata Kuliah</td>
                      <td>:</td>
                      <td>
                      	<input name="sks_total" type="text" size="3" maxlength="3"/>&nbsp;<font color="grey"><i>(SKS Tatap Muka + SKS Praktikum + SKS Praktek Lapangan + SKS Simulasi)</i></font>  
                      </td>
                    </tr>
                    <tr>
                      <td>SKS Tatap Muka</td>
                      <td>:</td>
                      <td>
                      	<input name="sks_tatap_muka" type="text" size="3" maxlength="3"/> 
                      </td>
                    </tr>
                    <tr>
                      <td>SKS Praktikum</td>
                      <td>:</td>
                      <td>
                      	<input name="sks_praktikum" type="text" size="3" maxlength="3"/> 
                      </td>
                    </tr>
                    <tr>
                      <td>SKS Praktek Lapangan</td>
                      <td>:</td>
                      <td>
                      	<input name="sks_praktek_lapangan" type="text" size="3" maxlength="3"/> 
                      </td>
                    </tr>
                    <tr>
                      <td>SKS Simulasi</td>
                      <td>:</td>
                      <td>
                      	<input name="sks_simulasi" type="text" size="3" maxlength="3"/> 
                      </td>
                    </tr>
                    <tr>
                      <td>Metode Kuliah</td>
                      <td>:</td>
                      <td><input type="text" name="metode_kuliah" size="40" /></td>
                    </tr>
					<tr>
					  <td>Sub Rumpun Ilmu</td>
					  <td>:</td>
					  <td>
					  
							<select class="chosen-select" multiple style="width:98%;" tabindex="4" name="sub_rumpun_ilmu" data-placeholder="Pilih Sub Rumpun Ilmu..." >
								{foreach item="sub_rumpun_ilmu" from=$T_RUMPUN}
								{html_options values=$sub_rumpun_ilmu.ID_SUB_RUMPUN_ILMU output=$sub_rumpun_ilmu.DETAIL}
							{/foreach}
							</select>   
					  </td>
					</tr>
					<tr>
                      <td>Ada SAP?</td>
                      <td>:</td>
                      <td>
                      	  <input type="radio" name="ada_sap" value="1" checked>Ya
						  <input type="radio" name="ada_sap" value="0">Tidak
                      </td>
                    </tr>
                    <tr>
                      <td>Ada Silabus?</td>
                      <td>:</td>
                      <td>
                      	  <input type="radio" name="ada_silabus" value="1" checked>Ya
						  <input type="radio" name="ada_silabus" value="0">Tidak
                      </td>
                    </tr>
                    <tr>
                      <td>Ada Bahan Ajar?</td>
                      <td>:</td>
                      <td>
                      	  <input type="radio" name="ada_bahan_ajar" value="1" checked>Ya
						  <input type="radio" name="ada_bahan_ajar" value="0">Tidak
                      </td>
                    </tr>
                    <tr>
                      <td>Ada Acara Praktek?</td>
                      <td>:</td>
                      <td>
                      	  <input type="radio" name="status_praktikum" value="1" checked>Ya
						  <input type="radio" name="status_praktikum" value="0">Tidak
                      </td>
                    </tr>
                    <tr>
                      <td>Ada Diktat?</td>
                      <td>:</td>
                      <td>
                      	  <input type="radio" name="ada_diktat" value="1" checked>Ya
						  <input type="radio" name="ada_diktat" value="0">Tidak
                      </td>
                    </tr>
                    <tr>
						<td>Tanggal Mulai Efektif</td>
						<td>:</td>
						<td><input type="Text" name="tgl_mulai_efektif" value="" id="calawal" maxlength="25" size="25"><input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calawal','ddmmmyyyy');" /></td>
					</tr>
					<tr>
						<td>Tanggal Akhir Efektif</td>
						<td>:</td>
						<td><input type="Text" name="tgl_akhir_efektif" value="" id="calakhir" maxlength="25" size="25"><input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calakhir','ddmmmyyyy');" /></td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="ttambah" value="Simpan" id="ttambah"></td>
					</tr>
		  		  </table>
			</form>	
</div>
<div class="panel" id="panel3" style="display: {$disp3} ">
		  <form action="insert-mk.php" method="post" >
		  		  <input type="hidden" name="action" value="proses" >
				  <table class="tb_frame" width="80%" border="0" cellspacing="0" cellpadding="0">
				{foreach item="datarumpun" from=$T_MATA_KULIAH}
					<input type="hidden" name="id_mata_kuliah" value="{$datarumpun.ID_MATA_KULIAH}" >

                    <tr>
                      <td>Kode Mata Kuliah</td>
                      <td>:</td>
                      <td><input type="text" name="kd_mata_kuliah" id="kd_mata_kuliah" value="{$datarumpun.KD_MATA_KULIAH}" size="40" required/></td>
                    </tr>
					<tr>
                      <td>Nama Mata Kuliah</td>
                      <td>:</td>
                      <td><input type="text" name="nm_mata_ajar" id="nm_mata_ajar" value="{$datarumpun.NM_MATA_KULIAH}" size="40" required/></td>
                    </tr>
                    <tr>
                      <td>Program Studi</td>
                      <td>:</td>
                      <td>
                      	<select  name="program_studi" >
                      		{foreach $T_DAFTAR_PRODI as $prodi}
			                    <option value="{$prodi.ID_PROGRAM_STUDI}" {if $prodi.ID_PROGRAM_STUDI == $datarumpun.ID_PROGRAM_STUDI}selected="selected"{/if}>{$prodi.NM_JENJANG} - {$prodi.NM_PROGRAM_STUDI}</option>
			                {/foreach}
							</select>   
                      </td>
                    </tr>
                    <tr>
                      <td>Jenis Mata Kuliah</td>
                      <td>:</td>
                      <td>
                      	{foreach $T_JENIS_MK as $jenis_mk}
                      	  <input type="radio" name="jenis_mk" value="{$jenis_mk.ID_JENIS_MK}" {if $jenis_mk.ID_JENIS_MK == $datarumpun.ID_JENIS_MK} checked {/if}> {$jenis_mk.NM_JENIS_MK}
						 {/foreach}
                      </td>
                    </tr>
                    <tr>
                      <td>Kelompok Mata Kuliah</td>
                      <td>:</td>
                      <td>
                      	<select  name="kelompok_mk" >
                      		  {foreach $T_KELOMPOK_MK as $kel_mk}
			                       <option value="{$kel_mk.ID_KELOMPOK_MK}" {if $kel_mk.ID_KELOMPOK_MK == $datarumpun.ID_KELOMPOK_MK} selected="selected" {/if}>{$kel_mk.KET_KELOMPOK_MK}</option>
			                      {/foreach}
						            </select>   
                      </td>
                    </tr>

                    {if $id_unit_kerja_sd == 30 or $id_pt_user != 1}
                      <tr>
                        <td>SKS Mata Kuliah</td>
                        <td>:</td>
                        <td>
                        	<input name="sks_total" type="text" size="3" maxlength="3" value="{$datarumpun.KREDIT_SEMESTER}" />&nbsp;<font color="grey"><i>(SKS Tatap Muka + SKS Praktikum + SKS Praktek Lapangan + SKS Simulasi)</i></font>  
                        </td>
                      </tr>
                      <tr>
                        <td>SKS Tatap Muka</td>
                        <td>:</td>
                        <td>
                        	<input name="sks_tatap_muka" type="text" size="3" maxlength="3" value="{$datarumpun.KREDIT_TATAP_MUKA}"/> 
                        </td>
                      </tr>
                      <tr>
                        <td>SKS Praktikum</td>
                        <td>:</td>
                        <td>
                        	<input name="sks_praktikum" type="text" size="3" maxlength="3" value="{$datarumpun.KREDIT_PRAKTIKUM}"/> 
                        </td>
                      </tr>
                      <tr>
                        <td>SKS Praktek Lapangan</td>
                        <td>:</td>
                        <td>
                        	<input name="sks_praktek_lapangan" type="text" size="3" maxlength="3" value="{$datarumpun.KREDIT_PRAK_LAPANGAN}"/> 
                        </td>
                      </tr>
                      <tr>
                        <td>SKS Simulasi</td>
                        <td>:</td>
                        <td>
                        	<input name="sks_simulasi" type="text" size="3" maxlength="3" value="{$datarumpun.KREDIT_SIMULASI}"/> 
                        </td>
                      </tr>
                    {else}
                      <tr>
                        <td>SKS Mata Kuliah</td>
                        <td>:</td>
                        <td>
                          <input name="sks_total" type="text" size="3" maxlength="3" value="{$datarumpun.KREDIT_SEMESTER}" readonly />&nbsp;<font color="grey"><i>(SKS Tatap Muka + SKS Praktikum + SKS Praktek Lapangan + SKS Simulasi)</i></font>  
                        </td>
                      </tr>
                      <tr>
                        <td>SKS Tatap Muka</td>
                        <td>:</td>
                        <td>
                          <input name="sks_tatap_muka" type="text" size="3" maxlength="3" value="{$datarumpun.KREDIT_TATAP_MUKA}" readonly /> 
                        </td>
                      </tr>
                      <tr>
                        <td>SKS Praktikum</td>
                        <td>:</td>
                        <td>
                          <input name="sks_praktikum" type="text" size="3" maxlength="3" value="{$datarumpun.KREDIT_PRAKTIKUM}" readonly /> 
                        </td>
                      </tr>
                      <tr>
                        <td>SKS Praktek Lapangan</td>
                        <td>:</td>
                        <td>
                          <input name="sks_praktek_lapangan" type="text" size="3" maxlength="3" value="{$datarumpun.KREDIT_PRAK_LAPANGAN}" readonly /> 
                        </td>
                      </tr>
                      <tr>
                        <td>SKS Simulasi</td>
                        <td>:</td>
                        <td>
                          <input name="sks_simulasi" type="text" size="3" maxlength="3" value="{$datarumpun.KREDIT_SIMULASI}" readonly /> 
                        </td>
                      </tr>
                    {/if}

                    <tr>
                      <td>Metode Kuliah</td>
                      <td>:</td>
                      <td><input type="text" name="metode_kuliah" size="40" value="{$datarumpun.METODE_PELAKSANAAN}" /></td>
                    </tr>
					<tr>
					  <td width="20%">Sub Rumpun Ilmu</td>
					  <td width="2%">:</td>
					  <td width="78%">
					  
							<select class="chosen-select" multiple style="width:98%;" tabindex="4" name="sub_rumpun_ilmu" data-placeholder="Pilih Sub Rumpun Ilmu..." >
								{foreach item="sub_rumpun_ilmu" from=$T_RUMPUN}
								{html_options values=$sub_rumpun_ilmu.ID_SUB_RUMPUN_ILMU output=$sub_rumpun_ilmu.DETAIL selected=$datarumpun.ID_SUB_RUMPUN_ILMU}
							{/foreach}
							</select>   
					  </td>
					</tr>
					<tr>
                      <td>Ada SAP?</td>
                      <td>:</td>
                      <td>
                      {if $datarumpun.ADA_SAP == 1}
                      		<input type="radio" name="ada_sap" value="1" checked>Ya
						  	<input type="radio" name="ada_sap" value="0">Tidak
					  {else}
					  	  	<input type="radio" name="ada_sap" value="1">Ya
						  	<input type="radio" name="ada_sap" value="0" checked>Tidak
					  {/if}
                      </td>
                    </tr>
                    <tr>
                      <td>Ada Silabus?</td>
                      <td>:</td>
                      <td>
                      {if $datarumpun.ADA_SILABUS == 1}
                      		<input type="radio" name="ada_silabus" value="1" checked>Ya
						  	<input type="radio" name="ada_silabus" value="0">Tidak
					  {else}
					  	  	<input type="radio" name="ada_silabus" value="1">Ya
						  	<input type="radio" name="ada_silabus" value="0" checked>Tidak
					  {/if}
                      </td>
                    </tr>
                    <tr>
                      <td>Ada Bahan Ajar?</td>
                      <td>:</td>
                      <td>
                      {if $datarumpun.ADA_BAHAN_AJAR == 1}
                      		<input type="radio" name="ada_bahan_ajar" value="1" checked>Ya
						  	<input type="radio" name="ada_bahan_ajar" value="0">Tidak
					  {else}
					  	  	<input type="radio" name="ada_bahan_ajar" value="1">Ya
						  	<input type="radio" name="ada_bahan_ajar" value="0" checked>Tidak
					  {/if}
                      </td>
                    </tr>
                    <tr>
                      <td>Mata Kuliah Praktikum ?</td>
                      <td>:</td>
                      <td>
                      {if $datarumpun.STATUS_PRAKTIKUM == 1}
                      		<input type="radio" name="status_praktikum" value="1" checked>Ya
						  	<input type="radio" name="status_praktikum" value="0">Tidak
					  {else}
					  	  	<input type="radio" name="status_praktikum" value="1">Ya
						  	<input type="radio" name="status_praktikum" value="0" checked>Tidak
					  {/if}
                      </td>
                    </tr>
                    <tr>
                      <td>Ada Diktat?</td>
                      <td>:</td>
                      <td>
                      {if $datarumpun.ADA_DIKTAT == 1}
                      		<input type="radio" name="ada_diktat" value="1" checked>Ya
						  	<input type="radio" name="ada_diktat" value="0">Tidak
					  {else}
					  	  	<input type="radio" name="ada_diktat" value="1">Ya
						  	<input type="radio" name="ada_diktat" value="0" checked>Tidak
					  {/if}
                      </td>
                    </tr>
                    <tr>
						<td>Tanggal Mulai Efektif</td>
						<td>:</td>
						<td><input type="Text" name="tgl_mulai_efektif" value="{$datarumpun.TGL_MULAI_EFEKTIF}" id="calawal1" maxlength="25" size="25"><input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calawal1','ddmmmyyyy');" /></td>
					</tr>
					<tr>
						<td>Tanggal Akhir Efektif</td>
						<td>:</td>
						<td><input type="Text" name="tgl_akhir_efektif" value="{$datarumpun.TGL_AKHIR_EFEKTIF}" id="calakhir1" maxlength="25" size="25"><input type="button" value="" style="background-image:url(includes/cal/cal.gif); background-repeat:no-repeat; background-position:center; border:none;" onClick="NewCal('calakhir1','ddmmmyyyy');" /></td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					 <td><input type="submit" name="tambah" value="Update"></td>
					</tr>
				{/foreach}
				</table>
	    </form>			
</div>	
<script>$('form').validate();</script>
	  		
   