{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
      3: { sorter: false }
		}
		}
	);
}
);

	var i = $('input type="text"').size() + 2;

	$('#addButton').click(function() {


		 $('#tableText tr:last').after('<tr><td>Mahasiswa ' + i + '</td>' +
		  '<td>:</td>' +
		  '<td>' +
          ' <select name="select"> ' +
                  '<option value"">-- Mahasiswa --</option>' +
				  '    {foreach $dosen_pengguna as $dosen}'+
               	  '			<option value="{$dosen.id_dosen}">{$dosen.nama_dosen}</option>' +
             	  '	   {/foreach}'+
          ' </select>' +
          '</td></tr>').
			animate({ opacity: "show" }, "slow");
		i++;
	});


	function update(ID_MHS){
		$.ajax({
		type: "POST",
        url: "seting_minat_mhs.php",
        data: "action=update&id_minat="+$("#id_minat_up").val()+"&id_mhs="+ID_MHS,
        cache: false,
        success: function(data){
			confirm("Yakin Untuk Menyimpan?");
			$('#content').html(data);
        }
        });
	}
</script>
{/literal}

<div class="center_title_bar">Seting Peminatan Mahasiswa</div>
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" align="center" onclick="javascript: displayPanel('2');">No Peminatan</div>
	<div id="tab3" class="tab" align="center" onclick="javascript: displayPanel('3');">Update</div>
	<div id="tab4" class="tab" align="center" onclick="javascript: displayPanel('4');">Dist. Peminatan</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table>
	<tr bgcolor="#efefef">
{foreach item="agk" from=$ANG}
		<td><a href="seting_minat_mhs.php?action=tampil&angk={$agk.ANGKATAN}"><strong>{$agk.ANGKATAN}</strong></a></td>
{/foreach}
	</tr>
</table>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Nim</th>
			<th>Nama Mahasiswa</th>
			<th>Peminatan</th>
			<th class="noheader">Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="minat" from=$MINAT1}
		<tr>
			<td>{$minat.NIM_MHS}</td>
			<td>{$minat.MHS}</td>
			<td>{$minat.PEMINATAN}</td>
			<td><center><a href="seting_minat_mhs.php?action=viewupdate&id_mhs={$minat.ID_MHS}">Update</a></center></td>
		</tr>
	{foreachelse}
		<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable1").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
      3: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="seting_minat_mhs.php" method="post" >
<input type="hidden" name="action" value="add" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>Pilih Peminatan :</td>
			<td>
				<select name="id_minat" id="id_minat">
					<option value="">-- Pilih Peminatan --</option>
					{foreach item="minat" from=$T_MINAT}
					{html_options values=$minat.ID_PRODI_MINAT output=$minat.NM_PRODI_MINAT}
					{/foreach}
				</select>
			</td>
		</tr>
</table>
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Nim</th>
			<th>Nama Mahasiswa</th>
			<th>Pilih</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=nominat item="mhs_nominat" from=$NO_MINAT}
		<tr>
			<td>{$mhs_nominat.NIM_MHS}</td>
			<td>{$mhs_nominat.NM_PENGGUNA}</td>
			<td><center><input name="mhs{$smarty.foreach.nominat.iteration}" type="checkbox" value="{$mhs_nominat.ID_MHS}" {$mhs_nominat.TANDA} /></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
		<input type="hidden" name="counter1" value="{$smarty.foreach.nominat.iteration}" >
		</tbody>
		<tr>
			<td colspan="2"></td>
			<td><center><input type="submit" name="PROSES" value="PROSES"></center></td>
		</tr>
</table>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
</p> </p>
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Nim</th>
			<th>Nama Mahasiswa</th>
			<th>Peminatan</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=nominat item="mhs_minat" from=$MINAT}
	<input type="hidden" value="{$mhs_minat.ID_MHS}" name="mhs" id="id_mhs{$mhs_minat.ID_MHS}" />
		<tr>
			<td>{$mhs_minat.NIM_MHS}</td>
			<td>{$mhs_minat.MHS}</td>
			<td>
				<select name="id_minat" id="id_minat_up" onChange="update({$mhs_minat.ID_MHS})">
					<option value="">-- Pilih Peminatan --</option>
					{foreach item="minat" from=$T_MINAT}
					{html_options values=$minat.ID_PRODI_MINAT output=$minat.NM_PRODI_MINAT}
					{/foreach}
				</select>
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable2").tablesorter(
		{
		sortList: [[0,0]],
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel4" style="display: {$disp4}">
<p> </p>
<table id="myTable2" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Peminatan</th>
			<th>Total Mahasiswa</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="seb" from=$SEBPEMINATAN}
		<tr>
			<td>{$seb.NM_PRODI_MINAT}</td>
			<td><center>{$seb.TTL}</center></td>
		</tr>
	{foreachelse}
		<tr><td colspan="2"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

{literal}
 <script>$('form').validate();</script>
{/literal}

