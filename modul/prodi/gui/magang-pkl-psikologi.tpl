{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[0,0]],
		headers: {
		}
		}
	);
}
);
</script>
{/literal}

<div id="pjmk">
<div class="center_title_bar">BIMBINGAN MAGANG/PRAKTEK KERJA LAPANGAN</div>
<div id="tabs">
        <div id="tab1" {if $smarty.get.action == ''}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
        <div id="tab2" {if $smarty.get.action == 'edit'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">INSERT/EDIT</div>
		<div id="tab3" {if $smarty.get.action == 'searchdosen'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Search Dosen</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<form action="magang-pkl.php" method="post">
<input type="hidden" name="action" value="tampil" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>Tahun Ajaran : 
			<select name="smt" id="thn_ajaran">
			{foreach item="smt" from=$T_ST}
			{html_options  values=$smt.ID_SEMESTER output=$smt.SMT selected=$SMT}
			{/foreach}
			</select>
			<input type="submit" name="View" value="View">
		</td>
	</tr>
</table>
</form>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>No</th>
			<th>Nim</th>
			<th>Nama</th>
			<th>Nama MK</th>
			<th>Judul</th>
			<th class="noheader">Tim Pembimbing</th>
			<th class="noheader">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{$no = 1}
		{foreach name=test item="list" from=$mhs_skripsi}
		{if $list.JUDUL_TUGAS_AKHIR == ""}
		<tr bgcolor="yellow">
		{else}
		<tr>
		{/if}
			<td><center>{$no++}<center></td>
			<td>{$list.NIM_MHS}</td>
			<td>{$list.MHS}</td>
			<td>{$list.NM_MATA_KULIAH|upper}</td>
			<td>{$list.JUDUL_TUGAS_AKHIR|upper}</td>
			<td>
			{if $list.JENIS_PEMBIMBING_1 != ''}
			       {$list.JENIS_PEMBIMBING_1} : 
				 <br/> - {$list.PEMBIMBING_1}
			 {/if}
			
			{if $list.JENIS_PEMBIMBING_2 != ''}
				<br/> {$list.JENIS_PEMBIMBING_2} : 
				<br/> - {$list.PEMBIMBING_2}
			 {/if}

			 {if $list.JENIS_PEMBIMBING_3 != ''}
				<br/> {$list.JENIS_PEMBIMBING_3} : 
				<br/> - {$list.PEMBIMBING_3}
			 {/if}

			 {if $list.JENIS_PEMBIMBING_4 != ''}
				<br/> {$list.JENIS_PEMBIMBING_4} : 
				<br/> - {$list.PEMBIMBING_4}
			 {/if}

			{if $list.JENIS_PEMBIMBING_5 != ''}
				<br/> {$list.JENIS_PEMBIMBING_5} : 
				<br/> - {$list.PEMBIMBING_5}
			 {/if}
			 </td>
			<td><center><a href="magang-pkl.php?action=edit&id_mhs={$list.ID_MHS}&smt={$SMT}&tipe={$list.ID_TIPE_TA}&des={$list.NM_MATA_KULIAH}">EDIT</a></td>
		</tr>
		{foreachelse}
		<tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="magang-pkl.php?action=simpanjudul&smt={$SMT}&id_mhs={$smarty.get.id_mhs}" method="post" id="cari1">
<table class="tb_frame" width="100%" border="1" cellspacing="0" cellpadding="0" id="tableText">
{foreach name=test item="list1" from=$ta}		
                  <tr>
                    <td width="149">NIM</td>
                    <td width="9">:</td>
                    <td width="154">{$list1.NIM_MHS}</td>
                  </tr>
				  <tr>
                    <td width="149">Nama Mhs</td>
                    <td width="9">:</td>
                    <td>{$list1.MHS}</td>
                  </tr>		  
		{if $list1.ID_TUGAS_AKHIR != ''}
		{foreach $jenis as $data}
		  <tr>
                    <td width="149">{$data.NM_JENIS_PEMBIMBING}</td>
                    <td width="9">:</td>
                    <td>{$data.NM_PENGGUNA}
				{if $data.ID_DOSEN != ''}
				<a href="magang-pkl.php?action=hapusdosen&smt={$smarty.get.smt}&id_mhs={$smarty.get.id_mhs}&pembimbing={$data.ID_PEMBIMBING_TA}&jenis={$data.ID_JENIS_PEMBIMBING}&id_dosen={$data.ID_DOSEN}">Hapus</a>
				{else}
				<a href="magang-pkl.php?action=searchdosen&smt={$smarty.get.smt}&id_mhs={$smarty.get.id_mhs}&jenis={$data.ID_JENIS_PEMBIMBING}&ta={$list1.ID_TUGAS_AKHIR}">Cari Dosen</a>
				{/if}
		    </td>
           </tr>
		{/foreach}
		{else}<p style="color:red;">Untuk mengeset PEMBIMBING, isi judul terlebih dahulu dan simpan.</p>{/if}
		<tr>
                    <td width="149">Tipe TA</td>
                    <td width="9">:</td>
                    <td>
						<input type="hidden" name="tipe_ta" value="4">Magang/PKL
						<input type="hidden" name="ta" value="{$list1.ID_TUGAS_AKHIR}">
					</td>
                  </tr>
		  <tr>
                    <td width="149">Nama MK</td>
                    <td width="9">:</td>
                    <td><input type="hidden" name="des" value="{$smarty.get.des|upper}">{$smarty.get.des|upper}</td>
                  </tr>				  
		  <tr>
                    <td width="149">Judul</td>
                    <td width="9">:</td>
                    <td><textarea name="judul" cols="60" rows="6">{$list1.JUDUL_TUGAS_AKHIR}</textarea></td>
                  </tr>
		  <tr>
                    <td width="149"></td>
                    <td width="9"></td>
                    <td><input type="submit" value="Simpan Judul"></td>
                  </tr>
{/foreach}
</table>
</form>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
<p> </p>
<form action="magang-pkl.php?action=searchdosen&smt={$SMT}&id_mhs={$smarty.get.id_mhs}&jenis={$smarty.get.jenis}&ta={$smarty.get.ta}" method="post" id="cari1">
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="121">Nama Dosen</td>
    <td width="431">: <input name="namadosen" type="text" /> <input type="submit" name="cari" value="Cari Dosen">
	</td>
	</tr>
</table>
</form>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
			   <tr bgcolor="#000000">
			     <td width="2%"><font color="#FFFFFF">No</font></td>
				 <td width="20%"><font color="#FFFFFF">Photo Dosen</font></td>
				 <td width="8%"><font color="#FFFFFF">Kode Dosen</font></td>
				 <td width="45%"><font color="#FFFFFF">Nama Dosen</font></td>
				 <td width="20%"><font color="#FFFFFF">Prodi</font></td>
				 <td width="5%"><font color="#FFFFFF">Aksi</font></td>
			  </tr>
			  <tr>
			 {foreach name=test item="list" from=$DOSEN}
			     <td>{$smarty.foreach.test.iteration}</td>
				 <td><img src="{$list.FOTO_PENGGUNA}/{$list.NIP_DOSEN}.JPG" width="160px"/></td>
				 <td>{$list.NIP_DOSEN}</td>
				 <td>{$list.NM_PENGGUNA}</td>
				 <td>{$list.NM_PROGRAM_STUDI}</td>
				 <td><a href="magang-pkl.php?action=addview&id_dosen={$list.ID_DOSEN}&id_mhs={$smarty.get.id_mhs}&smt={$SMT}&jenis={$smarty.get.jenis}">Pilih</a></td>
			</tr>
        {/foreach}
			</table>
</div>

</div>
{literal}
 <script>$('form').validate();</script>
{/literal}