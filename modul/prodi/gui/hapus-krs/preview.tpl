<div class="center_title_bar">HAPUS KRS</div>

<h2>Step 3 : Cek Akhir</h2>

<table>
    <tbody>
        <tr>
            <td style="padding: 0px 10px 0px 0px; vertical-align: top">
                
                <table>
                    <thead>
                        <tr>
                            <th>Kode MK</th>
                            <th>Nama MK</th>
                            <th>SKS</th>
                            <th>Tingkat<br/>Semester</th>
                            <th>Kelas</th>
                            <th>Dosen PJMK</th>
                            <th>KRS</th>
                            <th>Aprv</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $kelas_mk_set as $kelas_mk}
                            <tr>
                                <td>{$kelas_mk.KD_MATA_KULIAH}</td>
                                <td>{$kelas_mk.NM_MATA_KULIAH}</td>
                                <td class="center">{$kelas_mk.KREDIT_SEMESTER}</td>
                                <td class="center">{$kelas_mk.TINGKAT_SEMESTER}</td>
                                <td>{$kelas_mk.NAMA_KELAS}</td>
                                <td>{$kelas_mk.NAMA_PJMK}</td>
                                <td class="center">{$kelas_mk.PESERTA}</td>
                                <td class="center">{$kelas_mk.PESERTA_APPROVE}</td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
				
            </td>
            <td style="padding: 0px 0px 0px 10px;  vertical-align: top">
                
                <table>
                    <thead>
                        <tr>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Angkatan</th>
                            <th>Kelas</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $mahasiswa_set as $mahasiswa}
                            <tr>
                                <td>{$mahasiswa.NIM_MHS}</td>
                                <td>{$mahasiswa.NM_PENGGUNA}</td>
                                <td class="center">{$mahasiswa.THN_ANGKATAN_MHS}</td>
                                <td class="center">{$mahasiswa.NAMA_KELAS}</td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
                
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
					<form action="krs-hapus.php?mode=proses" method="post">
						{foreach $kelas_mk_set as $kelas_mk}
						<input type="hidden" name="id_kelas_mk[]" value="{$kelas_mk.ID_KELAS_MK}">
						{/foreach}

						{foreach $mahasiswa_set as $mahasiswa}
						<input type="hidden" name="id_mhs[]" value="{$mahasiswa.ID_MHS}">
						{/foreach}

						<input type="submit" value="Proses HAPUS KRS" />
					</form>
            </td>
        </tr>
    </tbody>
</table>

