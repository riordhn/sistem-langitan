<div class="center_title_bar">HAPUS KRS</div>

<h2>Step 2 : Pilih Mahasiswa - {$prodi.NM_PROGRAM_STUDI}</h2>

<table>
    <thead>
        <tr>
            <th>
                <form action="krs-hapus.php?mode=pilih-mhs" method="post">
                    {foreach $smarty.post.id_kelas_mk as $id_kelas_mk}
                    <input type="hidden" name="id_kelas_mk[]" value="{$id_kelas_mk}">
                    {/foreach}
                    
                    <input type="hidden" name="id_program_studi" value="{$prodi.ID_PROGRAM_STUDI}" />
                    
                    Angkatan : 
                    <select name="thn_angkatan_mhs" onchange="javascript: $(this).submit();">
                        <option value="all">Semua Angkatan</option>
                        {foreach $thn_angkatan_set as $thn}
                            <option value="{$thn.THN_ANGKATAN_MHS}" {if !empty($smarty.post.thn_angkatan_mhs)}{if $thn.THN_ANGKATAN_MHS == $smarty.post.thn_angkatan_mhs}selected{/if}{/if}>{$thn.THN_ANGKATAN_MHS}</option>
                        {/foreach}
                    </select>
                    
                    Kelas :
                    <select name="id_nama_kelas" onchange="javascript: $(this).submit();">
                        <option value="all">Semua</option>
                        {foreach $nama_kelas_set as $nama_kelas}
                            <option value="{$nama_kelas.ID_NAMA_KELAS}"
                                    {if !empty($smarty.post.id_nama_kelas)}
                                        {if $nama_kelas.ID_NAMA_KELAS == $smarty.post.id_nama_kelas}selected{/if}
                                    {/if}>{$nama_kelas.NAMA_KELAS}</option>
                        {/foreach}
                    </select>
                </form>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="padding: 2px">
                
                <form action="krs-hapus.php?mode=preview" method="post">
                    
                    {foreach $smarty.post.id_kelas_mk as $id_kelas_mk}
                    <input type="hidden" name="id_kelas_mk[]" value="{$id_kelas_mk}">
                    {/foreach}
                    
                    <table class="tablesorter" id="tabel_mhs">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" name="select_all" />
                                </th>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th>Angkatan</th>
                                <th>Kelas</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach $mahasiswa_set as $mahasiswa}
                                <tr>
                                    <td><input type="checkbox" name="id_mhs[]" value="{$mahasiswa.ID_MHS}" /></td>
                                    <td>{$mahasiswa.NIM_MHS}</td>
                                    <td>{$mahasiswa.NM_PENGGUNA}</td>
                                    <td class="center">{$mahasiswa.THN_ANGKATAN_MHS}</td>
                                    <td class="center">{$mahasiswa.NAMA_KELAS}</td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                            {if count($mahasiswa_set) > 0}
                                <tr>
                                    <td colspan="5" class="center">
                                        <input type="submit" value="Lanjut Cek Akhir" />
                                    </td>
                                </tr>
                            {/if}
                        </tfoot>
                    </table>
                </form>
                
            </td>
        </tr>
    </tbody>
</table>
                    
<script type="text/javascript">
    $(document).ready(function() {
        $("#tabel_mhs").tablesorter({
            headers: {
              0: { sorter: false }  
            },
            sortList: [[1,0]] /* Order By NIM*/
        });
        
        /* Select All Checkbox */
        $('input[name="select_all"]').change(function() {
            var select_all_checked = this.checked;
            $('input[name="id_mhs[]"]').each(function() {
                this.checked = select_all_checked;
            });
        });
    });
</script>