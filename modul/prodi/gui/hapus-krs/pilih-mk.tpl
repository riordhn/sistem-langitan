<div class="center_title_bar">HAPUS KRS</div>

<h2>Step 1 : Pilih Mata Kuliah</h2>

<a href="krs-hapus.php">Kembali ke tampilan rekap Hapus KRS</a>
<table>
    <thead>
        <tr>
            <th>
                <form action="krs-hapus.php" method="get">
                    <input type="hidden" name="mode" value="pilih-mk" />
                Program Studi : 
                <select name="id_program_studi" onchange="javascript: $(this).submit();">
                    {foreach $prodi_set as $prodi}
                        <option value="{$prodi.ID_PROGRAM_STUDI}"
                                {if !empty($smarty.get.id_program_studi)}
                                    {if $prodi.ID_PROGRAM_STUDI == $smarty.get.id_program_studi}selected{/if}
                                {/if}
                                >{$prodi.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
                Semester :
                {* KHUSUS DSI UMAHA *}
                {if $id_unit_kerja_sd == 30}
                    <select name="id_semester" onchange="javascript: $(this).submit();">
                            <option value="0" >-- Pilih Semester --</option>
                        {foreach $semester_set as $s}
                            <option value="{$s.ID_SEMESTER}" 
                                    {if empty($smarty.get.id_semester)}
                                        {if $s.STATUS_AKTIF_SEMESTER == 'True'}selected{/if}
                                    {else}
                                        {if $s.ID_SEMESTER == $smarty.get.id_semester}selected{/if}
                                    {/if}
                                    >{$s.NM_SEMESTER} {$s.TAHUN_AJARAN} {if $s.STATUS_AKTIF_SEMESTER == 'True'}(AKTIF){/if}</option>
                        {/foreach}
                    </select>
                {else}
                    <select name="id_semester">
                        {foreach $semester_aktif_set as $s}
                            <option value="{$s.ID_SEMESTER}" >{$s.TAHUN_AJARAN} {$s.NM_SEMESTER}</option>
                        {/foreach}
                    </select>
                {/if}
                Tingkat :
                <select name="tingkat_semester" onchange="javascript: $(this).submit();">
                    <option value="all">Semua</option>
                    {for $tingkat = 1 to 8}
                        <option value="{$tingkat}"
                                {if !empty($smarty.get.tingkat_semester)}
                                    {if $tingkat == $smarty.get.tingkat_semester}selected{/if}
                                {/if}>{$tingkat}</option>
                    {/for}
                </select>
                Kelas :
                <select name="id_nama_kelas" onchange="javascript: $(this).submit();">
                    <option value="all">Semua</option>
                    {foreach $nama_kelas_set as $nama_kelas}
                        <option value="{$nama_kelas.ID_NAMA_KELAS}"
                                {if !empty($smarty.get.id_nama_kelas)}
                                    {if $nama_kelas.ID_NAMA_KELAS == $smarty.get.id_nama_kelas}selected{/if}
                                {/if}>{$nama_kelas.NAMA_KELAS}</option>
                    {/foreach}
                </select>
                </form>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="padding: 2px">
                
                <form action="krs-hapus.php?mode=pilih-mhs" method="post">
                    <input type="hidden" name="id_program_studi" value="{$smarty.get.id_program_studi}" />
                    <table class="tablesorter" id="tabel_kelas">
                        <thead>
                            <tr>
                                <th>
                                    <!-- <input type="checkbox" name="select_all" /> -->

                                </th>
                                <th>Kode MK</th>
                                <th>Nama MK</th>
                                <th>SKS</th>
                                <th>Tingkat<br/>Semester</th>
                                <th>Kelas</th>
                                <th>Dosen PJMK</th>
                                <th>KRS</th>
                                <th>Aprv</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach $kelas_mk_set as $kelas_mk}
                                <tr>
                                    <td>
                                        <!-- <input type="checkbox" name="id_kelas_mk[]" value="{$kelas_mk.ID_KELAS_MK}" /> -->
                                        <input type="radio" name="id_kelas_mk" value="{$kelas_mk.ID_KELAS_MK}" />
                                        
                                    </td>
                                    <td>{$kelas_mk.KD_MATA_KULIAH}</td>
                                    <td>{$kelas_mk.NM_MATA_KULIAH}</td>
                                    <td class="center">{$kelas_mk.KREDIT_SEMESTER}</td>
                                    <td class="center">{$kelas_mk.TINGKAT_SEMESTER}</td>
                                    <td>{$kelas_mk.NAMA_KELAS}</td>
                                    <td>{$kelas_mk.NAMA_PJMK}</td>
                                    <td class="center">{$kelas_mk.PESERTA}</td>
                                    <td class="center">{$kelas_mk.PESERTA_APPROVE}</td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                            {if count($kelas_mk_set) > 0}
                                <tr>
                                    <td colspan="9" class="center">
                                        <input type="submit" value="Lanjut Pilih Mahasiswa" />
                                    </td>
                                </tr>
                            {/if}
                        </tfoot>
                    </table>
                </form>
                    
            </td>
        </tr>
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function() {
        $("#tabel_kelas").tablesorter({
            headers: {
              0: { sorter: false },
              7: { sorter: false },
              8: { sorter: false }
            },
            sortList: [[2,0],[5,0]] /* Order By Nama, Kelas */
        });
        
        /* Select All Checkbox */
        $('input[name="select_all"]').change(function() {
            var select_all_checked = this.checked;
            $('input[name="id_kelas_mk[]"]').each(function() {
                this.checked = select_all_checked;
            });
        });
    });
</script>