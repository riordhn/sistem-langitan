<div class="center_title_bar">Input Nilai</div>

<form action="input-nilai.php" method="get">
    <table>
        <tbody>
            <tr>
                <td>Dosen Pengampu :
                    <select name="id_dosen">
                        <option value="0" >-- Pilih Dosen --</option>
                        {foreach $dosen_set as $d}
                            {if ! empty ($smarty.get.id_dosen)}
                                <option value="{$d.ID_DOSEN}" {if $d.ID_DOSEN == $smarty.get.id_dosen}selected{/if}
                                    >{$d.NM_PENGGUNA} - {$d.NIP_DOSEN}</option>
                            {else}
                                <option value="{$d.ID_DOSEN}">{$d.NM_PENGGUNA} - {$d.NIP_DOSEN}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
                <td>Semester :
                    {* KHUSUS DSI UMAHA *}
                    {if $id_unit_kerja_sd == 30}
                        <select name="id_semester">
                            <option value="0" >-- Pilih Semester --</option>
                            {foreach $semester_set as $s}
                                {if ! empty ($smarty.get.id_semester)}
                                    <option value="{$s.ID_SEMESTER}" 
                                            {if $s.ID_SEMESTER == $smarty.get.id_semester}selected{/if}
                                            >{$s.TAHUN_AJARAN} {$s.NM_SEMESTER}</option>
                                {else}
                                    <option value="{$s.ID_SEMESTER}">{$s.TAHUN_AJARAN} {$s.NM_SEMESTER}</option>
                                {/if}
                            {/foreach}
                        </select>
                    {else}
                        <select name="id_semester">
                            {foreach $semester_aktif_set as $s}
                                {if ! empty ($smarty.get.id_semester)}
                                    <option value="{$s.ID_SEMESTER}" 
                                        {if $s.ID_SEMESTER == $smarty.get.id_semester}selected{/if} 
                                        >{$s.TAHUN_AJARAN} {$s.NM_SEMESTER}</option>
                                {else}
                                    <option value="{$s.ID_SEMESTER}">{$s.TAHUN_AJARAN} {$s.NM_SEMESTER}</option>
                                {/if}
                            {/foreach}
                        </select>
                    {/if}
                </td>
                <td>
                    <input type="submit" value="Lihat"/>
                </td>
            </tr>
        </tbody>
    </table>
</form>
{if isset($matkul_set)}                    
<table>
    <thead>
        <tr>
            <th class="center">No</th>
            <th class="center">Kode - Nama Mata Kuliah</th>
            <th class="center">Kelas</th>
            <th class="center">Jml Mhs KRS</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        {foreach $matkul_set as $matkul}
            <tr>
                <td class="center">{$matkul@index + 1}</td>
                <td>{$matkul.KD_MATA_KULIAH} - {$matkul.NM_MATA_KULIAH} 
                                                {if $matkul.PJMK_PENGAMPU_MK==1}
                                                    (PJMK)
                                                {else}
                                                    (Anggota)
                                                {/if}</td>
                <td class="center">{$matkul.NAMA_KELAS}</td>
                <td class="center">{$matkul.JML_MHS}</td>
                <td>
                    <form action="input-nilai.php" method="get">
                        <input type="hidden" name="mode" value="pilih-menu" />
                        <input type="hidden" name="id_kelas_mk" value="{$matkul.ID_KELAS_MK}" />
                        <input type="hidden" name="id_dosen" value="{$matkul.ID_DOSEN}" />
                        <input type="hidden" name="id_semester" value="{$matkul.ID_SEMESTER}" />
                        <button>Pilih</button>
                    </form>
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>
{/if}