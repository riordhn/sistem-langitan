<div class="center_title_bar">Input Nilai</div>

<form action="input-nilai.php" method="get">
    <table>
        <tbody>
            <tr>
                <td>Dosen Pengampu :
                    <select name="id_dosen">
                        <option value="0" >-- Pilih Dosen --</option>
                        {foreach $dosen_set as $d}
                            {if ! empty ($smarty.get.id_dosen)}
                                <option value="{$d.ID_DOSEN}" {if $d.ID_DOSEN == $smarty.get.id_dosen}selected{/if}
                                    >{$d.NM_PENGGUNA} - {$d.NIP_DOSEN}</option>
                            {else}
                                <option value="{$d.ID_DOSEN}">{$d.NM_PENGGUNA} - {$d.NIP_DOSEN}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
                <td>Semester :
                    {* KHUSUS DSI UMAHA *}
                    {if $id_unit_kerja_sd == 30}
                        <select name="id_semester">
                            <option value="0" >-- Pilih Semester --</option>
                            {foreach $semester_set as $s}
                                {if ! empty ($smarty.get.id_semester)}
                                    <option value="{$s.ID_SEMESTER}" 
                                            {if $s.ID_SEMESTER == $smarty.get.id_semester}selected{/if}
                                            >{$s.TAHUN_AJARAN} {$s.NM_SEMESTER}</option>
                                {else}
                                    <option value="{$s.ID_SEMESTER}">{$s.TAHUN_AJARAN} {$s.NM_SEMESTER}</option>
                                {/if}
                            {/foreach}
                        </select>
                    {else}
                        <select name="id_semester">
                            {foreach $semester_aktif_set as $s}
                                <option value="{$s.ID_SEMESTER}" >{$s.TAHUN_AJARAN} {$s.NM_SEMESTER}</option>
                            {/foreach}
                        </select>
                    {/if}
                </td>
                <td>
                    <input type="submit" value="Lihat"/>
                </td>
            </tr>
        </tbody>
    </table>
</form>
{if $persentase == 100} 
<table>
    <thead>
        <tr>
            <th class="center" colspan="2">MATKUL : {$nm_matkul} {if $pengampu==1}
                                                    (PJMK)
                                                {else}
                                                    (Anggota)
                                                {/if}</th>
        </tr>
    </thead>
    <tbody>
            <tr>
                <td class="center">
                    <form action="input-nilai.php" method="get">
                        <input type="hidden" name="mode" value="komponen" />
                        <input type="hidden" name="id_kelas_mk" value="{$id_kelas_mk}" />
                        <input type="hidden" name="id_dosen" value="{$id_dosen}" />
                        <input type="hidden" name="id_semester" value="{$id_semester}" />
                        <button>Setting Komponen</button>
                    </form>
                </td>
                <td class="center">
                    <form action="input-nilai.php" method="get">
                        <input type="hidden" name="mode" value="input" />
                        <input type="hidden" name="id_kelas_mk" value="{$id_kelas_mk}" />
                        <input type="hidden" name="id_dosen" value="{$id_dosen}" />
                        <input type="hidden" name="id_semester" value="{$id_semester}" />
                        <button>Input Nilai</button>
                    </form>
                </td>
            </tr>
    </tbody>
</table>
{else}
<table>
    <thead>
        <tr>
            <th class="center" colspan="2">MATKUL : {$nm_matkul} {if $pengampu==1}
                                                    (PJMK)
                                                {else}
                                                    (Anggota)
                                                {/if}</th>
        </tr>
    </thead>
    <tbody>
            <tr>
                <td class="center">
                    <form action="input-nilai.php" method="get">
                        <input type="hidden" name="mode" value="komponen" />
                        <input type="hidden" name="id_kelas_mk" value="{$id_kelas_mk}" />
                        <input type="hidden" name="id_dosen" value="{$id_dosen}" />
                        <button>Setting Komponen</button>
                    </form>
                </td>
                <td class="center">
                    Lakukan Setting Komponen Terlebih Dahulu <br> (Belum Ada Komponen / Komponen Belum 100 %)
                </td>
            </tr>
    </tbody>
</table>
{/if}

<button onclick="goBack()"><< Kembali</button>

{literal}
<script>
function goBack() {
    window.history.back();
}
</script>
{/literal}