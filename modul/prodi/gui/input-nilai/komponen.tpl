<div class="center_title_bar">Input Nilai</div>

<form action="input-nilai.php" method="get">
    <table>
        <tbody>
            <tr>
                <td>Dosen Pengampu :
                    <select name="id_dosen">
                        <option value="0" >-- Pilih Dosen --</option>
                        {foreach $dosen_set as $d}
                            <option value="{$d.ID_DOSEN}" 
                                    {if $d.ID_DOSEN == $smarty.get.id_dosen}selected{/if}
                                    >{$d.NM_PENGGUNA} - {$d.NIP_DOSEN}</option>
                        {/foreach}
                    </select>
                </td>
                <td>Semester :
                    {* KHUSUS DSI UMAHA *}
                    {if $id_unit_kerja_sd == 30}
                        <select name="id_semester">
                            <option value="0" >-- Pilih Semester --</option>
                            {foreach $semester_set as $s}
                                <option value="{$s.ID_SEMESTER}" 
                                        {if $s.ID_SEMESTER == $smarty.get.id_semester}selected{/if}
                                        >{$s.TAHUN_AJARAN} {$s.NM_SEMESTER}</option>
                            {/foreach}
                        </select>
                    {else}
                        <select name="id_semester">
                            {foreach $semester_aktif_set as $s}
                                <option value="{$s.ID_SEMESTER}" >{$s.TAHUN_AJARAN} {$s.NM_SEMESTER}</option>
                            {/foreach}
                        </select>
                    {/if}
                </td>
                <td>
                    <input type="submit" value="Lihat"/>
                </td>
            </tr>
        </tbody>
    </table>
</form>     

<table>
    <thead>
        <tr>
            <th class="center" colspan="5">MATKUL : {$nm_matkul} {if $pengampu==1}
                                                    (PJMK)
                                                {else}
                                                    (Anggota)
                                                {/if}</th>
        </tr>
        <tr>
            <th class="center">Urutan Komponen</th>
            <th class="center">Nama Komponen</th>
            <th class="center">Persentase</th>
            <th class="center" colspan="2">Aksi</th>
        </tr>
    </thead>
    <tbody>
        {foreach $komponen_set as $komponen}
            <tr>
                <td class="center">{$komponen.URUTAN_KOMPONEN_MK}</td>
                <td>{$komponen.NM_KOMPONEN_MK}</td>
                <td class="center">{$komponen.PERSENTASE_KOMPONEN_MK}</td>
                <td>
                    <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_edit_komponen').dialog('open').load('/modul/prodi/input-nilai.php?mode=edit-komponen&id_komponen={$komponen.ID_KOMPONEN_MK}&id_kelas_mk={$id_kelas_mk}&id_dosen={$id_dosen}')">Edit</span>
                </td>
                <td>
                    <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_delete_komponen').dialog('open').load('/modul/prodi/input-nilai.php?mode=delete-komponen&id_komponen={$komponen.ID_KOMPONEN_MK}&id_kelas_mk={$id_kelas_mk}&id_dosen={$id_dosen}')">Hapus</span>
                </td>
            </tr>
        {/foreach}
            <tr>
                <td colspan="9" class="center">
                    <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_tambah_komponen').dialog('open').load('/modul/prodi/input-nilai.php?mode=add-komponen&id_kelas_mk={$id_kelas_mk}&id_dosen={$id_dosen}')">Tambah Komponen</span>
                </td>
            </tr>
    </tbody>
</table>

<button onclick="goBack()"><< Kembali</button>


<div id="dialog_tambah_komponen" title="Tambah Komponen Nilai"></div>
<div id="dialog_edit_komponen" title="Edit Komponen Nilai"></div>
<div id="dialog_delete_komponen" title="Delete Komponen Nilai"></div>

{literal}
<script>
function goBack() {
    window.history.back();
}
</script>
{/literal}

{literal}
    <script type="text/javascript">
            $( "#dialog:ui-dialog" ).dialog( "destroy" );
            $( "#dialog_tambah_komponen" ).dialog({
                    width:'500',
                    modal: true,
                    resizable:false,
                    autoOpen:false
            });
            $( "#dialog_edit_komponen" ).dialog({
                    width:'500',
                    modal: true,
                    resizable:false,
                    autoOpen:false
            });
            $( "#dialog_copy_komponen" ).dialog({
                    width:'700',
                    modal: true,
                    resizable:false,
                    autoOpen:false
            });    
            $( "#dialog_delete_komponen" ).dialog({
                    width:'420',
                    modal: true,
                    resizable:false,
                    autoOpen:false
            });
            $('#table_komponen_nilai td').addClass('center ui-widget-content');
    </script>
{/literal}