<form id="form_delete_komponen" method="post" action="input-nilai.php?mode=komponen&id_kelas_mk={$id_kelas_mk}&id_dosen={$id_dosen}">
    <table class="ui-widget" width="370px">
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="2"><h2>Hapus Komponen Nilai</h2></th>
        </tr>
        <tr class="ui-widget-content">
            <td class="center">
                <p>Apakah anda yakin untuk menghapus komponen nilai {$data_komponen_mk_by_id['NM_KOMPONEN_MK']} ?</p>
                <input type="hidden" name="id_komponen" value="{$data_komponen_mk_by_id['ID_KOMPONEN_MK']}" />
                <input type="hidden" name="mode" value="delete-komponen"/>
                <input type="hidden" name="id_kelas_mk" value="{$id_kelas_mk}"/>
                <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#form_delete_komponen').submit();$('#dialog_delete_komponen').dialog('close');">Ya</span>
                <span class="ui-button ui-state-default ui-corner-all" style="padding:5px" onclick="$('#dialog_delete_komponen').dialog('close')">Batal</span>
            </td>
        </tr>
    </table>
</form>