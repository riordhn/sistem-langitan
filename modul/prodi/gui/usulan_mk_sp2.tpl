{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
 $("#myTable").tablesorter(
		{
		sortList: [[1,0], [2,0]],
		headers: {
   7: { sorter: false },
		}
		}
	);
}
);
</script>
{/literal}

<div id="usulan_mk_sp2">
<div class="center_title_bar">Usulan Mata Ajar</div>
	<div id="tabs">
  <div id="tab1" {if $smarty.get.action == ''}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('1');"><a href="usulan_mk_sp2.php" style="color:#000000;text-decoration:none;cursor:pointer">Rincian </a></div>
  <div id="tab2" {if $smarty.get.action == 'penawaran'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');"><a href="usulan_mk_sp2.php?action=penawaran" style="color:#000000;text-decoration:none;cursor:pointer">Penawaran</a></div>
	<div id="tab3" {if $smarty.get.action == 'updateview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Update</div>
	<div id="tab4" {if $smarty.get.action == 'adkelview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('4');">Tambah Kelas</div>
	<div id="tab5" {if $smarty.get.action == 'adhariview'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('5');">Tambah Hari</div>
 </div>

<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
<form action="usulan_mk_sp2.php" method="post">
<input type="hidden" name="action" value="tampil" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		Tahun Akademik :
			<select name="smt">
				<option value=''>-- PILIH THN AKD --</option>
				{foreach item="smt" from=$T_ST}
				{html_options values=$smt.ID_SEMESTER output=$smt.SMT selected=$smtaktif}
				{/foreach}
			</select>
		<input type="submit" name="View" value="View">
		</td>
	</tr>
</table>
</form>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="5%">Kode</th>
			<th width="30%">Nama Mata Ajar</th>
			<th width="5%">KLS</th>
			<th width="5%">SKS</th>
			<th width="5%">SMT</th>
			<th width="10%">Ruang</th>
			<th width="8%">Hari</th>
			<th width="12%">Jam</th>
			<th class="noheader" width="15%">Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$T_MK}
	{if $list.NM_JADWAL_HARI == "" || $list.JAM == ""}
		<tr bgcolor="#ff828e">
	{else}
		<tr>
	{/if}
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.NAMA_KELAS}</center></td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td><center>{$list.TINGKAT_SEMESTER}</center></td>
			<td>{$list.NM_RUANGAN}</td>
			<td>{$list.NM_JADWAL_HARI}</td>
			<td>{$list.JAM}</td>
			<td>
			{if $kdf==7}
			<a href="usulan_mk_sp2.php?action=del&id_klsmk={$list.ID_KELAS_MK}">Del</a>
			&nbsp;|&nbsp;
			<a href="usulan_mk_sp2.php?action=adkelview&id_klsmk={$list.ID_KELAS_MK}&smt={$list.ID_SEMESTER}">Add Kls</a>
			{else}
			<a href="usulan_mk_sp2.php?action=updateview&id_klsmk={$list.ID_KELAS_MK}">Upd</a>
			&nbsp;|&nbsp;
			<a href="usulan_mk_sp2.php?action=del&id_klsmk={$list.ID_KELAS_MK}">Del</a>
			&nbsp;|&nbsp;
			<a href="usulan_mk_sp2.php?action=adkelview&id_klsmk={$list.ID_KELAS_MK}&smt={$list.ID_SEMESTER}">Add Kls</a>
			&nbsp;|&nbsp;
			<a href="usulan_mk_sp2.php?action=adhariview&id_klsmk={$list.ID_KELAS_MK}&smt={$list.ID_SEMESTER}">Add Hari</a>
			{/if}
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
 $("#myTable1").tablesorter(
		{
		sortList: [[4,0], [1,0]],
		headers: {
   3: { sorter: false },
   5: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="usulan_mk_sp2.php" method="post" >
<input type="hidden" name="action" value="penawaran" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>Tahun Akademik </td>
		<td>
			<select name="smt"> 

				{foreach item="smt" from=$T_ST}
					{if isset($smarty.request.smt)}
						{html_options values=$smt.ID_SEMESTER output=$smt.SMT selected=$smarty.request.smt}
					{else}
						{html_options values=$smt.ID_SEMESTER output=$smt.SMT selected=$smtaktif}
					{/if}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Tahun Kurikulum </td>
		<td>
			<select name="thkur">

				{foreach item="kur" from=$T_KUR}
					{if isset($smarty.request.thkur)}
						{html_options values=$kur.ID_KURIKULUM_PRODI output=$kur.NAMA selected=$smarty.request.thkur }
					{else}
						{html_options values=$kur.ID_KURIKULUM_PRODI output=$kur.NAMA selected=$kurpil }
					{/if}
				{/foreach}
			</select>
		<input type="submit" name="View" value="View">
		</td>
	</tr>
</table>
</form>
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="10%">Kode</th>
			<th width="35%">Nama Mata Ajar</th>
			<th width="10%">SKS</th>
			<th class="noheader" width="10%">Thn-Kur</th>
			<th width="10%">Smt</th>
			<th width="10%">Status</th>
			<th class="noheader" width="15%">Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$TAWAR}
		<tr>
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td><center>{$list.TAHUN_KURIKULUM}</center></td>
			<td><center>{$list.TINGKAT_SEMESTER}</center></td>
			<td><center>{$list.NM_STATUS_MK}</center></td>
			<td><center><a href="usulan_mk_sp2.php?action=add&id_mk={$list.ID_KURIKULUM_MK}&id_kur={$kurpil}&smt={$smarty.request.smt}">DiTawarkan</a></center></td>
		</tr>
	{foreachelse}
		<tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel3" style="display: {$disp3}">
<p> </p>
<form action="usulan_mk_sp2.php" method="post">
<input type="hidden" name="action" value="update1" >
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	<tr bgcolor="#265226"><td colspan="3"><strong><font color="#fff"> Lengkapi Kelas </font></strong></td></tr>
	{foreach item="ubah" from=$TJAF}
	<input type="hidden" name="id_kelas_mk" value="{$ubah.ID_KELAS_MK}" />
	<input type="hidden" name="smtb" value="{$ubah.ID_SEMESTER}" />
	<tr>
		<td width="29%">Kode Mata Ajar</td>
		<td width="2%"><center>:</center></td>
		<td width="69%">{$ubah.KD_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar</td>
		<td><center>:</center></td>
		<td>{$ubah.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Kelas MK [A/B/C/D]</td>
		<td><center>:</center></td>
		<td>
			<select name="kelas_mk" id="kelas_mk">
			{foreach item="namakls" from=$NM_KELAS}
			{html_options values=$namakls.ID_NAMA_KELAS output=$namakls.NAMA_KELAS selected=$ubah.NO_KELAS_MK}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>KAPASITAS KELAS</td>
		<td><center>:</center></td>
		<td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$ubah.KAPASITAS_KELAS_MK}"/> [WAJIB DI-ISI]</td>
	</tr>
	<tr>
		<td>Rencana Perkuliahan (Tatap Muka)</td>
		<td><center>:</center></td>
		<td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$ubah.JUMLAH_PERTEMUAN_KELAS_MK}"/> [WAJIB DI-ISI]</td>
	</tr>
	<tr>
		<td>Hari</td>
		<td><center>:</center></td>
		<td>
			<select name="hari" id="hari">
			{foreach item="hari" from=$T_HARI}
			{html_options values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI selected=$ubah.ID_JADWAL_HARI}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Jam</td>
		<td><center>:</center></td>
			<td>
			<select name="jam" id="jam">
			{foreach item="jam" from=$T_JAM}
			{html_options values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$ubah.ID_JADWAL_JAM}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Ruangan</td>
		<td><center>:</center></td>
		<td>
			<select name="ruangan" id="ruangan">
			{foreach item="ruangan" from=$T_RUANG}
			{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG selected=$ubah.ID_RUANGAN}
			{/foreach}
			</select>
		</td>
	</tr>
	{/foreach}
</table>
{if $ubah.KD_MATA_KULIAH == ""}
<p><em>Data tidak ditemukan</em></p>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript: displayPanel('1');"></p>
{else}
<p><input type="button" name="kembali" value="Kembali" onClick="window.location.href='#supro_1-umasp1!usulan_mk_sp2.php';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Simpan1" value="Simpan"></p>
{/if}
</form>
</div>

<div class="panel" id="panel4" style="display: {$disp4}">
<p> </p>
<form action="usulan_mk_sp2.php" method="post">
<input type="hidden" name="action" value="tambahkelas" >
<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr bgcolor="#265226"><td colspan="3"><strong><font color="#fff"> Tambah Kelas </font></strong></td></tr>
	{foreach item="tambahkelas" from=$TJAF1}
	<input type="hidden" name="id_kur_mk" value="{$tambahkelas.ID_KURIKULUM_MK}" />
	<input type="hidden" name="smtb" value="{$tambahkelas.ID_SEMESTER}" />
	<tr>
		<td width="29%">Kode Mata Ajar</td>
		<td width="2%"><center>:</center></td>
		<td width="69%">{$tambahkelas.KD_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar</td>
		<td><center>:</center></td>
		<td>{$tambahkelas.NM_MATA_KULIAH}</td>
	</tr>
	<tr bgcolor="yellow">
		<td>Kelas MK [A/B/C/D]</td>
		<td><center>:</center></td>
		<td>
			<select name="kelas_mk" id="kelas_mk">
				<option value=''>------</option>
				{foreach item="namakls" from=$NM_KELAS}
				{html_options values=$namakls.ID_NAMA_KELAS output=$namakls.NAMA_KELAS}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>KAPASITAS KELAS</td>
		<td><center>:</center></td>
		<td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambahkelas.KAPASITAS_KELAS_MK}"/></td>
	</tr>
	<tr>
		<td>Rencana Perkuliahan</td>
		<td><center>:</center></td>
		<td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambahkelas.JUMLAH_PERTEMUAN_KELAS_MK}"/></td>
	</tr>
	<tr>
		<td>Hari</td>
		<td><center>:</center></td>
		<td>
			<select name="hari" id="hari">
			{foreach item="hari" from=$T_HARI}
			{html_options values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI selected=$tambahkelas.ID_JADWAL_HARI}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Jam</td>
		<td><center>:</center></td>
		<td>
			<select name="jam" id="jam">
			{foreach item="jam" from=$T_JAM}
			{html_options values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$tambahkelas.ID_JADWAL_JAM}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr bgcolor="yellow">
		<td>Ruangan</td>
		<td><center>:</center></td>
		<td>
			<select name="ruangan" id="ruangan">
				<option value=''>------</option>
				{foreach item="ruangan" from=$T_RUANG}
				{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG}
				{/foreach}
			</select>
		</td>
	</tr>
	{/foreach}
</table>
{if $tambahkelas.KD_MATA_KULIAH == ""}
<p><em>Data tidak ditemukan</em></p>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript: displayPanel('1');"></p>
{else}
<p><input type="button" name="kembali" value="Kembali" onClick="window.location.href='#supro_1-umasp1!usulan_mk_sp2.php';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Simpan1" value="Tambah Kelas"></p>
{/if}
</form>
</div>

<div class="panel" id="panel5" style="display: {$disp5}">
<p> </p>
<form action="usulan_mk_sp2.php" method="post">
<input type="hidden" name="action" value="tambahhari" >
<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr bgcolor="#265226"><td colspan="3"><strong><font color="#fff"> Tambah Hari </font></strong></td></tr>
	{foreach item="tambahhari" from=$TJAF2}
	<input type="hidden" name="id_kelas_mk" value="{$tambahhari.ID_KELAS_MK}" />
	<input type="hidden" name="smtb" value="{$tambahhari.ID_SEMESTER}" />
	<tr>
		<td width="29%">Kode Mata Ajar</td>
		<td width="2%"><center>:</center></td>
		<td width="69%">{$tambahhari.KD_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar</td>
		<td><center>:</center></td>
		<td>{$tambahhari.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Kelas MK [A/B/C/D]</td>
		<td><center>:</center></td>
		<td>
			<select name="kelas_mk" id="kelas_mk">
			{foreach item="namakls" from=$NM_KELAS}
			{html_options values=$namakls.ID_NAMA_KELAS output=$namakls.NAMA_KELAS selected=$tambahhari.ID_NAMA_KELAS}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>KAPASITAS KELAS</td>
		<td><center>:</center></td>
		<td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambahhari.KAPASITAS_KELAS_MK}"/></td>
	</tr>
	<tr>
		<td>Rencana Perkuliahan</td>
		<td><center>:</center></td>
		<td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$tambahhari.JUMLAH_PERTEMUAN_KELAS_MK}"/></td>
	</tr>
	<tr bgcolor="yellow">
		<td>Hari</td>
		<td><center>:</center></td>
		<td>
			<select name="hari" id="hari">
				<option value=''>------</option>
				{foreach item="hari" from=$T_HARI}
				{html_options values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI}
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Jam</td>
		<td><center>:</center></td>
		<td>
			<select name="jam" id="jam">
			{foreach item="jam" from=$T_JAM}
			{html_options values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$tambahhari.ID_JADWAL_JAM}
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Ruangan</td>
		<td><center>:</center></td>
		<td>
			<select name="ruangan" id="ruangan">
			{foreach item="ruangan" from=$T_RUANG}
			{html_options values=$ruangan.ID_RUANGAN output=$ruangan.RUANG selected=$tambahhari.ID_RUANGAN}
			{/foreach}
			</select>
		</td>
	</tr>
	{/foreach}
</table>
{if $tambahhari.KD_MATA_KULIAH == ""}
<p><em>Data tidak ditemukan</em></p>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript: displayPanel('1');"></p>
{else}
<p><input type="button" name="kembali" value="Kembali" onClick="window.location.href='#supro_1-umasp1!usulan_mk_sp2.php';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="Simpan2" value="Tambah Hari"></p>
{/if}
</form>
</div>
</div>