<div class="center_title_bar">PROSES KRS MAHASISWA BARU</div>  

<div id="tabs">
<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">KRS-MABA</div>
<div id="tab3" class="tab_sel" align="center" onclick="javascript: displayPanel('3');">KRS-CUSTOMIZE</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
			 <td width="5%" bgcolor="#FFCC33"><center>No</center></td>
             <td width="30%" bgcolor="#FFCC33"><center>Prodi</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Angkatan</center></td>
             <td width="10%" bgcolor="#FFCC33"><center>Total Mhs</center></td>
             <td width="10%" bgcolor="#FFCC33"><center>Total KRS</center></td>
			 <td width="10%" bgcolor="#FFCC33"><center>Aksi</center></td>
		  </tr>
		  {foreach name=test item="list" from=$T_MABA}
			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.PRODI}</td>
			 <td>{$list.THN_ANGKATAN_MHS}</td>
             <td>{$list.MABA}</td>
             <td>{$list.KRS}</td>
			 <td height="0" align="center"><input name="Button" type="button" class="button" onClick="location.href='#utility-krs_paket!krs_prodi.php?action=viewcustem&prodi={$list.ID_PROGRAM_STUDI}&agk={$list.THN_ANGKATAN_MHS}';disableButtons()" onMouseOver="window.status='Click untuk Insert';return true" onMouseOut="window.status='Insert KRS_MABA'" value="CUSTOMIZE" /> </td> 
			</tr>
		  {/foreach}
</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2} ">
<p> </p>
<form action="krs_prodi.php" method="post" >
<input type="hidden" name="action" value="prosesall" >	
<input type="hidden" name="prodi" value="{$ID_PROGRAM_STUDI}" >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td colspan="8" bgcolor="#FFCC33"><center>PILIH MATA AJAR DAN KELAS</center></td>
				</tr>
				<tr>
				<td colspan="8" bgcolor="#FFCC33"><center>*Jika MATA AJAR tdk TAMPIL, periksalah Penawaran KRS untuk Prodi tersebut</center></td>
				</tr>
				<tr>
				<td colspan="8" bgcolor="#FFCC33"><center>*Jika MATA AJAR masih tdk TAMPIL, periksalah posisi semester MATA AJAR via menu PRODI DAFTAR MA</center></td>
				</tr>
				<tr>
			     <td width="3%" bgcolor="#FFCC33">No</td>
				 <td width="5%" bgcolor="#FFCC33">Kode MA</td>
				 <td width="20%" bgcolor="#FFCC33">Nama MA</td>
				 <td width="5%" bgcolor="#FFCC33">SKS</td>
				 <td width="5%" bgcolor="#FFCC33">Kelas MA</td>
				 <td width="5%" bgcolor="#FFCC33">Status</td>
				 <td width="10%" bgcolor="#FFCC33">Prodi</td>
				 <td width="5%" bgcolor="#FFCC33">Pilih</td>
				</tr>
				{foreach name=test1 item="krs" from=$MA_PILIH}
				<tr>
				 <td>{$smarty.foreach.test1.iteration}</td>
				 <td>{$krs.KD_MATA_KULIAH}</td>
				 <td>{$krs.NM_MATA_KULIAH}</td>
				 <td>{$krs.KREDIT_SEMESTER}</td>
				 <td>{$krs.NAMA_KELAS}</td>
				 <td>{$krs.STATUS}</td>
				 <td>{$krs.PRODI}</td>
				 <td><center><input name="mk{$smarty.foreach.test1.iteration}" type="checkbox" value="{$krs.ID_KELAS_MK}" {$krs.TANDA} /></center></td>
				</tr>
				
			    {foreachelse}
        		<tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
        		{/foreach}
				<input type="hidden" name="counter" value="{$smarty.foreach.test1.iteration}" >
				<tr>
				<td colspan="7"></td><td><input type="submit" name="PROSES" value="PROSES"></td>
				</tr>
		</table>
		
</form>
</div>

<div class="panel" id="panel3" style="display: {$disp3} ">
<p> </p>
<form action="krs_prodi.php" method="post" >
<input type="hidden" name="action" value="prosescustem" >	
<input type="hidden" name="prodi" value="{$ID_PROGRAM_STUDI}" >	
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td colspan="8" bgcolor="#FFCC33"><center>PILIH MATA AJAR DAN KELAS</center></td>
				</tr>
				<tr>
				<td colspan="8" bgcolor="#FFCC33"><center>*Jika MATA AJAR tdk TAMPIL, periksalah Penawaran KRS untuk Prodi tersebut</center></td>
				</tr>
				<tr>
				<td colspan="8" bgcolor="#FFCC33"><center>*Jika MATA AJAR masih tdk TAMPIL, periksalah posisi semester MATA AJAR via menu PRODI DAFTAR MA</center></td>
				</tr>
				<tr>
			     <td width="3%" bgcolor="#FFCC33">No</td>
				 <td width="5%" bgcolor="#FFCC33">Kode MA</td>
				 <td width="20%" bgcolor="#FFCC33">Nama MA</td>
				 <td width="5%" bgcolor="#FFCC33">SKS</td>
				 <td width="5%" bgcolor="#FFCC33">Kelas MA</td>
				 <td width="5%" bgcolor="#FFCC33">Status</td>
				 <td width="5%" bgcolor="#FFCC33">SMT</td>
				 <td width="10%" bgcolor="#FFCC33">Prodi</td>
				 <td width="5%" bgcolor="#FFCC33">Pilih</td>
				</tr>
				{foreach name=test1 item="krs" from=$MA_PILIH}
				<tr>
				 <td>{$smarty.foreach.test1.iteration}</td>
				 <td>{$krs.KD_MATA_KULIAH}</td>
				 <td>{$krs.NM_MATA_KULIAH}</td>
				 <td>{$krs.KREDIT_SEMESTER}</td>
				 <td>{$krs.NAMA_KELAS}</td>
				 <td>{$krs.STATUS}</td>
				 <td>{$krs.SMT}</td>
				 <td>{$krs.PRODI}</td>
				 <td><center><input name="mk{$smarty.foreach.test1.iteration}" type="checkbox" value="{$krs.ID_KELAS_MK}" {$krs.TANDA} /></center></td>
				</tr>
			    {foreachelse}
        		<tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
        		{/foreach}
				<input type="hidden" name="counter" value="{$smarty.foreach.test1.iteration}" >
		</table>
		<p></p>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td colspan="8" bgcolor="#FFCC33"><center>DAFTAR MAHASISWA</center></td>
				</tr>
				<tr>
			     <td width="3%" bgcolor="#FFCC33">No</td>
				 <td width="5%" bgcolor="#FFCC33">NIM</td>
				 <td width="20%" bgcolor="#FFCC33">Nama Mhs</td>
				 <td width="5%" bgcolor="#FFCC33">Prodi</td>
				 <td width="5%" bgcolor="#FFCC33">Status</td>
				 <td width="5%" bgcolor="#FFCC33">Agama</td>
				 <td width="5%" bgcolor="#FFCC33">Bayar</td>
				 <td width="5%" bgcolor="#FFCC33">Pilih</td>
				</tr>
				{foreach name=test3 item="mhs" from=$MHS_PILIH}
				{if $mhs.STATUSBAYAR=='BELUM'}
				<tr bgcolor="red">
				{/if}
				 <td>{$smarty.foreach.test3.iteration}</td>
				 <td>{$mhs.NIM_MHS}</td>
				 <td>{$mhs.NM_PENGGUNA}</td>
				 <td>{$mhs.PRODI}</td>
				 <td>{$mhs.STATUS}</td>
				 <td>{$mhs.NM_AGAMA}</td>
				 <td>{$mhs.STATUSBAYAR}</td>
				 {if $mhs.STATUSBAYAR=='SUDAH'}
				 <td><center><input name="mhs{$smarty.foreach.test3.iteration}" type="checkbox" value="{$mhs.ID_MHS}" {$mhs.TANDA} /></center></td>
				 {else}
				 <td></td>
				 {/if}
				</tr>
			    {foreachelse}
        		<tr><td colspan="7"><em>Data tidak ditemukan</em></td></tr>
        		{/foreach}
				<input type="hidden" name="counter1" value="{$smarty.foreach.test3.iteration}" >
				<tr>
				<td colspan="6"></td><td><input type="submit" name="PROSES" value="PROSES"></td>
				</tr>
		</table>
</form>
</div>
<script>$('form').validate();</script>


