<div class="center_title_bar"> Info Pengisian Aktifitas Semester (Prodi) </div>

<p>Langkah-langkah mengisi Aktifitas Semester meliputi :</p>
<table width="660" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="38" bgcolor="#FFCC33"><div align="center">No</div></td>
    <td width="178" bgcolor="#FFCC33"><div align="center">Modul</div></td>
    <td width="434" bgcolor="#FFCC33"><div align="center">Uraian</div></td>
  </tr>
  <tr>
    <td><center>1</center></td>
    <td>Usulan Mata Ajar </td>
    <td>Difungsikan untuk menawarkan Mata Ajar yang akan diselenggarakan dalam satu semester, meliputi Mata Ajar dan Jadwal </td>
  </tr>
  <tr>
    <td><center>2</center></td>
    <td>PJMA</td>
    <td>Difungsikan untuk menentukan Pengampu Mata Ajar dan Tim Pengajar </td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Penawaran KRS</td>
    <td>Difungsikan untuk menawarkan Mata Ajar yang telah diusulkan pada Usulan Mata Ajar yang dapat diambil oleh Mahasiswa dalam proses KRS </td>
  </tr>
  <tr>
    <td><center>4</center></td>
    <td>Monitoring Kelas</td>
    <td>Difungsikan untuk memonitoring Mata Ajar dalam proses KRS </td>
  </tr>
  <tr>
    <td><center>5</center></td>
    <td>Dosen Wali</td>
    <td>Difungsikan untuk menentukan perwalian Mahasiswa dan Manajemen Perwalian </td>
  </tr>
</table>
