
<div class="center_title_bar">Proses Insert Nilai [Transfer/Lainnya] : {$nim} - {$nama} </div> 
<form action="nilai-transfer-fk.php" method="post">
<input type="hidden" name="action" value="viewdetail" >
     <table width="100%" border="1">
        <tr>
               <td>NIM</td>
               <td>:</td>
               <td><input name="nim" type="text"></td>
			   <td><input type="submit" name="Submit" value="Tampil"></td>
        </tr>
	</table>
</form>	
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">INSERT NILAI</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
			 <td width="5%" bgcolor="#FFCC33"><center>No</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>Kode MA</center></td>
			 <td width="20%" bgcolor="#FFCC33"><center>Nama MA</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>SKS</center></td>
             <td width="10%" bgcolor="#FFCC33"><center>Smt</center></td>
			 <td width="5%" bgcolor="#FFCC33"><center>Nilai</center></td>
		  </tr>
		  {foreach name=test item="list" from=$DETAIL_TRANSFER}
			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.KD_MATA_KULIAH}</td>
			 <td>{$list.NM_MATA_KULIAH}</td>
             <td>{$list.KREDIT_SEMESTER}</td>
             <td>{$list.SMT}</td>
			 <td>{$list.NILAI_HURUF}</td>
			</tr>
		  {/foreach}
</table>
</div>


<div>

<div class="panel" id="panel2" style="display: {$disp2} ">
<form action="nilai-transfer-fk.php" method="post" name="id_konversi_nilai" id="id_konversi_nilai">
<input type="hidden" name="action" value="proses" >
<input type="hidden" name="id_mhs" value="{$data_mhs.ID_MHS}" >
<input type="hidden" name="nim" value="{$nim}" >

	<table width="70%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        	<td width="5%">MK </td>
            <td width="30%">
            	<select name="kd_konversi" class="required">
                	<option value=""> ---------- </option>
                    {foreach $kd_konversi as $data}
                    	<option value="{$data.ID_KURIKULUM_MK}">{$data.TAHUN} - {$data.KD_MATA_KULIAH} - {$data.NM_MATA_KULIAH} - {$data.KREDIT_SEMESTER}</option>
                    {/foreach}
                </select>
			</td>
			<td width="5%">
				Nilai :<input type="text" name="nilai" maxlength="2" size="2" class="required"/>

            </td>
        </tr>
		<tr>
        	<td width="5%">SMT </td>
            <td width="30%">
            	<select name="smt" class="required">
                	<option value=""> ---------- </option>
                    {foreach $smtpil as $data}
                    	<option value="{$data.ID_SEMESTER}">{$data.SMT}</option>
                    {/foreach}
                </select>
			</td>
        </tr>
        <tr>
        	<td colspan="2" style="text-align:center">
            	<input type="submit" name="Submit" value="Proses">
            </td>
        </tr>
    </table>
</form>	
</div>


	
{literal}
    <script>
			$("#id_konversi_nilai").validate();
    </script>
{/literal}
    