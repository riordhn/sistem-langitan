
<div class="center_title_bar">EDIT NILAI : {$nim} - {$nama}</div> 
<form action="edit-nilai-fk.php" method="post">
<input type="hidden" name="action" value="viewdetail" >
     <table width="100%" border="1">
        <tr>
               <td>NIM</td>
               <td>:</td>
               <td><input name="nim" type="text"></td>
			   <td><input type="submit" name="Submit" value="Tampil"></td>
        </tr>
	</table>
</form>	

<div id="tabs" style="width:100%">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">EDIT NILAI</div>
</div>

<div class="panel" id="panel1" style="width:95%;display: {$disp1}">
<p> </p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
			 <td width="3%" bgcolor="#FFCC33"><center>No</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>Kode MA</center></td>
			 <td width="20%" bgcolor="#FFCC33"><center>Nama MA</center></td>
             <td width="5%" bgcolor="#FFCC33"><center>SKS</center></td>
             <td width="10%" bgcolor="#FFCC33"><center>Smt</center></td>
			 <td width="5%" bgcolor="#FFCC33"><center>Nilai</center></td>
			 <td width="13%" bgcolor="#FFCC33"><center>Aksi</center></td>
		  </tr>
		  {foreach name=test item="list" from=$T_NILAI}
			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.KD_MATA_KULIAH}</td>
			 <td>{$list.NM_MATA_KULIAH}</td>
             <td>{$list.KREDIT_SEMESTER}</td>
             <td>{$list.SMT}</td>
			 <td>{$list.NILAI_HURUF}</td>
			 {if $list.STATUS==1 }
			 <td height="0" align="center"><input name="Button" type="button" class="button" onClick="location.href='#perkuliahan-updnilai!edit-nilai-fk.php?action=viewproses&id_pmk={$list.ID_PENGAMBILAN_MK}&id_log={$list.ID_LOG_GRANTED_NILAI}&nim={$nim}';disableButtons()" onMouseOver="window.status='Click untuk Insert';return true" onMouseOut="window.status='Edit Nilai'" value="EDIT" /> 
			 <input name="Button" type="button" class="button" onClick="location.href='#perkuliahan-updnilai!edit-nilai-fk.php?action=delete&id_pmk={$list.ID_PENGAMBILAN_MK}';disableButtons()" onMouseOver="window.status='Click untuk Insert';return true" onMouseOut="window.status='Delete Nilai'" value="Delete" />
			 </td> 
			 {else}
			 <td></td>
			 {/if}
			</tr>
		  {/foreach}
</table>
</div>


<div>

<div class="panel" id="panel2" style="display: {$disp2} ">
<p> </p>
<form action="edit-nilai-fk.php" method="post" name="id_edit_nilai" id="id_edit_nilai">
<input type="hidden" name="action" value="proses" >
<input type="hidden" name="id_pengambilan_mk" value="{$datamk.ID_PENGAMBILAN_MK}" >

	<table>
        <tr>
        	<td>Kode MA</td>
            <td>{$datamk.KD_MATA_KULIAH}</td>
        </tr>
		<tr>
        	<td>Nama MA</td>
            <td>{$datamk.NM_MATA_KULIAH}</td>
        </tr>
		<tr>
        	<td>SKS</td>
            <td>{$datamk.KREDIT_SEMESTER}</td>
        </tr>
		<tr>
        	<td>Nilai Lama</td>
            <td>{$datamk.NILAI_HURUF}</td>
        </tr>
		<tr>
        	<td>Semester</td>
            <td>{$datamk.SMT}</td>
        </tr>
        <tr>
        	<td>Nilai Baru</td>
            <td>
            	<input type="text" name="nilai" maxlength="2" size="2" class="required"/>
            </td>
        </tr>
        <tr>
        	<td colspan="2" style="text-align:center">
            	<input type="hidden" value="{$nim}" name="nim" />
				<input type="hidden" value="{$log}" name="log" />
            	<input type="submit" name="Submit" value="Proses">
            </td>
        </tr>
    </table>
</form>	
</div>


	
{literal}
    <script>
			$("#id_edit_nilai").validate();
    </script>
{/literal}
    