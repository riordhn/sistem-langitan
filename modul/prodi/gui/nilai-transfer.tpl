
<div class="center_title_bar">Proses Insert Nilai [Transfer/Lainnya]</div> 

<div id="tabs">
	<div id="tab1" {if $smarty.get.action == ''}class="tab_sel"{else}class="tab"{/if} align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" {if $smarty.get.action == 'viewproses'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Konversi</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
			 <td width="2%" bgcolor="#FFCC33"><center>No</center></td>
             <td bgcolor="#FFCC33"><center>NIM</center></td>
			 <td bgcolor="#FFCC33"><center>Nama Mhs</center></td>
			 <td bgcolor="#FFCC33"><center>Prodi</center></td>
             <td bgcolor="#FFCC33">Jalur Masuk</td>
             <td bgcolor="#FFCC33"><center>Total MK Diakui</center></td>
             <td bgcolor="#FFCC33"><center>Total SKS Diakui</center></td>
			 <td bgcolor="#FFCC33"><center>AKSI</center></td>
		  </tr>
		  {foreach name=test item="list" from=$detail_transfer}
			<tr>
			 <td>{$smarty.foreach.test.iteration}</td>
             <td>{$list.NIM_MHS}</td>
			 <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.PRODI}</td>
             <td>{$list.NM_JALUR}</td>
             <td>{$list.TOTAL_MATKUL}</td>
			 <td>{$list.TOTAL_SKS}</td>
			 <td height="0" align="center"><input name="Button" type="button" class="button" onClick="location.href='#utility-transfernil!nilai-transfer.php?action=viewproses&id_mhs={$list.ID_MHS}&smt={$list.SEMESTER_PROSES}';disableButtons()" onMouseOver="window.status='Click untuk Insert';return true" onMouseOut="window.status='Edit Nilai'" value="INSERT NILAI" /> </td> 
			</tr>
		  {/foreach}
</table>
</div>


<div>

<div class="panel" id="panel2" style="display: {$disp2} ">
<p> </p>
<table>
        <tr>
            <td>NIM</td>
            <td>{$datamhs.NIM_MHS}</td>
        </tr>
        <tr>
            <td>Nama Mhs</td>
            <td>{$datamhs.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Prodi</td>
            <td>{$datamhs.PRODI}</td>
        </tr>
        
        <tr>
            <td>Diakui Pada Semester</td>
            <td>{$datamhs.SMT}</td>
        </tr>
        <tr>
            <td>Total MK Diakui</td>
            <td>{$datamhs.TOTAL_MATKUL}</td>
        </tr>
        <tr>
            <td>Total SKS Diakui</td>
            <td>{$datamhs.TOTAL_SKS}</td>
        </tr>
</table>

        <form action="nilai-transfer.php?action=viewproses&id_mhs={$datamhs.ID_MHS}&smt={$datamhs.ID_SEMESTER}" method="post">
            <table>
                <tbody>
                    <tr>
                        <td colspan="2">
                            <label><strong>Kurikulum : </strong></label>
                            <select name="id_kurikulum" onchange="javascript: $(this).submit();">
                                <option value="">-- Pilih Kurikulum --</option>
                                {foreach $kurikulum_set as $kur}
                                    <option value="{$kur.ID_KURIKULUM}" {if isset($smarty.post.id_kurikulum)}{if $smarty.post.id_kurikulum == $kur.ID_KURIKULUM}selected{/if}{/if}>{$kur.NAMA}</option>
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>

{if isset($kd_konversi)}
<h3>*) Nilai Matkul Yang Bukan Termasuk Pengakuan Nilai Biarkan Kosong..</h3>
{else if isset($nilai_konversi)}

	<h3>Daftar nilai diakui</h3>
	<table>
		<thead>
			<tr>
				<th></th>
				<th>Kode Mata Kuliah</th>
				<th>Nama Mata Kuliah</th>
				<th>SKS</th>
				<th>Nilai</th>
			</tr>
		</thead>
		<tbody>
			{foreach $nilai_konversi as $nk}
				<tr>
					<td>{$nk@index + 1}</td>
					<td>{$nk.KD_MATA_KULIAH}</td>
					<td>{$nk.NM_MATA_KULIAH}</td>
					<td class="center">{$nk.KREDIT_SEMESTER}</td>
					<td class="center">{$nk.NILAI_HURUF}</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="5">Nilai konversi belum dimasukkan</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	
{/if}

<form action="nilai-transfer.php?action=viewproses&id_mhs={$smarty.get.id_mhs}&smt={$smarty.get.smt}" method="post" name="id_konversi_nilai" id="id_konversi_nilai">
<input type="hidden" name="action" value="proses" >
<!-- <input type="hidden" name="counter" value="{$datamhs.TOTAL_MATKUL}" > -->
<input type="hidden" name="id_mhs" value="{$datamhs.ID_MHS}" >
<input type="hidden" name="semester_diakui" value="{$datamhs.ID_SEMESTER}" >
<!-- <input type="hidden" name="log_granted" value="{$datamhs.ID_LOG_GRANTED_TRANSFER}" > -->
	<table>

            {if isset($kd_konversi)}
                {foreach name=test item="data" from=$kd_konversi}
                    <tr>
                        <td>{$data.TAHUN} - {$data.KD_MATA_KULIAH} - {$data.NM_MATA_KULIAH} - {$data.KREDIT_SEMESTER} Sks</td>
                        <td>
                            <input type="hidden" name="kd_konversi{$data@index + 1}" value="{$data.ID_KURIKULUM_MK}" >
                            Nilai Huruf : <input type="text" name="nilai{$data@index + 1}" maxlength="2" size="2" value="{$data.NILAI_HURUF}" />
                            <input type="hidden" name="counter" value="{$data@index + 1}" >
                        </td>
                    </tr>
                {/foreach}

                <!-- {for $x = 1 to $datamhs.TOTAL_MATKUL}
                <tr>
                    <td>MK ke - {$x}</td>
                    <td>
                        <select name="kd_konversi{$x}" class="required">
                            <option value=""> ---------- </option>
                            {foreach $kd_konversi as $data}
                                <option value="{$data.ID_KURIKULUM_MK}">{$data.TAHUN} - {$data.KD_MATA_KULIAH} - {$data.NM_MATA_KULIAH} - {$data.KREDIT_SEMESTER}</option>
                            {/foreach}
                        </select>
                        Nilai - {$x}: <input type="text" name="nilai{$x}" maxlength="2" size="2" class="required"/>

                    </td>
                </tr>
                {/for} -->

                <tr>
                    <td colspan="2" style="text-align:center">
                        <input type="submit" name="Submit" value="Proses">
                    </td>
                </tr>
            {/if}
    </table>
</form>	
</div>


	
{literal}
    <script>
			$("#id_konversi_nilai").validate();
    </script>
{/literal}
    