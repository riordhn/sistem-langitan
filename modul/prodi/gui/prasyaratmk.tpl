<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
	/*
 $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
   7: { sorter: false }
		}
		}
	);
	*/
}
);

function lookup(inputString) {
if(inputString.length == 0) {
// Hide the suggestion box.
$('#suggestions').hide();
} else {
$.post("gp.php", {queryString: ""+inputString+""}, function(data){
if(data.length >0) {
$('#suggestions').show();
$('#autoSuggestionsList').html(data);
}
});
}
} // lookup

function fill1(thisValue) {
$('#kode_matkul').val(thisValue);
setTimeout("$('#suggestions').hide();", 200);
}
function fill2(thisValue) {
$('#nama_matkul').val(thisValue);
setTimeout("$('#suggestions').hide();", 200);
}
function fill3(thisValue) {
$('#nama_en_matkul').val(thisValue);
setTimeout("$('#suggestions').hide();", 200);
}
function ambil(ID_KURIKULUM){
		$.ajax({
		type: "GET",
        url: "prasyaratmk.php?action=setting&id_kmk="+$("#id_kmk").val(),
        data: "id_kp="+$("#id_kurikulum").val(),
        cache: false,
        success: function(data){
			$('#content').html(data);
        }
        });
	}
</script>
{/literal}

<div class="center_title_bar">Daftar Prasyarat Mata Ajar </div>
	<div id="tabs">
		<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
		<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Update</div>
		<div id="tab3" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Setting</div>
  	</div>

<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<form action="prasyaratmk.php" method="post">
<input type="hidden" name="action" value="view" />
	<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>Tahun Kurikulum</td>
			<td><center>:</center></td>
			<td>
				<select name="thkur">
				{foreach item="kur" from=$T_KUR}
				{html_options values=$kur.ID_KURIKULUM output=$kur.NAMA selected=$IDKP}
				{/foreach}
				</select>
			</td>
			<td><input type="submit" name="View" value="View"></td>
		</tr>
	</table>
</form>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Kode</th>
			<th>Nama Mata Ajar</th>
			<th>SKS</th>
			<th>Syarat 1</th>
			<th>Syarat 2</th>
			<th>Syarat 3</th>
			<th>Syarat 4</th>
			<th>Syarat 5</th>
			<th class="noheader" width="5%">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=tmk item="list" from=$T_MK}
		<tr>
			<td><a href="prasyaratmk.php?action=setting&id_kmk={$list.ID_KURIKULUM_MK}&id_kp={$list.ID_KURIKULUM}" onclick="displayPanel('3')">{$list.KD_MATA_KULIAH}</a></td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td>{$list.G1}</td>
			<td>{$list.G2}</td>
			<td>{$list.G3}</td>
			<td>{$list.G4}</td>
			<td>{$list.G5}</td>
			<td><center><a href="prasyaratmk.php?action=del&id_kmk={$list.ID_KURIKULUM_MK}">Delete</a></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="prasyaratmk.php" method="post" >
<input type="hidden" name="action" value="update1" />
<input type="hidden" name="id_km" value="{$IDKM}" />
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3"><strong> PERUBAHAN STATUS MATA AJAR </strong></td>
	</tr>
	{foreach item="ganti" from=$T_MK1}
	<tr>
		<td>Kode Mata Ajar</td>
		<td><center>:</center></td>
		<td>{$ganti.KD_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar</td>
		<td><center>:</center></td>
		<td>{$ganti.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar (English)</td>
		<td><center>:</center></td>
		<td>{$ganti.NM_MATA_KULIAH_EN}</td>
	</tr>
	<tr>
		<td>SKS TTL</td>
		<td><center>:</center></td>
		<td>{$ganti.KREDIT_SEMESTER}</td>
	</tr>
	<tr>
		<td>Tingkat Semester</td>
		<td><center>:</center></td>
		<td>{$ganti.TINGKAT_SEMESTER}</td>
	</tr>
	<tr>
		<td>Kelompok Mata Ajar</td>
		<td><center>:</center></td>
		<td>{$ganti.NM_KELOMPOK_MK}</td>
	</tr>
	<tr>
		<td>Minat Prodi</td>
		<td><center>:</center></td>
		<td>{$ganti.NM_PRODI_MINAT}</td>
	</tr>
	<tr>
		<td>Tahun Kurikulum</td>
		<td><center>:</center></td>
		<td>{$ganti.TAHUN_KURIKULUM}</td>
	</tr>
	<tr>
		<td>Status</td>
		<td><center>:</center></td>
		<td>
			<select name="status_mk" id="status_mk">
			{foreach item="list" from=$ST_MK}
			{html_options values=$list.ID_STATUS_MK output=$list.NM_STATUS_MK selected=$ganti.ID_STATUS_MK}
			{/foreach}
			</select>
		</td>
	</tr>
	{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>
<p><input type="submit" name="simpan" value="Simpan"></p>
</form>
</div>

{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
 $("#myTable1").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
   5: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="panel" id="panel3" style="display: {$disp3}">
<p> </p>
<form action="prasyaratmk.php" method="post">
<input type="hidden" name="action" value="insert" />
<input type="hidden" name="id_kmk" id="id_kmk" value="{$id_kmk}" />
<table class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Kode</th>
			<th>Nama Mata Ajar</th>
			<th>SKS</th>
		</tr>
	</thead>
	<tbody>
	{foreach name="test" item="list" from=$INSERT_MK}
		<tr>
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td>{$list.KREDIT_SEMESTER}</td>
		</tr>
	{foreachelse}
		<tr><td colspan="3"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
</table>
<table>
	<tr>
		<td>Tahun Kurikulum</td>
			<td><center>:</center></td>
			<td>
				<select name="id_kur" id="id_kurikulum" onChange="ambil(23)">
				{foreach item="kur" from=$T_KUR}
				{html_options values=$kur.ID_KURIKULUM output=$kur.NAMA selected=$id_kp}
				{/foreach}
				</select>
		</td>
	</tr>
</table>
<table>
	<!-- <tr>
		<td>Status</td>
		<td><center>:</center></td>
		<td>
			<select name="pilih" id="pilih">
				<option selected="yes" value="1">DAN</option>
				<option value="0">ATAU</option>
			</select>
		</td>
		<td><input type="submit" name="proses" value="Proses"></td>
	</tr> -->
	<tr>
		<td>Kelompok Syarat</td>
		<td><center>:</center></td>
		<td>
			<select name="pilih" id="pilih">
				<option selected="yes" value="1">Syarat 1</option>
				<option value="2">Syarat 2</option>
				<option value="3">Syarat 3</option>
				<option value="4">Syarat 4</option>
				<option value="5">Syarat 5</option>\
			</select>
		</td>
		<td><input type="submit" name="proses" value="Proses"></td>
	</tr>
</table>
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="10%">Kode</th>
			<th width="35%">Nama Mata Ajar</th>
			<th width="20%">Kelompok MA</th>
			<th width="10%">SKS</th>
			<th width="10%">Smt</th>
			<th width="10%">Tahun</th>
			<th class="noheader" width="10%">PILIH</th>
		</tr>
	</thead>
	<tbody>
	{foreach name="test" item="list" from=$MK_PILIH}
		<tr>
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.NM_KELOMPOK_MK}</center></td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td><center>{$list.TINGKAT_SEMESTER}</center></td>
			<td><center>{$list.TAHUN_KURIKULUM}</center></td>
			<td><center><input name="pilihmk{$smarty.foreach.test.iteration}" type="checkbox" value="{$list.ID_KURIKULUM_MK}" /></center></td>
		</tr>
	{foreachelse}
			<tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	<input type="hidden" name="counter" value="{$smarty.foreach.test.iteration}" >
</table>
</form>
</div>

 <script>$('form').validate();</script>