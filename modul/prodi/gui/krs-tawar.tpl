{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable").tablesorter(
		{
		sortList: [[1,0], [3,0]],
		headers: {
      7: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="center_title_bar">Penawaran KRS</div>
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Penawaran</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
<form action="krs-tawar.php" method="post">
<input type="hidden" name="action" value="tampil" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
  <tr>
	 <td>
		Tahun Akademik :
		<select name="smt">
		<option value=''>-- PILIH THN AKD --</option>
		{foreach item="smt" from=$T_ST}
		{html_options values=$smt.ID_SEMESTER output=$smt.SMT selected=$id_smt}
		{/foreach}
		</select>
		<input type="submit" name="View" value="View">
	</td>
  </tr>
</table>
</form>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="5%">Kode</th>
			<th width="33%">Nama Mata Ajar</th>
			<th width="5%">SKS</th>
			<th width="5%">KLS</th>
			<th width="8%">Hari</th>
			<th width="12%">Jam</th>
			<th width="20%">Prodi Asal</th>
			<th class="noheader" width="12%">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=test item="list" from=$T_MK}
		<tr>
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td><center>{$list.NAMA_KELAS}</center></td>
			<td>{$list.NM_JADWAL_HARI}</td>
			<td><center>{$list.NM_JADWAL_JAM}</center></td>
			<td>{$list.PRODIASAL}</td>
			<td><center><a href="krs-tawar.php?action=del&id_krs_prodi={$list.ID_KRS_PRODI}&smt={$id_smt}">Hapus</a></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
</table>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable1").tablesorter(
		{
		sortList: [[1,0], [3,0]],
		headers: {
      5: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="krs-tawar.php" method="post" >
<input type="hidden" name="action" value="penawaran" >
</form>
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="10%">Kode</th>
			<th width="43%">Nama Mata Ajar</th>
			<th width="5%">SKS</th>
			<th width="5%">Kelas</th>
			<th width="25%">Prodi Asal</th>
			<th class="noheader" width="12%">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=test item="list" from=$TAWAR}
		<tr>
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td><center>{$list.NAMA_KELAS}</center></td>
			<td>{$list.PRODIASAL}</td>
			<td><center><a href="krs-tawar.php?action=add&id_mk={$list.ID_KELAS_MK}&smt={$id_smt}">Ditawarkan</a></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>
</div>
