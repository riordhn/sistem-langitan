<div class="center_title_bar"> Info Pengisian Desertasi </div>

<p>Langkah-langkah mengisi Desertasi meliputi :</p>
<table width="660" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="38" bgcolor="#FFCC33"><div align="center">No</div></td>
    <td width="178" bgcolor="#FFCC33"><div align="center">Modul</div></td>
    <td width="434" bgcolor="#FFCC33"><div align="center">Uraian</div></td>
  </tr>
  <tr>
    <td><center>1</center></td>
    <td>MKPD </td>
    <td>Difungsikan untuk Memonitoring Pengambilan MKPD </td>
  </tr>
  <tr>
    <td><center>2</center></td>
    <td>Ujian Qualifikasi</td>
    <td>Difungsikan untuk Memonitoring Ujian Qualifikasi </td>
  </tr>
  <tr>
    <td><center>3</center></td>
    <td>Ujian Proposal</td>
    <td>Difungsikan untuk Memonitoring Ujian Proposal </td>
  </tr>
  <tr>
    <td><center>4</center></td>
    <td>Ujian Kelayakan</td>
    <td>Difungsikan untuk memonitoring Ujian Kelayakan </td>
  </tr>
  <tr>
    <td><center>5</center></td>
    <td>Ujian Tertutup</td>
    <td>Difungsikan untuk Memonitoring Ujian Tertutup </td>
  </tr>
  <tr>
    <td><center>6</center></td>
    <td>Ujian Terbuka</td>
    <td>Difungsikan untuk Memonitoring Ujian Terbuka </td>
  </tr>
</table>
