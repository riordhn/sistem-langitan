{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
            2: { sorter: false },
            3: { sorter: false },
            4: { sorter: false },
            5: { sorter: false },
            6: { sorter: false },
            7: { sorter: false },
            8: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}

<div class="center_title_bar">Tambah MK Setara (Kelompok MK {$mk_penyetaraan.NM_MK_PENYETARAAN})</div>

<a href="penyetaraan-mk.php"><< Kembali</a>
<br/>
<br/>

<form action="penyetaraan-mk.php" method="post" >

<input type="hidden" name="mode" value="save-mk-setara" >
<input type="hidden" name="id_mk_penyetaraan" value="{$mk_penyetaraan.ID_MK_PENYETARAAN}" >


<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
  	<tr>
  		<th>Kode</th>
  		<th width="35%">Nama Mata Ajar</th>
  		<th>SKS</th>
  		<th title="SKS Tatap Muka">SKS<br/>TM</th>
  		<th title="SKS Praktikum">SKS<br/>Prak.</th>
  		<th title="SKS Praktikum Lapangan">SKS<br/>Prak.Lap.</th>
  		<th title="SKS Simulasi">SKS<br/>Sim.</th>
  		<th>Jenis MK</th>
  		<th>Kelompok MK</th>
  		<th>Status<br/>Sync</th>
  		<th><center>Pilih</center></th>
  	</tr>
  {foreach name=matkulset item="list" from=$MATKULSET}
	  
	  <tr {if $list.KREDIT_SEMESTER == '' or $list.KREDIT_SEMESTER == '0'}bgcolor="#FE2E2E"{/if}{if $list.RELASI_MATKUL > 1}bgcolor="#f6ff01"{/if}>
		<td>{$list.KD_MATA_KULIAH}</td>
		<td>{$list.NM_MATA_KULIAH}</td>
		<td>{$list.KREDIT_SEMESTER}</td>
		<td>{$list.KREDIT_TATAP_MUKA}</td>
		<td>{$list.KREDIT_PRAKTIKUM}</td>
		<td>{$list.KREDIT_PRAK_LAPANGAN}</td>
		<td>{$list.KREDIT_SIMULASI}</td>
		<td>{$list.NM_JENIS_MK}</td>
		<td>{$list.NM_KELOMPOK_MK}</td>
		<td class="center">{if $list.RELASI_FEEDER == 0}-{else}Ya{/if}</td>
	    <td><center><input name="mk{$smarty.foreach.matkulset.iteration}" type="checkbox" value="{$list.ID_MATA_KULIAH}" {$list.TANDA} /></center></td>
	  </tr>
     {foreachelse}
        <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
     {/foreach}
     <input type="hidden" name="counter" value="{$smarty.foreach.matkulset.iteration}" >
		<tr>
			<td colspan="10"></td><td><input type="submit" name="PROSES" value="PROSES"></td>
		</tr>
</table>


{literal}
 <script>$('form').validate();</script>
{/literal}
