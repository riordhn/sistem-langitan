<div class="center_title_bar">ID MK Setara : {$mk_setara.ID_MK_SETARA}</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="penyetaraan-mk.php?mode=edit-delete-mk&id_mk_penyetaraan={$mk_setara.ID_MK_PENYETARAAN}" method="post">
<input type="hidden" name="mode" value="delete-mk" />
<input type="hidden" name="id_mk_setara" value="{$mk_setara.ID_MK_SETARA}" />
<table>
    <tr>
        <th colspan="2">Detail MK Setara</th>
    </tr>
    <tr>
        <td>ID</td>
        <td>{$mk_setara.ID_MK_SETARA}</td>
    </tr>
    <tr>
        <td>Nama Kelompok MK Setara</td>
        <td>{$mk_setara.NM_MK_PENYETARAAN}</td>
    </tr>
    <tr>
        <td>Kode Mata Kuliah</td>
        <td>{$mk_setara.KD_MATA_KULIAH}</td>
    </tr>
    <tr>
        <td>Nama Mata Kuliah</td>
        <td>{$mk_setara.NM_MATA_KULIAH}</td>
    </tr>
    <tr>
        <td>Dibuat Pada</td>
        <td>{$mk_setara.CREATED_BY}</td>
    </tr>
    <tr>
        <td>Dibuat Oleh</td>
        <td>{$mk_setara.NM_PENGGUNA}</td>
    </tr>
    <tr>
        <td>Diubah Pada</td>
        <td>{$mk_setara.UPDATED_BY}</td>
    </tr>
    <tr>
        <td>Diubah Oleh</td>
        <td>{$mk_setara.PENGGUNA_UPDATE}</td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Hapus" />
        </td>
    </tr>
</table>
</form>

<a href="penyetaraan-mk.php?mode=edit-delete-mk&id_mk_penyetaraan={$mk_setara.ID_MK_PENYETARAAN}">Kembali</a>