<div class="center_title_bar">Kelompok MK Setara</div>

{if isset($mk_penyetaraan_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Kelompok MK Setara</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $mk_penyetaraan_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td><strong>{$uk.NM_MK_PENYETARAAN}</strong></td>
					<td class="center">
						<a href="penyetaraan-mk-master.php?mode=edit&id_mk_penyetaraan={$uk.ID_MK_PENYETARAAN}" style="color: blue">Edit</a>
						{if $uk.JML_JOIN == 0}
							<a href="penyetaraan-mk-master.php?mode=delete&id_mk_penyetaraan={$uk.ID_MK_PENYETARAAN}" style="color: red">Delete</a>
						{/if}
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="9" class="center">
					<a href="penyetaraan-mk-master.php?mode=add">Tambah Kelompok MK Setara</a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}