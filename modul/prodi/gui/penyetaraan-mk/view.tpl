<div class="center_title_bar">Kelompok MK Setara</div>

{if isset($mk_penyetaraan_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Kelompok MK Setara</th>
				<th>Jumlah MK Setara</th>
				<th align="center">Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $mk_penyetaraan_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td><strong>{$uk.NM_MK_PENYETARAAN}</strong></td>
					{if $uk.JML_JOIN == ''}
						<td><center>0</center></td>
					{else}
						<td><center>{$uk.JML_JOIN}</center></td>
					{/if}
					<td class="center">
						<a href="penyetaraan-mk.php?mode=add-mk&id_mk_penyetaraan={$uk.ID_MK_PENYETARAAN}" style="color: blue">Tambah MK Setara</a>
						{if $uk.JML_JOIN > 0}
							<a href="penyetaraan-mk.php?mode=edit-delete-mk&id_mk_penyetaraan={$uk.ID_MK_PENYETARAAN}" style="color: red">Hapus MK Setara</a>
						{/if}
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
{/if}