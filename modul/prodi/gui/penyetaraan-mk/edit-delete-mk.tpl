<div class="center_title_bar">MK Setara (Kelompok MK {$mk_penyetaraan.NM_MK_PENYETARAAN})</div>

<a href="penyetaraan-mk.php"><< Kembali</a>
<br/>
<br/>

{if isset($mk_setara_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Kode Mata Kuliah</th>
				<th>Nama Mata Kuliah</th>
				<th>Dibuat Pada</th>
				<th>Dibuat Oleh</th>
				<th>Diubah Pada</th>
				<th>Diubah Oleh</th>
				<th align="center">Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $mk_setara_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td><strong>{$uk.KD_MATA_KULIAH}</strong></td>
					<td><center>{$uk.NM_MATA_KULIAH}</center></td>
					<td>{$uk.CREATED_ON}</td>
					<td>{$uk.NM_PENGGUNA}</td>
					<td>{$uk.UPDATED_ON}</td>
					<td>{$uk.PENGGUNA_UPDATE}</td>
					<td class="center">
						<a href="penyetaraan-mk.php?mode=delete-mk&id_mk_setara={$uk.ID_MK_SETARA}" style="color: red">Hapus</a>
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
{/if}