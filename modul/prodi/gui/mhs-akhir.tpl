{literal}
<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#myTable").tablesorter(
    {
    sortList: [[1,0]],
    }
  ); 
} 
);
</script>
{/literal}
<div class="center_title_bar"> Mahasiswa Tingkat Akhir </div>
<div class="panel" id="panel1">

<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
  <thead>
  <tr>
    <td width="25" bgcolor="#FFCC33"><div align="center">No.</div></td>
    <td width="38" bgcolor="#FFCC33"><div align="center">NIM</div></td>
    <td width="450" bgcolor="#FFCC33"><div align="center">Nama</div></td>
    <td width="35" bgcolor="#FFCC33"><div align="center">Angkatan</div></td>
     <td width="200" bgcolor="#FFCC33"><div align="center">SKS</div></td>
      <td width="200" bgcolor="#FFCC33"><div align="center">IPK</div></td>
  </thead>
  <tbody>
    {$nomer=1}
    {foreach name=test item="list" from=$mhs}
  <tr> 
  	<td>{$nomer++}</td> 
    <td>{$list.NIM_MHS}</td>
  	<td>{$list.NM_PENGGUNA} </td>
    <td>{$list.THN_ANGKATAN_MHS} </td>
    <td>{$list.SKS_TOTAL_MHS} </td>
    <td>{$list.IPK_MHS} </td>
  	
  	
    </tr>
{foreachelse}
    <tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
    {/foreach}
  </tbody>
</table>
</div>