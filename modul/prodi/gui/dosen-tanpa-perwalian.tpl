<script type="text/javascript">
$('#check-all').click(function() {
	var group = ':checkbox[class=' + $(this).attr('name') + ']';
	if(this.checked == false){
		$(group).attr('checked', false);
	}
	else{
		$(group).attr('checked', $(this).attr('checked'));		
	}
});


$("#thn_angkatan").change(function(){
		angkatan();	  
		$("#dataMhs").show();
});


function angkatan(){
	  $("#loading").show();
	  thn_angkatan = $("#thn_angkatan").val();
	 
	 $.ajax({
	  type: "POST",
	  url: "proses/dosen-tanpa-perwalian-proses.php",
	  data: "mode=view&thn_angkatan="+thn_angkatan,
	  cache: false,
	  success: function(data){
		  $('#tableBody').html(data);
		  $("#loading").hide();
	  }
	  });
}


$.metadata.setType("attr", "validate");

$("#finput").validate({
	rules: {
		thn_angkatan: "required",
		dosen: "required"
	},
	messages: {
		thn_angkatan: "Harus dipilih",
		dosen: "Pilih"
	},
	submitHandler: function() { 
		$("#loading").show();
		//dosen = $("input[@name='dosen']:checked").val();
		alert($("#finput").serialize());
		$.ajax({
		type: "POST",
		url: "proses/dosen-tanpa-perwalian-proses.php",
		data: "mode=input&"+$("#finput").serialize(),
		cache: false,
		success: function(data){
			alert(data);
			//alert("data sudah dimasukkan");
			$("#loading").hide();
			clearForm();
		}
		});
	}
});

function clearForm(){
		$("input[@name='dosen']").checked('false');
		$('#thn_angkatan').val('');
		var group = ':checkbox[class=' + $('#check-all').attr('name') + ']';
		$(group).attr('checked', false);
}
</script>
        
        <div class="center_title_bar">Dosen Tanpa Perwalian</div>  
        <img src="../../img/akademik_images/loading.gif" id="loading" style="display:none">
        <form name="finput" id="finput" action="" method="post">
        <table class="tb_dosen" width="50%" border="0" cellpadding="0" cellspacing="0" align="left">
          <tr>
            <td width="166" class="tb_frame">Tahun Angkatan</td>
            <td width="10" class="tb_frame">:</td>
            <td width="414" class="tb_frame">
            <select name="thn_angkatan" id="thn_angkatan">
              <option value="">-- Tahun Angkatan --</option>
			{for $i=$thn_skrg; $i>=($thn_skrg-15); $i--}
              <option value="{$i}">{$i}</option>
             {/for}
            </select>
            </td>
          </tr>
    	</table>
	    <br />
		<br />
        <br />
        <br />
        <br />
        <br />
	     <p align="left">Dosen yang belum mendapatkan perwalian</p>
	    <table width="80%" border="0" cellspacing="0" cellpadding="0" align="left">
          <tr class="left_menu">
            <td width="26%" bgcolor="#333333"><div align="center"><font color="#FFFFFF">NIP </font></div></td>
            <td width="62%" bgcolor="#333333"><div align="center"><font color="#FFFFFF">Nama Dosen </font> </div></td>
            <td width="12%" bgcolor="#333333"><div align="center"><font color="#FFFFFF">Set</font> </div></td>
          </tr>
          {foreach $dosen as $d}
          <tr>
            <td >{$d.NIP_DOSEN}</td>
            <td >{$d.NM_PENGGUNA}</td>
            <td align="center"><input type="radio" value="{$d.ID_DOSEN}"  name="dosen"/><br />
            </td>
          </tr>
          {/foreach}
          
        </table>
        <div id="dataMhs" style="display:none">
	    <table width="80%" border="0" cellspacing="0" cellpadding="0" align="left">
          <tr class="left_menu">
            <td bgcolor="#333333"><div align="center"><font color="#FFFFFF">NIM</font></div></td>
            <td bgcolor="#333333"><div align="center"><font color="#FFFFFF">Nama Mahasiswa </font> </div></td>
            <td bgcolor="#333333"><div align="center"><input type="checkbox" name="check-all" id="check-all" value="checkbox" class="check-all" /></div></td>
          </tr>
          <tbody id="tableBody">
          </tbody>
        </table>
		</div>
        </form>