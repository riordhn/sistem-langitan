<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!-- <script type="text/javascript" src="jquery-1.2.1.pack.js"></script> -->
{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
			3: { sorter: false },
			4: { sorter: false },
			5: { sorter: false },
			6: { sorter: false },
			7: { sorter: false }
		}
		}
	);
}
);

function lookup(inputString) {
if(inputString.length == 0) {
// Hide the suggestion box.
$('#suggestions').hide();
} else {
$.post("gp.php", {queryString: ""+inputString+""}, function(data){
if(data.length >0) {
$('#suggestions').show();
$('#autoSuggestionsList').html(data);
}
});
}
} // lookup

function fill1(thisValue) {
$('#kode_matkul').val(thisValue);
setTimeout("$('#suggestions').hide();", 200);
}
function fill2(thisValue) {
$('#nama_matkul').val(thisValue);
setTimeout("$('#suggestions').hide();", 200);
}
function fill3(thisValue) {
$('#nama_en_matkul').val(thisValue);
setTimeout("$('#suggestions').hide();", 200);
}
</script>
{/literal}

<div class="center_title_bar">Daftar Mata Ajar </div>
	<div id="tabs">
		<div id="tab1" {if $smarty.post.action == 'insertmk'}class="tab"{else if $smarty.get.action == ''}class="tab_sel"{else}class="tab"{/if} align="center" onclick="javascript: displayPanel('1');">Rincian</div>
		<div id="tab2" {if $smarty.get.action == 'viewup'}class="tab_sel"{else}class="tab"{/if} style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Update</div>
		<div id="tab3" {if $smarty.get.action == 'add' or $smarty.post.action == 'insertmk'}class="tab_sel"{else}class="tab"{/if}style="margin-left:1px;" align="center" onclick="javascript: displayPanel('3');">Insert MA</div>
  	</div>
<div class="panel" id="panel1" style="display: {$disp1}">
<p> </p>
<form action="daftarmk.php" method="post">
<input type="hidden" name="action" value="view" />
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
      <tr>
       <td>
			 	Kurikulum :
			  <select name="thkur">
			  	<option value="">-- Pilih Kurikulum --</option>
	 		  {foreach item="kur" from=$T_KUR}			  
			  		{if isset($smarty.request.thkur)}
						{html_options values=$kur.ID_KURIKULUM output=$kur.NAMA selected=$smarty.request.thkur }
					{else if $id_kur_prodi != ''}
						{html_options values=$kur.ID_KURIKULUM output=$kur.NAMA selected=$id_kur_prodi}
					{else}
						{html_options values=$kur.ID_KURIKULUM output=$kur.NAMA selected=$IDKUR}
					{/if}
	 		  {/foreach}
			  </select>
			  <input type="submit" name="View" value="View">
		   </td>
      </tr>
</table>
</form>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Kode</th>
			<th>Nama Mata Ajar</th>
			<th>English</th>
			<th title="Tatap Muka">SKS <br/> T.M.</th>
			<th title="Praktikum">SKS <br/> Prak.</th>
			<th title="Praktik Lapangan">SKS <br/> Prak. Lap.</th>
			<th title="Tutorial">SKS <br/> Tut.</th>
			<th title="Simulasi">SKS <br/> Sim.</th>
			<th>SKS <br/> Total</th>
			<th>Status</th>
			<th>Kelompok <br/> MA</th>
			<th>SMT</th>
			<!-- <th class="noheader" width="6%">Thn Kur</th> -->
			<th class="noheader">Aksi</th>
		</tr>
	</thead>
	<tbody>
      	{foreach name=tmk item="list" from=$T_MK}
      		{if $list.JML > 1}
			  	<tr bgcolor="#f6ff01">
					<td>{$list.KD_MATA_KULIAH}</td>
					<td>{$list.NM_MATA_KULIAH}</td>
					<td>{$list.NM_MATA_KULIAH_EN}</td>
					<td><center>{$list.KREDIT_TATAP_MUKA}</center></td>
					<td><center>{$list.KREDIT_PRAKTIKUM}</center></td>
					<td><center>{$list.KREDIT_PRAK_LAPANGAN}</center></td>
					<td><center>{$list.KREDIT_TUTOR}</center></td>
					<td><center>{$list.KREDIT_SIMULASI}</center></td>
					<td><center><strong>{$list.KREDIT_SEMESTER}</strong></center></td>
					<td>{$list.NM_STATUS_MK}</td>
					<td><center>{$list.NM_KELOMPOK_MK}</center></td>
					<td><center>{$list.TINGKAT_SEMESTER}</center></td>
					<!-- <td><center>{$list.TAHUN_KURIKULUM}</center></td> -->
					<td><center><a href="daftarmk.php?action=viewup&id_mk={$list.ID_KURIKULUM_MK}" onclick="displayPanel('3')">Update</a>
						{if $list.RELASI_PENGAMBILAN == 0 and $list.RELASI_KELAS == 0}
							<br><a href="daftarmk.php?action=del&id_mk={$list.ID_KURIKULUM_MK}">Delete</a></center>
						{/if}
					</td>
				</tr>
			{else}
				<tr {if $list.KREDIT_SEMESTER == 0}bgcolor="lightpink"{/if}>
					<td>{$list.KD_MATA_KULIAH}</td>
					<td>{$list.NM_MATA_KULIAH}</td>
					<td>{$list.NM_MATA_KULIAH_EN}</td>
					<td><center>{$list.KREDIT_TATAP_MUKA}</center></td>
					<td><center>{$list.KREDIT_PRAKTIKUM}</center></td>
					<td><center>{$list.KREDIT_PRAK_LAPANGAN}</center></td>
					<td><center>{$list.KREDIT_TUTOR}</center></td>
					<td><center>{$list.KREDIT_SIMULASI}</center></td>
					<td><center><strong>{$list.KREDIT_SEMESTER}</strong></center></td>
					<td>{$list.NM_STATUS_MK}</td>
					<td><center>{$list.NM_KELOMPOK_MK}</center></td>
					<td><center>{$list.TINGKAT_SEMESTER}</center></td>
					<!-- <td><center>{$list.TAHUN_KURIKULUM}</center></td> -->
					<td><center><a href="daftarmk.php?action=viewup&id_mk={$list.ID_KURIKULUM_MK}" onclick="displayPanel('3')">Update</a>
						{if $list.RELASI_PENGAMBILAN == 0 and $list.RELASI_KELAS == 0}
							<br><a href="daftarmk.php?action=del&id_mk={$list.ID_KURIKULUM_MK}">Delete</a></center>
						{/if}
					</td>
				</tr>
			{/if}
		{foreachelse}
			<tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="daftarmk.php" method="post" >
<input type="hidden" name="action" value="update1" />
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	{foreach item="ganti" from=$T_MK1}
	<input type="hidden" name="id_mk" value="{$ganti.ID_KURIKULUM_MK}" />
	<input type="hidden" name="id_kur_prodi" value="{$ganti.ID_KURIKULUM}" />
	<input type="hidden" name="thkur" value="{$ganti.ID_KURIKULUM}" />
	<tr>
	 <td width="23%">Kode Mata Ajar</td>
	 <td width="2%"><center>:</center></td>
	 <td width="75%">{$ganti.KD_MATA_KULIAH}</td>
	</tr>
	<tr>
	 <td>Nama Mata Ajar</td>
	 <td><center>:</center></td>
	 <td>{$ganti.NM_MATA_KULIAH}</td>
	</tr>
	<tr>
	 <td>Nama Mata Ajar (English)</td>
	 <td><center>:</center></td>
	 <td><input name="en" id="en" type="text" value="{$ganti.NM_MATA_KULIAH_EN}" style="width:500px" /></td>
	</tr>

	{if id_unit_kerja_sd == 30 or $id_pt_user != 1}
		<tr>
		 <td>SKS Tatap Muka</td>
		 <td><center>:</center></td>
		 <td>
		 <input name="kredit_tm" id="kredit_tm" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_TATAP_MUKA}"/>
		 &nbsp;<font color="grey">Tanda koma diganti dengan titik. (1,5 --> 1.5)</font>
		 </td>
		</tr>
		<tr>
		 <td>SKS Praktikum</td>
		 <td><center>:</center></td>
		 <td>
		 <input name="kredit_prk" id="kredit_prk" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_PRAKTIKUM}"/>
		 &nbsp;<font color="grey">Tanda koma diganti dengan titik. (1,5 --> 1.5)</font></td>
		</tr>
		<tr>
		 <td>SKS Praktek Lapangan</td>
		 <td><center>:</center></td>
		 <td>
		 <input name="kredit_prak_lapangan" id="kredit_prak_lapangan" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_PRAK_LAPANGAN}"/>
		 &nbsp;<font color="grey">Tanda koma diganti dengan titik. (1,5 --> 1.5)</font></td>
		</tr>
		<tr>
		 <td>SKS Tutorial</td>
		 <td><center>:</center></td>
		 <td>
		 <input name="kredit_tutor" id="kredit_tutor" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_TUTOR}"/>
		 &nbsp;<font color="grey">Tanda koma diganti dengan titik. (1,5 --> 1.5)</font></td>
		</tr>
		<tr>
		 <td>SKS Simulasi</td>
		 <td><center>:</center></td>
		 <td>
		 <input name="kredit_simulasi" id="kredit_simulasi" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_SIMULASI}"/>
		 &nbsp;<font color="grey">Tanda koma diganti dengan titik. (1,5 --> 1.5)</font></td>
		</tr>
		<tr>
		 <td><strong>SKS Total</strong></td>
		 <td><center>:</center></td>
		 <td>
		 <input name="kredit_matkul" id="kredit_matkul" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_SEMESTER}"/>
		 &nbsp;<font color="grey">SKS Total : Tatap Muka + Praktikum + Praktikum Lapangan + Tutorial + Simulasi</font>
		 </td>
		</tr>
	{else}
		<tr>
		 <td>SKS Tatap Muka</td>
		 <td><center>:</center></td>
		 <td>
		 <input name="kredit_tm" id="kredit_tm" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_TATAP_MUKA}" readonly />
		 &nbsp;<font color="grey">Tanda koma diganti dengan titik. (1,5 --> 1.5)</font>
		 </td>
		</tr>
		<tr>
		 <td>SKS Praktikum</td>
		 <td><center>:</center></td>
		 <td>
		 <input name="kredit_prk" id="kredit_prk" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_PRAKTIKUM}" readonly />
		 &nbsp;<font color="grey">Tanda koma diganti dengan titik. (1,5 --> 1.5)</font></td>
		</tr>
		<tr>
		 <td>SKS Praktek Lapangan</td>
		 <td><center>:</center></td>
		 <td>
		 <input name="kredit_prak_lapangan" id="kredit_prak_lapangan" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_PRAK_LAPANGAN}" readonly />
		 &nbsp;<font color="grey">Tanda koma diganti dengan titik. (1,5 --> 1.5)</font></td>
		</tr>
		<tr>
		 <td>SKS Tutorial</td>
		 <td><center>:</center></td>
		 <td>
		 <input name="kredit_tutor" id="kredit_tutor" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_TUTOR}" readonly />
		 &nbsp;<font color="grey">Tanda koma diganti dengan titik. (1,5 --> 1.5)</font></td>
		</tr>
		<tr>
		 <td>SKS Simulasi</td>
		 <td><center>:</center></td>
		 <td>
		 <input name="kredit_simulasi" id="kredit_simulasi" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_SIMULASI}" readonly />
		 &nbsp;<font color="grey">Tanda koma diganti dengan titik. (1,5 --> 1.5)</font></td>
		</tr>
		<tr>
		 <td><strong>SKS Total</strong></td>
		 <td><center>:</center></td>
		 <td>
		 <input name="kredit_matkul" id="kredit_matkul" type="text" size="3" maxlength="3" value="{$ganti.KREDIT_SEMESTER}" readonly />
		 &nbsp;<font color="grey">SKS Total : Tatap Muka + Praktikum + Praktikum Lapangan + Tutorial + Simulasi</font>
		 </td>
		</tr>
	{/if}

	<tr>
	 <td>Tingkat Semester</td>
	 <td><center>:</center></td>
	 <td>
		<select name="tingkat_smt" id="tingkat_smt">
			<!-- horror kalau tanpa if selected // FIKRIE -->
			<!-- <option value="{$ganti.TINGKAT_SEMESTER}">{$ganti.TINGKAT_SEMESTER}</option> -->
			<option value="">--</option>
			<option value="1" {if $ganti.TINGKAT_SEMESTER == 1}selected{/if}>SEMESTER 1</option>
			<option value="2" {if $ganti.TINGKAT_SEMESTER == 2}selected{/if}>SEMESTER 2</option>
			<option value="3" {if $ganti.TINGKAT_SEMESTER == 3}selected{/if}>SEMESTER 3</option>
			<option value="4" {if $ganti.TINGKAT_SEMESTER == 4}selected{/if}>SEMESTER 4</option>
			<option value="5" {if $ganti.TINGKAT_SEMESTER == 5}selected{/if}>SEMESTER 5</option>
			<option value="6" {if $ganti.TINGKAT_SEMESTER == 6}selected{/if}>SEMESTER 6</option>
			<option value="7" {if $ganti.TINGKAT_SEMESTER == 7}selected{/if}>SEMESTER 7</option>
			<option value="8" {if $ganti.TINGKAT_SEMESTER == 8}selected{/if}>SEMESTER 8</option>
		 </select>
	 </td>
	</tr>
	<tr>
		<td>Kelompok Mata Ajar</td>
		<td><center>:</center></td>
		<td>
		 <select name="kelompok_mk" id="kelompok_mk">
			<option value="">--</option>
			{foreach item="list" from=$KEL_MK}
			{html_options values=$list.ID_KELOMPOK_MK output=$list.KET_KELOMPOK_MK selected=$ganti.ID_KELOMPOK_MK}
			{/foreach}
		 </select>
		</td>
	</tr>
	<tr>
		<td>Tahun Kurikulum</td>
		<td><center>:</center></td>
		<td>
		 <select name="thkur1" disabled>
			<!-- <option value='{$ganti.ID_KURIKULUM_PRODI}'>{$ganti.NAMA}</option> -->
			{foreach item="list" from=$T_KUR1}
			{html_options values=$list.ID_KURIKULUM output=$list.NAMA selected=$ganti.ID_KURIKULUM}
			{/foreach}
		 </select>
		</td>
	</tr>
	<tr>
	 <td>MA (KKN, Skripsi, TA)</td>
	 <td><center>:</center></td>
	 <td>
		<select name="status_mkta">
			<option value="{$ganti.STATUS_MKTA}">{$ganti.MKTA}</option>
			<option value="0">-----</option>
			<option value="1">Skripsi/TA/Thesis</option>
			<option value="2">KKN</option>
			<option value="3">Magang/PKL</option>
			<option value="4">Keasistenan</option>
			<option value="5">Modul/Skill/Micro</option>
			<option value="6">Praktikum</option>
			<option value="10">MKPD</option>
		</select>
	 </td>
	</tr>
	<tr>
	 <td>MA Agama</td>
	 <td><center>:</center></td>
	 <td>
		<select name="status_mk_agama">
		{if $ganti.STATUS_MK_AGAMA == null}
			<option value="">-----</option>
			{foreach item="list" from=$T_AGM}
			{html_options values=$list.ID_AGAMA output=$list.NM_AGAMA}
			{/foreach}
		{else}
			<option value="">-----</option>
			{foreach item="list" from=$T_AGM}
			{html_options values=$list.ID_AGAMA output=$list.NM_AGAMA selected=$ganti.STATUS_MK_AGAMA}
			{/foreach}
		{/if}
		</select>
	 </td>
	</tr>
    {* {if $kdfak == 1} *}
    <!-- <tr>
	 <td>Status Paket</td>
	 <td><center>:</center></td>
	 <td>
		<select name="status_paket" id="status_paket">
			<option value="{$ganti.STATUS_PAKET}">{$ganti.NM_STATUS_PAKET}</option>
			<option value="">Reguler</option>
			<option value="sp1">Suppro 1</option>
			<option value="sp2">Suppro 2</option>
			<option value="sp3">Suppro 3</option>
			<option value="dm">Dokter Muda</option>
		 </select>
	 </td>
	</tr> -->
    {* {/if} *}
    <tr>
	 <td>Status Seri</td>
	 <td><center>:</center></td>
	 <td>
		<select name="status_seri" id="status_seri">
			<option value="0" {if $ganti.STATUS_SERI == 0}selected{/if}>Tidak</option>
			<option value="1" {if $ganti.STATUS_SERI == 1}selected{/if}>Ya</option>
		 </select>
	 </td>
	</tr>
	{/foreach}
</table>
<p><input type="submit" name="simpan" value="Simpan"></p>
</form>
</div>

{literal}
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable1").tablesorter(
		{
		sortList: [[1,0]],
		headers: {
      3: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="panel" id="panel3" style="display: {$disp3}">
<p> </p>
<form action="daftarmk.php" method="post">
<input type="hidden" name="action" value="insertmk" />
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>Tahun Kurikulum</td>
		<td><center>:</center></td>
		<td colspan="2">
			<select name="thkur1">
		  {foreach item="kur" from=$T_KUR}
		  {html_options values=$kur.ID_KURIKULUM output=$kur.NAMA selected=$ID_KP}
		  {/foreach}
		  </select>
		</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar</td>
		<td><center>:</center></td>
		<td><input name="nama_mk" id="nama_mk" type="text" size="40" maxlength="40" value="{if isset($smarty.post.nama_mk)}{$smarty.post.nama_mk}{/if}" /></td>
		<td><input type="submit" name="View" value="Cari Mata Ajaran"></td>
	</tr>
</table>
</form>
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
    <tr>
	 <th width="15%">Kode</th>
	 <th width="35%">Nama Mata Ajar</th>
	 <th width="35%">Nama Mata Ajar (English)</th>
	 <th class="noheader" width="15%">Aksi</th>
	</tr>
	</thead>
	<tbody>
	{foreach name="test" item="list" from=$INSERT_MK}
	<tr>
	 <td>{$list.KD_MATA_KULIAH}</td>
	 <td>{$list.NM_MATA_KULIAH}</td>
	 <td>{$list.NM_MATA_KULIAH_EN}</td>
	 <td><center><a href="daftarmk.php?action=add&id_mk={$list.ID_MATA_KULIAH}&tkur={$ID_KP}" onclick="javascript: displayPanel('3');">Pilih</a></center></td>
	</tr>
	{foreachelse}
	<tr><td colspan="4"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
</div>

 <script>$('form').validate();</script>