<div class="center_title_bar">Usulan Mata Ajar - Update Kelas</div>


<form action="usulan-mk-new.php" method="post">
	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="id_kelas_mk" value="{$smarty.get.kls}" />
	<input type="hidden" name="id_semester" value="{$kelas.ID_SEMESTER}" />
	{foreach $jadwal_set as $jadwal}
	<input type="hidden" name="id_jadwal_kelas_old[]" value="{$jadwal.ID_JADWAL_KELAS}" />
	<input type="hidden" name="id_jadwal_hari_old[]" value="{$jadwal.ID_JADWAL_HARI}" />
	<input type="hidden" name="id_jadwal_jam_old[]" value="{$jadwal.ID_JADWAL_JAM}" />
	<input type="hidden" name="id_ruangan_old[]" value="{$jadwal.ID_RUANGAN}" />
	{/foreach}
	{foreach $pengampu_set as $pengampu}
	<input type="hidden" name="id_pengampu_mk_old[]" value="{$pengampu.ID_PENGAMPU_MK}" />
	<input type="hidden" name="id_dosen_old[]" value="{$pengampu.ID_DOSEN}" />
	{/foreach}
	<table>
		<thead>
			<tr>
				<th colspan="2">Update Kelas</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Mata Kuliah</td>
				<td>
					<strong>{$mata_kuliah.KD_MATA_KULIAH} {$mata_kuliah.NM_MATA_KULIAH} ({$mata_kuliah.KREDIT_SEMESTER} SKS) -- {$mata_kuliah.NM_KURIKULUM}</strong>
				</td>
			</tr>
			<tr>
				<td>Semester</td>
				<td>
					<strong>{$semester.NM_SEMESTER} {$semester.TAHUN_AJARAN}</strong>
				</td>
			</tr>
			<tr>
				<td>Kelas</td>
				<td>
					<select name="id_nama_kelas">
						<option value=""></option>
						{foreach $nama_kelas_set as $nama_kelas}
							{html_options values=$nama_kelas.ID_NAMA_KELAS output=$nama_kelas.NAMA_KELAS selected=$kelas.NO_KELAS_MK}
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Kapasitas</td>
				<td>
					<input type="text" name="kapasitas_kelas_mk" size="4" maxlength="2" value="{$kelas.KAPASITAS_KELAS_MK}"/>
				</td>
			</tr>
			<tr>
				<td>Rencana Pertemuan</td>
				<td>
					<input type="text" name="jumlah_pertemuan_kelas_mk" size="4" maxlength="2" value="{$kelas.JUMLAH_PERTEMUAN_KELAS_MK}"/>
				</td>
			</tr>
			{* Jika TA/Skripsi/Thesis/Disertasi/KKN/PKL tanpa jadwal *}
			{if $mata_kuliah.STATUS_MKTA == 0 or $mata_kuliah.STATUS_MKTA == 5 or $mata_kuliah.STATUS_MKTA == 6}
			<tr>
				<td>Jadwal 1</td>
				<td>
					Hari: 
					<select name="id_jadwal_hari[0]">
						<option value=""></option>
						{foreach $hari_set as $hari}{html_options values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI selected=$jadwal_set[0].ID_JADWAL_HARI}{/foreach}
					</select> 
					Jam : 
					<select name="id_jadwal_jam[0]">
						<option value=""></option>
						{foreach $jam_set as $jam}{html_options values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$jadwal_set[0].ID_JADWAL_JAM}{/foreach}
					</select>
					Ruang : 
					<select name="id_ruangan[0]">
						<option value=""></option>
						{foreach $ruangan_set as $ruangan}{html_options values=$ruangan.ID_RUANGAN output=$ruangan.NM_RUANGAN selected=$jadwal_set[0].ID_RUANGAN}{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Jadwal 2</td>
				<td>
					Hari: 
					<select name="id_jadwal_hari[1]">
						<option value=""></option>
						{foreach $hari_set as $hari}{html_options values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI selected=$jadwal_set[1].ID_JADWAL_HARI}{/foreach}
					</select> 
					Jam : 
					<select name="id_jadwal_jam[1]">
						<option value=""></option>
						{foreach $jam_set as $jam}{html_options values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$jadwal_set[1].ID_JADWAL_JAM}{/foreach}
					</select>
					Ruang : 
					<select name="id_ruangan[1]">
						<option value=""></option>
						{foreach $ruangan_set as $ruangan}{html_options values=$ruangan.ID_RUANGAN output=$ruangan.NM_RUANGAN selected=$jadwal_set[1].ID_RUANGAN}{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Jadwal 3</td>
				<td>
					Hari: 
					<select name="id_jadwal_hari[2]">
						<option value=""></option>
						{foreach $hari_set as $hari}{html_options values=$hari.ID_JADWAL_HARI output=$hari.NM_JADWAL_HARI selected=$jadwal_set[2].ID_JADWAL_HARI}{/foreach}
					</select> 
					Jam : 
					<select name="id_jadwal_jam[2]">
						<option value=""></option>
						{foreach $jam_set as $jam}{html_options values=$jam.ID_JADWAL_JAM output=$jam.NM_JADWAL_JAM selected=$jadwal_set[2].ID_JADWAL_JAM}{/foreach}
					</select>
					Ruang : 
					<select name="id_ruangan[2]">
						<option value=""></option>
						{foreach $ruangan_set as $ruangan}{html_options values=$ruangan.ID_RUANGAN output=$ruangan.NM_RUANGAN selected=$jadwal_set[2].ID_RUANGAN}{/foreach}
					</select>
				</td>
			</tr>
			{/if}
			<tr>
				<td>Penanggung Jawab Mata Ajar (PJMA)</td>
				<td>
					<select name="id_dosen[0]">
						<option value=""></option>
						{foreach $dosen_set as $dosen}
							{html_options values=$dosen.ID_DOSEN output=$dosen.NM_PENGGUNA selected=$pengampu_set[0].ID_DOSEN}
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Tim PJMA 1</td>
				<td>
					<select name="id_dosen[1]">
						<option value=""></option>
						{foreach $dosen_set as $dosen}
							{html_options values=$dosen.ID_DOSEN output=$dosen.NM_PENGGUNA selected=$pengampu_set[1].ID_DOSEN}
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Tim PJMA 2</td>
				<td>
					<select name="id_dosen[2]">
						<option value=""></option>
						{foreach $dosen_set as $dosen}
							{html_options values=$dosen.ID_DOSEN output=$dosen.NM_PENGGUNA selected=$pengampu_set[2].ID_DOSEN}
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Bahasan</td>
				<td>
					<textarea name="bahasan" cols="50" rows="4" maxlength="200">{$kelas.BAHASAN}</textarea>
				</td>
			</tr>
			<tr>
				<td>Tanggal Efektif</td>
				<td>
					Mulai : <input type="text" name="tgl_mulai" value="{$kelas.TGL_MULAI_DMY}"/>
					Akhir : <input type="text" name="tgl_akhir" value="{$kelas.TGL_AKHIR_DMY}"/>
				</td>
			</tr>
			<tr>
				<td>Status Seri</td>
				<td>
					<select name="status_seri">
						{if $kelas.STATUS_SERI == 0}
							<option value="0" selected >Reguler</option>
							<option value="1">Seri</option>
						{else}
							<option value="0">Reguler</option>
							<option value="1" selected >Seri</option>
						{/if}
					</select>
				</td>
			</tr>
			<tr>
				<td>Tingkat Semester Seri</td>
				<td>
					<input type="number" name="tingkat_semester_seri" value="{$kelas.TINGKAT_SEMESTER_SERI}"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" value="Simpan" /> |
					<a href="usulan-mk-new.php?action=add-kelas&smt={$smarty.get.smt}">Kembali ke pilih MA</a> |
					<a href="usulan-mk-new.php?action=tampil&smt={$smarty.get.smt}">Kembali ke daftar usulan MA</a>
				</td>
			</tr>
		</tbody>
	</table>
</form>

<script>
	$('select[name^=id_dosen]').chosen({ allow_single_deselect: true });
	$('input[name^=tgl_]').datepicker({ dateFormat: 'dd-mm-yy' });
	$('form').validate({
		rules: {
			id_nama_kelas: "required",
			kapasitas_kelas_mk: "required",
			jumlah_pertemuan_kelas_mk: "required"
		}
	});
</script>

