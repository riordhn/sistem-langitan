<div class="center_title_bar">Usulan Mata Ajar</div>

<table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 5px">
	<tr>
		<td>
			<form action="usulan-mk-new.php" method="get">
			<input type="hidden" name="action" value="tampil" />
				Semester :
					<select name="smt">
					{foreach $semester_set as $semester}
						<option value="{$semester.ID_SEMESTER}" {if $semester.ID_SEMESTER == $id_semester_terpilih}selected{/if}
						>{$semester.TAHUN_AJARAN} {$semester.NM_SEMESTER}{if $semester.STATUS_AKTIF_SEMESTER == 'True'} (AKTIF REG) {/if} {if $semester.STATUS_AKTIF_SP == 'True'} (AKTIF SP){/if}</option>
					{/foreach}
				</select>
				<input type="submit" name="View" value="View">
			</form>
		</td>
		<td>
			<form action="usulan-mk-new.php" method="get">
				<input type="hidden" name="action" value="add-kelas" />
				<input type="hidden" name="smt" value="{$id_semester_terpilih}" />
				<input type="submit" value="Tambah Usulan Mata Ajar" />
			</form>
		</td>
	</tr>
</table>

<table id="table" class="tablesorter">
	<thead>
		<tr>
            <th>#</th>
			<th>Kode</th>
			<th>Mata Kuliah</th>
			<th title="Tingkat Semester">Smt</th>
			<th>Kelas</th>
			<th title="Kapasitas Kelas">Kap.</th>
			<th title="Terisi">KRS</th>
			<th>Jadwal &amp; Ruang</th>
			<th title="Penanggung Jawab Mata Kuliah">PJMA</th>
			<th title="Jumlah Rencana Pertemuan">Renc. Pert.</th>
			<th title="Status Seri">Status Seri</th>
			<th class="noheader">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach $kelas_set as $kelas}
			<tr>
				<td class="center">{$kelas@index + 1}</td>
				<td>{$kelas.KD_MATA_KULIAH}</td>
				<td>{$kelas.NM_MATA_KULIAH} ({$kelas.SKS} SKS)</td>
				<td class="center">{$kelas.TINGKAT_SEMESTER}</td>
				<td class="center">{$kelas.NAMA_KELAS}</td>
				<td class="center">{$kelas.KAPASITAS}</td>
				<td class="center">{$kelas.KRS_APPROVE}/{$kelas.KRS}</td>
				<td {if $kelas.JML_JADWAL == 0}style="background: lightpink"{else if $kelas.JML_RUANGAN == 0}style="background: yellow"{/if}>
					{if $kelas.STATUS_MKTA == 0}{* Regular *}
							{foreach $jadwal_set as $jadwal}
								{if $kelas.ID_KELAS_MK == $jadwal.ID_KELAS_MK}{$jadwal.JADWAL} ({$jadwal.NM_RUANGAN})<br/>{/if}
							{/foreach}
					{else if $kelas.STATUS_MKTA == 1}{* SKRIPSI *}
						Skripsi/TA/Thesis
					{else if $kelas.STATUS_MKTA == 2}{* KKN *}
						KKN

					{else if $kelas.STATUS_MKTA == 3}{* Magang/PKL *}
						Magang/PKL
					{else if $kelas.STATUS_MKTA == 4}{* Keasistenan *}
						Keasistenan

					{else if $kelas.STATUS_MKTA == 5}{* Modul/Skill/Micro *}
						Modul/Skill/Micro
					{else if $kelas.STATUS_MKTA == 6}{* Praktikum *}
						Praktikum

					{else if $kelas.STATUS_MKTA == 10}{* MKPD *}
						MKPD
					
					{/if}
				</td>
				<td {if $kelas.STATUS_MKTA != 3}{if $kelas.JML_DOSEN == 0}style="background-color: lightpink"{/if}{/if}>
					{foreach $dosen_set as $dosen}
						{if $kelas.ID_KELAS_MK == $dosen.ID_KELAS_MK}{$dosen.NM_DOSEN}<br/>{/if}
					{/foreach}
				</td>
				<td class="center">
					0
				</td>
				<td>
					{if $kelas.STATUS_SERI == 0}{* Regular *}
						Reguler
					{else}
						Seri (Smt {$kelas.TINGKAT_SEMESTER_SERI})
					{/if}
				</td>
				<td>
					<a href="usulan-mk-new.php?action=update&kls={$kelas.ID_KELAS_MK}">Update</a> |
					<a href="usulan-mk-new.php?action=add-kelas&kls-ref={$kelas.ID_KELAS_MK}">Copy Kelas</a> |
					{if $kelas.KRS == 0}
					<a href="usulan-mk-new.php?action=delete&kls={$kelas.ID_KELAS_MK}" class="disable-ajax" data-action="delete" data-kode="{$kelas.KD_MATA_KULIAH}" style="color: red">Hapus Kelas<a/>
					{/if}
				</td>
			</tr>
		{foreachelse}
			<tr>
				<td colspan="11">Tidak ada data</td>
			</tr>
		{/foreach}
	</tbody>
</table>
	
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').tablesorter();
		$('a[data-action=delete]').bind('click', function(e) {
			e.preventDefault();
			var result = (prompt('Masukkan kode mata kuliah untuk menghapus kelas ini :') == $(this).data('kode'));
			if (result) {
				window.location = window.location.hash.substr(0, window.location.hash.indexOf('!') + 1) + $(this).attr('href');
			}
		});
	});
		
</script>