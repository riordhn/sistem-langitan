{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
  $("#myTable").tablesorter(
		{
		sortList: [[0,0], [2,0]],
		headers: {
      7: { sorter: false }
		}
		}
	);
}
);
</script>
{/literal}
<div class="center_title_bar">Monitoring MKPD</div>
<div id="tabs">
	<div id="tab1" class="tab_sel" align="center" onclick="javascript: displayPanel('1');">Rincian</div>
	<div id="tab2" class="tab" style="margin-left:1px;" align="center" onclick="javascript: displayPanel('2');">Update</div>
</div>

<div class="panel" id="panel1" style="display: {$disp1} ">
<p> </p>
<form action="mkpd.php" method="post">
<input type="hidden" name="action" value="tampil" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
  <tr>
	 <td>
		Tahun Akademik :
		<select name="smt">
		<option value='0'>-- ALL --</option>
		{foreach item="smt" from=$T_ST}
		{html_options values=$smt.ID_SEMESTER output=$smt.SMT selected=$id_smt}
		{/foreach}
		</select>
		<input type="submit" name="View" value="View">
	</td>
  </tr>
</table>
</form>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="10%">NIM</th>
			<th width="15%">Nama Mhs</th>
			<th width="7%">Kode</th>
			<th width="30%">JUDUL MKPD</th>
			<th width="10%">Smt</th>
			<th class="noheader" width="12%">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=test item="list" from=$T_MKPD}
		<tr>
			<td>{$list.NIM_MHS}</td>
			<td>{$list.NM_PENGGUNA}</td>
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.JUDUL_MKPD}</td>
			<td>{$list.THN_AKADEMIK_SEMESTER}-{$list.NM_SEMESTER}</td>
			<td><center><a href="mkpd.php?action=update&id_mhs={$list.ID_MHS}&smt={$list.ID_SEMESTER}&kode={$list.KD_MATA_KULIAH}">Update</a></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
</table>
</div>

<div class="panel" id="panel2" style="display: {$disp2}">
<p> </p>
<form action="tugas-akhir.php?action=simpanjudul&smt={$SMT}&id_mhs={$smarty.get.id_mhs}" method="post" id="cari1">
<table class="tb_frame" width="100%" border="1" cellspacing="0" cellpadding="0" id="tableText">
{foreach name=test item="list1" from=$T_UPD_MKPD}		
                  <tr>
                    <td width="149">NIM</td>
                    <td width="9">:</td>
                    <td width="154">{$list1.NIM_MHS}</td>
                  </tr>
				  <tr>
                    <td width="149">Nama Mhs</td>
                    <td width="9">:</td>
                    <td>{$list1.NM_PENGGUNA}</td>
                  </tr>
				  <tr>
                    <td width="149">Kode Mata_kuliah</td>
                    <td width="9">:</td>
                    <td>{$list1.KD_MATA_KULIAH}</td>
                  </tr>	

				  <tr>
                    <td width="149">Judul MKPD</td>
                    <td width="9">:</td>
                    <td><textarea name="judul_mkpd" cols="60" rows="6">{$list1.JUDUL_MKPD}</textarea></td>
                  </tr>
				  <tr>
                    <td width="149"></td>
                    <td width="9"></td>
                    <td><input type="submit" value="Simpan Judul"></td>
                  </tr>
{/foreach}
</table>
</form>
</div>
