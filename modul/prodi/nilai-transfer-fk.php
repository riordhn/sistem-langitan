<?php
require('common.php');
require_once ('ociFunction.php');
$kd_fak= $user->ID_FAKULTAS;
$id_pengguna= $user->ID_PENGGUNA; 
$kd_prodi=$user->ID_PROGRAM_STUDI;

/*
echo '<script>alert("Maaf, Fitur Ini di non aktifkan, Hubungi Data Controler..")</script>';
*/

if ($kd_fak==1)
{
$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$smtaktif=getvar("select id_semester,thn_akademik_semester from semester where status_aktif_semester='True'");

$smtpil=getData("select id_semester,thn_akademik_semester||'-'||group_semester||'-'||nm_semester as smt from semester
		where id_semester !='".$smtaktif['ID_SEMESTER']."'
		order by thn_akademik_semester desc");
				
$smarty->assign(smtpil,$smtpil);

if(($_POST['action'])=='proses'){

	$id_mhs=$_POST['id_mhs'];	
	$ip = $_SERVER['REMOTE_ADDR'];
	

	$id_kurikulum=$_POST['kd_konversi'];
	$nilai=strtoupper($_POST['nilai']);
	$smt_diakui=strtoupper($_POST['smt']);
	
	if($nilai!= 'A' and $nilai  != 'AB' and $nilai  != 'B' and $nilai  != 'BC' and $nilai  != 'C' and $nilai  != 'D' and $nilai  != 'E'){
	echo '<script>alert("Masukkan Nilai Huruf [A/AB/B/BC/C/D/E]")</script>';
	}else{
	
	InsertData("insert into pengambilan_mk (id_mhs, id_semester,id_kurikulum_mk,nilai_huruf,status_apv_pengambilan_mk,status_pengambilan_mk,flagnilai,keterangan)
		values ($id_mhs,$smt_diakui,$id_kurikulum,'$nilai',1,9,'1','INSERT NILAI')");

	
	InsertData("insert into log_transfer_nilai(id_pengambilan_mk,id_kurikulum_mk,id_mhs,nilai,id_semester,id_pengguna,ip_address,tanggal_insert)
	select id_pengambilan_mk,id_kurikulum_mk,id_mhs,nilai_huruf,id_semester,'$id_pengguna','$ip',sysdate 
	from pengambilan_mk where id_mhs=$id_mhs and id_semester=$smt_diakui and id_kurikulum_mk=$id_kurikulum and nilai_huruf='$nilai' and keterangan='INSERT NILAI'");
	
	}
	
}

$nim =$_REQUEST['nim'];
$nama=getvar("select nm_pengguna from pengguna where username='$nim'");

$smarty->assign('nama', $nama['NM_PENGGUNA']);
$smarty->assign('nim', $nim);

$detail_nilai=getData("select kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,nilai_huruf,
					pengambilan_mk.id_pengambilan_mk,mahasiswa.id_program_studi,pengambilan_mk.id_kurikulum_mk,
					thn_akademik_semester||'/'||group_semester||'/'||nm_semester as smt,1 as status
					from pengambilan_mk
					left join kurikulum_mk on pengambilan_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
					left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
					left join semester on pengambilan_mk.id_semester=semester.id_semester
					left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
					where nim_mhs='$nim' 
					and mahasiswa.id_program_studi=$kd_prodi 
					order by thn_akademik_semester,group_semester,nm_semester,kd_mata_kuliah");

	
$smarty->assign('DETAIL_TRANSFER', $detail_nilai);

$data_mhs=getvar("select nim_mhs,nm_pengguna,nm_jenjang||'-'||nm_program_studi as prodi,
						m.id_program_studi,m.id_mhs
						from mahasiswa m 
						join pengguna p on m.id_pengguna=p.id_pengguna
						join program_studi ps on m.id_program_studi=ps.id_program_studi
						join jenjang j on ps.id_jenjang=j.id_jenjang
						where m.nim_mhs='$nim'");
						
$smarty->assign('data_mhs', $data_mhs);

$kd_konversi=getData("select kd_mata_kuliah, trim(substr(nm_mata_kuliah,1,60)) as nm_mata_kuliah, id_kurikulum_mk,kurikulum_mk.kredit_semester,tahun_kurikulum as tahun
						 from kurikulum_mk
						join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi 
						join mata_kuliah on mata_kuliah.id_mata_kuliah = kurikulum_mk.id_mata_kuliah
						where kurikulum_mk.id_program_studi='$data_mhs[ID_PROGRAM_STUDI]'
						order by tahun_kurikulum,kd_mata_kuliah asc");			
$smarty->assign('kd_konversi', $kd_konversi);
$smarty->display('nilai-transfer-fk.tpl');
} else
{
echo '<script>alert("Maaf, Ini Hanya untuk Fakultas Kedokteran")</script>';
}



?>
