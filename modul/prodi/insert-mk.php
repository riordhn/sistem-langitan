<?php
/*
YAH 06/06/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');

$id_perguruan_tinggi = $user->ID_PERGURUAN_TINGGI; 
$id_pengguna= $user->ID_PENGGUNA;
$id_program_studi = $user->ID_PROGRAM_STUDI;
//print_r($user);


$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');


if ($_GET['action']=='del')
{
	$id_mata_kuliah=$_GET['id_mata_kuliah'];
	$db->Query("delete from mata_kuliah where id_mata_kuliah=$id_mata_kuliah");
}

if ($_GET['action']=='update')
{
	$smarty->assign('disp1','none');
	$smarty->assign('disp2','none');
	$smarty->assign('disp3','block');

	$id_mata_kuliah=$_GET['id_mata_kuliah'];
	$mata_kuliah_update=$db->QueryToArray("select mk.*,jenjang.id_jenjang as id_jenjang from mata_kuliah mk join program_studi ps on mk.id_program_studi = ps.id_program_studi 
		join jenjang  on ps.id_jenjang = jenjang.id_jenjang
		where mk.id_mata_kuliah=$id_mata_kuliah");
	
	
	$smarty->assign('T_MATA_KULIAH', $mata_kuliah_update);  

}

if ($_POST['action']=='proses')
{
	
	$smarty->assign('disp1','block');
	$smarty->assign('disp2','none');
	$smarty->assign('disp3','none');

	$id_mata_kuliah = $_POST['id_mata_kuliah'];

	$id_sub_rumpun_ilmu=$_POST['sub_rumpun_ilmu'] ? $_POST['sub_rumpun_ilmu']:0 ;
	$kd_mata_kuliah=strtoupper($_POST['kd_mata_kuliah']);
	$nm_mata_kuliah=str_replace("'", "''", strtoupper($_POST['nm_mata_ajar']));
	$id_program_studi = $_POST['program_studi'];
	$id_jenis_mk=$_POST['jenis_mk'];
	$id_kelompok_mk=$_POST['kelompok_mk'];
	$sks_total = isset($_POST['sks_total']) ? $_POST['sks_total'] : 0;
	$sks_tatap_muka = isset($_POST['sks_tatap_muka']) ? $_POST['sks_tatap_muka'] : 0;
	$sks_praktikum = isset($_POST['sks_praktikum']) ? $_POST['sks_praktikum'] : 0;
	$sks_praktek_lapangan = isset($_POST['sks_praktek_lapangan']) ? $_POST['sks_praktek_lapangan'] : 0;
	$sks_simulasi = isset($_POST['sks_simulasi']) ? $_POST['sks_simulasi'] : 0;
	$metode_kuliah=$_POST['metode_kuliah'];
	$ada_sap=$_POST['ada_sap'];
	$ada_silabus=$_POST['ada_silabus'];
	$ada_bahan_ajar=$_POST['ada_bahan_ajar'];
	$status_praktikum=$_POST['status_praktikum'];
	$ada_diktat=$_POST['ada_diktat'];
	$tgl_mulai_efektif=$_POST['tgl_mulai_efektif'];
	$tgl_akhir_efektif=$_POST['tgl_akhir_efektif'];

	$db->Query("UPDATE mata_kuliah set id_sub_rumpun_ilmu=$id_sub_rumpun_ilmu,
				kd_mata_kuliah='$kd_mata_kuliah',nm_mata_kuliah='$nm_mata_kuliah',
				updated_by='$id_pengguna',updated_on=sysdate,id_program_studi='$id_program_studi',
				id_jenis_mk='$id_jenis_mk', id_kelompok_mk = '$id_kelompok_mk', kredit_semester = '$sks_total', kredit_tatap_muka = '$sks_tatap_muka', kredit_praktikum = '$sks_praktikum', kredit_prak_lapangan = '$sks_praktek_lapangan', kredit_simulasi = '$sks_simulasi', metode_pelaksanaan = '$metode_kuliah', ada_sap = '$ada_sap', ada_silabus = '$ada_silabus', ada_bahan_ajar = '$ada_bahan_ajar', status_praktikum = '$status_praktikum', ada_diktat = '$ada_diktat', tgl_mulai_efektif = '$tgl_mulai_efektif', tgl_akhir_efektif = '$tgl_akhir_efektif' where id_mata_kuliah=$id_mata_kuliah"); 
	
}

if ($_POST['action']=='add')
{
	$id_sub_rumpun_ilmu		= $_POST['sub_rumpun_ilmu'] ? $_POST['sub_rumpun_ilmu']:0 ;
	$kd_mata_kuliah			= strtoupper($_POST['kd_mata_kuliah']);
	$nm_mata_kuliah			= str_replace("'", "''", strtoupper($_POST['nm_mata_ajar']));
	$id_program_studi		= $_POST['program_studi'];
	$id_jenis_mk			= $_POST['jenis_mk'];
	$id_kelompok_mk			= $_POST['kelompok_mk'];
	$sks_total				= (!empty($_POST['sks_total'])) ? $_POST['sks_total'] : 0;
	$sks_tatap_muka			= (!empty($_POST['sks_tatap_muka'])) ? $_POST['sks_tatap_muka'] : 0;
	$sks_praktikum			= (!empty($_POST['sks_praktikum'])) ? $_POST['sks_praktikum'] : 0;
	$sks_praktek_lapangan	= (!empty($_POST['sks_praktek_lapangan'])) ? $_POST['sks_praktek_lapangan'] : 0;
	$sks_simulasi 			= (!empty($_POST['sks_simulasi'])) ? $_POST['sks_simulasi'] : 0;
	$metode_kuliah 			= $_POST['metode_kuliah'];
	$ada_sap				= $_POST['ada_sap'];
	$ada_silabus			= $_POST['ada_silabus'];
	$ada_bahan_ajar			= $_POST['ada_bahan_ajar'];
	$status_praktikum		= $_POST['status_praktikum'];
	$ada_diktat				= $_POST['ada_diktat'];
	$tgl_mulai_efektif		= $_POST['tgl_mulai_efektif'];
	$tgl_akhir_efektif		= $_POST['tgl_akhir_efektif'];



	// tambahan utk mendeteksi kode mk dan nama mk yg sama, sekaligus menjadikan satu spasi apabila ada dobel spasi atau lebih
	// FIKRIE
	// 05-08-2016
	$sama = FALSE;

	$mata_kuliah_prodi=$db->QueryToArray("select kd_mata_kuliah, nm_mata_kuliah from mata_kuliah where id_program_studi = '{$id_program_studi}'");

	foreach ($mata_kuliah_prodi as $mk) {
        $kd_mk_lama = trim($mk['KD_MATA_KULIAH']);
        $nm_mk_lama = trim($mk['NM_MATA_KULIAH']);

        $kd_mk_input = preg_replace('/\s\s+/', ' ', trim($kd_mata_kuliah));
        $nm_mk_input = preg_replace('/\s\s+/', ' ', trim($nm_mata_kuliah));

        if($kd_mk_input == $kd_mk_lama && $nm_mk_input == $nm_mk_lama){
            $sama = TRUE;
        }
    }
	
	if($_POST['kd_mata_kuliah'] =='' || $_POST['nm_mata_ajar'] == ''  || $_POST['program_studi'] == ''){
		echo '<script>alert("- Kode Mata Kuliah\n - Nama Mata Kuliah\n  - Program Studi\n harus diisi semua")</script>'; 
		
	}
	else if($sama){
		echo '<script>alert("Kode dan Nama Mata Kuliah Tidak Boleh Sama")</script>'; 
	}
	else{
	//cek
		$db->Query("select count(*) as cek from mata_kuliah mk
					join program_studi ps on mk.id_program_studi = ps.id_program_studi
					join fakultas fak on ps.id_fakultas = fak.id_fakultas 
					where ps.id_program_studi = '{$id_program_studi}' AND 
					fak.id_perguruan_tinggi  = '{$id_perguruan_tinggi}' AND upper(trim(kd_mata_kuliah))='$kd_mata_kuliah'
					--and flag=1");
		
		$data=$db->FetchAssoc();

		if ($data['CEK'] > 0)
		{
			echo '<script>alert("Maaf Kode Sudah Ada..")</script>';
		}else {
			$result = $db->Query("INSERT into mata_kuliah (id_sub_rumpun_ilmu,kd_mata_kuliah,
					nm_mata_kuliah,keterangan,created_by,created_on,id_program_studi,id_jenis_mk,id_kelompok_mk,kredit_semester,kredit_tatap_muka,kredit_praktikum,kredit_prak_lapangan,kredit_simulasi,metode_pelaksanaan,ada_sap,ada_silabus,ada_bahan_ajar,status_praktikum,ada_diktat,tgl_mulai_efektif,tgl_akhir_efektif) 
					values ($id_sub_rumpun_ilmu,'$kd_mata_kuliah',
					'$nm_mata_kuliah','INSERT PRODI',$id_pengguna,sysdate,$id_program_studi,'$id_jenis_mk','$id_kelompok_mk',$sks_total,$sks_tatap_muka,$sks_praktikum,$sks_praktek_lapangan,$sks_simulasi,'$metode_kuliah',$ada_sap,$ada_silabus,$ada_bahan_ajar,$status_praktikum,$ada_diktat,'$tgl_mulai_efektif','$tgl_akhir_efektif')");
			if (! $result)
				echo '<script>alert("Terjadi kegagalan insert MK. '.$db->Error.'")</script>';
		} 
	}
	
}




$daftar_program_studi=$db->QueryToArray("SELECT * FROM program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang WHERE ps.id_program_studi = '{$user->ID_PROGRAM_STUDI}'");
if($id_perguruan_tinggi == 4){
		$rumpun = $db->QueryToArray("select ID_SUB_RUMPUN_ILMU,kd_rumpun_ilmu||kd_sub_rumpun_ilmu||' - '||
							nm_rumpun_ilmu||' '||nm_sub_rumpun_ilmu as detail 
							from SUB_RUMPUN_ILMU
							left join RUMPUN_ILMU on SUB_RUMPUN_ILMU.ID_RUMPUN_ILMU=RUMPUN_ILMU.ID_RUMPUN_ILMU
							where SUB_RUMPUN_ILMU.id_perguruan_tinggi = '{$id_perguruan_tinggi}' order by kd_rumpun_ilmu,kd_sub_rumpun_ilmu");

		$rincian = $db->QueryToArray("SELECT id_mata_kuliah,kd_mata_kuliah,nm_mata_kuliah,nm_jenjang_ijasah,
							sub_rumpun_ilmu.id_rumpun_ilmu,nm_rumpun_ilmu,
							kd_sub_rumpun_ilmu,nm_sub_rumpun_ilmu,SUB_RUMPUN_ILMU.ID_SUB_RUMPUN_ILMU,nm_strata_mk,nomor,
							(SELECT COUNT(*) FROM MATA_KULIAH mk WHERE mk.KD_MATA_KULIAH = MATA_KULIAH.KD_MATA_KULIAH AND mk.ID_PROGRAM_STUDI = '{$user->ID_PROGRAM_STUDI}') AS RELASI_MATKUL,
							(SELECT COUNT(*) FROM KURIKULUM_MK WHERE KURIKULUM_MK.ID_MATA_KULIAH = mata_kuliah.ID_MATA_KULIAH) AS RELASI,
							(SELECT COUNT(*) FROM FEEDER_MATA_KULIAH WHERE FEEDER_MATA_KULIAH.ID_MATA_KULIAH = mata_kuliah.ID_MATA_KULIAH) AS RELASI_FEEDER, jmk.nm_jenis_mk, kmk.nm_kelompok_mk
							from mata_kuliah
							join program_studi ps on ps.id_program_studi = mata_kuliah.id_program_studi
							join jenjang on jenjang.id_jenjang = ps.id_jenjang  
							left join jenis_mk jmk on jmk.id_jenis_mk = mk.id_jenis_mk
							left join kelompok_mk kmk on kmk.id_kelompok_mk = mk.id_kelompok_mk
							left join SUB_RUMPUN_ILMU on MATA_KULIAH.ID_SUB_RUMPUN_ILMU=SUB_RUMPUN_ILMU.ID_SUB_RUMPUN_ILMU
							left join rumpun_ilmu on sub_rumpun_ilmu.id_rumpun_ilmu=rumpun_ilmu.id_rumpun_ilmu
							left join kd_strata_mk on MATA_KULIAH.strata=kd_strata_mk
							where mata_kuliah.id_program_studi = '{$id_program_studi}' ORDER BY nm_mata_kuliah");
}else{

	$rumpun = $db->QueryToArray("select ID_SUB_RUMPUN_ILMU,kd_rumpun_ilmu||kd_sub_rumpun_ilmu||' - '||
							nm_rumpun_ilmu||' '||nm_sub_rumpun_ilmu as detail 
							from SUB_RUMPUN_ILMU
							left join RUMPUN_ILMU on SUB_RUMPUN_ILMU.ID_RUMPUN_ILMU=RUMPUN_ILMU.ID_RUMPUN_ILMU
							where SUB_RUMPUN_ILMU.id_perguruan_tinggi = '{$id_perguruan_tinggi}' order by kd_rumpun_ilmu,kd_sub_rumpun_ilmu");


	$rincian = $db->QueryToArray("SELECT id_mata_kuliah,kd_mata_kuliah,nm_mata_kuliah,kredit_semester, kredit_tatap_muka, kredit_praktikum, kredit_prak_lapangan, kredit_simulasi,nm_jenjang_ijasah,
								sub_rumpun_ilmu.id_rumpun_ilmu,nm_rumpun_ilmu,
								kd_sub_rumpun_ilmu,nm_sub_rumpun_ilmu,SUB_RUMPUN_ILMU.ID_SUB_RUMPUN_ILMU,jenjang.nm_jenjang as nm_strata_mk,
								(SELECT COUNT(*) FROM MATA_KULIAH mk WHERE mk.KD_MATA_KULIAH = MATA_KULIAH.KD_MATA_KULIAH AND mk.ID_PROGRAM_STUDI = '{$user->ID_PROGRAM_STUDI}') AS RELASI_MATKUL,
								(SELECT COUNT(*) FROM KURIKULUM_MK WHERE KURIKULUM_MK.ID_MATA_KULIAH = mata_kuliah.ID_MATA_KULIAH) AS RELASI,
								fd_id_mk AS RELASI_FEEDER, jmk.nm_jenis_mk, kmk.nm_kelompok_mk
								from mata_kuliah
								 join program_studi ps on ps.id_program_studi = mata_kuliah.id_program_studi
								 join jenjang on jenjang.id_jenjang = ps.id_jenjang 
								 left join jenis_mk jmk on jmk.id_jenis_mk = mata_kuliah.id_jenis_mk
								 left join kelompok_mk kmk on kmk.id_kelompok_mk = mata_kuliah.id_kelompok_mk
								 left join SUB_RUMPUN_ILMU on MATA_KULIAH.ID_SUB_RUMPUN_ILMU=SUB_RUMPUN_ILMU.ID_SUB_RUMPUN_ILMU
								 left join rumpun_ilmu on sub_rumpun_ilmu.id_rumpun_ilmu=rumpun_ilmu.id_rumpun_ilmu
								where mata_kuliah.id_program_studi = '{$user->ID_PROGRAM_STUDI}' ORDER BY nm_mata_kuliah");


}

$srt=$db->QueryToArray("select id_jenjang,nm_jenjang|| '-' ||nm_jenjang_ijasah AS NM_JENJANG_IJASAH from jenjang where id_jenjang = 
	(select id_jenjang from program_studi where id_program_studi = '{$user->ID_PROGRAM_STUDI}') ");
$smarty->assign('T_STRATA', $srt);

$smarty->assign('T_DAFTAR_PRODI', $daftar_program_studi);
$smarty->assign('T_RUMPUN', $rumpun);
$smarty->assign('T_RINCIAN', $rincian);  


$jenis_mk = $db->QueryToArray("SELECT ID_JENIS_MK, NM_JENIS_MK FROM JENIS_MK");
$kelompok_mk = $db->QueryToArray("SELECT ID_KELOMPOK_MK, NM_KELOMPOK_MK, KET_KELOMPOK_MK FROM KELOMPOK_MK");
$smarty->assign('T_JENIS_MK', $jenis_mk);
$smarty->assign('T_KELOMPOK_MK', $kelompok_mk);

$smarty->assign('id_pengguna', $id_pengguna);

// khusus DSI UMAHA
$id_unit_kerja_sd = $db->QuerySingle("SELECT ID_UNIT_KERJA_SD 
										FROM PEGAWAI 
										JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = PEGAWAI.ID_PENGGUNA
										WHERE PEGAWAI.ID_PENGGUNA = {$id_pengguna}  AND PENGGUNA.ID_PERGURUAN_TINGGI = 1");
$smarty->assign('id_unit_kerja_sd', $id_unit_kerja_sd);

$smarty->assign('id_pt_user', $id_perguruan_tinggi);

$smarty->display('insert_kode_baru_s1.tpl');
?>



