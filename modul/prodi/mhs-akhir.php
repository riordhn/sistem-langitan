<?php  
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$id_perguruan_tinggi = $user->ID_PERGURUAN_TINGGI;
$kdprodi= $user->ID_PROGRAM_STUDI; 
$id_pengguna= $user->ID_PENGGUNA;
$kdfak= $user->ID_FAKULTAS;
$tahun=date('Y');

$semes = getvar("SELECT ID_SEMESTER, THN_AKADEMIK_SEMESTER, NM_SEMESTER FROM SEMESTER 
	WHERE STATUS_AKTIF_SEMESTER = 'True' AND ID_PERGURUAN_TINGGI='{$id_perguruan_tinggi}'");

$sem_aktif = $semes['ID_SEMESTER'];
//2017
$sem_aktif_tahun = $semes['THN_AKADEMIK_SEMESTER'];
$sem_aktif_semes = $semes['NM_SEMESTER'];

$kemarin_thn=""; $kemarin_sem="";
	if($sem_aktif_semes=="Ganjil") {
		$kemarin_thn = $sem_aktif_tahun-1;
		$kemarin_sem = "Genap";
	}else if($sem_aktif_semes=="Genap") {
		$kemarin_thn = $sem_aktif_tahun;
		$kemarin_sem = "Ganjil";
	}

$kemarin_idsem="";
$id_kemarin = getvar("select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."' and id_perguruan_tinggi = '{$id_perguruan_tinggi}'");
/*
echo "select id_semester from semester where thn_akademik_semester='".$kemarin_thn."' and nm_semester='".$kemarin_sem."' and id_perguruan_tinggi = '{$id_perguruan_tinggi}'";*/

$kemarin_idsem = $id_kemarin['ID_SEMESTER'];

$smt_temp=getvar("select case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,thn_akademik_semester from semester where id_semester=$kemarin_idsem");
$smarty->assign('THN_HITUNG', $smt_temp['TAHUN']);

$mhs = getData ("select s1.id_mhs,s1.nim_mhs,upper(s1.nm_pengguna) as NM_PENGGUNA,thn_angkatan_mhs,
coalesce(sks_sem,0) as sks_sem,round(coalesce(ips,0),2)as ips,
coalesce(SKS_TOTAL_MHS,0) as SKS_TOTAL_MHS,round(coalesce(IPK_MHS,0),2)as IPK_MHS
from
(select id_mhs,nim_mhs,nm_pengguna,case when sum(bobot*kredit_semester)=0 then 0 else 
round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
sum(kredit_semester) as sks_sem
--case when id_fakultas=$kdfak then sum(sksreal) else sum(kredit_semester) end as sks_sem 
from 
(select id_mhs,nim_mhs,nm_pengguna,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
(select a.id_mhs,m.nim_mhs,pg.nm_pengguna,ps.id_fakultas,a.id_kurikulum_mk, 
case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
and d.status_mkta in (1,2) then 0
else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
from pengambilan_mk a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join pengguna pg on m.id_pengguna=pg.id_pengguna 
left join program_studi ps on m.id_program_studi=ps.id_program_studi
left join semester s on a.id_semester=s.id_semester
where group_semester||thn_akademik_semester in
(select group_semester||thn_akademik_semester from semester where id_Semester=$kemarin_idsem)
and tipe_semester in ('UP','REG','RD') 
and a.status_apv_pengambilan_mk='1' and m.id_program_studi=$kdprodi and a.status_hapus=0 
and a.status_pengambilan_mk !=0)
group by id_mhs, nim_mhs,nm_pengguna,id_kurikulum_mk, id_fakultas, kredit_semester, sksreal
)
group by id_mhs,nim_mhs,nm_pengguna,id_fakultas)s1
left join
(select id_mhs, sum(kredit_semester) as SKS_TOTAL_MHS, 
round((sum((kredit_semester * nilai_standar_nilai)) / sum(kredit_semester)), 2) as IPK_MHS
from 
(select a.id_mhs, c.kd_mata_kuliah,d.kredit_semester,a.nilai_huruf, e.nilai_standar_nilai,
case when group_semester='Ganjil' then thn_akademik_semester||1 else thn_akademik_semester||2 end as tahun,
row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a
join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
join mata_kuliah c on d.id_mata_kuliah=c.id_mata_kuliah 
join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
join semester smt on a.id_semester=smt.id_semester
join mahasiswa m on a.id_mhs=m.id_mhs
where a.status_apv_pengambilan_mk = 1 and (a.nilai_huruf<>'E' or a.nilai_huruf<>null) 
and m.id_program_studi=$kdprodi and a.status_hapus=0 
and a.status_pengambilan_mk !=0)
where tahun<= '$smt_temp[TAHUN]'and rangking=1
group by id_mhs) s2 on s1.id_mhs=s2.id_mhs
join
(select distinct id_mhs,thn_angkatan_mhs,program_studi.id_jenjang 
from mahasiswa
join pengguna on mahasiswa.id_pengguna=PENGGUNA.id_pengguna
join program_studi on mahasiswa.id_program_studi = program_studi.id_program_studi
where thn_angkatan_mhs <= 
case when id_jenjang = 5 then  {$tahun}-2
when id_jenjang in (1,4) then {$tahun}-3
else 1 end
and id_mhs in (select id_mhs from mahasiswa where id_program_studi=$kdprodi))s3 on s1.id_mhs=s3.id_mhs 
 order by nim_mhs"); 
	
$smarty->assign('mhs', $mhs);
$smarty->display('mhs-akhir.tpl');
?>