<?php
include 'config.php';
include 'ociFunction.php';

$kdprodi	= $user->ID_PROGRAM_STUDI;
$id_pt		= $id_pt_user;

$id_pengguna	= $user->ID_PENGGUNA;
$kdfak			= $user->ID_FAKULTAS;
$smarty->assign('FAK', $kdfak);

$smtaktif = getvar("SELECT id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = '{$id_pt}'");
$smt = $smtaktif['ID_SEMESTER'];

if ($kdfak == 0)
{
	// ubah by FIKRIE
	/*$dataprodi = getData(
		"SELECT id_program_studi||'-'||id_fakultas as id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi 
		left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang 
		where id_fakultas=$kdfak");*/
	$dataprodi = getData(
		"SELECT id_program_studi||'-'||id_fakultas as id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi 
		left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang 
		where id_program_studi=$kdprodi");
	$smarty->assign('T_PRODI', $dataprodi);	
}
else
{
	// ubah by FIKRIE
	/*$dataprodi = getData(
		"SELECT id_program_studi||'-'||id_fakultas as id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi 
		left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang 
		where id_fakultas=$kdfak and status_aktif_prodi = 1");*/
	$dataprodi = getData(
		"SELECT id_program_studi||'-'||id_fakultas as id_program_studi,nm_jenjang||'-'||nm_program_studi as prodi from program_studi 
		left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang 
		where id_program_studi=$kdprodi and status_aktif_prodi = 1");
	$smarty->assign('T_PRODI', $dataprodi);
}

if (isset($_POST['action']) == 'view')
{
	$var = explode("-", $_POST['kdprodi']);

	if ($kdfak == 6)
	{
		if ($var[0] == 'all')
		{
			$dataprodi1=getData("SELECT (': FAKULTAS '||nm_fakultas) as prodi from fakultas where id_fakultas=".$var[1]."");
			$smarty->assign('NM_PRODI', $dataprodi1);

			$jaf = getData("SELECT s1.id_kelas_mk,kd_mata_kuliah, nm_mata_kuliah,
			kredit_semester, nama_kelas,id_jadwal_hari, nm_jadwal_hari,nm_ruangan,jam_awal||'-'||b.jam_selesai||':'||b.menit_selesai as jam,  
			nm_semester,tahun_ajaran, s1.id_fakultas,id_program_studi,id_jenjang,s2.ttl as peserta
			from
			(select distinct kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,
			kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.id_jadwal_hari, jadwal_hari.nm_jadwal_hari, ruangan.nm_ruangan,
			jam_mulai||':'||menit_mulai as jam_awal, kdjam+kurikulum_mk.kredit_semester-1 as akhir, 
			semester.nm_semester, semester.tahun_ajaran, program_studi.id_fakultas, kelas_mk.id_program_studi, program_studi.id_jenjang
			from kelas_mk
			left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
			left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
			left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
			left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
			left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
			where kelas_mk.id_semester=$smt 
			and program_studi.id_fakultas=".$var[1].")s1
			left join
			(select distinct id_kelas_mk,count(*) as ttl from pengambilan_mk
			where id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi
			in (select id_program_studi from program_studi where id_fakultas=".$var[1].") and id_semester=$smt
			and status_apv_pengambilan_mk=1
			group by id_kelas_mk)s2 on s1.id_kelas_mk=s2.id_kelas_mk
			left join jadwal_jam b on s1.akhir=b.kdjam and s1.id_fakultas=b.id_fakultas
			order by kd_mata_kuliah,nama_kelas");

		}
		else
		{
			$dataprodi1=getData("select (': PRODI '||nm_jenjang||'-'||nm_program_studi) as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=".$var[0]."");
			$smarty->assign('NM_PRODI', $dataprodi1);

			$jaf=getData("SELECT s1.id_kelas_mk,kd_mata_kuliah, nm_mata_kuliah,
			kredit_semester, nama_kelas,id_jadwal_hari, nm_jadwal_hari,nm_ruangan,jam_awal||'-'||b.jam_selesai||':'||b.menit_selesai as jam,  
			nm_semester,tahun_ajaran, s1.id_fakultas,id_program_studi,id_jenjang,s2.ttl as peserta
			from
			(select distinct kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,
			kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.id_jadwal_hari, jadwal_hari.nm_jadwal_hari, ruangan.nm_ruangan,
			jam_mulai||':'||menit_mulai as jam_awal, kdjam+kurikulum_mk.kredit_semester-1 as akhir, 
			semester.nm_semester, semester.tahun_ajaran, program_studi.id_fakultas, kelas_mk.id_program_studi, program_studi.id_jenjang
			from kelas_mk
			left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
			left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
			left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
			left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
			left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
			where kelas_mk.id_semester=$smt 
			and kelas_mk.id_program_studi=".$var[0].")s1
			left join
			(select distinct id_kelas_mk,count(*) as ttl from pengambilan_mk
			where id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi=".$var[0].") and id_semester=$smt
			and status_apv_pengambilan_mk=1
			group by id_kelas_mk)s2 on s1.id_kelas_mk=s2.id_kelas_mk
			left join jadwal_jam b on s1.akhir=b.kdjam and s1.id_fakultas=b.id_fakultas
			order by kd_mata_kuliah,nama_kelas");
		}
	}
	else
	{
		if ($var[0] == 'all') 
		{
			$dataprodi1=getData("select (': FAKULTAS '||nm_fakultas) as prodi from fakultas where id_fakultas=".$var[1]."");
			$smarty->assign('NM_PRODI', $dataprodi1);

			$jaf=getData("select s1.id_kelas_mk,kd_mata_kuliah, nm_mata_kuliah,
			kredit_semester, nama_kelas,id_jadwal_hari, nm_jadwal_hari,nm_ruangan,jam, 
			nm_semester,tahun_ajaran, id_fakultas,id_program_studi,id_jenjang,s2.ttl as peserta
			from
			(select distinct kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,
			kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.id_jadwal_hari, jadwal_hari.nm_jadwal_hari, ruangan.nm_ruangan,
			jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, 
			semester.nm_semester, semester.tahun_ajaran, program_studi.id_fakultas, kelas_mk.id_program_studi, program_studi.id_jenjang
			from kelas_mk
			left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
			left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
			left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
			left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
			left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
			where kelas_mk.id_semester=$smt 
			and program_studi.id_fakultas=".$var[1].")s1
			left join
			(select distinct id_kelas_mk,count(*) as ttl from pengambilan_mk
			where id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi
			in (select id_program_studi from program_studi where id_fakultas=".$var[1].") and id_semester=$smt
			and status_apv_pengambilan_mk=1
			group by id_kelas_mk)s2 on s1.id_kelas_mk=s2.id_kelas_mk
			order by kd_mata_kuliah,nama_kelas");

		} 
		else
		{
			$dataprodi1=getData("select (': PRODI '||nm_jenjang||'-'||nm_program_studi) as prodi from program_studi left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang where id_program_studi=".$var[0]."");
			$smarty->assign('NM_PRODI', $dataprodi1);

			$jaf=getData("select s1.id_kelas_mk,kd_mata_kuliah, nm_mata_kuliah,
			kredit_semester, nama_kelas,id_jadwal_hari, nm_jadwal_hari,nm_ruangan,jam, 
			nm_semester,tahun_ajaran, id_fakultas,id_program_studi,id_jenjang,s2.ttl as peserta
			from
			(select distinct kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.nm_mata_kuliah,
			kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, jadwal_hari.id_jadwal_hari, jadwal_hari.nm_jadwal_hari, ruangan.nm_ruangan,
			jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, 
			semester.nm_semester, semester.tahun_ajaran, program_studi.id_fakultas, kelas_mk.id_program_studi, program_studi.id_jenjang
			from kelas_mk
			left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk=kelas_mk.id_kurikulum_mk
			left join mata_kuliah on mata_kuliah.id_mata_kuliah=kurikulum_mk.id_mata_kuliah
			left join nama_kelas on nama_kelas.id_nama_kelas=kelas_mk.no_kelas_mk
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join jadwal_hari on jadwal_hari.id_jadwal_hari=jadwal_kelas.id_jadwal_hari
			left join jadwal_jam on jadwal_jam.id_jadwal_jam=jadwal_kelas.id_jadwal_jam
			left join ruangan on ruangan.id_ruangan=jadwal_kelas.id_ruangan
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
			where kelas_mk.id_semester=$smt 
			and kelas_mk.id_program_studi=".$var[0].")s1
			left join
			(select distinct id_kelas_mk,count(*) as ttl from pengambilan_mk
			where id_kelas_mk in (select id_kelas_mk from kelas_mk where id_program_studi=".$var[0].") and id_semester=$smt
			and status_apv_pengambilan_mk=1
			group by id_kelas_mk)s2 on s1.id_kelas_mk=s2.id_kelas_mk
			order by kd_mata_kuliah,nama_kelas");
		}
	}

	$smarty->assign('T_MK', $jaf);
}
/*if ($kdfak == 11) {
$smarty->display('presensi-kuliah-psikologi.tpl');
} else {*/
$smarty->display('presensi-kuliah.tpl');
/*}*/