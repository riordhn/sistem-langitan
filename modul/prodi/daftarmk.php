<?php
require 'common.php';
require_once 'ociFunction.php';

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');

$id_perguruan_tinggi = $user->ID_PERGURUAN_TINGGI; 

$kdfak = $user->ID_FAKULTAS;
$kdprodi = $user->ID_PROGRAM_STUDI; 
$id_pengguna= $user->ID_PENGGUNA;
$smarty->assign('kdfak', $kdfak);


$id_thkur=getvar("SELECT id_kurikulum from kurikulum 
where id_program_studi=$kdprodi and status_aktif=1"); // ambil kurikulum tahun terakhir

$smarty->assign('IDKUR',$id_thkur['ID_KURIKULUM']);

$datakur=getData("SELECT id_kurikulum,nm_kurikulum||' ('||thn_kurikulum||')' AS nama FROM kurikulum
where status_aktif = 1 and id_program_studi=$kdprodi");
$smarty->assign('T_KUR', $datakur);

//Yudi Sulistya, 23/08/2012 (seting mk agama)
$dataagama=getData("SELECT id_agama,nm_agama from agama where id_agama!=6 order by id_agama");
$smarty->assign('T_AGM', $dataagama);

$status = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'view';
//echo $status;

switch($status) {
	 case 'view':
		 //echo $id_thkur['ID_KURIKULUM_PRODI'];
		//$thkur = $_POST['thkur'];
		$thkur = isSet($_POST['thkur']) ? $_POST['thkur'] :$id_thkur['ID_KURIKULUM'];
        break;
		
	case 'del':
		$idmk = $_GET['id_mk'];
		echo '<script>alert("Data Akan di Hapus...")</script>';

		deleteData("delete from kurikulum_mk where id_kurikulum_mk='$idmk'");
        
        break;
	
	case 'update1':
		$id_matkul = $_POST['id_mk'];		
		$sks_total = $_POST['kredit_matkul'];
		$sks_tatap_muka = $_POST['kredit_tm'];
		$sks_praktikum = $_POST['kredit_prk'];
		$sks_prak_lapangan = $_POST['kredit_prak_lapangan'];
		$sks_tutor = $_POST['kredit_tutor'];
		$sks_simulasi = $_POST['kredit_simulasi'];
		$nama_en = $_POST['en'];
		$kelompok_mk = $_POST['kelompok_mk'];
		$smt = $_POST['tingkat_smt'];
		$thkur = $_POST['thkur'];
		$mkta = $_POST['status_mkta'];
		$agama = $_POST['status_mk_agama'];
		$status_seri = $_POST['status_seri'];

		$id_kur_prodi = $_POST['id_kur_prodi'];

		$smarty->assign('id_kur_prodi',$id_kur_prodi);
		
		if(isset($_POST['status_paket'])){
			$status_paket = $_POST['status_paket'];
		}else{
			$status_paket = '';
		}
		/*
		
		
		$replace = array(",");
		$replaceto = array(".");
		$sks_total = str_replace($replace, $replaceto, $kredit_matkul);
		$sks_tatap_muka = str_replace($replace, $replaceto, $kredit_tatap_muka);
		$sks_praktikum = str_replace($replace, $replaceto, $kredit_praktikum);
		*/
		/*
		echo "update kurikulum_mk set kredit_semester='$sks_total', id_kelompok_mk='$kelompok_mk',
					id_program_studi='$kdprodi', tingkat_semester='$smt', status_mkta='$mkta', status_mk_agama='$agama',
					id_kurikulum_prodi='$thkur', kredit_tatap_muka='$sks_tatap_muka', kredit_praktikum='$sks_praktikum'
					where id_kurikulum_mk=$id_matkul";
		*/
		
		UpdateData("UPDATE kurikulum_mk set kredit_semester='$sks_total', id_kelompok_mk='$kelompok_mk',
					id_program_studi='$kdprodi', tingkat_semester='$smt', status_mkta='$mkta', status_mk_agama='$agama',
					id_kurikulum='$thkur',
					kredit_tatap_muka='$sks_tatap_muka', kredit_praktikum='$sks_praktikum',
					kredit_prak_lapangan='$sks_prak_lapangan', kredit_tutor='$sks_tutor', kredit_simulasi='$sks_simulasi',
					status_paket = '$status_paket', status_seri = '$status_seri'
					where id_kurikulum_mk=$id_matkul");
		
		#$mk = getvar("");
		$id_mata_kuliah = $mk['ID_MATA_KULIAH'];
		UpdateData("UPDATE mata_kuliah set nm_mata_kuliah_en = upper('".$nama_en."') where id_mata_kuliah =(select id_mata_kuliah from kurikulum_mk where id_kurikulum_mk='{$id_matkul}')");

        break;
		
	case 'add':
		
		$id_matkul = $_GET['id_mk'];
		$tkur=$_GET['tkur'];

		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none'); 
		$smarty->assign('disp3','block');

		// Ambil data kredit_semester, dll
		// FIKRIE (11-02-2019)
		$db->Query("SELECT * FROM MATA_KULIAH WHERE ID_MATA_KULIAH = {$id_matkul}");
		$matkul = $db->FetchAssoc();

		$kredit_tatap_muka 		= $matkul['KREDIT_TATAP_MUKA'];
		$kredit_praktikum 		= $matkul['KREDIT_PRAKTIKUM'];
		$kredit_prak_lapangan 	= $matkul['KREDIT_PRAK_LAPANGAN'];
		$kredit_simulasi 		= $matkul['KREDIT_SIMULASI'];
		$kredit_semester 		= $matkul['KREDIT_SEMESTER']; 

		tambahdata("kurikulum_mk","id_mata_kuliah,id_program_studi,id_kurikulum,kredit_tatap_muka,kredit_praktikum,kredit_prak_lapangan,kredit_simulasi,kredit_semester","$id_matkul,$kdprodi,$tkur,$kredit_tatap_muka,$kredit_praktikum,$kredit_prak_lapangan,$kredit_simulasi,$kredit_semester");
         
        break;    
   
	case 'viewup':
		$thkur = $_POST['thkur'];
		$idmk = $_GET['id_mk'];
		if (trim($idmk) != '') {
		
		$smarty->assign('idmk',$idmk);
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block'); 
		$smarty->assign('disp3','none');}

		$datamp=getData("SELECT * from prodi_minat where id_program_studi=$kdprodi");
		$smarty->assign('MP_PRODI', $datamp);

		$datakelmk=getData("SELECT * from kelompok_mk");
		$smarty->assign('KEL_MK', $datakelmk);
	
		$datakur=getData(
			"SELECT id_kurikulum,nm_kurikulum||' ('||thn_kurikulum||')'  as nama  from kurikulum
			where id_program_studi=$kdprodi");

		$smarty->assign('T_KUR1', $datakur);
		
		$mk=getData("SELECT kmk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,nm_mata_kuliah_en, 
		kmk.kredit_tatap_muka,kmk.kredit_praktikum,kmk.kredit_semester, kmk.status_mkta,nm_kelompok_mk,
		case when kmk.status_mkta=1 then 'Skripsi/TA/Thesis' when kmk.status_mkta=2 then 'KKN' when kmk.status_mkta=3 then 'Magang/PKL' when kmk.status_mkta=4 then 'Keasistenan'
		when kmk.status_mkta=5 then 'Modul/Skill/Micro' when kmk.status_mkta=6 then 'Praktikum' else 'Tidak' end as mkta,
		nm_prodi_minat,nm_kurikulum||'-'||kurikulum.thn_kurikulum as nama, kurikulum.thn_kurikulum,
		tingkat_semester,status_seri,kmk.id_status_mk, kmk.id_kelompok_mk,kmk.id_kurikulum, kmk.status_mk_agama, status_paket,
		(case when status_paket is null then 'Reguler' when status_paket = 'dm' then 'Dokter Muda' when status_paket = 'sp1' then 'Suppro 1' 
		when status_paket = 'sp2' then 'Suppro 2' when status_paket = 'sp3' then 'Suppro 3' end) as nm_status_paket
		from kurikulum_mk kmk left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
		left join status_mk on kmk.id_status_mk=status_mk.id_status_mk 
		left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk 
		--left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi 
		left join kurikulum on kmk.id_kurikulum=kurikulum.id_kurikulum 
		left join prodi_minat on kmk.id_prodi_minat=prodi_minat.id_prodi_minat and prodi_minat.id_program_studi=$kdprodi
		where kmk.id_program_studi=$kdprodi and id_kurikulum_mk=$idmk");
		$smarty->assign('T_MK1', $mk);
           
	break;
	
	case 'insertmk':
		$nama_mk = $_POST['nama_mk'];
		
		// Added by Yudi Sulistya on Dec 02, 2011
		$upper = strtoupper($nama_mk);
		$lower = strtolower($nama_mk);
		$proper = ucwords($nama_mk);
		
		$idkurprodi = $_POST['thkur1'];
		$smarty->assign('ID_KP', $idkurprodi);
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block'); 

		## TAMBAHAN MATA_KULIAH YANG SUDAH MASUK PADA KURIKULUM_MK TIDAK TAMPIL LAGI
		$mk=getData("SELECT id_mata_kuliah,kd_mata_kuliah,nm_mata_kuliah,nm_mata_kuliah_en 
			FROM mata_kuliah WHERE id_program_studi = '{$kdprodi}' AND kd_mata_kuliah NOT IN (SELECT KD_MATA_KULIAH FROM KURIKULUM_MK JOIN MATA_KULIAH mk ON mk.ID_MATA_KULIAH = KURIKULUM_MK.ID_MATA_KULIAH WHERE KURIKULUM_MK.ID_PROGRAM_STUDI = '{$kdprodi}' AND KURIKULUM_MK.ID_KURIKULUM = '{$idkurprodi}') AND (nm_mata_kuliah LIKE '%$upper%' OR nm_mata_kuliah 
			LIKE '%$lower%' OR nm_mata_kuliah LIKE '%$proper%') ORDER BY nm_mata_kuliah");
		$smarty->assign('INSERT_MK', $mk);
           
	break;

}

$id_kur_prodi = $_POST['id_kur_prodi'];

$thkur = isset($_POST['thkur']) ? $_POST['thkur'] :$id_thkur['ID_KURIKULUM'];

if($id_kur_prodi != ''){
	$thkur = $id_kur_prodi;
}

// $mk=getData("
// SELECT kmk.id_kurikulum_mk,(SELECT count(kmk1.ID_KURIKULUM_MK) 
// FROM kurikulum_mk kmk1 
// join mata_kuliah mk on mk.id_mata_kuliah = kmk1.id_mata_kuliah 
// where mk.kd_mata_kuliah = mata_kuliah.kd_mata_kuliah and kmk1.id_program_studi='{$kdprodi}' and kmk1.id_kurikulum='{$thkur}') jml, kd_mata_kuliah,nm_mata_kuliah,nm_mata_kuliah_en,kmk.kredit_tatap_muka,
// kmk.kredit_praktikum,kmk.kredit_prak_lapangan,kmk.kredit_tutor,kmk.kredit_simulasi,kmk.kredit_semester,nm_status_mk,nm_kelompok_mk,kurikulum.thn_kurikulum,
// tingkat_semester, (SELECT COUNT(*) FROM PENGAMBILAN_MK WHERE PENGAMBILAN_MK.ID_KURIKULUM_MK = kmk.ID_KURIKULUM_MK) AS RELASI_PENGAMBILAN, (SELECT COUNT(*) FROM KELAS_MK WHERE KELAS_MK.ID_KURIKULUM_MK = kmk.ID_KURIKULUM_MK) AS RELASI_KELAS
// from kurikulum_mk kmk 
// left join kurikulum on kurikulum.id_kurikulum = kmk.id_kurikulum
// left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
// left join status_mk on kmk.id_status_mk=status_mk.id_status_mk 
// left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk 
// where kmk.id_program_studi='{$kdprodi}' and kmk.id_kurikulum='{$thkur}'");
$mk=getData("
SELECT
	kmk.id_kurikulum_mk,
	(
SELECT
	count( * ) 
FROM
	kurikulum_mk kmk1
	JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk1.id_mata_kuliah 
WHERE
	mk.kd_mata_kuliah = mata_kuliah.kd_mata_kuliah 
	AND kmk1.id_program_studi = '{$kdprodi}' 
	AND kmk1.id_kurikulum = '{$thkur}' 
	) jml,
	mata_kuliah.kd_mata_kuliah,
	mata_kuliah.nm_mata_kuliah,
	mata_kuliah.nm_mata_kuliah_en,
	kmk.kredit_tatap_muka,
	kmk.kredit_praktikum,
	kmk.kredit_prak_lapangan,
	kmk.kredit_tutor,
	kmk.kredit_simulasi,
	kmk.kredit_semester,
	status_mk.nm_status_mk,
	kelompok_mk.nm_kelompok_mk,
	kurikulum.thn_kurikulum,
	kmk.tingkat_semester,
	 COUNT(PENGAMBILAN_MK.ID_PENGAMBILAN_MK)  AS RELASI_PENGAMBILAN,
	COUNT( KELAS_MK.ID_KELAS_MK ) AS RELASI_KELAS 
FROM
	kurikulum_mk kmk
	LEFT JOIN kurikulum ON kurikulum.id_kurikulum = kmk.id_kurikulum
	LEFT JOIN mata_kuliah ON kmk.id_mata_kuliah = mata_kuliah.id_mata_kuliah
	LEFT JOIN status_mk ON kmk.id_status_mk = status_mk.id_status_mk
	LEFT JOIN kelompok_mk ON kmk.id_kelompok_mk = kelompok_mk.id_kelompok_mk 
	left join pengambilan_mk on PENGAMBILAN_MK.ID_KURIKULUM_MK = kmk.ID_KURIKULUM_MK 
	left join KELAS_MK on KELAS_MK.ID_KURIKULUM_MK = kmk.ID_KURIKULUM_MK
WHERE
	kmk.id_program_studi = '{$kdprodi}' 
	AND kmk.id_kurikulum = '{$thkur}'
group by kmk.id_kurikulum_mk,
	mata_kuliah.kd_mata_kuliah,
	mata_kuliah.nm_mata_kuliah,
	mata_kuliah.nm_mata_kuliah_en,
	kmk.kredit_tatap_muka,
	kmk.kredit_praktikum,
	kmk.kredit_prak_lapangan,
	kmk.kredit_tutor,
	kmk.kredit_simulasi,
	kmk.kredit_semester,
	status_mk.nm_status_mk,
	kelompok_mk.nm_kelompok_mk,
	kurikulum.thn_kurikulum,
	kmk.tingkat_semester");
$smarty->assign('T_MK', $mk);

// Tambahan untuk mengetahui kode mk yang kembar dalam satu kurikulum prodi
// FIKRIE
// 18-08-2016
$mk_kembar=getData("SELECT 
 kd_mata_kuliah ,
 COUNT(*) jumlah
FROM 
 kurikulum_mk kmk
JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
WHERE kmk.id_program_studi='{$kdprodi}' and kmk.id_kurikulum='{$thkur}'
GROUP BY
 kd_mata_kuliah
Having
COUNT(*) > 1");
$smarty->assign('T_MK_KEMBAR', $mk_kembar);

$smarty->assign('id_pengguna', $id_pengguna);

// khusus DSI UMAHA
$id_unit_kerja_sd = $db->QuerySingle("SELECT ID_UNIT_KERJA_SD 
										FROM PEGAWAI 
										JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = PEGAWAI.ID_PENGGUNA
										WHERE PEGAWAI.ID_PENGGUNA = {$id_pengguna}  AND PENGGUNA.ID_PERGURUAN_TINGGI = 1");
$smarty->assign('id_unit_kerja_sd', $id_unit_kerja_sd);

$smarty->assign('id_pt_user', $id_perguruan_tinggi);

$smarty->display('daftarmk.tpl');  