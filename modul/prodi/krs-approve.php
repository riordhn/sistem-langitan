<?php
/*echo "fitur sedang dalam perbaikan";
exit();*/


include 'config.php';

$id_pengguna= $user->ID_PENGGUNA; 

//  FITUR khusus DSI UMAHA
$id_unit_kerja_sd = $db->QuerySingle("SELECT ID_UNIT_KERJA_SD 
										FROM PEGAWAI 
										JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = PEGAWAI.ID_PENGGUNA
										WHERE PEGAWAI.ID_PENGGUNA = {$id_pengguna} AND PENGGUNA.ID_PERGURUAN_TINGGI = 1");

if($id_unit_kerja_sd != 30) {
	echo "FITUR DITUTUP";
	exit();
}

$mode = get('mode', 'view');

$id_semester_aktif = $db->QuerySingle("select id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}");

$id_semester_sp_aktif = $db->QuerySingle("select id_semester from semester where status_aktif_sp='True' and id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}");

if ($_SERVER['REQUEST_METHOD'] == 'GET' || $_SERVER['REQUEST_METHOD'] == 'POST')
{
	if ($mode == 'view')
	{
		$id_semester		= get('id_semester', 0);
		$id_semester		= (get('id_semester', 0) == 0) ? $id_semester_aktif : $id_semester;
		$thn_angkatan_mhs	= get('thn_angkatan_mhs', 'all');

		// khusus DSI UMAHA
		$id_unit_kerja_sd = $db->QuerySingle("SELECT ID_UNIT_KERJA_SD 
												FROM PEGAWAI 
												JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = PEGAWAI.ID_PENGGUNA
												WHERE PEGAWAI.ID_PENGGUNA = {$id_pengguna}  AND PENGGUNA.ID_PERGURUAN_TINGGI = 1");
		
		$semester_set = $db->QueryToArray(
			"select * from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}'
			order by thn_akademik_semester desc, nm_semester desc");

		$semester_aktif_set = $db->QueryToArray(
			"select * from semester where id_semester IN ('{$id_semester_aktif}','{$id_semester_sp_aktif}')");

		$thn_angkatan_set = $db->QueryToArray(
			"SELECT DISTINCT thn_angkatan_mhs FROM mahasiswa m
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna AND p.id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}
			JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs
			WHERE sp.status_aktif = 1
			ORDER BY 1 DESC");
			
		$where_angkatan = ($thn_angkatan_mhs != 'all') ? 'AND m.thn_angkatan_mhs = '.$thn_angkatan_mhs : '';

		$prodi_set = $db->QueryToArray(
			"SELECT ps.id_program_studi, nm_jenjang||' '||nm_program_studi AS nm_program_studi, coalesce(jumlah_mhs,0) as jumlah_mhs, coalesce(jumlah_krs,0) as jumlah_krs
			FROM program_studi ps
			JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
			JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
			LEFT JOIN (
				/* Jumlah Mhs Aktif per Angkatan */
				SELECT ps.id_program_studi, COUNT(m.id_mhs) jumlah_mhs 
				FROM mahasiswa m
				JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
				JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs
				WHERE
					ps.id_program_studi = {$user->ID_PROGRAM_STUDI} AND
					sp.status_aktif = 1 
					{$where_angkatan}
				GROUP BY ps.id_program_studi
				) maba ON maba.id_program_studi = ps.id_program_studi
			LEFT JOIN (
				/* Jumlah Mhs Aktif per Angkatan yg KRS */
				SELECT m.id_program_studi, count(DISTINCT pmk.id_mhs) AS jumlah_krs 
				FROM pengambilan_mk pmk
				JOIN mahasiswa m ON m.id_mhs = pmk.id_mhs
				JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
				JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs
				WHERE
					ps.id_program_studi = {$user->ID_PROGRAM_STUDI} AND
					sp.status_aktif = 1 AND
					pmk.id_semester = {$id_semester}
					{$where_angkatan}
				GROUP BY m.id_program_studi
				) krs ON krs.id_program_studi = ps.id_program_studi
			WHERE 
				ps.id_program_studi = {$user->ID_PROGRAM_STUDI}
			ORDER BY 2 ASC");
		
		$smarty->assign('id_pengguna', $id_pengguna);
		$smarty->assign('id_unit_kerja_sd', $id_unit_kerja_sd);
		$smarty->assign('id_semester', $id_semester);
		$smarty->assign('semester_set', $semester_set);
		$smarty->assign('semester_aktif_set', $semester_aktif_set);
		$smarty->assign('thn_angkatan_mhs', $thn_angkatan_mhs);
		$smarty->assign('thn_angkatan_set', $thn_angkatan_set);
		$smarty->assign('prodi_set', $prodi_set);
		
		
	}
	
	if ($mode == 'pilih-mk')
	{
		$id_program_studi	= (int)get('id_program_studi', 0);
		$id_semester		= (int)(get('id_semester', 0) == 0) ? $id_semester_aktif : get('id_semester', 0);
		$tingkat_semester	= get('tingkat_semester', 'all');
		$id_nama_kelas		= get('id_nama_kelas', 'all');
		$thn_angkatan_mhs	= get('thn_angkatan_mhs', 'all');
		
		$prodi_set = $db->QueryToArray(
			"SELECT ps.id_program_studi, nm_jenjang||' '||nm_program_studi AS nm_program_studi
			FROM program_studi ps
			JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
			JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
			WHERE ps.id_program_studi = {$user->ID_PROGRAM_STUDI}");
		
		// khusus DSI UMAHA
		$id_unit_kerja_sd = $db->QuerySingle("SELECT ID_UNIT_KERJA_SD 
												FROM PEGAWAI 
												JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = PEGAWAI.ID_PENGGUNA
												WHERE PEGAWAI.ID_PENGGUNA = {$id_pengguna}  AND PENGGUNA.ID_PERGURUAN_TINGGI = 1");
		
		$semester_set = $db->QueryToArray(
			"select * from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}'
			order by thn_akademik_semester desc, nm_semester desc");

		$semester_aktif_set = $db->QueryToArray(
			"select * from semester where id_semester IN ('{$id_semester_aktif}','{$id_semester_sp_aktif}')");
			
		$nama_kelas_set = $db->QueryToArray(
			"select * from nama_kelas where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' ORDER BY nama_kelas");

		$thn_angkatan_set = $db->QueryToArray(
			"SELECT DISTINCT thn_angkatan_mhs FROM mahasiswa m
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna AND p.id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}
			JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs
			WHERE sp.status_aktif = 1
			ORDER BY 1 DESC");
		
		$where = "";
		if ($tingkat_semester != 'all') $where .= "AND tingkat_semester = {$tingkat_semester} ";
		if ($id_nama_kelas != 'all') $where .= "AND nk.id_nama_kelas = {$id_nama_kelas} ";
			
		$kelas_mk_set = $db->QueryToArray(
			"SELECT
				kmk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah,
				kur.kredit_semester, kur.tingkat_semester,
				nk.nama_kelas,
				nvl2(pjmk_gelar_depan,pjmk_gelar_depan||' ','')||initcap(nama_pjmk)||nvl2(pjmk_gelar_belakang,', '||pjmk_gelar_belakang,'') as nama_pjmk,

				/* Jumlah KRS */
				(SELECT count(pmk.id_mhs) FROM pengambilan_mk pmk
				WHERE pmk.id_kelas_mk = kmk.id_kelas_mk) as peserta,
				
				/* Jumlah KRS Approve */
				(SELECT count(pmk.id_mhs) FROM pengambilan_mk pmk
				WHERE pmk.status_apv_pengambilan_mk = 1 and pmk.id_kelas_mk = kmk.id_kelas_mk) as peserta_approve
			FROM krs_prodi krs
			JOIN kelas_mk kmk ON kmk.id_kelas_mk = krs.id_kelas_mk AND kmk.id_semester = krs.id_semester
			JOIN kurikulum_mk kur ON kur.id_kurikulum_mk = kmk.id_kurikulum_mk
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kur.id_mata_kuliah
			LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kmk.no_kelas_mk
			LEFT JOIN (
				SELECT pmk.id_kelas_mk, p.nm_pengguna as nama_pjmk, p.gelar_depan as pjmk_gelar_depan, p.gelar_belakang as pjmk_gelar_belakang
				FROM pengampu_mk pmk
				JOIN dosen d ON d.id_dosen = pmk.id_dosen
				JOIN pengguna p ON p.id_pengguna = d.id_pengguna
				WHERE pjmk_pengampu_mk = 1) pjmk on pjmk.id_kelas_mk = kmk.id_kelas_mk
			JOIN program_studi ps ON ps.id_program_studi = krs.id_program_studi
			JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
			WHERE
				f.id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI} AND
				kmk.id_program_studi = {$id_program_studi} AND
				kmk.id_semester = {$id_semester} 
				{$where}");
		
		$smarty->assign('id_pengguna', $id_pengguna);
		$smarty->assign('id_unit_kerja_sd', $id_unit_kerja_sd);
		$smarty->assign('id_semester', $id_semester);
		$smarty->assign('semester_set', $semester_set);
		$smarty->assign('semester_aktif_set', $semester_aktif_set);
		$smarty->assign('prodi_set', $prodi_set);
		$smarty->assign('nama_kelas_set', $nama_kelas_set);
		$smarty->assign('thn_angkatan_mhs', $thn_angkatan_mhs);
		$smarty->assign('thn_angkatan_set', $thn_angkatan_set);
		$smarty->assign('kelas_mk_set', $kelas_mk_set);
	}
	
	
	if ($mode == 'pilih-mhs')
	{
		// Redirect ke view jika GET
		if ($_SERVER['REQUEST_METHOD'] == 'GET') { echo '<script>top.window.location=\'#aktivitas-approvekrs!krs-approve.php\';</script>'; exit(); }
		
		$id_program_studi	= post('id_program_studi', 0);
		$thn_angkatan_mhs	= post('thn_angkatan_mhs', 'all');
		$id_nama_kelas		= post('id_nama_kelas', 'all');

		$id_kelas_mk = post('id_kelas_mk', 0);
		
		$db->Query(
			"SELECT ps.id_program_studi, nm_jenjang||' '||nm_program_studi AS nm_program_studi
			FROM program_studi ps
			JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
			WHERE ps.id_program_studi = {$id_program_studi}");
		$prodi = $db->FetchAssoc();

		//disable option pilih angkatan dan option pilih kelas
		$thn_angkatan_set = $db->QueryToArray(
			"SELECT DISTINCT thn_angkatan_mhs FROM mahasiswa m
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna AND p.id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}
			JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs
			WHERE sp.status_aktif = 1
			ORDER BY 1 DESC");
			
		$nama_kelas_set = $db->QueryToArray(
			"select * from nama_kelas where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' ORDER BY nama_kelas");
		
		$where = "";
		if ($id_nama_kelas != 'all') $where .= "AND nk.id_nama_kelas = {$id_nama_kelas} ";
		if ($thn_angkatan_mhs != 'all') $where .= "AND m.thn_angkatan_mhs = {$thn_angkatan_mhs} ";
			
		$mahasiswa_set = $db->QueryToArray(
			"SELECT m.id_mhs, m.nim_mhs, p.nm_pengguna, thn_angkatan_mhs, nk.nama_kelas
			FROM mahasiswa m
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna
			JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs
			LEFT JOIN mahasiswa_kelas mk ON mk.id_mhs = m.id_mhs AND mk.is_aktif = 1
			LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = mk.id_nama_kelas
			JOIN pengambilan_mk pmk ON pmk.id_mhs = m.id_mhs
			WHERE 
				m.id_program_studi = {$id_program_studi} 
				AND pmk.id_kelas_mk = '{$id_kelas_mk}'
				AND pmk.status_apv_pengambilan_mk = 0
				{$where}");
			
		$smarty->assign('prodi', $prodi);
		$smarty->assign('thn_angkatan_set', $thn_angkatan_set);
		$smarty->assign('nama_kelas_set', $nama_kelas_set);
		$smarty->assign('mahasiswa_set', $mahasiswa_set);
	}
	
	if ($mode == 'preview')
	{
		// Redirect ke view jika GET
		if ($_SERVER['REQUEST_METHOD'] == 'GET') { echo '<script>top.window.location=\'#aktivitas-approvekrs!krs-approve.php\';</script>'; exit(); }
		
		$id_kelas_mk_set	= post('id_kelas_mk', array());
		$id_mhs_set			= post('id_mhs', array());
		
		$id_kelas_mk	= implode(',', $id_kelas_mk_set);
		$id_mhs			= implode(',', $id_mhs_set);
		
		$kelas_mk_set = $db->QueryToArray(
			"SELECT
				kmk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah,
				kur.kredit_semester, kur.tingkat_semester,
				nk.nama_kelas,
				nvl2(pjmk_gelar_depan,pjmk_gelar_depan||' ','')||initcap(nama_pjmk)||nvl2(pjmk_gelar_belakang,', '||pjmk_gelar_belakang,'') as nama_pjmk,
				
				/* Jumlah KRS */
				(SELECT count(pmk.id_mhs) FROM pengambilan_mk pmk
				WHERE pmk.id_kelas_mk = kmk.id_kelas_mk) as peserta,
				/* Jumlah KRS Approve */
				(SELECT count(pmk.id_mhs) FROM pengambilan_mk pmk
				WHERE pmk.status_apv_pengambilan_mk = 1 and pmk.id_kelas_mk = kmk.id_kelas_mk) as peserta_approve
			FROM krs_prodi krs
			JOIN kelas_mk kmk ON kmk.id_kelas_mk = krs.id_kelas_mk AND kmk.id_semester = krs.id_semester
			JOIN kurikulum_mk kur ON kur.id_kurikulum_mk = kmk.id_kurikulum_mk
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kur.id_mata_kuliah
			LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kmk.no_kelas_mk
			LEFT JOIN (
				SELECT pmk.id_kelas_mk, p.nm_pengguna as nama_pjmk, p.gelar_depan as pjmk_gelar_depan, p.gelar_belakang as pjmk_gelar_belakang
				FROM pengampu_mk pmk
				JOIN dosen d ON d.id_dosen = pmk.id_dosen
				JOIN pengguna p ON p.id_pengguna = d.id_pengguna
				WHERE pjmk_pengampu_mk = 1) pjmk on pjmk.id_kelas_mk = kmk.id_kelas_mk
			JOIN program_studi ps ON ps.id_program_studi = krs.id_program_studi
			JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
			WHERE kmk.id_kelas_mk IN ({$id_kelas_mk})");
			
		$mahasiswa_set = $db->QueryToArray(
			"SELECT m.id_mhs, m.nim_mhs, p.nm_pengguna, thn_angkatan_mhs, nk.nama_kelas
			FROM mahasiswa m
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna
			JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs
			LEFT JOIN mahasiswa_kelas mk ON mk.id_mhs = m.id_mhs AND mk.is_aktif = 1
			LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = mk.id_nama_kelas
			WHERE m.id_mhs IN ({$id_mhs})
			ORDER BY m.nim_mhs");
			
		// echo $id_mhs . '<br/>' . $id_kelas_mk; exit();
		
			
		$smarty->assign('kelas_mk_set', $kelas_mk_set);
		$smarty->assign('mahasiswa_set', $mahasiswa_set);
	}
	
	if ($mode == 'proses')
	{
		$id_kelas_mk_set	= post('id_kelas_mk', array());
		$id_mhs_set			= post('id_mhs', array());
		
		$id_kelas_mk		= implode(',', $id_kelas_mk_set);
		$id_mhs				= implode(',', $id_mhs_set);

		$result = $db->Query(
				"UPDATE PENGAMBILAN_MK SET STATUS_APV_PENGAMBILAN_MK = 1, UPDATED_ON = SYSDATE, UPDATED_BY = '{$id_pengguna}' WHERE ID_KELAS_MK IN ({$id_kelas_mk}) AND ID_MHS IN ({$id_mhs})");


		$jumlah_kelas	= count($id_kelas_mk_set);
		$jumlah_mhs		= count($id_mhs_set);
		
		$smarty->assign('jumlah_kelas', $jumlah_kelas);
		$smarty->assign('jumlah_mhs', $jumlah_mhs);
		$smarty->assign('result', $result);
	}
}

$smarty->display("approve-krs/{$mode}.tpl");