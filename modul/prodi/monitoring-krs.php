<?php
include '../../config.php';

$akademik = new Akademik($db);

$mode = empty($_GET['mode']) ? 'list' : $_GET['mode'];

if ($mode == 'list')
{
	$semester_aktif = $PT->get_semester_aktif();

	$id_semester = empty($_GET['id_semester']) ? $semester_aktif['ID_SEMESTER'] : (int)$_GET['id_semester'];

	$smarty->assign('semester_aktif', $semester_aktif);
	$smarty->assign('semester_set', $PT->list_semester());
	$smarty->assign('data_set', $akademik->list_mahasiswa_krs_per_prodi($user->ID_PROGRAM_STUDI, $id_semester));
}

if ($mode == 'detail')
{
	$id_mhs			= empty($_GET['id_mhs']) ? 0 : (int)$_GET['id_mhs'];
	$id_semester	= empty($_GET['id_semester']) ? 0 : (int)$_GET['id_semester'];

	$mahasiswa = new Mahasiswa($db, $id_mhs);

	$smarty->assign('data_set', $mahasiswa->list_krs_by_semester($id_semester));
}

$smarty->assign('mode', $mode);
$smarty->display('monitoring/krs.tpl');