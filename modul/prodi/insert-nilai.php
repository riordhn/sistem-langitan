<?php
// ====== FITUR PENGAKUAN NILAI INTERNAL ======
// ================== FIKRIE ==================
// =============== 07-10-2016 =================

require('common.php');
require_once ('ociFunction.php');
$kd_fak= $user->ID_FAKULTAS;
$kd_prodi= $user->ID_PROGRAM_STUDI;
$id_pengguna= $user->ID_PENGGUNA; 

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');

$id_pt = $id_pt_user;

$smtaktif=getvar("select id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = '{$id_pt}'");



if(($_POST['action'])=='proses'){

	$id_mhs=$_POST['id_mhs'];
	$id_kurikulum_mk=$_POST['id_kurikulum_mk'];
	$id_semester=$_POST['id_semester'];
	$nilai=strtoupper($_POST['nilai']);

	if(!empty($_POST['nilai'])){
		if($nilai!= 'A' and $nilai  != 'AB' and $nilai  != 'B' and $nilai  != 'BC' and $nilai  != 'C' and $nilai  != 'D' and $nilai  != 'E'){
			echo '<script>alert("Masukkan Nilai Huruf [A/AB/B/BC/C/D/E]")</script>';
		}else{
			InsertData("insert into pengambilan_mk (id_mhs, id_semester,id_kurikulum_mk,nilai_huruf,status_apv_pengambilan_mk,status_pengambilan_mk,flagnilai,keterangan,status_transfer)
					values ($id_mhs,$id_semester,$id_kurikulum_mk,'$nilai',1,1,'1','INSERT NILAI','0')");
		}
	}
	else{
		echo '<script>alert("Masukkan Nilai Huruf [A/AB/B/BC/C/D/E]")</script>';
	}
}

if (!empty($_GET['s']) or ($_POST['action'])=='proses') {

	$nim_mhs =$_GET['s'];
	
	$datamhs=getvar("SELECT
			mahasiswa.id_pengguna, MAHASISWA.id_mhs,MAHASISWA.id_program_studi,nm_pengguna,
			nm_jenjang||'-'||nm_program_studi as nm_program_studi,nim_mhs,jenjang.id_jenjang, thn_angkatan_mhs,mahasiswa.status as status_mhs, id_semester_masuk,
			nk.nama_kelas
		from mahasiswa
		join pengguna		on mahasiswa.id_pengguna=pengguna.id_pengguna and pengguna.id_perguruan_tinggi = {$id_pt}
		join program_studi	on mahasiswa.id_program_studi=program_studi.id_program_studi
		join jenjang		on program_studi.id_jenjang=jenjang.id_jenjang
		left join mahasiswa_kelas mk on mk.id_mhs = mahasiswa.id_mhs and mk.is_aktif = 1
		left join nama_kelas nk		 on nk.id_nama_kelas = mk.id_nama_kelas
		where nim_mhs = '".$nim_mhs."' and program_studi.id_program_studi={$kd_prodi}");

	$smarty->assign('datamhs', $datamhs);

	$kurikulum_set=getData("SELECT id_kurikulum,nm_kurikulum||' ('||thn_kurikulum||')' as nama from kurikulum
								where status_aktif = 1 and id_program_studi='$datamhs[ID_PROGRAM_STUDI]'");
						
	$smarty->assign('kurikulum_set', $kurikulum_set);

	$smt = getData(
		"SELECT id_semester, tahun_ajaran||' - '||nm_semester as nm_semester 
		from semester 
		where thn_akademik_semester in (
			select * from (
				select distinct thn_akademik_semester
				from semester where thn_akademik_semester BETWEEN '$datamhs[THN_ANGKATAN_MHS]' AND EXTRACT(YEAR FROM sysdate) order by thn_akademik_semester desc
			)
			
		) and nm_semester in ('Ganjil', 'Genap') and id_perguruan_tinggi = '{$id_pt}'
		order by thn_akademik_semester desc, nm_semester desc");
	$smarty->assign('semester_set', $smt);

	$id_kurikulum 	= (int)post('id_kurikulum', '');
	$smarty->assign('id_kurikulum_post', $id_kurikulum);

	if($id_kurikulum!=''){

		$matkul_set=getData("select kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.id_kurikulum_mk,kurikulum_mk.kredit_semester,thn_kurikulum as tahun, pmk.nilai_huruf
							 from kurikulum_mk
							join kurikulum on kurikulum.id_kurikulum = kurikulum_mk.id_kurikulum
							join mata_kuliah on mata_kuliah.id_mata_kuliah = kurikulum_mk.id_mata_kuliah
							left join pengambilan_mk pmk on pmk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk and pmk.id_mhs = '$datamhs[ID_MHS]'
							where kurikulum.id_kurikulum='{$id_kurikulum}' and pmk.id_kelas_mk is null and pmk.nilai_huruf is null
							order by nm_mata_kuliah asc");

		$smarty->assign('matkul_set', $matkul_set);

		$matkul_tampil=getData("select kd_mata_kuliah, nm_mata_kuliah, kurikulum_mk.id_kurikulum_mk,kurikulum_mk.kredit_semester,thn_kurikulum as tahun, pmk.nilai_huruf, s.tahun_ajaran||' - '||s.nm_semester as nm_semester
							 from kurikulum_mk 
							join kurikulum on kurikulum.id_kurikulum = kurikulum_mk.id_kurikulum
							join mata_kuliah on mata_kuliah.id_mata_kuliah = kurikulum_mk.id_mata_kuliah
							left join pengambilan_mk pmk on pmk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk and pmk.id_mhs = '$datamhs[ID_MHS]'
							left join semester s on s.id_semester = pmk.id_semester
							where kurikulum.id_kurikulum='{$id_kurikulum}' and pmk.id_kelas_mk is null 
								and pmk.status_transfer = 0 and pmk.nilai_huruf is not null
							order by s.id_semester, nm_mata_kuliah asc");

		$smarty->assign('matkul_tampil', $matkul_tampil);
	}
	else // jika belum dipilih, tampil nilai yg sudah terentri order by smeester, nama
	{
		$nilai_konversi = getData(
			"SELECT 
				pmk.id_pengambilan_mk, 
				mk.kd_mata_kuliah, mk.nm_mata_kuliah, 
				coalesce(kmk.kredit_semester, mk.kredit_semester) as kredit_semester, pmk.nilai_huruf,
				s.tahun_ajaran || ' - ' || s.nm_semester as nm_semester
			FROM pengambilan_mk pmk
			JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
			JOIN semester s ON s.id_semester = pmk.id_semester
			WHERE 
				pmk.status_transfer = 0 AND pmk.id_kelas_mk is null AND
				pmk.id_mhs = {$datamhs['ID_MHS']}
			ORDER BY s.thn_akademik_semester, nm_mata_kuliah");
		$smarty->assign('nilai_konversi', $nilai_konversi);
	}
						
}


$smarty->display('insert-nilai.tpl');

?>
