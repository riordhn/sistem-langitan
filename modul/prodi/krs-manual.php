<?php
include 'config.php';

$mode = get('mode', 'view');

$id_semester_aktif = $db->QuerySingle("select id_semester from semester where status_aktif_semester='True' and id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}");

if ($_SERVER['REQUEST_METHOD'] == 'GET' || $_SERVER['REQUEST_METHOD'] == 'POST')
{
	if ($mode == 'view')
	{
		$id_semester		= get('id_semester', 0);
		$id_semester		= (get('id_semester', 0) == 0) ? $id_semester_aktif : $id_semester;
		$thn_angkatan_mhs	= get('thn_angkatan_mhs', 'all');
		
		$semester_set = $db->QueryToArray(
			"select * from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}'
			order by thn_akademik_semester desc, nm_semester desc");

		$thn_angkatan_set = $db->QueryToArray(
			"SELECT DISTINCT thn_angkatan_mhs FROM mahasiswa m
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna AND p.id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}
			JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs
			WHERE sp.status_aktif = 1
			ORDER BY 1 DESC");
			
		$where_angkatan = ($thn_angkatan_mhs != 'all') ? 'AND m.thn_angkatan_mhs = '.$thn_angkatan_mhs : '';

		$prodi_set = $db->QueryToArray(
			"SELECT ps.id_program_studi, nm_jenjang||' '||nm_program_studi AS nm_program_studi, coalesce(jumlah_mhs,0) as jumlah_mhs, coalesce(jumlah_krs,0) as jumlah_krs
			FROM program_studi ps
			JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
			JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
			LEFT JOIN (
				/* Jumlah Mhs Aktif per Angkatan */
				SELECT ps.id_program_studi, COUNT(m.id_mhs) jumlah_mhs 
				FROM mahasiswa m
				JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
				JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs
				WHERE
					ps.id_program_studi = {$user->ID_PROGRAM_STUDI} AND
					sp.status_aktif = 1 
					{$where_angkatan}
				GROUP BY ps.id_program_studi
				) maba ON maba.id_program_studi = ps.id_program_studi
			LEFT JOIN (
				/* Jumlah Mhs Aktif per Angkatan yg KRS */
				SELECT m.id_program_studi, count(DISTINCT pmk.id_mhs) AS jumlah_krs 
				FROM pengambilan_mk pmk
				JOIN mahasiswa m ON m.id_mhs = pmk.id_mhs
				JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
				JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs
				WHERE
					ps.id_program_studi = {$user->ID_PROGRAM_STUDI} AND
					sp.status_aktif = 1 AND
					pmk.id_semester = {$id_semester}
					{$where_angkatan}
				GROUP BY m.id_program_studi
				) krs ON krs.id_program_studi = ps.id_program_studi
			WHERE 
				ps.id_program_studi = {$user->ID_PROGRAM_STUDI}
			ORDER BY 2 ASC");
		
		$smarty->assign('id_semester', $id_semester);
		$smarty->assign('semester_set', $semester_set);
		$smarty->assign('thn_angkatan_mhs', $thn_angkatan_mhs);
		$smarty->assign('thn_angkatan_set', $thn_angkatan_set);
		$smarty->assign('prodi_set', $prodi_set);
		
		
	}
	
	if ($mode == 'pilih-mk')
	{
		$id_program_studi	= (int)get('id_program_studi', 0);
		$id_semester		= (int)(get('id_semester', 0) == 0) ? $id_semester_aktif : get('id_semester', 0);
		$tingkat_semester	= get('tingkat_semester', 'all');
		$id_nama_kelas		= get('id_nama_kelas', 'all');
		$thn_angkatan_mhs	= get('thn_angkatan_mhs', 'all');
		
		$prodi_set = $db->QueryToArray(
			"SELECT ps.id_program_studi, nm_jenjang||' '||nm_program_studi AS nm_program_studi
			FROM program_studi ps
			JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
			JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
			WHERE ps.id_program_studi = {$user->ID_PROGRAM_STUDI}");
		
		$semester_set = $db->QueryToArray(
			"select * from semester where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}'
			order by thn_akademik_semester desc, nm_semester desc");
			
		$nama_kelas_set = $db->QueryToArray(
			"select * from nama_kelas where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' ORDER BY nama_kelas");

		$thn_angkatan_set = $db->QueryToArray(
			"SELECT DISTINCT thn_angkatan_mhs FROM mahasiswa m
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna AND p.id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}
			JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs
			WHERE sp.status_aktif = 1
			ORDER BY 1 DESC");
		
		$where = "";
		if ($tingkat_semester != 'all') $where .= "AND tingkat_semester = {$tingkat_semester} ";
		if ($id_nama_kelas != 'all') $where .= "AND nk.id_nama_kelas = {$id_nama_kelas} ";
			
		$kelas_mk_set = $db->QueryToArray(
			"SELECT
				kmk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah,
				kur.kredit_semester, kur.tingkat_semester,
				nk.nama_kelas,
				nvl2(pjmk_gelar_depan,pjmk_gelar_depan||' ','')||initcap(nama_pjmk)||nvl2(pjmk_gelar_belakang,', '||pjmk_gelar_belakang,'') as nama_pjmk,

				/* Jumlah KRS */
				(SELECT count(pmk.id_mhs) FROM pengambilan_mk pmk
				WHERE pmk.id_kelas_mk = kmk.id_kelas_mk) as peserta,
				
				/* Jumlah KRS Approve */
				(SELECT count(pmk.id_mhs) FROM pengambilan_mk pmk
				WHERE pmk.status_apv_pengambilan_mk = 1 and pmk.id_kelas_mk = kmk.id_kelas_mk) as peserta_approve
			FROM krs_prodi krs
			JOIN kelas_mk kmk ON kmk.id_kelas_mk = krs.id_kelas_mk AND kmk.id_semester = krs.id_semester
			JOIN kurikulum_mk kur ON kur.id_kurikulum_mk = kmk.id_kurikulum_mk
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kur.id_mata_kuliah
			LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kmk.no_kelas_mk
			LEFT JOIN (
				SELECT pmk.id_kelas_mk, p.nm_pengguna as nama_pjmk, p.gelar_depan as pjmk_gelar_depan, p.gelar_belakang as pjmk_gelar_belakang
				FROM pengampu_mk pmk
				JOIN dosen d ON d.id_dosen = pmk.id_dosen
				JOIN pengguna p ON p.id_pengguna = d.id_pengguna
				WHERE pjmk_pengampu_mk = 1) pjmk on pjmk.id_kelas_mk = kmk.id_kelas_mk
			JOIN program_studi ps ON ps.id_program_studi = krs.id_program_studi
			JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
			WHERE
				f.id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI} AND
				kmk.id_program_studi = {$id_program_studi} AND
				kmk.id_semester = {$id_semester} 
				{$where}");
		
		$smarty->assign('prodi_set', $prodi_set);
		$smarty->assign('id_semester', $id_semester);
		$smarty->assign('semester_set', $semester_set);
		$smarty->assign('nama_kelas_set', $nama_kelas_set);
		$smarty->assign('thn_angkatan_mhs', $thn_angkatan_mhs);
		$smarty->assign('thn_angkatan_set', $thn_angkatan_set);
		$smarty->assign('kelas_mk_set', $kelas_mk_set);
	}
	
	
	if ($mode == 'pilih-mhs')
	{
		// Redirect ke view jika GET
		if ($_SERVER['REQUEST_METHOD'] == 'GET') { echo '<script>top.window.location=\'#aktivitas-krsman!krs-manual.php\';</script>'; exit(); }
		
		$id_program_studi	= post('id_program_studi', 0);
		$thn_angkatan_mhs	= post('thn_angkatan_mhs', 'all');
		$id_nama_kelas		= post('id_nama_kelas', 'all');
		
		$db->Query(
			"SELECT ps.id_program_studi, nm_jenjang||' '||nm_program_studi AS nm_program_studi
			FROM program_studi ps
			JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
			WHERE ps.id_program_studi = {$id_program_studi}");
		$prodi = $db->FetchAssoc();
		
		$thn_angkatan_set = $db->QueryToArray(
			"SELECT DISTINCT thn_angkatan_mhs FROM mahasiswa m
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna AND p.id_perguruan_tinggi = {$user->ID_PERGURUAN_TINGGI}
			JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs
			WHERE sp.status_aktif = 1
			ORDER BY 1 DESC");
			
		$nama_kelas_set = $db->QueryToArray(
			"select * from nama_kelas where id_perguruan_tinggi = '{$user->ID_PERGURUAN_TINGGI}' ORDER BY nama_kelas");
			
		$where = "";
		if ($id_nama_kelas != 'all') $where .= "AND nk.id_nama_kelas = {$id_nama_kelas} ";
		if ($thn_angkatan_mhs != 'all') $where .= "AND m.thn_angkatan_mhs = {$thn_angkatan_mhs} ";
			
		$mahasiswa_set = $db->QueryToArray(
			"SELECT m.id_mhs, m.nim_mhs, p.nm_pengguna, thn_angkatan_mhs, nk.nama_kelas
			FROM mahasiswa m
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna
			JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs AND sp.status_aktif = 1
			LEFT JOIN mahasiswa_kelas mk ON mk.id_mhs = m.id_mhs AND mk.is_aktif = 1
			LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = mk.id_nama_kelas
			WHERE 
				m.id_program_studi = {$id_program_studi} 
				{$where}");
			
		$smarty->assign('prodi', $prodi);
		$smarty->assign('thn_angkatan_set', $thn_angkatan_set);
		$smarty->assign('nama_kelas_set', $nama_kelas_set);
		$smarty->assign('mahasiswa_set', $mahasiswa_set);
	}
	
	if ($mode == 'preview')
	{
		// Redirect ke view jika GET
		if ($_SERVER['REQUEST_METHOD'] == 'GET') { echo '<script>top.window.location=\'#aktivitas-krsman!krs-manual.php\';</script>'; exit(); }
		
		$id_kelas_mk_set	= post('id_kelas_mk', array());
		$id_mhs_set			= post('id_mhs', array());
		
		$id_kelas_mk	= implode(',', $id_kelas_mk_set);
		$id_mhs			= implode(',', $id_mhs_set);
		
		$kelas_mk_set = $db->QueryToArray(
			"SELECT
				kmk.id_kelas_mk, kd_mata_kuliah, nm_mata_kuliah,
				kur.kredit_semester, kur.tingkat_semester,
				nk.nama_kelas,
				nvl2(pjmk_gelar_depan,pjmk_gelar_depan||' ','')||initcap(nama_pjmk)||nvl2(pjmk_gelar_belakang,', '||pjmk_gelar_belakang,'') as nama_pjmk,
				
				/* Jumlah KRS */
				(SELECT count(pmk.id_mhs) FROM pengambilan_mk pmk
				WHERE pmk.id_kelas_mk = kmk.id_kelas_mk) as peserta,
				/* Jumlah KRS Approve */
				(SELECT count(pmk.id_mhs) FROM pengambilan_mk pmk
				WHERE pmk.status_apv_pengambilan_mk = 1 and pmk.id_kelas_mk = kmk.id_kelas_mk) as peserta_approve
			FROM krs_prodi krs
			JOIN kelas_mk kmk ON kmk.id_kelas_mk = krs.id_kelas_mk AND kmk.id_semester = krs.id_semester
			JOIN kurikulum_mk kur ON kur.id_kurikulum_mk = kmk.id_kurikulum_mk
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kur.id_mata_kuliah
			LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kmk.no_kelas_mk
			LEFT JOIN (
				SELECT pmk.id_kelas_mk, p.nm_pengguna as nama_pjmk, p.gelar_depan as pjmk_gelar_depan, p.gelar_belakang as pjmk_gelar_belakang
				FROM pengampu_mk pmk
				JOIN dosen d ON d.id_dosen = pmk.id_dosen
				JOIN pengguna p ON p.id_pengguna = d.id_pengguna
				WHERE pjmk_pengampu_mk = 1) pjmk on pjmk.id_kelas_mk = kmk.id_kelas_mk
			JOIN program_studi ps ON ps.id_program_studi = krs.id_program_studi
			JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
			WHERE kmk.id_kelas_mk IN ({$id_kelas_mk})");
			
		$mahasiswa_set = $db->QueryToArray(
			"SELECT m.id_mhs, m.nim_mhs, p.nm_pengguna, thn_angkatan_mhs, nk.nama_kelas
			FROM mahasiswa m
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna
			JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs AND sp.status_aktif = 1
			LEFT JOIN mahasiswa_kelas mk ON mk.id_mhs = m.id_mhs AND mk.is_aktif = 1
			LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = mk.id_nama_kelas
			WHERE m.id_mhs IN ({$id_mhs})
			ORDER BY m.nim_mhs");
			
		// echo $id_mhs . '<br/>' . $id_kelas_mk; exit();
		
		// Mengambil data mahasiswa yg sudah ambil mata kuliah yg sama
		$mahasiswa_sudah_krs_set = $db->QueryToArray(
			"SELECT m.id_mhs, m.nim_mhs, p.nm_pengguna, kls.id_kurikulum_mk, mk.kd_mata_kuliah, mk.nm_mata_kuliah, nk.nama_kelas,
				kls2.id_kurikulum_mk
			FROM pengambilan_mk pmk
			JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
			JOIN kurikulum_mk kur ON kur.id_kurikulum_mk = kls.id_kurikulum_mk
			JOIN mata_kuliah mk ON mk.id_mata_kuliah = kur.id_mata_kuliah
			JOIN mahasiswa m ON m.id_mhs = pmk.id_mhs
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna
			LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kls.no_kelas_mk
			JOIN (
				SELECT kls.id_kurikulum_mk, mk.nm_mata_kuliah, kls.id_semester FROM kelas_mk kls
				JOIN kurikulum_mk kur ON kur.id_kurikulum_mk = kls.id_kurikulum_mk
				JOIN mata_kuliah mk ON mk.id_mata_kuliah = kur.id_mata_kuliah
				WHERE id_kelas_mk IN ({$id_kelas_mk})
				) kls2 on kls2.id_kurikulum_mk = kls.id_kurikulum_mk AND kls2.id_semester = kls.id_semester
			WHERE 
				pmk.id_mhs IN ({$id_mhs})");
			
		$smarty->assign('kelas_mk_set', $kelas_mk_set);
		$smarty->assign('mahasiswa_set', $mahasiswa_set);
		$smarty->assign('mahasiswa_sudah_krs_set', $mahasiswa_sudah_krs_set);
	}
	
	if ($mode == 'proses')
	{
		$id_kelas_mk_set	= post('id_kelas_mk', array());
		$id_mhs_set			= post('id_mhs', array());
		
		$id_kelas_mk		= implode(',', $id_kelas_mk_set);
		$id_mhs				= implode(',', $id_mhs_set);

		$approve = post('approve', 0);

		$is_boleh = 0;

		// Checking PERTAMA
		$db->Query("SELECT SUM(mk.kredit_semester) AS JML_SKS, kmk.id_semester
						FROM kelas_mk kmk
						JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
						WHERE kmk.id_kelas_mk IN ({$id_kelas_mk})");
		$kelas_mk = $db->FetchAssoc();

		$jml_sks = $kelas_mk['JML_SKS'];

		$id_smt = $kelas_mk['ID_SEMESTER'];

		if($jml_sks > 24) {
			$is_boleh = $is_boleh + 1;
		}

		// Checking Kedua >> JUMLAH KRS By FIKRIE (31-01-2020)
		foreach ($id_mhs_set as $id_mhs) {

			$db->Query("SELECT SUM(COALESCE(mk1.kredit_semester, mk2.kredit_semester)) AS JML_KRS
	                        FROM pengambilan_mk
	                        JOIN mahasiswa ON mahasiswa.id_mhs = pengambilan_mk.id_mhs
	                        -- Via Kelas
	                        LEFT JOIN kelas_mk      ON pengambilan_mk.id_kelas_mk = kelas_mk.id_kelas_mk
	                        LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kelas_mk.id_mata_kuliah
	                        LEFT JOIN nama_kelas    ON kelas_mk.no_kelas_mk = nama_kelas.id_nama_kelas
	                        -- Via Kurikulum
	                        LEFT JOIN kurikulum_mk  ON pengambilan_mk.id_kurikulum_mk = kurikulum_mk.id_kurikulum_mk
	                        LEFT JOIN mata_kuliah mk2 ON kurikulum_mk.id_mata_kuliah = mk2.id_mata_kuliah
	                        WHERE pengambilan_mk.id_semester = '{$id_smt}' AND
	                            -- status_apv_pengambilan_mk = 1 AND 
	                            pengambilan_mk.status_hapus = 0 AND 
	                            pengambilan_mk.status_pengambilan_mk != 0 AND
	                            mahasiswa.id_mhs = '{$id_mhs}'");
	        $krs = $db->FetchAssoc();

	        $jml_krs_smt = 0;

	        if (! empty($krs['JML_KRS'])) {
	        	$jml_krs_smt = $krs['JML_KRS'];
	        }

	        $jml_krs = $jml_krs_smt + $jml_sks;

	        if ($jml_krs > 24) {
	        	$is_boleh = $is_boleh + 1;
	        }

	        if ($is_boleh > 0) {
				$gagal = "Gagal";
				$smarty->assign('gagal', $gagal);
			}
			else {
				// Catatan :
				// - Status langsung teraprove = 1 (tergantung pilihan di sistem)
				// - Jenis pengambilan KRS = 1
				// - Status hitung nilai (flagnilai) = 0

				if($approve == 0){

					$result = $db->Query(
						"INSERT INTO pengambilan_mk (
							id_kelas_mk, id_mhs, id_semester, id_kurikulum_mk, status_apv_pengambilan_mk, status_pengambilan_mk, flagnilai, created_on, created_by)
						SELECT 
							kmk.id_kelas_mk, m.id_mhs, kmk.id_semester, kmk.id_kurikulum_mk,
							0 as status_apv_pengambilan_mk, 1 as status_pengambilan_mk, 0 as flagnilai,
							sysdate, {$user->ID_PENGGUNA}
						FROM kelas_mk kmk
						CROSS JOIN mahasiswa m
						WHERE 
							kmk.id_kelas_mk IN ({$id_kelas_mk}) AND 
							m.id_mhs = {$id_mhs}");
				}
				else{

					$result = $db->Query(
						"INSERT INTO pengambilan_mk (
							id_kelas_mk, id_mhs, id_semester, id_kurikulum_mk, status_apv_pengambilan_mk, status_pengambilan_mk, flagnilai, created_on, created_by)
						SELECT 
							kmk.id_kelas_mk, m.id_mhs, kmk.id_semester, kmk.id_kurikulum_mk,
							1 as status_apv_pengambilan_mk, 1 as status_pengambilan_mk, 0 as flagnilai,
							sysdate, {$user->ID_PENGGUNA}
						FROM kelas_mk kmk
						CROSS JOIN mahasiswa m
						WHERE 
							kmk.id_kelas_mk IN ({$id_kelas_mk}) AND 
							m.id_mhs = {$id_mhs}");
				}
				
				$jumlah_kelas	= count($id_kelas_mk_set);
				$jumlah_mhs		= count($id_mhs_set);
				
				$smarty->assign('jumlah_kelas', $jumlah_kelas);
				$smarty->assign('jumlah_mhs', $jumlah_mhs);
				$smarty->assign('result', $result);
				$smarty->assign('approve', $approve);
			}
		}
	}
}

$smarty->display("krs-manual/{$mode}.tpl");