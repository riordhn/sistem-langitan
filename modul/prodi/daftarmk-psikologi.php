<?php
/*
YAH 06/06/2011
*/
error_reporting (E_ALL & ~E_NOTICE);
require('common.php');
require_once ('ociFunction.php');

$smarty->assign('disp1','block');
$smarty->assign('disp2','none');
$smarty->assign('disp3','none');

$kdprodi = $user->ID_PROGRAM_STUDI;
$id_thkur=getvar("select id_kurikulum_prodi from kurikulum_prodi
where id_program_studi=$kdprodi and status_aktif=1");

$smarty->assign('IDKURPRODI',$id_thkur['ID_KURIKULUM_PRODI']);

$datakur=getData("select id_kurikulum_prodi,nm_kurikulum||'-'||tahun_kurikulum as nama from kurikulum_prodi
left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
where kurikulum_prodi.id_program_studi=$kdprodi");
$smarty->assign('T_KUR', $datakur);

//Yudi Sulistya, 23/08/2012 (seting mk agama)
$dataagama=getData("select id_agama,nm_agama from agama where id_agama!=6 order by id_agama");
$smarty->assign('T_AGM', $dataagama);

$status = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'view';
//echo $status;

switch($status) {
	 case 'view':
		$thkur = isSet($_POST['thkur']) ? $_POST['thkur'] :$id_thkur['ID_KURIKULUM_PRODI'];
        break;

	case 'del':
		$idmk = $_GET['id_mk'];
		deleteData("delete from kurikulum_mk where id_kurikulum_mk='$idmk'");
        break;

	case 'update1':
		$id_matkul = $_POST['id_mk'];
		$sks_total = $_POST['kredit_matkul'];
		$sks_tatap_muka = $_POST['kredit_tm'];
		$sks_praktikum = $_POST['kredit_prk'];
		$nama_en = $_POST['en'];
		$kelompok_mk = $_POST['kelompok_mk'];
		$smt = $_POST['tingkat_smt'];
		$thkur = $_POST['thkur'];
		$mkta = $_POST['status_mkta'];
		$agama = $_POST['status_mk_agama'];
		$paket = $_POST['status_paket'];

		$mk = getvar("select id_mata_kuliah from kurikulum_mk where id_kurikulum_mk=$id_matkul");
		$id_mata_kuliah = $mk['ID_MATA_KULIAH'];
		/*
		$replace = array(",");
		$replaceto = array(".");
		$sks_total = str_replace($replace, $replaceto, $kredit_matkul);
		$sks_tatap_muka = str_replace($replace, $replaceto, $kredit_tatap_muka);
		$sks_praktikum = str_replace($replace, $replaceto, $kredit_praktikum);
		*/
		UpdateData("update kurikulum_mk set kredit_semester='$sks_total', id_kelompok_mk='$kelompok_mk',
					id_program_studi='$kdprodi', tingkat_semester='$smt', status_mkta='$mkta', status_mk_agama='$agama',
					id_kurikulum_prodi='$thkur', kredit_tatap_muka='$sks_tatap_muka', kredit_praktikum='$sks_praktikum',
					status_paket='".$paket."'
					where id_kurikulum_mk=$id_matkul");
					
		UpdateData("update mata_kuliah set nm_mata_kuliah_en=upper('".$nama_en."') where id_mata_kuliah=$id_mata_kuliah");

        break;

	case 'add':
		$id_matkul = $_GET['id_mk'];
		$tkur=$_GET['tkur'];
		tambahdata("kurikulum_mk","id_mata_kuliah,id_program_studi,id_kurikulum_prodi","$id_matkul,$kdprodi,$tkur");
        break;

	case 'viewup':
		$thkur = $_POST['thkur'];
		$idmk = $_GET['id_mk'];
		if (trim($idmk) != '') {

		$smarty->assign('idmk',$idmk);
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','block');
		$smarty->assign('disp3','none');}

		$datamp=getData("select * from prodi_minat where id_program_studi=$kdprodi");
		$smarty->assign('MP_PRODI', $datamp);

		$datakelmk=getData("select * from kelompok_mk");
		$smarty->assign('KEL_MK', $datakelmk);

$datakur=getData("select id_kurikulum_prodi,nm_kurikulum||'-'||tahun_kurikulum as nama  from kurikulum_prodi
left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
where kurikulum_prodi.id_program_studi=$kdprodi");
$smarty->assign('T_KUR1', $datakur);

$mk=getData("select kmk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,nm_mata_kuliah_en,
kmk.kredit_tatap_muka,kmk.kredit_praktikum,kmk.kredit_semester, kmk.status_mkta,nm_kelompok_mk,
case when kmk.status_mkta=1 then 'Skripsi/TA/Thesis' when kmk.status_mkta=2 then 'KKN' when kmk.status_mkta=3 then 'Magang/PKL' when kmk.status_mkta=4 then 'Keasistenan'
when kmk.status_mkta=5 then 'Modul/Skill/Micro' when kmk.status_mkta=6 then 'Praktikum' else 'Tidak' end as mkta,
nm_prodi_minat,nm_kurikulum||'-'||kurikulum_prodi.tahun_kurikulum as nama, kurikulum_prodi.tahun_kurikulum,
tingkat_semester,kmk.id_status_mk, kmk.id_kelompok_mk,kmk.id_kurikulum_prodi, kmk.status_mk_agama, kmk.status_paket
from kurikulum_mk kmk left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join status_mk on kmk.id_status_mk=status_mk.id_status_mk
left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk
left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
left join kurikulum on kurikulum_prodi.id_kurikulum=kurikulum.id_kurikulum
left join prodi_minat on kmk.id_prodi_minat=prodi_minat.id_prodi_minat and prodi_minat.id_program_studi=$kdprodi
where kmk.id_program_studi=$kdprodi and id_kurikulum_mk=$idmk");
$smarty->assign('T_MK1', $mk);

	break;

	case 'insertmk':
		$nama_mk = $_POST['nama_mk'];

		// Added by Yudi Sulistya on Dec 02, 2011
		$upper = strtoupper($nama_mk);
		$lower = strtolower($nama_mk);
		$proper = ucwords($nama_mk);

		$idkurprodi = $_POST['thkur1'];
		$smarty->assign('ID_KP', $idkurprodi);
		$smarty->assign('disp1','none');
		$smarty->assign('disp2','none');
		$smarty->assign('disp3','block');

		$mk=getData("SELECT id_mata_kuliah,kd_mata_kuliah,nm_mata_kuliah,nm_mata_kuliah_en FROM mata_kuliah WHERE nm_mata_kuliah LIKE '%$upper%' OR nm_mata_kuliah LIKE '%$lower%' OR nm_mata_kuliah LIKE '%$proper%'");
		$smarty->assign('INSERT_MK', $mk);

	break;

}
$thkur = isSet($_POST['thkur']) ? $_POST['thkur'] :$id_thkur['ID_KURIKULUM_PRODI'];
$mk=getData("select kmk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_tatap_muka,
kredit_praktikum,kmk.kredit_semester,nm_status_mk,nm_kelompok_mk,kurikulum_prodi.tahun_kurikulum,
tingkat_semester,case when status_paket='Ya' then 'YA' else '' end as status_paket from kurikulum_mk kmk
left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
left join status_mk on kmk.id_status_mk=status_mk.id_status_mk
left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk
left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
where kmk.id_program_studi=$kdprodi and kmk.id_kurikulum_prodi=$thkur");
$smarty->assign('T_MK', $mk);
$smarty->display('daftarmk-psikologi.tpl');

?>
