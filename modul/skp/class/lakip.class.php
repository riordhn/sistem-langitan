<?php

class lakip {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    // LAKIP tujuan

    function load_lakip_tujuan() {
        return $this->db->QueryToArray("SELECT * FROM LAKIP_TUJUAN_RENSTRA ORDER BY TUJUAN_RENSTRA");
    }

    function get_lakip_tujuan($id) {
        $this->db->Query("SELECT * FROM LAKIP_TUJUAN_RENSTRA WHERE ID_TUJUAN_RENSTRA='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_lakip_tujuan($kode, $tujuan, $id_pengguna, $keterangan) {
        $this->db->Query("
            INSERT INTO LAKIP_TUJUAN_RENSTRA 
                (KODE_TUJUAN_RENSTRA,TUJUAN_RENSTRA,ID_PENGGUNA, KETERANGAN) 
            VALUES 
            ('{$kode}','{$tujuan}','{$id_pengguna}', {$keterangan}')");
    }

    function update_lakip_tujuan($id, $kode, $tujuan, $id_pengguna, $keterangan) {
        $this->db->Query("
            UPDATE LAKIP_TUJUAN_RENSTRA
            SET 
                KODE_TUJUAN_RENSTRA='{$kode}',
                TUJUAN_RENSTRA='{$tujuan}',
                ID_PENGGUNA = '{$id_pengguna}',
                KETERANGAN='{$keterangan}'
            WHERE ID_TUJUAN_RENSTRA='{$id}'");
    }

    function delete_lakip_tujuan($id) {
        $this->db->Query("DELETE FROM LAKIP_TUJUAN_RENSTRA WHERE ID_TUJUAN_RENSTRA='{$id}'");
    }

    // LAKIP Program

    function load_lakip_program() {
        return $this->db->QueryToArray("
            SELECT LPU.*,LJ.TUJUAN_RENSTRA 
            FROM LAKIP_PROGRAM_UTAMA LPU
            JOIN LAKIP_TUJUAN_RENSTRA LJ ON LPU.ID_TUJUAN_RENSTRA=LJ.ID_TUJUAN_RENSTRA
            ORDER BY LJ.TUJUAN_RENSTRA,LPU.PROGRAM_UTAMA");
    }

    function get_lakip_program($id) {
        $this->db->Query("
            SELECT LPU.*,LJ.TUJUAN_RENSTRA 
            FROM LAKIP_PROGRAM_UTAMA LPU
            JOIN LAKIP_TUJUAN_RENSTRA LJ ON LPU.ID_TUJUAN_RENSTRA=LJ.ID_TUJUAN_RENSTRA
            WHERE LPU.ID_PROGRAM_UTAMA='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_lakip_program($kode, $tujuan, $program, $id_pengguna, $keterangan) {
        $this->db->Query("
            INSERT INTO LAKIP_PROGRAM_UTAMA 
                (ID_TUJUAN_RENSTRA,KODE_PROGRAM_UTAMA,PROGRAM_UTAMA, ID_PENGGUNA, KETERANGAN) 
            VALUES 
                ('{$tujuan}','{$kode}','{$program}', '{$id_pengguna}', '{$keterangan}')");
    }

    function update_lakip_program($id, $kode, $tujuan, $program, $id_pengguna, $keterangan) {
        $this->db->Query("
            UPDATE LAKIP_PROGRAM_UTAMA 
            SET 
                ID_TUJUAN_RENSTRA='{$tujuan}',
                KODE_PROGRAM_UTAMA='{$kode}',
                PROGRAM_UTAMA='{$program}',
                ID_PENGGUNA='{$id_pengguna}',
                KETERANGAN='{$keterangan}'
            WHERE ID_PROGRAM_UTAMA='{$id}'");
    }

    function delete_lakip_program($id) {
        $this->db->Query("DELETE FROM LAKIP_PROGRAM_UTAMA WHERE ID_PROGRAM_UTAMA='{$id}'");
    }

    // Lakip Periode

    function load_lakip_periode() {
        return $this->db->QueryToArray("SELECT * FROM LAKIP_PERIODE ORDER BY AWAL_PERIODE");
    }

    function get_lakip_periode($id) {
        $this->db->Query("SELECT * FROM LAKIP_PERIODE WHERE ID_PERIODE='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_lakip_periode($nama, $awal, $akhir) {
        $this->db->Query("
            INSERT INTO LAKIP_PERIODE
                (NAMA_PERIODE,AWAL_PERIODE,AKHIR_PERIODE)
            VALUES
                ('{$nama}','{$awal}','{$akhir}')");
    }

    function update_lakip_periode($id, $nama, $awal, $akhir) {
        $this->db->Query("
            UPDATE LAKIP_PERIODE
            SET
                NAMA_PERIODE='{$nama}',
                AWAL_PERIODE='{$awal}',
                AKHIR_PERIODE='{$akhir}'
            WHERE ID_PERIODE='{$id}'");
    }

    function delete_lakip_periode($id) {
        $this->db->Query("DELETE FROM LAKIP_PERIODE WHERE ID_PERIODE='{$id}'");
    }

    // Lakip Program Kerja

    function load_lakip_program_kerja() {
        return $this->db->QueryToArray("
            SELECT LPK.*,UK.NM_UNIT_KERJA,LP.NAMA_PERIODE,LP.AWAL_PERIODE,LP.AKHIR_PERIODE,LPU.PROGRAM_UTAMA,LPR.TUJUAN_RENSTRA
            FROM LAKIP_PROGRAM_KERJA LPK
            JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=LPK.ID_UNIT_KERJA
            JOIN LAKIP_PERIODE LP ON LP.ID_PERIODE=LPK.ID_PERIODE
            JOIN LAKIP_PROGRAM_UTAMA LPU ON LPU.ID_PROGRAM_UTAMA=LPK.ID_PROGRAM_UTAMA
            JOIN LAKIP_TUJUAN_RENSTRA LPR ON LPR.ID_TUJUAN_RENSTRA=LPU.ID_TUJUAN_RENSTRA
            ORDER BY LPK.ID_UNIT_KERJA,LPU.PROGRAM_UTAMA,LPK.PROGRAM_KERJA");
    }

    function get_lakip_program_kerja($id) {
        $this->db->Query("
            SELECT LPK.*,UK.NM_UNIT_KERJA,LP.NAMA_PERIODE,LP.AWAL_PERIODE,LP.AKHIR_PERIODE,LPU.PROGRAM_UTAMA,LPR.TUJUAN_RENSTRA
            FROM LAKIP_PROGRAM_KERJA LPK
            JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=LPK.ID_UNIT_KERJA
            JOIN LAKIP_PERIODE LP ON LP.ID_PERIODE=LPK.ID_PERIODE
            JOIN LAKIP_PROGRAM_UTAMA LPU ON LPU.ID_PROGRAM_UTAMA=LPK.ID_PROGRAM_UTAMA
            JOIN LAKIP_TUJUAN_RENSTRA LPR ON LPR.ID_TUJUAN_RENSTRA=LPU.ID_TUJUAN_RENSTRA
            WHERE LPK.ID_PROGRAM_KERJA='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_lakip_program_kerja($unit_kerja, $periode, $program_utama, $kode, $program, $id_pengguna, $keterangan) {
        $this->db->Query("
            INSERT INTO LAKIP_PROGRAM_KERJA LPK
                (ID_UNIT_KERJA,ID_PERIODE,ID_PROGRAM_UTAMA,KODE_PROGRAM_KERJA,PROGRAM_KERJA, ID_PENGGUNA, KETERANGAN)
            VALUES
                ('{$unit_kerja}','{$periode}','{$program_utama}','{$kode}','{$program}', '{$id_pengguna}', '{$keterangan}')");
    }

    function update_lakip_program_kerja($id, $unit_kerja, $periode, $program_utama, $kode, $program, $id_pengguna, $keterangan) {
        $this->db->Query("
            UPDATE LAKIP_PROGRAM_KERJA
            SET
                ID_UNIT_KERJA='{$unit_kerja}',
                ID_PERIODE='{$periode}',
                ID_PROGRAM_UTAMA='{$program_utama}',
                KODE_PROGRAM_KERJA='{$kode}',
                PROGRAM_KERJA='{$program}',
                ID_PENGGUNA='{$id_pengguna}',
                KETERANGAN='{$keterangan}'
            WHERE ID_PROGRAM_KERJA='{$id}'");
    }

    function delete_lakip_program_kerja($id) {
        $this->db->Query("DELETE LAKIP_PROGRAM_KERJA WHERE ID_PROGRAM_KERJA='{$id}'");
    }

    // Lakip Indikator
    function load_lakip_indikator() {
        return $this->db->QueryToArray("
            SELECT LPK.PROGRAM_KERJA,LP.NAMA_PERIODE,LIK.* 
            FROM LAKIP_INDIKATOR_KINERJA LIK
            JOIN LAKIP_PROGRAM_KERJA LPK ON LPK.ID_PROGRAM_KERJA=LIK.ID_PROGRAM_KERJA
            JOIN LAKIP_PERIODE LP ON LP.ID_PERIODE=LPK.ID_PERIODE
            JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=LPK.ID_UNIT_KERJA
            ORDER BY LP.NAMA_PERIODE,LPK.PROGRAM_KERJA,LIK.NAMA_INDIKATOR_KINERJA");
    }

    function get_lakip_indikator($id) {
        $this->db->Query("
            SELECT LPK.PROGRAM_KERJA,LIK.* 
            FROM LAKIP_INDIKATOR_KINERJA LIK
            JOIN LAKIP_PROGRAM_KERJA LPK ON LPK.ID_PROGRAM_KERJA=LIK.ID_PROGRAM_KERJA
            JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=LPK.ID_UNIT_KERJA
            WHERE LIK.ID_INDIKATOR_KINERJA='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_lakip_indikator($program_kerja, $kode, $nama, $target) {
        $this->db->Query("
            INSERT INTO LAKIP_INDIKATOR_KINERJA
                (ID_PROGRAM_KERJA,KODE_INDIKATOR_KINERJA,NAMA_INDIKATOR_KINERJA,TARGET)
            VALUES
                ('{$program_kerja}','{$kode}','{$nama}','{$target}')");
    }

    function update_lakip_indikator($id, $program_kerja, $kode, $nama, $target) {
        $this->db->Query("
            UPDATE LAKIP_INDIKATOR_KINERJA
            SET
                ID_PROGRAM_KERJA='{$program_kerja}',
                KODE_INDIKATOR_KINERJA='{$kode}',
                NAMA_INDIKATOR_KINERJA='{$nama}',
                TARGET='{$target}'
            WHERE ID_INDIKATOR_KINERJA='{$id}'");
    }

    function delete_lakip_indikator($id) {
        $this->db->Query("DELETE LAKIP_INDIKATOR_KINERJA WHERE ID_INDIKATOR_KINERJA='{$id}'");
    }

    // Usulan Lakip

    function load_usulan_lakip() {
        return $this->db->QueryToArray("
            SELECT UK.NM_UNIT_KERJA,LPK.PROGRAM_KERJA,LPU.PROGRAM_UTAMA,LT.TUJUAN_RENSTRA,LA.*
            FROM LAKIP_ANGGARAN LA
            LEFT JOIN LAKIP_PROGRAM_KERJA LPK ON LPK.ID_PROGRAM_KERJA=LA.ID_PROGRAM_KERJA
            LEFT JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=LPK.ID_UNIT_KERJA
            LEFT JOIN LAKIP_PROGRAM_UTAMA LPU ON LPU.ID_PROGRAM_UTAMA=LPK.ID_PROGRAM_UTAMA
            LEFT JOIN LAKIP_TUJUAN_RENSTRA LT ON LT.ID_TUJUAN_RENSTRA=LPU.ID_TUJUAN_RENSTRA");
    }

    function get_usulan_lakip($id) {
        $this->db->Query("
            SELECT UK.NM_UNIT_KERJA,LPK.PROGRAM_KERJA,LPU.PROGRAM_UTAMA,LT.TUJUAN_RENSTRA,LA.*
            FROM LAKIP_ANGGARAN LA
            LEFT JOIN LAKIP_PROGRAM_KERJA LPK ON LPK.ID_PROGRAM_KERJA=LA.ID_PROGRAM_KERJA
            LEFT JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=LPK.ID_UNIT_KERJA
            LEFT JOIN LAKIP_PROGRAM_UTAMA LPU ON LPU.ID_PROGRAM_UTAMA=LPK.ID_PROGRAM_UTAMA
            LEFT JOIN LAKIP_TUJUAN_RENSTRA LT ON LT.ID_TUJUAN_RENSTRA=LPU.ID_TUJUAN_RENSTRA
            WHERE LA.ID_ANGGARAN='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_usulan_lakip($program_kerja, $usulan_pagu, $keterangan) {
        $this->db->Query("
            INSERT INTO LAKIP_ANGGARAN
                (ID_PROGRAM_KERJA,USULAN_PAGU,KETERANGAN)
            VALUES
                ('{$program_kerja}','{$usulan_pagu}','{$keterangan}')");
    }

    function update_usulan_lakip($id, $program_kerja, $usulan_pagu, $keterangan) {
        $this->db->Query("
            UPDATE LAKIP_ANGGARAN
            SET
                ID_PROGRAM_KERJA='{$program_kerja}',
                USULAN_PAGU='{$usulan_pagu}',
                KETERANGAN='{$keterangan}'
            WHERE ID_ANGGARAN='{$id}'");
    }

    function delete_usulan_lakip($id) {
        $this->db->Query("DELETE FROM LAKIP_ANGGARAN WHERE ID_ANGGARAN='{$id}'");
    }

    //Laporan LAKIP

    function load_laporan_lakip($unit_kerja) {
        $data_hasil = array();
        if ($unit_kerja != '') {
            $data_usulan = $this->db->QueryToArray("
            SELECT UK.NM_UNIT_KERJA,LPK.PROGRAM_KERJA,LPU.PROGRAM_UTAMA,LT.TUJUAN_RENSTRA,LA.STATUS STATUS_ANGGARAN
                ,LA.USULAN_PAGU,LA.REVISI_PAGU,LA.PAGU,LA.SERAPAN,LA.ID_PROGRAM_KERJA,LA.ID_ANGGARAN,LA.KETERANGAN,
                LPK.STATUS STATUS_PROKER
            FROM LAKIP_ANGGARAN LA
            LEFT JOIN LAKIP_PROGRAM_KERJA LPK ON LPK.ID_PROGRAM_KERJA=LA.ID_PROGRAM_KERJA
            LEFT JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=LPK.ID_UNIT_KERJA
            LEFT JOIN LAKIP_PROGRAM_UTAMA LPU ON LPU.ID_PROGRAM_UTAMA=LPK.ID_PROGRAM_UTAMA
            LEFT JOIN LAKIP_TUJUAN_RENSTRA LT ON LT.ID_TUJUAN_RENSTRA=LPU.ID_TUJUAN_RENSTRA
            WHERE UK.ID_UNIT_KERJA='{$unit_kerja}'");
            foreach ($data_usulan as $d) {
                $data_indikator = $this->db->QueryToArray("
                SELECT LI.*,LA.KETERANGAN_ANALISA 
                FROM LAKIP_INDIKATOR_KINERJA LI
                LEFT JOIN LAKIP_ANALISA LA ON LA.ID_INDIKATOR_KINERJA=LI.ID_INDIKATOR_KINERJA
                WHERE LI.ID_PROGRAM_KERJA='{$d['ID_PROGRAM_KERJA']}'");
                array_push($data_hasil, array_merge($d, array('DATA_INDIKATOR' => $data_indikator)));
            }
        } else {
            $data_usulan = $this->db->QueryToArray("
            SELECT UK.NM_UNIT_KERJA,LPK.PROGRAM_KERJA,LPU.PROGRAM_UTAMA,LT.TUJUAN_RENSTRA,LA.STATUS STATUS_ANGGARAN
                ,LA.USULAN_PAGU,LA.REVISI_PAGU,LA.PAGU,LA.SERAPAN,LA.ID_PROGRAM_KERJA,LA.ID_ANGGARAN,LA.KETERANGAN,
                LPK.STATUS STATUS_PROKER
            FROM LAKIP_ANGGARAN LA
            LEFT JOIN LAKIP_PROGRAM_KERJA LPK ON LPK.ID_PROGRAM_KERJA=LA.ID_PROGRAM_KERJA
            LEFT JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=LPK.ID_UNIT_KERJA
            LEFT JOIN LAKIP_PROGRAM_UTAMA LPU ON LPU.ID_PROGRAM_UTAMA=LPK.ID_PROGRAM_UTAMA
            LEFT JOIN LAKIP_TUJUAN_RENSTRA LT ON LT.ID_TUJUAN_RENSTRA=LPU.ID_TUJUAN_RENSTRA");
            foreach ($data_usulan as $d) {
                $data_indikator = $this->db->QueryToArray("
                SELECT LI.*,LA.KETERANGAN_ANALISA 
                FROM LAKIP_INDIKATOR_KINERJA LI
                LEFT JOIN LAKIP_ANALISA LA ON LA.ID_INDIKATOR_KINERJA=LI.ID_INDIKATOR_KINERJA
                WHERE LI.ID_PROGRAM_KERJA='{$d['ID_PROGRAM_KERJA']}'");
                array_push($data_hasil, array_merge($d, array('DATA_INDIKATOR' => $data_indikator)));
            }
        }
        return $data_hasil;
    }

    // Lakip Revisi
    function revisi_usulan_lakip($id, $revisi, $pagu, $serapan, $keterangan) {
        $this->db->Query("
            UPDATE LAKIP_ANGGARAN
            SET
                REVISI_PAGU='{$revisi}',
                PAGU='{$pagu}',
                SERAPAN='{$serapan}',
                KETERANGAN='{$keterangan}'
            WHERE ID_ANGGARAN='{$id}'");
    }

    //LAKIP Capaian

    function load_lakip_capaian_indikator($id) {
        return $this->db->QueryToArray("
            SELECT LPK.PROGRAM_KERJA,LP.NAMA_PERIODE,LIK.*,UK.NM_UNIT_KERJA,LA.KETERANGAN_ANALISA
            FROM LAKIP_INDIKATOR_KINERJA LIK
            JOIN LAKIP_PROGRAM_KERJA LPK ON LPK.ID_PROGRAM_KERJA=LIK.ID_PROGRAM_KERJA
            JOIN LAKIP_PERIODE LP ON LP.ID_PERIODE=LPK.ID_PERIODE
            JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=LPK.ID_UNIT_KERJA
            LEFT JOIN LAKIP_ANALISA LA ON LA.ID_INDIKATOR_KINERJA=LIK.ID_INDIKATOR_KINERJA
            WHERE LPK.ID_PROGRAM_KERJA='{$id}'
            ORDER BY LP.NAMA_PERIODE,LPK.PROGRAM_KERJA,LIK.NAMA_INDIKATOR_KINERJA");
    }
    
    //
    function load_list_unit_kerja() {
        return $this->db->QueryToArray("SELECT ID_UNIT_KERJA, NM_UNIT_KERJA FROM UNIT_KERJA ORDER BY TYPE_UNIT_KERJA");
    }
    
    function load_list_unit_kerja_pengguna($id) {
        return $this->db->QueryToArray("SELECT ID_UNIT_KERJA, NM_UNIT_KERJA FROM UNIT_KERJA WHERE ID_UNIT_KERJA IN (SELECT ID_UNIT_KERJA FROM SEJARAH_JABATAN_STRUKTURAL WHERE ID_PENGGUNA = '{$id}')");
    }
    
    function load_sasaran_kerja_unit($id_unit_kerja, $id_periode){
        return $this->db->QueryToArray("SELECT * FROM LAKIP_PROGRAM_KERJA WHERE ID_UNIT_KERJA='$id_unit_kerja' AND ID_PERIODE='$id_periode'");
    }

}

?>
