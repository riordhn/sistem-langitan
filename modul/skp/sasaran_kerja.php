<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include 'config.php';

$SejarahJabatanStruktural = new SejarahJabatanStruktural($db);

if($SejarahJabatanStruktural->isPimpinan($user->ID_PENGGUNA)){
    header("Location: rencana_strategis.php");
}
else if($SejarahJabatanStruktural->isKepalaSubDit($user->ID_PENGGUNA)){
    header("Location: skp_sub_unit.php?admin");
}
else if($SejarahJabatanStruktural->isKepalaSeksi($user->ID_PENGGUNA)){
    header("Location: skp_seksi_sub_unit.php?admin");
}
else {
    header("Location: skp_pegawai.php?admin");
}

