<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UnitKerja
 *
 * @author sani
 */
class UnitKerja {
    public $db;
    public $created;
    public $modified;
    public $model;

    function __construct($db) {
        $this->db = $db;
        $this->created = date('Y-m-d');
        $this->modified = date('Y-m-d');
        $this->model = 'UNIT_KERJA';
    }
    
    public function findParent($id){
        return $this->db->QuerySingle("select id_unit_kerja_induk from unit_kerja where id_unit_kerja = '$id'");
    }
}
