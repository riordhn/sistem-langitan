<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SkpProgres
 *
 * @author sani
 */
class SkpProgres {
    //put your code here
    public $db;
    public $created;
    public $modified;
    public $model;

    function __construct($db) {
        $this->db = $db;
        $this->created = date('Y-m-d H:i:s');
        $this->modified = date('Y-m-d H:i:s');
        $this->model = 'SKP_PROGRES';
    }

    public function actionSave($type, $id, $id_pengguna, $id_skp_pegawai, $progres, $output, $keterangan) {

        if ($type == 'create') {
            $this->db->Query("
            INSERT INTO SKP_PEGAWAI 
                (
                    ID_PENGGUNA, 
                    ID_SKP_PEGAWAI,
                    CREATED,
                    MODIFIED,
                    KETERANGAN
                ) 
            VALUES 
                (
                    ID_PENGGUNA, 
                    ID_SKP_PEGAWAI,
                    TO_DATE('$this->created', 'YYYY-MM-DD HH24:MI:SS'),
                    TO_DATE('$this->modified', 'YYYY-MM-DD HH24:MI:SS'),
                    '{$keterangan}'
                )
        ");
        } else if ($type == 'update') {
            $this->db->Query("
            UPDATE SKP_PROGRES
            SET
                MODIFIED = TO_DATE('$this->modified', 'YYYY-MM-DD HH24:MI:SS'),
                KETERANGAN = '{$keterangan}'
            WHERE ID_SKP_PEGAWAI = '{$id}'
        ");
        }
    }

    public function findByPk($id) {


        return $this->db->QueryToArray("SELECT skp.*, 
                        seksi.sasaran_kerja as sasaran_kerja_seksi, 
                        periode.nama_periode
                        FROM skp_progres skp
                        LEFT JOIN skp_pegawai skp_pegawai ON skp_pegawai.id_skp_pegawai=skp.id_skp_pegawai
                         WHERE skp.id_skp_pegawai = '$id'
                        ");
    }

    public function findAll() {


        return $this->db->QueryToArray("SELECT skp.*, 
                        seksi.sasaran_kerja as sasaran_kerja_seksi, 
                        periode.nama_periode
                        FROM skp_progres skp
                        LEFT JOIN skp_pegawai skp_pegawai ON skp_pegawai.id_skp_pegawai=skp.id_skp_pegawai");
    }
    
    public function delete($id) {
        $result = $this->db->Query("DELETE FROM SKP_PROGRES WHERE ID_SKP_PROGRES='{$id}'");
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function sejarahJabatanStruktural($id) {

        return $this->db->QuerySingle("
            select id_unit_kerja from (
                select id_unit_kerja from sejarah_jabatan_struktural
                where id_pengguna = '{$id}' order by id_sejarah_jabatan_struktural
            )
        ");
    }
}
