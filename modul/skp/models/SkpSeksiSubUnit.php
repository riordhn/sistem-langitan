<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SkpSeksiSubUnit
 *
 * @author sani
 */
class SkpSeksiSubUnit {
    public $db;
    public $created;
    public $modified;
    public $model;

    function __construct($db) {
        $this->db = $db;
        $this->created = date('Y-m-d H:i:s');
        $this->modified = date('Y-m-d H:i:s');
        $this->model = 'SKP_SEKSI_SUB_UNIT';
    }

    public function actionSave($type, $id, $id_skp_sub_unit, $id_unit_kerja, $id_periode, $id_sejarah_jabatan_struktural, $kode_program_kerja, $skp_seksi_sub_unit, $keterangan) {
        if ($type == 'create') {
            $this->db->Query("
            INSERT INTO SKP_SEKSI_SUB_UNIT 
                (
                    ID_SKP_SUB_UNIT, 
                    ID_UNIT_KERJA,
                    ID_PERIODE, 
                    ID_SEJARAH_JABATAN_STRUKTURAL, 
                    KODE_PROGRAM_KERJA, 
                    SASARAN_KERJA, 
                    CREATED, 
                    MODIFIED,
                    KETERANGAN
                ) 
            VALUES 
                (
                    '{$id_skp_sub_unit}',
                    '{$id_unit_kerja}',
                    '{$id_periode}',
                    '{$id_sejarah_jabatan_struktural}',
                    '{$kode_program_kerja}',
                    '{$skp_seksi_sub_unit}', 
                    TO_DATE('$this->created', 'YYYY-MM-DD HH24:MI:SS'),
                    TO_DATE('$this->modified', 'YYYY-MM-DD HH24:MI:SS'),                  
                    '{$keterangan}'
                )
        ");
        } else if ($type == 'update') {
            $this->db->Query("
            UPDATE SKP_SEKSI_SUB_UNIT 
            SET             
                ID_SKP_SUB_UNIT = '{$id_skp_sub_unit}', 
                ID_UNIT_KERJA = '{$id_unit_kerja}',
                ID_PERIODE = '{$id_periode}',
                ID_SEJARAH_JABATAN_STRUKTURAL = '{$id_sejarah_jabatan_struktural}',
                KODE_PROGRAM_KERJA = '{$kode_program_kerja}',
                SASARAN_KERJA = '{$skp_seksi_sub_unit}', 
                MODIFIED = TO_DATE('$this->modified', 'YYYY-MM-DD HH24:MI:SS'),
                KETERANGAN = '{$keterangan}'               
            WHERE ID_SKP_SEKSI_SUB_UNIT = '{$id}'
        ");
        }
    }

    public function delete($id) {
        $result = $this->db->Query("DELETE FROM SKP_SEKSI_SUB_UNIT WHERE ID_SKP_SEKSI_SUB_UNIT='{$id}'");
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function findAll() {
        return $this->db->QueryToArray("
                    SELECT
                            SKP_SEKSI_SUB_UNIT.*,
                            SKP_SEKSI_SUB_UNIT.KODE_PROGRAM_KERJA,
                            LP.NAMA_PERIODE AS NAMA_PERIODE,
                            SKP_SEKSI_SUB_UNIT.SASARAN_KERJA as SASARAN_KERJA,
                            SKP_SUNIT.SASARAN_KERJA AS SASARAN_KERJA_SUB_UNIT
                    FROM
                            SKP_SEKSI_SUB_UNIT
                    LEFT JOIN SKP_SUB_UNIT SKP_SUNIT ON SKP_SUNIT.ID_SKP_SUB_UNIT = SKP_SEKSI_SUB_UNIT.ID_SKP_SUB_UNIT 
                    LEFT JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA = SKP_SEKSI_SUB_UNIT.ID_UNIT_KERJA
                    LEFT JOIN LAKIP_PERIODE LP ON LP.ID_PERIODE = SKP_SEKSI_SUB_UNIT.ID_PERIODE
                    ORDER BY SKP_SEKSI_SUB_UNIT.ID_SKP_SEKSI_SUB_UNIT DESC
                ");
    }

    public function findByPk($id) {
        return $this->db->QueryToArray("
                    SELECT
                            SKP_SEKSI_SUB_UNIT.*,
                            SKP_SEKSI_SUB_UNIT.KODE_PROGRAM_KERJA,
                            LP.NAMA_PERIODE AS NAMA_PERIODE,
                            SKP_SEKSI_SUB_UNIT.SASARAN_KERJA as SASARAN_KERJA,
                            SKP_SUNIT.SASARAN_KERJA AS SASARAN_KERJA_SUB_UNIT
                    FROM
                            SKP_SEKSI_SUB_UNIT
                    LEFT JOIN SKP_SUB_UNIT SKP_SUNIT ON SKP_SUNIT.ID_SKP_SUB_UNIT = SKP_SEKSI_SUB_UNIT.ID_SKP_SUB_UNIT 
                    LEFT JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA = SKP_SEKSI_SUB_UNIT.ID_UNIT_KERJA
                    LEFT JOIN LAKIP_PERIODE LP ON LP.ID_PERIODE = SKP_SEKSI_SUB_UNIT.ID_PERIODE
                    WHERE SKP_SEKSI_SUB_UNIT.ID_SKP_SEKSI_SUB_UNIT = '{$id}' 
                ");
    }
    
    public function findAllByUnitKerja($id) {
        return $this->db->QueryToArray("
            SELECT SKP_SEKSI_SUNIT.*,UK.NM_UNIT_KERJA,LP.NAMA_PERIODE,LP.AWAL_PERIODE,LP.AKHIR_PERIODE
                    FROM SKP_SEKSI_SUB_UNIT SKP_SEKSI_SUNIT
                    JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=SKP_SEKSI_SUNIT.ID_UNIT_KERJA
                    JOIN LAKIP_PERIODE LP ON LP.ID_PERIODE=SKP_SEKSI_SUNIT.ID_PERIODE
            WHERE SKP_SEKSI_SUNIT.ID_UNIT_KERJA='$id'
        ");
    }

    public function sejarahJabatanStruktural($id) {
              
        return $this->db->QuerySingle("
            select id_unit_kerja from (
                select id_unit_kerja from sejarah_jabatan_struktural
                where id_pengguna = '{$id}' order by id_sejarah_jabatan_struktural
            )
        ");
                
    }
}