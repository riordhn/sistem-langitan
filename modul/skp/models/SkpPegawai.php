<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SkpPegawai
 *
 * @author sani
 */
class SkpPegawai {

    //put your code here
    public $db;
    public $created;
    public $modified;
    public $model;

    function __construct($db) {
        $this->db = $db;
        $this->created = date('Y-m-d H:i:s');
        $this->modified = date('Y-m-d H:i:s');
        $this->model = 'SKP_PEGAWAI';
    }

    public function actionSave($type, $id, $id_skp_seksi_sub_unit, $id_periode, $id_sejarah_jabatan_struktural, $sasaran_kerja, $kuantitas, $angka_kredit, $biaya, $tanggal_mulai, $tanggal_selesai, $keterangan) {

        if ($type == 'create') {
            $this->db->Query("
            INSERT INTO SKP_PEGAWAI 
                (
                    ID_SKP_SEKSI_SUB_UNIT,
                    ID_PERIODE, 
                    ID_SEJARAH_JABATAN_STRUKTURAL,
                    SASARAN_KERJA,
                    KUANTITAS,
                    BIAYA,
                    ANGKA_KREDIT,
                    TANGGAL_MULAI, 
                    TANGGAL_SELESAI,
                    CREATED,
                    MODIFIED,
                    KETERANGAN
                ) 
            VALUES 
                (
                    '{$id_skp_seksi_sub_unit}',
                    '{$id_periode}',
                    '{$id_sejarah_jabatan_struktural}',
                    '{$sasaran_kerja}', 
                    '{$kuantitas}',
                    '{$biaya}',
                    '{$angka_kredit}',
                    TO_DATE('$tanggal_mulai', 'YYYY-MM-DD'),
                    TO_DATE('$tanggal_selesai', 'YYYY-MM-DD'),
                    TO_DATE('$this->created', 'YYYY-MM-DD HH24:MI:SS'),
                    TO_DATE('$this->modified', 'YYYY-MM-DD HH24:MI:SS'),
                    '{$keterangan}'
                )
        ");
        } else if ($type == 'update') {
            $this->db->Query("
            UPDATE SKP_PEGAWAI 
            SET             
                ID_SKP_SEKSI_SUB_UNIT = '{$id_skp_seksi_sub_unit}',
                ID_PERIODE = '{$id_periode}',
                ID_SEJARAH_JABATAN_STRUKTURAL = '{$id_sejarah_jabatan_struktural}',
                SASARAN_KERJA = '{$sasaran_kerja}', 
                KUANTITAS = '{$kuantitas}',
                BIAYA = '{$biaya}',
                ANGKA_KREDIT = '{$angka_kredit}',
                TANGGAL_MULAI = TO_DATE('$tanggal_mulai', 'YYYY-MM-DD'),
                TANGGAL_SELESAI = TO_DATE('$tanggal_selesai', 'YYYY-MM-DD'),
                MODIFIED = TO_DATE('$this->modified', 'YYYY-MM-DD HH24:MI:SS'),
                KETERANGAN = '{$keterangan}'
            WHERE ID_SKP_PEGAWAI = '{$id}'
        ");
        }
    }

    public function findByPk($id) {


        return $this->db->QueryToArray("SELECT skp.*, 
                        seksi.sasaran_kerja as sasaran_kerja_seksi, 
                        periode.nama_periode
                        FROM skp_pegawai skp
                       
                        LEFT JOIN skp_seksi_sub_unit seksi ON seksi.id_skp_seksi_sub_unit=skp.id_skp_seksi_sub_unit
                        LEFT JOIN lakip_periode periode ON periode.id_periode = skp.id_periode
                         WHERE skp.id_skp_pegawai = '$id'
                        ");
    }

    public function findAll() {


        return $this->db->QueryToArray("SELECT skp.*, 
                        seksi.sasaran_kerja as sasaran_kerja_seksi, 
                        periode.nama_periode
                        FROM skp_pegawai skp
                        LEFT JOIN skp_seksi_sub_unit seksi ON seksi.id_skp_seksi_sub_unit=skp.id_skp_seksi_sub_unit
                        LEFT JOIN lakip_periode periode ON periode.id_periode = skp.id_periode");
    }
    
    public function delete($id) {
        $result = $this->db->Query("DELETE FROM SKP_PEGAWAI WHERE ID_SKP_PEGAWAI='{$id}'");
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function sejarahJabatanStruktural($id) {

        return $this->db->QuerySingle("
            select id_unit_kerja from (
                select id_unit_kerja from sejarah_jabatan_struktural
                where id_pengguna = '{$id}' order by id_sejarah_jabatan_struktural
            )
        ");
    }

}
