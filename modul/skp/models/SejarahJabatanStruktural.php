<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SejarahJabatanStruktural
 *
 * @author sani
 */
class SejarahJabatanStruktural {
    //put your code here
    public $db;
    public $created;
    public $modified;
    public $model;

    function __construct($db) {
        $this->db = $db;
        $this->created = date('Y-m-d');
        $this->modified = date('Y-m-d');
        $this->model = 'SEJARAH_JABATAN_STRUKTURAL';
    }
    
    
    public function findByPengguna($id){
        return $this->db->QuerySingle("
            select id_unit_kerja from (
                select id_unit_kerja from sejarah_jabatan_struktural
                where id_pengguna = '{$id}' order by id_sejarah_jabatan_struktural
            )
        ");
    }
    
    public function isKepalaSeksi($id_pengguna){
   
        $result =  $this->db->QuerySingle("select count(*)
                FROM sejarah_jabatan_struktural sjs
                LEFT JOIN jabatan_struktural js on js.id_jabatan_struktural = sjs.id_jabatan_struktural
                WHERE (js.nm_singkatan='kasi' OR js.nm_singkatan='kabid') and sjs.id_pengguna = '{$id_pengguna}'");
        if($result > 0){
            return true;
        }else{
            return false;
        }
        
    }
    
    public function isKepalaSubDit($id_pengguna){
   
        $result =  $this->db->QuerySingle("select count(*)
                FROM sejarah_jabatan_struktural sjs
                LEFT JOIN jabatan_struktural js on js.id_jabatan_struktural = sjs.id_jabatan_struktural
                WHERE (js.nm_singkatan='kasubdit' OR js.nm_singkatan='kabag') and sjs.id_pengguna = '{$id_pengguna}'");
        if($result > 0){
            return true;
        }else{
            return false;
        }
        
    }
    
    public function isPimpinan($id_pengguna){
   
        $result =  $this->db->QuerySingle("select count(*)
                FROM sejarah_jabatan_struktural sjs
                LEFT JOIN jabatan_struktural js on js.id_jabatan_struktural = sjs.id_jabatan_struktural
                WHERE (js.nm_singkatan='dir' OR js.nm_singkatan='wadek1' OR js.nm_singkatan = 'wadek2' OR js.nm_singkatan='wadek3') and sjs.id_pengguna = '{$id_pengguna}'");
        if($result > 0){
            return true;
        }else{
            return false;
        }
        
    }
}
