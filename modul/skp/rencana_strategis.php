<?php

include 'config.php';
include 'class/lakip.class.php';

$lak = new lakip($db);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $lak->tambah_lakip_tujuan(post('kode'), post('tujuan'), post('keterangan'));
    } else if (post('mode') == 'edit') {
        $lak->update_lakip_tujuan(post('id'), post('kode'), post('tujuan'), post('keterangan'));
    } else if (post('mode') == 'delete') {
        $lak->delete_lakip_tujuan(post('id'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit' || get('mode') == 'delete') {
        $smarty->assign('tujuan', $lak->get_lakip_tujuan(get('id')));
    }
}

$smarty->assign('data_tujuan', $lak->load_lakip_tujuan());
$smarty->display('SasaranKerja/rencana_strategis.tpl');
?>


