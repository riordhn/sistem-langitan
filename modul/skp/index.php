<?php
include 'config.php';

if ($user->Role() == 46)
{
    // data menu dari $user
    if($_SESSION['standar_iso']=='no'){
        header("Location: ../../login.php?mode=ltp-null");
        exit();
    }
    $smarty->assign('modul_set', $user->MODULs);
    $smarty->assign('name', $user->NM_PENGGUNA);
    $smarty->display("index.tpl");
}
else
{
    header("location: /logout.php");
    exit();
}
?>
