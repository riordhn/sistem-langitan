<?php

include 'config.php';
include 'class/lakip.class.php';

include 'models/SkpPegawai.php';
include 'models/UnitKerja.php';


$lak = new lakip($db);
$SkpPegawai = new SkpPegawai($db);
$UnitKerja = new UnitKerja($db);
$SejarahJabatanStruktural = new SejarahJabatanStruktural($db);


## action create

if (isset($_GET['create'])) {
    if (isset($_POST['sasaran_kerja'])) {

        $SkpPegawai->actionSave(
                'create', 
                null, 
                post('skp_seksi_sub_unit'), 
                post('periode'), 
                $SkpPegawai->sejarahJabatanStruktural($user->ID_PENGGUNA),
                post('sasaran_kerja'), 
                post('kuantitas'),
                             
                post('angka_kredit'),
                post('biaya'), 
                date('Y-m-d',  strtotime(post('tanggal_mulai'))),
                date('Y-m-d', strtotime(post('tanggal_selesai'))),
                post('keterangan')
        );

        $data = array();
        $data["type"] = 'success';
        $data["title"] = 'Success';
        $data["text"] = 'Data berhasil ditambah';
        $data["delay"] = 3000;
        $data["styling"] = 'jqueryui';
        echo json_encode($data);
        exit();
        
    } else {

        $smarty->assign('model_periode', $lak->load_lakip_periode());
        $smarty->assign('SejarahJabatanStruktural', $SejarahJabatanStruktural->findByPengguna($user->ID_PENGGUNA));
        $smarty->assign('model_unit_kerja', $lak->load_list_unit_kerja_pengguna($user->ID_PENGGUNA));
        $smarty->assign('skp_seksi_sub_unit', $SkpSeksiSubUnit->findAll());

        $smarty->display('SkpPegawai/create.tpl');
    }
}

## action update
else if (isset($_GET['update'])) {
    $id = (int) $_GET['id'];
    $model = $SkpPegawai->findByPk($id);
    if (isset($_POST['sasaran_kerja'])) {
        $SkpPegawai->actionSave(
                'update', 
                post('id'), 
                post('skp_seksi_sub_unit'), 
                post('periode'), 
                $SkpPegawai->sejarahJabatanStruktural($user->ID_PENGGUNA),
                post('sasaran_kerja'),
                post('kuantitas'),
                post('angka_kredit'),
                post('biaya'), 
                date('Y-m-d',  strtotime(post('tanggal_mulai'))),
                date('Y-m-d', strtotime(post('tanggal_selesai'))),
                post('keterangan')
        );
        $data = array();
        $data["type"] = 'success';
        $data["title"] = 'Success';
        $data["text"] = 'Data berhasil diupdate';
        $data["delay"] = 3000;
        $data["styling"] = 'jqueryui';
        echo json_encode($data);
        exit();
    } else {
        
        $smarty->assign('model_periode', $lak->load_lakip_periode());
        $smarty->assign('sejarah_jabatan_struktural', $SkpPegawai->sejarahJabatanStruktural($user->ID_PENGGUNA));
        $smarty->assign('model_skp_seksi_sub_unit', $SkpSeksiSubUnit->findAll());
        $smarty->assign('model', $model);
        $smarty->display('SkpPegawai/update.tpl');
    }
}

## action delete
else if (isset($_GET['delete'])) {
    if (isset($_POST['id'])) {
        $id = (int) $_POST['id'];
        if ($SkpPegawai->delete($id)) {
            $data = array();
            $data["type"] = 'success';
            $data["title"] = 'Success';
            $data["text"] = 'Data berhasil dihapus';
            $data["delay"] = 3000;
            $data["styling"] = 'jqueryui';
            echo json_encode($data);
            exit();
        } else {
            $data = array();
            $data["type"] = 'notice';
            $data["title"] = 'Failed';
            $data["text"] = 'Data gagal dihapus';
            $data["delay"] = 3000;
            $data["styling"] = 'jqueryui';
            echo json_encode($data);
            exit();
        }
    }
}

## action admin
else if (isset($_GET['admin'])) {

    $smarty->assign('model', $SkpProgres->findAll());
    
    if (isset($_GET['ajax'])) {
        $smarty->assign('url_create', '#sasaran_kerja-skp_pegawai!skp_pegawai.php?create');
        $smarty->assign('url_update', '#sasaran_kerja-skp_pegawai!skp_pegawai.php?update&id=');
    } else {
        $smarty->assign('url_create', 'skp_pegawai.php?create');
        $smarty->assign('url_update', 'skp_pegawai.php?update&id=');
    }

    $smarty->display('SkpProgres/admin.tpl');
}


## return ajax data
else if (isset($_GET['id_unit_kerja'])) {

    $id_unit_kerja = (int) get('id_unit_kerja');

    //$parentUnitKerja = $UnitKerja->findParent($id_unit_kerja);

    $model = $SkpSeksiSubUnit->findAllByUnitKerja($id_unit_kerja); //$SkpSeksiSubUnit->findAllByUnitKerja($parentUnitKerja);

    echo '<option value="">Pilih Sasaran Kerja Seksi Sub Unit</option>';

    foreach ($model as $d) {
        echo "<option value='{$d['ID_SKP_SEKSI_SUB_UNIT']}'>{$d['SASARAN_KERJA']} ({$d['NAMA_PERIODE']})</option>";
    }

    exit();
}