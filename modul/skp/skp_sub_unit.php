<?php

include 'config.php';
include 'class/lakip.class.php';

include 'models/SkpSubUnit.php';
//include 'models/SejarahJabatanStruktural.php';

$lak = new lakip($db);
$SkpSubUnit = new SkpSubUnit($db);
$SejarahJabatanStruktural = new SejarahJabatanStruktural($db);


## rules
if(!$SejarahJabatanStruktural->isKepalaSubDit($user->ID_PENGGUNA)){
    echo "Anda tidak memiliki akses di halaman ini";
    exit();
}


## action create

if (isset($_GET['create'])) {
    if (isset($_POST['sasaran_kerja'])) {

        $SkpSubUnit->actionSave(
            'create', null, post('program_kerja'), post('unit_kerja'), post('periode'), $SkpSubUnit->sejarahJabatanStruktural($user->ID_PENGGUNA), post('kode'), post('sasaran_kerja'), post('keterangan')
        );
        $data = array();
        $data["type"] = 'success';
        $data["title"] = 'Success';
        $data["text"] = 'Data berhasil ditambah';
        $data["delay"] = 3000;
        $data["styling"] = 'jqueryui';
        echo json_encode($data);
        exit();
        //header("Location: skp_sub_unit.php?admin");
    } else {
        $smarty->assign('data_program_utama', $lak->load_lakip_program());
        $smarty->assign('data_periode', $lak->load_lakip_periode());
        $smarty->assign('sejarah_jabatan_struktural', $SkpSubUnit->sejarahJabatanStruktural($user->ID_PENGGUNA));
        $smarty->assign('data_unit_kerja', $lak->load_list_unit_kerja_pengguna($user->ID_PENGGUNA));
        $smarty->assign('lakip_program_kerja', $lak->load_lakip_program_kerja());

        $smarty->display('SkpSubUnit/create.tpl');
    }
} 

## action update
else if (isset($_GET['update'])) {
    $id = (int) $_GET['id'];
    $model = $SkpSubUnit->findByPk($id);
    if (isset($_POST['sasaran_kerja'])) {

        $SkpSubUnit->actionSave(
                'update', post('id'), post('program_kerja'), post('unit_kerja'), post('periode'), $sejarah_jabatan_struktural, post('kode'), post('sasaran_kerja'), post('keterangan')
        );
        $data = array();
        $data["type"] = 'success';
        $data["title"] = 'Success';
        $data["text"] = 'Data berhasil diupdate';
        $data["delay"] = 3000;
        $data["styling"] = 'jqueryui';
        echo json_encode($data);
        exit();
        //header("Location: skp_sub_unit.php?update&id=$id");
    } else {
        $smarty->assign('data_program_utama', $lak->load_lakip_program());
        $smarty->assign('data_periode', $lak->load_lakip_periode());
        $smarty->assign('sejarah_jabatan_struktural', $SkpSubUnit->sejarahJabatanStruktural($user->ID_PENGGUNA));
        $smarty->assign('data_unit_kerja', $lak->load_list_unit_kerja_pengguna($user->ID_PENGGUNA));
        $smarty->assign('lakip_program_kerja', $lak->load_lakip_program_kerja());
        $smarty->assign('model', $model);
        $smarty->display('SkpSubUnit/update.tpl');
    }
} 

## action delete
else if (isset($_GET['delete'])) {
    if (isset($_POST['id'])) {
        $id = (int) $_POST['id'];
        if ($SkpSubUnit->delete($id)) {
            $data = array();
            $data["type"] = 'success';
            $data["title"] = 'Success';
            $data["text"] = 'Data berhasil dihapus';
            $data["delay"] = 3000;
            $data["styling"] = 'jqueryui';
            echo json_encode($data);
            exit();
        } else {
            $data = array();
            $data["type"] = 'notice';
            $data["title"] = 'Failed';
            $data["text"] = 'Data gagal dihapus';
            $data["delay"] = 3000;
            $data["styling"] = 'jqueryui';
            echo json_encode($data);
            exit();
        }
    }
} 

## action admin
else if (isset($_GET['admin'])) {

    $isAjax = $smarty->assign('data_program_utama', $lak->load_lakip_program());
    $smarty->assign('data_periode', $lak->load_lakip_periode());
    $smarty->assign('sejarah_jabatan_struktural', $SkpSubUnit->sejarahJabatanStruktural($user->ID_PENGGUNA));
    $smarty->assign('data_unit_kerja', $lak->load_list_unit_kerja_pengguna($user->ID_PENGGUNA));
    $smarty->assign('data_tujuan', $lak->load_lakip_tujuan());
    $smarty->assign('model', $SkpSubUnit->findAll());
    if (isset($_GET['ajax'])) {
        $smarty->assign('url_create', '#sasaran_kerja-skp_sub_unit!skp_sub_unit.php?create');
        $smarty->assign('url_update', '#sasaran_kerja-skp_sub_unit!skp_sub_unit.php?update&id=');
    } else {
        $smarty->assign('url_create', 'skp_sub_unit.php?create');
        $smarty->assign('url_update', 'skp_sub_unit.php?update&id=');
    }
    $smarty->display('SkpSubUnit/admin.tpl');
} 


## return ajax data
else if (isset($_GET['id_unit_kerja'])) {

    $id_unit_kerja = (int) get('id_unit_kerja');
    $id = $db->QuerySingle("SELECT ID_UNIT_KERJA_INDUK FROM UNIT_KERJA WHERE ID_UNIT_KERJA='$id_unit_kerja'");

    $data = $db->QueryToArray("
            SELECT LPK.*,UK.NM_UNIT_KERJA,LP.NAMA_PERIODE,LP.AWAL_PERIODE,LP.AKHIR_PERIODE,LPU.PROGRAM_UTAMA,LPR.TUJUAN_RENSTRA
                    FROM LAKIP_PROGRAM_KERJA LPK
                    JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=LPK.ID_UNIT_KERJA
                    JOIN LAKIP_PERIODE LP ON LP.ID_PERIODE=LPK.ID_PERIODE
                    JOIN LAKIP_PROGRAM_UTAMA LPU ON LPU.ID_PROGRAM_UTAMA=LPK.ID_PROGRAM_UTAMA
                    JOIN LAKIP_TUJUAN_RENSTRA LPR ON LPR.ID_TUJUAN_RENSTRA=LPU.ID_TUJUAN_RENSTRA
            WHERE LPK.ID_UNIT_KERJA='$id'
        ");
    echo $id;
    print_r($data);
    echo '<option value="">Pilih Sasaran Kerja Unit</option>';
    foreach ($data as $d) {
        echo "<option value='{$d['ID_PROGRAM_KERJA']}'>{$d['PROGRAM_KERJA']} ({$d['NAMA_PERIODE']})</option>";
    }

    exit();
}
