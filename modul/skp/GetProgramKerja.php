<?php

include 'config.php';

if (isset($_GET['id_unit_kerja']) && isset($_GET['id_periode'])) {
    $id = $_REQUEST['id_unit_kerja'];

    $data = $db->QueryToArray("
    SELECT LPK.*,UK.NM_UNIT_KERJA,LP.NAMA_PERIODE,LP.AWAL_PERIODE,LP.AKHIR_PERIODE,LPU.PROGRAM_UTAMA,LPR.TUJUAN_RENSTRA
            FROM LAKIP_PROGRAM_KERJA LPK
            JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=LPK.ID_UNIT_KERJA
            JOIN LAKIP_PERIODE LP ON LP.ID_PERIODE=LPK.ID_PERIODE
            JOIN LAKIP_PROGRAM_UTAMA LPU ON LPU.ID_PROGRAM_UTAMA=LPK.ID_PROGRAM_UTAMA
            JOIN LAKIP_TUJUAN_RENSTRA LPR ON LPR.ID_TUJUAN_RENSTRA=LPU.ID_TUJUAN_RENSTRA
    WHERE LPK.ID_UNIT_KERJA='{$id}'");
    echo '<option value="">Pilih Sasaran Kerja Unit</option>';
    foreach ($data as $d) {
        echo "<option value='{$d['ID_PROGRAM_KERJA']}'>{$d['PROGRAM_KERJA']} ({$d['NAMA_PERIODE']})</option>";
    }    
} else {
    $id = $_REQUEST['id_unit_kerja'];

    $data = $db->QueryToArray("
    SELECT LPK.*,UK.NM_UNIT_KERJA,LP.NAMA_PERIODE,LP.AWAL_PERIODE,LP.AKHIR_PERIODE,LPU.PROGRAM_UTAMA,LPR.TUJUAN_RENSTRA
            FROM LAKIP_PROGRAM_KERJA LPK
            JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=LPK.ID_UNIT_KERJA
            JOIN LAKIP_PERIODE LP ON LP.ID_PERIODE=LPK.ID_PERIODE
            JOIN LAKIP_PROGRAM_UTAMA LPU ON LPU.ID_PROGRAM_UTAMA=LPK.ID_PROGRAM_UTAMA
            JOIN LAKIP_TUJUAN_RENSTRA LPR ON LPR.ID_TUJUAN_RENSTRA=LPU.ID_TUJUAN_RENSTRA
    WHERE LPK.ID_UNIT_KERJA='{$id}'");
    echo '<option value="">Pilih Sasaran Kerja Unit</option>';
    foreach ($data as $d) {
        echo "<option value='{$d['ID_PROGRAM_KERJA']}'>{$d['PROGRAM_KERJA']} ({$d['NAMA_PERIODE']})</option>";
    }
}
?>
