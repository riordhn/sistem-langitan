<?php

include 'config.php';
include 'class/lakip.class.php';

include 'models/SkpSeksiSubUnit.php';
include 'models/SkpSubUnit.php';
//include 'models/SejarahJabatanStruktural.php';

$lak = new lakip($db);
$SkpSeksiSubUnit = new SkpSeksiSubUnit($db);
$SkpSubUnit = new SkpSubUnit($db);
$SejarahJabatanStruktural = new SejarahJabatanStruktural($db);

## rules


if(!$SejarahJabatanStruktural->isKepalaSeksi($user->ID_PENGGUNA)){
    echo "Anda tidak memiliki akses di halaman ini";
    exit();
}


## action create

if (isset($_GET['create'])) {
    if (isset($_POST['skp_seksi_sub_unit'])) {

        $SkpSeksiSubUnit->actionSave(
            'create', null, post('skp_sub_unit'), post('unit_kerja'), post('periode'), $SkpSeksiSubUnit->sejarahJabatanStruktural($user->ID_PENGGUNA), post('kode'), post('skp_seksi_sub_unit'), post('keterangan')
        );
        $data = array();
        $data["type"] = 'success';
        $data["title"] = 'Success';
        $data["text"] = 'Data berhasil ditambah';
        $data["delay"] = 3000;
        $data["styling"] = 'jqueryui';
        echo json_encode($data);
        exit();
        //header("Location: skp_seksi_sub_unit.php?admin");
    } else {
        //$smarty->assign('data_program_utama', $lak->load_lakip_program());
        $smarty->assign('data_periode', $lak->load_lakip_periode());
        $smarty->assign('sejarah_jabatan_struktural', $SkpSeksiSubUnit->sejarahJabatanStruktural($user->ID_PENGGUNA));
        $smarty->assign('data_unit_kerja', $lak->load_list_unit_kerja_pengguna($user->ID_PENGGUNA));
        $smarty->assign('skp_sub_unit', $SkpSubUnit->findAll());

        $smarty->display('SkpSeksiSubUnit/create.tpl');
    }
} 

## action update
else if (isset($_GET['update'])) {
    $id = (int) $_GET['id'];
    $model = $SkpSeksiSubUnit->findByPk($id);
    if (isset($_POST['skp_seksi_sub_unit'])) {

        $SkpSeksiSubUnit->actionSave(
                'update', post('id'), post('skp_sub_unit'), post('unit_kerja'), post('periode'), $sejarah_jabatan_struktural, post('kode'), post('skp_seksi_sub_unit'), post('keterangan')
        );
        $data = array();
        $data["type"] = 'success';
        $data["title"] = 'Success';
        $data["text"] = 'Data berhasil diupdate';
        $data["delay"] = 3000;
        $data["styling"] = 'jqueryui';
        echo json_encode($data);
        exit();
        //header("Location: skp_seksi_sub_unit.php?update&id=$id");
    } else {
        $smarty->assign('model_periode', $lak->load_lakip_periode());
        $smarty->assign('sejarah_jabatan_struktural', $SkpSeksiSubUnit->sejarahJabatanStruktural($user->ID_PENGGUNA));
        $smarty->assign('model_unit_kerja', $lak->load_list_unit_kerja_pengguna($user->ID_PENGGUNA));
        $smarty->assign('model_skp_sub_unit', $SkpSubUnit->findAll());
        $smarty->assign('model', $model);
        
        //print_r($model);
        $smarty->display('SkpSeksiSubUnit/update.tpl');
    }
} 

## action delete
else if (isset($_GET['delete'])) {
    if (isset($_POST['id'])) {
        $id = (int) $_POST['id'];
        if ($SkpSeksiSubUnit->delete($id)) {
            $data = array();
            $data["type"] = 'success';
            $data["title"] = 'Success';
            $data["text"] = 'Data berhasil dihapus';
            $data["delay"] = 3000;
            $data["styling"] = 'jqueryui';
            echo json_encode($data);
            exit();
        } else {
            $data = array();
            $data["type"] = 'notice';
            $data["title"] = 'Failed';
            $data["text"] = 'Data gagal dihapus';
            $data["delay"] = 3000;
            $data["styling"] = 'jqueryui';
            echo json_encode($data);
            exit();
        }
    }
} 

## action admin
else if (isset($_GET['admin'])) {
    
    $smarty->assign('data_periode', $lak->load_lakip_periode());
    $smarty->assign('sejarah_jabatan_struktural', $SkpSeksiSubUnit->sejarahJabatanStruktural($user->ID_PENGGUNA));
    $smarty->assign('data_unit_kerja', $lak->load_list_unit_kerja_pengguna($user->ID_PENGGUNA));
    //$smarty->assign('data_tujuan', $lak->load_lakip_tujuan());
    $smarty->assign('model', $SkpSeksiSubUnit->findAll());
    if (isset($_GET['ajax'])) {
        $smarty->assign('url_create', '#sasaran_kerja-skp_sub_unit!skp_seksi_sub_unit.php?create');
        $smarty->assign('url_update', '#sasaran_kerja-skp_sub_unit!skp_seksi_sub_unit.php?update&id=');
    } else {
        $smarty->assign('url_create', 'skp_seksi_sub_unit.php?create');
        $smarty->assign('url_update', 'skp_seksi_sub_unit.php?update&id=');
    }
    $smarty->display('SkpSeksiSubUnit/admin.tpl'); 
    
} 


## return ajax data
else if (isset($_GET['id_unit_kerja'])) {

    $id_unit_kerja = (int) get('id_unit_kerja');
    $id = $db->QuerySingle("SELECT ID_UNIT_KERJA_INDUK FROM UNIT_KERJA WHERE ID_UNIT_KERJA='$id_unit_kerja'");

    
    $data = $db->QueryToArray("
            SELECT SKP_SUNIT.*,UK.NM_UNIT_KERJA,LP.NAMA_PERIODE,LP.AWAL_PERIODE,LP.AKHIR_PERIODE
                    FROM SKP_SUB_UNIT SKP_SUNIT
                    JOIN UNIT_KERJA UK ON UK.ID_UNIT_KERJA=SKP_SUNIT.ID_UNIT_KERJA
                    JOIN LAKIP_PERIODE LP ON LP.ID_PERIODE=SKP_SUNIT.ID_PERIODE
            WHERE SKP_SUNIT.ID_UNIT_KERJA='$id'
        ");
   
    
    echo '<option value="">Pilih Sasaran Kerja Unit</option>';
    
    foreach ($data as $d) {
        echo "<option value='{$d['ID_SKP_SUB_UNIT']}'>{$d['SASARAN_KERJA']} ({$d['NAMA_PERIODE']})</option>";
    }

    exit();
}