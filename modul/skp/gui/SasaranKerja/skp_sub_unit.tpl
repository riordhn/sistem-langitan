{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Sasaran Kerja Sub Unit Tambah</div>
    <form action="skp_sub_unit.php" method="post">
        <fieldset style="width: 98%">
            <legend>Tambah Sasaran Kerja Sub Unit</legend>
            <div class="field_form">
                <label>Unit Kerja : </label>
                <select id="unit_kerja" name="unit_kerja" class="required">
                    <option value="">Pilih Unit Kerja</option>
                    {foreach $data_unit_kerja as $f}
                        <option value="{$f.ID_UNIT_KERJA}">{$f.NM_UNIT_KERJA}</option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Kode Program : </label>
                <input type="text" size="10" maxlength="10" name="kode" class="required"/>
            </div>
            <div class="field_form">
                <label>Periode : </label>
                <select name="periode" class="required">
                    <option value="">Pilih Periode</option>
                    {foreach $data_periode as $per}
                        <option value="{$per.ID_PERIODE}">{$per.NAMA_PERIODE} ({$per.AWAL_PERIODE} - {$per.AKHIR_PERIODE})</option>
                    {/foreach}
                </select>
            </div>

            <div class="field_form">
                <label>Sasaran Kerja Unit : </label>
                <select id="program_kerja" name="program_kerja" class="required">
                    <option value="">Pilih Sasaran Kerja Unit</option>
                </select>
            </div>
                
            <div class="field_form">
                <label>Sasaran Kerja Sub Unit: </label>
                <textarea name="sasaran_kerja" class="required" cols="40"></textarea>
            </div>
                
            <div class="field_form">
                <label>Keterangan : </label>
                <textarea name="keterangan" cols="40"></textarea>
            </div>
                
            <div class="bottom_field_form">
                <a href="skp_sub_unit.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
                
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Sasaran Kerja Sub Unit Edit</div>
    <form action="skp_sub_unit.php" method="post">
        <fieldset style="width: 98%">
            <legend>Tambah Sasaran Kerja Sub Unit</legend>
            <div class="field_form">
                <label>Unit Kerja : </label>
                <select id="unit_kerja" name="unit_kerja" class="required">
                    <option value="">Pilih Unit Kerja</option>
                    {foreach $data_unit_kerja as $f}
                        <option value="{$f.ID_UNIT_KERJA}">{$f.NM_UNIT_KERJA}</option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Kode Program : </label>
                <input type="text" size="10" maxlength="10" name="kode" class="required"/>
            </div>
            <div class="field_form">
                <label>Periode : </label>
                <select name="periode" class="required">
                    <option value="">Pilih Periode</option>
                    {foreach $data_periode as $per}
                        <option value="{$per.ID_PERIODE}">{$per.NAMA_PERIODE} ({$per.AWAL_PERIODE} - {$per.AKHIR_PERIODE})</option>
                    {/foreach}
                </select>
            </div>

            <div class="field_form">
                <label>Sasaran Kerja Unit : </label>
                <select id="program_kerja" name="program_kerja" class="required">
                    <option value="">Pilih Sasaran Kerja Unit</option>
                </select>
            </div>
                
            <div class="field_form">
                <label>Sasaran Kerja Sub Unit: </label>
                <textarea name="sasaran_kerja" class="required" cols="40"></textarea>
            </div>
                
            <div class="field_form">
                <label>Keterangan : </label>
                <textarea name="keterangan" cols="40"></textarea>
            </div>
                
            <div class="bottom_field_form">
                <a href="skp_sub_unit.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
                
        </fieldset>
    </form>
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">Sasaran Kerja Sub Unit Hapus</div>
    <form action="skp_sub_unit.php" method="post">
        <fieldset style="width: 98%">
            <legend>Hapus Program Kerja</legend>
            <div class="field_form">
                <label>Kode Program : </label>
                {$program.KODE_PROGRAM_KERJA}
            </div>
            <div class="field_form">
                <label>Fakultas : </label>
                {$program.NM_UNIT_KERJA}
            </div>
            <div class="field_form">
                <label>Program Kerja: </label>
                {$program.PROGRAM_KERJA}
            </div>
            <div class="bottom_field_form">
                Apakah anda yakin menghapus item ini ? <br/>
                <a href="skp_sub_unit.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$program.ID_PROGRAM_KERJA}"/>
                <input type="hidden" name="mode" value="delete"/>
                <input type="submit" class="button" value="Ya"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Sasaran Kerja Sub Unit</div>
    <table style="width: 98%">
        <tr>
            <th>No</th>
            <th>Kode</th>
             <th>Periode</th>
            <!--
            <th>Unit Kerja</th>
            -->
            <th>Sasaran Kerja Unit</th>
           
            <th>Sasaran Kerja Sub Unit</th>
            <th style="width: 120px" class="center">Operasi</th>
        </tr>
        {foreach $data_skp_sub_unit as $skp}
            <tr>
                <td>{$skp@index+1}</td>
                <td>{$skp.KODE_PROGRAM_KERJA}</td>
                <td>{$skp.NAMA_PERIODE} <span style="color: green;font-size: 11px">({$skp.AWAL_PERIODE} - {$skp.AKHIR_PERIODE})</span></td>
                <!--
                <td>{$skp.NM_UNIT_KERJA}</td>
                -->
                <td>{$skp.PROGRAM_KERJA}</td>
                <td>{$skp.SASARAN_KERJA}</td>
                <td>
                    <a class="button" href="skp_sub_unit.php?mode=edit&id={$skp.ID_SKP_SUB_UNIT}">Edit</a>
                    <a class="button" href="skp_sub_unit.php?mode=delete&id={$skp.ID_SKP_SUB_UNIT}">Delete</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="10" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="10" class="center">
                <a href="skp_sub_unit.php?mode=tambah" class="button">Tambah</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
        $('form').validate();
        $('#unit_kerja').change(function(){
            $.ajax({
                type:'get',
                url:'skp_sub_unit.php',
                data:'id_unit_kerja='+$(this).val(),
                success:function(data){
                    $('#program_kerja').html(data);
                }
            })
        })
    </script>
{/literal}