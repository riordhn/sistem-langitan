{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Sasaran Kerja Pegawai Tambah</div>
    <form action="sasaran_kerja_pegawai.php" method="post">
        <fieldset style="width: 98%">
            <legend>Tambah Sasaran Kerja</legend>
            <div class="field_form">
                <label>Unit Kerja : </label>
                <select name="unit_kerja" class="required">
                    <option value="">Pilih Unit Kerja</option>
                    {foreach $data_unit_kerja as $f}
                        <option value="{$f.ID_UNIT_KERJA}">{$f.NM_UNIT_KERJA}</option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Kode Program : </label>
                <input type="text" size="10" maxlength="10" name="kode" class="required"/>
            </div>
            <div class="field_form">
                <label>Periode : </label>
                <select name="periode" class="required">
                    <option value="">Pilih Periode</option>
                    {foreach $data_periode as $per}
                        <option value="{$per.ID_PERIODE}">{$per.NAMA_PERIODE} ({$per.AWAL_PERIODE} - {$per.AKHIR_PERIODE})</option>
                    {/foreach}
                </select>
            </div>
            <!--
            <div class="field_form">
                <label>Rencana Strategis: </label>
                <select id="tujuan" name="tujuan">
                    <option>Pilih Tujuan</option>
                    {foreach $data_tujuan as $tuj}
                        <option value="{$tuj.ID_TUJUAN_RENSTRA}" {if $program.ID_TUJUAN_RENSTRA==$tuj.ID_TUJUAN_RENSTRA}selected="true"{/if}>{$tuj.TUJUAN_RENSTRA|substr:0:70} ......</option>
                    {/foreach}
                </select>
            </div>
            
            <div class="field_form">
                <label>Program Utama : </label>
                <select id="program_utama" name="program_utama" class="required">
                    <option value="">Pilih Program</option>
                </select>
            </div>
            -->
            <div class="field_form">
                <label>Sasaran Kerja Pegawai: </label>
                <textarea name="program_kerja" class="required" cols="40"></textarea>
            </div>
            <div class="field_form">
                <label>Keterangan : </label>
                <textarea name="keterangan" cols="40"></textarea>
            </div>
            <div class="bottom_field_form">
                <a href="sasaran_kerja_pegawai.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Sasaran Kerja Pegawai Edit</div>
    <form action="sasaran_kerja_pegawai.php" method="post">
        <fieldset style="width: 98%">
            <legend>Edit Program Kerja</legend>
            <div class="field_form">
                <label>Unit Kerja : </label>
                <select name="unit_kerja" class="required">
                    <option value="">Pilih Fakultas</option>
                    {foreach $data_unit_kerja as $f}
                        <option value="{$f.ID_UNIT_KERJA}" {if $f.ID_UNIT_KERJA==$program.ID_UNIT_KERJA}selected="true"{/if}>{$f.NM_UNIT_KERJA}</option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Kode Program : </label>
                <input type="text" size="10" maxlength="10" name="kode" value="{$program.KODE_PROGRAM_KERJA}" class="required"/>
            </div>
            <div class="field_form">
                <label>Periode : </label>
                <select name="periode" class="required">
                    <option value="">Pilih Periode</option>
                    {foreach $data_periode as $per}
                        <option value="{$per.ID_PERIODE}" {if $per.ID_PERIODE==$program.ID_PERIODE}selected="true"{/if}>{$per.NAMA_PERIODE} ({$per.AWAL_PERIODE} - {$per.AKHIR_PERIODE})</option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Tujuan Renstra: </label>
                <select id="tujuan" name="tujuan">
                    <option>Pilih Tujuan</option>
                    {foreach $data_tujuan as $tuj}
                        <option value="{$tuj.ID_TUJUAN_RENSTRA}" {if $program.ID_TUJUAN_RENSTRA==$tuj.ID_TUJUAN_RENSTRA}selected="true"{/if}>{$tuj.TUJUAN_RENSTRA|substr:0:70} ......</option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Program Utama : </label>
                <select id="program_utama" name="program_utama" class="required">
                    <option value="">Pilih Program</option>
                </select>
            </div>
            <div class="field_form">
                <label>Program Kerja: </label>
                <textarea name="program_kerja" class="required" cols="40">{$program.PROGRAM_KERJA}</textarea>
            </div>
            <div class="field_form">
                <label>Keterangan : </label>
                <textarea name="keterangan" cols="40">{$program.KETERANGAN}</textarea>
            </div>
            <div class="bottom_field_form">
                <a href="sasaran_kerja_pegawai.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$program.ID_PROGRAM_KERJA}"/>
                <input type="hidden" name="mode" value="edit"/>
                <input type="submit" class="button" value="Update"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">Sasaran Kerja Pegawai Hapus</div>
    <form action="sasaran_kerja_pegawai.php" method="post">
        <fieldset style="width: 98%">
            <legend>Hapus Program Kerja</legend>
            <div class="field_form">
                <label>Kode Program : </label>
                {$program.KODE_PROGRAM_KERJA}
            </div>
            <div class="field_form">
                <label>Fakultas : </label>
                {$program.NM_UNIT_KERJA}
            </div>
            <div class="field_form">
                <label>Program Kerja: </label>
                {$program.PROGRAM_KERJA}
            </div>
            <div class="bottom_field_form">
                Apakah anda yakin menghapus item ini ? <br/>
                <a href="sasaran_kerja_pegawai.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$program.ID_PROGRAM_KERJA}"/>
                <input type="hidden" name="mode" value="delete"/>
                <input type="submit" class="button" value="Ya"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">Sasaran Kerja Pegawai</div>
    <table style="width: 98%">
        <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Unit Kerja</th>
            <th>Rencana Strategis</th>
            <th>Program Utama</th>
            <th>Periode</th>
            <th>Sasaran Kerja Pegawai</th>
            <!--<th>Keterangan</th>-->
            <th>Status</th>
            <th style="width: 120px" class="center">Operasi</th>
        </tr>
        {foreach $data_proker as $pr}
            <tr>
                <td>{$pr@index+1}</td>
                <td>{$pr.KODE_PROGRAM_KERJA}</td>
                <td>{$pr.NM_UNIT_KERJA}</td>
                <td>{$pr.TUJUAN_RENSTRA}</td>
                <td>{$pr.PROGRAM_UTAMA}</td>
                <td>{$pr.NAMA_PERIODE} <span style="color: green;font-size: 11px">({$pr.AWAL_PERIODE} - {$pr.AKHIR_PERIODE})</span></td>
                <td>{$pr.PROGRAM_KERJA}</td>
                <!--<td>{$pr.KETERANGAN}</td>-->
                <td>
                    {if $pr.STATUS==1}
                        <span style="color: red;font-size: 12px">Tidak Disetujui</span>
                    {else}
                        <span style="color: green;font-size: 12px">Disetujui</span>
                    {/if}
                </td>
                <td>
                    <a class="button" href="sasaran_kerja_pegawai.php?mode=edit&id={$pr.ID_PROGRAM_KERJA}">Edit</a>
                    <a class="button" href="sasaran_kerja_pegawai.php?mode=delete&id={$pr.ID_PROGRAM_KERJA}">Delete</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="10" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="10" class="center">
                <a href="sasaran_kerja_pegawai.php?mode=tambah" class="button">Tambah</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
        $('form').validate();
        $('#tujuan').change(function(){
            $.ajax({
                type:'post',
                url:'GetProgramUtama.php',
                data:'id_tujuan='+$(this).val(),
                success:function(data){
                    $('#program_utama').html(data);
                }
            })
        })
    </script>
{/literal}