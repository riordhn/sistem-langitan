<html>
    <head>
        <title>Sasaran Kerja Pegawai</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <link rel="stylesheet" type="text/css" href="../../css/reset.css" />
        <link rel="stylesheet" type="text/css" href="../../css/text.css" />

        <link rel="stylesheet" type="text/css" href="../../css/skp.css" />

        <script type="text/javascript" src="../../js/jquery/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="../../js/jquery/jquery-migrate-1.2.1.min.js"></script>

        
        <script type="text/javascript" src="../../js/jquery/jquery-ui-1.9.2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui/jquery-ui.min.css" />
        
        <!--
        <link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" media="all" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
        -->
        <script type="text/javascript" src="../../js/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="../../js/jquery-validation/additional-methods.js"></script>

        <script type="text/javascript" src="../../api/js/pnotify/pnotify.custom.js"></script>
        <link href="../../api/js/pnotify/pnotify.custom.css" media="all" rel="stylesheet" type="text/css" />
        <!--
        <link rel="stylesheet" type="text/css" href="../../css/lakip-style.css" />
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-ppm.custom.css" />
        -->

        <!--
        <script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        -->
        
        <link rel="shortcut icon" href="../../img/icon.ico" />

        <style>
            .content { font-size: 13px; }

            .content .center_title_bar {
                background: #233D0E;
                border-radius: 5px;
                color: #fff;
                font-size: 14px;
                font-weight: bold;
                margin: 0px -5px 20px -5px;
                padding: 10px;
                text-transform: uppercase;
            }
            .content h1 { font-size: 20px; }
            .content h2 { font-size: 18px; }
            .content h3 { font-size: 16px; }
            .content h4 { font-size: 14px; }

            .content table {
                width: auto;
            }

            .content table caption { 
                padding: 2px; 
                font-size: 14px;
                font-weight: bold;
            }

            .content table tr.row1 { background-color: #eee; }
            .content table tr.row2 { background-color: #fff; }
            .content table tr.row3 { }

            .content table tr th {
                background: #233D0E;
                border: 1px solid #969BA5;
                color: #fff;
                font-weight: bold;
                padding: 10px;
                text-transform :uppercase;
            }

            .content table tr td {
                border: 1px solid #969BA5;
                padding: 8px;
                vertical-align: middle;
            }
            .content tr.even td 
            {
                background-color:#f0f0f0;
            }

            .content input[type=text],textarea{
                border-bottom: 1px solid #233D0E;
                border-top:none;
                border-right:none;
                border-left:none;
                padding:3px;
            }

            .content fieldset{
                padding:12px;
                border:1px solid #c0c0c0;
                -webkit-border-radius: 7px;
                -moz-border-radius: 7px;
                border-radius: 7px;
            }
            .content legend{
                color:#233D0E;
                font-size:14px;
                padding:2px;
                font-weight:bold;
                text-transform:uppercase;
            }
            .content div.field_form{
                margin-bottom: 15px;
                padding: 8px;
                display: block;	
            }
            .content div.bottom_field_form{
                clear: both;
                padding-top: 20px;
                text-align: center;
            }
            .content div.field_form  label:not(.error){
                float:left;
                width:35%;
                color:grey;
                font-weight:bold;
                text-align:right;
                margin:0px 15px 0 0;
            }
            .content input[type=text]:focus,textarea:focus{
                background: #ededed;
            }

            .content table tr td.title {
                background: #c3cbd4;
                padding: 8px;
            }

            .content a.button, input.button , button.button{
                margin:9px 1px;
                padding:6px;
                background-color:#233D0E;
                color: white;
                -webkit-border-radius: 7px;
                -moz-border-radius: 7px;
                border-radius: 7px;
                text-decoration:none;
                border: none;
                font-size:13px;
                display:inline-block;
            }
            .content a.button{
                padding:7px;
            }
            .content a.button:hover, input.button:hover ,button.button:hover{
                color: #cee3ff;
            }

            .small { font-size: 80%; }
            .center {
                text-align: center;
            }
            .middle {
                vertical-align: middle;
            }
            .cursor-pointer {
                cursor: pointer;
            }

            .content button {
                cursor: pointer;
            }
            .content a {
                color: #008000;
            }
            .content a:hover {
                color: #00aa00;
            }
            .content a:active {
                color: #00bb00;
            }
            .content pre {
                font-size: 11px;
            }
            .anchor-span {
                color: #008000;
                cursor: pointer;
                text-decoration: underline;
            }
            .anchor-span:hover {
                color: #00aa00;
            }

            .anchor-black {
                color: #000 !important;
                text-decoration: none;
            }
            .anchor-black:hover {
                color: #000 !important;
                text-decoration: underline;
            }

            /* FORM CONTENT */
            .content input[type=submit], .radio {
                cursor: pointer;
            }
            label.error {
                color: #c00;
                font-size: 12px;
                display:block;
            }
            input[type=text].error,textarea.error{
                border-bottom:1px solid red;
                background:#ffeaed;
            }

            .data-kosong{
                color:red;
                text-align:center;
            }
            .total{
                background-color:#c9f797;
                font-weight:bold;
            }
        </style>
    </head>
    <body>
        <table class="clear-margin-bottom">
            <colgroup>
                <col />
                <col class="main-width"/>
                <col />
            </colgroup>
            <thead>
                <tr>
                    <td class="header-left"></td>
                    <td class="header-center"></td>
                    <td class="header-right"></td>
                </tr>
                <tr>
                    <td class="tab-left"></td>
                    <td class="tab-center">
                        <ul>
                            <!-- Untuk Menampilkan Menu Utama  -->
                            {foreach $modul_set as $m}
                                {if $m.AKSES == 1}
                                    <li><a href="#{$m.NM_MODUL}!{$m.PAGE}" class="nav">{$m.TITLE}</a></li>
                                    <li class="divider"></li>
                                    {/if}
                                {/foreach}
                            <li><a class="disable-ajax" href="../../logout.php">Logout</a></li>
                            <li style="float:right;text-decoration: none;">
                                <a>
                                    <span style="color:#FFF;">Selamat Datang {$name}</span>
                                </a>
                            </li>                            
                        </ul>
                    </td>
                    <td class="tab-right">

                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="body-left">&nbsp;</td>
                    <td class="body-center">
                        <table class="content-table">
                            <colgroup>
                                <col />
                                <col />
                            </colgroup>
                            <tr>
                                <td colspan="2" id="breadcrumbs" class="breadcrumbs" ></td>
                            </tr>
                            <tr>
                                <td id="menu" class="menu"></td>
                                <td id="content" class="content">Loading data...</td>
                            </tr>
                        </table>
                    </td>
                    <td class="body-right">&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td class="foot-left">&nbsp;</td>
                    <td class="foot-center">
                        <div class="footer-nav">
                            <a href="">Home</a> | <a href="">About</a> | <a href="">Sitemap</a> | <a href="">RSS</a> | <a href="">Contact Us</a>
                        </div>
                        <div class="footer">Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU <br />oleh <a target="_blank" href="http://unair.ac.id" class="disable-ajax">Universitas Airlangga</a></div>
                    </td>
                    <td class="foot-right">&nbsp;</td>
                </tr>
            </tfoot>
        </table>

        <!-- Loading Javascript di akhir baris -->

        <script type="text/javascript">
            var defaultRel = 'account';
            var defaultPage = 'account.php';
        </script>
        <script type="text/javascript" src="../../js/cybercampus.ajax-1.0.js"></script>

        {literal}
            <!-- Google Analytics memakai akun fathoni@staf.unair.ac.id -->
            
            <script>
                /*
                (function(i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function() {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                ga('create', 'UA-41992721-1', 'unair.ac.id');
                ga('send', 'pageview');
                */
            </script>
        {/literal}
    </body>
</html>