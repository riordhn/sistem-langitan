<div id="gridview">
    <div class="center_title_bar">
        Progres Sasaran Kerja
    </div>
    <table style="width: 98%">
        <tr>
            <th>No</th>
            <th>Periode</th>
            <th>Sasaran Kerja Seksi Sub Unit</th>
            <th>Sasaran Kerja Pegawai</th>
            <th style="width: 120px" class="center">Aksi</th>
        </tr>
        {foreach $model as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>{$data.NAMA_PERIODE} <span style="color: green;font-size: 11px">({$data.AWAL_PERIODE} - {$data.AKHIR_PERIODE})</span></td>

                <td>{$data.SASARAN_KERJA_SEKSI}</td>
                <td>{$data.SASARAN_KERJA}</td>
                <td>
                    <a class="button" href="{$url_update}{$data.ID_SKP_PEGAWAI}">Edit</a>
                    <a id="btn-delete-{$data.ID_SKP_PEGAWAI}" class="button" href="#skp_pegawai.php?delete&id={$data.ID_SKP_PEGAWAI}">Delete</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="10" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="10" class="center">
                <a href="{$url_create}" class="button">Tambah</a>
            </td>
        </tr>
    </table>
</div>