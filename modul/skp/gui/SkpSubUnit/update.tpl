<div class="center_title_bar">Sasaran Kerja Sub Unit Edit</div>

{foreach $model as $data}
    <form id="form" action="skp_sub_unit.php?update&id={$data.ID_SKP_SUB_UNIT}" method="post">
        <fieldset style="width: 98%">
            <legend>Tambah Sasaran Kerja Sub Unit</legend>
            <div class="field_form">
                <label>Unit Kerja : </label>
                <select id="unit_kerja" name="unit_kerja" class="required">
                    <!--
                    <option value="">Pilih Unit Kerja</option>
                    -->
                    {foreach $data_unit_kerja as $f}
                        <option value="{$f.ID_UNIT_KERJA}" {if $f.ID_UNIT_KERJA==$data.ID_UNIT_KERJA}selected="true"{/if}>{$f.NM_UNIT_KERJA}</option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Kode Program : </label>
                <input type="text" size="10" maxlength="10" name="kode" class="required" value="{$data.KODE_PROGRAM_KERJA}"/>
            </div>
            <div class="field_form">
                <label>Periode : </label>
                <select name="periode" class="required">
                    <option value="">Pilih Periode</option>
                    {foreach $data_periode as $per}
                        <option value="{$per.ID_PERIODE}" {if $per.ID_PERIODE==$data.ID_PERIODE}selected="true"{/if}>{$per.NAMA_PERIODE} ({$per.AWAL_PERIODE} - {$per.AKHIR_PERIODE})</option>
                    {/foreach}
                </select>
            </div>

            <div class="field_form">
                <label>Sasaran Kerja Unit : </label>
                <select name="program_kerja" class="required">
                    {foreach $lakip_program_kerja as $lpk}
                        <option value="{$lpk.ID_PROGRAM_KERJA}" {if $data.ID_PROGRAM_KERJA==$lpk.ID_PROGRAM_KERJA}selected="true"{/if}>{$lpk.PROGRAM_KERJA|substr:0:70} ......</option>
                    {/foreach}
                </select>
            </div>

            <div class="field_form">
                <label>Sasaran Kerja Sub Unit: </label>
                <textarea name="sasaran_kerja" class="required" cols="40">{$data.SASARAN_KERJA}</textarea>
            </div>

            <div class="field_form">
                <label>Keterangan : </label>
                <textarea name="keterangan" cols="40">{$data.KETERANGAN}</textarea>
            </div>

            <div class="bottom_field_form">
                <a href="skp_sub_unit.php?admin" class="button">Kembali</a>
                <input type="hidden" name="id" value="{$data.ID_SKP_SUB_UNIT}"/>
                <input id="btn-save" type="submit" class="button" value="Simpan"/>
            </div>

        </fieldset>
    </form>
{/foreach}

{literal}
<script type="text/javascript">
    $(function() {
//hang on event of form with id=myform


        $("#form").submit(function(e) {

            //prevent Default functionality
            e.preventDefault();

            //get the action-url of the form
            var actionurl = e.currentTarget.action;

            //do your own request an handle the results
            $.ajax({
                url: actionurl,
                type: 'post',
                //dataType: 'json',
                data: $("#form").serialize(),
                success: function(data) {
                    var obj = $.parseJSON(data);
                    new PNotify(obj);
                }
            });

            return false;

        });
    });
</script>

{/literal}