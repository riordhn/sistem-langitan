<div class="center_title_bar">Sasaran Kerja Sub Unit Tambah</div>
<form id="form" action="skp_sub_unit.php?create" method="post">
    <fieldset style="width: 98%">
        <legend>Tambah Sasaran Kerja Sub Unit</legend>
        <div class="field_form">
            <label>Unit Kerja : </label>
            <select id="unit_kerja" name="unit_kerja" class="required">
                <option value="">Pilih Unit Kerja</option>
                {foreach $data_unit_kerja as $f}
                    <option value="{$f.ID_UNIT_KERJA}">{$f.NM_UNIT_KERJA}</option>
                {/foreach}
            </select>
        </div>
        <div class="field_form">
            <label>Kode Program : </label>
            <input type="text" size="10" maxlength="10" name="kode" class="required"/>
        </div>
        <div class="field_form">
            <label>Periode : </label>
            <select name="periode" class="required">
                <option value="">Pilih Periode</option>
                {foreach $data_periode as $per}
                    <option value="{$per.ID_PERIODE}">{$per.NAMA_PERIODE} ({$per.AWAL_PERIODE} - {$per.AKHIR_PERIODE})</option>
                {/foreach}
            </select>
        </div>

        <div class="field_form">
            <label>Sasaran Kerja Unit : </label>
            <select id="program_kerja" name="program_kerja" class="required">
                <option value="">Pilih Sasaran Kerja Unit</option>
            </select>
        </div>

        <div class="field_form">
            <label>Sasaran Kerja Sub Unit: </label>
            <textarea name="sasaran_kerja" class="required" cols="40"></textarea>
        </div>

        <div class="field_form">
            <label>Keterangan : </label>
            <textarea name="keterangan" cols="40"></textarea>
        </div>

        <div class="bottom_field_form">
            <a href="skp_sub_unit.php?admin" class="button">Kembali</a>
            <input type="hidden" name="mode" value="tambah"/>
            <input type="submit" class="button" value="Simpan"/>
        </div>

    </fieldset>
</form>
{literal}
    <script>
        $('form').validate();
        $('#unit_kerja').change(function() {
            $.ajax({
                type: 'get',
                url: 'skp_sub_unit.php',
                data: 'id_unit_kerja=' + $(this).val(),
                success: function(data) {
                    $('#program_kerja').html(data);
                }
            })
        });

    </script>
    
{/literal}
{literal}
<script type="text/javascript">
    $(function() {
//hang on event of form with id=myform


        $("#form").submit(function(e) {

            //prevent Default functionality
            e.preventDefault();

            //get the action-url of the form
            var actionurl = e.currentTarget.action;

            //do your own request an handle the results
            $.ajax({
                url: actionurl,
                type: 'post',
                //dataType: 'json',
                data: $("#form").serialize(),
                success: function(data) {
                    var obj = $.parseJSON(data);
                    new PNotify(obj);
                }
            });

            return false;

        });
    });
</script>

{/literal}