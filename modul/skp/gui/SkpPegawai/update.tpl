<div class="center_title_bar">Sasaran Kerja Sub Unit Update</div>


{foreach $model as $data}

    <form id="form" action="skp_pegawai.php?update" method="post">
        <fieldset style="width: 98%">
            <legend>Update Sasaran Kerja Pegawai</legend>

            <div class="field_form">
                <label>Periode : </label>
                <select name="periode" class="required">
                    <option value="">Pilih Periode</option>
                    {foreach $model_periode as $per}
                        <option value="{$per.ID_PERIODE}" {if $per.ID_PERIODE==$data.ID_PERIODE}selected="true"{/if}>{$per.NAMA_PERIODE} ({$per.AWAL_PERIODE} - {$per.AKHIR_PERIODE})</option>
                    {/foreach}
                </select>
            </div>

            <div class="field_form">
                <label>Sasaran Kerja Seksi Sub Unit : </label>

                <select id="skp_seksi_sub_unit" name="skp_seksi_sub_unit" class="required">
                    {foreach $model_skp_seksi_sub_unit as $sunit}
                        <option value="{$sunit.ID_SKP_SEKSI_SUB_UNIT}" {if $data.ID_SKP_SEKSI_SUB_UNIT==$sunit.ID_SKP_SEKSI_SUB_UNIT}selected="true"{/if}>{$sunit.SASARAN_KERJA|substr:0:70} </option>
                    {/foreach}
                </select>

            </div>

            <div class="field_form">
                <label>Sasaran Kerja Pegawai: </label>
                <textarea name="sasaran_kerja" class="required" cols="40">{$data.SASARAN_KERJA}</textarea>
            </div>

            <div class="field_form">
                <label>Kuantitas/Output : </label>
                <input id="SkpPegawai_kuantitas" name="kuantitas" value="{$data.KUANTITAS}" class="required" />
            </div>

            <div class="field_form">
                <label>Angka Kredit : </label>
                <input id="SkpPegawai_angka_kredit" name="angka_kredit" value="{$data.ANGKA_KREDIT}" class="required" />
            </div>

            <div class="field_form">
                <label>Biaya : </label>
                <input id="SkpPegawai_biaya" name="biaya" value="{$data.BIAYA}" class="required" />
            </div>

            <div class="field_form">
                <label>Tanggal Mulai : </label>
                <input id="tanggal_mulai" name="tanggal_mulai" value="{$data.TANGGAL_MULAI|date_format:'%d/%m/%Y'}" class="required" />
            </div>

            <div class="field_form">
                <label>Tanggal Selesai : </label>
                <input id="tanggal_selesai" name="tanggal_selesai" value="{$data.TANGGAL_SELESAI|date_format:'%d/%m/%Y'}" class="required" />
            </div>

            <div class="field_form">
                <label>Keterangan : </label>
                <textarea name="keterangan" cols="40">{$data.KETERANGAN}</textarea>
            </div>

            <div class="bottom_field_form">
                <a href="skp_pegawai.php?admin" class="button">Kembali</a>
                <input type="hidden" name="id" value="{$data.ID_SKP_PEGAWAI}"/>
                <input id="btn-save" type="submit" class="button" value="Simpan"/>
            </div>

        </fieldset>
    </form>
{/foreach}

{literal}
    <script type="text/javascript">
        $(function() {
            //hang on event of form with id=myform

            $("#tanggal_mulai").datepicker();
            $("#tanggal_selesai").datepicker();

            $("#form").submit(function(e) {

                //prevent Default functionality
                e.preventDefault();

                //get the action-url of the form
                var actionurl = e.currentTarget.action;

                //do your own request an handle the results
                $.ajax({
                    url: actionurl,
                    type: 'post',
                    //dataType: 'json',
                    data: $("#form").serialize(),
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        new PNotify(obj);
                    }
                });

                return false;

            });
        });
    </script>

{/literal}