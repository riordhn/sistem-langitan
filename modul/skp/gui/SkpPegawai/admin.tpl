<div id="gridview">
    <div class="center_title_bar">Sasaran Kerja Seksi Sub Unit</div>
    <table style="width: 98%">
        <tr>
            <th>No</th>
            <th>Periode</th>
            <th>Sasaran Kerja Seksi Sub Unit</th>
            <th>Sasaran Kerja Pegawai</th>
            <th style="width: 120px" class="center">Aksi</th>
        </tr>
        {foreach $model as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>{$data.NAMA_PERIODE} <span style="color: green;font-size: 11px">({$data.AWAL_PERIODE} - {$data.AKHIR_PERIODE})</span></td>

                <td>{$data.SASARAN_KERJA_SEKSI}</td>
                <td>{$data.SASARAN_KERJA}</td>
                <td>
                    <a class="button" href="skp_pegawai.php?view&id={$data.ID_SKP_PEGAWAI}" >View</a>
                    <a class="button" href="{$url_update}{$data.ID_SKP_PEGAWAI}">Edit</a>
                    <a id="btn-delete-{$data.ID_SKP_PEGAWAI}" class="button" href="#skp_pegawai.php?delete&id={$data.ID_SKP_PEGAWAI}">Delete</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="10" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="10" class="center">
                <a href="{$url_create}" class="button">Tambah</a>
            </td>
        </tr>
    </table>
    <div id="dialog-confirm" title="Delete confirmation!" style="display:none;">
        <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
    </div>


    <script>
        $(document).ready(function() {


        {foreach $model as $data}
            $('#btn-delete-' +{$data.ID_SKP_PEGAWAI} + "").click(function() {

                var id = {$data.ID_SKP_PEGAWAI};
            {literal}
                        var data = {"id": id};
            {/literal}

                        $("#dialog-confirm").dialog({
                            resizable: false,
                            height: 200,
                            modal: true,
                            buttons: {
                                "Delete": function() {
                                    $.ajax({
                                        url: "skp_pegawai.php?delete",
                                        type: 'post',
                                        data: data,
                                        success: function(data) {
                                            var obj = $.parseJSON(data);
                                            new PNotify(obj);

                                            $("#gridview").load("skp_pegawai.php?admin&ajax=true");
                                        }
                                    });
                                    $(this).dialog("close");
                                },
                                Cancel: function() {
                                    $(this).dialog("close");
                                }
                            }
                        });
                        return false;
                    });
        {/foreach}

                });
    </script>
</div>
