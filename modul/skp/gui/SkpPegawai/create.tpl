<div class="center_title_bar">Sasaran Kerja Pegawai Tambah</div>
<form id="form" action="skp_pegawai.php?create" method="post">
    <fieldset style="width: 98%">
        <legend>Tambah Sasaran Kerja Pegawai</legend>
        <div class="field_form">
            <label>Unit Kerja : </label>
            <select id="unit_kerja" name="unit_kerja" class="required">
                <option value="">Pilih Unit Kerja</option>
                {foreach $model_unit_kerja as $f}
                    <option value="{$f.ID_UNIT_KERJA}">{$f.NM_UNIT_KERJA}</option>
                {/foreach}
            </select>
        </div>

        <!--
        <div class="field_form">
            <label>Kode Program : </label>
            <input type="text" size="10" maxlength="10" name="kode" class="required"/>
        </div>
        -->

        <div class="field_form">
            <label>Periode : </label>
            <select name="periode" class="required">
                <option value="">Pilih Periode</option>
                {foreach $model_periode as $per}
                    <option value="{$per.ID_PERIODE}">{$per.NAMA_PERIODE} ({$per.AWAL_PERIODE} - {$per.AKHIR_PERIODE})</option>
                {/foreach}
            </select>
        </div>

        <div class="field_form">
            <label>Sasaran Kerja Seksi Sub Unit : </label>
            <select id="skp_seksi_sub_unit" name="skp_seksi_sub_unit" class="required">
                <option value="">Pilih Sasaran Kerja Seksi Sub Unit</option>
            </select>
        </div>

        <div class="field_form">
            <label>Sasaran Kerja Pegawai: </label>
            <textarea id="SkpPegawai_sasaran_kerja" name="sasaran_kerja" class="required" cols="40"></textarea>
        </div>

        <div class="field_form">
            <label>Kuantitas/Output : </label>
            <input id="SkpPegawai_kuantitas" name="kuantitas" class="required" />
        </div>
            
        <div class="field_form">
            <label>Angka Kredit : </label>
            <input id="SkpPegawai_angka_kredit" name="angka_kredit" class="required" />
        </div>
            
        <div class="field_form">
            <label>Biaya : </label>
            <input id="SkpPegawai_biaya" name="biaya" class="required" />
        </div>
            
        <div class="field_form">
            <label>Tanggal Mulai : </label>
            <input id="SkpPegawai_tanggal_mulai" name="tanggal_mulai" class="required" />
        </div>

        <div class="field_form">
            <label>Tanggal Selesai : </label>
            <input id="SkpPegawai_tanggal_selesai" name="tanggal_selesai" class="required" />
        </div>

        <div class="field_form">
            <label>Keterangan : </label>
            <textarea id="SkpPegawai_keterangan" name="keterangan" cols="40"></textarea>
        </div>

        <div class="bottom_field_form">
            <a href="skp_pegawai.php?admin" class="button">Kembali</a>
            <input type="hidden" name="mode" value="create"/>
            <input type="submit" class="button" value="Simpan"/>
        </div>

    </fieldset>
</form>
{literal}
    <script>
        $('form').validate();
        $('#unit_kerja').change(function() {
            $.ajax({
                type: 'get',
                url: 'skp_pegawai.php',
                data: 'id_unit_kerja=' + $(this).val(),
                success: function(data) {
                    $('#skp_seksi_sub_unit').html(data);
                }
            })
        });

    </script>

{/literal}
{literal}
    <script type="text/javascript">
        $(function() {

            $("#SkpPegawai_tanggal_mulai").datepicker();
            $("#SkpPegawai_tanggal_selesai").datepicker();

            $("#form").submit(function(e) {

                //prevent Default functionality
                e.preventDefault();
                //get the action-url of the form
                var actionurl = e.currentTarget.action;
                //do your own request an handle the results
                $.ajax({
                    url: actionurl,
                    type: 'post',
                    data: $("#form").serialize(),
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        new PNotify(obj);
                    }
                });

                return false;

            });
        });
    </script>

{/literal}