<?php
include 'config.php';

if ($user->Role() == AUCC_ROLE_ARSIP)
{
	// force password to ISO27001:2009
	if($_SESSION['standar_iso']=='no'){
        header("Location: ../../login.php?mode=ltp-null");
        exit();
    }

	// data menu dari $user
    $smarty->assign('modul_set', $user->MODULs);

    $smarty->display("index.tpl");
}
else
{
    header("location: /logout.php");
    exit();
}
?>
