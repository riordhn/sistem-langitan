<?php
include("config.php");

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'update-sekolah')
    {
        $db->Parse("update calon_mahasiswa_sekolah set id_sekolah_asal = :id_sekolah_asal where id_c_mhs = :id_c_mhs");
        $db->BindByName('id_c_mhs', post('id_c_mhs'));
        $db->BindByName('id_sekolah_asal', post('id_sekolah_asal'));
        $db->Execute();

		exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $id_provinsi = get('id_provinsi', '');
        $id_kota = get('id_kota', '');
		
		$limit_awal = get('limit_awal', 0);
		$limit_akhir = get('limit_akhir', 1000);
        
        $provinsi_set = $db->QueryToArray("
            select p.id_provinsi, nm_provinsi,
                (select count(cms.id_c_mhs) from calon_mahasiswa_sekolah cms
                 join calon_mahasiswa_sekolah_snmptn cmss on cmss.id_c_mhs = cms.id_c_mhs
                 where cms.id_sekolah_asal is not null and cmss.id_provinsi = p.id_provinsi) terisi,
                (select count(cms.id_c_mhs) from calon_mahasiswa_sekolah cms
                 join calon_mahasiswa_sekolah_snmptn cmss on cmss.id_c_mhs = cms.id_c_mhs
                 where cmss.id_provinsi = p.id_provinsi) total
            from provinsi p
            where p.id_negara = 1
            order by nm_provinsi");
        $smarty->assign('provinsi_set', $provinsi_set);
        
        if ($id_provinsi != '')
        {
            $kota_set = $db->QueryToArray("
                select k.*,
                    (select count(cms.id_c_mhs) from calon_mahasiswa_sekolah cms
                    join calon_mahasiswa_sekolah_snmptn cmss on cmss.id_c_mhs = cms.id_c_mhs
                    where cms.id_sekolah_asal is not null and cmss.id_kota = k.id_kota) terisi,
                    (select count(cms.id_c_mhs) from calon_mahasiswa_sekolah cms
                    join calon_mahasiswa_sekolah_snmptn cmss on cmss.id_c_mhs = cms.id_c_mhs
                    where cmss.id_kota = k.id_kota) total
                from kota k
                where k.id_provinsi = {$id_provinsi}
                order by nm_kota");
            $smarty->assign('kota_set', $kota_set);
        }
        
        if ($id_provinsi != '' && $id_kota != '')
        {
			if (!in_array($id_kota, array(217, 195)))
			{
				$cms_set = $db->QueryToArray("
					select cmss.id_c_mhs, cmss.no_ujian, sekolah, cms.id_sekolah_asal from calon_mahasiswa_sekolah_snmptn cmss
					join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmss.id_c_mhs
					where cmss.id_kota = {$id_kota}
					order by sekolah, no_ujian");
			}
			else
			{
				$cms_set = $db->QueryToArray("
					SELECT * FROM ( SELECT r.*, ROWNUM as row_number FROM (
						select cmss.id_c_mhs, cmss.no_ujian, sekolah, cms.id_sekolah_asal from calon_mahasiswa_sekolah_snmptn cmss
						join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmss.id_c_mhs
						where cmss.id_kota = {$id_kota}
						order by sekolah, no_ujian
					) r WHERE ROWNUM <= {$limit_akhir}) WHERE {$limit_awal} <= row_number");
			}
			
			$sekolah_set = $db->QueryToArray("select * from sekolah s where s.id_kota = {$id_kota} order by nm_sekolah");
			
			$smarty->assign('cms_set', $cms_set);
			$smarty->assign('sekolah_set', $sekolah_set);
        }
    }
    
    if ($mode == 'get-kota')
    {
        $id_provinsi = get('id_provinsi');
        $kota_set = $db->QueryToArray("
            select k.*,
                (select count(cms.id_c_mhs) from calon_mahasiswa_sekolah cms
                 join calon_mahasiswa_sekolah_snmptn cmss on cmss.id_c_mhs = cms.id_c_mhs
                 where cms.id_sekolah_asal is not null and cmss.id_kota = k.id_kota) terisi,
                (select count(cms.id_c_mhs) from calon_mahasiswa_sekolah cms
                 join calon_mahasiswa_sekolah_snmptn cmss on cmss.id_c_mhs = cms.id_c_mhs
                 where cmss.id_kota = k.id_kota) total
            from kota k
            where k.id_provinsi = {$id_provinsi}
            order by nm_kota");
        
        echo "<option value=\"\">--</option>";
		
        foreach ($kota_set as $k)
        {
			if (intval($k['TOTAL']) > 0)
				$persentase = number_format(($k['TERISI'] / $k['TOTAL']) * 100, 1);
			else
				$persentase = number_format(0, 1);
            echo "<option value=\"{$k['ID_KOTA']}\">{$k['NM_KOTA']} ({$k['TERISI']}/{$k['TOTAL']}) -- {$persentase}%</option>";
        }
		
        exit();
    }
}

$smarty->display("sekolah-psnmptn/{$mode}.tpl");
?>
