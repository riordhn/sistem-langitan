<div class="center_title_bar">Master Sekolah PPMB</div>

<form action="sekolah-ppmb.php" method="get" id="filter-form">
    <table>
        <tr>
            <th colspan="4">Filter Kota</th>
        </tr>
        <tr>
            <td>Provinsi</td>
            <td>
                <select name="id_provinsi">
                    <option value="">--</option>
                {foreach $provinsi_set as $p}
                    {$persentase = number_format(($p.TERISI / $p.TOTAL) * 100, 1)}
                    <option value="{$p.ID_PROVINSI}" {if $p.ID_PROVINSI == $smarty.get.id_provinsi}selected="selected"{/if}
                            {if $persentase > 90 and $p.TOTAL > 0}style="background-color: #afa;"{/if}>{$p.NM_PROVINSI} ({$p.TERISI}/{$p.TOTAL}) -- {$persentase}%</option>
                {/foreach}
                </select>
                <script type="text/javascript">
                    $('select[name="id_provinsi"]').change(function() {
                        $.ajax({
                            url: 'sekolah-ppmb.php',
                            type: 'GET',
                            data: 'mode=get-kota&id_provinsi='+this.value,
                            success: function(data) {
                                $('select[name="id_kota"]').html(data);
                            }
                        });
                    });
                </script>
            </td>
            <td>Kota</td>
            <td>
                <select name="id_kota">
                    <option value="">--</option>
                {foreach $kota_set as $k}
                    {$persentase = number_format(($k.TERISI / $k.TOTAL) * 100, 1)}
                    <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $smarty.get.id_kota}selected="selected"{/if}
                            {if $persentase > 90 and $k.TOTAL > 0}style="background-color: #afa;"
                            {else if $k.TOTAL == 0}style="background-color: #aaa;"
                            {/if}>{$k.NM_KOTA} ({$k.TERISI}/{$k.TOTAL}) -- {$persentase}%</option>
                {/foreach}
                </select>
                <script type="text/javascript">
                    $('select[name="id_kota"]').change(function() {
                        $('#filter-form').submit();
                    });
                </script>
            </td>
        </tr>
    </table>
</form>
                
{if !empty($sekolah_set)}
<table>
    <tr>
        <th>No</th>
        <th>Sekolah</th>
        <th>Kode</th>
        <th>Cari PPMB</th>
    </tr>
    {foreach $sekolah_set as $s}
    <tr {if $s@index is not div by 2}class="row1"{/if}>
        <td class="center">{$s@index + 1}</td>
        <td>{$s.NM_SEKOLAH}</td>
        <td class="center"><span id="span{$s.ID_SEKOLAH}">{$s.KODE_PPMB}</span><img id="img{$s.ID_SEKOLAH}" style="display: none;" src="ajax-loader.gif" /></td>
        <td>
            <select name="ppmb{$s.ID_SEKOLAH}">
                <option value="">--</option>
            {foreach $sekolah_ppmb_set as $sp}
                <option value="{$sp.ID_SEKOLAH_PPMB}" {if $sp.KODE_SEKOLAH == $s.KODE_PPMB}selected="selected"{/if} >{if $sp.ID_SEKOLAH != ''}&blacktriangleright;{/if} {$sp.NAMA_SEKOLAH}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    {/foreach}
</table>
<script>
    $('select[name^="ppmb"]').change(function() {
        var id_sekolah = $(this).attr('name').replace('ppmb','');
        var id_sekolah_ppmb = this.value;
        var id_kota = $('select[name="id_kota"]').val();
        $.ajax({
            url: 'sekolah-ppmb.php',
            type: 'POST',
            data: 'mode=update-kode&id_sekolah='+id_sekolah+'&id_sekolah_ppmb='+id_sekolah_ppmb,
            beforeSend: function() {
                $('#img'+id_sekolah).css('display', 'block');
            },
            success: function(data) {
                if (data == 1)
                {
                    $.ajax({
                        url: 'sekolah-ppmb.php',
                        type: 'GET',
                        data: 'mode=get-kode-ppmb&id_sekolah='+id_sekolah,
                        success: function(data) {
                            $('#span'+id_sekolah).html(data);
                            $('#img'+id_sekolah).css('display', 'none');
                        }
                    });
                }
                else
                {
                    alert('Gagal Update');
                    $('#img'+id_sekolah).css('display', 'none');
                }
            }
        });
    });
</script>
{/if}