<div class="center_title_bar">Sekolah Peserta SNMPTN</div>

<form action="sekolah-psnmptn.php" method="get" id="filter-form">
    <table>
        <tr>
            <th colspan="6">Filter Kota</th>
        </tr>
        <tr>
            <td>Provinsi</td>
            <td>
                <select name="id_provinsi">
                    <option value="">--</option>
                {foreach $provinsi_set as $p}
                    {$persentase = number_format(($p.TERISI / $p.TOTAL) * 100, 1)}
                    <option value="{$p.ID_PROVINSI}" {if $p.ID_PROVINSI == $smarty.get.id_provinsi}selected="selected"{/if}>{$p.NM_PROVINSI} ({$p.TERISI}/{$p.TOTAL}) -- {$persentase}%</option>
                {/foreach}
                </select>
                <script type="text/javascript">
                    $('select[name="id_provinsi"]').change(function() {
                        $.ajax({
                            url: 'sekolah-psnmptn.php',
                            type: 'GET',
                            data: 'mode=get-kota&id_provinsi='+this.value,
                            success: function(data) {
                                $('select[name="id_kota"]').html(data);
                            }
                        });
                    });
                </script>
            </td>
            <td>Kota</td>
            <td>
                <select name="id_kota">
                    <option value="">--</option>
                {foreach $kota_set as $k}
                    {$persentase = number_format(($k.TERISI / $k.TOTAL) * 100, 1)}
                    <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $smarty.get.id_kota}selected="selected"{/if}>{$k.NM_KOTA} ({$k.TERISI}/{$k.TOTAL}) -- {$persentase}%</option>
                {/foreach}
                </select>
                <script type="text/javascript">
                    $('select[name="id_kota"]').change(function() {
                        $('#filter-form').submit();
                    });
                </script>
            </td>
			<td>Limit</td>
			<td>
				<input type="text" name="limit_awal" value="{$smarty.get.limit_awal}" size="4" />
				<input type="text" name="limit_akhir" value="{$smarty.get.limit_akhir}" size="4" />
				<input type="submit" value="OK" />
			</td>
        </tr>
    </table>
</form>
                
{if $cms_set}
<table>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Sekolah</th>
        <th>Data Master</th>
    </tr>
    {foreach $cms_set as $cms}
    <tr>
        <td class="center">{$cms@index+1}</td>
        <td>{$cms.NO_UJIAN}</td>
        <td>{$cms.SEKOLAH}</td>
        <td>
            <select name="id_sekolah_{$cms.ID_C_MHS}" id="select{$cms.ID_C_MHS}" onchange="changeSekolah('{$cms.ID_C_MHS}');">
                <option value="">--</option>
            {foreach $sekolah_set as $s}
                <option value="{$s.ID_SEKOLAH}" {if $s.ID_SEKOLAH == $cms.ID_SEKOLAH_ASAL}selected="selected"{/if}>{$s.NM_SEKOLAH}</option>
            {/foreach}
            </select>
            <img id="img{$cms.ID_C_MHS}" style="display: none;" src="ajax-loader.gif" />
        </td>
    </tr>
    {/foreach}
</table>
<script type="text/javascript">
    function changeSekolah(id_c_mhs) {
        var id_sekolah = $('#select'+id_c_mhs).val();
        
        $.ajax({
            url: 'sekolah-psnmptn.php',
            type: 'POST',
            data: 'mode=update-sekolah&id_c_mhs='+id_c_mhs+'&id_sekolah_asal='+id_sekolah,
            beforeSend: function() {
                $('#img'+id_c_mhs).css('display', 'inline');
            },
            success: function(data) {
                $('#img'+id_c_mhs).css('display', 'none');
            }
        });
    }
</script>
{/if}