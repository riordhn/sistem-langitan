<html>
    <head>
        <title>Cleansing Sekolah - PPMB Universitas Airlangga</title>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        
        <link rel="stylesheet" type="text/css" href="/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="/css/text.css" />
        <link rel="stylesheet" type="text/css" href="/css/ppmb.css" />
        <link rel="stylesheet" type="text/css" href="/css/jquery-ui-1.8.11.custom.css" />
        
        <link rel="shortcut icon" href="/img/icon.ico" />
        
		<script type="text/javascript" src="/js/?f=jquery-1.5.1.js"></script>
        <script type="text/javascript" src="/js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript">var defaultRel = 'x'; var defaultPage = 'sekolah-snmptn.php'; var id_fakultas = null;</script>
        <script type="text/javascript" src="/js/cybercampus.ajax-1.0.js"></script>
    </head>
    <body>
        <table class="clear-margin-bottom">
            <colgroup>
                <col />
                <col class="main-width"/>
                <col />
            </colgroup>
            <thead>
                <tr>
                    <td class="header-left"></td>
                    <td class="header-center"></td>
                    <td class="header-right"></td>
                </tr>
                <tr>
                    <td class="tab-left"></td>
                    <td class="tab-center">
                        <ul>
							<li><a href="#x!sekolah-snmptn.php" class="nav">Sekolah SNMPTN</a></li>
							<li class="divider"></li>
                            <li><a href="#x!sekolah-ppmb.php" class="nav">Sekolah PPMB</a></li>
							<li class="divider"></li>
                            <li><a href="#x!sekolah-psnmptn.php" class="nav">Peserta SNMPTN 2011</a></li>
							<li class="divider"></li>
                            <li><a href="#x!sekolah-prestasi.php" class="nav">Prestasi SNMPTN 2012</a></li>
							<li class="divider"></li>
                            <li><a class="disable-ajax" href="logout.php">Logout</a></li>
                        </ul>
                    </td>
                    <td class="tab-right"></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="body-left">&nbsp;</td>
                    <td class="body-center">
                        <table class="content-table">
                            <colgroup>
                                <col />
                                <col />
                            </colgroup>
                            <tr>
                                <!--<td id="menu" class="menu"></td>-->
                                <td id="content" class="content">ABC</td>
                            </tr>
                        </table>
                    </td>
                    <td class="body-right">&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td class="foot-left">&nbsp;</td>
                    <td class="foot-center">
                        <div class="footer-nav">
                            <a href="">Home</a> | <a href="">About</a> | <a href="">Sitemap</a> | <a href="">RSS</a> | <a href="">Contact Us</a>
                        </div>
                        <div class="footer">Copyright &copy; 2011 - Universitas Airlangga <br/>All Rights Reserved</div>
                    </td>
                    <td class="foot-right">&nbsp;</td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>