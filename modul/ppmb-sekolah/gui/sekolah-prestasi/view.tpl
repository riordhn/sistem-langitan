<div class="center_title_bar">Matching Prestasi Peserta SNMPTN 2012</div>

<form action="sekolah-prestasi.php" method="get" id="filter">
<table>
    <tr>
        <td>Program Studi</td>
        <td>
            <select name="id_program_studi" onchange="$('#filter').submit();">
                <option value="">--</option>
            {foreach $program_studi_set as $ps}
                <option value="{$ps.ID_PROGRAM_STUDI}" {if $smarty.get.id_program_studi == $ps.ID_PROGRAM_STUDI}selected="selected"{/if}>{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</option>
            {/foreach}
            </select>
            Pilihan Ke
            <select name="pilihan_ke" onchange="$('#filter').submit();">
                <option value="">--</option>
            {foreach $pilihan_ke as $pk}
                <option value="{$pk.ID}" {if $smarty.get.pilihan_ke == $pk.ID}selected="selected"{/if}>{$pk.ID}</option>
            {/foreach}
            </select>
        </td>
    </tr>
</table>
</form>
            
<div id="dialog-modal" title="Edit Prestasi" style="display: none">
    <input type="hidden" name="id_c_mhs" value="" />
    <input type="hidden" name="nm_file" value="" />
    <select name="id_skor_prestasi">
            <option value="">--</option>
        {foreach $skor_prestasi_set as $sp}
            <option value="{$sp.ID_SKOR_PRESTASI}">{$sp.JENIS} - {$sp.TINGKAT} - {$sp.PERINGKAT}</option>
        {/foreach}
    </select>
    <button onclick="insertPrestasi(); return false;" id="tombol">Tambah</button>
    <img id="loading" src="ajax-loader.gif" style="display: none;" />
</div>
            
<script type="text/javascript">
    function showDialog(id_c_mhs, nm_file)
    {
        // set pilihan
        $('input[name="id_c_mhs"]').val(id_c_mhs);
        $('input[name="nm_file"]').val(nm_file);
        
        // reset pilihan
        $('select[name="id_skor_prestasi"]').val('');
        
        $('#dialog-modal').dialog({
            modal: true,
            width: 500
        });
    }
    
    function insertPrestasi()
    {
        var id_c_mhs;
        id_c_mhs = $('input[name="id_c_mhs"]').val();
        var nm_file;
        nm_file = $('input[name="nm_file"]').val();
        var id_skor_prestasi;
        id_skor_prestasi = $('select[name="id_skor_prestasi"]').val();
        
        $.ajax({
            url: 'sekolah-prestasi.php',
            type: 'POST',
            data: 'mode=add-prestasi&id_c_mhs='+id_c_mhs+'&nm_file='+nm_file+'&id_skor_prestasi='+id_skor_prestasi,
            beforeSend: function() {
                $('#loading').css('display', 'inline');
                $('#tombol').css('display', 'none');
            },
            success: function(data) {
                if (data == 1)
                {
                    $.ajax({
                        url: 'sekolah-prestasi.php',
                        typ: 'GET',
                        data: 'mode=get-files&id_c_mhs='+id_c_mhs,
                        success: function(data) {
                            $('#dialog-modal').dialog('close');
                            $('#db'+id_c_mhs).text(data);
                            $('span[idx="a'+nm_file+'"]').css('display', 'none');
                            $('span[idx="d'+nm_file+'"]').css('display', 'inline');
                        }
                    });
                }
                else
                {
                    alert('Gagal menambahkan');
                }
                $('#loading').css('display', 'none');
                $('#tombol').css('display', 'inline');
            }
        });
    }
    
    function deletePrestasi(id_c_mhs, nm_file)
    {
        if (confirm('Apakah data prestasi akan dihapus ?') == true)
        {
            $.ajax({
                url: 'sekolah-prestasi.php',
                type: 'POST',
                data: 'mode=del-prestasi&nm_file='+nm_file,
                success: function(data) {
                    if (data == 1)
                    {
                        $.ajax({
                            url: 'sekolah-prestasi.php',
                            typ: 'GET',
                            data: 'mode=get-files&id_c_mhs='+id_c_mhs,
                            success: function(data) {
                                $('#dialog-modal').dialog('close');
                                $('#db'+id_c_mhs).text(data);
                                $('span[idx="a'+nm_file+'"]').css('display', 'inline');
                                $('span[idx="d'+nm_file+'"]').css('display', 'none');
                            }
                        });
                    }
                    else
                    {
                        alert('data gagal dihapus');
                    }
                }
            });
        }
    }
    
    function updateStatus(id_c_mhs)
    {
		var status = $('#status'+id_c_mhs).attr('checked');

		if (status == true)
			status = 1;
		else
			status = 0;

		$.ajax({
			url: 'sekolah-prestasi.php',
			type: 'POST',
			data: 'mode=update-status&id_c_mhs='+id_c_mhs+'&status='+status,
			beforeSend: function() {
                $('#lket'+id_c_mhs).css('display', 'block');
            },
			success: function(data) {
				if (data == 1)
                    $('#lket'+id_c_mhs).css('display', 'none');
                else
                    alert('gagal');
			}
		});
    }
    
    function updateKeterangan(id_c_mhs)
    {
        //$('#lket'+id_c_mhs).css('display', 'block');
        var keterangan = $('#ket'+id_c_mhs).val();
        
        $.ajax({
            url: 'sekolah-prestasi.php',
            type: 'POST',
            data: 'mode=update-keterangan&id_c_mhs='+id_c_mhs+'&keterangan='+keterangan,
            beforeSend: function() {
                $('#lket'+id_c_mhs).css('display', 'block');
            },
            success: function(data) {
                if (data == 1)
                    $('#lket'+id_c_mhs).css('display', 'none');
                else
                    alert('gagal');
            }
        });
    }
</script>
            
<table>
    <tr>
        <th>No</th>
        <th>No Ujian</th>
        <th>Nama</th>
        <th>Files</th>
        <th>DB</th>
        <th>File</th>
        <th>Status</th>
        <th>Catatan</th>
    </tr>
    {foreach $cmb_set as $cmb}
    {if count($cmb.FILES) > 0}
    <tr>
        <td class="center">{$cmb@index+1}</td>
        <td>{$cmb.NO_UJIAN}</td>
        <td>{$cmb.NM_C_MHS}</td>
        <td class="center">{$cmb.JUMLAH_FILE}</td>
        <td class="center" id="db{$cmb.ID_C_MHS}">{$cmb.JUMLAH_DB}</td>
        <td id="file{$cmb.ID_C_MHS}">
            {foreach $cmb.FILES as $f}
                <a href="/files/snmptn/2012/prestasi/{$f.FILE}" class="disable-ajax" target="_blank">{$f.FILE}</a> |
                
                <span class="anchor-span" idx="a{$f.FILE}" onclick="showDialog({$cmb.ID_C_MHS},'{$f.FILE}');" {if $f.STATUS != ''}style="display: none"{/if}>Add</span>
                <span class="anchor-span" idx="d{$f.FILE}" onclick="deletePrestasi({$cmb.ID_C_MHS}, '{$f.FILE}');" {if $f.STATUS == ''}style="display: none"{/if}>X</span>
                {if $f.STATUS != ''}<span>{$f.PRESTASI}</span>{/if}
                <br/>
            {/foreach}
        </td>
        <td class="center"><input type="checkbox" id="status{$cmb.ID_C_MHS}" onclick="updateStatus({$cmb.ID_C_MHS});" {if $cmb.STATUS == 1}checked="checked"{/if}/></td>
        <td>
            <textarea id="ket{$cmb.ID_C_MHS}" cols="30" onblur="updateKeterangan({$cmb.ID_C_MHS});">{$cmb.KETERANGAN}</textarea>
            <img id="lket{$cmb.ID_C_MHS}" src="ajax-loader.gif" style="display: none;" />
        </td>
    </tr>
    {/if}
    {/foreach}
</table>

