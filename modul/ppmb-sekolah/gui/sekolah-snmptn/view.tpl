<div class="center_title_bar">Master Sekolah SNMPTN</div>

<div id="announcement" style="border: 1px dashed #000; padding: 2px; margin: 5px; {if !empty($smarty.get.id_kota)}display:none;{/if}">
<h2 style="margin-bottom: 4px">Peraturan cleansing Data SMA</h2>
<ul style="margin-bottom: 4px">
    <li>Pilih Provinsi, kemudian Pilih Kota.</li>
    <li>Setelah muncul tabel sekolah, pilih pada kolom Cari SNMPTN</li>
    <li>Selalu perhatikan jenis sekolahnya, untuk <strong>SMA</strong> pilih <strong>SMA</strong>, untuk <strong>SMK</strong> pilih <strong>SMK</strong>, begitu seterusnya.</li>
</ul>
<button onclick="$('#announcement').hide('slow'); return false;">Sembunyikan</button>
</div>

<form action="sekolah-snmptn.php" method="get" id="filter-form">
    <table>
        <tr>
            <th colspan="4">Filter Kota</th>
        </tr>
        <tr>
            <td>Provinsi</td>
            <td>
                <select name="id_provinsi">
                    <option value="">--</option>
                {foreach $provinsi_set as $p}
                    {$persentase = number_format(($p.TERISI / $p.TOTAL) * 100, 1)}
                    <option value="{$p.ID_PROVINSI}" {if $p.ID_PROVINSI == $smarty.get.id_provinsi}selected="selected"{/if}>{$p.NM_PROVINSI} ({$p.TERISI}/{$p.TOTAL}) -- {$persentase}%</option>
                {/foreach}
                </select>
                <script type="text/javascript">
                    $('select[name="id_provinsi"]').change(function() {
                        $.ajax({
                            url: 'sekolah-snmptn.php',
                            type: 'GET',
                            data: 'mode=get-kota&id_provinsi='+this.value,
                            success: function(data) {
                                $('select[name="id_kota"]').html(data);
                            }
                        });
                    });
                </script>
            </td>
            <td>Kota</td>
            <td>
                <select name="id_kota">
                    <option value="">--</option>
                {foreach $kota_set as $k}
                    {$persentase = number_format(($k.TERISI / $k.TOTAL) * 100, 1)}
                    <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $smarty.get.id_kota}selected="selected"{/if}>{$k.NM_KOTA} ({$k.TERISI}/{$k.TOTAL}) -- {$persentase}%</option>
                {/foreach}
                </select>
                <script type="text/javascript">
                    $('select[name="id_kota"]').change(function() {
                        $('#filter-form').submit();
                    });
                </script>
            </td>
        </tr>
    </table>
</form>

{if !empty($sekolah_set)}
<table>
    <tr>
        <th>No</th>
        <th>Sekolah</th>
        <th>Kode</th>
        <th>Cari SNMPTN</th>
    </tr>
    {foreach $sekolah_set as $s}
    <tr {if $s@index is not div by 2}class="row1"{/if}>
        <td class="center">{$s@index + 1}</td>
        <td>{$s.NM_SEKOLAH}</td>
        <td class="center"><span id="span{$s.ID_SEKOLAH}">{$s.KODE_SNMPTN}</span><img id="img{$s.ID_SEKOLAH}" style="display: none;" src="ajax-loader.gif" /></td>
        <td>
            <select name="snmptn{$s.ID_SEKOLAH}">
                <option value="">--</option>
            {foreach $sekolah_snmptn_set as $ss}
                <option value="{$ss.ID_SEKOLAH_SNMPTN}" {if $ss.KODE_SEKOLAH == $s.KODE_SNMPTN}selected="selected"{/if} >{if $ss.ID_SEKOLAH != ''}&blacktriangleright;{/if} {$ss.NAMA_SEKOLAH}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    {/foreach}
</table>
<script>
    $('select[name^="snmptn"]').change(function() {
        var id_sekolah = $(this).attr('name').replace('snmptn','');
        var id_sekolah_snmptn = this.value;
        var id_kota = $('select[name="id_kota"]').val();
        $.ajax({
            url: 'sekolah-snmptn.php',
            type: 'POST',
            data: 'mode=update-kode&id_sekolah='+id_sekolah+'&id_sekolah_snmptn='+id_sekolah_snmptn,
            beforeSend: function() {
                $('#img'+id_sekolah).css('display', 'block');
            },
            success: function(data) {
                if (data == 1)
                {
                    $.ajax({
                        url: 'sekolah-snmptn.php',
                        type: 'GET',
                        data: 'mode=get-kode-snmptn&id_sekolah='+id_sekolah,
                        success: function(data) {
                            $('#span'+id_sekolah).html(data);
                            $('#img'+id_sekolah).css('display', 'none');
                        }
                    });
                }
                else
                {
                    alert('Gagal Update');
                    $('#img'+id_sekolah).css('display', 'none');
                }
            }
        });
    });
</script>
{/if}