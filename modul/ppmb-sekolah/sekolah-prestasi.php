<?php
include("config.php");

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'add-prestasi')
    {
        $db->Parse("insert into calon_mahasiswa_prestasi (id_c_mhs, id_skor_prestasi, nm_file) values (:id_c_mhs, :id_skor_prestasi, :nm_file)");
        $db->BindByName(':id_c_mhs', post('id_c_mhs'));
        $db->BindByName(':id_skor_prestasi', post('id_skor_prestasi'));
        $db->BindByName(':nm_file', post('nm_file'));
        $result = $db->Execute();
        
        echo ($result) ? "1" : "0";
        
        exit();
    }
    
    if (post('mode') == 'del-prestasi')
    {
        $nm_file = post('nm_file');
        $result = $db->Query("delete from calon_mahasiswa_prestasi where nm_file = '{$nm_file}'");
        
        echo ($result) ? "1" : "0";
        
        exit();
    }
    
    if (post('mode') == 'update-keterangan')
    {
        $id_c_mhs = post('id_c_mhs');
        $keterangan = post('keterangan');
        
        $result = $db->Query("update calon_mahasiswa_vprestasi set keterangan = '{$keterangan}' where id_c_mhs = {$id_c_mhs}");
        
        echo ($result) ? "1" : "0";
        
        exit();
    }
	
	if (post('mode') == 'update-status')
	{
		$id_c_mhs = post('id_c_mhs');
		$status = post('status');
		
		$result = $db->Query("update calon_mahasiswa_vprestasi set status = {$status} where id_c_mhs = {$id_c_mhs}");
		
		echo ($result) ? "1" : "0";
		
		exit();
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        // program studi pilihan
        $id_program_studi = get('id_program_studi', '');
        $pilihan_ke = get('pilihan_ke', '');
        
        $smarty->assign('program_studi_set', $db->QueryToArray("
            select id_program_studi, nm_program_studi, nm_jenjang from program_studi ps
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where kode_snmptn is not null order by nm_program_studi"));
        
        $smarty->assign('pilihan_ke', array(
            array('ID' => 1),
            array('ID' => 2)
        ));
        
        $smarty->assign('skor_prestasi_set', $db->QueryToArray("select * from skor_prestasi"));
        
        if ($id_program_studi != '' && $pilihan_ke != '')
        {
            // calon mahasiswa baru
            $cmb_set = $db->QueryToArray("
                select cmb.id_c_mhs, no_ujian, nm_c_mhs, ps1.nm_program_studi nm_program_studi1, ps2.nm_program_studi nm_program_studi2,
                    (select count(*) from calon_mahasiswa_prestasi cmp where cmp.id_c_mhs = cmb.id_c_mhs) jumlah_db,
                    cmv.status, cmv.keterangan
                from calon_mahasiswa_baru cmb
                join calon_mahasiswa_vprestasi cmv on cmv.id_c_mhs = cmb.id_c_mhs
                left join program_studi ps1 on ps1.id_program_studi = id_pilihan_1
                left join program_studi ps2 on ps2.id_program_studi = id_pilihan_2
                where id_penerimaan = 44 and id_pilihan_{$pilihan_ke} = {$id_program_studi}
                order by no_ujian");
            
            // pindah direktori aktif
            $cwd = getcwd();
            chdir("/var/www/html/files/snmptn/2012/prestasi/");
            
            // mendapatkan jumlah file prestasi
            foreach ($cmb_set as &$cmb)
            {
                $lines = array();
				exec("ls -1 {$cmb['NO_UJIAN']}-*", $lines);
                $cmb['JUMLAH_FILE'] = count($lines);
                $cmb['FILES'] = array();
                
                foreach ($lines as $line)
                {
                    $rows = $db->QueryToArray("
						select id_c_mhs, sp.jenis, sp.tingkat, sp.peringkat from calon_mahasiswa_prestasi cmp
						join skor_prestasi sp on sp.id_skor_prestasi = cmp.id_skor_prestasi
						where nm_file = '{$line}'");
                    
                    array_push($cmb['FILES'], array(
                        'FILE' => $line,
                        'STATUS' => $rows[0]['ID_C_MHS'],
						'PRESTASI' => $rows[0]['JENIS'] . " " . $rows[0]['TINGKAT'] . " " . $rows[0]['PERINGKAT']
                    ));
                }
            }
            
            // mengembalikan direktori aktif
            chdir($cwd);
            
            $smarty->assign('cmb_set', $cmb_set);
        }
    }
    
    if ($mode == 'get-files')
    {
        $id_c_mhs = get('id_c_mhs');
        
        echo $db->QuerySingle("select count(*) from calon_mahasiswa_prestasi where id_c_mhs = {$id_c_mhs}");
        exit();
    }
}

$smarty->display("sekolah-prestasi/{$mode}.tpl");
?>
