<?php
include('config.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'update-kode')
    {
        $id_sekolah_ppmb = post('id_sekolah_ppmb');
        $id_sekolah = post('id_sekolah');
        
        $db->Parse("
            update sekolah set kode_ppmb = (select kode_sekolah from sekolah_ppmb where id_sekolah_ppmb = :id_sekolah_ppmb)
            where id_sekolah = :id_sekolah");
        $db->BindByName('id_sekolah_ppmb', $id_sekolah_ppmb);
        $db->BindByName('id_sekolah', $id_sekolah);
        $result = $db->Execute();
        
        if ($result)
        {
            $db->Parse("update sekolah_ppmb set id_sekolah = :id_sekolah where id_sekolah_ppmb = :id_sekolah_ppmb");
            $db->BindByName('id_sekolah_ppmb', $id_sekolah_ppmb);
            $db->BindByName('id_sekolah', $id_sekolah);
            $result = $db->Execute();
        }
        
        echo $result ? "1" : "0";
        
        exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $id_provinsi = get('id_provinsi', '');
        $id_kota = get('id_kota', '');

        $smarty->assign('provinsi_set', $db->QueryToArray("
            select p.*,
                (select count(s.id_sekolah) from sekolah s 
                 join kota k on k.id_kota = s.id_kota
                 where k.id_provinsi = p.id_provinsi and s.kode_ppmb is not null) terisi,
                (select count(s.id_sekolah) from sekolah s 
                 join kota k on k.id_kota = s.id_kota
                 where k.id_provinsi = p.id_provinsi) total
            from provinsi p
            where p.id_negara = 1
            order by p.nm_provinsi"));

        if ($id_provinsi != '')
        {
            $kota_set = $db->QueryToArray("
                select k.*,
                    (select count(s.id_sekolah) from sekolah s where s.id_kota = k.id_kota and s.kode_ppmb is not null) terisi,
                    (select count(s.id_sekolah) from sekolah s where s.id_kota = k.id_kota) total
                from kota k
                where k.id_provinsi = {$id_provinsi}
                order by nm_kota");
            $smarty->assign('kota_set', $kota_set);
        }
        
        if ($id_provinsi != '' && $id_kota != '')
        {
            $sekolah_set = $db->QueryToArray("select * from sekolah where id_kota = {$id_kota} order by nm_sekolah");
            $smarty->assign('sekolah_set', $sekolah_set);
            
            $kode_kota_ppmb = $db->QuerySingle("select kode_ppmb from kota where id_kota = {$id_kota}");
            
            $sekolah_ppmb_set = $db->QueryToArray("select * from sekolah_ppmb where substr(kode_sekolah,1,4) = '{$kode_kota_ppmb}' order by nama_sekolah");
            $smarty->assign('sekolah_ppmb_set', $sekolah_ppmb_set);
        }
    }
    
    if ($mode == 'get-kota')
    {
        $id_provinsi = get('id_provinsi');
        $kota_set = $db->QueryToArray("
            select k.*,
                (select count(s.id_sekolah) from sekolah s where s.id_kota = k.id_kota and s.kode_ppmb is not null) terisi,
                (select count(s.id_sekolah) from sekolah s where s.id_kota = k.id_kota) total
            from kota k
            where k.id_provinsi = {$id_provinsi}
            order by nm_kota");
        
        echo "<option value=\"\">--</option>";
        foreach ($kota_set as $k)
        {
            $persentase = number_format(($k['TERISI'] / $k['TOTAL']) * 100, 1);
            
            if ($persentase > 90 && $k['TOTAL'] > 0)
                $style = "style=\"background-color:#afa;\"";
            else if ($k['TOTAL'] == 0)
                $style = "style=\"background-color:#aaa;\"";
            
            echo "<option value=\"{$k['ID_KOTA']}\" {$style}>{$k['NM_KOTA']} ({$k['TERISI']}/{$k['TOTAL']}) -- {$persentase}%</option>";
        }
        exit();
    }
    
    if ($mode == 'get-kode-ppmb')
    {
        $id_sekolah = get('id_sekolah');
        echo $db->QuerySingle("select kode_ppmb from sekolah where id_sekolah = {$id_sekolah}");
        exit();
    }
}

$smarty->display("sekolah-ppmb/{$mode}.tpl");
?>
