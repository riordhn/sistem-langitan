<?php
include('config.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'update-kode')
    {
        $id_sekolah_snmptn = post('id_sekolah_snmptn');
        $id_sekolah = post('id_sekolah');
        
        $db->Parse("
            update sekolah set kode_snmptn = (select kode_sekolah from sekolah_snmptn where id_sekolah_snmptn = :id_sekolah_snmptn)
            where id_sekolah = :id_sekolah");
        $db->BindByName('id_sekolah_snmptn', $id_sekolah_snmptn);
        $db->BindByName('id_sekolah', $id_sekolah);
        $result = $db->Execute();
        
        if ($result)
        {
            $db->Parse("update sekolah_snmptn set id_sekolah = :id_sekolah where id_sekolah_snmptn = :id_sekolah_snmptn");
            $db->BindByName('id_sekolah_snmptn', $id_sekolah_snmptn);
            $db->BindByName('id_sekolah', $id_sekolah);
            $result = $db->Execute();
        }
        
        echo $result ? "1" : "0";
        
        exit();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $id_provinsi = get('id_provinsi', '');
        $id_kota = get('id_kota', '');
        
        $where = "";
        
        if (substr($_SESSION['id'], 0, 3) == 'dsi')
        {
            $where = " and p.id_provinsi in (17,16,7,14,11,28,5,12,13,15,20,22,21,23,9,10,8)";
        }
        
        if (substr($_SESSION['id'], 0, 4) == 'ppmb')
        {
            $where = " and p.id_provinsi not in (17,16,7,14,11,28,5,12,13,15,20,22,21,23,9,10,8)";
        }
        
        $provinsi_set = $db->QueryToArray("
            select p.*,
                (select count(s.id_sekolah) from sekolah s 
                 join kota k on k.id_kota = s.id_kota
                 where k.id_provinsi = p.id_provinsi and s.kode_snmptn is not null) terisi,
                (select count(s.id_sekolah) from sekolah s 
                 join kota k on k.id_kota = s.id_kota
                 where k.id_provinsi = p.id_provinsi) total
            from provinsi p
            where p.id_negara = 1 {$where}
            order by nm_provinsi");
        $smarty->assign('provinsi_set', $provinsi_set);
        
        if ($id_provinsi != '')
        {
            $kota_set = $db->QueryToArray("
                select k.*,
                    (select count(s.id_sekolah) from sekolah s where s.id_kota = k.id_kota and s.kode_snmptn is not null) terisi,
                    (select count(s.id_sekolah) from sekolah s where s.id_kota = k.id_kota) total
                from kota k
                where k.id_provinsi = {$id_provinsi}
                order by nm_kota");
            $smarty->assign('kota_set', $kota_set);
        }
        
        if ($id_provinsi != '' && $id_kota != '')
        {
            $sekolah_set = $db->QueryToArray("select * from sekolah where id_kota = {$id_kota} order by nm_sekolah");
            $smarty->assign('sekolah_set', $sekolah_set);
            
            $kode_kota_snmptn = $db->QuerySingle("select kode_snmptn from kota where id_kota = {$id_kota}");
            
            $sekolah_snmptn_set = $db->QueryToArray("select * from sekolah_snmptn where kode_kota = '{$kode_kota_snmptn}' order by nama_sekolah");
            $smarty->assign('sekolah_snmptn_set', $sekolah_snmptn_set);
        }
    }
    
    if ($mode == 'get-kota')
    {
        $id_provinsi = get('id_provinsi');
        $kota_set = $db->QueryToArray("
            select k.*,
                (select count(s.id_sekolah) from sekolah s where s.id_kota = k.id_kota and s.kode_snmptn is not null) terisi,
                (select count(s.id_sekolah) from sekolah s where s.id_kota = k.id_kota) total
            from kota k
            where k.id_provinsi = {$id_provinsi}
            order by nm_kota");
        
        echo "<option value=\"\">--</option>";
        foreach ($kota_set as $k)
        {
            $persentase = number_format(($k['TERISI'] / $k['TOTAL']) * 100, 1);
            echo "<option value=\"{$k['ID_KOTA']}\">{$k['NM_KOTA']} ({$k['TERISI']}/{$k['TOTAL']}) -- {$persentase}%</option>";
        }
        exit();
    }
    
    if ($mode == 'get-kode-snmptn')
    {
        $id_sekolah = get('id_sekolah');
        echo $db->QuerySingle("select kode_snmptn from sekolah where id_sekolah = {$id_sekolah}");
        exit();
    }
}

$smarty->display("sekolah-snmptn/{$mode}.tpl");
?>
