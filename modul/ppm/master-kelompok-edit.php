<?php

include 'config.php';
include 'class/master.class.php';

$master = new master($db);

$smarty->assign('data_instrumen',$master->get_evaluasi_instrumen(get('id')));
$smarty->assign('data_kelompok_aspek',$master->get_kelompok_aspek(get('id_kel')));
$smarty->display('master-kelompok-edit.tpl');
?>
