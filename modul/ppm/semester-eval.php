<?php

include 'config.php';

$id_pt = $id_pt_user;

$tahun_sekarang = date('Y');

if (isset($_POST)) {
    if (post('mode') == 'aktifkan') {
        $semester = post('semester');
        $db->Query("UPDATE EVALUASI_SEMESTER_ISI SET STATUS_AKTIF='0' WHERE STATUS_AKTIF='1'");
        $db->Query("UPDATE EVALUASI_SEMESTER_ISI SET STATUS_AKTIF='1' WHERE ID_SEMESTER='{$semester}'");
    }
}
$data_semester = $db->QueryToArray("
    SELECT * 
    FROM SEMESTER S
    WHERE S.TAHUN_AJARAN LIKE '%{$tahun_sekarang}%' AND S.TIPE_SEMESTER IN ('REG') 
    ORDER BY S.TAHUN_AJARAN DESC,S.NM_SEMESTER");
foreach ($data_semester as $d) {
    $ada = $db->QuerySingle("SELECT COUNT(*) FROM EVALUASI_SEMESTER_ISI WHERE ID_SEMESTER='{$d['ID_SEMESTER']}'");
    if ($ada == 0) {
        $db->Query("INSERT INTO EVALUASI_SEMESTER_ISI (ID_SEMESTER,STATUS_AKTIF) VALUES ('{$d['ID_SEMESTER']}','0')");
    }
}
$data_semester_eval = $db->QueryToArray("
    SELECT S.*,ES.STATUS_AKTIF 
    FROM EVALUASI_SEMESTER_ISI ES
    JOIN SEMESTER S ON S.ID_SEMESTER=ES.ID_SEMESTER AND S.ID_PERGURUAN_TINGGI = '{$id_pt}'
    ORDER BY S.TAHUN_AJARAN DESC,S.NM_SEMESTER DESC");

$smarty->assign('data_semester', $data_semester_eval);
$smarty->display("semester-eval.tpl");
?>