<?php
include 'config.php';
include 'class/evaluasi.class.php';
include '../keuangan/class/list_data.class.php';

$eva = new evaluasi($db);
$list = new list_data($db);

$fakultas = get('fakultas');
$semester = get('semester');
$q_prodi = $prodi==''?"":" AND ID_PROGRAM_STUDI='{$prodi}'";

$header_kelompok_aspek = $db->QueryToArray("
            SELECT EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK,COUNT(EA.ID_EVAL_ASPEK)  JUMLAH_ASPEK
            FROM EVALUASI_KELOMPOK_ASPEK EKS
            JOIN EVALUASI_ASPEK EA ON EA.ID_EVAL_KELOMPOK_ASPEK=EKS.ID_EVAL_KELOMPOK_ASPEK AND EA.TIPE_ASPEK=1 AND EA.ID_EVAL_ASPEK IN 
            (
                SELECT ID_EVAL_ASPEK 
                FROM EVALUASI_HASIL 
                WHERE ID_SEMESTER='{$semester}'
                AND ID_FAKULTAS='{$fakultas}' {$q_prodi}
            )
            WHERE EKS.ID_EVAL_INSTRUMEN=1
            GROUP BY EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK
            ORDER BY ID_EVAL_KELOMPOK_ASPEK
            ");
$header_aspek = $db->QueryToArray("
            SELECT * FROM EVALUASI_ASPEK 
            WHERE ID_EVAL_INSTRUMEN=1 
            AND TIPE_ASPEK=1 
            AND ID_EVAL_ASPEK IN (
                SELECT ID_EVAL_ASPEK 
                FROM EVALUASI_HASIL 
                WHERE ID_SEMESTER='{$semester}'
                AND ID_FAKULTAS='{$fakultas}' {$q_prodi}
            ) 
            ORDER BY ID_EVAL_KELOMPOK_ASPEK,URUTAN");

$data_excel = $eva->load_laporan_evaluasi_kuliah(get('fakultas'), get('prodi'), get('semester'));
if (get('fakultas') != '' || get('prodi') != '') {
    $fakultas = $list->get_fakultas(get('fakultas'));
    $prodi = $list->get_prodi(get('prodi'));
}
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #220344;
        text-transform:capitalize;
    }
    td{
        text-align: left;
    }
</style>
<table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="<?= count($header_aspek) + 8 ?>" class="header_text">
                <?php
                echo strtoupper('Rekapitulasi Evaluasi Perkuliahan Dosen<br/>');
                if ($fakultas != '') {
                    echo strtoupper('Fakultas ' . $fakultas['NM_FAKULTAS'] . '<br/>');
                }
                if ($prodi != '') {
                    echo strtoupper('Program Studi (' . $prodi['NM_JENJANG'] . ') ' . $prodi['NM_PROGRAM_STUDI'] . '<br/>');
                }
                ?>
            </td>
        </tr>
        <tr>
            <td class="header_text" rowspan="2">NO</td>
            <td class="header_text" rowspan="2">PENGAJAR</td>
            <td class="header_text" rowspan="2">MATA KULIAH</td>
            <td class="header_text" rowspan="2">JUMLAH PESERTA</td>
            <td class="header_text" rowspan="2">JUMLAH RESPONDEN</td>
            <?php
            foreach ($header_kelompok_aspek as $hka):
                $kelompok = strtoupper($hka['NAMA_KELOMPOK']);
                echo '<td class="header_text" colspan="' . $hka['JUMLAH_ASPEK'] . '">' . $kelompok . "</td>";
            endforeach;
            ?>
            <td class="header_text" rowspan="2">RERATA DOSEN/MATA KULIAH</td>
            <td class="header_text" rowspan="2">RERATA DOSEN</td>
            <td class="header_text" rowspan="2">RERATA PERSEN DOSEN</td>
        </tr>
        <tr>
            <?php
            foreach ($header_aspek as $ha):
                $aspek = strtoupper($ha['EVALUASI_ASPEK']);
                echo '<td class="header_text">' . $aspek . "</td>";
            endforeach;
            ?>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data_excel as $d):
            echo "
                <tr>
                    <td>{$no}</td>
                    <td>{$d['NM_PENGGUNA']}</td>
                    <td>{$d['KD_MATA_KULIAH']} ({$d['NM_MATA_KULIAH']})</td>
                    <td>{$d['JUMLAH_PESERTA']}</td>
                    <td>{$d['JUMLAH_RESPONDEN']}</td>";
            foreach ($d['DATA_HASIL'] as $h):
                $eva = round($h['RATA'], 2);
                // echo "<td>{$eva}  <br/><br/> Nilai 0 : {$h['KOSONG']}</td>";
                echo "<td>{$eva}</td>";
            endforeach;
            echo "  <td>{$d['RERATA_DOSEN_PERMK']}</td>
                    <td>{$d['RERATA_DOSEN']}</td>
                    <td>{$d['RERATA_PERSEN_DOSEN']}</td>
                </tr>
                ";
            $no++;
        endforeach;
        ?>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "Data-Evaluasi-Kuliah(" . date('d-m-Y') . ")";
header("Content-disposition: attachment; filename={$nm_file}.xls");
header("Content-type: application/vnd.ms-excel");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
