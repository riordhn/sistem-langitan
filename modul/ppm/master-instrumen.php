<?php

include 'config.php';
include 'class/master.class.php';

$master = new master($db);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $master->tambah_eval_instrumen(post('nama'), post('keterangan'));
    } else if (post('mode') == 'edit') {
        $master->update_evaluasi_intrumen(post('id'), post('nama'), post('keterangan'));
    } else if (post('mode') == 'open') {
        $id = post('id');
        $db->Query("UPDATE EVALUASI_INSTRUMEN SET STATUS_BUKA=1 WHERE ID_EVAL_INSTRUMEN='{$id}'");
    } else if (post('mode') == 'close') {
        $id = post('id');
        $db->Query("UPDATE EVALUASI_INSTRUMEN SET STATUS_BUKA=0 WHERE ID_EVAL_INSTRUMEN='{$id}'");
    } else if (post('mode') == 'pengumuman') {
        $isi = post('pengumuman');
        $db->Query("UPDATE EVALUASI_INSTRUMEN SET PENGUMUMAN='{$isi}'");
    }
}
$smarty->assign('pengumuman',$db->QuerySingle("SELECT PENGUMUMAN FROM EVALUASI_INSTRUMEN GROUP BY PENGUMUMAN"));
$smarty->assign('data_intrumen', $master->load_evaluasi_intrumen());
$smarty->display('master-instrumen.tpl');
?>
