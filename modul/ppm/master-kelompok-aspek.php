<?php

include 'config.php';
include 'class/master.class.php';

$master = new master($db);


if (isset($_GET)) {
    if (get('mode') == 'kelompok') {
        if (isset($_POST)) {
            if (post('mode') == 'tambah') {
                $master->tambah_kelompok_aspek(post('id_ins'), post('nama'));
            } else if (post('mode') == 'edit') {
                $master->update_kelompok_aspek(post('id_kel'), post('nama'));
            }
        }
        $smarty->assign('data_kelompok_aspek', $master->load_kelompok_aspek(get('id')));
        $smarty->assign('data_instrumen_one', $master->get_evaluasi_instrumen(get('id')));
    }
}

$smarty->assign('data_instrumen', $master->load_evaluasi_intrumen());
$smarty->display('master-kelompok-aspek.tpl');
?>
