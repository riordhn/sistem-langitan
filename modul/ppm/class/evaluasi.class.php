<?php

class evaluasi {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    //menu evaluasi perkuliahan

    function load_row_evaluasi_kuliah($fakultas, $prodi, $semester) {
        $q_prodi = $prodi != '' ? "AND ID_PROGRAM_STUDI='{$prodi}'" : "";
        return $this->db->QueryToArray("
            SELECT ID_DOSEN,ID_PENGGUNA,NM_PENGGUNA,NM_MATA_KULIAH,ID_MATA_KULIAH,KD_MATA_KULIAH,RATA_DOSEN,RATA_DOSEN_PERMK,SUM(JUMLAH_RESPONDEN) JUMLAH_RESPONDEN,SUM(JUMLAH_PESERTA) JUMLAH_PESERTA
            FROM(
                SELECT D.ID_DOSEN,P.ID_PENGGUNA,EH.ID_KELAS_MK,P.NM_PENGGUNA,MK.ID_MATA_KULIAH,MK.NM_MATA_KULIAH,MK.KD_MATA_KULIAH,
                EH.JUMLAH_RESPONDEN,EHD.RATA_DOSEN,EHDMK.RATA_DOSEN_PERMK,COUNT(PMK.ID_MHS) JUMLAH_PESERTA
                FROM (
                  SELECT ID_DOSEN,ID_SEMESTER,ID_KELAS_MK,ID_PROGRAM_STUDI,ID_FAKULTAS,COUNT(DISTINCT(ID_MHS)) JUMLAH_RESPONDEN
                  FROM AUCC.EVALUASI_HASIL
                  WHERE  ID_FAKULTAS='{$fakultas}' AND ID_SEMESTER='{$semester}' {$q_prodi}
                  AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM AUCC.EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=1 AND TIPE_ASPEK=1)
                  GROUP BY ID_DOSEN,ID_SEMESTER,ID_KELAS_MK,ID_PROGRAM_STUDI,ID_FAKULTAS
                ) EH
                JOIN (
                    SELECT ID_DOSEN,AVG(
                      CASE
                      WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                      THEN NILAI_EVAL
                      END
                  ) RATA_DOSEN
                  FROM AUCC.EVALUASI_HASIL
                  WHERE ID_SEMESTER='{$semester}'
                  AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM AUCC.EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=1 AND TIPE_ASPEK=1)
                  GROUP BY ID_DOSEN
                ) EHD ON EHD.ID_DOSEN=EH.ID_DOSEN
                JOIN AUCC.DOSEN D ON EH.ID_DOSEN=D.ID_DOSEN
                JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
                JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = EH.ID_KELAS_MK
                LEFT JOIN AUCC.PENGAMBILAN_MK PMK ON PMK.ID_KELAS_MK=KMK.ID_KELAS_MK
                JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
                JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
                JOIN (
                  SELECT EH.ID_DOSEN,MK.ID_MATA_KULIAH,AVG(
                        CASE
                        WHEN EH.NILAI_EVAL BETWEEN '1' AND '4' 
                        THEN EH.NILAI_EVAL
                        END
                    ) RATA_DOSEN_PERMK
                  FROM AUCC.EVALUASI_HASIL EH
                  JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = EH.ID_KELAS_MK
                  JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
                  JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
                  WHERE EH.ID_SEMESTER='{$semester}'
                  AND EH.ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM AUCC.EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=1 AND TIPE_ASPEK=1)
                  GROUP BY EH.ID_DOSEN,MK.ID_MATA_KULIAH
                ) EHDMK ON EHDMK.ID_DOSEN=EH.ID_DOSEN AND EHDMK.ID_MATA_KULIAH=MK.ID_MATA_KULIAH
                WHERE (KUMK.STATUS_MKTA='0' OR KUMK.STATUS_MKTA IS NULL) AND (MK.STATUS_PRAKTIKUM IS NULL OR MK.STATUS_PRAKTIKUM ='0')
                GROUP BY D.ID_DOSEN,P.ID_PENGGUNA,EH.ID_KELAS_MK,P.NM_PENGGUNA,MK.NM_MATA_KULIAH,
                MK.KD_MATA_KULIAH,EH.JUMLAH_RESPONDEN,EHD.RATA_DOSEN,EHDMK.RATA_DOSEN_PERMK,MK.ID_MATA_KULIAH
                ORDER BY MK.NM_MATA_KULIAH,NM_PENGGUNA
            )
            GROUP BY ID_DOSEN,ID_PENGGUNA,NM_PENGGUNA,NM_MATA_KULIAH,KD_MATA_KULIAH,RATA_DOSEN,RATA_DOSEN_PERMK,ID_MATA_KULIAH
            ORDER BY NM_MATA_KULIAH,NM_PENGGUNA");
    }

    function load_laporan_evaluasi_kuliah($fakultas, $prodi, $semester) {
        $data_laporan = array();
        $q_prodi = $prodi == '' ? "" : "AND EH.ID_PROGRAM_STUDI='{$prodi}'";
        foreach ($this->load_row_evaluasi_kuliah($fakultas, $prodi, $semester) as $row) {
            $row_hasil = $this->db->QueryToArray("
                SELECT EH.ID_EVAL_KELOMPOK_ASPEK,EA.URUTAN,EH.ID_EVAL_ASPEK,AVG(
                    CASE
                    WHEN EH.NILAI_EVAL BETWEEN '1' AND '4' 
                    THEN EH.NILAI_EVAL
                    END
                ) RATA,
                SUM(
                    CASE
                    WHEN EH.NILAI_EVAL ='0'
                    THEN 1
                    ELSE 0
                    END
                ) KOSONG
                FROM AUCC.EVALUASI_HASIL EH
                JOIN AUCC.EVALUASI_ASPEK EA ON EA.ID_EVAL_ASPEK=EH.ID_EVAL_ASPEK
                JOIN AUCC.KELAS_MK KMK ON EH.ID_KELAS_MK=KMK.ID_KELAS_MK
                JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
                WHERE EH.ID_DOSEN='{$row['ID_DOSEN']}' AND KUMK.ID_MATA_KULIAH='{$row['ID_MATA_KULIAH']}' AND EH.ID_SEMESTER='{$semester}' 
                {$q_prodi} AND EH.ID_FAKULTAS='{$fakultas}'
                AND EH.ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM AUCC.EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=1 AND TIPE_ASPEK=1)
                GROUP BY EH.ID_EVAL_KELOMPOK_ASPEK,EH.ID_EVAL_ASPEK,EA.URUTAN
                ORDER BY EH.ID_EVAL_KELOMPOK_ASPEK,EH.ID_EVAL_ASPEK,EA.URUTAN");
            array_push($data_laporan, array_merge($row, array(
                'DATA_HASIL' => $row_hasil,
                'JUMLAH_PESERTA' => $row['JUMLAH_PESERTA'],
                'RERATA_DOSEN' => round($row['RATA_DOSEN'], 2),
                'RERATA_DOSEN_PERMK' => round($row['RATA_DOSEN_PERMK'], 2),
                'RERATA_PERSEN_DOSEN' => round($row['RATA_DOSEN'] / 4 * 100, 2)
                            )
                    )
            );
        }
        return $data_laporan;
    }

    function load_laporan_rerata_fakultas_evaluasi_kuliah($fakultas, $semester) {
        return $this->db->QuerySingle("
            SELECT AVG(
                CASE
                WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                THEN NILAI_EVAL
                END
                ) 
            FROM EVALUASI_HASIL
            WHERE ID_FAKULTAS='{$fakultas}'
            AND ID_EVAL_INSTRUMEN=1 
            AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=1)
            AND ID_SEMESTER='{$semester}'
            ");
    }

    function load_laporan_rerata_prodi_evaluasi_kuliah($prodi, $semester) {
        return $this->db->QuerySingle("
            SELECT AVG(
                CASE
                WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                THEN NILAI_EVAL
                END
                ) 
            FROM EVALUASI_HASIL
            WHERE ID_PROGRAM_STUDI='{$prodi}'
            AND ID_EVAL_INSTRUMEN=1 
            AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=1)
            AND ID_SEMESTER='{$semester}'
            ");
    }

    // menu evaluasi praktikum

    function load_row_evaluasi_prak($fakultas, $prodi, $semester) {
        $q_prodi = $prodi != '' ? "AND ID_PROGRAM_STUDI='{$prodi}'" : "";
        if ($fakultas == 1) {
            $query = "
            SELECT ID_DOSEN,ID_PENGGUNA,NM_PENGGUNA,NM_MATA_KULIAH,ID_MATA_KULIAH,KD_MATA_KULIAH,RATA_DOSEN,RATA_DOSEN_PERMK,SUM(JUMLAH_RESPONDEN) JUMLAH_RESPONDEN,SUM(JUMLAH_PESERTA) JUMLAH_PESERTA
            FROM(
            SELECT D.ID_DOSEN,P.ID_PENGGUNA,EH.ID_KELAS_MK,P.NM_PENGGUNA,MK.ID_MATA_KULIAH,MK.NM_MATA_KULIAH,MK.KD_MATA_KULIAH,
                              EH.JUMLAH_RESPONDEN,EHD.RATA_DOSEN,EHDMK.RATA_DOSEN_PERMK,COUNT(PMK.ID_MHS) JUMLAH_PESERTA
                        FROM (
                          SELECT ID_DOSEN,ID_SEMESTER,ID_KELAS_MK,ID_PROGRAM_STUDI,ID_FAKULTAS,COUNT(DISTINCT(ID_MHS)) JUMLAH_RESPONDEN
                          FROM AUCC.EVALUASI_HASIL
                          WHERE  ID_FAKULTAS='{$fakultas}' AND ID_SEMESTER='{$semester}' {$q_prodi}
                            AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM AUCC.EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=2 AND TIPE_ASPEK=1)
                          GROUP BY ID_DOSEN,ID_SEMESTER,ID_KELAS_MK,ID_PROGRAM_STUDI,ID_FAKULTAS
                        ) EH
                        JOIN (
                            SELECT ID_DOSEN,AVG(
                              CASE
                              WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                              THEN NILAI_EVAL
                              END
                            ) RATA_DOSEN
                            FROM AUCC.EVALUASI_HASIL
                            WHERE ID_SEMESTER='{$semester}'
                            AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM AUCC.EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=2 AND TIPE_ASPEK=1)
                            GROUP BY ID_DOSEN
                        ) EHD ON EHD.ID_DOSEN=EH.ID_DOSEN
                        JOIN AUCC.DOSEN D ON D.ID_DOSEN=EH.ID_DOSEN
                        JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
                        JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = EH.ID_KELAS_MK
                        LEFT JOIN AUCC.PENGAMBILAN_MK PMK ON PMK.ID_KELAS_MK=KMK.ID_KELAS_MK
                        JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
                        JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
                        JOIN (
                            SELECT EH.ID_DOSEN,MK.ID_MATA_KULIAH,AVG(
                                  CASE
                                  WHEN EH.NILAI_EVAL BETWEEN '1' AND '4' 
                                  THEN EH.NILAI_EVAL
                                  END
                              ) RATA_DOSEN_PERMK
                            FROM AUCC.EVALUASI_HASIL EH
                            JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = EH.ID_KELAS_MK
                            JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
                            JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
                            WHERE EH.ID_SEMESTER='{$semester}'
                            AND EH.ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM AUCC.EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=2 AND TIPE_ASPEK=1)
                            GROUP BY EH.ID_DOSEN,MK.ID_MATA_KULIAH
                          ) EHDMK ON EHDMK.ID_DOSEN=EH.ID_DOSEN AND EHDMK.ID_MATA_KULIAH=MK.ID_MATA_KULIAH
                        GROUP BY D.ID_DOSEN,P.ID_PENGGUNA,EH.ID_KELAS_MK,P.NM_PENGGUNA,MK.NM_MATA_KULIAH,
                        MK.KD_MATA_KULIAH,EH.JUMLAH_RESPONDEN,EHD.RATA_DOSEN,EHDMK.RATA_DOSEN_PERMK,MK.ID_MATA_KULIAH
                        ORDER BY MK.NM_MATA_KULIAH,NM_PENGGUNA
            )
            GROUP BY ID_DOSEN,ID_PENGGUNA,NM_PENGGUNA,NM_MATA_KULIAH,KD_MATA_KULIAH,RATA_DOSEN,RATA_DOSEN_PERMK,ID_MATA_KULIAH
            ORDER BY NM_MATA_KULIAH,NM_PENGGUNA";
        } else {
            $query = "SELECT ID_DOSEN,ID_PENGGUNA,NM_PENGGUNA,NM_MATA_KULIAH,ID_MATA_KULIAH,KD_MATA_KULIAH,RATA_DOSEN,RATA_DOSEN_PERMK,SUM(JUMLAH_RESPONDEN) JUMLAH_RESPONDEN,SUM(JUMLAH_PESERTA) JUMLAH_PESERTA
            FROM(
            SELECT D.ID_DOSEN,P.ID_PENGGUNA,EH.ID_KELAS_MK,P.NM_PENGGUNA,MK.ID_MATA_KULIAH,MK.NM_MATA_KULIAH,MK.KD_MATA_KULIAH,
                             EH.JUMLAH_RESPONDEN,EHD.RATA_DOSEN,EHDMK.RATA_DOSEN_PERMK,COUNT(PMK.ID_MHS) JUMLAH_PESERTA
                        FROM (
                          SELECT ID_DOSEN,ID_SEMESTER,ID_KELAS_MK,ID_PROGRAM_STUDI,ID_FAKULTAS,COUNT(DISTINCT(ID_MHS)) JUMLAH_RESPONDEN
                          FROM AUCC.EVALUASI_HASIL
                          WHERE  ID_FAKULTAS='{$fakultas}' AND ID_SEMESTER='{$semester}' {$q_prodi}
                            AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM AUCC.EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=2 AND TIPE_ASPEK=1)
                          GROUP BY ID_DOSEN,ID_SEMESTER,ID_KELAS_MK,ID_PROGRAM_STUDI,ID_FAKULTAS
                        ) EH
                        JOIN (
                            SELECT ID_DOSEN,AVG(
                              CASE
                              WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                              THEN NILAI_EVAL
                              END
                            ) RATA_DOSEN
                            FROM AUCC.EVALUASI_HASIL
                            WHERE ID_SEMESTER='{$semester}'
                            AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM AUCC.EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=2 AND TIPE_ASPEK=1)
                            GROUP BY ID_DOSEN
                        ) EHD ON EHD.ID_DOSEN=EH.ID_DOSEN
                        JOIN AUCC.DOSEN D ON EH.ID_DOSEN=D.ID_DOSEN
                        JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
                        JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = EH.ID_KELAS_MK
                        LEFT JOIN AUCC.PENGAMBILAN_MK PMK ON PMK.ID_KELAS_MK=KMK.ID_KELAS_MK
                        JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
                        JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
                        JOIN (
                            SELECT EH.ID_DOSEN,MK.ID_MATA_KULIAH,AVG(
                                  CASE
                                  WHEN EH.NILAI_EVAL BETWEEN '1' AND '4' 
                                  THEN EH.NILAI_EVAL
                                  END
                              ) RATA_DOSEN_PERMK
                            FROM AUCC.EVALUASI_HASIL EH
                            JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = EH.ID_KELAS_MK
                            JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK
                            JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
                            WHERE EH.ID_SEMESTER='{$semester}'
                            AND EH.ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM AUCC.EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=2 AND TIPE_ASPEK=1)
                            GROUP BY EH.ID_DOSEN,MK.ID_MATA_KULIAH
                          ) EHDMK ON EHDMK.ID_DOSEN=EH.ID_DOSEN AND EHDMK.ID_MATA_KULIAH=MK.ID_MATA_KULIAH
                        WHERE (KUMK.STATUS_MKTA IN (5,6)  OR MK.STATUS_PRAKTIKUM=1)
                        GROUP BY D.ID_DOSEN,P.ID_PENGGUNA,EH.ID_KELAS_MK,P.NM_PENGGUNA,MK.NM_MATA_KULIAH,
                        MK.KD_MATA_KULIAH,EH.JUMLAH_RESPONDEN,EHD.RATA_DOSEN,EHDMK.RATA_DOSEN_PERMK,MK.ID_MATA_KULIAH
                        ORDER BY MK.NM_MATA_KULIAH,NM_PENGGUNA
            )
            GROUP BY ID_DOSEN,ID_PENGGUNA,NM_PENGGUNA,NM_MATA_KULIAH,KD_MATA_KULIAH,RATA_DOSEN,RATA_DOSEN_PERMK,ID_MATA_KULIAH
            ORDER BY NM_MATA_KULIAH,NM_PENGGUNA";
        }
        return $this->db->QueryToArray($query);
    }

    function load_laporan_evaluasi_prak($fakultas, $prodi, $semester) {
        $data_laporan = array();
        $q_prodi = $prodi == '' ? "" : "AND EH.ID_PROGRAM_STUDI='{$prodi}'";
        foreach ($this->load_row_evaluasi_prak($fakultas, $prodi, $semester) as $row) {
            $row_hasil = $this->db->QueryToArray("
                SELECT ID_EVAL_ASPEK,AVG(
                    CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                    THEN NILAI_EVAL
                    END
                ) RATA ,
                SUM(
                    CASE
                    WHEN NILAI_EVAL ='0'
                    THEN 1
                    ELSE 0
                    END
                ) KOSONG
                FROM AUCC.EVALUASI_HASIL EH
                JOIN AUCC.KELAS_MK KMK ON EH.ID_KELAS_MK=KMK.ID_KELAS_MK
                JOIN AUCC.KURIKULUM_MK KUMK ON KUMK.ID_KURIKULUM_MK=KMK.ID_KURIKULUM_MK
                WHERE EH.ID_DOSEN='{$row['ID_DOSEN']}' AND KUMK.ID_MATA_KULIAH='{$row['ID_MATA_KULIAH']}' AND EH.ID_SEMESTER='{$semester}'
                {$q_prodi} AND EH.ID_FAKULTAS='{$fakultas}'
                AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM AUCC.EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=2 AND TIPE_ASPEK=1)
                GROUP BY ID_EVAL_ASPEK
                ORDER BY ID_EVAL_ASPEK");
            array_push($data_laporan, array_merge($row, array(
                'DATA_HASIL' => $row_hasil,
                'JUMLAH_PESERTA' => $row['JUMLAH_PESERTA'],
                'RERATA_DOSEN_PERMK' => round($row['RATA_DOSEN_PERMK'], 2),
                'RERATA_DOSEN' => round($row['RATA_DOSEN'], 2),
                'RERATA_PERSEN_DOSEN' => round($row['RATA_DOSEN'] / 4 * 100, 2)
                            )
                    )
            );
        }
        return $data_laporan;
    }

    function load_laporan_rerata_fakultas_evaluasi_prak($fakultas, $semester) {
        return $this->db->QuerySingle("
            SELECT AVG(
                CASE
                WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                THEN NILAI_EVAL
                END
                ) 
            FROM EVALUASI_HASIL
            WHERE ID_FAKULTAS='{$fakultas}'
            AND ID_EVAL_INSTRUMEN=2 
            AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=2)
            AND ID_SEMESTER='{$semester}'
            ");
    }

    function load_laporan_rerata_prodi_evaluasi_prak($prodi, $semester) {
        return $this->db->QuerySingle("
            SELECT AVG(
                CASE
                WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                THEN NILAI_EVAL
                END
                ) 
            FROM EVALUASI_HASIL
            WHERE ID_PROGRAM_STUDI='{$prodi}'
            AND ID_EVAL_INSTRUMEN=2
            AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=2)
            AND ID_SEMESTER='{$semester}'
            ");
    }

    // menu evaluasi perwalian

    function load_row_evaluasi_wali($fakultas, $prodi, $semester) {
        if ($prodi != '') {
            $q_prodi = $prodi != '' ? "AND EH.ID_PROGRAM_STUDI='{$prodi}'" : "";
            return $this->db->QueryToArray("
            SELECT D.ID_DOSEN,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,DEP.NM_DEPARTEMEN,COUNT(DISTINCT(EH.ID_MHS)) JUMLAH_RESPONDEN
                ,DW.JUMLAH_PESERTA,EHD.RERATA_DOSEN
                FROM AUCC.EVALUASI_HASIL EH
                JOIN AUCC.DOSEN D ON EH.ID_DOSEN=D.ID_DOSEN
                JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
                JOIN (
                  SELECT ID_DOSEN,COUNT(ID_MHS) JUMLAH_PESERTA
                  FROM AUCC.DOSEN_WALI
                  WHERE STATUS_DOSEN_WALI=1
                  GROUP BY ID_DOSEN
                ) DW ON DW.ID_DOSEN=D.ID_DOSEN
                JOIN (
                   SELECT ID_DOSEN,AVG(CASE WHEN NILAI_EVAL BETWEEN '1' AND '4' THEN NILAI_EVAL END) RERATA_DOSEN
                    FROM AUCC.EVALUASI_HASIL 
                    WHERE ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM AUCC.EVALUASI_ASPEK WHERE  ID_EVAL_INSTRUMEN=3 AND TIPE_ASPEK=1)
                    AND ID_SEMESTER='{$semester}'
                    GROUP BY ID_DOSEN
                ) EHD ON EHD.ID_DOSEN=D.ID_DOSEN
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
                JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                LEFT JOIN AUCC.DEPARTEMEN DEP ON DEP.ID_DEPARTEMEN = PS.ID_DEPARTEMEN
                WHERE EH.ID_FAKULTAS='{$fakultas}' AND EH.ID_SEMESTER='{$semester}' AND EH.ID_EVAL_INSTRUMEN=3 {$q_prodi}
            GROUP BY D.ID_DOSEN,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,DEP.NM_DEPARTEMEN,DW.JUMLAH_PESERTA,EHD.RERATA_DOSEN
            ORDER BY NM_PENGGUNA");
        } else {
            return $this->db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,COUNT(DISTINCT(EH.ID_MHS)) JUMLAH_RESPONDEN
                ,COUNT(DISTINCT(DW.ID_MHS)) JUMLAH_PESERTA,AVG(EH.NILAI_EVAL) RERATA_PRODI
                FROM AUCC.EVALUASI_HASIL EH
                JOIN AUCC.DOSEN D ON EH.ID_DOSEN=D.ID_DOSEN
                JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=D.ID_PENGGUNA
                JOIN AUCC.DOSEN_WALI DW ON DW.ID_DOSEN=D.ID_DOSEN AND DW.STATUS_DOSEN_WALI=1
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
                JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                LEFT JOIN AUCC.DEPARTEMEN DEP ON DEP.ID_DEPARTEMEN = PS.ID_DEPARTEMEN
                WHERE PS.ID_FAKULTAS='{$fakultas}' AND EH.ID_SEMESTER='{$semester}' AND EH.ID_EVAL_INSTRUMEN=3
                    AND EH.ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_ASPEK=EH.ID_EVAL_ASPEK)
            GROUP BY PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
        }
    }

    function load_laporan_evaluasi_wali($fakultas, $prodi, $semester) {
        $data_laporan = array();
        foreach ($this->load_row_evaluasi_wali($fakultas, $prodi, $semester) as $row) {
            if ($prodi != '') {
                $row_hasil = $this->db->QueryToArray("
                SELECT EA.ID_EVAL_ASPEK,RATA,KOSONG
                FROM AUCC.EVALUASI_ASPEK EA
                JOIN (
                SELECT ID_EVAL_ASPEK,AVG(
                    CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                    THEN NILAI_EVAL
                    END
                ) RATA ,
                SUM(
                    CASE
                    WHEN NILAI_EVAL ='0'
                    THEN 1
                    ELSE 0
                    END
                ) KOSONG
                FROM AUCC.EVALUASI_HASIL EH
                WHERE EH.ID_DOSEN='{$row['ID_DOSEN']}' AND EH.ID_SEMESTER='{$semester}'
                     AND EH.ID_PROGRAM_STUDI='{$prodi}' AND EH.ID_FAKULTAS='{$fakultas}'
                GROUP BY ID_EVAL_ASPEK
                ) EH ON EH.ID_EVAL_ASPEK=EA.ID_EVAL_ASPEK
                WHERE EA.TIPE_ASPEK=1 AND EA.ID_EVAL_INSTRUMEN=3
                ORDER BY EA.ID_EVAL_ASPEK");
                array_push($data_laporan, array_merge($row, array(
                    'DATA_HASIL' => $row_hasil,
                    'JUMLAH_PESERTA' => $row['JUMLAH_PESERTA'],
                    'RERATA_DOSEN' => round($row['RERATA_DOSEN'], 2),
                    'RERATA_PERSEN_DOSEN' => round($row['RERATA_DOSEN'] / 4 * 100, 2)
                                )
                        )
                );
            } else {
                $row_hasil = $this->db->QueryToArray("
                SELECT EA.ID_EVAL_ASPEK,RATA,KOSONG
                FROM AUCC.EVALUASI_ASPEK EA
                JOIN (
                SELECT ID_EVAL_ASPEK,AVG(
                    CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                    THEN NILAI_EVAL
                    END
                ) RATA ,
                SUM(
                    CASE
                    WHEN NILAI_EVAL ='0'
                    THEN 1
                    ELSE 0
                    END
                ) KOSONG
                FROM AUCC.EVALUASI_HASIL EH
                WHERE  EH.ID_SEMESTER='{$semester}'
                     AND EH.ID_PROGRAM_STUDI='{$row['ID_PROGRAM_STUDI']}' AND EH.ID_FAKULTAS='{$fakultas}'
                GROUP BY ID_EVAL_ASPEK
                ) EH ON EH.ID_EVAL_ASPEK=EA.ID_EVAL_ASPEK
                WHERE EA.TIPE_ASPEK=1 AND EA.ID_EVAL_INSTRUMEN=3
                ORDER BY EA.ID_EVAL_ASPEK");
                array_push($data_laporan, array_merge($row, array(
                    'DATA_HASIL' => $row_hasil,
                    'JUMLAH_PESERTA' => $row['JUMLAH_PESERTA'],
                    'RERATA_PRODI' => round($row['RERATA_PRODI'], 2),
                    'RERATA_PERSEN_PRODI' => round($row['RERATA_PRODI'] / 4 * 100, 2)
                                )
                        )
                );
            }
        }
        return $data_laporan;
    }

    function load_laporan_rerata_departemen_evaluasi_wali($prodi, $semester) {
        $id_departemen = $this->db->QuerySingle("SELECT ID_DEPARTEMEN FROM AUCC.PROGRAM_STUDI WHERE ID_PROGRAM_STUDI='{$prodi}'");
        $query = "SELECT PS.ID_DEPARTEMEN,DEP.NM_DEPARTEMEN,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM AUCC.EVALUASI_HASIL EH
                    JOIN AUCC.DOSEN D ON D.ID_DOSEN=EH.ID_DOSEN
                    JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI
                    JOIN AUCC.DEPARTEMEN DEP ON PS.ID_DEPARTEMEN = DEP.ID_DEPARTEMEN
                    WHERE EH.ID_SEMESTER='{$semester}' AND DEP.ID_DEPARTEMEN='{$id_departemen}' AND EH.ID_EVAL_INSTRUMEN=3
                    GROUP BY PS.ID_DEPARTEMEN,DEP.NM_DEPARTEMEN";
        $this->db->Query($query);
        return $this->db->FetchAssoc();
    }

    function load_laporan_rerata_prodi_evaluasi_wali($prodi, $semester) {
        return $this->db->QuerySingle("
            SELECT AVG(
                CASE
                WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                THEN NILAI_EVAL
                END
                ) 
            FROM EVALUASI_HASIL
            WHERE ID_PROGRAM_STUDI='{$prodi}'
            AND ID_EVAL_INSTRUMEN=3
            AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=3)
            AND ID_SEMESTER='{$semester}'
            ");
    }

    //Menu Evaluasi Mahasiswa Baru

    function load_row_evaluasi_maba($fakultas, $semester) {
        return $this->db->QueryToArray("
            SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,COUNT(DISTINCT(EH.ID_MHS)) JUMLAH_RESPONDEN
            FROM AUCC.EVALUASI_HASIL EH
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=EH.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE PS.ID_FAKULTAS='{$fakultas}' AND EH.ID_SEMESTER='{$semester}' AND EH.ID_EVAL_INSTRUMEN=4
            GROUP BY PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
    }

    function load_laporan_evaluasi_maba($fakultas, $semester) {
        $data_laporan = array();
        foreach ($this->load_row_evaluasi_maba($fakultas, $semester) as $row) {
            $row_hasil = $this->db->QueryToArray("
                SELECT EA.ID_EVAL_ASPEK,RATA,KOSONG
                FROM AUCC.EVALUASI_ASPEK EA
                JOIN (
                SELECT ID_EVAL_ASPEK,AVG(
                    CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                    THEN NILAI_EVAL
                    END
                ) RATA ,
                SUM(
                    CASE
                    WHEN NILAI_EVAL ='0'
                    THEN 1
                    ELSE 0
                    END
                ) KOSONG
                FROM AUCC.EVALUASI_HASIL EH
                WHERE EH.ID_PROGRAM_STUDI='{$row['ID_PROGRAM_STUDI']}' AND EH.ID_SEMESTER='{$semester}'
                GROUP BY ID_EVAL_ASPEK
                ) EH ON EH.ID_EVAL_ASPEK=EA.ID_EVAL_ASPEK
                WHERE EA.TIPE_ASPEK=1 AND EA.ID_EVAL_INSTRUMEN=4
                ORDER BY EA.ID_EVAL_ASPEK
                ");
            array_push($data_laporan, array_merge($row, array(
                'DATA_HASIL' => $row_hasil)
                    )
            );
        }
        return $data_laporan;
    }

    function load_laporan_rerata_fakultas_evaluasi_maba($fakultas, $semester) {
        return $this->db->QuerySingle("
            SELECT AVG(
                CASE
                WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                THEN NILAI_EVAL
                END
                ) 
            FROM EVALUASI_HASIL
            WHERE ID_FAKULTAS='{$fakultas}'
            AND ID_EVAL_INSTRUMEN=4
            AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=4)
            AND ID_SEMESTER='{$semester}'
            ");
    }

    //Menu Evaluasi WIsuda

    function load_row_evaluasi_wisuda($fakultas, $periode) {
        if ($fakultas != '') {
            $query = "SELECT PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,COUNT(DISTINCT(EH.ID_MHS)) JUMLAH_RESPONDEN
            FROM AUCC.EVALUASI_HASIL EH
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=EH.ID_PROGRAM_STUDI
            JOIN AUCC.JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE PS.ID_FAKULTAS='{$fakultas}' AND EH.ID_TARIF_WISUDA='{$periode}' AND EH.ID_EVAL_INSTRUMEN=5
            GROUP BY PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.NM_JENJANG
            ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI";
        } else {
            $query = "SELECT F.ID_FAKULTAS,F.NM_FAKULTAS,COUNT(DISTINCT(EH.ID_MHS)) JUMLAH_RESPONDEN
            FROM AUCC.EVALUASI_HASIL EH
            JOIN AUCC.FAKULTAS F ON EH.ID_FAKULTAS=F.ID_FAKULTAS
            WHERE EH.ID_TARIF_WISUDA='{$periode}' AND EH.ID_EVAL_INSTRUMEN=5
            GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
            ORDER BY F.ID_FAKULTAS,F.NM_FAKULTAS";
        }
        return $this->db->QueryToArray($query);
    }

    function load_laporan_evaluasi_wisuda($fakultas, $periode) {
        $data_laporan = array();
        if ($fakultas != '') {
            foreach ($this->load_row_evaluasi_wisuda($fakultas, $periode) as $row) {
                $row_hasil = $this->db->QueryToArray("
                SELECT EA.ID_EVAL_ASPEK,RATA,KOSONG
                FROM AUCC.EVALUASI_ASPEK EA
                JOIN (
                SELECT ID_EVAL_ASPEK,AVG(
                    CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                    THEN NILAI_EVAL
                    END
                ) RATA ,
                SUM(
                    CASE
                    WHEN NILAI_EVAL ='0'
                    THEN 1
                    ELSE 0
                    END
                ) KOSONG
                FROM AUCC.EVALUASI_HASIL EH
                WHERE EH.ID_PROGRAM_STUDI='{$row['ID_PROGRAM_STUDI']}' AND EH.ID_TARIF_WISUDA='{$periode}'
                GROUP BY ID_EVAL_ASPEK
                ) EH ON EH.ID_EVAL_ASPEK=EA.ID_EVAL_ASPEK
                WHERE EA.TIPE_ASPEK=1 AND EA.ID_EVAL_INSTRUMEN=5
                ORDER BY EA.ID_EVAL_ASPEK
                ");
                array_push($data_laporan, array_merge($row, array(
                    'DATA_HASIL' => $row_hasil)
                        )
                );
            }
        } else {
            foreach ($this->load_row_evaluasi_wisuda($fakultas, $periode) as $row) {
                $row_hasil = $this->db->QueryToArray("
                SELECT EA.ID_EVAL_ASPEK,RATA,KOSONG
                FROM AUCC.EVALUASI_ASPEK EA
                JOIN (
                SELECT ID_EVAL_ASPEK,AVG(
                    CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                    THEN NILAI_EVAL
                    END
                ) RATA ,
                SUM(
                    CASE
                    WHEN NILAI_EVAL ='0'
                    THEN 1
                    ELSE 0
                    END
                ) KOSONG
                FROM AUCC.EVALUASI_HASIL EH
                WHERE EH.ID_FAKULTAS='{$row['ID_FAKULTAS']}' AND EH.ID_TARIF_WISUDA='{$periode}'
                GROUP BY ID_EVAL_ASPEK
                ) EH ON EH.ID_EVAL_ASPEK=EA.ID_EVAL_ASPEK
                WHERE EA.TIPE_ASPEK=1 AND EA.ID_EVAL_INSTRUMEN=5
                ORDER BY EA.ID_EVAL_ASPEK
                ");
                array_push($data_laporan, array_merge($row, array(
                    'DATA_HASIL' => $row_hasil)
                        )
                );
            }
        }
        return $data_laporan;
    }

    function load_laporan_rerata_fakultas_evaluasi_wisuda($fakultas, $periode) {
        return $this->db->QuerySingle("
            SELECT AVG(
                CASE
                WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                THEN NILAI_EVAL
                END
                ) 
            FROM EVALUASI_HASIL
            WHERE ID_FAKULTAS='{$fakultas}'
            AND ID_EVAL_INSTRUMEN=5
            AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=5)
            AND ID_TARIF_WISUDA='{$periode}'
            ");
    }

    //Menu Evaluasi Administrasi Departemen

    function load_row_evaluasi_adm_dep($fakultas, $semester) {
        return $this->db->QueryToArray("
            SELECT DEP.ID_DEPARTEMEN,DEP.NM_DEPARTEMEN,COUNT(DISTINCT(EH.ID_MHS)) JUMLAH_RESPONDEN
            FROM DEPARTEMEN DEP
            JOIN PROGRAM_STUDI PS ON PS.ID_DEPARTEMEN=DEP.ID_DEPARTEMEN
            JOIN EVALUASI_HASIL EH ON EH.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
            WHERE EH.ID_SEMESTER='{$semester}' AND PS.ID_FAKULTAS='{$fakultas}'
            GROUP BY DEP.ID_DEPARTEMEN,DEP.NM_DEPARTEMEN
            ORDER BY DEP.NM_DEPARTEMEN
            ");
    }

    function load_laporan_evaluasi_adm_dep($fakultas, $semester) {
        $data_laporan = array();
        foreach ($this->load_row_evaluasi_adm_dep($fakultas, $semester) as $row) {
            $row_hasil = $this->db->QueryToArray("
                SELECT EA.ID_EVAL_ASPEK,RATA
                FROM AUCC.EVALUASI_ASPEK EA
                JOIN (
                SELECT ID_EVAL_ASPEK,AVG(
                    CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                    THEN NILAI_EVAL
                    END
                ) RATA 
                FROM AUCC.EVALUASI_HASIL EH
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=EH.ID_PROGRAM_STUDI
                WHERE PS.ID_DEPARTEMEN='{$row['ID_DEPARTEMEN']}' AND EH.ID_SEMESTER='{$semester}'
                GROUP BY ID_EVAL_ASPEK
                ) EH ON EH.ID_EVAL_ASPEK=EA.ID_EVAL_ASPEK
                WHERE EA.TIPE_ASPEK=1 AND EA.ID_EVAL_INSTRUMEN=6
                ORDER BY EA.ID_EVAL_ASPEK");
            array_push($data_laporan, array_merge($row, array(
                'DATA_HASIL' => $row_hasil)
                    )
            );
        }
        return $data_laporan;
    }

    //Menu Evaluasi Administrasi Fakultas

    function load_row_evaluasi_adm_fak($semester) {
        return $this->db->QueryToArray("
            SELECT F.ID_FAKULTAS,F.NM_FAKULTAS 
            FROM FAKULTAS F 
            JOIN EVALUASI_HASIL EH ON EH.ID_FAKULTAS=F.ID_FAKULTAS
            WHERE EH.ID_SEMESTER='{$semester}'
            GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
            ORDER BY F.ID_FAKULTAS
            ");
    }

    function load_row_evaluasi_bimbingan_skripsi($semester) {
        return $this->db->QueryToArray("
            SELECT F.ID_FAKULTAS,F.NM_FAKULTAS 
            FROM FAKULTAS F 
            JOIN EVALUASI_HASIL EH ON EH.ID_FAKULTAS=F.ID_FAKULTAS
            WHERE EH.ID_SEMESTER='{$semester}'
            AND F.ID_PERGURUAN_TINGGI=".getenv('ID_PT')."
            GROUP BY F.ID_FAKULTAS,F.NM_FAKULTAS
            ORDER BY F.ID_FAKULTAS
            ");
    }

    function load_laporan_evaluasi_adm_fak($semester) {
        $data_laporan = array();
        foreach ($this->load_row_evaluasi_adm_fak($semester) as $row) {
            $row_hasil = $this->db->QueryToArray("
                SELECT EA.ID_EVAL_ASPEK,RATA,KOSONG
                FROM AUCC.EVALUASI_ASPEK EA
                JOIN (
                SELECT ID_EVAL_ASPEK,AVG(
                    CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                    THEN NILAI_EVAL
                    END
                ) RATA ,
                SUM(
                    CASE
                    WHEN NILAI_EVAL ='0'
                    THEN 1
                    ELSE 0
                    END
                ) KOSONG
                FROM AUCC.EVALUASI_HASIL EH
                WHERE EH.ID_FAKULTAS='{$row['ID_FAKULTAS']}' AND EH.ID_SEMESTER='{$semester}'
                GROUP BY ID_EVAL_ASPEK
                ) EH ON EH.ID_EVAL_ASPEK=EA.ID_EVAL_ASPEK
                WHERE EA.TIPE_ASPEK=1 AND EA.ID_EVAL_INSTRUMEN=6
                ORDER BY EA.ID_EVAL_ASPEK
                ");
            array_push($data_laporan, array_merge($row, array(
                'DATA_HASIL' => $row_hasil)
                    )
            );
        }
        return $data_laporan;
    }

    function load_laporan_evaluasi_bimbingan_skripsi($semester) {
        $data_laporan = array();
        foreach ($this->load_row_evaluasi_bimbingan_skripsi($semester) as $row) {
            $row_hasil = $this->db->QueryToArray("
                SELECT EA.ID_EVAL_ASPEK,RATA,KOSONG
                FROM AUCC.EVALUASI_ASPEK EA
                JOIN (
                SELECT ID_EVAL_ASPEK,AVG(
                    CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                    THEN NILAI_EVAL
                    END
                ) RATA ,
                SUM(
                    CASE
                    WHEN NILAI_EVAL ='0'
                    THEN 1
                    ELSE 0
                    END
                ) KOSONG
                FROM AUCC.EVALUASI_HASIL EH
                JOIN FAKULTAS FAK ON FAK.ID_FAKULTAS=EH.ID_FAKULTAS
                WHERE EH.ID_FAKULTAS='{$row['ID_FAKULTAS']}' AND EH.ID_SEMESTER='{$semester}'
                AND FAK.ID_PERGURUAN_TINGGI=".getenv('ID_PT')."
                GROUP BY ID_EVAL_ASPEK
                ) EH ON EH.ID_EVAL_ASPEK=EA.ID_EVAL_ASPEK
                WHERE EA.TIPE_ASPEK=1 AND EA.ID_EVAL_INSTRUMEN=7
                ORDER BY EA.ID_EVAL_ASPEK
                ");
            array_push($data_laporan, array_merge($row, array(
                'DATA_HASIL' => $row_hasil)
                    )
            );
        }
        return $data_laporan;
    }

}

?>
