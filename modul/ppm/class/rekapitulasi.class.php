<?php

class rekapitulasi {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    function load_rekap_evakul($fakultas, $semester) {
        $data_rekap = array();
        if ($fakultas != '') {
            $row_prodi = $this->db->QueryToArray("
                SELECT PS.ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI 
                FROM PROGRAM_STUDI PS 
                JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG 
                WHERE PS.ID_FAKULTAS='{$fakultas}' AND PS.ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=1 AND ID_SEMESTER='{$semester}')
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
            foreach ($row_prodi as $row) {
                $rekap = $this->db->QueryToArray("
                    SELECT ID_EVAL_KELOMPOK_ASPEK,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4'
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM EVALUASI_HASIL EV
                    WHERE ID_EVAL_INSTRUMEN=1 
                    AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=1) 
                    AND ID_PROGRAM_STUDI='{$row['ID_PROGRAM_STUDI']}'
                    AND ID_SEMESTER='{$semester}'
                    AND ID_EVAL_KELOMPOK_ASPEK IS NOT NULL
                    GROUP BY ID_EVAL_KELOMPOK_ASPEK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    ");
                array_push($data_rekap, array_merge($row, array('DATA_EVAL' => $rekap)));
            }
        } else {
            $row_fakultas = $this->db->QueryToArray("
                SELECT F.ID_FAKULTAS,F.NM_FAKULTAS
                FROM FAKULTAS F 
                WHERE F.ID_FAKULTAS IN (SELECT ID_FAKULTAS FROM EVALUASI_HASIL)
                ORDER BY ID_FAKULTAS");
            foreach ($row_fakultas as $row) {
                $rekap = $this->db->QueryToArray("
                    SELECT ID_EVAL_KELOMPOK_ASPEK,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4'
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM EVALUASI_HASIL EV
                    WHERE ID_EVAL_INSTRUMEN=1 
                    AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=1) 
                    AND ID_FAKULTAS='{$row['ID_FAKULTAS']}'
                    AND ID_SEMESTER='{$semester}'
                    AND ID_EVAL_KELOMPOK_ASPEK IS NOT NULL
                    GROUP BY ID_EVAL_KELOMPOK_ASPEK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    ");
                array_push($data_rekap, array_merge($row, array('DATA_EVAL' => $rekap)));
            }
        }
        return $data_rekap;
    }

    function load_rekap_evaprak($fakultas, $semester) {
        $data_rekap = array();
        if ($fakultas != '') {
            $row_prodi = $this->db->QueryToArray("
                SELECT PS.ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI 
                FROM PROGRAM_STUDI PS 
                JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG 
                WHERE PS.ID_FAKULTAS='{$fakultas}' AND PS.ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=2 AND ID_SEMESTER='{$semester}')
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
            foreach ($row_prodi as $row) {
                $rekap = $this->db->QueryToArray("
                    SELECT ID_EVAL_KELOMPOK_ASPEK,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4'
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM EVALUASI_HASIL EV
                    WHERE ID_EVAL_INSTRUMEN=2
                    AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=2) 
                    AND ID_PROGRAM_STUDI='{$row['ID_PROGRAM_STUDI']}'
                    AND ID_SEMESTER='{$semester}'
                    AND ID_EVAL_KELOMPOK_ASPEK IS NOT NULL
                    GROUP BY ID_EVAL_KELOMPOK_ASPEK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    ");
                array_push($data_rekap, array_merge($row, array('DATA_EVAL' => $rekap)));
            }
        } else {
            $row_fakultas = $this->db->QueryToArray("
                SELECT F.ID_FAKULTAS,F.NM_FAKULTAS
                FROM FAKULTAS F 
                WHERE F.ID_FAKULTAS IN (SELECT ID_FAKULTAS FROM EVALUASI_HASIL)
                ORDER BY ID_FAKULTAS");
            foreach ($row_fakultas as $row) {
                $rekap = $this->db->QueryToArray("
                    SELECT ID_EVAL_KELOMPOK_ASPEK,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4'
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM EVALUASI_HASIL EV
                    WHERE ID_EVAL_INSTRUMEN=2
                    AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=2) 
                    AND ID_FAKULTAS='{$row['ID_FAKULTAS']}'
                    AND ID_SEMESTER='{$semester}'
                    AND ID_EVAL_KELOMPOK_ASPEK IS NOT NULL
                    GROUP BY ID_EVAL_KELOMPOK_ASPEK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    ");
                array_push($data_rekap, array_merge($row, array('DATA_EVAL' => $rekap)));
            }
        }
        return $data_rekap;
    }

    function load_rekap_evawali($fakultas, $semester) {
        $data_rekap = array();
        if ($fakultas != '') {
            $row_prodi = $this->db->QueryToArray("
                SELECT PS.ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI 
                FROM PROGRAM_STUDI PS 
                JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG 
                WHERE PS.ID_FAKULTAS='{$fakultas}' AND PS.ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=3 AND ID_SEMESTER='{$semester}')
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
            foreach ($row_prodi as $row) {
                $rekap = $this->db->QueryToArray("
                    SELECT ID_EVAL_KELOMPOK_ASPEK,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4'
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM EVALUASI_HASIL EV
                    WHERE ID_EVAL_INSTRUMEN=3
                    AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=3) 
                    AND ID_PROGRAM_STUDI='{$row['ID_PROGRAM_STUDI']}'
                    AND ID_SEMESTER='{$semester}'
                    AND ID_EVAL_KELOMPOK_ASPEK IS NOT NULL
                    GROUP BY ID_EVAL_KELOMPOK_ASPEK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    ");
                array_push($data_rekap, array_merge($row, array('DATA_EVAL' => $rekap)));
            }
        } else {
            $row_fakultas = $this->db->QueryToArray("
                SELECT F.ID_FAKULTAS,F.NM_FAKULTAS
                FROM FAKULTAS F 
                WHERE F.ID_FAKULTAS IN (SELECT ID_FAKULTAS FROM EVALUASI_HASIL)
                ORDER BY ID_FAKULTAS");
            foreach ($row_fakultas as $row) {
                $rekap = $this->db->QueryToArray("
                    SELECT ID_EVAL_KELOMPOK_ASPEK,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4'
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM EVALUASI_HASIL EV
                    WHERE ID_EVAL_INSTRUMEN=3
                    AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=3) 
                    AND ID_FAKULTAS='{$row['ID_FAKULTAS']}'
                    AND ID_SEMESTER='{$semester}'
                    AND ID_EVAL_KELOMPOK_ASPEK IS NOT NULL
                    GROUP BY ID_EVAL_KELOMPOK_ASPEK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    ");
                array_push($data_rekap, array_merge($row, array('DATA_EVAL' => $rekap)));
            }
        }
        return $data_rekap;
    }

    function load_rekap_evamaba($fakultas, $semester) {
        $data_rekap = array();
        if ($fakultas != '') {
            $row_prodi = $this->db->QueryToArray("
                SELECT PS.ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI 
                FROM PROGRAM_STUDI PS 
                JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG 
                WHERE PS.ID_FAKULTAS='{$fakultas}' AND PS.ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=4 AND ID_SEMESTER='{$semester}')
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
            foreach ($row_prodi as $row) {
                $rekap = $this->db->QueryToArray("
                    SELECT ID_EVAL_KELOMPOK_ASPEK,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4'
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM EVALUASI_HASIL EV
                    WHERE ID_EVAL_INSTRUMEN=4
                    AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=4) 
                    AND ID_PROGRAM_STUDI='{$row['ID_PROGRAM_STUDI']}'
                    AND ID_SEMESTER='{$semester}'
                    AND ID_EVAL_KELOMPOK_ASPEK IS NOT NULL
                    GROUP BY ID_EVAL_KELOMPOK_ASPEK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    ");
                array_push($data_rekap, array_merge($row, array('DATA_EVAL' => $rekap)));
            }
        } else {
            $row_fakultas = $this->db->QueryToArray("
                SELECT F.ID_FAKULTAS,F.NM_FAKULTAS
                FROM FAKULTAS F 
                WHERE F.ID_FAKULTAS IN (SELECT ID_FAKULTAS FROM EVALUASI_HASIL)
                ORDER BY ID_FAKULTAS");
            foreach ($row_fakultas as $row) {
                $rekap = $this->db->QueryToArray("
                    SELECT ID_EVAL_KELOMPOK_ASPEK,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4'
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM EVALUASI_HASIL EV
                    WHERE ID_EVAL_INSTRUMEN=4
                    AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=4) 
                    AND ID_FAKULTAS='{$row['ID_FAKULTAS']}'
                    AND ID_SEMESTER='{$semester}'
                    AND ID_EVAL_KELOMPOK_ASPEK IS NOT NULL
                    GROUP BY ID_EVAL_KELOMPOK_ASPEK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    ");
                array_push($data_rekap, array_merge($row, array('DATA_EVAL' => $rekap)));
            }
        }
        return $data_rekap;
    }

    function load_rekap_evawisuda($fakultas, $periode) {
        $data_rekap = array();
        if ($fakultas != '') {
            $row_prodi = $this->db->QueryToArray("
                SELECT PS.ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI 
                FROM PROGRAM_STUDI PS 
                JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG 
                WHERE PS.ID_FAKULTAS='{$fakultas}' AND PS.ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=5 AND ID_TARIF_WISUDA='{$periode}')
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
            foreach ($row_prodi as $row) {
                $rekap = $this->db->QueryToArray("
                    SELECT ID_EVAL_KELOMPOK_ASPEK,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4'
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM EVALUASI_HASIL EV
                    WHERE ID_EVAL_INSTRUMEN=5
                    AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=5) 
                    AND ID_PROGRAM_STUDI='{$row['ID_PROGRAM_STUDI']}'
                    AND ID_TARIF_WISUDA='{$periode}'
                    AND ID_EVAL_KELOMPOK_ASPEK IS NOT NULL
                    GROUP BY ID_EVAL_KELOMPOK_ASPEK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    ");
                array_push($data_rekap, array_merge($row, array('DATA_EVAL' => $rekap)));
            }
        } else {
            $row_fakultas = $this->db->QueryToArray("
                SELECT F.ID_FAKULTAS,F.NM_FAKULTAS
                FROM FAKULTAS F 
                WHERE F.ID_FAKULTAS IN (SELECT ID_FAKULTAS FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=5)
                ORDER BY ID_FAKULTAS");
            foreach ($row_fakultas as $row) {
                $rekap = $this->db->QueryToArray("
                    SELECT ID_EVAL_KELOMPOK_ASPEK,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4'
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM EVALUASI_HASIL EV
                    WHERE ID_EVAL_INSTRUMEN=5
                    AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=5) 
                    AND ID_FAKULTAS='{$row['ID_FAKULTAS']}'
                    AND ID_TARIF_WISUDA='{$periode}'
                    AND ID_EVAL_KELOMPOK_ASPEK IS NOT NULL
                    GROUP BY ID_EVAL_KELOMPOK_ASPEK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    ");
                array_push($data_rekap, array_merge($row, array('DATA_EVAL' => $rekap)));
            }
        }
        return $data_rekap;
    }

    function load_evaluasi_adm_dep($fakultas, $semester) {
        $data_rekap = array();
        if ($fakultas != '') {
            $row_departemen = $this->db->QueryToArray("
                SELECT * FROM DEPARTEMEN D
                WHERE D.ID_FAKULTAS='{$fakultas}' AND D.ID_FAKULTAS IN (SELECT ID_FAKULTAS FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=6 AND ID_SEMESTER='{$semester}')
                ORDER BY D.ID_FAKULTAS,D.NM_DEPARTEMEN");
            foreach ($row_departemen as $row) {
                $rekap = $this->db->QueryToArray("
                    SELECT ID_EVAL_KELOMPOK_ASPEK,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4'
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM EVALUASI_HASIL EV
                    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=EV.ID_PROGRAM_STUDI
                    WHERE ID_EVAL_INSTRUMEN=6
                    AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=6) 
                    AND PS.ID_DEPARTEMEN='{$row['ID_DEPARTEMEN']}'
                    AND ID_SEMESTER='{$semester}'
                    AND ID_EVAL_KELOMPOK_ASPEK IS NOT NULL
                    GROUP BY ID_EVAL_KELOMPOK_ASPEK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    ");
                array_push($data_rekap, array_merge($row, array('DATA_EVAL' => $rekap)));
            }
        }
        return $data_rekap;
    }

    function load_rekap_adm_fak($fakultas, $semester) {
        $data_rekap = array();
        if ($fakultas != '') {
            $row_prodi = $this->db->QueryToArray("
                SELECT PS.ID_PROGRAM_STUDI,J.NM_JENJANG,PS.NM_PROGRAM_STUDI 
                FROM PROGRAM_STUDI PS 
                JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG 
                WHERE PS.ID_FAKULTAS='{$fakultas}' AND PS.ID_PROGRAM_STUDI IN (SELECT ID_PROGRAM_STUDI FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=6 AND ID_SEMESTER='{$semester}')
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI");
            foreach ($row_prodi as $row) {
                $rekap = $this->db->QueryToArray("
                    SELECT ID_EVAL_KELOMPOK_ASPEK,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4'
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM EVALUASI_HASIL EV
                    WHERE ID_EVAL_INSTRUMEN=6
                    AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=6) 
                    AND ID_PROGRAM_STUDI='{$row['ID_PROGRAM_STUDI']}'
                    AND ID_SEMESTER='{$semester}'
                    AND ID_EVAL_KELOMPOK_ASPEK IS NOT NULL
                    GROUP BY ID_EVAL_KELOMPOK_ASPEK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    ");
                array_push($data_rekap, array_merge($row, array('DATA_EVAL' => $rekap)));
            }
        } else {
            $row_fakultas = $this->db->QueryToArray("
                SELECT F.ID_FAKULTAS,F.NM_FAKULTAS
                FROM FAKULTAS F 
                WHERE F.ID_FAKULTAS IN (SELECT ID_FAKULTAS FROM EVALUASI_HASIL)
                ORDER BY ID_FAKULTAS");
            foreach ($row_fakultas as $row) {
                $rekap = $this->db->QueryToArray("
                    SELECT ID_EVAL_KELOMPOK_ASPEK,AVG(
                        CASE
                        WHEN NILAI_EVAL BETWEEN '1' AND '4'
                        THEN NILAI_EVAL
                        END
                    ) RATA
                    FROM EVALUASI_HASIL EV
                    WHERE ID_EVAL_INSTRUMEN=6
                    AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_ASPEK WHERE TIPE_ASPEK=1 AND ID_EVAL_INSTRUMEN=6) 
                    AND ID_FAKULTAS='{$row['ID_FAKULTAS']}'
                    AND ID_SEMESTER='{$semester}'
                    AND ID_EVAL_KELOMPOK_ASPEK IS NOT NULL
                    GROUP BY ID_EVAL_KELOMPOK_ASPEK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    ");
                array_push($data_rekap, array_merge($row, array('DATA_EVAL' => $rekap)));
            }
        }
        return $data_rekap;
    }

}

?>
