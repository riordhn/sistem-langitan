<?php

class master {

    public $db;
    private $id_role = 13;

    function __construct($db) {
        $this->db = $db;
    }

    //funngsi master instrumen

    function load_evaluasi_intrumen() {
        return $this->db->QueryToArray("SELECT * FROM EVALUASI_INSTRUMEN WHERE ID_ROLE='{$this->id_role}' ORDER BY ID_EVAL_INSTRUMEN");
    }

    function get_evaluasi_instrumen($id) {
        $this->db->Query("SELECT * FROM EVALUASI_INSTRUMEN WHERE ID_EVAL_INSTRUMEN='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_eval_instrumen($nama, $keterangan) {
        $this->db->Query("
            INSERT INTO EVALUASI_INSTRUMEN
                (NAMA_EVAL_INSTRUMEN,KET_EVAL_INSTRUMEN)
            VALUES
                ('{$nama}','{$keterangan}')");
    }

    function update_evaluasi_intrumen($id, $nama, $keterangan) {
        $this->db->Query("
            UPDATE EVALUASI_INSTRUMEN
            SET 
                NAMA_EVAL_INSTRUMEN='{$nama}',
                KET_EVAL_INSTRUMEN='{$keterangan}'
            WHERE ID_EVAL_INSTRUMEN='{$id}'");
    }

    //master kelompok aspek

    function load_kelompok_aspek($id) {
        return $this->db->QueryToArray("SELECT * FROM EVALUASI_KELOMPOK_ASPEK WHERE ID_EVAL_INSTRUMEN='{$id}' ORDER BY ID_EVAL_KELOMPOK_ASPEK");
    }

    function get_kelompok_aspek($id) {
        $this->db->Query("SELECT * FROM EVALUASI_KELOMPOK_ASPEK WHERE ID_EVAL_KELOMPOK_ASPEK='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_kelompok_aspek($id_ins, $nama) {
        $this->db->Query("
            INSERT INTO EVALUASI_KELOMPOK_ASPEK
                (ID_EVAL_INSTRUMEN,NAMA_KELOMPOK)
            VALUES
                ('{$id_ins}','{$nama}')");
    }

    function update_kelompok_aspek($id, $nama) {
        $this->db->Query("
            UPDATE EVALUASI_KELOMPOK_ASPEK
            SET
                NAMA_KELOMPOK='{$nama}'
            WHERE ID_EVAL_KELOMPOK_ASPEK='{$id}'
            ");
    }

    // master aspek instrumen

    function load_aspek($kelompok_aspek, $instrumen) {
        $arr_hasil = array();
        $data_aspek = $this->db->QueryToArray("
            SELECT EA.*,EKN.NM_KELOMPOK 
            FROM EVALUASI_ASPEK EA
            LEFT JOIN EVALUASI_KELOMPOK_NILAI EKN ON EKN.ID_KELOMPOK_NILAI=EA.ID_KELOMPOK_NILAI
            WHERE EA.ID_EVAL_INSTRUMEN='{$instrumen}' AND EA.ID_EVAL_KELOMPOK_ASPEK='{$kelompok_aspek}'
            ORDER BY EA.URUTAN,EA.ID_EVAL_ASPEK");
        foreach ($data_aspek as $a) {
            if ($a['ID_KELOMPOK_NILAI'] != '') {
                array_push($arr_hasil, array_merge($a, array(
                    'EVA_NILAI' => $this->db->QueryToArray("SELECT * FROM EVALUASI_NILAI WHERE ID_KELOMPOK_NILAI='{$a['ID_KELOMPOK_NILAI']}'")
                                )
                        )
                );
            } else {
                array_push($arr_hasil, $a);
            }
        }
        return $arr_hasil;
    }

    function get_aspek($aspek) {
        $this->db->Query("SELECT * FROM EVALUASI_ASPEK WHERE ID_EVAL_ASPEK='{$aspek}'");
        return $this->db->FetchAssoc();
    }

    function tambah_aspek($kelompok_aspek, $instrumen, $isi_aspek, $tipe, $kelompok_nilai, $urutan) {
        $this->db->Query("
            INSERT INTO EVALUASI_ASPEK
                (ID_EVAL_KELOMPOK_ASPEK,ID_EVAL_INSTRUMEN,EVALUASI_ASPEK,TIPE_ASPEK,ID_KELOMPOK_NILAI,URUTAN)
            VALUES
                ('{$kelompok_aspek}','{$instrumen}','{$isi_aspek}','{$tipe}','{$kelompok_nilai}','{$urutan}')");
    }

    function update_aspek($aspek, $isi_aspek, $tipe, $kelompok_nilai, $urutan) {
        $this->db->Query("
            UPDATE EVALUASI_ASPEK
            SET
                EVALUASI_ASPEK='{$isi_aspek}',
                TIPE_ASPEK='{$tipe}',
                ID_KELOMPOK_NILAI='{$kelompok_nilai}',
                URUTAN='{$urutan}'
            WHERE ID_EVAL_ASPEK='{$aspek}'");
    }

    //fungsi nilai aspek

    function load_nilai_aspek() {
        $arr_hasil = array();
        foreach ($this->db->QueryToArray("SELECT * FROM EVALUASI_KELOMPOK_NILAI ORDER BY NM_KELOMPOK") as $kel) {
            array_push($arr_hasil, array_merge($kel, array('EVA_NILAI' => $this->db->QueryToArray("SELECT * FROM EVALUASI_NILAI WHERE ID_KELOMPOK_NILAI='{$kel['ID_KELOMPOK_NILAI']}' ORDER BY URUTAN"))));
        }
        return $arr_hasil;
    }

    function get_nilai_aspek($id_kelompok) {
        return $this->db->QueryToArray("
            SELECT EKN.NM_KELOMPOK,EN.* 
            FROM EVALUASI_KELOMPOK_NILAI EKN
            JOIN EVALUASI_NILAI EN ON EKN.ID_KELOMPOK_NILAI=EN.ID_KELOMPOK_NILAI
            WHERE EKN.ID_KELOMPOK_NILAI='{$id_kelompok}'");
    }

    function tambah_kelompok_nilai_aspek($nm_kelompok,$id_pt) {
        $this->db->Query("INSERT INTO EVALUASI_KELOMPOK_NILAI (NM_KELOMPOK,ID_PERGURUAN_TINGGI) VALUES ('{$nm_kelompok}','{$id_pt}')");
    }

    function tambah_nilai_aspek($id_kelompok, $urutan, $nilai, $keterangan) {
        $this->db->Query("
            INSERT INTO EVALUASI_NILAI
                (ID_KELOMPOK_NILAI,URUTAN,NILAI_ASPEK,KET_NILAI_ASPEK)
            VALUES
                ('{$id_kelompok}','{$urutan}','{$nilai}','{$keterangan}')
            ");
    }

    function update_kelompok_nilai_aspek($id_kelompok, $nm_kelompok) {
        $this->db->Query("
            UPDATE EVALUASI_KELOMPOK_NILAI
                SET NM_KELOMPOK='{$nm_kelompok}'
            WHERE ID_KELOMPOK_NILAI='{$id_kelompok}'
            ");
    }

    function update_nilai_aspek($id_nilai, $nilai, $keterangan) {
        $this->db->Query("
            UPDATE EVALUASI_NILAI
                SET NILAI_ASPEK='{$nilai}',
                    KET_NILAI_ASPEK='{$keterangan}'
            WHERE ID_EVAL_NILAI_ASPEK='{$id_nilai}'");
    }

    function load_evaluasi_template($id_instrumen) {
        $this->db->Query("SELECT * FROM EVALUASI_INSTRUMEN WHERE ID_EVAL_INSTRUMEN='{$id_instrumen}'");
        $instrumen = $this->db->FetchAssoc();
        $data_instrumen = array_merge($instrumen,array('DATA_KELOMPOK'=>[]));
        $data_kelompok_aspek = $this->db->QueryToArray("SELECT * FROM EVALUASI_KELOMPOK_ASPEK WHERE ID_EVAL_INSTRUMEN='{$id_instrumen}' ORDER BY ID_EVAL_KELOMPOK_ASPEK");
        $index_kelompok = 0;
        foreach ($data_kelompok_aspek as $dk) {
            $data_aspek = $this->db->QueryToArray("SELECT * FROM EVALUASI_ASPEK WHERE ID_EVAL_KELOMPOK_ASPEK='{$dk['ID_EVAL_KELOMPOK_ASPEK']}' AND STATUS_AKTIF=0 ORDER BY URUTAN,ID_EVAL_ASPEK");
            array_push($data_instrumen['DATA_KELOMPOK'], array_merge($dk, array('DATA_ASPEK' => $data_aspek)));
            $index_aspek = 0;
            foreach ($data_aspek as $da) {
                array_push($data_instrumen['DATA_KELOMPOK'][$index_kelompok]['DATA_ASPEK'][$index_aspek], $this->db->QueryToArray("SELECT * FROM EVALUASI_NILAI WHERE ID_KELOMPOK_NILAI='{$da['ID_KELOMPOK_NILAI']}' ORDER BY URUTAN"));
                $index_aspek++;
            }
            $index_kelompok++;
        }
        return $data_instrumen;
    }

}
