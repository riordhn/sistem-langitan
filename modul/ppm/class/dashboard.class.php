<?php

class dashboard {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    function GetNamaInstrumen($eva) {
        return $this->db->QuerySingle("SELECT NAMA_EVAL_INSTRUMEN NAMA FROM EVALUASI_INSTRUMEN WHERE ID_EVAL_INSTRUMEN='{$eva}'");
    }

    function LoadSemesterEvaluasi($eva) {
        $tahun = date('Y');
        $tahun_setelah = $tahun + 1;
        $tahun_sebelum = $tahun - 1;
        $tahun_sebelum_2 = $tahun - 2;
        $arr_tahun = "'" . $tahun_sebelum_2 . "','" . $tahun_sebelum . "','" . $tahun . "','" . $tahun_setelah . "'";
        return $this->db->QueryToArray("
            SELECT * FROM AUCC.SEMESTER 
            WHERE ID_SEMESTER IN (
                SELECT DISTINCT(ID_SEMESTER) ID_SEMESTER
                FROM AUCC.EVALUASI_HASIL 
                WHERE ID_EVAL_INSTRUMEN='{$eva}'
            )
            AND THN_AKADEMIK_SEMESTER IN ({$arr_tahun})
            ORDER BY THN_AKADEMIK_SEMESTER DESC,NM_SEMESTER
            ");
    }

    function LoadPeriodeWisuda($eva) {
        return $this->db->QueryToArray("
            SELECT * FROM AUCC.TARIF_WISUDA
            WHERE ID_TARIF_WISUDA IN (
                SELECT DISTINCT(ID_TARIF_WISUDA) ID_TARIF_WISUDA
                FROM AUCC.EVALUASI_HASIL 
                WHERE ID_EVAL_INSTRUMEN='{$eva}'
            )
            ");
    }

    function LoadFakultasEvaluasi($eva) {
        return $this->db->QueryToArray("
            SELECT * FROM AUCC.FAKULTAS
            WHERE ID_FAKULTAS IN (
                SELECT DISTINCT(ID_FAKULTAS) ID_FAKULTAS 
                FROM AUCC.EVALUASI_HASIL 
                WHERE ID_EVAL_INSTRUMEN='{$eva}'
            )
            ");
    }

    function GetAvgEvaluasi($eva, $fakultas, $semester_or_periode) {
        if ($eva != 5) {
            return $this->db->QuerySingle("
            SELECT AVG(
                    CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                    THEN NILAI_EVAL
                    END
                ) RATA 
            FROM AUCC.EVALUASI_HASIL EH
            JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=EH.ID_SEMESTER
            JOIN AUCC.EVALUASI_ASPEK EA ON EA.ID_EVAL_ASPEK=EH.ID_EVAL_ASPEK
            WHERE EH.ID_SEMESTER='{$semester_or_periode}' AND EH.ID_FAKULTAS='{$fakultas}' AND EA.TIPE_ASPEK=1
            AND EH.ID_EVAL_INSTRUMEN='{$eva}'
            ");
        } else {
            return $this->db->QuerySingle("
            SELECT AVG(
                    CASE
                    WHEN NILAI_EVAL BETWEEN '1' AND '4' 
                    THEN NILAI_EVAL
                    END
                ) RATA 
            FROM AUCC.EVALUASI_HASIL EH
            JOIN AUCC.EVALUASI_ASPEK EA ON EA.ID_EVAL_ASPEK=EH.ID_EVAL_ASPEK
            WHERE EH.ID_TARIF_WISUDA='{$semester_or_periode}' AND EH.ID_FAKULTAS='{$fakultas}' AND EA.TIPE_ASPEK=1
            AND EH.ID_EVAL_INSTRUMEN='{$eva}'
            ");
        }
    }

    function GetJumlahMhs($eva, $fakultas, $semester_or_periode) {
        if ($eva != 5) {
            return $this->db->QuerySingle("
            SELECT COUNT(DISTINCT(ID_MHS)) JUMLAH_MHS
            FROM AUCC.EVALUASI_HASIL EH
            JOIN AUCC.SEMESTER S ON S.ID_SEMESTER=EH.ID_SEMESTER
            JOIN AUCC.EVALUASI_ASPEK EA ON EA.ID_EVAL_ASPEK=EH.ID_EVAL_ASPEK
            WHERE EH.ID_SEMESTER='{$semester_or_periode}' AND EH.ID_FAKULTAS='{$fakultas}' AND EA.TIPE_ASPEK=1
            AND EH.ID_EVAL_INSTRUMEN='{$eva}'
            ");
        } else {
            return $this->db->QuerySingle("
            SELECT COUNT(DISTINCT(ID_MHS)) JUMLAH_MHS
            FROM AUCC.EVALUASI_HASIL EH
            JOIN AUCC.EVALUASI_ASPEK EA ON EA.ID_EVAL_ASPEK=EH.ID_EVAL_ASPEK
            WHERE EH.ID_TARIF_WISUDA='{$semester_or_periode}' AND EH.ID_FAKULTAS='{$fakultas}' AND EA.TIPE_ASPEK=1
            AND EH.ID_EVAL_INSTRUMEN='{$eva}'
            ");
        }
    }

}

?>
