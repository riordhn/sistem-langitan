<?php

include 'config.php';


if (!empty($_GET)) {
    if (get('mode') == 'detail') {
        $id_komplain = get('k');
        $q_data_komplain = "
            SELECT P.NM_PENGGUNA,
            (CASE
                WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.DOSEN)
                THEN 'Dosen'
                WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.PEGAWAI)
                THEN 'Tenaga Kependidikan'
                ELSE 'Mahasiswa'
            END  
            ) STATUS
            ,KK.NM_KATEGORI,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA,K.*,TO_CHAR(K.WAKTU_KOMPLAIN,'DD-MM-YYYY HH24:MI:SS') WAKTU
            FROM AUCC.KOMPLAIN K
            LEFT JOIN AUCC.KOMPLAIN_KATEGORI KK ON K.ID_KOMPLAIN_KATEGORI=KK.ID_KOMPLAIN_KATEGORI
            JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=K.ID_UNIT_TERTUJU
            JOIN AUCC.PENGGUNA P ON K.ID_PELAPOR=P.ID_PENGGUNA
            WHERE K.ID_KOMPLAIN='{$id_komplain}'
            ";
        $db->Query($q_data_komplain);
        $data_komplain = $db->FetchAssoc();
        $q_data_tanggapan = "
            SELECT
            KT.*,P.NM_PENGGUNA PENJAWAB,
            (CASE
                WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.DOSEN)
                THEN 'Dosen'
                WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.PEGAWAI)
                THEN 'Tenaga Kependidikan'
            END  
            ) STATUS_PENJAWAB,TO_CHAR(KT.WAKTU_JAWAB,'DD-MM-YYYY HH24:MI:SS') WAKTU_JAWAB
            FROM AUCC.KOMPLAIN K
            JOIN AUCC.KOMPLAIN_TANGGAPAN KT ON KT.ID_KOMPLAIN=K.ID_KOMPLAIN
            JOIN AUCC.PENGGUNA P ON P.ID_PENGGUNA=KT.ID_PENJAWAB
            WHERE K.ID_KOMPLAIN='{$id_komplain}'
            ";
        $data_tanggapan = $db->QueryToArray($q_data_tanggapan);
        $unit_kerja_all = $db->QueryToArray("SELECT * FROM UNIT_KERJA WHERE TYPE_UNIT_KERJA NOT IN ('PRODI') AND ID_UNIT_KERJA NOT IN ({$data_komplain['ID_UNIT_TERTUJU']}) ORDER BY NM_UNIT_KERJA");
        $smarty->assign('data_unit_kerja', $unit_kerja_all);
        $smarty->assign('data_tanggapan', $data_tanggapan);
        $smarty->assign('komplain', $data_komplain);
    }
}
$q_data_komplain = "
SELECT P.NM_PENGGUNA,
(CASE
    WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.DOSEN)
    THEN 'Dosen'
    WHEN P.ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM AUCC.PEGAWAI)
    THEN 'Tenaga Kependidikan'
    ELSE 'Mahasiswa'
END  
) STATUS
,KK.NM_KATEGORI,UK.NM_UNIT_KERJA,UK.DESKRIPSI_UNIT_KERJA,K.*,TO_CHAR(K.WAKTU_KOMPLAIN,'DD-MM-YYYY HH24:MI:SS') WAKTU
FROM AUCC.KOMPLAIN K
LEFT JOIN AUCC.KOMPLAIN_KATEGORI KK ON K.ID_KOMPLAIN_KATEGORI=KK.ID_KOMPLAIN_KATEGORI
JOIN AUCC.UNIT_KERJA UK ON UK.ID_UNIT_KERJA=K.ID_UNIT_TERTUJU
JOIN AUCC.PENGGUNA P ON K.ID_PELAPOR=P.ID_PENGGUNA
ORDER BY K.WAKTU_KOMPLAIN DESC
";
$data_komplain = $db->QueryToArray($q_data_komplain);
$smarty->assign('data_komplain', $data_komplain);
$smarty->display('komplain/monitor-komplain.tpl');
?>
