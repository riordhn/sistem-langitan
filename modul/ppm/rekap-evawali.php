<?php

include 'config.php';
include 'class/rekapitulasi.class.php';
include '../keuangan/class/list_data.class.php';

$rekap = new rekapitulasi($db);
$list = new list_data($db);

$id_pt = $id_pt_user;

if (isset($_GET)) {
    if (get('mode') == 'tampil') {

        $fakultas = get('fakultas');
        $semester = get('semester');
        $prodi = get('prodi');
        $q_fakultas = $fakultas != '' ? "AND ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != '' ? "AND ID_PROGRAM_STUDI='{$prodi}'" : "";

        $smarty->assign('header_kelompok_aspek', $db->QueryToArray("
                    SELECT EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK,COUNT(EA.ID_EVAL_ASPEK)  JUMLAH_ASPEK
                    FROM EVALUASI_KELOMPOK_ASPEK EKS
                    JOIN EVALUASI_ASPEK EA ON EA.ID_EVAL_KELOMPOK_ASPEK=EKS.ID_EVAL_KELOMPOK_ASPEK AND EA.TIPE_ASPEK=1 AND EA.ID_EVAL_ASPEK IN 
                    (
                        SELECT ID_EVAL_ASPEK 
                        FROM EVALUASI_HASIL 
                        WHERE ID_SEMESTER='{$semester}' {$q_prodi}
                        {$q_fakultas}
                    )
                    WHERE EKS.ID_EVAL_INSTRUMEN=3
                    GROUP BY EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    "));

        $smarty->assign('data_rekap', $rekap->load_rekap_evawali(get('fakultas'), get('semester')));
    }
}
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->display('rekap-evawali.tpl');
?>
