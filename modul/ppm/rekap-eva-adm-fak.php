<?php

include 'config.php';
include 'class/rekapitulasi.class.php';
include '../keuangan/class/list_data.class.php';

$rekap = new rekapitulasi($db);
$list = new list_data($db);

$id_pt = $id_pt_user;

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $smarty->assign('header_kelompok_aspek', $db->QueryToArray("
                    SELECT EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK
                    FROM EVALUASI_KELOMPOK_ASPEK EKS
                    JOIN EVALUASI_ASPEK EA ON EA.ID_EVAL_KELOMPOK_ASPEK=EKS.ID_EVAL_KELOMPOK_ASPEK AND EA.TIPE_ASPEK=1
                    WHERE EKS.ID_EVAL_INSTRUMEN=6
                    GROUP BY EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    "));
        
        $smarty->assign('data_rekap', $rekap->load_rekap_adm_fak(get('fakultas'), get('semester')));
    }
}
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->display('rekap-eva-adm-fak.tpl');
?>
