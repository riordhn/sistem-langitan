<?php

include 'config.php';
include 'class/rekapitulasi.class.php';
include '../keuangan/class/list_data.class.php';

$rekap = new rekapitulasi($db);
$list = new list_data($db);

$id_pt = $id_pt_user;

if (isset($_GET)) {
    $periode = get('periode');
    if (get('mode') == 'tampil') {
        $smarty->assign('header_kelompok_aspek', $db->QueryToArray("
                    SELECT EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK
                    FROM EVALUASI_KELOMPOK_ASPEK EKS
                    JOIN EVALUASI_ASPEK EA ON EA.ID_EVAL_KELOMPOK_ASPEK=EKS.ID_EVAL_KELOMPOK_ASPEK
                    WHERE EKS.ID_EVAL_INSTRUMEN=5 AND EA.TIPE_ASPEK=1
                    AND EKS.ID_EVAL_KELOMPOK_ASPEK IN (SELECT ID_EVAL_KELOMPOK_ASPEK FROM AUCC.EVALUASI_HASIL WHERE ID_TARIF_WISUDA='{$periode}')
                    GROUP BY EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK
                    ORDER BY ID_EVAL_KELOMPOK_ASPEK
                    "));

        $smarty->assign('data_rekap', $rekap->load_rekap_evawisuda(get('fakultas'), get('periode')));
    }
}
$smarty->assign('data_periode', $db->QueryToArray("
    SELECT * FROM TARIF_WISUDA 
    WHERE ID_TARIF_WISUDA IN (
        SELECT ID_TARIF_WISUDA 
        FROM PERIODE_WISUDA
        WHERE TO_CHAR(TGL_BAYAR_SELESAI,'YYYY')=TO_CHAR(SYSDATE,'YYYY')
        OR TO_CHAR(TGL_BAYAR_SELESAI,'YYYY')=(TO_CHAR(SYSDATE,'YYYY')-1)
    ) 
    ORDER BY ID_TARIF_WISUDA DESC"));
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->display('rekap-evawisuda.tpl');
?>
