<?php

include 'config.php';
include 'class/master.class.php';

$master = new master($db);
$smarty->assign('data_nilai',$master->load_nilai_aspek());
$smarty->assign('data_instrumen_one', $master->get_evaluasi_instrumen(get('id')));
$smarty->assign('data_kelompok_aspek_one', $master->get_kelompok_aspek(get('id_kel')));
$smarty->display('master-aspek-tambah.tpl');
?>
