<?php

include 'config.php';
include 'class/dashboard.class.php';

$d = new dashboard($db);

if (get('eva') != 5) {
    $fakultas = $d->LoadFakultasEvaluasi(get('eva'));
    $semester = $d->LoadSemesterEvaluasi(get('eva'));
    $instrumen = $d->GetNamaInstrumen(get('eva'));
    echo "<chart caption='Grafik {$instrumen}' labelDisplay='ROTATE' slantLabels='1' showValues='1' xAxisName='Semester' yAxisName='Rata-Rata'  decimals='2' formatNumberScale='0'> palette='5'";
    echo "<categories font='Trebuchet MS'>";
    foreach ($fakultas as $f) {
        echo "<category label='{$f['SINGKATAN_FAKULTAS']}' toolText='{$f['NM_FAKULTAS']}' />";
    }
    echo "</categories>";
    foreach ($semester as $s) {
        echo "<dataset seriesName='{$s['NM_SEMESTER']} {$s['THN_AKADEMIK_SEMESTER']}' >";
        foreach ($fakultas as $f) {
            $rata = round($d->GetAvgEvaluasi(get('eva'), $f['ID_FAKULTAS'], $s['ID_SEMESTER']), 2);
            $rata = str_replace(",", ".", $rata);
            $jumlah_mhs = $d->GetJumlahMhs(get('eva'), $f['ID_FAKULTAS'], $s['ID_SEMESTER']);
            echo "<set value='{$rata}' baseFontSize='40'  toolText='{$f['NM_FAKULTAS']},{$s['NM_SEMESTER']} {$s['THN_AKADEMIK_SEMESTER']} Pengisi : {$jumlah_mhs}' />";
        }
        echo '</dataset>';
    }
    echo '<styles>

            <definition>

                <style type="font" name="CaptionFont" color="666666" size="15" />

                <style type="font" name="SubCaptionFont" bold="0" />

            </definition>

            <application>

                <apply toObject="caption" styles="CaptionFont" />

                <apply toObject="SubCaption" styles="SubCaptionFont" />

            </application>

        </styles>';
    echo '</chart>';
} else {
    $fakultas = $d->LoadFakultasEvaluasi(get('eva'));
    $periode = $d->LoadPeriodeWisuda(get('eva'));
    $instrumen = $d->GetNamaInstrumen(get('eva'));
    echo "<chart caption='Grafik {$instrumen}' labelDisplay='ROTATE' slantLabels='1' showValues='1' xAxisName='Semester' yAxisName='Rata-Rata'  decimals='2' formatNumberScale='0'> palette='5'";
    echo "<categories font='Trebuchet MS'>";
    foreach ($fakultas as $f) {
        echo "<category label='{$f['SINGKATAN_FAKULTAS']}' toolText='{$f['NM_FAKULTAS']}' />";
    }
    echo "</categories>";
    foreach ($periode as $p) {
        echo "<dataset seriesName='{$p['NM_TARIF_WISUDA']}' >";
        foreach ($fakultas as $f) {
            $rata = round($d->GetAvgEvaluasi(get('eva'), $f['ID_FAKULTAS'], $p['ID_TARIF_WISUDA']), 2);
            $rata = str_replace(",", ".", $rata);
            $jumlah_mhs = $d->GetJumlahMhs(get('eva'), $f['ID_FAKULTAS'], $p['ID_TARIF_WISUDA']);
            echo "<set value='{$rata}' toolText='{$f['NM_FAKULTAS']},{$p['NM_TARIF_WISUDA']} Pengisi : {$jumlah_mhs}' />";
        }
        echo '</dataset>';
    }
    echo '<styles>

            <definition>

                <style type="font" name="CaptionFont" color="666666" size="15" />

                <style type="font" name="SubCaptionFont" bold="0" />

            </definition>

            <application>

                <apply toObject="caption" styles="CaptionFont" />

                <apply toObject="SubCaption" styles="SubCaptionFont" />

            </application>

        </styles>';
    echo '</chart>';
}
?>
