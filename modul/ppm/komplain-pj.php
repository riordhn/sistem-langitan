<?php

include 'config.php';


if (!empty($_POST)) {
    if (post('mode') == 'tambah-pj') {
        $pengguna = post('pengguna');
        $status = post('status');
        $id_unit = post('unit');
        if ($status == '1') {
            $db->Query("UPDATE KOMPLAIN_PJ SET PENANGGUNGJAWAB=0 WHERE ID_UNIT_KERJA='{$id_unit}'");
        }
        $db->Query("INSERT INTO KOMPLAIN_PJ (ID_PENGGUNA,ID_UNIT_KERJA,PENANGGUNGJAWAB) VALUES ('{$pengguna}','{$id_unit}','{$status}')");
    } else if (post('mode') == 'hapus-pj') {
        $id_pj = post('id_pj');
        $db->Query("DELETE FROM KOMPLAIN_PJ WHERE ID_KOMPLAIN_PJ='{$id_pj}'");
    }
}


$arr_data_unit = array();
$data_unit = $db->QueryToArray("SELECT * FROM AUCC.UNIT_KERJA WHERE TYPE_UNIT_KERJA IN ('FAKULTAS','LEMBAGA','REKTORAT') ORDER BY TYPE_UNIT_KERJA,ID_FAKULTAS,NM_UNIT_KERJA");

foreach ($data_unit as $d) {
    $data_pengguna_unit = $db->QueryToArray("
        SELECT ID_PENGGUNA,GELAR_DEPAN,GELAR_BELAKANG,NM_PENGGUNA
        FROM PENGGUNA
        WHERE ID_PENGGUNA IN (
            SELECT ID_PENGGUNA 
            FROM PEGAWAI 
            WHERE ID_UNIT_KERJA='{$d['ID_UNIT_KERJA']}'
        )
        OR ID_PENGGUNA IN (
            SELECT D.ID_PENGGUNA 
            FROM DOSEN D
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=D.ID_PROGRAM_STUDI_SD
            WHERE PS.ID_FAKULTAS='{$d['ID_FAKULTAS']}'
        )
        
    ");
    $data_pj = $db->QueryToArray("
        SELECT KPJ.*,GELAR_DEPAN,GELAR_BELAKANG,NM_PENGGUNA
        FROM KOMPLAIN_PJ KPJ
        JOIN PENGGUNA P ON KPJ.ID_PENGGUNA=P.ID_PENGGUNA
        WHERE KPJ.ID_UNIT_KERJA='{$d['ID_UNIT_KERJA']}'
        ORDER BY PENANGGUNGJAWAB DESC,NM_PENGGUNA
    ");
    array_push($arr_data_unit, array_merge($d, array(
        'PENGGUNA' => $data_pengguna_unit,
        'PJ' => $data_pj
    )));
}
$smarty->assign('data_unit', $arr_data_unit);
$smarty->display('komplain/komplain-pj.tpl');
?>
