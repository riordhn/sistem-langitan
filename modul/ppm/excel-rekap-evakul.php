<?php
include 'config.php';
include 'class/rekapitulasi.class.php';
include '../keuangan/class/list_data.class.php';

$rekap = new rekapitulasi($db);
$list = new list_data($db);

$fakultas = get('fakultas');
$semester = get('semester');
$prodi = get('prodi');
$q_prodi = $prodi != '' ? "AND ID_PROGRAM_STUDI='{$prodi}'" : "";

$header_kelompok_aspek = $db->QueryToArray("
    SELECT EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK,COUNT(EA.ID_EVAL_ASPEK)  JUMLAH_ASPEK
    FROM EVALUASI_KELOMPOK_ASPEK EKS
    JOIN EVALUASI_ASPEK EA ON EA.ID_EVAL_KELOMPOK_ASPEK=EKS.ID_EVAL_KELOMPOK_ASPEK AND EA.TIPE_ASPEK=1 AND EA.ID_EVAL_ASPEK IN 
    (
        SELECT ID_EVAL_ASPEK 
        FROM EVALUASI_HASIL 
        WHERE ID_SEMESTER='{$semester}'
        AND ID_FAKULTAS='{$fakultas}' {$q_prodi}
    )
    WHERE EKS.ID_EVAL_INSTRUMEN=1
    GROUP BY EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK
    ORDER BY ID_EVAL_KELOMPOK_ASPEK
    ");

$data_rekap = $rekap->load_rekap_evakul($fakultas, $semester);

ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #220344;
        text-transform:capitalize;
    }
    td{
        text-align: left;
    }
</style>
<table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="<?= count($header_kelompok_aspek) * 2 + 2 ?>" class="header_text">
                Excel Rekapitulasi Evaluasi Perkuliahan
            </td>
        </tr>
        <tr>
            <td class="header_text" rowspan="2">NO</td>
            <td class="header_text" rowspan="2">
                <?php
                if (get('fakulas') == '') {
                    echo "Fakultas";
                } else {
                    echo "Program Studi";
                }
                ?>
            </td>
            <?php
            foreach ($header_kelompok_aspek as $hka):
                $kelompok = strtoupper($hka['NAMA_KELOMPOK']);
                echo '<td class="header_text"  colspan="2">' . $kelompok . "</td>";
            endforeach;
            ?>
        </tr>
        <tr>
            <?php
            foreach ($header_kelompok_aspek as $hka):
                echo '<td class="header_text" >Rerata Aspek</td>';
                echo '<td class="header_text" >Persen Aspek</td>';
            endforeach;
            ?>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data_rekap as $d):
            if ($fakultas == '') {
                echo "
                <tr>
                    <td>{$no}</td>
                    <td>{$d['NM_FAKULTAS']}</td>";
            } else {
                echo "
                <tr>
                    <td>{$no}</td>
                    <td>({$d['NM_JENJANG']}) {$d['NM_PROGRAM_STUDI']}</td>
                    ";
            }

            foreach ($d['DATA_EVAL'] as $r):
                $rekap = round($r['RATA'], 2);
                $rekap_prosen = round($r['RATA'] / 4 * 200, 2);
                echo "
                    <td>{$rekap}</td>
                    <td>{$rekap_prosen}</td>
                ";
            endforeach;
            echo "</tr>";
            $no++;
        endforeach;
        ?>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "Data-Rekap-Evaluasi-Perkuliahan-" . date('d_m_Y-H.i.s') . "";

header("Content-disposition: attachment; filename={$nm_file}.xls");
header("Content-type: application/vnd.ms-excel");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>




