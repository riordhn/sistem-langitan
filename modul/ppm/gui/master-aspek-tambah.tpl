<div class="center_title_bar">Tambah Aspek Instrumen</div>
<form id="form_tambah" method="post" action="master-aspek.php?mode=aspek&id={$smarty.get.id}&id_kel={$smarty.get.id_kel}">
    <table class="ui-widget-content" style="width: 70%">
        <caption>
            NAMA INSTRUMEN : {$data_instrumen_one.NAMA_EVAL_INSTRUMEN}<br/>
            NAMA KELOMPOK ASPEK : {$data_kelompok_aspek_one.NAMA_KELOMPOK}
        </caption>
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="2">Tambah Aspek Instrumen</th>
        </tr>
        <tr>
            <td>Urutan Aspek Instrumen</td>
            <td>
                <input type="text" name="urutan" class="required number"/>
            </td>
        </tr>
        <tr>
            <td>Nama Aspek Instrumen</td>
            <td>
                <textarea name="aspek" cols="40" class="required"></textarea>
            </td>
        </tr>
        <tr>
            <td>Tipe Aspek</td>
            <td>
                <select name="tipe" onchange="if($(this).val()==1){ $('.pilih-nilai').show() } else{ $('.pilih-nilai').hide() }">
                    <option value="1">Pilihan</option>
                    <option value="2">Isian</option>
                </select>
            </td>
        </tr>
        <tr class="pilih-nilai ui-widget-header">
            <td colspan="2" class="center">Pilihan Nilai Aspek</td>
        </tr>
        {foreach $data_nilai as $n}
            <tr class="pilih-nilai">
                <td colspan="2" >
                    <input type="radio" name="nilai" value="{$n.ID_KELOMPOK_NILAI}" />Kelompok : {$n.NM_KELOMPOK}<br/>
                    <p>
                        {foreach $n.EVA_NILAI as $en}
                            {$en@index+1}. Nilai {$en.NILAI_ASPEK} ({$en.KET_NILAI_ASPEK})<br/>
                        {/foreach}
                    </p>
                </td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" value="tambah" name="mode"/>
                <input type="hidden" value="{$smarty.get.id}" name="id_ins"/>
                <input type="hidden" value="{$smarty.get.id_kel}" name="id_kel"/>
                <input type="submit" value="Tambah" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;"/>
                <a class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" href="master-aspek.php?mode=aspek&id={$smarty.get.id}&id_kel={$smarty.get.id_kel}" >Kembali</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#form_tambah').validate();
    </script>
{/literal}