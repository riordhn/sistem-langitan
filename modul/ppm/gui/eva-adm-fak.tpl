<div class="center_title_bar">Laporan Evaluasi Administrasi Fakultas</div>
<form method="get" id="report_form" action="eva-adm-fak.php">
    <table class="ui-widget-content" style="width: 500px">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Parameter</th>
        </tr>
        <tr>
            <td>Semester</td>
            <td>
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$smarty.get.semester}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_evaluasi)}
    <table class="ui-widget-content" style="width: 99%">
        <tr class="ui-widget-header">
            <th colspan="{count($header_aspek)+2}">Hasil Evaluasi Administrasi Fakultas</th>
        </tr>
        <tr class="ui-widget-header">
            <th style="vertical-align: middle" rowspan="2">NO</th>
            <th style="vertical-align: middle" rowspan="2">FAKULTAS</th>
            {foreach $header_kelompok_aspek as $hka}
                <th colspan="{$hka.JUMLAH_ASPEK}">{$hka.NAMA_KELOMPOK}</th>
            {/foreach}
        </tr>
        <tr class="ui-widget-header">
            {foreach $header_aspek as $ha}
                <th>{$ha.EVALUASI_ASPEK}</th>
            {/foreach}
        </tr>
        {foreach $data_evaluasi as $eva}
            <tr>
                <td>{$eva@index+1}</td>
                <td>{$eva.NM_FAKULTAS}</td>
                {foreach $eva.DATA_HASIL as $hasil}
                    <td>{round($hasil.RATA,2)}  <br/><br/> Nilai 0 : {$hasil.KOSONG}</td>
                {/foreach}
            </tr>
        {foreachelse}
            <tr>
                <td colspan="{count($header_aspek)+2}" class="kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
    </table>
{/if}

{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}