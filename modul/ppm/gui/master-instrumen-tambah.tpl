<div class="center_title_bar">Tambah Master Intrumen</div>
<form id="form_tambah" method="post" action="master-instrumen.php">
    <table class="ui-widget-content" style="width: 60%">
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="2">Tambah Master Intrumen</th>
        </tr>
        <tr>
            <td>Nama Intrumen</td>
            <td>
                <input type="text" name="nama" class="required" />
            </td>
        </tr>
        <tr>
            <td>Keterangan Intrumen</td>
            <td>
                <textarea name="keterangan"></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" value="tambah" name="mode"/>
                <input type="submit" value="Tambah" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;"/>
                <a href="master-instrumen.php" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;">Batal</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#form_tambah').validate();
    </script>
{/literal}