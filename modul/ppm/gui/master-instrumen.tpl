<div class="center_title_bar">Master Instrumen</div>
<table class="ui-widget-content">
    <tr class="ui-widget-header">
        <th>NO</th>
        <th>NAMA EVALUASI</th>
        <th>KETERANGAN EVALUASI</th>
        <th>STATUS BUKA UNTUK MAHASISWA</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_intrumen as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data.NAMA_EVAL_INSTRUMEN}</td>
            <td>{$data.KET_EVAL_INSTRUMEN}</td>
            <td class="center">
                {if $data.STATUS_BUKA==1}
                    <span style="color: green">Terbuka</span>
                {else}
                    <span style="color: red">Tertutup</span>
                {/if}
            </td>
            <td class="center">
                {if $data.STATUS_BUKA==1}
                    <form action="master-instrumen.php" style="display: inline" method="post">
                        <input type="hidden" name="id" value="{$data.ID_EVAL_INSTRUMEN}" />
                        <input type="hidden" name="mode" value="close"/>
                        <input type="submit" class="ui-button ui-corner-all ui-state-active" style="padding:4px;cursor:pointer;" value="Tutup"/>
                    </form>
                {else}
                    <form action="master-instrumen.php" style="display: inline" method="post">
                        <input type="hidden" name="id" value="{$data.ID_EVAL_INSTRUMEN}" />
                        <input type="hidden" name="mode" value="open"/>
                        <input type="submit" class="ui-button ui-corner-all ui-state-active" style="padding:4px;cursor:pointer;" value="Buka"/>
                    </form>
                {/if}
                <a href="master-instrumen-edit.php?id={$data.ID_EVAL_INSTRUMEN}" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;">Edit</a>
                <a href="master-instrumen-template.php?id={$data.ID_EVAL_INSTRUMEN}" class="ui-button ui-corner-all ui-state-highlight" style="padding:5px;cursor:pointer;">Template</a>
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="5" class="kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="5" class="center">
            <a class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" href="master-instrumen-tambah.php">Tambah</a>
        </td>
    </tr>
    <tr>
        <td colspan="5" class="center">
            <p><b>PENGUMUMAN EVALUASI (JIKA DALAM KEADAAN TERTUTUP)</b></p>
            <form action="master-instrumen.php" style="display: inline" method="post">
                <input type="hidden" name="mode" value="pengumuman"/>
                <textarea name="pengumuman" style="width: 80%;height: 100px;resize: none">{$pengumuman}</textarea><br/>
                <input type="submit" class="ui-button ui-corner-all ui-state-active" style="padding:4px;cursor:pointer;" value="Update Pengumuman"/>
            </form>
        </td>
    </tr>

</table>    