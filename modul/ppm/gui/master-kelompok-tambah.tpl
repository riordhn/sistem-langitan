<div class="center_title_bar">Tambah Kelompok Aspek</div>
<form id="form_tambah" method="post" action="master-kelompok-aspek.php?mode=kelompok&id={$smarty.get.id}">
    <table class="ui-widget-content" style="width: 70%">
        <caption>NAMA INSTRUMEN :{$data_instrumen.NAMA_EVAL_INSTRUMEN}</caption>
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="2">Tambah Kelompok Aspek</th>
        </tr>
        <tr>
            <td>Nama Kelompok Aspek</td>
            <td>
                <textarea name="nama" class="required"></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" value="tambah" name="mode"/>
                <input type="hidden" value="{$smarty.get.id}" name="id_ins"/>
                <input type="submit" value="Tambah" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;"/>
                <a href="master-kelompok-aspek.php?mode=kelompok&id={$smarty.get.id}" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;">Batal</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#form_tambah').validate();
    </script>
{/literal}