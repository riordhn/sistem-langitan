<html>
    <head>
        <title>PPM - {$nama_pt}</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="../../css/reset.css" />
        <link rel="stylesheet" type="text/css" href="../../css/text.css" />
        <link rel="stylesheet" type="text/css" href="../../css/ppm-style.css" />
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui-ppm.custom.css" />
        
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css" />
        <link rel="stylesheet" type="text/css" href="../spi/css/chosen.css">

        <script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
        <script type="text/javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
        <script language="javascript" src="../../js/jquery.validate.js"></script>
        <script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="js/jquery.dataTables.js" ></script>
        <script type="text/javascript" src="js/jquery.raty.min.js"></script>
        <script type="text/javascript" src="../spi/js/chosen.jquery.min.js" ></script>
        <script type="text/javascript">var defaultRel = 'acc'; var defaultPage = 'ubah-password.php';</script>
        <script type="text/javascript" src="../../js/cybercampus.ajax-1.0.js"></script>
    </head>
    <body>
        <table class="clear-margin-bottom">
            <colgroup>
                <col />
                <col class="main-width"/>
                <col />
            </colgroup>
            <thead>
                <tr>
                    <td class="header-left"></td>
                    <td class="header-center" style="background-image: url('../../img/header/ppm-{$nama_singkat}.png')"></td>
                    <td class="header-right"></td>
                </tr>
                <tr>
                    <td class="tab-left"></td>
                    <td class="tab-center">
                        <ul>
                            <!-- Untuk Menampilkan Menu Utama  -->
                            {foreach $modul_set as $m}
                                {if $m.AKSES == 1}
                                    <li><a href="#{$m.NM_MODUL}!{$m.PAGE}" class="nav">{$m.TITLE}</a></li>
                                    <li class="divider" style="width: 1px;"></li>
                                {/if}
                            {/foreach}
                            <li><a class="disable-ajax" href="../../logout.php">Logout</a></li>
                        </ul>
                    </td>
                    <td class="tab-right"></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="body-left">&nbsp;</td>
                    <td class="body-center">
                        <table class="content-table">
                            <colgroup>
                                <col />
                            </colgroup>
                            <tr>
                                <td colspan="2" id="breadcrumbs" class="breadcrumbs" ></td>
                            </tr>
                            <tr>
                                <td id="menu" class="menu"></td>
                                <td id="content" class="content">Loading data...</td>
                            </tr>
                        </table>
                    </td>
                    <td class="body-right">&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td class="foot-left">&nbsp;</td>
                    <td class="foot-center">
                        <div class="footer-nav">
                            <a href="">Home</a> | <a href="">About</a> | <a href="">Sitemap</a> | <a href="">RSS</a> | <a href="">Contact Us</a>
                        </div>
                        <div class="footer">Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU <br />oleh <a target="_blank" href="http://unair.ac.id" class="disable-ajax">Universitas Airlangga</a></div>
                    </td>
                    <td class="foot-right">&nbsp;</td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>