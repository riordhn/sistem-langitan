<div class="center_title_bar">Grafik Evaluasi Perwalian</div>
<div style="width: 100%;text-align: center">
    <div id="chartEva3" style="width: 100%;margin: auto;text-align: center">Loading</div>          
    <script type="text/javascript" src="../../fusionchart/FusionCharts.js"></script>
    <script type="text/javascript">
        var myChart = new FusionCharts( "../../swf/Charts/MSColumn3D.swf", 
        "myChartId3", "900", "700", "0", "1" );
        myChart.setXMLUrl("dash-data.php?eva=3");
        myChart.render("chartEva3");
    </script>
</div>