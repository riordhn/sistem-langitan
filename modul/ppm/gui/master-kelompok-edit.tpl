<div class="center_title_bar">Edit Kelompok Aspek</div>
<form id="form_tambah" method="post" action="master-kelompok-aspek.php?mode=kelompok&id={$smarty.get.id}">
    <table class="ui-widget-content" style="width: 60%">
        <caption>NAMA INSTRUMEN :{$data_instrumen.NAMA_EVAL_INSTRUMEN}</caption>
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="2">Edit Kelompok Aspek</th>
        </tr>
        <tr>
            <td>Nama Kelompok Aspek</td>
            <td>
                <textarea name="nama" class="required">{$data_kelompok_aspek.NAMA_KELOMPOK}</textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" value="edit" name="mode"/>
                <input type="hidden" value="{$smarty.get.id_kel}" name="id_kel"/>
                <input type="submit" value="Update" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;"/>
                <a href="master-kelompok-aspek.php?mode=kelompok&id={$smarty.get.id}" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;">Batal</a>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#form_tambah').validate();
    </script>
{/literal}