<div class="center_title_bar">Master Kelompok Aspek</div>
{if $smarty.get.mode=='kelompok'}
    <table class="ui-widget-content" style="width: 60%">
        <caption>NAMA INSTRUMEN : {$data_instrumen_one.NAMA_EVAL_INSTRUMEN}</caption>
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>KELOMPOK ASPEK</th>
            <th>Operasi</th>
        </tr>
        {foreach $data_kelompok_aspek as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>{$data.NAMA_KELOMPOK}</td>
                <td class="center"><a href="master-kelompok-edit.php?id={$smarty.get.id}&id_kel={$data.ID_EVAL_KELOMPOK_ASPEK}" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;">Edit</a></td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="3" class="kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="3" class="center">
                <a class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" href="master-kelompok-tambah.php?id={$smarty.get.id}">Tambah</a>
                <a class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" href="master-kelompok-aspek.php">Kembali</a>
            </td>
        </tr>
    </table>
{else}
    <table class="ui-widget-content" style="width: 90%">
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>NAMA EVALUASI</th>
            <th>KETERANGAN EVALUASI</th>
            <th>Operasi</th>
        </tr>
        {foreach $data_instrumen as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>{$data.NAMA_EVAL_INSTRUMEN}</td>
                <td>{$data.KET_EVAL_INSTRUMEN}</td>
                <td class="center"><a href="master-kelompok-aspek.php?mode=kelompok&id={$data.ID_EVAL_INSTRUMEN}" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;">Lihat Kelompok Aspek</a></td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="4" class="kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
    </table>    
{/if}
