<div class="center_title_bar">Edit Master Intrumen</div>
<form id="form_edit" method="post" action="master-instrumen.php" >
    <table class="ui-widget-content" style="width: 60%">
        <tr class="ui-widget-header">
            <th class="header-coloumn" colspan="2">Edit Master Intrumen</th>
        </tr>
        <tr>
            <td>Nama Intrumen</td>
            <td>
                <input type="text" name="nama" value="{$data_instrumen.NAMA_EVAL_INSTRUMEN}" />
            </td>
        </tr>
        <tr>
            <td>Keterangan Intrumen</td>
            <td>
                <textarea name="keterangan">{$data_instrumen.KET_EVAL_INSTRUMEN}</textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <input type="hidden" value="edit" name="mode"/>
                <input type="hidden" value="{$data_instrumen.ID_EVAL_INSTRUMEN}" name="id"/>
                <input type="submit" value="Update" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;"/>
                <a href="master-instrumen.php" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;">Batal</a>
            </td>
        </tr>
    </table>
</form>