{if $smarty.get.mode=='detail'}
    <div class="center_title_bar">Detail Komplain </div>
    <fieldset style="width: 98%">
        <legend>Komplain Prosedur</legend>
        <div class="field_form">
            <label>Pelapor : </label>
            {$komplain.NM_PENGGUNA|upper} - {$komplain.STATUS} ({$komplain.WAKTU})
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Jenis Komplain : </label>
            {$komplain.NM_KATEGORI|upper} - {$komplain.DESKRIPSI_UNIT_KERJA}
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Isi Komplain: </label>
            {$komplain.ISI_KOMPLAIN}
        </div>
        <div class="clear-fix"></div>
        <div class="field_form">
            <label>Saran dan Tindakan: </label>
            {$komplain.SARAN_TINDAKAN}
        </div>
        {if $komplain.STATUS_KOMPLAIN==4}
            <div class="field_form">
                <label style="font-size: 1.2em;color: blue;">Respon Tanggapan</label>
            </div>
            <div class="clear-fix"></div>
            <p></p>
            <table cellpadding="0" cellspacing="0" border="0" class="datatable display" >
                <thead>
                    <tr>
                        <th>Penjawab</th>
                        <th>Analisa Tindakan</th>
                        <th>Tindakan</th>
                        <th>Keterangan</th>
                        <th>Tanggal Tindakan</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $data_tanggapan as $d}
                        <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                            <td style="width: 220px">
                                {$d.PENJAWAB|upper} - {$d.STATUS_PENJAWAB}<br/>
                                {$d.WAKTU_JAWAB}
                            </td>
                            <td>{$d.ANALISA_PENYEBAB}</td>
                            <td>{$d.TINDAKAN}</td>
                            <td>{$d.KETERANGAN}</td>
                            <td>
                                {if $d.TGL_SELESAI_RENCANA==''&&$d.TGL_SELESAI_REALISASI==''}
                                    <span style="color: chocolate">Tidak ada tindak Lanjut</span>
                                {else}
                                    Rencana   : {$d.TGL_SELESAI_RENCANA}<br/>
                                    Realisasi : {$d.TGL_SELESAI_REALISASI}
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
            {if $komplain.KETERANGAN_TANGGAPAN!=''}
                <div class="field_form">
                    <label style="font-size: 1.2em;color: blue;">Penilaian</label>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Nilai</label>
                    <div id="star" data-score="{$komplain.NILAI_TANGGAPAN}" style="display: inline-block;"></div>
                </div>
                <div class="clear-fix"></div>
                <div class="field_form">
                    <label>Keterangan Nilai</label>
                    {$komplain.KETERANGAN_TANGGAPAN}
                </div>
                {literal}
                    <script type="text/javascript">
                        $('#star').raty({
                            score: function() {
                                return $(this).attr('data-score');
                            },
                            readOnly: true
                        });
                    </script>
                {/literal}
            {/if}
            <div class="bottom_field_form">
                <a href="monitor-komplain.php" class="button">Kembali</a>
            </div>
        {else if $komplain.STATUS_KOMPLAIN==2||$komplain.STATUS_KOMPLAIN==3}
            <div class="field_form">
                <label style="font-size: 1.2em;color: blue;">Respon Tanggapan</label>
            </div>
            <div class="clear-fix"></div>
            <p></p>
            <table cellpadding="0" cellspacing="0" border="0" class="datatable display" >
                <thead>
                    <tr>
                        <th>Penjawab</th>
                        <th>Analisa Tindakan</th>
                        <th>Tindakan</th>
                        <th>Keterangan</th>
                        <th>Tanggal Tindakan</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $data_tanggapan as $d}
                        <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                            <td style="width: 220px">
                                {$d.PENJAWAB|upper} - {$d.STATUS_PENJAWAB}<br/>
                                {$d.WAKTU_JAWAB}
                            </td>
                            <td>{$d.ANALISA_PENYEBAB}</td>
                            <td>{$d.TINDAKAN}</td>
                            <td>{$d.KETERANGAN}</td>
                            <td>
                                {if $d.TGL_SELESAI_RENCANA==''&&$d.TGL_SELESAI_REALISASI==''}
                                    <span style="color: chocolate">Tidak ada tindak Lanjut</span>
                                {else}
                                    Rencana   : {$d.TGL_SELESAI_RENCANA}<br/>
                                    Realisasi : {$d.TGL_SELESAI_REALISASI}
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
            <div class="clear-fix"></div>
            <div id="aksi" class="bottom_field_form">
                <a href="monitor-komplain.php" class="button">Kembali</a>
            </div>
        {else}
            <div class="clear-fix"></div>
            <div id="aksi" class="bottom_field_form">
                <a href="monitor-komplain.php" class="button">Kembali</a>
            </div>
        {/if}
    </fieldset>
{else}
    <div class="center_title_bar">Monitoring Komplain (PTPP)</div>
    <h2>Data Komplain </h2>
    <table cellpadding="0" cellspacing="0" border="0" class="datatable display" >
        <thead>
            <tr>
                <th>Pelapor</th>
                <th>Tanggal</th>
                <th>Jenis Komplain & Unit Kerja Dituju</th>
                <th>Isi Komplain</th>
                <th>Saran</th>
                <th class="center">Status</th>
            </tr>
        </thead>
        <tbody>
            {foreach $data_komplain as $d}
                <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                    <td style="width: 220px">
                        {$d.NM_PENGGUNA|upper} - {$d.STATUS}<br/>
                    </td>
                    <td>
                        {$d.WAKTU}
                    </td>
                    <td style="width: 220px">
                        {$d.NM_KATEGORI}<br/> ({$d.NM_UNIT_KERJA})
                    </td>
                    <td>{SplitWord($d.ISI_KOMPLAIN,10)}...</td>
                    <td>{SplitWord($d.SARAN_TINDAKAN,10)}...</td>
                    <td class="center">
                        {if $d.STATUS_KOMPLAIN==1}
                            <label style="color: #009999;font-weight: bold">Komplain Baru</label><br/>
                            <a href="monitor-komplain.php?mode=detail&k={$d.ID_KOMPLAIN}" class="button">Detail</a>
                        {elseif $d.STATUS_KOMPLAIN==2}
                            <label style="color: #ff6600;font-weight: bold">Terjawab</label><br/>
                            <a href="monitor-komplain.php?mode=detail&k={$d.ID_KOMPLAIN}" class="button">Detail</a>
                        {elseif $d.STATUS_KOMPLAIN==3}
                            <label style="color: #ff0000;font-weight: bold">Sedang Tindak Lanjuti</label><br/>
                            <a href="monitor-komplain.php?mode=detail&k={$d.ID_KOMPLAIN}" class="button">Detail</a>
                        {elseif $d.STATUS_KOMPLAIN==4}
                            <label style="color: #00bb00;font-weight: bold">Sudah Selesai</label><br/>
                            <a href="monitor-komplain.php?mode=detail&k={$d.ID_KOMPLAIN}" class="button">Detail</a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{/if}
{literal}
    <script type="text/javascript">
        $(document).ready(function() {
            $('.datatable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "aaSorting": [],
            });
            $('form').validate();
            $('#jawaban').validate();
            $('#tindakan').validate();
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-M-y'
            }).css({'text-transform': 'uppercase'});
        });
    </script>
{/literal}