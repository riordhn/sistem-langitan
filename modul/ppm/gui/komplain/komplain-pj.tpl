<div class="center_title_bar">Penjawab Komplain  Komplain</div>
<h2>Data Unit dan Penjawab Jawab </h2>
<table cellpadding="0" cellspacing="0" border="0" class="ui-widget" style="width: 98%" >
    <thead class="ui-widget-header">
        <tr>
            <th>Nama Unit</th>
            <th>Deskripsi</th>
            <th>Penanggung Jawab</th>
            <th class="center">--</th>
        </tr>
    </thead>
    <tbody class="ui-widget-content">
        {foreach $data_unit as $d}
            <tr {if $d@index%2==0}class="even"{else}class="odd"{/if}>
                <td>{$d.NM_UNIT_KERJA}</td>
                <td>{if $d.DESKRIPSI_UNIT_KERJA!=''}({$d.DESKRIPSI_UNIT_KERJA}){/if}</td>
                <td>
                    <ul style="margin-left: 10px">
                        {foreach $d.PJ as $pj}
                            <li>{$pj.GELAR_DEPAN} {$pj.NM_PENGGUNA} {$pj.GELAR_BELAKANG} 
                                <form method="post" action="komplain-pj.php" style="display: inline-block;vertical-align: middle">
                                    <input type="hidden" name="id_pj" value="{$pj.ID_KOMPLAIN_PJ}"/>
                                    <input type="hidden" name="mode" value="hapus-pj"/>
                                    <input class="ui-state-default ui-corner-all" style="cursor: pointer;display: inline-block;padding: 3px;" type="submit" value="x"/>
                                </form>
                                <br/>
                                (<b>{if $pj.PENANGGUNGJAWAB==1}PJ {else}Anggota {/if}</b>)
                            </li>
                        {/foreach}
                    </ul>
                </td>
                <td class="center">
                    <a class="disable-ajax ui-state-default ui-corner-all" style="padding: 3px;margin: 5px 2px;display: inline-block;vertical-align: top;">
                        <span class="ui-icon ui-icon-plusthick" onclick="$('#tambah-pj-{$d.ID_UNIT_KERJA}').fadeIn();" style="cursor: pointer;"></span>
                    </a>
                </td>
            </tr>
            <tr style="display: none" class="even" id="tambah-pj-{$d.ID_UNIT_KERJA}" >
                <td colspan="4">
                    <form action="komplain-pj.php" method="post">
                        <fieldset style="width: 90%;margin: 10px; ">
                            <legend>Tambah Penanggung Jawab/Anggota Penjawab Komplain {$d.NM_UNIT_KERJA}</legend>
                            <div class="clear-fix"></div>
                            <div class="field_form">
                                <label>Nama : </label>
                                <select name="pengguna" data-placeholder="Pilih Pengguna" class="chosen" >
                                    {foreach $d.PENGGUNA as $dp}
                                        <option value="{$dp.ID_PENGGUNA}">{$dp.GELAR_DEPAN} {$dp.NM_PENGGUNA} {$dp.GELAR_BELAKANG}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="clear-fix"></div>
                            <div class="field_form">
                                <label>Status : </label>
                                <select name="status">
                                    <option value="1">Penanggung Jawab</option>
                                    <option value="0">Anggota Penjawab</option>
                                </select>
                            </div>
                            <div class="clear-fix"></div>
                            <div class="bottom_field_form">
                                <input type="hidden" name="mode" value="tambah-pj"/>
                                <input type="hidden" name="unit" value="{$d.ID_UNIT_KERJA}" />
                                <a style="cursor: pointer;padding: 4px" onclick="$('#tambah-pj-{$d.ID_UNIT_KERJA}').fadeOut();"  class="disable-ajax ui-state-default ui-corner-all" >Batal</a>
                                <input type="submit"  style="cursor: pointer;padding: 4px" class=" ui-state-default ui-corner-all" value="Tambah"/>
                            </div>

                        </fieldset>
                    </form>
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>
{literal}
    <script type="text/javascript">
        $(document).ready(function() {
            $('.chosen').chosen({width: "80%"});
            $('.datatable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "aaSorting": [],
            });
        });
    </script>
{/literal}