<div class="center_title_bar">Laporan Rekapitulasi Evaluasi Wisuda</div>
<form method="get" id="report_form" action="rekap-evawisuda.php">
    <table style="width:90%" class="ui-widget-content">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
            <td>Periode Wisuda</td>
            <td>
                <select name="periode">
                    {foreach $data_periode as $data}
                        <option value="{$data.ID_TARIF_WISUDA}" {if $data.ID_TARIF_WISUDA==$smarty.get.periode}selected="true"{/if}>{$data.NM_TARIF_WISUDA}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="center">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_rekap)}
    <table class="ui-widget-content">
        <tr class="ui-widget-header">
            <th colspan="{count($header_kelompok_aspek)*2+2}" style="text-align: center;text-transform: uppercase">Rekapitulasi Evaluasi Wisuda</th>
        </tr>
        <tr class="ui-widget-header">
            <th rowspan="2">No</th>
            <th rowspan="2">
                {if $smarty.get.fakultas==''}
                    Fakultas
                {else}
                    Program Studi
                {/if}
            </th>
            {foreach $header_kelompok_aspek as $h}
                <th colspan="2">{$h.NAMA_KELOMPOK}</th>
            {/foreach}
        </tr>
        <tr class="ui-widget-header">
            {foreach $header_kelompok_aspek as $h}
                <th>Rerata Aspek</th>
                <th>Persen Aspek</th>
            {/foreach}
        </tr>
        {foreach $data_rekap as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>
                    {if $smarty.get.fakultas==''}
                        {$data.NM_FAKULTAS|upper}
                    {else}
                        ({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}
                    {/if}
                </td>
                {foreach $data.DATA_EVAL as $r}
                    <td>{round($r.RATA,2)}</td>
                    <td>{round($r.RATA/4*100,2)} %</td>
                {foreachelse}
                    <td colspan="{count($header_kelompok_aspek)*2}" class="kosong">Data Kosong</td>
                {/foreach}
            </tr>
        {foreachelse}
            <tr>
                <td colspan="{count($header_kelompok_aspek)*2+2}" style="color: red;text-align: center">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="{count($header_kelompok_aspek)*2+2}" style="text-align: center;">
                <a href="excel-rekap-evawisuda.php?{$smarty.server.QUERY_STRING}" class="disable-ajax ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" >Download Excel</a>
            </td>
        </tr>
    </table>
{/if}