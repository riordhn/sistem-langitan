<div class="center_title_bar">Master Aspek Instrumen</div>
{if $smarty.get.mode=='kelompok'}
    <table class="ui-widget-content" style="width: 60%">
        <caption>NAMA INSTRUMEN : {$data_instrumen_one.NAMA_EVAL_INSTRUMEN}</caption>
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>KELOMPOK ASPEK</th>
            <th>Operasi</th>
        </tr>
        {foreach $data_kelompok_aspek as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>{$data.NAMA_KELOMPOK}</td>
                <td class="center"><a href="master-aspek.php?mode=aspek&id={$smarty.get.id}&id_kel={$data.ID_EVAL_KELOMPOK_ASPEK}" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;">Lihat Aspek</a></td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="3" class="kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="3" class="center">
                <a class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" href="master-aspek.php" >Kembali</a>
            </td>
        </tr>
    </table>
{else if $smarty.get.mode=='aspek'}
    <table class="ui-widget-content" style="width: 90%">
        <caption>
            NAMA INSTRUMEN : {$data_instrumen_one.NAMA_EVAL_INSTRUMEN}<br/>
            NAMA KELOMPOK ASPEK : {$data_kelompok_aspek_one.NAMA_KELOMPOK}
        </caption>
        <tr class="ui-widget-header">
            <th>NO URUT</th>
            <th>ASPEK INTRUMEN</th>
            <th>TIPE ASPEK</th>
            <th style="width: 250px">NILAI ASPEK</th>
            <th style="width: 250px">JANGAN TAMPILKAN / HAPUS</th>
            <th style="width: 110px">Operasi</th>
        </tr>
        {foreach $data_aspek as $data}
            <tr>
                <td>{$data.URUTAN}</td>
                <td>{$data.EVALUASI_ASPEK}</td>
                <td>
                    {if $data.TIPE_ASPEK==1}
                        Pilihan
                    {else if $data.TIPE_ASPEK==2}
                        Isian
                    {else}
                        <span style="color:red">Kosong</span>
                    {/if}
                </td>
                <td>
                    {if $data.ID_KELOMPOK_NILAI}
                        Kelompok : {$data.NM_KELOMPOK}<br/>
                        <p>
                            {foreach $data.EVA_NILAI as $en}
                                {$en@index+1}. Nilai {$en.NILAI_ASPEK} ({$en.KET_NILAI_ASPEK})<br/>
                            {/foreach}
                        </p>
                    {else}
                        <span style="color:red">Kosong</span>
                    {/if}
                </td>
                <td>
                    {if $data.STATUS_AKTIF==0}
                        Tampil
                    {else}
                        <span style="color:red">Tidak Tampil</span>
                    {/if}
                </td>
                <td class="center">
                    <a href="master-aspek-edit.php?id={$smarty.get.id}&id_kel={$smarty.get.id_kel}&id_aspek={$data.ID_EVAL_ASPEK}" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;margin: 2px 0px">Edit</a>
                    <a href="master-aspek-edit.php?mode=delete&id={$smarty.get.id}&id_kel={$smarty.get.id_kel}&id_aspek={$data.ID_EVAL_ASPEK}" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;margin: 2px 0px">Tidak Tampil</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="6" class="kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="6" class="center">
                <a class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" href="master-aspek-tambah.php?id={$smarty.get.id}&id_kel={$smarty.get.id_kel}">Tambah</a>
                <a class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" href="master-aspek.php?mode=kelompok&id={$smarty.get.id}&id_kel={$smarty.get.id_kel}" >Kembali</a>
            </td>
        </tr>
    </table>
{else}
    <table class="ui-widget-content" style="width: 90%">
        <tr class="ui-widget-header">
            <th>NO </th>
            <th>NAMA EVALUASI</th>
            <th>KETERANGAN EVALUASI</th>
            <th>Operasi</th>
        </tr>
        {foreach $data_instrumen as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>{$data.NAMA_EVAL_INSTRUMEN}</td>
                <td>{$data.KET_EVAL_INSTRUMEN}</td>
                <td class="center"><a href="master-aspek.php?mode=kelompok&id={$data.ID_EVAL_INSTRUMEN}" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;">Lihat Kelompok Aspek</a></td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="4" class="kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
    </table>    
{/if}