<div class="center_title_bar">Edit Aspek Instrumen</div>
{if $smarty.get.mode==''}
    <form id="form_edit" method="post" action="master-aspek.php?mode=aspek&id={$smarty.get.id}&id_kel={$smarty.get.id_kel}">
        <table class="ui-widget-content" style="width: 70%">
            <caption>
                NAMA INSTRUMEN : {$data_instrumen_one.NAMA_EVAL_INSTRUMEN}<br/>
                NAMA KELOMPOK ASPEK : {$data_kelompok_aspek_one.NAMA_KELOMPOK}
            </caption>
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="2">Edit Aspek Instrumen</th>
            </tr>
            <tr>
                <td>Urutan Aspek Instrumen</td>
                <td>
                    <input type="text" name="urutan"  value="{$data_aspek_one.URUTAN}"/>
                </td>
            </tr>
            <tr>
                <td>Nama Aspek Instrumen</td>
                <td>
                    <textarea name="aspek" cols="40" class="required">{$data_aspek_one.EVALUASI_ASPEK}</textarea>
                </td>
            </tr>
            <tr>
                <td>Tipe Aspek</td>
                <td>
                    <select name="tipe" onclick="if($(this).val()==1){ $('.pilih-nilai').show() } else{ $('.pilih-nilai').hide();$('input[type=radio]').attr('checked',false) }">
                        <option value="1" {if $data_aspek_one.TIPE_ASPEK==1}selected="true"{/if}>Pilihan</option>
                        <option value="2" {if $data_aspek_one.TIPE_ASPEK==2}selected="true"{/if}>Isian</option>
                    </select>
                </td>
            </tr>
            <tr {if $data_aspek_one.TIPE_ASPEK==2} style="display: none"{/if} class="pilih-nilai ui-widget-header">
                <td colspan="2" class="center">Pilihan Nilai Aspek</td>
            </tr>
            {foreach $data_nilai as $n}
                <tr class="pilih-nilai" {if $data_aspek_one.TIPE_ASPEK==2} style="display: none"{/if}>
                    <td colspan="2" >
                        <input type="radio" name="nilai" {if $data_aspek_one.ID_KELOMPOK_NILAI==$n.ID_KELOMPOK_NILAI}checked="true"{/if} value="{$n.ID_KELOMPOK_NILAI}" />
                        Kelompok : {$n.NM_KELOMPOK}<br/>
                        <p>
                            {foreach $n.EVA_NILAI as $en}
                                {$en@index+1}. Nilai {$en.NILAI_ASPEK} ({$en.KET_NILAI_ASPEK})<br/>
                            {/foreach}
                        </p>
                    </td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="2" class="center">
                    <input type="hidden" value="edit" name="mode"/>
                    <input type="hidden" value="{$smarty.get.id_aspek}" name="id_aspek"/>
                    <input type="submit" value="Update" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;"/>
                    <a class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" href="master-aspek.php?mode=aspek&id={$smarty.get.id}&id_kel={$smarty.get.id_kel}" >Kembali</a>
                </td>
            </tr>
        </table>
    </form>
{else}
    <form id="form_edit" method="post" action="master-aspek.php?mode=aspek&id={$smarty.get.id}&id_kel={$smarty.get.id_kel}">
        <table class="ui-widget-content" style="width: 70%">
            <caption>
                NAMA INSTRUMEN : {$data_instrumen_one.NAMA_EVAL_INSTRUMEN}<br/>
                NAMA KELOMPOK ASPEK : {$data_kelompok_aspek_one.NAMA_KELOMPOK}
            </caption>
            <tr class="ui-widget-header">
                <th class="header-coloumn" colspan="2">Hapus Aspek Instrumen</th>
            </tr>
            <tr>
                <td>Nama Aspek Instrumen</td>
                <td>
                    {$data_aspek_one.EVALUASI_ASPEK}
                </td>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <input type="hidden" value="delete" name="mode"/>
                    <p>Apakah anda yakin Hapus/Tidak Ditampil item ini?</p>
                    <input type="hidden" value="{$smarty.get.id_aspek}" name="id_aspek"/>
                    <input type="submit" value="Ya" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;"/>
                    <a class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" href="master-aspek.php?mode=aspek&id={$smarty.get.id}&id_kel={$smarty.get.id_kel}" >Tidak</a>
                </td>
            </tr>
        </table>
    </form>
{/if}
{literal}
    <script type="text/javascript">
        $('#form_edit').validate();
    </script>
{/literal}