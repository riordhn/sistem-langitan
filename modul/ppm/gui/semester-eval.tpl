<div class="center_title_bar">Master Semester Aktif Evaluasi</div>
<table  class="ui-widget-content" style="width: 60%">
    <tr class="ui-widget-header">
        <th colspan="6" class="header-coloumn" style="text-align: center;">Semester Pengisian Evaluasi</th>
    </tr>
    <tr class="ui-widget-header">
        <th>No</th>
        <th>Nama Semester</th>
        <th>Group Semester</th>
        <th>Tahun Ajaran</th>
        <th>Tahun Akademik</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_semester as $sem}
        <tr>
            <td>{$sem@index+1}</td>
            <td>{$sem.NM_SEMESTER}</td>
            <td>{$sem.GROUP_SEMESTER}</td>
            <td>{$sem.TAHUN_AJARAN}</td>
            <td>{$sem.THN_AKADEMIK_SEMESTER}</td>
            <td class="center">
                {if $sem.STATUS_AKTIF=='1'}
                    <span class="ui-button ui-corner-all ui-state-hover" style="padding:5px;font-size: 12px;font-weight: bold;color: green">Aktif Evaluasi</span>
                {else}
                    <form action="semester-eval.php" method="post">
                        <input type="submit"  value="Aktifkan" value="aktifkan" class="ui-button ui-corner-all ui-state-active" style="font-weight: bold;font-size: 12px;padding:5px;cursor:pointer;"/>
                        <input type="hidden" name="mode" value="aktifkan"  />
                        <input type="hidden" name="semester" value="{$sem.ID_SEMESTER}" />
                    </form>
                {/if}
            </td>
        </tr>
    {/foreach}
</table>


