<div class="center_title_bar">Master Nilai Aspek</div>
{if $smarty.get.mode==''}
    <table class="ui-widget-content" style="width: 90%">
        <tr class="ui-widget-header">
            <th>NO</th>
            <th>Nama Kelompok</th>
            <th>Nilai Aspek</th>
            <th>Operasi</th>
        </tr>
        {foreach $data_nilai_aspek as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>{$data.NM_KELOMPOK}</td>
                <td>
                    <ol style="margin-bottom: 5px">
                        {foreach $data.EVA_NILAI as $n}
                            <li style="margin-left: 35px">Nilai {$n.NILAI_ASPEK} ({$n.KET_NILAI_ASPEK})</li>
                        {/foreach}
                    </ol>
                </td>
                <td class="center">
                    <a href="master-nilai-aspek.php?mode=edit&id_kelompok={$data.ID_KELOMPOK_NILAI}" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;">Edit</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="4" class="kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="4" class="center">
                <a class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" href="master-nilai-aspek.php?mode=tambah">Tambah</a>
            </td>
        </tr>
    </table>
{else if $smarty.get.mode=='tambah'}
    <form action="master-nilai-aspek.php" method="post">
        <table class="ui-widget-content" style="width: 70%">
            <tr class="ui-widget-header">
                <th colspan="4">Master Nilai Aspek - Tambah</th>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right">Nama Kelompok : </td>
                <td colspan="2">
                    <input type="text" name="kelompok" size="20" maxlength="20" class="required"/>
                </td>
            </tr>
            {section name=data start=0 loop=5 step=1}
                <tr>
                    <td style="width: 20%">Nilai Aspek {$smarty.section.data.index+1} : </td>
                    <td style="width: 25%">
                        <input type="text" name="nilai{$smarty.section.data.index+1}" size="2" maxlength="1" class="number required"/>
                    </td>
                    <td style="width: 20%">Keterangan Nilai {$smarty.section.data.index+1} : </td>
                    <td style="width: 35%">
                        <textarea name="keterangan{$smarty.section.data.index+1}" cols="10" class="required"></textarea>
                    </td>
                </tr>
            {/section}
            <tr>
                <td colspan="4" class="center">
                    <input type="hidden" name="mode" value="tambah"/>
                    <input type="submit" value="Tambah" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;"/>
                    <a class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" href="master-nilai-aspek.php">Batal</a>
                </td>
            </tr>
        </table>
    </form>
{else if $smarty.get.mode=='edit'}
    <form action="master-nilai-aspek.php" method="post">
        <table class="ui-widget-content" style="width: 70%">
            <tr class="ui-widget-header">
                <th colspan="4">Master Nilai Aspek - Tambah</th>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right">Nama Kelompok : </td>
                <td colspan="2">
                    <input type="hidden" name="id_kelompok" value="{$nilai[0].ID_KELOMPOK_NILAI}" />
                    <input type="text" name="kelompok" size="20" maxlength="20" value="{$nilai[0].NM_KELOMPOK}" class="required"/>
                </td>
            </tr>
            {section name=data start=0 loop=5 step=1}
                <tr>
                    <td style="width: 20%">Nilai Aspek {$smarty.section.data.index+1} : </td>
                    <td style="width: 25%">
                        <input type="text" name="nilai{$smarty.section.data.index+1}" size="2" maxlength="1" value="{$nilai[$smarty.section.data.index].NILAI_ASPEK}" class="number required"/>
                    </td>
                    <td style="width: 20%">Keterangan Nilai {$smarty.section.data.index+1} : </td>
                    <td style="width: 35%">
                        <input type="hidden" name="id_nilai{$smarty.section.data.index+1}" value="{$nilai[$smarty.section.data.index].ID_EVAL_NILAI_ASPEK}" />
                        <textarea name="keterangan{$smarty.section.data.index+1}" cols="10" class="required">{$nilai[$smarty.section.data.index].KET_NILAI_ASPEK}</textarea>
                    </td>
                </tr>
            {/section}
            <tr>
                <td colspan="4" class="center">
                    <input type="hidden" name="mode" value="edit"/>
                    <input type="submit" value="Update" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;"/>
                    <a class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" href="master-nilai-aspek.php">Batal</a>
                </td>
            </tr>
        </table>
    </form>
{/if}
{literal}
    <script type="text/javascript">
        $('form').validate();
    </script>
{/literal}