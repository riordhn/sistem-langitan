<div class="center_title_bar">{$instrumen['NAMA_EVAL_INSTRUMEN']}</div>
{if isset($instrumen)}
<a href="master-instrumen.php" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;color:white">Kembali</a>
<table class="ui-widget" style="width:97%">
        {foreach $instrumen['DATA_KELOMPOK'] as $dk}
            <tr class="ui-widget-header">
                <th colspan="6" style="font-size: 1.1em">{$dk.NAMA_KELOMPOK}</th>
            </tr>
            {foreach $dk.DATA_ASPEK as $da}
                <tr class="ui-widget-content">
                    {$jumlah_aspek=$jumlah_aspek+1}
                    <td style="padding: 5px">
                        {$da.EVALUASI_ASPEK}
                        <br/>
                        <label class="error" for="nilai{$jumlah_aspek}" style="font-size:0.8em ;display: none">Harus Diisi</label>
                    </td>
                    {if $da.TIPE_ASPEK==1}
                        {foreach $da[0] as $dn}
                            <td align="center" style="font-size: 0.8em">
                                {$dn.KET_NILAI_ASPEK}<br/>
                                <input type="radio" class="required" name="nilai{$jumlah_aspek}" value="{$dn.NILAI_ASPEK}"/>       
                            </td>                                
                        {/foreach}
                    {else}
                        <td colspan="5">
                            <textarea class="required" style="resize: none;width: 90%" maxlength="120" name="nilai{$jumlah_aspek}"></textarea>
                        </td>
                    {/if}
                </tr>                
            {foreachelse}
            <tr>
                <td style="text-align:center"><label class="ui-error-text">Data kosong</label></td>
            </tr>
            {/foreach}
        {foreachelse}
        <tr>
            <td style="text-align:center"><label class="ui-error-text">Data kosong</label></td>
        </tr>
        {/foreach}
    </table>
{else}
    {$alert}
{/if}
{literal}
    <script>
        $('#eva').validate();
    </script>
{/literal}
