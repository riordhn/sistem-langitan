<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';
include 'class/evaluasi.class.php';

$list = new list_data($db);
$eva = new evaluasi($db);

$id_pt = $id_pt_user;

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $smarty->assign('header_kelompok_aspek', $db->QueryToArray("
            SELECT EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK,COUNT(EA.ID_EVAL_ASPEK)  JUMLAH_ASPEK
            FROM EVALUASI_KELOMPOK_ASPEK EKS
            JOIN EVALUASI_ASPEK EA ON EA.ID_EVAL_KELOMPOK_ASPEK=EKS.ID_EVAL_KELOMPOK_ASPEK AND EA.TIPE_ASPEK=1
            WHERE EKS.ID_EVAL_INSTRUMEN=7
            GROUP BY EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK
            ORDER BY NAMA_KELOMPOK
            ")
        );
        $smarty->assign('header_aspek', $db->QueryToArray("SELECT * FROM EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=7 AND TIPE_ASPEK=1 AND ID_EVAL_ASPEK IN (SELECT ID_EVAL_ASPEK FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=7 GROUP BY ID_EVAL_ASPEK) ORDER BY ID_EVAL_ASPEK"));
        
        $smarty->assign('data_evaluasi', $eva->load_laporan_evaluasi_bimbingan_skripsi(get('semester')));
    }
}
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt));
$smarty->display('eva-srkipsi.tpl');
?>
