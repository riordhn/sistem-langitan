<?php

include 'config.php';
include 'class/master.class.php';

$master = new master($db);
$id_pt=$user->ID_PERGURUAN_TINGGI;

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $master->tambah_kelompok_nilai_aspek(post('kelompok'),$id_pt);
        $id_kelompok = $db->QuerySingle("SELECT ID_KELOMPOK_NILAI FROM EVALUASI_KELOMPOK_NILAI WHERE ID_PERGURUAN_TINGGI='{$id_pt}'ORDER BY ID_KELOMPOK_NILAI DESC");
        for ($i = 1; $i <= 5; $i++) {
            $master->tambah_nilai_aspek($id_kelompok, $i, post('nilai' . $i), post('keterangan' . $i));
        }
    } else if (post('mode') == 'edit') {
        $master->update_kelompok_nilai_aspek(post('id_kelompok'), post('kelompok'));
        for ($i = 1; $i <= 5; $i++) {
            $master->update_nilai_aspek(post('id_nilai' . $i), post('nilai' . $i), post('keterangan' . $i));
        }
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit' or get('mode') == 'view') {
        $smarty->assign('nilai', $master->get_nilai_aspek(get('id_kelompok')));
    }
}
$smarty->assign('data_nilai_aspek', $master->load_nilai_aspek());
$smarty->display('master-nilai-aspek.tpl');
?>
