<?php

include 'config.php';
include 'class/master.class.php';

$master = new master($db);


if (isset($_GET)) {
    if (get('mode') == 'kelompok') {
        $smarty->assign('data_kelompok_aspek', $master->load_kelompok_aspek(get('id')));
        $smarty->assign('data_instrumen_one', $master->get_evaluasi_instrumen(get('id')));
    } else if (get('mode') == 'aspek') {
        if (isset($_POST)) {
            if (post('mode') == 'tambah') {
                $master->tambah_aspek(post('id_kel'), post('id_ins'), post('aspek'), post('tipe'), post('nilai'),post('urutan'));
            } else if (post('mode') == 'edit') {
                $master->update_aspek(post('id_aspek'), post('aspek'), post('tipe'), post('nilai'),post('urutan'));
            } else if (post('mode') == 'delete') {
                $id = post('id_aspek');
                $db->Query("UPDATE EVALUASI_ASPEK SET STATUS_AKTIF=1 WHERE ID_EVAL_ASPEK='{$id}'");
            }
        }
        $smarty->assign('data_aspek', $master->load_aspek(get('id_kel'), get('id')));
        $smarty->assign('data_instrumen_one', $master->get_evaluasi_instrumen(get('id')));
        $smarty->assign('data_kelompok_aspek_one', $master->get_kelompok_aspek(get('id_kel')));
    }
}

$smarty->assign('data_instrumen', $master->load_evaluasi_intrumen());
$smarty->display('master-aspek.tpl');
?>
