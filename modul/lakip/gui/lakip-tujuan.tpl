{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">LAKIP Tujuan Tambah</div>
    <form action="lakip-tujuan.php" method="post">
        <fieldset style="width: 80%">
            <legend>Tambah Tujuan LAKIP</legend>
            <div class="field_form">
                <label>Kode : </label>
                <input type="text" size="10" maxlength="10" name="kode" class="required" />
            </div>
            <div class="field_form">
                <label>Isi Tujuan : </label>
                <textarea name="tujuan" class="required" cols="40"></textarea>
            </div>
            <div class="field_form">
                <label>Keterangan : </label>
                <textarea name="keterangan" cols="40"></textarea>
            </div>
            <div class="bottom_field_form">
                <a href="lakip-tujuan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">LAKIP Edit</div>
    <form action="lakip-tujuan.php" method="post">
        <fieldset style="width: 80%">
            <legend>Tambah Tujuan LAKIP</legend>
            <div class="field_form">
                <label>Kode : </label>
                <input type="text" size="10" maxlength="10" name="kode" value="{$tujuan.KODE_TUJUAN_RENSTRA}" class="required" />
            </div>
            <div class="field_form">
                <label>Isi Tujuan :</label>
                <textarea name="tujuan" class="required" cols="40">{$tujuan.TUJUAN_RENSTRA}</textarea>
            </div>
            <div class="field_form">
                <label>Keterangan : </label>
                <textarea name="keterangan" cols="40">{$tujuan.KETERANGAN}</textarea>
            </div>
            <div class="bottom_field_form">
                <a href="lakip-tujuan.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$tujuan.ID_TUJUAN_RENSTRA}" />
                <input type="hidden" name="mode" value="edit"/>
                <input type="submit" class="button" value="Update"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">LAKIP Hapus</div>
    <form action="lakip-tujuan.php" method="post">
        <fieldset style="width: 80%">
            <legend>Tambah Tujuan LAKIP</legend>
            <div class="field_form">
                <label>Kode : </label>
                {$tujuan.KODE_TUJUAN_RENSTRA}
            </div>
            <div class="field_form">
                <label>Isi Tujuan : </label>
                {$tujuan.TUJUAN_RENSTRA}
            </div>
            <div class="bottom_field_form">
                Apakah anda yakin menghapus item ini ?<br/>
                <a href="lakip-tujuan.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$tujuan.ID_TUJUAN_RENSTRA}" />
                <input type="hidden" name="mode" value="delete"/>
                <input type="submit" class="button" value="Ya"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">LAKIP Tujuan</div>
    <table style="width: 98%">
        <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Tujuan</th>
            <th>Keterangan</th>
            <th style="width: 120px" class="center">Operasi</th>
        </tr>
        {foreach $data_tujuan as $tuj}
            <tr>
                <td>{$tuj@index+1}</td>
                <td>{$tuj.KODE_TUJUAN_RENSTRA}</td>
                <td>{$tuj.TUJUAN_RENSTRA}</td>
                <td>{$tuj.KETERANGAN}</td>
                <td>
                    <a class="button" href="lakip-tujuan.php?mode=edit&id={$tuj.ID_TUJUAN_RENSTRA}">Edit</a>
                    <a class="button" href="lakip-tujuan.php?mode=delete&id={$tuj.ID_TUJUAN_RENSTRA}">Delete</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="5" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="5" class="center">
                <a href="lakip-tujuan.php?mode=tambah" class="button">Tambah</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
        $('form').validate();
    </script>
{/literal}