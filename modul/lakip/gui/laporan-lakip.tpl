<div class="center_title_bar">Laporan Pencapaian</div>
<form action="laporan-lakip.php" method="get" id="filter">
    <fieldset style="width: 40%">
        <legend>Parameter</legend>
        <div class="field_form">
            <label for="fakultas">Fakultas : </label>
            <select name="fakultas" onchange="$('#filter').submit()">
                <option value="">Semua</option>
                {foreach $data_fakultas as $f}
                    <option value="{$f.ID_FAKULTAS}" {if $f.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$f.NM_FAKULTAS}</option>
                {/foreach}
            </select>
        </div>
    </fieldset>
</form>
<table style="width: 98%;font-size: 0.85em">
    <tr>
        <th rowspan="2">NO</th>
        <th rowspan="2">Tujuan Renstra</th>
        <th rowspan="2">Program Utama (Universitas)</th>
        <th rowspan="2">Fakultas</th>
        <th rowspan="2">Program Kerja</th>
        <th colspan="4">Indikator Kinerja</th>
        <th colspan="5">Anggaran</th>
        <th rowspan="2">Keterangan</th>
        <th rowspan="2">Status Proker</th>
    </tr>
    <tr>
        <th>Nama Indikator</th>
        <th>Target</th>
        <th>Capaian</th>
        <th>%</th>
        <th>Usulan Pagu</th>
        <th>Revisi Pagu</th>
        <th>Pagu</th>
        <th>Serapan</th>
        <th>%</th>
    </tr>
    {foreach $data_laporan as $l}
        <tr>
            <td>{$l@index+1}</td>
            <td>{$l.TUJUAN_RENSTRA}</td>
            <td>{$l.PROGRAM_UTAMA}</td>
            <td>{$l.NM_FAKULTAS}</td>
            <td>{$l.PROGRAM_KERJA}</td>
            <td>
                <ol>
                    {foreach $l.DATA_INDIKATOR as $in}
                        <li>{$in.NAMA_INDIKATOR_KINERJA}</li>
                    {/foreach}
                </ol>
            </td>
            <td>
                <ol>
                    {foreach $l.DATA_INDIKATOR as $in}
                        <li>{number_format($in.TARGET)}</li>
                    {/foreach}
                </ol>
            </td>
            <td>
                <ol>
                    {foreach $l.DATA_INDIKATOR as $in}
                        <li>{number_format($in.CAPAIAN)}</li>
                    {/foreach}
                </ol>
            </td>
            <td>
                <ol>
                    {foreach $l.DATA_INDIKATOR as $in}
                        <li>{number_format(($in.CAPAIAN/$in.TARGET)*100|string_format:"%d")}%</li>
                    {/foreach}
                </ol>
            </td>
            <td>{number_format($l.USULAN_PAGU)}</td>
            <td>{number_format($l.REVISI_PAGU)}</td>
            <td>{number_format($l.PAGU)}</td>
            <td>{number_format($l.SERAPAN)}</td>
            <td>{number_format(($l.SERAPAN/$l.PAGU)*100)|string_format:"%d"}%</td>
            <td>
                <ol>
                    {foreach $l.DATA_INDIKATOR as $in}
                        <li>{if $in.KETERANGAN_ANALISA!=''}{$in.KETERANGAN_ANALISA}{else}<span style="color: red">Kosong</span>{/if}</li>
                    {/foreach}
                </ol>
            </td>
            <td>
                {if $l.STATUS_PROKER==1}
                    <span style="color: red;font-size: 12px">Tidak Disetujui</span>
                {else}
                    <span style="color: green;font-size: 12px">Disetujui</span>
                {/if}
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="16" class="data-kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}
</table>