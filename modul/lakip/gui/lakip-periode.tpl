{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">LAKIP Periode Tambah</div>
    <form action="lakip-periode.php" method="post">
        <fieldset style="width: 80%">
            <legend>Tambah Periode LAKIP</legend>
            <div class="field_form">
                <label>Nama Periode : </label>
                <input type="text" name="nama" class="required" />
            </div>
            <div class="field_form">
                <label>Awal Periode : </label>
                <input type="text" name="awal" size="10"  class="required datepicker" />
            </div>
            <div class="field_form">
                <label>Akhir Periode : </label>
                <input type="text" name="akhir" size="10"  class="required datepicker" />
            </div>
            <div class="bottom_field_form">
                <a href="lakip-periode.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">LAKIP Periode Edit</div>
    <form action="lakip-periode.php" method="post">
        <fieldset style="width: 80%">
            <legend>Edit Periode LAKIP</legend>
            <div class="field_form">
                <label>Nama Periode : </label>
                <input type="text" name="nama" class="required" value="{$periode.NAMA_PERIODE}" />
            </div>
            <div class="field_form">
                <label>Awal Periode : </label>
                <input type="text" name="awal" size="10" class="required datepicker" value="{$periode.AWAL_PERIODE}" />
            </div>
            <div class="field_form">
                <label>Akhir Periode : </label>
                <input type="text" name="akhir" size="10"  class="required datepicker" value="{$periode.AKHIR_PERIODE}" />
            </div>
            <div class="bottom_field_form">
                <a href="lakip-periode.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$periode.ID_PERIODE}"/>
                <input type="hidden" name="mode" value="edit"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">LAKIP Hapus</div>
    <form action="lakip-periode.php" method="post">
        <fieldset style="width: 80%">
            <legend>Delete Periode LAKIP</legend>
            <div class="field_form">
                <label>Nama Periode : </label>
                {$periode.NAMA_PERIODE}
            </div>
            <div class="field_form">
                <label>Awal Periode : </label>
                {$periode.AWAL_PERIODE}
            </div>
            <div class="field_form">
                <label>Akhir Periode : </label>
                {$periode.AKHIR_PERIODE}
            </div>
            <div class="bottom_field_form">
                Apakah anda yakin menghapus item ini ?<br/>
                <a href="lakip-periode.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$periode.ID_PERIODE}"/>
                <input type="hidden" name="id" value="{$periode.ID_PERIODE}"/>
                <input type="hidden" name="mode" value="delete"/>
                <input type="submit" class="button" value="Ya"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">LAKIP Periode</div>
    <table style="width: 98%">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Awal Periode</th>
            <th>Akhir Periode</th>
            <th style="width: 120px" class="center">Operasi</th>
        </tr>
        {foreach $data_periode as $pr}
            <tr>
                <td>{$pr@index+1}</td>
                <td>{$pr.NAMA_PERIODE}</td>
                <td>{$pr.AWAL_PERIODE}</td>
                <td>{$pr.AKHIR_PERIODE}</td>
                <td>
                    <a class="button" href="lakip-periode.php?mode=edit&id={$pr.ID_PERIODE}">Edit</a>
                    <a class="button" href="lakip-periode.php?mode=delete&id={$pr.ID_PERIODE}">Delete</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="5" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="5" class="center">
                <a href="lakip-periode.php?mode=tambah" class="button">Tambah</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
        $('form').validate();
        $( ".datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat : 'dd-M-y'
        }).css({'text-transform':'uppercase'});
    </script>
{/literal}