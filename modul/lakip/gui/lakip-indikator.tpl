{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">LAKIP Indikator Kinerja Tambah</div>
    <form action="lakip-indikator.php" method="post">
        <fieldset style="width: 90%">
            <legend>Tambah Indikator Kinerja</legend>
            <div class="field_form">
                <label>Kode Indikator : </label>
                <input type="text" size="10" maxlength="10" name="kode" class="required"/>
            </div>
            <div class="field_form">
                <label>Fakultas : </label>
                <select id="fakultas" name="fakultas">
                    <option value="">Pilih Fakultas</option>
                    {foreach $data_fakultas as $f}
                        <option value="{$f.ID_FAKULTAS}">{$f.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Program Kerja: </label>
                <select id="program_kerja" name="program_kerja" class="required">
                    <option value="">Pilih Program Kerja</option>
                </select>
            </div>
            <div class="field_form">
                <label>Nama Indikator Kinerja : </label>
                <textarea name="nama" cols="40" class="required"></textarea>
            </div>
            <div class="field_form">
                <label>Target : </label>
                <input type="text" name="target" class="required"/>
            </div>
            <div class="bottom_field_form">
                <a href="lakip-indikator.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">LAKIP Indikator Kinerja Edit</div>
    <form action="lakip-indikator.php" method="post">
        <fieldset style="width: 90%">
            <legend>Edit Indikator Kinerja</legend>
            <div class="field_form">
                <label>Kode Indikator : </label>
                <input type="text" size="10" maxlength="10" name="kode" value="{$indikator.KODE_INDIKATOR_KINERJA}" class="required"/>
            </div>
            <div class="field_form">
                <label>Fakultas : </label>
                <select id="fakultas" name="fakultas">
                    <option value="">Pilih Fakultas</option>
                    {foreach $data_fakultas as $f}
                        <option value="{$f.ID_FAKULTAS}">{$f.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Program Kerja: </label>
                <select id="program_kerja" name="program_kerja" class="required">
                    <option value="">Pilih Program Kerja</option>
                </select>
            </div>
            <div class="field_form">
                <label>Nama Indikator Kinerja : </label>
                <textarea name="nama" cols="40" class="required">{$indikator.NAMA_INDIKATOR_KINERJA}</textarea>
            </div>
            <div class="field_form">
                <label>Target : </label>
                <input type="text" name="target" value="{$indikator.TARGET}" class="required"/>
            </div>
            <div class="bottom_field_form">
                <a href="lakip-indikator.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$indikator.ID_INDIKATOR_KINERJA}"/>
                <input type="hidden" name="mode" value="edit"/>
                <input type="submit" class="button" value="Update"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">LAKIP Indikator Kinerja Hapus</div>
    <form action="lakip-indikator.php" method="post">
        <fieldset style="width: 90%">
            <legend>Hapus Indikator Kinerja</legend>
            <div class="field_form">
                <label>Program Kerja: </label>
                {$indikator.PROGRAM_KERJA}
            </div>
            <div class="field_form">
                <label>Nama Indikator Kinerja : </label>
                {$indikator.NAMA_INDIKATOR_KINERJA}
            </div>
            <div class="field_form">
                <label>Target : </label>
                {$indikator.TARGET}
            </div>
            <div class="bottom_field_form">
                Apakah anda yakin menghapus item ini ? <br/>
                <a href="lakip-indikator.php" class="button">Batal</a>
                <input type="hidden" name="id" value="{$indikator.ID_INDIKATOR_KINERJA}"/>
                <input type="hidden" name="mode" value="delete"/>
                <input type="submit" class="button" value="Ya"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">LAKIP Indikator Kinerja</div>
    <table style="width: 98%">
        <tr>
            <th>No</th>
            <th>Program Kerja</th>
            <th>Kode</th>
            <th>Nama Indikator</th>
            <th>Target</th>
            <th>Status</th>
            <th style="width: 120px" class="center">Operasi</th>
        </tr>
        {foreach $data_indikator as $pr}
            <tr>
                <td>{$pr@index+1}</td>
                <td>{$pr.PROGRAM_KERJA} ({$pr.NAMA_PERIODE})</td>
                <td>{$pr.KODE_INDIKATOR_KINERJA}</td>
                <td>{$pr.NAMA_INDIKATOR_KINERJA}</td>
                <td>{$pr.TARGET}</td>
                <td>
                    {if $pr.STATUS==1}
                        <span style="color: red;font-size: 12px">Tidak Disetujui</span>
                    {else}
                        <span style="color: green;font-size: 12px">Disetujui</span>
                    {/if}
                </td>
                <td>
                    <a class="button" href="lakip-indikator.php?mode=edit&id={$pr.ID_INDIKATOR_KINERJA}">Edit</a>
                    <a class="button" href="lakip-indikator.php?mode=delete&id={$pr.ID_INDIKATOR_KINERJA}">Delete</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="7" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="7" class="center">
                <a href="lakip-indikator.php?mode=tambah" class="button">Tambah</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
        $('form').validate();
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'GetProgramKerja.php',
                data:'id_fakultas='+$(this).val(),
                success:function(data){
                    $('#program_kerja').html(data);
                }
            })
        })
    </script>
{/literal}