{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">Usulan Anggaran Lakip</div>
    <form action="lakip-usulan.php" method="post">
        <fieldset style="width: 80%">
            <legend>Tambah Usulan Anggaran LAKIP</legend>
            <div class="field_form">
                <label>Fakultas : </label>
                <select id="fakultas" name="fakultas">
                    <option value="">Pilih Fakultas</option>
                    {foreach $data_fakultas as $f}
                        <option value="{$f.ID_FAKULTAS}">{$f.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Program Kerja: </label>
                <select id="program_kerja" name="program_kerja" class="required">
                    <option value="">Pilih Program Kerja</option>
                </select>
            </div>
            <div class="field_form">
                <label style="font-size: 1.2em;color: blue;">-- Anggaran --</label>
            </div>
            <div class="field_form">
                <label>Usulan Pagu : </label>
                <input type="text" name="usulan_pagu" class="required"/>
            </div>
            <div class="field_form">
                <label>Keterangan : </label>
                <textarea name="keterangan" cols="40"></textarea>
            </div>
            <div class="bottom_field_form">
                <a href="lakip-usulan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">Edit Usulan Anggaran LAKIP</div>
    <form action="lakip-usulan.php" method="post">
        <fieldset style="width: 80%">
            <legend>Edit Usulan Anggaran LAKIP</legend>
            <div class="field_form">
                <label>Fakultas : </label>
                <select id="fakultas" name="fakultas">
                    <option value="">Pilih Fakultas</option>
                    {foreach $data_fakultas as $f}
                        <option value="{$f.ID_FAKULTAS}">{$f.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Program Kerja: </label>
                <select id="program_kerja" name="program_kerja" class="required">
                    <option value="">Pilih Program Kerja</option>
                </select>
            </div>
            <div class="field_form">
                <label style="font-size: 1.2em;color: blue;">-- Anggaran --</label>
            </div>
            <div class="field_form">
                <label>Usulan Pagu : </label>
                <input type="text" name="usulan_pagu" value="{$usulan.USULAN_PAGU}" class="required"/>
            </div>
            <div class="field_form">
                <label>Keterangan : </label>
                <textarea name="keterangan" cols="40">{$usulan.KETERANGAN}</textarea>
            </div>
            <div class="bottom_field_form">
                <a href="lakip-usulan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" value="{$usulan.ID_ANGGARAN}"/>
                <input type="submit" class="button" value="Update"/>
            </div>
    </form>
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">Delete Usulan Anggaran LAKIP</div>
    <form action="lakip-usulan.php" method="post">
        <fieldset style="width: 80%">
            <legend>Delete Usulan Anggaran LAKIP</legend>
            <div class="field_form">
                <label>Fakultas : </label>
                {$usulan.NM_FAKULTAS}
            </div>
            <div class="field_form">
                <label>Program Kerja: </label>
                {$usulan.PROGRAM_KERJA}
            </div>
            <div class="field_form">
                <label style="font-size: 1.2em;color: blue;">-- Anggaran --</label>
            </div>
            <div class="field_form">
                <label>Usulan Pagu : </label>
                {$usulan.USULAN_PAGU}
            </div>
            <div class="field_form">
                <label>Keterangan : </label>
                {$usulan.KETERANGAN}
            </div>
            <div class="bottom_field_form">
                Apakah anda yakin menghapus item ini?<br/>
                <a href="lakip-usulan.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="delete"/>
                <input type="hidden" name="id" value="{$usulan.ID_ANGGARAN}"/>
                <input type="submit" class="button" value="Ya"/>
            </div>
    </form>
{else}
    <div class="center_title_bar">Usulan Anggaran</div>
    <table style="width: 98%;font-size: 0.9em">
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">Tujuan Renstra</th>
            <th rowspan="2">Program Utama (Universitas)</th>
            <th rowspan="2">Fakultas</th>
            <th rowspan="2">Program Kerja</th>
            <th colspan="5">Anggaran</th>
            <th rowspan="2">Keterangan</th>
            <th rowspan="2">Status</th>
            <th rowspan="2" style="width: 120px">Operasi</th>
        </tr>
        <tr>
            <th>Usulan Pagu</th>
            <th>Revisi Pagu</th>
            <th>Pagu</th>
            <th>Serapan</th>
            <th>%</th>
        </tr>
        {foreach $data_usulan as $u}
            <tr>
                <td>{$u@index+1}</td>
                <td>{$u.TUJUAN_RENSTRA}</td>
                <td>{$u.PROGRAM_UTAMA}</td>
                <td>{$u.NM_FAKULTAS}</td>
                <td>{$u.PROGRAM_KERJA}</td>
                <td>{number_format($u.USULAN_PAGU)}</td>
                <td>{number_format($u.REVISI_PAGU)}</td>
                <td>{number_format($u.PAGU)}</td>
                <td>{number_format($u.SERAPAN)}</td>
                <td>{(($u.SERAPAN/$u.PAGU)*100)|string_format:"%d"}%</td>
                <td>{$u.KETERANGAN}</td>
                <td>
                    {if $u.STATUS==1}
                        <span style="color: red;font-size: 12px">Tidak Disetujui</span>
                    {else}
                        <span style="color: green;font-size: 12px">Disetujui</span>
                    {/if}
                </td>
                <td>
                    <a href="lakip-usulan.php?mode=edit&id={$u.ID_ANGGARAN}" class="button">Edit</a>
                    <a href="lakip-usulan.php?mode=delete&id={$u.ID_ANGGARAN}" class="button">Delete</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="13" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="13" class="center">
                <a href="lakip-usulan.php?mode=tambah" class="button">Tambah Baru</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
        $('form').validate();
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'GetProgramKerja.php',
                data:'id_fakultas='+$(this).val(),
                success:function(data){
                    $('#program_kerja').html(data);
                }
            })
        })
    </script>
{/literal}