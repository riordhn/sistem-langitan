<div class="center_title_bar">Capaian Indikator</div>
<table style="width: 98%;font-size: 0.9em">
    <tr>
        <td colspan="14" class="center">
            <b>KETERANGAN</b><br/><br/>
            Klik tulisan <span style="color: green">Approve</span> untuk Unapprove<br/>
            Klik tulisan <span style="color: red">Unapprove</span> untuk Approve <br/>
            Jika terjadi masalah klik tombol <br/>
            <a class="disable-ajax button" style="cursor: pointer" onclick="window.location.reload()">Reload</a>
        </td>
    </tr>
    <tr>
        <th rowspan="2">NO</th>
        <th rowspan="2">Fakultas</th>
        <th rowspan="2">Program Kerja</th>
        <th colspan="4">Indikator Kinerja</th>
        <th colspan="6">Anggaran</th>
        <th rowspan="2">Keterangan</th>
    </tr>
    <tr>
        <th>Nama Indikator</th>
        <th>Target</th>
        <th>Capaian</th>
        <th>%</th>
        <th>Usulan Pagu</th>
        <th>Revisi Pagu</th>
        <th>Pagu</th>
        <th>Serapan</th>
        <th>%</th>
        <th style="width: 80px">Status Anggaran</th>
    </tr>
    {foreach $data_laporan as $l}
        <tr>
            <td>{$l@index+1}</td>
            <td>{$l.NM_FAKULTAS}</td>
            <td>
                {$l.PROGRAM_KERJA}<br/>(
                {if $l.STATUS_PROKER==1}
                    <span id="{$l@index+1}" class="status_proker" style="color: red;cursor: pointer">Unapproved</span>
                {else}
                    <span id="{$l@index+1}" class="status_proker" style="color: green;cursor: pointer">Approved</span>
                {/if}
                )
                <input type="hidden" id="proker{$l@index+1}" value="{$l.ID_PROGRAM_KERJA}"/>
            </td>
            <td>
                <ol>
                    {foreach $l.DATA_INDIKATOR as $in}
                        <li>
                            {$in.NAMA_INDIKATOR_KINERJA}
                            <br/>(
                            {if $in.STATUS==1}
                                <span id="{$l@index+1}{$in@index+1}" class="status_indikator" style="color: red;cursor: pointer">Unapproved</span>
                            {else}
                                <span id="{$l@index+1}{$in@index+1}" class="status_indikator" style="color: green;cursor: pointer">Approved</span>
                            {/if}
                            )
                            <input type="hidden" id="indikator{$l@index+1}{$in@index+1}" value="{$in.ID_INDIKATOR_KINERJA}"/>
                        </li>                        
                    {/foreach}
                </ol>
            </td>
            <td>
                <ol>
                    {foreach $l.DATA_INDIKATOR as $in}
                        <li>{number_format($in.TARGET)}</li>
                    {/foreach}
                </ol>
            </td>
            <td>
                <ol>
                    {foreach $l.DATA_INDIKATOR as $in}
                        <li>{number_format($in.CAPAIAN)}</li>
                    {/foreach}
                </ol>
            </td>
            <td>
                <ol>
                    {foreach $l.DATA_INDIKATOR as $in}
                        <li>{number_format(($in.CAPAIAN/$in.TARGET)*100|string_format:"%d")}%</li>
                    {/foreach}
                </ol>
            </td>
            <td>{number_format($l.USULAN_PAGU)}</td>
            <td>{number_format($l.REVISI_PAGU)}</td>
            <td>{number_format($l.PAGU)}</td>
            <td>{number_format($l.SERAPAN)}</td>
            <td>{number_format(($l.SERAPAN/$l.PAGU)*100)|string_format:"%d"}%</td>
            <td>
                (
                {if $l.STATUS_ANGGARAN==1}
                    <span id="{$l@index+1}" class="status_anggaran" style="color: red;cursor: pointer">Unapproved</span>
                {else}
                    <span id="{$l@index+1}" class="status_anggaran" style="color: green;cursor: pointer">Approved</span>
                {/if}
                )
                <input type="hidden" id="anggaran{$l@index+1}" value="{$l.ID_ANGGARAN}"/>
            </td>
            <td>
                <ol>
                    {foreach $l.DATA_INDIKATOR as $in}
                        <li>{if $in.KETERANGAN_ANALISA!=''}{$in.KETERANGAN_ANALISA}{else}<span style="color: red">Kosong</span>{/if}</li>
                    {/foreach}
                </ol>
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="14" class="data-kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}
</table>
{literal}
    <script>
        $('.status_proker').click(function(){
            id=$(this).attr('id');
            $.ajax({
                type:'post',
                url:'lakip-approve.php',
                data:'mode=proker&id='+$('#proker'+id).val(),
                success:function(data){
                    $('#content').html(data);
                }
            })
        });
        $('.status_anggaran').click(function(){
            id=$(this).attr('id');
            $.ajax({
                type:'post',
                url:'lakip-approve.php',
                data:'mode=anggaran&id='+$('#anggaran'+id).val(),
                success:function(data){
                    $('#content').html(data);
                }
            })
        });
        $('.status_indikator').click(function(){
            id=$(this).attr('id');
            $.ajax({
                type:'post',
                url:'lakip-approve.php',
                data:'mode=indikator&id='+$('#indikator'+id).val(),
                success:function(data){
                    $('#content').html(data);
                }
            })
        });
    </script>
{/literal}