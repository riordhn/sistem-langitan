{if $smarty.get.mode=='revisi'}
    <div class="center_title_bar">Revisi Usulan Anggaran LAKIP</div>
    <form action="lakip-revisi.php" method="post">
        <fieldset style="width: 80%">
            <legend>Revisi Usulan Anggaran LAKIP</legend>
            <div class="field_form">
                <label>Fakultas : </label>
                {$usulan.NM_FAKULTAS}
            </div>
            <div class="field_form">
                <label>Program Kerja: </label>
                {$usulan.PROGRAM_KERJA}
            </div>
            <div class="field_form">
                <label style="font-size: 1.2em;color: blue;">-- Anggaran --</label>
            </div>
            <div class="field_form">
                <label>Revisi Pagu : </label>
                <input type="text" name="revisi" value="{$usulan.REVISI_PAGU}" class="required"/>
            </div>
            <div class="field_form">
                <label>Pagu : </label>
                <input type="text" name="pagu" value="{$usulan.PAGU}" class="required"/>
            </div>
            <div class="field_form">
                <label>Serapan : </label>
                <input type="text" name="serapan" value="{$usulan.SERAPAN}" class="required"/>
            </div>
            <div class="field_form">
                <label>Keterangan : </label>
                <textarea name="keterangan" cols="40">{$usulan.KETERANGAN}</textarea>
            </div>
            <div class="bottom_field_form">
                <a href="lakip-revisi.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="revisi"/>
                <input type="hidden" name="id" value="{$usulan.ID_ANGGARAN}"/>
                <input type="submit" class="button" value="Revisi"/>
            </div>
    </form>
{else}
    <div class="center_title_bar">Revisi Usulan Anggaran</div>
    <table style="width: 98%;font-size: 0.9em">
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">Tujuan Renstra</th>
            <th rowspan="2">Program Utama (Universitas)</th>
            <th rowspan="2">Fakultas</th>
            <th rowspan="2">Program Kerja</th>
            <th colspan="5">Anggaran</th>
            <th rowspan="2">Keterangan</th>
            <th rowspan="2">Status</th>
            <th rowspan="2">Operasi</th>
        </tr>
        <tr>
            <th>Usulan Pagu</th>
            <th>Revisi Pagu</th>
            <th>Pagu</th>
            <th>Serapan</th>
            <th>%</th>
        </tr>
        {foreach $data_usulan as $u}
            <tr>
                <td>{$u@index+1}</td>
                <td>{$u.TUJUAN_RENSTRA}</td>
                <td>{$u.PROGRAM_UTAMA}</td>
                <td>{$u.NM_FAKULTAS}</td>
                <td>{$u.PROGRAM_KERJA}</td>
                <td>{number_format($u.USULAN_PAGU)}</td>
                <td>{number_format($u.REVISI_PAGU)}</td>
                <td>{number_format($u.PAGU)}</td>
                <td>{number_format($u.SERAPAN)}</td>
                <td>{(($u.SERAPAN/$u.PAGU)*100)|string_format:"%d"}%</td>
                <td>{$u.KETERANGAN}</td>
                <td>
                    {if $u.STATUS==1}
                        <span style="color: red;font-size: 12px">Tidak Disetujui</span>
                    {else}
                        <span style="color: green;font-size: 12px">Disetujui</span>
                    {/if}
                </td>
                <td>
                    <a href="lakip-revisi.php?mode=revisi&id={$u.ID_ANGGARAN}" class="button">Revisi</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="13" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
    </table>
{/if}
{literal}
    <script>
        $('form').validate();
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'GetProgramKerja.php',
                data:'id_fakultas='+$(this).val(),
                success:function(data){
                    $('#program_kerja').html(data);
                }
            })
        })
    </script>
{/literal}