{if $smarty.get.mode=='capaian'}
    <div class="center_title_bar">Capaian Indikator</div>
    <form action="lakip-capaian.php" method="post">
        <table style="width: 80%">
            <tr>
                <th colspan="7" class="center">Capaian Indikator LAKIP</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>Fakultas</th>
                <th>Program Kerja</th>
                <th>Indikator</th>
                <th>Target</th>
                <th>Capaian</th>
                <th>Keterangan Analisa</th>
            </tr>
            {foreach $indikator as $in}
                <tr>
                    <td>{$in@index+1}</td>
                    <td>{$in.NM_FAKULTAS}</td>
                    <td>{$in.PROGRAM_KERJA}</td>
                    <td>{$in.NAMA_INDIKATOR_KINERJA}</td>
                    <td>{$in.TARGET}</td>
                    <td>
                        <input type="text" name="capaian{$in@index+1}" size="5" value="{$in.CAPAIAN}" class="required"/>
                        <input type="hidden" name="id_indikator{$in@index+1}" value="{$in.ID_INDIKATOR_KINERJA}"/>
                    </td>
                    <td>
                        <textarea name="keterangan{$in@index+1}" cols="20">{$in.KETERANGAN_ANALISA}</textarea>
                    </td>
                </tr>
            {/foreach}
            <tr>
                <td colspan="7" class="center">
                    <a href="lakip-capaian.php" class="button">Batal</a>
                    <input type="hidden" name="mode" value="capaian"/>
                    <input type="hidden" name="jumlah" value="{count($indikator)}"/>
                    <input type="submit" class="button" value="Update Capaian"/>
                </td>
            </tr>
        </table>
    </form>
{else}
    <div class="center_title_bar">Capaian Indikator</div>
    <table style="width: 98%;font-size: 0.9em">
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">Fakultas</th>
            <th rowspan="2">Program Kerja</th>
            <th colspan="4">Indikator Kinerja</th>
            <th colspan="5">Anggaran</th>
            <th rowspan="2">Keterangan</th>
            <th rowspan="2">Status</th>
            <th rowspan="2">Operasi</th>
        </tr>
        <tr>
            <th>Nama Indikator</th>
            <th>Target</th>
            <th>Capaian</th>
            <th>%</th>
            <th>Usulan Pagu</th>
            <th>Revisi Pagu</th>
            <th>Pagu</th>
            <th>Serapan</th>
            <th>%</th>
        </tr>
        {foreach $data_laporan as $l}
            <tr>
                <td>{$l@index+1}</td>
                <td>{$l.NM_FAKULTAS}</td>
                <td>{$l.PROGRAM_KERJA}</td>
                <td>
                    <ol>
                        {foreach $l.DATA_INDIKATOR as $in}
                            <li>{$in.NAMA_INDIKATOR_KINERJA}</li>
                        {/foreach}
                    </ol>
                </td>
                <td>
                    <ol>
                        {foreach $l.DATA_INDIKATOR as $in}
                            <li>{number_format($in.TARGET)}</li>
                        {/foreach}
                    </ol>
                </td>
                <td>
                    <ol>
                        {foreach $l.DATA_INDIKATOR as $in}
                            <li>{number_format($in.CAPAIAN)}</li>
                        {/foreach}
                    </ol>
                </td>
                <td>
                    <ol>
                        {foreach $l.DATA_INDIKATOR as $in}
                            <li>{number_format(($in.CAPAIAN/$in.TARGET)*100|string_format:"%d")}%</li>
                        {/foreach}
                    </ol>
                </td>
                <td>{number_format($l.USULAN_PAGU)}</td>
                <td>{number_format($l.REVISI_PAGU)}</td>
                <td>{number_format($l.PAGU)}</td>
                <td>{number_format($l.SERAPAN)}</td>
                <td>{number_format(($l.SERAPAN/$l.PAGU)*100)|string_format:"%d"}%</td>
                <td>
                    <ol>
                        {foreach $l.DATA_INDIKATOR as $in}
                            <li>{if $in.KETERANGAN_ANALISA!=''}{$in.KETERANGAN_ANALISA}{else}<span style="color: red">Kosong</span>{/if}</li>
                        {/foreach}
                    </ol>
                </td>
                <td>
                    {if $l.STATUS==1}
                        <span style="color: red;font-size: 12px">Tidak Disetujui</span>
                    {else}
                        <span style="color: green;font-size: 12px">Disetujui</span>
                    {/if}
                </td>
                <td>
                    <a href="lakip-capaian.php?mode=capaian&id={$l.ID_PROGRAM_KERJA}" class="button">Capaian</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="14" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
    </table>
{/if}