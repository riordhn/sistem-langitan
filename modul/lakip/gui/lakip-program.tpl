{if $smarty.get.mode=='tambah'}
    <div class="center_title_bar">LAKIP Program Tambah</div>
    <form action="lakip-program.php" method="post">
        <fieldset style="width: 80%">
            <legend>Tambah Program LAKIP</legend>
            <div class="field_form">
                <label>Kode Program : </label>
                <input type="text" size="10" maxlength="10" name="kode" class="required"/>
            </div>
            <div class="field_form">
                <label>Tujuan : </label>
                <select name="tujuan" class="required">
                    <option value="">Pilih Tujuan</option>
                    {foreach $data_tujuan as $tuj}
                        <option value="{$tuj.ID_TUJUAN_RENSTRA}">{$tuj.TUJUAN_RENSTRA|substr:0:80} ......</option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Isi Program : </label>
                <textarea name="program" class="required" cols="40"></textarea>
            </div>
            <div class="field_form">
                <label>Keterangan : </label>
                <textarea name="keterangan" cols="40"></textarea>
            </div>
            <div class="bottom_field_form">
                <a href="lakip-program.php" class="button">Batal</a>
                <input type="hidden" name="mode" value="tambah"/>
                <input type="submit" class="button" value="Simpan"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='edit'}
    <div class="center_title_bar">LAKIP Program Edit</div>
    <form action="lakip-program.php" method="post">
        <fieldset style="width: 80%">
            <legend>Tambah Program LAKIP</legend>
            <div class="field_form">
                <label>Kode Program : </label>
                <input type="text" size="10" maxlength="10" name="kode" value="{$program.KODE_PROGRAM_UTAMA}" class="required"/>
            </div>
            <div class="field_form">
                <label>Tujuan : </label>
                <select name="tujuan">
                    <option>Pilih Tujuan</option>
                    {foreach $data_tujuan as $tuj}
                        <option value="{$tuj.ID_TUJUAN_RENSTRA}" {if $program.ID_TUJUAN_RENSTRA==$tuj.ID_TUJUAN_RENSTRA}selected="true"{/if}>{$tuj.TUJUAN_RENSTRA|substr:0:70} ......</option>
                    {/foreach}
                </select>
            </div>
            <div class="field_form">
                <label>Isi Program :</label>
                <textarea name="program" cols="40">{$program.PROGRAM_UTAMA}</textarea>
            </div>
            <div class="field_form">
                <label>Keterangan : </label>
                <textarea name="keterangan" cols="40">{$program.KETERANGAN}</textarea>
            </div>
            <div class="bottom_field_form">
                <input type="hidden" name="mode" value="edit"/>
                <input type="hidden" name="id" value="{$program.ID_PROGRAM_UTAMA}" />
                <a href="lakip-program.php" class="button">Batal</a>
                <input type="submit" class="button" value="Update"/>
            </div>
        </fieldset>
    </form>
{else if $smarty.get.mode=='delete'}
    <div class="center_title_bar">LAKIP Program Hapus</div>
    <form action="lakip-program.php" method="post">
        <fieldset style="width: 80%">
            <legend>Tambah Program LAKIP</legend>
            <div class="field_form">
                <label>Kode Program : </label>
                {$program.KODE_PROGRAM_UTAMA}
            </div>
            <div class="field_form">
                <label>Tujuan : </label>
                {$program.TUJUAN_RENSTRA}
            </div>
            <div class="field_form">
                <label>Isi Program : </label>
                {$program.PROGRAM_UTAMA}
            </div>
            <div class="bottom_field_form">
                Apakah anda yakin menghapus item ini ?<br/>
                <input type="hidden" name="mode" value="delete"/>
                <input type="hidden" name="id" value="{$program.ID_PROGRAM_UTAMA}" />
                <a href="lakip-program.php" class="button">Batal</a>
                <input type="submit" class="button" value="Ya"/>
            </div>
        </fieldset>
    </form>
{else}
    <div class="center_title_bar">LAKIP Program</div>
    <table style="width: 98%">
        <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Tujuan</th>
            <th>Program</th>
            <th>Keterangan</th>
            <th style="width: 120px" class="center">Operasi</th>
        </tr>
        {foreach $data_program as $pr}
            <tr>
                <td>{$pr@index+1}</td>
                <td>{$pr.KODE_PROGRAM_UTAMA}</td>
                <td>{$pr.TUJUAN_RENSTRA}</td>
                <td>{$pr.PROGRAM_UTAMA}</td>
                <td>{$pr.KETERANGAN}</td>
                <td>
                    <a class="button" href="lakip-program.php?mode=edit&id={$pr.ID_PROGRAM_UTAMA}">Edit</a>
                    <a class="button" href="lakip-program.php?mode=delete&id={$pr.ID_PROGRAM_UTAMA}">Delete</a>
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="6" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="6" class="center">
                <a href="lakip-program.php?mode=tambah" class="button">Tambah</a>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script>
        $('form').validate();
    </script>
{/literal}