<?php

include 'config.php';
include 'class/lakip.class.php';
include '../keuangan/class/list_data.class.php';

$lak = new lakip($db);
$list = new list_data($db);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $lak->tambah_lakip_program_kerja(post('fakultas'), post('periode'), post('program_utama'), post('kode'), post('program_kerja'), post('keterangan'));
    } else if (post('mode') == 'edit') {
        $lak->update_lakip_program_kerja(post('id'), post('fakultas'), post('periode'), post('program_utama'), post('kode'), post('program_kerja'), post('keterangan'));
    } else if (post('mode') == 'delete') {
        $lak->delete_lakip_program_kerja(post('id'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit' || get('mode') == 'delete') {
        $smarty->assign('program', $lak->get_lakip_program_kerja(get('id')));
    }
}

$smarty->assign('data_program_utama', $lak->load_lakip_program());
$smarty->assign('data_periode',$lak->load_lakip_periode());
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_tujuan',$lak->load_lakip_tujuan());
$smarty->assign('data_proker',$lak->load_lakip_program_kerja());
$smarty->display('lakip-proker.tpl');
?>
