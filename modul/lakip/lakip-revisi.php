<?php

include 'config.php';
include 'class/lakip.class.php';
include '../keuangan/class/list_data.class.php';

$lak = new lakip($db);
$list = new list_data($db);

if (isset($_POST)) {
    if (post('mode') == 'revisi') {
        $lak->revisi_usulan_lakip(post('id'), post('revisi'), post('pagu'), post('serapan'), post('keterangan'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'revisi') {
        $smarty->assign('usulan', $lak->get_usulan_lakip(get('id')));
    }
}
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_proker', $lak->load_lakip_program_kerja());
$smarty->assign('data_usulan', $lak->load_usulan_lakip());
$smarty->display('lakip-revisi.tpl');
?>
