<?php

include 'config.php';
include 'class/lakip.class.php';

$lak = new lakip($db);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $lak->tambah_lakip_program(post('kode'), post('tujuan'), post('program'), post('keterangan'));
    } else if (post('mode') == 'edit') {
        $lak->update_lakip_program(post('id'), post('kode'), post('tujuan'), post('program'), post('keterangan'));
    } else if (post('mode') == 'delete') {
        $lak->delete_lakip_program(post('id'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit' || get('mode') == 'delete') {
        $smarty->assign('program', $lak->get_lakip_program(get('id')));
    }
}

$smarty->assign('data_program', $lak->load_lakip_program());
$smarty->assign('data_tujuan', $lak->load_lakip_tujuan());
$smarty->display('lakip-program.tpl');
?>
