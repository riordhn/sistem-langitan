<?php

include 'config.php';
include 'class/lakip.class.php';
include '../keuangan/class/list_data.class.php';

$lak = new lakip($db);
$list = new list_data($db);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $lak->tambah_lakip_indikator(post('program_kerja'), post('kode'), post('nama'), post('target'));
    } else if (post('mode') == 'edit') {
        $lak->update_lakip_indikator(post('id'), post('program_kerja'), post('kode'), post('nama'), post('target'));
    } else if (post('mode') == 'delete') {
        $lak->delete_lakip_indikator(post('id'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit' || get('mode') == 'delete') {
        $smarty->assign('indikator', $lak->get_lakip_indikator(get('id')));
    }
}
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_proker',$lak->load_lakip_program_kerja());
$smarty->assign('data_indikator',$lak->load_lakip_indikator());
$smarty->display('lakip-indikator.tpl');
?>
