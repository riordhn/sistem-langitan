<?php

include 'config.php';
include 'class/lakip.class.php';

$lak = new lakip($db);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $lak->tambah_lakip_periode(post('nama'), post('awal'), post('akhir'));
    } else if (post('mode') == 'edit') {
        $lak->update_lakip_periode(post('id'), post('nama'), post('awal'), post('akhir'));
    } else if (post('mode') == 'delete') {
        $lak->delete_lakip_periode(post('id'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit' || get('mode') == 'delete') {
        $smarty->assign('periode', $lak->get_lakip_periode(get('id')));
    }
}

$smarty->assign('data_periode', $lak->load_lakip_periode());
$smarty->display('lakip-periode.tpl');
?>
