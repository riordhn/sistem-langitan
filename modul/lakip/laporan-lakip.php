<?php

include 'config.php';
include 'class/lakip.class.php';
include '../keuangan/class/list_data.class.php';

$lak = new lakip($db);
$list = new list_data($db);

$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_proker',$lak->load_lakip_program_kerja());
$smarty->assign('data_laporan',$lak->load_laporan_lakip(get('fakultas')));
$smarty->display('laporan-lakip.tpl');
?>
