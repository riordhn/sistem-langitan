<?php

include 'config.php';
include 'class/lakip.class.php';
include '../keuangan/class/list_data.class.php';

$lak = new lakip($db);
$list = new list_data($db);

if (isset($_POST)) {
    $id = post('id');
    if (post('mode') == 'proker') {
        $cek_status = $db->QuerySingle("SELECT STATUS FROM LAKIP_PROGRAM_KERJA WHERE ID_PROGRAM_KERJA='{$id}'");
        if ($cek_status == 1) {
            $db->Query("UPDATE LAKIP_PROGRAM_KERJA SET STATUS=0 WHERE ID_PROGRAM_KERJA='{$id}'");
            $db->Query("UPDATE LAKIP_ANGGARAN SET STATUS=0 WHERE ID_PROGRAM_KERJA='{$id}'");
            $db->Query("UPDATE LAKIP_INDIKATOR_KINERJA SET STATUS=0 WHERE ID_PROGRAM_KERJA='{$id}'");
        } else {
            $db->Query("UPDATE LAKIP_PROGRAM_KERJA SET STATUS=1 WHERE ID_PROGRAM_KERJA='{$id}'");
            $db->Query("UPDATE LAKIP_ANGGARAN SET STATUS=1 WHERE ID_PROGRAM_KERJA='{$id}'");
            $db->Query("UPDATE LAKIP_INDIKATOR_KINERJA SET STATUS=1 WHERE ID_PROGRAM_KERJA='{$id}'");
        }
    } else if (post('mode') == 'anggaran') {
        $cek_status = $db->QuerySingle("SELECT STATUS FROM LAKIP_ANGGARAN WHERE ID_ANGGARAN='{$id}'");
        if ($cek_status == 1) {
            $db->Query("UPDATE LAKIP_ANGGARAN SET STATUS=0 WHERE ID_ANGGARAN='{$id}'");
        } else {
            $db->Query("UPDATE LAKIP_ANGGARAN SET STATUS=1 WHERE ID_ANGGARAN='{$id}'");
        }
    } else if (post('mode') == 'indikator') {
        $cek_status = $db->QuerySingle("SELECT STATUS FROM LAKIP_INDIKATOR_KINERJA WHERE ID_INDIKATOR_KINERJA='{$id}'");
        if ($cek_status == 1) {
            $db->Query("UPDATE LAKIP_INDIKATOR_KINERJA SET STATUS=0 WHERE ID_INDIKATOR_KINERJA='{$id}'");
        } else {
            $db->Query("UPDATE LAKIP_INDIKATOR_KINERJA SET STATUS=1 WHERE ID_INDIKATOR_KINERJA='{$id}'");
        }
    }
}

if (isset($_GET)) {
    if (get('mode') == 'capaian') {
        $smarty->assign('indikator', $lak->load_lakip_capaian_indikator(get('id')));
    }
}
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_proker', $lak->load_lakip_program_kerja());
$smarty->assign('data_laporan', $lak->load_laporan_lakip());
$smarty->display('lakip-approve.tpl');
?>
