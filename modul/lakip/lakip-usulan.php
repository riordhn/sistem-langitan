<?php

include 'config.php';
include 'class/lakip.class.php';
include '../keuangan/class/list_data.class.php';

$lak = new lakip($db);
$list = new list_data($db);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $lak->tambah_usulan_lakip(post('program_kerja'), post('usulan_pagu'), post('keterangan'));
    } else if (post('mode') == 'edit') {
        $lak->update_usulan_lakip(post('id'), post('program_kerja'), post('usulan_pagu'), post('keterangan'));
    } else if (post('mode') == 'delete') {
        $lak->delete_usulan_lakip(post('id'));
    }
}

if (isset($_GET)) {
    if (get('mode') == 'edit' || get('mode') == 'delete') {
        $smarty->assign('usulan', $lak->get_usulan_lakip(get('id')));
    }
}
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_proker', $lak->load_lakip_program_kerja());
$smarty->assign('data_usulan', $lak->load_usulan_lakip());
$smarty->display('lakip-usulan.tpl');
?>
