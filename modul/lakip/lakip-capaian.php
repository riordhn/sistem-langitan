<?php

include 'config.php';
include 'class/lakip.class.php';
include '../keuangan/class/list_data.class.php';

$lak = new lakip($db);
$list = new list_data($db);

if (isset($_POST)) {
    if (post('mode') == 'capaian') {
        for ($i = 1; $i <= post('jumlah'); $i++) {
            $id_indikator = post('id_indikator' . $i);
            $capaian = post('capaian' . $i);
            $keterangan = post('keterangan' . $i);
            $cek_keterangan = $db->QuerySingle("SELECT COUNT(*) FROM LAKIP_ANALISA WHERE ID_INDIKATOR_KINERJA='{$id_indikator}'");
            if ($id_indikator != '') {
                $db->Query("UPDATE LAKIP_INDIKATOR_KINERJA SET CAPAIAN='{$capaian}' WHERE ID_INDIKATOR_KINERJA='{$id_indikator}'");
                if ($cek_keterangan > 0) {
                    // Update Keterangan
                    $db->Query("UPDATE LAKIP_ANALISA SET KETERANGAN_ANALISA='{$keterangan}' WHERE ID_INDIKATOR_KINERJA='{$id_indikator}'");
                } else {
                    //Insert Keterangan
                    $db->Query("INSERT INTO LAKIP_ANALISA (ID_INDIKATOR_KINERJA,KETERANGAN_ANALISA) VALUES ('{$id_indikator}','{$keterangan}')");
                }
            }
        }
    }
}

if (isset($_GET)) {
    if (get('mode') == 'capaian') {
        $smarty->assign('indikator', $lak->load_lakip_capaian_indikator(get('id')));
    }
}
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->assign('data_proker', $lak->load_lakip_program_kerja());
$smarty->assign('data_laporan', $lak->load_laporan_lakip());
$smarty->display('lakip-capaian.tpl');
?>
