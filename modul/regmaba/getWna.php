<?PHP
include 'config.php';
include '../registrasi/class/Kota.class.php';


$Kota = new Kota($db);

$mode = get('mode', 'view');
$id_kota = get('id_kota', '');

if ($mode == 'view')
{
    $smarty->assign('negara_set', $Kota->GetListNegara());  
	$smarty->assign('negara_indo_set', $Kota->GetListNegaraIndonesia());
	$smarty->assign('negara_asing_set', $Kota->GetListNegaraAsing()); 
}
$smarty->display("form/wna.tpl");
?>