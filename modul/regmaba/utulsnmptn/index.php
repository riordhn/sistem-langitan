<html>
<head>
    <title>Registrasi Mahasiswa Baru (Regmaba) - Universitas Airlangga</title>
    <meta name="google-site-verification" content="ihkYFpHX_zbP4SfrkqIEF7c2Lp08dZFIxJ2MKI6m_Hk" />
    <link rel="shortcut icon" href="http://cybercampus.unair.ac.id/img/icon.ico"/>
    <link rel="stylesheet" type="text/css" href="http://cybercampus.unair.ac.id/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="http://regmaba.unair.ac.id/css/style.css" />
    <meta name="description" content="Halaman registrasi ulang mahasiswa baru yang telah diterima di Universitas Airlangga">
</head>

<body>
    <div class="wrap-box2">
        <div class="login-box">
            <table>
                <tr>
                    <td class="center-align"><strong><a href="daftar snmptn-tulis-2012.doc">PENGUMUMAN PENDAFTARAN ULANG MAHASISWA BARU UNIVERSITAS AIRLANGGA (UNAIR) YANG DITERIMA MELALUI SELEKSI NASIONAL MASUK PERGURUAN TINGGI NEGERI (SNMPTN) JALUR UJIAN TULIS TAHUN 2012</a></strong></td>
                </tr>
				<tr>
                    <td class="center-align"><strong></td>
                </tr>
				<tr>
                    <td class="center-align"><strong><a href="../">Registrasi Mahasiswa Baru Online</a></strong></td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>