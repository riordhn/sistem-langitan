<?php
include '../../config.php';

if (!in_array($_SERVER['REMOTE_ADDR'], array('210.57.212.66')))
{
    //echo "Server sedang maintenis, silahkan kembali beberapa saat lagi..."; exit();
}

function date_to_timestamp($date)
{
    $tgl_lahir_parse = date_parse($date);
    if ($tgl_lahir_parse['year'] > date('Y')) $tgl_lahir_parse['year'] -= 100;
    return mktime(0, 0, 0, $tgl_lahir_parse['month'], $tgl_lahir_parse['day'], $tgl_lahir_parse['year']);
}
?>
