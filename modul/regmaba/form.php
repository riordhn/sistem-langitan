<?php

session_start();

if ($_SERVER['REMOTE_ADDR'] != '36.73.235.75')
{
	// echo "Tunggu beberapa menit lagi... (untuk mengecek klik Refresh / Ctrl+F5)";
	// exit();
}

include '../../config.php';
include 'class/CalonMahasiswaNambi.class.php';
include '../registrasi/class/CalonMahasiswa.class.php';
include 'class/Kota.class.php';

$CalonMahasiswa = new CalonMahasiswa($db);
$CalonMahasiswaNambi = new CalonMahasiswaNambi($db);
$Kota = new Kota($db);

$mode = get('mode', 'view');

// Melakukan cek apakah sudah login atau belum
if (empty($_SESSION['ID_C_MHS']))
{
	header("location: /index.php");
	exit();
}

$id_c_mhs = $_SESSION['ID_C_MHS'];

function getTemplate(&$cmb, $page = '')
{
	if ($_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] != '')
	{
		//hasil tes kesehatan
		$form_template = 'hasil-kesehatan';
	}
	else
	{
		// snmptn
		if ($cmb['ID_JALUR'] == 1)
		{
			$form_template = 's1-snmptn';
		}

		// mandiri
		if ($cmb['ID_JALUR'] == 3 || $cmb['ID_JALUR'] == 20 || $cmb['ID_JALUR'] == 27)
		{
			$form_template = 's1';
		}
		if ($cmb['ID_JALUR'] == 4)
		{
			$form_template = 's1-aj';
		}
		if ($cmb['ID_JALUR'] == 5)
		{
			$form_template = 'd3';
		}

		// Pasca
		//if ($cmb['ID_JALUR'] == 6)
		//{
		if ($cmb['ID_JENJANG'] == 2)
			$form_template = 's2';
		if ($cmb['ID_JENJANG'] == 3)
			$form_template = 's3';
		if ($cmb['ID_JENJANG'] == 10)
			$form_template = 'sp';
		//}
		// Profesi
		if ($cmb['ID_JALUR'] == 23)
		{
			$form_template = 'pr';
		}
	}

	if ($page != '')
		$form_template = $form_template . "-" . $page;

	// return hasil
	return $form_template;
}

if ($request_method == 'POST')
{

	// snmptn-page-1
	if (post('mode') == 'page-1')
	{
		$jenis_berkas = array('ijazah', 'skhun', 'akte', 'kk', 'penghasilan', 'siup', 'petani', 'sppt_pbb', 'listrik', 'air', 'stnk_motor', 'stnk_mobil');

		foreach ($jenis_berkas as $berkas)
		{
			if ($_FILES["file_{$berkas}"]['size'] <= (1024 * 1024 * 32))
			{  // diganti jadi 32MB
				$destination = "/var/www/html/files/snmptn/pdf-berkas/{$id_c_mhs}_{$berkas}.pdf";
				if (move_uploaded_file($_FILES["file_{$berkas}"]['tmp_name'], $destination))
					$CalonMahasiswa->UpdateFile($id_c_mhs, "file_{$berkas}", '1');
			}
		}

		$result = $CalonMahasiswaNambi->Update($id_c_mhs, $_POST);
		$CalonMahasiswaNambi->UpdateTglRegmaba($id_c_mhs);
		if ($_POST['status_bidik_misi'] == 1)
		{
			$smarty->assign('status_bidik_misi', $_POST['status_bidik_misi']);
		}

		if ($result)
			$form_template = 'result';
		else
			$form_template = 'failed';
	}

	// snmptn-page-2
	if (post('mode') == 'page-2')
	{
		//$result = $CalonMahasiswa->UpdateIsianSp3($id_c_mhs, $_POST);
		//$CalonMahasiswaNambi->UpdateJadwalVerifikasi($id_c_mhs, $_POST);

		$form_template = 'result';
	}


	// mandiri d3
	if (post('mode') == 'd3')
	{
		$jenis_berkas = array('ijazah', 'skhun', 'akte', 'kk');

		foreach ($jenis_berkas as $berkas)
		{
			if ($_FILES["file_{$berkas}"]['size'] <= 5242880)
			{
				$destination = "/var/www/html/files/snmptn/pdf-berkas/{$id_c_mhs}_{$berkas}.pdf";
				if (move_uploaded_file($_FILES["file_{$berkas}"]['tmp_name'], $destination))
					$CalonMahasiswa->UpdateFile($id_c_mhs, "file_{$berkas}", '1');
			}
		}

		$result = $CalonMahasiswa->Update($id_c_mhs, $_POST);

		if ($result)
			$form_template = 'result';
		else
			$form_template = 'failed';
	}

	// mandiri s1-alih jenis
	if (post('mode') == 's1-aj')
	{
		$jenis_berkas = array('ijazah');

		foreach ($jenis_berkas as $berkas)
		{
			if ($_FILES["file_{$berkas}"]['size'] <= 5242880)
			{
				$destination = "/var/www/html/files/snmptn/pdf-berkas/{$id_c_mhs}_{$berkas}.pdf";
				if (move_uploaded_file($_FILES["file_{$berkas}"]['tmp_name'], $destination))
					$CalonMahasiswa->UpdateFile($id_c_mhs, "file_{$berkas}", '1');
			}
		}

		$result = $CalonMahasiswa->Update($id_c_mhs, $_POST);

		if ($result)
			$form_template = 'result';
		else
			$form_template = 'failed';
	}

	// pasca sarjana s2, s3
	if (in_array(post('mode'), array('s2', 's3', 'pr', 'sp')))
	{
		$jenis_berkas = array('ijazah');

		foreach ($jenis_berkas as $berkas)
		{
			if ($_FILES["file_{$berkas}"]['size'] <= 5242880)
			{
				$destination = "/var/www/html/files/snmptn/pdf-berkas/{$id_c_mhs}_{$berkas}.pdf";
				if (move_uploaded_file($_FILES["file_{$berkas}"]['tmp_name'], $destination))
					$CalonMahasiswa->UpdateFile($id_c_mhs, "file_{$berkas}", '1');
			}
		}

		$result = $CalonMahasiswa->Update($id_c_mhs, $_POST);

		if ($result)
		{
			$form_template = 'result';
		}
		else
		{
			$form_template = 'failed';
			$smarty->assign('error_message', $CalonMahasiswa->ErrorMessage);
			$smarty->assign('debug_message', $CalonMahasiswa->DebugMessage);
		}
	}
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'hasil-kesehatan')
	{
		// Mendapatkan data calon mahasiswa secara lengkap
		$cmb = $CalonMahasiswa->GetData($id_c_mhs, true);
		$smarty->assign('cmb', $cmb);

		//harcode dayat >> opo o kok hardcode ? (tony)
		if ($id_c_mhs == '59696' or $id_c_mhs == '60437')
		{
			
		}
		else
		{
			$db->Query("SELECT KESEHATAN_KESIMPULAN_AKHIR, TO_CHAR(JADWAL_KESEHATAN.TGL_TEST, 'DD-MM-YYYY') AS TGL_TEST_KESEHATAN, 
					TGL_VERIFIKASI_KESEHATAN, NIM_MHS
					  FROM CALON_MAHASISWA_BARU
					  JOIN PENERIMAAN ON PENERIMAAN.ID_PENERIMAAN = CALON_MAHASISWA_BARU.ID_PENERIMAAN
					  LEFT JOIN CALON_MAHASISWA_DATA ON CALON_MAHASISWA_DATA.ID_C_MHS = CALON_MAHASISWA_BARU.ID_C_MHS
					  LEFT JOIN JADWAL_KESEHATAN ON JADWAL_KESEHATAN.ID_JADWAL_KESEHATAN = CALON_MAHASISWA_DATA.ID_JADWAL_KESEHATAN
					  WHERE CALON_MAHASISWA_BARU.ID_C_MHS = '$id_c_mhs'
					  AND TO_CHAR(JADWAL_KESEHATAN.TGL_TEST, 'YYYY-MM-DD') <= TO_CHAR(TGL_AKHIR_KESEHATAN, 'YYYY-MM-DD')
					  AND TO_CHAR(JADWAL_KESEHATAN.TGL_TEST, 'YYYY-MM-DD') >= TO_CHAR(TGL_AWAL_KESEHATAN, 'YYYY-MM-DD')");

			$hasil_kesehatan = $db->FetchAssoc();
			$smarty->assign('hasil_kesehatan', $hasil_kesehatan);

			$db->Query("SELECT TO_CHAR(JADWAL_TOEFL.TGL_TEST, 'DD-MM-YYYY') AS TGL_TEST_ELPT, TGL_VERIFIKASI_ELPT, ELPT_SCORE
                                              FROM CALON_MAHASISWA_BARU
                                              JOIN PENERIMAAN ON PENERIMAAN.ID_PENERIMAAN = CALON_MAHASISWA_BARU.ID_PENERIMAAN
                                              LEFT JOIN CALON_MAHASISWA_DATA ON CALON_MAHASISWA_DATA.ID_C_MHS = CALON_MAHASISWA_BARU.ID_C_MHS
                                              LEFT JOIN JADWAL_TOEFL ON JADWAL_TOEFL.ID_JADWAL_TOEFL = CALON_MAHASISWA_DATA.ID_JADWAL_TOEFL
                                              WHERE CALON_MAHASISWA_BARU.ID_C_MHS = '$id_c_mhs'");

			$hasil_elpt = $db->FetchAssoc();
			$smarty->assign('hasil_elpt', $hasil_elpt);
		}
		$form_template = getTemplate($cmb);
	}

	if ($mode == 'page-1' or $mode == 'view')
	{
		// Mendapatkan data calon mahasiswa secara lengkap
		$cmb = $CalonMahasiswaNambi->GetData($id_c_mhs, true);
		$smarty->assign('cmb', $cmb);

		$form_template = getTemplate($cmb);

		$smarty->assign('kota_set', $CalonMahasiswa->GetListKota());
		$smarty->assign('negara_set', $Kota->GetListNegara());
		$smarty->assign('negara_indo_set', $Kota->GetListNegaraIndonesia());
		$smarty->assign('negara_asing_set', $Kota->GetListNegaraAsing());
		$smarty->assign('jenis_kelamin_set', $CalonMahasiswa->GetListJenisKelamin());
		$smarty->assign('kewarganegaraan_set', $CalonMahasiswa->GetListKewarganegaraan());
		$smarty->assign('agama_set', $CalonMahasiswa->GetListAgama());
		$smarty->assign('sumber_biaya_set', $CalonMahasiswa->GetListSumberBiaya());
		$smarty->assign('jurusan_sekolah_set', $CalonMahasiswa->GetListJurusanSekolah());
		$smarty->assign('pendidikan_ortu_set', $CalonMahasiswa->GetListPendidikanOrtu());
		$smarty->assign('pekerjaan_set', $CalonMahasiswa->GetListPekerjaan());
		$smarty->assign('penghasilan_ortu_set', $CalonMahasiswa->GetListPenghasilanOrtu());
		$smarty->assign('skala_pekerjaan_set', $CalonMahasiswa->GetListSkalaPekerjaan());
		$smarty->assign('kediaman_ortu_set', $CalonMahasiswa->GetListKediamanOrtu());
		$smarty->assign('luas_tanah_set', $CalonMahasiswa->GetListLuasTanah());
		$smarty->assign('luas_bangunan_set', $CalonMahasiswa->GetListLuasBangunan());
		$smarty->assign('njop_set', $CalonMahasiswa->GetListNJOP());
		$smarty->assign('listrik_set', $CalonMahasiswa->GetListListrik());
		$smarty->assign('kendaraan_r4_set', $CalonMahasiswa->GetListKendaraanR4());
		$smarty->assign('kendaraan_r2_set', $CalonMahasiswa->GetListKendaraanR2());
		$smarty->assign('status_ptn_set', $CalonMahasiswa->GetListStatusPTN());
		$smarty->assign('program_studi_set', $CalonMahasiswa->GetListProgramStudi($id_c_mhs));

		// Tambahan Nambi
		// Khusus S2
		$prodi_minat_set = $db->QueryToArray("
			select * from prodi_minat pm
			left join penerimaan_prodi_minat ppm on ppm.id_prodi_minat = pm.id_prodi_minat
			where ppm.is_aktif = 1 and ppm.id_penerimaan = '".$cmb['ID_PENERIMAAN']."' and pm.id_program_studi = ".$cmb['ID_PROGRAM_STUDI']."
			");
		$smarty->assign('prodi_minat_set', $prodi_minat_set);
		$smarty->assign('data_pekerjaan', $CalonMahasiswaNambi->LoadPekerjaan());
		$smarty->assign('data_range_listrik', $CalonMahasiswaNambi->LoadRange(5));
		$smarty->assign('data_range_pbb', $CalonMahasiswaNambi->LoadRange(7));
		$smarty->assign('data_range_njop', $CalonMahasiswaNambi->LoadRange(3));
		$smarty->assign('data_range_air', $CalonMahasiswaNambi->LoadRange(6));
		// Penutupan Form (Untuk Membuka Kosongi nilai variabel)
		$smarty->assign('tutup_form', '');

		if ($cmb['ID_JALUR'] == 1)
		{  // khusus snmptn ada halamn ke dua
			if ($cmb['STATUS_BIDIK_MISI'] == 1)
			{
				$smarty->assign('next_page', $p = 'page-result');
			}
			else
			{
				$smarty->assign('next_page', $p = 'page-2');
			}
		}
		else
		{
			$CalonMahasiswaNambi->UpdateTglRegmaba($id_c_mhs);
			$smarty->assign('next_page', $p = 'page-result');
		}
	}

	if ($mode == 'page-2')
	{
		$cmb = $CalonMahasiswa->GetData($id_c_mhs, true);
		$smarty->assign('cmb', $cmb);

		//$sp3 = $CalonMahasiswa->GetSp3ByPendapatanOrtu($id_c_mhs, $cmb['TOTAL_PENDAPATAN_ORTU']);
		//$smarty->assign('sp3', $sp3);
		// Cek Sudah Verifikasi Keuangan atau belum
		$id_biaya_kuliah = $db->QuerySingle("SELECT ID_BIAYA_KULIAH FROM BIAYA_KULIAH_CMHS WHERE ID_C_MHS='{$id_c_mhs}'");
		$smarty->assign('id_biaya_kuliah', $id_biaya_kuliah);

		$kelompok = $CalonMahasiswaNambi->GetKelompokBiayaMaba($id_c_mhs);
		$smarty->assign('kelompok', $kelompok);

		$jadwal_set = $CalonMahasiswa->GetListJadwalVerifikasiKeuangan($cmb['ID_PENERIMAAN']);
		$smarty->assign('jadwal_set', $jadwal_set);

		$jadwal_set_pend = $CalonMahasiswa->GetListJadwalVerifikasiPendidikan($cmb['ID_PENERIMAAN']);
		$jadwal_set_pend = $db->FetchAssoc();
		$smarty->assign('jadwal_set_pend', $jadwal_set_pend);

		$form_template = getTemplate($cmb, 2);
	}

	if ($mode == 'page-result')
	{
		$cmb = $CalonMahasiswa->GetData($id_c_mhs, true);
		$smarty->assign('cmb', $cmb);
	}

	// Autocomplete kota
	if ($mode == 'get-kota')
	{
		$term = trim(strip_tags(get('term')));

		echo json_encode($CalonMahasiswa->GetListKotaSearch(strtoupper($term)));
		exit();
	}

	// autocomplete sekolah
	if ($mode == 'get-sekolah')
	{
		$term = trim(strip_tags(get('term')));

		echo json_encode($CalonMahasiswa->GetListSekolahSearch(strtoupper($term)));
		exit();
	}

	if ($mode == 'get-prodi')
	{
		$terpilih = implode(",", $_GET['terpilih']);

		foreach ($CalonMahasiswa->GetListProgramStudi($id_c_mhs, $terpilih) as $ps)
		{
			echo "<option value=\"{$ps['ID_PROGRAM_STUDI']}\">{$ps['NM_PROGRAM_STUDI']}</option>";
		}

		exit();
	}

	if ($mode == 'get-prodi-minat')
	{
		$id_program_studi = $_GET['id_program_studi'];

		foreach ($CalonMahasiswa->GetListProgramStudiMinat($id_c_mhs, $id_program_studi) as $ps)
		{
			echo "<option value=\"{$ps['ID_PRODI_MINAT']}\">{$ps['NM_PRODI_MINAT']}</option>";
		}
		exit();
	}

	if ($mode == 'get-json-sp3-by-pendapatan')
	{
		$total_pendapatan_ortu = get('total_pendapatan_ortu', 0);

		$row = $CalonMahasiswa->GetSp3ByPendapatanOrtu($id_c_mhs, $total_pendapatan_ortu);

		$result = array(
			'min_sp3' => $row['SP3'],
			'id_kelompok_biaya' => $row['ID_KELOMPOK_BIAYA'],
		);

		echo json_encode($result);
		exit();
	}
}

$smarty->display("form/{$form_template}.tpl");
?>
