<?php

class CalonMahasiswaNambi {

    private $db;

    function __construct($oracle_db) {
        $this->db = $oracle_db;
    }

    function GetData($id_c_mhs, $is_regmaba = false) {
        if ($is_regmaba)
            $left = "left";

        $rows = $this->db->QueryToArray("
            select
                cmb.id_c_mhs, cmb.nim_mhs,
            
                -- informasi penerimaan
                cmb.id_penerimaan, p.id_jalur, p.id_jenjang, v.kode_jurusan, p.tahun as tahun_penerimaan, no_invoice,
                p.nm_penerimaan, p.gelombang as gelombang_penerimaan, p.semester as semester_penerimaan,
                
                -- biodata
                cmb.kode_voucher, cmb.no_ujian, nm_c_mhs, gelar, id_kota_lahir, to_char(tgl_lahir,'yyyy-mm-dd') as tgl_lahir, 
                alamat, id_kota, telp, jenis_kelamin,kewarganegaraan, id_agama, sumber_biaya, email, status_ujian,
                id_disabilitas, id_kebangsaan, cmb.telp_utama, cmb.pin_bb,
                
                -- Isian prodi / minat / isian sp3
                id_program_studi, id_pilihan_1, id_pilihan_2, id_pilihan_3, id_pilihan_4, cmp.id_prodi_minat, id_kelas_pilihan, sp3_1, sp3_2, sp3_3, sp3_4, id_kelompok_biaya,
                (select nm_kelompok_biaya from kelompok_biaya kb where kb.id_kelompok_biaya = cmb.id_kelompok_biaya) as nm_kelompok_biaya,
                (select nm_prodi_kelas from prodi_kelas pk where pk.id_prodi_kelas = cmb.id_kelas_pilihan) as nm_kelas_pilihan,
                
                -- data sekolah
                id_sekolah_asal, cms.jurusan_sekolah, tahun_lulus, no_ijazah, to_char(tgl_ijazah, 'yyyy-mm-dd') as tgl_ijazah, jumlah_pelajaran_ijazah, nilai_ijazah, tahun_uan, jumlah_pelajaran_uan, nilai_uan, cms.nisn,
                
                -- data orang tua
                nama_ayah, alamat_ayah, id_kota_ayah, telp_ayah, pendidikan_ayah, pekerjaan_ayah, instansi_ayah, jabatan_ayah, masa_kerja_ayah,
                nama_ibu, alamat_ibu, id_kota_ibu, telp_ibu, pendidikan_ibu, pekerjaan_ibu, instansi_ibu, jabatan_ibu, masa_kerja_ibu,
                penghasilan_ortu, skala_pekerjaan_ortu, jumlah_kakak, jumlah_adik,
                
                -- data kekayaan ortu
                kediaman_ortu, luas_tanah, luas_bangunan, njop, listrik, kendaraan_r4, kendaraan_r2, 
                kekayaan_lain, info_lain, tahun_kendaraan_r4, tahun_kendaraan_r2, merek_kendaraan_r4, merek_kendaraan_r2,
            
                -- data pendapatan ortu
                cmo.penghasilan_ayah, cmo.penghasilan_ibu,
                gaji_ayah, tunjangan_keluarga_ayah, tunjangan_jabatan_ayah, tunjangan_sertifikasi_ayah, tunjangan_kehormatan_ayah, renumerasi_ayah, tunjangan_lain_ayah, penghasilan_lain_ayah,
                gaji_ibu, tunjangan_keluarga_ibu, tunjangan_jabatan_ibu, tunjangan_sertifikasi_ibu, tunjangan_kehormatan_ibu, renumerasi_ibu, tunjangan_lain_ibu, penghasilan_lain_ibu,
                total_pendapatan_ortu,
                
                -- data terbaru
                cmo.range_listrik,cmo.range_air,cmo.range_njop,cmo.range_penghasilan,cmo.range_pbb,cmo.transportasi,cmo.jumlah_handphone,
            
                -- data pekerjaan
                pekerjaan, asal_instansi, alamat_instansi, telp_instansi, nrp, karpeg, pangkat,
            
                -- data pendidikan d3 / s1
                ptn_s1, status_ptn_s1, prodi_s1, to_char(tgl_masuk_s1,'yyyy-mm-dd') as tgl_masuk_s1, to_char(tgl_lulus_s1,'yyyy-mm-dd') as tgl_lulus_s1, lama_studi_s1, ip_s1, jumlah_karya_ilmiah, nim_lama_s1,
            
                -- data pendidikan s2 / pr
                ptn_s2, status_ptn_s2, prodi_s2, to_char(tgl_masuk_s2,'yyyy-mm-dd') as tgl_masuk_s2, to_char(tgl_lulus_s2,'yyyy-mm-dd') as tgl_lulus_s2, lama_studi_s2, ip_s2, nim_lama_s2,
            
                -- lookup kota lahir
                (select k.tipe_dati2||' '||k.nm_kota from kota k join provinsi p on p.id_provinsi = k.id_provinsi where k.id_kota = cmb.id_kota_lahir) as nm_kota_lahir,
            
                -- lookup kota
                (select k.tipe_dati2||' '||k.nm_kota from kota k join provinsi p on p.id_provinsi = k.id_provinsi where k.id_kota = cmb.id_kota) as nm_kota,
            
                -- lookup sekolah asal
                (select s.nm_sekolah||', '||k.nm_kota from sekolah s join kota k on k.id_kota = s.id_kota where s.id_sekolah = cms.id_sekolah_asal) as nm_sekolah_asal,
            
                -- lookup kota_ayah
                (select k.tipe_dati2||' '||k.nm_kota||', '|| p.nm_provinsi from kota k join provinsi p on p.id_provinsi = k.id_provinsi where k.id_kota = cmo.id_kota_ayah) as nm_kota_ayah,
            
                -- lookup kota ibu
                (select k.tipe_dati2||' '||k.nm_kota||', '|| p.nm_provinsi from kota k join provinsi p on p.id_provinsi = k.id_provinsi where k.id_kota = cmo.id_kota_ibu) as nm_kota_ibu,
            
                -- lookup pilihan prodi / diterima
                (select f.nm_fakultas from program_studi ps join fakultas f on f.id_fakultas = ps.id_fakultas where ps.id_program_studi = cmb.id_program_studi) as nm_fakultas,
                (select j.nm_jenjang||' '||ps.nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_program_studi) as nm_program_studi,
                (select j.nm_jenjang||' '||ps.nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_pilihan_1) as nm_pilihan_1,
                (select j.nm_jenjang||' '||ps.nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_pilihan_2) as nm_pilihan_2,
                (select j.nm_jenjang||' '||ps.nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_pilihan_3) as nm_pilihan_3,
                (select j.nm_jenjang||' '||ps.nm_program_studi from program_studi ps join jenjang j on j.id_jenjang = ps.id_jenjang where ps.id_program_studi = cmb.id_pilihan_4) as nm_pilihan_4,
                (select pm.nm_prodi_minat from prodi_minat pm where pm.id_prodi_minat = cmp.id_prodi_minat) as nm_prodi_minat,
            
                -- data verifikator
                cmb.id_verifikator_ppmb, cmb.id_verifikator_keuangan,
            
                -- data data untuk keperluan verifikasi
                status_bidik_misi, status_bidik_misi_baru, id_jadwal_verifikasi_keuangan,
                (select to_char(tgl_jadwal,'yyyy-mm-dd HH24:MI') from jadwal_verifikasi_keuangan j where j.id_jadwal = id_jadwal_verifikasi_keuangan) as tgl_jadwal_verifikasi_keuangan, 
				id_jadwal_verifikasi_pend,
				(select to_char(tgl_registrasi,'yyyy-mm-dd') from jadwal_verifikasi_pendidikan j where j.id_jadwal_verifikasi_pend = cmd.id_jadwal_verifikasi_pend) as tgl_jadwal_verifikasi_pend,
				(select to_char(jam_awal,'HH24:MI') from jadwal_verifikasi_pendidikan j where j.id_jadwal_verifikasi_pend = cmd.id_jadwal_verifikasi_pend) as jam_awal_verifikasi_pend,
				(select to_char(jam_akhir,'HH24:MI') from jadwal_verifikasi_pendidikan j where j.id_jadwal_verifikasi_pend = cmd.id_jadwal_verifikasi_pend) as jam_akhir_verifikasi_pend,
            
                -- nilai skor perolehan untuk jalur mandiri (mandiri, diploma, alih-jenis depag)
                (select nilai_tpa from nilai_cmhs n where n.id_c_mhs = cmb.id_c_mhs and jurusan_sekolah = 1) as nilai_tpa,
                (select nilai_prestasi from nilai_cmhs n where n.id_c_mhs = cmb.id_c_mhs and jurusan_sekolah = 1) as nilai_prestasi_ipa,
                (select nilai_prestasi from nilai_cmhs n where n.id_c_mhs = cmb.id_c_mhs and jurusan_sekolah = 2) as nilai_prestasi_ips,
                (select nilai_tpa+nilai_prestasi from nilai_cmhs n where n.id_c_mhs = cmb.id_c_mhs and jurusan_sekolah = 1) as nilai_total_ipa,
                (select nilai_tpa+nilai_prestasi from nilai_cmhs n where n.id_c_mhs = cmb.id_c_mhs and jurusan_sekolah = 2) as nilai_total_ips,
            
                -- nilai pasca
                np.nilai_tpa as nilai_tpa_pasca, np.nilai_inggris, np.nilai_ilmu, np.nilai_wawancara, np.nilai_ipk, np.nilai_karya_ilmiah, 
                np.nilai_rekomendasi, np.nilai_matrikulasi, np.nilai_psiko, np.nilai_gab_ppds, np.total_nilai,
            
                -- file berkas
                file_ijazah, file_skhun, file_akte, file_kk, file_penghasilan, file_siup, file_petani, file_sppt_pbb, file_listrik, file_air, file_stnk_motor, file_stnk_mobil, berkas_ijazah,
            
                -- informasi pengumuman
                to_char(p.tgl_pengumuman, 'YYYY-MM-DD HH24:MI:SS') as tgl_pengumuman, cmb.tgl_diterima
            
            from calon_mahasiswa_baru cmb
            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
            left join voucher v on v.kode_voucher = cmb.kode_voucher
            join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
            join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
            join calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
            join calon_mahasiswa_data cmd on cmd.id_c_mhs = cmb.id_c_mhs
            left join calon_mahasiswa_file cmf on cmf.id_c_mhs = cmb.id_c_mhs
            left join nilai_cmhs_pasca np on np.id_c_mhs = cmb.id_c_mhs
            where cmb.id_c_mhs = {$id_c_mhs}");
        return $rows[0];
    }

    function Update($id_c_mhs, &$post) {
        $this->db->BeginTransaction();

        // tabel calon_mahasiswa_baru
        $this->db->Parse("
            update calon_mahasiswa_baru set
                nm_c_mhs        = upper(:nm_c_mhs),
                gelar           = :gelar,
                id_kota_lahir   = :id_kota_lahir,
                tgl_lahir       = to_date(:tgl_lahir,'dd-mm-yyyy'),
                alamat          = :alamat,
                id_kota         = :id_kota,
                telp            = :telp,
                jenis_kelamin   = :jenis_kelamin,
                kewarganegaraan = :kewarganegaraan,
                id_agama        = :id_agama,
                sumber_biaya    = :sumber_biaya,
                email           = :email,
                id_disabilitas	= :id_disabilitas,
                id_kebangsaan	= :id_kebangsaan,
                
                id_pilihan_1    = :id_pilihan_1,
                id_pilihan_2    = :id_pilihan_2,
                id_pilihan_3    = :id_pilihan_3,
                id_pilihan_4    = :id_pilihan_4,
                
                id_kelas_pilihan  = :id_kelas_pilihan,
                id_kelompok_biaya = :id_kelompok_biaya,
                
                telp_utama  = :telp_utama,
                pin_bb      = :pin_bb
            where id_c_mhs = :id_c_mhs");
        $this->db->BindByName(':nm_c_mhs', $post['nm_c_mhs']);
        $this->db->BindByName(':gelar', $post['gelar']);
        $this->db->BindByName(':id_kota_lahir', $post['id_kota_lahir']);
        $this->db->BindByName(':tgl_lahir', $tgl_lahir = str_pad($post['tgl_lahir_Day'], 2, "0", STR_PAD_LEFT) . "-" . $post['tgl_lahir_Month'] . "-" . $post['tgl_lahir_Year']);
        $this->db->BindByName(':alamat', $post['alamat']);
        $this->db->BindByName(':id_kota', $post['id_kota']);
        $this->db->BindByName(':telp', $post['telp']);
        $this->db->BindByName(':jenis_kelamin', $post['jenis_kelamin']);
        $this->db->BindByName(':kewarganegaraan', $post['kewarganegaraan']);
        $this->db->BindByName(':id_agama', $post['id_agama']);
        $this->db->BindByName(':sumber_biaya', $post['sumber_biaya']);
        $this->db->BindByName(':email', $post['email']);
        $this->db->BindByName(':id_disabilitas', $post['id_disabilitas']);
        $this->db->BindByName(':id_kebangsaan', $post['id_kebangsaan']);
        $this->db->BindByName(':id_pilihan_1', $post['id_pilihan_1']);
        $this->db->BindByName(':id_pilihan_2', $post['id_pilihan_2']);
        $this->db->BindByName(':id_pilihan_3', $post['id_pilihan_3']);
        $this->db->BindByName(':id_pilihan_4', $post['id_pilihan_4']);
        $this->db->BindByName(':id_kelas_pilihan', $post['id_kelas_pilihan']);
        $this->db->BindByName(':id_kelompok_biaya', $post['id_kelompok_biaya']);
        $this->db->BindByName(':telp_utama', $post['telp_utama']);
        $this->db->BindByName(':pin_bb', $post['pin_bb']);
        $this->db->BindByName(':id_c_mhs', $id_c_mhs);
        $result = $this->db->Execute();

        if (!$result)
            die("ERROR at " . __LINE__ . ": " . print_r(error_get_last(), true));

        // Cek tanggal ijazah
        $column_tgl_ijazah = ($post['tgl_ijazah_Day'] == '' or $post['tgl_ijazah_Month'] == '' or $post['tgl_ijazah_Year'] == '') ? "tgl_ijazah = null," : "tgl_ijazah = to_date(:tgl_ijazah,'dd-mm-yyyy'),";

        // tabel calon_mahasiswa_sekolah
        $this->db->Parse("
            update calon_mahasiswa_sekolah set
                nisn                    = :nisn,
                id_sekolah_asal         = :id_sekolah_asal,
                jurusan_sekolah         = :jurusan_sekolah,
                no_ijazah               = :no_ijazah,
                {$column_tgl_ijazah}
                tahun_lulus             = :tahun_lulus,
                jumlah_pelajaran_ijazah = :jumlah_pelajaran_ijazah,
                nilai_ijazah            = :nilai_ijazah,
                tahun_uan               = :tahun_uan,
                jumlah_pelajaran_uan    = :jumlah_pelajaran_uan,
                nilai_uan               = :nilai_uan
            where id_c_mhs = :id_c_mhs");
        $this->db->BindByName(':nisn', $post['nisn']);
        $this->db->BindByName(':id_sekolah_asal', $post['id_sekolah_asal']);
        $this->db->BindByName(':jurusan_sekolah', $post['jurusan_sekolah']);
        $this->db->BindByName(':no_ijazah', $post['no_ijazah']);
        $this->db->BindByName(':tgl_ijazah', $tgl_ijazah = str_pad($post['tgl_ijazah_Day'], 2, '0', STR_PAD_LEFT) . "-" . $post['tgl_ijazah_Month'] . "-" . $post['tgl_ijazah_Year']);
        $this->db->BindByName(':tahun_lulus', $post['tahun_lulus']);
        $this->db->BindByName(':jumlah_pelajaran_ijazah', $post['jumlah_pelajaran_ijazah']);
        $this->db->BindByName(':nilai_ijazah', $post['nilai_ijazah']);
        $this->db->BindByName(':tahun_uan', $post['tahun_uan']);
        $this->db->BindByName(':jumlah_pelajaran_uan', $post['jumlah_pelajaran_uan']);
        $this->db->BindByName(':nilai_uan', $post['nilai_uan']);
        $this->db->BindByName(':id_c_mhs', $id_c_mhs);
        $result = $this->db->Execute();

        if (!$result)
            die("ERROR at " . __LINE__ . ": <br/>" . print_r(error_get_last(), true));


        // tabel orang tua
        /*
          $this->db->Parse("
          update calon_mahasiswa_ortu set
          nama_ayah		= upper(:nama_ayah),
          alamat_ayah		= :alamat_ayah,
          id_kota_ayah		= :id_kota_ayah,
          telp_ayah		= :telp_ayah,
          pendidikan_ayah		= :pendidikan_ayah,
          pekerjaan_ayah		= :pekerjaan_ayah,
          instansi_ayah		= :instansi_ayah,
          jabatan_ayah		= :jabatan_ayah,
          masa_kerja_ayah		= :masa_kerja_ayah,
          penghasilan_ayah	= :penghasilan_ayah,
          nama_ibu		= upper(:nama_ibu),
          alamat_ibu		= :alamat_ibu,
          id_kota_ibu		= :id_kota_ibu,
          telp_ibu		= :telp_ibu,
          pendidikan_ibu		= :pendidikan_ibu,
          pekerjaan_ibu		= :pekerjaan_ibu,
          instansi_ibu		= :instansi_ibu,
          jabatan_ibu		= :jabatan_ibu,
          masa_kerja_ibu		= :masa_kerja_ibu,
          penghasilan_ibu         = :penghasilan_ibu,

          skala_pekerjaan_ortu    = :skala_pekerjaan_ortu,
          jumlah_kakak            = :jumlah_kakak,
          jumlah_adik             = :jumlah_adik,

          kediaman_ortu       = :kediaman_ortu,
          luas_tanah          = :luas_tanah,
          luas_bangunan       = :luas_bangunan,
          listrik             = :listrik,
          kendaraan_r2        = :kendaraan_r2,
          tahun_kendaraan_r2  = :tahun_kendaraan_r2,
          kendaraan_r4        = :kendaraan_r4,
          tahun_kendaraan_r4  = :tahun_kendaraan_r4,
          kekayaan_lain       = :kekayaan_lain,
          info_lain           = :info_lain,

          gaji_ayah                   = :gaji_ayah,
          tunjangan_keluarga_ayah     = :tunjangan_keluarga_ayah,
          tunjangan_jabatan_ayah      = :tunjangan_jabatan_ayah,
          tunjangan_sertifikasi_ayah  = :tunjangan_sertifikasi_ayah,
          tunjangan_kehormatan_ayah   = :tunjangan_kehormatan_ayah,
          renumerasi_ayah             = :renumerasi_ayah,
          tunjangan_lain_ayah         = :tunjangan_lain_ayah,
          penghasilan_lain_ayah       = :penghasilan_lain_ayah,

          gaji_ibu                   = :gaji_ibu,
          tunjangan_keluarga_ibu     = :tunjangan_keluarga_ibu,
          tunjangan_jabatan_ibu      = :tunjangan_jabatan_ibu,
          tunjangan_sertifikasi_ibu  = :tunjangan_sertifikasi_ibu,
          tunjangan_kehormatan_ibu   = :tunjangan_kehormatan_ibu,
          renumerasi_ibu             = :renumerasi_ibu,
          tunjangan_lain_ibu         = :tunjangan_lain_ibu,
          penghasilan_lain_ibu       = :penghasilan_lain_ibu,

          total_pendapatan_ortu       = :total_pendapatan_ortu,

          range_air           = :range_air,
          range_pbb           = :range_pbb,
          range_listrik       = :range_listrik,
          range_njop          = :range_njop,
          range_penghasilan   = :range_penghasilan,
          jumlah_handphone    = :jumlah_handphone,
          transportasi        = :transportasi
          where id_c_mhs = :id_c_mhs");
          $this->db->BindByName(':nama_ayah', $post['nama_ayah']);
          $this->db->BindByName(':alamat_ayah', $post['alamat_ayah']);
          $this->db->BindByName(':id_kota_ayah', $post['id_kota_ayah']);
          $this->db->BindByName(':telp_ayah', $post['telp_ayah']);
          $this->db->BindByName(':pendidikan_ayah', $post['pendidikan_ayah']);
          $this->db->BindByName(':pekerjaan_ayah', $post['pekerjaan_ayah']);
          $this->db->BindByName(':instansi_ayah', $post['instansi_ayah']);
          $this->db->BindByName(':jabatan_ayah', $post['jabatan_ayah']);
          $this->db->BindByName(':masa_kerja_ayah', $post['masa_kerja_ayah']);
          $this->db->BindByName(':penghasilan_ayah', $post['penghasilan_ayah']);

          $this->db->BindByName(':nama_ibu', $post['nama_ibu']);
          $this->db->BindByName(':alamat_ibu', $post['alamat_ibu']);
          $this->db->BindByName(':id_kota_ibu', $post['id_kota_ibu']);
          $this->db->BindByName(':telp_ibu', $post['telp_ibu']);
          $this->db->BindByName(':pendidikan_ibu', $post['pendidikan_ibu']);
          $this->db->BindByName(':pekerjaan_ibu', $post['pekerjaan_ibu']);
          $this->db->BindByName(':instansi_ibu', $post['instansi_ibu']);
          $this->db->BindByName(':jabatan_ibu', $post['jabatan_ibu']);
          $this->db->BindByName(':masa_kerja_ibu', $post['masa_kerja_ibu']);
          $this->db->BindByName(':penghasilan_ibu', $post['penghasilan_ibu']);

          $this->db->BindByName(':skala_pekerjaan_ortu', $post['skala_pekerjaan_ortu']);
          $this->db->BindByName(':jumlah_kakak', $post['jumlah_kakak']);
          $this->db->BindByName(':jumlah_adik', $post['jumlah_adik']);
          $this->db->BindByName(':kediaman_ortu', $post['kediaman_ortu']);
          $this->db->BindByName(':luas_tanah', $post['luas_tanah']);
          $this->db->BindByName(':luas_bangunan', $post['luas_bangunan']);
          $this->db->BindByName(':njop', $post['njop']);
          $this->db->BindByName(':listrik', $post['listrik']);
          $this->db->BindByName(':kendaraan_r2', $post['kendaraan_r2']);
          $this->db->BindByName(':tahun_kendaraan_r2', $post['tahun_kendaraan_r2']);
          $this->db->BindByName(':kendaraan_r4', $post['kendaraan_r4']);
          $this->db->BindByName(':tahun_kendaraan_r4', $post['tahun_kendaraan_r4']);
          $this->db->BindByName(':kekayaan_lain', $post['kekayaan_lain']);
          $this->db->BindByName(':info_lain', $post['info_lain']);

          $this->db->BindByName(':gaji_ayah', $post['gaji_ayah']);
          $this->db->BindByName(':tunjangan_keluarga_ayah', $post['tunjangan_keluarga_ayah']);
          $this->db->BindByName(':tunjangan_jabatan_ayah', $post['tunjangan_jabatan_ayah']);
          $this->db->BindByName(':tunjangan_sertifikasi_ayah', $post['tunjangan_sertifikasi_ayah']);
          $this->db->BindByName(':tunjangan_kehormatan_ayah', $post['tunjangan_kehormatan_ayah']);
          $this->db->BindByName(':renumerasi_ayah', $post['renumerasi_ayah']);
          $this->db->BindByName(':tunjangan_lain_ayah', $post['tunjangan_lain_ayah']);
          $this->db->BindByName(':penghasilan_lain_ayah', $post['penghasilan_lain_ayah']);

          $this->db->BindByName(':gaji_ibu', $post['gaji_ibu']);
          $this->db->BindByName(':tunjangan_keluarga_ibu', $post['tunjangan_keluarga_ibu']);
          $this->db->BindByName(':tunjangan_jabatan_ibu', $post['tunjangan_jabatan_ibu']);
          $this->db->BindByName(':tunjangan_sertifikasi_ibu', $post['tunjangan_sertifikasi_ibu']);
          $this->db->BindByName(':tunjangan_kehormatan_ibu', $post['tunjangan_kehormatan_ibu']);
          $this->db->BindByName(':renumerasi_ibu', $post['renumerasi_ibu']);
          $this->db->BindByName(':tunjangan_lain_ibu', $post['tunjangan_lain_ibu']);
          $this->db->BindByName(':penghasilan_lain_ibu', $post['penghasilan_lain_ibu']);

          $this->db->BindByName(':total_pendapatan_ortu', $post['total_pendapatan_ortu']);
          $this->db->BindByName(':range_listrik', $post['tagihan_listrik']);
          $this->db->BindByName(':range_pbb', $post['tagihan_pbb']);
          $this->db->BindByName(':range_air', $post['tagihan_listrik']);
          $this->db->BindByName(':range_njop', $post['njop']);
          $this->db->BindByName(':range_penghasilan', $range_penghasilan['ID_RANGE_JUMLAH']);
          $this->db->BindByName(':jumlah_handphone', $post['jumlah_handphone']);
          $this->db->BindByName(':transportasi', $post['transportasi']);

          $this->db->BindByName(':id_c_mhs', $id_c_mhs);
          $result = $this->db->Execute();
         */

        // Mengkategorikan Range Penghasilan
        $range_penghasilan = $this->GetRangebyData($post['total_pendapatan_ortu'], 1);
        // Query Update dari Nambi
        $nama_ayah = str_replace("'", "''", $post['nama_ayah']);
        $nama_ibu = str_replace("'", "''", $post['nama_ibu']);
        $post['alamat_ayah'] = str_replace("'", "''", $post['alamat_ayah']);
        $post['alamat_ibu'] = str_replace("'", "''", $post['alamat_ibu']);

        $query_update_cmo = "
            update calon_mahasiswa_ortu set
                nama_ayah		= upper('{$nama_ayah}'),
                alamat_ayah		= '{$post['alamat_ayah']}',
                id_kota_ayah		= '{$post['id_kota_ayah']}',
                telp_ayah		= '{$post['telp_ayah']}',
                pendidikan_ayah		= '{$post['pendidikan_ayah']}',
                pekerjaan_ayah		= '{$post['pekerjaan_ayah']}',
                instansi_ayah		= '{$post['instansi_ayah']}',
                jabatan_ayah		= '{$post['jabatan_ayah']}',
                masa_kerja_ayah		= '{$post['masa_kerja_ayah']}',
                penghasilan_ayah	= '{$post['penghasilan_ayah']}',                
                nama_ibu		= upper('{$nama_ibu}'),
                alamat_ibu		= '{$post['alamat_ibu']}',
                id_kota_ibu		= '{$post['id_kota_ibu']}',
                telp_ibu		= '{$post['telp_ibu']}',
                pendidikan_ibu		= '{$post['pendidikan_ibu']}',
                pekerjaan_ibu		= '{$post['pekerjaan_ibu']}',
                instansi_ibu		= '{$post['instansi_ibu']}',
                jabatan_ibu		= '{$post['jabatan_ibu']}',
                masa_kerja_ibu		= '{$post['masa_kerja_ibu']}',
                penghasilan_ibu         = '{$post['penghasilan_ibu']}',
                
                skala_pekerjaan_ortu    = '{$post['skala_pekerjaan_ortu']}',
                jumlah_kakak            = '{$post['jumlah_kakak']}',
                jumlah_adik             = '{$post['jumlah_adik']}',
                
                kediaman_ortu       = '{$post['kediaman_ortu']}',
                luas_tanah          = '{$post['luas_tanah']}',
                luas_bangunan       = '{$post['luas_bangunan']}',
                listrik             = '{$post['listrik']}',
                kendaraan_r2        = '{$post['kendaraan_r2']}',
                tahun_kendaraan_r2  = '{$post['tahun_kendaraan_r2']}',
                merek_kendaraan_r2  = '{$post['merek_kendaraan_r2']}',
                kendaraan_r4        = '{$post['kendaraan_r4']}',
                tahun_kendaraan_r4  = '{$post['tahun_kendaraan_r4']}',
                merek_kendaraan_r4  = '{$post['merek_kendaraan_r4']}',
                kekayaan_lain       = '{$post['kekayaan_lain']}',
                info_lain           = '{$post['info_lain']}',
                
                gaji_ayah                   = '{$post['gaji_ayah']}',
                tunjangan_keluarga_ayah     = '{$post['tunjangan_keluarga_ayah']}',
                tunjangan_jabatan_ayah      = '{$post['tunjangan_jabatan_ayah']}',
                tunjangan_sertifikasi_ayah  = '{$post['tunjangan_sertifikasi_ayah']}',
                tunjangan_kehormatan_ayah   = '{$post['tunjangan_kehormatan_ayah']}',
                renumerasi_ayah             = '{$post['renumerasi_ayah']}',
                tunjangan_lain_ayah         = '{$post['tunjangan_lain_ayah']}',
                penghasilan_lain_ayah       = '{$post['penghasilan_lain_ayah']}',
                
                gaji_ibu                   = '{$post['gaji_ibu']}',
                tunjangan_keluarga_ibu     = '{$post['tunjangan_keluarga_ibu']}',
                tunjangan_jabatan_ibu      = '{$post['tunjangan_jabatan_ibu']}',
                tunjangan_sertifikasi_ibu  = '{$post['tunjangan_sertifikasi_ibu']}',
                tunjangan_kehormatan_ibu   = '{$post['tunjangan_kehormatan_ibu']}',
                renumerasi_ibu             = '{$post['renumerasi_ibu']}',
                tunjangan_lain_ibu         = '{$post['tunjangan_lain_ibu']}',
                penghasilan_lain_ibu       = '{$post['penghasilan_lain_ibu']}',
                
                total_pendapatan_ortu       = '{$post['total_pendapatan_ortu']}',
                
                range_air           = '{$post['range_air']}',
                range_pbb           = '{$post['range_pbb']}',
                range_listrik       = '{$post['range_listrik']}',
                range_njop          = '{$post['range_njop']}',
                range_penghasilan   = '{$range_penghasilan['ID_RANGE_JUMLAH']}',
                jumlah_handphone    = '{$post['jumlah_handphone']}',
                transportasi        = '{$post['transportasi']}'
            where id_c_mhs = '{$id_c_mhs}'
            ";

        $result = $this->db->Query($query_update_cmo);

        if (!$result) {
            die("ERROR at " . __LINE__ . ": <br/>" . print_r(error_get_last(), true));
        }

        // Proses Pengelompokan Kelompok Biaya
        //$kelompok_biaya = $this->AutoVerifikasiKelompokBiaya($id_c_mhs);
        //$result = $this->db->Query("UPDATE CALON_MAHASISWA_BARU SET ID_KELOMPOK_BIAYA='{$kelompok_biaya}' WHERE ID_C_MHS='{$id_c_mhs}'");

        if (!$result) {
            die("ERROR at " . __LINE__ . ": <br/>" . print_r(error_get_last(), true));
        }

        // cek tanggal masuk dan lulus D3 / S1
        $column_tgl_masuk_s1 = ($post['tgl_masuk_s1_Day'] == '' or $post['tgl_masuk_s1_Month'] == '' or $post['tgl_masuk_s1_Year'] == '') ? "tgl_masuk_s1 = null," : "tgl_masuk_s1 = to_date(:tgl_masuk_s1, 'dd-mm-yyyy'),";
        $column_tgl_lulus_s1 = ($post['tgl_lulus_s1_Day'] == '' or $post['tgl_lulus_s1_Month'] == '' or $post['tgl_lulus_s1_Year'] == '') ? "tgl_lulus_s1 = null," : "tgl_lulus_s1 = to_date(:tgl_lulus_s1, 'dd-mm-yyyy'),";

        // cek tanggal masuk dan lulus S1 / Pr
        $column_tgl_masuk_s2 = ($post['tgl_masuk_s2_Day'] == '' or $post['tgl_masuk_s2_Month'] == '' or $post['tgl_masuk_s2_Year'] == '') ? "tgl_masuk_s2 = null," : "tgl_masuk_s2 = to_date(:tgl_masuk_s2, 'dd-mm-yyyy'),";
        $column_tgl_lulus_s2 = ($post['tgl_lulus_s2_Day'] == '' or $post['tgl_lulus_s2_Month'] == '' or $post['tgl_lulus_s2_Year'] == '') ? "tgl_lulus_s2 = null," : "tgl_lulus_s2 = to_date(:tgl_lulus_s2, 'dd-mm-yyyy'),";

        // tabel calon_mahasiswa_pasca
        $this->db->Parse("
            update calon_mahasiswa_pasca set
                pekerjaan       = :pekerjaan,
                asal_instansi   = :asal_instansi,
                alamat_instansi = :alamat_instansi,
                telp_instansi   = :telp_instansi,
                nrp             = :nrp,
                karpeg          = :karpeg,
                pangkat         = :pangkat,
                
                ptn_s1          = :ptn_s1,
                status_ptn_s1   = :status_ptn_s1,
                prodi_s1        = :prodi_s1,
                {$column_tgl_masuk_s1}
                {$column_tgl_lulus_s1}
                lama_studi_s1   = :lama_studi_s1,
                ip_s1           = :ip_s1,
				nim_lama_s1		= :nim_lama_s1,
                
                ptn_s2          = :ptn_s2,
                status_ptn_s2   = :status_ptn_s2,
                prodi_s2        = :prodi_s2,
                {$column_tgl_masuk_s2}
                {$column_tgl_lulus_s2}
                lama_studi_s2   = :lama_studi_s2,
                ip_s2           = :ip_s2,
				nim_lama_s2		= :nim_lama_s2,
                
                jumlah_karya_ilmiah = :jumlah_karya_ilmiah,
                id_prodi_minat      = :id_prodi_minat
            where id_c_mhs = :id_c_mhs");
        $this->db->BindByName(':pekerjaan', $post['pekerjaan']);
        $this->db->BindByName(':asal_instansi', $post['asal_instansi']);
        $this->db->BindByName(':alamat_instansi', $post['alamat_instansi']);
        $this->db->BindByName(':telp_instansi', $post['telp_instansi']);
        $this->db->BindByName(':nrp', $post['nrp']);
        $this->db->BindByName(':karpeg', $post['karpeg']);
        $this->db->BindByName(':pangkat', $post['pangkat']);

        $this->db->BindByName(':ptn_s1', $post['ptn_s1']);
        $this->db->BindByName(':status_ptn_s1', $post['status_ptn_s1']);
        $this->db->BindByName(':prodi_s1', $post['prodi_s1']);
        $this->db->BindByName(':tgl_masuk_s1', $tgl_masuk_s1 = str_pad($post['tgl_masuk_s1_Day'], 2, '0', STR_PAD_LEFT) . "-" . $post['tgl_masuk_s1_Month'] . "-" . $post['tgl_masuk_s1_Year']);
        $this->db->BindByName(':tgl_lulus_s1', $tgl_lulus_s1 = str_pad($post['tgl_lulus_s1_Day'], 2, '0', STR_PAD_LEFT) . "-" . $post['tgl_lulus_s1_Month'] . "-" . $post['tgl_lulus_s1_Year']);
        $this->db->BindByName(':lama_studi_s1', $post['lama_studi_s1']);
        $this->db->BindByName(':ip_s1', $post['ip_s1']);
        $this->db->BindByName(':nim_lama_s1', $post['nim_lama_s1']);

        $this->db->BindByName(':ptn_s2', $post['ptn_s2']);
        $this->db->BindByName(':status_ptn_s2', $post['status_ptn_s2']);
        $this->db->BindByName(':prodi_s2', $post['prodi_s2']);
        $this->db->BindByName(':tgl_masuk_s2', $tgl_masuk_s2 = str_pad($post['tgl_masuk_s2_Day'], 2, '0', STR_PAD_LEFT) . "-" . $post['tgl_masuk_s2_Month'] . "-" . $post['tgl_masuk_s2_Year']);
        $this->db->BindByName(':tgl_lulus_s2', $tgl_lulus_s2 = str_pad($post['tgl_lulus_s2_Day'], 2, '0', STR_PAD_LEFT) . "-" . $post['tgl_lulus_s2_Month'] . "-" . $post['tgl_lulus_s2_Year']);
        $this->db->BindByName(':lama_studi_s2', $post['lama_studi_s2']);
        $this->db->BindByName(':ip_s2', $post['ip_s2']);
        $this->db->BindByName(':nim_lama_s2', $post['nim_lama_s2']);

        $this->db->BindByName(':jumlah_karya_ilmiah', $post['jumlah_karya_ilmiah']);
        $this->db->BindByName(':id_prodi_minat', $post['id_prodi_minat']);
        $this->db->BindByName(':id_c_mhs', $id_c_mhs);
        $result = $this->db->Execute();

        if (!$result)
            die("ERROR at " . __LINE__ . ": <br/>" . print_r(error_get_last(), true));

        //update jadwal verifikasi pendidikan
        $result = $this->db->Query("update calon_mahasiswa_data set id_jadwal_verifikasi_pend = (select id_jadwal_verifikasi_pend from (
						select j.id_jadwal_verifikasi_pend, j.TGL_REGISTRASI, TO_CHAR(j.JAM_AWAL, 'HH24:MI') AS JAM_AWAL, 
						TO_CHAR(j.JAM_AKHIR, 'HH24:MI') AS JAM_AKHIR, 
						row_number() OVER (order by j.TGL_REGISTRASI, j.JAM_AWAL, j.JAM_AKHIR asc) as peringkat
													from jadwal_verifikasi_pendidikan j
													join calon_mahasiswa_baru cmb on cmb.id_penerimaan = j.id_penerimaan
													join calon_mahasiswa_data cmd on cmd.id_c_mhs = cmb.id_c_mhs
													where cmb.id_c_mhs = {$id_c_mhs} 
													and (select count(id_c_mhs) from calon_mahasiswa_data cmd 
															where cmd.id_jadwal_verifikasi_pend = j.id_jadwal_verifikasi_pend) < j.kuota)
						where peringkat = 1) 
						where id_c_mhs = {$id_c_mhs}");

        if (!$result)
            die("ERROR at " . __LINE__ . ": <br/>" . print_r(error_get_last(), true));

        return $this->db->Commit();
    }

    function UpdateIsianSp3($id_c_mhs, &$post) {
        $this->db->BeginTransaction();

        if (!empty($post['sp3_1']))
            $this->db->Query("update calon_mahasiswa_baru set sp3_1 = {$post['sp3_1']} where id_c_mhs = {$id_c_mhs}");
        if (!empty($post['sp3_2']))
            $this->db->Query("update calon_mahasiswa_baru set sp3_2 = {$post['sp3_2']} where id_c_mhs = {$id_c_mhs}");
        if (!empty($post['sp3_3']))
            $this->db->Query("update calon_mahasiswa_baru set sp3_3 = {$post['sp3_3']} where id_c_mhs = {$id_c_mhs}");
        if (!empty($post['sp3_4']))
            $this->db->Query("update calon_mahasiswa_baru set sp3_4 = {$post['sp3_4']} where id_c_mhs = {$id_c_mhs}");

        $this->db->Query("update calon_mahasiswa_baru set id_kelompok_biaya = {$post['id_kelompok_biaya']} where id_c_mhs = {$id_c_mhs}");

        $this->db->Query("update calon_mahasiswa_data set id_jadwal_verifikasi_keuangan = {$post['id_jadwal_verifikasi_keuangan']}
							where id_c_mhs = {$id_c_mhs}");

        $this->db->Query("update calon_mahasiswa_data set id_jadwal_verifikasi_pend = (
							select id_jadwal_verifikasi_pend from (
							select j.id_jadwal_verifikasi_pend, row_number() OVER (order by j.TGL_REGISTRASI, j.JAM_AWAL, j.JAM_AKHIR asc) as peringkat
            				from jadwal_verifikasi_pendidikan j
							join calon_mahasiswa_baru cmb on cmb.id_penerimaan = j.id_penerimaan
							join calon_mahasiswa_data cmd on cmd.id_c_mhs = cmb.id_c_mhs
							join jadwal_verifikasi_keuangan jvk on jvk.id_jadwal = cmd.id_jadwal_verifikasi_keuangan
            				where cmb.id_c_mhs = {$id_c_mhs} and j.tgl_registrasi > jvk.tgl_jadwal
							and (select count(id_c_mhs) from calon_mahasiswa_data cmd 
									where cmd.id_jadwal_verifikasi_pend = j.id_jadwal_verifikasi_pend) < j.kuota
							) 
							where peringkat = 1)
							
						where id_c_mhs = {$id_c_mhs}");

        return $this->db->Commit();
    }

    function UpdateTglRegmaba($id_c_mhs) {

        return $this->db->Query("update calon_mahasiswa_baru set tgl_regmaba = current_timestamp where id_c_mhs = {$id_c_mhs} and tgl_regmaba is null");
    }

    function GetSp3ByPendapatanOrtu($id_c_mhs, $total_pendapatan_ortu) {
        $rows = $this->db->QueryToArray("
            select s.id_semester, id_program_studi, kb.id_kelompok_biaya, k.nm_kelompok_biaya,
                nvl((select nvl(db.besar_biaya,0) from biaya_kuliah bk
                 join detail_biaya db on db.id_biaya_kuliah = bk.id_biaya_kuliah
                 where db.id_biaya = 81 and bk.id_semester = s.id_semester and bk.id_program_studi = cmb.id_program_studi and bk.id_kelompok_biaya = kb.id_kelompok_biaya),0) as sp3
            from calon_mahasiswa_baru cmb
            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
            join semester s on s.tipe_semester = 'REG' and s.thn_akademik_semester = p.tahun and substr(p.semester, 1, 2) = substr(s.nm_semester, 1, 2)
            join kelompok_biaya_ortu kb on kb.id_semester = s.id_semester
            join kelompok_biaya k on k.id_kelompok_biaya = kb.id_kelompok_biaya
            where cmb.id_c_mhs = {$id_c_mhs} and (round({$total_pendapatan_ortu}) between batas_bawah and batas_atas)");
        return $rows[0];
    }

    function GetListProgramStudi($id_c_mhs, $terpilih = '') {
        $rows = $this->db->QueryToArray("
            select cmb.id_penerimaan, v.kode_jurusan, p.id_jenjang from calon_mahasiswa_baru cmb
            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
            join voucher v on v.kode_voucher = cmb.kode_voucher
            where id_c_mhs = {$id_c_mhs}");
        $cmb = $rows[0];

        if ($cmb['ID_JENJANG'] == 1 or $cmb['ID_JENJANG'] == 5) {
            if ($cmb['KODE_JURUSAN'] == '03')
                $jurusan_sekolah = '';
            else
                $jurusan_sekolah = "and jurusan_sekolah = {$cmb['KODE_JURUSAN']}";
        }
        else {
            $jurusan_sekolah = '';
        }

        if ($terpilih != '')
            $terpilih = " and pp.id_program_studi not in ({$terpilih})";

        return array_merge(
                array(array('ID_PROGRAM_STUDI' => '', 'NM_PROGRAM_STUDI' => '')), $this->db->QueryToArray("
                select ps.id_program_studi, j.nm_jenjang||' '||ps.nm_program_studi as nm_program_studi from penerimaan_prodi pp
                join program_studi ps on ps.id_program_studi = pp.id_program_studi
                join jenjang j on j.id_jenjang = ps.id_jenjang
                where pp.is_aktif = 1 and pp.id_penerimaan = {$cmb['ID_PENERIMAAN']} {$jurusan_sekolah} {$terpilih}
                order by ps.nm_program_studi asc"));
    }

    function GetListProgramStudiMinat($id_c_mhs, $id_program_studi) {
        return $this->db->QueryToArray("
            select pm.* from calon_mahasiswa_baru cmb
            join penerimaan_prodi pp on pp.id_penerimaan = cmb.id_penerimaan
            join penerimaan_prodi_minat ppm on ppm.id_penerimaan = cmb.id_penerimaan
            join prodi_minat pm on (pm.id_prodi_minat = ppm.id_prodi_minat and pm.id_program_studi = pp.id_program_studi)
            where pp.is_aktif = 1 and ppm.is_aktif = 1 and pp.id_program_studi = {$id_program_studi} and cmb.id_c_mhs = {$id_c_mhs}
            order by nm_prodi_minat");
    }

    function GetListProdiKelas($id_program_studi) {
        return $this->db->QueryToArray("select * from prodi_kelas where id_program_studi = {$id_program_studi}");
    }

    function GetListKota() {
        return array_merge(
                array(array('ID_KOTA' => '', 'NM_KOTA' => '')), $this->db->QueryToArray("
            select k.id_kota, k.nm_kota||' ('||k.tipe_dati2||'), '||p.nm_provinsi as nm_kota
            from kota k
            join provinsi p on p.id_provinsi = k.id_provinsi
            join negara n on n.id_negara = p.id_negara
            order by k.nm_kota, p.nm_provinsi, n.nm_negara"));
    }

    function GetListAgama() {
        return array_merge(
                array(array('ID_AGAMA' => '', 'NM_AGAMA' => '')), $this->db->QueryToArray("select * from agama order by id_agama")
        );
    }

    function GetListJenisKelamin() {
        return array(
            array('JENIS_KELAMIN' => '', 'NM_JENIS_KELAMIN' => ''),
            array('JENIS_KELAMIN' => 1, 'NM_JENIS_KELAMIN' => 'Laki-Laki'),
            array('JENIS_KELAMIN' => 2, 'NM_JENIS_KELAMIN' => 'Perempuan'),
        );
    }

    function GetListKewarganegaraan() {
        return array_merge(
                array(array('ID_KEWARGANEGARAAN' => '', 'NM_KEWARGANEGARAAN' => '')), $this->db->QueryToArray("select * from kewarganegaraan order by id_kewarganegaraan")
        );
    }

    function GetListSumberBiaya() {
        return array_merge(
                array(array('ID_SUMBER_BIAYA' => '', 'NM_SUMBER_BIAYA' => '')), $this->db->QueryToArray("select * from sumber_biaya order by id_sumber_biaya")
        );
    }

    function GetListJurusanSekolah() {
        return array_merge(
                array(array('ID_JURUSAN_SEKOLAH' => '', 'NM_JURUSAN_SEKOLAH' => '')), $this->db->QueryToArray("select * from jurusan_sekolah_cmhs order by id_jurusan_sekolah")
        );
    }

    function GetListPendidikanOrtu() {
        return array_merge(
                array(array('ID_PENDIDIKAN_ORTU' => '', 'NM_PENDIDIKAN_ORTU' => '')), $this->db->QueryToArray("select * from pendidikan_ortu order by id_pendidikan_ortu")
        );
    }

    function GetListPekerjaan() {
        return array_merge(
                array(array('ID_PEKERJAAN' => '', 'NM_PEKERJAAN' => '')), $this->db->QueryToArray("select * from pekerjaan order by id_pekerjaan")
        );
    }

    function GetListPenghasilanOrtu() {
        return array(
            array('PENGHASILAN_ORTU' => '', 'NM_PENGHASILAN_ORTU' => ''),
            array('PENGHASILAN_ORTU' => '1', 'NM_PENGHASILAN_ORTU' => 'Lebih besar dari 7.500.000'),
            array('PENGHASILAN_ORTU' => '2', 'NM_PENGHASILAN_ORTU' => '2.500.000 sampai 7.500.000'),
            array('PENGHASILAN_ORTU' => '3', 'NM_PENGHASILAN_ORTU' => '1.350.000 sampai 2.500.000'),
            array('PENGHASILAN_ORTU' => '4', 'NM_PENGHASILAN_ORTU' => 'Kurang dari 1.350.000'),
        );
    }

    function GetListSkalaPekerjaan() {
        return array(
            array('SKALA_PEKERJAAN' => '', 'NM_SKALA_PEKERJAAN' => ''),
            array('SKALA_PEKERJAAN' => '1', 'NM_SKALA_PEKERJAAN' => 'Besar'),
            array('SKALA_PEKERJAAN' => '2', 'NM_SKALA_PEKERJAAN' => 'Menengah'),
            array('SKALA_PEKERJAAN' => '3', 'NM_SKALA_PEKERJAAN' => 'Kecil'),
            array('SKALA_PEKERJAAN' => '4', 'NM_SKALA_PEKERJAAN' => 'Mikro'),
        );
    }

    function GetListKediamanOrtu() {
        return array(
            array('KEDIAMAN_ORTU' => '', 'NM_KEDIAMAN_ORTU' => ''),
            array('KEDIAMAN_ORTU' => '1', 'NM_KEDIAMAN_ORTU' => 'Mewah / Besar'),
            array('KEDIAMAN_ORTU' => '2', 'NM_KEDIAMAN_ORTU' => 'Sedang'),
            array('KEDIAMAN_ORTU' => '3', 'NM_KEDIAMAN_ORTU' => 'Rumah Sederhana'),
            array('KEDIAMAN_ORTU' => '4', 'NM_KEDIAMAN_ORTU' => 'Rumah Sangat Sederhana'),
        );
    }

    function GetListLuasTanah() {
        return array(
            array('LUAS_TANAH' => '', 'NM_LUAS_TANAH' => ''),
            array('LUAS_TANAH' => '1', 'NM_LUAS_TANAH' => 'Lebih luas dari 200m' . chr(178)),
            array('LUAS_TANAH' => '2', 'NM_LUAS_TANAH' => '100m' . chr(178) . ' sampai 200m' . chr(178)),
            array('LUAS_TANAH' => '3', 'NM_LUAS_TANAH' => '45m' . chr(178) . ' sampai 100m' . chr(178)),
            array('LUAS_TANAH' => '4', 'NM_LUAS_TANAH' => 'Kurang dari 45m' . chr(178)),
        );
    }

    function GetListLuasBangunan() {
        return array(
            array('LUAS_BANGUNAN' => '', 'NM_LUAS_BANGUNAN' => ''),
            array('LUAS_BANGUNAN' => '1', 'NM_LUAS_BANGUNAN' => 'Lebih luas dari 100m' . chr(178)),
            array('LUAS_BANGUNAN' => '2', 'NM_LUAS_BANGUNAN' => '56m' . chr(178) . ' sampai 100m' . chr(178)),
            array('LUAS_BANGUNAN' => '3', 'NM_LUAS_BANGUNAN' => '27m' . chr(178) . ' sampai 56m' . chr(178)),
            array('LUAS_BANGUNAN' => '4', 'NM_LUAS_BANGUNAN' => 'Kurang dari 27m' . chr(178)),
        );
    }

    function GetListNJOP() {
        return array(
            array('NJOP' => '', 'NM_NJOP' => ''),
            array('NJOP' => '1', 'NM_NJOP' => 'Lebih besar dari 300jt'),
            array('NJOP' => '2', 'NM_NJOP' => '100jt sampai 300jt'),
            array('NJOP' => '3', 'NM_NJOP' => '50jt sampai 100jt'),
            array('NJOP' => '4', 'NM_NJOP' => 'Kurang dari 50jt'),
        );
    }

    function GetListListrik() {
        return array(
            array('LISTRIK' => '', 'NM_LISTRIK' => ''),
            array('LISTRIK' => '1', 'NM_LISTRIK' => '450 VA'),
            array('LISTRIK' => '2', 'NM_LISTRIK' => '900 VA'),
            array('LISTRIK' => '3', 'NM_LISTRIK' => '1300 VA'),
            array('LISTRIK' => '4', 'NM_LISTRIK' => 'Lebih besar dari 2200 VA'),
        );
    }

    function GetListKendaraanR4() {
        return array(
            array('KENDARAAN' => '', 'NM_KENDARAAN' => ''),
            array('KENDARAAN' => '1', 'NM_KENDARAAN' => 'Lebih dari 1'),
            array('KENDARAAN' => '2', 'NM_KENDARAAN' => '1'),
            array('KENDARAAN' => '3', 'NM_KENDARAAN' => 'Tidak Punya'),
        );
    }

    function GetListKendaraanR2() {
        return array(
            array('KENDARAAN' => '', 'NM_KENDARAAN' => ''),
            array('KENDARAAN' => '1', 'NM_KENDARAAN' => 'Lebih dari 2'),
            array('KENDARAAN' => '2', 'NM_KENDARAAN' => '2'),
            array('KENDARAAN' => '3', 'NM_KENDARAAN' => '1'),
            array('KENDARAAN' => '4', 'NM_KENDARAAN' => 'Tidak Punya'),
        );
    }

    function GetListStatusPTN() {
        return array(
            array('STATUS_PTN' => '', 'NM_STATUS_PTN' => ''),
            array('STATUS_PTN' => '1', 'NM_STATUS_PTN' => 'Negeri'),
            array('STATUS_PTN' => '2', 'NM_STATUS_PTN' => 'Swasta'),
        );
    }

    function GetListDisabilitas() {
        return $this->db->QueryToArray("select * from disabilitas order by id_disabilitas");
    }

    static function GetValueFromArray($value, $array, $column_find, $column_result) {
        foreach ($array as $row)
            if ($row[$column_find] == $value)
                return str_replace("&sup2;", "²", $row[$column_result]);
        return null;
    }

    function GetListKotaSearch($term) {
        $rows = $this->db->QueryToArray("
            select k.id_kota as id, k.nm_kota||', '|| p.nm_provinsi||' ('||  k.tipe_dati2||')' as value
            from kota k
            join provinsi p on p.id_provinsi = k.id_provinsi
            join negara n on n.id_negara = p.id_negara
            where upper(k.nm_kota) like '%{$term}%'
            order by k.nm_kota, p.nm_provinsi, n.nm_negara");

        foreach ($rows as &$row) {
            $r['id'] = $row['ID'];
            $r['value'] = $row['VALUE'];
            $return[] = $r;
        }

        return $return;
    }

    function GetListSekolahSearch($term) {
        $rows = $this->db->QueryToArray("
            select s.id_sekolah as id, s.nm_sekolah||', '||k.nm_kota as value
            from sekolah s
            join kota k on k.id_kota = s.id_kota
            where upper(s.nm_sekolah)||upper(k.nm_kota) like '%{$term}%'
            order by nm_sekolah");

        foreach ($rows as &$row) {
            $r['id'] = $row['ID'];
            $r['value'] = $row['VALUE'];
            $return[] = $r;
        }

        return $return;
    }

    /**
     * Login untuk camaba SNMPTN / PBSB (tidak punya kode voucher)
     * @param string $no_ujian
     * @param date $tgl_lahir
     * @return integer 
     */
    function LoginRegmabaSNMPTN($no_ujian, $tgl_lahir) {
        // Smoothing no ujian
        $no_ujian = str_replace(" ", "", $no_ujian);
        $no_ujian = str_replace("-", "", $no_ujian);

        $rows = $this->db->QueryToArray("
            select id_c_mhs, tgl_verifikasi_pendidikan, tgl_verifikasi_keuangan, to_char(tgl_awal_regmaba, 'YYYY-MM-DD HH24:MI') as tgl_awal_regmaba
			from calon_mahasiswa_baru cmb
            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
            where
                p.id_jalur in (1, 20) and
                replace(no_ujian,'-','') = '{$no_ujian}' and to_char(tgl_lahir, 'YYYY-MM-DD') = '{$tgl_lahir}' and tgl_diterima is not null");

        if ($_SERVER['REMOTE_ADDR'] == '39.194.40.62') {
            /**
              echo "select id_c_mhs, tgl_verifikasi_pendidikan, tgl_verifikasi_keuangan, to_char(tgl_awal_regmaba, 'YYYY-MM-DD HH24:MI') as tgl_awal_regmaba
              from calon_mahasiswa_baru cmb
              join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
              where
              p.id_jalur in (1, 20) and
              no_ujian = '{$no_ujian}' and to_char(tgl_lahir,'YYYY-MM-DD') = '{$tgl_lahir}' and tgl_diterima is not null";
              exit();
             * 
             */
        }

        if (count($rows) > 0) {
            return $rows;
        }

        return '';
    }

    /**
     * Login untuk camaba mandiri (mempunyai kode voucher)
     * @param type $no_ujian
     * @param type $kode_voucher
     * @return string 
     */
    function LoginRegmabaMandiri($no_ujian, $kode_voucher) {
        $this->db->Parse("
            select id_c_mhs, tgl_verifikasi_pendidikan, to_char(tgl_awal_regmaba, 'YYYY-MM-DD HH24:MI') as tgl_awal_regmaba, id_jalur 
            from calon_mahasiswa_baru cmb
            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
            where
                p.id_jalur not in (1)
                and no_ujian = :no_ujian and kode_voucher = :kode_voucher and tgl_diterima is not null");
        $this->db->BindByName(':no_ujian', $no_ujian);
        $this->db->BindByName(':kode_voucher', $kode_voucher);
        $this->db->Execute();

        $row = $this->db->FetchAssoc();

        if (count($row) > 0) {
            return $row;
        }

        return '';
    }

    function LogRegmaba($remote_addr, $id_c_mhs) {
        return $this->db->Query("insert into log_regmaba (remote_addr, id_c_mhs) values ('{$remote_addr}', {$id_c_mhs})");
    }

    function UpdateFile($id_c_mhs, $file, $status) {
        return $this->db->Query("update calon_mahasiswa_file set {$file} = {$status} where id_c_mhs = {$id_c_mhs}");
    }

    function GetListJadwalVerifikasiKeuangan($id_penerimaan) {
        return $this->db->QueryToArray("
            select j.id_jadwal, to_char(tgl_jadwal,'yyyy-mm-dd') tgl_jadwal, kuota,
                (select count(id_c_mhs) from calon_mahasiswa_data cmd where cmd.id_jadwal_verifikasi_keuangan = j.id_jadwal) as isi
            from jadwal_verifikasi_keuangan j
            where j.id_penerimaan = {$id_penerimaan}
            order by tgl_jadwal");
    }

    function GetListJadwalVerifikasiPendidikan($id_penerimaan) {

        return $this->db->Query("
            select j.id_jadwal_verifikasi_pend, to_char(tgl_registrasi,'yyyy-mm-dd') tgl_jadwal, kuota,
				TO_CHAR(JAM_AWAL, 'HH24:MI') AS JAM_AWAL, 
				TO_CHAR(JAM_AKHIR, 'HH24:MI') AS JAM_AKHIR,
                (select count(id_c_mhs) from calon_mahasiswa_data cmd where cmd.id_jadwal_verifikasi_pend = j.id_jadwal_verifikasi_pend) as isi
            from jadwal_verifikasi_pendidikan j
            where j.id_penerimaan = {$id_penerimaan} 
			and (select count(id_c_mhs) from calon_mahasiswa_data cmd where cmd.id_jadwal_verifikasi_pend = j.id_jadwal_verifikasi_pend) < kuota
            order by tgl_registrasi");
    }

    function PrintPdf($id_c_mhs, FPDF &$pdf, $is_regmaba = false) {
        $cmb = $this->GetData($id_c_mhs, $is_regmaba);

        $jalur = array("1" => "SARJANA", "3" => "SARJANA", "4" => "ALIH JENIS", "5" => "DIPLOMA", "6" => "PASCASARJANA", "20" => "SARJANA", "23" => "PROFESI", "24" => "SPESIALIS", "27" => "SARJANA", "35" => "PASCASARJANA");
        $jurusan = array("01" => "IPA", "02" => "IPS", "03" => "IPC");

        if ($cmb['ID_JENJANG'] == 2 or $cmb['ID_JENJANG'] == 3 or $cmb['ID_JENJANG'] == 9 or $cmb['ID_JENJANG'] == 10) {
            if ($cmb['ID_JENJANG'] == 2)
                $jalur = array('6' => 'MAGISTER', '35' => 'MAGISTER');
            if ($cmb['ID_JENJANG'] == 3)
                $jalur = array('6' => 'DOKTOR', '35' => 'DOKTOR');
            if ($cmb['ID_JENJANG'] == 9)
                $jalur = array('23' => 'PROFESI');
            if ($cmb['ID_JENJANG'] == 10)
                $jalur = array('24' => 'SPESIALIS');
            $jurusan = array("01" => "Reguler");
        }

        // Cleansing get data
        $jalur_penerimaan = $jalur[$cmb['ID_JALUR']];
        $tahun_penerimaan = $cmb['TAHUN_PENERIMAAN'] . " / " . ($cmb['TAHUN_PENERIMAAN'] + 1);
        $kode_jurusan = $jurusan[$cmb['KODE_JURUSAN']];

        $kota_lahir = explode(",", $cmb['NM_KOTA_LAHIR']);
        $tanggal_lahir = strftime('%d %B %Y', strtotime($cmb['TGL_LAHIR']));
        $tempat_tanggal_lahir = "{$kota_lahir[0]}, {$tanggal_lahir}";

        // Range
        $range_listrik = $this->GetTextRange($cmb['RANGE_LISTRIK']);
        $range_njop = $this->GetTextRange($cmb['RANGE_NJOP']);
        $range_air = $this->GetTextRange($cmb['RANGE_AIR']);
        $range_pbb = $this->GetTextRange($cmb['RANGE_PBB']);
        $range_penghasilan = $this->GetTextRange($cmb['RANGE_PENGHASILAN']);

        // Document Properties
        $pdf->SetCreator("Cyber Campus Universitas Airlangga");
        $pdf->SetAuthor("PPMB Universitas Airlangga");
        $pdf->SetTitle("Formulir Registrasi Program Diploma");

        // Import Font
        $pdf->AddFont('Calibri', '', 'calibri.php');
        $pdf->AddFont('Calibri', 'B', 'calibrib.php');

        // create page
        $pdf->AddPage('P', 'A4');

        // debugging
        $border = 0;

        // logo unair
        $pdf->Image("/var/www/html/modul/registrasi/img/logo-unair.png", null, 15, 30, 30);

        // kotak foto
        $pdf->Rect(169, 10, 30, 40);
        $pdf->SetXY(169, 25);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(30, 4, "4 x 6", $border, false, 'C');

        // title
        $pdf->SetY(20);
        $pdf->SetFont('Calibri', 'B', 18);
        $pdf->Cell(0, 7, "FORMULIR REGISTRASI MAHASISWA BARU", $border, true, 'C');
        $pdf->Cell(0, 7, "PROGRAM {$jalur_penerimaan}", $border, true, 'C');
        $pdf->Cell(0, 7, "TAHUN AKADEMIK {$tahun_penerimaan}", $border, true, 'C');

        // Horizontal line
        $pdf->Line(10, 55, 200, 55);

        // Kalimat pertama
        $pdf->SetXY(10, 58);
        $pdf->SetFont('Calibri', '', 12);
        $pdf->Cell(0, 4, "YANG BERTANDA TANGAN DI BAWAH INI MOHON DIDAFTAR SEBAGAI MAHASISWA PADA : ", $border, true, 'L');
        $pdf->Ln();

        if ($is_regmaba == false) {  // cetak untuk pendaftaran
            // Program Studi
            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Cell(0, 6, "PROGRAM STUDI PILIHAN", true, true, 'C', true);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(70, 6, "Kode Voucher", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['KODE_VOUCHER']}", true, true, 'L');
            $pdf->Cell(70, 6, "Kode Jurusan", true, false, 'L');
            $pdf->Cell(0, 6, "{$kode_jurusan}", true, true, 'L');
            $pdf->Cell(70, 6, "Prodi Pilihan 1", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NM_PILIHAN_1']}", true, true, 'L');


            if ($cmb['ID_PRODI_MINAT'] != '') {
                $pdf->Cell(70, 6, "Minat Studi", true, false, 'L');
                $pdf->Cell(0, 6, "{$cmb['NM_PRODI_MINAT']}", true, true, 'L');
            }

            if ($cmb['ID_PILIHAN_2'] != '') {
                $pdf->Cell(70, 6, "Prodi Pilihan 2", true, false, 'L');
                $pdf->Cell(0, 6, "{$cmb['NM_PILIHAN_2']}", true, true, 'L');
            }

            if ($cmb['ID_PILIHAN_3'] != '') {
                $pdf->Cell(70, 6, "Prodi Pilihan 3", true, false, 'L');
                $pdf->Cell(0, 6, "{$cmb['NM_PILIHAN_3']}", true, true, 'L');
            }

            if ($cmb['ID_PILIHAN_3'] != '') {
                $pdf->Cell(70, 6, "Prodi Pilihan 4", true, false, 'L');
                $pdf->Cell(0, 6, "{$cmb['NM_PILIHAN_4']}", true, true, 'L');
            }

            if ($cmb['ID_KELAS_PILIHAN'] != '') {
                $pdf->Cell(70, 6, "Kelas Pilihan", true, false, 'L');
                $pdf->Cell(0, 6, "{$cmb['NM_KELAS_PILIHAN']}", true, true, 'L');
            }
        }

        if ($is_regmaba == true) {
            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Cell(0, 6, "PROGRAM STUDI", true, true, 'C', true);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(70, 6, "Fakultas", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NM_FAKULTAS']}", true, true, 'L');
            $pdf->Cell(70, 6, "Program Studi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NM_PROGRAM_STUDI']}", true, true, 'L');

            if ($cmb['ID_PRODI_MINAT'] != '') {
                $pdf->Cell(70, 6, "Minat Studi", true, false, 'L');
                $pdf->Cell(0, 6, "{$cmb['NM_PRODI_MINAT']}", true, true, 'L');
            }

            if ($cmb['ID_KELAS_PILIHAN'] != '') {
                $pdf->Cell(70, 6, "Kelas Pilihan", true, false, 'L');
                $pdf->Cell(0, 6, "{$cmb['NM_KELAS_PILIHAN']}", true, true, 'L');
            }
        }

        $pdf->Ln();

        $pdf->SetFont('Calibri', 'B', 12);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Cell(0, 6, "IDENTITAS DIRI", true, true, 'C', true);
        $pdf->SetFont('Calibri', '', 12);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(70, 6, "Nama Lengkap", true, false, 'L');
        $pdf->Cell(0, 6, "{$cmb['NM_C_MHS']}", true, true, 'L');

        if ($cmb['GELAR'] != '') {
            $pdf->Cell(70, 6, "Gelar", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['GELAR']}", true, true, 'L');
        }

        $pdf->Cell(70, 6, "Tempat dan Tanggal Lahir", true, false, 'L');
        $pdf->Cell(0, 6, "{$tempat_tanggal_lahir}", true, true, 'L');
        $pdf->Cell(70, 6, "Alamat", true, false, 'L');
        $pdf->Cell(0, 6, "{$cmb['ALAMAT']}", true, true, 'L');
        $pdf->Cell(70, 6, "Kota", true, false, 'L');
        $pdf->Cell(0, 6, "{$cmb['NM_KOTA']}", true, true, 'L');
        $pdf->Cell(70, 6, "No Telp", true, false, 'L');
        $pdf->Cell(0, 6, "{$cmb['TELP']}", true, true, 'L');

        $array_jalur_umum = array(1, 2, 3, 20, 27);

        if (in_array($cmb['ID_JALUR'], $array_jalur_umum)) {
            $pdf->Cell(70, 6, "No Telp Utama", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['TELP_UTAMA']}", true, true, 'L');
            $pdf->Cell(70, 6, "PIN Blackberry", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PIN_BB']}", true, true, 'L');
        }

        $pdf->Cell(70, 6, "Jenis Kelamin", true, false, 'L');
        $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['JENIS_KELAMIN'], $this->GetListJenisKelamin(), 'JENIS_KELAMIN', 'NM_JENIS_KELAMIN'), true, true, 'L');
        $pdf->Cell(70, 6, "Kewarganegaraan", true, false, 'L');
        $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['KEWARGANEGARAAN'], $this->GetListKewarganegaraan(), 'ID_KEWARGANEGARAAN', 'NM_KEWARGANEGARAAN'), true, true, 'L');
        $pdf->Cell(70, 6, "Agama", true, false, 'L');
        $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['ID_AGAMA'], $this->GetListAgama(), 'ID_AGAMA', 'NM_AGAMA'), true, true, 'L');
        $pdf->Cell(70, 6, "Sumber Biaya", true, false, 'L');
        $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['SUMBER_BIAYA'], $this->GetListSumberBiaya(), 'ID_SUMBER_BIAYA', 'NM_SUMBER_BIAYA'), true, true, 'L');
        $pdf->Cell(70, 6, "Email", true, false, 'L');
        $pdf->Cell(0, 6, "{$cmb['EMAIL']}", true, true, 'L');

        $pdf->Ln();

        if ($cmb['ID_JALUR'] == 1 or $cmb['ID_JALUR'] == 3 or $cmb['ID_JALUR'] == 5 or $cmb['ID_JALUR'] == 20 or $cmb['ID_JALUR'] == 27) { // SNMPTN / MANDIRI / DIPLOMA / DEPAG
            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Cell(0, 6, "DATA PENDIDIKAN", true, true, 'C', true);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(70, 6, "NISN", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NISN']}", true, true, 'L');
            $pdf->Cell(70, 6, "Asal SMTA / MA", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NM_SEKOLAH_ASAL']}", true, true, 'L');
            $pdf->Cell(70, 6, "Jurusan SMTA / MA", true, false, 'L');
            $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['JURUSAN_SEKOLAH'], $this->GetListJurusanSekolah(), 'ID_JURUSAN_SEKOLAH', 'NM_JURUSAN_SEKOLAH'), true, true, 'L');
            $pdf->Cell(70, 6, "No Ijazah", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NO_IJAZAH']}", true, true, 'L');
            $pdf->Cell(70, 6, "Tanggal Ijazah", true, false, 'L');
            $pdf->Cell(0, 6, $cmb['TGL_IJAZAH'] != '' ? strftime('%d %B %Y', strtotime($cmb['TGL_IJAZAH'])) : '', true, true, 'L');
            $pdf->Cell(70, 6, "Tahun Ijazah", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['TAHUN_LULUS']}", true, true, 'L');
            $pdf->Cell(70, 6, "Jumlah Pelajaran Ijazah", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['JUMLAH_PELAJARAN_IJAZAH']}", true, true, 'L');
            $pdf->Cell(70, 6, "Total Nilai Ijazah", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NILAI_IJAZAH']}", true, true, 'L');
            $pdf->Cell(70, 6, "Tahun UNAS", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['TAHUN_UAN']}", true, true, 'L');
            $pdf->Cell(70, 6, "Jumlah Pelajaran UNAS", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['JUMLAH_PELAJARAN_UAN']}", true, true, 'L');
            $pdf->Cell(70, 6, "Total Nilai UNAS", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NILAI_UAN']}", true, true, 'L');

            //$pdf->Ln();
            $pdf->AddPage();
            $pdf->SetY(20);

            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Cell(0, 6, "DATA ORANG TUA - AYAH", true, true, 'C', true);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(70, 6, "Nama Ayah", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NAMA_AYAH']}", true, true, 'L');
            $pdf->Cell(70, 6, "Alamat", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['ALAMAT_AYAH']}", true, true, 'L');
            $pdf->Cell(70, 6, "Kota", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NM_KOTA_AYAH']}", true, true, 'L');
            $pdf->Cell(70, 6, "Telp", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['TELP_AYAH']}", true, true, 'L');
            $pdf->Cell(70, 6, "Pendidikan Ayah", true, false, 'L');
            $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['PENDIDIKAN_AYAH'], $this->GetListPendidikanOrtu(), 'ID_PENDIDIKAN_ORTU', 'NM_PENDIDIKAN_ORTU'), true, true, 'L');
            $pdf->Cell(70, 6, "Pekerjaan Ayah", true, false, 'L');
            $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['PEKERJAAN_AYAH'], $this->GetListPekerjaan(), 'ID_PEKERJAAN', 'NM_PEKERJAAN'), true, true, 'L');
            //$pdf->Cell(70, 6, "Penghasilan Ayah", true, false, 'L');
            //$pdf->Cell(0, 6, "Rp " . number_format($cmb['PENGHASILAN_AYAH'], 0, ',', '.'), true, true, 'L');
            $pdf->Cell(70, 6, "Instansi / Perusahaan Tempat Kerja", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['INSTANSI_AYAH']}", true, true, 'L');
            $pdf->Cell(70, 6, "Jabatan / Golongan", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['JABATAN_AYAH']}", true, true, 'L');
            $pdf->Cell(70, 6, "Masa Kerja", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['MASA_KERJA_AYAH']}", true, true, 'L');

            $pdf->Ln();

            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Cell(0, 6, "DATA ORANG TUA - IBU", true, true, 'C', true);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(70, 6, "Nama Ibu", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NAMA_IBU']}", true, true, 'L');
            $pdf->Cell(70, 6, "Alamat", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['ALAMAT_IBU']}", true, true, 'L');
            $pdf->Cell(70, 6, "Kota", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NM_KOTA_IBU']}", true, true, 'L');
            $pdf->Cell(70, 6, "Telp", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['TELP_IBU']}", true, true, 'L');
            $pdf->Cell(70, 6, "Pendidikan Ibu", true, false, 'L');
            $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['PENDIDIKAN_IBU'], $this->GetListPendidikanOrtu(), 'ID_PENDIDIKAN_ORTU', 'NM_PENDIDIKAN_ORTU'), true, true, 'L');
            $pdf->Cell(70, 6, "Pekerjaan Ibu", true, false, 'L');
            $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['PEKERJAAN_IBU'], $this->GetListPekerjaan(), 'ID_PEKERJAAN', 'NM_PEKERJAAN'), true, true, 'L');
            //$pdf->Cell(70, 6, "Penghasilan Ibu", true, false, 'L');
            //$pdf->Cell(0, 6, "Rp " . number_format($cmb['PENGHASILAN_IBU'], 0, ',', '.'), true, true, 'L');
            $pdf->Cell(70, 6, "Instansi / Perusahaan Tempat Kerja", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['INSTANSI_IBU']}", true, true, 'L');
            $pdf->Cell(70, 6, "Jabatan / Golongan", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['JABATAN_IBU']}", true, true, 'L');
            $pdf->Cell(70, 6, "Masa Kerja", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['MASA_KERJA_IBU']}", true, true, 'L');

            $pdf->Ln();

            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Cell(0, 6, "DATA ORANG TUA - LAIN-LAIN", true, true, 'C', true);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(70, 6, "Skala Pekerjaan Ortu", true, false, 'L');
            $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['SKALA_PEKERJAAN_ORTU'], $this->GetListSkalaPekerjaan(), 'SKALA_PEKERJAAN', 'NM_SKALA_PEKERJAAN'), true, true, 'L');
            $pdf->Cell(70, 6, "Jumlah Kakak", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['JUMLAH_KAKAK']}", true, true, 'L');
            $pdf->Cell(70, 6, "Jumlah Adik", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['JUMLAH_ADIK']}", true, true, 'L');
        }

        if ($cmb['ID_JALUR'] == 4) { // ALIH JENIS
            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Cell(0, 6, "DATA PEKERJAAN", true, true, 'C', true);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(70, 6, "Pekerjaan", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PEKERJAAN']}", true, true, 'L');
            $pdf->Cell(70, 6, "Asal Instansi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['ASAL_INSTANSI']}", true, true, 'L');
            $pdf->Cell(70, 6, "Alamat Instansi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['ALAMAT_INSTANSI']}", true, true, 'L');
            $pdf->Cell(70, 6, "Telp Instansi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['TELP_INSTANSI']}", true, true, 'L');
            $pdf->Cell(70, 6, "NIP / NIS / NRP", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NRP']}", true, true, 'L');
            $pdf->Cell(70, 6, "Karpeg", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['KARPEG']}", true, true, 'L');
            $pdf->Cell(70, 6, "Pangkat", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PANGKAT']}", true, true, 'L');

            $pdf->AddPage();
            $pdf->SetY(20);

            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Cell(0, 6, "DATA PENDIDIKAN (D3)", true, true, 'C', true);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(70, 6, "Perguruan Tinggi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PTN_S1']}", true, true, 'L');
            $pdf->Cell(70, 6, "Status Perguruan Tinggi", true, false, 'L');
            $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['STATUS_PTN_S1'], $this->GetListStatusPTN(), 'STATUS_PTN', 'NM_STATUS_PTN'), true, true, 'L');
            $pdf->Cell(70, 6, "Program Studi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PRODI_S1']}", true, true, 'L');
            $pdf->Cell(70, 6, "Tanggal Masuk", true, false, 'L');
            $pdf->Cell(0, 6, strftime('%d %B %Y', strtotime($cmb['TGL_MASUK_S1'])), true, true, 'L');
            $pdf->Cell(70, 6, "Tanggal Lulus", true, false, 'L');
            $pdf->Cell(0, 6, strftime('%d %B %Y', strtotime($cmb['TGL_LULUS_S1'])), true, true, 'L');
            $pdf->Cell(70, 6, "Lama Studi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['LAMA_STUDI_S1']}", true, true, 'L');
            $pdf->Cell(70, 6, "Index Prestasi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['IP_S1']}", true, true, 'L');
            $pdf->Cell(70, 6, "Jumlah Karya Ilmiah", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['JUMLAH_KARYA_ILMIAH']}", true, true, 'L');
        }

        if ((in_array($cmb['ID_JALUR'], array(6, 34, 35)) && $cmb['ID_JENJANG'] == 2) || ($cmb['ID_JALUR'] == 23 && $cmb['ID_JENJANG'] == 9)) {  // Magister, Profesi
            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Cell(0, 6, "DATA PEKERJAAN", true, true, 'C', true);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(70, 6, "Pekerjaan", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PEKERJAAN']}", true, true, 'L');
            $pdf->Cell(70, 6, "Asal Instansi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['ASAL_INSTANSI']}", true, true, 'L');
            $pdf->Cell(70, 6, "Alamat Instansi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['ALAMAT_INSTANSI']}", true, true, 'L');
            $pdf->Cell(70, 6, "Telp Instansi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['TELP_INSTANSI']}", true, true, 'L');
            $pdf->Cell(70, 6, "NIP / NIS / NRP", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NRP']}", true, true, 'L');
            $pdf->Cell(70, 6, "Karpeg", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['KARPEG']}", true, true, 'L');
            $pdf->Cell(70, 6, "Pangkat", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PANGKAT']}", true, true, 'L');

            $pdf->AddPage();
            $pdf->SetY(20);

            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Cell(0, 6, "DATA PENDIDIKAN (S1)", true, true, 'C', true);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(70, 6, "Perguruan Tinggi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PTN_S1']}", true, true, 'L');
            $pdf->Cell(70, 6, "Status Perguruan Tinggi", true, false, 'L');
            $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['STATUS_PTN_S1'], $this->GetListStatusPTN(), 'STATUS_PTN', 'NM_STATUS_PTN'), true, true, 'L');
            $pdf->Cell(70, 6, "Program Studi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PRODI_S1']}", true, true, 'L');
            $pdf->Cell(70, 6, "Tanggal Masuk", true, false, 'L');
            $pdf->Cell(0, 6, strftime('%d %B %Y', strtotime($cmb['TGL_MASUK_S1'])), true, true, 'L');
            $pdf->Cell(70, 6, "Tanggal Lulus", true, false, 'L');
            $pdf->Cell(0, 6, strftime('%d %B %Y', strtotime($cmb['TGL_LULUS_S1'])), true, true, 'L');
            $pdf->Cell(70, 6, "Lama Studi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['LAMA_STUDI_S1']}", true, true, 'L');
            $pdf->Cell(70, 6, "Index Prestasi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['IP_S1']}", true, true, 'L');
            $pdf->Cell(70, 6, "Jumlah Karya Ilmiah", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['JUMLAH_KARYA_ILMIAH']}", true, true, 'L');
        }

        if ((in_array($cmb['ID_JALUR'], array(6, 35)) && $cmb['ID_JENJANG'] == 3) || ($cmb['ID_JALUR'] == 24)) {  // Doktor, spesialis
            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Cell(0, 6, "DATA PEKERJAAN", true, true, 'C', true);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(70, 6, "Pekerjaan", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PEKERJAAN']}", true, true, 'L');
            $pdf->Cell(70, 6, "Asal Instansi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['ASAL_INSTANSI']}", true, true, 'L');
            $pdf->Cell(70, 6, "Alamat Instansi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['ALAMAT_INSTANSI']}", true, true, 'L');
            $pdf->Cell(70, 6, "Telp Instansi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['TELP_INSTANSI']}", true, true, 'L');
            $pdf->Cell(70, 6, "NIP / NIS / NRP", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['NRP']}", true, true, 'L');
            $pdf->Cell(70, 6, "Karpeg", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['KARPEG']}", true, true, 'L');
            $pdf->Cell(70, 6, "Pangkat", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PANGKAT']}", true, true, 'L');

            $pdf->AddPage();
            $pdf->SetY(20);

            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Cell(0, 6, "DATA PENDIDIKAN (S1)", true, true, 'C', true);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(70, 6, "Perguruan Tinggi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PTN_S1']}", true, true, 'L');
            $pdf->Cell(70, 6, "Status Perguruan Tinggi", true, false, 'L');
            $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['STATUS_PTN_S1'], $this->GetListStatusPTN(), 'STATUS_PTN', 'NM_STATUS_PTN'), true, true, 'L');
            $pdf->Cell(70, 6, "Program Studi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PRODI_S1']}", true, true, 'L');
            $pdf->Cell(70, 6, "Tanggal Masuk", true, false, 'L');
            $pdf->Cell(0, 6, strftime('%d %B %Y', strtotime($cmb['TGL_MASUK_S1'])), true, true, 'L');
            $pdf->Cell(70, 6, "Tanggal Lulus", true, false, 'L');
            $pdf->Cell(0, 6, strftime('%d %B %Y', strtotime($cmb['TGL_LULUS_S1'])), true, true, 'L');
            $pdf->Cell(70, 6, "Lama Studi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['LAMA_STUDI_S1']}", true, true, 'L');
            $pdf->Cell(70, 6, "Index Prestasi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['IP_S1']}", true, true, 'L');
            $pdf->Cell(70, 6, "Jumlah Karya Ilmiah", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['JUMLAH_KARYA_ILMIAH']}", true, true, 'L');

            $pdf->Ln();

            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Cell(0, 6, "DATA PENDIDIKAN (S2/Pr)", true, true, 'C', true);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(70, 6, "Perguruan Tinggi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PTN_S2']}", true, true, 'L');
            $pdf->Cell(70, 6, "Status Perguruan Tinggi", true, false, 'L');
            $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['STATUS_PTN_S2'], $this->GetListStatusPTN(), 'STATUS_PTN', 'NM_STATUS_PTN'), true, true, 'L');
            $pdf->Cell(70, 6, "Program Studi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['PRODI_S2']}", true, true, 'L');
            $pdf->Cell(70, 6, "Tanggal Masuk", true, false, 'L');
            $pdf->Cell(0, 6, strftime('%d %B %Y', strtotime($cmb['TGL_MASUK_S2'])), true, true, 'L');
            $pdf->Cell(70, 6, "Tanggal Lulus", true, false, 'L');
            $pdf->Cell(0, 6, strftime('%d %B %Y', strtotime($cmb['TGL_LULUS_S2'])), true, true, 'L');
            $pdf->Cell(70, 6, "Lama Studi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['LAMA_STUDI_S2']}", true, true, 'L');
            $pdf->Cell(70, 6, "Index Prestasi", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['IP_S2']}", true, true, 'L');
            $pdf->Cell(70, 6, "Jumlah Karya Ilmiah", true, false, 'L');
            $pdf->Cell(0, 6, "{$cmb['JUMLAH_KARYA_ILMIAH']}", true, true, 'L');
        }

        $pdf->Ln();

        $pdf->MultiCell(0, 5, "Dengan ini saya menyatakan bahwa data yang sudah diisi adalah sebenar-benarnya dan jika ada pemalsuan dalam pengisian data maka saya siap kehilangan hak sebagai mahasiswa Universitas Airlangga", false, 'L');

        $pdf->Ln();

        $pdf->SetX(130);
        $pdf->MultiCell(0, 6, "................., " . strftime('%d %B %Y') . "\r\nTanda tangan yang bersangkutan", false, 'C');

        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetX(130);
        $pdf->MultiCell(0, 6, "{$cmb['NM_C_MHS']}", false, 'C');

        // Surat gaji orang tua dan pernyataan
        if ($is_regmaba) {
            if ($cmb['ID_JALUR'] == 1 or $cmb['ID_JALUR'] == 3 or $cmb['ID_JALUR'] == 20 or $cmb['ID_JALUR'] == 27) {  // snmptn, mandiri, diploma, depag
                $pdf->AddPage();
                $pdf->SetFont('Calibri', 'B', 16);
                $pdf->Cell(0, 6, "SURAT - PERNYATAAN", '0', false, 'C', false);
                $pdf->Ln();
                $pdf->Ln();
                $pdf->Ln();

                if ($is_regmaba == true) {
                    $pdf->SetFont('Calibri', 'B', 12);
                    $pdf->SetTextColor(255, 255, 255);
                    $pdf->Cell(0, 6, "PROGRAM STUDI", true, true, 'C', true);
                    $pdf->SetFont('Calibri', '', 12);
                    $pdf->SetTextColor(0, 0, 0);
                    $pdf->Cell(70, 6, "Fakultas", true, false, 'L');
                    $pdf->Cell(0, 6, "{$cmb['NM_FAKULTAS']}", true, true, 'L');
                    $pdf->Cell(70, 6, "Program Studi", true, false, 'L');
                    $pdf->Cell(0, 6, "{$cmb['NM_PROGRAM_STUDI']}", true, true, 'L');

                    if ($cmb['ID_PRODI_MINAT'] != '') {
                        $pdf->Cell(70, 6, "Minat Studi", true, false, 'L');
                        $pdf->Cell(0, 6, "{$cmb['NM_PRODI_MINAT']}", true, true, 'L');
                    }

                    if ($cmb['ID_KELAS_PILIHAN'] != '') {
                        $pdf->Cell(70, 6, "Kelas Pilihan", true, false, 'L');
                        $pdf->Cell(0, 6, "{$cmb['NM_KELAS_PILIHAN']}", true, true, 'L');
                    }
                }

                $pdf->Ln();

                $pdf->SetFont('Calibri', 'B', 12);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Cell(0, 6, "IDENTITAS DIRI", true, true, 'C', true);
                $pdf->SetFont('Calibri', '', 12);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(70, 6, "Nama Lengkap", true, false, 'L');
                $pdf->Cell(0, 6, "{$cmb['NM_C_MHS']}", true, true, 'L');

                if ($cmb['GELAR'] != '') {
                    $pdf->Cell(70, 6, "Gelar", true, false, 'L');
                    $pdf->Cell(0, 6, "{$cmb['GELAR']}", true, true, 'L');
                }

                $pdf->Cell(70, 6, "Tempat dan Tanggal Lahir", true, false, 'L');
                $pdf->Cell(0, 6, "{$tempat_tanggal_lahir}", true, true, 'L');
                $pdf->Cell(70, 6, "Alamat", true, false, 'L');
                $pdf->Cell(0, 6, "{$cmb['ALAMAT']}", true, true, 'L');
                $pdf->Cell(70, 6, "Kota", true, false, 'L');
                $pdf->Cell(0, 6, "{$cmb['NM_KOTA']}", true, true, 'L');
                $pdf->Cell(70, 6, "No Telp", true, false, 'L');
                $pdf->Cell(0, 6, "{$cmb['TELP']}", true, true, 'L');

                if (in_array($cmb['ID_JALUR'], $array_jalur_umum)) {
                    $pdf->Cell(70, 6, "No Telp Utama", true, false, 'L');
                    $pdf->Cell(0, 6, "{$cmb['TELP_UTAMA']}", true, true, 'L');
                    $pdf->Cell(70, 6, "PIN Blackberry", true, false, 'L');
                    $pdf->Cell(0, 6, "{$cmb['PIN_BB']}", true, true, 'L');
                }

                $pdf->Cell(70, 6, "Jenis Kelamin", true, false, 'L');
                $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['JENIS_KELAMIN'], $this->GetListJenisKelamin(), 'JENIS_KELAMIN', 'NM_JENIS_KELAMIN'), true, true, 'L');
                $pdf->Cell(70, 6, "Kewarganegaraan", true, false, 'L');
                $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['KEWARGANEGARAAN'], $this->GetListKewarganegaraan(), 'ID_KEWARGANEGARAAN', 'NM_KEWARGANEGARAAN'), true, true, 'L');
                $pdf->Cell(70, 6, "Agama", true, false, 'L');
                $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['ID_AGAMA'], $this->GetListAgama(), 'ID_AGAMA', 'NM_AGAMA'), true, true, 'L');
                $pdf->Cell(70, 6, "Sumber Biaya", true, false, 'L');
                $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['SUMBER_BIAYA'], $this->GetListSumberBiaya(), 'ID_SUMBER_BIAYA', 'NM_SUMBER_BIAYA'), true, true, 'L');
                $pdf->Cell(70, 6, "Email", true, false, 'L');
                $pdf->Cell(0, 6, "{$cmb['EMAIL']}", true, true, 'L');


                $pdf->Ln();
                $pdf->SetFont('Calibri', 'B', 12);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Cell(0, 6, "KEDIAMAN DAN KENDARAAN KELUARGA", true, true, 'C', true);
                $pdf->SetFont('Calibri', '', 12);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(70, 6, "Kediaman Orang Tua", true, false, 'L');
                $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['KEDIAMAN_ORTU'], $this->GetListKediamanOrtu(), 'KEDIAMAN_ORTU', 'NM_KEDIAMAN_ORTU'), true, true, 'L');
                $pdf->Cell(70, 6, "Luas Tanah", true, false, 'L');
                $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['LUAS_TANAH'], $this->GetListLuasTanah(), 'LUAS_TANAH', 'NM_LUAS_TANAH'), true, true, 'L');
                $pdf->Cell(70, 6, "Luas Bangunan", true, false, 'L');
                $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['LUAS_BANGUNAN'], $this->GetListLuasBangunan(), 'LUAS_BANGUNAN', 'NM_LUAS_BANGUNAN'), true, true, 'L');
                $pdf->Cell(70, 6, "Nilai NJOP", true, false, 'L');
                $pdf->Cell(0, 6, $range_njop, true, true, 'L');
                $pdf->Cell(70, 6, "Tagihan PBB", true, false, 'L');
                $pdf->Cell(0, 6, $range_pbb, true, true, 'L');
                $pdf->Cell(70, 6, "Listrik", true, false, 'L');
                $pdf->Cell(0, 6, $this->GetValueFromArray($cmb['LISTRIK'], $this->GetListListrik(), 'LISTRIK', 'NM_LISTRIK'), true, true, 'L');
                $pdf->Cell(70, 6, "Tagihan Listrik", true, false, 'L');
                $pdf->Cell(0, 6, $range_listrik, true, true, 'L');
                $pdf->Cell(70, 6, "Tagihan Air", true, false, 'L');
                $pdf->Cell(0, 6, $range_air, true, true, 'L');
                $pdf->Cell(70, 6, "Kendaraan R4", true, false, 'L');
                $pdf->Cell(0, 6, $cmb['KENDARAAN_R4'], true, true, 'L');
                $pdf->Cell(70, 6, "Tahun Kendaraan R4", true, false, 'L');
                $pdf->Cell(0, 6, $cmb['TAHUN_KENDARAAN_R4'], true, true, 'L');
                $pdf->Cell(70, 6, "Nama/Merek Kendaraan R4", true, false, 'L');
                $pdf->Cell(0, 6, $cmb['MEREK_KENDARAAN_R4'], true, true, 'L');
                $pdf->Cell(70, 6, "Kendaraan R2", true, false, 'L');
                $pdf->Cell(0, 6, $cmb['KENDARAAN_R2'], true, true, 'L');
                $pdf->Cell(70, 6, "Tahun Kendaraan R2", true, false, 'L');
                $pdf->Cell(0, 6, $cmb['TAHUN_KENDARAAN_R2'], true, true, 'L');
                $pdf->Cell(70, 6, "Nama/Merek Kendaraan R2", true, false, 'L');
                $pdf->Cell(0, 6, $cmb['MEREK_KENDARAAN_R2'], true, true, 'L');
                $pdf->Cell(70, 6, "Jumlah Handphone", true, false, 'L');
                $pdf->Cell(0, 6, $cmb['JUMLAH_HANDPHONE'], true, true, 'L');
                $pdf->Cell(70, 6, "Transportasi Ke Kampus", true, false, 'L');
                $pdf->Cell(0, 6, $cmb['TRANSPORTASI'], true, true, 'L');
                $pdf->Cell(70, 6, "Kekayaan lain", true, false, 'L');
                $pdf->Cell(0, 6, str_replace("\r\n", ' ', $cmb['KEKAYAAN_LAIN']), true, true, 'L');
                $pdf->Cell(70, 6, "Informasi lain", true, false, 'L');
                $pdf->Cell(0, 6, str_replace("\r\n", ' ', $cmb['INFO_LAIN']), true, true, 'L');
                $pdf->Cell(70, 6, "Range Penghasilan Ortu", true, false, 'L');
                $pdf->Cell(0, 6, $range_penghasilan, true, true, 'L');

                $pdf->AddPage();
                // ayah
                $pdf->SetFont('Calibri', 'B', 12);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Cell(0, 6, "DATA PENGHASILAN AYAH", true, true, 'C', true);
                $pdf->SetFont('Calibri', '', 12);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(70, 6, "Gaji", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['GAJI_AYAH'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Tunjangan Keluarga", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['TUNJANGAN_KELUARGA_AYAH'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Tunjangan Jabatan / Golongan", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['TUNJANGAN_JABATAN_AYAH'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Tunjangan Sertifikasi Guru / Dosen", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['TUNJANGAN_SERTIFIKASI_AYAH'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Tunjangan Kehormatan", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['TUNJANGAN_KEHORMATAN_AYAH'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Remunerasi", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['RENUMERASI_AYAH'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Tunjangan lain-lain", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['TUNJANGAN_LAIN_AYAH'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Penghasilan lain-lain", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['PENGHASILAN_LAIN_AYAH'], 0, ",", "."), true, true, 'L');

                // Ibu
                $pdf->SetFont('Calibri', 'B', 12);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Cell(0, 6, "DATA PENGHASILAN IBU", true, true, 'C', true);
                $pdf->SetFont('Calibri', '', 12);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(70, 6, "Gaji", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['GAJI_IBU'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Tunjangan Keluarga", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['TUNJANGAN_KELUARGA_IBU'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Tunjangan Jabatan / Golongan", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['TUNJANGAN_JABATAN_IBU'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Tunjangan Sertifikasi Guru / Dosen", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['TUNJANGAN_SERTIFIKASI_IBU'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Tunjangan Kehormatan", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['TUNJANGAN_KEHORMATAN_IBU'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Remunerasi", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['RENUMERASI_IBU'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Tunjangan lain-lain", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['TUNJANGAN_LAIN_IBU'], 0, ",", "."), true, true, 'L');
                $pdf->Cell(70, 6, "Penghasilan lain-lain", true, false, 'L');
                $pdf->Cell(0, 6, number_format($cmb['PENGHASILAN_LAIN_IBU'], 0, ",", "."), true, true, 'L');

                $pdf->Ln();
                $pdf->Ln();
                $pdf->MultiCell(190, 6, "Dengan ini saya menyatakan bahwa data yang sudah diisi adalah sebenar-benarnya dan jika ada pemalsuan dalam pengisian data maka saya siap kehilangan hak sebagai mahasiswa Universitas Airlangga");

                $pdf->Ln();
                $pdf->Ln();

                // kotak foto
                /*
                  $pdf->Rect(90, 165, 30, 40);
                  $pdf->SetXY(90, 180);
                  $pdf->SetFont('Calibri', '', 12);
                  $pdf->Cell(30, 4, "4 x 6", false, false, 'C');
                 * */
                $pdf->Cell(70, 5, "Orang Tua Mahasiswa", false, false, 'L');
                $pdf->Cell(110, 5, "Surabaya, .........................", false, false, 'R');
                $pdf->Ln();
                $pdf->Cell(166, 5, "Mengetahui,", false, false, 'L');
                $pdf->Ln();
                $pdf->Ln();
                $pdf->Ln();
                $pdf->SetFont('Calibri', '', 10);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(145, 5, "Materai", false, false, 'R');
                $pdf->Ln();
                $pdf->Cell(150, 5, "Rp 6.000,-,", false, false, 'R');
                $pdf->Ln();
                $pdf->SetFont('Calibri', '', 12);
                $pdf->Cell(90, 5, "( {$cmb['NAMA_AYAH']} )", false, false, 'L');
                $pdf->Cell(90, 5, "( {$cmb['NM_C_MHS']} )", false, false, 'R');

                // Jadwal verifikasi keuangan
                if ($cmb['ID_JALUR'] == 1) {
                    if ($cmb['STATUS_BIDIK_MISI'] == 0 && $cmb['TGL_JADWAL_VERIFIKASI_KEUANGAN'] != '') {
                        $border = 1;

                        $pdf->AddPage();
                        $pdf->SetFont('Calibri', 'B', 16);
                        $pdf->Cell(0, 6, "JADWAL VERIFIKASI KEUANGAN", $border, true, 'C');

                        $pdf->Ln();
                        $pdf->SetFont('Calibri', '', 14);
                        $pdf->Cell(60, 7, "NOMOR UJIAN :", true, false);
                        $pdf->Cell(0, 7, $cmb['NO_UJIAN'], true, true);
                        $pdf->Cell(60, 7, "NAMA :", true, false);
                        $pdf->Cell(0, 7, $cmb['NM_C_MHS'], true, true);
                        $pdf->Cell(60, 7, "PROGRAM STUDI :", true, false);
                        $pdf->Cell(0, 7, $cmb['NM_PROGRAM_STUDI'], true, true);
                        $pdf->Cell(60, 7, "TANGGAL VERIFIKASI :", true, false);
                        // HARDCODE untuk verifikasi Online
                        $pdf->Cell(0, 7, $cmb['ID_PENERIMAAN'] == '149' ? "--" : strftime('%d %B %Y', strtotime($cmb['TGL_JADWAL_VERIFIKASI_KEUANGAN'])), true, true);
                        $pdf->SetFont('Calibri', '', 12);
                        $pdf->Cell(0, 7, $cmb['ID_PENERIMAAN'] == '149' ? "--" : "Harap membawa berkas ini untuk ditunjukkan kepada petugas verifikasi keuangan", $border, true);
                    }
                }
            }



            //jadwal daftar ulang
            //if ($cmb['ID_JALUR'] == 1 or $cmb['ID_JALUR'] == 3 or $cmb['ID_JALUR'] == 5)
            if ($cmb['TGL_JADWAL_VERIFIKASI_PEND'] != '') {
                $pdf->AddPage();
                $pdf->SetFont('Calibri', 'B', 16);
                $pdf->Cell(0, 6, "JADWAL DAFTAR ULANG", $border, true, 'C');

                $pdf->Ln();
                $pdf->SetFont('Calibri', '', 14);
                $pdf->Cell(60, 7, "NOMOR UJIAN :", true, false);
                $pdf->Cell(0, 7, $cmb['NO_UJIAN'], true, true);
                $pdf->Cell(60, 7, "NAMA :", true, false);
                $pdf->Cell(0, 7, $cmb['NM_C_MHS'], true, true);
                $pdf->Cell(60, 7, "PROGRAM STUDI :", true, false);
                $pdf->Cell(0, 7, $cmb['NM_PROGRAM_STUDI'], true, true);
                $pdf->Cell(60, 7, "TANGGAL DAFTAR ULANG :", true, false);
                $pdf->Cell(0, 7, strftime('%d %B %Y', strtotime($cmb['TGL_JADWAL_VERIFIKASI_PEND'])), true, true);
                $pdf->Cell(60, 7, "JAM DAFTAR ULANG :", true, false);
                $pdf->Cell(0, 7, $cmb['JAM_AWAL_VERIFIKASI_PEND'] . ' - ' . $cmb['JAM_AKHIR_VERIFIKASI_PEND'], true, true);
                $pdf->SetFont('Calibri', '', 12);
                $pdf->Cell(0, 7, "Harap membawa berkas ini untuk ditunjukkan kepada petugas daftar ulang", $border, true);
            }

            // surat pernyataan
            $pdf->AddPage();
            $pdf->SetFont('Calibri', 'B', 16);
            $pdf->Cell(0, 6, "SURAT - PERNYATAAN", '0', false, 'C', false);
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Ln();

            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(0, 8, "Yang bertanda tangan dibawah ini, Saya :", false, false, 'L', false);
            $pdf->Ln();
            $pdf->Cell(60, 8, "Nama", false, false, 'L');
            $pdf->Cell(0, 8, ":", false, true, 'L');
            $pdf->Cell(60, 8, "Tempat dan tanggal lahir", false, false, 'L');
            $pdf->Cell(0, 8, ":", false, true, 'L');
            $pdf->Cell(60, 8, "Alamat Surabaya", false, false, 'L');
            $pdf->Cell(0, 8, ": ................................................................................", false, true, 'L');
            $pdf->Cell(60, 8, "", false, false, 'L');
            $pdf->Cell(0, 8, " Telp.  .............................................", false, true, 'L');
            $pdf->Cell(60, 8, "Alamat Asal", false, false, 'L');
            $pdf->Cell(0, 8, ":", false, true, 'L');
            $pdf->Cell(60, 8, "Pekerjaan", false, false, 'L');
            $pdf->Cell(0, 8, ":", false, true, 'L');
            $pdf->Cell(60, 8, "Alamat Rumah", false, false, 'L');
            $pdf->Cell(0, 8, ": ................................................................................", false, true, 'L');
            $pdf->Cell(60, 8, "", false, false, 'L');
            $pdf->Cell(0, 8, " Telp.  .............................................", false, true, 'L');
            $pdf->Cell(60, 15, "Dengan ini menyatakan bahwa :", false, false, 'L');
            $pdf->Ln();
            $pdf->MultiCell(190, 6, "Saya tidak pernah, sedang atau akan terlibat dalam penyalahgunaan Narkotika, Alkohol, Psikotropikadan Zat Adiktif (NAPZA) baik sebagai pengguna, pengedar, produsen atau yang berkaitan dengan hal tersebut. Apabila ternyata di kemudian hari pada saat saya menuntut ilmu di Universitas Airlangga saya terlibat dan atau terbukti terlibat dalam penyalahgunaan NAPZA sebagaimana dimaksud di atas, maka saya sanggup dan bersedia dikenakan sanksi sampai dengan dibatalkan status saya sebagai mahasiswa Universitas Airlangga.");
            $pdf->Ln();
            $pdf->MultiCell(190, 8, "Demikian surat pernyataan ini saya buat dengan sebenarnya, tanpa adanya paksaan dari pihak siapapun.");

            // kotak foto
            $pdf->Rect(90, 185, 30, 40);
            $pdf->SetXY(90, 200);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->Cell(30, 4, "4 x 6", false, false, 'C');

            $pdf->Cell(74, 5, "Surabaya, .............................", false, false, 'R');
            $pdf->Ln();
            $pdf->Cell(158, 5, "Hormat Saya,", false, false, 'R');
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Ln();
            $pdf->SetFont('Calibri', '', 9);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(145, 5, "Materai", false, false, 'R');
            $pdf->Ln();
            $pdf->Cell(150, 5, "Rp 6.000,-,", false, false, 'R');

            $pdf->AddPage();
            $pdf->SetFont('Calibri', 'B', 16);
            $pdf->Cell(0, 6, "SURAT PERNYATAAN BERSEDIA MEMENUHI DAN MENTAATI KETENTUAN", '0', false, 'C', false);
            $pdf->Ln();
            $pdf->Cell(0, 6, "DAN PERATURAN SERTA KEPUTUSAN YANG BERLAKU", '0', false, 'C', false);
            $pdf->Ln();
            $pdf->Cell(0, 6, "DI UNIVERSITAS AIRLANGGA", '0', false, 'C', false);
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Ln();

            $pdf->SetFont('Calibri', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(0, 8, "Yang bertanda tangan dibawah ini, Saya :", false, false, 'L', false);
            $pdf->Ln();
            $pdf->Cell(60, 8, "Nama", false, false, 'L');
            $pdf->Cell(0, 8, ":", false, true, 'L');
            $pdf->Cell(60, 8, "Tempat dan tanggal lahir", false, false, 'L');
            $pdf->Cell(0, 8, ":", false, true, 'L');
            $pdf->Cell(60, 8, "Alamat Surabaya", false, false, 'L');
            $pdf->Cell(0, 8, ": ................................................................................", false, true, 'L');
            $pdf->Cell(60, 8, "", false, false, 'L');
            $pdf->Cell(0, 8, " Telp.  .............................................", false, true, 'L');
            $pdf->Cell(60, 8, "Alamat Asal", false, false, 'L');
            $pdf->Cell(0, 8, ":", false, true, 'L');
            $pdf->Cell(60, 8, "Pekerjaan", false, false, 'L');
            $pdf->Cell(0, 8, ":", false, true, 'L');
            $pdf->Cell(60, 8, "Alamat Rumah", false, false, 'L');
            $pdf->Cell(0, 8, ": ................................................................................", false, true, 'L');
            $pdf->Cell(60, 8, "", false, false, 'L');
            $pdf->Cell(0, 8, " Telp.  .............................................", false, true, 'L');
            $pdf->Ln();
            $pdf->SetFont('Calibri', 'B', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->MultiCell(190, 6, "Dengan ini menyatakan bahwa sebagai mahasiswa Universitas Airlangga, saya bersedia memenuhi dan mentaati ketentuan dan peraturan serta keputusan yang berlaku di Universitas Airlangga dan apabila saya melanggar saya bersedia dikenakan sangsi.");
            $pdf->Ln();
            $pdf->MultiCell(190, 6, "Demikian surat pernyataan ini saya buat dengan sebenarnya, tanpa adanya paksaan dari pihak siapapun.");

            // kotak foto
            $pdf->Rect(90, 165, 30, 40);
            $pdf->SetXY(90, 180);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->Cell(30, 4, "4 x 6", false, false, 'C');

            $pdf->Cell(73, 5, "Surabaya, .............................", false, false, 'R');
            $pdf->Ln();
            $pdf->Cell(157, 5, "Hormat Saya,", false, false, 'R');
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Ln();
            $pdf->SetFont('Calibri', '', 9);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(145, 5, "Materai", false, false, 'R');
            $pdf->Ln();
            $pdf->Cell(150, 5, "Rp 6.000,-,", false, false, 'R');
        }


        $pdf->SetDisplayMode('real', 'continuous');
        $pdf->Output('formulir.pdf', 'I');
    }

    function LoadPembayaranCalonMahasiswa($id_c_mhs) {
        $rows = $this->db->QueryToArray("
            SELECT b.id_biaya, B.NM_BIAYA, PEM.BESAR_BIAYA FROM PEMBAYARAN_CMHS PEM
            JOIN DETAIL_BIAYA DB ON DB.ID_DETAIL_BIAYA=PEM.ID_DETAIL_BIAYA
            JOIN BIAYA B ON B.ID_BIAYA=DB.ID_BIAYA
            WHERE PEM.ID_C_MHS='{$id_c_mhs}' order by nm_biaya");

        $result = array(
            'TOTAL_1' => 0,
            'TOTAL_2' => 0,
            'TOTAL_3' => 0,
            'TOTAL_4' => 0,
            'TOTAL' => 0,
        );

        // mengelompokkan biaya
        foreach ($rows as $row) {
            // Nomer 1
            if ($row['ID_BIAYA'] == 102) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }
            if ($row['ID_BIAYA'] == 103) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }
            if ($row['ID_BIAYA'] == 104) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }
            if ($row['ID_BIAYA'] == 105) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }
            if ($row['ID_BIAYA'] == 106) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }
            if ($row['ID_BIAYA'] == 107) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }
            if ($row['ID_BIAYA'] == 108) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }
            if ($row['ID_BIAYA'] == 109) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }
            if ($row['ID_BIAYA'] == 110) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }
            if ($row['ID_BIAYA'] == 111) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }
            if ($row['ID_BIAYA'] == 112) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }
            if ($row['ID_BIAYA'] == 113) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }
            if ($row['ID_BIAYA'] == 114) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }
            if ($row['ID_BIAYA'] == 115) {
                $result['TOTAL_1'] += $row['BESAR_BIAYA'];
            }

            // Nomer 2 : SOP
            if ($row['ID_BIAYA'] == 47) {
                $result['TOTAL_2'] += $row['BESAR_BIAYA'];
            }

            // Nomer 3 : SP3
            if ($row['ID_BIAYA'] == 81) {
                $result['TOTAL_3'] += $row['BESAR_BIAYA'];
            }

            // Nomer 4 : Sumbangan
            if ($row['ID_BIAYA'] == 124) {
                $result['TOTAL_4'] += $row['BESAR_BIAYA'];
            }
        }

        $result['TOTAL'] = $result['TOTAL_1'] + $result['TOTAL_2'] + $result['TOTAL_3'] + $result['TOTAL_4'];

        return $result;
    }

    function GetDirekturKeuangan() {
        $rows = $this->db->QueryToArray("
            select (gelar_depan||' '||nm_pengguna||', '||gelar_belakang) as nm_pengguna, nip_dosen from dosen
            join pengguna on pengguna.id_pengguna = dosen.id_pengguna
            where id_jabatan_pegawai = 32");
        return $rows[0];
    }

    function GetVerifikatorKeuangan($id_c_mhs) {
        $rows = $this->db->QueryToArray("
            select p.nm_pengguna, p.username from calon_mahasiswa_baru cmb
            join pengguna p on p.id_pengguna = cmb.id_verifikator_keuangan
            where cmb.id_c_mhs = {$id_c_mhs}");
        return $rows[0];
    }

    function PrintPdfKeuangan($id_c_mhs, FPDF &$pdf) {
        $cmb = $this->GetData($id_c_mhs, true);
        $pembayaran = $this->LoadPembayaranCalonMahasiswa($id_c_mhs);
        $this->db->Query("SELECT * FROM KELOMPOK_BIAYA WHERE ID_KELOMPOK_BIAYA IN (
		  SELECT ID_KELOMPOK_BIAYA 
		  FROM BIAYA_KULIAH 
		  WHERE ID_BIAYA_KULIAH IN (
			SELECT ID_BIAYA_KULIAH 
			FROM BIAYA_KULIAH_CMHS 
			WHERE ID_C_MHS='{$cmb['ID_C_MHS']}'
		  )
		)");
        $kelompok_biaya_cmb = $this->db->FetchAssoc();
        $tgl_cetak = strftime('%d %B %Y');
        $dir_keu = $this->GetDirekturKeuangan();
        $verifikator = $this->GetVerifikatorKeuangan($id_c_mhs);

        // Mengelompokkan data pembayaran
        // Document Properties
        $pdf->SetSubject("Invoice Keuangan Calon Mahasiswa Baru");
        $pdf->SetCreator("Cyber Campus Universitas Airlangga");
        $pdf->SetAuthor("Universitas Airlangga");
        $pdf->SetTitle("Invoice Keuangan Calon Mahasiswa Baru");

        // Import Font
        $pdf->AddFont('Calibri', '', 'calibri.php');
        $pdf->AddFont('Calibri', 'B', 'calibrib.php');
        $pdf->AddFont('Monotype Corsiva', 'I', 'MTCORSVA.php');


        for ($i = 1; $i <= 2; $i++) { // diulang 2x
            // create page
            $pdf->AddPage('P', 'A4');

            // debugging
            $border = 0;

            // Logo unair
            $pdf->Image("/var/www/html/modul/registrasi/img/logo-unair.png", 15, 15, 20, 20);

            // Judul Invoice
            $pdf->SetX(160);
            $pdf->SetFont('Calibri', 'B', 22);
            $pdf->Cell(0, 7, "INVOICE", $border, true);
            $pdf->SetX(160);
            $pdf->SetFont('Calibri', '', 14);
            $pdf->Cell(0, 7, "NO. {$cmb['NO_INVOICE']}", $border, true);
            $pdf->SetX(160);
            $pdf->SetFont('Calibri', '', 11);
            $pdf->Cell(0, 5, $i == 1 ? "Petugas Verifikasi" : "Mahasiswa", true, true);

            // Header Universitas Airlangga
            $pdf->SetXY(40, 15);
            $pdf->SetFont('Calibri', 'B', 18);
            $pdf->Cell(0, 6, "UNIVERSITAS AIRLANGGA", $border, true);
            $pdf->SetX(40);
            $pdf->SetFont('Monotype Corsiva', 'I', 16);
            $pdf->Cell(0, 6, "Excellence with morality", $border, true);
            $pdf->SetX(40);
            $pdf->SetFont('Calibri', '', 12);
            $pdf->Cell(0, 6, "http://www.unair.ac.id", $border, true);

            $pdf->Ln();

            $tempY = $pdf->GetY();

            // Keterangan mahasiswa
            $pdf->SetFont('Calibri', '', 11);
            $pdf->MultiCell(0, 5, "Nama : {$cmb['NM_C_MHS']}\nCamaba Fakultas : {$cmb['NM_FAKULTAS']} \nCamaba Prodi : {$cmb['NM_JENJANG']} {$cmb['NM_PROGRAM_STUDI']}\nUNIVERSITAS AIRLANGGA\nSURABAYA", $border);

            // Keterangan Kelompok tes
            $pdf->SetXY(125, $tempY);
            $pdf->MultiCell(0, 5, "Nomor Test\nKelompok\nTanggal Cetak\n", $border);
            $pdf->SetXY(160, $tempY);
            if ($cmb['STATUS_BIDIK_MISI'] != '1') {
                $pdf->MultiCell(0, 5, ": {$cmb['NO_UJIAN']}\n: {$kelompok_biaya_cmb['NM_KELOMPOK_BIAYA']}\n: {$tgl_cetak}\n", $border);
            } else {
                $pdf->MultiCell(0, 5, ": {$cmb['NO_UJIAN']}\n: Bidik Misi\n: {$tgl_cetak}\n", $border);
            }


            $pdf->Ln(5);
            // Kolom Rincian Pembayaran
            //$pdf->SetXY($x, $y)
            $pdf->SetFillColor(0, 0, 0);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->SetFont('Calibri', 'B', '14');
            $pdf->Cell(95, 6, "RINCIAN PEMBAYARAN", $border, false, 'C', true);

            $pdf->SetX($pdf->GetX() + 1);
            $pdf->Cell(40, 6, "JUMLAH (Rp)", $border, false, 'C', true);

            $pdf->SetX($pdf->GetX() + 2);
            $pdf->Cell(0, 6, "METODE PEMBAYARAN", $border, true, 'C', true);

            $pdf->Ln(1);

            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Calibri', '', 11);
            $top_no_1 = $pdf->GetY();

            $pdf->Cell(95, 6, "1. Uang Kuliah", $border, true);

            $pdf->SetXY(108, $top_no_1);
            $pdf->Cell(8, 6, "Rp.", $border, false);
            // Untuk Bidik Misi
            if ($cmb['STATUS_BIDIK_MISI'] != '1') {
                $pdf->Cell(30, 6, number_format($pembayaran['TOTAL_2'], null, null, '.'), $border, true, 'R');
            } else {
                $pdf->Cell(30, 6, 'Bidik Misi (*)', $border, true, 'R');
            }

            // Total
            $pdf->SetY(140);
            $pdf->SetFillColor(0, 0, 0);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->SetFont('Calibri', 'B', '11');
            $pdf->Cell(95, 6, "TOTAL   ", $border, false, 'R', true);

            $pdf->SetX($pdf->GetX() + 1);
            $pdf->Cell(8, 6, "Rp.", $border, false, 'L', true);
            //Untuk Bidik Misi
            if ($cmb['STATUS_BIDIK_MISI'] != '1') {
                $pdf->Cell(32, 6, number_format($pembayaran['TOTAL'], null, null, '.'), $border, true, 'R', true);
            } else {
                $pdf->Cell(32, 6, 'Bidik Misi (*)', $border, true, 'R', true);
            }



            // Keterangan Metode Pembayaran
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Calibri', '', 11);
            $pdf->SetXY(148, $top_no_1);
            $pdf->MultiCell(0, 5, "Pembayaran dilakukan melalui mekanisme Host to Host pada bank persepsi yaitu :\n1. Bank Mandiri\n2. Bank BNI\n3. Bank BRI\n4. Bank BTN\n5. Bank BNI Syariah\n6. Bank Syariah Mandiri\nmelalui\n\n  - Direct Debet\n  - Internet Banking\n  - Teller\nPembayaran diluar sistem dianggap belum membayar.", 1, 'L');


            $pdf->Image('/var/www/html/img/keuangan/ttd-dir-keu.png', 20, 170, 43, 27, 'PNG');

            $tempY = $pdf->GetY();

            $pdf->SetY($tempY + 20);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Calibri', '', 11);
            $pdf->MultiCell(65, 5, "Mengetahui,\nDirektur Keuangan\n\n\n\n{$dir_keu['NM_PENGGUNA']}\nNIP. {$dir_keu['NIP_DOSEN']}", $border, 'C');
            // PERUBAHAN UNTUK SBMPTN
            /*
              $pdf->SetXY(-75, $tempY + 20);
              $pdf->MultiCell(65, 5, "Surabaya,\nVerifikator\n\n\n\n{$verifikator['NM_PENGGUNA']}\nNIP / NIK {$verifikator['USERNAME']}", $border, 'C');
             */
            $pdf->Ln(5);

            $pdf->SetFont('Calibri', '', 9);
            $pdf->Cell(0, 5, "Catatan :", $border, true);
            $pdf->Cell(0, 5, "1. Apabila Saudara telah melakukan pembayaran / memenuhi semua kewajiban saudara abaikan tagihan / invoice ini ", $border, true);
            $pdf->Cell(0, 5, "2. Keterangan (*) ditanggung Negara", $border, true);
        }

        $pdf->SetDisplayMode('real', 'continuous');
        $pdf->Output('invoice.pdf', 'I');
        exit();
    }

    // Tambahan Dari Nambi

    function PrintPdfKeuanganMaba($id_c_mhs, FPDF &$pdf) {
        $cmb = $this->GetData($id_c_mhs, true);
        $pembayaran = $this->LoadPembayaranCalonMahasiswa($id_c_mhs);
        $this->db->Query("SELECT * FROM KELOMPOK_BIAYA WHERE ID_KELOMPOK_BIAYA IN (
		  SELECT ID_KELOMPOK_BIAYA 
		  FROM BIAYA_KULIAH 
		  WHERE ID_BIAYA_KULIAH IN (
			SELECT ID_BIAYA_KULIAH 
			FROM BIAYA_KULIAH_CMHS 
			WHERE ID_C_MHS='{$cmb['ID_C_MHS']}'
		  )
		)");
        $kelompok_biaya_cmb = $this->db->FetchAssoc();
        $tgl_cetak = strftime('%d %B %Y');
        $dir_keu = $this->GetDirekturKeuangan();
        $verifikator = $this->GetVerifikatorKeuangan($id_c_mhs);

        // Mengelompokkan data pembayaran
        // Document Properties
        $pdf->SetSubject("Invoice Keuangan Calon Mahasiswa Baru");
        $pdf->SetCreator("Cyber Campus Universitas Airlangga");
        $pdf->SetAuthor("Universitas Airlangga");
        $pdf->SetTitle("Invoice Keuangan Calon Mahasiswa Baru");

        // Import Font
        $pdf->AddFont('Calibri', '', 'calibri.php');
        $pdf->AddFont('Calibri', 'B', 'calibrib.php');
        $pdf->AddFont('Monotype Corsiva', 'I', 'MTCORSVA.php');



        // create page
        $pdf->AddPage('P', 'A4');

        // debugging
        $border = 0;

        // Logo unair
        $pdf->Image("/var/www/html/modul/registrasi/img/logo-unair.png", 15, 15, 20, 20);

        // Judul Invoice
        $pdf->SetX(160);
        $pdf->SetFont('Calibri', 'B', 22);
        $pdf->Cell(0, 7, "INVOICE", $border, true);
        $pdf->SetX(160);
        $pdf->SetFont('Calibri', '', 14);
        $pdf->Cell(0, 7, "NO. {$cmb['NO_INVOICE']}", $border, true);
        $pdf->SetX(160);
        $pdf->SetFont('Calibri', '', 11);
        $pdf->Cell(0, 5, "Mahasiswa", true, true);

        // Header Universitas Airlangga
        $pdf->SetXY(40, 15);
        $pdf->SetFont('Calibri', 'B', 18);
        $pdf->Cell(0, 6, "UNIVERSITAS AIRLANGGA", $border, true);
        $pdf->SetX(40);
        $pdf->SetFont('Monotype Corsiva', 'I', 16);
        $pdf->Cell(0, 6, "Excellence with morality", $border, true);
        $pdf->SetX(40);
        $pdf->SetFont('Calibri', '', 12);
        $pdf->Cell(0, 6, "http://www.unair.ac.id", $border, true);

        $pdf->Ln();

        $tempY = $pdf->GetY();

        // Keterangan mahasiswa
        $pdf->SetFont('Calibri', '', 11);
        $pdf->MultiCell(0, 5, "Nama : {$cmb['NM_C_MHS']}\nCamaba Fakultas : {$cmb['NM_FAKULTAS']} \nCamaba Prodi : {$cmb['NM_JENJANG']} {$cmb['NM_PROGRAM_STUDI']}\nUNIVERSITAS AIRLANGGA\nSURABAYA", $border);

        // Keterangan Kelompok tes
        $pdf->SetXY(125, $tempY);
        $pdf->MultiCell(0, 5, "Nomor Test\nKelompok\nTanggal Cetak\n", $border);
        $pdf->SetXY(160, $tempY);
        if ($cmb['STATUS_BIDIK_MISI'] != '1') {
            $pdf->MultiCell(0, 5, ": {$cmb['NO_UJIAN']}\n: {$kelompok_biaya_cmb['NM_KELOMPOK_BIAYA']}\n: {$tgl_cetak}\n", $border);
        } else {
            $pdf->MultiCell(0, 5, ": {$cmb['NO_UJIAN']}\n: Bidik Misi\n: {$tgl_cetak}\n", $border);
        }


        $pdf->Ln(5);
        // Kolom Rincian Pembayaran
        //$pdf->SetXY($x, $y)
        $pdf->SetFillColor(0, 0, 0);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->SetFont('Calibri', 'B', '14');
        $pdf->Cell(95, 6, "RINCIAN PEMBAYARAN", $border, false, 'C', true);

        $pdf->SetX($pdf->GetX() + 1);
        $pdf->Cell(40, 6, "JUMLAH (Rp)", $border, false, 'C', true);

        $pdf->SetX($pdf->GetX() + 2);
        $pdf->Cell(0, 6, "METODE PEMBAYARAN", $border, true, 'C', true);

        $pdf->Ln(1);

        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Calibri', '', 11);
        $top_no_1 = $pdf->GetY();

        $pdf->Cell(95, 6, "1. Uang Kuliah", $border, true);

        $pdf->SetXY(108, $top_no_1);
        $pdf->Cell(8, 6, "Rp.", $border, false);
        // Untuk Bidik Misi
        if ($cmb['STATUS_BIDIK_MISI'] != '1') {
            $pdf->Cell(30, 6, number_format($pembayaran['TOTAL_2'], null, null, '.'), $border, true, 'R');
        } else {
            $pdf->Cell(30, 6, 'Bidik Misi (*)', $border, true, 'R');
        }

        // Total
        $pdf->SetY(140);
        $pdf->SetFillColor(0, 0, 0);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->SetFont('Calibri', 'B', '11');
        $pdf->Cell(95, 6, "TOTAL   ", $border, false, 'R', true);

        $pdf->SetX($pdf->GetX() + 1);
        $pdf->Cell(8, 6, "Rp.", $border, false, 'L', true);
        //Untuk Bidik Misi
        if ($cmb['STATUS_BIDIK_MISI'] != '1') {
            $pdf->Cell(32, 6, number_format($pembayaran['TOTAL'], null, null, '.'), $border, true, 'R', true);
        } else {
            $pdf->Cell(32, 6, 'Bidik Misi (*)', $border, true, 'R', true);
        }



        // Keterangan Metode Pembayaran
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Calibri', '', 11);
        $pdf->SetXY(148, $top_no_1);
        $pdf->MultiCell(0, 5, "Pembayaran dilakukan melalui mekanisme Host to Host pada bank persepsi yaitu :\n1. Bank Mandiri\n2. Bank BNI\n3. Bank BRI\n4. Bank BTN\n5. Bank BNI Syariah\n6. Bank Syariah Mandiri\nmelalui\n\n  - Direct Debet\n  - Internet Banking\n  - Teller\nPembayaran diluar sistem dianggap belum membayar.", 1, 'L');


        $pdf->Image('/var/www/html/img/keuangan/ttd-dir-keu.png', 20, 170, 43, 27, 'PNG');

        $tempY = $pdf->GetY();

        $pdf->SetY($tempY + 20);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Calibri', '', 11);
        $pdf->MultiCell(65, 5, "Mengetahui,\nDirektur Keuangan\n\n\n\n{$dir_keu['NM_PENGGUNA']}\nNIP. {$dir_keu['NIP_DOSEN']}", $border, 'C');
        // PERUBAHAN UNTUK SBMPTN
        /*
          $pdf->SetXY(-75, $tempY + 20);
          $pdf->MultiCell(65, 5, "Surabaya,\nVerifikator\n\n\n\n{$verifikator['NM_PENGGUNA']}\nNIP / NIK {$verifikator['USERNAME']}", $border, 'C');
         */
        $pdf->Ln(5);

        $pdf->SetFont('Calibri', '', 9);
        $pdf->Cell(0, 5, "Catatan :", $border, true);
        $pdf->Cell(0, 5, "1. Apabila Saudara telah melakukan pembayaran / memenuhi semua kewajiban saudara abaikan tagihan / invoice ini ", $border, true);
        $pdf->Cell(0, 5, "2. Keterangan (*) ditanggung Negara", $border, true);



        $pdf->SetDisplayMode('real', 'continuous');
        $pdf->Output('invoice.pdf', 'I');
        exit();
    }

    function PrintPdfKeuanganVerifikator($id_c_mhs, FPDF &$pdf) {
        $cmb = $this->GetData($id_c_mhs, true);
        $pembayaran = $this->LoadPembayaranCalonMahasiswa($id_c_mhs);
        $this->db->Query("SELECT * FROM KELOMPOK_BIAYA WHERE ID_KELOMPOK_BIAYA IN (
		  SELECT ID_KELOMPOK_BIAYA 
		  FROM BIAYA_KULIAH 
		  WHERE ID_BIAYA_KULIAH IN (
			SELECT ID_BIAYA_KULIAH 
			FROM BIAYA_KULIAH_CMHS 
			WHERE ID_C_MHS='{$cmb['ID_C_MHS']}'
		  )
		)");
        $kelompok_biaya_cmb = $this->db->FetchAssoc();
        $tgl_cetak = strftime('%d %B %Y');
        $dir_keu = $this->GetDirekturKeuangan();
        $verifikator = $this->GetVerifikatorKeuangan($id_c_mhs);

        // Mengelompokkan data pembayaran
        // Document Properties
        $pdf->SetSubject("Invoice Keuangan Calon Mahasiswa Baru");
        $pdf->SetCreator("Cyber Campus Universitas Airlangga");
        $pdf->SetAuthor("Universitas Airlangga");
        $pdf->SetTitle("Invoice Keuangan Calon Mahasiswa Baru");

        // Import Font
        $pdf->AddFont('Calibri', '', 'calibri.php');
        $pdf->AddFont('Calibri', 'B', 'calibrib.php');
        $pdf->AddFont('Monotype Corsiva', 'I', 'MTCORSVA.php');



        // create page
        $pdf->AddPage('P', 'A4');

        // debugging
        $border = 0;

        // Logo unair
        $pdf->Image("/var/www/html/modul/registrasi/img/logo-unair.png", 15, 15, 20, 20);

        // Judul Invoice
        $pdf->SetX(160);
        $pdf->SetFont('Calibri', 'B', 22);
        $pdf->Cell(0, 7, "INVOICE", $border, true);
        $pdf->SetX(160);
        $pdf->SetFont('Calibri', '', 14);
        $pdf->Cell(0, 7, "NO. {$cmb['NO_INVOICE']}", $border, true);
        $pdf->SetX(160);
        $pdf->SetFont('Calibri', '', 11);
        $pdf->Cell(0, 5, "Verifikator", true, true);

        // Header Universitas Airlangga
        $pdf->SetXY(40, 15);
        $pdf->SetFont('Calibri', 'B', 18);
        $pdf->Cell(0, 6, "UNIVERSITAS AIRLANGGA", $border, true);
        $pdf->SetX(40);
        $pdf->SetFont('Monotype Corsiva', 'I', 16);
        $pdf->Cell(0, 6, "Excellence with morality", $border, true);
        $pdf->SetX(40);
        $pdf->SetFont('Calibri', '', 12);
        $pdf->Cell(0, 6, "http://www.unair.ac.id", $border, true);

        $pdf->Ln();

        $tempY = $pdf->GetY();

        // Keterangan mahasiswa
        $pdf->SetFont('Calibri', '', 11);
        $pdf->MultiCell(0, 5, "Nama : {$cmb['NM_C_MHS']}\nCamaba Fakultas : {$cmb['NM_FAKULTAS']} \nCamaba Prodi : {$cmb['NM_JENJANG']} {$cmb['NM_PROGRAM_STUDI']}\nUNIVERSITAS AIRLANGGA\nSURABAYA", $border);

        // Keterangan Kelompok tes
        $pdf->SetXY(125, $tempY);
        $pdf->MultiCell(0, 5, "Nomor Test\nKelompok\nTanggal Cetak\n", $border);
        $pdf->SetXY(160, $tempY);
        if ($cmb['STATUS_BIDIK_MISI'] != '1') {
            $pdf->MultiCell(0, 5, ": {$cmb['NO_UJIAN']}\n: {$kelompok_biaya_cmb['NM_KELOMPOK_BIAYA']}\n: {$tgl_cetak}\n", $border);
        } else {
            $pdf->MultiCell(0, 5, ": {$cmb['NO_UJIAN']}\n: Bidik Misi\n: {$tgl_cetak}\n", $border);
        }


        $pdf->Ln(5);
        // Kolom Rincian Pembayaran
        //$pdf->SetXY($x, $y)
        $pdf->SetFillColor(0, 0, 0);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->SetFont('Calibri', 'B', '14');
        $pdf->Cell(95, 6, "RINCIAN PEMBAYARAN", $border, false, 'C', true);

        $pdf->SetX($pdf->GetX() + 1);
        $pdf->Cell(40, 6, "JUMLAH (Rp)", $border, false, 'C', true);

        $pdf->SetX($pdf->GetX() + 2);
        $pdf->Cell(0, 6, "METODE PEMBAYARAN", $border, true, 'C', true);

        $pdf->Ln(1);

        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Calibri', '', 11);
        $top_no_1 = $pdf->GetY();

        $pdf->Cell(95, 6, "1. Uang Kuliah", $border, true);

        $pdf->SetXY(108, $top_no_1);
        $pdf->Cell(8, 6, "Rp.", $border, false);
        // Untuk Bidik Misi
        if ($cmb['STATUS_BIDIK_MISI'] != '1') {
            $pdf->Cell(30, 6, number_format($pembayaran['TOTAL_2'], null, null, '.'), $border, true, 'R');
        } else {
            $pdf->Cell(30, 6, 'Bidik Misi (*)', $border, true, 'R');
        }

        // Total
        $pdf->SetY(140);
        $pdf->SetFillColor(0, 0, 0);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->SetFont('Calibri', 'B', '11');
        $pdf->Cell(95, 6, "TOTAL   ", $border, false, 'R', true);

        $pdf->SetX($pdf->GetX() + 1);
        $pdf->Cell(8, 6, "Rp.", $border, false, 'L', true);
        //Untuk Bidik Misi
        if ($cmb['STATUS_BIDIK_MISI'] != '1') {
            $pdf->Cell(32, 6, number_format($pembayaran['TOTAL'], null, null, '.'), $border, true, 'R', true);
        } else {
            $pdf->Cell(32, 6, 'Bidik Misi (*)', $border, true, 'R', true);
        }



        // Keterangan Metode Pembayaran
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Calibri', '', 11);
        $pdf->SetXY(148, $top_no_1);
        $pdf->MultiCell(0, 5, "Pembayaran dilakukan melalui mekanisme Host to Host pada bank persepsi yaitu :\n1. Bank Mandiri\n2. Bank BNI\n3. Bank BRI\n4. Bank BTN\n5. Bank BNI Syariah\n6. Bank Syariah Mandiri\nmelalui\n\n  - Direct Debet\n  - Internet Banking\n  - Teller\nPembayaran diluar sistem dianggap belum membayar.", 1, 'L');


        $pdf->Image('/var/www/html/img/keuangan/ttd-dir-keu.png', 20, 170, 43, 27, 'PNG');

        $tempY = $pdf->GetY();

        $pdf->SetY($tempY + 20);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Calibri', '', 11);
        $pdf->MultiCell(65, 5, "Mengetahui,\nDirektur Keuangan\n\n\n\n{$dir_keu['NM_PENGGUNA']}\nNIP. {$dir_keu['NIP_DOSEN']}", $border, 'C');
        // PERUBAHAN UNTUK SBMPTN
        /*
          $pdf->SetXY(-75, $tempY + 20);
          $pdf->MultiCell(65, 5, "Surabaya,\nVerifikator\n\n\n\n{$verifikator['NM_PENGGUNA']}\nNIP / NIK {$verifikator['USERNAME']}", $border, 'C');
         */
        $pdf->Ln(5);

        $pdf->SetFont('Calibri', '', 9);
        $pdf->Cell(0, 5, "Catatan :", $border, true);
        $pdf->Cell(0, 5, "1. Apabila Saudara telah melakukan pembayaran / memenuhi semua kewajiban saudara abaikan tagihan / invoice ini ", $border, true);
        $pdf->Cell(0, 5, "2. Keterangan (*) ditanggung Negara", $border, true);



        $pdf->SetDisplayMode('real', 'continuous');
        $pdf->Output('invoice.pdf', 'I');
        exit();
    }

    function LoadRange($jenis) {
        $query = "
            SELECT * 
            FROM RANGE_JUMLAH
            WHERE ID_RANGE_JENIS='{$jenis}'
            ORDER BY NILAI_SKOR
            ";
        return $this->db->QueryToArray($query);
    }

    function LoadPekerjaan() {
        $query = "
            SELECT * 
            FROM PEKERJAAN
            WHERE STATUS_AKTIF=1
            ORDER BY NM_PEKERJAAN
            ";
        return $this->db->QueryToArray($query);
    }

    function GetRangebyData($data, $jenis) {
        $result = '';
        $data_range = $this->db->QueryToArray("SELECT * FROM RANGE_JUMLAH WHERE ID_RANGE_JENIS='{$jenis}' ORDER BY NILAI_SKOR");
        foreach ($data_range as $d) {
            if ($d['BATAS_ATAS'] != '' and $d['BATAS_BAWAH'] != '') {
                if ($data >= $d['BATAS_BAWAH'] and $data <= $d['BATAS_ATAS']) {
                    $result = $d;
                }
            } else if ($d['BATAS_ATAS'] != '' and $d['BATAS_BAWAH'] == '') {
                if ($data <= $d['BATAS_ATAS']) {
                    $result = $d;
                }
            } else if ($d['BATAS_ATAS'] == '' and $d['BATAS_BAWAH'] != '') {
                if ($data >= $d['BATAS_BAWAH']) {
                    $result = $d;
                }
            }
        }
        return $result;
    }

    function GetRangeSkor($id) {
        return $this->db->QuerySingle("SELECT NILAI_SKOR FROM RANGE_JUMLAH WHERE ID_RANGE_JUMLAH='{$id}'");
    }

    function GetSkorPekerjaan($id) {
        return $this->db->QuerySingle("SELECT NILAI_SKOR FROM PEKERJAAN WHERE ID_PEKERJAAN='{$id}'");
    }

    function GetSkorKendaraan($jumlah) {
        $nilai_skor = '';
        if ($jumlah == 1) {
            $nilai_skor = 1;
        } else if ($jumlah == 2) {
            $nilai_skor = 2;
        } else if ($jumlah >= 3) {
            $nilai_skor = 3;
        }
        return $nilai_skor;
    }

    function GetSkorHandPhone($jumlah) {
        $nilai_skor = '';
        $jumlah = intval($jumlah);
        if ($jumlah == 1) {
            $nilai_skor = 2;
        } else if ($jumlah >= 2) {
            $nilai_skor = 4;
        }
        return $nilai_skor;
    }

    function AutoVerifikasiKelompokBiaya($id_c_mhs) {
        $this->db->Query("SELECT * FROM CALON_MAHASISWA_ORTU WHERE ID_C_MHS='{$id_c_mhs}'");
        $cmo = $this->db->FetchAssoc();
        $s_penghasilan = $this->GetRangeSkor($cmo['RANGE_PENGHASILAN']);
        $s_pekerjaan_ayah = $this->GetSkorPekerjaan($cmo['PEKERJAAN_AYAH']);
        $s_pekerjaan_ibu = $this->GetSkorPekerjaan($cmo['PEKERJAAN_IBU']);
        if ($s_pekerjaan_ayah > $s_pekerjaan_ibu) {
            $s_pekerjaan = $s_pekerjaan_ayah;
        } else {
            $s_pekerjaan = $s_pekerjaan_ibu;
        }

        $s_njop = $this->GetRangeSkor($cmo['RANGE_NJOP']);
        $s_listrik = $this->GetRangeSkor($cmo['RANGE_LISTRIK']);
        $s_air = $this->GetRangeSkor($cmo['RANGE_AIR']);
        $s_pbb = $this->GetRangeSkor($cmo['RANGE_PBB']);
        $s_asset = $this->GetSkorKendaraan($cmo['KENDARAAN_R2'] + $cmo['KENDARAAN_R4']);
        // Otomatis Verifikasi Dari Menggunakan Skor 28-MEI-2013
        $n_penghasilan = 0.3 * $s_penghasilan;
        $n_pekerjaan = 0.3 * $s_pekerjaan;
        $n_njop = 0.075 * $s_njop;
        $n_listrik = 0.05 * $s_listrik;
        $n_air = 0.05 * $s_air;
        $n_pbb = 0.075 * $s_pbb;
        $n_asset = 0.15 * $s_asset;
        $total_n = $n_penghasilan + $n_pekerjaan + $n_njop + $n_listrik + $n_air + $n_pbb + $n_asset;
        //echo $s_penghasilan . "+" . $s_pekerjaan . "+" . $s_njop . "+" . $s_listrik . "+" . $s_air . "+" . $s_pbb . "+" . $s_asset . "=" . $total_n . "<br/>";
        //echo $n_penghasilan . "+" . $n_pekerjaan . "+" . $n_njop . "+" . $n_listrik . "+" . $n_air . "+" . $n_pbb . "+" . $n_asset . "=" . $total_n;
        // Pemilihan Kelompok Biaya
        if ($total_n <= 1 and $total_n >= 0) {
            $kelompok = 265;
        } else if ($total_n <= 2 and $total_n >= 1) {
            $kelompok = 266;
        } else if ($total_n <= 3 and $total_n >= 2) {
            $kelompok = 267;
        } else if ($total_n <= 4 and $total_n >= 3) {
            $kelompok = 268;
        } else if ($total_n <= 5 and $total_n >= 4) {
            $kelompok = 269;
        } else if ($total_n > 5) {
            $kelompok = 270;
        }
        return $kelompok;
    }

    function GetKelompokBiayaMaba($id) {
        $this->db->Query("
            SELECT KB.*
            FROM CALON_MAHASISWA_BARU CMB
            JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA=CMB.ID_KELOMPOK_BIAYA
            WHERE CMB.ID_C_MHS='{$id}'");
        return $this->db->FetchAssoc();
    }

    function GetTextRange($id) {
        $text = "";
        $this->db->Query("SELECT * FROM RANGE_JUMLAH WHERE ID_RANGE_JUMLAH='{$id}'");
        $range = $this->db->FetchAssoc();
        if ($range['BATAS_BAWAH'] == '' and $range['BATAS_ATAS'] != '') {
            $text = "kurang dari/sama dengan " . number_format($range['BATAS_ATAS'], 0, ',', '.');
        } else if ($range['BATAS_BAWAH'] != '' and $range['BATAS_ATAS'] == '') {
            $text = "lebih dari " . number_format($range['BATAS_BAWAH'], 0, ',', '.');
        } else {
            $text = number_format($range['BATAS_BAWAH'], 0, ',', '.') . ' - ' . number_format($range['BATAS_ATAS'], 0, ',', '.');
        }
        return $text;
    }

    function UpdateJadwalVerifikasi($id_c_mhs, $post) {
        $this->db->Query("update calon_mahasiswa_data set id_jadwal_verifikasi_keuangan = {$post['id_jadwal_verifikasi_keuangan']}
                        where id_c_mhs = {$id_c_mhs}");

        $this->db->Query("update calon_mahasiswa_data set id_jadwal_verifikasi_pend = (
						select id_jadwal_verifikasi_pend from (
						select j.id_jadwal_verifikasi_pend, row_number() OVER (order by j.TGL_REGISTRASI, j.JAM_AWAL, j.JAM_AKHIR asc) as peringkat
                        from jadwal_verifikasi_pendidikan j
                            join calon_mahasiswa_baru cmb on cmb.id_penerimaan = j.id_penerimaan
                            join calon_mahasiswa_data cmd on cmd.id_c_mhs = cmb.id_c_mhs
                            join jadwal_verifikasi_keuangan jvk on jvk.id_jadwal = cmd.id_jadwal_verifikasi_keuangan
                        where cmb.id_c_mhs = {$id_c_mhs} and j.tgl_registrasi > jvk.tgl_jadwal
                            and (select count(id_c_mhs) from calon_mahasiswa_data cmd 
                            where cmd.id_jadwal_verifikasi_pend = j.id_jadwal_verifikasi_pend) < j.kuota
                            ) 							
							where peringkat = 1)
                        where id_c_mhs = {$id_c_mhs}");
    }

}

?>