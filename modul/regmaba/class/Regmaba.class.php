<?php
/**
 * Description of Regmaba
 *
 * @author Fathoni
 */
class Regmaba
{
    private $db;
    
    function __construct(MyOracle $db)
    {
        $this->db = $db;
    }
    
    function GetListProdiRegmaba($id_penerimaan)
    {
        return $this->db->QueryToArray("
            select cmb.id_program_studi, nm_jenjang||' '||nm_program_studi as nm_program_studi, diterima, count(cmb.id_c_mhs) jumlah
            from calon_mahasiswa_baru cmb
            join program_studi ps on ps.id_program_studi = cmb.id_program_studi
            join jenjang j on j.id_jenjang = ps.id_jenjang
			join (
				select id_program_studi, id_penerimaan, count(id_c_mhs) diterima from calon_mahasiswa_baru
				group by id_program_studi, id_penerimaan) diterima on diterima.id_program_studi = cmb.id_program_studi and diterima.id_penerimaan = cmb.id_penerimaan
            where cmb.id_penerimaan = {$id_penerimaan} and tgl_regmaba is not null
            group by cmb.id_program_studi, nm_jenjang||' '||nm_program_studi, diterima
            order by 2");
    }
    
    function GetListTanggalRegmaba($id_penerimaan)
    {
        return $this->db->QueryToArray("
            select to_char(tgl_regmaba,'YYYY-MM-DD') as tgl_regmaba, count(id_c_mhs) as jumlah
            from calon_mahasiswa_baru
            where id_penerimaan = {$id_penerimaan} and tgl_diterima is not null and tgl_regmaba is not null
            group by to_char(tgl_regmaba,'YYYY-MM-DD')
            order by 1");
    }
    
    function GetListDataRegmaba($id_penerimaan)
    {
        return $this->db->QueryToArray("
            select id_program_studi, to_char(tgl_regmaba,'YYYY-MM-DD') as tgl_regmaba, count(cmb.id_c_mhs) as jumlah
            from calon_mahasiswa_baru cmb
            where id_penerimaan = {$id_penerimaan} and tgl_diterima is not null and tgl_regmaba is not null
            group by id_program_studi, to_char(tgl_regmaba,'YYYY-MM-DD')");
    }
}

?>
