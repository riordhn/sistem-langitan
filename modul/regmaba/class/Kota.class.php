<?php

/**
 * Description of Kota
 *
 * @author User
 */
class Kota {

    private $db;

    function __construct(MyOracle $db) {
        $this->db = $db;
    }

    function GetListNegara() {
        // Query dibuat agar indonesia jadi nomer 1
        return $this->db->QueryToArray("
            select id_negara, upper(nm_negara) as nm_negara from negara order by nm_negara");
    }

    function GetListNegaraIndonesia() {
        // Query dibuat agar indonesia jadi nomer 1
        return $this->db->QueryToArray("
            select id_negara, upper(nm_negara) as nm_negara from negara where id_negara = 114
			order by nm_negara");
    }

    function GetListNegaraAsing() {
        // Query dibuat agar indonesia jadi nomer 1
        return $this->db->QueryToArray("
            select id_negara, upper(nm_negara) as nm_negara from negara where id_negara <> 114
			order by nm_negara");
    }

    function GetListProvinsi($id_negara) {
        return $this->db->QueryToArray("
            select id_provinsi, upper(nm_provinsi) as nm_provinsi from provinsi where id_negara = {$id_negara} order by nm_provinsi");
    }

    function GetListKota($id_provinsi) {
        return $this->db->QueryToArray("
            select id_kota, upper(tipe_dati2||' '||nm_kota) as nm_kota from kota where id_provinsi = {$id_provinsi} order by nm_kota");
    }

    function GetListSekolah($id_kota) {
        return $this->db->QueryToArray("
            select id_sekolah, upper(nm_sekolah) as nm_sekolah from sekolah where id_kota = {$id_kota} order by nm_sekolah");
    }

    function GetRowKota($id_kota) {
        $this->db->Query("
            select n.id_negara, p.id_provinsi, k.id_kota from kota k
            join provinsi p on p.id_provinsi = k.id_provinsi
            join negara n on n.id_negara = p.id_negara
            where k.id_kota = {$id_kota}");
        return $this->db->FetchAssoc();
    }

    function GetRowSekolah($id_sekolah) {
        $this->db->Query("
            select n.id_negara, p.id_provinsi, k.id_kota, s.id_sekolah from sekolah s
            join kota k on k.id_kota = s.id_kota
            join provinsi p on p.id_provinsi = k.id_provinsi
            join negara n on n.id_negara = p.id_negara
            where s.id_sekolah = {$id_sekolah}");
        return $this->db->FetchAssoc();
    }

}

?>
