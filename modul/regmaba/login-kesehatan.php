<?php

include('config.php');

$mode = get('mode', 'view');
if ($request_method == 'POST') {
    if ($mode == 'login') {
        $no_ujian = post('no_ujian', '');
        $kode_voucher = post('kode_voucher', '');

        write_log("login regmaba : '{$no_ujian}' '{$kode_voucher}'");

        if ($no_ujian != '' && $kode_voucher != '') {
            // TGL_VERIFIKASI_PENDIDIKAN harus null

            $db->Parse("
                SELECT ID_C_MHS, TGL_REGMABA, TGL_VERIFIKASI_PENDIDIKAN FROM CALON_MAHASISWA_BARU CMB
                JOIN PENERIMAAN P ON P.ID_PENERIMAAN = CMB.ID_PENERIMAAN
                WHERE NO_UJIAN = :no_ujian AND KODE_VOUCHER = :kode_voucher AND
                    TGL_DITERIMA IS NOT NULL AND
                    CURRENT_DATE >= P.TGL_PENGUMUMAN");
            $db->BindByName(':no_ujian', $no_ujian);
            $db->BindByName(':kode_voucher', $kode_voucher);
            $db->Execute();

            $row = $db->FetchArray();

            if ($row) {
                $_SESSION['maba'] = $row['ID_C_MHS'];

                if ($row['TGL_VERIFIKASI_PENDIDIKAN'] != '') {
                    $_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] = $row['TGL_VERIFIKASI_PENDIDIKAN'];
                }

                // Mengupdate tanggal regmaba
                if ($row['TGL_REGMABA'] == '') {
                    $current_time = date('YmdHis');
                    $db->Query("update calon_mahasiswa_baru set tgl_regmaba = to_date('{$current_time}', 'YYYYMMDDHH24MISS') where id_c_mhs = {$row['ID_C_MHS']}");
                }

                header('Location: /');
                exit();
            }
        }
    }
}

if ($mode == 'view') {
    //header('Location: /snmptn/'); exit();
}

if ($mode == 'logout') {
	$_SESSION['maba'] = '';
	$_SESSION['ID_C_MHS'] = '';
	$_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] = '';
    // unset($_SESSION['maba']);
    // unset($_SESSION['ID_C_MHS']);
    // unset($_SESSION['TGL_VERIFIKASI_PENDIDIKAN']);
    header('Location: /');
    exit();
}

// cek login
if (!empty($_SESSION['ID_C_MHS'])) {

    $halaman = "biodata-isi.php";
    $smarty->assign('halaman', $halaman);
    $smarty->display('index.tpl');
} elseif (!empty($_SESSION['TGL_VERIFIKASI_PENDIDIKAN'])) {
    echo __LINE__; exit();
} else {
    $smarty->assign('false_login', 0);
    $smarty->display('login-kesehatan.tpl');
}
?>
