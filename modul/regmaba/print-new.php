<?php

session_start();

include '../../config.php';
include '../registrasi/class/CalonMahasiswa.class.php';
include 'class/CalonMahasiswaNambi.class.php';
include '../../includes/fpdf/fpdf.php';

if (empty($_SESSION['ID_C_MHS'])) {
    header("location: /index.php");
    exit();
}

$id_c_mhs = $_SESSION['ID_C_MHS'];
$CalonMahasiswa = new CalonMahasiswa($db);

$CalonMahasiswaNambi = new CalonMahasiswaNambi($db);
$CalonMahasiswaNambi->PrintPdf($id_c_mhs, $pdf = new FPDF(), true);

// Auto logout by Fathoni biar data ngga numpuk

$_SESSION['ID_C_MHS'] = '';
$_SESSION['JALUR'] = '';
$_SESSION['JALUR_LOGIN'] = '';
$_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] = '';
$_SESSION['TGL_VERIFIKASI_KEUANGAN'] = '';

session_destroy();

?>
