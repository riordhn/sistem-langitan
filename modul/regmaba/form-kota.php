<?php
include 'config.php';
include '../registrasi/class/Kota.class.php';

$Kota = new Kota($db);

$mode = get('mode', 'view');
$id_kota = get('id_kota', '');

if ($mode == 'view')
{
    $smarty->assign('negara_set', $Kota->GetListNegara());
    
    if ($id_kota != '')
    {
        $kota = $Kota->GetRowKota($id_kota);
        $smarty->assign('provinsi_set', $Kota->GetListProvinsi($kota['ID_NEGARA']));
        $smarty->assign('kota_set', $Kota->GetListKota($kota['ID_PROVINSI']));
        $smarty->assign('kota', $kota);
    }
}

if ($mode == 'get-provinsi')
{
    $id_negara = get('id_negara');
    
    $rows = $Kota->GetListProvinsi($id_negara);
    
    echo '<option value=""></option>';
    
    foreach ($rows as $row)
    {
        echo "<option value=\"{$row['ID_PROVINSI']}\">{$row['NM_PROVINSI']}</option>";
    }
    
    exit();
}

if ($mode == 'get-kota')
{
    $id_provinsi = get('id_provinsi');
    
    $rows = $Kota->GetListKota($id_provinsi);
    
    echo '<option value=""></option>';
    
    foreach ($rows as $row)
    {
        echo "<option value=\"{$row['ID_KOTA']}\">{$row['NM_KOTA']}</option>";
    }
    
    exit();
}

$smarty->display("form/kota.tpl");
?>
