<?php
include '../../../config.php';
include '../../ppmb/class/Penerimaan.class.php';
include '../class/Regmaba.class.php';

$smarty = new Smarty();
$smarty->setTemplateDir('../gui/');
$smarty->setCompileDir('../gui_c/');

$Penerimaan = new Penerimaan($db);
$Regmaba    = new Regmaba($db);

$id_penerimaan = get('id_penerimaan', '');

$smarty->assign('penerimaan_set', $Penerimaan->GetAllForView());

if ($id_penerimaan != '')
{
    $smarty->assign('program_studi_set', $Regmaba->GetListProdiRegmaba($id_penerimaan));
    $smarty->assign('tanggal_set', $Regmaba->GetListTanggalRegmaba($id_penerimaan));
    $smarty->assign('data_set', $Regmaba->GetListDataRegmaba($id_penerimaan));
}

$smarty->display("report/index.tpl");
?>
