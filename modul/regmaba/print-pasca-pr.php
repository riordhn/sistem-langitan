<?php
include('config.php');
include_once('../../tcpdf/config/lang/ind.php');
include_once('../../tcpdf/tcpdf.php');

// TCPDF
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$html = <<<EOF
<style>
    p { font-size: 28px;}
    td { font-size: 30px; }
	.isi{border:1px solid; border-collapse:collapse;}
	.headers{margin:20px; height:100;}
	#headers{margin:20px; height:100;}
</style>
<table width="100%" border="0" class="headers">
    <tr>
        <td width="14%"><br/><br/><br/><img src="../../img/maba/logounair.png" width="75px" height="75px" /></td>
        <td width="68%" align="center">
            <p>&nbsp;</p>
            <p><font size="14"><b>FORMULIR REGISTRASI MAHASISWA BARU PROGRAM PROFESI<br>
            TAHUN AKADEMIK 2012 / 2013</b></font></p>
            <p>&nbsp;</p>
        </td>
        <td width="18%">
            <table width="100%" border="1" class="isi" id="headers">
              <tr>
                <td align="center"><p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>4 x 6 cm</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                </td>
              </tr>
            </table>
        </td>
    </tr>
</table>
<hr/>
<p>YANG BERTANDA TANGAN DI BAWAH INI MOHON DIDAFTAR SEBAGAI MAHASISWA PADA : </p>
<table border="1" cellpadding="3">
    <tr>
        <td colspan="3" align="center">PROGRAM STUDI</td>
    </tr>
    <tr>
        <td align="center" width="30px"></td>
        <td width="160px">Program Studi</td>
        <td width="auto">Profesi {nm_program_studi}</td>
    </tr>
    <tr>
        <td align="center"></td>
        <td>Minat Studi / Konsentrasi</td>
        <td>{prodi_minat}</td>
    </tr>
    <tr>
        <td align="center"></td>
        <td>Kelas pilihan</td>
        <td>{kelas_pilihan}</td>
    </tr>
</table>
<div>&nbsp;</div><div>&nbsp;</div>
<table border="1" cellpadding="3">
    <tr>
        <td colspan="3" align="center">IDENTITAS DIRI</td>
    </tr>
    <tr>
        <td align="center" width="30px">1</td>
        <td width="160px">No Ujian</td>
        <td width="auto"><b>{no_ujian}</b></td>
    </tr>
    <tr>
        <td align="center">2</td>
        <td>Nama Lengkap</td>
        <td>{nm_c_mhs}</td>
    </tr>

    <tr>
        <td align="center">3</td>
        <td>Tempat dan Tgl Lahir</td>
        <td>{tempat_lahir}, {tgl_lahir}</td>
    </tr>
    <tr>
        <td align="center">4</td>
        <td>Jenis Kelamin</td>
        <td>{jenis_kelamin}</td>
    </tr>
    <tr>
        <td align="center">5</td>
        <td>Agama</td>
        <td>{agama}</td>
    </tr>
    <tr>
        <td align="center">6</td>
        <td>Status Perkawinan</td>
        <td>{status_perkawinan}</td>
    </tr>
    <tr>
        <td align="center">7</td>
        <td>Alamat</td>
        <td>{alamat}</td>
    </tr>
    <tr>
        <td align="center">8</td>
        <td>No Telp</td>
        <td>{telp}</td>
    </tr>
    <tr>
        <td align="center">9</td>
        <td>Email</td>
        <td>{email}</td>
    </tr>
    <tr>
        <td align="center">10</td>
        <td>Kewarganegaraan</td>
        <td>{kewarganegaraan}</td>
    </tr>
    <tr>
        <td align="center">11</td>
        <td>Pekerjaan</td>
        <td>{pekerjaan}</td>
    </tr>
    <tr>
        <td align="center">12</td>
        <td>Instansi Asal</td>
        <td>{asal_instansi}</td>
    </tr>
    <tr>
        <td align="center">13</td>
        <td>Alamat Instansi</td>
        <td>{alamat_instansi}</td>
    </tr>
    <tr>
        <td align="center">14</td>
        <td>Telp Instansi</td>
        <td>{telp_instansi}</td>
    </tr>
    <tr>
        <td align="center">15</td>
        <td>NIS / NRP / NIS</td>
        <td>{nrp}</td>
    </tr>
    <tr>
        <td align="center">16</td>
        <td>KARPEG</td>
        <td>{karpeg}</td>
    </tr>
    <tr>
        <td align="center">17</td>
        <td>Pangkat</td>
        <td>{pangkat}</td>
    </tr>
</table>
<br/><br/><br/><br/><br/><br/><br/>
<table border="1" cellpadding="3">
    <tr>
        <td colspan="3" align="center">PENDIDIKAN SARJANA (S1)</td>
    </tr>
    <tr>
        <td align="center" width="30px">18</td>
        <td width="160px">Perguruan Tinggi</td>
        <td width="auto">{ptn_s1}</td>
    </tr>
    <tr>
        <td align="center">19</td>
        <td>Status Perguruan Tinggi</td>
        <td>{status_ptn_s1}</td>
    </tr>
    <tr>
        <td align="center">20</td>
        <td>Program Studi</td>
        <td>{prodi_s1}</td>
    </tr>
    <tr>
        <td align="center">21</td>
        <td>Tanggal Masuk</td>
        <td>{tgl_masuk_s1}</td>
    </tr>
    <tr>
        <td align="center">22</td>
        <td>Tanggal Lulus</td>
        <td>{tgl_lulus_s1}</td>
    </tr>
    <tr>
        <td align="center">23</td>
        <td>Lama Studi</td>
        <td>{lama_studi_s1}</td>
    </tr>
    <tr>
        <td align="center">24</td>
        <td>Indeks Prestasi</td>
        <td>{ip_s1}</td>
    </tr>
    <tr>
        <td align="center">25</td>
        <td>Jumlah Karya Ilmiah</td>
        <td>{jumlah_karya_ilmiah}</td>
    </tr>
</table>
<br/>
<table border="1" cellpadding="3">
    <tr>
        <td colspan="3" align="center">LAIN - LAIN</td>
    </tr>
    <tr>
        <td align="center" width="30px">26</td>
        <td width="160px">Lamaran ke PPS Unair</td>
        <td width="auto">{lamaran_pps}</td>
    </tr>
    <tr>
        <td align="center">26</td>
        <td>Sumber Dana</td>
        <td>{sumber_biaya}</td>
    </tr>
</table>
<br/>
<table>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">Surabaya, {tgl_cetak}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">Tanda tangan yang bersangkutan,</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center"><u>{nm_c_mhs}</u></td>
    </tr>
</table>
EOF;

// calon_mahasiswa
$db->Query("
    SELECT
        CM.ID_C_MHS, NO_UJIAN, NM_C_MHS, K.NM_KOTA AS TEMPAT_LAHIR, TGL_LAHIR, ALAMAT, TELP, EMAIL, JENIS_KELAMIN, KEWARGANEGARAAN, KEWARGANEGARAAN_LAIN,
        A.NM_AGAMA, STATUS_PERKAWINAN,
        PS.NM_PROGRAM_STUDI, SUMBER_BIAYA
    FROM CALON_MAHASISWA CM
    LEFT JOIN KOTA K ON K.ID_KOTA = CM.ID_KOTA_LAHIR
    LEFT JOIN AGAMA A ON A.ID_AGAMA = CM.ID_AGAMA
    LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI
    WHERE ID_C_MHS = {$_SESSION['maba']}");
$cm = $db->FetchAssoc();

// calon_mahasiswa_pasca
$db->Query("
    SELECT CMP.*, PM.NM_PRODI_MINAT FROM CALON_MAHASISWA_PASCA CMP
    LEFT JOIN PRODI_MINAT PM ON PM.ID_PRODI_MINAT = CMP.ID_PRODI_MINAT
    WHERE CMP.ID_C_MHS = {$_SESSION['maba']}");
$cmp = $db->FetchAssoc();

// status perkawinan
if ($cm['STATUS_PERKAWINAN'] == 1) { $cm['STATUS_PERKAWINAN'] = "Belum Kawin"; }
else if ($cm['STATUS_PERKAWINAN'] == 2) { $cm['STATUS_PERKAWINAN'] = "Kawin"; }
else if ($cm['STATUS_PERKAWINAN'] == 3) { $cm['STATUS_PERKAWINAN'] = "Bercerai"; }
else if ($cm['STATUS_PERKAWINAN'] == 4) { $cm['STATUS_PERKAWINAN'] = "Kawin, Suami/Istri meninggal"; }

// kewarganegaraan
if ($cm['KEWARGANEGARAAN'] == 1) { $cm['KEWARGANEGARAAN'] = "Indonesia"; }
else if ($cm['KEWARGANEGARAAN'] == 2) { $cm['KEWARGANEGARAAN'] = $cm['KEWARGANEGARAAN_LAIN']; }

if ($cm['SUMBER_BIAYA'] == '5') $cm['SUMBER_BIAYA'] = 'Sendiri';
else if ($cm['SUMBER_BIAYA'] == '6') $cm['SUMBER_BIAYA'] = 'Instansi';
else if ($cm['SUMBER_BIAYA'] == '7') $cm['SUMBER_BIAYA'] = 'BPPS';

// kelas pilihan
switch ($cmp['KELAS_PILIHAN'])
{
    case 'KRUA':    $cmp['KELAS_PILIHAN'] = 'Kelas Reguler Universitas Airlangga'; break;
    case 'KKUBT':   $cmp['KELAS_PILIHAN'] = 'Kelas Kerjasama Universitas Borneo Tarakan'; break;
    case 'KKUHK':   $cmp['KELAS_PILIHAN'] = 'Kelas Kerjasama Universitas Haluoleo Kendari'; break;
    case 'KJ':      $cmp['KELAS_PILIHAN'] = 'Kelas di luar domisili (Jakarta)'; break;
    case 'KS':      $cmp['KELAS_PILIHAN'] = 'Kelas Sore'; break;
    case 'KP':      $cmp['KELAS_PILIHAN'] = 'Kelas Pagi'; break;
    case 'KAP':     $cmp['KELAS_PILIHAN'] = 'Kelas Akhir Pekan'; break;
    default:        $cmp['KELAS_PILIHAN'] = ''; break;
}

if ($cmp['STATUS_PTN_S1'] == '1') $cmp['STATUS_PTN_S1'] = 'Negeri';
else if ($cmp['STATUS_PTN_S1'] == '2') $cmp['STATUS_PTN_S1'] = 'Swasta';
else if ($cmp['STATUS_PTN_S1'] == '3') $cmp['STATUS_PTN_S1'] = 'Luar Negeri';

if ($cmp['STATUS_LAMARAN_PPS'] == '1') $cmp['STATUS_LAMARAN_PPS'] = 'Pernah, Tahun ' . $cmp['TAHUN_LAMARAN_PPS'];
else if ($cmp['STATUS_LAMARAN_PPS'] == '0') $cmp['STATUS_LAMARAN_PPS'] = 'Tidak Pernah';

// replace-replace
$html = str_replace('{nm_program_studi}', $cm['NM_PROGRAM_STUDI'], $html);
$html = str_replace('{prodi_minat}', $cmp['NM_PRODI_MINAT'], $html);
$html = str_replace('{kelas_pilihan}', $cmp['KELAS_PILIHAN'], $html);

$html = str_replace('{no_ujian}', $cm['NO_UJIAN'], $html);
$html = str_replace('{nm_c_mhs}', $cm['NM_C_MHS'], $html);
$html = str_replace('{tempat_lahir}', $cm['TEMPAT_LAHIR'], $html);
$html = str_replace('{tgl_lahir}', strftime('%d %B %Y', date_to_timestamp($cm['TGL_LAHIR'])), $html);
$html = str_replace('{jenis_kelamin}', $cm['JENIS_KELAMIN'] == 1 ? "Laki-Laki" : "Perempuan", $html);
$html = str_replace('{agama}', $cm['NM_AGAMA'], $html);
$html = str_replace('{status_perkawinan}', $cm['STATUS_PERKAWINAN'], $html);
$html = str_replace('{alamat}', $cm['ALAMAT'], $html);
$html = str_replace('{telp}', $cm['TELP'], $html);
$html = str_replace('{email}', $cm['EMAIL'], $html);
$html = str_replace('{kewarganegaraan}', $cm['KEWARGANEGARAAN'], $html);

$html = str_replace('{pekerjaan}', $cmp['PEKERJAAN'], $html);
$html = str_replace('{asal_instansi}', $cmp['ASAL_INSTANSI'], $html);
$html = str_replace('{alamat_instansi}', $cmp['ALAMAT_INSTANSI'], $html);
$html = str_replace('{telp_instansi}', $cmp['TELP_INSTANSI'], $html);
$html = str_replace('{nrp}', $cmp['NRP'], $html);
$html = str_replace('{karpeg}', $cmp['KARPEG'], $html);
$html = str_replace('{pangkat}', $cmp['PANGKAT'], $html);

$html = str_replace('{ptn_s1}', $cmp['PTN_S1'], $html);
$html = str_replace('{status_ptn_s1}', $cmp['STATUS_PTN_S1'], $html);
$html = str_replace('{prodi_s1}', $cmp['PRODI_S1'], $html);
$html = str_replace('{tgl_masuk_s1}', strftime('%d %B %Y', date_to_timestamp($cmp['TGL_MASUK_S1'])), $html);
$html = str_replace('{tgl_lulus_s1}', strftime('%d %B %Y', date_to_timestamp($cmp['TGL_LULUS_S1'])), $html);
$html = str_replace('{lama_studi_s1}', $cmp['LAMA_STUDI_S1'], $html);
$html = str_replace('{ip_s1}', $cmp['IP_S1'], $html);
$html = str_replace('{jumlah_karya_ilmiah}', $cmp['JUMLAH_KARYA_ILMIAH'], $html);

$html = str_replace('{lamaran_pps}', $cmp['STATUS_LAMARAN_PPS'], $html);
$html = str_replace('{sumber_biaya}', $cm['SUMBER_BIAYA'], $html);

$html = str_replace('{tgl_cetak}', strftime('%d %B %Y'), $html);

$pdf->AddPage();
$pdf->writeHTML($html);
$pdf->endPage();

$html = <<<EOF
<style>
    * { font-size: 30px; }
</style>
<table width="100%" border="0" cellpadding="5">
  <tr>
    <td colspan="2" align="center"><b><font size="14">SURAT - PERNYATAAN</font></b></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">Yang bertanda tangan dibawah ini, Saya : </td>
  </tr>
  <tr>
    <td width="30%">Nama</td>
    <td width="60%">: {nama}</td>
  </tr>
  <tr>
    <td>Tempat dan tanggal lahir</td>
    <td>: {tempat_lahir}, {tgl_lahir}</td>
  </tr>
  <tr>
    <td>Alamat Surabaya</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp. ................................</td>
  </tr>
  <tr>
    <td>Alamat Asal</td>
    <td>: {alamat}</td>
  </tr>
  <tr>
    <td>Pekerjaan</td>
    <td>: {pekerjaan}</td>
  </tr>
  <tr>
    <td>Alamat Rumah</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp.  ................................</td>
  </tr>
</table>

<br />
Dengan ini menyatakan bahwa : 
<p>Saya tidak pernah, sedang atau akan terlibat dalam penyalahgunaan <b>Narkotika, Alkohol, Psikotropikadan Zat Adiktif (NAPZA)</b> baik sebagai pengguna, pengedar,
produsen atau yang berkaitan dengan hal tersebut. Apabila ternyata di kemudian hari pada saat saya menuntut ilmu di Universitas Airlangga saya terlibat dan atau
terbukti terlibat dalam penyalahgunaan NAPZA sebagaimana dimaksud di atas, maka saya sanggup dan bersedia dikenakan sanksi sampai dengan dibatalkan status saya
sebagai mahasiswa Universitas Airlangga.</p>
<p>
	Demikian surat pernyataan ini saya buat dengan sebenarnya, tanpa adanya paksaan dari pihak siapapun.
</p>
<p>&nbsp;</p>
<table>
    <tr>
        <td width="33%">
            <br/><br/><br/><br/><br/><br/><br/><br/>
        </td>
        <td width="33%" align="center">
            <table border="1" width="65%">
                <tr>
                    <td><br/><br/><br/><br/>
                        Pas Foto <br/>3 x 4
                        <br/><br/><br/><br/></td>
                </tr>
            </table>
        </td>
        <td width="33%">Surabaya, .............................<br/>Hormat Saya,
            <br/><br/><br/><font size="8">Materai</font><br/><font size="8">Rp 6.000,-</font><br/><br/><br/>
            <u>{nama}</u>
        </td>
    </tr>
</table>
EOF;

    $html = str_replace('{nama}', $cm['NM_C_MHS'], $html);
    $html = str_replace('{tempat_lahir}', $cm['TEMPAT_LAHIR'], $html);
    $html = str_replace('{tgl_lahir}', strftime('%d %B %Y', date_to_timestamp($cm['TGL_LAHIR'])), $html);
    $html = str_replace('{alamat}', $cm['ALAMAT'], $html);
    $html = str_replace('{pekerjaan}', $cmp['PEKERJAAN'], $html);

    $pdf->AddPage();
    $pdf->writeHTML($html);
    $pdf->endPage();
    
    
    $html = <<<EOF
<style>
    * { font-size: 30px; }
</style>
<table width="100%" border="0" cellpadding="5">
  <tr>
    <td colspan="2" align="center"><b><font size="14">SURAT PERNYATAAN BERSEDIA MEMENUHI DAN MENTAATI KETENTUAN DAN PERATURAN SERTA KEPUTUSAN YANG BERLAKU</font><br/><font size="14">DI UNIVERSITAS AIRLANGGA</font></b></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">Yang bertanda tangan dibawah ini, Saya : </td>
  </tr>
  <tr>
    <td width="30%">Nama</td>
    <td width="60%">: {nama}</td>
  </tr>
  <tr>
    <td>Tempat dan tanggal lahir</td>
    <td>: {tempat_lahir}, {tgl_lahir}</td>
  </tr>
  <tr>
    <td>Alamat Surabaya</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp. ................................</td>
  </tr>
  <tr>
    <td>Alamat Asal</td>
    <td>: {alamat}</td>
  </tr>
  <tr>
    <td>Pekerjaan</td>
    <td>: {pekerjaan}</td>
  </tr>
  <tr>
    <td>Alamat Rumah</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp.  ................................</td>
  </tr>
</table>

<br />
<p><b>Dengan ini menyatakan bahwa sebagai mahasiswa Universitas Airlangga, saya bersedia memenuhi dan mentaati ketentuan dan peraturan serta keputusan yang
berlaku di Universitas Airlangga dan apabila saya melanggar saya bersedia dikenakan sangsi.</b></p>
<p><b>Demikian surat pernyataan ini saya buat dengan sebenarnya, tanpa adanya paksaan dari pihak siapapun.</b></p>
<p>&nbsp;</p>
<table>
    <tr>
        <td width="33%">
            <br/><br/><br/><br/><br/><br/><br/><br/>
        </td>
        <td width="33%" align="center">
            <table border="1" width="65%">
                <tr>
                    <td><br/><br/><br/><br/>
                        Pas Foto <br/>3 x 4
                        <br/><br/><br/><br/></td>
                </tr>
            </table>
        </td>
        <td width="33%">Surabaya, .............................<br/>Hormat Saya,
            <br/><br/><br/><font size="8">Materai</font><br/><font size="8">Rp 6.000,-</font><br/><br/><br/>
            <u>{nama}</u>
        </td>
    </tr>
</table>
EOF;
    
$html = str_replace('{nama}', $cm['NM_C_MHS'], $html);
$html = str_replace('{tempat_lahir}', $cm['TEMPAT_LAHIR'], $html);
$html = str_replace('{tgl_lahir}', strftime('%d %B %Y', date_to_timestamp($cm['TGL_LAHIR'])), $html);
$html = str_replace('{alamat}', $cm['ALAMAT'], $html);
$html = str_replace('{pekerjaan}', $cmp['PEKERJAAN'], $html);

$pdf->AddPage();
$pdf->writeHTML($html);
$pdf->endPage();

$pdf->Output();
?>
