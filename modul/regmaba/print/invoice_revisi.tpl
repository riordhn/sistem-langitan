<style>
	.isi{border:1px solid; border-collapse:collapse;}
	.rincian{font-size:18px;}
	.header{font-size:small;}
	.footer{font-size:small;}
</style>
<table width="100%" border="0" cellpadding="3" class="header">
  <tr>
    <td width="14%">Logo UNAIR</td>
    <td width="36%"><font size="+3"><strong>UNIVERSITAS AIRLANGGA</strong></font><br />
    <em>Excellent with morality</em><br />
    http://www.unair.ac.id
    </td>
    <td width="21%">&nbsp;</td>
    <td width="29%"><font size="+6"><strong>INVOICE</strong></font><br />
    <font size="+3"><strong>No. <u>{nomer}</u></strong></font>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>{Nama Mahasiswa}</td>
    <td>Nomor Test</td>
    <td>:</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Camaba Fakultas {Fakultas}</td>
    <td>Kelompok</td>
    <td>:</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>UNIVERSITAS AIRLANGGA</td>
    <td>Tanggal Cetak</td>
    <td>:</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>SURABAYA</td>
    <td>Tanggal Jatuh Tempo</td>
    <td>:</td>
  </tr>
</table><br />
<table width="100%" border="0" class="header">
  <tr>
    <td width="3%" bgcolor="#CCFFCC">&nbsp;</td>
    <td width="30%" bgcolor="#CCFFCC"><div align="right"><strong>RINCIAN PEMBAYARAN</strong></div></td>
    <td width="18%" bgcolor="#CCFFCC"><div align="center"></div></td>
    <td width="21%" bgcolor="#CCFFCC"><div align="center"><strong>JUMLAH (Rp)</strong></div></td>
    <td width="28%" bgcolor="#CCFFCC"><div align="center"><strong>METODE PEMBAYARAN</strong></div></td>
  </tr>
  <tr>
    <td width="3%">1.</td>
    <td width="30%">Administrasi dan Pembinaan Maba</td>
    <td width="18%">&nbsp;</td>
    <td width="21%">&nbsp;</td>
    <td width="28%" rowspan="18"><br />
	<br />
      <table width="100%" height="345" border="1" cellpadding="5">
      <tr>
        <td>Pembayaran dapat dilakukan melalui mekanisme Hos to Host pada bank persepsi yaitu : <br />
1. Bank Mandiri <br />
2. Bank BNI <br />
3. Bank BRI <br />
4. Bank BTN <br />
5. Bank BNI Syariah <br />
Melalui : <br />
- ATM <br />
- Auto Debet <br />
- Internet Banking <br />
- Teller <br />
Pembayaran diluar sistem dianggap belum membayar.</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="3%">&nbsp;</td>
    <td width="30%" class="rincian">- Biaya Administrasi</td>
    <td width="18%" class="rincian">Rp. </td>
    <td width="21%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="rincian">- Biaya Pembinaan Maba</td>
    <td class="rincian">Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="rincian">- Biaya Pengembangan Kegiatan Maba</td>
    <td class="rincian">Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="rincian">- Baya Pemb. Jati Diri Kebangsaan</td>
    <td class="rincian">Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>2.</td>
    <td class="rincian">- Biaya Jaket, Muts dan Topi</td>
    <td class="rincian">Rp.</td>
    <td>Rp.</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="rincian">- Buku Pedoman Pendidikan</td>
    <td class="rincian">Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="rincian">- Buku Informasi Kemahasiswaan</td>
    <td class="rincian">Rp. </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="rincian">- Asuransi Kecelakaan Mahasiswa</td>
    <td class="rincian">Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="rincian">- Asuransi Kesehatan</td>
    <td class="rincian">Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="rincian">- Biaya Tes Kesehatan</td>
    <td class="rincian">Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="rincian">- Kartu Tanda Mahasiswa</td>
    <td class="rincian">Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="rincian">- Kemampuan Bahasa Inggris Lanjut</td>
    <td class="rincian">Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>3.</td>
    <td>KKN dan Wisuda</td>
    <td>&nbsp;</td>
    <td>Rp.</td>
  </tr>
  <tr>
    <td>4.</td>
    <td>Sumbangan Operasional (SOP)</td>
    <td>&nbsp;</td>
    <td>Rp.</td>
  </tr>
  <tr>
    <td>5.</td>
    <td>Sumbangan Pengembagan (SP3)</td>
    <td>&nbsp;</td>
    <td>Rp.</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#CCFFCC">&nbsp;</td>
    <td bgcolor="#CCFFCC">&nbsp;</td>
    <td bgcolor="#CCFFCC"><strong>TOTAL</strong></td>
    <td bgcolor="#CCFFCC"><strong>Rp.</strong></td>
  </tr>
</table><br />
<table width="100%" cellpadding="3" class="footer">
  <tr>
    <td width="52%">Mengetahui, <br />
Direktur Keuangan
<br />
<br />
<br />
<br />
<br /> 
<u>Dr. Heru Tjaraka, SE., MSi., BKP., Ak</u>
<br />
NIP. 196709271993031003</td>
    <td width="48%">Surabaya, <br />
      Verivikator<br />
      <br />
      <br />
      <br />
      <br />
      <u>Eko Siswantoro, Drs., MM</u> <br />
NIP. 196211201988021001</td>
  </tr>
</table>