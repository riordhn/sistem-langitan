<style>
	.isi{border:1px solid; border-collapse:collapse;}
</style>
<table width="100%" border="0" cellpadding="3">
  <tr>
    <td width="14%">Logo UNAIR</td>
    <td width="36%">UNIVERSITAS AIRLANGGA<br />
      <em>Excellent with morality</em><br />
      http://www.unair.ac.id</td>
    <td width="21%">&nbsp;</td>
    <td width="29%">INVOICE<br />
      No. <u>{nomer}</u></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>{Nama Mahasiswa}</td>
    <td>Nomor Test</td>
    <td>:</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>{Fakultas}</td>
    <td>Kelompok</td>
    <td>:</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>UNIVERSITAS AIRLANGGA</td>
    <td>Tanggal Cetak</td>
    <td>:</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>SURABAYA</td>
    <td>Tanggal Jatuh Tempo</td>
    <td>:</td>
  </tr>
</table><br />
<table width="100%" border="0">
  <tr>
    <td width="3%" bgcolor="#CCFFCC">&nbsp;</td>
    <td width="33%" bgcolor="#CCFFCC"><div align="right"><strong>RINCIAN PEMBAYARAN</strong></div></td>
    <td width="18%" bgcolor="#CCFFCC"><div align="center"></div></td>
    <td width="20%" bgcolor="#CCFFCC"><div align="center"><strong>JUMLAH (Rp)</strong></div></td>
    <td width="26%" bgcolor="#CCFFCC"><div align="center"><strong>METODE PEMBAYARAN</strong></div></td>
  </tr>
  <tr>
    <td width="3%">1.</td>
    <td width="33%">Administrasi dan Pembinaan Maba</td>
    <td width="18%">&nbsp;</td>
    <td width="20%">&nbsp;</td>
    <td width="26%" rowspan="18"><br />
	<br />
      <table width="100%" border="1">
      <tr>
        <td>Pembayaran dilakukan melalui mekanisme Hos to Host pada bank persepsi yaitu : <br />
1. Bank Mandiri <br />
2. Bank BNI <br />
3. Bank BRI <br />
4. Bank BTN <br />
5. Bank BNI Syariah <br />
Melalui : <br />
- ATM <br />
- Direct Debet <br />
- Internet Banking <br />
- Teller <br />
Pembayaran diluar sistem dianggap belum membayar.</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="3%">&nbsp;</td>
    <td width="33%"><font size="9">- Biaya Administrasi</font></td>
    <td width="18%">Rp. </td>
    <td width="20%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><font size="9">- Biaya Pembinaan Maba</font></td>
    <td>Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><font size="9">- Biaya Pengembangan Kegiatan Maba</font></td>
    <td>Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><font size="9">- Baya Pemb. Jati Diri Kebangsaan</font></td>
    <td>Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>2.</td>
    <td><font size="9">- Biaya Jaket, Muts dan Topi</font></td>
    <td>Rp.</td>
    <td>Rp.</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><font size="9">- Buku Pedoman Pendidikan</font></td>
    <td>Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><font size="9">- Buku Informasi Kemahasiswaan</font></td>
    <td>Rp. </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><font size="9">- Asuransi Kecelakaan Mahasiswa</font></td>
    <td>Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><font size="9">- Asuransi Kesehatan</font></td>
    <td>Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><font size="9">- Biaya Tes Kesehatan</font></td>
    <td>Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><font size="9">- Kartu Tanda Mahasiswa</font></td>
    <td>Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><font size="9">- Kemampuan Bahasa Inggris Lanjut</font></td>
    <td>Rp.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>3.</td>
    <td>KKN dan Wisuda</td>
    <td>&nbsp;</td>
    <td>Rp.</td>
  </tr>
  <tr>
    <td>4.</td>
    <td>Sumbangan Operasional (SOP)</td>
    <td>&nbsp;</td>
    <td>Rp.</td>
  </tr>
  <tr>
    <td>5.</td>
    <td>Sumbangan Pengembagan (SP3)</td>
    <td>&nbsp;</td>
    <td>Rp.</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#CCFFCC">&nbsp;</td>
    <td bgcolor="#CCFFCC">&nbsp;</td>
    <td bgcolor="#CCFFCC"><strong>TOTAL</strong></td>
    <td bgcolor="#CCFFCC"><strong>Rp.</strong></td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" cellpadding="3">
  <tr>
    <td width="52%">Mengetahui, <br />
Direktur Keuangan,<font size="6px"><br />
</font> <br />
<br />
<br />
<br /> 
<u>Dr. Heru Tjaraka, SE., MSi., BKP., Ak</u>
<br />
NIP. 196709271993031003</td>
    <td width="48%"> Surabaya, <br />
      Verivikator,<br />
      <br />
      <br />
      <br />
      <u>Eko Siswantoro, Drs., MM</u> <br />
NIP. 196211201988021001</td>
  </tr>
</table>
<p><br /><br />
<font size="8px">
Catatan :<br />
Apabila Saudara telah melakukan pembayaran / memenuhi semua kewajiban saudara abaikan tagihan / invoice ini.
</font>
</p>