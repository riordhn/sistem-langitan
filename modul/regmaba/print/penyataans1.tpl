<table width="100%" border="0" cellpadding="5">
  <tr>
    <td colspan="2" align="center"><b><font size="14">SURAT -PERNYATAAN</font></b></td>
  </tr>
  <tr>
    <td colspan="2">Yang bertanda tangan dibawah ini, Saya : </td>
  </tr>
  <tr>
    <td width="25%">Nama</td>
    <td width="75%">: ...........................................................................................................................................</td>
  </tr>
  <tr>
    <td>Tempat dan tanggal lahir</td>
    <td>: ...........................................................................................................................................</td>
  </tr>
  <tr>
    <td>Alamat Surabaya</td>
    <td>: ...........................................................................................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp. ..................................................................................................................................</td>
  </tr>
  <tr>
    <td>Alamat Asal</td>
    <td>: ...........................................................................................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>  &nbsp;...........................................................................................................................................</td>
  </tr>
  <tr>
    <td>Asal SMTA</td>
    <td>: ...........................................................................................................................................</td>
  </tr>
  <tr>
    <td>Alamat AMTA</td>
    <td>: ...........................................................................................................................................</td>
  </tr>
  <tr>
    <td>Nama Orang Tua / Wali</td>
    <td>: ...........................................................................................................................................</td>
  </tr>
  <tr>
    <td>Pekerjaan</td>
    <td>: ...........................................................................................................................................</td>
  </tr>
  <tr>
    <td>Jabatan</td>
    <td>: ...........................................................................................................................................</td>
  </tr>
  <tr>
    <td>Alamat Rumah</td>
    <td>: ...........................................................................................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp.  ..................................................................................................................................</td>
  </tr>
</table>

<br />
Dengan ini menyatakan bahwa : 
<ol>
   <li>Saya
  tidak pernah, sedang atau akan terlibat dalam penyalahgunaan Narkotika, Alkohol, Psikotropikadan Zat Adiktif (NAPZA) baik sebagai pengguna, pengedar, produsen atau yang berkaitan dengan hal tersebut. Apabila ternyata di kemudian hari pada saat saya menuntut ilmu di Universitas Airlangga saya terlibat dan atau terbukti terlibat dalam penyalahgunaan NAPZA sebagaimana dimaksud di atas, maka saya sanggup dan bersedia dikenakan sanksi sampai dengan dibatalkan status saya sebagai mahasiswa Universitas Airlangga.
  </li>
   <li>Bersedia mentaati segala peraturan dan keputusan yang berlaku di lingkungan Universitas Airlangga.</li>
</ol>
<p>
	Demikian surat pernyataan ini saya buat dengan sebenarnya, tanpa adanya paksaan dari pihak siapapun.
</p>
<p>&nbsp;</p>
<table width="100%">
  <tr>
    <td width="54%" align="right">&nbsp;</td>
    <td width="15%" align="right"><table width="100%" border="1">
      <tr>
        <td align="center">
        <p>&nbsp;</p>
        <p>4 x 6</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        </td>
      </tr>
    </table></td>
    <td width="31%">
    Surabaya, ....................................<br>
    Yang membuat pernyataan,<br>
    <br />
    <font size="6px">
    &nbsp;&nbsp;&nbsp;
    Materai<br />
    &nbsp;&nbsp;&nbsp;
    Rp. 6.000,-<br>
    </font>
    <br>
	<br />
	<br />
	<br />
	<hr>			
    &nbsp;&nbsp;( Nama Lengkap )
    </td>
  </tr>
</table>
