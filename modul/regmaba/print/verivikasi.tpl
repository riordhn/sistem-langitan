<style>
    td { font-size: 26px; }
 </style>
<table border="1" cellpadding="3">
    <tr>
        <td align="center" style="background-color: #008" colspan="4"><h3 style="color: yellow">FORMULIR VERIFIKASI ADMINISTRASI</h3></td>
    </tr>
    <tr><td colspan="4"></td></tr>
    <tr>
        <td align="center" style="background-color: #008" colspan="4"><h4 style="color: yellow">IDENTITAS DIRI</h4></td>
    </tr>
    <tr>
        <td width="20" align="center">1</td>
        <td style="width: 160px;">Nama CAMABA</td>
        <td colspan="4" style="width: auto"></td>
    </tr>
    <tr>
        <td align="center">2</td>
        <td>Nomor Ujian Tulis SNMPTN</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">3</td>
        <td>Program Studi (Diterima)</td>
        <td colspan="4"></td>
    </tr>
    <tr>
      <td align="center">4</td>
      <td>No. Identitas</td>
      <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">5</td>
        <td>Nama Ayah</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">6</td>
        <td>Alamat Ayah</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">7</td>
        <td>Nama Ibu</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">8</td>
        <td>Alamat Ibu</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">9</td>
        <td>Jumlah Anggota Keluarga</td>
        <td width="150">Orang</td>
        <td  width="100">No. KSK</td>
        <td style="width: auto"></td>
    </tr>
    <tr>
        <td align="center">10</td>
        <td>Sanggup Membayar SP3</td>
        <td colspan="4" style="width: auto">
            <table width="100%" border="0">
              <tr>
                <td>a. SP3b</td>
                <td>b. SP3a</td>
                <td>c. SP30</td>
                <td>d. SOP0</td>
              </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="center">11</td>
        <td>Penghasilan Orang Tua / Bulan</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; 7.5 Jt</td>
                <td>b. 2.5 - 7.5 Jt</td>
                <td>c. 1.35 - 2.5 Jt</td>
                <td>d. &lt; 1.35 Jt</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
      <td align="center" style="background-color: #008;width: auto;" colspan="4"><h4 style="color: yellow">PEKERJAAN / PROFESI / JABATAN ORANG TUA</h4></td>
    </tr>
    <tr>
      <td style="width: 20px; text-align: center;">12</td>
      <td colspan="4" style="width:auto;">Perkerjaan</td>
    </tr>
    <tr>
        <td style="width: 20px; text-align: center;">&nbsp;</td>
        <td style="width: 160px;">a. Wirausahaawan</td>
        <td colspan="4" style="width: auto">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; Rp 500 Jt</td>
                <td>b. Rp 100 - 500 Jt</td>
                <td>c. Rp 25 - 100 Jt</td>
                <td>d. &lt; Rp 25 Jt</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;"></td>
        <td>b. Pegawai Swasta</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. Manager</td>
                <td>b. Lower Manajer</td>
                <td>c. Pelaksana</td>
                <td>d. Pemb. Pel</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;"></td>
        <td>c. Pegawai BUMN</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. Manager</td>
                <td>b. Lower Manajer</td>
                <td>c. Pelaksana</td>
                <td>d. Pemb. Pel</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;"></td>
        <td>d. PNS Fungsional (Dosen / Guru)</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. Gol IV</td>
                <td>b. Gol III</td>
                <td>c. Gol II</td>
                <td>d. Gol I</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;"></td>
        <td>e. PNS Administrasi</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. Gol IV</td>
                <td>b. Gol III</td>
                <td>c. Gol II</td>
                <td>d. Gol I</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;"></td>
        <td>f. Petani</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; 1 ha</td>
                <td>b. 0.5 - 1 ha</td>
                <td>c. 0.25 - 0.5 ha</td>
                <td>d. &lt; 0.25 ha</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;"></td>
        <td>g. Lain-lain</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. Buruh Pabrik</td>
                <td>b. Buruh Tani</td>
                <td>c. Pegawai Honorer</td>
                <td>d. PRT</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">13</td>
        <td>Skala Usaha / Pekerjaan</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. Besar</td>
                <td>b. Menengah</td>
                <td>c. Kecil</td>
                <td>d. Mikro</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
      <td align="center" style="background-color: #008;width: auto;" colspan="4"><h4 style="color: yellow">KEDIAMAN DAN KENDARAAN KELUARGA</h4></td>
    </tr>
    <tr>
        <td style="width: 20px; text-align: center;">14</td>
        <td style="width: 160px;">Kediaman Orang Tua</td>
        <td colspan="4" style="width: auto">
            <table width="100%" border="0">
              <tr>
                <td>a. Mewah / Besar</td>
                <td>b. Sedang</td>
                <td>c. RS</td>
                <td>d. RSS</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">15</td>
        <td>Luas Tanah Kediaman</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; 200 m2</td>
                <td>b. 100 - 200 m2</td>
                <td>c. 45 - 100 m2</td>
                <td>d. &lt; 45 m2</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">16</td>
        <td>Luas Bangunan Kediaman</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; 100 m2</td>
                <td>b. 56 - 100 m2</td>
                <td>c. 27 - 56 m2</td>
                <td>d. &lt; 27 m2</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">17</td>
        <td>NJOP</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; 300 Jt</td>
                <td>b. 100 - 300 Jt</td>
                <td>c. 50 - 100 Jt</td>
                <td>d. &lt; 50 Jt</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
      <td align="center">18</td>
      <td>Daya Listrik Kediaman</td>
      <td colspan="4">
          <table width="100%" border="0">
            <tr>
              <td>a. &gt; 2.200 VA</td>
              <td>b. 1.300 VA</td>
              <td>c. 900 VA</td>
              <td>d. &lt; 450 VA</td>
            </tr>
          </table>
      </td>
    </tr>
    <tr>
        <td align="center">19</td>
        <td>Kendaraan Bermotor R4</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; 1</td>
                <td>b. 1</td>
                <td>c. Tidak Punya</td>
                <td>d. Tidak Punya</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">20</td>
        <td>Kendaraan Bermotor R2</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; 2</td>
                <td>b. 2</td>
                <td>c. 1</td>
                <td>d. Tidak Punya</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">21</td>
        <td>Daya Listrik (VA)</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">22</td>
        <td>Kekayaan Lainya :</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">23</td>
        <td>Informasi Lainnya, sebutkan :</td>
        <td colspan="4"></td>
    </tr>
    <tr>
      <td align="center" style="background-color: #008;width: auto;" colspan="4"><h4 style="color: yellow">PERNYATAAN</h4></td>
    </tr>
    <tr>
        <td align="center">
        <font color="#f00"><b>Data dan informasi ini disampaikan dengan sesungguhnya dan sebenar-benarnya. Apabila ternyata data dan informasi
        ini tidak benar dan tidak lengkap, saya bersedia kehilangan dan atau melepaskan hak saya sebagai mahasiswa Universitas Airlangga
        selama 2 tahun pada semua program studi melalui program seleksi apapun.</b></font>
        <br/><br/><br/><br/>
        <table>
            <tr>
                <td>&nbsp;</td>
                <td align="center">...................</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center">Mengetahui,</td>
                <td align="center">Yang Menyatakan,</td>
            </tr>
            <tr>
                <td align="center">Orang Tua / Wali</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><font size="8px">(materai)</font></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center">_____________________</td>
                <td align="center">_____________________</td>
            </tr>
        </table>
        <br/>
        </td>
    </tr>
</table>