<style>
	.isi{border:1px solid; border-collapse:collapse;}
	.headers{margin:20px; height:100;}
	#headers{margin:20px; height:100;}
</style>
<table width="100%" border="0" class="headers">
  <tr>
    <td width="84%" align="center">
    	<p>&nbsp;</p>
    	<p><font size="14"><b>FORMULIR REGISTRASI MAHASISWA BARU <br>
		TAHUN AKADEMIK 2011 / 2012</b></font></p>
        <p>&nbsp;</p>
    </td>
    <td width="16%">
		<table width="100%" border="1" class="isi" id="headers">
          <tr>
            <td align="center"><p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>4 x 6 cm</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            </td>
          </tr>
        </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" border="1" class="isi" cellpadding="5">
  <tr>
    <td width="19%" valign="top">CARA MENGISI :</td>
    <td width="81%">1. Lingkari angka / nomor yang sesuai dengan jawaban Saudara dan atau
    <br>2. Isilah titik-titik untuk keterangan yang berhubungan
    <br>3. Bila salah mengisi, berilah tanda X dan gantilah dengan isian yang benar</td>
  </tr>
</table>
<p>&nbsp;</p>
<p>YANG BERTANDA TANGAN PADA FORMULIR INI MINTA DIDAFTAR SEBAGAI MAHASISWA PADA : </p>

<table width="100%" border="1" class="isi" cellpadding="5">
  <tr>
    <td width="4%" align="center">1</td>
    <td width="28%">Nama Universitas</td>
    <td width="68%">UNIVERSITAS AIRLANGGA</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td>Fakultas</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center">2a</td>
    <td>Program Studi</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td>Jenjang Studi</td>
    <td>1. Profesi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        2. Spesialis &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        3. S2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        4. S3</td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" border="1" class="isi" cellpadding="5px">
  <tr>
    <td width="4%"><div align="center">4</div></td>
    <td width="28%">No. Test.</td>
    <td width="68%">&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">5</div></td>
    <td>Nama Lengkap</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">6</div></td>
    <td>No. Induk Mhs. / NIM.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">7</div></td>
    <td>Tempat &amp; Tanggal lahir</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">8</div></td>
    <td>Jenis Kelamin</td>
    <td>1. Pria &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   	2. Wanita</td>
  </tr>
  <tr>
    <td><div align="center">9</div></td>
    <td>Agama</td>
    <td>1. Islam &nbsp;&nbsp;&nbsp;&nbsp; 
    	2. Kristen &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        3. Katholik &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	4. Budha &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        5. Hindu &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        6. Lain-lain &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">10</div></td>
    <td>Status Perkawinan</td>
    <td>1. Belum Kawin &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        2. Kawin &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        3. Bercerai &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   	4. Kawin, Suami / Istri Meninggal</td>
  </tr>
  <tr>
    <td><div align="center">11</div></td>
    <td>Biaya Pendidikan</td>
    <td>1. Biaya Sendiri &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    2. Beasiswa </td>
  </tr>
  <tr>
    <td><div align="center">12</div></td>
    <td>Biaya Pendidikan</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>( Diisi jika isian biaya pendidikan anda no. &quot; 2 &quot; )</td>
  </tr>
  <tr>
    <td><div align="center">13</div></td>
    <td>PT. Asal / Insts. Pengirim</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">14</div></td>
    <td>Alamat Kantor</td>
    <td>&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%">
  <tr>
    <td width="68%" align="right">&nbsp;</td>
    <td width="32%">
    Surabaya, ....................................<br>
    Tandatangan yang bersangkutan,<br><br><br>
	<br>
	<hr>			
    </td>
  </tr>
</table>
