<style>
	.isi{border:1px solid; border-collapse:collapse;}
	.headers{margin:20px; height:100;}
	#headers{margin:20px; height:100;}
</style>
<table width="100%" border="0" class="headers">
  <tr>
    <td width="84%" align="center">
    	<p>&nbsp;</p>
    	<p><font size="14"><b>FORMULIR REGISTRASI MAHASISWA BARU <br>
		TAHUN AKADEMIK 2011 / 2012</b></font></p>
        <p>&nbsp;</p>
    </td>
    <td width="16%">
		<table width="100%" border="1" class="isi" id="headers">
          <tr>
            <td align="center"><p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>4 x 6 cm</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            </td>
          </tr>
        </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" border="1" class="isi" cellpadding="5">
  <tr>
    <td width="19%" valign="top">CARA MENGISI :</td>
    <td width="81%">1. Lingkari angka / nomor yang sesuai dengan jawaban Saudara dan atau
    <br>2. Isilah titik-titik untuk keterangan yang berhubungan
    <br>3. Bila salah mengisi, berilah tanda X dan gantilah dengan isian yang benar</td>
  </tr>
</table>
<p>&nbsp;</p>
<p>YANG BERTANDA TANGAN PADA FORMULIR INI MINTA DIDAFTAR SEBAGAI MAHASISWA PADA : </p>

<table width="100%" border="1" class="isi" cellpadding="5">
  <tr>
    <td width="4%" align="center">1</td>
    <td width="28%">Nama Universitas</td>
    <td width="68%">UNIVERSITAS AIRLANGGA</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td>Fakultas</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center">2a</td>
    <td>Program Studi</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td>Jenjang Studi</td>
    <td>1. Program D3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        2. Program S1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        3. Program S1 Alih Jenis</td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" border="1" class="isi" cellpadding="5px">
  <tr>
    <td width="4%" valign="top" align="center">4</td>
    <td width="28%">Nama Lengkap</td>
    <td width="68%">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" align="center">5</td>
    <td>No. Induk Mhs. / NIM.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" align="center">6</td>
    <td>No. Peserta</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" align="center">7</td>
    <td>Tempat &amp; Tanggal lahir</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" align="center">8</td>
    <td valign="top">Alamat Mahasiswa</td>
    <td><p>&nbsp;</p>
    <p>No. Telp. :</p></td>
  </tr>
  <tr>
    <td valign="top" align="center">9</td>
    <td>Jenis Kelamin</td>
    <td>1. Pria &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   	2. Wanita</td>
  </tr>
  <tr>
    <td valign="top" align="center">10</td>
    <td>Kewarganegaraan</td>
    <td>1. Indonesia &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	2. W.N.A. ............................... (Nyatakan)</td>
  </tr>
  <tr>
    <td valign="top" align="center">11</td>
    <td valign="top">Sumber Biaya</td>
    <td>1. O.T. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	2. O.T. Asuh &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        3. Beasiswa (sebutkan) : ........................<br />
    	4. Lain-lain (sebutkan) : ...............................</td>
  </tr>
  <tr>
    <td valign="top" align="center">12</td>
    <td>Agama</td>
    <td>1. Islam &nbsp;&nbsp;&nbsp;&nbsp; 
    	2. Kristen &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        3. Katholik &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	4. Budha &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        5. Hindu &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        6. Lain-lain &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" align="center">13</td>
    <td>Jumlah Kakak dan Adik</td>
    <td>Kakak ............... Adik ..................</td>
  </tr>
  <tr>
    <td valign="top" align="center">14</td>
    <td>Asal SMTA / MA<br />
    Alamat SMTA/ MA</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" align="center">15</td>
    <td valign="top">Jurusan SMTA/ MA</td>
    <td>1. SMU / MA IPA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	2. SMU / MA IPS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        3. SMU / MA Bhs. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        4. SMK &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
        5. Lain-lain (sebutkan) : ........................</td>
  </tr>
  <tr>
    <td valign="top" align="center">16</td>
    <td>Tgl. No. Ijasah SMTA</td>
    <td>Tanggal ....................... &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   	No. Ijasah ......................................</td>
  </tr>
  <tr>
    <td valign="top" align="center">17</td>
    <td>Ijasah</td>
    <td>Tahun ............... &nbsp;&nbsp;&nbsp;&nbsp;
    	Jumlah Mata Pelajaran ............. &nbsp;&nbsp;&nbsp;&nbsp;
        Jumlah Nilai Ijasah ..............</td>
  </tr>
  <tr>
    <td valign="top" align="center">18</td>
    <td>UAN</td>
    <td>Tahun ............... &nbsp;&nbsp;&nbsp;&nbsp;
    	Jumlah Mata Pelajaran ............. &nbsp;&nbsp;&nbsp;&nbsp;
    	Jumlah Nilai UAN ..............</td>
  </tr>
  <tr>
    <td valign="top" align="center">19</td>
    <td valign="top">Nama dan Alamat OT / Wali</td>
    <td>Nama 
    	&nbsp;&nbsp;&nbsp;&nbsp;: ......................................................................................................................<br />
    	Alamat &nbsp;&nbsp;: ......................................................................................................................<br />
        Kabupaten / Kodya : ...................................................... Telp. .....................................<br />
        Propinsi : ......................................................................................................................
    </td>
  </tr>
  <tr>
    <td valign="top" align="center">20</td>
    <td valign="top">Pendidikan Ayah / Ibu</td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
    	Tdk. Tamat SD &nbsp;&nbsp;&nbsp;&nbsp; 
        Tamat SD &nbsp;&nbsp;&nbsp;&nbsp; 
        SLTP &nbsp;&nbsp;&nbsp;&nbsp; 
        SLTA &nbsp;&nbsp;&nbsp;&nbsp;
    	Diploma &nbsp;&nbsp;&nbsp;&nbsp; 
        S1 &nbsp;&nbsp;&nbsp;&nbsp; 
        S2 &nbsp;&nbsp;&nbsp;&nbsp; 
        S3 &nbsp;&nbsp;&nbsp;&nbsp;<br />
		Ayah &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        4 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        5 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        6 &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; 
        7 &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; 
        8 &nbsp;&nbsp;&nbsp;&nbsp;<br />
		Ibu &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        4 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        5 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        6 &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; 
        7 &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; 
        8 &nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" align="center">21</td>
    <td valign="top">Pekerjaan Ayah</td>
    <td>1. Guru / Dosen Negeri <br />
		2. PNS Bukan Guru / Dosen 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        7. Petani / Nelayan
        <br />
		3. TNI / POLRI 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        8. Buruh
        <br />
		4. Guru / Dosen Swasta
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;
        9. Pensiunan PNS / TNI / POLRI
        <br />
		5. Karyawan Swasta
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        10. Pensiunan K. Swasta <br />
		6. Pedagang / Wiraswasta
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	11. Lain-lain (sebutkan) ..............................
    </td>
  </tr>
  <tr>
    <td valign="top" align="center">22</td>
    <td valign="top">Pekerjaan Ibu</td>
    <td>1. Guru / Dosen Negeri <br />
		2. PNS Bukan Guru / Dosen 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        7. Petani / Nelayan
        <br />
		3. TNI / POLRI 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        8. Buruh
        <br />
		4. Guru / Dosen Swasta
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;
        9. Pensiunan PNS / TNI / POLRI
        <br />
		5. Karyawan Swasta
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        10. Pensiunan K. Swasta <br />
		6. Pedagang / Wiraswasta
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	11. Lain-lain (sebutkan) ..............................
    </td>
  </tr>
  <tr>
    <td valign="top" align="center">23</td>
    <td valign="top">Besarnya penghasilan O.T. / Wali sebulan</td>
    <td>1. Tidak Lebih dari Rp. 1.350.000,-
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	3. Rp. 2.500.001,- - Rp. 7.500.000,-<br />
     	2. Rp. 1.350.001,- - Rp. 2.500.000,-
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	4. Lebih dari Rp. 7.500.000,-
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" border="1" class="isi">
  <tr>
    <td width="4%" valign="top" align="center">24</td>
    <td width="96%">Pernahkah Saudara mempunyai Nomor Induk Mahasiswa / NIM yang berbeda dengan sekarang ?<br />
      1. Ya &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      2. Tidak</td>
  </tr>
  <tr>
    <td valign="top" align="center">25</td>
    <td>Jika ya nyatakan :<br />
      Nomor Induk Mahasiswa / NIM lama : 
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    NIM Baru :</td>
  </tr>
  <tr>
    <td valign="top" align="center">26</td>
    <td>Apakah Saudara pindahan dari perguruan tinggi lain ? <br />
      1. Ya &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      2. Tidak<br />
      Jika ya nyatakan :</td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%">
  <tr>
    <td width="68%" align="right">&nbsp;</td>
    <td width="32%">
    Surabaya, ....................................<br>
    Tandatangan yang bersangkutan,<br><br><br>
	<br>
	<hr>			
    </td>
  </tr>
</table>
