<?php

session_start();

include '../../../config.php';
include 'class/regmaba.class.php';
include '../class/Kota.class.php';

$regmaba = new regmaba($db);

$Kota = new Kota($db);

$mode = get('mode');

// Melakukan cek apakah sudah login atau belum
if (empty($_SESSION['ID_C_MHS'])) {
    header("location: /index.php");
    exit();
}

$id_c_mhs = $_SESSION['ID_C_MHS'];
$jalur = strtoupper($_SESSION['JALUR']);
$jalur_login = strtoupper($_SESSION['JALUR_LOGIN']);

if (isset($_POST)) {
    // UPDATE BIODATA
    if (post('mode') == 'biodata') {

        $regmaba->UpdateTglRegmaba($id_c_mhs);
        $result = $regmaba->Update($id_c_mhs, $_POST);
    }

    // UPLOAD FILE
    if (post('mode') == 'upload') {
        $jenis_berkas = array('ijazah', 'skhun', 'akte', 'kk', 'penghasilan', 'siup', 'petani', 'sppt_pbb', 'listrik', 'air', 'stnk_motor', 'stnk_mobil');
        foreach ($jenis_berkas as $berkas) {
            if (count($_FILES["file_{$berkas}"]) > 0) {
                if ($_FILES["file_{$berkas}"]['size'] <= (1024 * 1024 * 32)) {  // diganti jadi 32MB
                    $destination = "/var/www/html/files/snmptn/pdf-berkas/{$id_c_mhs}_{$berkas}.pdf";
                    if (move_uploaded_file($_FILES["file_{$berkas}"]['tmp_name'], $destination))
                        $regmaba->UpdateFile($id_c_mhs, "file_{$berkas}", '1');
                }
            }
        }
    }
    // UPDATE JADWAL
    if (post('mode') == 'jadwal') {
        $regmaba->UpdateJadwalVerifikasi($id_c_mhs, $_POST);
    }
}


// Inisialisasi tampilan awal
if ($mode != '') {
    // Step Pengisian Biodata
    if ($mode == 'biodata') {
        $form_template = $mode;
        // Mendapatkan data calon mahasiswa secara lengkap
        $cmb = $regmaba->GetData($id_c_mhs, true);
        $smarty->assign('cmb', $cmb);

        $smarty->assign('kota_set', $regmaba->GetListKota());
        $smarty->assign('negara_set', $Kota->GetListNegara());
        $smarty->assign('negara_indo_set', $Kota->GetListNegaraIndonesia());
        $smarty->assign('negara_asing_set', $Kota->GetListNegaraAsing());
        $smarty->assign('jenis_kelamin_set', $regmaba->GetListJenisKelamin());
        $smarty->assign('kewarganegaraan_set', $regmaba->GetListKewarganegaraan());
        $smarty->assign('agama_set', $regmaba->GetListAgama());
        $smarty->assign('sumber_biaya_set', $regmaba->GetListSumberBiaya());
        $smarty->assign('jurusan_sekolah_set', $regmaba->GetListJurusanSekolah());
        $smarty->assign('pendidikan_ortu_set', $regmaba->GetListPendidikanOrtu());
        $smarty->assign('pekerjaan_set', $regmaba->GetListPekerjaan());
        $smarty->assign('penghasilan_ortu_set', $regmaba->GetListPenghasilanOrtu());
        $smarty->assign('skala_pekerjaan_set', $regmaba->GetListSkalaPekerjaan());
        $smarty->assign('kediaman_ortu_set', $regmaba->GetListKediamanOrtu());
        $smarty->assign('luas_tanah_set', $regmaba->GetListLuasTanah());
        $smarty->assign('luas_bangunan_set', $regmaba->GetListLuasBangunan());
        $smarty->assign('njop_set', $regmaba->GetListNJOP());
        $smarty->assign('listrik_set', $regmaba->GetListListrik());
        $smarty->assign('kendaraan_r4_set', $regmaba->GetListKendaraanR4());
        $smarty->assign('kendaraan_r2_set', $regmaba->GetListKendaraanR2());
        $smarty->assign('status_ptn_set', $regmaba->GetListStatusPTN());
        $smarty->assign('program_studi_set', $regmaba->GetListProgramStudi($id_c_mhs));

        // Tambahan Nambi
        $id_biaya_kuliah = $db->QuerySingle("SELECT ID_BIAYA_KULIAH FROM BIAYA_KULIAH_CMHS WHERE ID_C_MHS='{$id_c_mhs}'");
        $smarty->assign('id_biaya_kuliah', $id_biaya_kuliah);
        $smarty->assign('data_pekerjaan', $regmaba->LoadPekerjaan());
        $smarty->assign('data_range_listrik', $regmaba->LoadRange(5));
        $smarty->assign('data_range_pbb', $regmaba->LoadRange(7));
        $smarty->assign('data_range_njop', $regmaba->LoadRange(3));
        $smarty->assign('data_range_air', $regmaba->LoadRange(6));

        if ($jalur_login == 'MANDIRI') {  // KHUSUS UNTUK JALUR MANDIRI
            $smarty->assign('next_page', $p = 'upload');
        } else {  //  UNTUK JALUR SNMPTN / SBMPTN
            if ($cmb['STATUS_BIDIK_MISI'] == 1) {
                $smarty->assign('next_page', $p = 'upload');
            } else {
                $smarty->assign('next_page', $p = 'upload');
            }
        }
        $smarty->assign('status_bidik_misi', $cmb['STATUS_BIDIK_MISI']);
    }

    // Step Upload File
    if ($mode == 'upload') {
        $form_template = $mode;
        // Mendapatkan data calon mahasiswa secara lengkap
        $cmb = $regmaba->GetData($id_c_mhs, true);
        $smarty->assign('cmb', $cmb);

        if ($jalur_login == 'MANDIRI') {  // UNTUK JALUR MANDIRI
            $smarty->assign('next_page', $p = 'verifikasi');
        } else if ($jalur_login == 'SNMPTN') { // UNTUK JALUR SNMPTN UNDANGAN
            if ($cmb['STATUS_BIDIK_MISI'] == 1) {
                $smarty->assign('next_page', $p = 'jadwal');
            } else {
                $smarty->assign('next_page', $p = 'jadwal');
            }
        } else if ($jalur_login == 'SBMPTN') {  //  UNTUK JALUR SBMPTN
            if ($cmb['STATUS_BIDIK_MISI'] == 1) {
                $smarty->assign('next_page', $p = 'jadwal');
            } else {
                $smarty->assign('next_page', $p = 'verifikasi');
            }
        }
        $smarty->assign('status_bidik_misi', $cmb['STATUS_BIDIK_MISI']);
    }


    // Step Update Jadwal Verifikasi 
    if ($mode == 'jadwal') {
        $form_template = $mode;
        $cmb = $regmaba->GetData($id_c_mhs, true);
        $smarty->assign('cmb', $cmb);

        $kelompok = $regmaba->GetKelompokBiayaMaba($id_c_mhs);
        $smarty->assign('kelompok', $kelompok);

        $jadwal_set = $regmaba->GetListJadwalVerifikasiKeuangan($cmb['ID_PENERIMAAN']);
        $smarty->assign('jadwal_set', $jadwal_set);

        $jadwal_set_pend = $regmaba->GetListJadwalVerifikasiPendidikan($cmb['ID_PENERIMAAN']);
        $smarty->assign('jadwal_set_pend', $jadwal_set_pend);


        if ($jalur_login == 'MANDIRI') {  // UNTUK JALUR MANDIRI
            $smarty->assign('next_page', $p = 'verifikasi');
        } else if ($jalur_login == 'SNMPTN') { // UNTUK JALUR SNMPTN UNDANGAN
            if ($cmb['STATUS_BIDIK_MISI'] == 1) {
                $smarty->assign('next_page', $p = 'jadwal');
            } else {
                $smarty->assign('next_page', $p = 'jadwal');
            }
        } else if ($jalur_login == 'SBMPTN') {  //  UNTUK JALUR SBMPTN
            if ($cmb['STATUS_BIDIK_MISI'] == 1) {
                $smarty->assign('next_page', $p = 'verifikasi');
            } else {
                $smarty->assign('next_page', $p = 'verifikasi');
            }
        }
        $smarty->assign('status_bidik_misi', $cmb['STATUS_BIDIK_MISI']);
    }

    // Step Hasil Verifikasi
    if ($mode == 'verifikasi') {
        $form_template = $mode;
        // Mendapatkan data calon mahasiswa secara lengkap
        $cmb = $regmaba->GetData($id_c_mhs, true);
        $id_biaya_kuliah = $db->QuerySingle("SELECT ID_BIAYA_KULIAH FROM BIAYA_KULIAH_CMHS WHERE ID_C_MHS='{$id_c_mhs}'");
        $db->Query("SELECT TO_CHAR(TGL_AWAL_PERIODE_BAYAR,'DD-MM-YYYY') TGL_AWAL,TO_CHAR(TGL_AKHIR_PERIODE_BAYAR,'DD-MM-YYYY') TGL_AKHIR FROM AUCC.PERIODE_BAYAR_CMHS WHERE ID_PENERIMAAN='{$cmb['ID_PENERIMAAN']}'");
        $periode_bayar = $db->FetchAssoc();


        $smarty->assign('periode_bayar', $periode_bayar);
        $smarty->assign('id_biaya_kuliah', $id_biaya_kuliah);
        $smarty->assign('cmb', $cmb);

        if ($jalur_login == 'MANDIRI') {  // UNTUK JALUR MANDIRI
            $smarty->assign('next_page', $p = 'verifikasi');
        } else if ($jalur_login == 'SNMPTN') { // UNTUK JALUR SNMPTN UNDANGAN
            if ($cmb['STATUS_BIDIK_MISI'] == 1) {
                $smarty->assign('next_page', $p = 'jadwal');
            } else {
                $smarty->assign('next_page', $p = 'jadwal');
            }
        } else if ($jalur_login == 'SBMPTN') {  //  UNTUK JALUR SBMPTN
            if ($cmb['STATUS_BIDIK_MISI'] == 1) {
                $smarty->assign('next_page', $p = 'verifikasi');
            } else {
                $smarty->assign('next_page', $p = 'verifikasi');
            }
        }
    }

    // Step Hasil Tes Toefl dan Kesehatan
    if ($mode == 'hasil-test') {
        $form_template = $mode;
        // Mendapatkan data calon mahasiswa secara lengkap
        $cmb = $regmaba->GetData($id_c_mhs, true);
        $smarty->assign('cmb', $cmb);

        //harcode dayat
        if ($id_c_mhs == '59696' or $id_c_mhs == '60437') {
            
        } else {
            $db->Query("SELECT KESEHATAN_KESIMPULAN_AKHIR, TO_CHAR(JADWAL_KESEHATAN.TGL_TEST, 'DD-MM-YYYY') AS TGL_TEST_KESEHATAN, 
            TGL_VERIFIKASI_KESEHATAN, NIM_MHS
              FROM CALON_MAHASISWA_BARU
              JOIN PENERIMAAN ON PENERIMAAN.ID_PENERIMAAN = CALON_MAHASISWA_BARU.ID_PENERIMAAN
              LEFT JOIN CALON_MAHASISWA_DATA ON CALON_MAHASISWA_DATA.ID_C_MHS = CALON_MAHASISWA_BARU.ID_C_MHS
              LEFT JOIN JADWAL_KESEHATAN ON JADWAL_KESEHATAN.ID_JADWAL_KESEHATAN = CALON_MAHASISWA_DATA.ID_JADWAL_KESEHATAN
              WHERE CALON_MAHASISWA_BARU.ID_C_MHS = '$id_c_mhs'
              AND TO_CHAR(JADWAL_KESEHATAN.TGL_TEST, 'YYYY-MM-DD') <= TO_CHAR(TGL_AKHIR_KESEHATAN, 'YYYY-MM-DD')
              AND TO_CHAR(JADWAL_KESEHATAN.TGL_TEST, 'YYYY-MM-DD') >= TO_CHAR(TGL_AWAL_KESEHATAN, 'YYYY-MM-DD')");

            $hasil_kesehatan = $db->FetchAssoc();
            $smarty->assign('hasil_kesehatan', $hasil_kesehatan);

            $db->Query("SELECT TO_CHAR(JADWAL_TOEFL.TGL_TEST, 'DD-MM-YYYY') AS TGL_TEST_ELPT, TGL_VERIFIKASI_ELPT, ELPT_SCORE
            FROM CALON_MAHASISWA_BARU
            JOIN PENERIMAAN ON PENERIMAAN.ID_PENERIMAAN = CALON_MAHASISWA_BARU.ID_PENERIMAAN
            LEFT JOIN CALON_MAHASISWA_DATA ON CALON_MAHASISWA_DATA.ID_C_MHS = CALON_MAHASISWA_BARU.ID_C_MHS
            LEFT JOIN JADWAL_TOEFL ON JADWAL_TOEFL.ID_JADWAL_TOEFL = CALON_MAHASISWA_DATA.ID_JADWAL_TOEFL
            WHERE CALON_MAHASISWA_BARU.ID_C_MHS = '$id_c_mhs'");

            $hasil_elpt = $db->FetchAssoc();
            $smarty->assign('hasil_elpt', $hasil_elpt);
        }
    }



    // Autocomplete kota
    if ($mode == 'get-kota') {
        $term = trim(strip_tags(get('term')));

        echo json_encode($regmaba->GetListKotaSearch(strtoupper($term)));
        exit();
    }

    // autocomplete sekolah
    if ($mode == 'get-sekolah') {
        $term = trim(strip_tags(get('term')));

        echo json_encode($regmaba->GetListSekolahSearch(strtoupper($term)));
        exit();
    }

    if ($mode == 'get-prodi') {
        $terpilih = implode(",", $_GET['terpilih']);

        foreach ($regmaba->GetListProgramStudi($id_c_mhs, $terpilih) as $ps) {
            echo "<option value=\"{$ps['ID_PROGRAM_STUDI']}\">{$ps['NM_PROGRAM_STUDI']}</option>";
        }

        exit();
    }

    if ($mode == 'get-prodi-minat') {
        $id_program_studi = $_GET['id_program_studi'];

        foreach ($regmaba->GetListProgramStudiMinat($id_c_mhs, $id_program_studi) as $ps) {
            echo "<option value=\"{$ps['ID_PRODI_MINAT']}\">{$ps['NM_PRODI_MINAT']}</option>";
        }
        exit();
    }
}

// tutup cetak invoice keuangan
$waktu_pembukaan = date('d-m-Y H:i', strtotime('19-07-2014 21:00'));
$waktu_sekarang = date('d-m-Y H:i');
$smarty->assign('waktu_pembukaan', $waktu_pembukaan);
$smarty->assign('waktu_sekarang', $waktu_sekarang);

$smarty->assign('jalur', $jalur_login);
$smarty->display("form/{$form_template}.tpl");
?>
