<?php

include '../config.php';
include 'class/regmaba.class.php';
include '../funtion-tampil-informasi.php';

$regmaba = new regmaba($db);

//$db->Query("UPDATE PENERIMAAN SET TGL_AWAL_REGMABA='26-JUN-14', TGL_AKHIR_REGMABA='06-AUG-14' WHERE ID_PENERIMAAN=212");

$smarty->setTemplateDir('gui');
$smarty->setCompileDir('gui_c');

$mode = get('mode');
$login = get('login');
$lihat = get('lihat');

if (isset($_POST)) {
    if (post('mode') == 'login') {
        $jalur = post('jalur');
        if ($lihat == 'kesehatan') {
            // JALUR MANDIRI
            if ($jalur == 'mandiri') {
                $login_result = $regmaba->LoginRegmabaMandiriKesehatan(post('no_ujian'), post('kode_voucher'));
            }
            // JALUR SNMPTN / SBMPTN
            else if ($jalur == 'snmptn' || $jalur == 'sbmptn') {
                $tgl_lahir = post('tgl_lahir_Year') . '-' . post('tgl_lahir_Month') . '-' . str_pad(post('tgl_lahir_Day'), 2, "0", STR_PAD_LEFT);
                $login_result = $regmaba->LoginRegmabaSNMPTNKesehatan(post('no_ujian'), $tgl_lahir);
            }
            // CEK HASIL LOGIN
            if ($login_result['ID_C_MHS'] != '') {
                $_SESSION['ID_C_MHS'] = $login_result['ID_C_MHS'];
                $_SESSION['JALUR'] = $login_result['NM_JALUR'];
                $_SESSION['JALUR_LOGIN'] = $login;
                $_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] = $login_result['TGL_VERIFIKASI_PENDIDIKAN'];
                $_SESSION['TGL_VERIFIKASI_KEUANGAN'] = $login_result['TGL_VERIFIKASI_KEUANGAN'];
            }
            // LOGIN GAGAL
            else {
                header($_SERVER['REQUEST_URI']);
                exit();
            }
        } else {
            // JALUR MANDIRI
            if ($jalur == 'mandiri') {
                $login_result = $regmaba->LoginRegmabaMandiri(post('no_ujian'), post('kode_voucher'));
            }
            // JALUR SNMPTN / SBMPTN
            else if ($jalur == 'snmptn' || $jalur == 'sbmptn') {
                $tgl_lahir = post('tgl_lahir_Year') . '-' . post('tgl_lahir_Month') . '-' . str_pad(post('tgl_lahir_Day'), 2, "0", STR_PAD_LEFT);
                $login_result = $regmaba->LoginRegmabaSNMPTN(post('no_ujian'), $tgl_lahir);
            }
            // CEK HASIL LOGIN
            if ($login_result['ID_C_MHS'] != '') {
                $_SESSION['ID_C_MHS'] = $login_result['ID_C_MHS'];
                $_SESSION['JALUR'] = $login_result['NM_JALUR'];
                $_SESSION['JALUR_LOGIN'] = $login;
                $_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] = $login_result['TGL_VERIFIKASI_PENDIDIKAN'];
                $_SESSION['TGL_VERIFIKASI_KEUANGAN'] = $login_result['TGL_VERIFIKASI_KEUANGAN'];
            }
            // LOGIN GAGAL
            else {
                header("Location: ./?login={$login}");
                exit();
            }
        }
    }
}



if ($_SESSION['ID_C_MHS'] != '' and $_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] == '') {
    // Awal Registrasi Regmaba
    header("Location: ./form.php?mode=biodata");
    exit();
} elseif ($_SESSION['ID_C_MHS'] != '' and $_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] != '') {
    // Jika Sudah Verifikasi Pendidikan tunggu hasil tes Kesehatan
    header("Location: ./form.php?mode=hasil-test");
    exit();
} else {
    // PENUTUPAN LOGIN
    // JIKA INGIN DI TUTUP COMMENT BARIS 78 DAN BUKA COMMENT BARIS 81
    $tutup = '';
    // JIKA INGIN DIBUKA COMMENT BARIS 81 DAN BUKA COMMENT BARIS 78
    //$tutup = alert_error('Mohon jadwal pendaftaran ' . strtoupper($login) . '  Sudah ditutup/Belum dibuka.Terima Kasih', 90);
    if (!empty($tutup)) {
        $ip_allow_regmaba = array('210.57.214.106', '210.57.215.206', '210.57.215.198', '210.57.214.22');
        if (!in_array($_SERVER['REMOTE_ADDR'], $ip_allow_regmaba)) {
            $smarty->assign('tutup', $tutup);
        }
    }
    $smarty->display("login/{$login}.tpl");
}
?>