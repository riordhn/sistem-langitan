<!doctype html>
<html>
    <head>
        <title>{block name='main-title'}{/block}</title>

        <link rel="shorcut icon" href="http://ppmb.unair.ac.id/images/favicon.ico" />

        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="Stylesheet" type="text/css" href="../css/jui/jquery-ui-1.8.20.custom.css" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

        <script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="../js/jquery-ui-1.8.20.custom.min.js"></script>
        <script type="text/javascript" src="../js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="../js/additional-methods.min.js"></script>
        <script type="text/javascript" src="../js/jquery.formatCurrency-1.4.0.min.js"></script>

        <style>

            .ui-autocomplete {
                max-height: 200px;
                overflow-y: auto;
                /* prevent horizontal scrollbar */
                overflow-x: hidden;
                /* add padding to account for vertical scrollbar */
                padding-right: 20px;
            }
            /* IE 6 doesn't support max-height
             * we use height instead, but this forces the menu to always be this tall
             */
            * html .ui-autocomplete {
                height: 200px;
            }
        </style>
    </head>
    <body>
        <div id="wrap-layout">
            <div id="header-layout">
                <div id="header-content"></div>
            </div>
            <div id="nav-layout">
                <div id="nav-content">
                    {if !empty($smarty.session.ID_C_MHS)}
                        <a class="nav-item" href="logout.php">Logout</a>
                    {/if}
                </div>
            </div>
            {block name='tabs'}{/block}

            <div id="content">
                {block name='content'}{/block}
            </div>
            <div style="clear: both;"></div>
            <div id="footer-layout">
                <div id="footer-content" style="text-align: center">
                    <p>Copyright &copy; 2013 by PPMB Universitas Airlangga.<br/>All Rights Reserved</p>
                </div>
            </div>
            <script type="text/javascript" src="js/application.js"></script>
        </div>
    </body>
</html>