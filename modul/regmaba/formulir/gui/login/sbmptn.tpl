<html>
    <head>
        <title>Regmaba - Universitas Airlangga</title>
        <meta name="google-site-verification" content="ihkYFpHX_zbP4SfrkqIEF7c2Lp08dZFIxJ2MKI6m_Hk" />
        <link rel="shortcut icon" href="http://cybercampus.unair.ac.id/img/icon.ico"/>
        <link rel="stylesheet" type="text/css" href="http://cybercampus.unair.ac.id/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="http://regmaba.unair.ac.id/css/style.css" />
        <meta name="description" content="Halaman registrasi ulang mahasiswa baru jalur SNMPTN Universitas Airlangga">
        <link rel="Stylesheet" type="text/css" href="/css/jui/jquery-ui-1.8.20.custom.css" />
        <style>
            .button {

                -moz-box-shadow:inset 0px 1px 0px 0px #54a3f7;
                -webkit-box-shadow:inset 0px 1px 0px 0px #54a3f7;
                box-shadow:inset 0px 1px 0px 0px #54a3f7;

                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #007dc1), color-stop(1, #044c8a));
                background:-moz-linear-gradient(top, #007dc1 5%, #044c8a 100%);
                background:-webkit-linear-gradient(top, #007dc1 5%, #044c8a 100%);
                background:-o-linear-gradient(top, #007dc1 5%, #044c8a 100%);
                background:-ms-linear-gradient(top, #007dc1 5%, #044c8a 100%);
                background:linear-gradient(to bottom, #007dc1 5%, #044c8a 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#007dc1', endColorstr='#044c8a',GradientType=0);

                background-color:#007dc1;

                -moz-border-radius:3px;
                -webkit-border-radius:3px;
                border-radius:3px;

                border:1px solid #124d77;

                display:inline-block;
                color:#ffffff;
                font-family:arial;
                font-size:10px;
                font-weight:normal;
                padding:5px;
                text-decoration:none;

                text-shadow:0px 1px 0px #154682;

            }
            .button:hover {

                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #044c8a), color-stop(1, #007dc1));
                background:-moz-linear-gradient(top, #044c8a 5%, #007dc1 100%);
                background:-webkit-linear-gradient(top, #044c8a 5%, #007dc1 100%);
                background:-o-linear-gradient(top, #044c8a 5%, #007dc1 100%);
                background:-ms-linear-gradient(top, #044c8a 5%, #007dc1 100%);
                background:linear-gradient(to bottom, #044c8a 5%, #007dc1 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#044c8a', endColorstr='#007dc1',GradientType=0);

                background-color:#044c8a;
            }
            .button:active {
                position:relative;
                top:1px;
            }
        </style>
    </head>

    <body>
        <div class="wrap-box">
            <div class="login-box">

                <form action="{$smarty.server.REQUEST_URI}" method="post">
                    <input type="hidden" name="mode" value="login">
                    <input type="hidden" name="jalur" value="sbmptn">
                    {if $tutup==''}
                        <table>
                            <tr>
                                <td class="right-align"><strong>No Peserta</strong></td>
                                <td>
                                    <input name="no_ujian" type="text" maxlength="12" style="width: 200px" title="No Peserta SNMPTN (bukan NISN)" required>
                                </td>
                            </tr>
                            <tr>
                                <td class="right-align"><strong>Tanggal Lahir</strong></td>
                                <td>
                                    <select name="tgl_lahir_Day" required>
                                        <option value=""></option>
                                        <option value="1">01</option>
                                        <option value="2">02</option>
                                        <option value="3">03</option>
                                        <option value="4">04</option>
                                        <option value="5">05</option>
                                        <option value="6">06</option>
                                        <option value="7">07</option>
                                        <option value="8">08</option>
                                        <option value="9">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select>
                                    <select name="tgl_lahir_Month" required>
                                        <option value=""></option>
                                        <option value="01">Januari</option>
                                        <option value="02">Pebruari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                    <input type="text" name="tgl_lahir_Year" maxlength="4" size="4" required/>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <button class="button" onclick="window.location = 'http://regmaba.unair.ac.id';return false;">Kembali</button>
                                    <input class="button" type="submit" value="Login" />
                                </td>
                            </tr>
                        </table>
                    {else}
                        <div style="margin-top: 20px">
                            {$tutup}
                        </div>
                        <button class="button" onclick="window.location = 'http://regmaba.unair.ac.id';return false;" style="margin: 15px">Kembali</button>
                    {/if}
                </form>
            </div>
        </div>
    </body>
</html>