{extends '../layout.tpl'}
{block name='tabs'}
    {if $jalur=='SNMPTN'}
        <ul id="tabs">
            <li><a href="form.php?mode=biodata" >Biodata</a></li>
            <li><a href="form.php?mode=upload"  >Upload</a></li>
            <li><a href="form.php?mode=jadwal" >Jadwal</a></li>
            <li><a id="current">Hasil Tes</a></li>
        </ul>
    {else if $jalur=='MANDIRI'||$jalur=='SBMPTN'}
        <ul id="tabs">
            <li><a href="form.php?mode=biodata">Biodata</a></li>
            <li><a  href="form.php?mode=upload">Upload</a></li>
            <li><a href="form.php?mode=verifikasi" >Verifikasi</a></li>
            <li><a id="current" href="form.php?mode=biodata" >Hasil Tes</a></li>
        </ul>
    {/if}
{/block}
{block name='main-title'}Formulir Online Pendaftaran Mahasiswa Baru Universitas Airlangga{/block}
{block name='content'}
    <div>
        <form action="form.php?mode=page-result" method="post">
            <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
            <input type="hidden" name="mode" value="page-2" />
            <table id="table-formulir" style="width: 60%">
                <caption>PENGUMUMAN HASIL TES KESEHATAN DAN ELPT</caption>
                <tr>
                    <th colspan="2">Biodata</th>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>{$cmb.NM_C_MHS}</td>
                </tr>
                {if isset($hasil_kesehatan) and $hasil_kesehatan != ''}
                    {if $hasil_kesehatan.KESEHATAN_KESIMPULAN_AKHIR == 1}
                        <tr>
                            <td>NIM</td>
                            <td>{$hasil_kesehatan.NIM_MHS}</td>
                        </tr>    
                    {/if}
                {/if}
                <tr>
                    <td>No Ujian</td>
                    <td>{$cmb.NO_UJIAN}</td>
                </tr>
                <tr>
                    <td>Fakultas</td>
                    <td>{$cmb.NM_FAKULTAS}</td>
                </tr>
                <tr>
                    <td>Program Studi</td>
                    <td>{$cmb.NM_PROGRAM_STUDI}</td>
                </tr>
                <tr>
                    <th colspan="2">Pengumuman Hasil Tes Kesehatan</th>
                </tr>
                <tr>
                    <td>Jadwal Tes Kesehatan</td>
                    <td>{$hasil_kesehatan.TGL_TEST_KESEHATAN}</td>
                </tr>
                <tr>
                    <td>Hasil Tes Kesehatan</td>
                    <td>
                        {if isset($hasil_kesehatan) and $hasil_kesehatan != ''}
                            {if $hasil_kesehatan.KESEHATAN_KESIMPULAN_AKHIR == 1}Lulus
                            {elseif $hasil_kesehatan.KESEHATAN_KESIMPULAN_AKHIR == 0 and $hasil_kesehatan.TGL_VERIFIKASI_KESEHATAN != ''}Tidak Lulus
                            {else}
                                Belum Tes
                            {/if}
                        {else}
                            Belum Waktunya Pengumuman
                        {/if}
                    </td>
                </tr>
                <tr>
                    <th colspan="2">Pengumuman Hasil Tes ELPT</th>
                </tr>
                <tr>
                    <td>Jadwal Tes ELPT</td>
                    <td>{$hasil_elpt.TGL_TEST_ELPT}</td>
                </tr>
                <tr>
                    <td>Hasil Tes ELPT</td>
                    <td>{$hasil_elpt.ELPT_SCORE}</td>
                </tr>
            </table>
        </form>
    </div>
{/block}