{extends '../layout.tpl'}

{block name='tabs'}
    <ul id="tabs">
        <li><a href="form.php?mode=biodata" >Biodata</a></li>
        <li><a href="form.php?mode=upload">Upload</a></li> 
        <li><a id="current">Verifikasi</a></li> 
        <li><a>Hasil Tes</a></li>
    </ul>
{/block}
{block name='main-title'}Formulir Online Pendaftaran Mahasiswa Baru Universitas Airlangga{/block}
{block name='content'}
    <div>
        <form action="form.php?mode=verifikasi" method="post">
            <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
            <input type="hidden" name="mode" value="verifikasi" />
            <table id="table-formulir" style="width: 60%">
                <tr>
                    <th colspan="2">Pembayaran</th>
                </tr>
                {if $jalur=='SBMPTN'}
                    <tr>
                        <td colspan="2">
                            <div class="ui-widget">
                                <div class="ui-state-highlight ui-corner-all" style="padding: 10px 5px 0px 10px;margin: 10px"> 
                                    <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                                        <strong>Info :</strong>
                                    </p>
                                    <ul style="padding-left:20px ">
                                        {if $waktu_sekarang<$waktu_pembukaan}
                                            <li>
                                                Calon Maba SBMPTN Unair dapat mencetak invoice Uang Kuliah Tunggal mulai <b>{$waktu_pembukaan}</b> setelah Calon Mahasiswa selesai melengkapi data di sistem ini
                                            </li>
                                        {else}
                                            {if $id_biaya_kuliah==''}
                                                <li>
                                                    Mohon maaf hasil verifikasi keuangan belum bisa di lihat. Silahkan hubungi helpdesk atau direktorat keuangan
                                                </li>
                                            {/if}
                                        {/if}

                                        <li>
                                            Calon Mahasiswa Baru wajib membayar Biaya Pendidikan  setelah melakukan pengisian formulir registrasi secara online pada tanggal :<br/><b>{$periode_bayar.TGL_AWAL}</b> sampai dengan <b>{$periode_bayar.TGL_AKHIR}</b>  (Sampai dengan Pukul 15.00 WIB)
                                        </li>
                                        <li>
                                            Pembayaran dapat dilakukan di teller semua kantor cabang Bank Mandiri, BNI, BNI Syariah, BRI, BTN dan Bank Mandiri Syariah seluruh Indonesia (termasuk mobile banking yang disediakan di Kampus UNAIR), dengan  menyebutkan  Nomor Peserta . Pembayaran tidak dapat dilakukan melalui transfer.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="center-align">
                            {if $waktu_sekarang>$waktu_pembukaan&&$id_biaya_kuliah!=''}
                                <a class="button" target="_blank" href="cetak-invoice.php?cmhs={$cmb.ID_C_MHS}">Cetak Invoice Pembayaran</a>
                            {/if}
                            <a class="button" target="_blank" href="../print-new.php">Cetak Formulir Regmaba</a>
                        </td>
                    </tr>
                {else if $jalur=='MANDIRI'}
                    <tr>
                        <td colspan="2">
                            <div class="ui-widget">
                                <div class="ui-state-highlight ui-corner-all" style="padding: 10px 5px 0px 10px;margin: 10px"> 
                                    <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                                        <strong>Info :</strong>
                                    </p>
                                    <ul style="padding-left:20px ">

                                        <li>
                                            Calon Mahasiswa Baru wajib membayar Biaya Pendidikan  setelah melakukan pengisian formulir registrasi secara online pada tanggal :<br/><b>{$periode_bayar.TGL_AWAL}</b> sampai dengan <b>{$periode_bayar.TGL_AKHIR}</b> (Sampai dengan Pukul 15.00 WIB)
                                        </li>
                                        <li>
                                            Pembayaran dapat dilakukan di teller semua kantor cabang Bank Mandiri, BNI, BNI Syariah, BRI, BTN dan Bank Mandiri Syariah seluruh Indonesia (termasuk mobile banking yang disediakan di Kampus UNAIR), dengan  menyebutkan  Nomor Peserta . Pembayaran tidak dapat dilakukan melalui transfer.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="center-align">
                            <a class="button" target="_blank" href="../print-new.php">Cetak Formulir Regmaba</a>
                        </td>
                    </tr>
                {/if}
            </table>
        </form>
    </div>
{/block}