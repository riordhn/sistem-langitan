{extends '../layout.tpl'}
{block name='tabs'}
    <ul id="tabs">
        <li><a href="form.php?mode=biodata" >Biodata</a></li>
        <li><a href="form.php?mode=upload">Upload</a></li> 
        <li><a id="current">Jadwal</a></li> 
        <li><a>Hasil Tes</a></li>
    </ul>
{/block}
{block name='main-title'}Formulir Online Pendaftaran Mahasiswa Baru Universitas Airlangga{/block}
{block name='content'}
    <div>
        <form action="form.php?mode=jadwal" method="post">
            <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
            <input type="hidden" name="mode" value="jadwal" />
            <table id="table-formulir" style="width: 60%">
                <tr>
                    <th colspan="2">Jadwal Verifikasi</th>
                </tr>
                {if $status_bidik_misi==1}
                    
                    <tr>
                        <td colspan="2" class="center-align">
                            <div class="ui-state-highlight ui-corner-all" style="padding: 10px 5px 0px 10px;margin: 10px"> 
                                <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                                <p style="text-align:justify;">
                                    <strong>Info :</strong><br/>Untuk Mahasiswa Bidik Misi informasi jadwal mengikuti prosedur yang ada di <a target="_blank" href="http://kemahasiswaan.unair.ac.id/">Direktorat Kemahasiswaan Unair</a>
                                </p>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td style="width: 200px"> Jadwal Verifikasi Pendidikan</td>
                        <td>
                            <table style="border: none">
                                <tr style="vertical-align: top">
                                    <td style="border: none;width: 60px"><b>Tanggal </b></td>
                                    <td style="border: none">: {$cmb.TGL_JADWAL_VERIFIKASI_PEND|date_format:"%d-%m-%Y"}</td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td style="border: none"><b>Pukul</b></td>
                                    <td style="border: none">: 08.00 - 16.00 WIB </td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td style="border: none"><b>Tempat</b></td>
                                    <td style="border: none">: 
                                        Gedung Airlangga Convention Center (ACC) Kampus C Universitas
                                        Airlangga, Jl. Mulyorejo, Surabaya
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="center-align">
                            <a class="button" target="_blank" href="../print-new.php">Cetak Formulir Regmaba</a>
                        </td>
                    </tr>
                {else}
                    <tr>
                        <td style="width: 200px"> Jadwal Verifikasi Keuangan</td>
                        <td>
                            {if $cmb.ID_JADWAL_VERIFIKASI_KEUANGAN!=''}
                                <input type="hidden" name="id_jadwal_verifikasi_keuangan" value="{$cmb.ID_JADWAL_VERIFIKASI_KEUANGAN}" />
                                <table style="border: none">
                                    {foreach $jadwal_set as $j}
                                        {if $j.ID_JADWAL == $cmb.ID_JADWAL_VERIFIKASI_KEUANGAN}
                                            <tr style="vertical-align: top">
                                                <td style="border: none;width: 60px"><b>Tanggal </b></td>
                                                <td style="border: none">: {strftime('%d %B %Y', strtotime($j.TGL_JADWAL))} </td>
                                            </tr>
                                        {/if}
                                    {/foreach}

                                    <tr style="vertical-align: top">
                                        <td style="border: none"><b>Pukul</b></td>
                                        <td style="border: none">:  08.00 - 15.00 </td>
                                    </tr>
                                    <tr style="vertical-align: top">
                                        <td style="border: none"><b>Tempat</b></td>
                                        <td style="border: none">: 
                                            Gedung Airlangga Convention Center (ACC) Kampus C Universitas
                                            Airlangga, Jl. Mulyorejo, Surabaya
                                        </td>
                                    </tr>
                                </table>

                            {else}
                                <select name="id_jadwal_verifikasi_keuangan">
                                    <option value="">Pilih</option>
                                    {foreach $jadwal_set as $j}
                                        <option value="{$j.ID_JADWAL}"  {if $j.ISI>=$j.KUOTA}disabled=""{/if} {if $j.ID_JADWAL == $cmb.ID_JADWAL_VERIFIKASI_KEUANGAN}selected="selected"{/if}>{strftime('%d %B %Y', strtotime($j.TGL_JADWAL))} {if $j.ISI>=$j.KUOTA}(Penuh){/if}</option>
                                    {/foreach}
                                </select>
                            {/if}
                        </td>
                    </tr>
                    {if $cmb.ID_JADWAL_VERIFIKASI_KEUANGAN!=''}

                        <tr>
                            <td> Jadwal Verifikasi Pendidikan</td>
                            <td>
                                <table style="border: none">
                                    <tr style="vertical-align: top">
                                        <td style="border: none;width: 60px"><b>Tanggal </b></td>
                                        <td style="border: none">: 21 Juli 2014</td>
                                    </tr>
                                    <tr style="vertical-align: top">
                                        <td style="border: none"><b>Pukul</b></td>
                                        <td style="border: none">: 08.00 - 16.00 WIB </td>
                                    </tr>
                                    <tr style="vertical-align: top">
                                        <td style="border: none"><b>Tempat</b></td>
                                        <td style="border: none">: 
                                            Gedung Airlangga Convention Center (ACC) Kampus C Universitas
                                            Airlangga, Jl. Mulyorejo, Surabaya
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="center-align">
                                <div class="ui-state-highlight ui-corner-all" style="padding: 10px 5px 0px 10px;margin: 10px"> 
                                    <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                                    <p style="text-align:justify;">
                                        <strong>Info :</strong><br/>Anda telan selesai mendapatkan jadwal verifikasi keuangan.
                                        Silahkan cetak formulir regmaba</p>
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="center-align">
                                <a class="button" target="_blank" href="../print-new.php">Cetak Formulir Regmaba</a>
                            </td>
                        </tr>
                    {else}
                        <tr>
                            <td colspan="2" class="center-align">
                                <input type="submit" value="Simpan" class="button"/>
                            </td>
                        </tr>
                    {/if}
                {/if}

            </table>
        </form>
    </div>
{/block}