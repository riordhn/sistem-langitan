{extends '../layout.tpl'}
{block name='tabs'}
    {if $jalur=='SNMPTN'}
        <ul id="tabs">
            <li><a id="current" >Biodata</a></li>
            <li><a>Upload</a></li>
            <li><a>Jadwal</a></li>
            <li><a>Hasil Tes</a></li>
        </ul>
    {else if $jalur=='MANDIRI'||$jalur=='SBMPTN'}
        <ul id="tabs">
            <li><a id="current" >Biodata</a></li>
            <li><a>Upload</a></li>
            <li><a>Verifikasi</a></li>
            <li><a>Hasil Tes</a></li>
        </ul>
    {/if}
{/block}
{block name='main-title'}Formulir Online Pendaftaran Mahasiswa Baru Universitas Airlangga{/block}
{block name='content'}
    <div>
        <form action="form.php?mode={$next_page}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
            <input type="hidden" name="mode" value="biodata" />
            <table id="table-formulir" style="display: none">
                <caption>Registrasi Mahasiswa Baru Program Sarjana (S1) Universitas Airlangga</caption>
                {* INFORMASI *}
                <tr>
                    <td colspan="2">
                        <div class="ui-widget">
                            <div class="ui-state-highlight ui-corner-all" style="padding: 10px 5px 0px 10px;margin: 10px"> 
                                <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                                <p style="text-align:justify;">
                                    <strong>Info :</strong><br/>
                                </p>
                                <ul style="padding-left:20px ">
                                    {* KHUSUS UNTUK JALUR MANDIRI DAN SBMPTN AUTO VERIFIKASI KEUANGAN *}
                                    {if ($jalur=='MANDIRI'||$jalur=='SBMPTN')&&$status_bidik_misi==0}
                                        {if $id_biaya_kuliah!=''}
                                            {if $jalur=='SBMPTN'}
                                                {if $waktu_sekarang<$waktu_pembukaan}
                                                    <li>
                                                        Mohon maaf, Hasil verifikasi keuangan berupa invoice keuangan dapat di cetak mulai <b>{$waktu_pembukaan}</b>
                                                    </li>

                                                {else}
                                                    <li>
                                                        Data Anda telah berhasil diverifikasi silahkan klik link untuk melihat hasil verifikasi keuangan dan mencetak invoice
                                                        <a href="form.php?mode=verifikasi"  class="button" style="color: white" >Hasil Verifikasi</a>
                                                    </li>

                                                {/if}
                                            {else $jalur=='MANDIRI'}
                                                <li>
                                                    Data Anda telah berhasil diverifikasi silahkan klik link untuk melihat hasil verifikasi keuangan dan mencetak invoice
                                                    <a href="form.php?mode=verifikasi"  class="button" style="color: white" >Hasil Verifikasi</a>
                                                </li>
                                            {/if}
                                        {/if}
                                    {/if}
                                    <li>
                                        Silahkan Lengkapi Data Anda, jika masih terdapat data yang masih belum bisa di isi 
                                        silahkan klik Save & Continue untuk menyimpan data sementara dan melanjutkan ke langkah berikutnya
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">Program Studi Diterima</th>
                </tr>
                <tr>
                    <td>Fakultas</td>
                    <td><strong>{$cmb.NM_FAKULTAS}</strong></td>
                </tr>
                <tr>
                    <td>Program Studi</td>
                    <td><strong>{$cmb.NM_PROGRAM_STUDI}</strong></td>
                </tr>
                <tr>
                    <th colspan="2">Data Diri</th>
                </tr>
                <tr>
                    <td>No Ujian</td>
                    <td>{$cmb.NO_UJIAN}</td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td><input type="hidden" name="nm_c_mhs" value="{$cmb.NM_C_MHS}" />{$cmb.NM_C_MHS}</td>
                </tr>
                <tr>
                    <td>Tempat Lahir</td>
                    <td>
                        <input type="hidden" name="id_kota_lahir" id="id-kota-lahir" value="{$cmb.ID_KOTA_LAHIR}" />
                        <input type="text" name="nm_kota_lahir" id="nama-kota-lahir" size="50" value="{$cmb.NM_KOTA_LAHIR}" readonly="readonly"/>
                        <button class="button" id="button-kota-lahir">Edit</button>
                    </td>
                </tr>
                <tr>
                    <td>Tanggal Lahir</td>
                    <td>{$cmb.TGL_LAHIR|date_format:'%d %B %Y'}
                        <input type="hidden" name="tgl_lahir_Day" value="{$cmb.TGL_LAHIR|date_format:'%d'}" />
                        <input type="hidden" name="tgl_lahir_Month" value="{$cmb.TGL_LAHIR|date_format:'%m'}" />
                        <input type="hidden" name="tgl_lahir_Year" value="{$cmb.TGL_LAHIR|date_format:'%Y'}" />
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>
                        <input type="text" name="alamat" size="65" maxlength="250" value="{$cmb.ALAMAT}" />
                    </td>
                </tr>
                <tr>
                    <td>Kota</td>
                    <td>
                        <input type="hidden" name="id_kota" id="id-kota" value="{$cmb.ID_KOTA}" />
                        <input type="text" name="nm_kota" id="nama-kota" size="50" value="{$cmb.NM_KOTA}" readonly="readonly"/>
                        <button class="button"  id="button-kota">Edit</button>
                    </td>
                </tr>
                <tr>
                    <td>No Handphone</td>
                    <td><input type="text" name="telp"  maxlength="32" value="{$cmb.TELP}"/></td>
                </tr>
                <tr>
                    <td>No Telp Utama <br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Yang Bisa Di Hubungi</i></td>
                    <td><input type="text" name="telp_utama"  maxlength="32" value="{$cmb.TELP_UTAMA}"/></td>
                </tr>
                <tr>
                    <td>PIN Blackberry</td>
                    <td>
                        <input type="text" name="pin_bb" size="30" maxlength="64" value="{$cmb.PIN_BB}" />
                    </td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>
                        <input type="text" name="email" size="30" maxlength="64" value="{$cmb.EMAIL}" />
                    </td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>
                        <select name="jenis_kelamin">
                            {foreach $jenis_kelamin_set as $jk}
                                <option value="{$jk.JENIS_KELAMIN}" {if $cmb.JENIS_KELAMIN == $jk.JENIS_KELAMIN}selected="selected"{/if}>{$jk.NM_JENIS_KELAMIN}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Kewarganegaraan</td>
                    <td>
                        <select name="kewarganegaraan" id="id_kewarganegaraan">
                            {foreach $kewarganegaraan_set as $kw}
                                <option value="{$kw.ID_KEWARGANEGARAAN}" {if $cmb.KEWARGANEGARAAN == $kw.ID_KEWARGANEGARAAN}selected="selected"{/if}>{$kw.NM_KEWARGANEGARAAN}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr id="wna-hide">
                    <td>Asal Negara</td>
                    <td>
                        <select id="id_kebangsaan" name="id_kebangsaan">
                            <option value=""></option>
                            {foreach $negara_set as $n}
                                <option value="{$n.ID_NEGARA}" {if $n.ID_NEGARA == $cmb.ID_KEBANGSAAN}selected="selected"{/if}>{$n.NM_NEGARA}</option>
                            {/foreach} 
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Agama</td>
                    <td>
                        <select name="id_agama">
                            {foreach $agama_set as $a}
                                <option value="{$a.ID_AGAMA}" {if $cmb.ID_AGAMA == $a.ID_AGAMA}selected="selected"{/if}>{$a.NM_AGAMA}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Sumber Biaya</td>
                    <td>
                        <select name="sumber_biaya">
                            {foreach $sumber_biaya_set as $sb}
                                <option value="{$sb.ID_SUMBER_BIAYA}" {if $cmb.SUMBER_BIAYA == $sb.ID_SUMBER_BIAYA}selected="selected"{/if}>{$sb.NM_SUMBER_BIAYA}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">Data Pendidikan</th>
                </tr>
                <tr>
                    <td>NISN</td>
                    <td>
                        <input type="text" name="nisn" value="{$cmb.NISN}" size="30" />
                    </td>
                </tr>
                <tr>
                    <td>Asal SMTA / MA</td>
                    <td>
                        <input type="hidden" name="id_sekolah_asal" id="id-sekolah-asal" value="{$cmb.ID_SEKOLAH_ASAL}" />
                        <input type="text" name="nm_sekolah_asal" id="nama-sekolah-asal" size="50" value="{$cmb.NM_SEKOLAH_ASAL}" readonly="readonly"/>
                        <button class="button"  id="button-sekolah-asal">Edit</button>
                    </td>
                </tr>
                <tr>
                    <td>Jurusan SMTA / MA</td>
                    <td>
                        <select name="jurusan_sekolah">
                            {foreach $jurusan_sekolah_set as $js}
                                <option value="{$js.ID_JURUSAN_SEKOLAH}" {if $cmb.JURUSAN_SEKOLAH == $js.ID_JURUSAN_SEKOLAH}selected="selected"{/if}>{$js.NM_JURUSAN_SEKOLAH}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>No &amp; Tgl Ijazah SMTA</td>
                    <td> No Ijazah:
                        <input type="text" name="no_ijazah"  maxlength="40" value="{$cmb.NO_IJAZAH}" />
                        <br/>
                        Tgl Ijazah: {html_select_date prefix="tgl_ijazah_" day_empty="" month_empty="" year_empty="" field_order="DMY" start_year="-15" time=$cmb.TGL_IJAZAH}
                    </td>
                </tr>
                <tr>
                    <td>Ijazah</td>
                    <td> Tahun :
                        <input type="text" name="tahun_lulus" size="4" maxlength="4" value="{$cmb.TAHUN_LULUS}"/>
                        <br/>
                        Jumlah Mata Pelajaran :
                        <input type="text" name="jumlah_pelajaran_ijazah" size="2" maxlength="2" value="{$cmb.JUMLAH_PELAJARAN_IJAZAH}"/>
                        <br/>
                        Jumlah Nilai Akhir :
                        <input type="text" name="nilai_ijazah" size="5" maxlength="5" value="{$cmb.NILAI_IJAZAH}" /> 
                        * Bukan rata-rata</td>
                </tr>
                <tr>
                    <td>UAN</td>
                    <td> Tahun :
                        <input type="text" name="tahun_uan" size="4" maxlength="4" value="{$cmb.TAHUN_UAN}"/>
                        <br/>
                        Jumlah Mata Pelajaran :
                        <input type="text" name="jumlah_pelajaran_uan" size="2" maxlength="2" value="{$cmb.JUMLAH_PELAJARAN_UAN}"/>
                        <br/>
                        Jumlah Nilai UAN :
                        <input type="text" name="nilai_uan" size="5" maxlength="5" value="{$cmb.NILAI_UAN}"/>
                        * Bukan rata-rata</td>
                </tr>
                <tr>
                    <th colspan="2">Data Keluarga - Ayah</td>
                </tr>
                <tr>
                    <td>Nama Ayah</td>
                    <td><input type="text" name="nama_ayah" maxlength="64"  value="{$cmb.NAMA_AYAH}"/></td>
                </tr>
                <tr>
                    <td>Alamat Ayah</td>
                    <td><input type="text" name="alamat_ayah" size="65" maxlength="200" value="{$cmb.ALAMAT_AYAH}" /></td>
                </tr>
                <tr>
                    <td>Kota</td>
                    <td>
                        <input type="hidden" name="id_kota_ayah" id="id-kota-ayah" value="{$cmb.ID_KOTA_AYAH}" />
                        <input type="text" name="nm_kota_ayah" id="nama-kota-ayah" size="50" value="{$cmb.NM_KOTA_AYAH}" readonly="readonly"/>
                        <button class="button"  id="button-kota-ayah">Edit</button>
                    </td>
                </tr>
                <tr>
                    <td>Telp</td>
                    <td><input type="text" name="telp_ayah"  maxlength="32" value="{$cmb.TELP_AYAH}"/></td>
                </tr>
                <tr>
                    <td>Pendidikan Ayah</td>
                    <td>
                        <select name="pendidikan_ayah">
                            {foreach $pendidikan_ortu_set as $po}
                                <option value="{$po.ID_PENDIDIKAN_ORTU}" {if $cmb.PENDIDIKAN_AYAH == $po.ID_PENDIDIKAN_ORTU}selected="selected"{/if}>{$po.NM_PENDIDIKAN_ORTU}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Jenis Pekerjaan Ayah</td>
                    <td>
                        <select name="pekerjaan_ayah">
                            <option value=""></option>
                            {foreach $data_pekerjaan as $p}
                                <option value="{$p.ID_PEKERJAAN}" {if $cmb.PEKERJAAN_AYAH == $p.ID_PEKERJAAN}selected="selected"{/if}>{$p.NM_PEKERJAAN}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Instansi / Perusahaan Tempat Kerja</td>
                    <td><input type="text" name="instansi_ayah" size="30" maxlength="50" value="{$cmb.INSTANSI_AYAH}" /></td>
                </tr>
                {*<tr>
                <td>Jabatan / Golongan</td>
                <td><input type="text" name="jabatan_ayah" size="30" maxlength="50" value="{$cmb.JABATAN_AYAH}" /></td>
                </tr>*}
                <tr>
                    <td>Masa Kerja (jika ada)</td>
                    <td><input type="text" name="masa_kerja_ayah" size="4" maxlength="4" value="{$cmb.MASA_KERJA_AYAH}" /></td>
                </tr>
                <tr>
                    <th colspan="2" >Data Keluarga - Ibu</td>
                </tr>
                <tr>
                    <td>Nama Ibu </td>
                    <td><input type="text" name="nama_ibu" maxlength="64"  value="{$cmb.NAMA_IBU}"/></td>
                </tr>
                <tr>
                    <td>Alamat Ibu</td>
                    <td><input type="text" name="alamat_ibu" size="65" maxlength="200" value="{$cmb.ALAMAT_IBU}" /></td>
                </tr>
                <tr>
                    <td>Kota</td>
                    <td>
                        <input type="hidden" name="id_kota_ibu" id="id-kota-ibu" value="{$cmb.ID_KOTA_IBU}" />
                        <input type="text" name="nm_kota_ibu" id="nama-kota-ibu" size="50" value="{$cmb.NM_KOTA_IBU}" readonly="readonly"/>
                        <button  class="button" id="button-kota-ibu">Edit</button>
                    </td>
                </tr>
                <tr>
                    <td>Telp</td>
                    <td><input type="text" name="telp_ibu"  maxlength="32" value="{$cmb.TELP_IBU}"/></td>
                </tr>
                <tr>
                    <td>Pendidikan Ibu</td>
                    <td>
                        <select name="pendidikan_ibu">
                            {foreach $pendidikan_ortu_set as $po}
                                <option value="{$po.ID_PENDIDIKAN_ORTU}" {if $cmb.PENDIDIKAN_IBU == $po.ID_PENDIDIKAN_ORTU}selected="selected"{/if}>{$po.NM_PENDIDIKAN_ORTU}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Jenis Pekerjaan Ibu</td>
                    <td>
                        <select name="pekerjaan_ibu">
                            <option value=""></option>
                            {foreach $data_pekerjaan as $p}
                                <option value="{$p.ID_PEKERJAAN}" {if $cmb.PEKERJAAN_IBU == $p.ID_PEKERJAAN}selected="selected"{/if}>{$p.NM_PEKERJAAN}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Instansi / Perusahaan Tempat Kerja</td>
                    <td><input type="text" name="instansi_ibu" size="30" maxlength="50" value="{$cmb.INSTANSI_IBU}" /></td>
                </tr>
                {*<tr>
                <td>Jabatan / Golongan</td>
                <td><input type="text" name="jabatan_ibu" size="30" maxlength="50" value="{$cmb.JABATAN_IBU}" /></td>
                </tr>*}
                <tr>
                    <td>Masa Kerja (jika ada)</td>
                    <td><input type="text" name="masa_kerja_ibu" size="4" maxlength="4" value="{$cmb.MASA_KERJA_IBU}" /></td>
                </tr>
                <tr>
                    <th colspan="2" >Data Keluarga - Lain-Lain</td>
                </tr>	
                <tr>
                    <td>Skala Usaha/Pekerjaan</td>
                    <td>
                        <select name="skala_pekerjaan_ortu">
                            {foreach $skala_pekerjaan_set as $sp}
                                <option value="{$sp.SKALA_PEKERJAAN}" {if $cmb.SKALA_PEKERJAAN_ORTU == $sp.SKALA_PEKERJAAN}selected="selected"{/if}>{$sp.NM_SKALA_PEKERJAAN}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Jumlah Kakak</td>
                    <td><input type="text" name="jumlah_kakak" size="2" maxlength="2"  value="{$cmb.JUMLAH_KAKAK}" /></td>
                </tr>
                <tr>
                    <td>Jumlah Adik</td>
                    <td><input type="text" name="jumlah_adik" size="2" maxlength="2"  value="{$cmb.JUMLAH_ADIK}" /></td>
                </tr>
                <tr>
                    <th colspan="2">Kediaman dan Kendaraan Keluarga</th>
                </tr>
                <tr>
                    <td>Kediaman Orang Tua</td>
                    <td>
                        <select name="kediaman_ortu">
                            {foreach $kediaman_ortu_set as $ko}
                                <option value="{$ko.KEDIAMAN_ORTU}" {if $cmb.KEDIAMAN_ORTU == $ko.KEDIAMAN_ORTU}selected="selected"{/if}>{$ko.NM_KEDIAMAN_ORTU}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Luas Tanah Kediaman</td>
                    <td>
                        <select name="luas_tanah">
                            {foreach $luas_tanah_set as $lt}
                                <option value="{$lt.LUAS_TANAH}" {if $cmb.LUAS_TANAH == $lt.LUAS_TANAH}selected="selected"{/if}>{$lt.NM_LUAS_TANAH}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Luas Bangunan Kediaman</td>
                    <td>
                        <select name="luas_bangunan">
                            {foreach $luas_bangunan_set as $lb}
                                <option value="{$lb.LUAS_BANGUNAN}" {if $cmb.LUAS_BANGUNAN == $lb.LUAS_BANGUNAN}selected="selected"{/if}>{$lb.NM_LUAS_BANGUNAN}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Nilai NJOP</td>
                    <td>
                        <select name="range_njop">
                            <option value=""></option>
                            {foreach $data_range_njop as $d}
                                {if $d.BATAS_BAWAH==''&&$d.BATAS_ATAS!=''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_NJOP}selected="true" {/if}> kurang dari/sama dengan {number_format($d.BATAS_ATAS)}</option>
                                {else if $d.BATAS_BAWAH!=''&&$d.BATAS_ATAS==''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_NJOP}selected="true" {/if}> lebih dari {number_format($d.BATAS_BAWAH)}</option>
                                {else}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_NJOP}selected="true" {/if}>{number_format($d.BATAS_BAWAH)} - {number_format($d.BATAS_ATAS)}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Tagihan PBB</td>
                    <td>
                        <select name="range_pbb">
                            <option value=""></option>
                            {foreach $data_range_pbb as $d}
                                {if $d.BATAS_BAWAH==''&&$d.BATAS_ATAS!=''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_PBB}selected="true" {/if}> kurang dari/sama dengan {number_format($d.BATAS_ATAS)}</option>
                                {else if $d.BATAS_BAWAH!=''&&$d.BATAS_ATAS==''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_PBB}selected="true" {/if}> lebih dari {number_format($d.BATAS_BAWAH)}</option>
                                {else}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_PBB}selected="true" {/if}>{number_format($d.BATAS_BAWAH)} - {number_format($d.BATAS_ATAS)}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Daya Listrik</td>
                    <td>
                        <select name="listrik">
                            {foreach $listrik_set as $l}
                                <option value="{$l.LISTRIK}" {if $cmb.LISTRIK == $l.LISTRIK}selected="selected"{/if}>{$l.NM_LISTRIK}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Tagihan Listrik <br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Tiap Bulan</i></td>
                    <td>
                        <select name="range_listrik">
                            <option value=""></option>
                            {foreach $data_range_listrik as $d}
                                {if $d.BATAS_BAWAH==''&&$d.BATAS_ATAS!=''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_LISTRIK}selected="true" {/if}> kurang dari/sama dengan {number_format($d.BATAS_ATAS)}</option>
                                {else if $d.BATAS_BAWAH!=''&&$d.BATAS_ATAS==''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_LISTRIK}selected="true" {/if}> lebih dari {number_format($d.BATAS_BAWAH)}</option>
                                {else}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_LISTRIK}selected="true" {/if}>{number_format($d.BATAS_BAWAH)} - {number_format($d.BATAS_ATAS)}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Tagihan Air<br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Tiap Bulan</i></td>
                    <td>
                        <select name="range_air">
                            <option value=""></option>
                            {foreach $data_range_air as $d}
                                {if $d.BATAS_BAWAH==''&&$d.BATAS_ATAS!=''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_AIR}selected="true" {/if}> kurang dari/sama dengan {number_format($d.BATAS_ATAS)}</option>
                                {else if $d.BATAS_BAWAH!=''&&$d.BATAS_ATAS==''}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_AIR}selected="true" {/if}> lebih dari {number_format($d.BATAS_BAWAH)}</option>
                                {else}
                                    <option value="{$d.ID_RANGE_JUMLAH}" {if $d.ID_RANGE_JUMLAH==$cmb.RANGE_AIR}selected="true" {/if}>{number_format($d.BATAS_BAWAH)} - {number_format($d.BATAS_ATAS)}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Jumlah Kendaraan Bermotor R4</td>
                    <td>
                        <input type="text" name="kendaraan_r4" size="4" maxlength="4" value="{$cmb.KENDARAAN_R4}" />
                    </td>
                </tr>
                <tr>
                    <td>Sebutkan Nama/Merek Kendaraan R4 Sesuai Jumlah</td>
                    <td>
                        <input type="text" name="merek_kendaraan_r4" size="50" maxlength="50" value="{$cmb.MEREK_KENDARAAN_R4}" />
                    </td>
                </tr>
                <tr>
                    <td>Tahun Kendaraan R4<br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Paling Terbaru</i></td>
                    <td><input type="text" name="tahun_kendaraan_r4" size="4" maxlength="4" value="{$cmb.TAHUN_KENDARAAN_R4}" /></td>
                </tr>
                <tr>
                    <td>Jumlah Kendaraan bermotor R2</td>
                    <td>
                        <input type="text" name="kendaraan_r2" size="4" maxlength="4" value="{$cmb.KENDARAAN_R2}" />
                    </td>
                </tr>
                <tr>
                    <td>Sebutkan Nama/Merek Kendaraan R2 Sesuai Jumlah</td>
                    <td>
                        <input type="text" name="merek_kendaraan_r2" size="50" maxlength="50" value="{$cmb.MEREK_KENDARAAN_R2}" />
                    </td>
                </tr>
                <tr>
                    <td>Tahun Kendaraan R2<br/><i style="font-size: 0.9em;color: green;font-family: Trebuchet MS">Paling Terbaru</i></td>
                    <td><input type="text" name="tahun_kendaraan_r2" size="4" maxlength="4" value="{$cmb.TAHUN_KENDARAAN_R2}" /></td>
                </tr>
                <tr>
                    <td>Jumlah Handphone</td>
                    <td><input type="text" name="jumlah_handphone" size="4" maxlength="4" value="{$cmb.JUMLAH_HANDPHONE}" /></td>
                </tr>
                <tr>
                    <td>Kepemilikan Kekayaan Lainya :</td>
                    <td>
                        <textarea name="kekayaan_lain" cols="75" rows="2" maxlength="256">{$cmb.KEKAYAAN_LAIN}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Jenis Transportasi Ke Kampus</td>
                    <td>
                        <select name="transportasi">
                            <option value=""></option>
                            <option value="Sepeda" {if $cmb.TRANSPORTASI=='Sepedah'||$cmb.TRANSPORTASI=='Sepeda'}selected="true"{/if}>Sepeda</option>
                            <option value="Motor" {if $cmb.TRANSPORTASI=='Motor'}selected="true"{/if}>Motor</option>
                            <option value="Mobil" {if $cmb.TRANSPORTASI=='Mobil'}selected="true"{/if}>Mobil</option>
                            <option value="Angkutan Umum" {if $cmb.TRANSPORTASI=='Angkutan Umum'}selected="true"{/if}>Angkutan Umum</option>
                            <option value="Jalan Kaki" {if $cmb.TRANSPORTASI=='Jalan Kaki'}selected="true"{/if}>Jalan Kaki</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Informasi lainnya, sebutkan :</td>
                    <td>
                        <textarea name="info_lain" cols="75" rows="2" maxlength="256">{$cmb.INFO_LAIN}</textarea>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">Data Penghasilan Ayah</th>
                </tr>
                <tr>
                    <td>Gaji</td>
                    <td><input type="text" name="gaji_ayah" id="pendapatan-ayah-1" size="20" maxlength="20" value="{$cmb.GAJI_AYAH}" style="text-align:right" class="" /></td>
                </tr>
                <tr>
                    <td>Tunjangan Keluarga</td>
                    <td><input type="text" name="tunjangan_keluarga_ayah" id="pendapatan-ayah-2" size="20" maxlength="20" value="{$cmb.TUNJANGAN_KELUARGA_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Jabatan / Golongan</td>
                    <td><input type="text" name="tunjangan_jabatan_ayah" id="pendapatan-ayah-3" size="20" maxlength="20" value="{$cmb.TUNJANGAN_JABATAN_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Sertifikasi Guru / Dosen</td>
                    <td><input type="text" name="tunjangan_sertifikasi_ayah" id="pendapatan-ayah-4" size="20" maxlength="20" value="{$cmb.TUNJANGAN_SERTIFIKASI_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Kehormatan</td>
                    <td><input type="text" name="tunjangan_kehormatan_ayah" id="pendapatan-ayah-5" size="20" maxlength="20" value="{$cmb.TUNJANGAN_KEHORMATAN_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Remunerasi</td>
                    <td><input type="text" name="renumerasi_ayah" id="pendapatan-ayah-6" size="20" maxlength="20" value="{$cmb.RENUMERASI_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan lain-lain</td>
                    <td><input type="text" name="tunjangan_lain_ayah" id="pendapatan-ayah-7" size="20" maxlength="20" value="{$cmb.TUNJANGAN_LAIN_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Penghasilan lain-lain</td>
                    <td><input type="text" name="penghasilan_lain_ayah" id="pendapatan-ayah-8" size="20" maxlength="20" value="{$cmb.PENGHASILAN_LAIN_AYAH}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <th colspan="2">Data Penghasilan Ibu </th>
                </tr>
                <tr>
                    <td>Gaji</td>
                    <td><input type="text" name="gaji_ibu" id="pendapatan-ibu-1" size="20" maxlength="20" value="{$cmb.GAJI_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Keluarga</td>
                    <td><input type="text" name="tunjangan_keluarga_ibu" id="pendapatan-ibu-2" size="20" maxlength="20" value="{$cmb.TUNJANGAN_KELUARGA_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Jabatan / Golongan</td>
                    <td><input type="text" name="tunjangan_jabatan_ibu" id="pendapatan-ibu-3" size="20" maxlength="20" value="{$cmb.TUNJANGAN_JABATAN_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Sertifikasi Guru / Dosen</td>
                    <td><input type="text" name="tunjangan_sertifikasi_ibu" id="pendapatan-ibu-4" size="20" maxlength="20" value="{$cmb.TUNJANGAN_SERTIFIKASI_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan Kehormatan Guru Besar</td>
                    <td><input type="text" name="tunjangan_kehormatan_ibu" id="pendapatan-ibu-5" size="20" maxlength="20" value="{$cmb.TUNJANGAN_KEHORMATAN_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Remunerasi</td>
                    <td><input type="text" name="renumerasi_ibu" size="20" id="pendapatan-ibu-6" maxlength="20" value="{$cmb.RENUMERASI_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Tunjangan lain-lain</td>
                    <td><input type="text" name="tunjangan_lain_ibu" size="20" id="pendapatan-ibu-7" maxlength="20" value="{$cmb.TUNJANGAN_LAIN_IBU}" style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <td>Penghasilan lain-lain</td>
                    <td><input type="text" name="penghasilan_lain_ibu" id="pendapatan-ibu-8" size="20" maxlength="20" value="{$cmb.PENGHASILAN_LAIN_IBU}"  style="text-align:right" class=""/></td>
                </tr>
                <tr>
                    <th colspan="2">Total Penghasilan Orang Tua</th>
                </tr>
                <tr>
                    <td>Total Penghasilan Orang Tua</td>
                    <td>
                        <input type="text" id="total-pendapatan-ortu" class="right-align" value="Rp {number_format($cmb.TOTAL_PENDAPATAN_ORTU,0,',','.')}" readonly="readonly" style="background-color: #dedede"/>
                        <input type="hidden" name="total_pendapatan_ortu" value="{$cmb.TOTAL_PENDAPATAN_ORTU}" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="center-align">
                        <input type="hidden" name="id_pilihan_1" value="{$cmb.ID_PILIHAN_1}" />
                        <input type="hidden" name="id_pilihan_2" value="{$cmb.ID_PILIHAN_2}"/>
                        <input type="hidden" name="id_pilihan_3" value="{$cmb.ID_PILIHAN_3}"/>
                        <input type="hidden" name="id_pilihan_4" value="{$cmb.ID_PILIHAN_4}"/>
                        <input type="hidden" name="id_kelompok_biaya" value="{$cmb.ID_KELOMPOK_BIAYA}" />
                        <input type="hidden" name="status_bidik_misi" value="{$cmb.STATUS_BIDIK_MISI}" />
                        <input class="button" type="submit" value="Save & Continue" />
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div id="dialog-kota"></div><div id="dialog-sekolah"></div>
{/block}