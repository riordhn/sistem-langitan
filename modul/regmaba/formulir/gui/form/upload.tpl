{extends '../layout.tpl'}
{block name='tabs'}
    {if $jalur=='SNMPTN'}
        <ul id="tabs">
            <li><a href="form.php?mode=biodata" >Biodata</a></li>
            <li><a id="current" >Upload</a></li>
            <li><a>Jadwal</a></li>
            <li><a>Hasil Tes</a></li>
        </ul>
    {else if $jalur=='MANDIRI'||$jalur=='SBMPTN'}
        <ul id="tabs">
            <li><a href="form.php?mode=biodata" >Biodata</a></li>
            <li><a id="current" >Upload</a></li>
            <li><a>Verifikasi</a></li>
            <li><a>Hasil Tes</a></li>
        </ul>
    {/if}
{/block}
{block name='main-title'}Formulir Online Pendaftaran Mahasiswa Baru Universitas Airlangga{/block}
{block name='content'}
    <div>
        <table id="table-formulir" style="display: none;font-size: 0.8em">
            <caption>Registrasi Mahasiswa Baru Program Sarjana (S1) Universitas Airlangga</caption>
            {* INFORMASI *}

            <tr>
                <td colspan="2">
                    <div class="ui-widget">
                        <div class="ui-state-highlight ui-corner-all" style="padding: 10px 5px 0px 10px;margin: 10px"> 
                            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                                <strong>Info :</strong><br/>Upload file dilakukan satu-persatu, setelah memilih file lalu tekan tombol upload yang terdapat pada sebelah kiri dan seterusnya pada file lain.
                                Jika upload file berhasil maka di kolom sebelah kiri akan keluar file yang berhasil terupload
                            </p>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th colspan="2">Upload File Berkas</th>
            </tr>
            <tr>
                <td>File Ijazah / Surat Keterangan Lulus
                    {if $cmb.FILE_IJAZAH}
                        <a target="_blank" href="/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_ijazah.pdf">{$cmb.ID_C_MHS}_ijazah.pdf</a>
                    {/if}
                </td>
                <td>
                    <form action="form.php?mode=upload" method="post" enctype="multipart/form-data">
                        <input class="button" type="submit" value="Upload" />
                        <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
                        <input type="hidden" name="mode" value="upload" />
                        <input type="hidden" id="status_file_ijazah" {if $cmb.FILE_IJAZAH}value="1"{/if} />
                        <input type="file" accept="application/pdf" name="file_ijazah"/>
                    </form>
                </td>
            </tr>
            <tr>
                <td>File SKHUN/ SKHUN Sementara
                    {if $cmb.FILE_SKHUN}
                        <a target="_blank" href="/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_skhun.pdf">{$cmb.ID_C_MHS}_skhun.pdf</a>
                    {/if}
                    <label class="small-text">Bisa menyusul</label>
                </td>
                <td>
                    <form action="form.php?mode=upload" method="post" enctype="multipart/form-data">
                        <input class="button" type="submit" value="Upload" />
                        <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
                        <input type="hidden" name="mode" value="upload" />
                        <input type="hidden" id="status_file_skhun" {if $cmb.FILE_SKHUN}value="1"{/if} />
                        <input type="file" accept="application/pdf" name="file_skhun"/>
                    </form>
                </td>
            </tr>
            <tr>
                <td>File Akte Kelahiran
                    {if $cmb.FILE_AKTE}
                        <a target="_blank" href="/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_akte.pdf">{$cmb.ID_C_MHS}_akte.pdf</a>
                    {/if}
                </td>
                <td>
                    <form action="form.php?mode=upload" method="post" enctype="multipart/form-data">
                        <input class="button" type="submit" value="Upload" />
                        <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
                        <input type="hidden" name="mode" value="upload" />
                        <input type="hidden" id="status_file_akte" {if $cmb.FILE_AKTE}value="1"{/if} />
                        <input type="file" accept="application/pdf" name="file_akte"/>
                    </form>
                </td>
            </tr>
            <tr>
                <td>File KK
                    {if $cmb.FILE_KK}
                        <a target="_blank" href="/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_kk.pdf">{$cmb.ID_C_MHS}_kk.pdf</a>
                    {/if}

                </td>
                <td>
                    <form action="form.php?mode=upload" method="post" enctype="multipart/form-data">
                        <input class="button" type="submit" value="Upload" />
                        <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
                        <input type="hidden" name="mode" value="upload" />
                        <input type="hidden" id="status_file_kk" {if $cmb.FILE_KK}value="1"{/if} />
                        <input type="file" accept="application/pdf" name="file_kk"/>
                    </form>
                </td>
            </tr>
            <tr>
                <td>File Keterangan penghasilan bruto dan pekerjaan serta jabatan ortu
                    {if $cmb.FILE_PENGHASILAN}
                        <a target="_blank" href="/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_penghasilan.pdf">{$cmb.ID_C_MHS}_penghasilan.pdf</a>
                    {/if}
                </td>
                <td>
                    <form action="form.php?mode=upload" method="post" enctype="multipart/form-data">
                        <input class="button" type="submit" value="Upload" />
                        <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
                        <input type="hidden" name="mode" value="upload" />
                        <input type="hidden" id="status_file_penghasilan" {if $cmb.FILE_PENGHASILAN}value="1"{/if} />

                        <input type="file" accept="application/pdf" name="file_penghasilan"/>
                    </form>
                </td>
            </tr>
            <tr>
                <td>File Akta Pendirian / SIUP bagi Wiraswasta/ Pengusaha
                    {if $cmb.FILE_SIUP}
                        <a target="_blank" href="/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_siup.pdf">{$cmb.ID_C_MHS}_siup.pdf</a>
                    {/if}
                </td>
                <td>
                    <form action="form.php?mode=upload" method="post" enctype="multipart/form-data">
                        <input class="button" type="submit" value="Upload" />
                        <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
                        <input type="hidden" name="mode" value="upload" />
                        <input type="hidden" id="status_file_siup" {if $cmb.FILE_SIUP}value="1"{/if} />
                        <input type="file" accept="application/pdf" name="file_siup"/>
                    </form>
                </td>
            </tr>
            <tr>
                <td>File Surat keterangan luas lahan petani
                    {if $cmb.FILE_PETANI}
                        <a target="_blank" href="/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_petani.pdf">{$cmb.ID_C_MHS}_petani.pdf</a>
                    {/if}
                </td>
                <td>
                    <form action="form.php?mode=upload" method="post" enctype="multipart/form-data">
                        <input class="button" type="submit" value="Upload" />
                        <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
                        <input type="hidden" name="mode" value="upload" />
                        <input type="hidden" id="status_file_petani" {if $cmb.FILE_PETANI}value="1"{/if} />
                        <input type="file" accept="application/pdf" name="file_petani"/>
                    </form>
                </td>
            </tr>
            <tr>
                <td>File SPPT PBB (NJOP) tahun terakhir
                    {if $cmb.FILE_SPPT_PBB}
                        <a target="_blank" href="/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_sppt_pbb.pdf">{$cmb.ID_C_MHS}_sppt_pbb.pdf</a>
                    {/if}
                </td>
                <td>
                    <form action="form.php?mode=upload" method="post" enctype="multipart/form-data">
                        <input class="button" type="submit" value="Upload" />
                        <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
                        <input type="hidden" name="mode" value="upload" />
                        <input type="hidden" id="status_file_sppt_pbb" {if $cmb.FILE_SPPT_PBB}value="1"{/if} />
                        <input type="file" accept="application/pdf" name="file_sppt_pbb"/>
                    </form>
                </td>
            </tr>
            <tr>
                <td>File rekening listrik
                    {if $cmb.FILE_LISTRIK}
                        <a target="_blank" href="/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_listrik.pdf">{$cmb.ID_C_MHS}_listrik.pdf</a>
                    {/if}
                </td>
                <td>
                    <form action="form.php?mode=upload" method="post" enctype="multipart/form-data">
                        <input class="button" type="submit" value="Upload" />
                        <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
                        <input type="hidden" name="mode" value="upload" />
                        <input type="hidden" id="status_file_listrik" {if $cmb.FILE_LISTRIK}value="1"{/if} />
                        <input type="file" accept="application/pdf" name="file_listrik"/>
                    </form>
                </td>
            </tr>
            <tr>
                <td>File rekening air
                    {if $cmb.FILE_AIR}
                        <a target="_blank" href="/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_air.pdf">{$cmb.ID_C_MHS}_air.pdf</a>
                    {/if}
                </td>
                <td>
                    <form action="form.php?mode=upload" method="post" enctype="multipart/form-data">
                        <input class="button" type="submit" value="Upload" />
                        <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
                        <input type="hidden" name="mode" value="upload" />
                        <input type="hidden" id="status_file_air" {if $cmb.FILE_AIR}value="1"{/if} />
                        <input type="file" accept="application/pdf" name="file_air"/>
                    </form>
                </td>
            </tr>
            <tr>
                <td>File STNK Motor (Jika ada)
                    {if $cmb.FILE_STNK_MOTOR}
                        <a target="_blank" href="/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_stnk_motor.pdf">{$cmb.ID_C_MHS}_stnk_motor.pdf</a>
                    {/if}
                </td>
                <td>
                    <form action="form.php?mode=upload" method="post" enctype="multipart/form-data">
                        <input class="button" type="submit" value="Upload" />
                        <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
                        <input type="hidden" name="mode" value="upload" />
                        <input type="hidden" id="status_stnk_motor" {if $cmb.FILE_STNK_MOTOR}value="1"{/if} />
                        <input type="file" accept="application/pdf" name="file_stnk_motor"/>
                    </form>
                </td>
            </tr>
            <tr>
                <td>File STNK Mobil (Jika ada)
                    {if $cmb.FILE_STNK_MOBIL}
                        <a target="_blank" href="/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_stnk_mobil.pdf">{$cmb.ID_C_MHS}_stnk_mobil.pdf</a>
                    {/if}
                </td>
                <td>
                    <form action="form.php?mode=upload" method="post" enctype="multipart/form-data">
                        <input class="button" type="submit" value="Upload" />
                        <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
                        <input type="hidden" name="mode" value="upload" />
                        <input type="hidden" id="status_stnk_mobil" {if $cmb.FILE_STNK_MOBIL}value="1"{/if} />
                        <input type="file" accept="application/pdf" name="file_stnk_mobil"/>
                    </form>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p>* Semua file upload harus bertipe (*.pdf) dan besar per file tidak lebih dari 30 MB.</p>
                    <p>* Untuk mengkonversi hasil scan berkas yg berupa image (*.jpg) ke (*.pdf), bisa menggunakan aplikasi berikut ini : <a target="_blank" href="http://www.softsea.com/download.php?id=836715621">JPEG to PDF</a></p>
                    <p>* Petunjuk penggunaan aplikasi JPEG to PDF <a target="_blank" href="/img/maba/tutorial-jpegtopdf.png">klik disini</a></p>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="center-align">
                    <a href="form.php?mode={$next_page}"  class="button" >Continue</a>
                </td>
            </tr>
        </table>
    </div>
    <div id="dialog-kota"></div><div id="dialog-sekolah"></div>
{/block}