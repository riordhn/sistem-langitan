<html>
<head>
    <title>Regmaba Mandiri (S1, Alih Jenis, Diploma 3) - Universitas Airlangga</title>
    <meta name="google-site-verification" content="ihkYFpHX_zbP4SfrkqIEF7c2Lp08dZFIxJ2MKI6m_Hk" />
    <link rel="shortcut icon" href="http://cybercampus.unair.ac.id/img/icon.ico"/>
    <link rel="stylesheet" type="text/css" href="http://cybercampus.unair.ac.id/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="http://regmaba.unair.ac.id/css/style.css" />
    <meta name="description" content="Halaman registrasi ulang mahasiswa baru jalur mandiri Universitas Airlangga">
</head>

<body>
    <div class="wrap-box">
        <div class="login-box">
            <form action="index.php" method="post">
            <input type="hidden" name="mode" value="login">
            <table>
                <tr>
                    <td class="right-align"><strong>No Ujian</strong></td>
                    <td>
                        <input name="no_ujian" type="text" maxlength="12" style="width: 110px">
                    </td>
                </tr>
                <tr>
                    <td class="right-align"><strong>Kode Voucher</strong></td>
                    <td>
                        <input name="kode_voucher" type="text" maxlength="16" style="width: 110px" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button onclick="window.location = '/index.php'; return false;">Kembali</button>
                        <input type="submit" value="Login" />
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</body>
</html>