<?php
//die('Halaman tidak ditemukan');
include '../../../config.php';
include '../../registrasi/class/CalonMahasiswa.class.php';

$smarty->setTemplateDir('../gui');
$smarty->setCompileDir('../gui_c');

$CalonMahasiswa = new CalonMahasiswa($db);

$mode = get('mode');

if ($request_method == 'POST')
{
    if (post('mode') == 'login')
    {
        $id_c_mhs = $CalonMahasiswa->LoginRegmabaMandiri(post('no_ujian'), post('kode_voucher'));
        
        if ($id_c_mhs['ID_C_MHS'] != '')
        {
            // Memastikan login sestelah waktu pengumuman
            if (time() > strtotime($id_c_mhs['TGL_PENGUMUMAN']))
            {
                $_SESSION['ID_C_MHS'] = $id_c_mhs['ID_C_MHS'];
                $_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] = $id_c_mhs['TGL_VERIFIKASI_PENDIDIKAN'];
            }
        }
    }
}

if ($_SESSION['ID_C_MHS'] != '' and $_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] == '')
{
    header("Location: /form.php");
    exit();
}elseif ($_SESSION['ID_C_MHS'] != '' and $_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] != ''){
	header("Location: /form.php?mode=hasil-kesehatan");
    exit();
}
else
{
    $smarty->display("login/mandiri.tpl");
}
?>