<?php
if ($_SERVER['HTTP_REFERER'] != 'http://regmaba.unair.ac.id/') { header('location: http://regmaba.unair.ac.id/index.php?mode=logout'); exit(); }

include('config.php');

$id_c_mhs = $_SESSION['maba'];

if ($request_method == 'POST')
{
    if (post('mode') == 'save_s2' or post('mode') == 'save_pr')
    {
        // biodata
        $db->Parse("
            UPDATE CALON_MAHASISWA_BARU SET
                NM_C_MHS = :nm_c_mhs,
                GELAR = :gelar,
                ID_KOTA_LAHIR = :id_kota_lahir,
                TGL_LAHIR = :tgl_lahir,
                ALAMAT = :alamat,
                ID_KOTA = :id_kota,
                TELP = :telp,
                EMAIL = :email,
                JENIS_KELAMIN = :jenis_kelamin,
                KEWARGANEGARAAN = :kewarganegaraan,
                KEWARGANEGARAAN_LAIN = :kewarganegaraan_lain,
                ID_AGAMA = :id_agama,
                STATUS_PERKAWINAN = :status_perkawinan,
                
                SUMBER_BIAYA = :sumber_biaya,
                TGL_REGMABA = TO_DATE(:tgl_regmaba, 'YYYYMMDDHH24MISS')
                
            WHERE ID_C_MHS = :id_c_mhs");

        $db->BindByName(':id_c_mhs', $id_c_mhs);

        $db->BindByName(':nm_c_mhs', post('nm_c_mhs'));
        $db->BindByName(':gelar', post('gelar'));
        $db->BindByName(':id_kota_lahir', post('id_kota_lahir'));
        $db->BindByName(':tgl_lahir', date('d-M-Y', mktime(0, 0, 0, intval(post('tgl_lahir_Month')), intval(post('tgl_lahir_Day')), post('tgl_lahir_Year'))));
        $db->BindByName(':alamat', post('alamat'));
        $db->BindByName(':id_kota', post('id_kota'));
        $db->BindByName(':telp', post('telp'));
        $db->BindByName(':email', post('email'));

        $db->BindByName(':jenis_kelamin', post('jenis_kelamin'));
        $db->BindByName(':kewarganegaraan', post('kewarganegaraan'));
        $db->BindByName(':kewarganegaraan_lain', post('kewarganegaraan_lain'));
        $db->BindByName(':id_agama', post('id_agama'));
        $db->BindByName(':status_perkawinan', post('status_perkawinan'));

        $db->BindByName(':sumber_biaya', post('sumber_biaya'));
        $db->BindByName(':tgl_regmaba', $tgl_regmaba = date('YmdHis'));

        $db->Execute();
        
        $db->Parse("
            UPDATE CALON_MAHASISWA_PASCA SET
                PEKERJAAN = :pekerjaan,
                ASAL_INSTANSI = :asal_instansi,
                ALAMAT_INSTANSI = :alamat_instansi,
                TELP_INSTANSI = :telp_instansi,
                NRP = :nrp,
                KARPEG = :karpeg,
                PANGKAT = :pangkat,
                
                PTN_S1 = :ptn_s1,
                STATUS_PTN_S1 = :status_ptn_s1,
                PRODI_S1 = :prodi_s1,
                TGL_MASUK_S1 = :tgl_masuk_s1,
                TGL_LULUS_S1 = :tgl_lulus_s1,
                LAMA_STUDI_S1 = :lama_studi_s1,
                IP_S1 = :ip_s1,
                
                JUMLAH_KARYA_ILMIAH = :jumlah_karya_ilmiah,
                
                STATUS_LAMARAN_PPS = :status_lamaran_pps,
                TAHUN_LAMARAN_PPS = :tahun_lamaran_pps

            WHERE ID_C_MHS = :id_c_mhs");

        $db->BindByName(':id_c_mhs', $id_c_mhs);

        $db->BindByName(':pekerjaan', post('pekerjaan'));
        $db->BindByName(':asal_instansi', post('asal_instansi'));
        $db->BindByName(':alamat_instansi', post('alamat_instansi'));
        $db->BindByName(':telp_instansi', post('telp_instansi'));
        $db->BindByName(':nrp', post('nrp'));
        $db->BindByName(':karpeg', post('karpeg'));
        $db->BindByName(':pangkat', post('pangkat'));

        $db->BindByName(':ptn_s1', post('ptn_s1'));
        $db->BindByName(':status_ptn_s1', post('status_ptn_s1'));
        $db->BindByName(':prodi_s1', post('prodi_s1'));
        $db->BindByName(':tgl_masuk_s1', date('d-M-Y', mktime(0, 0, 0, post('tgl_masuk_s1_Month'), post('tgl_masuk_s1_Day'), post('tgl_masuk_s1_Year'))));
        $db->BindByName(':tgl_lulus_s1', date('d-M-Y', mktime(0, 0, 0, post('tgl_lulus_s1_Month'), post('tgl_lulus_s1_Day'), post('tgl_lulus_s1_Year'))));
        $db->BindByName(':lama_studi_s1', post('lama_studi_s1'));
        $db->BindByName(':ip_s1', post('ip_s1'));
        $db->BindByName(':jumlah_karya_ilmiah', post('jumlah_karya_ilmiah'));

        $db->BindByName(':status_lamaran_pps', post('status_lamaran_pps'));
        $db->BindByName(':tahun_lamaran_pps', post('tahun_lamaran_pps'));

        $result = $db->Execute();
    }
    
    if (post('mode') == 'save_s3' or post('mode') == 'save_sp')
    {
        // biodata
        $db->Parse("
            UPDATE CALON_MAHASISWA_BARU SET
                NM_C_MHS = :nm_c_mhs,
                GELAR = :gelar,
                ID_KOTA_LAHIR = :id_kota_lahir,
                TGL_LAHIR = :tgl_lahir,
                ALAMAT = :alamat,
                ID_KOTA = :id_kota,
                TELP = :telp,
                EMAIL = :email,
                JENIS_KELAMIN = :jenis_kelamin,
                KEWARGANEGARAAN = :kewarganegaraan,
                KEWARGANEGARAAN_LAIN = :kewarganegaraan_lain,
                ID_AGAMA = :id_agama,
                STATUS_PERKAWINAN = :status_perkawinan,
                
                SUMBER_BIAYA = :sumber_biaya,
                TGL_REGMABA = TO_DATE(:tgl_regmaba, 'YYYYMMDDHH24MISS')
                
            WHERE ID_C_MHS = :id_c_mhs");

        $db->BindByName(':id_c_mhs', $id_c_mhs);

        $db->BindByName(':nm_c_mhs', post('nm_c_mhs'));
        $db->BindByName(':gelar', post('gelar'));
        $db->BindByName(':id_kota_lahir', post('id_kota_lahir'));
        $db->BindByName(':tgl_lahir', date('d-M-Y', mktime(0, 0, 0, intval(post('tgl_lahir_Month')), intval(post('tgl_lahir_Day')), post('tgl_lahir_Year'))));
        $db->BindByName(':alamat', post('alamat'));
        $db->BindByName(':id_kota', post('id_kota'));
        $db->BindByName(':telp', post('telp'));
        $db->BindByName(':email', post('email'));

        $db->BindByName(':jenis_kelamin', post('jenis_kelamin'));
        $db->BindByName(':kewarganegaraan', post('kewarganegaraan'));
        $db->BindByName(':kewarganegaraan_lain', post('kewarganegaraan_lain'));
        $db->BindByName(':id_agama', post('id_agama'));
        $db->BindByName(':status_perkawinan', post('status_perkawinan'));

        $db->BindByName(':sumber_biaya', post('sumber_biaya'));
        $db->BindByName(':tgl_regmaba', $tgl_regmaba = date('YmdHis'));

        $db->Execute();
        
        $db->Parse("
            UPDATE CALON_MAHASISWA_PASCA SET
                PEKERJAAN = :pekerjaan,
                ASAL_INSTANSI = :asal_instansi,
                ALAMAT_INSTANSI = :alamat_instansi,
                TELP_INSTANSI = :telp_instansi,
                NRP = :nrp,
                KARPEG = :karpeg,
                PANGKAT = :pangkat,
                
                PTN_S1 = :ptn_s1,
                STATUS_PTN_S1 = :status_ptn_s1,
                PRODI_S1 = :prodi_s1,
                TGL_MASUK_S1 = :tgl_masuk_s1,
                TGL_LULUS_S1 = :tgl_lulus_s1,
                LAMA_STUDI_S1 = :lama_studi_s1,
                IP_S1 = :ip_s1,
                
                PTN_S2 = :ptn_s2,
                STATUS_PTN_S2 = :status_ptn_s2,
                PRODI_S2 = :prodi_s2,
                TGL_MASUK_S2 = :tgl_masuk_s2,
                TGL_LULUS_S2 = :tgl_lulus_s2,
                LAMA_STUDI_S2 = :lama_studi_s2,
                IP_S2 = :ip_s2,
                
                JUMLAH_KARYA_ILMIAH = :jumlah_karya_ilmiah,
                
                STATUS_LAMARAN_PPS = :status_lamaran_pps,
                TAHUN_LAMARAN_PPS = :tahun_lamaran_pps

            WHERE ID_C_MHS = :id_c_mhs");

        $db->BindByName(':id_c_mhs', $id_c_mhs);

        $db->BindByName(':pekerjaan', post('pekerjaan'));
        $db->BindByName(':asal_instansi', post('asal_instansi'));
        $db->BindByName(':alamat_instansi', post('alamat_instansi'));
        $db->BindByName(':telp_instansi', post('telp_instansi'));
        $db->BindByName(':nrp', post('nrp'));
        $db->BindByName(':karpeg', post('karpeg'));
        $db->BindByName(':pangkat', post('pangkat'));

        $db->BindByName(':ptn_s1', post('ptn_s1'));
        $db->BindByName(':status_ptn_s1', post('status_ptn_s1'));
        $db->BindByName(':prodi_s1', post('prodi_s1'));
        $db->BindByName(':tgl_masuk_s1', date('d-M-Y', mktime(0, 0, 0, post('tgl_masuk_s1_Month'), post('tgl_masuk_s1_Day'), post('tgl_masuk_s1_Year'))));
        $db->BindByName(':tgl_lulus_s1', date('d-M-Y', mktime(0, 0, 0, post('tgl_lulus_s1_Month'), post('tgl_lulus_s1_Day'), post('tgl_lulus_s1_Year'))));
        $db->BindByName(':lama_studi_s1', post('lama_studi_s1'));
        $db->BindByName(':ip_s1', post('ip_s1'));
        
        $db->BindByName(':ptn_s2', post('ptn_s2'));
        $db->BindByName(':status_ptn_s2', post('status_ptn_s2'));
        $db->BindByName(':prodi_s2', post('prodi_s2'));
        $db->BindByName(':tgl_masuk_s2', date('d-M-Y', mktime(0, 0, 0, post('tgl_masuk_s2_Month'), post('tgl_masuk_s2_Day'), post('tgl_masuk_s2_Year'))));
        $db->BindByName(':tgl_lulus_s2', date('d-M-Y', mktime(0, 0, 0, post('tgl_lulus_s2_Month'), post('tgl_lulus_s2_Day'), post('tgl_lulus_s2_Year'))));
        $db->BindByName(':lama_studi_s2', post('lama_studi_s2'));
        $db->BindByName(':ip_s2', post('ip_s2'));
        
        $db->BindByName(':jumlah_karya_ilmiah', post('jumlah_karya_ilmiah'));

        $db->BindByName(':status_lamaran_pps', post('status_lamaran_pps'));
        $db->BindByName(':tahun_lamaran_pps', post('tahun_lamaran_pps'));

        $result = $db->Execute();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{   
    // melakukan seleksi semua dari tabel calon mahasiswa baru
    $db->Query("select * from calon_mahasiswa_baru where id_c_mhs = {$id_c_mhs}");
    $cmb = $db->FetchAssoc();
    
    // tabel pasca
    $db->Query("select * from calon_mahasiswa_pasca where id_c_mhs = {$id_c_mhs}");
    $cmp = $db->FetchAssoc();
    
    // perbaikan tanggal lahir, tgl masuk, tgl lulus
    $cmb['TGL_LAHIR'] = date_to_timestamp($cmb['TGL_LAHIR']);
    $cmp['TGL_MASUK_S1'] = date_to_timestamp($cmp['TGL_MASUK_S1']);
    $cmp['TGL_LULUS_S1'] = date_to_timestamp($cmp['TGL_LULUS_S1']);
    $cmp['TGL_MASUK_S2'] = date_to_timestamp($cmp['TGL_MASUK_S2']);
    $cmp['TGL_LULUS_S2'] = date_to_timestamp($cmp['TGL_LULUS_S2']);
    
    // mendapatkan data jenjang
    $db->Query("select id_jenjang from penerimaan where id_penerimaan = {$cmb['ID_PENERIMAAN']}");
    $row = $db->FetchAssoc();
    $id_jenjang = $row['ID_JENJANG'];
    
    // mendapatkan program studi, prodi minat, dan kelas pilihannya
    $db->Query("
        select nm_fakultas, nm_program_studi, nm_jenjang, nm_prodi_minat, cmp.kelas_pilihan, pk.nm_prodi_kelas from calon_mahasiswa_baru cmb
        join program_studi ps on ps.id_program_studi = cmb.id_program_studi
        join fakultas f on f.id_fakultas = ps.id_fakultas
        join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
        join jenjang j on j.id_jenjang = p.id_jenjang
        join calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
        left join prodi_minat pm on pm.id_prodi_minat = cmp.id_prodi_minat
        left join prodi_kelas pk on (pk.id_program_studi = ps.id_program_studi and pk.singkatan = cmp.kelas_pilihan)
        where cmb.id_c_mhs = {$id_c_mhs}");
    $row = $db->FetchAssoc();
    $smarty->assign('ps', $row);
    
    // data provinsi set
    $provinsi_set = $db->QueryToArray("select id_provinsi, nm_provinsi from provinsi order by id_provinsi");
    for ($i = 0; $i < count($provinsi_set); $i++)
        $provinsi_set[$i]['kota_set'] = $db->QueryToArray("
            select id_kota, nm_kota from kota where id_provinsi = {$provinsi_set[$i]['ID_PROVINSI']} order by nm_kota");
    $smarty->assign('provinsi_set', $provinsi_set);
    
    // Assign data ke template
    $smarty->assign('cm', $cmb);
    $smarty->assign('cmp', $cmp);
    
    
    if ($id_jenjang == '1')
        $smarty->display("form/s1.tpl");
    
    if ($id_jenjang == '2')
        $smarty->display("form/s2.tpl");
    
    if ($id_jenjang == '3')
        $smarty->display("form/s3.tpl");
    
    if ($id_jenjang == '5')
        $smarty->display("form/d3.tpl");
    
    if ($id_jenjang == '9')
        $smarty->display("form/pr.tpl");
    
    if ($id_jenjang == '10')
        $smarty->display("form/sp.tpl");
}
?>
