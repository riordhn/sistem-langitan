<?php
include('config.php');

if (!isset($_SESSION['maba'])) { echo "<b>Session telah Habis, silahkan logout dan login kembali</b>"; exit(); }

$id_c_mhs = $_SESSION['maba'];

if ($request_method == 'POST')
{
    if (post('mode') == 'save_biodata_pasca')
    {
        // mendapatkan status
        $db->Query("SELECT STATUS FROM CALON_MAHASISWA WHERE ID_C_MHS = {$id_c_mhs}");
        $row = $db->FetchAssoc();
        $status_cmhs = $row['STATUS'];
        
        // data calon mahasisswa

        $db->Parse("
            UPDATE CALON_MAHASISWA SET
                ID_KOTA_LAHIR = :id_kota_lahir,
                TGL_LAHIR = :tgl_lahir,
                ALAMAT = :alamat,
                TELP = :telp,
                EMAIL = :email,
                JENIS_KELAMIN = :jenis_kelamin,
                KEWARGANEGARAAN = :kewarganegaraan,
                KEWARGANEGARAAN_LAIN = :kewarganegaraan_lain,
                ID_AGAMA = :id_agama,
                STATUS_PERKAWINAN = :status_perkawinan,

                SUMBER_BIAYA = :sumber_biaya,
                
                STATUS = :status

            WHERE ID_C_MHS = :id_c_mhs");

        $db->BindByName(':id_c_mhs', $id_c_mhs);

        $db->BindByName(':nm_c_mhs', post('nm_c_mhs'));
        $db->BindByName(':id_kota_lahir', post('id_kota_lahir'));
        $db->BindByName(':tgl_lahir', date('d-M-Y', mktime(0, 0, 0, intval(post('tgl_lahir_Month')), intval(post('tgl_lahir_Day')), post('tgl_lahir_Year'))));
        $db->BindByName(':alamat', post('alamat'));
        $db->BindByName(':telp', post('telp'));
        $db->BindByName(':email', post('email'));

        $db->BindByName(':jenis_kelamin', post('jenis_kelamin'));
        $db->BindByName(':kewarganegaraan', post('kewarganegaraan'));
        $db->BindByName(':kewarganegaraan_lain', post('kewarganegaraan_lain'));
        $db->BindByName(':id_agama', post('id_agama'));
        $db->BindByName(':status_perkawinan', post('status_perkawinan'));

        $db->BindByName(':sumber_biaya', post('sumber_biaya'));
		
        if ($status_cmhs == 0 || $status_cmhs == 4) $status_cmhs = 6;
        $db->BindByName(':status', $status_cmhs);

        $db->Execute();
        
        // data calon mahasiswa pasca
        
        $db->Parse("
            UPDATE CALON_MAHASISWA_PASCA SET
                PEKERJAAN = :pekerjaan,
                ASAL_INSTANSI = :asal_instansi,
                ALAMAT_INSTANSI = :alamat_instansi,
                TELP_INSTANSI = :telp_instansi,
                NRP = :nrp,
                KARPEG = :karpeg,
                PANGKAT = :pangkat,
                
                PTN_S1 = :ptn_s1,
                STATUS_PTN_S1 = :status_ptn_s1,
                PRODI_S1 = :prodi_s1,
                TGL_MASUK_S1 = :tgl_masuk_s1,
                TGL_LULUS_S1 = :tgl_lulus_s1,
                LAMA_STUDI_S1 = :lama_studi_s1,
                IP_S1 = :ip_s1,
                
                PTN_S2 = :ptn_s2,
                STATUS_PTN_S2 = :status_ptn_s2,
                PRODI_S2 = :prodi_s2,
                TGL_MASUK_S2 = :tgl_masuk_s2,
                TGL_LULUS_S2 = :tgl_lulus_s2,
                LAMA_STUDI_S2 = :lama_studi_s2,
                IP_S2 = :ip_s2,
                
                JUMLAH_KARYA_ILMIAH = :jumlah_karya_ilmiah,
                
                STATUS_LAMARAN_PPS = :status_lamaran_pps,
                TAHUN_LAMARAN_PPS = :tahun_lamaran_pps

            WHERE ID_C_MHS = :id_c_mhs");

        $db->BindByName(':id_c_mhs', $id_c_mhs);

        $db->BindByName(':pekerjaan', post('pekerjaan'));
        $db->BindByName(':asal_instansi', post('asal_instansi'));
        $db->BindByName(':alamat_instansi', post('alamat_instansi'));
        $db->BindByName(':telp_instansi', post('telp_instansi'));
        $db->BindByName(':nrp', post('nrp'));
        $db->BindByName(':karpeg', post('karpeg'));
        $db->BindByName(':pangkat', post('pangkat'));

        $db->BindByName(':ptn_s1', post('ptn_s1'));
        $db->BindByName(':status_ptn_s1', post('status_ptn_s1'));
        $db->BindByName(':prodi_s1', post('prodi_s1'));
        $db->BindByName(':tgl_masuk_s1', date('d-M-Y', mktime(0, 0, 0, post('tgl_masuk_s1_Month'), post('tgl_masuk_s1_Day'), post('tgl_masuk_s1_Year'))));
        $db->BindByName(':tgl_lulus_s1', date('d-M-Y', mktime(0, 0, 0, post('tgl_lulus_s1_Month'), post('tgl_lulus_s1_Day'), post('tgl_lulus_s1_Year'))));
        $db->BindByName(':lama_studi_s1', post('lama_studi_s1'));
        $db->BindByName(':ip_s1', post('ip_s1'));

        $db->BindByName(':ptn_s2', post('ptn_s2'));
        $db->BindByName(':status_ptn_s2', post('status_ptn_s2'));
        $db->BindByName(':prodi_s2', post('prodi_s2'));
        $db->BindByName(':tgl_masuk_s2', date('d-M-Y', mktime(0, 0, 0, post('tgl_masuk_s2_Month'), post('tgl_masuk_s2_Day'), post('tgl_masuk_s2_Year'))));
        $db->BindByName(':tgl_lulus_s2', date('d-M-Y', mktime(0, 0, 0, post('tgl_lulus_s2_Month'), post('tgl_lulus_s2_Day'), post('tgl_lulus_s2_Year'))));
        $db->BindByName(':lama_studi_s2', post('lama_studi_s2'));
        $db->BindByName(':ip_s2', post('ip_s2'));

        $db->BindByName(':jumlah_karya_ilmiah', post('jumlah_karya_ilmiah'));

        $db->BindByName(':status_lamaran_pps', post('status_lamaran_pps'));
        $db->BindByName(':tahun_lamaran_pps', post('tahun_lamaran_pps'));

        $db->Execute();

        $db->Close();
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{   
    // Tabel Calon Mahasiswa
    $db->Query("
        SELECT
            CM.ID_C_MHS, NO_UJIAN, NM_C_MHS, ID_KOTA_LAHIR, TGL_LAHIR, ALAMAT, TELP, EMAIL, JENIS_KELAMIN, KEWARGANEGARAAN, KEWARGANEGARAAN_LAIN,
            ID_AGAMA, STATUS_PERKAWINAN,
            NM_PROGRAM_STUDI, NM_PRODI_MINAT, NM_FAKULTAS, PS.ID_JENJANG, NM_JENJANG, SUMBER_BIAYA,
            STATUS
        FROM CALON_MAHASISWA CM
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI
        LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN CALON_MAHASISWA_PASCA CMP ON CMP.ID_C_MHS = CM.ID_C_MHS
        LEFT JOIN PRODI_MINAT PM ON PM.ID_PRODI_MINAT = CMP.ID_PRODI_MINAT
        WHERE CM.ID_C_MHS = {$id_c_mhs}");
    $cm = $db->FetchAssoc();
    $cm['TGL_LAHIR'] = date_to_timestamp($cm['TGL_LAHIR']);
    $smarty->assign('cm', $cm);
    
    // Tabel Calon Mahasiswa Pasca
    $db->Query("SELECT * FROM CALON_MAHASISWA_PASCA WHERE ID_C_MHS = {$id_c_mhs}");
    $cmp = $db->FetchAssoc();
    $smarty->assign('cmp', $cmp);
    
    // mendapatkan provinsi dan kota
    $provinsi_set = $db->QueryToArray("SELECT ID_PROVINSI, NM_PROVINSI FROM PROVINSI ORDER BY ID_PROVINSI");
    for ($i = 0; $i < count($provinsi_set); $i++)
        $provinsi_set[$i]['kota_set'] = $db->QueryToArray("SELECT ID_KOTA, NM_KOTA FROM KOTA WHERE ID_PROVINSI = {$provinsi_set[$i]['ID_PROVINSI']} ORDER BY NM_KOTA");
    $smarty->assign('provinsi_set', $provinsi_set);
    // -------------------------------
    
    $db->Close();
}

$smarty->display('step2_pasca.tpl');
?>
