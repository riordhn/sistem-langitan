{literal}<style type="text/css">
.center         { text-align: center; }
.formulir       { width: 100% }
.formulir tr td { font-size: 13px; }
.formulir tr td h2 { margin: 0px; }
.formulir tr td h3 { margin: 0px; }
.formulir tr td h4 { margin: 0px; }
label.error        { display: none; color: #f00; font-size: 12px; }
</style>{/literal}

<div class="center_title_bar" style="margin-left: -20px">Formulir Registrasi Calon Mahasiswa - Program Pascasarjana</div>

{$jam_aktif=mktime(14, 0, 0, 9, 6, 2011)}

{if time() < $jam_aktif}

    <h2 style="text-align:center">Pengisian formulir pendaftaran program pascasarjana bisa dilakukan tanggal :<br/>06 September 2011, Jam 14:00</h2>
    <h3 style="text-align:center">-- Universitas Airlangga Cyber Campus --</h3>

{else}


<form action="verifikasi-pasca.php" method="post" id="formulir">
    <input type="hidden" name="mode" value="save_biodata_pasca" />
    
    <table style="width: 100%; margin-left: 0px" class="formulir">
        <col style="width: 30px" />
        <col style="width: 250px" />
        <col style="" />
        <tr>
            <td colspan="3"><h2>Program Studi</h2></td>
        </tr>
        <tr>
            <td></td>
            <td>Fakultas</td>
            <td>{$cm.NM_FAKULTAS}</td>
        </tr>
        <tr>
            <td></td>
            <td>Program Studi</td>
            <td><strong>{$cm.NM_JENJANG} {$cm.NM_PROGRAM_STUDI}</strong>{if $cm.NM_PRODI_MINAT != ''} - {$cm.NM_PRODI_MINAT}{/if}</td>
        </tr>
        <tr>
            <td colspan="3"><h2>Biodata Mahasiswa Baru</h2></td>
        </tr>
        <tr>
            <td></td>
            <td>No Ujian</td>
            <td>{$cm.NO_UJIAN}</td>
        </tr>
        <tr>
            <td></td>
            <td>Nama Lengkap</td>
            <td>{$cm.NM_C_MHS}</td>
        </tr>
        <tr>
            <td></td>
            <td>Tempat dan Tanggal Lahir</td>
            <td>
                <select name="id_kota_lahir" class="required">
                    <option value=""></option>
                    {foreach $provinsi_set as $p}
                    <optgroup label="{$p.NM_PROVINSI}">
                        {foreach $p.kota_set as $k}
                        <option value="{$k.ID_KOTA}" {if $cm.ID_KOTA_LAHIR==$k.ID_KOTA}selected="selected"{/if}>{$k.NM_KOTA}</option>
                        {/foreach}
                    </optgroup>
                    {/foreach}
                </select>,
                {html_select_date prefix="tgl_lahir_" field_order="DMY" start_year="-80" end_year="-14" time=$cm.TGL_LAHIR}
                <br/>
                <label for="id_kota_lahir" class="error" style="display: none;">Pilih kota kelahiran</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td>
                <textarea name="alamat" maxlength="250" cols="50" rows="3" class="required">{$cm.ALAMAT}</textarea>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>No Telp</td>
            <td><input type="text" name="telp" class="required" maxlength="32" value="{$cm.TELP}"/></td>
        </tr>
        <tr>
            <td></td>
            <td>Email</td>            
            <td><input type="text" name="email" class="email required" maxlength="50" size="50" value="{$cm.EMAIL}" /></td>
        </tr>
        <tr>
            <td></td>
            <td>Jenis Kelamin</td>
            <td>
                <label><input type="radio" name="jenis_kelamin" value="1" class="required" {if $cm.JENIS_KELAMIN==1}checked="checked"{/if} >Laki-Laki</label>
                <label><input type="radio" name="jenis_kelamin" value="2" {if $cm.JENIS_KELAMIN==2}checked="checked"{/if}>Perempuan</label>
                <br/>
                <label for="jenis_kelamin" class="error" style="display: none;">Pilih salah satu</label>
        </tr>
        <tr>
            <td></td>
            <td>Kewarganegaraan</td>
            <td>
                <label><input type="radio" name="kewarganegaraan" value="1" class="required" {if $cm.KEWARGANEGARAAN==1}checked="checked"{/if} />Indonesia</label>
                <label><input type="radio" name="kewarganegaraan" value="2" {if $cm.KEWARGANEGARAAN==2}checked="checked"{/if}/>W.N.A. <input type="text" name="kewarganegaraan_lain" maxlength="20" value="{$cm.KEWARGANEGARAAN_LAIN}" /><label for="kewarganegaraan_lain" class="error" style="display: none;">Harus di isi</label></label>
                <br/>
                <label for="kewarganegaraan" class="error" style="display: none;">Pilih salah satu</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>Agama</td>
            <td>
                <label><input type="radio" name="id_agama" value="1" class="required" {if $cm.ID_AGAMA==1}checked="checked"{/if} />Islam</label>
                <label><input type="radio" name="id_agama" value="2" {if $cm.ID_AGAMA==2}checked="checked"{/if}/>Kristen Protestan</label>
                <label><input type="radio" name="id_agama" value="3" {if $cm.ID_AGAMA==3}checked="checked"{/if}/>Kristen Katholik</label>
                <label><input type="radio" name="id_agama" value="4" {if $cm.ID_AGAMA==4}checked="checked"{/if}/>Hindu</label>
                <label><input type="radio" name="id_agama" value="5" {if $cm.ID_AGAMA==5}checked="checked"{/if}/>Budha</label>
                <label><input type="radio" name="id_agama" value="6" {if $cm.ID_AGAMA==6}checked="checked"{/if}/>Lain-lain</label>
                <br/>
                <label for="id_agama" class="error" style="display: none;">Pilih salah satu</label></td>
        </tr>
        <tr>
            <td></td>
            <td>Status Perkawinan</td>
            <td>
                <label><input type="radio" name="status_perkawinan" value="1" class="required" {if $cm.STATUS_PERKAWINAN==1}checked="checked"{/if}/>Belum Kawin</label>
                <label><input type="radio" name="status_perkawinan" value="2" {if $cm.STATUS_PERKAWINAN==2}checked="checked"{/if}/>Kawin</label>
                <label><input type="radio" name="status_perkawinan" value="3" {if $cm.STATUS_PERKAWINAN==3}checked="checked"{/if}/>Bercerai</label>
                <label><input type="radio" name="status_perkawinan" value="4" {if $cm.STATUS_PERKAWINAN==4}checked="checked"{/if}/>Kawin, Suami / Istri Meninggal</label>
                <br/>
                <label for="status_perkawinan" class="error" style="display: none;">Pilih salah satu</label></td>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>Pekerjaan / Jabatan</td>
            <td>
                <input type="text" name="pekerjaan" maxlength="30" size="30" value="{$cmp.PEKERJAAN}" class="required"/>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>Instansi Asal</td>
            <td><input type="" name="asal_instansi" maxlength="50" size="50" value="{$cmp.ASAL_INSTANSI}"/></td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat Instansi</td>
            <td>
                <textarea name="alamat_instansi" maxlength="250" cols="50" >{$cmp.ALAMAT_INSTANSI}</textarea>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>Telp / Faks Instansi</td>
            <td><input type="" name="telp_instansi" maxlength="30" size="30" value="{$cmp.TELP_INSTANSI}" /></td>
        </tr>
        <tr>
            <td></td>
            <td>NIP / NRP / NIS</td>
            <td>
                <input type="text" name="nrp" maxlength="20" size=20" value="{$cmp.NRP}" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td>KARPEG</td>
            <td><input type="text" name="karpeg" maxlength="30" size="30" value="{$cmp.KARPEG}" /></td>
        </tr>
        <tr>
            <td></td>
            <td>Pangkat & Golongan</td>
            <td><input type="text" name="pangkat" maxlength="20" size="20" value="{$cmp.PANGKAT}" /></td>
        </tr>
        <tr>
            <td colspan="3"><h2>Pendidikan S1</h2></td>
        </tr>
        <tr>
            <td></td>
            <td>Perguruan Tinggi</td>
            <td>
                <input type="text" name="ptn_s1" maxlength="30" size="30" value="{$cmp.PTN_S1}" class="required"/>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>Status Perguruan Tinggi</td>
            <td>
                <label><input type="radio" name="status_ptn_s1" value="1" {if $cmp.STATUS_PTN_S1==1}checked="checked"{/if} class="required"/>Negeri</label>
                <label><input type="radio" name="status_ptn_s1" value="2" {if $cmp.STATUS_PTN_S1==2}checked="checked"{/if}/>Swasta</label>
                <label><input type="radio" name="status_ptn_s1" value="3" {if $cmp.STATUS_PTN_S1==3}checked="checked"{/if}/>Luar Negeri</label>
                <br/>
                <label for="status_ptn_s1" class="error" style="display:none;">Pilih salah satu</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>Program Studi</td>
            <td><input type="text" name="prodi_s1" maxlength="30" size="30" value="{$cmp.PRODI_S1}" class="required"/></td>
        </tr>
        <tr>
            <td></td>
            <td>Tanggal Masuk</td>
            <td>{html_select_date prefix="tgl_masuk_s1_" field_order="DMY" start_year=-30 time=$cmp.TGL_MASUK_S1}</td>
        </tr>
        <tr>
            <td></td>
            <td>Tanggal Lulus</td>
            <td>{html_select_date prefix="tgl_lulus_s1_" field_order="DMY" start_year=-30 time=$cmp.TGL_LULUS_S1}</td>
        </tr>
        <tr>
            <td></td>
            <td>Lama Studi</td>
            <td><input type="text" name="lama_studi_s1" maxlength="4" size="4" class="number required" value="{$cmp.LAMA_STUDI_S1}" /> tahun</td>
        </tr>
        <tr>
            <td></td>
            <td>Index Prestasi</td>
            <td><input type="text" name="ip_s1" maxlength="5" size="5" class="number required" value="{$cmp.IP_S1}" /></td>
        </tr>
        {if $cm.ID_JENJANG == 3}
        <tr>
            <td colspan="3"><h2>Pendidikan S2</h2></td>
        </tr>
        <tr>
            <td></td>
            <td>Perguruan Tinggi</td>
            <td>
                <input type="text" name="ptn_s2" maxlength="30" size="30" value="{$cmp.PTN_S2}" class="required"/>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>Status Perguruan Tinggi</td>
            <td>
                <label><input type="radio" name="status_ptn_s2" value="1" {if $cmp.STATUS_PTN_S2==1}checked="checked"{/if} class="required"/>Negeri</label>
                <label><input type="radio" name="status_ptn_s2" value="2" {if $cmp.STATUS_PTN_S2==2}checked="checked"{/if}/>Swasta</label>
                <label><input type="radio" name="status_ptn_s2" value="3" {if $cmp.STATUS_PTN_S2==3}checked="checked"{/if}/>Luar Negeri</label>
                <br/>
                <label for="status_ptn_s2" class="error" style="display:none;">Pilih salah satu</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>Program Studi</td>
            <td><input type="text" name="prodi_s2" maxlength="30" size="30" value="{$cmp.PRODI_S2}" class="required"/></td>
        </tr>
        <tr>
            <td></td>
            <td>Tanggal Masuk</td>
            <td>{html_select_date prefix="tgl_masuk_s2_" field_order="DMY" start_year=-30 time=$cmp.TGL_MASUK_S2}</td>
        </tr>
        <tr>
            <td></td>
            <td>Tanggal Lulus</td>
            <td>{html_select_date prefix="tgl_lulus_s2_" field_order="DMY" start_year=-30 time=$cmp.TGL_LULUS_S2}</td>
        </tr>
        <tr>
            <td></td>
            <td>Lama Studi</td>
            <td><input type="text" name="lama_studi_s2" maxlength="4" size="4" class="number required" value="{$cmp.LAMA_STUDI_S2}" /> tahun</td>
        </tr>
        <tr>
            <td></td>
            <td>Index Prestasi</td>
            <td><input type="text" name="ip_s2" maxlength="5" size="5" class="number required" value="{$cmp.IP_S2}" /></td>
        </tr>
        <tr>
            <td></td>
            <td>Jumlah Karya Ilmiah</td>
            <td><input type="text" name="jumlah_karya_ilmiah" maxlength="2" size="2" class="number required" value="{$cmp.JUMLAH_KARYA_ILMIAH}" /></td>
        </tr>
        {/if}
        <tr>
            <td colspan="3"><h2> Lain-Lain</h2></td>
        </tr>
        <tr>
            <td></td>
            <td>Lamaran ke PPS Unair</td>
            <td>
                <label><input type="radio" name="status_lamaran_pps" value="1" {if $cmp.STATUS_LAMARAN_PPS==1}checked="checked"{/if}/>Pernah, Tahun <input type="text" name="tahun_lamaran_pps" maxlength="4" size="4" value="{$cmp.TAHUN_LAMARAN_PPS}" /></label>
                <label><input type="radio" name="status_lamaran_pps" value="0" {if $cmp.STATUS_LAMARAN_PPS==0}checked="checked"{/if}/>Tidak pernah</label>
                {literal}<script type="text/javascript">
                    $('input:radio[name="status_lamaran_pps"]').click(function() {
                        if (this.value == 1) {
                            $('input[name="tahun_lamaran_pps"]').focus();
                        }
                    });
                </script>{/literal}
            </td>
        </tr>
        <tr>
            <td></td>
            <td>Sumber Dana</td>
            <td>
                <label><input type="radio" name="sumber_biaya" value="5" {if $cm.SUMBER_BIAYA==5}checked="checked"{/if}/>Sendiri</label>
                <label><input type="radio" name="sumber_biaya" value="6" {if $cm.SUMBER_BIAYA==6}checked="checked"{/if}/>Instansi</label>
                <label><input type="radio" name="sumber_biaya" value="7" {if $cm.SUMBER_BIAYA==7}checked="checked"{/if}/>BPPS</label>
            </td>
        </tr>
    </table>
    <div style="text-align: center">
        <!--<input type="submit" value="Simpan" />-->
		<!-- {$cm.STATUS} -->
        {if $cm.STATUS >= 6 || $cm.STATUS == 1}
        
            {if $cm.ID_JENJANG == 2}
            <a href="print-pasca-s2.php" class="disable-ajax" target="_blank">CETAK</a>
            {/if}

            {if $cm.ID_JENJANG == 3}
            <a href="print-pasca-s3.php" class="disable-ajax" target="_blank">CETAK</a>    
            {/if}
            
            {if $cm.ID_JENJANG == 9}
            <a href="print-pasca-pr.php" class="disable-ajax" target="_blank">CETAK</a>        
            {/if}

            {if $cm.ID_JENJANG == 10}
            <a href="print-pasca-sp.php" class="disable-ajax" target="_blank">CETAK</a>        
            {/if}
        
        {/if}
        
        <div style="text-align: left">
            Harap tidak melakukan pencetakan melalui pengambilan <i>screenshot</i>, karena setelah di submit nanti akan muncul tombol <u>cetak</u>.
        </div>
    </div>                
</form>
                
{literal}
<script type="text/javascript">
    $('#formulir').validate();
</script>
{/literal}

{/if}