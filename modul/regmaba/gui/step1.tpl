{literal}
<style type="text/css">
.center         { text-align: center; }
.formulir       { width: 100% }
.formulir tr td { font-size: 13px; }
.formulir tr td h2 { margin: 0px; }
.formulir tr td h3 { margin: 0px; }
.formulir tr td h4 { margin: 0px; }
label.error        { display: none; color: #f00; font-size: 12px; }
</style>
{/literal}

{if $calon_mahasiswa->STATUS == 3}
<div class="center_title_bar">Formulir Verifikasi Administrasi - Step 1 of 3</div>

<form action="verifikasi.php" method="post">
<input type="hidden" name="mode" value="save" />
<table style="margin-left: 20px" class="formulir">
    <col style="width: 30px" />
    <col style="width: 250px" />
    <col style="" />
    <col style="" />
    <col style="" />
    <col style="" />
    <tr>
        <td colspan="6" class="center"><h3>IDENTITAS DIRI</h3></td>
    </tr>
    <tr>
        <td>1</td>
        <td>Nama CAMABA</td>
        <td><label>{$calon_mahasiswa->NM_C_MHS}</label></td>
    </tr>
    <tr>
        <td>2</td>
        <td>Nomor Ujian Tulis SNMPTN</td>
        <td><label>{$calon_mahasiswa->NO_UJIAN}</label></td>
    </tr>
    <tr>
        <td>3</td>
        <td>Program Studi (Diterima)</td>
        <td><label>{$calon_mahasiswa->PROGRAM_STUDI->JENJANG->NM_JENJANG} - {$calon_mahasiswa->PROGRAM_STUDI->NM_PROGRAM_STUDI}</label></td>
    </tr>
    <tr>
        <td>4</td>
        <td>Nama Ayah</td>
        <td><input type="text" name="nama_ayah" maxlength="64" class="required"/></td>
    </tr>
    <tr>
        <td>5</td>
        <td>Alamat Ayah</td>
        <td><textarea name="alamat_ayah" cols="50" rows="2" maxlength="200" class="required"></textarea><br/>
            <select name="id_kota_ayah" class="required">
                <option value=""></option>
            {for $i=0 to $provinsi_set->Count()-1}{$provinsi=$provinsi_set->Get($i)}
                <optgroup label="{$provinsi->NM_PROVINSI}">
                {for $j=0 to $provinsi->KOTAs->Count()-1}{$kota=$provinsi->KOTAs->Get($j)}
                    <option value="{$kota->ID_KOTA}">{$kota->NM_KOTA}</option>
                {/for}
                </optgroup>
            {/for}
            </select>
            Posisi Alamat : <select name="status_alamat_ayah" class="required">
                <option value=""></option>
                <option value="1">Kota</option>
                <option value="2">Desa</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>Nama Ibu </td>
        <td><input type="text" name="nama_ibu" maxlength="64" class="required"/></td>
    </tr>
    <tr>
        <td>7</td>
        <td>Alamat Ibu</td>
        <td><textarea name="alamat_ibu" cols="50" rows="2" maxlength="200" class="required"></textarea><br/>
            <select name="id_kota_ibu" class="required">
                <option value=""></option>
            {for $i=0 to $provinsi_set->Count()-1}{$provinsi=$provinsi_set->Get($i)}
                <optgroup label="{$provinsi->NM_PROVINSI}">
                {for $j=0 to $provinsi->KOTAs->Count()-1}{$kota=$provinsi->KOTAs->Get($j)}
                    <option value="{$kota->ID_KOTA}">{$kota->NM_KOTA}</option>
                {/for}
                </optgroup>
            {/for}
            </select>
            Posisi Alamat : <select name="status_alamat_ibu" class="required">
                <option value=""></option>
                <option value="1">Kota</option>
                <option value="2">Desa</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>Jumlah Anggota Keluarga</td>
        <td><input type="text" name="anggota_keluarga" class="required number" maxlength="2" size="2" /></td>
    </tr>
    <tr>
        <td>9</td>
        <td>Nomor KSK</td>
        <td><input type="text" name="nomor_ksk" class="required" size="50" maxlength="50" /></td>
    </tr>
    <tr>
        <td>10</td>
        <td>Sanggup Membayar SP3</td>
        <td>
            <label><input type="radio" name="sp3" value="5" class="required"/>SP3b</label>
            <label><input type="radio" name="sp3" value="4"/>SP3a</label>
            <label><input type="radio" name="sp3" value="3"/>BEBAS SP3</label>
            <label><input type="radio" name="sp3" value="2"/>GAKIN</label>
            <br/>
            <label for="sp3_lain" style="display: none;">SP3 : <input type="text" name="sp3_lain" id="sp3_lain" value="{$sp3_lain}" minimal="{$sp3_lain}" /><span style="font-size: 95%">Bisa diisi dengan nilai yang lebih besar</span></label>
            <label class="error" for="sp3">Pilih Salah satu</label>
            <script type="text/javascript">
            {literal}
                $('input[name="sp3"]:radio').bind('click', function() {
                    if ($(this).attr('value') == 5) {
                        $('label[for="sp3_lain"]').css('display', 'block');
                        $('input[name="sp3_lain"]').addClass('required');
                    }
                    else {
                        $('label[for="sp3_lain"]').css('display', 'none');
                        $('input[name="sp3_lain"]').removeClass('required');
                    }
                });
            {/literal}
            </script>
        </td>
    </tr>
    <tr>
        <td>11</td>
        <td>Penghasilan Orang Tua/Bulan</td>
        <td>
            <label><input type="radio" name="penghasilan_ortu" value="1" class="required"/>&gt; 7,5 juta</label>
            <label><input type="radio" name="penghasilan_ortu" value="2"/>2,5 - 7,5 juta</label>
            <label><input type="radio" name="penghasilan_ortu" value="3"/>1,35 - 2,5 juta</label>
            <label><input type="radio" name="penghasilan_ortu" value="4"/>&lt; 1,35 juta</label>
            <br/>
            <label class="error" for="penghasilan_ortu">Pilih Salah satu</label>
        </td>
    </tr>
    <tr>
        <td colspan="6" class="center"><h3>PEKERJAAN / PROFESI / JABATAN ORANG TUA</h3></td>
    </tr>
    <tr>
        <td>12</td>
        <td>Pekerjaan Ayah</td>
        <td>
            <label><input type="radio" name="pekerjaan_ayah" value="1" class="required" />Guru/Dosen Negeri</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="2" />PNS Bukan Guru/Dosen</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="3" />TNI / POLRI</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="4" />Guru / Dosen Swasta</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="5" />Karyawan Swasta</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="6" />Pedagang / Wiraswasta</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="7" />Petani / Nelayan</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="8" />Buruh</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="9" />Pensiunan PNS / TNI / POLRI</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="10" />Pensiunan K.Swasta</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="11" />Lain-lain : <input type="text" name="pekerjaan_ayah_lain" maxlength="60" /></label>
            <br/>
            <label for="pekerjaan_ayah" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>13</td>
        <td>Pekerjaan Ibu</td>
        <td>
            <label><input type="radio" name="pekerjaan_ibu" value="1" class="required"/>Guru/Dosen Negeri</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="2" />PNS Bukan Guru/Dosen</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="3" />TNI / POLRI</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="4" />Guru / Dosen Swasta</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="5" />Karyawan Swasta</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="6" />Pedagang / Wiraswasta</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="7" />Petani / Nelayan</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="8" />Buruh</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="9" />Pensiunan PNS / TNI / POLRI</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="10" />Pensiunan K.Swasta</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="11" />Lain-lain : <input type="text" name="pekerjaan_ibu_lain" maxlength="60" /></label>
            <br/>
            <label for="pekerjaan_ibu" class="error" style="display: none;">Pilih salah satu</label>
        </td>
        {literal}<script language="javascript">
            $('input:radio').click(function() {
                if ($(this).attr('name') == 'pekerjaan_ayah') {
                    if ($(this).attr('value') == 11) {
                        $('input[name="pekerjaan_ayah_lain"]').addClass("required");
                        $('input[name="pekerjaan_ayah_lain"]').focus();
                    }
                    else {
                        $('input[name="pekerjaan_ayah_lain"]').removeClass("required");
                    }
                }

                if ($(this).attr('name') == 'pekerjaan_ibu') {
                    if ($(this).attr('value') == 11) {
                        $('input[name="pekerjaan_ibu_lain"]').addClass("required");
                        $('input[name="pekerjaan_ibu_lain"]').focus();
                    }
                    else {
                        $('input[name="pekerjaan_ibu_lain"]').removeClass("required");
                    }
                }
            });
        </script>{/literal}
    </tr>
    
    <tr>
        <td>14</td>
        <td>Skala Usaha/Pekerjaan</td>
        <td>
            <label><input type="radio" name="skala_pekerjaan_ortu" value="1" class="required"/>Besar</label>
            <label><input type="radio" name="skala_pekerjaan_ortu" value="2"/>Menengah</label>
            <label><input type="radio" name="skala_pekerjaan_ortu" value="3"/>Kecil</label>
            <label><input type="radio" name="skala_pekerjaan_ortu" value="4"/>Mikro</label><br/>
            <label class="error" for="skala_pekerjaan_ortu">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td colspan="6" class="center"><h3>KEDIAMAN DAN KENDARAAN KELUARGA</h3></td>
    </tr>
    <tr>
        <td>15</td>
        <td>Kediaman Orang Tua</td>
        <td>
            <label><input type="radio" name="kediaman_ortu" value="1" class="required"/>Mewah/Besar</label>
            <label><input type="radio" name="kediaman_ortu" value="2"/>Sedang</label>
            <label><input type="radio" name="kediaman_ortu" value="3"/>Rumah Sederhana</label>
            <label><input type="radio" name="kediaman_ortu" value="4"/>Rumah Sangat Sederhana</label><br/>
            <label class="error" for="kediaman_ortu">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>16</td>
        <td>Luas Tanah Kediaman</td>
        <td>
            <label><input type="radio" name="luas_tanah" value="1" class="required"/>&gt; 200 m2</label>
            <label><input type="radio" name="luas_tanah" value="2"/>100 - 200 m2</label>
            <label><input type="radio" name="luas_tanah" value="3"/>45 - 100 m2</label>
            <label><input type="radio" name="luas_tanah" value="4"/>&lt; 45 m2</label><br/>
            <label class="error" for="luas_tanah">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>17</td>
        <td>Luas Bangunan Kediaman</td>
        <td>
            <label><input type="radio" name="luas_bangunan" value="1" class="required"/>&gt; 100 m2</label>
            <label><input type="radio" name="luas_bangunan" value="2"/>56 - 100 m2</label>
            <label><input type="radio" name="luas_bangunan" value="3"/>27 - 56 m2</label>
            <label><input type="radio" name="luas_bangunan" value="4"/>&lt; 27 m2</label><br/>
            <label class="error" for="luas_bangunan">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>18</td>
        <td>NJOP</td>
        <td>
            <label><input type="radio" name="njop" value="1" class="required"/>&gt; 300 juta</label>
            <label><input type="radio" name="njop" value="2"/>100 - 300 juta</label>
            <label><input type="radio" name="njop" value="3"/>50 - 100 juta</label>
            <label><input type="radio" name="njop" value="4"/>&lt; 50 juta</label><br/>
            <label class="error" for="njop">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>19</td>
        <td>Daya Listrik</td>
        <td>
            <label><input type="radio" name="listrik" value="4" class="required"/>2200 atau lebih</label>
            <label><input type="radio" name="listrik" value="3"/>1300</label>
            <label><input type="radio" name="listrik" value="2"/>900</label>
            <label><input type="radio" name="listrik" value="1"/>450 atau kurang</label><br/>
            <label class="error" for="listrik">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>20</td>
        <td>Kendaraan Bermotor R4</td>
        <td>
            <label><input type="radio" name="kendaraan_r4" value="1" class="required"/>&gt; 1</label>
            <label><input type="radio" name="kendaraan_r4" value="2"/>1</label>
            <label><input type="radio" name="kendaraan_r4" value="3"/>Tidak Punya</label><br/>
            <label class="error" for="kendaraan_r4">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>21</td>
        <td>Kendaraan bermotor R2</td>
        <td>
            <label><input type="radio" name="kendaraan_r2" value="1" class="required"/>&gt; 2</label>
            <label><input type="radio" name="kendaraan_r2" value="2"/>2</label>
            <label><input type="radio" name="kendaraan_r2" value="3"/>1</label>
            <label><input type="radio" name="kendaraan_r2" value="4"/>Tidak Punya</label><br/>
            <label class="error" for="kendaraan_r2">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>22</td>
        <td>Kepemilikan Kekayaan Lainya :</td>
        <td>
            <textarea name="kekayaan_lain" cols="40" rows="5" maxlength="256"></textarea>
        </td>
    </tr>
    <tr>
        <td>23</td>
        <td>Informasi lainnya, sebutkan :</td>
        <td>
            <textarea name="info_lain" cols="40" rows="5" maxlength="256"></textarea>
        </td>
    </tr>
    <tr>
        <td colspan="6" class="center"><h3>PERNYATAAN</h3></td>
    </tr>
    <tr>
        <td colspan="6" class="center">
            <font style="color: #f00; font-weight: bold; font-size: 120%">
            Data dan informasi ini disampaikan dengan sesungguhnya dan sebenar-benarnya. Apabila ternyata data dan informasi ini tidak benar dan tidak lengkap,
            saya bersedia kehilangan dan atau melepaskan hak saya sebagai mahasiswa Universitas Airlangga selama 2 tahun pada semua program studi melalu program
            seleksi apapun.
            </font>
        </td>
    </tr>
    <tr>
        <td colspan="6" class="center">
            <input type="submit" value="Submit Verifikasi" />
        </td>
    </tr>
</table>
<div>NB: Ketika data sudah disubmit, maka tidak bisa diedit lagi.</div>
</form>
{/if}
{literal}
<script type="text/javascript">
$('form').validate({
    rules: { sp3_lain: { min : $('input[name="sp3_lain"]').attr('minimal'), required: true, number: true } }
});
</script>
{/literal}