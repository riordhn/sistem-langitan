function formatAngka(num) {
    num = num.toString().replace(/\\$|\\,/g,'');
    if (isNaN(num)) num = '0';
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num*100+0.50000000001);
    cents = num%100;
    num = Math.floor(num/100).toString();
    if (cents < 10) cents = '0' + cents;

    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
        num = num.substring(0,num.length-(4*i+3))+'.'+ num.substring(num.length-(4*i+3));

    return (((sign)?'':'-') + '' + num);
}

$(document).ready(function() {
    
    $('input[name^="sp3_"]').each(function(index, item) {
        // Cek exist
        if ($(item).length > 0)
        {
            $(item).keyup(function() {
                $(item).formatCurrency('#sp3_info', {symbol: 'Rp ', digitGroupSymbol: '.', roundToDecimalPlace: 0});
            });
        }
    });
    
    $('form').validate({
        rules: {
           sp3_1: {required: true, number: true, min: $('input[name="sp3_min"]').val()},
           sp3_2: {required: true, number: true, min: $('input[name="sp3_min"]').val()},
           sp3_3: {required: true, number: true, min: $('input[name="sp3_min"]').val()},
           sp3_4: {required: true, number: true, min: $('input[name="sp3_min"]').val()}
        },
        messages: {
           sp3_1: {required: 'Masukkan kesanggupan sp3', number: 'Masukan format angka', min: 'Kesanggupan membayar sp3 tidak boleh kurang dari nilai minimal '},
           sp3_2: {required: 'Masukkan kesanggupan sp3', number: 'Masukan format angka', min: 'Kesanggupan membayar sp3 tidak boleh kurang dari nilai minimal '},
           sp3_3: {required: 'Masukkan kesanggupan sp3', number: 'Masukan format angka', min: 'Kesanggupan membayar sp3 tidak boleh kurang dari nilai minimal '},
           sp3_4: {required: 'Masukkan kesanggupan sp3', number: 'Masukan format angka', min: 'Kesanggupan membayar sp3 tidak boleh kurang dari nilai minimal '}
        },
        errorPlacement: function(error, element) {
            error.appendTo(element.parent());
        }
    });
    
    $('#table-formulir').fadeIn('slow');
});

