{if $smarty.server.REMOTE_ADDR == '210.57.215.234'}
<div style="text-align: center"><h2>Regmaba untuk program penerimaan Pascasarjana (S2, S3, Profesi) bisa di akses kembali pada jam 17.00 WIB</h2><br/>
<a href="index.php?mode=logout">Logout</a>
</div>
{else}
    
{include file="header.tpl"}

<form action="form.php?mode=page-result" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
<input type="hidden" name="mode" value="sp" />
<table id="table-formulir" style="display: none">
    <caption>Formulir Pendaftaran Spesialis Universitas Airlangga</caption>
    <tr>
        <th colspan="2">Program Studi Diterima</th>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td><strong>{$cmb.NM_FAKULTAS}</strong></td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td><strong>{$cmb.NM_PROGRAM_STUDI}</strong></td>
    </tr>
    <tr>
        <th colspan="2">Data Diri</th>
    </tr>
    <tr>
        <td>No Ujian</td>
        <td>{$cmb.NO_UJIAN}</td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>
            {$cmb.NM_C_MHS}
            <input type="hidden" name="nm_c_mhs" maxlength="64" size="64" value="{$cmb.NM_C_MHS}"/>
        </td>
    </tr>
    <tr>
        <td>Gelar</td>
        <td><input type="text" name="gelar" maxlength="20" size="6" value="{$cmb.GELAR}" /></td>
    </tr>
    <tr>
        <td>Tempat Lahir</td>
        <td>
            <input type="hidden" name="id_kota_lahir" id="id-kota-lahir" value="{$cmb.ID_KOTA_LAHIR}" />
            <input type="text" name="nm_kota_lahir" id="nama-kota-lahir" size="50" value="{$cmb.NM_KOTA_LAHIR}" readonly="readonly"/>
            <button id="button-kota-lahir">Edit</button>
        </td>
    </tr>
    <tr>
        <td>Tanggal Lahir</td>
        <td>{html_select_date prefix="tgl_lahir_" time=$cmb.TGL_LAHIR day_empty="" month_empty="" year_empty="" field_order="DMY" start_year=-80 end_year=-10}</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>
            <input type="text" name="alamat" size="65" maxlength="250" value="{$cmb.ALAMAT}" />
        </td>
    </tr>
    <tr>
        <td>Kota</td>
        <td>
            <input type="hidden" name="id_kota" id="id-kota" value="{$cmb.ID_KOTA}" />
            <input type="text" name="nm_kota" id="nama-kota" size="50" value="{$cmb.NM_KOTA}" readonly="readonly"/>
            <button id="button-kota">Edit</button>
        </td>
    </tr>
    <tr>
        <td>No Telp</td>
        <td><input type="text" name="telp"  maxlength="32" value="{$cmb.TELP}"/></td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>
            <select name="jenis_kelamin">
            {foreach $jenis_kelamin_set as $jk}
                <option value="{$jk.JENIS_KELAMIN}" {if $cmb.JENIS_KELAMIN == $jk.JENIS_KELAMIN}selected="selected"{/if}>{$jk.NM_JENIS_KELAMIN}</option>
            {/foreach}
            </select>
		</td>
    </tr>
    <tr>
        <td>Kewarganegaraan</td>
        <td>
			<select name="kewarganegaraan" id="id_kewarganegaraan">
			{foreach $kewarganegaraan_set as $kw}
				<option value="{$kw.ID_KEWARGANEGARAAN}" {if $cmb.KEWARGANEGARAAN == $kw.ID_KEWARGANEGARAAN}selected="selected"{/if}>{$kw.NM_KEWARGANEGARAAN}</option>
			{/foreach}
			</select>
        </td>
    </tr>
    <tr id="wna-hide">
    	<td>Asal Negara</td>
        <td>
            <select id="id_kebangsaan" name="id_kebangsaan">
            {if $cmb.ID_KEBANGSAAN == 114}
            {foreach $negara_indo_set as $n}
                <option value="{$n.ID_NEGARA}" {if $n.ID_NEGARA == $cmb.ID_KEBANGSAAN}selected="selected"{/if}>{$n.NM_NEGARA}</option>
            {/foreach} 
            {else if $cmb.KEWARGANEGARAAN == 1}
             {foreach $negara_indo_set as $n}
                <option value="{$n.ID_NEGARA}" {if $n.ID_NEGARA == $cmb.ID_KEBANGSAAN}selected="selected"{/if}>{$n.NM_NEGARA}</option>
            {/foreach} 
            {else}
                <option value=""></option>
            {foreach $negara_asing_set as $n}
                <option value="{$n.ID_NEGARA}" {if $n.ID_NEGARA == $cmb.ID_KEBANGSAAN}selected="selected"{/if}>{$n.NM_NEGARA}</option>
            {/foreach}
            {/if}
        </td>
    </tr>
    <tr>
        <td>Agama</td>
        <td>
			<select name="id_agama">
			{foreach $agama_set as $a}
				<option value="{$a.ID_AGAMA}" {if $cmb.ID_AGAMA == $a.ID_AGAMA}selected="selected"{/if}>{$a.NM_AGAMA}</option>
			{/foreach}
			</select>
		</td>
    </tr>
    <tr>
        <td>Sumber Biaya</td>
        <td>
			<select name="sumber_biaya">
			{foreach $sumber_biaya_set as $sb}
				<option value="{$sb.ID_SUMBER_BIAYA}" {if $cmb.SUMBER_BIAYA == $sb.ID_SUMBER_BIAYA}selected="selected"{/if}>{$sb.NM_SUMBER_BIAYA}</option>
			{/foreach}
			</select>
		</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>
            <input type="text" name="email" size="30" maxlength="64" value="{$cmb.EMAIL}" />
        </td>
    </tr>
    <tr>
        <th colspan="2">Data Pekerjaan</th>
    </tr>
    <tr>
        <td>Pekerjaan</td>
        <td><input type="text" name="pekerjaan" size="30" maxlength="30" value="{$cmb.PEKERJAAN}" /></td>
    </tr>
    <tr>
        <td>Asal Instansi</td>
        <td><input type="text" name="asal_instansi" size="30" maxlength="50" value="{$cmb.ASAL_INSTANSI}" /></td>
    </tr>
    <tr>
        <td>Alamat Instansi</td>
        <td><input type="text" name="alamat_instansi" size="65" maxlength="250" value="{$cmb.ALAMAT_INSTANSI}" /></td>
    </tr>
    <tr>
        <td>Telp Instansi</td>
        <td><input type="text" name="telp_instansi" size="30" maxlength="30" value="{$cmb.TELP_INSTANSI}" /></td>
    </tr>
    <tr>
        <td>NIP / NIS / NRP</td>
        <td><input type="text" name="nrp" size="20" maxlength="20" value="{$cmb.NRP}" /></td>
    </tr>
    <tr>
        <td>Karpeg</td>
        <td><input type="text" name="karpeg" size="30" maxlength="30" value="{$cmb.KARPEG}" /></td>
    </tr>
    <tr>
        <td>Pangkat</td>
        <td><input type="text" name="pangkat" size="20" maxlength="20" value="{$cmb.PANGKAT}" /></td>
    </tr>
    <tr>
        <th colspan="2">Data Pendidikan (S1)</th>
    </tr>
    <tr>
        <td>Perguruan Tinggi</td>
        <td><input type="text" name="ptn_s1" size="30" maxlength="30" value="{$cmb.PTN_S1}" /></td>
    </tr>
    <tr>
        <td>Status Perguruan Tinggi</td>
        <td>
            <select name="status_ptn_s1">
            {foreach $status_ptn_set as $sp}
                <option value="{$sp.STATUS_PTN}" {if $cmb.STATUS_PTN_S1 == $sp.STATUS_PTN}selected="selected"{/if}>{$sp.NM_STATUS_PTN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td><input type="text" name="prodi_s1" size="30" maxlength="30" value="{$cmb.PRODI_S1}" /></td>
    </tr>
    <tr>
        <td>Tanggal Masuk</td>
        <td>{html_select_date prefix="tgl_masuk_s1_" time=$cmb.TGL_MASUK_S1 day_empty="" month_empty="" year_empty="" field_order="DMY" start_year=-100}</td>
    </tr>
    <tr>
        <td>Tanggal Lulus</td>
        <td>{html_select_date prefix="tgl_lulus_s1_" time=$cmb.TGL_LULUS_S1 day_empty="" month_empty="" year_empty="" field_order="DMY" start_year=-100}</td>
    </tr>
    <tr>
        <td>Lama Studi</td>
        <td><input type="text" name="lama_studi_s1" size="4" maxlength="4" value="{$cmb.LAMA_STUDI_S1}" /></td>
    </tr>
    <tr>
        <td>Index Prestasi</td>
        <td><input type="text" name="ip_s1" size="4" maxlength="4" value="{$cmb.IP_S1}" /></td>
    </tr>
	<tr>
        <th colspan="2">Data Pendidikan (S2 / Profesi)</th>
    </tr>
    <tr>
        <td>Perguruan Tinggi</td>
        <td><input type="text" name="ptn_s2" size="30" maxlength="30" value="{$cmb.PTN_S2}" /></td>
    </tr>
    <tr>
        <td>Status Perguruan Tinggi</td>
        <td>
            <select name="status_ptn_s2">
				{foreach $status_ptn_set as $sp}
                <option value="{$sp.STATUS_PTN}" {if $cmb.STATUS_PTN_S2 == $sp.STATUS_PTN}selected="selected"{/if}>{$sp.NM_STATUS_PTN}</option>
				{/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td><input type="text" name="prodi_s2" size="30" maxlength="30" value="{$cmb.PRODI_S2}" /></td>
    </tr>
    <tr>
        <td>Tanggal Masuk</td>
        <td>{html_select_date prefix="tgl_masuk_s2_" time=$cmb.TGL_MASUK_S2 day_empty="" month_empty="" year_empty="" field_order="DMY" start_year=-75}</td>
    </tr>
    <tr>
        <td>Tanggal Lulus</td>
        <td>{html_select_date prefix="tgl_lulus_s2_" time=$cmb.TGL_LULUS_S2 day_empty="" month_empty="" year_empty="" field_order="DMY" start_year=-75}</td>
    </tr>
    <tr>
        <td>Lama Studi</td>
        <td><input type="text" name="lama_studi_s2" size="4" maxlength="4" value="{$cmb.LAMA_STUDI_S2}" /></td>
    </tr>
    <tr>
        <td>Index Prestasi</td>
        <td><input type="text" name="ip_s2" size="4" maxlength="4" value="{$cmb.IP_S2}" /></td>
    </tr>
    <tr>
        <td>Jumlah Karya Ilmiah</td>
        <td><input type="text" name="jumlah_karya_ilmiah" size="4" maxlength="4" value="{$cmb.JUMLAH_KARYA_ILMIAH}" /></td>
    </tr>
    <tr>
        <th colspan="2">Upload File Berkas</th>
    </tr>
    <tr>
        <td>File Ijazah / Surat Keterangan Lulus</td>
        <td>
            <input type="hidden" id="status_file_ijazah" {if $cmb.FILE_IJAZAH}value="1"{/if} />
            {if $cmb.FILE_IJAZAH}<a target="_blank" href="http://cybercampus.unair.ac.id/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_ijazah.pdf">{$cmb.ID_C_MHS}_ijazah.pdf</a>{/if}
            <input type="file" accept="application/pdf" name="file_ijazah"/>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center-align">
            <input type="hidden" name="id_pilihan_1" value="{$cmb.ID_PILIHAN_1}" />
            <input type="hidden" name="id_pilihan_2" value="{$cmb.ID_PILIHAN_2}" />
            <input type="hidden" name="id_pilihan_3" value="{$cmb.ID_PILIHAN_3}" />
            <input type="hidden" name="id_pilihan_4" value="{$cmb.ID_PILIHAN_4}" />
            <input type="hidden" name="id_kelompok_biaya" value="{$cmb.ID_KELOMPOK_BIAYA}" />
            <input type="hidden" name="id_prodi_minat" value="{$cmb.ID_PRODI_MINAT}" />
            <input type="submit" value="Lanjut" />
        </td>
    </tr>
</table>
</form>

<div id="dialog-kota"></div><div id="dialog-sekolah"></div>
<script type="text/javascript">$(document).ready(function() { $.getScript('gui/form/s2.js'); });</script>

{include file="footer.tpl"}

{/if}