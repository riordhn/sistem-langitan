{include file="header.tpl"}

<form action="form.php?mode=page-result" method="post">
    <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
    <input type="hidden" name="mode" value="page-2" />
    <table id="table-formulir" style="width: 60%">
        <caption>Registrasi Mahasiswa Baru Program Sarjana (S1) Universitas Airlangga</caption>
        <tr>
            <th colspan="2">KELOMPOK BIAYA</th>
        </tr>
		{if $id_biaya_kuliah==''}
		<tr>
            <td colspan="2">
				<div class="ui-widget">
					<div class="ui-state-highlight ui-corner-all" style="padding: 10px 5px 0px 10px;margin: 10px"> 
						<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
							<strong>Info :</strong><br/>Hasil verifikasi keuangan berupa invoice Uang Kuliah Tunggal dapat dicetak  paling lama 1 (satu) hari setelah Calon Mahasiswa mengirimkan (submit) online Form Biodata yang telah diisi secara lengkap.
								Setelah itu silahkan login ke halaman ini kembali untuk mencetak invoice pembayaran. Terima Kasih
							</p>
					</div>
				</div>
			</td>
        </tr>
		{else}
			<tr>
				<td colspan="2">
					<div class="ui-widget">
						<div class="ui-state-highlight ui-corner-all" style="padding: 10px 5px 0px 10px;margin: 10px"> 
							<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
								<strong>Info :</strong><br/>Selamat anda telah berhasil diverifikasi silahkan cetak invoice untuk melakukan pembayaran. Terima Kasih</p>
						</div>
					</div>
				</td>
			<tr>
			<td colspan="2" class="center-align">
				<button onclick="window.open('cetak-invoice.php?cmhs={$cmb.ID_C_MHS}', '_blank');
						return false;" >Cetak Invoice Pembayaran</button>
				<button onclick="window.open('print-new.php', '_blank'); return false;">Cetak Formulir Regmaba</button>
			</td>
		</tr>
        </tr>
		{/if}
		<!--
        <tr>
            <td>Total Penghasilan Orang Tua</td>
            <td><label>Rp {number_format($cmb.TOTAL_PENDAPATAN_ORTU,0,',','.')}</label></td>
        </tr>
       {* <tr>
            <td>Kelompok Pembayaran</td>
            <td>{$kelompok.NM_KELOMPOK_BIAYA}
                <input type="hidden" name="id_kelompok_biaya" value="{$kelompok.ID_KELOMPOK_BIAYA}" />
            </td>
        </tr>*}
        {*<tr>
        <td>Kesanggupan Membayar SP3</td>
        <td>
        {if $cmb.ID_PROGRAM_STUDI == $cmb.ID_PILIHAN_1}
        <input type="text" name="sp3_1" value="{$cmb.SP3_1}" />
        <label id="sp3_info"></label>
        {else if $cmb.ID_PROGRAM_STUDI == $cmb.ID_PILIHAN_2}
        <input type="text" name="sp3_2" value="{$cmb.SP3_2}" />
        <label id="sp3_info"></label>
        {else if $cmb.ID_PROGRAM_STUDI == $cmb.ID_PILIHAN_3}
        <input type="text" name="sp3_3" value="{$cmb.SP3_3}" />
        <label id="sp3_info"></label>
        {else if $cmb.ID_PROGRAM_STUDI == $cmb.ID_PILIHAN_4}
        <input type="text" name="sp3_4" value="{$cmb.SP3_4}" />
        <label id="sp3_info"></label>
        {/if}
        <label class="small-text">Min: Rp {number_format($sp3.SP3,0,',','.')}</label>
        <input type="hidden" name="sp3_min" value="{$sp3.SP3}" />
        </td>
        </tr>*}
        <tr>
            <td> Jadwal Verifikasi Keuangan</td>
            <td>
                {if $cmb.ID_JADWAL_VERIFIKASI_KEUANGAN}
                    <input type="hidden" name="id_jadwal_verifikasi_keuangan" value="{$cmb.ID_JADWAL_VERIFIKASI_KEUANGAN}" />
                    {foreach $jadwal_set as $j}
                    {if $j.ID_JADWAL == $cmb.ID_JADWAL_VERIFIKASI_KEUANGAN}{strftime('%d %B %Y', strtotime($j.TGL_JADWAL))} 08.00 - 15.00{/if}
                {/foreach}
            {else}
                <select name="id_jadwal_verifikasi_keuangan">
                    <option value=""></option>
                    {foreach $jadwal_set as $j}
                        <option value="{$j.ID_JADWAL}" {if $j.ID_JADWAL == $cmb.ID_JADWAL_VERIFIKASI_KEUANGAN}selected="selected"{/if}>{strftime('%d %B %Y', strtotime($j.TGL_JADWAL))} {if $j.ISI>=$j.KUOTA}(Penuh){/if}</option>
                    {/foreach}
                </select>
            {/if}
        </td>
    </tr>
    {if $cmb.TGL_JADWAL_VERIFIKASI_PEND!=''}
        <tr>
            <td>Jadwal Verifikasi Pendidikan</td>
            <td>{strftime('%d %B %Y', strtotime($cmb.TGL_JADWAL_VERIFIKASI_PEND))} {$cmb.JAM_AWAL_VERIFIKASI_PEND} - {$cmb.JAM_AKHIR_VERIFIKASI_PEND}</td>
        </tr>
    {/if}
    <tr>
        <td colspan="2" class="center-align">
            <button onclick="window.open('PERATURAN2.pdf', '_blank');
                    return false;" >Lihat Surat Keputusan UKT</button>
            <button onclick="window.open('PERATURAN.pdf', '_blank');
                    return false;" >Lihat Tarif UKT</button>
            <button onclick="window.location = 'form.php?mode=page-1';
                    return false;">Kembali</button>
            <input type="submit" value="Lanjut" />
        </td>
    </tr>
	<!-->
</table>

<script type="text/javascript">$(document).ready(function() {
                    $.getScript('gui/form/s1-snmptn-2.js');
                });</script>

{include file="footer.tpl"}