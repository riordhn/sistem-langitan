var ajax_loading_html       = '<div style="text-align: center"><img src="http://pendaftaran.unair.ac.id/img/ajax-loading.gif" /></div>';

var dialog_kota             = '#dialog-kota';
var dialog_kota_item_text   = '#dialog-kota-item-kota :selected';
var dialog_kota_item        = '#dialog-kota-item-kota';

var dialog_sekolah           = '#dialog-sekolah';
var dialog_sekolah_item_text = '#dialog-sekolah-item-sekolah :selected';
var dialog_sekolah_item      = '#dialog-sekolah-item-sekolah';

$(document).ready(function() {
    
    /* --------------------------------------------------------------- AUTOCOMPLETE KOTA LAHIR */
    $('#button-kota-lahir').click(function() {
        
        $(dialog_kota).dialog({ 
            modal: true, width: 450, draggable: true, title: 'Pilih Kota Kelahiran',
            buttons: {
                Batal: function() { $(dialog_kota).dialog('destroy'); },
                Pilih : function() { 
                    $('#nama-kota-lahir').val($(dialog_kota_item_text).text());
                    $('#id-kota-lahir').val($(dialog_kota_item).val());
                    $(dialog_kota).dialog('destroy');
                }
            },
            close: function() { $(dialog_kota).dialog('destroy'); }
        });
        
        $.ajax({
            url: 'form-kota.php',
            data: 'id_kota='+$('#id-kota-lahir').val(),
            beforeSend: function() { $(dialog_kota).html(ajax_loading_html); },
            success: function(r) { $(dialog_kota).html(r); }
        });
        
        return false;
    });
    /* --------------------------------------------------------------- */
    
    
    /* --------------------------------------------------------------- AUTOCOMPLETE KOTA */
    $('#button-kota').click(function() {
        
        $(dialog_kota).dialog({ 
            modal: true, width: 450, draggable: true, title: 'Pilih Kota',
            buttons: {
                Batal: function() { $(dialog_kota).dialog('destroy'); },
                Pilih : function() { 
                    $('#nama-kota').val($(dialog_kota_item_text).text());
                    $('#id-kota').val($(dialog_kota_item).val());
                    $(dialog_kota).dialog('destroy');
                }
            },
            close: function() { $(dialog_kota).dialog('destroy'); }
        });
        
        $.ajax({
            url: 'form-kota.php',
            data: 'id_kota='+$('#id-kota').val(),
            beforeSend: function() { $(dialog_kota).html(ajax_loading_html); },
            success: function(r) { $(dialog_kota).html(r); }
        });
        
        return false;
    });
    /* --------------------------------------------------------------- */
    
    
    /* --------------------------------------------------------------- AJAX PRODI PILIHAN */
    $('select[name="id_pilihan_1"]').change(function() {
        
        $('select[name="id_pilihan_2"]').html('');  // clear next pilihan
        $('select[name="id_pilihan_3"]').html('');  // clear next pilihan
        $('select[name="id_pilihan_4"]').html('');  // clear next pilihan
        
        $.ajax({
            url: 'form.php',
            type: 'GET',
            data: 'mode=get-prodi&terpilih[]='+$('select[name="id_pilihan_1"]').val(),
            success: function(r) {
                $('select[name="id_pilihan_2"]').html(r);
            }
        });
    });
    
    $('select[name="id_pilihan_2"]').change(function() {
        
        $('select[name="id_pilihan_3"]').html('');  // clear next pilihan
        $('select[name="id_pilihan_4"]').html('');  // clear next pilihan
        
        $.ajax({
            url: 'form.php',
            type: 'GET',
            data: 'mode=get-prodi&terpilih[]='+$('select[name="id_pilihan_1"]').val()+','+$('select[name="id_pilihan_2"]').val(),
            success: function(r) {
                $('select[name="id_pilihan_3"]').html(r);
            }
        });
    });
    
    $('select[name="id_pilihan_3"]').change(function() {
        
        $('select[name="id_pilihan_4"]').html('');  // clear next pilihan
        
        $.ajax({
            url: 'form.php',
            type: 'GET',
            data: 'mode=get-prodi&terpilih[]='+$('select[name="id_pilihan_1"]').val()+','+$('select[name="id_pilihan_2"]').val()+','+$('select[name="id_pilihan_3"]').val(),
            success: function(r) {
                $('select[name="id_pilihan_4"]').html(r);
            }
        });
    });
    /* --------------------------------------------------------------- */


	$('#id_kewarganegaraan').change(function(){
            $.ajax({
                type:'post',
                url:'getWna.php',
                data:'id_kewarganegaraan='+$('#id_kewarganegaraan').val(),
                success:function(data){
                    $('#wna-hide').html(data);
                }                    
            })
        });
    
    $('form').validate({
		rules: {
			nm_c_mhs:           { required: true },
            nm_kota_lahir:      { required: true },
            tgl_lahir_Year:     { required: true, number: true },
            alamat:             { required: true },
            nm_kota:            { required: true },
            telp:               { required: true },
            jenis_kelamin:      { required: true },
            kewarganegaraan:    { required: true },
            id_agama:           { required: true },
            sumber_biaya:       { required: true },
            email:              { email: true },
            
            ptn_s1:             { required: true },
            status_ptn_s1:      { required: true },
            prodi_s1:           { required: true },
            tgl_masuk_s1_Year:  { required: true },
            tgl_lulus_s1_Year:  { required: true },
            lama_studi_s1:      { required: true, number: true},
            ip_s1:              { required: true, number: true },
            jumlah_karya_ilmiah:{ required: true, number: true },

id_kebangsaan:  { required: true },

            
            file_ijazah:    {accept: 'pdf'}
            
		},
		messages: {
			nm_c_mhs:           { required: 'Silahkan isi nama anda disini' },
            nm_kota_lahir:      { required: 'Pilih kota kelahiran, apabila tidak ada dalam daftar silahkan menghubungi panitia pendaftaran' },
            tgl_lahir_Year:     { required: 'Masukkan tanggal kelahiran', number: 'Masukkan tanggal kelahiran dengan benar' },
            alamat:             { required: 'Masukkan alamat anda' },
            nm_kota:            { required: 'Masukkan kota alamat anda' },
            telp:               { required: 'Masukkan telp anda, bisa rumah / hp' },
            jenis_kelamin:      { required: 'Pilih jenis kelamin' },
            kewarganegaraan:    { required: 'Pilih kewarganegaraan anda' },
            id_agama:           { required: 'Pilih agama anda' },
            sumber_biaya:       { required: 'Pilih sumber biaya yang anda' },
            email:              { email: 'Masukkan email yang benar' },
            
            ptn_s1:             { required: 'Masukkan perguruan tinggi asal' },
            status_ptn_s1:      { required: 'Pilih status perguruan tinggi asal' },
            prodi_s1:           { required: 'Masukkan asal prodi anda ketika D3' },
            tgl_masuk_s1_Year:  { required: 'Masukkan tanggal masuk D3' },
            tgl_lulus_s1_Year:  { required: 'Masukkan tanggal lulus D3' },
            lama_studi_s1:      { required: 'Masukkan lama tempuh studi D3', number: 'Masukkan lama tempuh studi D3 dengan benar (koma diganti titik)' },
            ip_s1:              { required: 'Masukkan IPK kelulusan ', number: 'Masukkan IPK kelulusan dengan benar (koma diganti titik)' },
            jumlah_karya_ilmiah:{ required: 'Masukkan jumlah karya ilmiah yang sudah di buat', number: 'Masukkan dengan format angka (koma diganti titik)' },

id_kebangsaan:       { required: 'Pilih negara asal anda' },

            
            file_ijazah:    { accept: 'Harap memilih file pdf untuk diupload'}
		},
        ignore : '.ignore',
        errorPlacement: function(error, element) { error.appendTo(element.parent()); }
	});
    
    // Menampilkan tombol submit
    //$('#row-submit').show('slow');
    $('#table-formulir').fadeIn('slow');
});