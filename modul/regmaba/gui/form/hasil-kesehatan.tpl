{include file="header.tpl"}
<table id="table-formulir" width='500'>
<caption>PENGUMUMAN HASIL TES KESEHATAN DAN ELPT</caption>
    <tr>
        <th colspan="2">Biodata</th>
    </tr>
    <tr>
        <td>Nama</td>
        <td>{$cmb.NM_C_MHS}</td>
    </tr>
    {if isset($hasil_kesehatan) and $hasil_kesehatan != ''}
    {if $hasil_kesehatan.KESEHATAN_KESIMPULAN_AKHIR == 1}
    <tr>
    	<td>NIM</td>
        <td>{$hasil_kesehatan.NIM_MHS}</td>
    </tr>    
    {/if}
    {/if}
    <tr>
        <td>No Ujian</td>
        <td>{$cmb.NO_UJIAN}</td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>{$cmb.NM_FAKULTAS}</td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>{$cmb.NM_PROGRAM_STUDI}</td>
    </tr>
    <tr>
        <th colspan="2">Pengumuman Hasil Tes Kesehatan</th>
    </tr>
    <tr>
        <td>Jadwal Tes Kesehatan</td>
        <td>{$hasil_kesehatan.TGL_TEST_KESEHATAN}</td>
    </tr>
    <tr>
        <td>Hasil Tes Kesehatan</td>
        <td>
        {if isset($hasil_kesehatan) and $hasil_kesehatan != ''}
            {if $hasil_kesehatan.KESEHATAN_KESIMPULAN_AKHIR == 1}Lulus
            {elseif $hasil_kesehatan.KESEHATAN_KESIMPULAN_AKHIR == 0 and $hasil_kesehatan.TGL_VERIFIKASI_KESEHATAN != ''}Tidak Lulus
            {else}
                Belum Tes
            {/if}
        {else}
            Belum Waktunya Pengumuman
        {/if}
        </td>
    </tr>
    <tr>
        <th colspan="2">Pengumuman Hasil Tes ELPT</th>
    </tr>
    <tr>
        <td>Jadwal Tes ELPT</td>
        <td>{$hasil_elpt.TGL_TEST_ELPT}</td>
    </tr>
    <tr>
        <td>Hasil Tes ELPT</td>
        <td>{$hasil_elpt.ELPT_SCORE}</td>
    </tr>
</table>
{include file="footer.tpl"}