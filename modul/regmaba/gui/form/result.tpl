{include file="header.tpl"}

<table>
    <caption>Registrasi Mahasiswa Baru Universitas Airlangga</caption>
    {if $cmb.ID_JALUR == 1}
        {if $cmb.FILE_IJAZAH == 0 or $cmb.FILE_AKTE == 0 or $cmb.FILE_KK == 0}
        <tr>
            <td colspan="2">
                <div class="ui-widget">
                    <div class="ui-state-error ui-corner-all" style="padding: 10px 5px 0px 10px;"> 
                        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                        <strong>Perhatian :</strong><br/>Sebelum file Ijazah / Surat Keterangan Lulus, Akte Kelahiran, dan KK diupload.<br/> Pengisian formulir registrasi mahasiswa baru dianggap belum lengkap.</p>
                    </div>
                </div>
            </td>
        </tr>
        {/if}
    {/if}
    <tr>
        <th class="center-align">Informasi</th>
    </tr>
    <tr>
        <td class="center-align" colspan="2">
            <h2>Form berhasil disimpan</h2>
            {if $cmb.ID_JALUR == 1}{* SNMPTN *}
                <p>Calon Mahasiswa Baru wajib datang ke tempat verifikasi sesuai tanggal yang telah dipilih</p>
            {else}
                <p>Formulir bisa dicetak dengan meklik tombol cetak dibawah ini.</p>
            {/if}
        </td>
    </tr>
    <tr>
        <td class="center-align" colspan="2">
            {if $cmb.ID_JALUR == 1}{* SNMPTN *}
                {if $status_bidik_misi}
                    <button onclick="window.location='form.php?mode=page-1'; return false;">Kembali</button>
                {else}
                    <button onclick="window.location='form.php?mode=page-2'; return false;">Kembali</button>
                {/if}
                {*if $cmb.FILE_IJAZAH and $cmb.FILE_AKTE and $cmb.FILE_KK*}
				{if $cmb.FILE_IJAZAH}
                <button onclick="window.open('print-new.php', '_blank'); return false;">Cetak</button>
                {/if}
            {else}
                <button onclick="window.location='form.php'; return false;">Kembali</button>
                <button onclick="window.open('print-new.php', '_blank'); return false;">Cetak</button>
            {/if}
        </td>
    </tr>
</table>

{include file="footer.tpl"}