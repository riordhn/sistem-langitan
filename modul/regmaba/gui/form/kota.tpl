<table>
    <tr>
        <td>Negara</td>
        <td>
            <select id="dialog-kota-item-negara">
                <option value=""></option>
            {foreach $negara_set as $n}
                <option value="{$n.ID_NEGARA}" {if $n.ID_NEGARA == $kota.ID_NEGARA}selected="selected"{/if}>{$n.NM_NEGARA}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Provinsi</td>
        <td>
            <select id="dialog-kota-item-provinsi">
            {if !empty($provinsi_set)}
                <option value=""></option>
                {foreach $provinsi_set as $p}
                    <option value="{$p.ID_PROVINSI}" {if $p.ID_PROVINSI == $kota.ID_PROVINSI}selected="selected"{/if}>{$p.NM_PROVINSI}</option>
                {/foreach}
            {/if}
            </select>
        </td>
    </tr>
    <tr>
        <td>Kota</td>
        <td>
            <select id="dialog-kota-item-kota">
            {if !empty($kota_set)}
                <option value=""></option>
                {foreach $kota_set as $k}
                    <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $kota.ID_KOTA}selected="selected"{/if}>{$k.NM_KOTA}</option>
                {/foreach}
            {/if}
            </select>
        </td>
    </tr>
</table>
            
<div style="display: none; text-align: center" id="loading-dialog"><img src="http://pendaftaran.unair.ac.id/img/ajax-loading.gif" /></div>
        
<script type="text/javascript">
    $('#dialog-kota-item-negara').change(function() {
        $.ajax({
            url: 'form-kota.php',
            data: 'mode=get-provinsi&id_negara=' + $('#dialog-kota-item-negara').val(),
            beforeSend: function() { 
                $('#dialog-kota-item-provinsi').html('');
                $('#dialog-kota-item-kota').html(''); 
                $('#loading-dialog').css('display', 'block'); 
            },
            success: function(r) { 
                $('#dialog-kota-item-provinsi').html(r); 
                $('#loading-dialog').css('display', 'none'); 
            }
        });
    });
    
    $('#dialog-kota-item-provinsi').change(function() {
        $.ajax({
            url: 'form-kota.php',
            data: 'mode=get-kota&id_provinsi=' + $('#dialog-kota-item-provinsi').val(),
            beforeSend: function() { 
                $('#dialog-kota-item-kota').html(''); 
            },
            success: function(r) { 
                $('#dialog-kota-item-kota').html(r); 
            }
        });
    });
</script>