var ajax_loading_html       = '<div style="text-align: center"><img src="http://pendaftaran.unair.ac.id/img/ajax-loading.gif" /></div>';

var dialog_kota             = '#dialog-kota';
var dialog_kota_item_text   = '#dialog-kota-item-kota :selected';
var dialog_kota_item        = '#dialog-kota-item-kota';

var dialog_sekolah           = '#dialog-sekolah';
var dialog_sekolah_item_text = '#dialog-sekolah-item-sekolah :selected';
var dialog_sekolah_item      = '#dialog-sekolah-item-sekolah';

function GetTotalPendapatanOrtu()
{
    var total_pendapatan = 0;
    
    $('input[id^="pendapatan-"]').each(function(index, item) {
        total_pendapatan += parseInt($(item).val());
    });
    
    if (isNaN(total_pendapatan))
        return 0;
    else
        return total_pendapatan;
}

$(document).ready(function() {
    
    /* --------------------------------------------------------------- AUTOCOMPLETE KOTA LAHIR */
    $('#button-kota-lahir').click(function() {
        
        $(dialog_kota).dialog({ 
            modal: true, width: 450, draggable: true, title: 'Pilih Kota Kelahiran',
            buttons: {
                Batal: function() { $(dialog_kota).dialog('destroy'); },
                Pilih : function() { 
                    $('#nama-kota-lahir').val($(dialog_kota_item_text).text());
                    $('#id-kota-lahir').val($(dialog_kota_item).val());
                    $(dialog_kota).dialog('destroy');
                }
            },
            close: function() { $(dialog_kota).dialog('destroy'); }
        });
        
        $.ajax({
            url: 'form-kota.php',
            data: 'id_kota='+$('#id-kota-lahir').val(),
            beforeSend: function() { $(dialog_kota).html(ajax_loading_html); },
            success: function(r) { $(dialog_kota).html(r); }
        });
        
        return false;
    });
    /* --------------------------------------------------------------- */
    
    
    /* --------------------------------------------------------------- AUTOCOMPLETE KOTA */
    $('#button-kota').click(function() {
        
        $(dialog_kota).dialog({ 
            modal: true, width: 450, draggable: true, title: 'Pilih Kota',
            buttons: {
                Batal: function() { $(dialog_kota).dialog('destroy'); },
                Pilih : function() { 
                    $('#nama-kota').val($(dialog_kota_item_text).text());
                    $('#id-kota').val($(dialog_kota_item).val());
                    $(dialog_kota).dialog('destroy');
                }
            },
            close: function() { $(dialog_kota).dialog('destroy'); }
        });
        
        $.ajax({
            url: 'form-kota.php',
            data: 'id_kota='+$('#id-kota').val(),
            beforeSend: function() { $(dialog_kota).html(ajax_loading_html); },
            success: function(r) { $(dialog_kota).html(r); }
        });
        
        return false;
    });
    /* --------------------------------------------------------------- */
    
    
    /* --------------------------------------------------------------- AUTOCOMPLETE ASAL SEKOLAH */
    $('#button-sekolah-asal').click(function() {
        
        $(dialog_sekolah).dialog({ 
            modal: true, width: 500, draggable: true, title: 'Pilih asal sekolah',
            buttons: {
                Batal: function() { $(dialog_sekolah).dialog('destroy'); },
                Pilih : function() { 
                    $('#nama-sekolah-asal').val($(dialog_sekolah_item_text).text());
                    $('#id-sekolah-asal').val($(dialog_sekolah_item).val());
                    $(dialog_sekolah).dialog('destroy');
                }
            },
            close: function() { $(dialog_sekolah).dialog('destroy'); }
        });
        
        $.ajax({
            url: 'form-sekolah.php',
            data: 'id_sekolah='+$('#id-sekolah-asal').val(),
            beforeSend: function() { $(dialog_sekolah).html(ajax_loading_html); },
            success: function(r) { $(dialog_sekolah).html(r); }
        });
        
        return false;
    });
    /* --------------------------------------------------------------- */
    
    
    /* --------------------------------------------------------------- AUTOCOMPLETE KOTA AYAH */
    $('#button-kota-ayah').click(function() {
        
        $(dialog_kota).dialog({ 
            modal: true, width: 450, draggable: true, title: 'Pilih asal kota Ayah',
            buttons: {
                Batal: function() { $(dialog_kota).dialog('destroy'); },
                Pilih : function() { 
                    $('#nama-kota-ayah').val($(dialog_kota_item_text).text());
                    $('#id-kota-ayah').val($(dialog_kota_item).val());
                    $(dialog_kota).dialog('destroy');
                }
            },
            close: function() { $(dialog_kota).dialog('destroy'); }
        });
        
        $.ajax({
            url: 'form-kota.php',
            data: 'id_kota='+$('#id-kota-ayah').val(),
            beforeSend: function() { $(dialog_kota).html(ajax_loading_html); },
            success: function(r) { $(dialog_kota).html(r); }
        });
        
        return false;
    });
    /* --------------------------------------------------------------- */
    
    /* --------------------------------------------------------------- AUTOCOMPLETE KOTA IBU */
    $('#button-kota-ibu').click(function() {
        
        $(dialog_kota).dialog({ 
            modal: true, width: 450, draggable: true, title: 'Pilih asal kota Ibu',
            buttons: {
                Batal: function() { $(dialog_kota).dialog('destroy'); },
                Pilih : function() { 
                    $('#nama-kota-ibu').val($(dialog_kota_item_text).text());
                    $('#id-kota-ibu').val($(dialog_kota_item).val());
                    $(dialog_kota).dialog('destroy');
                }
            },
            close: function() { $(dialog_kota).dialog('destroy'); }
        });
        
        $.ajax({
            url: 'form-kota.php',
            data: 'id_kota='+$('#id-kota-ibu').val(),
            beforeSend: function() { $(dialog_kota).html(ajax_loading_html); },
            success: function(r) { $(dialog_kota).html(r); }
        });
        
        return false;
    });
    /* --------------------------------------------------------------- */
    
    
    /* --------------------------------------------------------------- PENGISIAN PENDAPATAN ORTU */
    
    $('input[id^="pendapatan-"]').each(function(index, item) {
        
        if ($(item).val() == '') {$(item).val(0);}
        
        $(item).keyup(function() {
            var total_pendapatan_ortu = GetTotalPendapatanOrtu();
            $('input[name="total_pendapatan_ortu"]').val(total_pendapatan_ortu);
            $('input[name="total_pendapatan_ortu"]').formatCurrency('#total-pendapatan-ortu', { symbol: 'Rp ', digitGroupSymbol: '.', roundToDecimalPlace: 0 });
        });
    })
    /* --------------------------------------------------------------- */



	$('#id_kewarganegaraan').change(function(){
            $.ajax({
                type:'post',
                url:'getWna.php',
                data:'id_kewarganegaraan='+$('#id_kewarganegaraan').val(),
                success:function(data){
                    $('#wna-hide').html(data);
                }                    
            })
        });

    
    $('form').validate({
		rules: {
			nm_c_mhs:           {required: true},
            nm_kota_lahir:      {required: true},
            tgl_lahir_Year:     {required: true, number: true},
            alamat:             {required: true},
            nm_kota:            {required: true},
            telp:               {required: true},
            jenis_kelamin:      {required: true},
            kewarganegaraan:    {required: true},
            id_agama:           {required: true},
            sumber_biaya:       {required: true},
            email:              {required: true, email: true},
            
            nm_sekolah_asal:         {required: true},
            jurusan_sekolah:         {required: true},
            tahun_lulus:             {number: true},
            jumlah_pelajaran_ijazah: {number: true},
            nilai_ijazah:            {number: true},
            tahun_uan:               {number: true},
            jumlah_pelajaran_uan:    {number: true},
            nilai_uan:               {number: true},
            
            nama_ayah:          {required: true},
            alamat_ayah:        {required: true},
            nm_kota_ayah:       {required: true},
            telp_ayah:          {required: true}, 
            pendidikan_ayah:    {required: true},
            pekerjaan_ayah:     {required: true},
            instansi_ayah:      {required: true},
            jabatan_ayah:       {required: true},
            masa_kerja_ayah:    {number: true},
            
            nama_ibu:           {required: true},
            alamat_ibu:         {required: true},
            nm_kota_ibu:        {required: true},
            telp_ibu:           {required: true},
            pendidikan_ibu:     {required: true},
            pekerjaan_ibu:      {required: true},
            instansi_ibu:       {required: true},
            jabatan_ibu:        {required: true},
            masa_kerja_ibu:    {number: true},
            
            penghasilan_ortu:   {required: true},
            jumlah_kakak:       {required: true, number: true},
            jumlah_adik:        {required: true, number: true},
            
            kediaman_ortu:      {required: true},
            luas_tanah:         {required: true},
            luas_bangunan:      {required: true},
            njop:               {required: true},
            listrik:            {required: true},
            kendaraan_r4:       {required: true},
            tahun_kendaraan_r4: {required: true, number: true},
            kendaraan_r2:       {required: true},
            tahun_kendaraan_r2: {required: true, number: true},

id_kebangsaan:  { required: true },

            
            gaji_ayah:                  {required: true, number: true},
            tunjangan_keluarga_ayah:    {required: true, number: true},
            tunjangan_jabatan_ayah:     {required: true, number: true},
            tunjangan_sertifikasi_ayah: {required: true, number: true},
            tunjangan_kehormatan_ayah:  {required: true, number: true},
            renumerasi_ayah:            {required: true, number: true},
            tunjangan_lain_ayah:        {required: true, number: true},
            penghasilan_lain_ayah:      {required: true, number: true},

            gaji_ibu:                   {required: true, number: true},
            tunjangan_keluarga_ibu:     {required: true, number: true},
            tunjangan_jabatan_ibu:      {required: true, number: true},
            tunjangan_sertifikasi_ibu:  {required: true, number: true},
            tunjangan_kehormatan_ibu:   {required: true, number: true},
            renumerasi_ibu:             {required: true, number: true},
            tunjangan_lain_ibu:         {required: true, number: true},
            penghasilan_lain_ibu:       {required: true, number: true},
            
            file_ijazah:    {required: '#status_file_ijazah:blank', accept: 'pdf'},
            file_skhun:     {accept: 'pdf'},
            file_akte:      {required: '#status_file_akte:blank', accept: 'pdf'},
            file_kk:        {required: '#status_file_kk:blank', accept: 'pdf'}
		},
		messages: {
			nm_c_mhs:           {required: 'Silahkan isi nama anda disini'},
            nm_kota_lahir:      {required: 'Pilih kota kelahiran, apabila tidak ada dalam daftar silahkan menghubungi panitia pendaftaran'},
            tgl_lahir_Year:     {required: 'Masukkan tanggal kelahiran', number: 'Masukkan tanggal kelahiran dengan benar'},
            alamat:             {required: 'Masukkan alamat anda'},
            nm_kota:            {required: 'Masukkan kota alamat anda'},
            telp:               {required: 'Masukkan telp anda, bisa rumah / hp'},
            jenis_kelamin:      {required: 'Pilih jenis kelamin'},
            kewarganegaraan:    {required: 'Pilih kewarganegaraan anda'},
            id_agama:           {required: 'Pilih agama anda'},
            sumber_biaya:       {required: 'Pilih sumber biaya yang anda'},
            email:              {required: 'Masukkan email yang benar', email: 'Masukkan email yang benar'},
            
            nm_sekolah_asal:        {required: 'Pilih asal sekolah anda'},
            jurusan_sekolah:        {required: 'Pilih jurusan sekolah anda'},
            tahun_lulus:             {number: 'Masukkan format angka'},
            jumlah_pelajaran_ijazah: {number: 'Masukkan format angka'},
            nilai_ijazah:            {number: 'Masukkan format angka'},
            tahun_uan:               {number: 'Masukkan format angka'},
            jumlah_pelajaran_uan:    {number: 'Masukkan format angka'},
            nilai_uan:               {number: 'Masukkan format angka'},
            
            nama_ayah:          {required: 'Masukkan nama ayah'},
            alamat_ayah:        {required: 'Masukkan alamat ayah tanpa kota'},
            nm_kota_ayah:       {required: 'Masukkan kota alamat ayah'},
            telp_ayah:          {required: 'Masukkan telp ayah'},
            pendidikan_ayah:    {required: 'Pilih pendidikan terakhir ayah'},
            pekerjaan_ayah:     {required: 'Pilih pekerjaan terakhir ayah'},
            instansi_ayah:      {required: 'Masukkan instai atau perusahaan tempat ayah bekerja'},
            jabatan_ayah:       {required: 'Masukan jabatan ayah'},
            masa_kerja_ayah:    {number: 'Masukkan masa kerja ayah dalam angka tahun'},
            
            nama_ibu:           {required: 'Masukkan nama ibu'},
            alamat_ibu:         {required: 'Masukkan alamat ibu tanpa kota'},
            nm_kota_ibu:        {required: 'Masukkan kota alamat ibu'},
            telp_ibu:           {required: 'Masukkan telp ibu'},
            pendidikan_ibu:     {required: 'Pilih pendidikan terakhir ibu'},
            pekerjaan_ibu:      {required: 'Pilih pekerjaan terakhir ibu'},
            instansi_ibu:       {required: 'Masukkan instai atau perusahaan tempat ibu bekerja'},
            jabatan_ibu:        {required: 'Masukan jabatan ibu'},
            masa_kerja_ibu:     {number: 'Masukkan masa kerja ibu dalam angka tahun'},
            
            penghasilan_ortu:   {required: 'Pilih penghasilan ortu'},
            jumlah_kakak:       {required: 'Masukkan jumlah kakak', number: 'Masukkan jumlah kakak dengan benar'},
            jumlah_adik:        {required: 'Masukkan jumlah adik', number: 'Masukkan jumlah adik dengan benar'},
            
            kediaman_ortu:      {required: 'Pilih kediaman ortu'},
            luas_tanah:         {required: 'Pilih luas tanah yang dipunyai'},
            luas_bangunan:      {required: 'Pilih luas bangunan yang dipunyai'},
            njop:               {required: 'Pilih besar NJOP'},
            listrik:            {required: 'Pilih besar daya listrik yang dipakai'},
            kendaraan_r4:       {required: 'Pilih jumlah kendaraan R4 yang dipunyai'},
            tahun_kendaraan_r4: {required: 'Masukan tahun kendaraan R4 yang paling akhir', number: 'Isikan dalam format angka'},
            kendaraan_r2:       {required: 'Pilih jumlah kendaraan R2 yang dipunyai'},
            tahun_kendaraan_r2: {required: 'Masukan tahun kendaraan R2 yang paling akhir', number: 'Isikan dalam format angka'},
            
            gaji_ayah:                  {required: 'Isikan gaji ayah dalam format angka', number: 'Isikan dalam format angka'},
            tunjangan_keluarga_ayah:    {required: 'Isikan tunjangan keluarga ayah dalam format angka', number: 'Isikan dalam format angka'},
            tunjangan_jabatan_ayah:     {required: 'Isikan tunjangan jabatan ayah dalam format angka', number: 'Isikan dalam format angka'},
            tunjangan_sertifikasi_ayah: {required: 'Isikan tunjangan sertifikasi ayah dalam format angka', number: 'Isikan dalam format angka'},
            tunjangan_kehormatan_ayah:  {required: 'Isikan tunjangan kehormatan ayah dalam format angka', number: 'Isikan dalam format angka'},
            renumerasi_ayah:            {required: 'Isikan renumerasi ayah dalam format angka', number: 'Isikan dalam format angka'},
            tunjangan_lain_ayah:        {required: 'Isikan tunjangan lain ayah dalam format angka', number: 'Isikan dalam format angka'},
            penghasilan_lain_ayah:      {required: 'Isikan penghasilan lain ayah dalam format angka', number: 'Isikan dalam format angka'},

            gaji_ibu:                   {required: 'Isikan gaji ibu dalam format angka', number: 'Isikan dalam format angka'},
            tunjangan_keluarga_ibu:     {required: 'Isikan tunjangan keluarga ibu dalam format angka', number: 'Isikan dalam format angka'},
            tunjangan_jabatan_ibu:      {required: 'Isikan tunjangan jabatan ibu dalam format angka', number: 'Isikan dalam format angka'},
            tunjangan_sertifikasi_ibu:  {required: 'Isikan tunjangan sertifikasi ibu dalam format angka', number: 'Isikan dalam format angka'},
            tunjangan_kehormatan_ibu:   {required: 'Isikan tunjangan kehormatan ibu dalam format angka', number: 'Isikan dalam format angka'},
            renumerasi_ibu:             {required: 'Isikan renumerasi ibu dalam format angka', number: 'Isikan dalam format angka'},
            tunjangan_lain_ibu:         {required: 'Isikan tunjangan lain ibu dalam format angka', number: 'Isikan dalam format angka'},
            penghasilan_lain_ibu:       {required: 'Isikan penghasilan lain ibu dalam format angka', number: 'Isikan dalam format angka'},

id_kebangsaan:       { required: 'Pilih negara asal anda' },

            
            file_ijazah:    {required: 'Harap memilih file pdf untuk di upload', accept: 'Harap memilih file pdf untuk diupload'},
            file_skhun:     {accept: 'Harap memilih file pdf untuk diupload'},
            file_akte:      {required: 'Harap memilih file pdf untuk di upload', accept: 'Harap memilih file pdf untuk diupload'},
            file_kk:        {required: 'Harap memilih file pdf untuk di upload', accept: 'Harap memilih file pdf untuk diupload'}
		},
        ignore : '.ignore',
        errorPlacement: function(error, element) { error.appendTo(element.parent()); }
	});
    
    // Menampilkan tombol submit
    //$('#row-submit').show('slow');
    $('#table-formulir').fadeIn('slow');
});