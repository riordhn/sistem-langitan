{include file="header.tpl"}

<form action="form.php?mode=page-result" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
<input type="hidden" name="mode" value="d3" />
<table id="table-formulir" style="display: none">
    <caption>Registrasi Mahasiswa Baru Program Diploma 3 (D3) Universitas Airlangga</caption>
    <tr>
        <th colspan="2">Program Studi Diterima</th>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td><strong>{$cmb.NM_FAKULTAS}</strong></td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td><strong>{$cmb.NM_PROGRAM_STUDI}</strong></td>
    </tr>
    <tr>
        <th colspan="2">Data Diri</th>
    </tr>
    <tr>
        <td>No Ujian</td>
        <td>{$cmb.NO_UJIAN}</td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>{$cmb.NM_C_MHS}
            <input type="hidden" name="nm_c_mhs" maxlength="64" size="64" value="{$cmb.NM_C_MHS}"/>
        </td>
    </tr>
    <tr>
        <td>Tempat Lahir</td>
        <td>
            <input type="hidden" name="id_kota_lahir" id="id-kota-lahir" value="{$cmb.ID_KOTA_LAHIR}" />
            <input type="text" name="nm_kota_lahir" id="nama-kota-lahir" size="50" value="{$cmb.NM_KOTA_LAHIR}" readonly="readonly"/>
            <button id="button-kota-lahir">Edit</button>
        </td>
    </tr>
    <tr>
        <td>Tanggal Lahir</td>
        <td>{html_select_date prefix="tgl_lahir_" time=$cmb.TGL_LAHIR day_empty="" month_empty="" year_empty="" field_order="DMY" start_year=-80 end_year=-10}</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>
            <input type="text" name="alamat" size="65" maxlength="250" value="{$cmb.ALAMAT}" />
        </td>
    </tr>
    <tr>
        <td>Kota</td>
        <td>
            <input type="hidden" name="id_kota" id="id-kota" value="{$cmb.ID_KOTA}" />
            <input type="text" name="nm_kota" id="nama-kota" size="50" value="{$cmb.NM_KOTA}" readonly="readonly"/>
            <button id="button-kota">Edit</button>
        </td>
    </tr>
    <tr>
        <td>No Telp</td>
        <td><input type="text" name="telp"  maxlength="32" value="{$cmb.TELP}"/></td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>
            <select name="jenis_kelamin">
            {foreach $jenis_kelamin_set as $jk}
                <option value="{$jk.JENIS_KELAMIN}" {if $cmb.JENIS_KELAMIN == $jk.JENIS_KELAMIN}selected="selected"{/if}>{$jk.NM_JENIS_KELAMIN}</option>
            {/foreach}
            </select>
		</td>
    </tr>
    <tr>
        <td>Kewarganegaraan</td>
        <td>
			<select name="kewarganegaraan" id="id_kewarganegaraan">
			{foreach $kewarganegaraan_set as $kw}
				<option value="{$kw.ID_KEWARGANEGARAAN}" {if $cmb.KEWARGANEGARAAN == $kw.ID_KEWARGANEGARAAN}selected="selected"{/if}>{$kw.NM_KEWARGANEGARAAN}</option>
			{/foreach}
			</select>
        </td>
    </tr>
     <tr id="wna-hide">
    	<td>Asal Negara</td>
        <td>
            <select id="id_kebangsaan" name="id_kebangsaan">
            {if $cmb.ID_KEBANGSAAN == 114}
            {foreach $negara_indo_set as $n}
                <option value="{$n.ID_NEGARA}" {if $n.ID_NEGARA == $cmb.ID_KEBANGSAAN}selected="selected"{/if}>{$n.NM_NEGARA}</option>
            {/foreach} 
            {else}
                <option value=""></option>
            {foreach $negara_set as $n}
                <option value="{$n.ID_NEGARA}" {if $n.ID_NEGARA == $cmb.ID_KEBANGSAAN}selected="selected"{/if}>{$n.NM_NEGARA}</option>
            {/foreach}
            {/if}
        </td>
    </tr>
    <tr>
        <td>Agama</td>
        <td>
			<select name="id_agama">
			{foreach $agama_set as $a}
				<option value="{$a.ID_AGAMA}" {if $cmb.ID_AGAMA == $a.ID_AGAMA}selected="selected"{/if}>{$a.NM_AGAMA}</option>
			{/foreach}
			</select>
		</td>
    </tr>
    <tr>
        <td>Sumber Biaya</td>
        <td>
			<select name="sumber_biaya">
			{foreach $sumber_biaya_set as $sb}
				<option value="{$sb.ID_SUMBER_BIAYA}" {if $cmb.SUMBER_BIAYA == $sb.ID_SUMBER_BIAYA}selected="selected"{/if}>{$sb.NM_SUMBER_BIAYA}</option>
			{/foreach}
			</select>
		</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>
            <input type="text" name="email" size="30" maxlength="64" value="{$cmb.EMAIL}" />
        </td>
    </tr>
    <tr>
        <th colspan="2">Data Pendidikan</th>
    </tr>
    <tr>
        <td>Asal SMTA / MA</td>
        <td>
            <input type="hidden" name="id_sekolah_asal" id="id-sekolah-asal" value="{$cmb.ID_SEKOLAH_ASAL}" />
            <input type="text" name="nm_sekolah_asal" id="nama-sekolah-asal" size="50" value="{$cmb.NM_SEKOLAH_ASAL}" readonly="readonly"/>
            <button id="button-sekolah-asal">Edit</button>
        </td>
    </tr>
    <tr>
        <td>Jurusan SMTA / MA</td>
        <td>
			<select name="jurusan_sekolah">
			{foreach $jurusan_sekolah_set as $js}
				<option value="{$js.ID_JURUSAN_SEKOLAH}" {if $cmb.JURUSAN_SEKOLAH == $js.ID_JURUSAN_SEKOLAH}selected="selected"{/if}>{$js.NM_JURUSAN_SEKOLAH}</option>
			{/foreach}
			</select>
		</td>
    </tr>
    <tr>
        <td>No &amp; Tgl Ijazah SMTA</td>
        <td> No Ijazah:
            <input type="text" name="no_ijazah"  maxlength="40" value="{$cmb.NO_IJAZAH}" />
            <br/>
            Tgl Ijazah: {html_select_date prefix="tgl_ijazah_" day_empty="" month_empty="" year_empty="" field_order="DMY" start_year="-15" time=$cmb.TGL_IJAZAH}
        </td>
    </tr>
    <tr>
        <td>Ijazah</td>
        <td> Tahun :
            <input type="text" name="tahun_lulus" size="4" maxlength="4" value="{$cmb.TAHUN_LULUS}"/>
            <br/>
            Jumlah Mata Pelajaran :
            <input type="text" name="jumlah_pelajaran_ijazah" size="2" maxlength="2" value="{$cmb.JUMLAH_PELAJARAN_IJAZAH}"/>
            <br/>
            Jumlah Nilai Akhir :
            <input type="text" name="nilai_ijazah" size="5" maxlength="5" value="{$cmb.NILAI_IJAZAH}" /> 
            * Bukan rata-rata</td>
    </tr>
    <tr>
        <td>UAN</td>
        <td> Tahun :
            <input type="text" name="tahun_uan" size="4" maxlength="4" value="{$cmb.TAHUN_UAN}"/>
            <br/>
            Jumlah Mata Pelajaran :
            <input type="text" name="jumlah_pelajaran_uan" size="2" maxlength="2" value="{$cmb.JUMLAH_PELAJARAN_UAN}"/>
            <br/>
            Jumlah Nilai UAN :
            <input type="text" name="nilai_uan" size="5" maxlength="5" value="{$cmb.NILAI_UAN}"/>
            * Bukan rata-rata</td>
    </tr>
    <tr>
        <th colspan="2" >Data Keluarga - Ayah</td>
    </tr>
    <tr>
        <td>Nama Ayah</td>
        <td><input type="text" name="nama_ayah" maxlength="64"  value="{$cmb.NAMA_AYAH}"/></td>
    </tr>
    <tr>
        <td>Alamat Ayah</td>
        <td><input type="text" name="alamat_ayah" size="65" maxlength="200" value="{$cmb.ALAMAT_AYAH}" /></td>
    </tr>
    <tr>
        <td>Kota</td>
        <td>
            <input type="hidden" name="id_kota_ayah" id="id-kota-ayah" value="{$cmb.ID_KOTA_AYAH}" />
            <input type="text" name="nm_kota_ayah" id="nama-kota-ayah" size="50" value="{$cmb.NM_KOTA_AYAH}" readonly="readonly"/>
            <button id="button-kota-ayah">Edit</button>
        </td>
    </tr>
    <tr>
        <td>Telp</td>
        <td><input type="text" name="telp_ayah"  maxlength="32" value="{$cmb.TELP_AYAH}"/></td>
    </tr>
    <tr>
        <td>Pendidikan Ayah</td>
        <td>
            <select name="pendidikan_ayah">
            {foreach $pendidikan_ortu_set as $po}
                <option value="{$po.ID_PENDIDIKAN_ORTU}" {if $cmb.PENDIDIKAN_AYAH == $po.ID_PENDIDIKAN_ORTU}selected="selected"{/if}>{$po.NM_PENDIDIKAN_ORTU}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Pekerjaan Ayah</td>
        <td>
            <select name="pekerjaan_ayah">
            {foreach $pekerjaan_set as $p}
                <option value="{$p.ID_PEKERJAAN}" {if $cmb.PEKERJAAN_AYAH == $p.ID_PEKERJAAN}selected="selected"{/if}>{$p.NM_PEKERJAAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Instansi / Perusahaan Tempat Kerja</td>
        <td><input type="text" name="instansi_ayah" size="30" maxlength="50" value="{$cmb.INSTANSI_AYAH}" /></td>
    </tr>
    <tr>
        <td>Jabatan / Golongan</td>
        <td><input type="text" name="jabatan_ayah" size="30" maxlength="50" value="{$cmb.JABATAN_AYAH}" /></td>
    </tr>
    <tr>
        <td>Masa Kerja (jika ada)</td>
        <td><input type="text" name="masa_kerja_ayah" size="4" maxlength="4" value="{$cmb.MASA_KERJA_AYAH}" /></td>
    </tr>
	<tr>
        <th colspan="2" >Data Keluarga - Ibu</td>
    </tr>
    <tr>
        <td>Nama Ibu </td>
        <td><input type="text" name="nama_ibu" maxlength="64"  value="{$cmb.NAMA_IBU}"/></td>
    </tr>
    <tr>
        <td>Alamat Ibu</td>
        <td><input type="text" name="alamat_ibu" size="65" maxlength="200" value="{$cmb.ALAMAT_IBU}" /></td>
    </tr>
    <tr>
        <td>Kota</td>
        <td>
            <input type="hidden" name="id_kota_ibu" id="id-kota-ibu" value="{$cmb.ID_KOTA_IBU}" />
            <input type="text" name="nm_kota_ibu" id="nama-kota-ibu" size="50" value="{$cmb.NM_KOTA_IBU}" readonly="readonly"/>
            <button id="button-kota-ibu">Edit</button>
        </td>
    </tr>
    <tr>
        <td>Telp</td>
        <td><input type="text" name="telp_ibu"  maxlength="32" value="{$cmb.TELP_IBU}"/></td>
    </tr>
    <tr>
        <td>Pendidikan Ibu</td>
        <td>
            <select name="pendidikan_ibu">
            {foreach $pendidikan_ortu_set as $po}
                <option value="{$po.ID_PENDIDIKAN_ORTU}" {if $cmb.PENDIDIKAN_IBU == $po.ID_PENDIDIKAN_ORTU}selected="selected"{/if}>{$po.NM_PENDIDIKAN_ORTU}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Pekerjaan Ibu</td>
        <td>
            <select name="pekerjaan_ibu">
            {foreach $pekerjaan_set as $p}
                <option value="{$p.ID_PEKERJAAN}" {if $cmb.PEKERJAAN_IBU == $p.ID_PEKERJAAN}selected="selected"{/if}>{$p.NM_PEKERJAAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Instansi / Perusahaan Tempat Kerja</td>
        <td><input type="text" name="instansi_ibu" size="30" maxlength="50" value="{$cmb.INSTANSI_IBU}" /></td>
    </tr>
    <tr>
        <td>Jabatan / Golongan</td>
        <td><input type="text" name="jabatan_ibu" size="30" maxlength="50" value="{$cmb.JABATAN_IBU}" /></td>
    </tr>
    <tr>
        <td>Masa Kerja (jika ada)</td>
        <td><input type="text" name="masa_kerja_ibu" size="4" maxlength="4" value="{$cmb.MASA_KERJA_IBU}" /></td>
    </tr>
	<tr>
        <th colspan="2" >Data Keluarga - Lain-Lain</td>
    </tr>	
	<tr>
        <td>Skala Usaha/Pekerjaan</td>
        <td>
            <select name="skala_pekerjaan_ortu">
            {foreach $skala_pekerjaan_set as $sp}
                <option value="{$sp.SKALA_PEKERJAAN}" {if $cmb.SKALA_PEKERJAAN_ORTU == $sp.SKALA_PEKERJAAN}selected="selected"{/if}>{$sp.NM_SKALA_PEKERJAAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Jumlah Kakak</td>
        <td><input type="text" name="jumlah_kakak" size="2" maxlength="2"  value="{$cmb.JUMLAH_KAKAK}" /></td>
    </tr>
    <tr>
        <td>Jumlah Adik</td>
        <td><input type="text" name="jumlah_adik" size="2" maxlength="2"  value="{$cmb.JUMLAH_ADIK}" /></td>
    </tr>
	<tr>
        <th colspan="2">Kediaman dan Kendaraan Keluarga</th>
    </tr>
    <tr>
        <td>Kediaman Orang Tua</td>
        <td>
            <select name="kediaman_ortu">
            {foreach $kediaman_ortu_set as $ko}
                <option value="{$ko.KEDIAMAN_ORTU}" {if $cmb.KEDIAMAN_ORTU == $ko.KEDIAMAN_ORTU}selected="selected"{/if}>{$ko.NM_KEDIAMAN_ORTU}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Luas Tanah Kediaman</td>
        <td>
            <select name="luas_tanah">
            {foreach $luas_tanah_set as $lt}
                <option value="{$lt.LUAS_TANAH}" {if $cmb.LUAS_TANAH == $lt.LUAS_TANAH}selected="selected"{/if}>{$lt.NM_LUAS_TANAH}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Luas Bangunan Kediaman</td>
        <td>
            <select name="luas_bangunan">
            {foreach $luas_bangunan_set as $lb}
                <option value="{$lb.LUAS_BANGUNAN}" {if $cmb.LUAS_BANGUNAN == $lb.LUAS_BANGUNAN}selected="selected"{/if}>{$lb.NM_LUAS_BANGUNAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>NJOP</td>
        <td>
            <select name="njop">
            {foreach $njop_set as $n}
                <option value="{$n.NJOP}" {if $cmb.NJOP == $n.NJOP}selected="selected"{/if}>{$n.NM_NJOP}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Daya Listrik</td>
        <td>
            <select name="listrik">
            {foreach $listrik_set as $l}
                <option value="{$l.LISTRIK}" {if $cmb.LISTRIK == $l.LISTRIK}selected="selected"{/if}>{$l.NM_LISTRIK}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Kendaraan Bermotor R4</td>
        <td>
            <select name="kendaraan_r4">
            {foreach $kendaraan_r4_set as $kr4}
                <option value="{$kr4.KENDARAAN}" {if $cmb.KENDARAAN_R4 == $kr4.KENDARAAN}selected="selected"{/if}>{$kr4.NM_KENDARAAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Tahun Kendaraan R4<br/>(Yg paling akhir)</td>
        <td><input type="text" name="tahun_kendaraan_r4" size="4" maxlength="4" value="{$cmb.TAHUN_KENDARAAN_R4}" /></td>
    </tr>
    <tr>
        <td>Kendaraan bermotor R2</td>
        <td>
            <select name="kendaraan_r2">
            {foreach $kendaraan_r2_set as $kr2}
                <option value="{$kr2.KENDARAAN}" {if $cmb.KENDARAAN_R2 == $kr2.KENDARAAN}selected="selected"{/if}>{$kr2.NM_KENDARAAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Tahun Kendaraan R2<br/>(Yg paling akhir)</td>
        <td><input type="text" name="tahun_kendaraan_r2" size="4" maxlength="4" value="{$cmb.TAHUN_KENDARAAN_R2}" /></td>
    </tr>
    <tr>
        <td>Kepemilikan Kekayaan Lainya :</td>
        <td>
            <textarea name="kekayaan_lain" cols="75" rows="2" maxlength="256">{$cmb.KEKAYAAN_LAIN}</textarea>
        </td>
    </tr>
    <tr>
        <td>Informasi lainnya, sebutkan :</td>
        <td>
            <textarea name="info_lain" cols="75" rows="2" maxlength="256">{$cmb.INFO_LAIN}</textarea>
        </td>
    </tr>
    <tr>
			<th colspan="2">Data Penghasilan Ayah</th>
		</tr>
		<tr>
			<td>Gaji Pokok</td>
			<td><input type="text" name="gaji_ayah" id="pendapatan-ayah-1" size="20" maxlength="20" value="{$cmb.GAJI_AYAH}" style="text-align:right" class="" /></td>
		</tr>
		<tr>
			<td>Tunjangan Keluarga</td>
			<td><input type="text" name="tunjangan_keluarga_ayah" id="pendapatan-ayah-2" size="20" maxlength="20" value="{$cmb.TUNJANGAN_KELUARGA_AYAH}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<td>Tunjangan Jabatan / Golongan</td>
			<td><input type="text" name="tunjangan_jabatan_ayah" id="pendapatan-ayah-3" size="20" maxlength="20" value="{$cmb.TUNJANGAN_JABATAN_AYAH}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<td>Tunjangan Sertifikasi Guru / Dosen</td>
			<td><input type="text" name="tunjangan_sertifikasi_ayah" id="pendapatan-ayah-4" size="20" maxlength="20" value="{$cmb.TUNJANGAN_SERTIFIKASI_AYAH}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<td>Tunjangan Kehormatan</td>
			<td><input type="text" name="tunjangan_kehormatan_ayah" id="pendapatan-ayah-5" size="20" maxlength="20" value="{$cmb.TUNJANGAN_KEHORMATAN_AYAH}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<td>Remunerasi</td>
			<td><input type="text" name="renumerasi_ayah" id="pendapatan-ayah-6" size="20" maxlength="20" value="{$cmb.RENUMERASI_AYAH}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<td>Tunjangan lain-lain</td>
			<td><input type="text" name="tunjangan_lain_ayah" id="pendapatan-ayah-7" size="20" maxlength="20" value="{$cmb.TUNJANGAN_LAIN_AYAH}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<td>Penghasilan lain-lain</td>
			<td><input type="text" name="penghasilan_lain_ayah" id="pendapatan-ayah-8" size="20" maxlength="20" value="{$cmb.PENGHASILAN_LAIN_AYAH}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<th colspan="2">Data Penghasilan Ibu </th>
		</tr>
		<tr>
			<td>Gaji Pokok</td>
			<td><input type="text" name="gaji_ibu" id="pendapatan-ibu-1" size="20" maxlength="20" value="{$cmb.GAJI_IBU}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<td>Tunjangan Keluarga</td>
			<td><input type="text" name="tunjangan_keluarga_ibu" id="pendapatan-ibu-2" size="20" maxlength="20" value="{$cmb.TUNJANGAN_KELUARGA_IBU}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<td>Tunjangan Jabatan / Golongan</td>
			<td><input type="text" name="tunjangan_jabatan_ibu" id="pendapatan-ibu-3" size="20" maxlength="20" value="{$cmb.TUNJANGAN_JABATAN_IBU}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<td>Tunjangan Sertifikasi Guru / Dosen</td>
			<td><input type="text" name="tunjangan_sertifikasi_ibu" id="pendapatan-ibu-4" size="20" maxlength="20" value="{$cmb.TUNJANGAN_SERTIFIKASI_IBU}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<td>Tunjangan Kehormatan Guru Besar</td>
			<td><input type="text" name="tunjangan_kehormatan_ibu" id="pendapatan-ibu-5" size="20" maxlength="20" value="{$cmb.TUNJANGAN_KEHORMATAN_IBU}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<td>Remunerasi</td>
			<td><input type="text" name="renumerasi_ibu" size="20" id="pendapatan-ibu-6" maxlength="20" value="{$cmb.RENUMERASI_IBU}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<td>Tunjangan lain-lain</td>
			<td><input type="text" name="tunjangan_lain_ibu" size="20" id="pendapatan-ibu-7" maxlength="20" value="{$cmb.TUNJANGAN_LAIN_IBU}" style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<td>Penghasilan lain-lain</td>
			<td><input type="text" name="penghasilan_lain_ibu" id="pendapatan-ibu-8" size="20" maxlength="20" value="{$cmb.PENGHASILAN_LAIN_IBU}"  style="text-align:right" class=""/></td>
		</tr>
		<tr>
			<th colspan="2">Total Penghasilan Orang Tua</th>
		</tr>
		<tr>
			<td>Total Penghasilan Orang Tua</td>
			<td>
				<input type="text" id="total-pendapatan-ortu" class="right-align" value="Rp {number_format($cmb.TOTAL_PENDAPATAN_ORTU,0,',','.')}" readonly="readonly" style="background-color: #dedede"/>
				<input type="hidden" name="total_pendapatan_ortu" value="{$cmb.TOTAL_PENDAPATAN_ORTU}" />
			</td>
		</tr>
    <tr>
        <th colspan="2">Upload File Berkas</th>
    </tr>
    <tr>
        <td>File Ijazah / Surat Keterangan Lulus</td>
        <td>
            <input type="hidden" id="status_file_ijazah" {if $cmb.FILE_IJAZAH}value="1"{/if} />
            {if $cmb.FILE_IJAZAH}<a target="_blank" href="http://cybercampus.unair.ac.id/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_ijazah.pdf">{$cmb.ID_C_MHS}_ijazah.pdf</a>{/if}
            <input type="file" accept="application/pdf" name="file_ijazah"/>
        </td>
    </tr>
    <tr>
        <td>File SKHUN<label class="small-text">Bisa menyusul</label>
        </td>
        <td>
            <input type="hidden" id="status_file_skhun" {if $cmb.FILE_SKHUN}value="1"{/if} />
            {if $cmb.FILE_SKHUN}<a target="_blank" href="http://cybercampus.unair.ac.id/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_skhun.pdf">{$cmb.ID_C_MHS}_skhun.pdf</a>{/if}
            <input type="file" accept="application/pdf" name="file_skhun"/>
        </td>
    </tr>
    <tr>
        <td>File Akte Kelahiran</td>
        <td>
            <input type="hidden" id="status_file_akte" {if $cmb.FILE_AKTE}value="1"{/if} />
            {if $cmb.FILE_AKTE}<a target="_blank" href="http://cybercampus.unair.ac.id/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_akte.pdf">{$cmb.ID_C_MHS}_akte.pdf</a>{/if}
            <input type="file" accept="application/pdf" name="file_akte"/>
        </td>
    </tr>
    <tr>
        <td>File KK</td>
        <td>
            <input type="hidden" id="status_file_kk" {if $cmb.FILE_KK}value="1"{/if} />
            {if $cmb.FILE_KK}<a target="_blank" href="http://cybercampus.unair.ac.id/files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_kk.pdf">{$cmb.ID_C_MHS}_kk.pdf</a>{/if}
            <input type="file" accept="application/pdf" name="file_kk"/>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>* Semua file upload harus bertipe (*.pdf) dan besar per file tidak lebih dari 5 MB.</p>
            <p>* Untuk mengkonversi hasil scan berkas yg berupa image (*.jpg) ke (*.pdf), bisa menggunakan aplikasi berikut ini : <a target="_blank" href="http://www.softsea.com/download.php?id=836715621">JPEG to PDF</a></p>
            <p>* Petunjuk penggunaan aplikasi JPEG to PDF <a target="_blank" href="http://cybercampus.unair.ac.id/img/maba/tutorial-jpegtopdf.png">klik disini</a></p>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center-align">
            <input type="hidden" name="id_pilihan_1" value="{$cmb.ID_PILIHAN_1}" />
            <input type="hidden" name="id_pilihan_2" value="{$cmb.ID_PILIHAN_2}" />
            <input type="hidden" name="id_pilihan_3" value="{$cmb.ID_PILIHAN_3}" />
            <input type="hidden" name="id_pilihan_4" value="{$cmb.ID_PILIHAN_4}" />
            <input type="hidden" name="id_kelompok_biaya" value="{$cmb.ID_KELOMPOK_BIAYA}" />
            <input type="submit" value="Lanjut" />
        </td>
    </tr>
</table>
</form>

<div id="dialog-kota"></div><div id="dialog-sekolah"></div>
<script type="text/javascript">$(document).ready(function() { $.getScript('gui/form/d3.js'); });</script>

{include file="footer.tpl"}