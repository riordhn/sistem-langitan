<html>
    <head>
        <title>Registrasi Mahasiswa Baru (Regmaba) - Universitas Airlangga</title>
        <meta name="google-site-verification" content="ihkYFpHX_zbP4SfrkqIEF7c2Lp08dZFIxJ2MKI6m_Hk" />
        <link rel="shortcut icon" href="http://cybercampus.unair.ac.id/img/icon.ico"/>
        <link rel="stylesheet" type="text/css" href="http://cybercampus.unair.ac.id/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="http://regmaba.unair.ac.id/css/style.css" />
        <meta name="description" content="Halaman registrasi ulang mahasiswa baru yang telah diterima di Universitas Airlangga">
        <script>

        </script>
        <style>
            .button {

                -moz-box-shadow:inset 0px 1px 0px 0px #54a3f7;
                -webkit-box-shadow:inset 0px 1px 0px 0px #54a3f7;
                box-shadow:inset 0px 1px 0px 0px #54a3f7;

                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #007dc1), color-stop(1, #044c8a));
                background:-moz-linear-gradient(top, #007dc1 5%, #044c8a 100%);
                background:-webkit-linear-gradient(top, #007dc1 5%, #044c8a 100%);
                background:-o-linear-gradient(top, #007dc1 5%, #044c8a 100%);
                background:-ms-linear-gradient(top, #007dc1 5%, #044c8a 100%);
                background:linear-gradient(to bottom, #007dc1 5%, #044c8a 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#007dc1', endColorstr='#044c8a',GradientType=0);

                background-color:#007dc1;

                -moz-border-radius:3px;
                -webkit-border-radius:3px;
                border-radius:3px;

                border:1px solid #124d77;

                display:inline-block;
                color:#ffffff;
                font-family:arial;
                font-size:10px;
                font-weight:normal;
                padding:5px;
                text-decoration:none;

                text-shadow:0px 1px 0px #154682;

            }
            .button:hover {

                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #044c8a), color-stop(1, #007dc1));
                background:-moz-linear-gradient(top, #044c8a 5%, #007dc1 100%);
                background:-webkit-linear-gradient(top, #044c8a 5%, #007dc1 100%);
                background:-o-linear-gradient(top, #044c8a 5%, #007dc1 100%);
                background:-ms-linear-gradient(top, #044c8a 5%, #007dc1 100%);
                background:linear-gradient(to bottom, #044c8a 5%, #007dc1 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#044c8a', endColorstr='#007dc1',GradientType=0);

                background-color:#044c8a;
            }
            .button:active {
                position:relative;
                top:1px;
            }
        </style>
    </head>

    <body>
        <div class="wrap-box">

            <div class="login-box">
                <table style="margin-left:35px;">
                    <tr>
                        <td class="center-align">
                            <strong>
                                <a class='button' href="formulir/?login=snmptn&lihat=kesehatan">Login Jalur SNMPTN</a>
                                <a class='button' href="formulir/?login=sbmptn&lihat=kesehatan">Login Jalur SBMPTN</a>
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="center-align"><strong><a  class="button" href="formulir/?login=mandiri&lihat=kesehatan">Klik disini untuk login dari S1 Mandiri </a></strong></td>
                    </tr>
                    <tr>
                        <td class="center-align"><strong><a  class="button" href="mandiri/">Klik disini untuk login dari S1 Alih Jenis / Diploma 3</a></strong></td>
                    </tr>

                    <tr>
                        <td  class="center-align">
                            <a class="button" href="http://regmaba.unair.ac.id/" style="">Halaman Utama</a>
                        </td>
                    </tr>
                    <tr>
                        <td><br/><br/><br/><br/>
                            <h1><strong><font color="red">Gunakan Google Chrome untuk mengakses aplikasi ini</font></strong></h1>
                        </td>
                    </tr>
                </table>
            </div>

            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="login-box2" style="display:none">
                <table>
                    <tr>
                        <td class="center-align"><strong><a href="">Pengumuman Daftar Ulang SNMPTN / SBMPTN Ujian Tulis</a></strong></td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>
