<html>
    <head>
        <title>Regmaba Mandiri (S1, Alih Jenis, Diploma 3) - Universitas Airlangga</title>
        <meta name="google-site-verification" content="ihkYFpHX_zbP4SfrkqIEF7c2Lp08dZFIxJ2MKI6m_Hk" />
        <link rel="shortcut icon" href="http://cybercampus.unair.ac.id/img/icon.ico"/>
        <link rel="stylesheet" type="text/css" href="http://cybercampus.unair.ac.id/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="http://regmaba.unair.ac.id/css/style.css" />
        <meta name="description" content="Halaman registrasi ulang mahasiswa baru jalur mandiri Universitas Airlangga">
        <style>
        .button {

            -moz-box-shadow:inset 0px 1px 0px 0px #54a3f7;
            -webkit-box-shadow:inset 0px 1px 0px 0px #54a3f7;
            box-shadow:inset 0px 1px 0px 0px #54a3f7;

            background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #007dc1), color-stop(1, #044c8a));
            background:-moz-linear-gradient(top, #007dc1 5%, #044c8a 100%);
            background:-webkit-linear-gradient(top, #007dc1 5%, #044c8a 100%);
            background:-o-linear-gradient(top, #007dc1 5%, #044c8a 100%);
            background:-ms-linear-gradient(top, #007dc1 5%, #044c8a 100%);
            background:linear-gradient(to bottom, #007dc1 5%, #044c8a 100%);
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#007dc1', endColorstr='#044c8a',GradientType=0);

            background-color:#007dc1;

            -moz-border-radius:3px;
            -webkit-border-radius:3px;
            border-radius:3px;

            border:1px solid #124d77;

            display:inline-block;
            color:#ffffff;
            font-family:arial;
            font-size:10px;
            font-weight:normal;
            padding:5px;
            text-decoration:none;

            text-shadow:0px 1px 0px #154682;

        }
        .button:hover {

            background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #044c8a), color-stop(1, #007dc1));
            background:-moz-linear-gradient(top, #044c8a 5%, #007dc1 100%);
            background:-webkit-linear-gradient(top, #044c8a 5%, #007dc1 100%);
            background:-o-linear-gradient(top, #044c8a 5%, #007dc1 100%);
            background:-ms-linear-gradient(top, #044c8a 5%, #007dc1 100%);
            background:linear-gradient(to bottom, #044c8a 5%, #007dc1 100%);
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#044c8a', endColorstr='#007dc1',GradientType=0);

            background-color:#044c8a;
        }
        .button:active {
            position:relative;
            top:1px;
        }
    </style>
    </head>

    <body>
		<div class="wrap-box">
            <div class="login-box">
                <form action="index.php" method="post">
                    <input type="hidden" name="mode" value="login">
                    <table>
                        <tr>
                            <td class="right-align"><strong>No Ujian</strong></td>
                            <td>
                                <input name="no_ujian" type="text" maxlength="12" style="width: 110px">
                            </td>
                        </tr>
                        <tr>
                            <td class="right-align"><strong>Kode Voucher</strong></td>
                            <td>
                                <input name="kode_voucher" type="text" maxlength="16" style="width: 110px" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" class="button" value="Login" />
								<button class="button" onclick="window.location = '/index.php'; return false;">Kembali</button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>