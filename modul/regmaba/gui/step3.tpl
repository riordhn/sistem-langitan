<div class="center_title_bar">Cetak - Step 3 of 3</div>

{if $cm.ID_JALUR == 20 || $cm.ID_JALUR == 3 || $cm.ID_JALUR == 5 || $cm.ID_JALUR == 27}
<p style="text-align: center; font-size: 120%"><br/><a class="disable-ajax" href="print-pmdk.php" target="_blank">Cetak Form Registrasi + Surat Pernyataan</a></p>
{elseif $cm.ID_JALUR == 4}
<p style="text-align: center; font-size: 120%"><br/><a class="disable-ajax" href="print-alih.php" target="_blank">Cetak Form Registrasi + Surat Pernyataan</a></p>
{else}
<p style="text-align: center; font-size: 120%"><br/><a class="disable-ajax" href="print.php" target="_blank">Cetak Form Registrasi + Surat Pernyataan</a></p>
{/if}
{if isset($hasil_kesehatan)}
<p style="text-align: center; font-size: 120%"><br/><a><!--ANDA {if $hasil_kesehatan=='1'}TELAH LULUS {else} TIDAK LULUS{/if}DALAM PEMERIKSAAN HASIL KESEHATAN --></a></p>
{/if}