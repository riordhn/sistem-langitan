<html>
<head>
    <title>Registrasi Mahasiswa Baru (Regmaba) - Universitas Airlangga</title>
    <meta name="google-site-verification" content="ihkYFpHX_zbP4SfrkqIEF7c2Lp08dZFIxJ2MKI6m_Hk" />
    <link rel="shortcut icon" href="http://cybercampus.unair.ac.id/img/icon.ico"/>
    <link rel="stylesheet" type="text/css" href="http://cybercampus.unair.ac.id/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="http://regmaba.unair.ac.id/css/style.css" />
    <meta name="description" content="Halaman registrasi ulang mahasiswa baru yang telah diterima di Universitas Airlangga">
    <script>
		
    </script>
</head>

<body>
    <div class="wrap-box">
    
        <div class="login-box">
	    <br /><br />	
            <table style="margin-left:35px;">
                
				<tr>
                    <td class="center-align"><strong><a href="snmptn/">Klik disini untuk login dari SNMPTN / SBMPTN / PBSB</a></strong></td>
                </tr>
                <tr>
                    <td class="center-align"><strong><a href="mandiri">Klik disini untuk login dari Mandiri / Diploma 3</a></strong></td>
                </tr>
				
                
                <tr>
			<td>
				<h1><strong><font color="red">Gunakan Google Chrome untuk mengakses aplikasi ini</font></strong></h1>
			</td>
                </tr>
		<tr>
			<td  class="center-align">
				<a href="https://www.facebook.com/groups/mhsua2013/" style="background-color:#e5e5e5;color:blue;"><strong>Group Facebook Maba 
2013</strong></a>
			</td>
		</tr>
            </table>
        </div>
        
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<div class="login-box2" style="display:none">
            <table>
                <tr>
                   <td class="center-align"><strong><a href="">Pengumuman Daftar Ulang SNMPTN / SBMPTN Ujian Tulis</a></strong></td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
