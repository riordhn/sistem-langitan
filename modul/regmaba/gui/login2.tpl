<html>
<head>
    <title>Universitas Airlangga Cyber Campus</title>
    <link rel="stylesheet" type="text/css" href="http://cybercampus.unair.ac.id/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="http://cybercampus.unair.ac.id/css/index.css" />
    <script src="http://cybercampus.unair.ac.id/js/jquery-1.5.1.min.js" type="text/javascript"></script>
{literal}<script type="text/javascript">
    $(document).ready(function() {

        $('.show-captcha').click(function() {
            $(this).css('display', 'none');
            $('.username-box').css('display', 'none');
            $('.password-box').css('display', 'none');
            $('input[type="submit"]').css('display', 'block');
            $('.recaptcha-box').show('slow');
            return false;
        });
    });
</script>{/literal}
</head>

<body>
    <form action="login2.php?mode=login" method="post">
    <div class="wrapper_maba">
        <div class="login-box">
            {if $false_login > 3}
            <div class="recaptcha-box" style="display: none;">    
                {$recaptcha}
            </div>
            {/if}
            <div class="username-box">
                <label>NO UJIAN</label>
                <input type="text" name="no_ujian" /><br/>
                <div style="font-size: 80%" align="right"></div>
            </div>
            <!--<div class="password-box">
                <label>NAMA</label>
                <input type="text" name="nm_c_mhs" /><br/>
                <div style="font-size: 80%" align="right">Nama menggunakan huruf kapital</div>
            </div>-->
            <div class="submit-box">
                {if $false_login <= 3}
                    {if isset($smarty.get.err)}{if $smarty.get.err==1}<label style="font-size: 80%">Login salah</label>{/if}{/if}
                    <input type="submit" value="Login" />
                {else}
                    <button class="show-captcha">Login</button>
                    <input type="submit" value="Login" style="display: none;" />
                {/if}
            </div>
        </div>
    </div>
    </form>
</body>
</html>