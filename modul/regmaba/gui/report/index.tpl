{include file="header.tpl"}

<script>setTimeout("location.reload(true);", 300000);</script>

<table>
    <caption>Report Pengisian Form Registrasi Mahasiswa Baru</caption>
    <tr>
        <th colspan="2"></th>
    </tr>
    <tr>
        <td>Penerimaan</td>
        <td>
            <form action="index.php" method="get" id="filter">
                <select name="id_penerimaan" onchange="$('#filter').submit(); return false;">
                    <option value="">-- Pilih Penerimaan --</option>
                {foreach $penerimaan_set as $p}
                    <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                    {foreach $p.p_set as $p2}
                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} </option>
                    {/foreach}
                    </optgroup>
                {/foreach}
                </select>
            </form>
        </td>
    </tr>
</table>
                
<table>
    <tr>
        <th>No</th>
        <th>Program Studi</th>
		<th>Diterima</th>
        {foreach $tanggal_set as $t}
            <th>{$t.TGL_REGMABA|date_format:'%d/%m'}</th>
        {/foreach}
        <th>Regmaba</th>
		<th class="center-align">Belum<br/>Regmaba</th>
    </tr>
	{$total_diterima = 0}
	{$belum_regmaba = 0}
    {foreach $program_studi_set as $ps}
    <tr {if $ps@index is not div by 2}class="alternate"{/if}>
        <td class="center-align">{$ps@index + 1}</td>
        <td>{$ps.NM_PROGRAM_STUDI}</td>
		<td class="center-align">{$ps.DITERIMA}</td>
        {foreach $tanggal_set as $t}
            <td class="center-align">
                {foreach $data_set as $d}
                    {if $d.ID_PROGRAM_STUDI == $ps.ID_PROGRAM_STUDI && $d.TGL_REGMABA == $t.TGL_REGMABA}{$d.JUMLAH}{/if}
                {/foreach}
            </td>
        {/foreach}
        <td class="center-align"><strong>{$ps.JUMLAH}</strong></td>
		<td class="center-align"><strong>{$ps.DITERIMA - $ps.JUMLAH}</strong></td>
    </tr>
	{$total_diterima = $total_diterima + $ps.DITERIMA}
	{$belum_regmaba = $belum_regmaba + ($ps.DITERIMA - $ps.JUMLAH)}
    {/foreach}
    <tr>
        <td></td>
        <td>Jumlah</td>
		<td class="center-align">{$total_diterima}</td>
        {$total = 0}
        {foreach $tanggal_set as $t}
            <td class="center-align"><strong>{$t.JUMLAH}</strong></td>
            {$total = $total + $t.JUMLAH}
        {/foreach}
        <td class="center-align"><strong>{$total}</strong></td>
		<td class="center-align"><strong>{$belum_regmaba}</strong></td>
    </tr>
</table>

{include file="footer.tpl"}