{literal}<style type="text/css">
.center         { text-align: center; }
.formulir       { width: 100% }
.formulir tr td { font-size: 13px; }
.formulir tr td h2 { margin: 0px; }
.formulir tr td h3 { margin: 0px; }
.formulir tr td h4 { margin: 0px; }
label.error        { display: none; color: #f00; font-size: 12px; }
</style>{/literal}

<div class="center_title_bar">Formulir Registrasi Biodata Mahasiswa - Step 1 of 2</div>

<form action="verifikasi.php" method="post">
<input type="hidden" name="mode" value="save_biodata_s2" />
<table style="width: 100%; margin-left: 20px" class="formulir">
    <col style="width: 30px" />
    <col style="width: 250px" />
    <col style="" />
    <tr>
        <td colspan="3"><h2>Program Studi</h2></td>
    </tr>
    <tr>
        <td>1</td>
        <td>Fakultas</td>
        <td>{$cm.NM_FAKULTAS}</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Program Studi</td>
        <td>{$cm.NM_PROGRAM_STUDI}</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Jenjang Studi</td>
        <td>{$cm.NM_JENJANG}</td>
    </tr>
    <tr>
        <td colspan="3"><h2>Biodata Mahasiswa Baru</h2></td>
    </tr>
    <tr>
        <td>4</td>
        <td>No. Test</td>
        <td>{$cm.NO_UJIAN}</td>
    </tr>
    <tr>
        <td>5</td>
        <td>Nama Lengkap</td>
        <td>{$cm.NM_C_MHS}</td>
    </tr>
    <tr>
        <td>6</td>
        <td>No Mhs</td>
        <td></td>
    </tr>
    <tr>
        <td>7</td>
        <td>Tempat dan Tanggal Lahir</td>
        <td>
            <select name="id_kota_lahir" class="required">
                <option value=""></option>
            {for $i=0 to $provinsi_set->Count()-1}{$provinsi=$provinsi_set->Get($i)}
                <optgroup label="{$provinsi->NM_PROVINSI}">
                {for $j=0 to $provinsi->KOTAs->Count()-1}{$kota=$provinsi->KOTAs->Get($j)}
                    <option value="{$kota->ID_KOTA}" {if $cm.ID_KOTA_LAHIR==$kota->ID_KOTA}selected="selected"{/if}>{$kota->NM_KOTA}</option>
                {/for}
                </optgroup>
            {/for}
            </select>,
            {html_select_date prefix="tgl_lahir_" field_order="DMY" start_year="-80" end_year="-14" time=$calon_mahasiswa->TGL_LAHIR}
            <br/>
            <label for="id_kota_lahir" class="error" style="display: none;">Pilih kota kelahiran</label>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>Jenis Kelamin</td>
        <td>
            <label><input type="radio" name="jenis_kelamin" value="1" class="required"/>Laki-Laki</label>
            <label><input type="radio" name="jenis_kelamin" value="2" />Perempuan</label>
            <br/>
            <label for="jenis_kelamin" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>9</td>
        <td>Agama</td>
        <td>
            <label><input type="radio" name="id_agama" value="1" class="required" />Islam</label>
            <label><input type="radio" name="id_agama" value="2" />Kristen Protestan</label>
            <label><input type="radio" name="id_agama" value="3" />Kristen Katholik</label>
            <label><input type="radio" name="id_agama" value="4" />Hindu</label>
            <label><input type="radio" name="id_agama" value="5" />Budha</label>
            <label><input type="radio" name="id_agama" value="6" />Lain-lain</label>
            <br/>
            <label for="id_agama" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>10</td>
        <td>Status Perkawinan</td>
        <td>
            <label><input type="radio" name="status_perkawinan" value="1" class="required" />Belum Kawin</label>
            <label><input type="radio" name="status_perkawinan" value="2" />Kawin</label>
            <label><input type="radio" name="status_perkawinan" value="3" />Bercerai</label>
            <label><input type="radio" name="status_perkawinan" value="4" />Kawin, Suami / Istri Meninggal</label>
            <br/>
            <label for="status_perkawinan" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>11</td>
        <td>Biaya Pendidikan</td>
        <td>
            <label><input type="radio" name="sumber_biaya" value="5" class="required" />Biaya Sendiri</label>
            <label><input type="radio" name="sumber_biaya" value="3" />Beasiswa : <input type="text" name="beasiswa" /></label>
            <br/>
            <label for="sumber_biaya" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>12</td>
        <td>PT. Asal / Instansi Pengirim</td>
        <td>
            <input type="text" name="pt_instansi" maxlength="50" size="50" />
        </td>
    </tr>
    <tr>
        <td>13</td>
        <td>Alamat Kantor</td>
        <td>
            <textarea name="alamat_kantor" cols="50" rows="2"></textarea>
        </td>
    </tr>
</table>
<div style="text-align: center">
    <input type="submit" value="Proses" />
</div>
</form>

{literal}<script type="text/javascript">
$('input:radio').click(function() {
    if ($(this).attr('name') == 'sumber_biaya')
    {
        if ($(this).attr('value') == 3) {
            $('input[name="beasiswa"]').addClass("required");
            $('input[name="beasiswa"]').focus();
        }
        else {
            $('input[name="beasiswa"]').removeClass("required");
        }
    }
});
    
$('form').validate();
</script>{/literal}