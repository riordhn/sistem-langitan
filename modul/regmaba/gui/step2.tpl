{literal}<style type="text/css">
.center         { text-align: center; }
.formulir       { width: 100% }
.formulir tr td { font-size: 13px; }
.formulir tr td h2 { margin: 0px; }
.formulir tr td h3 { margin: 0px; }
.formulir tr td h4 { margin: 0px; }
label.error        { display: none; color: #f00; font-size: 12px; }
</style>{/literal}

<div class="center_title_bar">Formulir Registrasi Biodata Mahasiswa - Step 2 of 3</div>
    
<form action="verifikasi.php" method="post">
<input type="hidden" name="mode" value="save_biodata" />
<table style="width: 100%; margin-left: 20px" class="formulir">
    <col style="width: 30px" />
    <col style="width: 250px" />
    <col style="" />
    <tr>
        <td colspan="3"><h3>Program Studi</h3></td>
    </tr>
    <tr>
        <td>1</td>
        <td>Fakultas</td>
        <td>{$cm.NM_FAKULTAS}</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Program Studi</td>
        <td>{$cm.NM_PROGRAM_STUDI}</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Jenjang Studi</td>
        <td>{$cm.NM_JENJANG}</td>
    </tr>
    <tr>
        <td colspan="3"><h3>Biodata Mahasiswa Baru</h3></td>
    </tr>
    <tr>
        <td>4</td>
        <td>Nama Lengkap</td>
        <td>
            <input type="text" name="nm_c_mhs" size="64" maxlength="64" class="required" value="{$cm.NM_C_MHS}" />
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td>Nomor Ujian</td>
        <td>{$cm.NO_UJIAN}</td>
    </tr>
    <tr>
        <td>6</td>
        <td>Tempat dan Tanggal Lahir</td>
        <td>
            <select name="id_kota_lahir" class="required">
                <option value=""></option>
                {foreach $provinsi_set as $p}
                    <optgroup label="{$p.NM_PROVINSI}">
                    {foreach $p.kota_set as $k}<option value="{$k.ID_KOTA}" {if $k.ID_KOTA==$cm.ID_KOTA_LAHIR}selected="selected"{/if}>{$k.NM_KOTA}</option>{/foreach}
                    </optgroup>
                {/foreach}
            </select>,
            {html_select_date prefix="tgl_lahir_" field_order="DMY" start_year="-80" end_year="-14" time=$cm.TGL_LAHIR}
            <br/>
            <label for="id_kota_lahir" class="error" style="display: none;">Pilih kota kelahiran</label>
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td>Alamat Mahasiswa</td>
        <td>
            <textarea name="alamat" maxlength="250" cols="50" class="required">{$cm.ALAMAT}</textarea>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td>No Telp</td>
        <td><input type="text" name="telp" class="required" maxlength="32" value="{$cm.TELP}" /></td>
    </tr>
    <tr>
        <td>9</td>
        <td>Jenis Kelamin</td>
        <td>
            <label><input type="radio" name="jenis_kelamin" value="1" class="required" {if $cm.JENIS_KELAMIN==1}checked="checked"{/if}>Laki-Laki</label>
            <label><input type="radio" name="jenis_kelamin" value="2" {if $cm.JENIS_KELAMIN==2}checked="checked"{/if}>Perempuan</label>
            <br/>
            <label for="jenis_kelamin" class="error" style="display: none;">Pilih salah satu</label>
    </tr>
    <tr>
        <td>10</td>
        <td>Kewarganegaraan</td>
        <td>
            <label><input type="radio" name="kewarganegaraan" value="1" {if $cm.KEWARGANEGARAAN==1}checked="checked"{/if} class="required" />Indonesia</label>
            <label><input type="radio" name="kewarganegaraan" value="2" {if $cm.KEWARGANEGARAAN==2}checked="checked"{/if}/>W.N.A. <input type="text" name="kewarganegaraan_lain" maxlength="20" value="{$cm.KEWARGANEGARAAN_LAIN}" /><label for="kewarganegaraan_lain" class="error" style="display: none;">Harus di isi</label></label>
            <br/>
            <label for="kewarganegaraan" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>11</td>
        <td>Sumber Biaya</td>
        <td>
            <label><input type="radio" name="sumber_biaya" value="1" {if $cm.SUMBER_BIAYA==1}checked="checked"{/if} class="required" />Orang Tua</label><br/>
            <label><input type="radio" name="sumber_biaya" value="2" {if $cm.SUMBER_BIAYA==2}checked="checked"{/if}/>Orang Tua Asuh</label><br/>
            <label><input type="radio" name="sumber_biaya" value="3" {if $cm.SUMBER_BIAYA==3}checked="checked"{/if}/>Beasiswa : <input type="text" name="beasiswa" maxlength="30" value="{$cm.BEASISWA}"/></label><br/>
            <label><input type="radio" name="sumber_biaya" value="4" {if $cm.SUMBER_BIAYA==4}checked="checked"{/if}/>Lain-lain : <input type="text" name="sumber_biaya_lain" maxlength="30" value="{$cm.SUMBER_BIAYA_LAIN}"/></label>
            <br/>
            <label for="sumber_biaya" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>12</td>
        <td>Agama</td>
        <td>
            <label><input type="radio" name="id_agama" value="1" {if $cm.ID_AGAMA==1}checked="checked"{/if} class="required" />Islam</label>
            <label><input type="radio" name="id_agama" value="2" {if $cm.ID_AGAMA==2}checked="checked"{/if}/>Kristen Protestan</label>
            <label><input type="radio" name="id_agama" value="3" {if $cm.ID_AGAMA==3}checked="checked"{/if}/>Kristen Katholik</label>
            <label><input type="radio" name="id_agama" value="4" {if $cm.ID_AGAMA==4}checked="checked"{/if}/>Hindu</label>
            <label><input type="radio" name="id_agama" value="5" {if $cm.ID_AGAMA==5}checked="checked"{/if}/>Budha</label>
            <label><input type="radio" name="id_agama" value="6" {if $cm.ID_AGAMA==6}checked="checked"{/if}/>Lain-lain</label>
            <br/>
            <label for="id_agama" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>13</td>
        <td>Jumlah Kakak</td>
        <td><input type="text" name="jumlah_kakak" size="2" maxlength="2" class="required number" value="{$cm.JUMLAH_KAKAK}"/></td>
    </tr>
    <tr>
        <td>14</td>
        <td>Jumlah Adik</td>
        <td><input type="text" name="jumlah_adik" size="2" maxlength="2" class="required number" value="{$cm.JUMLAH_ADIK}"/></td>
    </tr>
    {if $cm.ID_JALUR == 20 || $cm.ID_JALUR == 3 || $cm.ID_JALUR == 5 || $cm.ID_JALUR == 27} {* DEPAG / MANDIRI / D3 / INTERNASIONAL *}
    <tr>
        <td>15</td>
        <td>Asal SMTA / MA</td>
        <td>
            <select name="id_sekolah_asal" class="required">
                <option value=""></option>
                {foreach $sekolah_set as $s}<option value="{$s.ID_SEKOLAH}" {if $s.ID_SEKOLAH==$cm.ID_SEKOLAH_ASAL}selected="selected"{/if}>{$s.NM_KOTA} - {$s.NM_SEKOLAH}</option>{/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>16</td>
        <td>Jurusan SMTA / MA</td>
        <td>
            <label><input type="radio" name="jurusan_sekolah" value="1" {if $cm.JURUSAN_SEKOLAH==1}checked="checked"{/if} class="required" />SMU / MA IPA</label><br/>
            <label><input type="radio" name="jurusan_sekolah" value="2" {if $cm.JURUSAN_SEKOLAH==2}checked="checked"{/if}/>SMU / MA IPS</label><br/>
            <label><input type="radio" name="jurusan_sekolah" value="3" {if $cm.JURUSAN_SEKOLAH==3}checked="checked"{/if}/>SMU / MA Bahasa</label><br/>
            <label><input type="radio" name="jurusan_sekolah" value="4" {if $cm.JURUSAN_SEKOLAH==4}checked="checked"{/if}/>SMK</label><br/>
            <label><input type="radio" name="jurusan_sekolah" value="5" {if $cm.JURUSAN_SEKOLAH==5}checked="checked"{/if}/>Lain-lain : <input type="text" name="jurusan_sekolah_lain" maxlength="20" value="{$cm.JURUSAN_SEKOLAH_LAIN}"/></label>
            <br/>
            <label for="jurusan_sekolah" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>17</td>
        <td>No & Tgl Ijazah SMTA</td>
        <td>
            No Ijazah: <input type="text" name="no_ijazah" class="required" maxlength="40" value="{$cm.NO_IJAZAH}"/><br/>
            Tgl Ijazah: {html_select_date field_order="DMY" prefix="tgl_ijazah_" start_year="-15" time=$cm.TGL_IJAZAH}
        </td>
    </tr>
    <tr>
        <td>18</td>
        <td>Ijazah</td>
        <td>
            Tahun : <input type="text" name="tahun_lulus" size="4" maxlength="4" class="required number" value="{$cm.TAHUN_LULUS}"/> <br/>
            Jumlah Mata Pelajaran : <input type="text" name="jumlah_pelajaran_ijazah" size="2" maxlength="2" class="required number" value="{$cm.JUMLAH_PELAJARAN_IJAZAH}"/><br/>
            Jumlah Nilai Ijazah : <input type="text" name="nilai_ijazah" size="5" maxlength="5" class="required number" value="{$cm.NILAI_IJAZAH}"/>
        </td>
    </tr>
    <tr>
        <td>19</td>
        <td>UAN</td>
        <td>
            Tahun : <input type="text" name="tahun_uan" size="4" maxlength="4" class="required number" value="{$cm.TAHUN_UAN}"/> <br/>
            Jumlah Mata Pelajaran : <input type="text" name="jumlah_pelajaran_uan" size="2" maxlength="2" class="required number" value="{$cm.JUMLAH_PELAJARAN_UAN}"/><br/>
            Jumlah Nilai UAN : <input type="text" name="nilai_uan" size="5" maxlength="5" class="required number" value="{$cm.NILAI_UAN}"/>
        </td>
    </tr>
    {elseif $cm.ID_JALUR == 4} {* ALIH JENIS *}
    <tr>
        <td colspan="3"><h3>Data Pendidikan</h3></td>
    </tr>
    <tr>
        <td></td>
        <td>Perguruan Tinggi</td>
        <td>
            <input type="text" name="ptn_s1" maxlength="30" size="30" value="{$cmp.PTN_S1}" class="required"/>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>Status Perguruan Tinggi</td>
        <td>
            <label><input type="radio" name="status_ptn_s1" value="1" {if $cmp.STATUS_PTN_S1==1}checked="checked"{/if} class="required"/>Negeri</label>
            <label><input type="radio" name="status_ptn_s1" value="2" {if $cmp.STATUS_PTN_S1==2}checked="checked"{/if}/>Swasta</label>
            <label><input type="radio" name="status_ptn_s1" value="3" {if $cmp.STATUS_PTN_S1==3}checked="checked"{/if}/>Luar Negeri</label>
            <br/>
            <label for="status_ptn_s1" class="error" style="display:none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>Program Studi</td>
        <td><input type="text" name="prodi_s1" maxlength="30" size="30" value="{$cmp.PRODI_S1}" class="required"/></td>
    </tr>
    <tr>
        <td></td>
        <td>Tanggal Masuk</td>
        <td>{html_select_date prefix="tgl_masuk_s1_" field_order="DMY" start_year=-30 time=$cmp.TGL_MASUK_S1}</td>
    </tr>
    <tr>
        <td></td>
        <td>Tanggal Lulus</td>
        <td>{html_select_date prefix="tgl_lulus_s1_" field_order="DMY" start_year=-30 time=$cmp.TGL_LULUS_S1}</td>
    </tr>
    <tr>
        <td></td>
        <td>Lama Studi</td>
        <td><input type="text" name="lama_studi_s1" maxlength="4" size="4" class="number" value="{$cmp.LAMA_STUDI_S1}" /> tahun</td>
    </tr>
    <tr>
        <td></td>
        <td>Index Prestasi</td>
        <td><input type="text" name="ip_s1" maxlength="5" size="5" class="number" value="{$cmp.IP_S1}" /></td>
    </tr>
    {/if}
    {if $cm.ID_JALUR == 3 || $cm.ID_JALUR == 4 || $cm.ID_JALUR == 5 || $cm.ID_JALUR == 7 || $cm.ID_JALUR == 20 || $cm.ID_JALUR == 27}
    <tr>
        <td colspan="3"><h3>Data Orang Tua</h3></td>
    </tr>
    <tr>
        <td>20</td>
        <td>Nama Ayah</td>
        <td><input type="text" name="nama_ayah" maxlength="64" class="required" value="{$cm.NAMA_AYAH}"/></td>
    </tr>
    <tr>
        <td>21</td>
        <td>Alamat Ayah</td>
        <td><textarea name="alamat_ayah" cols="50" rows="2" maxlength="200" class="required">{$cm.ALAMAT_AYAH}</textarea><br/>
            <select name="id_kota_ayah" class="required">
                <option value=""></option>
                {foreach $provinsi_set as $p}
                    <optgroup label="{$p.NM_PROVINSI}">
                    {foreach $p.kota_set as $k}<option value="{$k.ID_KOTA}" {if $k.ID_KOTA==$cm.ID_KOTA_AYAH}selected="selected"{/if}>{$k.NM_KOTA}</option>{/foreach}
                    </optgroup>
                {/foreach}
            </select>
            Posisi Alamat : <select name="status_alamat_ayah" class="required">
                <option value=""></option>
                <option value="1" {if $cm.STATUS_ALAMAT_AYAH==1}selected="selected"{/if}>Kota</option>
                <option value="2" {if $cm.STATUS_ALAMAT_AYAH==2}selected="selected"{/if}>Desa</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>22</td>
        <td>Pekerjaan Ayah</td>
        <td>
            <label><input type="radio" name="pekerjaan_ayah" value="1" {if $cm.PEKERJAAN_AYAH==1}checked="checked"{/if} class="required" />Guru/Dosen Negeri</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="2" {if $cm.PEKERJAAN_AYAH==2}checked="checked"{/if}/>PNS Bukan Guru/Dosen</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="3" {if $cm.PEKERJAAN_AYAH==3}checked="checked"{/if}/>TNI / POLRI</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="4" {if $cm.PEKERJAAN_AYAH==4}checked="checked"{/if}/>Guru / Dosen Swasta</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="5" {if $cm.PEKERJAAN_AYAH==5}checked="checked"{/if}/>Karyawan Swasta</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="6" {if $cm.PEKERJAAN_AYAH==6}checked="checked"{/if}/>Pedagang / Wiraswasta</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="7" {if $cm.PEKERJAAN_AYAH==7}checked="checked"{/if}/>Petani / Nelayan</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="8" {if $cm.PEKERJAAN_AYAH==8}checked="checked"{/if}/>Buruh</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="9" {if $cm.PEKERJAAN_AYAH==9}checked="checked"{/if}/>Pensiunan PNS / TNI / POLRI</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="10" {if $cm.PEKERJAAN_AYAH==10}checked="checked"{/if}/>Pensiunan K.Swasta</label><br/>
            <label><input type="radio" name="pekerjaan_ayah" value="11" {if $cm.PEKERJAAN_AYAH==11}checked="checked"{/if}/>Lain-lain : <input type="text" name="pekerjaan_ayah_lain" maxlength="60" value="{$cm.PEKERJAAN_AYAH_LAIN}"/></label>
            <br/>
            <label for="pekerjaan_ayah" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>23</td>
        <td>Pendidikan Ayah</td>
        <td>
            <label><input type="radio" name="pendidikan_ayah" value="1" {if $cm.PENDIDIKAN_AYAH==1}checked="checked"{/if} class="required"/>Tdk. Tamat SD</label>
            <label><input type="radio" name="pendidikan_ayah" value="2" {if $cm.PENDIDIKAN_AYAH==2}checked="checked"{/if}/>SD</label>
            <label><input type="radio" name="pendidikan_ayah" value="3" {if $cm.PENDIDIKAN_AYAH==3}checked="checked"{/if}/>SLTP</label>
            <label><input type="radio" name="pendidikan_ayah" value="4" {if $cm.PENDIDIKAN_AYAH==4}checked="checked"{/if}/>SLTA</label>
            <label><input type="radio" name="pendidikan_ayah" value="5" {if $cm.PENDIDIKAN_AYAH==5}checked="checked"{/if}/>Diploma</label>
            <label><input type="radio" name="pendidikan_ayah" value="6" {if $cm.PENDIDIKAN_AYAH==6}checked="checked"{/if}/>S1</label>
            <label><input type="radio" name="pendidikan_ayah" value="7" {if $cm.PENDIDIKAN_AYAH==7}checked="checked"{/if}/>S2</label>
            <label><input type="radio" name="pendidikan_ayah" value="8" {if $cm.PENDIDIKAN_AYAH==8}checked="checked"{/if}/>S3</label>
            <br/>
            <label for="pendidikan_ayah" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr>
        <td>24</td>
        <td>Nama Ibu </td>
        <td><input type="text" name="nama_ibu" maxlength="64" class="required" value="{$cm.NAMA_IBU}"/></td>
    </tr>
    <tr>
        <td>25</td>
        <td>Alamat Ibu</td>
        <td><textarea name="alamat_ibu" cols="50" rows="2" maxlength="200" class="required">{$cm.ALAMAT_IBU}</textarea><br/>
            <select name="id_kota_ibu" class="required">
                <option value=""></option>
                {foreach $provinsi_set as $p}
                    <optgroup label="{$p.NM_PROVINSI}">
                    {foreach $p.kota_set as $k}<option value="{$k.ID_KOTA}" {if $k.ID_KOTA==$cm.ID_KOTA_IBU}selected="selected"{/if}>{$k.NM_KOTA}</option>{/foreach}
                    </optgroup>
                {/foreach}
            </select>
            Posisi Alamat : <select name="status_alamat_ibu" class="required">
                <option value=""></option>
                <option value="1" {if $cm.STATUS_ALAMAT_IBU==1}selected="selected"{/if}>Kota</option>
                <option value="2" {if $cm.STATUS_ALAMAT_IBU==2}selected="selected"{/if}>Desa</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>26</td>
        <td>Pekerjaan Ibu</td>
        <td>
            <label><input type="radio" name="pekerjaan_ibu" value="1" {if $cm.PEKERJAAN_IBU==1}checked="checked"{/if} class="required"/>Guru/Dosen Negeri</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="2" {if $cm.PEKERJAAN_IBU==2}checked="checked"{/if}/>PNS Bukan Guru/Dosen</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="3" {if $cm.PEKERJAAN_IBU==3}checked="checked"{/if}/>TNI / POLRI</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="4" {if $cm.PEKERJAAN_IBU==4}checked="checked"{/if}/>Guru / Dosen Swasta</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="5" {if $cm.PEKERJAAN_IBU==5}checked="checked"{/if}/>Karyawan Swasta</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="6" {if $cm.PEKERJAAN_IBU==6}checked="checked"{/if}/>Pedagang / Wiraswasta</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="7" {if $cm.PEKERJAAN_IBU==7}checked="checked"{/if}/>Petani / Nelayan</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="8" {if $cm.PEKERJAAN_IBU==8}checked="checked"{/if}/>Buruh</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="9" {if $cm.PEKERJAAN_IBU==9}checked="checked"{/if}/>Pensiunan PNS / TNI / POLRI</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="10" {if $cm.PEKERJAAN_IBU==10}checked="checked"{/if}/>Pensiunan K.Swasta</label><br/>
            <label><input type="radio" name="pekerjaan_ibu" value="11" {if $cm.PEKERJAAN_IBU==11}checked="checked"{/if}/>Lain-lain : <input type="text" name="pekerjaan_ibu_lain" maxlength="60" value="{$cm.PEKERJAAN_IBU_LAIN}" /></label>
            <br/>
            <label for="pekerjaan_ibu" class="error" style="display: none;">Pilih salah satu</label>
        </td>
        {literal}<script language="javascript">
            $('input:radio').click(function() {
                if ($(this).attr('name') == 'pekerjaan_ayah') {
                    if ($(this).attr('value') == 11) {
                        $('input[name="pekerjaan_ayah_lain"]').addClass("required");
                        $('input[name="pekerjaan_ayah_lain"]').focus();
                    }
                    else {
                        $('input[name="pekerjaan_ayah_lain"]').removeClass("required");
                    }
                }

                if ($(this).attr('name') == 'pekerjaan_ibu') {
                    if ($(this).attr('value') == 11) {
                        $('input[name="pekerjaan_ibu_lain"]').addClass("required");
                        $('input[name="pekerjaan_ibu_lain"]').focus();
                    }
                    else {
                        $('input[name="pekerjaan_ibu_lain"]').removeClass("required");
                    }
                }
            });
        </script>{/literal}
    </tr>
    <tr>
        <td>27</td>
        <td>Pendidikan Ibu</td>
        <td>
            <label><input type="radio" name="pendidikan_ibu" value="1" {if $cm.PENDIDIKAN_IBU==1}checked="checked"{/if} class="required"/>Tdk. Tamat SD</label>
            <label><input type="radio" name="pendidikan_ibu" value="2" {if $cm.PENDIDIKAN_IBU==2}checked="checked"{/if}/>SD</label>
            <label><input type="radio" name="pendidikan_ibu" value="3" {if $cm.PENDIDIKAN_IBU==3}checked="checked"{/if}/>SLTP</label>
            <label><input type="radio" name="pendidikan_ibu" value="4" {if $cm.PENDIDIKAN_IBU==4}checked="checked"{/if}/>SLTA</label>
            <label><input type="radio" name="pendidikan_ibu" value="5" {if $cm.PENDIDIKAN_IBU==5}checked="checked"{/if}/>Diploma</label>
            <label><input type="radio" name="pendidikan_ibu" value="6" {if $cm.PENDIDIKAN_IBU==6}checked="checked"{/if}/>S1</label>
            <label><input type="radio" name="pendidikan_ibu" value="7" {if $cm.PENDIDIKAN_IBU==7}checked="checked"{/if}/>S2</label>
            <label><input type="radio" name="pendidikan_ibu" value="8" {if $cm.PENDIDIKAN_IBU==8}checked="checked"{/if}/>S3</label>
            <br/>
            <label for="pendidikan_ibu" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    {/if}
    <tr>
        <td>28</td>
        <td>Penghasilan Orang Tua/Bulan</td>
        <td>
            <label><input type="radio" name="penghasilan_ortu" value="1" {if $cm.PENGHASILAN_ORTU==1}checked="checked"{/if} class="required"/>&gt; 7,5 juta</label>
            <label><input type="radio" name="penghasilan_ortu" value="2" {if $cm.PENGHASILAN_ORTU==2}checked="checked"{/if}/>2,5 - 7,5 juta</label>
            <label><input type="radio" name="penghasilan_ortu" value="3" {if $cm.PENGHASILAN_ORTU==3}checked="checked"{/if}/>1,35 - 2,5 juta</label>
            <label><input type="radio" name="penghasilan_ortu" value="4" {if $cm.PENGHASILAN_ORTU==4}checked="checked"{/if}/>&lt; 1,35 juta</label>
            <br/>
            <label class="error" for="penghasilan_ortu">Pilih Salah satu</label>
        </td>
    </tr>
    <tr>
        <td colspan="3"><h3>Lain-lain</h3></td>
    </tr>
    <tr>
        <td>29</td>
        <td colspan="2">Pernahkan Saudara(i) mempunyai Nomor Induk Mahasiswa / NIM yang berbeda dengan sekarang ?
            <label><input type="radio" name="status_nim_lama" value="1" {if $cm.STATUS_NIM_LAMA==1}checked="checked"{/if} class="required" />Ya</label>
            <label><input type="radio" name="status_nim_lama" value="0" {if $cm.STATUS_NIM_LAMA==0}checked="checked"{/if}/>Tidak</label>
            <label for="status_nim_lama" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr id="nim_lama" style="display: none;">
        <td></td>
        <td>Nomor Induk Mahasiswa / NIM Lama :</td>
        <td><input type="text" name="nim_lama" maxlength="12" value="{$cm.NIM_LAMA}" /></td>
    </tr>
    <tr>
        <td>30</td>
        <td colspan="2">Apakah Saudara(i) pindahan dari Perguruan Tinggi lain ?
            <label><input type="radio" name="status_pt_asal" value="1" class="required" {if $cm.STATUS_PT_ASAL==1}checked="checked"{/if} />Ya</label>
            <label><input type="radio" name="status_pt_asal" value="0" {if $cm.STATUS_PT_ASAL==0}checked="checked"{/if}/>Tidak</label>
            <label for="status_pt_asal" class="error" style="display: none;">Pilih salah satu</label>
        </td>
    </tr>
    <tr id="pt_asal" style="display: none;">
        <td></td>
        <td>Nama Perguruan Tinggi Asal :</td>
        <td><input type="text" name="nama_pt_asal" maxlength="30" value="{$cm.NAMA_PT_ASAL}"/></td>
    </tr>
</table>
<div style="text-align: center">
    <input type="submit" value="Proses" />
</div>
<div>NB:<br/> -Ketika data sudah disubmit, maka tidak bisa diedit lagi.<br/>
-Tombol cetak akan muncul setelah data di submit</div>
{literal}<script type="text/javascript">
$('input:radio').click(function() {
    if ($(this).attr('name') == 'status_nim_lama') {
        if ($(this).attr('value') == 1) {
            $('tr[id="nim_lama"]').css('display', '');
            $('input[name="nim_lama"]').addClass("required");
        }
        else {
            $('tr[id="nim_lama"]').css('display', 'none');
            $('input[name="nim_lama"]').removeClass("required");
        }    
    }
        
    if ($(this).attr('name') == 'status_pt_asal') {
        if ($(this).attr('value') == 1) {
            $('tr[id="pt_asal"]').css('display', '');
            $('input[name="pt_asal"]').addClass("required");
        }
        else {
            $('tr[id="pt_asal"]').css('display', 'none');
            $('input[name="pt_asal"]').removeClass("required");
        }    
    }
        
    if ($(this).attr('name') == 'kewarganegaraan') {
        if ($(this).attr('value') == 2) {
            $('input[name="kewarganegaraan_lain"]').addClass("required");
            $('input[name="kewarganegaraan_lain"]').focus();
        }
        else {
            $('input[name="kewarganegaraan_lain"]').removeClass("required");
        }    
    }
    
    if ($(this).attr('name') == 'sumber_biaya') {
        if ($(this).attr('value') == 3) {
            $('input[name="beasiswa"]').addClass("required");
            $('input[name="sumber_biaya_lain"]').removeClass("required");
            $('input[name="beasiswa"]').focus();
        }
        else if ($(this).attr('value') == 4) {
            $('input[name="beasiswa"]').removeClass("required");
            $('input[name="sumber_biaya_lain"]').addClass("required");
            $('input[name="sumber_biaya_lain"]').focus();
        }
        else {
            $('input[name="beasiswa"]').removeClass("required");
            $('input[name="sumber_biaya_lain"]').removeClass("required");
        }    
    }
        
    if ($(this).attr('name') == 'jurusan_sekolah') {
        if ($(this).attr('value') == 5) {
            $('input[name="jurusan_sekolah_lain"]').addClass("required");
            $('input[name="jurusan_sekolah_lain"]').focus();
        }
        else {
            $('input[name="jurusan_sekolah_lain"]').removeClass("required");
        }
    }
});

</script>{/literal}

{literal}<script type="text/javascript">
$('form').validate();
</script>{/literal}