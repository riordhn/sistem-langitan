<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="http://cybercampus.unair.ac.id/css/maba.css" />
<link rel="SHORTCUT ICON" href="http://cybercampus.unair.ac.id/img/maba/iconunair.ico" />
<title>Registrasi Mahasiswa Baru - Universitas Airlangga</title>

<!-- javascript mahasiswa -->
<script language="javascript" src="http://cybercampus.unair.ac.id/js/jquery-1.5.1.js"></script>
<script language="javascript" src="http://cybercampus.unair.ac.id/js/jquery-history.js"></script>
<script language="javascript" src="http://cybercampus.unair.ac.id/js/jquery.validate.min.js"></script>
<script language="javascript">
    var defaultRel = 'a';
    var defaultPage = '{$halaman}';
</script>
<script language="javascript" src="http://cybercampus.unair.ac.id/js/maba.js"></script>

</head>

<body>
    <!--  -->
<div id="main_container">
  <div id="header"></div>
  <div id="main_content">
    <div id="menu_tab">
    <div class="left_menu_corner"></div>
        <ul class="menu">
            {*
        {foreach $tabs as $tab}
            <li><a class="nav1" rel="{$tab.ID}" href="{$tab.PAGE}">{$tab.TITLE}</a></li>
            <li class="divider"></li>
            {if $tab@last}
            <li><a class="disable-ajax" href="?mode=logout">Logout</a></li>
            {/if}
        {/foreach} *}
        <li><a class="disable-ajax" href="?mode=logout" style="color: #fff">Logout</a></li>
        </ul>
    <div class="right_menu_corner"></div>
  </div>
    
    <div class="crumb_navigation" id="breadcrumbs">
    Navigation: <span class="current">Biodata Mahasiswa</span>
  </div> 
  </div>
      <div class="left_content" >
        <div class="border_box"><img src="http://cybercampus.unair.ac.id/img/maba/photo.jpg" /><br />
            <b>Welcome</b>, mahasiswa<br /><br />
				<!--<?php //include "leftMenu.php";?>-->
				<div id="menu"></div>
                <br />
        </div>
      </div>
      <div class="center_content" id="content" style="min-height: 400px">
        Loading data...
   	  </div>             
  
   <div class="footer">
   

     <div class="left_footer">
        <img src="http://cybercampus.unair.ac.id/img/maba/footer_logo.jpg" />
     </div>
        
     <div class="center_footer">
       Copyright &copy; 2011 - Universitas Airlangga. <br/>All Rights Reserved
     </div>
        
     <div class="right_footer">
     </div>   
   
   </div>  
 </div>

</body>
</html>
