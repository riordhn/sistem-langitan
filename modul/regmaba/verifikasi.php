<?php
include('config.php');

if (!isset($_SESSION['maba'])) { echo "<b>Session Expired</b>"; exit(); }

$smarty->assign('debug', get('debug', 'false'));

$calon_mahasiswa_table = new CALON_MAHASISWA_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);
$jenjang_table = new JENJANG_TABLE($db);
$fakultas_table = new FAKULTAS_TABLE($db);
$kota_table = new KOTA_TABLE($db);
$sekolah_table = new SEKOLAH_TABLE($db);
$provinsi_table = new PROVINSI_TABLE($db);
$detail_biaya_table = new DETAIL_BIAYA_TABLE($db);

if ($request_method == 'POST')
{
    if (post('mode') == 'save')
    {    
        $calon_mahasiswa = $calon_mahasiswa_table->Single($_SESSION['maba']);
        
        $calon_mahasiswa->NAMA_AYAH = post('nama_ayah');
        $calon_mahasiswa->ALAMAT_AYAH = post('alamat_ayah');
        $calon_mahasiswa->ID_KOTA_AYAH = post('id_kota_ayah');
        $calon_mahasiswa->STATUS_ALAMAT_AYAH = post('status_alamat_ayah');
        
        $calon_mahasiswa->NAMA_IBU = post('nama_ibu');
        $calon_mahasiswa->ALAMAT_IBU = post('alamat_ibu');
        $calon_mahasiswa->ID_KOTA_IBU = post('id_kota_ibu');
        $calon_mahasiswa->STATUS_ALAMAT_IBU = post('status_alamat_ibu');
        
        $calon_mahasiswa->ANGGOTA_KELUARGA = post('anggota_keluarga');
        $calon_mahasiswa->NOMOR_KSK = post('nomor_ksk');
        $calon_mahasiswa->SP3 = post('sp3');
        $calon_mahasiswa->SP3_VERIFIKASI = post('sp3');
        $calon_mahasiswa->SP3_LAIN = post('sp3_lain');
        $calon_mahasiswa->PENGHASILAN_ORTU = post('penghasilan_ortu');
        
        $calon_mahasiswa->PEKERJAAN_AYAH = post('pekerjaan_ayah');
        $calon_mahasiswa->PEKERJAAN_AYAH_LAIN = post('pekerjaan_ayah_lain');
        $calon_mahasiswa->PEKERJAAN_IBU = post('pekerjaan_ibu');
        $calon_mahasiswa->PEKERJAAN_IBU_LAIN = post('pekerjaan_ibu_lain');
        
        $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = post('skala_pekerjaan_ortu');
        $calon_mahasiswa->KEDIAMAN_ORTU = post('kediaman_ortu');
        $calon_mahasiswa->LUAS_TANAH = post('luas_tanah');
        $calon_mahasiswa->LUAS_BANGUNAN = post('luas_bangunan');
        $calon_mahasiswa->NJOP = post('njop');
        $calon_mahasiswa->LISTRIK = post('listrik');
        $calon_mahasiswa->KENDARAAN_R4 = post('kendaraan_r4');
        $calon_mahasiswa->KENDARAAN_R2 = post('kendaraan_r2');
        $calon_mahasiswa->KEKAYAAN_LAIN = post('kekayaan_lain');
        $calon_mahasiswa->INFO_LAIN = post('info_lain');
        
        $calon_mahasiswa->STATUS = AUCC_CMHS_ISI_KEUANGAN;
        
        $calon_mahasiswa_table->Update($calon_mahasiswa);
    }
    
    if (post('mode') == 'save_biodata')
    {   
        $calon_mahasiswa = $calon_mahasiswa_table->Single($_SESSION['maba']);
        
        $calon_mahasiswa->NM_C_MHS = post('nm_c_mhs');
        $calon_mahasiswa->ID_KOTA_LAHIR = post('id_kota_lahir');
        $calon_mahasiswa->TGL_LAHIR = date('d-M-Y', mktime(0, 0, 0, post('tgl_lahir_Month'), post('tgl_lahir_Day'), post('tgl_lahir_Year')));
        $calon_mahasiswa->ALAMAT = post('alamat');
        $calon_mahasiswa->TELP = post('telp');
        $calon_mahasiswa->KEWARGANEGARAAN = post('kewarganegaraan');
        $calon_mahasiswa->KEWARGANEGARAAN_LAIN = post('kewarganegaraan_lain');
        $calon_mahasiswa->SUMBER_BIAYA = post('sumber_biaya');
        $calon_mahasiswa->BEASISWA = post('beasiswa');
        $calon_mahasiswa->SUMBER_BIAYA_LAIN = post('sumber_biaya_lain');
        $calon_mahasiswa->ID_AGAMA = post('id_agama');
        $calon_mahasiswa->JUMLAH_KAKAK = post('jumlah_kakak');
        $calon_mahasiswa->JUMLAH_ADIK = post('jumlah_adik');
        
        if ($calon_mahasiswa->ID_JALUR != 4)  // pengecualian alih jenis
        {
            $calon_mahasiswa->ID_SEKOLAH_ASAL = post('id_sekolah_asal');
            $calon_mahasiswa->JURUSAN_SEKOLAH = post('jurusan_sekolah');
            $calon_mahasiswa->JURUSAN_SEKOLAH_LAIN = post('jurusan_sekolah_lain');
            $calon_mahasiswa->NO_IJAZAH = post('no_ijazah');
            $calon_mahasiswa->TGL_IJAZAH = date('d-M-Y', mktime(0, 0, 0, post('tgl_ijazah_Month'), post('tgl_ijazah_Day'), post('tgl_ijazah_Year')));
            $calon_mahasiswa->TAHUN_LULUS = post('tahun_lulus');
            $calon_mahasiswa->JUMLAH_PELAJARAN_IJAZAH = post('jumlah_pelajaran_ijazah');
            $calon_mahasiswa->NILAI_IJAZAH = post('nilai_ijazah');
            $calon_mahasiswa->TAHUN_UAN = post('tahun_uan');
            $calon_mahasiswa->JUMLAH_PELAJARAN_UAN = post('jumlah_pelajaran_uan');
            $calon_mahasiswa->NILAI_UAN = post('nilai_uan');
        }
        
        $calon_mahasiswa->PENDIDIKAN_AYAH = post('pendidikan_ayah');
        $calon_mahasiswa->PENDIDIKAN_IBU = post('pendidikan_ibu');
        
        $calon_mahasiswa->STATUS_NIM_LAMA = post('status_nim_lama');
        $calon_mahasiswa->NIM_LAMA = post('nim_lama');
        $calon_mahasiswa->STATUS_PT_ASAL = post('status_pt_asal');
        $calon_mahasiswa->NAMA_PT_ASAL = post('nama_pt_asal');
        
        $calon_mahasiswa->TGL_ISI = date('d-M-Y');
        
        // jalur PMDK / depag / d3 / internasional
        if ($calon_mahasiswa->ID_JALUR == 20 || $calon_mahasiswa->ID_JALUR == 3 || $calon_mahasiswa->ID_JALUR == 5 || $calon_mahasiswa->ID_JALUR == 27)
        {
            $calon_mahasiswa->JENIS_KELAMIN = post('jenis_kelamin');
            
            $calon_mahasiswa->NAMA_AYAH = post('nama_ayah');
            $calon_mahasiswa->ALAMAT_AYAH = post('alamat_ayah');
            $calon_mahasiswa->ID_KOTA_AYAH = post('id_kota_ayah');
            $calon_mahasiswa->STATUS_ALAMAT_AYAH = post('status_alamat_ayah');

            $calon_mahasiswa->NAMA_IBU = post('nama_ibu');
            $calon_mahasiswa->ALAMAT_IBU = post('alamat_ibu');
            $calon_mahasiswa->ID_KOTA_IBU = post('id_kota_ibu');
            $calon_mahasiswa->STATUS_ALAMAT_IBU = post('status_alamat_ibu');
            
            $calon_mahasiswa->PEKERJAAN_AYAH = post('pekerjaan_ayah');
            $calon_mahasiswa->PEKERJAAN_AYAH_LAIN = post('pekerjaan_ayah_lain');
            $calon_mahasiswa->PEKERJAAN_IBU = post('pekerjaan_ibu');
            $calon_mahasiswa->PEKERJAAN_IBU_LAIN = post('pekerjaan_ibu_lain');
            
            $calon_mahasiswa->PENGHASILAN_ORTU = post('penghasilan_ortu');
            
            $calon_mahasiswa->STATUS = AUCC_CMHS_VER_KEUANGAN;
        }
        
        // hanya untuk alih jenis + d3
        $calon_mahasiswa->STATUS = AUCC_CMHS_VER_KEUANGAN;
        
        // jalur alih jenis
        if ($calon_mahasiswa->ID_JALUR == 4)
        {
            $db->Query('
                UPDATE CALON_MAHASISWA_PASCA SET
                    PTN_S1 = :ptn_s1,
                    STATUS_PTN_S1 = :status_ptn_s1,
                    PRODI_S1 = :prodi_s1,
                    TGL_MASUK_S1 = :tgl_masuk_s1,
                    TGL_LULUS_S1 = :tgl_lulus_s1,
                    LAMA_STUDI_S1 = :lama_studi_s1,
                    IP_S1 = :ip_s1
                WHERE ID_C_MHS = :id_c_mhs');
            $db->BindByName(':id_c_mhs', $_SESSION['maba']);
            $db->BindByName(':ptn_s1', post('ptn_s1'));
            $db->BindByName(':status_ptn_s1', post('status_ptn_s1'));
            $db->BindByName(':prodi_s1', post('prodi_s1'));
            $db->BindByName(':tgl_masuk_s1', date('d-M-Y', mktime(0, 0, 0, post('tgl_masuk_s1_Month'), post('tgl_masuk_s1_Day'), post('tgl_masuk_s1_Year'))));
            $db->BindByName(':tgl_lulus_s1', date('d-M-Y', mktime(0, 0, 0, post('tgl_lulus_s1_Month'), post('tgl_lulus_s1_Day'), post('tgl_lulus_s1_Year'))));
            $db->BindByName(':lama_studi_s1', post('lama_studi_s1'));
            $db->BindByName(':ip_s1', post('ip_s1'));
            $db->Execute();
        }
        
        $calon_mahasiswa_table->Update($calon_mahasiswa);
        
        //$db->Close();
    }
    
    if (post('mode') == 'save_biodata_s2')
    {
        $calon_mahasiswa = $calon_mahasiswa_table->Single($_SESSION['maba']);
        
        $calon_mahasiswa->ID_KOTA_LAHIR = post('id_kota_lahir');
        $calon_mahasiswa->TGL_LAHIR = str_pad(post('tgl_lahir_Day'), 2, "0", STR_PAD_LEFT) . "-" . post('tgl_lahir_Month') . "-" . post('tgl_lahir_Year');
        $calon_mahasiswa->JENIS_KELAMIN = post('jenis_kelamin');
        $calon_mahasiswa->ID_AGAMA = post('id_agama');
        $calon_mahasiswa->STATUS_PERKAWINAN = post('status_perkawinan');
        $calon_mahasiswa->SUMBER_BIAYA = post('sumber_biaya');
        $calon_mahasiswa->BEASISWA = str_replace("'", "''", post('beasiswa'));
        $calon_mahasiswa->PT_INSTANSI = str_replace("'", "''", post('pt_instansi'));
        $calon_mahasiswa->ALAMAT_KANTOR = str_replace("'", "''", post('alamat_kantor'));
        
        $calon_mahasiswa->STATUS = AUCC_CMHS_VER_KEUANGAN;
        
        // bentuk tanggal lahir (baru)
        $calon_mahasiswa->TGL_ISI = date('d-m-Y');
        
        $db->Query("
            UPDATE CALON_MAHASISWA SET
                ID_KOTA_LAHIR = {$calon_mahasiswa->ID_KOTA_LAHIR},
                TGL_LAHIR = TO_DATE('{$calon_mahasiswa->TGL_LAHIR}', 'DD-MM-YYYY'),
                JENIS_KELAMIN = {$calon_mahasiswa->JENIS_KELAMIN},
                ID_AGAMA = {$calon_mahasiswa->ID_AGAMA},
                STATUS_PERKAWINAN = {$calon_mahasiswa->STATUS_PERKAWINAN},
                SUMBER_BIAYA = {$calon_mahasiswa->SUMBER_BIAYA},
                BEASISWA = '{$calon_mahasiswa->BEASISWA}',
                PT_INSTANSI = '{$calon_mahasiswa->PT_INSTANSI}',
                ALAMAT_KANTOR = '{$calon_mahasiswa->ALAMAT_KANTOR}',
                TGL_ISI = TO_DATE('{$calon_mahasiswa->TGL_ISI}', 'DD-MM-YYYY'),
                STATUS = {$calon_mahasiswa->STATUS}
            WHERE ID_C_MHS = {$_SESSION['maba']}");
    }
}

if ($request_method == 'GET' || $request_method == 'POST')
{
    $calon_mahasiswa = $calon_mahasiswa_table->Single($_SESSION['maba']);
    $program_studi_table->FillCalonMahasiswa($calon_mahasiswa);
    $jenjang_table->FillProgramStudi($calon_mahasiswa->PROGRAM_STUDI);
    $fakultas_table->FillProgramStudi($calon_mahasiswa->PROGRAM_STUDI);

    //$smarty->assign('calon_mahasiswa', $calon_mahasiswa);

    $db->Query("
        SELECT 
            CM.ID_C_MHS, CM.ID_JALUR, NM_FAKULTAS, NM_PROGRAM_STUDI, J.ID_JENJANG, NM_JENJANG, NM_C_MHS, NO_UJIAN, ID_KOTA_LAHIR, TGL_LAHIR, ALAMAT, TELP, JENIS_KELAMIN,
            KEWARGANEGARAAN, KEWARGANEGARAAN_LAIN, SUMBER_BIAYA, SUMBER_BIAYA_LAIN, BEASISWA, ID_AGAMA, JUMLAH_KAKAK, JUMLAH_ADIK, ID_SEKOLAH_ASAL,
            CM.JURUSAN_SEKOLAH, JURUSAN_SEKOLAH_LAIN, NO_IJAZAH, TGL_IJAZAH, TAHUN_LULUS, JUMLAH_PELAJARAN_IJAZAH, NILAI_IJAZAH, TAHUN_UAN, JUMLAH_PELAJARAN_UAN, NILAI_UAN,
            NAMA_AYAH, ALAMAT_AYAH, ID_KOTA_AYAH, STATUS_ALAMAT_AYAH, NAMA_IBU, ALAMAT_IBU, ID_KOTA_IBU, STATUS_ALAMAT_IBU,
            PEKERJAAN_AYAH, PEKERJAAN_AYAH_LAIN, PEKERJAAN_IBU, PEKERJAAN_IBU_LAIN, PENDIDIKAN_AYAH, PENDIDIKAN_IBU, STATUS_NIM_LAMA, NIM_LAMA, STATUS_PT_ASAL, NAMA_PT_ASAL,
            PENGHASILAN_ORTU,
            CM.STATUS, KESEHATAN_KESIMPULAN_AKHIR
        FROM CALON_MAHASISWA CM
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI
        LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        WHERE CM.ID_C_MHS = {$_SESSION['maba']}");
    $cm = $db->FetchAssoc();

    $db->Query("
        SELECT * FROM CALON_MAHASISWA_PASCA
        WHERE ID_C_MHS = {$_SESSION['maba']}");
    $cmp = $db->FetchAssoc();

    // parsing date
    $cm['TGL_LAHIR'] = date_to_timestamp($cm['TGL_LAHIR']);

    $smarty->assign('cm', $cm);
    $smarty->assign('cmp', $cmp);

    if ($cm['ID_JENJANG'] == AUCC_JENJANG_S1 || $cm['ID_JENJANG'] == AUCC_JENJANG_D3 || $cm['ID_JENJANG'] == AUCC_JENJANG_PROGRAM)
    {
        if ($cm['ID_JALUR'] != 20 && $cm['ID_JALUR'] != 3)  // mandiri + depag
        {
            /*
            $detail_biaya = $detail_biaya_table->SelectCriteria("
                LEFT JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = DETAIL_BIAYA.ID_BIAYA_KULIAH
                WHERE BK.ID_KELOMPOK_BIAYA = 5 AND DETAIL_BIAYA.ID_BIAYA = 81 AND
                    BK.ID_PROGRAM_STUDI = {$calon_mahasiswa->ID_PROGRAM_STUDI} AND
                    BK.ID_JALUR = {$calon_mahasiswa->ID_JALUR}")->Get(0);

            $smarty->assign('sp3_lain', $detail_biaya->BESAR_BIAYA); **/
        }

        if ($cm['STATUS'] == AUCC_CMHS_LOLOS_SELEKSI)
        {
            $provinsi_set = $provinsi_table->Select();
            $kota_table->FillProvinsiSet($provinsi_set);

            $smarty->assign('provinsi_set', $provinsi_set);
            $smarty->display('step1.tpl');
        }
        else if ($cm['STATUS'] == AUCC_CMHS_ISI_KEUANGAN)
        {
            // mendapatkan provinsi dan kota
            $provinsi_set = $db->QueryToArray("SELECT ID_PROVINSI, NM_PROVINSI FROM PROVINSI");
            for ($i = 0; $i < count($provinsi_set); $i++)
                $provinsi_set[$i]['kota_set'] = $db->QueryToArray("SELECT ID_KOTA, NM_KOTA FROM KOTA WHERE ID_PROVINSI = {$provinsi_set[$i]['ID_PROVINSI']}");
            $smarty->assign('provinsi_set', $provinsi_set);

            // mendapatkan kota dan sekolah
            $sekolah_set = $db->QueryToArray("
                SELECT ID_SEKOLAH, NM_SEKOLAH, K.NM_KOTA FROM SEKOLAH S
                JOIN KOTA K ON K.ID_KOTA = S.ID_KOTA
                ORDER BY K.NM_KOTA, S.NM_SEKOLAH");
            $smarty->assign('sekolah_set', $sekolah_set);

            // mendapatkan kota
            $smarty->assign('kota_set', $db->QueryToArray("SELECT ID_KOTA, NM_KOTA FROM KOTA ORDER BY NM_KOTA"));

            $smarty->display('step2.tpl');
        }
        else if ($cm['STATUS'] >= AUCC_CMHS_ISI_BIODATA || $cm['STATUS'] == 1)
        {
            if (($cm['STATUS'] >= AUCC_CMHS_PRINT_JADWAL || $cm['STATUS'] == 1) && $cm['KESEHATAN_KESIMPULAN_AKHIR'] != '')
            {
                $smarty->assign('hasil_kesehatan', $cm['KESEHATAN_KESIMPULAN_AKHIR']);
            }

            $smarty->display('step3.tpl');
        }
    }

    if ($cm['ID_JENJANG'] == AUCC_JENJANG_S2 || $cm['ID_JENJANG'] == AUCC_JENJANG_S3 || $cm['ID_JENJANG'] == AUCC_JENJANG_PROFESI || $cm['ID_JENJANG'] == AUCC_JENJANG_SPESIALIS)
    {
        if ($cm['STATUS'] == AUCC_CMHS_ISI_KEUANGAN)
        {
            $provinsi_set = $provinsi_table->Select();
            $kota_table->FillProvinsiSet($provinsi_set);
            $smarty->assign('provinsi_set', $provinsi_set);
            $smarty->display('step2_s2.tpl');
        }
        else if ($cm['STATUS'] >= AUCC_CMHS_VER_KEUANGAN)
        {
            $smarty->display('step3.tpl');
        }
    }
}
?>
