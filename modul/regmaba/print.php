<?php
include('config.php');
include_once('../../tcpdf/config/lang/ind.php');
include_once('../../tcpdf/tcpdf.php');

if (!isset($_SESSION['maba'])) { echo "<b>Anda harus login terlebih dahulu.</b>"; exit(); }

$calon_mahasiswa_table = new CALON_MAHASISWA_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);
$fakultas_table = new FAKULTAS_TABLE($db);
$jenjang_table = new JENJANG_TABLE($db);
$kota_table = new KOTA_TABLE($db);
$agama_table = new AGAMA_TABLE($db);
$sekolah_table = new SEKOLAH_TABLE($db);
$provinsi_table = new PROVINSI_TABLE($db);

$calon_mahasiswa = $calon_mahasiswa_table->Single($_SESSION['maba']);
$program_studi_table->FillCalonMahasiswa($calon_mahasiswa);
$fakultas_table->FillProgramStudi($calon_mahasiswa->PROGRAM_STUDI);
$jenjang_table->FillProgramStudi($calon_mahasiswa->PROGRAM_STUDI);
$kota_table->FillCalonMahasiswa($calon_mahasiswa);
$agama_table->FillCalonMahasiswa($calon_mahasiswa);

// TCPDF Configuration
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// Pengecekan terhadap mahasiswa S1 & D3
if ($calon_mahasiswa->PROGRAM_STUDI->ID_JENJANG == AUCC_JENJANG_S1 ||
    $calon_mahasiswa->PROGRAM_STUDI->ID_JENJANG == AUCC_JENJANG_D4 ||
    $calon_mahasiswa->PROGRAM_STUDI->ID_JENJANG == AUCC_JENJANG_D3 ||
    $calon_mahasiswa->PROGRAM_STUDI->ID_JENJANG == AUCC_JENJANG_D2 ||
    $calon_mahasiswa->PROGRAM_STUDI->ID_JENJANG == AUCC_JENJANG_D1 ||
    $calon_mahasiswa->PROGRAM_STUDI->ID_JENJANG == AUCC_JENJANG_PROGRAM) {

    // Join khusus S1 & D3
    $sekolah_table->FillCalonMahasiswa($calon_mahasiswa);
    $kota_table->FillCalonMahasiswa2($calon_mahasiswa); //ayah
    $kota_table->FillCalonMahasiswa3($calon_mahasiswa); //ibu
    $provinsi_table->FillKota($calon_mahasiswa->KOTA2);
    $provinsi_table->FillKota($calon_mahasiswa->KOTA3);

    // Replacement
    if ($calon_mahasiswa->SP3 == 5) { $calon_mahasiswa->SP3 = "SP3b"; }
    else if ($calon_mahasiswa->SP3 == 4) { $calon_mahasiswa->SP3 = "SP3a"; }
    else if ($calon_mahasiswa->SP3 == 3) { $calon_mahasiswa->SP3 = "BEBAS SP3"; }
    else if ($calon_mahasiswa->SP3 == 2) { $calon_mahasiswa->SP3 = "GAKIN"; }

    if ($calon_mahasiswa->PENGHASILAN_ORTU == 1) { $calon_mahasiswa->PENGHASILAN_ORTU = "> Rp 7,5 jt"; }
    else if ($calon_mahasiswa->PENGHASILAN_ORTU == 2) { $calon_mahasiswa->PENGHASILAN_ORTU = "Rp 2,5 - Rp 7,5 jt"; }
    else if ($calon_mahasiswa->PENGHASILAN_ORTU == 3) { $calon_mahasiswa->PENGHASILAN_ORTU = "Rp 1,35 - Rp 2,5 jt"; }
    else if ($calon_mahasiswa->PENGHASILAN_ORTU == 4) { $calon_mahasiswa->PENGHASILAN_ORTU = "< Rp 1,35 jt"; }

    if ($calon_mahasiswa->SKALA_PEKERJAAN_ORTU == 1) { $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = "Besar"; }
    else if ($calon_mahasiswa->SKALA_PEKERJAAN_ORTU == 2) { $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = "Menengah"; }
    else if ($calon_mahasiswa->SKALA_PEKERJAAN_ORTU == 3) { $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = "Kecil"; }
    else if ($calon_mahasiswa->SKALA_PEKERJAAN_ORTU == 4) { $calon_mahasiswa->SKALA_PEKERJAAN_ORTU = "Mikro"; }

    if ($calon_mahasiswa->KEDIAMAN_ORTU == 1) { $calon_mahasiswa->KEDIAMAN_ORTU = "Mewah"; }
    else if ($calon_mahasiswa->KEDIAMAN_ORTU == 2) { $calon_mahasiswa->KEDIAMAN_ORTU = "Sedang"; }
    else if ($calon_mahasiswa->KEDIAMAN_ORTU == 3) { $calon_mahasiswa->KEDIAMAN_ORTU = "Rumah Sederhana"; }
    else if ($calon_mahasiswa->KEDIAMAN_ORTU == 4) { $calon_mahasiswa->KEDIAMAN_ORTU = "Rumah Sangat Sederhana"; }

    if ($calon_mahasiswa->LUAS_TANAH == 1) { $calon_mahasiswa->LUAS_TANAH = "> 200 m2"; }
    else if ($calon_mahasiswa->LUAS_TANAH == 2) { $calon_mahasiswa->LUAS_TANAH = "100 - 200 m2"; }
    else if ($calon_mahasiswa->LUAS_TANAH == 3) { $calon_mahasiswa->LUAS_TANAH = "45 - 100 m2"; }
    else if ($calon_mahasiswa->LUAS_TANAH == 4) { $calon_mahasiswa->LUAS_TANAH = "< 45 m2"; }

    if ($calon_mahasiswa->LUAS_BANGUNAN == 1) { $calon_mahasiswa->LUAS_BANGUNAN = "> 100 m2"; }
    else if ($calon_mahasiswa->LUAS_BANGUNAN == 2) { $calon_mahasiswa->LUAS_BANGUNAN = "56 - 100 m2"; }
    else if ($calon_mahasiswa->LUAS_BANGUNAN == 3) { $calon_mahasiswa->LUAS_BANGUNAN = "27 - 56 m2"; }
    else if ($calon_mahasiswa->LUAS_BANGUNAN == 4) { $calon_mahasiswa->LUAS_BANGUNAN = "< 27 m2"; }

    if ($calon_mahasiswa->NJOP == 1) { $calon_mahasiswa->NJOP = "> Rp 300 jt"; }
    else if ($calon_mahasiswa->NJOP == 2) { $calon_mahasiswa->NJOP = "Rp 100 - Rp 300 jt"; }
    else if ($calon_mahasiswa->NJOP == 3) { $calon_mahasiswa->NJOP = "Rp 50 - Rp 100 jt"; }
    else if ($calon_mahasiswa->NJOP == 4) { $calon_mahasiswa->NJOP = "< Rp 50 jt"; }

    if ($calon_mahasiswa->KENDARAAN_R4 == 1) { $calon_mahasiswa->KENDARAAN_R4 = "> 1"; }
    else if ($calon_mahasiswa->KENDARAAN_R4 == 2) { $calon_mahasiswa->KENDARAAN_R4 = "1"; }
    else if ($calon_mahasiswa->KENDARAAN_R4 == 3) { $calon_mahasiswa->KENDARAAN_R4 = "Tidak Punya"; }

    if ($calon_mahasiswa->KENDARAAN_R2 == 1) { $calon_mahasiswa->KENDARAAN_R2 = "> 2"; }
    else if ($calon_mahasiswa->KENDARAAN_R2 == 2) { $calon_mahasiswa->KENDARAAN_R2 = "2"; }
    else if ($calon_mahasiswa->KENDARAAN_R2 == 3) { $calon_mahasiswa->KENDARAAN_R2 = "1"; }
    else if ($calon_mahasiswa->KENDARAAN_R2 == 4) { $calon_mahasiswa->KENDARAAN_R2 = "Tidak Punya"; }

    if ($calon_mahasiswa->LISTRIK == 1) { $calon_mahasiswa->LISTRIK = "450 VA"; }
    else if ($calon_mahasiswa->LISTRIK == 2) { $calon_mahasiswa->LISTRIK = "900 VA"; }
    else if ($calon_mahasiswa->LISTRIK == 3) { $calon_mahasiswa->LISTRIK = "1300 VA"; }
    else if ($calon_mahasiswa->LISTRIK == 4) { $calon_mahasiswa->LISTRIK = ">= 2200 VA"; }

    if ($calon_mahasiswa->KEWARGANEGARAAN == 1) { $calon_mahasiswa->KEWARGANEGARAAN = "Indonesia"; }
    else if ($calon_mahasiswa->KEWARGANEGARAAN == 2) { $calon_mahasiswa->KEWARGANEGARAAN = $calon_mahasiswa->KEWARGANEGARAAN_LAIN; }

    if ($calon_mahasiswa->SUMBER_BIAYA == 1) { $calon_mahasiswa->SUMBER_BIAYA = "Orang Tua"; }
    else if ($calon_mahasiswa->SUMBER_BIAYA == 2) { $calon_mahasiswa->SUMBER_BIAYA = "Orang Tua Asuh"; }
    else if ($calon_mahasiswa->SUMBER_BIAYA == 3) { $calon_mahasiswa->SUMBER_BIAYA = $calon_mahasiswa->BEASISWA; }
    else if ($calon_mahasiswa->SUMBER_BIAYA == 4) { $calon_mahasiswa->SUMBER_BIAYA = $calon_mahasiswa->SUMBER_BIAYA_LAIN; }

    if ($calon_mahasiswa->JURUSAN_SEKOLAH == 1) { $calon_mahasiswa->JURUSAN_SEKOLAH = "SMU / MA IPA"; }
    else if ($calon_mahasiswa->JURUSAN_SEKOLAH == 2) { $calon_mahasiswa->JURUSAN_SEKOLAH = "SMU / MA IPS"; }
    else if ($calon_mahasiswa->JURUSAN_SEKOLAH == 3) { $calon_mahasiswa->JURUSAN_SEKOLAH = "SMU / MA BAHASA"; }
    else if ($calon_mahasiswa->JURUSAN_SEKOLAH == 4) { $calon_mahasiswa->JURUSAN_SEKOLAH = "SMK"; }
    else if ($calon_mahasiswa->JURUSAN_SEKOLAH == 5) { $calon_mahasiswa->JURUSAN_SEKOLAH = $calon_mahasiswa->JURUSAN_SEKOLAH_LAIN; }

    if ($calon_mahasiswa->PENDIDIKAN_AYAH == 1) { $calon_mahasiswa->PENDIDIKAN_AYAH = "Tidak Tamat SD"; }
    else if ($calon_mahasiswa->PENDIDIKAN_AYAH == 2) { $calon_mahasiswa->PENDIDIKAN_AYAH = "Tamat SD"; }
    else if ($calon_mahasiswa->PENDIDIKAN_AYAH == 3) { $calon_mahasiswa->PENDIDIKAN_AYAH = "SLTP"; }
    else if ($calon_mahasiswa->PENDIDIKAN_AYAH == 4) { $calon_mahasiswa->PENDIDIKAN_AYAH = "SLTA"; }
    else if ($calon_mahasiswa->PENDIDIKAN_AYAH == 5) { $calon_mahasiswa->PENDIDIKAN_AYAH = "Diploma"; }
    else if ($calon_mahasiswa->PENDIDIKAN_AYAH == 6) { $calon_mahasiswa->PENDIDIKAN_AYAH = "S1"; }
    else if ($calon_mahasiswa->PENDIDIKAN_AYAH == 7) { $calon_mahasiswa->PENDIDIKAN_AYAH = "S2"; }
    else if ($calon_mahasiswa->PENDIDIKAN_AYAH == 8) { $calon_mahasiswa->PENDIDIKAN_AYAH = "S3"; }

    if ($calon_mahasiswa->PENDIDIKAN_IBU == 1) { $calon_mahasiswa->PENDIDIKAN_IBU = "Tidak Tamat SD"; }
    else if ($calon_mahasiswa->PENDIDIKAN_IBU == 2) { $calon_mahasiswa->PENDIDIKAN_IBU = "Tamat SD"; }
    else if ($calon_mahasiswa->PENDIDIKAN_IBU == 3) { $calon_mahasiswa->PENDIDIKAN_IBU = "SLTP"; }
    else if ($calon_mahasiswa->PENDIDIKAN_IBU == 4) { $calon_mahasiswa->PENDIDIKAN_IBU = "SLTA"; }
    else if ($calon_mahasiswa->PENDIDIKAN_IBU == 5) { $calon_mahasiswa->PENDIDIKAN_IBU = "Diploma"; }
    else if ($calon_mahasiswa->PENDIDIKAN_IBU == 6) { $calon_mahasiswa->PENDIDIKAN_IBU = "S1"; }
    else if ($calon_mahasiswa->PENDIDIKAN_IBU == 7) { $calon_mahasiswa->PENDIDIKAN_IBU = "S2"; }
    else if ($calon_mahasiswa->PENDIDIKAN_IBU == 8) { $calon_mahasiswa->PENDIDIKAN_IBU = "S3"; }

    if ($calon_mahasiswa->PEKERJAAN_AYAH == 1) { $calon_mahasiswa->PEKERJAAN_AYAH = "Guru/Dosen"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 2) { $calon_mahasiswa->PEKERJAAN_AYAH = "PNS Bukan Guru/Dosen"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 3) { $calon_mahasiswa->PEKERJAAN_AYAH = "TNI / POLRI"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 4) { $calon_mahasiswa->PEKERJAAN_AYAH = "Guru / Dosen Swasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 5) { $calon_mahasiswa->PEKERJAAN_AYAH = "Karyawan Swasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 6) { $calon_mahasiswa->PEKERJAAN_AYAH = "Pedagang / Wiraswasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 7) { $calon_mahasiswa->PEKERJAAN_AYAH = "Petani / Nelayan"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 8) { $calon_mahasiswa->PEKERJAAN_AYAH = "Buruh"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 9) { $calon_mahasiswa->PEKERJAAN_AYAH = "Pensiunan PNS / TNI / POLRI"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 10) { $calon_mahasiswa->PEKERJAAN_AYAH = "Pensiunan K. Swasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_AYAH == 11) { $calon_mahasiswa->PEKERJAAN_AYAH = $calon_mahasiswa->PEKERJAAN_AYAH_LAIN; }

    if ($calon_mahasiswa->PEKERJAAN_IBU == 1) { $calon_mahasiswa->PEKERJAAN_IBU = "Guru/Dosen"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 2) { $calon_mahasiswa->PEKERJAAN_IBU = "PNS Bukan Guru/Dosen"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 3) { $calon_mahasiswa->PEKERJAAN_IBU = "TNI / POLRI"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 4) { $calon_mahasiswa->PEKERJAAN_IBU = "Guru / Dosen Swasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 5) { $calon_mahasiswa->PEKERJAAN_IBU = "Karyawan Swasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 6) { $calon_mahasiswa->PEKERJAAN_IBU = "Pedagang / Wiraswasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 7) { $calon_mahasiswa->PEKERJAAN_IBU = "Petani / Nelayan"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 8) { $calon_mahasiswa->PEKERJAAN_IBU = "Buruh"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 9) { $calon_mahasiswa->PEKERJAAN_IBU = "Pensiunan PNS / TNI / POLRI"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 10) { $calon_mahasiswa->PEKERJAAN_IBU = "Pensiunan K. Swasta"; }
    else if ($calon_mahasiswa->PEKERJAAN_IBU == 11) { $calon_mahasiswa->PEKERJAAN_IBU = $calon_mahasiswa->PEKERJAAN_IBU_LAIN; }

    // INFO JADWAL VERIFIKASI
    $html = <<<EOF
<h4>TERIMA KASIH {nama}, TELAH MELAPORKAN DIRI</h4>
<h4>JADWAL VALIDASI BERKAS ANDA : {tgl_jadwal_verifikasi}</h4>
<h4>TEMPAT : AUDITORIUM KAMPUS C, UNIVERSITAS AIRLANGGA</h4>
<h4>BERKAS YANG HARUS DIBAWA : </h4>
<ol>
    <li>FORMULIR VERIFIKASI ADMINISTRASI</li>
    <li>FORMULIR REGISTRASI MAHASISWA BARU (2 FORM)</li>
    <li>SURAT PERNYATAAN BEBAS NAPZA</li>
    <li>SURAT PERNYATAAN BERSEDIA MEMENUHI DAN MENTAATI PERATURAN</li>
    <li>Berkas Penunjang :
        <ul>
            <li>Keterangan Penghasilan dan Pekerjaan serta Jabatan bagi Pegawai</li>
            <li>Akta/SIUP bagi Wiraswasta/Pengusaha (Copy)</li>
            <li>Keterangan luas lahan bagi petani</li>
            <li>SPPT PBB tahun terakhir (copy)</li>
            <li>Rekening Listrik bulan terakhir (copy)</li>
            <li>STNK kendaraan R4 atau Kendaraan R2</li>
            <li>Kartu Keluarga</li>
        </ul>
    </li>
</ol>
EOF;
        
    $html = str_replace('{nama}', $calon_mahasiswa->NM_C_MHS, $html);
    $html = str_replace('{tgl_jadwal_verifikasi}', strftime('%d %B %Y', date_create($calon_mahasiswa->TGL_JADWAL_VERIFIKASI)->getTimestamp()), $html);

    $pdf->AddPage();
    $pdf->writeHTML($html);
    $pdf->endPage();
    
    // FORM KEUANGAN + FORM KEUANGAN KOSONG
    $html = <<<EOF
<style>
    td { font-size: 26px; }
 </style>
<table border="1" cellpadding="3">
    <tr>
        <td align="center" style="background-color: #008" colspan="3"><h3 style="color: yellow">FORMULIR VERIFIKASI ADMINISTRASI</h3></td>
    </tr>
    <tr><td colspan="3"></td></tr>
    <tr>
        <td align="center" style="background-color: #008" colspan="3"><h4 style="color: yellow">IDENTITAS DIRI</h4></td>
    </tr>
    <tr>
        <td style="width: 20px; text-align: center;">1</td>
        <td style="width: 160px;">Nama CAMABA</td>
        <td style="width: auto">{nama}</td>
    </tr>
    <tr>
        <td align="center">2</td>
        <td>Nomor Ujian Tulis SNMPTN</td>
        <td>{no_ujian}</td>
    </tr>
    <tr>
        <td align="center">3</td>
        <td>Program Studi (Diterima)</td>
        <td>{program_studi}</td>
    </tr>
    <tr>
        <td align="center">4</td>
        <td>Nama Ayah</td>
        <td>{nama_ayah}</td>
    </tr>
    <tr>
        <td align="center">5</td>
        <td>Alamat Ayah</td>
        <td>{alamat_ayah}</td>
    </tr>
    <tr>
        <td align="center">6</td>
        <td>Nama Ibu</td>
        <td>{nama_ibu}</td>
    </tr>
    <tr>
        <td align="center">7</td>
        <td>Alamat Ibu</td>
        <td>{alamat_ibu}</td>
    </tr>
    <tr>
        <td align="center">8</td>
        <td>Jumlah Anggota Keluarga</td>
        <td>{jumlah_anggota_keluarga} orang, No KSK : {nomor_ksk}</td>
    </tr>
    <tr>
        <td align="center">9</td>
        <td>Sanggup Membayar SP3</td>
        <td>{sp3}</td>
    </tr>
    <tr>
        <td align="center">10</td>
        <td>Penghasilan Orang Tua / Bulan</td>
        <td>{penghasilan_ortu}</td>
    </tr>
    <tr>
        <td align="center" style="background-color: #008" colspan="3" width="auto"><h4 style="color: yellow">PEKERJAAN / PROFESI / JABATAN ORANG TUA</h4></td>
    </tr>
    <tr>
        <td style="width: 20px; text-align: center;">11</td>
        <td style="width: 160px;">Pekerjaan Ayah</td>
        <td style="width: auto">{pekerjaan_ayah}</td>
    </tr>
    <tr>
        <td style="text-align: center;">12</td>
        <td>Pekerjaan Ibu</td>
        <td>{pekerjaan_ibu}</td>
    </tr>
    <tr>
        <td align="center">13</td>
        <td>Skala Usaha / Pekerjaan</td>
        <td>{skala_pekerjaan_ortu}</td>
    </tr>
    <tr>
        <td align="center" style="background-color: #008" colspan="3" width="auto"><h4 style="color: yellow">KEDIAMAN DAN KENDARAAN KELUARGA</h4></td>
    </tr>
    <tr>
        <td style="width: 20px; text-align: center;">14</td>
        <td style="width: 160px;">Kediaman Orang Tua</td>
        <td style="width: auto">{kediaman_ortu}</td>
    </tr>
    <tr>
        <td align="center">15</td>
        <td>Luas Tanah Kediaman</td>
        <td>{luas_tanah}</td>
    </tr>
    <tr>
        <td align="center">16</td>
        <td>Luas Bangunan Kediaman</td>
        <td>{luas_bangunan}</td>
    </tr>
    <tr>
        <td align="center">17</td>
        <td>NJOP</td>
        <td>{njop}</td>
    </tr>
    <tr>
        <td align="center">18</td>
        <td>Kendaraan Bermotor R4</td>
        <td>{kendaraan_r4}</td>
    </tr>
    <tr>
        <td align="center">19</td>
        <td>Kendaraan Bermotor R2</td>
        <td>{kendaraan_r2}</td>
    </tr>
    <tr>
        <td align="center">20</td>
        <td>Daya Listrik (VA)</td>
        <td>{listrik}</td>
    </tr>
    <tr>
        <td align="center">21</td>
        <td>Kekayaan Lainya :</td>
        <td>{kekayaan_lain}</td>
    </tr>
    <tr>
        <td align="center">22</td>
        <td>Informasi Lainnya, sebutkan :</td>
        <td>{info_lain}</td>
    </tr>
    <tr>
        <td align="center" style="background-color: #008" colspan="3" width="auto"><h4 style="color: yellow">PERNYATAAN</h4></td>
    </tr>
    <tr>
        <td align="center">
        <font color="#f00"><b>Data dan informasi ini disampaikan dengan sesungguhnya dan sebenar-benarnya. Apabila ternyata data dan informasi
        ini tidak benar dan tidak lengkap, saya bersedia kehilangan dan atau melepaskan hak saya sebagai mahasiswa Universitas Airlangga
        selama 2 tahun pada semua program studi melalui program seleksi apapun.</b></font>
        <br/><br/><br/><br/>
        <table>
            <tr>
                <td>&nbsp;</td>
                <td align="center">{tgl_cetak}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center">Mengetahui,</td>
                <td align="center">Yang Menyatakan,</td>
            </tr>
            <tr>
                <td align="center">Orang Tua / Wali</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><font size="8px">(materai)</font></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center">_____________________</td>
                <td align="center">{nama}</td>
            </tr>
        </table>
        <br/>
        </td>
    </tr>
</table>
EOF;

    $html = str_replace('{nama}', $calon_mahasiswa->NM_C_MHS, $html);
    $html = str_replace('{no_ujian}', $calon_mahasiswa->NO_UJIAN, $html);
    $html = str_replace('{program_studi}', $calon_mahasiswa->PROGRAM_STUDI->NM_PROGRAM_STUDI, $html);
    $html = str_replace('{nama_ayah}', $calon_mahasiswa->NAMA_AYAH, $html);
    $html = str_replace('{alamat_ayah}', $calon_mahasiswa->ALAMAT_AYAH, $html);
    $html = str_replace('{nama_ibu}', $calon_mahasiswa->NAMA_IBU, $html);
    $html = str_replace('{alamat_ibu}', $calon_mahasiswa->ALAMAT_IBU, $html);
    $html = str_replace('{jumlah_anggota_keluarga}', $calon_mahasiswa->ANGGOTA_KELUARGA, $html);
    $html = str_replace('{nomor_ksk}', $calon_mahasiswa->NOMOR_KSK, $html);
    $html = str_replace('{sp3}', $calon_mahasiswa->SP3, $html);
    $html = str_replace('{penghasilan_ortu}', $calon_mahasiswa->PENGHASILAN_ORTU, $html);

    $html = str_replace('{pekerjaan_ayah}', $calon_mahasiswa->PEKERJAAN_AYAH, $html);
    $html = str_replace('{pekerjaan_ibu}', $calon_mahasiswa->PEKERJAAN_IBU, $html);

    $html = str_replace('{skala_pekerjaan_ortu}', $calon_mahasiswa->SKALA_PEKERJAAN_ORTU, $html);
    $html = str_replace('{kediaman_ortu}', $calon_mahasiswa->KEDIAMAN_ORTU, $html);
    $html = str_replace('{luas_tanah}', $calon_mahasiswa->LUAS_TANAH, $html);
    $html = str_replace('{luas_bangunan}', $calon_mahasiswa->LUAS_BANGUNAN, $html);
    $html = str_replace('{njop}', $calon_mahasiswa->NJOP, $html);
    $html = str_replace('{kendaraan_r4}', $calon_mahasiswa->KENDARAAN_R4, $html);
    $html = str_replace('{kendaraan_r2}', $calon_mahasiswa->KENDARAAN_R2, $html);
    $html = str_replace('{listrik}', $calon_mahasiswa->LISTRIK, $html);
    $html = str_replace('{kekayaan_lain}', $calon_mahasiswa->KEKAYAAN_LAIN, $html);
    $html = str_replace('{info_lain}', $calon_mahasiswa->INFO_LAIN, $html);
    $html = str_replace('{tgl_cetak}', date('d F Y'), $html);

    $pdf->AddPage();
    $pdf->writeHTML($html);
    $pdf->endPage();

    // FORM VERIFIKASI ADMINISTRASI
    $html = <<<EOF
<style>
    td { font-size: 26px; }
 </style>
<table border="1" cellpadding="3">
    <tr>
        <td align="center" style="background-color: #008" colspan="4"><h3 style="color: yellow">FORMULIR VERIFIKASI ADMINISTRASI</h3></td>
    </tr>
    <tr><td colspan="4"></td></tr>
    <tr>
        <td align="center" style="background-color: #008" colspan="4"><h4 style="color: yellow">IDENTITAS DIRI</h4></td>
    </tr>
    <tr>
        <td width="20" align="center">1</td>
        <td style="width: 160px;">Nama CAMABA</td>
        <td colspan="4" style="width: auto"></td>
    </tr>
    <tr>
        <td align="center">2</td>
        <td>Nomor Ujian Tulis SNMPTN</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">3</td>
        <td>Program Studi (Diterima)</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">4</td>
        <td>Nama Ayah</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">5</td>
        <td>Alamat Ayah</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">6</td>
        <td>Nama Ibu</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">7</td>
        <td>Alamat Ibu</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">8</td>
        <td>Jumlah Anggota Keluarga</td>
        <td width="150">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Orang</td>
        <td  width="100">No. KSK</td>
        <td style="width: auto"></td>
    </tr>
    <tr>
        <td align="center">9</td>
        <td>Sanggup Membayar SP3</td>
        <td colspan="4" style="width: auto">
            <table width="100%" border="0">
              <tr>
                <td>a. SP3b</td>
                <td>b. SP3a</td>
                <td>c. BEBAS SP3</td>
                <td>d. GAKIN</td>
              </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="center">10</td>
        <td>Penghasilan Orang Tua / Bulan</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; 7.5 Jt</td>
                <td>b. 2.5 - 7.5 Jt</td>
                <td>c. 1.35 - 2.5 Jt</td>
                <td>d. &lt; 1.35 Jt</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
      <td align="center" style="background-color: #008;width: auto;" colspan="4"><h4 style="color: yellow">PEKERJAAN / PROFESI / JABATAN ORANG TUA</h4></td>
    </tr>
    <tr>
        <td style="width: 20px; text-align: center;">11</td>
        <td style="width: 160px;">Pekerjaan Ayah / Ibu</td>
        <td colspan="4" style="width: auto">
            <table width="100%" border="0">
              <tr>
                <td>a. Guru/Dosen Negeri<br/>
b. PNS Bukan Guru/Dosen<br/>
c. TNI / POLRI<br/>
d. Guru / Dosen Swasta<br/>
e. Karyawan Swasta<br/>
f. Pedagang / Wiraswasta<br/>
g. Petani / Nelayan<br/>
h. Buruh<br/>
i. Pensiunan PNS / TNI / POLRI<br/>
j. Pensiunan K.Swasta<br/>
k. Lain-lain :  </td>
                <td>a. Guru/Dosen Negeri<br/>
b. PNS Bukan Guru/Dosen<br/>
c. TNI / POLRI<br/>
d. Guru / Dosen Swasta<br/>
e. Karyawan Swasta<br/>
f. Pedagang / Wiraswasta<br/>
g. Petani / Nelayan<br/>
h. Buruh<br/>
i. Pensiunan PNS / TNI / POLRI<br/>
j. Pensiunan K.Swasta<br/>
k. Lain-lain : </td>
              </tr>
            </table>
        </td>
    </tr>
     <tr>
        <td align="center">12</td>
        <td>Skala Usaha / Pekerjaan</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. Besar</td>
                <td>b. Menengah</td>
                <td>c. Kecil</td>
                <td>d. Mikro</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
      <td align="center" style="background-color: #008;width: auto;" colspan="4"><h4 style="color: yellow">KEDIAMAN DAN KENDARAAN KELUARGA</h4></td>
    </tr>
    <tr>
        <td style="width: 20px; text-align: center;">13</td>
        <td style="width: 160px;">Kediaman Orang Tua</td>
        <td colspan="4" style="width: auto">
            <table width="100%" border="0">
              <tr>
                <td>a. Mewah / Besar</td>
                <td>b. Sedang</td>
                <td>c. RS</td>
                <td>d. RSS</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">14</td>
        <td>Luas Tanah Kediaman</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; 200 m2</td>
                <td>b. 100 - 200 m2</td>
                <td>c. 45 - 100 m2</td>
                <td>d. &lt; 45 m2</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">15</td>
        <td>Luas Bangunan Kediaman</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; 100 m2</td>
                <td>b. 56 - 100 m2</td>
                <td>c. 27 - 56 m2</td>
                <td>d. &lt; 27 m2</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">16</td>
        <td>NJOP</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; 300 Jt</td>
                <td>b. 100 - 300 Jt</td>
                <td>c. 50 - 100 Jt</td>
                <td>d. &lt; 50 Jt</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
      <td align="center">17</td>
      <td>Daya Listrik Kediaman</td>
      <td colspan="4">
          <table width="100%" border="0">
            <tr>
              <td>a. &gt; 2.200 VA</td>
              <td>b. 1.300 VA</td>
              <td>c. 900 VA</td>
              <td>d. &lt; 450 VA</td>
            </tr>
          </table>
      </td>
    </tr>
    <tr>
        <td align="center">18</td>
        <td>Kendaraan Bermotor R4</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; 1</td>
                <td>b. 1</td>
                <td>c. Tidak Punya</td>
                <td>d. Tidak Punya</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">19</td>
        <td>Kendaraan Bermotor R2</td>
        <td colspan="4">
            <table width="100%" border="0">
              <tr>
                <td>a. &gt; 2</td>
                <td>b. 2</td>
                <td>c. 1</td>
                <td>d. Tidak Punya</td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">20</td>
        <td>Daya Listrik (VA)</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">21</td>
        <td>Kekayaan Lainya :</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td align="center">22</td>
        <td>Informasi Lainnya, sebutkan :</td>
        <td colspan="4"></td>
    </tr>
    <tr>
      <td align="center" style="background-color: #008;width: auto;" colspan="4"><h4 style="color: yellow">PERNYATAAN</h4></td>
    </tr>
    <tr>
        <td align="center">
        <font color="#f00"><b>Data dan informasi ini disampaikan dengan sesungguhnya dan sebenar-benarnya. Apabila ternyata data dan informasi
        ini tidak benar dan tidak lengkap, saya bersedia kehilangan dan atau melepaskan hak saya sebagai mahasiswa Universitas Airlangga
        selama 2 tahun pada semua program studi melalui program seleksi apapun.</b></font>
        <br/><br/>
        <table>
            <tr>
                <td>&nbsp;</td>
                <td align="center">...................</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center">Mengetahui,</td>
                <td align="center">Yang Menyatakan,</td>
            </tr>
            <tr>
                <td align="center">Orang Tua / Wali</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><font size="8px">(materai)</font></td>
            </tr>
            <tr>
                <td align="center">_____________________</td>
                <td align="center">_____________________</td>
            </tr>
        </table>
        <br/>
        </td>
    </tr>
</table>
EOF;
    
    $pdf->addPage();
    $pdf->writeHTML($html);
    $pdf->endPage();

    // FORMULIR REGISTRASI MAHASISWA BARU
    $html = <<<EOF
<style>
    p { font-size: 28px;}
    td { font-size: 30px; }
	.isi{border:1px solid; border-collapse:collapse;}
	.headers{margin:20px; height:100;}
	#headers{margin:20px; height:100;}
</style>
<table width="100%" border="0" class="headers">
    <tr>
        <td width="14%"><br/><br/><br/><img src="../../img/maba/logounair.png" width="75px" height="75px" /></td>
        <td width="68%" align="center">
            <p>&nbsp;</p>
            <p><font size="14"><b>FORMULIR REGISTRASI MAHASISWA BARU <br>
            TAHUN AKADEMIK 2011 / 2012</b></font></p>
            <p>&nbsp;</p>
        </td>
        <td width="18%">
            <table width="100%" border="1" class="isi" id="headers">
              <tr>
                <td align="center"><p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>4 x 6 cm</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                </td>
              </tr>
            </table>
        </td>
    </tr>
</table>
<hr/>
<p>YANG BERTANDA TANGAN DI BAWAH INI MOHON DIDAFTAR SEBAGAI MAHASISWA PADA : </p>
<table width="100%" cellpadding="3" border="1">
    <tr>
        <td align="center" width="30px">1</td>
        <td width="160px">Fakultas</td>
        <td width="auto">{fakultas}</td>
    </tr>
    <tr>
        <td align="center">2</td>
        <td>Program Studi</td>
        <td>{program_studi}</td>
    </tr>
    <tr>
        <td align="center">3</td>
        <td>Jenjang Studi</td>
        <td>{jenjang}</td>
    </tr>
</table>
<div>&nbsp;</div>
<table border="1" cellpadding="3">
    <tr>
        <td align="center" width="30px">4</td>
        <td width="160px">Nama Lengkap</td>
        <td width="auto">{nama}</td>
    </tr>
    <tr>
        <td align="center">5</td>
        <td>No Induk Mhs. NIM</td>
        <td></td>
    </tr>
    <tr>
        <td align="center">6</td>
        <td>No Peserta</td>
        <td>{no_ujian}</td>
    </tr>
    <tr>
        <td align="center">7</td>
        <td>Tempat dan Tgl Lahir</td>
        <td>{tempat_lahir}, {tgl_lahir}</td>
    </tr>
    <tr>
        <td align="center">8</td>
        <td>Alamat Mahasiswa</td>
        <td>{alamat}</td>
    </tr>
    <tr>
        <td align="center"></td>
        <td>No Telp Mahasiswa</td>
        <td>{telp}</td>
    </tr>
    <tr>
        <td align="center">9</td>
        <td>Jenis Kelamin</td>
        <td>{jenis_kelamin}</td>
    </tr>
    <tr>
        <td align="center">10</td>
        <td>Kewarganegaraan</td>
        <td>{kewarganegaraan}</td>
    </tr>
    <tr>
        <td align="center">11</td>
        <td>Sumber Biaya</td>
        <td>{sumber_biaya}</td>
    </tr>
    <tr>
        <td align="center">12</td>
        <td>Agama</td>
        <td>{agama}</td>
    </tr>
    <tr>
        <td align="center">13</td>
        <td>Jumlah Kakak dan Adik</td>
        <td>Kakak : {jumlah_kakak} &nbsp;&nbsp;&nbsp; Adik : {jumlah_adik}</td>
    </tr>
    <tr>
        <td align="center">14</td>
        <td>Asal SMTA / MA<br/>Alamat SMTA / MA</td>
        <td>{asal_sekolah}</td>
    </tr>
    <tr>
        <td align="center">15</td>
        <td>Jurusan SMTA / MA</td>
        <td>{jurusan_sekolah}</td>
    </tr>
    <tr>
        <td align="center">16</td>
        <td>Tgl. No. Ijazah SMTA</td>
        <td>Tanggal : {tgl_ijazah} &nbsp;&nbsp;&nbsp; No Ijazah : {no_ijazah}</td>
    </tr>
    <tr>
        <td align="center">17</td>
        <td>Ijazah</td>
        <td>Tahun : {tahun_lulus}<br/>
            Jumlah Mata Pelajaran : {jumlah_pelajaran_ijazah}<br/>
            Jumlah Nilai Ijazah : {nilai_ijazah}
        </td>
    </tr>
    <tr>
        <td align="center">18</td>
        <td>UAN</td>
        <td>Tahun : {tahun_uan}<br/>
            Jumlah Mata Pelajaran : {jumlah_pelajaran_uan}<br/>
            Jumlah Nilai Ijazah : {nilai_uan}
        </td>
    </tr>
    <tr>
        <td align="center">19</td>
        <td>Nama dan Alamat Ayah</td>
        <td>Nama : {nama_ayah}<br/>
            Alamat : {alamat_ayah}<br/>
            Kabupaten / Kodya : {kota_ayah}<br/>
            Provinsi : {provinsi_ayah}<br/>
            Telp : {telp_ayah}
        </td>
    </tr>
    <tr>
        <td align="center">19</td>
        <td>Nama dan Alamat Ibu</td>
        <td>Nama : {nama_ibu}<br/>
            Alamat : {alamat_ibu}<br/>
            Kabupaten / Kodya : {kota_ibu}<br/>
            Provinsi : {provinsi_ibu}<br/>
            Telp : {telp_ibu}
        </td>
    </tr>
    <tr>
        <td align="center">20</td>
        <td>Pendidikan Ayah</td>
        <td>{pendidikan_ayah}</td>
    </tr>
    <tr>
        <td align="center">20</td>
        <td>Pendidikan Ibu</td>
        <td>{pendidikan_ibu}</td>
    </tr>
    <tr>
        <td align="center">21</td>
        <td>Pekerjaan Ayah</td>
        <td>{pekerjaan_ayah}</td>
    </tr>
    <tr>
        <td align="center">22</td>
        <td>Pekerjaan Ibu</td>
        <td>{pekerjaan_ibu}</td>
    </tr>
    <tr>
        <td align="center">23</td>
        <td>Besarnya penghasilan orang tua / wali sebulan</td>
        <td>{penghasilan_ortu}</td>
    </tr>
</table>
<br/><br/><br/>
<table border="1" cellpadding="3">
    <tr>
        <td width="30px">24</td>
        <td width="auto">Pernahkah saudara mempunyai Nomor Induk Mahasiswa / NIM yang berbeda dengan sekarang ?<br/>{status_nim_lama}</td>
    </tr>
    <tr>
        <td width="30px">25</td>
        <td width="auto">Jika Ya Nyatakan NIM :<br/>{nim_lama}</td>
    </tr>
    <tr>
        <td width="30px">26</td>
        <td width="auto">Apakah saudara pindahan dari Perguruan Tinggi lain ?<br/>{status_pt_asal}<br/>Jika YA Nyatakan :{nama_pt_asal}</td>
    </tr>
</table>
<br/>
<table>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">Surabaya, {tgl_cetak}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">Tanda tangan yang bersangkutan,</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center"><u>{nama}</u></td>
    </tr>
</table>
EOF;

    $html = str_replace('{fakultas}', $calon_mahasiswa->PROGRAM_STUDI->FAKULTAS->NM_FAKULTAS, $html);
    $html = str_replace('{program_studi}', $calon_mahasiswa->PROGRAM_STUDI->NM_PROGRAM_STUDI, $html);
    $html = str_replace('{jenjang}', $calon_mahasiswa->PROGRAM_STUDI->JENJANG->NM_JENJANG, $html);
    $html = str_replace('{nama}', $calon_mahasiswa->NM_C_MHS, $html);
    $html = str_replace('{no_ujian}', $calon_mahasiswa->NO_UJIAN, $html);
    $html = str_replace('{tempat_lahir}', $calon_mahasiswa->KOTA->NM_KOTA, $html);
    $html = str_replace('{tgl_lahir}', $calon_mahasiswa->TGL_LAHIR, $html);
    $html = str_replace('{alamat}', $calon_mahasiswa->ALAMAT, $html);
    $html = str_replace('{telp}', $calon_mahasiswa->TELP, $html);
    $html = str_replace('{jenis_kelamin}', $calon_mahasiswa->JENIS_KELAMIN == 1 ? "Laki-Laki" : "Perempuan", $html);
    $html = str_replace('{kewarganegaraan}', $calon_mahasiswa->KEWARGANEGARAAN, $html);
    $html = str_replace('{sumber_biaya}', $calon_mahasiswa->SUMBER_BIAYA, $html);
    $html = str_replace('{agama}', $calon_mahasiswa->AGAMA->NM_AGAMA, $html);
    $html = str_replace('{jumlah_kakak}', $calon_mahasiswa->JUMLAH_KAKAK, $html);
    $html = str_replace('{jumlah_adik}', $calon_mahasiswa->JUMLAH_ADIK, $html);
    $html = str_replace('{asal_sekolah}', $calon_mahasiswa->SEKOLAH->NM_SEKOLAH, $html);
    $html = str_replace('{jurusan_sekolah}', $calon_mahasiswa->JURUSAN_SEKOLAH, $html);

    $html = str_replace('{tgl_ijazah}', $calon_mahasiswa->TGL_IJAZAH, $html);
    $html = str_replace('{no_ijazah}', $calon_mahasiswa->NO_IJAZAH, $html);
    $html = str_replace('{tahun_lulus}', $calon_mahasiswa->TAHUN_LULUS, $html);
    $html = str_replace('{jumlah_pelajaran_ijazah}', $calon_mahasiswa->JUMLAH_PELAJARAN_IJAZAH, $html);
    $html = str_replace('{nilai_ijazah}', $calon_mahasiswa->NILAI_IJAZAH, $html);
    $html = str_replace('{tahun_uan}', $calon_mahasiswa->TAHUN_UAN, $html);
    $html = str_replace('{jumlah_pelajaran_uan}', $calon_mahasiswa->JUMLAH_PELAJARAN_UAN, $html);
    $html = str_replace('{nilai_uan}', $calon_mahasiswa->NILAI_UAN, $html);

    $html = str_replace('{nama_ayah}', $calon_mahasiswa->NAMA_AYAH, $html);
    $html = str_replace('{alamat_ayah}', $calon_mahasiswa->ALAMAT_AYAH, $html);
    $html = str_replace('{kota_ayah}', $calon_mahasiswa->KOTA2->NM_KOTA, $html);
    $html = str_replace('{provinsi_ayah}', $calon_mahasiswa->KOTA2->PROVINSI->NM_PROVINSI, $html);
    $html = str_replace('{telp_ayah}', $calon_mahasiswa->TELP_AYAH, $html);

    $html = str_replace('{nama_ibu}', $calon_mahasiswa->NAMA_IBU, $html);
    $html = str_replace('{alamat_ibu}', $calon_mahasiswa->ALAMAT_IBU, $html);
    $html = str_replace('{kota_ibu}', $calon_mahasiswa->KOTA3->NM_KOTA, $html);
    $html = str_replace('{provinsi_ibu}', $calon_mahasiswa->KOTA3->PROVINSI->NM_PROVINSI, $html);
    $html = str_replace('{telp_ibu}', $calon_mahasiswa->TELP_IBU, $html);

    $html = str_replace('{pendidikan_ayah}', $calon_mahasiswa->PENDIDIKAN_AYAH, $html);
    $html = str_replace('{pendidikan_ibu}', $calon_mahasiswa->PENDIDIKAN_IBU, $html);
    $html = str_replace('{pekerjaan_ayah}', $calon_mahasiswa->PEKERJAAN_AYAH, $html);
    $html = str_replace('{pekerjaan_ibu}', $calon_mahasiswa->PEKERJAAN_IBU, $html);
    $html = str_replace('{penghasilan_ortu}', $calon_mahasiswa->PENGHASILAN_ORTU, $html);

    $html = str_replace('{status_nim_lama}', $calon_mahasiswa->STATUS_NIM_LAMA == 1 ? "Ya" : "Tidak", $html);
    $html = str_replace('{nim_lama}', $calon_mahasiswa->NIM_LAMA, $html);
    $html = str_replace('{status_pt_asal}', $calon_mahasiswa->STATUS_PT_ASAL == 1 ? "Ya" : "Tidak", $html);
    $html = str_replace('{nama_pt_asal}', $calon_mahasiswa->NAMA_PT_ASAL, $html);

    $html = str_replace('{tgl_cetak}', date('d F Y'), $html);

    $pdf->AddPage();
    $pdf->writeHTML($html);
    $pdf->endPage();

    
    $html = <<<EOF
<style>
    * { font-size: 30px; }
</style>
<table width="100%" border="0" cellpadding="5">
  <tr>
    <td colspan="2" align="center"><b><font size="14">SURAT - PERNYATAAN</font></b></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">Yang bertanda tangan dibawah ini, Saya : </td>
  </tr>
  <tr>
    <td width="30%">Nama</td>
    <td width="60%">: {nama}</td>
  </tr>
  <tr>
    <td>Tempat dan tanggal lahir</td>
    <td>: {tempat_lahir}, {tgl_lahir}</td>
  </tr>
  <tr>
    <td>Alamat Surabaya</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp. ................................</td>
  </tr>
  <tr>
    <td>Alamat Asal</td>
    <td>: {alamat}</td>
  </tr>
  <tr>
    <td>Asal SMTA</td>
    <td>: {asal_sekolah}</td>
  </tr>
  <tr>
    <td>Alamat SMTA</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>Nama Orang Tua / Wali</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>Pekerjaan</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>Jabatan</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>Alamat Rumah</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp.  ................................</td>
  </tr>
</table>

<br />
Dengan ini menyatakan bahwa : 
<p>Saya tidak pernah, sedang atau akan terlibat dalam penyalahgunaan <b>Narkotika, Alkohol, Psikotropikadan Zat Adiktif (NAPZA)</b> baik sebagai pengguna, pengedar,
produsen atau yang berkaitan dengan hal tersebut. Apabila ternyata di kemudian hari pada saat saya menuntut ilmu di Universitas Airlangga saya terlibat dan atau
terbukti terlibat dalam penyalahgunaan NAPZA sebagaimana dimaksud di atas, maka saya sanggup dan bersedia dikenakan sanksi sampai dengan dibatalkan status saya
sebagai mahasiswa Universitas Airlangga.</p>
<p>
	Demikian surat pernyataan ini saya buat dengan sebenarnya, tanpa adanya paksaan dari pihak siapapun.
</p>
<p>&nbsp;</p>
<table>
    <tr>
        <td width="33%">
            Mengetahui :<br/>
            Orang Tua / Wali<br/><br/><br/><br/><br/><br/><br/>
            _________________________
        </td>
        <td width="33%" align="center">
            <table border="1" width="65%">
                <tr>
                    <td><br/><br/><br/><br/>
                        Pas Foto <br/>3 x 4
                        <br/><br/><br/><br/></td>
                </tr>
            </table>
        </td>
        <td width="33%">Surabaya, .............................<br/>Hormat Saya,
            <br/><br/><br/><font size="8">Materai</font><br/><font size="8">Rp 6.000,-</font><br/><br/><br/>
            <u>{nama}</u>
        </td>
    </tr>
</table>
EOF;

    $html = str_replace('{nama}', $calon_mahasiswa->NM_C_MHS, $html);
    $html = str_replace('{tempat_lahir}', $calon_mahasiswa->KOTA->NM_KOTA, $html);
    $html = str_replace('{tgl_lahir}', $calon_mahasiswa->TGL_LAHIR, $html);
    $html = str_replace('{alamat}', $calon_mahasiswa->ALAMAT, $html);
    $html = str_replace('{asal_sekolah}', $calon_mahasiswa->SEKOLAH->NM_SEKOLAH, $html);

    $pdf->AddPage();
    $pdf->writeHTML($html);
    $pdf->endPage();
    
    
    $html = <<<EOF
<style>
    * { font-size: 30px; }
</style>
<table width="100%" border="0" cellpadding="5">
  <tr>
    <td colspan="2" align="center"><b><font size="14">SURAT PERNYATAAN BERSEDIA MEMENUHI DAN MENTAATI KETENTUAN DAN PERATURAN SERTA KEPUTUSAN YANG BERLAKU</font><br/><font size="14">DI UNIVERSITAS AIRLANGGA</font></b></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">Yang bertanda tangan dibawah ini, Saya : </td>
  </tr>
  <tr>
    <td width="30%">Nama</td>
    <td width="60%">: {nama}</td>
  </tr>
  <tr>
    <td>Tempat dan tanggal lahir</td>
    <td>: {tempat_lahir}, {tgl_lahir}</td>
  </tr>
  <tr>
    <td>Alamat Surabaya</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp. ................................</td>
  </tr>
  <tr>
    <td>Alamat Asal</td>
    <td>: {alamat}</td>
  </tr>
  <tr>
    <td>Asal SMTA</td>
    <td>: {asal_sekolah}</td>
  </tr>
  <tr>
    <td>Alamat SMTA</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>Nama Orang Tua / Wali</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>Pekerjaan</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>Jabatan</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>Alamat Rumah</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp.  ................................</td>
  </tr>
</table>

<br />
<p><b>Dengan ini menyatakan bahwa sebagai mahasiswa Universitas Airlangga, saya bersedia memenuhi dan mentaati ketentuan dan peraturan serta keputusan yang
berlaku di Universitas Airlangga dan apabila saya melanggar saya bersedia dikenakan sangsi.</b></p>
<p><b>Demikian surat pernyataan ini saya buat dengan sebenarnya, tanpa adanya paksaan dari pihak siapapun.</b></p>
<p>&nbsp;</p>
<table>
    <tr>
        <td width="33%">
            Mengetahui :<br/>
            Orang Tua / Wali<br/><br/><br/><br/><br/><br/><br/>
            _________________________
        </td>
        <td width="33%" align="center">
            <table border="1" width="65%">
                <tr>
                    <td><br/><br/><br/><br/>
                        Pas Foto <br/>3 x 4
                        <br/><br/><br/><br/></td>
                </tr>
            </table>
        </td>
        <td width="33%">Surabaya, .............................<br/>Hormat Saya,
            <br/><br/><br/><font size="8">Materai</font><br/><font size="8">Rp 6.000,-</font><br/><br/><br/>
            <u>{nama}</u>
        </td>
    </tr>
</table>
EOF;
    
    $html = str_replace('{nama}', $calon_mahasiswa->NM_C_MHS, $html);
    $html = str_replace('{tempat_lahir}', $calon_mahasiswa->KOTA->NM_KOTA, $html);
    $html = str_replace('{tgl_lahir}', $calon_mahasiswa->TGL_LAHIR, $html);
    $html = str_replace('{alamat}', $calon_mahasiswa->ALAMAT, $html);
    $html = str_replace('{asal_sekolah}', $calon_mahasiswa->SEKOLAH->NM_SEKOLAH, $html);

    $pdf->AddPage();
    $pdf->writeHTML($html);
    $pdf->endPage();

} else if ($calon_mahasiswa->PROGRAM_STUDI->ID_JENJANG == AUCC_JENJANG_S2 ||
    $calon_mahasiswa->PROGRAM_STUDI->ID_JENJANG == AUCC_JENJANG_S3 ||
    $calon_mahasiswa->PROGRAM_STUDI->ID_JENJANG == AUCC_JENJANG_PROFESI ||
    $calon_mahasiswa->PROGRAM_STUDI->ID_JENJANG == AUCC_JENJANG_SPESIALIS) {
    
    // Formulir registrasi
    $html = <<<EOF
<style>
    p { font-size: 28px;}
    td { font-size: 30px; }
	.isi{border:1px solid; border-collapse:collapse;}
	.headers{margin:20px; height:100;}
	#headers{margin:20px; height:100;}
</style>
<table width="100%" border="0" class="headers">
    <tr>
        <td width="14%"><br/><br/><br/><img src="../../img/maba/logounair.png" width="75px" height="75px" /></td>
        <td width="68%" align="center">
            <p>&nbsp;</p>
            <p><font size="14"><b>FORMULIR REGISTRASI MAHASISWA BARU <br>
            TAHUN AKADEMIK 2011 / 2012</b></font></p>
            <p>&nbsp;</p>
        </td>
        <td width="18%">
            <table width="100%" border="1" class="isi" id="headers">
              <tr>
                <td align="center"><p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>4 x 6 cm</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                </td>
              </tr>
            </table>
        </td>
    </tr>
</table>
<hr/>
<p>YANG BERTANDA TANGAN DI BAWAH INI MOHON DIDAFTAR SEBAGAI MAHASISWA PADA : </p>
<table width="100%" cellpadding="3" border="1">
    <tr>
        <td align="center" width="30px">1</td>
        <td width="160px">Nama Universitas</td>
        <td width="auto"><b>UNIVERSITAS AIRLANGGA</b></td>
    </tr>
    <tr>
        <td align="center" width="30px">2</td>
        <td width="160px">Fakultas</td>
        <td width="auto">{fakultas}</td>
    </tr>
    <tr>
        <td align="center">3</td>
        <td>Program Studi</td>
        <td>{program_studi}</td>
    </tr>
    <tr>
        <td align="center">4</td>
        <td>Jenjang Studi</td>
        <td>{jenjang}</td>
    </tr>
</table>
<div>&nbsp;</div>
<table width="100%" cellpadding="3" border="1">
    <tr>
        <td align="center" width="30px">5</td>
        <td width="160px">No Test</td>
        <td width="auto">{no_ujian}</td>
    </tr>
    <tr>
        <td align="center">6</td>
        <td>Nama Lengkap</td>
        <td>{nm_c_mhs}</td>
    </tr>
    <tr>
        <td align="center">7</td>
        <td>Tempat dan Tanggal Lahir</td>
        <td>{tempat_lahir}, {tgl_lahir}</td>
    </tr>
    <tr>
        <td align="center">8</td>
        <td>Jenis Kelamin</td>
        <td>{jenis_kelamin}</td>
    </tr>
    <tr>
        <td align="center">9</td>
        <td>Agama</td>
        <td>{agama}</td>
    </tr>
    <tr>
        <td align="center">10</td>
        <td>Status Perkawinan</td>
        <td>{status_perkawinan}</td>
    </tr>
    <tr>
        <td align="center">11</td>
        <td>Biaya pendidikan</td>
        <td>{sumber_biaya}</td>
    </tr>
    <tr>
        <td align="center">12</td>
        <td>PT. Asal / Instansi Pengirim</td>
        <td>{pt_instansi}</td>
    </tr>
    <tr>
        <td align="center">13</td>
        <td>Alamat Kantor</td>
        <td>{alamat_kantor}</td>
    </tr>
</table>
<br/>
<table>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">Surabaya, {tgl_cetak}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">Tanda tangan yang bersangkutan,</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center"><u>{nm_c_mhs}</u></td>
    </tr>
</table>
EOF;
    
    if ($calon_mahasiswa->STATUS_PERKAWINAN == 1) { $calon_mahasiswa->STATUS_PERKAWINAN = "Belum Kawin"; }
    else if ($calon_mahasiswa->STATUS_PERKAWINAN == 2) { $calon_mahasiswa->STATUS_PERKAWINAN = "Kawin"; }
    else if ($calon_mahasiswa->STATUS_PERKAWINAN == 3) { $calon_mahasiswa->STATUS_PERKAWINAN = "Bercerai"; }
    else if ($calon_mahasiswa->STATUS_PERKAWINAN == 4) { $calon_mahasiswa->STATUS_PERKAWINAN = "Kawin, Suami/Istri meninggal"; }
    
    if ($calon_mahasiswa->SUMBER_BIAYA == 5) { $calon_mahasiswa->SUMBER_BIAYA = "Biaya Sendiri"; }
    else if ($calon_mahasiswa->SUMBER_BIAYA == 3) { $calon_mahasiswa->SUMBER_BIAYA = $calon_mahasiswa->BEASISWA; }
    
    $html = str_replace('{fakultas}', $calon_mahasiswa->PROGRAM_STUDI->FAKULTAS->NM_FAKULTAS, $html);
    $html = str_replace('{program_studi}', $calon_mahasiswa->PROGRAM_STUDI->NM_PROGRAM_STUDI, $html);
    $html = str_replace('{jenjang}', $calon_mahasiswa->PROGRAM_STUDI->JENJANG->NM_JENJANG, $html);
    
    $html = str_replace('{no_ujian}', $calon_mahasiswa->NO_UJIAN, $html);
    $html = str_replace('{nm_c_mhs}', $calon_mahasiswa->NM_C_MHS, $html);
    
    $html = str_replace('{tempat_lahir}', $calon_mahasiswa->KOTA->NM_KOTA, $html);
    
    // tanggal lahir pembetulan
    $tgl_lahir_parse = date_parse($calon_mahasiswa->TGL_LAHIR);
    if ($tgl_lahir_parse['year'] > date('Y')) $tgl_lahir_parse['year'] -= 100;
    $html = str_replace('{tgl_lahir}', strftime('%d %B %Y', mktime(0, 0, 0, $tgl_lahir_parse['month'], $tgl_lahir_parse['day'], $tgl_lahir_parse['year'])), $html);
    
    $html = str_replace('{jenis_kelamin}', $calon_mahasiswa->JENIS_KELAMIN == 1 ? "Laki-Laki" : "Perempuan", $html);
    $html = str_replace('{agama}', $calon_mahasiswa->AGAMA->NM_AGAMA, $html);
    $html = str_replace('{status_perkawinan}', $calon_mahasiswa->STATUS_PERKAWINAN, $html);
    $html = str_replace('{sumber_biaya}', $calon_mahasiswa->SUMBER_BIAYA, $html);
    
    $html = str_replace('{pt_instansi}', $calon_mahasiswa->PT_INSTANSI, $html);
    $html = str_replace('{alamat_kantor}', $calon_mahasiswa->ALAMAT_KANTOR, $html);
    
    $html = str_replace('{tgl_cetak}', strftime('%d %B %Y'), $html);
    
    $pdf->AddPage();
    $pdf->writeHTML($html);
    $pdf->endPage();
    
    // Pernyataan
    $html = <<<EOF
<style>
    * { font-size: 30px; }
</style>
<table width="100%" border="0" cellpadding="5">
  <tr>
    <td colspan="2" align="center"><b><font size="14">SURAT - PERNYATAAN</font></b></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">Yang bertanda tangan dibawah ini, Saya : </td>
  </tr>
  <tr>
    <td width="30%">Nama</td>
    <td width="60%">: {nama}</td>
  </tr>
  <tr>
    <td>Tempat dan tanggal lahir</td>
    <td>: {tempat_lahir}, {tgl_lahir}</td>
  </tr>
  <tr>
    <td>Alamat Surabaya</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp. ................................</td>
  </tr>
  <tr>
    <td>Alamat Asal</td>
    <td>: {alamat}</td>
  </tr>
  <tr>
    <td>Pekerjaan</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>Instansi Asal</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>Alamat Instansi</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp.  ................................</td>
  </tr>
</table>

<br />
Dengan ini menyatakan bahwa : 
<ol>
   <li>Saya
  tidak pernah, sedang atau akan terlibat dalam penyalahgunaan Narkotika, Alkohol, Psikotropikadan Zat Adiktif (NAPZA) baik sebagai pengguna, pengedar, produsen atau yang berkaitan dengan hal tersebut. Apabila ternyata di kemudian hari pada saat saya menuntut ilmu di Universitas Airlangga saya terlibat dan atau terbukti terlibat dalam penyalahgunaan NAPZA sebagaimana dimaksud di atas, maka saya sanggup dan bersedia dikenakan sanksi sampai dengan dibatalkan status saya sebagai mahasiswa Universitas Airlangga.
  </li>
   <li>Bersedia mentaati segala peraturan dan keputusan yang berlaku di lingkungan Universitas Airlangga.</li>
</ol>
<p>
	Demikian surat pernyataan ini saya buat dengan sebenarnya, tanpa adanya paksaan dari pihak siapapun.
</p>
<p>&nbsp;</p>
<table>
    <tr>
        <td width="33%"></td>
        <td width="33%" align="center">
            <table border="1" width="65%">
                <tr>
                    <td><br/><br/><br/><br/>
                        Pas Foto <br/>3 x 4
                        <br/><br/><br/><br/></td>
                </tr>
            </table>
        </td>
        <td width="33%">Surabaya, .............................<br/>Hormat Saya,
            <br/><br/><br/><font size="8">Materai</font><br/><font size="8">Rp 6.000,-</font><br/><br/><br/>
            <u>{nama}</u>
        </td>
    </tr>
</table>
EOF;
    
    $html = str_replace('{nama}', $calon_mahasiswa->NM_C_MHS, $html);
    $html = str_replace('{tempat_lahir}', $calon_mahasiswa->KOTA->NM_KOTA, $html);
    $html = str_replace('{tgl_lahir}', strftime('%d %B %Y', mktime(0, 0, 0, $tgl_lahir_parse['month'], $tgl_lahir_parse['day'], $tgl_lahir_parse['year'])), $html);
    
    $html = str_replace('{alamat}', $calon_mahasiswa->ALAMAT, $html);
    
    $pdf->AddPage();
    $pdf->writeHTML($html);
    $pdf->endPage();
    
    // Pernyataan ke dua
    $html = <<<EOF
<style>
    * { font-size: 30px; }
</style>
<table width="100%" border="0" cellpadding="5">
  <tr>
    <td colspan="2" align="center"><b><font size="14">SURAT PERNYATAAN BERSEDIA MEMENUHI DAN MENTAATI KETENTUAN DAN PERATURAN SERTA KEPUTUSAN YANG BERLAKU</font><br/><font size="14">DI UNIVERSITAS AIRLANGGA</font></b></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">Yang bertanda tangan dibawah ini, Saya : </td>
  </tr>
  <tr>
    <td width="30%">Nama</td>
    <td width="60%">: {nama}</td>
  </tr>
  <tr>
    <td>Tempat dan tanggal lahir</td>
    <td>: {tempat_lahir}, {tgl_lahir}</td>
  </tr>
  <tr>
    <td>Alamat Surabaya</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp. ................................</td>
  </tr>
  <tr>
    <td>Alamat Asal</td>
    <td>: {alamat}</td>
  </tr>
  <tr>
    <td>Pekerjaan</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>Instansi Asal</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>Alamat Instansi</td>
    <td>: .........................................................................</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;Telp.  ................................</td>
  </tr>
</table>

<br />
<p><b>Dengan ini menyatakan bahwa sebagai mahasiswa Universitas Airlangga, saya bersedia memenuhi dan mentaati ketentuan dan peraturan serta keputusan yang
berlaku di Universitas Airlangga dan apabila saya melanggar saya bersedia dikenakan sangsi.</b></p>
<p><b>Demikian surat pernyataan ini saya buat dengan sebenarnya, tanpa adanya paksaan dari pihak siapapun.</b></p>
<p>&nbsp;</p>
<table>
    <tr>
        <td width="33%"></td>
        <td width="33%" align="center">
            <table border="1" width="65%">
                <tr>
                    <td><br/><br/><br/><br/>
                        Pas Foto <br/>3 x 4
                        <br/><br/><br/><br/></td>
                </tr>
            </table>
        </td>
        <td width="33%">Surabaya, .............................<br/>Hormat Saya,
            <br/><br/><br/><font size="8">Materai</font><br/><font size="8">Rp 6.000,-</font><br/><br/><br/>
            <u>{nama}</u>
        </td>
    </tr>
</table>
EOF;
    
    $html = str_replace('{nama}', $calon_mahasiswa->NM_C_MHS, $html);
    $html = str_replace('{tempat_lahir}', $calon_mahasiswa->KOTA->NM_KOTA, $html);
    $html = str_replace('{tgl_lahir}', strftime('%d %B %Y', mktime(0, 0, 0, $tgl_lahir_parse['month'], $tgl_lahir_parse['day'], $tgl_lahir_parse['year'])), $html);
    
    $html = str_replace('{alamat}', $calon_mahasiswa->ALAMAT, $html);
    
    $pdf->AddPage();
    $pdf->writeHTML($html);
    $pdf->endPage();
}

$pdf->Output();
?>