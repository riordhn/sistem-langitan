<?php

include '../../../config.php';
include '../../registrasi/class/CalonMahasiswa.class.php';
include '../funtion-tampil-informasi.php';

$smarty->setTemplateDir('../gui');
$smarty->setCompileDir('../gui_c');

$CalonMahasiswa = new CalonMahasiswa($db);

$mode = get('mode');

if ($request_method == 'POST')
{
	if (post('mode') == 'login')
	{

		// Cleansing nomer ujian
		$no_ujian = str_replace("-", "", trim(post('no_ujian')));

		// if (trim(post('no_ujian')) == '' ||)
		if ($no_ujian == '' || post('tgl_lahir_Day') == '' || post('tgl_lahir_Month') == '' || post('tgl_lahir_Year') == '')
		{
			$smarty->assign('error', alert_success('Pastikan isian nomer peserta dan tanggal lahir anda sudah benar.'));
		}
		else
		{
			$id_c_mhs = $CalonMahasiswa->LoginRegmabaSNMPTN($no_ujian, post('tgl_lahir_Year') . "-" . post('tgl_lahir_Month') . "-" . str_pad(post('tgl_lahir_Day'), 2, '0', STR_PAD_LEFT));

			if (!empty($id_c_mhs))
			{
				if ($id_c_mhs[0]['ID_C_MHS'] != '' and $id_c_mhs[0]['TGL_VERIFIKASI_PENDIDIKAN'] == '')
				{
					//if($id_c_mhs[0]['TGL_AWAL_REGMABA'] <= date('Y-m-d H:i')){
					$CalonMahasiswa->LogRegmaba($_SERVER['REMOTE_ADDR'], $id_c_mhs);
					$_SESSION['ID_C_MHS'] = $id_c_mhs[0]['ID_C_MHS'];
					//}else{
					//	echo "<script>alert('Regmaba dilaksanakan pada tanggal ".$id_c_mhs[0]['TGL_AWAL_REGMABA']."');<//script>";
					//}
				}
				elseif ($id_c_mhs[0]['ID_C_MHS'] != '' and $id_c_mhs[0]['TGL_VERIFIKASI_PENDIDIKAN'] != '')
				{
					$_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] = $id_c_mhs[0]['TGL_VERIFIKASI_PENDIDIKAN'];
					$_SESSION['ID_C_MHS'] = $id_c_mhs[0]['ID_C_MHS'];
				}
			}
		}		
	}
}

if ($mode == 'logout')
{
	$_SESSION['ID_C_MHS'] = ''; // form baru
	$_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] = '';
	session_destroy();
	header('Location: index.php');
	exit();
}

if ($_SESSION['ID_C_MHS'] != '' and $_SESSION['TGL_VERIFIKASI_PENDIDIKAN'] != '')
{
	header("Location: /form.php?mode=hasil-kesehatan");
	exit();
}
elseif ($_SESSION['ID_C_MHS'] != '')
{
	header("Location: /form.php?mode=page-1");
	exit();
} 
else
{
	$smarty->display("login/snmptn.tpl");
}
?>