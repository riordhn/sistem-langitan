<html>
<head>
    <title>Regmaba SNMPTN - Universitas Airlangga</title>
    <meta name="google-site-verification" content="ihkYFpHX_zbP4SfrkqIEF7c2Lp08dZFIxJ2MKI6m_Hk" />
    <link rel="shortcut icon" href="http://cybercampus.unair.ac.id/img/icon.ico"/>
    <link rel="stylesheet" type="text/css" href="http://cybercampus.unair.ac.id/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="http://regmaba.unair.ac.id/css/style.css" />
	<meta name="description" content="Halaman registrasi ulang mahasiswa baru jalur SNMPTN Universitas Airlangga">
</head>

<body>
    <div class="wrap-box">
        <div class="login-box">
            <form action="index.php" method="post">
            <input type="hidden" name="mode" value="login">
            <table>
                <tr>
                    <td class="right-align"><strong>No Ujian</strong></td>
                    <td>
                        <input name="no_ujian" type="text" maxlength="12" style="width: 200px">
                    </td>
                </tr>
                <tr>
                    <td class="right-align"><strong>Tanggal Lahir</strong></td>
                    <td>
						<select name="tgl_lahir_Day">
							<option value=""></option>
							<option value="1">01</option>
							<option value="2">02</option>
							<option value="3">03</option>
							<option value="4">04</option>
							<option value="5">05</option>
							<option value="6">06</option>
							<option value="7">07</option>
							<option value="8">08</option>
							<option value="9">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="26">26</option>
							<option value="27">27</option>
							<option value="28">28</option>
							<option value="29">29</option>
							<option value="30">30</option>
							<option value="31">31</option>
						</select>
						<select name="tgl_lahir_Month">
							<option value=""></option>
							<option value="01">Januari</option>
							<option value="02">Pebruari</option>
							<option value="03">Maret</option>
							<option value="04">April</option>
							<option value="05">Mei</option>
							<option value="06">Juni</option>
							<option value="07">Juli</option>
							<option value="08">Agustus</option>
							<option value="09">September</option>
							<option value="10">Oktober</option>
							<option value="11">November</option>
							<option value="12" selected="selected">Desember</option>
						</select>
						<input type="text" name="tgl_lahir_Year" maxlength="4" size="4" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
						<input type="submit" value="Login" />
                        <button onClick="window.location = '/index.php'; return false;">Kembali</button>
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</body>
</html>