<?php

ini_set('max_execution_time',0);
ini_set('memory_limit','-1');
require('../nusoap/nusoap.php');
require('../nusoap/class.wsdlcache.php');

class Get_token{

	var $wsdl;
	var $username;
	var $password;
	var $client;
	var $token;

	public function __construct(){

		$this->wsdl = 'http://localhost:8082/ws/sandbox.php?wsdl';
		$this->username = '071086';
		$this->password = 'piwulang';
		$this->token = $this->getToken();
	}

	public function getToken(){

		$this->client =  new nusoap_client($this->wsdl,true);
		$proxy = $this->client->getProxy();
		$err = $this->client->getError();
		
		$result = $proxy->GetToken($this->username,$this->password);
		return $result;
	}

	public function getTableList(){
		return $this->client->getProxy($this->token);

	}

	public function getDictionary($tableName){
		return $this->client->getProxy()->GetDictionary($this->token,$tableName);
	}

	public function getRecord($tableName,$filter){
		return $this->client->getProxy()->GetRecord($this->token,$tableName,$filter);
	}

	public function getRecordset($tableName,$filter,$limit,$order,$offset){ # Jumlah parameter 5
		return $this->client->getProxy()->GetRecordset($this->token,$tableName,$filter,$order,$limit,$offset);
	}

	public function getDeletedRecordset($tableName,$filter,$limit,$order,$offset){ # Jumlah parameter 5
		return $this->client->getProxy()->GetDeletedRecordset($this->token,$tableName,$filter,$order,$limit,$offset);
	}

	public function getCountRecordset($tableName){
		return $this->client->getProxy()->GetCountRecordset($this->token,$tableName);
	}

	public function getCountDeletedRecordset($tableName){
		return $this->client->getProxy()->GetCountDeletedRecordset($this->token,$tableName);
	}

	public function InsertRecord($tableName,$data){
		return $this->client->getProxy()->InsertRecord($this->token,$tableName,json_encode($data));
	}

	public function InsertRecordset($tableName,$x_data){
		return $this->client->getProxy()->InsertRecordset($this->token,$tableName,json_encode($x_data));
	}

	public function UpdateRecord($tableName,$key,$x_data){
		$data['key'] = $key;
		$data['data'] = $x_data;
		return $this->client->getProxy()->UpdateRecord($this->token,$tableName,json_encode($data));
	}

	public function UpdateRecordset($tableName,$key,$x_data){
		$data['key'] = $key;
		$data['data'] = $x_data;
		return $this->client->getProxy()->UpdateRecordset($this->token,$tableName,json_encode($data));
	}

	public function DeleteRecord($tableName,$key_and_value){
		return $this->client->getProxy()->DeleteRecord($this->token,$tableName,json_encode($key_and_value));
	}

	public function DeleteRecordset($tableName,$key_and_value){

		$data = array();
		$data[] = $key_and_value;
		return $this->client->getProxy()->DeleteRecord($this->token,$tableName,json_encode($data));
	}

}




