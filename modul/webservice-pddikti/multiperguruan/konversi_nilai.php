<?php

function field_nilai(){
	$field = array('id_kls','id_reg_pd','asal_data','nilai_angka','nilai_huruf','nilai_indeks');
	return $field;
}

####################################### KONVERSI STRUKTUR (FIELD TABLE LANGITAN) KE STRUKTUR (FIELD TABLE FEEDER) ###################################  
function structure_nilai($x_field){

	switch ($x_field) {
		/**
		#dimatikan karena proses update belum dipakai
		case 'ID_KURIKULUM_PRODI':
		  		$field = 'id_kurikulum_sp'; # kosongkan jika mode tambah
			break;
		*/
		case 'ID_REG_PD':
		  		$field = 'id_reg_pd';
			break;
		case 'ID_KLS':
		  		$field = 'id_kls'; # L / P
			break;
		case 'NILAI_ANGKA':
		  		$field = 'nilai_angka';
			break;
		default :
				$field = $x_field;
			break;
	}
		return $field;
}


####################################### KONVERSI NILAI (DATA LANGITAN) KE NILAI (DATA FEEDER) ###################################  
/**
function satuanpendidikan($nspn){
	return $id_sp = $npsn == 1 ? '151b4051-d151-44d3-a2b7-520e2421a4fb' : NULL; #proses filter belum berhasil
}


function get_kurikulum_sp($id_sms,$id_smt,$token){
	
	$filter = "id_sms='".$id_sms."' and id_smt_berlaku='".$id_smt."'";
	$get_data = $token->getRecord('kurikulum',$filter,$limit = null,$order=null,null);

	if(!array_key_exists('id_kurikulum_sp', $get_data['result'])) $get_data['result']['id_kurikulum_sp'] = '';
	return $get_data['result']['id_kurikulum_sp'];
}

function get_mk_mk_kurikulum($id_sms,$kode_mk,$token){
	
	$filter = "id_sms='".$id_sms."' and kode_mk='".$kode_mk."'";
	$get_data = $token->getRecord('mata_kuliah',$filter,$limit = null,$order = null,$offset = null);
	if(!array_key_exists('id_mk', $get_data['result'])) $get_data['result']['id_mk'] = '';
	return $get_data['result']['id_mk'];
}
*/