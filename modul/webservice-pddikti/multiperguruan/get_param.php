<?php
require '../../../config.php';

	if($_POST['table'] == 'get_prodi'){
		$sql = "select ps.id_program_studi,jenjang.nm_jenjang||' '||ps.nm_program_studi as nm_program_studi from program_studi ps 
				join jenjang on jenjang.id_jenjang = ps.id_jenjang
				join fakultas fak on ps.id_fakultas = fak.id_fakultas
				where fak.id_perguruan_tinggi = '".$_POST['id_pt']."'";			
		$output = $db->QueryToArray($sql);
		
		$options = "<option value=''>--- Semua Program Studi ---</option>";
		foreach ($output as $o){
			$options .= "<option value='".$o['ID_PROGRAM_STUDI']."'>".$o['NM_PROGRAM_STUDI']."</option>";
		}
		echo $options; 
	}

	if($_POST['table'] == 'get_semester'){
		$sql = "select smt.id_semester,smt.tahun_ajaran||' - '|| smt.group_semester as label from semester smt
				where smt.id_perguruan_tinggi = '".$_POST['id_pt']."' and tipe_semester = 'REG'
				order by label desc";			
		$output = $db->QueryToArray($sql);
		
		$options = "<option value=''>--- Semua Kurikulum ---</option>";
		foreach ($output as $o){
			$options .= "<option value='".$o['ID_SEMESTER']."'>".$o['LABEL']."</option>";
		}
		echo $options; 
	}

	if($_POST['table'] == 'get_kurikulum'){
		$sql = "select upper(nm_kurikulum) as nm_kurikulum,kur_prodi.id_kurikulum_prodi from kurikulum_prodi kur_prodi 
				join kurikulum kur on kur_prodi.id_kurikulum = kur.id_kurikulum
				where kur_prodi.id_program_studi = '".$_POST['id_prodi']."' order by nm_kurikulum";			
		$output = $db->QueryToArray($sql);
		
		$options = "<option value=''>--- Semua Kurikulum ---</option>";
		
		foreach ($output as $o){
			$options .= "<option value='".$o['ID_KURIKULUM_PRODI']."'>".$o['NM_KURIKULUM']."</option>";
		}
		
		echo $options; 
	}
	
	
?>