<?php

function field_mahasiswa(){
	return $field = array('id_pd','nm_pd','jk','nm_ibu_kandung','tmpt_lahir','tgl_lahir','id_agama','id_sp',
						   'ds_kel','id_wil','a_terima_kps','stat_pd','id_kebutuhan_khusus_ayah','id_kebutuhan_khusus_ibu',
						   'kewarganegaraan'
					);
}

####################################### KONVERSI STRUKTUR (FIELD TABLE LANGITAN) KE STRUKTUR (FIELD TABLE FEEDER) ###################################  
function structure_mahasiswa($x_field){

	switch ($x_field) {
		case 'ID_MHS':
		  		$field = 'id_pd'; # kosongkan jika mode tambah
			break;
		case 'NM_PENGGUNA':
		  		$field = 'nm_pd';
			break;
		case 'KELAMIN_PENGGUNA':
		  		$field = 'jk'; # L / P
			break;
		case 'NM_IBU_MHS':
		  		$field = 'nm_ibu_kandung'; # L / P
			break;
		case 'NM_KOTA':
		  		$field = 'tmpt_lahir';
			break;
		case 'TGL_LAHIR_PENGGUNA':
		  		$field = 'tgl_lahir'; # yyyy-mm-dd
			break;
		case 'ID_AGAMA':
		  		$field = 'id_agama'; # GetRecordset:agama
			break;
		case 'ID_PERGURUAN_TINGGI':
		  		$field = 'id_sp'; #get record satuan_pendidikan
			break;
		case 'ID_KELURAHAN':
		  		$field = 'ds_kel';
			break;
		case 'ID_WILAYAH':
		  		$field = 'id_wil'; # GetRecordset:wilayah
			break;
		case 'A_TERIMA_KPS':
		  		$field = 'a_terima_kps'; # 0 = bukan penerima kps
			break;
		/**
		case 'NM_PENGGUNA':
		  		$field = 'stat_pd'; #A 
			break;
		case 'NM_IBU_MHS':
		  		$field = 'nm_ibu_kandung';
			break;
		case 'NM_PENGGUNA':
		  		$field = 'kewarganegaraan'; # set default to ID = indonesia
			break;
		case 'NM_PENGGUNA':
		  		$field = 'regpd_id_reg_pd'; # kosongkan jika mode tambah
			break;
		case 'ID_PROGRAM_STUDI':
		  		$field = 'regpd_id_sms'; # ID SMS (Satuan Manajemen Sumberdaya). Web Service: GetRecordse:sms
			break;
		case 'NM_PENGGUNA':
		  		$field = 'regpd_id_pd'; # ID_PD Mahasiswa. Web Service: GetRecordset:mahasiswa
			break;
		case 'NM_PENGGUNA':
		  		$field = 'regpd_id_sp'; # ID Perguruan Tinggi. Web Service: GetRecordset:satuan_pendidikan
			break;
		case 'NM_PENGGUNA':
		  		$field = 'regpd_id_jns_daftar'; # Web Service: GetRecordset:jenis_pendaftaran
			break;
		case 'NM_PENGGUNA':
		  		$field = 'regpd_tgl_masuk_sp';  # yyyy-mm-dd
			break;
		case 'NM_PENGGUNA':
		  		$field = 'regpd_a_pernah_paud'; # 0 : tdk pernah PAUD, 1 : pernah PAUD
			break;
		case 'NM_PENGGUNA':
		  		$field = 'regpd_a_pernah_tk'; # 0 : tdk pernah TK, 1 : pernah TK
			break;
		*/
		default :
				$field = $x_field;
			break;
	}
		return $field;
}


####################################### KONVERSI NILAI (DATA LANGITAN) KE NILAI (DATA FEEDER) ###################################  
function getagama($id_agama_langitan){
	switch ($id_agama_langitan) {
		case 1:
				$id_agama = 1; # islam
			break;
		case 2:
				$id_agama = 2; # kristen Protestan
			break;
		case 3:
				$id_agama = 3; # Kristen katolik
			break;
		case 4:
				$id_agama = 4; # Hindu
			break;
		case 5:
				$id_agama = 5; # Budha
			break;
		case 6:
				$id_agama = 99; # Lain-lain
			break;
		case 22:
				$id_agama = 6; # Konghucu
			break;
		case 42:
				$id_agama = ''; # agama jawa tidak tersedia di web service PDDIKTI
			break;
		case 43:
				$id_agama = ''; # agama sunda tidak tersedia di web service PDDIKTI
			break;
		default:
				$id_agama = $id_agama_langitan;
			break;
	}
	return $id_agama;
}

function getjeniskelamin($id_jk){
	return $jk = $id_jk == 1 ? 'L' : 'P';
}


function getwilayah(){
	return '000000'; # proses filter wilayah belum berhasil, sementara memakai id_wilayah = 000000 = indonesia
}

function _getkelurahan(){
	return '000000'; # proses filter wilayah belum berhasil, sementara memakai id_kelurahan = 000000 = indonesia
}

function _getjenisdaftar($id_jenis_daftar){

}

