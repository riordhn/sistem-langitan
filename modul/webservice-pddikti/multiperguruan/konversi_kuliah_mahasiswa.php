<?php

function field_kuliah_mahasiswa(){
	return $field = array('id_smt','id_reg_pd','ips','sks_smt','ipk','sks_total','id_stat_mhs');
}

####################################### KONVERSI STRUKTUR (FIELD TABLE LANGITAN) KE STRUKTUR (FIELD TABLE FEEDER) ###################################  
function structure_kuliah_mahasiswa($x_field){

	switch ($x_field) {
		/**
		#dimatikan karena proses update belum dipakai
		case 'ID_KURIKULUM_PRODI':
		  		$field = 'id_kurikulum_sp'; # kosongkan jika mode tambah
			break;
		*/
		case 'NM_KURIKULUM':
		  		$field = 'id_smt';
			break;
		case 'ID_REG_PD':
		  		$field = 'id_reg_pd'; # L / P
			break;
		case 'IPS':
		  		$field = 'ips';
			break;
		case 'SKS_SMT':
		  		$field = 'sks_smt'; # yyyy-mm-dd
			break;
		case 'IPK':
		  		$field = 'ipk'; # GetRecordset:agama
			break;
		case 'SKS_TOTAL':
		  		$field = 'sks_total'; #get record satuan_pendidikan
			break;
		case 'ID_STAT_MHS':
		  		$field = 'id_stat_mhs';
			break;
		default :
				$field = $x_field;
			break;
	}
		return $field;
}


####################################### KONVERSI NILAI (DATA LANGITAN) KE NILAI (DATA FEEDER) ###################################  

function getsemester_kuliah_mahasiswa($tahun_semester,$x_semester,$token){
	#global $token;
	$semester = $x_semester == 'Genap' ? 2 : 1;
	$filter = "id_thn_ajaran = '".$tahun_semester."' and smt = '".$semester."' and nm_smt LIKE '%".$x_semester."%'";
	$get_data = $token->getRecord('semester',$filter,$limit = null,$order = null,$offset = null);
	if(isset($get_data['result']['id_smt'])){
		return $get_data['result']['id_smt'];
	}
}
	
