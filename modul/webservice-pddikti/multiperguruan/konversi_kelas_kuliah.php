<?php

####################################### KONVERSI STRUKTUR (FIELD TABLE LANGITAN) KE STRUKTUR (FIELD TABLE FEEDER) ###################################  
function field_kelas_kuliah(){
	return $field = array('id_kls','id_sms','id_smt','id_mk','nm_kls','sks_mk','sks_tm','sks_prak',
						   'sks_prak_lap','sks_sim','bahasan_case','tgl_mulai_koas','tgl_selesai_koas',
						   'id_mou','a_selenggara_pditt','kuota_pditt','a_pengguna_pditt','id_kls_pditt'
					);
}

function structure_kelas_kuliah($x_field){

	switch ($x_field) {
			/**
			# Dimatikan karena untuk proses update belum dipakai
			case 'ID_KELAS_MK':
				$field = 'id_kls';
			break; 
			*/
			case 'ID_SMS':
				$field = 'id_sms';
			break;
			case 'ID_SEMESTER':
			$field = 'id_smt';
			break;
			case 'ID_MK':
				$field = 'id_mk';
			break;
			case 'NAMA_KELAS':
				$field = 'nm_kls';
			break;
			case 'KREDIT_SEMESTER':
				$field = 'sks_mk';
			break;
			case 'KREDIT_TATAP_MUKA':
				$field = 'sks_tm';
			break;
			case 'KREDIT_PRAKTIKUM':
				$field = 'sks_prak';
			break;
			/**
			## field feeder ini tidak harus diisi
			$field = 'sks_prak_lap';
			$field = 'sks_sim';
			$field = 'bahasan_case';
			$field = 'tgl_mulai_koas';
			$field = 'tgl_selesai_koas';
			$field = 'id_mou';
			$field = 'a_selenggara_pditt';
			$field = 'kuota_pditt';
			$field = 'a_pengguna_pditt';
			$field = 'id_kls_pditt';
			*/

		default :
				$field = $x_field;
			break;
	}
		return $field;
}


####################################### KONVERSI NILAI (DATA LANGITAN) KE NILAI (DATA FEEDER) ###################################  

function getsemester_kelas_kuliah($tahun_semester,$x_semester,$token){
	#global $token;
	#$x_semester = 'Ganjil';
	$semester = $x_semester == 'Genap' ? 2 : 1;
	$filter = "id_thn_ajaran = '".$tahun_semester."' and smt = '".$semester."'";
	$get_data = $token->getRecord('semester',$filter,$limit = null,$order = null,$offset = null);
	return $get_data['result']['id_smt'];
}

function getmatakuliah_kelas_kuliah($id_prodi,$kode_mk,$token){
	$filter = "id_sms='".$id_prodi."' and kode_mk='".$kode_mk."'";
	$get_data = $token->getRecord('mata_kuliah',$filter,$limit = null,$order = null,$offset = null);
	if(!array_key_exists('id_mk', $get_data['result'])) $get_data['result']['id_mk'] = '';
	return $get_data['result']['id_mk'];
}

