<?php

function field_mahasiswa_pt(){
	return $field = array('id_reg_pd','id_sms','id_pd','id_sp','id_jns_daftar','nipd','tgl_masuk_sp','id_jns_keluar',
						   'tgl_keluar','ket','skhun','a_pernah_paud','a_pernah_tk','mulai_smt'
					);
}

####################################### KONVERSI STRUKTUR (FIELD TABLE LANGITAN) KE STRUKTUR (FIELD TABLE FEEDER) ###################################  
function structure_mahasiswa_pt($x_field){

	switch ($x_field) {
		case 'ID_SMS_FEEDER':
		  		$field = 'id_sms'; # kosongkan jika mode tambah
			break;
		case 'ID_PD':
		  		$field = 'id_pd';
			break;
		case 'NIM_MHS':
		  		$field = 'nipd';
			break;
		case 'THN_ANGKATAN_MHS':
		  		$field = 'mulai_smt';
			break;
		default :
				$field = $x_field;
			break;
	}
		return $field;
}


