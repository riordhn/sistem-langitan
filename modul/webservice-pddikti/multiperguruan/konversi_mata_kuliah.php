<?php

####################################### KONVERSI STRUKTUR (FIELD TABLE LANGITAN) KE STRUKTUR (FIELD TABLE FEEDER) ###################################  
function field_mata_kuliah(){
	return $field = array('id_mk','id_sms','id_jenj_didik','kode_mk','nm_mk','jns_mk','kel_mk','sks_mk',
						   'sks_tm','sks_prak','sks_prak_lap','sks_sim','metode_pelaksanaan_kuliah',
						   'a_sap','a_silabus','a_bahan_ajar','acara_prak','a_diktat','tgl_mulai_efektif','tgl_akhir_efektif'
					);
}

function structure_mata_kuliah($x_field){

	switch ($x_field) {
			/**
			# Dimatikan karena untuk proses update belum dipakai
			case 'ID_MATA_KULIAH':
				$field = 'id_mk';
			break; 
			*/
			case 'ID_SMS':
				$field = 'id_sms';
			break;
			case 'ID_JENJ_DIDIK':
			$field = 'id_jenj_didik';
			break;
			case 'KODE_MK':
				$field = 'kode_mk';
			break;
			case 'NM_MK':
				$field = 'nm_mk';
			break;
			case 'SKS_MK':
				$field = 'sks_mk';
			break;
			case 'SKS_TM':
				$field = 'sks_tm';
			break;
			case 'SKS_PRAK':
				$field = 'sks_prak';
			break;
			
		default :
				$field = $x_field;
			break;
	}
		return $field;
}


####################################### KONVERSI NILAI (DATA LANGITAN) KE NILAI (DATA FEEDER) ###################################  

function getjenjangdidik($tahun_semester,$x_semester,$token){
	#global $token;
	$semester = $x_semester == 'Genap' ? 2 : 1;
	$filter = "id_thn_ajaran = '".$tahun_semester."' and smt = '".$semester."'";
	$get_data = $token->getRecord('semester',$filter,$limit = null,$order = null,$offset = null);
	return $get_data['result']['id_smt'];
}



