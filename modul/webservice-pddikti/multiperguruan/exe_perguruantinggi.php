<?php

include('konversi_mata_kuliah.php');
include('konversi_kurikulum.php');
include('konversi_mk_kurikulum.php');
include('konversi_kelas_kuliah.php');
include('konversi_nilai.php');
include('konversi_kuliah_mahasiswa.php');
include('konversi_mahasiswa.php');
include('konversi_mahasiswa_pt.php');

function make_request_to_langitan($id_pt,$table_request,$database){
	return $output;   
}

function decode_from_langitan($output){
	#Decode from json
	$data_langitan = json_decode($output,true);
	return $data_langitan;
}

################################# Proses Konversi struktur langitan menjadi struktur Feeder ########################################
## mata kuliah Struktur
function convert_lang_feeder_mata_kuliah($data_langitan){
	#looping to convert field structure from langitan to feeder
	$temp = array();
	foreach ($data_langitan as $key => $value) {
		foreach ($value as $subkey => $subvalue) {
			$new_subkey = structure_mata_kuliah($subkey); #get return from convert field
			$temp[$key][$new_subkey] = $subvalue;
		}
	}
	return $temp;
}

## kurikulum Struktur
function convert_lang_feeder_kurikulum($data_langitan){
	#looping to convert field structure from langitan to feeder
	$temp = array();
	foreach ($data_langitan as $key => $value) {
		foreach ($value as $subkey => $subvalue) {
			$new_subkey = structure_kurikulum($subkey); #get return from convert field
			$temp[$key][$new_subkey] = $subvalue;
		}
	}

	return $temp;
}
## Kurikulum_mk Struktur
function convert_lang_feeder_kurikulum_mk($data_langitan){
	$temp = array();
	foreach ($data_langitan as $key => $value) {
		foreach ($value as $subkey => $subvalue) {
			$new_subkey = structure_mk_kurikulum($subkey); #get return from convert field
			$temp[$key][$new_subkey] = $subvalue;
		}
	}
	return $temp;
}
## Kelas_kuliah Struktur
function convert_lang_feeder_kelas_kuliah($data_langitan){
	#looping to convert field structure from langitan to feeder
	$temp = array();
	foreach ($data_langitan as $key => $value) {
		foreach ($value as $subkey => $subvalue) {
			$new_subkey = structure_kelas_kuliah($subkey); #get return from convert field
			$temp[$key][$new_subkey] = $subvalue;
		}
	}

	return $temp;
}
## nilai struktur 
function convert_lang_feeder_nilai($data_langitan){
	#looping to convert field structure from langitan to feeder
	$temp = array();
	foreach ($data_langitan as $key => $value) {
		foreach ($value as $subkey => $subvalue) {
			$new_subkey = structure_nilai($subkey); #get return from convert field
			$temp[$key][$new_subkey] = $subvalue;
		}
	}

	return $temp;
}

## kuliah_mahasiswa struktur 
function convert_lang_feeder_kuliah_mahasiswa($data_langitan){
	#looping to convert field structure from langitan to feeder
	$temp = array();
	foreach ($data_langitan as $key => $value) {
		foreach ($value as $subkey => $subvalue) {
			$new_subkey = structure_kuliah_mahasiswa($subkey); #get return from convert field
			$temp[$key][$new_subkey] = $subvalue;
		}
	}

	return $temp;
}

## mahasiswa Struktur
function convert_lang_feeder_mahasiswa($data_langitan){
	#looping to convert field structure from langitan to feeder
	$temp = array();
	foreach ($data_langitan as $key => $value) {
		foreach ($value as $subkey => $subvalue) {
			$new_subkey = structure_mahasiswa($subkey); #get return from convert field
			$temp[$key][$new_subkey] = $subvalue;
		}
	}

	return $temp;
}

## mahasiswa Struktur
function convert_lang_feeder_mahasiswa_pt($data_langitan){
	#looping to convert field structure from langitan to feeder
	$temp = array();
	foreach ($data_langitan as $key => $value) {
		foreach ($value as $subkey => $subvalue) {
			$new_subkey = structure_mahasiswa_pt($subkey); #get return from convert field
			$temp[$key][$new_subkey] = $subvalue;
		}
	}

	return $temp;
}

################################# Proses Cleaning Field yang tidak dipakai di feeder #################################################
### Cleaning struktur mata_kuliah
function cleaning_field_mata_kuliah($data_langitan,$token){
	#looping to cleaning empty field
	foreach ($data_langitan as $key => $value) {

			if($data_langitan[$key]['id_jenj_didik'] == 1)$data_langitan[$key]['id_jenj_didik'] = 30;
			if($data_langitan[$key]['id_jenj_didik'] == 5)$data_langitan[$key]['id_jenj_didik'] = 22;
			if($data_langitan[$key]['id_jenj_didik'] == 4)$data_langitan[$key]['id_jenj_didik'] = 23;
			if($data_langitan[$key]['id_jenj_didik'] == 2)$data_langitan[$key]['id_jenj_didik'] = 35;
			/**
			S1 = 1 = 30;
			D3 = 5 = 22;
			D4 = 4 = 23;
			S2 = 2 = 35;
			*/
		foreach ($value as $subkey => $subvalue) {
			#menghilangkan key array yang bernilai kosong
			if ($data_langitan[$key][$subkey] == ''){
				unset($data_langitan[$key][$subkey]);
			} 		
		}
		foreach ($value as $subkey => $subvalue) {
			if(!in_array($subkey, field_mata_kuliah())){
				unset($data_langitan[$key][$subkey]);
			}
		}

	}
	return $data_langitan;
}
### Cleaning struktur kurikulum
function cleaning_field_kurikulum($data_langitan,$token){
	#looping to cleaning empty field
	foreach ($data_langitan as $key => $value) {
		foreach ($value as $subkey => $subvalue) {
			
			#set id_semester berdasarkan id_semester di feeder
			if($subkey == 'id_smt_berlaku'){
				$data_langitan[$key][$subkey] = getsemester_kurikulum($data_langitan[$key]['id_smt_berlaku'],$data_langitan[$key]['GROUP_SEMESTER'],$token);
			}
			#menghilangkan key array yang bernilai kosong
			if ($data_langitan[$key][$subkey] == ''){
				unset($data_langitan[$key][$subkey]);
			} 		
			
			//$data_langitan[$key]['ID_JENJANG'] = 6;
			if($data_langitan[$key]['id_jenj_didik'] == 1)$data_langitan[$key]['id_jenj_didik'] = 30;
			if($data_langitan[$key]['id_jenj_didik'] == 5)$data_langitan[$key]['id_jenj_didik'] = 22;
			if($data_langitan[$key]['id_jenj_didik'] == 4)$data_langitan[$key]['id_jenj_didik'] = 23;
			if($data_langitan[$key]['id_jenj_didik'] == 2)$data_langitan[$key]['id_jenj_didik'] = 35;
			/**
			S1 = 1 = 30;
			D3 = 5 = 22;
			D4 = 4 = 23;
			S2 = 2 = 35;
			*/

			#menghilangkan index array yang dari langitan (index tidak sesuai dengan index feeder)
			if(!in_array($subkey, field_kurikulum())){
				unset($data_langitan[$key][$subkey]);
			}
			
		}
	}
	return $data_langitan;
}
### Cleaning struktur mata_kuliah kurikulum_mk
function cleaning_field_kurikulum_mk($data_langitan,$token){

	foreach ($data_langitan as $key => $value) {
		foreach ($value as $subkey => $subvalue) {
			if ($data_langitan[$key][$subkey] == ''){
				unset($data_langitan[$key][$subkey]);
			} 
		}
	}
	return $data_langitan; 

}
### Cleaning struktur kelas_kuliah
function cleaning_field_kelas_kuliah($data_langitan,$token){
	#looping to cleaning empty field
	foreach ($data_langitan as $key => $value) {
		#set id_semester berdasarkan id_semester di feeder
		$data_langitan[$key]['id_smt'] = getsemester_kelas_kuliah($data_langitan[$key]['THN_AKADEMIK_SEMESTER'],$data_langitan[$key]['GROUP_SEMESTER'],$token);
		
		foreach ($value as $subkey => $subvalue) {
			#menghilangkan index array yang dari langitan (index tidak sesuai dengan index feeder)
			if(!in_array($subkey, field_kelas_kuliah())){
				unset($data_langitan[$key][$subkey]);
			}
		}
	}
	foreach ($data_langitan as $key => $value) {
		if($data_langitan[$key]['id_sms'] == '' || $data_langitan[$key]['id_smt'] == '' || $data_langitan[$key]['id_mk'] == '' || $data_langitan[$key]['sks_mk'] == '' || $data_langitan[$key]['nm_kls'] == ''){
			unset($data_langitan[$key]);
		}
		
	}
	return $data_langitan;
}
### Cleaning struktur Nilai
function cleaning_field_nilai($data_langitan,$token){
	#looping to cleaning empty field
	foreach ($data_langitan as $key => $value) {
		foreach ($value as $subkey => $subvalue) {
			#menghilangkan index array yang dari langitan (index tidak sesuai dengan index feeder)
			if(!in_array($subkey, field_nilai())){
				unset($data_langitan[$key][$subkey]);
			}
		}
	}

	foreach ($data_langitan as $key => $value) {
		if($data_langitan[$key]['id_kls'] == '' || $data_langitan[$key]['id_reg_pd'] == '' || $data_langitan[$key]['nilai_angka'] == ''){
			unset($data_langitan[$key]);
		}
		
	}
	return $data_langitan;
}
### Cleaning struktur kuliah mahasiswa
function cleaning_field_kuliah_mahasiswa($data_langitan,$token){
	#looping to cleaning empty field

	foreach ($data_langitan as $key => $value) {
		#set id_semester berdasarkan id_semester di feeder
		$data_langitan[$key]['id_stat_mhs'] = 'A';
		$data_langitan[$key]['id_smt'] = getsemester_kuliah_mahasiswa($data_langitan[$key]['THN_AKADEMIK_SEMESTER'],$data_langitan[$key]['GROUP_SEMESTER'],$token);
	}

	foreach ($data_langitan as $key => $value) {
		foreach ($value as $subkey => $subvalue) {
			#menghilangkan index array yang dari langitan (index tidak sesuai dengan index feeder)
			if(!in_array($subkey, field_kuliah_mahasiswa())){
				unset($data_langitan[$key][$subkey]);
			}
		}
	}

	foreach ($data_langitan as $key => $value) {
		if($data_langitan[$key]['id_smt'] == '' || $data_langitan[$key]['id_reg_pd'] == ''){
			unset($data_langitan[$key]);
		}
		
	}
	return $data_langitan;
}
### Cleaning struktur Nilai
function cleaning_field_kuliah_mahasiswa_tanpa_nilai($data_langitan,$id_smt,$stat_mhs){
	#looping to cleaning empty field

	foreach ($data_langitan as $key => $value) {
		#set id_semester berdasarkan id_semester di feeder
		$data_langitan[$key]['id_stat_mhs'] = $stat_mhs;
		$data_langitan[$key]['id_smt'] = $id_smt;
		$data_langitan[$key]['id_reg_pd'] = $data_langitan[$key]['ID_REG_PD'];
	}

	foreach ($data_langitan as $key => $value) {
		unset($data_langitan[$key]['ID_REG_PD']);
		if($data_langitan[$key]['id_smt'] == '' || $data_langitan[$key]['id_reg_pd'] == ''){
			unset($data_langitan[$key]);
		}
		
	}
	return $data_langitan;
}

### Cleaning struktur mahasiswa
function cleaning_field_mahasiswa($data_langitan,$token){
	#looping to cleaning empty field
	foreach ($data_langitan as $key => $value) {
		$data_langitan[$key]['id_sp'] = satuanpendidikan(1);
		$data_langitan[$key]['jk'] = getjeniskelamin($data_langitan[$key]['jk']);
		$data_langitan[$key]['ds_kel'] = 'ngelom';
		$data_langitan[$key]['id_kk'] = 0;
		$data_langitan[$key]['id_wil'] =  getwilayah();
		$data_langitan[$key]['a_terima_kps'] = 0;
		$data_langitan[$key]['stat_pd'] = 'A';
		$data_langitan[$key]['id_kebutuhan_khusus_ayah'] = 0;
		$data_langitan[$key]['id_kebutuhan_khusus_ibu'] = 0;
		$data_langitan[$key]['kewarganegaraan'] = 1;

		if($data_langitan[$key]['tgl_lahir'] == '') $data_langitan[$key]['tgl_lahir'] = '1997-01-01';
		if($data_langitan[$key]['nm_ibu_kandung'] == '') $data_langitan[$key]['nm_ibu_kandung'] = 'BELUM TEREKAM';
		if($data_langitan[$key]['tmpt_lahir'] == '') $data_langitan[$key]['tmpt_lahir'] = 'BELUM TEREKAM';
		if($data_langitan[$key]['id_agama'] == '') $data_langitan[$key]['id_agama'] = 1;


		
		foreach ($value as $subkey => $subvalue) {
			/**
			#menghilangkan key array yang bernilai kosong
			if ($data_langitan[$key][$subkey] == ''){
				unset($data_langitan[$key][$subkey]);
			} 	
			*/	
			#menghilangkan index array yang dari langitan (index tidak sesuai dengan index feeder)
			if(!in_array($subkey, field_mahasiswa())){
				unset($data_langitan[$key][$subkey]);
			}
		}
		
	}
	return $data_langitan;
}

### Cleaning struktur mahasiswa pt
function cleaning_field_mahasiswa_pt($data_langitan,$token){
	#looping to cleaning empty field
	foreach ($data_langitan as $key => $value) {
		$data_langitan[$key]['id_sp'] = satuanpendidikan(1);
		$data_langitan[$key]['id_jns_daftar'] = 1;# 1 = peserta didik baru
		$data_langitan[$key]['tgl_masuk_sp'] = '2015-07-07';
		$data_langitan[$key]['a_pernah_paud'] = 1;
		$data_langitan[$key]['a_pernah_tk'] = 1;
		$data_langitan[$key]['mulai_smt'] = getsemester_kurikulum($data_langitan[$key]['mulai_smt'],'Ganjil',$token);
		
		
		/**
		if($data_langitan[$key]['id_lahir'] == '') $data_langitan[$key]['tgl_lahir'] = '1997-01-01';
		if($data_langitan[$key]['nm_ibu_kandung'] == '') $data_langitan[$key]['nm_ibu_kandung'] = 'BELUM TEREKAM';
		if($data_langitan[$key]['tmpt_lahir'] == '') $data_langitan[$key]['tmpt_lahir'] = 'BELUM TEREKAM';
		if($data_langitan[$key]['id_agama'] == '') $data_langitan[$key]['id_agama'] = 1;
		*/

		
		foreach ($value as $subkey => $subvalue) {
						#menghilangkan index array yang dari langitan (index tidak sesuai dengan index feeder)
			if(!in_array($subkey, field_mahasiswa_pt())){
				unset($data_langitan[$key][$subkey]);
			}
		}
		
	}
	return $data_langitan;
}
############################################### Membuat Koneksi ke webservice ################################################

function make_feeder_token($database){
	#set database will be syncronize to sandbox
	
	if($database == 'sandbox'){
		require('get_token_sandbox.php');
		$token = new Get_token();
	}else{
		require('get_token_live.php');
		$token = new Get_token();
	}

	return $token;
}

function get_data_mk(){
	$token = make_feeder_token('sandbox');
	$get_data = $token->getRecordSet('mata_kuliah',$filter = null,$limit = null,$order=null,null);
	return $get_data;
}

########################################## Insert Data Ke Feeder ##########################################################

?>