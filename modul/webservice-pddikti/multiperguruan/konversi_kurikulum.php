<?php

function field_kurikulum(){
	return $field = array('id_kurikulum_sp','nm_kurikulum_sp','jml_sem_normal','jml_sks_lulus',
						  'jml_sks_wajib','jml_sks_pilihan','id_sms','id_jenj_didik','id_smt_berlaku'
					);
}

####################################### KONVERSI STRUKTUR (FIELD TABLE LANGITAN) KE STRUKTUR (FIELD TABLE FEEDER) ###################################  
function structure_kurikulum($x_field){

	switch ($x_field) {
		/**
		#dimatikan karena proses update belum dipakai
		case 'ID_KURIKULUM_PRODI':
		  		$field = 'id_kurikulum_sp'; # kosongkan jika mode tambah
			break;
		*/
		case 'NM_KURIKULUM':
		  		$field = 'nm_kurikulum_sp';
			break;
		case 'KELAMIN_PENGGUNA':
		  		$field = 'jml_sem_normal'; # L / P
			break;
		case 'NM_PENGGUNA':
		  		$field = 'jml_sks_lulus';
			break;
		case 'JML_SKS_WAJIB':
		  		$field = 'jml_sks_wajib'; # yyyy-mm-dd
			break;
		case 'JML_SKS_PILIHAN':
		  		$field = 'jml_sks_pilihan'; # GetRecordset:agama
			break;
		case 'ID_SMS_FEEDER':
		  		$field = 'id_sms'; #get record satuan_pendidikan
			break;
		case 'ID_JENJANG':
		  		$field = 'id_jenj_didik';
			break;
		case 'TAHUN_KURIKULUM':
		  		$field = 'id_smt_berlaku';
			break;
		default :
				$field = $x_field;
			break;
	}
		return $field;
}


####################################### KONVERSI NILAI (DATA LANGITAN) KE NILAI (DATA FEEDER) ###################################  

function satuanpendidikan($nspn){
	return $id_sp = $nspn == 1 ? '151b4051-d151-44d3-a2b7-520e2421a4fb' : NULL; #proses filter belum berhasil
}


function getsemester_kurikulum($tahun_semester,$x_semester,$token){
	#global $token;
	$semester = $x_semester == 'Genap' ? 2 : 1;
	$filter = "id_thn_ajaran = '".$tahun_semester."' and smt = '".$semester."' and nm_smt LIKE '%".$x_semester."%'";
	$get_data = $token->getRecord('semester',$filter,$limit = null,$order = null,$offset = null);
	if(isset($get_data['result']['id_smt'])){
		return $get_data['result']['id_smt'];
	}
}
	
