<?php

function field_ajar_dosen(){
	$field = array('id_ajar','id_reg_ptk','id_subst','id_kls','sks_subst_tot','sks_tm_subst','sks_prak_subst','sks_prak_lap_subst','sks_sim_subst','jml_tm_renc','jml_tm_real','id_jns_eval');
	return $field;
}

####################################### KONVERSI STRUKTUR (FIELD TABLE LANGITAN) KE STRUKTUR (FIELD TABLE FEEDER) ###################################  
function structure_ajar_dosen($x_field){

	switch ($x_field) {
		/**
		#dimatikan karena proses update belum dipakai
		case 'ID_KURIKULUM_PRODI':
		  		$field = 'id_kurikulum_sp'; # kosongkan jika mode tambah
			break;
		*/
		case 'NM_KURIKULUM':
		  		$field = 'nm_kurikulum_sp';
			break;
		case 'KELAMIN_PENGGUNA':
		  		$field = 'jml_sem_normal'; # L / P
			break;
		case 'NM_PENGGUNA':
		  		$field = 'jml_sks_lulus';
			break;
		case 'JML_SKS_WAJIB':
		  		$field = 'jml_sks_wajib'; # yyyy-mm-dd
			break;
		case 'JML_SKS_PILIHAN':
		  		$field = 'jml_sks_pilihan'; # GetRecordset:agama
			break;
		case 'ID_SMS_FEEDER':
		  		$field = 'id_sms'; #get record satuan_pendidikan
			break;
		case 'NM_PENGGUNA':
		  		$field = 'id_jenj_didik';
			break;
		case 'TAHUN_KURIKULUM':
		  		$field = 'id_smt_berlaku';
			break;
		default :
				$field = $x_field;
			break;
	}
		return $field;
}


####################################### KONVERSI NILAI (DATA LANGITAN) KE NILAI (DATA FEEDER) ###################################  
/**
function get_dosen_pt($nidn,$token){
	
	$filter = "nidn='".$nidn."'";
	$get_data = $token->getRecord('dosen',$filter,$limit = null,$order=null,null);

	if(!array_key_exists('id_ptk', $get_data['result'])) {
		$get_data['result']['id_ptk'] = '';
		return 0;
		exit();
	}

	$id_ptk = $get_data['result']['id_ptk'];
	$filter = "id_ptk='".$id_ptk."'";
	$get_data = $token->getRecord('dosen_pt',$filter,$limit = null,$order=null,null);

	if(!array_key_exists('id_reg_ptk', $get_data['result'])) $get_data['result']['id_reg_ptk'] = '';
	return $get_data['result']['id_reg_ptk'];
}

function get_id_kelas_kuliah($id_sms,$id_smt,$id_mk,$nm_kls,$token){
	
	$filter = "id_sms='".$id_sms."' and id_smt='".$kode_mk."'";
	$get_data = $token->getRecord('kelas_kuliah',$filter,$limit = null,$order = null,$offset = null);
	if(!array_key_exists('id_mk', $get_data['result'])) $get_data['result']['id_mk'] = '';
	return $get_data['result']['id_mk'];
}
*/