<?php
require '../../../config.php';

function buildquery($sql){
	global $db;
	return $db->QueryToArray($sql);
}

function buildquery_update($sql){
	global $db;
	return $db->Query($sql);
}

#get master semester
function getsemester($param){
	return $sql = "select * from semester where id_perguruan_tinggi = '".$param['id_pt']."'";
}

# get master mata kuliah 
function getmatakuliah($param){
	$sql = "select ps.id_sms_feeder as id_sms,mk.kd_mata_kuliah as kode_mk ,mk.nm_mata_kuliah as nm_mk,mk.id_mata_kuliah as id_mk,
			case when kmk.kredit_semester is null then 0 else kmk.kredit_semester end SKS_MK,
			case when kmk.kredit_tatap_muka is null then 0 else kmk.kredit_tatap_muka end sks_tm,
			case when kmk.kredit_praktikum is null then 0 else kmk.kredit_praktikum end sks_prak,ps.id_jenjang as id_jenj_didik
			from mata_kuliah mk 
			join program_studi ps on ps.id_program_studi =  mk.id_program_studi
			join fakultas fak on fak.id_fakultas = ps.id_fakultas
			left join kurikulum_mk kmk on kmk.id_mata_kuliah = mk.id_mata_kuliah
			where fak.id_perguruan_tinggi = '".$param['id_pt']."'";

			if(!empty($param['id_prodi']) && is_numeric($param['id_prodi'])) {
				$sql .= " and mk.id_program_studi = '".$param['id_prodi']."' and 
						  mk.id_mata_kuliah in(select kmk.id_mata_kuliah from kurikulum_mk kmk  where kmk.id_program_studi = '".$param['id_prodi']."')";
			}else{
				$sql .= "and mk.id_mata_kuliah in(select kmk.id_mata_kuliah from kurikulum_mk kmk)";
			}
			

	return $sql;
}

#get master kurikulum
function getkurikulum($param){
	$sql = "select ps.id_sms_feeder,ps.id_jenjang,kur_prodi.id_kurikulum_prodi,nm_kurikulum,tahun_kurikulum,jml_sks_wajib,jml_sks_pilihan,group_semester 
			from kurikulum_prodi kur_prodi
			join kurikulum kur on kur.id_kurikulum = kur_prodi.id_kurikulum 
			join program_studi ps on ps.id_program_studi = kur_prodi.id_program_studi
			join fakultas fak on fak.id_fakultas  = ps.id_fakultas
			join semester s on s.id_semester = kur_prodi.id_semester
			where fak.id_perguruan_tinggi = '".$param['id_pt']."'";

			if(!empty($param['id_prodi']) && is_numeric($param['id_prodi'])) $sql .= " and kur_prodi.id_program_studi = '".$param['id_prodi']."'";

	return $sql;
}

#get list mata kuliah kurikulum
function getkurikulummk($param){
	
	$sql = "select kur_prodi.id_kurikulum_sp,mk.id_mk,kmk.kredit_semester as sks_mk,
			case when mod(kmk.tingkat_semester,2) = 0 then 2 else 1 end smt,
			case when kmk.kredit_tatap_muka is null then 0 else kmk.kredit_tatap_muka end sks_tm,
      		case when kmk.kredit_praktikum is null then 0 else kmk.kredit_praktikum end sks_prak			
			from kurikulum_prodi kur_prodi 
			join kurikulum kur on kur.id_kurikulum = kur_prodi.id_kurikulum 
			join kurikulum_mk kmk on kmk.id_kurikulum_prodi = kur_prodi.id_kurikulum_prodi
			join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
			join program_studi ps on ps.id_program_studi = kur_prodi.id_program_studi
			join fakultas fak on fak.id_fakultas = ps.id_fakultas
			where fak.id_perguruan_tinggi = '".$param['id_pt']."'";

			if(!empty($param['id_prodi']) && is_numeric($param['id_prodi'])) $sql .= " and kur_prodi.id_program_studi = '".$param['id_prodi']."'";
			if(!empty($param['id_kurikulum_mk']) && is_numeric($param['id_kurikulum_mk'])) $sql .= " and kur_prodi.id_kurikulum = '".$param['id_kurikulum_mk']."'";
	
	return $sql;

}


#get master kelas_kuliah
function getkelaskuliah($param){
	$sql = "select kls_mk.id_kelas_mk,kls_mk.id_program_studi,kls_mk.id_semester,kmk.id_mata_kuliah,kmk.kredit_semester,
			smt.thn_akademik_semester,smt.group_semester,ps.id_sms_feeder as id_sms,mk.id_mk,
			case when kmk.kredit_tatap_muka IS NULL then 0 ELSE kmk.kredit_tatap_muka end kredit_tatap_muka,
      		case when kmk.kredit_praktikum IS NULL then 0 ELSE kmk.kredit_praktikum end kredit_praktikum,
			case 
				when nm_kls.nama_kelas = 'Pagi A' then 'PGI1'
				when nm_kls.nama_kelas = 'Pagi B' then 'PGI2'
				when nm_kls.nama_kelas = 'Pagi C' then 'PGI3'
				when nm_kls.nama_kelas = 'Sore/Malam A' then 'MLM1'
				when nm_kls.nama_kelas = 'Sore/Malam B' then 'MLM2'
				when nm_kls.nama_kelas = 'Sore/Malam C' then 'MLM3'
			end nama_kelas
			from kelas_mk kls_mk 
			join nama_kelas nm_kls on nm_kls.id_nama_kelas = kls_mk.no_kelas_mk
			join kurikulum_mk kmk on kmk.id_kurikulum_mk = kls_mk.id_kurikulum_mk
			join mata_kuliah mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
			join semester smt on smt.id_semester = kls_mk.id_semester
			join program_studi ps on ps.id_program_studi = kls_mk.id_program_studi
			join fakultas fak on fak.id_fakultas = ps.id_fakultas
			where fak.id_perguruan_tinggi = '".$param['id_pt']."'";

	if(!empty($param['id_semester'])) $sql .= " and kls_mk.id_semester = '".$param['id_semester']."'";
	if(!empty($param['id_prodi'])) $sql .= " and kls_mk.id_program_studi = '".$param['id_prodi']."'";
	if(!empty($param['limit'])) $sql .= " and rownum <= ".$param['limit'];

	return $sql;
}

#get nilai dari pengambilan
function getnilai($param){
	$sql = "select id_reg_pd,kls.id_kls,nilai_angka from kelas_mk kls 
			join pengambilan_mk pmk on kls.id_kelas_mk = pmk.id_kelas_mk 
			join mahasiswa mhs on mhs.id_mhs = pmk.id_mhs
			join pengguna pengg on mhs.id_pengguna = pengg.id_pengguna 
			where 1 = 1 and pengg.id_perguruan_tinggi = '".$param['id_pt']."'";

	if(!empty($param['id_semester'])) $sql .= " and kls.id_semester = '".$param['id_semester']."'";
	if(!empty($param['id_prodi'])) $sql .= " and kls.id_program_studi = '".$param['id_prodi']."'";
	if(!empty($param['id_mhs'])) $sql .= " and mhs.id_mhs = '".$param['id_mhs']."'";
	
	return $sql;
}

#get aktivitas kuliah mahasiswa tiap semester
function kuliah_mahasiswa($param){
	$sql = "select id_reg_pd,sum(KREDIT_SEMESTER) as sks_smt,--PENGAMBILAN_MK.ID_SEMESTER,
			case when sum(kredit_semester)=0 then 0 
					 else round((sum((case when standar_nilai.nilai_standar_nilai is null then 0 
										   else standar_nilai.nilai_standar_nilai end * kredit_semester)
					)/sum(kredit_semester)
			),2) end as ips, smt.thn_akademik_semester,smt.group_semester 
			from pengambilan_mk 
			join kurikulum_mk kmk on pengambilan_mk.id_kurikulum_mk = kmk.id_kurikulum_mk
			join semester smt on pengambilan_mk.id_semester = smt.id_semester
			join mahasiswa mhs on pengambilan_mk.id_mhs = mhs.id_mhs
			join pengguna pengg on mhs.id_pengguna = pengg.id_pengguna
			left join standar_nilai on pengambilan_mk.nilai_huruf = standar_nilai.nm_standar_nilai
			where 1 = 1 and pengg.id_perguruan_tinggi = '".$param['id_pt']."'";

			if(!empty($param['id_prodi'])) $sql .= " and mhs.id_program_studi = '".$param['id_prodi']."' ";
			if(!empty($param['id_semester'])) $sql .= " and pengambilan_mk.id_semester = '".$param['id_semester']."' ";

	$sql .= "group by id_reg_pd,pengambilan_mk.id_semester,smt.thn_akademik_semester,smt.group_semester order by id_reg_pd asc ";
	return $sql;
}

#get aktivitas kuliah mahasiswa tiap semester tanpa nilai
function kuliah_mahasiswa_tanpa_nilai($param){
	$sql = "select id_reg_pd from MAHASISWA mhs
			join pengguna pengg on mhs.id_pengguna = pengg.id_pengguna
			where pengg.id_perguruan_tinggi = '".$param['id_pt']."'"; 
			if(!empty($param['id_prodi'])) $sql .= " and mhs.ID_PROGRAM_STUDI = '".$param['id_prodi']."'";
			if(!empty($param['angkatan'])) $sql .= " and mhs.THN_ANGKATAN_MHS = '".$param['angkatan']."'";
	return $sql;
}

#get master mahasiswa
function getmahasiswa($param){
	$sql = "select mhs.nim_mhs,peng.nm_pengguna,kelamin_pengguna,nm_ibu_mhs,NM_KOTA,TO_CHAR(TGL_LAHIR_PENGGUNA, 'yyyy-mm-dd') as TGL_LAHIR_PENGGUNA,
			ID_AGAMA,ID_PERGURUAN_TINGGI 
			from mahasiswa mhs join pengguna peng on peng.id_pengguna = mhs.id_pengguna
			left join kota on mhs.LAHIR_KOTA_MHS = kota.ID_KOTA
			where peng.id_perguruan_tinggi = '".$param['id_pt']."' and peng.join_table = 3";
			if(!empty($param['id_prodi'])) $sql .= " and mhs.ID_PROGRAM_STUDI = '".$param['id_prodi']."'";
			if(!empty($param['angkatan'])) $sql .= " and mhs.THN_ANGKATAN_MHS = '".$param['angkatan']."'";
	return $sql;
}

#get master mahasiswa
function getmahasiswa_pt($param){
	$sql = "select THN_ANGKATAN_MHS,ps.id_sms_feeder,mhs.id_pd,mhs.nim_mhs,ID_PERGURUAN_TINGGI 
			from mahasiswa mhs join pengguna peng on peng.id_pengguna = mhs.id_pengguna
			join program_studi ps on ps.id_program_studi = mhs.id_program_studi
			left join kota on mhs.LAHIR_KOTA_MHS = kota.ID_KOTA
			where peng.id_perguruan_tinggi = '".$param['id_pt']."' and peng.join_table = 3 and id_pd is not null";
			if(!empty($param['id_prodi'])) $sql .= " and mhs.ID_PROGRAM_STUDI = '".$param['id_prodi']."'";
			if(!empty($param['angkatan'])) $sql .= " and mhs.THN_ANGKATAN_MHS = '".$param['angkatan']."'";
	return $sql;
}


function update_mk($data,$key){
	$sql = "update mata_kuliah set id_mk = '".$data."' where id_mata_kuliah = '".$key."'";
	buildquery_update($sql);
}

function update_kurikulum($data,$key){
	$sql = "update kurikulum_prodi set id_kurikulum_sp = '".$data."' where id_kurikulum_prodi = '".$key."'";
	buildquery_update($sql);
}

function update_kelas_kuliah($data,$key){
	$sql = "update kelas_mk set id_kls = '".$data."' where id_kelas_mk = '".$key."'";
	buildquery_update($sql);
}

function update_dosen($data,$key){
	$sql = "update dosen set id_reg_ptk = '".$data."' where id_dosen = (
			select id_dosen from dosen dsn join pengguna pgn on dsn.id_pengguna = pgn.id_pengguna 
			where id_perguruan_tinggi = '1' and nidn_dosen = '".$key."')";
	buildquery_update($sql);
}

function update_mahasiswa($data,$key){
	$sql = "update mahasiswa set id_reg_pd = '".$data."' where id_mhs = (
			select id_mhs from mahasiswa mhs join pengguna pgn on mhs.id_pengguna = pgn.id_pengguna 
			where id_perguruan_tinggi = '".$key['id_pt']."' and nim_mhs = '".$key['nim_mhs']."')";
	buildquery_update($sql);
}

function update_mahasiswa_idpd($data,$key){
	$sql = "update mahasiswa set id_pd = '".$data."' where id_mhs = (
			select id_mhs from mahasiswa mhs join pengguna pgn on mhs.id_pengguna = pgn.id_pengguna 
			where id_perguruan_tinggi = '".$key['id_pt']."' and nim_mhs = '".$key['nim_mhs']."')";
	buildquery_update($sql);
}



#### tambahaan untuk parameter query ####
function get_program_studi($param){
	$sql = "select ps.id_program_studi,jenjang.nm_jenjang||' '||ps.nm_program_studi as nm_program_studi from program_studi ps 
			join jenjang on jenjang.id_jenjang = ps.id_jenjang
			join fakultas fak on ps.id_fakultas = fak.id_fakultas
			where fak.id_perguruan_tinggi = '".$param['id_pt']."'";
	return $sql;
}



?>