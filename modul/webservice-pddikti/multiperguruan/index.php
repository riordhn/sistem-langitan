<!DOCTYPE html>
		<head>
			<title>FORM SYNCRONIZE</title>
			<!-- Latest compiled and minified CSS -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
			<!-- Optional theme -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
			<meta charset="UTF-8">
		</head>
		<body>

		<div class="container">
			<div class="col-lg-3">
				<ul class="nav nav-pills nav-stacked">
				  <li role="presentation" class="active"><a href="#">Home</a></li>
				  <li role="presentation"><a href="#">Mahasiswa</a></li>
				  <li role="presentation"><a href="#">Mahasiswa PT</a></li>
				  <li role="presentation"><a href="#">Mata Kuliah</a></li>
				  <li role="presentation"><a href="#">Kurikulum</a></li>
				  <li role="presentation"><a href="#">Mata Kuliah Kurikulum</a></li>
				  <li role="presentation"><a href="#">Kelas Kuliah</a></li>
				  <li role="presentation"><a href="#">Nilai</a></li>
				  <li role="presentation"><a href="#">Kuliah Mahasiswa</a></li>
				  <li role="presentation"><a href="#">Kuliah Mahasiswa Tanpa kelas</a></li>
				</ul>
			</div>
			<div class="col-lg-9">
			
			<div class="row panel">
				<div class="panel-default">
					<div class="page-header"><h2>Seting Konfigurasi</h2></div>
					<div class="panel-body">
						<form action="index.php" method="POST">
							 <div class="form-group">
							    <label for="exampleInputEmail1">Perguruan Tinggi</label>
							    <select class="form-control" name="id_pt" id="id_pt">
						    	  <option value="">--- Pilih Perguruan Tinggi ---</option>
								  <option value="1" <?php if(isset($_POST['id_pt']) && $_POST['id_pt'] == '1'){ ?> selected="selected" <?php } ?>>Universitas Maarif Hasyim latif</option>
								  <option value="8" <?php if(isset($_POST['id_pt']) && $_POST['id_pt'] == '8'){ ?> selected="selected" <?php } ?>>Universitas Nahdlatul Ulama Lampung</option>
								</select>
							  </div>
							  <div class="form-group" id="param_program_studi">
							  		<label for="id_prodi">Program Studi</label>
							    	<select class="form-control" name="id_prodi" id="id_prodi">
					    				 <option value="">--- Pilih Program Studi ---</option>
						    		</select>
							  </div>
							  <div class="form-group" id="semester">
							  		<label for="id_semester">Semester</label>
							    	<select class="form-control" name="id_semester" id="id_semester">
					    				 <option value="">--- Pilih Semester ---</option>
						    		</select>
							  </div>
							  <div class="form-group" id="kurikulum_prodi">
							  		<label for="id_kurikulum_prodi">Kurikulum</label>
							    	<select class="form-control" name="id_kurikulum_prodi" id="id_kurikulum_prodi">
					    				 <option value="">--- Pilih Kurikulum ---</option>
						    		</select>
							  </div>
							  <div class="form-group" id="angkatan">
							  		<label for="angkatan">Angkatan</label>
							    	<select class="form-control" name="angkatan" id="angkatan">
					    				 <option value="">--- Pilih Angkatan ---</option>
					    				 <option value="2015">2015</option>
					    				 <option value="2014">2014</option>
					    				 <option value="2013">2013</option>
						    		</select>
							  </div>
							  <div class="form-group">
							    <label for="exampleInputPassword1">Data Source</label>
							    <select class="form-control" name="table_request" id="table_request">
						    	  <option value="">Data Source From Langitan</option>
								  <option value="mahasiswa" <?php if(isset($table_request) && $table_request == 'mahasiswa'){ ?> selected="selected" <?php } ?>>Mahasiswa</option>
								  <option value="mahasiswa_pt" <?php if(isset($table_request) && $table_request == 'mahasiswa_pt'){ ?> selected="selected" <?php } ?>>Mahasiswa PT</option>
								  <option value="mata_kuliah" <?php if(isset($table_request) && $table_request == 'mata_kuliah'){ ?> selected="selected" <?php } ?>>Mata Kuliah</option>
								  <option value="kurikulum" <?php if(isset($table_request) && $table_request == 'kurikulum'){ ?> selected="selected" <?php } ?>>Kurikulum</option>
								  <option value="mata_kuliah_kurikulum" <?php if(isset($table_request) && $table_request == 'mata_kuliah_kurikulum'){ ?> selected="selected" <?php } ?>>Mata Kuliah Kurikulum</option>
								  <option value="kelas_kuliah" <?php if(isset($table_request) && $table_request == 'kelas_kuliah'){ ?> selected="selected" <?php } ?>>Kelas Kuliah</option>
								  <option value="nilai" <?php if(isset($table_request) && $table_request == 'nilai'){ ?> selected="selected" <?php } ?>>Nilai</option>
								  <option value="kuliah_mahasiswa" <?php if(isset($table_request) && $table_request == 'kuliah_mahasiswa'){ ?> selected="selected" <?php } ?>>Kuliah Mahasiswa</option>
								  <option value="kuliah_mahasiswa_tanpa" <?php if(isset($table_request) && $table_request == 'kuliah_mahasiswa_tanpa'){ ?> selected="selected" <?php } ?>>Kuliah Mahasiswa Tanpa Kelas</option>
								</select>
							  </div>

						      <div class="form-group" id="param_limit">
							    
							  </div>
							  <div class="form-group" id="param_semester">
							    
							  </div>
							  <div class="form-group">
							    <label for="exampleInputPassword1">Running On Database ?</label>
							  </div>
							  <div class="radio">
								  <label>
								    <input type="radio" name="database" id="optionsRadios1" value="sandbox" checked>
								    Sandbox (untuk proses cek ulang di feeder sebelum proses sinkronisasi)
								  </label>
							  </div>
							  <div class="radio">
							  <label>
							    <input type="radio" name="database" id="optionsRadios2" value="live">
							    Live (Untuk proses sinkronisasi ke forlap, Data sudah dianggap fix)
							  </label>
							  </div>
							  <button type="submit" class="btn btn-default">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		</div>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script type="text/javascript">

		$(document).ready(function(){

			
			$('#id_pt').change(function(){
				var id_pt = $('#id_pt').val();
				var id_prodi = $('#id_prodi').val();
				get_prodi(id_pt);
				get_semester(id_pt);
				get_kurikulum_prodi(id_pt,id_prodi);
			});

			$('#id_prodi').change(function(){
				var id_pt = $('#id_pt').val();
				var id_prodi = $('#id_prodi').val();
				get_kurikulum_prodi(id_pt,id_prodi);
			});


			/*
			$('#table_request').change(function(){
				var tb_value = $('#table_request').val();
				if(tb_value == 'kurikulum') $('#param_program_studi').show();
				else $('#param_program_studi').hide();
			});
*/
		});
		

		function get_kurikulum_prodi(id_pt,id_prodi){

			return $.ajax({
						url : 'get_param.php',
						method : 'POST',
						dataType : 'html',
						data: {'table' : 'get_kurikulum','id_prodi' : id_prodi},
						cache : false

					}).done(function(data){
							$('select[name="id_kurikulum_prodi"]').html(data);
							
					}).fail(function( jqXHR, textStatus ) {
					  	alert( "Request failed: " + textStatus );
					});

		}

		function get_prodi(id_pt){

			return $.ajax({
						url : 'get_param.php',
						method : 'POST',
						dataType : 'html',
						data: {'table' : 'get_prodi','id_pt' : id_pt},
						cache : false

					}).done(function(data){
							$('select[name="id_prodi"]').html(data);
							
					}).fail(function( jqXHR, textStatus ) {
					  	alert( "Request failed: " + textStatus );
					});

		}

		function get_semester(id_pt){
			return $.ajax({
						url : 'get_param.php',
						method : 'POST',
						dataType : 'html',
						data: {'table' : 'get_semester','id_pt' : id_pt},
						cache : false

					}).done(function(data){
							$('select[name="id_semester"]').html(data);
							
					}).fail(function( jqXHR, textStatus ) {
					  	alert( "Request failed: " + textStatus );
					});
		}
			
		</script>
		</body>
</html>

<?php
ini_set('display_errors', 1); 
set_time_limit(3600);
error_reporting (E_ALL & ~E_DEPRECATED);
#ini_set('error_reporting', E_STRICT);
include('exe_perguruantinggi.php');
include('getdata.php');
#echo phpinfo(-1); 


if($_SERVER['REQUEST_METHOD'] == 'POST'){
	
	if($_POST['id_pt'] == '' || $_POST['table_request'] == '' || $_POST['database'] == ''){
		echo "<div class='container'><div class='page-header'><div class='alert alert-danger'>Parameter Ada Yang Tidak Lengkap</h2></div></div></div>";
		exit();
	}

	$id_pt = $_POST['id_pt'];
	$table_request = $_POST['table_request'];
	$database = $_POST['database'];
	$token = make_feeder_token($database);

	if($table_request == 'mata_kuliah'){

		$dt_from_query = buildquery(getmatakuliah($_POST));
		$data_langitan_after_convert = convert_lang_feeder_mata_kuliah($dt_from_query);
		$data_langitan_cleaning = cleaning_field_mata_kuliah($data_langitan_after_convert,$token);

		foreach ($data_langitan_cleaning as $key => $value) {
			$ret = $token->InsertRecord($table_request,$data_langitan_cleaning[$key]);
			if($database == 'live'){
				update_mk($ret['result']['id_mk'],$dt_from_query[$key]['ID_MK']);
			}
			if($ret['error_code'] != 0 || $ret['result']['error_code'] != 0){
				echo '<pre>'.print_r($data_langitan_cleaning[$key]).' ---- '.$ret['result']['error_desc'],'</pre><br>';
			}
			
		}

	}elseif($table_request == 'kurikulum'){

		$dt_from_query = buildquery(getkurikulum($_POST));
		$data_langitan_after_convert = convert_lang_feeder_kurikulum($dt_from_query);
		$data_langitan_cleaning = cleaning_field_kurikulum($data_langitan_after_convert,$token);

		foreach ($data_langitan_cleaning as $key => $value) {
			$ret = $token->InsertRecord($table_request,$data_langitan_cleaning[$key]);
			if($ret['error_code'] == 0 && $database == 'live'){
				update_kurikulum($ret['result']['id_kurikulum_sp'],$dt_from_query[$key]['ID_KURIKULUM_PRODI']);
			}
		}

	}elseif($table_request == 'mata_kuliah_kurikulum'){

		$dt_from_query = buildquery(getkurikulummk($_POST));
		$data_langitan_after_convert = convert_lang_feeder_kurikulum_mk($dt_from_query);
		$data_langitan_cleaning = cleaning_field_kurikulum_mk($data_langitan_after_convert,$token);
		
		foreach ($data_langitan_cleaning as $key => $value) {
			$ret = $token->InsertRecord($table_request,$data_langitan_cleaning[$key]);
			if($ret['error_code'] != 0){
				echo '<pre>'.print_r($ret).'</pre><br>--------';
				echo '<pre>'.print_r($data_langitan_cleaning[$key]).'</pre>';
			}
		}
		
	
	}elseif($table_request == 'kelas_kuliah'){

		$dt_from_query = buildquery(getkelaskuliah($_POST));
		$data_langitan_after_convert = convert_lang_feeder_kelas_kuliah($dt_from_query);
		$data_langitan_cleaning = cleaning_field_kelas_kuliah($data_langitan_after_convert,$token);

		foreach ($data_langitan_cleaning as $key => $value) {
			$ret = $token->InsertRecord($table_request,$data_langitan_cleaning[$key]);
			if($ret['error_code'] == 0 && $database == 'live'){
				update_kelas_kuliah($ret['result']['id_kls'],$dt_from_query[$key]['ID_KELAS_MK']);
			}else{
				echo '<pre>'.print_r($ret).'</pre><br>--------';
				echo '<pre>'.print_r($data_langitan_cleaning[$key]).'</pre>';
			}
		}

	}else if($table_request == 'nilai'){

		$dt_from_query = buildquery(getnilai($_POST));
		$data_langitan_after_convert = convert_lang_feeder_nilai($dt_from_query);
		$data_langitan_cleaning = cleaning_field_nilai($data_langitan_after_convert,$token);
		
		foreach ($data_langitan_cleaning as $key => $value) {
			$ret = $token->InsertRecord($table_request,$data_langitan_cleaning[$key]);
			if($ret['error_code'] != 0 || $ret['result']['error_code'] != 0){
				echo '<pre>'.print_r($data_langitan_cleaning[$key]).' ---- '.$ret['result']['error_desc'],'</pre><br>';
			}
		}
	}else if($table_request == 'kuliah_mahasiswa'){

		$dt_from_query = buildquery(kuliah_mahasiswa($_POST));
		$data_langitan_after_convert = convert_lang_feeder_kuliah_mahasiswa($dt_from_query);
		$data_langitan_cleaning = cleaning_field_kuliah_mahasiswa($data_langitan_after_convert,$token);
		
		foreach ($data_langitan_cleaning as $key => $value) {
			$ret = $token->InsertRecord($table_request,$data_langitan_cleaning[$key]);
			if($ret['error_code'] != 0 || $ret['result']['error_code'] != 0){
				echo '<pre>'.print_r($data_langitan_cleaning[$key]).' ---- '.$ret['result']['error_desc'],'</pre><br>';
			}
		}
			
	}else if($table_request == 'kuliah_mahasiswa_tanpa'){
		$table_request = 'kuliah_mahasiswa';
		$dt_from_query = buildquery(kuliah_mahasiswa_tanpa_nilai($_POST));
		#$data_langitan_after_convert = convert_lang_feeder_kuliah_mahasiswa($dt_from_query);
		$data_langitan_cleaning = cleaning_field_kuliah_mahasiswa_tanpa_nilai($dt_from_query,'20151','A');
		print_r($data_langitan_cleaning); 
		foreach ($data_langitan_cleaning as $key => $value) {
			$ret = $token->InsertRecord($table_request,$data_langitan_cleaning[$key]);
			#print_r($ret);
			
			if($ret['error_code'] != 0 || $ret['result']['error_code'] != 0){
				echo '<pre>'.print_r($data_langitan_cleaning[$key]).' ---- '.$ret['result']['error_desc'],'</pre><br>';
			}
			
		}
			
	}elseif($table_request == 'mahasiswa') {

		$keyword = array();
		$dt_from_query = buildquery(getmahasiswa($_POST));
		$data_langitan_after_convert = convert_lang_feeder_mahasiswa($dt_from_query);
		$data_langitan_cleaning = cleaning_field_mahasiswa($data_langitan_after_convert,$token);
		foreach ($data_langitan_cleaning as $key => $value) {
			$ret = $token->InsertRecord($table_request,$data_langitan_cleaning[$key]);
			if($ret['error_code'] != 0 || $ret['result']['error_code'] != 0){
				echo '<pre>'.print_r($data_langitan_cleaning[$key]).' ---- '.$ret['result']['error_desc'],'</pre><br>';
			}else{
				$keyword['id_pt'] = $_POST['id_pt'];
				$keyword['nim_mhs'] = $dt_from_query[$key]['NIM_MHS'];
				if($database == 'live'){
					update_mahasiswa_idpd($ret['result']['id_pd'],$keyword);
				}
				
			}
			
		}
	
	}elseif($table_request == 'mahasiswa_pt') {

		$keyword = array();
		$dt_from_query = buildquery(getmahasiswa_pt($_POST));
		#print_r($dt_from_query); exit();
		$data_langitan_after_convert = convert_lang_feeder_mahasiswa_pt($dt_from_query);
		#print_r($data_langitan_after_convert);
		$data_langitan_cleaning = cleaning_field_mahasiswa_pt($data_langitan_after_convert,$token);
		print_r($data_langitan_after_convert); 

		print_r($data_langitan_cleaning); 

		#echo json_encode($data_langitan_cleaning);
		foreach ($data_langitan_cleaning as $key => $value) {
			$ret = $token->InsertRecord($table_request,$data_langitan_cleaning[$key]);
			if($ret['error_code'] != 0 || $ret['result']['error_code'] != 0){
				echo '<pre>'.print_r($data_langitan_cleaning[$key]).' ---- '.$ret['result']['error_desc'],'</pre><br>';
			}else{
				$keyword['id_pt'] = $_POST['id_pt'];
				$keyword['nim_mhs'] = $dt_from_query[$key]['NIM_MHS'];
				if($database == 'live'){
					update_mahasiswa($ret['result']['id_reg_pd'],$keyword);
				}
			}
			
		}
	
	}

	

}



?>