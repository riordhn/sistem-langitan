<?php
require_once('config.php');

$mode = isset($_GET['mode']) ? $_GET['mode'] : '';

$smarty->assign('bidang', array(
    '1' => 'Bidang Organisasi dan Kepemimpinan',
    '2' => 'Bidang Penalaran dan Keilmuan',
    '3' => 'Bidang Minat dan Bakat',
    '4' => 'Bidang Kepedulian Sosial'
));

$smarty->assign('kegiatan', array(
    '1' => 'Pengurus Organisasi',
    '2' => 'Anggota Aktif Organisasi',
    '3' => 'Mengikuti Pelatihan Kepemimpinan LKMM',
    '4' => 'Latihan Kepemimpinan lainnya',
    '5' => 'Panitia dalam suatu Kegiatan Kemahasiswaan'
));

$smarty->assign('tingkat', array(
    '1' => 'Internasional',
    '2' => 'Nasional',
    '3' => 'Universitas',
    '4' => 'Fakultas',
    '5' => 'Departemen',
    '6' => 'UKM'
));

$smarty->assign('jabatan', array(
    '1' => 'Ketua',
    '2' => 'Wakil Ketua',
    '3' => 'Sekretaris',
    '4' => 'Pengurus Inti Lain',
    '5' => 'Anggota Pengurus'
));

$smarty->assign('penilaian', array(
    '1' => 'Surat Keputusan',
    '2' => 'Sertifikat',
    '3' => 'Surat Penugasan',
    '4' => 'Dokumentasi'
));

if ($mode == 'preview')
{
    $smarty->display('skp-isi-preview.tpl');
}
else
{
    $smarty->display('skp-isi.tpl');
}
?>
