<?php

include "config.php";
include "../../modul/pendidikan/class/aucc.class.php";
$aucc = new aucc($db);
if (post('nim')) {

    $nim = str_replace("'", "''", post('nim'));
    if (strlen($nim) >= 8) {
        if ($aucc->detail_pembayaran($nim))
            $smarty->assign('data_pembayaran', $aucc->detail_pembayaran($nim));
        else
            $smarty->assign('data_kosong', 'Data Mahasiswa Masih Belum ada dalam database kami');
    }
}

$smarty->display('mahasiswa/cek-pembayaran/view.tpl');
?>
