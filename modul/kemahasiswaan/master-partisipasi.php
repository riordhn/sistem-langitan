<?php
require_once('config.php');

$mode = isset($_GET['mode']) ? $_GET['mode'] : '';

if ($mode == 'add')
    $smarty->display('master-partisipasi-add.tpl');
elseif ($mode == 'edit')
    $smarty->display('master-partisipasi-edit.tpl');
elseif ($mode == 'delete')
    $smarty->display('master-partisipasi-delete.tpl');
else
    $smarty->display('master-partisipasi.tpl');
?>
