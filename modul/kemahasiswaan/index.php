<?php
include('config.php');

$struktur_menu= array();

foreach ($user->MODULs as $data) {
    array_push($struktur_menu, array(
        'NM_MODUL' => $data['NM_MODUL'],
        'PAGE' => $data['PAGE'],
        'TITLE' => $data['TITLE'],
        'AKSES' => $data['AKSES'],
        'SUBMENU' => $db->QueryToArray("SELECT * FROM MENU WHERE ID_MODUL={$data['ID_MODUL']} ORDER BY URUTAN")
            )
    );
}
$smarty->assign('modul_set', $user->MODULs);
$smarty->assign('struktur_menu', $struktur_menu);
$smarty->assign('user', $user->NAMA_PENGGUNA);
$smarty->display("index.tpl");
?>
