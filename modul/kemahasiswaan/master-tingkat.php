<?php
require_once('config.php');

$mode = isset($_GET['mode']) ? $_GET['mode'] : '';

if ($mode == 'add')
    $smarty->display('master-tingkat-add.tpl');
elseif ($mode == 'edit')
    $smarty->display('master-tingkat-edit.tpl');
elseif ($mode == 'delete')
    $smarty->display('master-tingkat-delete.tpl');
else
    $smarty->display('master-tingkat.tpl');
?>
