<?php
include 'config.php';
include 'class/beasiswa.class.php';

$b = new beasiswa($db, $user->ID_ROLE);

$data_excel = $b->load_data_mahasiswa_beasiswa(get('fakultas'), get('prodi'), get('beasiswa'));
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #2b1212;
        text-transform:capitalize;
    }
    td{
        text-align: left;
    }
</style>
<table border='1' align='left' cellpadding='0' cellspacing='0' class='keuangan'>
    <thead>
        <tr>
            <td colspan="8" class="header_text">
                <?php
                echo 'LAPORAN KONTROL BEASISWA';
                ?>
            </td>
        </tr>
        <tr>
            <td class="header_text">NO</td>
            <td class="header_text">NIM</td>
            <td class="header_text">NAMA</td>
            <td class="header_text">PRODI</td>
            <td class="header_text">JALUR</td>
            <td class="header_text">STATUS AKADEMIK</td>
            <td class="header_text">BEASISWA</td>
            <td class="header_text">KETERANGAN</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($data_excel as $data) {
            echo "<tr>
                    <td>{$no}</td>
                    <td>'{$data['NIM_MHS']}</td>
                    <td>{$data['NM_PENGGUNA']}</td>
                    <td>{$data['NM_JENJANG']} {$data['NM_PROGRAM_STUDI']}</td>
                    <td>{$data['NM_JALUR']}</td>
                    <td>
                        IPS :{$data['IPS']}, 
                        IPK :{$data['IPK']}, 
                        TOTAL SKS :{$data['TOTAL_SKS']}
                    </td>    
                    <td>{$data['NM_BEASISWA']} {$data['PENYELENGGARA_BEASISWA']}  ({$data['TGL_MULAI']}-{$data['TGL_SELESAI']})</td>
                    <td>{$data['KETERANGAN']}</td>
                  </tr>  
                    ";
        }
        ?>
    </tbody>
</table>

<?php
echo '<b>Data Ini Di Ambil Pada Tanggal ' . date('d F Y  H:i:s') . '</b>';

$nm_file = "Mahasiswa-beasiswa_" . date('d-m-Y');
header("Content-disposition: attachment; filename=$nm_file.xls");
header("Content-type: Application/exe");
header("Content-Transfer-Encoding: binary");
ob_flush();
?>
