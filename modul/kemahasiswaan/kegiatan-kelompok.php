<?php
include 'config.php';

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_kelompok_kegiatan = post('id_kelompok_kegiatan');
        $nm_kelompok_kegiatan = post('nm_kelompok_kegiatan');
        
        $result = $db->Query("update kelompok_kegiatan set nm_kelompok_kegiatan = '{$nm_kelompok_kegiatan}'
        						where id_kelompok_kegiatan = '{$id_kelompok_kegiatan}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }
    if (post('mode') == 'add')
    {
        //$id_unit_kerja = post('id_unit_kerja');
        $nm_kelompok_kegiatan = post('nm_kelompok_kegiatan');
        
        $result = $db->Query("insert into kelompok_kegiatan (nm_kelompok_kegiatan, id_perguruan_tinggi) 
        						values ('{$nm_kelompok_kegiatan}', '{$id_pt}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }
    if (post('mode') == 'delete')
    {
        $id_kelompok_kegiatan = post('id_kelompok_kegiatan');
        
        $result = $db->Query("delete from kelompok_kegiatan where id_kelompok_kegiatan = '{$id_kelompok_kegiatan}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$kelompok_kegiatan_set = $db->QueryToArray("SELECT KELOMPOK_KEGIATAN.*, 
                                                            (SELECT SUM(ID_KELOMPOK_KEGIATAN) FROM KEGIATAN_1 k1 WHERE k1.ID_KELOMPOK_KEGIATAN = KELOMPOK_KEGIATAN.ID_KELOMPOK_KEGIATAN) AS JML_JOIN 
                                                        FROM KELOMPOK_KEGIATAN WHERE ID_PERGURUAN_TINGGI = '{$id_pt}'");
		$smarty->assignByRef('kelompok_kegiatan_set', $kelompok_kegiatan_set);
	}
	else if ($mode == 'edit' or $mode == 'delete')
	{
		$id_kelompok_kegiatan 	= (int)get('id_kelompok_kegiatan', '');

		$kelompok_kegiatan = $db->QueryToArray("
            select * 
            from kelompok_kegiatan 
            where id_kelompok_kegiatan = {$id_kelompok_kegiatan}");
        $smarty->assign('kelompok_kegiatan', $kelompok_kegiatan[0]);

	}
	else if($mode == 'add')
	{
		
	}
}

$smarty->display("kegiatan/kelompok/{$mode}.tpl");