<?php

include 'config.php';
include 'class/kemahasiswaan.class.php';
include '../ppmb/class/Penerimaan.class.php';
include '../keuangan/class/list_data.class.php';

$list = new list_data($db);
$siswa = new kemahasiswaan($db);
$penerimaan = new Penerimaan($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $smarty->assign('data_laporan_jaket', $siswa->load_rekapitulasi_ukur_jaket(get('fakultas'), get('penerimaan'), get('tgl_awal'), get('tgl_akhir')));
        $smarty->assign('data_laporan_mutz', $siswa->load_rekapitulasi_ukur_mutz(get('fakultas'), get('penerimaan'), get('tgl_awal'), get('tgl_akhir')));
    }
}

$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
$smarty->assign('data_tanggal', $siswa->load_tanggal_ukur(get('penerimaan')));
$smarty->assign('data_fakultas', $list->load_list_fakultas());
$smarty->display('kemahasiswaan-laporan.tpl');
?>
