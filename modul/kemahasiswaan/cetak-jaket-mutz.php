<?php
include('../../config.php');
include_once('../../tcpdf/config/lang/ind.php');
include_once('../../tcpdf/tcpdf.php');

$db->Query("select calon_mahasiswa_baru.ID_C_MHS, NM_C_MHS, NO_UJIAN, NM_PROGRAM_STUDI, NM_JENJANG, NM_FAKULTAS, 
					JAKET, MUTZ, alamat, NM_JALUR
				  from calon_mahasiswa_baru
				  join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
				  join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
				  join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
				  join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
				  join jalur on jalur.id_jalur = calon_mahasiswa_baru.id_jalur
				  where calon_mahasiswa_baru.ID_C_MHS = '$_GET[id]'");
$cmhs = $db->FetchAssoc();

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .header { font-size: 14pt; font-family: times; font-weight: bold; }
    .address { font-size: 12pt; font-family: serif; margin-top: 0px ; }
    td { font-size: 10pt; }
</style>
<table border="0" width="100%" align="left">
    <tr>
        <td width="20%"><img src="../../img/maba/logounair.png" width="50px" height="50px"/></td>
        <td width="50%" align="left"><span class="header">UNIVERSITAS AIRLANGGA</span><br/><span class="address">Tanda Bukti Pengukuran Jaket</span></td>
        <td width="30%"><strong>Untuk Petugas</strong></td>
    </tr>
</table>
<br/>
<table border="0" width="100%" cellpadding="2">
    <tr>
        <td width="20%">No Test</td>
        <td width="30%">: {no_ujian}</td>
        <td width="20%"></td>
        <td width="30%" ></td>
    </tr>
    <tr>
        <td>Nama</td>
        <td colspan="3">: {nama}</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td colspan="3">: {alamat}</td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>: {fakultas}</td>
        <td>Ukuran Jaket</td>
        <td>: {ukuran_jaket}</td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>: {program_studi}</td>
        <td>Ukuran Muts/Topi</td>
        <td>: {ukuran_muts}</td>
    </tr>
    <tr>
        <td>Jenjang</td>
        <td>: {jenjang}</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Jalur</td>
        <td>: {jalur}</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td colspan="2">
            <table border="0.5" cellpadding="5">
                <tr>
                    <td colspan="2" align="center">Jaket dan Mutz Diukur</td>
                </tr>
                <tr>
                    <td align="center">Tanggal<br/><br/>{tanggal}<br/></td>
                    <td align="center">Tanda Tangan Ybs,<br/><br/><br/></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p></p>
<hr/>
<p></p><p></p>
<table border="0" width="100%" align="left">
    <tr>
        <td width="20%"><img src="../../img/maba/logounair.png" width="50px" height="50px"/></td>
        <td width="50%" align="left"><span class="header">UNIVERSITAS AIRLANGGA</span><br/><span class="address">Tanda Bukti Pengambilan Jaket</span></td>
        <td width="30%"><strong>Untuk Mahasiswa</strong></td>
    </tr>
</table>
<p></p>
<table border="0" width="100%" cellpadding="2">
    <tr>
        <td width="20%">No Test</td>
        <td width="30%">: {no_ujian}</td>
        <td width="20%"></td>
        <td width="30%" ></td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>: {nama}</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td colspan="3">: {alamat}</td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>: {fakultas}</td>
        <td>Ukuran Jaket</td>
        <td>: <strong> {ukuran_jaket} </strong></td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>: {program_studi}</td>
        <td>Ukuran Muts/Topi</td>
        <td>: <strong> {ukuran_muts} </strong></td>
    </tr>
    <tr>
        <td>Jenjang</td>
        <td>: {jenjang}</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Jalur</td>
        <td>: {jalur}</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td colspan="2">
            <table border="0.5" cellpadding="5">
                <tr>
                    <td colspan="2" align="center">Jaket dan Mutz Diambil</td>
                </tr>
                <tr>
                    <td align="center">Tanggal<br/><br/><br/></td>
                    <td align="center">Tanda Tangan Ybs,<br/><br/><br/></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"><span class="adderess">CATATAN</span></td>
    </tr>
    <tr>
        <td width="5%">1.</td>
        <td width="95%">UKURAN TIDAK DAPAT DITUKAR</td>
    </tr>
    <tr>
        <td>2.</td>
        <td>TANDA BUKTI INI TIDAK BOLEH HILANG / TERTUKAR</td>
    </tr>
    <tr>
        <td>3.</td>
        <td>PENGAMBILAN JAKET ALMAMATER PROGRAM S1 DAN D3 TUNGGU PENGUNGUMAN DARI FAKULTAS</td>
    </tr>
    <tr>
        <td>4.</td>
        <td>PENGAMBILAN JAKET ALMAMATER PROGRAM S2 DAN S3 TUNGGU PENGUNGUMAN DARI FAKULTAS/PASCA SARJANA</td>
    </tr>
</table>
EOF;

function get_ukur_jaket($ukur) {
    switch ($ukur) {
        case '1':return 'S';
            break;
        case '2':return 'M';
            break;
        case '3':return 'L';
            break;
        case '4':return 'XL';
            break;
        case '5':return 'XXL';
            break;
        case '6':return 'XXXL';
            break;
        case '7':return 'Ukur';
            break;
    }
}

$html = str_replace('{nama}', $cmhs['NM_C_MHS'], $html);
$html = str_replace('{alamat}', $cmhs['ALAMAT'], $html);
$html = str_replace('{no_ujian}', $cmhs['NO_UJIAN'], $html);
$html = str_replace('{program_studi}', strtolower(ucwords($cmhs['NM_PROGRAM_STUDI'])), $html);
$html = str_replace('{fakultas}', strtoupper($cmhs['NM_FAKULTAS']), $html);
$html = str_replace('{jenjang}', $cmhs['NM_JENJANG'], $html);
$html = str_replace('{tanggal}', date('d-m-Y'), $html);
$html = str_replace('{jalur}', $cmhs['NM_JALUR'], $html);
$html = str_replace('{ukuran_jaket}', get_ukur_jaket($cmhs['JAKET']), $html);
$html = str_replace('{ukuran_muts}', $cmhs['MUTZ'], $html);



$pdf->writeHTML($html);

$pdf->Output($cmhs['NO_UJIAN'] . '.pdf', 'I');
?>
