<?php
require_once('config.php');

$mode = isset($_GET['mode']) ? $_GET['mode'] : '';

$smarty->assign('fakultas', array(
    'FST'   => 'Sains dan Teknologi',
    'FKP'   => 'Keperawatan',
    'FPK'   => 'Perikanan dan Kelautan'
));

$smarty->assign('prodi', array(
    'ALL'       => 'Semua Prodi',
    'FST-S1MTM' => 'S1 Matematika',
    'FST-S1SI'  => 'S1 Sistem Informasi',
    'FST-S1BIO' => 'S1 Biologi'
));

$smarty->assign('approval', array(
    'Y' => 'Y',
    'T' => 'T'
));

if ($mode == 'view')
{
    $smarty->display('skp-approval-view.tpl');
}
else
{
    $smarty->display('skp-approval.tpl');
}
?>
