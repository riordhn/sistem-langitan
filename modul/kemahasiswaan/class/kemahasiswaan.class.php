<?php

class kemahasiswaan {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    function load_tanggal_ukur($penerimaan) {
        return $this->db->QueryToArray("
            SELECT TO_CHAR(TGL_UKUR_JAKET) TGL_UKUR
            FROM AUCC.CALON_MAHASISWA_BARU 
            WHERE TGL_UKUR_JAKET IS NOT NULL
            AND ID_PENERIMAAN='{$penerimaan}'
            GROUP BY TO_CHAR(TGL_UKUR_JAKET)
            ORDER BY TGL_UKUR");
    }

    function load_rekapitulasi_ukur_jaket($fakultas, $penerimaan, $tgl_awal, $tgl_akhir) {
        if ($fakultas != '') {
            $query = "
                SELECT PS.NM_PROGRAM_STUDI,J.NM_JENJANG,CM.* 
                FROM AUCC.PROGRAM_STUDI PS
                JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                LEFT JOIN
                (
                SELECT CMB.ID_PROGRAM_STUDI,
                SUM(
                    CASE
                    WHEN CMD.JAKET=1
                    THEN 1
                    ELSE 0
                END) S,
                SUM(CASE
                    WHEN CMD.JAKET=2
                    THEN 1
                    ELSE 0
                END) M,
                SUM(CASE
                    WHEN CMD.JAKET=3
                    THEN 1
                    ELSE 0
                END) L,
                SUM(CASE
                    WHEN CMD.JAKET=4
                    THEN 1
                    ELSE 0
                END) XL,
                SUM(CASE
                    WHEN CMD.JAKET=5
                    THEN 1
                    ELSE 0
                END) XXL,
                SUM(CASE
                    WHEN CMD.JAKET=6
                    THEN 1
                    ELSE 0
                END) XXXL,
                SUM(CASE
                    WHEN CMD.JAKET=7
                    THEN 1
                    ELSE 0
                END) UKUR
                FROM AUCC.CALON_MAHASISWA_BARU CMB
                JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND TO_CHAR(CMB.TGL_UKUR_JAKET) BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}'
                GROUP BY CMB.ID_PROGRAM_STUDI
                ) CM ON CM.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                WHERE PS.ID_FAKULTAS='{$fakultas}'
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI
                ";
        } else {
            $query = "
                SELECT F.NM_FAKULTAS,CM.* FROM AUCC.FAKULTAS F
                LEFT JOIN
                (
                SELECT PS.ID_FAKULTAS,
                SUM(
                    CASE
                    WHEN CMD.JAKET=1
                    THEN 1
                    ELSE 0
                END) S,
                SUM(CASE
                    WHEN CMD.JAKET=2
                    THEN 1
                    ELSE 0
                END) M,
                SUM(CASE
                    WHEN CMD.JAKET=3
                    THEN 1
                    ELSE 0
                END) L,
                SUM(CASE
                    WHEN CMD.JAKET=4
                    THEN 1
                    ELSE 0
                END) XL,
                SUM(CASE
                    WHEN CMD.JAKET=5
                    THEN 1
                    ELSE 0
                END) XXL,
                SUM(CASE
                    WHEN CMD.JAKET=6
                    THEN 1
                    ELSE 0
                END) XXXL,
                SUM(CASE
                    WHEN CMD.JAKET=7
                    THEN 1
                    ELSE 0
                END) UKUR
                FROM AUCC.CALON_MAHASISWA_BARU CMB
                JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND TO_CHAR(CMB.TGL_UKUR_JAKET) BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}'
                GROUP BY PS.ID_FAKULTAS
                ) CM ON CM.ID_FAKULTAS=F.ID_FAKULTAS
                ";
        }
        return $this->db->QueryToArray($query);
    }
    
    function load_rekapitulasi_ukur_mutz($fakultas, $penerimaan, $tgl_awal, $tgl_akhir) {
        if ($fakultas != '') {
            $query = "
                SELECT PS.NM_PROGRAM_STUDI,J.NM_JENJANG,CM.* 
                FROM AUCC.PROGRAM_STUDI PS
                JOIN AUCC.JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
                LEFT JOIN
                (
                SELECT CMB.ID_PROGRAM_STUDI,
                SUM(
                    CASE
                    WHEN CMD.MUTZ=5
                    THEN 1
                    ELSE 0
                END) UKUR_5,
                SUM(CASE
                    WHEN CMD.MUTZ=6
                    THEN 1
                    ELSE 0
                END) UKUR_6,
                SUM(CASE
                    WHEN CMD.MUTZ=7
                    THEN 1
                    ELSE 0
                END) UKUR_7,
                SUM(CASE
                    WHEN CMD.MUTZ=8
                    THEN 1
                    ELSE 0
                END) UKUR_8,
                SUM(CASE
                    WHEN CMD.MUTZ=9
                    THEN 1
                    ELSE 0
                END) UKUR_9,
                SUM(CASE
                    WHEN CMD.MUTZ=10
                    THEN 1
                    ELSE 0
                END) UKUR_10,
                SUM(CASE
                    WHEN CMD.MUTZ=12
                    THEN 12
                    ELSE 0
                END) UKUR_12
                FROM AUCC.CALON_MAHASISWA_BARU CMB
                JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND TO_CHAR(CMB.TGL_UKUR_JAKET) BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}'
                GROUP BY CMB.ID_PROGRAM_STUDI
                ) CM ON CM.ID_PROGRAM_STUDI=PS.ID_PROGRAM_STUDI
                WHERE PS.ID_FAKULTAS='{$fakultas}'
                ORDER BY J.NM_JENJANG,PS.NM_PROGRAM_STUDI
                ";
        } else {
            $query = "
                SELECT F.NM_FAKULTAS,CM.* FROM AUCC.FAKULTAS F
                LEFT JOIN
                (
                SELECT PS.ID_FAKULTAS,
                SUM(
                    CASE
                    WHEN CMD.MUTZ=5
                    THEN 1
                    ELSE 0
                END) UKUR_5,
                SUM(CASE
                    WHEN CMD.MUTZ=6
                    THEN 1
                    ELSE 0
                END) UKUR_6,
                SUM(CASE
                    WHEN CMD.MUTZ=7
                    THEN 1
                    ELSE 0
                END) UKUR_7,
                SUM(CASE
                    WHEN CMD.MUTZ=8
                    THEN 1
                    ELSE 0
                END) UKUR_8,
                SUM(CASE
                    WHEN CMD.MUTZ=9
                    THEN 1
                    ELSE 0
                END) UKUR_9,
                SUM(CASE
                    WHEN CMD.MUTZ=10
                    THEN 1
                    ELSE 0
                END) UKUR_10,
                SUM(CASE
                    WHEN CMD.MUTZ=12
                    THEN 12
                    ELSE 0
                END) UKUR_12
                FROM AUCC.CALON_MAHASISWA_BARU CMB
                JOIN AUCC.CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS=CMB.ID_C_MHS
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=CMB.ID_PROGRAM_STUDI
                WHERE CMB.ID_PENERIMAAN='{$penerimaan}' AND TO_CHAR(CMB.TGL_UKUR_JAKET) BETWEEN '{$tgl_awal}' AND '{$tgl_akhir}'
                GROUP BY PS.ID_FAKULTAS
                ) CM ON CM.ID_FAKULTAS=F.ID_FAKULTAS
                ";
        }
        return $this->db->QueryToArray($query);
    }

}

?>
