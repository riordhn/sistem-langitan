<?php

$id_pt = $id_pt_user;

class beasiswa {

    public $db;
    public $role;

    function __construct($db, $role) {
        $this->db = $db;
        $this->role = $role;
    }

    //fungsi Kerjasama

    function load_jenis_beasiswa() {
        return $this->db->QueryToArray("SELECT * FROM JENIS_BEASISWA ORDER BY NM_JENIS_BEASISWA");
    }

    function load_group_beasiswa() {
        return $this->db->QueryToArray("SELECT GB.*,(SELECT COUNT(*) FROM BEASISWA WHERE ID_GROUP_BEASISWA=GB.ID_GROUP_BEASISWA) RELASI 
            FROM GROUP_BEASISWA GB 
            WHERE ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ORDER BY NM_GROUP");
    }

    function get_group_beasiswa($id) {
        $this->db->Query("SELECT * FROM GROUP_BEASISWA WHERE ID_GROUP_BEASISWA='{$id}'");
        return $this->db->FetchAssoc();
    }

    function add_group_beasiswa($nama) {
        $this->db->Query("INSERT INTO GROUP_BEASISWA (NM_GROUP, ID_PERGURUAN_TINGGI) 
                            VALUES ('{$nama}', '".getenv('ID_PT')."')");
    }

    function edit_group_beasiswa($id, $nama) {
        $this->db->Query("UPDATE GROUP_BEASISWA SET NM_GROUP='{$nama}' WHERE ID_GROUP_BEASISWA='{$id}'");
    }

    function delete_group_beasiswa($id) {
        $this->db->Query("DELETE FROM GROUP_BEASISWA WHERE ID_GROUP_BEASISWA='{$id}'");
    }
    
    function load_semester(){
        $query ="SELECT * FROM SEMESTER 
                    WHERE TIPE_SEMESTER='REG' 
                        AND ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."' 
                    ORDER BY THN_AKADEMIK_SEMESTER DESC,NM_SEMESTER DESC";
        return $this->db->QueryToArray($query);
    }

    function load_beasiswa($akses = 1) {
        if ($akses == 1) {
            return $this->db->QueryToArray("
            SELECT B.*,GB.NM_GROUP,JB.NM_JENIS_BEASISWA,(SELECT COUNT(*) FROM SEJARAH_BEASISWA WHERE ID_BEASISWA=B.ID_BEASISWA) RELASI
            FROM BEASISWA B 
            JOIN GROUP_BEASISWA GB ON GB.ID_GROUP_BEASISWA=B.ID_GROUP_BEASISWA
            LEFT JOIN JENIS_BEASISWA JB ON JB.ID_JENIS_BEASISWA=B.ID_JENIS_BEASISWA
            WHERE B.ID_ROLE_BEASISWA='{$this->role}' AND GB.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ORDER BY B.NM_BEASISWA");
        } else {
            return $this->db->QueryToArray("
            SELECT B.*,GB.NM_GROUP,JB.NM_JENIS_BEASISWA,(SELECT COUNT(*) FROM SEJARAH_BEASISWA WHERE ID_BEASISWA=B.ID_BEASISWA) RELASI
            FROM BEASISWA B 
            JOIN GROUP_BEASISWA GB ON GB.ID_GROUP_BEASISWA=B.ID_GROUP_BEASISWA
            LEFT JOIN JENIS_BEASISWA JB ON JB.ID_JENIS_BEASISWA=B.ID_JENIS_BEASISWA
            WHERE GB.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ORDER BY B.NM_BEASISWA");
        }
    }

    function load_beasiswa_fakultas() {
        return $this->db->QueryToArray("
            SELECT B.*,GB.NM_GROUP,JB.NM_JENIS_BEASISWA,(SELECT COUNT(*) FROM SEJARAH_BEASISWA WHERE ID_BEASISWA=B.ID_BEASISWA) RELASI
            FROM BEASISWA B 
            JOIN GROUP_BEASISWA GB ON GB.ID_GROUP_BEASISWA=B.ID_GROUP_BEASISWA
            LEFT JOIN JENIS_BEASISWA JB ON JB.ID_JENIS_BEASISWA=B.ID_JENIS_BEASISWA
            WHERE B.PUSAT_BEASISWA=0 AND GB.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ORDER BY B.NM_BEASISWA");
    }

    function get_beasiswa($id) {
        $this->db->Query("SELECT * FROM BEASISWA WHERE ID_BEASISWA='{$id}'");
        return $this->db->FetchAssoc();
    }

    function add_beasiswa($nama, $penyelenggara, $jenis, $group, $keterangan, $pusat) {
        $this->db->Query("
            INSERT INTO BEASISWA 
                (NM_BEASISWA,PENYELENGGARA_BEASISWA,ID_JENIS_BEASISWA,ID_GROUP_BEASISWA,KETERANGAN,PUSAT_BEASISWA,ID_ROLE_BEASISWA)
            VALUES 
                ('{$nama}','{$penyelenggara}','{$jenis}','{$group}','{$keterangan}','{$pusat}','{$this->role}')");
    }

    function edit_beasiswa($id, $nama, $penyelenggara, $jenis, $group, $keterangan, $pusat) {
        $this->db->Query("
            UPDATE BEASISWA
            SET
                NM_BEASISWA='{$nama}',
                PENYELENGGARA_BEASISWA='{$penyelenggara}',
                ID_JENIS_BEASISWA='{$jenis}',
                ID_GROUP_BEASISWA='{$group}',
                KETERANGAN='{$keterangan}',
                PUSAT_BEASISWA='{$pusat}'
            WHERE ID_BEASISWA='{$id}'");
    }

    function delete_beasiswa($id) {
        $this->db->Query("DELETE FROM BEASISWA WHERE ID_BEASISWA='{$id}'");
    }

    //fungsi upload mahasiswa beasiswa


    function get_mahasiswa($nim) {
        $this->db->Query("
            SELECT M.*,P.NM_PENGGUNA FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            WHERE M.NIM_MHS='{$nim}' AND P.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ");
        return $this->db->FetchAssoc();
    }

    function get_mahasiswa_beasiswa($nim) {
        $this->db->Query("
            SELECT B.*,M.NIM_MHS,M.ID_MHS,SB.*
            FROM SEJARAH_BEASISWA SB
            JOIN BEASISWA B ON SB.ID_BEASISWA=B.ID_BEASISWA
            JOIN MAHASISWA M ON M.ID_MHS=SB.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            WHERE M.NIM_MHS='{$nim}' AND P.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."' ");
        return $this->db->FetchAssoc();
    }

    function non_aktif_mahasiswa_beasiswa($id_mhs) {
        $this->db->Query("UPDATE SEJARAH_BEASISWA SET BEASISWA_AKTIF=0 WHERE ID_MHS='{$id_mhs}'");
    }

    function insert_mahasiswa_beasiswa($id_mhs, $id_beasiswa,$semester, $tgl_mulai, $tgl_selesai, $keterangan, $status = 1) {
        $this->db->Query("
                INSERT INTO SEJARAH_BEASISWA 
                    (ID_MHS,ID_BEASISWA,ID_SEMESTER,TGL_MULAI,TGL_SELESAI,KETERANGAN,BEASISWA_AKTIF) 
                VALUES 
                    ('{$id_mhs}','{$id_beasiswa}','{$semester}','{$tgl_mulai}','{$tgl_selesai}','{$keterangan}','{$status}')");
    }

    function get_last_id_sejarah_beasiswa($id_mhs) {
        return $this->db->QuerySingle("SELECT ID_SEJARAH_BEASISWA FROM SEJARAH_BEASISWA WHERE ID_MHS='{$id_mhs}' ORDER BY ID_SEJARAH_BEASISWA DESC");
    }

    function insert_detail_biaya_beasiswa($id_mhs, $id_biaya, $besar) {
        $last_id_sejarah = $this->get_last_id_sejarah_beasiswa($id_mhs);
        $this->db->Query("
                INSERT INTO DETAIL_BIAYA_BEASISWA 
                    (ID_SEJARAH_BEASISWA,ID_BIAYA,BESAR_BIAYA_BEASISWA) 
                VALUES 
                    ('{$last_id_sejarah}','{$id_biaya}','{$besar}')");
    }

    // FUngsi Input History Tiap Mahasiswa

    function load_biodata_mhs($nim) {
        $this->db->Query("
            SELECT M.*,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR
            FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND JM.ID_JALUR_AKTIF=1
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR=JM.ID_JALUR
            WHERE M.NIM_MHS='{$nim}' AND P.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ");
        return $this->db->FetchAssoc();
    }

    function get_sejarah_beasiswa($id) {
        $this->db->Query("
            SELECT * FROM SEJARAH_BEASISWA WHERE ID_SEJARAH_BEASISWA='{$id}'
            ");
        return $this->db->FetchAssoc();
    }

    function load_sejarah_beasiswa($nim) {
        return $this->db->QueryToArray("
            SELECT SB.*,B.NM_BEASISWA,B.PENYELENGGARA_BEASISWA,JB.NM_JENIS_BEASISWA
            FROM MAHASISWA M
            JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
            JOIN SEJARAH_BEASISWA SB ON SB.ID_MHS=M.ID_MHS
            JOIN BEASISWA B ON B.ID_BEASISWA=SB.ID_BEASISWA
            JOIN JENIS_BEASISWA JB ON JB.ID_JENIS_BEASISWA=B.ID_JENIS_BEASISWA
            WHERE M.NIM_MHS='{$nim}' AND P.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ORDER BY SB.TGL_MULAI
            ");
    }

    function update_sejarah_beasiswa($id, $beasiswa, $tgl_mulai, $tgl_selesai, $keterangan, $status) {
        $this->db->Query("
            UPDATE SEJARAH_BEASISWA
                SET
                    ID_BEASISWA='{$beasiswa}',
                    TGL_MULAI='{$tgl_mulai}',
                    TGL_SELESAI='{$tgl_selesai}',
                    KETERANGAN='{$keterangan}',
                    BEASISWA_AKTIF='{$status}'
            WHERE ID_SEJARAH_BEASISWA='{$id}'
            ");
    }

    function delete_sejarah_beasiswa($id) {
        $this->db->Query("DELETE SEJARAH_BEASISWA WHERE ID_SEJARAH_BEASISWA='{$id}'");
    }

    // FUngsi Menu Laporan
    function load_data_mahasiswa_beasiswa($fakultas, $prodi, $beasiswa) {
        $data_rekap = array();
        $q_fakultas = $fakultas != "" ? "AND PS.ID_FAKULTAS='{$fakultas}'" : "";
        $q_prodi = $prodi != "" ? "AND PS.ID_PROGRAM_STUDI='{$prodi}'" : "";
        $data_query = $this->db->QueryToArray("
            SELECT M.ID_MHS,M.NIM_MHS,P.NM_PENGGUNA,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,J.NM_JALUR,SB.*,B.NM_BEASISWA,B.PENYELENGGARA_BEASISWA
                ,GB.NM_GROUP,JB.NM_JENIS_BEASISWA
            FROM SEJARAH_BEASISWA SB
            JOIN BEASISWA B ON SB.ID_BEASISWA=B.ID_BEASISWA AND SB.BEASISWA_AKTIF=1
            JOIN GROUP_BEASISWA GB ON GB.ID_GROUP_BEASISWA=B.ID_GROUP_BEASISWA
            JOIN JENIS_BEASISWA JB ON JB.ID_JENIS_BEASISWA=B.ID_JENIS_BEASISWA
            JOIN MAHASISWA M ON SB.ID_MHS=M.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI=M.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
            JOIN FAKULTAS F ON F.ID_FAKULTAS=PS.ID_FAKULTAS
            LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS=M.ID_MHS AND JM.ID_JALUR_AKTIF=1
            LEFT JOIN JALUR J ON J.ID_JALUR=JM.ID_JALUR
            WHERE SB.ID_BEASISWA='{$beasiswa}' {$q_fakultas} {$q_prodi}");
        foreach ($data_query as $q) {
            $mhs_status = $this->load_mhs_status($q['ID_MHS']);
            array_push($data_rekap, array_merge($q, $mhs_status));
        }
        return $data_rekap;
    }

    function load_mhs_status($id_mhs) {
        $id_semester_kemarin = $this->get_semester_kemarin();
        $ips = $this->get_ips_mhs($id_mhs, $id_semester_kemarin);
        $mhs_status = $this->get_mhs_status($id_mhs);
        $this->db->Query("
        SELECT P.NM_PENGGUNA,M.ID_MHS,M.NIM_MHS,M.THN_ANGKATAN_MHS,PS.NM_PROGRAM_STUDI,'{$ips}' AS IPS,'{$mhs_status['IPK']}' AS IPK,'{$mhs_status['SKSTOTAL']}' AS TOTAL_SKS,J.NM_JENJANG FROM MAHASISWA M
            LEFT JOIN PENGGUNA P ON M.ID_PENGGUNA  = P.ID_PENGGUNA
            LEFT JOIN PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        WHERE M.ID_MHS ='{$id_mhs}'");
        return $this->db->FetchAssoc();
    }

    function get_ips_mhs($id_mhs, $id_semester) {
        $this->db->Query("
            select id_mhs,case when sum(bobot*kredit_semester)=0 then 0 else 
            round((sum((bobot*kredit_semester))/sum(kredit_semester)),2) end as ips,
            case when id_fakultas=7 then sum(sksreal) else sum(kredit_semester) end as sks_sem 
            from 
            (select id_mhs,id_kurikulum_mk,id_fakultas,kredit_semester, sksreal,min(nilai_huruf) as nilai, max(bobot) as bobot from
            (select a.id_mhs,ps.id_fakultas,a.id_kurikulum_mk, 
            case when (a.nilai_huruf = 'E' or a.nilai_huruf is null) 
            and d.status_mkta in (1,2) then 0
            else d.kredit_semester end as kredit_semester,d.kredit_semester as sksreal,
            case when a.nilai_huruf is null then 'E' else a.nilai_huruf end as nilai_huruf,
            case when e.nilai_standar_nilai is null then 0 else e.nilai_standar_nilai end as bobot
            from pengambilan_mk a
            left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk 
            left join standar_nilai e on a.nilai_huruf=e.nm_standar_nilai
            left join mahasiswa m on a.id_mhs=m.id_mhs
            left join program_studi ps on m.id_program_studi=ps.id_program_studi
            left join semester s on a.id_semester=s.id_semester
            where group_semester||thn_akademik_semester in
            (select group_semester||thn_akademik_semester from semester where id_Semester='" . $id_semester . "')
            and tipe_semester in ('UP','REG','RD') 
            and a.status_apv_pengambilan_mk='1' and m.id_mhs='" . $id_mhs . "' and a.status_hapus=0 and a.flagnilai='1'
            and a.status_pengambilan_mk !=0
            )
            group by id_mhs, id_kurikulum_mk, id_fakultas, kredit_semester, sksreal
            )
            group by id_mhs,id_fakultas
            ");
        $data = $this->db->FetchAssoc();
        return $data['IPS'];
    }

    function get_mhs_status($id_mhs) {
        $this->db->Query("SELECT PS.ID_FAKULTAS FROM MAHASISWA M JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI WHERE M.ID_MHS='{$id_mhs}'");
        $id_fakultas_mhs = $this->db->FetchAssoc();
        // ngulang -> nilai terakhir yg berlaku
        if ($id_fakultas_mhs['ID_FAKULTAS'] == 10) {
            $sql = "select a.id_mhs,sum(a.kredit_semester) skstotal,
                    round(sum(a.kredit_semester*(case a.nilai_huruf 
                    when 'A' then 4 
                    when 'AB' then 3.5 
                    when 'B' then 3
                    when 'BC' then 2.5
                    when 'C' then 2
                    when 'D' then 1
                    end))/sum(a.kredit_semester),2) IPK
                    from
                    (
                    select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
                    select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by sm.thn_akademik_semester desc,sm.nm_semester desc) rangking
                    from pengambilan_mk a 
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    left join semester sm on sm.id_semester=a.id_semester
                    where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.flagnilai=1 and a.id_semester is not null
                    ) a
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    where rangking=1 and id_mhs='{$id_mhs}'
                    ) a
                    left join mahasiswa b on a.id_mhs=b.id_mhs
                    left join program_studi f on b.id_program_studi=f.id_program_studi
                    where a.id_mhs='{$id_mhs}'
                    group by a.id_mhs order by a.id_mhs";
            $this->db->Query($sql);
            $mhs_status = $this->db->FetchAssoc();
        } else {
            // ipk versi lukman, ngulang diambil nilai terbaik.
            $sql = "select a.id_mhs,sum(a.kredit_semester) skstotal,
                    round(sum(a.kredit_semester*(case a.nilai_huruf 
                    when 'A' then 4 
                    when 'AB' then 3.5 
                    when 'B' then 3
                    when 'BC' then 2.5
                    when 'C' then 2
                    when 'D' then 1
                    end))/sum(a.kredit_semester),2) IPK
                    from
                    (
                    select a.id_mhs,e.nm_mata_kuliah,d.kredit_semester kredit_semester,a.nilai_huruf from (
                    select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
                    from pengambilan_mk a 
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.id_semester is not null 
                    ) a
                    left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
                    left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
                    where rangking=1 and id_mhs='{$id_mhs}'
                    ) a
                    left join mahasiswa b on a.id_mhs=b.id_mhs
                    left join program_studi f on b.id_program_studi=f.id_program_studi
                    where a.id_mhs='{$id_mhs}'
                    group by a.id_mhs order by a.id_mhs";
            $this->db->Query($sql);
            $mhs_status = $this->db->FetchAssoc();
        }
        return $mhs_status;
    }

    function get_semester_kemarin() {
        $this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER='True' AND ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'");
        $semester_aktif = $this->db->FetchAssoc();
        if ($semester_aktif['NM_SEMESTER'] == 'Genap') {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER!='Genap' AND THN_AKADEMIK_SEMESTER='{$semester_aktif['THN_AKADEMIK_SEMESTER']}' AND NM_SEMESTER='Ganjil'");
            $semester_kemarin = $this->db->FetchAssoc();
        } else {
            $this->db->Query("SELECT * FROM SEMESTER WHERE NM_SEMESTER='Genap' AND THN_AKADEMIK_SEMESTER=('{$semester_aktif['THN_AKADEMIK_SEMESTER']}'-1)");
            $semester_kemarin = $this->db->FetchAssoc();
        }
        return $semester_kemarin['ID_SEMESTER'];
    }

    // Pengumuman Beasiswa
    function load_pengumuman_beasiswa() {
        return $this->db->QueryToArray("
            SELECT PB.*,B.PUSAT_BEASISWA,B.NM_BEASISWA,B.PENYELENGGARA_BEASISWA,JB.NM_JENIS_BEASISWA
            FROM PENGUMUMAN_BEASISWA PB
            JOIN BEASISWA B ON B.ID_BEASISWA=PB.ID_BEASISWA
            LEFT JOIN JENIS_BEASISWA JB ON JB.ID_JENIS_BEASISWA=B.ID_JENIS_BEASISWA
            LEFT JOIN GROUP_BEASISWA GB ON GB.ID_GROUP_BEASISWA=B.ID_GROUP_BEASISWA
            WHERE TO_CHAR(PB.BATAS_AKHIR,'YYYY')=TO_CHAR(SYSDATE,'YYYY') 
                AND GB.ID_PERGURUAN_TINGGI = '".getenv('ID_PT')."'
            ORDER BY PB.BATAS_AKHIR DESC,B.NM_BEASISWA
            ");
    }

    function get_pengumuman_beasiswa($id) {
        $this->db->Query("SELECT * FROM PENGUMUMAN_BEASISWA WHERE ID_PENGUMUMAN='{$id}'");
        return $this->db->FetchAssoc();
    }

    function tambah_pengumuman_beasiswa($beasiswa, $besar, $syarat, $keterangan, $batas) {
        $this->db->Query("
            INSERT INTO PENGUMUMAN_BEASISWA
                (ID_BEASISWA,BESAR_BEASISWA,SYARAT,KETERANGAN,BATAS_AKHIR)
            VALUES
                ('{$beasiswa}','{$besar}','{$syarat}','{$keterangan}','{$batas}')
            ");
    }

    function update_pengumuman_beasiswa($id, $beasiswa, $besar, $syarat, $keterangan, $batas) {
        $this->db->Query("
            UPDATE PENGUMUMAN_BEASISWA
                SET
                    ID_BEASISWA='{$beasiswa}',
                    BESAR_BEASISWA='{$besar}',
                    SYARAT='{$syarat}',
                    KETERANGAN='{$keterangan}',
                    BATAS_AKHIR='{$batas}'
            WHERE ID_PENGUMUMAN='{$id}'
            ");
    }

    function delete_pengumuman_beasiswa($id) {
        $this->db->Query("
            DELETE FROM PENGUMUMAN_BEASISWA WHERE ID_PENGUMUMAN='{$id}'
            ");
    }

}

?>
