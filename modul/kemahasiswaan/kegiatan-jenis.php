<?php
include 'config.php';

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_jenis_kegiatan = post('id_jenis_kegiatan');
        $nm_jenis_kegiatan = post('nm_jenis_kegiatan');
        $id_kelompok_kegiatan = post('id_kelompok_kegiatan');
        
        $result = $db->Query("update kegiatan_1 set nm_kegiatan_1 = '{$nm_jenis_kegiatan}', id_kelompok_kegiatan = '{$id_kelompok_kegiatan}'
        						where id_kegiatan_1 = '{$id_jenis_kegiatan}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }
    if (post('mode') == 'add')
    {
        $nm_jenis_kegiatan = post('nm_jenis_kegiatan');
        $id_kelompok_kegiatan = post('id_kelompok_kegiatan');
        
        $result = $db->Query("insert into kegiatan_1 (nm_kegiatan_1, id_kelompok_kegiatan) 
        						values ('{$nm_jenis_kegiatan}', '{$id_kelompok_kegiatan}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }
    if (post('mode') == 'delete')
    {
        $id_jenis_kegiatan = post('id_jenis_kegiatan');
        
        $result = $db->Query("delete from kegiatan_1 where id_kegiatan_1 = '{$id_jenis_kegiatan}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$jenis_kegiatan_set = $db->QueryToArray("SELECT k1.ID_KEGIATAN_1, k1.NM_KEGIATAN_1, kk.NM_KELOMPOK_KEGIATAN,
                                                    (SELECT SUM(ID_KEGIATAN_1) FROM KEGIATAN_2 k2 WHERE k2.ID_KEGIATAN_1 = k1.ID_KEGIATAN_1) AS JML_JOIN  
                                                        FROM KEGIATAN_1 k1
                                                        JOIN KELOMPOK_KEGIATAN kk ON kk.ID_KELOMPOK_KEGIATAN = k1.ID_KELOMPOK_KEGIATAN 
                                                        WHERE kk.ID_PERGURUAN_TINGGI = '{$id_pt}'
                                                        ORDER BY kk.ID_KELOMPOK_KEGIATAN, kk.NM_KELOMPOK_KEGIATAN, k1.NM_KEGIATAN_1");
		$smarty->assignByRef('jenis_kegiatan_set', $jenis_kegiatan_set);
	}
	else if ($mode == 'edit' or $mode == 'delete')
	{
		$id_jenis_kegiatan 	= (int)get('id_jenis_kegiatan', '');

		$jenis_kegiatan = $db->QueryToArray("
            select * 
            from kegiatan_1 k1
            join kelompok_kegiatan kk on kk.id_kelompok_kegiatan = k1.id_kelompok_kegiatan
            where id_kegiatan_1 = {$id_jenis_kegiatan}");
        $smarty->assign('jenis_kegiatan', $jenis_kegiatan[0]);

        $kelompok_kegiatan_set = $db->QueryToArray("
            select id_kelompok_kegiatan, nm_kelompok_kegiatan 
            from kelompok_kegiatan
            where id_perguruan_tinggi = {$id_pt}
            order by id_kelompok_kegiatan");
        $smarty->assign('kelompok_kegiatan_set', $kelompok_kegiatan_set);

	}
	else if($mode == 'add')
	{
		$kelompok_kegiatan_set = $db->QueryToArray("
            select id_kelompok_kegiatan, nm_kelompok_kegiatan 
            from kelompok_kegiatan
            where id_perguruan_tinggi = {$id_pt}
            order by id_kelompok_kegiatan");
        $smarty->assign('kelompok_kegiatan_set', $kelompok_kegiatan_set);
	}
}

$smarty->display("kegiatan/jenis/{$mode}.tpl");