<?php

include 'config.php';
include '../../modul/pendidikan/class/report_mahasiswa.class.php';

$rm = new report_mahasiswa($db);

if (isset($_GET['x'])) {
    $smarty->assign('data_mahasiswa', $rm->get_hasil_cari_mahasiswa(get('x')));
} else if (isset($_GET['detail'])) {
    $smarty->assign('biodata_mahasiswa', $rm->get_biodata_mahasiswa(get('y')));

}
$smarty->display("mahasiswa/cari/view.tpl");
?>
