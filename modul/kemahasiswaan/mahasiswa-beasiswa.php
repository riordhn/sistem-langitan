<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';
include 'class/beasiswa.class.php';
include '../keuangan/class/paging.class.php';

$list = new list_data($db);
$b = new beasiswa($db, $user->ID_ROLE);

if (isset($_GET['mode'])) {
    $smarty->assign('data_mahasiswa_kerjasama', $b->load_data_mahasiswa_beasiswa(get('fakultas'), get('prodi'), get('beasiswa')));
    $smarty->assign('data_fakultas_one', $list->get_fakultas(get('fakultas')));
    $smarty->assign('data_prodi_one', $list->get_prodi(get('prodi')));
    $smarty->assign('data_prodi', $list->load_list_prodi(get('fakultas')));
}

$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt_user));
$smarty->assign('data_beasiswa', $b->load_beasiswa());
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_jalur', $list->load_list_jalur());
$smarty->display('beasiswa/mahasiswa-beasiswa.tpl');
?>
