<?php
require_once('config.php');

$mode = isset($_GET['mode']) ? $_GET['mode'] : '';

if ($mode == 'add')
    $smarty->display('master-kegiatan-add.tpl');
elseif ($mode == 'edit')
    $smarty->display('master-kegiatan-edit.tpl');
elseif ($mode == 'delete')
    $smarty->display('master-kegiatan-delete.tpl');
else
    $smarty->display('master-kegiatan.tpl');
?>
