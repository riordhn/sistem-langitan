<div class="center_title_bar">Pengukuran Jaket Mahasiswa</div>
<table>
    <tr>
        <th colspan="2">PARAMETER</th>
    </tr>
    <tr>
        <td>
            <label>Nomor Ujian :</label>
        </td>
        <td>
            <form action="kemahasiswaan-jaket-mutz.php" method="post">
                <input type="search" name="no_ujian" />
                <input class="button" type="submit" value="Tampil" />
            </form>
        </td>
    </tr>
</table>


{if isset($cmhs)}
    <form action="kemahasiswaan-jaket-mutz.php" method="post" id="ukur">
        <input type="hidden" name="mode" value="ukur" />
        <input type="hidden" name="no_ujian" value="{$cmhs.NO_UJIAN}" />
        <input type="hidden" name="id_c_mhs" value="{$cmhs.ID_C_MHS}" />
        <table>
            <tr>
                <th colspan="2">DATA CALON MAHASISWA</th>
            </tr>
            <tr>
                <td>Nama</td>
                <td>{$cmhs.NM_C_MHS}</td>
            </tr>
            <tr>
                <td>No Ujian</td>
                <td>{$cmhs.NO_UJIAN}</td>
            </tr>
            <tr>
                <td>Program Studi</td>
                <td>{$cmhs.NM_PRODI}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td style="font-size:15px;">{if $cmhs.JAKET==''}Belum di ukur{else}<b>Sudah di ukur</b>{/if}</td>
            </tr>
            {if $cmhs.JAKET!=''}
                <tr>
                    <td>Cetak Pengukuran Jaket Muts</td>
                    <td><a  href="cetak-jaket-mutz.php?id={$cmhs.ID_C_MHS}" target="_blank" class="button disable-ajax" >cetak</a></td>
                </tr>    
            {/if}
            <tr>
                <td>Ukuran Jaket</td>
                <td>
                    <label><input type="radio" name="jaket" value="1" {if $cmhs.JAKET==1}checked="true"{/if} class="required" />S</label></br>
                    <label><input type="radio" name="jaket" value="2" {if $cmhs.JAKET==2}checked="true"{/if} />M</label></br>
                    <label><input type="radio" name="jaket" value="3" {if $cmhs.JAKET==3}checked="true"{/if} />L</label></br>
                    <label><input type="radio" name="jaket" value="4" {if $cmhs.JAKET==4}checked="true"{/if} />XL</label></br>
                    <label><input type="radio" name="jaket" value="5" {if $cmhs.JAKET==5}checked="true"{/if} />XXL</label></br>
                    <label><input type="radio" name="jaket" value="6" {if $cmhs.JAKET==6}checked="true"{/if} />XXXL</label></br>
                    <label><input type="radio" name="jaket" value="7" {if $cmhs.JAKET==7}checked="true"{/if} />Ukur</label>
                    <br/>
                    <label for="jaket" class="error" style="display: none;">Pilih ukuran jaket</label>
                </td>
            </tr>
            <tr>
                <td>Ukuran Muts</td>
                <td>
                    <label><input type="radio" name="mutz" value="5" {if $cmhs.MUTZ==5}checked="true"{/if} class="required"/>5</label><br/>
                    <label><input type="radio" name="mutz" value="6" {if $cmhs.MUTZ==6}checked="true"{/if} />6</label><br/>
                    <label><input type="radio" name="mutz" value="7" {if $cmhs.MUTZ==7}checked="true"{/if} />7</label><br/>
                    <label><input type="radio" name="mutz" value="8" {if $cmhs.MUTZ==8}checked="true"{/if} />8</label><br/>
                    <label><input type="radio" name="mutz" value="9" {if $cmhs.MUTZ==9}checked="true"{/if} />9</label><br/>
                    <label><input type="radio" name="mutz" value="10" {if $cmhs.MUTZ==10}checked="true"{/if} />10</label><br/>
                    <label><input type="radio" name="mutz" value="12" {if $cmhs.MUTZ==12}checked="true"{/if} />12</label><br/>
                    <br/>
                    <label for="mutz" class="error" style="display: none;">Pilih ukuran mutz</label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center">
                    <input class="button" type="submit" value="Proses" />
                </td>
            </tr>
        </table>    
    </form>
    
    
    <form action="pendaftaran-jaket.php" method="post">
	<table>
    	<tr>
        	<td>Reset Pengukuran Jaket Muts</td>
            <td>
            	<input type="submit" value="Reset" />
                <input type="hidden" value="{$smarty.post.no_ujian}" name="no_ujian" />
                <input type="hidden" value="reset" name="mode" />
            </td>
        </tr>
    </table>
    </form>


    {literal}<script language="javascript">
    $('#ukur').validate();
        </script>
    {/literal}
{/if}