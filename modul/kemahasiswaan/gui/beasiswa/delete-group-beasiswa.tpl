<div class="center_title_bar">Delete - Master Kerjasama / Beasiswa</div>
<form action="group-beasiswa.php" method="post" id="edit_group">
    <table class="ui-widget-content" style="width: 90%">
        <tr class="ui-widget-header">
            <th colspan="2" class="header-coloumn">Edit Group Kerjasama / Beasiswa</th>
        </tr>
        <tr>
            <td>Nama Grup</td>
            <td>{$data_group_beasiswa.NM_GROUP}</td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <input type="hidden" name="mode" value="delete_group"/>
                <input type="hidden" name="id" value="{$data_group_beasiswa.ID_GROUP_BEASISWA}"/>
                Apakah anda yakin menghapus item ini?<br/>
                <a class="disable-ajax button" onclick="history.back(-1)">Kembali</a>
                <input type="submit" class="button" onclick="" value="Hapus"/>
            </td>
        </tr>
    </table>
</form>