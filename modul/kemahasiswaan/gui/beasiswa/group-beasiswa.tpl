<div class="center_title_bar">Master Kerjasama / Beasiswa</div>
<table class="ui-widget-content" style="width: 60%">
    <tr>
        <th class="center" colspan="3">Grup Kerjasama / Beasiswa</th>
    </tr>
    <tr>
        <th>NO</th>
        <th>NAMA GROUP</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_group_beasiswa as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data.NM_GROUP}</td>
            <td class="center">
                <a class="button" href="group-beasiswa.php?mode=edit_group&id={$data.ID_GROUP_BEASISWA}">Edit</a>
                {if $data.RELASI==0}
                    <a class="button" href="group-beasiswa.php?mode=delete_group&id={$data.ID_GROUP_BEASISWA}">Delete</a>
                {/if}
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="3" class="data-kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="3" class="center">
            <a class="button" href="group-beasiswa.php?mode=add_group">Tambah</a>
        </td>
    </tr>
</table>

