<div class="center_title_bar">Master Kerjasama / Beasiswa</div>
<table class="ui-widget-content" style="width: 90%">
    <tr>
        <th class="center" colspan="8">Kerjasama / Beasiswa</th>
    </tr>
    <tr>
        <th>NO</th>
        <th>Nama Kerjasama / Beasiswa</th>
        <th>Penyelenggara</th>
        <th>Jenis</th>
        <th>Group Beasiswa</th>
        <th>Keterangan</th>
        <th>Pusat Beasiswa</th>
        <th>Operasi</th>
    </tr>
    {foreach $data_beasiswa as $data}
        <tr>
            <td>{$data@index+1}</td>
            <td>{$data.NM_BEASISWA}</td>
            <td>{$data.PENYELENGGARA_BEASISWA}</td>
            <td>{$data.NM_JENIS_BEASISWA}</td>
            <td>{$data.NM_GROUP}</td>
            <td>{$data.KETERANGAN}</td>
            <td>
                {if $data.PUSAT_BEASISWA==1}
                    Universitas
                {else}
                    Fakultas
                {/if}
            </td>
            <td>
                <a class="button" href="beasiswa.php?mode=edit_beasiswa&id={$data.ID_BEASISWA}">Edit</a>
                {if $data.RELASI==0}
                    <a class="button" href="beasiswa.php?mode=delete_beasiswa&id={$data.ID_BEASISWA}">Delete</a>
                {/if}
            </td>
        </tr>
    {foreachelse}
        <tr>
            <td colspan="8" class="data-kosong">Data Masih Kosong</td>
        </tr>
    {/foreach}
    <tr>
        <td colspan="8" class="center">
            <a class="button" href="beasiswa.php?mode=add_beasiswa">Tambah</a>
        </td>
    </tr>
</table>

