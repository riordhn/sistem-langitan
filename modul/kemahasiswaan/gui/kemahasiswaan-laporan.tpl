<div class="center_title_bar">Data Rekap Jaket dan Mutz</div>
<form method="get" action="kemahasiswaan-laporan-jaket-mutz.php">
    <table style="width:98%">
        <tr>
            <th colspan="4">Rekapitulasi Ukur Jaket dan Mutz</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select name="fakultas" id="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>            
            <td>Penerimaan</td>
            <td>
                <select id="penerimaan" name="penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                {if ($p2.ID_JENJANG==1||$p2.ID_JENJANG==5)}
                                    <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                                {/if}
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Tanggal Awal</td>
            <td>
                <select class="required" id="tgl_awal" name="tgl_awal">
                    <option value="">Pilih Tanggal</option>
                    {foreach $data_tanggal as $tgl}
                        <option value="{$tgl.TGL_UKUR}" {if $smarty.get.tgl_awal==$tgl.TGL_UKUR}selected="true"{/if}>{$tgl.TGL_UKUR}</option>
                    {/foreach}
                </select>
            </td>
            <td>Tanggal Akhir</td>
            <td>
                <select class="required" id="tgl_akhir" name="tgl_akhir">
                    <option value="">Pilih Tanggal</option>
                    {foreach $data_tanggal as $tgl}
                        <option value="{$tgl.TGL_UKUR}" {if $smarty.get.tgl_akhir==$tgl.TGL_UKUR}selected="true"{/if}>{$tgl.TGL_UKUR}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="center">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="button" value="Tampilkan"/>
            </td>
        </tr>
    </table>
</form>
{if isset($data_laporan_jaket)&&isset($data_laporan_mutz)}
    <table style="width: 98%">
        <caption>HASIL PENGUKURAN JAKET</caption>
        <tr>
            <th colspan="10" class="center">Rekapitulasi Ukur Jaket</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>
                {if $smarty.get.fakultas!=''}
                    PROGRAM STUDI
                {else}
                    FAKULTAS
                {/if}
            </th>
            <th>S</th>
            <th>M</th>
            <th>L</th>
            <th>XL</th>
            <th>XXL</th>
            <th>XXXL</th>
            <th>Ukur</th>
            <th>TOTAL</th>
        </tr>
        {$total_s=0}
        {$total_m=0}
        {$total_l=0}
        {$total_xl=0}
        {$total_xxl=0}
        {$total_xxxl=0}
        {$total_ukur=0}
        {foreach $data_laporan_jaket as $lap}
            {$total=$lap.S+$lap.M+$lap.L+$lap.XL+$lap.XXL+$lap.XXXL+$lap.UKUR}            
            <tr>
                <td>{$lap@index+1}</td>
                <td>
                    {if $smarty.get.fakultas!=''}
                        {$lap.NM_JENJANG} {$lap.NM_PROGRAM_STUDI}
                    {else}
                        {$lap.NM_FAKULTAS}
                    {/if}
                </td>
                <td>{number_format($lap.S)}</td>
                <td>{number_format($lap.M)}</td>
                <td>{number_format($lap.L)}</td>
                <td>{number_format($lap.XL)}</td>
                <td>{number_format($lap.XXL)}</td>
                <td>{number_format($lap.XXXL)}</td>
                <td>{number_format($lap.UKUR)}</td>
                <td>{number_format($total)}</td>
            </tr>
            {$total_s=$total_s+$lap.S}
            {$total_m=$total_m+$lap.M}
            {$total_l=$total_l+$lap.L}
            {$total_xl=$total_xl+$lap.XL}
            {$total_xxl=$total_xxl+$lap.XXL}
            {$total_xxxl=$total_xxxl+$lap.XXXL}
            {$total_ukur=$total_ukur+$lap.UKUR}
        {foreachelse}
            <tr>
                <td colspan="10" class="data-kosong">Data Kosong</td>
            </tr>
        {/foreach}
        <tr class="total">
            <td colspan="2">TOTAL</td>
            <td>{number_format($total_s)}</td>
            <td>{number_format($total_m)}</td>
            <td>{number_format($total_l)}</td>
            <td>{number_format($total_xl)}</td>
            <td>{number_format($total_xxl)}</td>
            <td>{number_format($total_xxxl)}</td>
            <td>{number_format($total_ukur)}</td>
            <td>{number_format($total_s+$total_m+$total_l+$total_xl+$total_xxl+$total_xxxl+$total_ukur)}</td>
        </tr>
    </table>
    <p></p>        
    <table style="width: 98%">
        <caption>HASIL PENGUKURAN MUTZ</caption>
        <tr>
            <th colspan="10" class="center">Rekapitulasi Ukur MUTZ</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>
                {if $smarty.get.fakultas!=''}
                    PROGRAM STUDI
                {else}
                    FAKULTAS
                {/if}
            </th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>12</th>
            <th>TOTAL</th>
        </tr>
        {$total_5=0}
        {$total_6=0}
        {$total_7=0}
        {$total_8=0}
        {$total_9=0}
        {$total_10=0}
        {$total_12=0}
        {foreach $data_laporan_mutz as $lap}
            {$total=$lap.UKUR_5+$lap.UKUR_6+$lap.UKUR_7+$lap.UKUR_8+$lap.UKUR_9+$lap.UKUR_10+$lap.UKUR_12}            
            <tr>
                <td>{$lap@index+1}</td>
                <td>
                    {if $smarty.get.fakultas!=''}
                        {$lap.NM_JENJANG} {$lap.NM_PROGRAM_STUDI}
                    {else}
                        {$lap.NM_FAKULTAS}
                    {/if}
                </td>
                <td>{number_format($lap.UKUR_5)}</td>
                <td>{number_format($lap.UKUR_6)}</td>
                <td>{number_format($lap.UKUR_7)}</td>
                <td>{number_format($lap.UKUR_8)}</td>
                <td>{number_format($lap.UKUR_9)}</td>
                <td>{number_format($lap.UKUR_10)}</td>
                <td>{number_format($lap.UKUR_12)}</td>
                <td>{number_format($total)}</td>
            </tr>
            {$total_5=$total_5+$lap.UKUR_5}
            {$total_6=$total_6+$lap.UKUR_6}
            {$total_7=$total_7+$lap.UKUR_7}
            {$total_8=$total_8+$lap.UKUR_8}
            {$total_9=$total_9+$lap.UKUR_9}
            {$total_10=$total_10+$lap.UKUR_10}
            {$total_12=$total_12+$lap.UKUR_12}
        {foreachelse}
            <tr>
                <td colspan="10" class="data-kosong">Data Kosong</td>
            </tr>
        {/foreach}
        <tr class="total">
            <td colspan="2">TOTAL</td>
            <td>{number_format($total_5)}</td>
            <td>{number_format($total_6)}</td>
            <td>{number_format($total_7)}</td>
            <td>{number_format($total_8)}</td>
            <td>{number_format($total_9)}</td>
            <td>{number_format($total_10)}</td>
            <td>{number_format($total_12)}</td>
            <td>{number_format($total_5+$total_6+$total_7+$total_8+$total_9+$total_10+$total_12)}</td>
        </tr>
    </table>
{/if}
{literal}
    <script>
            $('#penerimaan').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getTanggal.php',
                            data:'id_penerimaan='+$(this).val(),
                            success:function(data){
                                    $('#tgl_awal').html(data);
                                    $('#tgl_akhir').html(data);
                            }                    
                    })
            });
            $('form').validate();
    </script>
{/literal}