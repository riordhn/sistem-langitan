<div class="center_title_bar">Approval SKP</div>
<table>
    <tbody>
        <tr>
            <td><label>Fakultas :</label></td>
            <td>{html_options name=fakultas options=$fakultas}
                <button onclick="$('#tabel-skp').hide();$('#tabel-skp').fadeIn('slow');">Tampilkan</button></td>
        </tr>
        <tr>
            <td><label>Prodi :</label></td>
            <td>{html_options name=prodi options=$prodi}
                <button onclick="$('#tabel-skp').hide();$('#tabel-skp').fadeIn('slow');">Tampilkan</button></td>
        </tr>
    </tbody>
</table>

<table style="width: 100%;" id="tabel-skp">
    <thead>
        <tr>
            <th style="width: 50px">Tanggal</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Keterangan</th>
            <th>SKP</th>
            <th style="width: 70px;">Approve</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>03/03/2011</td>
            <td>080916062</td>
            <td>Mokhammad Fathoni</td>
            <td><a href="skp-approval.php?mode=view&id=080916062&view=1">Olimpiade Matematika 2011 - Anggota Pengurus</a></td>
            <td><input type="text" size="2" maxlength="3" /></td>
            <td><label><input name="approval_080916062_1" value="Y" type="radio">Y</label>
                <label><input name="approval_080916062_1" value="T" type="radio">T</label></td>
        </tr>
        <tr>
            <td>03/03/2011</td>
            <td>080916062</td>
            <td>Mokhammad Fathoni</td>
            <td><a href="skp-approval.php?mode=view&id=080916062&view=1">Lomba Voli Unair 2011 - Peserta</a></td>
            <td><input type="text" size="2" maxlength="3" /></td>
            <td><label><input name="approval_080916062_2" value="Y" type="radio">Y</label>
                <label><input name="approval_080916062_2" value="T" type="radio">T</label></td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6"><button>Submit</button></td>
        </tr>
    </tfoot>
</table>