<div class="center_title_bar">Master Kegiatan</div>
<table style="width: 70%">
    <thead>
        <tr>
            <th>Kegiatan</th>
            <th style="width: 70px">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <tr class="row1">
            <td>Kegiatan Bidang Organisasi dan Kepemimpinan</td>
            <td><a href="master-kegiatan.php?mode=edit">Edit</a> | <a href="master-kegiatan.php?mode=delete">Hapus</a></td>
        </tr>
        <tr class="row2">
            <td>Kegiatan Bidang Penalaran dan Keilmuan</td>
            <td><a href="master-kegiatan.php?mode=edit">Edit</a> | <a href="master-kegiatan.php?mode=delete">Hapus</a></td>
        </tr>
        <tr class="row1">
            <td>Kegiatan Bidang Minat dan Bakat</td>
            <td><a href="master-kegiatan.php?mode=edit">Edit</a> | <a href="master-kegiatan.php?mode=delete">Hapus</a></td>
        </tr>
        <tr class="row2">
            <td>Kegiatan Bidang Kepedulian Sosial</td>
            <td><a href="master-kegiatan.php?mode=edit">Edit</a> | <a href="master-kegiatan.php?mode=delete">Hapus</a></td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2"><a href="master-kegiatan.php?mode=add">Tambah</a></td>
        </tr>
    </tfoot>
</table>