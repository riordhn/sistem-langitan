<div class="center_title_bar">ID Jenis Kegiatan : {$jenis_kegiatan.ID_KEGIATAN_1}</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="kegiatan-jenis.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_jenis_kegiatan" value="{$jenis_kegiatan.ID_KEGIATAN_1}" />
<!--<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="2">Detail Jenis Kegiatan</th>
    </tr>
    <tr>
        <td>ID</td>
        <td>{$jenis_kegiatan.ID_KEGIATAN_1}</td>
    </tr>
    <tr>
        <td>Kelompok Kegiatan</td>
        <td>
            <select name="id_kelompok_kegiatan">
                <option value="0">-- Tidak Ada --</option>
            {foreach $kelompok_kegiatan_set as $kk}
                <option value="{$kk.ID_KELOMPOK_KEGIATAN}" {if $kk.ID_KELOMPOK_KEGIATAN == $jenis_kegiatan.ID_KELOMPOK_KEGIATAN}selected="selected"{/if}>{$kk.NM_KELOMPOK_KEGIATAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Nama Jenis Kegiatan</td>
        <td><input class="form-control" id="nm_jenis_kegiatan" type="text" name="nm_jenis_kegiatan" value="{$jenis_kegiatan.NM_KEGIATAN_1}" size="75" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

<a href="kegiatan-jenis.php">Kembali</a>