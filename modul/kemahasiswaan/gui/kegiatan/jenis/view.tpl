<div class="center_title_bar">Jenis Kegiatan</div>

{if isset($jenis_kegiatan_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Kelompok Kegiatan</th>
				<th>Nama Jenis Kelompok</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $jenis_kegiatan_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td><strong>{$uk.NM_KELOMPOK_KEGIATAN}</strong></td>
					<td>{$uk.NM_KEGIATAN_1}</td>
					<td class="center">
						<a href="kegiatan-jenis.php?mode=edit&id_jenis_kegiatan={$uk.ID_KEGIATAN_1}"><b style="color: blue">Edit</b></a>
						{if $uk.JML_JOIN == 0}
							<a href="kegiatan-jenis.php?mode=delete&id_jenis_kegiatan={$uk.ID_KEGIATAN_1}"><b style="color: red">Delete</b></a>
						{/if}
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="9" class="center">
					<a href="kegiatan-jenis.php?mode=add"><b style="color: red">Tambah Jenis Kegiatan</b></a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}