<div class="center_title_bar">Tambah Jenis Kegiatan</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="kegiatan-jenis.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="add" />
<!--<input type="hidden" name="id_role" value="{$smarty.get.id_role}" />
<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="2">Detail Jenis Kegiatan</th>
    </tr>
    <tr>
        <td>Kelompok Kegiatan</td>
        <td>
            <select name="id_kelompok_kegiatan">
                <option value="0">-- Tidak Ada --</option>
            {foreach $kelompok_kegiatan_set as $kk}
                <option value="{$kk.ID_KELOMPOK_KEGIATAN}">{$kk.NM_KELOMPOK_KEGIATAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Nama Jenis Kegiatan</td>
        <td><input class="form-control" id="nm_jenis_kegiatan" type="text" name="nm_jenis_kegiatan" value="" size="75" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

<a href="kegiatan-jenis.php">Kembali</a>