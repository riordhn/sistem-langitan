<div class="center_title_bar">Kelompok Kegiatan</div>

{if isset($kelompok_kegiatan_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Kelompok Kegiatan</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $kelompok_kegiatan_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td><strong>{$uk.NM_KELOMPOK_KEGIATAN}</strong></td>
					<td class="center">
						<a href="kegiatan-kelompok.php?mode=edit&id_kelompok_kegiatan={$uk.ID_KELOMPOK_KEGIATAN}"><b style="color: blue">Edit</b></a>
						{if $uk.JML_JOIN == 0}
							<a href="kegiatan-kelompok.php?mode=delete&id_kelompok_kegiatan={$uk.ID_KELOMPOK_KEGIATAN}"><b style="color: red">Delete</b></a>
						{/if}
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="9" class="center">
					<a href="kegiatan-kelompok.php?mode=add"><b style="color: red">Tambah Kelompok Kegiatan</b></a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}