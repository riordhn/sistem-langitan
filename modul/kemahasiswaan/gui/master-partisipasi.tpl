<div class="center_title_bar">Master Partisipasi</div>

<table style="width: 70%">
    <thead>
        <tr>
            <th>Partisipasi / Jabatan / Prestasi</th>
            <th style="width: 70px">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <tr class="row1">
            <td>Ketua</td>
            <td><a href="master-partisipasi.php?mode=edit">Edit</a> | <a href="master-partisipasi.php?mode=delete">Hapus</a></td>
        </tr>
        <tr class="row2">
            <td>Wakil Ketua</td>
            <td><a href="master-partisipasi.php?mode=edit">Edit</a> | <a href="master-partisipasi.php?mode=delete">Hapus</a></td>
        </tr>
        <tr class="row1">
            <td>Sekretaris</td>
            <td><a href="master-partisipasi.php?mode=edit">Edit</a> | <a href="master-partisipasi.php?mode=delete">Hapus</a></td>
        </tr>
        <tr class="row2">
            <td>Pengurus Inti Lain</td>
            <td><a href="master-partisipasi.php?mode=edit">Edit</a> | <a href="master-partisipasi.php?mode=delete">Hapus</a></td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2"><a href="master-partisipasi.php?mode=add">Tambah</a></td>
        </tr>
    </tfoot>
</table>