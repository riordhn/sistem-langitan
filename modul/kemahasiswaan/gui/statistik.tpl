<div class="center_title_bar">Statistik</div>

<table>
    <tbody>
        <tr>
            <td><label>Fakultas</label></td>
            <td>{html_options name="fakultas" options=$fakultas}</td>
        </tr>
    </tbody>
</table>

<div id="chartdiv" align="center"> FusionCharts. </div>

{literal}
<script>
    var chart = new FusionCharts("../../fusionchart/Charts/MSColumn2D.swf", "ChartId", "689", "450", "0", "0");
    chart.setDataURL("gui/statistik.xml");
    chart.render("chartdiv");
</script>
{/literal}