<div class="center_title_bar">Isi SKP - Preview</div>
<table>
    <caption>Mahasiswa</caption>
    <tbody>
        <tr>
            <td>NIM : </td>
            <td>080916062</td>
        </tr>
        <tr>
            <td>Nama :</td>
            <td>Sani Imam P</td>
        </tr>
        <tr>
            <td>Jurusan :</td>
            <td>D3 Sistem Informasi</td>
        </tr>
    </tbody>
</table>
<table>
    <caption>Satuan Kredit Prestasi</caption>
    <tbody>
        <tr>
            <td>Nama Kegiatan :</td>
            <td>Lomba Robotika </td>
        </tr>
        <tr>
            <td>Bidang :</td>
            <td>Bidang Organisasi dan Kepemimpinan</td>
        </tr>
        <tr>
            <td>Kegiatan :</td>
            <td>Pengurus Organisasi</td>
        </tr>
        <tr>
            <td>Tingkat :</td>
            <td>Universitas</td>
        </tr>
        <tr>
            <td>Jabatan :</td>
            <td>Anggota</td>
        </tr>
        <tr>
            <td>Dasar Penilaian :</td>
            <td>Sertifikat</td>
        </tr>
        <tr>
            <td>Bobot SKP :</td>
            <td>20</td>
        </tr>
        <tr>
            <td>File Upload :</td>
            <td>sertifikat_lomba.jpg (456kb)</td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2">
                <a href="skp-isi.php?mode=add">Kembali</a> |
                <a href="skp-isi.php">Simpan</a>
            </td>
        </tr>
    </tfoot>
</table>