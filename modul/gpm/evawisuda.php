<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';
include '../ppm/class/evaluasi.class.php';

$list = new list_data($db);
$eva = new evaluasi($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $smarty->assign('data_evaluasi', $eva->load_laporan_evaluasi_wisuda(get('fakultas'), get('semester')));
        $smarty->assign('data_rata_fakultas', $eva->load_laporan_rerata_fakultas_evaluasi_wisuda(get('fakultas'), get('semester')));
    }
}
$smarty->assign('id_program_studi', $user->ID_PROGRAM_STUDI);
$smarty->assign('id_fakultas',$user->ID_FAKULTAS);
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt_user));
$smarty->display('evawisuda.tpl');
?>
