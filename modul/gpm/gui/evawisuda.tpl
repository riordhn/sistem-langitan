<div class="center_title_bar">Laporan Evaluasi Wisuda</div>
<form method="get" id="report_form" action="evawisuda.php">
    <table class="ui-widget-content" style="width: 900px">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select name="fakultas" id="fakultas">
                    {foreach $data_fakultas as $data}
                        {if $data.ID_FAKULTAS==$id_fakultas}
                            <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
            <td>Semester</td>
            <td>
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$smarty.get.semester}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="center">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_evaluasi)}
    <table class="ui-widget-content" style="width: 99%">
        <tr class="ui-widget-header">
            <th colspan="33">Hasil Evaluasi Wisuda</th>
        </tr>
        <tr class="ui-widget-header">
            <th style="vertical-align: middle" rowspan="2">NO</th>
            <th style="vertical-align: middle" rowspan="2">PROGRAM STUDI</th>
            <th style="vertical-align: middle" rowspan="2">Jumlah Responden</th>
            <th colspan="28">Kualitas institusi dalam penyediaan fasilitas akademik dan kemahasiswaan</th>
            <th style="vertical-align: middle" rowspan="2">Rerata Prodi</th>
            <th style="vertical-align: middle" rowspan="2">Rerata Persen Prodi</th>
        </tr>
        <tr class="ui-widget-header">
            <th>Keterampilan analisis</th>
            <th>Keterampilan komunikasi tertulis</th>
            <th>Ketrampilan TIK</th>
            <th>Ketrampilan komunikasi lisan</th>
            <th>Pengelolaan & pengembangan diri</th>
            <th>Keterampilan kerjasama</th>
            <th>Pemberian motivasi</th>
            <th>Mendorong partisipasi mahasiswa</th>
            <th>Menyampaikan materi dengan jelas</th>
            <th>Umpan balik yang konstruktif</th>
            <th>Pembimbingan tugas akhir yang konstruktif</th>
            <th>Pembimbingan akademik yang konstruktif</th>
            <th>Akses perpustakaan</th>
            <th>Sumber belajar di perpustakaan</th>
            <th>Sumber belajar di ruang baca</th>
            <th>Pemutakhiran sumber belajar perpustakaan</th>
            <th>Pemutakhiran sumber belajar ruang baca</th>
            <th>Akses informasi pendidikan</th>
            <th>Fasilitas teknologi informasi</th>
            <th>Unsur pendukung pembelajaran</th>
            <th>Jumlah mahasiswa dalam kelas</th>
            <th>Fasilitas dan media pembelajaran</th>
            <th>Pelayanan administrasi akademik fakultas/th>
            <th>Pelayanan administrasi akademik universitas</th>
            <th>Pelayanan administrasi keuangan</th>
            <th>Kegiatan ekstrakurikuler</th>
            <th>Pengelolaa kegiatan mahasiswa</th>
            <th>Layanan untuk pencarian kerja</th>
        </tr>
        {foreach $data_evaluasi as $eva}
            {if $data.ID_PROGRAM_STUDI==$id_program_studi}
                <tr>
                    <td>{$eva@index+1}</td>
                    <td>{$eva.NM_JENJANG} {$eva.NM_PROGRAM_STUDI}</td>
                    <td>{$eva.JUMLAH_RESPONDEN}</td>
                    {foreach $eva.DATA_HASIL as $hasil}
                        <td>{round($hasil.A1,2)}</td>
                        <td>{round($hasil.A2,2)}</td>
                        <td>{round($hasil.A3,2)}</td>
                        <td>{round($hasil.A4,2)}</td>
                        <td>{round($hasil.A5,2)}</td>
                        <td>{round($hasil.A6,2)}</td>
                        <td>{round($hasil.A7,2)}</td>
                        <td>{round($hasil.A8,2)}</td>
                        <td>{round($hasil.A9,2)}</td>
                        <td>{round($hasil.A10,2)}</td>
                        <td>{round($hasil.A11,2)}</td>
                        <td>{round($hasil.A12,2)}</td>
                        <td>{round($hasil.A13,2)}</td>
                        <td>{round($hasil.A14,2)}</td>
                        <td>{round($hasil.A15,2)}</td>
                        <td>{round($hasil.A16,2)}</td>
                        <td>{round($hasil.A17,2)}</td>
                        <td>{round($hasil.A18,2)}</td>
                        <td>{round($hasil.A19,2)}</td>
                        <td>{round($hasil.A20,2)}</td>
                        <td>{round($hasil.A21,2)}</td>
                        <td>{round($hasil.A22,2)}</td>
                        <td>{round($hasil.A23,2)}</td>
                        <td>{round($hasil.A24,2)}</td>
                        <td>{round($hasil.A25,2)}</td>
                        <td>{round($hasil.A26,2)}</td>
                        <td>{round($hasil.A27,2)}</td>
                        <td>{round($hasil.A28,2)}</td>
                        <td>{round($hasil.RERATA_PRODI,2)}</td>
                        <td>{round($hasil.RERATA_PERSEN_PRODI,2)} %</td>
                    {/foreach}
                </tr>
            {/if}
        {foreachelse}
            <tr>
                <td colspan="33" class="kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        {if $data_rata_fakultas!=''}
            <tr>
                <td colspan="33" class="total center">
                    Rerata Fakultas : {round($data_rata_fakultas.RERATA,2)} <br/>
                    Rerata Persen Fakultas : {round($data_rata_fakultas.RERATA_PERSEN,2)} % <br/>
                </td>
            </tr>
        {/if}
    </table>
{/if}

{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}