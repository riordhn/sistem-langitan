<div class="center_title_bar">Laporan Evaluasi Perwalian</div>
<form method="get" id="report_form" action="evawali.php">
    <table class="ui-widget-content">
        <tr class="ui-widget-header">
            <th colspan="4" class="header-coloumn">Parameter</th>
        </tr>
        <tr>
            <td style="width: 10%">Fakultas</td>
            <td style="width: 20%">
                <select name="fakultas" id="fakultas">
                    {foreach $data_fakultas as $data}
                        {if $data.ID_FAKULTAS==$id_fakultas}
                            {if $data.ID_FAKULTAS==$id_fakultas}
                                <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS==$smarty.get.fakultas}selected="true"{/if}>{$data.NM_FAKULTAS}</option>
                            {/if}
                        {/if}
                    {/foreach}
                </select>
            </td>
            <td style="width: 15%">Program Studi</td>
            <td>
                <select name="prodi" id="prodi">
                    {foreach $data_prodi as $data}
                        {if $data.ID_PROGRAM_STUDI==$id_program_studi}
                            <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$smarty.get.prodi}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}  ({$data.GELAR})</option>
                        {/if}
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Semester</td>
            <td colspan="3">
                <select name="semester">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$smarty.get.semester}selected="true"{/if}>{$data.NM_SEMESTER} ( {$data.TAHUN_AJARAN} )</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="center">
                <input type="hidden" name="mode" value="tampil" />
                <input type="submit" class="ui-button ui-corner-all ui-state-active" style="padding:5px;cursor:pointer;" value="Tampilkan"/>
            </td>
        </tr>

    </table>
</form>
{if isset($data_evaluasi)}
    <table class="ui-widget-content" style="width: 99%">
        <tr class="ui-widget-header">
            <th colspan="{count($header_aspek)+8}">Hasil Evaluasi Perwalian</th>
        </tr>
        <tr class="ui-widget-header">
            <th style="vertical-align: middle" rowspan="2">NO</th>
            <th style="vertical-align: middle" rowspan="2">Nama Dosen</th>
            <th style="vertical-align: middle" rowspan="2">Prodi</th>
            <th style="vertical-align: middle" rowspan="2">Departemen</th>
            <th style="vertical-align: middle" rowspan="2">Jumlah Peserta</th>
            <th style="vertical-align: middle" rowspan="2">Jumlah Responden</th>
            {foreach $header_kelompok_aspek as $hka}
                <th colspan="{$hka.JUMLAH_ASPEK}">{$hka.NAMA_KELOMPOK}</th>
            {/foreach}
            <th style="vertical-align: middle" rowspan="2">Rerata Dosen</th>
            <th style="vertical-align: middle" rowspan="2">Rerata Persen Dosen</th>
        </tr>
        <tr class="ui-widget-header">
            {foreach $header_aspek as $ha}
                <th>{$ha.EVALUASI_ASPEK}</th>
            {/foreach}
        </tr>
        {foreach $data_evaluasi as $eva}
            <tr>
                <td>{$eva@index+1}</td>
                <td>{$eva.NM_PENGGUNA}</td>
                <td>({$eva.NM_JENJANG})  {$eva.NM_PROGRAM_STUDI}</td>
                <td>{$eva.NM_DEPARTEMEN}</td>
                <td>{$eva.JUMLAH_PESERTA}</td>
                <td>{$eva.JUMLAH_RESPONDEN}</td>
                {foreach $eva.DATA_HASIL as $hasil}
                    <td>{round($hasil.RATA,2)}</td>
                {/foreach}
                <td>{$eva.RERATA_DOSEN}</td>
                <td>{$eva.RERATA_PERSEN_DOSEN} %</td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="{count($header_aspek)+8}" class="kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        {if $data_rata_dep!=''||$data_rata_prodi!=''}
            <tr>
                <td colspan="{count($header_aspek)+8}" class="total center">
                    {if $smarty.get.prodi!=''}
                        {if $smarty.get.prodi!=''}
                            Rerata Prodi : {round($data_rata_prodi,2)} <br/>
                            Rerata Persen Prodi : {round($data_rata_prodi/4*100,2)} % <br/>
                        {/if}
                        Rerata Departemen {$data_rata_departemen.NM_DEPARTEMEN}: {round($data_rata_departemen.RATA,2)} <br/>
                        Rerata Persen Departemen : {round($data_rata_departemen.RATA/4*100,2)} % <br/>
                    {/if}
                </td>
            </tr>
        {/if}
    </table>
{/if}

{literal}
    <script>
            $('#fakultas').change(function(){
                    $.ajax({
                            type:'post',
                            url:'getProdi.php',
                            data:'id_fakultas='+$(this).val(),
                            success:function(data){
                                    $('#prodi').html(data);
                            }                    
                    })
            });
    </script>
{/literal}