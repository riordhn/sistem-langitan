<?php

include 'config.php';
include '../keuangan/class/list_data.class.php';
include '../ppm/class/evaluasi.class.php';

$list = new list_data($db);
$eva = new evaluasi($db);

if (isset($_GET)) {
    if (get('mode') == 'tampil') {
        $smarty->assign('header_kelompok_aspek', $db->QueryToArray("
            SELECT EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK,COUNT(EA.ID_EVAL_ASPEK)  JUMLAH_ASPEK
            FROM EVALUASI_KELOMPOK_ASPEK EKS
            JOIN EVALUASI_ASPEK EA ON EA.ID_EVAL_KELOMPOK_ASPEK=EKS.ID_EVAL_KELOMPOK_ASPEK AND EA.TIPE_ASPEK=1
            WHERE EKS.ID_EVAL_INSTRUMEN=3
            GROUP BY EKS.ID_EVAL_KELOMPOK_ASPEK,EKS.NAMA_KELOMPOK
            ORDER BY NAMA_KELOMPOK
            ")
        );
        $smarty->assign('header_aspek', $db->QueryToArray("SELECT * FROM EVALUASI_ASPEK WHERE ID_EVAL_INSTRUMEN=3 AND TIPE_ASPEK=1 ORDER BY ID_EVAL_ASPEK"));
        $smarty->assign('data_evaluasi', $eva->load_laporan_evaluasi_wali(get('fakultas'), get('prodi'), get('semester')));
        $smarty->assign('data_rata_dep', $eva->load_laporan_rerata_departemen_evaluasi_wali(get('prodi'), get('semester')));
        $smarty->assign('data_rata_prodi', $eva->load_laporan_rerata_prodi_evaluasi_wali(get('prodi'), get('semester')));
    }
}
$smarty->assign('id_program_studi', $user->ID_PROGRAM_STUDI);
$smarty->assign('id_fakultas',$user->ID_FAKULTAS);
$smarty->assign('data_semester', $list->load_list_semester());
$smarty->assign('data_fakultas', $list->load_list_fakultas($id_pt_user));
$smarty->assign('data_prodi', $list->load_list_prodi($id_pt_user, get('fakultas') != '' ? get('fakultas') : $user->ID_FAKULTAS));
$smarty->display('evawali.tpl');
?>
