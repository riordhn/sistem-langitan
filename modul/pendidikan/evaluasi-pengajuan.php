<?php
error_reporting (E_ALL & ~E_NOTICE);
include 'config.php';
include 'class/evaluasi.class.php';

$eval = new evaluasi($db);
$id_pengguna= $user->ID_PENGGUNA; 


$smarty->assign('semester', $semester);
$smarty->assign('fakultas', $eval->fakultas());
$smarty->assign('semester', $eval->semester());
$smarty->assign('jenjang', $eval->jenjang());
$smarty->assign('status', $eval->status_rekomendasi());
$smarty->assign('status_penetapan', $eval->status_penetapan());

if($_POST['mode'] == 'insert'){
	
	$no = $_POST['no'];
	$tgl = date('d-m-Y');
	
	for($i=2; $i<=$no; $i++){
		$id_mhs = $_POST['id_mhs'.$i];
		$cek = $_POST['cek'.$i];
		$penetapan = $_POST['penetapan'.$i];
		$keterangan = $_POST['keterangan'.$i];
		
		if($cek == 1){
			$db->Query("UPDATE EVALUASI_STUDI SET PENGAJUAN_EVALUASI = 1 WHERE ID_MHS = '$id_mhs' AND ID_SEMESTER = '$_POST[semester]' 
						AND IS_AKTIF = 1");
		}else{
			$db->Query("UPDATE EVALUASI_STUDI SET PENGAJUAN_EVALUASI = 0 WHERE ID_MHS = '$id_mhs' AND ID_SEMESTER = '$_POST[semester]' 
						AND IS_AKTIF = 1");
		}
		
		$db->Query("SELECT PENETAPAN_STATUS FROM EVALUASI_STUDI 
					WHERE IS_AKTIF = 1 AND ID_MHS = '$id_mhs' 
					AND ID_SEMESTER = '$_POST[semester]' AND JENIS_EVALUASI = 4");
		$cek = $db->FetchAssoc();
		
		if($cek['PENETAPAN_STATUS'] != '$penetapan'){	
		
		$db->Query("UPDATE EVALUASI_STUDI SET IS_AKTIF = 0 WHERE ID_MHS = '$id_mhs' AND ID_SEMESTER = '$_POST[semester]' AND JENIS_EVALUASI = 4");
		
		$db->Query("INSERT INTO EVALUASI_STUDI (ID_MHS, ID_SEMESTER, PENETAPAN_STATUS, TGL_PENETAPAN_STATUS,
					   KETERANGAN, JENIS_EVALUASI,  ID_PENGGUNA, WAKTU_UBAH, IS_AKTIF)
					  VALUES 
					 ('$id_mhs', '$_POST[semester]', '$penetapan', to_date('$tgl', 'DD-MM-YYYY'), 
					  '$keterangan', 4, '$id_pengguna', to_date('$tgl', 'DD-MM-YYYY'), 1)");
		}

	}

}


if(isset($_REQUEST['semester'])){

$smarty->assign('evaluasi', $eval->evaluasi_pengajuan($_REQUEST['fakultas'], $_REQUEST['jenjang'], $_REQUEST['prodi'], $_REQUEST['semester']));

}

$smarty->display("evaluasi/pengajuan/evaluasi-pengajuan.tpl");
?>