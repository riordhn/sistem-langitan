<?php
include('config.php');
$periode = $db->QueryToArray("SELECT DISTINCT TO_CHAR(TGL_LULUS,'YYYY')tgl_lls FROM ADMISI WHERE TO_CHAR(TGL_LULUS,'YYYY') IS NOT NULL AND SUBSTR(TO_CHAR(TGL_LULUS,'YYYY'), 1, 2)<>'00' AND ID_MHS IS NOT NULL ORDER BY TO_CHAR(TGL_LULUS,'YYYY') DESC");
$smarty->assign('periode', $periode);
$fakultas = $db->QueryToArray("SELECT ID_FAKULTAS,NM_FAKULTAS FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY ID_FAKULTAS ASC");
$smarty->assign('fakultas', $fakultas);
$jenjang = $db->QueryToArray("SELECT ID_JENJANG,NM_JENJANG FROM JENJANG where NIM_JENJANG IS NOT NULL ORDER BY ID_JENJANG ASC");
$smarty->assign('jenjang', $jenjang);

if(isset($_POST['periode'])){
		if($_POST['fakultas'] <> ''){
			$where  = " AND PS.ID_FAKULTAS = '$_POST[fakultas]'";
			$order = "";
		}else{
			$where  = "";
			$order = "PS.ID_FAKULTAS, ";
		}
		
		$rekap = $db->QueryToArray("select (case when pg.gelar_depan is null then '' else pg.gelar_depan || '. ' end)gelar_depan, pg.nm_pengguna, (case when    					    pg.gelar_belakang is null then '' else ', ' || pg.gelar_belakang 	
					    end)gelar_belakang,mh.nim_mhs,
					    (case when pg.kelamin_pengguna = '2' then 'Perempuan' else 'Laki-laki' end)jk,fk.nm_fakultas,
					    ps.nm_program_studi,jj.nm_jenjang,(case when pw.lahir_ijazah is null then (case when kt.nm_kota is null then 'Tidak Ada Data' 						    else kt.nm_kota end 
					    || ', ' || 
					    case when to_char(pg.tgl_lahir_pengguna,'DD-MM-YYYY') is null then 'Tidak Ada Data' else 
					    to_char(pg.tgl_lahir_pengguna,'DD-MM-YYYY') end) else pw.lahir_ijazah end)ttl,ad.tgl_lulus as tgl_lls,
					    pw.no_ijasah,pw.no_seri_kertas,pw.tgl_posisi_ijasah,pw.judul_ta
					    FROM admisi ad
					    left join mahasiswa mh on mh.id_mhs=ad.id_mhs
					    left join pengguna pg on pg.id_pengguna=mh.id_pengguna
					    left join program_studi ps on mh.id_program_studi=ps.id_program_studi
					    left join fakultas fk on ps.id_fakultas=fk.id_fakultas
					    left join jenjang jj on ps.id_jenjang=jj.id_jenjang
					    left join kota kt on kt.id_kota=mh.lahir_kota_mhs
					    left join pengajuan_wisuda pw on pw.id_mhs=ad.id_mhs
					    where ad.status_akd_mhs=4 
					    $where
					    and ad.id_mhs is not null
					    and jj.id_jenjang='$_POST[jenjang]'
					    and to_char(ad.tgl_lulus,'YYYY')='$_POST[periode]'
					    order by pw.no_ijasah, ps.id_program_studi,ps.id_jenjang,$order to_char(ad.tgl_lulus,'DD-MM-YYYY')"
						);
		$total = $db->QueryToArray("SELECT id_fakultas,nm_fakultas,sum(decode(NM_JENJANG,'D3',jml,NULL))D3,sum(decode(NM_JENJANG,'S1',jml,NULL))S1,
									sum(decode(NM_JENJANG,'S2',jml,NULL))S2,sum(decode(NM_JENJANG,'S3',jml,NULL))S3,sum(decode(NM_JENJANG,'Profesi',jml,NULL))PROFESI
									,sum(decode(NM_JENJANG,'Spesialis',jml,NULL))SPESIALIS
									FROM
									(
										SELECT ff.id_fakultas,ff.nm_fakultas,jj.nm_jenjang,count(AI.id_mhs) as jml
										FROM ADMISI ai
										join MAHASISWA ms on MS.ID_MHS=AI.ID_MHS
										join PROGRAM_STUDI ps on ps.id_program_studi=ms.id_program_studi
										join JENJANG jj on ps.id_jenjang=jj.id_jenjang
										join FAKULTAS ff on ps.id_fakultas=ff.id_fakultas
										WHERE AI.STATUS_AKD_MHS='4'
										AND AI.STATUS_APV='1'
										AND to_char(AI.TGL_LULUS,'yyyy')='$_POST[periode]'
										GROUP BY ff.id_fakultas,ff.nm_fakultas,jj.nm_jenjang
									)
									GROUP BY id_fakultas,nm_fakultas
									ORDER BY id_fakultas"
								);
$smarty->assign('rekap', $rekap);
$smarty->assign('total', $total);	
$smarty->assign('id_periode', $_POST['periode']);
$smarty->assign('id_fakultas', $_POST['fakultas']);
$smarty->assign('id_jenjang', $_POST['jenjang']);
}

$smarty->display("wisuda/wisuda-lls.tpl");
?>
