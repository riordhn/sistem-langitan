<?php
include('config.php');
$id_pengguna= $user->ID_PENGGUNA; 

$mode = get('mode', 'view');
$id_pt = $id_pt_user;
$ip=$_SERVER['REMOTE_ADDR'];
$tgl = date('d-m-Y');

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
		//$gelar_panjang = ucwords(strtolower($_POST['gelar_panjang']));
		$gelar_panjang = $_POST['gelar_panjang'];
        $db->Query("INSERT INTO PROGRAM_STUDI (ID_FAKULTAS, ID_JENJANG, NM_PROGRAM_STUDI, NO_SK, 
					TGL_PENDIRIAN, MASA_BERLAKU, GELAR_PANJANG, GELAR_PENDEK, GELAR_INGGRIS, STATUS_AKTIF_PRODI, KODE_PROGRAM_STUDI, KODE_NIM_PRODI, NM_PROGRAM_STUDI_ENG) 
					VALUES 
					('$_POST[fakultas]', '$_POST[jenjang]', UPPER('$_POST[nm_prodi]'), '$_POST[no_sk]', 
					to_date('$_POST[tgl_pendirian]', 'DD-MM-YYYY'),
					$_POST[masa_berlaku], '$gelar_panjang', '$_POST[gelar_pendek]', '$_POST[gelar_inggris]', '$_POST[status_prodi]', '$_POST[kode_prodi]', '$_POST[kode_nim_prodi]', '$_POST[nm_prodi_en]')");
				
		
		$db->Query("SELECT * FROM PROGRAM_STUDI WHERE ROWNUM = 1 ORDER BY ID_PROGRAM_STUDI DESC");
		$prodi = $db->FetchAssoc();
		
		/* Baris ini dimatikan, untuk sementara sudah dibuatkan trigger, nanti bakal dikembalikan
		$db->Query("INSERT INTO UNIT_KERJA (ID_FAKULTAS, TYPE_UNIT_KERJA, NM_UNIT_KERJA, ID_PROGRAM_STUDI, ID_PERGURUAN_TINGGI) 
					VALUES 
					('$_POST[fakultas]', 'PRODI', UPPER('$_POST[nm_prodi]'), '".$prodi['ID_PROGRAM_STUDI']."', '$id_pt')");
		*/


		$db->Query("INSERT INTO LOG_PRODI (ID_FAKULTAS, ID_JENJANG, NM_PROGRAM_STUDI, NO_SK, 
					TGL_PENDIRIAN, MASA_BERLAKU, GELAR_PANJANG, GELAR_PENDEK, STATUS_AKTIF_PRODI, ID_PENGGUNA, 
					ID_PROGRAM_STUDI, KODE_PROGRAM_STUDI, TGL_UBAH, IP) 
					VALUES 
					('$_POST[fakultas]', '$_POST[jenjang]', UPPER('$_POST[nm_prodi]'), '$_POST[no_sk]', 
					to_date('$_POST[tgl_pendirian]', 'DD-MM-YYYY'),
					$_POST[masa_berlaku], '$gelar_panjang', '$_POST[gelar_pendek]', '$_POST[status_prodi]', 
					'$id_pengguna', '".$prodi['ID_PROGRAM_STUDI']."', '$_POST[kode_prodi]', to_date('$tgl', 'DD-MM-YYYY'), '$ip')");
		
						
    }
    else if (post('mode') == 'edit')
    {
		//$gelar_panjang = ucwords(strtolower($_POST['gelar_panjang']));
		$gelar_panjang = $_POST['gelar_panjang'];
		$db->Query("SELECT * FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI = '$_POST[id_prodi]'");
		$prodi = $db->FetchAssoc();
		
		$db->Query("UPDATE PROGRAM_STUDI SET ID_FAKULTAS = '$_POST[fakultas]', ID_JENJANG = '$_POST[jenjang]', 
					NM_PROGRAM_STUDI = UPPER('$_POST[nm_prodi]'), NO_SK = '$_POST[no_sk]',
					TGL_PENDIRIAN = to_date('$_POST[tgl_pendirian]', 'DD-MM-YYYY'), MASA_BERLAKU = '$_POST[masa_berlaku]', 
					GELAR_PANJANG = '$gelar_panjang', GELAR_PENDEK = '$_POST[gelar_pendek]', GELAR_INGGRIS = '$_POST[gelar_inggris]', 
					STATUS_AKTIF_PRODI = '$_POST[status_prodi]', KODE_PROGRAM_STUDI = '$_POST[kode_prodi]', KODE_NIM_PRODI = '$_POST[kode_nim_prodi]', 
					NM_PROGRAM_STUDI_ENG = '$_POST[nm_prodi_en]'
					WHERE ID_PROGRAM_STUDI = '$_POST[id_prodi]'");
					
		$db->Query("UPDATE UNIT_KERJA SET ID_FAKULTAS = '$_POST[fakultas]', NM_UNIT_KERJA = UPPER('$_POST[nm_prodi]')
					WHERE ID_PROGRAM_STUDI = '$_POST[id_prodi]'");
		
		
		$db->Query("INSERT INTO LOG_PRODI (ID_FAKULTAS, ID_JENJANG, NM_PROGRAM_STUDI, NO_SK, 
					TGL_PENDIRIAN, MASA_BERLAKU, GELAR_PANJANG, GELAR_PENDEK, STATUS_AKTIF_PRODI, ID_PENGGUNA, 
					ID_PROGRAM_STUDI, KODE_PROGRAM_STUDI, KODE_NIM_PRODI,
					ID_FAKULTAS_UBAH, ID_JENJANG_UBAH, NM_PROGRAM_STUDI_UBAH, NO_SK_UBAH, 
					TGL_PENDIRIAN_UBAH, MASA_BERLAKU_UBAH, GELAR_PANJANG_UBAH, GELAR_PENDEK_UBAH, STATUS_AKTIF_PRODI_UBAH, ID_PENGGUNA_UBAH, 
					ID_PROGRAM_STUDI_UBAH, KODE_PROGRAM_STUDI_UBAH, TGL_UBAH, IP)
					 VALUES 
					('".$prodi['ID_FAKULTAS']."', '".$prodi['ID_JENJANG']."', UPPER('".$prodi['NM_PROGRAM_STUDI']."'), '".$prodi['NO_SK']."', 
					to_date('".$prodi['TGL_PENDIRIAN']."', 'DD-MM-YYYY'),
					'".$prodi['MASA_BERLAKU']."', '".$prodi['GELAR_PANJANG']."', '".$prodi['GELAR_PENDEK']."', 
					'".$prodi['STATUS_AKTIF_PRODI']."', '$id_pengguna', 
					'".$prodi['ID_PROGRAM_STUDI']."', '".$prodi['KODE_PROGRAM_STUDI']."', '".$prodi['KODE_NIM_PRODI']."',
					'$_POST[fakultas]', '$_POST[jenjang]', UPPER('$_POST[nm_prodi]'), '$_POST[no_sk]', 
					to_date('$_POST[tgl_pendirian]', 'DD-MM-YYYY'),
					$_POST[masa_berlaku], '$gelar_panjang', '$_POST[gelar_pendek]', '$_POST[status_prodi]', '$id_pengguna', 
					'$_POST[id_prodi]', '$_POST[kode_prodi]', to_date('$tgl', 'DD-MM-YYYY'), '$ip')");
					
    }
    else if (post('mode') == 'delete')
    {
		$db->Query("DELETE UNIT_KERJA
					WHERE ID_PROGRAM_STUDI = '$_POST[id_prodi]'");
					
        $db->Query("DELETE PROGRAM_STUDI
					WHERE ID_PROGRAM_STUDI = '$_POST[id_prodi]'");
    }
}



if ($mode == 'view')
{
	$fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = {$id_pt} ORDER BY ID_FAKULTAS ASC");
    $smarty->assign('fakultas', $fakultas);
	
	if($_POST['fakultas'] <> ''){
		$prodi_set = $db->QueryToArray("SELECT * FROM PROGRAM_STUDI
									LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
									LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
									WHERE FAKULTAS.ID_FAKULTAS = '$_POST[fakultas]' -- AND STATUS_AKTIF_PRODI = 1
									ORDER BY FAKULTAS.ID_FAKULTAS, JENJANG.ID_JENJANG, NM_PROGRAM_STUDI");
	}else{
		$prodi_set = $db->QueryToArray("SELECT * FROM PROGRAM_STUDI
									LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
									LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
									-- WHERE STATUS_AKTIF_PRODI = 1
									WHERE FAKULTAS.ID_PERGURUAN_TINGGI = {$id_pt}
									ORDER BY FAKULTAS.ID_FAKULTAS, JENJANG.ID_JENJANG, NM_PROGRAM_STUDI");
	}
    
	
	$smarty->assign('id_fakultas', $_POST['fakultas']);
    $smarty->assign('prodi_set', $prodi_set);
}
else if ($mode == 'edit' or $mode == 'delete')
{
	$prodi_set = $db->QueryToArray("SELECT PROGRAM_STUDI.*, TO_CHAR(PROGRAM_STUDI.TGL_PENDIRIAN, 'DD-MM-YYYY') AS TGL_PENDIRIAN1 FROM PROGRAM_STUDI
									WHERE ID_PROGRAM_STUDI = '$_GET[id_prodi]'");
    $smarty->assign('prodi_set', $prodi_set);
	
    $fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = {$id_pt} ORDER BY ID_FAKULTAS ASC");
    $smarty->assign('fakultas', $fakultas);
	
	$jenjang = $db->QueryToArray("SELECT * FROM JENJANG ORDER BY ID_JENJANG ASC");
    $smarty->assign('jenjang', $jenjang);
}
else if ($mode == 'add')
{
    $fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = {$id_pt} ORDER BY ID_FAKULTAS ASC");
    $smarty->assign('fakultas', $fakultas);
	
	$jenjang = $db->QueryToArray("SELECT * FROM JENJANG ORDER BY ID_JENJANG ASC");
    $smarty->assign('jenjang', $jenjang);
}

$smarty->display("perkuliahan/prodi/{$mode}.tpl");
?>