<?php
include('config.php');

$mode = get('mode', 'view');
$id_periode_bayar = get('id_periode_bayar', '');

if ($request_method == 'POST')
{
	$id_periode_bayar = post('id_periode_bayar');
	
	if ($_POST['mode'] == 'update')
	{
		$db->Parse("
			UPDATE PERIODE_BAYAR SET
				TGL_AWAL_PERIODE_BAYAR = :tgl_awal_periode_bayar,
				TGL_AKHIR_PERIODE_BAYAR = :tgl_akhir_periode_bayar,
				KET_PERIODE_BAYAR = :ket_periode_bayar
			WHERE ID_PERIODE_BAYAR = {$id_periode_bayar}");
		$db->BindByName(':tgl_awal_periode_bayar', date('d-M-Y', mktime(0, 0, 0, post('tgl_awal_Month'), post('tgl_awal_Day') - 1, post('tgl_awal_Year'))));
		$db->BindByName(':tgl_akhir_periode_bayar', date('d-M-Y', mktime(0, 0, 0, post('tgl_akhir_Month'), post('tgl_akhir_Day') + 1, post('tgl_akhir_Year'))));
		$db->BindByName(':ket_periode_bayar', post('ket_periode_bayar'));
		$result = $db->Execute();
		
		if ($result)
		{
			$smarty->assign('periode_changed', true);
		}
	}
	
	if($_POST['mode'] == 'hapus'){
		$db->Query("DELETE PERIODE_BAYAR WHERE ID_PERIODE_BAYAR = $id_periode_bayar");
	}
	
	if ($_POST['mode'] == 'add'){
		
		$tgl_awal = date('d-M-Y', mktime(0, 0, 0, post('tgl_awal_Month'), post('tgl_awal_Day') - 1, post('tgl_awal_Year')));
		$tgl_akhir = date('d-M-Y', mktime(0, 0, 0, post('tgl_akhir_Month'), post('tgl_akhir_Day') + 1, post('tgl_akhir_Year')));
		$semester = post('semester');
		$keterangan = post('ket_periode_bayar');
		
		$db->Query("
			INSERT INTO PERIODE_BAYAR 
				(TGL_AWAL_PERIODE_BAYAR,
				TGL_AKHIR_PERIODE_BAYAR,
				KET_PERIODE_BAYAR,
				ID_SEMESTER)
				VALUES (to_date('$tgl_awal','DD-MM-YYYY'), to_date('$tgl_akhir','DD-MM-YYYY'), '$keterangan', '$semester')");
				
	}
}

if ($request_method == 'GET' || $request_method == 'POST')
{
	if ($mode == 'view')
	{		
		$periode_bayar_set = $db->QueryToArray("
			SELECT PB.ID_PERIODE_BAYAR, S.NM_SEMESTER, S.TAHUN_AJARAN, 
				(PB.TGL_AWAL_PERIODE_BAYAR + 1) AS TGL_AWAL_PERIODE_BAYAR,
				(PB.TGL_AKHIR_PERIODE_BAYAR - 1) AS TGL_AKHIR_PERIODE_BAYAR, PB.KET_PERIODE_BAYAR
			FROM PERIODE_BAYAR PB
			JOIN SEMESTER S ON S.ID_SEMESTER = PB.ID_SEMESTER
			ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
		$smarty->assign('periode_bayar_set', $periode_bayar_set);
	}
	
	if ($mode == 'edit' or $mode == 'delete')
	{
		$periode_bayar_set = $db->QueryToArray("
			SELECT PB.ID_PERIODE_BAYAR, S.NM_SEMESTER, S.TAHUN_AJARAN, 
				(PB.TGL_AWAL_PERIODE_BAYAR + 1) AS TGL_AWAL_PERIODE_BAYAR,
				(PB.TGL_AKHIR_PERIODE_BAYAR - 1) AS TGL_AKHIR_PERIODE_BAYAR, PB.KET_PERIODE_BAYAR
			FROM PERIODE_BAYAR PB
			JOIN SEMESTER S ON S.ID_SEMESTER = PB.ID_SEMESTER
			WHERE PB.ID_PERIODE_BAYAR = {$id_periode_bayar}
			ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
		$smarty->assign('pb', $periode_bayar_set[0]);
	}
	
	if ($mode == 'add'){
		$semester_set = $db->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER = 'Ganjil' or NM_SEMESTER = 'Genap' ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
		$smarty->assign('semester_set', $semester_set);
	}
}

$smarty->display("aktivitas/periodebayar/{$mode}.tpl");
?>