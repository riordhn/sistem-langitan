<?php
include('config.php');

$mode = get('mode', 'view');
$id_semester = get('id_semester', 0);

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
		
		if (post('status_aktif_semester') == 'True' && post('nm_semester') != 'Pendek')
        {
           $db->Query("UPDATE SEMESTER SET STATUS_AKTIF_SEMESTER = 'False'
           	WHERE ID_PERGURUAN_TINGGI = '{$PT->id_perguruan_tinggi}'");
        }
        elseif (post('status_aktif_semester') == 'True' && post('nm_semester') == 'Pendek') 
        {
        	$db->Query("UPDATE SEMESTER SET STATUS_AKTIF_PEMBAYARAN_SP = 'False', STATUS_AKTIF_SP = 'False' 
           	WHERE ID_PERGURUAN_TINGGI = '{$PT->id_perguruan_tinggi}'");
        }

		/*$db->Query("INSERT INTO SEMESTER (ID_PERGURUAN_TINGGI, NM_SEMESTER, THN_AKADEMIK_SEMESTER, STATUS_AKTIF_SEMESTER, TAHUN_AJARAN, GROUP_SEMESTER, TIPE_SEMESTER)
					VALUES ('{$PT->id_perguruan_tinggi}', 'Ujian Perbaikan', '$_POST[thn_akademik_semester]', 'False','$_POST[tahun_ajaran]', '$_POST[nm_semester]', 'UP')");*/

		if(post('nm_semester') == 'Pendek') {
			// building FD_ID_SMT for SP
	        $fd_id_smt_sp = $_POST['thn_akademik_semester'] . '3';

			$db->Query("INSERT INTO SEMESTER (ID_PERGURUAN_TINGGI, NM_SEMESTER, THN_AKADEMIK_SEMESTER, STATUS_AKTIF_SEMESTER, TAHUN_AJARAN, GROUP_SEMESTER, TIPE_SEMESTER, STATUS_AKTIF_SP, STATUS_AKTIF_PEMBAYARAN_SP, FD_ID_SMT)
						VALUES ('{$PT->id_perguruan_tinggi}', 'Pendek', '$_POST[thn_akademik_semester]', 'False','$_POST[tahun_ajaran]', '$_POST[nm_semester]', 'SP', '$_POST[status_aktif_semester]', '$_POST[status_aktif_semester]', '$fd_id_smt_sp')");
		}
		else {
			// building FD_ID_SMT
	        $fd_id_smt = $_POST['thn_akademik_semester'] . ($_POST['nm_semester'] == 'Ganjil') ? '1' : '2';
			
			$db->Query("INSERT INTO SEMESTER (ID_PERGURUAN_TINGGI, NM_SEMESTER, THN_AKADEMIK_SEMESTER, STATUS_AKTIF_SEMESTER, TAHUN_AJARAN, GROUP_SEMESTER, TIPE_SEMESTER, FD_ID_SMT)
						VALUES ('{$PT->id_perguruan_tinggi}', '$_POST[nm_semester]', '$_POST[thn_akademik_semester]', '$_POST[status_aktif_semester]','$_POST[tahun_ajaran]', '$_POST[nm_semester]', 'REG', '$fd_id_smt')");
		}
        
        // Menambah di tabel JADWAL_KEGIATAN_SEMESTER
		/* dimatikan dulu
		$semester = $db->Query("SELECT ID_SEMESTER from SEMESTER where rownum = 1 AND ID_PERGURUAN_TINGGI = '{$PT->id_perguruan_tinggi}' order by rowid desc");
		$semester = $db->FetchAssoc();
        $kegiatan_set = $db->QueryToArray("SELECT * FROM KEGIATAN");
        
        foreach ($kegiatan_set as $data)
        {
			 $db->Query("INSERT INTO JADWAL_KEGIATAN_SEMESTER (ID_KEGIATAN, ID_SEMESTER)
					VALUES ('$data[ID_KEGIATAN]', '$semester[ID_SEMESTER]')");
        }
		*/
    }
    else if (post('mode') == 'edit')
    {		
		if(post('nm_semester') != 'Pendek') {
			if (post('status_aktif_semester') == 'True')
	        {
	           $db->Query("UPDATE SEMESTER SET STATUS_AKTIF_SEMESTER = 'False' WHERE ID_PERGURUAN_TINGGI = '{$PT->id_perguruan_tinggi}'");
	        }

			$db->Query("UPDATE SEMESTER SET THN_AKADEMIK_SEMESTER = '$_POST[thn_akademik_semester]', STATUS_AKTIF_SEMESTER = '$_POST[status_aktif_semester]', TAHUN_AJARAN = '$_POST[tahun_ajaran]'
					WHERE ID_SEMESTER = '$_POST[id_semester]' AND ID_PERGURUAN_TINGGI = '{$PT->id_perguruan_tinggi}'");
		}
		else {
			if (post('status_aktif_semester') == 'True') 
	        {
	        	$db->Query("UPDATE SEMESTER SET STATUS_AKTIF_PEMBAYARAN_SP = 'False', STATUS_AKTIF_SP = 'False' 
	           	WHERE ID_PERGURUAN_TINGGI = '{$PT->id_perguruan_tinggi}'");
	        }

			$db->Query("UPDATE SEMESTER SET  THN_AKADEMIK_SEMESTER = '$_POST[thn_akademik_semester]', STATUS_AKTIF_PEMBAYARAN_SP = '$_POST[status_aktif_semester]', STATUS_AKTIF_SP = '$_POST[status_aktif_semester]', TAHUN_AJARAN = '$_POST[tahun_ajaran]'
					WHERE ID_SEMESTER = '$_POST[id_semester]' AND ID_PERGURUAN_TINGGI = '{$PT->id_perguruan_tinggi}'");
		}        
    }
    else if (post('mode') == 'delete')
    {
        // Menghapus data FK dari JADWAL_KEGIATAN_SEMESTER
		$db->Query("DELETE JADWAL_KEGIATAN_SEMESTER
					WHERE ID_SEMESTER = '$_POST[id_semester]'");
        $db->Query("DELETE SEMESTER
					WHERE ID_SEMESTER = '$_POST[id_semester]' AND ID_PERGURUAN_TINGGI = '{$PT->id_perguruan_tinggi}'");
        //$smarty->assign('dump', print_r($semester, true));
    }
}

if ($mode == 'view')
{
	$semester_set = $db->QueryToArray(
		"SELECT * FROM SEMESTER 
		WHERE NM_SEMESTER IN ('Ganjil', 'Genap', 'Pendek') AND ID_PERGURUAN_TINGGI = {$PT->id_perguruan_tinggi}
		ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
    $smarty->assign('semester_set', $semester_set);
}
else if ($mode == 'edit' or $mode == 'delete')
{
	$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '$id_semester' AND ID_PERGURUAN_TINGGI = {$PT->id_perguruan_tinggi}");
    $semester = $db->FetchAssoc();
    $smarty->assign('semester', $semester);
}

$smarty->display("perkuliahan/semester/{$mode}.tpl");
?>
