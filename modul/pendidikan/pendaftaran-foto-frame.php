<?php
ini_set('post_max_size','20M'); 
ini_set('upload_max_filesize','20M'); 
ini_set('max_execution_time','20M');
ini_set('max_input_time','20M'); 
ini_set('memory_limit','20M'); 
set_time_limit(65536);

include('config.php');
include('class/image.class.php');
$mode = get('mode', 'view');
$nim_mhs = get('nim_mhs', '');
$image = new image();

$nama_singkat_pt = $nama_singkat;

if ($request_method == 'POST')
{
    $id_c_mhs = post('id_c_mhs');	
	$no_ujian = post('no_ujian');
	$nim_mhs = post('nim_mhs');		

			$image->resize(600, 900);
			
			if ($_FILES["upload_foto"]['type'] == "image/jpeg"){				    
				
				/*if($no_ujian != ''){
					$filename = "/var/www/html/foto_mhs/{$nama_singkat_pt}/{$no_ujian}.jpg";				
					$filename2 = "/var/www/html/foto_mhs/{$nama_singkat_pt}/{$nim_mhs}.jpg";
					
					//buat ganti yang baru
					unlink($filename);
					unlink($filename2);
					
					$image->load($_FILES["upload_foto"]["tmp_name"]);
					
					move_uploaded_file($image->save($filename), $filename);
					move_uploaded_file($image->save($filename2), $filename2);
					
					$source = "/var/www/html/foto_mhs/{$nama_singkat_pt}/".$nim_mhs.".jpg";
					$target = "/var/www/html/foto_mhs/{$nama_singkat_pt}/".$no_ujian.".jpg";
					copy($source, $target);
					
					 $result = $db->Query("
						update calon_mahasiswa_baru set
							tgl_foto_ktm = to_date('".date('YmdHis')."', 'YYYYMMDDHH24MISS')
						where id_c_mhs = {$id_c_mhs}");
				}else{*/

					// 1 = server xampp
					// 2 = server /var/www/
					$server = 1;

					if($server == 1){
						$path = "../..";
					}
					elseif($server == 2){
						$path = "/var/www/html";
					}

					$filename2 = $path."/foto_mhs/{$nama_singkat_pt}/{$nim_mhs}.jpg";
					
					unlink($path.'/foto_mhs/{$nama_singkat_pt}/{$nim_mhs}.jpg');
					
					//$image->load($_FILES["upload_foto"]["tmp_name"]);
					
					if(move_uploaded_file($_FILES["upload_foto"]["tmp_name"], $path."/foto_mhs/{$nama_singkat_pt}/{$nim_mhs}.jpg")){
						$result = 'ok';
					}
					
					
					//$tmp_name = $_FILES["upload_foto"]["tmp_name"];
					//move_uploaded_file($tmp_name, $filename2);
					
				/*}*/
			}
	
   
        
    if ($result)
            $smarty->assign('updated', "Data berhasil diupload");
        else
            $smarty->assign('updated', "Data gagal diupload");
}

if ($request_method == 'GET' or $request_method == 'POST')
{

    if ($nim_mhs != '')
    {
			$db->Query("select count(*) as cek from calon_mahasiswa_baru
						where (no_ujian = '$nim_mhs' or calon_mahasiswa_baru.nim_mhs = '$nim_mhs')");
			$cek_cmb = $db->FetchAssoc();
			
			if($cek_cmb['CEK'] == 1){
				
				// tgl_verifikasi_pendidikan tidak dipakai
				// FIKRIE
				$db->Query("select count(*) as cek from calon_mahasiswa_baru
							join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
							where (no_ujian = '$nim_mhs' or calon_mahasiswa_baru.nim_mhs = '$nim_mhs') 
							--and (tgl_verifikasi_pendidikan is not null)");
				$cek_loket = $db->FetchAssoc();
												
				
				if($cek_loket['CEK'] == 0){
					
				echo "Calon Mahasiswa Belum ke Loket A (Verifikasi Berkas)";
								
				}else{
					
					// FINGERPRINT tidak dipakai
					// FIKRIE
					/*$db->Query("select count(*) as cek from calon_mahasiswa_baru
							join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
							join FINGERPRINT_CMHS on FINGERPRINT_CMHS.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
							where (no_ujian = '$nim_mhs' or calon_mahasiswa_baru.nim_mhs = '$nim_mhs') and 
							(FINGER_DATA is not null)");
					$cek_loket1 = $db->FetchAssoc();
					if($cek_loket1['CEK'] == 0){
					
						echo "Calon Mahasiswa Belum ke Loket B (Sidik Jari)";
								
					}else{*/
				
						$db->Query("select count(*) as cek from calon_mahasiswa_baru
								where (no_ujian = '$nim_mhs' or calon_mahasiswa_baru.nim_mhs = '$nim_mhs') and nim_mhs is null");
						$cek_loket = $db->FetchAssoc();
					
						if($cek_loket['CEK'] == 0){
						$db->Query("
							select cmb.id_c_mhs, nm_c_mhs, nm_program_studi, tgl_foto_ktm, nm_jenjang, cmb.nim_mhs, no_ujian, mhs.status_akademik_mhs
							from calon_mahasiswa_baru cmb
							left join mahasiswa mhs on mhs.id_c_mhs = cmb.id_c_mhs
							join program_studi ps on ps.id_program_studi = cmb.id_program_studi
							join jenjang on jenjang.id_jenjang = ps.id_jenjang
							where (no_ujian = '$nim_mhs' or cmb.nim_mhs = '$nim_mhs')");
						$cmb = $db->FetchAssoc();
						$smarty->assign('cmb', $cmb);
						}else{
							echo "NIM Belum tergenerate";
						}
					/*}*/
				
				}
			}else{
				$db->Query("select count(*) as cek from mahasiswa m 
						join pengguna p on p.id_pengguna = m.id_pengguna
						where m.nim_mhs = '$nim_mhs' and p.id_perguruan_tinggi = '{$id_pt_user}'");
				$cek_cmb = $db->FetchAssoc();
			
				if($cek_cmb['CEK'] == 1){
					$db->Query("
						select id_c_mhs, nm_pengguna as nm_c_mhs, nm_program_studi, nm_jenjang, nim_mhs, status_akademik_mhs
						from mahasiswa cmb
						join pengguna p on p.id_pengguna = cmb.id_pengguna and p.id_perguruan_tinggi = {$id_pt_user}
						join program_studi ps on ps.id_program_studi = cmb.id_program_studi
						join jenjang on jenjang.id_jenjang = ps.id_jenjang
						where cmb.nim_mhs = '$nim_mhs'");
					$cmb = $db->FetchAssoc();
					$smarty->assign('cmb', $cmb);
				}
				else{
					echo "<b>Bukan Mahasiswa Aktif</b>";
				}
			}
    }
}

$smarty->display("pendaftaran/foto/pendaftaran-foto-frame.tpl");
?>
