<?php
include '../../config.php';
include_once '../../tcpdf/config/lang/ind.php';
include_once '../../tcpdf/tcpdf.php';

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


$db->Query("SELECT NM_C_MHS, NO_UJIAN, (NM_PROGRAM_STUDI || ' - ' || NM_JENJANG) AS NM_PRODI, NM_JALUR, 
		  							TO_CHAR(JADWAL_KESEHATAN.TGL_TEST, 'DD-MM-YYYY') AS TGL_TEST_KESEHATAN, 
									JADWAL_KESEHATAN.JAM_MULAI AS JAM_MULAI_KESEHATAN, 
									JADWAL_KESEHATAN.JAM_SELESAI AS JAM_SELESAI_KESEHATAN, 
									JADWAL_KESEHATAN.MENIT_MULAI AS MENIT_MULAI_KESEHATAN, 
									JADWAL_KESEHATAN.MENIT_SELESAI AS MENIT_SELESAI_KESEHATAN, 
									JADWAL_KESEHATAN.KELOMPOK_JAM AS KELOMPOK_JAM_KESEHATAN,
									TO_CHAR(JADWAL_TOEFL.TGL_TEST, 'DD-MM-YYYY') AS TGL_TEST_TOEFL, 
									RUANG_TOEFL.NM_RUANG,
									JADWAL_TOEFL.JAM_MULAI AS JAM_MULAI_TOEFL, JADWAL_TOEFL.MENIT_MULAI AS MENIT_MULAI_TOEFL
									  FROM CALON_MAHASISWA_BARU 
									  LEFT JOIN CALON_MAHASISWA_DATA ON CALON_MAHASISWA_DATA.ID_C_MHS = CALON_MAHASISWA_BARU.ID_C_MHS
									  LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = CALON_MAHASISWA_BARU.ID_PROGRAM_STUDI
									  LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
									  LEFT JOIN JALUR ON JALUR.ID_JALUR = CALON_MAHASISWA_BARU.ID_JALUR
									  LEFT JOIN URUTAN_KESEHATAN ON URUTAN_KESEHATAN.ID_C_MHS = CALON_MAHASISWA_BARU.ID_C_MHS
									  LEFT JOIN JADWAL_KESEHATAN ON JADWAL_KESEHATAN.ID_JADWAL_KESEHATAN = URUTAN_KESEHATAN.ID_JADWAL_KESEHATAN
									  LEFT JOIN JADWAL_TOEFL ON JADWAL_TOEFL.ID_JADWAL_TOEFL = CALON_MAHASISWA_DATA.ID_JADWAL_TOEFL
									  LEFT JOIN RUANG_TOEFL ON RUANG_TOEFL.ID_RUANG_TOEFL = JADWAL_TOEFL.ID_RUANG_TOEFL
									  WHERE NO_UJIAN = '".$_GET['no_ujian']."'");
$calon_mahasiswa = $db->FetchAssoc();

$db->Query("SELECT TAHUN_AJARAN FROM CALON_MAHASISWA_BARU JOIN PENERIMAAN ON PENERIMAAN.ID_PENERIMAAN = CALON_MAHASISWA_BARU.ID_PENERIMAAN JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PENERIMAAN.ID_SEMESTER WHERE NO_UJIAN = '".$_GET['no_ujian']."'");
$smt = $db->FetchAssoc();

$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    td{font-size: 30px;}
    h1{font-size: 60px;}
</style>
<table cellpadding="6" width="100%" border="0">
    <tr><td colspan="2" align="center"><h1>Jadwal Test Calon Mahasiswa</h1></td></tr>
    <tr>
        <td colspan="2"></td>  
    </tr>
    <tr>
        <td width="35%">Nama</td>
        <td width="65%">: {nama}</td>
    </tr>
	<tr>
        <td width="35%">No Ujian</td>
        <td width="65%">: {no_ujian}</td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>: {program_studi}</td>
    </tr>
    <tr>
        <td colspan="2"></td>  
    </tr>
    <tr>
        <td>Tempat Test ELPT</td>
        <td>: Pusat Bahasa, Kampus B - Universitas Airlangga</td>
    </tr>
    <tr>
        <td>Jadwal Test ELPT</td>
        <td>: {jadwal_toefl}</td>
    </tr>
    <tr>
        <td>Ruang Test ELPT</td>
        <td>: {ruang_toefl}</td>
    </tr>
    <tr>
        <td>Waktu Test ELPT</td>
        <td>: {waktu_toefl}</td>
    </tr>
    <tr>
        <td colspan="2"></td>  
    </tr>
    <tr>
        <td>Tempat Test Kesehatan</td>
        <td>: Rumah Sakit Universitas Airlangga, Kampus C UNAIR,Surabaya</td>
    </tr>
    <tr>
        <td>Jadwal Test Kesehatan</td>
        <td>: {jadwal_kesehatan}</td>
    </tr>
    <tr>
        <td>Kelompok Test Kesehatan</td>
        <td>: {kelompok_jam}</td>
    </tr>
    <tr>
        <td>Waktu Test Kesehatan</td>
        <td>: {waktu_kesehatan} - {waktu_kesehatan_selesai}</td>
    </tr>
    <tr>
        <td colspan="2"></td>  
    </tr>
    <tr>
        <td><strong>NB :</strong> untuk tes kesehatan</td>
        <td>
            <ul>
                <li>Wajib memakai kemeja dan sepatu</li>
                <li>Wajib datang tepat waktu sesuai jadwal</li>
				<li>Yang berkacamata/ softlens harap membawa kacamata/ softlens tersebut</li>
                <li>Wajib membawa kartu identitas yang jelas dan bukti registrasi</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>  
    </tr>
    <tr>
        <td></td>
        <td border="1">
            <table>
                <tr>
                    <td colspan="3" align="center"><strong>
                        BUKTI DAFTAR ULANG<br/>
                        MAHASISWA BARU UNAIR {smt}</strong>
                    </td>
                </tr>
                <tr><td colspan="3"></td></tr>
                <tr>
                    <td>Nama</td>
                    <td colspan="2">: {nama}</td>
                </tr>
                <tr>
                    <td>No Ujian</td>
                    <td colspan="2">: {no_ujian}</td>
                </tr>
                <tr>
                    <td>Program Studi</td>
                    <td colspan="2">: {program_studi}</td>
                </tr>
                <tr><td colspan="3"></td></tr>
                <tr>
                    <td style="font-size:20px;" colspan="2">
					</td>

                    <td>Surabaya,<br/>Petugas Registrasi<br/><br/><br/><br/><br/>_______________</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
EOF;


if($calon_mahasiswa['MENIT_MULAI_KESEHATAN'] == 0){
	$menit_mulai = $calon_mahasiswa['MENIT_MULAI_KESEHATAN'] . "0";
}else{
	$menit_mulai = $calon_mahasiswa['MENIT_MULAI_KESEHATAN'];
}

if($calon_mahasiswa['MENIT_SELESAI_KESEHATAN'] == 0){
	$menit_selesai = $calon_mahasiswa['MENIT_SELESAI_KESEHATAN'] . "0";
}else{
	$menit_selesai = $calon_mahasiswa['MENIT_SELESAI_KESEHATAN'];
}


$html = str_replace('{nama}', $calon_mahasiswa['NM_C_MHS'], $html);
$html = str_replace('{no_ujian}', $calon_mahasiswa['NO_UJIAN'], $html);
$html = str_replace('{program_studi}', strtolower(ucwords($calon_mahasiswa['NM_PRODI'])), $html);
$html = str_replace('{jadwal_toefl}', strftime('%d %B %Y', strtotime($calon_mahasiswa['TGL_TEST_TOEFL'])), $html);
$html = str_replace('{ruang_toefl}', $calon_mahasiswa['NM_RUANG'], $html);
$html = str_replace('{waktu_toefl}', $calon_mahasiswa['JAM_MULAI_TOEFL'] . ":" . $calon_mahasiswa['MENIT_MULAI_TOEFL'], $html);
$html = str_replace('{jadwal_kesehatan}', strftime('%d %B %Y', strtotime($calon_mahasiswa['TGL_TEST_KESEHATAN'])), $html);
$html = str_replace('{kelompok_jam}', $calon_mahasiswa['KELOMPOK_JAM_KESEHATAN'], $html);
$html = str_replace('{waktu_kesehatan}', $calon_mahasiswa['JAM_MULAI_KESEHATAN'] . ":" . $menit_mulai, $html);
$html = str_replace('{waktu_kesehatan_selesai}', $calon_mahasiswa['JAM_SELESAI_KESEHATAN'] . ":" . $menit_selesai, $html);
$html = str_replace('{smt}', $smt['TAHUN_AJARAN'], $html);

$pdf->writeHTML($html);
$pdf->addPage();

$html = '
<style>
    td{font-size: 30px;}
    h1{font-size: 60px;}
</style>
<table width="100%">
	<tr>
		<td align="center"><h1><b>PENGUMUMAN</b></h1></td>
	</tr>
	<tr>
		<td width="3%"></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>1. </td>
		<td colspan="2">KTM diterbitkan oleh bank mitra UNAIR setelah dinyatakan lulus tes ketunaan</td>
	</tr>
	<tr>
		<td>2. </td>
		<td colspan="2">Pengumuman hasil tes kesehatan bisa di lihat  di regmaba.unair.ac.id (&#60;= 3 minggu) setelah pelaksanaan tes ketunaan berakhir</td>
	</tr>
	<tr>
		<td>3. </td>
		<td colspan="2">Mahasiswa mengisi kuisioner registrasi di ' . $_SERVER['HTTP_HOST'] . ' di menu Evaluasi</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td><strong>NB :</strong> Login di ' . $_SERVER['HTTP_HOST'] . ' menggunakan Username=nim dan password=nim</td>
	</tr>
	<tr>
		<td></td>
		<td><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bank Mitra UNAIR :</strong> Mandiri, Mandiri Syariah, BNI, BNI Syariah, BTN , BRI</td>
	</tr>
</table>';

$pdf->writeHTML($html);

$pdf->Output($_GET['no_ujian'].'.pdf', 'I');
?>
