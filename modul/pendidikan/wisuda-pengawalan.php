<?php
include('config.php');

$periode = $db->QueryToArray("SELECT * FROM TARIF_WISUDA WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY ID_TARIF_WISUDA DESC");
$smarty->assign('periode', $periode);
$fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY ID_FAKULTAS ASC");
$smarty->assign('fakultas', $fakultas);

if(isset($_POST['no'])){
	$no = $_POST['no'];
	$tgl = date('Y-m-d');
	
	for($i=1;$i<=$no;$i++){
		$id_mhs = $_POST['id_mhs'.$i];
		$posisi_ijasah = $_POST['cek'.$i];
		$seri_kertas = $_POST['seri_kertas'.$i];
		$id_jenjang = $_POST['jenjang'.$i];
		$seri_hidden = $_POST['seri_hidden'.$i];
		
		$db->Query("SELECT COUNT(*) AS ADA FROM PENGAJUAN_WISUDA 
					JOIN MAHASISWA ON MAHASISWA.ID_MHS = PENGAJUAN_WISUDA.ID_MHS
					JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
					WHERE NO_SERI_KERTAS = '$seri_kertas' AND PROGRAM_STUDI.ID_FAKULTAS = '$_POST[fakultas]' AND PROGRAM_STUDI.ID_JENJANG = '$id_jenjang'");
		$cek = $db->FetchAssoc();				
		
		if($cek['ADA'] >= 1 and $seri_hidden == ''){
				echo "No seri kertas $seri_kertas sudah ada di fakultas<br />";
		}else{
		
						
			if($posisi_ijasah <> ''){
			
				if($posisi_ijasah == 1){
					$set = ", TGL_POSISI_IJASAH = to_date('$tgl', 'YYYY-MM-DD')";
				}elseif($posisi_ijasah == 2){
					$set = ", TGL_POSISI_IJASAH2 = to_date('$tgl', 'YYYY-MM-DD')";
				}elseif($posisi_ijasah == 3){
					$set = ", TGL_POSISI_IJASAH3 = to_date('$tgl', 'YYYY-MM-DD')";
				}elseif($posisi_ijasah == 4){
					$set = ", TGL_POSISI_IJASAH4 = to_date('$tgl', 'YYYY-MM-DD')";
				}elseif($posisi_ijasah == 5){
					$set = ", TGL_POSISI_IJASAH5 = to_date('$tgl', 'YYYY-MM-DD')";
				}elseif($posisi_ijasah == 6){
					$set = ", TGL_POSISI_IJASAH6 = to_date('$tgl', 'YYYY-MM-DD')";
				}elseif($posisi_ijasah == 7){
					$set = ", TGL_POSISI_IJASAH7 = to_date('$tgl', 'YYYY-MM-DD')";
				}elseif($posisi_ijasah == 8){
					$set = ", TGL_POSISI_IJASAH8 = to_date('$tgl', 'YYYY-MM-DD')";
				}elseif($posisi_ijasah == 9){
					$set = ", TGL_POSISI_IJASAH9 = to_date('$tgl', 'YYYY-MM-DD')";
				}else{
					$set = "";
				}
				
				
				$db->Query("SELECT POSISI_IJASAH FROM PENGAJUAN_WISUDA
							WHERE ID_MHS = '$id_mhs'");
				$row = $db->FetchAssoc();
				
				if($row['POSISI_IJASAH'] <> $posisi_ijasah){
					
				$db->Query("UPDATE PENGAJUAN_WISUDA SET POSISI_IJASAH = '$posisi_ijasah', NO_SERI_KERTAS = '$seri_kertas'
							$set
							WHERE ID_MHS = '$id_mhs'");
				}else{
				$db->Query("UPDATE PENGAJUAN_WISUDA SET NO_SERI_KERTAS = '$seri_kertas'
							WHERE ID_MHS = '$id_mhs'");
				}
			}else{
				$db->Query("UPDATE PENGAJUAN_WISUDA SET NO_SERI_KERTAS = '$seri_kertas'
							WHERE ID_MHS = '$id_mhs'");
			}
		
		}
		
		
	}
	
	
}


if(isset($_POST['periode']) and $_POST['fakultas'] <> ''){
		if($_POST['fakultas'] <> ''){
			$where  = " AND FAKULTAS.ID_FAKULTAS = '$_POST[fakultas]'";
		}else{
			$where  = "";
		}
		
		$batas_waktu = date('d-m-Y');
		
		$pengajuan = $db->QueryToArray("SELECT NIM_MHS, (GELAR_DEPAN || ' ' || NM_PENGGUNA || CASE WHEN GELAR_BELAKANG IS NULL THEN '' ELSE ', ' END || GELAR_BELAKANG) AS NM_PENGGUNA, JENJANG.ID_JENJANG,
								NM_JENJANG, NM_PROGRAM_STUDI, NM_FAKULTAS, PENGAJUAN_WISUDA.NO_IJASAH, 
								ABSTRAK_TA_CLOB, JUDUL_TA, TGL_LULUS_PENGAJUAN, PENGAJUAN_WISUDA.ID_MHS, POSISI_IJASAH, NO_SERI_KERTAS
								,TO_CHAR(TGL_LULUS_PENGAJUAN, 'DD-MM-YYYY') AS TGL_LULUS_PENGAJUAN, 
								(CASE WHEN LAHIR_IJAZAH IS NULL THEN initcap(NM_KOTA) || ', ' || TO_CHAR(TGL_LAHIR_PENGGUNA, 'YYYY-MM-DD') 
									ELSE LAHIR_IJAZAH END) AS LAHIR_IJAZAH,
								CASE WHEN 
									TO_DATE(SYSDATE, 'DD-MM-YYYY') >= TO_DATE(ADD_MONTHS(TGL_POSISI_IJASAH4, 3), 'DD-MM-YYYY') 
									THEN 1 ELSE 0 END AS BATAS_WAKTU,
								TO_CHAR(TGL_POSISI_IJASAH, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH, 
								TO_CHAR(TGL_POSISI_IJASAH2, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH2,
								TO_CHAR(TGL_POSISI_IJASAH3, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH3,
								TO_CHAR(TGL_POSISI_IJASAH4, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH4,
								TO_CHAR(TGL_POSISI_IJASAH5, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH5,
								TO_CHAR(TGL_POSISI_IJASAH6, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH6,
								TO_CHAR(TGL_POSISI_IJASAH7, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH7,
								TO_CHAR(TGL_POSISI_IJASAH8, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH8,
								TO_CHAR(TGL_POSISI_IJASAH9, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH9
								FROM PENGAJUAN_WISUDA
								LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PENGAJUAN_WISUDA.ID_MHS
								LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
								LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
								LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA
								LEFT JOIN KOTA ON KOTA.ID_KOTA = LAHIR_KOTA_MHS
								WHERE PERIODE_WISUDA.ID_TARIF_WISUDA = '$_POST[periode]'
								$where
								ORDER BY PENGAJUAN_WISUDA.NO_IJASAH, NM_JENJANG, NM_PROGRAM_STUDI");
		$smarty->assign('pengajuan', $pengajuan);	
		$smarty->assign('batas_waktu', $batas_waktu);	
	

}

$smarty->assign('id_periode', $_POST['periode']);
$smarty->assign('id_fakultas', $_POST['fakultas']);
$smarty->display("wisuda/wisuda-pengawalan.tpl");
?>
