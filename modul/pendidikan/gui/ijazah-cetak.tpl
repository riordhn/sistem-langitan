<div class="center_title_bar">Cetak Ijazah</div>
{literal}
<script>
$(document).ready(function() {
    $('a[target="_blank"]').unbind('click');
});
</script>
{/literal}
<table>
    <tr>
        <td>Fakultas</td>
        <td>{html_options name="fakultas" options=$fakultas}
            <button onclick="$('#ijazah-table').hide(); $('#ijazah-table').fadeIn('slow');">Tampilkan</button>
        </td>
    </tr>
</table>
<div id="ijazah-picture" style="display: none;">
    <img src="../../img/pendidikan/ijasah.jpg" width="650px" /><br/>
    <button onclick="$('#ijazah-table').fadeIn('slow'); $('#ijazah-picture').hide();">Kembali</button>
    <button>Cetak</button>
</div>
<table width="692px" id="ijazah-table" style="display: none">
    <tr>
        <th>NIM</th>
        <th>Nama</th>
        <th>Jurusan</th>
        <th width="70px">Aksi</th>
    </tr>
    <tr>
        <td>08002179</td>
        <td>Airlangga Putra</td>
        <td>S1 Matematika</td>
        <td><button onclick="$('#ijazah-picture').fadeIn('slow'); $('#ijazah-table').hide();">Lihat</button></td>
    </tr>
    <tr>
        <td>08002179</td>
        <td>Airlangga Putra</td>
        <td>S1 Matematika</td>
        <td><button onclick="$('#ijazah-picture').fadeIn('slow'); $('#ijazah-table').hide();">Lihat</button></td>
    </tr>
    <tr>
        <td>08002179</td>
        <td>Airlangga Putra</td>
        <td>S1 Matematika</td>
        <td><button onclick="$('#ijazah-picture').fadeIn('slow'); $('#ijazah-table').hide();">Lihat</button></td>
    </tr>
</table>

