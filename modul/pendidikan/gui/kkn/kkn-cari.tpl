<div class="center_title_bar">CARI KKN MHS</div>  
<form action="kkn-cari.php" method="post">
	<table width="700">
    	<tr>
            <td>NIM</td>
            <td><input type="text" name="nim" /></td>			
        	<td colspan="6" style="text-align:center"><input type="submit" value="Cari" /></td>
        </tr>
    </table>
</form>


{if isset($kkn_cari)}

<table>
<tr>
	<td colspan="3" style="text-align:center"><img src='../../foto_mhs/{$kkn_cari['NIM_MHS']}.JPG' width=150 height=200 vspace=1 hspace=2></td>
</tr>
<tr>
	<td>NIM</td>
	<td> : </td>
	<td>{$kkn_cari['NIM_MHS']}</td>
</tr>
<tr>
	<td>Nama</td>
	<td> : </td>
	<td>{$kkn_cari['NM_PENGGUNA']}</td>
</tr>
<tr>
	<td>Fakultas</td>
	<td> : </td>
	<td>{$kkn_cari['NM_FAKULTAS']}</td>
</tr>
<tr>
	<td>Prodi</td>
	<td> : </td>
	<td>{$kkn_cari['NM_PROGRAM_STUDI']}</td>
</tr>
<tr>
	<td>Periode KKN</td>
	<td> : </td>
	<td>{$kkn_cari['NAMA_ANGKATAN']}</td>
</tr>
<tr>
	<td>Jenis KKN</td>
	<td> : </td>
	<td>{$kkn_cari['JENIS_KKN']}</td>
</tr>
<tr>
	<td>Dosen Pembimbing Lapangan</td>
	<td> : </td>
	<td>{$kkn_cari['GELAR_DEPAN']} {$kkn_cari['NM_DOSEN']}, {$kkn_cari['GELAR_BELAKANG']}</td>
</tr>
<tr>
	<td>Kelompok</td>
	<td> : </td>
	<td>{$kkn_cari['NAMA_KELOMPOK']}</td>
</tr>
<tr>
	<td>Kelurahan</td>
	<td> : </td>
	<td>{$kkn_cari['NM_KELURAHAN']}</td>
</tr>
<tr>
	<td>Kecamatan</td>
	<td> : </td>
	<td>{$kkn_cari['NM_KECAMATAN']}</td>
</tr>
<tr>
	<td>Kota</td>
	<td> : </td>
	<td>{$kkn_cari['NM_KOTA']}</td>
</tr>
</table>

{else if isset($data_kosong)}
    <span>{$data_kosong}</span>
{/if}

