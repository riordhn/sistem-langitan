<div class="center_title_bar">REKAP KKN DPL</div>  
<form action="kkn-dpl.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode KKN</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_KKN_ANGKATAN}" {if $smarty.request.periode==$data.ID_KKN_ANGKATAN} selected="selected" {/if}>{$data.NAMA_ANGKATAN}
					</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>


{if isset($kkn_dpl)}

<table>
<tr>
	<th>No</th>
	<th>NIP Dosen</th>
	<th>Nama</th>
	<th>Fakultas</th>
	<th>Tempat</th>
	<th>Tugas</th>
	<th>Kota</th>
</tr>
{$no = 1}
{foreach $kkn_dpl as $data}
<tr>
	<td>{$no++}</td>
	<td>{$data.NIP_DOSEN}</td>
	<td>{$data.NM_PENGGUNA}</td>
	<td>{$data.SINGKATAN_FAKULTAS}</td>
	<td>{$data.TEMPAT}</td>
	<td>{$data.TUGAS}</td>
	<td>{$data.NM_KOTA}</td>
</tr>
{/foreach}
</table>

{/if}

