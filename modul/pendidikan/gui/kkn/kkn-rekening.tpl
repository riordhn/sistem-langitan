{literal}
<script type="text/javascript" src="../akademik/includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[0,0],[0,0]],
		}
	);
}
);
</script>
{/literal}
<div class="center_title_bar">REKAP NO REKENING KKN MHS</div>  
<form action="kkn-rekening.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode KKN</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_KKN_ANGKATAN}" {if $smarty.request.periode==$data.ID_KKN_ANGKATAN} selected="selected" {/if}>{$data.NAMA_ANGKATAN}
					</option>
					{/foreach}
            	</select>
            </td>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas">
					<option value="">Semua</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $smarty.request.fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
	    <td>
				Tampilan
			</td>
			<td>
				<select name="tampilan">
					<option value="all" {if $smarty.request.tampilan == 'all'} selected="selected"{/if}>Semua</option>
					<option value="no_rek" {if $smarty.request.tampilan == 'no_rek'} selected="selected"{/if}>No Rekening</option>
				</select>
		</td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>


{if isset($kkn_mhs)}

	<table id="myTable" class="tablesorter">
	<thead>
<tr>
	<th>No</th>
	<th>NIM</th>
	<th>Nama</th>
	<th>Fakultas</th>
	<th>Program Studi</th>
	<th>Kelurahan</th>
	<th>Kecamatan</th>
	<th>Kota</th>
	<th>No Rekening</th>
	<th>Bank</th>
	<th>Atas Nama</th>
    <th>Nilai</th>
</tr>
</thead>
	<tbody>
{$no = 1}
{foreach $kkn_mhs as $data}
<tr>
	<td>{$no++}</td>
	<td>{$data.NIM_MHS}</td>
	<td>{$data.NM_PENGGUNA}</td>
	<td>{$data.SINGKATAN_FAKULTAS}</td>
	<td>{$data.NM_PROGRAM_STUDI}</td>
	<td>{$data.NM_KELURAHAN}</td>
	<td>{$data.NM_KECAMATAN}</td>
	<td>{$data.NM_KOTA}</td>
	<td>{$data.NO_REKENING}</td>
	<td>{$data.BANK_REKENING}</td>
	<td>{$data.ATAS_NAMA_REKENING}</td>
    <td>{$data.NILAI_HURUF}</td>
</tr>
{/foreach}
</tbody>
</table>
{/if}

