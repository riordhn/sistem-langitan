<div class="center_title_bar">REKAP KKN MHS</div>  
<form action="kkn-mhs.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode KKN</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_KKN_ANGKATAN}" {if $smarty.request.periode==$data.ID_KKN_ANGKATAN} selected="selected" {/if}>{$data.NAMA_ANGKATAN}
					</option>
					{/foreach}
            	</select>
            </td>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas">
					<option value="">Semua</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $smarty.request.fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
			<td>
				Tampilan
			</td>
			<td>
				<select name="tampilan">
					<option value="fak" {if $smarty.request.tampilan == 'fak'} selected="selected"{/if}>Fakultas</option>
                    <option value="prodi" {if $smarty.request.tampilan == 'prodi'} selected="selected"{/if}>Prodi</option>
					<option value="kelurahan" {if $smarty.request.tampilan == 'kelurahan'} selected="selected"{/if}>Kelurahan</option>
					<option value="kecamatan" {if $smarty.request.tampilan == 'kecamatan'} selected="selected"{/if}>Kecamatan</option>
					<option value="kota" {if $smarty.request.tampilan == 'kota'} selected="selected"{/if}>Kota</option>
				</select>
			</td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>


{if isset($kkn_mhs)}

	{if $smarty.request.tampilan == 'kelurahan'}
	
	<table>
	<tr>
		<th>No</th>
		<th>Kelurahan</th>
		<th>Kecamatan</th>
		<th>Kota</th>
		<th>Reguler</th>
		<th>Tematik</th>
	</tr>
	{$no = 1}
	{foreach $kkn_mhs as $data}
	<tr>
		<td>{$no++}</td>
		<td>{$data.NM_KELURAHAN}</td>
		<td>{$data.NM_KECAMATAN}</td>
		<td>{$data.NM_KOTA}</td>
		<td>{$data.REGULER}</td>
		<td>{$data.TEMATIK}</td>
	</tr>
	{$total_reguler = $total_reguler + $data.REGULER}
	{$total_tematik = $total_tematik + $data.TEMATIK}
	{/foreach}
	<tr>
		<td colspan="4">Total</td>
		<td>{$total_reguler}</td>
		<td>{$total_tematik}</td>
	</tr>
	</table>	
	
	{elseif $smarty.request.tampilan == 'kecamatan'}
	
	<table>
	<tr>
		<th>No</th>
		<th>Kecamatan</th>
		<th>Kota</th>
		<th>Reguler</th>
		<th>Tematik</th>
	</tr>
	{$no = 1}
	{foreach $kkn_mhs as $data}
	<tr>
		<td>{$no++}</td>
		<td>{$data.NM_KECAMATAN}</td>
		<td>{$data.NM_KOTA}</td>
		<td>{$data.REGULER}</td>
		<td>{$data.TEMATIK}</td>
	</tr>
	{$total_reguler = $total_reguler + $data.REGULER}
	{$total_tematik = $total_tematik + $data.TEMATIK}
	{/foreach}
	<tr>
		<td colspan="3">Total</td>
		<td>{$total_reguler}</td>
		<td>{$total_tematik}</td>
	</tr>
	</table>
		
	{elseif $smarty.request.tampilan == 'kota'}
	
	<table>
	<tr>
		<th>No</th>
		<th>Kota</th>
		<th>Reguler</th>
		<th>Tematik</th>
	</tr>
	{$no = 1}
	{foreach $kkn_mhs as $data}
	<tr>
		<td>{$no++}</td>
		<td>{$data.NM_KOTA}</td>
		<td>{$data.REGULER}</td>
		<td>{$data.TEMATIK}</td>
	</tr>
	{$total_reguler = $total_reguler + $data.REGULER}
	{$total_tematik = $total_tematik + $data.TEMATIK}
	{/foreach}
	<tr>
		<td colspan="2">Total</td>
		<td>{$total_reguler}</td>
		<td>{$total_tematik}</td>
	</tr>
	</table>
	
	
	{else}
	<table>
	<tr>
		<th>No</th>
		<th>Fakultas</th>
		{if $smarty.request.tampilan == 'prodi'}
		<th>Program Studi</th>
		{/if}
		<th>Reguler</th>
		<th>Tematik</th>
	</tr>
	{$no = 1}
	{foreach $kkn_mhs as $data}
	<tr>
		<td>{$no++}</td>
		<td>{$data.NM_FAKULTAS}</td>
		{if $smarty.request.tampilan == 'prodi'}
		<td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
		{/if}
		<td><a href="kkn-mhs-detail.php?jenis=1&periode={$smarty.request.periode}&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}">{$data.REGULER}</a></td>
		<td><a href="kkn-mhs-detail.php?jenis=2&periode={$smarty.request.periode}&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}">{$data.TEMATIK}</a></td>
	</tr>
	{$total_reguler = $total_reguler + $data.REGULER}
	{$total_tematik = $total_tematik + $data.TEMATIK}
	{/foreach}
	<tr>
		<td {if $smarty.request.tampilan == 'prodi'} colspan="3"{else} colspan="2"{/if}>Total</td>
		<td><a href="kkn-mhs-detail.php?jenis=1&periode={$smarty.request.periode}">{$total_reguler}</a></td>
		<td><a href="kkn-mhs-detail.php?jenis=2&periode={$smarty.request.periode}">{$total_tematik}</a></td>
	</tr>
	</table>
	{/if}

{/if}

