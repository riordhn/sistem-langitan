<div class="center_title_bar">REKAP KKN MHS</div>  
<form action="kkn-mhs.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode KKN</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_KKN_ANGKATAN}" {if $smarty.request.periode==$data.ID_KKN_ANGKATAN} selected="selected" {/if}>{$data.NAMA_ANGKATAN}
					</option>
					{/foreach}
            	</select>
            </td>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas">
					<option value="">Semua</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $smarty.request.fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
			<td>
				Tampilan
			</td>
			<td>
				<select name="tampilan">
					<option value="fak" {if $smarty.request.tampilan == 'fak'} selected="selected"{/if}>Fakultas</option>
                    <option value="prodi" {if $smarty.request.tampilan == 'prodi'} selected="selected"{/if}>Prodi</option>
				</select>
			</td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>


{if isset($kkn_mhs_detail)}

<table>
<tr>
	<th>No</th>
	<th>NIM</th>
	<th>Nama</th>
	<th>Fakultas</th>
	<th>Program Studi</th>
	<th>Jenis KKN</th>
	<th>Kota</th>
	<th>Kecamatan</th>
	<th>kelurahan</th>
</tr>
{$no = 1}
{foreach $kkn_mhs_detail as $data}
<tr>
	<td>{$no++}</td>
	<td>{$data.NIM_MHS}</td>
	<td>{$data.NM_PENGGUNA}</td>
	<td>{$data.NM_FAKULTAS}</td>
	<td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
	<td>{$data.JENIS_KKN}</td>
	<td>{$data.NM_KOTA}</td>
	<td>{$data.NM_KECAMATAN}</td>
	<td>{$data.NM_KELURAHAN}</td>
</tr>
{/foreach}
</table>

{/if}

