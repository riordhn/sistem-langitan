<div class="center_title_bar">BATAS BAYAR MAHASISWA KKN</div>  
<form action="kkn-batas-bayar.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode KKN</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_KKN_ANGKATAN}" {if $smarty.request.periode==$data.ID_KKN_ANGKATAN} selected="selected" {/if}>{$data.NAMA_ANGKATAN}
					</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>


{if isset($kkn_batas_bayar)}
<form name="f2" id="f2" action="kkn-batas-bayar.php" method="post">
	<table>
		<tr>
			<td>Batas Pembayaran</td>
			<td><input type="text" id="batas_bayar" name="batas_bayar" class="required" /></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<input type="submit" value="Simpan" />
				<input type="hidden" value="{$smarty.post.periode}" name="periode" />
			</td>
		</tr>
	</table>

<table>
<tr>
	<th>No</th>
	<th>NIM</th>
	<th>Nama</th>
	<th>Fakultas</th>
	<th>Program Studi</th>
	<th>Kelurahan</th>
	<th>Kecamatan</th>
	<th>Kota</th>
	<th>Batas Bayar</th
</tr>
{$no = 1}
{foreach $kkn_batas_bayar as $data}
<tr>
	<td>{$no++}</td>
	<td>{$data.NIM_MHS}</td>
	<td>{$data.NM_PENGGUNA}</td>
	<td>{$data.SINGKATAN_FAKULTAS}</td>
	<td>{$data.NM_PROGRAM_STUDI}</td>
	<td>{$data.NM_KELURAHAN}</td>
	<td>{$data.NM_KECAMATAN}</td>
	<td>{$data.NM_KOTA}</td>
	<td>{$data.BATAS_BAYAR}</td>
</tr>
{/foreach}
</table>

</form>

{literal}
    <script>
            $("#batas_bayar").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$('#f2').validate();
    </script>
{/literal}

{/if}

