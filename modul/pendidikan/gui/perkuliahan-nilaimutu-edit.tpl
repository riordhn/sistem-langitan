<div class="center_title_bar">Rentang Nilai - Edit</div>
<form method="post" action="perkuliahan-nilaimutu.php">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_peraturan_nilai" value="{$peraturan_nilai->ID_PERATURAN_NILAI}" />
<table>
    <tr>
        <td>Peraturan</td>
        <td>{$peraturan_nilai->PERATURAN->NM_PERATURAN}</td>
    </tr>
    <tr>
        <td>Nilai Huruf</td>
        <td>{$peraturan_nilai->STANDAR_NILAI->NM_STANDAR_NILAI} [{$peraturan_nilai->STANDAR_NILAI->NILAI_STANDAR_NILAI}]</td>
    </tr>
    <tr>
        <td>Rentang Nilai Min</td>
        <td><input type="text" size="4" name="nilai_min" value="{$peraturan_nilai->NILAI_MIN_PERATURAN_NILAI}" /></td>
    </tr>
    <tr>
        <td>Rentang Nilai Max</td>
        <td><input type="text" size="4" name="nilai_max" value="{$peraturan_nilai->NILAI_MAX_PERATURAN_NILAI}" /></td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <button value="perkuliahan-nilaimutu.php">Kembali</button>
            <input type="submit" value="Simpan"/>
        </td>
    </tr>
</table>
</form>
{include 'debug.tpl'}