<div class="center_title_bar">Kode NIM Benua</div>
{if $smarty.get.mode == "tambah"}
	<form name="f2" id="f2" method="post" action="perkuliahan-benua.php">
    	<table>
        	<tr>
            	<td>
                	Nama
                </td>
                <td><input type="text" name="nama" class="required" /></td>
            </tr>
            <tr>
            	<td>
                	Kode
                </td>
                	<td><input type="text" name="kode" class="required" /></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center">
                	<input type="submit" value="Simpan" />
                	<input type="hidden" name="mode" value="insert" />
                </td>
            </tr>
        </table>
    </form>
{elseif $smarty.get.mode == "edit"}
	<form name="f2" id="f2" method="post" action="perkuliahan-benua.php">
    	<table>
        	<tr>
            	<td>
                	Nama
                </td>
                <td><input type="text" name="nama" class="required" value="{$edit.NM_BENUA}" /></td>
            </tr>
            <tr>
            	<td>
                	Kode
                </td>
                	<td><input type="text" name="kode" class="required" value="{$edit.KODE_NIM_ASING}" /></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center">
                	<input type="submit" value="Update" />
                	<input type="hidden" name="mode" value="update" />
                    <input type="hidden" name="id" value="{$smarty.get.id}" />
                </td>
            </tr>
        </table>
    </form>
{else}

<table>
	<tr>
    	<th>
        	No
        </th>
        <th>
        	Nama	
        </th>
        <th>
        	Kode
        </th>
        <th>
        	Aksi
        </th>
    </tr>
    {$no++}
    {foreach $benua as $data}
    <tr>
    	<td>{$no++}</td>
        <td>{$data.NM_BENUA}</td>
        <td>{$data.KODE_NIM_ASING}</td>
        <td><a href="perkuliahan-benua.php?mode=edit&id={$data.ID_BENUA}">Edit</a> | 
        	<a href="perkuliahan-benua.php?mode=hapus&id={$data.ID_BENUA}">Hapus</a></td>
    </tr>
    {/foreach}
    <tr>
    	<td colspan="4" style="text-align:center">
        	<input type="button" value="Tambah" onclick="location.href='#perkuliahan-benua!perkuliahan-benua.php?mode=tambah'"/>
        </td>
    </tr>
</table>
{/if}

{literal}
    <script>
			$("#f2").validate();
    </script>
{/literal}