<div class="center_title_bar">Master Predikat Kelulusan</div>

<table>
    <tr>
        <th>No</th>
        <th>Jenjang</th>
        <th>IPK Range</th>
        <th>Predikat</th>
        <th>Batas Studi</th>
        <!--
        <th>Aksi</th>
        -->
    </tr>
    {$i = 1}
    {foreach $mhs_predikat_set as $mhs_predikat}
    <tr>
        <td class="center">{$i++}</td>
        <td>{$mhs_predikat.NM_JENJANG}</td>
        <td class="center">{$mhs_predikat.IPK_MIN_MHS_PREDIKAT|string_format:"%.2f"} - {$mhs_predikat.IPK_MAX_MHS_PREDIKAT|string_format:"%.2f"}</td>
        <td>{$mhs_predikat.NM_MHS_PREDIKAT}</td>
        <td>{$mhs_predikat.MASA_STUDI_PREDIKAT}</td>
        <!--
        <td>
            <a href="perkuliahan-predikat.php?mode=edit&id_mhs_predikat={$mhs_predikat.ID_MHS_PREDIKAT}">Edit</a>
            <a href="perkuliahan-predikat.php?mode=delete&id_mhs_predikat={$mhs_predikat.ID_MHS_PREDIKAT}">Hapus</a>
        </td>
        -->
    </tr>
    {/foreach}
    <!--
    <tr>
        <td class="center" colspan="6"><button href="perkuliahan-predikat.php?mode=add">Tambah</button></td>
    </tr>
    -->
</table>