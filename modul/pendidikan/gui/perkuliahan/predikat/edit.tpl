<div class="center_title_bar">Master Predikat Kelulusan - Edit</div>

<form action="perkuliahan-predikat.php" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_mhs_predikat" value="{$mhs_predikat.ID_MHS_PREDIKAT}" />
    <table>
        <tr>
            <td>Jenjang</td>
            <td>
                <select name="id_jenjang">
                {foreach $jenjang_set as $jenjang}
                    <option value="{$jenjang.ID_JENJANG}" {if $mhs_predikat.ID_JENJANG==$jenjang.ID_JENJANG}selected="selected"{/if}>{$jenjang.NM_JENJANG}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>IPK Minimal</td>
            <td><input type="text" name="ipk_min_mhs_predikat" class="required number" value="{$mhs_predikat.IPK_MIN_MHS_PREDIKAT}" /></td>
        </tr>
        <tr>
            <td>IPK Maksimal</td>
            <td><input type="text" name="ipk_max_mhs_predikat" class="required number" value="{$mhs_predikat.IPK_MAX_MHS_PREDIKAT}" /></td>
        </tr>
        <tr>
            <td>Nama Predikat</td>
            <td><input type="text" name="nm_mhs_predikat" class="required" value="{$mhs_predikat.NM_MHS_PREDIKAT}" /></td>
        </tr>
         <tr>
            <td>Batas Studi</td>
            <td><input type="text" name="batas_studi" class="required" value="{$mhs_predikat.MASA_STUDI_PREDIKAT}" /></td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button href="perkuliahan-predikat.php">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script language="javascript">
$('form').validate();
</script>{/literal}