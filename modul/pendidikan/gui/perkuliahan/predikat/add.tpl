<div class="center_title_bar">Master Predikat Kelulusan - Tambah</div>

<form action="perkuliahan-predikat.php" method="post">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <td>Jenjang</td>
            <td>
                <select name="id_jenjang">
                {foreach $jenjang_set as $jenjang}
                    <option value="{$jenjang.ID_JENJANG}">{$jenjang.NM_JENJANG}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>IPK Minimal</td>
            <td><input type="text" name="ipk_min_mhs_predikat" class="required number"/></td>
        </tr>
        <tr>
            <td>IPK Maksimal</td>
            <td><input type="text" name="ipk_max_mhs_predikat" class="required number" /></td>
        </tr>
        <tr>
            <td>Nama Predikat</td>
            <td><input type="text" name="nm_mhs_predikat" class="required" /></td>
        </tr>
        <tr>
            <td>Batas Studi</td>
            <td><input type="text" name="batas_studi" class="required" /></td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button href="perkuliahan-predikat.php">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script language="javascript">
$('form').validate();
</script>{/literal}