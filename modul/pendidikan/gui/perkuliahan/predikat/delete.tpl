<div class="center_title_bar">Master Predikat Kelulusan - Hapus</div>
<h4>Apakah data predikat ini akan dihapus ?</h4>
<form action="perkuliahan-predikat.php" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="id_mhs_predikat" value="{$mhs_predikat.ID_MHS_PREDIKAT}" />
    <table>
        <tr>
            <td>Jenjang</td>
            <td>{$mhs_predikat.NM_JENJANG}</td>
        </tr>
        <tr>
            <td>IPK Minimal</td>
            <td>{$mhs_predikat.IPK_MIN_MHS_PREDIKAT}</td>
        </tr>
        <tr>
            <td>Nama Predikat</td>
            <td>{$mhs_predikat.NM_MHS_PREDIKAT}</td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button href="perkuliahan-predikat.php">Batal</button>
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>