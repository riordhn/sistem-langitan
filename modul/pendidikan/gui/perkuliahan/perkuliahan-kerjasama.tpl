<div class="center_title_bar">Kode NIM Kerjasama</div>
{if $smarty.get.mode == "tambah"}
	<form name="f2" id="f2" method="post" action="perkuliahan-kerjasama.php">
    	<table>
        	<tr>
            	<td>
                	Nama
                </td>
                <td><input type="text" name="nama" class="required" /></td>
            </tr>
            <tr>
            	<td>
                	Kode
                </td>
                	<td><input type="text" name="kode" class="required" /></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center">
                	<input type="submit" value="Simpan" />
                	<input type="hidden" name="mode" value="insert" />
                </td>
            </tr>
        </table>
    </form>
{elseif $smarty.get.mode == "edit"}
	<form name="f2" id="f2" method="post" action="perkuliahan-kerjasama.php">
    	<table>
        	<tr>
            	<td>
                	Nama
                </td>
                <td><input type="text" name="nama" class="required" value="{$edit.NM_JENIS_KERJASAMA}" /></td>
            </tr>
            <tr>
            	<td>
                	Kode
                </td>
                	<td><input type="text" name="kode" class="required" value="{$edit.KODE_NIM_ASING}" /></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center">
                	<input type="submit" value="Update" />
                	<input type="hidden" name="mode" value="update" />
                    <input type="hidden" name="id" value="{$smarty.get.id}" />
                </td>
            </tr>
        </table>
    </form>
{else}

<table>
	<tr>
    	<th>
        	No
        </th>
        <th>
        	Nama	
        </th>
        <th>
        	Kode
        </th>
        <th>
        	Aksi
        </th>
    </tr>
    {$no++}
    {foreach $benua as $data}
    <tr>
    	<td>{$no++}</td>
        <td>{$data.NM_JENIS_KERJASAMA}</td>
        <td>{$data.KODE_NIM_ASING}</td>
        <td><a href="perkuliahan-kerjasama.php?mode=edit&id={$data.ID_JENIS_KERJASAMA}">Edit</a>
    </tr>
    {/foreach}
    <tr>
    	<td colspan="4" style="text-align:center">
        	<input type="button" value="Tambah" onclick="location.href='#perkuliahan-kerjasama!perkuliahan-kerjasama.php?mode=tambah'"/>
        </td>
    </tr>
</table>
{/if}

{literal}
    <script>
			$("#f2").validate();
    </script>
{/literal}