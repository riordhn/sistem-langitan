{literal}
<script type="text/javascript" src="../akademik/includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#myTable").tablesorter(
		{
		sortList: [[2,0]],
		}
	);
}
);
</script>
{/literal}
<div class="center_title_bar">Kode NIM Negara</div>
{if $smarty.get.mode == "tambah"}
	<form name="f2" id="f2" method="post" action="perkuliahan-negara.php">
    	<table>
			<tr>
            	<td>
                	Benua
                </td>
                <td>
					<select name="benua" class="required">
						<option value="">Pilih Benua</option>
						{foreach $ben as $data}
							<option value="{$data.ID_BENUA}">{$data.NM_BENUA}</option>
						{/foreach}
					</select>
				</td>
            </tr>
        	<tr>
            	<td>
                	Negara
                </td>
                <td><input type="text" name="nama" class="required" /></td>
            </tr>
            <tr>
            	<td>
                	Kode
                </td>
                	<td><input type="text" name="kode" class="required" /></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center">
                	<input type="submit" value="Simpan" />
                	<input type="hidden" name="mode" value="insert" />
                </td>
            </tr>
        </table>
    </form>
{elseif $smarty.get.mode == "edit"}
	<form name="f2" id="f2" method="post" action="perkuliahan-negara.php">
    	<table>
        	<tr>
            	<td>
                	Benua
                </td>
                <td>
					<select name="benua" class="required">
						<option value="">Pilih Benua</option>
						{foreach $ben as $data}
							<option value="{$data.ID_BENUA}" {if $data.ID_BENUA == $edit.ID_BENUA} selected="selected"{/if}>{$data.NM_BENUA}</option>
						{/foreach}
					</select>
				</td>
            </tr>
			<tr>
            	<td>
                	Negara
                </td>
                <td><input type="text" name="nama" class="required" value="{$edit.NM_NEGARA}" /></td>
            </tr>
            <tr>
            	<td>
                	Kode
                </td>
                	<td><input type="text" name="kode" class="required" value="{$edit.KODE_NIM_IOP}" /></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center">
                	<input type="submit" value="Update" />
                	<input type="hidden" name="mode" value="update" />
                    <input type="hidden" name="id" value="{$smarty.get.id}" />
                </td>
            </tr>
        </table>
    </form>
{else}

<table id="myTable" class="tablesorter">
<thead>
	<tr>
    	<th>
        	No
        </th>
        <th>
        	Benua
        </th>
        <th>
        	Negara
        </th>
		<th>
        	Kode
        </th>
        <th class="noheader">
        	Aksi
        </th>
    </tr>
</thead>
    {$no++}
<tbody>    
    {foreach $benua as $data}
    <tr>
    	<td>{$no++}</td>
        <td>{$data.NM_BENUA}</td>
		<td>{$data.NM_NEGARA}</td>
        <td>{$data.KODE_NIM_IOP}</td>
        <td><a href="perkuliahan-negara.php?mode=edit&id={$data.ID_NEGARA}">Edit</a></td>
    </tr>
    {/foreach}
</tbody>    
    <tr>
    	<td colspan="8" style="text-align:center">
        	<input type="button" value="Tambah" onclick="location.href='#perkuliahan-negara!perkuliahan-negara.php?mode=tambah'"/>
        </td>
    </tr>
</table>
{/if}

{literal}
    <script>
			$("#f2").validate();
    </script>
{/literal}