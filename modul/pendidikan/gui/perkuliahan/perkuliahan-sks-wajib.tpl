<div class="center_title_bar">SKS WAJIB</div>
{if $smarty.get.mode == "tambah"}
	<form name="f2" id="f2" method="post" action="perkuliahan-sks-wajib.php">
    	<table>
        	<tr>
            	<td>
                	SKS
                </td>
                <td><input type="text" name="sks" class="required" /></td>
            </tr>
			<tr>
				<td>Fakultas</td>
				<td>
					<select name="fakultas" id="fakultas" class="required">
						<option value="">Semua</option>
						{foreach $fakultas as $data}
						<option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS == $smarty.request.fakultas}selected="selected"{/if}>{$data.NM_FAKULTAS}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Jenjang</td>
					<td>
						<select id="jenjang" name="jenjang" class="required">
							<option value="">Semua</option>
							{foreach $jenjang as $data}
								<option value="{$data.ID_JENJANG}" {if $smarty.request.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG|upper}</option>
							{/foreach}
						</select>
				</td>
			</tr>
			<tr>
			<td>Program Studi</td>
				<td>
					<select id="program_studi" name="program_studi" class="required">
						<option value="">Semua</option>
						{foreach $prodi as $data}
							<option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.request.program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
						{/foreach}
					</select>
			 </td>
			</tr>
            <tr>
            	<td>
                	Alih Jenis
                </td>
                	<td>
						<select name="jenis">
							<option value="R">Reguler</option>
							<option value="AJ">Alih Jenis</option>
						</select>
					</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center">
                	<input type="submit" value="Simpan" />
                	<input type="hidden" name="mode" value="insert" />
                </td>
            </tr>
        </table>
    </form>
{elseif $smarty.get.mode == "edit"}
	<form name="f2" id="f2" method="post" action="perkuliahan-kerjasama.php">
    	<table>
        	<tr>
            	<td>
                	SKS
                </td>
                <td><input type="text" name="sks" class="required" value="{$edit.SKS_DIWAJIBKAN}" /></td>
            </tr>
			<tr>
				<td>Fakultas</td>
				<td>
					<select name="fakultas" id="fakultas" class="required">
						<option value="">Semua</option>
						{foreach $fakultas as $data}
						<option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS == $smarty.request.fakultas or $data.ID_FAKULTAS == $edit.ID_FAKULTAS}selected="selected"{/if}>{$data.NM_FAKULTAS}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Jenjang</td>
					<td>
						<select id="jenjang" name="jenjang" class="required">
							<option value="">Semua</option>
							{foreach $jenjang as $data}
								<option value="{$data.ID_JENJANG}" {if $smarty.request.jenjang==$data.ID_JENJANG or $data.ID_JENJANG == $edit.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG|upper}</option>
							{/foreach}
						</select>
				</td>
			</tr>
			<tr>
			<td>Program Studi</td>
				<td>
					<select id="program_studi" name="program_studi" class="required">
						<option value="">Semua</option>
						{foreach $prodi as $data}
							<option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.request.program_studi==$data.ID_PROGRAM_STUDI or $data.ID_PROGRAM_STUDI == $edit.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
						{/foreach}
					</select>
			 </td>
			</tr>
            <tr>
            	<td>
                	Alih Jenis
                </td>
                	<td>
						<select name="jenis">
							<option value="R">Reguler</option>
							<option value="AJ">Alih Jenis</option>
						</select>
					</td>
            </tr>
            <tr>
                <td colspan="4" style="text-align:center">
                	<input type="submit" value="Update" />
                	<input type="hidden" name="mode" value="update" />
                    <input type="hidden" name="id" value="{$smarty.get.id}" />
                </td>
            </tr>
        </table>
    </form>
{else}

<table>
	<tr>
    	<th>
        	No
        </th>
        <th>
        	Fakultas	
        </th>
        <th>
        	Program Studi
        </th>
		<th>
        	Jenis
        </th>
		<th>
        	SKS Wajib
        </th>
        <th>
        	Aksi
        </th>
    </tr>
    {$no++}
    {foreach $sks as $data}
    <tr>
    	<td>{$no++}</td>
        <td>{$data.NM_FAKULTAS}</td>
        <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
		<td>{$data.ALIH_JALUR}</td>
		<td>{$data.SKS_DIWAJIBKAN}</td>
        <td><a href="perkuliahan-sks-wajib.php?mode=edit&id={$data.ID_SKS_DIWAJIBKAN}">Edit</a>
    </tr>
    {/foreach}
    <tr>
    	<td colspan="7" style="text-align:center">
        	<input type="button" value="Tambah" onclick="location.href='#perkuliahan-sks_wajib!perkuliahan-sks-wajib.php?mode=tambah'"/>
        </td>
    </tr>
</table>
{/if}

{literal}
    <script>
			$("#f2").validate();
			 $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
    </script>
{/literal}