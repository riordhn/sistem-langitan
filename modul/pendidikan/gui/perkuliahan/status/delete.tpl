<div class="center_title_bar">Master Status Mahasiswa - Hapus</div>
<h4>Apakah data status mahasiswa ini akan dihapus ?</h4>
<form action="perkuliahan-status.php" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="id_status_pengguna" value="{$status_pengguna.ID_STATUS_PENGGUNA}" />
    <table>
        <tr>
            <td>Nama Status</td>
            <td>{$status_pengguna.NM_STATUS_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Status Aktif</td>
            <td>{if $status_pengguna.STATUS_AKTIF==1}Aktif{else}Tidak Aktif{/if}</td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button href="perkuliahan-status.php">Batal</button>
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>