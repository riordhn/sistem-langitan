<div class="center_title_bar">Master Status Mahasiswa - Edit</div>

<form action="perkuliahan-status.php" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_status_pengguna" value="{$status_pengguna.ID_STATUS_PENGGUNA}" />
    <table>
        <tr>
            <td>Nama Status</td>
            <td><input type="text" name="nm_status_pengguna" class="required" value="{$status_pengguna.NM_STATUS_PENGGUNA}" /></td>
        </tr>
        <tr>
            <td>Status Aktif</td>
            <td>
                <select name="aktif_status_pengguna">
                    <option value="1" {if $status_pengguna.AKTIF_STATUS_PENGGUNA==1}selected="selected"{/if}>Aktif</option>
                    <option value="0" {if $status_pengguna.AKTIF_STATUS_PENGGUNA==0}selected="selected"{/if}>Tidak Aktif</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button href="perkuliahan-status.php">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>