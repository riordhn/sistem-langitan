<div class="center_title_bar">Master Status Pengguna - Tambah</div>

<form action="perkuliahan-status.php" method="post">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <td>Nama Status</td>
            <td><input type="text" name="nm_status_pengguna" class="required" /></td>
        </tr>
        <tr>
            <td>Status Aktif</td>
            <td>
                <select name="aktif_status_pengguna">
                    <option value="1">Aktif</option>
                    <option value="0">Tidak Aktif</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button href="perkuliahan-status.php">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>