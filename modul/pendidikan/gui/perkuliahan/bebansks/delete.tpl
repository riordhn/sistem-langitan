<div class="center_title_bar">Beban SKS - Delete</div>

<form action="perkuliahan-bebansks.php?mode=fakultas&id_fakultas={$beban_sks.ID_FAKULTAS}" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="id_beban_sks" value="{$beban_sks.ID_BEBAN_SKS}" />
    <table>
        <tr>
            <td>Fakultas</td>
            <td>{$beban_sks.NM_FAKULTAS}</td>
        </tr>
        <tr>
            <td>IPK Minimal</td>
            <td>{$beban_sks.IPK_MINIMUM}</td>
        </tr>
        <tr>
            <td>SKS Yang bisa diambil</td>
            <td>{$beban_sks.SKS_MAKSIMAL}</td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <button href="perkuliahan-bebansks.php?mode=fakultas&id_fakultas={$beban_sks.ID_FAKULTAS}">Batal</button>
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>