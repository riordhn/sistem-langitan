<div class="center_title_bar">Beban SKS - Edit</div>

<form action="perkuliahan-bebansks.php?mode=fakultas&id_fakultas={$beban_sks.ID_FAKULTAS}" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_fakultas" value="{$beban_sks.ID_FAKULTAS}" />
    <input type="hidden" name="id_beban_sks" value="{$beban_sks.ID_BEBAN_SKS}" />
    <table>
        <tr>
            <td>Fakultas</td>
            <td>{$beban_sks.NM_FAKULTAS}</td>
        </tr>
		<tr>
            <td>Program Studi</td>
            <td>
			<select name="id_program_studi">
				{foreach $program_studi as $data}
				 {if $data.ID_PROGRAM_STUDI == $beban_sks.ID_PROGRAM_STUDI}
				<option value="{$data.ID_PROGRAM_STUDI}" selected="selected">{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</option>
				{else}
				<option value="{$data.ID_PROGRAM_STUDI}">{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</option>
				{/if}
				{/foreach}
            </select>
            </td>
        </tr>
        <tr>
            <td>IPK Minimal</td>
            <td><input type="text" name="ipk" size="5" maxlength="4" class="required number" value="{$beban_sks.IPK_MINIMUM}" /></td>
        </tr>
        <tr>
            <td>SKS Yang bisa diambil</td>
            <td><input type="text" name="sks" size="3" maxlength="2" class="required number" value="{$beban_sks.SKS_MAKSIMAL}" /></td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <button href="perkuliahan-bebansks.php?mode=fakultas&id_fakultas={$beban_sks.ID_FAKULTAS}">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script language="javascript">
$('form').validate();
</script>{/literal}