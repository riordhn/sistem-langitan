<div class="center_title_bar">Beban SKS - Tambah</div>

<form action="perkuliahan-bebansks.php?mode=fakultas&id_fakultas={$fakultas.ID_FAKULTAS}" method="post">
    <input type="hidden" name="mode" value="add" />
    <input type="hidden" name="id_fakultas" value="{$fakultas.ID_FAKULTAS}" />
    <table>
        <tr>
            <td>Fakultas</td>
            <td>{$fakultas.NM_FAKULTAS}</td>
        </tr>
		<tr>
            <td>Program Studi</td>
            <td>
			<select name="id_program_studi">
				{foreach $program_studi as $data}
				<option value="{$data.ID_PROGRAM_STUDI}">{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</option>
				{/foreach}
            </select>
            </td>
        </tr>
        <tr>
            <td>IPK Minimal</td>
            <td><input type="text" name="ipk" size="5" maxlength="4" class="required number" /></td>
        </tr>
        <tr>
            <td>SKS Yang bisa diambil</td>
            <td><input type="text" name="sks" size="3" maxlength="2" class="required number" /></td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button href="perkuliahan-bebansks.php?mode=fakultas&id_fakultas={$fakultas.ID_FAKULTAS}">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script language="javascript">
$('form').validate();
</script>{/literal}