<div class="center_title_bar">Beban SKS_MAKSIMAL</div>
<h2>Fakultas : {$fakultas.NM_FAKULTAS}</h2>
<table>
    <tr>
        <th>No</th>
        <th>IPK Minimal</th>
        <th>SKS Maksimal</th>
		<th>Program Studi</th>
        <th>Aksi</th>
    </tr>
	{$i = 1}
    {foreach $beban_sks as $data}
    <tr>
        <td class="center">{$i++}</td>
        <td class="center">{$data.IPK_MINIMUM}</td>
        <td class="center">{$data.SKS_MAKSIMAL}</td>
		<td class="center">{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
        <td>
            <a href="perkuliahan-bebansks.php?mode=edit&id_beban_sks={$data.ID_BEBAN_SKS}">Edit</a>
            <a href="perkuliahan-bebansks.php?mode=delete&id_beban_sks={$data.ID_BEBAN_SKS}">Hapus</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="8">
            <a href="perkuliahan-bebansks.php?mode=add&id_fakultas={$fakultas.ID_FAKULTAS}">Tambah</a>
        </td>
    </tr>
</table>
<a href="perkuliahan-bebansks.php">Kembali ke pilihan fakultas</a>