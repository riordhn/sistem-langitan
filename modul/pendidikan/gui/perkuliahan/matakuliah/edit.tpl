<div class="center_title_bar">Daftar Mata Kuliah - Edit</div>
<form action="perkuliahan-matakuliah.php?mode=mk&id_program_studi={$mata_kuliah->PROGRAM_STUDI->ID_PROGRAM_STUDI}" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_mata_kuliah" value="{$mata_kuliah->ID_MATA_KULIAH}" />
    <input type="hidden" name="id_program_studi" value="{$mata_kuliah->PROGRAM_STUDI->ID_PROGRAM_STUDI}" />
    <table>
        <tr>
            <td>Prodi</td>
            <td>{$mata_kuliah->PROGRAM_STUDI->NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td>Kode</td>
            <td><input type="text" name="kode_mata_kuliah" size="10" maxlength="32" class="required" value="{$mata_kuliah->KODE_MATA_KULIAH}" /></td>
        </tr>
        <tr>
            <td>Nama Mata Kuliah</td>
            <td><input type="text" name="nm_mata_kuliah" size="50" maxlength="64" class="required" value="{$mata_kuliah->NM_MATA_KULIAH}"/> </td>
        </tr>
        <tr>
            <td>Nama Mata Kuliah (English)</td>
            <td><input type="text" name="nm_en_mata_kuliah" size="50" maxlength="64" class="required" value="{$mata_kuliah->NM_EN_MATA_KULIAH}"/></td>
        </tr>
        <tr>
            <td>Jumlah SKS</td>
            <td><input type="text" name="kredit_mata_kuliah" size="3" maxlength="2" class="required number" value="{$mata_kuliah->KREDIT_MATA_KULIAH}"/></td>
        </tr>
        <tr>
            <td>SKS Minimal</td>
            <td><input type="text" name="sks_min" size="3" maxlength="2" class="required number" value="{$mata_kuliah->SKS_MIN}"/></td>
        </tr>
        <tr>
            <td>IPK Minimal</td>
            <td><input type="text" name="ipk_min" size="5" maxlength="4" class="required number" value="{$mata_kuliah->IPK_MIN}"/></td>
        </tr>
        <tr>
            <td>Semester</td>
            <td><input type="text" name="tingkat_semester" size="3" maxlength="2" class="required number" value="{$mata_kuliah->TINGKAT_SEMESTER}"/></td>
        </tr>
        <tr>
            <td>Buka</td>
            <td><input type="text" name="buka" value="{$mata_kuliah->BUKA}" /></td>
        </tr>
        <tr>
            <td>Referensi</td>
            <td><input type="text" name="referensi" value="{$mata_kuliah->REFERENSI}" /></td>
        </tr>
        <tr>
            <td>Kelompok</td>
            <td>
                <select name="id_kelompok_mk">
                {for $i=0 to $kelompok_mk_set->Count()-1}
                    <option value="{$kelompok_mk_set->Get($i)->ID_KELOMPOK_MK}"
                            {if $kelompok_mk_set->Get($i)->ID_KELOMPOK_MK==$mata_kuliah->ID_KELOMPOK_MK}selected="selected"{/if}
                            >{$kelompok_mk_set->Get($i)->NM_KELOMPOK_MK}</option>
                {/for}
                </select>
            </td>
        </tr>
        <tr>
            <td>Status</td>
            <td>
                <select name="id_status_mk">
                {for $i=0 to $status_mk_set->Count()-1}
                    <option value="{$status_mk_set->Get($i)->ID_STATUS_MK}"
                            {if $status_mk_set->Get($i)->ID_STATUS_MK==$mata_kuliah->ID_STATUS_MK}selected="selected"{/if}
                            >{$status_mk_set->Get($i)->NM_STATUS_MK}</option>
                {/for}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <button href="perkuliahan-matakuliah.php?mode=mk&id_program_studi={$mata_kuliah->PROGRAM_STUDI->ID_PROGRAM_STUDI}">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script type="text/javascript">
    $('form').validate();
</script>{/literal}