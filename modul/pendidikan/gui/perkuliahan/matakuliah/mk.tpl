<div class="center_title_bar">Daftar Mata Kuliah</div>
<h2>Prodi : {$program_studi->NM_PROGRAM_STUDI}</h2>
<table>
    <tr>
        <th>No</th>
        <th>Kode</th>
        <th>Nama</th>
        <th>English</th>
        <th>SKS</th>
        <th>SKS Min</th>
        <th>IPK Min</th>
        <th>Smt</th>
        <th>Aksi</th>
    </tr>
    {for $i=0 to $program_studi->MATA_KULIAHs->Count()-1}
    {$mata_kuliah=$program_studi->MATA_KULIAHs->Get($i)}
    <tr>
        <td class="center">{$i+1}</td>
        <td>{$mata_kuliah->KODE_MATA_KULIAH}</td>
        <td>{$mata_kuliah->NM_MATA_KULIAH}</td>
        <td>{$mata_kuliah->NM_EN_MATA_KULIAH}</td>
        <td class="center" title="Jumlah SKS">{$mata_kuliah->KREDIT_MATA_KULIAH}</td>
        <td class="center" title="SKS Minimal">{$mata_kuliah->SKS_MIN}</td>
        <td class="center" title="IPK Minimal">{$mata_kuliah->IPK_MIN}</td>
        <td class="center" title="Semester">{$mata_kuliah->TINGKAT_SEMESTER}</td>
        <td>
            <a href="perkuliahan-matakuliah.php?mode=edit&id_mata_kuliah={$mata_kuliah->ID_MATA_KULIAH}">Edit</a>
            <a href="perkuliahan-matakuliah.php?mode=delete&id_mata_kuliah={$mata_kuliah->ID_MATA_KULIAH}">Hapus</a>
        </td>
    </tr>
    {/for}
    <tr>
        <td class="center" colspan="9"><button href="perkuliahan-matakuliah.php?mode=add&id_program_studi={$program_studi->ID_PROGRAM_STUDI}">Tambah</button></td>
    </tr>
</table>
<button href="perkuliahan-matakuliah.php?mode=prodi&id_fakultas={$program_studi->ID_FAKULTAS}">Kembali ke pilihan Prodi</button>
