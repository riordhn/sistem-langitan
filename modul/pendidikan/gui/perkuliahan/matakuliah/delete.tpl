<div class="center_title_bar">Daftar Mata Kuliah - Hapus</div>
<h4>Apakah data mata kuliah ini akan dihapus ?</h4>
<form action="perkuliahan-matakuliah.php?mode=mk&id_program_studi={$mata_kuliah->PROGRAM_STUDI->ID_PROGRAM_STUDI}" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="id_mata_kuliah" value="{$mata_kuliah->ID_MATA_KULIAH}" />
    <table>
        <tr>
            <td>Prodi</td>
            <td>{$mata_kuliah->PROGRAM_STUDI->NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td>Kode</td>
            <td>{$mata_kuliah->KODE_MATA_KULIAH}</td>
        </tr>
        <tr>
            <td>Nama Mata Kuliah</td>
            <td>{$mata_kuliah->NM_MATA_KULIAH}</td>
        </tr>
        <tr>
            <td>Nama Mata Kuliah (English)</td>
            <td>{$mata_kuliah->NM_EN_MATA_KULIAH}</td>
        </tr>
        <tr>
            <td>Jumlah SKS</td>
            <td>{$mata_kuliah->KREDIT_MATA_KULIAH}</td>
        </tr>
        <tr>
            <td>SKS Minimal</td>
            <td>{$mata_kuliah->SKS_MIN}</td>
        </tr>
        <tr>
            <td>IPK Minimal</td>
            <td>{$mata_kuliah->IPK_MIN}</td>
        </tr>
        <tr>
            <td>Semester</td>
            <td>{$mata_kuliah->TINGKAT_SEMESTER}</td>
        </tr>
        <tr>
            <td>Buka</td>
            <td>{$mata_kuliah->BUKA}</td>
        </tr>
        <tr>
            <td>Referensi</td>
            <td>{$mata_kuliah->REFERENSI}</td>
        </tr>
        <tr>
            <td>Kelompok</td>
            <td>{$mata_kuliah->KELOMPOK_MK->NM_KELOMPOK_MK}</td>
        </tr>
        <tr>
            <td>Status</td>
            <td>{$mata_kuliah->STATUS_MK->NM_STATUS_MK}</td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <button href="perkuliahan-matakuliah.php?mode=mk&id_program_studi={$mata_kuliah->PROGRAM_STUDI->ID_PROGRAM_STUDI}">Batal</button>
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>