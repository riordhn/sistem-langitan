<div class="center_title_bar">Daftar Mata Kuliah - Tambah</div>
<form action="perkuliahan-matakuliah.php?mode=mk&id_program_studi={$program_studi->ID_PROGRAM_STUDI}" method="post">
    <input type="hidden" name="mode" value="add" />
    <input type="hidden" name="id_program_studi" value="{$program_studi->ID_PROGRAM_STUDI}" />
    <table>
        <tr>
            <td>Prodi</td>
            <td>{$program_studi->NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td>Kode</td>
            <td><input type="text" name="kode_mata_kuliah" size="10" maxlength="32" class="required" /></td>
        </tr>
        <tr>
            <td>Nama Mata Kuliah</td>
            <td><input type="text" name="nm_mata_kuliah" size="50" maxlength="64" class="required"/> </td>
        </tr>
        <tr>
            <td>Nama Mata Kuliah (English)</td>
            <td><input type="text" name="nm_en_mata_kuliah" size="50" maxlength="64"  class="required"/></td>
        </tr>
        <tr>
            <td>Jumlah SKS</td>
            <td><input type="text" name="kredit_mata_kuliah" size="3" maxlength="2" class="required number"/></td>
        </tr>
        <tr>
            <td>SKS Minimal</td>
            <td><input type="text" name="sks_min" size="3" maxlength="2" class="required number"/></td>
        </tr>
        <tr>
            <td>IPK Minimal</td>
            <td><input type="text" name="ipk_min" size="5" maxlength="4" class="required number"/></td>
        </tr>
        <tr>
            <td>Semester</td>
            <td><input type="text" name="tingkat_semester" size="3" maxlength="2" class="required number"/></td>
        </tr>
        <tr>
            <td>Buka</td>
            <td><input type="text" name="buka" /></td>
        </tr>
        <tr>
            <td>Referensi</td>
            <td><input type="text" name="referensi" /></td>
        </tr>
        <tr>
            <td>Kelompok</td>
            <td>
                <select name="id_kelompok_mk">
                {for $i=0 to $kelompok_mk_set->Count()-1}
                    <option value="{$kelompok_mk_set->Get($i)->ID_KELOMPOK_MK}">{$kelompok_mk_set->Get($i)->NM_KELOMPOK_MK}</option>
                {/for}
                </select>
            </td>
        </tr>
        <tr>
            <td>Status</td>
            <td>
                <select name="id_status_mk">
                {for $i=0 to $status_mk_set->Count()-1}
                    <option value="{$status_mk_set->Get($i)->ID_STATUS_MK}">{$status_mk_set->Get($i)->NM_STATUS_MK}</option>
                {/for}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <button href="perkuliahan-matakuliah.php?mode=mk&id_program_studi={$program_studi->ID_PROGRAM_STUDI}">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script type="text/javascript">
    $('form').validate();
</script>{/literal}