<div class="center_title_bar">Master Program Studi - Tambah</div>

<form action="perkuliahan-prodi.php" method="post" id="prod">
    <input type="hidden" name="mode" value="add" />
    <table>
	    <tr>
            <td>Fakultas</td>
            <td>
            	<select name="fakultas" class="required">
                	<option value="">-- Pilih Fakultas --</option>
                	{foreach $fakultas as $data}
            		<option value="{$data.ID_FAKULTAS}">{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Jenjang</td>
            <td>
            	<select name="jenjang" class="required">
                	<option value="">-- Pilih Jenjang --</option>
                	{foreach $jenjang as $data}
            		<option value="{$data.ID_JENJANG}">{$data.NM_JENJANG}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama Prodi</td>
            <td><input name="nm_prodi" type="text" class="required" size="80" /></td>
        </tr>
		<tr>
            <td>Nama Prodi Inggris</td>
            <td><input name="nm_prodi_en" type="text" size="80" /></td>
        </tr>
        <tr>
            <td>No SK</td>
            <td><input name="no_sk" type="text" class="required" size="50" /></td>
        </tr>
        <tr>
            <td>Tgl Pendirian</td>
            <td><input name="tgl_pendirian" type="text" id="tgl_pendirian" class="datepicker"/></td>
        </tr>
        <tr>
            <td>Masa Berlaku</td>
            <td><input name="masa_berlaku" type="text" class="required number"  /> Tahun</td>
        </tr>
        <tr>
            <td>Gelar Panjang</td>
            <td><input name="gelar_panjang" type="text" class="required"  size="50" /></td>
        </tr>
        <tr>
            <td>Gelar Pendek</td>
            <td><input name="gelar_pendek" type="text" class="required"  /></td>
        </tr>
		<tr>
            <td>Gelar Inggris</td>
            <td><input name="gelar_inggris" type="text" size="50"  /></td>
        </tr>
		<tr>
            <td>Status Prodi</td>
          	<td>
				<input type="radio" value="1" name="status_prodi" checked="checked" />
				Aktif
            	<input type="radio" value="0" name="status_prodi" />
            	Non Aktif 
			</td>
        </tr>
		<tr>
            <td>Kode Forlap</td>
            <td>
                <input name="kode_prodi" type="text" class="required" />
            </td>
        </tr>
        <tr>
            <td>Kode Prodi NIM</td>
            <td>
                <input name="kode_nim_prodi" type="text" />
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}
<script language="javascript">
$('form').validate();
</script>
{/literal}
{literal}
    <script>
            /**
            $('#prod').submit(function() {
                    if($('#tgl_pendirian').val()==''){
                            $('#alert').fadeIn();
                    return false;
                    }  
            });
            */
            $('#prod').validate();
            $( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy',changeMonth: true,
			changeYear: true});
    </script>
{/literal}