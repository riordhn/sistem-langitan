<div class="center_title_bar">Master Program Studi - Edit</div>

<form action="perkuliahan-prodi.php" method="post" id="prod">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_prodi" value="{$prodi_set[0]['ID_PROGRAM_STUDI']}" />
    <table>
	    <tr>
            <td>Fakultas</td>
            <td>
            	<select name="fakultas" class="required">
                	<option value="">-- Pilih Fakultas --</option>
                	{foreach $fakultas as $data}
            		<option value="{$data.ID_FAKULTAS}" {if $prodi_set[0]['ID_FAKULTAS']==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Jenjang</td>
            <td>
            	<select name="jenjang" class="required">
                	<option value="">-- Pilih Jenjang --</option>
                	{foreach $jenjang as $data}
            		<option value="{$data.ID_JENJANG}" {if $prodi_set[0]['ID_JENJANG']==$data.ID_JENJANG} selected="selected" {/if}>{$data.NM_JENJANG}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama Prodi</td>
            <td><input name="nm_prodi" type="text" class="required" value="{$prodi_set[0]['NM_PROGRAM_STUDI']}" size="80" /></td>
        </tr>
		<tr>
            <td>Nama Prodi Inggris</td>
            <td><input name="nm_prodi_en" type="text" class="required" value="{$prodi_set[0]['NM_PROGRAM_STUDI_ENG']}" size="80" /></td>
        </tr>
        <tr>
            <td>No SK</td>
            <td><input name="no_sk" type="text"  value="{$prodi_set[0]['NO_SK']}" size="50" /></td>
        </tr>
        <tr>
            <td>Tgl Pendirian</td>
            <td><input name="tgl_pendirian" type="text" value="{$prodi_set[0]['TGL_PENDIRIAN1']}" id="tgl_pendirian" class="datepicker"/></td>
        </tr>
        <tr>
            <td>Masa Berlaku</td>
            <td><input name="masa_berlaku" type="text" class="required number" value="{$prodi_set[0]['MASA_BERLAKU']}"  /></td>
        </tr>
        <tr>
            <td>Gelar Panjang</td>
            <td><input name="gelar_panjang" type="text" class="required" value="{$prodi_set[0]['GELAR_PANJANG']}" size="50" /></td>
        </tr>
        <tr>
            <td>Gelar Pendek</td>
            <td><input name="gelar_pendek" type="text" class="required" value="{$prodi_set[0]['GELAR_PENDEK']}" /></td>
        </tr>
		<tr>
            <td>Gelar Inggris</td>
            <td><input name="gelar_inggris" type="text" class="required" value="{$prodi_set[0]['GELAR_INGGRIS']}" /></td>
        </tr>
		<tr>
            <td>Status Prodi</td>
          	<td>
				<input type="radio" value="1" name="status_prodi" {if $prodi_set[0]['STATUS_AKTIF_PRODI'] == 1}checked="checked"{/if} />
				Aktif
            	<input type="radio" value="0" name="status_prodi" {if $prodi_set[0]['STATUS_AKTIF_PRODI'] != 1}checked="checked"{/if} />
            	Non Aktif 
			</td>
        </tr>
		<tr>
            <td>Kode Forlap</td>
          	<td>
				<input name="kode_prodi" type="text" class="required"  value="{$prodi_set[0]['KODE_PROGRAM_STUDI']}" />
			</td>
        </tr>
        <tr>
            <td>Kode Prodi NIM</td>
            <td>
                <input name="kode_nim_prodi" type="text" value="{$prodi_set[0]['KODE_NIM_PRODI']}" />
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script language="javascript">
$('form').validate();
</script>{/literal}
{literal}
    <script>
/*            $('#prod').submit(function() {
                    if($('#tgl_pendirian').val()==''){
                            $('#alert').fadeIn();
                    return false;
                    }  
            });*/
            $( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy',changeMonth: true,
			changeYear: true});
    </script>
{/literal}