<div class="center_title_bar">Master Program Studi</div>

<form action="perkuliahan-prodi.php" method="post">
<table>
	<tr>
		<td>Fakultas </td>
		<td>
			<select name="fakultas">
				<option value="">Semua</option>
				{foreach $fakultas as $data}
				<option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS == $id_fakultas} selected="selected"{/if}>{$data.NM_FAKULTAS}</option>
				{/foreach}
			</select>
		</td>
		<td><input type="submit" value="Tampil" /></td>
	</tr>
</table>
</form>
<div style="overflow: scroll;height: 550px;">
<table>
    <tr>
        <th>No</th>
        <th>Fakultas</th>
        <th>Jenjang</th>
        <th>Program Studi</th>
		<th>Program Studi Eng</th>
        <th>No Sk</th>
        <th>Tgl Pendirian</th>
        <th>Masa Berlaku</th>
        <th>Gelar Panjang</th>
        <th>Gelar Pendek</th>
		<th>Gelar Inggris</th>
		<th>Status Prodi</th>
        <th>Aksi</th>
    </tr>
    {$i = 1}
    {foreach $prodi_set as $prodi}
	{if $prodi.STATUS_AKTIF_PRODI == 1}
		{$status = 'Aktif'}
	{else}
		{$status = 'Non Aktif'}
	{/if}
    <tr>
        <td class="center">{$i++}</td>
        <td>{$prodi.NM_FAKULTAS}</td>
        <td>{$prodi.NM_JENJANG}</td>
        <td>{$prodi.NM_PROGRAM_STUDI}</td>
		<td>{$prodi.NM_PROGRAM_STUDI_ENG}</td>
        <td>{$prodi.NO_SK}</td>
        <td>{$prodi.TGL_PENDIRIAN}</td>
        <td>{$prodi.MASA_BERLAKU}</td>
        <td>{$prodi.GELAR_PANJANG}</td>
        <td>{$prodi.GELAR_PENDEK}</td>
		<td>{$prodi.GELAR_INGGRIS}</td>
		<td>{$status}</td>
        <td>
            <a href="perkuliahan-prodi.php?mode=edit&id_prodi={$prodi.ID_PROGRAM_STUDI}">Edit</a>
            <!--a href="perkuliahan-prodi.php?mode=delete&id_prodi={$prodi.ID_PROGRAM_STUDI}">Hapus</a-->
        </td>
    </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="20"><input type=button onClick="parent.location='#perkuliahan-prodi!perkuliahan-prodi.php?mode=add'" value='Tambah' /></td>
    </tr>
</table>
</div>