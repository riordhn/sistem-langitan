<div class="center_title_bar">Master Jam Kuliah - Hapus</div>
<h4>Apakah data jam ini akan di hapus ?</h4>
<form action="perkuliahan-jam.php?mode=jadwal&id_fakultas={$jadwal_jam.ID_FAKULTAS}" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="id_jadwal_jam" value="{$jadwal_jam.ID_JADWAL_JAM}" />
    <table>
        <tr>
            <td>Nama Jam</td>
            <td>{$jadwal_jam.NM_JADWAL_JAM}</td>
        </tr>
        <tr>
            <td>Waktu Mulai</td>
            <td>{$jadwal_jam.JAM_MULAI}:{$jadwal_jam.MENIT_MULAI}</td>
        </tr>
        <tr>
            <td>Waktu Selesai</td>
            <td>{$jadwal_jam.JAM_SELESAI}:{$jadwal_jam.MENIT_SELESAI}</td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <button href="perkuliahan-jam.php?mode=jadwal&id_fakultas={$jadwal_jam.ID_FAKULTAS}">Batal</button>
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>