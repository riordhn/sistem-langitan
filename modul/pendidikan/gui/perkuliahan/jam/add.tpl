<div class="center_title_bar">Master Jam Kuliah - Tambah</div>
<h2>Fakultas : {$fakultas.NM_FAKULTAS}</h2>

<form action="perkuliahan-jam.php?mode=jadwal&id_fakultas={$fakultas.ID_FAKULTAS}" method="post">
    <input type="hidden" name="mode" value="add" />
    <input type="hidden" name="id_fakultas" value="{$fakultas.ID_FAKULTAS}" />
    <table>
        <tr>
            <td>Nama Jam</td>
            <td><input type="text" name="nm_jadwal_jam" class="required" /></td>
        </tr>
        <tr>
            <td>Waktu Mulai</td>
            <td>
                <input type="text" name="jam_mulai" size="2" maxlength="2" class="required number"/>:
                <input type="text" name="menit_mulai" size="2" maxlength="2" class="required number"/>
            </td>
        </tr>
        <tr>
            <td>Waktu Selesai</td>
            <td>
                <input type="text" name="jam_selesai" size="2" maxlength="2" class="required number"/>:
                <input type="text" name="menit_selesai" size="2" maxlength="2" class="required number"/>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <button href="perkuliahan-jam.php?mode=jadwal&id_fakultas={$fakultas.ID_FAKULTAS}">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script type="text/javascript">
    $('form').validate();
</script>{/literal}