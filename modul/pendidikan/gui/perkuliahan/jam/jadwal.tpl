<div class="center_title_bar">Master Jam Kuliah</div>
<h2>Fakultas : {$fakultas[0]['NM_FAKULTAS']}</h2>

<table>
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Waktu Mulai</th>
        <th>Waktu Selesai</th>
        <th>Aksi</th>
    </tr>
    {$i=1}
    {foreach $fakultas as $jadwal_jam}
    <tr>
        <td class="center">{$i++}</td>
        <td>{$jadwal_jam.NM_JADWAL_JAM}</td>
        <td class="center">{$jadwal_jam.JAM_MULAI}:{$jadwal_jam.MENIT_MULAI}</td>
        <td class="center">{$jadwal_jam.JAM_SELESAI}:{$jadwal_jam.MENIT_SELESAI}</td>
        <td>
            <a href="perkuliahan-jam.php?mode=edit&id_jadwal_jam={$jadwal_jam.ID_JADWAL_JAM}">Edit</a>
            <a href="perkuliahan-jam.php?mode=delete&id_jadwal_jam={$jadwal_jam.ID_JADWAL_JAM}">Hapus</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="5">
            <a href="perkuliahan-jam.php?mode=add&id_fakultas={$smarty.get.id_fakultas}">Tambah</a>
        </td>
    </tr>
</table>
<a href="perkuliahan-jam.php">Kembali ke pilihan fakultas</a>