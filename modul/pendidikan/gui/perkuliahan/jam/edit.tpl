<div class="center_title_bar">Master Jam Kuliah - Edit</div>
<h2>Fakultas : {$jadwal_jam.NM_FAKULTAS}</h2>

<form action="perkuliahan-jam.php?mode=jadwal&id_fakultas={$jadwal_jam.ID_FAKULTAS}" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_jadwal_jam" value="{$jadwal_jam.ID_JADWAL_JAM}" />
    <table>
        <tr>
            <td>Nama Jam</td>
            <td><input type="text" name="nm_jadwal_jam" class="required" value="{$jadwal_jam.NM_JADWAL_JAM}" /></td>
        </tr>
        <tr>
            <td>Waktu Mulai</td>
            <td>
                <input type="text" name="jam_mulai" size="2" maxlength="2" class="required number" value="{$jadwal_jam.JAM_MULAI}" />:
                <input type="text" name="menit_mulai" size="2" maxlength="2" class="required number" value="{$jadwal_jam.MENIT_MULAI}" />
            </td>
        </tr>
        <tr>
            <td>Waktu Selesai</td>
            <td>
                <input type="text" name="jam_selesai" size="2" maxlength="2" class="required number" value="{$jadwal_jam.JAM_SELESAI}"/>:
                <input type="text" name="menit_selesai" size="2" maxlength="2" class="required number" value="{$jadwal_jam.MENIT_SELESAI}"/>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <button href="perkuliahan-jam.php?mode=jadwal&id_fakultas={$jadwal_jam.ID_FAKULTAS}">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script type="text/javascript">
    $('form').validate();
</script>{/literal}