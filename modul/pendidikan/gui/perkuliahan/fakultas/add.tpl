<div class="center_title_bar">Master Fakultas - Tambah</div>

<form action="perkuliahan-fakultas.php" method="post">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <td>Nama Fakultas</td>
            <td><input type="text" name="nm_fakultas" class="required" /><br/>* Nama Fakultasnya saja, misal: Psikologi, tanpa di imbuhi kata "Fakultas"</td>
        </tr>
        <tr>
            <td>Nama Fakultas Inggris</td>
            <td><input type="text" name="nm_fakultas_en" /></td>
        </tr>
       <tr>
            <td>Nama Singkat</td>
            <td><input type="text" name="nm_singkat" /></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td><input type="text" name="alamat" /></td>
        </tr>
        <tr>
            <td>Alamat EN</td>
            <td><input type="text" name="alamat_en" /></td>
        </tr>
        <tr>
            <td>Kode NIM Fakultas</td>
            <td>
                <input type="text" name="kode_nim_fakultas" />
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script language="javascript">
$('form').validate();
</script>{/literal}