<div class="center_title_bar">Master Fakultas - Edit</div>

<form action="perkuliahan-fakultas.php" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_fakultas" value="{$fakultas[0]['ID_FAKULTAS']}" />
    <table>
		<!-- <tr>
            <td>No Fakultas</td>
            <td><input type="text" name="no_fakultas" class="required number" value="{$fakultas[0]['ID_FAKULTAS']}" /></td>
        </tr> -->
        <tr>
            <td>Nama Fakultas</td>
            <td><input type="text" name="nm_fakultas" class="required" value="{$fakultas[0]['NM_FAKULTAS']}" /></td>
        </tr>
       <tr>
            <td>Nama Fakultas Inggris</td>
            <td><input type="text" name="nm_fakultas_en" value="{$fakultas[0]['NM_FAKULTAS_ENG']}" /></td>
        </tr>
       <tr>
            <td>Nama Singkat</td>
            <td><input type="text" name="nm_singkat" value="{$fakultas[0]['SINGKATAN_FAKULTAS']}" /></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td><input type="text" name="alamat" value="{$fakultas[0]['ALAMAT_FAKULTAS']}" size="100" /></td>
        </tr>
        <tr>
            <td>Alamat EN</td>
            <td><input type="text" name="alamat_en" value="{$fakultas[0]['ALAMAT_FAKULTAS_ENG']}" size="100" /></td>
        </tr>
        <tr>
            <td>Kode NIM Fakultas</td>
            <td>
                <input name="kode_nim_fakultas" type="text" value="{$fakultas[0]['KODE_NIM_FAKULTAS']}" />
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script language="javascript">
$('form').validate();
</script>{/literal}