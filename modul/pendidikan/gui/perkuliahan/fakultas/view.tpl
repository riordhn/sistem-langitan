<div class="center_title_bar">Master Fakultas</div>
<form method="get" action="perkuliahan-fakultas.php">
<table>
    <tr>
        <th>No</th>
        <th>Nama Fakultas</th>
        <th>Nama Fakultas Inggris</th>
        <th>Nama Singkat</th>
        <th>Alamat</th>
        <th>Alamat EN</th>
        <th>Aksi</th>
    </tr>
    {$i = 1}
    {foreach $fakultas_set as $fakultas}
    <tr>
        <td class="center">{$i++}</td>
        <td>{$fakultas.NM_FAKULTAS}</td>
        <td>{$fakultas.NM_FAKULTAS_ENG}</td>
        <td>{$fakultas.SINGKATAN_FAKULTAS}</td>
        <td>{$fakultas.ALAMAT_FAKULTAS}</td>
        <td>{$fakultas.ALAMAT_FAKULTAS_ENG}</td>
        <td>
            <a href="perkuliahan-fakultas.php?mode=edit&id_fakultas={$fakultas.ID_FAKULTAS}">Edit</a>
          <!--  <a href="perkuliahan-fakultas.php?mode=delete&id_fakultas={$fakultas.ID_FAKULTAS}">Hapus</a>-->
        </td>
    </tr>
    {/foreach}
    
    <tr>
        <td class="center" colspan="9">
        	<input type="submit" value="Tambah" />
            <input type="hidden" value="add" name="mode" />
        </td>
    </tr>
    
</table>
</form>