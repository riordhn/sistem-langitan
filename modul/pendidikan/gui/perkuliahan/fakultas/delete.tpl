<div class="center_title_bar">Master Fakultas - Hapus</div>
<h4>Apakah data fakultas ini akan dihapus ?</h4>
<form action="perkuliahan-fakultas.php" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="id_fakultas" value="{$fakultas[0]['ID_FAKULTAS']}" />
    <table>
        <tr>
            <td>Nama Fakultas</td>
            <td>{$fakultas[0]['NM_FAKULTAS']}</td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button href="perkuliahan-fakultas.php">Batal</button>
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>