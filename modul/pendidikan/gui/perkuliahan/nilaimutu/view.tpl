<div class="center_title_bar">Rentang Nilai</div>
<table>
    <tr>
        <th>Nilai Huruf</th>
        <th>Nilai Mutu</th>
        <th>Rentang Nilai Min</th>
        <th>Rentang Nilai Maks</th>
        <th>Aksi</th>
    </tr>
        {foreach $peraturan as $nilai}
        <tr>
            <td class="center">{$nilai.NM_STANDAR_NILAI}</td>
            <td class="center">{$nilai.NILAI_STANDAR_NILAI}</td>
            <td class="center">{$nilai.NILAI_MIN_PERATURAN_NILAI}</td>
            <td class="center">{$nilai.NILAI_MAX_PERATURAN_NILAI}</td>
            <td class="center">
                <a href="perkuliahan-nilaimutu.php?mode=edit&id={$nilai.ID_PERATURAN_NILAI}">Edit</a>
                <a href="perkuliahan-nilaimutu.php?mode=delete&id={$nilai.ID_PERATURAN_NILAI}">Delete</a></td>
        </tr>
        {/foreach}
    <tr>
        <td colspan="5" style="text-align: center;"><a href="perkuliahan-nilaimutu.php?mode=add">Tambah</a></td>
    </tr>
</table>

