<div class="center_title_bar">Rentang Nilai - Hapus</div>
<h3>Apakah data ini akan dihapus ?</h3>
<form method="post" action="perkuliahan-nilaimutu.php">
<input type="hidden" name="mode" value="delete" />
<input type="hidden" name="id_peraturan_nilai" value="{$peraturan_nilai.ID_PERATURAN_NILAI}" />
<table>
    <tr>
        <td>Peraturan</td>
        <td>{$peraturan_nilai.NM_PERATURAN}</td>
    </tr>
    <tr>
        <td>Nilai Huruf</td>
        <td>{$peraturan_nilai.NM_STANDAR_NILAI} [{$peraturan_nilai.NILAI_STANDAR_NILAI}]</td>
    </tr>
    <tr>
        <td>Rentang Nilai Min</td>
        <td>{$peraturan_nilai.NILAI_MIN_PERATURAN_NILAI}</td>
    </tr>
    <tr>
        <td>Rentang Nilai Max</td>
        <td>{$peraturan_nilai.NILAI_MAX_PERATURAN_NILAI}</td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <a href="perkuliahan-nilaimutu.php">Kembali</a>
            <input type="submit" value="Hapus"/>
        </td>
    </tr>
</table>
</form>