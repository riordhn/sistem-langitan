<div class="center_title_bar">Rentang Nilai - Edit</div>
<form method="post" action="perkuliahan-nilaimutu.php">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_peraturan_nilai" value="{$peraturan_nilai.ID_PERATURAN_NILAI}" />
<table>
    <tr>
        <td>Peraturan</td>
        <td>
			<select name="id_peraturan">
                {foreach $peraturan as $sn}
                    <option value="{$sn.ID_PERATURAN}" {if $sn.ID_PERATURAN==$peraturan_nilai.ID_PERATURAN}selected="selected"{/if}>
						{$sn.NM_PERATURAN}
					</option>
                {/foreach}
            </select>
		</td>
    </tr>
    <tr>
        <td>Nilai Huruf</td>
        <td><select name="id_standar_nilai">
                {foreach $standar_nilai_set as $sn}
                    <option value="{$sn.ID_STANDAR_NILAI}" {if $sn.ID_STANDAR_NILAI==$peraturan_nilai.ID_STANDAR_NILAI}selected="selected"{/if}>
						{$sn.NM_STANDAR_NILAI}
					</option>
                {/foreach}
            </select>
		</td>
        </td>
    </tr>
    <tr>
        <td>Rentang Nilai Min</td>
        <td><input type="text" size="5" name="nilai_min" value="{$peraturan_nilai.NILAI_MIN_PERATURAN_NILAI}" /></td>
    </tr>
    <tr>
        <td>Rentang Nilai Max</td>
        <td><input type="text" size="5" name="nilai_max" value="{$peraturan_nilai.NILAI_MAX_PERATURAN_NILAI}" /></td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <a href="perkuliahan-nilaimutu.php">Kembali</a>
            <input type="submit" value="Simpan"/>
        </td>
    </tr>
</table>
</form>