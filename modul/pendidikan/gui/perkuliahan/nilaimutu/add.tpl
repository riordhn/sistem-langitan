<div class="center_title_bar">Rentang Nilai - Tambah</div>
<form method="post" action="perkuliahan-nilaimutu.php">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <th>Keterangan</th>
            <th>Nilai</th>
        </tr>
		<tr>
            <td>Peraturan</td>
            <td><select name="id_peraturan">
                {foreach $peraturan as $sn}
                    <option value="{$sn.ID_PERATURAN}">{$sn.NM_PERATURAN}</option>
                {/foreach}
                </select>
			</td>
        </tr>
        <tr>
            <td>Nilai Huruf / Mutu</td>
            <td><select name="id_standar_nilai">
                {foreach $standar_nilai_set as $sn}
                    <option value="{$sn.ID_STANDAR_NILAI}">{$sn.NM_STANDAR_NILAI}</option>
                {/foreach}
                </select>
			</td>
        </tr>
        <tr>
            <td>Nilai Range Minimum</td>
            <td><input type="text" maxlength="5" size="5" name="nilai_min"/></td>
        </tr>
        <tr>
            <td>Nilai Range Maximum</td>
            <td><input type="text" maxlength="5" size="5" name="nilai_max"/></td>
        </tr>
        <tr>
            <td colspan="2" class="center"><a href="perkuliahan-nilaimutu.php">Batal</a><input type="submit" value="Simpan" /></td>
        </tr>
    </table>
</form>