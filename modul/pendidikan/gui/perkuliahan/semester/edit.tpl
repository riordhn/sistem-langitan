<div class="center_title_bar">Daftar Nama Semester - Edit</div>

<form action="perkuliahan-semester.php" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_semester" value="{$semester.ID_SEMESTER}" />
    <table>
        <tr>
            <td>Tahun Ajaran</td>
            <td><input type="text" name="tahun_ajaran" class="required" value="{$semester.TAHUN_AJARAN}" /> &nbsp; * Contoh: 2015/2016 atau 2016/2017</td>
        </tr>
        <tr>
            <td>Nama Semester</td>
            <td><input type="text" name="nm_semester" value="{$semester.NM_SEMESTER}" class="required" readonly /></td>
        </tr>
        <tr>
            <td>Tahun Akademik</td>
            <td><input type="text" name="thn_akademik_semester" size="4" maxlength="4" value="{$semester.THN_AKADEMIK_SEMESTER}" class="required number"/></td>
        </tr>
        <tr>
            <td>Status</td>
            <td>
                {if $semester.NM_SEMESTER == 'Pendek'}
                    <select name="status_aktif_semester">
                        <option value="False" {if $semester.STATUS_AKTIF_SP=='False'}selected="selected"{/if}>Tidak Aktif</option>
                        <option value="True" {if $semester.STATUS_AKTIF_SP=='True'}selected="selected"{/if}>Aktif</option>
                    </select>
                {else}
                    <select name="status_aktif_semester">
                        <option value="False" {if $semester.STATUS_AKTIF_SEMESTER=='False'}selected="selected"{/if}>Tidak Aktif</option>
                        <option value="True" {if $semester.STATUS_AKTIF_SEMESTER=='True'}selected="selected"{/if}>Aktif</option>
                    </select>
                {/if}
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <a href="perkuliahan-semester.php">Batal</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script type="text/javascript">
    $('form').validate();
</script>{/literal}