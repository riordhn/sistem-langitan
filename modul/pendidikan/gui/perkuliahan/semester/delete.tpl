<div class="center_title_bar">Daftar Nama Semester - Delete</div>
<h4>Apakah data nama semester ini akan dihapus ?</h4>
<form action="perkuliahan-semester.php" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="id_semester" value="{$semester.ID_SEMESTER}" />
    <table>
        <tr>
            <td>Tahun Ajaran</td>
            <td>{$semester.TAHUN_AJARAN}</td>
        </tr>
        <tr>
            <td>Nama Semester</td>
            <td>{$semester.NM_SEMESTER}</td>
        </tr>
        <tr>
            <td>Tahun Akademik</td>
            <td>{$semester.THN_AKADEMIK_SEMESTER}</td>
        </tr>
        <tr>
            <td>Status</td>
            <td>{if $semester.STATUS_AKTIF_SEMESTER=="True"}Aktif{else}Non-Aktif{/if}</td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button href="perkuliahan-semester.php">Batal</button>
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>