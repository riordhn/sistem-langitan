<div class="center_title_bar">Daftar Nama Semester</div>

<table>
    <tr>
        <th>No</th>
        <th>Tahun Ajaran</th>
        <th>Nama Semester</th>
        <th>Tahun Akademik</th>
        <th>Status Aktif REG</th>
        <th>Status Aktif SP</th>
        <th></th>
    </tr>
    {$i=1}
    {foreach $semester_set as $semester}
    <tr>
        <td class="center">{$i++}</td>
        <td>{$semester.TAHUN_AJARAN}</td>
        <td>{$semester.NM_SEMESTER}</td>
        <td class="center">{$semester.THN_AKADEMIK_SEMESTER}</td>
        <td class="center">
            {if $semester.STATUS_AKTIF_SEMESTER=="True"}Aktif{else}Non-Aktif{/if}
        </td>
        <td class="center">
            {if $semester.STATUS_AKTIF_SP=="True"}Aktif{else}Non-Aktif{/if}
        </td>
        <td>
            <a href="perkuliahan-semester.php?mode=edit&id_semester={$semester.ID_SEMESTER}">Edit</a>
            <!--<a href="perkuliahan-semester.php?mode=delete&id_semester={$semester.ID_SEMESTER}">Hapus</a>-->
        </td>
    </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="7"><a href="perkuliahan-semester.php?mode=add">Tambah</a></td>
    </tr>
</table>