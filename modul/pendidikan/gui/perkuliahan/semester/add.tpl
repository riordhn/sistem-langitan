<div class="center_title_bar">Daftar Nama Semester - Tambah</div>

<form action="perkuliahan-semester.php" method="post">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <td>Tahun Ajaran</td>
            <td><input type="text" name="tahun_ajaran" class="required" /> &nbsp; * Contoh: 2015/2016 atau 2016/2017</td>
        </tr>
        <tr>
            <td>Nama Semester</td>
            <td>
		<select name="nm_semester" class="required">
			<option value="Ganjil">Ganjil</option>
			<option value="Genap">Genap</option>
            <option value="Pendek">Pendek</option>
		</select>
	    </td>
        </tr>
        <tr>
            <td>Tahun Akademik</td>
            <td><input type="text" name="thn_akademik_semester" size="4" maxlength="4" class="required number" /></td>
        </tr>
        <tr>
            <td>Status</td>
            <td>
                <select name="status_aktif_semester">
                    <option value="False">Tidak Aktif</option>
                    <option value="True">Aktif</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <a href="perkuliahan-semester.php">Batal</a>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script type="text/javascript">
    $('form').validate();
</script>{/literal}