<div class="center_title_bar">Master Jenjang - Edit</div>

<form action="perkuliahan-jenjang.php" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_jenjang" value="{$jenjang[0]['ID_JENJANG']}" />
    <table>
        <tr>
            <td>Nama Jenjang</td>
            <td><input type="text" name="nm_jenjang" class="required" value="{$jenjang[0]['NM_JENJANG']}" /></td>
        </tr>
		<tr>
            <td>Nama Jenjang</td>
            <td><input type="text" name="nm_jenjang_en" class="required" value="{$jenjang[0]['NM_JENJANG_EN']}" /></td>
        </tr>
        <tr>
            <td>Nama Jenjang Ijasah</td>
            <td><input type="text" name="nm_jenjang_ijasah" class="required" value="{$jenjang[0]['NM_JENJANG_IJASAH']}" /></td>
        </tr>
        <tr>
            <td>Nama Singkat Ijasah</td>
            <td><input type="text" name="nm_singkat_ijasah" value="{$jenjang[0]['NM_JENJANG_SNGKAT']}"/></td>
        </tr>
        <tr>
            <td>Kode NIM</td>
            <td><input type="text" name="kode_nim" value="{$jenjang[0]['NIM_JENJANG']}" class="required"/></td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script language="javascript">
$('form').validate();
</script>{/literal}