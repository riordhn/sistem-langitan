<div class="center_title_bar">Master Jenjang</div>

<table>
    <tr>
        <th>No</th>
        <th>Nama Jenjang</th>
		<th>Nama Jenjang Inggris</th>
        <th>Nama Jenjang Ijasah</th>
        <th>Nama Singkat Ijasah</th>
        <!--
        <th>Kode NIM</th>
        <th>Aksi</th>
        -->
    </tr>
    {$i = 1}
    {foreach $jenjang_set as $jenjang}
    <tr>
        <td class="center">{$i++}</td>
        <td>{$jenjang.NM_JENJANG}</td>
		<td>{$jenjang.NM_JENJANG_EN}</td>
        <td>{$jenjang.NM_JENJANG_IJASAH}</td>
        <td>{$jenjang.NM_JENJANG_SNGKAT}</td>
        <!--
        <td>{$jenjang.NIM_JENJANG}</td>
        <td>
            <a href="perkuliahan-jenjang.php?mode=edit&id_jenjang={$jenjang.ID_JENJANG}">Edit</a>
            <a href="perkuliahan-jenjang.php?mode=delete&id_jenjang={$jenjang.ID_JENJANG}">Hapus</a>
        </td>
        -->
    </tr>
    {/foreach}
    <!--
    <form method="get" action="perkuliahan-jenjang.php">
    <tr>
        <td class="center" colspan="8">
        	<input type="submit" value="Tambah" />
            <input type="hidden" value="add" name="mode" />
        </td>
    </tr>
    </form>
    -->
</table>