<div class="center_title_bar">Master Jenjang - Hapus</div>
<h4>Apakah data jenjang ini akan dihapus ?</h4>
<form action="perkuliahan-jenjang.php" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="id_jenjang" value="{$jenjang[0]['ID_JENJANG']}" />
    <table>
        <tr>
            <td>Nama Jenjang</td>
            <td>{$jenjang->NM_JENJANG}</td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button href="perkuliahan-jenjang.php">Batal</button>
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>