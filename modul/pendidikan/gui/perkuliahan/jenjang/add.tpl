<div class="center_title_bar">Master Jenjang - Tambah</div>

<form action="perkuliahan-jenjang.php" method="post">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <td>Nama Jenjang</td>
            <td><input type="text" name="nm_jenjang" class="required" /></td>
        </tr>
		<tr>
            <td>Nama Jenjang Inggris</td>
            <td><input type="text" name="nm_jenjang_en" class="required" /></td>
        </tr>
        <tr>
            <td>Nama Jenjang Ijasah</td>
            <td><input type="text" name="nm_jenjang_ijasah" class="required" /></td>
        </tr>
       <tr>
            <td>Nama Singkat Ijasah</td>
            <td><input type="text" name="nm_singkat_ijasah" /></td>
        </tr>
        <tr>
            <td>Kode NIM</td>
            <td><input type="text" name="kode_nim" class="required" /></td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script language="javascript">
$('form').validate();
</script>{/literal}