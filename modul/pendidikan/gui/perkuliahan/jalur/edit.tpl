<div class="center_title_bar">Master Jalur - Edit</div>

<form action="perkuliahan-jalur.php" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_jalur" value="{$jalur[0]['ID_JALUR']}" />
    <table>
        <tr>
            <td>Nama Jalur</td>
            <td>{$jalur[0]['NM_JALUR']}</td>
        </tr>
        <tr>
            <td>Kode NIM</td>
            <td><input type="text" name="kode_nim" value="{$jalur[0]['NIM_JALUR']}" class="required"/></td>
        </tr>
        <tr>
            <td>Kode Jalur</td>
            <td><select name="kode_jalur">
                {foreach $kode_jalur_set as $j}
                    <option value="{$j.KODE_JALUR}" {if $jalur[0]['KODE_JALUR'] == $j.KODE_JALUR}selected="selected"{/if}>{$j.KETERANGAN}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script language="javascript">
$('form').validate();
</script>{/literal}