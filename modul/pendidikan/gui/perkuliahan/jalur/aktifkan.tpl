<div class="center_title_bar">Master Jalur - Delete</div>

<form action="perkuliahan-jalur.php" method="post">
    <input type="hidden" name="mode" value="aktifkan" />
    <input type="hidden" name="id_jalur" value="{$jalur[0]['ID_JALUR']}" />
    <table>
        <tr>
            <td>Nama Jalur</td>
            <td>{$jalur[0]['NM_JALUR']}</td>
        </tr>
        <tr>
            <td>Kode NIM</td>
            <td>{$jalur[0]['NIM_JALUR']}</td>
        </tr>
        <tr>
            <td>Kode Jalur</td>
            <td>{$jalur[0]['KETERANGAN']}</td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
                <input type="submit" value="Aktifkan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script language="javascript">
$('form').validate();
</script>{/literal}