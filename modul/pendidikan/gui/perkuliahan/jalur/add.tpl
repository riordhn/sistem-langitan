<div class="center_title_bar">Master Jalur - Tambah</div>

<form action="perkuliahan-jalur.php" method="post">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <td>Nama Jalur</td>
            <td><input type="text" name="nm_jalur" class="required"/></td>
        </tr>
        <tr>
            <td>Kode NIM</td>
            <td><input type="text" name="kode_nim" /></td>
        </tr>
        <tr>
            <td>Kode Jalur</td>
            <td><select name="kode_jalur">
                {foreach $kode_jalur_set as $j}
                    <option value="{$j.KODE_JALUR}">{$j.KETERANGAN}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1);return;" value="Batal" />
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
{literal}<script language="javascript">
$('form').validate();
</script>{/literal}