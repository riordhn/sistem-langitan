<div class="center_title_bar">Master Jalur</div>

<table>
    <tr>
        <th>No</th>
        <th>Nama Jalur</th>
        <th>Kode NIM</th>
        <th>Kode Jalur</th>
        <th>Jml Mhs</th>
        <th>Aksi</th>
    </tr>
    {$i = 1}
    {foreach $jenjang_set as $jalur}
        {if $jalur.IS_AKTIF == '1'}
            <tr>
                <td class="center">{$i++}</td>
                <td>{$jalur.NM_JALUR}</td>
                <td>{$jalur.NIM_JALUR}</td>
                <td>{$jalur.KETERANGAN}</td>
                <td class="center">{$jalur.JML_MHS}</td>
                <td>
                    <a href="perkuliahan-jalur.php?mode=edit&id_jalur={$jalur.ID_JALUR}">Edit</a>
                    <a href="perkuliahan-jalur.php?mode=delete&id_jalur={$jalur.ID_JALUR}"><b style="color: red">Hapus</b></a>
                </td>
            </tr>
        {else}
            <tr bgcolor="#FF0000">
                <td class="center">{$i++}</td>
                <td>{$jalur.NM_JALUR}</td>
                <td>{$jalur.NIM_JALUR}</td>
                <td>{$jalur.KETERANGAN}</td>
                <td class="center">{$jalur.JML_MHS}</td>
                <td>
                    <a href="perkuliahan-jalur.php?mode=aktifkan&id_jalur={$jalur.ID_JALUR}" style="color: blue">Aktifkan</a>
                </td>
            </tr>
        {/if}
    {/foreach}
    <tr>
        <td colspan="6" class="center">
            <a href="perkuliahan-jalur.php?mode=add"><b style="color: green">Tambah Jalur</b></a>
        </td>
    </tr>
</table>