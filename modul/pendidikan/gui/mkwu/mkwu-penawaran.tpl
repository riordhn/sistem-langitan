
{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
 $("#myTable1").tablesorter(
		{
		sortList: [[1,0], [2,0]],
		headers: {
   3: { sorter: false },
   5: { sorter: false }
		}
		}
	);
}
);
</script>


<style>
#dark{background-color:#333;border:1px solid #000;padding:10px;margin-top:20px;}#light{background-color:#FFF;border:1px solid #dedede;padding:10px;margin-top:20px;}.button,.button:visited{background:#222 url(overlay.png) repeat-x;display:inline-block;padding:5px 10px 6px;color:#fff;text-decoration:none;-moz-border-radius:6px;-webkit-border-radius:6px;-moz-box-shadow:0 1px 3px rgba(0,0,0,0.6);-webkit-box-shadow:0 1px 3px rgba(0,0,0,0.6);text-shadow:0 -1px 1px rgba(0,0,0,0.25);border-bottom:1px solid rgba(0,0,0,0.25);position:relative;cursor:pointer}.button:hover{background-color:#111;color:#fff;}.button:active{top:1px;}.small.button,.small.button:visited{font-size:11px}.button,.button:visited,.medium.button,.medium.button:visited{font-size:13px;font-weight:bold;line-height:1;text-shadow:0 -1px 1px rgba(0,0,0,0.25);}.large.button,.large.button:visited{font-size:14px;padding:8px 14px 9px;}.super.button,.super.button:visited{font-size:34px;padding:8px 14px 9px;}.pink.button,.magenta.button:visited{background-color:#e22092;}.pink.button:hover{background-color:#c81e82;}.green.button,.green.button:visited{background-color:#060;}.green.button:hover{background-color:#749a02;}.red.button,.red.button:visited{background-color:#e62727;}.red.button:hover{background-color:#cf2525;}.orange.button,.orange.button:visited{background-color:#ff5c00;}.orange.button:hover{background-color:#d45500;}.blue.button,.blue.button:visited{background-color:#2981e4;}.blue.button:hover{background-color:#2575cf;}.yellow.button,.yellow.button:visited{background-color:#ffb515;}.yellow.button:hover{background-color:#fc9200;}
</style>
{/literal}

<div class="center_title_bar">Penawaran MKWU</div>

<button class="large button green" onclick="location='#kkn-kkn8!mkwu-penawaran.php'">Rincian</button>
<button class="large button green" onclick="location='#kkn-kkn8!mkwu-penawaran.php?mode=penawaran&smt={$smarty.request.smt}'">Penawaran</button>


{if $smarty.request.mode == 'penawaran'}

<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="10%">Kode</th>
			<th width="43%">Nama Mata Ajar</th>
			<th width="5%">SKS</th>
			<th width="5%">Kelas</th>
			<th width="25%">Prodi</th>
			<th class="noheader" width="12%">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=test item="list" from=$tawar}
		<tr>
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td><center>{$list.NAMA_KELAS}</center></td>
			<td>{$list.PRODIASAL}</td>
			<td><center>
            <a href="mkwu-penawaran.php?mode=penawaran&action=add&id_mk={$list.ID_KELAS_MK}&smt={$semester_request}&id_prodi={$list.ID_PROGRAM_STUDI}">
            	Ditawarkan
            </a></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>

{else}


<form action="mkwu-penawaran.php" method="post" >
<input type="hidden" name="action" value="penawaran" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
        Tahun Akademik :
		<select name="smt">
				<option value=''>-- PILIH THN AKD --</option>
				{foreach $semester as $smt}
					<option value="{$smt.ID_SEMESTER}" {if $semester_aktif['ID_SEMESTER'] == $smt.ID_SEMESTER or $smt.ID_SEMESTER == $smarty.request.smt} selected="selected"{/if}>{$smt.THN_AKADEMIK_SEMESTER} ({$smt.NM_SEMESTER})</option>
				{/foreach}
			</select>
		<input type="submit" name="View" value="Tampil">
		</td>
	</tr>
</table>
</form>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="5%">Kode</th>
			<th width="33%">Nama Mata Ajar</th>
			<th width="5%">SKS</th>
			<th width="5%">KLS</th>
			<th width="8%">Hari</th>
			<th width="12%">Jam</th>
			<th width="20%">Prodi</th>
			<th class="noheader" width="12%">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=test item="list" from=$T_MK}
		<tr>
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td><center>{$list.NAMA_KELAS}</center></td>
			<td>{$list.NM_JADWAL_HARI}</td>
			<td><center>{$list.JAM}</center></td>
			<td>{$list.PRODIASAL}</td>
			<td><center><a href="mkwu-penawaran.php"  onclick="javascript:if(confirm('Anda yakin untuk menghapus?'))hapus({$list.ID_KRS_PRODI});">Hapus</a></center></td>
		</tr>
		{foreachelse}
		<tr><td colspan="8"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
</table>

{/if}



{literal}
    <script type="text/javascript">
	
	function hapus(id){
	$.ajax({
	type: "POST",
	url: "mkwu-penawaran.php",
	data: "action=del&id_krs_prodi="+id,
	cache: false,
	success: function(data){
		if (data != '')
		{
			window.location.reload(true);
		}
		else
		{
				alert('gagal hapus');
			}
		}
		});
	}
		
    </script>
{/literal}