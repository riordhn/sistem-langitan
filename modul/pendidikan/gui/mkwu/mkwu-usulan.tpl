
{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
 $("#myTable1").tablesorter(
		{
		sortList: [[1,0], [2,0]],
		headers: {
   3: { sorter: false },
   5: { sorter: false }
		}
		}
	);
}
);
</script>


<style>
#dark{background-color:#333;border:1px solid #000;padding:10px;margin-top:20px;}#light{background-color:#FFF;border:1px solid #dedede;padding:10px;margin-top:20px;}.button,.button:visited{background:#222 url(overlay.png) repeat-x;display:inline-block;padding:5px 10px 6px;color:#fff;text-decoration:none;-moz-border-radius:6px;-webkit-border-radius:6px;-moz-box-shadow:0 1px 3px rgba(0,0,0,0.6);-webkit-box-shadow:0 1px 3px rgba(0,0,0,0.6);text-shadow:0 -1px 1px rgba(0,0,0,0.25);border-bottom:1px solid rgba(0,0,0,0.25);position:relative;cursor:pointer}.button:hover{background-color:#111;color:#fff;}.button:active{top:1px;}.small.button,.small.button:visited{font-size:11px}.button,.button:visited,.medium.button,.medium.button:visited{font-size:13px;font-weight:bold;line-height:1;text-shadow:0 -1px 1px rgba(0,0,0,0.25);}.large.button,.large.button:visited{font-size:14px;padding:8px 14px 9px;}.super.button,.super.button:visited{font-size:34px;padding:8px 14px 9px;}.pink.button,.magenta.button:visited{background-color:#e22092;}.pink.button:hover{background-color:#c81e82;}.green.button,.green.button:visited{background-color:#060;}.green.button:hover{background-color:#749a02;}.red.button,.red.button:visited{background-color:#e62727;}.red.button:hover{background-color:#cf2525;}.orange.button,.orange.button:visited{background-color:#ff5c00;}.orange.button:hover{background-color:#d45500;}.blue.button,.blue.button:visited{background-color:#2981e4;}.blue.button:hover{background-color:#2575cf;}.yellow.button,.yellow.button:visited{background-color:#ffb515;}.yellow.button:hover{background-color:#fc9200;}
</style>

{/literal}


<div class="center_title_bar">Usulan MKWU</div>

<button class="large button green" onclick="location='#kkn-kkn8!mkwu-usulan.php'">Rincian</button>
<button class="large button green" onclick="location='#kkn-kkn8!mkwu-usulan.php?mode=usulan'">Usulan</button>


{if $smarty.request.mode == 'usulan'}

<form action="mkwu-usulan.php?mode=usulan" method="post" >
<input type="hidden" name="action" value="usulan" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		Tahun Akademik :
			<select name="smt">
				<option value=''>-- PILIH THN AKD --</option>
				{foreach $semester as $smt}
					<option value="{$smt.ID_SEMESTER}" {if $semester_aktif['ID_SEMESTER'] == $smt.ID_SEMESTER or $smt.ID_SEMESTER == $smarty.request.smt} selected="selected"{/if}>{$smt.THN_AKADEMIK_SEMESTER} ({$smt.NM_SEMESTER})</option>
				{/foreach}
			</select>
		Tahun Kurikulum :
			<select name="kur">
				<option value=''>-- THN KUR --</option>
				{foreach $kurikulum_mkwu as $thkur}
                <option value="{$thkur.ID_KURIKULUM}" {if $thkur.ID_KURIKULUM == $smarty.request.kur} selected="selected"{/if}>{$thkur.NM_KURIKULUM} ({$thkur.THN_KURIKULUM})</option>
				{/foreach}
			</select>
		<input type="submit" name="View" value="Tampil">
		</td>
	</tr>
</table>
</form>


{if isset($usulan)}
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="10%">Kode</th>
			<th width="35%">Nama Mata Ajar</th>
			<th width="10%">SKS</th>
			<th class="noheader" width="20%">Tahun Kurikulum</th>
			<th class="noheader" width="15%">Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$usulan}
		<tr>
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td><center>{$list.TAHUN_KURIKULUM}</center></td>
			<td><center><a href="mkwu-usulan.php?action=add&id_mk={$list.ID_KURIKULUM_MK}&smt={$smarty.request.smt}&kur={$smarty.request.kur}&mode=usulan">Usulkan</a></center></td>
		</tr>
	{foreachelse}
		<tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
{/if}

{else if $action == 'view'}
<form action="mkwu-usulan.php" method="post">
<input type="hidden" name="action" value="view" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		Tahun Akademik :
			<select name="smt">
				<option value=''>-- PILIH THN AKD --</option>
				{foreach $semester as $smt}
					<option value="{$smt.ID_SEMESTER}" {if $semester_aktif['ID_SEMESTER'] == $smt.ID_SEMESTER or $smt.ID_SEMESTER == $smarty.request.smt} selected="selected"{/if}>{$smt.THN_AKADEMIK_SEMESTER} ({$smt.NM_SEMESTER})</option>
				{/foreach}
			</select>
		<input type="submit" name="View" value="View">
		</td>
	</tr>
</table>
</form>

{if isset($rincian)}
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="5%">Kode</th>
			<th width="18%">Nama Mata Ajar</th>
			<th width="6%">Thn Kur</th>
			<th width="5%">KLS</th>
			<th width="5%">SKS</th>
			<th width="10%">Ruang</th>
			<th width="5%">Hari</th>
			<th width="10%">Jam</th>
            <th width="20%">PRODI</th>
			<th class="noheader" width="20%">Aksi</th>
		</tr>
	</thead>
	<tbody>
	{foreach name=test item="list" from=$rincian}
	{if $list.NM_JADWAL_HARI == "" || $list.JAM == ""}
		<tr bgcolor="#ff828e">
	{else}
		<tr>
	{/if}
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>
			<td>{$list.TAHUN_KURIKULUM}</td>
			<td><center>{$list.NAMA_KELAS}</center></td>
			<td><center>{$list.KREDIT_SEMESTER}</center></td>
			<td>{$list.NM_RUANGAN} ({$list.KAPASITAS_KELAS_MK})</td>
			<td>{$list.NM_JADWAL_HARI}</td>
			<td>{$list.JAM}</td>
            <td>{if $list.ID_PROGRAM_STUDI != 228}{$list.PRODI}{/if}</td>
			<td>
			
			<a href="mkwu-usulan.php?action=updateview&id_klsmk={$list.ID_KELAS_MK}&smt={$semester_request}">Update</a>			
			&nbsp;|&nbsp;
			<a href="mkwu-usulan.php?action=adkelview&id_klsmk={$list.ID_KELAS_MK}&smt={$semester_request}">Add</a>
			&nbsp;|&nbsp;
			<a href="mkwu-usulan.php" onclick="javascript:if(confirm('Anda yakin untuk menghapus?'))hapus({$list.ID_KELAS_MK});">Del</a>
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="15"><em>Data tidak ditemukan</em></td></tr>
	{/foreach}
	</tbody>
</table>
{/if}

{else if $action == 'updateview' or $action == 'adkelview'}


<form action="mkwu-usulan.php" method="post" id="f1">
<input type="hidden" name="action" value="{$action}" >
<table class="tb_frame" border="0" cellspacing="0" cellpadding="0">
	<tr>
    	<th colspan="8">Setting Mata Ajar</th>
    </tr>
	{foreach item="ubah" from=$usulan}
	<input type="hidden" name="id_kelas_mk" value="{$ubah.ID_KELAS_MK}" />
    <input type="hidden" name="id_kur_mk" value="{$ubah.ID_KURIKULUM_MK}" />
	<input type="hidden" name="smt" value="{$smarty.request.smt}" />
	
	<tr>
		<td width="29%">Kode Mata Ajar</td>
		<td width="2%"><center>:</center></td>
		<td width="69%">{$ubah.KD_MATA_KULIAH}</td>
	</tr>
	<tr>
		<td>Nama Mata Ajar</td>
		<td><center>:</center></td>
		<td>{$ubah.NM_MATA_KULIAH}</td>
	</tr>
	<tr {if $action == 'adkelview'}bgcolor="yellow"{/if}>
		<td>Kelas MK [A/B/C/D]</td>
		<td><center>:</center></td>
		<td>
			<select name="kelas_mk" id="kelas_mk" class="required">
            	<option value=""> ----- </option>
			{foreach $kelas as $data}   	
            	<option value="{$data.ID_NAMA_KELAS}" {if $data.ID_NAMA_KELAS == $ubah.NO_KELAS_MK and $action != 'adkelview'} selected="selected"{/if}>{$data.NAMA_KELAS}</option>
			{/foreach}
			</select>
		</td>
	</tr>
    <tr>
            <td>Fakultas</td>
            <td><center>:</center></td>
            <td>
                <select id="fakultas" name="fakultas" class="required">
				<option value="">PILIH FAKULTAS</option> 
                    {foreach $fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $ubah.ID_FAKULTAS==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
			<td>Jenjang</td>
            <td><center>:</center></td>
			<td>
				<select id="jenjang" name="jenjang" class="required">
                    <option value="">PILIH JENJANG</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $ubah.ID_JENJANG==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
		</tr>
		<tr>
			<td>Program Studi</td>
            <td><center>:</center></td>
            <td>
                <select id="program_studi" name="program_studi" class="required">
                    <option value="">PILIH PRODI</option>
                    {foreach $data_program_studi as $data}
                        {if $ubah.ID_PROGRAM_STUDI!=''}
                            <option value="{$data.ID_PROGRAM_STUDI}" {if $ubah.ID_PROGRAM_STUDI==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
                        {else if $ubah.ID_FAKULTAS!=''&&$ubah.ID_PROGRAM_STUDI==''}
                            {if $data.ID_FAKULTAS==$ubah.ID_FAKULTAS}
                                <option value="{$data.ID_PROGRAM_STUDI}" >{$data.NM_PROGRAM_STUDI}</option>
                            {/if}
                        {/if}
                    {/foreach}
                </select>
            </td>
		</tr>
	<tr>
		<td>KAPASITAS KELAS</td>
		<td><center>:</center></td>
		<td><input name="kap_kelas" id="kap_kelas" type="text" class="required" size="3" maxlength="3" class="required" value="{$ubah.KAPASITAS_KELAS_MK}"/> [WAJIB DI-ISI]</td>
	</tr>
	<tr>
		<td>Rencana Perkuliahan (Tatap Muka)</td>
		<td><center>:</center></td>
		<td><input name="ren_kul" id="ren_kul" type="text" class="required" size="3" maxlength="3" class="required" value="{$ubah.JUMLAH_PERTEMUAN_KELAS_MK}"/> [WAJIB DI-ISI]</td>
	</tr>
	<tr>
		<td>Hari</td>
		<td><center>:</center></td>
		<td>
			<select name="hari" id="hari" class="required">
            <option value=""> ----- </option>
            {foreach $hari as $data}   	
            	<option value="{$data.ID_JADWAL_HARI}" {if $data.ID_JADWAL_HARI == $ubah.ID_JADWAL_HARI and $action != 'adhariview'} selected="selected"{/if}>{$data.NM_JADWAL_HARI}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Jam</td>
		<td><center>:</center></td>
			<td>
			<select name="jam" id="jam">
			{foreach item="jam" from=$t_jam}
            	<option value="{$jam.ID_JADWAL_JAM}" {if $jam.ID_JADWAL_JAM == $ubah.ID_JADWAL_JAM} selected="selected"{/if}>{$jam.NM_JADWAL_JAM} ({$jam.JAM_MULAI}:{$jam.MENIT_MULAI} - {$jam.JAM_SELESAI}:{$jam.MENIT_SELESAI})</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr {if $action == 'adkelview'}bgcolor="yellow"{/if}>
		<td>Ruangan</td>
		<td><center>:</center></td>
		<td>
        	<select name="fakultas_gedung" id="fakultas_gedung">
            	<option value=""></option>
                {foreach $fakultas as $data}
                <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS == $ubah.ID_FAKULTAS_GEDUNG} selected="selected"{/if}>{$data.NM_FAKULTAS}</option>	
                {/foreach}
            </select>
            &nbsp;&nbsp;
			<select name="ruangan" id="ruangan" class="required">
            	<option value=""> ----- </option>
                {foreach $t_ruangan as $data}   	
            	<option value="{$data.ID_RUANGAN}" {if $data.ID_RUANGAN == $ubah.ID_RUANGAN and $action != 'adkelview'} selected="selected"{/if}>{$data.RUANG}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	{/foreach}

{if $ubah.KD_MATA_KULIAH == ""}
<p><em>Data tidak ditemukan</em></p>
<p><input type="button" name="kembali" value="Kembali" onClick="javascript: history.go(-1);"></p>
{else}
<tr><td colspan="8" style="text-align:center">
<input type="button" name="kembali" value="Kembali" onClick="javascript: history.go(-1);">
&nbsp;
<input type="submit" name="Simpan1" value="Simpan">
</td></tr>
{/if}

</table>
</form>

{/if}





{literal}
    <script type="text/javascript">
	
	function hapus(id){
	$.ajax({
	type: "POST",
	url: "mkwu-usulan.php",
	data: "action=del&id_klsmk="+id,
	cache: false,
	success: function(data){
		if (data != '')
		{
			window.location.reload(true);
		}
		else
		{
				alert('gagal hapus');
			}
		}
		});
	}

		$('#fakultas_gedung').change(function(){
            $.ajax({
                type:'post',
                url:'getRuangan.php',
                data:'id_fakultas='+$('#fakultas_gedung').val(),
                success:function(data){
                    $('#ruangan').html(data);
                }                    
            })
        });
		
		
		
		$('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });

		$('#f1').validate();
		
		
    $('.confirmation').on('click', function () {
        return confirm('Are you sure?');
    });
		
    </script>
{/literal}