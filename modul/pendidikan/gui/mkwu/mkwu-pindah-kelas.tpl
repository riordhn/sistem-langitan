
{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
 $("#myTable").tablesorter(
		{
		sortList: [[7,0], [0,0]],
		headers: {
   3: { sorter: false },
   5: { sorter: false }
		}
		}
	);
}
);


			$("#cek_all").click(function()				
			{
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
			});
</script>


<style>
#dark{background-color:#333;border:1px solid #000;padding:10px;margin-top:20px;}#light{background-color:#FFF;border:1px solid #dedede;padding:10px;margin-top:20px;}.button,.button:visited{background:#222 url(overlay.png) repeat-x;display:inline-block;padding:5px 10px 6px;color:#fff;text-decoration:none;-moz-border-radius:6px;-webkit-border-radius:6px;-moz-box-shadow:0 1px 3px rgba(0,0,0,0.6);-webkit-box-shadow:0 1px 3px rgba(0,0,0,0.6);text-shadow:0 -1px 1px rgba(0,0,0,0.25);border-bottom:1px solid rgba(0,0,0,0.25);position:relative;cursor:pointer}.button:hover{background-color:#111;color:#fff;}.button:active{top:1px;}.small.button,.small.button:visited{font-size:11px}.button,.button:visited,.medium.button,.medium.button:visited{font-size:13px;font-weight:bold;line-height:1;text-shadow:0 -1px 1px rgba(0,0,0,0.25);}.large.button,.large.button:visited{font-size:14px;padding:8px 14px 9px;}.super.button,.super.button:visited{font-size:34px;padding:8px 14px 9px;}.pink.button,.magenta.button:visited{background-color:#e22092;}.pink.button:hover{background-color:#c81e82;}.green.button,.green.button:visited{background-color:#060;}.green.button:hover{background-color:#749a02;}.red.button,.red.button:visited{background-color:#e62727;}.red.button:hover{background-color:#cf2525;}.orange.button,.orange.button:visited{background-color:#ff5c00;}.orange.button:hover{background-color:#d45500;}.blue.button,.blue.button:visited{background-color:#2981e4;}.blue.button:hover{background-color:#2575cf;}.yellow.button,.yellow.button:visited{background-color:#ffb515;}.yellow.button:hover{background-color:#fc9200;}
</style>

{/literal}

<div class="center_title_bar">PINDAH KELAS MKWU</div>


<form action="mkwu-pindah-kelas.php?mode=tampil" method="post" >
<input type="hidden" name="action" value="usulan" >
<table class="tb_frame" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		Mata Kuliah :
			<select name="mata_kuliah">
				<option value=''>Semua</option>
                <option value='AGAMA ISLAM' {if $smarty.request.mata_kuliah == 'AGAMA ISLAM'} selected="selected"{/if}>Agama Islam</option>
                <option value='PROTESTAN' {if $smarty.request.mata_kuliah == 'PROTESTAN'} selected="selected"{/if}>Agama Protestan</option>
                <option value='KATOLIK' {if $smarty.request.mata_kuliah == 'KATOLIK'} selected="selected"{/if}>Agama Katolik</option>
                <option value='HINDU' {if $smarty.request.mata_kuliah == 'HINDU'} selected="selected"{/if}>Agama Hindu</option>
                <option value='BUDHA' {if $smarty.request.mata_kuliah == 'BUDHA'} selected="selected"{/if}>Agama Budha</option>
                <option value='KONGHUCU' {if $smarty.request.mata_kuliah == 'KONGHUCU'} selected="selected"{/if}>Agama Konghucu</option>
                <option value='PPKN' {if $smarty.request.mata_kuliah == 'PPKN'} selected="selected"{/if}>PPKN</option>
                <option value='FILSAFAT ILMU' {if $smarty.request.mata_kuliah == 'FILSAFAT ILMU'} selected="selected"{/if}>Filsafat Ilmu</option>
                <option value='ILMU ALAMIAH DASAR' {if $smarty.request.mata_kuliah == 'ILMU ALAMIAH DASAR'} selected="selected"{/if}>Ilmu Alamiah Dasar</option>
                <option value='ILMU SOSIAL BUDAYA DASAR' {if $smarty.request.mata_kuliah == 'ILMU SOSIAL BUDAYA DASAR'} selected="selected"{/if}>Ilmu Sosial Budaya Dasar</option>
                <option value='BAHASA INGGRIS' {if $smarty.request.mata_kuliah == 'BAHASA INGGRIS'} selected="selected"{/if}>Bahasa Inggris</option>
                <option value='BAHASA INDONESIA' {if $smarty.request.mata_kuliah == 'BAHASA INDONESIA'} selected="selected"{/if}>Bahasa Indonesia</option>
			</select>
        </td>
        <td>
		Fakultas :
			<select name="fakultas">
				<option value=''>Semua</option>
				{foreach $fakultas as $data}
                <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS == $smarty.request.fakultas} selected="selected"{/if}>{$data.NM_FAKULTAS}</option>
				{/foreach}
			</select>
		</td>
        <td>
		Jenjang :
			<select name="jenjang">
				<option value=''>Semua</option>
				{foreach $jenjang as $data}
                <option value="{$data.ID_JENJANG}" {if $data.ID_JENJANG == $smarty.request.jenjang} selected="selected"{/if}>{$data.NM_JENJANG}</option>
				{/foreach}
			</select>
		<input type="submit" name="View" value="Tampil">
		</td>
	</tr>
</table>
</form>

{if isset($view_kelas)}
	<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
            <th width="7%">Kode</th>
            <th width="25%">Nama Mata Ajar</th>
			<th width="5%">SKS</th>
            <th width="15%">Ruang</th>
			<th width="5%">KLS</th>
            <th width="5%">Kpst</th>
			<th width="5%">Terisi</th>
			<th width="25%">Prodi MA</th>
            <th class="noheader" width="8%">Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$view_kelas}
		   {if $list.KAPASITAS_KELAS_MK - $list.KLS_TERISI == 0}
		   <tr bgcolor="red">
   		   {elseif $list.KAPASITAS_KELAS_MK - $list.KLS_TERISI <= 5}
		   <tr bgcolor="yellow">
		   {else}
		   <tr>
		   {/if}
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
             <td>{$list.NM_RUANGAN}</td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
			 <td><center>{$list.KAPASITAS_KELAS_MK}</center></td>
			 <td><center>{$list.KLS_TERISI}</center></td>
			 <td>{$list.PRODIMK}</td>
             <td><center><a href="mkwu-pindah-kelas.php?mode=view_pindah&id_kelas_mk={$list.ID_KELAS_MK}&kdmk={$list.KD_MATA_KULIAH}&fakultas={$smarty.request.fakultas}&mata_kuliah={$smarty.request.mata_kuliah}&jenjang={$smarty.request.jenjang}&prodi={$list.ID_PROGRAM_STUDI}">Pindah Kls</a></center></td>
           </tr>
		   {foreachelse}
           <tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
		</tbody>
        {/foreach}
   	 </table>
{/if}




{if isset($view_pindah)}
<form action="mkwu-pindah-kelas.php?mode=gantikelas&fakultas={$smarty.request.fakultas}&mata_kuliah={$smarty.request.mata_kuliah}&jenjang={$smarty.request.jenjang}&prodi={$smarty.request.prodi}" method="post" name="pg" id="pg">

	<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>Kelas Asal</td>
			<td><center>:</center></td>
			<td>
				<select name="kelas_mk_asal" id="kelas_mk_asal">
                    {foreach item="kelas_tujuan" from=$asal_kelas_view}
    		   		{html_options values=$kelas_tujuan.ID_KELAS_MK output=$kelas_tujuan.KELAS_TUJUAN selected=$kls_asal}
	 		   		{/foreach}
                 </select>
			</td>
		</tr>
		<tr>
			<td>Kelas MKWU</td>
			<td><center>:</center></td>
			<td>
				<select name="kelas_mk_tujuan" id="kelas_mk_tujuan">
                    {foreach item="kelas_tujuan" from=$tujuan_kelas_view}
    		   		{html_options values=$kelas_tujuan.ID_KELAS_MK output=$kelas_tujuan.KELAS_TUJUAN}
	 		   		{/foreach}
                 </select>
			</td>
		</tr>
		<tr>
			<td></td><td></td>
			<td><input name="Proses" type="submit" value="Proses" /></td>
		</tr>
	</table>
	<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
           <tr>
             <th width="5%">No</th>
             <th width="20%">NIM</th>
             <th width="35%">Nama Mahasiswa</font> </td>
			 <th width="25%">Prodi</th>
			 <th class="noheader" width="15%"><input type="checkbox" id="cek_all" name="cek_all" /></th>
		   </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$view_pindah}
		   <tr>
             <td><center>{$smarty.foreach.test.iteration}</center></td>
             <td>{$list.NIM_MHS}</td>
             <td>{$list.NM_PENGGUNA}</td>
             <td>{$list.NM_PROGRAM_STUDI}</td>
			 <td><center><input name="pil{$smarty.foreach.test.iteration}" type="checkbox" value="{$list.ID_PENGAMBILAN_MK}" class="cek" /><center></td>
           </tr>
		   {foreachelse}
          <tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
		   {/foreach}
		<input type="hidden" name="counter" value="{$smarty.foreach.test.iteration}" >
		</tbody>
	</table>
</form>
{/if}