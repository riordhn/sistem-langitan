
{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
 $("#myTable").tablesorter(
		{
		sortList: [[0,0], [0,0]],
		headers: {
   3: { sorter: false },
   5: { sorter: false }
		}
		}
	);
}
);

</script>


<style>
#dark{background-color:#333;border:1px solid #000;padding:10px;margin-top:20px;}#light{background-color:#FFF;border:1px solid #dedede;padding:10px;margin-top:20px;}.button,.button:visited{background:#222 url(overlay.png) repeat-x;display:inline-block;padding:5px 10px 6px;color:#fff;text-decoration:none;-moz-border-radius:6px;-webkit-border-radius:6px;-moz-box-shadow:0 1px 3px rgba(0,0,0,0.6);-webkit-box-shadow:0 1px 3px rgba(0,0,0,0.6);text-shadow:0 -1px 1px rgba(0,0,0,0.25);border-bottom:1px solid rgba(0,0,0,0.25);position:relative;cursor:pointer}.button:hover{background-color:#111;color:#fff;}.button:active{top:1px;}.small.button,.small.button:visited{font-size:11px}.button,.button:visited,.medium.button,.medium.button:visited{font-size:13px;font-weight:bold;line-height:1;text-shadow:0 -1px 1px rgba(0,0,0,0.25);}.large.button,.large.button:visited{font-size:14px;padding:8px 14px 9px;}.super.button,.super.button:visited{font-size:34px;padding:8px 14px 9px;}.pink.button,.magenta.button:visited{background-color:#e22092;}.pink.button:hover{background-color:#c81e82;}.green.button,.green.button:visited{background-color:#060;}.green.button:hover{background-color:#749a02;}.red.button,.red.button:visited{background-color:#e62727;}.red.button:hover{background-color:#cf2525;}.orange.button,.orange.button:visited{background-color:#ff5c00;}.orange.button:hover{background-color:#d45500;}.blue.button,.blue.button:visited{background-color:#2981e4;}.blue.button:hover{background-color:#2575cf;}.yellow.button,.yellow.button:visited{background-color:#ffb515;}.yellow.button:hover{background-color:#fc9200;}
</style>

{/literal}

<div class="center_title_bar">CEKAL PRESENSI MKWU</div>




<form action="mkwu-presensi-cekal.php?mode=tampil" method="post"> 
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
           	<td>
			 	Mata Kuliah : 
                <select name="mata_kuliah">
                <option value="">Semua</option>
                {foreach $mata_kuliah_mkwu as $data}
					<option value="{$data.ID_MATA_KULIAH}" {if $data.ID_MATA_KULIAH == $smarty.request.mata_kuliah} selected="selected"{/if}>{$data.NM_MATA_KULIAH} ({$data.KD_MATA_KULIAH})</option>
				{/foreach}                	 	
			   </select>
           </td>
           <td>
               <input type="submit" name="Tampil" value="Tampil">
		     </td>
           </tr>
           <tr>
           	<td colspan="4">* Proses cekal mohon dilakukan kembali jika ada penambahan atau perubahan presensi mhs</td>
           </tr>
</table>
</form>	



{if isset($mkwu_presensi_cekal_view)}
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
			<tr>
             <th width="10%">Kode</th>
             <th width="40%">Nama Mata Ajar</th>
             <th width="15%">Sudah Di Proses</th>
             <th width="15%">Belum Di Proses</th>
             <th width="10%">Aksi</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$mkwu_presensi_cekal_view}
		   <tr>
             <td>{$list.KD_MATA_KULIAH}</td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.TTL_SDH}</center></td>
             <td><center>{$list.TTL_BLM}</center></td>
             <td style="text-align:center">
             <button class="small button green" onclick="location='#kkn-kkn12!mkwu-presensi-cekal.php?mode=proses&mata_kuliah={$smarty.request.mata_kuliah}'">Proses Cekal</button>
             </td>
            </td>
           </tr>
		   {foreachelse}
			<tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
</table>
{/if}     