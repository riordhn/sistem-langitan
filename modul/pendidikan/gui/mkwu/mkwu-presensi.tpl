
{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
 $("#myTable").tablesorter(
		{
		sortList: [[0,0], [0,0]],
		headers: {
   3: { sorter: false },
   5: { sorter: false }
		}
		}
	);
}
);

</script>


<style>
#dark{background-color:#333;border:1px solid #000;padding:10px;margin-top:20px;}#light{background-color:#FFF;border:1px solid #dedede;padding:10px;margin-top:20px;}.button,.button:visited{background:#222 url(overlay.png) repeat-x;display:inline-block;padding:5px 10px 6px;color:#fff;text-decoration:none;-moz-border-radius:6px;-webkit-border-radius:6px;-moz-box-shadow:0 1px 3px rgba(0,0,0,0.6);-webkit-box-shadow:0 1px 3px rgba(0,0,0,0.6);text-shadow:0 -1px 1px rgba(0,0,0,0.25);border-bottom:1px solid rgba(0,0,0,0.25);position:relative;cursor:pointer}.button:hover{background-color:#111;color:#fff;}.button:active{top:1px;}.small.button,.small.button:visited{font-size:11px}.button,.button:visited,.medium.button,.medium.button:visited{font-size:13px;font-weight:bold;line-height:1;text-shadow:0 -1px 1px rgba(0,0,0,0.25);}.large.button,.large.button:visited{font-size:14px;padding:8px 14px 9px;}.super.button,.super.button:visited{font-size:34px;padding:8px 14px 9px;}.pink.button,.magenta.button:visited{background-color:#e22092;}.pink.button:hover{background-color:#c81e82;}.green.button,.green.button:visited{background-color:#060;}.green.button:hover{background-color:#749a02;}.red.button,.red.button:visited{background-color:#e62727;}.red.button:hover{background-color:#cf2525;}.orange.button,.orange.button:visited{background-color:#ff5c00;}.orange.button:hover{background-color:#d45500;}.blue.button,.blue.button:visited{background-color:#2981e4;}.blue.button:hover{background-color:#2575cf;}.yellow.button,.yellow.button:visited{background-color:#ffb515;}.yellow.button:hover{background-color:#fc9200;}
</style>

{/literal}

<div class="center_title_bar">PRESENSI MKWU</div>


<form action="mkwu-presensi.php?mode=tampil" method="post"> 
<table class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
           <tr>
           	<td colspan="4">
			 	Mata Kuliah : 
                <select name="mata_kuliah">
                <option value="">Semua</option>
                {foreach $mata_kuliah_mkwu as $data}
					<option value="{$data.ID_MATA_KULIAH}" {if $data.ID_MATA_KULIAH == $smarty.request.mata_kuliah} selected="selected"{/if}>{$data.NM_MATA_KULIAH} ({$data.KD_MATA_KULIAH})</option>
				{/foreach}                	 	
			   </select>
		     </td>
           </tr>
           
           <tr>
             <td>
			 	Tahun Akademik : 
                <select name="smt">
	 		   {foreach $semester as $smt}
					<option value="{$smt.ID_SEMESTER}" {if $semester_aktif['ID_SEMESTER'] == $smt.ID_SEMESTER or $smt.ID_SEMESTER == $smarty.request.smt} selected="selected"{/if}>{$smt.THN_AKADEMIK_SEMESTER} ({$smt.NM_SEMESTER})</option>
				{/foreach}
			   </select>
		     </td>
             <td>
			 	Hari : 
                <select name="hari">
	 		   {foreach item="data" from=$hari}
    		   {html_options values=$data.ID_JADWAL_HARI output=$data.NM_JADWAL_HARI selected=$smarty.request.hari}
	 		   {/foreach}
			   </select>
		     </td>
			 <td> <input type="submit" name="Tampil" value="Tampil"> </td>
           </tr>
</table>
</form>	

{if isset($view_presensi)}
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
		<thead>
			<tr>
             <th width="5%">Kode</th>
             <th width="40%">Nama Mata Ajar</th>
             <th width="4%">SKS</th>
             <th width="10%">Ruang</th>
			 <th width="4%">KLS</th>
             <th width="7%">Hari</th>
			 <th width="16%">Jam</th>
			 <th width="4%">Pst</th>
			 <th width="10%">TTL TM</th>
           </tr>
		</thead>
		<tbody>
           {foreach name=test item="list" from=$view_presensi}
		   <tr>
             <td><a href="mkwu-presensi.php?mode=materi&mata_kuliah={$list.ID_MATA_KULIAH}&smt={$semester_request}&hari={$list.ID_JADWAL_HARI}&jam={$list.ID_JADWAL_JAM}&ruang={$list.ID_RUANGAN}&kelas={$list.ID_NAMA_KELAS}">{$list.KD_MATA_KULIAH}</a></td>
             <td>{$list.NM_MATA_KULIAH}</td>
             <td><center>{$list.KREDIT_SEMESTER}</center></td>
             <td>{$list.NM_RUANGAN}</td>
			 <td><center>{$list.NAMA_KELAS}</center></td>
             <td><center>{$list.NM_JADWAL_HARI}</center></td>
			 <td><center>{$list.JAM}</center></td>
			 <td><center>{$list.KLS_TERISI}</center></td>
			 <td><center>{$list.TM}</center></td>
            </td>
           </tr>
		   {foreachelse}
			<tr><td colspan="10"><em>Data tidak ditemukan</em></td></tr>
        {/foreach}
		</tbody>
   	 </table>
{/if}     


{if $smarty.request.mode == 'materi'}

<form action="mkwu-presensi.php?mode=materi&mata_kuliah={$smarty.request.mata_kuliah}&smt={$smarty.request.smt}&hari={$smarty.request.hari}&jam={$smarty.request.jam}&ruang={$smarty.request.ruang}&kelas={$smarty.request.kelas}" method="post" id="f1" >
	<table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
    			  <tr>
                  	<th colspan="4" align="center">Materi Kuliah dan Jadwal Presensi</th>
                  </tr>
                  <tr>
                    <td>Kode Mata Ajar</td>
                    <td>:</td>
                    <td>{$mkwu_pjma_ma.KD_MATA_KULIAH}</td>
                  </tr>
                  <tr>
                    <td>Nama Mata Ajar</td>
                    <td>:</td>
                    <td>{$mkwu_pjma_ma.NM_MATA_KULIAH}</td>
                  </tr>
                  <tr>
                    <td>SKS</td>
                    <td>:</td>
                    <td>{$mkwu_pjma_ma.KREDIT_SEMESTER}</td>
                  </tr>
                  <tr>
                    <td>Ruang / Kelas</td>
                    <td>:</td>
                    <td>{$mkwu_pjma_ma.NM_RUANGAN} / {$mkwu_pjma_ma.NAMA_KELAS}</td>
                  </tr>
                  <tr>
                    <td width="20%">Tgl Kuliah</td>
                    <td width="1%">:</td>
					<td width="79%"><input type="text" id="calawal" name="calawal" class="required" {if $smarty.request.action == 'edit'}value="{$smarty.get.calawal}"{/if} /></td>
                  </tr>
				  <tr>
                    <td>Jam Mulai Kuliah</td>
                    <td>:</td>
					<td><input type="text" name="jam_mulai" id="jam_mulai" size="7" class="required" {if $smarty.request.action == 'edit'}value="{$smarty.get.jam_mulai}"{/if} /></td>
                  </tr>
				  <tr>
                    <td>Jam Selesai Kuliah</td>
                    <td>:</td>
					<td><input type="text" name="jam_selesai" id="jam_selesai" size="7" class="required" {if $smarty.request.action == 'edit'}value="{$smarty.get.jam_selesai}"{/if} /></td>
                  </tr>
				  {foreach name=nomer item="dosen" from=$mkwu_pjma_pengampu}
				  <tr>
                    <td>Dosen Pengajar-{$smarty.foreach.nomer.iteration}</td>
                    <td>:</td>
					<td><input name="dosen{$smarty.foreach.nomer.iteration}" type="checkbox" value="{$dosen.ID_DOSEN}" {$dosen.TANDA} />{$dosen.NM_PENGGUNA}</td>
                  </tr>
				  {/foreach}
				  <input type="hidden" name="counter1" value="{$smarty.foreach.nomer.iteration}" />
				  <tr>
                    <td>Materi</td>
                    <td>:</td>
					<td><textarea name="materi" cols="60" class="required">{if $smarty.request.action == 'edit'}{$smarty.get.materi}{/if}</textarea></td>
                  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>
                      	{if $smarty.request.action == 'edit'}
                        	<input type="hidden" name="action" value="update" />
                       	{else}
	                        <input type="hidden" name="action" value="entry" />
                        {/if}
                      	<input type="submit" name="simpan" value="Simpan">
                      </td>
					</tr>
        </table>                
</form>

{if isset($PESERTA)}
<form action="mkwu-presensi.php?mode=materi&action=entrypresensi&mata_kuliah={$smarty.request.mata_kuliah}&smt={$smarty.request.smt}&hari={$smarty.request.hari}&jam={$smarty.request.jam}&ruang={$smarty.request.ruang}&kelas={$smarty.request.kelas}&id={$smarty.request.id}&calawal={$smarty.request.calawal}&jam_mulai={$smarty.request.jam_mulai}&jam_selesai={$smarty.request.jam_selesai}&materi={$smarty.request.materi}" method="post" >
<table id="myTable1" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
			     <th><center>No</center></th>
				 <th><center>NIM</center></th>
				 <th><center>Nama</center></th>
				 <th><center>Status</center></th>
				 <th><center>Absen</center></th>
			  </tr>
	</thead>
	<tbody>
			  {foreach name=presen item="pst" from=$PESERTA}
			  {if $smarty.foreach.presen.iteration % 2 == 0}
			  <tr bgcolor="lightgray">
			  {else}
			  <tr>
			  {/if}
			     <td><center>{$smarty.foreach.presen.iteration}</center></td>
				 <td>{$pst.NIM_MHS}</td>
				 <td>{$pst.NM_PENGGUNA}</td>
				 <td><center>{$pst.STKRS}</center></td>
				 <td><center>
                 	<input name="mhs{$smarty.foreach.presen.iteration}" type="checkbox" value="{$pst.ID_MHS}" {$pst.TANDA} />
                    <input type="hidden" name="mhs2{$smarty.foreach.presen.iteration}" value="{$pst.ID_MHS}" />
                    </center>
                 </td>
			   </tr>
			     {foreachelse}
        			<tr><td colspan="5"><em>Data tidak ditemukan</em></td></tr>
        	  {/foreach}
				<input type="hidden" name="counter" value="{$smarty.foreach.presen.iteration}" >
		<tr>
		<td colspan="9" style="text-align:right">
        <input type="button" name="kembali" value="Kembali" onclick="window.history.back()">
		{if $smarty.request.id==1}
		<input type="submit" name="proses" value="Proses">		
		{else}
		<input type="submit" name="proses" value="Update">
		{/if}
         </td></tr>
	</tbody>
  </table>
</form>

{else}
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
	<tr>
	 <th><center>Kuliah ke</center></th>
	 <th><center>Tgl</center></th>
	 <th><center>Jam</center></th>
	 <th><center>Materi</center></th>
	 <th><center>Dosen</center></th>
	 <th><center>Hadir</center></th>
	 <th><center>Absen</center></th>
	 <th><center>Presensi</center></th>
	 <th><center>Aksi</center></th>
	</tr>
	</thead>
	<tbody>
		{foreach name=test item="presensi" from=$mkwu_presensi_view}
	<tr>
	 <td ><center>{$smarty.foreach.test.iteration}</center></td>
	 <td ><center><a href="mkwu-presensi.php?mode=materi&action=edit&mata_kuliah={$smarty.request.mata_kuliah}&smt={$smarty.request.smt}&hari={$smarty.request.hari}&jam={$smarty.request.jam}&ruang={$smarty.request.ruang}&kelas={$smarty.request.kelas}&calawal={$presensi.TGL_PRESENSI_KELAS}&jam_mulai={$presensi.WAKTU_MULAI}&jam_selesai={$presensi.WAKTU_SELESAI}&materi={$presensi.ISI_MATERI_MK}">{$presensi.TGL_PRESENSI_KELAS}</a></center></td>
	 <td ><center>{$presensi.JAM}</center></td>
	 <td >{$presensi.ISI_MATERI_MK}</td>
	 <td ><ol>{$presensi.NMDOS}</ol></td>
	 <td ><center>{$presensi.HADIR}</center></td>
	 <td ><center>{$presensi.ABSEN}</center></td>
	 {if $presensi.HADIR==0 && $presensi.ABSEN==0}
	 <td ><center><a href="mkwu-presensi.php?mode=materi&action=presensi&mata_kuliah={$smarty.request.mata_kuliah}&smt={$smarty.request.smt}&hari={$smarty.request.hari}&jam={$smarty.request.jam}&ruang={$smarty.request.ruang}&kelas={$smarty.request.kelas}&id=1&hari={$smarty.request.hari}&jam={$smarty.request.jam}&ruang={$smarty.request.ruang}&calawal={$presensi.TGL_PRESENSI_KELAS}&jam_mulai={$presensi.WAKTU_MULAI}&jam_selesai={$presensi.WAKTU_SELESAI}&materi={$presensi.ISI_MATERI_MK}">Entry</a></center></td>
	 {else}
	 <td ><center><a href="mkwu-presensi.php?mode=materi&action=presensi&mata_kuliah={$smarty.request.mata_kuliah}&smt={$smarty.request.smt}&hari={$smarty.request.hari}&jam={$smarty.request.jam}&ruang={$smarty.request.ruang}&kelas={$smarty.request.kelas}&id=2&hari={$smarty.request.hari}&jam={$smarty.request.jam}&ruang={$smarty.request.ruang}&calawal={$presensi.TGL_PRESENSI_KELAS}&jam_mulai={$presensi.WAKTU_MULAI}&jam_selesai={$presensi.WAKTU_SELESAI}&materi={$presensi.ISI_MATERI_MK}">Edit</a></center></td>
	 {/if}
	  <td ><center>

      <a href="mkwu-presensi.php?mode=materi&action=hapuspresensi&mata_kuliah={$smarty.request.mata_kuliah}&smt={$smarty.request.smt}&hari={$smarty.request.hari}&jam={$smarty.request.jam}&ruang={$smarty.request.ruang}&kelas={$smarty.request.kelas}&id=2&hari={$smarty.request.hari}&jam={$smarty.request.jam}&ruang={$smarty.request.ruang}&calawal={$presensi.TGL_PRESENSI_KELAS}&jam_mulai={$presensi.WAKTU_MULAI}&jam_selesai={$presensi.WAKTU_SELESAI}&materi={$presensi.ISI_MATERI_MK}">Hapus</a></center></td>
	</tr>
	 {foreachelse}
		<tr><td colspan="9"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>
{/if}


{/if}



{literal}
    <script>
	    $("#jam_mulai").timepicker({timeFormat: 'hh:mm'});
	    $("#jam_selesai").timepicker({timeFormat: 'hh:mm'});
            $("#calawal").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
            $('#f1').validate();
    </script>
{/literal}