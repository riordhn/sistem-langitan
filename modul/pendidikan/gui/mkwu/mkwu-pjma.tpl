{literal}
<script type="text/javascript" src="includes/sortable/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
 $("#myTable").tablesorter(
		{
		sortList: [[1,0], [2,0]],
		headers: {
   8: { sorter: false },
		}
		}
	);
}
);
</script>
{/literal}


<div class="center_title_bar">PJMA MKWU</div>


{if $smarty.request.mode == 'addview'}

<table width="100%" border="1">
            <tr>
              <td>
			  

              <table class="tb_frame" width="100%" border="1" cellspacing="0" cellpadding="0" id="tableText">
                  <tr>
                    <td width="149">Kode Mata Ajar</td>
                    <td width="9">:</td>
                    <td width="754">{$t_pjma_addview.KD_MATA_KULIAH}</td>
                  </tr>
				  <tr>
                    <td width="149">Nama Mata Ajar</td>
                    <td width="9">:</td>
                    <td>{$t_pjma_addview.NM_MATA_KULIAH}</td>
                  </tr>
				  <tr>
                    <td width="149">SKS Mata Ajar</td>
                    <td width="9">:</td>
                    <td>{$t_pjma_addview.KREDIT_SEMESTER}</td>
                  </tr>

				{if count($t_pjma_pengampu)>=1}
				{foreach item="pgp" from=$t_pjma_pengampu}

				{if $pgp.PJMK_PENGAMPU_MK==1}
                  <tr>
                    <td>PJMA </td>
                    <td>:</td>
                    <td>
                    {$pgp.NIP_DOSEN} - {$pgp.NM_PENGGUNA} &nbsp;
					<a href="mkwu-pjma.php?mode=searchdosen&id_dosen_ganti={$pgp.ID_DOSEN}&action=edit&id_kur={$smarty.request.id_kur}&smt={$semester_request}">Edit</a> | 
                    <a href="mkwu-pjma.php?mode=addview&id_dosen={$pgp.ID_DOSEN}&action=hapus&id_kur={$smarty.request.id_kur}&smt={$semester_request}">Hapus</a>
                    </td>
                  </tr>
				{else}
                  <tr>
                    <td>Anggota </td>
                    <td>:</td>
                    <td>
                    {$pgp.NIP_DOSEN} - {$pgp.NM_PENGGUNA} &nbsp;
					<a href="mkwu-pjma.php?mode=searchdosen&action=edit&id_dosen_ganti={$pgp.ID_DOSEN}&id_kur={$smarty.request.id_kur}&smt={$semester_request}">Edit</a> | 
                    <a href="mkwu-pjma.php?mode=addview&action=hapus&id_dosen={$pgp.ID_DOSEN}&id_kur={$smarty.request.id_kur}&smt={$semester_request}">Hapus</a>
                    </td>
                  </tr>
				{/if}
				 {/foreach}
				 {else}
				  	<tr>
                    <td>PJMA </td>
                    <td>:</td>
                    <td>
                    {$pgp.NIP_DOSEN} - {$pgp.NM_PENGGUNA}
					<a href="mkwu-pjma.php?mode=searchdosen&pjmk=1&id_kur={$smarty.request.id_kur}&smt={$semester_request}">Search</a>
                    </td>
                  </tr>
				 {/if}
               
                </table>
				{if count($t_pjma_pengampu)==0}
                  <p>
					<input name="Kembali" type="button" value="Kembali" ONCLICK="window.location.href='#kkn-kkn6!mkwu-pjma.php'"/>
					{if $PJ=='kosong'}
					<input name="nip_pjmk" id="nippjmk" type="button" value="Tambah PJMA" ONCLICK="window.location.href='#kkn-kkn6!mkwu-pjma.php?mode=searchdosen&pjmk=1&id_kur={$smarty.request.id_kur}'"/>
					{else}
					<input type="button" name="addButton" id="addButton" value="Tambah Anggota" ONCLICK="window.location.href='#kkn-kkn6!mkwu-pjma.php?mode=searchdosen&pjmk=2&id_kur={$smarty.request.id_kur}'"/>
					{/if}
					</p>
				{else}
                  <p>
					<input name="Kembali" type="button" value="Kembali" ONCLICK="window.location.href='#kkn-kkn6!mkwu-pjma.php'"/>
					<input type="button" name="addButton" id="addButton" value="Tambah Anggota" ONCLICK="window.location.href='#kkn-kkn6!mkwu-pjma.php?mode=searchdosen&pjmk=2&id_kur={$smarty.request.id_kur}&smt={$semester_request}'"/>
					</p>
				{/if}
				</td>
            </tr>
	
</table>
          
          
{else if $smarty.request.mode == 'searchdosen'}


<form action="mkwu-pjma.php?mode=searchdosen&action={$smarty.request.action}&id_dosen_ganti={$smarty.request.id_dosen_ganti}&id_kur={$smarty.request.id_kur}&pjmk={$smarty.request.pjmk}&smt={$semester_request}" method="post">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>Nama Dosen
    <td>: <input name="namadosen" type="text" size="50" value="{$smarty.request.namadosen}" /> <input type="submit" name="cari" value="Cari Dosen">
	</td>
	</tr>
</table>
</form>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
			   <tr>
			     <th width="2%"><font color="#FFFFFF">No</font></th>
				 <th width="20%"><font color="#FFFFFF">Photo Dosen</font></th>
				 <th width="8%"><font color="#FFFFFF">Kode Dosen</font></th>
				 <th width="45%"><font color="#FFFFFF">Nama Dosen</font></th>
				 <th width="20%"><font color="#FFFFFF">Prodi</font></th>
				 <th width="5%"><font color="#FFFFFF">Aksi</font></th>
			  </tr>
			  <tr>
			 {foreach name=test item="list" from=$t_pjma_searchdosen}
			     <td>{$smarty.foreach.test.iteration}</td>
				 <td><img src="{$list.FOTO_PENGGUNA}/{$list.NIP_DOSEN}.JPG" width="160px"/></td>
				 <td>{$list.NIP_DOSEN}</td>
				 <td>{$list.NM_PENGGUNA}</td>
				 <td>{$list.NM_PROGRAM_STUDI}</td>
				 <td><a href="mkwu-pjma.php?mode=addview&action={$smarty.request.action}&id_dosen_ganti={$smarty.request.id_dosen_ganti}&id_dosen={$list.ID_DOSEN}&id_kur={$smarty.request.id_kur}&pjmk={$smarty.request.pjmk}&smt={$semester_request}">Pilih</a></td>
			</tr>
        {/foreach}
</table>          

{else if $smarty.request.mode == 'copy'}

<form action="mkwu-pjma.php" method="post">
<input type="hidden" name="action" value="copy" />
<input type="hidden" name="id_kur" value="{$smarty.request.id_kur}" >
 <table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>Copy Dari </td>
    <td>:
      <select name="cp" id="cp">
        <option value="">-- Pilih Kelas --</option>
       		{foreach item="cp1" from=$t_pjma_copy}
    		   {html_options  values=$cp1.ID_KELAS_MK output=$cp1.NAMA}
	 		{/foreach}
      </select>
    </td>

<td> <input type="submit" name="Proses" value="Proses"> <input name="Kembali" type="button" value="Kembali" ONCLICK="window.location.href='#kkn-kkn6!mkwu-pjma.php'"/></td></tr>

</table>
</form>
          
{else}
<form action="mkwu-pjma.php" method="post">
<table class="tb_frame" width="80%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>Tahun Ajaran : 
			<select name="smt" id="thn_ajaran">
			<option value="">-- Tahun Ajaran --</option>
				{foreach $semester as $smt}
					<option value="{$smt.ID_SEMESTER}" {if $semester_aktif['ID_SEMESTER'] == $smt.ID_SEMESTER or $smt.ID_SEMESTER == $smarty.request.smt} selected="selected"{/if}>{$smt.THN_AKADEMIK_SEMESTER} ({$smt.NM_SEMESTER})</option>
				{/foreach}
			</select>
			<input type="submit" name="View" value="Tampil">
		</td>
	</tr>
</table>
</form>
<table id="myTable" class="tablesorter" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<th width="8%">Kode</th>
			<th width="25%">Nama Mata Ajar</th>
			<th width="40%">Tim Pengajar</th>
			<th class="noheader" width="10%">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{foreach name=test item="list" from=$t_pjma}
		{if $list.ID_STATUS == "0"}
		<tr bgcolor="yellow">
		{else}
		<tr>
		{/if}
			<td>{$list.KD_MATA_KULIAH}</td>
			<td>{$list.NM_MATA_KULIAH}</td>  
		{if $list.ID_STATUS == "0"}
			<td></td>
		{else}
			<td><br><ol>{$list.TIM}</li></ol></td>
		{/if}
		{if $list.ID_STATUS == "0"}
			<td><center>    
          <a href="mkwu-pjma.php?mode=addview&id_kur={$list.ID_KURIKULUM_MK}&pjmk={$list.ID_STATUS}&smt={$semester_request}">Isi</a>
            </center></td>
		{else}
			<td><center>            
            <a href="mkwu-pjma.php?mode=addview&id_kur={$list.ID_KURIKULUM_MK}&pjmk={$list.ID_STATUS}&smt={$semester_request}">Edit</a>
            <br />
			<a href="mkwu-pjma.php?action=hapus&id_kur={$list.ID_KURIKULUM_MK}&smt={$semester_request}">Hapus</a>
            </center></td>
		{/if}
		</tr>
		{foreachelse}
		<tr><td colspan="6"><em>Data tidak ditemukan</em></td></tr>
		{/foreach}
	</tbody>
</table>
{/if}
