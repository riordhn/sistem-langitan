<div class="center_title_bar">Master Evaluasi Instrumen - Tambah</div>

<form action="kuisioner-evalinstrumen.php" method="post">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <td>Nama Instrumen</td>
            <td>
		<textarea name="NAMA_EVAL_INSTRUMEN" class="required" cols="40" rows="2" />
	    </td>
        </tr>
        <tr>
            <td>Keterangan Evaluasi Instrumen</td>
            <td><textarea name="KET_EVAL_INSTRUMEN" class="required" cols="40" rows="3" /></td>

        </tr>
        <tr>
            <td class="center" colspan="2">
                <input type="button" value="Batal" Onclick="window.history.back()">
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>

{literal}
<script language="javascript">
	$('form').validate();
</script>
{/literal}