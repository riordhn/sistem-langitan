<div class="center_title_bar">Master Aspek Evaluasi</div>

<form action="kuisioner-evalaspek.php" method="post">
<table>
	<tr>
		<td>Pilih Instrument</td>
		<td>:</td>
		<td><select name="instrument">
				<option value="">Semua</option>
			{foreach $instrument as $data}
				
					<option value="{$data.ID_EVAL_INSTRUMEN}" {if $smarty.post.instrument == $data.ID_EVAL_INSTRUMEN} selected="selected"{/if}>{$data.NAMA_EVAL_INSTRUMEN}</option>
				
			{/foreach}
			</select>
		</td>
		<td>
			<input type="submit" value="Tampil" />
			<input type="hidden" value="view" name="mode"/>
		</td>
	</tr>
</table>
</form>

{if isset($evaluasi_aspek_set)}
<table>
    <tr>
        <th>No</th>
        <th>Nama Aspek Evaluasi</th>
        <th>Nama Kelompok Aspek</th>
		<th>Nama Evaluasi Instrumen </th>
        <th>Nama Tipe Aspek </th>
        <th>Status</th>
	<th>Aksi</th>
    </tr>
    {$i=1}
    {foreach $evaluasi_aspek_set as $evaluasi_aspek}
    <tr>
	
        <td>{$i++}</td>
        <td>{$evaluasi_aspek.EVALUASI_ASPEK}</td>
        <td>{$evaluasi_aspek.NAMA_KELOMPOK}</td>
        <td>{$evaluasi_aspek.NAMA_EVAL_INSTRUMEN}</td>
        <td>{if $evaluasi_aspek.TIPE_ASPEK == 1}
          		Pilihan
             {else $evaluasi_aspek.TIPE_ASPEK == 2}
          		Komentar Tertulis{/if}</td>
	<td>{if $evaluasi_aspek.STATUS_AKTIF == 1}Aktif{else}Tidak Aktif{/if}</td>
       <td>
            <a href="kuisioner-evalaspek.php?mode=edit&id_eval_aspek={$evaluasi_aspek.ID_EVAL_ASPEK}">Edit</a>
            <a href="kuisioner-evalaspek.php?mode=delete&id_eval_aspek={$evaluasi_aspek.ID_EVAL_ASPEK}">Delete</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="8"><a href="kuisioner-evalaspek.php?mode=add">Tambah</a></td>
    </tr>
</table>
{/if}