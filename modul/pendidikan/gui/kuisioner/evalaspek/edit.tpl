<div class="center_title_bar">Master Kelompok Aspek - Tambah</div>

<form action="kuisioner-evalaspek.php?mode=edit" method="post" id="gen_mhs">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_eval_aspek" value="{$evaluasi_aspek.ID_EVAL_ASPEK}" />
    <table width="693">
	<tr>
            <td width="196">Nama Aspek</td>
      <td width="485"><textarea name="EVALUASI_ASPEK" class="required" rows="2" cols="50">{$evaluasi_aspek.EVALUASI_ASPEK}</textarea></td>
      </tr>
	  <tr>
        	<td>Instrumen</td>
            <td>
            	<select id="id_eval_instrumen"  name="id_eval_instrumen" class="required">
                	<option value="">PILIH INSTRUMEN</option>
                    {foreach $instrument as $data}
                        <option value="{$data.ID_EVAL_INSTRUMEN}" {if $evaluasi_aspek.ID_EVAL_INSTRUMEN==$data.ID_EVAL_INSTRUMEN}selected="true"{/if}>{$data.NAMA_EVAL_INSTRUMEN|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
	    <tr>
        	<td>Kelompok Aspek</td>
            <td>
            	<select id="kelompok_aspek" name="kelompok_aspek" class="required">
                    <option value="">PILIH KELOMPOK ASPEK</option>
                    {foreach $evaluasi_kelompok_aspek_set as $data}
                         <option value="{$data.ID_EVAL_KELOMPOK_ASPEK}" {if $evaluasi_aspek.ID_EVAL_KELOMPOK_ASPEK==$data.ID_EVAL_KELOMPOK_ASPEK}selected="true"{/if}>{$data.NAMA_KELOMPOK}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
	<tr>
            <td>Tipe Aspek</td>
            <td>
            	<input type="radio" value="1" name="TIPE_ASPEK"  class="required" {if $evaluasi_aspek.TIPE_ASPEK == 1}checked="checked"{/if}/>
				Pilihan
            	<input type="radio" value="2" name="TIPE_ASPEK" {if $evaluasi_aspek.TIPE_ASPEK == 2}checked="checked"{/if} />
            	Komentar Tertulis    
				</td>
        </tr>
       <tr>
            <td>Kelompok Nilai </td>
            <td>
            	<select name="ID_KELOMPOK_NILAI" class="required" id="id_kelompok_nilai">
                	<option value="">-- Pilih Kelompok Nilai --</option>
                	{foreach $evaluasi_kelompok_nilai_set as $data}
            		<option value="{$data.ID_KELOMPOK_NILAI}" {if $evaluasi_aspek.ID_KELOMPOK_NILAI==$data.ID_KELOMPOK_NILAI}selected="true"{/if}>{$data.NM_KELOMPOK}</option>
                    {/foreach}
                </select>         </td>
        </tr>
	<tr>
            <td>Skala Nilai </td>
            <td><div id="skala_nilai">
		<ul>
		{foreach $skala_nilai as $data}
			<li>{$data.KET_NILAI_ASPEK} ({$data.NILAI_ASPEK})</li>
		{/foreach}
		</ul>
	    </div></td>
        </tr> 
	<tr>
            <td>Aktifasi</td>
            <td>
            	<input type="radio" value="1" name="STATUS_AKTIF"  class="required" {if $evaluasi_aspek.STATUS_AKTIF == 1}checked="checked"{/if}/>
				Aktif
            	<input type="radio" value="0" name="STATUS_AKTIF" {if $evaluasi_aspek.STATUS_AKTIF == 0}checked="checked"{/if} />
            	Tidak Aktif   
				</td>
        </tr>
	<tr>
            <td>Urutan</td>
            <td>
		<select name="URUTAN" class="required">
			<option value="">No Urut</option>
			{for $no=1 to 30}
				<option value="{$no}" {if $evaluasi_aspek.URUTAN==$no}selected="true"{/if}>{$no}</option>
			{/for}
		</select>
	    </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
                <input type="submit" value="Simpan" />            
		</td>
        </tr>
  </table>
</form>

{literal}
    <script type="text/javascript">
		$('#gen_mhs').validate();
		
$('#id_eval_instrumen').change(function(){
            $.ajax({
                type:'post',
                url:'getkelompok_eval_aspek.php',
                data:'id_eval_instrumen='+$('#id_eval_instrumen').val(),
                success:function(data){
                    $('#kelompok_aspek').html(data);
                }                    
            })
        });

$('#id_kelompok_nilai').change(function(){
            $.ajax({
                type:'post',
                url:'getSkalaNilai.php',
                data:'id_kelompok_nilai='+$('#id_kelompok_nilai').val(),
                success:function(data){
		    $('#skala_nilai').html(data);
                }                    
            })
        });
		
</script>
{/literal}