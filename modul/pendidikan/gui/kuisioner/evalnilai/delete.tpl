<div class="center_title_bar">Master Nilai Aspek - Hapus</div>
<h4>Apakah data nilai aspek ini akan dihapus ?</h4>
<form action="kuisioner-evalnilai.php" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="ID_EVAL_NILAI_ASPEK" value="{$evaluasi_nilai.ID_EVAL_NILAI_ASPEK}" />
    <table>
        <tr>
            <td>Nama Kelompok Nilai</td>
            <td>{$evaluasi_nilai.NM_KELOMPOK}</td>
        </tr>
        <tr>
            <td>Keterangan Nilai Aspek</td>
            <td>{$evaluasi_nilai.KET_NILAI_ASPEK}</td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <input type="button" value="Batal" Onclick="window.history.back()">
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>