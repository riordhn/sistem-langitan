<div class="center_title_bar">Master Evaluasi Kelompok Nilai</div>

<table>
    <tr>
        <th>No</th>
        <th>Nama Evaluasi Instrumen</th>
        <th>Keterangan Evaluasi Instrumen</th>
        <th></th>
    </tr>
    {$i=1}
    {foreach $evaluasi_kelompok_nilai_set as $evaluasi_kelompok_nilai}
    <tr>
        <td>{$i++}</td>
        <td>{$evaluasi_kelompok_nilai.NM_KELOMPOK}</td>
        <td>
            <a href="kuisioner-evalkelompoknilai.php?mode=edit&id_kelompok_nilai={$evaluasi_kelompok_nilai.ID_KELOMPOK_NILAI}">Edit</a>
            <a href="kuisioner-evalkelompoknilai.php?mode=delete&id_kelompok_nilai={$evaluasi_kelompok_nilai.ID_KELOMPOK_NILAI}">Delete</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td class="center" colspan="5"><A href="kuisioner-evalkelompoknilai.php?mode=add">Tambah</A></td>
    </tr>
</table>