<div class="center_title_bar">Laporan Kuisioner Wisuda</div>

<table>
    <tr>
        <th>No</th>
        <th class="center">Nama Aspek</th>
        <th class="center">Sangat Tidak Setuju</th>
        <th class="center">Tidak Setuju</th>
        <th class="center">Setuju</th>
        <th class="center">Sangat Setuju</th>
        <th class="center">Tidak Dapat Berkomentar</th>
    </tr>
    {$i=1}
    {foreach $lapo_wisuda as $data}
    <tr>
        <td class="center">{$i++}</td>
        <td>{$data.EVALUASI_ASPEK}</td>
		<td class="center">{$data.A1}</td>
        <td class="center">{$data.A2}</td>
        <td class="center">{$data.A3}</td>
        <td class="center">{$data.A4}</td>
		<td class="center">{$data.A0}</td>
    </tr>
    {/foreach}
	<tr>
		<td colspan="9" style="text-align:center">
		<input type="button" value="Cetak" onclick="window.open('kuisioner-wisuda-laporan-cetak.php');"/>
        </td>
		</tr>
</table>