<div class="center_title_bar">Master Evaluasi Instrumen - Edit</div>

<form action="kuisioner-evalkelompokaspek.php" method="post" id="prod">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_eval_kelompok_aspek" value="{$evaluasi_kelompok_aspek_set[0]['ID_EVAL_KELOMPOK_ASPEK']}" />
    <table>
	    <tr>
            <td>Nama Instrumen</td>
            <td>
            	<select name="instrumen" class="required">
                	<option value="">-- Pilih Instrumen --</option>
                	{foreach $instrumen_set as $data}
            		<option value="{$data.ID_EVAL_INSTRUMEN}" {if $evaluasi_kelompok_aspek_set[0]['ID_EVAL_INSTRUMEN']==$data.ID_EVAL_INSTRUMEN} selected="selected" {/if}>{$data.NAMA_EVAL_INSTRUMEN}</option>
                    {/foreach}
                </select>            </td>
        </tr>
        <tr>
            <td>Nama Evaluasi Kelompok Aspek</td>
            <td><input name="NAMA_KELOMPOK" type="text" class="required" value="{$evaluasi_kelompok_aspek_set[0]['NAMA_KELOMPOK']}" size="80" /></td>
        </tr>
        <tr>
            <td class="center" colspan="2">
            	<input type="button" onclick="javascript:history.back(1)" value="Batal" />
                <input type="submit" value="Simpan" />            </td>
        </tr>
    </table>
</form>

{literal}
<script language="javascript">
	$('form').validate();
</script>
{/literal}