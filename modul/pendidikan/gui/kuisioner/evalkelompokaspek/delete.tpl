<div class="center_title_bar">Master Evaluasi Kelompok Aspek - Hapus</div>
<h4>Apakah data evaluasi kelompok aspek ini akan dihapus ?</h4>
<form action="kuisioner-evalkelompokaspek.php" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="ID_EVAL_KELOMPOK_ASPEK" value="{$evaluasi_kelompok_aspek.ID_EVAL_KELOMPOK_ASPEK}" />
    <table>
        <tr>
            <td>Nama Evaluasi Kelompok Aspek</td>
            <td>{$evaluasi_kelompok_aspek.NAMA_KELOMPOK}</td>
        </tr>
        <tr>
            <td>Nama Instrumen</td>
            <td>{$evaluasi_kelompok_aspek.NAMA_EVAL_INSTRUMEN}</td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <input type="button" value="Batal" Onclick="window.history.back()">
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>