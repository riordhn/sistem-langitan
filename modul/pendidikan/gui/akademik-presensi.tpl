   <div class="center_title_bar">Presensi Kuliah</div>  
     <table width="80%" border="0" cellpadding="0" cellspacing="0">
       <tr>
       <td><table width="70%" border="1">
         <tr>
           <td><table class="tb_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td>Prodi</td>
               <td>:</td>
               <td>Teknik Kelautan </td>
             </tr>
             <tr>
               <td width="135">Kode Mata Kuliah </td>
               <td width="10">:</td>
               <td width="239"><input name="textfield" type="text" value="MK-0012"></td>
             </tr>
             <tr>
               <td>Tahun Ajaran </td>
               <td>:</td>
               <td><select name="select">
                   <option>2010/2011</option>
               </select></td>
             </tr>
             <tr>
               <td>Semester</td>
               <td>:</td>
               <td><select name="select">
                   <option>Gasal</option>
               </select></td>
             </tr>
             <tr>
               <td>Kegiatan</td>
               <td>:</td>
               <td><select name="select">
                 <option>Kuliah</option>
                 <option>Praktikum</option>
                 <option>UTS</option>
                 <option>UAS</option>
                 <option>Rapat</option>
               </select></td>
             </tr>
           </table>
           <p>
               <input type="submit" name="Submit" value="Preview">
             </p></td>
         </tr>
       </table>       
         <table class="tb_dosen" width="80%" border="0" cellpadding="0" cellspacing="0">
           <tr>
             <td class="tb_frame">Presensi</td>
             <td class="tb_frame">:</td>
             <td class="tb_frame">Kuliah</td>
             <td width="108" class="tb_frame">Ruang</td>
             <td width="9" class="tb_frame">:</td>
             <td width="148" class="tb_frame">R-209</td>
           </tr>
           <tr>
             <td class="tb_frame">Prodi</td>
             <td class="tb_frame">:</td>
             <td class="tb_frame">Teknik Kelautan </td>
             <td class="tb_frame">Jam</td>
             <td class="tb_frame">:</td>
             <td class="tb_frame">07:00 - 08-50 </td>
           </tr>
           <tr>
             <td width="131" class="tb_frame">Nama Mata Kuliah </td>
             <td width="8" class="tb_frame">:</td>
             <td width="144" class="tb_frame">Jaringan</td>
             <td>Tahun Ajaran </td>
             <td>:</td>
             <td>2000/2001</td>
           </tr>
           <tr>
             <td class="tb_frame">Dosen Pembina</td>
             <td class="tb_frame">:</td>
             <td class="tb_frame">Ir. Herawati </td>
             <td>Semester</td>
             <td>:</td>
             <td>Gasal</td>
           </tr>
         </table>
         <br>
         Peserta Mata kuliah <br>
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr class="left_menu">
             <td width="5%" bgcolor="#333333"><font color="#FFFFFF">No.</font></td>
             <td width="24%" bgcolor="#333333"><font color="#FFFFFF">NIM</font></td>
             <td width="23%" bgcolor="#333333"><font color="#FFFFFF">Nama Mahasiswa </font> </td>
             <td width="12%" bgcolor="#333333"><font color="#FFFFFF">Tgl 2/3</font></td>
             <td width="12%" bgcolor="#333333"><font color="#FFFFFF">Tgl 9/3</font></td>
             <td width="12%" bgcolor="#333333"><font color="#FFFFFF">Tgl 16/3</font></td>
             <td width="12%" bgcolor="#333333"><font color="#FFFFFF">Tgl 23/3</font></td>
           </tr>
           <tr>
             <td >1</td>
             <td >0809123023</td>
             <td >Kurniawati Sularsih</td>
             <td >H</td>
             <td >H</td>
             <td >H</td>
             <td >H</td>
           </tr>
           <tr>
             <td >2</td>
             <td >0809123024</td>
             <td >Putri Ayuningendi</td>
             <td >H</td>
             <td >H</td>
             <td >&nbsp;</td>
             <td >H</td>
           </tr>
         </table>
         <br>
Dosen PJMK / Pembina Mata Kuliah<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="left_menu">
    <td width="5%" bgcolor="#333333"><font color="#FFFFFF">No.</font></td>
    <td width="26%" bgcolor="#333333"><font color="#FFFFFF">NIP</font></td>
    <td width="24%" bgcolor="#333333"><font color="#FFFFFF">Nama Dosen PJMK </font> </td>
    <td width="11%" bgcolor="#333333"><font color="#FFFFFF">Tgl 23/3</font></td>
    <td width="13%" bgcolor="#333333"><font color="#FFFFFF">Tgl 23/3</font></td>
    <td width="11%" bgcolor="#333333"><font color="#FFFFFF">Tgl 23/3</font></td>
    <td width="10%" bgcolor="#333333"><font color="#FFFFFF">Tgl 23/3</font></td>
  </tr>
  <tr>
    <td >1</td>
    <td >123456 123456 1234 </td>
    <td >Drs. Kristanto, M.Si. </td>
    <td >H</td>
    <td >H</td>
    <td >H</td>
    <td >&nbsp;</td>
  </tr>
  <tr>
    <td >2</td>
    <td >123456 123456 1234 </td>
    <td >Drs. Abdur, M.Si. </td>
    <td >&nbsp;</td>
    <td >&nbsp;</td>
    <td >&nbsp;</td>
    <td >H</td>
  </tr>
  <tr>
    <td >...</td>
    <td >...</td>
    <td >...`</td>
    <td >...</td>
    <td >...</td>
    <td >...</td>
    <td >...</td>
  </tr>
</table>
<br>
         <p>           <input type="submit" name="Submit" value="Cetak">
         </p>
         <p>Keterangan :<br>
           H : Hadir</p>
         <div class="panel" id="panel1" style="display: block"></div></td>
       </tr>
</table>

		<div class="panel" id="panel2" style="display: none">
				  <table class="tb_frame" width="46%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td width="213">Kode Mahasiswa</td>
					  <td width="10">:</td>
					  <td width="274">080911911</td>
					</tr>
					<tr>
					  <td>Nama Mahasiswa </td>
					  <td>:</td>
					  <td>Ismail Bisri</td>
					</tr>
					<tr>
					  <td>Edit Status </td>
					  <td>&nbsp;</td>
					  <td><select name="select">
					    <option>Tidak Aktif</option>
				      </select></td>
				    </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td><input type="submit" name="Submit" value="Simpan"></td>
					</tr>
				  </table>
		</div>