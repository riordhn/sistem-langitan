<div class="center_title_bar">Status Entri Nilai</div>

<table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 5px">
	<tr>
		<td>
			<form action="monitoring-nilai.php" method="get">
				Program Studi :
					<select name="id_program_studi">
						<option value="" >-- Pilih Program Studi --</option>
					{foreach $prodi_set as $prodi}
						<option value="{$prodi.ID_PROGRAM_STUDI}" {if $prodi.ID_PROGRAM_STUDI == $id_program_studi_terpilih}selected{/if} >{$prodi.NM_PROGRAM_STUDI}</option>
					{/foreach}
					</select>
				Semester :
					<select name="id_semester">
					{foreach $semester_set as $semester}
						<option value="{$semester.ID_SEMESTER}" {if $semester.ID_SEMESTER == $id_semester_terpilih}selected{/if}
						>{$semester.TAHUN_AJARAN} {$semester.NM_SEMESTER}{if $semester.STATUS_AKTIF_SEMESTER == 'True'} (Aktif){/if}</option>
					{/foreach}
					</select>
				<input type="submit" name="View" value="View">
			</form>
		</td>
	</tr>
</table>

<table>
	<thead>
		<tr>
			<th>No</th>
			<th>Prodi</th>
			<th>Kode</th>
			<th>Mata Kuliah</th>
			<th>SKS</th>
			<th>Kelas</th>
			<th>Dosen PJMK</th>
			<th>Setting Komponen</th>
			<th>Mahasiswa</th>
			<th>Penilaian Dosen</th>
			<th>Penilaian Staff</th>
		</tr>
	</thead>
	<tbody>
		{foreach $kelas_set as $kelas}
			<tr>
				<td class="center">{$kelas@index + 1}</td>
				<td>{$kelas.NM_PROGRAM_STUDI}</td>
				<td>{$kelas.KD_MATA_KULIAH}</td>
				<td>{$kelas.NM_MATA_KULIAH}</td>
				<td class="center">{$kelas.SKS}</td>
				<td>{$kelas.NAMA_KELAS}</td>
				<td>{$kelas.NM_DOSEN_PJMK}</td>
				<td class="center">{if $kelas.JML_KOMPONEN_NILAI > 0}Sudah{else}Belum{/if}</td>
				<td class="center">{$kelas.KRS_APPROVE}</td>
				<td class="center" {if $kelas.JML_NILAI+$kelas.JML_NILAI_PEGAWAI < $kelas.KRS_APPROVE}style="background-color: yellow"{/if}>{$kelas.JML_NILAI}</td>
				<td class="center" {if $kelas.JML_NILAI+$kelas.JML_NILAI_PEGAWAI < $kelas.KRS_APPROVE}style="background-color: yellow"{/if}>{$kelas.JML_NILAI_PEGAWAI} {if $kelas.JML_NILAI_PEGAWAI > 0}<br> {$kelas.NM_PEGAWAI}{/if}</td>
			</tr>
		{/foreach}
	</tbody>
</table>