<div class="center_title_bar">Evaluasi Gasal</div>
<form action="evaluasi-gasal.php" method="post" name="f2" id="f2">
	<table width="700">
    	<tr>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas">
					<option value="">Semua Fakultas</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $smarty.request.fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
            <td>Semester</td>
            <td>
            	<select name="semester" class="required">
                	<option value="">Pilih Semester</option>
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $smarty.request.semester==$data.ID_SEMESTER or $data.ID_SEMESTER==$semester_aktif} selected="selected" {/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($evaluasi)}
<form action="evaluasi-gasal.php" name="f1" id="f1" method="post">
<table style="font-size:11px">
	<tr>
		<th style="text-align:center">NO</th>
		<th style="text-align:center">NIM</th>
		<th style="text-align:center">NAMA</th>
		<th style="text-align:center">JENJANG</th>
		<th style="text-align:center">PRODI</th>
		<th style="text-align:center">STATUS TERKINI</th>
		
		<th style="text-align:center">SKS DIWAJIBKAN</th>
        
		<th style="text-align:center">SKS DIPEROLEH</th>
		<th style="text-align:center">IPK</th>
        <th style="text-align:center">SKS Dengsn IPK 2</th>
		<th style="text-align:center">% NILAI D</th>
		<th style="text-align:center">PIUTANG SEMESTER</th>
		<th style="text-align:center">CUTI SEMESTER</th>
		<th style="text-align:center">SEMESTER MASUK MHS</th>
		
		<th style="text-align:center">REKOMENDASI STATUS</th>
		<th style="text-align:center">KETERANGAN</th>
        
	</tr>
	{$no = 1}
	{foreach $evaluasi as $data}
	<tr>
		<td>{$no++}</td>
		<td><a href="evaluasi-status-mhs.php?nim={$data.NIM_MHS}">{$data.NIM_MHS}</a></td>
		<td>{$data.NM_PENGGUNA}</td>
		<td>{$data.NM_JENJANG} {if $data.STATUS == 'AJ'}(Alih Jenis){/if}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
		<td>{$data.NM_STATUS_PENGGUNA}</td>
		
			<td>
            	{if $data.ID_JENJANG == 1 and ($data.ID_JALUR != 4 and $data.ID_JALUR != 22)}144{else if $data.ID_JENJANG  == 5}110{elseif $data.ID_JENJANG  == 1 and ($data.ID_JALUR == 4 or $data.ID_JALUR == 22)}45{else}{/if}
            </td>
		
		<td>{$data.SKS}</td>
		<td>{$data.IPK}</td>
        <td>{$data.SKS_IPK_2}</td>
		<td>{$data.PERSEN_NILAI_D}</td>
		<td>{$data.PIUTANG}</td>
		<td></td>
		<td>{$data.NM_SEMESTER}</td>
		
		<td>
					<input type="hidden" value="{$data.ID_MHS}" name="id_mhs{$no}" />
					<input type="hidden" value="{$data.SKS}" name="sks{$no}" />
					<input type="hidden" value="{$data.IPK}" name="ipk{$no}" />
                    <input type="hidden" value="{$data.PIUTANG}" name="piutang{$no}" />
					<input type="hidden" value="{$data.ID_STATUS_PENGGUNA}" name="status_terkini{$no}" />
					<input name="tgl_rekomendasi{$no}" type="hidden" class="datepicker" size="8" value="{$smarty.now|date_format:"%e-%m-%Y"}"/>
			<select name="rekomendasi{$no}">
				<option value="">Pilih Status</option>
				{foreach $status as $x}
					<option value="{$x.ID_STATUS_EVALUASI}" {if $data.REKOMENDASI_STATUS==$x.ID_STATUS_EVALUASI}selected="selected"{/if}>{$x.NM_STATUS_EVALUASI}</option>
				{/foreach}
			</select>
		</td>
		<td><textarea name="keterangan{$no}" rows="4" cols="30">{$data.KETERANGAN}</textarea></td>
</tr>
    <tr style="display:none" id="detail{$data@index+1}">
    	<td colspan="17">
        	<table style="width:100%" id="detail2{$data@index+1}">
               
                
            </table>
    	</td>
    </tr>
	{/foreach}
	<tr>
		<td colspan="25" style="text-align:center">
			<input type="submit" value="Simpan" />
			<input type="button" value="Cetak" onclick="window.open('evaluasi-gasal-cetak.php?sem={$smarty.request.semester}&fak={$smarty.request.fakultas}&jenj={$smarty.request.jenjang}');"/>
			<input type="hidden" value="{$no}" name="no" />
			<input type="hidden" value="{$smarty.request.fakultas}" name="fakultas" />
			<input type="hidden" value="{$smarty.request.semester}" name="semester" />
			<input type="hidden" value="insert" name="mode" />
		</td>
	</tr>
</table>
</form>
{/if}

{literal}
    <script>
			$("#f2").validate();
            $(".datepicker").datepicker({dateFormat:'dd-mm-yy',changeMonth: true,changeYear: true});
			$("#cek_all").click(function(){
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
			});
    </script>
{/literal}
