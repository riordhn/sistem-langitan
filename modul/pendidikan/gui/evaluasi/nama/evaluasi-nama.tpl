<div class="center_title_bar">Nama Status Evaluasi</div>
{if $smarty.get.mode == "tambah"}
	<form name="f2" id="f2" method="post" action="evaluasi-nama.php">
    	<table>
        	<tr>
            	<td>
                	Nama
                </td>
                <td><input type="text" name="nama" class="required" /></td>
            </tr>
            <tr>
            	<td>
                	Jenis
                </td>
                <td>
                	<select name="jenis">
                    	<option value="1">Rekomendasi</option>
                        <option value="2">Penetapan</option>
                	</select>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center">
                	<input type="submit" value="Simpan" />
                	<input type="hidden" name="mode" value="insert" />
                </td>
            </tr>
        </table>
    </form>
{elseif $smarty.get.mode == "edit"}
	<form name="f2" id="f2" method="post" action="evaluasi-nama.php">
    	<table>
        	<tr>
            	<td>
                	Nama
                </td>
                <td><input type="text" name="nama" class="required" value="{$evaluasi.NM_STATUS_EVALUASI}" /></td>
            </tr>
            <tr>
            	<td>
                	Jenis
                </td>
                <td>
                	<select name="jenis">
                    	<option value="1" {if $evaluasi.JENIS_STATUS_EVALUASI == 1}selected="selected"{/if}>Rekomendasi</option>
                        <option value="2" {if $evaluasi.JENIS_STATUS_EVALUASI == 2}selected="selected"{/if}>Penetapan</option>
                	</select>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center">
                	<input type="submit" value="Update" />
                	<input type="hidden" name="mode" value="update" />
                    <input type="hidden" name="id" value="{$smarty.get.id}" />
                </td>
            </tr>
        </table>
    </form>
{elseif $smarty.get.mode == "hapus"}
	<b>{$eror}</b><br />
	<a href="evaluasi-nama.php">Kembali</a>
{else}
<table>
	<tr>
    	<th>
        	No
        </th>
        <th>
        	Nama	
        </th>
        <th>
        	Jenis
        </th>
        <th>
        	Aksi
        </th>
    </tr>
    {$no++}
    {foreach $status_evaluasi as $data}
    <tr>
    	<td>{$no++}</td>
        <td>{$data.NM_STATUS_EVALUASI}</td>
        <td>{if $data.JENIS_STATUS_EVALUASI == 1}Rekomendasi{else}Penetapan{/if}</td>
        <td><!--<a href="evaluasi-nama.php?mode=edit&id={$data.ID_STATUS_EVALUASI}">Edit</a> | 
        	<a href="evaluasi-nama.php?mode=hapus&id={$data.ID_STATUS_EVALUASI}">Hapus</a></td>-->
    </tr>
    {/foreach}
    <tr>
    	<td colspan="4" style="text-align:center">
        	<input type="button" value="Tambah" onclick="location.href='#evaluasi-evaluasi-nama!evaluasi-nama.php?mode=tambah'"/>
        </td>
    </tr>
</table>
{/if}

{literal}
    <script>
			$("#f2").validate();
    </script>
{/literal}