<div class="center_title_bar">Lock Evaluasi</div>
<form action="evaluasi-lock.php" method="post" name="f2" id="f2">
	<table width="700">
    	<tr>
			<td>Semester</td>
            <td>
            	<select name="semester" class="required">
					<option value="">Pilih Semester</option>
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $id_semester==$data.ID_SEMESTER or $data.ID_SEMESTER==$semester_aktif} selected="selected" {/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($evaluasi)}
<form name="f1" method="post" action="evaluasi-lock.php">
	<table>
    	<tr>
        	<th>
            	No
            </th>
        	<th>
            	Fakultas
            </th>
            <th>
            	<input type="checkbox" id="cek_all" name="cek_all" />
            </th>
        </tr>
        {$no = 1}
        {foreach $evaluasi as $data}
        <tr>
        	<td>
            	{$no++}
            </td>
        	<td>
            	{$data.NM_FAKULTAS}
            </td>
            <td>
            	<input type="checkbox" id="cek" class="cek" name="cek{$no}" value="1" {if $data.STATUS_LOCK == 1} checked="checked"{/if} />
            </td>
        </tr>
        <input type="hidden" name="id_fakultas{$no}" value="{$data.ID_FAKULTAS}" />
        {/foreach}
        <tr>
        	<td colspan="3" style="text-align:center"><input type="submit" value="Simpan" />
           	 	<input type="hidden" name="semester" value="{$id_semester}" />
        		<input type="hidden" name="mode" value="insert" />
                <input type="hidden" name="no" value="{$no}" />
            </td>
        </tr>
    </table>
</form>
{/if}

{literal}
    <script>
			$("#f2").validate();
			
			$("#cek_all").click(function(){
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
			});
    </script>
{/literal}