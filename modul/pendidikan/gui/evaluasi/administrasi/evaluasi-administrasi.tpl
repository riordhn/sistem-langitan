<div class="center_title_bar">Evaluasi Administrasi</div>
<form name="form1" action="evaluasi-administrasi.php" method="post" id="f2">
	<table>
    <tr>
    	<td>Fakultas</td>
        <td>
    <select name="fakultas" id="fakultas">
    	<option value="">Semua</option>
    	{foreach $fakultas as $data}
    	<option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS == $smarty.request.fakultas}selected="selected"{/if}>{$data.NM_FAKULTAS}</option>
        {/foreach}
    </select>
    	</td>
        <td>Jenjang</td>
            <td>
                <select id="jenjang" name="jenjang">
                    <option value="">Semua</option>
                    {foreach $jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.request.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG|upper}</option>
                    {/foreach}
                </select>
        </td>
   </tr>
   
   <tr>
   		<td>Program Studi</td>
            <td>
                <select id="program_studi" name="program_studi">
                    <option value="">Semua</option>
					{foreach $prodi as $data}
						<option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.request.program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
					{/foreach}
                </select>
         </td>
		<td>Semester</td>
            <td>
            	<select name="semester" class="required">
					<option value="">Pilih Semester</option>
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $smarty.request.semester==$data.ID_SEMESTER or $data.ID_SEMESTER==$semester_aktif} selected="selected" {/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
    </tr>
    <tr>
    	<td colspan="10" style="text-align:center"><input type="submit" value="Tampil" /></td>
    </tr>
    </table>
</form>


{if isset($mhs)}
<form action="evaluasi-administrasi.php" name="f1" id="f1" method="post">
<table style="font-size:10px">
<tr>
	<th style="text-align:center">NO</th>
	<th style="text-align:center">NIM</th>
	<th style="text-align:center">NAMA</th>
	<th style="text-align:center">JENJANG</th>
	<th style="text-align:center">PRODI</th>
	<th style="text-align:center">STATUS AKADEMIK</th>
	<th style="text-align:center">STATUS KRS</th>
    <th style="text-align:center">PIUTANG SEMESTER</th>
	<th style="text-align:center">KETERANGAN</th>
	<th style="text-align:center">REKOMENDASI STATUS</th>
	<th style="text-align:center">DETAIL</th>
</tr>
{$no=1}
{foreach $mhs as $data}
	<tr {if $data.SUDAH_KRS == 1} bgcolor="#CCFFCC"{/if}>
		<td>{$no++}</td>
		<td>{$data.NIM_MHS}</td>
		<td>{$data.NM_PENGGUNA}</td>
		<td>{$data.NM_JENJANG}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
		<td>{$data.NM_STATUS_PENGGUNA}</td>
		<td>{if $data.SUDAH_KRS == 1}Sudah{else}Belum{/if}</td>
		<td>{$data.PIUTANG}</td>
		<td><textarea name="keterangan{$no}" rows="2">{$data.KETERANGAN}</textarea></td>
		<td>
					<input type="hidden" value="{$data.ID_MHS}" name="id_mhs{$no}" />
					<input type="hidden" value="" name="sks{$no}" />
					<input type="hidden" value="" name="ipk{$no}" />
					<input type="hidden" value="{$data.ID_STATUS_PENGGUNA}" name="status_terkini{$no}" />
			<select name="rekomendasi{$no}">
				<option value="">Pilih Status</option>
				<option value="11" {if $data.REKOMENDASI_STATUS==11}selected="selected"{/if}>Lanjut</option>
                <option value="5" {if $data.REKOMENDASI_STATUS==5}selected="selected"{/if}>Calon DO Administrasi</option>
			</select>
		</td>
	<td>    
        	<input type="button" value="Detail"  onclick="show_detail('{$data@index+1}','#detail','{$data.ID_MHS}','#detail2')"/>
        </td>
	</tr>
    <tr style="display:none" id="detail{$data@index+1}">
    	<td colspan="15">
        	<table style="width:100%" id="detail2{$data@index+1}">
               
                
            </table>
    	</td>
    </tr>

	</tr>
{/foreach}
	<tr>
		<td colspan="20" style="text-align:center">
			<input type="submit" value="Simpan" />
			<input type="button" value="Cetak" onclick="window.open('evaluasi-administrasi-cetak.php?sem={$id_semester}&fak={$id_fakultas}');"/>
			<input type="hidden" value="{$no}" name="no" />
			<input type="hidden" value="{$smarty.request.semester}" name="semester" />
			<input type="hidden" value="{$smarty.request.fakultas}" name="fakultas" />
			<input type="hidden" value="insert" name="mode" />
		</td>
	</tr>
</table>
</form>
{/if}

{literal}
    <script>
	
	$("#f2").validate();
    
	$(".datepicker").datepicker({dateFormat:'dd-mm-yy',changeMonth: true,changeYear: true});
			
	function show_detail(index,tag,id_mhs,tag2){
		$.ajax({
		type:'get',
		url:'getPembayaran.php',
		data:'id_mhs='+id_mhs,
		success:function(data){
			$(tag2+index).html(data);
		}                    
		})
		
		$(tag+index).fadeToggle("slow");

	}
			
	 $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });

    </script>
{/literal}
