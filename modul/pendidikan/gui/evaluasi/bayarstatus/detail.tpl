<div class="center_title_bar">Admisi Mahasiswa</div>

<table>
    <tr>
        <td>NIM / Nama</td>
        <td><form action="admisi-mhs.php" method="get" id="fcari">
            	<input name="nim" type="text" value="" size="30" class="required" />
                <input type="hidden" value="cari" name="mode" />
          		<input type="submit" value="Cari" />
            </form>
        </td>
    </tr>
</table>

{if $error <> ''}
	<h2 style="color:#F00">{$error}</h2>    
{else}
<table style="width: 50%" class="detail">
	<tr>
            <th colspan="2" style="text-align: center;">Biodata</th>
        </tr>
    <tr>
		<td>NIM</td>
        <td>{$mhs[0]['NIM_MHS']}</td>
    </tr>
    <tr>
		<td>Nama</td>
        <td>{$mhs[0]['NM_PENGGUNA']}</td>
    </tr>
    <tr>
		<td>Prodi</td>
        <td>{$mhs[0]['NM_JENJANG']} - {$mhs[0]['NM_PROGRAM_STUDI']}</td>
    </tr>
</table>
<table style="width: 100%">
    <tr>
        <th>Semester</th>
        <th>Status</th>
        <th>No SK</th>
        <th>Tgl SK</th>
        <th>Keterangan</th>
    </tr>
    
    {foreach $mhs as $a}
    <tr>
        <td>{$a.THN_AKADEMIK_SEMESTER} / {$a.NM_SEMESTER}</td>
        <td>{$a.NM_STATUS_PENGGUNA}</td>
        <td>{$a.NO_SK}</td>
        <td>{$a.TGL_SK}</td>
        <td>{$a.KETERANGAN}</td>
    </tr>
    {/foreach}
</table>
{/if}
<script type="text/javascript">
$().ready(function() {
		$("#fcari").validate();
	});
</script>