<div class="center_title_bar">Riwayat Bayar dan Status Mahasiswa</div>

<table>
    <tr>
        <td>NIM</td>
        <td><form action="evaluasi-bayar-status.php" method="get" id="fcari">
            	<input name="nim" type="text" value="" size="30" class="required" />
                <input type="hidden" value="cari" name="mode" />
          		<input type="submit" value="Cari" />
            </form>
        </td>
    </tr>
</table>
    <table class="detail">
        <tr>
            <th colspan="2" style="text-align: center;">Biodata</th>
        </tr>
        <tr>
            <td>NIM</td>
            <td>{$admisi[0]['NIM_MHS']}</td>            
        </tr>
        <tr>
            <td>Nama</td>
            <td>{$admisi[0]['NM_PENGGUNA']}</td>            
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>{$admisi[0]['NM_PROGRAM_STUDI']}</td>            
        </tr>
        <tr>
            <td>Jenjang</td>
            <td>{$admisi[0]['NM_JENJANG']}</td>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>{$admisi[0]['NM_FAKULTAS']}</td>            
        </tr>
		<tr>
            <td>Status Cekal Pembayaran</td>
            <td>{if $admisi[0]['STATUS_CEKAL'] == 1}CEKAL{else}TIDAK CEKAL{/if}</td>            
        </tr>
		<tr>
            <td>Status Akademik</td>
            <td>{$status}</td>            
        </tr>
    </table>
	<div class="center_title_bar">Database Baru</div>
	<h2 style="color:#000099">Riwayat Status Mahasiswa</h2>
	  <table cellpadding=3 cellspacing=0 border=1 align=center>
	<tr>
		<th align=center><B>No</B></th>
		<th align=center><B>Semester</B></th>
		<th align=center><B>Status</B></th>
		<th align=center><B>No. SK</B></th>
		<th align=center><B>Tgl. SK</B></th>
		<th align=center><B>Keterangan</B></th>
		<th align=center><B>Waktu</B></th>
		<th align=center><B>Tgl Lulus</B></th>
		<th align=center><B>No Ijasah</B></th>
	</tr>
	{$no=1}
	{foreach $admisi as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.THN_AKADEMIK_SEMESTER}/{$data.NM_SEMESTER}</td>
			<td>{$data.NM_STATUS_PENGGUNA}</td>
			<td>{$data.NO_SK}</td>
			<td>{$data.TGL_SK}</td>
			<td>{$data.ALASAN}</td>
			<td>{if $data.KURUN_WAKTU <> ''}{$data.KURUN_WAKTU} Semester{/if}</td>
			<td>{$data.TGL_LULUS}</td>
			<td>{$data.NO_IJASAH}</td>
		</tr>
	{/foreach}
	</table>
	
	<h2 style="color:#000099">Riwayat Pembayaran Mahasiswa</h2>
	{if isset($pembayaran_mhs)}
		<table>
			<tr>
				<th>Tahun Ajaran</th>
				<th>Tgl Bayar</th>
				<th>Bank</th>
				<th>Via</th>
				<th>Status Tagihan</th>
				<th>Status Pembayaran</th>
				<th>Keterangan</th>
				<th>Besar Biaya</th>
				<th>Denda</th>
			</tr>
			{foreach $pembayaran_mhs as $data}
			
			<tr {if $data.ID_STATUS_PEMBAYARAN == 2} bgcolor="#CCFF66" {/if}>
				<td>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</td>
				<td>{$data.TGL_BAYAR}</td>
				<td>{$data.NM_BANK}</td>
				<td>{$data.NAMA_BANK_VIA}</td>
				<td style="text-align:center">{$data.IS_TAGIH}</td>
				<td>{$data.NAMA_STATUS}</td>
				<td>{$data.KETERANGAN}</td>
				<td style="text-align:right">{$data.BESAR_BIAYA|number_format:0:",":"."}</td>
				<td style="text-align:right">{$data.DENDA_BIAYA|number_format:0:",":"."}</td>
			</tr>
			{/foreach}
		</table>
<!--	    <table class="detail">
        <tr>
            <th colspan="10" style="text-align: center;">Status Pembayaran</th>
        </tr>
        <tr>
            <th>Nama Biaya</th>
            <th>Besar Biaya</th>
            <th>Denda Biaya</th>
            <th>Status Tagihan Biaya</th>
            <th>Tanggal Bayar</th>
            <th>Nama Bank</th>
            <th>Via Bank</th>
            <th>Nomer Transaksi</th>
            <th>Keterangan</th>
            <th>Semester Pembayaran</th>
        </tr>
        {foreach $data_pembayaran as $data}
            {if $data['NM_BIAYA']}
                <tr>
                    <td>{$data['NM_BIAYA']}</td>
                    <td>{$data['BESAR_BIAYA']}</td>
                    <td>{$data['DENDA_BIAYA']}</td>
                    <td>{$data['IS_TAGIH']}</td>
                    <td>{$data['TGL_BAYAR']}</td>
                    <td>{$data['NM_BANK']}</td>
                    <td>{$data['NAMA_BANK_VIA']}</td>
                    <td>{$data['NO_TRANSAKSI']}</td>
                    <td>{$data['KETERANGAN']}</td>
                    <td>{$data['NM_SEMESTER']} ({$data['TAHUN_AJARAN']}) </td>
                </tr>
          {else}
                <tr>
                    <td colspan="10" style="text-align: center;">Data Pembayaran Kosong</td>
                </tr>
            {/if}

        {/foreach} 
        <tr>
            <td colspan="10" style="text-align: center;">
                <p>
                    <span style="color: #007000;font-weight: bold;">TOTAL BIAYA </span><br/><strong>{$data_pembayaran[0]['TOTAL_SPP']+$data_pembayaran[0]['TOTAL_DENDA']}</strong><br/>
                    <span style="color: #ff3300;font-weight: bold;">DAFTAR TAGIHAN</span><br/>Biaya Kuliah : <strong>{$data_pembayaran[0]['TOTAL_SPP_TAGIH']}</strong> + Denda Biaya :<strong> {$data_pembayaran[0]['TOTAL_DENDA']}</strong><br/>
                    <strong>Total Biaya : {$data_pembayaran[0]['TOTAL_SPP_TAGIH']+$data_pembayaran[0]['TOTAL_DENDA_BIAYA']}</strong>
                </p>
            </td>
        </tr>--> 
    </table>
	{else}
    <table class="detail">
        <tr>
            <th colspan="10" style="text-align: center;">Status Pembayaran</th>
        </tr>
        <tr>
            <th>Nama Biaya</th>
            <th>Besar Biaya</th>
            <th>Status Tagihan Biaya</th>
            <th>Tanggal Bayar</th>
            <th>Nama Bank</th>
            <th>Via Bank</th>
            <th>Nomer Transaksi</th>
        </tr>
        {foreach $data_pembayaran as $data}
            {if $data['NM_BIAYA']}
                <tr>
                    <td>{$data['NM_BIAYA']}</td>
                    <td style="text-align: right">{number_format($data['BESAR_BIAYA'])}</td>
                    <td>{$data['IS_TAGIH']}</td>
                    <td>{$data['TGL_BAYAR']}</td>
                    <td>{$data['NM_BANK']}</td>
                    <td>{$data['NAMA_BANK_VIA']}</td>
                    <td>{$data['NO_TRANSAKSI']}</td>
                </tr>
            {else}
                <tr>
                    <td colspan="7" style="text-align: center;">Data Pembayaran Kosong</td>
                </tr>
            {/if}

        {/foreach}
        <tr>
            <td colspan="10" style="text-align: center;">
                <p>
                    <span style="color: #007000;font-weight: bold;">TOTAL BIAYA </span><br/><strong>{number_format($data_pembayaran[0]['TOTAL_BIAYA'])}</strong><br/>
                    <span style="color: #ff3300;font-weight: bold;">DAFTAR TAGIHAN</span><br/>Biaya Pendaftaran : <strong>{if $data_pembayaran[0]['TOTAL_BIAYA_TAGIH']==null}0{else}{number_format($data_pembayaran[0]['TOTAL_BIAYA_TAGIH'])}{/if}</strong>
                </p>
            </td>
        </tr>
    </table>
	{/if}
	<br /><br />
<br />
	<div class="center_title_bar">Database Lama</div>
	<h2 style="color:#000099">Riwayat Status Mahasiswa</h2> 
	<table cellpadding=3 cellspacing=0 border=1 align=center>
	<tr>
		<th align=center><B>No</B></th>
		<th align=center><B>Thn Semes</B></th>
		<th align=center><B>Status Lama</B></th>
		<th align=center><B>Status Baru</B></th>
		<th align=center><B>Waktu Ubah Status</B></th>
		<th align=center><B>Operator Perubah</B></th>
		<th align=center><B>SK</B></th>
		<th align=center><B>Tgl SK</B></th>
		<th align=center><B>Keterangan</B></th>
	</tr>
		{$baris}
	</table>
		
		
	<h2 style="color:#000099">Riwayat Pembayaran Mahasiswa</h2> 
	<table bgcolor="#ffffff" cellspacing="1" cellpadding="1" border="0" width="760" style="font-size:9px">
	<tr bgcolor="Silver">
		<td width="80">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Thn akademik</STRONG></FONT></P></td>
		<td width="80">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Lunas</STRONG></FONT></P></td>
		<td width="72">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Bank</STRONG></FONT></P></td>
		<td width="65">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Jns Ringan</STRONG></FONT></P></td>
		<td width="72">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Tgl bayar</STRONG></FONT></P></td>
		<td width="72">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>SPP</STRONG></FONT></P></td>
		<td width="72">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Prakt.</STRONG></FONT></P></td>
		<td width="72">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Asuransi</STRONG></FONT></P></td>
		<td width="52">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>SP3</STRONG></FONT></P></td>
		<td width="52">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>PKL</STRONG></FONT></P></td>
		<td width="52">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Registr.</STRONG></FONT></P></td>
		<td width="60">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Denda</STRONG></FONT></P></td>
		<td width="60">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Piutang</STRONG></FONT></P></td>
		<td width="60">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Matrikulasi</STRONG></FONT></P></td>
		<td width="60">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>KKN</STRONG></FONT></P></td>
		<td width="60">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Wisuda</STRONG></FONT></P></td>
		<td width="60">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Pembinaan</STRONG></FONT></P></td>
		<td width="60">
			<P align=center><FONT face=Arial color=blue size=1><STRONG>Ikoma</STRONG></FONT></P></td>
	</tr>
	{$tampil_pembayaran}
	</table>
<script type="text/javascript">
$().ready(function() {
		$("#fcari").validate();
	});
</script>