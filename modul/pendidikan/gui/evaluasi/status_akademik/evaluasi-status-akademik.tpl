<div class="center_title_bar">Status Akademik Setelah Evaluasi</div>
<form action="evaluasi-status-akademik.php" method="post" name="f2" id="f2">
	<table width="700">
    <tr>
    	<td>Fakultas</td>
        <td>
    <select name="fakultas" id="fakultas">
    	<option value="">Semua</option>
    	{foreach $fakultas as $data}
    	<option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS == $smarty.request.fakultas}selected="selected"{/if}>{$data.NM_FAKULTAS}</option>
        {/foreach}
    </select>
    	</td>
        <td>Jenjang</td>
            <td>
                <select id="jenjang" name="jenjang">
                    <option value="">Semua</option>
                    {foreach $jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.request.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG|upper}</option>
                    {/foreach}
                </select>
        </td>
   </tr>
   <tr>
   		<td>Program Studi</td>
            <td>
                <select id="prodi" name="prodi">
                    <option value="">Semua</option>
					{foreach $prodi as $data}
						<option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.request.prodi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
					{/foreach}
                </select>
         </td>
		<td>Semester</td>
            <td>
            	<select name="semester" class="required">
					<option value="">Pilih Semester</option>
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $smarty.request.semester==$data.ID_SEMESTER or $data.ID_SEMESTER==$semester_aktif} selected="selected" {/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
    </tr>
    <tr>
    	<td colspan="10" style="text-align:center"><input type="submit" value="Tampil" /></td>
    </tr>
    </table>
</form>



{if isset($evaluasi)}


{literal}
	<style>
    .floatingHeader {
      position: fixed;
      top: 0;
      visibility: hidden;
    }
	</style>
    
 <script>
    function UpdateTableHeaders() {
       $(".persist-area").each(function() {
       
           var el             = $(this),
               offset         = el.offset(),
               scrollTop      = $(window).scrollTop(),
               floatingHeader = $(".floatingHeader", this)
           
           if ((scrollTop > offset.top) && (scrollTop < offset.top + el.height())) {
               floatingHeader.css({
                "visibility": "visible"
               });
           } else {
               floatingHeader.css({
                "visibility": "hidden"
               });      
           };
       });
    }
    
    // DOM Ready      
    $(function() {
    
      var floatingHeader;

	$(".persist-area").each(function(){

		floatingHeader = $(".persist-header", this);
		
		floatingHeader.before(floatingHeader.clone());
	
		floatingHeader.children().css("width", function(i, val){
			return $(floatingHeader).children().eq(i).css("width", val);
		});
	
		floatingHeader.addClass("floatingHeader");
		
	});
       
       $(window)
        .scroll(UpdateTableHeaders)
        .trigger("scroll");
       
    });
  </script>
{/literal}

<div id="tableContainer" class="tableContainer">
<form action="evaluasi-status-akademik.php" name="f1" id="f1" method="post">
<table style="font-size:11px"  >
<thead  class="persist-header">
	<tr>
		<th style="text-align:center">NO</th>
		<th style="text-align:center">NIM</th>
		<th style="text-align:center">NAMA</th>
        <th style="text-align:center">JENJANG</th>
		<th style="text-align:center">PRODI</th>
		<th style="text-align:center">STATUS TERKINI</th>
        <th style="text-align:center">HASIL PENETAPAN</th>
		<th style="text-align:center">UBAH STATUS AKADMEIK</th>
        <th style="text-align:center">KETERANGAN</th>
        <th style="text-align:center">NO SK</th>
        <th style="text-align:center">TGL SK</th>
        
	</tr>
</thead>
<tbody class="scrollContent">
	{$no = 1}
	{foreach $evaluasi as $data}
	<tr {if $data.PENGAJUAN_EVALUASI == 1}bgcolor="#E0EBFF"{/if}>
		<td>{$no++}</td>
		<td>{$data.NIM_MHS}</td>
		<td>{$data.NM_PENGGUNA}</td>
        <td>{$data.NM_JENJANG}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
		<td>{$data.STATUS_TERKINI}</td>
        <td>
			{$data.PENETAPAN}
        </td>
		<td>
					<input type="hidden" value="{$data.ID_MHS}" name="id_mhs{$no}" />
        	<select name="penetapan{$no}">
				<option value="">Pilih Status</option>
				{foreach $status_akademik as $x}
					<option value="{$x.ID_STATUS_PENGGUNA}" {if $data.ID_STATUS_PENGGUNA==$x.ID_STATUS_PENGGUNA}selected="selected"{/if} {if $data.ID_STATUS_PENGGUNA == 4} disabled="disabled"{/if}>{$x.NM_STATUS_PENGGUNA}</option>
				{/foreach}
			</select>
        </td>
        <td><textarea name="keterangan{$no}" rows="2">{$data.ALASAN}</textarea></td>
        <td><input type="text" value="{$data.NO_SK_ADMISI}" name="sk{$no}" /></td>
        <td><input name="tgl_sk{$no}" type="text" class="datepicker" size="8"  value="{$data.TGL_SK_ADMISI}"/></td>
        
    </tr>
	{/foreach}
	<tr>
		<td colspan="25" style="text-align:center">
			<input type="submit" value="Simpan" />
			<input type="hidden" value="{$no}" name="no" />
			<input type="hidden" value="{$smarty.request.fakultas}" name="fakultas" />
			<input type="hidden" value="{$smarty.request.jenjang}" name="jenjang" />
			<input type="hidden" value="{$smarty.request.prodi}" name="prodi" />
			<input type="hidden" value="{$smarty.request.semester}" name="semester" />
			<input type="hidden" value="insert" name="mode" />
		</td>
	</tr>
    </tbody>
</table>
</form>
</div>
{/if}

{literal}
    <script>
			$("#f2").validate();
    
	$(".datepicker").datepicker({dateFormat:'dd-mm-yy',changeMonth: true,changeYear: true});
			
	 $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#prodi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#prodi').html(data);
                }                    
            })
        });
    </script>
{/literal}