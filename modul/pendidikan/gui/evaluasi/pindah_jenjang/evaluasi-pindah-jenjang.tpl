<div class="center_title_bar">Mahasiswa Evaluasi Yang Pindah Jenjang</div>
<form action="evaluasi-pindah-jenjang.php" method="post" name="f2" id="f2">
	<table width="700">
    	<tr>
			<td>Semester</td>
            <td>
            	<select name="semester" class="required">
					<option value="">Pilih Semester</option>
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $smarty.post.semester==$data.ID_SEMESTER or $data.ID_SEMESTER==$semester_aktif} selected="selected" {/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($evaluasi)}

<table>
	<tr>
    	<th>No</th>
        <th>Nama</th>
        <th>NIM</th>
        <th>NIM Baru</th>
        <th>Evaluasi</th>
    </tr>
    {$no = 1}
    {foreach $evaluasi as $data}
    		<tr>
            	<td>{$no++}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NIM_BARU}</td>
                <td>{$data.NM_STATUS_EVALUASI}</td>
            </tr>
    {/foreach}
</table>

{/if}


{literal}
    <script>
			$("#f2").validate();
    </script>
{/literal}