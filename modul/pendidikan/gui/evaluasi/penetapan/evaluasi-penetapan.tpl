<div class="center_title_bar">Penetapan Evaluasi</div>
<form action="evaluasi-penetapan.php" method="post" name="f2" id="f2">
	<table width="700">
    	<tr>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas"  class="required">
					<option value="">Pilih Fakultas</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $smarty.request.fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
            <td>Jenjang</td>
            <td>
            	<select name="jenjang" >
					<option value="all">Semua</option>
                	{foreach $jenjang as $data}
                	<option value="{$data.ID_JENJANG}" {if $smarty.request.jenjang==$data.ID_JENJANG} selected="selected" {/if}>{$data.NM_JENJANG}</option>
					{/foreach}
            	</select>
            </td>
            <td>Semester</td>
            <td>
            	<select name="semester" class="required">
                	<option value="">Pilih Semester</option>
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $smarty.request.semester==$data.ID_SEMESTER or $data.ID_SEMESTER==$semester_aktif} selected="selected" {/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
	    <td>Status</td>
            <td>
            	<select name="status_penetapan">
                	<option value="">Semua Status</option>
                	{foreach $status_penetapan as $data}
                	<option value="{$data.ID_STATUS_EVALUASI}" {if $smarty.request.status_penetapan==$data.ID_STATUS_EVALUASI} selected="selected" {/if}>{$data.NM_STATUS_EVALUASI}</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="8" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($evaluasi)}


{literal}
	<style>
    .floatingHeader {
      position: fixed;
      top: 0;
      visibility: hidden;
    }
	</style>
    
 <script>
    function UpdateTableHeaders() {
       $(".persist-area").each(function() {
       
           var el             = $(this),
               offset         = el.offset(),
               scrollTop      = $(window).scrollTop(),
               floatingHeader = $(".floatingHeader", this)
           
           if ((scrollTop > offset.top) && (scrollTop < offset.top + el.height())) {
               floatingHeader.css({
                "visibility": "visible"
               });
           } else {
               floatingHeader.css({
                "visibility": "hidden"
               });      
           };
       });
    }
    
    // DOM Ready      
    $(function() {
    
      var floatingHeader;

	$(".persist-area").each(function(){

		floatingHeader = $(".persist-header", this);
		
		floatingHeader.before(floatingHeader.clone());
	
		floatingHeader.children().css("width", function(i, val){
			return $(floatingHeader).children().eq(i).css("width", val);
		});
	
		floatingHeader.addClass("floatingHeader");
		
	});
       
       $(window)
        .scroll(UpdateTableHeaders)
        .trigger("scroll");
       
    });
	

  </script>
{/literal}

<div id="tableContainer" class="tableContainer">
<form action="evaluasi-penetapan.php" name="f1" id="f1" method="post">
<table style="font-size:11px"  >
<thead  class="persist-header">
	<tr>
		<th style="text-align:center">NO</th>
		<th style="text-align:center">NIM</th>
		<th style="text-align:center">NAMA</th>
        <th style="text-align:center">JENJANG</th>
		<th style="text-align:center">PRODI</th>
		<th style="text-align:center">STATUS TERKINI</th>
   		<!--<th style="text-align:center">SKS WAJIB</th>-->
		<th style="text-align:center">SKS DIPEROLEH</th>
		<th style="text-align:center">IPK</th>
        <th style="text-align:center">SKS Dengsn IPK 2</th>
		<th style="text-align:center">% NILAI D</th>
		<th style="text-align:center">PIUTANG SEMESTER</th>
		<th style="text-align:center">CUTI SEMESTER</th>
		<th style="text-align:center">SEMESTER MASUK MHS</th>
		<th style="text-align:center">EVALUASI BWS</th>
        <th style="text-align:center">KETERANGAN BWS</th>
        <th style="text-align:center">EVALUASI AKADEMIK</th>
        <th style="text-align:center">KETERANGAN AKADEMIK</th>
        <th style="text-align:center">EVALUASI ADMINISTRASI</th>
        <th style="text-align:center">KETERANGAN ADMINISTRASI</th>
        <th style="text-align:center">EVALUASI GASAL</th>
        <th style="text-align:center">KETERANGAN GASAL</th>
		<th style="text-align:center">NIM</th>
		<th style="text-align:center">NAMA</th>
        <th style="text-align:center">HASIL PENETAPAN</th>
        <th style="text-align:center">KETERANGAN</th>
        <th style="text-align:center">NO SK</th>
        <th style="text-align:center">TGL SK</th>
        
	</tr>
</thead>
<tbody class="scrollContent">
	{$no = 1}
	{foreach $evaluasi as $data}
		
		
		{if $data.REKOMENDASI_ADMINISTRASI != '' and $data.REKOMENDASI_STATUS_BWS == '' and $data.REKOMENDASI_STATUS_AKADEMIK == '' and $data.REKOMENDASI_GASAL == ''}
			{$bgcolor = '#FF99CC'}
		{else}
			{$bgcolor = ''}
		{/if}
		
		
	<tr {if $bgcolor != ''} bgcolor="#FF99CC" {else if $no % 2 != 1} bgcolor="#E0EBFF" {/if} >
		<td>{$no++}</td>
		<td><a href="evaluasi-status.php?nim={$data.NIM_MHS}" target="_blank">{$data.NIM_MHS}</a></td>
		<td>{$data.NM_PENGGUNA}</td>
        <td>{$data.NM_JENJANG}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
		<td>{$data.STATUS_TERKINI}</td>
		<!--<td>{$data.SKS_WAJIB}</td>-->
        <td>{$data.SKS}</td>
		<td>{$data.IPK}</td>
        <td>{$data.SKS_IPK_2}</td>
		<td>{$data.PERSEN_NILAI_D}</td>
		<td>{$data.PIUTANG}</td>
		<td>{$data.CUTI}</td>
		<td>{$data.NM_SEMESTER}</td>
		<td>{$data.REKOMENDASI_STATUS_BWS}</td>
        <td>{$data.KETERANGAN_BWS}</td>
        <td>{$data.REKOMENDASI_STATUS_AKADEMIK}</td>
        <td>{$data.KETERANGAN_AKADEMIK}</td>
        <td>{$data.REKOMENDASI_ADMINISTRASI}</td>
        <td>{$data.KETERANGAN_ADMINISTRASI}</td>
        <td>{$data.REKOMENDASI_GASAL}</td>
        <td>{$data.KETERANGAN_GASAL}</td>
        <td><a href="evaluasi-status.php?nim={$data.NIM_MHS}" target="_blank">{$data.NIM_MHS}</a></td>
		<td>{$data.NM_PENGGUNA}</td>
		
        <td>
					<input type="hidden" value="{$data.ID_MHS}" name="id_mhs{$no}" />
					<input type="hidden" value="{$data.SKS}" name="sks{$no}" />
					<input type="hidden" value="{$data.IPK}" name="ipk{$no}" />
                    <input type="hidden" value="{$data.PIUTANG}" name="piutang{$no}" />
					<input type="hidden" value="{$data.ID_STATUS_PENGGUNA}" name="status_terkini{$no}" />
        	<select name="penetapan{$no}">
				<option value="">Pilih Status</option>
				{foreach $status_penetapan as $x}
					<option value="{$x.ID_STATUS_EVALUASI}" {if $data.PENETAPAN_STATUS==$x.ID_STATUS_EVALUASI}selected="selected"{/if}>{$x.NM_STATUS_EVALUASI}</option>
				{/foreach}
			</select>
        </td>
        <td><textarea name="keterangan{$no}" rows="2">{$data.KETERANGAN_PENGAJUAN}</textarea></td>
        <td><input type="text" value="{$data.NO_SK_EVALUASI}" name="sk{$no}" /></td>
        <td><input name="tgl_sk{$no}" type="text" class="datepicker" size="8"  value="{$data.TGL_SK_EVALUASI}"/></td>
        
    </tr>
	{/foreach}
	<tr>
		<td colspan="30" style="text-align:center">
			<input type="submit" value="Simpan" />
			<input type="button" value="Cetak" onclick="window.open('evaluasi-penetapan-cetak.php?sem={$smarty.request.semester}&fak={$smarty.request.fakultas}&jenjang={$smarty.request.jenjang}');"/>
			<input type="button" value="Excel" onclick="get_link_report('excel', '{$smarty.request.semester}', '{$smarty.request.fakultas}', '{$smarty.request.jenjang}');"/>
			<input type="hidden" value="{$no}" name="no" />
			<input type="hidden" value="{$smarty.request.fakultas}" name="fakultas" />
			<input type="hidden" value="{$smarty.request.semester}" name="semester" />
            <input type="hidden" value="{$smarty.request.jenjang}" name="jenjang" />
			<input type="hidden" value="insert" name="mode" />
		</td>
	</tr>
    </tbody>
</table>
</form>
</div>
{/if}

{literal}
    <script>
			$("#f2").validate();
            $(".datepicker").datepicker({dateFormat:'dd-mm-yy',changeMonth: true,changeYear: true});
			
			function get_link_report(type, sem, fak, jenjang){
            	link=document.location.hash.replace('#evaluasi-evaluasi_penetapan!', '');
                window.location.href='excel-'+link+'?fak='+fak+'&sem='+sem+'&jenjang='+jenjang;
        	}	
    </script>
{/literal}
