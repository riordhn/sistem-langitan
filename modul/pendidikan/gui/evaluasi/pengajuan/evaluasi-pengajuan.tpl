<div class="center_title_bar">Pengajuan Evaluasi</div>
<form action="evaluasi-pengajuan.php" method="post" name="f2" id="f2">
	<table width="700">
    	<tr>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas">
					<option value="">Semua Fakultas</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $smarty.request.fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
            <td>Semester</td>
            <td>
            	<select name="semester" class="required">
                	<option value="">Pilih Semester</option>
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $smarty.request.semester==$data.ID_SEMESTER or $data.ID_SEMESTER==$semester_aktif} selected="selected" {/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($evaluasi)}
<form action="evaluasi-pengajuan.php" name="f1" id="f1" method="post">
<table style="font-size:11px">
	<tr>
		<th style="text-align:center">NO</th>
		<th style="text-align:center">NIM</th>
		<th style="text-align:center">NAMA</th>
        <th style="text-align:center">JENJANG</th>
		<th style="text-align:center">PRODI</th>
		<th style="text-align:center">STATUS TERKINI</th>
		<th style="text-align:center">EVALUASI BWS</th>
        <th style="text-align:center">EVALUASI AKADEMIK</th>
        <th style="text-align:center">EVALUASI ADMINISTRASI</th>
        <th style="text-align:center">HASIL PENETAPAN</th>
       	<th style="text-align:center">
        	 <input type="checkbox" id="cek_all" name="cek_all" />
        </th>
		<th style="text-align:center">UBAH HASIL PENETAPAN</th>
		<th style="text-align:center">KETERANGAN</th>
        
	</tr>
	{$no = 1}
	{foreach $evaluasi as $data}
	<tr {if $data.PENGAJUAN_EVALUASI == 1}bgcolor="#E0EBFF"{/if}>
		<td>{$no++}</td>
		<td><a href="evaluasi-status-mhs.php?nim={$data.NIM_MHS}" target="_blank">{$data.NIM_MHS}</a></td>
		<td>{$data.NM_PENGGUNA}</td>
        <td>{$data.NM_JENJANG}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
		<td>{$data.STATUS_TERKINI}</td>
		<td>{$data.REKOMENDASI_STATUS_BWS}</td>
        <td>{$data.REKOMENDASI_STATUS_AKADEMIK}</td>
        <td>{$data.REKOMENDASI_ADMINISTRASI}</td>
        <td>
			{$data.PENETAPAN}

        </td>
        <td>
        	<input type="checkbox" id="cek" class="cek" name="cek{$no}" value="1" {if $data.PENGAJUAN_EVALUASI == 1}checked="checked"{/if} />
        </td>
		<td>
        	<select name="penetapan{$no}">
				<option value="">Pilih Status</option>
				{foreach $status_penetapan as $x}
					<option value="{$x.ID_STATUS_EVALUASI}" {if $data.PENETAPAN_STATUS==$x.ID_STATUS_EVALUASI}selected="selected"{/if}>{$x.NM_STATUS_EVALUASI}</option>
				{/foreach}
			</select>
        </td>
		<td><textarea name="keterangan{$no}" rows="2">{$data.KETERANGAN}</textarea></td>
        <input type="hidden" value="{$data.ID_MHS}" name="id_mhs{$no}" />
    </tr>
	{/foreach}
	<tr>
		<td colspan="25" style="text-align:center">
			<input type="submit" value="Simpan" />
			<input type="hidden" value="{$no}" name="no" />
			<input type="hidden" value="{$smarty.request.fakultas}" name="fakultas" />
			<input type="hidden" value="{$smarty.request.semester}" name="semester" />
			<input type="hidden" value="insert" name="mode" />
		</td>
	</tr>
</table>
</form>
{/if}

{literal}
    <script>
			$("#f2").validate();
            $(".datepicker").datepicker({dateFormat:'dd-mm-yy',changeMonth: true,changeYear: true});
			$("#cek_all").click(function(){
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
			});
    </script>
{/literal}
