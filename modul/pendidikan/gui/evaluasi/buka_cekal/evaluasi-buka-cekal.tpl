<div class="center_title_bar">Buka Cekal Hasil Penetapan Evaluasi</div>
	<form name="f2" id="f2" method="post" action="evaluasi-buka-cekal.php">
    	<table>
        	<td>Fakultas</td>
            <td>
            	<select name="fakultas">
					<option value="">Semua</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $smarty.post.fakultas==$data.ID_FAKULTAS}selected="selected"{/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
            <tr>
            	<td>
                	Status Penetapan
                </td>
                <td>
                	<select name="status_evaluasi">
                    	<option value="">Semua</option>
                    	{foreach $status_evaluasi as $data}
                        <option value="{$data.ID_STATUS_EVALUASI}" {if $smarty.post.status_evaluasi==$data.ID_STATUS_EVALUASI}selected="selected"{/if}>{$data.NM_STATUS_EVALUASI}</option>
                        {/foreach}
                	</select>
                </td>
            </tr>
            <td>Semester</td>
            <td>
            	<select name="semester" class="required">
                	<option value="">Pilih Semester</option>
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $smarty.post.semester==$data.ID_SEMESTER} selected="selected" {/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
            <tr>
                <td colspan="2" style="text-align:center">
                	<input type="submit" value="Tampil" />
                </td>
            </tr>
        </table>
    </form>
    
{if isset($evaluasi)}
<form name="f1" method="post" action="evaluasi-buka-cekal.php">
	<table style="width:900;">
    	<tr>
        	<th width="30">No</th>
            <th width="70">NIM</th>
            <th width="200">Nama</th>
            <th width="100">Fakultas</th>
            <th width="50">Jenjang</th>
            <th width="150">Program Studi</th>
            <th width="100">Hasil Penetapan</th>
            <th width="100"><input type="checkbox" id="cek_all" name="cek_all" /></th>
        </tr>
        {$no = 1}
        {foreach $evaluasi as $data}
        <tr>
        	<td>{$no++}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_PENGGUNA}</td>
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_JENJANG}</td>
            <td>{$data.NM_PROGRAM_STUDI}</td>
            <td>{$data.NM_STATUS_EVALUASI}</td>
            <td>{if $data.STATUS_CEKAL == 1}Cekal{else}Tidak Cekal{/if}
            	<input type="checkbox" id="cek" class="cek" name="cek{$no}" value="0" {if $data.STATUS_CEKAL == 0}checked="checked"{/if} />
            	<input type="hidden" value="{$data.ID_MHS}" name="id_mhs{$no}" />
            </td>
        </tr>
        {/foreach}
        <tr>
        	<td colspan="8" style="text-align:center">
            	<input type="submit" value="Simpan" />
                <input type="hidden" name="fakultas" value="{$smarty.post.fakultas}" />
                <input type="hidden" name="semester" value="{$smarty.post.semester}" />
                <input type="hidden" name="status_evaluasi" value="{$smarty.post.status_evaluasi}" />
                <input type="hidden" name="mode" value="update" />
                <input type="hidden" name="no" value="{$no}" />
            </td>
        </tr>
    </table>
</form>
{/if}
    
{literal}
    <script>
			$("#f2").validate();
			
			$("#cek_all").click(function(){
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
			});
    </script>
{/literal}