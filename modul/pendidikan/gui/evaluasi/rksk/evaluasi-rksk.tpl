<div class="center_title_bar">REKAP SK</div>  
<form action="evaluasi-rekap-sk.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode SK</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.THN}" {if $id_periode==$data.THN} selected="selected" {/if}>{$data.THN}</option>
					{/foreach}
            	</select>
            </td>
			<td>Jenis SK</td>
            <td>
            	<select name="jenis">
					<option value="">Semua</option>
                	{foreach $jenis as $data}
                	<option value="{$data.ID_STAT}" {if $id_jenis==$data.ID_STAT} selected="selected" {/if}>{$data.NM_STATUS_PENGGUNA}</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($rekap_detail)}
<table>
		<tr>
			<th>No</th>
			<th>NIM</th>
			<th>Nama</th>
			<th>Fakultas</th>
			<th>Jenjang</th>
			<th>Prodi</th>
			<th>Nomor SK</th>
			<th>Tanggal SK</th>
			{if $id_jenis == ''}
			<th>Jenis SK</th>
			{/if}
		</tr>
		{$no=1}
		{foreach $rekap_detail as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.NIM_MHS}</td>
			<td>{$data.NM_PENGGUNA}</td>
			<td>{$data.NM_FAKULTAS}</td>
			<td>{$data.NM_JENJANG}</td>
			<td>{$data.NM_PROGRAM_STUDI}</td>
			<td>{$data.NO_SK}</td>
			<td>{$data.TGL_SK}</td>
			{if $id_jenis == ''}
			<td>{$data.NM_STATUS_PENGGUNA}</td>
			{/if}
		</tr>
		{/foreach}
</table>

{/if}

{if isset($rekap)}

	<table>
		<tr>
			<th>No</th>
			<th>Nomor SK</th>
			<th>Tanggal SK</th>
			{if $id_jenis == ''}
			<th>Jenis SK</th>
			{/if}
			<th>Jenjang D3</th>
			<th>Jenjang S1</th>
			<th>Jenjang S2</th>
			<th>Jenjang S3</th>
			<th>Jenjang Profesi</th>
			<th>Jenjang Spesialis</th>

		</tr>
		{$no=1}
		{foreach $rekap as $data}
			<tr>
				<td>{$no++}</td>
				<td>{$data.NO_SK}</td>
				<td>{$data.TGL_SK}</td>
				{if $id_jenis == ''}
				<td>{$data.TTG}</td>
				{/if}
				<td style="text-align:right"><a href="evaluasi-rekap-sk.php?jen=5&periode={$smarty.post.periode}&jenis={$smarty.post.jenis}&sk={$data.NO_SK}">{$data.D3}</a></td>
				<td style="text-align:right"><a href="evaluasi-rekap-sk.php?jen=1&periode={$smarty.post.periode}&jenis={$smarty.post.jenis}&sk={$data.NO_SK}">{$data.S1}</a></td>
				<td style="text-align:right"><a href="evaluasi-rekap-sk.php?jen=2&periode={$smarty.post.periode}&jenis={$smarty.post.jenis}&sk={$data.NO_SK}">{$data.S2}</a></td>
				<td style="text-align:right"><a href="evaluasi-rekap-sk.php?jen=3&periode={$smarty.post.periode}&jenis={$smarty.post.jenis}&sk={$data.NO_SK}">{$data.S3}</a></td>
				<td style="text-align:right"><a href="evaluasi-rekap-sk.php?jen=9&periode={$smarty.post.periode}&jenis={$smarty.post.jenis}&sk={$data.NO_SK}">{$data.PROFESI}</a></td>
				<td style="text-align:right"><a href="evaluasi-rekap-sk.php?jen=10&periode={$smarty.post.periode}&jenis={$smarty.post.jenis}&sk={$data.NO_SK}">{$data.SPESIALIS}</a></td>
			</tr>
		{/foreach}
	</table>

{/if}
