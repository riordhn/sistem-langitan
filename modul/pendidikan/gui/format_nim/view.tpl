<div class="center_title_bar">Format Untuk Generate NIM</div>

<form action="format-nim.php" method="post">
    <table>
        <!-- <tr>
            <th colspan="2">Format Perguruan Tinggi</th>
        </tr> -->
        <tr>
            <td>Format NIM Perguruan Tinggi</td>
            <td><input type="text" name="format_nim_pt" value="{$perguruan_tinggi[0]['FORMAT_NIM_PT']}" />
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <th>No</th>
            <th>Fakultas</th>
            <th>Format NIM Fakultas</th>
        </tr>
        {foreach $fakultas_set as $f}
        <tr>
            <td class="center">{$f@index + 1}</td>
            <td>Fakultas {$f.NM_FAKULTAS}</td>
            <td><input type="text" name="format_nim_fakultas_{$f.ID_FAKULTAS}" value="{$f.FORMAT_NIM_FAKULTAS}" /></td>
        </tr>
        {/foreach}
        <tr>
            <td colspan="3" class="center">
                <input type="submit" value="Simpan" />
                <input type="hidden" name="mode" value="update" />
            </td>
        </tr>
    </table>

</form>

<br/>
<h3>Keterangan Format NIM:</h3>
<ul>
    <li><b>[F] </b> : Kode NIM Dari Isian Master Fakultas</li>
    <li><b>[PS] </b> : Kode NIM Dari Isian Master Program Studi</li>
    <li><b>[A] </b> : Angkatan Mahasiswa</li>
    <li><b>[Seri] </b> : Nomor Urut NIM Mahasiswa</li>
</ul>

<br/>
<h3>*) Format NIM Fakultas Diisi Hanya Apabila Terdapat Perbedaan Format Antar Fakultas</h3>