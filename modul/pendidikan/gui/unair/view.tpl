{literal}
    <style>
        .span_button{
            padding: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            background-color: #009933;
            color: #ffffff;
            cursor: pointer;
        }
    </style>
{/literal}    
<div class="center_title_bar">Mahasiswa</div>
<form action="data-mahasiswa.php" method="post">
    <table>
        <tr>
            <th colspan="4">PARAMETER MAHASISWA</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select id="fakultas" name="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $smarty.request.fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
            <td>Tahun Akademik Mahasiswa</td>
            <td>
                <select name="tahun_akademik">
                    <option value="">Semua</option>
                    {foreach $data_tahun_akademik as $data}
                        <option value="{$data.THN_AKADEMIK_SEMESTER}" {if $smarty.request.tahun_akademik==$data.THN_AKADEMIK_SEMESTER}selected="true"{/if}>{$data.THN_AKADEMIK_SEMESTER}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
			<td>Jenjang</td>
			<td>
				<select id="jenjang" name="jenjang">
                    <option value="">Semua</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.request.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
            <td>Status Mahasiswa</td>
            <td>
                <select id="status" name="status">
                    <option value="">Semua</option> 
                   {foreach $data_status as $data}
                        <option value="{$data.ID_STATUS_PENGGUNA}" {if $smarty.request.status==$data.ID_STATUS_PENGGUNA}selected="true"{/if}>{$data.NM_STATUS_PENGGUNA}</option>
                    {/foreach}                    
                </select>
            </td>
        </tr>
		<tr>
			<td>Program Studi</td>
            <td>
                <select id="program_studi" name="program_studi">
                    <option value="">Semua</option>
					{foreach $program_studi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.request.program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach} 
                </select>
            </td>

			<td>Jalur</td>
			<td><select id="jalur" name="jalur">
                    <option value="">Semua</option> 
                   {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $smarty.request.jalur==$data.ID_JALUR}selected="true"{/if}>{$data.NM_JALUR}</option>
                    {/foreach}                    
                </select>
			</td>
		</tr>
		<tr>
			<td>Negara</td>
			<td colspan="4">
				<select id="negara" name="negara">
                    <option value="">Semua</option>
					{foreach $data_negara as $data}
                        <option value="{$data.ID_NEGARA}" {if $smarty.post.negara==$data.ID_NEGARA}selected="true"{/if}>{$data.NM_NEGARA|upper}</option>
                    {/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Provinsi</td>
			<td>
				<select id="provinsi" name="provinsi">
                    <option value="">Semua</option>
                </select>
			</td>
			<td>kota</td>
			<td>
				<select id="kota" name="kota">
                    <option value="">Semua</option>
                </select>
			</td>
		</tr>
        <tr>
            <td colspan="4" class="center"><input type="submit" value="Tampilkan"/></td>
        </tr>
        <input type="hidden" name="tampil" value="ok"/>
    </table>
</form>
{if isset($tampil)}
    <table>
        <tr>
            <th>NO</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>ANGKATAN</th>
            <th>JENJANG</th>
            <th>PROGRAM STUDI</th>
            <th>FAKULTAS</th>
            <th>STATUS</th>
            <th>ALAMAT</th>
			<th>KOTA</th>
			<th>TELP</th>
			<th>AGAMA</th>
        </tr>
        {$nomer=1}
        {foreach $data_mahasiswa as $data}
            <tr>
                <td>{$nomer++}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>{$data.THN_ANGKATAN_MHS}</td>
                <td>{$data.NM_JENJANG}</td>
                <td>{$data.NM_PROGRAM_STUDI}</td>
                <td>{$data.NM_FAKULTAS}</td>
                <td>{$data.NM_STATUS_PENGGUNA}</td>
                <td>{$data.ALAMAT_MHS}</td>
				<td>{$data.NM_KOTA}</td>
				<td>{$data.MOBILE_MHS}</td>
				<td>{$data.NM_AGAMA}</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="12" class="center" style="padding: 10px;">
                <span class="span_button" onclick="get_link_report('excel', '{$smarty.request.fakultas}', '{$smarty.request.tahun_akademik}', '{$smarty.request.jenjang}', '{$smarty.request.status}', '{$smarty.request.program_studi}', '{$smarty.request.jalur}')">Export Excel</span>
                <span class="span_button" onclick="get_link_report('pdf')">Print PDF</span>
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script type="text/javascript">
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#negara').change(function(){
            $.ajax({
                type:'post',
                url:'getNegara.php',
                data:'id_negara='+$('#negara').val(),
                success:function(data){
                    $('#provinsi').html(data);
                }                    
            })
			
		 });
		 
		 $('#provinsi').change(function(){
				$.ajax({
					type:'post',
					url:'getKota.php',
					data:'id_provinsi='+$('#provinsi').val(),
					success:function(data){
						$('#kota').html(data);
					}                    
				})
		 });
		
        function get_link_report(type, fakultas, tahun_akademik, jenjang, status, program_studi, jalur){
            link=document.location.hash.replace('#mahasiswa-unair!', '');
            if(type=='excel'){
                window.location.href='excel-'+link+'?fakultas='+fakultas+'&tahun_akademik='+tahun_akademik+'&jenjang='+jenjang+'&status='+status+'&program_studi='+program_studi+'&jalur='+jalur;
            }else
                window.open('cetak-'+link);
        }
    </script>
{/literal}