<div class="center_title_bar">TRANSKRIP MAHASISWA</div>  
<form id="form1" name="form1" method="post" action="wisuda-transkrip.php">
<table>
<tr>
	<td>NIM</td>
	<td> : </td>
	<td><input type="text" name="nim" /></td>
	<td><input type="submit" value="Tampil" /></td>
</tr>
</table>
</form>


{if isset($mhs)}

<table width="600">
  <tr>
    <th colspan="2" style="text-align:center">Biodata</th>
  </tr>
  <tr>
    <td>NIM</td>
    <td>{$mhs['NIM_MHS']}</td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>{$mhs['NM_PENGGUNA']}</td>
  </tr>
  <tr>
    <td>Program Studi</td>
    <td>{$mhs['NM_JENJANG']} - {$mhs['NM_PROGRAM_STUDI']}</td>
  </tr>
  <tr>
  	<td colspan="2" style="text-align:center">
    	<input type="button" class="button" onClick="window.open('transkrip.php?nim={$smarty.request.nim}')" value="Cetak" /> 
    </td>
  </tr>
</table>
</form>

{else if isset($data_kosong)}
    <span>{$data_kosong}</span>
{/if}