<div class="center_title_bar">YUDISIUM WISUDA</div>
<form action="wisuda-yudisium.php" method="post">
	<table width="700">
    	<tr>
        	<td>Status Bayar</td>
            <td>
            	<select name="status_bayar">
                	<option value="1" {if $status_bayar==1} selected="selected" {/if}>Sudah Bayar</option>
                    <option value="2" {if $status_bayar==2} selected="selected" {/if}>Belum Bayar</option>
					<option value="3" {if $status_bayar==3} selected="selected" {/if}>Yudisium</option>
                    <option value="4" {if $status_bayar==4} selected="selected" {/if}>Batal Yudisium</option>
            	</select>
            </td>
            <td>Periode Wisuda</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $id_periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}</option>
					{/foreach}
            	</select>
            </td>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas">
					<option value="">Semua</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $id_fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" />
            	{if $smarty.post.status_bayar == 1}
<input type="button" value="Cetak" onclick="window.open('wisuda-yudisium-cetak.php?status_bayar={$smarty.post.status_bayar}&periode={$smarty.post.periode}&fakultas={$smarty.post.fakultas}');">
{/if}
            </td>
        </tr>
    </table>
</form>
{if isset($pengajuan)}


<table>
<tr>
	<th>NO</th>
	<th>NIM</th>
    <th>NAMA</th>
    <th>FAKULTAS</th>
    <th>PROGRAM STUDI</th>	
	<th>No Ijasah</th>
	<th>Tgl Lulus</th>
	<th>Bayar</th>
	<th>Biodata</th>
	<th>Evaluasi</th>
	<th>Upload File</th>
	<th>Abstrak dan Judul</th>
    <th>Foto</th>
    <th>ELPT</th>
</tr>
	{$no = 1}
	{foreach $pengajuan as $data}
    	<tr>
        	<td>{$no++}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_PENGGUNA}</td>
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>			
			<td>{$data.NO_IJASAH}</td>
			<td>{$data.TGL_LULUS_PENGAJUAN}</td>
			<td>{if $data.TGL_BAYAR <> ''}Sudah{else}Belum{/if}</td>
			<td>{if $data.BIODATA <> 0 or $data.BIODATA <> ''}Sudah{else}Belum{/if}</td>
			<td>{if $data.EVA_HASIL <> 0 or $data.EVA_HASIL <> ''}Sudah{else}Belum{/if}</td>
			<td>{if $data.STAT_UPLOAD <> '' and $data.STAT_UPLOAD <> ''}Sudah{else}Belum{/if}</td>
			<td>{if $data.ABSTRAK_TA_CLOB <> '' and $data.JUDUL_TA <> ''}Sudah{else}Belum{/if}</td>
            <td>
            	{assign var='foto' value="../../foto_wisuda/{$data.NIM_MHS}.jpg"}    
            {if file_exists($foto)} 
  					Sudah
                {else}
                	Belum 
				{/if}
            </td>
            <td>{$data.ELPT}</td>
        </tr>
    {/foreach}
</table>
{elseif isset($pengajuan_bayar)}


<table>
<tr>
	<th>NO</th>
	<th>NIM</th>
    <th>NAMA</th>
	<th>TTL</th>
	<th>KELAMIN</th>
    <th>FAKULTAS</th>
    <th>PROGRAM STUDI</th>
	<th>TGL LULUS</th>
	<th>NO IJASAH</th>
	<th>Tanggal Bayar</th>
	<th>Jumlah Bayar</th>
	<th>Jumlah Cicilan</th>	
	<th>Foto</th>
    <th>ELPT</th>
</tr>
	{$no = 1}
	{foreach $pengajuan_bayar as $data}
		{if ($no % 2) == 0}
		 	{$warna = "#CCCCCC"}
		{else}
		 	{$warna = ""}
		{/if}
    	<tr bgcolor="{$warna}">
        	<td>{$no++}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_PENGGUNA}</td>
            <td>{$data.LAHIR_IJAZAH}</td>
			<td>{$data.KELAMIN_PENGGUNA}</td>
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
			<td>{$data.TGL_LULUS_PENGAJUAN}</td>
			<td>{$data.NO_IJASAH}</td>
			<td>{$data.TGL_BAYAR}</td>
			<td style="text-align:right">{if $data.TARIF == $data.CICILAN}0{else}{$data.BESAR_BIAYA|number_format:0:"":"."}{/if}</td>
			<td style="text-align:right">{$data.CICILAN|number_format:0:"":"."}</td>
			<td>
            	{capture assign='foto'}../../foto_wisuda/{$data.NIM_MHS}.jpg{/capture}
                {if $foto|file_exists eq ''} 
  					Belum
                {else}
                	Sudah 
				{/if}
            </td>
            <td>{$data.ELPT}</td>
        </tr>
    {/foreach}
</table>
{/if}


{literal}
    <script>
            $("#tgl_mulai").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$("#tgl_selesai").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
    </script>
{/literal}
