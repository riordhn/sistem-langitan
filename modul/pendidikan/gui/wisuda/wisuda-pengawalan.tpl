<div class="center_title_bar">POSISI IJASAH</div>  
<form action="wisuda-pengawalan.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode Wisuda</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $id_periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}</option>
					{/foreach}
            	</select>
            </td>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas">
					<option value="">- Pilih Fakultas -</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $id_fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>
{if isset($pengajuan)}
<form name="f3" action="wisuda-pengawalan.php" method="post">
<table style="font-size:12px; width:1200px;">
<tr>
	<th rowspan="2" style="text-align:center">NO</th>
	<th rowspan="2" style="text-align:center">NAMA</th>
	<th rowspan="2" style="text-align:center">NIM</th>
    <th rowspan="2" style="text-align:center">FAKULTAS</th>
    <th rowspan="2" style="text-align:center">JENJANG</th>
    <th rowspan="2" style="text-align:center">PROGRAM STUDI</th>
    <th rowspan="2" style="text-align:center">TEMPAT DAN TANGGAL LAHIR</th>
	<th rowspan="2" style="text-align:center">TGL LULUS</th>
    <th rowspan="2" style="text-align:center">NO IJASAH</th>
    <th rowspan="2" style="text-align:center">NO SERI</th>
    <th colspan="9" style="text-align:center">POSISI IJASAH</th>
    <th rowspan="2" style="text-align:center">NIM</th>
	<th colspan="9" style="text-align:center">TANGGAL POSISI IJASAH</th>
</tr>
	
        <tr>
          <th>Dirpend Cetak <input type="checkbox" id="cek_all1" name="cek_all1" /></th>
    	  <th>Dirpend Ke Fakultas (Ttd Dekan) <input type="checkbox" id="cek_all2" name="cek_all2" /></th>
    	  <th>Fakultas Ke Dirpend (Ttd Rektor)</th>
          <th>Dirpend Ke Rektor <input type="checkbox" id="cek_all8" name="cek_all8" /></th>
          <th>Rektor Ke Dirpend <input type="checkbox" id="cek_all9" name="cek_all9" /></th>
		  <th>Dirpend Ke Fakultas <input type="checkbox" id="cek_all4" name="cek_all4" /></th>
    	  <th>Fakultas Ke Mhs</th>
          <th>Fakultas Ke Dirpend (> 3 Bulan)</th>
		  <th>Dirpend Ke Mhs <input type="checkbox" id="cek_all7" name="cek_all7" /></th>
            
		  <th>Dirpend Ke Cetak</th>
    	  <th>Dirpend Ke Fakultas (Ttd Dekan)</th>
    	  <th>Fakultas Ke Dirpend (Ttd Rektor)</th>
		  <th>Dirpend Ke Rektor</th>
          <th>Rektor Ke Dirpend</th>
          <th>Dirpend Ke Fakultas</th>
    	  <th>Fakultas Ke Mhs</th>
          <th>Fakultas Ke Dirpend (> 3 Bulan)</th>
		  <th>Dirpend Ke Mhs</th>
      	</tr>
	{$no = 1}
	{foreach $pengajuan as $data}
    	<tr {if $data.TGL_POSISI_IJASAH5 == "" and $data.BATAS_WAKTU == 1 and $data.TGL_POSISI_IJASAH4 != ""}bgcolor="#CCFF66"{/if}>
        	<td>{$no++}</td>
			<td>{$data.NM_PENGGUNA}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_JENJANG}</td>
            <td>{$data.NM_PROGRAM_STUDI}</td>
			<td>{$data.LAHIR_IJAZAH}</td>
            <td>{$data.TGL_LULUS_PENGAJUAN}</td>
            <td>{$data.NO_IJASAH}</td>
            <td><input type="text" name="seri_kertas{$no}" value="{$data.NO_SERI_KERTAS}" size="6" /></td>
            <td style="text-align:center">
					<input type="radio" value="1" class="cek1" name="cek{$no}" {if $data.POSISI_IJASAH == 1}checked="checked"{/if}/>
			</td>
            <td style="text-align:center">
				<input type="radio" value="2" class="cek2" name="cek{$no}" {if $data.POSISI_IJASAH == 2}checked="checked"{/if}/>
			</td>
            <td style="text-align:center">
				{if $data.POSISI_IJASAH == 3}Ya{else}Tidak{/if}
			</td>

            <td style="text-align:center">
				<input type="radio" value="8"  class="cek8" name="cek{$no}" {if $data.POSISI_IJASAH == 8}checked="checked"{/if}/>
			</td>
            <td style="text-align:center">
				<input type="radio" value="9"  class="cek9" name="cek{$no}" {if $data.POSISI_IJASAH == 9}checked="checked"{/if}/>
			</td>
			<td style="text-align:center">
				<input type="radio" value="4"  class="cek4" name="cek{$no}" {if $data.POSISI_IJASAH == 4}checked="checked"{/if}/>
			</td>
            <td style="text-align:center">{if $data.POSISI_IJASAH == 5}Ya{else}Tidak{/if}</td>
            <td style="text-align:center">
				{if $data.POSISI_IJASAH == 6}Ya{else}Tidak{/if}
			</td>
			<td style="text-align:center">
				<input type="radio"  value="7" class="cek7" name="cek{$no}" {if $data.POSISI_IJASAH == 7}checked="checked"{/if}/>
			</td>
            <td>{$data.NIM_MHS}</td>
			<td style="text-align:center">{$data.TGL_POSISI_IJASAH}</td>
			<td style="text-align:center">{$data.TGL_POSISI_IJASAH2}</td>
			<td style="text-align:center">{$data.TGL_POSISI_IJASAH3}</td>
            <td style="text-align:center">{$data.TGL_POSISI_IJASAH8}</td>
            <td style="text-align:center">{$data.TGL_POSISI_IJASAH9}</td>
			<td style="text-align:center">{$data.TGL_POSISI_IJASAH4}</td>
            <td style="text-align:center">{$data.TGL_POSISI_IJASAH5}</td>
			<td style="text-align:center">{$data.TGL_POSISI_IJASAH6}</td>
			<td style="text-align:center">{$data.TGL_POSISI_IJASAH7}</td>
        </tr>
        <input type="hidden" name="id_mhs{$no}" value="{$data.ID_MHS}" />
		<input type="hidden" name="jenjang{$no}" value="{$data.ID_JENJANG}" />
		<input type="hidden" name="seri_hidden{$no}" value="{$data.NO_SERI_KERTAS}" />
    {/foreach}
    <tr>
    	<td colspan="30" style="text-align:center">
        	<input type="submit" value="Simpan" name="simpan" />
            <input type="button" value="Cetak" onclick="window.open('excel-wisuda-pengawalan.php?periode={$id_periode}&fakultas={$id_fakultas}');" />
            <input type="hidden" name="no" value="{$no}" />
            <input type="hidden" name="periode" value="{$id_periode}" />
            <input type="hidden" name="fakultas" value="{$id_fakultas}" />
        </td>
    </tr>
</table>
</form>
{/if}


{literal}
    <script type="text/javascript">
			
		$("#cek_all1").click(function(){
				var checked_status = this.checked;
				$(".cek1").each(function()
				{
					this.checked = checked_status;
				});
		});
		
		$("#cek_all2").click(function(){
				var checked_status = this.checked;
				$(".cek2").each(function()
				{
					this.checked = checked_status;
				});
		});
		
		$("#cek_all3").click(function(){
				var checked_status = this.checked;
				$(".cek3").each(function()
				{
					this.checked = checked_status;
				});
		});
		
		$("#cek_all4").click(function(){
				var checked_status = this.checked;
				$(".cek4").each(function()
				{
					this.checked = checked_status;
				});
		});
		
		$("#cek_all5").click(function(){
				var checked_status = this.checked;
				$(".cek5").each(function()
				{
					this.checked = checked_status;
				});
		});
		
		$("#cek_all6").click(function(){
				var checked_status = this.checked;
				$(".cek6").each(function()
				{
					this.checked = checked_status;
				});
		});
		
		$("#cek_all7").click(function(){
				var checked_status = this.checked;
				$(".cek7").each(function()
				{
					this.checked = checked_status;
				});
		});
		
		$("#cek_all8").click(function(){
				var checked_status = this.checked;
				$(".cek8").each(function()
				{
					this.checked = checked_status;
				});
		});
		
		$("#cek_all9").click(function(){
				var checked_status = this.checked;
				$(".cek9").each(function()
				{
					this.checked = checked_status;
				});
		});
		
    </script>
{/literal}