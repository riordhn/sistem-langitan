<div class="center_title_bar">INPUT NOMER IJASAH DAN CETAK DRAFT IJASAH</div>
{literal}

<script type="text/javascript">
function SubmitForm(){
//make a reference to the iFrame;
var iframeElem = parent.document.getElementById("iframe");
//make a reference to the window of the iFrame (Note: for Mozilla it's diferent you got to use contentDocument instead of contentWindow);
var win = iframeElem.contentWindow;
//make a reference to the form element in the iframe;
var theForm = win.document.getElementById("form1");
//submit the form
theForm.submit();
//just testing
document.getElementById('judul_ta').value=theForm.elements[0].value;

}
</script>

{/literal}
<form id="form1" name="form1" method="post" action="wisuda-no-draft.php">
<table>
<tr>
	<td>NIM</td>
	<td> : </td>
	<td><input type="text" name="nim" /></td>
	<td><input type="submit" value="Tampil"/></td>
</tr>
</table>
</form>

<h2><font color="#FF0000">{$pesan}</font></h2>

{if isset($ijasah)}
<form action="wisuda-no-draft.php" method="post" id="f2" name="f2">
<input type="hidden" value="" name="judul_ta" id="judul_ta" />
<table width="600">
  <tr>
    <th colspan="2" style="text-align:center">Biodata</th>
  </tr>
  <tr>
    <td>NIM</td>
    <td>{$ijasah[0]['NIM_MHS']}</td>
  </tr>
  <tr>
    <td>Program Studi</td>
    <td>{$ijasah[0]['NM_JENJANG']} - {$ijasah[0]['NM_PROGRAM_STUDI']}</td>
  </tr>
  <tr>
    <td><b>Nama Lengkap</b></td>
    <td><input name="nm_pengguna" type="text" class="required" value="{$ijasah[0]['NM_PENGGUNA']}" size="50" /> * Tanpa Gelar</td>
  </tr>
  <tr>
    <td><b>Gelar Depan</b></td>
    <td><input type="text"  name="gelar_depan" value="{$ijasah[0]['GELAR_DEPAN']}" />
    &nbsp; Contoh : Prof. Dr. </td>
  </tr>
  <tr>
    <td><b>Gelar Belakang</b></td>
    <td><input type="text" name="gelar_belakang" value="{$ijasah[0]['GELAR_BELAKANG']}"  />
    &nbsp; Contoh : M.Kes., Drh</td>
  </tr>
    <tr>
      <td><b>Tempat, Tanggal Lahir</b></td>
      <td><input type="text" name="ttl" id="ttl" value="{if $ijasah[0]['LAHIR_IJAZAH'] != ''}{$ijasah[0]['LAHIR_IJAZAH']}{else if $ijasah[0]['NM_KOTA'] != '' and $tgl_lahir != ''}{$ijasah[0]['NM_KOTA']}, {$tgl_lahir}{else}{/if}" size="40" /><br /><br />
		<b><i>* Diisi jika, tempat dan tanggal lahir tidak sesuai dengan ijasah yang terakhir</i></b><br />
        <b><i>* Contoh : Semarang, 20 Nopember 2012</i></b>
        </td>
  </tr>
  <tr>
    <th colspan="2" style="text-align:center">No Ijasah</th>
  </tr>
  <tr>
    <td>No Ijasah</td>
    <td>{if $ijasah[0]['NO_IJASAH'] != ''}<input type="text" value="{$ijasah[0]['NO_IJASAH']}" name="no_ijasah_full" />{/if}</td>
  </tr>
	<tr>
    	<td>No. Urut Ijasah</td>
    	<td><input name="no_ijasah" type="text" id="no_ijasah"  class="required number" size="5" value="{$kode_ijasah}" /> 
    	*hanya no urut </td>
    </tr>
  <tr>
    	<td>Tgl Lulus</td>
    	<td><input type="text" name="tgl_lulus" id="tgl_lulus" class="required" value="{$ijasah[0]['TGL_LULUS']}" /></td>
    </tr>
    <tr>
    	<td>SK Yudisium</td>
    	<td><input type="text" name="sk_yudisium" size="30" value="{$ijasah[0]['SK_YUDISIUM']}"/></td>
    </tr>
    <tr>
    	<td>Tgl SK Yudisium</td>
    	<td><input type="text" name="tgl_sk_yudisium" id="tgl_sk_yudisium" value="{$ijasah[0]['TGL_SK_YUDISIUM']}" /></td>
    </tr>
    <tr>
      <td>IPK</td>
      <td><input type="text" name="ipk" size="4" value="{$ijasah[0]['IPK']}"/></td>
    </tr>
    <tr>
      <td>SKS Total</td>
      <td><input type="text" name="sks_total" size="4" value="{$ijasah[0]['SKS_TOTAL']}"/></td>
    </tr>
    <tr>
      <td>ELPT</td>
      <td><input type="text" id="elpt" name="elpt" size="4" value="{$ijasah[0]['ELPT']}"/></td>
    </tr>
	{if $ijasah[0]['ID_JENJANG'] == 3}
	<tr>
      <td>Judul Disertasi</td>
      <td>
      	<iframe src="wisuda-rte.php?nim={$ijasah[0]['NIM_MHS']}" width="100%" height="300" name="myframename"  id="iframe">
       <!-- <textarea name="judul_ta" cols="50" rows="3" style="width:100%" id="judul_ta">{$ijasah[0]['JUDUL_TA']}</textarea>-->
       	</iframe>
       </td>
    </tr>
	{/if}
    <tr>
    	<td colspan="2" style="text-align:center">
        	<input type="submit" value="Simpan" onclick="SubmitForm()" />
			<input type="button" value="Cetak Draft" onclick="window.open('../akademik/proses/wisuda-draft-cetak.php?nim={$ijasah[0]['NIM_MHS']}');">
        	<input type="hidden" value="{$ijasah[0]['NIM_MHS']}" name="nim" />
            <input type="hidden" value="{$ijasah[0]['ID_PROGRAM_STUDI']}" name="id_prodi" />
			<input type="hidden" value="{$ijasah[0]['ID_FAKULTAS']}" name="id_fakultas" />
            <input type="hidden" value="{$ijasah[0]['ID_JENJANG']}" name="id_jenjang" />
            <input type="hidden" value="{$ijasah[0]['ID_MHS']}" name="id_mhs" />
            <input type="hidden" value="{$ijasah[0]['NO_IJASAH']}" name="ijasah_admisi" />
			 <input type="hidden" value="{$ijasah[0]['ID_ADMISI']}" name="id_admisi" />
        </td>
    </tr>
</table>
</form>
Contoh No Ijasah : 
<font color="#FF0000">16</font>/<font color="#0066FF">0113</font>/<font color="#00FF00">01</font>.<font color="#FF00FF">8</font>/<font color="#993300">Sp</font>/<font color="#330033">2012</font>
<br />
<font color="#FF0000">Merah </font> : No. urut Ijasah
<br />
<font color="#0066FF">Biru </font> : Kode Universitas
<br />
<font color="#00FF00">Hijau </font> : Kode Fakultas
<br />
<font color="#FF00FF">Pink </font> : Kode Program Studi Ijasah
<br />
<font color="#993300">Coklat </font> : Kode Jenjang Ijasah
<br />
<font color="#330033">Ungu </font> : Tahun Wisuda

{literal}
<script language="text/javascript">
            $('#f2').validate();
            $("#tgl_lulus" ).datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			 $("#tgl_sk_yudisium").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
</script>
{/literal}

{/if}

