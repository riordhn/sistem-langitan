<div class="center_title_bar">PINDAH PERIODE WISUDA MAHASISWA</div>  
<form id="form1" name="form1" method="get" action="wisuda-pindah-periode.php">
<table>
<tr>
	<td>NIM</td>
	<td> : </td>
	<td><input type="text" name="nim" /></td>
	<td><input type="submit" value="Tampil" /></td>
</tr>
</table>
</form>

<h2><font color="#FF0000">{$pesan}</font></h2>

{if isset($ijasah)}
<form action="wisuda-pindah-periode.php" method="get" id="f2">
<table width="600">
  <tr>
    <th colspan="2" style="text-align:center">Biodata</th>
  </tr>
  <tr>
    <td>NIM</td>
    <td>{$ijasah[0]['NIM_MHS']}</td>
  </tr>
  <tr>
    <td>Nama Lengkap</td>
    <td>{$ijasah[0]['NM_PENGGUNA']}</td>
  </tr>
  <tr>
    <td>Program Studi</td>
    <td>{$ijasah[0]['NM_JENJANG']} - {$ijasah[0]['NM_PROGRAM_STUDI']}</td>
  </tr>
  <tr>
    <td>Tagihan Wisuda</td>
    <td>{$ijasah[0]['TAGIHAN_WISUDA']}</td>
  </tr>
  <tr>
    <td>Periode Wisuda</td>
    <td>{$ijasah[0]['NM_TARIF_WISUDA']}</td>
  </tr>
  <tr>
    <td>Pindah Ke Periode Wisuda</td>
    <td>
		<select name="periode">
			{foreach $periode as $data}
				<option value="{$data.ID_TARIF_WISUDA}" {if $data.ID_TARIF_WISUDA == $ijasah[0][ID_TARIF_WISUDA]} selected="selected"{/if}>{$data.NM_TARIF_WISUDA}</option>
			{/foreach}
		</select>
	</td>
  </tr>
  <tr>
    <td>Role</td>
    <td>{if $ijasah[0]['ID_ROLE'] == 28}
			<select name="role">
				<option value="28" {if $ijasah[0]['ID_ROLE'] == 28} selected="selected"{/if}>Alumni</option>
				<option value="3">Mahasiswa</option>
			</select>
		{else}
			{$ijasah[0]['NM_ROLE']}
		{/if}
	</td>
  </tr>
    <tr>
    	<td colspan="2" style="text-align:center">
			<input type="hidden" name="nim" value="{$ijasah[0]['NIM_MHS']}" />
        	<input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>
<br />


<form action="wisuda-pindah-periode.php" method="post" id="f">
<table width="600">
  <tr>
    <th colspan="8" style="text-align:center">PEMBAYARAN WISUDA</th>
  </tr>
  		<tr>
        	<th>No</th>
        	<th>Periode Bayar</th>
            <th>Besar Biaya</th>
            <th>Tgl Bayar</th>
            <th>Bank</th>
            <th>Keterangan</th>
            <th>Aksi</th>
        </tr>
     {$no = 1}   
	{foreach $pembayaran_w as $data}
    	
        {if $smarty.request.aksi == 'edit'}
        
        <tr>
        	<td>{$no++}</td>
        	<td>
            <select name="periode{$no}">
			{foreach $periode as $data1}
				<option value="{$data1.ID_TARIF_WISUDA}" {if $data1.ID_TARIF_WISUDA == $data.ID_TARIF_WISUDA} selected="selected"{/if}>{$data1.NM_TARIF_WISUDA}</option>
			{/foreach}
			</select>
            </td>
            <td>{$data.BESAR_BIAYA}</td>
            <td>{$data.TGL_BAYAR}</td>
            <td>{$data.NM_BANK}</td>
            <td><textarea name="keterangan{$no}">{$data.KETERANGAN}</textarea></td>
            <td></td>
        </tr>
       
        <input type="hidden" name="id_pembayaran{$no}" value="{$data.ID_PEMBAYARAN_WISUDA}" />
        {else}
    
    	<tr>
        	<td>{$no++}</td>
        	<td>{$data.NM_TARIF_WISUDA}</td>
            <td>{$data.BESAR_BIAYA}</td>
            <td>{$data.TGL_BAYAR}</td>
            <td>{$data.NM_BANK}</td>
            <td>{$data.KETERANGAN}</td>
            <td><a href="wisuda-pindah-periode.php?nim={$smarty.request.nim}&aksi=edit">Edit</a></td>
        </tr>
        {/if}
        
    {foreachelse}
    	Tidak ada data pembayaran
    {/foreach}
    <tr>
    	<td colspan="8" style="text-align:center">
			<input type="hidden" name="nim" value="{$smarty.request.nim}" />
             <input type="hidden" name="no" value="{$no}" />
        	<input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

{literal}
<script language="text/javascript">
            $('#f2').validate();
</script>
{/literal}

{/if}
