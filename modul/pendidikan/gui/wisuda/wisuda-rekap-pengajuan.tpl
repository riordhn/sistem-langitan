<div class="center_title_bar">REKAP PENGAJUAN WISUDA</div>  
<form action="wisuda-rekap-pengajuan.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode Wisuda</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $id_periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}</option>
					{/foreach}
            	</select>
            </td>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas">
					<option value="">Semua</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $id_fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($rekap)}

	<table>
		<tr>
			{if $id_fakultas == ''}
			<th>Fakultas</th>
			{/if}
			<th>Program Studi</th>
			<th>Jumlah Wisudawan</th>
			<th>Pengajuan Cetak</th>
            <th>fakultas -> mhs</th>
            <th>dirpen -> mhs</th>
		</tr>
		{foreach $rekap as $data}
			<tr>
				{if $id_fakultas == ''}
				<td>{$data.NM_FAKULTAS}</td>
				{/if}
				<td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
				<td style="text-align:center"><a href="wisuda-rekap-pengajuan.php?id_prodi={$data.ID_PROGRAM_STUDI}&id_periode={$smarty.request.periode}&detail=1">{$data.IKUT_WISUDA}</a></td>
				<td style="text-align:center"><a href="wisuda-rekap-pengajuan.php?id_prodi={$data.ID_PROGRAM_STUDI}&id_periode={$smarty.request.periode}&detail=2">{$data.PENGAJUAN_CETAK}</a></td>
                <td style="text-align:center"><a href="wisuda-rekap-pengajuan.php?id_prodi={$data.ID_PROGRAM_STUDI}&id_periode={$smarty.request.periode}&detail=3">{$data.FAK_MHS}</a></td>
                <td style="text-align:center"><a href="wisuda-rekap-pengajuan.php?id_prodi={$data.ID_PROGRAM_STUDI}&id_periode={$smarty.request.periode}&detail=4">{$data.PEND_MHS}</a></td>
			</tr>
			{$total1 = $total1 + $data.IKUT_WISUDA}
			{$total2 = $total2 + $data.PENGAJUAN_CETAK}
            {$total3 = $total3 + $data.FAK_MHS}
            {$total4 = $total4 + $data.PEND_MHS}
		{/foreach}
		<tr>
			<th {if $id_fakultas == ''}colspan="2"{/if}>Total</th>
			<th style="text-align:center">{$total1}</th>
			<th style="text-align:center">{$total2}</th>
            <th style="text-align:center">{$total3}</th>
			<th style="text-align:center">{$total4}</th>
		</tr>
	</table>
{else if isset($detail)}
<table>
	<tr>
    	<th>No</th>
        <th>NIM</th>
        <th>NAMA</th>
        <th>Prodi</th>
    </tr>
    {$no = 1}
    {foreach $detail as $data}
    <tr>
    	<td>{$no++}</td>
        <td>{$data.NIM_MHS}</td>
        <td>{$data.NM_PENGGUNA}</td>
        <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
    </tr>
    {/foreach}
</table>
{/if}
