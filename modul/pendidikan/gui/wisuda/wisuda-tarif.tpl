<div class="center_title_bar">Periode dan Tarif Wisuda</div>

{if isset($mode) and $mode != ''}
<div id="alert" class="ui-widget" style="display:none;margin-left:15px;">
        <div class="ui-state-error ui-corner-all" style="padding: 5px;width:20%;"> 
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
                    Tanggal Harus Diisi.</p>
        </div>
</div>
<form name="tarif" id="tarif" action="wisuda-tarif.php?{$smarty.server.QUERY_STRING}" method="post">
<table>
    <tr>
        <td>
            Nama Wisuda
        </td>
        <td>
            <select name="nama_tarif">
                {foreach $master as $data}
                <option value="{$data.ID_TARIF_WISUDA}" {if $nama_tarif==$data.ID_TARIF_WISUDA} selected="selected" {/if}>
                    {$data.NM_TARIF_WISUDA}
                </option>
                {/foreach}
            </select>        	
        </td>
    </tr>
    <tr>
        <td>
            Semester
        </td>
        <td>
            <select name="semester">
                {foreach $get_semester_all as $data}
                <option value="{$data.ID_SEMESTER}" {if $semester==$data.ID_SEMESTER} selected="selected" {/if}>
                    {$data.NM_SEMESTER} {$data.THN_AKADEMIK_SEMESTER}
                </option>
                {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>
            Jenjang
        </td>
        <td>
            <select name="jenjang">
                {foreach $get_jenjang as $data}
                <option value="{$data.ID_JENJANG}" {if $jenjang==$data.ID_JENJANG} selected="selected" {/if}>{$data.NM_JENJANG}</option>
                {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>
            Tanggal Bayar Mulai
        </td>
        <td>
            <input type="text" id="tgl_awal" class="datepicker"  name="tgl_awal" {if isset($tgl_awal)} value="{$tgl_awal}"{/if} />       	
        </td>
    </tr>
    <tr>
        <td>
            Tanggal Bayar Selesai
        </td>
        <td>
            <input type="text" id="tgl_akhir" class="datepicker"  name="tgl_akhir" {if isset($tgl_akhir)} value="{$tgl_akhir}"{/if} />      	
        </td>
    </tr>
    <tr>
        <td>
            Besar Tarif
        </td>
        <td>
            Rp. <input type="text" name="besar_biaya" class="required number" {if isset($besar_biaya)} value="{$besar_biaya}"{/if}/>      	
        </td>
    </tr>
    <tr>
        <td>
            Status Tarif
        </td>
        <td>
            <label><input type="radio" value="1" name="aktif" {if $status_aktif == 1} checked="checked"{/if} /> Aktif</label>
            <label><input type="radio" value="0" name="aktif" {if $status_aktif == 0} checked="checked"{/if} /> Tidak Aktif</label>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align:center">
            <a href="wisuda-tarif.php">Kembali</a>
            <input type="hidden" name="id" {if isset($id)} value="{$id}"{/if} />
            <input type="hidden" value="{$mode}" name="mode" />
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

{literal}
    <script>
            $('#tarif').submit(function() {
                    if($('#tgl_awal').val()==''||$('#tgl_akhir').val()==''){
                            $('#alert').fadeIn();
                    return false;
                    }  
            });
            $('#tarif').validate();
            $( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy',changeMonth: true,
            changeYear: true});
    </script>
{/literal}
{else}
<table>
    <tr>
        <th>Nama Wisuda</th>
        <th>Semester</th>
        <th>Jenjang</th>
        <th>Tgl Bayar Mulai</th>
        <th>Tgl Selesai Mulai</th>
        <th>Besar Biaya</th>
        <th>Status</th>
        <th>Aksi</th>
    </tr>
    {foreach $periode_wiuda as $data}
    <tr>
        <td>{$data.NM_TARIF_WISUDA}</td>
        <td>{$data.NM_SEMESTER} {$data.THN_AKADEMIK_SEMESTER}</td>
        <td>{$data.NM_JENJANG}</td>
        <td>{date_create_from_format('d-M-y', $data.TGL_BAYAR_MULAI)|date_format:"d F Y"}</td>
        <td>{date_create_from_format('d-M-y', $data.TGL_BAYAR_SELESAI)|date_format:"d F Y"}</td>
        <td style="text-align:right">{$data.BESAR_BIAYA|number_format:0:",":"."}</td>
        <td>{if $data.STATUS_AKTIF == 1}Aktif{else}Tidak Aktif{/if}</td>
        <td><a href="wisuda-tarif.php?mode=edit&id={$data.ID_PERIODE_WISUDA}">Edit</a></td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="8" style="text-align:center">
            <form action="wisuda-tarif.php" method="get">
            <input type="hidden" name="mode" value="input" />
            <input type="submit" value="Input" />
            </form>
        </td>
    </tr>
</table>
{/if}