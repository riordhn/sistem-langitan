<div class="center_title_bar">MASTER NAMA WISUDA</div>
{if $mode == 'tambah' or $mode == 'edit'}
	{if $mode == 'edit'}
		{$tarif = $tarif_edit[0]['NM_TARIF_WISUDA']}
		{$id = $tarif_edit[0]['ID_TARIF_WISUDA']}
    {else}
    	{$tarif = ''}
        {$id = ''}
    {/if}
<form name="f1" action="wisuda-master-tarif.php" method="post">
<table>
	<tr>
   	  <td>Nama Master Wisuda</td>
        <td><input name="tarif" type="text" value="{$tarif}" size="30" />
			<input type="hidden" name="id" value="{$id}"  /> 
        </td>
    </tr>
    <tr>
    	<td></td>
        <td><input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>
{else}
<table>
	<tr>
    	<th>NO</th>
        <th>NAMA Wisuda</th>
        <th>AKSI</th>
    </tr>
    {$no = 1}
    {foreach $tarif as $data}
    	<tr>
        	<td>{$no++}</td>
            <td>{$data.NM_TARIF_WISUDA}</td>
            <td style="text-align:center"><a href="wisuda-master-tarif.php?id={$data.ID_TARIF_WISUDA}&mode=edit">Edit</a></td>
        </tr>
    {/foreach}
    <tr>
    	<td colspan="3" style="text-align:center">
        	<form action="wisuda-master-tarif.php" method="get">
                <input type="submit" value="Tambah" />
                <input type="hidden" value="tambah" name="mode" />
            </form>
        </td>
    </tr>
</table>
{/if}