<div class="center_title_bar">LOCK ABSENSI WISUDA</div>
<form action="wisuda-lock-absensi.php" method="post" id="f2">
	<table width="700">
    	<tr>
            <td>Periode Wisuda</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $smarty.post.periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}
					</option>
					{/foreach}
            	</select>
            </td>		
			<td>Fakultas</td>
            <td>
                <select id="fakultas" name="fakultas" class="required">
                    <option value="">Semua Fakultas</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $smarty.post.fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
		</tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>


{if isset($lock)}
<form name="f1" method="post" action="wisuda-lock-absensi.php">
	<table>
		<tr>
			<th>No</th>
			<th>Fakultas</th>
			<th><input type="checkbox" id="cek_all" name="cek_all" /></th>
		</tr>
		{$no=1}
	{foreach $lock as $data}
    	<tr>
			<td>{$no++}</td>
			<td>{$data.NM_FAKULTAS}</td>
			<td style="text-align:center">
				<input type="checkbox" class="cek" name="cek{$no}" {if $data.STATUS_LOCK_ABSENSI == 1}checked="checked"{/if} value="1" />
			</td>
		</tr>
		<input type="hidden" name="fakultas{$no}" value="{$data.ID_FAKULTAS}" />
	{/foreach}
    <tr>
		<td colspan="6" style="text-align:center">
			<input type="hidden" name="mode" value="insert" />
			<input type="hidden" name="no" value="{$no}" />
			<input type="hidden" name="periode" value="{$smarty.post.periode}" />
    		<input type="submit" value="Simpan" />
        </td>
    </tr>

	</table>
</form>
{/if}


{literal}
    <script>
		
		
		$("#cek_all").click(function()				
			{
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
			});
		
    </script>
{/literal}