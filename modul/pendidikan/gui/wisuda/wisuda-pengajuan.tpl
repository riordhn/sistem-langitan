<div class="center_title_bar">PENGAJUAN WISUDA</div>  
<form name="f1" action="wisuda-pengajuan.php" method="post">
	<table>
		<tr>
			<td>NIM</td>
			<td><input type="text" name="nim" /></td>
			<td><input type="submit" value="Tampil" /></td>
		</tr>
	</table>
</form>

<b>{$error}</b>

{if isset($mhs)}
<form action="wisuda-pengajuan.php" method="post" id="f3">
<table>
	<tr>
    	<td>NIM</td>
        <td>{$mhs.NIM_MHS}</td>
    </tr>
    <tr>
    	<td>Nama</td>
        <td>{$mhs.NM_PENGGUNA}</td>
    </tr>
    <tr>
    	<td>Fakultas</td>
        <td>{$mhs.NM_FAKULTAS}</td>
    </tr>
    <tr>
    	<td>Program Studi</td>
        <td>{$mhs.NM_JENJANG} - {$mhs.NM_PROGRAM_STUDI}</td>
    </tr>
    <tr>
    	<td>Periode</td>
        <td>
        		<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $smarty.request.periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}</option>
					{/foreach}
            	</select>
         </td>
    </tr>
    <tr>
    	<td>No Ijasah</td>
        <td><input type="text" name="no_ijasah" value="{$pengajuan.NO_IJASAH}" size="30" class="required" /></td>
    </tr>
    <tr>
    	<td>Tanggal Lulus</td>
        <td><input type="text" name="tgl_lulus" id="tgl_lulus" value="{$pengajuan.TGL_LULUS_PENGAJUAN}" class="required" /></td>
    </tr>
    <tr>
    	<td>Tempat, Tanggal Lahir</td>
        <td><input type="text" name="ttl" value="{$pengajuan.LAHIR_IJAZAH}" size="50" class="required" /></td>
    </tr>
    <tr>
    	<td>No Seri</td>
        <td><input type="text" name="no_seri" value="{$pengajuan.NO_SERI_KERTAS}" /></td>
    </tr>
    <tr>
        <td colspan="2" style="text-align:center">
        	<input type="submit" value="Pengajuan" name="pengajuan" />
        	<input type="hidden" value="{$smarty.request.nim}" name="nim" />
        </td>
    </tr>
</table>
</form>
{/if}


{literal}
    <script>
            $("#tgl_lulus").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$('#f3').validate();
    </script>
{/literal}