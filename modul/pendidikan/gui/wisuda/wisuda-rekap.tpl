<div class="center_title_bar">REKAP WISUDA</div>  
<form action="wisuda-rekap.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode Wisuda</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $id_periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}</option>
					{/foreach}
            	</select>
            </td>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas">
					<option value="">Semua</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $id_fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
	    <td>Tampilan</td>
	    <td>
            	<select name="tampilan">
		    <option value="fak" {if $smarty.request.tampilan == 'fak'} selected="selected"{/if}>Fakultas/ Prodi</option>
		    <option value="thn_angkatan" {if $smarty.request.tampilan == 'thn_angkatan'} selected="selected"{/if}>Angkatan</option>		
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($rekap)}

	{if $smarty.request.tampilan == 'thn_angkatan'}
	<table>
		<tr>
			<th>NO</th>
			<th>THN ANGKATAN</th>
			<th>D3</th>
			<th>S1 Reguler</th>
			<th>S1 Aj</th>
			<th>S2</th>
			<th>S3</th>
			<th>PROFESI</th>
			<th>SPESIALIS</th>
			<th>TOTAL</th>
		</tr>
		{$no = 1}
		{foreach $rekap as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.THN_ANGKATAN_MHS}</td>
			<td>{$data.D3}</td>
			<td>{$data.S1_R}</td>
			<td>{$data.S1_AJ}</td>
			<td>{$data.S2}</td>
			<td>{$data.S3}</td>
			<td>{$data.PROFESI}</td>
			<td>{$data.SPESIALIS}</td>
			<td>{$data.TOTAL}</td>
		</tr>
		{$total = $total + $data.TOTAL}
		{$totald3 = $totald3 + $data.D3}
		{$totals1 = $totals1 + $data.S1_R}
		{$totals1_aj = $totals1_aj + $data.S1_AJ}
		{$totals2 = $totals2 + $data.S2}
		{$totals3 = $totals3 + $data.S3}
		{$totalprof = $totalprof + $data.PROFESI}
		{$totalspes = $totalspes + $data.SPESIALIS}
		{/foreach}
		<tr>

			<td colspan="2">Total</td>
			<td>{$totald3}</td>
			<td>{$totals1}</td>
			<td>{$totals1_aj}</td>
			<td>{$totals2}</td>
			<td>{$totals3}</td>
			<td>{$totalprof}</td>
			<td>{$totalspes}</td>
			<td>{$total}</td>
		</tr>
	</table>


	{else}
	<table>
		<tr>
			{if $id_fakultas == ''}
			<th>Fakultas</th>
			{/if}
			<th>Program Studi</th>
			<th>Jumlah Wisudawan</th>
		</tr>
		{foreach $rekap as $data}
			<tr>
				{if $id_fakultas == ''}
				<td>{$data.NM_FAKULTAS}</td>
				{/if}
				<td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
				<td style="text-align:center">{$data.JML}</td>
			</tr>
			{$total = $total + $data.JML}
		{/foreach}
		<tr>
			<th {if $id_fakultas == ''}colspan="2"{/if}>Total</th>
			<th style="text-align:center">{$total}</th>
		</tr>
	</table>
	{/if}

{/if}