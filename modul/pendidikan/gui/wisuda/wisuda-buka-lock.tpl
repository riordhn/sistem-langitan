<div class="center_title_bar">BUKA LOCK WISUDA</div>  
<form name="f1" action="wisuda-buka-lock.php" method="post">
	<table>
		<tr>
			<td>NIM</td>
			<td><input type="text" name="nim" /></td>
			<td><input type="submit" value="Tampil" /></td>
		</tr>
	</table>
</form>

{if isset($mhs)}

<form name="f2" id="f2" action="wisuda-buka-lock.php" method="post">
	<table>
		<tr>
			<th colspan="2">BIODATA</th>
		</tr>
		<tr>
			<td>NIM</td>
			<td>{$mhs['NIM_MHS']}</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>{$mhs['NM_PENGGUNA']}</td>
		</tr>
		<tr>
			<td>Fakultas</td>
			<td>{$mhs['NM_FAKULTAS']}</td>
		</tr>
		<tr>
			<td>Program Studi</td>
			<td>{$mhs['NM_JENJANG']} - {$mhs['NM_PROGRAM_STUDI']}</td>
		</tr>
		<tr>
			<td>Periode Wisuda</td>
			<td>{$mhs['NM_TARIF_WISUDA']}</td>
		</tr>
		<tr>
			<td>Buka Lock Wisuda</td>
			<td><input type="checkbox" name="lock" {if $mhs['BUKA_LOCK_WISUDA'] == 1} checked="checked"{/if} value="1"  /></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<input type="submit" value="Simpan" />
				<input type="hidden" value="{$mhs['ID_MHS']}" name="id_mhs" />
				<input type="hidden" value="{$mhs['NIM_MHS']}" name="nim" />
			</td>
		</tr>
	</table>
</form>

{else if isset($data_kosong)}
    <span>{$data_kosong}</span>
{/if}