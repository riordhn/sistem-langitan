<div class="center_title_bar">REKAP BUKU WISUDAWAN</div>  
<form action="wisuda-buku.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode Wisuda</td>
            <td>
            	<select name="periode">
				    <option value="">Pilih Periode</option>
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $id_periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}</option>
					{/foreach}
            	</select>
            </td>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas">
					<option value="">Semua</option>
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $id_fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($rekap)}
    {if $id_fakultas<>''}
	<table width="800">
		{$i=1}{$j=2}{$nmfk=''}{$nmjjg=''}{$k=0}{$l=0}
		{foreach $rekap as $indx => $data}			
			{if $rekap[$k].NM_MHS==''}
			{break}			
			{/if}
		    	{if $rekap[$k].NM_FAKULTAS <> $nmfk}
				<tr>
					<th colspan="4" style="text-align:center">
					FAKULTAS {$rekap[$k].NM_FAKULTAS}
					</th>
				</tr>	
				{$nmjjg=''}
				{$nmfk=$rekap[$k].NM_FAKULTAS}
				{$i=1}{$j=2}
			{/if}
			
			{if $rekap[$k].NM_JENJANG <> $nmjjg}
				<tr>
					<th colspan="4" style="text-align:center">
					JENJANG {$rekap[$k].NM_JENJANG}
					</th>
				</tr>
				
				{$i=1}
			{/if}
			{if $i==11}
				<tr>
					<th colspan="4" style="text-align:center"></th>
				</tr>
			{/if}
			{if $i==21}
				<tr>
					<th colspan="4" style="text-align:center">
					Halaman {$j++}
					</th>
				</tr>
				{$i=1}
			{/if}
			{$nmjjg=$rekap[$k].NM_JENJANG}
					
			<tr>
			    	<td colspan="2" style="text-align:right" width="295">{$i++}</td>
				{$k=$k+1}
				{if $rekap[$k].NM_JENJANG==$rekap[$k-1].NM_JENJANG}
				<td colspan="2" style="text-align:left" width="295">{$i++}</td>
				{/if}
				{$k=$k-1}		
				
				
			</tr>
			<tr width="350">
			    	<td colspan="2" style="text-align:right;font-weight: bold" width="295">{$rekap[$k].NM_MHS}</td>
				{$k=$k+1}
				{if $rekap[$k].NM_JENJANG==$rekap[$k-1].NM_JENJANG}	
				<td colspan="2" style="text-align:left;font-weight: bold" width="295">{$rekap[$k].NM_MHS}</td>
				{/if}
				{$k=$k-1}
			</tr>
			<tr>
				<td style="text-align:right" bgcolor="#CCFF66" width="295" height="150">
					{$rekap[$k].NIM_MHS}<br />
					{$rekap[$k].LAHIR_IJAZAH}<br />
					{$rekap[$k].NM_PROGRAM_STUDI}<br />
					Lulus : {$rekap[$k].TGL_LULUS_PENGAJUAN}<br /><br/>
					{$rekap[$k].NM_AYAH_MHS}<br />
					{$rekap[$k].ALAMAT_MHS}<br /><br />
					{$rekap[$k].EMAIL_PENGGUNA}<br />
				</td>
				<td bgcolor="#CCFF66">
					<img src='../../foto_wisuda/{$rekap[$k].NIM_MHS}.jpg' width=105 height=150 vspace=1 hspace=2>{$k=$k+1}
				</td>
				{if $rekap[$k].NM_JENJANG==$rekap[$k-1].NM_JENJANG}
				<td bgcolor="#CCFF66">
					<img src='../../foto_wisuda/{$rekap[$k].NIM_MHS}.jpg' width=105 height=150 vspace=1 hspace=2>
				</td>
				<td style="text-align:left" bgcolor="#CCFF66" width="295" height="150">
					{$rekap[$k].NIM_MHS}<br />
					{$rekap[$k].LAHIR_IJAZAH}<br />
					{$rekap[$k].NM_PROGRAM_STUDI}<br />
					Lulus : {$rekap[$k].TGL_LULUS_PENGAJUAN}<br /><br />
					{$rekap[$k].NM_AYAH_MHS}<br />
					{$rekap[$k].ALAMAT_MHS}<br /><br />
					{$rekap[$k].EMAIL_PENGGUNA}<br />
				</td>
				{$k=$k+1}
				{/if}
			</tr>
		{/foreach}
	<tr>
		{if $id_fakultas == ''}
		<td colspan="4" style="text-align:center">
		{/if}
        	{if $id_fakultas <> ''}
		<td colspan="4" style="text-align:center">
		<input type="button" value="Cetak" onclick="window.open('wisuda_buku_cetak.php?sem={$id_semester}&fak={$id_fakultas}&jenj={$id_jenjang}&tahun={$tahun}');"/>
		{/if}                
		
            </td>
		</tr>
	</table>

  {else}

	<table>
		{$i=1}{$j=2}{$nmfk=''}{$nmjjg=''}
		{foreach $rekap as $data}	
		    	{if $data.NM_FAKULTAS <> $nmfk}
				<tr>
					<th colspan="9" style="text-align:center">
					FAKULTAS {$data.NM_FAKULTAS}
					</th>
				</tr>	
				{$nmjjg=''}
				{$nmfk=$data.NM_FAKULTAS}
				{$i=1}{$j=2}
			{/if}
			
			{if $data.NM_JENJANG <> $nmjjg}
				<tr>
					<th colspan="9" style="text-align:center">
					JENJANG {$data.NM_JENJANG}
					</th>
				</tr>
				<tr>
		    			<th>No</th>
					<th>Nama Mahasiswa</th>
					<th>NIM</th>
					<th>Tempat, Tanggal Lahir</th>
					<th>Program Studi</th>			
					<th>Tanggal Lulus</th>
					<th>Nama Orang Tua</th>			
					<th>Alamat</th>
					<th>Email</th>
				</tr>	
				{$i=1}
			{/if}
			{if $i==21}
				<tr>
					<th colspan="9" style="text-align:center">
					Halaman {$j++}
					</th>
				</tr>
				{$i=1}
			{/if}
			{$nmjjg=$data.NM_JENJANG}
					
			<tr>
			    	<td>{$i++}</td>
				<td>{$data.NM_MHS}</td>
				<td>{$data.NIM_MHS}</td>
				<td>{$data.LAHIR_IJAZAH}</td>
				<td>{$data.NM_PROGRAM_STUDI}</td>
				<td>{$data.TGL_LULUS_PENGAJUAN}</td>
				<td>{$data.NM_AYAH_MHS}</td>
				<td>{$data.ALAMAT_MHS}</td>
				<td>{$data.EMAIL_PENGGUNA}</td>
			</tr>
		{/foreach}
		<tr>
		{if $id_fakultas == ''}
		<td colspan="9" style="text-align:center">
			{/if}
        		{if $id_fakultas <> ''}
			<td colspan="9" style="text-align:center">
			<input type="button" value="Cetak" onclick="window.open('wisuda_buku_cetak.php?sem={$id_semester}&fak={$id_fakultas}&jenj={$id_jenjang}&tahun={$tahun}');"/>
			{/if}                
		
            	</td>
		</tr>
	</table>		
  {/if}
{/if}
