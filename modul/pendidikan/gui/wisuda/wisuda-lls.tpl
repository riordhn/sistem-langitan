<div class="center_title_bar">REKAP LULUSAN</div>  
<form action="wisuda-lls.php" method="post">
	<table width="700">
    	<tr>
            <td>Periode Lulus</td>
            <td>
            	<select name="periode">
                	{foreach $periode as $data}
                	<option value="{$data.TGL_LLS}" {if $id_periode==$data.TGL_LLS} selected="selected" {/if}>{$data.TGL_LLS}</option>
					{/foreach}
            	</select>
            </td>
			<td>Fakultas</td>
            <td>
            	<select name="fakultas">
                	{foreach $fakultas as $data}
                	<option value="{$data.ID_FAKULTAS}" {if $id_fakultas==$data.ID_FAKULTAS} selected="selected" {/if}>{$data.NM_FAKULTAS}</option>
					{/foreach}
            	</select>
            </td>
			<td>Jenjang</td>
	    <td>
            	<select name="jenjang">
                	{foreach $jenjang as $data}
                	<option value="{$data.ID_JENJANG}" {if $id_jenjang==$data.ID_JENJANG} selected="selected" {/if}>{$data.NM_JENJANG}</option>
					{/foreach}
            	</select>
            </td>
        </tr>
        <tr>
        	<td colspan="6" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>

{if isset($rekap)}
	<table align='center'>
	<tr>
	<th colspan=8 style="text-align:center">
	REKAP LULUSAN SELURUH FAKULTAS
	</th>
	</tr>
	<tr>
	<th>
	FAKULTAS
	</th>
	<th>
	D3
	</th>
	<th>
	S1
	</th>
	<th>
	S2
	</th>
	<th>
	S3
	</th>
	<th>
	PROFESI
	</th>
	<th>
	SPESIALIS
	</th>
	<th>
	Total
	</th>
	</tr>
	
	{foreach $total as $data}
	<tr>
	<td>
	{$data.NM_FAKULTAS}
	</td>
	<td>
	{$data.D3}
	</td>
	<td>
	{$data.S1}
	</td>
	<td>
	{$data.S2}
	</td>
	<td>
	{$data.S3}
	</td>
	<td>
	{$data.PROFESI}
	</td>
	<td>
	{$data.SPESIALIS}
	</td>
	{$ttl = $data.D3 + $data.S1 + $data.S2 + $data.S3 + $data.PROFESI + $data.SPESIALIS}
	<td>
	{$ttl}
	</td>
	</tr>
	
	{$ttld3 = $ttld3 + $data.D3}
	{$ttls1 = $ttls1 + $data.S1}
	{$ttls2 = $ttls2 + $data.S2}
	{$ttls3 = $ttls3 + $data.S3}
	{$ttlprofesi = $ttlprofesi + $data.PROFESI}
	{$ttlspesialis = $ttlspesialis + $data.SPESIALIS}
	{$tot = $tot + $ttl}
	
	{/foreach}
	<tr>
	<th>
	</th>
	<td>
	{$ttld3}
	</td>
	<td>
	{$ttls1}
	</td>
	<td>
	{$ttls2}
	</td>
	<td>
	{$ttls3}
	</td>
	<td>
	{$ttlprofesi}
	</td>
	<td>
	{$ttlspesialis}
	</td>
	<td>
	{$tot}
	</td>
    </tr>
	</table>
	<table>
		
		{$i=1}{$nmfk=''}{$nmjjg=''}
		{foreach $rekap as $data}	
		    	{if $data.NM_FAKULTAS <> $nmfk}
				<tr>
					{if $data.NM_JENJANG=='S3'}
					<th colspan="11" style="text-align:center">
					{/if}
					{if $data.NM_JENJANG<>'S3'}
					<th colspan="10" style="text-align:center">
					{/if}
					REKAP LULUSAN FAKULTAS {$data.NM_FAKULTAS}
					</th>
				</tr>
			{$nmfk=$data.NM_FAKULTAS}
			{$nmjjg=''}	
			{/if}
			
			{if $data.NM_JENJANG <> $nmjjg}
				<tr>
					{if $data.NM_JENJANG=='S3'}
					<th colspan="11" style="text-align:center">
					{/if}
					{if $data.NM_JENJANG<>'S3'}
					<th colspan="10" style="text-align:center">
					{/if}
					JENJANG {$data.NM_JENJANG}
					</th>
				</tr>
				<tr>
		    			<th>No</th>
					<th>Nama Mahasiswa</th>
					<th>NIM</th>
					<th>Tempat, Tanggal Lahir</th>
					<th>Program Studi</th>			
					<th>Tanggal Lulus</th>
					<th>No Ijasah</th>
					<th>No Seri Kertas</th>
					<th>Tgl Cetak Ijasah</th>
					{if $data.NM_JENJANG=='S3'}
					<th>Judul TA</th>
					{/if}			
				</tr>	
			{/if}
			{$nmjjg=$data.NM_JENJANG}
			<tr>
			    	<td>{$i++}</td>
				<td>{$data.GELAR_DEPAN}{ucwords(strtolower($data.NM_PENGGUNA))}{$data.GELAR_BELAKANG}</td>
				<td>{$data.NIM_MHS}</td>
				<td>{$data.TTL}</td>
				<td>{$data.NM_PROGRAM_STUDI}</td>
				<td>{$data.TGL_LLS}</td>
				<td>{$data.NO_IJASAH}</td>
				<td>{$data.NO_SERI_KERTAS}</td>
				<td>{$data.TGL_POSISI_IJASAH}</td>
				{if $data.NM_JENJANG=='S3'}
				<td>{$data.JUDUL_TA}</td>
				{/if}
			</tr>
		{/foreach}
	</table>
<table>	
<tr>
		<td colspan=2 style="text-align:center">             
		<input type="button" value="Cetak" onclick="window.open('wisuda-lls-cetak.php?fak={$id_fakultas}&jnj={$id_jenjang}&prd={$id_periode}');"/>            
		<input type="button" value="Excel" onclick="window.open('wisuda-lls-excel.php?fak={$id_fakultas}&jnj={$id_jenjang}&prd={$id_periode}');"/>
            	</td>
		</tr>
</table>	

{/if}
