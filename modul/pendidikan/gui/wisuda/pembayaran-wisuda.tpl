<div class="center_title_bar">Pembayaran Wisuda</div>  
<form method="get" action="pembayaran-wisuda.php">
<table border="1">
	<tr>
    	<td>Periode Wisuda</td>
        <td>
        	<select name="periode">
            	{foreach $periode as $data}
            		<option value="{$data.ID_TARIF_WISUDA}" {if $id_periode == $data.ID_TARIF_WISUDA} selected="selected"{/if}>{$data.NM_TARIF_WISUDA}</option>
                {/foreach}
            </select>
        </td>
    	<td>NIM</td>
        <td><input type="text" name="nim" /></td>
        <td><input type="submit" value="Cari" /></td>
    </tr>
</table>
</form>

{if isset($tagihan)}
<form action="pembayaran-wisuda.php" method="post">
<table border="1">
    <tr>
    	<td>Tarif</td>
        <td style="text-align:right">{$tarif}</td>
    </tr>
    <tr>
    	<td>Cicilan</td>
        <td style="text-align:right">{$cicilan}</td>
    </tr>
    <tr>
    	<td>Tagihan</td>
        <td><input type="text" name="tagihan" value="{$tagihan}"  style="text-align:right" /></td>
    </tr>
    <tr>
    	<td>Bank</td>
        <td>
        	<select name="bank">
            		<option value="">- PILIH BANK -</option>
            	{foreach $bank as $data}
                	<option value="{$data.ID_BANK}">{$data.NM_BANK}</option>
                {/foreach}
            </select>
        </td>
    </tr>
    <tr>
    	<td>Bank Via</td>
        <td>
        	<select name="bank_via">
            		<option value="">- PILIH BANK VIA -</option>
            	{foreach $bank_via as $data}
                	<option value="{$data.ID_BANK_VIA}">{$data.NAMA_BANK_VIA}</option>
                {/foreach}
            </select>
        </td>
    </tr>
    <tr>
    	<td>No Transaksi</td>
        <td><input type="text" name="no_transaksi" /></td>
    </tr>
    <tr>
    	<td>Keterangan</td>
        <td><textarea name="keterangan"></textarea></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align:center">
        	<input type="submit" value="Simpan" />
        	<input type="hidden" name="id_mhs" value="{$id_mhs}" />
            <input type="hidden" name="id_periode" value="{$id_periode}" />
        </td>
    </tr>
</table>
</form>
{/if}