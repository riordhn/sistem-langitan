   <div class="center_title_bar">Histori Mahasiswa</div>

<form action="akademik-historymhs.php" method="get">
<table>
    <tr>
        <td>NIM Mahasiswa</td>
        <td><input type="search" name="nim_mhs" {if isset($smarty.get.nim_mhs)}value="{$smarty.get.nim_mhs}"{/if}/><input type="submit" value="Tampilkan" /></td>
    </tr>
</table>
</form>
   
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>      
        <table class="tb_dosen" width="70%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="134" class="tb_frame">Kode Mahasiswa</td>
            <td width="10" class="tb_frame">:</td>
            <td width="294" class="tb_frame">0803123456</td>
          </tr>
          <tr>
            <td class="tb_frame">Nama Mahasiswa</td>
            <td class="tb_frame">:</td>
            <td class="tb_frame">Sutejo Sudarsono </td>
          </tr>
        </table>
        <table width="100%" border="1" cellpadding="0" cellspacing="0">
          <tr>
            <td><strong> Mata kuliah yang pernah diambil mahasiswa</strong>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr class="left_menu">
                  <td width="8%" bgcolor="#333333"><font color="#FFFFFF">No.</font></td>
                  <td width="14%" bgcolor="#333333"><font color="#FFFFFF">Kode MK </font></td>
                  <td width="27%" bgcolor="#333333"><font color="#FFFFFF">Nama MK </font> </td>
                  <td width="13%" bgcolor="#333333"><font color="#FFFFFF">Thn Ajaran </font></td>
                  <td width="20%" bgcolor="#333333"><font color="#FFFFFF">Semester</font></td>
                  <td width="9%" bgcolor="#333333"><font color="#FFFFFF">N. UTS</font></td>
                  <td width="9%" bgcolor="#333333"><font color="#FFFFFF">N. UAS</font></td>
                </tr>
                <tr>
                  <td >1</td>
                  <td >MK-0012</td>
                  <td >Jaringan</td>
                  <td >2007/2008</td>
                  <td >Gasal</td>
                  <td >98</td>
                  <td >72</td>
                </tr>
                <tr>
                  <td >2</td>
                  <td >MK-0034</td>
                  <td >Analisa Pasar Keuangan </td>
                  <td >2009/2010</td>
                  <td >Genap</td>
                  <td >78</td>
                  <td >89</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="100%" border="1" cellpadding="0" cellspacing="0">
          <tr>
            <td><strong> Mata kuliah yang sedang diambil mahasiswa</strong>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr class="left_menu">
                  <td width="8%" bgcolor="#333333"><font color="#FFFFFF">No.</font></td>
                  <td width="14%" bgcolor="#333333"><font color="#FFFFFF">Kode MK </font></td>
                  <td width="27%" bgcolor="#333333"><font color="#FFFFFF">Nama MK </font> </td>
                  <td width="13%" bgcolor="#333333"><font color="#FFFFFF">Thn Ajaran </font></td>
                  <td width="20%" bgcolor="#333333"><font color="#FFFFFF">Semester</font></td>
                  <td width="9%" bgcolor="#333333"><font color="#FFFFFF">N. UTS</font></td>
                  <td width="9%" bgcolor="#333333"><font color="#FFFFFF">N. UAS</font></td>
                </tr>
                <tr>
                  <td >1</td>
                  <td >MK-0078</td>
                  <td >Proses Bisnis </td>
                  <td >2007/2008</td>
                  <td >Gasal</td>
                  <td >-</td>
                  <td >-</td>
                </tr>
              </table>
            <p></p></td>
          </tr>
        </table>
        <table width="100%" border="1" cellpadding="0" cellspacing="0">
          <tr>
            <td><strong>Indeks Prestasi Mahasiswa</strong>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr class="left_menu">
                  <td width="14%" bgcolor="#333333"><font color="#FFFFFF">Indeks Prestasi (IP)</font></td>
                  <td width="13%" bgcolor="#333333"><font color="#FFFFFF">Thn Ajaran </font></td>
                  <td width="20%" bgcolor="#333333"><font color="#FFFFFF">Semester</font></td>
                  <td width="9%" bgcolor="#333333"><font color="#FFFFFF">Total IPS </font></td>
                </tr>
                <tr>
                  <td >IPS</td>
                  <td >2007/2008</td>
                  <td >Gasal</td>
                  <td >3.06</td>
                </tr>
                <tr>
                  <td >IPS</td>
                  <td >2007/208</td>
                  <td >Genap</td>
                  <td >3.32</td>
                </tr>
              </table>
			  <br>
			  Indeks Prestasi Komulatif (IPK) = 3,67</td>
          </tr>
        </table>
        <div class="panel" id="panel1" style="display: block"></div></td>
  </tr>
</table>
<table width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td><strong>Tugas Akhir / Skripsi / Thesis</strong>
        <table  class="tb_frame" width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="132">Judul</td>
            <td width="8">:</td>
            <td width="533">Pengoptimalan Jaringan Client Server</td>
          </tr>
          <tr>
            <td>Dosen Pembimbing </td>
            <td>:</td>
            <td>1. Budi Sudarsono, M.Kom.</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>:</td>
            <td>2. Dr. Arif Efendi</td>
          </tr>
        </table></td>
  </tr>
</table>
<table width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td><strong>Progress Tugas Akhir </strong>        <table width="100%" border="1">
      <tr>
        <td>Bab I </td>
        <td>Bab II</td>
        <td>Bab III</td>
        <td>Bab IV</td>
        <td>Bab V </td>
      </tr>
      <tr>
        <td><input name="checkbox" type="checkbox" value="checkbox" checked></td>
        <td><input name="checkbox" type="checkbox" value="checkbox" checked></td>
        <td><input name="checkbox" type="checkbox" value="checkbox" checked></td>
        <td>-</td>
        <td>-</td>
      </tr>
    </table></td>
  </tr>
</table>
