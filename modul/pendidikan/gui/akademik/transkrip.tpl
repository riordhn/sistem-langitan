<div class="center_title_bar">Transkrip Mahasiswa </div>

<form action="akademik-transkrip.php" method="get">
<table>
    <tr>
        <td>NIM Mahasiswa</td>
        <td><input type="search" name="nim_mhs" {if isset($smarty.get.nim_mhs)}value="{$smarty.get.nim_mhs}"{/if}/><input type="submit" value="Tampilkan" /></td>
    </tr>
</table>
</form>
{if $mahasiswa_found}
<table style="width: 100%; border: 1px solid #000;">
    <tr>
        <td style="border: none; width: 100px;">Nama</td>
        <td style="border: none; width: 280px;">: {$mahasiswa->PENGGUNA->NM_PENGGUNA}</td>
        <td style="border: none; width: 100px;">Nomer Ijazah</td>
        <td style="border: none;">: {$mahasiswa->NO_IJAZAH}</td>
    </tr>
    <tr>
        <td style="border: none;">NIM</td>
        <td style="border: none;">: {$mahasiswa->NIM_MHS}</td>
        <td style="border: none;">Jumlah SKS</td>
        <td style="border: none;">: </td>
    </tr>
    <tr>
        <td style="border: none;">Program Studi</td>
        <td style="border: none;">: {$mahasiswa->PROGRAM_STUDI->NM_PROGRAM_STUDI}</td>
        <td style="border: none;">IP Komulatif</td>
        <td style="border: none;">: </td>
    </tr>
    <tr>
        <td style="border: none;">Tanggal Daftar</td>
        <td style="border: none;">: {$mahasiswa->TGL_TERDAFTAR_MHS}</td>
        <td style="border: none;">Tanggal Lulus</td>
        <td style="border: none;">: {$mahasiswa->TGL_LULUS_MHS}</td>
    </tr>
</table>
<table style="width: 100%">
    <tr>
        <th style="width: 120px;">Semester</th>
        <th style="width: 80px;">Kode MK</th>
        <th>Nama Mata Kuliah</th>
        <th style="width:35px;">SKS</th>
        <th style="width:35px;">Nilai</th>
        <th style="width:35px;">Bobot</th>
    </tr>
    {$total_sks=0}
    {$total_bobot=0}
    {for $i=0 to $mahasiswa->MHS_STATUSs->Count()-1}
    {$mhs_status=$mahasiswa->MHS_STATUSs->Get($i)}
    <tr>
        <td {if $mhs_status->JUMLAH_MK>1}rowspan="{$mhs_status->JUMLAH_MK}"{/if}>{$mhs_status->SEMESTER->TAHUN_AJARAN}<br/>{$mhs_status->SEMESTER->NM_SEMESTER}</td>
        <td>{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get(0)->KELAS_MK->MATA_KULIAH->KODE_MATA_KULIAH}</td>
        <td>{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get(0)->KELAS_MK->MATA_KULIAH->NM_MATA_KULIAH}</td>
        <td class="center">{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get(0)->KELAS_MK->MATA_KULIAH->KREDIT_MATA_KULIAH}</td>
        <td class="center">{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get(0)->NILAI_HURUF}</td>
        <td class="center">{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get(0)->NILAI_BOBOT}</td>
    </tr>
    {$total_sks=$total_sks+$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get(0)->KELAS_MK->MATA_KULIAH->KREDIT_MATA_KULIAH}
    {$total_bobot=$total_bobot+$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get(0)->NILAI_BOBOT}
    {* Menampilkan data mata kuliah *}
    {if $mhs_status->JUMLAH_MK>1}
    {for $j=1 to $mhs_status->SEMESTER->PENGAMBILAN_MKs->Count()-1}
    <tr>
        <td>{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get($j)->KELAS_MK->MATA_KULIAH->KODE_MATA_KULIAH}</td>
        <td>{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get($j)->KELAS_MK->MATA_KULIAH->NM_MATA_KULIAH}</td>
        <td class="center">{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get($j)->KELAS_MK->MATA_KULIAH->KREDIT_MATA_KULIAH}</td>
        <td class="center">{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get($j)->NILAI_HURUF}</td>
        <td class="center">{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get($j)->NILAI_BOBOT}</td>
    </tr>
    {$total_sks=$total_sks+$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get($j)->KELAS_MK->MATA_KULIAH->KREDIT_MATA_KULIAH}
    {$total_bobot=$total_bobot+$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get($j)->NILAI_BOBOT}
    {/for}
    {/if}
    {/for}
    <tr>
        <td colspan="2" rowspan="3"></td>
        <td>Jumlah SKS dan Bobot</td>
        <td class="center">{$total_sks}</td>
        <td></td>
        <td class="center">{$total_bobot}</td>
    </tr>
    <tr>
        <td>IP Komulatif</td>
        <td colspan="3" class="center">{printf("%.1f",$total_bobot/$total_sks)}</td>
    </tr>
    <tr>
        <td>Predikat Kelulusan</td>
        <td colspan="3" class="center"></td>
    </tr>
</table>
<button href="akademik-transkrip.php?nim_mhs={$smarty.get.nim_mhs}">Cetak</button>
{/if}