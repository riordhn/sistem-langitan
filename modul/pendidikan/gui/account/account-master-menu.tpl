<div class="center_title_bar">Master Template MENU</div>



{if $mode == 'add' or $mode == 'edit'}

<form action="account-master-menu.php" method="post">
<table>
    <tr>
        <td>Nama Master</td>
		<td><input type="text" name="template" value="{$template.NM_TEMPLATE}" /></td>
		<td>
			<input type="submit" value="Simpan" />
			<input type="hidden" value="{$mode}" name="mode" />
			<input type="hidden" value="{$id}" name="id" />
		</td>
    </tr>
</table>
</form>

{else}

<table>
	<tr>
		<th>No</th>
		<th>Nama Master</th>
		<th>Aksi</th>
	</tr>
	{$no = 1}
	{foreach $template2 as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.NM_TEMPLATE}</td>
			<td><a href="account-master-menu.php?id={$data.ID_TEMPLATE_ROLE}&mode=edit">Edit</a></td>
		</tr>
	{/foreach}
	<tr>
		<td colspan="3" style="text-align:center">
			<a href="account-master-menu.php?mode=add">Tambah</a>
		</td>
	</tr>
</table>

{/if}