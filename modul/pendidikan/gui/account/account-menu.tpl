<div class="center_title_bar">Setting MENU</div>
{if $mode == 'add' or $mode == 'edit'}

<form action="account-menu.php" method="post">
	<table>
		<tr>
		<th colspan="2">Setting Menu</th>
		</tr>
		<tr>
			<td>Master Menu</td>
			<td>
				<select name="template_role">
					{foreach $template_role as $data}
						<option value="{$data.ID_TEMPLATE_ROLE}" {if $data.ID_TEMPLATE_ROLE == $template.ID_TEMPLATE_ROLE} selected="selected"{/if}>{$data.NM_TEMPLATE}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Menu</td>
			<td>
				<select name="modul">
					{foreach $modul as $data}
						<option value="{$data.ID_MODUL}" {if $data.ID_MODUL == $template.ID_MODUL} selected="selected"{/if}>{$data.TITLE}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Akses</td>
			<td>
				<input type="checkbox" value="1" name="akses" {if $template.AKSES == 1} checked="checked"{/if}/>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<input type="submit" value="Simpan" />
				<input type="hidden" value="{$mode}" name="mode" />
				<input type="hidden" value="{$id}" name="id" />
			</td>
		</tr>
	</table>
</form>


{else}

<table>
	<tr>
		<th>No</th>
		<th>Nama Master</th>
		<th>Menu</th>
		<th>Akses</th>
		<th>Aksi</th>
	</tr>
	{$no = 1}
	{foreach $template2 as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.NM_TEMPLATE}</td>
			<td>{$data.TITLE}</td>
			<td>{$data.AKSES}</td>
			<td><a href="account-menu.php?id={$data.ID_TEMPLATE_MODUL}&mode=edit">Edit</a></td>
		</tr>
	{/foreach}
	<tr>
		<td colspan="5" style="text-align:center">
			<a href="account-menu.php?mode=add">Tambah</a>
		</td>
	</tr>
</table>

{/if}
