<div class="center_title_bar">Setting Sub MENU</div>
{if $mode == 'add' or $mode == 'edit'}

<form action="account-sub-menu.php" method="post">
	<table>
		<tr>
		<th colspan="2">Setting Menu</th>
		</tr>
		<tr>
			<td>Master Menu</td>
			<td>
				<select name="template_role" id="template_role">
                		<option value="">-- Pilih Master --</option>
					{foreach $template_role as $data}
						<option value="{$data.ID_TEMPLATE_ROLE}" {if $data.ID_TEMPLATE_ROLE == $template.ID_TEMPLATE_ROLE} selected="selected"{/if}>{$data.NM_TEMPLATE}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Menu</td>
			<td>
				<select name="modul" id="modul">
                		<option value="">-- Pilih Menu --</option>
                    {foreach $modul as $data}
						<option value="{$data.ID_MODUL}" {if $data.ID_MODUL == $template.ID_MODUL} selected="selected"{/if}>{$data.TITLE}</option>
					{/foreach}
				</select>
			</td>
		</tr>
        <tr>
			<td>Sub Menu</td>
			<td>
				<select name="sub_menu" id="sub_menu">
                		<option value="">-- Pilih Sub Menu --</option>
                   	{foreach $sub_menu as $data}
						<option value="{$data.ID_MENU}" {if $data.ID_MENU == $template.ID_MENU} selected="selected"{/if}>{$data.TITLE}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Akses</td>
			<td>
				<input type="checkbox" value="1" name="akses" {if $template.AKSES == 1} checked="checked"{/if}/>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<input type="submit" value="Simpan" />
				<input type="hidden" value="{$mode}" name="mode" />
				<input type="hidden" value="{$id}" name="id" />
			</td>
		</tr>
	</table>
</form>


{else}

<table>
	<tr>
		<th>No</th>
		<th>Nama Master</th>
		<th>Menu</th>
        <th>Sub Menu</th>
		<th>Akses</th>
		<th>Aksi</th>
	</tr>
	{$no = 1}
	{foreach $template2 as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.NM_TEMPLATE}</td>
			<td>{$data.MODUL_TITLE}</td>
            <td>{$data.MENU_TITLE}</td>
			<td>{$data.AKSES}</td>
			<td><a href="account-sub-menu.php?id={$data.ID_TEMPLATE_MENU}&mode=edit">Edit</a> | 
            <a href="account-sub-menu.php?id={$data.ID_TEMPLATE_MENU}&mode=delete">Delete</a></td>
		</tr>
	{/foreach}
	<tr>
		<td colspan="6" style="text-align:center">
			<a href="account-sub-menu.php?mode=add">Tambah</a>
		</td>
	</tr>
</table>

{/if}

{literal}
    <script type="text/javascript">
        $('#template_role').change(function(){
            $.ajax({
                type:'post',
                url:'getMenu.php',
                data:'id_template_role='+$('#template_role').val(),
                success:function(data){
                    $('#modul').html(data);
                }                    
            })
        });
		
		
		$('#modul').change(function(){
            $.ajax({
                type:'post',
                url:'getSubMenu.php',
                data:'id_modul='+$('#modul').val(),
                success:function(data){
                    $('#sub_menu').html(data);
                }                    
            })
        });
    </script>
{/literal}
