<div class="center_title_bar">Setting Sub MENU</div>

<form action="account-menu-pengguna.php" method="post">
<table>
    <tr>
        <td>NIP / Nama Pegawai</td>
        <td><input type="text" name="nip" /></td>
        <td class="center">
            <input type="submit" value="Tampil" />
        </td>
    </tr>
</table>
</form>


{if isset($pegawai)}

	<table>
		<tr>
			<th style="text-align:center">NO</th>
			<th style="text-align:center">NIP</th>
			<th style="text-align:center">Nama</th>
			<th style="text-align:center">Role</th>
			<th style="text-align:center">Setting</th>
		</tr>
		{$no = 1}
	{foreach $pegawai as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.USERNAME}</td>
			<td>{$data.NM_PENGGUNA}</td>
			<td>{$data.NM_ROLE}</td>
			<td><a href="account-menu-pengguna.php?id={$data.ID_PENGGUNA}">Edit</a></td>
		</tr>
	{/foreach}
	</table>

{/if}

{if isset($pengguna)}

{if $pengguna.ID_ROLE != 11}
<form action="account-menu-pengguna.php" method="post">
	<table>
		<tr>
			<th colspan="2">Setting Role</th>
		</tr>
		<tr>
			<td>NIP</td>
			<td>{$pengguna.USERNAME}</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>{$pengguna.NM_PENGGUNA}</td>
		</tr>
		<tr>
			<td>Role Sekarang</td>
			<td>{$pengguna.NM_ROLE}</td>
		</tr>
		<tr>
			<td>Setting Role</td>
			<td>
				<select name="role">
					<option value="11">Pendidikan</option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<input type="submit" value="Simpan" />
				<input type="hidden" value="{$pengguna.ID_PENGGUNA}" name="id_pengguna" />
			</td>
		</tr>
	</table>
</form>

{else}

<form action="account-menu-pengguna.php" method="post">
	<table>
		<tr>
			<th colspan="2">Setting Role</th>
		</tr>
		<tr>
			<td>NIP</td>
			<td>{$pengguna.USERNAME}</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>{$pengguna.NM_PENGGUNA}</td>
		</tr>
		<tr>
			<td>Setiing Menu</td>
			<td>
				<select name="template_role">
						<option></option>
						<option value="pendidikan">Pendidikan</option>
					{foreach $template_role as $data}
						<option value="{$data.ID_TEMPLATE_ROLE}">{$data.NM_TEMPLATE}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<input type="submit" value="Simpan" />
				<input type="hidden" value="{$pengguna.ID_PENGGUNA}" name="id_pengguna" />
			</td>
		</tr>
	</table>
</form>

{/if}
{/if}
