<!DOCTYPE html>
	<head>
		<title>Modul Pendidikan - Sistem Langitan - {$nama_pt}</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="../../css/reset.css" />
		<link rel="stylesheet" type="text/css" href="../../css/text.css" />
		<link rel="stylesheet" type="text/css" href="../../css/pendidikan.css" />		
		<link rel="stylesheet" type="text/css" href="../../css/jquery-ui-1.8.11.custom.css" />
		<link rel="stylesheet" type="text/css" href="includes/sortable/themes/green/style.css" />
	</head>
	<body>
		<table class="clear-margin-bottom">
			<colgroup>
				<col />
				<col class="main-width"/>
				<col />
			</colgroup>
			<thead>
				<tr>
					<td class="header-left"></td>
					<td class="header-center" style="background-image: url('../../img/header/pendidikan-{$nama_singkat}.png')"></td>
					<td class="header-right"></td>
				</tr>
				<tr>
					<td class="tab-left"></td>
					<td class="tab-center">
						<ul>
						{foreach $modul_set as $m}
							{if $m.AKSES == 1}
								<li><a href="#{$m.NM_MODUL}!{$m.PAGE}">{$m.TITLE}</a></li>
								<li class="divider"></li>
							{/if}
						{/foreach}
							<li><a class="disable-ajax" href="../../logout.php">Logout</a></li>
						</ul>
					</td>
					<td class="tab-right"></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="body-left">&nbsp;</td>
					<td class="body-center">
						<table class="content-table">
							<colgroup>
								<col />
								<col />
							</colgroup>
							<tr>
								<td colspan="2" id="breadcrumbs" class="breadcrumbs">Loading content...</td>
							</tr>
							<tr>
							   <td id="menu" class="menu">
									
								</td>
								<td id="content" class="content">Loading content...</td>
							</tr>
						</table>
					</td>
					<td class="body-right">&nbsp;</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td class="foot-left">&nbsp;</td>
					<td class="foot-center">
						<div class="footer-nav">
							<a href="{$base_url}" class="disable-ajax">&nbsp;</a>
							<a href="{$base_url}" class="disable-ajax"></a>
						</div>
						<div class="footer">Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU</div>
					</td>
					<td class="foot-right">&nbsp;</td>
				</tr>
			</tfoot>
		</table>

		<!-- Script -->
		<script src="https://code.jquery.com/jquery-1.5.1.min.js" integrity="sha256-dkuenzrThqqlzerpNoNTmU3mHAvt4IfI9+NXnLRD3js=" crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/ui/1.8.11/jquery-ui.min.js" integrity="sha256-U+Mg4jKaeTIbzkpk6YGWzUyBahjLJJHW9gG1yynX87U=" crossorigin="anonymous"></script>
		<script type="text/javascript" src="../../js/jquery.validate.min.js"></script>
		<script language="javascript" src="../../js/jquery-input-format.js"></script>
		<script type="text/javascript">var defaultRel = 'perkuliahan'; var defaultPage = 'perkuliahan.php';</script>
		<script type="text/javascript" src="../../js/cybercampus.ajax-1.0.js"></script>
		<script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
		<script type="text/javascript" src="gui/tab.js"></script>
		<script type="text/javascript" src="includes/sortable/jquery.tablesorter.min.js"></script>
	</body>
</html>