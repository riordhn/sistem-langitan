<div class="center_title_bar">Perbarui Data Aktivitas Kuliah Mahasiswa</div>

<form method="post" action="pddikti-status-generate.php">
	<table>
		<tbody>
			<tr>
				<td>Semester</td>
				<td>
					<select name="id_semester">
						<option value="">Pilih Semester</option>
						{foreach $semester_set as $semester}
							<option value="{$semester.ID_SEMESTER}" 
								{if isset($smarty.get.id_semester)}
									{if $smarty.get.id_semester == $semester.ID_SEMESTER}selected{/if}
								{/if}>{$semester.TAHUN_AJARAN} {$semester.NM_SEMESTER}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Program Studi</td>
				<td>
					<select name="id_program_studi">
						<option value="">Pilih Program Studi</option>
						{foreach $program_studi_set as $ps}
							<option value="{$ps.ID_PROGRAM_STUDI}"
								{if isset($smarty.get.id_program_studi)}
									{if $smarty.get.id_program_studi == $ps.ID_PROGRAM_STUDI}selected{/if}
								{/if}>{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Setting</td>
				<td>
					<label><input type="checkbox" name="is_override" /> Timpa data yang sudah ada.<br/>Jika tidak dicentang, maka hanya menggenerate aktivitas mahasiswa yang belum ada saja.</label>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					Kegiatan perbarui data aktivitas kuliah akan melakukan proses berikut :
					<ul>
						<li>Mahasiswa yang melakukan KRS dan sudah terapprove, akan diberikan status AKTIF</li>
						<li>Mahasiswa yang tidak melakukan KRS (SKS = 0), maka akan diberikan status CUTI</li>
						<li>Mahasiswa yang terdapat usulan CUTI di admisi, maka SKS nya akan di set 0</li>
						<li>Mahasiswa yang CUTI pada 2 semester sebelumnya berturut-turut, tidak akan digenerate sebelum diaktifkan melalui menu Admisi</li>
						<li>Mahasiswa yang telah LULUS/KELUAR DO pada semester sebelumnya, tidak akan di proses</li>
					</ul>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" value="Proses" />
					<a href="pddikti-status.php">Kembali</a>
				</td>
			</tr>
		</tbody>
	</table>
</form>
					
{if isset($generate_result)}
	{if $generate_result}
		<strong>Data berhasil di proses.</strong>
		<a href="pddikti-status.php">Klik disini</a> untuk mengecek data
	{else}
		<strong>Data gagal di proses.</strong>
	{/if}
{/if}