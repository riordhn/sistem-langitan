<div class="center_title_bar">Aktivitas Kuliah Mahasiswa</div>

<form method="get" action="pddikti-status.php">
	<table>
		<tbody>
			<tr>
				<td>Semester</td>
				<td>
					<select name="id_semester">
						<option value="all">Semua</option>
						{foreach $semester_set as $semester}
							<option value="{$semester.ID_SEMESTER}" 
								{if isset($smarty.get.id_semester)}
									{if $smarty.get.id_semester == $semester.ID_SEMESTER}selected{/if}
								{/if}>{$semester.TAHUN_AJARAN} {$semester.NM_SEMESTER}</option>
						{/foreach}
					</select>
				</td>
				<td>Program Studi</td>
				<td>
					<select name="id_program_studi">
						<option value="all">Semua</option>
						{foreach $program_studi_set as $ps}
							<option value="{$ps.ID_PROGRAM_STUDI}"
								{if isset($smarty.get.id_program_studi)}
									{if $smarty.get.id_program_studi == $ps.ID_PROGRAM_STUDI}selected{/if}
								{/if}>{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Angkatan</td>
				<td>
					<select name="angkatan">
						<option value="all">Semua</option>
						{foreach $angkatan_set as $angkatan}
							<option value="{$angkatan.THN_AKADEMIK_SEMESTER}"
								{if isset($smarty.get.angkatan)}
									{if $smarty.get.angkatan == $angkatan.THN_AKADEMIK_SEMESTER}selected{/if}
								{/if}>{$angkatan.THN_AKADEMIK_SEMESTER}</option>
						{/foreach}
					</select>
				</td>
				<td>Status Mahasiswa</td>
				<td>
					<select name="id_status_pengguna">
						<option value="all">Semua</option>
						{foreach $status_set as $status}
							<option value="{$status.ID_STATUS_PENGGUNA}"
								{if isset($smarty.get.id_status_pengguna)}
									{if $smarty.get.id_status_pengguna == $status.ID_STATUS_PENGGUNA}selected{/if}
								{/if}>
								{if $status.FD_ID_STAT_MHS == 'A'}AKTIF{/if}
								{if $status.FD_ID_STAT_MHS == 'C'}CUTI{/if}
								{if $status.FD_ID_STAT_MHS == 'K'}KELUAR{/if}
								{if $status.FD_ID_STAT_MHS == 'N'}NON AKTIF{/if}
								{if $status.FD_ID_STAT_MHS == 'G'}DOUBLE DEGREE{/if}
								- {$status.NM_STATUS_PENGGUNA}
							</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<input type="submit" value="Tampilkan" style="float:left"/>
					<a href="pddikti-status-generate.php" style="float:right">Perbarui Data</a>
				</td>
			</tr>
		</tbody>
	</table>
</form>
					
<table>
	<thead>
		<tr>
			<th>No</th>
			<th>NIM</th>
			<th>Nama</th>
			<th>Program Studi</th>
			<th>Angkatan</th>
			<th>Semester</th>
			<th>Status Forlap</th>
			<th>Status Sistem</th>
			<th>IPS</th>
			<th>IPK</th>
			<th>SKS Semester</th>
			<th>SKS Total</th>
			<th>Sync</th>
		</tr>
	</thead>
	{if count($data_set) > 0}
		<tbody>
			{foreach $data_set as $data}
				<tr>
					<td>{$data@index + 1}</td>
					<td>{$data.NIM_MHS}</td>
					<td>{$data.NM_PENGGUNA}</td>
					<td>{$data.NM_JENJANG} {$data.NM_PROGRAM_STUDI}</td>
					<td>{$data.THN_ANGKATAN_MHS}</td>
					<td>{$data.TAHUN_AJARAN} {$data.NM_SEMESTER}</td>
					<td>
						{if $data.FD_ID_STAT_MHS == 'A'}AKTIF{/if}
						{if $data.FD_ID_STAT_MHS == 'C'}CUTI{/if}
						{if $data.FD_ID_STAT_MHS == 'K'}KELUAR{/if}
						{if $data.FD_ID_STAT_MHS == 'N'}NON AKTIF{/if}
						{if $data.FD_ID_STAT_MHS == 'G'}DOUBLE DEGREE{/if}
					</td>
					<td>{$data.NM_STATUS_PENGGUNA}</td>
					<td>{$data.IPS|floatval}</td>
					<td>{$data.IPK|floatval}</td>
					<td>{$data.SKS_SEMESTER}</td>
					<td>{$data.SKS_TOTAL}</td>
					<td>
						{if $data.FD_SYNC_ON != ''}
						{strftime('%d/%m/%Y', strtotime($data.FD_SYNC_ON))}
						{/if}
					</td>
				</tr>
			{/foreach}
		</tbody>
	{/if}
</table>