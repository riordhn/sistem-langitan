<div class="center_title_bar">Generate NIM Mahasiswa</div>

<h2>{$fakultas.NM_FAKULTAS}</h2>

<table>
    <tr>
        <th>No</th>
        <th>Program Studi</th>
    </tr>
    {foreach $program_studi_set as $ps}
    <tr>
        <td class="center">{$ps@index + 1}</td>
        <td>
            <a href="generate-nim.php?mode=detail&id_program_studi={$ps.ID_PROGRAM_STUDI}">{$ps.NM_PROGRAM_STUDI}</a>
        </td>
    </tr>
    {/foreach}
</table>

<a href="generate-nim.php">Kembali</a>