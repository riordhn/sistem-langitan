<div class="center_title_bar">Generate NIM Mahasiswa</div>

<table>
    <tr>
        <th>No</th>
        <th>Fakultas</th>
    </tr>
    {foreach $fakultas_set as $f}
    <tr>
        <td class="center">{$f@index + 1}</td>
        <td>
            <a href="generate-nim.php?mode=prodi&id_fakultas={$f.ID_FAKULTAS}">{$f.NM_FAKULTAS}</a>
        </td>
    </tr>
    {/foreach}
</table>