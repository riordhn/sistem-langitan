<div class="center_title_bar">Generate NIM Mahasiswa</div>

<h2>{$program_studi.NM_PROGRAM_STUDI}</h2>

{if $generated}<h3>{$generated}</h3>{/if}

{if !empty($cmb_set)}

<form action="generate-nim.php?mode=detail&id_program_studi={$program_studi.ID_PROGRAM_STUDI}" method="post">
    <input type="hidden" name="mode" value="generate" />
<table>
    <tr>
        <th>No</th>
        <th style="width: 80px">NIM</th>
        <th style="width: 80px">No Ujian</th>
        <th>Nama</th>
        <th></th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr>
        <td>{$cmb@index + 1}</td>
        <td class="center">{$cmb.NIM_MHS}</td>
        <td class="center">{$cmb.NO_UJIAN}</td>
        <td>{$cmb.NM_C_MHS}</td>
        <td>
            {if $cmb.NIM_MHS == ''}
                {if $cmb.STATUS == 12}
                <label><input type="checkbox" name="cek{$cmb.ID_C_MHS}" />Pilih</label>
                {/if}
                
                {if $cmb.STATUS == 11}Belum Test Kesehatan{/if}
                {if $cmb.STATUS == 10}Belum Ambil Jadwal Ujian{/if}
                {if $cmb.STATUS == 9}Belum Foto{/if}
            {/if}
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="5" class="center"><input type="submit" value="Generate" /></td>
    </tr>
</table>

</form>
{/if}

<a href="generate-nim.php?mode=prodi&id_fakultas={$program_studi.ID_FAKULTAS}">Kembali</a>