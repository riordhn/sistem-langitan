<div class="center_title_bar">Utility Sekolah</div>

<form action="utility_sekolah.php" method="get" id="filter">
	<table>
		<tr>
			<td>Negara</td>
			<td>
				<select name="id_negara">
					<option value=""> -- </option>
					{foreach $negara_set as $negara}
						<option value="{$negara.ID_NEGARA}" {if !empty($smarty.get.id_negara)}{if $smarty.get.id_negara == $negara.ID_NEGARA}selected{/if}{/if}>{$negara.NM_NEGARA}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Provinsi</td>
			<td>
				{if $provinsi_set}
					<select name="id_provinsi">
						<option value=""> -- </option>
						{foreach $provinsi_set as $provinsi}
							<option value="{$provinsi.ID_PROVINSI}" {if !empty($smarty.get.id_provinsi)}{if $smarty.get.id_provinsi == $provinsi.ID_PROVINSI}selected{/if}{/if}>{$provinsi.NM_PROVINSI}</option>
						{/foreach}
					</select>
				{/if}
			</td>
		</tr>
		<tr>
			<td>Kota</td>
			<td>
				{if $kota_set}
					<select name="id_kota">
						<option value=""> -- </option>
						{foreach $kota_set as $kota}
							<option value="{$kota.ID_KOTA}" {if !empty($smarty.get.id_kota)}{if $smarty.get.id_kota == $kota.ID_KOTA}selected{/if}{/if}>{$kota.TIPE_DATI2} {$kota.NM_KOTA}</option>
						{/foreach}
					</select>
				{/if}
			</td>
		</tr>
	</table>
</form>

<script type="text/javascript">
	jQuery(document).ready(function() {
		$('[name=id_negara]').change(function() {
			$('[name=id_kota]').val('');
			$('[name=id_provinsi]').val('');
			$('#filter').submit();
		});
		$('[name=id_provinsi]').change(function() {
			$('[name=id_kota]').val('');
			$('#filter').submit();
		});
		$('[name=id_kota]').change(function() {
			$('#filter').submit();
		});
	});
</script>

{if !empty($smarty.get.id_kota)}
<table>
	<tbody>
		<tr>
			<td>Tambah Sekolah Baru</td>
			<td>
				<form action="utility_sekolah.php?id_negara={$smarty.get.id_negara}&id_provinsi={$smarty.get.id_provinsi}&id_kota={$smarty.get.id_kota}" method="post">
					<input type="text" name="nm_sekolah" value="" placeholder="Nama sekolah"/>
					<input type="submit" value="Tambah" />
				</form>
			</td>

		</tr>
	</tbody>
</table>
{/if}

{if $sekolah_set}

<table>
	<tbody>
		<tr>
			<th>No</th>
			<th>Sekolah</th>
		</tr>
		{foreach $sekolah_set as $sekolah}
			<tr>
				<td>{$sekolah@index + 1}</td>
				<td>{$sekolah.NM_SEKOLAH}</td>
			</tr>
		{/foreach}
	</tbody>
</table>

{/if}