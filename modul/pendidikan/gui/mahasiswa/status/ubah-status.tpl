<div class="center_title_bar">Ubah Status</div>
<form action="ubah-status.php" method="get">
    <input type="hidden" name="mode" value="search" />
    <table>
        <tr>
            <td>NIM Mahasiswa</td>
            <td><input type="text" name="nim_mhs" value="{if !empty($smarty.get.nim_mhs)}{$smarty.get.nim_mhs}{/if}"/>
                <input type="submit" value="CARI" />
            </td>
        </tr>
    </table>
</form>

{if $mahasiswa}
<form action="ubah-status.php?{$smarty.server.QUERY_STRING}" method="post" id="form_add">
<table border="1">
		<tr>
			<th colspan="2">Status Mahasiswa</th>
		</tr>
        <tr>
            <td>NIM</td>
            <td><label>{$mahasiswa.NIM_MHS}</label>
            </td>
        </tr>
		<tr>
            <td>Nama</td><td>{$mahasiswa.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Tanggal Lahir</td><td>{$mahasiswa.TGL_LAHIR_PENGGUNA}</td>
        </tr>
        <tr>
            <td>Program Studi</td><td>{$mahasiswa.NM_PROGRAM_STUDI} - {$mahasiswa.NM_FAKULTAS}</td>
        </tr>
        <tr>
            <td>Angkatan</td><td>{$mahasiswa.THN_ANGKATAN_MHS}</td>
        </tr>
        <tr>
            <td>Status Akademik</td><td>{$mahasiswa.NM_STATUS_PENGGUNA}</td>
        </tr>
        <tr>    
            <td>History</td>
			<td>
				{if $cekal_mhs[0]['ID_MHS'] == ''}
					Belum pernah tercekal pembayaran
				{else}
				<table>
					<tr>
						<th>Status Cekal</th>
						<th>Semester</th>
						<th>Keterangan</th>
					</tr>
					{foreach $cekal_mhs as $data}
					<tr>
						<td>{if $data.CEKAL_UNIVERSITAS == 0}Boleh Bayar{else}Tidak Boleh Bayar{/if}</td>
						<td>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</td>
						<td>{$data.KETERANGAN}</td>
					</tr>
					{/foreach}
				</table>
				{/if}
			</td>
        </tr>
        <tr>
            <td>Status Cekal Pembayaran</td>
            <td>
                    <input type="hidden" name="mode" value="set_cekal" />
                    <input type="hidden" name="nim_mhs" value="{$mahasiswa.NIM_MHS}" />
					<select name="status_cekal">
						<option value="1" {if $mahasiswa.STATUS_CEKAL == 1} selected="selected"{/if}>CEKAL</option>
						<option value="0" {if $mahasiswa.STATUS_CEKAL == 0} selected="selected"{/if}>TIDAK CEKAL</option>
					</select>
            </td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td>
                    <textarea name="keterangan" cols="40" rows="5" class="required"></textarea>
          </td>
        </tr>
        <tr>
        	<td></td>
            <td><input type="submit" value="Simpan" /></td>
        </tr>
</table>
</form>


<!--<form action="ubah-status.php?{$smarty.server.QUERY_STRING}" method="post" id="form_stat">
<table>
		<tr>
			<th colspan="2">Status Akademik</th>
		</tr>
        <tr>
            <td>Ubah Status Akademik</td>
            <td>
               	 <input type="hidden" name="mode" value="set_status_akd" />
				 <input type="hidden" name="nim_mhs" value="{$mahasiswa.NIM_MHS}" />
				 	<select name="status_akd">
						{if $mahasiswa.STATUS_AKADEMIK_MHS == 1}
							<option value="{$mahasiswa.STATUS_AKADEMIK_MHS}">{$mahasiswa.NM_STATUS_PENGGUNA}</option>
						{else}
							{if $mahasiswa.AKTIF_STATUS_PENGGUNA == 1}
							<option value="{$mahasiswa.STATUS_AKADEMIK_MHS}">{$mahasiswa.NM_STATUS_PENGGUNA}</option>
							<option value="1">Aktif</option>
							{else}
							<option value="{$mahasiswa.STATUS_AKADEMIK_MHS}">{$mahasiswa.NM_STATUS_PENGGUNA}</option>
							{/if}
						{/if}
					</select>
            </td>
        </tr>
		<tr>
            <td>Keterangan</td>
            <td>
                    <textarea name="alasan" cols="40" rows="5" class="required" id="alasan"></textarea>
          </td>
        </tr>
        <tr>
        	<td></td>
            <td><input type="submit" value="Simpan" /></td>
        </tr>
</table>
</form>



<table>
		<tr>
			<th colspan="2">Status Fingerprint</th>
		</tr>
        <tr>
            <td>Status Fingerprint Mhs</td>
            <td>
                {if $mahasiswa.FINGER_DATA}SUDAH{else}BELUM{/if}
            </td>
        </tr>
		<tr>
            <td>Status Fingerprint C-Mhs</td>
            <td>
                {if $mahasiswa.FINGER_DATA3}SUDAH{else}BELUM{/if}
            </td>
        </tr>
        <tr>
            <td>Status Data Fingerprint</td>
            <td>{if $mahasiswa.TGL_LAHIR_PENGGUNA}
                    <form action="ubah-status.php?{$smarty.server.QUERY_STRING}" method="post">    
                    {if $mahasiswa.TGL_LAHIR}<label>SUDAH ADA</label>{else}<label>BELUM ADA</label>
                        <input type="hidden" name="mode" value="insert_fp" />
                        <input type="hidden" name="nim_mhs" value="{$mahasiswa.NIM_MHS}" />
                        <input type="submit" value="Insert" />
                    {/if}
                    </form>
                {else}
                    Disuruh melengkapi biodata
                {/if}
            </td>
        </tr>
</table>-->
{else if isset($smarty.request.nim_mhs)}
<h2><b>Bukan Mhs Aktif</b></h2>
{/if}
{literal}
    <script type="text/javascript">
            $('#form_add').validate();
			 $('#form_stat').validate();
    </script>
{/literal}