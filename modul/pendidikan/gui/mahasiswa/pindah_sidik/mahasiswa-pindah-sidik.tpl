<div class="center_title_bar">Pindah Sidik Mahasiswa</div>
<form id="form2" name="form2" method="post" action="mahasiswa-pindah-sidik.php">
    <table width="200" border="1">
      <tr>
        <td>NIM</td>
        <td><input type="text" name="nim"  class="required" /></td>
        <td><input type="submit" value="Cari" /></td>
      </tr>
    </table>
</form>

{if isset($mhs)}
<form id="form1" name="form1" method="post" action="mahasiswa-pindah-sidik.php">
  <table width="200" border="1">
    <tr>
      <th colspan="2" style="text-align:center">BIODATA</th>
    </tr>
    <tr>
      <td>Nama</td>
      <td>{$mhs.NM_PENGGUNA}</td>
    </tr>
    <tr>
      <td>Fakultas</td>
      <td>{$mhs.NM_FAKULTAS}</td>
    </tr>
    <tr>
      <td>Program Studi </td>
      <td>{$mhs.NM_JENJANG} - {$mhs.NM_PROGRAM_STUDI}</td>
    </tr>
	<tr>
      <td>NIM</td>
      <td>
	  	{$mhs.NIM_MHS}
	  	<input type="hidden" value="{$mhs.ID_MHS}" name="id_mhs" />
		<input type="hidden" value="{$mhs.NIM_MHS}" name="nim" />
		<input type="hidden" value="{$mhs.NIM_LAMA}" name="nim_lama" />
	  </td>
    </tr>
	<tr>
      <td>Sidik Jari </td>
      <td>{if $mhs.FINGER1 != ''}Sudah{else}Belum{/if}</td>
    </tr>
	<tr>
      <td>NIM LAMA</td>
      <td>
	  	{$mhs.NIM_LAMA}
	  </td>
    </tr>
	<tr>
      <td>Sidik Jari NIM LAMA</td>
      <td>{if $mhs.FINGER2 != ''}Sudah{else}Belum{/if}</td>
    </tr>
    <tr>
    	<td colspan="8" style="text-align:center">
        	<input type="submit" value="Pindah" />
        </td>
    </tr>
  </table>
</form>
{/if}

{literal}
<script language="text/javascript">
            $('#form2').validate();
</script>
{/literal}
