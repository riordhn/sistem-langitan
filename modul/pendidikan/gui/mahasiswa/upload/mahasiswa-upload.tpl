<div class="center_title_bar">Upload Data Mahasiswa</div>
<table>
        <tr>
            <th colspan="2">Upload File Excel</th>
        </tr>
        <tr>
            <td>
				<iframe src="upload-mahasiswa-iframe.php"></iframe>
            </td>
            <td>
                <label><a class="disable-ajax" href="includes/Template_Excel_Upload_Mahasiswa.xlsx">Download Template Excel</a></label>
				
				<p>Format susunan file excel :</p>
				<ul>
					<li>NIM : Nomer Induk Mahasiswa</li>
					<li>PRODI : Program Studi mahasiswanya. Contoh format program studi yang benar : "S1 Sistem Informasi". Pastikan sudah ada di master program studi</li>
					<li>JALUR : Jalur dari mahasiswa, harus dibuat masternya dulu di menu Master Perkuliahan &gt; Jalur</li>
					<li>ANGKATAN : tahun angkatan 4 digit. Contoh : 2015</li>
					<li>NAMA : Nama lengkap mahasiswa</li>
				</ul>
            </td>
			<td>
				
			</td>
        </tr>
</table>