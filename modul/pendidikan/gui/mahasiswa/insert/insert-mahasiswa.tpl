<div class="center_title_bar">Insert / Update Data Mahasiswa</div> 
<form method="post" action="insert-mahasiswa.php">
    <table>
        <tr>
            <td>NIM Mahasiswa</td>
            <td><input type="text" name="cari_nim" {if isset($cari_nim)}value="{$mahasiswa.NIM_MHS}"{/if}/></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if $pesan != ''}
    {$pesan}
{/if}

{if isset($cari_nim)}
    <form method="post" action="insert-mahasiswa.php" >
        <table>
            <tr>
                <th>Kolom</th>
                <th>Input</th>
            </tr>
             <tr>
                <td>NIK/NO KTP</td>
                <td><input type="text" size="30" name="nik" value="{$mahasiswa.NIK_MHS}"/></td>
            </tr>
            <tr>
                <td>NIM Mahasiswa</td>
                <!-- khusus untuk MABA dan yg belom punya pengambilan, edited : FIKRIE (29-06-2016) -->
                {* if $mahasiswa.JML_MATKUL == 0 && $mahasiswa.THN_ANGKATAN_MHS == $thn_sekarang *} {* Nanti diaktifkan lagi, sementara *}
				{if $mahasiswa.JML_MATKUL == 0}
                    <td><input type="text" size="14" name="nim" value="{if $mahasiswa.NIM_MHS==null}{$cari_nim}{else}{$mahasiswa.NIM_MHS}{/if}"/></td>
                    <td><input type="hidden" name="nim_lama" value="{$mahasiswa.NIM_MHS}"/></td>
                {else}
                    <td><input type="text" size="14" name="nim" readonly="true" value="{if $mahasiswa.NIM_MHS==null}{$cari_nim}{else}{$mahasiswa.NIM_MHS}{/if}"/></td>
                {/if}
            </tr>
            <tr>
                <td>Nama Mahasiswa</td>
                <td><input type="text" size="50" name="nama" value="{$mahasiswa.NM_PENGGUNA}"/></td>
            </tr>
            <tr>
                <td>Angkatan Akademik Semester</td>
                <td>
                    <select name="semester" id="semester">
                        <option></option>
                        {foreach $data_semester as $data}
                            {if $mahasiswa.ID_SEMESTER==null}
                                <option value="{$data.ID_SEMESTER}">{$data.THN_AKADEMIK_SEMESTER} ({$data.NM_SEMESTER})</option>
                            {else}
                                <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER==$mahasiswa.ID_SEMESTER}selected="true"{/if}>{$data.THN_AKADEMIK_SEMESTER} ({$data.NM_SEMESTER})</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Jenjang</td>
                <td>
                    <select name="jenjang" id="jenjang">
                        <option></option>
                        {foreach $data_jenjang as $data}
                            {if $mahasiswa.ID_JENJANG==null}
                                <option value="{$data.ID_JENJANG}">{$data.NM_JENJANG}</option>
                            {else}
                                <option value="{$data.ID_JENJANG}" {if $data.ID_JENJANG==$mahasiswa.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Program Studi</td>
                <td>
                    <select name="prodi" id="prodi">
                        <option></option>
                        {foreach $data_program_studi as $data}
                            {if $mahasiswa.ID_PROGRAM_STUDI==null}
                                
                            {else}
                                <option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI==$mahasiswa.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Jalur</td>
                <td>
                    <select name="jalur" id="jalur">
                        <option></option>
                        {foreach $data_jalur as $data}
                            {if $mahasiswa.ID_JALUR==null}
                                <option value="{$data.ID_JALUR}">{$data.NM_JALUR}</option>
                            {else}
                                <option value="{$data.ID_JALUR}" {if $data.ID_JALUR==$mahasiswa.ID_JALUR}selected="true"{/if}>{$data.NM_JALUR}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Tempat Lahir (Negara)</td>
                <td>
                    <select name="lahir_negara" id="lahir_negara">
                        <option value="">--- pilih negara ---</option>
                        {foreach $data_negara as $data}
                            {if $negara_lahir_mhs==null}
                                <option value="{$data.ID_NEGARA}">{$data.NM_NEGARA}</option>
                            {else}
                                <option value="{$data.ID_NEGARA}" {if $data.ID_NEGARA==$negara_lahir_mhs}selected="true"{/if}>{$data.NM_NEGARA}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Tempat Lahir (Propinsi)</td>
                <td>
                    <select name="lahir_propinsi" id="lahir_propinsi">
                        <option value="">--- pilih propinsi ---</option>
                        {foreach $data_propinsi as $data}
                            {if $mahasiswa.LAHIR_PROP_MHS==null}
                                
                            {else}
                                <option value="{$data.ID_PROVINSI}" {if $data.ID_PROVINSI==$mahasiswa.LAHIR_PROP_MHS}selected="true"{/if}>{$data.NM_PROVINSI}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Kota Lahir</td>
                <td>
                    <select name="lahir_kota" id="lahir_kota">
                        <option value="">--- pilih kota ---</option>
                        {foreach $data_kota as $data}
                            {if $mahasiswa.LAHIR_KOTA_MHS==null}
                                
                            {else}
                                <option value="{$data.ID_KOTA}" {if $data.ID_KOTA==$mahasiswa.LAHIR_KOTA_MHS}selected="true"{/if}>{$data.NM_KOTA} ({$data.TIPE_DATI2})</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
            </tr>
	        <tr>
                <td>Tanggal Lahir</td>
                <td><input type="text" id="tgl_lahir" name="tgl_lahir" value="{$mahasiswa.TGL_LAHIR_PENGGUNA}" class="required" /> Format: DD-MM-YYYY</td>
            </tr>
            <tr>
                <td>Email Pribadi Mahasiswa</td>
                <td><input type="text" size="50" id="email_alternate" name="email_alternate" value="{$mahasiswa.EMAIL_ALTERNATE}" /></td>
            </tr> 
            <tr>
                <td>Nama Ibu</td>
                <td><input type="text" size="50" id="nama_ibu" name="nama_ibu" value="{$mahasiswa.NM_IBU_MHS}" /></td>
            </tr>  
            <tr>
                <td>Alamat Jalan</td>
                <td><input type="text" size="50" id="alamat_jalan" name="alamat_jalan" value="{$mahasiswa.ALAMAT_ASAL_MHS}" /></td>
            </tr>   
            <tr>
                <td>Alamat Dusun</td>
                <td><input type="text" size="50" id="alamat_dusun" name="alamat_dusun" value="{$mahasiswa.ALAMAT_DUSUN_ASAL_MHS}" /></td>
            </tr>   
            <tr>
                <td>Alamat RT</td>
                <td><input type="number" size="15" id="alamat_rt" name="alamat_rt" value="{$mahasiswa.ALAMAT_RT_ASAL_MHS}" /></td>
            </tr>   
            <tr>
                <td>Alamat RW</td>
                <td><input type="number" size="15" id="alamat_rw" name="alamat_rw" value="{$mahasiswa.ALAMAT_RW_ASAL_MHS}" /></td>
            </tr>   
            <tr>
                <td>Alamat Kelurahan</td>
                <td><input type="text" size="50" id="alamat_kelurahan" name="alamat_kelurahan" value="{$mahasiswa.ALAMAT_KELURAHAN_ASAL_MHS}" /></td>
            </tr>   
            <tr>
                <td>Alamat Kode Pos</td>
                <td><input type="text" size="50" id="alamat_kodepos" name="alamat_kodepos" value="{$mahasiswa.ALAMAT_KODEPOS_ASAL}" /></td>
            </tr>   
            <tr>
                <td>Alamat Kecamatan</td>
                <td><input type="text" size="50" id="alamat_kecamatan" name="alamat_kecamatan" value="{$mahasiswa.ALAMAT_KECAMATAN_ASAL_MHS}" /></td>
            </tr>             
            <tr>
                <td><strong>SKS Diakui</strong>
                    <br/> *) Khusus Jalur Transfer/Pindahan</td>
                <td><input type="text" size="5" name="sks_diakui" value="{$mahasiswa.SKS_DIAKUI}"/></td>
            </tr>
            <tr>
                <td><strong>Perguruan Tinggi Asal</strong>
                    <br/> *) Khusus Jalur Transfer/Pindahan</td>
                <td>
                    <input type="text" name="nama_pt" placeholder="NPSN atau Nama Perguruan Tinggi" id="nama_pt" size="50" value="{if $mahasiswa.KODE_PT_ASAL == 'kosong'} {else}{$mahasiswa.NM_PT_ASAL}{/if}" />
                    <button id="cari_pt" type="button">Cari</button>
                </td>
            </tr>
            <tr>
                <td>Kode - Nama Perguruan Tinggi</td>
                <td>
                    <label id="kode-pt">{$mahasiswa.KODE_PT_ASAL} - {$mahasiswa.NM_PT_ASAL}</label>
                </td>
            </tr>
            <!-- <tr>
                <td><strong>Perguruan Tinggi Asal</strong>
                    <br/> *) Khusus Jalur Transfer/Pindahan</td>
                <td>
                    <select name="pt_asal" id="pt_asal">
                        <option></option>
                        {* {foreach $perguruan_tinggi_asal as $data} *}
                            <option value="{$data.NPSN}" {if $data.NPSN==$mahasiswa.KODE_PT_ASAL}selected="true"{/if}>{$data.NAMA_PERGURUAN_TINGGI}</option>
                        {* {/foreach} *}
                    </select>
                </td>
            </tr> -->
            <!-- <tr>
                <td><strong>Program Studi Asal</strong>
                    <br/> *) Khusus Jalur Transfer/Pindahan</td>
                <td>
                    <select name="prodi_asal" id="prodi_asal">
                        <option></option>
                        {foreach $program_studi_asal as $data}
                            {if $mahasiswa.KODE_PRODI_ASAL==null}
                                
                            {else}
                                <option value="{$data.KODE_PROGRAM_STUDI}" {if $data.KODE_PERGURUAN_TINGGI==$mahasiswa.KODE_PT_ASAL && $data.KODE_PROGRAM_STUDI==$mahasiswa.KODE_PRODI_ASAL}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
            </tr> -->
            <tr>
                <input type="hidden" name="pt_asal" value="" />
                {if $mahasiswa!=null}
                    <input type="hidden" name="update" value="1"/>
                    <td colspan="2"><input style="float: right;" type="submit" value="update" /></td>
                {else}
                    <input type="hidden" name="insert" value="1"/>
                    <td colspan="2"><input style="float: right;" type="submit" value="insert" /></td>
                {/if}
            </tr>
        </table>
    </form>
{/if}

{literal}
    <script type="text/javascript">

        

        /*$('#pt_asal').change(function(){
            $.ajax({
                type:'post',
                url:'getProdiAsal.php',
                data:'id_pt='+$(this).val(),
                success:function(data){
                    $('#prodi_asal').html(data);
                }                    
            })
        });*/

        $('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_jenjang='+$(this).val(),
                success:function(data){
                    $('#prodi').html(data);
                }                    
            })
        });

        $('#lahir_negara').change(function(){
            $.ajax({
                type:'post',
                url:'getPropinsi.php',
                data:'id_negara='+$(this).val(),
                success:function(data){
                    $('#lahir_propinsi').html(data);
                }                    
            })
        });

        $('#lahir_propinsi').change(function(){
            $.ajax({
                type:'post',
                url:'getKota.php',
                data:'id_propinsi='+$(this).val(),
                success:function(data){
                    $('#lahir_kota').html(data);
                }                    
            })
        });

    $(document).ready(function() {
        $('#cari_pt').click(function(e) {
            e.preventDefault();

            var nama_pt = $('#nama_pt').val();

            $.ajax({
                url: 'insert-mahasiswa.php?mode=cari-pt&nama_pt=' + nama_pt,
                dataType: 'json',
                beforeSend: function() {
                    $('#kode-pt').html('');
                    $('input[name="pt_asal"]').val('');
                }
            }).done(function(r) {
                $('#kode-pt').html(r.NAMA_PERGURUAN_TINGGI);

                if(r.NPSN === 'kosong'){
                    $('input[name="pt_asal"]').val();

                    document.getElementById('kode-pt').style.color = "red";
                    document.getElementById('kode-pt').style.fontWeight = "bold";
                }
                else{
                    $('input[name="pt_asal"]').val(r.NPSN);

                    document.getElementById('kode-pt').style.color = "black";
                    document.getElementById('kode-pt').style.fontWeight = "normal";
                }
            });

            return false;
        });

        /*$('#submit_button').on('click', function(e) {
            e.preventDefault();

            if ($('input[name="id_pengguna"]').val() === '')
            {
                alert('Harap memilih petugas terlebih dahulu');
                $('#nip').focus();
            }
            else
            {
                $('#form').submit();
            /*}

        });*/


    });
    </script>
{/literal}