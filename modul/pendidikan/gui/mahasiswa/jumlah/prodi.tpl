<div class="center_title_bar">Status Mahasiswa - Fakultas {$nm_fakultas}</div>
<h4>Semester : {$semester.TAHUN_AJARAN} {$semester.NM_SEMESTER}</h4>
<table style="width: 50%">
    <tr>
        <th>No</th>
        <th>Program Studi</th>
        <th>Aktif</th>
        <th>Non-Aktif</th>
    </tr>
	{$total_aktif=0}
	{$total_non_aktif=0}
    {foreach $program_studi_set as $ps}
    <tr>
        <td class="center">{$ps@index+1}</td>
        <td>{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</td>
        <td class="center">{$ps.JUMLAH_AKTIF}</td>
        <td class="center">{$ps.JUMLAH_NON_AKTIF}</td>
    </tr>
	{$total_aktif=$total_aktif+$ps.JUMLAH_AKTIF}
	{$total_non_aktif=$total_non_aktif+$ps.JUMLAH_NON_AKTIF}
    {/foreach}
	<tr>
		<td></td>
		<td>Jumlah</td>
		<td class="center">{$total_aktif}</td>
		<td class="center">{$total_non_aktif}</td>
	</tr>
</table>
<button href="mahasiswa-status.php?id_semester={$semester.ID_SEMESTER}">Kembali</button>