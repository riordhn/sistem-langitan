<div class="center_title_bar">Status Mahasiswa</div>

<table>
    <tr>
        <td>Tahun Ajaran</td>
        <td><form action="mahasiswa-status.php" method="get">
            <select name="id_semester">
			{foreach $semester_set as $s}
                <option value="{$s.ID_SEMESTER}" {if $s.ID_SEMESTER == $id_semester}selected="selected"{/if}>{$s.NM_SEMESTER} {$s.TAHUN_AJARAN} </option>
            {/foreach}
            </select>
            <input type="submit" value="Lihat" />
            </form>
        </td>
    </tr>
</table>

<table style="width: 50%">
    <tr>
        <th>No</th>
        <th>Fakultas</th>
        <th>Aktif</th>
        <th>Non-Aktif</th>
    </tr>
	{$total_aktif=0}
	{$total_non_aktif=0}
    {foreach $fakultas_set as $f}
    <tr>
        <td class="center">{$f@index+1}</td>
        <td><a href="mahasiswa-status.php?mode=prodi&id_fakultas={$f.ID_FAKULTAS}&id_semester={$id_semester}">{$f.NM_FAKULTAS}</a></td>
        <td class="center">{$f.JUMLAH_AKTIF}</td>
        <td class="center">{$f.JUMLAH_NON_AKTIF}</td>
    </tr>
	{$total_aktif=$total_aktif+$f.JUMLAH_AKTIF}
	{$total_non_aktif=$total_non_aktif+$f.JUMLAH_NON_AKTIF}
    {/foreach}
	<tr>
		<td></td>
		<td>Jumlah</td>
		<td class="center">{$total_aktif}</td>
		<td class="center">{$total_non_aktif}</td>
	</tr>
</table>