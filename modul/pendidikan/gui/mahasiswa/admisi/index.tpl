<div class="center_title_bar">History Admisi Mahasiswa</div>
{if !isset($mhs)}
<form id="form2" name="form2" method="get" action="mahasiswa-admisi.php">
    <table width="200" border="1">
        <tr>
            <td>NIM</td>
            <td><input type="text" name="nim" value="{if isset($smarty.get.nim)}{$smarty.get.nim}{/if}"/></td>
            <td><input type="submit" value="Cari" /></td>
        </tr>
    </table>
</form>
{/if}
{if isset($mhs)}
<table width="200" border="1">
    <tr>
        <th colspan="2" style="text-align:center">BIODATA</th>
    </tr>
    <tr>
        <td>NIM</td>
        <td>
            {$mhs.NIM_MHS}
            <input type="hidden" value="{$mhs.ID_MHS}" name="id_mhs" />
        </td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>{$mhs.NM_PENGGUNA}</td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>{$mhs.NM_FAKULTAS}</td>
    </tr>
    <tr>
        <td>Program Studi </td>
        <td>{$mhs.NM_JENJANG} {$mhs.NM_PROGRAM_STUDI}</td>
    </tr>
    <tr>
        <td>Angkatan </td>
        <td>{$mhs.THN_ANGKATAN_MHS}</td>
    </tr>
    <tr>
        <td>Status Akademik</td>
        <td>{$mhs.NM_STATUS_PENGGUNA}</td>
    </tr>
</table>
<table>
    <tr>
        <th>Semester</th>
        <th>Status</th>
        <th>Jalur Masuk</th>
        <th>No SK</th>
        <th>Tgl SK</th>
        <th>No Ijazah</th>
        <th>Tgl Keluar</th>
        <th>Ket.</th>
        <th>Aksi</th>
    </tr>
    {foreach $admisi as $data}
    <tr>
        <td>{$data.TAHUN_AJARAN} {$data.NM_SEMESTER}</td>
        <td>{$data.NM_STATUS_PENGGUNA}</td>
        <td>{$data.NM_JALUR}</td>
        <td>{$data.NO_SK}</td>
        <td>{$data.TGL_SK|date_format:"%d %B %Y"}</td>
        <td>{$data.NO_IJASAH}</td>
        <td>{$data.TGL_KELUAR|date_format:"%d %B %Y"}</td>
        <td>{$data.KETERANGAN}</td>
        <td>
            {if $data.NM_JALUR == '' and $data.STATUS_KELUAR == 0}
            <form action="mahasiswa-admisi.php" method="get">
                <input type="hidden" name="mode" value="edit" />
                <input type="hidden" name="id_admisi" value="{$data.ID_ADMISI}" />
                <input type="submit" value="Update" />
            </form>
            {/if}
            {if $data.STATUS_KELUAR == 1}
            <form action="mahasiswa-admisi.php" method="get">
                <input type="hidden" name="mode" value="delete" />
                <input type="hidden" name="id_admisi" value="{$data.ID_ADMISI}" />
                <input type="submit" value="Delete" />
            </form>
            {/if}
        </td>
    </tr>
    {/foreach}
    {* Jika mahasiswa belum keluar, bisa ditambahkan admisi baru*}
    {if $mhs.STATUS_KELUAR == 0}
    <tr>
        <td colspan="9">
            <form action="mahasiswa-admisi-add.php" method="get">
                <!-- <input type="hidden" name="mode" value="add" /> -->
                <input type="hidden" name="nim" value="{$mhs.NIM_MHS}" />
                <!-- <input type="submit" value="Pengajuan Cuti / Undur Diri / Drop Out / Lulus" /> -->
            </form>
        </td>
    </tr>
    {/if}
</table>
{/if}