<div class="center_title_bar">Admisi Mahasiswa</div>
<form id="form2" name="form2" method="get" action="mahasiswa-admisi-add.php">
    <table width="200" border="1">
		<tr>
			<td>NIM</td>
			<td><input type="text" name="nim" value="{if !empty($smarty.get.nim)}{$smarty.get.nim}{/if}"/></td>
			<td><input type="submit" value="Cari" /></td>
		</tr>
    </table>
</form>

{if isset($mhs)}
	<form id="form1" name="form1" method="post" action="mahasiswa-admisi-add.php">
		<table width="200" border="1">
			<tr>
				<th colspan="2" style="text-align:center">BIODATA</th>
			</tr>
			<tr>
				<td>NIM</td>
				<td>
					{$mhs.NIM_MHS}
					<input type="hidden" value="{$mhs.ID_MHS}" name="id_mhs" />
					<input type="hidden" value="{$mhs.NIM_MHS}" name="nim" />
					<input type="hidden" name="no_ijasah" value="" />
					<input type="hidden" name="no_sk" value="" />
					<input type="hidden" name="tgl_sk" value="" />
				</td>
			</tr>
			<tr>
				<td>Nama</td>
				<td>{$mhs.NM_PENGGUNA}</td>
			</tr>
			<tr>
				<td>Fakultas</td>
				<td>{$mhs.NM_FAKULTAS}</td>
			</tr>
			<tr>
				<td>Program Studi </td>
				<td>{$mhs.NM_JENJANG} {$mhs.NM_PROGRAM_STUDI}</td>
			</tr>
			<tr>
				<td>Status Akademik Saat Ini </td>
				<td>{$mhs.NM_STATUS_PENGGUNA}</td>
			</tr>
			<tr>
				<td>Status Akademik Baru </td>
				<td>
					<select name="status_pengguna" id="status_pengguna" class="required">
						<option value="">-- Pilih Status --</option>
						{foreach $status_pengguna as $data}
							<option value="{$data.ID_STATUS_PENGGUNA}">{$data.NM_STATUS_PENGGUNA}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<!-- <tr>
				<td>No Ijasah</td>
				<td><input type="text" name="no_ijasah" /></td>
			</tr> -->
			<tr>
				<td>Tgl Keluar</td>
				<td><input type="text" name="tgl_keluar" id="tgl_keluar" /></td>
			</tr>
			<!-- <tr>
				<td>No SK</td>
				<td><input type="text" name="no_sk" /></td>
			</tr>
			<tr>
				<td>Tgl SK </td>
				<td><input name="tgl_sk" type="text" id="tgl_sk"/></td>
			</tr> -->
			<tr>
				<td>Keterangan</td>
				<td><textarea name="keterangan" cols="30" rows="3"></textarea></td>
			</tr>
			<tr>
				<td>Semester</td>
				<td>
					<select name="semester" class="required">
						<option value="">-- Pilih Semester --</option>
						{foreach $semester as $data}
							<option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER == $semester_aktif} selected="selected"{/if}>
								{$data.THN_AKADEMIK_SEMESTER} ({$data.NM_SEMESTER})
							</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center"><input type="submit" value="Simpan" /></td>
			</tr>
		</table>
	</form>
{/if}

{literal}
<script language="javascript">
	$('#form1').validate();
	$("#tgl_sk").datepicker({dateFormat: 'dd-mm-yy', changeMonth: true, changeYear: true});
	$("#tgl_keluar").datepicker({dateFormat: 'dd-mm-yy', changeMonth: true, changeYear: true});
</script>
{/literal}