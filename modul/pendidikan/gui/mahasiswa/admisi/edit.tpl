<div class="center_title_bar">Edit History Admisi Mahasiswa</div>

<form id="form_edit" name="form_edit" method="post" action="mahasiswa-admisi.php?nim={$admisi.NIM_MHS}">
    <table>
        <thead>
            <tr>
                <th colspan="2">Detail Admisi Mahasiswa</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>NIM</td>
                <td>{$admisi.NIM_MHS}
                    <input type="hidden" value="update_admisi" name="mode" />
                    <input type="hidden" value="{$admisi.ID_ADMISI}" name="id_admisi" />
                </td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>{$admisi.NM_PENGGUNA}</td>
            </tr>
            <tr>
                <td>Program Studi</td>
                <td>{$admisi.NM_PROGRAM_STUDI}</td>
            </tr>
            <tr>
                <td>Angkatan</td>
                <td>{$admisi.THN_ANGKATAN_MHS}</td>
            </tr>
            <tr><td></td><td></td></tr>
            <tr>
                <td>Semester</td>
                <td>{$admisi.NM_SEMESTER} {$admisi.THN_AKADEMIK_SEMESTER}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>
                    <select name="status_akd_mhs">
                        {foreach $status_akademik_set as $sa}
                            <option value="{$sa.ID_STATUS_PENGGUNA}" {if $sa.ID_STATUS_PENGGUNA == $admisi.STATUS_AKD_MHS}selected{/if}>{$sa.NM_STATUS_PENGGUNA} {if $sa.STATUS_KELUAR == 1}(Keluar){/if}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td><textarea name="keterangan" cols="30" rows="3"></textarea></td>
            </tr>
            <tr>
                <td>No SK</td>
                <td></td>
            </tr>
            <tr>
                <td>Tanggal SK</td>
                <td></td>
            </tr>
            <tr>
                <td>No Ijazah</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center"><input type="submit" value="Update" /></td>
            </tr>
        </tbody>
    </table>
</form>


{literal}
<script language="javascript">
    $('#form_edit').validate();
</script>
{/literal}