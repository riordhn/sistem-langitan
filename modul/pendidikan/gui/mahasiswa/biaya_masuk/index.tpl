<div class="center_title_bar">Setting Biaya Masuk Mahasiswa</div>

<form method="post" action="mahasiswa-biaya_masuk.php">
<table>
    <tr>
        <td>Program Studi</td>
        <td>
            <select name="id_program_studi">
                {foreach $prodi_set as $prodi}
                    <option value="{$prodi.ID_PROGRAM_STUDI}">{$prodi.NM_JENJANG} {$prodi.NM_PROGRAM_STUDI}</option>
                {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Angkatan</td>
        <td>
            <input type="text" name="angkatan" size="4"/>
        </td>
    </tr>
    <tr>
        <td>Set Biaya</td>
        <td>
            <input type="text" name="biaya_masuk" min="0" value="0"/>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
    </tr>
</table>
</form>

{if isset($num_rows)}
    <p>Jumlah data berhasil diubah : {$num_rows}</p>
{/if}