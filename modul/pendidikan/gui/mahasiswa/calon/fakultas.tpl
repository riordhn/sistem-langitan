<div class="center_title_bar">Calon Mahasiswa Yang Diterima</div>

<h2>Tahun Penerimaan : 2011</h2>

<table style="width: 50%">
    <tr>
        <th>No</th>
        <th>Fakultas</th>
        <th>Jumlah</th>
    </tr>
    {foreach $fakultas_set as $f}
    <tr>
        <td class="center">{$f@index+1}</td>
        <td><a href="mahasiswa-calon.php?mode=prodi&id_fakultas={$f.ID_FAKULTAS}">{$f.NM_FAKULTAS}</a></td>
        <td class="center">{$f.JUMLAH_CALON}</td>
    </tr>
    {/foreach}
</table>