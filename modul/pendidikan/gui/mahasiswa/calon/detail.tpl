<div class="center_title_bar">Calon Mahasiswa Yang Diterima - Prodi {$nm_program_studi}</div>

<h2>Tahun Penerimaan 2011</h2>

<button href="mahasiswa-calon.php?mode=prodi&id_fakultas={$id_fakultas}">Kembali</button>
<table style="width: 80%">
    <tr>
        <th>No</th>
        <th>Nomer Ujian</th>
        <th>Jalur</th>
		<th>Nama Peserta</th>
		<th>NIM</th>
    </tr>
	{foreach $calon_mahasiswa_set as $cm}
	<tr>
		<td>{$cm@index+1}</td>
		<td>{$cm.NO_UJIAN}</td>
		<td>{$cm.NM_JALUR}</td>
		<td>{$cm.NM_C_MHS}</td>
		<td>{$cm.NIM_MHS}</td>
	</tr>
	{/foreach}
</table>

<button href="mahasiswa-calon.php?mode=prodi&id_fakultas={$id_fakultas}">Kembali</button>