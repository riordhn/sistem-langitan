<div class="center_title_bar">Calon Mahasiswa Yang Diterima - Fakultas {$nm_fakultas}</div>

<h2>Tahun Penerimaan 2011</h2>

<table style="width: 50%">
    <tr>
        <th>No</th>
        <th>Program Studi</th>
        <th>Jumlah</th>
    </tr>
	{$total=0}
    {foreach $program_studi_set as $ps}
    <tr>
        <td class="center">{$ps@index+1}</td>
        <td><a href="mahasiswa-calon.php?mode=detail&id_program_studi={$ps.ID_PROGRAM_STUDI}">{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</a></td>
        <td class="center">{$ps.JUMLAH_CALON}</td>
    </tr>
	{$total=$total+$ps.JUMLAH_CALON}
    {/foreach}
	<tr>
		<td></td>
		<td>Jumlah</td>
		<td class="center">{$total}</td>
	</tr>
</table>
<button href="mahasiswa-calon.php">Kembali</button>