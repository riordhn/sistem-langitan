{literal}
    <style>
        .span_button{
            padding: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            background-color: #009933;
            color: #ffffff;
            cursor: pointer;
        }
    </style>
{/literal}   <div class="center_title_bar">Report Mahasiswa</div>
{if isset($data_mahasiswa)}
    <table>
        <tr>
            <td colspan="8" style="border: none;padding: 5px 0px 15px 0px;">    
                <span class="span_button" onclick="history.back()">Kembali</span>
            </td>
        </tr>
        <tr>
            <th>NOMER</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>ANGKATAN</th>
            <th>JENJANG</th>
            <th>PROGRAM STUDI</th>
            <th>FAKULTAS</th>
            <th>STATUS</th>
        </tr>
        {$nomer=1}
        {foreach $data_mahasiswa as $data}
            <tr>
                <td>{$nomer++}</td>
                <td><a href="cari-mahasiswa.php?detail=ok&y={$data.NIM_MHS}">{$data.NIM_MHS}</a></td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>{$data.THN_ANGKATAN_MHS}</td>
                <td>{$data.NM_JENJANG}</td>
                <td>{$data.NM_PROGRAM_STUDI}</td>
                <td>{$data.NM_FAKULTAS}</td>
                <td>{$data.NM_STATUS_PENGGUNA}</td>
            </tr>
        {/foreach}
    </table>
{else}
    <div style="overflow: scroll;width: 800px;height: 600px;">
		{$view}
    </div>
    {literal}
        <script>
                $('.row_tahun').click(
                    function(){
                        if($(this).hasClass('clicked')){
                            $(this).css("background-color","#ffffff");
                            $(this).removeClass('clicked');
                        }else{
                            $(this).css("background-color","#ccffcc");
                            $(this).addClass('clicked');  }  
                });
        </script>
    {/literal}
{/if}