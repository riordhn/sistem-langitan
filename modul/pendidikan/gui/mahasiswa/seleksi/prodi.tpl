<div class="center_title_bar">Calon Mahasiswa Lulus Seleksi - Fakultas {$fakultas->NM_FAKULTAS}</div>

<table style="width: 50%">
    <tr>
        <th>No</th>
        <th>Program Studi</th>
        <th>Jumlah</th>
    </tr>
    {for $i=0 to $program_studi_set->Count()-1}
    {$program_studi=$program_studi_set->Get($i)}
    <tr>
        <td class="center">{$i+1}</td>
        <td>{$program_studi->NM_PROGRAM_STUDI}</td>
        <td class="center">{$program_studi->JUMLAH_CALON}</td>
    </tr>
    {/for}
</table>
<button href="mahasiswa-seleksi.php">Kembali</button>