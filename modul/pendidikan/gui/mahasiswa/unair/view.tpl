{literal}
    <style>
        .span_button {
            padding: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            background-color: #009933;
            color: #ffffff;
            cursor: pointer;
        }
    </style>
{/literal}
<div class="center_title_bar">Data-Data Mahasiswa</div>
<form action="data-mahasiswa.php" method="post">
    <table style="width: 95%">
        <tr>
            <th colspan="4">PARAMETER MAHASISWA</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select id="fakultas" name="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}"
                                {if $smarty.request.fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
            <td>Tahun Akademik Mahasiswa</td>
            <td>
                <select name="tahun_akademik">
                    <option value="">Semua</option>
                    {foreach $data_tahun_akademik as $data}
                        <option value="{$data.THN_AKADEMIK_SEMESTER}"
                                {if $smarty.request.tahun_akademik==$data.THN_AKADEMIK_SEMESTER}selected="true"{/if}>{$data.THN_AKADEMIK_SEMESTER}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Jenjang</td>
            <td>
                <select id="jenjang" name="jenjang">
                    <option value="">Semua</option>
                    {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}"
                                {if $smarty.request.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}
                </select>
            </td>
            <td>Status Mahasiswa</td>
            <td>
                <select id="status" name="status">
                    <option value="">Semua</option>
                    {foreach $data_status as $data}
                        <option value="{$data.ID_STATUS_PENGGUNA}"
                                {if $smarty.request.status==$data.ID_STATUS_PENGGUNA}selected="true"{/if}>{$data.NM_STATUS_PENGGUNA}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>
                <select id="program_studi" name="program_studi">
                    <option value="">Semua</option>
                    {foreach $data_program_studi as $data}
                        <option value="{$data.ID_PROGRAM_STUDI}"
                                {if $smarty.request.program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>
                            ({$data.NM_JENJANG}) {$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>

            <td>Jalur</td>
            <td><select id="jalur" name="jalur">
                    <option value="">Semua</option>
                    {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}"
                                {if $smarty.request.jalur==$data.ID_JALUR}selected="true"{/if}>{$data.NM_JALUR}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Alamat Provinsi</td>
            <td>
                <select id="provinsi" name="provinsi">
                    <option value="">Semua</option>
                    {foreach $data_provinsi as $data}
                        <option value="{$data.ID_PROVINSI}"
                                {if $smarty.request.provinsi==$data.ID_PROVINSI}selected="true"{/if}>{$data.NM_PROVINSI|upper}</option>
                    {/foreach}
                </select>
            </td>
            <td>Ajukan Bidikmisi</td>
            <td>
                <select name="bidikmisi">
                    <option value="">Semua</option>
                    <option value="100" {if $smarty.request.bidikmisi==100}selected="true"{/if}>Tidak</option>
                    <option value="1" {if $smarty.request.bidikmisi==1}selected="true"{/if}>Ya</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Alamat Kota</td>
            <td>
                <select id="kota" name="kota">
                    <option value="">Semua</option>
                </select>
            </td>
            <td>Tahun Lulus Sekolah</td>
            <td>
                <select name="tahun_lulus">
                    <option value="">Semua</option>
                    {foreach $data_tahun_lulus as $data}
                        <option value="{$data.TAHUN_LULUS}"
                                {if $smarty.request.tahun_lulus==$data.TAHUN_LULUS}selected="true"{/if}>{$data.TAHUN_LULUS}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <!-- 18 Feb 2019 Penambahan Filter Jenis Kelamin (Fatah)-->
        <tr>
            <td>Jenis Kelamin</td>
            <td>
                <select name="kelamin">
                    <option value="">Semua</option>
                    <option value="1000" {if $smarty.request.kelamin==1000}selected="true"{/if}>Belum diset</option>
                    <option value="1" {if $smarty.request.kelamin==1}selected="true"{/if}>Laki-laki</option>
                    <option value="2" {if $smarty.request.kelamin==2}selected="true"{/if}>Perempuan</option>
                </select>
            </td>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <td colspan="4" class="center"><input type="submit" value="Tampilkan"/></td>
        </tr>
        <input type="hidden" name="tampil" value="ok"/>
    </table>
</form>
{if isset($tampil)}
    <table>
        <tr>
            <th>NO</th>
            <th>NOMOR PENDAFTARAN</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>ANGKATAN</th>
            <th>JENIS KELAMIN</th>
            <th>PROGRAM STUDI</th>
            <th>FAKULTAS</th>
            <th>STATUS</th>
            <th>ASAL SEKOLAH</th>
            <th>ALAMAT</th>
            <th>TELP</th>
            <th>EMAIL</th>
            <th>JALUR</th>
        </tr>
        {$nomer=1}
        {foreach $data_mahasiswa as $data}
            <tr>
                <td>{$nomer++}</td>
                <td>{$data.KODE_VOUCHER}</td>
                <td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td>{$data.THN_ANGKATAN_MHS}</td>
                <td>
                    {if $data.KELAMIN_PENGGUNA == 1}Laki-laki{elseif $data.KELAMIN_PENGGUNA == 2}Perempuan{else}-{/if}
                </td>
                <td>{$data.NM_JENJANG} {$data.NM_PROGRAM_STUDI}</td>
                <td>{$data.NM_FAKULTAS}</td>
                <td>{$data.NM_STATUS_PENGGUNA}</td>
                <td>{$data.NM_SEKOLAH}</td>
                <td>
                    {$data.ALAMAT_ASAL_MHS}
                    {if isset($data.ALAMAT_RT_ASAL_MHS)}RT. {$data.ALAMAT_RT_ASAL_MHS} {/if}
                    {if isset($data.ALAMAT_RW_ASAL_MHS)}RW. {$data.ALAMAT_RW_ASAL_MHS} {/if}
                    {if isset($data.ALAMAT_DUSUN_ASAL_MHS)}Dusun {$data.ALAMAT_DUSUN_ASAL_MHS} {/if}
                    {if isset($data.ALAMAT_KELURAHAN_ASAL_MHS)}Kelurahan {$data.ALAMAT_KELURAHAN_ASAL_MHS}{/if}
                    {if isset($data.ALAMAT_KECAMATAN_ASAL_MHS)}Kecamatan {$data.ALAMAT_KECAMATAN_ASAL_MHS}{/if}
                    {if isset($data.ALAMAT_KODEPOS_ASAL)}, {$data.ALAMAT_KODEPOS_ASAL} {/if}
                    {if isset($data.ALAMAT_KOTA_MHS)}Kota {$data.ALAMAT_KOTA_MHS}{/if}
                    {if isset($data.ALAMAT_PROV_MHS)}, {$data.ALAMAT_PROV_MHS}{/if}
                </td>
                <td>{$data.MOBILE_MHS}</td>
                <td>{$data.EMAIL_ALTERNATE}</td>
                <td>{$data.NM_JALUR}</td>
                <!-- <td class="center">{if $data.CEK_KELENGKAPAN == 0}Tidak{else}Ya{/if}</td> -->
            </tr>
        {/foreach}
        <tr>
            <td colspan="20" class="center" style="padding: 10px;">
                <!-- <span class="span_button" onclick="window.open('{$link_ktm}');">Export PDF KTM</span> -->
                <span class="span_button" onclick="window.open('{$link_excel}');">Export Excel</span>
                <!-- <span class="span_button" onclick="window.open('{$link_pdf}');">Print PDF</span> -->
                <!-- <span class="span_button" onclick="get_link_report('excel')">Export Excel</span>
                <span class="span_button" onclick="get_link_report('pdf')">Print PDF</span> -->
            </td>
        </tr>
    </table>
{/if}
{literal}
    <script type="text/javascript">
        $('#fakultas').change(function () {
            $.ajax({
                type: 'post',
                url: 'getProdi.php',
                data: 'id_fakultas=' + $('#fakultas').val() + '&id_jenjang=' + $('#jenjang').val(),
                success: function (data) {
                    $('#program_studi').html(data);
                }
            })
        });

        $('#jenjang').change(function () {
            $.ajax({
                type: 'post',
                url: 'getProdi.php',
                data: 'id_fakultas=' + $('#fakultas').val() + '&id_jenjang=' + $('#jenjang').val(),
                success: function (data) {
                    $('#program_studi').html(data);
                }
            })
        });

        $('#provinsi').change(function () {
            $.ajax({
                type: 'post',
                url: 'getKota.php',
                data: 'id_provinsi=' + $('#provinsi').val(),
                success: function (data) {
                    $('#kota').html(data);
                }
            })
        });
    </script>
{/literal}

{literal}
    <!-- COBA MENJALANKAN SEND VALUE SMARTY KE JS -->
    <script type="text/javascript">
        function get_link_report(type) {

            var fak = {$id_fakultas};
            var prodi = {$id_prodi};
            var thn_akd = {$tahun_akademik};
            var stat = {$status};
            var jen = {$jenjang};
            var jal = {$jalur};
            var pt = {$id_pt};

            link = document.location.hash.replace('#mahasiswa-data_mahasiswa!', '');
            if (type == 'excel') {
                window.location.href = 'excel-' + link + '?fakultas=' + fak + '&program_studi=' + prodi + '&tahun_akademik=' + thn_akd + '&status=' + stat + '&jenjang=' + jen + '&jalur=' + jal + '&id_pt=' + pt;
                //window.location.href='excel-'+link;
            } else
                //window.open('cetak-'+link+'?fakultas='+fak+'&program_studi='+prodi+'&tahun_akademik='+thn_akd+'&status='+stat+'&jenjang='+jen+'&jalur='+jal+'&id_pt='+pt);
                window.open('cetak-' + link);
        }
    </script>
{/literal}