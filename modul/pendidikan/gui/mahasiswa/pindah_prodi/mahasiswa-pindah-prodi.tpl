<div class="center_title_bar">PINDAH PRODI MAHASISWA</div>
<form action="mahasiswa-pindah-prodi.php" method="get">
    <table style="width: 100%">
        <tr>
            <th colspan="4">PRODI MAHASISWA YANG AKAN DI PINDAH</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select id="fakultas" name="fakultas">
				<option value="">PILIH FAKULTAS</option> 
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
			<td>Jenjang</td>
			<td>
				<select id="jenjang" name="jenjang">
                    <option value="">PILIH JENJANG</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
		</tr>
		<tr>
			<td>Program Studi</td>
            <td>
                <select id="program_studi" name="program_studi">
                    <option value="">PILIH PRODI</option>
                    {foreach $data_program_studi as $data}
                        {if $program_studi!=''}
                            <option value="{$data.ID_PROGRAM_STUDI}" {if $program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
                        {else if $fakultas!=''&&$program_studi==''}
                            {if $data.ID_FAKULTAS==$fakultas}
                                <option value="{$data.ID_PROGRAM_STUDI}" >{$data.NM_PROGRAM_STUDI}</option>
                            {/if}
                        {/if}
                    {/foreach}
                </select>
            </td>
		</tr>
        <tr>
			<td>Jumlah Mahasiswa</td>
			<td><span id="jml_mhs">{$jml1}</span></td>
		</tr>
		<tr>
            <th colspan="4">PRODI TUJUAN</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select id="fakultas2" name="fakultas2">
				<option value="">PILIH FAKULTAS</option> 
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $fakultas2==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
			<td>Jenjang</td>
			<td>
				<select id="jenjang2" name="jenjang2">
                    <option value="">PILIH JENJANG</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $jenjang2==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
		</tr>
		<tr>
			<td>Program Studi</td>
            <td>
                <select id="program_studi2" name="program_studi2">
                    <option value="">PILIH PRODI</option>
                    {foreach $data_program_studi as $data}
                        {if $program_studi!=''}
                            <option value="{$data.ID_PROGRAM_STUDI}" {if $program_studi2==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
                        {else if $fakultas!=''&&$program_studi==''}
                            {if $data.ID_FAKULTAS==$fakultas}
                                <option value="{$data.ID_PROGRAM_STUDI}" >{$data.NM_PROGRAM_STUDI}</option>
                            {/if}
                        {/if}
                    {/foreach}
                </select>
            </td>
		</tr>
        <tr>
			<td>Jumlah Mahasiswa</td>
			<td><span id="jml_mhs2">{$jml2}</span></td>
		</tr>
		<tr>
            <td colspan="4" class="center"><input type="submit" value="Pindahkan"/></td>
        </tr>
        <input type="hidden" name="tampil" value="ok"/>
    </table>
</form>

{literal}
    <script type="text/javascript">
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#program_studi').change(function(){
            $.ajax({
                type:'post',
                url:'getMhs.php',
                data:'id_prodi='+$('#program_studi').val(),
                success:function(data){
                    $('#jml_mhs').html(data);
                }                    
            })
        });

	$('#fakultas2').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas2').val()+'&id_jenjang='+$('#jenjang2').val(),
                success:function(data){
                    $('#program_studi2').html(data);
                }                    
            })
        });
		
		$('#jenjang2').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas2').val()+'&id_jenjang='+$('#jenjang2').val(),
                success:function(data){
                    $('#program_studi2').html(data);
                }                    
            })
        });
		
		$('#program_studi2').change(function(){
            $.ajax({
                type:'post',
                url:'getMhs.php',
                data:'id_prodi='+$('#program_studi2').val(),
                success:function(data){
                    $('#jml_mhs2').html(data);
                }                    
            })
        });
    </script>
{/literal}