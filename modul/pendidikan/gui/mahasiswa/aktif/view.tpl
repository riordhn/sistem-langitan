<div class="center_title_bar">Rekap Data Mahasiswa Aktif</div>
 <div style="overflow: scroll;width: 800px;height: 600px;">
   <table>
    <tr>
		<th rowspan="3" style="text-align:center">
        	Fakultas		
		</th>
        <th rowspan="3" style="text-align:center">
        	Program Studi		
		</th>
		<th colspan="{count($data_jenjang)*2}" style="text-align:center">
        	Jenjang
		</th>
        <th scope="col" style="text-align: center" rowspan="2" colspan="2">Σ Per Prodi</th>
    </tr>
	<tr>
	{foreach $data_jenjang as $data}
      <th colspan="2" style="text-align:center">{$data.NM_JENJANG}</th>
	{/foreach}
    </tr>
    <tr>
    {foreach $data_jenjang as $data}
      <th>Laki</th>
      <th>Perempuan</th>
    {/foreach}
    	<th>Laki</th>
        <th>Perempuan</th>
    </tr>
	
    {foreach $jml as $data}
	<tr>
    	<td>{$data.NM_FAKULTAS}</td>
        <td>{$data.NM_PROGRAM_STUDI}</td>
        {$jml_laki = 0}
        {$jml_wanita = 0}
        
        {foreach $data_jenjang as $jenjang}
          
          <td>
          	{if $data.ID_JENJANG == $jenjang.ID_JENJANG}
          		{$data.LAKI}
                {$jml_laki = $jml_laki + $data.LAKI}
            {/if}
          </td>
          <td>
	        {if $data.ID_JENJANG == $jenjang.ID_JENJANG}
          		{$data.WANITA}
                {$jml_wanita = $jml_wanita + $data.WANITA}
          	{/if}
          </td>
          
        {/foreach}
        <td>{$jml_laki}</td>
        <td>{$jml_wanita}</td>
        {$total_laki = $total_laki + $jml_laki}
        {$total_wanita = $total_wanita + $jml_wanita}
    </tr>
    {/foreach}
	<tr>
    	<td colspan="2">&Sigma; Per Jenjang</td>
        {foreach $data_jenjang as $jenjang}
       	 	<td></td>
            <td></td>
        {/foreach}
        	<td>{$total_laki}</td>
            <td>{$total_wanita}</td>
    </tr>        
   </table>
 </div>
