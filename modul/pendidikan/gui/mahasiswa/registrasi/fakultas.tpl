<div class="center_title_bar">Calon Mahasiswa Yg Registrasi</div>

<table style="width: 50%">
    <tr>
        <th>No</th>
        <th>Fakultas</th>
        <th>Jumlah</th>
    </tr>
    {for $i=0 to $fakultas_set->Count()-1}
    {$fakultas=$fakultas_set->Get($i)}
    <tr>
        <td class="center">{$i+1}</td>
        <td><a href="mahasiswa-registrasi.php?mode=prodi&id_fakultas={$fakultas->ID_FAKULTAS}">{$fakultas->NM_FAKULTAS}</a></td>
        <td class="center">{$fakultas->JUMLAH_CALON}</td>
    </tr>
    {/for}
</table>