{literal}
    <style>
        .span_button{
            padding: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            background-color: #009933;
            color: #ffffff;
            cursor: pointer;
        }
    </style>
{/literal}

<div class="center_title_bar">Cari Mahasiswa</div>

<form action="cari-mahasiswa.php" method="get">
        <table>
            <tr>
                <td>NAMA / NIM MAHASISWA / No Ujian</td>
                <td><input type="text" name="x" value="{if !empty($smarty.get.x)}{$smarty.get.x}{else if !empty($smarty.get.y)}{$smarty.get.y}{/if}"/></td>
				<td><input type="submit" value="Cari" /></td>
            </tr>
        </table>
    </form>

{if isset($data_mahasiswa)}
    <table>
        <tr>
			<th>NO</th>
			<th>NIM</th>
            <th>NAMA</th>
	    	<th>NO UJIAN</th>
            <th>FAKULTAS</th>
            <th>PROGRAM STUDI</th>
			<th>STATUS AKADEMIK</th>
			<th>JALUR</th>
            <!-- <th>JALUR AKTIF</th> -->
            <th></th>
        </tr>
        {if $data_mahasiswa==null}
            <tr>
                <td colspan="8" class="center">Hasil Pencarian Tidak Ditemukan</td>
            </tr>
        {else}
            {foreach $data_mahasiswa as $data}
                <tr>
					<td>{$data@index + 1}</td>
					<td>{$data.NIM_MHS}</td>
                    <td>{$data.NM_PENGGUNA}</td>
		    		<td>{$data.NO_UJIAN}</td>
                    <td>{$data.NM_FAKULTAS}</td>
                    <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
					<td>{$data.NM_STATUS_PENGGUNA}</td>
					<td>{$data.NM_JALUR}</td>
                    <!-- <td>{$data.STATUS_JALUR}</td> -->
                    <td><a href="cari-mahasiswa.php?detail=ok&y={$data.NIM_MHS}">Detail</a></td>
                </tr>
            {/foreach}
        {/if}
    </table>
	<a href="cari-mahasiswa.php" class="button">Kembali</a>
{else if isset($biodata_mahasiswa)}

    <span class="span_button" onclick="history.back()">Kembali</span>

    <table>
        
        <tr>
            <td style="vertical-align: top">

                <table style="margin: 0; width: 100%;">
                    <tr>
                        <th colspan="2" class="center">BIODATA MAHASISWA</th>
                    </tr>
                    <tr>
                        <td colspan="2" class="center" style="vertical-align: top">
                            <img src="/foto_mhs/{$nama_singkat}/{$biodata_mahasiswa.NIM_MHS}.jpg" width="180" height="230"/>
                            <form action="cari-mahasiswa.php?{$smarty.server.QUERY_STRING}" method="post">
                            <input type="hidden" name="mode" value="ubahPassword" />
                            <input type="hidden" name="id_pengguna" value="{$biodata_mahasiswa.ID_PENGGUNA}" />
                            <input type="hidden" name="username" value="{$biodata_mahasiswa.USERNAME}" />
                            <table>
                                <tr>
                                    <th colspan="2" class="center">Reset Password?</th>
                                    <td colspan="2" class="center"></td>
                                    <th colspan="2" class="center">Cetak Biodata</th>
                                </tr>
                                <tr>
                                    <td colspan="2" class="center">
                                        <input type="submit" value="Reset" />
                                    </td>
                                    <td colspan="2" class="center">
                                    </td>
                                    <td colspan="2" class="center">
                                        <input type="button" name="cetak" value="Cetak Biodata" onclick="window.open('{$LINK}', 'baru');" />
                                    </td>
                                </tr>
                            </table>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td>NAMA</td>
                        <td>{$biodata_mahasiswa.NM_PENGGUNA}</td>
                    </tr>
                    <tr>
                        <td>NIM</td>
                        <td>{$biodata_mahasiswa.NIM_MHS}</td>
                    </tr>
                    <tr>
                        <td>NO. PENDAFTARAN / UJIAN</td>
                        <td>{$biodata_mahasiswa.NO_UJIAN}</td>
                    </tr>
                    <tr>
                        <td>ANGKATAN</td>
                        <td>{$biodata_mahasiswa.THN_ANGKATAN_MHS}</td>
                    </tr>
                    <tr>
                        <td>JENJANG</td>
                        <td>{$biodata_mahasiswa.NM_JENJANG}</td>
                    </tr>
                    <tr>
                        <td>FAKULTAS</td>
                        <td>{$biodata_mahasiswa.NM_FAKULTAS|upper}</td>
                    </tr>
                    <tr>
                        <td>PROGRAM STUDI</td>
                        <td>{$biodata_mahasiswa.NM_PROGRAM_STUDI}</td>
                    </tr>
                    {* {if $biodata_mahasiswa.ID_C_MHS_PARAM == ''} *}
                        <tr>
                            <td>TEMPAT/TANGGAL LAHIR</td>
                            <td>{$biodata_mahasiswa.NM_KOTA} / {$biodata_mahasiswa.TGL_LAHIR_PENGGUNA}</td>
                        </tr>
                        <tr>
                            <td>NIK</td>
                            <td>{$biodata_mahasiswa.NIK_MHS}</td>
                        </tr>
                        <tr>
                            <td>TELP</td>
                            <td>{$biodata_mahasiswa.MOBILE_MHS}</td>
                        </tr>
                        <tr>
                            <td>KELAMIN</td>
                            <td>
                                {if $biodata_mahasiswa.KELAMIN_PENGGUNA==1}
                                    Laki-laki
                                {else if $biodata_mahasiswa.KELAMIN_PENGGUNA==2}
                                    Perempuan
                                {else}
                                    -
                                {/if}
                            </td>
                        </tr>
                        <tr>
                            <td>EMAIL</td>
                            <td>{$biodata_mahasiswa.EMAIL_PENGGUNA}</td>
                        </tr>
                        <tr>
                            <td>EMAIL PRIBADI MHS</td>
                            <td>{$biodata_mahasiswa.EMAIL_ALTERNATE}</td>
                        </tr>
                        <tr>
                            <td>ALAMAT MAHASISWA</td>
                            <td>{$biodata_mahasiswa.ALAMAT_ASAL_MHS} 
                            {if isset($biodata_mahasiswa.ALAMAT_RT_ASAL_MHS)}RT. {$biodata_mahasiswa.ALAMAT_RT_ASAL_MHS} {/if} 
                            {if isset($biodata_mahasiswa.ALAMAT_RW_ASAL_MHS)}RW. {$biodata_mahasiswa.ALAMAT_RW_ASAL_MHS} {/if} 
                            {if isset($biodata_mahasiswa.ALAMAT_DUSUN_ASAL_MHS)}Dusun {$biodata_mahasiswa.ALAMAT_DUSUN_ASAL_MHS} {/if} 
                            {if isset($biodata_mahasiswa.ALAMAT_KELURAHAN_ASAL_MHS)}Kelurahan {$biodata_mahasiswa.ALAMAT_KELURAHAN_ASAL_MHS}{/if} 
                            {if isset($biodata_mahasiswa.ALAMAT_KECAMATAN_ASAL_MHS)}Kecamatan {$biodata_mahasiswa.ALAMAT_KECAMATAN_ASAL_MHS}{/if} 
                            {if isset($biodata_mahasiswa.ALAMAT_KODEPOS_ASAL)}, {$biodata_mahasiswa.ALAMAT_KODEPOS_ASAL} {/if}
                            {if isset($biodata_mahasiswa.ALAMAT_KOTA_MHS)}Kota {$biodata_mahasiswa.ALAMAT_KOTA_MHS}{/if}
                            {if isset($biodata_mahasiswa.ALAMAT_PROV_MHS)}, {$biodata_mahasiswa.ALAMAT_PROV_MHS}{/if}</td>
                        </tr>
                        <tr>
                            <td>NAMA AYAH</td>
                            <td>{$biodata_mahasiswa.NM_AYAH_MHS}</td>
                        </tr>
                        <tr>
                            <td>ALAMAT AYAH</td>
                            <td>{$biodata_mahasiswa.ALAMAT_AYAH_MHS}</td>
                        </tr>
                        <tr>
                            <td>TELP AYAH</td>
                            <td>{$biodata_mahasiswa.TELP_AYAH}</td>
                        </tr>
                        <tr>
                            <td>PENGHASILAN AYAH</td>
                            <td>{$biodata_mahasiswa.PENGHASILAN_AYAH_MHS}</td>
                        </tr>
                        <tr>
                            <td>NAMA IBU</td>
                            <td>{$biodata_mahasiswa.NM_IBU_MHS}</td>
                        </tr>
                        <tr>
                            <td>ALAMAT IBU</td>
                            <td>{$biodata_mahasiswa.ALAMAT_IBU_MHS}</td>
                        </tr>
                        <tr>
                            <td>TELP IBU</td>
                            <td>{$biodata_mahasiswa.TELP_IBU}</td>
                        </tr>
                        <tr>
                            <td>PENGHASILAN IBU</td>
                            <td>{$biodata_mahasiswa.PENGHASILAN_IBU_MHS}</td>
                        </tr>
                    {* {else} *}
                        <!-- <tr>
                            <td>TEMPAT/TANGGAL LAHIR</td>
                            <td>{$biodata_mahasiswa.NM_KOTA_LAHIR_CMB} / {$biodata_mahasiswa.TGL_LAHIR_PENGGUNA}</td>
                        </tr>
                        <tr>
                            <td>TELP</td>
                            <td>{$biodata_mahasiswa.TELP_MHS_CMB}</td>
                        </tr>
                        <tr>
                            <td>KELAMIN</td>
                            <td>
                                {if $biodata_mahasiswa.KELAMIN_PENGGUNA==1}
                                    Laki-laki
                                {else if $biodata_mahasiswa.KELAMIN_PENGGUNA==2}
                                    Perempuan
                                {else}
                                    -
                                {/if}
                            </td>
                        </tr>
                        <tr>
                            <td>EMAIL</td>
                            <td>{$biodata_mahasiswa.EMAIL_MHS_CMB}</td>
                        </tr>
                        <tr>
                            <td>ALAMAT MAHASISWA</td>
                            <td>{$biodata_mahasiswa.ALAMAT_MHS_CMB}</td>
                        </tr>
                        <tr>
                            <td>NAMA AYAH</td>
                            <td>{$biodata_mahasiswa.NAMA_AYAH}</td>
                        </tr>
                        <tr>
                            <td>ALAMAT AYAH</td>
                            <td>{$biodata_mahasiswa.ALAMAT_AYAH} {$biodata_mahasiswa.KOTA_AYAH_CMB}</td>
                        </tr>
                        <tr>
                            <td>TELP AYAH</td>
                            <td>{$biodata_mahasiswa.TELP_AYAH}</td>
                        </tr>
                        <tr>
                            <td>NAMA IBU</td>
                            <td>{$biodata_mahasiswa.NAMA_IBU}</td>
                        </tr>
                        <tr>
                            <td>ALAMAT IBU</td>
                            <td>{$biodata_mahasiswa.ALAMAT_IBU} {$biodata_mahasiswa.KOTA_IBU_CMB}</td>
                        </tr>
                        <tr>
                            <td>TELP IBU</td>
                            <td>{$biodata_mahasiswa.TELP_IBU}</td>
                        </tr>
                        <tr>
                            <td>PENGHASILAN ORTU</td>
                            <td>Ayah : {$biodata_mahasiswa.PENGHASILAN_AYAH}, Ibu : {$biodata_mahasiswa.PENGHASILAN_IBU}</td>
                        </tr> -->
                    {* {/if} *}
                </table>

                <h3>Admisi Status</h3>

                <table>
                    <tr>
                        <th>Semester</th>
                        <th>Status</th>
                        <th>Jalur Masuk</th>
                        <th>No SK</th>
                        <th>Tgl SK</th>
                        <th>No Ijazah</th>
                        <th>Tgl Keluar</th>
                        <th>Ket.</th>
                    </tr>
                    {foreach $admisi_mahasiswa as $data}
                    <tr>
                        <td>{$data.TAHUN_AJARAN} {$data.NM_SEMESTER}</td>
                        <td>{$data.NM_STATUS_PENGGUNA}</td>
                        <td>{$data.NM_JALUR}</td>
                        <td>{$data.NO_SK}</td>
                        <td>{$data.TGL_SK|date_format:"%d %B %Y"}</td>
                        <td>{$data.NO_IJASAH}</td>
                        <td>{$data.TGL_KELUAR|date_format:"%d %B %Y"}</td>
                        <td>{$data.KETERANGAN}</td>
                    </tr>
                    {/foreach}
                </table>

                <h3>Aktivitas</h3>

                <table>
                    <tr>
                        <th>Semester</th>
                        <th>Status</th>
                        <th>SKS Semester</th>
                        <th>IPS</th>
                        <th>SKS Total</th>
                        <th>IPK</th>
                    </tr>
                    {foreach $aktivitas_mahasiswa as $status}
                    <tr>
                        <td>{$status.TAHUN_AJARAN} {$status.NM_SEMESTER}</td>
                        <td>{$status.NM_STATUS_PENGGUNA}</td>
                        <td style="text-align: center">{$status.SKS_SEMESTER}</td>
                        <td style="text-align: center">{if $status.IPS != ''}{$status.IPS|floatval}{/if}</td>
                        <td style="text-align: center">{$status.SKS_TOTAL}</td>
                        <td style="text-align: center">{$status.IPK|floatval}</td>
                    </tr>
                    {/foreach}
                </table>
                <!-- PENAMBAHAN UNTUK MENAMPILKAN JUMLAH TAGIHAN MAHASISWA DENGAN TABEL KEUANGAN_KHS -->

                
                <h3>HISTORI TAGIHAN MAHASISWA</h3>
                <table>
                    <tr>
                        <th>Semester</th>                     
                        <th>JUMLAH TAGIHAN MAHASISWA</th> 
                        <th>Per Tanggal</th> 
                                             
                    </tr>
                    {foreach $tagihan_mahasiswa as $tagihan}
                    <tr> 
                        <td>{$tagihan.TAHUN_AJARAN} {$tagihan.NM_SEMESTER}</td>
                        {if $tagihan.JUMLAH_TAGIHAN == '1'}
                            <td>Drop Out</td>
                        {elseif $tagihan.JUMLAH_TAGIHAN == '2'}
                            <td>Mengundurkan Diri</td>
                        {else}
                            <td>Rp{number_format($tagihan.JUMLAH_TAGIHAN)}</td>
                        {/if} 
                        <td>{$tagihan.TANGGAL} {$tagihan.WAKTU}</td>                  
                    </tr>
                    {/foreach}
                    <tr></tr>
                </table>

            </td>
            <td style="vertical-align: top">

                {* Data Akademik *}
                {foreach $khs_mahasiswa as $semester}
                    <h3>{$semester['NM_SEMESTER']} {$semester['TAHUN_AJARAN']}</h3>

                    <table>
                        <tr>
                            <th>No</th>
                            <th>Kode</th>
                            <th>Mata Kuliah</th>
                            <th>SKS</th>
                            <th>Angka</th>
                            <th>Nilai</th>
                        </tr>
                        {$total_sks = 0}
                        {foreach $semester['pengambilan_set'] as $pengambilan}
                        <tr>
                            <td style="text-align: center">{$pengambilan@index + 1}</td>
                            <td>{$pengambilan.KD_MATA_KULIAH}</td>
                            <td>{$pengambilan.NM_MATA_KULIAH}</td>
                            <td style="text-align: center">{$pengambilan.KREDIT_SEMESTER}</td>
                            <td style="text-align: center">{$pengambilan.NILAI_ANGKA}</td>
                            <td style="text-align: center">{$pengambilan.NILAI_HURUF}</td>
                        </tr>
                        {$total_sks = $total_sks + $pengambilan.KREDIT_SEMESTER}
                        {/foreach}
                        <tr>
                            <td colspan="3"></td>
                            <td style="text-align: center">{$total_sks}</td>
                            <td colspan="2"></td>
                        </tr>
                    </table>
                {/foreach}

            </td>
        </tr>
    </table>

    
 
    
{/if}
