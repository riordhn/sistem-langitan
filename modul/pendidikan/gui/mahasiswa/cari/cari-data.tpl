<div class="center_title_bar">Cari Data Mahasiswa</div> 
<form method="post" action="cari-data.php">
    <table>
        <tr>
            <td>NIM Mahasiswa</td>
            <td><input type="text" name="nim" {if isset($nim)}value="{$nim}"{/if}/></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if isset($data_mhs)}
    <table>
        <tr>
            <th>Kolom</th>
            <th>Data</th>
        </tr>
        <tr>
            <td>NIM</td>
            <td>{$data_mhs.nim}</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>{$data_mhs.nama}</td>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>{$data_mhs.nm_fakulta}</td>
        </tr>
        <tr>
            <td>Prodi</td>
            <td>{$data_mhs.nm_prodi}</td>
        </tr>
        <tr>
            <td>Prodi Lengkap</td>
            <td>{$data_mhs.nama_lengkap}</td>
        </tr>
        <tr>
            <td>Jenjang</td>
            <td>{$data_mhs.nm_jenjang}</td>
        </tr>
        <tr>
            <td>Jalur</td>
            <td>{$data_mhs.nm_jalur}</td>
        </tr>
        <tr>
            <td>Program</td>
            <td>{$data_mhs.nm_program}</td>
        </tr>
        <tr>
            <th>Jenis Biaya</th>
            <th>Besar Biaya</th>
        </tr>
        <tr>
            <td>SPP</td>
            <td>{$data_mhs.spp}</td>
        </tr>
        <tr>
            <td>Asuransi</td>
            <td>{$data_mhs.asuransi}</td>
        </tr>
        <tr>
            <td>Praktikum</td>
            <td>{$data_mhs.praktikum}</td>
        </tr>
        <tr>
            <td>PKL</td>
            <td>{$data_mhs.pkl}</td>
        </tr>        
        <tr>
            <td>KKN</td>
            <td>{$data_mhs.kkn}</td>
        </tr>
        <tr>
            <td>Wisuda</td>
            <td>{$data_mhs.wisuda}</td>
        </tr>
        <tr>
            <td><b>Total</b></td>
            <td>{$data_mhs.spp+$data_mhs.asuransi+$data_mhs.praktikum+$data_mhs.pkl+$data_mhs.kkn+$data_mhs.wisuda}</td>
        </tr>
    </table>
{else if isset($data_mhs_kosong)}
    <span>Data Mahasiswa tidak ada</span>
{/if}