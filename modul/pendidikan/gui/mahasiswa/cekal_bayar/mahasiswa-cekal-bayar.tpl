<div class="center_title_bar">PROSES CEKAL PEMBAYARAN MAHASISWA</div>

<form action="mahasiswa-cekal-bayar.php" method="post" id="f2">
<table>
		<tr>
		<td>Semester</td>
            <td>
            	<select name="semester" class="required">
                	<option value="">Pilih Semester</option>
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $smarty.request.semester==$data.ID_SEMESTER} selected="selected" {/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>


	
    	<td style="text-align:center">
			<input type="submit" value="Tampil" />
		</td>
    </tr>
    <tr>
    	<td colspan="4">* Pilih semester yang sesuai dengan semester evaluasi penetapan</td>
    </tr>
</table>

</form>


{if isset($cekal)}

<table>
	<tr>
		<td style="text-align:center" colspan="12">
        <form action="mahasiswa-cekal-bayar.php" method="post" id="f1">
			<input type="submit" value="Proses Cekal Pembayaran" />
			<input type="hidden" value="proses_cekal" name="mode" />
            <input type="hidden" name="semester" value="{$smarty.request.semester}" />
         </form>
		</td>
	</tr>
	<tr>
		<th>NO</th>
		<th>NIM</th>
		<th>NAMA</th>
		<th>FAKULTAS</th>
		<th>PROGRAM STUDI</th>
		<th>STATUS AKADEMIK</th>
		<th>STATUS CEKAL</th>
	</tr>
	{$no=1}
	{foreach $cekal as $data}
	<tr>
		<td>{$no++}</td>
		<td>{$data.NIM_MHS}</td>
		<td>{$data.NM_PENGGUNA}</td>
		<td>{$data.NM_FAKULTAS}</td>
		<td>{$data.NM_PROGRAM_STUDI} ({$data.NM_JENJANG})</td>
		<td>{$data.NM_STATUS_PENGGUNA}</td>
		<td>{$data.CEKAL}</td>
	</tr>
	{/foreach}
	
</table>


{/if}

{literal}
    <script>
			$("#f2").validate();
    </script>
{/literal}