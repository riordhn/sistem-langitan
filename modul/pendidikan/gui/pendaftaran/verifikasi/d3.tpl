{literal}

<style type="text/css">
.center         { text-align: center; }
.formulir       { width: 100% }
.formulir tr td { font-size: 13px; }
.formulir tr td h2 { margin: 0px; }
.formulir tr td h3 { margin: 0px; }
.formulir tr td h4 { margin: 0px; }
.formulir tr td.head { font-size: 18px; font-weight: bold; background: #233D0E; color: #fff; }
label.error        { display: none; color: #f00; font-size: 12px; }

/* BUTTON */
.button{-moz-border-radius:5px;-webkit-border-radius:5px;background:#777 url(button.png) repeat-x bottom;border:none;border-radius:5px;color:#fff;cursor:pointer;display:inline;font-weight:700;padding:5px 10px;text-shadow:1px 1px #666}
.button:hover{background-position:0 -48px}
.button:active{background-position:0 top;padding:6px 10px 4px;position:relative;top:1px}
.button.red{background-color:#e50000}
.button.purple{background-color:#9400bf}
.button.green{background-color:#58aa00}
.button.orange{background-color:#ff9c00}
.button.blue{background-color:#2c6da0}
.button.black{background-color:#333}
.button.white{background-color:#fff;color:#000;text-shadow:1px 1px #fff}
.button.small{font-size:75%;padding:3px 7px}
.button.small:hover{background-position:0 -50px}
.button.small:active{background-position:0 top;padding:4px 7px 2px}
.button.large{font-size:125%;padding:7px 12px}
.button.large:hover{background-position:0 -35px}
.button.large:active{background-position:0 top;padding:8px 12px 6px}
/* END BUTTON */


</style>

 {/literal}

<form action="pendaftaran-verifikasi.php{if !empty($smarty.get.no_ujian)}?no_ujian={$smarty.get.no_ujian}{/if}" method="post" id="verifikasi" enctype="multipart/form-data">
    <input type="hidden" name="mode" value="save_d3" />
    <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
    
    <table style="width: 100%; margin-left: 0px" class="formulir">
        <tr>
            <td colspan="2" class="head">I. Program Studi</td>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>{$ps.NM_FAKULTAS}</td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>{$ps.NM_JENJANG} - {$ps.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td colspan="2" class="head">II. Data Diri</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td style="font-size: 16px; font-weight: bold;">{$cmb.NIM_MHS}</td>
        </tr>
        <tr>
            <td>No Ujian</td>
            <td style="font-size: 16px; font-weight: bold;">{$cmb.NO_UJIAN}</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>
                <input type="text" name="nm_c_mhs" maxlength="64" size="50" class="required" value="{$cmb.NM_C_MHS}"/>
                <br/>
                <label for="nm_c_mhs" class="error" style="display:none">* Isi nama lengkap</label>
            </td>
        </tr>
        <tr>
            <td>Tempat dan Tanggal Lahir</td>
            <td>
            	<table style="width:100%;height:100%;border-bottom-width:0px">
                	<tr>
                    	<td width="25%">Negara</td>
                        <td>
                        	<select name="id_negara_lahir" id="id_negara_lahir" class="required">
                                <option value=""></option>
                                {foreach $negara_set as $k}  
                                  <option value="{$k.ID_NEGARA}" {if $k.ID_NEGARA == $cmb.ID_NEGARA_LAHIR}selected="selected"{/if}>{$k.NM_NEGARA}</option>
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                    <tr>
                    	<td>Provinsi</td>
                        <td>
                        <select name="id_provinsi_lahir" id="id_provinsi_lahir" class="required">
                            <option value=""></option>
                            {foreach $provinsi_lahir_set as $k}  
                              <option value="{$k.ID_PROVINSI}" {if $k.ID_PROVINSI == $cmb.ID_PROVINSI_LAHIR}selected="selected"{/if}>{$k.NM_PROVINSI}</option>
                            {/foreach}
                        </select>
                        </td>
                    </tr>
                    <tr>
                    	<td>Kota</td>
                        <td>
                        	<select name="id_kota_lahir" id="id_kota_lahir" class="required">
                                <option value=""></option>
                                {foreach $kota_lahir_set as $k}  
                                  <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $cmb.ID_KOTA_LAHIR}selected="selected"{/if}>{$k.NM_KOTA} ({$k.TIPE_DATI2})</option>
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                    <tr>
                    	<td>Tanggal Lahir</td>
                        <td><input type="text" id="tgl_lahir" name="tgl_lahir" size="10" class="required" value="{$cmb.TGL_LAHIR}" /></td>
                    </tr>
                </table>
				
            </td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>
                <textarea name="alamat" maxlength="250" cols="50" class="required">{$cmb.ALAMAT}</textarea>
            </td>
        </tr>
        <tr>
            <td>Negara</td>
            <td>
            	<select name="id_negara" id="id_negara" class="required">
                    <option value=""></option>
                    {foreach $negara_set as $k}  
                      <option value="{$k.ID_NEGARA}" {if $k.ID_NEGARA == $cmb.ID_NEGARA}selected="selected"{/if}>{$k.NM_NEGARA}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Provinsi</td>
            <td>
            	<select name="id_provinsi" id="id_provinsi" class="required">
                    <option value=""></option>
                    {foreach $provinsi_set as $k}  
                      <option value="{$k.ID_PROVINSI}" {if $k.ID_PROVINSI == $cmb.ID_PROVINSI}selected="selected"{/if}>{$k.NM_PROVINSI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Kota</td>
            <td>
            	<select name="id_kota" id="id_kota" class="required">
                    <option value=""></option>
                    {foreach $kota_set as $k}  
                      <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $cmb.ID_KOTA}selected="selected"{/if}>{$k.NM_KOTA} ({$k.TIPE_DATI2})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>No Telp</td>
            <td><input type="text" name="telp" class="required" maxlength="32" value="{$cmb.TELP}"/></td>
        </tr>
        <tr>
            <td>Email</td>            
            <td><input type="text" name="email" class="required email" maxlength="50" size="50" value="{$cmb.EMAIL}" /></td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>
                <label><input type="radio" name="jenis_kelamin" value="1" class="required" {if $cmb.JENIS_KELAMIN==1}checked="checked"{/if} >Laki-Laki</label>
                <label><input type="radio" name="jenis_kelamin" value="2" {if $cmb.JENIS_KELAMIN==2}checked="checked"{/if}>Perempuan</label>
                <br/>
                <label for="jenis_kelamin" class="error" style="display: none;">Pilih salah satu</label>
        </tr>
        <tr>
        <td>Kewarganegaraan</td>
        <td>
			<select name="kewarganegaraan">
			{foreach $kewarganegaraan_set as $kw}
				<option value="{$kw.ID_KEWARGANEGARAAN}" {if $cmb.KEWARGANEGARAAN == $kw.ID_KEWARGANEGARAAN}selected="selected"{/if}>{$kw.NM_KEWARGANEGARAAN}</option>
			{/foreach}
			</select>
        </td>
    </tr>
    <tr>
        <td>Agama</td>
        <td>
			<select name="id_agama">
			{foreach $agama_set as $a}
				<option value="{$a.ID_AGAMA}" {if $cmb.ID_AGAMA == $a.ID_AGAMA}selected="selected"{/if}>{$a.NM_AGAMA}</option>
			{/foreach}
			</select>
		</td>
    </tr>
        <tr>
            <td>Sumber Biaya</td>
            <td>
            
            	<select name="sumber_biaya" class="required">
			{foreach $sumber_biaya_set as $sb}
				<option value="{$sb.ID_SUMBER_BIAYA}" {if $cmb.SUMBER_BIAYA == $sb.ID_SUMBER_BIAYA}selected="selected"{/if}>{$sb.NM_SUMBER_BIAYA}</option>
			{/foreach}
			</select>
                <br/>
                <label for="sumber_biaya" class="error" style="display:none;">Pilih salah satu</label>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="head">III. Data Pendidikan</td>
        </tr>
            <tr>
        <td>Asal SMTA / MA</td>
        <td>{$cmb.NM_SEKOLAH_ASAL}
        <input type="hidden" name="id_sekolah_asal" value="{$cmb.ID_SEKOLAH_ASAL}" />
        </td>
    </tr>
    <tr>
        <td>Jurusan SMTA / MA</td>
        <td>
			<select name="jurusan_sekolah">
			{foreach $jurusan_sekolah_set as $js}
				<option value="{$js.ID_JURUSAN_SEKOLAH}" {if $cmb.JURUSAN_SEKOLAH == $js.ID_JURUSAN_SEKOLAH}selected="selected"{/if}>{$js.NM_JURUSAN_SEKOLAH}</option>
			{/foreach}
			</select>
		</td>
    </tr>
    <tr>
        <td>No &amp; Tgl Ijazah SMTA</td>
        <td> No Ijazah:
            <input type="text" name="no_ijazah"  maxlength="40" value="{$cmb.NO_IJAZAH}" />
            <br/>
            Tgl Ijazah: {html_select_date prefix="tgl_ijazah_" day_empty="" month_empty="" year_empty="" field_order="DMY" start_year="-15" time=$cmb.TGL_IJAZAH}
        </td>
    </tr>
    <tr>
        <td>Ijazah</td>
        <td> Tahun :
            <input type="text" name="tahun_lulus" size="4" maxlength="4" value="{$cmb.TAHUN_LULUS}"/>
            <br/>
            Jumlah Mata Pelajaran :
            <input type="text" name="jumlah_pelajaran_ijazah" size="2" maxlength="2" value="{$cmb.JUMLAH_PELAJARAN_IJAZAH}"/>
            <br/>
            Jumlah Nilai Akhir :
            <input type="text" name="nilai_ijazah" size="5" maxlength="5" value="{$cmb.NILAI_IJAZAH}" /> 
            * Bukan rata-rata</td>
    </tr>
    <tr>
        <td>UAN</td>
        <td> Tahun :
            <input type="text" name="tahun_uan" size="4" maxlength="4" value="{$cmb.TAHUN_UAN}"/>
            <br/>
            Jumlah Mata Pelajaran :
            <input type="text" name="jumlah_pelajaran_uan" size="2" maxlength="2" value="{$cmb.JUMLAH_PELAJARAN_UAN}"/>
            <br/>
            Jumlah Nilai UAN :
            <input type="text" name="nilai_uan" size="5" maxlength="5" value="{$cmb.NILAI_UAN}"/>
            * Bukan rata-rata</td>
    </tr>
    <tr>
            <td colspan="2" class="head">IV. Data Keluarga - Ayah</td>
     </tr>
    <tr>
        <td>Nama Ayah</td>
        <td><input type="text" name="nama_ayah" maxlength="64"  value="{$cmb.NAMA_AYAH}"/></td>
    </tr>
    <tr>
        <td>Alamat Ayah</td>
        <td><input type="text" name="alamat_ayah" size="65" maxlength="200" value="{$cmb.ALAMAT_AYAH}" /></td>
    </tr>
	   <tr>
            <td>Negara</td>
            <td>
            	<select name="id_negara_ayah" id="id_negara_ayah" class="required">
                    <option value=""></option>
                    {foreach $negara_set as $k}  
                      <option value="{$k.ID_NEGARA}" {if $k.ID_NEGARA == $cmb.ID_NEGARA_AYAH}selected="selected"{/if}>{$k.NM_NEGARA}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Provinsi</td>
            <td>
            	<select name="id_provinsi_ayah" id="id_provinsi_ayah" class="required">
                    <option value=""></option>
                    {foreach $provinsi_ayah_set as $k}  
                      <option value="{$k.ID_PROVINSI}" {if $k.ID_PROVINSI == $cmb.ID_PROVINSI_AYAH}selected="selected"{/if}>{$k.NM_PROVINSI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Kota</td>
            <td>
            	<select name="id_kota_ayah" id="id_kota_ayah" class="required">
                    <option value=""></option>
                    {foreach $kota_ayah_set as $k}  
                      <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $cmb.ID_KOTA_AYAH}selected="selected"{/if}>{$k.NM_KOTA} ({$k.TIPE_DATI2})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
    <tr>
        <td>Telp</td>
        <td><input type="text" name="telp_ayah"  maxlength="32" value="{$cmb.TELP_AYAH}"/></td>
    </tr>
    <tr>
        <td>Pendidikan Ayah</td>
        <td>
            <select name="pendidikan_ayah">
            {foreach $pendidikan_ortu_set as $po}
                <option value="{$po.ID_PENDIDIKAN_ORTU}" {if $cmb.PENDIDIKAN_AYAH == $po.ID_PENDIDIKAN_ORTU}selected="selected"{/if}>{$po.NM_PENDIDIKAN_ORTU}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Pekerjaan Ayah</td>
        <td>
            <select name="pekerjaan_ayah">
            {foreach $pekerjaan_set as $p}
                <option value="{$p.ID_PEKERJAAN}" {if $cmb.PEKERJAAN_AYAH == $p.ID_PEKERJAAN}selected="selected"{/if}>{$p.NM_PEKERJAAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Instansi / Perusahaan Tempat Kerja</td>
        <td><input type="text" name="instansi_ayah" size="30" maxlength="50" value="{$cmb.INSTANSI_AYAH}" /></td>
    </tr>
    <tr>
        <td>Jabatan / Golongan</td>
        <td><input type="text" name="jabatan_ayah" size="30" maxlength="50" value="{$cmb.JABATAN_AYAH}" /></td>
    </tr>
    <tr>
        <td>Masa Kerja (jika ada)</td>
        <td><input type="text" name="masa_kerja_ayah" size="4" maxlength="4" value="{$cmb.MASA_KERJA_AYAH}" /></td>
    </tr>
    <tr>
            <td colspan="2" class="head">V. Data Keluarga - Ibu</td>
     </tr>
    <tr>
        <td>Nama Ibu </td>
        <td><input type="text" name="nama_ibu" maxlength="64"  value="{$cmb.NAMA_IBU}"/></td>
    </tr>
    <tr>
        <td>Alamat Ibu</td>
        <td><input type="text" name="alamat_ibu" size="65" maxlength="200" value="{$cmb.ALAMAT_IBU}" /></td>
    </tr>
    	   <tr>
            <td>Negara</td>
            <td>
            	<select name="id_negara_ibu" id="id_negara_ibu" class="required">
                    <option value=""></option>
                    {foreach $negara_set as $k}  
                      <option value="{$k.ID_NEGARA}" {if $k.ID_NEGARA == $cmb.ID_NEGARA_IBU}selected="selected"{/if}>{$k.NM_NEGARA}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Provinsi</td>
            <td>
            	<select name="id_provinsi_ibu" id="id_provinsi_ibu" class="required">
                    <option value=""></option>
                    {foreach $provinsi_ibu_set as $k}  
                      <option value="{$k.ID_PROVINSI}" {if $k.ID_PROVINSI == $cmb.ID_PROVINSI_IBU}selected="selected"{/if}>{$k.NM_PROVINSI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Kota</td>
            <td>
            	<select name="id_kota_ibu" id="id_kota_ibu" class="required">
                    <option value=""></option>
                    {foreach $kota_ibu_set as $k}  
                      <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $cmb.ID_KOTA_IBU}selected="selected"{/if}>{$k.NM_KOTA} ({$k.TIPE_DATI2})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
    <tr>
        <td>Telp</td>
        <td><input type="text" name="telp_ibu"  maxlength="32" value="{$cmb.TELP_IBU}"/></td>
    </tr>
    <tr>
        <td>Pendidikan Ibu</td>
        <td>
            <select name="pendidikan_ibu">
            {foreach $pendidikan_ortu_set as $po}
                <option value="{$po.ID_PENDIDIKAN_ORTU}" {if $cmb.PENDIDIKAN_IBU == $po.ID_PENDIDIKAN_ORTU}selected="selected"{/if}>{$po.NM_PENDIDIKAN_ORTU}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Pekerjaan Ibu</td>
        <td>
            <select name="pekerjaan_ibu">
            {foreach $pekerjaan_set as $p}
                <option value="{$p.ID_PEKERJAAN}" {if $cmb.PEKERJAAN_IBU == $p.ID_PEKERJAAN}selected="selected"{/if}>{$p.NM_PEKERJAAN}</option>
            {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Instansi / Perusahaan Tempat Kerja</td>
        <td><input type="text" name="instansi_ibu" size="30" maxlength="50" value="{$cmb.INSTANSI_IBU}" /></td>
    </tr>
    <tr>
        <td>Jabatan / Golongan</td>
        <td><input type="text" name="jabatan_ibu" size="30" maxlength="50" value="{$cmb.JABATAN_IBU}" /></td>
    </tr>
    <tr>
        <td>Masa Kerja (jika ada)</td>
        <td><input type="text" name="masa_kerja_ibu" size="4" maxlength="4" value="{$cmb.MASA_KERJA_IBU}" /></td>
    </tr>
    
	    <tr>
            <td colspan="2" class="head">V. Upload File Berkas</td>
        </tr>
        
            <tr>
        <td>File Ijazah / Surat Keterangan Lulus</td>
        <td>
            <input type="hidden" id="status_file_ijazah" {if $cmb.FILE_IJAZAH}value="1"{/if} />
            {if $cmb.FILE_IJAZAH}<a target="_blank" href="pendaftaran-verifikasi.php?no_ujian={$cmb.NO_UJIAN}" onclick="window.open('{$base_url}files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_ijazah.pdf');">{$cmb.ID_C_MHS}_ijazah.pdf</a>{/if}
            <input type="file" accept="application/pdf" name="file_ijazah"/>
        </td>
    </tr>
    <tr>
        <td>File SKHUN<label class="small-text">Bisa menyusul</label>
        </td>
        <td>
            <input type="hidden" id="status_file_skhun" {if $cmb.FILE_SKHUN}value="1"{/if} />
            {if $cmb.FILE_SKHUN}<a target="_blank" href="pendaftaran-verifikasi.php?no_ujian={$cmb.NO_UJIAN}" onclick="window.open('{$base_url}files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_skhun.pdf');">{$cmb.ID_C_MHS}_skhun.pdf</a>{/if}
            <input type="file" accept="application/pdf" name="file_skhun"/>
        </td>
    </tr>
    <tr>
        <td>File Akte Kelahiran</td>
        <td>
            <input type="hidden" id="status_file_akte" {if $cmb.FILE_AKTE}value="1"{/if} />
            {if $cmb.FILE_AKTE}<a target="_blank" href="pendaftaran-verifikasi.php?no_ujian={$cmb.NO_UJIAN}" onclick="window.open('{$base_url}files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_akte.pdf');">{$cmb.ID_C_MHS}_akte.pdf</a>{/if}
            <input type="file" accept="application/pdf" name="file_akte"/>
        </td>
    </tr>
    <tr>
        <td>File KK</td>
        <td>
            <input type="hidden" id="status_file_kk" {if $cmb.FILE_KK}value="1"{/if} />
            {if $cmb.FILE_KK}<a target="_blank" href="pendaftaran-verifikasi.php?no_ujian={$cmb.NO_UJIAN}" onclick="window.open('{$base_url}files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_kk.pdf');">{$cmb.ID_C_MHS}_kk.pdf</a>{/if}
            <input type="file" accept="application/pdf" name="file_kk"/>
        </td>
    </tr>

        <tr>
            <td colspan="2" class="head">VI. Berkas</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <label><input type="checkbox" name="berkas_ijazah" value="1" {if $cmd.BERKAS_IJAZAH==1}checked="checked"{/if}/>Ijazah / Surat Keterangan Lulus (Asli dan Foto copy)</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <label><input type="checkbox" name="berkas_skhun" value="1" {if $cmd.BERKAS_SKHUN==1}checked="checked"{/if}/>SKHUN</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <label><input type="checkbox" name="berkas_akte" value="1" {if $cmd.BERKAS_AKTA==1}checked="checked"{/if}/>Akte Kelahiran</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <label><input type="checkbox" name="berkas_ksk" value="1" {if $cmd.BERKAS_KSK==1}checked="checked"{/if}/>KK / KSK</label>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
            <input type="hidden" name="id_pilihan_1" value="{$cmb.ID_PILIHAN_1}" />
                <input type="hidden" name="id_pilihan_2" value="{$cmb.ID_PILIHAN_2}" />
                <input type="hidden" name="id_pilihan_3" value="{$cmb.ID_PILIHAN_3}" />
                <input type="hidden" name="id_pilihan_4" value="{$cmb.ID_PILIHAN_4}" />
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>

                
{literal}
<script type="text/javascript">
		$('#id_negara').change(function(){
            $.ajax({
                type:'post',
                url:'getNegara.php',
                data:'id_negara='+$('#id_negara').val(),
                success:function(data){
                    $('#id_provinsi').html(data);
                }                    
            })
			
     });
	 
	 $('#id_provinsi').change(function(){
            $.ajax({
                type:'post',
                url:'getKota.php',
                data:'id_provinsi='+$('#id_provinsi').val(),
                success:function(data){
                    $('#id_kota').html(data);
                }                    
            })
     });
	 
	 
	$('#id_negara_lahir').change(function(){
            $.ajax({
                type:'post',
                url:'getNegara.php',
                data:'id_negara='+$('#id_negara_lahir').val(),
                success:function(data){
                    $('#id_provinsi_lahir').html(data);
                }                    
            })
			
     });
	 
	 $('#id_provinsi_lahir').change(function(){
            $.ajax({
                type:'post',
                url:'getKota.php',
                data:'id_provinsi='+$('#id_provinsi_lahir').val(),
                success:function(data){
                    $('#id_kota_lahir').html(data);
                }                    
            })
     });


	 $('#id_negara_ayah').change(function(){
            $.ajax({
                type:'post',
                url:'getNegara.php',
                data:'id_negara='+$('#id_negara_ayah').val(),
                success:function(data){
                    $('#id_provinsi_ayah').html(data);
                }                    
            })
			
     });
	 
	 $('#id_provinsi_ayah').change(function(){
            $.ajax({
                type:'post',
                url:'getKota.php',
                data:'id_provinsi='+$('#id_provinsi_ayah').val(),
                success:function(data){
                    $('#id_kota_ayah').html(data);
                }                    
            })
     });
	 
	 $('#id_negara_ibu').change(function(){
            $.ajax({
                type:'post',
                url:'getNegara.php',
                data:'id_negara='+$('#id_negara_ibu').val(),
                success:function(data){
                    $('#id_provinsi_ibu').html(data);
                }                    
            })
			
     });
	 
	 $('#id_provinsi_ibu').change(function(){
            $.ajax({
                type:'post',
                url:'getKota.php',
                data:'id_provinsi='+$('#id_provinsi_ibu').val(),
                success:function(data){
                    $('#id_kota_ibu').html(data);
                }                    
            })
     });
	 
	$("#tgl_lahir").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});

    $('#verifikasi').validate();
</script>
{/literal}