{literal}<style type="text/css">
.center         { text-align: center; }
.formulir       { width: 100% }
.formulir tr td { font-size: 13px; }
.formulir tr td h2 { margin: 0px; }
.formulir tr td h3 { margin: 0px; }
.formulir tr td h4 { margin: 0px; }
.formulir tr td.head { font-size: 18px; font-weight: bold; background: #233D0E; color: #fff; }
label.error        { display: none; color: #f00; font-size: 12px; }

/* BUTTON */
.button{-moz-border-radius:5px;-webkit-border-radius:5px;background:#777 url(button.png) repeat-x bottom;border:none;border-radius:5px;color:#fff;cursor:pointer;display:inline;font-weight:700;padding:5px 10px;text-shadow:1px 1px #666}
.button:hover{background-position:0 -48px}
.button:active{background-position:0 top;padding:6px 10px 4px;position:relative;top:1px}
.button.red{background-color:#e50000}
.button.purple{background-color:#9400bf}
.button.green{background-color:#58aa00}
.button.orange{background-color:#ff9c00}
.button.blue{background-color:#2c6da0}
.button.black{background-color:#333}
.button.white{background-color:#fff;color:#000;text-shadow:1px 1px #fff}
.button.small{font-size:75%;padding:3px 7px}
.button.small:hover{background-position:0 -50px}
.button.small:active{background-position:0 top;padding:4px 7px 2px}
.button.large{font-size:125%;padding:7px 12px}
.button.large:hover{background-position:0 -35px}
.button.large:active{background-position:0 top;padding:8px 12px 6px}
/* END BUTTON */
</style>{/literal}

<form action="pendaftaran-verifikasi.php{if !empty($smarty.get.no_ujian)}?no_ujian={$smarty.get.no_ujian}{/if}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="mode" value="save_s1_aj" />
    <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
    
    <table style="width: 100%; margin-left: 0px" class="formulir">
        <tr>
            <td colspan="2" class="head">I. Program Studi</td>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>{$ps.NM_FAKULTAS}</td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>{$ps.NM_JENJANG} - {$ps.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td colspan="2" class="head">II. Data Diri</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td style="font-size: 16px; font-weight: bold;">{$cmb.NIM_MHS}</td>
        </tr>
        <tr>
            <td>No Ujian</td>
            <td style="font-size: 16px; font-weight: bold;">{$cmb.NO_UJIAN}</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>
                <input type="text" name="nm_c_mhs" maxlength="64" size="50" class="required" value="{$cmb.NM_C_MHS}"/>
                <br/>
                <label for="nm_c_mhs" class="error" style="display:none">* Isi nama lengkap</label>
            </td>
        </tr>
        <tr>
            <td>Tempat dan Tanggal Lahir</td>
            <td>
            	<table style="width:100%;height:100%;border-bottom-width:0px">
                	<tr>
                    	<td width="25%">Negara</td>
                        <td>
                        	<select name="id_negara_lahir" id="id_negara_lahir" class="required">
                                <option value=""></option>
                                {foreach $negara_set as $k}  
                                  <option value="{$k.ID_NEGARA}" {if $k.ID_NEGARA == $cmb.ID_NEGARA_LAHIR}selected="selected"{/if}>{$k.NM_NEGARA}</option>
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                    <tr>
                    	<td>Provinsi</td>
                        <td>
                        <select name="id_provinsi_lahir" id="id_provinsi_lahir" class="required">
                            <option value=""></option>
                            {foreach $provinsi_lahir_set as $k}  
                              <option value="{$k.ID_PROVINSI}" {if $k.ID_PROVINSI == $cmb.ID_PROVINSI_LAHIR}selected="selected"{/if}>{$k.NM_PROVINSI}</option>
                            {/foreach}
                        </select>
                        </td>
                    </tr>
                    <tr>
                    	<td>Kota</td>
                        <td>
                        	<select name="id_kota_lahir" id="id_kota_lahir" class="required">
                                <option value=""></option>
                                {foreach $kota_lahir_set as $k}  
                                  <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $cmb.ID_KOTA_LAHIR}selected="selected"{/if}>{$k.NM_KOTA} ({$k.TIPE_DATI2})</option>
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                    <tr>
                    	<td>Tanggal Lahir</td>
                        <td><input type="text" id="tgl_lahir" name="tgl_lahir" size="10" class="required" value="{$cmb.TGL_LAHIR}" /></td>
                    </tr>
                </table>
				
            </td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>
                <textarea name="alamat" maxlength="250" cols="50" class="required">{$cmb.ALAMAT}</textarea>
            </td>
        </tr>
        <tr>
            <td>Negara</td>
            <td>
            	<select name="id_negara" id="id_negara" class="required">
                    <option value=""></option>
                    {foreach $negara_set as $k}  
                      <option value="{$k.ID_NEGARA}" {if $k.ID_NEGARA == $cmb.ID_NEGARA}selected="selected"{/if}>{$k.NM_NEGARA}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Provinsi</td>
            <td>
            	<select name="id_provinsi" id="id_provinsi" class="required">
                    <option value=""></option>
                    {foreach $provinsi_set as $k}  
                      <option value="{$k.ID_PROVINSI}" {if $k.ID_PROVINSI == $cmb.ID_PROVINSI}selected="selected"{/if}>{$k.NM_PROVINSI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Kota</td>
            <td>
            	<select name="id_kota" id="id_kota" class="required">
                    <option value=""></option>
                    {foreach $kota_set as $k}  
                      <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $cmb.ID_KOTA}selected="selected"{/if}>{$k.NM_KOTA} ({$k.TIPE_DATI2})</option>
                    {/foreach}
                </select>
            </td>
        </tr>

        <tr>
            <td>No Telp</td>
            <td><input type="text" name="telp" class="required" maxlength="32" value="{$cmb.TELP}"/></td>
        </tr>
        <tr>
            <td>Email</td>            
            <td><input type="text" name="email" class="required email" maxlength="50" size="50" value="{$cmb.EMAIL}" /></td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>
                <label><input type="radio" name="jenis_kelamin" value="1" class="required" {if $cmb.JENIS_KELAMIN==1}checked="checked"{/if} >Laki-Laki</label>
                <label><input type="radio" name="jenis_kelamin" value="2" {if $cmb.JENIS_KELAMIN==2}checked="checked"{/if}>Perempuan</label>
                <br/>
                <label for="jenis_kelamin" class="error" style="display: none;">Pilih salah satu</label>
        </tr>
        <tr>
            <td>Kewarganegaraan</td>
            <td>
                <label><input type="radio" name="kewarganegaraan" value="1" class="required" {if $cmb.KEWARGANEGARAAN==1}checked="checked"{/if} />Indonesia</label>
                <label><input type="radio" name="kewarganegaraan" value="2" {if $cmb.KEWARGANEGARAAN==2}checked="checked"{/if}/>W.N.A. <input type="text" name="kewarganegaraan_lain" maxlength="20" value="{$cmb.KEWARGANEGARAAN_LAIN}" /><label for="kewarganegaraan_lain" class="error" style="display: none;">Harus di isi</label></label>
                <br/>
                <label for="kewarganegaraan" class="error" style="display: none;">Pilih salah satu</label>
            </td>
        </tr>
        <tr>
            <td>Agama</td>
            <td>
                <label><input type="radio" name="id_agama" value="1" class="required" {if $cmb.ID_AGAMA==1}checked="checked"{/if} />Islam</label>
                <label><input type="radio" name="id_agama" value="2" {if $cmb.ID_AGAMA==2}checked="checked"{/if}/>Kristen Protestan</label>
                <label><input type="radio" name="id_agama" value="3" {if $cmb.ID_AGAMA==3}checked="checked"{/if}/>Kristen Katholik</label>
                <label><input type="radio" name="id_agama" value="4" {if $cmb.ID_AGAMA==4}checked="checked"{/if}/>Hindu</label>
                <label><input type="radio" name="id_agama" value="5" {if $cmb.ID_AGAMA==5}checked="checked"{/if}/>Budha</label>
                <label><input type="radio" name="id_agama" value="6" {if $cmb.ID_AGAMA==6}checked="checked"{/if}/>Lain-lain</label>
                <br/>
                <label for="id_agama" class="error" style="display: none;">Pilih salah satu</label></td>
        </tr>
        <tr>
            <td>Status Perkawinan</td>
            <td>
                <label><input type="radio" name="status_perkawinan" value="1" class="required" {if $cmb.STATUS_PERKAWINAN==1}checked="checked"{/if}/>Belum Kawin</label>
                <label><input type="radio" name="status_perkawinan" value="2" {if $cmb.STATUS_PERKAWINAN==2}checked="checked"{/if}/>Kawin</label>
                <label><input type="radio" name="status_perkawinan" value="3" {if $cmb.STATUS_PERKAWINAN==3}checked="checked"{/if}/>Bercerai</label>
                <label><input type="radio" name="status_perkawinan" value="4" {if $cmb.STATUS_PERKAWINAN==4}checked="checked"{/if}/>Kawin, Suami / Istri Meninggal</label>
                <br/>
                <label for="status_perkawinan" class="error" style="display: none;">Pilih salah satu</label></td>
            </td>
        </tr>
        <tr>
            <td>Pekerjaan / Jabatan</td>
            <td>
                <input type="text" name="pekerjaan" maxlength="30" size="30" value="{$cmb.PEKERJAAN}" class="required"/>
            </td>
        </tr>
        <tr>
            <td>Instansi Asal</td>
            <td><input type="" name="asal_instansi" maxlength="50" size="50" value="{$cmb.ASAL_INSTANSI}" class="required"/></td>
        </tr>
        <tr>
            <td>Alamat Instansi</td>
            <td>
                <textarea name="alamat_instansi" maxlength="250" cols="50" class="required">{$cmb.ALAMAT_INSTANSI}</textarea>
            </td>
        </tr>
        <tr>
            <td>Telp / Faks Instansi</td>
            <td><input type="" name="telp_instansi" maxlength="30" size="30" value="{$cmb.TELP_INSTANSI}" class="required"/></td>
        </tr>
        <tr>
            <td>NIP / NRP / NIS</td>
            <td>
                <input type="text" name="nrp" maxlength="20" size="20" value="{$cmb.NRP}" class="required" />
            </td>
        </tr>
        <tr>
            <td>KARPEG</td>
            <td><input type="text" name="karpeg" maxlength="30" size="30" value="{$cmb.KARPEG}" class="required"/></td>
        </tr>
        <tr>
            <td>Pangkat & Golongan</td>
            <td><input type="text" name="pangkat" maxlength="20" size="20" value="{$cmb.PANGKAT}" class="required"/></td>
        </tr>
        <tr>
            <td colspan="2" class="head">III. Pendidikan Diploma (D3)</td>
        </tr>
        <tr>
            <td>Perguruan Tinggi</td>
            <td>
                <input type="text" name="ptn_s1" maxlength="30" size="30" value="{$cmb.PTN_S1}" class="required"/>
            </td>
        </tr>
        <tr>
            <td>Status Perguruan Tinggi</td>
            <td>
                <label><input type="radio" name="status_ptn_s1" value="1" {if $cmb.STATUS_PTN_S1==1}checked="checked"{/if} class="required"/>Negeri</label>
                <label><input type="radio" name="status_ptn_s1" value="2" {if $cmb.STATUS_PTN_S1==2}checked="checked"{/if}/>Swasta</label>
                <label><input type="radio" name="status_ptn_s1" value="3" {if $cmb.STATUS_PTN_S1==3}checked="checked"{/if}/>Luar Negeri</label>
                <br/>
                <label for="status_ptn_s1" class="error" style="display:none;">Pilih salah satu</label>
            </td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td><input type="text" name="prodi_s1" maxlength="30" size="30" value="{$cmb.PRODI_S1}" class="required"/></td>
        </tr>
        <tr>
            <td>Tanggal Masuk</td>
            <td>{html_select_date prefix="tgl_masuk_s1_" field_order="DMY" start_year=-40 time=$cmb.TGL_MASUK_S1}</td>
        </tr>
        <tr>
            <td>Tanggal Lulus</td>
            <td>{html_select_date prefix="tgl_lulus_s1_" field_order="DMY" start_year=-40 time=$cmb.TGL_LULUS_S1}</td>
        </tr>
        <tr>
            <td>Lama Studi</td>
            <td><input type="text" name="lama_studi_s1" maxlength="4" size="4" class="number required" value="{$cmb.LAMA_STUDI_S1}" /> tahun</td>
        </tr>
        <tr>
            <td>Index Prestasi</td>
            <td><input type="text" name="ip_s1" maxlength="5" size="5" class="number required" value="{$cmb.IP_S1}" /></td>
        </tr>
        <tr>
            <td>Jumlah Karya Ilmiah</td>
            <td><input type="text" name="jumlah_karya_ilmiah" maxlength="2" size="2" class="number required" value="{$cmb.JUMLAH_KARYA_ILMIAH}" /></td>
        </tr>
        <tr>
            <td colspan="2" class="head">IV. Upload File Berkas</td>
        </tr>
            <tr>
        <td>File Ijazah / Surat Keterangan Lulus</td>
        <td>
            <input type="hidden" id="status_file_ijazah" {if $cmb.FILE_IJAZAH}value="1"{/if} />
            {if $cmb.FILE_IJAZAH}<a target="_blank" href="pendaftaran-verifikasi.php?no_ujian={$cmb.NO_UJIAN}" onclick="window.open('{$base_url}files/snmptn/pdf-berkas/{$cmb.ID_C_MHS}_ijazah.pdf');">{$cmb.ID_C_MHS}_ijazah.pdf</a>{/if}
            <input type="file" accept="application/pdf" name="file_ijazah"/>
        </td>
    </tr>
        <tr>
            <td colspan="2" class="head">V. Berkas</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
				<input type="checkbox" name="berkas_ijazah" value="1" {if $cmd.BERKAS_IJAZAH==1}checked="checked"{/if}/>Ijazah (Asli dan Foto copy)
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <input type="hidden" name="id_pilihan_1" value="{$cmb.ID_PILIHAN_1}" />
                <input type="hidden" name="id_pilihan_2" value="{$cmb.ID_PILIHAN_2}" />
                <input type="hidden" name="id_pilihan_3" value="{$cmb.ID_PILIHAN_3}" />
                <input type="hidden" name="id_pilihan_4" value="{$cmb.ID_PILIHAN_4}" />
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
                
{literal}<script type="text/javascript">
		$('#id_negara').change(function(){
            $.ajax({
                type:'post',
                url:'getNegara.php',
                data:'id_negara='+$('#id_negara').val(),
                success:function(data){
                    $('#id_provinsi').html(data);
                }                    
            })
			
     });
	 
	 $('#id_provinsi').change(function(){
            $.ajax({
                type:'post',
                url:'getKota.php',
                data:'id_provinsi='+$('#id_provinsi').val(),
                success:function(data){
                    $('#id_kota').html(data);
                }                    
            })
     });
	 
	  $('#id_negara_lahir').change(function(){
            $.ajax({
                type:'post',
                url:'getNegara.php',
                data:'id_negara='+$('#id_negara_lahir').val(),
                success:function(data){
                    $('#id_provinsi_lahir').html(data);
                }                    
            })
			
     });
	 
	 $('#id_provinsi_lahir').change(function(){
            $.ajax({
                type:'post',
                url:'getKota.php',
                data:'id_provinsi='+$('#id_provinsi_lahir').val(),
                success:function(data){
                    $('#id_kota_lahir').html(data);
                }                    
            })
     });
	 
	 $('#id_negara_ayah').change(function(){
            $.ajax({
                type:'post',
                url:'getNegara.php',
                data:'id_negara='+$('#id_negara_ayah').val(),
                success:function(data){
                    $('#id_provinsi_ayah').html(data);
                }                    
            })
			
     });
	 
	 $('#id_provinsi_ayah').change(function(){
            $.ajax({
                type:'post',
                url:'getKota.php',
                data:'id_provinsi='+$('#id_provinsi_ayah').val(),
                success:function(data){
                    $('#id_kota_ayah').html(data);
                }                    
            })
     });
	 
	 $('#id_negara_ibu').change(function(){
            $.ajax({
                type:'post',
                url:'getNegara.php',
                data:'id_negara='+$('#id_negara_ibu').val(),
                success:function(data){
                    $('#id_provinsi_ibu').html(data);
                }                    
            })
			
     });
	 
	 $('#id_provinsi_ibu').change(function(){
            $.ajax({
                type:'post',
                url:'getKota.php',
                data:'id_provinsi='+$('#id_provinsi_ibu').val(),
                success:function(data){
                    $('#id_kota_ibu').html(data);
                }                    
            })
     });
	 
	$("#tgl_lahir").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});

    $('#verifikasi').validate();
</script>{/literal}