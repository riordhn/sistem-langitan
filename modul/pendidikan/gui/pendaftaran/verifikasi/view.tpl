<div class="center_title_bar">Verifikasi Berkas Mahasiswa</div>

<table>
    <tr>
        <td>
            <label>Nomer Ujian :</label>
        </td>
        <td>
            <form action="pendaftaran-verifikasi.php" method="get">
            <input type="search" name="no_ujian" {if isset($smarty.get.no_ujian)}value="{$smarty.get.no_ujian}"{/if} />
            <input type="submit" value="Cari..." />
            </form>
        </td>
    </tr>
</table>
            
{if !empty($cmb)}
    {if !empty($updated)}<h2>{$updated}</h2>{/if}
    
    {if $cmb1.TGL_REGMABA == ''}
        <h2>Calon mahasiswa belum melakukan regmaba</h2>
    {elseif $pembayaran['JUMLAH'] == 0}
        <h2>Calon mahasiswa belum melakukan pembayaran</h2>
    {else}      
        {include $page_view}
    {/if}
{/if}