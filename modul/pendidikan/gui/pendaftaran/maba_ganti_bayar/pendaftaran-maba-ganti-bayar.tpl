<div class="center_title_bar">GANTI PEMBAYARAN MAHASISWA BARU</div>
 <form action="pendaftaran-maba-ganti-bayar.php" method="post">
	<table>
		<tr>
			<td>No Ujian</td>
			<td><input type="text" name="no_ujian" /></td>
			<td><input type="submit" value="Tampil" /></td>
		</tr>
    </table>
</form>


{if isset($cmhs)}
<form action="pendaftaran-maba-ganti-bayar.php" method="post"  id="gen_mhs">
	<table>
    	<tr>
        	<th colspan="2">BIODATA MAHASISWA</th>
        </tr>
   		 <tr>
        	<td>No Ujian</td>
            <td>{$cmhs['NO_UJIAN']}</td>
        </tr>
    	<tr>
        	<td>Nama</td>
				<td>{$cmhs['NM_C_MHS']}</td>
        </tr>
       	<tr>
        	<td>Semester</td>
            <td>
            	<select name="semester" id="semester" class="required">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER == $data_semester_aktif[0]['ID_SEMESTER']}selected="selected"{/if}>{$data.THN_AKADEMIK_SEMESTER} ({$data.NM_SEMESTER})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Fakultas</td>
            <td>
            	<select id="fakultas"  name="fakultas" class="required">
                	<option value="">PILIH FAKULTAS</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Jenjang</td>
            <td>
            	<select id="jenjang" name="jenjang" class="required">
                    <option value="">PILIH JENJANG</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
            </td>
        </tr>
        <tr>
        	<td>Program Studi</td>
            <td>
            	<select id="program_studi" name="program_studi" class="required">
                    <option value="">PILIH PRODI</option>
                </select>
            </td>
        </tr>
        <tr>
        	<td>Jalur</td>
            <td>
            	<select id="jalur" name="jalur" class="required">
                    <option value="">PILIH JALUR</option> 
                   {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $jalur==$data.ID_JALUR}selected="true"{/if}>{$data.NM_JALUR}</option>
                    {/foreach}                    
                </select>
            </td>
        </tr>
        <tr style="display:none" id="showtr">
        	<td colspan="2"><span id="biaya"></span></td>
        </tr>
        <tr>
        	<td colspan="2" style="text-align:center">
				<input type="hidden" value="{$cmhs['NO_UJIAN']}" name="no_ujian" />
            	<input type="hidden" value="generate" name="generate" />
                <input type="button" value="Biaya Kuliah" id="biaya_kuliah" />
            	<input type="submit" value="Generate"/>
            </td>
        </tr>
    </table>
</form>

{/if}


{literal}
    <script type="text/javascript">
		$('#gen_mhs').validate();
		
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$("#biaya_kuliah").click(function(){
			$('#showtr').fadeToggle("slow");
			$.ajax({
                type:'post',
                url:'getBiaya.php',
                data:'id_program_studi='+$('#program_studi').val()+'&id_semester='+$('#semester').val()+'&id_jalur='+$('#jalur').val(),
                success:function(data){
                    $('#biaya').html(data);
                }                    
            })
			
			
		});
		</script>
{/literal}