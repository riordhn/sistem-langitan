<div class="center_title_bar">GENERATE NIM MAHASISWA ASING</div>
 <form action="pendaftaran-nim-asing.php" method="post"  id="gen_mhs">
	<table>
    	<tr>
        	<th colspan="2">BIODATA MAHASISWA</th>
        </tr>
        {if isset($nim_baru)}
   		 <tr>
        	<td>NIM Baru</td>
            <td>{$nim_baru}</td>
        </tr>
        {/if}
    	<tr>
        	<td>Nama</td>
            <td><input type="text" name="nama" value="{$nama}" size="30" class="required" /></td>
        </tr>
        <tr>
        	<td>Gelar Depan</td>
            <td><input type="text" name="gelar_depan" value="{$gelar_depan}" size="10" /></td>
        </tr>
        <tr>
        	<td>Gelar Belakang</td>
            <td><input type="text" name="gelar_belakang" value="{$gelar_belakang}" size="10" /></td>
        </tr>
       	<tr>
        	<td>Semester</td>
            <td>
            	<select name="semester" id="semester" class="required">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER == $data_semester_aktif[0]['ID_SEMESTER']}selected="selected"{/if}>{$data.THN_AKADEMIK_SEMESTER} ({$data.NM_SEMESTER})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Fakultas</td>
            <td>
            	<select id="fakultas"  name="fakultas" class="required">
                	<option value="">PILIH FAKULTAS</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Jenjang</td>
            <td>
            	<select id="jenjang" name="jenjang" class="required">
                    <option value="">PILIH JENJANG</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
            </td>
        </tr>
        <tr>
        	<td>Program Studi</td>
            <td>
            	<select id="program_studi" name="program_studi" class="required">
                    <option value="">PILIH PRODI</option>
                    {foreach $data_program_studi as $data}
                         <option value="{$data.ID_PROGRAM_STUDI}" {if $program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Type</td>
            <td>
            	<select id="type" name="type" class="required">
                    <option value="">PILIH TYPE</option> 
                        <option value="1" {if $smarty.request.type==1}selected="true"{/if}>Akademik</option>
                        <option value="2" {if $smarty.request.type==2}selected="true"{/if}>Non-Akademik</option>
                </select>
            </td>
        </tr>
		<tr>
        	<td>Jenis Kerjasama</td>
            <td>
            	<select id="kerjasama" name="kerjasama" class="required">
                    <option value="">PILIH JENIS KERJASAMA</option> 
                   {foreach $data_jenis_kerjasama as $data}
                        <option value="{$data.ID_JENIS_KERJASAMA}" {if $kerjasama==$data.ID_JENIS_KERJASAMA}selected="true"{/if}>{$data.NM_JENIS_KERJASAMA}</option>
                    {/foreach}                    
                </select>
            </td>
        </tr>
				<tr>
        	<td>Alamat Asal</td>
            <td><textarea name="alamat_asal" rows="3" cols="25">{$alamat_asal}</textarea></td>
        </tr>
		<tr>
        	<td>Kota Asal</td>
            <td>
				<select id="benua" name="benua" class="required">
                    <option value="">PILIH BENUA</option> 
                   {foreach $data_benua as $data}
                        <option value="{$data.ID_BENUA}" {if $benua==$data.ID_BENUA}selected="true"{/if}>{$data.NM_BENUA}</option>
                    {/foreach}                    
                </select><br />
				<select id="negara_asal" name="negara_asal" class="required">
                    <option value="">PILIH NEGARA</option> 
                   {foreach $data_negara as $data}
                        <option value="{$data.ID_NEGARA}" {if $negara_asal==$data.ID_NEGARA}selected="true"{/if}>{$data.NM_NEGARA}</option>
                    {/foreach}                    
                </select><br />
				<select id="prov_asal" name="prov_asal">
                    <option value="">PILIH PROVINSI</option> 
                   {foreach $data_prov_asal as $data}
                        <option value="{$data.ID_PROVINSI}" {if $prov_asal==$data.ID_PROVINSI}selected="true"{/if}>{$data.NM_PROVINSI}</option>
                    {/foreach}                    
                </select><br />
				<select id="kota_asal" name="kota_asal">
                    <option value="">PILIH KOTA</option> 
                   {foreach $data_kota_asal as $data}
                        <option value="{$data.ID_KOTA}" {if $kota_asal==$data.ID_KOTA}selected="true"{/if}>{$data.NM_KOTA}</option>
                    {/foreach}                    
                </select>
			</td>
        </tr>
		<tr>
        	<td>Alamat Indonesia</td>
            <td><textarea name="alamat" rows="3" cols="25">{$alamat}</textarea></td>
        </tr>
		<tr>
        	<td>Kota Indonesia</td>
            <td>
				<select id="prov_in" name="prov_in">
                    <option value="">PILIH PROVINSI</option> 
                   {foreach $data_prov_in as $data}
                        <option value="{$data.ID_PROVINSI}" {if $prov_in==$data.ID_PROVINSI}selected="true"{/if}>{$data.NM_PROVINSI}</option>
                    {/foreach}                    
                </select><br />
				<select id="kota_in" name="kota_in">
                    <option value="">PILIH KOTA</option> 
                   {foreach $data_kota_in as $data}
                        <option value="{$data.ID_KOTA}" {if $kota_in==$data.ID_KOTA}selected="true"{/if}>{$data.NM_KOTA}</option>
                    {/foreach}                    
                </select>
			</td>
        </tr>
        <tr>
        	<td colspan="2" style="text-align:center">
            	<input type="hidden" value="generate" name="generate" />
            	<input type="submit" value="Generate"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
		$('#gen_mhs').validate();
		
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#benua').change(function(){
            $.ajax({
                type:'post',
                url:'getBenua.php',
                data:'id_benua='+$('#benua').val(),
                success:function(data){
                    $('#negara_asal').html(data);
                }                    
            })
        });
		
		$('#prov_in').change(function(){
            $.ajax({
                type:'post',
                url:'getKota.php',
                data:'id_provinsi='+$('#prov_in').val(),
                success:function(data){
                    $('#kota_in').html(data);
                }                    
            })
        });
		
		
		$('#negara_asal').change(function(){
            $.ajax({
                type:'post',
                url:'getNegara.php',
                data:'id_negara='+$('#negara_asal').val(),
                success:function(data){
                    $('#prov_asal').html(data);
                }                    
            })
        });
		
		
		$('#prov_asal').change(function(){
            $.ajax({
                type:'post',
                url:'getKota.php',
                data:'id_provinsi='+$('#prov_asal').val(),
                success:function(data){
                    $('#kota_asal').html(data);
                }                    
            })
        });
		</script>
{/literal}