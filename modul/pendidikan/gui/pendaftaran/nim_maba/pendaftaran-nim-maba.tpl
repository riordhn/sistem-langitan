<div class="center_title_bar">GENERATE NIM CALON MAHASISWA</div>
{if !empty($result_set)}
    <h3>Hasil Generate</h3>
    
    <table>
        <tr>
            <th>No</th>
            <th>No Ujian</th>
            <th>Status</th>
            <th>NIM</th>
        </tr>
        {foreach $result_set as $r}
        <tr>
            <td>{$r@index + 1}</td>
            <td>{$r.NO_UJIAN}</td>
            <td>{$r.RESULT}</td>
            <td>{$r.NIM_MHS}</td>
        </tr>
        {/foreach}
    </table>
{/if}


{if !empty($cmb_set)}

<form action="pendaftaran-nim-maba.php" method="post">
    <input type="hidden" name="mode" value="generate" />
    <input type="submit" value="Generate NIM" /><br /><br />

<table style="font-size:12px">
    <tr>
        <th>No</th>
        <th style="width: 60px">No Ujian</th>
        <th>Nama</th>
        <th>Bayar</th>
		<th>Prodi</th>
        <th>Penerimaan</th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr>
        <td class="center">{$cmb@index + 1}</td>
        <td class="center">{$cmb.NO_UJIAN}</td>
        <td>{$cmb.NM_C_MHS}</td>
        <td>{if $cmb.PEMBAYARAN > 0}Sudah{elseif $cmb.STATUS_BIDIK_MISI == 1}Bidik Misi{else}<font style="color: #f00">Belum</font>{/if}</td>
		<td>{$cmb.NM_PROGRAM_STUDI}</td>
        <td>{$cmb.PENERIMAAN}</td>
            <input type="hidden" name="no_ujian[]" value="{$cmb.NO_UJIAN}" />
    </tr>
    {/foreach}
</table>

</form>
{/if}