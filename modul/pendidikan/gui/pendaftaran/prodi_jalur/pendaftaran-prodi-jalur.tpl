<div class="center_title_bar">MAHASISWA PINDAH JALUR ATAU PRODI</div>
<form action="pendaftaran-prodi-jalur.php" method="post" id="f1">
<table>
	<tr>
    	<td>No Ujian</td>
        <td><input type="text" name="no_ujian" class="required" value="{$smarty.post.no_ujian}"  /></td>
    </tr>
    <tr>
    	<td>NIM Mhs Lama</td>
        <td><input type="text" name="nim" class="required" value="{$smarty.post.nim}" /></td>
    </tr>
    <tr>
    	<td>Program Studi/ Jalur</td>
        <td>
        	<select name="pindah">
        		<option value=""></option>
                <option value="1" {if $smarty.post.pindah == 1}selected="selected"{/if}>Pindah Prodi</option>
                <option value="2" {if $smarty.post.pindah == 2}selected="selected"{/if}>Pindah Jalur</option>
            </select>
        </td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align:center">
        	<input type="submit" value="Tampil" />
        </td>
    </tr>
</table>
</form>


{if isset($prodi_jalur)}
<form action="pendaftaran-prodi-jalur.php" method="post">
	<table>
    	<tr>
        	<td>No Ujian</td>
            <td>{$prodi_jalur.NO_UJIAN}</td>
        </tr>
        <tr>
        	<td>NIM Baru</td>
            <td>{$prodi_jalur.NIM_MHS}</td>
        </tr>
        <tr>
        	<td>NIM Lama</td>
            <td>{$smarty.post.nim}</td>
        </tr>
        <tr>
        	<td>Nama</td>
            <td>{$prodi_jalur.NM_C_MHS}</td>
        </tr>
        <tr>
        	<td colspan="2" style="text-align:center">
            	<input type="submit" value="Simpan" />
            	<input type="hidden" value="insert" name="mode" />
                <input type="hidden" value="{$prodi_jalur.NO_UJIAN}" name="no_ujian" />
                <input type="hidden" value="{$smarty.post.nim}" name="nim" />
                <input type="hidden" value="{$smarty.post.pindah}" name="pindah" />
            </td>
        </tr>
    </table>
</form>    
{/if}



{literal}
<script type="text/javascript">
    $('#f1').validate();
</script>
{/literal}