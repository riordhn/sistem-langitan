<div class="center_title_bar">Hapus Mahasiswa</div>

<form action="delete-mhs.php" method="get">
	<table>
		<tr>
			<td>NIM :</td>
			<td><input type="text" name="nim" value="{if !empty($smarty.get.nim)}{$smarty.get.nim}{/if}"/></td>
			<td><input type="submit" value="Tampil" /></td>
		</tr>
	</table>
</form>

{if isset($smarty.get.nim)}
	<form action="delete-mhs.php" method="post" id="f2">
		<table>
			<tr>
				<th colspan="2">BIODATA</th>
			</tr>
			<tr>
				<td>NIM :</td>
				<td>{$mhs.NIM_MHS}</td>

			</tr>
			<tr>
				<td>No Ujian</td>
				<td>{$mhs.NO_UJIAN}</td>
			</tr>
			<tr>
				<td>Nama</td>
				<td>{$mhs.NM_PENGGUNA}</td>
			</tr>
			<tr>
				<td>Program Studi</td>
				<td>{$mhs.NM_PROGRAM_STUDI} - {$mhs.NM_JENJANG}</td>
			</tr>
			<tr>
				<td>Keterangan</td>
				<td><textarea name="keterangan" class="required"></textarea></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center">
					<input type="hidden" value="{$mhs.ID_MHS}" name="id_mhs" />
					<input type="hidden" value="{$mhs.ID_PENGGUNA}" name="id_pengguna" />
					
					<input type="hidden" value="{$mhs.NIM_MHS}" name="nim" />
					<input type="hidden" value="{$mhs.NO_UJIAN}" name="no_ujian" />
					<input type="hidden" value="{$mhs.NM_PENGGUNA}" name="nama" />
					<input type="hidden" value="{$mhs.ID_PROGRAM_STUDI}" name="prodi" />
					<input type="submit" value="Hapus" onclick="return confirm('Are you sure you want to delete?');" />
				</td>
			</tr>
		</table>

	</form>

	{literal}
		<script language="text/javascript">
			$('#f2').validate();
		</script>
	{/literal}
{/if}