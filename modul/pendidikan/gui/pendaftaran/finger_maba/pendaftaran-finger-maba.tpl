<div class="center_title_bar">GENERATE FINGER CALON MAHASISWA</div>
<form method="post" action="pendaftaran-finger-maba.php">
	<input type="submit" value="Generate Fingerprint" />
    <input type="hidden" value="update" name="mode" />
</form><br />

<table>
	<tr>
    	<th>No</th>
        <th>NIM</th>
        <th>Nama</th>
        <th>Fakultas</th>
        <th>Prodi</th>
    </tr>
    {$no = 1}
    {foreach $finger_set as $data}
    	<tr>
        	<td>{$no++}</td>
            <td>{$data.NIM_MHS}</td>
            <td>{$data.NM_PENGGUNA}</td>
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
        </tr>
    {/foreach}
</table>