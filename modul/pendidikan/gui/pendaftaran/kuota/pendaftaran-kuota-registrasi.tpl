<div class="center_title_bar">Kuota Registrasi</div>
<form action="pendaftaran-kuota-registrasi.php" method="post" id="f2">
    <table>
        <tr>
            <th colspan="2">
                Parameter
            </th>
        </tr>
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="id_penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.post.id_penerimaan || $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center"><input type="submit" value="Tampil"></td>
        </tr>
    </table>
</form>

{if isset($kuota_registrasi)}
	
    <form action="pendaftaran-kuota-registrasi.php" method="post" id="f1">
    	<table>
    		<tr>
          	  	<th colspan="2">
            	</th>
        	</tr>
            <tr>
            	<td>Tgl Registrasi</td>
                <td><input type="text" id="tgl_registrasi" name="tgl_registrasi"  value="{$edit.TGL_REGISTRASI}" class="required" /></td>
            </tr>
	    <tr>
		<td>Jam Awal</td>
		<td><input type="text" id="jam_awal" name="jam_awal" size="7"  value="{$edit.JAM_AWAL}" /></td>
	   </tr>
	   <tr>
		<td>Jam Akhir</td>
		<td><input type="text" id="jam_akhir" name="jam_akhir" size="7"  value="{$edit.JAM_AKHIR}" /></td>
	   </tr>
            <tr>
            	<td>Kuota</td>
                <td><input type="text" name="kuota" size="4" class="required"  value="{$edit.KUOTA}" /></td>
            </tr>
            <tr>
            	<td colspan="2" style="text-align:center">
                	<input type="submit" value="Simpan" />
               		<input type="hidden" value="{if isset($smarty.post.id_penerimaan)}{$smarty.post.id_penerimaan}{else}{$smarty.get.id_penerimaan}{/if}" name="id_penerimaan" />
                    <input type="hidden" value="{$smarty.get.id}" name="id" />
                </td>
            </tr>
    	</table>
	</form>    				
	             <table>
                    <tr>
                        <th>No</th>
                        <th>Tgl Registrasi</th>
                        <th>Jam Awal</th>
			<th>Jam Akhir</th>
			<th>Kuota</th>
			<th>Terisi</th>
                        <th>Edit</th>
                    </tr>
                    {$no = 1}
                    {foreach $kuota_registrasi as $data}
                    <tr>
                        <td>{$no++}</td>
                        <td>{$data.TGL_REGISTRASI}</td>
                        <td>{$data.JAM_AWAL}</td>
			<td>{$data.JAM_AKHIR}</td>
			<td>{$data.KUOTA}</td>
			<td>{$data.TERISI}</td>
                        <td>
			{if {$data.TERISI} == 0}
			<a href="pendaftaran-kuota-registrasi.php?id={$data.ID_JADWAL_VERIFIKASI_PEND}&id_penerimaan={$data.ID_PENERIMAAN}&mode=edit">Edit</a> |
			<a href="pendaftaran-kuota-registrasi.php?id={$data.ID_JADWAL_VERIFIKASI_PEND}&id_penerimaan={$data.ID_PENERIMAAN}&mode=delete">Delete</a>
			{else}
			<a href="pendaftaran-kuota-registrasi.php?id={$data.ID_JADWAL_VERIFIKASI_PEND}&id_penerimaan={$data.ID_PENERIMAAN}&mode=edit">Edit</a>			
			{/if}
			
			</td>
		    </tr>
                    {/foreach}
                </table>
				
{/if}

{literal}
    <script>
	    $("#jam_awal").timepicker({timeFormat: 'hh:mm'});
	    $("#jam_akhir").timepicker({timeFormat: 'hh:mm'});
            $("#tgl_registrasi").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
            $('#f1').validate();
    </script>
{/literal}