<div class="center_title_bar">PRINT JADWAL ELPT DAN TES KESEHATAN</div>

<form action="pendaftaran-jadwal.php" method="post">
<table>
	<tr>
    	<td>NO Ujian</td>
        <td>:</td>
        <td>
        	<input type="text" name="no_ujian" class="required" />
        	<input type="submit" value="Tampil" />
        </td>
    </tr>
</table>
</form>

<h2>{$eror}</h2>
{if isset($cmhs) and !isset($eror)}

<table cellpadding="6" width="100%" border="0">
    <tr><th colspan="2" align="center"><h1>Jadwal Test Calon Mahasiswa</h1></th></tr>
    <tr>
        <td colspan="2"></td>  
    </tr>
    <tr>
        <td width="35%">Nama</td>
        <td width="65%">: {$cmhs.NM_C_MHS}</td>
    </tr>
    <tr>
        <td width="35%">No Ujian</td>
        <td width="65%">: {$cmhs.NO_UJIAN}</td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>: {$cmhs.NM_PRODI}</td>
    </tr>    
    <tr>
        <td colspan="2"></td>  
    </tr>
    <tr>
        <td>Tempat Test Kesehatan</td>
        <td>: Rumah Sakit Universitas Airlangga, Kampus C UNAIR, Surabaya</td>
    </tr>
    <tr>
        <td>Jadwal Test Kesehatan</td>
        <td>: {$cmhs.TGL_TEST_KESEHATAN}</td>
    </tr>
    <tr>
        <td>Kelompok Test Kesehatan</td>
        <td>: {$cmhs.KELOMPOK_JAM_KESEHATAN}</td>
    </tr>
    <tr>
        <td>Waktu Test Kesehatan</td>
        <td>: {$cmhs.JAM_MULAI_KESEHATAN}:{if $cmhs.MENIT_MULAI_KESEHATAN == 0}{$cmhs.MENIT_MULAI_KESEHATAN}0{else}{$cmhs.MENIT_MULAI_KESEHATAN}{/if} - {$cmhs.JAM_SELESAI_KESEHATAN}:{if $cmhs.MENIT_SELESAI_KESEHATAN == 0}{$cmhs.MENIT_SELESAI_KESEHATAN}0{else}{$cmhs.MENIT_SELESAI_KESEHATAN}{/if} WIB</td>
    </tr>
    <tr>
        <td colspan="2"></td>  
    </tr>
    <tr>
        <td>Tempat Test ELPT</td>
        <td>: PINLABS, Kampus B - Universitas Airlangga</td>
    </tr>
    <tr>
        <td>Jadwal Test ELPT</td>
        <td>: {$cmhs.TGL_TEST_TOEFL}</td>
    </tr>
    <tr>
        <td>Ruang Test ELPT</td>
        <td>: {$cmhs.NM_RUANG}</td>
    </tr>
    <tr>
        <td>Waktu Test ELPT</td>
        <td>: {$cmhs.JAM_MULAI_TOEFL}:{if $cmhs.MENIT_MULAI_TOEFL == 0}{$cmhs.MENIT_MULAI_TOEFL}0{else}{$cmhs.MENIT_MULAI_TOEFL}{/if} WIB</td>
    </tr>
    <tr>
        <td colspan="2" style="text-align:center">
        <input type="button" value="Print" onclick="window.open('cetak-jadwal.php?no_ujian={$cmhs.NO_UJIAN}');"></td>  
    </tr>
</table>

{/if}

{literal}
<script language="javascript">
$('form').validate();
</script>
{/literal}
