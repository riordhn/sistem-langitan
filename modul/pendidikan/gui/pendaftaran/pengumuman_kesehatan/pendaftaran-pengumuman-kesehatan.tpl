<div class="center_title_bar">SETTING PENGUMUMAN HASIL TES KESEHATAN</div>

<form action="pendaftaran-pengumuman-kesehatan.php" method="post" id="f2">
    <table>
        <tr>
            <th colspan="2">
                Parameter
            </th>
        </tr>
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="id_penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Tanggal Awal Tes Kesehatan</td>
            <td><input type="text" id="tgl_awal" name="tgl_awal"  value="" class="required" /></td>
        </tr>
        <tr>
            <td>Tanggal Akhir Tes Kesehatan</td>
            <td><input type="text" id="tgl_akhir" name="tgl_akhir"  value="" class="required" /></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center"><input type="submit" value="Simpan"></td>
        </tr>
    </table>
</form>

                
                <table>
                    <tr>
                        <th>No</th>
                        <th>Penerimaan</th>
                        <th>Tahun</th>
                        <th>Semester</th>
                        <th>Tanggal Awal</th>
                        <th>Tanggal Akhir</th>
                        <th>Hapus</th>
                    </tr>
                    {$no = 1}
                    {foreach $view_kesehatan as $data}
                    <tr>
                        <td>{$no++}</td>
                        <td>{$data.NM_PENERIMAAN}</td>
                        <td>{$data.TAHUN}</td>
                        <td>{$data.SEMESTER}</td>
                        <td>{$data.TGL_AWAL_KESEHATAN}</td>
                        <td>{$data.TGL_AKHIR_KESEHATAN}</td>
                       <td><a href="pendaftaran-pengumuman-kesehatan.php?mode=hapus&id={$data.ID_PENERIMAAN}">Hapus</a></td>
                    </tr>
                    {/foreach}
                </table>
                
{literal}
    <script>
            $("#tgl_awal").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
            $("#tgl_akhir").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
            $('#f2').validate();
    </script>
{/literal}