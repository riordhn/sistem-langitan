<div class="center_title_bar">Rekap Registrasi</div>

<form action="pendaftaran-registrasi.php" method="post">
    <table>
        <tr>
            <th colspan="4">Pencarian</th>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>
                <select id="fakultas" name="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
            <td>Jenjang</td>
            <td>
                <select id="jenjang" name="jenjang">
                    <option value="">Semua</option>
                    {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
		<tr>
            <td>Jalur</td>
            <td>
                <select id="jalur" name="jalur">
                    <option value="">Semua</option>
                    {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $jalur==$data.ID_JALUR}selected="true"{/if}>{$data.NM_JALUR|upper}</option>
                    {/foreach}
                </select>
            </td>
            <td>Gelombang</td>
            <td>
                <select id="gelombang" name="gelombang">
                    <option value="">Semua</option>
                    {section name=foo loop=3} 
                    	<option value="{$smarty.section.foo.iteration}" {if $gelombang==$smarty.section.foo.iteration}selected="true"{/if}>{$smarty.section.foo.iteration}</option>
					{/section}
                </select>
            </td>
        </tr>
        <tr>
            <td>Tahun Angkatan</td>
            <td>
               <select name="thn" id="thn">
               	<option value="">Semua</option>
                    {foreach $data_thn as $data}
                        <option value="{$data.TAHUN}" {if $thn==$data.TAHUN}selected="true"{/if}>{$data.TAHUN|upper}</option>
                    {/foreach}
               </select>
            </td>
            <td>Semester</td>
            <td>
            	<select name="semester">
                	<option value="">Semua</option>
                    <option value="Gasal" {if $semester == 'Gasal'} selected="selected"{/if}>Ganjil</option>
                    <option value="Genap" {if $semester == 'Genap'} selected="selected"{/if}>Genap</option>	
                </select>
            </td>
        </tr>
		<tr>
            <td>Pembayaran Awal</td>
            <td>
               <input type="text" name="tgl_awal" id="tgl_awal" value="{if isset($smarty.get.tgl_awal)}{$smarty.get.tgl_awal}{else}{$smarty.post.tgl_awal}{/if}" />
            </td>
            <td>Pembayaran Akhir</td>
            <td>
            	<input type="text" name="tgl_akhir" id="tgl_akhir" value="{if isset($smarty.get.tgl_akhir)}{$smarty.get.tgl_akhir}{else}{$smarty.post.tgl_akhir}{/if}" />
            </td>
        </tr>
        <tr>
        	<td>Nama Penerimaan</td>
        	<td colspan="3">
            	<select name="id_penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.post.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td colspan="4" style="text-align:center"><input type="submit" value="Tampil" /></td>
        </tr>
    </table>
</form>
{if isset($penerimaan)}
<input type="button" value="Excel" onclick="window.open('excel-pendaftaran-registrasi.php?get_fakultas={$fakultas}&get_jenjang={$jenjang}&get_jalur={$jalur}&get_gel={$gelombang}&get_thn={$thn}&get_semester={$semester}');"/>
<table>
    <tr>
    {if $fakultas != ''}
        <th>Progam Studi</th>
		<th>Fakultas</th>
    {else}
    	<th>Fakultas</th>
		<th>Progam Studi</th>
    {/if}
		<th>Nama Penerimaan</th>
        <th>Jalur</th>
		<th>Jenjang</th>
        <th>Tahun</th>
        <th>Diterima</th>
        <th>Regmaba</th>
        <th>Verifikasi Keuangan</th>
        <th>Bayar</th>
		<th>Bidik Misi</th>
        <th>Verifikasi Pendidikan</th>        
        <th>Finger</th>
        <th>KTM</th>
    </tr>
    {$jml_diterima = 0}
    {$jml_bayar = 0}
    {$jml_verifikasi = 0}
    {$jml_verifikasi_keu = 0}
    {$jml_finger = 0}
    {$jml_ktm = 0}
    {$jml_regmaba = 0}
    {foreach $penerimaan as $data}
    <tr>
    {if $fakultas != ''}
		<td>{$data.NM_FAKULTAS}</td>
        <td>{$data.NM_PROGRAM_STUDI}</td>
		{$get_prodi = $data.ID_PROGRAM_STUDI}
    {else}
    	<td>{$data.NM_FAKULTAS}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
		{$get_fakultas = $data.ID_FAKULTAS}
    {/if}
		<td>{$data.NM_PENERIMAAN}</td>
        <td>{$data.NM_JALUR} Gel-{$data.GELOMBANG}</td>
		<td style="text-align:center">{$data.NM_JENJANG}</td>
        <td>{$data.TAHUN} / {$data.SEMESTER}</td>
        <td style="text-align:center"><a href="pendaftaran-registrasi.php?get_fakultas={$get_fakultas}&get_prodi={$get_prodi}&get_jalur={$jalur}&get_thn={$thn}&get_id={$data.ID_PENERIMAAN}&tgl_awal={$smarty.post.tgl_awal}&tgl_akhir={$smarty.post.tgl_akhir}">{$data.DITERIMA}</a></td>
        <td style="text-align:center">{$data.REGMABA}</td>
        <td style="text-align:center">{$data.VERIFIKASI_KEUANGAN}</td>
        <td style="text-align:center">{$data.BAYAR}</td>
		<td style="text-align:center">{$data.BIDIK_MISI}</td>
        <td style="text-align:center">{$data.VERIFIKASI}</td>        
        <td style="text-align:center">{$data.FINGER}</td>
        <td style="text-align:center">{$data.KTM}</td>
    </tr>
    {$jml_diterima = $jml_diterima + $data.DITERIMA}
    {$jml_bayar = $jml_bayar + $data.BAYAR}
    {$jml_verifikasi = $jml_verifikasi + $data.VERIFIKASI}
    {$jml_finger = $jml_finger + $data.FINGER}
    {$jml_ktm = $jml_ktm + $data.KTM}
    {$jml_regmaba = $jml_regmaba + $data.REGMABA}
    {$jml_verifikasi_keu = $jml_verifikasi_keu + $data.VERIFIKASI_KEUANGAN}
	{$jml_bidik_misi = $jml_bidik_misi + $data.BIDIK_MISI}
    {/foreach}
    <tr>
        <th colspan="6">TOTAL</th>
        <th style="text-align:center"><a href="pendaftaran-registrasi.php?get_fakultas={$fakultas}&get_jenjang={$jenjang}&get_jalur={$jalur}&get_gel={$gelombang}&get_thn={$thn}&get_semester={$semester}&tgl_awal={$smarty.post.tgl_awal}&tgl_akhir={$smarty.post.tgl_akhir}" style="color:#FFF">{$jml_diterima}</a></th>
        <th style="text-align:center">{$jml_regmaba}</th>
        <th style="text-align:center">{$jml_verifikasi_keu}</th> 
        <th style="text-align:center">{$jml_bayar}</th>
		<th style="text-align:center">{$jml_bidik_misi}</th>
        <th style="text-align:center">{$jml_verifikasi}</th>               
        <th style="text-align:center">{$jml_finger}</th>
        <th style="text-align:center">{$jml_ktm}</th>
    </tr>
</table>
{/if}

{if isset($get_penerimaan_detail)}
<table>	
	<tr>
		<th>No</th>
		<th>NO UJIAN</th>
        <th>NIM</th>
        <th>NAMA</th>
        <th>PRODI</th>
		<th>Diterima</th>
        <th>Bayar</th>
        <th>Verifikasi</th>
        <th>Finger</th>
        <th>KTM</th>
    </tr>
	{$no = 1}
	{foreach $get_penerimaan_detail as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.NO_UJIAN}</td>
			<td>{$data.NIM_MHS}</td>
			<td>{$data.NM_C_MHS}</td>
			<td>{$data.NM_PROGRAM_STUDI}</td>
			<td>{$data.TGL_DITERIMA}</td>
			<td>
				{$data.TGL_BAYAR}
			</td>
			<td>
				{$data.TGL_VERIFIKASI_PENDIDIKAN}
			</td>
			<td style="text-align:center">
				{if $data.FINGER <> ''}
					Sudah
				{else}
					-
				{/if}
			</td>
			<td style="text-align:center">
				{$data.TGL_CETAK_KTM}
			</td>
		</tr>
	{/foreach}
	<tr>
		<td colspan="10" style="text-align:center">
			<form action="excel-pendaftaran-registrasi.php" method="get" name="form2">
				<input type="hidden" value="{$get_fakultas}" name="get_fakultas" />
				<input type="hidden" value="{$get_id}" name="get_id" />
				<input type="hidden" value="{$get_prodi}" name="get_prodi" />
				<input type="button" value="Cetak" onclick="get_link_report()"/>
			</form>
		</td>
	</tr>
</table>

{/if}

{literal}
    <script type="text/javascript">
        function get_link_report(){
            link=document.location.hash.replace('#pendaftaran-rekap_regis!', '');
             window.location.href='excel-'+link;
        }
		
		$("#tgl_awal").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
		$("#tgl_akhir").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});		
    </script>
{/literal}