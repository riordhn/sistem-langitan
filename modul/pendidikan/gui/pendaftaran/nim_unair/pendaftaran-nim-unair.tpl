<div class="center_title_bar">GENERATE NIM MAHASISWA YANG PERNAH KULIAH DI UNAIR</div>
<form action="pendaftaran-nim-unair.php" method="post">
	<table>
    	<tr>
        	<td>NIM</td>
            <td><input type="text" name="nim" /></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>


{if (isset($mhs) and $mhs <> null)}
<form action="pendaftaran-nim-unair.php" method="post"  id="gen_mhs">
	<table>
    	<tr>
        	<th colspan="2">BIODATA MAHASISWA</th>
        </tr>
   		 <tr>
        	<td>NIM Lama</td>
            <td>{$mhs[0]['NIM_MHS']}</td>
        </tr>
    	<tr>
        	<td>Nama</td>
            <td><input type="text" name="nama" value="{$mhs[0]['NM_PENGGUNA']}" size="30" class="required" /></td>
        </tr>
        <tr>
        	<td>Gelar Depan</td>
            <td><input type="text" name="gelar_depan" size="10" /></td>
        </tr>
        <tr>
        	<td>Gelar Belakang</td>
            <td><input type="text" name="gelar_belakang" size="10" /></td>
        </tr>
       	<tr>
        	<td>Semester Masuk</td>
            <td>
            	<select name="semester" id="semester" class="required">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER == $data_semester_aktif[0]['ID_SEMESTER']}selected="selected"{/if}>{$data.THN_AKADEMIK_SEMESTER} ({$data.NM_SEMESTER})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Fakultas</td>
            <td>
            	<select id="fakultas"  name="fakultas" class="required">
                	<option value="">PILIH FAKULTAS</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Jenjang</td>
            <td>
            	<select id="jenjang" name="jenjang" class="required">
                    <option value="">PILIH JENJANG</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
            </td>
        </tr>
        <tr>
        	<td>Program Studi</td>
            <td>
            	<select id="program_studi" name="program_studi" class="required">
                    <option value="">PILIH PRODI</option>
                </select>
            </td>
        </tr>
        <tr>
        	<td>Jalur</td>
            <td>
            	<select id="jalur" name="jalur" class="required">
                    <option value="">PILIH JALUR</option> 
                   {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $jalur==$data.ID_JALUR}selected="true"{/if}>{$data.NM_JALUR}</option>
                    {/foreach}                    
                </select>
            </td>
        </tr>
		<tr>
        	<td>Semester Bayar</td>
            <td>
            	<select name="semester_bayar" id="semester_bayar" class="required">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER == $data_semester_aktif[0]['ID_SEMESTER']}selected="selected"{/if}>{$data.THN_AKADEMIK_SEMESTER} ({$data.NM_SEMESTER})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td colspan="2"><span id="biaya"></span></td>
        </tr>
        <tr>
        	<td colspan="2" style="text-align:center">
            	<input type="hidden" value="{$mhs[0]['NIM_MHS']}" name="nim_lama" />
            	<input type="hidden" value="generate" name="generate" />
                <input type="button" value="Biaya Kuliah" id="biaya_kuliah" />
            	<input type="submit" value="Generate"/>
            </td>
        </tr>
    </table>
</form>

{elseif isset($generate)}
<form action="pendaftaran-nim-unair.php" method="post"  id="gen_mhs">
	<table>
    	<tr>
        	<th colspan="2">BIODATA MAHASISWA</th>
        </tr>
   		 <tr>
        	<td>NIM Baru</td>
            <td>{$nim_baru}</td>
        </tr>
    	<tr>
        	<td>Nama</td>
            <td><input type="text" name="nama" value="{$nama}" size="30" class="required" /></td>
        </tr>
        <tr>
        	<td>Gelar Depan</td>
            <td><input type="text" name="gelar_depan" value="{$gelar_depan}" size="10" class="required" /></td>
        </tr>
        <tr>
        	<td>Gelar Belakang</td>
            <td><input type="text" name="gelar_belakang" value="{$gelar_belakang}" size="10" class="required" /></td>
        </tr>
       	<tr>
        	<td>Semester</td>
            <td>
            	<select name="semester" id="semester" class="required">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER == $data_semester_aktif[0]['ID_SEMESTER']}selected="selected"{/if}>{$data.THN_AKADEMIK_SEMESTER} ({$data.NM_SEMESTER})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Fakultas</td>
            <td>
            	<select id="fakultas"  name="fakultas" class="required">
                	<option value="">PILIH FAKULTAS</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Jenjang</td>
            <td>
            	<select id="jenjang" name="jenjang" class="required">
                    <option value="">PILIH JENJANG</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
            </td>
        </tr>
        <tr>
        	<td>Program Studi</td>
            <td>
            	<select id="program_studi" name="program_studi" class="required">
                    <option value="">PILIH PRODI</option>
                    {foreach $data_program_studi as $data}
                         <option value="{$data.ID_PROGRAM_STUDI}" {if $program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Jalur</td>
            <td>
            	<select id="jalur" name="jalur" class="required">
                    <option value="">PILIH JALUR</option> 
                   {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $jalur==$data.ID_JALUR}selected="true"{/if}>{$data.NM_JALUR}</option>
                    {/foreach}                    
                </select>
            </td>
        </tr>
    </table>
</form>
{/if}

{literal}
    <script type="text/javascript">
		$('#gen_mhs').validate();
		
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$("#biaya_kuliah").click(function(){
			$.ajax({
                type:'post',
                url:'getBiaya.php',
                data:'id_program_studi='+$('#program_studi').val()+'&id_semester='+$('#semester_bayar').val()+'&id_jalur='+$('#jalur').val(),
                success:function(data){
                    $('#biaya').html(data);
                }                    
            })
		});
		</script>
{/literal}