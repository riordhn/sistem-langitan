<div class="center_title_bar">Cetak KTM Mahasiswa</div>

<table>
    <tr>
        <td>
            <label>No Ujian/ NIM :</label>
        </td>
        <td>
            <form action="cetak-ktm.php" method="get">
            <input type="search" name="nim_mhs" {if !empty($nim_mhs)} value="{$nim_mhs}" {/if} />
            <input type="submit" value="Cari" />
            </form>
        </td>
    </tr>
</table>
            
{if !empty($mahasiswa)}
<form method="get" action="cetak-ktm-pdf.php" target="_new">
    <table cellpadding="2">
        <tr>
            <td>Nama</td><td>{$mahasiswa.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td>NIM</td><td>{$mahasiswa.NIM_MHS}</td>
        </tr>
        <tr>
            <td>Program Studi</td><td>{$mahasiswa.NM_JENJANG} - {$mahasiswa.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>
                <input type="hidden" name="nim" value="{$nim_mhs}" />
                <textarea rows="2" cols="80" name="alamat" style='font-family: "Source Sans Pro", "Trebuchet MS"'>{$alamat}</textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                <input type="submit" value="Cetak" />
            </td>
        </tr>
    </table>
</form>
{/if}