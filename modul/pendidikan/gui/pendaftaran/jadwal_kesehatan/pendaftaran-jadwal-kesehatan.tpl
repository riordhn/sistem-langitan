<div class="center_title_bar">JADWAL TES KESEHATAN</div>

<form action="pendaftaran-jadwal-kesehatan.php" method="post" id="f2">
    <table>
        <tr>
            <th colspan="2">
                Parameter
            </th>
        </tr>
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="id_penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.post.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center"><input type="submit" value="Tampil"></td>
        </tr>
    </table>
</form>

{if isset($view_kesehatan)}

	             <table>
                    <tr>
                        <th>No</th>
                        <th>Tgl Tes</th>
                        <th>Kelompok</th>
                        <th>Jam</th>
                        <th>Kapasitas</th>
						<th>Terisi</th>
                    </tr>
                    {$no = 1}
                    {foreach $view_kesehatan as $data}
                    <tr>
                        <td>{$no++}</td>
                        <td>{$data.TGL_TEST}</td>
                        <td>{$data.KELOMPOK_JAM}</td>
                        <td>{$data.JAM_MULAI}:{$data.MENIT_MULAI} - {$data.JAM_SELESAI}:{$data.MENIT_SELESAI}</td>
                        <td>{$data.KAPASITAS}</td>
						<td>{$data.TERISI}</td>
                    </tr>
                    {/foreach}
                </table>
				
{/if}