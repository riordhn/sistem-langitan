<div class="center_title_bar">HASIL TES KESEHATAN</div>
<form action="pendaftaran-hasil-kesehatan.php" method="get" id="f2">
    <table>
        <tr>
            <th colspan="2">
                Parameter
            </th>
        </tr>
        <tr>
            <td>Penerimaan</td>
            <td>
                <select name="id_penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.get.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
		<tr>
			<td>Hasil Tes</td>
			<td>
				<select name="hasil">
					<option value="">Semua</option>
					<option value="lulus" {if $smarty.get.hasil == "lulus"}selected="selected"{/if}>Lulus</option>
					<option value="tidak_lulus" {if $smarty.get.hasil == "tidak_lulus"}selected="selected"{/if}>Tidak Lulus</option>
					<option value="belum_tes" {if $smarty.get.hasil == "belum_tes"}selected="selected"{/if}>Belum Tes</option>
				</select>
			</td>
		</tr>
        <tr>
            <td colspan="2" style="text-align: center"><input type="submit" value="Tampil"></td>
        </tr>
    </table>
</form>
                
 {if isset($view_kesehatan)}
     <table>
         <tr>
             <th>No</th>
             <th>No Ujian</th>
             <th>Nama</th>
             <th>Program Studi</th>
             <th>Hasil Tes Kesehatan</th>
             <th>Tanggal Tes Kesehatan</th>
             <th>Hasil Tes ELPT</th>
             <th>Tanggal Tes ELPT</th>
         </tr>
         {$no = 1}
         {foreach $view_kesehatan as $data}
         <tr>
             <td>{$no++}</td>
             <td>{$data.NO_UJIAN}</td>
             <td>{$data.NM_C_MHS}</td>
             <td>{$data.NM_PROGRAM_STUDI}</td>
             <td>{if $data.KESEHATAN_KESIMPULAN_AKHIR == 1}Lulus{elseif $data.KESEHATAN_KESIMPULAN_AKHIR == 0 and $data.TGL_VERIFIKASI_KESEHATAN != ''}Tidak Lulus{else}Belum Tes{/if}</td>
             <td>{$data.TGL_TES_KESEHATAN}</td>
             <td>{if $data.TGL_VERIFIKASI_ELPT != '' and $data.ELPT_SCORE != 0}{$data.ELPT_SCORE}{else}Belum Tes{/if}</td>
             <td>{$data.TGL_TES_TOEFL}</td>
         </tr>
         {/foreach}
     </table>
 {/if}    