{if !empty($cmb)}

<style type="text/css">

body table {
    background: #fff;
    width: 70%;
	border-collapse: collapse;
  border-spacing: 0;
}

</style>

    {if !empty($updated)}<h2>{$updated}</h2>{/if}

<form action="pendaftaran-foto-frame.php{if !empty($smarty.get.nim_mhs)}?nim_mhs={$smarty.get.nim_mhs}{/if}" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
<input type="hidden" name="no_ujian" value="{$cmb.NO_UJIAN}" />
<input type="hidden" name="nim_mhs" value="{$cmb.NIM_MHS}" />
<table border="1" cellpadding="2">
    <tr>
        <td>Nama</td><td>{$cmb.NM_C_MHS}</td>
    </tr>
	<tr>
        <td>NIM</td><td>{$cmb.NIM_MHS}</td>
    </tr>
	<tr>
        <td>No Ujian</td><td>{$cmb.NO_UJIAN}</td>
    </tr>
    <tr>
        <td>Program Studi</td><td>{$cmb.NM_JENJANG} - {$cmb.NM_PROGRAM_STUDI}</td>
    </tr>
	<tr>
        <td>Upload Foto</td>
        <td>
			<input type="hidden" id="foto" name="foto" {if $cmb.TGL_FOTO_KTM}value="1"{/if} />
            {* {if $cmb.TGL_FOTO_KTM != ''} *}
                <!-- <a target="_blank" href="../../foto_mhs/{$nama_singkat}/{$cmb.NO_UJIAN}.jpg" >{$cmb.NO_UJIAN}.jpg</a> -->
			{* {elseif $cmb.STATUS_AKADEMIK_MHS != ''} *}
				<a target="_blank" href="../../foto_mhs/{$nama_singkat}/{$cmb.NIM_MHS}.jpg?t=time" >{$cmb.NIM_MHS}.jpg</a>
			{* {/if} *}
            <input type="file" name="upload_foto" accept="image/jpeg" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>
{/if}