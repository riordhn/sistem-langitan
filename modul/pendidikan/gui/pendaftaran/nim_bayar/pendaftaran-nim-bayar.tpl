<div class="center_title_bar">GENERATE NIM CALON MAHASISWA</div>
{if !empty($result_set)}
    <h3>Hasil Generate</h3>
    
    <table>
        <tr>
            <th>No</th>
            <th>No Ujian</th>
            <th>Status</th>
            <th>NIM</th>
        </tr>
        {foreach $result_set as $r}
        <tr>
            <td>{$r@index + 1}</td>
            <td>{$r.NO_UJIAN}</td>
            <td>{$r.RESULT}</td>
            <td>{$r.NIM_MHS}</td>
        </tr>
        {/foreach}
    </table>
{/if}

<p><a href="pendaftaran-nim-bayar.php">Kembali</a></p>

{if !empty($cmb_set)}

<form action="pendaftaran-nim-bayar.php" method="post">
    <input type="hidden" name="mode" value="generate" />
<table style="font-size:12px">
    <tr>
        <th>No</th>
        <th style="width: 60px">No Ujian</th>
        <th>Nama</th>
        <th>Bayar</th>
		<th>Prodi</th>
        <th>Penerimaan</th>
        <th><input type="checkbox" id="cek_all" name="cek_all" /></th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr>
        <td class="center">{$cmb@index + 1}</td>
        <td class="center">{$cmb.NO_UJIAN}</td>
        <td>{$cmb.NM_C_MHS}</td>
        <td>{if $cmb.PEMBAYARAN > 0}Sudah{else}<font style="color: #f00">Belum</font>{/if}</td>
		<td>{$cmb.NM_PROGRAM_STUDI}</td>
        <td>{$cmb.PENERIMAAN}</td>
        <td>
            {if $cmb.NIM_MHS == ''}
            <input type="checkbox" name="no_ujian[]" value="{$cmb.NO_UJIAN}" class="cek"/>
            {/if}
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="7" class="center"><input type="submit" value="Generate" /></td>
    </tr>
</table>

</form>
{/if}

{literal}
    <script type="text/javascript">
	
		$("#cek_all").click(function(){
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
		});
		
    </script>
{/literal}