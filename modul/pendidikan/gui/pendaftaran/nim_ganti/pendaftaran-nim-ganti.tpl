<div class="center_title_bar">GANTI NIM</div>
<form action="pendaftaran-nim-ganti.php" method="post">
	<table>
    	<tr>
        	<td>NIM</td>
            <td><input type="text" name="nim" /></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>


{if (isset($mhs) and $mhs <> null)}
<form action="pendaftaran-nim-ganti.php" method="post"  id="gen_mhs">
	<table>
    	<tr>
        	<th colspan="2">BIODATA MAHASISWA</th>
        </tr>
   		 <tr>
        	<td>NIM Lama</td>
            <td>{$mhs[0]['NIM_MHS']}</td>
        </tr>
        {if isset($nim_baru)}
        <tr>
        	<td>NIM Baru</td>
            <td>{$nim_baru}</td>
        </tr>
        {/if}
    	<tr>
        	<td>Nama</td>
            <td>{$mhs[0]['NM_PENGGUNA']}</td>
        </tr>
       	<tr>
        	<td>Semester Masuk</td>
            <td>
            	<select name="semester" id="semester" class="required">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER == $data_semester_aktif[0]['ID_SEMESTER'] or $data.ID_SEMESTER  == $smarty.request.semester}selected="selected"{/if}>{$data.THN_AKADEMIK_SEMESTER} ({$data.NM_SEMESTER})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Fakultas</td>
            <td>
            	<select id="fakultas"  name="fakultas" class="required">
                	<option value="">PILIH FAKULTAS</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Jenjang</td>
            <td>
            	<select id="jenjang" name="jenjang" class="required">
                    <option value="">PILIH JENJANG</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
            </td>
        </tr>
        <tr>
        	<td>Program Studi</td>
            <td>
            	<select id="program_studi" name="program_studi" class="required">
                    <option value="">PILIH PRODI</option>
                </select>
            </td>
        </tr>
        <tr>
        	<td>Keterangan</td>
            <td>
            	<textarea name="keterangan" class="required">{$smarty.request.keterangan}</textarea>
            </td>
        </tr>
        <tr>
        	<td colspan="2" style="text-align:center">
            	<input type="hidden" value="{$mhs[0]['NIM_MHS']}" name="nim_lama" />
            	<input type="hidden" value="generate" name="generate" />
            	<input type="submit" value="Generate"/>
            </td>
        </tr>
    </table>
</form>

{/if}

{literal}
    <script type="text/javascript">
		$('#gen_mhs').validate();
		
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		</script>
{/literal}