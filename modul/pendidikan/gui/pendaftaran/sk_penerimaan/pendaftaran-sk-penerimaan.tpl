<div class="center_title_bar">SK PENERIMAAN</div>

<form action="pendaftaran-sk-penerimaan.php" method="post">
    <table>
        <tr>
            <th colspan="2">Pencarian</th>
        </tr>
        <tr>
            <td>NIM / Nomer Ujian/ Nama</td>
            <td><input type="text" name="nim" />
                <input type="submit" value="Cari" />
                <input type="hidden" name="mode" value="view" />
            </td>
        </tr>
    </table>
</form>

{if isset($penerimaan_c)}

<table>
	<tr>
		<th>No</th>
		<th>No Ujian</th>
		<th>NIM</th>
		<th>Nama</th>
		<th>Program Studi</th>
		<th>Jalur</th>
		<th>Aksi</th>
	</tr>
	{$no = 1}
	{foreach $penerimaan_c as $data}
	<tr>
		<td>{$no++}</td>
		<td>{$data.NO_UJIAN}</td>
		<td>{$data.NIM_MHS}</td>
		<td>{$data.NM_C_MHS}</td>
		<td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
		<td>{$data.NM_JALUR}</td>
		<td><a href="pendaftaran-sk-penerimaan.php?detail={$data.ID_C_MHS}">Detail</a></td>
	</tr>
	{/foreach}
</table>
{else if isset($penerimaan)}
<table>
	<tr>
		<th colspan="2" style="text-align:center">BIODATA</th>
	</tr>
	<tr>
		<td>NIM</td>
		<td>{$penerimaan.NIM_MHS}</td>
	</tr>
	<tr>
		<td>NO UJIAN</td>
		<td>{$penerimaan.NO_UJIAN}</td>
	</tr>
	<tr>
		<td>KODE VOUCHER</td>
		<td>{$penerimaan.KODE_VOUCHER}</td>
	</tr>
    <tr>
		<td>TGL LAHIR</td>
		<td>{$penerimaan.TGL_LAHIR}</td>
	</tr>
	<tr>
		<td>NAMA</td>
		<td>{$penerimaan.NM_C_MHS}</td>
	</tr>
	<tr>
		<td>PRODI</td>
		<td>{$penerimaan.NM_JENJANG} - {$penerimaan.NM_PROGRAM_STUDI}</td>
	</tr>
	<tr>
		<td>NAMA PENERIMAAN</td>
		<td>{$penerimaan.NM_PENERIMAAN}</td>
	</tr>
	<tr>
		<td>NAMA PENETAPAN</td>
		<td>{$penerimaan.NM_PENETAPAN}</td>
	</tr>
	<tr>
		<td>NO SURAT</td>
		<td>{$penerimaan.NO_SURAT}</td>
	</tr>
	<tr>
		<td>TGL PENETAPAN</td>
		<td>{$penerimaan.TGL_PENETAPAN}</td>
	</tr>
    <tr>
		<td>TGL DITERIMA</td>
		<td>{$penerimaan.TGL_DITERIMA}</td>
	</tr>
	<tr>
		<td>PENERIMAAN TAHUN</td>
		<td>{$penerimaan.TAHUN} ({$penerimaan.SEMESTER})</td>
	</tr>
	<tr>
		<td>JUDUL SK</td>
		<td style="text-align:center">
			{$penerimaan.BARIS_1}<br />
			{$penerimaan.BARIS_2}<br />
			{$penerimaan.BARIS_3}<br />
			SEMESTER {$penerimaan.SEMESTER|upper} TAHUN AKADEMIK {$penerimaan.TAHUN}/{$penerimaan.TAHUN+1} UNIVERSITAS AIRLANGGA
		</td>
	</tr>
</table>
{/if}
