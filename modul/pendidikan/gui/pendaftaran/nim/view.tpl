<div class="center_title_bar">Generate NIM Calon Mahasiswa</div>

<form action="pendaftaran-nim.php" method="get">
    <table>
        <tr>
            <th colspan="2">Pencarian</th>
        </tr>
        <tr>
            <td>Nomer Ujian</td>
            <td><input type="text" name="no_ujian" />
                <input type="submit" value="Cari" />
                <input type="hidden" name="mode" value="cari" />
            </td>
        </tr>
    </table>
</form>

<table>
    <tr>
        <th>No</th>
        <th>Fakultas</th>
        <th></th>
    </tr>
    {foreach $fakultas_set as $f}
    <tr>
        <td class="center">{$f@index + 1}</td>
        <td>Fakultas {$f.NM_FAKULTAS}</td>
        <td><a href="pendaftaran-nim.php?mode=prodi&id_fakultas={$f.ID_FAKULTAS}">Lihat</a></td>
    </tr>
    {/foreach}
</table>