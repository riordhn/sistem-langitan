<div class="center_title_bar">Generate NIM Mahasiswa</div>

<h2>{$program_studi.NM_PROGRAM_STUDI}</h2>

{if !empty($result_set)}
    <h3>Hasil Generate</h3>
    
    <table>
        <tr>
            <th>No</th>
            <th>No Ujian</th>
            <th>Status</th>
            <th>NIM</th>
        </tr>
        {foreach $result_set as $r}
        <tr>
            <td>{$r@index + 1}</td>
            <td>{$r.NO_UJIAN}</td>
            <td>{$r.RESULT}</td>
            <td>{$r.NIM_MHS}</td>
        </tr>
        {/foreach}
    </table>
{/if}

<p><a href="pendaftaran-nim.php?mode=prodi&id_fakultas={$program_studi.ID_FAKULTAS}">Kembali</a></p>

{if !empty($cmb_set)}

<form action="pendaftaran-nim.php?mode=detail&id_program_studi={$program_studi.ID_PROGRAM_STUDI}" method="post">
    <input type="hidden" name="mode" value="generate" />
    <input type="hidden" name="hasil_nim" value="{$hasil_nim}" />
<table>
    <tr>
        <th colspan="2">Setting Nomor Seri</th>
    </tr>
    <tr>
        <td>Format NIM</td>
        <td>{$hasil_nim}</td>
    </tr>
    <tr>
        <td>NIM Terakhir</td>
        <td>{$nim_terakhir}</td>
    </tr>
    <tr>
        <td>Panjang Seri</td>
        <td><input type="text" name="panjang_seri" value="" /></td>
    </tr>
    <tr>
        <td>Nomor Awal Seri</td>
        <td><input type="text" name="nomor_awal_seri" value="" /> Tanpa Angka 0 Di Depan. Misal: 1 atau 11</td>
    </tr>
    <tr>
        <td colspan="2">*) Default Panjang Seri Adalah 3. Dengan Nomor Awal Seri 1 (001).</td>
    </tr>
</table>
<table>
    <tr>
        <th>No</th>
        <th style="width: 60px">NIM</th>
        <th style="width: 60px">No Ujian</th>
        <th>Nama</th>
        <!-- <th>Bayar</th> -->
        <th>Penerimaan</th>
        <th></th>
    </tr>
    {foreach $cmb_set as $cmb}
    <tr>
        <td class="center">{$cmb@index + 1}</td>
        <td class="center">{$cmb.NIM_MHS}</td>
        <td class="center">{$cmb.NO_UJIAN}</td>
        <td>
            {if $cmb.NIM_MHS == ''}
                {$cmb.NM_C_MHS}
            {else}
                {$cmb.NM_PENGGUNA}
            {/if}
        </td>
        <!-- <td>{if $cmb.PEMBAYARAN > 0}Sudah{else}<font style="color: #f00">Belum</font>{/if}</td> -->
        <td>
            {$cmb.PENERIMAAN_NON_PASCA} - Jalur {$cmb.NM_JALUR}
        </td>
        <td class="center">
            {if $cmb.NIM_MHS == ''}
                <input type="checkbox" name="no_ujian[]" value="{$cmb.NO_UJIAN}"/>
            {else if $cmb.ID_PROGRAM_STUDI_MHS == $cmb.ID_PROGRAM_STUDI_CMB}
                {$cmb.NM_PROGRAM_STUDI}
            {else}
                Pindah ({$cmb.NM_PROGRAM_STUDI})
            {/if}
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="7" class="center"><input type="submit" value="Generate" /></td>
    </tr>
</table>

</form>
{else}
<h3> Belum Ada Mahasiswa Yang Ditetapkan! </h3>
{/if}

<a href="pendaftaran-nim.php?mode=prodi&id_fakultas={$program_studi.ID_FAKULTAS}">Kembali</a>