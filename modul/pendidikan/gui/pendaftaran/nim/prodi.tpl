<div class="center_title_bar">Generate NIM Calon Mahasiswa</div>

<h2>Fakultas {$fakultas.NM_FAKULTAS}</h2>
<p><a href="pendaftaran-nim.php">Kembali</a></p>

<table>
    <tr>
        <th>No</th>
        <th>Prodi</th>
        <th></th>
    </tr>
    {foreach $program_studi_set as $ps}
    <tr>
        <td class="center">{$ps@index + 1}</td>
        <td>{$ps.NM_JENJANG} - {$ps.NM_PROGRAM_STUDI}</td>
        <td><a href="pendaftaran-nim.php?mode=detail&id_program_studi={$ps.ID_PROGRAM_STUDI}">Lihat</a></td>
    </tr>
    {/foreach}
</table>

<a href="pendaftaran-nim.php">Kembali</a>