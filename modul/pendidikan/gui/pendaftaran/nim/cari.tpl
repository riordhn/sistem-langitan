<div class="center_title_bar">Generate NIM Calon Mahasiswa</div>

{if !empty($cmb)}
<form action="pendaftaran-nim.php?mode=detail&id_program_studi={$cmb.ID_PROGRAM_STUDI}" method="post">
    <input type="hidden" name="mode" value="generate" />
    <input type="hidden" name="hasil_nim" value="{$hasil_nim}" />

    <table>
        <tr>
            <th colspan="2">Setting Nomor Seri</th>
        </tr>
        <tr>
            <td>Format NIM</td>
            <td>{$hasil_nim}</td>
        </tr>
        <tr>
            <td>NIM Terakhir</td>
            <td>{$nim_terakhir}</td>
        </tr>
        <tr>
            <td>Panjang Seri</td>
            <td><input type="text" name="panjang_seri" value="" /></td>
        </tr>
        <tr>
            <td>Nomor Awal Seri</td>
            <td><input type="text" name="nomor_awal_seri" value="" /> Tanpa Angka 0 Di Depan. Misal: 1 atau 11</td>
        </tr>
        <tr>
            <td colspan="2"><strong>*) Panjang Seri dan Nomor Awal Seri <u>WAJIB</u> Diisi.</strong></td>
        </tr>
    </table>

    <table>
        <tr>
            <th style="width: 60px">NIM</th>
            <th style="width: 60px">No Ujian</th>
            <th>Nama</th>
            <!-- <th>Bayar</th> -->
            <th>Program Studi</th>
            <th>Penerimaan</th>
            <th></th>
        </tr>
        <tr>
            <td class="center">{$cmb.NIM_MHS}</td>
            <td class="center">{$cmb.NO_UJIAN}</td>
            <td>{$cmb.NM_C_MHS}</td>
            <!-- <td>{if $cmb.PEMBAYARAN > 0}Sudah{else}<font style="color: #f00">Belum</font>{/if}</td> -->
            <td>{$cmb.NM_PROGRAM_STUDI}</td>
            <td>{$cmb.PENERIMAAN}</td>
            <td>
                {if $cmb.NIM_MHS == ''}
                <input type="checkbox" name="no_ujian[]" value="{$cmb.NO_UJIAN}"/>
                {/if}
            </td>
        </tr>
        <tr>
            <td colspan="7" class="center"><input type="submit" value="Generate" /></td>
        </tr>
    </table>
</form>
{else}
    <h2>Nomer Ujian tidak ditemukan</h2>
{/if}
        
<a href="pendaftaran-nim.php">Kembali</a>