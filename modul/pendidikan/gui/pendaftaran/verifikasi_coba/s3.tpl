{literal}<style type="text/css">
.center         { text-align: center; }
.formulir       { width: 100% }
.formulir tr td { font-size: 13px; }
.formulir tr td h2 { margin: 0px; }
.formulir tr td h3 { margin: 0px; }
.formulir tr td h4 { margin: 0px; }
.formulir tr td.head { font-size: 18px; font-weight: bold; background: #233D0E; color: #fff; }
label.error        { display: none; color: #f00; font-size: 12px; }

/* BUTTON */
.button{-moz-border-radius:5px;-webkit-border-radius:5px;background:#777 url(button.png) repeat-x bottom;border:none;border-radius:5px;color:#fff;cursor:pointer;display:inline;font-weight:700;padding:5px 10px;text-shadow:1px 1px #666}
.button:hover{background-position:0 -48px}
.button:active{background-position:0 top;padding:6px 10px 4px;position:relative;top:1px}
.button.red{background-color:#e50000}
.button.purple{background-color:#9400bf}
.button.green{background-color:#58aa00}
.button.orange{background-color:#ff9c00}
.button.blue{background-color:#2c6da0}
.button.black{background-color:#333}
.button.white{background-color:#fff;color:#000;text-shadow:1px 1px #fff}
.button.small{font-size:75%;padding:3px 7px}
.button.small:hover{background-position:0 -50px}
.button.small:active{background-position:0 top;padding:4px 7px 2px}
.button.large{font-size:125%;padding:7px 12px}
.button.large:hover{background-position:0 -35px}
.button.large:active{background-position:0 top;padding:8px 12px 6px}
/* END BUTTON */
</style>{/literal}

<form action="pendaftaran-verifikasi.php{if !empty($smarty.get.no_ujian)}?no_ujian={$smarty.get.no_ujian}{/if}" method="post" id="verifikasi">
    <input type="hidden" name="mode" value="save_s3" />
    <input type="hidden" name="id_c_mhs" value="{$cmb.ID_C_MHS}" />
    
    <table style="width: 100%; margin-left: 0px" class="formulir">
        <tr>
            <td colspan="2" class="head">I. Program Studi</td>
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>{$ps.NM_FAKULTAS}</td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>{$ps.NM_JENJANG} - {$ps.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td>Minat Studi</td>
            <td>{$ps.NM_PRODI_MINAT}</td>
        </tr>
        <tr>
            <td>Kelas Pilihan</td>
            <td>{$ps.NM_PRODI_KELAS}</td>
        </tr>
        <tr>
            <td colspan="2" class="head">II. Data Diri</td>
        </tr>
        <tr>
            <td>No Ujian</td>
            <td style="font-size: 16px; font-weight: bold;">{$cmb.NO_UJIAN}</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>
                <input type="text" name="nm_c_mhs" maxlength="64" size="50" class="required" value="{$cmb.NM_C_MHS}"/>
                <br/>
                <label for="nm_c_mhs" class="error" style="display:none">* Isi nama lengkap</label>
            </td>
        </tr>
        <tr>
            <td>Gelar</td>
            <td>
                <input type="text" name="gelar" maxlength="20" size="20" class="required" value="{$cmb.GELAR}"/>
                <span>Cth : <strong>S.Si.</strong> atau <strong>S.H, M.H.</strong></span>
                <br/>
                <label for="gelar" class="error" style="display:none">* Isi gelar</label>
            </td>
        </tr>
        <tr>
            <td>Tempat dan Tanggal Lahir</td>
            <td>
                <select name="id_kota_lahir" class="required">
                    <option value=""></option>
                    {foreach $provinsi_set as $p}
                        <optgroup label="{$p.NM_PROVINSI}">
                        {foreach $p.kota_set as $k}
                            <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $cmb.ID_KOTA_LAHIR}selected="selected"{/if}>{$k.NM_KOTA}</option>
                        {/foreach}
                        </optgroup>
                    {/foreach}
                </select>,
                {html_select_date prefix="tgl_lahir_" field_order="DMY" start_year="-80" end_year="-14" time=$cmb.TGL_LAHIR}
                <br/>
                <label for="id_kota_lahir" class="error" style="display: none;">Pilih kota kelahiran</label>
            </td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>
                <textarea name="alamat" maxlength="250" cols="50" class="required">{$cmb.ALAMAT}</textarea>
            </td>
        </tr>
        <tr>
            <td>Kota</td>
            <td>
                <select name="id_kota" class="required">
                    <option value=""></option>
                    {foreach $provinsi_set as $p}
                        <optgroup label="{$p.NM_PROVINSI}">
                        {foreach $p.kota_set as $k}
                            <option value="{$k.ID_KOTA}" {if $k.ID_KOTA == $cmb.ID_KOTA}selected="selected"{/if}>{$k.NM_KOTA}</option>
                        {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>No Telp</td>
            <td><input type="text" name="telp" class="required" maxlength="32" value="{$cmb.TELP}"/></td>
        </tr>
        <tr>
            <td>Email</td>            
            <td><input type="text" name="email" class="required email" maxlength="50" size="50" value="{$cmb.EMAIL}" /></td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>
                <label><input type="radio" name="jenis_kelamin" value="1" class="required" {if $cmb.JENIS_KELAMIN==1}checked="checked"{/if} >Laki-Laki</label>
                <label><input type="radio" name="jenis_kelamin" value="2" {if $cmb.JENIS_KELAMIN==2}checked="checked"{/if}>Perempuan</label>
                <br/>
                <label for="jenis_kelamin" class="error" style="display: none;">Pilih salah satu</label>
        </tr>
        <tr>
            <td>Kewarganegaraan</td>
            <td>
                <label><input type="radio" name="kewarganegaraan" value="1" class="required" {if $cmb.KEWARGANEGARAAN==1}checked="checked"{/if} />Indonesia</label>
                <label><input type="radio" name="kewarganegaraan" value="2" {if $cmb.KEWARGANEGARAAN==2}checked="checked"{/if}/>W.N.A. <input type="text" name="kewarganegaraan_lain" maxlength="20" value="{$cmb.KEWARGANEGARAAN_LAIN}" /><label for="kewarganegaraan_lain" class="error" style="display: none;">Harus di isi</label></label>
                <br/>
                <label for="kewarganegaraan" class="error" style="display: none;">Pilih salah satu</label>
            </td>
        </tr>
        <tr>
            <td>Agama</td>
            <td>
                <label><input type="radio" name="id_agama" value="1" class="required" {if $cmb.ID_AGAMA==1}checked="checked"{/if} />Islam</label>
                <label><input type="radio" name="id_agama" value="2" {if $cmb.ID_AGAMA==2}checked="checked"{/if}/>Kristen Protestan</label>
                <label><input type="radio" name="id_agama" value="3" {if $cmb.ID_AGAMA==3}checked="checked"{/if}/>Kristen Katholik</label>
                <label><input type="radio" name="id_agama" value="4" {if $cmb.ID_AGAMA==4}checked="checked"{/if}/>Hindu</label>
                <label><input type="radio" name="id_agama" value="5" {if $cmb.ID_AGAMA==5}checked="checked"{/if}/>Budha</label>
                <label><input type="radio" name="id_agama" value="6" {if $cmb.ID_AGAMA==6}checked="checked"{/if}/>Lain-lain</label>
                <br/>
                <label for="id_agama" class="error" style="display: none;">Pilih salah satu</label></td>
        </tr>
        <tr>
            <td>Status Perkawinan</td>
            <td>
                <label><input type="radio" name="status_perkawinan" value="1" class="required" {if $cmb.STATUS_PERKAWINAN==1}checked="checked"{/if}/>Belum Kawin</label>
                <label><input type="radio" name="status_perkawinan" value="2" {if $cmb.STATUS_PERKAWINAN==2}checked="checked"{/if}/>Kawin</label>
                <label><input type="radio" name="status_perkawinan" value="3" {if $cmb.STATUS_PERKAWINAN==3}checked="checked"{/if}/>Bercerai</label>
                <label><input type="radio" name="status_perkawinan" value="4" {if $cmb.STATUS_PERKAWINAN==4}checked="checked"{/if}/>Kawin, Suami / Istri Meninggal</label>
                <br/>
                <label for="status_perkawinan" class="error" style="display: none;">Pilih salah satu</label></td>
            </td>
        </tr>
        <tr>
            <td>Pekerjaan / Jabatan</td>
            <td>
                <input type="text" name="pekerjaan" maxlength="30" size="30" value="{$cmp.PEKERJAAN}" />
            </td>
        </tr>
        <tr>
            <td>Instansi Asal</td>
            <td><input type="" name="asal_instansi" maxlength="50" size="50" value="{$cmp.ASAL_INSTANSI}" /></td>
        </tr>
        <tr>
            <td>Alamat Instansi</td>
            <td>
                <textarea name="alamat_instansi" maxlength="250" cols="50" >{$cmp.ALAMAT_INSTANSI}</textarea>
            </td>
        </tr>
        <tr>
            <td>Telp / Faks Instansi</td>
            <td><input type="" name="telp_instansi" maxlength="30" size="30" value="{$cmp.TELP_INSTANSI}" /></td>
        </tr>
        <tr>
            <td>NIP / NRP / NIS</td>
            <td>
                <input type="text" name="nrp" maxlength="20" size=20" value="{$cmp.NRP}" />
            </td>
        </tr>
        <tr>
            <td>KARPEG</td>
            <td><input type="text" name="karpeg" maxlength="30" size="30" value="{$cmp.KARPEG}" /></td>
        </tr>
        <tr>
            <td>Pangkat & Golongan</td>
            <td><input type="text" name="pangkat" maxlength="20" size="20" value="{$cmp.PANGKAT}" /></td>
        </tr>
        <tr>
            <td colspan="2" class="head">III. Pendidikan Sarjana (S1)</td>
        </tr>
        <tr>
            <td>Perguruan Tinggi</td>
            <td>
                <input type="text" name="ptn_s1" maxlength="30" size="30" value="{$cmp.PTN_S1}" class="required"/>
            </td>
        </tr>
        <tr>
            <td>Status Perguruan Tinggi</td>
            <td>
                <label><input type="radio" name="status_ptn_s1" value="1" {if $cmp.STATUS_PTN_S1==1}checked="checked"{/if} class="required"/>Negeri</label>
                <label><input type="radio" name="status_ptn_s1" value="2" {if $cmp.STATUS_PTN_S1==2}checked="checked"{/if}/>Swasta</label>
                <label><input type="radio" name="status_ptn_s1" value="3" {if $cmp.STATUS_PTN_S1==3}checked="checked"{/if}/>Luar Negeri</label>
                <br/>
                <label for="status_ptn_s1" class="error" style="display:none;">Pilih salah satu</label>
            </td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td><input type="text" name="prodi_s1" maxlength="30" size="30" value="{$cmp.PRODI_S1}" class="required"/></td>
        </tr>
        <tr>
            <td>Tanggal Masuk</td>
            <td>{html_select_date prefix="tgl_masuk_s1_" field_order="DMY" start_year=-40 time=$cmp.TGL_MASUK_S1}</td>
        </tr>
        <tr>
            <td>Tanggal Lulus</td>
            <td>{html_select_date prefix="tgl_lulus_s1_" field_order="DMY" start_year=-40 time=$cmp.TGL_LULUS_S1}</td>
        </tr>
        <tr>
            <td>Lama Studi</td>
            <td><input type="text" name="lama_studi_s1" maxlength="4" size="4" class="number required" value="{$cmp.LAMA_STUDI_S1}" /> tahun</td>
        </tr>
        <tr>
            <td>Index Prestasi</td>
            <td><input type="text" name="ip_s1" maxlength="5" size="5" class="number required" value="{$cmp.IP_S1}" /></td>
        </tr>
        <tr>
            <td colspan="2" class="head">III. Pendidikan Magister (S2)</td>
        </tr>
        <tr>
            <td>Perguruan Tinggi</td>
            <td>
                <input type="text" name="ptn_s2" maxlength="30" size="30" value="{$cmp.PTN_S2}" class="required"/>
            </td>
        </tr>
        <tr>
            <td>Status Perguruan Tinggi</td>
            <td>
                <label><input type="radio" name="status_ptn_s2" value="1" {if $cmp.STATUS_PTN_S2==1}checked="checked"{/if} class="required"/>Negeri</label>
                <label><input type="radio" name="status_ptn_s2" value="2" {if $cmp.STATUS_PTN_S2==2}checked="checked"{/if}/>Swasta</label>
                <label><input type="radio" name="status_ptn_s2" value="3" {if $cmp.STATUS_PTN_S2==3}checked="checked"{/if}/>Luar Negeri</label>
                <br/>
                <label for="status_ptn_s2" class="error" style="display:none;">Pilih salah satu</label>
            </td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td><input type="text" name="prodi_s2" maxlength="30" size="30" value="{$cmp.PRODI_S2}" class="required"/></td>
        </tr>
        <tr>
            <td>Tanggal Masuk</td>
            <td>{html_select_date prefix="tgl_masuk_s2_" field_order="DMY" start_year=-30 time=$cmp.TGL_MASUK_S2}</td>
        </tr>
        <tr>
            <td>Tanggal Lulus</td>
            <td>{html_select_date prefix="tgl_lulus_s2_" field_order="DMY" start_year=-30 time=$cmp.TGL_LULUS_S2}</td>
        </tr>
        <tr>
            <td>Lama Studi</td>
            <td><input type="text" name="lama_studi_s2" maxlength="4" size="4" class="number required" value="{$cmp.LAMA_STUDI_S2}" /> tahun</td>
        </tr>
        <tr>
            <td>Index Prestasi</td>
            <td><input type="text" name="ip_s2" maxlength="5" size="5" class="number required" value="{$cmp.IP_S2}" /></td>
        </tr>
        <tr>
            <td>Jumlah Karya Ilmiah</td>
            <td><input type="text" name="jumlah_karya_ilmiah" maxlength="2" size="2" class="number required" value="{$cmp.JUMLAH_KARYA_ILMIAH}" /></td>
        </tr>
        <tr>
            <td colspan="2" class="head">IV. Lain-Lain</td>
        </tr>
        <tr>
            <td>Lamaran ke PPS Unair</td>
            <td>
                <label><input type="radio" name="status_lamaran_pps" value="1" {if $cmp.STATUS_LAMARAN_PPS==1}checked="checked"{/if}/>Pernah, Tahun <input type="text" name="tahun_lamaran_pps" maxlength="4" size="4" value="{$cmp.TAHUN_LAMARAN_PPS}" /></label>
                <label><input type="radio" name="status_lamaran_pps" value="0" {if $cmp.STATUS_LAMARAN_PPS==0}checked="checked"{/if}/>Tidak pernah</label>
                {literal}<script type="text/javascript">
                    $('input:radio[name="status_lamaran_pps"]').click(function() {
                        if (this.value == 1) {
                            $('input[name="tahun_lamaran_pps"]').focus();
                        }
                    });
                    </script>{/literal}
            </td>
        </tr>
        <tr>
            <td>Sumber Dana</td>
            <td>
                <label><input type="radio" name="sumber_biaya" value="5" {if $cmb.SUMBER_BIAYA==5}checked="checked"{/if}/>Sendiri</label>
                <label><input type="radio" name="sumber_biaya" value="6" {if $cmb.SUMBER_BIAYA==6}checked="checked"{/if}/>Instansi</label>
                <label><input type="radio" name="sumber_biaya" value="7" {if $cmb.SUMBER_BIAYA==7}checked="checked"{/if}/>BPPS</label>
                <label><input type="radio" name="sumber_biaya" value="8" {if $cmb.SUMBER_BIAYA==8}checked="checked"{/if}/>Kemenkes</label>
                <br/>
                <label for="sumber_biaya" class="error" style="display:none;">Pilih salah satu</label>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="head">V. Berkas</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <label><input type="checkbox" name="berkas_ijazah" class="required" {if $cmd.BERKAS_IJAZAH==1}checked="checked"{/if}/>Ijazah (Asli dan Foto copy)</label>
                <label for="berkas_ijazah" class="error" style="display: none">Harus ada</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <label><input type="checkbox" name="berkas_transkrip" class="required" {if $cmd.BERKAS_TRANSKRIP==1}checked="checked"{/if}/>Transkrip</label>
                <label for="berkas_transkrip" class="error" style="display: none">Harus ada</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <label><input type="checkbox" name="berkas_kesehatan" class="required" {if $cmd.BERKAS_KESEHATAN==1}checked="checked"{/if}/>Surat Kesehatan</label>
                <label for="berkas_kesehatan" class="error" style="display: none">Harus ada</label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <label><input type="checkbox" name="berkas_skck" class="required" {if $cmd.BERKAS_SKCK==1}checked="checked"{/if}/>SKCK / SKKB</label>
                <label for="berkas_skck" class="error" style="display: none">Harus ada</label>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
                
                
{literal}<script type="text/javascript">
    $('#verifikasi').validate();
</script>{/literal}