<div class="center_title_bar">GENERATE NIM MAHASISWA PROFESI YANG PERNAH KULIAH DI UNAIR</div>
<form name="f1" id="f1" action="pendaftaran-nim-profesi.php" method="post">
<table>
        <tr>
            <td>Fakultas</td>
            <td>
                <select id="fakultas" name="fakultas" class="required">
				<option value="">PILIH FAKULTAS</option> 
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
			<td>Jenjang</td>
			<td>
				<select id="jenjang" name="jenjang" class="required">
                    <option value="">PILIH JENJANG</option> 
                   {foreach $data_jenjangs1 as $data}
                        <option value="{$data.ID_JENJANG}" {if $jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
		</tr>
		<tr>
			<td>Program Studi</td>
            <td>
                <select id="program_studi" name="program_studi" class="required">
                    <option value="">PILIH PRODI</option>
                    {foreach $data_program_studi as $data}
                        {if $program_studi!=''}
                            <option value="{$data.ID_PROGRAM_STUDI}" {if $program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
                        {else if $fakultas!=''&&$program_studi==''}
                            {if $data.ID_FAKULTAS==$fakultas}
                                <option value="{$data.ID_PROGRAM_STUDI}" >{$data.NM_PROGRAM_STUDI}</option>
                            {/if}
                        {/if}
                    {/foreach}
                </select>
            </td>
		</tr>
        <td>Periode Wisuda</td>
            <td>
            	<select name="periode">
                	<option value="">Semua</option>
                	{foreach $periode as $data}
                	<option value="{$data.ID_TARIF_WISUDA}" {if $smarty.post.periode==$data.ID_TARIF_WISUDA} selected="selected" {/if}>{$data.NM_TARIF_WISUDA}</option>
					{/foreach}
            	</select>
            </td>
		<tr>
        	<td colspan="4" class="center">
            	<input type="submit" value="Tampil"/>
            	<input type="hidden" name="tampil" value="ok"/>
            </td>
        </tr>
</table>
</form>


{if isset($mhs)}
<form name="f2" id="f2" action="pendaftaran-nim-profesi.php" method="post">
	
    	<table>
    	<tr>
        	<th colspan="2">KOMPONEN</th>
        </tr>
        <tr>
        	<td>Gelar Depan</td>
            <td><input type="text" name="gelar_depan" size="10" /></td>
        </tr>
        <tr>
        	<td>Gelar Belakang</td>
            <td><input type="text" name="gelar_belakang" size="10" /></td>
        </tr>
       	<tr>
        	<td>Semester</td>
            <td>
            	<select name="semester" id="semester" class="required">
                    {foreach $data_semester as $data}
                        <option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER == $data_semester_aktif[0]['ID_SEMESTER']}selected="selected"{/if}>{$data.THN_AKADEMIK_SEMESTER} ({$data.NM_SEMESTER})</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Fakultas</td>
            <td>
            	<select id="fakultas2"  name="fakultas2" class="required">
                	<option value="">PILIH FAKULTAS</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}">{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>Jenjang</td>
            <td>
            	<select id="jenjang2" name="jenjang2" class="required">
                    <option value="">PILIH JENJANG</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}">{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
            </td>
        </tr>
        <tr>
        	<td>Program Studi</td>
            <td>
            	<select id="program_studi2" name="program_studi2" class="required">
                    <option value="">PILIH PRODI</option>
                </select>
            </td>
        </tr>
	</table>
		
	
	<table>
    	<tr>
        	<th>No</th>
        	<th>NIM</th>
            <th>Nama</th>
            <th>Pilih <input type="checkbox" id="cek_all" name="cek_all" /></th>
        </tr>
        {$no=1}
        {foreach $mhs as $data}
        	<tr>
            	<td>{$no++}</td>
            	<td>{$data.NIM_MHS}</td>
                <td>{$data.NM_PENGGUNA}</td>
                <td style="text-align:center">
                	<input type="checkbox" id="cek" class="cek" name="cek{$no}" value="1" />
                	<input type="hidden" name="nim_lama{$no}" value="{$data.NIM_MHS}" />
                </td>
            </tr>
        {/foreach}
        <tr>
        	<td colspan="5" class="center">
            	<input type="submit" value="Generate" />
            	<input type="hidden" value="generate" name="generate" />
                <input type="hidden" value="{$no}" name="no" />
				<input type="hidden" value="{$program_studi}" name="program_studi" />
            </td>
        </tr>
    </table>
</form>
{/if}

{if isset($mhs_baru)}

	<table>
    	<tr>
        	<th>No</th>
        	<th>NIM Lama</th>
			<th>NIM Baru</th>
            <th>Nama</th>
        </tr>
        {$no=1}
        {foreach $mhs_baru as $data}
        	<tr>
            	<td>{$no++}</td>
            	<td>{$data.NIM_MHS_LAMA}</td>
				<td>{$data.NIM_MHS_BARU}</td>
                <td>{$data.NM_PENGGUNA}</td>
            </tr>
        {/foreach}
        <tr>
        	<td colspan="4" class="center">
				<input type="button" value="Cetak" onclick="window.open('excel-pendaftaran-nim-profesi.php?get_semester={$get_semester}&get_prodi={$get_prodi}','baru2')"/>
            </td>
        </tr>
    </table>
{/if}

{literal}
    <script type="text/javascript">
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });

        $('#fakultas2').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas2').val()+'&id_jenjang='+$('#jenjang2').val(),
                success:function(data){
                    $('#program_studi2').html(data);
                }                    
            })
        });
		
		$('#jenjang2').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas2').val()+'&id_jenjang='+$('#jenjang2').val(),
                success:function(data){
                    $('#program_studi2').html(data);
                }                    
            })
        });

		$('#f1').validate();
		$('#f2').validate();
		
		$("#cek_all").click(function(){
				var checked_status = this.checked;
				$(".cek").each(function()
				{
					this.checked = checked_status;
				});
		});
		
    </script>
{/literal}