<div class="center_title_bar">History Aktivitas Mahasiswa</div>
<table border="0">
    <tr>
        <td>Fakultas</td>
        <td><select><option>Sains dan Teknologi</option><option>Kesehatan Masyarakat</option></select></td>
        <td>Prodi</td>
        <td><select><option>Sistem Informasi</option></select></td>
        <td>NIM</td>
        <td><input type="text" value="080916062" /><button onclick="$('#tabel-history').fadeIn('slow');">Tampilkan</button></td>
    </tr>
</table>
<table style="width: 689px;" id="tabel-history">
    <thead>
        <tr>
            <td colspan="2">Nama: M. Fathoni<br/>
            NIM: 080916062<br/>
            Jurusan: Sistem Informasi<br/>
            Angkatan: 2009/2010</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th style="width: 120px">Tanggal / Jam</th>
            <th>Aktivitas</th>
        </tr>
        <tr>
            <td>14/04/2011 14:03:02</td>
            <td>Login.</td>
        </tr>
        <tr>
            <td>14/04/2011 15:04:36</td>
            <td>Input data nilai [S001] Riset Operasi : C</td>
        </tr>
        <tr>
            <td>14/04/2011 15:07:20</td>
            <td>Mensubmit Feedback Dosen</i></td>
        </tr>
        <tr>
            <td>14/04/2011 16:03:22</td>
            <td>Logout.</td>
        </tr>
    </tbody>
</table>