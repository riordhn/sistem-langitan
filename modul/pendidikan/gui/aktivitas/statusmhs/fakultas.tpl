<div class="center_title_bar">Status Mahasiswa - Pilih Fakultas</div>

<table style="width: 400px;">
    <tr>
        <th>No</th>
        <th>Nama Fakultas</th>
    </tr>
    {for $i=0 to $fakultas_set->Count()-1}
    {$fakultas=$fakultas_set->Get($i)}
    <tr>
        <td class="center" style="width: 10%;">{$i+1}</td>
        <td><a href="aktivitas-statusmhs.php?mode=prodi&id_fakultas={$fakultas->ID_FAKULTAS}">{$fakultas->NM_FAKULTAS}</a></td>
    </tr>
    {/for}
</table>