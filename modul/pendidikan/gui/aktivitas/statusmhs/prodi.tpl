<div class="center_title_bar">Status Mahasiswa - Pilih Prodi</div>
<h2>Fakultas : {$fakultas->NM_FAKULTAS}</h2>
<table style="width: 400px">
    <tr>
        <th>No</th>
        <th>Nama Prodi</th>
    </tr>
    {for $i=0 to $fakultas->PROGRAM_STUDIs->Count()-1}
    {$prodi=$fakultas->PROGRAM_STUDIs->Get($i)}
    <tr>
        <td class="center" style="width: 10%;">{$i+1}</td>
        <td><a href="aktivitas-statusmhs.php?mode=view&id_program_studi={$prodi->ID_PROGRAM_STUDI}">{$prodi->NM_PROGRAM_STUDI}</a></td>
    </tr>
    {/for}
</table>
<button href="aktivitas-statusmhs.php">Kembali ke pilihan fakultas</button>