<div class="center_title_bar">Status Mahasiswa</div>

<h4>Prodi : {$program_studi->NM_PROGRAM_STUDI}</h4>

<form action="aktivitas-statusmhs.php?mode=view&id_program_studi={$program_studi->ID_PROGRAM_STUDI}" method="post">
<table>
    <tr>
        <th>No</th>
        <th>NIM</th>
        <th>Nama</th>
        <th>Status</th>
        <th colspan="2">Approval</th>
        <th>Keterangan</th>
    </tr>
    {for $i=0 to $mahasiswa_set->Count()-1}
    {$mahasiswa=$mahasiswa_set->Get($i)}
    <tr>
        <td class="center">{$i+1}</td>
        <td>{$mahasiswa->NIM_MHS}</td>
        <td>{$mahasiswa->PENGGUNA->NM_PENGGUNA}</td>
        <td>
            {$mahasiswa->PENGGUNA->SEJARAH_STATUSs->Get(0)->STATUS_PENGGUNA->NM_STATUS_PENGGUNA}
        </td>
        <td class="center">
            <label class="radio">Ya <input type="radio" name="P{$mahasiswa->ID_PENGGUNA}" value="1" /></label>
        </td>
        <td class="center">
            <label class="radio">Tidak <input type="radio" name="P{$mahasiswa->ID_PENGGUNA}" value="0" /></label>
        </td>
        <td>
            <input type="text" name="C{$mahasiswa->ID_PENGGUNA}" maxlength="64" />
        </td>
    </tr>
    {if $i==$mahasiswa_set->Count()-1}
    <tr>
        <td colspan="4"></td>
        <td class="center"><span class="anchor-span" action="select-all-true">Pilih Semua</span></td>
        <td class="center"><span class="anchor-span" action="select-all-false">Pilih Semua</span></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="7" class="center"><input type="submit" value="Simpan" /></td>
    </tr>
    {/if}
    {forelse}
    <tr>
        <td colspan="7" class="center">Data tidak ada</td>
    </tr>
    {/for}
</table>
</form>
<button href="aktivitas-statusmhs.php?mode=prodi&id_fakultas={$program_studi->ID_FAKULTAS}">Kembali ke pilihan prodi</button>
{literal}
<script type="text/javascript">
$('span[action="select-all-true"]').bind('click', function(e) {
    e.preventDefault();
    $('input:radio[value=1]').attr('checked', 'checked');
    return false;
});
$('span[action="select-all-false"]').bind('click', function(e) {
    e.preventDefault();
    $('input:radio[value=0]').attr('checked', 'checked');
    return false;
});
</script>
{/literal}