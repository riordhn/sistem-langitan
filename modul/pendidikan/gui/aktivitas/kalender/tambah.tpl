<div class="center_title_bar">Jadwal Kegiatan Akademik - Tambah</div>
<form action="aktivitas-kalender.php" method="post">
<table>
	<tr>
		<td>
			Fakultas / Univeristas
		</td>
		<td>
			<select name="fakultas">
					<option value="">Universitas</option>
			{foreach $fakultas as $data}
					<option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS == $smarty.post.fakultas}selected="selected"{/if}>{$data.NM_FAKULTAS}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Semester</td>
		<td>
			<select name="semester">
			{foreach $semester as $data}
					<option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER == $smarty.post.semester}selected="selected"{/if}>{$data.THN_AKADEMIK_SEMESTER} ({$data.NM_SEMESTER})</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="text-align:center">
			<input type="submit" value="Tampil" />
		</td>
	</tr>
</table>
</form>

<form action="aktivitas-kalender.php" method="post" id="f2">
	<table>
		<tr>
			<th>No</th>
			<th>Nama Kegiatan</th>
			<th>Deskripsi Keigatan</th>
			<th>Semester</th>
			<th>Tanggal Mulai</th>
			<th>Tanggal Selesai</th>
		</tr>
		{$no = 1}
		{foreach $jadwal as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.NM_KEGIATAN}</td>
			<td>{$data.DESKRIPSI_KEGIATAN}</td>
			<td>{$id_semester.THN_AKADEMIK_SEMESTER} ({$id_semester.NM_SEMESTER})</td>
			{if $smarty.post.fakultas == ''}
			<td><input type="text" class="tgl_mulai" name="tgl_mulai{$no}"  value="{$data.TGL_MULAI_JKS|date_format:"%d-%m-%Y"}" /></td>
			<td>
				<input type="text" class="tgl_selesai" name="tgl_selesai{$no}"  value="{$data.TGL_SELESAI_JKS|date_format:"%d-%m-%Y"}" />
				<input type="hidden" value="{$data.ID_JADWAL_KEGIATAN_SEMESTER}" name="id{$no}" />
				<input type="hidden" value="{$data.ID_KEGIATAN}" name="id_kegiatan{$no}" />
			</td>
			{else}
			<td>{$data.TGL_MULAI_JSF}</td>
			<td>{$data.TGL_SELESAI_JSF}</td>
			{/if}
		</tr>
		{/foreach}
		{if $smarty.post.fakultas == ''}
		<tr>
			<td colspan="10" style="text-align:center">
				<input type="submit" value="Simpan" />
				<input type="hidden" value="insert" name="mode" />
				<input type="hidden" value="{$no}" name="no" />
				<input type="hidden" value="{$smarty.post.semester}" name="semester" />
				<input type="hidden" value="{$smarty.post.fakultas}" name="fakultas" />
			</td>
		</tr>
		{/if}
	</table>
</form>

{literal}
    <script>
            $(".tgl_mulai").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
            $(".tgl_selesai").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
           
    </script>
{/literal}