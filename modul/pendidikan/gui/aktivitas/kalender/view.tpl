<div class="center_title_bar">Jadwal Kegiatan Akademik</div>

<form action="aktivitas-kalender.php" method="post" name="a">
<table>
	<tr>
		<td>
			Fakultas / Univeristas
		</td>
		<td>
			<select name="fakultas">
					<option value="">Universitas</option>
			{foreach $fakultas as $data}
					<option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS == $smarty.post.fakultas}selected="selected"{/if}>{$data.NM_FAKULTAS}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Semester</td>
		<td>
			<select name="semester">
			{foreach $semester as $data}
					<option value="{$data.ID_SEMESTER}" {if $data.ID_SEMESTER == $smarty.post.semester}selected="selected"{/if}>{$data.NM_SEMESTER} {$data.TAHUN_AJARAN}{if $data.STATUS_AKTIF_SEMESTER == 'True'} - Aktif{/if}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="text-align:center">
			<input type="submit" value="Tampil" />
		</td>
	</tr>
</table>
</form>

{if isset($jadwal)}
<form action="aktivitas-kalender.php" method="post" name="b">
	<table>
		<tr>
			<th>No</th>
			<th>Nama Kegiatan</th>
			<th>Deskripsi Keigatan</th>
			<th>Semester</th>
			<th>Tanggal Mulai</th>
			<th>Tanggal Selesai</th>
		</tr>
		{$no = 1}
		{foreach $jadwal as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.NM_KEGIATAN}</td>
			<td>{$data.DESKRIPSI_KEGIATAN}</td>
			<td>{$id_semester.NM_SEMESTER} {$id_semester.TAHUN_AJARAN}</td>
			{if $smarty.post.fakultas == ''}
			<td class="center">{$data.TGL_MULAI_JKS|date_format:"%d %B %Y"}</td>
			<td class="center">{$data.TGL_SELESAI_JKS|date_format:"%d %B %Y"}</td>
			{else}
			<td>{$data.TGL_MULAI_JSF|date_format:"%d %B %Y"}</td>
			<td>{$data.TGL_SELESAI_JSF|date_format:"%d %B %Y"}</td>
			{/if}
			
		</tr>
		{/foreach}
		
		<tr>
			<td colspan="10" style="text-align:center">
				<input type="submit" value="Edit" />
				<input type="hidden" value="tambah" name="mode" />
				<input type="hidden" value="{$smarty.post.semester}" name="semester" />
				<input type="hidden" value="{$smarty.post.fakultas}" name="fakultas" />
			</td>
		</tr>
		
	</table>
	</form>
{/if}