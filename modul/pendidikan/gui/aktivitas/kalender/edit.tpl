<div class="center_title_bar">Jadwal Kegiatan Akademik - Edit</div>

<form action="aktivitas-kalender.php" method="post" id="f2">
	<table>
		<tr>
			<td>Nama Kegiatan</td>
			<td>{$jadwal.NM_KEGIATAN}</td>
		</tr>
		<tr>
			<td>Deskripsi Kegiatan</td>
			<td>{$jadwal.DESKRIPSI_KEGIATAN}</td>
		</tr>
		<tr>
			<td>Semester</td>
			<td>{$jadwal.THN_AKADEMIK_SEMESTER} ({$jadwal.NM_SEMESTER})</td>
		</tr>
		<tr>
			<td>Tanggal Mulai</td>
			<td><input type="text" id="tgl_mulai" name="tgl_mulai"  value="{$jadwal.TGL_MULAI_JKS}" class="required" /></td>
		</tr>
		<tr>
			<td>Tanggal Selesai</td>
			<td><input type="text" id="tgl_selesai" name="tgl_selesai"  value="{$jadwal.TGL_SELESAI_JKS}" class="required" /></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<input type="hidden" value="update" name="mode" />
				<input type="hidden" value="{$jadwal.ID_JADWAL_KEGIATAN_SEMESTER}" name="id" />
				<input type="hidden" value="" name="fakultas" />
				<input type="hidden" value="{$jadwal.ID_SEMESTER}" name="semester" />
				<input type="submit" value="Simpan" />
			</td>
		</tr>
	</table>
</form>

{literal}
    <script>
            $("#tgl_mulai").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
            $("#tgl_selesai").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
            $('#f2').validate();
    </script>
{/literal}