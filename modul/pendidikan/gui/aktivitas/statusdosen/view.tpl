<div class="center_title_bar">Status Dosen</div>

<h3>Prodi : {$program_studi.NM_PROGRAM_STUDI}</h3>

<table>
    <tr>
        <th>No</th>
        <th>NIP</th>
        <th>NIDN</th>
        <th>Nama</th>
        <th>Golongan</th>
        <th>Jabatan</th>
		<th>Status</th>
    </tr>
    {foreach $dosen_set as $d}
    <tr>
        <td class="center">{$d@index+1}</td>
        <td>{$d.NIP_DOSEN}</td>
        <td>{$d.NIDN_DOSEN}</td>
        <td>{$d.NM_PENGGUNA}</td>
        <td>{$d.NM_GOLONGAN}</td>
		<td>{$d.NM_JABATAN_PEGAWAI}</td>
		<td>{if $d.NM_GOLONGAN == 'DLB'}DLB{/if}</td>
    </tr>
	{/foreach}
</table>

<button href="aktivitas-statusdosen.php?mode=prodi&id_fakultas={$program_studi.ID_FAKULTAS}">Kembali ke pilihan prodi</button>