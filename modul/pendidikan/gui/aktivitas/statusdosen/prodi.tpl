<div class="center_title_bar">Status Dosen</div>
<h2>Fakultas : {$nm_fakultas}</h2>
<table style="width: 400px">
    <tr>
        <th>No</th>
        <th>Nama Prodi</th>
		<th>Jumlah</th>
    </tr>
    {$total=0}
	{foreach $program_studi_set as $ps}
    <tr>
        <td class="center" style="width: 10%;">{$ps@index+1}</td>
        <td><a href="aktivitas-statusdosen.php?mode=view&id_program_studi={$ps.ID_PROGRAM_STUDI}">{$ps.NM_JENJANG} {$ps.NM_PROGRAM_STUDI}</a></td>
		<td class="center">{$ps.JUMLAH_DOSEN}</td>
    </tr>
	{$total=$total+$ps.JUMLAH_DOSEN}
    {/foreach}
	<tr>
		<td></td>
		<td>JUMLAH</td>
		<td class="center">{$total}</td>
	</tr>
</table>
<button href="aktivitas-statusdosen.php">Kembali ke pilihan fakultas</button>