<div class="center_title_bar">Status Dosen</div>

<table style="width: 400px;">
    <tr>
        <th>No</th>
        <th>Nama Fakultas</th>
		<th>Jumlah Dosen</th>
    </tr>
	{$total=0}
    {foreach $fakultas_set as $f}
    <tr>
        <td class="center" style="width: 10%;">{$f@index+1}</td>
        <td><a href="aktivitas-statusdosen.php?mode=prodi&id_fakultas={$f.ID_FAKULTAS}">{$f.NM_FAKULTAS}</a></td>
		<td class="center">{$f.JUMLAH_DOSEN}</td>
    </tr>
	{$total=$total+$f.JUMLAH_DOSEN}
    {/foreach}
	<tr>
		<td></td>
		<td>JUMLAH</td>
		<td class="center">{$total}</td>
	</tr>
</table>