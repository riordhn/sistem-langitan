<div class="center_title_bar">Master Kegiatan Semester - Hapus</div>
<h4>Apakah data master kegiatan ini akan dihapus ?</h4>
<form action="aktivitas-kegiatan.php" method="post">
    <input type="hidden" name="mode" value="delete" />
    <input type="hidden" name="id_kegiatan" value="{$kegiatan.ID_KEGIATAN}" />
    <table>
        <tr>
            <td>Nama Kegiatan</td>
            <td>{$kegiatan.NM_KEGIATAN}</td>
        </tr>
        <tr>
            <td>Kode Kegiatan</td>
            <td>{$kegiatan.KODE_KEGIATAN}</td>
        </tr>
        <tr>
            <td>Deskripsi Kegiatan</td>
            <td>{$kegiatan.DESKRIPSI_KEGIATAN}</td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button href="aktivitas-kegiatan.php">Batal</button>
                <input type="submit" value="Hapus" />
            </td>
        </tr>
    </table>
</form>