<div class="center_title_bar">Master Kegiatan Semester - Edit</div>

<form action="aktivitas-kegiatan.php" method="post">
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="id_kegiatan" value="{$kegiatan.ID_KEGIATAN}" />
    <table>
        <tr>
            <td>Nama Kegiatan</td>
            <td><input type="text" name="nm_kegiatan" size="65" class="required" value="{$kegiatan.NM_KEGIATAN}" /></td>
        </tr>
        <tr>
            <td>Kode Kegiatan</td>
            <td>
                <select name="kode_kegiatan">
                {foreach $kode_kegiatan_set as $kk}
                    <option value="{$kk.KODE_KEGIATAN}" {if $kk.KODE_KEGIATAN == $kegiatan.KODE_KEGIATAN}selected="selected"{/if}>{$kk.KODE_KEGIATAN}</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Deskripsi Kegiatan</td>
            <td>
				<textarea name="deskripsi_kegiatan" cols="50" rows="5" >{$kegiatan.DESKRIPSI_KEGIATAN}</textarea>
		  </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button href="aktivitas-kegiatan.php">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
<script>$('form').validate();</script>