<div class="center_title_bar">Master Kegiatan Semester - Tambah</div>

<form action="aktivitas-kegiatan.php" method="post">
    <input type="hidden" name="mode" value="add" />
    <table>
        <tr>
            <td>Nama Kegiatan</td>
            <td><input name="nm_kegiatan" type="text" class="required" size="60" /></td>
        </tr>
        <tr>
            <td>Kode Kegiatan</td>
            <td>
                <select name="kode_kegiatan">
                {foreach $kode_kegiatan_set as $kk}
                    <option value="{$kk.KODE_KEGIATAN}">{$kk.KODE_KEGIATAN} ({$kk.KETERANGAN})</option>
                {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Deskripsi Kegiatan</td>
            <td>
            <textarea name="deskripsi_kegiatan" id="deskripsi_kegiatan" cols="45" rows="5" ></textarea>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <button onClick="history.go(-1);">Batal</button>
                <input type="submit" value="Simpan" />
            </td>
        </tr>
    </table>
</form>
<script>$('form').validate();</script>