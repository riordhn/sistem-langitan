<div class="center_title_bar">Master Kegiatan Semester</div>

<table>
    <tr>
        <th>No</th>
        <th>Nama Kegiatan</th>
        <th>Kode Kegiatan</th>
        <th>Deskripsi</th>
        <th>Aksi</th>
    </tr>
	{$i = 1}
    {foreach $kegiatan_set as $kegiatan}
    <tr>
        <td class="center">{$i++}</td>
        <td>{$kegiatan.NM_KEGIATAN}</td>
        <td>{$kegiatan.KODE_KEGIATAN}</td>
        <td>{$kegiatan.DESKRIPSI_KEGIATAN}</td>
        <td>
            <a href="aktivitas-kegiatan.php?mode=edit&id_kegiatan={$kegiatan.ID_KEGIATAN}">Edit</a>
            <a href="aktivitas-kegiatan.php?mode=delete&id_kegiatan={$kegiatan.ID_KEGIATAN}">Hapus</a>
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="5" class="center"><a href="aktivitas-kegiatan.php?mode=add">Tambah</a></td>
    </tr>
</table>