<div class="center_title_bar">Setting Periode Bayar Mahasiswa</div>

<form action="aktivitas-periodebayar.php" method="post">
<table>
	<tr>
		<td>Semester</td>
		<td>
            <select name="semester" id="semester">
            	{foreach $semester_set as $data}
				<option value="{$data.ID_SEMESTER}">{$data.THN_AKADEMIK_SEMESTER} / {$data.NM_SEMESTER}</option>
                {/foreach}
            </select>
        </td>
	</tr>
	<tr>
		<td>Tanggal Pembukaan</td>
		<td>{html_select_date prefix="tgl_awal_" field_order="DMY" end_year="+2"}</td>
	</tr>
	<tr>
		<td>Tanggal Penutupan</td>
		<td>{html_select_date prefix="tgl_akhir_" field_order="DMY" end_year="+2"}</td>
	</tr>
	<tr>
		<td>Keterangan</td>
		<td>
			<textarea name="ket_periode_bayar" cols="50" rows="2"></textarea>
		</td>
	</tr>
	<tr>
		<td class="center" colspan="2">
			<button href="aktivitas-periodebayar.php">Batal</button>
			<input type="submit" value="Simpan" />
            <input type="hidden" value="add" name="mode" />
		</td>
	</tr>
</table>

</form>