<div class="center_title_bar">Setting Periode Bayar Mahasiswa</div>

<form action="aktivitas-periodebayar.php" method="post">
<input type="hidden" name="id_periode_bayar" value="{$pb.ID_PERIODE_BAYAR}">
<table>
	<tr>
		<td>Semester</td>
		<td>{$pb.NM_SEMESTER} {$pb.TAHUN_AJARAN}</td>
	</tr>
	<tr>
		<td>Tanggal Pembukaan</td>
		<td>{html_select_date prefix="tgl_awal_" field_order="DMY" time=$pb.TGL_AWAL_PERIODE_BAYAR end_year="+2"}</td>
	</tr>
	<tr>
		<td>Tanggal Penutupan</td>
		<td>{html_select_date prefix="tgl_akhir_" field_order="DMY" time=$pb.TGL_AKHIR_PERIODE_BAYAR end_year="+2"}</td>
	</tr>
	<tr>
		<td>Keterangan</td>
		<td>
			<textarea name="ket_periode_bayar" cols="50" rows="2">{$pb.KET_PERIODE_BAYAR}</textarea>
		</td>
	</tr>
	<tr>
		<td class="center" colspan="2">
			<button href="aktivitas-periodebayar.php">Batal</button>
			<input type="hidden" name="mode" value="hapus" />
			<input type="submit" value="Hapus" />
		</td>
	</tr>
</table>

</form>