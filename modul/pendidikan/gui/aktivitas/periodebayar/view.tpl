<div class="center_title_bar">Setting Periode Bayar Mahasiswa</div>

{if $periode_changed}
<h2>Periode bayar berhasil diganti</h2>
{/if}

<table>
	<tr>
		<th style="width: 110px">Semester</th>
		<th>Tanggal Pembukaan</th>
		<th>Tanggal Penutupan</th>
		<th>Keterangan</th>
		<th>Aksi</th>
	</tr>
	{foreach $periode_bayar_set as $pb}
	<tr>
		<td>{$pb.NM_SEMESTER} {$pb.TAHUN_AJARAN}</td>
		<td>{$pb.TGL_AWAL_PERIODE_BAYAR|date_format:"%d %B %Y"}</td>
		<td>{$pb.TGL_AKHIR_PERIODE_BAYAR|date_format:"%d %B %Y"}</td>
		<td style="font-size: .9em;">{$pb.KET_PERIODE_BAYAR}</td>
		<td><a href="aktivitas-periodebayar.php?mode=edit&id_periode_bayar={$pb.ID_PERIODE_BAYAR}">Edit</a>
		<a href="aktivitas-periodebayar.php?mode=delete&id_periode_bayar={$pb.ID_PERIODE_BAYAR}">Hapus</a></td>
	</tr>
	{/foreach}
    <tr>
    	<td colspan="6" style="text-align:center">
            <form method="get" action="aktivitas-periodebayar.php">
            	<input type="hidden" name="mode" value="add" />
                <input type="submit" name="add" value="Tambah"/>
            </form>
        </td>
    </tr>
</table>