<div class="center_title_bar">Setting Periode Bayar Calon Mahasiswa</div>

<form action="aktivitas-periodebayar-cmhs.php" method="post">
<input type="hidden" name="id_periode_bayar" value="{$pb.ID_PERIODE_BAYAR_CMHS}">
<table>
	<tr>
		<td>Nama Penerimaan</td>
		<td>Gelombang {$pb.GELOMBANG} {$pb.NM_PENERIMAAN} Jalur ({$pb.NM_JALUR})</td>
	</tr>
	<tr>
		<td>Tanggal Pembukaan</td>
		<td>{$pb.TGL_AWAL_PERIODE_BAYAR|date_format:"%d %B %Y"}</td>
	</tr>
	<tr>
		<td>Tanggal Penutupan</td>
		<td>{$pb.TGL_AKHIR_PERIODE_BAYAR|date_format:"%d %B %Y"}</td>
	</tr>
	<tr>
		<td class="center" colspan="2">			
        	<input type="hidden" name="mode" value="hapus" />
			<input type="submit" value="Delete" />
		</td>
	</tr>
</table>

</form>