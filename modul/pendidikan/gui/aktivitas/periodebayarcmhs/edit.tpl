<div class="center_title_bar">Setting Periode Bayar Calon Mahasiswa</div>

<form action="aktivitas-periodebayar-cmhs.php" method="post" id="f1">
<input type="hidden" name="id_periode_bayar" value="{$pb.ID_PERIODE_BAYAR_CMHS}">
<table>
	<tr>
		<td>Nama Penerimaan</td>
		<td colspan="3">
            	<select name="id_penerimaan" class="required">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $pb.ID_PENERIMAAN}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
            </td>
	</tr>
	<tr>
		<td>Tanggal Pembukaan</td>
		<td><input type="text" name="tgl_awal" id="tgl_awal" class="required"  value="{$pb.TGL_AWAL_PERIODE_BAYAR}" /></td>
	</tr>
	<tr>
		<td>Tanggal Penutupan</td>
		<td><input type="text" name="tgl_akhir" id="tgl_akhir" class="required"  value="{$pb.TGL_AKHIR_PERIODE_BAYAR}" /></td>
	</tr>
	<tr>
		<td class="center" colspan="2">
			<button href="aktivitas-periodebayar-cmhs.php">Batal</button>
			<input type="hidden" name="mode" value="update" />
			<input type="submit" value="Simpan" />
		</td>
	</tr>
</table>

</form>

{literal}
    <script type="text/javascript">		
		$('#f1').validate();
		$("#tgl_awal").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
		$("#tgl_akhir").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});		
    </script>
{/literal}