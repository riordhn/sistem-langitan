<div class="center_title_bar">Setting Periode Bayar Calon Mahasiswa</div>

{if $periode_changed}
<h2>Periode bayar berhasil diganti</h2>
{/if}

<table>
	<tr>
		<th>Penerimaan</th>
		<th>Tanggal Pembukaan</th>
		<th>Tanggal Penutupan</th>
		<th>Aksi</th>
	</tr>
	{foreach $periode_bayar_set as $pb}
	<tr>
		<td>Gelombang {$pb.GELOMBANG} {$pb.NM_PENERIMAAN} Jalur ({$pb.NM_JALUR}) {$pb.TAHUN} ({$pb.SEMESTER})</td>
		<td>{$pb.TGL_AWAL_PERIODE_BAYAR|date_format:"%d %B %Y"}</td>
		<td>{$pb.TGL_AKHIR_PERIODE_BAYAR|date_format:"%d %B %Y"}</td>
		<td><a href="aktivitas-periodebayar-cmhs.php?mode=edit&id_periode_bayar={$pb.ID_PERIODE_BAYAR_CMHS}">Edit</a>
			<a href="aktivitas-periodebayar-cmhs.php?mode=delete&id_periode_bayar={$pb.ID_PERIODE_BAYAR_CMHS}">Hapus</a>
		</td>
	</tr>
	{/foreach}
    <tr>
    	<td colspan="6" style="text-align:center">
            <form method="get" action="aktivitas-periodebayar-cmhs.php">
            	<input type="hidden" name="mode" value="add" />
                <input type="submit" name="add" value="Tambah"/>
            </form>
        </td>
    </tr>
</table>