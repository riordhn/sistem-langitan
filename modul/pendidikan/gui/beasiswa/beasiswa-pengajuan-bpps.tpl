<div class="center_title_bar">Pengajuan BPPS</div>
<form action="beasiswa-pengajuan-bpps.php" method="post">
	<table>
		<tr>
			<th colspan="5">PARAMETER</th>
		</tr>
		<tr>
            <td>Fakultas</td>
            <td>
                <select id="fakultas" name="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $smarty.post.fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
			<td>Semester</td>
            <td>
            	<select name="semester">
                	<option value="">Semua</option>
                    <option value="Gasal" {if $smarty.post.semester == 'Gasal'} selected="selected"{/if}>Ganjil</option>
                    <option value="Genap" {if $smarty.post.semester == 'Genap'} selected="selected"{/if}>Genap</option>	
                </select>
            </td>
        </tr>
		<tr>
            <td>Jalur</td>
            <td>
                <select id="jalur" name="jalur">
                    <option value="">Semua</option>
                    {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $smarty.post.jalur==$data.ID_JALUR}selected="true"{/if}>{$data.NM_JALUR|upper}</option>
                    {/foreach}
                </select>
            </td>
			<td>Tahun Penerimaan</td>
            <td>
               <select name="thn" id="thn">
               	<option value="">Semua</option>
                    {foreach $data_thn as $data}
                        <option value="{$data.TAHUN}" {if $smarty.post.thn==$data.TAHUN}selected="true"{/if}>{$data.TAHUN|upper}</option>
                    {/foreach}
               </select>
            </td>
		</tr>
		<tr>
			
            <td>Jenjang</td>
            <td>
                <select id="jenjang" name="jenjang">
                    <option value="">Semua</option>
                    {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.post.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG|upper}</option>
                    {/foreach}
                </select>
            </td>
		 <td>Gelombang</td>
            <td>
                <select id="gelombang" name="gelombang">
                    <option value="">Semua</option>
                    {section name=foo loop=3} 
                    	<option value="{$smarty.section.foo.iteration}" {if $smarty.post.gelombang==$smarty.section.foo.iteration}selected="true"{/if}>{$smarty.section.foo.iteration}</option>
					{/section}
                </select>
            </td>
        </tr>
		<tr>
			<td>
				Nama Penerimaan
			</td>
			<td  colspan="4">				
            	<select name="id_penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.post.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="text-align:center"><input type="submit" value="Tampil" /><input type="hidden" name="tampilan" /></td>
		</tr>
	</table>
</form>

{if isset($pengajuan_bpps)}
<form action="beasiswa-pengajuan-bpps.php" method="post">
<table>
	<tr>
		<th>No</th>
		<th>No Ujian</th>
		<th>Nama</th>
		<th>Prodi Pilihan</th>
		<th>Fakultas</th>
		<th>Telp</th>
		<th>Alamat</th>
		<th>Pengajuan <input type="checkbox" id="cek_all" name="cek_all" /></th>
	</tr>
	{$no = 1}
{foreach $pengajuan_bpps as $data}
	<tr>
		<td>{$no++}</td>
		<td>{$data.NO_UJIAN}</td>
		<td>{$data.NM_C_MHS}</td>
		<td>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI}</td>
		<td>{$data.NM_FAKULTAS}</td>
		<td>{$data.TELP}</td>
		<td>{$data.ALAMAT}</td>
		<td><input type="checkbox" name="pengajuan{$no}" class="pengajuan" value="8" {IF $data.ID_BEASISWA == 8} checked="checked"{/if} /></td>
		<input type="hidden" name="id_c_mhs{$no}" value="{$data.ID_C_MHS}" />
	</tr>
{/foreach}
	<tr>
		<td colspan="8" style="text-align:center">
		<input type="hidden" name="no" value="{$no}" />
		<input type="hidden" name="tampilan" />
		<input type="submit" value="Simpan" />
		</td>
	</tr>
</table>
</form>

{literal}
    <script>
		
		
		$("#cek_all").click(function()				
			{
				var checked_status = this.checked;
				$(".pengajuan").each(function()
				{
					this.checked = checked_status;
				});
			});
		
    </script>
{/literal}

{/if}