<div class="center_title_bar">Tambah - Master Kerjasama / Beasiswa</div>
<form action="beasiswa-group.php" method="post" id="add_group">
    <table class="ui-widget-content" style="width: 50%">
        <tr>
            <th colspan="2" class="center">Tambah Group Kerjasama / Beasiswa</th>
        </tr>
        <tr>
            <td>Nama Grup</td>
            <td><input class="required" id="nama" type="text" name="nama"/></td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <input type="hidden" name="mode" value="add_group"/>
                <a class="disable-ajax button" onclick="history.back(-1)">Kembali</a>
                <input type="submit" class="button" value="Tambah"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#add_group').validate();  
    </script>
{/literal}