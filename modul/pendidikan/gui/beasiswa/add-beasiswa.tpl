<div class="center_title_bar">Tambah - Master Kerjasama / Beasiswa</div>
<form action="beasiswa-master.php" method="post" id="add_beasiswa">
    <table class="ui-widget-content" style="width: 90%">
        <tr>
            <th colspan="6" class="center">Tambah Kerjasama / Beasiswa</th>
        </tr>
        <tr>
            <td>Nama Kerjasama / Beasiswa</td>
            <td>
                <input class="required" type="text" name="nama"/>
            </td>
            <td>Penyelenggara</td>
            <td>
                <input class="required" type="text" name="penyelenggara"/>
            </td>
            <td>Jenis</td>
            <td>
                <select name="jenis">
                    {foreach $data_jenis as $j}
                        <option value="{$j.ID_JENIS_BEASISWA}">{$j.NM_JENIS_BEASISWA}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Group</td>
            <td>
                <select name="group">
                    {foreach $data_group_beasiswa as $data}
                        <option value="{$data.ID_GROUP_BEASISWA}">{$data.NM_GROUP}</option>
                    {/foreach}
                </select>
            </td>
            <td>Keterangan</td>
            <td>
                <textarea name="keterangan"></textarea>
            </td>
            <td>Pusat Beasiswa</td>
            <td>
                <select name="pusat">
                    <option value="1">Universitas</option>
                    <option value="0">Fakultas</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="6">
                <input type="hidden" name="mode" value="add_beasiswa"/>
                <input type="submit" class="button" value="Kembali" onclick="history.back(-1)"/>
                <input type="submit" class="button" value="Tambah"/>
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#add_beasiswa').validate();            
    </script>
{/literal}