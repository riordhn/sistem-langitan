<link rel="stylesheet" type="text/css" href="../../css/cupertino/jquery-ui-cupertino.css" />
<link rel="stylesheet" type="text/css" href="../../css/reset.css" />
<link rel="stylesheet" type="text/css" href="../../css/text.css" />
<link rel="stylesheet" type="text/css" href="../../css/kemahasiswaan-style.css" />
<script language="javascript" src="../../js/jquery-1.5.1.min.js"></script>
<script language="javascript" src="../../js/jquery.validate.js"></script>
<script language="javascript" src="../../js/jquery-ui-1.8.11.custom.min.js"></script>
{literal}
    <style>
        label.error {
            color: red;
            font-size:11px;
        }
    </style>
{/literal}
<div class="content">
    {if $sukses_upload!=''}
        <div style="margin: 0px auto;width: 50%">
            {$sukses_upload}
        </div>
    {/if}
    <form id="form_upload" action="beasiswa-upload.php" method="post" enctype="multipart/form-data">
        <table class="ui-widget-content" style="margin: 20px auto;width: 50%">
            <tr class="center">
                <th colspan="2" class="center">Upload Mahasiswa Beasiswa</th>
            </tr>        
            <tr>
                <td>Jenis Beasiswa</td>
                <td>
                    <select name="beasiswa">
                        {foreach $data_beasiswa as $data}
                            <option value="{$data['ID_BEASISWA']}" {if $data['ID_BEASISWA']==$smarty.post.beasiswa}selected="true"{/if}>{$data['NM_BEASISWA']} {$data['PENYELENGGARA_BEASISWA']} {$data['NM_GROUP_BEASISWA']}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>

            <tr>
                <td>Tanggal Mulai</td>
                <td>
                    <input type="text" style="text-transform: uppercase" class="datepick" name="tgl_mulai" />
                </td>
            </tr>
            <tr>
                <td>Tanggal Selesai</td>
                <td>
                    <input type="text" style="text-transform: uppercase" class="datepick" name="tgl_selesai" />
                </td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td><textarea name="keterangan" class="required"   rows="5" cols="40">{if isset($smarty.post.keterangan)}{$smarty.post.keterangan} {/if}</textarea> </td>
            </tr>
            <tr>
                <td>Upload File</td>
                <td><input type="file" name="file" id="file" class="required" /> </td>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <a class="button"  href="excel-beasiswa-template.php">Download Template</a>
                </td>
            </tr>
            <!--
            <tr>
                <th colspan="2" class="center">DETAIL BIAYA BEASISWA</th>
            </tr>            
            {foreach $data_biaya as $b}
                <tr>
                    <td>
                        <input type="checkbox" name="id_biaya{$b@index+1}" value="{$b.ID_BIAYA}"/>
                {$b.NM_BIAYA}
            </td>
            <td>
                <input type="text" name="besar_biaya{$b@index+1}" size="10" />
            </td>
        </tr>
            {/foreach}
            -->
            <tr>
                <td colspan="2" class="center">
                    <input type="submit" name="excel" class="button"  value="Submit" />
                    <input type="reset" class="button"  value="Reset" />
                    <a class="button"  href="beasiswa-upload.php">Reload</a>
                </td>
            </tr>
        </table>
    </form>
    <center>
        {$status_upload}
    </center>
    {if isset($data_mhs_upload)}
        <table class="ui-widget-content" style="margin: 20px auto;width: 50%">
            <tr>
                <th colspan="5" class="center">Status Upload Data</th>
            </tr>
            <tr>
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Status Mahasiswa</th>
                <th>Status Beasiswa Mahasiswa</th>
            </tr>
            {foreach $data_mhs_upload as $data}
                <tr  {if $data.STATUS_MAHASISWA=='Data Mahasiswa Tidak Ada'}style="background-color: #eab8b8"{/if}>
                    <td>{$data@index+1}</td>
                    <td>{$data.NIM_MHS}</td>
                    <td>{$data.NAMA}</td>
                    <td>{$data.STATUS_MAHASISWA}</td>
                    <td>
                        {if $data.STATUS_BEASISWA!='Data Kosong'}
                            Sudah Terdaftar {$data.STATUS_BEASISWA['NM_BEASISWA']} {$data.STATUS_BEASISWA['PENYELENGGARA_BEASISWA']} ({$data.STATUS_BEASISWA['TGL_MULASI']}-{$data.STATUS_BEASISWA['TGL_MULASI']})
                        {else}
                            {$data.STATUS_BEASISWA}
                        {/if}
                    </td>
                </tr>
            {/foreach}
            <tr class="ui-widget-content total">
                <td colspan="5" class="center">
                    <a class="button"  onclick="confirm_save_data('beasiswa-upload.php?mode=save_excel')">Save Data</a>
                </td>
            </tr>
        </table>
    {/if}
</div>
{literal}
    <script>
            function confirm_save_data(url){
                var c=confirm("Silahkan Periksa Kembali Parameternya. Apakah anda yakin menyimpan data ini? ");
                if(c==true){
                    window.location.href=url;
                }
            }
            $(function() {
                $( ".datepick" ).datepicker({dateFormat:'dd-M-y',changeMonth: true,
                        changeYear: true});
            });
            $('form').validate();
    </script>
{/literal}