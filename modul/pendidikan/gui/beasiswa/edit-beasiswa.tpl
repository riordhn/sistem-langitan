<div class="center_title_bar">Edit - Master Kerjasama / Beasiswa</div>
<form action="beasiswa-master.php" method="post" id="edit_beasiswa">
    <table class="ui-widget-content" style="width: 90%">
        <tr>
            <th colspan="6" class="center">Edit Kerjasama / Beasiswa</th>
        </tr>
        <tr>
            <td>Nama Kerjasama / Beasiswa</td>
            <td>
                <input class="required" type="text" name="nama" value="{$beasiswa.NM_BEASISWA}"/>
            </td>
            <td>Penyelenggara</td>
            <td>
                <input type="text" class="required" name="penyelenggara" value="{$beasiswa.PENYELENGGARA_BEASISWA}"/>
            </td>
            <td>Jenis</td>
            <td>
                <select name="jenis">
                    {foreach $data_jenis as $j}
                        <option value="{$j.ID_JENIS_BEASISWA}" {if $j.ID_JENIS_BEASISWA==$beasiswa.ID_JENIS_BEASISWA}selected="true"{/if}>{$j.NM_JENIS_BEASISWA}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Group</td>
            <td>
                <select name="group">
                    {foreach $data_group_beasiswa as $data}
                        <option value="{$data.ID_GROUP_BEASISWA}" {if $data.ID_GROUP_BEASISWA==$beasiswa.ID_GROUP_BEASISWA}selected="true"{/if}>{$data.NM_GROUP}</option>
                    {/foreach}
                </select>
            </td>
            <td>Keterangan</td>
            <td>
                <textarea name="keterangan">{$beasiswa.KETERANGAN}</textarea>
            </td>
            <td>Pusat Beasiswa</td>
            <td>
                <select name="pusat">
                    <option value="1" {if $beasiswa.PUSAT_BEASISWA==1}selected="true"{/if}>Universitas</option>
                    <option value="0" {if $beasiswa.PUSAT_BEASISWA==0}selected="true"{/if}>Fakultas</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="6">
                <input type="hidden" name="id" value="{$beasiswa.ID_BEASISWA}"/>
                <a class="disable-ajax button" onclick="history.back(-1)">Kembali</a>
                <input type="hidden" name="mode" value="edit_beasiswa"/>
                <input type="submit" class="button" value="Update" />
            </td>
        </tr>
    </table>
</form>
{literal}
    <script type="text/javascript">
        $('#edit_beasiswa').validate();
    </script>
{/literal}