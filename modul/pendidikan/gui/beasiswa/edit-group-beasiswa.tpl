<div class="center_title_bar">Edit - Master Kerjasama / Beasiswa</div>
<form action="beasiswa-group.php" method="post" id="edit_group">
    <table class="ui-widget-content" style="width: 50%">
        <tr>
            <th colspan="2" class="center">Edit Group Kerjasama / Beasiswa</th>
        </tr>
        <tr>
            <td>Nama Grup</td>
            <td><input class="required" type="text" name="nama" value="{$data_group_beasiswa.NM_GROUP}"/></td>
        </tr>
        <tr>
            <td class="center" colspan="2">
                <input type="hidden" name="mode" value="edit_group"/>
                <input type="hidden" name="id" value="{$data_group_beasiswa.ID_GROUP_BEASISWA}"/>
                <a class="disable-ajax button" onclick="history.back(-1)">Kembali</a>
                <input type="submit" class="button" onclick="" value="Update"/>
            </td>
        </tr>
    </table>
</form>