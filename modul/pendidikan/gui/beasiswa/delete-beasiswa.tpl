<div class="center_title_bar">Delete - Master Kerjasama / Beasiswa</div>
<form action="beasiswa-master.php" method="post" id="edit_beasiswa">
    <table class="ui-widget-content" style="width: 90%">
        <tr>
            <th colspan="6" class="center">Delete Kerjasama / Beasiswa</th>
        </tr>
        <tr>
            <td>Nama Kerjasama / Beasiswa</td>
            <td>
                {$beasiswa.NM_BEASISWA}
            </td>
            <td>Penyelenggara</td>
            <td>
                {$beasiswa.PENYELENGGARA_BEASISWA}
            </td>
            <td>Jenis</td>
            <td>
                {foreach $data_jenis as $j}
                    {if $j.ID_JENIS_BEASISWA==$beasiswa.ID_JENIS_BEASISWA}
                        {$j.NM_JENIS_BEASISWA}
                    {/if}
                {/foreach}
            </td>
        </tr>
        <tr>
            <td>Group</td>
            <td>
                {foreach $data_group_beasiswa as $data}
                    {if $data.ID_GROUP_BEASISWA==$beasiswa.ID_GROUP_BEASISWA}
                        {$data.NM_GROUP}
                    {/if}
                {/foreach}
            </td>
            <td>Keterangan</td>
            <td>
                {$beasiswa.KETERANGAN}
            </td>
        </tr>
        <tr>
            <td class="center" colspan="6">
                <input type="hidden" name="id" value="{$beasiswa.ID_BEASISWA}"/>
                Apakah Yakin ingin Menghapus Item ini?<br/>
                <a class="disable-ajax button" onclick="history.back(-1)">Kembali</a>
                <input type="hidden" name="mode" value="delete_beasiswa"/>
                <input type="submit" class="button" value="Hapus" />
            </td>
        </tr>
    </table>
</form>
