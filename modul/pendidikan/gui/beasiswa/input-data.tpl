<div class='center_title_bar'>Input Beasiswa Mahasiswa</div>
<form method="get" action="beasiswa-input.php">
    <table class="ui-widget-content">
        <tr>
            <th class="center" colspan="3">Input Data History Beasiswa </th>
        </tr>
        <tr>
            <td>
                NIM :
            </td>
            <td>
                <input type="text" name="cari" value="{$smarty.get.cari}"/>
            </td>
            <td>
                <input type="submit" class="button" value="Cari"/>
                <a target='_blank' class='button disable-ajax' href='beasiswa-upload.php'>Upload Excel</a>
            </td>
        </tr>
    </table>
</form>
{if $data_mhs!=''}
    <table class="ui-widget-content">
        <tr>
            <th colspan="2" class="center">Biodata Mahasiswa</th>
        </tr>
        <tr>
            <td>NAMA</td>
            <td>{$data_mhs.NM_PENGGUNA}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>{$data_mhs.NIM_MHS}</td>
        </tr>
        <tr>
            <td>ANGKATAN</td>
            <td>{$data_mhs.THN_ANGKATAN_MHS}</td>
        </tr>
        <tr>
            <td>PRODI</td>
            <td>{$data_mhs.NM_JENJANG} {$data_mhs.NM_PROGRAM_STUDI}</td>
        </tr>
        <tr>
            <td>JALUR</td>
            <td>{$data_mhs.NM_JALUR}</td>
        </tr>
    </table>
    <p></p>
    {if $smarty.get.mode==''}
        <table class="ui-widget-content" style="width: 80%">
            <tr>
                <th class="center" colspan="6">DATA HISTORY BEASISWA</th>
            </tr>
            <tr>
                <th>NO</th>
                <th>BEASISWA</th>
                <th>PERIODE</th>
                <th>KETERANGAN</th>
                <th>STATUS</th>
                <th>OPERASI</th>
            </tr>
            {foreach $data_history as $h}
                <tr>
                    <td>{$h@index+1}</td>
                    <td>{$h.NM_BEASISWA} {$h.PENYELENGGARA_BEASISWA} {$h.NM_JENIS_BEASISWA}</td>
                    <td class="center">{$h.TGL_MULAI} - {$h.TGL_SELESAI}</td>
                    <td>{$h.KETERANGAN}</td>
                    <td>
                        {if $h.BEASISWA_AKTIF==1}
                            AKtif
                        {else}
                            Tidak Aktif
                        {/if}
                    </td>
                    <td>
                        <a class="button" href="beasiswa-input.php?{$smarty.server.QUERY_STRING}&mode=edit&id={$h.ID_SEJARAH_BEASISWA}">Edit</a>
                        <a class="button" href="beasiswa-input.php?{$smarty.server.QUERY_STRING}&mode=hapus&id={$h.ID_SEJARAH_BEASISWA}">Hapus</a>
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="6" class="center data-kosong">Data Masih Kosong</td>
                </tr>
            {/foreach}
            <tr>
                <td class="center" colspan="6">
                    <a class="button" href="beasiswa-input.php?{$smarty.server.QUERY_STRING}&mode=tambah">Tambah</a>
                </td>
            </tr>
        </table>
    {else if $smarty.get.mode=='tambah'}
        <form method="post" action="beasiswa-input.php?cari={$smarty.get.cari}">
            <table class="ui-widget-content" style="width: 98%">
                <tr>
                    <th class="center" colspan="4">TAMBAH HISTORY BEASISWA</th>
                </tr>
                <tr>
                    <th>BEASISWA</th>
                    <th>PERIODE</th>
                    <th>KETERANGAN</th>
                    <th>STATUS</th>
                </tr>
                <tr>
                    <td>
                        <select name="beasiswa">
                            {foreach $data_beasiswa as $data}
                                <option value="{$data['ID_BEASISWA']}" {if $data['ID_BEASISWA']==$smarty.get.beasiswa}selected="true"{/if}>{$data['NM_BEASISWA']} oleh {$data['PENYELENGGARA_BEASISWA']} {$data['NM_GROUP_BEASISWA']}</option>
                            {/foreach}
                        </select>
                    </td>
                    <td>
                        Mulai : <input type="text" size="9" class="datepick required" name="tgl_mulai"/> Selesai : <input type="text" size="9" class="datepick required" name="tgl_selesai"/>
                    </td>
                    <td>
                        <textarea name="keterangan"></textarea>
                    </td>
                    <td>
                        <select name="status">
                            <option value="1">Aktif</option>
                            <option value="0">Tidak Aktif</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="center" colspan="5">
                        <input type="submit" class="button" value="Simpan"/>
                        <input type="hidden" name="mode" value="tambah"/>
                        <a class="button" href="beasiswa-input.php?cari={$smarty.get.cari}">Kembali</a>
                    </td>
                </tr>
            </table>
        </form>
    {else if $smarty.get.mode=='edit'}
        <form method="post" action="beasiswa-input.php?cari={$smarty.get.cari}">
            <table class="ui-widget-content"  style="width: 98%">
                <tr>
                    <th class="center" colspan="4">EDIT HISTORY BEASISWA</th>
                </tr>
                <tr>
                    <th>BEASISWA</th>
                    <th>PERIODE</th>
                    <th>KETERANGAN</th>
                    <th>STATUS</th>
                </tr>
                <tr>
                    <td>
                        <select name="beasiswa">
                            {foreach $data_beasiswa as $data}
                                <option value="{$data['ID_BEASISWA']}" {if $data['ID_BEASISWA']==$history.ID_BEASISWA}selected="true"{/if}>{$data['NM_BEASISWA']} oleh {$data['PENYELENGGARA_BEASISWA']} {$data['NM_GROUP_BEASISWA']}</option>
                            {/foreach}
                        </select>
                    </td>
                    <td>
                        Mulai : <input type="text" size="9" class="datepick" value="{$history.TGL_MULAI}" name="tgl_mulai"/>
                        Selesai : <input type="text" size="9" class="datepick"  value="{$history.TGL_SELESAI}" name="tgl_selesai"/>
                    </td>
                    <td>
                        <textarea name="keterangan">{$history.KETERANGAN}</textarea>
                    </td>
                    <td>
                        <select name="status">
                            <option value="1" {if $history.BEASISWA_AKTIF==1}selected="true"{/if}>Aktif</option>
                            <option value="0" {if $history.BEASISWA_AKTIF==0}selected="true"{/if}>Tidak Aktif</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="center" colspan="5">
                        <input type="submit" class="button" value="Update"/>
                        <input type="hidden" name="mode" value="edit"/>
                        <input type="hidden" name="id" value="{$smarty.get.id}"/>
                        <a class="button" href="beasiswa-input.php?cari={$smarty.get.cari}">Kembali</a>
                    </td>
                </tr>
            </table>
        </form>
    {else if $smarty.get.mode=='hapus'}
        <form method="post" action="beasiswa-input.php?cari={$smarty.get.cari}">
            <table class="ui-widget-content" style="width: 80%">
                <tr>
                    <th class="center" colspan="4">HAPUS HISTORY BEASISWA</th>
                </tr>
                <tr>
                    <th>BEASISWA</th>
                    <th>PERIODE</th>
                    <th>KETERANGAN</th>
                    <th>STATUS</th>
                </tr>
                <tr>
                    <td>
                        {foreach $data_beasiswa as $data}
                            {if $data['ID_BEASISWA']==$history.ID_BEASISWA}
                                {$data['NM_BEASISWA']} oleh {$data['PENYELENGGARA_BEASISWA']} {$data['NM_GROUP_BEASISWA']}
                            {/if}
                        {/foreach}
                    </td>
                    <td>
                        Mulai : {$history.TGL_MULAI} Selesai : {$history.TGL_MULAI}
                    </td>
                    <td>
                        {$history.KETERANGAN}
                    </td>
                    <td>
                        {if $history.BEASISWA_AKTIF==1}
                            Aktif
                        {else}
                            Tidak Aktif
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td class="center" colspan="4">
                        Apakah Anda yakin menghapus item ini ?<br/>
                        <input type="submit" class="button" value="Ya"/>
                        <input type="hidden" name="mode" value="hapus"/>
                        <input type="hidden" name="id" value="{$smarty.get.id}"/>
                        <a class="button" href="beasiswa-input.php?cari={$smarty.get.cari}">Kembali</a>
                    </td>
                </tr>
            </table>
        </form>
    {/if}
{else if isset($data_kosong)}
    <span>{$data_kosong}</span>
{/if}
{literal}
    <script>            
            $(function() {
                $( ".datepick" ).datepicker({dateFormat:'dd-M-y',changeMonth: true,
                        changeYear: true}).css({'text-transform':'uppercase'});
            });
            $('form').validate();
    </script>
{/literal}