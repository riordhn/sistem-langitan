<div class="center_title_bar">Pengumuman Beasiswa</div>
{if $smarty.get.mode==''}
    <table class="ui-widget-content" style="width: 98%">
        <tr>
            <th class="center" colspan="9">Data Pengumuman Beasiswa</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>Nama Beasiswa</th>
            <th>Dari</th>
            <th>Jenis</th>
            <th>Besar Beasiswa</th>
            <th>Syarat</th>
            <th>Keterangan</th>
            <th>Batas AKhir</th>
            <th>Operasi</th>
        </tr>
        {foreach $data_pengumuman as $data}
            <tr>
                <td>{$data@index+1}</td>
                <td>
                    {$data.NM_BEASISWA}
                    <br/>
                    {if $data.PUSAT_BEASISWA==0}
                        (Fakultas)
                    {else}
                        (Universitas)
                    {/if}
                </td>
                <td>{$data.PENYELENGGARA_BEASISWA}</td>
                <td>{$data.NM_JENIS_BEASISWA}</td>
                <td>{number_format($data.BESAR_BEASISWA)}</td>
                <td>{$data.SYARAT}</td>
                <td>{$data.KETERANGAN}</td>
                <td>{$data.BATAS_AKHIR}</td>
                <td class="center" style="width: 130px">
                    {if $data.PUSAT_BEASISWA==1}
                        <a class="button" href="beasiswa-pengumuman.php?mode=edit&id={$data.ID_PENGUMUMAN}">Edit</a>
                        <a class="button" href="beasiswa-pengumuman.php?mode=hapus&id={$data.ID_PENGUMUMAN}">Delete</a>
                    {else}
                        Tidak Ada Akses
                    {/if}
                </td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan="9" class="data-kosong">Data Masih Kosong</td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="9" class="center">
                <a class="button" href="beasiswa-pengumuman.php?mode=tambah">Tambah</a>
            </td>
        </tr>
    </table>
{else if $smarty.get.mode=='tambah'}
    <form action="beasiswa-pengumuman.php" method="post">
        <table class="ui-widget-content" style="width: 90%">
            <tr>
                <th colspan="2" class="center">Tambah Pengumuman Beasiswa</th>
            </tr>
            <tr>
                <td>Nama Beasiswa</td>
                <td>
                    <select name="beasiswa">
                        {foreach $data_beasiswa as $data}
                            <option value="{$data['ID_BEASISWA']}" {if $data['ID_BEASISWA']==$pengumuman.ID_BEASISWA}selected="true"{/if}>{$data['NM_BEASISWA']} oleh {$data['PENYELENGGARA_BEASISWA']} {$data['NM_GROUP_BEASISWA']}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Besar Beasiswa</td>
                <td>
                    <input type="text" name="besar" class="required number"/>
                </td>
            </tr>
            <tr>
                <td>Syarat</td>
                <td>
                    <textarea name="syarat" class="required" style="width: 380px;height: 150px"></textarea>
                </td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td>
                    <textarea name="keterangan" class="required" style="width: 380px;height: 150px"></textarea>
                </td>
            </tr>
            <tr>
                <td>Batas Akhir</td>
                <td>
                    <input type="text" name="batas" class="datepick required"/>
                </td>
            </tr>
            <tr>
                <td class="center" colspan="2">
                    <a href="beasiswa-pengumuman.php" class="button">Kembali</a>
                    <input type="submit" value="Simpan" class="button" />
                    <input type="hidden" name="mode" value="tambah"/>
                </td>
            </tr>
        </table>
    </form>
{else if $smarty.get.mode=='edit'}
    <form action="beasiswa-pengumuman.php" method="post">
        <table class="ui-widget-content" style="width: 90%">
            <tr>
                <th colspan="2" class="center">Edit Pengumuman Beasiswa</th>
            </tr>
            <tr>
                <td>Nama Beasiswa</td>
                <td>
                    <select name="beasiswa">
                        {foreach $data_beasiswa as $data}
                            <option value="{$data['ID_BEASISWA']}" {if $data['ID_BEASISWA']==$pengumuman.ID_BEASISWA}selected="true"{/if}>{$data['NM_BEASISWA']} oleh {$data['PENYELENGGARA_BEASISWA']} {$data['NM_GROUP_BEASISWA']}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Besar Beasiswa</td>
                <td>
                    <input type="text" name="besar" value="{$pengumuman.BESAR_BEASISWA}" class="required number"/>
                </td>
            </tr>
            <tr>
                <td>Syarat</td>
                <td>
                    <textarea name="syarat" class="required" style="width: 380px;height: 150px">{$pengumuman.SYARAT}</textarea>
                </td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td>
                    <textarea name="keterangan" class="required" style="width: 380px;height: 150px">{$pengumuman.KETERANGAN}</textarea>
                </td>
            </tr>
            <tr>
                <td>Batas Akhir</td>
                <td>
                    <input type="text" name="batas" class="datepick required" value="{$pengumuman.BATAS_AKHIR}"/>
                </td>
            </tr>
            <tr>
                <td class="center" colspan="2">
                    <a href="beasiswa-pengumuman.php" class="button">Kembali</a>
                    <input type="submit" value="Update" class="button" />
                    <input type="hidden" name="id" value="{$pengumuman.ID_PENGUMUMAN}"/>
                    <input type="hidden" name="mode" value="edit"/>                    
                </td>
            </tr>
        </table>
    </form>
{else if $smarty.get.mode=='hapus'}
    <form action="beasiswa-pengumuman.php" method="post">
        <table class="ui-widget-content" style="width: 90%">
            <tr>
                <th colspan="2" class="center">Hapus Pengumuman Beasiswa</th>
            </tr>
            <tr>
                <td>Nama Beasiswa</td>
                <td>
                    {foreach $data_beasiswa as $data}
                        {if $data['ID_BEASISWA']==$pengumuman.ID_BEASISWA}
                            {$data['NM_BEASISWA']} oleh {$data['PENYELENGGARA_BEASISWA']} {$data['NM_GROUP_BEASISWA']}
                        {/if}
                    {/foreach}
                </td>
            </tr>
            <tr>
                <td>Besar Beasiswa</td>
                <td>
                    {$pengumuman.BESAR_BEASISWA}
                </td>
            </tr>
            <tr>
                <td>Syarat</td>
                <td>
                    {$pengumuman.SYARAT}
                </td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td>
                    {$pengumuman.KETERANGAN}
                </td>
            </tr>
            <tr>
                <td>Batas Akhir</td>
                <td>
                    {$pengumuman.BATAS_AKHIR}
                </td>
            </tr>
            <tr>
                <td class="center" colspan="2">
                    Apakah anda yakin menghapus item ini?<br/>
                    <a href="beasiswa-pengumuman.php" class="button">Kembali</a>
                    <input type="submit" class="button" value="Ya" />
                    <input type="hidden" name="id" value="{$pengumuman.ID_PENGUMUMAN}"/>
                    <input type="hidden" name="mode" value="hapus"/>
                </td>
            </tr>
        </table>
    </form>
{/if}
{literal}
    <script>            
            $(function() {
                $( ".datepick" ).datepicker({dateFormat:'dd-M-y',changeMonth: true,
                        changeYear: true}).css({'text-transform':'uppercase'});
            });
            $('form').validate();
    </script>
{/literal}

