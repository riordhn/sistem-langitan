<div class="center_title_bar">Admisi Mahasiswa</div>

<table>
    <tr>
        <td>NIM / Nama</td>
        <td><form action="admisi-mhs.php" method="get" id="fcari">
            	<input name="nim" type="text" value="" size="30" class="required" />
                <input type="hidden" value="cari" name="mode" />
          		<input type="submit" value="Cari" />
            </form>
        </td>
    </tr>
</table>
<h3 style="color:#F00">{$error}</h2>
<form action="admisi-mhs.php" method="post" id="fproses">
<table style="width: 100%">
    <tr>
        <th>No</th>
        <th>NIM</th>
        <th>Nama</th>
        <th>Prodi</th>
        <th>Status</th>
        <th>SK</th>
        <th>App  <input type="checkbox" name="app" value="checkbox" class="app" id="app"></th>
    </tr>
    {foreach $mhs as $a}
    <tr>
        <td class="center">{$a@index+1}</td>
        <td>{$a.NIM_MHS}</td>
        <td>{$a.NM_PENGGUNA}</td>
        <td>{$a.NM_PROGRAM_STUDI} - {$a.NM_JENJANG}</td>
        <td>{$a.NM_STATUS_PENGGUNA}</td>
        <td><input type="text" value="" name="sk{$a@index+1}" /></td>
        <td class="center">
        <input type="checkbox" name="app{$a@index+1}" class="app" value="1" />
        <input type="hidden" name="nomer" value="{$a@index+1}" />
        <input type="hidden" name="id_status_pengguna" value="{$a.ID_STATUS_PENGGUNA}" />
        <input type="hidden" name="nim{$a@index+1}" value="{$a.NIM_MHS}" />
        <input type="hidden" name="id_mhs{$a@index+1}" value="{$a.ID_MHS}" />
		<input type="hidden" name="id_admisi{$a@index+1}" value="{$a.ID_ADMISI}" />
        </td>
        
    </tr>
    {/foreach}
    <tr>
    	<td colspan="7" class="center">
        	<input type="submit" value="P R O S E S" />
            <input type="hidden" value="proses" name="mode" />
        </td>
    </tr>
</table>
</form>

<script type="text/javascript">
$().ready(function() {
		$("#fcari").validate();
	});
	
$('#app').click(function() {
	var group = ':checkbox[class=' + $(this).attr('class') + ']';
	if(this.checked == false){
		$(group).attr('checked', false);
	}
	else{
		$(group).attr('checked', $(this).attr('checked'));
	}
});
</script>