<div class="center_title_bar">Admisi Mahasiswa</div>

<table>
    <tr>
        <td>NIM / Nama</td>
        <td><form action="admisi-mhs.php" method="get" id="fcari">
            	<input name="nim" type="text" value="" size="30" class="required" />
                <input type="hidden" value="cari" name="mode" />
          		<input type="submit" value="Cari" />
            </form>
        </td>
    </tr>
</table>

{if $error <> ''}
	<h2 style="color:#F00">{$error}</h2>    
{else}
<table style="width: 100%">
    <tr>
        <th>No</th>
        <th>NIM</th>
        <th>Nama</th>
        <th>Prodi</th>
        <th>Status</th>
        <th>Aksi</th>
    </tr>
    
    {foreach $mhs as $a}
    <tr>
        <td class="center">{$a@index+1}</td>
        <td>{$a.NIM_MHS}</td>
        <td>{$a.NM_PENGGUNA}</td>
        <td>{$a.NM_JENJANG} - {$a.NM_PROGRAM_STUDI}</td>
        <td>{$a.NM_STATUS_PENGGUNA}</td>
        <td><a href="admisi-mhs.php?mode=detail&id_mhs={$a.ID_MHS}">Detail</a></td>
    </tr>
    {/foreach}
</table>
{/if}
<script type="text/javascript">
$().ready(function() {
		$("#fcari").validate();
	});
</script>