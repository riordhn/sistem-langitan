<div class="center_title_bar">HAPUS KRS MAHASISWA</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="hapus-krs.php" method="post">
<table>
	<tr>
		<td>Program Studi </td>
		<td>
			<select name="prodi">
				<option value="">Semua</option>
				{foreach $prodi as $data}
				<option value="{$data.ID_PROGRAM_STUDI}" {if $data.ID_PROGRAM_STUDI == $id_prodi} selected="selected"{/if}>{$data.NM_JENJANG} - {$data.NM_PROGRAM_STUDI} (Fakultas {$data.NM_FAKULTAS})</option>
				{/foreach}
			</select>
		</td>
		<td><input type="submit" value="Tampil" /></td>
	</tr>
</table>
</form>


<div style="overflow: scroll;height: 550px;">
<table>
    <tr>
        <th>No</th>
        <th>Program Studi</th>
		<th>Nama Kelas</th>
        <th>Kode Mata Kuliah</th>
        <th>Nama Mata Kuliah</th>
        <th>Pegawai Hapus</th>
        <th>Aksi</th>
    </tr>
    {$i = 1}
    {foreach $kelas_set as $kelas}
    <tr>
        <td class="center">{$i++}</td>
        <td>{$kelas.NM_PROGRAM_STUDI}</td>
		<td>{$kelas.NAMA_KELAS}</td>
        <td>{$kelas.KD_MATA_KULIAH}</td>
        <td>{$kelas.NM_MATA_KULIAH}</td>
        <td>{$kelas.NM_PENGGUNA}</td>
        <td>
            <a href="hapus-krs.php?mode=detail&id_kelas_mk={$kelas.ID_KELAS_MK}">Hapus</a>
        </td>
    </tr>
    {foreachelse}
    <tr>
        <td class="center" colspan="6">Tidak Ada Data Kelas Dengan KRS Sudah Dihapus</td>
    </tr>
    {/foreach}
</table>
</div>