<div class="center_title_bar">HAPUS KRS MAHASISWA</div>

<a href="hapus-krs.php"><< Kembali</a>

<table>
    <thead>
        <tr>
            <th>
                HAPUS KRS UNTUK KELAS <u>{$kelas_mk.NAMA_KELAS}</u> MATKUL <u>{$kelas_mk.NM_MATA_KULIAH} ({$kelas_mk.KD_MATA_KULIAH})</u>
                <br>
                PROGRAM STUDI <u>{$kelas_mk.NM_PROGRAM_STUDI}</u>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="padding: 2px">
                
                <form action="hapus-krs.php" method="post">
                    
                    <table class="tablesorter" id="tabel_mhs">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" name="select_all" />
                                </th>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th>Angkatan</th>
                                <th>Kelas</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach $mahasiswa_set as $mahasiswa}
                                <tr>
                                    <td><input type="checkbox" name="id_pengambilan_mk[]" value="{$mahasiswa.ID_PENGAMBILAN_MK}" /></td>
                                    <td>{$mahasiswa.NIM_MHS}</td>
                                    <td>{$mahasiswa.NM_PENGGUNA}</td>
                                    <td class="center">{$mahasiswa.THN_ANGKATAN_MHS}</td>
                                    <td class="center">{$mahasiswa.NAMA_KELAS}</td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                            {if count($mahasiswa_set) > 0}
                                <tr>
                                    <td colspan="5" class="center">
                                        <input type="hidden" name="mode" value="delete" />
                                        <input type="submit" value="HAPUS KRS" />
                                    </td>
                                </tr>
                            {/if}
                        </tfoot>
                    </table>
                </form>
                
            </td>
        </tr>
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function() {
        $("#tabel_mhs").tablesorter({
            headers: {
              0: { sorter: false }  
            },
            sortList: [[1,0]] /* Order By NIM*/
        });
        
        /* Select All Checkbox */
        $('input[name="select_all"]').change(function() {
            var select_all_checked = this.checked;
            $('input[name="id_pengambilan_mk[]"]').each(function() {
                this.checked = select_all_checked;
            });
        });
    });
</script>