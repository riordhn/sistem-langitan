<div class="center_title_bar">Rentang Nilai</div>
<table>
    <tr>
        <th>Nilai Huruf</th>
        <th>Nilai Mutu</th>
        <th>Rentang Nilai Min</th>
        <th>Rentang Nilai Maks</th>
        <th>Aksi</th>
    </tr>
    {if $peraturan->PERATURAN_NILAIs->Count() > 0}
        {for $i=0 to $peraturan->PERATURAN_NILAIs->Count()-1}
        {$nilai=$peraturan->PERATURAN_NILAIs->Get($i)}
        <tr>
            <td class="center">{$nilai->STANDAR_NILAI->NM_STANDAR_NILAI}</td>
            <td class="center">{$nilai->STANDAR_NILAI->NILAI_STANDAR_NILAI}</td>
            <td class="center">{$nilai->NILAI_MIN_PERATURAN_NILAI}</td>
            <td class="center">{$nilai->NILAI_MAX_PERATURAN_NILAI}</td>
            <td class="center"><a href="perkuliahan-nilaimutu.php?mode=edit&id={$nilai->ID_PERATURAN_NILAI}">Edit</a></td>
        </tr>
        {/for}
    {/if}
    <tr>
        <td colspan="5" style="text-align: center;"><button value="perkuliahan-nilaimutu.php?mode=add">Tambah</button></td>
    </tr>
</table>
{include 'debug.tpl'}

