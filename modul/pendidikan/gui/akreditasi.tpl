<div class="center_title_bar">Akreditasi</div>  

{if $smarty.request.mode == 'add' or isset($smarty.request.id)}

<form action="akreditasi.php" method="post">
	<table>
        <tr>
        	<td>Fakultas</td>
            <td>:</td>
            <td>
            	<select name="fakultas" id="fakultas">
                    <option value="">Pilih Fakultas</option>
                    {foreach $fakultas as $data}
                    <option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS == $akreditasi.ID_FAKULTAS} selected="selected"{/if}>{$data.NM_FAKULTAS}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>Jenjang</td>
            <td>:</td>
			<td>
				<select id="jenjang" name="jenjang">
                    <option value="">Pilih Jenjang</option> 
                   {foreach $jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $akreditasi.ID_JENJANG==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>            			
         </tr>
        <tr>
        	<td>Program Studi</td>
            <td>:</td>
            <td>
                <select id="program_studi" name="program_studi" required="required">\
                	<option value="">Pilih Program Studi</option>
					{foreach $prodi as $data}
						<option value="{$data.ID_PROGRAM_STUDI}" {if $akreditasi.ID_PROGRAM_STUDI==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
					{/foreach}
                </select>
            </td>
        </tr>
        <tr>
        	<td>No. SK. Akreditasi</td>
            <td>:</td>
            <td><input type="text" name="sk_akreditasi" required="required" value="{$akreditasi.SK_AKREDITASI}" /></td>
        </tr>
        <tr>
        	<td>Nilai Angka Akreditasi</td>
            <td>:</td>
            <td><input type="text" name="nilai_angka_akreditasi" required="required" value="{$akreditasi.NILAI_ANGKA_AKREDITASI}"  /></td>
        </tr>
        <tr>
        	<td>Nilai Huruf Akreditasi</td>
            <td>:</td>
            <td><input type="text" name="nilai_huruf_akreditasi" required="required" value="{$akreditasi.NILAI_HURUF_AKREDITASI}"  /></td>
        </tr>
        <tr>
        	<td>Jatuh Tempo Penyelenggaraan</td>
            <td>:</td>
            <td><input type="text" name="jatuh_tempo_penyelenggaraan" class="datepicker" required="required" value="{$akreditasi.JATUH_TEMPO_PENYELENGGARAAN}"  /></td>
        </tr>
        <tr>
        	<td>Jatuh Tempo Akreditasi</td>
            <td>:</td>
            <td><input type="text" name="jatuh_tempo_akreditasi" class="datepicker" required="required" value="{$akreditasi.JATUH_TEMPO_AKREDITASI}"  /></td>
        </tr>
        <tr>
        	<td colspan="3" class="center"><input type="submit" value="Simpan" /></td>
        </tr>
    </table>
    
    {if isset($smarty.request.id)}
    	<input type="hidden" name="id_akreditasi_program_studi" value="{$smarty.request.id}" />
    {/if}
</form>

{else}

<form action="akreditasi.php" method="post">
<table>	    

	<tr>
		<td>Fakultas </td>
		<td>
			<select name="fakultas">
				<option value="">Semua</option>
				{foreach $fakultas as $data}
				<option value="{$data.ID_FAKULTAS}" {if $data.ID_FAKULTAS == $smarty.request.fakultas} selected="selected"{/if}>{$data.NM_FAKULTAS}</option>
				{/foreach}
			</select>
		</td>
		<td><input type="submit" value="Tampil" /></td>
	</tr>     
</table>
</form> 


<form action="akreditasi.php" method="post">
<table>
	<tr>
    	<td colspan="10" class="center">
        	<input type="submit" value="Tambah" name="tambah" />
            <input type="hidden" value="add" name="mode" />
        </td>
    </tr>

	<tr>
    	<th>fakultas</th>
        <th>jenjang</th>
    	<th>Program Studi</th>
        <th>SK AKREDITASI</th>
        <th>NILAI ANGKA</th>
        <th>NILAI HURUF</th>
        <th>JATUH TEMPO PENYELENGGARAAN</th>
        <th>JATUH TEMPO AKREDITASI</th>
        <th>aksi</th>
    </tr>
    {foreach $akreditasi as $data}
    	<tr>
        	<td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_JENJANG}</td>
            <td>{$data.NM_PROGRAM_STUDI}</td>
            <td>{$data.SK_AKREDITASI}</td>
            <td>{$data.NILAI_ANGKA_AKREDITASI}</td>
            <td>{$data.NILAI_HURUF_AKREDITASI}</td>
            <td>{$data.JATUH_TEMPO_PENYELENGGARAAN}</td>
            <td>{$data.JATUH_TEMPO_AKREDITASI}</td>
            <td><a href="akreditasi.php?id={$data.ID_AKREDITASI_PROGRAM_STUDI}">Edit</a></td>
        </tr>
    {/foreach}
    
</table>
    </form>  
{/if}



{literal}
    <script type="text/javascript">
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
    </script>
{/literal}