<div class="center_title_bar">IPK Mahasiswa</div>

<form action="history-ipk.php" method="get">
<table>
    <tr>
        <td>NIM</td>
        <td><input type="search" name="nim_mhs" {if isset($smarty.get.nim_mhs)}value="{$smarty.get.nim_mhs}"{/if}/><input type="submit" value="Tampilkan" /></td>
    </tr>
</table>
</form>

{if $mahasiswa_found}
{$pengambilan_mk_set=$mahasiswa->PENGAMBILAN_MKs}
<table style="width: 100%;" id="tabel-history">
    <thead>
        <tr>
            <td colspan="7">
                Nama: {$mahasiswa->PENGGUNA->NM_PENGGUNA}<br/>
                NIM: {$mahasiswa->NIM_MHS}<br/>
                Jurusan: {$mahasiswa->PROGRAM_STUDI->NM_PROGRAM_STUDI}<br/>
                Angkatan: 2009/2010<br/>
            </td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th style="width: 60px" class="middle center">Tahun Ajaran</th>
            <th style="width: 60px" class="middle center">Semester</th>
            <th class="middle center">Jumlah SKS</th>
            <th class="middle center">Total SKS</th>
            <th class="middle center">IPS</th>
            <th class="middle center">IPK</th>
            <th class="middle center">Ket.</th>
        </tr>
        
        {for $i=0 to $mahasiswa->MHS_STATUSs->Count()-1}
        {$mhs_status=$mahasiswa->MHS_STATUSs->Get($i)}
        <tr>
            <td class="middle center">{$mhs_status->SEMESTER->TAHUN_AJARAN}</td>
            <td class="middle center">{$mhs_status->SEMESTER->NM_SEMESTER}</td>
            <td class="middle center">{$mhs_status->SKS_SEMESTER}</td>
            <td class="middle center">{$mhs_status->SKS_TOTAL_MHS_STATUS}</td>
            <td class="middle center">{$mhs_status->IPS_MHS_STATUS}</td>
            <td class="middle center">{$mhs_status->IPK_MHS_STATUS}</td>
            <td class="middle center"></td>
        </tr>
        {/for}
    </tbody>
</table>
{/if}
