<div class="center_title_bar">History Ujian Mahasiswa</div>

<form action="history-ujian.php" method="get">
<table>
    <tr>
        <td>NIM</td>
        <td><input type="search" name="nim_mhs" {if isset($smarty.get.nim_mhs)}value="{$smarty.get.nim_mhs}"{/if}/><input type="submit" value="Tampilkan" /></td>
    </tr>
</table>
</form>

{if $mahasiswa_found}
<table style="width: 100%;" id="tabel-history">
    <col style="width: 50px"/>
    <col style="width: 50px"/>
    <col /><col /><col /><col />
    <thead>
        <tr>
            <td colspan="6">
                Nama: {$mahasiswa->PENGGUNA->NM_PENGGUNA}<br/>
                NIM: {$mahasiswa->NIM_MHS}<br/>
                Jurusan: {$mahasiswa->PROGRAM_STUDI->NM_PROGRAM_STUDI}<br/>
                Angkatan: 2009/2010<br/>
            </td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>Tahun</th>
            <th>Semester</th>
            <th>Mata Kuliah</th>
            <th>UTS</th>
            <th>UAS</th>
            <th>Ket.</th>
        </tr>
        {* Menampilkan data status semester mahasiswa *}
        {for $i=0 to $mahasiswa->MHS_STATUSs->Count()-1}
        {$mhs_status=$mahasiswa->MHS_STATUSs->Get($i)}
        <tr>
            <td class="center" {if $mhs_status->JUMLAH_MK>1}rowspan="{$mhs_status->JUMLAH_MK}"{/if}>{$mhs_status->SEMESTER->TAHUN_AJARAN}</td>
            <td class="center" {if $mhs_status->JUMLAH_MK>1}rowspan="{$mhs_status->JUMLAH_MK}"{/if}>{$mhs_status->SEMESTER->NM_SEMESTER}</td>
            <td>
            {if $mhs_status->SEMESTER->PENGAMBILAN_MKs->Count()>1}
                {$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get(0)->KELAS_MK->MATA_KULIAH->NM_MATA_KULIAH}
            {/if}
            </td>
            <td>
            {if $mhs_status->SEMESTER->PENGAMBILAN_MKs->Count()>1}
                {$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get(0)->KELAS_MK->TGL_UTS}
            {/if}
            </td>
            <td>
            {if $mhs_status->SEMESTER->PENGAMBILAN_MKs->Count()>1}
                {$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get(0)->KELAS_MK->TGL_UAS}
            {/if}
            </td>
            <td></td>
        </tr>
        {* Menampilkan data mata kuliah *}
        {if $mhs_status->SEMESTER->PENGAMBILAN_MKs->Count()>1}
        {for $j=1 to $mhs_status->SEMESTER->PENGAMBILAN_MKs->Count()-1}
        <tr>
            <td>{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get($j)->KELAS_MK->MATA_KULIAH->NM_MATA_KULIAH}</td>
            <td>{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get($j)->KELAS_MK->TGL_UTS}</td>
            <td>{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get($j)->KELAS_MK->TGL_UAS}</td>
            <td></td>
        </tr>
        {/for}
        {/if}
        {/for}
    </tbody>
</table>
{/if}