<div class="center_title_bar">History SKS Mahasiswa</div>

<form action="history-sks.php" method="get">
<table>
    <tr>
        <td>NIM</td>
        <td><input type="search" name="nim_mhs" {if isset($smarty.get.nim_mhs)}value="{$smarty.get.nim_mhs}"{/if}/><input type="submit" value="Tampilkan" /></td>
    </tr>
</table>
</form>

{if $mahasiswa_found}
<table style="width: 100%;" id="tabel-history">
    <thead>
        <tr>
            <td colspan="10">
                Nama: {$mahasiswa->PENGGUNA->NM_PENGGUNA}<br/>
                NIM: {$mahasiswa->NIM_MHS}<br/>
                Jurusan: {$mahasiswa->PROGRAM_STUDI->NM_PROGRAM_STUDI}<br/>
                Angkatan: 2009/2010<br/>
            </td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th style="width: 60px" class="middle center">Tahun Ajaran</th>
            <th style="width: 60px" class="middle center">Semester</th>
            <th class="middle center">Mata Kuliah</th>
            <th class="middle center">SKS</th>
            <th class="middle center">Total SKS</th>
            <th class="middle center">Maksimum SKS</th>
            <th class="middle center">Ket.</th>
        </tr>
        
        {for $i=0 to $mahasiswa->MHS_STATUSs->Count()-1}
        {$mhs_status=$mahasiswa->MHS_STATUSs->Get($i)}
        <tr>
            <td class="middle center" {if $mhs_status->JUMLAH_MK>1}rowspan="{$mhs_status->JUMLAH_MK}"{/if}>{$mhs_status->SEMESTER->TAHUN_AJARAN}</td>
            <td class="middle center" {if $mhs_status->JUMLAH_MK>1}rowspan="{$mhs_status->JUMLAH_MK}"{/if}>{$mhs_status->SEMESTER->NM_SEMESTER}</td>
            <td style="width: 200px;">
            {if $mhs_status->SEMESTER->PENGAMBILAN_MKs->Count()>1}
                [{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get(0)->KELAS_MK->MATA_KULIAH->KODE_MATA_KULIAH}]
                {$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get(0)->KELAS_MK->MATA_KULIAH->NM_MATA_KULIAH}
            {/if}
            </td>
            <td class="center">
            {if $mhs_status->SEMESTER->PENGAMBILAN_MKs->Count()>1}
                {$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get(0)->KELAS_MK->MATA_KULIAH->KREDIT_MATA_KULIAH}
            {/if}
            </td>
            <td class="middle center" {if $mhs_status->JUMLAH_MK>1}rowspan="{$mhs_status->JUMLAH_MK}"{/if}>{$mhs_status->SKS_SEMESTER}</td>
            <td class="middle center" {if $mhs_status->JUMLAH_MK>1}rowspan="{$mhs_status->JUMLAH_MK}"{/if}>{$mhs_status->SKS_MAX}</td>
            <td class="middle center" {if $mhs_status->JUMLAH_MK>1}rowspan="{$mhs_status->JUMLAH_MK}"{/if}></td>
        </tr>
        {if $mhs_status->SEMESTER->PENGAMBILAN_MKs->Count()>1}
        {for $j=1 to $mhs_status->SEMESTER->PENGAMBILAN_MKs->Count()-1}
        <tr>
            <td style="width: 200px;">
                [{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get($j)->KELAS_MK->MATA_KULIAH->KODE_MATA_KULIAH}]
                {$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get($j)->KELAS_MK->MATA_KULIAH->NM_MATA_KULIAH}
            </td>
            <td class="center">{$mhs_status->SEMESTER->PENGAMBILAN_MKs->Get($j)->KELAS_MK->MATA_KULIAH->KREDIT_MATA_KULIAH}</td>
        </tr>
        {/for}
        {/if}
        {/for}
    </tbody>
</table>
{/if}
