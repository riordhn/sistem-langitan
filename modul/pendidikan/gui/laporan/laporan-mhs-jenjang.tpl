<div class="center_title_bar">LAPORAN MAHASISWA BERDASARKAN JENJANG</div>
<form action="laporan-mhs-jenjang.php" method="post" id="f2">
	<table>
		<tr>
			<th colspan="8">PARAMETER</th>
		</tr>
		<tr>
			<td>
				Angkatan Mhs
			</td>
			<td>
				<select name="angkatan">
					<option value="">Semua</option>
					{foreach $angkatan as $data}
					<option value="{$data.THN_ANGKATAN_MHS}" {if $smarty.request.angkatan == $data.THN_ANGKATAN_MHS} selected="selected"{/if}>{$data.THN_ANGKATAN_MHS}</option>
					{/foreach}
				</select>
			</td>
			<td>
				ATAU
			</td>
			<td>
				Tahun Akademik
			</td>
			<td>
				<select name="thn_akademik">
					<option value="">Pilih Tahun Akademik</option>
					{foreach $thn_akademik as $data}
					<option value="{$data.THN_AKADEMIK_SEMESTER}" {if $smarty.request.thn_akademik == $data.THN_AKADEMIK_SEMESTER} selected="selected"{/if}>{$data.TAHUN_AJARAN}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>		
            <td>
				Tampilan
			</td>
			<td>
				<select name="tampilan">
					<option value="fak" {if $smarty.request.tampilan == 'fak'} selected="selected"{/if}>Fakultas</option>
                    <option value="prodi" {if $smarty.request.tampilan == 'prodi'} selected="selected"{/if}>Prodi</option>
		    <option value="thn_angkatan" {if $smarty.request.tampilan == 'thn_angkatan'} selected="selected"{/if}>Angkatan</option>
				</select>
			</td>
			<td></td>
			<td>
				Status Akademik
			</td>
			<td>
				<select name="status_akademik">
					<option value="">Semua</option>
					<option value="1" {if $smarty.request.status_akademik == 1} selected="selected"{/if}>Aktif</option>
					{foreach $status_tidak_aktif as $data}
					<option value="{$data.ID_STATUS_PENGGUNA}" {if $smarty.request.status_akademik == $data.ID_STATUS_PENGGUNA} selected="selected"{/if}>{$data.NM_STATUS_PENGGUNA}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="8" style="text-align:center"><input type="submit" value="Tampil" /></td>
		</tr>
	</table>
</form>




{if isset($mhs_jenjang)}

{if $smarty.get.tampilan == 'detail'}
<table>
		<tr>
			<th>No</th>
			<th>NIM</th>	
			<th>Nama</th>
			<th>Fakultas</th>
			<th>Jenjang</th>
			<th>Prodi</th>
			<th>Status Akademik</th>
		</tr>
		{$no = 1}
		{foreach $mhs_jenjang as $data}
		<tr>
			<td>{$no++}</td>
			<td>{$data.NIM_MHS}</td>
			<td>{$data.NM_PENGGUNA}</td>
			<td>{$data.NM_FAKULTAS}</td>
			<td>{$data.NM_JENJANG}</td>
			<td>{$data.NM_PROGRAM_STUDI}</td>
			<td>{$data.NM_STATUS_PENGGUNA}</td>
		</tr>
		{/foreach}
</table>

{else}	
	{if $smarty.post.thn_akademik == ''}
		Tampilan berdasarkan angkatan
	{else}
		Tampilan berdasarkan tahun akademik
	{/if}
	
	<table>
		<tr>
			<th>NO</th>
            {if $smarty.post.tampilan == 'fak'}
			<th>FAKUTLAS</th>
	    {else if $smarty.post.tampilan == 'thn_angkatan'}
			<th>THN ANGKATAN</th>
            {else}
            <th>FAKUTLAS</th>
            <th>PRODI</th>
            {/if}
			<th>D3</th>
			<th>S1-REGULER</th>
            <th>S1-AJ</th>
            <th>S1-INTERNASIONAL</th>
			<th>S2</th>
			<th>S3</th>
			<th>PROFESI</th>
			<th>SPESIALIS</th>
			<th>TOTAL</th>
		</tr>
		{$no = 1}
		{foreach $mhs_jenjang as $data}
		<tr>
			<td>{$no++}</td>
            {if $smarty.post.tampilan == 'fak'}
			<td>{$data.NM_FAKULTAS}</td>
	    {else if $smarty.post.tampilan == 'thn_angkatan'}
			<td>{$data.THN_ANGKATAN_MHS}</td>
            {else}
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_PROGRAM_STUDI}</td>
            {/if}
			<td><a href="laporan-mhs-jenjang.php?tampilan=detail&angkatan={$smarty.post.angkatan}&thn_akademik={$smarty.post.thn_akademik}&status_akademik={$smarty.post.status_akademik}&jenjang=5&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}&status=R">{$data.D3}</a></td>
			<td><a href="laporan-mhs-jenjang.php?tampilan=detail&angkatan={$smarty.post.angkatan}&thn_akademik={$smarty.post.thn_akademik}&status_akademik={$smarty.post.status_akademik}&jenjang=1&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}&status=R">{$data.S1_REGULER}</a></td>
            <td><a href="laporan-mhs-jenjang.php?tampilan=detail&angkatan={$smarty.post.angkatan}&thn_akademik={$smarty.post.thn_akademik}&status_akademik={$smarty.post.status_akademik}&jenjang=1&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}&status=AJ">{$data.S1_AJ}</a></td>
            <td><a href="laporan-mhs-jenjang.php?tampilan=detail&angkatan={$smarty.post.angkatan}&thn_akademik={$smarty.post.thn_akademik}&status_akademik={$smarty.post.status_akademik}&jenjang=1&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}&status=I">{$data.S1_I}</a></td>
			<td><a href="laporan-mhs-jenjang.php?tampilan=detail&angkatan={$smarty.post.angkatan}&thn_akademik={$smarty.post.thn_akademik}&status_akademik={$smarty.post.status_akademik}&jenjang=2&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}&status=R">{$data.S2}</td>
			<td><a href="laporan-mhs-jenjang.php?tampilan=detail&angkatan={$smarty.post.angkatan}&thn_akademik={$smarty.post.thn_akademik}&status_akademik={$smarty.post.status_akademik}&jenjang=3&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}&status=R">{$data.S3}</td>
			<td><a href="laporan-mhs-jenjang.php?tampilan=detail&angkatan={$smarty.post.angkatan}&thn_akademik={$smarty.post.thn_akademik}&status_akademik={$smarty.post.status_akademik}&jenjang=9&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}&status=R">{$data.PROFESI}</td>
			<td><a href="laporan-mhs-jenjang.php?tampilan=detail&angkatan={$smarty.post.angkatan}&thn_akademik={$smarty.post.thn_akademik}&status_akademik={$smarty.post.status_akademik}&jenjang=10&fakultas={$data.ID_FAKULTAS}&prodi={$data.ID_PROGRAM_STUDI}&status=R">{$data.SPESIALIS}</td>
			<td>{$data.TOTAL}</td>
		</tr>
		{$total = $total + $data.TOTAL}
		{$totald3 = $totald3 + $data.D3}
		{$totals1 = $totals1 + $data.S1_REGULER}
        {$totals1_aj = $totals1_aj + $data.S1_AJ}
        {$totals1_i = $totals1_i + $data.S1_I}
		{$totals2 = $totals2 + $data.S2}
		{$totals3 = $totals3 + $data.S3}
		{$totalprof = $totalprof + $data.PROFESI}
		{$totalspes = $totalspes + $data.SPESIALIS}
		{/foreach}
		<tr>
            {if $smarty.post.tampilan == 'fak'}
			<td colspan="2">Total</td>
            {else if $smarty.post.tampilan == 'thn_angkatan'}
			<td colspan="2">Total</td>
	    {else}
            <td colspan="3">Total</td>
            {/if}
			<td>{$totald3}</td>
			<td>{$totals1}</td>
            <td>{$totals1_aj}</td>
            <td>{$totals1_i}</td>
			<td>{$totals2}</td>
			<td>{$totals3}</td>
			<td>{$totalprof}</td>
			<td>{$totalspes}</td>
			<td><a href="laporan-mhs-jenjang.php?tampilan=detail&angkatan={$smarty.post.angkatan}&thn_akademik={$smarty.post.thn_akademik}&status_akademik={$smarty.post.status_akademik}">{$total}</a></td>
		</tr>
	</table>
{/if}
{/if}