<div class="center_title_bar">UPADTE HISTORY MAHASISWA</div>
<form action="laporan-mhs-history.php" method="post" id="f2">
	<table>
		<tr>
			<th colspan="8">PARAMETER</th>
		</tr>
		<tr>		
            <td>
				Tampilan
			</td>
			<td>
				<select name="tampilan">
					<option value="fak" {if $smarty.request.tampilan == 'fak'} selected="selected"{/if}>Fakultas</option>
                    <option value="prodi" {if $smarty.request.tampilan == 'prodi'} selected="selected"{/if}>Prodi</option>
				</select>
			</td>
			<td>
				Status Akademik
			</td>
			<td>
				<select name="status_akademik">
					<option value="">Semua</option>
					<option value="1" {if $smarty.request.status_akademik == 1} selected="selected"{/if}>Aktif</option>
					{foreach $status_tidak_aktif as $data}
					<option value="{$data.ID_STATUS_PENGGUNA}" {if $smarty.request.status_akademik == $data.ID_STATUS_PENGGUNA} selected="selected"{/if}>{$data.NM_STATUS_PENGGUNA}</option>
					{/foreach}
				</select>
			</td>			
			<td>
				Semester
			</td>
			<td>
				<select name="semester">
					<option value="">Semua</option>
					{foreach $semester as $data}
					<option value="{$data.ID_SEMESTER}" {if $smarty.request.semester == $data.ID_SEMESTER} selected="selected"{/if}>{$data.THN_AKADEMIK_SEMESTER} - {$data.NM_SEMESTER}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="8" style="text-align:center"><input type="submit" value="Tampil" /></td>
		</tr>
	</table>
</form>

