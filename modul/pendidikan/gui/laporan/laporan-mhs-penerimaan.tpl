<div class="center_title_bar">LAPORAN MAHASISWA BERDASARKAN PENERIMAAN MAHASISWA BARU</div>
<form action="laporan-mhs-penerimaan.php" method="post">
	<table>
		<tr>
			<th colspan="5">PARAMETER</th>
		</tr>
		<tr>
            <td>Fakultas</td>
            <td>
                <select id="fakultas" name="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $smarty.post.fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
			<td>Semester</td>
            <td>
            	<select name="semester">
                	<option value="">Semua</option>
                    <option value="Gasal" {if $smarty.post.semester == 'Gasal'} selected="selected"{/if}>Ganjil</option>
                    <option value="Genap" {if $smarty.post.semester == 'Genap'} selected="selected"{/if}>Genap</option>	
                </select>
            </td>
        </tr>
		<tr>
            <td>Jalur</td>
            <td>
                <select id="jalur" name="jalur">
                    <option value="">Semua</option>
                    {foreach $data_jalur as $data}
                        <option value="{$data.ID_JALUR}" {if $smarty.post.jalur==$data.ID_JALUR}selected="true"{/if}>{$data.NM_JALUR|upper}</option>
                    {/foreach}
                </select>
            </td>
			<td>Tahun Penerimaan</td>
            <td>
               <select name="thn" id="thn">
               	<option value="">Semua</option>
                    {foreach $data_thn as $data}
                        <option value="{$data.TAHUN}" {if $smarty.post.thn==$data.TAHUN}selected="true"{/if}>{$data.TAHUN|upper}</option>
                    {/foreach}
               </select>
            </td>
		</tr>
		<tr>
			
            <td>Jenjang</td>
            <td>
                <select id="jenjang" name="jenjang">
                    <option value="">Semua</option>
                    {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.post.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG|upper}</option>
                    {/foreach}
                </select>
            </td>
		 <td>Gelombang</td>
            <td>
                <select id="gelombang" name="gelombang">
                    <option value="">Semua</option>
                    {section name=foo loop=3} 
                    	<option value="{$smarty.section.foo.iteration}" {if $smarty.post.gelombang==$smarty.section.foo.iteration}selected="true"{/if}>{$smarty.section.foo.iteration}</option>
					{/section}
                </select>
            </td>
        </tr>
		<tr>
			<td>
				Nama Penerimaan
			</td>
			<td>				
            	<select name="id_penerimaan">
                    <option value="">Pilih Penerimaan</option>
                    {foreach $penerimaan_set as $p}
                        <optgroup label="{$p.TAHUN} {$p.SEMESTER}">
                            {foreach $p.p_set as $p2}
                                <option value="{$p2.ID_PENERIMAAN}" {if $p2.ID_PENERIMAAN == $smarty.post.id_penerimaan}selected="selected"{/if}>Gelombang {$p2.GELOMBANG} {$p2.NM_PENERIMAAN} Jalur ({$p2.NM_JALUR})</option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>
			</td>
			<td>
				Tampilan
			</td>
			<td>
				<select name="tampilan">
					<option value="fak" {if $smarty.post.tampilan == 'fak'} selected="selected"{/if}>Fakultas</option>
                    <option value="prodi" {if $smarty.post.tampilan == 'prodi'} selected="selected"{/if}>Prodi</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Tgl Penerimaan Dari</td>
			<td>
				<input type="text" id="tgl_mulai" name="tgl_mulai"  value="{$smarty.post.tgl_mulai}" />
			</td>
            <td>Tgl Penerimaan Sampai</td>
            <td>
				<input type="text" id="tgl_selesai" name="tgl_selesai"  value="{$smarty.post.tgl_selesai}" />
			</td>
		</tr>
        
        <tr>
            <td>Pembayaran Awal</td>
            <td>
               <input type="text" name="tgl_awal" id="tgl_awal" value="{if isset($smarty.get.tgl_awal)}{$smarty.get.tgl_awal}{else}{$smarty.post.tgl_awal}{/if}" />
            </td>
            <td>Pembayaran Akhir</td>
            <td>
            	<input type="text" name="tgl_akhir" id="tgl_akhir" value="{if isset($smarty.get.tgl_akhir)}{$smarty.get.tgl_akhir}{else}{$smarty.post.tgl_akhir}{/if}" />
            </td>
        </tr>
		<tr>
			<td colspan="4" style="text-align:center"><input type="submit" value="Tampil" /></td>
		</tr>
	</table>
</form>


{if isset($penerimaan)}

<table>
	<tr>
		<th>NO</th>
		<th>FAKULTAS</th>
		{if $smarty.post.tampilan == 'prodi'}
		<th>PRODI</th>
		{/if}
		<th>NAMA PENERIMAAN</th>
		<th>DITERIMA</th>
		<th>REGMABA</th>
		<th>VERIFIKASI KEUANGAN</th>
		<th>BAYAR</th>
		<th>PENANGGUHAN</th>
		<th>PEMBEBASAN</th>
		<th>BIDIK MISI UNAIR</th>
		<th>BIDIK MISI NASIONAL</th>
		<th>VERIFIKASI PENDIDIKAN</th>
		<th>FINGER</th>
		<th>KTM</th>
		<th>TIDAK LULUS KESEHATAN</th>
		<th>REGISTRASI</th>
		<th>TIDAK REGISTRASI</th>
	</tr>
	{$no = 1}
	{$diterima = 0}
	{$regmaba = 0}
	{$verifikasi_keuangan = 0}
	{$bayar = 0}
	{$penangguhan = 0}
	{$pembebasan = 0}
	{$bidik_misi_unair = 0}
	{$bidik_misi_nasional = 0}
	{$verifikasi_pendidikan = 0}
	{$finger = 0}
	{$ktm = 0}
	{$tidak_lulus_kesehatan = 0}
	{$registrasi = 0}
	{$tidak_registrasi = 0}
	
	{foreach $penerimaan as $data}
	<tr>
		<td>{$no++}</td>
		<td>{$data.NM_FAKULTAS}</td>
		{if $smarty.post.tampilan == 'prodi'}
		<td>{$data.NM_PROGRAM_STUDI}</td>
		{/if}
		<td>{$data.NAMA_PENERIMAAN}</td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$data.ID_FAKULTAS}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=diterima">{$data.DITERIMA}</a></td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$data.ID_FAKULTAS}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=regmaba">{$data.REGMABA}</a></td>
		<td>{$data.VERIFIKASI_KEUANGAN}</td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$data.ID_FAKULTAS}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=bayar">{$data.BAYAR}</a></td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$data.ID_FAKULTAS}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=penangguhan">{$data.PENANGGUHAN}</a></td>
		<td>{$data.PEMBEBASAN}</td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$data.ID_FAKULTAS}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=bayar">{$data.BIDIK_MISI_UNAIR}</a></td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$data.ID_FAKULTAS}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=bidik">{$data.BIDIK_MISI_NASIONAL}</a></td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$data.ID_FAKULTAS}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=verifikasi">{$data.VERIFIKASI_PENDIDIKAN}</a></td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$data.ID_FAKULTAS}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=finger">{$data.FINGER}</a></td>
		<td>{$data.KTM}</td>
		<td>{$data.TIDAK_LULUS_KESEHATAN}</td>
		<td>{$data.REGISTRASI}</td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$data.ID_FAKULTAS}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=tidak_registrasi">{$data.TIDAK_REGISTRASI}</a></td>
	</tr>
	{$diterima = $diterima + $data.DITERIMA}
	{$regmaba = $regmaba + $data.REGMABA}
	{$verifikasi_keuangan = $verifikasi_keuangan + $data.VERIFIKASI_KEUANGAN}
	{$bayar = $bayar + $data.BAYAR}
	{$penangguhan = $penangguhan + $data.PENANGGUHAN}
	{$pembebasan = $pembebasan + $data.PEMBEBASAN}
	{$bidik_misi_unair = $bidik_misi_unair + $data.BIDIK_MISI_UNAIR}
	{$bidik_misi_nasional = $bidik_misi_nasional + $data.BIDIK_MISI_NASIONAL}
	{$verifikasi_pendidikan = $verifikasi_pendidikan + $data.VERIFIKASI_PENDIDIKAN}
	{$finger = $finger + $data.FINGER}
	{$ktm = $ktm + $data.KTM}
	{$tidak_lulus_kesehatan = $tidak_lulus_kesehatan + $data.TIDAK_LULUS_KESEHATAN}
	{$registrasi = $registrasi + $data.REGISTRASI}
	{$tidak_registrasi = $tidak_registrasi + $data.TIDAK_REGISTRASI}
	{/foreach}
	<tr>
		<td colspan="{if $smarty.post.tampilan == 'fak'}3{else}4{/if}"></td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$smarty.post.fakultas}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=diterima">{$diterima}</a></td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$smarty.post.fakultas}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=regmaba">{$regmaba}</a></td>
		<td>{$verifikasi_keuangan}</td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$smarty.post.fakultas}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=bayar">{$bayar}</a></td>
		<td>{$penangguhan}</td>
		<td>{$pembebasan}</td>
		<td>{$bidik_misi_unair}</td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$smarty.post.fakultas}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=bidik">{$bidik_misi_nasional}</a></td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$smarty.post.fakultas}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=verifikasi">{$verifikasi_pendidikan}</a></td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$smarty.post.fakultas}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=finger">{$finger}</a></td>
		<td>{$ktm}</td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$smarty.post.fakultas}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=kesehatan">{$tidak_lulus_kesehatan}</a></td>
		<td>{$registrasi}</td>
		<td><a href="laporan-mhs-penerimaan-detail.php?fakultas={$smarty.post.fakultas}&semester={$smarty.post.semester}&jalur={$smarty.post.jalur}&thn={$smarty.post.thn}&jenjang={$smarty.post.jenjang}&gelombang={$smarty.post.gelombang}&id_penerimaan={$smarty.post.id_penerimaan}&id_prodi={$data.ID_PROGRAM_STUDI}&status=tidak_registrasi">{$tidak_registrasi}</a></td>
	</tr>
</table>

{/if}


{literal}
    <script>
            $("#tgl_mulai").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$("#tgl_selesai").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$("#tgl_awal").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
		$("#tgl_akhir").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});	
    </script>
{/literal}
