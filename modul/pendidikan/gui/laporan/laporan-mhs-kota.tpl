<div class="center_title_bar">LAPORAN MAHASISWA BERDASARKAN KOTA</div>
<form action="laporan-mhs-kota.php" method="post">
	<table>
		<tr>
			<th colspan="8">PARAMETER</th>
		</tr>
		<tr>
			<td>
				Angkatan Mhs
			</td>
			<td>
				<select name="angkatan">
					<option value="">Semua</option>
					{foreach $angkatan as $data}
					<option value="{$data.THN_ANGKATAN_MHS}" {if $smarty.post.angkatan == $data.THN_ANGKATAN_MHS} selected="selected"{/if}>{$data.THN_ANGKATAN_MHS}</option>
					{/foreach}
				</select>
			</td>
			<td>
				ATAU
			</td>
			<td>
				Tahun Akademik
			</td>
			<td>
				<select name="thn_akademik">
					<option value="">Pilih Tahun Akademik</option>
					{foreach $thn_akademik as $data}
					<option value="{$data.THN_AKADEMIK_SEMESTER}" {if $smarty.post.thn_akademik == $data.THN_AKADEMIK_SEMESTER} selected="selected"{/if}>{$data.TAHUN_AJARAN}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
            <td>Fakultas</td>
            <td>
                <select id="fakultas" name="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $smarty.post.fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
			<td>
			</td>
            <td>Jenjang</td>
            <td>
                <select id="jenjang" name="jenjang">
                    <option value="">Semua</option>
                    {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.post.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
		<tr>	
			<td>Program Studi</td>
            <td colspan="4">
                <select id="program_studi" name="program_studi">
                    <option value="">Semua</option>
					{foreach $prodi as $data}
						<option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.post.program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
					{/foreach}
                </select>
            </td>
        </tr>
		<tr>
			<td>Negara</td>
			<td colspan="4">
				<select id="negara" name="negara">
                    <option value="">Semua</option>
					{foreach $data_negara as $data}
                        <option value="{$data.ID_NEGARA}" {if $smarty.post.negara==$data.ID_NEGARA}selected="true"{/if}>{$data.NM_NEGARA|upper}</option>
                    {/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td>Provinsi</td>
			<td>
				<select id="provinsi" name="provinsi">
                    <option value="">Semua</option>
                </select>
			</td>
			<td>
			</td>
			<td>kota</td>
			<td>
				<select id="kota" name="kota">
                    <option value="">Semua</option>
                </select>
			</td>
		</tr>
		<tr>		
            <td>
				Tampilan
			</td>
			<td>
				<select name="tampilan">
					<option value="fak" {if $smarty.post.tampilan == 'fak'} selected="selected"{/if}>Fakultas</option>
                    <option value="prodi" {if $smarty.post.tampilan == 'prodi'} selected="selected"{/if}>Prodi</option>
					<option value="negara" {if $smarty.post.tampilan == 'negara'} selected="selected"{/if}>Negara</option>
				</select>
			</td>
			<td></td>
			<td>
				Status Akademik
			</td>
			<td>
				<select name="status_akademik">
					<option value="">Semua</option>
					<option value="1" {if $smarty.post.status_akademik == 1} selected="selected"{/if}>Aktif</option>
					{foreach $status_tidak_aktif as $data}
					<option value="{$data.ID_STATUS_PENGGUNA}" {if $smarty.post.status_akademik == $data.ID_STATUS_PENGGUNA} selected="selected"{/if}>{$data.NM_STATUS_PENGGUNA}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="8" style="text-align:center">
				<input type="submit" value="Tampil" />
				<input type="hidden" value="show" name="mode" />
			</td>
		</tr>
	</table>
</form>
{if isset($mhs_jenjang)}
	
	{if $smarty.post.thn_akademik == ''}
		Tampilan berdasarkan angkatan
	{else}
		Tampilan berdasarkan tahun akademik
	{/if}
	
	<table>
		<tr>
			<th>NO</th>
            {if $smarty.post.tampilan == 'fak'}
			<th>FAKUTLAS</th>
            {elseif $smarty.post.tampilan == 'negara'}
            <th>NEGARA</th>
			{else}
            <th>FAKUTLAS</th>
            <th>PRODI</th>
            {/if}
			<th>D3</th>
			<th>S1</th>
			<th>S2</th>
			<th>S3</th>
			<th>PROFESI</th>
			<th>SPESIALIS</th>
			<th>TOTAL</th>
		</tr>
		{$no = 1}
		{foreach $mhs_jenjang as $data}
		<tr>
			<td>{$no++}</td>
            {if $smarty.post.tampilan == 'fak'}
			<td>{$data.NM_FAKULTAS}</td>
            {elseif $smarty.post.tampilan == 'negara'}
            <td>{$data.NM_NEGARA}</td>
			{else}
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_PROGRAM_STUDI}</td>
            {/if}
			<td>{$data.D3}</td>
			<td>{$data.S1}</td>
			<td>{$data.S2}</td>
			<td>{$data.S3}</td>
			<td>{$data.PROFESI}</td>
			<td>{$data.SPESIALIS}</td>
			<td>{$data.TOTAL}</td>
		</tr>
		{$total = $total + $data.TOTAL}
		{$totald3 = $totald3 + $data.D3}
		{$totals1 = $totals1 + $data.S1}
		{$totals2 = $totals2 + $data.S2}
		{$totals3 = $totals3 + $data.S3}
		{$totalprof = $totalprof + $data.PROFESI}
		{$totalspes = $totalspes + $data.SPESIALIS}
		{/foreach}
		<tr>
        	{if $smarty.post.tampilan == 'fak' or $smarty.post.tampilan == 'negara'}
			<td colspan="2">Total</td>
            {else}
            <td colspan="3">Total</td>
            {/if}
			<td>{$totald3}</td>
			<td>{$totals1}</td>
			<td>{$totals2}</td>
			<td>{$totals3}</td>
			<td>{$totalprof}</td>
			<td>{$totalspes}</td>
			<td>{$total}</td>
		</tr>
	</table>
	
{/if}

{literal}
    <script type="text/javascript">
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#negara').change(function(){
            $.ajax({
                type:'post',
                url:'getNegara.php',
                data:'id_negara='+$('#negara').val(),
                success:function(data){
                    $('#provinsi').html(data);
                }                    
            })
			
		 });
		 
		 $('#provinsi').change(function(){
				$.ajax({
					type:'post',
					url:'getKota.php',
					data:'id_provinsi='+$('#provinsi').val(),
					success:function(data){
						$('#kota').html(data);
					}                    
				})
		 });
    </script>
{/literal}