<div class="center_title_bar">DETAIL LAPORAN MAHASISWA BERDASARKAN PENERIMAAN MAHASISWA BARU</div>
{if isset($kesehatan)}

<table>
	<tr>
		<th>NO</th>
        <th>NO UJIAN</th>
        
        <th>NAMA</th>
        <th>JALUR</th>
		<th>FAKULTAS</th>
		<th>PRODI</th>
        <th>NIM</th>
        <th>FAKULTAS PINDAH</th>
		<th>PRODI PINDAH</th>
	</tr>
	{$no = 1}
	
	{foreach $kesehatan as $data}
	<tr>
		<td>{$no++}</td>
        <td>{$data.NO_UJIAN}</td>
        
        <td>{$data.NM_C_MHS}</td>
        <td>{$data.NM_PENERIMAAN}</td>
		<td>{$data.NM_FAKULTAS}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
        <td>{$data.NIM_MHS}</td>
		<td>{$data.NM_FAK}</td>
        <td>{$data.NM_PROD}</td>
	</tr>
	{/foreach}

</table>
{elseif isset($diterima)}

<table>
	<tr>
		<th>NO</th>
        <th>NO UJIAN</th>
        <th>VOUCHER</th>
        <th>NAMA</th>
        <th>JALUR</th>
		<th>FAKULTAS</th>
		<th>PRODI</th>
		<th>JENJANG</th>
        <th>NIM</th>
		<th>DITERIMA</th>
	</tr>
	{$no = 1}
	
	{foreach $diterima as $data}
	<tr>
		<td>{$no++}</td>
        <td>{$data.NO_UJIAN}</td>
        <td>{$data.KODE_VOUCHER}</td>
        <td>{$data.NM_C_MHS}</td>
        <td>{$data.NM_PENERIMAAN}</td>
		<td>{$data.NM_FAKULTAS}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
        <td>{$data.NM_JENJANG}</td>
		<td>{$data.NIM_MHS}</td>
        <td>{$data.TGL_DITERIMA}</td>
	</tr>
	{/foreach}

</table>

{elseif isset($finger)}

<table>
	<tr>
		<th>NO</th>
        <th>NO UJIAN</th>
        <th>NAMA</th>
        <th>JALUR</th>
		<th>FAKULTAS</th>
		<th>PRODI</th>
		<th>JENJANG</th>
        <th>NIM</th>
	</tr>
	{$no = 1}
	
	{foreach $finger as $data}
	<tr>
		<td>{$no++}</td>
        <td>{$data.NO_UJIAN}</td>
        <td>{$data.NM_C_MHS}</td>
        <td>{$data.NM_PENERIMAAN}</td>
		<td>{$data.NM_FAKULTAS}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
        <td>{$data.NM_JENJANG}</td>
		<td>{$data.NIM_MHS}</td>
	</tr>
	{/foreach}

</table>

{elseif isset($verifikasi)}

<table>
	<tr>
		<th>NO</th>
        <th>NO UJIAN</th>
        <th>NAMA</th>
        <th>JALUR</th>
		<th>FAKULTAS</th>
		<th>PRODI</th>
		<th>JENJANG</th>
        <th>NIM</th>
		<th>VERIFIKASI</th>
	</tr>
	{$no = 1}
	
	{foreach $verifikasi as $data}
	<tr>
		<td>{$no++}</td>
        <td>{$data.NO_UJIAN}</td>
        <td>{$data.NM_C_MHS}</td>
        <td>{$data.NM_PENERIMAAN}</td>
		<td>{$data.NM_FAKULTAS}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
        <td>{$data.NM_JENJANG}</td>
		<td>{$data.NIM_MHS}</td>
        <td>{$data.TGL_VERIFIKASI_PENDIDIKAN}</td>
	</tr>
	{/foreach}

</table>

{elseif isset($tidak_registrasi)}

<table>
	<tr>
		<th>NO</th>
        <th>NO UJIAN</th>
        <th>NAMA</th>
        <th>JALUR</th>
		<th>FAKULTAS</th>
		<th>PRODI</th>
		<th>JENJANG</th>
		<th>ALAMAT</th>
		<th>TELP</th>
		<th>BEASISWA</th>
	</tr>
	{$no = 1}
	
	{foreach $tidak_registrasi as $data}
	<tr>
		<td>{$no++}</td>
        <td>{$data.NO_UJIAN}</td>
        <td>{$data.NM_C_MHS}</td>
        <td>{$data.NM_PENERIMAAN}</td>
		<td>{$data.NM_FAKULTAS}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
        <td>{$data.NM_JENJANG}</td>
	<td>{$data.ALAMAT}</td>
	<td>{$data.TELP}</td>
	<td>{if $data.STATUS_BIDIK_MISI == 1}Bidik Misi{/if}</td>
	</tr>
	{/foreach}

</table>

{/if}
