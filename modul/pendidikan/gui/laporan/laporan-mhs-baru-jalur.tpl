<div class="center_title_bar">LAPORAN MAHASISWA BARU BERDASARKAN JALUR PENERIMAAN</div>
<form action="laporan-mhs-baru-jalur.php" method="post">
	<table>
		<tr>
			<th colspan="6">PARAMETER</th>
		</tr>
		<tr>
			<td>Angkatan Mhs</td>
			<td>
				<select name="angkatan">
					{foreach $tahun as $data}
					<option value="{$data.TAHUN}">{$data.TAHUN}</option>
					{/foreach}
				</select>
			</td>
			<td>
				Tampilan
			</td>
			<td>
				<select name="tampilan">
					<option value="fak" {if $smarty.post.tampilan == 'fak'} selected="selected"{/if}>Fakultas</option>
                    <option value="prodi" {if $smarty.post.tampilan == 'prodi'} selected="selected"{/if}>Prodi</option>
				</select>
			</td>
			<td><input type="submit" value="Tampil" /></td>
		</tr>
	</table>
</form>

{if isset($smarty.post.angkatan)}
<table>
	<tr>
		{if $smarty.post.tampilan == 'fak'}
		<th>Fakultas/ Jalur</th>
		{else}
		<th>Fakultas</th>
		<th>Prodi/ Jalur</th>
		{/if}
		{foreach $jalur_mhs as $data}
		<th>{$data.NAMA_JALUR}</th>
		{/foreach}
		<th>Jml Mhs</th>
	</tr>
	{$tampil}
	
</table>
{/if}