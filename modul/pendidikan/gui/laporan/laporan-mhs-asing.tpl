<div class="center_title_bar">LAPORAN MAHASISWA ASING</div>
<form action="laporan-mhs-asing.php" method="post">
	<table>
		<tr>
			<th colspan="8">PARAMETER</th>
		</tr>
		<tr>
			<td>
				Angkatan Mhs
			</td>
			<td>
				<select name="angkatan">
					<option value="">Semua</option>
					{foreach $angkatan as $data}
					<option value="{$data.THN_ANGKATAN_MHS}" {if $smarty.post.angkatan == $data.THN_ANGKATAN_MHS} selected="selected"{/if}>{$data.THN_ANGKATAN_MHS}</option>
					{/foreach}
				</select>
			</td>
			<td>
				ATAU
			</td>
			<td>
				Tahun Akademik
			</td>
			<td>
				<select name="thn_akademik">
					<option value="">Pilih Tahun Akademik</option>
					{foreach $thn_akademik as $data}
					<option value="{$data.THN_AKADEMIK_SEMESTER}" {if $smarty.post.thn_akademik == $data.THN_AKADEMIK_SEMESTER} selected="selected"{/if}>{$data.TAHUN_AJARAN}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
            <td>Fakultas</td>
            <td>
                <select id="fakultas" name="fakultas">
                    <option value="">Semua</option>
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $smarty.post.fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
			<td>
			</td>
            <td>Jenjang</td>
            <td>
                <select id="jenjang" name="jenjang">
                    <option value="">Semua</option>
                    {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.post.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
		<tr>	
			<td>Program Studi</td>
            <td colspan="4">
                <select id="program_studi" name="program_studi">
                    <option value="">Semua</option>
					{foreach $prodi as $data}
						<option value="{$data.ID_PROGRAM_STUDI}" {if $smarty.post.program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
					{/foreach}
                </select>
            </td>
        </tr>
		<tr>
			<td>Negara</td>
			<td colspan="2">
				<select id="negara" name="negara">
                    <option value="">Semua</option>
					{foreach $data_negara as $data}
                        <option value="{$data.ID_NEGARA}" {if $smarty.post.negara==$data.ID_NEGARA}selected="true"{/if}>{$data.NM_NEGARA|upper}</option>
                    {/foreach}
				</select>
			</td>
			<td>Jenis Kerjasama</td>
			<td >
				<select id="jenis_kerjasama" name="jenis_kerjasama">
                    <option value="">Semua</option>
					{foreach $jenis_kerjasama as $data}
                        <option value="{$data.ID_JENIS_KERJASAMA}" {if $smarty.post.jenis_kerjasama==$data.ID_JENIS_KERJASAMA}selected="true"{/if}>{$data.NM_JENIS_KERJASAMA|upper}</option>
                    {/foreach}
				</select>
			</td>
		</tr>
		<tr>		
            <td>
				Tampilan
			</td>
			<td>
				<select name="tampilan">
					<option value="fak" {if $smarty.post.tampilan == 'fak'} selected="selected"{/if}>Fakultas</option>
                    <option value="prodi" {if $smarty.post.tampilan == 'prodi'} selected="selected"{/if}>Prodi</option>
					<option value="negara" {if $smarty.post.tampilan == 'negara'} selected="selected"{/if}>Negara</option>
				</select>
			</td>
			<td></td>
			<td>
				Status Akademik
			</td>
			<td>
				<select name="status_akademik">
					<option value="">Semua</option>
					<option value="1" {if $smarty.post.status_akademik == 1} selected="selected"{/if}>Aktif</option>
					{foreach $status_tidak_aktif as $data}
					<option value="{$data.ID_STATUS_PENGGUNA}" {if $smarty.post.status_akademik == $data.ID_STATUS_PENGGUNA} selected="selected"{/if}>{$data.NM_STATUS_PENGGUNA}</option>
					{/foreach}
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="8" style="text-align:center">
				<input type="submit" value="Tampil" />
				<input type="hidden" value="show" name="mode" />
			</td>
		</tr>
	</table>
</form>

{if isset($smarty.get.tampilan)}
<table>
	<tr>
    	<th>No</th>
        <th>NIM</th>
        <th>Nama</th>
        <th>Fakultas</th>
        <th>Jenjang</th>
        <th>Prodi</th>
        <th>Negara</th>
        <th>Jenis Kerjasama</th>
    </tr>
    {$no = 1}
    {foreach $mhs_detail as $data}
    <tr>
    	<td>{$no++}</td>
        <td>{$data.NIM_MHS_ASING}</td>
        <td>{$data.NM_PENGGUNA}</td>
        <td>{$data.NM_FAKULTAS}</td>
        <td>{$data.NM_JENJANG}</td>
        <td>{$data.NM_PROGRAM_STUDI}</td>
        <td>{$data.NM_NEGARA}</td>
        <td>{$data.NM_JENIS_KERJASAMA}</td>
    </tr>
    {/foreach}
</table>
{else if isset($mhs_jenjang)}
	
	{if $smarty.post.thn_akademik == ''}
		Tampilan berdasarkan angkatan
	{else}
		Tampilan berdasarkan tahun akademik
	{/if}
	
	<table>
		<tr>
			<th>NO</th>
            {if $smarty.post.tampilan == 'fak'}
			<th>FAKUTLAS</th>
            {elseif $smarty.post.tampilan == 'negara'}
            <th>FAKUTLAS</th>
            <th>NEGARA</th>
			{else}
            <th>FAKUTLAS</th>
            <th>PRODI</th>
            {/if}
			<th>D3</th>
			<th>S1</th>
			<th>S2</th>
			<th>S3</th>
			<th>PROFESI</th>
			<th>SPESIALIS</th>
			<th>TOTAL</th>
		</tr>
		{$no = 1}
		{foreach $mhs_jenjang as $data}
		<tr>
			<td>{$no++}</td>
            {if $smarty.post.tampilan == 'fak'}
			<td>{$data.NM_FAKULTAS}</td>
            {elseif $smarty.post.tampilan == 'negara'}
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_NEGARA}</td>
			{else}
            <td>{$data.NM_FAKULTAS}</td>
            <td>{$data.NM_PROGRAM_STUDI}</td>
            {/if}
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$data.ID_FAKULTAS}&jenjang=5&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$data.D3}</a></td>
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$data.ID_FAKULTAS}&jenjang=1&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$data.S1}</a></td>
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$data.ID_FAKULTAS}&jenjang=2&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$data.S2}</a></td>
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$data.ID_FAKULTAS}&jenjang=3&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$data.S3}</a></td>
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$data.ID_FAKULTAS}&jenjang=9&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$data.PROFESI}</a></td>
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$data.ID_FAKULTAS}&jenjang=10&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$data.SPESIALIS}</a></td>
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$data.ID_FAKULTAS}&jenjang={$smarty.request.jenjang}&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$data.TOTAL}</a></td>
		</tr>
		{$total = $total + $data.TOTAL}
		{$totald3 = $totald3 + $data.D3}
		{$totals1 = $totals1 + $data.S1}
		{$totals2 = $totals2 + $data.S2}
		{$totals3 = $totals3 + $data.S3}
		{$totalprof = $totalprof + $data.PROFESI}
		{$totalspes = $totalspes + $data.SPESIALIS}
		{/foreach}
		<tr>
        	{if $smarty.post.tampilan == 'fak'}
			<td colspan="2">Total</td>
            {else}
            <td colspan="3">Total</td>
            {/if}
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$smarty.request.fakultas}&jenjang=3&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$totald3}</a></td>
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$smarty.request.fakultas}&jenjang=1&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$totals1}</a></td>
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$smarty.request.fakultas}&jenjang=2&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$totals2}</a></td>
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$smarty.request.fakultas}&jenjang=3&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$totals3}</a></td>
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$smarty.request.fakultas}&jenjang=9&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$totalprof}</a></td>
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$smarty.request.fakultas}&jenjang=10&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$totalspes}</a></td>
			<td><a href="laporan-mhs-asing.php?tampilan=detail&angkatan={$smarty.request.angkatan}&thn_akademik={$smarty.request.thn_akademik}&status_akademik={$smarty.request.status_akademik}&fakultas={$smarty.request.fakultas}&jenjang={$smarty.request.jenjang}&program_studi={$smarty.request.program_studi}&negara={$smarty.request.negara}&provinsi={$smarty.request.provinsi}&kota={$smarty.request.kota}&jenis_kerjasama={$smarty.request.jenis_kerjasama}">{$total}</a></td>
		</tr>
	</table>
	
{/if}

{literal}
    <script type="text/javascript">
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#negara').change(function(){
            $.ajax({
                type:'post',
                url:'getNegara.php',
                data:'id_negara='+$('#negara').val(),
                success:function(data){
                    $('#provinsi').html(data);
                }                    
            })
			
		 });
		 
		 $('#provinsi').change(function(){
				$.ajax({
					type:'post',
					url:'getKota.php',
					data:'id_provinsi='+$('#provinsi').val(),
					success:function(data){
						$('#kota').html(data);
					}                    
				})
		 });
    </script>
{/literal}