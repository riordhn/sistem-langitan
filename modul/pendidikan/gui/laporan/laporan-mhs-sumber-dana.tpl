<div class="center_title_bar">LAPORAN MAHASISWA BERDASARKAN SUMBER DANA</div>
<form action="laporan-mhs-sumber-dana.php" method="post">
	<table>
		<tr>
			<th colspan="5">PARAMETER</th>
		</tr>
		<tr>
			<td>
				Sumber Dana
			</td>
			<td>
				<select name="sumber_dana">
					<option value="">Semua</option>
					{foreach $sumber_dana as $data}
					<option value="{$data.ID_BEASISWA}" {if $smarty.post.sumber_dana == $data.ID_BEASISWA} selected="selected"{/if}>{$data.NM_BEASISWA}</option>
					{/foreach}
				</select>
			</td>
            <td>
				Tampilan
			</td>
			<td>
				<select name="tampilan">
					<option value="fak" {if $smarty.post.tampilan == 'fak'} selected="selected"{/if}>Fakultas</option>
                    <option value="prodi" {if $smarty.post.tampilan == 'prodi'} selected="selected"{/if}>Prodi</option>
				</select>
			</td>
			<td><input type="submit" value="Tampil" /></td>
		</tr>
	</table>
</form>
