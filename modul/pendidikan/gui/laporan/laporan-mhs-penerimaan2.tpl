<div class="center_title_bar">LAPORAN MAHASISWA BERDASARKAN PENERIMAAN MAHASISWA BARU</div>
<form action="laporan-mhs-penerimaan2.php" method="post">
	<table>
		<tr>
			<th colspan="8">PARAMETER</th>
		</tr>
		<tr>
			<td>Tahun Penerimaan</td>
            <td>
               <select name="thn" id="thn">
               	<option value="">Semua</option>
                    {foreach $data_thn as $data}
                        <option value="{$data.TAHUN}" {if $smarty.post.thn==$data.TAHUN}selected="true"{/if}>{$data.TAHUN|upper}</option>
                    {/foreach}
               </select>
            </td>
            <td>Jenjang</td>
            <td>
               <select name="jenjang" id="jenjang">
               	<option value="">Semua</option>
                    {foreach $jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $smarty.post.jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG|upper}</option>
                    {/foreach}
               </select>
            </td>
			<td>
				Tampilan
			</td>
			<td>
				<select name="tampilan">
					<option value="fak" {if $smarty.post.tampilan == 'fak'} selected="selected"{/if}>Fakultas</option>
                    <option value="prodi" {if $smarty.post.tampilan == 'prodi'} selected="selected"{/if}>Prodi</option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="8" style="text-align:center"><input type="submit" value="Tampil" /></td>
		</tr>
	</table>
</form>


{if isset($smarty.post.thn)}

<table>

  <tr>
  		{if $smarty.post.tampilan == 'fak'}
		<th rowspan="2">Fakultas/ Jalur</th>
		{else}
		<th rowspan="2">Fakultas</th>
		<th rowspan="2">Prodi/ Jalur</th>
		<th rowspan="2">Jenjang</th>
		{/if}
    
		{foreach $jalur_mhs as $data}
		<th colspan="3">{$data.NAMA_JALUR}</th>
		{/foreach}
  </tr>
  <tr>
  	{foreach $jalur_mhs as $data}
    <th>Terima</th>
    <th>DU</th>
	<th>TDU</th>
	{/foreach}
  </tr>

	{$tampil}
		
</table>

{/if}
