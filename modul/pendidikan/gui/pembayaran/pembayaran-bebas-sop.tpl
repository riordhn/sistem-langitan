<div class="center_title_bar">PERIODE PEMBEBASAN SOP</div>  
{if $mode == 'tambah'}
	<form name="f2" id="f2" action="pembayaran-bebas-sop.php" method="post">
	<table>
		<tr>
			<td>Periode Pembebasan</td>
			<td><input type="text" id="batas_bayar" name="batas_bayar"  value="{$batas_bayar}" class="required" /></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<input type="submit" value="Simpan" />
				<input type="hidden" value="insert" name="mode" />
			</td>
		</tr>
	</table>
	</form>
{elseif $mode == 'edit'}
	<form name="f2" id="f2" action="pembayaran-bebas-sop.php" method="post">
	<table>
		<tr>
			<td>Periode Pembebasan</td>
			<td><input type="text" id="batas_bayar" name="batas_bayar"  value="{$batas_bayar}" class="required" /></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<input type="submit" value="Simpan" />
				<input type="hidden" value="update" name="mode" />
				<input type="hidden" value="{$smarty.get.id}" name="id" />
			</td>
		</tr>
	</table>
	</form>
{else}
<table>
	<tr>
		<th>NO</th>
		<th>PERIODE BEBAS SOP</th>
		<th>AKSI</th>
	</tr>
	{$no=1}
	{foreach $periode as $data}
	<tr>
		<td>{$no++}</td>
		<td>{$data.TGL_PERIODE_BEBAS_SOP}</td>
		<td><a href="pembayaran-bebas-sop.php?mode=edit&id={$data.ID_PERIODE_BEBAS_SOP}">Edit</a> | 
			<a href="pembayaran-bebas-sop.php?mode=hapus&id={$data.ID_PERIODE_BEBAS_SOP}">Hapus</a></td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="3" style="text-align:center">
			<input type="button" value="Tambah" onclick="location.href='#pembayaran-periode_bebas_sop!pembayaran-bebas-sop.php?mode=tambah'" />
		</td>
	</tr>
</table>
{/if}

{literal}
    <script>
            $("#batas_bayar").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$('#f2').validate();
    </script>
{/literal}