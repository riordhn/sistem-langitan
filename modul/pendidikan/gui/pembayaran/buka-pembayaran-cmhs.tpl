<div class="center_title_bar">Buka Pembayaran Maba</div> 
<form method="post" action="buka-pembayaran-cmhs.php">
    <table>
        <tr>
            <td>No Ujian</td>
            <td><input type="text" name="no_ujian"/></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
    </table>
</form>
{if isset($cari_no_ujian)}
    <table class="detail">
        <tr>
            <th colspan="2" style="text-align: center;">Biodata</th>
        </tr>
        <tr>
            <td>No Ujian</td>
            <td>{$data_pembayaran[0]['NO_UJIAN']}</td>            
        </tr>
        <tr>
            <td>Nama</td>
            <td>{$data_pembayaran[0]['NM_C_MHS']}</td>            
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>{$data_pembayaran[0]['NM_PROGRAM_STUDI']}</td>            
        </tr>
        <tr>
            <td>Fakultas</td>
            <td>{$data_pembayaran[0]['NM_FAKULTAS']}</td>            
        </tr>
		
    </table>
    <p></p>
    <form action="buka-pembayaran-cmhs.php?mode=biaya_detail" method="post">
        <table>
            <tr>
                <th colspan="10" style="text-align: center;">Buka Biaya Calon Mahasiswa Detail</th>
            </tr>
            <tr>
                <th>Nama Biaya</th>
                <th>Besar Biaya</th>
                <th>Nama Bank</th>
                <th>Via Bank</th>
                <th>Tanggal Bayar</th>
                <th>No Transaksi</th>
				<th>Keterangan</th>
				<th>Tagih</th>
            </tr>
            {$index=1}
            {foreach $data_pembayaran as $data}
                {if $data['NM_BIAYA']}
                    <tr>
                        <td>
                            <input type="hidden" name="id{$index}" value="{$data['ID_PEMBAYARAN_CMHS']}"/>
                            {$data['NM_BIAYA']}
                        </td>
                        <td>
                            {$data['BESAR_BIAYA']}
                        </td>
                        <td>
                            {$data['NM_BANK']}
                        </td>
                        <td>
                            {$data['NAMA_BANK_VIA']}
                        </td>
                        <td>
                            {$data['TGL_BAYAR']}
                        </td>
                        <td>
                            {$data['NO_TRANSAKSI']}
                        </td>
						<td>
							{$data['KETERANGAN']}
						</td>
						<td>
                            <select name="is_tagih{$index++}">
                                <option value="Y" {if $data['IS_TAGIH']=='Y'}selected="true"{/if}>Buka</option>
                                <option value="T" {if $data['IS_TAGIH']=='T'}selected="true"{/if}>Tutup</option>
                            </select>
                        </td>
                    </tr>
                {else}
                    <tr>
                        <td colspan="7" style="text-align: center;">Data Pembayaran Kosong</td>
                    </tr>
                {/if}
            {/foreach}
            <tr>
                <td colspan="8">
                    <input type="hidden" name="no_ujian" value="{$data_pembayaran[0]['NO_UJIAN']}"/>
                    <input type="hidden" name="id_c_mhs" value="{$data_pembayaran[0]['ID_C_MHS']}"/>
                    <input type="hidden" name="jumlah" value="{$index}"/>
                    <input style="float: right;" type="submit" value="Update"/>
                </td>
            </tr>
        </table>
    </form>
	<form action="buka-pembayaran-cmhs.php?mode=biaya_all" method="post">
        <table>
            <tr>
                <th colspan="8">Buka Biaya Calon Mahasiswa All</th>
            </tr>
            <tr>
				<th>Nama Bank</th>
                <th>Via Bank</th>
                <th>Tanggal Bayar</th>
                <th>No Transaksi</th>
                <th>Keterangan</th>
                <th>Tagih</th>
				<th>Proses</th>
            </tr>
            <tr>
				
				<td>
                    {$data_pembayaran[0]['NM_BANK']}
                </td>
                <td>
                    {$data_pembayaran[0]['NAMA_BANK_VIA']}
                </td>
                <td>
                    {$data_pembayaran[0]['TGL_BAYAR']}
                </td>
                <td>
                    {$data_pembayaran[0]['NO_TRANSAKSI']}
                </td>
                <td>
                    {$data_pembayaran[0]['KETERANGAN']}
                </td>
				<td>
					<select name="is_tagih">
						<option value="Y" {if $data_pembayaran[0]['IS_TAGIH']=='Y'}selected="true"{/if}>Buka</option>
						<option value="T" {if $data_pembayaran[0]['IS_TAGIH']=='T'}selected="true"{/if}>Tutup</option>
					</select>
				</td>
                <td>
					<input type="hidden" name="no_ujian" value="{$data_pembayaran[0]['NO_UJIAN']}"/>                    
					<input type="hidden" name="id_c_mhs" value="{$data_pembayaran[0]['ID_C_MHS']}"/>
                    <input type="submit" value="Update"/>
                </td>
				
            </tr>
        </table>
{/if}
{literal}
    <script>
            $(function() {
                    $( ".date" ).datepicker({dateFormat:'dd-M-yy'});
            });
    </script>
{/literal}