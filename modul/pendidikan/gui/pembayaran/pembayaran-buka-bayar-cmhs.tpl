<div class="center_title_bar">BUKA BAYAR MAHASISWA BARU</div>  
<form name="f1" action="pembayaran-buka-bayar-cmhs.php" method="post">
	<table>
		<tr>
			<td>NO UJIAN</td>
			<td><input type="text" name="nim" /></td>
			<td><input type="submit" value="Tampil" /></td>
		</tr>
	</table>
</form>

{if isset($mhs)}

<form name="f2" id="f2" action="pembayaran-buka-bayar-cmhs.php" method="post">
	<table>
		<tr>
			<th colspan="2">BIODATA</th>
		</tr>
		<tr>
			<td>NO UJIAN</td>
			<td>{$mhs['NO_UJIAN']}</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>{$mhs['NM_C_MHS']}</td>
		</tr>
		<tr>
			<td>Fakultas</td>
			<td>{$mhs['NM_FAKULTAS']}</td>
		</tr>
		<tr>
			<td>Program Studi</td>
			<td>{$mhs['NM_JENJANG']} - {$mhs['NM_PROGRAM_STUDI']}</td>
		</tr>
		<tr>
			<td>Batas Pembayaran</td>
			<td><input type="text" id="batas_bayar" name="batas_bayar"  value="{$batas_bayar}" class="required" /></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<input type="submit" value="Simpan" />
				<input type="hidden" value="{$mhs['ID_C_MHS']}" name="id_mhs" />
			</td>
		</tr>
	</table>
</form>

{/if}


{literal}
    <script>
            $("#batas_bayar").datepicker({dateFormat:'dd-mm-yy',changeMonth: true, changeYear: true});
			$('#f2').validate();
    </script>
{/literal}