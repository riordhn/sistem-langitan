<div class="center_title_bar">REKAP BIAYA</div> 
<form action="pembayaran-rekap-biaya.php" method="post">
    <table style="width: 80%">
        <tr>
            <td>Fakultas</td>
            <td>
                <select id="fakultas" name="fakultas">
				<option value="">PILIH FAKULTAS</option> 
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $id_fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
			<td>Jenjang</td>
			<td>
				<select id="jenjang" name="jenjang">
                    <option value="">PILIH JENJANG</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $id_jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
		</tr>
		<tr>
			<td>Program Studi</td>
            <td>
                <select id="program_studi" name="program_studi">
                    <option value="">SEMUA PRODI</option>
                    {foreach $data_program_studi as $data}
                        {if $id_program_studi!=''}
                            <option value="{$data.ID_PROGRAM_STUDI}" {if $id_program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
                        {else if $fakultas!=''&&$id_program_studi==''}
                            {if $data.ID_FAKULTAS==$fakultas}
                                <option value="{$data.ID_PROGRAM_STUDI}" >{$data.NM_PROGRAM_STUDI}</option>
                            {/if}
                        {/if}
                    {/foreach}
                </select>
            </td>
		</tr>
		<tr>
			<td>Semester</td>
            <td>
                <select name="semester">
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $id_semester==$data.ID_SEMESTER}selected="selected"{/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center"><input type="submit" value="Tampil" /></td>
		</tr>
</table>
</form>

{if isset($biaya)}
<table>
	<tr>
		<th>NO</th>
		<th>FAKULTAS</th>
		<th>PROGRAM STUDI</th>
		<th>JENJANG</th>
		<th>JALUR</th>
		<th>KELOMPOK BIAYA</th>
		<th>SOP</th>
		<th>SP3</th>
		<th>MARTIKULASI</th>
		<th>TOTAL BIAYA</th>
	</tr>
	{$no = 1}
	{foreach $biaya as $data}
	<tr>
		<td>{$no++}</td>
		<td>{$data.NM_FAKULTAS}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
		<td>{$data.NM_JENJANG}</td>
		<td>{$data.NM_JALUR}</td>
		<td>{$data.NM_KELOMPOK_BIAYA}</td>
		<td style="text-align:right">{$data.SOP|number_format:0:",":"."}</td>
		<td style="text-align:right">{$data.SP3|number_format:0:",":"."}</td>
		<td style="text-align:right">{$data.MATRIKULASI|number_format:0:",":"."}</td>
		<td style="text-align:right">{$data.BESAR_BIAYA_KULIAH|number_format:0:",":"."}</td>
	</tr>
	{/foreach}
</table>

{/if}

{literal}
    <script type="text/javascript">
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });

    </script>
{/literal}