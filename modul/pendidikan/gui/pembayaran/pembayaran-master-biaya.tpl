<div class="center_title_bar">MASTER BIAYA</div> 
<form action="pembayaran-master-biaya.php" method="post">
    <table style="width: 80%">
        <tr>
            <td>Fakultas</td>
            <td>
                <select id="fakultas" name="fakultas">
				<option value="">PILIH FAKULTAS</option> 
                    {foreach $data_fakultas as $data}
                        <option value="{$data.ID_FAKULTAS}" {if $id_fakultas==$data.ID_FAKULTAS}selected="true"{/if}>{$data.NM_FAKULTAS|upper}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
			<td>Jenjang</td>
			<td>
				<select id="jenjang" name="jenjang">
                    <option value="">PILIH JENJANG</option> 
                   {foreach $data_jenjang as $data}
                        <option value="{$data.ID_JENJANG}" {if $id_jenjang==$data.ID_JENJANG}selected="true"{/if}>{$data.NM_JENJANG}</option>
                    {/foreach}                    
                </select>
			</td>
		</tr>
		<tr>
			<td>Program Studi</td>
            <td>
                <select id="program_studi" name="program_studi">
                    <option value="">SEMUA PRODI</option>
                    {foreach $data_program_studi as $data}
                        {if $id_program_studi!=''}
                            <option value="{$data.ID_PROGRAM_STUDI}" {if $id_program_studi==$data.ID_PROGRAM_STUDI}selected="true"{/if}>{$data.NM_PROGRAM_STUDI}</option>
                        {else if $fakultas!=''&&$id_program_studi==''}
                            {if $data.ID_FAKULTAS==$fakultas}
                                <option value="{$data.ID_PROGRAM_STUDI}" >{$data.NM_PROGRAM_STUDI}</option>
                            {/if}
                        {/if}
                    {/foreach}
                </select>
            </td>
		</tr>
		<tr>
			<td>Semester</td>
            <td>
                <select name="semester">
                	{foreach $semester as $data}
                	<option value="{$data.ID_SEMESTER}" {if $id_semester==$data.ID_SEMESTER}selected="selected"{/if}>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</option>
					{/foreach}
            	</select>
            </td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center"><input type="submit" value="Tampil" /></td>
		</tr>
</table>
</form>

{if isset($biaya)}
<table>
	<tr>
		<th>NO</th>
		<th>PROGRAM STUDI</th>
		<th>JENJANG</th>
		<th>JALUR</th>
		<th>SEMESTER</th>
		<th>KELOMPOK BIAYA</th>
		<th>TOTAL BIAYA PENDAFTARAN</th>
		<th>DETAIL</th>
	</tr>
	{$no = 1}
	{foreach $biaya as $data}
	<tr>
		<td>{$no++}</td>
		<td>{$data.NM_PROGRAM_STUDI}</td>
		<td>{$data.NM_JENJANG}</td>
		<td>{$data.NM_JALUR}</td>
		<td>{$data.NM_SEMESTER} ({$data.THN_AKADEMIK_SEMESTER})</td>
		<td>{$data.NM_KELOMPOK_BIAYA}</td>
		<td style="text-align:right">{$data.BESAR_BIAYA_KULIAH|number_format:0:",":"."}</td>
		<td><a href="pembayaran-master-biaya.php?id={$data.ID_BIAYA_KULIAH}">Detail</a></td>
	</tr>
	{/foreach}
</table>

{elseif isset($detail)}

<table>
	<tr>
		<th>NO</th>
		<th>NAMA BIAYA</th>
		<th>BESAR BIAYA</th>
		<th>AKTIF / PENDAFTARAN</th>
	</tr>
	{$no = 1}
	{$biaya_pendaftaran = 0}
	{$biaya_aktif = 0}
	{$biaya_total = 0}
	{foreach $detail as $data}
	<tr>
		<td>{$no++}</td>
		<td>{$data.NM_BIAYA}</td>
		<td style="text-align:right">{$data.BESAR_BIAYA|number_format:0:",":"."}</td>
		<td style="text-align:center">{if $data.PENDAFTARAN_BIAYA == 1}Pendaftaran{else}Aktif{/if}</td>
	</tr>
	{if $data.PENDAFTARAN_BIAYA == 1}
		{$biaya_pendaftaran = $biaya_pendaftaran + $data.BESAR_BIAYA}
	{else}
		{$biaya_aktif = $biaya_aktif + $data.BESAR_BIAYA}
	{/if}

	{$biaya_total = $biaya_total + $data.BESAR_BIAYA}
	{/foreach}
</table>

<table>
	<tr>
		<th colspan="3" style="text-align:center">TOTAL BIAYA</th>
	</tr>
	<tr>
		<td>TOTAL BIAYA PENDAFTARAN</td>
		<td style="text-align:right">{$biaya_pendaftaran|number_format:0:",":"."}</td>
	</tr>
	<tr>
		<td>TOTAL BIAYA AKTIF</td>
		<td style="text-align:right">{$biaya_aktif|number_format:0:",":"."}</td>
	</tr>
	<tr>
		<td>TOTAL BIAYA</td>
		<td style="text-align:right">{$biaya_total|number_format:0:",":"."}</td>
	</tr>
</table>
{/if}

{literal}
    <script type="text/javascript">
        $('#fakultas').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });
		
		$('#jenjang').change(function(){
            $.ajax({
                type:'post',
                url:'getProdi.php',
                data:'id_fakultas='+$('#fakultas').val()+'&id_jenjang='+$('#jenjang').val(),
                success:function(data){
                    $('#program_studi').html(data);
                }                    
            })
        });

    </script>
{/literal}