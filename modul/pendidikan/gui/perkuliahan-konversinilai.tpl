<script language="JavaScript"type="text/javascript">

</script>
<div class="center_title_bar">Konversi Nilai IPK pada Predikat Kelulusan </div>
<br/>
<div id="tabs">
<div id="tab1" class="tab_sel" onclick="javascript: displayPanel('1');" align="center">Rincian</div>
<div id="tab2" class="tab" style="margin-left:1px;" onclick="javascript: displayPanel('2');" align="center">Input</div>
</div>
<div class="tab_bdr"></div>
<div class="panel" id="panel1" style="display: block;">    
<table border="0" cellpadding="0" cellspacing="0" width="50%">
   <tbody><tr class="left_menu">
     <td bgcolor="#333333" width="24%"><font color="#FFFFFF">Jenjang</font></td>
     <td bgcolor="#333333" width="32%"><font color="#FFFFFF">Rentang Nilai IPK </font></td>
     <td bgcolor="#333333" width="44%"><font color="#FFFFFF">Predikat Kelulusan </font> </td>
   </tr>
   <tr>
     <td>D3</td>
     <td>2,00 - 2,75</td>
     <td>Memuaskan</td>
   </tr>
   <tr>
     <td>D3</td>
     <td>2,76 - 3,50</td>
     <td>Sangat Memuaskan </td>
   </tr>
   <tr>
     <td>D3</td>
     <td>3,51 - 4,00</td>
     <td>Dengan Pujian</td>
   </tr>
   <tr>
     <td>S1</td>
     <td>2,00 - 2,75</td>
     <td>Memuaskan</td>
   </tr>
   <tr>
     <td>S1</td>
     <td>2,76 - 3,50</td>
     <td>Sangat Memuaskan </td>
   </tr>
   <tr>
     <td>S1</td>
     <td>3,51 - 4,00</td>
     <td>Dengan Pujian</td>
   </tr>
   <tr>
     <td>S2</td>
     <td>2,75 - 3,40</td>
     <td>Memuaskan</td>
   </tr>
   <tr>
     <td>S2</td>
     <td>3,41 - 3,70</td>
     <td>Sangat Memuaskan </td>
   </tr>
   <tr>
     <td>S2</td>
     <td>3,71 - 4,00</td>
     <td>Dengan Pujian</td>
   </tr>
   <tr>
     <td>SP1</td>
     <td>2,75 - 3,40</td>
     <td>Memuaskan</td>
   </tr>
   <tr>
     <td>SP1</td>
     <td>3,41 - 3,70</td>
     <td>Sangat Memuaskan </td>
   </tr>
   <tr>
     <td>SP1</td>
     <td>3,71 - 4,00</td>
     <td>Dengan Pujian</td>
   </tr>
   <tr>
     <td>S3</td>
     <td>3,00 - 3,40</td>
     <td>Memuaskan</td>
   </tr>
   <tr>
     <td>S3</td>
     <td>3,41 - 3,74</td>
     <td>Sangat Memuaskan </td>
   </tr>
   <tr>
     <td>S3</td>
     <td>3,75- 4,00</td>
     <td>Dengan Pujian</td>
   </tr>
</tbody></table>
</div>

<div class="panel" id="panel2" style="display: none;">
  <table class="tb_frame" border="0" cellpadding="0" cellspacing="0" width="46%">
    <tbody><tr>
      <td>Jenjang</td>
      <td>:</td>
      <td><label for="select"></label>
        <select name="select" id="select">
          <option>D3</option>
          <option>S1</option>
          <option>S2</option>
          <option>S3</option>
          <option>SP1</option>
      </select></td>
    </tr>
    <tr>
      <td width="213">Rentang Nilai IPK </td>
      <td width="10">:</td>
      <td width="274"><input name="textfield" size="5" maxlength="5" type="text">
      - 
        <input name="textfield2" size="5" maxlength="5" type="text"></td>
    </tr>
    <tr>
      <td>Predikat Kelulusan </td>
      <td>:</td>
      <td><label for="select2">
        <select name="select2" id="select2">
          <option>Memuaskan</option>
          <option>Sangat Memuaskan</option>
          <option>Dengan Pujian</option>
        </select>
      </label></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input name="Submit" value="Simpan" type="submit"></td>
    </tr>
  </tbody></table>
</div>