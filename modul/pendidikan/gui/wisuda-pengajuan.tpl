<div class="center_title_bar">Pengajuan Yudisium</div>  
<form id="form1" name="form1" method="post" action="wisuda-pengajuan.php">
<table>
<tr>
	<td>NIM</td>
	<td> : </td>
	<td><input type="text" name="nim" /></td>
	<td><input type="submit" value="Tampil" /></td>
</tr>
</table>
</form>

<h2><font color="#FF0000">{$pesan}</font></h2>
{if isset($pengajuan_wisuda)}
<table width="500">
  <tr>
    <th colspan="2" style="text-align:center">Biodata</th>
  </tr>
  <tr>
    <td>NIM</td>
    <td>{$pengajuan_wisuda[0]['NIM_MHS']}</td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>{$pengajuan_wisuda[0]['NM_PENGGUNA']}</td>
  </tr>
  <tr>
    <td>Program Studi</td>
    <td>{$pengajuan_wisuda[0]['NM_JENJANG']} - {$pengajuan_wisuda[0]['NM_PROGRAM_STUDI']}</td>
  </tr>
  <tr>
    <td>Periode</td>
    <td>{if $pengajuan_wisuda[0]['NM_TARIF_WISUDA'] == ''}{$periode['NM_TARIF_WISUDA']}{else}{$pengajuan_wisuda[0]['NM_TARIF_WISUDA']}{/if}</td>
  </tr>
<!--  <tr>
    <td>Mata Ajar</td>
    <td>{$pengajuan_wisuda[0]['NM_MATA_KULIAH']}</td>
  </tr>
  <tr>
    <td>Nilai</td>
    <td>{$pengajuan_wisuda[0]['NILAI_HURUF']}</td>
  </tr>-->
  <tr>
  	<td>Status Pengajuan</td>
    <td>{if $pengajuan_wisuda[0]['YUDISIUM'] == ''}BELUM{else}SUDAH{/if}</td>
  </tr>
  <tr>
  	<td colspan="2" style="text-align:center">
    	<form method="post" action="wisuda-pengajuan.php">
        	<input type="hidden" value="{$pengajuan_wisuda[0]['NIM_MHS']}" name="nim" />
        	<input type="hidden" value="{$pengajuan_wisuda[0]['ID_MHS']}" name="id_mhs" />
        	{if $pengajuan_wisuda[0]['YUDISIUM'] == '2'}
            	<input type="hidden" value="hapus" name="mode" />
            	<input type="submit" value="Hapus" />
             {elseif $pengajuan_wisuda[0]['YUDISIUM'] == '1' and $pengajuan_wisuda[0]['ID_JENJANG'] != 9}
             	Sudah Bayar Wisuda
             {else}
             	<input type="submit" value="Pengajuan" />
            {/if}
        </form>
    </td>
  </tr>
</table>
{elseif isset($cek)}
 <table width="500">
  <tr>
    <th colspan="2" style="text-align:center">Biodata</th>
  </tr>
  <tr>
    <td>NIM</td>
    <td>{$cek[0]['NIM_MHS']}</td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>{$cek[0]['NM_PENGGUNA']}</td>
  </tr>
  <tr>
    <td>Program Studi</td>
    <td>{$cek[0]['NM_JENJANG']} - {$cek[0]['NM_PROGRAM_STUDI']}</td>
  </tr>
  <tr>
  	<td>Status Akademik</td>
    <td>{$cek[0]['NM_STATUS_PENGGUNA']}</td>
  </tr>
  </table>
{/if}

{if isset($pembayaran)}
<table>
	<tr>
    	<th colspan="5" style="text-align:center">Pembayaran</th>
  	</tr>
    <tr>
    	<th>Tahun Ajaran</th>
        <th>Tanggal Bayar</th>
        <th>Besar Biaya</th>
        <th>Status Pembayaran</th>
        <th>Keterangan</th>
    </tr>
    {foreach $pembayaran as $data}
    
    <tr {if $data.ID_STATUS_PEMBAYARAN == 2} bgcolor="#CCFF33" {/if}>
    	<td>{$data.TAHUN_AJARAN} ({$data.NM_SEMESTER})</td>
        <td>{$data.TGL_BAYAR}</td>
        <td style="text-align:right">{$data.BESAR_BIAYA|number_format:0:",":"."}</td>
        <td>{$data.NAMA_STATUS}</td>
        <td>{$data.KETERANGAN}</td>
    </tr>
    
    {/foreach}
</table>
{/if}
