<?php
class PERATURAN extends Entity
{
    public $ID_PERATURAN;
    public $NM_PERATURAN;
    public $JENIS_PERATURAN;
    public $DESKRIPSI_PERATURAN;

    public $PERATURAN_NILAIs;

    function __construct(&$row = null)
    {
        if ($row != null)
        {
            $this->ID_PERATURAN = $row["ID_PERATURAN"];
            $this->NM_PERATURAN = $row["NM_PERATURAN"];
            $this->JENIS_PERATURAN = $row["JENIS_PERATURAN"];
            $this->DESKRIPSI_PERATURAN = $row["DESKRIPSI_PERATURAN"];
        }
    }
}

class PERATURAN_SET extends Entities
{
    function Assign(PERATURAN &$peraturan)
    {
        parent::Assign($peraturan);
    }
}

class PERATURAN_TABLE extends OracleTable
{
    function Count()
    {
        $this->db->Query("SELECT COUNT(*) FROM PERATURAN");
        $row = $this->db->FetchRow();
        return $row[0];
    }

    function CountWhere($where)
    {
        $this->db->Query("SELECT COUNT(*) FROM PERATURAN WHERE {$where}");
        $row = $this->db->FetchRow();
        return $row[0];
    }

    function Select()
    {
        $peraturan_set = new PERATURAN_SET();
        $this->db->Query("SELECT * FROM PERATURAN");
        while ($row = $this->db->FetchAssoc())
            $peraturan_set->Assign(new PERATURAN($row));
        return $peraturan_set;
    }

    function SelectWhere($where)
    {
        $peraturan_set = new PERATURAN_SET();
        $this->db->Query("SELECT * FROM PERATURAN WHERE {$where}");
        while ($row = $this->db->FetchAssoc())
            $peraturan_set->Assign(new PERATURAN($row));
        return $peraturan_set;
    }

    function SelectPage($page, $row_per_page)
    {
        $start_row = (($page - 1) * $row_per_page) + 1;
        $end_row = $page * $row_per_page;

        $peraturan_set = new PERATURAN_SET();
        $this->db->Query("SELECT * FROM (SELECT A.*, ROWNUM RNUM FROM (SELECT * FROM PERATURAN) A WHERE ROWNUM <= {$end_row}) WHERE RNUM >= {$start_row}");
        while ($row = $this->db->FetchAssoc())
            $peraturan_set->Assign(new PERATURAN($row));
        return $peraturan_set;
    }

    function SelectPageWhere($where, $page, $row_per_page)
    {
        $start_row = (($page - 1) * $row_per_page) + 1;
        $end_row = $page * $row_per_page;

        $peraturan_set = new PERATURAN_SET();
        $this->db->Query("SELECT * FROM (SELECT A.*, ROWNUM RNUM FROM (SELECT * FROM PERATURAN WHERE {$where}) A WHERE ROWNUM <= {$end_row}) WHERE RNUM >= {$start_row}");
        while ($row = $this->db->FetchAssoc())
            $peraturan_set->Assign(new PERATURAN($row));
        return $peraturan_set;
    }

    function Single($id_peraturan)
    {
        $this->db->Query("SELECT * FROM PERATURAN WHERE ID_PERATURAN = {$id_peraturan}");
        return new PERATURAN($this->db->FetchAssoc());
    }

    function Insert(PERATURAN &$peraturan)
    {
        $this->db->Parse("INSERT INTO PERATURAN
            ( ID_PERATURAN, NM_PERATURAN, JENIS_PERATURAN, DESKRIPSI_PERATURAN) VALUES
            (:ID_PERATURAN,:NM_PERATURAN,:JENIS_PERATURAN,:DESKRIPSI_PERATURAN)");
        $this->db->BindByName(":ID_PERATURAN", $peraturan->ID_PERATURAN);
        $this->db->BindByName(":NM_PERATURAN", $peraturan->NM_PERATURAN);
        $this->db->BindByName(":JENIS_PERATURAN", $peraturan->JENIS_PERATURAN);
        $this->db->BindByName(":DESKRIPSI_PERATURAN", $peraturan->DESKRIPSI_PERATURAN);
        $this->db->Execute();

        $this->db->Query("SELECT PERATURAN_SEQ.CURRVAL FROM dual");
        $row = $this->db->FetchAssoc();
        $peraturan->ID_PERATURAN = $row["CURRVAL"];

        $peraturan->ForInsert = false;
    }

    function Update(PERATURAN &$peraturan)
    {
        $this->db->Parse("UPDATE PERATURAN SET
            NM_PERATURAN = :NM_PERATURAN,
            JENIS_PERATURAN = :JENIS_PERATURAN,
            DESKRIPSI_PERATURAN = :DESKRIPSI_PERATURAN
            WHERE ID_PERATURAN = :ID_PERATURAN");
        $this->db->BindByName(":ID_PERATURAN", $peraturan->ID_PERATURAN);
        $this->db->BindByName(":NM_PERATURAN", $peraturan->NM_PERATURAN);
        $this->db->BindByName(":JENIS_PERATURAN", $peraturan->JENIS_PERATURAN);
        $this->db->BindByName(":DESKRIPSI_PERATURAN", $peraturan->DESKRIPSI_PERATURAN);
        $this->db->Execute();

        $peraturan->ForUpdate = false;
    }

    function Delete(PERATURAN &$peraturan)
    {
        $this->db->Query("DELETE FROM PERATURAN WHERE ID_PERATURAN = {$peraturan->ID_PERATURAN}");
    }

    function FillPeraturanNilai(PERATURAN_NILAI &$peraturan_nilai)
    {
        $this->db->Query("SELECT * FROM PERATURAN WHERE ID_PERATURAN = {$peraturan_nilai->ID_PERATURAN}");
        $peraturan_nilai->PERATURAN = new PERATURAN($this->db->FetchAssoc());
    }

    function FillPeraturanNilais(PERATURAN_NILAI_SET &$peraturan_nilai_set)
    {
        for ($i = 0; $i < $peraturan_nilai_set->Count(); $i++)
        {
            $id_peraturan = $peraturan_nilai_set->Get($i)->ID_PERATURAN;
            $this->db->Query("SELECT * FROM PERATURAN WHERE ID_PERATURAN = {$id_peraturan}");
            while ($row = $this->db->FetchAssoc())
                $peraturan_nilai_set->Get($i)->PERATURAN = new PERATURAN($row);
        }
    }
}
?>