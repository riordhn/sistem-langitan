<?php
include '../../../includes/fpdf/fpdf.php';

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0, 6, "SURAT - PERNYATAAN", '0', false, 'C', false);
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();

        // ayah
        $pdf->SetFont('Arial', 'B', 11); $pdf->SetTextColor(255,255,255);
        $pdf->Cell(0, 6, "Data Penghasilan Ayah", true, true, 'C',true);
        $pdf->SetFont('Arial', '', 11); $pdf->SetTextColor(0,0,0);
        $pdf->Cell(70, 6, "Gaji", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
        $pdf->Cell(70, 6, "Tunjangan Keluarga", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
        $pdf->Cell(70, 6, "Tunjangan Jabatan / Golongan", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
		$pdf->Cell(70, 6, "Tunjangan Sertifikasi Guru / Dosen", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
		$pdf->Cell(70, 6, "Tunjangan Kehormatan", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
		$pdf->Cell(70, 6, "Remunerasi", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
		$pdf->Cell(70, 6, "Tunjangan lain-lain", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
		$pdf->Cell(70, 6, "Penghasilan lain-lain", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
		
		// Ibu
        $pdf->SetFont('Arial', 'B', 11); $pdf->SetTextColor(255,255,255);
        $pdf->Cell(0, 6, "Data Penghasilan Ibu", true, true, 'C',true);
        $pdf->SetFont('Arial', '', 11); $pdf->SetTextColor(0,0,0);
        $pdf->Cell(70, 6, "Gaji", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
        $pdf->Cell(70, 6, "Tunjangan Keluarga", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
        $pdf->Cell(70, 6, "Tunjangan Jabatan / Golongan", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
		$pdf->Cell(70, 6, "Tunjangan Sertifikasi Guru / Dosen", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
		$pdf->Cell(70, 6, "Tunjangan Kehormatan", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
		$pdf->Cell(70, 6, "Remunerasi", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
		$pdf->Cell(70, 6, "Tunjangan lain-lain", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');
		$pdf->Cell(70, 6, "Penghasilan lain-lain", true, false, 'L');
        $pdf->Cell(0, 6, "", true, true, 'L');

$pdf->Ln();
$pdf->Ln();
$pdf->MultiCell(190, 6, "Dengan ini saya menyatakan bahwa data yang sudah diisi adalah sebenar-benarnya dan jika ada pemalsuan dalam pengisian data maka saya siap kehilangan hak sebagai mahasiswa Universitas Airlangga");

$pdf->Ln();
$pdf->Ln();	

		// kotak foto
        $pdf->Rect(90, 165, 30, 40);
        $pdf->SetXY(90, 180);
        $pdf->SetFont('Arial', '', 11);
        $pdf->Cell(30, 4, "4 x 6", false, false, 'C');
		
		$pdf->Cell(73, 5, "Surabaya, .............................", false, false, 'R');
		$pdf->Ln();
		$pdf->Cell(157, 5, "Hormat Saya,", false, false, 'R');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont('Arial', '', 9); $pdf->SetTextColor(0,0,0);
		$pdf->Cell(145, 5, "Materai", false, false, 'R');
		$pdf->Ln();
		$pdf->Cell(150, 5, "Rp 6.000,-,", false, false, 'R');
		
$pdf->AddPage();
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0, 6, "SURAT - PERNYATAAN", '0', false, 'C', false);
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();

		$pdf->SetFont('Arial', '', 11); $pdf->SetTextColor(0,0,0);
        $pdf->Cell(0, 8, "Yang bertanda tangan dibawah ini, Saya :", false, false, 'L',false);
		$pdf->Ln();
		$pdf->Cell(60, 8, "Nama", false, false, 'L');
        $pdf->Cell(0, 8, ":", false, true, 'L');
		$pdf->Cell(60, 8, "Tempat dan tanggal lahir", false, false, 'L');
        $pdf->Cell(0, 8, ":", false, true, 'L');
		$pdf->Cell(60, 8, "Alamat Surabaya", false, false, 'L');
        $pdf->Cell(0, 8, ": ................................................................................", false, true, 'L');
		$pdf->Cell(60, 8, "", false, false, 'L');
		$pdf->Cell(0, 8, " Telp.  .............................................", false, true, 'L');
		$pdf->Cell(60, 8, "Alamat Asal", false, false, 'L');
        $pdf->Cell(0, 8, ":", false, true, 'L');
		$pdf->Cell(60, 8, "Pekerjaan", false, false, 'L');
        $pdf->Cell(0, 8, ":", false, true, 'L');
		$pdf->Cell(60, 8, "Alamat Rumah", false, false, 'L');
        $pdf->Cell(0, 8, ": ................................................................................", false, true, 'L');
		$pdf->Cell(60, 8, "", false, false, 'L');
		$pdf->Cell(0, 8, " Telp.  .............................................", false, true, 'L');
		$pdf->Cell(60, 15, "Dengan ini menyatakan bahwa :", false, false, 'L');
		$pdf->Ln();
		$pdf->MultiCell(190, 6, "Saya tidak pernah, sedang atau akan terlibat dalam penyalahgunaan Narkotika, Alkohol, Psikotropikadan Zat Adiktif (NAPZA) baik sebagai pengguna, pengedar, produsen atau yang berkaitan dengan hal tersebut. Apabila ternyata di kemudian hari pada saat saya menuntut ilmu di Universitas Airlangga saya terlibat dan atau terbukti terlibat dalam penyalahgunaan NAPZA sebagaimana dimaksud di atas, maka saya sanggup dan bersedia dikenakan sanksi sampai dengan dibatalkan status saya sebagai mahasiswa Universitas Airlangga.");
		$pdf->Ln();
		$pdf->MultiCell(190, 8, "Demikian surat pernyataan ini saya buat dengan sebenarnya, tanpa adanya paksaan dari pihak siapapun.");
		
		// kotak foto
        $pdf->Rect(90, 185, 30, 40);
        $pdf->SetXY(90, 200);
        $pdf->SetFont('Arial', '', 11);
        $pdf->Cell(30, 4, "4 x 6", false, false, 'C');
		
		$pdf->Cell(74, 5, "Surabaya, .............................", false, false, 'R');
		$pdf->Ln();
		$pdf->Cell(158, 5, "Hormat Saya,", false, false, 'R');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont('Arial', '', 9); $pdf->SetTextColor(0,0,0);
		$pdf->Cell(145, 5, "Materai", false, false, 'R');
		$pdf->Ln();
		$pdf->Cell(150, 5, "Rp 6.000,-,", false, false, 'R');

$pdf->AddPage();
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0, 6, "SURAT PERNYATAAN BERSEDIA MEMENUHI DAN MENTAATI KETENTUAN", '0', false, 'C', false);
$pdf->Ln();
$pdf->Cell(0, 6, "DAN PERATURAN SERTA KEPUTUSAN YANG BERLAKU", '0', false, 'C', false);
$pdf->Ln();
$pdf->Cell(0, 6, "DI UNIVERSITAS AIRLANGGA", '0', false, 'C', false);
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();

		$pdf->SetFont('Arial', '', 11); $pdf->SetTextColor(0,0,0);
        $pdf->Cell(0, 8, "Yang bertanda tangan dibawah ini, Saya :", false, false, 'L',false);
		$pdf->Ln();
		$pdf->Cell(60, 8, "Nama", false, false, 'L');
        $pdf->Cell(0, 8, ":", false, true, 'L');
		$pdf->Cell(60, 8, "Tempat dan tanggal lahir", false, false, 'L');
        $pdf->Cell(0, 8, ":", false, true, 'L');
		$pdf->Cell(60, 8, "Alamat Surabaya", false, false, 'L');
        $pdf->Cell(0, 8, ": ................................................................................", false, true, 'L');
		$pdf->Cell(60, 8, "", false, false, 'L');
		$pdf->Cell(0, 8, " Telp.  .............................................", false, true, 'L');
		$pdf->Cell(60, 8, "Alamat Asal", false, false, 'L');
        $pdf->Cell(0, 8, ":", false, true, 'L');
		$pdf->Cell(60, 8, "Pekerjaan", false, false, 'L');
        $pdf->Cell(0, 8, ":", false, true, 'L');
		$pdf->Cell(60, 8, "Alamat Rumah", false, false, 'L');
        $pdf->Cell(0, 8, ": ................................................................................", false, true, 'L');
		$pdf->Cell(60, 8, "", false, false, 'L');
		$pdf->Cell(0, 8, " Telp.  .............................................", false, true, 'L');
		$pdf->Ln();
		$pdf->SetFont('Arial', 'B', 11); $pdf->SetTextColor(0,0,0);
		$pdf->MultiCell(190, 6, "Dengan ini menyatakan bahwa sebagai mahasiswa Universitas Airlangga, saya bersedia memenuhi dan mentaati ketentuan dan peraturan serta keputusan yang berlaku di Universitas Airlangga dan apabila saya melanggar saya bersedia dikenakan sangsi.");
		$pdf->Ln();
		$pdf->MultiCell(190, 6, "Demikian surat pernyataan ini saya buat dengan sebenarnya, tanpa adanya paksaan dari pihak siapapun.");
		
		// kotak foto
        $pdf->Rect(90, 165, 30, 40);
        $pdf->SetXY(90, 180);
        $pdf->SetFont('Arial', '', 11);
        $pdf->Cell(30, 4, "4 x 6", false, false, 'C');
		
		$pdf->Cell(73, 5, "Surabaya, .............................", false, false, 'R');
		$pdf->Ln();
		$pdf->Cell(157, 5, "Hormat Saya,", false, false, 'R');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont('Arial', '', 9); $pdf->SetTextColor(0,0,0);
		$pdf->Cell(145, 5, "Materai", false, false, 'R');
		$pdf->Ln();
		$pdf->Cell(150, 5, "Rp 6.000,-,", false, false, 'R');
			
$pdf->SetDisplayMode('real', 'continuous');
$pdf->Output();
?>