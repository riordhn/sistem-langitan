<?php
include('config.php');
include('../ppmb/class/Penerimaan.class.php');

$penerimaan = new Penerimaan($db);
$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());


if(isset($_POST['id_penerimaan'])){

	$view_kesehatan = $db->QueryToArray("SELECT TO_CHAR(TGL_TEST, 'DD-MM-YYYY') AS TGL_TEST, 
										(CASE WHEN MENIT_MULAI = '0' THEN MENIT_MULAI || '0' ELSE TO_CHAR(MENIT_MULAI) END) AS MENIT_MULAI,  
										(CASE WHEN length(JAM_MULAI) = 1 THEN '0' || JAM_MULAI ELSE TO_CHAR(JAM_MULAI) END) AS JAM_MULAI, 
										NM_RUANG, KAPASITAS, TERISI
										FROM JADWAL_TOEFL
										LEFT JOIN RUANG_TOEFL ON RUANG_TOEFL.ID_RUANG_TOEFL = JADWAL_TOEFL.ID_RUANG_TOEFL
										 WHERE ID_PENERIMAAN = '$_POST[id_penerimaan]' ORDER BY TGL_TEST DESC,
										JAM_MULAI, MENIT_MULAI asc");
	$smarty->assign('view_kesehatan', $view_kesehatan);

}


$smarty->display("pendaftaran/jadwal_elpt/pendaftaran-jadwal-elpt.tpl");
?>
