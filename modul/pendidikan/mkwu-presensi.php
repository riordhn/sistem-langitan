<?php
include('config.php');
include 'class/mkwu.class.php';

$mkwu = new mkwu($db);
$semester_aktif =  $mkwu->semester_aktif();

$username= $user->USERNAME;
$smarty->assign('semester_aktif', $semester_aktif);

$semester_request = isset($_REQUEST['smt'])?  $_REQUEST['smt'] : $semester_aktif['ID_SEMESTER'];
$mode = isset($_REQUEST['mode'])?  $_REQUEST['mode'] : 'view';

if($mode == 'tampil'){
	$smarty->assign('view_presensi', $mkwu->presensi_view($semester_request, $_REQUEST['mata_kuliah'], $_REQUEST['hari']));
}



if($mode == 'materi'){
	
	$id_kurikulum = $mkwu->kurikulum_mata_kuliah_mkwu($_REQUEST['mata_kuliah']);
	
	if($_REQUEST['action'] == 'entry'){	
		
		$mkwu->mkwu_presensi_add_materi($id_kurikulum['ID_KURIKULUM_MK'], $_REQUEST['materi'], $_REQUEST['calawal'], $semester_request, $_REQUEST['kelas']);
		$mkwu->mkwu_presensi_add_presensi_kelas($id_kurikulum['ID_KURIKULUM_MK'], $_REQUEST['materi'], $_REQUEST['calawal'], $semester_request, $_REQUEST['jam_mulai'], $_REQUEST['jam_selesai'], $_REQUEST['kelas']);
	
	
		for($i=1; $i<=$_REQUEST['counter1']; $i++){
				
				$materi_mk = $db->QueryToArray("select ID_MATERI_MK from materi_mk 
								join kelas_mk on kelas_mk.id_kelas_mk = materi_mk.id_kelas_mk
								where kelas_mk.id_kurikulum_mk = '".$id_kurikulum['ID_KURIKULUM_MK']."' and kelas_mk.id_semester = $semester_request 
								 and no_kelas_mk = '".$_REQUEST['kelas']."'
								and isi_materi_mk='".$_REQUEST['materi']."' and tgl_materi_mk = to_date('".$_REQUEST['calawal']."', 'DD-MM-YYYY')");
			
				foreach($materi_mk as $data){
						$db->Query("
								select id_presensi_kelas from presensi_kelas 
								join kelas_mk on kelas_mk.id_kelas_mk = presensi_kelas.id_kelas_mk
								where kelas_mk.id_kurikulum_mk = '".$id_kurikulum['ID_KURIKULUM_MK']."' and kelas_mk.id_semester = $semester_request 
								and id_materi_mk='$data[ID_MATERI_MK]' and no_kelas_mk = '".$_REQUEST['kelas']."'
							");			
							
						$id_presensi_kelas = $db->FetchAssoc();
						
						$db->Query("insert into presensi_mkdos (id_presensi_kelas,id_dosen,kehadiran)
							  values ('$id_presensi_kelas[ID_PRESENSI_KELAS]','".$_REQUEST['dosen'.$i]."',1)");
				}
				
				
		}
	
		
	}
	
	
	if($_REQUEST['action'] == 'update'){
		
		$mkwu->mkwu_presensi_update_materi($id_kurikulum['ID_KURIKULUM_MK'], $_POST['materi'], $_POST['calawal'], $semester_request, $_REQUEST['kelas']);
		$mkwu->mkwu_presensi_update_presensi_kelas($id_kurikulum['ID_KURIKULUM_MK'], $_POST['materi'], $_POST['calawal'], $semester_request, $_POST['jam_mulai'], $_POST['jam_selesai'], $_REQUEST['kelas']);
		
		for($i=1; $i<=$_REQUEST['counter1']; $i++){
				
				$materi_mk = $db->QueryToArray("select ID_MATERI_MK from materi_mk 
								join kelas_mk on kelas_mk.id_kelas_mk = materi_mk.id_kelas_mk
								where kelas_mk.id_kurikulum_mk = '".$id_kurikulum['ID_KURIKULUM_MK']."' and kelas_mk.id_semester = $semester_request 
								 and no_kelas_mk = '".$_REQUEST['kelas']."'
								and isi_materi_mk='".$_POST['materi']."' and tgl_materi_mk = to_date('".$_POST['calawal']."', 'DD-MM-YYYY')");
			
				foreach($materi_mk as $data){
						$db->Query("
								select id_presensi_kelas from presensi_kelas 
								join kelas_mk on kelas_mk.id_kelas_mk = presensi_kelas.id_kelas_mk
								where kelas_mk.id_kurikulum_mk = '".$id_kurikulum['ID_KURIKULUM_MK']."' and kelas_mk.id_semester = $semester_request 
								and id_materi_mk='$data[ID_MATERI_MK]' and no_kelas_mk = '".$_REQUEST['kelas']."'
							");			
							
						$id_presensi_kelas = $db->FetchAssoc();	
						
						if ($_POST['dosen'.$i]<>'' || $_POST['dosen'.$i]<>null) {
						$db->Query("delete presensi_mkdos where id_presensi_kelas = '$id_presensi_kelas[ID_PRESENSI_KELAS]' and id_dosen = '".$_REQUEST['dosen'.$i]."'");	
						$db->Query("insert into presensi_mkdos (id_presensi_kelas,id_dosen,kehadiran)
							  values ('$id_presensi_kelas[ID_PRESENSI_KELAS]','".$_REQUEST['dosen'.$i]."',1)");
						$dosen .= "'".$_REQUEST['dosen'.$i]."', ";	  
						}else{
						$db->Query("delete presensi_mkdos where id_presensi_kelas = '$id_presensi_kelas[ID_PRESENSI_KELAS]' and id_dosen not in (".$dosen." '0')");
						}
				}
				
				
		}
	}
	
		
	
	if($_REQUEST['action'] == 'edit'){
		$smarty->assign('mkwu_pjma_pengampu', $mkwu->mkwu_presensi_pengampu($id_kurikulum['ID_KURIKULUM_MK'], $semester_request, $_REQUEST['hari'], $_REQUEST['jam'], $_REQUEST['ruang'], $_REQUEST['kelas']));	
	}else{
	$smarty->assign('mkwu_pjma_pengampu', $mkwu->mkwu_pjma_pengampu($id_kurikulum['ID_KURIKULUM_MK'], $semester_request));
	}
	
	
	if($_REQUEST['action'] == 'presensi'){
			$smarty->assign('PESERTA', $mkwu->mkwu_presensi_msh_view($id_kurikulum['ID_KURIKULUM_MK'], $semester_request, $_REQUEST['hari'], $_REQUEST['jam'], $_REQUEST['ruang'], $_REQUEST['kelas'], $_REQUEST['id'], $_REQUEST['materi'], $_REQUEST['calawal'], $_REQUEST['jam_mulai'], $_REQUEST['jam_selesai']));	
	}
	
	
	if($_REQUEST['action'] == 'entrypresensi'){
	
		if ($_REQUEST['id']==1) {
			for ($i=1; $i<=$_POST['counter']; $i++)
			{
				if ($_POST['mhs'.$i]<>'' || $_POST['mhs'.$i]<>null) {
					$id_presensi_kelas = $mkwu->mkwu_presensi_kelas_mhs($id_kurikulum['ID_KURIKULUM_MK'], $semester_request, $_REQUEST['kelas'], $_POST['mhs'.$i], $_REQUEST['hari'], $_REQUEST['jam'], $_REQUEST['ruang'], $_REQUEST['materi'], $_REQUEST['calawal'], $_REQUEST['jam_mulai'], $_REQUEST['jam_selesai']);
				   $db->Query("insert into presensi_mkmhs (id_presensi_kelas,id_mhs,kehadiran)
							  values ($id_presensi_kelas[ID_PRESENSI_KELAS],'".$_POST['mhs'.$i]."',1)");
				}else{
					$id_presensi_kelas = $mkwu->mkwu_presensi_kelas_mhs($id_kurikulum['ID_KURIKULUM_MK'], $semester_request, $_REQUEST['kelas'], $_POST['mhs2'.$i], $_REQUEST['hari'], $_REQUEST['jam'], $_REQUEST['ruang'], $_REQUEST['materi'], $_REQUEST['calawal'], $_REQUEST['jam_mulai'], $_REQUEST['jam_selesai']);
				   $db->Query("insert into presensi_mkmhs (id_presensi_kelas,id_mhs,kehadiran)
							  values ($id_presensi_kelas[ID_PRESENSI_KELAS],'".$_POST['mhs2'.$i]."',0)");
				}
			}
		}
	
		if ($_REQUEST['id']==2) {
		
		
		for ($i=1; $i<=$_POST['counter']; $i++)
			{				
				if ($_POST['mhs'.$i]<>'' || $_POST['mhs'.$i]<>null) {
					$id_presensi_kelas = $mkwu->mkwu_presensi_kelas_mhs($id_kurikulum['ID_KURIKULUM_MK'], $semester_request, $_REQUEST['kelas'], $_POST['mhs'.$i], $_REQUEST['hari'], $_REQUEST['jam'], $_REQUEST['ruang'], $_REQUEST['materi'], $_REQUEST['calawal'], $_REQUEST['jam_mulai'], $_REQUEST['jam_selesai']);
				   $db->Query("update presensi_mkmhs set kehadiran=1 where id_presensi_kelas=$id_presensi_kelas[ID_PRESENSI_KELAS] and id_mhs='".$_POST['mhs'.$i]."'");
				}else{
					$id_presensi_kelas = $mkwu->mkwu_presensi_kelas_mhs($id_kurikulum['ID_KURIKULUM_MK'], $semester_request, $_REQUEST['kelas'], $_POST['mhs2'.$i], $_REQUEST['hari'], $_REQUEST['jam'], $_REQUEST['ruang'], $_REQUEST['materi'], $_REQUEST['calawal'], $_REQUEST['jam_mulai'], $_REQUEST['jam_selesai']);
					$db->Query("update presensi_mkmhs set kehadiran=0 where id_presensi_kelas=$id_presensi_kelas[ID_PRESENSI_KELAS] and id_mhs='".$_POST['mhs2'.$i]."'");
				}	
			}
				
		}
		
	}
	
	
	if($_REQUEST['action'] == 'hapuspresensi'){
		$mkwu->mkwu_presensi_hapus($id_kurikulum['ID_KURIKULUM_MK'], $semester_request, $_REQUEST['kelas'], $_POST['mhs'.$i], $_REQUEST['hari'], $_REQUEST['jam'], $_REQUEST['ruang'], $_REQUEST['materi'], $_REQUEST['calawal'], $_REQUEST['jam_mulai'], $_REQUEST['jam_selesai']);
	}
	
	
	$smarty->assign('mkwu_presensi_view', $mkwu->mkwu_presensi_view($id_kurikulum['ID_KURIKULUM_MK'], $semester_request, $_REQUEST['hari'], $_REQUEST['jam'], $_REQUEST['ruang'], $_REQUEST['kelas']));
	$smarty->assign('mkwu_pjma_ma', $mkwu->mkwu_pjma_ma($id_kurikulum['ID_KURIKULUM_MK'], $semester_request, $_REQUEST['hari'], $_REQUEST['jam'], $_REQUEST['ruang'], $_REQUEST['kelas']));
	
}



$smarty->assign('mata_kuliah_mkwu', $mkwu->mata_kuliah_mkwu());
$smarty->assign('semester', $mkwu->semester());
$smarty->assign('hari', $mkwu->hari());
$smarty->assign('semester_request', $semester_request);
$smarty->display("mkwu/mkwu-presensi.tpl")
?>