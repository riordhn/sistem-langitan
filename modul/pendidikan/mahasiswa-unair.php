<?php

include 'config.php';
include 'class/report_mahasiswa.class.php';
include 'class/evaluasi.class.php';
include "class/laporan.class.php";

$aucc = new laporan($db);
$rm = new report_mahasiswa($db);
$eval = new evaluasi($db);

if (isset($_POST['tampil'])) {
	
	$smarty->assign('program_studi', $eval->program_studi($_POST['fakultas'], $_POST['jenjang'], $_POST['program_studi']));
    $smarty->assign('data_mahasiswa',$rm->get_mahasiswa_dayat($_POST['fakultas'], $_POST['program_studi'], $_POST['tahun_akademik'], $_POST['status'], $_POST['jenjang'], $_POST['jalur'], $_POST['negara'], $_POST['provinsi'], $_POST['kota']));

   	$smarty->assign('tampil', $_POST['tampil']);
}

$smarty->assign('data_fakultas', $rm->get_fakultas());
$smarty->assign('data_jalur', $rm->get_jalur($user->ID_PERGURUAN_TINGGI));
$smarty->assign('data_jenjang', $rm->get_jenjang());
$smarty->assign('data_tahun_akademik', $rm->get_tahun_akademik());
$smarty->assign('data_negara', $aucc->negara());
$smarty->assign('data_status', $rm->get_status());

$smarty->display("mahasiswa/unair/view.tpl");
?>
