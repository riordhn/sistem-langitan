<?php
include('config.php');
include "class/laporan.class.php";
$aucc = new laporan($db);

if($_GET['tampilan'] == 'detail'){
$smarty->assign('mhs_detail', $aucc->mhs_asing($_REQUEST['angkatan'], $_REQUEST['tampilan'], $_REQUEST['thn_akademik'], $_REQUEST['status_akademik'], $_REQUEST['fakultas'], $_REQUEST['jenjang'], $_REQUEST['program_studi'], $_REQUEST['negara'], $_REQUEST['provinsi'], $_REQUEST['kota'], $_REQUEST['jenis_kerjasama']));
}

if(isset($_POST['angkatan'])){
$smarty->assign('prodi', $aucc->prodi_condition($_POST['fakultas'], $_POST['jenjang']));
$smarty->assign('mhs_jenjang', $aucc->mhs_asing($_POST['angkatan'], $_POST['tampilan'], $_POST['thn_akademik'], $_POST['status_akademik'], $_POST['fakultas'], $_POST['jenjang'], $_POST['program_studi'], $_POST['negara'], $_POST['provinsi'], $_POST['kota'], $_POST['jenis_kerjasama']));
}

$smarty->assign('data_fakultas', $aucc->fakultas());
$smarty->assign('data_jenjang', $aucc->jenjang());
$smarty->assign('data_negara', $aucc->negara());
$smarty->assign('angkatan', $aucc->angkatan());
$smarty->assign('thn_akademik', $aucc->thn_akademik());
$smarty->assign('jenis_kerjasama', $aucc->jenis_kerjasama());
$smarty->assign('status_tidak_aktif', $aucc->status_tidak_aktif());

$smarty->display('laporan/laporan-mhs-asing.tpl');
?>
