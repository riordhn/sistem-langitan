<?php

include "config.php";
include "class/aucc.class.php";
$aucc = new AUCC_Pendidikan($db);

$smarty->assign('get_fakultas', $aucc->get_fakultas());

if(isset($_GET['fakultas'])){
$mhs = $aucc->get_mhs_piutang($_GET['fakultas']);
$no = 1;
foreach($mhs as $data){
	$id_semester_piutang = $aucc->get_semester_piutang($data['NIM_MHS']);
	$semester = 0;
	$nim = "";
	foreach($id_semester_piutang as $sp){
		
		$piutang = $aucc->piutang_mhs2($sp['ID_SEMESTER'], $data['ID_MHS']);
		
		if($piutang['TGL_BAYAR'] == ''){
			$semester = $semester + 1;
		}
	}
			if($semester > 0){
			$tampil = $tampil. "<tr>
						<td>$no</td>
						<td><a href='pembayaran-piutang.php?nim=$data[NIM_MHS]'>$data[NIM_MHS]</a></td>
						<td>$data[NM_PENGGUNA]</td>
						<td>$data[NM_FAKULTAS]</td>
						<td>$data[NM_JENJANG] - $data[NM_PROGRAM_STUDI]</td>
						<td>$data[NM_STATUS_PENGGUNA]</td>
						<td style='text-align:center'>$semester</td>
					</tr>";
			$no++;
			}
}
$smarty->assign('fakultas', $_GET['fakultas']);
$smarty->assign('tampil', $tampil);
}elseif(isset($_GET['nim'])){
	$smarty->assign('cek_data', $aucc->cek_data($_GET['nim']));
	$smarty->assign('biaya_mhs', $aucc->biaya_mhs($_GET['nim']));
	$tampil_mhs="";
	
	$semester = $aucc->get_semester_piutang($_GET['nim']);
	$biaya = $aucc->biaya_mhs($_GET['nim']);
	$x = 0;
	foreach($semester as $s){
		
		$bayar = $aucc->pembayaran_mhs($_GET['nim'], $s['ID_SEMESTER']);
		$tampil_mhs = $tampil_mhs . "<tr>
				<td>" . $s['NM_SEMESTER'] . "<br>" . $s['TAHUN_AJARAN'] . "</td>
				<td>".$bayar[0]['NM_BANK']."</td>
				<td>".$bayar[0]['TGL_BAYAR']."</td>";
		$i = 0;
		$x++;
		$denda = 0;
		foreach($biaya as $b){
			$tampil_mhs = $tampil_mhs . "<td style='text-align:right'>";
			if($b['ID_BIAYA'] == $bayar[$i]['ID_BIAYA']){
			$tampil_mhs = $tampil_mhs . $bayar[$i]['BESAR_BIAYA'];
			}
			$tampil_mhs = $tampil_mhs . "</td>";
			$denda = $bayar[$i]['DENDA_BIAYA'] + $denda;
			$i++;
		}
		$tampil_mhs = $tampil_mhs .	"<td style='text-align:right'>$denda</td></tr>";
	}
	$smarty->assign('tampil_mhs', $tampil_mhs);
}

$smarty->display('pembayaran/pembayaran-piutang.tpl');
?>
