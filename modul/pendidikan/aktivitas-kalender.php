<?php
require 'config.php';

$mode = 'view';
$id_pt = $id_pt_user;
$mode_post = post('mode');

$fakultas = $db->QueryToArray("SELECT ID_FAKULTAS, NM_FAKULTAS FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = {$user->ID_PERGURUAN_TINGGI} ORDER BY ID_FAKULTAS");
$smarty->assign('fakultas', $fakultas);

$semester = $db->QueryToArray(
	"SELECT * FROM SEMESTER WHERE (NM_SEMESTER = 'Ganjil' OR NM_SEMESTER = 'Genap') AND ID_PERGURUAN_TINGGI = {$PT->id_perguruan_tinggi}
	ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
$smarty->assign('semester', $semester);

if ($mode_post == 'update') {
		$tgl_mulai = $_POST['tgl_mulai'] . ' 00:00:00';
		$tgl_selesai = $_POST['tgl_selesai']. ' 23:59:59';
    $db->Query("UPDATE JADWAL_KEGIATAN_SEMESTER SET TGL_MULAI_JKS = TO_DATE('$tgl_mulai', 'DD-MM-YYYY HH24:MI:SS'), TGL_SELESAI_JKS = TO_DATE('$tgl_selesai', 'DD-MM-YYYY HH24:MI:SS') WHERE ID_JADWAL_KEGIATAN_SEMESTER = '{$_POST['id']}'");
}

if ($mode_post == 'insert') {
	$no = $_POST['no'];
	for ($i = 1; $i <= $no; $i++) {
		$id = $_POST['id'.$i];
		$tgl_mulai = $_POST['tgl_mulai'.$i] . ' 00:00:00';
		$tgl_selesai = $_POST['tgl_selesai'.$i]. ' 23:59:59';
		$id_kegiatan = $_POST['id_kegiatan'.$i];
		if ($id != '') {
			$db->Query("UPDATE JADWAL_KEGIATAN_SEMESTER SET TGL_MULAI_JKS = TO_DATE('$tgl_mulai', 'DD-MM-YYYY HH24:MI:SS'), 
						TGL_SELESAI_JKS = TO_DATE('$tgl_selesai', 'DD-MM-YYYY HH24:MI:SS') WHERE ID_JADWAL_KEGIATAN_SEMESTER = '$id'");
		} else {
			if ($tgl_mulai != '' and $tgl_selesai != '') {
				$db->Query("INSERT INTO JADWAL_KEGIATAN_SEMESTER (ID_KEGIATAN, ID_SEMESTER, TGL_MULAI_JKS, TGL_SELESAI_JKS, ID_PERGURUAN_TINGGI) 
                            VALUES ('$id_kegiatan', '{$_POST['semester']}', TO_DATE('$tgl_mulai', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('$tgl_selesai', 'DD-MM-YYYY HH24:MI:SS'), {$id_pt})");	
			}
		}
	}
}

if (isset($_POST['fakultas'])) {
	if ($_POST['fakultas'] == '') {
        $db->Query("SELECT COUNT(*) AS CEK
            FROM KEGIATAN
            LEFT JOIN JADWAL_KEGIATAN_SEMESTER ON KEGIATAN.ID_KEGIATAN = JADWAL_KEGIATAN_SEMESTER.ID_KEGIATAN
            LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = JADWAL_KEGIATAN_SEMESTER.ID_SEMESTER
            WHERE KEGIATAN.ID_PERGURUAN_TINGGI = {$id_pt} AND THN_AKADEMIK_SEMESTER = (SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE ID_SEMESTER = '{$_POST['semester']}')
            AND GROUP_SEMESTER = (SELECT GROUP_SEMESTER FROM SEMESTER WHERE ID_SEMESTER = '{$_POST['semester']}')
            ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER ASC, TGL_MULAI_JKS DESC");
        $cek = $db->FetchArray();
	
        $db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '{$_POST['semester']}'");
        $id_semester = $db->FetchArray();
        $smarty->assign('id_semester', $id_semester);
	
		if ($cek['CEK'] >= 1) {
			$jadwal = $db->QueryToArray(
				"SELECT 
					nm_kegiatan, deskripsi_kegiatan, thn_akademik_semester, nm_semester, 
					tgl_mulai_jks, tgl_selesai_jks, 
					id_jadwal_kegiatan_semester, id_semester, kegiatan.id_kegiatan
				FROM kegiatan
				LEFT JOIN 
				(
					SELECT 
						thn_akademik_semester, nm_semester, 
						to_char(tgl_mulai_jks, 'YYYY-MM-DD') AS tgl_mulai_jks, 
						to_char(tgl_selesai_jks, 'YYYY-MM-DD') AS tgl_selesai_jks, 
						id_jadwal_kegiatan_semester, semester.id_semester, kegiatan.id_kegiatan 
					FROM kegiatan
					LEFT JOIN jadwal_kegiatan_semester ON kegiatan.id_kegiatan = jadwal_kegiatan_semester.id_kegiatan
					LEFT JOIN semester ON semester.id_semester = jadwal_kegiatan_semester.id_semester
					WHERE 
						thn_akademik_semester = (SELECT thn_akademik_semester FROM semester WHERE id_semester = {$_POST['semester']}) AND 
						group_semester = (SELECT group_semester FROM semester WHERE id_semester = {$_POST['semester']})
				) A ON A.id_kegiatan = kegiatan.id_kegiatan
				WHERE kegiatan.id_perguruan_tinggi = {$id_pt}
				ORDER BY thn_akademik_semester DESC NULLS LAST, nm_semester ASC, tgl_mulai_jks ASC NULLS LAST");
			$smarty->assign('jadwal', $jadwal);
		} else {
			$jadwal = $db->QueryToArray("SELECT NM_KEGIATAN, DESKRIPSI_KEGIATAN, ID_KEGIATAN
											FROM KEGIATAN WHERE ID_PERGURUAN_TINGGI = {$id_pt}");
			$smarty->assign('jadwal', $jadwal);
		}
	} else {
        $jadwal = $db->QueryToArray("
            SELECT * FROM JADWAL_SEMESTER_FAKULTAS
            LEFT JOIN KEGIATAN ON KEGIATAN.ID_KEGIATAN = JADWAL_SEMESTER_FAKULTAS.ID_KEGIATAN
            LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = JADWAL_SEMESTER_FAKULTAS.ID_SEMESTER
            WHERE KEGIATAN.ID_PERGURUAN_TINGGI = {$id_pt} AND THN_AKADEMIK_SEMESTER = (SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE ID_SEMESTER = '{$_POST['semester']}')
            AND GROUP_SEMESTER = (SELECT GROUP_SEMESTER FROM SEMESTER WHERE ID_SEMESTER = '{$_POST['semester']}')
            AND ID_FAKULTAS = '{$_POST['fakultas']}'
            ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER ASC, TGL_MULAI_JSF DESC");
        $smarty->assign('jadwal', $jadwal);
	}
}

if ($mode_post == 'edit') {
    $db->Query("SELECT NM_KEGIATAN, DESKRIPSI_KEGIATAN, THN_AKADEMIK_SEMESTER, NM_SEMESTER, TO_CHAR(TGL_MULAI_JKS, 
    'DD-MM-YYYY') AS TGL_MULAI_JKS, TO_CHAR(TGL_SELESAI_JKS, 'DD-MM-YYYY') AS TGL_SELESAI_JKS, ID_JADWAL_KEGIATAN_SEMESTER, SEMESTER.ID_SEMESTER
    FROM JADWAL_KEGIATAN_SEMESTER
    LEFT JOIN KEGIATAN ON KEGIATAN.ID_KEGIATAN = JADWAL_KEGIATAN_SEMESTER.ID_KEGIATAN
    LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = JADWAL_KEGIATAN_SEMESTER.ID_SEMESTER
    WHERE ID_JADWAL_KEGIATAN_SEMESTER = '{$_GET['id']}'
    ORDER BY TGL_MULAI_JKS ASC");
    $jadwal = $db->FetchArray();
    $smarty->assign('jadwal', $jadwal);
}

if ($mode_post == 'tambah') {
    $mode= 'tambah';
}

$smarty->display("aktivitas/kalender/$mode.tpl");