<?php
include('config.php');

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
        $db->Query("INSERT INTO KEGIATAN (NM_KEGIATAN, KODE_KEGIATAN, DESKRIPSI_KEGIATAN, ID_PERGURUAN_TINGGI) VALUES ('$_POST[nm_kegiatan]', '$_POST[kode_kegiatan]', '$_POST[deskripsi_kegiatan]', '{$id_pt}')");
        
    }
    else if (post('mode') == 'edit')
    {
        $db->Query("UPDATE KEGIATAN SET NM_KEGIATAN = '$_POST[nm_kegiatan]', KODE_KEGIATAN = '$_POST[kode_kegiatan]', DESKRIPSI_KEGIATAN = '$_POST[deskripsi_kegiatan]' 
					WHERE ID_KEGIATAN = '$_POST[id_kegiatan]'");
    }
    else if (post('mode') == 'delete')
    {
		$db->Query("DELETE KEGIATAN WHERE ID_KEGIATAN = '$_POST[id_kegiatan]'");
    }
}

if ($mode == 'view')
{
	$kegiatan_set = $db->QueryToArray("SELECT * FROM KEGIATAN WHERE ID_PERGURUAN_TINGGI = {$id_pt} ORDER BY NM_KEGIATAN");
	$smarty->assign('kegiatan_set', $kegiatan_set);
}
else if ($mode == 'edit' or $mode == 'delete' or $mode == 'add')
{
	$db->Query("SELECT * FROM KEGIATAN WHERE ID_KEGIATAN = '$_GET[id_kegiatan]' ORDER BY NM_KEGIATAN");
	$kegiatan = $db->FetchArray();
	$smarty->assign('kegiatan', $kegiatan);

    $kode_kegiatan_set = $db->QueryToArray("SELECT * FROM KODE_KEGIATAN ORDER BY KODE_KEGIATAN");
    $smarty->assignByRef('kode_kegiatan_set', $kode_kegiatan_set);
}

$smarty->display("aktivitas/kegiatan/{$mode}.tpl");
?>
