<?php
include 'config.php';
include 'class/report_mahasiswa.class.php';

$rm = new report_mahasiswa($db);
$data_excel = $rm->get_mahasiswa_dayat(get('fakultas'), get('program_studi'), get('tahun_akademik'), get('status'), get('jenjang'), get('jalur'));
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
<table cellpadding="3" cellspacing="3" border="1">
    <thead>
        <tr>
            <td class="header_text">NIM</td>
            <td class="header_text">NAMA</td>
			<td class="header_text">JENIS KELAMIN</td>
            <td class="header_text">ANGKATAN</td>
            <td class="header_text">JENJANG</td>
            <td class="header_text">PROGRAM STUDI</td>
            <td class="header_text">FAKULTAS</td>
            <td class="header_text">STATUS</td>
            <td class="header_text">ALAMAT</td>
			<td class="header_text">TELP</td>
			<td class="header_text">TGL lAHIR</td>
			<td class="header_text">TEMPAT lAHIR</td>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($data_excel as $data) {
			if($data['KELAMIN_PENGGUNA'] == 1){
				$kelamin = 'Laki-Laki';
			}elseif($data['KELAMIN_PENGGUNA'] == 2){
				$kelamin = 'Perempuan';
			}else{
				$kelamin = '';
			}
			
            echo
            "<tr>
                <td style>'{$data['NIM_MHS']}</td>
                <td>{$data['NM_PENGGUNA']}</td>
				<td>$kelamin</td>
                <td>{$data['THN_ANGKATAN_MHS']}</td>
                <td>{$data['NM_JENJANG']}</td>
                <td>{$data['NM_PROGRAM_STUDI']}</td>
                <td>{$data['NM_FAKULTAS']}</td>
                <td>{$data['NM_STATUS_PENGGUNA']}</td>
				<td>{$data['ALAMAT_MHS']}</td>
				<td>'{$data['MOBILE_MHS']}</td>
				<td>'{$data['TGL_LAHIR_PENGGUNA']}</td>
				<td>'{$data['NM_KOTA']}</td>
            </tr>";
        }
        ?>
    </tbody>
</table>

<?php
$filename = 'Excel_Mhs_Unair' . date('Y-m-d');
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
