<?php
include('config.php');

if ($request_method == 'POST')
{
    $db->BeginTransaction();
    
    $result = $db->Query("TRUNCATE TABLE TEMP_MAHASISWA");
    
    if ($result)
        echo "Pengosongan Mahasiswa Berhasil<br/>";
    else
        echo "Pengosongan Mahasiswa Gagal<br/>";
    
    $result = $db->Query("TRUNCATE TABLE TEMP_PENGGUNA");
    
    if ($result)
        echo "Pengosongan Pengguna Berhasil<br/>";
    else
        echo "Pengosongan Pengguna Gagal<br/>";
    
    $result = $db->Query("UPDATE TEMP_CALON_MAHASISWA SET STATUS = 12");
    
    if ($result)
        echo "Update Calon Mahasiswa berhasil<br/>";
    else
        echo "Update Calon Mahasiswa Gagal<br/>";
    
    $result = $db->Commit();
    
    if ($result)
        echo "Reset Berhasil<br/>";
    else
        echo "Reset Gagal<br/>";
}
?>

<form action="reset-test-nim.php" method="post">
    <input type="submit" value="Reset Test Nim" />
</form>