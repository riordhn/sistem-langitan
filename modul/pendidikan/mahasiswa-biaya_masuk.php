<?php
require 'config.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id_program_studi = post('id_program_studi');
    $angkatan = post('angkatan');
    $biaya_masuk = post('biaya_masuk');

    $sql =
        "UPDATE MAHASISWA SET BIAYA_MASUK = {$biaya_masuk} WHERE 
        ID_PROGRAM_STUDI = {$id_program_studi} AND 
        THN_ANGKATAN_MHS = {$angkatan} AND 
        ID_PENGGUNA IN (SELECT ID_PENGGUNA FROM PENGGUNA WHERE ID_PERGURUAN_TINGGI = {$user->ID_PERGURUAN_TINGGI})";

    $db->Query($sql);
    $num_rows = $db->NumRows();
    $smarty->assign('num_rows', $num_rows);
}

$prodi_set = $db->QueryToArray(
    "SELECT PS.*, J.NM_JENJANG FROM FAKULTAS F
    JOIN PROGRAM_STUDI PS ON PS.ID_FAKULTAS = F.ID_FAKULTAS
    JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
    WHERE F.ID_PERGURUAN_TINGGI = {$user->ID_PERGURUAN_TINGGI}");

$smarty->assign('prodi_set', $prodi_set);
$smarty->display('mahasiswa/biaya_masuk/index.tpl');
