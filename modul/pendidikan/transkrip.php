<?php

if(isset($_REQUEST['nim'])){
include '../../config.php';
include('class/date.class.php');

include_once '../../tcpdf/config/lang/ind.php';
include_once '../../tcpdf/tcpdf.php';

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Transkrip Akademik');
$pdf->SetSubject('Transkrip Akademik');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');


$idmhs="select nim_mhs from mahasiswa where nim_mhs='".$_REQUEST['nim']."'";
$result0 = $db->Query($idmhs)or die("salah kueri get ");
while($r0 = $db->FetchRow()) {
	$nim = $r0[0];
}

$biomhs="select mhs.nim_mhs, 
		case when p.gelar_belakang is not null then p.gelar_depan || ' ' || INITCAP(p.nm_pengguna)||', '||p.gelar_belakang 
		when p.gelar_depan is not null then p.gelar_depan || ' ' || INITCAP(p.nm_pengguna)
		else INITCAP(p.nm_pengguna) end , 
		j.nm_jenjang_ijasah, INITCAP(ps.nm_program_studi), mhs.status_akademik_mhs, f.nm_fakultas,
			f.alamat_fakultas, f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas, f.id_fakultas,
			kt.nm_kota, TO_CHAR(p.tgl_lahir_pengguna, 'DD-MM-YYYY'), TO_CHAR(pw.TGL_LULUS_PENGAJUAN, 'DD-MM-YYYY'), pw.no_ijasah, TO_CHAR(mhs.tgl_terdaftar_mhs, 'DD-MM-YYYY'), mhs.id_mhs, pw.judul_ta, pw.lahir_ijazah,
			j.id_jenjang,mhs.id_program_studi,INITCAP(pm.nm_prodi_minat),mhs.status,f.id_fakultas,trim('AGUSTUS'||mhs.thn_angkatan_mhs) as tgldaftar,mhs.thn_angkatan_mhs as agk,
			P.FOTO_PENGGUNA, KODE_PROGRAM_STUDI,  TO_CHAR(pw.TGL_LULUS_PENGAJUAN, 'YYYY'), GELAR_PENDEK, GELAR_PANJANG,
			cmp.PTN_S1, cmp.PTN_S2, cmp.PRODI_S1, cmp.PRODI_S2, cmp.IP_S1, cmp.IP_S2
			from mahasiswa mhs
			left join kota kt on kt.id_kota=mhs.lahir_kota_mhs
			left join pengajuan_wisuda pw on pw.id_mhs=mhs.id_mhs
			left join pengguna p on mhs.id_pengguna=p.id_pengguna
			left join program_studi ps on mhs.id_program_studi=ps.id_program_studi
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			left join prodi_minat pm on mhs.id_prodi_minat=pm.id_prodi_minat
			left join calon_mahasiswa_pasca cmp on cmp.id_c_mhs = mhs.id_c_mhs
			where mhs.nim_mhs='".$nim."'";
$result1 = $db->Query($biomhs)or die("salah kueri biodata ");
while($r1 = $db->FetchRow()) {
	$nim_mhs = $r1[0];
	$nm_mhs = $r1[1];
	$jenjang = $r1[2];
	$prodi = $r1[3];
	$status = $r1[4];
	$fak = $r1[5];
	$alm_fak = $r1[6];
	$pos_fak = $r1[7];
	$tel_fak = $r1[8];
	$fax_fak = $r1[9];
	$web_fak = $r1[10];
	$eml_fak = $r1[11];
	$kd_fak = $r1[12];
	$tpt_lhr = $r1[13];
	$tgl_lhr = $r1[14];
	$tgl_lls = $r1[15];
	$no_ijazah = $r1[16];
	$tgl_dtr = $r1[17];
	$id_mhs = $r1[18];
	$jdl_ta = $r1[19];
	$ttl = $r1[20];
	$id_jjg = $r1[21];
	$idprodi = $r1[22];
	$minatmhs = $r1[23];
	$aj = $r1[24];
	$idfak = $r1[25];
	$tgl_dtr1 = $r1[26];
	$agk = $r1[27];
	$noseri= $r1[0].'/'.$r1[2].'/'.$r1[29].'/'.$r1[30];
	$foto = "../../foto_wisuda/$r1[0].jpg";
	$gelar_pendek = $r1[31];
	$gelar_panjang = $r1[32];
	$ptn_1 = $r1[33];
	$ptn_2 = $r1[34];
	$prodi_1 = $r1[35];
	$prodi_2 = $r1[36];
	$ip_1 = $r1[37];
	$ip_2 = $r1[38];
	
	if($ttl <> ""){$ttl;}else{$ttl = $tpt_lhr.', '. $tgl_lhr;}
}




$lama_studi="select  
(lama_studi + (case when semester_studi = 1 then 1 when semester_studi = 0 then 0.5 else 0 end)) as LAMA_STUDI
from (
select PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, JENJANG.id_jenjang, STATUS, 
TO_CHAR(TGL_LULUS, 'YYYY') as THN_AKADEMIK_LULUS, b.THN_AKADEMIK_SEMESTER || (CASE WHEN b.NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_AKADEMIK_SMT, 
(b.THN_AKADEMIK_SEMESTER - a.THN_AKADEMIK_SEMESTER - (case when cuti is null then 0 else cuti end)) as lama_studi, 
(CASE WHEN b.NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) - (CASE WHEN a.NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) as semester_studi
, kelamin_pengguna
from mahasiswa 
join semester a on a.id_semester = mahasiswa.id_semester_masuk
join pengguna on PENGGUNA.id_pengguna = MAHASISWA.id_pengguna
join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi 
join fakultas on fakultas.id_fakultas = program_studi.id_fakultas 
join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs 
join jenjang on jenjang.id_jenjang = program_studi.id_jenjang 
join admisi on ADMISI.ID_MHS = MAHASISWA.ID_MHS and status_akd_mhs = '4' and status_apv = 1 AND TGL_LULUS IS NOT NULL
JOIN SEMESTER b ON b.ID_SEMESTER = ADMISI.ID_SEMESTER

left join (
	select id_mhs, count(*) as cuti from ADMISI where status_apv = 1 and status_akd_mhs = 2
group by id_mhs 
) x on x.id_mhs = MAHASISWA.id_mhs
where NIM_MHS = '".$nim_mhs."'
)";
$result2 = $db->Query($lama_studi)or die("salah kueri lama_studi ");
while($r2 = $db->FetchRow()) {
	$lama_studi = $r2[0];
}

						$filename = '../../foto_wisuda/'.$nim_mhs.'.jpg';

						if (file_exists($filename) and filesize($filename)>0) {
							
						}else{
							$namafile = $nim_mhs.'.jpg';
							//original file
							$filenya = "http://10.0.110.13/foto/fotoijasah/".$namafile;
							//directory to copy to (must be CHMOD to 777)
							$copydir = ".../../foto_wisuda/";
							$data = file_get_contents($filenya);
							$file = fopen($copydir . $namafile, "w+");
				
							fputs($file, $data);
							fclose($file);
						}

//parameter seting panjang
$q1 = $db->Query("select count(*) from 
(select k.*, rownum rnum from 
(select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='".$nim_mhs."'
order by e.nm_mata_kuliah) k
)") or die("salah kueri q1 ");
$rq1 = $db->FetchRow();
$panjang_mk = round($rq1[0]/2);


$nilai="select id_jenjang, sum(sks) as sks_total, round((sum((bobot*sks))/sum(sks)),2) as ipk
from 
(
select ps.id_jenjang, e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
left join program_studi ps on m.id_program_studi=ps.id_program_studi
where rangking=1 and m.nim_mhs='".$nim_mhs."'
)
group by id_jenjang";
$result2 = $db->Query($nilai)or die("salah kueri nilai ");
while($r2 = $db->FetchRow()) {
	$jjg = $r2[0];
	$jum_sks = $r2[1];
	$ipk = number_format($r2[2],2,'.',',');
}

if($ipk != ''){
$pre="select nm_mhs_predikat 
from mhs_predikat left join jenjang on mhs_predikat.id_jenjang=jenjang.id_jenjang 
where jenjang.id_jenjang=$jjg and ipk_min_mhs_predikat<=$ipk and ipk_max_mhs_predikat>=$ipk";
$result_pre = $db->Query($pre)or die("salah kueri predikat ");
while($r_pre = $db->FetchRow()) {
	$predikat = $r_pre[0];
}


if($ipk >= '3.51' and $ipk <= '4.00'){
	if($jjg == '1'){
		if($lama_studi <= '4'){
			$predikat = $predikat;
		}else{
			$predikat = 'Sangat Memuaskan';
		}
	}
	
	if($jjg == '5'){
		if($lama_studi <= '3'){
			$predikat = $predikat;
		}else{
			$predikat = 'Sangat Memuaskan';
		}
	}
}

}

$pemb="SELECT NM_PENGGUNA 
FROM PEMBIMBING_TA
JOIN DOSEN ON PEMBIMBING_TA.ID_DOSEN = DOSEN.ID_DOSEN
JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = DOSEN.ID_PENGGUNA
JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBIMBING_TA.ID_MHS
WHERE NIM_MHS = '".$nim_mhs."'";
$result_pemb = $db->Query($pemb)or die("salah kueri predikat ");
while($r_pemb = $db->FetchRow()) {
	$pembibing_ta = $r_pemb[0];
}



$bulan['01'] = "Januari"; $bulan['02'] = "Februari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "Juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "November";  $bulan['12'] = "Desember";

$dekan="select d.nip_dosen ,case when p.gelar_belakang is not null then 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)||', '||p.gelar_belakang) else 
trim(p.gelar_depan||' '||upper(p.nm_pengguna)) end as nm_dosen from fakultas f, pengguna p, dosen d
where p.id_pengguna=f.id_dekan and f.id_fakultas=".$kd_fak." and p.id_pengguna=d.id_pengguna";
$resultdekan = $db->Query($dekan)or die("salah kueri dekan ");
while($dkn = $db->FetchRow()) {
	$nm_dkn = $dkn[1];
	$nip_dkn = $dkn[0];
}

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
if(strlen($nm_mhs) >= 45){
$pdf->SetMargins(7, 10, 7);
}else{
$pdf->SetMargins(10, 10, 10);
}

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('times', '', 8);

// add a page
$pdf->AddPage('P', 'FOLIO');

// set the starting point for the page content
$pdf->setPageMark();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td width="20%" align="center" valign="middle"><img src="../../img/akademik_images/logounair.gif" width="90" border="0"></td>
    <td width="80%" align="center" valign="middle">
	<font size="14"><b>Kementrian Pendidikan dan Kebudayaan</b></font><br />
	<font size="20"><b>UNIVERSITAS AIRLANGGA</b></font><br />
	<font size="14"><b>';
	if($idfak == 9){
$html .= 		
	'PROGRAM ';	
	}else{
$html .= 		
	'FAKULTAS ';
	}
$html .= strtoupper($fak).'</b></font><br />
	<font size="10">'.strtoupper($alm_fak).', '.$pos_fak.' Telp. '.$tel_fak.', Fax. '.$fax_fak.'<br />
	Website : '.strtolower($web_fak).', Email : '.strtolower($eml_fak).'</font>
	</td>
  </tr>
  <tr>
    <td colspan="2">
		<table style="border-bottom:2px solid black; width:98%">
			<tr>
				<td colspan="2"></td>
			</tr>
		</table>
	</td>
  </tr>
</table>';

$html .= '<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td align="center" width="90%"><font size="12"><b>TRANSKRIP AKADEMIK</b></font></td>
	<td align="right" width="10%" rowspan="6"><img src="'.$foto.'" width="80" border="0"></td>
  </tr>
  <tr>
  	<td align="center"><font size="10"><b>Nomor : '.$noseri.'</b></font></td>
	<td></td>
  </tr>
  <tr>
  	<td align="center"><font size="10"><b></b></font></td>
	<td></td>
  </tr>
  <tr>
  	<td align="center"><font size="12"><b>'.$nm_mhs.'</b></font></td>
	<td></td>
  </tr>
  <tr>
  	<td align="center"><font size="11"><b>Nomor Induk Mahasiswa : '.$nim_mhs.'</b></font></td>
	<td></td>
  </tr>
  <tr>
  	<td align="center"><font size="11"><b>Lahir di '.$ttl.'</b></font></td>
	<td></td>
  </tr>
</table>
';

//diploma dan s1 reguler
if  ($id_jjg==5 or ($id_jjg==1 and $aj!='AJ'))
{

$html .= '<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td width="16%"><font size="9"><b>Program studi</b></font></td>
	<td width="2%"><font size="9">:</font></td>
	<td width="25%"><font size="9">'.strtoupper($prodi).'</font></td>
	<td width="20%"><font size="9"><b>Nomor Ijasah</b></font></td>
	<td width="2%"><font size="9">:</font></td>
	<td width="35%"><font size="9">'.$no_ijazah.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Program Pendidikan</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$jenjang.'</font></td>
	<td><font size="9"><b>Gelar Akademik</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$gelar_panjang.' ('.$gelar_pendek.')</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Asal SLTA</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9"></font></td>
	<td><font size="9"><b>Kredit Yang Ditempuh</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$jum_sks.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Tanggal Terdaftar</b></font></td>
	<td><font size="9">:</font></td>';
	if (strlen($tgl_dtr) == 4) {
	$html .= '<td><font size="9">'.$tgl_dtr.'</font></td>';
	} elseif($tgl_dtr == ''){
	$html .= '<td><font size="9"> Agustus '.$agk.'</font></td>';
	}else {
	$html .= '<td><font size="9">'.date("d", strtotime($tgl_dtr)).' '.$bulan[date("m", strtotime($tgl_dtr))].' '.date("Y", strtotime($tgl_dtr)).'</font></td>';
	}
$html .= '	
	<td><font size="9"><b>Indeks Prestasi Komulatif</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$ipk.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Tanggal Lulus</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.date("d", strtotime($tgl_lls)).' '.$bulan[date("m", strtotime($tgl_lls))].' '.date("Y", strtotime($tgl_lls)).'</font></td>
	<td><font size="9"><b>Predikat Kelulusan</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$predikat.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b></b></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"><b></b></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"></font></td>
  </tr>';
}


//S1 alih jenis
if  (($id_jjg==1 and $aj=='AJ'))
{

$html .= '<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td width="16%"><font size="9"><b>Program studi</b></font></td>
	<td width="2%"><font size="9">:</font></td>
	<td width="30%"><font size="9">'.strtoupper($prodi).'</font></td>
	<td width="20%"><font size="9"><b>Tanggal Terdaftar</b></font></td>
	<td width="2%"><font size="9">:</font></td>';
	if (strlen($tgl_dtr) == 4) {
	$html .= '<td width="30%"><font size="9">'.$tgl_dtr.'</font></td>';
	} elseif($tgl_dtr == ''){
	$html .= '<td width="30%"><font size="9"> Agustus '.$agk.'</font></td>';
	}else {
	$html .= '<td width="30%"><font size="9">'.date("d", strtotime($tgl_dtr)).' '.$bulan[date("m", strtotime($tgl_dtr))].' '.date("Y", strtotime($tgl_dtr)).'</font></td>';
	}
$html .= '
  </tr>
  <tr>
  	<td><font size="9"><b>Program Pendidikan</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$jenjang.'</font></td>
	<td><font size="9"><b>Tanggal Lulus</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.date("d", strtotime($tgl_lls)).' '.$bulan[date("m", strtotime($tgl_lls))].' '.date("Y", strtotime($tgl_lls)).'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Minat Studi</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$minatmhs.'</font></td>
	<td><font size="9"><b>Nomor Ijasah</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$no_ijazah.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Program Studi DIII</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$prodi_1.'</font></td>
	<td><font size="9"><b>Gelar Akademik</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$gelar_panjang.' ('.$gelar_pendek.')</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Perguruan Tinggi DIII</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$ptn_1.'</font></td>
	<td><font size="9"><b>Kredit Yang Ditempuh</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$jum_sks.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Sks Diakui</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9"></font></td>
	<td><font size="9"><b>Indeks Prestasi Komulatif</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$ipk.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>IPK Diakui</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$ip_1.'</font></td>
	<td><font size="9"><b>Predikat Kelulusan</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$predikat.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>No Ijasah DIII</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9"></font></td>
	<td><font size="9"><b></b></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"></font></td>
  </tr>
  <tr>
  	<td><font size="9"><b></b></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"><b></b></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"></font></td>
  </tr>';
}


//S2 dan profesi
if  ($id_jjg==2 or $id_jjg==9 or $id_jjg==10)
{

$html .= '<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td width="16%"><font size="9"><b>Program studi</b></font></td>
	<td width="2%"><font size="9">:</font></td>
	<td width="30%"><font size="9">'.strtoupper($prodi).'</font></td>
	<td width="20%"><font size="9"><b>Tanggal Lulus</b></font></td>
	<td width="2%"><font size="9">:</font></td>
	<td width="30%"><font size="9">'.date("d", strtotime($tgl_lls)).' '.$bulan[date("m", strtotime($tgl_lls))].' '.date("Y", strtotime($tgl_lls)).'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Program Pendidikan</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$jenjang.'</font></td>
	<td><font size="9"><b>Nomor Ijasah</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$no_ijazah.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Minat Studi</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$minatmhs.'</font></td>
	<td><font size="9"><b>Gelar Akademik</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$gelar_panjang.' ('.$gelar_pendek.')</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Program Studi S1</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$prodi_1.'</font></td>
	<td><font size="9"><b>Kredit Yang Ditempuh</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$jum_sks.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Perguruan Tinggi S1</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$ptn_1.'</font></td>
	<td><font size="9"><b>Indeks Prestasi Komulatif</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$ipk.'</font></td>
  </tr>
 
  <tr>
  	<td><font size="9"><b>No Ijasah S1</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9"></font></td>
	<td><font size="9"><b>Predikat Kelulusan</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$predikat.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Tanggal Terdaftar</b></font></td>
	<td><font size="9">:</font></td>';
	if (strlen($tgl_dtr) == 4) {
	$html .= '<td><font size="9">'.$tgl_dtr.'</font></td>';
	} elseif($tgl_dtr == ''){
	$html .= '<td><font size="9"> Agustus '.$agk.'</font></td>';
	}else {
	$html .= '<td><font size="9">'.date("d", strtotime($tgl_dtr)).' '.$bulan[date("m", strtotime($tgl_dtr))].' '.date("Y", strtotime($tgl_dtr)).'</font></td>';
	}
$html .= '	
	<td><font size="9"><b></b></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"></font></td>
  </tr>
  <tr>
  	<td><font size="9"><b></b></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"><b></b></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"></font></td>
  </tr>';
}




//S3
if  ($id_jjg==3)
{

$html .= '<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td width="16%"><font size="9"><b>Program studi</b></font></td>
	<td width="2%"><font size="9">:</font></td>
	<td width="30%"><font size="9">'.strtoupper($prodi).'</font></td>
	<td width="20%"><font size="9"><b>No Ijasah S2</b></font></td>
	<td width="2%"><font size="9">:</font></td>
	<td width="30%"><font size="9"></font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Program Pendidikan</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$jenjang.'</font></td>
	<tr>
  	<td><font size="9"><b>Tanggal Terdaftar</b></font></td>
	<td><font size="9">:</font></td>';
	if (strlen($tgl_dtr) == 4) {
	$html .= '<td><font size="9">'.$tgl_dtr.'</font></td>';
	} elseif($tgl_dtr == ''){
	$html .= '<td><font size="9"> Agustus '.$agk.'</font></td>';
	}else {
	$html .= '<td><font size="9">'.date("d", strtotime($tgl_dtr)).' '.$bulan[date("m", strtotime($tgl_dtr))].' '.date("Y", strtotime($tgl_dtr)).'</font></td>';
	}
$html .= '	
  </tr>
  <tr>
  	<td><font size="9"><b>Minat Studi</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$minatmhs.'</font></td>
	<td><font size="9"><b>Tanggal Lulus</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.date("d", strtotime($tgl_lls)).' '.$bulan[date("m", strtotime($tgl_lls))].' '.date("Y", strtotime($tgl_lls)).'</font></td>
	
  </tr>
  <tr>
  	<td><font size="9"><b>Program Studi S1</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$prodi_1.'</font></td>
	<td width="20%"><font size="9"><b>Nomor Ijasah</b></font></td>
	<td width="2%"><font size="9">:</font></td>
	<td width="32%"><font size="9">'.$no_ijazah.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Perguruan Tinggi S1</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$ptn_1.'</font></td>
	<td><font size="9"><b>Gelar Akademik</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$gelar_panjang.' ('.$gelar_pendek.')</font></td>
  </tr> 
  <tr>
  	<td><font size="9"><b>No Ijasah S1</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9"></font></td>
	<td><font size="9"><b>Kredit Yang Ditempuh</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$jum_sks.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Program Studi S2</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$prodi_2.'</font></td>
	<td><font size="9"><b>Indeks Prestasi Komulatif</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$ipk.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Perguruan Tinggi S2</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$ptn_2.'</font></td>
	<td><font size="9"><b>Predikat Kelulusan</b></font></td>
	<td><font size="9">:</font></td>
	<td><font size="9">'.$predikat.'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b></b></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"><b></b></font></td>
	<td><font size="9"></font></td>
	<td><font size="9"></font></td>
  </tr>';
}


  
if  ($id_jjg==5)
{
$html .= ' 	
  <tr>
  	<td><font size="9"><b>Judul Tugas Akhir</b></font></td>
	<td><font size="9">:</font></td>
	<td colspan="4"><font size="9">'.trim(strtoupper($jdl_ta)).'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Pembimbing 1</b></font></td>
	<td><font size="9">:</font></td>
	<td colspan="4"><font size="9"></font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Pembimbing 2</b></font></td>
	<td><font size="9">:</font></td>
	<td colspan="4"><font size="9"></font></td>
  </tr>';
}
if  ($id_jjg==1 or $id_jjg==9 or $id_jjg==10)
{
$html .= '	
  <tr>
  	<td><font size="9"><b>Judul Skripsi</b></font></td>
	<td><font size="9">:</font></td>
	<td colspan="4"><font size="9">'.trim(strtoupper($jdl_ta)).'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Pembimbing 1</b></font></td>
	<td><font size="9">:</font></td>
	<td colspan="4"><font size="9"></font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Pembimbing 2</b></font></td>
	<td><font size="9">:</font></td>
	<td colspan="4"><font size="9"></font></td>
  </tr>';
}
if  ($id_jjg==2)
{
$html .= '	
  <tr>
  	<td><font size="9"><b>Judul Thesis</b></font></td>
	<td><font size="9">:</font></td>
	<td colspan="4"><font size="9">'.trim(strtoupper($jdl_ta)).'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Pembimbing Utama</b></font></td>
	<td><font size="9">:</font></td>
	<td colspan="4"><font size="9"></font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Pembimbing Serta</b></font></td>
	<td><font size="9">:</font></td>
	<td colspan="4"><font size="9"></font></td>
  </tr>';
}
if  ($id_jjg==3)
{
$html .= '	
  <tr>
  	<td><font size="9"><b>Judul Desertasi</b></font></td>
	<td><font size="9">:</font></td>
	<td colspan="4"><font size="9">'.trim(strtoupper($jdl_ta)).'</font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Promotor</b></font></td>
	<td><font size="9">:</font></td>
	<td colspan="4"><font size="9"></font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Ko-Promotor 1</b></font></td>
	<td><font size="9">:</font></td>
	<td colspan="4"><font size="9"></font></td>
  </tr>
  <tr>
  	<td><font size="9"><b>Ko-Promotor 2</b></font></td>
	<td><font size="9">:</font></td>
	<td colspan="4"><font size="9"></font></td>
  </tr>';
}
$html .= '  
</table><br />
';

$html .= '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="48%">
	<table width="100%" border="1" cellspacing="0" cellpadding="2">
		<tr align="center">
			<td width="20%"><b>Kode</b></td>
			<td width="60%"><b>Mata Kuliah</b></td>
			<td width="10%"><b>sks</b></td>
			<td width="10%"><b>Nilai</b></td>
		</tr>';
$list1="select * from 
(select k.*, rownum rnum from 
(select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='".$nim_mhs."'
order by e.nm_mata_kuliah) k
where rownum<=$panjang_mk)";
$result21 = $db->Query($list1)or die("salah kueri list1 ");
while($r21 = $db->FetchRow()) {
	$kode_ma21 = $r21[0];
	$nama_ma21 = $r21[1];
	$sks_ma21 = $r21[3];
	$nilai_ma21 = $r21[4];
	$bobot_ma21 = $r21[5];

$html .= '		
		<tr>
			<td>'.$kode_ma21.'</td>
			<td>'.strtoupper($nama_ma21).'</td>
			<td align="center">'.$sks_ma21.'</td>
			<td align="center">'.$nilai_ma21.'</td>
		</tr>
';
}
$html .= '
	</table>
</td>	
<td width="4%" style="border-color:#FFF"></td>
<td width="48%">
	<table width="100%" border="1" cellspacing="0" cellpadding="2">
		<tr align="center">
			<td width="20%"><b>Kode</b></td>
			<td width="60%"><b>Mata Kuliah</b></td>
			<td width="10%"><b>sks</b></td>
			<td width="10%"><b>Nilai</b></td>
		</tr>';
$list2="select * from 
(select k.*, rownum rnum from 
(select e.kd_mata_kuliah kode,e.nm_mata_kuliah nama,a.id_mhs,d.kredit_semester sks,a.nilai_huruf nilai,f.nilai_standar_nilai bobot from (
select a.*,row_number() over(partition by a.id_mhs,e.nm_mata_kuliah order by nilai_huruf) rangking
from pengambilan_mk a 
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
where a.nilai_huruf<'E' and a.nilai_huruf is not null and a.status_apv_pengambilan_mk=1 and a.status_hapus=0
) a
left join kurikulum_mk d on a.id_kurikulum_mk=d.id_kurikulum_mk
left join mata_kuliah e on d.id_mata_kuliah=e.id_mata_kuliah
left join standar_nilai f on a.nilai_huruf=f.nm_standar_nilai
left join mahasiswa m on a.id_mhs=m.id_mhs
where rangking=1 and m.nim_mhs='".$nim_mhs."'
order by e.nm_mata_kuliah) k
)
where rnum>$panjang_mk";
$result22 = $db->Query($list2)or die("salah kueri list2 ");
while($r22 = $db->FetchRow()) {
	$kode_ma22 = $r22[0];
	$nama_ma22 = $r22[1];
	$sks_ma22 = $r22[3];
	$nilai_ma22 = $r22[4];
	$bobot_ma22 = $r22[5];

$html .= '		
		<tr>
			<td>'.$kode_ma22.'</td>
			<td>'.strtoupper($nama_ma22).'</td>
			<td align="center">'.$sks_ma22.'</td>
			<td align="center">'.$nilai_ma22.'</td>
		</tr>
';
}
$html .= '		
	</table>
</td>	
</tr>
</table>
';

$html .= '<br /><br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td rowspan="10" width="55%">			
			<table width="30%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2"><br><br>Keterangan</td>
			</tr>
			<tr>
				<td align="center" style="border:1px solid black;"><b>Nilai Huruf</b></td>
				<td align="center" style="border:1px solid black;"><b>Bobot</b></td>
			</tr>
			<tr>
				<td align="center" style="border:1px solid black;"><em>A</em></td>
				<td align="center" style="border:1px solid black;"><em>4</em></td>
			</tr>
			<tr>
				<td align="center" style="border:1px solid black;"><em>AB</em></td>
				<td align="center" style="border:1px solid black;"><em>3.5</em></td>
			</tr>
			<tr>
				<td align="center" style="border:1px solid black;"><em>B</em></td>
				<td align="center" style="border:1px solid black;"><em>3</em></td>
			</tr>
			<tr>
				<td align="center" style="border:1px solid black;"><em>BC</em></td>
				<td align="center" style="border:1px solid black;"><em>2.5</em></td>
			</tr>
			<tr>
				<td align="center" style="border:1px solid black;"><em>C</em></td>
				<td align="center" style="border:1px solid black;"><em>2</em></td>
			</tr>
			<tr>
				<td align="center" style="border:1px solid black;"><em>D</em></td>
				<td align="center" style="border:1px solid black;"><em>1</em></td>
			</tr>
			<tr>
				<td align="center" style="border:1px solid black;"><em>E</em></td>
				<td align="center" style="border:1px solid black;"><em>0</em></td>
			</tr>
			</table>
		</td>
		<td></td>
	</tr>
	<tr>
		<td><font size="11">Surabaya, '.date("d", strtotime($tgl_lls)).' '.$bulan[date("m", strtotime($tgl_lls))].' '.date("Y", strtotime($tgl_lls)).'</font></td>
	</tr>
	<tr>';
	if($idfak == 9){
$html .= '<td><font size="11">Direktur</font></td>';
	}else{
$html .= '<td><font size="11">Dekan</font></td>';
	}
	
$html .= '
	</tr>
	<tr>
		<td></td>
	</tr>
	<tr>
		<td></td>
	</tr>
	<tr>
		<td></td>
	</tr>
	<tr>
		<td></td>
	</tr>
	<tr>
		<td><font size="11">'.$nm_dkn.'</font></td>
	</tr>
	<tr>
		<td><font size="11">NIP. '.$nip_dkn.'</font></td>
	</tr>
</table>
';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('TRANSKRIP AKADEMIK '.strtoupper($nim_mhs).'.pdf', 'I');

}
?>