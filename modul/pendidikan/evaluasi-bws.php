<?php
error_reporting (E_ALL & ~E_NOTICE);
include 'config.php';
include 'class/evaluasi.class.php';

$eval = new evaluasi($db);
$id_pengguna= $user->ID_PENGGUNA; 

$smarty->assign('fakultas', $eval->fakultas());
$smarty->assign('semester', $eval->semester());
$smarty->assign('jenjang', $eval->jenjang());
$smarty->assign('status', $eval->status_rekomendasi_bws());
$smarty->assign('status_penetapan', $eval->status_penetapan());


if($_POST['mode'] == 'insert'){
	
	$no = $_POST['no'];
	$tgl = date('d-m-Y');
	for($i=2; $i<=$no; $i++){
		$id_mhs = $_POST['id_mhs'.$i];
		$rekomendasi = $_POST['rekomendasi'.$i];
		$penetapan = $_POST['penetapan'.$i];
		$tgl_rekomendasi = $_POST['tgl_rekomendasi'.$i];
		$tgl_penetapan = $_POST['tgl_penetapan'.$i];
		$keterangan = $_POST['keterangan'.$i];
		$sks = $_POST['sks'.$i];
		$ipk = $_POST['ipk'.$i];
		$piutang = $_POST['piutang'.$i];
		$status_terkini = $_POST['status_terkini'.$i];
		
		if($tgl_rekomendasi != '' and $rekomendasi != ''){
				
				$db->Query("SELECT REKOMENDASI_STATUS FROM EVALUASI_STUDI 
							WHERE IS_AKTIF = 1 AND ID_MHS = '$id_mhs' 
							AND ID_SEMESTER = '$_POST[semester]' AND JENIS_EVALUASI = 1");
				$cek = $db->FetchAssoc();
				
				$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '$_POST[semester]'");
				$semester_aktif = $db->FetchAssoc();
				
				if($cek['REKOMENDASI_STATUS'] != '$rekomendasi'){
				
				$db->Query("UPDATE EVALUASI_STUDI SET IS_AKTIF = 0 WHERE ID_MHS = '$id_mhs' AND ID_SEMESTER = '$_POST[semester]' AND JENIS_EVALUASI = 1");
				
				$db->Query("INSERT INTO EVALUASI_STUDI (ID_MHS, ID_SEMESTER, REKOMENDASI_STATUS, TGL_REKOMENDASI_STATUS, 
							  KETERANGAN, JENIS_EVALUASI, SKS_DIPEROLEH, IPK_EVALUASI, STATUS_TERKINI, ID_PENGGUNA, WAKTU_UBAH, IS_AKTIF, PIUTANG) 
							  VALUES 
							 ('$id_mhs', '$_POST[semester]', '$rekomendasi', to_date('$tgl_rekomendasi', 'DD-MM-YYYY'), 
							  '$keterangan', 1, '$sks', '$ipk', '$status_terkini', '$id_pengguna', to_date('$tgl', 'DD-MM-YYYY'), 1, '$piutang')");
				
				$keterangan = "Evaluasi Batas Waktu Studi " . $semester_aktif['TAHUN_AJARAN'] . " " . $semester_aktif['NM_SEMESTER'];
				
				$db->Query("SELECT COUNT(*) AS CEK FROM CEKAL_PEMBAYARAN 
							WHERE STATUS_CEKAL = 1 AND ID_SEMESTER = '$_POST[semester]' AND ID_MHS = '$id_mhs' AND JENIS_CEKAL = 1");
				$cek = $db->FetchAssoc();
				
				if($cek['CEK'] < 1){
				$db->Query("INSERT INTO CEKAL_PEMBAYARAN (ID_MHS, ID_PENGGUNA, ID_SEMESTER, STATUS_CEKAL, TGL_UBAH, KETERANGAN, STATUS_CEKAL_FAKULTAS, JENIS_CEKAL)
					VALUES ('$id_mhs','$id_pengguna','$_POST[semester]','1',to_date('$tgl', 'DD-MM-YYYY'), '$keterangan', 1, 1)");
        		
				$db->Query("UPDATE MAHASISWA SET STATUS_CEKAL = 1 WHERE ID_MHS = '$id_mhs'");
				}
				
				}
			
		}

	}

}


if(isset($_REQUEST['jenjang']) and isset($_REQUEST['semester'])){

	$smarty->assign('evaluasi', $db->QueryToArray($eval->evaluasi_bws($_REQUEST['fakultas'], $_REQUEST['jenjang'], $_REQUEST['prodi'], $_REQUEST['semester'])));
	
}

if(!isset($_REQUEST['semester'])){
	$db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True'");
	$semester_aktif = $db->FetchAssoc();
	$smarty->assign('semester_aktif', $semester_aktif['ID_SEMESTER']);
}


$smarty->display("evaluasi/bws/evaluasi-bws.tpl");
?>