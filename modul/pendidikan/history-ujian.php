<?php
include('config.php');

$nim_mhs = get('nim_mhs', 0);

$mahasiswa_table= new MAHASISWA_TABLE($db);
$pengguna_table = new PENGGUNA_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);
$mhs_status_table = new MHS_STATUS_TABLE($db);
$semester_table = new SEMESTER_TABLE($db);
$pengambilan_mk_table = new PENGAMBILAN_MK_TABLE($db);
$kelas_mk_table = new KELAS_MK_TABLE($db);
$mata_kuliah_table = new MATA_KULIAH_TABLE($db);
$ujian_mk_table = new UJIAN_MK_TABLE($db);
$mahasiswa_found = false;

if ($nim_mhs != 0)
{
    $mahasiswa_set = $mahasiswa_table->SelectWhere("NIM_MHS = '{$nim_mhs}'");
    if ($mahasiswa_set->Count() > 0)
    {
        // Mendapatkan data mahasiswa
        $mahasiswa = $mahasiswa_set->Get(0);
        $pengguna_table->FillMahasiswa($mahasiswa);
        $program_studi_table->FillMahasiswa($mahasiswa);
        $mahasiswa_found = true;
        
        // Mendapatkan data status mahasiswa per semester
        $mahasiswa->MHS_STATUSs = $mhs_status_table->SelectCriteria("
            JOIN SEMESTER S ON S.ID_SEMESTER = MHS_STATUS.ID_SEMESTER
            WHERE ID_MHS = {$mahasiswa->ID_MHS}
            ORDER BY S.THN_AKADEMIK_SEMESTER, S.NM_SEMESTER");
        $semester_table->FillMhsStatusSet($mahasiswa->MHS_STATUSs);
        
        // Mendapatkan data pengambilan MK
        for ($i = 0; $i < $mahasiswa->MHS_STATUSs->Count(); $i++)
        {
            $semester = $mahasiswa->MHS_STATUSs->Get($i)->SEMESTER;
            $semester->PENGAMBILAN_MKs = $pengambilan_mk_table->SelectCriteria("
                WHERE ID_MHS = {$mahasiswa->ID_MHS} AND ID_SEMESTER = {$semester->ID_SEMESTER} AND
                STATUS_APV_PENGAMBILAN_MK=1 AND STATUS_PENGAMBILAN_MK BETWEEN 1 AND 3");
            $kelas_mk_table->FillPengambilanMkSet($semester->PENGAMBILAN_MKs);
            
            // Mendapatkan info mata kuliah pada pengambilan MK
            for ($j = 0; $j < $semester->PENGAMBILAN_MKs->Count(); $j++)
            {
                $kelas_mk = $semester->PENGAMBILAN_MKs->Get($j)->KELAS_MK;
                $mata_kuliah_table->FillKelasMk($kelas_mk);
                $kelas_mk->TGL_UTS = $ujian_mk_table->SelectCriteria("WHERE ID_KELAS_MK = {$kelas_mk->ID_KELAS_MK} AND NM_UJIAN_MK = 'UTS'")->Get(0)->TGL_UJIAN;
                $kelas_mk->TGL_UAS = $ujian_mk_table->SelectCriteria("WHERE ID_KELAS_MK = {$kelas_mk->ID_KELAS_MK} AND NM_UJIAN_MK = 'UAS'")->Get(0)->TGL_UJIAN;
            }
        }
        
        $smarty->assign('mahasiswa', $mahasiswa);
    }
}

$smarty->assign('mahasiswa_found', $mahasiswa_found);
$smarty->display('history/ujian.tpl');
?>
