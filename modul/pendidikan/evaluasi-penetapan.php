<?php
error_reporting (E_ALL & ~E_NOTICE);
include 'config.php';
include 'class/evaluasi.class.php';

$eval = new evaluasi($db);
$id_pengguna= $user->ID_PENGGUNA; 

$smarty->assign('fakultas', $eval->fakultas());
$smarty->assign('semester', $eval->semester());
$smarty->assign('jenjang', $eval->jenjang_penetapan());
$smarty->assign('status_penetapan', $eval->status_penetapan());

if($_POST['mode'] == 'insert'){
	
	$no = $_POST['no'];
	$tgl = date('d-m-Y');
	for($i=2; $i<=$no; $i++){
		$id_mhs = $_POST['id_mhs'.$i];
		$penetapan = $_POST['penetapan'.$i];
		$tgl_sk = $_POST['tgl_sk'.$i];
		$sk = $_POST['sk'.$i];
		$keterangan = $_POST['keterangan'.$i];
		$sks = $_POST['sks'.$i];
		$ipk = $_POST['ipk'.$i];
		$piutang = $_POST['piutang'.$i];
		$status_terkini = $_POST['status_terkini'.$i];
		

			
				$db->Query("SELECT PENETAPAN_STATUS FROM EVALUASI_STUDI 
							WHERE IS_AKTIF = 1 AND ID_MHS = '$id_mhs' 
							AND ID_SEMESTER = '$_POST[semester]' AND JENIS_EVALUASI = 4");
				$cek = $db->FetchAssoc();
				
				if(($cek['PENETAPAN_STATUS'] != $penetapan) or ($cek['PENETAPAN_STATUS'] == '' and $penetapan != '')){
					
					//echo $cek['PENETAPAN_STATUS'] . ' != ' . $penetapan . ' - ' . $id_mhs. '<br />';
					
					$db->Query("UPDATE EVALUASI_STUDI SET IS_AKTIF = 0 WHERE ID_MHS = '$id_mhs' AND ID_SEMESTER = '$_POST[semester]' AND JENIS_EVALUASI = 4");
					
					$db->Query("INSERT INTO EVALUASI_STUDI (ID_MHS, ID_SEMESTER, PENETAPAN_STATUS, NO_SK_EVALUASI, TGL_PENETAPAN_STATUS,
								  TGL_SK_EVALUASI, KETERANGAN, JENIS_EVALUASI, SKS_DIPEROLEH, IPK_EVALUASI, STATUS_TERKINI, ID_PENGGUNA, 
								  WAKTU_UBAH, IS_AKTIF, PIUTANG)
								  VALUES 
								 ('$id_mhs', '$_POST[semester]', '$penetapan', '$sk', to_date('$tgl', 'DD-MM-YYYY'), 
								  to_date('$tgl_sk', 'DD-MM-YYYY'), '$keterangan', 4, '$sks', '$ipk', '$status_terkini', '$id_pengguna',
								  to_date('$tgl', 'DD-MM-YYYY'), 1, '$piutang')");
				}

	}

}


if(isset($_REQUEST['fakultas'])){
$smarty->assign('evaluasi', $db->QueryToArray($eval->evaluasi_penetapan($_REQUEST['fakultas'], $_REQUEST['jenjang'], $_REQUEST['prodi'], $_REQUEST['semester'], $_REQUEST['status_penetapan'])));
}



if(!isset($_REQUEST['semester'])){
	$db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True'");
	$semester_aktif = $db->FetchAssoc();
	$smarty->assign('semester_aktif', $semester_aktif['ID_SEMESTER']);
}

$smarty->display("evaluasi/penetapan/evaluasi-penetapan.tpl");
?>