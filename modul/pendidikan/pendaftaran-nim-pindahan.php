<?php
include('config.php');
include 'class/report_mahasiswa.class.php';
$rm = new report_mahasiswa($db);
$id_pengguna= $user->ID_PENGGUNA; 

		$smarty->assign('data_fakultas', $rm->get_fakultas());
		$smarty->assign('data_jalur', $rm->get_jalur());
		$smarty->assign('data_jenjang', $rm->get_jenjang());
		$smarty->assign('data_semester', $rm->get_semester());
		$smarty->assign('data_semester_aktif', $rm->get_semester_aktif());
		
		if(isset($_POST['generate'])){
	
			$smarty->assign('data_program_studi', $rm->get_program_studi_by_fakultas_jenjang($_POST['fakultas'], $_POST['jenjang']));
			$smarty->assign('fakultas', $_POST['fakultas']);
			$smarty->assign('jenjang', $_POST['jenjang']);
			$smarty->assign('program_studi', $_POST['program_studi']);
			$smarty->assign('jalur', $_POST['jalur']);
			$smarty->assign('semester', $_POST['semester']);
			$smarty->assign('nama', $_POST['nama']);
			$smarty->assign('gelar_depan', $_POST['gelar_depan']);
			$smarty->assign('gelar_belakang', $_POST['gelar_belakang']);
			$smarty->assign('tgl_lahir', $_POST['tgl_lahir']);
			$smarty->assign('generate', $_POST['generate']);
						
			$nim_baru = $db->QueryToArray("select t.id_program_studi, prodi, nim_terakhir, trim(to_char(nim_terakhir+1,'099999999999')) nim_baru 
			from (
				select ps.id_program_studi, substr(nm_jenjang,1,2)||' '||ps.nm_program_studi prodi,
					(select max(nim_mhs) from mahasiswa m 
					where m.id_program_studi = ps.id_program_studi 
					and m.thn_angkatan_mhs in (select thn_akademik_semester from semester where id_semester = '$_POST[semester]')) nim_terakhir
				from program_studi ps
				join jenjang j on j.id_jenjang = ps.id_jenjang
			) t
			where t.id_program_studi = '$_POST[program_studi]'");	
			$smarty->assign('nim_baru', $nim_baru);
			
						
			$db->beginTransaction();
			
			//INSERT PENGGUNA
			$pass = $user->Encrypt($nim_baru[0]['NIM_BARU']);
			$nama =  str_replace("'", "''", $_POST['nama']);
			// menyiapkan data password utk disimpan ke db
				$password_hash = sha1($nim_baru[0]['NIM_BARU']);
				$password_encrypted = $user->PublicKeyEncrypt($nim_baru[0]['NIM_BARU']);
				$last_time_password = date('d-M-Y');
			
			$db->Query("INSERT INTO PENGGUNA (USERNAME, NM_PENGGUNA, ID_ROLE, JOIN_TABLE, SE1, GELAR_DEPAN, GELAR_BELAKANG, TGL_LAHIR_PENGGUNA, 
						password_hash, password_encrypted, last_time_password, password_hash_temp, password_hash_temp_expired, password_must_change) 
						VALUES ('".$nim_baru[0]['NIM_BARU']."', UPPER('$nama'), '3', '3', '$pass', '$_POST[gelar_depan]', '$_POST[gelar_belakang]', TO_DATE('$_POST[tgl_lahir]', 'DD-MM-YYYY'),
						'{$password_hash}', '{$password_encrypted}', '{$last_time_password}', null, null, 1)") or die('gagal 1');
			
			
			$pengguna = $db->QueryToArray("SELECT * FROM PENGGUNA WHERE USERNAME = '".$nim_baru[0]['NIM_BARU']."'");
			$semester = $db->QueryToArray("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '$_POST[semester]'");	
			$tgl = date('Y-m-d');
			$nim_bank = substr($nim_baru[0]['NIM_BARU'], 0, 12);
								
			//INSERT MAHASISWA
			if($_POST['jalur'] == 4){
				$status = 'AJ';
			}else{
				$status = 'R';
			}
			$db->Query("INSERT INTO MAHASISWA (ID_PENGGUNA, ID_PROGRAM_STUDI, NIM_MHS, THN_ANGKATAN_MHS, 
						TGL_TERDAFTAR_MHS, STATUS_CEKAL, NIM_BANK, STATUS_AKADEMIK_MHS, 
						STATUS, ID_SEMESTER_MASUK, ID_KEBANGSAAN)
				VALUES ('".$pengguna[0]['ID_PENGGUNA']."', '$_POST[program_studi]', '".$nim_baru[0]['NIM_BARU']."', 
				'".$semester[0]['THN_AKADEMIK_SEMESTER']."', to_date('$tgl', 'YYYY-MM-DD'), '0', '$nim_bank', '1', 
				'".$status."', '$_POST[semester]', 114)") or die('gagal 2');
		
			
			$mhs_baru = $db->QueryToArray("SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS = '".$nim_baru[0]['NIM_BARU']."'");
			
			//INSERT ADMISI
			$db->Query("INSERT INTO ADMISI (ID_MHS, STATUS_AKD_MHS, TGL_USULAN, TGL_APV, ID_SEMESTER, STATUS_APV, ID_PENGGUNA)
			VALUES ('".$mhs_baru[0]['ID_MHS']."', '1', to_date('$tgl','YYYY-MM-DD'), to_date('$tgl','YYYY-MM-DD'), '$_POST[semester]', '1', $id_pengguna)")  or die('gagal 3');
			
			//INSERT JALUR MAHASISWA
			$db->Query("INSERT INTO JALUR_MAHASISWA (ID_MHS, ID_PROGRAM_STUDI, ID_JALUR, ID_SEMESTER, NIM_MHS, ID_JALUR_AKTIF)
			VALUES ('".$mhs_baru[0]['ID_MHS']."', '$_POST[program_studi]', '$_POST[jalur]', '$_POST[semester]', '".$nim_baru[0]['NIM_BARU']."', '1')")  or die('gagal 4');
			
			//INSERT PEMBAYARAN
			if($_POST['set_biaya'] != ''){
			$set_biaya = $db->QueryToArray("SELECT * FROM DETAIL_BIAYA  WHERE ID_BIAYA_KULIAH = '$_POST[set_biaya]'");
				foreach($set_biaya as $biaya){
					$db->Query("INSERT INTO PEMBAYARAN (ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, BESAR_BIAYA, DENDA_BIAYA, 
								IS_TAGIH, IS_TARIK, ID_STATUS_PEMBAYARAN)
							VALUES ('".$mhs_baru[0]['ID_MHS']."', '$_POST[semester]', '$biaya[ID_DETAIL_BIAYA]', '$biaya[BESAR_BIAYA]', '0', 
							'Y', '0', '2')") 
							 or die('gagal 6');
				}	
				
				$db->Query("INSERT INTO BIAYA_KULIAH_MHS (ID_BIAYA_KULIAH, ID_MHS) VALUES ($_POST[set_biaya], '".$mhs_baru[0]['ID_MHS']."')") 
					or die('gagal 8');
				
				$db->Query("UPDATE MAHASISWA 
						SET ID_KELOMPOK_BIAYA = (SELECT ID_KELOMPOK_BIAYA FROM BIAYA_KULIAH WHERE ID_BIAYA_KULIAH = '$_POST[set_biaya]') 
						WHERE ID_MHS = '".$mhs_baru[0]['ID_MHS']."'") or die('gagal 7');
			
			}
			
			//INSERT FINGERPRINT
			$huruf_depan = strtoupper(substr($nama, 0, 1));
			$finger = $db->Query("INSERT INTO FINGERPRINT_MAHASISWA (NIM_MHS, HURUF_DEPAN)
									VALUES ('".$nim_baru[0]['NIM_BARU']."', '$huruf_depan')") or die('gagal 5');
			
			if($finger){
				echo "Berhasil";
				$db->commit();
			}else{
				$db->rollBack();
			}
						
		}


$smarty->display("pendaftaran/nim_pindahan/pendaftaran-nim-pindahan.tpl");
?>
