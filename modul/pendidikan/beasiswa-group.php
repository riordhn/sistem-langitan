<?php

include 'config.php';
include 'class/beasiswa.class.php';

$b = new beasiswa($db);

if (isset($_GET['mode'])) {
    if (get('mode') == 'add_group') {
        $smarty->display("beasiswa/add-group-beasiswa.tpl");
    } elseif (get('mode') == 'edit_group') {
        $smarty->assign('data_group_beasiswa', $b->get_group_beasiswa(get('id')));
        $smarty->display("beasiswa/edit-group-beasiswa.tpl");
    } elseif (get('mode') == 'delete_group') {
        $smarty->assign('data_group_beasiswa', $b->get_group_beasiswa(get('id')));
        $smarty->display("beasiswa/delete-group-beasiswa.tpl");
    }
} else {
    if (isset($_POST)) {
        if (post('mode') == 'add_group') {
            $b->add_group_beasiswa(strip_tags(post('nama')));
        } else if (post('mode') == 'edit_group') {
            $b->edit_group_beasiswa(post('id'), strip_tags(post('nama')));
        } else if (post('mode') == 'delete_group') {
            $b->delete_group_beasiswa(post('id'));
        }
    }
    $smarty->assign("data_group_beasiswa", $b->load_group_beasiswa());
    $smarty->assign('data_beasiswa', $b->load_beasiswa());
    $smarty->display("beasiswa/group-beasiswa.tpl");
}
?>