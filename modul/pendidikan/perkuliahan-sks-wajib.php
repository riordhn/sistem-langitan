<?php
error_reporting (E_ALL & ~E_NOTICE);
include 'config.php';

$mode = $_REQUEST['mode'];

if($mode == 'insert'){

	$db->Query("INSERT INTO SKS_WAJIB (SKS_DIWAJIBKAN, ID_PROGRAM_STUDI, ALIH_JALUR)
				VALUES ('$_POST[sks]', '$_POST[program_studi]', '$_POST[jenis]')");
	
}elseif($mode == 'edit'){

	$db->Query("SELECT * FROM SKS_WAJIB 
							JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = SKS_WAJIB.ID_PROGRAM_STUDI 
							JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS AND FAKULTAS.ID_PERGURUAN_TINGGI = {$PT->ID_PERGURUAN_TINGGI}
							JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
							WHERE ID_SKS_DIWAJIBKAN = '$_GET[id]'");
	$edit = $db->FetchAssoc();
	$smarty->assign('edit', $edit);
	
}elseif($mode == 'update'){

	$db->Query("UPDATE SKS_WAJIB SET SKS_DIWAJIBKAN = '$_POST[sks]', ID_PROGRAM_STUDI = '$_POST[program_studi]', ALIH_JALUR = '$_POST[jenis]'
	 			WHERE ID_SKS_DIWAJIBKAN = '$_POST[id]'");
	
}elseif($mode == 'hapus'){
	
		$db->Query("DELETE SKS_WAJIB WHERE ID_SKS_DIWAJIBKAN = '$_GET[id]'");

}
$sks = $db->QueryToArray("SELECT * FROM SKS_WAJIB 
							JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = SKS_WAJIB.ID_PROGRAM_STUDI 
							JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
							JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
							WHERE FAKULTAS.ID_PERGURUAN_TINGGI = {$PT->ID_PERGURUAN_TINGGI}
							ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI ASC");
$smarty->assign('sks', $sks);

$jenjang = $db->QueryToArray("SELECT * FROM JENJANG ORDER BY NM_JENJANG");
$smarty->assign('jenjang', $jenjang);

$fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = {$PT->ID_PERGURUAN_TINGGI} ORDER BY ID_FAKULTAS ");
$smarty->assign('fakultas', $fakultas);

$prodi = $db->QueryToArray(
	"SELECT ID_PROGRAM_STUDI, NM_JENJANG||' '||NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI FROM PROGRAM_STUDI 
	JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
	WHERE ID_FAKULTAS IN (SELECT ID_FAKULTAS FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = {$PT->ID_PERGURUAN_TINGGI})");
$smarty->assign('prodi', $prodi);


$smarty->display("perkuliahan/perkuliahan-sks-wajib.tpl");
?>