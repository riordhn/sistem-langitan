<?php
include('config.php');
include '../pendidikan/class/wisuda.class.php';


$wd = new wisuda($db);

$periode = $db->QueryToArray("SELECT * FROM TARIF_WISUDA WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY ID_TARIF_WISUDA DESC");
$smarty->assign('periode', $periode);
$fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY ID_FAKULTAS ASC");
$smarty->assign('fakultas', $fakultas);

if(isset($_POST['no'])){

	$no = $_POST['no'];
	for($i=2;$i<=$no;$i++){
		$cek = $_POST['cek'.$i];
		$id = $_POST['id'.$i];
		$tgl_pengajuan = $_POST['tgl_pengajuan'.$i];
		$tgl = date('d-m-Y');
		//echo $cek;
		if($cek==1 and $tgl_pengajuan == ''){
			$db->Query("UPDATE PENGAJUAN_WISUDA SET PENGAJUAN_CETAK_IJASAH =  TO_DATE('$tgl', 'DD-MM-YYYY') WHERE ID_PENGAJUAN_WISUDA = '$id'");
		}elseif($cek!=1){
			$db->Query("UPDATE PENGAJUAN_WISUDA SET PENGAJUAN_CETAK_IJASAH =  '' WHERE ID_PENGAJUAN_WISUDA = '$id'");
		}
	}
	

}


if(isset($_POST['periode']) and $_POST['fakultas'] != ''){

	$tgl_cetak = $db->QueryToArray("SELECT TO_CHAR(PENGAJUAN_CETAK_IJASAH, 'DD-MM-YYYY') AS PENGAJUAN_CETAK_IJASAH 
									FROM PENGAJUAN_WISUDA 
									JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA 
									JOIN MAHASISWA ON MAHASISWA.ID_MHS = PENGAJUAN_WISUDA.ID_MHS
									JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
									WHERE ID_TARIF_WISUDA = '$_POST[periode]' AND PROGRAM_STUDI.ID_FAKULTAS = '$_POST[fakultas]' AND PENGAJUAN_CETAK_IJASAH IS NOT NULL
									GROUP BY PENGAJUAN_CETAK_IJASAH
									ORDER BY PENGAJUAN_CETAK_IJASAH DESC");
	$smarty->assign('tgl_cetak', $tgl_cetak);
	$smarty->assign('pengajuan', $wd->wisuda_yudisium($_POST['fakultas'], 3, $_POST['periode']));		
}


$smarty->display("wisuda/wisuda-pengajuan-cetak.tpl");
?>
