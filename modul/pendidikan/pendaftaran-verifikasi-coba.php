<?php
include('config.php');
include 'class/CalonMahasiswa.class.php';

$CalonMahasiswa = new CalonMahasiswa($db);

$mode = get('mode', 'view');
$no_ujian = get('no_ujian', '');

if ($request_method == 'POST')
{

	$id_c_mhs = $_POST['id_c_mhs'];	

		
	if($_POST['mode'] == 'save_s1_aj'){
		
		
		$jenis_berkas = array('ijazah');
        
        foreach ($jenis_berkas as $berkas)
        {
            if ($_FILES["file_{$berkas}"]['size'] <= 5242880) 
            {
                $destination = "/var/www/html/files/snmptn/pdf-berkas/{$id_c_mhs}_{$berkas}.pdf";
                if (move_uploaded_file($_FILES["file_{$berkas}"]['tmp_name'], $destination))
                    $CalonMahasiswa->UpdateFile($id_c_mhs, "file_{$berkas}", '1');
            }
        }

	}
	
	
	if($_POST['mode'] == 'save_d3'){		
		
		$jenis_berkas = array('ijazah', 'skhun', 'akte', 'kk');
        
        foreach ($jenis_berkas as $berkas)
        {
            if ($_FILES["file_{$berkas}"]['size'] <= 5242880) 
            {
                $destination = "/var/www/html/files/snmptn/pdf-berkas/{$id_c_mhs}_{$berkas}.pdf";
                if (move_uploaded_file($_FILES["file_{$berkas}"]['tmp_name'], $destination))
                    $CalonMahasiswa->UpdateFile($id_c_mhs, "file_{$berkas}", '1');
            }
        }        

	}


	if($_POST['mode'] == 'save_s1'){
		
		$jenis_berkas = array('ijazah', 'skhun', 'akte', 'kk');
        
        foreach ($jenis_berkas as $berkas)
        {
            if ($_FILES["file_{$berkas}"]['size'] <= 5242880) 
            {
                $destination = "/var/www/html/files/snmptn/pdf-berkas/{$id_c_mhs}_{$berkas}.pdf";
                if (move_uploaded_file($_FILES["file_{$berkas}"]['tmp_name'], $destination))
                    $CalonMahasiswa->UpdateFile($id_c_mhs, "file_{$berkas}", '1');
            }
        }
		
	}
	
    if (in_array(post('mode'), array('save_s2', 'save_s3', 'save_pr', 'save_sp')))
    {
        
		$jenis_berkas = array('ijazah');
        
        foreach ($jenis_berkas as $berkas)
        {
            if ($_FILES["file_{$berkas}"]['size'] <= 5242880) 
            {
                $destination = "/var/www/html/files/snmptn/pdf-berkas/{$id_c_mhs}_{$berkas}.pdf";
                if (move_uploaded_file($_FILES["file_{$berkas}"]['tmp_name'], $destination))
                    $CalonMahasiswa->UpdateFile($id_c_mhs, "file_{$berkas}", '1');
            }
        }
        
    }
	
	
        $result = $CalonMahasiswa->Update($id_c_mhs, $_POST, $user->ID_PENGGUNA);
	
	    if ($result)
            $smarty->assign('updated', "Data berhasil diupdate");
        else
            $smarty->assign('updated', "Data gagal diupdate");
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($no_ujian != '')
    {
		
        $db->Query("select * from calon_mahasiswa_baru 
					where no_ujian = '{$no_ujian}'");
        $cmb1 = $db->FetchAssoc();

        if ($cmb1)
        {
            // data pembayaran
            $db->Query("select count(*) as jumlah from pembayaran_cmhs 
						left join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = pembayaran_cmhs.id_c_mhs
						where (tgl_bayar is not null or status_bidik_misi = 1) and pembayaran_cmhs.id_c_mhs = {$cmb1['ID_C_MHS']}");
            $pembayaran = $db->FetchAssoc();
            $smarty->assign('pembayaran', $pembayaran);
            
            // calon mahasiswa data
            $db->Query("select * from calon_mahasiswa_data where id_c_mhs = {$cmb1['ID_C_MHS']}");
            $cmd = $db->FetchAssoc();
            $smarty->assign('cmd', $cmd);
            
            // data jenjang
            $db->Query("select id_jenjang, id_jalur from penerimaan where id_penerimaan = {$cmb1['ID_PENERIMAAN']}");
            $penerimaan = $db->FetchAssoc();
                
            
            if ($penerimaan['ID_JENJANG'] == 1 and $penerimaan['ID_JALUR'] == 4){ $smarty->assign('page_view', 'pendaftaran/verifikasi/s1-alih.tpl'); }
            if ($penerimaan['ID_JENJANG'] == 1){ $smarty->assign('page_view', 'pendaftaran/verifikasi_coba/s1.tpl'); }
            if ($penerimaan['ID_JENJANG'] == 2) { $smarty->assign('page_view', 'pendaftaran/verifikasi/s2.tpl'); }
            if ($penerimaan['ID_JENJANG'] == 3) { $smarty->assign('page_view', 'pendaftaran/verifikasi/s3.tpl'); }
            if ($penerimaan['ID_JENJANG'] == 5) { $smarty->assign('page_view', 'pendaftaran/verifikasi/d3.tpl'); }
            if ($penerimaan['ID_JENJANG'] == 9) { $smarty->assign('page_view', 'pendaftaran/verifikasi/pr.tpl'); }
            if ($penerimaan['ID_JENJANG'] == 10) { $smarty->assign('page_view', 'pendaftaran/verifikasi/sp.tpl'); }
            
            // data pasca
            if (in_array($penerimaan['ID_JENJANG'], array(2, 3, 5, 9, 10)))  // s1-alih, s2, s3, profesi, sp
            {
                $db->Query("select * from calon_mahasiswa_pasca where id_c_mhs = {$cmb1['ID_C_MHS']}");
                $cmp = $db->FetchAssoc();
            }
            
            // mendapatkan program studi, prodi minat, dan kelas pilihannya
            $db->Query("
                select nm_fakultas, nm_program_studi, nm_jenjang, nm_prodi_minat, cmp.kelas_pilihan, pk.nm_prodi_kelas 
				from calon_mahasiswa_baru cmb
                join program_studi ps on ps.id_program_studi = cmb.id_program_studi
                join fakultas f on f.id_fakultas = ps.id_fakultas
                join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
                join jenjang j on j.id_jenjang = p.id_jenjang
                left join calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
                left join prodi_minat pm on pm.id_prodi_minat = cmp.id_prodi_minat
                left join prodi_kelas pk on (pk.id_program_studi = ps.id_program_studi and pk.singkatan = cmp.kelas_pilihan)
                where cmb.id_c_mhs = {$cmb1['ID_C_MHS']}");
            $ps = $db->FetchAssoc();
            $smarty->assign('ps', $ps);

            // data provinsi set
         	$provinsi_set = $db->QueryToArray("select * from provinsi 
												where id_negara in (select id_negara from provinsi 
												join kota on kota.id_provinsi = provinsi.id_provinsi
												join calon_mahasiswa_baru on calon_mahasiswa_baru.id_kota = kota.id_kota
												where id_c_mhs = '$cmb1[ID_C_MHS]')");
            $smarty->assign('provinsi_set',  $provinsi_set);
			
			
			$kota_set = $db->QueryToArray("select * from kota where id_provinsi in 
									(select id_provinsi from kota left join calon_mahasiswa_baru 
									on calon_mahasiswa_baru.id_kota = kota.id_kota where id_c_mhs = '$cmb1[ID_C_MHS]')");
            $smarty->assign('kota_set',  $kota_set);
			
			//ayah
            $provinsi_ayah_set = $db->QueryToArray("select * from provinsi 
												where id_negara in (select id_negara from provinsi 
												join kota on kota.id_provinsi = provinsi.id_provinsi
												join calon_mahasiswa_ortu on calon_mahasiswa_ortu.id_kota_ayah = kota.id_kota
												where id_c_mhs = '$cmb1[ID_C_MHS]')");
            $smarty->assign('provinsi_ayah_set',  $provinsi_ayah_set);
			
			
			$kota_ayah_set = $db->QueryToArray("select * from kota where id_provinsi in 
									(select id_provinsi from kota left join calon_mahasiswa_ortu 
									on calon_mahasiswa_ortu.id_kota_ayah = kota.id_kota where id_c_mhs = '$cmb1[ID_C_MHS]')");
            $smarty->assign('kota_ayah_set',  $kota_ayah_set);
			
			//ibu
			$provinsi_ibu_set = $db->QueryToArray("select * from provinsi 
												where id_negara in (select id_negara from provinsi 
												join kota on kota.id_provinsi = provinsi.id_provinsi
												join calon_mahasiswa_ortu on calon_mahasiswa_ortu.id_kota_ibu = kota.id_kota
												where id_c_mhs = '$cmb1[ID_C_MHS]')");
            $smarty->assign('provinsi_ibu_set',  $provinsi_ibu_set);
			
			
			$kota_ibu_set = $db->QueryToArray("select * from kota where id_provinsi in 
									(select id_provinsi from kota left join calon_mahasiswa_ortu 
									on calon_mahasiswa_ortu.id_kota_ibu = kota.id_kota where id_c_mhs = '$cmb1[ID_C_MHS]')");
            $smarty->assign('kota_ibu_set',  $kota_ibu_set);
			
			$negara_set = $db->QueryToArray("select * from negara order by nm_negara");
            $smarty->assign('negara_set',  $negara_set);
			
			
             $cmb = $CalonMahasiswa->GetData($cmb1['ID_C_MHS'], true);
			 $smarty->assign('cmb', $cmb);
			 $smarty->assign('cmb1', $cmb1);
             $smarty->assign('cmp', $cmp);
        }
    }

}


	
$smarty->assign('sekloah_set',  $sekloah_set);
$smarty->assign('kewarganegaraan_set',  $CalonMahasiswa->GetListKewarganegaraan());
$smarty->assign('jenis_kelamin_set',    $CalonMahasiswa->GetListJenisKelamin());
$smarty->assign('agama_set',            $CalonMahasiswa->GetListAgama());
$smarty->assign('sumber_biaya_set',     $CalonMahasiswa->GetListSumberBiaya());
$smarty->assign('jurusan_sekolah_set',  $CalonMahasiswa->GetListJurusanSekolah());
$smarty->assign('pendidikan_ortu_set',  $CalonMahasiswa->GetListPendidikanOrtu());
$smarty->assign('pekerjaan_set',        $CalonMahasiswa->GetListPekerjaan());
$smarty->assign('penghasilan_ortu_set', $CalonMahasiswa->GetListPenghasilanOrtu());
$smarty->assign('status_ptn_set',       $CalonMahasiswa->GetListStatusPTN());

		
$smarty->display("pendaftaran/verifikasi_coba/{$mode}.tpl");
?>
