<?php
include '../../config.php';
include('class/date.class.php');
include_once '../../tcpdf/config/lang/ind.php';
include_once '../../tcpdf/tcpdf.php';
		
		$tgl_wisuda = date("Y-m-d", strtotime($_GET['tgl']));
		$tgl_wisuda = tgl_indo($tgl_wisuda);
		
		$hari_wisuda = date("w", strtotime($_GET['tgl']));
		$hari_wisuda = getHari($hari_wisuda);		

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');

$pdf->setPrintHeader(false);
//$pdf->setPrintFooter(true);
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetMargins(14, 5, 4, false);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->startPageGroup();


$data_prgoram_studi = $db->QueryToArray("SELECT * FROM (
									SELECT PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, UPPER(NM_PROGRAM_STUDI) AS NM_PROGRAM_STUDI, 
									UPPER(NM_JENJANG) AS NM_JENJANG, UPPER(NM_FAKULTAS) AS NM_FAK,
									UPPER(NM_JENJANG_IJASAH) AS NM_JENJANG_IJASAH, NM_FAKULTAS, GELAR_PANJANG, GELAR_PENDEK, WISUDAWAN_TERBAIK,
									row_number() over(partition by PROGRAM_STUDI.ID_PROGRAM_STUDI order by WISUDAWAN_TERBAIK) rangking
									FROM PROGRAM_STUDI
									JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
									JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
									JOIN MAHASISWA ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
									JOIN PEMBAYARAN_WISUDA ON PEMBAYARAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
									JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PEMBAYARAN_WISUDA.ID_PERIODE_WISUDA								
									WHERE PROGRAM_STUDI.ID_FAKULTAS = '$_GET[fakultas]' AND PROGRAM_STUDI.ID_JENJANG = '$_GET[jenjang]'
									AND PERIODE_WISUDA.ID_TARIF_WISUDA = '$_GET[periode]' AND ABSENSI_WISUDA  = 1 
									AND TGL_WISUDA = TO_DATE('$_GET[tgl]', 'DD-MM-YYYY') 
									GROUP BY PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, UPPER(NM_PROGRAM_STUDI), UPPER(NM_JENJANG), 
									UPPER(NM_FAKULTAS), UPPER(NM_JENJANG_IJASAH), NM_FAKULTAS, GELAR_PANJANG, GELAR_PENDEK, WISUDAWAN_TERBAIK
									) WHERE rangking = 1
									ORDER BY WISUDAWAN_TERBAIK, NM_JENJANG, NM_PROGRAM_STUDI");
									
			
$index = 1;
foreach ($data_prgoram_studi as $prodi) {
$html = "";	
		$absensi = $db->QueryToArray("SELECT NIM_MHS, UPPER(NM_PENGGUNA) AS NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, 
										UPPER(NM_FAKULTAS) AS NM_FAKULTAS, KETERANGAN_ABSENSI, ABSENSI_WISUDA, 
										TO_CHAR(TGL_WISUDA, 'DD-MM-YYYY') AS TGL_WISUDA, UPPER(NM_JENJANG_IJASAH) AS NM_JENJANG_IJASAH, GELAR_DEPAN, GELAR_BELAKANG
										FROM PEMBAYARAN_WISUDA
										JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS
										JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PEMBAYARAN_WISUDA.ID_PERIODE_WISUDA
										JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								 		JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								 		JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
								 		JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
										WHERE PERIODE_WISUDA.ID_TARIF_WISUDA = '$_GET[periode]' AND ABSENSI_WISUDA  = 1 
										AND TGL_WISUDA = TO_DATE('$_GET[tgl]', 'DD-MM-YYYY') 
										AND PROGRAM_STUDI.ID_PROGRAM_STUDI = '$prodi[ID_PROGRAM_STUDI]'
										ORDER BY WISUDAWAN_TERBAIK, NM_JENJANG, NM_PROGRAM_STUDI, MAHASISWA.NIM_MHS");
	
	
	$pdf->AddPage();
	$pdf->setPageMark();
	
	
$html = '
<style>
    .header { font-size: 16pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 14pt; font-family: serif; margin-top: 0px ;text-align:center; }
    th { 
        text-align: center;
        color: #ffffff;
        background-color: #006600;
        padding:10px;
    }
    td { font-size: 14pt;font-weight: bold; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center" class="header">
            DAFTAR NAMA WISUDAWAN
        </td>
    </tr>
	<tr>
		<td width="100%" align="center" class="header">';
			if($prodi['ID_FAKULTAS'] == '9'){
				$html .= 'PROGRAM ';
			}else{
				$html .= 'FAKULTAS ';
			} 
			$html .= $prodi['NM_FAK'].'
		</td>	
	</tr>
	<tr>
		<td width="100%" align="center" class="header">
			UNIVERSITAS AIRLANGGA
		</td>	
	</tr>
</table>
<p><p/>
<table>
	<tr>
		<td width="45%">Hari</td>
		<td width="2%">:</td>
		<td width="53%">'.$hari_wisuda.'</td>
	</tr>
	<tr>
		<td>Tanggal</td>
		<td>:</td>
		<td>'.$tgl_wisuda.'</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>PROGRAM '.$prodi['NM_JENJANG_IJASAH'].' ('.$prodi['NM_JENJANG'].')<hr/></td>
		<td></td>
		<td></td>
	</tr>
	
	<tr>
		<td colspan="3">Lulusan Program ';
			if($prodi['ID_FAKULTAS'] == '9'){
				$html .= ucwords(strtolower($prodi['NM_JENJANG_IJASAH'])). ' Program ';
			}else{
				if($prodi['NM_JENJANG'] == 'D3'){
					$html .= 'Diploma III Fakultas ';
				}else{
					$html .= ucwords(strtolower($prodi['NM_JENJANG_IJASAH'])).' Fakultas ';
				}
			} 
			$html .= $prodi['NM_FAKULTAS'].'</td>
	</tr>
	<tr>
		<td>PROGRAM STUDI</td>
		<td>:</td>
		<td>'.$prodi['NM_PROGRAM_STUDI'].'</td>
	</tr>
	<tr>
		<td colspan="3">Dengan ';
			if($prodi['NM_JENJANG'] == 'D3'){
				$html .= "Gelar ";
			}else{
				$html .= "Gelar ";
			}	
			$html .= ucwords(strtolower($prodi['GELAR_PANJANG'])).' ('.$prodi['GELAR_PENDEK'].')</td>
	</tr>
</table>
<p></p>
<table width="100%" cellpadding="4" border="0.5">
        <tr>
            <td align="center" width="8%">NO</td>
            <td align="center" width="72%">NAMA MAHASISWA</td>
        </tr>
';
	
	foreach ($absensi as $data) {
		
		$gelar_belakang = $data['GELAR_BELAKANG'];
		$gelar_depan = $data['GELAR_DEPAN'];
	
		if($gelar_belakang <> ''){
			$gelar_belakang = ", " . $gelar_belakang; 
		}
		if($gelar_depan <> ''){
			$gelar_depan = $gelar_depan . " ";
		}
	
		$html .= '
					<tr>
					<td align="center">'.$index.'</td>
					<td>'.$gelar_depan.''.$data['NM_PENGGUNA'].''.$gelar_belakang.'</td>
		</tr>';
		$index++;
	}

$html .= '</table>';

	$pdf->writeHTML($html);
}


//$html = str_replace('{data_mahasiswa}', $data_mahasiswa, $html);

//$pdf->writeHTML($html);

$pdf->Output('urautan_mc_wisuda'.'.pdf', 'I');
?>
