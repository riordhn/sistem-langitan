<?php
error_reporting (E_ALL & ~E_NOTICE);
require('config.php');
require_once ('ociFunction.php');
require('class/date.class.php');


$id_pengguna= $user->ID_PENGGUNA; 
$pesan = "";

if(isset($_POST['no_ijasah']) or isset($_POST['tgl_lulus'])){
	
	$id_fakultas = $_POST['id_fakultas'];
	$kd_ijasah = getvar("SELECT KODE_IJASAH FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI = '$_POST[id_prodi]'");
	
			$kd_jenjang = getvar("SELECT JENJANG_IJASAH FROM PROGRAM_STUDI WHERE ID_PROGRAM_STUDI = '$_POST[id_prodi]'");
			if($kd_jenjang['JENJANG_IJASAH'] == ''){
				$pesan = "Isi kode jenjang di menu Prodi Ijasah";
			}
				
	
			if($pesan == ''){
				$tgl = "''";
				if($_POST['tgl_sk_yudisium'] <> ''){
					$tgl = "to_date('".$_POST['tgl_sk_yudisium']."', 'DD-MM-YYYY')";
				}
				
				if(strlen($id_fakultas) == 1){
					$id_fakultas = "0".$id_fakultas;
				}

				$thn = getvar("SELECT TO_CHAR(TGL_LULUS, 'YYYY') AS TAHUN FROM ADMISI
								WHERE ID_MHS = '$_POST[id_mhs]' AND STATUS_AKD_MHS = 4
								ORDER BY ID_ADMISI DESC");

				$prodi = $kd_ijasah['KODE_IJASAH'];
				$jenjang = $kd_jenjang['JENJANG_IJASAH'];
				$tahun = $thn['TAHUN'];
				
				if($tahun == ''){
					$tahun = substr($_POST['tgl_lulus'], 6, 4);
				}
				
				
				//$row = getvar("SELECT ID_PROGRAM_STUDI FROM MAHASISWA WHERE ID_MHS = '$_POST[id_mhs]'");	
				

				if($prodi == ''){
					$no_ijasah = $_POST['no_ijasah']."/0113/". $id_fakultas . "/". $jenjang . "/" . $tahun;
				}else{
					$no_ijasah = $_POST['no_ijasah']."/0113/". $id_fakultas . "." . $prodi . "/". $jenjang . "/" . $tahun;
				}

				
				
				$row = getvar("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True'");	
				
				
				//UPDATE LULUS
				$nama =  str_replace("'", "''", $_POST['nm_pengguna']);
				
				UpdateData("UPDATE MAHASISWA SET STATUS_AKADEMIK_MHS = '4' WHERE ID_MHS = '$_POST[id_mhs]'");
				
				UpdateData("UPDATE PENGGUNA SET NM_PENGGUNA = '$nama', GELAR_DEPAN = '$_POST[gelar_depan]', 
							GELAR_BELAKANG = '$_POST[gelar_belakang]' WHERE USERNAME = '$_POST[nim]'");
							
				
					$tgl_lulus = $_POST['tgl_lulus'];
					if($tgl_lulus <> ''){
						$tgl_lulus = "to_date('".$tgl_lulus."', 'DD-MM-YYYY')";
					}else{$tgl_lulus = "";}
				
				
				$judul_ta = str_replace("</p>","</br>",$_POST['judul_ta']);
				
				if(isset($_POST['no_ijasah_full']) and $_POST['no_ijasah_full'] != '')
				{
					$no_ijasah = $_POST['no_ijasah_full'];
				}else{
					$no_ijasah = $no_ijasah;
				}
				
				
				UpdateData("UPDATE PENGAJUAN_WISUDA SET NO_IJASAH = '$no_ijasah', SK_YUDISIUM = '$_POST[sk_yudisium]', 
							TGL_SK_YUDISIUM = $tgl, SKS_TOTAL = '$_POST[sks_total]', ELPT = '$_POST[elpt]', IPK = '$_POST[ipk]',
							LAHIR_IJAZAH = '$_POST[ttl]', TGL_LULUS_PENGAJUAN = $tgl_lulus, JUDUL_TA = '$judul_ta'
							WHERE ID_MHS = '$_POST[id_mhs]'");
							
				//INSERT ADMISI
				if($_POST['ijasah_admisi'] == ''){
				$tgl = date('Y-m-d');
								
				InsertData("INSERT INTO ADMISI (ID_MHS, STATUS_AKD_MHS, TGL_USULAN, TGL_APV, ID_SEMESTER, STATUS_APV, ID_PENGGUNA, 
							NO_IJASAH, TGL_LULUS)
							VALUES ('$_POST[id_mhs]', '4', to_date('$tgl', 'YYYY-MM-DD'), to_date('$tgl', 'YYYY-MM-DD'), '$row[ID_SEMESTER]'
							,'1', $id_pengguna, '$no_ijasah', $tgl_lulus)") or die ('error admisi');
				}else{

					UpdateData("UPDATE ADMISI SET TGL_LULUS = $tgl_lulus, NO_IJASAH = '$no_ijasah' 
								WHERE ID_MHS = '$_POST[id_mhs]' AND STATUS_AKD_MHS = '4' AND ID_ADMISI = '$_POST[id_admisi]'");
				}
			}
}



if($_POST['nim']){
	
	$cek = getvar("SELECT COUNT(*) AS JML FROM MAHASISWA 
					JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
					JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
					WHERE NIM_MHS = '$_POST[nim]' AND FAKULTAS.ID_PERGURUAN_TINGGI = '{$id_pt_user}'
					");

	if($cek['JML'] == 1){	
		$nim = $_POST['nim'];
		$pw = getvar("SELECT COUNT(*) AS JML 
				FROM MAHASISWA
				LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
				WHERE NIM_MHS = '$nim' AND (YUDISIUM = 1 OR YUDISIUM = 2)");
		
		if($pw['JML'] >= 1){
			
			$ijasah = getData("SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_JENJANG, PROGRAM_STUDI.ID_FAKULTAS, 
				PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_JENJANG, PENGAJUAN_WISUDA.NO_IJASAH, SK_YUDISIUM, 
				TO_CHAR(TGL_SK_YUDISIUM, 'DD-MM-YYYY') AS TGL_SK_YUDISIUM, TO_CHAR(TGL_LULUS, 'DD-MM-YYYY') AS TGL_LULUS, SKS_TOTAL, ELPT, IPK,
				GELAR_DEPAN, GELAR_BELAKANG, LAHIR_IJAZAH, JUDUL_TA, 
				TO_CHAR(TGL_LAHIR_PENGGUNA, 'YYYY-MM-DD') AS TGL_LAHIR_PENGGUNA, initcap(NM_KOTA) as NM_KOTA, ADMISI.ID_ADMISI
				FROM MAHASISWA
				LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
				LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN ADMISI ON ADMISI.ID_MHS = MAHASISWA.ID_MHS AND TGL_LULUS IS NOT NULL
				LEFT JOIN KOTA ON KOTA.ID_KOTA = LAHIR_KOTA_MHS
				WHERE NIM_MHS = '$nim'
				ORDER BY ID_ADMISI DESC
				");
			$smarty->assign('ijasah', $ijasah);
					$tgl_lahir = tgl_indo($ijasah[0]['TGL_LAHIR_PENGGUNA']);
					$smarty->assign('tgl_lahir', $tgl_lahir);
					$kode_ijasah =  strstr($ijasah[0]['NO_IJASAH'], '/', true);
					$smarty->assign('kode_ijasah', $kode_ijasah);

						
		}else{
			$pesan = "Mahasiswa Belum Yudisium";
		}
	}
	else{
		$pesan = "Mahasiswa Tidak Terdaftar";
	}

}

$smarty->assign('pesan', $pesan);
$smarty->display("wisuda/wisuda-no-draft.tpl");
?>
