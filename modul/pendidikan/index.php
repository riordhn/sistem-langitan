<?php
require_once('config.php');

if ($user->Role() == AUCC_ROLE_PENDIDIKAN)
{

    // data menu dari $user
    $smarty->assign('modul_set', $user->MODULs);

    $smarty->display("index.tpl");
}
else
{
    header("location: /logout.php");
    exit();
}
?>
