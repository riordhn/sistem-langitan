<?php

include 'config.php';
include 'class/report_mahasiswa.class.php';

$id_pt = $id_pt_user;

$rm = new report_mahasiswa($db);

if (get('mode') == 'list_mhs') {
	// ($mode, $fakultas, $program_studi, $tahun_akademik, $status, $jenjang, $jalur, $id_perguruan_tinggi = 0, $bidikmisi = '', $kota_mhs = '', $tahun_lulus = '', $kelamin = '')
    $smarty->assign('data_mahasiswa',$rm->get_mahasiswa_dayat(1, '', get('prodi'), get('angkatan'), '1', '', '', $id_pt_user));
	$smarty->display("mahasiswa/report/report-mahasiswa-unair.tpl");
	exit();
}




$prodi = $db->QueryToArray("SELECT * FROM PROGRAM_STUDI 
							LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
							LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
							WHERE STATUS_AKTIF_PRODI = 1 
							AND FAKULTAS.ID_PERGURUAN_TINGGI = '{$id_pt}'
							ORDER BY PROGRAM_STUDI.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI");

$fakultas = $db->QueryToArray("SELECT count(*) as jml, NM_FAKULTAS, PROGRAM_STUDI.ID_FAKULTAS 
							FROM PROGRAM_STUDI 
							LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
							LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
							WHERE STATUS_AKTIF_PRODI = 1
							AND FAKULTAS.ID_PERGURUAN_TINGGI = '{$id_pt}'
							GROUP BY PROGRAM_STUDI.ID_FAKULTAS, NM_FAKULTAS
							ORDER BY PROGRAM_STUDI.ID_FAKULTAS, NM_FAKULTAS");

$thn_angkatan = $db->QueryToArray("SELECT THN_ANGKATAN_MHS 
									FROM MAHASISWA
									JOIN STATUS_PENGGUNA ON ID_STATUS_PENGGUNA = STATUS_AKADEMIK_MHS
									WHERE STATUS_PENGGUNA.STATUS_AKTIF = 1
									GROUP BY THN_ANGKATAN_MHS
									ORDER BY THN_ANGKATAN_MHS DESC");

$jml_prodi = $db->QueryToArray("SELECT PROGRAM_STUDI.ID_FAKULTAS, JENJANG.NM_JENJANG, PROGRAM_STUDI.NM_PROGRAM_STUDI, PROGRAM_STUDI.ID_PROGRAM_STUDI,count(*) jml
							FROM MAHASISWA
							LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
							LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
							LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
							LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
							WHERE STATUS_AKTIF_PRODI = 1 AND STATUS_PENGGUNA.AKTIF_STATUS_PENGGUNA = 1
							AND FAKULTAS.ID_PERGURUAN_TINGGI = '{$id_pt}'
							GROUP BY PROGRAM_STUDI.ID_FAKULTAS, JENJANG.NM_JENJANG, PROGRAM_STUDI.NM_PROGRAM_STUDI, PROGRAM_STUDI.ID_PROGRAM_STUDI
							ORDER BY PROGRAM_STUDI.ID_FAKULTAS, JENJANG.NM_JENJANG, PROGRAM_STUDI.NM_PROGRAM_STUDI");


$view = '<table>
			<tr>
				<th rowspan="3">THN ANGKATAN MHS</th>
			</tr>
			<tr>';
			
			foreach($fakultas as $data){
				
				$view .= '<th style="font-size: 25px;font-weight: bolder;text-align: center;" colspan="'.$data['JML'].'">
								'.$data['NM_FAKULTAS'].'
						  </th>';
			}

$view .= '<th style="font-size: 20px;vertical-align: middle;" rowspan="2">
                    &Sigma; Per-Angkatan
          </th>
		  </tr>
			<tr>';
			
			foreach($prodi as $data){
				$view .= '<th style="font-size: 10px;">
								'.$data['NM_PROGRAM_STUDI'].' ('.$data['NM_JENJANG'].')
						  </th>';
			}
				
$view .= '</tr>';
		foreach($thn_angkatan as $data_thn){	
			$view .= '
			<tr>
				<td>'.$data_thn['THN_ANGKATAN_MHS'].'</td>';
			
			$total_perangkatan = 0;
			
			foreach($prodi as $data){
				$view .= '<td style="font-size: 10px;">
			<a href="report-mahasiswa.php?mode=list_mhs&prodi='.$data['ID_PROGRAM_STUDI'].'&angkatan='.$data_thn['THN_ANGKATAN_MHS'].'">';
				$db->Query("SELECT COUNT(*) AS JML, THN_ANGKATAN_MHS, PROGRAM_STUDI.ID_PROGRAM_STUDI
				FROM MAHASISWA
				LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				WHERE STATUS_PENGGUNA.AKTIF_STATUS_PENGGUNA = 1 AND PROGRAM_STUDI.STATUS_AKTIF_PRODI = 1
				AND THN_ANGKATAN_MHS = '".$data_thn['THN_ANGKATAN_MHS']."' AND PROGRAM_STUDI.ID_PROGRAM_STUDI = '".$data['ID_PROGRAM_STUDI']."'
				GROUP BY THN_ANGKATAN_MHS, PROGRAM_STUDI.ID_PROGRAM_STUDI");

				$row = $db->FetchAssoc();
				$view .= $row['JML'];
				$view .= '</a></td>';
				$total_perangkatan = $total_perangkatan + $row['JML'];
			}
		
			$view .= '<td>'.$total_perangkatan.'</td>
					</tr>';
		}

$view .= '<tr>
			<td>&Sigma; Per Prodi</td>';
			foreach($prodi as $data){
$view .= 		'<td>';
					$db->Query("SELECT count(*) jml
							FROM MAHASISWA
							LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
							LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
							LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
							LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
							WHERE STATUS_AKTIF_PRODI = 1 AND STATUS_PENGGUNA.AKTIF_STATUS_PENGGUNA = 1
							AND PROGRAM_STUDI.ID_PROGRAM_STUDI = '".$data['ID_PROGRAM_STUDI']."'");

				$row = $db->FetchAssoc();
				$view .= $row['JML'];
				$total = $total + $row['JML'];
$view .= '		</td>';
			}
$view .= '<td>'.$total.'</td>
		  </tr>
		 </table>';
		
$smarty->assign('view',$view);

$smarty->display("mahasiswa/report/report-mahasiswa-unair.tpl");