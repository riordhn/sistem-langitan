<?php
include 'config.php';

$pengguna	= $user->ID_PENGGUNA;
$tgl		= date('Y-m-d');
$id_pt		= $id_pt_user;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$db->BeginTransaction();

	$db->Query("SELECT count(*) as cek
				FROM MAHASISWA_ASING MA
				JOIN PENGGUNA P ON P.ID_PENGGUNA = MA.ID_PENGGUNA
				WHERE NIM_MHS_ASING = '{$_POST['nim']}' AND P.ID_PERGURUAN_TINGGI = {$id_pt}");
	
	$row = $db->FetchAssoc();
		
	// Jika mahasiswa asing
	if ($row['CEK'] >= 1) {

		$db->Query("INSERT INTO ADMISI_MHS_ASING (ID_MHS_ASING, STATUS_AKD_MHS, TGL_USULAN, TGL_APV, ID_SEMESTER, STATUS_APV, ID_PENGGUNA, ALASAN, NO_SK, 
					TGL_SK, NO_IJASAH, TGL_LULUS)
					VALUES ('$_POST[id_mhs]', '$_POST[status_pengguna]', to_date('$tgl', 'YYYY-MM-DD'), to_date('$tgl', 'YYYY-MM-DD'),
					 '$_POST[semester]' ,'1', $pengguna, '$_POST[keterangan]', '$_POST[no_sk]', to_date('$_POST[tgl_sk]', 'DD-MM-YYYY'),
					 '$_POST[no_ijasah]', to_date('$_POST[tgl_lulus]', 'DD-MM-YYYY'))") 
					 or die('gagal ' . __LINE__);
		
		$db->Query("UPDATE MAHASISWA_ASING SET ID_STATUS_PENGGUNA = '$_POST[status_pengguna]' WHERE ID_MHS_ASING = '$_POST[id_mhs]'") or die('gagal' . __LINE__);
		
	} else {

		// Preventing empty input
		$no_ijasah	= (! empty($_POST['no_ijasah'])) ? "'{$_POST['no_ijasah']}'" : 'NULL';
		$tgl_keluar	= (! empty($_POST['tgl_keluar'])) ? "TO_DATE('{$_POST['tgl_keluar']}', 'DD-MM-YYYY')" : 'NULL';
		$no_sk		= (! empty($_POST['no_sk'])) ? "'{$_POST['no_sk']}'" : 'NULL';
		$tgl_sk		= (! empty($_POST['tgl_sk'])) ? "TO_DATE('{$_POST['tgl_sk']}', 'DD-MM-YYYY')" : 'NULL';
		$keterangan	= (! empty($_POST['keterangan'])) ? "'".addslashes($_POST['keterangan'])."'" : 'NULL';

		$db->Query("SELECT ID_ADMISI FROM ADMISI 
					WHERE ID_MHS = {$_POST['id_mhs']} AND ID_SEMESTER = {$_POST['semester']} AND STATUS_AKD_MHS = {$_POST['status_pengguna']}");
		$admisi = $db->FetchAssoc();
		$id_admisi = $admisi['ID_ADMISI'];

		if($id_admisi == ''){
			$db->Query(
				"INSERT INTO admisi (
					id_mhs, status_akd_mhs, tgl_usulan, tgl_apv, id_semester, status_apv, 
					no_ijasah, tgl_keluar, no_sk, tgl_sk, keterangan, 
					created_on, created_by)
				VALUES (
					{$_POST['id_mhs']}, {$_POST['status_pengguna']}, sysdate, sysdate, {$_POST['semester']}, 1, 
					{$no_ijasah}, {$tgl_keluar}, {$no_sk}, {$tgl_sk}, {$keterangan},
					sysdate, {$user->ID_PENGGUNA})") or die("Gagal " . __LINE__);
		}
		else{
			// Update data sebelumnya
			$db->Query("UPDATE ADMISI SET STATUS_AKD_MHS = {$_POST['status_pengguna']}, TGL_USULAN = SYSDATE, TGL_APV = SYSDATE, STATUS_APV = 1, NO_IJASAH = '{$no_ijasah}', TGL_KELUAR = '{$tgl_keluar}', NO_SK = '{$no_sk}', TGL_SK = '{$tgl_sk}', KETERANGAN = {$keterangan}, UPDATED_ON = SYSDATE, UPDATED_BY = {$pengguna} WHERE id_admisi = {$id_admisi}") or die('Gagal ' . __LINE__);
		}
	
		$set = "";
		$tgl = date('Y-m-d');
		
		if ($_POST['status_pengguna'] == 4 or $_POST['status_pengguna'] == 5 or $_POST['status_pengguna'] == 6 or $_POST['status_pengguna'] == 7 or $_POST['status_pengguna'] == 20) {
			
			$db->Query("INSERT INTO CEKAL_PEMBAYARAN (ID_MHS, ID_PENGGUNA, ID_SEMESTER, STATUS_CEKAL, TGL_UBAH, KETERANGAN)
						VALUES ('$_POST[id_mhs]','$pengguna','$_POST[semester]','1',to_date('$tgl', 'YYYY-MM-DD'), '$_POST[keterangan]')") or die('Gagal ' . __LINE__);
						
			$set = ", STATUS_CEKAL = 1";

		} else {

			$db->Query("INSERT INTO CEKAL_PEMBAYARAN (ID_MHS, ID_PENGGUNA, ID_SEMESTER, STATUS_CEKAL, TGL_UBAH, KETERANGAN)
						VALUES ('$_POST[id_mhs]','$pengguna','$_POST[semester]','0',to_date('$tgl', 'YYYY-MM-DD'), '$_POST[keterangan]')") or die('Gagal ' . __LINE__);
						
			$set = ", STATUS_CEKAL = 0";
		}

		$status_pengguna = $_POST['status_pengguna'];

		
		$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = {$_POST['semester']}");
        $smt = $db->FetchAssoc();
        $fd_id_smt = $smt['FD_ID_SMT'];
		
		// query get ipk
        $db->Query("SELECT id_mhs,
                        SUM(sks) AS total_sks, 
                        round(SUM(nilai_mutu) / SUM(sks), 2) AS ipk 
                    FROM (
                        SELECT
                            mhs.id_mhs,
                            /* Kode MK utk grouping total mutu */
                            COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
                            /* SKS */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
                            /* Nilai Mutu = SKS * Bobot */
                            COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu,
                            /* Urutan Nilai Terbaik */
                            row_number() OVER (PARTITION BY mhs.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY sn.nilai_standar_nilai DESC) urut
                        FROM pengambilan_mk pmk
                        JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
                        JOIN semester S ON S.id_semester = pmk.id_semester
                        -- Via Kelas
                        LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
                        LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
                        -- Via Kurikulum
                        LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
                        LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
                        -- Penyetaraan
                        LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
                        LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
                        -- nilai bobot
                        JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
                        WHERE 
                            pmk.status_apv_pengambilan_mk = 1 AND 
                            mhs.id_mhs = {$_POST['id_mhs']} AND 
                            S.fd_id_smt <= '{$fd_id_smt}'
                    )
                    GROUP BY id_mhs");
        $data_ipk = $db->FetchAssoc();
        $ipk = $data_ipk['IPK'];
        $total_sks = $data_ipk['TOTAL_SKS'];

        // query get ips
        $db->Query("SELECT id_mhs, SUM(sks) AS total_sks, round(SUM(nilai_mutu) / sum(sks), 2) as ips from (
					    SELECT
					        mhs.id_mhs,
					        /* Kode MK utk grouping total mutu */
					        COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
					        /* SKS */
					        COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
					        /* Nilai Mutu = SKS * Bobot */
					        COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu
					    FROM pengambilan_mk pmk
					    JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
					    JOIN semester S ON S.id_semester = pmk.id_semester
					    -- Via Kelas
					    LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
					    LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
					    -- Via Kurikulum
					    LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
					    LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
					    -- nilai bobot
					    JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
					    WHERE 
					        pmk.status_apv_pengambilan_mk = 1 AND 
					        mhs.id_mhs = {$_POST['id_mhs']} AND 
					        S.id_semester = {$_POST['semester']}
					)
					group by id_mhs");
        $data_ips = $db->FetchAssoc();
        $ips = $data_ips['IPS'];
        $total_sks_semester = $data_ips['TOTAL_SKS'];

		// Ambil mahasiswa_status pada mahasiswa dan semester terpilih
		$db->Query("SELECT * FROM mahasiswa_status WHERE id_mhs = {$_POST['id_mhs']} AND id_semester = {$_POST['semester']}");

		$mahasiswa_status = $db->FetchAssoc();

		// Jika belum ada :
		if ($mahasiswa_status == NULL)
		{

			if ($ipk == '' && $total_sks == '') {
				// Insert data baru
				$db->Query(
					"INSERT INTO MAHASISWA_STATUS (ID_MHS, ID_SEMESTER, ID_STATUS_PENGGUNA, IPS, SKS_SEMESTER, CREATED_BY)
						VALUES ('$_POST[id_mhs]', '$_POST[semester]', '$_POST[status_pengguna]', '{$ips}', '{$total_sks_semester}', '$pengguna')")
							or die('Gagal ' . __LINE__);
			}
			elseif($ipk == '' or $total_sks == ''){
				if($ipk == ''){
					// Insert data baru
					$db->Query(
						"INSERT INTO MAHASISWA_STATUS (ID_MHS, ID_SEMESTER, ID_STATUS_PENGGUNA, IPS, SKS_TOTAL, SKS_SEMESTER, CREATED_BY)
							VALUES ('$_POST[id_mhs]', '$_POST[semester]', '$_POST[status_pengguna]', '{$ips}', '{$total_sks}', '{$total_sks_semester}', '$pengguna')")
								or die('Gagal ' . __LINE__);
				}
				elseif($total_sks == ''){
					// Insert data baru
					$db->Query(
						"INSERT INTO MAHASISWA_STATUS (ID_MHS, ID_SEMESTER, ID_STATUS_PENGGUNA, IPS, IPK, SKS_SEMESTER, CREATED_BY)
							VALUES ('$_POST[id_mhs]', '$_POST[semester]', '$_POST[status_pengguna]', '{$ips}', '{$ipk}', '{$total_sks_semester}', '$pengguna')")
								or die('Gagal ' . __LINE__);
				}
			}
			else{
				// Insert data baru
				$db->Query(
					"INSERT INTO MAHASISWA_STATUS (ID_MHS, ID_SEMESTER, ID_STATUS_PENGGUNA, IPS, IPK, SKS_TOTAL, SKS_SEMESTER, CREATED_BY)
					VALUES ('$_POST[id_mhs]', '$_POST[semester]', '$_POST[status_pengguna]', '{$ips}', '{$ipk}', '{$total_sks}', '{$total_sks_semester}', '$pengguna')")
							or die('Gagal ' . __LINE__);
			}
		}
		else // Jika sudah ada
		{
			if ($ipk == '' && $total_sks == '') {	
				// Update data sebelumnya
				$db->Query("UPDATE mahasiswa_status SET id_status_pengguna = {$_POST['status_pengguna']}, ips = '{$ips}', sks_semester = '{$total_sks_semester}', updated_on = sysdate, updated_by = {$pengguna} WHERE id_mhs_status = {$mahasiswa_status['ID_MHS_STATUS']}") or die('Gagal ' . __LINE__);
			}
			elseif($ipk == '' or $total_sks == ''){
				if($ipk == ''){
					// Update data sebelumnya
					$db->Query("UPDATE mahasiswa_status SET id_status_pengguna = {$_POST['status_pengguna']}, ips = '{$ips}', sks_total = '{$total_sks}', sks_semester = '{$total_sks_semester}', updated_on = sysdate, updated_by = {$pengguna} WHERE id_mhs_status = {$mahasiswa_status['ID_MHS_STATUS']}") or die('Gagal ' . __LINE__);
				}
				elseif($total_sks == ''){
					// Update data sebelumnya
					$db->Query("UPDATE mahasiswa_status SET id_status_pengguna = {$_POST['status_pengguna']}, ips = '{$ips}', ipk = '{$ipk}', sks_semester = '{$total_sks_semester}', updated_on = sysdate, updated_by = {$pengguna} WHERE id_mhs_status = {$mahasiswa_status['ID_MHS_STATUS']}") or die('Gagal ' . __LINE__);
				}
			}
			else{
				// Update data sebelumnya
				$db->Query("UPDATE mahasiswa_status SET id_status_pengguna = {$_POST['status_pengguna']}, ips = '{$ips}', ipk = '{$ipk}', sks_total = '{$total_sks}', sks_semester = '{$total_sks_semester}', updated_on = sysdate, updated_by = {$pengguna} WHERE id_mhs_status = {$mahasiswa_status['ID_MHS_STATUS']}") or die('Gagal ' . __LINE__);
			}
		}

		$db->Query("UPDATE MAHASISWA SET STATUS_AKADEMIK_MHS = '$_POST[status_pengguna]' $set WHERE ID_MHS = '$_POST[id_mhs]'") or die('Gagal ' . __LINE__);

		$db->Commit();
	}
	
	echo 'Berhasil';
}

if (isset($_GET['nim'])) {
	
	$nim = get('nim', '');

	$db->Query("SELECT count(*) as ADA FROM MAHASISWA
	 			JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
	 			WHERE NIM_MHS = '{$nim}' AND PENGGUNA.ID_PERGURUAN_TINGGI = '{$id_pt}' ");
    $cek = $db->FetchAssoc();	
	
	$db->Query("SELECT count(*) as MHS_AKTIF
				FROM MAHASISWA 
				JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = STATUS_AKADEMIK_MHS
				JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
				WHERE NIM_MHS = '{$nim}' AND (STATUS_PENGGUNA.STATUS_AKTIF = 1)
				AND PENGGUNA.ID_PERGURUAN_TINGGI = '{$id_pt}' ");
    $mhs_aktif = $db->FetchAssoc();
	
	$kondisi = 'ok';

	if ($cek['ADA'] == 1 and $mhs_aktif['MHS_AKTIF'] == 1) {
		$kondisi = 'ok';
	}
	
	if ($kondisi == 'ok') {
	
		$status_pengguna = $db->QueryToArray("SELECT * FROM STATUS_PENGGUNA WHERE ID_ROLE = 3 AND ID_STATUS_PENGGUNA NOT IN (4,16) ORDER BY NM_STATUS_PENGGUNA ASC");
		$smarty->assign('status_pengguna', $status_pengguna);
		
		$semester = $db->QueryToArray("SELECT * FROM SEMESTER WHERE (NM_SEMESTER = 'Ganjil' OR NM_SEMESTER = 'Genap') AND ID_PERGURUAN_TINGGI = '{$id_pt}' ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
		$smarty->assign('semester', $semester);
			
		$db->Query("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True' AND ID_PERGURUAN_TINGGI = '{$id_pt}'");
		$semester_aktif = $db->FetchAssoc();	
		$smarty->assign('semester_aktif', $semester_aktif['ID_SEMESTER']);
		
		$db->Query("SELECT ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, NM_JENJANG, NM_STATUS_PENGGUNA 
					FROM MAHASISWA 
					JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA AND PENGGUNA.ID_PERGURUAN_TINGGI = '{$id_pt}'
					LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
					LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
					LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
					LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
					WHERE NIM_MHS = '{$nim}'");
		
		$row = $db->FetchAssoc();
		
		if ($row['ID_MHS'] == '') {
			$db->Query("SELECT ID_MHS_ASING AS ID_MHS, NIM_MHS_ASING AS NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, NM_JENJANG, NM_STATUS_PENGGUNA 
					FROM MAHASISWA_ASING
					JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA_ASING.ID_PENGGUNA AND PENGGUNA.ID_PERGURUAN_TINGGI = '{$id_pt}'
					LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA_ASING.ID_STATUS_PENGGUNA
					LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA_ASING.ID_PROGRAM_STUDI
					LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
					LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
					WHERE NIM_MHS_ASING = '{$nim}'");
			$row = $db->FetchAssoc();	
		}
		
		$smarty->assign('mhs', $row);

	} elseif ($mhs_aktif['MHS_AKTIF'] == 0) {
		echo 'Bukan mahasiswa aktif';
	} else {
		echo 'Tidak ada mahasiswa nya';
	}
}


$smarty->display("mahasiswa/admisi/add.tpl");