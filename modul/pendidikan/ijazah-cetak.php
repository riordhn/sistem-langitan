<?php
require_once('config.php');

$smarty->assign('fakultas', array(
    'FST'   => 'Sains dan Teknologi',
    'FKP'   => 'Keperawatan',
    'FPK'   => 'Perikanan dan Kelautan'
));

$smarty->display('ijazah-cetak.tpl');
?>
