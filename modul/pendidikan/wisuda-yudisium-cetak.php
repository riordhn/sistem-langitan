<?php
include('config.php');
include 'class/wisuda.class.php';

$wd = new wisuda($db);
		
		$pengajuan_bayar = $wd->wisuda_yudisium($_REQUEST['fakultas'], $_REQUEST['status_bayar'], $_REQUEST['periode'], $_REQUEST['tgl_mulai'], $_REQUEST['tgl_selesai']);

ob_start();

?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
<table cellpadding="3" cellspacing="3" border="1">
    <thead>
        <tr>
        	<td class="header_text">URUT</td>
            <td class="header_text">GELAR DEPAN</td>
            <td class="header_text">NAMA</td>
            <td class="header_text">GELAR BELAKANG</td>
            <td class="header_text">KELAMIN</td>
            <td class="header_text">NIM</td>
            <td class="header_text">TTL</td>
            <td class="header_text">JENJANG</td>            
            <td class="header_text">FAKULTAS</td>
            <td class="header_text">PROGRAM STUDI</td>
			<td class="header_text">TGL LULUS</td>
            <td class="header_text">ORTU</td>
            <td class="header_text">ALAMAT</td>
            <td class="header_text">EMAIL</td>
            <td class="header_text">NO IJASAH</td>
            <td class="header_text">KODE FAKULTAS</td>
            <td class="header_text">IPK</td>
            <td class="header_text">ELPT</td>
        </tr>
    </thead>
    <tbody>
        <?php
		$no = 1;
        foreach ($pengajuan_bayar as $data) {			
            echo
            '<tr>
                <td>'.$no++.'</td>
                <td>'.$data['GELAR_DEPAN'].'</td>
                <td>'.$data['NM_PENGGUNA'].'</td>
				<td>'.$data['GELAR_BELAKANG'].'</td>
				<td>'.$data['KELAMIN_PENGGUNA'].'</td>
				<td>\''.$data['NIM_MHS'].'</td>
				<td>'.$data['LAHIR_IJAZAH'].'</td>
				<td>'.$data['NM_JENJANG'].'</td>
				<td>'.$data['NM_FAKULTAS'].'</td>
                <td>'.$data['NM_PROGRAM_STUDI'].'</td>                
				<td>'.$data['TGL_LULUS_PENGAJUAN'].'</td>
				<td>'.$data['ORTU'].'</td>
				<td>'.$data['ALAMAT_MHS'].'</td>
				<td>'.$data['EMAIL_PENGGUNA'].'</td>
				<td>'.$data['NO_IJASAH'].'</td>
				<td>'.$data['ID_FAKULTAS'].'</td>
				<td>'.$data['IPK'].'</td>
				<td>'.$data['ELPT'].'</td>
            </tr>';
        }
        ?>
    </tbody>
</table>

<?php
$filename = 'wisuda' . date('Y-m-d').'.xls';
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
