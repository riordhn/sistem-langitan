<?php
include 'config.php';
include 'class/report_mahasiswa.class.php';
include 'class/evaluasi.class.php';

$id_pt = $id_pt_user;

$rm = new report_mahasiswa($db);
$eval = new evaluasi($db);

if (isset($_POST['tampil'])) {

    $smarty->assign('data_program_studi', $eval->program_studi(
        $_POST['fakultas'], $_POST['jenjang'], $_POST['program_studi']));

    $smarty->assign('data_mahasiswa', $rm->get_mahasiswa_dayat(1,
        $_POST['fakultas'], $_POST['program_studi'], $_POST['tahun_akademik'], $_POST['status'],
        $_POST['jenjang'], $_POST['jalur'], $id_pt, $_POST['bidikmisi'], $_POST['kota'],
        $_POST['tahun_lulus'], $_POST['kelamin']));

    $smarty->assign('tampil', $_POST['tampil']);

    $smarty->assign('id_fakultas', $_POST['fakultas']);
    $smarty->assign('id_prodi', $_POST['program_studi']);
    $smarty->assign('tahun_akademik', $_POST['tahun_akademik']);
    $smarty->assign('status', $_POST['status']);
    $smarty->assign('jenjang', $_POST['jenjang']);
    $smarty->assign('jalur', $_POST['jalur']);
    $smarty->assign('id_pt', $id_pt);
    $smarty->assign('provinsi', $_POST['provinsi']);
    $smarty->assign('bidikmisi', $_POST['bidikmisi']);
    $smarty->assign('kota', $_POST['kota']);
    $smarty->assign('tahun_lulus', $_POST['tahun_lulus']);
    $smarty->assign('kelamin', $_POST['kelamin']);
    $link_excel = "excel-data-mahasiswa.php?" .
        "fakultas=" . $_POST['fakultas'] .
        "&program_studi=" . $_POST['program_studi'] .
        "&tahun_akademik=" . $_POST['tahun_akademik'] .
        "&status=" . $_POST['status'] .
        "&jenjang=" . $_POST['jenjang'] .
        "&jalur=" . $_POST['jalur'] .
        "&id_pt=" . $id_pt .
        "&provinsi=" . $_POST['provinsi'] .
        "&bidikmisi=" . $_POST['bidikmisi'] .
        "&kota=" . $_POST['kota'] .
        "&tahun_lulus=" . $_POST['tahun_lulus'] .
        "&kelamin=" . $_POST['kelamin'];
    $smarty->assign('link_excel', $link_excel);

    $link_ktm = "ktm-data-mahasiswa.php?" .
        "fakultas=" . $_POST['fakultas'] .
        "&program_studi=" . $_POST['program_studi'] .
        "&tahun_akademik=" . $_POST['tahun_akademik'] .
        "&status=" . $_POST['status'] .
        "&jenjang=" . $_POST['jenjang'] .
        "&jalur=" . $_POST['jalur'] .
        "&id_pt=" . $id_pt .
        "&provinsi=" . $_POST['provinsi'] .
        "&bidikmisi=" . $_POST['bidikmisi'] .
        "&kota=" . $_POST['kota'] .
        "&tahun_lulus=" . $_POST['tahun_lulus'];
    $smarty->assign('link_ktm', $link_ktm);

    $link_pdf = "cetak-data-mahasiswa.php?" .
        "fakultas=" . $_POST['fakultas'] .
        "&program_studi=" . $_POST['program_studi'] .
        "&tahun_akademik=" . $_POST['tahun_akademik'] .
        "&status=" . $_POST['status'] .
        "&jenjang=" . $_POST['jenjang'] .
        "&jalur=" . $_POST['jalur'] .
        "&id_pt=" . $id_pt .
        "&provinsi=" . $_POST['provinsi'] .
        "&bidikmisi=" . $_POST['bidikmisi'] .
        "&kota=" . $_POST['kota'] .
        "&tahun_lulus=" . $_POST['tahun_lulus'];
    $smarty->assign('link_pdf', $link_pdf);
}

$smarty->assign('data_fakultas', $rm->get_fakultas($id_pt));
$smarty->assign('data_jalur', $rm->get_jalur($id_pt));
$smarty->assign('data_jenjang', $rm->get_jenjang());
$smarty->assign('data_tahun_akademik', $rm->get_tahun_akademik());
$smarty->assign('data_status', $rm->get_status());
$smarty->assign('data_provinsi', $rm->get_prov_in());
$smarty->assign('data_tahun_lulus', $rm->get_tahun_lulus_sekolah($id_pt));

$smarty->display("mahasiswa/unair/view.tpl");