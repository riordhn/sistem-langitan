<?php

include 'config.php';
include 'includes/function.php';
include 'class/report_mahasiswa.class.php';

$id_pt = $id_pt_user;

$rm = new report_mahasiswa($db);

$depan = time();
$belakang = strrev(time());

if (isset($_GET['x'])) {
    $smarty->assign('data_mahasiswa', $rm->get_hasil_cari_mahasiswa(get('x'), $id_pt));
} else if (isset($_GET['detail'])) {

    $smarty->assign('biodata_mahasiswa', $rm->get_biodata_mahasiswa(get('y'), $id_pt));

    $smarty->assign('khs_mahasiswa', $rm->get_detail_akademik_khs(get('y'), $id_pt));

    $smarty->assign('aktivitas_mahasiswa', $rm->get_detail_aktivitas(get('y'), $id_pt));

    $smarty->assign('admisi_mahasiswa', $rm->get_detail_admisi(get('y'), $id_pt));
    
	$smarty->assign('tagihan_mahasiswa', $rm->get_jumlah_tagihan_mahasiswa(get('y')));

    $link = 'proses/biodata-pdf.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&nim_mhs=' . get('y')) . '';
    $smarty->assign('LINK', $link);


    if (post('mode') == 'ubahPassword')
    {
        $id_pengguna = post('id_pengguna');
        $username = post('username');

        $password_hash = sha1($username);
        $result = $db->Query("update pengguna set password_hash = '{$password_hash}', password_must_change = '1' where id_pengguna = {$id_pengguna}");

        $smarty->assign('edited', $result ? "Password berhasil direset" : "Password gagal direset");
    }
}

$smarty->display("mahasiswa/cari/cari-mahasiswa.tpl");