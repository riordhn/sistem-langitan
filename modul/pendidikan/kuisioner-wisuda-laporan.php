<?php

include 'config.php';
//include 'class/report_mahasiswa.class.php';
/*
$rm = new report_mahasiswa($db);

if (get('mode') == 'list_mhs') {
    $smarty->assign('data_mahasiswa',$rm->get_mahasiswa_dayat('', get('prodi'), get('angkatan'), ''));
}
*/



$aspek = $db->QueryToArray("SELECT c.EVALUASI_ASPEK, c.ID_EVAL_ASPEK FROM EVALUASI_KELOMPOK_ASPEK a, EVALUASI_INSTRUMEN b , EVALUASI_ASPEK c WHERE a.ID_EVAL_INSTRUMEN=b.ID_EVAL_INSTRUMEN 
	AND a.ID_EVAL_INSTRUMEN = '9' and a.ID_EVAL_KELOMPOK_ASPEK = c.ID_EVAL_KELOMPOK_ASPEK 
	ORDER BY b.ID_EVAL_INSTRUMEN ASC");

$kelompokaspek = $db->QueryToArray("SELECT count(*) as jml, NAMA_KELOMPOK
							FROM EVALUASI_INSTRUMEN 
							LEFT JOIN EVALUASI_KELOMPOK_ASPEK ON EVALUASI_KELOMPOK_ASPEK.ID_EVAL_INSTRUMEN = EVALUASI_INSTRUMEN.ID_EVAL_INSTRUMEN
LEFT JOIN EVALUASI_ASPEK ON EVALUASI_ASPEK.ID_EVAL_KELOMPOK_ASPEK = EVALUASI_KELOMPOK_ASPEK.ID_EVAL_KELOMPOK_ASPEK
							WHERE EVALUASI_INSTRUMEN.ID_EVAL_INSTRUMEN = '9'
							GROUP BY EVALUASI_KELOMPOK_ASPEK.ID_EVAL_KELOMPOK_ASPEK, NAMA_KELOMPOK
							ORDER BY EVALUASI_KELOMPOK_ASPEK.ID_EVAL_KELOMPOK_ASPEK, NAMA_KELOMPOK");
$mhs = $db->QueryToArray("SELECT MAHASISWA.ID_MHS as ID_MHS 
FROM MAHASISWA
JOIN EVALUASI_HASIL ON EVALUASI_HASIL.ID_MHS = MAHASISWA.ID_MHS
JOIN EVALUASI_INSTRUMEN ON EVALUASI_INSTRUMEN.ID_EVAL_INSTRUMEN = EVALUASI_INSTRUMEN.ID_EVAL_INSTRUMEN
WHERE EVALUASI_HASIL.ID_EVAL_INSTRUMEN = '9'
GROUP BY MAHASISWA.ID_MHS");							

$view = '<table>
			<tr>
				<th rowspan="3">ID_MHS</th>
			</tr>
			<tr>';
			
			foreach($kelompokaspek as $data){
				
				$view .= '<th style="font-size: 25px;font-weight: bolder;text-align: center;" colspan="'.$data['JML'].'">
								'.$data['NAMA_KELOMPOK'].'
						  </th>';
			}

$view .= '
		  </tr>
			<tr>';
			
			foreach($aspek as $data){
				$view .= '<th style="font-size: 10px;">
								'.$data['EVALUASI_ASPEK'].'
						  </th>';
			}
				
$view .= '</tr>';
		foreach($mhs as $data_mhs){	
			$view .= '
			<tr>
				<td>'.$data_mhs['ID_MHS'].'</td>';
			
			$total_perangkatan = 0;
			
			foreach($aspek as $data){
				$view .= '<td style="font-size: 10px;">';
				$db->Query("SELECT NILAI_EVAL
				FROM MAHASISWA
				LEFT JOIN EVALUASI_HASIL ON EVALUASI_HASIL.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN EVALUASI_ASPEK ON EVALUASI_ASPEK.ID_EVAL_ASPEK = EVALUASI_HASIL.ID_EVAL_ASPEK
				LEFT JOIN EVALUASI_INSTRUMEN ON EVALUASI_INSTRUMEN.ID_EVAL_INSTRUMEN = EVALUASI_HASIL.ID_EVAL_INSTRUMEN
				WHERE EVALUASI_HASIL.ID_EVAL_INSTRUMEN = 9
				AND MAHASISWA.ID_MHS = '".$data_mhs['ID_MHS']."' AND EVALUASI_ASPEK.ID_EVAL_ASPEK = '".$data['ID_EVAL_ASPEK']."'");

				$row = $db->FetchAssoc();
				
				$view .= $row['NILAI_EVAL'].'</td>';
				$total_perangkatan = $total_perangkatan + $row['NILAI_EVAL'];
			}
		
			$view .= ' 
				</tr>';
		}

$view .= '<tr>
			<td>&Sigma; Per Aspek</td>';
			foreach($aspek as $data){
$view .= 		'<td>';
					$db->Query("SELECT ROUND(AVG(NILAI_EVAL), 2) as JML
				FROM MAHASISWA
				LEFT JOIN EVALUASI_HASIL ON EVALUASI_HASIL.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN EVALUASI_ASPEK ON EVALUASI_ASPEK.ID_EVAL_ASPEK = EVALUASI_HASIL.ID_EVAL_ASPEK
				LEFT JOIN EVALUASI_INSTRUMEN ON EVALUASI_INSTRUMEN.ID_EVAL_INSTRUMEN = EVALUASI_HASIL.ID_EVAL_INSTRUMEN
				WHERE EVALUASI_HASIL.ID_EVAL_INSTRUMEN = 9
				AND EVALUASI_ASPEK.ID_EVAL_ASPEK = '".$data['ID_EVAL_ASPEK']."'");

				$row = $db->FetchAssoc();
				$view .= $row['JML'];
				//$total = $total + $row['JML'];
$view .= '		</td>';
			}
$view .= '
		  </tr>
		 </table>';
		
$smarty->assign('view',$view);

$smarty->display("kuisioner/kuisioner-registrasi-laporan.tpl");
?>
