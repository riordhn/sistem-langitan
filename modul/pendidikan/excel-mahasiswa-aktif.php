<?php
include 'config.php';
include 'class/report_mahasiswa.class.php';

$rm = new report_mahasiswa($db);
	
	$data_fakultas = $rm->get_fakultas();
	$data_tahun_akademik= $rm->get_tahun_akademik();
	$data_status= $rm->get_status_aktif();
	$data_jenjang= $rm->get_jenjang();
	$fakultas= get('fakultas');
    $program_studi= get('program_studi');
    $tahun_akademik= get('tahun_akademik');
	$status= get('status');
    $data_program_studi= $rm->get_program_studi(get('fakultas'));
	$data_fakultas_prodi= $rm->get_fakultas_prodi();
	$data_mhs_aktif= $rm->get_mhs_aktif();
	
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
   <table>
    <tr>
		<th rowspan="3" style="text-align:center">
        	Fakultas		
		</th>
        <th rowspan="3" style="text-align:center">
        	Program Studi		
		</th>
		<th colspan="{count($data_jenjang)*2}" style="text-align:center">
        	Jenjang
		</th>
        <th scope="col" style="text-align: center" rowspan="2" colspan="2">Σ Per Prodi</th>
    </tr>
	<tr>
	<?PHP foreach($data_jenjang as $data){
      echo '<th colspan="2" style="text-align:center">'.$data['NM_JENJANG'].'</th>';
	}
	?>
    </tr>
    <tr>
    <?PHP foreach($data_jenjang as $data){
      echo '<th>Laki</th>
      		<th>Perempuan</th>';
	}?>
    	<th>Laki</th>
        <th>Perempuan</th>
    </tr>
    <?PHP foreach($data_fakultas_prodi as $data){
    echo '<tr>
		<td>
        	'.$data['FAKULTAS'].'        
		</td>
        <td>
        	'.$data['NM_PROGRAM_STUDI'].'        
		</td>';
        $total_prodi_laki = 0;
        $total_prodi_perempuan = 0;
		foreach($data_jenjang as $jenj){
		echo '<td>';
			 $found = false;
			 foreach($data_mhs_aktif as $mhs_aktif){
				if($data['ID_PROGRAM_STUDI'] == $mhs_aktif['ID_PROGRAM_STUDI'] and $mhs_aktif['ID_JENJANG'] == $jenj['ID_JENJANG'] 
                	and $mhs_aktif['KELAMIN_PENGGUNA'] == 1){
					echo '<span style="color:#0033FF; text-align:center">'.$mhs_aktif['JUMLAH'].'</span>';
					$found = true;
                    $total_prodi_laki = $total_prodi_laki + $mhs_aktif['JUMLAH'];
					break;
				}
			 }
			if($found == false){
                    echo '<span style="text-align:center">0</span>';
            }
		echo '
		</td>
        <td>';
			$found = false;
			foreach($data_mhs_aktif as $mhs_aktif){
				if($data['ID_PROGRAM_STUDI'] == $mhs_aktif['ID_PROGRAM_STUDI'] and $mhs_aktif['ID_JENJANG'] == $jenj['ID_JENJANG'] 
                	and $mhs_aktif['KELAMIN_PENGGUNA'] == 2){
					echo '<span style="color:#0033FF; text-align:center">'.$mhs_aktif['JUMLAH'].'</span>';
					$found = true;
                    $total_prodi_perempuan = $total_prodi_perempuan + $mhs_aktif['JUMLAH'];
					break;
				}
			}
			if($found == false){
                    echo '<span style="text-align:center">0</span>';
            }        	
        echo '</td>';
		}
        echo '<td style="text-align: center; font-weight: bold">'.$total_prodi_laki.'</td>
        <td style="text-align: center; font-weight: bold">'.$total_prodi_perempuan.'</td>
    </tr>';
    }
    	echo'<tr>
            <td colspan="2">TOTAL</td>';
            $total_all_perempuan = 0;
            $total_all_laki = 0;
        foreach($data_jenjang as $jenj){
            $found = false;
            $total_jenj_laki = 0;
			foreach($data_mhs_aktif as $mhs_aktif){
				if($mhs_aktif['ID_JENJANG'] == $jenj['ID_JENJANG'] and $mhs_aktif['KELAMIN_PENGGUNA'] == 1){
                	$total_jenj_laki = $total_jenj_laki + $mhs_aktif['JUMLAH'];
					$found = true;
                    $total_all_laki = $total_all_laki + $mhs_aktif['JUMLAH'];
				}
			}
			if($found == false){
                    echo '<td style="text-align: center; font-weight: bold">0</td>';
			}else{
            		echo '<td style="text-align: center; font-weight: bold">'.$total_jenj_laki.'</td>';
            }             
           
           	$found = false;
            $total_jenj_perempuan = 0;
			foreach($data_mhs_aktif as $mhs_aktif){
				if($mhs_aktif['ID_JENJANG'] == $jenj['ID_JENJANG'] and $mhs_aktif['KELAMIN_PENGGUNA'] == 2){
                	$total_jenj_perempuan = $total_jenj_perempuan + $mhs_aktif['JUMLAH'];
					$found = true;
                    $total_all_perempuan = $total_all_perempuan + $mhs_aktif['JUMLAH'];
				}
			}
			if($found == false){
                    echo '<td style="text-align: center; font-weight: bold">0</td>';
            }else{
            		echo '<td style="text-align: center; font-weight: bold">'.$total_jenj_perempuan.'</td>';
            } 
        }
            echo'<td style="text-align: center; font-weight: bold">'.$total_all_laki.'</td>
            <td style="text-align: center; font-weight: bold">'.$total_all_perempuan.'</td>
        </tr>
   </table>';


$filename = 'Excel_Mhs_Unair' . date('Y-m-d');
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
