<?php
include('config.php');
include 'class/report_mahasiswa.class.php';
$rm = new report_mahasiswa($db);

if(isset($_GET['get_id'])){
	$penerimaan = $rm->get_penerimaan_detail($_GET['get_fakultas'], $_GET['get_id'], $_GET['get_prodi'], $_GET['get_jenjang'], $_GET['get_jalur'], $_GET['get_gel'], $_GET['get_thn'], $_GET['get_semester']);
}else{

$penerimaan = $rm->get_penerimaan($_GET['get_fakultas'], $_GET['get_jenjang'], $_GET['get_jalur'], $_GET['get_gel'], $_GET['get_thn'], $_GET['get_semester']);
}

ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
<?PHP 
if(isset($_GET['get_id'])){
?>
<table border="1">	
	<tr>
		<th>No</th>
		<th>No Ujian</th>
        <th>NIM</th>
        <th>Nama</th>
		<th>Fakultas</th>
        <th>Prodi</th>
		<th>Diterima</th>
        <th>Bayar</th>
        <th>Verifikasi</th>
        <th>Finger</th>
        <th>KTM</th>
		<th>Alamat</th>
		<th>Kota</th>
		<th>Telp/No Hp</th>
    </tr>
	<?PHP
	$no = 1;
	foreach ($penerimaan as $data){
		echo "<tr>
			<td>$no</td>
			<td>$data[NO_UJIAN]</td>
			<td>'$data[NIM_MHS]</td>
			<td>$data[NM_C_MHS]</td>
			<td>$data[NM_FAKULTAS]</td>
			<td>$data[NM_JENJANG] - $data[NM_PROGRAM_STUDI]</td>
			<td>$data[TGL_DITERIMA]</td>
			<td>";
				$no++;
				if ($data['TGL_BAYAR'] <> ''){
					echo "Sudah";
				}else{
					echo "-";
				}
			echo "</td>
			<td>";
				if ($data['TGL_VERIFIKASI_PENDIDIKAN'] <> ''){
					echo "Sudah";
				}else{
					echo "-";
				}
			echo "</td>
			<td style='text-align:center'>";
				if ($data['FINGER'] <> ''){
					echo "Sudah";
				}else{
					echo "-";
				}
			echo "</td>
			<td style='text-align:center'>";
				if ($data['TGL_CETAK_KTM'] <> ''){
					echo "Sudah";
				}else{
					echo "-";
				}
			echo "</td>
			<td>$data[ALAMAT]</td>
			<td>$data[NM_KOTA]</td>
			<td>'$data[TELP]</td>
		</tr>";
	}
echo "</table>";
}else{

echo '<table>
    <tr>';
    if ($_GET['get_fakultas'] != ''){
       echo '<th>Progam Studi</th>';
	}
    else{
    	echo '<th>Fakultas</th>';
    }
	
	echo'	<th>Nama Penerimaan</th>
        <th>Jalur</th>
		<th>Jenjang</th>
        <th>Tahun</th>
        <th>Diterima</th>
        <th>Regmaba</th>
        <th>Verifikasi Keuangan</th>
        <th>Bayar</th>
        <th>Verifikasi Pendidikan</th>        
        <th>Finger</th>
        <th>KTM</th>
    </tr>';
    $jml_diterima = 0;
    $jml_bayar = 0;
    $jml_verifikasi = 0;
    $jml_verifikasi_keu = 0;
    $jml_finger = 0;
    $jml_ktm = 0;
    $jml_regmaba = 0;
    foreach ($penerimaan as $data){
    echo '<tr>';
    if ($_GET['get_fakultas'] != ''){
        echo '<td>'.$data['NM_PROGRAM_STUDI'].'</td>';
		$get_prodi = $data['ID_PROGRAM_STUDI'];
	}else{
    	echo '<td>'.$data['NM_FAKULTAS'].'</td>';
		$get_fakultas = $data['ID_FAKULTAS'];
    }
		echo '<td>'.$data['NM_PENERIMAAN'].'</td>
        <td>'.$data['NM_JALUR']. 'Gel-'.$data['GELOMBANG'].'</td>
		<td style="text-align:center">'.$data['NM_JENJANG'].'</td>
        <td>'.$data['TAHUN'] .'/'. $data['SEMESTER'].'</td>
        <td style="text-align:center">'.$data['DITERIMA'].'</td>
        <td style="text-align:center">'.$data['REGMABA'].'</td>
        <td style="text-align:center">'.$data['VERIFIKASI_KEUANGAN'].'</td>
        <td style="text-align:center">'.$data['BAYAR'].'</td>
        <td style="text-align:center">'.$data['VERIFIKASI'].'</td>        
        <td style="text-align:center">'.$data['FINGER'].'</td>
        <td style="text-align:center">'.$data['KTM'].'</td>
    </tr>';
    $jml_diterima = $jml_diterima + $data['DITERIMA'];
    $jml_bayar = $jml_bayar + $data['BAYAR'];
    $jml_verifikasi = $jml_verifikasi + $data['VERIFIKASI'];
    $jml_finger = $jml_finger + $data['FINGER'];
    $jml_ktm = $jml_ktm + $data['KTM'];
    $jml_regmaba = $jml_regmaba + $data['REGMABA'];
    $jml_verifikasi_keu = $jml_verifikasi_keu + $data['VERIFIKASI_KEUANGAN'];
    }
	echo '
    <tr>
        <th colspan="5">TOTAL</th>
        <th style="text-align:center">'.$jml_diterima.'</th>
        <th style="text-align:center">'.$jml_regmaba.'</th>
        <th style="text-align:center">'.$jml_verifikasi_keu.'</th> 
        <th style="text-align:center">'.$jml_bayar.'</th>
        <th style="text-align:center">'.$jml_verifikasi.'</th>               
        <th style="text-align:center">'.$jml_finger.'</th>
        <th style="text-align:center">'.$jml_ktm.'</th>
    </tr>
</table>';


}
$filename = 'Pendaftaran registrasi ' . date('Y-m-d');
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
