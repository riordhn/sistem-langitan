<?php
require 'config.php';
require 'class/aucc.class.php';

$aucc = new AUCC_Pendidikan($db);

if (isset($_POST['mode']) or isset($_GET['mode'])) {
    if (isset($_POST['mode'])) {
        $mode = $_POST['mode'];
    } else {
        $mode = $_GET['mode'];
    }
}

if (isset($mode)) {
    $smarty->assign('get_semester_all', $aucc->get_semester_all($user->ID_PERGURUAN_TINGGI));
    $smarty->assign('get_jenjang', $aucc->get_jenjang());
    $master = $db->QueryToArray("SELECT * FROM TARIF_WISUDA WHERE ID_PERGURUAN_TINGGI = {$id_pt_user} ORDER BY NM_TARIF_WISUDA ASC");
    $smarty->assign('master', $master);

    if ($mode == 'insert' or $mode == 'update') {
        $semester = $_POST['semester'];
        $jenjang = $_POST['jenjang'];
        $tgl_awal = $_POST['tgl_awal'];
        $tgl_akhir = $_POST['tgl_akhir'];
        $besar_biaya = $_POST['besar_biaya'];
        $aktif = $_POST['aktif'];
        $nama_tarif = $_POST['nama_tarif'];
    
        if ($mode == 'insert') {
            $db->Query("INSERT INTO PERIODE_WISUDA 
            (ID_TARIF_WISUDA, ID_SEMESTER, ID_JENJANG, BESAR_BIAYA, TGL_BAYAR_MULAI, TGL_BAYAR_SELESAI, STATUS_AKTIF)
            VALUES
            ('$nama_tarif', '$semester', '$jenjang', '$besar_biaya', to_date('$tgl_awal', 'DD-MM-YYYY'), to_date('$tgl_akhir', 'DD-MM-YYYY'), '$aktif')");
            $mode = 'input';
        } else {
            $id = $_POST['id'];
            $db->Query("UPDATE PERIODE_WISUDA SET ID_TARIF_WISUDA = '$nama_tarif', ID_SEMESTER = '$semester', ID_JENJANG = '$jenjang', 
                BESAR_BIAYA = '$besar_biaya', TGL_BAYAR_MULAI = to_date('$tgl_awal', 'DD-MM-YYYY'), 
                TGL_BAYAR_SELESAI = to_date('$tgl_akhir', 'DD-MM-YYYY'), STATUS_AKTIF = '$aktif'
                WHERE ID_PERIODE_WISUDA = '$id'");
            $mode = 'edit';
        }
    }

    if ($mode == 'input') {
        $mode = 'insert';
    } elseif ($mode == 'edit') {
        if ($_GET['id']) {
            $id = $_GET['id'];
        } else {
            $id = $_POST['id'];
        }
        $db->Query("SELECT PERIODE_WISUDA.* FROM PERIODE_WISUDA
                    LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PERIODE_WISUDA.ID_SEMESTER
                    LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
                    LEFT JOIN TARIF_WISUDA ON TARIF_WISUDA.ID_TARIF_WISUDA = PERIODE_WISUDA.ID_TARIF_WISUDA
                    WHERE ID_PERIODE_WISUDA = '$id'
                    ORDER BY TARIF_WISUDA.ID_TARIF_WISUDA DESC, NM_JENJANG ASC");
        $row = $db->FetchAssoc();
        $smarty->assign('nama_tarif', $row['ID_TARIF_WISUDA']);
        $smarty->assign('semester', $row['ID_SEMESTER']);
        $smarty->assign('jenjang', $row['ID_JENJANG']);
        $smarty->assign('besar_biaya', $row['BESAR_BIAYA']);
        $smarty->assign('status_aktif', $row['STATUS_AKTIF']);
        $smarty->assign('tgl_awal', date_format(date_create($row['TGL_BAYAR_MULAI']), 'd-m-Y'));
        $smarty->assign('tgl_akhir', date_format(date_create($row['TGL_BAYAR_SELESAI']), 'd-m-Y'));
        $smarty->assign('id', $id);
        $mode = 'update';
    }
    $smarty->assign('mode', $mode);
} else {
        $periode_wiuda = $db->QueryToArray("SELECT PERIODE_WISUDA.*, NM_JENJANG, NM_SEMESTER, THN_AKADEMIK_SEMESTER, NM_TARIF_WISUDA 
                            FROM PERIODE_WISUDA
                            LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PERIODE_WISUDA.ID_SEMESTER
                            LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PERIODE_WISUDA.ID_JENJANG
                            LEFT JOIN TARIF_WISUDA ON TARIF_WISUDA.ID_TARIF_WISUDA = PERIODE_WISUDA.ID_TARIF_WISUDA AND TARIF_WISUDA.ID_PERGURUAN_TINGGI = {$id_pt_user}
                            ORDER BY NM_TARIF_WISUDA, NM_JENJANG");
        $smarty->assign('periode_wiuda', $periode_wiuda);
}

$smarty->display("wisuda/wisuda-tarif.tpl");
