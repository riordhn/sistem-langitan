<?php
include('config.php');
include('../ppmb/class/Penerimaan.class.php');

$penerimaan = new Penerimaan($db);
$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());


if(isset($_POST['id_penerimaan'])){

	$view_kesehatan = $db->QueryToArray("SELECT TO_CHAR(TGL_TEST, 'DD-MM-YYYY') AS TGL_TEST, 
										(CASE WHEN MENIT_MULAI = '0' THEN MENIT_MULAI || '0' ELSE TO_CHAR(MENIT_MULAI) END) AS MENIT_MULAI, 
										(CASE WHEN MENIT_SELESAI = '0' THEN MENIT_SELESAI || '0' ELSE TO_CHAR(MENIT_SELESAI) END) AS MENIT_SELESAI, 
										(CASE WHEN length(JAM_MULAI) = 1 THEN '0' || JAM_MULAI ELSE TO_CHAR(JAM_MULAI) END) AS JAM_MULAI, 
										(CASE WHEN length(JAM_SELESAI) = 1 THEN '0' || JAM_SELESAI ELSE TO_CHAR(JAM_SELESAI) END) AS JAM_SELESAI, 
										KELOMPOK_JAM, KAPASITAS, TERISI
										FROM JADWAL_KESEHATAN WHERE ID_PENERIMAAN = '$_POST[id_penerimaan]' ORDER BY TGL_TEST DESC,
										JAM_MULAI, MENIT_MULAI asc");
	$smarty->assign('view_kesehatan', $view_kesehatan);

}


$smarty->display("pendaftaran/jadwal_kesehatan/pendaftaran-jadwal-kesehatan.tpl");
?>
