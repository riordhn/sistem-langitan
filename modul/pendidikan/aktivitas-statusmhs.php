<?php
include('config.php');

$mode = get('mode', 'fakultas');
$id_fakultas = get('id_fakultas', 0);
$id_program_studi = get('id_program_studi', 0);

$fakultas_table = new FAKULTAS_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);
$mahasiswa_table = new MAHASISWA_TABLE($db);
$pengguna_table = new PENGGUNA_TABLE($db);
$sejarah_status_table = new SEJARAH_STATUS_TABLE($db);
$status_pengguna_table = new STATUS_PENGGUNA_TABLE($db);

//tes
if ($request_method == 'POST')
{
    $keys = array_keys($_POST);
    for ($i = 0; $i < count($keys); $i++)
    {
        if (substr($keys[$i], 0, 1) == 'C') { continue; }
        
        $id_pengguna = str_replace('P', '', $keys[$i]);
        
        // Mencari status lama yg aktif
        $sejarah_status_aktif = $sejarah_status_table->SelectWhere("ID_PENGGUNA = {$id_pengguna} AND AKTIF_SEJARAH_STATUS = 1");
        if ($sejarah_status_aktif->Count() > 0)
        {
            $sejarah_status_aktif->Get(0)->AKTIF_SEJARAH_STATUS = 0;
            $sejarah_status_aktif->Get(0)->TGL_SELESAI_SEJARAH_STATUS = date('d-M-Y');
        }
        
        // Mengupdate status yg belum di approve
        $sejarah_status_set = $sejarah_status_table->SelectWhere("ID_PENGGUNA = {$id_pengguna} AND AKTIF_SEJARAH_STATUS = 2");
        
        if ($_POST[$keys[$i]] == 1)
        {
            $sejarah_status_set->Get(0)->AKTIF_SEJARAH_STATUS = 1;
            $sejarah_status_set->Get(0)->TGL_MULAI_SEJARAH_STATUS = date('d-M-Y');
            $sejarah_status_set->Get(0)->KETERANGAN_SEJARAH_STATUS = $_POST["C{$id_pengguna}"];
            
            // update status lama jadi nonaktif
            if ($sejarah_status_aktif->Count() > 0)
                $sejarah_status_table->UpdateSet($sejarah_status_aktif);
        }
        else
        {
            $sejarah_status_set->Get(0)->AKTIF_SEJARAH_STATUS = 3;
            $sejarah_status_set->Get(0)->KETERANGAN_SEJARAH_STATUS = $_POST["C{$id_pengguna}"];
        }
        
        // update status yang baru
        $sejarah_status_table->UpdateSet($sejarah_status_set);
    }
}

if ($mode == 'fakultas')
{
    $fakultas_set = $fakultas_table->Select();
    $smarty->assign('fakultas_set', $fakultas_set);
}
else if ($mode == 'prodi')
{
    $fakultas = $fakultas_table->Single($id_fakultas);
    $program_studi_table->FillFakultas($fakultas);
    $smarty->assign('fakultas', $fakultas);
}
else if ($mode == 'view')
{
    $program_studi = $program_studi_table->Single($id_program_studi);
    $fakultas_table->FillProgramStudi($program_studi);
    
    // Mencari status yang belum di approve
    $criteria = "
        JOIN SEJARAH_STATUS SS ON SS.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
        WHERE ID_PROGRAM_STUDI = {$id_program_studi} AND SS.AKTIF_SEJARAH_STATUS = 2";
    $mahasiswa_set = $mahasiswa_table->SelectCriteria($criteria);
    $pengguna_table->FillMahasiswaSet($mahasiswa_set);

    for ($i = 0; $i < $mahasiswa_set->Count(); $i++)
    {
        $pengguna = $mahasiswa_set->Get($i)->PENGGUNA;
        $sejarah_status_set = $sejarah_status_table->SelectWhere("ID_PENGGUNA = {$pengguna->ID_PENGGUNA} AND AKTIF_SEJARAH_STATUS = 2");
        $status_pengguna_table->FillSejarahStatusSet($sejarah_status_set);
        $pengguna->SEJARAH_STATUSs = $sejarah_status_set;
    }
    
    $smarty->assign('program_studi', $program_studi);
    $smarty->assign('mahasiswa_set', $mahasiswa_set);
}

$smarty->display("aktivitas/statusmhs/$mode.tpl");
?>
gn('mahasiswa_set', $mahasiswa_set);
}

$smarty->display("aktivitas/statusmhs/$mode.tpl");
?>
