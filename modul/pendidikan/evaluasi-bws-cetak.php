<?php
include 'config.php';
include 'class/evaluasi.class.php';
$eval = new evaluasi($db);


require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('EVALUASI BWS');
$pdf->SetSubject('EVALUASI BWS');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$semester = $_GET['sem'];
$id_fakultas = $_GET['fak'];
$jenjang = $_GET['jenj'];


if(isset($_REQUEST['jenj']) and isset($_REQUEST['sem'])){

	$evaluasi =  $db->QueryToArray($eval->evaluasi_bws($id_fakultas, $jenjang, $_REQUEST['prodi'], $semester));
	
}



$biomhs="select j.nm_jenjang, f.id_fakultas, f.alamat_fakultas, ps.nm_program_studi, f.nm_fakultas, 
			f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas
			from program_studi ps
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			where f.id_fakultas='".$id_fakultas."'";
$result1 = $db->Query($biomhs)or die("salah kueri 2 ");
while($r1 = $db->FetchRow()) {
	$nmjenjang = $r1[0];
	$fak = $r1[4];
	$alm_fak = $r1[2];
	$pos_fak = $r1[5];
	$tel_fak = $r1[6];
	$fax_fak = $r1[7];
	$web_fak = $r1[8];
	$eml_fak = $r1[9];
}

$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($alm_fak).", ".$pos_fak."\nTelp. ".$tel_fak.", Fax. ".$fax_fak."\n".strtolower($web_fak).", ".strtolower($eml_fak)."";

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

//set margins
$pdf->SetMargins(5, 40, 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default footer
//$pdf->setPrintFooter(false);
//$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('times', '', 9);

// add a page
$pdf->AddPage('L', 'A4');

$pdf->Image('includes/logo_unair.png', 100, 50, 100, '', '', '', '', false, 72);

// set the starting point for the page content
$pdf->setPageMark();



$html = '
<table width="100%" cellpadding="2" border="0.5">
	<tr bgcolor="green" style="color:#fff;">
		<th style="text-align:center" width="3%">NO</th>
		<th style="text-align:center" width="9%">NIM</th>
		<th style="text-align:center" width="19%">NAMA</th>
		<th style="text-align:center" width="6%">JENJANG</th>
		<th style="text-align:center" width="16%">PRODI</th>
		<th style="text-align:center" width="10%">STATUS TERKINI</th>
		<th style="text-align:center" width="4%">SKS</th>
		<th style="text-align:center" width="4%">IPK</th>
		<th style="text-align:center" width="6%">PIUTANG</th>
		<th style="text-align:center" width="9%">REKOMENDASI</th>
		<th style="text-align:center" width="16%">KETERANGAN</th>
	</tr>';
	$no = 1;
	foreach($evaluasi as $data){
	
	
$html .= '<tr>
		<td style="text-align:center">'.$no++.'</td>
		<td>'.$data['NIM_MHS'].'</td>
		<td>'.$data['NM_PENGGUNA'].'</td>
		<td>'.$data['NM_JENJANG'].'</td>
		<td>'.$data['NM_PROGRAM_STUDI'].'</td>
		<td>'.$data['NM_STATUS_PENGGUNA'].'</td>
		<td style="text-align:center">'.$data['SKS'].'</td>
		<td style="text-align:center">'.$data['IPK'].'</td>
		<td style="text-align:center">'.$data['PIUTANG'].'</td>
		<td>'.$data['NM_STATUS_EVALUASI'].'</td>
		<td>'.$data['KETERANGAN'].'</td>
	</tr>';
	}
$html .= '</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('evaluasi-bws.pdf', 'I');

?>