<?php
include '../../config.php';
include 'class/report_mahasiswa.class.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell;

$rm = new report_mahasiswa($db);
$query = $rm->get_mahasiswa_dayat(2 , get('fakultas'), get('program_studi'), get('tahun_akademik'), get('status'), get('jenjang'), get('jalur'), get('id_pt'), get('bidikmisi'), get('kota'), get('tahun_lulus'), get('kelamin'));

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$sheet->setTitle('Data');
$sheet->setCellValue('A1', 'No');
$sheet->setCellValue('B1', 'NOMOR PENDAFTARAN');
$sheet->setCellValue('C1', 'NIM');
$sheet->setCellValue('D1', 'NAMA');
$sheet->setCellValue('E1', 'JK');
$sheet->setCellValue('F1', 'ANGKATAN');
$sheet->setCellValue('G1', 'PROGRAM STUDI');
$sheet->setCellValue('H1', 'FAKULTAS');
$sheet->setCellValue('I1', 'STATUS');
$sheet->setCellValue('J1', 'ALAMAT');
$sheet->setCellValue('K1', 'TELP');
$sheet->setCellValue('L1', 'TEMPAT LAHIR');
$sheet->setCellValue('M1', 'TGL LAHIR');
$sheet->setCellValue('N1', 'ASAL SEKOLAH');
$sheet->setCellValue('O1', 'NISN');
$sheet->setCellValue('P1', 'NIK');
$sheet->setCellValue('Q1', 'NAMA IBU');

$i = 1;

$db->Query($query);
while ($row = $db->FetchAssoc()) {
	$i_row = $i + 1;
	$sheet->setCellValue('A'.$i_row, $i);
	$sheet->setCellValue('B'.$i_row, $row['NO_UJIAN']);
	//$sheet->setCellValue('C'.$i_row, $row['NIM_MHS']);
	$sheet->getCell('C'.$i_row)->setValueExplicit($row['NIM_MHS'], Cell\DataType::TYPE_STRING);
	$sheet->setCellValue('D'.$i_row, $row['NM_PENGGUNA']);
	$sheet->setCellValue('E'.$i_row, $row['KELAMIN_PENGGUNA'] == '1' ? 'L' : 'P');
	$sheet->setCellValue('F'.$i_row, $row['THN_ANGKATAN_MHS']);
	$sheet->setCellValue('G'.$i_row, $row['NM_JENJANG'] . ' ' . $row['NM_PROGRAM_STUDI']);
	$sheet->setCellValue('H'.$i_row, $row['NM_FAKULTAS']);
	$sheet->setCellValue('I'.$i_row, $row['NM_STATUS_PENGGUNA']);
	$sheet->setCellValue('J'.$i_row, $row['ALAMAT_MHS_BARU']);
	//$sheet->setCellValue('K'.$i_row, $row['MOBILE_MHS']);
	$sheet->getCell('K'.$i_row)->setValueExplicit($row['MOBILE_MHS'], Cell\DataType::TYPE_STRING);
	$sheet->setCellValue('L'.$i_row, $row['KOTA_LAHIR']);
	$sheet->setCellValue('M'.$i_row, $row['TGL_LAHIR_PENGGUNA']);
	$sheet->setCellValue('N'.$i_row, $row['NM_SEKOLAH']);
	//$sheet->setCellValue('O'.$i_row, $row['NISN']);
	$sheet->getCell('O'.$i_row)->setValueExplicit($row['NISN'], Cell\DataType::TYPE_STRING);
	//$sheet->setCellValue('P'.$i_row, $row['NIK_C_MHS']);
	$sheet->getCell('P'.$i_row)->setValueExplicit($row['NIK_C_MHS'], Cell\DataType::TYPE_STRING);
	$sheet->setCellValue('Q'.$i_row, $row['NM_IBU_MHS']);
	$i++;
}

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="data-mahasiswa.xlsx"');
header('Cache-Control: max-age=0');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit();