<?php
include('config.php');
include('../ppmb/class/Penerimaan.class.php');

$penerimaan = new Penerimaan($db);
$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());

if(isset($_POST['tgl_registrasi'])){
	
	$jam_awal = "''";
	$jam_akhir = "''";

	if($_POST['jam_awal'] != ''){
		$jam_awal = "to_date('$_POST[tgl_registrasi] $_POST[jam_awal]','DD-MM-YYYY HH24:MI')";
	}

	if($_POST['jam_awal'] != ''){
		$jam_akhir = "to_date('$_POST[tgl_registrasi] $_POST[jam_akhir]','DD-MM-YYYY HH24:MI')";
	}

	if(isset($_POST['id']) and $_POST['id'] != ''){
		echo "UPDATE JADWAL_VERIFIKASI_PENDIDIKAN SET KUOTA = '$_POST[kuota]', TGL_REGISTRASI = to_date('$_POST[tgl_registrasi]','DD-MM-YYYY'),
					JAM_AWAL = $jam_awal, 
					JAM_AKHIR = $jam_akhir
					WHERE ID_JADWAL_VERIFIKASI_PEND = '$_POST[id]'";
		$db->Query("UPDATE JADWAL_VERIFIKASI_PENDIDIKAN SET KUOTA = '$_POST[kuota]', TGL_REGISTRASI = to_date('$_POST[tgl_registrasi]','DD-MM-YYYY'),
					JAM_AWAL = $jam_awal, 
					JAM_AKHIR = $jam_akhir
					WHERE ID_JADWAL_VERIFIKASI_PEND = '$_POST[id]'");

	}else{
				$db->Query("INSERT INTO JADWAL_VERIFIKASI_PENDIDIKAN (ID_PENERIMAAN, KUOTA, TGL_REGISTRASI, JAM_AWAL, JAM_AKHIR) 
				VALUES ('$_POST[id_penerimaan]', $_POST[kuota], to_date('$_POST[tgl_registrasi]','DD-MM-YYYY'), 
				to_date('$_POST[tgl_registrasi] $_POST[jam_awal]','DD-MM-YYYY HH24:MI'), 
				to_date('$_POST[tgl_registrasi] $_POST[jam_akhir]','DD-MM-YYYY HH24:MI'))");
	}
}


if(isset($_POST['id_penerimaan']) or isset($_GET['id'])){

	if(isset($_GET['mode']) and $_GET['mode'] == 'delete'){
		$db->Query("DELETE JADWAL_VERIFIKASI_PENDIDIKAN WHERE ID_JADWAL_VERIFIKASI_PEND = '$_GET[id]'");
	}

	if(isset($_GET['id']) and $_GET['mode'] == 'edit'){

	$db->Query("SELECT KUOTA, TO_CHAR(TGL_REGISTRASI, 'DD-MM-YYYY') AS TGL_REGISTRASI, ID_JADWAL_VERIFIKASI_PEND , TO_CHAR(JAM_AWAL, 'HH24:MI') AS JAM_AWAL, TO_CHAR(JAM_AKHIR, 'HH24:MI') AS JAM_AKHIR
											FROM JADWAL_VERIFIKASI_PENDIDIKAN WHERE ID_JADWAL_VERIFIKASI_PEND = '$_GET[id]'");
	$edit = $db->FetchArray();									
	$smarty->assign('edit', $edit);

	}

	$kuota_registrasi = $db->QueryToArray("SELECT KUOTA, TO_CHAR(JAM_AWAL, 'HH24:MI') AS JAM_AWAL, 
							TO_CHAR(JAM_AKHIR, 'HH24:MI') AS JAM_AKHIR, TO_CHAR(TGL_REGISTRASI, 'DD-MM-YYYY') AS TGL_REGISTRASI, 
							ID_JADWAL_VERIFIKASI_PEND, ID_PENERIMAAN, 
							(SELECT COUNT(*) FROM CALON_MAHASISWA_BARU 
							JOIN CALON_MAHASISWA_DATA ON CALON_MAHASISWA_BARU.ID_C_MHS = CALON_MAHASISWA_DATA.ID_C_MHS 
							WHERE CALON_MAHASISWA_DATA.ID_JADWAL_VERIFIKASI_PEND = JADWAL_VERIFIKASI_PENDIDIKAN.ID_JADWAL_VERIFIKASI_PEND) AS TERISI
							FROM JADWAL_VERIFIKASI_PENDIDIKAN WHERE ID_PENERIMAAN = '$_POST[id_penerimaan]' OR ID_PENERIMAAN = '$_GET[id_penerimaan]'
							ORDER BY TGL_REGISTRASI, JAM_AWAL");
	$smarty->assign('kuota_registrasi', $kuota_registrasi);

}




$smarty->display('pendaftaran/kuota/pendaftaran-kuota-registrasi.tpl');
?>
