<?php
include('config.php');

$nim_mhs = get('nim_mhs', 0);

$mahasiswa_table = new MAHASISWA_TABLE($db);
$pengguna_table = new PENGGUNA_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);
$pengambilan_mk_table = new PENGAMBILAN_MK_TABLE($db);
$kelas_mk_table = new KELAS_MK_TABLE($db);
$mata_kuliah_table = new MATA_KULIAH_TABLE($db);
$semester_table = new SEMESTER_TABLE($db);
$mhs_status_table = new MHS_STATUS_TABLE($db);
$mahasiswa_found = false;

if ($nim_mhs != 0)
{
    $mahasiswa_set = $mahasiswa_table->SelectWhere("NIM_MHS = '{$nim_mhs}'");
    if ($mahasiswa_set->Count() > 0)
    {
        // isi data-data foreign key yg berelasi
        $mahasiswa = $mahasiswa_set->Get(0);
        $pengguna_table->FillMahasiswa($mahasiswa);
        $program_studi_table->FillMahasiswa($mahasiswa);
        $mahasiswa_found = true;
        
        // Mengisi data status per semester
        $mhs_status_table->FillMahasiswa($mahasiswa);
        $semester_table->FillMhsStatusSet($mahasiswa->MHS_STATUSs);
        
        for ($i = 0; $i < $mahasiswa->MHS_STATUSs->Count(); $i++)
        {
            // Mengisi data pengambilan mk
            $mhs_status = $mahasiswa->MHS_STATUSs->Get($i);
            $mhs_status->PENGAMBILAN_MKs =  $pengambilan_mk_table->SelectWhere("
                ID_MHS = {$mahasiswa->ID_MHS} AND ID_SEMESTER = {$mhs_status->ID_SEMESTER} AND STATUS_APV_PENGAMBILAN_MK = 1 AND 
                (STATUS_PENGAMBILAN_MK = 1 OR STATUS_PENGAMBILAN_MK = 2 OR STATUS_PENGAMBILAN_MK = 3)
                ORDER BY ID_SEMESTER");
        }
        
        $smarty->assign('mahasiswa', $mahasiswa);
    }
}

$smarty->assign('mahasiswa_found', $mahasiswa_found);

$smarty->display('history/ipk.tpl');
?>