<?php

include 'config.php';
include 'class/beasiswa.class.php';

$b = new beasiswa($db, $user->ID_ROLE);

if (isset($_POST)) {
    if (post('mode') == 'tambah') {
        $b->tambah_pengumuman_beasiswa(post('beasiswa'), post('besar'), post('syarat'), post('keterangan'), post('batas'));
    } else if (post('mode') == 'edit') {
        $b->update_pengumuman_beasiswa(post('id'), post('beasiswa'), post('besar'), post('syarat'), post('keterangan'), post('batas'));
    } else if (post('mode') == 'hapus') {
        $b->delete_pengumuman_beasiswa(post('id'));
    }
}

if (get('mode') == 'tambah') {
    $smarty->assign('data_beasiswa', $b->load_beasiswa(0));
} else if (get('mode') == 'edit' || get('mode') == 'hapus') {
    $smarty->assign('data_beasiswa', $b->load_beasiswa(0));
    $smarty->assign('pengumuman', $b->get_pengumuman_beasiswa(get('id')));
}

$smarty->assign('data_pengumuman', $b->load_pengumuman_beasiswa());
$smarty->display("beasiswa/pengumuman-beasiswa.tpl");
?>
