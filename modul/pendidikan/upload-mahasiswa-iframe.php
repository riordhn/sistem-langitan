<?php
include '../../config.php';
include 'class/aucc.class.php';

$id_pt = $id_pt_user;

$aucc = new AUCC_Pendidikan($db);

$mode = 'view';

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	// Jika tidak ada masalah upload
	if ($_FILES['file_mhs']['error'] == UPLOAD_ERR_INI_SIZE || $_FILES['file_mhs']['error'] == UPLOAD_ERR_FORM_SIZE)
	{
		echo "Ukuran file terlalu besar"; exit();
	}
	else if ($_FILES['file_mhs']['error'] == UPLOAD_ERR_NO_FILE)
	{
		echo "File tidak ada"; exit();
	}
	else if ($_FILES['file_mhs']['error'] == UPLOAD_ERR_OK)
	{

		// cara load file excel
		$excel = PhpOffice\PhpSpreadsheet\IOFactory::load($_FILES['file_mhs']['tmp_name']);

		// ambil sheet pertama
		$sheet = $excel->getSheet();

		// Perulangan
		for ($pRow = 2; $pRow <= $sheet->getHighestRow(); $pRow++)
		{
			// mengambil cell baris ke pRow kolom ke 2
			//$cell = $sheet->getCellByColumnAndRow(2, $pRow);  // k

			$nim = $sheet->getCellByColumnAndRow(0, $pRow)->getFormattedValue();
			$prodi1 = $sheet->getCellByColumnAndRow(1, $pRow)->getFormattedValue();
			$jalur = $sheet->getCellByColumnAndRow(2, $pRow)->getFormattedValue();
			$angkatan = $sheet->getCellByColumnAndRow(3, $pRow)->getFormattedValue();
			$nama = $sheet->getCellByColumnAndRow(4, $pRow)->getFormattedValue();

				//cek master pengguna dan kebutuhan input
				$pengguna       = $aucc->cek_pengguna($nim,$id_pt);
			    $mahasiswa      = $aucc->cek_mahasiswa_by_perguruan_tinggi($nim,$id_pt);
			    $jalur_mhs      = $aucc->cek_jalur($nim,$id_pt);

			    //cek master data sebelum input
			    $prodi          = $aucc->cek_prodi($prodi1,$id_pt);
			    $jalur          = $aucc->cek_nama_jalur($jalur,$id_pt);
			    $semester       = $aucc->cek_semester_by_angkatan($angkatan,$id_pt);

			    
			   /* if (empty($jalur)) {
			        $aucc->insert_master_jalur($_POST['jalur'], $id_pt);
			    }*/
			    

			    if(empty($prodi)){
			    	echo "Data Prodi Belum Terdaftar, Error Pada Baris Ke-".$pRow."</br>";
			    	exit();
			    }
			    if(empty($jalur)){
			    	echo "Data Jalur Belum Ada Pada Master, Error Pada Baris Ke-".$pRow."</br>";
			    	exit();
			    }
			    if(empty($semester)){
			    	echo "Data Angkatan Belum Ada Pada Master Semester, Error Pada Baris Ke-".$pRow."</br>";
			    	exit();
			    }


			    if(!empty($prodi) && !empty($jalur) && !empty($semester)){
			        if(empty($pengguna)){
			            $nama_pgg =  str_replace("'", "''", $nama);
			            $aucc->insert_pengguna_mahasiswa(stripslashes($nama_pgg), $nim, null, $id_pt);            
			        }
			        else{
			        	$nama_pgg =  str_replace("'", "''", $nama);
			            $aucc->update_pengguna_mahasiswa(stripslashes($nama_pgg), $nim, $id_pt);
			            //echo "Data Pengguna Sudah Ada";
			        }

			        if(empty($mahasiswa)){
			            $pengguna1 = $aucc->cek_pengguna($nim,$id_pt);
			            //echo "hmm ".$pengguna1['ID_PENGGUNA'];
			            $aucc->insert_mahasiswa($pengguna1['ID_PENGGUNA'], $nim, $prodi['ID_PROGRAM_STUDI'], NULL, $angkatan, $semester['ID_SEMESTER']);
			        }
			        else{
			        	$pengguna1 = $aucc->cek_pengguna($nim,$id_pt);
			            $aucc->update_mahasiswa_baru($pengguna1['ID_PENGGUNA'], $nim, $prodi['ID_PROGRAM_STUDI'], NULL, $angkatan, $semester['ID_SEMESTER']);
			            //echo "Data Mahasiswa Sudah Ada";
			        }

			        if(empty($jalur_mhs)){
			            $mahasiswa1 = $aucc->cek_mahasiswa_by_perguruan_tinggi($nim,$id_pt);
			            $aucc->insert_jalur_mahasiswa($mahasiswa1['ID_MHS'], $prodi['ID_PROGRAM_STUDI'], $jalur['ID_JALUR'], $semester['ID_SEMESTER'], $nim);
			        }
			        else{
			            echo "Data Jalur Mahasiswa Sudah Ada, Error Pada Baris Ke-".$pRow."</br>";
			        }
			    }

		}


		/*// convert csv menjadi array
		$csv = array_map('str_getcsv', file($_FILES['file_mhs']['tmp_name']));

		// Proses disini :
		// Edited By : FIKRIE
		// 21-09-2015
		$i=0;
		foreach ($csv as $baris) {
			$i++;
			if($i>1){
				$nim 		= $baris[0];
				$prodi 		= $baris[1];
				$jalur 		= $baris[2];
				$angkatan 	= $baris[3];
				$nama	 	= $baris[4];

				//cek master pengguna dan kebutuhan input
				$pengguna       = $aucc->cek_pengguna($nim,$id_pt);
			    $mahasiswa      = $aucc->cek_mahasiswa_by_perguruan_tinggi($nim,$id_pt);
			    $jalur_mhs      = $aucc->cek_jalur($nim,$id_pt);

			    //cek master data sebelum input
			    $prodi          = $aucc->cek_prodi($prodi,$id_pt);
			    $jalur          = $aucc->cek_nama_jalur($jalur,$id_pt);
			    $semester       = $aucc->cek_semester_by_angkatan($angkatan,$id_pt);

			    /*
			    if (empty($jalur)) {
			        $aucc->insert_master_jalur($_POST['jalur'], $id_pt);
			    }
			    

			    if(empty($prodi)){
			    	echo "Data Prodi Belum Terdaftar, Error Pada Baris Ke-".$i."</br>";
			    	exit();
			    }
			    if(empty($jalur)){
			    	echo "Data Jalur Belum Ada Pada Master, Error Pada Baris Ke-".$i."</br>";
			    	exit();
			    }
			    if(empty($semester)){
			    	echo "Data Angkatan Belum Ada Pada Master Semester, Error Pada Baris Ke-".$i."</br>";
			    	exit();
			    }


			    if(!empty($prodi) && !empty($jalur) && !empty($semester)){
			        if(empty($pengguna)){
			            $nama_pgg =  str_replace("'", "''", $nama);
			            $aucc->insert_pengguna_mahasiswa(stripslashes($nama_pgg), $nim, $id_pt);            
			        }
			        else{
			        	$nama_pgg =  str_replace("'", "''", $nama);
			            $aucc->update_pengguna_mahasiswa(stripslashes($nama_pgg), $nim, $id_pt);
			            //echo "Data Pengguna Sudah Ada";
			        }

			        if(empty($mahasiswa)){
			            $pengguna1 = $aucc->cek_pengguna($nim,$id_pt);
			            //echo "hmm ".$pengguna1['ID_PENGGUNA'];
			            $aucc->insert_mahasiswa($pengguna1['ID_PENGGUNA'], $nim, $prodi['ID_PROGRAM_STUDI'], NULL, $angkatan, $semester['ID_SEMESTER']);
			        }
			        else{
			        	$pengguna1 = $aucc->cek_pengguna($nim,$id_pt);
			            $aucc->update_mahasiswa_baru($pengguna1['ID_PENGGUNA'], $nim, $prodi['ID_PROGRAM_STUDI'], NULL, $angkatan, $semester['ID_SEMESTER']);
			            //echo "Data Mahasiswa Sudah Ada";
			        }

			        if(empty($jalur_mhs)){
			            $mahasiswa1 = $aucc->cek_mahasiswa_by_perguruan_tinggi($nim,$id_pt);
			            $aucc->insert_jalur_mahasiswa($mahasiswa1['ID_MHS'], $prodi['ID_PROGRAM_STUDI'], $jalur['ID_JALUR'], $semester['ID_SEMESTER'], $nim);
			        }
			        else{
			            echo "Data Jalur Mahasiswa Sudah Ada, Error Pada Baris Ke-".$i."</br>";
			        }
			    }
			}

		}*/

		echo "Data Mahasiswa Berhasil Dimasukkan";

		exit();
	}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	$smarty->display('mahasiswa/upload/iframe.tpl');
}

