<?php
include('config.php');

$mode = get('mode', 'fakultas');
$id_fakultas = get('id_fakultas', '');
$id_program_studi = get('id_program_studi', '');

if ($mode == 'fakultas')
{
	$smarty->assign('fakultas_set', $db->QueryToArray("
		select f.id_fakultas, f.nm_fakultas, count(id_c_mhs) as jumlah_calon from calon_mahasiswa cm
		join program_studi ps on ps.id_program_studi = cm.id_program_studi
		join fakultas f on f.id_fakultas = ps.id_fakultas
		where cm.id_c_mhs in (select id_c_mhs from pembayaran_cmhs where tgl_bayar is not null)
		group by f.id_fakultas, f.nm_fakultas
		order by f.id_fakultas"));
}

if ($mode == 'prodi' && $id_fakultas != '')
{
	$db->Query("SELECT * FROM FAKULTAS WHERE ID_FAKULTAS = {$id_fakultas}");
	$row = $db->FetchAssoc();
	$smarty->assign('nm_fakultas', $row['NM_FAKULTAS']);
	
	$smarty->assign('program_studi_set', $db->QueryToArray("
		select ps.id_program_studi, j.nm_jenjang, ps.nm_program_studi, count(id_c_mhs) as jumlah_calon from calon_mahasiswa cm
		join program_studi ps on ps.id_program_studi = cm.id_program_studi
		join jenjang j on j.id_jenjang = ps.id_jenjang
		where cm.id_c_mhs in (select id_c_mhs from pembayaran_cmhs where tgl_bayar is not null) and ps.id_fakultas = {$id_fakultas}
		group by ps.id_program_studi, j.nm_jenjang, j.id_jenjang, ps.nm_program_studi
		order by j.id_jenjang"));
}

if ($mode == 'detail' && $id_program_studi != '')
{
	// nama prodi
	$db->Query("SELECT NM_JENJANG, NM_PROGRAM_STUDI, ID_FAKULTAS FROM PROGRAM_STUDI JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG WHERE ID_PROGRAM_STUDI = {$id_program_studi}");
	$row = $db->FetchAssoc();
	$smarty->assign('nm_program_studi', "{$row['NM_JENJANG']} {$row['NM_PROGRAM_STUDI']}");
	$smarty->assign('id_fakultas', $row['ID_FAKULTAS']);
	
	$smarty->assign('calon_mahasiswa_set', $db->QueryToArray("
		SELECT NO_UJIAN, NM_JALUR, NM_C_MHS, NIM_MHS FROM CALON_MAHASISWA CM
		LEFT JOIN MAHASISWA M ON M.ID_C_MHS = CM.ID_C_MHS
		LEFT JOIN JALUR J ON J.ID_JALUR = CM.ID_JALUR
		WHERE CM.ID_C_MHS IN (SELECT ID_C_MHS FROM PEMBAYARAN_CMHS WHERE TGL_BAYAR IS NOT NULL) AND
			CM.ID_PROGRAM_STUDI = {$id_program_studi}
		ORDER BY M.NIM_MHS"));
}

/**

$fakultas_table = new FAKULTAS_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);

if ($mode == 'fakultas')
{
    // Mendapatkan daftar fakultas
    $fakultas_set = $fakultas_table->Select();

    // Mendapatkan data calon dari database
    $fakultas_info = array();
    $db->Query("
        SELECT F.ID_FAKULTAS, COUNT(PJ.ID_C_MHS) AS JUMLAH_CALON FROM FAKULTAS F
        JOIN PROGRAM_STUDI PS ON PS.ID_FAKULTAS = F.ID_FAKULTAS
        JOIN PILIHAN_JALUR PJ ON PJ.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
        JOIN CALON_MAHASISWA CM ON CM.ID_C_MHS = PJ.ID_C_MHS
        WHERE CM.STATUS = 0
        GROUP BY F.ID_FAKULTAS");
    while ($row = $db->FetchAssoc())
        array_push($fakultas_info, $row);
    
    // Membindingkan dengan data fakultas_set
    for ($i = 0; $i < $fakultas_set->Count(); $i++)
    {
        $exist = false;
        for ($j = 0; $j < count($fakultas_info); $j++)
        {
            if ($fakultas_set->Get($i)->ID_FAKULTAS == $fakultas_info[$j]['ID_FAKULTAS'])
            {
                $fakultas_set->Get($i)->JUMLAH_CALON = $fakultas_info[$j]['JUMLAH_CALON'];
                $exist = true;
                break;
            }
        }
        
        if (!$exist)
        {
            $fakultas_set->Get($i)->JUMLAH_CALON = 0;
        }
    }
    
    $smarty->assign('fakultas_set', $fakultas_set);
}
else if ($mode == 'prodi')
{
    // Mendapatkan fakultas, program studi
    $fakultas = $fakultas_table->Single($id_fakultas);
    $program_studi_set = $program_studi_table->SelectWhere("ID_FAKULTAS = {$id_fakultas}");
    
    // Mendapatkan data calon mahasiswa dari database
    $program_studi_info = array();
    $db->Query("
        SELECT PS.ID_PROGRAM_STUDI, COUNT(PJ.ID_C_MHS) AS JUMLAH_CALON FROM FAKULTAS F
        JOIN PROGRAM_STUDI PS ON PS.ID_FAKULTAS = F.ID_FAKULTAS
        JOIN PILIHAN_JALUR PJ ON PJ.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
        JOIN CALON_MAHASISWA CM ON CM.ID_C_MHS = PJ.ID_C_MHS
        WHERE CM.STATUS = 0 AND PS.ID_FAKULTAS = {$id_fakultas}
        GROUP BY PS.ID_PROGRAM_STUDI");
    while ($row = $db->FetchAssoc())
        array_push($program_studi_info, $row);
    
    // Melakukan binding dengan data program studi
    for ($i = 0; $i < $program_studi_set->Count(); $i++)
    {
        $exist = false;
        for ($j = 0; $j < count($program_studi_info); $j++)
        {
            if ($program_studi_set->Get($i)->ID_PROGRAM_STUDI == $program_studi_info[$j]['ID_PROGRAM_STUDI'])
            {
                $program_studi_set->Get($i)->JUMLAH_CALON = $program_studi_info[$j]['JUMLAH_CALON'];
                $exist = true;
                break;
            }
        }
        
        if (!$exist)
        {
            $program_studi_set->Get($i)->JUMLAH_CALON = 0;
        }
    }
        
    $smarty->assign('fakultas', $fakultas);
    $smarty->assign('program_studi_set', $program_studi_set);
}
**/

$smarty->display("mahasiswa/calon/{$mode}.tpl");
?>
