<?php
include('config.php');
include 'class/report_mahasiswa.class.php';
include('../ppmb/class/Penerimaan.class.php');

$penerimaan = new Penerimaan($db);
$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());

$rm = new report_mahasiswa($db);
$mode = get('mode', 'view');

if ($request_method == 'POST')
{
	
	$smarty->assign('fakultas', post('fakultas'));
	$smarty->assign('jenjang', post('jenjang'));
	$smarty->assign('jalur', post('jalur'));
	$smarty->assign('gelombang', post('gelombang'));
	$smarty->assign('thn', post('thn'));
	$smarty->assign('semester', post('semester'));
	$smarty->assign('penerimaan', $rm->get_penerimaan(post('fakultas'), post('jenjang'), post('jalur'), post('gelombang'), post('thn'), post('semester'), post('tgl_awal'), post('tgl_akhir'), post('id_penerimaan')));
}

if(isset($_GET['get_id']) or isset($_GET['get_jenjang'])){
	$smarty->assign('get_fakultas', $_GET['get_fakultas']);
	$smarty->assign('get_id', $_GET['get_id']);
	$smarty->assign('get_prodi', $_GET['get_prodi']);
	$smarty->assign('fakultas', get('get_fakultas'));
	$smarty->assign('jenjang', get('get_jenjang'));
	$smarty->assign('jalur', get('get_jalur'));
	$smarty->assign('gelombang', get('get_gel'));
	$smarty->assign('thn', get('get_thn'));
	$smarty->assign('semester', get('get_semester'));
	$penerimaan = $rm->get_penerimaan_detail($_GET['get_fakultas'], $_GET['get_id'], $_GET['get_prodi'], $_GET['get_jenjang'], $_GET['get_jalur'], $_GET['get_gel'], $_GET['get_thn'], $_GET['get_semester'], $_GET['tgl_awal'], $_GET['tgl_akhir']);
	$smarty->assign('get_penerimaan_detail', $penerimaan);
}

$smarty->assign('data_fakultas', $rm->get_fakultas());
$smarty->assign('data_jenjang', $rm->get_jenjang_penerimaan());
$smarty->assign('data_jalur', $rm->get_jalur_penerimaan());
$smarty->assign('data_thn', $rm->get_thn_penerimaan());

$smarty->display("pendaftaran/registrasi/{$mode}.tpl");
?>
