<?php
include 'config.php';

$mhs_found = false;

if ($request_method == 'POST')
{
	if (post('mode') == 'set_cekal')
	{
		$pengguna = $user->ID_PENGGUNA;
		$tgl = date('Y-m-d');
		$cekal = $_POST['status_cekal'];
		$keterangan = $_POST['keterangan'];
		$db->Query("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True' AND ID_PERGURUAN_TINGGI = '{$id_pt_user}'");
		$row = $db->FetchAssoc();
		$db->Query("SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS = '" . post('nim_mhs') . "'");
		$row2 = $db->FetchAssoc();


		$db->Query("INSERT INTO CEKAL_PEMBAYARAN (ID_MHS, ID_PENGGUNA, ID_SEMESTER, STATUS_CEKAL, TGL_UBAH, KETERANGAN)
					VALUES ('$row2[ID_MHS]','$pengguna','$row[ID_SEMESTER]','$cekal',to_date('$tgl', 'YYYY-MM-DD'), '$keterangan')");
		$return = $db->Query("UPDATE MAHASISWA SET STATUS_CEKAL = '" . post('status_cekal') . "' WHERE NIM_MHS = '{$_POST['nim_mhs']}'");
		//write_log('Status Cekal Telah DI update oleh ID_PENGGUNA = ' . $user->ID_PENGGUNA . ' Menjadi' . $_POST['status_cekal'].' ');
	}
}

if (get('mode') == 'search')
{
	$db->Query(
		"SELECT
			M.ID_MHS,
            M.NIM_MHS, TO_CHAR(P.TGL_LAHIR_PENGGUNA, 'YYYY-MM-DD') AS TGL_LAHIR_PENGGUNA, NM_PENGGUNA, SE1 AS PASSWORD_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, THN_ANGKATAN_MHS, 
			M.STATUS_CEKAL, FINGERPRINT_CMHS.FINGER_DATA AS FINGER_DATA3, FINGERPRINT_MAHASISWA.TGL_LAHIR, 
			M.NM_AYAH_MHS, M.NM_IBU_MHS, NM_STATUS_PENGGUNA, CEKAL_PEMBAYARAN.KETERANGAN, M.STATUS_AKADEMIK_MHS, FINGERPRINT_MAHASISWA.FINGER_DATA, AKTIF_STATUS_PENGGUNA
        FROM MAHASISWA M
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
        LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
		LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = M.STATUS_AKADEMIK_MHS
		LEFT JOIN CEKAL_PEMBAYARAN ON CEKAL_PEMBAYARAN.ID_MHS = M.ID_MHS
		LEFT JOIN FINGERPRINT_MAHASISWA ON FINGERPRINT_MAHASISWA.NIM_MHS = M.NIM_MHS
		LEFT JOIN FINGERPRINT_CMHS ON FINGERPRINT_CMHS.ID_C_MHS = M.ID_C_MHS
        WHERE M.NIM_MHS = '{$_GET['nim_mhs']}' AND STATUS_PENGGUNA.STATUS_AKTIF = 1 AND P.ID_PERGURUAN_TINGGI = '{$id_pt_user}'
		ORDER BY ID_CEKAL_PEMBAYARAN DESC");
	$mahasiswa = $db->FetchAssoc();

	if ($mahasiswa)
	{
		$cekal_mhs = $db->QueryToArray("SELECT NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_STATUS_PENGGUNA, CEKAL_PEMBAYARAN.KETERANGAN, MAHASISWA.STATUS_CEKAL,
				CEKAL_PEMBAYARAN.STATUS_CEKAL AS CEKAL_UNIVERSITAS, NM_SEMESTER, TAHUN_AJARAN, CEKAL_PEMBAYARAN.ID_MHS, MAHASISWA.ID_MHS AS MHS_ID
				FROM MAHASISWA 
				JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
				JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
				JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				LEFT JOIN CEKAL_PEMBAYARAN ON CEKAL_PEMBAYARAN.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = CEKAL_PEMBAYARAN.ID_SEMESTER
				WHERE MAHASISWA.ID_MHS = {$mahasiswa['ID_MHS']}
				ORDER BY ID_CEKAL_PEMBAYARAN DESC");
				
		$smarty->assign('cekal_mhs', $cekal_mhs);
		
		$mhs_found = true;
		
		// Format Indonesia
		$mahasiswa['TGL_LAHIR_PENGGUNA'] = strftime('%d %B %Y', strtotime($mahasiswa['TGL_LAHIR_PENGGUNA']));

		$smarty->assign('status_cekal', array('0' => 'TIDAK DICEKAL', '1' => 'CEKAL'));
		$smarty->assign('mahasiswa', $mahasiswa);
	}
}

$smarty->display('mahasiswa/status/ubah-status.tpl');