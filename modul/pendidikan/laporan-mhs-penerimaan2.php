<?php
include('config.php');
include "class/laporan.class.php";
include 'class/report_mahasiswa.class.php';
include('../ppmb/class/Penerimaan.class.php');

$aucc = new laporan($db);
$rm = new report_mahasiswa($db);
$penerimaan = new Penerimaan($db);

if(isset($_POST['tampilan'])){
		
	if($_POST['tampilan'] == 'fak'){
	foreach($aucc->fakultas() as $fak){
		$tampil .=  "
		<tr>
			<td>".$fak['NM_FAKULTAS']."</td>";
		
		$total_diterima = 0;
		$total_du = 0;
		$total_tdu = 0;
		

		foreach($aucc->jalur_penerimaan($_POST['thn'], '', $_POST['jenjang'], '') as $nm_penerimaan){
			
			$db->Query($aucc->penerimaan2($fak['ID_FAKULTAS'], $_POST['jenjang'], '', $_POST['thn'], $_POST['tampilan'], $nm_penerimaan['NAMA_JALUR']));
			$mhs = $db->FetchAssoc();
			$tampil .= "<td>".$mhs['DITERIMA']."</td>";
			$tampil .= "<td>".$mhs['DAFTAR_ULANG']."</td>";
			$tampil .= "<td>".$mhs['TIDAK_DAFTAR_ULANG']."</td>";
			
		}
		
		
		
		$tampil .= "</tr>";
	}
	
	}else{

	foreach($aucc->program_studi_all('', $_POST['jenjang'], '') as $prodi){
	$tampil .=  "
		<tr>
			<td>".$prodi['NM_FAKULTAS']."</td>
			<td>".$prodi['NM_PROGRAM_STUDI']."</td>
			<td>".$prodi['NM_JENJANG']."</td>";
			$jml = 0;
		foreach($aucc->jalur_penerimaan($_POST['thn'], '', $_POST['jenjang'], '') as $nm_penerimaan){
			
			$db->Query($aucc->penerimaan2('', $_POST['jenjang'], $prodi['ID_PROGRAM_STUDI'], $_POST['thn'], $_POST['tampilan'], $nm_penerimaan['NAMA_JALUR']));
			$mhs = $db->FetchAssoc();
			
			$tampil .= "<td>".$mhs['DITERIMA']."</td>";
			$tampil .= "<td>".$mhs['DAFTAR_ULANG']."</td>";
			$tampil .= "<td>".$mhs['TIDAK_DAFTAR_ULANG']."</td>";
		}
	$tampil .= "</tr>";
	}

}

	
}


$smarty->assign('data_thn', $rm->get_thn_penerimaan());
$smarty->assign('jenjang', $aucc->jenjang());
$smarty->assign('jalur_mhs', $aucc->jalur_penerimaan($_POST['thn'], '', $_POST['jenjang'], ''));
$smarty->assign('tampil', $tampil);
$smarty->display('laporan/laporan-mhs-penerimaan2.tpl');
?>
