<?php
include('config.php');

$mode = get('mode', 'fakultas');
$id_fakultas = get('id_fakultas', 0);

$fakultas_table = new FAKULTAS_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);

if ($mode == 'fakultas')
{
    // Mendapatkan daftar fakultas
    $fakultas_set = $fakultas_table->Select();

    // Mendapatkan data calon dari database
    $fakultas_info = array();
    $db->Query("
        SELECT F.ID_FAKULTAS, COUNT(PJ.ID_C_MHS) AS JUMLAH_CALON FROM FAKULTAS F
        JOIN PROGRAM_STUDI PS ON PS.ID_FAKULTAS = F.ID_FAKULTAS
        JOIN PILIHAN_JALUR PJ ON PJ.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
        JOIN CALON_MAHASISWA CM ON CM.ID_C_MHS = PJ.ID_C_MHS
        WHERE CM.STATUS = 2
        GROUP BY F.ID_FAKULTAS");
    while ($row = $db->FetchAssoc())
        array_push($fakultas_info, $row);
    
    // Membindingkan dengan data fakultas_set
    for ($i = 0; $i < $fakultas_set->Count(); $i++)
    {
        $exist = false;
        for ($j = 0; $j < count($fakultas_info); $j++)
        {
            if ($fakultas_set->Get($i)->ID_FAKULTAS == $fakultas_info[$j]['ID_FAKULTAS'])
            {
                $fakultas_set->Get($i)->JUMLAH_CALON = $fakultas_info[$j]['JUMLAH_CALON'];
                $exist = true;
                break;
            }
        }
        
        if (!$exist)
        {
            $fakultas_set->Get($i)->JUMLAH_CALON = 0;
        }
    }
    
    $smarty->assign('fakultas_set', $fakultas_set);
}
else if ($mode == 'prodi')
{
    // Mendapatkan fakultas, program studi
    $fakultas = $fakultas_table->Single($id_fakultas);
    $program_studi_set = $program_studi_table->SelectWhere("ID_FAKULTAS = {$id_fakultas}");
    
    // Mendapatkan data calon mahasiswa dari database
    $program_studi_info = array();
    $db->Query("
        SELECT PS.ID_PROGRAM_STUDI, COUNT(PJ.ID_C_MHS) AS JUMLAH_CALON FROM FAKULTAS F
        JOIN PROGRAM_STUDI PS ON PS.ID_FAKULTAS = F.ID_FAKULTAS
        JOIN PILIHAN_JALUR PJ ON PJ.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
        JOIN CALON_MAHASISWA CM ON CM.ID_C_MHS = PJ.ID_C_MHS
        WHERE CM.STATUS = 2 AND PS.ID_FAKULTAS = {$id_fakultas}
        GROUP BY PS.ID_PROGRAM_STUDI");
    while ($row = $db->FetchAssoc())
        array_push($program_studi_info, $row);
    
    // Melakukan binding dengan data program studi
    for ($i = 0; $i < $program_studi_set->Count(); $i++)
    {
        $exist = false;
        for ($j = 0; $j < count($program_studi_info); $j++)
        {
            if ($program_studi_set->Get($i)->ID_PROGRAM_STUDI == $program_studi_info[$j]['ID_PROGRAM_STUDI'])
            {
                $program_studi_set->Get($i)->JUMLAH_CALON = $program_studi_info[$j]['JUMLAH_CALON'];
                $exist = true;
                break;
            }
        }
        
        if (!$exist)
        {
            $program_studi_set->Get($i)->JUMLAH_CALON = 0;
        }
    }
        
    $smarty->assign('fakultas', $fakultas);
    $smarty->assign('program_studi_set', $program_studi_set);
}

$smarty->display("mahasiswa/seleksi/{$mode}.tpl");
?>
