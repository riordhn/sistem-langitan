<?php
include('config.php');

$mode = get('mode', 'view');
$ID_EVAL_KELOMPOK_ASPEK = get('id_eval_kelompok_aspek', 0);
 
	$evaluasi_kelompok_aspek_set = $db->QueryToArray("SELECT * FROM EVALUASI_KELOMPOK_ASPEK a, EVALUASI_INSTRUMEN b WHERE a.ID_EVAL_INSTRUMEN=b.ID_EVAL_INSTRUMEN
	and b.id_role = 11 and b.ID_PERGURUAN_TINGGI = '{$id_pt_user}'
	ORDER BY a.ID_EVAL_KELOMPOK_ASPEK");
    $smarty->assign('evaluasi_kelompok_aspek_set', $evaluasi_kelompok_aspek_set);

	$instrument = $db->QueryToArray("SELECT * FROM EVALUASI_INSTRUMEN b WHERE b.id_role = 11 and b.ID_PERGURUAN_TINGGI = '{$id_pt_user}'");
    $smarty->assign('instrument', $instrument);

	$evaluasi_kelompok_nilai_set = $db->QueryToArray("SELECT * FROM EVALUASI_KELOMPOK_NILAI WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}'");
    $smarty->assign('evaluasi_kelompok_nilai_set', $evaluasi_kelompok_nilai_set);

	$skala_nilai = $db->QueryToArray("SELECT * FROM EVALUASI_NILAI 
					JOIN EVALUASI_ASPEK ON EVALUASI_ASPEK.ID_KELOMPOK_NILAI = EVALUASI_NILAI.ID_KELOMPOK_NILAI 
					WHERE ID_EVAL_ASPEK = '$_REQUEST[id_eval_aspek]' ORDER BY EVALUASI_NILAI.URUTAN");
    $smarty->assign('skala_nilai', $skala_nilai);

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {

		$db->Query("INSERT INTO EVALUASI_ASPEK (ID_EVAL_KELOMPOK_ASPEK, ID_EVAL_INSTRUMEN, EVALUASI_ASPEK, TIPE_ASPEK, ID_KELOMPOK_NILAI, STATUS_AKTIF, URUTAN) 
					VALUES 
					('$_POST[kelompok_aspek]', '$_POST[id_eval_instrumen]', '$_POST[EVALUASI_ASPEK]', '$_POST[TIPE_ASPEK]', '$_POST[ID_KELOMPOK_NILAI]','$_POST[STATUS_AKTIF]' ,'$_POST[URUTAN]')");
					
    }
    else if (post('mode') == 'edit')
    {
		$db->Query("SELECT * FROM EVALUASI_KELOMPOK_ASPEK WHERE ID_EVAL_KELOMPOK_ASPEK = '$_POST[id_eval_kelompok_aspek]'");
		$id_eval_kelompok_aspek = $db->FetchAssoc();

		$db->Query("UPDATE EVALUASI_ASPEK SET ID_EVAL_INSTRUMEN = '$_POST[id_eval_instrumen]', 
					ID_EVAL_KELOMPOK_ASPEK = '$_POST[kelompok_aspek]', EVALUASI_ASPEK = '$_POST[EVALUASI_ASPEK]',
					TIPE_ASPEK = '$_POST[TIPE_ASPEK]', ID_KELOMPOK_NILAI = '$_POST[ID_KELOMPOK_NILAI]',
					STATUS_AKTIF = '$_POST[STATUS_AKTIF]', URUTAN = '$_POST[URUTAN]'
					WHERE ID_EVAL_ASPEK = '$_POST[id_eval_aspek]'");
    }
    else if (post('mode') == 'delete')
    {
		$db->Query("DELETE EVALUASI_KELOMPOK_ASPEK
					WHERE ID_EVAL_KELOMPOK_ASPEK = '$_POST[id_eval_kelompok_aspek]'");
	}
}


if ($mode == 'view')
{

	if(isset($_POST['instrument'])){
		$where = "";
		if($_POST['instrument'] != ''){
			$where = " and b.ID_EVAL_INSTRUMEN = '$_POST[instrument]'";
		}

$evaluasi_aspek_set = $db->QueryToArray("SELECT * FROM EVALUASI_KELOMPOK_ASPEK a, EVALUASI_INSTRUMEN b , EVALUASI_ASPEK c WHERE a.ID_EVAL_INSTRUMEN=b.ID_EVAL_INSTRUMEN AND a.ID_EVAL_KELOMPOK_ASPEK = c.ID_EVAL_KELOMPOK_ASPEK and b.id_role = 11 and b.ID_PERGURUAN_TINGGI = '{$id_pt_user}' $where
ORDER BY b.ID_EVAL_INSTRUMEN ASC");
    $smarty->assign('evaluasi_aspek_set', $evaluasi_aspek_set);
	}
}
else if ($mode == 'edit' or $mode == 'delete')
{

$db->Query("SELECT * FROM EVALUASI_KELOMPOK_ASPEK a, EVALUASI_INSTRUMEN b , EVALUASI_ASPEK c WHERE a.ID_EVAL_INSTRUMEN=b.ID_EVAL_INSTRUMEN AND a.ID_EVAL_KELOMPOK_ASPEK = c.ID_EVAL_KELOMPOK_ASPEK and b.id_role = 11 and ID_EVAL_ASPEK = '$_REQUEST[id_eval_aspek]'
ORDER BY b.ID_EVAL_INSTRUMEN ASC");
$evaluasi_aspek = $db->FetchAssoc();
    $smarty->assign('evaluasi_aspek', $evaluasi_aspek);

}
else if ($mode == 'add')
{

$evaluasi_kelompok_nilai_set = $db->QueryToArray("SELECT * FROM EVALUASI_KELOMPOK_NILAI a ORDER BY NM_KELOMPOK");
    $smarty->assign('evaluasi_kelompok_nilai_set', $evaluasi_kelompok_nilai_set);


}
$smarty->display("kuisioner/evalaspek/{$mode}.tpl");
?>
