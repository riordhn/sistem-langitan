<?php
include('config.php');
include 'class/mkwu.class.php';

$mkwu = new mkwu($db);
$semester_aktif =  $mkwu->semester_aktif();
$smarty->assign('semester', $mkwu->semester());
$smarty->assign('semester_aktif', $semester_aktif);

$semester_request = isset($_REQUEST['smt'])?  $_REQUEST['smt'] : $semester_aktif['ID_SEMESTER'];
$mode = isset($_REQUEST['mode'])?  $_REQUEST['mode'] : 'view';

if($_REQUEST['mode'] == 'addview'){
	
	if($_REQUEST['pjmk']==1){
		if (!$mkwu->mkwu_pjma_pengampu_cek($_REQUEST['id_kur'], $_REQUEST['smt'], 1, $_REQUEST['id_dosen'])) {
			$mkwu->mkwu_pjma_pengampu_add($_REQUEST['id_kur'], $_REQUEST['smt'], 1, $_REQUEST['id_dosen']);
		} 
	}
	
	if($_REQUEST['pjmk']==2){
		if (!$mkwu->mkwu_pjma_pengampu_cek($_REQUEST['id_kur'], $_REQUEST['smt'], 0, $_REQUEST['id_dosen'])) {
			$mkwu->mkwu_pjma_pengampu_add($_REQUEST['id_kur'], $_REQUEST['smt'], 0, $_REQUEST['id_dosen']);
		} 
	}
	
	if ($_REQUEST['action']=='edit'){
			$mkwu->mkwu_pjma_pengampu_update($_REQUEST['id_dosen'], $_REQUEST['id_kur'], $_REQUEST['smt'], $_REQUEST['id_dosen_ganti']);
	}
	
	if ($_REQUEST['action']=='hapus'){
			$mkwu->mkwu_pjma_pengampu_delete($_REQUEST['id_dosen'], $_REQUEST['id_kur'], $_REQUEST['smt']);
	}
	
	$smarty->assign('t_pjma_addview', $mkwu->mkwu_pjma_addview($_REQUEST['id_kur'], $_REQUEST['smt']));
	$smarty->assign('t_pjma_pengampu', $mkwu->mkwu_pjma_pengampu($_REQUEST['id_kur'], $_REQUEST['smt']));

}


if($_REQUEST['action'] == 'copy'){
	
	$mkwu->mkwu_pjma_pengampu_add_copy($_REQUEST['cp'], $_REQUEST['id_klsmk']);

}


if($_REQUEST['mode'] == 'copy'){
	
	$smarty->assign('t_pjma_copy', $mkwu->mkwu_pjma_copy($_REQUEST['id_kurmk'], $_REQUEST['id_klsmk'], $_REQUEST['smt']));

}


if($_REQUEST['mode'] == 'searchdosen'){
	if(isset($_REQUEST['namadosen'])){
	$smarty->assign('t_pjma_searchdosen', $mkwu->mkwu_pjma_searchdosen($_REQUEST['namadosen']));
	}
}

if($mode == 'view'){
	
	if ($_REQUEST['action']=='hapus'){
			$mkwu->mkwu_pjma_pengampu_delete('', $_REQUEST['id_kur'], $_REQUEST['smt']);
	}
	
	$smarty->assign('t_pjma', $mkwu->mkwu_pjma($semester_request));
}


$smarty->assign('semester_request', $semester_request);
$smarty->display("mkwu/mkwu-pjma.tpl")
?>