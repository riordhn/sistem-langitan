<?php
include 'config-mysql.php';
include "class/aucc.class.php";
$aucc = new AUCC_Pendidikan($db);

if(isset($_POST['mode'])){
	$mode = post('mode', 'index');
}else{
	$mode = get('mode', 'index');
}

$nim = trim(get('nim'));  
$pesan_error = '';


if ($mode == 'cari')
{		
	
	//admisi
	$admisi = $db->QueryToArray("SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, NM_STATUS_PENGGUNA, NM_JENJANG
				,ADMISI.NO_SK, ADMISI.TGL_SK, ADMISI.ALASAN, NM_SEMESTER, THN_AKADEMIK_SEMESTER, KURUN_WAKTU, NO_IJASAH, TGL_LULUS, STATUS_CEKAL
				FROM MAHASISWA
				LEFT JOIN PENGGUNA ON MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA
				LEFT JOIN ADMISI ON ADMISI.ID_MHS = MAHASISWA.ID_MHS
				LEFT JOIN PROGRAM_STUDI ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = ADMISI.STATUS_AKD_MHS
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = ADMISI.ID_SEMESTER
				WHERE NIM_MHS = '$nim'
				ORDER BY ADMISI.ID_ADMISI DESC");
			
		$smarty->assign('admisi', $admisi);
		
		$db->Query("SELECT NM_STATUS_PENGGUNA FROM MAHASISWA 
					JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA= MAHASISWA.STATUS_AKADEMIK_MHS WHERE NIM_MHS = '$nim'");
		$status = $db->FetchAssoc();
		$smarty->assign('status', $status['NM_STATUS_PENGGUNA']);
		
		//Pembayaran
		$db->Query("SELECT COUNT(ID_MHS) AS MHS FROM PEMBAYARAN WHERE ID_MHS IN (SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS = '$nim')");
		$row = $db->FetchAssoc();
		
		if ($row['MHS'] >= 1){
			$pembayaran_mhs = $db->QueryToArray("SELECT TAHUN_AJARAN, NM_SEMESTER, TGL_BAYAR, STATUS_PEMBAYARAN.NAMA_STATUS, PEMBAYARAN.KETERANGAN, 
									NM_BANK, NAMA_BANK_VIA, IS_TAGIH, STATUS_PEMBAYARAN.ID_STATUS_PEMBAYARAN,
									SUM(PEMBAYARAN.BESAR_BIAYA) BESAR_BIAYA, SUM(DENDA_BIAYA) AS DENDA_BIAYA
									FROM PEMBAYARAN
									LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
									LEFT JOIN STATUS_PEMBAYARAN ON STATUS_PEMBAYARAN.ID_STATUS_PEMBAYARAN = PEMBAYARAN.ID_STATUS_PEMBAYARAN
									LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PEMBAYARAN.ID_SEMESTER
									LEFT JOIN BANK ON BANK.ID_BANK = PEMBAYARAN.ID_BANK
									LEFT JOIN BANK_VIA ON BANK_VIA.ID_BANK_VIA = PEMBAYARAN.ID_BANK_VIA
									WHERE NIM_MHS = '$nim' 
									GROUP BY TAHUN_AJARAN, NM_SEMESTER, TGL_BAYAR, STATUS_PEMBAYARAN.NAMA_STATUS, PEMBAYARAN.KETERANGAN, 
									NM_BANK, NAMA_BANK_VIA, IS_TAGIH, STATUS_PEMBAYARAN.ID_STATUS_PEMBAYARAN
									ORDER BY TAHUN_AJARAN DESC, NM_SEMESTER DESC");
			
			
			//$smarty->assign('data_pembayaran', $aucc->detail_pembayaran($nim));
			//$smarty->assign('pembayaran_mhs', $row['MHS']);
			$smarty->assign('pembayaran_mhs', $pembayaran_mhs);
		}elseif($aucc->detail_pembayaran_cmhs_nim($nim)){
			$smarty->assign('data_pembayaran', $aucc->detail_pembayaran_cmhs_nim($nim));
		}
		
		
		$kuery = mysql_query("select a.id, b.nama_semes, a.status_lama, a.status_baru, date_format(a.waktu,'%d-%m-%Y %H:%i:%s') as waktu,
					a.sk_ubah_status, a.tgl_sk ,a.keterangan_ubah_status, a.operator_perubah
					from b_history_status a, thn_sem b, mhs_ua c 
					where a.id_thn=b.id_thn and a.id_mhs = c.id_mhs and c.nim = '$nim' 
					order by a.id_thn");


		$no = 1;
		while($m = mysql_fetch_array($kuery))
		{		
			$baris .= "<tr><td>".$no++."</td>
					 <td>$m[nama_semes]</td>";
			$status_lama = mysql_fetch_array(mysql_query("select nm_status from kd_stat where kd_status = '$m[status_lama]'"));
			$status_baru = mysql_fetch_array(mysql_query("select nm_status from kd_stat where kd_status = '$m[status_baru]'"));
			$baris .= "<td>$status_lama[nm_status]</td>
					 <td>$status_baru[nm_status]</td>
					 <td>$m[waktu]</td>
					 <td>$m[operator_perubah]</td>
					 <td>$m[sk_ubah_status]</td>
					 <td>$m[tgl_sk]</td>
					 <td>$m[keterangan_ubah_status]</td>
					 </tr>";
		}

		$smarty->assign('baris', $baris);
		
		
		//Riwayat Pembayaran
		$biomhs01 = "SELECT Mhs_ua.nama, Mhs_ua.nim, Mhs_ua.id_mhs,Mhs_ua.id_prod FROM kd_fakul Kd_fakul, kd_prodi Kd_prodi, mhs_ua Mhs_ua 
			WHERE Kd_fakul.kd_fakulta = Kd_prodi.kd_fakulta   AND Kd_prodi.id_prod = Mhs_ua.id_prod AND  left(nim,10) = '$nim'";
		$biomhs1 = mysql_fetch_array(mysql_query($biomhs01));
		$idmhs = $biomhs1[id_mhs];
		
		$datatrans01 = "SELECT * FROM transspp where id_mhs='$idmhs' order by (9999-id_thn)";
		$datatrans1 = mysql_query($datatrans01);

		while($datatrans=mysql_fetch_array($datatrans1))
		{
		$amb_sp3=number_format($datatrans[sp3]);
		$amb_pkl=number_format($datatrans[pkl]);
		$amb_asu=number_format($datatrans[asuransi]);
		$amb_reg=number_format($datatrans[regis]);
		$amb_spp=number_format($datatrans[spp]);
		$amb_pra=number_format($datatrans[praktikum]);
		$amb_iko=number_format($datatrans[denda]);
		$amb_piu=number_format($datatrans[piutang]);
		$amb_matrikulasi=number_format($datatrans[matrikulasi]);
		$amb_kkn=number_format($datatrans[kkn]);
		$amb_wisuda=number_format($datatrans[wisuda]);
		$amb_pembinaan=number_format($datatrans[pembinaan]);
		$amb_ikoma=number_format($datatrans[ikoma]);
	
		$amb_tgl=$datatrans[tgl_bayar];
		$amb_ban=$datatrans[id_bank];
		$amb_sem=$datatrans[id_thn];
		$amb_sta=$datatrans[stat_spp];
		if($datatrans["dibebaskan"]=="Y"){
			$amb_sudahbayar = "Y";
		}else if($datatrans["dibebaskan"]=="T"){
			if($datatrans["tgl_bayar"]=='0000-00-00'){
				$amb_sudahbayar = "T";
			}else{
				$amb_sudahbayar = "Y";
			}
		}
	
	
		$biothnsem01 = "SELECT * FROM thn_sem WHERE id_thn = '$amb_sem'";
		$biothnsem1 = mysql_query($biothnsem01);
		$biothnsem = mysql_fetch_array($biothnsem1);
		$amb_semes = $biothnsem[nama_semes];
		$thn_ring = $biothnsem[thn_akad];
		$sem_ring = $biothnsem[sem_akad];
	
		$databaru01 = "SELECT * FROM tbl_bank where id_bank='$amb_ban'";
		$databaru1 = mysql_query($databaru01);
		$databank=mysql_fetch_array($databaru1);
		$amb_bank=$databank[nm_bank];
	
		list($thn9,$bln9,$tgl9)=split("-",$amb_tgl);
		
		$tampil_pembayaran .=
		"<tr bgcolor='#e0f4f5'>
			<td>
				<P><FONT face=Arial color=#000099 size=1><STRONG>$amb_semes</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_sudahbayar</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_bank</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_ring</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$tgl9-$bln9-$thn9</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_spp</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_pra</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_asu</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_sp3</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_pkl</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_reg</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_iko</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_piu</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_matrikulasi</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_kkn</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_wisuda</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_pembinaan</STRONG></FONT></P></td>
			<td>
				<P align=center><FONT face=Arial color=#000099 size=1><STRONG>$amb_ikoma</STRONG></FONT></P></td>
		</tr>";
		}
		
		$smarty->assign('tampil_pembayaran', $tampil_pembayaran);

}

//$pesan_error = 'Mahasiswa tidak ada di database';
//$smarty->assign('error', $pesan_error);
$smarty->display("evaluasi/bayarstatus/{$mode}.tpl");
?>