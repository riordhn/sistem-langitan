<?php
require('../../config.php');
require('class/date.class.php');
$db = new MyOracle();
include 'class/evaluasi.class.php';

$eval = new evaluasi($db);
require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('EVALUASI PENETAPAN');
$pdf->SetSubject('EVALUASI PENETAPAN');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
$logo = "../../img/akademik_images/logounair.gif";
$logo_size = "25";
$title = "UNIVERSITAS AIRLANGGA";

$semester = $_GET['sem'];
$id_fakultas = $_GET['fak'];

if(isset($_REQUEST['fak'])){
	
$evaluasi = $db->QueryToArray($eval->evaluasi_penetapan($id_fakultas, $_REQUEST['jenjang'], $_REQUEST['prodi'], $semester, $_REQUEST['status_penetapan']));

}



$biomhs="select j.nm_jenjang, f.id_fakultas, f.alamat_fakultas, ps.nm_program_studi, f.nm_fakultas, 
			f.kodepos_fakultas, f.telpon_fakultas, f.faksimili_fakultas, f.website_fakultas, f.email_fakultas
			from program_studi ps
			left join fakultas f on ps.id_fakultas=f.id_fakultas
			left join jenjang j on ps.id_jenjang=j.id_jenjang
			where f.id_fakultas='".$id_fakultas."'";
			
$result1 = $db->Query($biomhs)or die("salah kueri 2 ");
while($r1 = $db->FetchRow()) {
	$nmjenjang = $r1[0];
	$fak = $r1[4];
	$alm_fak = $r1[2];
	$pos_fak = $r1[5];
	$tel_fak = $r1[6];
	$fax_fak = $r1[7];
	$web_fak = $r1[8];
	$eml_fak = $r1[9];
}

$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '$semester'");
$semester_aktif = $db->FetchAssoc();
$semester = strtoupper($semester_aktif['NM_SEMESTER']). ' ' . $semester_aktif['TAHUN_AJARAN'];

$tgl = tgl_indo(date("Y-m-d"));
				
$h = "Penetapan Bersama Antara Rektor, \nPimpinan Universitas dan Pimpinan Fakultas \nTentang Penetapan Status Hasil Evaluasi Semester ". $semester_aktif['NM_SEMESTER']. ' ' .$semester_aktif['TAHUN_AJARAN']."";				
$content = "FAKULTAS ".strtoupper($fak)."\n".strtoupper($h);

$dekan = "SELECT NM_PENGGUNA, GELAR_DEPAN, GELAR_BELAKANG, USERNAME FROM FAKULTAS 
			LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = FAKULTAS.ID_DEKAN
			WHERE FAKULTAS.ID_FAKULTAS = '$id_fakultas'";
$db->Query($dekan)or die("salah kueri 2 ");
$r3 = $db->FetchRow();
$nm_dekan = ucwords(strtolower($r3[0]));
$gelar_depan_dekan = $r3[1];
$gelar_belakang_dekan = $r3[2];
$nip_dekan = $r3[3];
	
	if($gelar_belakang_dekan <> ''){
		$gelar_belakang_dekan = ", " . $gelar_belakang_dekan; 
	}
	if($gelar_depan_dekan <> ''){
		$gelar_depan_dekan = $gelar_depan_dekan . " ";
	}

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
//$pdf->SetHeaderData($logo, $logo_size, $title, $content);

// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));

//set margins
$pdf->SetMargins(5, 10, 10);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// remove default footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 15);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullwidth', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('times', '', 10);

//$pdf->startPageGroup();
// add a page
$pdf->AddPage('P', 'A4');
//$pdf->Image('includes/logo_unair.png', 100, 50, 100, '', '', '', '', false, 72);

// set the starting point for the page content
$pdf->setPageMark();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="3">
  <tr>
	<td width="10%" align="left" valign="middle"><img src="../../img/akademik_images/logounair.gif" width="80" border="0"></td>
    <td width="90%" align="left" valign="middle"><font size="10"><b>UNIVERSITAS AIRLANGGA<br>FAKULTAS '.strtoupper($fak).'</b></font><br>HASIL PENETAPAN BERSAMA ANTARA REKTOR, PIMPINAN UNIVERSITAS DAN PIMPINAN FAKULTAS TENTANG EVALUASI STATUS MAHASISWA SEMESTER '.$semester.'<br>Telp. '.$tel_fak.', Fax. '.$fax_fak.'<br>'.strtolower($web_fak).', '.strtolower($eml_fak).'</td>
  </tr>
</table>
<br />
';

$html .= '
<table cellpadding="4" border="0.5" style="height:10px">
	<tr bgcolor="#99CCFF" style="color:#000000;height:10px">
		<th style="text-align:center" width="5%">NO</th>
		<th style="text-align:center" width="14%">NIM</th>
		<th style="text-align:center" width="30%">NAMA</th>
		<th style="text-align:center" width="9%">JENJANG</th>
		<th style="text-align:center" width="25%">PRODI</th>
		<th style="text-align:center" width="20%">HASIL PENETAPAN</th>
	</tr>';
	
	$no = 1;
	foreach($evaluasi as $data){ 	
		
		if(($no % 2) == 0){
		 	$warna = "#E0EBFF";
		}else{
		 	$warna = "";
		}
$html .= '<tr bgcolor="'.$warna.'">
		<td style="text-align:center">'.$no++.'</td>
		<td>'.$data['NIM_MHS'].'</td>
		<td>'.$data['NM_PENGGUNA'].'</td>
		<td>'.$data['NM_JENJANG'].'</td>
		<td>'.$data['NM_PROGRAM_STUDI'].'</td>
		<td>'.$data['REKOMENDASI_PENETAPAN'].'</td>
	</tr>';

	}
$html .= '</table>';	
$pdf->writeHTML($html, true, false, true, false, '');	

/*if($no >= 18){
	$pdf->AddPage();		
}*/

$html1 .= '<br />
			<table width="100%" style="font-size:45px">
			<tr>
				<td></td>
				<td>Surabaya, '. $tgl .'<br />Dekan, </td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>			
				<td></td>
			</tr>
			<tr>			
				<td></td>
				<td>'. $gelar_depan_dekan . $nm_dekan . $gelar_belakang_dekan.'<br />NIP. '.$nip_dekan.'</td>
			</tr>
			</table>';


// output the HTML content
$pdf->writeHTML($html1, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('evaluasi-penetapan.pdf', 'I');

?>