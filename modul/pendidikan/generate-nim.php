<?php
include('config.php');

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    $keys = array_keys($_POST);
   
    $id_c_mhs_set = array();
    foreach ($keys as $key)
    {
        if (substr($key, 0, 3) == "cek")
        {
            array_push($id_c_mhs_set, str_replace("cek", "", $key));
        }
    }
    
    if (count($id_c_mhs_set) > 0)
    {
        
        $start_execute = microtime(true);

        $no_ujians = "select no_ujian from temp_calon_mahasiswa where id_c_mhs in (" . implode(",", $id_c_mhs_set) . ")";

        // truncate nim_mhs
        $db->Query("TRUNCATE TABLE TEMP_NIM_MHS");

        // insert calon sementara ke tabel nim_mhs
        $db->Query("
            INSERT INTO TEMP_NIM_MHS (ID_C_MHS, NIM_MHS)
                SELECT CM.ID_C_MHS, LPAD(F.ID_FAKULTAS,2,'0') || '11' || PS.KODE_PROGRAM_STUDI AS NIM_MHS
                FROM TEMP_CALON_MAHASISWA CM
                LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = CM.ID_PROGRAM_STUDI
                LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS
                WHERE
                  CM.ID_C_MHS NOT IN (SELECT ID_C_MHS FROM TEMP_MAHASISWA WHERE ID_C_MHS IS NOT NULL) AND
                  CM.STATUS = 12 AND CM.KESEHATAN_KESIMPULAN_AKHIR = 1 AND CM.NO_UJIAN IN ({$no_ujians})
                ");

        // mengambil semua data dari tabel nim_mhs yg belum tergenerate
        $nim_mhs_set = array();
        $db->Query("
            SELECT NM.ID_C_MHS, NM.NIM_MHS, CM.TGL_VERIFIKASI, CM.NO_UJIAN
            FROM TEMP_NIM_MHS NM
            JOIN TEMP_CALON_MAHASISWA CM ON CM.ID_C_MHS = NM.ID_C_MHS
            WHERE LENGTH(NM.NIM_MHS) < 9
            ORDER BY CM.TGL_VERIFIKASI"); // order by tgl verifikasi
        while ($row = $db->FetchAssoc())
            array_push($nim_mhs_set, $row);

        // melakukan pencarian slot
        foreach ($nim_mhs_set as $nim_mhs)
        {
            //echo "{$nim_mhs['NO_UJIAN']} -&gt; ";

            if (strlen($nim_mhs['NIM_MHS']) == 6) // untuk format : FFTTPPXXX
            {
                // melakukan pencarian slot
                for ($i = 1; $i < 1000; $i++)
                {
                    // mendapatkan nomer urut slot
                    $no_urut = str_pad($i, 3, '0', STR_PAD_LEFT);
                    $db->Query("
                        SELECT COUNT(*) AS HASIL FROM (
                            SELECT NIM_MHS FROM TEMP_MAHASISWA
                            WHERE ID_C_MHS IS NOT NULL
                            UNION
                            SELECT NIM_MHS FROM TEMP_NIM_MHS
                            WHERE LENGTH(NIM_MHS) = 9
                        ) T
                        WHERE T.NIM_MHS = '{$nim_mhs['NIM_MHS']}{$no_urut}'");
                    $row = $db->FetchAssoc();

                    // saat slot kosong
                    if ($row['HASIL'] == 0)
                    {
                        // update data di nim_mhs
                        $db->Query("UPDATE TEMP_NIM_MHS SET NIM_MHS = '{$nim_mhs['NIM_MHS']}{$no_urut}' WHERE ID_C_MHS = {$nim_mhs['ID_C_MHS']}");

                        // tampilkan data
                        //echo "{$nim_mhs['NIM_MHS']}{$no_urut}";
                        break;
                    }
                }
            }
            else if (strlen($nim_mhs['NIM_MHS']) == 7) // untuk format : FFTTPPPXX
            {
                // melakukan pencarian slot
                for ($i = 1; $i < 100; $i++)
                {
                    // mendapatkan nomer urut slot
                    $no_urut = str_pad($i, 2, '0', STR_PAD_LEFT);
                    $db->Query("
                        SELECT COUNT(*) AS HASIL FROM (
                            SELECT NIM_MHS FROM TEMP_MAHASISWA
                            WHERE ID_C_MHS IS NOT NULL
                            UNION
                            SELECT NIM_MHS FROM TEMP_NIM_MHS
                            WHERE LENGTH(NIM_MHS) = 9
                        ) T
                        WHERE T.NIM_MHS = '{$nim_mhs['NIM_MHS']}{$no_urut}'");
                    $row = $db->FetchAssoc();

                    // saat slot kosong
                    if ($row['HASIL'] == 0)
                    {
                        // update data di nim_mhs
                        $db->Query("UPDATE TEMP_NIM_MHS SET NIM_MHS = '{$nim_mhs['NIM_MHS']}{$no_urut}' WHERE ID_C_MHS = {$nim_mhs['ID_C_MHS']}");

                        // tampilkan hasil generate
                        //echo "{$nim_mhs['NIM_MHS']}{$no_urut}";
                        break;
                    }
                }
            }

            //echo "<br/>";
        }

        $end_execute = microtime(true);

        echo "<h2>Nim tergenerate dalam " . ($end_execute - $start_execute) . " detik.</h2><br/>";

        // insert ke tabel pengguna dari nim yg sudah jadi
        // QUERY 1
        $result1 = $db->Query("
            INSERT INTO TEMP_PENGGUNA (USERNAME, PASSWORD_PENGGUNA, NM_PENGGUNA, ID_AGAMA, KELAMIN_PENGGUNA, TGL_LAHIR_PENGGUNA, TEMPAT_LAHIR, ID_ROLE, JOIN_TABLE)
                SELECT NM.NIM_MHS, NM.NIM_MHS, CM.NM_C_MHS, CM.ID_AGAMA, CM.JENIS_KELAMIN, CM.TGL_LAHIR, CM.ID_KOTA_LAHIR, 3 AS ID_ROLE, 3 AS JOIN_TABLE
                FROM TEMP_NIM_MHS NM
                LEFT JOIN TEMP_CALON_MAHASISWA CM ON CM.ID_C_MHS = NM.ID_C_MHS
                WHERE LENGTH(NM.NIM_MHS) = 9 AND NM.ID_PENGGUNA IS NULL AND NM.ID_MHS IS NULL");
        if ($result1)
            echo "Insert Pengguna OK<br/>";
        else
            echo "Insert Pengguna Gagal<br/>";

        if ($result1)
        {
            // update tabel nim_mhs dari pengguna
            $result2 = $db->Query("UPDATE TEMP_NIM_MHS NM SET NM.ID_PENGGUNA = (SELECT P.ID_PENGGUNA FROM TEMP_PENGGUNA P WHERE P.USERNAME = NM.NIM_MHS) WHERE NM.ID_PENGGUNA IS NULL");
            if ($result2)
                echo "Update Nim_Mhs OK<br/>";
            else
                echo "Update Nim_Mhs Gagal<br/>";
        }

        if ($result2)
        {
            // insert ke tabel mahasiswa dari nim_mhs
            $tgl_sekarang = date('d-m-Y');
            $tahun_angkatan = date('Y');
            $result3 = $db->Query("
                INSERT INTO TEMP_MAHASISWA (ID_PENGGUNA, ID_C_MHS, NIM_MHS, ID_PROGRAM_STUDI, LAHIR_KOTA_MHS, NO_IJAZAH, TGL_TERDAFTAR_MHS, THN_ANGKATAN_MHS, ALAMAT_MHS, NM_AYAH_MHS, NM_IBU_MHS, ALAMAT_AYAH_MHS, ALAMAT_IBU_MHS, SIDIK_JARI_MHS)
                    SELECT NM.ID_PENGGUNA, NM.ID_C_MHS, NM.NIM_MHS, CM.ID_PROGRAM_STUDI, CM.ID_KOTA_LAHIR, CM.NO_IJAZAH,
                    TO_DATE('{$tgl_sekarang}', 'DD-MM-YYYY') AS TGL_DAFTAR, {$tahun_angkatan} AS ANGKATAN, CM.ALAMAT,
                    CM.NAMA_AYAH, CM.NAMA_IBU, CM.ALAMAT_AYAH, CM.ALAMAT_IBU, CM.FINGER_DATA
                    FROM TEMP_NIM_MHS NM
                    LEFT JOIN TEMP_CALON_MAHASISWA CM ON CM.ID_C_MHS = NM.ID_C_MHS
                    WHERE NM.ID_PENGGUNA IS NOT NULL AND NM.ID_MHS IS NULL");
            if ($result3)
                echo "Insert Mahasiswa OK<br/>";
            else
                echo "Insert Mahasiswa Gagal<br/>";
        }

        if ($result3)
        {
            // update nim_mhs dari mahasiswa
            $result4 = $db->Query("UPDATE TEMP_NIM_MHS NM SET NM.ID_MHS = (SELECT M.ID_MHS FROM TEMP_MAHASISWA M WHERE M.NIM_MHS = NM.NIM_MHS AND M.ID_C_MHS = NM.ID_C_MHS)");
            if ($result4)
                echo "Update NIM OK<br/>";
            else
                echo "Update NIM Gagal<br/>";
        }

        if ($result4)
        {
            // update calon_mahasiswa dari nim_mhs
            $result5 = $db->Query("UPDATE TEMP_CALON_MAHASISWA SET STATUS = 13 WHERE ID_C_MHS IN (SELECT NM.ID_C_MHS FROM TEMP_NIM_MHS NM)");
            if ($result5)
                echo "Update Calon_mahasiswa OK<br/>";
            else
                echo "Update Calon_mahasiswa gagal<br/>";
        }
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $smarty->assign('fakultas_set', $db->QueryToArray("select * from fakultas"));
    }
    
    if ($mode == 'prodi')
    {
        $id_fakultas = get('id_fakultas');
        
        $db->Query("select * from fakultas where id_fakultas = {$id_fakultas}");
        $row = $db->FetchAssoc();
        $smarty->assign('fakultas', $row);
        
        $smarty->assign('program_studi_set', $db->QueryToArray("
            select id_program_studi, nm_jenjang || ' ' || nm_program_studi as nm_program_studi
            from program_studi ps
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where ps.id_fakultas = {$id_fakultas}"));
    }
    
    if ($mode == 'detail')
    {
        $id_program_studi = get('id_program_studi');
        
        $db->Query("
            select id_program_studi, nm_jenjang || ' ' || nm_program_studi as nm_program_studi, id_fakultas
            from program_studi ps
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where ps.id_program_studi = {$id_program_studi}");
        $row = $db->FetchAssoc();
        $smarty->assign('program_studi', $row);
        
        $cmb_set = $db->QueryToArray("
            select cmb.id_c_mhs, m.nim_mhs, cmb.no_ujian, cmb.nm_c_mhs, cmb.status
            from temp_calon_mahasiswa cmb
            left join temp_mahasiswa m on m.id_c_mhs = cmb.id_c_mhs
            where cmb.id_program_studi = {$id_program_studi}
            order by m.nim_mhs, cmb.no_ujian");
        $smarty->assign("cmb_set", $cmb_set);
    }
}

$smarty->display("generate/nim/{$mode}.tpl");
?>
