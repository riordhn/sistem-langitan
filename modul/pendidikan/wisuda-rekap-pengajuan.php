<?php
include('config.php');
$periode = $db->QueryToArray("SELECT id_tarif_wisuda,nm_tarif_wisuda FROM tarif_WISUDA WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY ID_TARIF_WISUDA DESC");
$smarty->assign('periode', $periode);
$fakultas = $db->QueryToArray("SELECT ID_FAKULTAS,NM_FAKULTAS FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY ID_FAKULTAS ASC");
$smarty->assign('fakultas', $fakultas);

if(isset($_REQUEST['detail'])){
	
	if($_REQUEST['detail'] == 1){
		$where = "";
	}elseif($_REQUEST['detail'] == 2){
		$where = " AND PENGAJUAN_CETAK_IJASAH IS NOT NULL";
	}elseif($_REQUEST['detail'] == 3){
		$where = " AND POSISI_IJASAH = 5";
	}elseif($_REQUEST['detail'] == 4){
		$where = " AND POSISI_IJASAH = 7";
	}
	
$detail = $db->QueryToArray("SELECT ff.id_fakultas,ps.id_program_studi,jj.id_jenjang,ff.nm_fakultas,ps.nm_program_studi,
						   jj.nm_jenjang,BW.ID_MHS, mh.NIM_MHS, NM_PENGGUNA
					    	FROM PENGAJUAN_WISUDA aw
					    	LEFT JOIN PEMBAYARAN_WISUDA bw on AW.ID_MHS=BW.ID_MHS
						LEFT JOIN PERIODE_WISUDA ww on bw.id_periode_wisuda=ww.id_periode_wisuda
						LEFT JOIN MAHASISWA mh on aw.id_mhs=mh.id_mhs
						LEFT JOIN PROGRAM_STUDI ps on mh.id_program_studi=ps.id_program_studi
						LEFT JOIN FAKULTAS ff on ff.id_fakultas=ps.id_fakultas
						LEFT JOIN JENJANG jj on ps.id_jenjang=jj.id_jenjang
						LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = mh.ID_PENGGUNA
						WHERE ww.id_tarif_wisuda='$_REQUEST[id_periode]' and ps.id_program_studi = '$_REQUEST[id_prodi]'
						AND BW.ABSENSI_WISUDA=1		
						$where				
						ORDER BY mh.NIM_MHS");

$smarty->assign('detail', $detail);
}


if(isset($_POST['periode'])){
		if($_POST['fakultas'] <> ''){
			$where  = " AND PS.ID_FAKULTAS = '$_POST[fakultas]'";
			$order = "";
		}else{
			$where  = "";
			$order = "ff.id_fakultas, ";
		}
		
		$rekap = $db->QueryToArray("SELECT ff.id_fakultas,ps.id_program_studi,jj.id_jenjang,ff.nm_fakultas,ps.nm_program_studi,
						   jj.nm_jenjang,count(BW.ID_MHS) as ikut_wisuda, count(AW.PENGAJUAN_CETAK_IJASAH) as pengajuan_cetak,
						   count((select id_mhs from pengajuan_wisuda where pengajuan_wisuda.id_pengajuan_wisuda = aw.ID_PENGAJUAN_WISUDA and posisi_ijasah = 5)) as fak_mhs,
								count((select id_mhs from pengajuan_wisuda where pengajuan_wisuda.id_pengajuan_wisuda = aw.ID_PENGAJUAN_WISUDA and posisi_ijasah = 7)) as pend_mhs					    	 
					    	FROM PENGAJUAN_WISUDA aw
					    	LEFT JOIN PEMBAYARAN_WISUDA bw on AW.ID_MHS=BW.ID_MHS
						LEFT JOIN PERIODE_WISUDA ww on bw.id_periode_wisuda=ww.id_periode_wisuda
						LEFT JOIN MAHASISWA mh on aw.id_mhs=mh.id_mhs
						LEFT JOIN PROGRAM_STUDI ps on mh.id_program_studi=ps.id_program_studi
						LEFT JOIN FAKULTAS ff on ff.id_fakultas=ps.id_fakultas
						LEFT JOIN JENJANG jj on ps.id_jenjang=jj.id_jenjang
						WHERE ww.id_tarif_wisuda='$_POST[periode]'
						$where
						AND BW.ABSENSI_WISUDA=1
						GROUP BY ff.id_fakultas,ps.id_program_studi,jj.id_jenjang,ff.nm_fakultas,ps.nm_program_studi,jj.nm_jenjang
						ORDER BY $order ps.id_program_studi,jj.id_jenjang");

$smarty->assign('rekap', $rekap);	
$smarty->assign('id_periode', $_POST['periode']);
$smarty->assign('id_fakultas', $_POST['fakultas']);
}

$smarty->display("wisuda/wisuda-rekap-pengajuan.tpl");
?>
