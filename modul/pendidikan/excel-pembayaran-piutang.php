<?php
include "config.php";
include "class/aucc.class.php";
$aucc = new AUCC_Pendidikan($db);

$mhs = $aucc->get_mhs_piutang($_GET['fakultas']);
		$no = 1;
		foreach($mhs as $data){
			$id_semester_piutang = $aucc->get_semester_piutang($data['NIM_MHS']);
			$semester = 0;
			$nim = "";
			foreach($id_semester_piutang as $sp){
				
				$piutang = $aucc->piutang_mhs2($sp['ID_SEMESTER'], $data['ID_MHS']);
				
				if($piutang['TGL_BAYAR'] == ''){
					$semester = $semester + 1;
				}
			}
					if($semester > 0){
					$tampil = $tampil. "<tr>
								<td>$no</td>
								<td><a href='pembayaran-piutang.php?nim=$data[NIM_MHS]'>$data[NIM_MHS]</a></td>
								<td>$data[NM_PENGGUNA]</td>
								<td>$data[NM_FAKULTAS]</td>
								<td>$data[NM_JENJANG] - $data[NM_PROGRAM_STUDI]</td>
								<td>$data[NM_STATUS_PENGGUNA]</td>
								<td style='text-align:center'>$semester</td>
							</tr>";
					$no++;
					}
		}

ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
<table cellpadding="3" cellspacing="3" border="1">
    <thead>
        <tr>
			<th>No</th>
			<th>NIM</th>
			<th>Nama</th>
			<th>Fakultas</th>
			<th>Program Studi</th>
			<th>Status Akademik</th>
			<th>Piutang Semester</th>
        </tr>
    </thead>
    <tbody>
        <?php
			echo $tampil;
        ?>
    </tbody>
</table>

<?php
$filename = 'Excel-piutang-mahasiswa' . date('Y-m-d');
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
