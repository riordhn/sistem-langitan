<?php
require '../../../config.php';

$db = new MyOracle();

require_once '../../../tcpdf/config/lang/eng.php';
require_once '../../../tcpdf/tcpdf.php';

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('Biodata Mahasiswa');
$pdf->SetSubject('Biodata Mahasiswa');


include '../class/report_mahasiswa.class.php';
include '../includes/function.php';

$id_pt = $id_pt_user;

$rm = new report_mahasiswa($db);

$var = decode($_SERVER['REQUEST_URI']);
$nim_mhs = $var['nim_mhs'];

$biodata_mahasiswa = $rm->get_biodata_mahasiswa($nim_mhs, $id_pt);

$foto_mhs = "../../../foto_mhs/".$nama_singkat."/".$nim_mhs.".jpg";

$bulan['01'] = "Januari"; $bulan['02'] = "Pebruari"; $bulan['03'] = "Maret"; $bulan['04'] = "April"; $bulan['05'] = "Mei"; $bulan['06'] = "Juni"; $bulan['07'] = "Juli"; $bulan['08'] = "Agustus"; $bulan['09'] = "September"; $bulan['10'] = "Oktober";  $bulan['11'] = "Nopember";  $bulan['12'] = "Desember";


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 8, 10);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 8);

// add a page
$pdf->AddPage('P', 'A4');
/*$pdf->AddPage('L', 'F4');*/

// draw jpeg image
//$pdf->Image('../includes/logo_unair.png', 75, 25, 150, '', '', '', '', false, 72);
//$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
// set the starting point for the page content
$pdf->setPageMark();

$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td width="8%" align="left" valign="middle"><img src="../../../img/akademik_images/logo-'.$nama_singkat.'.gif" width="45" border="0"></td>
    <td width="92%" align="left" valign="middle"><font size="18"><b>'.strtoupper($nama_pt).'</b></font><br><font size="12">'.strtoupper($alamat).' Telp. '.$telp_pt.'</font></td>
  </tr>
</table>';

if (file_exists($foto_mhs)) {

	$html .= '
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td colspan="2">
			<hr>
		</td>
	  </tr>
	  <tr>
	    <td colspan="2" align="center"><font size="14"><b>BIODATA MAHASISWA</b></font><br><font size="10"></font></td>
	  </tr>
	  <tr>
	    <td colspan="2">
			<hr>
		</td>
	  </tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td width="100%" align="center" valign="center"><img src="'.$foto_mhs.'" width="180" height="230"></td>
	  </tr>
	</table>';

}
else {
	$html .= '
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td colspan="2">
			<hr>
		</td>
	  </tr>
	  <tr>
	    <td colspan="2" align="center"><font size="14"><b>BIODATA MAHASISWA</b></font><br><font size="10"></font></td>
	  </tr>
	  <tr>
	    <td colspan="2">
			<hr>
		</td>
	  </tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td width="100%" align="center" valign="center"><img src="../../../foto_mhs/foto-tidak-ditemukan.png" width="180" height="230"></td>
	  </tr>
	</table>';
}

$html .= '
<br> <br>
<table width="100%" border="1" cellpadding="4">

	<tr>
		 <td width="30%"><font size="10">NAMA</font></td>
		 <td width="2%"><font size="10">:</font></td>
		 <td width="68%"><font size="10">' . $biodata_mahasiswa['NM_PENGGUNA'] . '</font></td>		 			 
    </tr>
    <tr>
        <td width="30%"><font size="10">NIM</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . $biodata_mahasiswa['NIM_MHS'] . '</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">NO. PENDAFTARAN / UJIAN</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . $biodata_mahasiswa['NO_UJIAN'] . '</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">ANGKATAN</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . $biodata_mahasiswa['THN_ANGKATAN_MHS'] . '</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">JENJANG</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . $biodata_mahasiswa['NM_JENJANG'] . '</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">FAKULTAS</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . strtoupper($biodata_mahasiswa['NM_FAKULTAS']) . '</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">PROGRAM STUDI</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . $biodata_mahasiswa['NM_PROGRAM_STUDI'] . '</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">TEMPAT/TANGGAL LAHIR</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . $biodata_mahasiswa['NM_KOTA'] . ' / '. $biodata_mahasiswa['TGL_LAHIR_PENGGUNA'] .'</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">TELP</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . $biodata_mahasiswa['MOBILE_MHS'] . '</font></td>
    </tr>
    ';

if ($biodata_mahasiswa['KELAMIN_PENGGUNA'] == 1) {
	$html .= '
	    <tr>
	        <td width="30%"><font size="10">KELAMIN</font></td>
			<td width="2%"><font size="10">:</font></td>
			<td width="68%"><font size="10">Laki-laki</font></td>
	    </tr>
	    <tr>
	        <td width="30%"><font size="10">EMAIL</font></td>
			<td width="2%"><font size="10">:</font></td>
			<td width="68%"><font size="10">' . $biodata_mahasiswa['EMAIL_PENGGUNA'] . '</font></td>
	    </tr>
	    ';
}
elseif ($biodata_mahasiswa['KELAMIN_PENGGUNA'] == 2) {
	$html .= '
	    <tr>
	        <td width="30%"><font size="10">KELAMIN</font></td>
			<td width="2%"><font size="10">:</font></td>
			<td width="68%"><font size="10">Perempuan</font></td>
	    </tr>
	    <tr>
	        <td width="30%"><font size="10">EMAIL</font></td>
			<td width="2%"><font size="10">:</font></td>
			<td width="68%"><font size="10">' . $biodata_mahasiswa['EMAIL_PENGGUNA'] . '</font></td>
	    </tr>
	    ';
}
else{
	$html .= '
	    <tr>
	        <td width="30%"><font size="10">KELAMIN</font></td>
			<td width="2%"><font size="10">:</font></td>
			<td width="68%"><font size="10">-</font></td>
	    </tr>
	    <tr>
	        <td width="30%"><font size="10">EMAIL</font></td>
			<td width="2%"><font size="10">:</font></td>
			<td width="68%"><font size="10">' . $biodata_mahasiswa['EMAIL_PENGGUNA'] . '</font></td>
	    </tr>
	    ';
}


$html .= '
    <tr>
        <td width="30%"><font size="10">ALAMAT MAHASISWA</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . ucwords($biodata_mahasiswa['ALAMAT_ASAL_MHS']) . '';

if(isset($biodata_mahasiswa['ALAMAT_RT_ASAL_MHS'])) {
	$html .= ' RT. '.$biodata_mahasiswa['ALAMAT_RT_ASAL_MHS'].'';
}

if(isset($biodata_mahasiswa['ALAMAT_RW_ASAL_MHS'])) {
	$html .= ' RW. '.$biodata_mahasiswa['ALAMAT_RW_ASAL_MHS'].'';
}

if(isset($biodata_mahasiswa['ALAMAT_DUSUN_ASAL_MHS'])) {
	$html .= ' Dusun '.ucwords($biodata_mahasiswa['ALAMAT_DUSUN_ASAL_MHS']).'';
}

if(isset($biodata_mahasiswa['ALAMAT_KELURAHAN_ASAL_MHS'])) {
	$html .= ' Kelurahan '.ucwords($biodata_mahasiswa['ALAMAT_KELURAHAN_ASAL_MHS']).'';
}

if(isset($biodata_mahasiswa['ALAMAT_KECAMATAN_ASAL_MHS'])) {
	$html .= ' Kecamatan '.ucwords($biodata_mahasiswa['ALAMAT_KECAMATAN_ASAL_MHS']).'';
}

if(isset($biodata_mahasiswa['ALAMAT_KODEPOS_ASAL'])) {
	$html .= ', '.ucwords($biodata_mahasiswa['ALAMAT_KODEPOS_ASAL']).' ';
}

if(isset($biodata_mahasiswa['ALAMAT_KOTA_MHS'])) {
	$html .= ' Kota '.ucwords($biodata_mahasiswa['ALAMAT_KOTA_MHS']).'';
}

if(isset($biodata_mahasiswa['ALAMAT_PROV_MHS'])) {
	$html .= ', '.ucwords($biodata_mahasiswa['ALAMAT_PROV_MHS']).'';
}
$html .= '
        </font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">NAMA AYAH</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . ucwords($biodata_mahasiswa['NM_AYAH_MHS']) . '</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">ALAMAT AYAH</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . ucwords($biodata_mahasiswa['ALAMAT_AYAH_MHS']) . '</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">TELP AYAH</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . $biodata_mahasiswa['TELP_AYAH'] . '</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">PENGHASILAN AYAH</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . $biodata_mahasiswa['PENGHASILAN_AYAH_MHS'] . '</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">NAMA IBU</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . ucwords($biodata_mahasiswa['NM_IBU_MHS']) . '</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">ALAMAT IBU</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . ucwords($biodata_mahasiswa['ALAMAT_IBU_MHS']) . '</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">TELP IBU</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . $biodata_mahasiswa['TELP_IBU'] . '</font></td>
    </tr>
    <tr>
        <td width="30%"><font size="10">PENGHASILAN IBU</font></td>
		<td width="2%"><font size="10">:</font></td>
		<td width="68%"><font size="10">' . $biodata_mahasiswa['PENGHASILAN_IBU_MHS'] . '</font></td>
    </tr>
</table>


<br><br>
<b><i>Waktu cetak : ' . date('d F Y  H:i:s') . '</i><b>
';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('Biodata Mahasiswa '.$nim_mhs.'.pdf', 'I');