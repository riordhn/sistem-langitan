<?php
include('config.php');
include 'class/mkwu.class.php';

$mkwu = new mkwu($db);

$semester_aktif =  $mkwu->semester_aktif();
$smarty->assign('semester', $mkwu->semester());
$smarty->assign('semester_aktif', $semester_aktif);

$semester_request = isset($_REQUEST['smt'])?  $_REQUEST['smt'] : $semester_aktif['ID_SEMESTER'];
$mode = isset($_REQUEST['mode'])?  $_REQUEST['mode'] : 'view';

if($_REQUEST['action'] == 'add'){
	$mkwu->mkwu_penawaran_add($_REQUEST['id_prodi'], $semester_request, $_REQUEST['id_mk']);
}

if($_REQUEST['action'] == 'del'){
	$mkwu->mkwu_penawaran_del($_REQUEST['id_krs_prodi']);
}

if($mode == 'penawaran'){
	$smarty->assign('tawar', $mkwu->mkwu_penawaran(228, $semester_request));
}


if($mode == 'view'){
	$smarty->assign('T_MK', $mkwu->mkwu_penawaran_rincian(228, $semester_request));
}


$smarty->assign('semester_request', $semester_request);
$smarty->display("mkwu/mkwu-penawaran.tpl")
?>