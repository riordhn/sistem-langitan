<?php

include "config.php";
include "class/aucc.class.php";
$aucc = new AUCC_Pendidikan($db);
if (post('no_ujian')) {
    if ($aucc->detail_pembayaran_cmhs(post('no_ujian')))
        $smarty->assign('data_pembayaran', $aucc->detail_pembayaran_cmhs(post('no_ujian')));
    else
        $smarty->assign('data_kosong', 'Data Mahasiswa Masih Belum ada dalam database kami');
}

$smarty->display('pembayaran/cek-pembayaran-cmhs.tpl');
?>
