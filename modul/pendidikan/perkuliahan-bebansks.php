<?php
include('config.php');

$mode = get('mode', 'view');
$id_pt = $id_pt_user;
$id_fakultas = get('id_fakultas', 0);
$id_beban_sks = get('id_beban_sks', 0);

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
        $db->Query("INSERT INTO BEBAN_SKS (ID_FAKULTAS, IPK_MINIMUM, SKS_MAKSIMAL, ID_PROGRAM_STUDI)
					VALUES ('$_POST[id_fakultas]', '$_POST[ipk]',  '$_POST[sks]','$_POST[id_program_studi]')");
    }
    else if (post('mode') == 'edit')
    {
		$db->Query("UPDATE BEBAN_SKS SET ID_FAKULTAS = '$_POST[id_fakultas]', IPK_MINIMUM = '$_POST[ipk]', 
					SKS_MAKSIMAL = '$_POST[sks]', ID_PROGRAM_STUDI = '$_POST[id_program_studi]'
					WHERE ID_BEBAN_SKS = '$_POST[id_beban_sks]'");
    }
    else if (post('mode') == 'delete')
    {
        $db->Query("DELETE BEBAN_SKS 
					WHERE ID_BEBAN_SKS = '$_POST[id_beban_sks]'");
    }
}

if ($mode == 'view')
{
    $fakultas_set = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = {$id_pt} ORDER BY ID_FAKULTAS");
    $smarty->assign('fakultas_set', $fakultas_set);
}
else if ($mode == 'fakultas')
{
    $db->Query("SELECT * FROM FAKULTAS WHERE ID_FAKULTAS = '$id_fakultas' ORDER BY ID_FAKULTAS");
    $fakultas = $db->FetchAssoc();
	$beban_sks = $db->QueryToArray("SELECT ID_BEBAN_SKS, IPK_MINIMUM, SKS_MAKSIMAL, NM_PROGRAM_STUDI, NM_JENJANG 
						FROM BEBAN_SKS
						JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = BEBAN_SKS.ID_PROGRAM_STUDI
						JOIN FAKULTAS ON PROGRAM_STUDI.ID_FAKULTAS = FAKULTAS.ID_FAKULTAS
						JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
						WHERE BEBAN_SKS.ID_FAKULTAS = '$id_fakultas' ORDER BY NM_JENJANG, NM_PROGRAM_STUDI, IPK_MINIMUM");
	
    $smarty->assign('fakultas', $fakultas);
	$smarty->assign('beban_sks', $beban_sks);
}
else if ($mode == 'add')
{
    $db->Query("SELECT * FROM FAKULTAS WHERE ID_FAKULTAS = '$id_fakultas' ORDER BY ID_FAKULTAS");
    $fakultas = $db->FetchAssoc();
    $smarty->assign('fakultas', $fakultas);
	$prodi = $db->QueryToArray("SELECT ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, NM_JENJANG, JENJANG.ID_JENJANG FROM PROGRAM_STUDI 
								JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG WHERE ID_FAKULTAS = '$id_fakultas'");
	$smarty->assign('program_studi', $prodi);
	
}
else if ($mode == 'edit')
{
    $db->Query("SELECT * FROM BEBAN_SKS JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = BEBAN_SKS.ID_FAKULTAS WHERE ID_BEBAN_SKS = '$id_beban_sks'");
	$beban_sks = $db->FetchAssoc();
    $smarty->assign('beban_sks', $beban_sks);
	$prodi = $db->QueryToArray("SELECT ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, NM_JENJANG, JENJANG.ID_JENJANG FROM PROGRAM_STUDI 
								JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG WHERE ID_FAKULTAS = '$beban_sks[ID_FAKULTAS]'");
	$smarty->assign('program_studi', $prodi);
}
else if ($mode == 'delete')
{
     $db->Query("SELECT * FROM BEBAN_SKS JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = BEBAN_SKS.ID_FAKULTAS WHERE ID_BEBAN_SKS = '$id_beban_sks'");
	$beban_sks = $db->FetchAssoc();
    $smarty->assign('beban_sks', $beban_sks);
}

$smarty->display("perkuliahan/bebansks/{$mode}.tpl");
?>
