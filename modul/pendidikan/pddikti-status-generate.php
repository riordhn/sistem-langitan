<?php
include 'config.php';
include 'class/aucc.class.php';
include 'class/pddikti.class.php';

$aucc       = new AUCC_Pendidikan($db);
$pddikti    = new PDDikti($db);

$semester_set = $aucc->get_semester();
$smarty->assign('semester_set', $semester_set);

$program_studi_set = $aucc->get_program_studi();
$smarty->assign('program_studi_set', $program_studi_set);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id_semester        = $_POST['id_semester'] != '' ? $_POST['id_semester'] : null;
    $id_program_studi   = $_POST['id_program_studi'] != '' ? $_POST['id_program_studi'] : null;
    $is_override        = $_POST['is_override'] == 'on' ? true : false;
    
    if ($id_semester != null && $id_program_studi != null) {
        $generate_result = $pddikti->generate_status_mahasiswa($id_semester, $id_program_studi, $is_override);
        
        $smarty->assign('generate_result', $generate_result);
    }
}

$smarty->display('pddikti/status-generate.tpl');
