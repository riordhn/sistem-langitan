<?php
include('config.php');

$mode = get('mode', 'view');
$id_jenjang = get('id_jenjang', 0);


if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {	
        $db->Query("INSERT INTO JENJANG (NM_JENJANG, NM_JENJANG_IJASAH, NM_JENJANG_SNGKAT, NIM_JENJANG, NM_JENJANG_EN) 
					VALUES ('$_POST[nm_jenjang]', '$_POST[nm_jenjang_ijasah]', '$_POST[nm_singkat_ijasah]', '$_POST[kode_nim]', '$_POST[nm_jenjang_en]')");
	}
    else if (post('mode') == 'edit')
    {
		$db->Query("UPDATE JENJANG SET NM_JENJANG = '$_POST[nm_jenjang]', NM_JENJANG_IJASAH = '$_POST[nm_jenjang_ijasah]' ,
					NM_JENJANG_SNGKAT = '$_POST[nm_singkat_ijasah]', NIM_JENJANG = '$_POST[kode_nim]', NM_JENJANG_EN = '$_POST[nm_jenjang_en]'
					WHERE ID_JENJANG = '$_POST[id_jenjang]'");
    }
    else if (post('mode') == 'delete')
    {
        $db->Query("DELETE JENJANG WHERE ID_JENJANG = '$_POST[id_jenjang]'");
    }
}

if ($mode == 'view')
{
    $jenjang_set = $db->QueryToArray("SELECT * FROM JENJANG ORDER BY NM_JENJANG ASC");
    $smarty->assign('jenjang_set', $jenjang_set);
}
else if ($mode == 'edit' or $mode == 'delete')
{
    $jenjang = $db->QueryToArray("SELECT * FROM JENJANG WHERE ID_JENJANG = '$id_jenjang'");
    $smarty->assign('jenjang', $jenjang);
}

$smarty->display("perkuliahan/jenjang/{$mode}.tpl");
?>