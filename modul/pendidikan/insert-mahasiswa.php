<?php

include 'config.php';
include 'class/aucc.class.php';
$id_pt = $id_pt_user;
$id_pengguna_admisi = $user->ID_PENGGUNA; 

$aucc = new AUCC_Pendidikan($db);
if (isset($_POST['cari_nim'])) {
    $nim = str_replace("'", "''", $_POST['cari_nim']);
	$mahasiswa = $aucc->cek_data($nim, $user->ID_PERGURUAN_TINGGI);
    $smarty->assign('mahasiswa', $mahasiswa);
    $smarty->assign('cari_nim', $_POST['cari_nim']);


    $smarty->assign('data_negara', $aucc->get_semua_negara());
    
    /*$tes = $aucc->get_negara($mahasiswa['LAHIR_PROP_MHS'])['ID_NEGARA'];
    echo "haha ".$tes;*/

    $smarty->assign('negara_lahir_mhs', $aucc->get_negara($mahasiswa['LAHIR_PROP_MHS'])['ID_NEGARA']);

    $smarty->assign('data_propinsi', $aucc->get_propinsi_by_negara($aucc->get_negara($mahasiswa['LAHIR_PROP_MHS'])['ID_NEGARA']));
    $smarty->assign('data_kota', $aucc->get_kota_by_propinsi($mahasiswa['LAHIR_PROP_MHS']));


    $thn = date("Y");
    $smarty->assign('thn_sekarang', $thn);

	$id_jenjang = $mahasiswa['ID_JENJANG'];

	// utk mahasisawa transfer
	$kode_pt_asal = $mahasiswa['KODE_PT_ASAL'];

} else if (isset($_POST['insert'])) {
	$nama_ibu = str_replace("'", "''", $_POST['nama_ibu']);
	$thn = $aucc->get_thn_semester($_POST['semester']);
    $pengguna = $aucc->cek_pengguna($_POST['nim'],$id_pt);
    if (empty($pengguna)) {
		$nama =  str_replace("'", "''", $_POST['nama']);
        $aucc->insert_pengguna_mahasiswa(stripslashes($nama), $_POST['nim'], $_POST['tgl_lahir'], $id_pt);
    } else {

        if( ! empty($_POST['email_alternate'])) {
            $email_alternate = $_POST['email_alternate'];

            if (!filter_var($email_alternate, FILTER_VALIDATE_EMAIL)) {
                echo '<script language="javascript">';
                echo 'alert("Email Pribadi Mahasiswa Gagal Diupdate, Format salah!")';
                echo '</script>';
            }
            else {
                $aucc->update_pengguna_mahasiswa(stripslashes($nama), $_POST['nim'], $id_pt, $email_alternate);
            }
        }
        else {
            $email_alternate = null;

            $aucc->update_pengguna_mahasiswa(stripslashes($nama), $_POST['nim'], $id_pt, $email_alternate);
        }        
    }
    
    $pengguna = $aucc->cek_pengguna($_POST['nim'],$id_pt);
    $mahasiswa = $aucc->cek_mahasiswa_by_perguruan_tinggi($_POST['nim'],$id_pt);
    
    if (empty($mahasiswa)) {
        $aucc->insert_mahasiswa($pengguna['ID_PENGGUNA'], $_POST['nim'], $_POST['prodi'], NULL, $thn['THN_AKADEMIK_SEMESTER'], $_POST['semester'],
                                    $_POST['lahir_propinsi'], $_POST['lahir_kota'], stripslashes($nama_ibu), $_POST['nik'],$_POST['alamat_jalan'],$_POST['alamat_dusun'],$_POST['alamat_rt'],$_POST['alamat_rw'],$_POST['alamat_kelurahan'],$_POST['alamat_kodepos'],$_POST['alamat_kecamatan']);
        $aucc->insert_role_mahasiswa($pengguna['ID_PENGGUNA']);

		$mahasiswa = $aucc->cek_mahasiswa_by_perguruan_tinggi($_POST['nim'],$id_pt);
		$aucc->insert_admisi_jalur($mahasiswa['ID_MHS'], $_POST['semester'], $id_pengguna_admisi, $_POST['jalur']);

		// khusus mahasiswa transfer
        //$aucc->update_jalur_admisi_transfer($mahasiswa['ID_MHS'], $_POST['sks_diakui'],$_POST['pt_asal'],$_POST['prodi_asal']);
        $aucc->update_jalur_admisi_transfer($mahasiswa['ID_MHS'], $_POST['sks_diakui'],$_POST['pt_asal'], NULL);

    } else {
        $aucc->update_mahasiswa(
            $_POST['kelompok'], $_POST['nim'], $_POST['prodi'], $thn['THN_AKADEMIK_SEMESTER'], $_POST['semester'],
            $_POST['lahir_propinsi'], $_POST['lahir_kota'], stripslashes($nama_ibu),$_POST['nik'],$_POST['alamat_jalan'],$_POST['alamat_dusun'],$_POST['alamat_rt'],$_POST['alamat_rw'],$_POST['alamat_kelurahan'],$_POST['alamat_kodepos'],$_POST['alamat_kecamatan']);
    }
    $jalur = $aucc->cek_jalur($_POST['nim'],$id_pt);
    if (empty($jalur)) {
        $aucc->insert_jalur_mahasiswa($mahasiswa['ID_MHS'], $_POST['prodi'], $_POST['jalur'], $_POST['semester'], $_POST['nim']);
    } else {
        $aucc->update_jalur_mahasiswa($mahasiswa['ID_MHS'], $_POST['prodi'], $_POST['jalur'], $_POST['semester'], $_POST['nim']);
        //$aucc->update_jalur_admisi($mahasiswa['ID_MHS'], $_POST['jalur']);
    }

    $smarty->assign('pesan', "Data Berhasil Dimasukkan!");

} else if (isset($_POST['update'])) {
	$nama_ibu = str_replace("'", "''", $_POST['nama_ibu']);
    $pengguna = $aucc->cek_pengguna($_POST['nim'],$id_pt);
	$thn = $aucc->get_thn_semester($_POST['semester']);
	
    if ($pengguna && empty($_POST['nim_lama'])) {

        if( ! empty($_POST['email_alternate'])) {
            $email_alternate = $_POST['email_alternate'];

            if (!filter_var($email_alternate, FILTER_VALIDATE_EMAIL)) {
                echo '<script language="javascript">';
                echo 'alert("Email Pribadi Mahasiswa Gagal Diupdate, Format salah!")';
                echo '</script>';
            }
            else {
                $nama =  str_replace("'", "''", $_POST['nama']);

                $aucc->update_pengguna($nama, $_POST['nim'], $_POST['tgl_lahir'], $pengguna['ID_PENGGUNA'], $user->ID_PERGURUAN_TINGGI, $email_alternate);
            }
        }
        else {
            $email_alternate = null;
            $nama =  str_replace("'", "''", $_POST['nama']);

            $aucc->update_pengguna($nama, $_POST['nim'], $_POST['tgl_lahir'], $pengguna['ID_PENGGUNA'], $user->ID_PERGURUAN_TINGGI, $email_alternate);
        }
    }
    // utk menangani ganti nim MABA
    // FIKRIE
    // 29-06-2016
    else{
        $pengguna = $aucc->cek_pengguna($_POST['nim_lama'],$id_pt);


        if( ! empty($_POST['email_alternate'])) {
            $email_alternate = $_POST['email_alternate'];

            if (!filter_var($email_alternate, FILTER_VALIDATE_EMAIL)) {
                echo '<script language="javascript">';
                echo 'alert("Email Pribadi Mahasiswa Gagal Diupdate, Format salah!")';
                echo '</script>';
            }
            else {
                $nama =  str_replace("'", "''", $_POST['nama']);
                
                $aucc->update_pengguna($nama, $_POST['nim'], $_POST['tgl_lahir'], $pengguna['ID_PENGGUNA'], $user->ID_PERGURUAN_TINGGI, $email_alternate);
            }
        }
        else {
            $email_alternate = null;
            $nama =  str_replace("'", "''", $_POST['nama']);
            
            $aucc->update_pengguna($nama, $_POST['nim'], $_POST['tgl_lahir'], $pengguna['ID_PENGGUNA'], $user->ID_PERGURUAN_TINGGI, $email_alternate);
        }
    }

    $mahasiswa = $aucc->cek_mahasiswa_by_perguruan_tinggi($_POST['nim'],$id_pt);
    // memastikan agar ganti nim tidak salah ke mahasiswa yang sudah ada datanya
    // FIKRIE (29-08-2016)
    if ($mahasiswa && empty($_POST['nim_lama'])) {
        $aucc->update_mahasiswa(NULL, $_POST['nim'], $_POST['prodi'], $thn['THN_AKADEMIK_SEMESTER'], $_POST['semester'], $mahasiswa['ID_MHS'],
                    $_POST['lahir_propinsi'], $_POST['lahir_kota'], stripslashes($nama_ibu),$_POST['nik'],$_POST['alamat_jalan'],$_POST['alamat_dusun'],$_POST['alamat_rt'],$_POST['alamat_rw'],$_POST['alamat_kelurahan'],$_POST['alamat_kodepos'],$_POST['alamat_kecamatan']);        
    }
    // utk menangani ganti nim MABA
    // FIKRIE
    // 29-06-2016
    else{
        $mahasiswa = $aucc->cek_mahasiswa_by_perguruan_tinggi($_POST['nim_lama'],$id_pt);

        $aucc->update_mahasiswa(NULL, $_POST['nim'], $_POST['prodi'], $thn['THN_AKADEMIK_SEMESTER'], $_POST['semester'], $mahasiswa['ID_MHS'],
                    $_POST['lahir_propinsi'], $_POST['lahir_kota'], stripslashes($nama_ibu),$_POST['nik'],$_POST['alamat_jalan'],$_POST['alamat_dusun'],$_POST['alamat_rt'],$_POST['alamat_rw'],$_POST['alamat_kelurahan'],$_POST['alamat_kodepos'],$_POST['alamat_kecamatan']);


        $aucc->update_nim_lama_mahasiswa($_POST['nim_lama'],$mahasiswa['ID_MHS']);
        $aucc->update_calon_mahasiswa($mahasiswa['ID_C_MHS'], $_POST['nim']);
    }

    $jalur = $aucc->cek_jalur($_POST['nim'],$id_pt);
    if ($jalur) {
        $mahasiswa = $aucc->cek_mahasiswa_by_perguruan_tinggi($_POST['nim'],$id_pt);
        $aucc->update_jalur_mahasiswa($mahasiswa['ID_MHS'], $_POST['prodi'], $_POST['jalur'], $_POST['semester'], $_POST['nim']);
        $aucc->update_jalur_admisi($mahasiswa['ID_MHS'], $_POST['jalur'],$_POST['semester']);

        // khusus mahasiswa transfer
        //$aucc->update_jalur_admisi_transfer($mahasiswa['ID_MHS'], $_POST['sks_diakui'],$_POST['pt_asal'],$_POST['prodi_asal']);
        $aucc->update_jalur_admisi_transfer($mahasiswa['ID_MHS'], $_POST['sks_diakui'],$_POST['pt_asal'], NULL);
    } else {
        $mahasiswa = $aucc->cek_mahasiswa_by_perguruan_tinggi($_POST['nim'],$id_pt);
        $aucc->insert_jalur_mahasiswa($mahasiswa['ID_MHS'], $_POST['prodi'], $_POST['jalur'], $_POST['semester'], $_POST['nim']);
    }

    $smarty->assign('pesan', "Data Berhasil Diupdate!");

    
}

if ($_GET['mode'] == 'cari-pt')
{
    
    $nama_pt = strtoupper(trim($_GET['nama_pt']));


    $cek = $aucc->get_count_pt_asal($nama_pt);
        /*echo "huhuhuhahaha";*/

    if($cek != 1){

        $db->Query("SELECT 'kosong' AS NPSN, 'Perguruan Tinggi tidak ditemukan ' || 'atau kurang spesifik' AS NAMA_PERGURUAN_TINGGI FROM DUAL");
        echo json_encode($db->FetchAssoc());
        exit();
    }
    else{

        $db->Query("SELECT NPSN, NPSN || ' - ' || NAMA_PERGURUAN_TINGGI AS NAMA_PERGURUAN_TINGGI 
                        FROM PERGURUAN_TINGGI 
                        WHERE (UPPER(NAMA_PERGURUAN_TINGGI) LIKE '%{$nama_pt}%' OR NPSN = '{$nama_pt}') AND ROWNUM <= 1");
        echo json_encode($db->FetchAssoc());
        exit();
    }
}

$smarty->assign('data_jenjang', $aucc->get_jenjang());
$smarty->assign('data_program_studi', $aucc->get_program_studi_by_jenjang($id_jenjang, $user->ID_PERGURUAN_TINGGI));

// utk mahasiswa transfer
//$smarty->assign('perguruan_tinggi_asal', $aucc->get_pt_asal());
/*$smarty->assign('perguruan_tinggi_asal', $aucc->get_pt_asal());
$smarty->assign('program_studi_asal', $aucc->get_program_studi_by_pt_asal($kode_pt_asal, $user->ID_PERGURUAN_TINGGI));*/

$smarty->assign('data_jalur', $aucc->get_jalur($user->ID_PERGURUAN_TINGGI));
$smarty->assign('data_kelompok', $aucc->get_kelompok());
$smarty->assign('data_semester', $aucc->get_semester_angkatan($user->ID_PERGURUAN_TINGGI));
$smarty->display('mahasiswa/insert/insert-mahasiswa.tpl');
?>
