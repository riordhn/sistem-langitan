<?php

include 'config.php';
include 'class/lulusan.class.php';

$id_pt = $id_pt_user;
$dt_lulusan = new lulusan($db);

if (isset($_POST['tampil'])) {
    $smarty->assign('data-lulusan', $dt_lulusan->get_lulusan(1, $_POST['tahun_lulus']));
   	$smarty->assign('tampil', $_POST['tampil']);
    $smarty->assign('tahun_lulus', $_POST['tahun_lulus']);
}
$smarty->assign('data_tahun_lulus', $dt_lulusan->get_tahun_lulus($id_pt));
$smarty->display("lulusan/data-lulusan.tpl");