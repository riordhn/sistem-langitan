<?php
include('config.php');
include 'class/report_mahasiswa.class.php';
$rm = new report_mahasiswa($db);
$id_pengguna= $user->ID_PENGGUNA; 

$mode = post('mode', 'view');

if($mode == 'generate'){
	
    // Mendapatkan ID
    $no_ujian_set = $_POST['no_ujian'];
    
    // Tabel informasi yg terjadi kegagalan
    $result_set = array();
  	//$no_ujian_set = array('82120200221');
    if (!empty($no_ujian_set))
    {
        foreach ($no_ujian_set as $no_ujian)
        {

            $db->Query("
                select cmd.kesehatan_kesimpulan_akhir, p.tahun, ps.id_fakultas, ps.kode_program_studi, cmb.nm_c_mhs, 
						cmb.jenis_kelamin, cmb.id_agama, cmb.tgl_lahir, cmb.id_kota_lahir, cmb.nim_mhs, ps.id_jenjang, cmb.id_c_mhs, nim_jenjang,
						cmb.id_jalur, cmb.id_penerimaan, p.is_snmptn, ps.id_program_studi
                from calon_mahasiswa_data cmd
                join calon_mahasiswa_baru cmb on cmb.id_c_mhs = cmd.id_c_mhs
                join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
                join program_studi ps on ps.id_program_studi = cmb.id_program_studi
				join jenjang j on j.id_jenjang = ps.id_jenjang
                where cmd.id_c_mhs = (select id_c_mhs from calon_mahasiswa_baru cmd where no_ujian = '{$no_ujian}')");
            $row = $db->FetchAssoc();			
					
					$db->Query("SELECT PENERIMAAN.TAHUN, PENERIMAAN.SEMESTER, ID_SEMESTER
								FROM CALON_MAHASISWA_BARU
								JOIN PENERIMAAN ON PENERIMAAN.ID_PENERIMAAN = CALON_MAHASISWA_BARU.ID_PENERIMAAN
								WHERE CALON_MAHASISWA_BARU.ID_C_MHS = '".$row['ID_C_MHS']."'");
                    
					$penerimaan = $db->FetchAssoc();
					$nm_semester = $penerimaan['SEMESTER'];
					$thn_nim = substr($penerimaan['TAHUN'], -2);
					$id_semester_masuk = $penerimaan['ID_SEMESTER'];
					
					
					if($nm_semester == 'Genap'){
						$semester_nim = 2;
						$nama_semester = 'Genap';
					}else{
						$semester_nim = 1;
						$nama_semester = 'Ganjil';
					}
					
					
					$db->Query("select * from semester where nm_semester like '%$nama_semester%' and thn_akademik_semester = '".$penerimaan['TAHUN']."'");
                    
					$semester_masuk = $db->FetchAssoc();
					//$id_semester_masuk = $semester_masuk['ID_SEMESTER'];
					

					if($row['ID_JALUR'] == 4){
						//alih jalur
						$jenjang_nim = 2;
					}else{
						$jenjang_nim = $row['NIM_JENJANG'];
					}
					
					
					//JALUR
					if($row['IS_SNMPTN'] == 1){
						//SMPTN UNDANGAN
						$jalur_nim = 1;
					}elseif($row['ID_PENERIMAAN'] == 45){
						//SNMPTN TULIS
						$jalur_nim = 2;
					}else{
						//MANDIRI, S2, S3, PROFESI, SPESIALIS
						$jalur_nim = 3;	
					}
					 //BELUM YANG INTERNASIONAL
					
					
					//BANYUWANGI
					if($row['ID_PROGRAM_STUDI'] == 238 or $row['ID_PROGRAM_STUDI'] == 239 or $row['ID_PROGRAM_STUDI'] == 240 or $row['ID_PROGRAM_STUDI'] == 241){
						$jalur_nim = 5;
					}

            // simpan ke array
            array_push($result_set, array(
                'NO_UJIAN'      => $no_ujian,
                'NIM_MHS'       => str_pad($row['ID_FAKULTAS'], 2, '0', STR_PAD_LEFT) . $thn_nim . $semester_nim . $row['KODE_PROGRAM_STUDI'] . $jenjang_nim . $jalur_nim,
                'NM_C_MHS'      => $row['NM_C_MHS'],
                'JENIS_KELAMIN' => $row['JENIS_KELAMIN'],
                'ID_AGAMA'      => $row['ID_AGAMA'],
                'TGL_LAHIR'     => $row['TGL_LAHIR'],
                'ID_KOTA_LAHIR' => $row['ID_KOTA_LAHIR'],
                'TAHUN'         => $row['TAHUN'],
                'ID_PENGGUNA'   => '',
                'ID_MHS'        => ''
            ));
        }

		

        $db->BeginTransaction();
        
/*        foreach ($result_set as &$result)
        {

				        // Mendapatkan nomer terakhir yg paling besar + 1

        $db->Query("SELECT MAX(NIM_MHS) NIM_MHS FROM MAHASISWA WHERE NIM_MHS LIKE '{$result['NIM_MHS']}%'");
        
        $row = $db->FetchAssoc();
        $len = strlen($result['NIM_MHS']);
        
        if ($row['NIM_MHS'] != '')
            $no_seri = substr($row['NIM_MHS'], $len, 12 - $len);
        else
            $no_seri = str_pad('0', 12 - $len, '0', STR_PAD_LEFT);


                // tambah satu
                $no_seri = str_pad(((int)$no_seri) + 1, 12 - $len, '0', STR_PAD_LEFT);
        

                // memasukan hasil generate sementara ke nim
                $result['NIM_MHS'] = $result['NIM_MHS'] . $no_seri;

				//cek sudah ada atau belum
				/*$db->Query("select count(*) as cek from mahasiswa 
							join calon_mahasiswa_baru on calon_mahasiswa_baru.id_c_mhs = mahasiswa.id_c_mhs
							where no_ujian = '{$result['NO_UJIAN']}'");*/
				/*$db->Query("select count(*) as cek from calon_mahasiswa_baru
							where nim_mhs = '{$result['NIM_MHS']}'");
				$cek = $db->FetchAssoc();
				
				if($cek['CEK'] <= '0'){
                // simpan ke tabel CMB
                $hasil = $db->Query("update calon_mahasiswa_baru set nim_mhs = '{$result['NIM_MHS']}', tgl_generate_nim = to_date('".date('YmdHis')."', 'YYYYMMDDHH24MISS') where no_ujian = '{$result['NO_UJIAN']}'")or die('gagal cmhs');
				}
                if ($hasil) {
					$result['RESULT'] = "Generate Berhasil"; 
				//}
				
				//echo "<br/>AFTER UPDATE CALON : " . print_r(error_get_last(), true);
				
				
        }*/
        
		
        foreach ($result_set as &$result)
        {
				$db->Query("SELECT MAX(NIM_MHS) NIM_MHS FROM MAHASISWA WHERE NIM_MHS LIKE '{$result['NIM_MHS']}%'");
			
				$row = $db->FetchAssoc();
				$len = strlen($result['NIM_MHS']);
				
				if ($row['NIM_MHS'] != '')
					$no_seri = substr($row['NIM_MHS'], $len, 12 - $len);
				else
					$no_seri = str_pad('0', 12 - $len, '0', STR_PAD_LEFT);


                // tambah satu
                $no_seri = str_pad(((int)$no_seri) + 1, 12 - $len, '0', STR_PAD_LEFT);
        

                // memasukan hasil generate sementara ke nim
                $result['NIM_MHS'] = $result['NIM_MHS'] . $no_seri;
				$nama =  str_replace("'", "''", $result['NM_C_MHS']);
				$nama = ucwords(strtolower($nama));
            //if ($result['RESULT'] == 'Generate Berhasil')
            //{
				
				
				
                // insert ke pengguna
				// menyiapkan data password utk disimpan ke db
				$pass_ortu = 'unair20'.substr($result['NIM_MHS'],2, 2);
				$password_hash = sha1($result['NIM_MHS']);
				$password_encrypted = $user->PublicKeyEncrypt($result['NIM_MHS']);
				$password_hash_ortu = sha1($pass_ortu);
				$password_encrypted_ortu = $user->PublicKeyEncrypt($pass_ortu);
				$last_time_password = date('d-M-Y');
			
				$db->Query("insert into pengguna
                    ( username, se1, nm_pengguna, id_agama, kelamin_pengguna, tgl_lahir_pengguna, tempat_lahir, id_role, join_table,
						password_hash, password_encrypted, last_time_password, password_hash_temp, password_hash_temp_expired, password_must_change) values
                    ('".$result['NIM_MHS']."', '".$user->Encrypt($result['NIM_MHS'])."', '".$nama."',
					'".$result['ID_AGAMA']."', '".$result['JENIS_KELAMIN']."', '".$result['TGL_LAHIR']."', '".$result['ID_KOTA_LAHIR']."',3,3,
					'{$password_hash}', '{$password_encrypted}', '{$last_time_password}', null, null, 1)");
              // echo "<br/>AFTER INSERT PENGGUNA : " . print_r(error_get_last(), true);
				
				
			
                // proses insert ke mahasiswa
	  $db->Query("update calon_mahasiswa_baru set nim_mhs = '{$result['NIM_MHS']}', tgl_generate_nim = to_date('".date('YmdHis')."', 'YYYYMMDDHH24MISS') 
	  				where no_ujian = '{$result['NO_UJIAN']}'")or die('gagal cmhs');
	  
                    $hasil2 = $db->Query("
                        insert into mahasiswa (id_c_mhs, 
							id_pengguna, nim_mhs, nim_bank, id_program_studi, thn_angkatan_mhs, id_kelompok_biaya, 
							id_prodi_minat, status_akademik_mhs, id_semester_masuk, STATUS_CEKAL, LAHIR_KOTA_MHS, 
							TGL_TERDAFTAR_MHS, alamat_mhs, mobile_mhs, asal_kota_mhs, ALAMAT_ASAL_MHS, ALAMAT_ASAL_MHS_KOTA, ID_SEKOLAH_ASAL_MHS,
							NM_AYAH_MHS, PENDIDIKAN_AYAH_MHS, PEKERJAAN_AYAH_MHS, ALAMAT_AYAH_MHS, ALAMAT_AYAH_MHS_KOTA,
							NM_IBU_MHS, PENDIDIKAN_IBU_MHS, PEKERJAAN_IBU_MHS, ALAMAT_IBU_MHS, ALAMAT_IBU_MHS_KOTA, STATUS, ID_KEBANGSAAN)
                            select (SELECT ID_C_MHS FROM CALON_MAHASISWA_BARU WHERE NO_UJIAN = '".$result['NO_UJIAN']."'), 
							p.id_pengguna, p.username, p.username, cmb.id_program_studi, pen.tahun, cmb.id_kelompok_biaya,
							cmp.id_prodi_minat, 1 status_akademik_mhs, {$id_semester_masuk} id_semester_masuk, 0 STATUS_CEKAL, cmb.ID_KOTA_LAHIR,
							TGL_DITERIMA, cmb.ALAMAT, cmb.TELP, cmb.ID_KOTA, cmb.ALAMAT, cmb.ID_KOTA, cms.ID_SEKOLAH_ASAL, 
							NAMA_AYAH, PENDIDIKAN_AYAH, PEKERJAAN_AYAH, ALAMAT_AYAH, ID_KOTA_AYAH,
							NAMA_IBU, PENDIDIKAN_IBU, PEKERJAAN_IBU, ALAMAT_IBU, ID_KOTA_IBU, 
							(CASE WHEN cmb.id_jalur = 27 THEN 'I' WHEN cmb.ID_JALUR = 22 OR cmb.ID_JALUR = 4 THEN 'AJ' ELSE 'R' END) AS STAT, 
							(case when cmb.id_kebangsaan is null then 114 else cmb.id_kebangsaan end) as ID_KEBANGSAAN
                            from pengguna p 
                            join calon_mahasiswa_baru cmb on cmb.nim_mhs = p.username
                            join penerimaan pen on pen.id_penerimaan = cmb.id_penerimaan
                            left join calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
							left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
							left join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                            where p.username = '{$result['NIM_MHS']}'");	
										
			
                 // echo "<br/>AFTER INSERT MAHASISWA : " . print_r(error_get_last(), true);
				if(!isset($hasil2))
                {
                    $result['RESULT'] = "Gagal Insert Mahasiswa <br/>" . print_r(error_get_last(), true);
                    $db->Rollback();
                }
				
				
				//insert ortu mhs
				$db->Query("insert into pengguna
						   (username, se1, nm_pengguna, id_agama, kelamin_pengguna, id_role, join_table,
						   password_hash, password_encrypted, last_time_password, password_hash_temp, password_hash_temp_expired, password_must_change) 
							SELECT NIM_MHS || '13', '".$user->Encrypt($pass_ortu)."', 
							COALESCE(CALON_MAHASISWA_ORTU.NAMA_AYAH, CALON_MAHASISWA_ORTU.NAMA_IBU, NM_AYAH_MHS, NM_IBU_MHS, '' ) AS NAMA, ID_AGAMA, 
							(CASE WHEN NM_AYAH_MHS IS NOT NULL THEN '1' WHEN NM_IBU_MHS IS NOT NULL THEN '2' ELSE '' END) AS JK, 6, 4,
							'{$password_hash_ortu}', '{$password_encrypted_ortu}', '{$last_time_password}', null, null, 1
							FROM MAHASISWA
							JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
							LEFT JOIN CALON_MAHASISWA_ORTU ON CALON_MAHASISWA_ORTU.ID_C_MHS = MAHASISWA.ID_C_MHS
							WHERE username = '".$result['NIM_MHS']."'") or die ('GAGAL ORTU');
				
				
                // proses insert jalur
                   
                    // mendapatkan id_mhs
                    $db->Query("select id_mhs from mahasiswa where nim_mhs = '{$result['NIM_MHS']}'");
                    $row = $db->FetchAssoc();
                    $result['ID_MHS'] = $row['ID_MHS'];
					
					
					//INSERT PEMBAYARAN

					$db->Query("INSERT INTO PEMBAYARAN (ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, ID_BANK, 
									ID_BANK_VIA, BESAR_BIAYA, DENDA_BIAYA, TGL_BAYAR, IS_TAGIH, 
									NO_TRANSAKSI, KETERANGAN, IS_TARIK, ID_SEMESTER_BAYAR, ID_STATUS_PEMBAYARAN, FLAG_PEMINDAHAN)
								SELECT '".$row['ID_MHS']."' AS ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, ID_BANK, 
									ID_BANK_VIA, BESAR_BIAYA, DENDA_BIAYA, 
									TGL_BAYAR, IS_TAGIH, 
									NO_TRANSAKSI, KETERANGAN, IS_TARIK, ID_SEMESTER_BAYAR, 
									PEMBAYARAN_CMHS.ID_STATUS_PEMBAYARAN, 1 AS FLAG_PEMINDAHAN 
								FROM PEMBAYARAN_CMHS
								JOIN CALON_MAHASISWA_BARU ON CALON_MAHASISWA_BARU.ID_C_MHS = PEMBAYARAN_CMHS.ID_C_MHS
								WHERE NO_UJIAN = '".$result['NO_UJIAN']."'") ;
					//echo "<br/>AFTER INSERT PEMBAYARAN : " . print_r(error_get_last(), true);
					
					//ID_CMHS
					$db->Query("SELECT ID_C_MHS FROM CALON_MAHASISWA_BARU WHERE NO_UJIAN = '".$result['NO_UJIAN']."'");
					$row12 = $db->FetchAssoc();
					$id_c_mhs = $row12['ID_C_MHS'];
					
					$set_biaya = $db->QueryToArray("SELECT DETAIL_BIAYA.* FROM DETAIL_BIAYA
											JOIN BIAYA_KULIAH_CMHS ON BIAYA_KULIAH_CMHS.ID_BIAYA_KULIAH = DETAIL_BIAYA.ID_BIAYA_KULIAH
											JOIN BIAYA ON BIAYA.ID_BIAYA = DETAIL_BIAYA.ID_BIAYA
											WHERE BIAYA_KULIAH_CMHS.ID_C_MHS = '$id_c_mhs' AND PENDAFTARAN_BIAYA = 0");
					
					$db->Query("INSERT INTO BIAYA_KULIAH_MHS (ID_BIAYA_KULIAH, ID_MHS) 
							VALUES ('".$set_biaya[0]['ID_BIAYA_KULIAH']."', '".$row['ID_MHS']."')")  or die('gagal 13');
					
			$db->Query("UPDATE MAHASISWA 
						SET ID_KELOMPOK_BIAYA = (SELECT ID_KELOMPOK_BIAYA FROM BIAYA_KULIAH WHERE ID_BIAYA_KULIAH = '".$set_biaya[0]['ID_BIAYA_KULIAH']."') 
						WHERE ID_MHS = '".$row['ID_MHS']."'") or die('gagal 14');
					
					 //INSERT ADMISI
					 $tgl = date('Y-m-d');
			$db->Query("INSERT INTO ADMISI (ID_MHS, STATUS_AKD_MHS, TGL_USULAN, TGL_APV, ID_SEMESTER, STATUS_APV, ID_PENGGUNA)
			VALUES ('".$row['ID_MHS']."', '1', to_date('$tgl','YYYY-MM-DD'), to_date('$tgl','YYYY-MM-DD'), 
					'$id_semester_masuk', '1', $id_pengguna)") or die('gagal 15');   
		//	echo "<br/>AFTER INSERT ADMISI : " . print_r(error_get_last(), true);

                    // insert ke jalur
                    $hasil3 = $db->Query("
                        insert into jalur_mahasiswa (id_mhs, id_program_studi, id_jalur, nim_mhs, id_semester, id_jalur_aktif)
                            select m.id_mhs, m.id_program_studi, p.id_jalur, m.nim_mhs,ID_SEMESTER_MASUK , 1 as id_jalur_aktif
                            from mahasiswa m
                            join calon_mahasiswa_baru cmb on cmb.nim_mhs = m.nim_mhs
                            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
                            where m.nim_mhs = '{$result['NIM_MHS']}'") or die('gagal 16');
                   // echo "<br/>AFTER INSERT jalur_mahasiswa : " . print_r(error_get_last(), true);
					
                  // echo "<br/>AFTER INSERT JALUR : " . print_r(error_get_last(), true);

                

                // insert ke fingerprint
                if ($hasil3)
                {
                    $hasil4 = $db->Query("
                        insert into fingerprint_mahasiswa (finger_data, nim_mhs, tgl_lahir, huruf_depan)
                            select fc.finger_data, cmb.nim_mhs, cmb.tgl_lahir, upper(substr(nm_c_mhs, 1, 1)) huruf_depan
                            from calon_mahasiswa_baru cmb
                            join fingerprint_cmhs fc on fc.id_c_mhs = cmb.id_c_mhs
                            where cmb.no_ujian = '".$result['NO_UJIAN']."'");
    
                   // echo "<br/>AFTER INSERT FINGER : " . print_r(error_get_last(), true);
                            
                    if ($hasil4)
                    {   
                        $result['RESULT'] = 'Sukses';
                        $db->Commit();
                    }
                    else
                    {
                        $result['RESULT'] = 'Gagal Copy Finger';
                        $db->Rollback();
                    }
                }
                else
                {
                    $result['RESULT'] = "Gagal Insert Jalur <br/>" . print_r(error_get_last(), true);
                    $db->Rollback();
                }
           }
        //}
		
        $smarty->assign('result_set', $result_set);
    }

}else{
        $cmb_set = $db->QueryToArray("
			select * from (
            select cmb.id_c_mhs, m.nim_mhs, cmb.no_ujian, cmb.nm_c_mhs, nm_program_studi, tgl_regmaba,
                (select count(*) from pembayaran_cmhs pc where pc.id_c_mhs = cmb.id_c_mhs and (id_status_pembayaran != 2 or tgl_bayar is not null)) as pembayaran,
                p.nm_penerimaan||' '||p.tahun||' Gel. '||p.gelombang||' Smt. '||p.semester penerimaan, p.tahun, status_bidik_misi, TIDAK_GENERATE_NIM, cmb.ID_KEBANGSAAN
            from calon_mahasiswa_baru cmb 
			join program_studi ps on ps.id_program_studi = cmb.id_program_studi
            left join mahasiswa m on m.id_c_mhs = cmb.id_c_mhs
            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
			join calon_mahasiswa_data cmd on cmd.id_c_mhs = cmb.id_c_mhs
			)
			where (pembayaran >= 1) and nim_mhs is null and tahun >= '2012' and (TIDAK_GENERATE_NIM = 0 OR TIDAK_GENERATE_NIM IS NULL)
			and tgl_regmaba is not null -- and id_kebangsaan is not null --and no_ujian = '86130201004'
            order by nim_mhs nulls first, no_ujian asc");
        $smarty->assign("cmb_set", $cmb_set);
}

$smarty->display("pendaftaran/nim_maba/pendaftaran-nim-maba.tpl");
?>
