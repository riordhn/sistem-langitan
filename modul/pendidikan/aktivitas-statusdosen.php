<?php
include('config.php');

$mode = get('mode', 'fakultas');
$id_fakultas = get('id_fakultas', '');
$id_program_studi = get('id_program_studi', '');

if ($mode == 'fakultas')
{
	$smarty->assign('fakultas_set', $db->QueryToArray("
		select f.id_fakultas, nm_fakultas, count(id_dosen) as jumlah_dosen from dosen d
		join program_studi ps on ps.id_program_studi = d.id_program_studi
		join fakultas f on f.id_fakultas = ps.id_fakultas
		where ps.id_program_studi <> 197
		group by f.id_fakultas, nm_fakultas
		order by f.id_fakultas"));
}

if ($mode == 'prodi' && $id_fakultas != '')
{
	$db->Query("SELECT * FROM FAKULTAS WHERE ID_FAKULTAS = {$id_fakultas}");
	$row = $db->FetchAssoc();
	$smarty->assign('nm_fakultas', $row['NM_FAKULTAS']);
	
	$smarty->assign('program_studi_set', $db->QueryToArray("
		select ps.id_program_studi, j.nm_jenjang, ps.nm_program_studi, count(id_dosen) as jumlah_dosen from dosen d
		join program_studi ps on ps.id_program_studi = d.id_program_studi
		join jenjang j on j.id_jenjang = ps.id_jenjang
		where ps.id_program_studi <> 197 and ps.id_fakultas = {$id_fakultas} 
		group by ps.id_program_studi, ps.nm_program_studi, j.id_jenjang, j.nm_jenjang
		order by j.id_jenjang"));
}

if ($mode == 'view' && $id_program_studi != '')
{
	$db->Query("
		SELECT * FROM PROGRAM_STUDI
		JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
		WHERE ID_PROGRAM_STUDI = {$id_program_studi}");
	$row = $db->FetchAssoc();
	$smarty->assign('program_studi', $row);
	
	$smarty->assign('dosen_set', $db->QueryToArray("
		SELECT
          J.NM_JENJANG,
          PS.NM_PROGRAM_STUDI,
          D.NIDN_DOSEN,
          D.NIP_DOSEN,
          P.NM_PENGGUNA,
          G.NM_GOLONGAN,
          JP.NM_JABATAN_PEGAWAI
        FROM DOSEN D
        LEFT JOIN PENGGUNA P ON P.ID_PENGGUNA = D.ID_PENGGUNA
        LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = D.ID_PROGRAM_STUDI
        LEFT JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
        LEFT JOIN GOLONGAN G ON G.ID_GOLONGAN = D.ID_GOLONGAN
        LEFT JOIN JABATAN_PEGAWAI JP ON JP.ID_JABATAN_PEGAWAI = D.ID_JABATAN_PEGAWAI
        WHERE D.ID_PROGRAM_STUDI = {$id_program_studi} ORDER BY P.NM_PENGGUNA"));
}

/**
$fakultas_table = new FAKULTAS_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);
$dosen_table = new DOSEN_TABLE($db);
$pengguna_table = new PENGGUNA_TABLE($db);
$sejarah_status_table = new SEJARAH_STATUS_TABLE($db);
$status_pengguna_table = new STATUS_PENGGUNA_TABLE($db);

if ($request_method == 'POST')
{
    $keys = array_keys($_POST);
    for ($i = 0; $i < count($keys); $i++)
    {
        if (substr($keys[$i], 0, 1) == 'C') { continue; }
        
        $id_pengguna = str_replace('P', '', $keys[$i]);
        
        // Mencari status lama yg aktif
        $sejarah_status_aktif = $sejarah_status_table->SelectWhere("ID_PENGGUNA = {$id_pengguna} AND AKTIF_SEJARAH_STATUS = 1");
        if ($sejarah_status_aktif->Count() > 0)
        {
            $sejarah_status_aktif->Get(0)->AKTIF_SEJARAH_STATUS = 0;
            $sejarah_status_aktif->Get(0)->TGL_SELESAI_SEJARAH_STATUS = date('d-M-Y');
        }
        
        // Mengupdate status yg belum di approve
        $sejarah_status_set = $sejarah_status_table->SelectWhere("ID_PENGGUNA = {$id_pengguna} AND AKTIF_SEJARAH_STATUS = 2");
        
        if ($_POST[$keys[$i]] == 1)
        {
            $sejarah_status_set->Get(0)->AKTIF_SEJARAH_STATUS = 1;
            $sejarah_status_set->Get(0)->TGL_MULAI_SEJARAH_STATUS = date('d-M-Y');
            $sejarah_status_set->Get(0)->KETERANGAN_SEJARAH_STATUS = $_POST["C{$id_pengguna}"];
            
            // update status lama jadi nonaktif
            if ($sejarah_status_aktif->Count() > 0)
                $sejarah_status_table->UpdateSet($sejarah_status_aktif);
        }
        else
        {
            $sejarah_status_set->Get(0)->AKTIF_SEJARAH_STATUS = 3;
            $sejarah_status_set->Get(0)->KETERANGAN_SEJARAH_STATUS = $_POST["C{$id_pengguna}"];
        }
        
        // update status yang baru
        $sejarah_status_table->UpdateSet($sejarah_status_set);
    }
}

if ($mode == 'fakultas')
{
    $fakultas_set = $fakultas_table->Select();
    $smarty->assign('fakultas_set', $fakultas_set);
}
else if ($mode == 'prodi')
{
    $fakultas = $fakultas_table->Single($id_fakultas);
    $program_studi_table->FillFakultas($fakultas);
    $smarty->assign('fakultas', $fakultas);
}
else if ($mode == 'view')
{
    $program_studi = $program_studi_table->Single($id_program_studi);
    $fakultas_table->FillProgramStudi($program_studi);
    
    $criteria = "
        JOIN SEJARAH_STATUS SS ON SS.ID_PENGGUNA = DOSEN.ID_PENGGUNA
        WHERE ID_PROGRAM_STUDI = {$id_program_studi} AND AKTIF_SEJARAH_STATUS = 2";
    $dosen_set = $dosen_table->SelectCriteria($criteria);
    $pengguna_table->FillDosenSet($dosen_set);
    
    for ($i = 0; $i < $dosen_set->Count(); $i++)
    {
        $pengguna = $dosen_set->Get($i)->PENGGUNA;
        $sejarah_status_set = $sejarah_status_table->SelectWhere("ID_PENGGUNA = {$pengguna->ID_PENGGUNA} AND AKTIF_SEJARAH_STATUS = 2");
        $status_pengguna_table->FillSejarahStatusSet($sejarah_status_set);
        $pengguna->SEJARAH_STATUSs = $sejarah_status_set;
    }
    
    $smarty->assign('program_studi', $program_studi);
    $smarty->assign('dosen_set', $dosen_set);
}
**/
$smarty->display("aktivitas/statusdosen/$mode.tpl");
?>
