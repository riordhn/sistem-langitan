<?php
include('config.php');
include 'class/report_mahasiswa.class.php';
$id_pengguna= $user->ID_PENGGUNA; 
$rm = new report_mahasiswa($db);


$smarty->assign('data_fakultas', $rm->get_fakultas());
$smarty->assign('data_jenjang', $db->QueryToArray("SELECT * FROM JENJANG WHERE ID_JENJANG = 9"));
$smarty->assign('data_jenjangs1', $db->QueryToArray("SELECT * FROM JENJANG WHERE ID_JENJANG = 1"));
$smarty->assign('data_semester', $rm->get_semester());
$smarty->assign('data_semester_aktif', $rm->get_semester_aktif());
$periode = $db->QueryToArray("SELECT * FROM TARIF_WISUDA ORDER BY NM_TARIF_WISUDA ASC");
$smarty->assign('periode', $periode);


if (isset($_POST['tampil'])) {
    $smarty->assign('fakultas', post('fakultas'));
	$smarty->assign('jenjang', post('jenjang'));
    $smarty->assign('program_studi', post('program_studi'));
	$smarty->assign('data_program_studi', $rm->get_program_studi(post('fakultas')));
    $smarty->assign('tampil', post('tampil'));
	
	if($_POST['periode'] == ''){
		$where = "";
	}else{
		$where = "AND ID_TARIF_WISUDA = '$_POST[periode]'";
	}

$mhs = $db->QueryToArray("SELECT NIM_MHS, NM_PENGGUNA FROM MAHASISWA
							JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
							JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
							JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA
							WHERE MAHASISWA.ID_PROGRAM_STUDI = '$_POST[program_studi]' $where
							ORDER BY NIM_MHS ASC");	
$smarty->assign('mhs', $mhs);

}


if(isset($_POST['generate'])){
	
	$smarty->assign('fakultas', post('fakultas'));
	$smarty->assign('jenjang', post('jenjang'));
    $smarty->assign('program_studi', post('program_studi'));
	$smarty->assign('get_semester', post('semester'));
	$smarty->assign('get_prodi', post('program_studi2'));
	$mhs_baru = array();
	$no = $_POST['no'];
	for($i=2;$i<=$no;$i++){
		$cek = $_POST['cek'.$i];
		$nim_lama = $_POST['nim_lama'.$i];
		if($cek == 1){
		

		$nim_baru = $db->QueryToArray("select t.id_program_studi, prodi, nim_terakhir, trim(to_char(nim_terakhir+1,'099999999999')) nim_baru 
			from (
				select ps.id_program_studi, substr(nm_jenjang,1,2)||' '||ps.nm_program_studi prodi,
					(select max(nim_mhs) from mahasiswa m 
					where m.id_program_studi = ps.id_program_studi 
					and m.thn_angkatan_mhs in (select thn_akademik_semester from semester where id_semester = '$_POST[semester]')) nim_terakhir
				from program_studi ps
				join jenjang j on j.id_jenjang = ps.id_jenjang
			) t
			where t.id_program_studi = '$_POST[program_studi2]'");	


		$semester = $db->QueryToArray("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '$_POST[semester]'");	
		$nim_baru_seru = $nim_baru[0]['NIM_BARU'];

		//jika belum ada nim pertama
		if($nim_baru[0]['NIM_TERAKHIR'] == NULL or $nim_baru[0]['NIM_TERAKHIR'] == ''){
			
			$db->Query("SELECT KODE_PROGRAM_STUDI, NIM_JENJANG FROM PROGRAM_STUDI 
						JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
						WHERE ID_PROGRAM_STUDI = '$_POST[program_studi2]'");
            $row = $db->FetchAssoc();
			
			$id_fakultas = $_POST['fakultas2'];
			$thn_nim = substr($semester[0]['THN_AKADEMIK_SEMESTER'], -2);
			$prodi_nim = $row['KODE_PROGRAM_STUDI'];
			
			if($_POST['jenjang2'] == 1 or $_POST['jenjang2'] == 5 or $_POST['jenjang2'] == 9){
				$seri = '001';
			}else{
				$seri = '01';
			}
			
			if(strlen($id_fakultas) == 1){
					$id_fakultas = "0".$id_fakultas;
			}
			
					if($semester[0]['NM_SEMESTER'] == 'Genap'){
						$semester_nim = 2;
						$nama_semester = 'Genap';
					}else{
						$semester_nim = 1;
						$nama_semester = 'Ganjil';
					}
													
			
					$jenjang_nim = $row['NIM_JENJANG'];
					
					//JALUR
						//MANDIRI, S2, S3, PROFESI, SPESIALIS
						$jalur_nim = 3;	

			
			if($prodi_nim == '' or $prodi_nim == NULL){
				$nim_baru_seru = '';
				die('Kode Prodi Untuk Generate NIM Masih Kosong.');
			}else{
				$nim_baru_seru = $id_fakultas . $thn_nim . $semester_nim . $prodi_nim . $jenjang_nim . $jalur_nim . $seri;
			}
		}

			
			$mhs = $db->QueryToArray("SELECT * FROM MAHASISWA 
									LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
									JOIN JALUR_MAHASISWA ON JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS
									WHERE MAHASISWA.NIM_MHS = '$nim_lama'");	
			
			
			$db->beginTransaction();
			
			//INSERT PENGGUNA
			$pass = $user->Encrypt($nim_baru_seru);
			$nama =  str_replace("'", "''", $mhs[0]['NM_PENGGUNA']);
			// menyiapkan data password utk disimpan ke db
				$password_hash = sha1($nim_baru_seru);
				$password_encrypted = $user->PublicKeyEncrypt($nim_baru_seru);
				$last_time_password = date('d-M-Y');
				

			$hasil1 = $db->Query("INSERT INTO PENGGUNA (USERNAME, NM_PENGGUNA, TGL_LAHIR_PENGGUNA, KELAMIN_PENGGUNA, 
											ID_AGAMA, TEMPAT_LAHIR, ID_ROLE, JOIN_TABLE, SE1, GELAR_DEPAN, GELAR_BELAKANG,
											password_hash, password_encrypted, last_time_password, password_hash_temp, password_hash_temp_expired, password_must_change) 
						VALUES ('".$nim_baru_seru."', '$nama', '".$mhs[0]['TGL_LAHIR_PENGGUNA']."', 
						'".$mhs[0]['KELAMIN_PENGGUNA']."', '".$mhs[0]['ID_AGAMA']."', '".$mhs[0]['TEMPAT_LAHIR']."', 
						'3', '".$mhs[0][JOIN_TABLE]."', '$pass', '$_POST[gelar_depan]', '$_POST[gelar_belakang]',
						'{$password_hash}', '{$password_encrypted}', '{$last_time_password}', null, null, 1)") ;
			
			if (!$hasil1)
            {
				$db->Rollback();
				die('gagal 1');
			}
			$pengguna = $db->QueryToArray("SELECT * FROM PENGGUNA WHERE USERNAME = '".$nim_baru_seru."'");
			$tgl = date('Y-m-d');
			$nim_bank = substr($nim_baru_seru, 0, 12);
			
			//INSERT MAHASISWA
			$db->Query("INSERT INTO MAHASISWA (ID_PENGGUNA, ID_PROGRAM_STUDI, NIM_MHS, THN_ANGKATAN_MHS, 
						ALAMAT_MHS, MOBILE_MHS, NM_AYAH_MHS, PENDIDIKAN_AYAH_MHS, 
						PEKERJAAN_AYAH_MHS, ALAMAT_AYAH_MHS, NM_IBU_MHS, PENDIDIKAN_IBU_MHS, 
						PEKERJAAN_IBU_MHS, ALAMAT_IBU_MHS, PENGHASILAN_ORTU_MHS, NEM_PELAJARAN_MHS, 
						LAHIR_KOTA_MHS, LAHIR_PROP_MHS, TGL_TERDAFTAR_MHS, STATUS_CEKAL, NIM_BANK, STATUS_AKADEMIK_MHS, 
						ASAL_KOTA_MHS, ASAL_PROV_MHS, STATUS, ALAMAT_MHS_KOTA, 
						ALAMAT_AYAH_MHS_KOTA, ALAMAT_AYAH_MHS_PROV, ALAMAT_IBU_MHS_KOTA, ALAMAT_IBU_MHS_PROV, 
						ALAMAT_ASAL_MHS, ALAMAT_ASAL_MHS_KOTA, ALAMAT_ASAL_MHS_PROV, ID_SEKOLAH_ASAL_MHS, ID_SEMESTER_MASUK, NIM_LAMA, ID_KEBANGSAAN)
				VALUES ('".$pengguna[0]['ID_PENGGUNA']."', '$_POST[program_studi2]', '".$nim_baru_seru."', 
				'".$semester[0]['THN_AKADEMIK_SEMESTER']."',
				'".$mhs[0]['ALAMAT_MHS']."', '".$mhs[0]['MOBILE_MHS']."', '".$mhs[0]['NM_AYAH_MHS']."', '".$mhs[0]['PENDIDIKAN_AYAH_MHS']."', 
				'".$mhs[0]['PEKERJAAN_AYAH_MHS']."', '".$mhs[0]['ALAMAT_AYAH_MHS']."', '".$mhs[0]['NM_IBU_MHS']."', 
				'".$mhs[0]['PENDIDIKAN_IBU_MHS']."',
				'".$mhs[0]['PEKERJAAN_IBU_MHS']."', '".$mhs[0]['ALAMAT_IBU_MHS']."', '".$mhs[0]['PENGHASILAN_ORTU_MHS']."',
				'".$mhs[0]['NEM_PELAJARAN_MHS']."',
				'".$mhs[0]['LAHIR_KOTA_MHS']."', '".$mhs[0]['LAHIR_PROP_MHS']."', to_date('$tgl', 'YYYY-MM-DD'), '0', '$nim_bank', '1', 
				'".$mhs[0]['ASAL_KOTA_MHS']."', '".$mhs[0]['ASAL_PROV_MHS']."', 'R', '".$mhs[0]['ALAMAT_MHS_KOTA']."',
				'".$mhs[0]['ALAMAT_AYAH_MHS_KOTA']."', 
				'".$mhs[0]['ALAMAT_AYAH_MHS_PROV']."', '".$mhs[0]['ALAMAT_IBU_MHS_KOTA']."', '".$mhs[0]['ALAMAT_IBU_MHS_PROV']."',
				'".$mhs[0]['ALAMAT_ASAL_MHS']."', 
				'".$mhs[0]['ALAMAT_ASAL_MHS_KOTA']."', '".$mhs[0]['ALAMAT_ASAL_MHS_PROV']."', '".$mhs[0]['ID_SEKOLAH_ASAL']."',
				'$_POST[semester]', '$nim_lama', 114)") or die('gagal 2');
		
			
			$mhs_baru = $db->QueryToArray("SELECT MAHASISWA.ID_MHS
											FROM MAHASISWA 
											WHERE MAHASISWA.NIM_MHS = '".$nim_baru_seru."'");
			
			//foto profesi
			$source = "http://10.0.110.13/foto/fotoijasah/".$nim_lama.".jpg";
			$target = "/var/www/html/foto_mhs/".$nim_baru_seru.".JPG";
			copy($source, $target);
			
			//INSERT ADMISI
			
			$db->Query("INSERT INTO ADMISI (ID_MHS, STATUS_AKD_MHS, TGL_USULAN, TGL_APV, ID_SEMESTER, STATUS_APV, ID_PENGGUNA)
			VALUES ('".$mhs_baru[0]['ID_MHS']."', '1', to_date('$tgl','YYYY-MM-DD'), to_date('$tgl','YYYY-MM-DD'), '$_POST[semester]', '1', $id_pengguna)")  or die('gagal 3');
			
			//INSERT JALUR MAHASISWA
			$db->Query("INSERT INTO JALUR_MAHASISWA (ID_MHS, ID_PROGRAM_STUDI, ID_JALUR, ID_SEMESTER, NIM_MHS, ID_JALUR_AKTIF)
			VALUES ('".$mhs_baru[0]['ID_MHS']."', '$_POST[program_studi2]', '".$mhs[0]['ID_JALUR']."', '$_POST[semester]', '".$nim_baru_seru."', '1')")  or die('gagal 4');
					
			//INSERT PEMBAYARAN
			$set_biaya = $db->QueryToArray("SELECT PEMBAYARAN.*, BIAYA_KULIAH_MHS.* FROM PEMBAYARAN
											JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
											JOIN SEMESTER ON SEMESTER.ID_SEMESTER = PEMBAYARAN.ID_SEMESTER
											JOIN BIAYA_KULIAH_MHS ON BIAYA_KULIAH_MHS.ID_MHS = MAHASISWA.ID_MHS
											WHERE NIM_MHS = '$nim_lama' AND SEMESTER.ID_SEMESTER = '$_POST[semester]'
											AND PEMBAYARAN.ID_STATUS_PEMBAYARAN = 1");

			if($set_biaya[0]['ID_PEMBAYARAN'] <> '' or $set_biaya[0]['ID_PEMBAYARAN'] <> NULL){
			
				foreach($set_biaya as $biaya){
					$db->Query("INSERT INTO PEMBAYARAN (ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, ID_BANK, 
								ID_BANK_VIA, BESAR_BIAYA, DENDA_BIAYA, TGL_BAYAR, IS_TAGIH, NO_TRANSAKSI, 
								KETERANGAN, IS_TARIK, ID_SEMESTER_BAYAR, ID_STATUS_PEMBAYARAN, FLAG_PEMINDAHAN)
							VALUES ('".$mhs_baru[0]['ID_MHS']."', '$biaya[ID_SEMESTER]', '$biaya[ID_DETAIL_BIAYA]', '$biaya[ID_BANK]', 
									'$biaya[ID_BANK_VIA]', '$biaya[BESAR_BIAYA]', '$biaya[DENDA_BIAYA]', '$biaya[TGL_BAYAR]', '$biaya[IS_TAGIH]', 
									'$biaya[NO_TRANSAKSI]', 
									'$biaya[KETERANGAN]', '$biaya[IS_TARIK]', '$biaya[ID_SEMESTER_BAYAR]', '$biaya[ID_STATUS_PEMBAYARAN]', '1')") 
							 or die('gagal 6');
				}	

			}else{
			
				$set_biaya = $db->QueryToArray("SELECT DETAIL_BIAYA.* FROM DETAIL_BIAYA
												JOIN BIAYA_KULIAH_MHS ON BIAYA_KULIAH_MHS.ID_BIAYA_KULIAH = DETAIL_BIAYA.ID_BIAYA_KULIAH
												JOIN BIAYA ON BIAYA.ID_BIAYA = DETAIL_BIAYA.ID_BIAYA
												JOIN MAHASISWA ON MAHASISWA.ID_MHS = BIAYA_KULIAH_MHS.ID_MHS
												WHERE NIM_MHS = '$nim_lama' AND PENDAFTARAN_BIAYA = 0");
											
				foreach($set_biaya as $biaya){
					$db->Query("INSERT INTO PEMBAYARAN (ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, BESAR_BIAYA, DENDA_BIAYA, 
								IS_TAGIH, IS_TARIK, ID_STATUS_PEMBAYARAN)
							VALUES ('".$mhs_baru[0]['ID_MHS']."', '$_POST[semester]', '$biaya[ID_DETAIL_BIAYA]', '$biaya[BESAR_BIAYA]', '0', 
							'Y', '0', '2')") 
							 or die('gagal 16');
				}	
			}
			
			$db->Query("INSERT INTO BIAYA_KULIAH_MHS (ID_BIAYA_KULIAH, ID_MHS) 
						VALUES ('".$set_biaya[0]['ID_BIAYA_KULIAH']."', '".$mhs_baru[0]['ID_MHS']."')") 
						or die('gagal 8');
					
			$db->Query("UPDATE MAHASISWA 
						SET  
						ID_KELOMPOK_BIAYA = (SELECT ID_KELOMPOK_BIAYA FROM BIAYA_KULIAH WHERE ID_BIAYA_KULIAH = '".$set_biaya[0]['ID_BIAYA_KULIAH']."') 
						WHERE ID_MHS = '".$mhs_baru[0]['ID_MHS']."'") or die('gagal 7');
			
		
			
			//INSERT FINGERPRINT
			
			$finger = $db->Query("INSERT INTO FINGERPRINT_MAHASISWA (FINGER_DATA, NIM_MHS, TGL_LAHIR, HURUF_DEPAN)
						SELECT FINGER_DATA, '".$nim_baru_seru."' AS NIM_BARU, TGL_LAHIR, HURUF_DEPAN 
						FROM FINGERPRINT_MAHASISWA WHERE NIM_MHS = '$nim_lama'") or die('gagal 5');
			
			//if($finger){
				$db->commit();
			//}else{
				//$db->rollBack();
			//}
			

			$mhs_baru_tampil[] = array(
                'NIM_MHS_BARU'       => $nim_baru_seru,
                'NIM_MHS_LAMA'       => $nim_lama,
                'NM_PENGGUNA'        => $nama
            );
			
		}

	}

			$smarty->assign('mhs_baru', $mhs_baru_tampil);
	
}

$smarty->display("pendaftaran/nim_profesi/pendaftaran-nim-profesi.tpl");
?>
