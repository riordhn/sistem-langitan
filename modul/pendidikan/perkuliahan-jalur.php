<?php
include('config.php');

$mode = get('mode', 'view');
$id_jalur = get('id_jalur', 0);


if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
		$db->Query("UPDATE JALUR SET  NIM_JALUR = '$_POST[kode_nim]', KODE_JALUR = '$_POST[kode_jalur]'
					WHERE ID_PERGURUAN_TINGGI = {$id_pt_user} AND ID_JALUR = '$_POST[id_jalur]'");
    }

    if (post('mode') == 'add')
    {
		$db->Query("INSERT INTO JALUR (NM_JALUR, NIM_JALUR, KODE_JALUR, ID_PERGURUAN_TINGGI, IS_AKTIF) VALUES ('$_POST[nm_jalur]','$_POST[kode_nim]','$_POST[kode_jalur]','{$id_pt_user}','1')");
    }

    if (post('mode') == 'delete')
    {
		$db->Query("UPDATE JALUR SET IS_AKTIF = '0'
					WHERE ID_PERGURUAN_TINGGI = {$id_pt_user} AND ID_JALUR = '$_POST[id_jalur]'");
    }

    if (post('mode') == 'aktifkan')
    {
        $db->Query("UPDATE JALUR SET IS_AKTIF = '1'
                    WHERE ID_PERGURUAN_TINGGI = {$id_pt_user} AND ID_JALUR = '$_POST[id_jalur]'");
    }
}

if ($mode == 'view')
{
    $jenjang_set = $db->QueryToArray("SELECT j.*, kj.KETERANGAN, (SELECT COUNT(*) FROM JALUR_MAHASISWA jm WHERE jm.ID_JALUR = j.ID_JALUR) AS JML_MHS
                                        FROM JALUR j
                                        JOIN KODE_JALUR kj ON kj.KODE_JALUR = j.KODE_JALUR 
                                        WHERE ID_PERGURUAN_TINGGI = {$id_pt_user} ORDER BY IS_AKTIF DESC");
    $smarty->assign('jenjang_set', $jenjang_set);
}
else if ($mode == 'add' or $mode == 'edit' or $mode == 'delete' or $mode == 'aktifkan')
{
    $jalur = $db->QueryToArray("SELECT j.*, kj.KETERANGAN 
                                        FROM JALUR j
                                        JOIN KODE_JALUR kj ON kj.KODE_JALUR = j.KODE_JALUR WHERE ID_PERGURUAN_TINGGI = {$id_pt_user} AND ID_JALUR = '$id_jalur'");
    $smarty->assign('jalur', $jalur);

    $kode_jalur_set = $db->QueryToArray("SELECT * FROM KODE_JALUR");
    $smarty->assign('kode_jalur_set', $kode_jalur_set);
}

$smarty->display("perkuliahan/jalur/{$mode}.tpl");
?>