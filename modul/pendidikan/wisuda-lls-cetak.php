<?php
include('config.php');

require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

$id_periode = $_GET['prd'];
$id_fakultas = $_GET['fak'];
$id_jenjang = $_GET['jnj'];

if($id_jenjang==1){$rw=30;} //S1
else if($id_jenjang==2){$rw=30;} //S2
else if($id_jenjang==3){$rw=5;} //S3
else if($id_jenjang==5){$rw=30;} //D3
else if($id_jenjang==9){$rw=15;} //Profesi
else{$rw=15;} // Spesialis

$rekap = $db->QueryToArray("SELECT (case when pg.gelar_depan is null then '' else pg.gelar_depan || '. ' end)gelar_depan, pg.nm_pengguna, (case when pg.gelar_belakang is null 						    then '' else ', ' || pg.gelar_belakang 	
					    end)gelar_belakang,mh.nim_mhs,
					    (case when pg.kelamin_pengguna = '2' then 'Perempuan' else 'Laki-laki' end)jk,fk.nm_fakultas,
					    ps.nm_program_studi,jj.nm_jenjang,jj.nm_jenjang_ijasah,(case when pw.lahir_ijazah is null then (case when kt.nm_kota is null 						    then 'Tidak Ada Data' else kt.nm_kota end 
					    || ', ' || 
					    case when to_char(pg.tgl_lahir_pengguna,'DD-MM-YYYY') is null then 'Tidak Ada Data' else 
					    to_char(pg.tgl_lahir_pengguna,'DD-MM-YYYY') end) else pw.lahir_ijazah end)ttl,ad.tgl_lulus as tgl_lls,
					    pw.no_ijasah,pw.no_seri_kertas,pw.tgl_posisi_ijasah,pw.judul_ta
					    FROM admisi ad
					    left join mahasiswa mh on mh.id_mhs=ad.id_mhs
					    left join pengguna pg on pg.id_pengguna=mh.id_pengguna
					    left join program_studi ps on mh.id_program_studi=ps.id_program_studi
					    left join fakultas fk on ps.id_fakultas=fk.id_fakultas
					    left join jenjang jj on ps.id_jenjang=jj.id_jenjang
					    left join kota kt on kt.id_kota=mh.lahir_kota_mhs
					    left join pengajuan_wisuda pw on pw.id_mhs=ad.id_mhs
					    where ad.status_akd_mhs=4 
					    AND PS.ID_FAKULTAS = '$id_fakultas'
					    and ad.id_mhs is not null
					    and jj.id_jenjang='$id_jenjang'
					    and to_char(ad.tgl_lulus,'YYYY')='$id_periode'
					    order by pw.no_ijasah, ps.id_program_studi,ps.id_jenjang,to_char(ad.tgl_lulus,'DD-MM-YYYY')")or die("g iso");


$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->setPrintHeader(false);
//$pdf->setFooterData($tc=array(0,64,0), $lc=array(0,64,128));
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('unair');
$pdf->SetTitle('Lulusan Unair');
$pdf->SetSubject('Lulusan Unair');

$pdf->SetAutoPageBreak(TRUE, 10);
//$pdf->SetAutoPageBreak(TRUE,PDF_MARGIN_BOTTOM);

$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('times', '', 9);

// add a page
$pdf->AddPage('L', 'F4');

	$no = 1;$rol=0;
	foreach($rekap as $data){
	$rol++;
	if($no==1){
	if($data['NM_JENJANG']=='S3'){
	$html = '
		<h1 style="text-align:center">DATA IJASAH '.$data['NM_JENJANG_IJASAH'].' ('.$data['NM_JENJANG'].') FAKULTAS '.$data['NM_FAKULTAS'].' TAHUN '.$id_periode.'</h1>		
		<table width="100%" cellpadding="2" border="0.5" style="table-layout:fixed; overflow-x:hidden">
		<tr bgcolor="green" style="color:#fff;">
		<th style="text-align:center" width="3%">NO</th>
		<th style="text-align:center" width="16%">NAMA</th>
		<th style="text-align:center" width="6%">NIM</th>
		<th style="text-align:center" width="18%">TEMPAT dan TANGGAL LAHIR</th>
		<th style="text-align:center" width="16%">PROGRAM STUDI</th>
		<th style="text-align:center" width="6%">TGL LULUS</th>
		<th style="text-align:center" width="10%">NO IJASAH</th>
		<th style="text-align:center" width="4%">NO SERI</th>
		<th style="text-align:center" width="15%">JUDUL DESERTASI</th>
		<th style="text-align:center" width="6%">TANGGAL CETAK IJASAH</th>
		</tr>';		
	}
	else{
	$html = '
		<h1 style="text-align:center">DATA IJASAH '.$data['NM_JENJANG_IJASAH'].' ('.$data['NM_JENJANG'].') FAKULTAS '.$data['NM_FAKULTAS'].' TAHUN '.$id_periode.'</h1>		
		<table width="100%" cellpadding="2" border="0.5" style="table-layout:fixed; overflow-x:hidden">
		<tr bgcolor="green" style="color:#fff;">
		<th style="text-align:center" width="3%">NO</th>
		<th style="text-align:center" width="22%">NAMA</th>
		<th style="text-align:center" width="6%">NIM</th>
		<th style="text-align:center" width="22%">TEMPAT dan TANGGAL LAHIR</th>
		<th style="text-align:center" width="18%">PROGRAM STUDI</th>
		<th style="text-align:center" width="6%">TGL LULUS</th>
		<th style="text-align:center" width="11%">NO IJASAH</th>
		<th style="text-align:center" width="4%">NO SERI</th>
		<th style="text-align:center" width="6%">TANGGAL CETAK IJASAH</th>
		</tr>';
	}	
	}	

	$html .= '<tr>
		<td style="text-align:center">'.$no++.'</td>
		<td>'.$data['GELAR_DEPAN'].''.ucwords(strtolower($data['NM_PENGGUNA'])).''.$data['GELAR_BELAKANG'].'</td>
		<td>'.$data['NIM_MHS'].'</td>
		<td>'.$data['TTL'].'</td>
		<td>'.$data['NM_PROGRAM_STUDI'].'</td>
		<td>'.$data['TGL_LLS'].'</td>
		<td>'.$data['NO_IJASAH'].'</td>
		<td>'.$data['NO_SERI_KERTAS'].'</td>';
	if($data['NM_JENJANG']=='S3'){$html.='<td style:"font-size:8px">'.$data['JUDUL_TA'].'</td>';}
	$html .='<td>'.$data['TGL_POSISI_IJASAH'].'</td>
		</tr>';

	if($rol%$rw==0){
	$rol=0;
	$html .= '</table>';
	$pdf->writeHTML($html, true, false, true, false, '');
	$html='';
	$pdf->AddPage('L', 'F4');
	if($data['NM_JENJANG']=='S3'){
	$html = '
		<h1 style="text-align:center">DATA IJASAH '.$data['NM_JENJANG_IJASAH'].' ('.$data['NM_JENJANG'].') FAKULTAS '.$data['NM_FAKULTAS'].' TAHUN '.$id_periode.'</h1>		
		<table width="100%" cellpadding="2" border="0.5" style="table-layout:fixed; overflow-x:hidden">
		<tr bgcolor="green" style="color:#fff;">
		<th style="text-align:center" width="3%">NO</th>
		<th style="text-align:center" width="16%">NAMA</th>
		<th style="text-align:center" width="6%">NIM</th>
		<th style="text-align:center" width="18%">TEMPAT dan TANGGAL LAHIR</th>
		<th style="text-align:center" width="16%">PROGRAM STUDI</th>
		<th style="text-align:center" width="6%">TGL LULUS</th>
		<th style="text-align:center" width="10%">NO IJASAH</th>
		<th style="text-align:center" width="4%">NO SERI</th>
		<th style="text-align:center" width="15%">JUDUL DESERTASI</th>
		<th style="text-align:center" width="6%">TANGGAL CETAK IJASAH</th>
		</tr>';		
	}
	else{
	$html = '
		<h1 style="text-align:center">DATA IJASAH '.$data['NM_JENJANG_IJASAH'].' ('.$data['NM_JENJANG'].') FAKULTAS '.$data['NM_FAKULTAS'].' TAHUN '.$id_periode.'</h1>		
		<table width="100%" cellpadding="2" border="0.5" style="table-layout:fixed; overflow-x:hidden">
		<tr bgcolor="green" style="color:#fff;">
		<th style="text-align:center" width="3%">NO</th>
		<th style="text-align:center" width="22%">NAMA</th>
		<th style="text-align:center" width="6%">NIM</th>
		<th style="text-align:center" width="22%">TEMPAT dan TANGGAL LAHIR</th>
		<th style="text-align:center" width="18%">PROGRAM STUDI</th>
		<th style="text-align:center" width="6%">TGL LULUS</th>
		<th style="text-align:center" width="11%">NO IJASAH</th>
		<th style="text-align:center" width="4%">NO SERI</th>
		<th style="text-align:center" width="6%">TANGGAL CETAK IJASAH</th>
		</tr>';
	}	
	}
	}
$html .= '</table>';

$html .= '';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('mhs_lls.pdf', 'I');
?>
