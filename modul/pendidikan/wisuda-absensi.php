<?php
include('config.php');
include "class/epsbed.class.php";
$aucc = new epsbed($db);

$smarty->assign('data_fakultas', $aucc->fakultas());
$smarty->assign('data_jenjang', $aucc->jenjang_wisuda());
$periode = $db->QueryToArray("SELECT * FROM TARIF_WISUDA WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY ID_TARIF_WISUDA DESC");
$smarty->assign('periode', $periode);


if(isset($_POST['no'])){

	$no = $_POST['no'];
	for($i=2;$i<=$no;$i++){
		$cek = $_POST['cek'.$i];
		$id = $_POST['id'.$i];
		//echo $cek;
		if($cek==1){
			if($_POST['tgl_wisuda'] != ''){
			$db->Query("UPDATE PEMBAYARAN_WISUDA SET TGL_WISUDA =  TO_DATE('$_POST[tgl_wisuda]', 'DD-MM-YYYY') WHERE ID_PEMBAYARAN_WISUDA = '$id'");
			}
		}else{
			$db->Query("UPDATE PEMBAYARAN_WISUDA SET TGL_WISUDA =  '' WHERE ID_PEMBAYARAN_WISUDA = '$id'");
		}
	}

}

if(isset($_POST['periode'])){
				
		
		
		if($_POST['fakultas'] <> '' and $_POST['program_studi'] <> '' and $_POST['jenjang'] <> ''){
			$where  = " AND PROGRAM_STUDI.ID_FAKULTAS = '$_POST[fakultas]' AND PROGRAM_STUDI.ID_PROGRAM_STUDI = '$_POST[program_studi]' 
						AND PROGRAM_STUDI.ID_JENJANG = '$_POST[jenjang]'";
		}elseif($_POST['fakultas'] <> '' and $_POST['program_studi'] <> ''){
			$where  = " AND PROGRAM_STUDI.ID_FAKULTAS = '$_POST[fakultas]' AND PROGRAM_STUDI.ID_PROGRAM_STUDI = '$_POST[program_studi]'";
		}elseif($_POST['fakultas'] <> '' and $_POST['jenjang'] <> ''){
			$where  = " AND PROGRAM_STUDI.ID_FAKULTAS = '$_POST[fakultas]' AND PROGRAM_STUDI.ID_JENJANG = '$_POST[jenjang]'";
		}elseif($_POST['program_studi'] <> '' and $_POST['jenjang'] <> ''){
			$where  = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI = '$_POST[program_studi]' AND PROGRAM_STUDI.ID_JENJANG = '$_POST[jenjang]'";
		}elseif($_POST['fakultas'] <> ''){
			$where  = " AND PROGRAM_STUDI.ID_FAKULTAS = '$_POST[fakultas]'";
		}elseif($_POST['program_studi'] <> ''){
			$where  = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI = '$_POST[program_studi]'";
		}elseif($_POST['jenjang'] <> ''){
			$where  = " AND PROGRAM_STUDI.ID_JENJANG = '$_POST[jenjang]'";
		}else{
			$where  = " ";
		}


		if($_POST['tgl_wisuda'] != ''){
			$where .= "AND (TGL_WISUDA IS NULL OR TGL_WISUDA = TO_DATE('$_POST[tgl_wisuda]', 'DD-MM-YYYY'))";
			$smarty->assign('tgl_wisuda', date_format(date_create($_POST['tgl_wisuda']), 'd-m-Y'));
		}else{
			$where .= "";
			$smarty->assign('tgl_wisuda', '');
		}

		$absensi = $db->QueryToArray("SELECT ID_PEMBAYARAN_WISUDA, NIM_MHS, NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, 
										NM_FAKULTAS, KETERANGAN_ABSENSI, ABSENSI_WISUDA, TO_CHAR(TGL_WISUDA, 'DD-MM-YYYY') AS TGL_WISUDA
										FROM PEMBAYARAN_WISUDA
										JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS
										JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PEMBAYARAN_WISUDA.ID_PERIODE_WISUDA
										JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								 		JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								 		JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
								 		JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
										WHERE PERIODE_WISUDA.ID_TARIF_WISUDA = '$_POST[periode]' AND ABSENSI_WISUDA  = 1 AND
										(PROGRAM_STUDI.ID_JENJANG <> 9 OR PROGRAM_STUDI.ID_JENJANG <> 10) 
										$where
										ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI, MAHASISWA.NIM_MHS");
	
	$smarty->assign('absensi', $absensi);
	
	
}



$smarty->display("wisuda/wisuda-absensi.tpl");
?>
