<?php
include 'config.php';
include('class/date.class.php');
include_once '../../tcpdf/config/lang/ind.php';
include_once '../../tcpdf/tcpdf.php';

$tgl = tgl_indo(date('Y-m-d'));

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$lapo_registrasi = $db->QueryToArray("SELECT ID_EVAL_ASPEK, EVALUASI_ASPEK,
SUM(DECODE(NILAI_EVAL, 0, JML, NULL)) A0,
SUM(DECODE(NILAI_EVAL, 1, JML, NULL)) A1,
SUM(DECODE(NILAI_EVAL, 2, JML, NULL)) A2,
SUM(DECODE(NILAI_EVAL, 3, JML, NULL)) A3,
SUM(DECODE(NILAI_EVAL, 4, JML, NULL)) A4
FROM (
SELECT AUCC.EVALUASI_ASPEK.ID_EVAL_ASPEK , AUCC.EVALUASI_ASPEK.EVALUASI_ASPEK, AUCC.EVALUASI_HASIL.NILAI_EVAL, COUNT(*) JML
FROM
AUCC.EVALUASI_INSTRUMEN
JOIN AUCC.EVALUASI_KELOMPOK_ASPEK ON AUCC.EVALUASI_KELOMPOK_ASPEK.ID_EVAL_INSTRUMEN = AUCC.EVALUASI_INSTRUMEN.ID_EVAL_INSTRUMEN
JOIN AUCC.EVALUASI_ASPEK ON AUCC.EVALUASI_ASPEK.ID_EVAL_KELOMPOK_ASPEK = AUCC.EVALUASI_KELOMPOK_ASPEK.ID_EVAL_KELOMPOK_ASPEK
JOIN AUCC.EVALUASI_HASIL ON AUCC.EVALUASI_HASIL.ID_EVAL_ASPEK = AUCC.EVALUASI_ASPEK.ID_EVAL_ASPEK
WHERE AUCC.EVALUASI_INSTRUMEN.ID_EVAL_INSTRUMEN = '9' and AUCC.EVALUASI_ASPEK.TIPE_ASPEK= 1 and AUCC.EVALUASI_HASIL.NILAI_EVAL is NOT NULL
GROUP BY  AUCC.EVALUASI_ASPEK.ID_EVAL_ASPEK , AUCC.EVALUASI_ASPEK.EVALUASI_ASPEK, AUCC.EVALUASI_HASIL.NILAI_EVAL)
GROUP BY ID_EVAL_ASPEK, EVALUASI_ASPEK
ORDER BY ID_EVAL_ASPEK ASC");

	
	
$pdf->SetCreator('Cybercampus Universitas Airlangga');
$pdf->SetAuthor('Universitas Airlangga');


$pdf->SetMargins(10, 3, 10);

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->SetAutoPageBreak(TRUE, 0);

$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

$pdf->AddPage('L', 'A4');

$pdf->setPageMark();

$html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .header { font-size: 18pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 14pt; font-family: serif; margin-top: 0px ;text-align:center; }
    th { 
        text-align: center;
        color: #ffffff;
        background-color: #006600;
        padding:4px;
		font-size: 10pt;
    }
    td { font-size: 9pt; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center" class="center">
            <span class="header">LAPORAN KUISIONER WISUDA</span>
        </td>
    </tr>
</table>
<hr/>
<p><p/>
<table width="100%" cellpadding="3" border="0.5">
        <tr>
            <th width="4%">NO</th>
            <th width="45%">NAMA ASPEK</th>
            <th width="10%">Sangat Tidak Setuju</th>
            <th width="10%">Tidak Setuju</th>
			<th width="10%">Setuju</th>
            <th width="10%">Sangat Setuju</th>
			<th width="10%">Tidak Dapat Berkomentar</th>
        </tr>
    {data_mahasiswa}
</table>

<table border="0">
<tr><td></td></tr>
</table>

EOF;

$index = 1;
foreach ($lapo_registrasi as $data) {
    $data_mahasiswa .=
            "<tr>
                <td>{$index}</td>
                <td>{$data['EVALUASI_ASPEK']}</td>
                <td>{$data['A1']}</td>
				<td>{$data['A2']}</td>
				<td>{$data['A3']}</td>
				<td>{$data['A4']}</td>
				<td>{$data['A0']}</td>               
            </tr>";
    $index++;
}

$html = str_replace('{data_mahasiswa}', $data_mahasiswa, $html);

$pdf->writeHTML($html);

$pdf->Output();
?>
