<?php
include('config.php');
include 'class/report_mahasiswa.class.php';
$rm = new report_mahasiswa($db);
$id_pengguna= $user->ID_PENGGUNA; 

if(isset($_POST['nim']) or isset($_POST['generate'])){
		$smarty->assign('data_fakultas', $rm->get_fakultas());
		$smarty->assign('data_jalur', $rm->get_jalur());
		$smarty->assign('data_jenjang', $rm->get_jenjang());
		$smarty->assign('data_semester', $rm->get_semester());
		$smarty->assign('data_semester_aktif', $rm->get_semester_aktif());
		
		if(isset($_POST['generate'])){
	
			$smarty->assign('data_program_studi', $rm->get_program_studi_by_fakultas_jenjang($_POST['fakultas'], $_POST['jenjang']));
			$smarty->assign('fakultas', $_POST['fakultas']);
			$smarty->assign('jenjang', $_POST['jenjang']);
			$smarty->assign('program_studi', $_POST['program_studi']);
			$smarty->assign('semester', $_POST['semester']);
			$smarty->assign('nama', $_POST['nama']);
			$smarty->assign('generate', $_POST['generate']);

			
			$db->Query("SELECT NIM_TERAKHIR,  trim(to_char(NIM_TERAKHIR+1, 
					CASE WHEN LENGTH(NIM_TERAKHIR) = 12 THEN '099999999999' 
					WHEN LENGTH(NIM_TERAKHIR) = 11 THEN '09999999999' 
					WHEN LENGTH(NIM_TERAKHIR) = 10 THEN '0999999999' 
					WHEN LENGTH(NIM_TERAKHIR) = 9 THEN '099999999' END)) NIM_BARU
					FROM (
					SELECT MAX(NIM_MHS) AS NIM_TERAKHIR FROM MAHASISWA WHERE ID_PROGRAM_STUDI = '$_POST[program_studi]' 
					AND THN_ANGKATAN_MHS IN (SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE ID_SEMESTER = '$_POST[semester]')
					)");	
			$nim_baru = $db->FetchAssoc();			
			
			$mhs = $db->QueryToArray("SELECT * FROM MAHASISWA 
									LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
									WHERE NIM_MHS = '$_POST[nim_lama]'");	
			
		
		
		$nim_baru_seru = $nim_baru['NIM_BARU'];
		$tgl = date('Y-m-d');
			
			//insert foto
			$source = "/var/www/html/foto_mhs/".$_POST['nim_lama'].".JPG";
			$target = "/var/www/html/foto_mhs/".$nim_baru_seru.".JPG";
			copy($source, $target);
			
			$db->beginTransaction();
			
			
			//INSERT ADMISI		
				
			$db->Query("INSERT INTO ADMISI (ID_MHS, STATUS_AKD_MHS, TGL_USULAN, TGL_APV, ID_SEMESTER, STATUS_APV, ID_PENGGUNA, ALASAN)
			VALUES ('".$mhs[0]['ID_MHS']."', ".$mhs[0]['STATUS_AKADEMIK_MHS'].", to_date('$tgl','YYYY-MM-DD'), to_date('$tgl','YYYY-MM-DD'), '$_POST[semester]', '1', $id_pengguna, '$_POST[keterangan]')")  
			or die('gagal 3');
			
			$db->Query("UPDATE PENGGUNA SET USERNAME = '$nim_baru_seru' WHERE ID_PENGGUNA = '".$mhs[0]['ID_PENGGUNA']."'");
			$db->Query("UPDATE MAHASISWA SET NIM_MHS = '$nim_baru_seru', NIM_BANK = '$nim_baru_seru', NIM_LAMA = '".$_POST['nim_lama']."' 
						WHERE ID_MHS = '".$mhs[0]['ID_MHS']."'");
			$db->Query("UPDATE FINGERPRINT_MAHASISWA SET NIM_MHS = '$nim_baru_seru' WHERE NIM_MHS = '".$_POST['nim_lama']."'");
			$db->commit();
						
		$smarty->assign('nim_baru', $nim_baru_seru);
		$smarty->assign('mhs', $mhs);
		
		}else{
			$mhs = $db->QueryToArray("SELECT * FROM MAHASISWA 
									LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
									WHERE NIM_MHS = '$_POST[nim]'");	
			$smarty->assign('mhs', $mhs);
		}

}


$smarty->display("pendaftaran/nim_ganti/pendaftaran-nim-ganti.tpl");
?>
