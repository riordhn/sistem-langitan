<?php
include('config.php');
$periode = $db->QueryToArray("SELECT DISTINCT(TO_CHAR(AD.TGL_SK,'yyyy')) as thn 
			      FROM ADMISI ad 
			      WHERE AD.NO_SK is NOT NULL 
			      AND AD.NO_SK <> '-' 
		   	      AND ad.tgl_sk is not null
			      ORDER by TO_CHAR(AD.TGL_SK,'yyyy') DESC
			     ");
$smarty->assign('periode', $periode);
$jenis = $db->QueryToArray("SELECT DISTINCT(AD.status_akd_mhs)as id_stat, STP.NM_STATUS_PENGGUNA 
			    FROM ADMISI ad LEFT JOIN STATUS_PENGGUNA stp on STP.ID_STATUS_PENGGUNA=AD.STATUS_AKD_MHS
			    WHERE STP.ID_STATUS_PENGGUNA in ('3','5','6','20')
			    ORDER BY AD.STATUS_AKD_MHS
			   ");
$smarty->assign('jenis', $jenis);





if(isset($_POST['periode'])){
		if($_POST['jenis'] <> ''){
			$where  = "AND AD.STATUS_AKD_MHS='$_POST[jenis]'";
		}else{
			$where  = "AND AD.STATUS_AKD_MHS in ('3','5','6','20')";
		}
		
		$rekap = $db->QueryToArray("SELECT TGL_SK,NO_SK,nm_status_pengguna as ttg, 
					    	sum(decode(id_jenjang,5,JML,NULL)) D3, 
						sum(decode(id_jenjang,1,JML,NULL)) S1, 
						sum(decode(id_jenjang,2,JML,NULL)) S2, 
						sum(decode(id_jenjang,3,JML,NULL)) S3,
						SUM(DECODE(id_jenjang,9,JML,NULL)) PROFESI,
						SUM(DECODE(id_jenjang,10,JML,NULL)) SPESIALIS
						from (
							SELECT AD.NO_SK,to_char(AD.TGL_SK,'dd-mm-yyyy') as tgl_sk,SPG.NM_STATUS_PENGGUNA,jj.id_jenjang,jj.nm_jenjang, 
								count(AD.ID_MHS) as JML
					    		FROM ADMISI ad
				   	  		LEFT JOIN STATUS_PENGGUNA spg on SPG.ID_STATUS_PENGGUNA=AD.STATUS_AKD_MHS
							LEFT JOIN MAHASISWA ms on ms.id_mhs=AD.id_mhs
							LEFT JOIN PROGRAM_STUDI ps ON ps.id_program_studi=ms.id_program_studi
							LEFT JOIN JENJANG jj on jj.id_jenjang=ps.id_jenjang
					    		WHERE ad.NO_SK IS NOT NULL 
					    		AND AD.NO_SK <> '-' 
					    		AND TO_CHAR(AD.TGL_SK,'yyyy')='$_POST[periode]'
					    		and substr(AD.NO_SK,-7,2)='KR'
							$where
							GROUP BY AD.NO_SK,to_char(AD.TGL_SK,'dd-mm-yyyy'),SPG.NM_STATUS_PENGGUNA,jj.id_jenjang,jj.nm_jenjang
						)
					    GROUP BY TGL_SK,NO_SK,nm_status_pengguna
					    ORDER BY tgl_sk,no_sk
					  ");

$smarty->assign('rekap', $rekap);	
$smarty->assign('id_periode', $_POST['periode']);
$smarty->assign('id_jenis', $_POST['jenis']);
}



$smarty->display("evaluasi/rksk/evaluasi-rksk.tpl");

/*
SELECT TGL_SK,NO_SK,nm_status_pengguna, 
				sum(decode(id_jenjang,5,JML,NULL)) D3, 
				sum(decode(id_jenjang,1,JML,NULL)) S1, 
				sum(decode(id_jenjang,2,JML,NULL)) S2, 
				sum(decode(id_jenjang,3,JML,NULL)) S3,
				SUM(DECODE(id_jenjang,9,JML,NULL)) PROFESI,
				SUM(DECODE(id_jenjang,10,JML,NULL)) SPESIALIS
				from (
							SELECT AD.NO_SK,to_char(AD.TGL_SK,'dd-mm-yyyy') as tgl_sk,SPG.NM_STATUS_PENGGUNA,jj.id_jenjang,jj.nm_jenjang, count(AD.ID_MHS) as JML
					    FROM ADMISI ad
				   	  LEFT JOIN STATUS_PENGGUNA spg on SPG.ID_STATUS_PENGGUNA=AD.STATUS_AKD_MHS
							LEFT JOIN MAHASISWA ms on ms.id_mhs=AD.id_mhs
							LEFT JOIN PROGRAM_STUDI ps ON ps.id_program_studi=ms.id_program_studi
							LEFT JOIN JENJANG jj on jj.id_jenjang=ps.id_jenjang
					    WHERE ad.NO_SK IS NOT NULL 
					    AND AD.NO_SK <> '-' 
					    AND TO_CHAR(AD.TGL_SK,'yyyy')='2012'
					    and substr(AD.NO_SK,-7,2)='KR'
							AND AD.STATUS_AKD_MHS in ('3','5','6','20')
							GROUP BY AD.NO_SK,to_char(AD.TGL_SK,'dd-mm-yyyy'),SPG.NM_STATUS_PENGGUNA,jj.id_jenjang,jj.nm_jenjang
						)
GROUP BY TGL_SK,NO_SK,nm_status_pengguna
ORDER BY tgl_sk,no_sk
;


SELECT DISTINCT(AD.NO_SK) as no_sk,to_char(AD.TGL_SK,'dd-mm-yyyy') as tgl_sk,SPG.NM_STATUS_PENGGUNA as ttg
					    FROM ADMISI ad
				   	    LEFT JOIN STATUS_PENGGUNA spg on SPG.ID_STATUS_PENGGUNA=AD.STATUS_AKD_MHS
					    WHERE ad.NO_SK IS NOT NULL 

					    AND AD.NO_SK <> '-' 
					    AND TO_CHAR(AD.TGL_SK,'yyyy')='$_POST[periode]'
					    and substr(AD.NO_SK,-7,2)='KR'
					    AND AD.STATUS_APV='1'
					    $where
					    ORDER BY to_char(AD.TGL_SK,'dd-mm-yyyy'),SPG.nm_STATUS_PENGGUNA
*/
?>
