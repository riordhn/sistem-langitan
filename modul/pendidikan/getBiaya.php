<?php

include '../../config.php';

echo '<table>
			<tr>
				<th colspan="5">Biaya Kuliah</th>
			</tr>
			<tr>
				<th rowspan="2">Kelompok Biaya</th>
				<th rowspan="2">Besar Biaya</th>
				<th rowspan="2">Pilih Biaya</th>
				<th colspan="2" style=text-align:center>Rincian</th>
			</tr>
			<tr>
				<th>Nama Biaya</th>
				<th>Besar Biaya</th>
			</tr>';
			
if(isset($_POST['id_program_studi']) and isset($_POST['id_semester'])){
$mhs = $db->QueryToArray("SELECT * FROM BIAYA_KULIAH 
						JOIN KELOMPOK_BIAYA ON KELOMPOK_BIAYA.ID_KELOMPOK_BIAYA = BIAYA_KULIAH.ID_KELOMPOK_BIAYA
						WHERE ID_PROGRAM_STUDI = '$_POST[id_program_studi]'
						AND ID_SEMESTER = $_POST[id_semester]
						");


foreach($mhs as $data){
		$biaya = $db->QueryToArray("SELECT NM_BIAYA, BESAR_BIAYA FROM DETAIL_BIAYA
								JOIN BIAYA ON BIAYA.ID_BIAYA = DETAIL_BIAYA.ID_BIAYA
								WHERE ID_BIAYA_KULIAH = $data[ID_BIAYA_KULIAH]");
		$count = count($biaya)+1;
	echo '<tr>
				<td rowspan="'.$count.'">'.$data['NM_KELOMPOK_BIAYA'].'</td>
				<td style=text-align:right  rowspan="'.$count.'">'.number_format($data['BESAR_BIAYA_KULIAH'], 0, '', '.').'</td>
				';
	echo '<td style=text-align:center rowspan="'.$count.'"><input type="radio" name="set_biaya" value="'.$data['ID_BIAYA_KULIAH'].'" class="required" /></td>
			</tr>';
	
	foreach($biaya as $data2){
		echo '	<tr>
					<td>'.$data2['NM_BIAYA'].'</td>
					<td style=text-align:right >'.number_format($data2['BESAR_BIAYA'], 0, '', '.').'</td>
				</tr>';
	}

}

}
echo '</table>';
?>
