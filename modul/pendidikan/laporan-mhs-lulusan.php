<?php
include('config.php');
include "class/laporan.class.php";
$aucc = new laporan($db);

if(isset($_POST['angkatan'])){
$smarty->assign('prodi', $aucc->prodi_condition($_POST['fakultas'], $_POST['jenjang']));
$smarty->assign('mhs_jenjang', $aucc->mhs_kota($_POST['angkatan'], $_POST['tampilan'], $_POST['thn_akademik'], $_POST['status_akademik'], $_POST['fakultas'], $_POST['jenjang'], $_POST['program_studi'], $_POST['negara'], $_POST['provinsi'], $_POST['kota']));
}

$smarty->assign('data_fakultas', $aucc->fakultas());
$smarty->assign('data_jenjang', $aucc->jenjang());
$smarty->assign('data_negara', $aucc->negara());
$smarty->assign('angkatan', $aucc->angkatan());
$smarty->assign('thn_akademik', $aucc->thn_akademik());
$smarty->assign('status_tidak_aktif', $aucc->status_tidak_aktif());

$smarty->display('laporan/laporan-mhs-lulusan.tpl');
?>
