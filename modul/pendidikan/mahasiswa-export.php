<?php
include 'config.php';

if (isset($_GET['data']))
{
	$data = $_GET['data'];

	if ($data == 'kelas-mahasiswa')
	{
		$sql =
			"SELECT nim_mhs, nm_pengguna, nm_jenjang, nm_program_studi, nama_kelas
			FROM mahasiswa m
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna AND p.id_perguruan_tinggi = {$id_pt_user}
			JOIN status_pengguna sp ON sp.id_status_pengguna = m.status_akademik_mhs AND sp.status_aktif = 1
			LEFT JOIN mahasiswa_kelas mk ON mk.id_mhs = m.id_mhs and mk.is_aktif = 1
			LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = mk.id_nama_kelas
			JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
			JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
			ORDER BY 1";

		$data_set = $db->QueryToArray($sql);

		// Create new PHPExcel object
		$objPHPExcel = new PhpOffice\PhpSpreadsheet\Spreadsheet();
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Sistem Langitan");
		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'NIM')
			->setCellValue('B1', 'Nama')
			->setCellValue('C1', 'Jenjang')
			->setCellValue('D1', 'Program Studi')
			->setCellValue('E1', 'Kelas');

		$i = 2;
		foreach ($data_set as $data)
		{
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, $data['NIM_MHS'])
				->setCellValue('B'.$i, $data['NM_PENGGUNA'])
				->setCellValue('C'.$i, $data['NM_JENJANG'])
				->setCellValue('D'.$i, $data['NM_PROGRAM_STUDI'])
				->setCellValue('E'.$i, $data['NAMA_KELAS']);

			$i++;
		}

		$filename = "mahasiswa-all-kelas-".date('Y-m-d_His').'.xlsx';

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment;filename={$filename}");
		header('Cache-Control: max-age=0');

		$objWriter = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
		$objWriter->save('php://output');

		exit();
	}
}

$smarty->display('mahasiswa/export/export.tpl');