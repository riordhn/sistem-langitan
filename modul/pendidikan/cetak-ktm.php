<?php
include('config.php');
$nim_mhs = get('nim_mhs');
if (!empty($nim_mhs)) {
    $db->Query(
    "SELECT P.NM_PENGGUNA,M.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG, K.NM_KOTA
    FROM MAHASISWA M  
    JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
    LEFT JOIN KOTA K ON K.ID_KOTA = M.ALAMAT_ASAL_MHS_KOTA
    WHERE M.NIM_MHS = '{$nim_mhs}'");
    $mahasiswa = $db->FetchAssoc();

    $alamat = 
        $mahasiswa['ALAMAT_ASAL_MHS'] . 
        ' ' . $mahasiswa['ALAMAT_DUSUN_ASAL_MHS'] .
        ' RT ' . $mahasiswa['ALAMAT_RT_ASAL_MHS'] .
        ' RW ' . $mahasiswa['ALAMAT_RW_ASAL_MHS'] . 
        ' ' . $mahasiswa['ALAMAT_KELURAHAN_ASAL_MHS'] .
        ', Kec. ' . $mahasiswa['ALAMAT_KECAMATAN_ASAL_MHS'] .
        ', ' . $mahasiswa['NM_KOTA'];

    $smarty->assign('nim_mhs', $nim_mhs);
    $smarty->assign('mahasiswa', $mahasiswa);
    $smarty->assign('alamat', $alamat);
}

$smarty->display("pendaftaran/ktm/cetak-ktm.tpl");
