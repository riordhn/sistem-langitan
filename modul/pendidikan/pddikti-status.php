<?php
include 'config.php';
include 'class/aucc.class.php';
include 'class/pddikti.class.php';

$aucc		= new AUCC_Pendidikan($db);
$pddikti	= new PDDikti($db);

$semester_set = $aucc->get_semester();
$smarty->assign('semester_set', $semester_set);

$program_studi_set = $aucc->get_program_studi();
$smarty->assign('program_studi_set', $program_studi_set);

$angkatan_set = $aucc->get_thn_semester_distinct();
$smarty->assign('angkatan_set', $angkatan_set);

$status_set = $pddikti->list_master_status_mhs();
$smarty->assign('status_set', $status_set);

$id_semester		= isset($_GET['id_semester']) ? $_GET['id_semester'] : 'all';
$id_program_studi	= isset($_GET['id_program_studi']) ? $_GET['id_program_studi'] : 'all';
$angkatan			= isset($_GET['angkatan']) ? $_GET['angkatan'] : 'all';
$id_status_pengguna	= isset($_GET['id_status_pengguna']) ? $_GET['id_status_pengguna'] : 'all';



// Jika all semua, jangan tampilkan
if ($id_semester == 'all' && $id_program_studi == 'all' && $angkatan == 'all' && $id_status_pengguna == 'all')
{
	$data_set = array();
}
else
{
	$data_set = $pddikti->list_status_mahasiswa($id_semester, $id_program_studi, $angkatan, $id_status_pengguna);	
}

$smarty->assign('data_set', $data_set);

// var_dump($data_set); exit();

$smarty->display('pddikti/status.tpl');