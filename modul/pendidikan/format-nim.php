<?php
include('config.php');
$id_pengguna= $user->ID_PENGGUNA; 

$mode = get('mode', 'view');

if ($request_method == 'POST')
{
    if (post('mode') == 'update')
    {   
        $db->Query("UPDATE PERGURUAN_TINGGI SET FORMAT_NIM_PT = '$_POST[format_nim_pt]' WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}'");

        $fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}'");

        foreach ($fakultas as $fak) {
            $id_fak = $fak['ID_FAKULTAS'];

            $format_nim_fak = $_POST['format_nim_fakultas_'.$id_fak.''];

            $db->Query("UPDATE FAKULTAS SET FORMAT_NIM_FAKULTAS = '{$format_nim_fak}' WHERE ID_FAKULTAS = '{$id_fak}'");

        }
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
    if ($mode == 'view')
    {
        $perguruan_tinggi = $db->QueryToArray("SELECT * FROM PERGURUAN_TINGGI WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}'");
        $smarty->assign('perguruan_tinggi', $perguruan_tinggi);

        $smarty->assign('fakultas_set', $db->QueryToArray("select * from fakultas where id_perguruan_tinggi = '{$id_pt_user}'"));
    }

}

$smarty->display("format_nim/{$mode}.tpl");
?>
