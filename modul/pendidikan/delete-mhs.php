<?php
include('config.php');
$id_pengguna = $user->ID_PENGGUNA;

if (isset($_GET['nim']))
{
	$db->Query("SELECT PENGGUNA.ID_PENGGUNA, MAHASISWA.ID_MHS, MAHASISWA.NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_JENJANG, NO_UJIAN, PROGRAM_STUDI.ID_PROGRAM_STUDI
				FROM MAHASISWA
				JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA AND PENGGUNA.ID_PERGURUAN_TINGGI = {$user->ID_PERGURUAN_TINGGI}
				JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
				JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				LEFT JOIN CALON_MAHASISWA_BARU ON CALON_MAHASISWA_BARU.ID_C_MHS = MAHASISWA.ID_C_MHS
				WHERE MAHASISWA.NIM_MHS = '$_GET[nim]'");

	$mhs = $db->FetchAssoc();

	if ($mhs['NIM_MHS'] == '')
	{
		$db->Query("SELECT MAHASISWA_ASING.NIM_MHS_ASING AS NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_JENJANG, '' AS NO_UJIAN, PROGRAM_STUDI.ID_PROGRAM_STUDI
					FROM MAHASISWA_ASING
					JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA_ASING.ID_PENGGUNA AND PENGGUNA.ID_PERGURUAN_TINGGI = {$user->ID_PERGURUAN_TINGGI}
					JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA_ASING.ID_PROGRAM_STUDI
					JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
					WHERE MAHASISWA_ASING.NIM_MHS_ASING = '$_GET[nim]'");

		$mhs = $db->FetchAssoc();
	}

	$smarty->assign('mhs', $mhs);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$id_mhs		= $_POST['id_mhs'];
	$keterangan	= $_POST['keterangan'];

	if (isset($id_mhs) and isset($keterangan))
	{

		echo $id_mhs;
		exit();

		try
		{
			// Start Transaction
			$db->BeginTransaction();

			$nama = str_replace("'", "''", $_POST['nama']);

			$db->Query("SELECT MAHASISWA_ASING.NIM_MHS_ASING AS NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_JENJANG, '' AS NO_UJIAN, PROGRAM_STUDI.ID_PROGRAM_STUDI
					FROM MAHASISWA_ASING
					JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA_ASING.ID_PENGGUNA AND PENGGUNA.ID_PERGURUAN_TINGGI = {$user->ID_PERGURUAN_TINGGI}
					JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA_ASING.ID_PROGRAM_STUDI
					JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
					WHERE MAHASISWA_ASING.NIM_MHS_ASING = '$_POST[nim]'");

			$mhs = $db->FetchAssoc();

			if ($mhs['NIM_MHS'] != '')
			{
				$db->Query("INSERT INTO MAHASISWA_HAPUS (NIM_MHS, NM_PENGGUNA, NO_UJIAN, TGL_HAPUS, KETERANGAN, ID_PENGGUNA, ID_PROGRAM_STUDI)
				VALUES ('$_POST[nim]', '$nama', '$_POST[no_ujian]', SYSDATE, '$_POST[keterangan]', '$id_pengguna', '$_POST[prodi]')")
					or die('gagal hapus');
				$db->Query("DELETE MAHASISWA_ASING WHERE NIM_MHS_ASING = '$_POST[nim]'") or die('gagal 5');
				$pengguna = $db->Query("DELETE PENGGUNA WHERE USERNAME = '$_POST[nim]'") or die('gagal 6');
			}
			else
			{
				$db->Query("INSERT INTO MAHASISWA_HAPUS (NIM_MHS, NM_PENGGUNA, NO_UJIAN, TGL_HAPUS, KETERANGAN, ID_PENGGUNA, ID_PROGRAM_STUDI)
				VALUES ('$_POST[nim]', '$nama', '$_POST[no_ujian]', SYSDATE, '$_POST[keterangan]', '$id_pengguna', '$_POST[prodi]')")
					or die('gagal hapus');
				$db->Query("DELETE BIAYA_KULIAH_MHS WHERE ID_MHS IN (SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS = '$_POST[nim]')") or die('gagal 7');
				$db->Query("DELETE JALUR_MAHASISWA WHERE NIM_MHS = '$_POST[nim]'") or die('gagal 1');
				$db->Query("DELETE PEMBAYARAN WHERE  ID_MHS IN (SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS = '$_POST[nim]')") or die('gagal 2');
				$db->Query("DELETE PEMBAYARAN_DIHAPUS WHERE  ID_MHS IN (SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS = '$_POST[nim]')") or die('gagal 9');
				$db->Query("DELETE FINGERPRINT_MAHASISWA WHERE NIM_MHS = '$_POST[nim]'") or die('gagal 3');
				$db->Query("DELETE ADMISI WHERE ID_MHS IN (SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS = '$_POST[nim]')") or die('gagal 4');
				$db->Query("UPDATE CALON_MAHASISWA_BARU SET NIM_MHS = '' WHERE NIM_MHS = '$_POST[nim]'") or die('gagal 8');
				$db->Query("DELETE CEKAL_PEMBAYARAN WHERE ID_MHS IN (SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS = '$_POST[nim]')") or die('gagal 53');
				$db->Query("DELETE MAHASISWA WHERE NIM_MHS = '$_POST[nim]'") or die('gagal 5');
				$db->Query("DELETE VALIDASI_USERNAME WHERE USERNAME = '$_POST[nim]'") or die('gagal 67');
				$db->Query("DELETE EMAIL WHERE ID_PENGGUNA in (SELECT ID_PENGGUNA FROM PENGGUNA WHERE USERNAME = '$_POST[nim]')") or die('gagal 67');
				$db->Query("DELETE PENGGUNA WHERE  USERNAME = '$_POST[nim]'") or die('gagal 6');
			}

			$db->commit();

			echo "Berhasil";
		}
		catch (Exception $e)
		{
			echo print_r(error_get_last(), true);

			// RollBack()
			$db->rollBack();
		}


		/* if($pengguna){
		  echo "Berhasil";
		  $db->commit();
		  }else{
		  $db->rollBack();
		  } */
	}
}

$smarty->display("pendaftaran/delete_mhs/delete-mhs.tpl");