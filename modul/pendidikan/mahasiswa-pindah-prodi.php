<?php
include('config.php');
include 'class/report_mahasiswa.class.php';
$id_pengguna= $user->ID_PENGGUNA; 
$rm = new report_mahasiswa($db);

if (isset($_GET['tampil'])) {
    $smarty->assign('fakultas', get('fakultas'));
	$smarty->assign('jenjang', get('jenjang'));
    $smarty->assign('program_studi', get('program_studi'));
	$smarty->assign('fakultas2', get('fakultas2'));
	$smarty->assign('jenjang2', get('jenjang2'));
    $smarty->assign('program_studi2', get('program_studi2'));
	$smarty->assign('data_program_studi', $rm->get_program_studi(get('fakultas')));
    $smarty->assign('tampil', get('tampil'));
	
	if($_GET['program_studi'] <> '' and $_GET['program_studi2'] <> ''){
		$mhs = $db->QueryToArray("SELECT ID_MHS FROM MAHASISWA WHERE ID_PROGRAM_STUDI = '$_GET[program_studi]'");
		foreach($mhs as $r){
			$db->Query("INSERT INTO LOG_PRODI_MHS (ID_MHS, ID_PRODI_SEBELUM, ID_PRODI_SEKARANG, ID_PENGUBAH) 
						VALUES ('$r[ID_MHS]', '$_GET[program_studi]', '$_GET[program_studi2]', '$id_pengguna')");
		}
		
		$db->Query("UPDATE MAHASISWA SET ID_PROGRAM_STUDI = '$_GET[program_studi2]' WHERE ID_PROGRAM_STUDI = '$_GET[program_studi]'
					AND STATUS_AKADEMIK_MHS IN (SELECT ID_STATUS_PENGGUNA FROM STATUS_PENGGUNA WHERE STATUS_AKTIF = 1)");
		//$db->Query("UPDATE CALON_MAHASISWA_BARU SET ID_PROGRAM_STUDI = '$_GET[program_studi2]' WHERE ID_PROGRAM_STUDI = '$_GET[program_studi]'");
						
		$jml1 = $db->QueryToArray("SELECT COUNT(*) AS JML FROM MAHASISWA 
									LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
									WHERE ID_PROGRAM_STUDI = '$_GET[program_studi]' AND STATUS_AKTIF = 1");
		$jml2 = $db->QueryToArray("SELECT COUNT(*) AS JML FROM MAHASISWA
									LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
									WHERE ID_PROGRAM_STUDI = '$_GET[program_studi2]' AND STATUS_AKTIF = 1");
		$smarty->assign('jml1', $jml1[0]['JML']);
		$smarty->assign('jml2', $jml2[0]['JML']);
	}
	
}

$smarty->assign('data_fakultas', $rm->get_fakultas());
$smarty->assign('data_jenjang', $rm->get_jenjang());
$smarty->display("mahasiswa/pindah_prodi/mahasiswa-pindah-prodi.tpl");
?>
