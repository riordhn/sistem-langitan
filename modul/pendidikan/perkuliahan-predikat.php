<?php
include('config.php');

$mode = get('mode', 'view');
$id_mhs_predikat = get('id_mhs_predikat', 0);

if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {
		$db->Query("INSERT INTO MHS_PREDIKAT (ID_JENJANG, IPK_MIN_MHS_PREDIKAT, IPK_MAX_MHS_PREDIKAT, NM_MHS_PREDIKAT, MASA_STUDI_PREDIKAT)
					VALUES ('$_POST[id_jenjang]','$_POST[ipk_min_mhs_predikat]','$_POST[ipk_max_mhs_predikat]','$_POST[nm_mhs_predikat]', '$_POST[batas_studi]')");
    }
    else if (post('mode') == 'edit')
    {
		$db->Query("UPDATE MHS_PREDIKAT SET ID_JENJANG = '$_POST[id_jenjang]', IPK_MIN_MHS_PREDIKAT = '$_POST[ipk_min_mhs_predikat]', 
					IPK_MAX_MHS_PREDIKAT = '$_POST[ipk_max_mhs_predikat]', NM_MHS_PREDIKAT = '$_POST[nm_mhs_predikat]', MASA_STUDI_PREDIKAT= '$_POST[batas_studi]'
					WHERE ID_MHS_PREDIKAT = '$_POST[id_mhs_predikat]'");
    }
    else if (post('mode') == 'delete')
    {
        $db->Query("DELETE MHS_PREDIKAT WHERE ID_MHS_PREDIKAT = '$_POST[id_mhs_predikat]'");
    }
}

if ($mode == 'view')
{
    $mhs_predikat_set = $db->QueryToArray("SELECT * FROM MHS_PREDIKAT
        JOIN JENJANG J ON J.ID_JENJANG = MHS_PREDIKAT.ID_JENJANG
        ORDER BY J.NM_JENJANG, MHS_PREDIKAT.IPK_MIN_MHS_PREDIKAT");
    $smarty->assign('mhs_predikat_set', $mhs_predikat_set);
}
else if ($mode == 'add')
{
    $smarty->assign('jenjang_set', $db->QueryToArray("SELECT * FROM JENJANG ORDER BY NM_JENJANG"));
}
else if ($mode == 'edit')
{
    $db->Query("SELECT * FROM MHS_PREDIKAT WHERE ID_MHS_PREDIKAT = '$id_mhs_predikat'");
    $mhs_predikat = $db->FetchAssoc();
	$smarty->assign('jenjang_set', $db->QueryToArray("SELECT * FROM JENJANG ORDER BY NM_JENJANG"));
    $smarty->assign('mhs_predikat', $mhs_predikat);
}
else if ($mode == 'delete')
{
    $db->Query("SELECT * FROM MHS_PREDIKAT JOIN JENJANG ON JENJANG.ID_JENJANG = MHS_PREDIKAT.ID_JENJANG 
				WHERE ID_MHS_PREDIKAT = '$id_mhs_predikat'");
    $mhs_predikat = $db->FetchAssoc();
    $smarty->assign('mhs_predikat', $mhs_predikat);
}

$smarty->display("perkuliahan/predikat/{$mode}.tpl");
?>
