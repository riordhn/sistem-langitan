<?php
include('config.php');
$periode = $db->QueryToArray("SELECT * FROM TARIF_WISUDA WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY NM_TARIF_WISUDA ASC");
$smarty->assign('periode', $periode);
$fakultas = $db->QueryToArray("SELECT ID_FAKULTAS,NM_FAKULTAS FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY ID_FAKULTAS ASC");
$smarty->assign('fakultas', $fakultas);

if(isset($_POST['periode'])){
		if($_POST['fakultas'] <> ''){
			$where  = " AND PS.ID_FAKULTAS = '$_POST[fakultas]'";
			$order = "";
			$fak = "";
		}else{
			$where  = "";
			$order = "PS.ID_FAKULTAS, ";
			$fak = "fk.nm_fakultas,";
		}
		
		$rekap = $db->QueryToArray("SELECT (pg.gelar_depan || pg.nm_pengguna || case when pg.gelar_belakang is null then '' else ', ' end || pg.gelar_belakang)nm_mhs,
             				    mh.nim_mhs,pw.lahir_ijazah,jj.nm_jenjang,$fak ps.nm_program_studi,pw.tgl_lulus_pengajuan,
					    mh.nm_ayah_mhs,mh.alamat_mhs,pg.email_pengguna
					    FROM pembayaran_wisuda bw
					    join mahasiswa mh on mh.id_mhs=bw.id_mhs
					    join pengguna pg on pg.id_pengguna=mh.id_pengguna
					    join program_studi ps on ps.id_program_studi=mh.id_program_studi
					    join fakultas fk on fk.id_fakultas=ps.id_fakultas
					    join jenjang jj on jj.id_jenjang=ps.id_jenjang
					    join periode_wisuda ww on ww.id_periode_wisuda=bw.id_periode_wisuda
					    left join pengajuan_wisuda pw on pw.id_mhs=bw.id_mhs
					    where bw.absensi_wisuda=1 and ww.id_tarif_wisuda='$_POST[periode]'
					    $where
					    order by $order ps.id_jenjang,ps.id_program_studi,mh.nim_mhs,bw.tgl_bayar");
$smarty->assign('rekap', $rekap);	
$smarty->assign('id_periode', $_POST['periode']);
$smarty->assign('id_fakultas', $_POST['fakultas']);
}

$smarty->display("wisuda/wisuda-buku.tpl");
?>
