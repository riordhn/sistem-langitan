<?php

include 'config.php';
include 'class/aucc.class.php';

$aucc = new AUCC_Pendidikan($db);
if (isset($_POST['cari_nim'])) {
    $nim = str_replace("'", "''", $_POST['cari_nim']);
    $smarty->assign('data_pembayaran', $aucc->detail_pembayaran($nim));
    $smarty->assign('data_bank', $aucc->get_bank());
    $smarty->assign('data_semester', $aucc->get_semester_all());
    $smarty->assign('data_bank_via', $aucc->get_via_bank());
    $smarty->assign('cari_nim', 1);
}
if (isset($_GET['mode'])) {
    $nim = str_replace("'", "''", $_POST['cari_nim']);
    if ($_GET['mode'] == 'biaya') {
        for ($i = 1; $i < $_POST['jumlah']; $i++) {
			$db->Query("UPDATE PEMBAYARAN SET IS_TAGIH='{$_POST['is_tagih' . $i]}' WHERE ID_PEMBAYARAN='{$_POST['id' . $i]}'");
		}
    } else if ($_GET['mode'] == 'status') {
        $db->Query("UPDATE PEMBAYARAN SET IS_TAGIH='{$_POST['is_tagih']}' WHERE ID_MHS='{$_POST['id']}' AND ID_SEMESTER='{$_POST['semester']}'");
    }
    $smarty->assign('data_pembayaran', $aucc->detail_pembayaran($nim));
    $smarty->assign('data_semester', $aucc->get_semester_all());
    $smarty->assign('data_bank', $aucc->get_bank());
    $smarty->assign('data_bank_via', $aucc->get_via_bank());
    $smarty->assign('cari_nim', 1);
}

$smarty->display('pembayaran/buka-pembayaran.tpl');
?>
