<?php
include('config.php');
$periode = $db->QueryToArray("SELECT * FROM TARIF_WISUDA WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY ID_TARIF_WISUDA DESC");
$smarty->assign('periode', $periode);
$fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' ORDER BY ID_FAKULTAS ASC");
$smarty->assign('fakultas', $fakultas);

if(isset($_POST['periode'])){

		if($_POST['fakultas'] <> ''){
			$select = "";
			$where  = " AND FAKULTAS.ID_FAKULTAS = '$_POST[fakultas]'";
			$order = "";
		}else{
			$select = " ,NM_FAKULTAS, FAKULTAS.ID_FAKULTAS";
			$where  = "";
			$order = "FAKULTAS.ID_FAKULTAS, ";
		}
		
		if($_POST['tampilan'] == 'thn_angkatan'){
		$rekap = $db->QueryToArray("SELECT THN_ANGKATAN_MHS, SUM(DECODE(id_jenjang, '5-R', JML, NULL)) D3, SUM(DECODE(id_jenjang, '1-R', JML, NULL)) S1_R, SUM(DECODE(id_jenjang, '1-AJ', JML, NULL)) S1_AJ
			, SUM(DECODE(id_jenjang, '2-R', JML, NULL)) S2 , SUM(DECODE(id_jenjang, '3-R', JML, NULL)) S3 
			, SUM(DECODE(id_jenjang, '9-R', JML, NULL)) PROFESI , SUM(DECODE(id_jenjang, '10-R', JML, NULL)) SPESIALIS , SUM(JML) AS TOTAL
FROM (
SELECT THN_ANGKATAN_MHS, NM_JENJANG, JENJANG.ID_JENJANG || '-' || STATUS AS ID_JENJANG, COUNT(*) AS JML 
									FROM PEMBAYARAN_WISUDA
									JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS
									JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS
									JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
									JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
									JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
									JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PEMBAYARAN_WISUDA.ID_PERIODE_WISUDA
									WHERE ABSENSI_WISUDA = 1 AND PERIODE_WISUDA.ID_TARIF_WISUDA = '$_POST[periode]' AND YUDISIUM != 0
									GROUP BY THN_ANGKATAN_MHS, NM_JENJANG, JENJANG.ID_JENJANG || '-' || STATUS
)
	GROUP BY THN_ANGKATAN_MHS
									ORDER BY THN_ANGKATAN_MHS DESC");
		}else{
		$rekap = $db->QueryToArray("SELECT NM_PROGRAM_STUDI, NM_JENJANG, COUNT(*) AS JML $select 
									FROM PEMBAYARAN_WISUDA
									JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS
									JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS
									JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
									JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
									JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
									JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PEMBAYARAN_WISUDA.ID_PERIODE_WISUDA
									WHERE ABSENSI_WISUDA = 1 AND PERIODE_WISUDA.ID_TARIF_WISUDA = '$_POST[periode]'  AND YUDISIUM != 0
									$where
									GROUP BY NM_PROGRAM_STUDI, NM_JENJANG $select
									ORDER BY $order NM_JENJANG, NM_PROGRAM_STUDI");
		}
		$smarty->assign('rekap', $rekap);	


$smarty->assign('id_periode', $_POST['periode']);
$smarty->assign('id_fakultas', $_POST['fakultas']);
}

$smarty->display("wisuda/wisuda-rekap.tpl");
?>
