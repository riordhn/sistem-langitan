<?php
include('config.php');
include 'class/report_mahasiswa.class.php';
$rm = new report_mahasiswa($db);
$id_pengguna= $user->ID_PENGGUNA; 

		$smarty->assign('data_fakultas', $rm->get_fakultas());
		$smarty->assign('data_jenis_kerjasama', $rm->get_jenis_kerjasama());
		$smarty->assign('data_jenjang', $rm->get_jenjang());
		$smarty->assign('data_semester', $rm->get_semester());
		$smarty->assign('data_benua', $rm->get_benua());
		$smarty->assign('data_prov_in', $rm->get_prov_in());
		$smarty->assign('data_semester_aktif', $rm->get_semester_aktif());
		
		if(isset($_POST['generate'])){
	
			$smarty->assign('data_program_studi', $rm->get_program_studi_by_fakultas_jenjang($_POST['fakultas'], $_POST['jenjang']));
			$smarty->assign('data_negara', $rm->get_negara_by_benua($_POST['benua']));
			$smarty->assign('data_kota_in', $rm->get_kota_by_prov($_POST['prov_in']));
			$smarty->assign('data_prov_asal', $rm->get_prov($_POST['negara_asal']));
			$smarty->assign('kota_asal', $rm->get_kota_by_prov($_POST['prov_asal']));
			$smarty->assign('fakultas', $_POST['fakultas']);
			$smarty->assign('jenjang', $_POST['jenjang']);
			$smarty->assign('program_studi', $_POST['program_studi']);
			$smarty->assign('kota_in', $_POST['kota_in']);
			$smarty->assign('kerjasama', $_POST['kerjasama']);
			$smarty->assign('negara_asal', $_POST['negara_asal']);
			$smarty->assign('prov_asal', $_POST['prov_asal']);
			$smarty->assign('kota_asal', $_POST['kota_asal']);
			$smarty->assign('semester', $_POST['semester']);
			$smarty->assign('prov_in', $_POST['prov_in']);
			$smarty->assign('nama', $_POST['nama']);
			$smarty->assign('benua', $_POST['benua']);
			$smarty->assign('gelar_depan', $_POST['gelar_depan']);
			$smarty->assign('gelar_belakang', $_POST['gelar_belakang']);
			$smarty->assign('alamat', $_POST['alamat']);
			$smarty->assign('alamat_asal', $_POST['alamat_asal']);
			$smarty->assign('generate', $_POST['generate']);
						
			
			
				$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '$_POST[semester]'");
				$semester = $db->FetchAssoc();
				$kode_semester = substr($semester['THN_AKADEMIK_SEMESTER'], -2);
				
				
				$db->Query("SELECT * FROM JENIS_KERJASAMA WHERE ID_JENIS_KERJASAMA = '$_POST[kerjasama]'");
				$kerjasama = $db->FetchAssoc();
				$kode_kerjasama = $kerjasama['KODE_NIM_ASING'];
				if($kode_kerjasama == ''){
					die("Kode Kerjasama Belum Ada");
				}
							
				
				$db->Query("SELECT * FROM BENUA WHERE ID_BENUA = '$_POST[benua]'");
				$benua = $db->FetchAssoc();
				$kode_benua = $benua['KODE_NIM_ASING'];
				if($kode_benua == ''){
					die("Kode Benua Belum Ada");
				}
				
				$db->Query("SELECT * FROM NEGARA WHERE ID_NEGARA = '$_POST[negara_asal]'");
				$negara = $db->FetchAssoc();
				//pergantian kode negara
				//$kode_negara = $negara['KODE_NIM_ASING'];
				$kode_negara = $negara['KODE_NIM_IOP'];
				if($kode_negara == ''){
					die("Kode Negara Belum Ada");
				}
				

			$db->Query("select max(trim(to_char(substr(nim_mhs_asing, 9)+1,'09'))) nim_baru
								from mahasiswa_asing m , semester s
							 where s.id_semester = m.id_semester_masuk and thn_akademik_semester = '$semester[THN_AKADEMIK_SEMESTER]'
							  and substr(nim_mhs_asing, 6, 2) = '$kerjasama[KODE_NIM_ASING]'
							  and substr(nim_mhs_asing, 2, 2) = '$negara[KODE_NIM_IOP]'
							  and substr(nim_mhs_asing, 0, 1) = '$_POST[type]'");
            $nim = $db->FetchAssoc();
			$nim_baru = $nim['NIM_BARU'];
			
			//if($nim_baru == ''){								
				
				
				
				if($nim_baru == ''){
					$seri = '01';
				}else{
					$seri = $nim_baru;
				}
				
				//permintaan IOP kode benua ganti type akademik
				//$nim_baru = $kode_benua . $kode_negara . $kode_semester . $kode_kerjasama . $seri;
				$nim_baru = $_POST['type'] . $kode_negara . $kode_semester . $kode_kerjasama . $seri;
			//}
			
			
			//INSERT PENGGUNA
			$pass = $user->Encrypt($nim_baru);
			$nama =  str_replace("'", "''", $_POST['nama']);
			$alamat_asal =  str_replace("'", "''", $_POST['alamat_asal']);
			
			$db->beginTransaction();
			
			 $db->Query("INSERT INTO PENGGUNA (USERNAME, NM_PENGGUNA, ID_ROLE, JOIN_TABLE, SE1, GELAR_DEPAN, GELAR_BELAKANG) 
						VALUES ('".$nim_baru."', '$nama', '3', '3', '$pass', '$_POST[gelar_depan]', '$_POST[gelar_belakang]')") ;
		
				$db->Query("SELECT * FROM PENGGUNA WHERE USERNAME = '".$nim_baru."'");
				$pengguna = $db->FetchAssoc();
			$alamat =  str_replace("'", "''", $_POST['alamat']);

			$hasil1 = $db->Query("INSERT INTO MAHASISWA_ASING (ID_PENGGUNA, ID_PROGRAM_STUDI, NIM_MHS_ASING, ID_STATUS_PENGGUNA, 
						ALAMAT_MHS_ASING, ID_KOTA, ALAMAT_ASAL_MHS_ASING, ID_KOTA_ASAL, ID_SEMESTER_MASUK, ID_NEGARA_ASAL, ID_JENIS_KERJASAMA)
				VALUES ('".$pengguna['ID_PENGGUNA']."', '$_POST[program_studi]', '".$nim_baru."', 1, 
						'$alamat', '$_POST[kota_in]', '$alamat_asal', '$_POST[kota_asal]', '$_POST[semester]', '$_POST[negara_asal]', '$_POST[kerjasama]')");
			
			if (!$hasil1)
            {
				$db->Rollback();
				die('gagal 1');
			}else{
				$db->commit();
				echo "berhasil";
			}
				
			$smarty->assign('nim_baru', $nim_baru);
						
		}


$smarty->display("pendaftaran/nim_asing/pendaftaran-nim-asing.tpl");
?>
