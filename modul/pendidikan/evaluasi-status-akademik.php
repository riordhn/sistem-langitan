<?php
error_reporting (E_ALL & ~E_NOTICE);
include 'config.php';
include 'class/evaluasi.class.php';

$eval = new evaluasi($db);
$id_pengguna= $user->ID_PENGGUNA; 

$smarty->assign('fakultas', $eval->fakultas());
$smarty->assign('semester', $eval->semester());
$smarty->assign('jenjang', $eval->jenjang_all());
$smarty->assign('status_akademik', $eval->status_akademik_non_lulus());

if($_POST['mode'] == 'insert'){
	
	$no = $_POST['no'];
	$tgl = date('d-m-Y');
	for($i=2; $i<=$no; $i++){
		$id_mhs = $_POST['id_mhs'.$i];
		$penetapan = $_POST['penetapan'.$i];
		$tgl_sk = $_POST['tgl_sk'.$i];
		$sk = $_POST['sk'.$i];
		$keterangan = $_POST['keterangan'.$i];
				
				
				if($penetapan != ''){
					
						$db->Query("SELECT COUNT(*) AS CEK FROM MAHASISWA 
							JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
							WHERE ID_MHS = '$id_mhs' AND STATUS_AKTIF = 1");
						$cek_status = $db->FetchAssoc();
						
						if($cek_status['CEK'] == 0){
							$db->Query("UPDATE MAHASISWA SET STATUS_AKADEMIK_MHS  = '$penetapan', STATUS_CEKAL = '1' WHERE ID_MHS = '$id_mhs'");
						}else{
							$db->Query("UPDATE MAHASISWA SET STATUS_AKADEMIK_MHS  = '$penetapan' WHERE ID_MHS = '$id_mhs'");
						}


					$db->Query("SELECT COUNT(*) AS CEK FROM ADMISI 
								WHERE STATUS_APV = 1 AND ID_MHS = '$id_mhs' 
								AND ID_SEMESTER = '$_POST[semester]' AND STATUS_AKD_MHS = '$penetapan'
								AND NO_SK = '$sk' AND ALASAN = '$keterangan' AND TGL_SK = to_date('$tgl_sk', 'DD-MM-YYYY')");
					$cek = $db->FetchAssoc();
				
				
					if($cek['CEK'] == 0){

						
						$db->Query("SELECT ID_ADMISI FROM ADMISI 
							WHERE STATUS_APV = 1 AND ID_MHS = '$id_mhs' 
							AND ID_SEMESTER = '$_POST[semester]' AND STATUS_AKD_MHS = '$penetapan'");
						$cek_admisi = $db->FetchAssoc();
						
						if($cek_admisi['ID_ADMISI'] != ''){
							
							$db->Query("UPDATE ADMISI SET 
										TGL_USULAN 	= to_date(SYSDATE, 'YYYY-MM-DD'), 
										TGL_APV 	= to_date(SYSDATE, 'YYYY-MM-DD'), 
										TGL_SK 		= to_date('$tgl_sk', 'DD-MM-YYYY'), 
										NO_SK 		= '$sk', 
										ALASAN		= '$keterangan', 
										ID_PENGGUNA	= '$id_pengguna' 
										WHERE '$cek_admisi[ID_ADMISI]'");
							
						}else{
												
						$db->Query("INSERT INTO CEKAL_PEMBAYARAN (ID_MHS, ID_PENGGUNA, ID_SEMESTER, STATUS_CEKAL, TGL_UBAH, KETERANGAN)
									VALUES ('$id_mhs','$id_pengguna','$_POST[semester]','1',to_date(SYSDATE, 'YYYY-MM-DD'), '$keterangan')");
					
						$db->Query("INSERT INTO ADMISI (ID_MHS, ID_SEMESTER, STATUS_AKD_MHS, STATUS_APV, 
								  TGL_USULAN, TGL_APV, TGL_SK, NO_SK, ALASAN, ID_PENGGUNA)
								  VALUES 
								 ('$id_mhs', '$_POST[semester]', '$penetapan', 1, 
								  to_date(SYSDATE, 'YYYY-MM-DD'), to_date(SYSDATE, 'YYYY-MM-DD'), to_date('$tgl_sk', 'DD-MM-YYYY'), 
								  '$sk', '$keterangan', '$id_pengguna')");
						}

					}
					
				}

	}

}


if(isset($_REQUEST['fakultas'])){
$smarty->assign('prodi', $eval->program_studi($_REQUEST['fakultas'], $_REQUEST['jenjang'], $_REQUEST['prodi']));
$smarty->assign('evaluasi', $eval->evaluasi_status_akademik($_REQUEST['fakultas'], $_REQUEST['jenjang'], $_REQUEST['prodi'], $_REQUEST['semester']));
}



if(!isset($_REQUEST['semester'])){
	$db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True'");
	$semester_aktif = $db->FetchAssoc();
	$smarty->assign('semester_aktif', $semester_aktif['ID_SEMESTER']);
}

$smarty->display("evaluasi/status_akademik/evaluasi-status-akademik.tpl");
?>