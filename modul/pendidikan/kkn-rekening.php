<?php
include('config.php');
include 'class/kkn.class.php';
include 'class/laporan.class.php';


$laporan = new laporan($db);
$kkn = new kkn($db);


if(isset($_POST['periode'])){
	$smarty->assign('kkn_mhs', $kkn->kkn_rekening($_POST['fakultas'], $_POST['periode'], $_POST['tampilan']));
}

$smarty->assign('fakultas', $laporan->fakultas());
$smarty->assign('periode', $kkn->kkn_periode());


$smarty->display("kkn/kkn-rekening.tpl")
?>