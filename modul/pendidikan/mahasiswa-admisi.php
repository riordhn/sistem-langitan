<?php
include('config.php');

$mode = get('mode', 'index');

$pengguna	= $user->ID_PENGGUNA;
$tgl		= date('Y-m-d');
$id_pt		= $id_pt_user;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if($_POST('mode') == 'update_admisi'){
		$id_admisi = $_POST['id_admisi'];
		$status_akd_mhs = $_POST['status_akd_mhs'];
		$keterangan = addslashes($_POST['keterangan']);

		$hasil = $db->Query("UPDATE ADMISI SET STATUS_AKD_MHS = '{$status_akd_mhs}', KETERANGAN = '{$keterangan}', UPDATED_ON = SYSDATE, UPDATED_BY = '{$pengguna}' WHERE ID_ADMISI = '{$id_admisi}'") or die('Gagal ' . __LINE__);


		if ($hasil) {
			echo "<script> alert('Admisi berhasil di update'); </script>";
			//exit();
		}
	}
}


if ($mode == 'index')
{
	$nim = get('nim');

	if ( ! empty($nim))
	{
	
		$db->Query(
			"SELECT count(*) as ADA FROM MAHASISWA
			JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
			WHERE NIM_MHS = '{$nim}' AND PENGGUNA.ID_PERGURUAN_TINGGI = '{$id_pt}'");
		$cek = $db->FetchAssoc();
		
		if ($cek['ADA'] == 1) {
		
		$db->Query(
			"SELECT ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, NM_JENJANG, THN_ANGKATAN_MHS, NM_STATUS_PENGGUNA, STATUS_KELUAR 
			FROM MAHASISWA 
			JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
			JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
			JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
			JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
			JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
			WHERE NIM_MHS = '{$nim}'");
		$row = $db->FetchAssoc();
		$smarty->assign('mhs', $row);
		
		$admisi = $db->QueryToArray(
			"SELECT a.id_admisi,
				s.tahun_ajaran, s.nm_semester, nm_status_pengguna, nm_jalur, sp.status_keluar,
				A.no_sk, to_char(A.tgl_sk, 'YYYY-MM-DD') as tgl_sk, to_char(A.tgl_keluar, 'YYYY-MM-DD') as tgl_keluar, A.no_ijasah, A.keterangan,
				tgl_usulan, tgl_apv,
				to_char(created_on,'YYYY-MM-DD HH24:MI:SS') as created_on
			FROM admisi a
			JOIN status_pengguna sp ON sp.id_status_pengguna = A.status_akd_mhs
			LEFT JOIN jalur j ON j.id_jalur = A.id_jalur
			LEFT JOIN semester s ON s.id_semester = A.id_semester
			WHERE status_apv = 1 AND A.id_mhs = {$row['ID_MHS']}
			ORDER BY A.id_jalur NULLS LAST, sp.status_keluar ASC, s.thn_akademik_semester, s.nm_semester");
	
		$smarty->assign('admisi', $admisi);
		
		} else {
			echo 'Tidak ada mahasiswa nya';
		}
	}
}

if ($mode == 'edit')
{
	$id_admisi = get('id_admisi');

	$db->Query(
		"SELECT m.nim_mhs, p.nm_pengguna, j.nm_jenjang||' '||ps.nm_program_studi as nm_program_studi, m.thn_angkatan_mhs, a.id_admisi,
			s.nm_semester, s.thn_akademik_semester, a.status_akd_mhs
		FROM admisi a 
		JOIN mahasiswa m ON m.id_mhs = a.id_mhs
		JOIN pengguna p ON p.id_pengguna = m.id_pengguna
		JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
		JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
		JOIN semester s ON s.id_semester = a.id_semester
		WHERE id_admisi = '{$id_admisi}'"
	);

	$admisi = $db->FetchAssoc();

	$status_akademik_set = $db->QueryToArray("SELECT * FROM status_pengguna WHERE ID_ROLE = 3 AND ID_STATUS_PENGGUNA NOT IN (4,16) ORDER BY status_keluar");

	$smarty->assign('admisi', $admisi);
	$smarty->assign('status_akademik_set', $status_akademik_set);
}

if ($mode == 'delete') 
{
	$id_admisi = get('id_admisi');

	$db->Query("SELECT * FROM ADMISI WHERE ID_ADMISI = '{$id_admisi}'");
	$admisi = $db->FetchAssoc();

	$id_mhs = $admisi['ID_MHS'];
	$id_semester = $admisi['ID_SEMESTER'];

	$hasil = $db->Query("DELETE FROM admisi WHERE id_admisi = '{$id_admisi}'");

	// proses update status akd mhs ke semester seblumnya
	$db->Query("SELECT a.STATUS_AKD_MHS 
				FROM ADMISI a
				JOIN SEMESTER s ON s.ID_SEMESTER = a.ID_SEMESTER
				WHERE a.ID_MHS = '{$id_mhs}'
				ORDER BY s.FD_ID_SMT DESC");
	$admisi_baru = $db->FetchAssoc();

	$status_akd_baru = $admisi_baru['STATUS_AKD_MHS'];

	$db->Query("UPDATE MAHASISWA SET STATUS_AKADEMIK_MHS = '{$status_akd_baru}' WHERE ID_MHS = '{$id_mhs}'") or die('Gagal ' . __LINE__);


	if ($hasil) {
		echo "Admisi berhasil dihapus";
		exit();
	}
}

$smarty->display("mahasiswa/admisi/{$mode}.tpl");