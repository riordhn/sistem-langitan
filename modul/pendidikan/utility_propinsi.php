<?php 
include 'config.php';


$id_negara = get('id_negara', '');

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$nm_provinsi = trim(post('nm_provinsi', ''));
	
	if ($nm_provinsi != '')
	{
		$db->Query("insert into provinsi (id_negara, nm_provinsi) values ('{$id_negara}', '{$nm_provinsi}')");	
	}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET' or $_SERVER['REQUEST_METHOD'] == 'POST')
{
	$negara_set = $db->QueryToArray("select * from negara order by nm_negara");
	$smarty->assignByRef('negara_set', $negara_set);
	
	if ($id_negara != '')
	{
		$provinsi_set = $db->QueryToArray("select * from provinsi where id_negara = {$id_negara} order by nm_provinsi");
		$smarty->assignByRef('provinsi_set', $provinsi_set);
	}		

	$smarty->display('utility/propinsi/index.tpl');
} 

?>