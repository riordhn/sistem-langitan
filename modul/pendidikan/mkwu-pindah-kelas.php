<?php
include('config.php');
include 'class/mkwu.class.php';

$mkwu = new mkwu($db);
$semester_aktif =  $mkwu->semester_aktif();

$username= $user->USERNAME;
$smarty->assign('semester_aktif', $semester_aktif);

$semester_request = isset($_REQUEST['smt'])?  $_REQUEST['smt'] : $semester_aktif['ID_SEMESTER'];
$mode = isset($_REQUEST['mode'])?  $_REQUEST['mode'] : 'view';

if($mode == 'gantikelas'){
	
	$counter=$_POST['counter'];
		
		for ($i=1; $i<=$counter; $i++)
  		{
			if ($_POST['pil'.$i]<>'' || $_POST['pil'.$i]<>null) {
				
			   $db->Query("update pengambilan_mk set id_kelas_mk=$_REQUEST[kelas_mk_tujuan], keterangan='GANTI KELAS MWKU OLEH ".$username."' where id_pengambilan_mk='".$_POST['pil'.$i]."'");
			   $db->Query("delete from nilai_mk where id_pengambilan_mk='".$_POST['pil'.$i]."'");
			}
  		}
	$mode = 'tampil';
}

if($mode == 'view_pindah'){
	
	$smarty->assign('asal_kelas_view', $mkwu->asal_kelas_view($semester_request, $_REQUEST['id_kelas_mk']));
	$smarty->assign('tujuan_kelas_view', $mkwu->tujuan_kelas_view($semester_request, $_REQUEST['id_kelas_mk'], $_REQUEST['mata_kuliah'], $_REQUEST['prodi']));
	$smarty->assign('view_pindah', $mkwu->pindah_kelas_view($semester_request, $_REQUEST['id_kelas_mk']));
}

if($mode == 'tampil'){
	
	$smarty->assign('view_kelas', $mkwu->pecah_kelas_view($semester_request, $_REQUEST['fakultas'], $_REQUEST['mata_kuliah'], $_REQUEST['jenjang']));
}

$smarty->assign('fakultas', $mkwu->fakultas());
$smarty->assign('jenjang', $mkwu->jenjang());
$smarty->display("mkwu/mkwu-pindah-kelas.tpl")
?>