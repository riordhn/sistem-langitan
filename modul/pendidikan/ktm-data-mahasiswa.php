<?php
require('../../config.php');
include 'class/report_mahasiswa.class.php';

$db = new MyOracle();

require_once('../../tcpdf/config/lang/eng.php');
require_once('../../tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sistem Langitan');
$pdf->SetTitle('PDF KTM');
$pdf->SetSubject('PDF KTM');
$pdf->SetKeywords('');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);
//$pdf->SetHeaderData($logo, $logo_size, $title, $content);
$pdf->SetPrintHeader(false);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 15, 10, 15);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 15);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('helvetica', '', 9);

// add a page
$pdf->AddPage('P', 'A4');

$garing1 = "\\";


$html = '
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		  	<td>\Nama\\NIM\\Jurusan\\Alamat\</td>
		  </tr>
		</table>

		';

$rm = new report_mahasiswa($db);
$query = $rm->get_mahasiswa_dayat(2 , get('fakultas'), get('program_studi'), get('tahun_akademik'), get('status'), get('jenjang'), get('jalur'), get('id_pt'), get('bidikmisi'), get('kota'), get('tahun_lulus'));
$result = $db->Query($query)or die("salah kueri 22 ");
while($data = $db->FetchRow()) {
	$nama = $data[1];
	$nim = $data[0];
	$prodi = $data[4].' - '.$data[5];
	$alamat = $data[8];

	$html .= '
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		  	<td>'.$pjma.'</td>
		  </tr>
		</table>

		';

}


$html = '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td width="10%"></td>
    <td width="20%" align="center"> <img src="'.$logo.'" alt="" height="80" width="80"> </td>
    <td width="40%" align="center"><span style="font-size: xx-large;"><b>PRESENSI MAHASISWA</b></span><br/><span style="font-size: xx-large;"><b>PERKULIAHAN SEMESTER '.strtoupper($smt).' '.$thn.'</b></span><br/><span style="font-size: xx-large;"><b>FAKULTAS '.strtoupper($fak).'</b></span></td>
    <td width="30%"></td>
  </tr>
</table>

<div style="color:#020202;height:170px;">_______________________________________________________________________________________________________________________________________________________________________________</div>

';

$html .= '
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="17">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td width="10%">PROGRAM STUDI</td>
  	<td width="40%"> : '.strtoupper($jenjang).' - '.strtoupper($prodi).'</td>
  	<td width="10%"> MATA KULIAH</td>
  	<td width="40%"> : '.$kode_ma.' - '.strtoupper($nama_ma).' ('.$sks_ma.' SKS)</td>

	<!--<td> KELAS :  '.strtoupper($kelas_ma).'</td>-->
    <!--<td colspan="12">'.$kode_ma.' - '.strtoupper($nama_ma).' ('.$sks_ma.' SKS) <br/>KELAS '.strtoupper($kelas_ma).'</td>
	<td colspan="5" rowspan="2">
	<table border="0" cellspacing="0" cellpadding="0">-->';

$kueri = "select case when pengguna.gelar_belakang is null then trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)) 
else trim(pengguna.gelar_depan||' '||upper(pengguna.nm_pengguna)||', '||pengguna.gelar_belakang) end as nama_dsn, 
case when pengampu_mk.pjmk_pengampu_mk=1 then 'PJMA' else 'TIM' end as pjma, kurikulum_mk.tingkat_semester
from kelas_mk
left join kurikulum_mk on kurikulum_mk.id_kurikulum_mk = kelas_mk.id_kurikulum_mk
left join pengampu_mk on kelas_mk.id_kelas_mk = pengampu_mk.id_kelas_mk
left join dosen on pengampu_mk.id_dosen = dosen.id_dosen
left join pengguna on dosen.id_pengguna = pengguna.id_pengguna
where kelas_mk.id_kelas_mk=$id_kelas_mk
order by pengampu_mk.pjmk_pengampu_mk desc";
$result = $db->Query($kueri)or die("salah kueri 26 ");
while($r = $db->FetchRow()) {
	$nama_dsn = $r[0];
	$pjma = $r[1];
	$tingkat_smt = $r[2];
if($nama_dsn != ',') {
$html .= '
<!--<tr>
	<td>'.strtoupper($nama_dsn).' ('.$pjma.')</td>
</tr>-->';
} else {
$html .= '
<!--<tr>
	<td>PJMA BELUM DITENTUKAN</td>
</tr>-->';
}
}
$html .= '
	<!--</table>
	</td>-->
  </tr>
  <tr>
  	<td>KELAS </td>
  	<td> : '.strtoupper($kelas_ma).'</td>
    <!--<td colspan="12">'.strtoupper($hari_ma).' : '.$jam.' <br/>RUANG : '.strtoupper($ruang_ma).'</td>-->
    <td> DOSEN</td>';

if($nama_dsn != ',') {
$html .= '
	<td> : '.strtoupper($nama_dsn).' ('.$pjma.')</td>';
} else {
$html .= '
	<td> : PJMA BELUM DITENTUKAN</td>';
}



$html .='
  </tr>
  <tr>
    <td>SEMESTER / HARI </td>
  	<td> : '.$tingkat_smt.' / '.strtoupper($hari_ma).' : '.$jam.'</td>
  	<td> RUANG </td>
  	<td> : '.strtoupper($ruang_ma).'</td>
  </tr>
  <tr>
    <td></td>
  	<td></td>
  	<td></td>
  	<td></td>
  </tr>
</table>
<table cellspacing="0" cellpadding="4" border="1" width="100%">
<thead>
	<tr bgcolor="lightgray" style="color:#000;">
      <td width="5%" rowspan="3" align="center"><strong><br/>NO.<br/>&nbsp;</strong></td>
	  <td width="9%" rowspan="3" align="center"><strong><br/>NIM<br/>&nbsp;</strong></td>
      <td width="16%" rowspan="3" align="center"><strong><br/>NAMA MAHASISWA<br/>&nbsp;</strong></td>
      <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
	  <td width="5%" align="center"><strong> Tgl </strong></td>
    </tr>
    <tr bgcolor="white" style="color:#000;">
      <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
	  <td width="5%" align="center"><strong> .... </strong></td>
    </tr>
    <tr bgcolor="lightgray" style="color:#000;">
      <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
	  <td width="5%" align="center"><strong> Ttd </strong></td>
    </tr>
</thead>';

$nomor=1;
$kueri = "select mahasiswa.nim_mhs, pengguna.nm_pengguna
from mahasiswa
left join pengambilan_mk on mahasiswa.id_mhs=pengambilan_mk.id_mhs
left join pengguna on pengguna.id_pengguna=mahasiswa.id_pengguna
where pengambilan_mk.id_kelas_mk=$id_kelas_mk and pengambilan_mk.status_apv_pengambilan_mk=1
order by mahasiswa.nim_mhs asc";
$result = $db->Query($kueri)or die("salah kueri 27 ");
while($r = $db->FetchRow()) {
	$nim_mhs = $r[0];
	$nama_mhs = $r[1];

$html .= '
    <tr nobr="true" bgcolor="#FFF" style="color:#000;">
      <td width="5%" align="center">'.$nomor.'</td>
	  <td width="9%" align="center">'.$nim_mhs.'</td>
      <td width="16%" align="left">'.singkat_nama(strtoupper($nama_mhs)).'</td>
      <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
    </tr>';
$nomor++;
}

for ($i=0; $i < 4; $i++) { 
	$html .= '
    <tr nobr="true" bgcolor="#FFF" style="color:#000;">
      <td width="5%" align="center">&nbsp;</td>
	  <td width="9%" align="center">&nbsp;</td>
      <td width="16%" align="center">&nbsp;</td>
      <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
	  <td width="5%" align="center">&nbsp;</td>
    </tr>';
}

$html .= '</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('PRESENSI KULIAH '.strtoupper($nama_ma).' '.strtoupper($kelas_ma).'.pdf', 'I');

?>