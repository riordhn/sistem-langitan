<?php
include('config.php');
$id_pengguna_user= $user->ID_PENGGUNA;

$mode = get('mode', 'view');

if ($request_method == 'POST') {
    // Mendapatkan ID
    $no_ujian_set = $_POST['no_ujian'];

    $hasil_nim = $_POST['hasil_nim'];
    $panjang_seri = (int) $_POST['panjang_seri'];
    $nomor_awal_seri = (int) $_POST['nomor_awal_seri'];
    
    // Tabel informasi yg terjadi kegagalan
    $result_set = array();
    
    if (!empty($no_ujian_set)) {
        foreach ($no_ujian_set as $no_ujian) {
            // Cek syarat-syarat (S1) :
            // - Lulus Tes Kesehatan

            $db->Query(
                "SELECT cmd.kesehatan_kesimpulan_akhir, p.tahun, ps.id_fakultas, ps.kode_program_studi, cmb.nm_c_mhs, 
                cmb.jenis_kelamin, cmb.id_agama, cmb.tgl_lahir, cmb.id_kota_lahir, cmb.nim_mhs, ps.id_jenjang,
                cmb.id_c_mhs, id_verifikator_pendidikan, ps.kode_nim_prodi, f.kode_nim_fakultas, f.format_nim_fakultas, cmb.email
                from calon_mahasiswa_data cmd
                join calon_mahasiswa_baru cmb on cmb.id_c_mhs = cmd.id_c_mhs
                join penerimaan p on p.id_penerimaan = cmb.id_penerimaan and p.id_perguruan_tinggi = '{$id_pt_user}'
                join program_studi ps on ps.id_program_studi = cmb.id_program_studi
                join fakultas f on f.id_fakultas = ps.id_fakultas
                where cmd.id_c_mhs = (select id_c_mhs from calon_mahasiswa_baru cmd where no_ujian = '{$no_ujian}')");
            $row = $db->FetchAssoc();

            // Cek kesehatan
            // Tidak Ada Kebutuhan Tes Kesehatan
            $tes = 'Lulus Tes';
            /*if ($row['KESEHATAN_KESIMPULAN_AKHIR'] == 1){
                $tes = 'Lulus Tes';
            }elseif($row['ID_JENJANG'] == 2 or $row['ID_JENJANG'] == 3 or $row['ID_JENJANG'] == 9 or $row['ID_JENJANG'] == 10){
                $tes = 'Lulus Tes';
            }else{
                $tes = 'Tidak Lulus Tes';
            }*/
            
            
            // Tidak Ada Kebutuhan di Langitan
            $verifikasi = 'Sudah Verifikasi';
            /*if($row['ID_VERIFIKATOR_PENDIDIKAN'] != ''){
                $verifikasi = 'Sudah Verifikasi';
            }else{
                $verifikasi = 'Belum Verifikasi';
            }*/
            
            // Status Generate
            if ($row['NIM_MHS'] != '') {
                $tes = 'Sudah Tergenerate';
            }
            
                    // pembayaran cmhs tidak ada kebutuhan di langitan
                    /*$db->Query("select pembayaran_cmhs.id_c_mhs, SEMESTER.id_semester, thn_akademik_semester, nm_semester
                                from pembayaran_cmhs
                                left join semester on semester.id_semester = pembayaran_cmhs.id_semester
                                where id_c_mhs = '".$row['ID_C_MHS']."'
                                group by pembayaran_cmhs.id_c_mhs, SEMESTER.id_semester, thn_akademik_semester, nm_semester");

                    $bayar = $db->FetchAssoc();
                    $id_semester_masuk = $bayar['ID_SEMESTER'];
                    $thn_nim = substr($bayar['THN_AKADEMIK_SEMESTER'], -2);
                    */

                    $db->Query("SELECT cmb.ID_C_MHS, p.ID_SEMESTER, p.TAHUN, p.SEMESTER
                                    FROM CALON_MAHASISWA_BARU cmb 
                                    JOIN PENERIMAAN p ON p.ID_PENERIMAAN = cmb.ID_PENERIMAAN
                                    WHERE cmb.ID_C_MHS = '".$row['ID_C_MHS']."' ");
                    
                    $penerimaan = $db->FetchAssoc();

                    $thn_smt = $penerimaan['TAHUN'];
                    $nm_smt = $penerimaan['SEMESTER'];
            if ($nm_smt == 'Gasal') {
                $nm_smt = 'Ganjil';
            }

                    $id_semester_masuk = $penerimaan['ID_SEMESTER'];
                    $thn_nim = substr($penerimaan['TAHUN'], -2);

            if (empty($id_semester_masuk)) {
                $db->Query("SELECT ID_SEMESTER FROM SEMESTER WHERE THN_AKADEMIK_SEMESTER = '{$thn_smt}' AND NM_SEMESTER = '{$nm_smt}' AND ID_PERGURUAN_TINGGI = '{$id_pt_user}'");
                $id_smt = $db->FetchAssoc();

                $id_semester_masuk = $id_smt['ID_SEMESTER'];
            }

            // sudah tertangani ketika mode detail
            /*$db->Query("SELECT FORMAT_NIM_PT FROM PERGURUAN_TINGGI WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}'");
            $nim = $db->FetchAssoc();

            $format_nim = $row['FORMAT_NIM_FAKULTAS'];

            if(empty($format_nim)){
                $format_nim = $nim['FORMAT_NIM_PT'];
            }

            // kebutuhan nilai NIM MAHASISWA
            $kode_nim_fakultas = $row['KODE_NIM_FAKULTAS'];
            $kode_nim_prodi = $row['KODE_NIM_PRODI'];
            $kode_nim_angkatan = $thn_nim;*/
            if (empty($panjang_seri)) {
                $panjang_seri = 3;
            }
            if (empty($nomor_awal_seri)) {
                $nomor_awal_seri = "001";
            }

            $hasil_nim = str_replace('[Seri]', '', $hasil_nim);

            // simpan ke array
            array_push($result_set, array(
                'NO_UJIAN'      => $no_ujian,
                // dirubah sesuai format nim PT atau FAKULTAS
                //'NIM_MHS'       => str_pad($row['ID_FAKULTAS'], 2, '0', STR_PAD_LEFT) . $thn_nim . $row['KODE_PROGRAM_STUDI'],
                'ID_C_MHS'      => $row['ID_C_MHS'],
                'NIM_MHS'       => $hasil_nim,
                'NM_C_MHS'      => $row['NM_C_MHS'],
                'JENIS_KELAMIN' => $row['JENIS_KELAMIN'],
                'ID_AGAMA'      => $row['ID_AGAMA'],
                'TGL_LAHIR'     => $row['TGL_LAHIR'],
                'ID_KOTA_LAHIR' => $row['ID_KOTA_LAHIR'],
                'TAHUN'         => $row['TAHUN'],
                'EMAIL'         => $row['EMAIL'],
                'ID_PENGGUNA'   => '',
                'ID_MHS'        => '',
                'RESULT'        => $tes,
                'VERIFIKASI'    => $verifikasi
            ));

        }

        // Mendapatkan nomer terakhir yg paling besar + 1
        $db->Query("SELECT MAX(NIM_MHS) NIM_MHS 
                        FROM MAHASISWA m 
                        JOIN PENGGUNA p ON p.ID_PENGGUNA = m.ID_PENGGUNA AND p.ID_PERGURUAN_TINGGI = '{$id_pt_user}' 
                        WHERE NIM_MHS LIKE '{$result_set[0]['NIM_MHS']}%'");
        
        $row = $db->FetchAssoc();
        $len = strlen($result_set[0]['NIM_MHS']);
       
        /*if ($row['NIM_MHS'] != '')
            $no_seri = substr($row['NIM_MHS'], $len, 9 - $len);
        else
            $no_seri = str_pad('0', 9 - $len, '0', STR_PAD_LEFT);*/

        $db->BeginTransaction();
        
        foreach ($result_set as &$result) {
            if ($result['RESULT'] == 'Lulus Tes') {
                if ($result['VERIFIKASI'] == 'Sudah Verifikasi') {
                // tambah satu
                //$no_seri = str_pad(((int)$no_seri) + 1, 9 - $len, '0', STR_PAD_LEFT);

                    $no_seri = str_pad($nomor_awal_seri, $panjang_seri, '0', STR_PAD_LEFT);
                
                // memasukan hasil generate sementara ke nim
                    $result['NIM_MHS'] = $result['NIM_MHS'] . $no_seri;

                // simpan ke tabel CMB
                    $hasil = $db->Query(
                        "UPDATE calon_mahasiswa_baru set 
                            nim_mhs = '{$result['NIM_MHS']}', 
                            tgl_generate_nim = to_date('".date('YmdHis')."', 'YYYYMMDDHH24MISS') 
                        where id_c_mhs = '{$result['ID_C_MHS']}'");
                        
                    if ($hasil) {
                        $result['RESULT'] = "Generate Berhasil";

                        $nomor_awal_seri++;
                    }
                } else {
                     $result['NIM_MHS'] = '';
                    $result['RESULT'] = $result['VERIFIKASI'];
                }
            } else {
                // hilangkan generate
                $result['NIM_MHS'] = '';
            }
        }
        
        foreach ($result_set as &$result) {
            if ($result['RESULT'] == 'Generate Berhasil') {
                // ambil ID dari seq
                $db->Query("select PENGGUNA_SEQ.nextval AS ID_PENGGUNA from dual");
                $pengguna = $db->FetchAssoc();

                $id_pengguna = $pengguna['ID_PENGGUNA'];

                /* CARA GENERATE EMAIL MHS DI TABEL PENGGUNA */
                /* FIKRIE 30-07-2018 */
                // ambil format email mhs dari tabel perguruan_tinggi
                $db->Query("SELECT FORMAT_EMAIL_MHS FROM PERGURUAN_TINGGI WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}'");
                $format = $db->FetchAssoc();

                $format_email_mhs = $format['FORMAT_EMAIL_MHS'];

                $kata = strtolower($result['NM_C_MHS']);
                $kata_pisah = explode(' ', $kata);

                $kata_gabung = "";

                for ($i=0; $i < str_word_count($kata); $i++) {
                    if ($kata_gabung != "") {
                        $kata_gabung = $kata_gabung."_".$kata_pisah[$i];
                    } else {
                        $kata_gabung = $kata_pisah[$i];
                    }
                }

                // return pertama m_fikrie_ramadhan@student.umaha.ac.id
                $email_pengguna = $kata_gabung.$format_email_mhs;

                // cari email yg sama di PT yg sama dan khusus Pengguna yg MAHASISWA
                $db->Query("SELECT * 
                                FROM PENGGUNA 
                                WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' 
                                    AND JOIN_TABLE = 3 
                                    AND EMAIL_PENGGUNA = '{$email_pengguna}'");
                $ada = $db->NumRows();

                // checking pertama (m_fikrie_ramadhan@student.umaha.ac.id) ke (m_fikrie_ramadhan18@student.umaha.ac.id)
                if ($ada > 0) {
                    $angkatan_mhs = substr($result['TAHUN'], 2);

                    // return kedua m_fikrie_ramadhan18@student.umaha.ac.id
                    $email_pengguna = $kata_gabung.$angkatan_mhs.$format_email_mhs;

                    // cari email yg sama di PT yg sama dan khusus Pengguna yg MAHASISWA
                    $db->Query("SELECT * 
                                    FROM PENGGUNA 
                                    WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' 
                                        AND JOIN_TABLE = 3 
                                        AND EMAIL_PENGGUNA = '{$email_pengguna}'");
                    $ada = $db->NumRows();

                    $kata_pisah_singkat = explode(' ', $kata);

                    $kata_gabung_singkat = "";

                    // checking kedua (m_fikrie_ramadhan18@student.umaha.ac.id) ke (m_f_ramadhan@student.umaha.ac.id)
                    if ($ada > 0) {
                        for ($i=0; $i < str_word_count($kata)-1; $i++) {
                            if ($kata_gabung_singkat != "") {
                                $kata_gabung_singkat = $kata_gabung_singkat."_".substr($kata_pisah_singkat[$i], 0, 1);
                            } else {
                                $kata_gabung_singkat = substr($kata_pisah_singkat[$i], 0, 1);
                            }
                        }

                        $kata_gabung_singkat = $kata_gabung_singkat."_".$kata_pisah_singkat[str_word_count($kata)-1];

                        // return ketiga m_f_ramadhan@student.umaha.ac.id
                        $email_pengguna = $kata_gabung_singkat.$format_email_mhs;

                        // cari email yg sama di PT yg sama dan khusus Pengguna yg MAHASISWA
                        $db->Query("SELECT * 
                                        FROM PENGGUNA 
                                        WHERE ID_PERGURUAN_TINGGI = '{$id_pt_user}' 
                                            AND JOIN_TABLE = 3 
                                            AND EMAIL_PENGGUNA = '{$email_pengguna}'");
                        $ada = $db->NumRows();

                        // checking ketiga (m_f_ramadhan@student.umaha.ac.id) ke (m_f_ramadhan18@student.umaha.ac.id)
                        if ($ada > 0) {
                            // return keempat m_f_ramadhan18@student.umaha.ac.id
                            $email_pengguna = $kata_gabung_singkat.$angkatan_mhs.$format_email_mhs;
                        }
                    }
                }

                /* END CARA GENERATE EMAIL MHS DI TABEL PENGGUNA */


                
                
                // insert ke pengguna
                $db->Parse(
                    "INSERT into pengguna
                    ( id_pengguna, username, se1, nm_pengguna, id_agama, kelamin_pengguna, email_alternate, email_pengguna, tgl_lahir_pengguna, tempat_lahir, password_hash, id_role, join_table,id_perguruan_tinggi) values
                    (:id_pengguna,:username,:se1,:nm_pengguna,:id_agama,:kelamin_pengguna,:email_alternate,:email_pengguna,:tgl_lahir_pengguna,:tempat_lahir,:password_hash,3,3,:id_perguruan_tinggi)");
                $db->BindByName(':id_pengguna', $id_pengguna);
                $db->BindByName(':username', $result['NIM_MHS']);
                $db->BindByName(':se1', $user->Encrypt($result['NIM_MHS']));
                $db->BindByName(':nm_pengguna', strtoupper($result['NM_C_MHS']));
                $db->BindByName(':id_agama', $result['ID_AGAMA']);
                $db->BindByName(':kelamin_pengguna', $result['JENIS_KELAMIN']);
                $db->BindByName(':email_alternate', $result['EMAIL']);
                $db->BindByName(':email_pengguna', $email_pengguna);
                $db->BindByName(':tgl_lahir_pengguna', $result['TGL_LAHIR']);
                $db->BindByName(':tempat_lahir', $result['ID_KOTA_LAHIR']);
                $db->BindByName(':password_hash', sha1($result['NIM_MHS']));
                $db->BindByName(':id_perguruan_tinggi', $id_pt_user);
                $hasil1 = $db->Execute();

                // Debugging
                if ( ! $hasil1) {
                    $db->Rollback();
                    echo '<pre>'.__LINE__.': '.print_r($db->Error, true).'</pre>'; 
                    exit();
                }

                // proses insert ke mahasiswa
                if ($hasil1) {
                    // mendapatkan id_pengguna
                    /*$db->Query("select id_pengguna from pengguna where username = '{$result['NIM_MHS']}' and id_perguruan_tinggi = '{$id_pt_user}'");
                    $row = $db->FetchAssoc();
                    $result['ID_PENGGUNA'] = $row['ID_PENGGUNA'];
                    */
                    $result['ID_PENGGUNA'] = $id_pengguna;

                    $db->Query("SELECT MAHASISWA_SEQ.nextval AS ID_MHS from dual");
                    $mhs = $db->FetchAssoc();

                    $id_mhs = $mhs['ID_MHS'];

                    // insert ke mahasiswa
                    $hasil2 = $db->Query(
                        "INSERT into mahasiswa (id_mhs, id_pengguna, id_c_mhs, nim_mhs, nim_lama, nim_bank, id_program_studi, thn_angkatan_mhs, id_kelompok_biaya, id_prodi_minat, status_akademik_mhs, id_semester_masuk, STATUS_CEKAL,
                            lahir_prop_mhs, lahir_kota_mhs, 
                            --alamat_mhs, 
                            mobile_mhs, 
                            id_sekolah_asal_mhs, no_ijazah, thn_lulus_mhs, nilai_skhun_mhs, nem_pelajaran_mhs, nm_ayah_mhs, pendidikan_ayah_mhs, pekerjaan_ayah_mhs, 
                            alamat_ayah_mhs, nm_ibu_mhs, pendidikan_ibu_mhs, pekerjaan_ibu_mhs, alamat_ibu_mhs, ASAL_KOTA_MHS, 
                            ASAL_PROV_MHS, ALAMAT_AYAH_MHS_PROV,
                            ALAMAT_AYAH_MHS_KOTA, ALAMAT_IBU_MHS_PROV,ALAMAT_IBU_MHS_KOTA, 
                            ALAMAT_ASAL_MHS, ALAMAT_RT_ASAL_MHS, ALAMAT_RW_ASAL_MHS, ALAMAT_DUSUN_ASAL_MHS, ALAMAT_KELURAHAN_ASAL_MHS, ALAMAT_KODEPOS_ASAL, ALAMAT_KECAMATAN_ASAL_MHS,
                            ALAMAT_ASAL_MHS_PROV, ALAMAT_ASAL_MHS_KOTA, TELP_AYAH_MHS, TELP_IBU_MHS, TGL_LAHIR_AYAH_MHS, 
                            TGL_LAHIR_IBU_MHS, ID_DISABILITAS_AYAH_MHS, ID_DISABILITAS_IBU_MHS, PENGHASILAN_AYAH_MHS, PENGHASILAN_IBU_MHS, NM_WALI_MHS, 
                            PENDIDIKAN_WALI_MHS, PEKERJAAN_WALI_MHS, ALAMAT_WALI_MHS, ALAMAT_WALI_MHS_KOTA, 
                            ALAMAT_WALI_MHS_PROV, PENGHASILAN_WALI_MHS, TELP_WALI_MHS, TGL_LAHIR_WALI_MHS, 
                            ID_DISABILITAS_WALI_MHS,
                            nik_mhs, nm_jenis_tinggal, kewarganegaraan, nisn, 
                            nomor_kps, sumber_biaya, tgl_terdaftar_mhs)
                            select {$id_mhs} id_mhs, p.id_pengguna, cmb.id_c_mhs, p.username, p.username, p.username, cmb.id_program_studi, pen.tahun, cmb.id_kelompok_biaya, cmp.id_prodi_minat, 1 status_akademik_mhs, {$id_semester_masuk} id_semester_masuk, 0 STATUS_CEKAL,
                                (select k.id_provinsi from kota k where k.id_kota = cmb.id_kota_lahir), cmb.id_kota_lahir, 
                                --(cmb.ALAMAT_JALAN || ', ' || cmb.ALAMAT_DUSUN || ', ' || cmb.ALAMAT_KELURAHAN || ', ' || cmb.ALAMAT_KECAMATAN), 
                                cmb.NOMOR_HP, 
                                cms.ID_SEKOLAH_ASAL, cms.NO_IJAZAH, cms.TAHUN_LULUS, cms.NILAI_UAN, cms.JUMLAH_PELAJARAN_UAN, 
                                cmo.NAMA_AYAH, cmo.PENDIDIKAN_AYAH, cmo.PEKERJAAN_AYAH, 
                                cmo.ALAMAT_AYAH, cmo.NAMA_IBU, cmo.PENDIDIKAN_IBU, cmo.PEKERJAAN_IBU, cmo.ALAMAT_IBU, cmb.ID_KOTA, 
                                (select k.id_provinsi from kota k where k.id_kota = cmb.id_kota), (select k.id_provinsi from kota k where k.id_kota = cmo.id_kota_ayah), 
                                cmo.id_kota_ayah, (select k.id_provinsi from kota k where k.id_kota = cmo.id_kota_ibu), cmo.id_kota_ibu, 
                                cmb.ALAMAT_JALAN, cmb.ALAMAT_RT, cmb.ALAMAT_RW, cmb.ALAMAT_DUSUN, cmb.ALAMAT_KELURAHAN, cmb.ALAMAT_KODEPOS, cmb.ALAMAT_KECAMATAN,
                                (select k.id_provinsi from kota k where k.id_kota = cmb.id_kota), cmb.id_kota, cmo.TELP_AYAH, cmo.TELP_IBU, cmo.TGL_LAHIR_AYAH, 
                                cmo.TGL_LAHIR_IBU, cmo.ID_DISABILITAS_AYAH, cmo.ID_DISABILITAS_IBU, cmo.PENGHASILAN_AYAH, cmo.PENGHASILAN_IBU, cmo.NAMA_WALI, 
                                cmo.PENDIDIKAN_WALI, cmo.PEKERJAAN_WALI, cmo.ALAMAT_WALI, cmo.ID_KOTA_WALI, 
                                (select k.id_provinsi from kota k where k.id_kota = cmo.id_kota_wali), cmo.PENGHASILAN_WALI, cmo.TELP_WALI, cmo.TGL_LAHIR_WALI, 
                                cmo.ID_DISABILITAS_WALI,
                                cmb.nik_c_mhs, cmb.nm_jenis_tinggal, cmb.kewarganegaraan, cms.nisn, 
                                cmb.nomor_kps, cmb.sumber_biaya, to_date(cmb.tgl_generate_nim, 'YYYY-MM-DD')
                            from pengguna p 
                            right join calon_mahasiswa_baru cmb on cmb.nim_mhs = p.username
                            left join penerimaan pen on pen.id_penerimaan = cmb.id_penerimaan
                            left join calon_mahasiswa_pasca cmp on cmp.id_c_mhs = cmb.id_c_mhs
                            left join calon_mahasiswa_ortu cmo on cmo.id_c_mhs = cmb.id_c_mhs
                            left join calon_mahasiswa_sekolah cms on cms.id_c_mhs = cmb.id_c_mhs
                            where p.id_pengguna = '{$result['ID_PENGGUNA']}' ");
                    // Debugging
                    if ( ! $hasil2) {
                        $db->Rollback();
                        echo '<pre>'.__LINE__.': '.print_r($db->Error, true).'</pre>'; 
                        exit();
                    }
                    
                } else {
                    $result['RESULT'] = 'Gagal Insert Pengguna';
                    $db->Rollback();
                }
                
                // proses insert jalur
                if ($hasil2) {
                    // mendapatkan id_mhs
                    $result['ID_MHS'] = $id_mhs;
                    
                     //INSERT ADMISI
                    // ambil ID dari seq
                    $db->Query("select ADMISI_SEQ.nextval AS ID_ADMISI from dual");
                    $admisi = $db->FetchAssoc();

                    $id_admisi = $admisi['ID_ADMISI'];

                    // ambil ID_JALUR dari penerimaan
                    $db->Query("SELECT p.id_jalur
                            from mahasiswa m
                            join calon_mahasiswa_baru cmb on cmb.nim_mhs = m.nim_mhs
                            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan and p.id_perguruan_tinggi = '{$id_pt_user}'
                            where m.nim_mhs = '{$result['NIM_MHS']}'");
                    $jalur = $db->FetchAssoc();

                    $id_jalur = $jalur['ID_JALUR'];

                     $tgl = date('Y-m-d');

                    $hasil3 = $db->Query(
                        "INSERT INTO ADMISI (ID_ADMISI, ID_MHS, STATUS_AKD_MHS, TGL_USULAN, TGL_APV, ID_SEMESTER, STATUS_APV, CREATED_BY, ID_JALUR)
                        VALUES ('".$id_admisi."', '".$result['ID_MHS']."', '1', to_date('$tgl','YYYY-MM-DD'), to_date('$tgl','YYYY-MM-DD'), '$id_semester_masuk', '1', '$id_pengguna_user', '$id_jalur')");
                    
                    // Debugging
                    if ( ! $hasil3) {
                        $db->Rollback();
                        echo '<pre>'.__LINE__.': '.print_r($db->Error, true).'</pre>'; 
                        exit();
                    }

                    // insert ke jalur
                    $hasil4 = $db->Query(
                        "INSERT INTO jalur_mahasiswa (id_mhs, id_program_studi, id_jalur, nim_mhs, id_semester, id_admisi, is_jalur_aktif)
                            select m.id_mhs, m.id_program_studi, p.id_jalur, m.nim_mhs,
                                (select id_semester from semester s where s.id_perguruan_tinggi = '{$id_pt_user}' and  s.nm_semester = case p.semester when '-' then 'Ganjil' when 'Gasal' then 'Ganjil' else p.semester end and s.thn_akademik_semester = p.tahun) id_semester, $id_admisi, 1 is_jalur_aktif
                            from mahasiswa m
                            join calon_mahasiswa_baru cmb on cmb.nim_mhs = m.nim_mhs
                            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan and p.id_perguruan_tinggi = '{$id_pt_user}'
                            where m.nim_mhs = '{$result['NIM_MHS']}'"
                    );
                    
                    // Debugging
                    if ( ! $hasil4) {
                        $db->Rollback();
                        echo '<pre>'.__LINE__.': '.print_r($db->Error, true).'</pre>'; 
                        exit();
                    }

                } else {
                    $result['RESULT'] = 'Gagal Insert Mahasiswa';
                    $db->Rollback();
                }

                // insert ke fingerprint
                // tidak ada kebutuhan fingerprint di langitan
                if ($hasil4) {
                    //save transaksi
                    $db->Commit();
                } else {
                    $result['RESULT'] = "Gagal Insert Jalur <br/>" . print_r(error_get_last(), true);
                    $db->Rollback();
                }
            }
        }

        $smarty->assign('result_set', $result_set);
    }
}

if ($request_method == 'GET' or $request_method == 'POST') {
    if ($mode == 'view') {
        $smarty->assign('fakultas_set', $db->QueryToArray("select * from fakultas where id_perguruan_tinggi = '{$id_pt_user}'"));
    }
    
    if ($mode == 'prodi') {
        $id_fakultas = get('id_fakultas');
        
        $db->Query("select * from fakultas where id_fakultas = {$id_fakultas}");
        $row = $db->FetchAssoc();
        $smarty->assign('fakultas', $row);
        
        $smarty->assign('program_studi_set', $db->QueryToArray("
            select id_program_studi, nm_jenjang, nm_program_studi as nm_program_studi
            from program_studi ps
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where ps.id_fakultas = {$id_fakultas}
            order by j.id_jenjang"));
    }
    
    if ($mode == 'detail') {
        $id_program_studi = get('id_program_studi');

        $tahun = date('Y');
        
        $db->Query("
            select id_program_studi, nm_jenjang || ' ' || nm_program_studi as nm_program_studi, id_fakultas
            from program_studi ps
            join jenjang j on j.id_jenjang = ps.id_jenjang
            where ps.id_program_studi = {$id_program_studi}");
        $row = $db->FetchAssoc();
        $smarty->assign('program_studi', $row);

        $cmb_set = $db->QueryToArray("
            select cmb.id_c_mhs, m.nim_mhs, cmb.no_ujian, cmb.nm_c_mhs,
                (select count(*) from pembayaran_cmhs pc where pc.id_c_mhs = cmb.id_c_mhs and tgl_bayar is not null) as pembayaran,
                p.nm_penerimaan||' '||p.tahun||' Gel. '||p.gelombang||' Smt. '||p.semester penerimaan,
                p.nm_penerimaan||' '||p.tahun||' Gel. '||p.gelombang penerimaan_non_pasca,
                (select nm_jalur from jalur j where j.id_jalur = p.id_jalur) nm_jalur, p.tahun, p.id_jenjang,
                (select j.nm_jenjang||' - '||ps.nm_program_studi from program_studi ps 
                	join jenjang j on j.id_jenjang = ps.id_jenjang 
                	where ps.id_program_studi = m.id_program_studi) as nm_program_studi,
                png.nm_pengguna, m.id_program_studi as id_program_studi_mhs, cmb.id_program_studi as id_program_studi_cmb
            from calon_mahasiswa_baru cmb
            left join mahasiswa m on m.id_c_mhs = cmb.id_c_mhs
            left join pengguna png on png.id_pengguna = m.id_pengguna
            join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
            where cmb.id_program_studi = {$id_program_studi} 
            and p.tahun = {$tahun}
            order by m.nim_mhs nulls first, cmb.no_ujian asc");
        $smarty->assign("cmb_set", $cmb_set);

        $db->Query(
            "SELECT ps.kode_nim_prodi, f.kode_nim_fakultas, f.format_nim_fakultas, pt.format_nim_pt 
            from program_studi ps 
            join fakultas f on f.id_fakultas = ps.id_fakultas 
            join perguruan_tinggi pt on pt.id_perguruan_tinggi = f.id_perguruan_tinggi
            where ps.id_program_studi = {$id_program_studi}"
        );
        $kode_nim = $db->FetchAssoc();

        $format_nim = $kode_nim['FORMAT_NIM_FAKULTAS'];

        if (empty($format_nim)) {
            $format_nim = $kode_nim['FORMAT_NIM_PT'];
        }


        $thn_nim = substr($cmb_set[0]['TAHUN'], -2);

        $kata_ganti = array(
                    '[F]' => $kode_nim['KODE_NIM_FAKULTAS'],
                    '[PS]' => $kode_nim['KODE_NIM_PRODI'],
                    '[A]' => $thn_nim
                    );

        $hasil_nim = strtr($format_nim, $kata_ganti);

        $smarty->assign('hasil_nim', $hasil_nim);

        $nim_potong = str_replace('[Seri]', '', $hasil_nim);

        $db->Query(
            "SELECT MAX(NIM_MHS) NIM_MHS 
                        FROM MAHASISWA m 
                        JOIN PENGGUNA p ON p.ID_PENGGUNA = m.ID_PENGGUNA AND p.ID_PERGURUAN_TINGGI = '{$id_pt_user}' 
                        JOIN ADMISI a ON a.ID_MHS = m.ID_MHS AND a.ID_JALUR IS NOT NULL
                        JOIN JALUR j ON j.ID_JALUR = a.ID_JALUR AND j.KODE_JALUR NOT IN ('PINDAHAN','LINTAS_JALUR','ALIH_JENJANG')
                        WHERE NIM_MHS LIKE '{$nim_potong}%'"
        );
        $nim = $db->FetchAssoc();

        $nim_terakhir = $nim['NIM_MHS'];

        $smarty->assign('nim_terakhir', $nim_terakhir);
    }
    
    if ($mode == 'cari') {
        $no_ujian = get('no_ujian');
        
        $cmb_set = $db->QueryToArray("
            select cmb.id_c_mhs, m.nim_mhs, cmb.no_ujian, cmb.nm_c_mhs, cmb.id_program_studi, p.tahun,
                (select nm_jenjang || ' - ' || nm_program_studi from program_studi join jenjang j on j.id_jenjang = program_studi.id_jenjang where id_program_studi = cmb.id_program_studi) as nm_program_studi,
                (select count(*) from pembayaran_cmhs pc where pc.id_c_mhs = cmb.id_c_mhs and tgl_bayar is not null) as pembayaran,
                p.nm_penerimaan||' '||p.tahun||' Gel. '||p.gelombang||' Smt. '||p.semester penerimaan
            from calon_mahasiswa_baru cmb
            left join mahasiswa m on m.nim_mhs = cmb.nim_mhs
            left join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
            where cmb.no_ujian = '{$no_ujian}'
            order by m.nim_mhs nulls first, cmb.no_ujian asc");
        $smarty->assign('cmb', $cmb_set[0]);


        $db->Query(
            "SELECT ps.kode_nim_prodi, f.kode_nim_fakultas, f.format_nim_fakultas, pt.format_nim_pt 
            from program_studi ps 
            join fakultas f on f.id_fakultas = ps.id_fakultas 
            join perguruan_tinggi pt on pt.id_perguruan_tinggi = f.id_perguruan_tinggi
            where ps.id_program_studi = '{$cmb_set[0]['ID_PROGRAM_STUDI']}'"
        );
        $kode_nim = $db->FetchAssoc();

        $format_nim = $kode_nim['FORMAT_NIM_FAKULTAS'];

        if (empty($format_nim)) {
            $format_nim = $kode_nim['FORMAT_NIM_PT'];
        }


        $thn_nim = substr($cmb_set[0]['TAHUN'], -2);

        $kata_ganti = array(
                    '[F]' => $kode_nim['KODE_NIM_FAKULTAS'],
                    '[PS]' => $kode_nim['KODE_NIM_PRODI'],
                    '[A]' => $thn_nim
                    );

        $hasil_nim = strtr($format_nim, $kata_ganti);

        $smarty->assign('hasil_nim', $hasil_nim);

        $nim_potong = str_replace('[Seri]', '', $hasil_nim);

        $db->Query(
            "SELECT MAX(NIM_MHS) NIM_MHS 
                        FROM MAHASISWA m 
                        JOIN PENGGUNA p ON p.ID_PENGGUNA = m.ID_PENGGUNA AND p.ID_PERGURUAN_TINGGI = '{$id_pt_user}' 
                        JOIN ADMISI a ON a.ID_MHS = m.ID_MHS AND a.ID_JALUR IS NOT NULL
                        JOIN JALUR j ON j.ID_JALUR = a.ID_JALUR AND j.KODE_JALUR NOT IN ('PINDAHAN','LINTAS_JALUR','ALIH_JENJANG')
                        WHERE NIM_MHS LIKE '{$nim_potong}%'"
        );
        $nim = $db->FetchAssoc();

        $nim_terakhir = $nim['NIM_MHS'];

        $smarty->assign('nim_terakhir', $nim_terakhir);
    }
}

$smarty->display("pendaftaran/nim/{$mode}.tpl");
