<?php
error_reporting (E_ALL & ~E_NOTICE);
include 'config.php';include 'class/evaluasi.class.php';

$eval = new evaluasi($db);
$id_pengguna= $user->ID_PENGGUNA; 

$smarty->assign('fakultas', $eval->fakultas());
$smarty->assign('semester', $eval->semester());
$smarty->assign('jenjang', $eval->jenjang());
$smarty->assign('status', $eval->status_rekomendasi_bws());
$smarty->assign('status_penetapan', $eval->status_penetapan());

$status_evaluasi = $db->QueryToArray("SELECT * FROM STATUS_EVALUASI WHERE JENIS_STATUS_EVALUASI = 2 ORDER BY NM_STATUS_EVALUASI ASC");
$smarty->assign('status_evaluasi', $status_evaluasi);
/*
$semester = $db->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER = 'Genap'
								ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
$smarty->assign('semester', $semester);
$fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS ORDER BY ID_FAKULTAS ASC");
$smarty->assign('fakultas', $fakultas);
*/


if(isset($_POST['status_evaluasi'])){
	
	if(isset($_POST['mode'])){
		
		$no = $_POST['no'];
		$tgl = date('d-m-Y');
		for($i=2; $i<=$no; $i++){
			$id_mhs = $_POST['id_mhs'.$i];
			$cek = $_POST['cek'.$i];
			
			if($cek == '0'){
			$db->Query("SELECT COUNT(*) AS CEK FROM MAHASISWA WHERE ID_MHS = '$id_mhs' AND STATUS_CEKAL = 1");
			$cek2 = $db->FetchAssoc();
			
			if($cek2['CEK'] == 1){
			
			$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '$_POST[semester]'");
			$semester_aktif = $db->FetchAssoc();
			
			$keterangan = "Buka Cekal Hasil Penetapan ". $semester_aktif['TAHUN_AJARAN'] . " " . $semester_aktif['NM_SEMESTER'];
				
			$db->Query("INSERT INTO CEKAL_PEMBAYARAN (ID_MHS, ID_PENGGUNA, ID_SEMESTER, TGL_UBAH, KETERANGAN, STATUS_CEKAL, JENIS_CEKAL)
					VALUES ('$id_mhs', '$id_pengguna', '$_POST[semester]', to_date('$tgl', 'DD-MM-YYYY'), '$keterangan', 0, 4)");
			$db->Query("UPDATE MAHASISWA SET STATUS_CEKAL = 0 WHERE ID_MHS = '$id_mhs'");
			
			}
			}
		}
		
	}

if($_POST['status_evaluasi'] == ''){
	$where = "";
}else{
	$where = " AND PENETAPAN_STATUS = $_POST[status_evaluasi]";
}

if($_POST['fakultas'] == ''){
	$where .= "";
}else{
	$where .= " AND FAKULTAS.ID_FAKULTAS = $_POST[fakultas]";
}



$evaluasi = $db->QueryToArray("SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, NM_FAKULTAS, 
								NM_STATUS_EVALUASI, MAHASISWA.STATUS_CEKAL
							   FROM EVALUASI_STUDI 
							   JOIN MAHASISWA ON MAHASISWA.ID_MHS = EVALUASI_STUDI.ID_MHS
							   JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
							   JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
							   JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
							   JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
							   JOIN STATUS_EVALUASI ON STATUS_EVALUASI.ID_STATUS_EVALUASI = EVALUASI_STUDI.PENETAPAN_STATUS
							   WHERE IS_AKTIF = 1 AND JENIS_EVALUASI = 4 AND ID_SEMESTER = '$_POST[semester]' $where 
							   ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI, NIM_MHS");
$smarty->assign('evaluasi', $evaluasi);

}


$smarty->display("evaluasi/buka_cekal/evaluasi-buka-cekal.tpl");
?>