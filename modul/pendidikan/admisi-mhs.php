<?php
include('config.php');

if(isset($_POST['mode'])){
	$mode = post('mode', 'index');
}else{
	$mode = get('mode', 'index');
}

$nim = trim(get('nim'));  
$pesan_error = '';

$mhs =  $db->QueryToArray("SELECT MAHASISWA.ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, NM_JENJANG, 
				NM_STATUS_PENGGUNA, STATUS_PENGGUNA.ID_STATUS_PENGGUNA, ID_ADMISI
				FROM MAHASISWA, PENGGUNA, PROGRAM_STUDI, FAKULTAS, ADMISI, JENJANG, STATUS_PENGGUNA
				WHERE MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA AND MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				AND FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS AND ADMISI.ID_MHS = MAHASISWA.ID_MHS
				AND JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG AND ADMISI.STATUS_AKD_MHS = STATUS_PENGGUNA.ID_STATUS_PENGGUNA
				AND (STATUS_APV IS NULL OR STATUS_APV = 0)");

if ($mode == 'cari')
{
	$db->Query("SELECT COUNT(ID_MHS) AS MHS FROM MAHASISWA, PENGGUNA, PROGRAM_STUDI, FAKULTAS 
				WHERE MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA AND MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				AND FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS AND (NIM_MHS = '$nim' OR NM_PENGGUNA LIKE UPPER('%$nim%'))");
	$row = $db->FetchAssoc();
	
	if($row['MHS'] > 0){
		$cari = $db->QueryToArray("SELECT ID_MHS, NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, NM_STATUS_PENGGUNA, NM_JENJANG
				FROM MAHASISWA, PENGGUNA, PROGRAM_STUDI, FAKULTAS, STATUS_PENGGUNA, JENJANG
				WHERE MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA AND MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				AND STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS AND JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				AND FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS AND (NIM_MHS = '$nim' OR NM_PENGGUNA LIKE UPPER('%$nim%'))");
		$smarty->assign('mhs', $cari);
		
	}else{
		$pesan_error = 'Mahasiswa tidak ada di database';
	}
}


elseif ($mode == 'detail')
{
	$id_mhs = get('id_mhs');
	$cari = $db->QueryToArray("SELECT NIM_MHS, NM_PENGGUNA, NM_PROGRAM_STUDI, NM_FAKULTAS, NM_JENJANG,
				NM_STATUS_PENGGUNA, THN_AKADEMIK_SEMESTER, NM_SEMESTER, ADMISI.NO_SK, TGL_SK, TGL_USULAN, TGL_APV, KETERANGAN, KURUN_WAKTU, ALASAN
				FROM MAHASISWA 
				LEFT JOIN PENGGUNA ON MAHASISWA.ID_PENGGUNA = PENGGUNA.ID_PENGGUNA   
				LEFT JOIN PROGRAM_STUDI ON MAHASISWA.ID_PROGRAM_STUDI = PROGRAM_STUDI.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS 
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				LEFT JOIN ADMISI ON MAHASISWA.ID_MHS = ADMISI.ID_MHS
				LEFT JOIN SEMESTER ON ADMISI.ID_SEMESTER = SEMESTER.ID_SEMESTER
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = ADMISI.STATUS_AKD_MHS 
				WHERE MAHASISWA.ID_MHS = $id_mhs
				ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER ASC");

	$smarty->assign('mhs', $cari);
	
	
}

elseif ($mode == 'proses')
{
	$nomer = post('nomer');  
	
	$db->Query("SELECT ID_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True'");
	$row = $db->FetchAssoc();
	
	for($i=1; $i<=$nomer; $i++){
		$sk = post('sk'.$i); 
		$app = post('app'.$i); 
		$nim = post('nim'.$i); 
		$id_mhs = post('id_mhs'.$i); 
		$id_admisi = post('id_admisi'.$i);
		$id_status_pengguna = post('id_status_pengguna'.$i); 
		
		if($app == 1){
			if($sk == ''){
				$pesan_error = $pesan_error . "NIM $nim -> SK belum diisi <br />";
			}else{
			  $db->Query("UPDATE ADMISI SET STATUS_APV = 1, NO_SK = '$sk'
						  WHERE ID_ADMISI = '$id_admisi'");
						  
				$db->Query("UPDATE MAHASISWA SET STATUS_AKADEMIK_MHS = '$id_status_pengguna'
						  WHERE ID_MHS = '$id_mhs'");
			}
			
		}
	
	}
	$smarty->assign('mhs', $mhs);
	$mode = 'index';
}


else
{
	$smarty->assign('mhs', $mhs);
}

$smarty->assign('error', $pesan_error);
$smarty->display("admisi/mhs/{$mode}.tpl");