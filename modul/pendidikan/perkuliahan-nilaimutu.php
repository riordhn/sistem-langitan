<?php
include('config.php');

// Deteksi role
if (!($user->IsLogged() && $user->Role() == AUCC_ROLE_PENDIDIKAN)) { $smarty->display('session-expired.tpl'); exit(); }

$mode = get('mode');
$id_peraturan = get('id_peraturan', 0);


if ($request_method == 'POST')
{   
    if (post('mode') == 'add')         // Add Data
    {   
		$db->Query("INSERT INTO PERATURAN_NILAI (ID_PERGURUAN_TINGGI, NILAI_MIN_PERATURAN_NILAI, NILAI_MAX_PERATURAN_NILAI, ID_STANDAR_NILAI, ID_PERATURAN)
					VALUES ({$PT->id_perguruan_tinggi}, '$_POST[nilai_min]', '$_POST[nilai_max]', '$_POST[id_standar_nilai]', '$_POST[id_peraturan]')");
    }
    else if (post('mode') == 'edit')   // Edit Data
    {
		$db->Query("UPDATE PERATURAN_NILAI SET NILAI_MIN_PERATURAN_NILAI = '$_POST[nilai_min]', NILAI_MAX_PERATURAN_NILAI = '$_POST[nilai_max]',
					ID_STANDAR_NILAI = '$_POST[id_standar_nilai]', ID_PERATURAN = '$_POST[id_peraturan]'
					WHERE ID_PERATURAN_NILAI = '$_POST[id_peraturan_nilai]' AND ID_PERGURUAN_TINGGI = {$PT->id_perguruan_tinggi}");
    }
    else if (post('mode') == 'delete')
    {
		$db->Query("DELETE PERATURAN_NILAI WHERE ID_PERATURAN_NILAI = '$_POST[id_peraturan_nilai]' AND ID_PERGURUAN_TINGGI = {$PT->id_perguruan_tinggi}");
    }
}

// View Halaman
if ($mode == '')
{   
    $peraturan = $db->QueryToArray("SELECT NM_STANDAR_NILAI, NILAI_STANDAR_NILAI, NILAI_MIN_PERATURAN_NILAI, 
					NILAI_MAX_PERATURAN_NILAI, ID_PERATURAN_NILAI
					FROM PERATURAN_NILAI
					JOIN PERATURAN ON PERATURAN.ID_PERATURAN = PERATURAN_NILAI.ID_PERATURAN
					JOIN STANDAR_NILAI ON STANDAR_NILAI.ID_STANDAR_NILAI = PERATURAN_NILAI.ID_STANDAR_NILAI
                    WHERE PERATURAN_NILAI.ID_PERGURUAN_TINGGI = {$PT->id_perguruan_tinggi}
                    ORDER BY NILAI_MIN_PERATURAN_NILAI DESC");
    
    $smarty->assign('peraturan', $peraturan);
}
else if ($mode == 'add')
{   
	$standar_nilai_set = $db->QueryToArray("SELECT * FROM STANDAR_NILAI ORDER BY 2");
    $smarty->assign('standar_nilai_set', $standar_nilai_set);
	
	$peraturan = $db->QueryToArray("SELECT * FROM PERATURAN");
    $smarty->assign('peraturan', $peraturan);
}
else if ($mode == 'edit' or $mode == 'delete')
{
    $standar_nilai_set = $db->QueryToArray("SELECT * FROM STANDAR_NILAI ORDER BY 2");
    $smarty->assign('standar_nilai_set', $standar_nilai_set);
	
	$peraturan = $db->QueryToArray("SELECT * FROM PERATURAN");
    $smarty->assign('peraturan', $peraturan);

	$db->Query("SELECT NM_STANDAR_NILAI, NILAI_STANDAR_NILAI, NILAI_MIN_PERATURAN_NILAI, NM_PERATURAN, 
					NILAI_MAX_PERATURAN_NILAI, ID_PERATURAN_NILAI, PERATURAN_NILAI.ID_STANDAR_NILAI, PERATURAN.ID_PERATURAN
					FROM PERATURAN_NILAI
					JOIN PERATURAN ON PERATURAN.ID_PERATURAN = PERATURAN_NILAI.ID_PERATURAN
					JOIN STANDAR_NILAI ON STANDAR_NILAI.ID_STANDAR_NILAI = PERATURAN_NILAI.ID_STANDAR_NILAI
					WHERE ID_PERATURAN_NILAI = '$_GET[id]' AND PERATURAN_NILAI.ID_PERGURUAN_TINGGI = {$PT->id_perguruan_tinggi}");
    $peraturan_nilai = $db->FetchAssoc();
    $smarty->assign('peraturan_nilai', $peraturan_nilai);

}

if ($mode == '')
    $smarty->display('perkuliahan/nilaimutu/view.tpl');
else if ($mode == 'add')
    $smarty->display('perkuliahan/nilaimutu/add.tpl');
else if ($mode == 'edit')
    $smarty->display('perkuliahan/nilaimutu/edit.tpl');
else if ($mode == 'delete')
    $smarty->display('perkuliahan/nilaimutu/delete.tpl');
?>