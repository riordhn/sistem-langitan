<?php
include('config.php');
$mode = get('mode', 'view');
$id_pt = $id_pt_user;
$id_fakultas = get('id_fakultas', 0);
$id_jadwal_jam = get('id_jadwal_jam', 0);


if ($request_method == 'POST')
{
	if (post('mode') == 'add')
    {
        $db->Query("INSERT INTO JADWAL_JAM (ID_FAKULTAS, NM_JADWAL_JAM, JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI)
					VALUES 
		('$_POST[id_fakultas]','$_POST[nm_jadwal_jam]','$_POST[jam_mulai]','$_POST[menit_mulai]','$_POST[jam_selesai]','$_POST[menit_selesai]')");
    }
    else if (post('mode') == 'edit')
    {
		$db->Query("UPDATE JADWAL_JAM SET NM_JADWAL_JAM = '{$_POST[nm_jadwal_jam]}', 
					JAM_MULAI = '{$_POST[jam_mulai]}', MENIT_MULAI = '{$_POST[menit_mulai]}',
					JAM_SELESAI = '{$_POST[jam_selesai]}', MENIT_SELESAI = '{$_POST[menit_selesai]}'
					WHERE ID_JADWAL_JAM = '{$_POST[id_jadwal_jam]}'");
    }
    else if (post('mode') == 'delete')
    {
        $db->Query("DELETE JADWAL_JAM 
					WHERE ID_JADWAL_JAM = '$_POST[id_jadwal_jam]'");
    }
}

if ($mode == 'view')
{
    $fakultas_set = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = {$id_pt} ORDER BY ID_FAKULTAS");
    $smarty->assign('fakultas_set', $fakultas_set);
}
else if ($mode == 'jadwal')
{
    $fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS JOIN JADWAL_JAM ON JADWAL_JAM.ID_FAKULTAS = FAKULTAS.ID_FAKULTAS 
				WHERE FAKULTAS.ID_FAKULTAS = '$id_fakultas' ORDER BY JAM_MULAI, MENIT_MULAI, JAM_SELESAI, MENIT_SELESAI");
    $smarty->assign('fakultas', $fakultas);
}
else if ($mode == 'add')
{
    $db->Query("SELECT * FROM FAKULTAS WHERE ID_FAKULTAS = '$id_fakultas' AND ID_PERGURUAN_TINGGI = {$PT->ID_PERGURUAN_TINGGI} ORDER BY ID_FAKULTAS");
    $fakultas = $db->FetchAssoc();
    $smarty->assign('fakultas', $fakultas);
}
else if ($mode == 'edit' or $mode == 'delete')
{
	$db->Query("SELECT * FROM FAKULTAS JOIN JADWAL_JAM ON JADWAL_JAM.ID_FAKULTAS = FAKULTAS.ID_FAKULTAS 
				WHERE ID_JADWAL_JAM = '$id_jadwal_jam' ORDER BY FAKULTAS.ID_FAKULTAS");
	$jadwal_jam = $db->FetchAssoc();
    $smarty->assign('jadwal_jam', $jadwal_jam);
}

$smarty->display("perkuliahan/jam/{$mode}.tpl");
?>
