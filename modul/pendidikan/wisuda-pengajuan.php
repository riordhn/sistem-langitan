<?php
include 'config.php';

$periode = $db->QueryToArray("SELECT * FROM TARIF_WISUDA ORDER BY ID_TARIF_WISUDA DESC");
$smarty->assign('periode', $periode);

$nim = $_POST['nim'];


$error = '';
if (isset($_POST['nim']))
{

	$db->Query("SELECT * FROM MAHASISWA 
				JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
				JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
				JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				WHERE NIM_MHS = '$nim' AND STATUS_AKADEMIK_MHS = 4");
	$mhs = $db->FetchArray();

	if (!$mhs)
	{
		$error = 'Mahasiswa tidak terdaftar atau belum lulus';
	}

	$db->Query("SELECT * FROM PENGAJUAN_WISUDA WHERE ID_MHS =  '$mhs[ID_MHS]'");
	$pengajuan = $db->FetchArray();
	if ($pengajuan)
	{
		$smarty->assign('pengajuan', $pengajuan);
	}


	if ($error == '')
	{
		$smarty->assign('mhs', $mhs);
	}
}


if (isset($_POST['no_ijasah']))
{

	if ($pengajuan)
	{

		$db->Query("UPDATE PENGAJUAN_WISUDA SET
	ID_MHS = '$pengajuan[ID_MHS]', 
	ID_PERIODE_WISUDA = '$_POST[periode]', 
	NO_IJASAH = '$_POST[no_ijasah]', 
	TGL_LULUS_PENGAJUAN = TO_DATE('$_POST[tgl_lulus]', 'DD-MM-YYYY'), 
	LAHIR_IJAZAH = '$_POST[ttl]', 
	NO_SERI_KERTAS = '$_POST[no_seri]'
	WHERE ID_PENGAJUAN_WISUDA = '$pengajuan[ID_PENGAJUAN_WISUDA]'
	");
	}
	else
	{

		$db->Query("INSERT INTO PENGAJUAN_WISUDA 
	(ID_MHS, YUDISIUM, ID_PERIODE_WISUDA, NO_IJASAH, TGL_LULUS_PENGAJUAN, LAHIR_IJAZAH, NO_SERI_KERTAS, JUMLAH_CETAK)
	SELECT ID_MHS, 2, '$_POST[periode]', '$_POST[no_ijasah]', TO_DATE('$_POST[tgl_lulus]', 'DD-MM-YYYY'), '$_POST[ttl]', '$_POST[no_seri]', 1
	FROM MAHASISWA WHERE NIM_MHS = '$nim'
	");
	}

	$db->Query("SELECT NO_IJASAH, TO_CHAR(TGL_LULUS_PENGAJUAN, 'DD-MM-YYYY') AS TGL_LULUS_PENGAJUAN, LAHIR_IJAZAH, NO_SERI_KERTAS 
					FROM PENGAJUAN_WISUDA WHERE ID_MHS =  '$mhs[ID_MHS]'");
	$pengajuan = $db->FetchArray();
	$smarty->assign('pengajuan', $pengajuan);
	$error = 'Berhasil';
}

$smarty->assign('error', $error);
$smarty->display("wisuda/wisuda-pengajuan.tpl");