<?php
include('config.php');
$id_pengguna= $user->ID_PENGGUNA; 

$mode = get('mode', 'view');
$id_pt = $id_pt_user;
$ip=$_SERVER['REMOTE_ADDR'];
$tgl = date('d-m-Y');

if ($request_method == 'POST')
{
    if (post('mode') == 'delete')
    {
		$id_pengambilan_mk_set	= post('id_pengambilan_mk', array());
		
		$id_pengambilan_mk	= implode(',', $id_pengambilan_mk_set);

		$result = $db->Query("DELETE FROM NILAI_MK WHERE ID_PENGAMBILAN_MK IN ({$id_pengambilan_mk})") or die("salah query baris " . __LINE__);
		$result = $db->Query("DELETE FROM PENGAMBILAN_MK WHERE ID_PENGAMBILAN_MK IN ({$id_pengambilan_mk})") or die("salah query baris " . __LINE__);

		$smarty->assign('edited', $result ? "KRS berhasil dihapus" : "KRS gagal dihapus"); 
    }
}


if ($mode == 'view')
{
	$prodi = $db->QueryToArray("SELECT * FROM PROGRAM_STUDI ps 
										JOIN FAKULTAS f ON f.ID_FAKULTAS = ps.ID_FAKULTAS 
										JOIN JENJANG j ON j.ID_JENJANG = ps.ID_JENJANG
										WHERE f.ID_PERGURUAN_TINGGI = {$id_pt} ORDER BY f.ID_FAKULTAS, ps.NM_PROGRAM_STUDI ASC");
    $smarty->assign('prodi', $prodi);
	
	if($_POST['prodi'] <> ''){
		$kelas_set = $db->QueryToArray("SELECT DISTINCT pmk.ID_KELAS_MK, nk.NAMA_KELAS, j.NM_JENJANG || ' - ' || ps.NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI, 
										mk.KD_MATA_KULIAH, mk.NM_MATA_KULIAH, p.NM_PENGGUNA 
									FROM PENGAMBILAN_MK pmk
									JOIN KELAS_MK kmk ON kmk.ID_KELAS_MK = pmk.ID_KELAS_MK
									JOIN PROGRAM_STUDI ps ON ps.ID_PROGRAM_STUDI = kmk.ID_PROGRAM_STUDI
									JOIN JENJANG j ON j.ID_JENJANG = ps.ID_JENJANG
									JOIN NAMA_KELAS nk ON nk.ID_NAMA_KELAS = kmk.NO_KELAS_MK
									JOIN MATA_KULIAH mk ON mk.ID_MATA_KULIAH = kmk.ID_MATA_KULIAH
									JOIN PENGGUNA p ON p.ID_PENGGUNA = pmk.ID_PENGGUNA_HAPUS_KRS
									WHERE kmk.ID_PROGRAM_STUDI = '$_POST[prodi]' AND pmk.STATUS_HAPUS_KRS = 1
									ORDER BY pmk.ID_KELAS_MK");
	} else{
		$kelas_set = $db->QueryToArray("SELECT DISTINCT pmk.ID_KELAS_MK, nk.NAMA_KELAS, j.NM_JENJANG || ' - ' || ps.NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI,
										mk.KD_MATA_KULIAH, mk.NM_MATA_KULIAH, p.NM_PENGGUNA 
									FROM PENGAMBILAN_MK pmk
									JOIN KELAS_MK kmk ON kmk.ID_KELAS_MK = pmk.ID_KELAS_MK
									JOIN PROGRAM_STUDI ps ON ps.ID_PROGRAM_STUDI = kmk.ID_PROGRAM_STUDI
									JOIN JENJANG j ON j.ID_JENJANG = ps.ID_JENJANG
									JOIN NAMA_KELAS nk ON nk.ID_NAMA_KELAS = kmk.NO_KELAS_MK
									JOIN MATA_KULIAH mk ON mk.ID_MATA_KULIAH = kmk.ID_MATA_KULIAH
									JOIN PENGGUNA p ON p.ID_PENGGUNA = pmk.ID_PENGGUNA_HAPUS_KRS
									WHERE nk.ID_PERGURUAN_TINGGI = '{$id_pt}' AND pmk.STATUS_HAPUS_KRS = 1
									ORDER BY pmk.ID_KELAS_MK");
	}
    
	
	$smarty->assign('id_prodi', $_POST['prodi']);

    $smarty->assign('kelas_set', $kelas_set);
}
else if ($mode == 'detail')
{
	$id_kelas_mk = get('id_kelas_mk');

	$kelas_mk = $db->QueryToArray("SELECT nk.NAMA_KELAS, j.NM_JENJANG || ' - ' || ps.NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI,
										mk.KD_MATA_KULIAH, mk.NM_MATA_KULIAH
									FROM PENGAMBILAN_MK pmk
									JOIN KELAS_MK kmk ON kmk.ID_KELAS_MK = pmk.ID_KELAS_MK
									JOIN PROGRAM_STUDI ps ON ps.ID_PROGRAM_STUDI = kmk.ID_PROGRAM_STUDI
									JOIN JENJANG j ON j.ID_JENJANG = ps.ID_JENJANG
									JOIN NAMA_KELAS nk ON nk.ID_NAMA_KELAS = kmk.NO_KELAS_MK
									JOIN MATA_KULIAH mk ON mk.ID_MATA_KULIAH = kmk.ID_MATA_KULIAH
									WHERE kmk.ID_KELAS_MK = '{$id_kelas_mk}' AND pmk.STATUS_HAPUS_KRS = 1");
    $smarty->assign('kelas_mk', $kelas_mk[0]);


    $mahasiswa_set = $db->QueryToArray("SELECT pmk.ID_PENGAMBILAN_MK, nk.NAMA_KELAS, j.NM_JENJANG || ' - ' || ps.NM_PROGRAM_STUDI AS NM_PROGRAM_STUDI,
										mk.KD_MATA_KULIAH, mk.NM_MATA_KULIAH, mhs.ID_MHS, mhs.NIM_MHS, mhs.THN_ANGKATAN_MHS, p.NM_PENGGUNA 
									FROM PENGAMBILAN_MK pmk
									JOIN KELAS_MK kmk ON kmk.ID_KELAS_MK = pmk.ID_KELAS_MK
									JOIN PROGRAM_STUDI ps ON ps.ID_PROGRAM_STUDI = kmk.ID_PROGRAM_STUDI
									JOIN JENJANG j ON j.ID_JENJANG = ps.ID_JENJANG
									JOIN NAMA_KELAS nk ON nk.ID_NAMA_KELAS = kmk.NO_KELAS_MK
									JOIN MATA_KULIAH mk ON mk.ID_MATA_KULIAH = kmk.ID_MATA_KULIAH
									JOIN MAHASISWA mhs ON mhs.ID_MHS = pmk.ID_MHS
									JOIN PENGGUNA p ON p.ID_PENGGUNA = mhs.ID_PENGGUNA
									WHERE kmk.ID_KELAS_MK = '{$id_kelas_mk}' AND pmk.STATUS_HAPUS_KRS = 1");
    $smarty->assign('mahasiswa_set', $mahasiswa_set);
}

$smarty->display("hapus-krs/{$mode}.tpl");
?>