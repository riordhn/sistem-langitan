<?php
include('config.php');

$id_periode = $_GET['prd'];
$id_fakultas = $_GET['fak'];
$id_jenjang = $_GET['jnj'];

$rekap = $db->QueryToArray("SELECT (case when pg.gelar_depan is null then '' else pg.gelar_depan || '. ' end)gelar_depan, pg.nm_pengguna, (case when pg.gelar_belakang is 						    null then '' else ', ' || pg.gelar_belakang 	
					    end)gelar_belakang,mh.nim_mhs,
					    (case when pg.kelamin_pengguna = '2' then 'Perempuan' else 'Laki-laki' end)jk,fk.nm_fakultas,
					    ps.nm_program_studi,jj.nm_jenjang,jj.nm_jenjang_ijasah,(case when pw.lahir_ijazah is null then (case when kt.nm_kota is null 						    then 'Tidak Ada Data' else kt.nm_kota end 
					    || ', ' || 
					    case when to_char(pg.tgl_lahir_pengguna,'DD-MM-YYYY') is null then 'Tidak Ada Data' else 
					    to_char(pg.tgl_lahir_pengguna,'DD-MM-YYYY') end) else pw.lahir_ijazah end)ttl,ad.tgl_lulus as tgl_lls,
					    pw.no_ijasah,pw.no_seri_kertas,pw.tgl_posisi_ijasah,pw.judul_ta
					    FROM admisi ad
					    left join mahasiswa mh on mh.id_mhs=ad.id_mhs
					    left join pengguna pg on pg.id_pengguna=mh.id_pengguna
					    left join program_studi ps on mh.id_program_studi=ps.id_program_studi
					    left join fakultas fk on ps.id_fakultas=fk.id_fakultas
					    left join jenjang jj on ps.id_jenjang=jj.id_jenjang
					    left join kota kt on kt.id_kota=mh.lahir_kota_mhs
					    left join pengajuan_wisuda pw on pw.id_mhs=ad.id_mhs
					    where ad.status_akd_mhs=4 
					    AND PS.ID_FAKULTAS = '$id_fakultas'
					    and ad.id_mhs is not null
					    and jj.id_jenjang='$id_jenjang'
					    and to_char(ad.tgl_lulus,'YYYY')='$id_periode'
					    order by pw.no_ijasah,ps.id_program_studi,ps.id_jenjang,to_char(ad.tgl_lulus,'DD-MM-YYYY')")or die("g iso");
ob_start();
?>

<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
<table style="font-size:11px" border="1">
<thead class="fixedHeader">
<?php if($id_jenjang==3){?>
	<tr>
		<th style="text-align:center">NO</th>
		<th style="text-align:center">NAMA</th>
		<th style="text-align:center">NIM</th>
        	<th style="text-align:center">TEMPAT DAN TANGGAL LAHIR</th>
		<th style="text-align:center">PROGRAM STUDI</th>
   		<th style="text-align:center">TANGGAL LULUS</th>
		<th style="text-align:center">NO IJASAH</th>
		<th style="text-align:center">NO SERI</th>
		<th style="text-align:center">JUDUL DESERTASI</th>
        	<th style="text-align:center">TANGGAL CETAK IJASAH</th>
	</tr>
<?php } else {?>
	<tr>
		<th style="text-align:center">NO</th>
		<th style="text-align:center">NAMA</th>
		<th style="text-align:center">NIM</th>
        	<th style="text-align:center">TEMPAT DAN TANGGAL LAHIR</th>
		<th style="text-align:center">PROGRAM STUDI</th>
   		<th style="text-align:center">TANGGAL LULUS</th>
		<th style="text-align:center">NO IJASAH</th>
		<th style="text-align:center">NO SERI</th>
        	<th style="text-align:center">TANGGAL CETAK IJASAH</th>
	</tr>
<?php } ?>
</thead>
<tbody class="scrollContent">

<?php
	$no = 1;
	foreach($rekap as $data){
	echo '<tr>
		<td style="text-align:center">'.$no++.'</td>
		<td>'.$data['GELAR_DEPAN'].''.ucwords(strtolower($data['NM_PENGGUNA'])).''.$data['GELAR_BELAKANG'].'</td>
		<td>'.$data['NIM_MHS'].'</td>
		<td>'.$data['TTL'].'</td>
		<td>'.$data['NM_PROGRAM_STUDI'].'</td>
		<td>'.$data['TGL_LLS'].'</td>
		<td>'.$data['NO_IJASAH'].'</td>
		<td>'.$data['NO_SERI_KERTAS'].'</td>';
	if($data['NM_JENJANG']=='S3'){echo '<td>'.$data['JUDUL_TA'].'</td>';}
	echo '<td>'.$data['TGL_POSISI_IJASAH'].'</td>
    	     </tr>';
	}

echo '</tbody>
</table>';


$filename = 'wisuda-lls-excel-' . date('Y-m-d').'.xls';
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
