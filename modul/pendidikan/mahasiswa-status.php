<?php
include('config.php');

$mode = get('mode', 'fakultas');
$id_semester = get('id_semester', 5);  // id_semester default ke 2011/2012 ganjil
$id_fakultas = get('id_fakultas', '');

// Semuanya di mode GET

if ($mode == 'fakultas')
{
	// pilihan semester
	$smarty->assign('semester_set', $db->QueryToArray("SELECT * FROM SEMESTER ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER"));
	$smarty->assign('id_semester', $id_semester);
	
	// data per fakultas yang bayar
	$fakultas_set = $db->QueryToArray("
		select f1.*, f2.jumlah_non_aktif from (
		  select 
			f.id_fakultas, f.nm_fakultas, count(b.id_mhs) as jumlah_aktif
		  from (
			select b.id_mhs, 
			  case j_pembayaran
				when 0 then
				  case j_pembayaran_cmhs when 0 then 0
				  else 1 end
				else 1
			  end as status_bayar
			from (
			  select m.id_mhs, thn_angkatan_mhs,
				(select count(*) from pembayaran p where tgl_bayar is not null and p.id_mhs = m.id_mhs and p.id_semester = {$id_semester}) as j_pembayaran,
				(select count(*) from pembayaran_cmhs pc where tgl_bayar is not null and pc.id_c_mhs = m.id_c_mhs and pc.id_semester = {$id_semester}) as j_pembayaran_cmhs
			  from mahasiswa m) b ) b
		  join mahasiswa m on m.id_mhs = b.id_mhs
		  join program_studi ps on ps.id_program_studi = m.id_program_studi
		  join fakultas f on f.id_fakultas = ps.id_fakultas
		  where b.status_bayar = 1
		  group by f.id_fakultas, f.nm_fakultas) f1
		join (
			select 
			f.id_fakultas, f.nm_fakultas, count(b.id_mhs) as jumlah_non_aktif
		  from (
			select b.id_mhs, 
			  case j_pembayaran
				when 0 then
				  case j_pembayaran_cmhs when 0 then 0
				  else 1 end
				else 1
			  end as status_bayar
			from (
			  select m.id_mhs, thn_angkatan_mhs,
				(select count(*) from pembayaran p where tgl_bayar is not null and p.id_mhs = m.id_mhs and p.id_semester = {$id_semester}) as j_pembayaran,
				(select count(*) from pembayaran_cmhs pc where tgl_bayar is not null and pc.id_c_mhs = m.id_c_mhs and pc.id_semester = {$id_semester}) as j_pembayaran_cmhs
			  from mahasiswa m) b ) b
		  join mahasiswa m on m.id_mhs = b.id_mhs
		  join program_studi ps on ps.id_program_studi = m.id_program_studi
		  join fakultas f on f.id_fakultas = ps.id_fakultas
		  where b.status_bayar = 0
		  group by f.id_fakultas, f.nm_fakultas) f2 on f2.id_fakultas = f1.id_fakultas
		order by f1.id_fakultas");
	
	$smarty->assign('fakultas_set', $fakultas_set);
}

if ($mode == 'prodi' && $id_fakultas != '')
{
	// nama semester
	$db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = {$id_semester}");
	$row = $db->FetchAssoc();
	$smarty->assign('semester', $row);
	
	$db->Query("SELECT * FROM FAKULTAS WHERE ID_FAKULTAS = {$id_fakultas}");
	$row = $db->FetchAssoc();
	$smarty->assign('nm_fakultas', $row['NM_FAKULTAS']);

	// data per prodi
	$program_studi_set = $db->QueryToArray("
		select ps1.*, ps2.jumlah_non_aktif from (
		  select ps.id_program_studi, ps.nm_program_studi, j.id_jenjang, j.nm_jenjang, count(b.id_mhs) as jumlah_aktif from (
			select b.id_mhs, 
			  case j_pembayaran
				when 0 then
				  case j_pembayaran_cmhs when 0 then 0
				  else 1 end
				else 1
			  end as status_bayar
			from (
			  select m.id_mhs, thn_angkatan_mhs,
				(select count(*) from pembayaran p where tgl_bayar is not null and p.id_mhs = m.id_mhs and p.id_semester = {$id_semester}) as j_pembayaran,
				(select count(*) from pembayaran_cmhs pc where tgl_bayar is not null and pc.id_c_mhs = m.id_c_mhs and pc.id_semester = {$id_semester}) as j_pembayaran_cmhs
			  from mahasiswa m) b) b
		  join mahasiswa m on m.id_mhs = b.id_mhs
		  join program_studi ps on ps.id_program_studi = m.id_program_studi
		  join jenjang j on j.id_jenjang = ps.id_jenjang
		  where ps.id_fakultas = {$id_fakultas} and b.status_bayar = 1
		  group by ps.id_program_studi, ps.nm_program_studi, j.nm_jenjang, j.id_jenjang) ps1
		join (
		  select ps.id_program_studi, ps.nm_program_studi, j.id_jenjang, j.nm_jenjang, count(b.id_mhs) as jumlah_non_aktif from (
			select b.id_mhs, 
			  case j_pembayaran
				when 0 then
				  case j_pembayaran_cmhs when 0 then 0
				  else 1 end
				else 1
			  end as status_bayar
			from (
			  select m.id_mhs, thn_angkatan_mhs,
				(select count(*) from pembayaran p where tgl_bayar is not null and p.id_mhs = m.id_mhs and p.id_semester = {$id_semester}) as j_pembayaran,
				(select count(*) from pembayaran_cmhs pc where tgl_bayar is not null and pc.id_c_mhs = m.id_c_mhs and pc.id_semester = {$id_semester}) as j_pembayaran_cmhs
			  from mahasiswa m) b) b
		  join mahasiswa m on m.id_mhs = b.id_mhs
		  join program_studi ps on ps.id_program_studi = m.id_program_studi
		  join jenjang j on j.id_jenjang = ps.id_jenjang
		  where ps.id_fakultas = {$id_fakultas} and b.status_bayar = 0
		  group by ps.id_program_studi, ps.nm_program_studi, j.nm_jenjang, j.id_jenjang) ps2 on ps2.id_program_studi = ps1.id_program_studi
		order by ps1.id_jenjang");
	$smarty->assign('program_studi_set', $program_studi_set);
}

$smarty->display("mahasiswa/jumlah/{$mode}.tpl");

/**

$semester_table = new SEMESTER_TABLE($db);
$fakultas_table = new FAKULTAS_TABLE($db);
$program_studi_table = new PROGRAM_STUDI_TABLE($db);
$mahasiswa_table = new MAHASISWA_TABLE($db);
$sejarah_status_table = new SEJARAH_STATUS_TABLE($db);
$pengguna_table = new PENGGUNA_TABLE($db);

if ($mode == 'fakultas')
{
    // Memilih fakultas dan prodi
    $semester_set = $semester_table->SelectCriteria('ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC');
    $fakultas_set = $fakultas_table->Select();
    
    // Mendapatkan id semester yang aktif
    if ($id_semester == 0)
    {
        for ($i = 0; $i < $semester_set->Count(); $i++)
        {
            if ($semester_set->Get($i)->STATUS_AKTIF_SEMESTER == 'True')
            {
                $id_semester = $semester_set->Get($i)->ID_SEMESTER;
                break;
            }
        }
    }
    
    // Mendapatkan data status mahasiswa dari database
    $fakultas_info = array();
    $db->Query("
        SELECT
            F.ID_FAKULTAS,
            F.NM_FAKULTAS,
            COUNT(SP.AKTIF_STATUS_PENGGUNA) AS JUMLAH_AKTIF,
            (
                SELECT COUNT(SP2.AKTIF_STATUS_PENGGUNA) FROM FAKULTAS F2
                JOIN PROGRAM_STUDI PS2 ON PS2.ID_FAKULTAS = F2.ID_FAKULTAS
                JOIN MAHASISWA M2 ON M2.ID_PROGRAM_STUDI = PS2.ID_PROGRAM_STUDI
                JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = M2.ID_PENGGUNA
                JOIN SEJARAH_STATUS SS2 ON SS2.ID_PENGGUNA = P2.ID_PENGGUNA
                JOIN STATUS_PENGGUNA SP2 ON SP2.ID_STATUS_PENGGUNA = SS2.ID_STATUS_PENGGUNA
                WHERE F2.ID_FAKULTAS = F.ID_FAKULTAS AND SP2.AKTIF_STATUS_PENGGUNA = 0 AND SS2.ID_SEMESTER = {$id_semester}
            ) AS JUMLAH_NONAKTIF
        FROM FAKULTAS F
        JOIN PROGRAM_STUDI PS ON PS.ID_FAKULTAS = F.ID_FAKULTAS
        JOIN MAHASISWA M ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
        JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        JOIN SEJARAH_STATUS SS ON SS.ID_PENGGUNA = P.ID_PENGGUNA
        JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = SS.ID_STATUS_PENGGUNA
        WHERE SP.AKTIF_STATUS_PENGGUNA = 1 AND SS.ID_SEMESTER = {$id_semester}
        GROUP BY F.ID_FAKULTAS, F.NM_FAKULTAS");
    while ($row = $db->FetchAssoc())
        array_push($fakultas_info, $row);
    
    // Membindingkan dengan data fakultas_set
    for ($i = 0; $i < $fakultas_set->Count(); $i++)
    {
        $exist = false;
        for ($j = 0; $j < count($fakultas_info); $j++)
        {
            if ($fakultas_set->Get($i)->ID_FAKULTAS == $fakultas_info[$j]['ID_FAKULTAS'])
            {
                $fakultas_set->Get($i)->JUMLAH_AKTIF = $fakultas_info[$j]['JUMLAH_AKTIF'];
                $fakultas_set->Get($i)->JUMLAH_NONAKTIF = $fakultas_info[$j]['JUMLAH_NONAKTIF'];
                $exist = true;
                break;
            }
        }
        
        if (!$exist)
        {
            $fakultas_set->Get($i)->JUMLAH_AKTIF = 0;
            $fakultas_set->Get($i)->JUMLAH_NONAKTIF = 0;
        }
    }

    $smarty->assign('id_semester', $id_semester);
    $smarty->assign('semester_set', $semester_set);
    $smarty->assign('fakultas_set', $fakultas_set);
}
else if ($mode == 'prodi')
{
    // Mendapatkan semester, fakultas, dan program_studi
    $semester = $semester_table->Single($id_semester);
    $fakultas = $fakultas_table->Single($id_fakultas);
    $program_studi_set = $program_studi_table->SelectWhere("ID_FAKULTAS = {$id_fakultas}");
    
    // Mendapatkan data status mahasiswa dari database
    $program_studi_info = array();
    $db->Query("
        SELECT
            PS.ID_PROGRAM_STUDI,
            PS.NM_PROGRAM_STUDI,
            COUNT(SP.AKTIF_STATUS_PENGGUNA) AS JUMLAH_AKTIF,
            (
                SELECT COUNT(SP2.AKTIF_STATUS_PENGGUNA) FROM PROGRAM_STUDI PS2
                JOIN MAHASISWA M2 ON M2.ID_PROGRAM_STUDI = PS2.ID_PROGRAM_STUDI
                JOIN PENGGUNA P2 ON P2.ID_PENGGUNA = M2.ID_PENGGUNA
                JOIN SEJARAH_STATUS SS2 ON SS2.ID_PENGGUNA = P2.ID_PENGGUNA
                JOIN STATUS_PENGGUNA SP2 ON SP2.ID_STATUS_PENGGUNA = SS2.ID_STATUS_PENGGUNA
                WHERE PS2.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI AND SP2.AKTIF_STATUS_PENGGUNA = 0 AND SS2.ID_SEMESTER = {$id_semester}
            ) AS JUMLAH_NONAKTIF
        FROM PROGRAM_STUDI PS
        JOIN MAHASISWA M ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
        JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA
        JOIN SEJARAH_STATUS SS ON SS.ID_PENGGUNA = P.ID_PENGGUNA
        JOIN STATUS_PENGGUNA SP ON SP.ID_STATUS_PENGGUNA = SS.ID_STATUS_PENGGUNA
        WHERE SP.AKTIF_STATUS_PENGGUNA = 1 AND SS.ID_SEMESTER = {$id_semester} AND PS.ID_FAKULTAS = {$id_fakultas}
        GROUP BY PS.ID_PROGRAM_STUDI, PS.NM_PROGRAM_STUDI");
    while ($row = $db->FetchAssoc())
        array_push($program_studi_info, $row);
    
    // Melakukan binding dengan data program studi
    for ($i = 0; $i < $program_studi_set->Count(); $i++)
    {
        $exist = false;
        for ($j = 0; $j < count($program_studi_info); $j++)
        {
            if ($program_studi_set->Get($i)->ID_PROGRAM_STUDI == $program_studi_info[$j]['ID_PROGRAM_STUDI'])
            {
                $program_studi_set->Get($i)->JUMLAH_AKTIF = $program_studi_info[$j]['JUMLAH_AKTIF'];
                $program_studi_set->Get($i)->JUMLAH_NONAKTIF = $program_studi_info[$j]['JUMLAH_NONAKTIF'];
                $exist = true;
                break;
            }
        }
        
        if (!$exist)
        {
            $program_studi_set->Get($i)->JUMLAH_AKTIF = 0;
            $program_studi_set->Get($i)->JUMLAH_NONAKTIF = 0;
        }
    }
    
    $smarty->assign('program_studi_set', $program_studi_set);
    $smarty->assign('semester', $semester);
    $smarty->assign('fakultas', $fakultas);
}
**/
?>
