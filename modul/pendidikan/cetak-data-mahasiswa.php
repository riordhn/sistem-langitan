<?php
include '../../config.php';
include 'class/report_mahasiswa.class.php';
include_once '../../tcpdf/config/lang/ind.php';
include_once '../../tcpdf/tcpdf.php';

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$rm = new report_mahasiswa($db);
$data_pdf = $rm->get_mahasiswa_dayat(get('fakultas'), get('program_studi'), get('tahun_akademik'), get('status'));
$pdf->SetCreator('Sistem Langitan NU');
$pdf->SetAuthor('Sistem Langitan');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage();

$html = <<<EOF
<style>
    div { margin-top: 0pt; }
    .header { font-size: 20pt; font-family: times; font-weight: bold; text-align:center;}
    .address { font-size: 14pt; font-family: serif; margin-top: 0px ;text-align:center; }
    th { 
        text-align: center;
        color: #ffffff;
        background-color: #006600;
        padding:10px;
    }
    td { font-size: 10pt; }
</style>
<table width="100%" border="0">
    <tr>
        <td width="100%" align="center">
            <span class="header">DAFTAR MAHASISWA {$nama_pt_kapital}</span>
        </td>
    </tr>
</table>
<hr/>
<p><p/>
<p><p/>
<table width="100%" cellpadding="4" border="0.5">
        <tr>
            <th width="5%">NO</th>
            <th width="12%">NIM</th>
            <th width="23%">NAMA</th>
            <th width="8%">AGK</th>
            <th width="7%">JJG</th>
            <th width="20%">PROGRAM STUDI</th>
            <th width="15%">FAKULTAS</th>
            <th width="10%">STS</th>
        </tr>
    {data_mahasiswa}
</table>
EOF;
$index = 1;
foreach ($data_pdf as $data) {
    $data_mahasiswa .=
            "<tr>
                <td>{$index}</td>
                <td>{$data['NIM_MHS']}</td>
                <td>{$data['NM_PENGGUNA']}</td>
                <td>{$data['THN_ANGKATAN_MHS']}</td>
                <td>{$data['NM_JENJANG']}</td>
                <td>{$data['NM_PROGRAM_STUDI']}</td>
                <td>{$data['NM_FAKULTAS']}</td>
                <td>{$data['NM_STATUS_PENGGUNA']}</td>
            </tr>";
    $index++;
}

$html = str_replace('{data_mahasiswa}', $data_mahasiswa, $html);

$pdf->writeHTML($html);

$pdf->Output();
?>
