<?php
include('config.php');

$mode = get('mode', 'view');
$id_pt = $id_pt_user;
$id_fakultas = get('id_fakultas', 0);


if ($request_method == 'POST')
{
    if (post('mode') == 'add')
    {	
        $db->Query("INSERT INTO FAKULTAS (NM_FAKULTAS, NM_FAKULTAS_ENG, SINGKATAN_FAKULTAS, ALAMAT_FAKULTAS, ALAMAT_FAKULTAS_ENG, KODE_NIM_FAKULTAS, ID_PERGURUAN_TINGGI) 
					VALUES ('$_POST[nm_fakultas]', '$_POST[nm_fakultas_en]', '$_POST[nm_singkat]', '$_POST[alamat]', '$_POST[alamat_en]', '$_POST[kode_nim_fakultas]', '{$id_pt}')");
	}
    else if (post('mode') == 'edit')
    {
		$db->Query("UPDATE FAKULTAS SET NM_FAKULTAS = '$_POST[nm_fakultas]', NM_FAKULTAS_ENG = '$_POST[nm_fakultas_en]' ,
					SINGKATAN_FAKULTAS = '$_POST[nm_singkat]', ALAMAT_FAKULTAS = '$_POST[alamat]', ALAMAT_FAKULTAS_ENG = '$_POST[alamat_en]', KODE_NIM_FAKULTAS = '$_POST[kode_nim_fakultas]'
					WHERE ID_FAKULTAS = '$_POST[id_fakultas]'");
    }
    else if (post('mode') == 'delete')
    {
        $db->Query("DELETE FAKULTAS WHERE ID_FAKULTAS = '$_POST[id_fakultas]'");
    }
}

if ($mode == 'view')
{
    $fakultas_set = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = {$id_pt} ORDER BY ID_FAKULTAS ASC");
    $smarty->assign('fakultas_set', $fakultas_set);
}
else if ($mode == 'edit' or $mode == 'delete')
{
    $fakultas = $db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_FAKULTAS = '$id_fakultas'");
    $smarty->assign('fakultas', $fakultas);
}

$smarty->display("perkuliahan/fakultas/{$mode}.tpl");
?>