<?php
include('config.php');
include 'class/report_mahasiswa.class.php';
$rm = new report_mahasiswa($db);
$id_pengguna= $user->ID_PENGGUNA; 

if(isset($_POST['nim']) or isset($_POST['generate'])){
		$smarty->assign('data_fakultas', $rm->get_fakultas());
		$smarty->assign('data_jalur', $rm->get_jalur());
		$smarty->assign('data_jenjang', $rm->get_jenjang());
		$smarty->assign('data_semester', $rm->get_semester());
		$smarty->assign('data_semester_aktif', $rm->get_semester_aktif());
		
		if(isset($_POST['generate'])){
	
			$smarty->assign('data_program_studi', $rm->get_program_studi_by_fakultas_jenjang($_POST['fakultas'], $_POST['jenjang']));
			$smarty->assign('fakultas', $_POST['fakultas']);
			$smarty->assign('jenjang', $_POST['jenjang']);
			$smarty->assign('program_studi', $_POST['program_studi']);
			$smarty->assign('jalur', $_POST['jalur']);
			$smarty->assign('semester', $_POST['semester']);
			$smarty->assign('nama', $_POST['nama']);
			$smarty->assign('gelar_depan', $_POST['gelar_depan']);
			$smarty->assign('gelar_belakang', $_POST['gelar_belakang']);
			$smarty->assign('generate', $_POST['generate']);
						
			$panjang_nim = strlen($_POST['nim_lama']);
			
			if($panjang_nim == 9){
				$panjang_nim_baru = "099999999";
			}elseif($panjang_nim == 10){
				$panjang_nim_baru = "0999999999";
			}elseif($panjang_nim == 11){
				$panjang_nim_baru = "09999999999";
			}elseif($panjang_nim == 12){
				$panjang_nim_baru = "099999999999";
			}
			
			$panjang_nim_baru = "099999999999";
			
			$nim_baru = $db->QueryToArray("select t.id_program_studi, prodi, nim_terakhir, trim(to_char(nim_terakhir+1,'$panjang_nim_baru')) nim_baru 
			from (
				select ps.id_program_studi, substr(nm_jenjang,1,2)||' '||ps.nm_program_studi prodi,
					(select max(nim_mhs) from mahasiswa m 
					where m.id_program_studi = ps.id_program_studi 
					and m.thn_angkatan_mhs in (select thn_akademik_semester from semester where id_semester = '$_POST[semester]')) nim_terakhir
				from program_studi ps
				join jenjang j on j.id_jenjang = ps.id_jenjang
			) t
			where t.id_program_studi = '$_POST[program_studi]'");	
						
			
			$mhs = $db->QueryToArray("SELECT * FROM MAHASISWA 
									LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
									WHERE NIM_MHS = '$_POST[nim_lama]'");	
			
		
		$semester = $db->QueryToArray("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '$_POST[semester]'");	
		$nim_baru_seru = $nim_baru[0]['NIM_BARU'];
		
		
		$smarty->assign('nim_baru', $nim_baru_seru);
			
			//insert foto
			$source = "/var/www/html/foto_mhs/".$_POST['nim_lama'].".JPG";
			$target = "/var/www/html/foto_mhs/".$nim_baru_seru.".JPG";
			copy($source, $target);
			
			$db->beginTransaction();
			
			//INSERT PENGGUNA
			$pass = $user->Encrypt($nim_baru_seru);
			$nama =  str_replace("'", "''", $_POST['nama']);
			// menyiapkan data password utk disimpan ke db
				$password_hash = sha1($nim_baru_seru);
				$password_encrypted = $user->PublicKeyEncrypt($nim_baru_seru);
				$last_time_password = date('d-M-Y');
			
			$hasil1= $db->Query("INSERT INTO PENGGUNA (USERNAME, NM_PENGGUNA, TGL_LAHIR_PENGGUNA, KELAMIN_PENGGUNA, 
											ID_AGAMA, TEMPAT_LAHIR, ID_ROLE, JOIN_TABLE, SE1, GELAR_DEPAN, GELAR_BELAKANG,
											password_hash, password_encrypted, last_time_password, password_hash_temp, password_hash_temp_expired, password_must_change) 
						VALUES ('".$nim_baru_seru."', '$nama', '".$mhs[0]['TGL_LAHIR_PENGGUNA']."', 
						'".$mhs[0]['KELAMIN_PENGGUNA']."', '".$mhs[0]['ID_AGAMA']."', '".$mhs[0]['TEMPAT_LAHIR']."', 
						'".$mhs[0][ID_ROLE]."', '".$mhs[0][JOIN_TABLE]."', '$pass', '$_POST[gelar_depan]', '$_POST[gelar_belakang]', 
						'{$password_hash}', '{$password_encrypted}', '{$last_time_password}', null, null, 1)");
			
			if($hasil1){
				$db->commit();
			}else{
				$db->rollBack();
			}
			if (!$hasil1) die("ERROR at ".__LINE__.": ".print_r(error_get_last(),true));
			
			$pengguna = $db->QueryToArray("SELECT * FROM PENGGUNA WHERE USERNAME = '".$nim_baru_seru."'");
			$semester = $db->QueryToArray("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '$_POST[semester]'");	
			$tgl = date('Y-m-d');
			$nim_bank = substr($nim_baru_seru, 0, $panjang_nim_baru);
								
			// update id_c_mhs = null
			$db->Query("UPDATE MAHASISWA SET ID_C_MHS = '' WHERE NIM_MHS = '$_POST[nim_lama]'");
			
			if($_POST['jalur'] == 4){
				$status = 'AJ';
			}else{
				$status = 'R';
			}
			//INSERT MAHASISWA
			$db->Query("INSERT INTO MAHASISWA (ID_PENGGUNA, ID_PROGRAM_STUDI, NIM_MHS, THN_ANGKATAN_MHS, 
						ALAMAT_MHS, MOBILE_MHS, NM_AYAH_MHS, PENDIDIKAN_AYAH_MHS, 
						PEKERJAAN_AYAH_MHS, ALAMAT_AYAH_MHS, NM_IBU_MHS, PENDIDIKAN_IBU_MHS, 
						PEKERJAAN_IBU_MHS, ALAMAT_IBU_MHS, PENGHASILAN_ORTU_MHS, NEM_PELAJARAN_MHS, 
						LAHIR_KOTA_MHS, LAHIR_PROP_MHS, TGL_TERDAFTAR_MHS, STATUS_CEKAL, NIM_BANK, STATUS_AKADEMIK_MHS, 
						ASAL_KOTA_MHS, ASAL_PROV_MHS, STATUS, ALAMAT_MHS_KOTA, 
						ALAMAT_AYAH_MHS_KOTA, ALAMAT_AYAH_MHS_PROV, ALAMAT_IBU_MHS_KOTA, ALAMAT_IBU_MHS_PROV, 
						ALAMAT_ASAL_MHS, ALAMAT_ASAL_MHS_KOTA, ALAMAT_ASAL_MHS_PROV, ID_SEKOLAH_ASAL_MHS, ID_SEMESTER_MASUK, ID_C_MHS, NIM_LAMA, ID_KEBANGSAAN)
				VALUES ('".$pengguna[0]['ID_PENGGUNA']."', '$_POST[program_studi]', '".$nim_baru_seru."', 
				'".$semester[0]['THN_AKADEMIK_SEMESTER']."',
				'".$mhs[0]['ALAMAT_MHS']."', '".$mhs[0]['MOBILE_MHS']."', '".$mhs[0]['NM_AYAH_MHS']."', '".$mhs[0]['PENDIDIKAN_AYAH_MHS']."', 
				'".$mhs[0]['PEKERJAAN_AYAH_MHS']."', '".$mhs[0]['ALAMAT_AYAH_MHS']."', '".$mhs[0]['NM_IBU_MHS']."', 
				'".$mhs[0]['PENDIDIKAN_IBU_MHS']."',
				'".$mhs[0]['PEKERJAAN_IBU_MHS']."', '".$mhs[0]['ALAMAT_IBU_MHS']."', '".$mhs[0]['PENGHASILAN_ORTU_MHS']."',
				'".$mhs[0]['NEM_PELAJARAN_MHS']."',
				'".$mhs[0]['LAHIR_KOTA_MHS']."', '".$mhs[0]['LAHIR_PROP_MHS']."', to_date('$tgl', 'YYYY-MM-DD'), '0', '$nim_bank', '1', 
				'".$mhs[0]['ASAL_KOTA_MHS']."', '".$mhs[0]['ASAL_PROV_MHS']."', '".$status."', '".$mhs[0]['ALAMAT_MHS_KOTA']."',
				'".$mhs[0]['ALAMAT_AYAH_MHS_KOTA']."', 
				'".$mhs[0]['ALAMAT_AYAH_MHS_PROV']."', '".$mhs[0]['ALAMAT_IBU_MHS_KOTA']."', '".$mhs[0]['ALAMAT_IBU_MHS_PROV']."',
				'".$mhs[0]['ALAMAT_ASAL_MHS']."', 
				'".$mhs[0]['ALAMAT_ASAL_MHS_KOTA']."', '".$mhs[0]['ALAMAT_ASAL_MHS_PROV']."', '".$mhs[0]['ID_SEKOLAH_ASAL']."',
				'$_POST[semester]', (SELECT ID_C_MHS FROM CALON_MAHASISWA_BARU WHERE NIM_MHS = '".$_POST['nim_lama']."'), 
				'".$_POST['nim_lama']."', 114)") or die('gagal 2');
		
			
			$mhs_baru = $db->QueryToArray("SELECT ID_MHS FROM MAHASISWA WHERE NIM_MHS = '".$nim_baru_seru."'");
			
			//INSERT ADMISI
			$db->Query("INSERT INTO ADMISI (ID_MHS, STATUS_AKD_MHS, TGL_USULAN, TGL_APV, ID_SEMESTER, STATUS_APV, ID_PENGGUNA)
			VALUES ('".$mhs_baru[0]['ID_MHS']."', '1', to_date('$tgl','YYYY-MM-DD'), to_date('$tgl','YYYY-MM-DD'), '$_POST[semester]', '1', $id_pengguna)")  or die('gagal 3');
			
			//INSERT PEMBAYARAN
			if($_POST['set_biaya'] != ''){
			$set_biaya = $db->QueryToArray("SELECT * FROM DETAIL_BIAYA  WHERE ID_BIAYA_KULIAH = '$_POST[set_biaya]'");
				foreach($set_biaya as $biaya){
					$db->Query("INSERT INTO PEMBAYARAN (ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, BESAR_BIAYA, DENDA_BIAYA, 
								IS_TAGIH, IS_TARIK, ID_STATUS_PEMBAYARAN)
							VALUES ('".$mhs_baru[0]['ID_MHS']."', '$_POST[semester]', '$biaya[ID_DETAIL_BIAYA]', '$biaya[BESAR_BIAYA]', '0', 
							'Y', '0', '2')") 
							 or die('gagal 6');
				}	
				
				$db->Query("INSERT INTO BIAYA_KULIAH_MHS (ID_BIAYA_KULIAH, ID_MHS) VALUES ($_POST[set_biaya], '".$mhs_baru[0]['ID_MHS']."')") 
					or die('gagal 8');
				
				$db->Query("UPDATE MAHASISWA 
						SET ID_KELOMPOK_BIAYA = (SELECT ID_KELOMPOK_BIAYA FROM BIAYA_KULIAH WHERE ID_BIAYA_KULIAH = '$_POST[set_biaya]') 
						WHERE ID_MHS = '".$mhs_baru[0]['ID_MHS']."'") or die('gagal 7');
			
			}else{
			
				$db->Query("SELECT count(*) as ada, ID_BIAYA_KULIAH FROM BIAYA_KULIAH_MHS WHERE ID_MHS = '".$mhs[0]['ID_MHS']."'
							 GROUP BY ID_BIAYA_KULIAH");
				$cek_biaya = $db->FetchAssoc();
				
				if($cek_biaya['ADA'] >= 1){
					$db->Query("INSERT INTO PEMBAYARAN (ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, ID_BANK, ID_BANK_VIA, BESAR_BIAYA, DENDA_BIAYA, 
								TGL_BAYAR, IS_TAGIH, NO_TRANSAKSI, KETERANGAN, IS_TARIK, ID_SEMESTER_BAYAR, ID_STATUS_PEMBAYARAN, FLAG_PEMINDAHAN)
								(SELECT '".$mhs_baru[0]['ID_MHS']."' AS ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, ID_BANK, ID_BANK_VIA, BESAR_BIAYA, DENDA_BIAYA, 
								TGL_BAYAR, IS_TAGIH, NO_TRANSAKSI, KETERANGAN, IS_TARIK, ID_SEMESTER_BAYAR, ID_STATUS_PEMBAYARAN, FLAG_PEMINDAHAN 
								FROM PEMBAYARAN WHERE ID_MHS = '".$mhs[0]['ID_MHS']."')") or die('gagal 234');
					
					$db->Query("UPDATE MAHASISWA 
						SET NIM_LAMA = '$_POST[nim_lama]', 
						ID_KELOMPOK_BIAYA = (SELECT ID_KELOMPOK_BIAYA FROM BIAYA_KULIAH WHERE ID_BIAYA_KULIAH = '$cek_biaya[ID_BIAYA_KULIAH]') 
						WHERE ID_MHS = '".$mhs_baru[0]['ID_MHS']."'") or die('gagal 7');
						
				}
				
			}
			
			
			$db->Query("SELECT COUNT(*) AS CEK_CMHS FROM CALON_MAHASISWA_BARU WHERE NIM_MHS = '$_POST[nim_lama]'");
			$cek_cmhs = $db->FetchAssoc();
			
			if($cek_cmhs['CEK_CMHS'] >= 1){
				$db->Query("UPDATE CALON_MAHASISWA_BARU SET NIM_MHS = '$nim_baru_seru' WHERE NIM_MHS = '$_POST[nim_lama]'");
			}
			
			
			//INSERT JALUR MAHASISWA
			$db->Query("INSERT INTO JALUR_MAHASISWA (ID_MHS, ID_PROGRAM_STUDI, ID_JALUR, ID_SEMESTER, NIM_MHS, ID_JALUR_AKTIF)
			VALUES ('".$mhs_baru[0]['ID_MHS']."', '$_POST[program_studi]', '$_POST[jalur]', '$_POST[semester]', '".$nim_baru_seru."', '1')")  or die('gagal 4');
			
			//INSERT FINGERPRINT
			$finger = $db->Query("INSERT INTO FINGERPRINT_MAHASISWA (FINGER_DATA, NIM_MHS, TGL_LAHIR, HURUF_DEPAN)
						SELECT FINGER_DATA, '".$nim_baru_seru."' AS NIM_BARU, TGL_LAHIR, HURUF_DEPAN 
						FROM FINGERPRINT_MAHASISWA WHERE NIM_MHS = '$_POST[nim_lama]'") or die('gagal 5');
			
			if($finger){
				echo "Berhasil";
				$db->commit();
			}else{
				$db->rollBack();
			}
						

		}else{
			$mhs = $db->QueryToArray("SELECT * FROM MAHASISWA 
									LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
									WHERE NIM_MHS = '$_POST[nim]'");	
			$smarty->assign('mhs', $mhs);
		}

}


$smarty->display("pendaftaran/nim_unair/pendaftaran-nim-unair.tpl");
?>
