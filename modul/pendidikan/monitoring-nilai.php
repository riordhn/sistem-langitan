<?php
include '../../config.php';

$akademik = new Akademik($db);

$semester_aktif		= $PT->get_semester_aktif();
$all_prodi 			= "";
$id_semester		= empty($_GET['id_semester']) ? $semester_aktif['ID_SEMESTER'] : (int)$_GET['id_semester'];
$id_program_studi	= empty($_GET['id_program_studi']) ? $all_prodi : (int)$_GET['id_program_studi'];
$semester_set		= $PT->list_semester();
$prodi_set			= $PT->list_prodi();
$kelas_set			= $akademik->list_kelas_per_prodi($id_program_studi, $id_semester);

// Assignment ke template
$smarty->assign('semester_set', $semester_set);
$smarty->assign('prodi_set', $prodi_set);
$smarty->assign('id_semester_terpilih', $id_semester);
$smarty->assign('id_program_studi_terpilih', $id_program_studi);
$smarty->assign('kelas_set', $kelas_set);

$smarty->display('monitoring/nilai.tpl');