<?php
include('config.php');
include 'class/mkwu.class.php';

$mkwu = new mkwu($db);
$id_prodi = 228;

$action = isset($_REQUEST['action'])?  $_REQUEST['action'] : 'view';

$semester_aktif =  $mkwu->semester_aktif();
$smarty->assign('semester', $mkwu->semester());
$smarty->assign('semester_aktif', $semester_aktif);
$smarty->assign('kurikulum_mkwu', $mkwu->kurikulum_mkwu());
$smarty->assign('kelas', $mkwu->kelas());
$smarty->assign('hari', $mkwu->hari());
$smarty->assign('t_jam', $mkwu->jam());
$smarty->assign('fakultas', $mkwu->fakultas());
$smarty->assign('data_jenjang', $mkwu->jenjang());



$semester_request = isset($_REQUEST['smt'])?  $_REQUEST['smt'] : $semester_aktif['ID_SEMESTER'];

if($action == 'usulan'){
$smarty->assign('usulan', $mkwu->usulan_mkwu($id_prodi, $semester_request, $_REQUEST['kur']));
}

if($action == 'add'){
$mkwu->usulan_mkwu_add($id_prodi, $semester_request, $_REQUEST['id_mk']);
$smarty->assign('usulan', $mkwu->usulan_mkwu($id_prodi, $semester_request, $_REQUEST['kur']));
}


if($action == 'del'){
$mkwu->usulan_mkwu_del($_REQUEST['id_klsmk']);
$action = 'view';
}


if($action == 'updateview' or $action == 'adkelview'){

	if(isset($_REQUEST['id_kelas_mk']) and $action == 'updateview'){
		$smarty->assign('usulan', $mkwu->usulan_mkwu_update($_REQUEST['program_studi'], $semester_request, $_REQUEST['kelas_mk'], $_REQUEST['kap_kelas'], $_REQUEST['ren_kul'], $_REQUEST['id_kelas_mk'], $_REQUEST['hari'], $_REQUEST['jam'], $_REQUEST['ruangan']));
		$action = 'view';
	}
	
	
	if(isset($_REQUEST['kelas_mk']) and $action == 'adkelview'){
		$smarty->assign('usulan', $mkwu->usulan_mkwu_addkls($_REQUEST['program_studi'], $semester_request, $_REQUEST['kelas_mk'], $_REQUEST['kap_kelas'], $_REQUEST['ren_kul'], $_REQUEST['id_kelas_mk'], $_REQUEST['hari'], $_REQUEST['jam'], $_REQUEST['ruangan'], $_REQUEST['id_kur_mk']));
		$action = 'view';
	}
	

$usulan = $mkwu->usulan_mkwu_updateview($id_prodi, $semester_request, $_REQUEST['id_klsmk']);

$smarty->assign('usulan', $usulan);
$smarty->assign('t_ruangan', $mkwu->ruangan($usulan[0]['ID_FAKULTAS_GEDUNG']));
$smarty->assign('data_program_studi', $mkwu->program_studi($usulan[0]['ID_FAKULTAS'], $usulan[0]['ID_JENJANG']));

}


if($action == 'view'){
		$smarty->assign('rincian', $mkwu->usulan_mkwu_rincian($id_prodi, $semester_request));	
}



$smarty->assign('semester_request', $semester_request);
$smarty->assign('action', $action);
$smarty->display("mkwu/mkwu-usulan.tpl")
?>