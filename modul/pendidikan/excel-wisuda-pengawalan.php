<?php
include 'config.php';

$pengajuan = $db->QueryToArray("SELECT NIM_MHS, (GELAR_DEPAN || ' ' || NM_PENGGUNA || CASE WHEN GELAR_BELAKANG IS NULL THEN '' ELSE ', ' END || GELAR_BELAKANG) AS NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, NM_FAKULTAS, PENGAJUAN_WISUDA.NO_IJASAH, 
								TGL_LULUS_PENGAJUAN, PENGAJUAN_WISUDA.ID_MHS, POSISI_IJASAH, NO_SERI_KERTAS
								,TO_CHAR(TGL_LULUS_PENGAJUAN, 'DD-MM-YYYY') AS TGL_LULUS_PENGAJUAN, 
								(CASE WHEN LAHIR_IJAZAH IS NULL THEN initcap(NM_KOTA) || ', ' || TO_CHAR(TGL_LAHIR_PENGGUNA, 'YYYY-MM-DD') 
									ELSE LAHIR_IJAZAH END) AS LAHIR_IJAZAH,
								TO_CHAR(ADD_MONTHS(TGL_POSISI_IJASAH4, 3), 'DD-MM-YYYY') AS BATAS_WAKTU,
								TO_CHAR(TGL_POSISI_IJASAH, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH, 
								TO_CHAR(TGL_POSISI_IJASAH2, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH2,
								TO_CHAR(TGL_POSISI_IJASAH3, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH3,
								TO_CHAR(TGL_POSISI_IJASAH4, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH4,
								TO_CHAR(TGL_POSISI_IJASAH5, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH5,
								TO_CHAR(TGL_POSISI_IJASAH6, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH6,
								TO_CHAR(TGL_POSISI_IJASAH7, 'DD-MM-YYYY') AS TGL_POSISI_IJASAH7
								FROM PENGAJUAN_WISUDA
								LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PENGAJUAN_WISUDA.ID_MHS
								LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
								LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
								LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
								LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
								LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA
								LEFT JOIN KOTA ON KOTA.ID_KOTA = LAHIR_KOTA_MHS
								WHERE PERIODE_WISUDA.ID_TARIF_WISUDA = '$_GET[periode]'
								AND FAKULTAS.ID_FAKULTAS = '$_GET[fakultas]'
								ORDER BY PENGAJUAN_WISUDA.NO_IJASAH, NM_JENJANG, NM_PROGRAM_STUDI");	
ob_start();
?>
<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
 <table style="font-size:12px; width:1200px;" border="1">
<tr>
	<th style="text-align:center">NO</th>
	<th style="text-align:center">NAMA</th>
	<th style="text-align:center">NIM</th>
    <th style="text-align:center">FAKULTAS</th>
    <th style="text-align:center">JENJANG</th>
    <th style="text-align:center">PROGRAM STUDI</th>
    <th style="text-align:center">TEMPAT DAN TANGGAL LAHIR</th>
	<th style="text-align:center">TGL LULUS</th>
    <th style="text-align:center">NO IJASAH</th>
    <th style="text-align:center">NO SERI</th>
</tr>

<?PHP
	$no = 1;	
	foreach($pengajuan as $data){
echo '<tr>
        	<td>'.$no++.'</td>
			<td>'.$data['NM_PENGGUNA'].'</td>
            <td>`'.$data['NIM_MHS'].'</td>
            <td>'.$data['NM_FAKULTAS'].'</td>
            <td>'.$data['NM_JENJANG'].'</td>
            <td>'.$data['NM_PROGRAM_STUDI'].'</td>
			<td>'.$data['LAHIR_IJAZAH'].'</td>
            <td>'.$data['TGL_LULUS_PENGAJUAN'].'</td>
            <td>'.$data['NO_IJASAH'].'</td>
            <td>'.$data['NO_SERI_KERTAS'].'</td>            
        </tr>';
    }

echo '</table>';


$filename = 'Excel_wisuda' . date('Y-m-d').'.xls';
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
