<?php
class wisuda {

	public $db;
	
	function __construct($db) {
		$this->db = $db;
	}
	
	function wisuda_yudisium($fakultas, $status_bayar, $periode, $tgl) {
	
		if($fakultas <> ''){
			$where  = " AND FAKULTAS.ID_FAKULTAS = '$fakultas'";
		}else{
			$where  = "";
		}
		
		
		if($tgl <> ''){
			$where .= " AND PENGAJUAN_CETAK_IJASAH = TO_DATE('$tgl', 'DD-MM-YYYY')";
			$order = "";
		}
		
		
		$ipk = "LEFT JOIN (select round(sum(kredit_semester*nilai_standar_nilai)/sum(kredit_semester), 2) as ipk, sum(kredit_semester) as sks, id_mhs
				from (select a.id_mhs,nilai_standar_nilai, e.tahun_ajaran, e.nm_semester, 
				c.kd_mata_kuliah,c.nm_mata_kuliah,d.kredit_semester,a.nilai_huruf, a.flagnilai, 
				row_number() over(partition by a.id_mhs,c.nm_mata_kuliah order by a.nilai_huruf) rangking,
				count(*) over(partition by c.nm_mata_kuliah) terulang
					from pengambilan_mk a, mata_kuliah c, kurikulum_mk d, semester e, standar_nilai f, mahasiswa g, program_studi h
					where a.id_kurikulum_mk=d.id_kurikulum_mk and d.id_mata_kuliah=c.id_mata_kuliah and a.id_semester=e.id_semester
					and a.STATUS_APV_PENGAMBILAN_MK='1' and nm_standar_nilai = nilai_huruf and g.id_mhs = a.id_mhs 
					and h.id_program_studi = g.id_program_studi
				) x
				where rangking = 1 and flagnilai = 1 and nilai_huruf < 'E' and nilai_huruf is not null and nilai_huruf != '-'
				group by id_mhs) C ON C.ID_MHS = MAHASISWA.ID_MHS ";

		
		if($status_bayar == 1){

			return $this->db->QueryToArray("SELECT NIM_MHS, NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, NM_FAKULTAS, 
									NO_IJASAH, TGL_BAYAR, PEMBAYARAN_WISUDA.BESAR_BIAYA, (SELECT SUM(PEMBAYARAN.BESAR_BIAYA) AS JML FROM PEMBAYARAN
										LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
										LEFT JOIN DETAIL_BIAYA ON DETAIL_BIAYA.ID_DETAIL_BIAYA = PEMBAYARAN.ID_DETAIL_BIAYA
										WHERE (TGL_BAYAR IS NOT NULL OR PEMBAYARAN.ID_STATUS_PEMBAYARAN = 1) 
										AND ID_BIAYA = 115 AND PEMBAYARAN.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS) AS CICILAN,
									LAHIR_IJAZAH, TGL_LULUS_PENGAJUAN, PERIODE_WISUDA.BESAR_BIAYA AS TARIF,
									(CASE WHEN KELAMIN_PENGGUNA = 1 THEN 'L' WHEN KELAMIN_PENGGUNA = 2 THEN 'P' ELSE '' END) AS KELAMIN_PENGGUNA, PENGAJUAN_WISUDA.ELPT,
									PENGGUNA.GELAR_DEPAN, PENGGUNA.GELAR_BELAKANG, COALESCE(NM_AYAH_MHS, NM_IBU_MHS) AS ORTU, ALAMAT_MHS, EMAIL_PENGGUNA, 
									C.IPK, FAKULTAS.ID_FAKULTAS
									 FROM PEMBAYARAN_WISUDA
									 JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS
									 JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
									 JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
									 JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
									 JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
									 JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PEMBAYARAN_WISUDA.ID_PERIODE_WISUDA
									 LEFT JOIN PENGAJUAN_WISUDA ON PENGAJUAN_WISUDA.ID_MHS = PEMBAYARAN_WISUDA.ID_MHS	
									  $ipk								
									WHERE ABSENSI_WISUDA = 1 AND YUDISIUM != 0 AND PERIODE_WISUDA.ID_TARIF_WISUDA = '$periode' 
									$where
									ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI, MAHASISWA.NIM_MHS, PEMBAYARAN_WISUDA.TGL_BAYAR");

		}elseif($status_bayar == 2){
	
			return $this->db->QueryToArray("SELECT NIM_MHS, NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, NM_FAKULTAS, PENGAJUAN_WISUDA.NO_IJASAH, 
									ABSTRAK_TA_CLOB, JUDUL_TA, TGL_LULUS_PENGAJUAN, EVA_HASIL, BIODATA, STAT_UPLOAD, PENGAJUAN_WISUDA.ELPT
									FROM PENGAJUAN_WISUDA
									LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PENGAJUAN_WISUDA.ID_MHS
									LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
									LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
									LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
									LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
									LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA
									LEFT JOIN (
												SELECT COUNT(*) AS EVA_HASIL, ID_MHS FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=5 GROUP BY ID_MHS
											  ) EH ON EH.ID_MHS = MAHASISWA.ID_MHS
									LEFT JOIN (
												SELECT COUNT(*) AS BIODATA, ID_MHS FROM MAHASISWA WHERE MOBILE_MHS IS NOT NULL AND NM_AYAH_MHS IS NOT NULL
												AND PENDIDIKAN_AYAH_MHS IS NOT NULL AND PEKERJAAN_AYAH_MHS IS NOT NULL AND ALAMAT_AYAH_MHS IS NOT NULL
												AND NM_IBU_MHS IS NOT NULL AND PENDIDIKAN_IBU_MHS IS NOT NULL AND PEKERJAAN_IBU_MHS IS NOT NULL
												AND ALAMAT_IBU_MHS IS NOT NULL AND PENGHASILAN_ORTU_MHS IS NOT NULL
												AND LAHIR_KOTA_MHS IS NOT NULL AND LAHIR_PROP_MHS IS NOT NULL AND ASAL_KOTA_MHS IS NOT NULL
												AND ASAL_PROV_MHS IS NOT NULL 
												AND ALAMAT_AYAH_MHS_KOTA IS NOT NULL AND ALAMAT_AYAH_MHS_PROV IS NOT NULL 
												AND ALAMAT_IBU_MHS_KOTA IS NOT NULL AND ALAMAT_IBU_MHS_PROV IS NOT NULL AND ALAMAT_ASAL_MHS IS NOT NULL
												AND ALAMAT_ASAL_MHS_KOTA IS NOT NULL AND ALAMAT_ASAL_MHS_PROV IS NOT NULL
												GROUP BY ID_MHS
											  ) BIO ON BIO.ID_MHS = MAHASISWA.ID_MHS
									LEFT JOIN (
												SELECT COUNT(*) AS STAT_UPLOAD, ID_MHS FROM UPLOAD_STATUS_TA GROUP BY ID_MHS
											  ) SU ON SU.ID_MHS = MAHASISWA.ID_MHS
									WHERE PERIODE_WISUDA.ID_TARIF_WISUDA = '$periode' AND YUDISIUM != 0 AND PENGAJUAN_WISUDA.ID_MHS NOT IN (
										SELECT ID_MHS FROM PEMBAYARAN_WISUDA A
										JOIN PERIODE_WISUDA B ON B.ID_PERIODE_WISUDA = A.ID_PERIODE_WISUDA
										WHERE B.ID_TARIF_WISUDA = '$periode'
									) AND PERIODE_WISUDA.BESAR_BIAYA <> 0
									$where AND PROGRAM_STUDI.ID_JENJANG NOT IN (9, 10)
									ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI, MAHASISWA.NIM_MHS");
		}elseif($status_bayar == 4){
	
			return $this->db->QueryToArray("SELECT NIM_MHS, NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, NM_FAKULTAS, PENGAJUAN_WISUDA.NO_IJASAH, 
									ABSTRAK_TA_CLOB, JUDUL_TA, TGL_BAYAR, TGL_LULUS_PENGAJUAN, PENGAJUAN_WISUDA.ELPT
									FROM PENGAJUAN_WISUDA
									LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PENGAJUAN_WISUDA.ID_MHS
									LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
									LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
									LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
									LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
									LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA
									LEFT JOIN PEMBAYARAN_WISUDA ON PEMBAYARAN_WISUDA.ID_MHS = MAHASISWA.ID_MHS
								
									WHERE PERIODE_WISUDA.ID_TARIF_WISUDA = '$periode' AND YUDISIUM = 0
									$where
									ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI, MAHASISWA.NIM_MHS");
		}else{
		
			return $this->db->QueryToArray("SELECT NIM_MHS, NM_PENGGUNA, NM_JENJANG, NM_PROGRAM_STUDI, NM_FAKULTAS, PENGAJUAN_WISUDA.NO_IJASAH, 
									ABSTRAK_TA_CLOB, JUDUL_TA, TGL_BAYAR, TO_CHAR(TGL_LULUS_PENGAJUAN, 'DD-MM-YYYY') AS TGL_LULUS_PENGAJUAN, EVA_HASIL, BIODATA, STAT_UPLOAD, 
									PENGAJUAN_WISUDA.ID_PENGAJUAN_WISUDA, JUMLAH_CETAK, PENGAJUAN_CETAK_IJASAH, LAHIR_IJAZAH, PENGAJUAN_WISUDA.ELPT
									FROM PENGAJUAN_WISUDA
									LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PENGAJUAN_WISUDA.ID_MHS
									LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
									LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
									LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
									LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
									LEFT JOIN PERIODE_WISUDA ON PERIODE_WISUDA.ID_PERIODE_WISUDA = PENGAJUAN_WISUDA.ID_PERIODE_WISUDA
									LEFT JOIN (
										SELECT ID_MHS, TGL_BAYAR FROM PEMBAYARAN_WISUDA A
										JOIN PERIODE_WISUDA B ON B.ID_PERIODE_WISUDA = A.ID_PERIODE_WISUDA
										WHERE B.ID_TARIF_WISUDA = '$periode'
										AND A.ABSENSI_WISUDA = 1
									) A ON A.ID_MHS = MAHASISWA.ID_MHS
									LEFT JOIN (
												SELECT COUNT(*) AS EVA_HASIL, ID_MHS FROM EVALUASI_HASIL WHERE ID_EVAL_INSTRUMEN=5 GROUP BY ID_MHS
											  ) EH ON EH.ID_MHS = MAHASISWA.ID_MHS
									LEFT JOIN (
												SELECT COUNT(*) AS BIODATA, ID_MHS FROM MAHASISWA WHERE MOBILE_MHS IS NOT NULL AND NM_AYAH_MHS IS NOT NULL
												AND PENDIDIKAN_AYAH_MHS IS NOT NULL AND PEKERJAAN_AYAH_MHS IS NOT NULL AND ALAMAT_AYAH_MHS IS NOT NULL
												AND NM_IBU_MHS IS NOT NULL AND PENDIDIKAN_IBU_MHS IS NOT NULL AND PEKERJAAN_IBU_MHS IS NOT NULL
												AND ALAMAT_IBU_MHS IS NOT NULL AND PENGHASILAN_ORTU_MHS IS NOT NULL
												AND LAHIR_KOTA_MHS IS NOT NULL AND LAHIR_PROP_MHS IS NOT NULL AND ASAL_KOTA_MHS IS NOT NULL
												AND ASAL_PROV_MHS IS NOT NULL 
												AND ALAMAT_AYAH_MHS_KOTA IS NOT NULL AND ALAMAT_AYAH_MHS_PROV IS NOT NULL 
												AND ALAMAT_IBU_MHS_KOTA IS NOT NULL AND ALAMAT_IBU_MHS_PROV IS NOT NULL AND ALAMAT_ASAL_MHS IS NOT NULL
												AND ALAMAT_ASAL_MHS_KOTA IS NOT NULL AND ALAMAT_ASAL_MHS_PROV IS NOT NULL
												GROUP BY ID_MHS
											  ) BIO ON BIO.ID_MHS = MAHASISWA.ID_MHS
									LEFT JOIN (
												SELECT COUNT(*) AS STAT_UPLOAD, ID_MHS FROM UPLOAD_STATUS_TA GROUP BY ID_MHS
											  ) SU ON SU.ID_MHS = MAHASISWA.ID_MHS
									WHERE PERIODE_WISUDA.ID_TARIF_WISUDA = '$periode'  AND YUDISIUM != 0
									$where
									ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG,NM_PROGRAM_STUDI, 
									TO_NUMBER(SUBSTR(PENGAJUAN_WISUDA.NO_IJASAH, 0, INSTR(PENGAJUAN_WISUDA.NO_IJASAH, '/')-1)),  MAHASISWA.NIM_MHS");
									
			
			}
			
	}
	
	
	
	
	private function get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi) {
		
		
		if($id_jenjang == 'all'){
			$id_jenjang = '';	
		}
		
        if ($id_fakultas != "" && $id_prodi != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}' 
						AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_fakultas != "" && $id_prodi != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } elseif ($id_fakultas != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_prodi != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}' AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_prodi != "") {
            $query = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } elseif ($id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } else if ($id_fakultas != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' ";
        } else {
            $query = " ";
        }
        return $query;
    }
	
	
	
	function wisuda_lama_studi($id_fakultas, $id_jenjang, $id_program_studi) {
		
		$condition_akademik =  $this->get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi);
		
		 $query = "select  
					(lama_studi + (case when semester_studi = 1 then 1 when semester_studi = 0 then 0.5 else 0 end)) as LAMA_STUDI, MAHASISWA.id_mhs
					from (
					select PROGRAM_STUDI.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_FAKULTAS, JENJANG.id_jenjang, STATUS, 
					TO_CHAR(TGL_LULUS, 'YYYY') as THN_AKADEMIK_LULUS, b.THN_AKADEMIK_SEMESTER || (CASE WHEN b.NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) AS THN_AKADEMIK_SMT, 
					(b.THN_AKADEMIK_SEMESTER - a.THN_AKADEMIK_SEMESTER - (case when cuti is null then 0 else cuti end)) as lama_studi, 
					(CASE WHEN b.NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) - (CASE WHEN a.NM_SEMESTER = 'Ganjil' THEN 1 ELSE 2 END) as semester_studi
					, kelamin_pengguna
					from mahasiswa 
					join semester a on a.id_semester = mahasiswa.id_semester_masuk
					join pengguna on PENGGUNA.id_pengguna = MAHASISWA.id_pengguna
					join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi 
					join fakultas on fakultas.id_fakultas = program_studi.id_fakultas 
					join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs 
					join jenjang on jenjang.id_jenjang = program_studi.id_jenjang 
					join admisi on ADMISI.ID_MHS = MAHASISWA.ID_MHS and status_akd_mhs = '4' and status_apv = 1 AND TGL_LULUS IS NOT NULL
					JOIN SEMESTER b ON b.ID_SEMESTER = ADMISI.ID_SEMESTER
					
					left join (
						select id_mhs, count(*) as cuti from ADMISI where status_apv = 1 and status_akd_mhs = 2
					group by id_mhs 
					) x on x.id_mhs = MAHASISWA.id_mhs
					WHERE program_studi.status_aktif_prodi = 1	{$condition_akademik}
				";
        return $query;
	}
	
	
}
?>
