<?php

class PDDikti
{
    /**
     * @var MyOracle
     */
    private $db;
    
    function __construct($db)
    {
        $this->db = $db;
    }
    
    function list_master_status_mhs()
    {
        return $this->db->QueryToArray(
            "SELECT id_status_pengguna, nm_status_pengguna, status_aktif, status_keluar, aktif_status_pengguna, fd_id_jns_keluar, fd_id_stat_mhs
            FROM status_pengguna
            where id_role = 3 and fd_id_stat_mhs in ('A','C','G','N') ORDER BY fd_id_stat_mhs"
        );
    }
    
    /**
     * Aktivitas Kuliah Mahasiswa
     */
    function list_status_mahasiswa($id_semester = 'all', $id_program_studi = 'all', $angkatan = 'all', $id_status_pengguna = 'all')
    {
        $sql =
            "SELECT
                id_mhs_status, nim_mhs, nm_pengguna, nm_jenjang, nm_program_studi, thn_angkatan_mhs, tahun_ajaran,
                nm_semester, nm_status_pengguna, sp.fd_id_stat_mhs, ips, ipk, sks_semester, sks_total, 
                to_char(ms.fd_sync_on,'YYYY-MM-DD') as fd_sync_on
            FROM mahasiswa_status ms
            JOIN mahasiswa M ON M.id_mhs = ms.id_mhs
            JOIN pengguna P ON P.id_pengguna = M.id_pengguna
            JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
            JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
            JOIN semester S ON S.id_semester = ms.id_semester
            JOIN status_pengguna sp ON sp.id_status_pengguna = ms.id_status_pengguna ";
        
        $params = array();
        
        if ($id_semester != 'all') {
            array_push($params, 'ms.id_semester = '.$id_semester);
        }
        
        if ($id_program_studi != 'all') {
            array_push($params, 'm.id_program_studi = '.$id_program_studi);
        }
        
        if ($angkatan != 'all') {
            array_push($params, 'm.thn_angkatan_mhs = '.$angkatan);
        }
        
        if ($id_status_pengguna != 'all') {
            array_push($params, 'ms.id_status_pengguna = '.$id_status_pengguna);
        }
        
        if (count($params) > 0) {
            $sql .= "WHERE " . join(' AND ', $params);
        }

        $sql .= "ORDER BY M.NIM_MHS";
        
        return $this->db->QueryToArray($sql);
    }
    
    function generate_status_mahasiswa($id_semester, $id_program_studi, $is_override = false)
    {
        global $user;
        
        // 1. Ambil data mahasiswa yang KRS Approve -> Asumsi Aktif.
        // ---------------------------------------------------------
        $sql_raw = file_get_contents('class/sql/pddikti-generate-status-mahasiswa.sql');
        
        // assign parameter
        $sql = strtr($sql_raw, [
            '$id_semester' => $id_semester,
            '$id_program_studi' => $id_program_studi
        ]);
        
        $new_status_set = $this->db->QueryToArray($sql);
        
        // Ambil existing status mahasiswa dari db
        $status_set = $this->db->QueryToArray(
            "SELECT ms.* FROM mahasiswa_status ms
            JOIN mahasiswa m ON m.id_mhs = ms.id_mhs
            WHERE ms.id_semester = {$id_semester} AND m.id_program_studi = {$id_program_studi} ORDER BY ms.ID_MHS"
        );
            
        // Casting nilai ips dan ipk ke float
        foreach ($status_set as &$status) {
            $status['IPS'] = floatval($status['IPS']);
            $status['IPK'] = floatval($status['IPK']);
        }
        
        // Komparasi status baru dengan yang lama
        foreach ($new_status_set as &$new_status) {
            $mhs_found = false;
            
            foreach ($status_set as &$status) {
                if ($new_status['ID_MHS'] == $status['ID_MHS']) {
                    $mhs_found = true;
                    
                    // Jika IPS / IPK / SKS / SKS Total beda -> perlu di update
                    if ($new_status['IPS'] != $status['IPS'] || $new_status['IPK'] != $status['IPK'] || $new_status['SKS_SEMESTER'] != $status['SKS_SEMESTER'] || $new_status['SKS_TOTAL'] != $status['SKS_TOTAL']) {
                        $new_status['MODE'] = 'UPDATE';
                        $new_status['ID_MHS_STATUS'] = $status['ID_MHS_STATUS'];
                    }
                    
                    break;
                }
            }
            
            if (! $mhs_found) {
                $new_status['MODE'] = 'INSERT';
            }
        }
        
        // Mulai proses ke database
        // -------------------------
        
        $this->db->BeginTransaction();
        
        foreach ($new_status_set as &$new_status) {

            // Rubah ke IPS = NULL jika kosong
            if ($new_status['IPS'] == '') {
                $new_status['IPS'] = 'NULL';
            }

            // Rubah ke IPK = NULL jika kosong
            if ($new_status['IPK'] == '') {
                $new_status['IPK'] = 0;
            }

            if ($new_status['MODE'] == 'INSERT') {
                $sql_insert =
                    "INSERT INTO mahasiswa_status (id_mhs, id_semester, ips, sks_semester, ipk, sks_total, id_status_pengguna, created_by)
                    VALUES (
                        {$new_status['ID_MHS']}, {$id_semester}, {$new_status['IPS']}, {$new_status['SKS_SEMESTER']},
                        {$new_status['IPK']}, {$new_status['SKS_TOTAL']}, 1, {$user->ID_PENGGUNA})";
                        
                if (! $this->db->Query($sql_insert)) {
                    echo "Error at " . __LINE__ . ": " . print_r($this->db->Error, true);
                    $this->db->Rollback();
                    exit();
                }
            }
            
            if ($is_override && $new_status['MODE'] == 'UPDATE') {
                $sql_update =
                    "UPDATE mahasiswa_status SET
                        ips = {$new_status['IPS']}, sks_semester = {$new_status['SKS_SEMESTER']},
                        ipk = {$new_status['IPK']}, sks_total = {$new_status['SKS_TOTAL']},
                        updated_on = sysdate, updated_by = {$user->ID_PENGGUNA}
                    WHERE id_mhs_status = {$new_status['ID_MHS_STATUS']}";
                
                if (! $this->db->Query($sql_update)) {
                    echo "Error at " . __LINE__ . ": " . print_r($this->db->Error, true);
                    $this->db->Rollback();
                    exit();
                }
            }
        }
        
        return $this->db->Commit();
    }
}
