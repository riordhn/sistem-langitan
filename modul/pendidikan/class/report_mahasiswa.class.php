<?php

/**
 * Description of report-mahasiswa
 *
 * @author 160590
 */
class report_mahasiswa {

    public $database;

    function __construct($database) {
        $this->database = $database;
    }
	
    function get_semester() {
        return $this->database->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER = 'Genap' or NM_SEMESTER = 'Ganjil' 
												ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
    }
    function get_kelamin($pt) {
        return $this->database->QueryToArray("
        	SELECT pguna.KELAMIN_PENGGUNA 
        	FROM PENGGUNA pguna
        	JOIN MAHASISWA mhs ON mhs.id_pengguna = pguna.id_pengguna
        	JOIN PERGURUAN_TINGGI pt ON pt.ID_PERGURUAN_TINGGI = pguna.ID_PERGURUAN_TINGGI
        	WHERE pguna.ID_PERGURUAN_TINGGI ='{$pt}'
        	ORDER BY pguna.KELAMIN_PENGGUNA ASC");
    }
	
    function get_jumlah_tagihan_mahasiswa($nim){
        return $this->database->QueryToArray("
            SELECT mhs.NIM_MHS, keuangan.JUMLAH_TAGIHAN, TO_DATE(TANGGAL_INPUT,
			'DD-MM-YYYY') AS TANGGAL, TO_CHAR(TANGGAL_INPUT,
			'HH24:MI:SS') AS WAKTU, s.NM_SEMESTER, s.TAHUN_AJARAN, s.FD_ID_SMT 
            FROM KEUANGAN_KHS keuangan
            JOIN MAHASISWA mhs ON mhs.ID_MHS = keuangan.ID_MHS
            JOIN semester s on s.id_semester = keuangan.id_semester  
            WHERE mhs.NIM_MHS = '{$nim}' AND s.FD_ID_SMT >='20181'
            ORDER BY s.THN_AKADEMIK_SEMESTER DESC, s.NM_SEMESTER DESC ");                
    }

    function get_program_studi_by_jenjang($id_jenjang) {
        return $this->database->QueryToArray("SELECT * FROM PROGRAM_STUDI WHERE ID_JENJANG={$id_jenjang} AND STATUS_AKTIF_PRODI = 1 
											ORDER BY NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }
	
	function get_program_studi_by_fakultas_jenjang($id_fakultas, $id_jenjang) {
        return $this->database->QueryToArray("SELECT * FROM PROGRAM_STUDI WHERE ID_JENJANG='$id_jenjang' and ID_FAKULTAS = '$id_fakultas' 
										AND STATUS_AKTIF_PRODI = 1
										ORDER BY NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }
	
	function get_program_studi_by_fakultas($id_fakultas) {
        return $this->database->QueryToArray("SELECT ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, NM_JENJANG FROM PROGRAM_STUDI 
		LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
		WHERE ID_FAKULTAS={$id_fakultas} AND STATUS_AKTIF_PRODI = 1 ORDER BY NM_JENJANG, NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }

	function get_semester_aktif() {
        return $this->database->QueryToArray("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True' 
										ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
    }

    function get_tahun_akademik() {
        return $this->database->QueryToArray("SELECT THN_AKADEMIK_SEMESTER FROM AUCC.SEMESTER GROUP BY THN_AKADEMIK_SEMESTER ORDER BY THN_AKADEMIK_SEMESTER DESC");
    }

    function get_fakultas($id_perguruan_tinggi) {
        return $this->database->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}' ORDER BY ID_FAKULTAS");
    }
	
	function get_status() {
        return $this->database->QueryToArray("SELECT ID_STATUS_PENGGUNA, NM_STATUS_PENGGUNA FROM STATUS_PENGGUNA WHERE ID_ROLE = 3 ORDER BY NM_STATUS_PENGGUNA");
    }
	
	function get_status_aktif() {
        return $this->database->QueryToArray("SELECT ID_STATUS_PENGGUNA, NM_STATUS_PENGGUNA FROM STATUS_PENGGUNA WHERE ID_ROLE = 3 AND 
												AKTIF_STATUS_PENGGUNA = 1");
    }
	
	function get_jenjang() {
        return $this->database->QueryToArray("SELECT * FROM JENJANG ORDER BY ID_JENJANG");
    }
	
	function get_jalur($id_pt = 0) {
        return $this->database->QueryToArray("SELECT * FROM JALUR WHERE ID_PERGURUAN_TINGGI = {$id_pt} ORDER BY NM_JALUR");
    }
	
	function get_jenis_kerjasama() {
        return $this->database->QueryToArray("SELECT * FROM JENIS_KERJASAMA ORDER BY KODE_NIM_ASING");
    }
	
	function get_benua() {
        return $this->database->QueryToArray("SELECT * FROM BENUA ORDER BY ID_BENUA");
    }
	
	
	function get_negara() {
        return $this->database->QueryToArray("SELECT * FROM NEGARA ORDER BY NM_NEGARA");
    }
	
	function get_prov_in() {
        return $this->database->QueryToArray("SELECT * FROM PROVINSI WHERE ID_NEGARA = 114 ORDER BY NM_PROVINSI");
    }
	
	function get_prov($negara) {
        return $this->database->QueryToArray("SELECT * FROM PROVINSI WHERE ID_NEGARA = '$negara' ORDER BY NM_PROVINSI");
    }
	
	function get_kota_by_prov($prov) {
        return $this->database->QueryToArray("SELECT * FROM KOTA WHERE ID_PROVINSI = '$prov' ORDER BY NM_KOTA");
    }
	
	function get_negara_by_benua($benua) {
        return $this->database->QueryToArray("SELECT * FROM NEGARA WHERE ID_BENUA = '$benua' ORDER BY ID_BENUA");
    }
	
	function get_jalur_penerimaan() {
        return $this->database->QueryToArray("SELECT * FROM JALUR WHERE ID_JALUR IN (SELECT ID_JALUR FROM PENERIMAAN) ORDER BY NM_JALUR");
    }
	
	function get_jenjang_penerimaan() {
        return $this->database->QueryToArray("SELECT * FROM JENJANG WHERE ID_JENJANG IN (SELECT ID_JENJANG FROM PENERIMAAN) ORDER BY ID_JENJANG");
    }
	
	function get_thn_penerimaan() {
        return $this->database->QueryToArray("SELECT TAHUN FROM PENERIMAAN GROUP BY TAHUN ORDER BY TAHUN DESC");
    }

	private function get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi) {
        if ($id_fakultas != "" && $id_prodi != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}' 
						AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_fakultas != "" && $id_prodi != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } elseif ($id_fakultas != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_prodi != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}' AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_prodi != "") {
            $query = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } elseif ($id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } else if ($id_fakultas != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' ";
        } else {
            $query = " ";
        }
        return $query;
    }
	
	
	function get_penerimaan_detail($fakultas, $id_penerimaan, $prodi, $jenjang, $jalur, $gelombang, $thn, $semester, $tgl_awal, $tgl_akhir) {
		if($fakultas <> '' and $id_penerimaan <> ''){
			$where  = "p.id_penerimaan = '$id_penerimaan' and f.id_fakultas = '$fakultas'";
		}elseif($prodi <> '' and $id_penerimaan <> ''){
			$where = "p.id_penerimaan = '$id_penerimaan' and ps.id_program_studi = '$prodi'";
		}else{
			
				if($jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
					$where = "p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' 
								and p.tahun = '$thn' and p.semester = '$semester'";
				}elseif($jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester == ''){
					$where = "p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' 
								and p.tahun = '$thn'";
				}elseif($jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester <> ''){
					$where = "p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' 
								 and p.semester = '$semester'";
				}elseif($jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester <> ''){
					$where = "p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' 
								and p.tahun = '$thn' and p.semester = '$semester'";
				}elseif($jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
					$where = "p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang' 
								and p.tahun = '$thn' and p.semester = '$semester'";
				}elseif($jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
					$where = "p.id_jalur = '$jalur' and p.gelombang = '$gelombang' 
								and p.tahun = '$thn' and p.semester = '$semester'";
				}elseif($jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester == ''){
					$where = "p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang'";
				}elseif($jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester == ''){
					$where = "p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.tahun = '$thn'";
				}elseif($jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester == ''){
					$where = "p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang' and p.tahun = '$thn' ";
				}elseif($jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester == ''){
					$where = "p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn'";
				}elseif($jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester <> ''){
					$where = "p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.semester = '$semester'";
				}elseif($jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester <> ''){
					$where = "p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang' and p.semester = '$semester'";
				}elseif($jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester <> ''){
					$where = "p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.semester = '$semester'";
				}elseif($jenjang <> '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester <> ''){
					$where = "p.id_jenjang = '$jenjang' and p.tahun = '$thn' and p.semester = '$semester'";
				}elseif($jenjang == '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester <> ''){
					$where = "p.id_jalur = '$jalur' and p.tahun = '$thn' and p.semester = '$semester'";
				}elseif($jenjang == '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
					$where = "p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
				}elseif($jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester == ''){
					$where = "p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur'";
				}elseif($jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester == ''){
					$where = "p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang'";
				}elseif($jenjang <> '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester == ''){
					$where = "p.id_jenjang = '$jenjang' and p.tahun = '$thn'=";
				}elseif($jenjang <> '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester <> ''){
					$where = "p.id_jenjang = '$jenjang' and p.semester = '$semester'";
				}elseif($jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester == ''){
					$where = "p.id_jalur = '$jalur' and p.gelombang = '$gelombang'";
				}elseif($jenjang == '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester == ''){
					$where = "p.id_jalur = '$jalur' and p.tahun = '$thn'";
				}elseif($jenjang == '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester <> ''){
					$where = "p.id_jalur = '$jalur' and p.semester = '$semester'";
				}elseif($jenjang == '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester == ''){
					$where = "p.gelombang = '$gelombang' and p.tahun = '$thn'";
				}elseif($jenjang == '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester <> ''){
					$where = "p.gelombang = '$gelombang' and p.semester = '$semester'";
				}elseif($jenjang == '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester <> ''){
					$where = "p.tahun = '$thn' and p.semester = '$semester'";
				}elseif($jenjang == '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester <> ''){
					$where = "p.semester = '$semester'";
				}elseif($jenjang == '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester == ''){
					$where = "p.tahun = '$thn'";
				}elseif($jenjang == '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester == ''){
					$where = "p.gelombang = '$gelombang'";
				}elseif($jenjang == '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester == ''){
					$where = "p.id_jalur = '$jalur'";
				}elseif($jenjang <> '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester == ''){
					$where = "p.id_jenjang = '$jenjang'";
				}

			if($fakultas <> ''){
				$where .=  " and f.id_fakultas = '$fakultas'";
			}
			
		}

		if($tgl_awal == '' and $tgl_akhir == ''){
			
		}else{
			$where .= " and cmb.id_c_mhs in (select distinct id_c_mhs from pembayaran_cmhs where tgl_bayar is not null
						and  tgl_bayar between to_date('$tgl_awal', 'DD-MM-YYYY') and to_date('$tgl_akhir', 'DD-MM-YYYY'))";
		}

		return $this->database->QueryToArray("select no_ujian, nim_mhs, nm_c_mhs, nm_fakultas, nm_program_studi, nm_jenjang, cmb.ALAMAT, cmb.TELP,
			NM_KOTA,
			to_char(tgl_diterima, 'MM/DD/YYYY') tgl_diterima,
            (select max(tgl_bayar) from pembayaran_cmhs pc where pc.id_c_mhs = cmb.id_c_mhs and (tgl_bayar is not null or id_status_pembayaran != 2)) tgl_bayar,
            to_char(tgl_verifikasi_pendidikan, 'MM/DD/YYYY') tgl_verifikasi_pendidikan,
            (select length(finger_data) from fingerprint_cmhs f where f.id_c_mhs = cmb.id_c_mhs) finger,
            tgl_cetak_ktm
        from calon_mahasiswa_baru cmb
		join program_studi ps on ps.id_program_studi = cmb.id_program_studi
		join fakultas f on f.id_fakultas = ps.id_fakultas
		join jenjang j on j.id_jenjang = ps.id_jenjang
		left join kota k on k.id_kota = cmb.id_kota
		join penerimaan p on p.id_penerimaan = cmb.id_penerimaan
        where  $where
        order by no_ujian");
	}
	
	function get_penerimaan($fakultas, $jenjang, $jalur, $gelombang, $thn, $semester, $tgl_awal, $tgl_akhir, $id_penerimaan) {
		if($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang'= and p.gelombang = '$gelombang' and p.tahun = '$thn'=";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and p.id_jalur = '$jalur' and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and p.id_jalur = '$jalur' and p.semester = '$semester' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester <> ''){
			$where = "and p.gelombang = '$gelombang' and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jenjang = '$jenjang'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.id_jalur = '$jalur'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.gelombang = '$gelombang'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.tahun = '$thn'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and ps.id_fakultas = '$fakultas' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.id_jalur = '$jalur' and p.gelombang = '$gelombang'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and p.id_jenjang = '$jenjang' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and p.id_jalur = '$jalur' and p.gelombang = '$gelombang'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and p.id_jalur = '$jalur' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and p.id_jalur = '$jalur' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn <> '' and $semester == ''){
			$where = "and p.gelombang = '$gelombang' and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester <> ''){
			$where = "and p.gelombang = '$gelombang' and p.semester = '$semester'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester <> ''){
			$where = "and p.tahun = '$thn' and p.semester = '$semester'";
		}elseif($fakultas <> '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and ps.id_fakultas = '$fakultas'";
		}elseif($fakultas == '' and $jenjang <> '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and p.id_jenjang = '$jenjang'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur <> '' and $gelombang == '' and $thn == '' and $semester == ''){
			$where = "and p.id_jalur = '$jalur'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang <> '' and $thn == '' and $semester == ''){
			$where = "and p.gelombang = '$gelombang'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn <> '' and $semester == ''){
			$where = "and p.tahun = '$thn'";
		}elseif($fakultas == '' and $jenjang == '' and $jalur == '' and $gelombang == '' and $thn == '' and $semester <> ''){
			$where = "and p.semester = '$semester'";
		}
		
		
		if($tgl_awal == '' and $tgl_akhir == ''){
			$where .= " "; 
			$where2 = "";
		}else{
			$where .= " and pp.id_program_studi in 
						(select id_program_studi from calon_mahasiswa_baru where id_c_mhs in 
							(select distinct id_c_mhs from pembayaran_cmhs 
						  		where tgl_bayar between to_date('$tgl_awal', 'DD-MM-YYYY') and to_date('$tgl_akhir', 'DD-MM-YYYY')
						 	)
						 )";
						 
			$where2 = " and id_c_mhs in (select distinct id_c_mhs from pembayaran_cmhs where tgl_bayar is not null
						and  tgl_bayar between to_date('$tgl_awal', 'DD-MM-YYYY') and to_date('$tgl_akhir', 'DD-MM-YYYY'))";
		}
		
		if($fakultas == ''){
			$select = "fak.id_fakultas, fak.nm_fakultas, pp.id_program_studi, ps.nm_program_studi, ";
			$orderby = "fak.id_fakultas asc ";
			$groupby = "group by fak.id_fakultas, fak.nm_fakultas, pp.id_penerimaan, p.tahun, j.nm_jalur, 
						p.nm_penerimaan, p.semester, p.gelombang, jg.nm_jenjang, pp.id_program_studi, ps.nm_program_studi ";
			$sum = "SUM((select count(id_c_mhs) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = p.id_penerimaan 
					and cmb.id_program_studi = ps.id_program_studi)) diterima, SUM(b.bayar) bayar, SUM(v.verifikasi) verifikasi, 
					SUM(f.finger) finger, SUM(c.verifikasi_keuangan) verifikasi_keuangan, 
					SUM((select count(id_c_mhs) from calon_mahasiswa_baru cmb 
					where cmb.id_penerimaan = p.id_penerimaan and cmb.id_program_studi = ps.id_program_studi 
					and cmb.tgl_cetak_ktm is not null)) ktm,
					sum((select count(id_c_mhs) from calon_mahasiswa_baru cmb 
					where cmb.tgl_regmaba is not null and cmb.id_penerimaan = p.id_penerimaan and cmb.id_program_studi = ps.id_program_studi
					$where2)) regmaba, sum((select count(cmb.id_c_mhs) from calon_mahasiswa_baru cmb, calon_mahasiswa_data cmd 
					where cmd.status_bidik_misi = 1 and cmb.id_c_mhs = cmd.id_c_mhs and cmb.id_penerimaan = p.id_penerimaan and cmb.id_program_studi = ps.id_program_studi
					$where2)) bidik_misi";
					

		}else{
			$select = "pp.id_program_studi, ps.nm_program_studi, fak.id_fakultas, fak.nm_fakultas, ";	
			$orderby = "ps.id_jenjang, ps.nm_program_studi asc, p.tahun desc, p.semester desc, p.id_jalur, p.gelombang ";
			$groupby = "";
			$sum = "(select count(id_c_mhs) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = p.id_penerimaan and 
					cmb.id_program_studi = ps.id_program_studi) diterima, b.bayar, v.verifikasi, f.finger, verifikasi_keuangan,
          (select count(id_c_mhs) from calon_mahasiswa_baru cmb where cmb.id_penerimaan = p.id_penerimaan 
		  and cmb.id_program_studi = ps.id_program_studi and cmb.tgl_cetak_ktm is not null  $where2) ktm,
		  (select count(id_c_mhs) from calon_mahasiswa_baru cmb 
					where cmb.tgl_regmaba is not null and cmb.id_penerimaan = p.id_penerimaan and cmb.id_program_studi = ps.id_program_studi
					  $where2) regmaba";
		}
		
		
		
		if($id_penerimaan != ''){
				$where = " and pp.id_penerimaan = '$id_penerimaan'";
		}

        return $this->database->QueryToArray("select $select pp.id_penerimaan, p.tahun, j.nm_jalur, 
		p.nm_penerimaan, p.semester, p.gelombang, jg.nm_jenjang, $sum  
        from penerimaan_prodi pp
        join penerimaan p on p.id_penerimaan = pp.id_penerimaan
        join jalur j on j.id_jalur = p.id_jalur
        join program_studi ps on ps.id_program_studi = pp.id_program_studi
        join jenjang jg on jg.id_jenjang = ps.id_jenjang
		join fakultas fak on fak.id_fakultas = ps.id_fakultas
        left join (
            select id_penerimaan, id_program_studi, count(calon_mahasiswa_baru.id_c_mhs) bayar from calon_mahasiswa_baru 
			join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
            where (status_bidik_misi = 0 or status_bidik_misi is null) and calon_mahasiswa_baru.id_c_mhs in 
			(select distinct id_c_mhs from pembayaran_cmhs where tgl_bayar is not null or id_status_pembayaran != 2) $where2
            group by id_penerimaan, id_program_studi) b on (b.id_penerimaan = pp.id_penerimaan and b.id_program_studi = pp.id_program_studi)
        left join (
            select id_penerimaan, id_program_studi, count(id_c_mhs) verifikasi from calon_mahasiswa_baru
            where tgl_verifikasi_pendidikan is not null $where2
            group by id_penerimaan, id_program_studi) v on (v.id_penerimaan = pp.id_penerimaan and v.id_program_studi = pp.id_program_studi)
		left join (
            select id_penerimaan, id_program_studi, count(id_c_mhs) verifikasi_keuangan from calon_mahasiswa_baru
            where tgl_verifikasi_keuangan is not null $where2
            group by id_penerimaan, id_program_studi) c on (c.id_penerimaan = pp.id_penerimaan and c.id_program_studi = pp.id_program_studi)
        left join (
            select id_penerimaan, id_program_studi, count(id_c_mhs) finger from calon_mahasiswa_baru
            where id_c_mhs in (select id_c_mhs from fingerprint_cmhs where finger_data is not null) $where2
            group by id_penerimaan, id_program_studi) f on (f.id_penerimaan = pp.id_penerimaan and f.id_program_studi = pp.id_program_studi)
        where pp.is_aktif = 1  $where 
		$groupby
        order by $orderby");

    }

    function get_fakultas_prodi() {
        return $this->database->QueryToArray("
            SELECT UPPER(F.NM_FAKULTAS) AS FAKULTAS,J.NM_JENJANG,PS.NM_PROGRAM_STUDI,PS.ID_PROGRAM_STUDI
            FROM AUCC.FAKULTAS F 
            JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_FAKULTAS = F.ID_FAKULTAS
            JOIN AUCC.JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
			WHERE PS.ID_PROGRAM_STUDI !=197
            ORDER BY F.ID_FAKULTAS,PS.ID_PROGRAM_STUDI, J.ID_JENJANG");
    }
	
	
	function get_mhs_aktif() {
        return $this->database->QueryToArray("
            SELECT PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, JENJANG.ID_JENJANG, KELAMIN_PENGGUNA, 
			COUNT(ID_MHS) AS JUMLAH
			FROM MAHASISWA
			LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI 
			LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
			LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
			LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
			WHERE STATUS_PENGGUNA.AKTIF_STATUS_PENGGUNA = 1 AND PROGRAM_STUDI.ID_PROGRAM_STUDI !=197 AND KELAMIN_PENGGUNA IS NOT NULL
			GROUP BY PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, JENJANG.ID_JENJANG, KELAMIN_PENGGUNA
			ORDER BY PROGRAM_STUDI.ID_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, JENJANG.ID_JENJANG, KELAMIN_PENGGUNA");
    }
	

    function get_jumlah_mahasiswa_all() {
        return $this->database->QueryToArray("
        SELECT MAHASISWA.ID_PROGRAM_STUDI, THN_ANGKATAN_MHS, PROGRAM_STUDI.ID_JENJANG, COUNT(ID_MHS) AS JUMLAH FROM MAHASISWA
LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
WHERE STATUS_PENGGUNA.STATUS_AKTIF = 1
GROUP BY MAHASISWA.ID_PROGRAM_STUDI, THN_ANGKATAN_MHS, PROGRAM_STUDI.ID_JENJANG
ORDER BY THN_ANGKATAN_MHS DESC, MAHASISWA.ID_PROGRAM_STUDI, PROGRAM_STUDI.ID_JENJANG ASC");
    }

    function get_mahasiswa($fakultas='', $program_studi='', $tahun_akademik='', $status='') {
        if (empty($fakultas) && empty($program_studi) && empty($tahun_akademik) && empty($status)) {
            $query = "
                SELECT M.NIM_MHS AS NIM,UPPER(P.NM_PENGGUNA) AS NAMA,M.THN_ANGKATAN_MHS AS ANGKATAN,J.NM_JENJANG AS JENJANG,PS.NM_PROGRAM_STUDI AS PRODI,UPPER(F.NM_FAKULTAS)  AS FAKULTAS,M.STATUS 
                FROM AUCC.PENGGUNA P
                JOIN AUCC.MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                JOIN AUCC.JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
                JOIN AUCC.FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
                WHERE 
					M.ID_MHS IN (SELECT ID_MHS FROM AUCC.PEMBAYARAN WHERE TGL_BAYAR IS NOT NULL)
					OR
					M.ID_MHS IN (SELECT M.ID_MHS FROM AUCC.MAHASISWA M
						JOIN AUCC.CALON_MAHASISWA_BARU CM ON CM.ID_C_MHS=M.ID_C_MHS 
						JOIN AUCC.PEMBAYARAN_CMHS PCM ON PCM.ID_C_MHS=CM.ID_C_MHS
						WHERE PCM.TGL_BAYAR IS NOT NULL)		
                ORDER BY F.ID_FAKULTAS,PS.NM_PROGRAM_STUDI,ANGKATAN,PS.ID_JENJANG,NIM
                ";
        } else if (isset($fakultas) && empty($program_studi) && empty($tahun_akademik) && empty($status)) {
            $query = "
                SELECT M.NIM_MHS AS NIM,UPPER(P.NM_PENGGUNA) AS NAMA,M.THN_ANGKATAN_MHS AS ANGKATAN,J.NM_JENJANG AS JENJANG,PS.NM_PROGRAM_STUDI AS PRODI,UPPER(F.NM_FAKULTAS)  AS FAKULTAS,M.STATUS 
                FROM AUCC.PENGGUNA P
                JOIN AUCC.MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                JOIN AUCC.JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
                JOIN AUCC.FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
                WHERE F.ID_FAKULTAS='{$fakultas}' AND
					(
						M.ID_MHS IN (SELECT ID_MHS FROM AUCC.PEMBAYARAN WHERE TGL_BAYAR IS NOT NULL)
						OR
						M.ID_MHS IN (SELECT M.ID_MHS FROM AUCC.MAHASISWA M
						JOIN AUCC.CALON_MAHASISWA_BARU CM ON CM.ID_C_MHS=M.ID_C_MHS 
						JOIN AUCC.PEMBAYARAN_CMHS PCM ON PCM.ID_C_MHS=CM.ID_C_MHS
						WHERE PCM.TGL_BAYAR IS NOT NULL)
					)
                ORDER BY F.ID_FAKULTAS,PS.NM_PROGRAM_STUDI,ANGKATAN,PS.ID_JENJANG,M.NIM_MHS
                ";
        } else if (empty($fakultas) && empty($program_studi) && isset($tahun_akademik) && empty($status)) {
            $query = "
                SELECT M.NIM_MHS AS NIM,UPPER(P.NM_PENGGUNA) AS NAMA,M.THN_ANGKATAN_MHS AS ANGKATAN,J.NM_JENJANG AS JENJANG,PS.NM_PROGRAM_STUDI AS PRODI,UPPER(F.NM_FAKULTAS)  AS FAKULTAS,M.STATUS
                FROM AUCC.PENGGUNA P
                JOIN AUCC.MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                JOIN AUCC.JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
                JOIN AUCC.FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
                WHERE M.THN_ANGKATAN_MHS='{$tahun_akademik}' AND 
					(
						M.ID_MHS IN (SELECT ID_MHS FROM AUCC.PEMBAYARAN WHERE TGL_BAYAR IS NOT NULL)
						OR
						M.ID_MHS IN (SELECT M.ID_MHS FROM AUCC.MAHASISWA M
						JOIN AUCC.CALON_MAHASISWA_BARU CM ON CM.ID_C_MHS=M.ID_C_MHS 
						JOIN AUCC.PEMBAYARAN_CMHS PCM ON PCM.ID_C_MHS=CM.ID_C_MHS
						WHERE PCM.TGL_BAYAR IS NOT NULL)
					)
                ORDER BY F.ID_FAKULTAS,PS.NM_PROGRAM_STUDI,ANGKATAN,PS.ID_JENJANG,M.NIM_MHS
                ";
        } else if (isset($fakultas) && empty($program_studi) && isset($tahun_akademik) && empty($status)) {
            $query = "
                SELECT M.NIM_MHS AS NIM,UPPER(P.NM_PENGGUNA) AS NAMA,M.THN_ANGKATAN_MHS AS ANGKATAN,J.NM_JENJANG AS JENJANG,PS.NM_PROGRAM_STUDI AS PRODI,UPPER(F.NM_FAKULTAS)  AS FAKULTAS,M.STATUS 
                FROM AUCC.PENGGUNA P
                JOIN AUCC.MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                JOIN AUCC.JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
                JOIN AUCC.FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
                WHERE F.ID_FAKULTAS='{$fakultas}' AND M.THN_ANGKATAN_MHS='{$tahun_akademik}' AND 
					(
						M.ID_MHS IN (SELECT ID_MHS FROM AUCC.PEMBAYARAN WHERE TGL_BAYAR IS NOT NULL)
						OR
						M.ID_MHS IN (SELECT M.ID_MHS FROM AUCC.MAHASISWA M
						JOIN AUCC.CALON_MAHASISWA_BARU CM ON CM.ID_C_MHS=M.ID_C_MHS 
						JOIN AUCC.PEMBAYARAN_CMHS PCM ON PCM.ID_C_MHS=CM.ID_C_MHS
						WHERE PCM.TGL_BAYAR IS NOT NULL)
					)
                ORDER BY F.ID_FAKULTAS,PS.NM_PROGRAM_STUDI,ANGKATAN,PS.ID_JENJANG,M.NIM_MHS
                ";
        } else if (isset($fakultas) && isset($program_studi) && empty($tahun_akademik) && empty($status)) {
            $query = "
                SELECT M.NIM_MHS AS NIM,UPPER(P.NM_PENGGUNA) AS NAMA,M.THN_ANGKATAN_MHS AS ANGKATAN,J.NM_JENJANG AS JENJANG,PS.NM_PROGRAM_STUDI AS PRODI,UPPER(F.NM_FAKULTAS)  AS FAKULTAS,M.STATUS 
                FROM AUCC.PENGGUNA P
                JOIN AUCC.MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                JOIN AUCC.JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
                JOIN AUCC.FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
                WHERE PS.ID_PROGRAM_STUDI='{$program_studi}' AND 
					(
						M.ID_MHS IN (SELECT ID_MHS FROM AUCC.PEMBAYARAN WHERE TGL_BAYAR IS NOT NULL)
						OR
						M.ID_MHS IN (SELECT M.ID_MHS FROM AUCC.MAHASISWA M
						JOIN AUCC.CALON_MAHASISWA_BARU CM ON CM.ID_C_MHS=M.ID_C_MHS 
						JOIN AUCC.PEMBAYARAN_CMHS PCM ON PCM.ID_C_MHS=CM.ID_C_MHS
						WHERE PCM.TGL_BAYAR IS NOT NULL)
					)
                ORDER BY F.ID_FAKULTAS,PS.NM_PROGRAM_STUDI,ANGKATAN,PS.ID_JENJANG,M.NIM_MHS
                ";
        } else if (isset($fakultas) && isset($program_studi) && isset($tahun_akademik) && empty($status)) {
            $query = "
                SELECT M.NIM_MHS AS NIM,UPPER(P.NM_PENGGUNA) AS NAMA,M.THN_ANGKATAN_MHS AS ANGKATAN,J.NM_JENJANG AS JENJANG,PS.NM_PROGRAM_STUDI AS PRODI,UPPER(F.NM_FAKULTAS)  AS FAKULTAS,M.STATUS
                FROM AUCC.PENGGUNA P
                JOIN AUCC.MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
                JOIN AUCC.PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
                JOIN AUCC.JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
                JOIN AUCC.FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS 
                WHERE PS.ID_PROGRAM_STUDI='{$program_studi}' AND M.THN_ANGKATAN_MHS='{$tahun_akademik}' AND 
					(
						M.ID_MHS IN (SELECT ID_MHS FROM AUCC.PEMBAYARAN WHERE TGL_BAYAR IS NOT NULL)
						OR
						M.ID_MHS IN (SELECT M.ID_MHS FROM AUCC.MAHASISWA M
						JOIN AUCC.CALON_MAHASISWA_BARU CM ON CM.ID_C_MHS=M.ID_C_MHS 
						JOIN AUCC.PEMBAYARAN_CMHS PCM ON PCM.ID_C_MHS=CM.ID_C_MHS
						WHERE PCM.TGL_BAYAR IS NOT NULL)
					)
                ORDER BY F.ID_FAKULTAS,PS.NM_PROGRAM_STUDI,ANGKATAN,PS.ID_JENJANG,M.NIM_MHS
                ";
        }
        return $this->database->QueryToArray($query);
    }

    function get_hasil_cari_mahasiswa($var,$id_perguruan_tinggi) {
	$var_trim = trim($var);
        /*$this->database->Query("SELECT * FROM CALON_MAHASISWA_BARU 
			WHERE NIM_MHS LIKE '%$var_trim%' OR UPPER(NM_C_MHS) LIKE UPPER('%$var_trim%') OR NO_UJIAN LIKE '%$var_trim%' 
            ");
        $row = $this->database->FetchAssoc();


		if($row){
			
		return $this->database->QueryToArray("
            SELECT MAHASISWA.NIM_MHS, NO_UJIAN, NM_JALUR, NM_PENGGUNA, NM_STATUS_PENGGUNA, 
			(CASE WHEN ID_JALUR_AKTIF = 1 THEN 'Aktif' ELSE 'Tidak Aktif' END) AS STATUS_JALUR, 
			NM_FAKULTAS, NM_PROGRAM_STUDI, NM_JENJANG
			FROM MAHASISWA 
			LEFT JOIN CALON_MAHASISWA_BARU ON MAHASISWA.ID_C_MHS = CALON_MAHASISWA_BARU.ID_C_MHS
			LEFT JOIN JALUR_MAHASISWA ON JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS
			LEFT JOIN JALUR ON JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
			LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
			LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
			JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
			JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
			JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
            WHERE MAHASISWA.NIM_MHS LIKE TRIM('%$var_trim%') OR UPPER(NM_PENGGUNA) LIKE UPPER('%$var_trim%') OR NO_UJIAN LIKE '%$var_trim%'
			order by nim_mhs asc
            ");
		}else{
		*/
		return $this->database->QueryToArray(
			"SELECT MAHASISWA.NIM_MHS, NO_UJIAN, NM_JALUR, NM_PENGGUNA, NM_STATUS_PENGGUNA, 
			--(CASE WHEN IS_JALUR_AKTIF = 1 THEN 'Aktif' ELSE 'Tidak Aktif' END) AS STATUS_JALUR, 
			NM_FAKULTAS, NM_PROGRAM_STUDI, NM_JENJANG
			FROM MAHASISWA 			
			--LEFT JOIN JALUR_MAHASISWA ON JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS
			--LEFT JOIN JALUR ON JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
			LEFT JOIN ADMISI ON ADMISI.ID_MHS = MAHASISWA.ID_MHS AND ADMISI.ID_JALUR IS NOT NULL
			LEFT JOIN JALUR ON JALUR.ID_JALUR = ADMISI.ID_JALUR
			LEFT JOIN CALON_MAHASISWA_BARU ON MAHASISWA.ID_C_MHS = CALON_MAHASISWA_BARU.ID_C_MHS
			JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA AND PENGGUNA.ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}'
			JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
			JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
			JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
			JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
            WHERE MAHASISWA.NIM_MHS LIKE TRIM('%$var_trim%') OR UPPER(NM_PENGGUNA) LIKE UPPER('%$var_trim%') OR NO_UJIAN LIKE '%$var_trim%'
			
			UNION ALL

			SELECT MAHASISWA_ASING.NIM_MHS_ASING AS NIM_MHS, '' AS NO_UJIAN, NM_JENIS_KERJASAMA AS NM_JALUR, NM_PENGGUNA, NM_STATUS_PENGGUNA, 
			--'Aktif' AS STATUS_JALUR, 
			NM_FAKULTAS, NM_PROGRAM_STUDI, NM_JENJANG
			FROM MAHASISWA_ASING
			JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA_ASING.ID_PENGGUNA AND PENGGUNA.ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}'
			LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA_ASING.ID_STATUS_PENGGUNA
			JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA_ASING.ID_PROGRAM_STUDI
			JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
			JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
			LEFT JOIN JENIS_KERJASAMA ON JENIS_KERJASAMA.ID_JENIS_KERJASAMA = MAHASISWA_ASING.ID_JENIS_KERJASAMA
            WHERE MAHASISWA_ASING.NIM_MHS_ASING LIKE TRIM('%$var_trim%') OR UPPER(NM_PENGGUNA) LIKE TRIM(UPPER('%$var_trim%'))

			ORDER BY NIM_MHS");
		//}
    }

    function get_biodata_mahasiswa($nim,$id_perguruan_tinggi) {
    	// tambahan query baru utk menangani c_mhs agar tampil info lengkap
    	// ==== FIKRIE (01-06-2016) ====
        $this->database->Query("
            SELECT M.*, P.*, J.*, F.*, PS.*, K.*, CMD.*, (SELECT k.NM_KOTA FROM KOTA k WHERE k.ID_KOTA = CM.ID_KOTA_LAHIR) AS NM_KOTA_LAHIR_CMB,
				CM.TELP AS TELP_MHS_CMB, LOWER(CM.EMAIL) AS EMAIL_MHS_CMB, 
				(CM.ALAMAT_JALAN || ' ' || CM.ALAMAT_DUSUN || ' ' || CM.ALAMAT_KELURAHAN || ' ' || 
					CM.ALAMAT_KECAMATAN || ' ' || (SELECT k.NM_KOTA FROM KOTA k WHERE k.ID_KOTA = CM.ID_KOTA)) AS ALAMAT_MHS_CMB, 
				(SELECT k.NM_KOTA FROM KOTA k WHERE k.ID_KOTA = CMD.ID_KOTA_AYAH) AS KOTA_AYAH_CMB,
				(SELECT k.NM_KOTA FROM KOTA k WHERE k.ID_KOTA = CMD.ID_KOTA_IBU) AS KOTA_IBU_CMB,
				M.ID_C_MHS AS ID_C_MHS_PARAM, (SELECT k.NM_KOTA FROM KOTA k WHERE k.ID_KOTA = M.ALAMAT_ASAL_MHS_KOTA) AS ALAMAT_KOTA_MHS, (SELECT p.NM_PROVINSI FROM PROVINSI p WHERE p.ID_PROVINSI = M.ALAMAT_ASAL_MHS_PROV) AS ALAMAT_PROV_MHS, CM.NO_UJIAN
            FROM AUCC.PENGGUNA P 
            LEFT JOIN AUCC.MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
			LEFT JOIN AUCC.KOTA K ON K.ID_KOTA = M.LAHIR_KOTA_MHS
            LEFT JOIN AUCC.CALON_MAHASISWA_BARU CM ON CM.ID_C_MHS = M.ID_C_MHS       
            LEFT JOIN AUCC.PROGRAM_STUDI PS ON M.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            LEFT JOIN AUCC.JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
            LEFT JOIN AUCC.FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
			LEFT JOIN AUCC.CALON_MAHASISWA_ORTU CMD ON CM.ID_C_MHS = CMD.ID_C_MHS 
            WHERE M.NIM_MHS='{$nim}' AND P.ID_PERGURUAN_TINGGI = {$id_perguruan_tinggi}
            ");
        return $this->database->FetchAssoc();
    }
	function get_detail_pembayaran_mahasiswa($nim){
        return $this->database->QueryToArray("
            SELECT 
              M.NIM_MHS,S.TAHUN_AJARAN,S.NM_SEMESTER,BNK.NM_BANK,BNVK.NAMA_BANK_VIA,PEM.TGL_BAYAR,
              PEM.NO_TRANSAKSI,PEM.KETERANGAN,B.NM_BIAYA,PEM.BESAR_BIAYA,PEM.DENDA_BIAYA
            FROM AUCC.MAHASISWA M
            LEFT JOIN AUCC.PEMBAYARAN PEM ON PEM.ID_MHS = M.ID_MHS
            LEFT JOIN AUCC.SEMESTER S ON S.ID_SEMESTER = PEM.ID_SEMESTER
            JOIN AUCC.BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            JOIN AUCC.BANK_VIA BNVK ON BNVK.ID_BANK_VIA = PEM.ID_BANK_VIA
            JOIN AUCC.DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            JOIN AUCC.BIAYA B ON B.ID_BIAYA = DB.ID_BIAYA
            WHERE M.NIM_MHS = '{$nim}'");                
    }
    function get_detail_akademik_mata_kuliah($nim){
        return $this->database->QueryToArray("
            SELECT 
                M.NIM_MHS,MK.KD_MATA_KULIAH,MK.KD_MATA_KULIAH2,MK.NM_MATA_KULIAH,MK.KREDIT_SEMESTER,PMK.NILAI_HURUF
            FROM AUCC.MAHASISWA M 
            LEFT JOIN AUCC.PENGAMBILAN_MK PMK ON PMK.ID_MHS=M.ID_MHS
            JOIN AUCC.KELAS_MK KMK ON KMK.ID_KELAS_MK = PMK.ID_KELAS_MK
            JOIN AUCC.KURIKULUM_MK KUMK ON (KUMK.ID_KURIKULUM_MK = KMK.ID_KURIKULUM_MK OR PMK.ID_KURIKULUM_MK = KUMK.ID_KURIKULUM_MK)
            JOIN AUCC.MATA_KULIAH MK ON MK.ID_MATA_KULIAH = KUMK.ID_MATA_KULIAH
            WHERE M.NIM_MHS='{$nim}'
            ORDER BY MK.NM_MATA_KULIAH");
    }
    function get_detail_akademik_status($nim){
        return $this->database->QueryToArray("
            SELECT 
                M.NIM_MHS,S.TAHUN_AJARAN,S.NM_SEMESTER,MS.IPK_MHS_STATUS AS IPK,MS.IPS_MHS_STATUS AS IPS,MS.SKS_TOTAL_MHS_STATUS AS TOTAL_SKS
            FROM AUCC.MAHASISWA M 
            LEFT JOIN AUCC.MHS_STATUS MS ON M.ID_MHS = MS.ID_MHS
            JOIN AUCC.SEMESTER S ON MS.ID_SEMESTER = S.ID_SEMESTER
            WHERE M.NIM_MHS='{$nim}'
            ORDER BY S.TAHUN_AJARAN,S.NM_SEMESTER ASC");
    }


    function get_program_studi($id_fakultas) {
        return $this->database->QueryToArray("SELECT * FROM PROGRAM_STUDI PS JOIN JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG WHERE ID_FAKULTAS ='{$id_fakultas}' ORDER BY ID_PROGRAM_STUDI");
    }

	/***
	 * @param $mode 1-Untuk data; 2-untuk Query
	 * @param $fakultas
	 * @param $program_studi
	 * @param $tahun_akademik
	 * @param $status
	 * @param $jenjang
	 * @param $jalur
	 * @param int $id_perguruan_tinggi
	 * @param string $bidikmisi
	 * @param string $kota_mhs
	 * @param string $tahun_lulus
	 * @param string $kelamin
	 * @return string|mixed
	 */
	function get_mahasiswa_dayat($mode, $fakultas, $program_studi, $tahun_akademik, $status, $jenjang, $jalur, $id_perguruan_tinggi = 0, $bidikmisi = '', $kota_mhs = '', $tahun_lulus = '', $kelamin = '')
	{

		$condition_akademik = $this->get_condition_akademik($fakultas, $jenjang, $program_studi);
		$where = "";

		// ROMBAK BY FIKRIE (10-09-2018)
		if ($tahun_akademik != '') {
			$where .= " AND THN_ANGKATAN_MHS = '$tahun_akademik' ";
		}
		if ($jalur != '') {
			$where .= " AND JALUR.ID_JALUR = '$jalur' ";
		}
		if ($status != '') {
			$where .= " AND STATUS_AKADEMIK_MHS = '$status' ";
		}
		if ($bidikmisi != '') {
			if ($bidikmisi == 100) {
				$bidikmisi = 0;
			}
			$where .= " AND CM.IS_AJUKAN_BIDIKMISI = '$bidikmisi' ";
		}
		if ($kota_mhs != '') {
			$where .= " AND ALAMAT_ASAL_MHS_KOTA = '$kota_mhs' ";
		}
		if ($tahun_lulus != '') {
			$where .= " AND CS.TAHUN_LULUS = '$tahun_lulus' ";
		}

		if ($kelamin != '') {
			if ($kelamin == 1000) {
				$where .= " AND PENGGUNA.KELAMIN_PENGGUNA IS NULL ";
			} else {
				$where .= " AND PENGGUNA.KELAMIN_PENGGUNA = '$kelamin' ";
			}
		}

		$query = "SELECT MAHASISWA.NIM_MHS, NM_PENGGUNA, KELAMIN_PENGGUNA, THN_ANGKATAN_MHS, NM_JENJANG, NM_PROGRAM_STUDI, NM_FAKULTAS, NM_STATUS_PENGGUNA, 
				(MAHASISWA.ALAMAT_ASAL_MHS || ' RT. ' || MAHASISWA.ALAMAT_RT_ASAL_MHS || ' RW. ' || MAHASISWA.ALAMAT_RW_ASAL_MHS || ' ' || 
					MAHASISWA.ALAMAT_DUSUN_ASAL_MHS || ' ' || MAHASISWA.ALAMAT_KELURAHAN_ASAL_MHS || ' ' || MAHASISWA.ALAMAT_KECAMATAN_ASAL_MHS || ' ' || (SELECT k.NM_KOTA FROM KOTA k WHERE k.ID_KOTA = MAHASISWA.ALAMAT_ASAL_MHS_KOTA) || ' ' || (SELECT p.NM_PROVINSI FROM PROVINSI p WHERE p.ID_PROVINSI = MAHASISWA.ALAMAT_ASAL_MHS_PROV)) AS ALAMAT_MHS_BARU,
				MOBILE_MHS, NM_KOTA AS KOTA_LAHIR, TO_CHAR(TGL_LAHIR_PENGGUNA, 'MM/DD/YYYY') TGL_LAHIR_PENGGUNA, 
				(SELECT s.NM_SEKOLAH FROM SEKOLAH s WHERE s.ID_SEKOLAH = CS.ID_SEKOLAH_ASAL) AS NM_SEKOLAH,
				CM.NO_UJIAN, CM.KODE_VOUCHER,
				ALAMAT_MHS, 
				MAHASISWA.ALAMAT_ASAL_MHS, MAHASISWA.ALAMAT_RT_ASAL_MHS, MAHASISWA.ALAMAT_RW_ASAL_MHS,MAHASISWA.ALAMAT_DUSUN_ASAL_MHS, MAHASISWA.ALAMAT_KELURAHAN_ASAL_MHS, MAHASISWA.ALAMAT_KECAMATAN_ASAL_MHS, (SELECT k.NM_KOTA FROM KOTA k WHERE k.ID_KOTA = MAHASISWA.ALAMAT_ASAL_MHS_KOTA) AS ALAMAT_KOTA_MHS, (SELECT p.NM_PROVINSI FROM PROVINSI p WHERE p.ID_PROVINSI = MAHASISWA.ALAMAT_ASAL_MHS_PROV) AS ALAMAT_PROV_MHS,
				MAHASISWA.ID_C_MHS AS ID_C_MHS_PARAM, CM.TELP AS TELP_MHS_CMB, NM_AYAH_MHS, NM_IBU_MHS,
				(CM.ALAMAT_JALAN || ' ' || CM.ALAMAT_DUSUN || ' ' || CM.ALAMAT_KELURAHAN || ' ' || 
					CM.ALAMAT_KECAMATAN || ' ' || (SELECT k.NM_KOTA FROM KOTA k WHERE k.ID_KOTA = CM.ID_KOTA)) AS ALAMAT_MHS_CMB, 
				CS.NISN, CM.NIK_C_MHS,
				(SELECT COUNT(*) AS CEK FROM MAHASISWA m
					WHERE m.ID_MHS = MAHASISWA.ID_MHS AND lahir_prop_mhs IS NOT NULL AND lahir_kota_mhs IS NOT NULL AND alamat_asal_mhs IS NOT NULL 
					AND mobile_mhs IS NOT NULL AND id_sekolah_asal_mhs IS NOT NULL 
					AND nm_ayah_mhs IS NOT NULL AND nm_ibu_mhs IS NOT NULL 
					AND ASAL_KOTA_MHS IS NOT NULL AND ASAL_PROV_MHS IS NOT NULL 
					AND	ALAMAT_ASAL_MHS IS NOT NULL AND ALAMAT_ASAL_MHS_PROV IS NOT NULL 
					AND ALAMAT_ASAL_MHS_KOTA IS NOT NULL) AS CEK_KELENGKAPAN,
				AGAMA.NM_AGAMA, PENGGUNA.EMAIL_ALTERNATE, JALUR.NM_JALUR
				FROM MAHASISWA
				LEFT JOIN CALON_MAHASISWA_BARU CM ON CM.ID_C_MHS = MAHASISWA.ID_C_MHS
				LEFT JOIN CALON_MAHASISWA_SEKOLAH CS ON CS.ID_C_MHS = CM.ID_C_MHS
				JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA AND PENGGUNA.ID_PERGURUAN_TINGGI = {$id_perguruan_tinggi}
				LEFT JOIN AGAMA ON AGAMA.ID_AGAMA = PENGGUNA.ID_AGAMA
				JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
				JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				LEFT JOIN JALUR_MAHASISWA ON JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS AND IS_JALUR_AKTIF = 1
				LEFT JOIN JALUR ON JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
				LEFT JOIN KOTA ON KOTA.ID_KOTA = LAHIR_KOTA_MHS
				WHERE PROGRAM_STUDI.STATUS_AKTIF_PRODI = 1 {$where} {$condition_akademik}
				ORDER BY FAKULTAS.ID_FAKULTAS,PROGRAM_STUDI.NM_PROGRAM_STUDI,THN_ANGKATAN_MHS,JENJANG.ID_JENJANG,MAHASISWA.NIM_MHS";

		// return array query
		if ($mode == 1) {
			return $this->database->QueryToArray($query);
		} // return string query (untuk cetak)
		elseif ($mode == 2) {
			return $query;
		}
	}

	function get_detail_akademik_khs($nim, $id_perguruan_tinggi)
	{
		$semester_set = $this->database->QueryToArray(
			"SELECT id_semester, nm_semester, tahun_ajaran FROM semester WHERE id_semester IN (
				SELECT distinct pmk.id_semester FROM pengambilan_mk pmk
				JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
				JOIN pengguna p ON p.id_pengguna = mhs.id_pengguna
				WHERE pmk.status_apv_pengambilan_mk = 1 AND mhs.nim_mhs = '{$nim}' AND p.id_perguruan_tinggi = {$id_perguruan_tinggi}
			)
			ORDER BY thn_akademik_semester DESC, nm_semester DESC"
		);

		foreach ($semester_set as &$semester)
		{
			$semester['pengambilan_set'] = $this->database->QueryToArray(
				"SELECT 
					nvl(mk.kd_mata_kuliah, mk2.kd_mata_kuliah) kd_mata_kuliah,
					nvl(mk.nm_mata_kuliah, mk2.nm_mata_kuliah) nm_mata_kuliah,
					nvl(mk.kredit_semester, mk2.kredit_semester) kredit_semester, 
					pmk.nilai_angka, pmk.nilai_huruf
				FROM pengambilan_mk pmk
				LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
				LEFT JOIN mata_kuliah mk ON mk.id_mata_kuliah = kls.id_mata_kuliah
				LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
				LEFT JOIN mata_kuliah mk2 on mk2.id_mata_kuliah = kmk.id_mata_kuliah
				LEFT JOIN nama_kelas nk ON nk.id_nama_kelas = kls.no_kelas_mk
				JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
				JOIN pengguna p ON p.id_pengguna = mhs.id_pengguna
				JOIN semester S ON S.id_semester = pmk.id_semester
				WHERE 
					p.id_perguruan_tinggi = {$id_perguruan_tinggi} AND 
					mhs.nim_mhs = '{$nim}' AND s.id_semester = {$semester['ID_SEMESTER']} AND 
					pmk.status_apv_pengambilan_mk = 1"
			);
		}

		return $semester_set;
	}

	function get_detail_aktivitas($nim, $id_perguruan_tinggi)
	{
		return $this->database->QueryToArray(
			"SELECT 
				s.tahun_ajaran, s.nm_semester, sp.nm_status_pengguna, ms.sks_semester, ms.ips, ms.sks_total, ms.ipk
			FROM mahasiswa_status ms
			JOIN mahasiswa M ON M.id_mhs = ms.id_mhs
			JOIN pengguna P ON P.id_pengguna = M.id_pengguna
			JOIN status_pengguna sp ON sp.id_status_pengguna = ms.id_status_pengguna
			JOIN semester s on s.id_semester = ms.id_semester
			WHERE 
				M.nim_mhs = '{$nim}' and p.id_perguruan_tinggi = {$id_perguruan_tinggi}
			ORDER BY S.thn_akademik_semester DESC, S.nm_semester DESC"
		);
	}

	function get_detail_admisi($nim, $id_perguruan_tinggi)
	{
		return $this->database->QueryToArray(
			"SELECT a.id_admisi,
				s.tahun_ajaran, s.nm_semester, nm_status_pengguna, nm_jalur, sp.status_keluar,
				A.no_sk, to_char(A.tgl_sk, 'YYYY-MM-DD') as tgl_sk, to_char(A.tgl_keluar, 'YYYY-MM-DD') as tgl_keluar, A.no_ijasah, A.keterangan,
				a.tgl_usulan, a.tgl_apv,
				to_char(a.created_on,'YYYY-MM-DD HH24:MI:SS') as created_on
			FROM admisi a
			JOIN mahasiswa m ON m.id_mhs = a.id_mhs
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna
			JOIN status_pengguna sp ON sp.id_status_pengguna = A.status_akd_mhs
			LEFT JOIN jalur j ON j.id_jalur = A.id_jalur
			LEFT JOIN semester s ON s.id_semester = A.id_semester
			WHERE status_apv = 1 AND m.nim_mhs = '{$nim}' AND p.id_perguruan_tinggi = {$id_perguruan_tinggi}
			ORDER BY A.id_jalur NULLS LAST, sp.status_keluar ASC, s.thn_akademik_semester, s.nm_semester"
		);
	}

	function get_tahun_lulus_sekolah($id_perguruan_tinggi)
	{
		return $this->database->QueryToArray(
			"SELECT DISTINCT TAHUN_LULUS 
				FROM CALON_MAHASISWA_SEKOLAH cms
				JOIN CALON_MAHASISWA_BARU cmb ON cmb.ID_C_MHS = cms.ID_C_MHS
				JOIN PENERIMAAN p ON p.ID_PENERIMAAN = cmb.ID_PENERIMAAN
				WHERE p.ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}' AND cms.NO_IJAZAH IS NOT NULL
				AND LENGTH(TAHUN_LULUS) = 4
				ORDER BY TAHUN_LULUS"
		);
	}
}

?>
