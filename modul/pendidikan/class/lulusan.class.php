<?php


class lulusan {

    public $database;

    function __construct($database) {
        $this->database = $database;
    }

	function get_lulusan($nim, $id_perguruan_tinggi)
	{
		return $this->database->QueryToArray(
			"SELECT a.id_admisi,
				s.tahun_ajaran, s.nm_semester, nm_status_pengguna, nm_jalur, sp.status_keluar,
				A.no_sk, to_char(A.tgl_sk, 'YYYY-MM-DD') as tgl_sk, to_char(A.tgl_keluar, 'YYYY-MM-DD') as tgl_keluar, A.no_ijasah, A.keterangan,
				a.tgl_usulan, a.tgl_apv,
				to_char(a.created_on,'YYYY-MM-DD HH24:MI:SS') as created_on
			FROM admisi a
			JOIN mahasiswa m ON m.id_mhs = a.id_mhs
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna
			JOIN status_pengguna sp ON sp.id_status_pengguna = A.status_akd_mhs
			LEFT JOIN jalur j ON j.id_jalur = A.id_jalur
			LEFT JOIN semester s ON s.id_semester = A.id_semester
			WHERE status_apv = 1 AND m.nim_mhs = '{$nim}' AND p.id_perguruan_tinggi = {$id_perguruan_tinggi}
			ORDER BY A.id_jalur NULLS LAST, sp.status_keluar ASC, s.thn_akademik_semester, s.nm_semester"
		);
	}

	function get_data_lulusan($id_perguruan_tinggi)
	{
		return $this->database->QueryToArray(
			"SELECT * FROM ADMISI ADM
			 JOIN STATUS_PENGGUNA ST ON ST.ID_STATUS_PENGGUNA = ADM.STATUS_AKD_MHS
			 WHERE ST.ID_STATUS_PENGGUNA ='4'"
		);
	}


	function get_tahun_lulus($id_perguruan_tinggi)
	{
		return $this->database->QueryToArray(
			"SELECT DISTINCT TAHUN_LULUS 
				FROM CALON_MAHASISWA_SEKOLAH cms
				JOIN CALON_MAHASISWA_BARU cmb ON cmb.ID_C_MHS = cms.ID_C_MHS
				JOIN PENERIMAAN p ON p.ID_PENERIMAAN = cmb.ID_PENERIMAAN
				WHERE p.ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}' AND cms.NO_IJAZAH IS NOT NULL
				AND LENGTH(TAHUN_LULUS) = 4
				ORDER BY TAHUN_LULUS"
		);
	}

	function get_tahun_lulus_baru($id_perguruan_tinggi)
	{
		return $this->database->QueryToArray(
			"SELECT DISTINCT TAHUN_LULUS 
				FROM CALON_MAHASISWA_SEKOLAH cms
				JOIN CALON_MAHASISWA_BARU cmb ON cmb.ID_C_MHS = cms.ID_C_MHS
				JOIN PENERIMAAN p ON p.ID_PENERIMAAN = cmb.ID_PENERIMAAN
				WHERE p.ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}' AND cms.NO_IJAZAH IS NOT NULL
				AND LENGTH(TAHUN_LULUS) = 4
				ORDER BY TAHUN_LULUS"
		);
	}


	function get_data_lulus($id_semester, $id_perguruan_tinggi)
	{
		return $this->database->QueryToArray(
			"SELECT ADM.ID_ADMISI, MHS.NIM_MHS, PGUNA.NM_PENGGUNA, STS_PGUNA.NM_STATUS_PENGGUNA, ADM.CREATED_ON AS TGL_LULUS 
			FROM ADMISI ADM
			JOIN MAHASISWA MHS ON MHS.ID_MHS =  ADM.ID_MHS
			JOIN PENGGUNA PGUNA ON PGUNA.ID_PENGGUNA = MHS.ID_PENGGUNA
			JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = MHS.ID_PROGRAM_STUDI
			JOIN STATUS_PENGGUNA STS_PGUNA ON STS_PGUNA.ID_STATUS_PENGGUNA = MHS.STATUS_AKADEMIK_MHS
			WHERE ID_SEMESTER ='{$id_semester}' AND ADM.STATUS_AKD_MHS ='4' 
			AND PGUNA.ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}' ORDER BY mhs.NIM_MHS ASC"
		);
	}
	
}

?>
