<?php

$id_pt = $id_pt_user;

class AUCC_Pendidikan {

	/**
	 * @var MyOracle 
	 */
    public $db;

    function __construct($db) {
        $this->db = $db;
    }
	
	function get_kelompok_eval_aspek_by_instrumen($id_eval_instrumen) {
        return $this->db->QueryToArray("SELECT * FROM EVALUASI_INSTRUMEN 
LEFT JOIN EVALUASI_KELOMPOK_ASPEK ON EVALUASI_KELOMPOK_ASPEK.ID_EVAL_INSTRUMEN = EVALUASI_INSTRUMEN.ID_EVAL_INSTRUMEN
WHERE EVALUASI_INSTRUMEN.ID_EVAL_INSTRUMEN = {$id_eval_instrumen} ORDER BY EVALUASI_INSTRUMEN.NAMA_EVAL_INSTRUMEN");
    }

	function get_hasil_cari_mahasiswa($var) {
		$var_trim = trim($var);
        return $this->db->QueryToArray("
            SELECT P.NM_PENGGUNA,M.NIM_MHS,NO_UJIAN,F.NM_FAKULTAS,PS.NM_PROGRAM_STUDI FROM PENGGUNA P
            JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
			LEFT JOIN CALON_MAHASISWA_BARU CMB ON M.ID_C_MHS = CMB.ID_C_MHS
            JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            JOIN FAKULTAS F ON PS.ID_FAKULTAS = F.ID_FAKULTAS
            WHERE M.NIM_MHS LIKE '%$var_trim%' OR UPPER(P.NM_PENGGUNA) LIKE UPPER('%$var_trim%') OR CMB.NO_UJIAN LIKE '%$var_trim%'
            ");
    }

    function detail_pembayaran($nim) {
        return $this->db->QueryToArray("
        SELECT * FROM 
            (SELECT P.ID_PERGURUAN_TINGGI, M.ID_MHS AS ID1,M.NIM_LAMA,P.NM_PENGGUNA,NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,M.NIM_MHS,PEM.*,B.NM_BIAYA,J.NM_JENJANG,M.STATUS_CEKAL,JAL.NM_JALUR,S.THN_AKADEMIK_SEMESTER
            ,KB.NM_KELOMPOK_BIAYA,M.NIM_BANK,BNK.NM_BANK,BNK.ID_BANK AS ID_BNK,BNKV.ID_BANK_VIA AS ID_BNKV,BNKV.NAMA_BANK_VIA,S2.NM_SEMESTER,S2.TAHUN_AJARAN, NAMA_STATUS
            FROM PENGGUNA P
            LEFT JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA
            LEFT JOIN PROGRAM_STUDI PR ON M.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            LEFT JOIN JALUR_MAHASISWA JM ON M.ID_MHS = JM.ID_MHS
            LEFT JOIN JALUR JAL ON JAL.ID_JALUR = JM.ID_JALUR
            LEFT JOIN SEMESTER S ON S.ID_SEMESTER = JM.ID_SEMESTER
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS
            LEFT JOIN PEMBAYARAN PEM ON M.ID_MHS = PEM.ID_MHS
            LEFT JOIN SEMESTER S2 ON S2.ID_SEMESTER = PEM.ID_SEMESTER
            LEFT JOIN BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            LEFT JOIN BANK_VIA BNKV ON BNKV.ID_BANK_VIA = PEM.ID_BANK_VIA
            LEFT JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            LEFT JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
			LEFT JOIN STATUS_PEMBAYARAN ON STATUS_PEMBAYARAN.ID_STATUS_PEMBAYARAN = PEM.ID_STATUS_PEMBAYARAN
            ) X1
        LEFT JOIN
            (SELECT M.ID_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_SPP_TAGIH,SUM(PEM.DENDA_BIAYA) AS TOTAL_DENDA_BIAYA FROM PEMBAYARAN PEM
            LEFT JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
            WHERE PEM.TGL_BAYAR IS NULL AND PEM.IS_TAGIH ='Y'
            GROUP BY M.ID_MHS
            ) X2 
        ON X1.ID1 = X2.ID2
        LEFT JOIN
            (SELECT M.ID_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_SPP,SUM(PEM.DENDA_BIAYA) AS TOTAL_DENDA FROM PEMBAYARAN PEM
            LEFT JOIN MAHASISWA M ON PEM.ID_MHS = M.ID_MHS
            GROUP BY M.ID_MHS
            ) X3 
        ON X1.ID1 = X3.ID2
        WHERE X1.NIM_MHS ='{$nim}' AND X1.ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}'
        ORDER BY X1.TAHUN_AJARAN DESC, NM_SEMESTER DESC");
    }
    
    function detail_pembayaran_cmhs($no_ujian){
		//$calon_mahasiswa=$this->db->QueryToArray("SELECT * FROM CALON_MAHASISWA WHERE NO_UJIAN ='{$no_ujian}'")==NULL? "_BARU":"";
        return $this->db->QueryToArray("
        SELECT * FROM 
            (SELECT CM.ID_C_MHS AS ID1,CM.NM_C_MHS,CM.NO_UJIAN,J.NM_JENJANG,PR.NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,PEM.*,B.NM_BIAYA,
            BNK.NM_BANK,BNK.ID_BANK AS ID_BNK,BNKV.ID_BANK_VIA AS ID_BNKV,BNKV.NAMA_BANK_VIA, TAHUN_AJARAN, NM_SEMESTER, BK.ID_KELOMPOK_BIAYA,
			(CASE WHEN STATUS_BIDIK_MISI = 1 THEN 'Bidik Misi' ELSE '' END) AS BEASISWA, STATUS_PEMBAYARAN.NAMA_STATUS
            FROM CALON_MAHASISWA_BARU CM
            LEFT JOIN PROGRAM_STUDI PR ON CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS
            LEFT JOIN PEMBAYARAN_CMHS PEM ON CM.ID_C_MHS = PEM.ID_C_MHS
            LEFT JOIN SEMESTER S2 ON S2.ID_SEMESTER = PEM.ID_SEMESTER
            LEFT JOIN BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            LEFT JOIN BANK_VIA BNKV ON BNKV.ID_BANK_VIA = PEM.ID_BANK_VIA
            LEFT JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            LEFT JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
			LEFT JOIN BIAYA_KULIAH BK ON BK.ID_BIAYA_KULIAH = DB.ID_BIAYA_KULIAH
			LEFT JOIN CALON_MAHASISWA_DATA CMD ON CMD.ID_C_MHS = CM.ID_C_MHS
			LEFT JOIN STATUS_PEMBAYARAN ON STATUS_PEMBAYARAN.ID_STATUS_PEMBAYARAN = PEM.ID_STATUS_PEMBAYARAN
            ) X1
        LEFT JOIN
            (SELECT CM.ID_C_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_BIAYA_TAGIH
            FROM PEMBAYARAN_CMHS PEM
            LEFT JOIN CALON_MAHASISWA_BARU CM ON PEM.ID_C_MHS = CM.ID_C_MHS
			
            WHERE PEM.TGL_BAYAR IS NULL AND IS_TAGIH = 'Y'
            GROUP BY CM.ID_C_MHS
            ) X2 
        ON X1.ID1 = X2.ID2
        LEFT JOIN
            (SELECT CM.ID_C_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_BIAYA
            FROM PEMBAYARAN_CMHS PEM
            LEFT JOIN CALON_MAHASISWA_BARU CM ON PEM.ID_C_MHS = CM.ID_C_MHS
            GROUP BY CM.ID_C_MHS
            ) X3
        ON X1.ID1 = X3.ID2
		LEFT JOIN KELOMPOK_BIAYA ON KELOMPOK_BIAYA.ID_KELOMPOK_BIAYA = X1.ID_KELOMPOK_BIAYA
        WHERE X1.NO_UJIAN ='{$no_ujian}'
        ORDER BY X1.TAHUN_AJARAN DESC, NM_SEMESTER DESC");
    }

    function detail_pembayaran_cmhs_nim($nim){
        return $this->db->QueryToArray("
        SELECT * FROM 
            (SELECT CM.ID_C_MHS AS ID1,CM.NM_C_MHS,CM.NO_UJIAN,J.NM_JENJANG,PR.NM_PROGRAM_STUDI,UPPER(F.NM_FAKULTAS) AS NM_FAKULTAS,PEM.*,B.NM_BIAYA,
            BNK.NM_BANK,BNK.ID_BANK AS ID_BNK,BNKV.ID_BANK_VIA AS ID_BNKV,BNKV.NAMA_BANK_VIA, NIM_MHS
            FROM CALON_MAHASISWA_BARU CM
            LEFT JOIN PROGRAM_STUDI PR ON CM.ID_PROGRAM_STUDI = PR.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON J.ID_JENJANG = PR.ID_JENJANG
            LEFT JOIN FAKULTAS F ON F.ID_FAKULTAS = PR.ID_FAKULTAS
            LEFT JOIN PEMBAYARAN_CMHS PEM ON CM.ID_C_MHS = PEM.ID_C_MHS
            LEFT JOIN SEMESTER S2 ON S2.ID_SEMESTER = PEM.ID_SEMESTER
            LEFT JOIN BANK BNK ON BNK.ID_BANK = PEM.ID_BANK
            LEFT JOIN BANK_VIA BNKV ON BNKV.ID_BANK_VIA = PEM.ID_BANK_VIA
            LEFT JOIN DETAIL_BIAYA DB ON PEM.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
            LEFT JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
            ) X1
        LEFT JOIN
            (SELECT CM.ID_C_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_BIAYA_TAGIH
            FROM PEMBAYARAN_CMHS PEM
            LEFT JOIN CALON_MAHASISWA_BARU CM ON PEM.ID_C_MHS = CM.ID_C_MHS
            WHERE PEM.TGL_BAYAR IS NULL
            GROUP BY CM.ID_C_MHS
            ) X2 
        ON X1.ID1 = X2.ID2
        LEFT JOIN
            (SELECT CM.ID_C_MHS AS ID2,SUM(PEM.BESAR_BIAYA) AS TOTAL_BIAYA
            FROM PEMBAYARAN_CMHS PEM
            LEFT JOIN CALON_MAHASISWA_BARU CM ON PEM.ID_C_MHS = CM.ID_C_MHS
            GROUP BY CM.ID_C_MHS
            ) X3
        ON X1.ID1 = X3.ID2
        WHERE X1.NIM_MHS ='{$nim}'
        ORDER BY X1.ID_PEMBAYARAN_CMHS");
    }


    function get_detail_pembayaran_mahasiswa($nim) {
        return $this->db->QueryToArray("SELECT B.NM_BIAYA,P.* FROM PEMBAYARAN P
        JOIN DETAIL_BIAYA DB ON P.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
        JOIN MAHASISWA M ON P.ID_MHS = M.ID_MHS
        WHERE NIM_MHS ='{$nim}'
        ");
    }

    function get_detail_pembayaran($id_pembayaran) {
        $this->db->Query("SELECT B.NM_BIAYA,P.* FROM PEMBAYARAN P
        JOIN DETAIL_BIAYA DB ON P.ID_DETAIL_BIAYA = DB.ID_DETAIL_BIAYA
        JOIN BIAYA B ON DB.ID_BIAYA = B.ID_BIAYA
        JOIN MAHASISWA M ON P.ID_MHS = M.ID_MHS
        WHERE P.ID_PEMBAYARAN ='{$id_pembayaran}'
        ");

        return $this->db->FetchAssoc();
    }
    
    function get_detail_pengguna($username){
        $this->db->Query("
            SELECT * FROM PENGGUNA P
            LEFT JOIN ROLE R ON R.ID_ROLE = P.ID_ROLE
            WHERE P.USERNAME='{$username}' AND R.ID_ROLE = 11
        ");
        
        return $this->db->FetchAssoc();
    }
    
    function reset_password_pengguna($username){
        $this->db->Query("UPDATE PENGGUNA SET PASSWORD_PENGGUNA = USERNAME WHERE USERNAME='{$username}'");
    }

    function cek_data($nim, $id_pt = 0) {
        // tambahan mencari jumlah pengambilan dan thn angkatan mhs utk kebutuhan update NIM pada MABA
        // FIKRIE
        // 29-06-2016
        $this->db->Query(
        "SELECT M.NIM_MHS,P.NM_PENGGUNA,
            M.NM_IBU_MHS, M.LAHIR_PROP_MHS, M.LAHIR_KOTA_MHS,
            J.ID_JENJANG,J.NM_JENJANG,M.ID_SEMESTER_MASUK AS ID_SEMESTER,
            S.THN_AKADEMIK_SEMESTER,PS.ID_PROGRAM_STUDI,PS.NM_PROGRAM_STUDI,J.ID_JALUR,J.NM_JALUR,
            KB.ID_KELOMPOK_BIAYA,KB.NM_KELOMPOK_BIAYA, TO_CHAR(P.TGL_LAHIR_PENGGUNA,'DD-MM-YYYY') AS TGL_LAHIR_PENGGUNA,
            (SELECT COUNT(*) FROM PENGAMBILAN_MK pmk WHERE pmk.ID_MHS = M.ID_MHS) AS JML_MATKUL,
            M.THN_ANGKATAN_MHS, A.SKS_DIAKUI, A.KODE_PT_ASAL, A.KODE_PRODI_ASAL, PT.NAMA_PERGURUAN_TINGGI AS NM_PT_ASAL,
            M.NIK_MHS, M.ALAMAT_ASAL_MHS, M.ALAMAT_DUSUN_ASAL_MHS, M.ALAMAT_RT_ASAL_MHS, M.ALAMAT_RW_ASAL_MHS, M.ALAMAT_KELURAHAN_ASAL_MHS, M.ALAMAT_KODEPOS_ASAL, M.ALAMAT_KECAMATAN_ASAL_MHS, P.EMAIL_ALTERNATE
            FROM PENGGUNA P
            LEFT JOIN MAHASISWA M ON P.ID_PENGGUNA = M.ID_PENGGUNA 
            LEFT JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
            LEFT JOIN JENJANG J ON PS.ID_JENJANG = J.ID_JENJANG
            LEFT JOIN KELOMPOK_BIAYA KB ON KB.ID_KELOMPOK_BIAYA = M.ID_KELOMPOK_BIAYA
            LEFT JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
            LEFT JOIN SEMESTER S ON JM.ID_SEMESTER = S.ID_SEMESTER 
            LEFT JOIN JALUR J ON JM.ID_JALUR = J.ID_JALUR
            LEFT JOIN ADMISI A ON A.ID_MHS = M.ID_MHS AND A.ID_JALUR IS NOT NULL
            LEFT JOIN PERGURUAN_TINGGI PT ON PT.NPSN = A.KODE_PT_ASAL
        WHERE P.ID_PERGURUAN_TINGGI = {$id_pt} AND M.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function get_negara($id_propinsi) {
        $this->db->Query(
        "select id_negara from provinsi where id_provinsi='{$id_propinsi}'");
        return $this->db->FetchAssoc();
    }

    function get_semua_negara() {
        return $this->db->QueryToArray("SELECT * FROM NEGARA WHERE id_negara = 114 ORDER BY NM_NEGARA");
    }

    function get_propinsi_by_negara($id_negara) {
        return $this->db->QueryToArray("SELECT * FROM PROVINSI WHERE ID_NEGARA = '{$id_negara}' ORDER BY NM_PROVINSI");
    }

    function get_kota_by_propinsi($id_propinsi) {
        return $this->db->QueryToArray("SELECT * FROM KOTA WHERE ID_PROVINSI = '{$id_propinsi}' ORDER BY NM_KOTA");
    }

    function cek_pengguna($nim,$id_perguruan_tinggi) {
        $this->db->Query("SELECT * FROM PENGGUNA WHERE USERNAME='{$nim}' AND ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}'");
        return $this->db->FetchAssoc();
    }

    function cek_mahasiswa($nim) {
        $this->db->Query("SELECT * FROM MAHASISWA WHERE NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_mahasiswa_by_perguruan_tinggi($nim, $id_perguruan_tinggi) {
        $this->db->Query("SELECT * FROM MAHASISWA m
                            JOIN PENGGUNA p ON p.ID_PENGGUNA = m.ID_PENGGUNA AND p.ID_PERGURUAN_TINGGI = {$id_perguruan_tinggi}
                            WHERE m.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_nama_jalur($nm_jalur, $id_pt = 0) {
        $this->db->Query("SELECT * FROM JALUR WHERE NM_JALUR = '{$nm_jalur}' AND ID_PERGURUAN_TINGGI = '{$id_pt}'");
        return $this->db->FetchAssoc();
    }

    function cek_semester_by_angkatan($angkatan, $id_pt) {
        return $this->db->QueryToArray("SELECT ID_SEMESTER FROM SEMESTER 
                                    WHERE THN_AKADEMIK_SEMESTER = '{$angkatan}' AND ID_PERGURUAN_TINGGI = {$id_pt} 
                                    AND TIPE_SEMESTER = 'REG' AND UPPER(NM_SEMESTER) = 'GANJIL'");
    }

    // NIM_MHS sudah dimasukkan lewat tabel MAHASISWA, jadi sudah terbaca
    function cek_jalur($nim, $id_pt = 0) {
        $this->db->Query("SELECT * FROM JALUR_MAHASISWA JM 
            JOIN MAHASISWA M ON M.ID_MHS = JM.ID_MHS
            JOIN PENGGUNA P ON P.ID_PENGGUNA = M.ID_PENGGUNA AND P.ID_PERGURUAN_TINGGI = {$id_pt}
            WHERE M.NIM_MHS = '{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_prodi($nama_prodi, $id_pt = 0) {
        $this->db->Query("SELECT PS.ID_PROGRAM_STUDI, PS.ID_FAKULTAS FROM PROGRAM_STUDI PS 
            JOIN FAKULTAS F ON F.ID_FAKULTAS = PS.ID_FAKULTAS AND F.ID_PERGURUAN_TINGGI = {$id_pt}
            JOIN JENJANG J ON J.ID_JENJANG = PS.ID_JENJANG
            WHERE TRIM(UPPER(J.NM_JENJANG || ' ' || PS.NM_PROGRAM_STUDI)) = TRIM(UPPER('{$nama_prodi}'))");
        return $this->db->FetchAssoc();
    }

    function insert_pengguna_mahasiswa($nama, $nim, $tgl_lahir, $id_perguruan_tinggi) {
        $password_hash = sha1($nim);
        $this->db->Query("INSERT INTO PENGGUNA 
                (USERNAME,PASSWORD_HASH,NM_PENGGUNA,TGL_LAHIR_PENGGUNA,ID_ROLE,JOIN_TABLE,ID_PERGURUAN_TINGGI) 
                VALUES 
                ('{$nim}','{$password_hash}',UPPER('{$nama}'),TO_DATE('$tgl_lahir', 'DD-MM-YYYY'),3,3,'{$id_perguruan_tinggi}')");
    }

    function update_pengguna_mahasiswa($nama, $nim, $id_perguruan_tinggi, $email_alternate = null) {
        $password_hash = sha1($nim);
        $this->db->Query("UPDATE PENGGUNA SET
                USERNAME = '{$nim}', PASSWORD_HASH = '{$password_hash}',NM_PENGGUNA = UPPER('{$nama}'),
                ID_ROLE = '3',JOIN_TABLE = '3',ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}', EMAIL_ALTERNATE = '{$email_alternate}'
                WHERE USERNAME = '{$nim}' AND ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}'");
    }

    function insert_mahasiswa($id_pengguna, $nim, $program_studi, $kelompok_biaya, $thn, $semester, $lahir_prop, $lahir_kota, $nama_ibu, $nik, $alamat_jalan, $alamat_dusun, $alamat_rt, $alamat_rw, $alamat_kelurahan, $alamat_kodepos, $alamat_kecamatan) {
        //$nim_bank = substr($nim, 0, 16);
        $this->db->Query("INSERT INTO MAHASISWA 
                (ID_PENGGUNA,NIM_MHS,ID_PROGRAM_STUDI,ID_KELOMPOK_BIAYA,NIM_BANK,STATUS_CEKAL, THN_ANGKATAN_MHS, STATUS_AKADEMIK_MHS, ID_SEMESTER_MASUK, LAHIR_PROP_MHS, LAHIR_KOTA_MHS, NM_IBU_MHS, NIK_MHS, ALAMAT_ASAL_MHS, ALAMAT_DUSUN_ASAL_MHS, ALAMAT_RT_ASAL_MHS, ALAMAT_RW_ASAL_MHS, ALAMAT_KELURAHAN_ASAL_MHS, ALAMAT_KODEPOS_ASAL, ALAMAT_KECAMATAN_ASAL_MHS) 
                VALUES 
                ('{$id_pengguna}','{$nim}','{$program_studi}','{$kelompok_biaya}','{$nim}','0', '$thn', 1, '$semester', '{$lahir_prop}', '{$lahir_kota}', '{$nama_ibu}', '{$nik}', '{$alamat_jalan}', '{$alamat_dusun}', '{$alamat_rt}', '{$alamat_rw}', '{$alamat_kelurahan}', '{$alamat_kodepos}', '{$alamat_kecamatan}')");
    }

    function insert_role_mahasiswa($id_pengguna) {
        //$nim_bank = substr($nim, 0, 16);
        $this->db->Query("INSERT INTO ROLE_PENGGUNA (ID_ROLE,ID_PENGGUNA) VALUES ('3', '{$id_pengguna}')");
    }

    function update_mahasiswa_baru($id_pengguna, $nim, $program_studi, $kelompok_biaya, $thn, $semester) {
        //$nim_bank = substr($nim, 0, 9);
        $this->db->Query("UPDATE MAHASISWA SET 
                ID_PENGGUNA = '{$id_pengguna}',NIM_MHS = '{$nim}',ID_PROGRAM_STUDI = '{$program_studi}',ID_KELOMPOK_BIAYA = '{$kelompok_biaya}',
                NIM_BANK = '{$nim}',STATUS_CEKAL = '0', THN_ANGKATAN_MHS = '{$thn}', STATUS_AKADEMIK_MHS = 1, ID_SEMESTER_MASUK = '{$semester}'
                WHERE ID_PENGGUNA = '{$id_pengguna}'");
    }
	
	function insert_admisi($id_mhs, $semester, $id_pengguna) {
        $tgl = date('Y-m-d');
        $this->db->Query("INSERT INTO ADMISI 
                (ID_MHS, ID_SEMESTER, STATUS_AKD_MHS, STATUS_APV, TGL_USULAN, TGL_APV, CREATED_BY) 
                VALUES 
                ('{$id_mhs}','{$semester}','1','1',TO_DATE('$tgl', 'YYYY-MM-DD'),TO_DATE('$tgl', 'YYYY-MM-DD'), '$id_pengguna')");
    }

    function insert_admisi_jalur($id_mhs, $semester, $id_pengguna, $jalur) {
        $tgl = date('Y-m-d');
        $this->db->Query("INSERT INTO ADMISI 
                (ID_MHS, ID_SEMESTER, STATUS_AKD_MHS, STATUS_APV, TGL_USULAN, TGL_APV, CREATED_BY, ID_JALUR) 
                VALUES 
                ('{$id_mhs}','{$semester}','1','1',TO_DATE('$tgl', 'YYYY-MM-DD'),TO_DATE('$tgl', 'YYYY-MM-DD'), '$id_pengguna', '$jalur')");
    }

    function insert_jalur_mahasiswa($id_mhs, $program_studi, $jalur, $semester, $nim_mhs) {
		
        $this->db->Query("INSERT INTO JALUR_MAHASISWA 
                (ID_MHS,ID_PROGRAM_STUDI,ID_JALUR,ID_SEMESTER,NIM_MHS, IS_JALUR_AKTIF)
                VALUES
                ('{$id_mhs}','{$program_studi}','{$jalur}','{$semester}','{$nim_mhs}', 1)");
    }

    function insert_master_jalur($nm_jalur,$id_perguruan_tinggi) {
        
        $this->db->Query("INSERT INTO JALUR 
                (NM_JALUR,NIM_JALUR,ID_PERGURUAN_TINGGI)
                VALUES
                ('{$nm_jalur}','','{$id_perguruan_tinggi}')");
    }

    function update_pengguna($nama, $nim, $tgl_lahir, $id_pengguna, $id_pt = 0, $email_alternate = null) {
        $this->db->Query("UPDATE PENGGUNA SET USERNAME = '{$nim}', NM_PENGGUNA =UPPER('$nama'), TGL_LAHIR_PENGGUNA = TO_DATE('$tgl_lahir', 'DD-MM-YYYY'), EMAIL_ALTERNATE = '{$email_alternate}'
		WHERE ID_PENGGUNA='{$id_pengguna}' AND ID_PERGURUAN_TINGGI = {$id_pt}");
    }

    function update_mahasiswa($id_kelompok_biaya, $nim, $id_program_studi, $thn, $semester, $id_mhs, $lahir_prop, $lahir_kota, $nama_ibu, $nik, $alamat_jalan, $alamat_dusun, $alamat_rt, $alamat_rw, $alamat_kelurahan, $alamat_kodepos, $alamat_kecamatan) {
       //if(strlen($nim) >= 13){
			$nim_bank = $nim;
	   /*}else{
			$nim_bank = substr($nim, 0, 9);
	   }*/

        $this->db->Query("UPDATE MAHASISWA SET NIM_MHS = '{$nim}', ID_KELOMPOK_BIAYA='{$id_kelompok_biaya}', NIM_BANK='{$nim_bank}', ID_SEMESTER_MASUK = '$semester',
							ID_PROGRAM_STUDI='{$id_program_studi}',STATUS_CEKAL=0, THN_ANGKATAN_MHS = '$thn', 
                            LAHIR_PROP_MHS = '{$lahir_prop}', LAHIR_KOTA_MHS = '{$lahir_kota}', NM_IBU_MHS = '{$nama_ibu}',
                            NIK_MHS='{$nik}', ALAMAT_ASAL_MHS='{$alamat_jalan}', ALAMAT_DUSUN_ASAL_MHS='{$alamat_dusun}', ALAMAT_RT_ASAL_MHS='{$alamat_rt}', ALAMAT_RW_ASAL_MHS='{$alamat_rw}', ALAMAT_KELURAHAN_ASAL_MHS='{$alamat_kelurahan}', ALAMAT_KODEPOS_ASAL='{$alamat_kodepos}', ALAMAT_KECAMATAN_ASAL_MHS='{$alamat_kecamatan}'
							WHERE ID_MHS='{$id_mhs}'");
    }

    function update_nim_lama_mahasiswa($nim_lama, $id_mhs) {
    
        $this->db->Query("UPDATE MAHASISWA SET NIM_LAMA = '{$nim_lama}' WHERE ID_MHS='{$id_mhs}'");
    }

    function update_calon_mahasiswa($id_c_mhs, $nim) {

        $this->db->Query("UPDATE CALON_MAHASISWA_BARU SET NIM_MHS = '{$nim}' WHERE ID_C_MHS='{$id_c_mhs}'");
    }

    function update_jalur_mahasiswa($id_mhs, $program_studi, $jalur, $semester, $nim) {
        $this->db->Query("UPDATE JALUR_MAHASISWA 
						SET ID_MHS='{$id_mhs}',ID_PROGRAM_STUDI='{$program_studi}',ID_JALUR='{$jalur}',
						ID_SEMESTER='{$semester}', NIM_MHS='{$nim}' WHERE IS_JALUR_AKTIF =1 AND ID_MHS = '{$id_mhs}'");
    }

    function update_jalur_admisi($id_mhs, $jalur, $id_semester) {
        $this->db->Query("UPDATE ADMISI 
                        SET ID_JALUR='{$jalur}', ID_SEMESTER = '{$id_semester}' WHERE ID_MHS='{$id_mhs}' AND ID_JALUR IS NOT NULL");
    }

    function update_jalur_admisi_transfer($id_mhs, $sks_diakui, $kode_pt_asal, $kode_prodi_asal) {
        $this->db->Query("UPDATE ADMISI 
                        SET SKS_DIAKUI='{$sks_diakui}', KODE_PT_ASAL = '{$kode_pt_asal}', KODE_PRODI_ASAL = '{$kode_prodi_asal}' 
                            WHERE ID_MHS='{$id_mhs}' AND ID_JALUR IS NOT NULL");
    }

    function get_jenjang() {
        return $this->db->QueryToArray("SELECT * FROM JENJANG ORDER BY NM_JENJANG,ID_JENJANG");
    }

    function get_semester() {
		global $PT;
        return $this->db->QueryToArray(
			"SELECT 
				id_semester, nm_semester, thn_akademik_semester, tahun_ajaran, fd_id_smt, 
				CASE status_aktif_semester WHEN 'True' THEN 1 ELSE 0 END AS status_aktif
			FROM semester WHERE id_perguruan_tinggi = {$PT->ID_PERGURUAN_TINGGI}
			ORDER BY thn_akademik_semester DESC, nm_semester DESC");
    }

    function get_semester_angkatan($id_pt) {
        return $this->db->QueryToArray("SELECT * FROM SEMESTER WHERE ID_PERGURUAN_TINGGI = {$id_pt} AND TIPE_SEMESTER = 'REG' ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
    }
	
	function get_thn_semester($id_semester) {
        $this->db->Query("SELECT * FROM SEMESTER WHERE ID_SEMESTER = '$id_semester'");
		return $this->db->FetchAssoc();
    }
	
	function get_thn_semester_distinct() {
		global $PT;
		return $this->db->QueryToArray(
			"SELECT DISTINCT thn_akademik_semester FROM semester
			WHERE id_perguruan_tinggi = {$PT->ID_PERGURUAN_TINGGI}
			ORDER BY thn_akademik_semester DESC");
	}

    // utk menangani kebutuhan arsiparis
    function get_subkategori($id_kategori) {
        return $this->db->QueryToArray(
            "SELECT * FROM ARSIP_SUBKATEGORI 
            WHERE ID_ARSIP_KATEGORI='{$id_kategori}'
            ORDER BY NM_ARSIP_SUBKATEGORI");
    }

    function get_pt_asal() {
        return $this->db->QueryToArray("SELECT NPSN, UPPER(NAMA_PERGURUAN_TINGGI) AS NAMA_PERGURUAN_TINGGI FROM PERGURUAN_TINGGI WHERE NPSN IS NOT NULL ORDER BY UPPER(NAMA_PERGURUAN_TINGGI)");
    }

    function get_count_pt_asal($nama_perguruan_tinggi) {
        $this->db->Query("SELECT COUNT(*) AS CEK
                            FROM PERGURUAN_TINGGI 
                            WHERE NPSN IS NOT NULL AND (UPPER(NAMA_PERGURUAN_TINGGI) LIKE '%{$nama_perguruan_tinggi}%' OR NPSN = '{$nama_perguruan_tinggi}')");
        $cek = $this->db->FetchAssoc();
        return $cek['CEK'];
    }

    function get_program_studi() {
		global $PT;
        return $this->db->QueryToArray(
			"SELECT ps.id_program_studi, ps.nm_program_studi, j.nm_jenjang FROM program_studi  ps
			JOIN fakultas F ON F.id_fakultas = ps.id_fakultas
			JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
			WHERE F.id_perguruan_tinggi = {$PT->ID_PERGURUAN_TINGGI}
			ORDER BY nm_program_studi, id_program_studi");
    }
	
	function get_fakultas($id_perguruan_tinggi) {
        return $this->db->QueryToArray("SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$id_perguruan_tinggi}' ORDER BY ID_FAKULTAS ASC");
    }

    function get_program_studi_by_jenjang($id_jenjang, $id_pt = 0) {
        return $this->db->QueryToArray(
            "SELECT * FROM PROGRAM_STUDI 
            JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS AND FAKULTAS.ID_PERGURUAN_TINGGI = '{$id_pt}'
			LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
			WHERE JENJANG.ID_JENJANG='{$id_jenjang}' AND STATUS_AKTIF_PRODI = 1
			ORDER BY NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }
	
	function get_program_studi_by_fakultas_jenjang($id_fakultas, $id_jenjang, $id_pt = 0) {
        return $this->db->QueryToArray(
            "SELECT * FROM PROGRAM_STUDI 
            JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS AND FAKULTAS.ID_PERGURUAN_TINGGI = '{$id_pt}'
			LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
			WHERE JENJANG.ID_JENJANG={$id_jenjang} and FAKULTAS.ID_FAKULTAS = '$id_fakultas' AND STATUS_AKTIF_PRODI = 1
			ORDER BY NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }
	
	function get_program_studi_by_fakultas($id_fakultas, $id_pt = 0) {
        return $this->db->QueryToArray(
            "SELECT ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, NM_JENJANG FROM PROGRAM_STUDI
            JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS AND FAKULTAS.ID_PERGURUAN_TINGGI = '{$id_pt}' 
    		LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
    		WHERE PROGRAM_STUDI.ID_FAKULTAS={$id_fakultas}  AND STATUS_AKTIF_PRODI = 1 
    		ORDER BY NM_JENJANG, NM_PROGRAM_STUDI,ID_PROGRAM_STUDI");
    }

    function get_jalur($id_pt = 0) {
        return $this->db->QueryToArray("SELECT * FROM JALUR WHERE ID_PERGURUAN_TINGGI = {$id_pt} ORDER BY NM_JALUR,ID_JALUR");
    }

    function get_kelompok() {
        return $this->db->QueryToArray("SELECT * FROM KELOMPOK_BIAYA ORDER BY NM_KELOMPOK_BIAYA,ID_KELOMPOK_BIAYA");
    }

    function get_bank() {
        return $this->db->QueryToArray("SELECT * FROM BANK ORDER BY NM_BANK,ID_BANK");
    }

    function get_via_bank() {
        return $this->db->QueryToArray("SELECT * FROM BANK_VIA ORDER BY NAMA_BANK_VIA,ID_BANK_VIA");
    }
	
	
    function get_biaya_kuliah_mahasiswa(){
        return $this->db->QueryToArray("
            SELECT S.THN_AKADEMIK_SEMESTER,S.NM_SEMESTER,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,JAL.NM_JALUR,KB.NM_KELOMPOK_BIAYA,BK.BESAR_BIAYA_KULIAH FROM BIAYA_KULIAH BK
            JOIN SEMESTER S ON BK.ID_SEMESTER = S.ID_SEMESTER
            JOIN PROGRAM_STUDI PS ON BK.ID_PROGRAM_STUDI = PS.ID_PROGRAM_STUDI
            JOIN JENJANG J ON J.ID_JENJANG = BK.ID_JENJANG
            JOIN JALUR JAL ON JAL.ID_JALUR = BK.ID_JALUR
            JOIN KELOMPOK_BIAYA KB ON BK.ID_KELOMPOK_BIAYA = KB.ID_KELOMPOK_BIAYA
            ORDER BY PS.NM_PROGRAM_STUDI,JAL.NM_JALUR,S.THN_AKADEMIK_SEMESTER
            ");
    }

    function get_semester_all($id_pt = 0) {
        return $this->db->QueryToArray(
            "SELECT * FROM SEMESTER WHERE (NM_SEMESTER = 'Ganjil' or NM_SEMESTER = 'Genap') AND id_perguruan_tinggi = {$id_pt}
			ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
    }
	
	function get_semester_piutang($nim) {
        return $this->db->QueryToArray("SELECT ID_SEMESTER, TAHUN_AJARAN, NM_SEMESTER 
							FROM SEMESTER WHERE (NM_SEMESTER = 'Ganjil' or NM_SEMESTER = 'Genap') 
							AND THN_AKADEMIK_SEMESTER BETWEEN (SELECT THN_ANGKATAN_MHS FROM MAHASISWA WHERE NIM_MHS = '$nim') AND 
							(SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True') 
							AND ID_SEMESTER <> (SELECT ID_SEMESTER FROM SEMESTER WHERE NM_SEMESTER = 'Genap' AND THN_AKADEMIK_SEMESTER = 
							(SELECT THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True'))
							ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
    }
	
	
	/*function piutang_mhs2($id_semester, $id_mhs) {
        $this->db->Query("SELECT ID_MHS, ID_SEMESTER, TGL_BAYAR,  SUM(BESAR_BIAYA) BESAR_BIAYA FROM PEMBAYARAN 
										WHERE ID_SEMESTER = '$id_semester' AND ID_MHS ='$id_mhs'
										GROUP BY ID_MHS, ID_SEMESTER, TGL_BAYAR
										UNION
										SELECT ID_MHS, ID_SEMESTER, TGL_BAYAR, SUM(BESAR_BIAYA) BESAR_BIAYA 
										FROM PEMBAYARAN_CMHS 
										LEFT JOIN MAHASISWA ON MAHASISWA.ID_C_MHS = PEMBAYARAN_CMHS.ID_C_MHS
										WHERE ID_SEMESTER = '$id_semester' AND ID_MHS = '$id_mhs'
										GROUP BY ID_MHS, ID_SEMESTER, TGL_BAYAR
										ORDER BY TGL_BAYAR DESC");
		return $this->db->FetchAssoc();
    }*/
	
	
	function piutang_mhs2($id_semester, $id_mhs) {
        $this->db->Query("SELECT ID_MHS, ID_SEMESTER, TGL_BAYAR,  SUM(BESAR_BIAYA) BESAR_BIAYA FROM PEMBAYARAN 
										WHERE ID_SEMESTER = '$id_semester' AND ID_MHS ='$id_mhs'
										GROUP BY ID_MHS, ID_SEMESTER, TGL_BAYAR
										ORDER BY TGL_BAYAR DESC");
		return $this->db->FetchAssoc();
    }
	
	
	function get_mhs_piutang($fakultas) {
		if($fakultas <> ''){
			$query = " AND PROGRAM_STUDI.ID_FAKULTAS = '$fakultas'";
		}else{
			$query = "";
		}
        return $this->db->QueryToArray("SELECT ID_MHS, NIM_MHS, NM_PENGGUNA, NM_STATUS_PENGGUNA, NM_FAKULTAS, NM_PROGRAM_STUDI, NM_JENJANG
				FROM MAHASISWA
				LEFT JOIN PENGGUNA ON PENGGUNA.ID_PENGGUNA = MAHASISWA.ID_PENGGUNA
				LEFT JOIN STATUS_PENGGUNA ON STATUS_PENGGUNA.ID_STATUS_PENGGUNA = MAHASISWA.STATUS_AKADEMIK_MHS
				LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = MAHASISWA.ID_PROGRAM_STUDI
				LEFT JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS
				LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
				WHERE (STATUS_AKADEMIK_MHS = 1 OR STATUS_AKADEMIK_MHS = 2 OR STATUS_AKADEMIK_MHS = 3 OR STATUS_AKADEMIK_MHS = 8
				OR STATUS_AKADEMIK_MHS = 9 OR STATUS_AKADEMIK_MHS = 10 OR STATUS_AKADEMIK_MHS = 11 OR STATUS_AKADEMIK_MHS = 14 
				OR STATUS_AKADEMIK_MHS = 15 OR STATUS_AKADEMIK_MHS = 17 OR STATUS_AKADEMIK_MHS = 18 OR STATUS_AKADEMIK_MHS = 19
				OR STATUS_AKADEMIK_MHS = 21)
				 $query
				ORDER BY FAKULTAS.ID_FAKULTAS, NM_JENJANG, NM_PROGRAM_STUDI, NIM_MHS");
    }


/*	function pembayaran_mhs($nim, $semester) {
        return $this->db->QueryToArray("SELECT NM_BIAYA, BIAYA.ID_BIAYA,  PEMBAYARAN.TGL_BAYAR, NM_BANK, 
								SUM(PEMBAYARAN.BESAR_BIAYA) BESAR_BIAYA, SUM(PEMBAYARAN.DENDA_BIAYA) DENDA_BIAYA FROM PEMBAYARAN
								LEFT JOIN MAHASISWA ON PEMBAYARAN.ID_MHS = MAHASISWA.ID_MHS
								LEFT JOIN DETAIL_BIAYA ON DETAIL_BIAYA.ID_DETAIL_BIAYA = PEMBAYARAN.ID_DETAIL_BIAYA
								LEFT JOIN BIAYA ON BIAYA.ID_BIAYA = DETAIL_BIAYA.ID_BIAYA
								LEFT JOIN BANK ON BANK.ID_BANK = PEMBAYARAN.ID_BANK
								WHERE NIM_MHS = '$nim' AND PEMBAYARAN.ID_SEMESTER = '$semester'
								GROUP BY NM_BIAYA, BIAYA.ID_BIAYA, PEMBAYARAN.TGL_BAYAR, NM_BANK
								ORDER BY TGL_BAYAR DESC, NM_BIAYA ASC");
    }*/
	
	
	function pembayaran_mhs($nim, $semester) {
        return $this->db->QueryToArray("SELECT PEMBAYARAN.TGL_BAYAR, NM_BANK, NM_BIAYA, T.ID_BIAYA,
								SUM(CASE WHEN T.ID_BIAYA = DETAIL_BIAYA.ID_BIAYA THEN PEMBAYARAN.BESAR_BIAYA ELSE 0 END) AS BESAR_BIAYA
							FROM (SELECT NM_BIAYA, BIAYA.ID_BIAYA FROM BIAYA
							LEFT JOIN DETAIL_BIAYA ON DETAIL_BIAYA.ID_BIAYA = BIAYA.ID_BIAYA
							LEFT JOIN PEMBAYARAN ON PEMBAYARAN.ID_DETAIL_BIAYA = DETAIL_BIAYA.ID_DETAIL_BIAYA
							LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
							WHERE NIM_MHS = '$nim'
							GROUP BY NM_BIAYA, BIAYA.ID_BIAYA) T, PEMBAYARAN
								LEFT JOIN MAHASISWA ON PEMBAYARAN.ID_MHS = MAHASISWA.ID_MHS
								LEFT JOIN DETAIL_BIAYA ON DETAIL_BIAYA.ID_DETAIL_BIAYA = PEMBAYARAN.ID_DETAIL_BIAYA
								LEFT JOIN BANK ON BANK.ID_BANK = PEMBAYARAN.ID_BANK
								WHERE NIM_MHS = '$nim' AND PEMBAYARAN.ID_SEMESTER = '$semester'
								GROUP BY PEMBAYARAN.TGL_BAYAR, NM_BANK, NM_BIAYA, T.ID_BIAYA
								ORDER BY PEMBAYARAN.TGL_BAYAR DESC, NM_BIAYA ASC");
    }

	function get_semester_aktif() {
        return $this->db->QueryToArray("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True' 
										ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
    }
	
	function biaya_mhs($nim) {
        return $this->db->QueryToArray("SELECT NM_BIAYA, BIAYA.ID_BIAYA FROM BIAYA
							LEFT JOIN DETAIL_BIAYA ON DETAIL_BIAYA.ID_BIAYA = BIAYA.ID_BIAYA
							LEFT JOIN PEMBAYARAN ON PEMBAYARAN.ID_DETAIL_BIAYA = DETAIL_BIAYA.ID_DETAIL_BIAYA
							LEFT JOIN MAHASISWA ON MAHASISWA.ID_MHS = PEMBAYARAN.ID_MHS
							WHERE NIM_MHS = '$nim'
							GROUP BY NM_BIAYA, BIAYA.ID_BIAYA 
							ORDER BY NM_BIAYA");
    }
	

    function cek_biaya_kuliah_mhs($nim) {
        $this->db->Query("
          SELECT T.NIM_MHS,T.ID_MHS, BK.ID_BIAYA_KULIAH,BK.BESAR_BIAYA_KULIAH FROM BIAYA_KULIAH BK
          RIGHT JOIN (
              SELECT M.NIM_MHS, M.ID_MHS, JM.ID_SEMESTER, M.ID_PROGRAM_STUDI, M.ID_KELOMPOK_BIAYA, JM.ID_JALUR FROM MAHASISWA M
              JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
              WHERE JM.ID_SEMESTER IS NOT NULL) T ON T.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
          WHERE
          BK.ID_KELOMPOK_BIAYA = T.ID_KELOMPOK_BIAYA AND BK.ID_SEMESTER = T.ID_SEMESTER AND BK.ID_JALUR = T.ID_JALUR
          AND T.ID_MHS NOT IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND T.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function cek_pembayaran($nim) {
        $this->db->Query("
        SELECT M.NIM_MHS,BKM.ID_MHS, 5 AS ID_SEMESTER, DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, 'Y' AS IS_TAGIH FROM BIAYA_KULIAH_MHS BKM
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
            LEFT JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
            WHERE
	M.ID_MHS IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND
	M.ID_MHS NOT IN (SELECT DISTINCT ID_MHS FROM PEMBAYARAN) AND M.NIM_MHS='{$nim}'");
        return $this->db->FetchAssoc();
    }

    function generate_pembayaran($nim) {
        $this->db->Query("
        INSERT INTO PEMBAYARAN (ID_MHS, ID_SEMESTER, ID_DETAIL_BIAYA, BESAR_BIAYA, IS_TAGIH)
            SELECT BKM.ID_MHS, 5 AS ID_SEMESTER, DB.ID_DETAIL_BIAYA, DB.BESAR_BIAYA, 'Y' AS IS_TAGIH FROM BIAYA_KULIAH_MHS BKM
            JOIN DETAIL_BIAYA DB ON DB.ID_BIAYA_KULIAH = BKM.ID_BIAYA_KULIAH
            LEFT JOIN MAHASISWA M ON M.ID_MHS = BKM.ID_MHS
            WHERE
        M.ID_MHS IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND
        M.ID_MHS NOT IN (SELECT DISTINCT ID_MHS FROM PEMBAYARAN) AND
        M.NIM_MHS ='{$nim}'");
    }

    function generate_biaya_kuliah($nim) {
        $this->db->Query("
        INSERT INTO BIAYA_KULIAH_MHS (ID_MHS, ID_BIAYA_KULIAH)
          SELECT T.ID_MHS, BK.ID_BIAYA_KULIAH FROM BIAYA_KULIAH BK
          RIGHT JOIN (
            SELECT M.NIM_MHS, M.ID_MHS, JM.ID_SEMESTER, M.ID_PROGRAM_STUDI, M.ID_KELOMPOK_BIAYA, JM.ID_JALUR FROM MAHASISWA M
            JOIN JALUR_MAHASISWA JM ON JM.ID_MHS = M.ID_MHS
            WHERE JM.ID_SEMESTER IS NOT NULL) T ON T.ID_PROGRAM_STUDI = BK.ID_PROGRAM_STUDI
          WHERE
            BK.ID_KELOMPOK_BIAYA = T.ID_KELOMPOK_BIAYA AND BK.ID_SEMESTER = T.ID_SEMESTER AND BK.ID_JALUR = T.ID_JALUR
            AND T.ID_MHS NOT IN (SELECT ID_MHS FROM BIAYA_KULIAH_MHS) AND
            T.NIM_MHS ='{$nim}'");
    }

}

?>
