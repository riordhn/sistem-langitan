/**
 * Author:  Fathoni <m.fathoni@mail.com>
 * Created: Sep 11, 2018
 */

WITH 
ips AS (
    SELECT id_mhs, SUM(sks) AS sks_semester, round(SUM(nilai_mutu) / SUM(CASE WHEN nilai_mutu IS NULL THEN 0 ELSE sks END), 2) AS ips 
    FROM (
        SELECT
            mhs.id_mhs,
            /* Kode MK */
            COALESCE(mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
            /* SKS */
            COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
            /* Nilai Mutu = SKS * Bobot */
            COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu
        FROM pengambilan_mk pmk
        JOIN mahasiswa mhs ON pmk.id_mhs = mhs.id_mhs
        JOIN semester S ON S.id_semester = pmk.id_semester
        -- Via Kelas
        LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
        LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
        -- Via Kurikulum
        LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
        LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
        -- nilai bobot
        LEFT JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
        WHERE 
            pmk.status_apv_pengambilan_mk = 1 AND 
            pmk.status_transfer = 0 AND
            pmk.id_semester = $id_semester AND
            mhs.id_program_studi = $id_program_studi
    )
    GROUP BY id_mhs
),

ipk AS (
    SELECT id_mhs,
        SUM(sks) AS sks_total, 
        round(SUM(nilai_mutu) / SUM(CASE WHEN nilai_mutu IS NULL THEN 0 ELSE sks END), 2) AS ipk 
    FROM (
        SELECT
            mhs.id_mhs,
            /* Kode MK utk grouping total mutu */
            COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) AS kd_mata_kuliah,
            /* SKS */
            COALESCE(mk1.kredit_semester, mk2.kredit_semester) AS sks,
            /* Nilai Mutu = SKS * Bobot */
            COALESCE(mk1.kredit_semester, mk2.kredit_semester) * sn.nilai_standar_nilai AS nilai_mutu,
			/* Jika seri, perulangan tetap ditotal */
			CASE kls.status_seri
				WHEN 1 THEN 1
				WHEN 0 THEN
					/* Urutan Nilai Terbaik */
					row_number() OVER (PARTITION BY mhs.id_mhs, COALESCE(mkp.nm_mk_penyetaraan, mk1.kd_mata_kuliah, mk2.kd_mata_kuliah) ORDER BY sn.nilai_standar_nilai DESC)
			END AS urut
        FROM pengambilan_mk pmk
        JOIN mahasiswa mhs ON mhs.id_mhs = pmk.id_mhs
        JOIN semester S ON S.id_semester = pmk.id_semester
        -- Via Kelas
        LEFT JOIN kelas_mk kls ON kls.id_kelas_mk = pmk.id_kelas_mk
        LEFT JOIN mata_kuliah mk1 ON mk1.id_mata_kuliah = kls.id_mata_kuliah
        -- Via Kurikulum
        LEFT JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = pmk.id_kurikulum_mk
        LEFT JOIN mata_kuliah mk2 ON mk2.id_mata_kuliah = kmk.id_mata_kuliah
        -- Penyetaraan
        LEFT JOIN mk_setara mks ON mks.id_mata_kuliah = COALESCE(mk1.id_mata_kuliah, mk2.id_mata_kuliah) AND mks.is_deleted = 0
        LEFT JOIN mk_penyetaraan mkp ON mkp.id_mk_penyetaraan = mks.id_mk_penyetaraan and mkp.is_deleted = 0
        -- nilai bobot
        LEFT JOIN standar_nilai sn ON sn.nm_standar_nilai = pmk.nilai_huruf
        WHERE 
            pmk.status_apv_pengambilan_mk = 1 AND 
            /* Hingga semester ke X */
            S.fd_id_smt <= (SELECT fd_id_smt FROM semester WHERE id_semester = $id_semester) AND
            mhs.id_program_studi = $id_program_studi AND
            pmk.id_mhs IN (
                /* Mahasiswa yang KRS pada semester ke X */
                SELECT DISTINCT pmk2.id_mhs FROM pengambilan_mk pmk2
                JOIN mahasiswa mhs2 ON mhs2.id_mhs = pmk2.id_mhs
                WHERE mhs2.id_program_studi = $id_program_studi AND pmk2.id_semester = $id_semester
            )
    )
    WHERE urut = 1
    GROUP BY id_mhs
)

SELECT ips.id_mhs, ips, sks_semester, ipk, sks_total
FROM ips
LEFT JOIN ipk ON ipk.id_mhs = ips.id_mhs