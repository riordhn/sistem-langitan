<?php

$id_pt = $id_pt_user;

class laporan {

	public $db;
	
	function __construct($db) {
		$this->db = $db;
	}
	
	function mhs_baru_jalur($fak, $angkatan, $tampilan) {
		if($tampilan == 'fak'){
        return $this->db->QueryToArray("
			select JML, id_fakultas from (
				SELECT NAMA_JALUR FROM (SELECT 
				(CASE WHEN B.NM_PENERIMAAN IS NULL THEN A.NM_JALUR ELSE NM_PENERIMAAN || ' (' || GELOMBANG || ')' END) NAMA_JALUR 
				FROM (
				select MAHASISWA.ID_MHS, NM_JALUR
				from mahasiswa
				join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
				join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
				join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs and AKTIF_STATUS_PENGGUNA = 1
				left join CALON_MAHASISWA_BARU on CALON_MAHASISWA_BARU.ID_C_MHS = MAHASISWA.ID_C_MHS
				left join JALUR_MAHASISWA on JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS and ID_JALUR_AKTIF = 1
				left join jalur on JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
				where thn_angkatan_mhs = '$angkatan'
				) A
				LEFT JOIN (
				select MAHASISWA.ID_MHS, NM_PENERIMAAN, GELOMBANG
				from mahasiswa
				join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
				join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
				join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs and AKTIF_STATUS_PENGGUNA = 1
				left join CALON_MAHASISWA_BARU on CALON_MAHASISWA_BARU.ID_C_MHS = MAHASISWA.ID_C_MHS
				left join JALUR_MAHASISWA on JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS and ID_JALUR_AKTIF = 1
				left join jalur on JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
				left join PENERIMAAN on PENERIMAAN.ID_PENERIMAAN = CALON_MAHASISWA_BARU.ID_PENERIMAAN
				where thn_angkatan_mhs = '$angkatan' AND TAHUN  = '$angkatan'
				) B ON B.ID_MHS = A.ID_MHS)
				GROUP BY NAMA_JALUR
			) w
			left join (
			select count(*) as jml, 
			(CASE WHEN NM_PENERIMAAN IS NULL THEN NM_JALUR ELSE NM_PENERIMAAN || ' (' || GELOMBANG || ')' END) AS NAMA_JALUR, 
			 id_fakultas
			from 
			(select MAHASISWA.ID_MHS, NM_JALUR, program_studi.id_fakultas	, program_studi.id_program_studi	
			from mahasiswa
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
						join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs and AKTIF_STATUS_PENGGUNA = 1
						left join JALUR_MAHASISWA on JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS and ID_JALUR_AKTIF = 1
						left join jalur on JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
			WHERE thn_angkatan_mhs = '$angkatan'
			) B
			LEFT JOIN (
			select MAHASISWA.ID_MHS, NM_PENERIMAAN, GELOMBANG
						from mahasiswa
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
						join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
						left join CALON_MAHASISWA_BARU on CALON_MAHASISWA_BARU.ID_C_MHS = MAHASISWA.ID_C_MHS
						left join JALUR_MAHASISWA on JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS and ID_JALUR_AKTIF = 1
						left join jalur on JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
						left join PENERIMAAN on PENERIMAAN.ID_PENERIMAAN = CALON_MAHASISWA_BARU.ID_PENERIMAAN
						where thn_angkatan_mhs = '$angkatan' AND TAHUN  = '$angkatan' 
			) A ON A.ID_MHS = B.ID_MHS
						where id_fakultas = '$fak'
			group by (CASE WHEN NM_PENERIMAAN IS NULL THEN NM_JALUR ELSE NM_PENERIMAAN || ' (' || GELOMBANG || ')' END), 
						 id_fakultas
			) Q ON Q.NAMA_JALUR = W.NAMA_JALUR
			ORDER BY W.NAMA_JALUR
            ");
			}else{
			return $this->db->QueryToArray("
			select JML, id_program_studi from (
				SELECT NAMA_JALUR FROM (SELECT 
				(CASE WHEN B.NM_PENERIMAAN IS NULL THEN A.NM_JALUR ELSE NM_PENERIMAAN || ' (' || GELOMBANG || ')' END) NAMA_JALUR 
				FROM (
				select MAHASISWA.ID_MHS, NM_JALUR
				from mahasiswa
				join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
				join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
				join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs and AKTIF_STATUS_PENGGUNA = 1
				left join CALON_MAHASISWA_BARU on CALON_MAHASISWA_BARU.ID_C_MHS = MAHASISWA.ID_C_MHS
				left join JALUR_MAHASISWA on JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS and ID_JALUR_AKTIF = 1
				left join jalur on JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
				where thn_angkatan_mhs = '$angkatan'
				) A
				LEFT JOIN (
				select MAHASISWA.ID_MHS, NM_PENERIMAAN, GELOMBANG
				from mahasiswa
				join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
				join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
				join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs and AKTIF_STATUS_PENGGUNA = 1
				left join CALON_MAHASISWA_BARU on CALON_MAHASISWA_BARU.ID_C_MHS = MAHASISWA.ID_C_MHS
				left join JALUR_MAHASISWA on JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS and ID_JALUR_AKTIF = 1
				left join jalur on JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
				left join PENERIMAAN on PENERIMAAN.ID_PENERIMAAN = CALON_MAHASISWA_BARU.ID_PENERIMAAN
				where thn_angkatan_mhs = '$angkatan' AND TAHUN  = '$angkatan'
				) B ON B.ID_MHS = A.ID_MHS)
				GROUP BY NAMA_JALUR
			) w
			left join (
			select count(*) as jml, 
			(CASE WHEN NM_PENERIMAAN IS NULL THEN NM_JALUR ELSE NM_PENERIMAAN || ' (' || GELOMBANG || ')' END) AS NAMA_JALUR, 
			 id_program_studi
			from 
			(select MAHASISWA.ID_MHS, NM_JALUR, program_studi.id_fakultas	, program_studi.id_program_studi	
			from mahasiswa
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
						join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs and AKTIF_STATUS_PENGGUNA = 1
						left join JALUR_MAHASISWA on JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS and ID_JALUR_AKTIF = 1
						left join jalur on JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
			WHERE thn_angkatan_mhs = '$angkatan'
			) B
			LEFT JOIN (
			select MAHASISWA.ID_MHS, NM_PENERIMAAN, GELOMBANG
						from mahasiswa
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
						join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
						left join CALON_MAHASISWA_BARU on CALON_MAHASISWA_BARU.ID_C_MHS = MAHASISWA.ID_C_MHS
						left join JALUR_MAHASISWA on JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS and ID_JALUR_AKTIF = 1
						left join jalur on JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
						left join PENERIMAAN on PENERIMAAN.ID_PENERIMAAN = CALON_MAHASISWA_BARU.ID_PENERIMAAN
						where thn_angkatan_mhs = '$angkatan' AND TAHUN  = '$angkatan' 
			) A ON A.ID_MHS = B.ID_MHS
						where id_program_studi = '$fak'
			group by (CASE WHEN NM_PENERIMAAN IS NULL THEN NM_JALUR ELSE NM_PENERIMAAN || ' (' || GELOMBANG || ')' END), 
						 id_program_studi
			) Q ON Q.NAMA_JALUR = W.NAMA_JALUR
			ORDER BY W.NAMA_JALUR
            ");
			}
    }
	
	function mhs_baru_jalur_total($jalur, $angkatan) {
        $this->db->Query("
            select count(*) as jml, 
			(CASE WHEN NM_PENERIMAAN IS NULL THEN JALUR.NM_JALUR ELSE NM_PENERIMAAN || ' (' || GELOMBANG || ')' END) AS NAMA_JALUR
						from mahasiswa
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
						join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs and AKTIF_STATUS_PENGGUNA = 1
						left join JALUR_MAHASISWA on JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS and ID_JALUR_AKTIF = 1
						left join jalur on JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
			LEFT JOIN (
			select MAHASISWA.NIM_MHS, MAHASISWA.ID_MHS, NM_JALUR, FAKULTAS.ID_FAKULTAS, NM_PENERIMAAN, GELOMBANG
						from mahasiswa
						join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
						join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
						join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
						left join CALON_MAHASISWA_BARU on CALON_MAHASISWA_BARU.ID_C_MHS = MAHASISWA.ID_C_MHS
						left join JALUR_MAHASISWA on JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS and ID_JALUR_AKTIF = 1
						left join jalur on JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
						left join PENERIMAAN on PENERIMAAN.ID_PENERIMAAN = CALON_MAHASISWA_BARU.ID_PENERIMAAN
						where thn_angkatan_mhs = '$angkatan' AND TAHUN  = '$angkatan'
			) A ON A.ID_MHS = MAHASISWA.ID_MHS
						where thn_angkatan_mhs = '$angkatan' AND 
						(CASE WHEN NM_PENERIMAAN IS NULL THEN JALUR.NM_JALUR ELSE NM_PENERIMAAN || ' (' || GELOMBANG || ')' END) = '$jalur'
			GROUP BY (CASE WHEN NM_PENERIMAAN IS NULL THEN JALUR.NM_JALUR ELSE NM_PENERIMAAN || ' (' || GELOMBANG || ')' END)
            ");
			
			return $this->db->FetchAssoc();
    }
	
	function fakultas() {
        return $this->db->QueryToArray("
            SELECT * FROM FAKULTAS WHERE ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}' ORDER BY ID_FAKULTAS ASC
            ");
    }

	function jenis_kerjasama() {
        return $this->db->QueryToArray("
            SELECT * FROM JENIS_KERJASAMA ORDER BY NM_JENIS_KERJASAMA
            ");
    }
	
	
	function jenjang() {
        return $this->db->QueryToArray("
            SELECT * FROM JENJANG ORDER BY NM_JENJANG ASC
            ");
    }
	
	function program_studi() {
        return $this->db->QueryToArray("
            SELECT * FROM PROGRAM_STUDI JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS 
			JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
			WHERE STATUS_AKTIF_PRODI = 1
			ORDER BY FAKULTAS.ID_FAKULTAS, JENJANG.NM_JENJANG, NM_PROGRAM_STUDI ASC
            ");
    }
	
	
	function program_studi_all($id_fakultas, $id_jenjang, $id_prodi) {
		$condition_akademik =  $this->get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi);
        return $this->db->QueryToArray("
            SELECT * FROM PROGRAM_STUDI JOIN FAKULTAS ON FAKULTAS.ID_FAKULTAS = PROGRAM_STUDI.ID_FAKULTAS 
			JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
			WHERE NM_PROGRAM_STUDI IS NOT NULL {$condition_akademik}
			ORDER BY FAKULTAS.ID_FAKULTAS, JENJANG.NM_JENJANG, NM_PROGRAM_STUDI ASC
            ");
    }
	
	
	function semester() {
        return $this->db->QueryToArray("
            SELECT * FROM SEMESTER WHERE (NM_SEMESTER = 'Genap' or NM_SEMESTER = 'Ganjil') AND ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}' ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC
            ");
    }
	
	function negara() {
        return $this->db->QueryToArray("
            SELECT * FROM NEGARA ORDER BY NM_NEGARA
            ");
    }
	
	
	function tahun() {
        return $this->db->QueryToArray("
            SELECT DISTINCT TAHUN FROM PENERIMAAN ORDER BY TAHUN DESC
            ");
    }
	
	function angkatan() {
        return $this->db->QueryToArray("
            SELECT DISTINCT THN_ANGKATAN_MHS FROM MAHASISWA ORDER BY THN_ANGKATAN_MHS DESC
            ");
    }
	
	function thn_akademik() {
        return $this->db->QueryToArray("
            SELECT TAHUN_AJARAN, THN_AKADEMIK_SEMESTER FROM SEMESTER WHERE ID_PERGURUAN_TINGGI = '{$GLOBALS[id_pt]}' GROUP BY TAHUN_AJARAN, THN_AKADEMIK_SEMESTER 
			ORDER BY THN_AKADEMIK_SEMESTER DESC
            ");
    }
	
	function sumber_dana() {
        return $this->db->QueryToArray("
            SELECT * FROM BEASISWA ORDER BY NM_BEASISWA ASC
            ");
    }
	
	function status_tidak_aktif() {
        return $this->db->QueryToArray("
            SELECT * FROM STATUS_PENGGUNA WHERE AKTIF_STATUS_PENGGUNA = 0 AND ID_ROLE = 3 ORDER BY NM_STATUS_PENGGUNA
            ");
    }
	
	
	function jalur_penerimaan($thn_penerimaan, $id_fakultas, $id_jenjang, $id_prodi) {

		$condition_akademik =  $this->get_condition_akademik($id_fakultas, $id_jenjang, '');
		
		return $this->db->QueryToArray("
				select distinct nm_penerimaan || ' Gel ' || gelombang as nama_jalur
				from calon_mahasiswa_baru
				join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
				join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
				where TAHUN  = '$thn_penerimaan' {$condition_akademik}
				order by nm_penerimaan || ' Gel ' || gelombang ");
	}
	
	
	
	function jalur_mhs($angkatan) {
		
        return $this->db->QueryToArray("
				SELECT NAMA_JALUR FROM (SELECT 
				(CASE WHEN B.NM_PENERIMAAN IS NULL THEN A.NM_JALUR ELSE NM_PENERIMAAN || ' (' || GELOMBANG || ')' END) NAMA_JALUR 
				FROM (
				select MAHASISWA.ID_MHS, NM_JALUR
				from mahasiswa
				join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
				join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
				join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs and AKTIF_STATUS_PENGGUNA = 1
				left join CALON_MAHASISWA_BARU on CALON_MAHASISWA_BARU.ID_C_MHS = MAHASISWA.ID_C_MHS
				left join JALUR_MAHASISWA on JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS and ID_JALUR_AKTIF = 1
				left join jalur on JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
				where thn_angkatan_mhs = '$angkatan'
				) A
				LEFT JOIN (
				select MAHASISWA.ID_MHS, NM_PENERIMAAN, GELOMBANG
				from mahasiswa
				join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
				join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
				join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs and AKTIF_STATUS_PENGGUNA = 1
				left join CALON_MAHASISWA_BARU on CALON_MAHASISWA_BARU.ID_C_MHS = MAHASISWA.ID_C_MHS
				left join JALUR_MAHASISWA on JALUR_MAHASISWA.ID_MHS = MAHASISWA.ID_MHS and ID_JALUR_AKTIF = 1
				left join jalur on JALUR.ID_JALUR = JALUR_MAHASISWA.ID_JALUR
				left join PENERIMAAN on PENERIMAAN.ID_PENERIMAAN = CALON_MAHASISWA_BARU.ID_PENERIMAAN
				where thn_angkatan_mhs = '$angkatan' AND TAHUN  = '$angkatan'
				) B ON B.ID_MHS = A.ID_MHS)
				GROUP BY NAMA_JALUR
				ORDER BY NAMA_JALUR ASC");
    }
	
	
	function mhs_jenjang($thn, $tampilan, $thn_akademik, $status_akademik, $jenjang, $fakultas, $prodi, $status) {
		
		if($thn != ''){
			$where .= " and thn_angkatan_mhs = '$thn' ";
		}
		
		$where .= $this->get_condition_akademik($fakultas, $jenjang, $prodi);

		
		if($thn_akademik != '' and $thn == ''){
			$where .= " and thn_angkatan_mhs <= '$thn_akademik' ";
		}
		
		if($status_akademik == 1){
			$where .= "  and AKTIF_STATUS_PENGGUNA = 1 ";
		}elseif($status_akademik == 4){
			$where .= "  and status_akademik_mhs = '{$status_akademik}' and mahasiswa.id_mhs in 
						(select id_mhs from admisi 
						where status_akd_mhs = '{$status_akademik}' and status_apv = 1 and to_char(tgl_lulus,  'YYYY') = '$thn_akademik')";
		}else{
			if($status_akademik != ''){
			$where .= "  and status_akademik_mhs = '{$status_akademik}' and mahasiswa.id_mhs in 
						(select id_mhs from admisi 
						where status_akd_mhs = '{$status_akademik}' and status_apv = 1 and to_char(tgl_sk,  'YYYY') = '$thn_akademik')";
			}
		}
		
		if($status != ''){
			$where .= " and mahasiswa.status = '$status'";	
		}
		
		if($tampilan == 'fak'){

        return $this->db->QueryToArray("
            select ID_FAKULTAS, NM_FAKULTAS, SUM(DECODE(id_jenjang, '5-R', JML, NULL)) D3, SUM(DECODE(id_jenjang, '1-R', JML, NULL)) S1_REGULER
			, SUM(DECODE(id_jenjang, '1-AJ', JML, NULL)) S1_AJ, SUM(DECODE(id_jenjang, '1-I', JML, NULL)) S1_I
			, SUM(DECODE(id_jenjang, '2-R', JML, NULL)) S2 , SUM(DECODE(id_jenjang, '3-R', JML, NULL)) S3 
			, SUM(DECODE(id_jenjang, '9-R', JML, NULL)) PROFESI , SUM(DECODE(id_jenjang, '10-R', JML, NULL)) SPESIALIS , SUM(JML) AS TOTAL
			from (
			select FAKULTAS.id_fakultas, nm_fakultas, JENJANG.id_jenjang || '-' || MAHASISWA.STATUS AS ID_JENJANG, nm_jenjang, count(*) as jml
							from mahasiswa
							join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
							join fakultas on fakultas.id_fakultas = program_studi.id_fakultas and fakultas.id_perguruan_tinggi = '{$GLOBALS[id_pt]}'
							join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
							join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
							where program_studi.status_aktif_prodi = 1 $where
			group by FAKULTAS.id_fakultas, nm_fakultas, JENJANG.id_jenjang || '-' || MAHASISWA.STATUS, nm_jenjang)
			GROUP BY ID_FAKULTAS, NM_FAKULTAS
			ORDER BY ID_FAKULTAS ASC
            ");
		}elseif($tampilan == 'detail'){
			return $this->db->QueryToArray("
			select FAKULTAS.id_fakultas, nm_fakultas, JENJANG.id_jenjang, nm_jenjang, NIM_MHS, NM_PENGGUNA, nm_program_studi, nm_status_pengguna
							from mahasiswa
							join pengguna on pengguna.id_pengguna = mahasiswa.id_pengguna
							join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
							join fakultas on fakultas.id_fakultas = program_studi.id_fakultas and fakultas.id_perguruan_tinggi = '{$GLOBALS[id_pt]}'
							join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
							join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
							where program_studi.status_aktif_prodi = 1 $where
			ORDER BY FAKULTAS.id_fakultas, nm_jenjang, nm_program_studi, nim_mhs ASC
            ");
		}elseif($tampilan == 'thn_angkatan'){
			
			return $this->db->QueryToArray("
			select THN_ANGKATAN_MHS, SUM(DECODE(id_jenjang, '5-R', JML, NULL)) D3, SUM(DECODE(id_jenjang, '1-R', JML, NULL)) S1_REGULER
			, SUM(DECODE(id_jenjang, '1-AJ', JML, NULL)) S1_AJ, SUM(DECODE(id_jenjang, '1-I', JML, NULL)) S1_I
			, SUM(DECODE(id_jenjang, '2-R', JML, NULL)) S2 , SUM(DECODE(id_jenjang, '3-R', JML, NULL)) S3 
			, SUM(DECODE(id_jenjang, '9-R', JML, NULL)) PROFESI , SUM(DECODE(id_jenjang, '10-R', JML, NULL)) SPESIALIS , SUM(JML) AS TOTAL
			from (
			select THN_ANGKATAN_MHS, JENJANG.id_jenjang || '-' || MAHASISWA.STATUS AS ID_JENJANG, nm_jenjang, count(*) as jml
							from mahasiswa
							join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
							join fakultas on fakultas.id_fakultas = program_studi.id_fakultas and fakultas.id_perguruan_tinggi = '{$GLOBALS[id_pt]}'
							join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
							join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
							where program_studi.status_aktif_prodi = 1  $where
			group by THN_ANGKATAN_MHS, JENJANG.id_jenjang|| '-' || MAHASISWA.STATUS, nm_jenjang, MAHASISWA.STATUS)
			GROUP BY THN_ANGKATAN_MHS
			ORDER BY THN_ANGKATAN_MHS DESC
            ");
		}else{
			return $this->db->QueryToArray("
            select ID_FAKULTAS, NM_FAKULTAS, ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, SUM(DECODE(id_jenjang, '5-R', JML, NULL)) D3, SUM(DECODE(id_jenjang, '1-R', JML, NULL)) S1_REGULER
			, SUM(DECODE(id_jenjang, '1-AJ', JML, NULL)) S1_AJ, SUM(DECODE(id_jenjang, '1-I', JML, NULL)) S1_I
			, SUM(DECODE(id_jenjang, '2-R', JML, NULL)) S2 , SUM(DECODE(id_jenjang, '3-R', JML, NULL)) S3 
			, SUM(DECODE(id_jenjang, '9-R', JML, NULL)) PROFESI , SUM(DECODE(id_jenjang, '10-R', JML, NULL)) SPESIALIS , SUM(JML) AS TOTAL
			from (
			select FAKULTAS.ID_FAKULTAS, NM_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, JENJANG.id_jenjang || '-' || MAHASISWA.STATUS AS ID_JENJANG, nm_jenjang, count(*) as jml
							from mahasiswa
							join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
							join fakultas on fakultas.id_fakultas = program_studi.id_fakultas and fakultas.id_perguruan_tinggi = '{$GLOBALS[id_pt]}'
							join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
							join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
							where program_studi.status_aktif_prodi = 1 $where
			group by FAKULTAS.ID_FAKULTAS, NM_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, JENJANG.id_jenjang || '-' || MAHASISWA.STATUS, nm_jenjang)
			GROUP BY ID_FAKULTAS, NM_FAKULTAS, ID_PROGRAM_STUDI, NM_PROGRAM_STUDI
			ORDER BY ID_FAKULTAS, ID_PROGRAM_STUDI ASC
            ");
		
		}				
    }
	
	
	function prodi_condition($id_fakultas, $id_jenjang) {
		
		$condition_akademik =  $this->get_condition_akademik($id_fakultas, $id_jenjang, '');
		
        return $this->db->QueryToArray("
            SELECT * FROM PROGRAM_STUDI WHERE PROGRAM_STUDI.STATUS_AKTIF_PRODI = 1 {$condition_akademik} ORDER BY NM_PROGRAM_STUDI ASC
            ");
    }
	
	 private function get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi) {
        if ($id_fakultas != "" && $id_prodi != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}' 
						AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_fakultas != "" && $id_prodi != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } elseif ($id_fakultas != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_prodi != "" && $id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}' AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } elseif ($id_prodi != "") {
            $query = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI ='{$id_prodi}'";
        } elseif ($id_jenjang != "") {
            $query = " AND PROGRAM_STUDI.ID_JENJANG ='{$id_jenjang}'";
        } else if ($id_fakultas != "") {
            $query = " AND PROGRAM_STUDI.ID_FAKULTAS = '{$id_fakultas}' ";
        } else {
            $query = " ";
        }
        return $query;
    }
	
	
	private function get_condition_negara($negara, $provinsi, $kota) {
        if ($negara != "" && $provinsi != "" && $kota != "") {
            $query = " AND NEGARA.ID_NEGARA = '{$negara}' AND PROVINSI.ID_PROVINSI ='{$provinsi}' AND KOTA.ID_KOTA ='{$kota}'";
        } elseif ($negara != "" && $kota != "") {
            $query = " AND NEGARA.ID_NEGARA = '{$negara}' AND KOTA.ID_KOTA ='{$kota}'";
        } elseif ($negara != "" && $provinsi != "") {
            $query = " AND NEGARA.ID_NEGARA = '{$negara}' AND PROVINSI.ID_PROVINSI ='{$provinsi}'";
        } elseif ($provinsi != "" && $kota != "") {
            $query = " AND PROVINSI.ID_PROVINSI ='{$provinsi}' AND KOTA.ID_KOTA ='{$kota}'";
        } elseif ($kota != "") {
            $query = " AND KOTA.ID_KOTA ='{$kota}'";
        } elseif ($provinsi != "") {
            $query = " AND PROVINSI.ID_PROVINSI ='{$provinsi}'";
        } else if ($negara != "") {
            $query = " AND NEGARA.ID_NEGARA = '{$negara}' ";
        } else {
            $query = " ";
        }
        return $query;
    }
	
	private function get_condition_penerimaan($id_penerimaan, $id_semester, $id_jalur, $thn_penerimaan, $id_jenjang, $gelombang, $tgl_mulai, $tgl_selesai) {
        						
		
		if ($id_penerimaan != "") {
            $query .= " and calon_mahasiswa_baru.id_penerimaan = '{$id_penerimaan}' ";
        } else if ($id_semester != "" && $id_jalur != "" && $thn_penerimaan != "" && $id_jenjang != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang'
					   and penerimaan.gelombang = '$gelombang' 
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        }else if ($id_semester != "" && $id_jalur != "" && $thn_penerimaan != "" && $id_jenjang != "" && $gelombang != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang'
					   and penerimaan.gelombang = '$gelombang' ";
        } else if ($id_semester != "" && $id_jalur != "" && $thn_penerimaan != "" && $id_jenjang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang'
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $id_jalur != "" && $thn_penerimaan != "" && $id_jenjang != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang' ";
        } else if ($id_semester != "" && $id_jalur != "" && $thn_penerimaan != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan' and  penerimaan.gelombang = '$gelombang' 
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $id_jalur != "" && $thn_penerimaan != "" && $gelombang != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan' and  penerimaan.gelombang = '$gelombang' ";
        } else if ($id_semester != "" && $id_jalur != "" && $id_jenjang != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.id_jenjang = '$id_jenjang' and penerimaan.gelombang = '$gelombang' 
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $id_jalur != "" && $id_jenjang != "" && $gelombang != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.id_jenjang = '$id_jenjang' and penerimaan.gelombang = '$gelombang' ";
        } else if ($id_semester != "" && $thn_penerimaan != "" && $id_jenjang != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester'
					   and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang'
					   and penerimaan.gelombang = '$gelombang' 
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $thn_penerimaan != "" && $id_jenjang != "" && $gelombang != "") {
            $query .= " and penerimaan.semester = '$id_semester'
					   and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang'
					   and penerimaan.gelombang = '$gelombang' ";
        } else if ($id_jalur != "" && $thn_penerimaan != "" && $id_jenjang != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang'
					   and penerimaan.gelombang = '$gelombang' 
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_jalur != "" && $thn_penerimaan != "" && $id_jenjang != "" && $gelombang != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang'
					   and penerimaan.gelombang = '$gelombang' ";
        } else if ($id_semester != "" && $id_jalur != "" && $thn_penerimaan != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan'
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $id_jalur != "" && $thn_penerimaan != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan'";
        } else if ($id_semester != "" && $id_jalur != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.gelombang = '$gelombang' 
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $id_jalur != "" && $gelombang != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.gelombang = '$gelombang' ";
        } else if ($id_semester != "" && $id_jenjang != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jenjang = '$id_jenjang'
					   and penerimaan.gelombang = '$gelombang' 
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $id_jenjang != "" && $gelombang != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jenjang = '$id_jenjang'
					   and penerimaan.gelombang = '$gelombang' ";
        } else if ($thn_penerimaan != "" && $id_jenjang != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang'
					   and penerimaan.gelombang = '$gelombang' 
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($thn_penerimaan != "" && $id_jenjang != "" && $gelombang != "") {
            $query .= " and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang'
					   and penerimaan.gelombang = '$gelombang' ";
        } else if ($id_jalur != "" && $id_jenjang != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.id_jenjang = '$id_jenjang'
					   and penerimaan.gelombang = '$gelombang' 
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_jalur != "" && $id_jenjang != "" && $gelombang != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.id_jenjang = '$id_jenjang'
					   and penerimaan.gelombang = '$gelombang' ";
        } else if ($id_jalur != "" && $thn_penerimaan != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan'
					   and penerimaan.gelombang = '$gelombang' 
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_jalur != "" && $thn_penerimaan != "" && $gelombang != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan'
					   and penerimaan.gelombang = '$gelombang' ";
        } else if ($id_jalur != "" && $thn_penerimaan != "" && $id_jenjang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang' 
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_jalur != "" && $thn_penerimaan != "" && $id_jenjang != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang' ";
        } else if ($id_semester != "" && $thn_penerimaan != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester'
					   and penerimaan.tahun = '$thn_penerimaan'
					   and penerimaan.gelombang = '$gelombang' 
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $thn_penerimaan != "" && $gelombang != "") {
            $query .= " and penerimaan.semester = '$id_semester'
					   and penerimaan.tahun = '$thn_penerimaan'
					   and penerimaan.gelombang = '$gelombang' ";
        } else if ($id_semester != "" && $thn_penerimaan != "" && $id_jenjang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester'
					   and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang'
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $thn_penerimaan != "" && $id_jenjang != "") {
            $query .= " and penerimaan.semester = '$id_semester'
					   and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang'";
        } else if ($id_semester != "" && $id_jalur != "" && $id_jenjang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.id_jenjang = '$id_jenjang'
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $id_jalur != "" && $id_jenjang != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur'
					   and penerimaan.id_jenjang = '$id_jenjang'";
        } else if ($id_semester != "" && $id_jalur != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur' 
					   and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $id_jalur != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jalur = '$id_jalur' ";
        } else if ($id_semester != "" && $thn_penerimaan != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.tahun = '$thn_penerimaan' 
						and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $thn_penerimaan != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.tahun = '$thn_penerimaan' ";
        } else if ($id_semester != "" && $id_jenjang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jenjang = '$id_jenjang' 
						and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $id_jenjang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jenjang = '$id_jenjang' 
						and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $id_jenjang != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.id_jenjang = '$id_jenjang' ";
        } else if ($id_semester != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.gelombang = '$gelombang' 
						and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "" && $gelombang != "") {
            $query .= " and penerimaan.semester = '$id_semester' and penerimaan.gelombang = '$gelombang' ";
        } else if ($id_jalur != "" && $thn_penerimaan != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur' and penerimaan.tahun = '$thn_penerimaan' 
						and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_jalur != "" && $thn_penerimaan != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur' and penerimaan.tahun = '$thn_penerimaan' ";
        } else if ($id_jalur != "" && $id_jenjang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur' and penerimaan.id_jenjang = '$id_jenjang' 
						and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_jalur != "" && $id_jenjang != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur' and penerimaan.id_jenjang = '$id_jenjang' ";
        } else if ($id_jalur != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur' and penerimaan.gelombang = '$gelombang' 
						and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_jalur != "" && $gelombang != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur' and penerimaan.gelombang = '$gelombang' ";
        } else if ($thn_penerimaan != "" && $id_jenjang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang' 
						and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($thn_penerimaan != "" && $id_jenjang != "") {
            $query .= " and penerimaan.tahun = '$thn_penerimaan' and penerimaan.id_jenjang = '$id_jenjang' ";
        } else if ($thn_penerimaan != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.tahun = '$thn_penerimaan' and penerimaan.gelombang = '$gelombang' 
						and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($thn_penerimaan != "" && $gelombang != "") {
            $query .= " and penerimaan.tahun = '$thn_penerimaan' and penerimaan.gelombang = '$gelombang' ";
        } else if ($id_jenjang != "" && $gelombang != "" && $tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and penerimaan.id_jenjang = '$id_jenjang' and penerimaan.gelombang = '$gelombang' 
						and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_jenjang != "" && $gelombang != "") {
            $query .= " and penerimaan.id_jenjang = '$id_jenjang' and penerimaan.gelombang = '$gelombang' ";
        } else if ($tgl_mulai != "" && $tgl_selesai != "") {
            $query .= " and calon_mahasiswa_baru.tgl_diterima between TO_DATE('$tgl_mulai', 'DD-MM-YYYY') AND TO_DATE('$tgl_selesai', 'DD-MM-YYYY') ";
        } else if ($id_semester != "") {
            $query .= " and penerimaan.semester = '$id_semester' ";
        } else if ($id_jalur != "") {
            $query .= " and penerimaan.id_jalur = '$id_jalur' ";
        } else if ($thn_penerimaan != "") {
            $query .= " and penerimaan.tahun = '$thn_penerimaan' ";
        } else if ($id_jenjang != "") {
            $query .= " and penerimaan.id_jenjang = '$id_jenjang' ";
        } else if ($gelombang != "") {
            $query .= " and penerimaan.gelombang = '$gelombang' ";
        } else {
            $query .= " ";
        }
        return $query;
    }
	
	
	function penerimaan($tampilan, $id_penerimaan, $id_fakultas, $id_semester, $id_jalur, $thn_penerimaan, $id_jenjang, $gelombang, $tgl_mulai, $tgl_selesai, $tgl_awal, $tgl_akhir) {
		
		$select = "";
		$where = "";
		$group = "";
		$order = "";
		
		$condition_akademik = $this->get_condition_akademik($id_fakultas, $id_jenjang, '');
		$condition_penerimaan = $this->get_condition_penerimaan($id_penerimaan, $id_semester, $id_jalur, $thn_penerimaan, $id_jenjang, $gelombang, $tgl_mulai, $tgl_selesai, $tgl_awal, $tgl_akhir);
		
		if ($id_penerimaan != "" || $id_fakultas != "" || $id_prodi != "" || $id_semester != "" || $id_jalur != "" || $thn_penerimaan != "" || $id_jenjang != "" || $gelombang != "" || $tgl_mulai != "" || $tgl_selesai != ""){
			$where = " where program_studi.id_program_studi is not null  AND TGL_DITERIMA IS NOT NULL {$condition_akademik} {$condition_penerimaan} ";
		}
		
		
		if($tgl_awal != '' and $tgl_akhir != ''){
			$query = " and tgl_bayar between to_date('$tgl_awal', 'DD-MM-YYYY') and to_date('$tgl_akhir 23:59', 'DD-MM-YYYY HH24:MI')";
		}else{
			$query = " ";	
		}
		
		
		if($tampilan == 'fak'){
			$select = "fakultas.id_fakultas, nm_fakultas, (nm_penerimaan||' Gel-'||gelombang||' '||tahun||' '||semester) as nama_penerimaan";
			$group = "fakultas.id_fakultas, nm_fakultas, (nm_penerimaan||' Gel-'||gelombang||' '||tahun||' '||semester)";
			$order = "fakultas.id_fakultas, nm_fakultas, (nm_penerimaan||' Gel-'||gelombang||' '||tahun||' '||semester)";
		}else{
			$select = "fakultas.id_fakultas, nm_fakultas, program_studi.id_program_studi, nm_program_studi, 
						(nm_penerimaan||' Gel-'||gelombang||' '||tahun||' '||semester) as nama_penerimaan";
			$group = "fakultas.id_fakultas, nm_fakultas, program_studi.id_program_studi, nm_program_studi, (nm_penerimaan||' Gel-'||gelombang||' '||tahun||' '||semester)";
			$order = "fakultas.id_fakultas, nm_fakultas, program_studi.id_program_studi, nm_program_studi, (nm_penerimaan||' Gel-'||gelombang||' '||tahun||' '||semester)";
		}
		
		
		
		return $this->db->QueryToArray("
			select $select, count(*) as diterima, count(a.id_c_mhs) as regmaba, count(b.id_c_mhs) as verifikasi_keuangan, count(c.id_c_mhs) as bayar, 
			count(d.id_c_mhs) as penangguhan, count(e.id_c_mhs) as pembebasan, count(f.id_c_mhs) as bidik_misi_unair, 
			count(g.id_c_mhs) as bidik_misi_nasional, count(h.id_c_mhs) as verifikasi_pendidikan, count(i.id_c_mhs) as finger, 
			count(j.id_c_mhs) as ktm, count(k.id_c_mhs) as tidak_lulus_kesehatan, count(l.id_c_mhs) as registrasi, 
			(count(*) - count(l.id_c_mhs)) as tidak_registrasi
			from calon_mahasiswa_baru
			join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
			join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
			join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			left join (
				select id_c_mhs from calon_mahasiswa_baru
				where tgl_regmaba is not null
			) a on a.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			left join (
				select id_c_mhs from calon_mahasiswa_baru
				where tgl_verifikasi_keuangan is not null
			) b on b.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			left join (
				select pembayaran_cmhs.id_c_mhs from pembayaran_cmhs
				where (id_status_pembayaran != 2 or tgl_bayar is not null) $query
				group by pembayaran_cmhs.id_c_mhs
				union
				select calon_mahasiswa_baru.id_c_mhs from calon_mahasiswa_baru
				join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
				where status_bidik_misi != 0 and status_bidik_misi is not null and tgl_verifikasi_pendidikan is not null
			) c on c.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			left join (
				select pembayaran_cmhs.id_c_mhs from pembayaran_cmhs
				where id_status_pembayaran = 3 
				group by pembayaran_cmhs.id_c_mhs
			) d on d.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			left join (
				select id_c_mhs from pembayaran_cmhs
				where id_status_pembayaran = 4 
				group by pembayaran_cmhs.id_c_mhs
			) e on e.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			left join (
				select calon_mahasiswa_baru.id_c_mhs from calon_mahasiswa_baru
				join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
				where status_bidik_misi = 2
				group by calon_mahasiswa_baru.id_c_mhs
			) f on f.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			left join (
				select calon_mahasiswa_baru.id_c_mhs from calon_mahasiswa_baru
				join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
				where status_bidik_misi = 1
				group by calon_mahasiswa_baru.id_c_mhs
			) g on g.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			left join (
				select id_c_mhs from calon_mahasiswa_baru
				where tgl_verifikasi_pendidikan is not null
			) h on h.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			left join (
				select id_c_mhs from fingerprint_cmhs
				where finger_data is not null
			) i on i.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			left join (
				select id_c_mhs from calon_mahasiswa_baru
				where tgl_cetak_ktm is not null
			) j on j.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			left join (
				select calon_mahasiswa_baru.id_c_mhs from calon_mahasiswa_baru
				join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
				where kesehatan_kesimpulan_akhir = 0 and (tgl_verifikasi_kesehatan != '' or tgl_verifikasi_kesehatan is not null)
			) k on k.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			left join (
				select pembayaran_cmhs.id_c_mhs from pembayaran_cmhs
				where (id_status_pembayaran != 2 or tgl_bayar is not null) $query
				group by pembayaran_cmhs.id_c_mhs
				union
				select calon_mahasiswa_baru.id_c_mhs from calon_mahasiswa_baru
				join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
				where status_bidik_misi != 0 and status_bidik_misi is not null and tgl_verifikasi_pendidikan is not null
			) l on l.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			$where
			group by $group
			order by $order
		");
		
	}
	
	
	function penerimaan2($id_fakultas, $id_jenjang, $id_prodi, $thn_penerimaan, $tampilan, $nm_penerimaan) {
		
		$condition_akademik = $this->get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi);
		
		if($tampilan == 'fak'){
		
			$select = " program_studi.id_fakultas ";
		
		}else{
			
			$select = " program_studi.id_program_studi ";
			
		}
		
		return $query = "
				select a.*, b.*, c.*, (b.diterima - c.daftar_ulang) as tidak_daftar_ulang
				from (
					select distinct nm_penerimaan || ' Gel ' || gelombang as nama_jalur
					from calon_mahasiswa_baru
					join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
					join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
					where TAHUN  = '$thn_penerimaan' and (nm_penerimaan || ' Gel ' || gelombang) = '$nm_penerimaan' {$condition_akademik}
					order by nm_penerimaan || ' Gel ' || gelombang
				) a
				left join (
					select count(*) as diterima, nm_penerimaan  || ' Gel ' || gelombang as nm_penerimaan, $select
					from calon_mahasiswa_baru
					join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
					join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
					join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
					join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
					where tahun = '$thn_penerimaan' and (nm_penerimaan || ' Gel ' || gelombang) = '$nm_penerimaan' {$condition_akademik}
					group by nm_penerimaan  || ' Gel ' || gelombang, $select
				) b on b.nm_penerimaan = a.nama_jalur
				left join (
					select count(*) as daftar_ulang, nm_penerimaan  || ' Gel ' || gelombang as nama_jalur, $select
					from calon_mahasiswa_baru
					join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
					join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
					join (
						select pembayaran_cmhs.id_c_mhs from pembayaran_cmhs
						where (id_status_pembayaran != 2 or tgl_bayar is not null)
						group by pembayaran_cmhs.id_c_mhs
						union
						select calon_mahasiswa_baru.id_c_mhs from calon_mahasiswa_baru
						join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
						where status_bidik_misi != 0 and status_bidik_misi is not null and tgl_verifikasi_pendidikan is not null
					) s on s.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
					where TAHUN  = '$thn_penerimaan' and (nm_penerimaan || ' Gel ' || gelombang) = '$nm_penerimaan' {$condition_akademik}
					group by nm_penerimaan  || ' Gel ' || gelombang, $select
				) c on c.nama_jalur = a.nama_jalur
				order by a.nama_jalur
		";
	
	}
	
	
	function penerimaan_detail($id_penerimaan, $id_fakultas, $id_semester, $id_jalur, $thn_penerimaan, $id_jenjang, $gelombang, $status, $id_prodi){
		
		$condition_akademik = $this->get_condition_akademik($id_fakultas, $id_jenjang, $id_prodi);
		$condition_penerimaan = $this->get_condition_penerimaan($id_penerimaan, $id_semester, $id_jalur, $thn_penerimaan, $id_jenjang, $gelombang);
		
		if ($id_penerimaan != "" || $id_fakultas != "" || $id_semester != "" || $id_jalur != "" || $thn_penerimaan != "" || $id_jenjang != "" || $gelombang != ""){
			$where = " where program_studi.id_program_studi is not null AND TGL_DITERIMA IS NOT NULL {$condition_akademik} {$condition_penerimaan} ";
		}
		
		if($status == 'kesehatan'){
			
			return $this->db->QueryToArray("
			select fakultas.nm_fakultas, program_studi.nm_program_studi, no_ujian, nm_c_mhs, 
					b.nm_fakultas as nm_fak, a.nm_program_studi as nm_prod, mahasiswa.nim_mhs, nm_penerimaan
			from calon_mahasiswa_baru
			join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
			join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
			join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			left join mahasiswa on mahasiswa.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			join program_studi a on a.id_program_studi = mahasiswa.id_program_studi
			join fakultas b on b.id_fakultas = a.id_fakultas
			$where and kesehatan_kesimpulan_akhir = 0 and (tgl_verifikasi_kesehatan != '' or tgl_verifikasi_kesehatan is not null)
			order by fakultas.id_fakultas, program_studi.id_program_studi, no_ujian");
			
		}elseif($status == 'diterima'){
		
			return $this->db->QueryToArray("
			select fakultas.nm_fakultas, program_studi.nm_program_studi, no_ujian, nm_c_mhs, nim_mhs, nm_penerimaan, tgl_diterima, nm_jenjang, tgl_verifikasi_pendidikan, kode_voucher
			from calon_mahasiswa_baru
			join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
			join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
			join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			$where
			order by fakultas.id_fakultas, program_studi.id_program_studi, no_ujian");	
		}elseif($status == 'bayar'){

			return $this->db->QueryToArray("
			select fakultas.id_fakultas, fakultas.nm_fakultas, program_studi.id_program_studi, program_studi.nm_program_studi, no_ujian, nm_c_mhs, nim_mhs, nm_penerimaan, tgl_diterima, nm_jenjang, tgl_verifikasi_pendidikan
			from calon_mahasiswa_baru
			join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
			join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
			join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			join (
				select pembayaran_cmhs.id_c_mhs from pembayaran_cmhs
				join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = pembayaran_cmhs.id_c_mhs
				where (id_status_pembayaran != 2 or tgl_bayar is not null) -- and (status_bidik_misi = 0 or status_bidik_misi is null) 
				group by pembayaran_cmhs.id_c_mhs
			) a on a.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			$where
			
			union

			select fakultas.id_fakultas, fakultas.nm_fakultas, program_studi.id_program_studi, program_studi.nm_program_studi, no_ujian, nm_c_mhs, nim_mhs, nm_penerimaan, tgl_diterima, nm_jenjang, tgl_verifikasi_pendidikan
			from calon_mahasiswa_baru
			join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
			join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
			join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs			
			$where and (status_bidik_misi != 0 and status_bidik_misi is not null)
			
			order by id_fakultas, id_program_studi, no_ujian");	
		}elseif($status == 'verifikasi'){		
			return $this->db->QueryToArray("
			select fakultas.nm_fakultas, program_studi.nm_program_studi, no_ujian, nm_c_mhs, nim_mhs, nm_penerimaan, tgl_diterima, nm_jenjang, tgl_verifikasi_pendidikan
			from calon_mahasiswa_baru
			join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
			join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
			join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			$where and tgl_verifikasi_pendidikan is not null
			order by fakultas.id_fakultas, program_studi.id_program_studi, no_ujian");	
		}elseif($status == 'finger'){		
			return $this->db->QueryToArray("
			select fakultas.nm_fakultas, program_studi.nm_program_studi, no_ujian, nm_c_mhs, nim_mhs, nm_penerimaan, tgl_diterima, nm_jenjang, TGL_SIDIK_JARI_PENDIDIKAN
			from calon_mahasiswa_baru
			join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
			join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
			join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			join fingerprint_cmhs on fingerprint_cmhs.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
			$where and finger_data is not null
			order by fakultas.id_fakultas, program_studi.id_program_studi, no_ujian");	
		}elseif($status == 'penangguhan'){

			return $this->db->QueryToArray("
			select fakultas.id_fakultas, fakultas.nm_fakultas, program_studi.id_program_studi, program_studi.nm_program_studi, no_ujian, nm_c_mhs, nim_mhs, nm_penerimaan, tgl_diterima, nm_jenjang, tgl_verifikasi_pendidikan
			from calon_mahasiswa_baru
			join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
			join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
			join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs	
			join (
				select pembayaran_cmhs.id_c_mhs from pembayaran_cmhs
				where id_status_pembayaran = 3 
				group by pembayaran_cmhs.id_c_mhs
			) a on a.id_c_mhs = calon_mahasiswa_baru.id_c_mhs		
			$where 			
			order by fakultas.id_fakultas, program_studi.id_program_studi, no_ujian");	
		}elseif($status == 'bidik'){

			return $this->db->QueryToArray("
			select fakultas.id_fakultas, fakultas.nm_fakultas, program_studi.id_program_studi, program_studi.nm_program_studi, no_ujian, nm_c_mhs, nim_mhs, nm_penerimaan, tgl_diterima, nm_jenjang, tgl_verifikasi_pendidikan
			from calon_mahasiswa_baru
			join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
			join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
			join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs			
			$where and (status_bidik_misi != 0 and status_bidik_misi is not null)
			
			order by fakultas.id_fakultas, program_studi.id_program_studi, no_ujian");	
		}elseif($status == 'regmaba'){

			return $this->db->QueryToArray("
			select fakultas.id_fakultas, fakultas.nm_fakultas, program_studi.id_program_studi, program_studi.nm_program_studi, no_ujian, nm_c_mhs, nim_mhs, nm_penerimaan, tgl_diterima, nm_jenjang, tgl_regmaba
			from calon_mahasiswa_baru
			join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
			join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
			join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs			
			$where and tgl_regmaba is not null
			
			order by fakultas.id_fakultas, program_studi.id_program_studi, no_ujian");	
		}elseif($status == 'tidak_registrasi'){



			return $this->db->QueryToArray("
			select fakultas.id_fakultas, fakultas.nm_fakultas, program_studi.id_program_studi, program_studi.nm_program_studi, no_ujian, nm_c_mhs, nm_penerimaan, tgl_diterima, nm_jenjang, TELP, ALAMAT, status_bidik_misi
			from calon_mahasiswa_baru
			join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
			join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
			join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs			
			$where and calon_mahasiswa_baru.id_c_mhs not in (
				select pembayaran_cmhs.id_c_mhs 
				from pembayaran_cmhs
				join calon_mahasiswa_baru on calon_mahasiswa_baru.id_c_mhs = pembayaran_cmhs.id_c_mhs
				join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
				join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
				join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
				join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
				$where and (id_status_pembayaran != 2 or tgl_bayar is not null)
				group by pembayaran_cmhs.id_c_mhs
				union
				select calon_mahasiswa_baru.id_c_mhs 
				from calon_mahasiswa_baru
				join penerimaan on penerimaan.id_penerimaan = calon_mahasiswa_baru.id_penerimaan
				join program_studi on program_studi.id_program_studi = calon_mahasiswa_baru.id_program_studi
				join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
				join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
				join calon_mahasiswa_data on calon_mahasiswa_data.id_c_mhs = calon_mahasiswa_baru.id_c_mhs
				$where and status_bidik_misi != 0 and status_bidik_misi is not null and tgl_verifikasi_pendidikan is not null)
			order by fakultas.id_fakultas, program_studi.id_program_studi, no_ujian");	
		}
		
	
	}
	
	
	
	function mhs_kota($thn, $tampilan, $thn_akademik, $status_akademik, $fakultas, $jenjang, $program_studi, $negara, $provinsi, $kota) {
		
		$condition_akademik = $this->get_condition_akademik($fakultas, $jenjang, $program_studi);
		$condition_negara = $this->get_condition_negara($negara, $provinsi, $kota);
		
		if($thn != ''){
			$where = " and thn_angkatan_mhs = '$thn' ";
		}
		
		if($thn_akademik != ''){
			$where = " and thn_angkatan_mhs <= '$thn_akademik' ";
		}
		
		if($status_akademik == 1){
			$where .= "  and AKTIF_STATUS_PENGGUNA = 1 ";
		}elseif($status_akademik == ''){
		
		}else{
			$where .= "  and status_akademik_mhs = '{$status_akademik}' and mahasiswa.id_mhs in 
						(select id_mhs from admisi 
						where status_akd_mhs = '{$status_akademik}' and status_apv = 1 and to_char(tgl_sk,  'YYYY') = '$thn_akademik')";
		}
		
		if($tampilan == 'fak'){
        return $this->db->QueryToArray("
            select ID_FAKULTAS, NM_FAKULTAS, SUM(DECODE(id_jenjang, 5, JML, NULL)) D3, SUM(DECODE(id_jenjang, 1, JML, NULL)) S1 
			, SUM(DECODE(id_jenjang, 2, JML, NULL)) S2 , SUM(DECODE(id_jenjang, 3, JML, NULL)) S3 
			, SUM(DECODE(id_jenjang, 9, JML, NULL)) PROFESI , SUM(DECODE(id_jenjang, 10, JML, NULL)) SPESIALIS , SUM(JML) AS TOTAL
			from (
			select FAKULTAS.id_fakultas, nm_fakultas, JENJANG.id_jenjang, nm_jenjang, count(*) as jml
							from mahasiswa
							join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
							join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
							join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
							join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
							left join kota on kota.id_kota = ASAL_KOTA_MHS OR KOTA.ID_KOTA = MAHASISWA.ALAMAT_ASAL_MHS_KOTA
							left join provinsi on provinsi.id_provinsi = kota.id_provinsi
							left join negara on negara.id_negara = provinsi.id_negara
							where program_studi.status_aktif_prodi = 1 $where {$condition_akademik} {$condition_negara}
			group by FAKULTAS.id_fakultas, nm_fakultas, JENJANG.id_jenjang, nm_jenjang)
			GROUP BY ID_FAKULTAS, NM_FAKULTAS
			ORDER BY ID_FAKULTAS ASC
            ");
		}elseif($tampilan == 'negara'){
        return $this->db->QueryToArray("
            select nm_negara, SUM(DECODE(id_jenjang, 5, JML, NULL)) D3, SUM(DECODE(id_jenjang, 1, JML, NULL)) S1 
			, SUM(DECODE(id_jenjang, 2, JML, NULL)) S2 , SUM(DECODE(id_jenjang, 3, JML, NULL)) S3 
			, SUM(DECODE(id_jenjang, 9, JML, NULL)) PROFESI , SUM(DECODE(id_jenjang, 10, JML, NULL)) SPESIALIS , SUM(JML) AS TOTAL
			from (
			select FAKULTAS.id_fakultas, nm_fakultas, JENJANG.id_jenjang, nm_jenjang, nm_negara, count(*) as jml
							from mahasiswa
							join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
							join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
							join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
							join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
							left join kota on kota.id_kota = ASAL_KOTA_MHS OR KOTA.ID_KOTA = MAHASISWA.ALAMAT_ASAL_MHS_KOTA
							left join provinsi on provinsi.id_provinsi = kota.id_provinsi
							left join negara on negara.id_negara = provinsi.id_negara
							where program_studi.status_aktif_prodi = 1 $where {$condition_akademik} {$condition_negara}
			group by FAKULTAS.id_fakultas, nm_fakultas, JENJANG.id_jenjang, nm_jenjang, nm_negara)
			GROUP BY  nm_negara
			ORDER BY nm_negara ASC
            ");
		}else{
			return $this->db->QueryToArray("
            select ID_FAKULTAS, NM_FAKULTAS, ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, SUM(DECODE(id_jenjang, 5, JML, NULL)) D3, SUM(DECODE(id_jenjang, 1, JML, NULL)) S1 
			, SUM(DECODE(id_jenjang, 2, JML, NULL)) S2 , SUM(DECODE(id_jenjang, 3, JML, NULL)) S3 
			, SUM(DECODE(id_jenjang, 9, JML, NULL)) PROFESI , SUM(DECODE(id_jenjang, 10, JML, NULL)) SPESIALIS , SUM(JML) AS TOTAL
			from (
			select FAKULTAS.ID_FAKULTAS, NM_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, JENJANG.id_jenjang, nm_jenjang, count(*) as jml
							from mahasiswa
							join program_studi on program_studi.id_program_studi = mahasiswa.id_program_studi
							join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
							join status_pengguna on status_pengguna.id_status_pengguna = status_akademik_mhs
							join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
							left join kota on kota.id_kota = ASAL_KOTA_MHS OR KOTA.ID_KOTA = MAHASISWA.ALAMAT_ASAL_MHS_KOTA
							left join provinsi on provinsi.id_provinsi = kota.id_provinsi
							left join negara on negara.id_negara = provinsi.id_negara
							where program_studi.status_aktif_prodi = 1 $where {$condition_akademik} {$condition_negara}
			group by FAKULTAS.ID_FAKULTAS, NM_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, JENJANG.id_jenjang, nm_jenjang)
			GROUP BY ID_FAKULTAS, NM_FAKULTAS, ID_PROGRAM_STUDI, NM_PROGRAM_STUDI
			ORDER BY ID_FAKULTAS, ID_PROGRAM_STUDI ASC
            ");
		
		}				
    }



	function mhs_asing($thn, $tampilan, $thn_akademik, $status_akademik, $fakultas, $jenjang, $program_studi, $negara, $provinsi, $kota, $jenis_kerjasama) {
		
		$condition_akademik = $this->get_condition_akademik($fakultas, $jenjang, $program_studi);
		$condition_negara = $this->get_condition_negara($negara, $provinsi, $kota);
		
		if($thn != ''){
			$where = " and thn_akademik_semester = '$thn' ";
		}
		
		if($thn_akademik != ''){
			$where = " and thn_akademik_semester <= '$thn_akademik' ";
		}
		
		if($status_akademik == 1){
			$where .= "  and AKTIF_STATUS_PENGGUNA = 1 ";
		}elseif($status_akademik == ''){
		
		}else{
			$where .= "  status_pengguna.id_status_pengguna = {$status_akademik}";
		}

		if($jenis_kerjasama != ''){
			$where .= " and mahasiswa_asing.ID_JENIS_KERJASAMA = '$jenis_kerjasama' ";
		}
		
		if($tampilan == 'detail'){
			
        return $this->db->QueryToArray("

			select *
							from mahasiswa_asing
							left join pengguna on pengguna.id_pengguna = mahasiswa_asing.id_pengguna
							join program_studi on program_studi.id_program_studi = mahasiswa_asing.id_program_studi
							join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
							join status_pengguna on status_pengguna.id_status_pengguna = mahasiswa_asing.id_status_pengguna
							join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
							left join semester on semester.id_semester = mahasiswa_asing.id_semester_masuk
							left join negara on negara.id_negara = mahasiswa_asing.id_negara_asal
							left join jenis_kerjasama on jenis_kerjasama.id_jenis_kerjasama = mahasiswa_asing.id_jenis_kerjasama
							where program_studi.status_aktif_prodi in (0, 1) $where {$condition_akademik} {$condition_negara}
							order by fakultas.id_fakultas, jenjang.id_jenjang, nm_program_studi, nm_negara, nim_mhs_asing

            ");
		}
		elseif($tampilan == 'fak'){
        return $this->db->QueryToArray("
            select ID_FAKULTAS, NM_FAKULTAS, SUM(DECODE(id_jenjang, 5, JML, NULL)) D3, SUM(DECODE(id_jenjang, 1, JML, NULL)) S1 
			, SUM(DECODE(id_jenjang, 2, JML, NULL)) S2 , SUM(DECODE(id_jenjang, 3, JML, NULL)) S3 
			, SUM(DECODE(id_jenjang, 9, JML, NULL)) PROFESI , SUM(DECODE(id_jenjang, 10, JML, NULL)) SPESIALIS , SUM(JML) AS TOTAL
			from (
			select FAKULTAS.id_fakultas, nm_fakultas, JENJANG.id_jenjang, nm_jenjang, count(*) as jml
							from mahasiswa_asing
							join program_studi on program_studi.id_program_studi = mahasiswa_asing.id_program_studi
							join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
							join status_pengguna on status_pengguna.id_status_pengguna = mahasiswa_asing.id_status_pengguna
							join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
							left join semester on semester.id_semester = mahasiswa_asing.id_semester_masuk
							left join kota on kota.id_kota = id_kota_asal
							left join provinsi on provinsi.id_provinsi = kota.id_provinsi
							left join negara on negara.id_negara = provinsi.id_negara
							where program_studi.status_aktif_prodi in (0, 1) $where {$condition_akademik} {$condition_negara}
			group by FAKULTAS.id_fakultas, nm_fakultas, JENJANG.id_jenjang, nm_jenjang)
			GROUP BY ID_FAKULTAS, NM_FAKULTAS
			ORDER BY ID_FAKULTAS ASC
            ");
		}elseif($tampilan == 'negara'){
        return $this->db->QueryToArray("
            select ID_FAKULTAS, NM_FAKULTAS, nm_negara, SUM(DECODE(id_jenjang, 5, JML, NULL)) D3, SUM(DECODE(id_jenjang, 1, JML, NULL)) S1 
			, SUM(DECODE(id_jenjang, 2, JML, NULL)) S2 , SUM(DECODE(id_jenjang, 3, JML, NULL)) S3 
			, SUM(DECODE(id_jenjang, 9, JML, NULL)) PROFESI , SUM(DECODE(id_jenjang, 10, JML, NULL)) SPESIALIS , SUM(JML) AS TOTAL
			from (
			select FAKULTAS.id_fakultas, nm_fakultas, JENJANG.id_jenjang, nm_jenjang, nm_negara, count(*) as jml
							from mahasiswa_asing
							join program_studi on program_studi.id_program_studi = mahasiswa_asing.id_program_studi
							join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
							join status_pengguna on status_pengguna.id_status_pengguna = mahasiswa_asing.id_status_pengguna
							join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
							left join semester on semester.id_semester = mahasiswa_asing.id_semester_masuk
							left join negara on negara.id_negara = mahasiswa_asing.id_negara_asal
							where program_studi.status_aktif_prodi in (0, 1) $where {$condition_akademik} {$condition_negara}
			group by FAKULTAS.id_fakultas, nm_fakultas, JENJANG.id_jenjang, nm_jenjang, nm_negara)
			GROUP BY ID_FAKULTAS, NM_FAKULTAS, nm_negara
			ORDER BY nm_negara, ID_FAKULTAS ASC
            ");
		}else{
			return $this->db->QueryToArray("
            select ID_FAKULTAS, NM_FAKULTAS, ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, SUM(DECODE(id_jenjang, 5, JML, NULL)) D3, SUM(DECODE(id_jenjang, 1, JML, NULL)) S1 
			, SUM(DECODE(id_jenjang, 2, JML, NULL)) S2 , SUM(DECODE(id_jenjang, 3, JML, NULL)) S3 
			, SUM(DECODE(id_jenjang, 9, JML, NULL)) PROFESI , SUM(DECODE(id_jenjang, 10, JML, NULL)) SPESIALIS , SUM(JML) AS TOTAL
			from (
			select FAKULTAS.ID_FAKULTAS, NM_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, JENJANG.id_jenjang, nm_jenjang, count(*) as jml
							from mahasiswa_asing
							join program_studi on program_studi.id_program_studi = mahasiswa_asing.id_program_studi
							join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
							join status_pengguna on status_pengguna.id_status_pengguna = mahasiswa_asing.id_status_pengguna
							left join semester on semester.id_semester = mahasiswa_asing.id_semester_masuk
							join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
							left join kota on kota.id_kota = id_kota_asal
							left join provinsi on provinsi.id_provinsi = kota.id_provinsi
							left join negara on negara.id_negara = provinsi.id_negara
							where program_studi.status_aktif_prodi in (0, 1) $where {$condition_akademik} {$condition_negara}
			group by FAKULTAS.ID_FAKULTAS, NM_FAKULTAS, PROGRAM_STUDI.ID_PROGRAM_STUDI, NM_PROGRAM_STUDI, JENJANG.id_jenjang, nm_jenjang)
			GROUP BY ID_FAKULTAS, NM_FAKULTAS, ID_PROGRAM_STUDI, NM_PROGRAM_STUDI
			ORDER BY ID_FAKULTAS, ID_PROGRAM_STUDI ASC
            ");
		
		}				
    }
	
}
?>
