<?php
class mkwu {

	public $db;
	
	function __construct($db) {
		$this->db = $db;
	}
	
	
	function kelas() {
		return $this->db->QueryToArray("select id_nama_kelas,'KELAS-'||nama_kelas as nama_kelas from nama_kelas order by nama_kelas");
    }
	
	function hari() {
		return $this->db->QueryToArray("select id_jadwal_hari,nm_jadwal_hari from jadwal_hari");
    }
	
	function jam() {
		return $this->db->QueryToArray("select * from jadwal_jam where id_fakultas='5' order by jam_mulai||menit_mulai, jam_selesai||menit_selesai");
    }
	
	function ruangan($kdfak) {
		
		if($kdfak == ''){
			$where = "where gedung.id_fakultas is null";	
		}else{
			$where = "where gedung.id_fakultas='".$kdfak."'";
		}
		return $this->db->QueryToArray("select id_ruangan,nm_ruangan||' ('||kapasitas_ruangan||')' as ruang from ruangan
			left join gedung on gedung.id_gedung=ruangan.id_gedung
			left join gedung_kuliah on gedung.id_gedung=gedung_kuliah.id_gedung
			$where order by nm_ruangan");
    }
	
	function fakultas() {
		return $this->db->QueryToArray("SELECT * FROM FAKULTAS ORDER BY ID_FAKULTAS ASC");
    }
	
	function jenjang() {
		return $this->db->QueryToArray("SELECT * FROM JENJANG ORDER BY ID_JENJANG ASC");
    }
	
	function program_studi($id_fakultas, $id_jenjang) {
		return $this->db->QueryToArray("SELECT * FROM PROGRAM_STUDI WHERE STATUS_AKTIF_PRODI = 1 
		AND ID_FAKULTAS = '$id_fakultas' AND ID_JENJANG = '$id_jenjang'
		ORDER BY ID_FAKULTAS, ID_JENJANG, NM_PROGRAM_STUDI ASC");
    }
	
	function semester() {
		return $this->db->QueryToArray("SELECT * FROM SEMESTER WHERE NM_SEMESTER IN ('Genap', 'Ganjil') ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER ASC");
    }
	
	function semester_aktif() {
		$this->db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True'");
		return $this->db->FetchAssoc();
    }
	
	function prodi_s1_d3() {
		return $this->db->QueryToArray("SELECT * FROM PROGRAM_STUDI WHERE ID_JENJANG IN (1, 5) AND STATUS_AKTIF_PRODI = 1");
    }
	
	function kurikulum_mkwu() {
		return $this->db->QueryToArray("SELECT * FROM KURIKULUM WHERE ID_PROGRAM_STUDI = 228");
    }
	
	function mata_kuliah_mkwu() {
		return $this->db->QueryToArray("SELECT * FROM MATA_KULIAH WHERE ID_PROGRAM_STUDI = 228 order by nm_mata_kuliah");
    }
	
	
	function kurikulum_mata_kuliah_mkwu($id_mata_kuliah) {
		$this->db->Query("SELECT * FROM KURIKULUM_MK WHERE ID_MATA_KULIAH = $id_mata_kuliah and ID_PROGRAM_STUDI = 228");
		return $this->db->FetchAssoc();
    }
	
	
	function usulan_mkwu($id_prodi, $id_semester, $id_kurikulum) {
		
		return $this->db->QueryToArray("
			select id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_tatap_muka,kredit_praktikum,
		kmk.kredit_semester, nm_status_mk,nm_kelompok_mk, tahun_kurikulum,kmk.keterangan_kur_mk,
		tingkat_semester
		from kurikulum_mk kmk
		left join mata_kuliah on kmk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
		left join status_mk on kmk.id_status_mk=status_mk.id_status_mk
		left join kelompok_mk on kmk.id_kelompok_mk=kelompok_mk.id_kelompok_mk
		left join kurikulum_prodi on kmk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
		where kmk.id_program_studi=$id_prodi and id_kurikulum_mk not in 
			(select id_kurikulum_mk from kelas_mk where  id_semester=$id_semester)
		and kurikulum_prodi.id_kurikulum=$id_kurikulum
		");
    }
	
	
	function cek_jadwal_kelas($id_kelas_mk) {
		 $this->db->Query("SELECT * FROM JADWAL_KELAS WHERE ID_KELAS_MK = '$id_kelas_mk'");
		 return $this->db->FetchAssoc();
    }
	
	
	function usulan_mkwu_add($id_prodi, $id_semester, $id_kurikulum_mk) {	
		return $this->db->Query("
			INSERT INTO KELAS_MK (ID_KURIKULUM_MK, ID_SEMESTER, ID_PROGRAM_STUDI)
			VALUES ($id_kurikulum_mk, $id_semester, $id_prodi)
		");
		
    }
	
	
	function usulan_mkwu_update($id_prodi, $id_semester, $kelas_mk, $kap_kelas, $ren_kul, $id_kelas_mk, $hari, $jam, $ruangan) {	
	
		 $this->db->Query("
			update kelas_mk set kapasitas_kelas_mk='$kap_kelas', jumlah_pertemuan_kelas_mk='$ren_kul', no_kelas_mk='$kelas_mk', id_program_studi = '$id_prodi'
			where id_kelas_mk='$id_kelas_mk'
		");
		
		if(!$this->cek_jadwal_kelas($id_kelas_mk)){
			$this->db->Query("
			INSERT INTO JADWAL_KELAS (ID_KELAS_MK)
			VALUES ($id_kelas_mk)
			");
		}
		
		return $this->db->Query("
			update jadwal_kelas set id_jadwal_jam=$jam, id_jadwal_hari=$hari,id_ruangan='$ruangan' where id_kelas_mk=$id_kelas_mk
			");
		
    }
	
	
	function usulan_mkwu_addkls($id_prodi, $id_semester, $kelas_mk, $kap_kelas, $ren_kul, $id_kelas_mk, $hari, $jam, $ruangan, $id_kur_mk) {	
		
		
		$this->db->Query("select id_kelas_mk from kelas_mk 
							where id_kurikulum_mk=$id_kur_mk and id_semester=$id_semester 
							and id_program_studi=$id_prodi and no_kelas_mk=$kelas_mk");
		$id_kelas_mk = $this->db->FetchAssoc();

		if($id_kelas_mk['ID_KELAS_MK'] == ''){
		 $this->db->Query("
			insert into kelas_mk (id_kurikulum_mk,id_semester,id_program_studi,no_kelas_mk,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk)
			values ('$id_kur_mk','$id_semester','$id_prodi','$kelas_mk','$kap_kelas','$ren_kul')
		");
		}

		$this->db->Query("select id_kelas_mk from kelas_mk 
							where id_kurikulum_mk=$id_kur_mk and id_semester=$id_semester 
							and id_program_studi=$id_prodi and no_kelas_mk=$kelas_mk");
		$id_kelas_mk = $this->db->FetchAssoc();

		return $this->db->Query("
			insert into jadwal_kelas (id_kelas_mk,id_jadwal_hari,id_jadwal_jam,id_ruangan)
			values ('$id_kelas_mk[ID_KELAS_MK]', '$hari', '$jam', '$ruangan')
			");
		
    }
	
	
	function usulan_mkwu_del($id_kelas_mk) {	
		$this->db->Query("
			delete from jadwal_kelas where id_kelas_mk=$id_kelas_mk
			");
		
		$this->db->Query("
			delete from pengampu_mk where id_kelas_mk=$id_kelas_mk
			");
		
		return $this->db->Query("
			delete from kelas_mk where id_kelas_mk=$id_kelas_mk
			");
		
    }
	
	
	function usulan_mkwu_rincian($id_prodi, $id_semester) {
		
		return $this->db->QueryToArray("
			select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
			kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt, kelas_mk.id_semester,
			jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
			jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
			ruangan.id_ruangan,nm_ruangan,case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
			no_kelas_mk,nama_kelas.nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,
			tahun_kurikulum, (NM_JENJANG || ' - ' || NM_PROGRAM_STUDI) AS PRODI, program_studi.id_fakultas, program_studi.id_jenjang, 
			program_studi.id_program_studi
			from kelas_mk
			join program_studi on program_studi.id_program_studi = kelas_mk.id_program_studi
			join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
			left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
			left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
			left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
			where kurikulum_mk.id_program_studi=228 and kelas_mk.id_semester=$id_semester
		");
    }
	
	
	
	function usulan_mkwu_updateview($id_prodi, $id_semester, $id_klsmk) {
		
		return $this->db->QueryToArray("
			select distinct kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.id_kurikulum_mk,
			kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,
			jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,kelas_mk.id_semester,
			ruangan.id_ruangan,nm_ruangan,no_kelas_mk,nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,tahun_kurikulum, 
			gedung.id_fakultas as id_fakultas_gedung,
			(NM_JENJANG || ' - ' || NM_PROGRAM_STUDI) AS PRODI, program_studi.id_fakultas, program_studi.id_jenjang, 
			program_studi.id_program_studi
			from kelas_mk
			join program_studi on program_studi.id_program_studi = kelas_mk.id_program_studi
			join jenjang on jenjang.id_jenjang = program_studi.id_jenjang
			join fakultas on fakultas.id_fakultas = program_studi.id_fakultas
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join kurikulum_prodi on kurikulum_mk.id_kurikulum_prodi=kurikulum_prodi.id_kurikulum_prodi
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
			left join gedung on gedung.id_gedung=ruangan.id_gedung
			left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
			left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
			where kurikulum_mk.id_program_studi=228 and kelas_mk.id_kelas_mk=$id_klsmk and rownum=1
		");
    }
	
	
	
	function mkwu_penawaran($id_prodi, $id_semester) {		
		return $this->db->QueryToArray("
			select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,
					kurikulum_mk.kredit_semester,nama_kelas,program_studi.id_program_studi,kelas_mk.status,
					nm_jenjang||'-'||nm_program_studi as prodiasal 
			from kelas_mk
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
			left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
			where id_semester=$id_semester and mata_kuliah.id_mata_kuliah in 
					(
					select kurikulum_mk.id_mata_kuliah from kurikulum_mk
					where kurikulum_mk.id_program_studi=$id_prodi
					)
			and id_kelas_mk not in (select id_kelas_mk from krs_prodi where id_semester=$id_semester)
		");
	}
	
	
	function mkwu_penawaran_rincian($id_prodi, $id_semester) {		
		return $this->db->QueryToArray("
			select distinct id_krs_prodi,kelas_mk.id_kelas_mk,kd_mata_kuliah,nm_mata_kuliah,
			kurikulum_mk.kredit_semester,tahun_ajaran||'-'||nm_semester as smt,
			kelas_mk.id_semester, jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,
			jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
			jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
			ruangan.id_ruangan,nm_ruangan,
			case when pengampu_mk.id_dosen is not null then 'sdh terisi' else 'blm terisi' end as status_pjmk,
			nama_kelas,kapasitas_kelas_mk,jumlah_pertemuan_kelas_mk,nm_jenjang||'-'||nm_program_studi as prodiasal
			from  krs_prodi
			left join kelas_mk on krs_prodi.id_kelas_mk=kelas_mk.id_kelas_mk
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
			left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
			left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
			left join semester on kelas_mk.id_semester=semester.id_semester
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
			left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
			left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
			left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
			left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
			left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
			where krs_prodi.id_semester=$id_semester and kurikulum_mk.id_program_studi=$id_prodi
		");
	}
	
	function mkwu_penawaran_add($id_prodi, $id_semester, $id_kelas_mk){	
		return $this->db->Query("
				insert into krs_prodi (id_kelas_mk, id_semester, id_program_studi)
				values ('$id_kelas_mk', '$id_semester', '$id_prodi')
			");
	}
	
	
	function mkwu_penawaran_del($id_krs_prodi){	
		return $this->db->Query("
				delete from krs_prodi where id_krs_prodi='$id_krs_prodi'
			");
	}
	
	
	function mkwu_pjma($id_semester){	
		return $this->db->QueryToArray("
				select id_kurikulum_mk, kd_mata_kuliah, nm_mata_kuliah, kredit_semester, id_status, tim, ada, id_mata_kuliah
				from (
				select kelas_mk.id_kurikulum_mk,kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, mata_kuliah.id_mata_kuliah,  
				upper(mata_kuliah.nm_mata_kuliah) as nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas,
				case when wm_concat(pengguna.nm_pengguna) is not null then '1' else '0' end as id_status,
				trim(wm_concat('<li>'||(case when pengampu_mk.pjmk_pengampu_mk=1 then '<b><font color=\"blue\">PJMK : '||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang||'</font></b>'
				else 'TIM : ' ||gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang end))) as tim, 
				case when sum(pengampu_mk.pjmk_pengampu_mk)=1 then 'ada' else 'kosong' end as ada, nm_ruangan, nm_jadwal_hari,
				jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
				jadwal_kelas.id_jadwal_hari, jadwal_kelas.id_jadwal_jam, jadwal_kelas.id_ruangan
				from kelas_mk 
				left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
				left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
				left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
				left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
				left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
				left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
				left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
				left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
				left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
				left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
				where kurikulum_mk.id_program_studi=228 and kelas_mk.id_semester=$id_semester
				group by kelas_mk.id_kurikulum_mk,kelas_mk.id_kelas_mk, mata_kuliah.kd_mata_kuliah, 
				mata_kuliah.nm_mata_kuliah, kurikulum_mk.kredit_semester, nama_kelas.nama_kelas, mata_kuliah.id_mata_kuliah, nm_ruangan,nm_jadwal_hari,
				jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai, 
				jadwal_kelas.id_jadwal_hari, jadwal_kelas.id_jadwal_jam, jadwal_kelas.id_ruangan
				) 
				group by id_kurikulum_mk, kd_mata_kuliah, nm_mata_kuliah, kredit_semester, id_status, tim, ada, id_mata_kuliah
				order by ada 
			");
	}
	
	
	function mkwu_pjma_addview($id_kur, $id_semester){	
	
		$this->db->Query("
				select distinct kd_mata_kuliah,upper(nm_mata_kuliah) as nm_mata_kuliah,kurikulum_mk.kredit_semester
				from kelas_mk 
				left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
				left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
				left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
				left join semester on kelas_mk.id_semester=semester.id_semester 
				left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk 
				left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
				left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
				where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester
			");
			
		return $this->db->FetchAssoc();
	}
	
	
	function mkwu_pjma_pengampu($id_kur, $id_semester){	
	
		return $this->db->QueryToArray("
				select distinct pengampu_mk.id_dosen,pjmk_pengampu_mk,nip_dosen, kelas_mk.id_kurikulum_mk, 
				trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna 
				from pengampu_mk
				left join kelas_mk on kelas_mk.id_kelas_mk = pengampu_mk.id_kelas_mk
				left join dosen on pengampu_mk.id_dosen=dosen.id_dosen 
				left join pengguna on dosen.id_pengguna=pengguna.id_pengguna 
				where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester
				order by pjmk_pengampu_mk desc 
			");		
	}
	
	
	function mkwu_pjma_pengampu_cek($id_kur, $id_semester, $pjmk_pengampu_mk, $id_dosen){	
		
		if($pjmk_pengampu_mk == '1'){
			$where = "where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester and pjmk_pengampu_mk = '$pjmk_pengampu_mk'";
		}else{
			$where = "where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester and pjmk_pengampu_mk = '$pjmk_pengampu_mk' and id_dosen = '$id_dosen'";
		}
		
		$this->db->Query("
				select *
				from pengampu_mk
				join kelas_mk on kelas_mk.id_kelas_mk = pengampu_mk.id_kelas_mk
				$where
			");			
			
		return $this->db->FetchAssoc();
	}
	
	
	function mkwu_pjma_pengampu_add($id_kur, $id_semester, $pjmk_pengampu_mk, $id_dosen){	
		
		$kelas_mk = $this->db->QueryToArray("select kelas_mk.id_kelas_mk from kelas_mk 
							where kelas_mk.id_kurikulum_mk = $id_kur and kelas_mk.id_semester = $id_semester");
		
		foreach($kelas_mk as $data){
		 $this->db->Query("
				insert into pengampu_mk (id_kelas_mk, id_dosen, pjmk_pengampu_mk)
				values ('$data[ID_KELAS_MK]', '$id_dosen', '$pjmk_pengampu_mk')
			");	
		}
		
	}
	
	
	function mkwu_pjma_pengampu_add_copy($id_kelas_asal, $id_kelas_tujuan){	

		return $this->db->Query("
				insert into pengampu_mk(id_kelas_mk,id_dosen,pjmk_pengampu_mk) 
				select '".$id_kelas_tujuan."',id_dosen,pjmk_pengampu_mk from pengampu_mk where id_kelas_mk=$id_kelas_asal
			");			
		
	}
	
	
	function mkwu_pjma_pengampu_update($id_dosen, $id_kur, $id_semester, $id_dosen_ganti){	

		return $this->db->Query("
				update pengampu_mk set id_dosen=$id_dosen where id_dosen = $id_dosen_ganti 
					and id_kelas_mk in (select kelas_mk.id_kelas_mk from kelas_mk 
										where kelas_mk.id_kurikulum_mk = $id_kur and kelas_mk.id_semester = $id_semester)
			");			
		
	}
	
	
	function mkwu_pjma_pengampu_delete($id_dosen, $id_kur, $id_semester){	
			
		if($id_dosen != ''){
			$this->db->Query("
				delete from pengampu_mk where id_dosen = $id_dosen and id_kelas_mk in (select kelas_mk.id_kelas_mk from kelas_mk
										where kelas_mk.id_kurikulum_mk = $id_kur and kelas_mk.id_semester = $id_semester)
			");			
		}else{
			$this->db->Query("
				delete from pengampu_mk where id_kelas_mk in (select kelas_mk.id_kelas_mk from kelas_mk 
										where kelas_mk.id_kurikulum_mk = $id_kur and kelas_mk.id_semester = $id_semester)
			");	
		}

	}
	
	
	function mkwu_pjma_searchdosen($namadosen){	
	
	
		return $this->db->QueryToArray("
				select id_dosen,nip_dosen,trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang) as nm_pengguna, foto_pengguna , upper(nm_program_studi) as nm_program_studi
		from dosen 
		left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
		left join program_studi on dosen.id_program_studi=program_studi.id_program_studi
		where upper(nm_pengguna) like upper('%$namadosen%') and dosen.id_status_pengguna=22
		order by trim(gelar_depan||' '||upper(nm_pengguna)||', '||gelar_belakang)
			");
			
	}
	
	
	function mkwu_pjma_copy($id_kurmk, $id_klsmk, $id_smt){	
	
		return $this->db->QueryToArray("
				select kelas_mk.id_kelas_mk,kd_mata_kuliah||'-'||nm_mata_kuliah||'-'||nama_kelas as nama from kelas_mk
						left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
						left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
						left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
						where kelas_mk.id_kurikulum_mk=$id_kurmk and kelas_mk.id_kelas_mk!=$id_klsmk and kelas_mk.id_semester=$id_smt
			");
			
	}
	
	
	
	
	function pecah_kelas_view($id_semester, $id_fakultas, $mata_kuliah, $id_jenjang){	
	
		if($mata_kuliah == ''){
			$where .= "and (upper(NM_MATA_KULIAH) LIKE ('%AGAMA ISLAM%') 
					  or upper(NM_MATA_KULIAH) LIKE ('%PROTESTAN%')
					  or upper(NM_MATA_KULIAH) LIKE ('%KATOLIK%')
					  or upper(NM_MATA_KULIAH) LIKE ('%HINDU%')
					  or upper(NM_MATA_KULIAH) LIKE ('%BUDHA%')
					  or upper(NM_MATA_KULIAH) LIKE ('%KONGHUCU%')
					  or upper(NM_MATA_KULIAH) LIKE ('%PPKN%')
					  or upper(NM_MATA_KULIAH) LIKE ('%FILSAFAT ILMU%')
					  or upper(NM_MATA_KULIAH) LIKE ('%ILMU ALAMIAH DASAR%')
					  or upper(NM_MATA_KULIAH) LIKE ('%ILMU SOSIAL BUDAYA DASAR%')
					  or upper(NM_MATA_KULIAH) LIKE ('%BAHASA INGGRIS%')
					  or upper(NM_MATA_KULIAH) LIKE ('%BAHASA INDONESIA%')) ";
		}else{
			$where .= " and upper(NM_MATA_KULIAH) LIKE ('%$mata_kuliah%')";
		}
				
		
		if($id_fakultas != ''){
			$where .= " and program_studi.id_fakultas = $id_fakultas"; 		
		}
	
		if($id_jenjang != ''){
			$where .= " and program_studi.id_jenjang = $id_jenjang"; 		
		}
		
		return $this->db->QueryToArray("
				select s1.id_kelas_mk,s1.id_kurikulum_mk,s1.kd_mata_kuliah,s1.nm_mata_kuliah,
				s1.kredit_semester, s1.nama_kelas,s1.kapasitas_kelas_mk,s1.terisi_kelas_mk,
				s1.kls_terisi,s1.status,nm_jenjang||'-'||s1.nm_program_studi as prodimk, s1.id_program_studi, s1.nm_ruangan
				from
				(select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,
				kurikulum_mk.kredit_semester, nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,
				sum(case when status_pengambilan_mk>=1 then 1 else 0 end) as kls_terisi,kelas_mk.status,
				kelas_mk.id_program_studi,nm_singkat_prodi,program_studi.id_jenjang,nm_program_studi,nm_ruangan
				from kelas_mk 
				left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester 
				left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
				left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
				left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
				left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi 
				left join mahasiswa on mahasiswa.id_mhs = pengambilan_mk.id_mhs
				left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
				left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
				where kelas_mk.id_semester='".$id_semester."' and mahasiswa.thn_angkatan_mhs >= '2013'
				$where
				group by kelas_mk.id_kelas_mk, kelas_mk.id_kurikulum_mk, kd_mata_kuliah, nm_mata_kuliah, 
				kurikulum_mk.kredit_semester, nama_kelas, kapasitas_kelas_mk, terisi_kelas_mk, 
				kelas_mk.status, kelas_mk.id_program_studi, nm_singkat_prodi, program_studi.id_jenjang,nm_program_studi,nm_ruangan)s1
				left join jenjang on s1.id_jenjang=jenjang.id_jenjang
				where s1.kls_terisi != '0'
			");
			
	}
	
		
	
	function pindah_kelas_view($id_semester, $id_kelas_mk){				
	
		return $this->db->QueryToArray("
				select distinct pengambilan_mk.id_mhs,id_pengambilan_mk,nim_mhs,nm_pengguna,nm_jenjang||'-'||nm_program_studi as nm_program_studi
				from pengambilan_mk
				left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
				left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
				left join program_studi on mahasiswa.id_program_studi=program_studi.id_program_studi
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang
				where pengambilan_mk.id_semester='".$id_semester."' and id_kelas_mk=$id_kelas_mk and status_apv_pengambilan_mk=1
				order by nim_mhs
			");
			
	}
	
	
	
	function asal_kelas_view($id_semester, $id_kelas_mk){				
	
		return $this->db->QueryToArray("
				select id_kelas_mk,kd_mata_kuliah||'/'||nm_mata_kuliah||'/ KLS-'||nama_kelas||'/'||nm_jenjang||'-'||nm_program_studi as kelas_tujuan 
				from kelas_mk
				left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
				left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
				left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
				left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang 
				where kelas_mk.id_semester='".$id_semester."'
				 and id_kelas_mk=$id_kelas_mk
			");
			
	}
	
	
	function tujuan_kelas_view($id_semester, $id_kelas_mk, $mata_kuliah, $id_prodi){				
	
		return $this->db->QueryToArray("
				select kelas_mk.id_kelas_mk,kd_mata_kuliah||'/'||nm_mata_kuliah||'/ Ruang-'||nm_ruangan||'/ KLS-'||nama_kelas||'/'||nm_jenjang||'-'||nm_program_studi as kelas_tujuan 
				from kelas_mk
				left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
				left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
				left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
				left join program_studi on kelas_mk.id_program_studi=program_studi.id_program_studi
				left join jenjang on program_studi.id_jenjang=jenjang.id_jenjang 
				left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
				left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
				where kelas_mk.id_semester='".$id_semester."' and kelas_mk.id_program_studi = '228'
				 and kurikulum_mk.id_program_studi=228 and upper(NM_MATA_KULIAH) LIKE ('%$mata_kuliah%')
			");
			
	}
	
	
	
	function presensi_view($id_semester, $mata_kuliah, $hari){				
	
		if($mata_kuliah != ''){
			$where = " and kurikulum_mk.id_mata_kuliah=$mata_kuliah ";	
		}

		return $this->db->QueryToArray("
			select id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,
			nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,sum(kls_terisi) as kls_terisi,
			status,id_jadwal_hari,nm_jadwal_hari,id_jadwal_jam, nm_jadwal_jam, jam, tm ,id_mata_kuliah, nm_ruangan,id_ruangan,id_nama_kelas
			from (
				select kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
				nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,(select count(status_apv_pengambilan_mk) from pengambilan_mk
				where kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and status_apv_pengambilan_mk=1) as kls_terisi,
				kelas_mk.status,mata_kuliah.id_mata_kuliah,
				jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
				jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam,
				(select count(id_presensi_kelas) from presensi_kelas where kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk) as tm, 
				nm_ruangan,ruangan.id_ruangan,nama_kelas.id_nama_kelas
				from kelas_mk
				left join pengambilan_mk on kelas_mk.id_kelas_mk=pengambilan_mk.id_kelas_mk and kelas_mk.id_semester=pengambilan_mk.id_semester
				left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
				left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
				left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
				left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
				left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
				left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
				left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
				left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
				where kurikulum_mk.id_program_studi = 228 and kelas_mk.id_semester=$id_semester and jadwal_hari.id_jadwal_hari=$hari
				$where
				group by kelas_mk.id_kelas_mk,kelas_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kurikulum_mk.kredit_semester,
				nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,kelas_mk.status,
				jadwal_kelas.id_jadwal_hari,nm_jadwal_hari,jadwal_kelas.id_jadwal_jam, nm_jadwal_jam,
				jam_mulai,menit_mulai,jam_selesai,menit_selesai,mata_kuliah.id_mata_kuliah, nm_ruangan,ruangan.id_ruangan,nama_kelas.id_nama_kelas
				)
			GROUP BY id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,kredit_semester,
			nama_kelas,kapasitas_kelas_mk,terisi_kelas_mk,id_mata_kuliah,
			status,id_jadwal_hari,nm_jadwal_hari,id_jadwal_jam, nm_jadwal_jam, jam, tm , nm_ruangan,id_ruangan,id_nama_kelas
			");
			
	}
	
	
	
	function mkwu_presensi_add_materi($id_kur, $isi_materi_mk, $tgl_materi_mk, $id_semester, $kelas){
		$kelas_mk = $this->db->QueryToArray("select id_kelas_mk from kelas_mk where id_kurikulum_mk = $id_kur and id_semester = $id_semester and no_kelas_mk = $kelas");
		
		foreach($kelas_mk as $data){
			$this->db->Query("insert into materi_mk (id_kelas_mk,isi_materi_mk,tgl_materi_mk)
								values ($data[ID_KELAS_MK],'$isi_materi_mk',to_date('$tgl_materi_mk', 'DD-MM-YYYY'))");
		}
	}
	
	
	function mkwu_presensi_update_materi($id_kur, $isi_materi_mk, $tgl_materi_mk, $id_semester, $kelas){
	
			$this->db->Query("update materi_mk set isi_materi_mk = '$isi_materi_mk', tgl_materi_mk = to_date('$tgl_materi_mk', 'DD-MM-YYYY')
							  where id_kelas_mk in (
							  	select id_kelas_mk from kelas_mk where id_kurikulum_mk = $id_kur and id_semester = $id_semester and no_kelas_mk = $kelas
							  )");
		
	}
	
	function mkwu_presensi_add_presensi_kelas($id_kur, $isi_materi_mk, $tgl_materi_mk, $id_semester, $jam_mulai, $jam_selesai, $kelas){
		
		$materi_mk = $this->db->QueryToArray("select * from materi_mk 
							join kelas_mk on kelas_mk.id_kelas_mk = materi_mk.id_kelas_mk
							where kelas_mk.id_kurikulum_mk = $id_kur and kelas_mk.id_semester = $id_semester  and no_kelas_mk = $kelas
							and isi_materi_mk='$isi_materi_mk' and tgl_materi_mk = to_date('$tgl_materi_mk', 'DD-MM-YYYY')");
		
		foreach($materi_mk as $data){
			$this->db->Query("insert into presensi_kelas (id_kelas_mk,waktu_mulai,waktu_selesai,tgl_presensi_kelas,id_materi_mk)
								values ($data[ID_KELAS_MK],'$jam_mulai','$jam_selesai',to_date('$tgl_materi_mk', 'DD-MM-YYYY'),'$data[ID_MATERI_MK]')");
		}
		
	}
	
	
	function mkwu_presensi_update_presensi_kelas($id_kur, $isi_materi_mk, $tgl_materi_mk, $id_semester, $jam_mulai, $jam_selesai, $kelas){
		
		$materi_mk = $this->db->QueryToArray("select * from materi_mk 
							join kelas_mk on kelas_mk.id_kelas_mk = materi_mk.id_kelas_mk
							where kelas_mk.id_kurikulum_mk = $id_kur and kelas_mk.id_semester = $id_semester 
							and isi_materi_mk='$isi_materi_mk' and tgl_materi_mk = to_date('$tgl_materi_mk', 'DD-MM-YYYY')");
		
		foreach($materi_mk as $data){
			$this->db->Query("update presensi_kelas set waktu_mulai = '$jam_mulai', waktu_selesai = '$jam_selesai', tgl_presensi_kelas = to_date('$tgl_materi_mk', 'DD-MM-YYYY')
							  where id_kelas_mk = $data[ID_KELAS_MK] and id_materi_mk = $data[ID_MATERI_MK]");
		}
		
	}
	
	
	function mkwu_presensi_pengampu($id_kur, $id_semester, $hari, $jam, $ruang, $kelas){

		return $this->db->QueryToArray("
			select id_dosen, nm_pengguna, tanda,pjmk_pengampu_mk
			from (			
			select pengampu_mk.id_dosen,gelar_depan||' '||nm_pengguna||','||gelar_belakang as NM_PENGGUNA, pjmk_pengampu_mk,
						 case when pengampu_mk.id_dosen in 
						 		(select presensi_mkdos.id_dosen 
								from presensi_mkdos
								left join presensi_kelas on presensi_kelas.ID_PRESENSI_KELAS = presensi_mkdos.ID_PRESENSI_KELAS  
								join kelas_mk on kelas_mk.id_kelas_mk = presensi_kelas.id_kelas_mk 
								left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
								left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
						 		where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester and jadwal_kelas.id_jadwal_hari=$hari
								and jadwal_kelas.id_jadwal_jam = $jam and jadwal_kelas.id_ruangan = $ruang and nama_kelas.id_nama_kelas = $kelas
								) then 'checked' else 'unchecked' end as tanda
						from kelas_mk
						left join pengampu_mk on kelas_mk.id_kelas_mk=pengampu_mk.id_kelas_mk
						left join dosen on pengampu_mk.id_dosen=dosen.id_dosen
						left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
						left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
						left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
						where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester and jadwal_kelas.id_jadwal_hari=$hari
						and jadwal_kelas.id_jadwal_jam = $jam and jadwal_kelas.id_ruangan = $ruang and nama_kelas.id_nama_kelas = $kelas
						 
			)
			group by id_dosen, nm_pengguna, tanda,pjmk_pengampu_mk
			order by pjmk_pengampu_mk desc
			");
		
	}
	
	function mkwu_presensi_view($id_kur, $id_semester, $hari, $jam, $ruang, $kelas){

		return $this->db->QueryToArray("
			select tgl_presensi_kelas,waktu_mulai||'-'||waktu_selesai as jam,isi_materi_mk, 
			wm_concat('<li>'||nmdos1) as nmdos,hadir,absen ,waktu_mulai, waktu_selesai
			from (
			select to_char(tgl_presensi_kelas, 'DD-MM-YYYY') as tgl_presensi_kelas,waktu_mulai,waktu_selesai,isi_materi_mk, 
			' '||gelar_depan||' '||nm_pengguna||','||gelar_belakang as nmdos1,
			sum(case when presensi_mkmhs.kehadiran=1 then 1 else 0 end ) as hadir,
			sum(case when presensi_mkmhs.kehadiran=0 then 1 else 0 end ) as absen
			from presensi_kelas
			left join materi_mk on presensi_kelas.id_materi_mk=materi_mk.id_materi_mk
			left join presensi_mkdos on presensi_kelas.id_presensi_kelas=presensi_mkdos.id_presensi_kelas
			left join dosen on presensi_mkdos.id_dosen=dosen.id_dosen
			left join pengguna on dosen.id_pengguna=pengguna.id_pengguna
			left join presensi_mkmhs on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas
			left join kelas_mk on kelas_mk.id_kelas_mk = presensi_kelas.id_kelas_mk
			left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
			left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
			where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester and jadwal_kelas.id_jadwal_hari=$hari
			and jadwal_kelas.id_jadwal_jam = $jam and jadwal_kelas.id_ruangan = $ruang and nama_kelas.id_nama_kelas = $kelas
			group by tgl_presensi_kelas, waktu_mulai, waktu_selesai, isi_materi_mk,' '||gelar_depan||' '||nm_pengguna||','||gelar_belakang)
			group by tgl_presensi_kelas,isi_materi_mk,hadir,absen,waktu_mulai, waktu_selesai
			");
		
	}
	
	
	
	
	function mkwu_pjma_ma($id_kur, $id_semester, $hari, $jam, $ruang, $kelas){	
	
		$this->db->Query("
				select distinct kd_mata_kuliah,upper(nm_mata_kuliah) as nm_mata_kuliah,kurikulum_mk.kredit_semester,
				jam_mulai||':'||menit_mulai||'-'||jam_selesai||':'||menit_selesai as jam, nm_ruangan, nama_kelas
				from kelas_mk 
				left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
				left join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah 
				left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas 
				left join semester on kelas_mk.id_semester=semester.id_semester 
				left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
				left join ruangan on jadwal_kelas.id_ruangan=ruangan.id_ruangan
				left join jadwal_hari on jadwal_kelas.id_jadwal_hari=jadwal_hari.id_jadwal_hari
				left join jadwal_jam on jadwal_kelas.id_jadwal_jam=jadwal_jam.id_jadwal_jam
				where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester
				and jadwal_kelas.id_jadwal_hari = $hari and jadwal_kelas.id_jadwal_jam = $jam and jadwal_kelas.id_ruangan = $ruang and nama_kelas.id_nama_kelas = $kelas
			");
			
		return $this->db->FetchAssoc();
	}
	
	
	function mkwu_presensi_msh_view($id_kur, $id_semester, $hari, $jam, $ruang, $kelas, $status, $isi_materi_mk, $tgl_materi_mk, $jam_mulai, $jam_selesai){	
	
	
		if($status == 1){
		return $this->db->QueryToArray("
				select pengambilan_mk.id_mhs,nim_mhs,nm_pengguna,
						case when status_pengambilan_mk=2 then 'KPRS' else 'KRS' end as stkrs,'checked' as tanda 
						from pengambilan_mk
						left join mahasiswa on pengambilan_mk.id_mhs=mahasiswa.id_mhs
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						left join kelas_mk on kelas_mk.id_kelas_mk = pengambilan_mk.id_kelas_mk
						left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
						left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
						where status_apv_pengambilan_mk=1 and kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester
						and jadwal_kelas.id_jadwal_hari = $hari and jadwal_kelas.id_jadwal_jam = $jam and jadwal_kelas.id_ruangan = $ruang and nama_kelas.id_nama_kelas = $kelas
			");
			
		}else{
			
		return $this->db->QueryToArray("
				select presensi_mkmhs.id_mhs,nim_mhs,nm_pengguna,case when status_pengambilan_mk=2 then 'KPRS' else 'KRS' end as stkrs,
						case when kehadiran=1 then 'checked' else 'absen' end as tanda 
						from presensi_mkmhs
						join presensi_kelas on presensi_mkmhs.id_presensi_kelas = presensi_kelas.id_presensi_kelas
						left join pengambilan_mk on presensi_mkmhs.id_mhs=pengambilan_mk.id_mhs and pengambilan_mk.id_kelas_mk = presensi_kelas.id_kelas_mk
						left join mahasiswa on presensi_mkmhs.id_mhs=mahasiswa.id_mhs
						left join pengguna on mahasiswa.id_pengguna=pengguna.id_pengguna
						left join kelas_mk on kelas_mk.id_kelas_mk = pengambilan_mk.id_kelas_mk
						left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
						left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
						left join materi_mk on presensi_kelas.id_materi_mk=materi_mk.id_materi_mk
						where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester
						and jadwal_kelas.id_jadwal_hari = $hari and jadwal_kelas.id_jadwal_jam = $jam and jadwal_kelas.id_ruangan = $ruang and nama_kelas.id_nama_kelas = $kelas
						and isi_materi_mk='$isi_materi_mk' and tgl_materi_mk = to_date('$tgl_materi_mk', 'DD-MM-YYYY')
						and waktu_mulai = '$jam_mulai' and waktu_selesai = '$jam_selesai' and tgl_presensi_kelas = to_date('$tgl_materi_mk', 'DD-MM-YYYY')
			");
		
		}
	}
	
	
	
	function mkwu_presensi_kelas_mhs($id_kur, $id_semester, $kelas, $id_mhs, $hari, $jam, $ruang, $isi_materi_mk, $tgl_materi_mk, $jam_mulai, $jam_selesai){
		
		$this->db->Query("
				select presensi_kelas.* from presensi_kelas 
							join kelas_mk on kelas_mk.id_kelas_mk = presensi_kelas.id_kelas_mk
							join mahasiswa on mahasiswa.id_program_studi = kelas_mk .id_program_studi
							left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
							left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
							left join materi_mk on presensi_kelas.id_materi_mk=materi_mk.id_materi_mk
							where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester
							and jadwal_kelas.id_jadwal_hari = $hari and jadwal_kelas.id_jadwal_jam = $jam and jadwal_kelas.id_ruangan = $ruang and nama_kelas.id_nama_kelas = $kelas
							and mahasiswa.id_mhs = '$id_mhs'
							and isi_materi_mk='$isi_materi_mk' and tgl_materi_mk = to_date('$tgl_materi_mk', 'DD-MM-YYYY')
							and waktu_mulai = '$jam_mulai' and waktu_selesai = '$jam_selesai' and tgl_presensi_kelas = to_date('$tgl_materi_mk', 'DD-MM-YYYY')

			");
			
		return $this->db->FetchAssoc();
	}
	
	
	
	function mkwu_presensi_hapus($id_kur, $id_semester, $kelas, $id_mhs, $hari, $jam, $ruang, $isi_materi_mk, $tgl_materi_mk, $jam_mulai, $jam_selesai){
	
		$this->db->Query("delete from presensi_mkmhs where id_presensi_kelas in (
								select presensi_mkdos.ID_PRESENSI_KELAS 
								from presensi_mkdos 
								left join presensi_kelas on presensi_kelas.ID_PRESENSI_KELAS = presensi_mkdos.ID_PRESENSI_KELAS
								join kelas_mk on kelas_mk.id_kelas_mk = presensi_kelas.id_kelas_mk
								left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
								left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
								join materi_mk on presensi_kelas.id_materi_mk=materi_mk.id_materi_mk
								where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester
								and jadwal_kelas.id_jadwal_hari = $hari and jadwal_kelas.id_jadwal_jam = $jam and jadwal_kelas.id_ruangan = $ruang 
								and nama_kelas.id_nama_kelas = $kelas and isi_materi_mk='$isi_materi_mk' and tgl_materi_mk = to_date('$tgl_materi_mk', 'DD-MM-YYYY')
								and waktu_mulai = '$jam_mulai' and waktu_selesai = '$jam_selesai' and tgl_presensi_kelas = to_date('$tgl_materi_mk', 'DD-MM-YYYY')
							)");
								
		$this->db->Query("delete from presensi_mkdos where id_presensi_kelas in (
								select presensi_mkdos.ID_PRESENSI_KELAS 
								from presensi_mkdos 
								left join presensi_kelas on presensi_kelas.ID_PRESENSI_KELAS = presensi_mkdos.ID_PRESENSI_KELAS
								join kelas_mk on kelas_mk.id_kelas_mk = presensi_kelas.id_kelas_mk
								left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
								left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
								join materi_mk on presensi_kelas.id_materi_mk=materi_mk.id_materi_mk
								where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester
								and jadwal_kelas.id_jadwal_hari = $hari and jadwal_kelas.id_jadwal_jam = $jam and jadwal_kelas.id_ruangan = $ruang 
								and nama_kelas.id_nama_kelas = $kelas and isi_materi_mk='$isi_materi_mk' and tgl_materi_mk = to_date('$tgl_materi_mk', 'DD-MM-YYYY')
								and waktu_mulai = '$jam_mulai' and waktu_selesai = '$jam_selesai' and tgl_presensi_kelas = to_date('$tgl_materi_mk', 'DD-MM-YYYY')
							)");
							
		$this->db->Query("delete from presensi_kelas where id_presensi_kelas in (
								select presensi_kelas.ID_PRESENSI_KELAS 
								from presensi_kelas
								join kelas_mk on kelas_mk.id_kelas_mk = presensi_kelas.id_kelas_mk
								left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
								left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
								join materi_mk on presensi_kelas.id_materi_mk=materi_mk.id_materi_mk
								where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester
								and jadwal_kelas.id_jadwal_hari = $hari and jadwal_kelas.id_jadwal_jam = $jam and jadwal_kelas.id_ruangan = $ruang 
								and nama_kelas.id_nama_kelas = $kelas and isi_materi_mk='$isi_materi_mk' and tgl_materi_mk = to_date('$tgl_materi_mk', 'DD-MM-YYYY')
								and waktu_mulai = '$jam_mulai' and waktu_selesai = '$jam_selesai' and tgl_presensi_kelas = to_date('$tgl_materi_mk', 'DD-MM-YYYY')
							)");
							
		$this->db->Query("delete from materi_mk where id_materi_mk in (
								select materi_mk.id_materi_mk 
								from materi_mk
								join kelas_mk on kelas_mk.id_kelas_mk = materi_mk.id_kelas_mk
								left join jadwal_kelas on kelas_mk.id_kelas_mk=jadwal_kelas.id_kelas_mk
								left join nama_kelas on kelas_mk.no_kelas_mk=nama_kelas.id_nama_kelas
								where kelas_mk.id_kurikulum_mk=$id_kur and kelas_mk.id_semester = $id_semester
								and jadwal_kelas.id_jadwal_hari = $hari and jadwal_kelas.id_jadwal_jam = $jam and jadwal_kelas.id_ruangan = $ruang 
								and nama_kelas.id_nama_kelas = $kelas and isi_materi_mk='$isi_materi_mk' and tgl_materi_mk = to_date('$tgl_materi_mk', 'DD-MM-YYYY')
							)");
	
	}
	
	
	function mkwu_presensi_cekal_view($id_semester, $id_kur){			
	
		if($id_kur != ''){
			$where = " and kurikulum_mk.id_kurikulum_mk=$id_kur ";	
		}

		return $this->db->QueryToArray("
			select id_mata_kuliah,kd_mata_kuliah,nm_mata_kuliah,sum(case when status='SUDAH' then 1 else 0 end) as ttl_sdh,
				sum(case when status='BELUM' then 1 else 0 end) as ttl_blm 
				from
				(select kurikulum_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,mata_kuliah.id_mata_kuliah,count(id_kelas_mk) 
				from kelas_mk
				join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
				join mata_kuliah on kurikulum_mk.id_mata_kuliah=mata_kuliah.id_mata_kuliah
				where kelas_mk.id_semester=$id_semester and kurikulum_mk.id_program_studi = 228 $where
				group by kurikulum_mk.id_kurikulum_mk,kd_mata_kuliah,nm_mata_kuliah,mata_kuliah.id_mata_kuliah)s1
				inner join (
				select kelas_mk.id_kurikulum_mk,case when (sum(case when status_cekal='2' then 1 else 0 end)>0)then 'BELUM' else 'SUDAH' end as status 
				from pengambilan_mk
				join kelas_mk on kelas_mk.id_kelas_mk = pengambilan_mk.id_kelas_mk
				where status_apv_pengambilan_mk=1 and pengambilan_mk.id_kelas_mk in 
							(select id_kelas_mk 
							from kelas_mk
							join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk
							where id_semester=$id_semester and kurikulum_mk.id_program_studi = 228 $where
							group by id_kelas_mk) 
				and pengambilan_mk.id_semester=$id_semester
				group by kelas_mk.id_kurikulum_mk)s2
				on s1.id_kurikulum_mk=s2.id_kurikulum_mk
				group by id_mata_kuliah,kd_mata_kuliah,nm_mata_kuliah
				order by kd_mata_kuliah,nm_mata_kuliah
			");
			
	}
	
	
	
	function mkwu_presensi_hitung_cekal($id_semester, $id_kur){
	
		return $this->db->QueryToArray("select kelas, mhs,pertemuan, masuk,round(((masuk/pertemuan)*100),2) as persen,
						case when (round(((masuk/pertemuan)*100),2)>=75 and status='0') or 
						(status in ('1','2','3','4')) or
						(round(((masuk/pertemuan)*100),2)=100 and status in ('5','6'))
						then 1 else 0 end as cekal_uas,id_pengambilan_mk
						from
						(select kelas_mk.id_kelas_mk as kelas,
						count(presensi_kelas.id_kelas_mk) as pertemuan, kurikulum_mk.status_mkta as status 
						from kelas_mk 
						left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk 
						left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
						left join program_studi on program_studi.id_program_studi = kurikulum_mk.id_program_studi
						left join JADWAL_SEMESTER_FAKULTAS a on a.id_fakultas = program_studi.id_fakultas and a.id_semester = kelas_mk.id_semester and a.id_kegiatan = 43
						left join JADWAL_SEMESTER_FAKULTAS b on b.id_fakultas = program_studi.id_fakultas  and b.id_semester = kelas_mk.id_semester and b.id_kegiatan = 47
						where kelas_mk.id_semester=$id_semester and kurikulum_mk.id_kurikulum_mk = $id_kur
						and tgl_presensi_kelas between a.TGL_MULAI_JSF AND b.TGL_MULAI_JSF 
						group by kelas_mk.id_kelas_mk, kurikulum_mk.status_mkta) k
						left join
						(select pengambilan_mk.id_kelas_mk, status_pengambilan_mk,id_pengambilan_mk,pengambilan_mk.id_mhs as mhs,
						sum(case when presensi_mkmhs.kehadiran >0 then 1 else 0 end) as masuk
						from pengambilan_mk
						inner join kelas_mk on pengambilan_mk.id_kelas_mk=kelas_mk.id_kelas_mk and kelas_mk.id_semester=$id_semester
						left join kurikulum_mk on kelas_mk.id_kurikulum_mk=kurikulum_mk.id_kurikulum_mk 
						left join program_studi on program_studi.id_program_studi = kurikulum_mk.id_program_studi
						left join JADWAL_SEMESTER_FAKULTAS a on a.id_fakultas = program_studi.id_fakultas and a.id_semester = kelas_mk.id_semester and a.id_kegiatan = 43
						left join JADWAL_SEMESTER_FAKULTAS b on b.id_fakultas = program_studi.id_fakultas  and b.id_semester = kelas_mk.id_semester and b.id_kegiatan = 47
						left join presensi_kelas on kelas_mk.id_kelas_mk=presensi_kelas.id_kelas_mk
						left join presensi_mkmhs on presensi_kelas.id_presensi_kelas=presensi_mkmhs.id_presensi_kelas and pengambilan_mk.id_mhs=presensi_mkmhs.id_mhs
						where pengambilan_mk.id_semester=$id_semester and kurikulum_mk.id_kurikulum_mk = $id_kur
						and tgl_presensi_kelas between a.TGL_MULAI_JSF AND b.TGL_MULAI_JSF
						group by pengambilan_mk.id_kelas_mk, status_pengambilan_mk, id_pengambilan_mk, pengambilan_mk.id_mhs)m
						on k.kelas=m.id_kelas_mk");
		
	}
	
}
?>
