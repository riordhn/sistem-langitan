<?php
error_reporting (E_ALL & ~E_NOTICE);
include 'config.php';
include 'class/evaluasi.class.php';
require('class/date.class.php');
$id_pengguna= $user->ID_PENGGUNA; 

$db = new MyOracle();
$eval = new evaluasi($db);
if(isset($_REQUEST['fak'])){
	
$evaluasi = $eval->evaluasi_penetapan($_REQUEST['fak'], $_REQUEST['jenjang'], $_REQUEST['prodi'], $_REQUEST['sem']);

}

ob_start();
?>

<style>
    .header_text{
        text-align: center;
        color: #ffffff;
        font-size: 14pt;
        font-weight: bold;
        background-color: #1d5700;
    }
    td{
        text-align: left;
    }
</style>
<table style="font-size:13px" border="1">
<thead class="fixedHeader">
	<tr>
		<th style="text-align:center">NO</th>
		<th style="text-align:center">NIM</th>
		<th style="text-align:center">NAMA</th>
        <th style="text-align:center">JENJANG</th>
		<th style="text-align:center">PRODI</th>
        <th style="text-align:center">HASIL PENETAPAN</th>        
	</tr>
</thead>
<tbody class="scrollContent">
<?PHP	
	$no = 1;
	foreach ($evaluasi as $data){
	echo '<tr>
		<td>'.$no++.'</td>
		<td>'."'".$data['NIM_MHS'].'</td>
		<td>'.$data['NM_PENGGUNA'].'</td>
        <td>'.$data['NM_JENJANG'].'</td>
		<td>'.$data['NM_PROGRAM_STUDI'].'</td>
		<td>'.$data['REKOMENDASI_PENETAPAN'].'</td>
        
    </tr>';
	}
echo '</tbody>
</table>';


$filename = 'Excel-evaluasi-penetapan' . date('Y-m-d');
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=$filename");
header("Pragma: ");
header("Cache-Control: ");
ob_flush();
?>
