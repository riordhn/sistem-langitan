<?php
include "config.php";
include 'class/report_mahasiswa.class.php';
$id_pengguna= $user->ID_PENGGUNA; 
$rm = new report_mahasiswa($db);

$db->Query("SELECT * FROM SEMESTER WHERE STATUS_AKTIF_SEMESTER = 'True'");
$semester_aktif = $db->FetchAssoc();
$smarty->assign('semester_aktif', $semester_aktif['ID_SEMESTER']);
$semester = $db->QueryToArray("SELECT * FROM SEMESTER 
								WHERE NM_SEMESTER = 'Genap' or NM_SEMESTER = 'Ganjil' ORDER BY THN_AKADEMIK_SEMESTER DESC, NM_SEMESTER DESC");
$smarty->assign('semester', $semester);
$smarty->assign('data_fakultas', $rm->get_fakultas());
$smarty->assign('data_jenjang', $rm->get_jenjang());
$smarty->assign('data_program_studi', $rm->get_program_studi_by_fakultas_jenjang(post('fakultas'), post('jenjang')));

if(isset($_GET['id'])){
$detail = $db->QueryToArray("SELECT DETAIL_BIAYA.BESAR_BIAYA, PENDAFTARAN_BIAYA, NM_BIAYA 
								FROM DETAIL_BIAYA 
								LEFT JOIN BIAYA ON BIAYA.ID_BIAYA = DETAIL_BIAYA.ID_BIAYA
								WHERE ID_BIAYA_KULIAH = '$_GET[id]'");

$smarty->assign('detail', $detail);

}elseif(isset($_POST['semester'])){
	
	if($_POST['fakultas'] <> '' and $_POST['program_studi'] <> '' and $_POST['jenjang'] <> ''){
		$where  = " AND PROGRAM_STUDI.ID_FAKULTAS = '$_POST[fakultas]' AND PROGRAM_STUDI.ID_PROGRAM_STUDI = '$_POST[program_studi]' AND PROGRAM_STUDI.ID_JENJANG = '$_POST[jenjang]'";
	}elseif($_POST['program_studi'] <> ''  and $_POST['jenjang'] <> ''){
		$where  = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI = '$_POST[program_studi]' AND PROGRAM_STUDI.ID_JENJANG = '$_POST[jenjang]'";
	}elseif($_POST['program_studi'] <> ''  and $_POST['fakultas'] <> ''){
		$where  = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI = '$_POST[program_studi]' AND PROGRAM_STUDI.ID_FAKULTAS = '$_POST[fakultas]'";
	}elseif($_POST['fakultas'] <> ''  and $_POST['jenjang'] <> ''){
		$where  = " AND PROGRAM_STUDI.ID_FAKULTAS = '$_POST[fakultas]' AND PROGRAM_STUDI.ID_JENJANG = '$_POST[jenjang]'";
	}elseif($_POST['program_studi'] <> ''){
		$where  = " AND PROGRAM_STUDI.ID_PROGRAM_STUDI = '$_POST[program_studi]'";
	}elseif($_POST['jenjang'] <> ''){
		$where  = " AND PROGRAM_STUDI.ID_JENJANG = '$_POST[jenjang]'";
	}elseif($_POST['fakultas'] <> ''){
		$where  = " AND PROGRAM_STUDI.ID_FAKULTAS = '$_POST[fakultas]'";
	}else{
		$where = "";
	}
$biaya = $db->QueryToArray("SELECT NM_PROGRAM_STUDI, NM_JENJANG, NM_JALUR, THN_AKADEMIK_SEMESTER, NM_SEMESTER, NM_KELOMPOK_BIAYA,
							BESAR_BIAYA_KULIAH, ID_BIAYA_KULIAH
							FROM BIAYA_KULIAH 
							LEFT JOIN PROGRAM_STUDI ON PROGRAM_STUDI.ID_PROGRAM_STUDI = BIAYA_KULIAH.ID_PROGRAM_STUDI
							LEFT JOIN SEMESTER ON SEMESTER.ID_SEMESTER = BIAYA_KULIAH.ID_SEMESTER
							LEFT JOIN JALUR ON JALUR.ID_JALUR = BIAYA_KULIAH.ID_JALUR
							LEFT JOIN JENJANG ON JENJANG.ID_JENJANG = PROGRAM_STUDI.ID_JENJANG
							LEFT JOIN KELOMPOK_BIAYA ON BIAYA_KULIAH.ID_KELOMPOK_BIAYA = KELOMPOK_BIAYA.ID_KELOMPOK_BIAYA
							WHERE BIAYA_KULIAH.ID_SEMESTER = '$_POST[semester]' $where");

$smarty->assign('biaya', $biaya);
}



$smarty->assign('id_fakultas', $_POST['fakultas']);
$smarty->assign('id_jenjang', $_POST['jenjang']);
$smarty->assign('id_semester', $_POST['semester']);
$smarty->assign('id_program_studi', $_POST['program_studi']);
$smarty->display('pembayaran/pembayaran-master-biaya.tpl');
?>
