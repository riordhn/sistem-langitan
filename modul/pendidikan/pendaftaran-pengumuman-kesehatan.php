<?php
include('config.php');
include('../ppmb/class/Penerimaan.class.php');

$penerimaan = new Penerimaan($db);
$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());


if(isset($_POST['tgl_awal'])){
    $db->Query("UPDATE PENERIMAAN SET TGL_AWAL_KESEHATAN = TO_DATE('$_POST[tgl_awal]','DD-MM-YYYY'), 
                TGL_AKHIR_KESEHATAN = TO_DATE('$_POST[tgl_akhir]','DD-MM-YYYY') 
                WHERE ID_PENERIMAAN = '$_POST[id_penerimaan]'");
    
}

if(isset($_GET['mode'])){
		$db->Query("UPDATE PENERIMAAN SET TGL_AWAL_KESEHATAN = '', 
                TGL_AKHIR_KESEHATAN = ''
                WHERE ID_PENERIMAAN = '$_GET[id]'");
}

$view_kesehatan = $db->QueryToArray("SELECT ID_PENERIMAAN, NM_PENERIMAAN, TAHUN, SEMESTER, 
									TO_CHAR(TGL_AWAL_KESEHATAN, 'DD-MM-YYYY') TGL_AWAL_KESEHATAN,
                                    TO_CHAR(TGL_AKHIR_KESEHATAN, 'DD-MM-YYYY') TGL_AKHIR_KESEHATAN
                                     FROM PENERIMAAN WHERE TGL_AWAL_KESEHATAN IS NOT NULL AND TGL_AKHIR_KESEHATAN IS NOT NULL
                                     ORDER BY TGL_AKHIR_KESEHATAN DESC");
$smarty->assign('view_kesehatan', $view_kesehatan);

$smarty->display("pendaftaran/pengumuman_kesehatan/pendaftaran-pengumuman-kesehatan.tpl");
?>
