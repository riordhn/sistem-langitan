<?php

include 'config.php';
include 'class/beasiswa.class.php';
include "class/laporan.class.php";
include 'class/report_mahasiswa.class.php';
include('../ppmb/class/Penerimaan.class.php');

$b = new beasiswa($db);
$aucc = new laporan($db);
$rm = new report_mahasiswa($db);
$penerimaan = new Penerimaan($db);

if(isset($_POST['tampilan'])){

	$smarty->assign('pengajuan_bpps', $b->diterima_bpps($_POST['id_penerimaan'], $_POST['fakultas'], $_POST['semester'], $_POST['jalur'], $_POST['thn'], $_POST['jenjang'], $_POST['gelombang']));
	
}


	$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());
	$smarty->assign('data_fakultas', $rm->get_fakultas());
	$smarty->assign('data_jenjang', $rm->get_jenjang_penerimaan());
	$smarty->assign('data_jalur', $rm->get_jalur());
	$smarty->assign('data_thn', $rm->get_thn_penerimaan());
    $smarty->display("beasiswa/beasiswa-terima-bpps.tpl");

?>