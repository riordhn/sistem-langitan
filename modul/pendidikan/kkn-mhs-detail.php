<?php
include('config.php');
include 'class/kkn.class.php';
include 'class/laporan.class.php';


$laporan = new laporan($db);
$kkn = new kkn($db);


if(isset($_REQUEST['jenis'])){
	$smarty->assign('kkn_mhs_detail', $kkn->kkn_mhs_detail($_REQUEST['jenis'], $_REQUEST['periode'], $_REQUEST['fakultas'], $_REQUEST['prodi']));
}

$smarty->assign('fakultas', $laporan->fakultas());
$smarty->assign('periode', $kkn->kkn_periode());


$smarty->display("kkn/kkn-mhs-detail.tpl")
?>