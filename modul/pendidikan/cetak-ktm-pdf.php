<?php
include('config.php');

$nim_mhs = get('nim');
$db->Query("
    SELECT P.NM_PENGGUNA,M.*,PS.NM_PROGRAM_STUDI,J.NM_JENJANG,KOT.NM_KOTA
    FROM MAHASISWA M  
    JOIN PENGGUNA P ON P.ID_PENGGUNA=M.ID_PENGGUNA
    JOIN PROGRAM_STUDI PS ON PS.ID_PROGRAM_STUDI = M.ID_PROGRAM_STUDI
    JOIN JENJANG J ON J.ID_JENJANG=PS.ID_JENJANG
    LEFT JOIN KOTA KOT ON KOT.ID_KOTA=M.ALAMAT_ASAL_MHS_KOTA
    WHERE M.NIM_MHS = '{$nim_mhs}'");
$mahasiswa = $db->FetchAssoc();

$width = 85.598;
$height = 54.102;
$orientation = ($height > $width) ? 'P' : 'L';
$pdf = new TCPDF($orientation, PDF_UNIT, [$width, $height], true, 'UTF-8', false);
$pdf->SetAuthor('Nambi Sembilu');
$pdf->SetMargins(0, 22, 0, false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->AddPage();
$html = <<<EOF
<style>
    td { font-size: 2.6mm; }
    table{margin:0px}
</style>
<table width="98%" border="0">
    <tr>
        <td width="27%" align="center">
        <img src="/foto_mhs/umaha/{nim}.jpg" width="45px" height="55px"/>
        </td>
        <td width="73%" align="left"><br/>
            <b>{nim}</b><br/>
            <b>{nama}</b><br/>
            <b>{prodi}</b><br/>
            {alamat}<br/>
            {kota_alamat}<br/>
        </td>
    </tr>
</table>
<br/>
EOF;
$html = str_replace('{nama}', strtoupper($mahasiswa['NM_PENGGUNA']), $html);
$html = str_replace('{nim}', strtoupper($mahasiswa['NIM_MHS']), $html);
$html = str_replace('{prodi}', strtoupper($mahasiswa['NM_JENJANG'] . ' ' . ($mahasiswa['NM_PROGRAM_STUDI']=='ANALIS KESEHATAN'?"TEKNOLOGI LAB MEDIK":$mahasiswa['NM_PROGRAM_STUDI'])), $html);
$alamat_mhs=!empty($mahasiswa['ALAMAT_MHS'])?$mahasiswa['ALAMAT_MHS']:$mahasiswa['ALAMAT_ASAL_MHS'];

// Manual Set
if (isset($_GET['alamat'])) {
    $alamat_mhs = $_GET['alamat'];
}

$html = str_replace('{alamat}', strtoupper($alamat_mhs), $html);
$html = str_replace('{kota_alamat}', strtoupper($mahasiswa['NM_KOTA']), $html);
$pdf->writeHTML($html,true,false,true,true,'C');
// CODE 128 A
$style = array(
    'position' => '',
    'align' => 'C',
    'stretch' => false,
    'fitwidth' => true,
    'cellfitalign' => '',
    'border' => false,
    'hpadding' => '13',
    'vpadding' => '0',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255),
    'text' => false,
    'font' => 'helvetica',
    'fontsize' => 8,
    'stretchtext' => 4
);
$pdf->write1DBarcode($nim_mhs, 'C128A', 0, 45, '', 6, 0.4, $style);
$pdf->Output();
