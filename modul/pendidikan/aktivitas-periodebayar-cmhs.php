<?php
include('config.php');
include('../ppmb/class/Penerimaan.class.php');

$mode = get('mode', 'view');
$id_periode_bayar = get('id_periode_bayar', '');


$penerimaan = new Penerimaan($db);
$smarty->assign('penerimaan_set', $penerimaan->GetAllForView());

if ($request_method == 'POST')
{
	$id_periode_bayar = post('id_periode_bayar');
	
	if ($_POST['mode'] == 'update')
	{
		$tgl_awal = post('tgl_awal');
		$tgl_akhir = post('tgl_akhir');
		$id_penerimaan = post('id_penerimaan');
		$db->Query("
			UPDATE PERIODE_BAYAR_CMHS SET
				TGL_AWAL_PERIODE_BAYAR = to_date('$tgl_awal','DD-MM-YYYY'),
				TGL_AKHIR_PERIODE_BAYAR = to_date('$tgl_akhir','DD-MM-YYYY'),
				ID_PENERIMAAN = '$id_penerimaan'
			WHERE ID_PERIODE_BAYAR_CMHS = {$id_periode_bayar}");
		
		if ($result)
		{
			$smarty->assign('periode_changed', true);
		}
	}
	
	
	if($_POST['mode'] == 'hapus'){
		$db->Query("DELETE PERIODE_BAYAR_CMHS WHERE ID_PERIODE_BAYAR_CMHS = $id_periode_bayar");
	}
	
	if (post('id_penerimaan') <> ''){
		
		$tgl_awal = post('tgl_awal');
		$tgl_akhir = post('tgl_akhir');
		$id_penerimaan = post('id_penerimaan');
		$db->Query("SELECT COUNT(*) AS CEK FROM PERIODE_BAYAR_CMHS WHERE ID_PENERIMAAN = '$id_penerimaan'");
		$cek = $db->FetchAssoc();
		
		if($cek['CEK'] < 1){
		$db->Query("
			INSERT INTO PERIODE_BAYAR_CMHS 
				(TGL_AWAL_PERIODE_BAYAR,
				TGL_AKHIR_PERIODE_BAYAR,
				ID_PENERIMAAN)
				VALUES (to_date('$tgl_awal','DD-MM-YYYY'), to_date('$tgl_akhir','DD-MM-YYYY'), '$id_penerimaan')");
		}
				
	}
}

if ($request_method == 'GET' || $request_method == 'POST')
{
	if ($mode == 'view')
	{		
		$periode_bayar_set = $db->QueryToArray("
			SELECT ID_PERIODE_BAYAR_CMHS, NM_PENERIMAAN, GELOMBANG, NM_JALUR, TGL_AWAL_PERIODE_BAYAR, TGL_AKHIR_PERIODE_BAYAR, TAHUN, SEMESTER
			FROM PERIODE_BAYAR_CMHS
			JOIN PENERIMAAN ON PENERIMAAN.ID_PENERIMAAN = PERIODE_BAYAR_CMHS.ID_PENERIMAAN
			JOIN JALUR ON JALUR.ID_JALUR = PENERIMAAN.ID_JALUR
			ORDER BY TAHUN DESC, SEMESTER DESC, GELOMBANG DESC, TGL_AKHIR_PERIODE_BAYAR DESC");
		$smarty->assign('periode_bayar_set', $periode_bayar_set);
	}
	
	if ($mode == 'edit' or $mode == 'delete')
	{
		$db->Query("
			SELECT ID_PERIODE_BAYAR_CMHS, NM_PENERIMAAN, GELOMBANG, NM_JALUR, 
			TO_CHAR(TGL_AWAL_PERIODE_BAYAR, 'DD-MM-YYYY') AS TGL_AWAL_PERIODE_BAYAR, 
			TO_CHAR(TGL_AKHIR_PERIODE_BAYAR, 'DD-MM-YYYY') AS TGL_AKHIR_PERIODE_BAYAR , PENERIMAAN.ID_PENERIMAAN, TAHUN, SEMESTER
			FROM PERIODE_BAYAR_CMHS
			JOIN PENERIMAAN ON PENERIMAAN.ID_PENERIMAAN = PERIODE_BAYAR_CMHS.ID_PENERIMAAN
			JOIN JALUR ON JALUR.ID_JALUR = PENERIMAAN.ID_JALUR
			WHERE ID_PERIODE_BAYAR_CMHS = '$_GET[id_periode_bayar]'");
			$periode_bayar_set = $db->FetchAssoc();
		$smarty->assign('pb', $periode_bayar_set);
	}
	
}

$smarty->display("aktivitas/periodebayarcmhs/{$mode}.tpl");
?>