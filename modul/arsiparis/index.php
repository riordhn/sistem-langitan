<?php
//ini_set("display_errors", 1);
include 'config.php';

if ($user->Role() == AUCC_ROLE_SEKRETARIAT_ARSIPARIS)
{
    // data menu dari $user
    $smarty->assign('modul_set', $user->MODULs);

    $smarty->display("index.tpl");
}
else
{
    header("location: /logout.php");
    exit();
}
?>
