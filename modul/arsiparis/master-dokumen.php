<?php
ini_set("display_errors", 1);
include ('config.php');
include ('includes/encrypt.php');

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

$id_unit_kerja_user = $user->ID_UNIT_KERJA;

$db->Query("select id_unit_kerja,type_unit_kerja from unit_kerja where ID_UNIT_KERJA = {$id_unit_kerja_user}");
$unit_kerja = $db->FetchAssoc();

$type_unit_kerja_user = $unit_kerja['TYPE_UNIT_KERJA'];

//echo $type_unit_kerja_user;// contoh menampilkan unit kerja dari user yang login */

$depan = time();
$belakang = strrev(time());

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_arsip_dokumen = post('id_arsip_dokumen');
        $kode_katalog = post('kode_katalog');
        $nomor_dokumen = post('nomor_dokumen');
        $nm_arsip_dokumen = post('nm_arsip_dokumen');
        $id_arsip_pemilik = post('id_arsip_pemilik');
        $tgl_penyusunan = post('tgl_penyusunan');
        $id_unit_kerja = post('id_unit_kerja');
        $id_arsip_loker = post('id_arsip_loker');
        $jml_halaman = post('jml_halaman');
        $contact_person = post('contact_person');
        $id_arsip_subkategori = post('arsip_subkategori');
        
        $result = $db->Query("update arsip_dokumen set kode_katalog = '{$kode_katalog}', nomor_arsip_dokumen = '{$nomor_dokumen}', nm_arsip_dokumen = '{$nm_arsip_dokumen}',
        						id_arsip_pemilik = '{$id_arsip_pemilik}', tgl_penyusunan = '{$tgl_penyusunan}', id_unit_kerja = '{$id_unit_kerja}',
        						id_arsip_loker = '{$id_arsip_loker}', jml_halaman = '{$jml_halaman}', contact_person = '{$contact_person}', 
                                id_arsip_subkategori = '{$id_arsip_subkategori}'
        						where id_arsip_dokumen = '{$id_arsip_dokumen}'");

        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah");
    }
    if (post('mode') == 'add')  
    {
        //$id_arsip_dokumen = post('id_arsip_dokumen');
        $kode_katalog = post('kode_katalog');
        $nomor_dokumen = post('nomor_dokumen');
        $nm_arsip_dokumen = post('nm_arsip_dokumen');
        $id_arsip_pemilik = post('id_arsip_pemilik');
        $tgl_penyusunan = post('tgl_penyusunan');
        $id_unit_kerja = post('id_unit_kerja');
        $id_arsip_loker = post('id_arsip_loker');
        $jml_halaman = post('jml_halaman');
        $contact_person = post('contact_person');
        $id_arsip_subkategori = post('arsip_subkategori');
        
        $result = $db->Query("insert into arsip_dokumen (kode_katalog, nomor_arsip_dokumen, nm_arsip_dokumen, id_arsip_pemilik, tgl_penyusunan, id_unit_kerja,
                                id_arsip_loker, jml_halaman, contact_person, id_perguruan_tinggi, id_arsip_subkategori) 
        						values ('{$kode_katalog}', '{$nomor_dokumen}', '{$nm_arsip_dokumen}', '{$id_arsip_pemilik}', '{$tgl_penyusunan}', '{$id_unit_kerja}',
                                '{$id_arsip_loker}', '{$jml_halaman}', '{$contact_person}', '{$id_pt}', '{$id_arsip_subkategori}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Maaf, Data gagal dimasukkan");
    }
    if (post('mode') == 'delete')  
    {
        $id_arsip_dokumen = post('id_arsip_dokumen');
        
        $result = $db->Query("delete from arsip_dokumen where id_arsip_dokumen = '{$id_arsip_dokumen}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Maaf, Data gagal dihapus");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$q = strtoupper(get('q', ''));

		// ketika ada get
		if (strlen(trim($q)) >= 3)
        {
        	// ketika tombol cari di klik, jika tipe unit kerja sekretariat,bisa melihat semua dokumen 310517 fth
			if($type_unit_kerja_user == "SEKRETARIAT"){        	
        	$arsip_dokumen_set = $db->QueryToArray("SELECT ad.*, ap.NM_ARSIP_PEMILIK, j.NM_JENJANG, uk.NM_UNIT_KERJA, al.NM_ARSIP_LOKER, ask.NM_ARSIP_SUBKATEGORI 
	                                                FROM ARSIP_DOKUMEN ad 
                                                    LEFT JOIN ARSIP_SUBKATEGORI ask ON ask.ID_ARSIP_SUBKATEGORI = ad.ID_ARSIP_SUBKATEGORI
                                                    LEFT JOIN ARSIP_KATEGORI ak ON ak.ID_ARSIP_KATEGORI = ask.ID_ARSIP_KATEGORI
													LEFT JOIN ARSIP_PEMILIK ap ON ap.ID_ARSIP_PEMILIK = ad.ID_ARSIP_PEMILIK
	                                                LEFT JOIN UNIT_KERJA uk ON uk.ID_UNIT_KERJA = ad.ID_UNIT_KERJA
	                                                LEFT JOIN PROGRAM_STUDI ps on ps.ID_PROGRAM_STUDI = uk.ID_PROGRAM_STUDI
            										LEFT JOIN JENJANG j on j.ID_JENJANG = ps.ID_JENJANG
													LEFT JOIN ARSIP_LOKER al ON al.ID_ARSIP_LOKER = ad.ID_ARSIP_LOKER
	                                                WHERE ad.ID_PERGURUAN_TINGGI = {$id_pt}
	                                                AND (upper(ad.KODE_KATALOG) like '%{$q}%' or upper(ad.NM_ARSIP_DOKUMEN) like '%{$q}%'
                                                    or upper(ask.NM_ARSIP_SUBKATEGORI) like '%{$q}%' 
                                                    or upper(ap.NM_ARSIP_PEMILIK) like '%{$q}%' or upper(ad.TGL_PENYUSUNAN) like '%{$q}%'
                                                    or upper(uk.NM_UNIT_KERJA) like '%{$q}%' or upper(al.NM_ARSIP_LOKER) like '%{$q}%'
                                                    or upper(ad.JML_HALAMAN) like '%{$q}%' or upper(ad.CONTACT_PERSON) like '%{$q}%')
													ORDER BY ad.KODE_KATALOG");
			$smarty->assignByRef('arsip_dokumen_set', $arsip_dokumen_set);
			}

			else {
			$arsip_dokumen_set = $db->QueryToArray("SELECT ad.*, ap.NM_ARSIP_PEMILIK, j.NM_JENJANG, uk.NM_UNIT_KERJA, al.NM_ARSIP_LOKER, ask.NM_ARSIP_SUBKATEGORI 
	                                                FROM ARSIP_DOKUMEN ad 
                                                    LEFT JOIN ARSIP_SUBKATEGORI ask ON ask.ID_ARSIP_SUBKATEGORI = ad.ID_ARSIP_SUBKATEGORI
                                                    LEFT JOIN ARSIP_KATEGORI ak ON ak.ID_ARSIP_KATEGORI = ask.ID_ARSIP_KATEGORI
													LEFT JOIN ARSIP_PEMILIK ap ON ap.ID_ARSIP_PEMILIK = ad.ID_ARSIP_PEMILIK
	                                                LEFT JOIN UNIT_KERJA uk ON uk.ID_UNIT_KERJA = ad.ID_UNIT_KERJA
	                                                LEFT JOIN PROGRAM_STUDI ps on ps.ID_PROGRAM_STUDI = uk.ID_PROGRAM_STUDI
            										LEFT JOIN JENJANG j on j.ID_JENJANG = ps.ID_JENJANG
													LEFT JOIN ARSIP_LOKER al ON al.ID_ARSIP_LOKER = ad.ID_ARSIP_LOKER
	                                                WHERE ad.ID_PERGURUAN_TINGGI = {$id_pt} AND (ad.ID_UNIT_KERJA = {$id_unit_kerja_user} OR uk.ID_UNIT_KERJA_INDUK = {$id_unit_kerja_user})
	                                                AND (upper(ad.KODE_KATALOG) like '%{$q}%' or upper(ad.NM_ARSIP_DOKUMEN) like '%{$q}%'
                                                    or upper(ask.NM_ARSIP_SUBKATEGORI) like '%{$q}%' 
                                                    or upper(ap.NM_ARSIP_PEMILIK) like '%{$q}%' or upper(ad.TGL_PENYUSUNAN) like '%{$q}%'
                                                    or upper(uk.NM_UNIT_KERJA) like '%{$q}%' or upper(al.NM_ARSIP_LOKER) like '%{$q}%'
                                                    or upper(ad.JML_HALAMAN) like '%{$q}%' or upper(ad.CONTACT_PERSON) like '%{$q}%')
													ORDER BY ad.KODE_KATALOG");
			$smarty->assignByRef('arsip_dokumen_set', $arsip_dokumen_set);
			}
		}
		else{
			// load semua

			if($type_unit_kerja_user == "SEKRETARIAT"){
				$arsip_dokumen_semua_set = $db->QueryToArray("SELECT ad.*, ap.NM_ARSIP_PEMILIK, j.NM_JENJANG, uk.NM_UNIT_KERJA, al.NM_ARSIP_LOKER, ask.NM_ARSIP_SUBKATEGORI 
	                                                        FROM ARSIP_DOKUMEN ad 
	                                                        LEFT JOIN ARSIP_SUBKATEGORI ask ON ask.ID_ARSIP_SUBKATEGORI = ad.ID_ARSIP_SUBKATEGORI
															LEFT JOIN ARSIP_PEMILIK ap ON ap.ID_ARSIP_PEMILIK = ad.ID_ARSIP_PEMILIK
			                                                LEFT JOIN UNIT_KERJA uk ON uk.ID_UNIT_KERJA = ad.ID_UNIT_KERJA
			                                                LEFT JOIN PROGRAM_STUDI ps on ps.ID_PROGRAM_STUDI = uk.ID_PROGRAM_STUDI
		            										LEFT JOIN JENJANG j on j.ID_JENJANG = ps.ID_JENJANG
															LEFT JOIN ARSIP_LOKER al ON al.ID_ARSIP_LOKER = ad.ID_ARSIP_LOKER
			                                                WHERE ad.ID_PERGURUAN_TINGGI = {$id_pt}
															ORDER BY ad.KODE_KATALOG");
				$smarty->assignByRef('arsip_dokumen_semua_set', $arsip_dokumen_semua_set);

			}
			else{
				$arsip_dokumen_semua_set = $db->QueryToArray("SELECT ad.*, ap.NM_ARSIP_PEMILIK, j.NM_JENJANG, uk.NM_UNIT_KERJA, al.NM_ARSIP_LOKER, ask.NM_ARSIP_SUBKATEGORI 
	                                                        FROM ARSIP_DOKUMEN ad 
	                                                        LEFT JOIN ARSIP_SUBKATEGORI ask ON ask.ID_ARSIP_SUBKATEGORI = ad.ID_ARSIP_SUBKATEGORI
															LEFT JOIN ARSIP_PEMILIK ap ON ap.ID_ARSIP_PEMILIK = ad.ID_ARSIP_PEMILIK
			                                                LEFT JOIN UNIT_KERJA uk ON uk.ID_UNIT_KERJA = ad.ID_UNIT_KERJA
			                                                LEFT JOIN PROGRAM_STUDI ps on ps.ID_PROGRAM_STUDI = uk.ID_PROGRAM_STUDI
		            										LEFT JOIN JENJANG j on j.ID_JENJANG = ps.ID_JENJANG
															LEFT JOIN ARSIP_LOKER al ON al.ID_ARSIP_LOKER = ad.ID_ARSIP_LOKER
			                                                WHERE ad.ID_PERGURUAN_TINGGI = {$id_pt} and (ad.ID_UNIT_KERJA = {$id_unit_kerja_user} or uk.ID_UNIT_KERJA_INDUK = {$id_unit_kerja_user})
															ORDER BY ad.KODE_KATALOG");
				$smarty->assignByRef('arsip_dokumen_semua_set', $arsip_dokumen_semua_set);
			}
		}
	}
	else if ($mode == 'edit' or $mode == 'delete')
	{
        /* if($type_unit_kerja_user == "SEKRETARIAT"){

        }
        else
        {
            */


    		$id_arsip_dokumen 	= (int)get('id_arsip_dokumen', '');

    		$arsip_dokumen = $db->QueryToArray("
                select ad.*, ask.id_arsip_kategori 
                from arsip_dokumen ad
                join arsip_subkategori ask on ask.id_arsip_subkategori = ad.id_arsip_subkategori
                where ad.id_arsip_dokumen = {$id_arsip_dokumen}");
            $smarty->assign('arsip_dokumen', $arsip_dokumen[0]);

            /*$db->Query("SELECT TYPE_arsip_dokumen FROM arsip_dokumen WHERE ID_PERGURUAN_TINGGI = '{$id_pt}'");
            $type_arsip_dokumen = $db->FetchAssoc();

            $isi_type = $type_arsip_dokumen['TYPE_arsip_dokumen'];

            $smarty->assign('isi_type', $isi_type);*/

            $arsip_pemilik_set = $db->QueryToArray("
    	            select id_arsip_pemilik, nm_arsip_pemilik 
    	            from arsip_pemilik
    	            where id_perguruan_tinggi = '{$id_pt}'");
    	    $smarty->assign('arsip_pemilik_set', $arsip_pemilik_set);

    	    /*
    	    $arsip_loker_set = $db->QueryToArray("
    	            select id_arsip_loker, nm_arsip_loker 
    	            from arsip_loker
    	            where id_perguruan_tinggi = '{$id_pt}'");
    	    $smarty->assign('arsip_loker_set', $arsip_loker_set);
    	    */
            /*
    	    $arsip_loker_set = $db->QueryToArray("
    	            select id_arsip_loker, nm_arsip_loker 
    	            from arsip_loker
    	            where id_perguruan_tinggi = '{$id_pt}'");
    	    $smarty->assign('arsip_loker_set', $arsip_loker_set);
            */
            if($type_unit_kerja_user == "SEKRETARIAT"){
                $arsip_loker_set = $db->QueryToArray("
                    select id_arsip_loker, nm_arsip_loker 
                    from arsip_loker al left join unit_kerja uk on uk.id_unit_kerja = al.id_unit_kerja
                    where uk.id_perguruan_tinggi = {$id_pt}");
            $smarty->assign('arsip_loker_set', $arsip_loker_set);

            	$unit_kerja_set = $db->QueryToArray("
                select uk.id_unit_kerja, uk.nm_unit_kerja, ps.nm_program_studi, j.nm_jenjang 
                from unit_kerja uk
                left join program_studi ps on ps.id_program_studi = uk.id_program_studi
                left join jenjang j on j.id_jenjang = ps.id_jenjang
                where uk.id_perguruan_tinggi = {$id_pt} ");
            $smarty->assign('unit_kerja_set', $unit_kerja_set);
            }
            else
            {
                $arsip_loker_set = $db->QueryToArray("
                    select id_arsip_loker, nm_arsip_loker 
                    from arsip_loker al left join unit_kerja uk on uk.id_unit_kerja = al.id_unit_kerja
                    where uk.id_perguruan_tinggi = {$id_pt} and (uk.id_unit_kerja ={$id_unit_kerja_user} or uk.ID_UNIT_KERJA_INDUK = {$id_unit_kerja_user}) ");
            $smarty->assign('arsip_loker_set', $arsip_loker_set);

            	$unit_kerja_set = $db->QueryToArray("
                select uk.id_unit_kerja, uk.nm_unit_kerja, ps.nm_program_studi, j.nm_jenjang 
                from unit_kerja uk
                left join program_studi ps on ps.id_program_studi = uk.id_program_studi
                left join jenjang j on j.id_jenjang = ps.id_jenjang
                where uk.id_perguruan_tinggi = {$id_pt}and (uk.ID_UNIT_KERJA = {$id_unit_kerja_user} or uk.ID_UNIT_KERJA_INDUK = {$id_unit_kerja_user}) ");
            $smarty->assign('unit_kerja_set', $unit_kerja_set);   
            }
            
            /*
    	    $unit_kerja_set = $db->QueryToArray("
                select uk.id_unit_kerja, uk.nm_unit_kerja, ps.nm_program_studi, j.nm_jenjang 
                from unit_kerja uk
                left join program_studi ps on ps.id_program_studi = uk.id_program_studi
                left join jenjang j on j.id_jenjang = ps.id_jenjang
                where uk.id_perguruan_tinggi = {$id_pt}and (uk.ID_UNIT_KERJA = {$id_unit_kerja_user} or uk.ID_UNIT_KERJA_INDUK = {$id_unit_kerja_user}) ");
            $smarty->assign('unit_kerja_set', $unit_kerja_set);
			*/
            $arsip_kategori_set = $db->QueryToArray("
                    select id_arsip_kategori, nm_arsip_kategori 
                    from arsip_kategori
                    where id_perguruan_tinggi = '{$id_pt}'");
            $smarty->assign('arsip_kategori_set', $arsip_kategori_set);

            $arsip_subkategori_set = $db->QueryToArray("
                    select id_arsip_subkategori, nm_arsip_subkategori 
                    from arsip_subkategori
                    where id_arsip_kategori = '{$arsip_dokumen[0]['ID_ARSIP_KATEGORI']}'");
            $smarty->assign('arsip_subkategori_set', $arsip_subkategori_set);


            /*$foto = getvar("select case when (pgg.gelar_belakang is null or pgg.gelar_belakang = '') then trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)) else trim(pgg.gelar_depan||' '||upper(pgg.nm_pengguna)||', '||pgg.gelar_belakang) end as nm_pengguna,
    		peg.nip_pegawai from pegawai peg left join pengguna pgg on pgg.id_pengguna=peg.id_pengguna
    		where peg.id_pengguna=$id_pgg");*/
            $to = $arsip_dokumen[0]['KODE_KATALOG'];
            $file = $arsip_dokumen[0]['ID_ARSIP_DOKUMEN'];
            $img = 'proses/upload_photo.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&yth=' . $to . '&file=' . $file . '&' . $belakang . '=' . $depan . $belakang) . '';
            $smarty->assign('IMG', $img);

            //$get_photo = getvar("select username as photo from pengguna where id_pengguna=$id_pgg");
            $filename = "../../files/arsiparis/" . $arsip_dokumen[0]['ID_ARSIP_DOKUMEN'] . ".JPG";
            if (file_exists($filename)) {
                $photo = $filename.'?t=timestamp';
            } else {
                $photo = 'includes/images/cancel.png';
            }
            $smarty->assign('PHOTO', $photo);

            // penambahan upload foto (sementara belum fleksibel)
            $to2 = $arsip_dokumen[0]['KODE_KATALOG'];
            $file2 = $arsip_dokumen[0]['ID_ARSIP_DOKUMEN']."_2";
            $img2 = 'proses/upload_photo.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&yth=' . $to2 . '&file=' . $file2 . '&' . $belakang . '=' . $depan . $belakang) . '';
            $smarty->assign('IMG2', $img2);

            //$get_photo = getvar("select username as photo from pengguna where id_pengguna=$id_pgg");
            $filename2 = "../../files/arsiparis/" . $file2 . ".JPG";
            if (file_exists($filename2)) {
                $photo2 = $filename2.'?t=timestamp';
            } else {
                $photo2 = 'includes/images/cancel.png';
            }
            $smarty->assign('PHOTO2', $photo2);
        //}
            // penambahan tombol upload file pdf 180517 fth :)
            /*$to2 = $arsip_dokumen[0]['KODE_KATALOG'];
            $file2 = $arsip_dokumen[0]['ID_ARSIP_DOKUMEN'];
            $img2 = 'proses/upload_photo.php?' . paramEncrypt($depan . '=' . $belakang . $depan . '&yth=' . $to . '&file=' . $file . '&' . $belakang . '=' . $depan . $belakang) . '';
            $smarty->assign('IMG2', $img2);

            //$get_photo = getvar("select username as photo from pengguna where id_pengguna=$id_pgg");
            $filename = "../../files/arsiparis/" . $arsip_dokumen[0]['ID_ARSIP_DOKUMEN'] . ".PDF";
            if (file_exists($filename)) {
                $photo = $filename.'?t=timestamp';
            } else {
                $photo = 'includes/images/cancel.png';
            }
            $smarty->assign('PHOTO', $photo);
            */

	}
	else if($mode == 'add')
	{
        if($type_unit_kerja_user == "SEKRETARIAT"){
            $arsip_pemilik_set = $db->QueryToArray("
                    select id_arsip_pemilik, nm_arsip_pemilik 
                    from arsip_pemilik
                    where id_perguruan_tinggi = '{$id_pt}'");
            $smarty->assign('arsip_pemilik_set', $arsip_pemilik_set);

            $arsip_loker_set = $db->QueryToArray("
                    select id_arsip_loker, nm_arsip_loker 
                    from arsip_loker al left join unit_kerja uk on uk.id_unit_kerja = al.id_unit_kerja
                    where uk.id_perguruan_tinggi = {$id_pt}");
            $smarty->assign('arsip_loker_set', $arsip_loker_set);

            /*$arsip_loker_set = $db->QueryToArray("
                    select id_arsip_loker, nm_arsip_loker 
                    from arsip_loker
                    where id_perguruan_tinggi = '{$id_pt}'");
            $smarty->assign('arsip_loker_set', $arsip_loker_set);
            */
            $unit_kerja_set = $db->QueryToArray("
                select uk.id_unit_kerja, uk.nm_unit_kerja, ps.nm_program_studi, j.nm_jenjang 
                from unit_kerja uk
                left join program_studi ps on ps.id_program_studi = uk.id_program_studi
                left join jenjang j on j.id_jenjang = ps.id_jenjang
                where uk.id_perguruan_tinggi = {$id_pt}");
            
            $smarty->assign('unit_kerja_set', $unit_kerja_set);

            $arsip_kategori_set = $db->QueryToArray("
                    select id_arsip_kategori, nm_arsip_kategori 
                    from arsip_kategori
                    where id_perguruan_tinggi = '{$id_pt}'");
            $smarty->assign('arsip_kategori_set', $arsip_kategori_set);
        }
        else
        {


    		$arsip_pemilik_set = $db->QueryToArray("
    	            select id_arsip_pemilik, nm_arsip_pemilik 
    	            from arsip_pemilik
    	            where id_perguruan_tinggi = '{$id_pt}'");
    	    $smarty->assign('arsip_pemilik_set', $arsip_pemilik_set);

    	    $arsip_loker_set = $db->QueryToArray("
    	            select id_arsip_loker, nm_arsip_loker 
    	            from arsip_loker al left join unit_kerja uk on uk.id_unit_kerja = al.id_unit_kerja
    	            where uk.id_perguruan_tinggi = {$id_pt} and (uk.id_unit_kerja ={$id_unit_kerja_user} or uk.ID_UNIT_KERJA_INDUK = {$id_unit_kerja_user}) ");
    	    $smarty->assign('arsip_loker_set', $arsip_loker_set);

    	    /*$arsip_loker_set = $db->QueryToArray("
    	            select id_arsip_loker, nm_arsip_loker 
    	            from arsip_loker
    	            where id_perguruan_tinggi = '{$id_pt}'");
    	    $smarty->assign('arsip_loker_set', $arsip_loker_set);
    		*/
    	    $unit_kerja_set = $db->QueryToArray("
                select uk.id_unit_kerja, uk.nm_unit_kerja, ps.nm_program_studi, j.nm_jenjang 
                from unit_kerja uk
                left join program_studi ps on ps.id_program_studi = uk.id_program_studi
                left join jenjang j on j.id_jenjang = ps.id_jenjang
                where uk.id_perguruan_tinggi = {$id_pt} and (uk.ID_UNIT_KERJA = {$id_unit_kerja_user} or uk.ID_UNIT_KERJA_INDUK = {$id_unit_kerja_user})");
            
            $smarty->assign('unit_kerja_set', $unit_kerja_set);

            $arsip_kategori_set = $db->QueryToArray("
                    select id_arsip_kategori, nm_arsip_kategori 
                    from arsip_kategori
                    where id_perguruan_tinggi = '{$id_pt}'");
            $smarty->assign('arsip_kategori_set', $arsip_kategori_set);
        }
	}
}

$smarty->display("master/dokumen/{$mode}.tpl");