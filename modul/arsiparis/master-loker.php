<?php
ini_set("display_errors", 1);
include 'config.php';

$mode = get('mode', 'view');
$id_pt = $id_pt_user;
$id_unit_kerja_user = $user->ID_UNIT_KERJA;

$db->Query("select id_unit_kerja,type_unit_kerja from unit_kerja where ID_UNIT_KERJA = {$id_unit_kerja_user}");
$unit_kerja = $db->FetchAssoc();

$type_unit_kerja_user = $unit_kerja['TYPE_UNIT_KERJA'];

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_arsip_loker = post('id_arsip_loker');
        $nm_arsip_loker = post('nm_arsip_loker');
        $id_unit_kerja = post('id_unit_kerja');
        if (strlen(trim($nm_arsip_loker)) >= 2){
        $result = $db->Query("update arsip_loker set nm_arsip_loker = '{$nm_arsip_loker}', id_unit_kerja = '{$id_unit_kerja}'
        						where id_arsip_loker = '{$id_arsip_loker}'");
        }
        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah, Data minimal 2 karakter");
    }

    if (post('mode') == 'add')
    {
        //$id_arsip_loker = post('id_arsip_loker');
        $nm_arsip_loker = post('nm_arsip_loker');
        $id_unit_kerja = post('id_unit_kerja');
        if (strlen(trim($nm_arsip_loker)) >= 2){
            $result = $db->Query("insert into arsip_loker (nm_arsip_loker, id_perguruan_tinggi,id_unit_kerja) 
        						values ('{$nm_arsip_loker}', '{$id_pt}', '{$id_unit_kerja}')");
        }

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }

    if (post('mode') == 'delete')
    {
        $id_arsip_loker = post('id_arsip_loker');
        
        $result = $db->Query("delete from arsip_loker where id_arsip_loker = '{$id_arsip_loker}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
        
        if( $type_unit_kerja_user == "SEKRETARIAT"){
            $arsip_loker_set = $db->QueryToArray("SELECT al.ID_ARSIP_LOKER, NM_ARSIP_LOKER,uk.NM_UNIT_KERJA,
                                                -- mencari relasi ke tabel ARSIP_DOKUMEN
                                                (SELECT COUNT(*) FROM ARSIP_DOKUMEN WHERE ID_ARSIP_LOKER=al.ID_ARSIP_LOKER) RELASI
                                                FROM ARSIP_LOKER al
                                                LEFT JOIN UNIT_KERJA uk on uk.ID_UNIT_KERJA = al.ID_UNIT_KERJA
                                                WHERE uk.ID_PERGURUAN_TINGGI = {$id_pt}
                                                ORDER BY NM_ARSIP_LOKER");
        $smarty->assignByRef('arsip_loker_set', $arsip_loker_set);
        
        }
        else
        {
            $arsip_loker_set = $db->QueryToArray("SELECT al.ID_ARSIP_LOKER, NM_ARSIP_LOKER,uk.NM_UNIT_KERJA,
                                                -- mencari relasi ke tabel ARSIP_DOKUMEN
                                                (SELECT COUNT(*) FROM ARSIP_DOKUMEN WHERE ID_ARSIP_LOKER=al.ID_ARSIP_LOKER) RELASI
                                                FROM ARSIP_LOKER al
                                                LEFT JOIN UNIT_KERJA uk on uk.ID_UNIT_KERJA = al.ID_UNIT_KERJA
                                                WHERE uk.ID_PERGURUAN_TINGGI = {$id_pt} and (al.ID_UNIT_KERJA = {$id_unit_kerja_user} or uk.ID_UNIT_KERJA_INDUK = {$id_unit_kerja_user})
                                                ORDER BY NM_ARSIP_LOKER");
        $smarty->assignByRef('arsip_loker_set', $arsip_loker_set);
            
        }
		
	}
	else if ($mode == 'edit' or $mode == 'delete')
	{
		$id_arsip_loker 	= (int)get('id_arsip_loker', '');

		$arsip_loker = $db->QueryToArray("
            select * 
            from arsip_loker 
            where id_arsip_loker = {$id_arsip_loker}");
        $smarty->assign('arsip_loker', $arsip_loker[0]);

        $unit_kerja_set = $db->QueryToArray("
            select uk.id_unit_kerja, uk.nm_unit_kerja, ps.nm_program_studi, j.nm_jenjang 
            from unit_kerja uk
            left join program_studi ps on ps.id_program_studi = uk.id_program_studi
            left join jenjang j on j.id_jenjang = ps.id_jenjang
            where uk.id_perguruan_tinggi = {$id_pt} and (uk.ID_UNIT_KERJA = {$id_unit_kerja_user} or uk.ID_UNIT_KERJA_INDUK = {$id_unit_kerja_user})");
        
        $smarty->assign('unit_kerja_set', $unit_kerja_set);


	}
	else if($mode == 'add')
	{
         if($type_unit_kerja_user == "SEKRETARIAT"){
             $unit_kerja_set = $db->QueryToArray("
                select uk.id_unit_kerja, uk.nm_unit_kerja, ps.nm_program_studi, j.nm_jenjang 
                from unit_kerja uk
                left join program_studi ps on ps.id_program_studi = uk.id_program_studi
                left join jenjang j on j.id_jenjang = ps.id_jenjang
                where uk.id_perguruan_tinggi = {$id_pt} ");
            
            $smarty->assign('unit_kerja_set', $unit_kerja_set);
        }
        else
        {
            $unit_kerja_set = $db->QueryToArray("
                select uk.id_unit_kerja, uk.nm_unit_kerja, ps.nm_program_studi, j.nm_jenjang 
                from unit_kerja uk
                left join program_studi ps on ps.id_program_studi = uk.id_program_studi
                left join jenjang j on j.id_jenjang = ps.id_jenjang
                where uk.id_perguruan_tinggi = {$id_pt} and (uk.ID_UNIT_KERJA = {$id_unit_kerja_user} or uk.ID_UNIT_KERJA_INDUK = {$id_unit_kerja_user})");
            $smarty->assign('unit_kerja_set', $unit_kerja_set);
        }
        
	}
}

$smarty->display("master/loker/{$mode}.tpl");