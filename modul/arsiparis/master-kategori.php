<?php
ini_set("display_errors", 1);
include 'config.php';

$mode = get('mode', 'view');
$id_pt = $id_pt_user;

if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_arsip_kategori = post('id_arsip_kategori');
        $nm_arsip_kategori = post('nm_arsip_kategori');
        if (strlen(trim($nm_arsip_kategori)) >= 2){
            $result = $db->Query("update arsip_kategori set nm_arsip_kategori = '{$nm_arsip_kategori}'
                                where id_arsip_kategori = '{$id_arsip_kategori}'");
        }
        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah, Data minimal 2 karakter");
    }

    if (post('mode') == 'add')
    {
        //$id_arsip_kategori = post('id_arsip_kategori');
        $nm_arsip_kategori = post('nm_arsip_kategori');
        if (strlen(trim($nm_arsip_kategori)) >= 2)
        $result = $db->Query("insert into arsip_kategori (nm_arsip_kategori, id_perguruan_tinggi) 
        						values ('{$nm_arsip_kategori}', '{$id_pt}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan, Data minimal 2 karakter");
    }

    if (post('mode') == 'delete')
    {
        $id_arsip_kategori = post('id_arsip_kategori');
        
        $result = $db->Query("delete from arsip_kategori where id_arsip_kategori = '{$id_arsip_kategori}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$arsip_kategori_set = $db->QueryToArray("SELECT ID_ARSIP_KATEGORI, NM_ARSIP_KATEGORI,
                                                -- mencari relasi ke tabel ARSIP_DOKUMEN
                                                (SELECT COUNT(*) FROM ARSIP_DOKUMEN JOIN ARSIP_SUBKATEGORI ask ON ask.ID_ARSIP_SUBKATEGORI = ARSIP_DOKUMEN.ID_ARSIP_SUBKATEGORI WHERE ask.ID_ARSIP_KATEGORI=ARSIP_KATEGORI.ID_ARSIP_KATEGORI) RELASI
                                                FROM ARSIP_KATEGORI
                                                WHERE ID_PERGURUAN_TINGGI = {$id_pt}
												ORDER BY NM_ARSIP_KATEGORI");
		$smarty->assignByRef('arsip_kategori_set', $arsip_kategori_set);

	}
	else if ($mode == 'edit' or $mode == 'delete')
	{
		$id_arsip_kategori 	= (int)get('id_arsip_kategori', '');

		$arsip_kategori = $db->QueryToArray("
            select * 
            from arsip_kategori 
            where id_arsip_kategori = {$id_arsip_kategori}");
        $smarty->assign('arsip_kategori', $arsip_kategori[0]);

	}
	else if($mode == 'add')
	{

	}
}

$smarty->display("master/kategori/{$mode}.tpl");