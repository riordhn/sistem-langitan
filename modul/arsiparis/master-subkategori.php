<?php
ini_set("display_errors", 1);
include 'config.php';

$mode = get('mode', 'view');
$id_pt = $id_pt_user;


if ($request_method == 'POST')
{
    if (post('mode') == 'edit')
    {
        $id_arsip_subkategori = post('id_arsip_subkategori');
        $id_arsip_kategori = post('id_arsip_kategori');
        $nm_arsip_subkategori = post('nm_arsip_subkategori');
        if (strlen(trim($nm_arsip_subkategori)) >= 2){
            $result = $db->Query("update arsip_subkategori set id_arsip_kategori = '{$id_arsip_kategori}', nm_arsip_subkategori = '{$nm_arsip_subkategori}'
                                where id_arsip_subkategori = '{$id_arsip_subkategori}'");
        }
        $smarty->assign('edited', $result ? "Data berhasil dirubah" : "Data gagal dirubah, Data minimal 2 karakter");
    }

    if (post('mode') == 'add')
    {
        //$id_arsip_subkategori = post('id_arsip_subkategori');
        $id_arsip_kategori = post('id_arsip_kategori');
        $nm_arsip_subkategori = post('nm_arsip_subkategori');
        if (strlen(trim($nm_arsip_subkategori)) >= 2)
            $result = $db->Query("insert into arsip_subkategori (id_arsip_kategori, nm_arsip_subkategori) 
                                    values ('{$id_arsip_kategori}', '{$nm_arsip_subkategori}')");

        $smarty->assign('edited', $result ? "Data berhasil dimasukkan" : "Data gagal dimasukkan");
    }

    if (post('mode') == 'delete')
    {
        $id_arsip_subkategori = post('id_arsip_subkategori');
        
        $result = $db->Query("delete from arsip_subkategori where id_arsip_subkategori = '{$id_arsip_subkategori}'");

        $smarty->assign('edited', $result ? "Data berhasil dihapus" : "Data gagal dihapus");
    }
}

if ($request_method == 'GET' or $request_method == 'POST')
{
	if ($mode == 'view')
	{
		$arsip_subkategori_set = $db->QueryToArray("SELECT ID_ARSIP_SUBKATEGORI, 
                                        (SELECT NM_ARSIP_KATEGORI 
                                            FROM ARSIP_KATEGORI
                                            WHERE ARSIP_KATEGORI.ID_ARSIP_KATEGORI = ARSIP_SUBKATEGORI.ID_ARSIP_KATEGORI) AS NM_ARSIP_KATEGORI, 
                                        NM_ARSIP_SUBKATEGORI,
                                        (SELECT COUNT(*) FROM ARSIP_DOKUMEN WHERE ID_ARSIP_SUBKATEGORI=ARSIP_SUBKATEGORI.ID_ARSIP_SUBKATEGORI) RELASI
                                                FROM ARSIP_SUBKATEGORI
                                                JOIN ARSIP_KATEGORI ab ON ab.ID_ARSIP_KATEGORI = ARSIP_SUBKATEGORI.ID_ARSIP_KATEGORI
                                                WHERE ab.ID_PERGURUAN_TINGGI = {$id_pt}
												ORDER BY NM_ARSIP_KATEGORI");
		$smarty->assignByRef('arsip_subkategori_set', $arsip_subkategori_set);

	}
	else if ($mode == 'edit' or $mode == 'delete')
	{
		$id_arsip_subkategori 	= (int)get('id_arsip_subkategori', '');

		$arsip_subkategori = $db->QueryToArray("
            select b.*, (select nm_arsip_kategori from arsip_kategori where id_arsip_kategori = b.id_arsip_kategori) as nm_arsip_kategori
            from arsip_subkategori b
            where b.id_arsip_subkategori = {$id_arsip_subkategori}");
        $smarty->assign('arsip_subkategori', $arsip_subkategori[0]);

        $kategori_set = $db->QueryToArray("
            select *
            from arsip_kategori
            where id_perguruan_tinggi = {$id_pt}");
        $smarty->assign('kategori_set', $kategori_set);

	}
	else if($mode == 'add')
	{
        $kategori_set = $db->QueryToArray("
            select *
            from arsip_kategori
            where id_perguruan_tinggi = {$id_pt}");
        $smarty->assign('kategori_set', $kategori_set);
	}
}

$smarty->display("master/subkategori/{$mode}.tpl");