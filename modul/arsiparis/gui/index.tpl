<!DOCTYPE html>
<html>
	<head>
		<title>Sekretariat - {$nama_pt}</title>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

		<link rel="stylesheet" type="text/css" href="//{$smarty.server.HTTP_HOST}/css/reset.css" />
		<link rel="stylesheet" type="text/css" href="//{$smarty.server.HTTP_HOST}/css/text.css" />
		<link rel="stylesheet" type="text/css" href="//{$smarty.server.HTTP_HOST}/css/ppmb.css" />
		<link rel="stylesheet" type="text/css" href="//{$smarty.server.HTTP_HOST}/css/jquery-ui/jquery-ui.min.css" />

		<!-- <link rel="shortcut icon" href="//{$smarty.server.HTTP_HOST}/img/icon.ico" /> -->
	</head>
	<body>
		<table class="clear-margin-bottom">
			<colgroup>
				<col />
				<col class="main-width"/>
				<col />
			</colgroup>
			<thead>
				<tr>
					<td class="header-left"></td>
					<td class="header-center" style="background-image: url('../../img/header/arsiparis-{$nama_singkat}.png')"></td>
					<td class="header-right"></td>
				</tr>
				<tr>
					<td class="tab-left"></td>
					<td class="tab-center">
						<ul>
							<!-- Untuk Menampilkan Menu Utama  -->
							{foreach $modul_set as $m}
								{if $m.AKSES == 1}
									<li><a href="#{$m.NM_MODUL}!{$m.PAGE}" class="nav">{$m.TITLE}</a></li>
									<li class="divider"></li>
									{/if}
								{/foreach}
							<li><a class="disable-ajax" href="//{$smarty.server.HTTP_HOST}/logout.php">Logout</a></li>
						</ul>
					</td>
					<td class="tab-right"></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="body-left">&nbsp;</td>
					<td class="body-center">
						<table class="content-table">
							<colgroup>
								<col />
								<col />
							</colgroup>
							<tr>
								<td colspan="2" id="breadcrumbs" class="breadcrumbs" ></td>
							</tr>
							<tr>
								<td id="menu" class="menu"></td>
								<td id="content" class="content">Loading data...</td>
							</tr>
						</table>
					</td>
					<td class="body-right">&nbsp;</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td class="foot-left">&nbsp;</td>
					<td class="foot-center">
						<div class="footer-nav">
							<br/>
						</div>
						<div class="footer">Copyright &copy; 2015 - {$nama_pt} <br />Sistem Langitan NU <br />oleh <a target="_blank" href="http://unair.ac.id" class="disable-ajax">Universitas Airlangga</a></div>
					</td>
					<td class="foot-right">&nbsp;</td>
				</tr>
			</tfoot>
		</table>

		<!-- Loading Javascript di akhir baris -->
		<script type="text/javascript" src="//{$smarty.server.HTTP_HOST}/js/jquery/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="//{$smarty.server.HTTP_HOST}/js/jquery/jquery-ui-1.9.2.min.js"></script>
		<script type="text/javascript" src="//{$smarty.server.HTTP_HOST}/js/jquery/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="//{$smarty.server.HTTP_HOST}/js/jquery-validation/jquery.validate.js"></script>
		<script type="text/javascript" src="//{$smarty.server.HTTP_HOST}/js/jquery-validation/additional-methods.js"></script>

		<script type="text/javascript">
			var defaultRel = 'master';
			var defaultPage = 'home.php';

			jQuery(document).ready(function() {
				// Refresh page sampai logout sendiri
				setInterval(function refreshSession() {
					$.ajax({
						url: '//{$smarty.server.HTTP_HOST}/includes/refresh-session.php'
					});
				}, 60000);
			});
		</script>
		<script type="text/javascript" src="//{$smarty.server.HTTP_HOST}/js/cybercampus.ajax-1.0.js"></script>

	</body>
</html>