<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Selamat Datang di Portal Cyber Campus Unair - UACC</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" lang="id-ID" content="portal cyber campus, unair, portal cyber campus unair, cyber campus"/>
		<meta name="description" lang="id-ID" content="{$discription}"/>
		<meta property="og:type" content="website"/>
		<meta property="og:title" content="Selamat Datang di Portal Cyber Campus Unair - UACC"/>
		<meta property="og:description" content="{$discription}"/>
		<meta property="og:image" content="{$img_link_link}"/>
		<meta property="og:url" content="{$cur_url}"/>
		<meta name="allow-search" content="yes" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta property="og:site_name" content="Cyber Campus Unair"/>
		<meta name="keywords" content="CYBER CAMPUS, KAMPUS, SISTEM IT TERINTEGRASI, UNAIR, KRS ONLINE, IT KAMPUS, CAMPUS, CYBER, CYBERCAMPUS, CYBER CAMPUS UNAIR" />
		<meta name="robots" content="index, follow" />
		<link rel="shortcut icon" href="api/images/uacc.ico"/>
		<link type="text/css" rel="stylesheet" href="css/portal.css" />
		<link type="text/css" rel="stylesheet" href="api/css/contentslider.css" />
		<link type="text/css" rel="stylesheet" href="css/custom-theme/jquery-ui-fix-portal.css" />
		<link type="text/css" rel="stylesheet" href="css/superfish.css" media="screen" />
		<link type="text/css" rel="stylesheet" href="api/css/popup.css" media="screen" />
	</head>
	<body id="body">
		<div id="main_container">
			<header>
				<div id="header">
					<div id="header-content">
						<div></div>
						<div id="header-content-login" name="loginForm">
							<!-- Login Space --> 
						</div>
					</div>
				</div>
			</header>
			<div id="main_content">
				<div id="header-menu">
					<div id="menu_tab">
						<ul class="sf-menu">
							<li class="current"><a href="index.php" class="disable-ajax"><span class="notranslate">{if $lang=='en-flg.gif'}BERANDA{else}HOME{/if}</span></a></li>
							<li class="current"><a href="#!portal_perkuliahan.php"><span class="notranslate">{if $lang=='en-flg.gif'}PERKULIAHAN{else}LECTURES{/if}</span></a></li>
							<li class="current">
								<a class="disable-ajax"><span class="notranslate">CYBER CAMPUS</span></a> 
								<ul>
									<li><a href="#!portal-about.php" class="disable-ajax"><span class="notranslate">{if $lang=='en-flg.gif'}PERIHAL{else}ABOUT{/if}</span></a></li>
									<li><a href="#!portal-products.php "class="disable-ajax"><span class="notranslate">{if $lang=='en-flg.gif'}PRODUK{else}PRODUCT{/if}</span></a></li>
									<li><a href="#!services-cybercampus.php" class="disable-ajax"><span class="notranslate">{if $lang=='en-flg.gif'}LAYANAN{else}SERVICE{/if}</span></a></li>
								</ul>
							</li>
							<li class="current"><a href="#!portal_news.php"><span class="notranslate">{if $lang=='en-flg.gif'}BERITA{else}NEWS{/if}</span></a></li>
							<li class="current"><a href="#!portal_article.php"><span class="notranslate">{if $lang=='en-flg.gif'}ARTIKEL{else}ARTICLE{/if}</span></a></li>
							<li class="current">
								<a class="disable-ajax"><span class="notranslate">JEJARING SOSIAL</span></a> 
								<ul>
									<li><a href="#!portal_sncc.php" class="disable-ajax"><span class="notranslate">{if $lang=='en-flg.gif'}CYBERCAMPUS{else}CYBERCAMPUS{/if}</span></a></li>
									<li><a href="https://www.facebook.com/universitasairlangga" class="disable-ajax"><span class="notranslate">{if $lang=='en-flg.gif'}UNAIR{else}UNAIR{/if}</span></a></li>
									<li><a href="https://www.facebook.com/groups/cybercampus/" class="disable-ajax"><span class="notranslate">{if $lang=='en-flg.gif'}GROUP FACEBOOK{else}FACEBOOK GROUP{/if}</span></a></li>
								</ul>
							</li>
							<li class="current"><a href="#!portal_blog.php"><span class="notranslate">BLOG</span></a></li>
							<li class="current"><a href="#!portal_ormawa.php"><span class="notranslate">ORMAWA</span></a></li>
							<li class="current"><a href="#!portal-contacts.php"><span class="notranslate">{if $lang=='en-flg.gif'}KONTAK{else}CONTACT{/if}</span></a></li>
							<li class="current"><a href="http://kb.dsi.unair.ac.id"><span class="disable-ajax">FAQ</span></a></li>
							<li class="current"></li>
						</ul>
					</div>
				</div>
			</div>
			<div id="content" class="content" style="clear:both;">
				<div id="left_content">
					<div id="line-width" style="width:200px;">&nbsp;</div>
					 
				</div>
				<div id="center_content">
					<div id="popupContact" style="display:none;">
						<a id="popupContactClose">x</a> 
						<h1>
							<center>{$judul_news}</center>
						</h1>
						<p id="contactArea"> 
							<div style="text-align:center;">News - Universitas Airlangga Cyber Campus</div>
							<hr>
								<br />{$resume_news}Baca selengkapnya <a href="#!portal_news.php?category=news&id={$id_news}"><span id="link_news">disini</span></a> </p> 
					</div>
					<div id="backgroundPopup"></div>
					<div id="center_data_content" class="center_data_content" style></div>
					<div id="line-width" style="width:593px;">&nbsp;</div>
				</div>
				<div id="right_content">
					<div id="line-width" style="width:200px;">&nbsp;</div>
				</div>
			</div>
			<footer>
				<div id="footer" class="footer" style="clear:both;">
					<center>
						<div class="left_footer"><img src="/img/dosen/footer_logo.jpg" /></div>
						<div class="center_footer">
							<center>Universitas Airlangga &copy; 2011 - {date('Y')} <br/> All Rights Reserved<br /><br/>{$smarty.server.REMOTE_ADDR}</center>
						</div>
						<div class="right_footer" style="font-size:11;"> 
							<a href="index.php" class="disable-ajax">Home</a> 
							<a href="#" class="disable-ajax">Sitem Map</a> 
							<a href="#" class="disable-ajax">RSS</a> 
							<a href="#" class="disable-ajax">Privacy Policy</a> 
						</div>
					</center>
				</div>
			</footer>
		</div>
		
		<!-- Footer Script -->
		<script src="js/?f=jquery-1.6.2.min.js"></script> 
		<script type="text/javascript" src="api/js/recaptcha_ajax.js"></script> 
		<script type="text/javascript" src="js/jquery-ui-fix-portal.min.js"></script> 
		<script type="text/javascript" src="js/portal.js"></script> 
		<script type="text/javascript" src="js/hoverIntent.js"></script> 
		<script type="text/javascript" src="js/superfish.js"></script> 
		<script type="text/javascript" src="api/js/popup.js"></script>
		
		<script type="text/javascript">
			$(function() {
				$("#selectable").selectable();
			});

			function usernameKlik() {
				var username = document.getElementById('username');
				if (username.value === "Username") {
					username.value = "";
					document.getElementById('username').style.color = '#000';
					document.getElementById('username').style.font.weight = 'bold';
				}
			}

			function passwordKlik() {
				var password = document.getElementById('password');
				if (password.value === "Password") {
					password.value = "";
					document.getElementById('password').style.color = '#000';
				}
			}
		</script>
		<script type="text/javascript" src="./unair_files/contentslider.js"></script> 
		<script type="text/javascript">
			jQuery(document).ready(function() {
				$("ul.subnav").parent().append("<span></span>"); //Only shows drop down trigger when js is enabled (Adds empty span tag after ul.subnav*)
				$("ul.topnav li span").click(function() { //When trigger is clicked...
					//Following events are applied to the subnav itself (moving subnav up and down)
					$(this).parent().find("ul.subnav").slideDown('fast').show(); //Drop down the subnav on click
					$(this).parent().hover(function() {
					}, function() {
						$(this).parent().find("ul.subnav").slideUp('slow'); //When the mouse hovers out of the subnav, move it back up
					});

					//Following events are applied to the trigger (Hover events for the trigger)
				}).hover(function() {
					$(this).addClass("subhover"); //On hover over, add class "subhover"
				}, function() {	//On Hover Out
					$(this).removeClass("subhover"); //On hover out, remove class "subhover"
				});
			});

		</script> 
		<script type="text/javascript">
			// initialise plugins
			jQuery(function() {
				jQuery('ul.sf-menu').superfish();
			});
		</script>
		<script>
			$(document).ready(function() {
				$('ul.sf-menu').superfish({
					delay: 0, // one second delay on mouseout 
					animation: {
						opacity: 'show', height: 'show'
					}, // fade-in and slide-down animation 
					speed: 'fast', // faster animation speed 
					autoArrows: false, // disable generation of arrow mark-up 
					dropShadows: false                            // disable drop shadows 
				});

				centerPopup();
				loadPopup();
			});

		</script> 
		
		<!-- Load Portal component -->
		<script>
			jQuery(document).ready(function() {
				$('#header-content-login').load('portal-login-space.php');
				$('#left_content').load('portal_left_side.php');
				$('#right_content').load('portal_right_side.php');
			});
		</script>
	</body>
</html>