<div class="center_title_bar">Loker Arsip</div>

{if isset($arsip_loker_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Arsip Loker</th>
				<th>Unit kerja </th>
				
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $arsip_loker_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td><strong>{$uk.NM_ARSIP_LOKER}</strong></td>
					<td><strong>{$uk.NM_UNIT_KERJA}</strong></td>

					
					<td class="center">
						<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="master-loker.php?mode=edit&id_arsip_loker={$uk.ID_ARSIP_LOKER}"><b style="color: green">Edit</b></a>
						{if $uk.RELASI == 0}
							<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="master-loker.php?mode=delete&id_arsip_loker={$uk.ID_ARSIP_LOKER}"><b style="color: red">Delete</b></a>
						{/if}
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="9" class="center">
					<a href="master-loker.php?mode=add"><b style="color: red">Tambah Master loker</b></a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}