<div class="center_title_bar">ID Sub-Kategori Arsip : {$arsip_subkategori.ID_ARSIP_SUBKATEGORI}</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="master-subkategori.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="edit" />
<input type="hidden" name="id_arsip_subkategori" value="{$arsip_subkategori.ID_ARSIP_SUBKATEGORI}" />
<!--<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table>
    <tr>
        <th colspan="2">Detail Sub-Kategori Arsip</th>
    </tr>
    <tr>
        <td>ID</td>
        <td>{$arsip_subkategori.ID_ARSIP_SUBKATEGORI}</td>
    </tr>
    <tr>
        <td>Nama Kategori Arsip</td>
        <td>
            <select name="id_arsip_kategori">
                {foreach $kategori_set as $data}
                    <option value="{$data.ID_ARSIP_KATEGORI}" {if $arsip_subkategori.ID_ARSIP_KATEGORI==$data.ID_ARSIP_KATEGORI}selected="true"{/if}>{$data.NM_ARSIP_KATEGORI}</option>
                {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Nama Sub-Kategori Arsip</td>
        <td><input class="form-control" type="text" name="nm_arsip_subkategori" size="70" value="{$arsip_subkategori.NM_ARSIP_SUBKATEGORI}"  />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

<a href="master-subkategori.php">Kembali</a>