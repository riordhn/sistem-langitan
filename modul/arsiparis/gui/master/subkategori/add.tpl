<div class="center_title_bar">Tambah Sub-Kategori Arsip</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

<form action="master-subkategori.php?{$smarty.server.QUERY_STRING}" method="post">
<input type="hidden" name="mode" value="add" />
<!--<input type="hidden" name="id_role" value="{$smarty.get.id_role}" />
<input type="hidden" name="id_join_table" value="{$pengguna.JOIN_TABLE}" /> -->
<table style="width: 100%">
    <tr>
        <th colspan="2">Detail Sub-Kategori Arsip</th>
    </tr>
    <tr>
        <td>Nama Kategori Arsip</td>
        <td>
            <select name="id_arsip_kategori">
                <option value="">Pilih Kategori</option>
                {foreach $kategori_set as $data}
                    <option value="{$data.ID_ARSIP_KATEGORI}">{$data.NM_ARSIP_KATEGORI}</option>
                {/foreach}
            </select>
        </td>
    </tr>
    <tr>
        <td>Nama Sub-Kategori Arsip</td>
        <td><input class="form-control" type="text" size="70" name="nm_arsip_subkategori" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <input type="submit" value="Simpan" />
        </td>
    </tr>
</table>
</form>

<a href="master-subkategori.php">Kembali</a>