<div class="center_title_bar">Master Sub-Kategori Arsip</div>

{if $edited}<script type="text/javascript">alert('{$edited}');</script>{/if}

{if isset($arsip_subkategori_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Kategori Arsip</th>
				<th>Nama Sub-Kategori Arsip</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $arsip_subkategori_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					<td>{$uk.NM_ARSIP_KATEGORI}</td>
					<td>{$uk.NM_ARSIP_SUBKATEGORI}</td>
					<td class="center">
						<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="master-subkategori.php?mode=edit&id_arsip_subkategori={$uk.ID_ARSIP_SUBKATEGORI}"><b style="color: green">Edit</b></a>
						{if $uk.RELASI == 0}
							<a class="ui-button ui-corner-all ui-state-hover" style="padding:5px;cursor:pointer;" href="master-subkategori.php?mode=delete&id_arsip_subkategori={$uk.ID_ARSIP_SUBKATEGORI}"><b style="color: red">Delete</b></a>
						{/if}
					</td>
				</tr>
			{/foreach}
			<tr>
				<td colspan="9" class="center">
					<a href="master-subkategori.php?mode=add"><b style="color: red">Tambah Sub-Kategori Arsip</b></a>
				</td>
			</tr>
		</tbody>
	</table>
{/if}