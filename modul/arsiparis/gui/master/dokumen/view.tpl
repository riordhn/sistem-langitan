<div class="center_title_bar">Master Dokumen</div>

<form action="master-dokumen.php" method="get">
    <table>
        <tr>
            <td>Kata Kunci : </td>
            <td><input type="search" name="q" value="{$smarty.get.q}" size="40" /></td>
            <td><input type="submit" value="Cari"/></td>
        </tr>
        <tr>
			<td colspan="3" class="center">
				<a href="master-dokumen.php?mode=add"><b style="color: red">Tambah Dokumen</b></a>
			</td>
		</tr>
    </table>
</form>

{if !empty($arsip_dokumen_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<!-- <th>Download Data</th> -->
				<th>Scan Photo</th>
				<th>Kode Katalog</th>
				<th>Nomor</th>
				<th>Sub-Kategori Dokumen</th>
				<th>Nama Dokumen</th>
				<th>Pemilik</th>
				<th>Tgl Penyusunan</th>
				<th>Unit Kerja</th>
				<th>Loker</th>
				<th>Jml Halaman</th>
				<th>Contact Person</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $arsip_dokumen_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					{assign var="image" 
					value="../../files/arsiparis/{$uk.ID_ARSIP_DOKUMEN}.JPG"} 
					{assign var="image2" 
					value="../../files/arsiparis/{$uk.ID_ARSIP_DOKUMEN}_2.JPG"}
					{if file_exists($image) or file_exists($image2)}
						{if file_exists($image)}
							<td>
								<img src="../../files/arsiparis/{$uk.ID_ARSIP_DOKUMEN}.JPG?t=timestamp" border="0" width="50" />
							</td>
						{else}
							<td><img src="includes/images/cancel.png" border="0" width="50" /></td>
						{/if}
						{if file_exists($image2)}
							<td>
								<img src="../../files/arsiparis/{$uk.ID_ARSIP_DOKUMEN}_2.JPG?t=timestamp" border="0" width="50" />
							</td>
						{else}
							<td><img src="includes/images/cancel.png" border="0" width="50" /></td>	
						{/if}
					{else}
						<td colspan="2" class="center"><img src="includes/images/cancel.png" border="0" width="50" /></td>
					{/if}
					<td><strong>{$uk.KODE_KATALOG}</strong></td>
					<td><strong>{$uk.NOMOR_ARSIP_DOKUMEN}</strong></td>
					<td>{$uk.NM_ARSIP_SUBKATEGORI}</td>
					<td>{$uk.NM_ARSIP_DOKUMEN}</td>
					<td>{$uk.NM_ARSIP_PEMILIK}</td>
					<td>{$uk.TGL_PENYUSUNAN}</td>
					<td>{$uk.NM_JENJANG} {$uk.NM_UNIT_KERJA}</td>
					<td>{$uk.NM_ARSIP_LOKER}</td>
					<td>{$uk.JML_HALAMAN}</td>
					<td>{$uk.CONTACT_PERSON}</td>
					<td class="center">
						<a href="master-dokumen.php?mode=edit&id_arsip_dokumen={$uk.ID_ARSIP_DOKUMEN}"><b style="color: blue">Edit</b></a>
						<a href="master-dokumen.php?mode=delete&id_arsip_dokumen={$uk.ID_ARSIP_DOKUMEN}"><b style="color: red">Hapus</b></a>
					</td>
				</tr>
			{/foreach}
			<!-- <tr>
				<td colspan="9" class="center">
					<a href="master-dokumen.php?mode=add"><b style="color: red">Tambah Unit Kerja</b></a>
				</td>
			</tr> -->
		</tbody>
	</table>
{else if isset($arsip_dokumen_semua_set)}
	<table class="tableabout">
		<thead>
			<tr>
				<th>No</th>
				<!-- <th>Download Data</th> -->
				<th colspan="2" class="center">Scan Photo</th>
				<th>Kode Katalog</th>
				<th>Nomor</th>
				<th>Sub-Kategori Dokumen</th>
				<th>Nama Dokumen</th>
				<th>Pemilik</th>
				<th>Tgl Penyusunan</th>
				<th>Unit Kerja</th>
				<th>Loker</th>
				<th>Jml Halaman</th>
				<th>Contact Person</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			{foreach $arsip_dokumen_semua_set as $uk}
				<tr {if $uk@index is not div by 2}class="odd"{/if}>
					<td class="center">{$uk@index + 1}</td>
					{assign var="image" 
					value="../../files/arsiparis/{$uk.ID_ARSIP_DOKUMEN}.JPG"} 
					{assign var="image2" 
					value="../../files/arsiparis/{$uk.ID_ARSIP_DOKUMEN}_2.JPG"}
					{if file_exists($image) or file_exists($image2)}
						{if file_exists($image)}
							<td>
								<img src="../../files/arsiparis/{$uk.ID_ARSIP_DOKUMEN}.JPG?t=timestamp" border="0" width="50" />
							</td>
						{else}
							<td><img src="includes/images/cancel.png" border="0" width="50" /></td>
						{/if}
						{if file_exists($image2)}
							<td>
								<img src="../../files/arsiparis/{$uk.ID_ARSIP_DOKUMEN}_2.JPG?t=timestamp" border="0" width="50" />
							</td>
						{else}
							<td><img src="includes/images/cancel.png" border="0" width="50" /></td>	
						{/if}
					{else}
						<td colspan="2" class="center"><img src="includes/images/cancel.png" border="0" width="50" /></td>
					{/if}
					<td><strong>{$uk.KODE_KATALOG}</strong></td>
					<td><strong>{$uk.NOMOR_ARSIP_DOKUMEN}</strong></td>
					<td>{$uk.NM_ARSIP_SUBKATEGORI}</td>
					<td>{$uk.NM_ARSIP_DOKUMEN}</td>
					<td>{$uk.NM_ARSIP_PEMILIK}</td>
					<td>{$uk.TGL_PENYUSUNAN}</td>
					<td>{$uk.NM_JENJANG} {$uk.NM_UNIT_KERJA}</td>
					<td>{$uk.NM_ARSIP_LOKER}</td>
					<td>{$uk.JML_HALAMAN}</td>
					<td>{$uk.CONTACT_PERSON}</td>
					<td class="center">
						<a href="master-dokumen.php?mode=edit&id_arsip_dokumen={$uk.ID_ARSIP_DOKUMEN}"><b style="color: blue">Edit</b></a>
						<a href="master-dokumen.php?mode=delete&id_arsip_dokumen={$uk.ID_ARSIP_DOKUMEN}"><b style="color: red">Hapus</b></a>
					</td>
				</tr>
			{/foreach}
			<!-- <tr>
				<td colspan="10" class="center">
					<a href="master-dokumen.php?mode=add"><b style="color: red">Tambah Dokumen</b></a>
				</td>
			</tr> -->
		</tbody>
	</table>
{/if}